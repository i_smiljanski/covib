/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.utd.isr.review.db;

import com.utd.isr.utils.ws.SOAPUtil;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.HashMap;

/**
 *
 * @author schroedera85
 */
public class ReviewServerManager {

    private Connection connection;

    public  HashMap<String, String> createReviewServer(String sessionId, String documentId) throws Exception {
        int[] ports = getFreeReviewServerPorts();
        if (ports == null) {
            throw new Exception("there are not free ports");
        }

        HashMap<String, String> reviewServerProperties = startReviewServer(sessionId, documentId, ports);
        return reviewServerProperties;
    }

    public void setConnection(Connection connection) {
        this.connection = connection;
    }

    private int[] getFreeReviewServerPorts() throws Exception {
        int[] freePorts = new int[2];
        String sql = "SELECT port FROM ISR$REVIEW$SERVER$PORTS where state = ?";
        PreparedStatement stmt = connection.prepareStatement(sql);
        stmt.setString(1, "free");
        ResultSet resultSet = stmt.executeQuery();

        for (int i = 0; i < 2; i++) {
            if (resultSet.next()) {
                freePorts[i] = resultSet.getInt(1);
            } else {
                return null;
            }
        }
        return freePorts;
    }

    public void setReviewServerPortState(HashMap<Integer, String> portsStates) throws Exception {
        String query = " update  ISR$REVIEW$SERVER$PORTS set state = ? where port = ?";

        // create the mysql insert preparedstatement
        PreparedStatement preparedStmt = connection.prepareStatement(query);
        for (Integer port : portsStates.keySet()) {
            String state = portsStates.get(port);
            preparedStmt.setString(1, state);
            preparedStmt.setInt(2, port);
            preparedStmt.addBatch();
        }
        preparedStmt.executeBatch();
    }

    private HashMap<String, String> getReviewObserverProperties() throws Exception {
        ReviewManager reviewManager = new ReviewManager();
        reviewManager.setConnection(connection);
        return reviewManager.getReviewObserverProperties();
    }

    private HashMap<String, String> startReviewServer(String sessionId, String documentId, int[] ports) throws Exception {
        HashMap<String, String> observerProperties = getReviewObserverProperties();
        String observerHost = observerProperties.get("host");
        String observerPort = observerProperties.get("port");
        String observerContext = observerProperties.get("context");
        String observerNamespace = observerProperties.get("namespace");
        String obesreverUrl = "http://" + observerHost + ":" + observerPort + "/" + observerContext;
        HashMap<String, String> arguments = new HashMap<>();
        arguments.put("documentId", documentId);
        arguments.put("sessionId", sessionId);
        arguments.put("port", String.valueOf(ports[0]));
        arguments.put("adminPort", String.valueOf(ports[1]));
        String result = SOAPUtil.callMethod(obesreverUrl, observerNamespace, arguments, "startServer");
        System.out.println("result of startServer method: " + result);

        String serverId = saveReviewServerPropertiesInDB(observerHost, ports);
        HashMap<Integer, String> portsStates = new HashMap<>();
        portsStates.put(ports[0], "busy");
        portsStates.put(ports[1], "busy");
        setReviewServerPortState(portsStates);

        HashMap<String, String> reviewServerProperties = new HashMap<>();
        reviewServerProperties.put("host", observerHost);
        reviewServerProperties.put("port", String.valueOf(ports[0]));
        reviewServerProperties.put("adminPort", String.valueOf(ports[1]));
        reviewServerProperties.put("serverId", serverId);
        return reviewServerProperties;
    }

    private String saveReviewServerPropertiesInDB(String host, int[] ports) throws Exception {
        String query = "insert into ISR$REVIEW$SERVER (port, adminPort, host) values (?, ?, ?)";

        // create the mysql insert preparedstatement
        PreparedStatement preparedStmt = connection.prepareStatement(query, Statement.RETURN_GENERATED_KEYS);
        preparedStmt.setInt(1, ports[0]);
        preparedStmt.setInt(2, ports[1]);
        preparedStmt.setString(3, host);

        // execute the preparedstatement
        preparedStmt.execute();
        try (ResultSet generatedKeys = preparedStmt.getGeneratedKeys()) {
            if (generatedKeys.next()) {
                return generatedKeys.getString(1);
            } else {
                throw new SQLException("Creating review failed");
            }
        }
    }

}
