/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.utd.isr.review.db;

import com.utd.isr.utils.exceptions.MessageStackException;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.sql.Blob;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import org.apache.commons.fileupload.FileItem;
import org.apache.commons.fileupload.FileItemFactory;
import org.apache.commons.fileupload.disk.DiskFileItemFactory;
import org.apache.commons.fileupload.servlet.ServletFileUpload;
import org.apache.commons.io.FileUtils;
import org.apache.commons.io.IOUtils;

/**
 *
 * @author schroedera85
 */
public class MainInputPoint extends HttpServlet {

    Connection connection;

    static {
        try {
            Class.forName("com.mysql.jdbc.Driver");
        } catch (ClassNotFoundException ex) {
            Logger.getLogger(MainInputPoint.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    /**
     * Processes requests for both HTTP <code>GET</code> and <code>POST</code>
     * methods.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws Exception {
        String servletPath = request.getRequestURI();
        if (servletPath.contains("document")) {
            sendDocument(request, response);
        } else if (servletPath.contains("getAnnotations")) {
            sendAnnotations(request, response);
        } else if (servletPath.contains("saveAnnotations")) {
            saveAnnotations(request, response);
        } else if (servletPath.contains("createReview")) {
            createReview(request, response);
        } else if (servletPath.contains("startReviewSession")) {
            startReviewSession(request, response);
        } else if (servletPath.contains("endReviewSession")) {
            startReviewSession(request, response);
        }
    }

    private void sendDocument(HttpServletRequest request, HttpServletResponse response) throws Exception {
        String documentId = request.getParameter("documentId");
        String sql = "SELECT content FROM ISR$DOCUMENT where documentId = ?";
        Connection conn = getConnection();
        PreparedStatement stmt = conn.prepareStatement(sql);
        stmt.setString(1, documentId);
        ResultSet resultSet = stmt.executeQuery();

        if (resultSet.next()) {
            Blob blob = resultSet.getBlob(1);
            sendBlob(blob, response);
        } else {
            response.getWriter().print("Document file not found for the id " + documentId);
        }
    }

    private void sendBlob(Blob blob, HttpServletResponse response) throws Exception {
        byte[] buffer = new byte[1024];
        InputStream inputStream = blob.getBinaryStream();
        int fileLength = inputStream.available();
        response.setContentLength(fileLength);
        String mimetype = "application/octet-stream";
        response.setContentType(mimetype);
        // writes the file to the client
        OutputStream outStream = response.getOutputStream();
        int bytesRead = -1;

        while ((bytesRead = inputStream.read(buffer)) != -1) {
            outStream.write(buffer, 0, bytesRead);
        }

        inputStream.close();
        outStream.close();
    }

    private void sendAnnotations(HttpServletRequest request, HttpServletResponse response) throws Exception {
        String documentId = request.getParameter("documentId");
        ReviewManager reviewManager = new ReviewManager();
        reviewManager.setConnection(connection);
        int reviewId = reviewManager.getReviewIdForDocument(documentId);
        String sql = "SELECT content FROM ISR$REVIEW$ANNOTATIONS where reviewId = ?";
        Connection conn = getConnection();
        PreparedStatement stmt = conn.prepareStatement(sql);
        stmt.setInt(1, reviewId);
        ResultSet resultSet = stmt.executeQuery();

        if (resultSet.next()) {
            Blob blob = resultSet.getBlob(1);
            sendBlob(blob, response);
        } else {
            response.getWriter().print("File with old annotations  not found for the id " + documentId);
        }
    }

    private void createReview(HttpServletRequest request, HttpServletResponse response) {
        String ownerId = request.getParameter("ownerId");

        String canUserIdsAsString = request.getParameter("canUserIds");
        String[] canUserIds = canUserIdsAsString.split("\\$");

        String mustUserIdsAsString = request.getParameter("mustUserIds");
        String[] mustUserIds = mustUserIdsAsString.split("\\$");

        String documentId = request.getParameter("documentId");

        String startTime = request.getParameter("startTime");
        String maxEndTime = request.getParameter("maxEndTime");

        ReviewManager reviewManager = new ReviewManager();
        reviewManager.setConnection(getConnection());
        try {
            reviewManager.createReview(documentId, ownerId, canUserIds, mustUserIds, startTime, maxEndTime);
            response.getWriter().write("Review created");
        } catch (Exception ex) {
            try {
                response.getWriter().write(ex.getMessage());
            } catch (IOException ex1) {
                Logger.getLogger(MainInputPoint.class.getName()).log(Level.SEVERE, null, ex1);
            }
        }

    }

    private Connection getConnection() {
        if (connection == null) {
            try {
                connection = DriverManager
                        .getConnection("jdbc:mysql://othala.utddomain.utd:3306/review?zeroDateTimeBehavior=convertToNull", "root", "Donnerschlag1");

            } catch (SQLException e) {
                System.out.println("Connection Failed! Check output console");
                e.printStackTrace();
            }

            if (connection != null) {
                System.out.println("You made it, take control your database now!");
            } else {
                System.out.println("Failed to make connection!");
            }
        }

        return connection;
    }

// <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        try {
            processRequest(request, response);
        } catch (Exception ex) {
            Logger.getLogger(MainInputPoint.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        try {
            processRequest(request, response);
        } catch (Exception ex) {
            Logger.getLogger(MainInputPoint.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

    private void startReviewSession(HttpServletRequest request, HttpServletResponse response) {
        String documentId = request.getParameter("documentId");
        String userId = request.getParameter("userId");
        ReviewSessionManager reviewSessionManager = new ReviewSessionManager();
        reviewSessionManager.setConnection(getConnection());
        try {
            String reviewServerUrl = reviewSessionManager.getURLForReviewSession(documentId, userId);
            response.getWriter().write("<a href=\"" + reviewServerUrl + "\">Zu Review</a>");
        } catch (Exception ex) {
            try {
                if (ex instanceof MessageStackException) {
                    MessageStackException messageStackException = (MessageStackException) ex;
                    String userStack = messageStackException.getUserStack();
                    String impStack = messageStackException.getImplStack();
                    String devStack = messageStackException.getDevStack();
                    response.getWriter().write(userStack + System.lineSeparator() + impStack + System.lineSeparator() + devStack);
                } else {
                    String devStack = org.apache.commons.lang.exception.ExceptionUtils.getFullStackTrace(ex);
                    response.getWriter().write(devStack);
                }
            } catch (IOException ex1) {
                Logger.getLogger(MainInputPoint.class.getName()).log(Level.SEVERE, null, ex1);
            }
        }

    }

    private void saveAnnotations(HttpServletRequest request, HttpServletResponse response) throws Exception {
        String sessionId = request.getParameter("sessionId");
        ReviewSessionManager reviewSessionManager = new ReviewSessionManager();
        reviewSessionManager.setConnection(getConnection());
        String reviewId = reviewSessionManager.getReviewIdBySessionId(sessionId);
        FileItemFactory factory = new DiskFileItemFactory();
        List<FileItem> files = new ServletFileUpload(factory).parseRequest(request);

        InputStream inputStream = files.get(0).getInputStream();
        int lenght = inputStream.available();
        
        String query = "insert into ISR$REVIEW$ANNOTATIONS (reviewId, content) values (?, ?)";

        // create the mysql insert preparedstatement
        PreparedStatement preparedStmt = connection.prepareStatement(query);
        preparedStmt.setInt(1, Integer.parseInt(reviewId));
        preparedStmt.setBinaryStream(2, inputStream, lenght);

        // execute the preparedstatement
        preparedStmt.execute();
    }
}
