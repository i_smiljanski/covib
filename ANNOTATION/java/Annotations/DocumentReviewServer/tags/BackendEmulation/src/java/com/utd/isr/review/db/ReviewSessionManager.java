/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.utd.isr.review.db;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.HashMap;
import java.util.UUID;

/**
 *
 * @author schroedera85
 */
public class ReviewSessionManager {

    private Connection connection;

    public String getURLForReviewSession(String documentId, String userId) throws Exception {
        String serverUrl;
        boolean hasPermission = checkRightForReview(documentId, userId);
        if (!hasPermission) {
            throw new Exception("the user don't have permission for review.");
        }
        String reviewSessionId = getReviewSessionIdForDocument(documentId);
        if (reviewSessionId == null) {
            reviewSessionId = UUID.randomUUID().toString();
            serverUrl = createReviewSession(documentId, reviewSessionId, userId);
        } else {
            boolean hasSessionForDocument = hasSessionForDocument(reviewSessionId, userId);
            if (hasSessionForDocument) {
                throw new Exception("the user already has a session for the document");
            }
            serverUrl = findRunnedServerForDocument(documentId);
        }
        String userName = getUserNameById(userId);
        setSessionForUserInDB(reviewSessionId, userId);
        return serverUrl + "?userName=" + userName;
    }

    public String getReviewSessionIdForDocument(String documentId) throws Exception {
        String sql = "select sessionId from `ISR$REVIEW$SESSION` where `reviewId` ="
                + "(select reviewId from  `ISR$REVIEW` where `documentId` = ?)";
        PreparedStatement stmt = connection.prepareStatement(sql);
        stmt.setString(1, documentId);
        ResultSet resultSet = stmt.executeQuery();
        if (resultSet.next()) {
            String sessionId = resultSet.getString(1);
            return sessionId;
        }

        return null;
    }

    public String getReviewIdBySessionId(String sessionId) throws Exception {
        String sql = "select reviewId from `ISR$REVIEW$SESSION` where `sessionId` = ?";
        PreparedStatement stmt = connection.prepareStatement(sql);
        stmt.setString(1, sessionId);
        ResultSet resultSet = stmt.executeQuery();
        if (resultSet.next()) {
            String reviewId = resultSet.getString(1);
            return reviewId;
        }
        return null;
    }
    
    private String findRunnedServerForDocument(String documentId) throws Exception {
        String sql = "SELECT port, host FROM `ISR$REVIEW$SERVER` WHERE `serverId` ="
                + "(select serverId from `ISR$REVIEW$SESSION` where `reviewId` ="
                + "(select reviewId from  `ISR$REVIEW` where `documentId` = ?))";
        PreparedStatement stmt = connection.prepareStatement(sql);
        stmt.setString(1, documentId);
        ResultSet resultSet = stmt.executeQuery();
        if (resultSet.next()) {
            String host = resultSet.getString("host");
            String port = resultSet.getString("port");
            return "http://" + host + ":" + port;
        }
        return null;
    }

    public void setConnection(Connection connection) {
        this.connection = connection;
    }

    private String createReviewSession(String documentId, String sessionId, String userId) throws Exception {
        ReviewServerManager reviewServerManager = new ReviewServerManager();
        reviewServerManager.setConnection(connection);
        HashMap<String, String> reviewServerProperties = reviewServerManager.createReviewServer(sessionId, documentId);

        int reviewId = getReviewIdForDocument(documentId);
        String query = " insert into ISR$REVIEW$SESSION (sessionId, serverId, reviewId) values (?, ?, ?)";

        // create the mysql insert preparedstatement
        PreparedStatement preparedStmt = connection.prepareStatement(query);
        preparedStmt.setString(1, sessionId);
        String reviewServerId = reviewServerProperties.get("serverId");
        preparedStmt.setInt(2, Integer.parseInt(reviewServerId));
        preparedStmt.setInt(3, reviewId);

        // execute the preparedstatement
        preparedStmt.execute();

        String reviewServerHost = reviewServerProperties.get("host");
        String reviewServerPort = reviewServerProperties.get("port");
        return "http://" + reviewServerHost + ":" + reviewServerPort;
    }

    private int getReviewIdForDocument(String documentId) throws Exception {
        ReviewManager reviewManager = new ReviewManager();
        reviewManager.setConnection(connection);
        return reviewManager.getReviewIdForDocument(documentId);
    }

    private boolean hasSessionForDocument(String reviewSessionId, String userId) throws Exception {
        String sql = "select reviewerId from `ISR$REVIEW$SESSION$REVIEWER` where `sessionId` = ?";
        PreparedStatement stmt = connection.prepareStatement(sql);
        stmt.setString(1, reviewSessionId);
        ResultSet resultSet = stmt.executeQuery();
        return resultSet.next();
    }

    private boolean checkRightForReview(String documentId, String userId) throws Exception {
        ReviewManager reviewManager = new ReviewManager();
        reviewManager.setConnection(connection);
        int reviewId = reviewManager.getReviewIdForDocument(documentId);

        String sql = "select userId from `ISR$REVIEW$REVIEWER` where `reviewId` = ?";
        PreparedStatement stmt = connection.prepareStatement(sql);
        stmt.setInt(1, reviewId);
        ResultSet resultSet = stmt.executeQuery();
        return resultSet.next();
    }

    private String getUserNameById(String userId) throws Exception {
        String sql = "select  firstName, lastName from `ISR$USER` where `userId` = ?";
        PreparedStatement stmt = connection.prepareStatement(sql);
        stmt.setInt(1, Integer.parseInt(userId));
        ResultSet resultSet = stmt.executeQuery();
        if (resultSet.next()) {
            String firstName = resultSet.getString(1);
            String lastName = resultSet.getString(2);
            return firstName + " " + lastName;
        }
        return null;
    }

    private void setSessionForUserInDB(String reviewSessionId, String userId) throws Exception {
        String query = " insert into ISR$REVIEW$SESSION$REVIEWER (sessionId, reviewerId) values (?, ?)";

        // create the mysql insert preparedstatement
        PreparedStatement preparedStmt = connection.prepareStatement(query);
        preparedStmt.setString(1, reviewSessionId);
        preparedStmt.setInt(2, Integer.parseInt(userId));
        // execute the preparedstatement
        preparedStmt.execute();
    }
}
