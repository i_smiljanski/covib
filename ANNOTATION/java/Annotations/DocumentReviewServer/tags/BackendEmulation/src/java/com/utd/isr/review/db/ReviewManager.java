/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.utd.isr.review.db;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.sql.Timestamp;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.HashMap;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author schroedera85
 */
public class ReviewManager {

    private Connection connection;

    public void createReview(
            String documentId,
            String ownerId,
            String[] canUserIds,
            String[] mustUserIds,
            String startTime,
            String maxEndTimeAsString) throws Exception {
        boolean hasRightForReviewCreate = checkOwnerRightsForReviewCreate();
        if (!hasRightForReviewCreate) {
            throw new Exception("User with id " + ownerId + " dont have right for creating a review");
        }
        int reviewIdForDocument = getReviewIdForDocument(documentId);
        if (reviewIdForDocument >= 0) {
            throw new Exception("the document with id " + documentId + " already has a review");
        }
        createReviewInDB(documentId, ownerId, canUserIds, mustUserIds, startTime, maxEndTimeAsString);
    }

    private boolean checkOwnerRightsForReviewCreate() {
        return true;
    }

    public int getReviewIdForDocument(String documentId) throws Exception {
        String sql = "SELECT reviewId FROM ISR$REVIEW where documentId = ?";
        PreparedStatement stmt = connection.prepareStatement(sql);
        stmt.setString(1, documentId);
        ResultSet resultSet = stmt.executeQuery();
        if (resultSet.next()) {
            return resultSet.getInt(1);
        } else {
            return -1;
        }
    }

    private void createReviewInDB(String documentId, String ownerId, String[] canUserIds, String[] mustUserIds, String startTime, String maxEndTimeAsString) throws Exception {
        String reviewId = createReview(documentId, ownerId, startTime, maxEndTimeAsString);
        createReviewers(reviewId, canUserIds, mustUserIds);
    }

    private String createReview(String documentId, String ownerId, String startTime, String maxEndTimeAsString) throws Exception {
        String query = " insert into ISR$REVIEW (documentId, ownerId, createTime, startTime, maxEndTime)"
                + " values (?, ?, ?, ?, ?)";

        // create the mysql insert preparedstatement
        PreparedStatement preparedStmt = connection.prepareStatement(query, Statement.RETURN_GENERATED_KEYS);
        preparedStmt.setInt(1, Integer.parseInt(documentId));
        preparedStmt.setInt(2, Integer.parseInt(ownerId));

        Timestamp createTime = getCurrentTime();
        preparedStmt.setTimestamp(3, createTime);

        Timestamp startTimestamp = getTimestampFromString(startTime);
        preparedStmt.setTimestamp(4, startTimestamp);

        Timestamp maxEndTimestamp = getTimestampFromString(maxEndTimeAsString);
        preparedStmt.setTimestamp(5, maxEndTimestamp);

        // execute the preparedstatement
        preparedStmt.execute();
        try (ResultSet generatedKeys = preparedStmt.getGeneratedKeys()) {
            if (generatedKeys.next()) {
                return generatedKeys.getString(1);
            } else {
                throw new SQLException("Creating review failed");
            }
        }
    }

    private void createReviewers(String reviewId, String[] canUserIds, String[] mustUserIds) throws Exception {
        String query = " insert into ISR$REVIEW$REVIEWER (userId, reviewId, type) values (?, ?, ?)";

        // create the mysql insert preparedstatement
        PreparedStatement preparedStmt = connection.prepareStatement(query);
        for (String canUserId : canUserIds) {
            preparedStmt.setInt(1, Integer.parseInt(canUserId));
            preparedStmt.setInt(2, Integer.parseInt(reviewId));
            preparedStmt.setString(3, "can");
            preparedStmt.addBatch();
        }
        for (String mustUserId : mustUserIds) {
            preparedStmt.setInt(1, Integer.parseInt(mustUserId));
            preparedStmt.setInt(2, Integer.parseInt(reviewId));
            preparedStmt.setString(3, "must");
            preparedStmt.addBatch();
        }
        preparedStmt.executeBatch();
    }

    private Timestamp getCurrentTime() {
        java.util.Date date = new Date();
        Timestamp timestamp = new Timestamp(date.getTime());
        return timestamp;
    }

    private Timestamp getTimestampFromString(String time) {
        try {
            DateFormat dateFormat = new SimpleDateFormat("dd-MM-yy");
            java.util.Date date = dateFormat.parse(time);
            Timestamp timestamp = new Timestamp(date.getTime());
            return timestamp;
        } catch (ParseException ex) {
            Logger.getLogger(ReviewManager.class.getName()).log(Level.SEVERE, null, ex);
        }
        return getCurrentTime();
    }

    public void setConnection(Connection connection) {
        this.connection = connection;
    }

    public HashMap<String, String> getReviewObserverProperties() throws Exception {
        HashMap<String, String> result = new HashMap<>();
        String sql = "SELECT host, port, context, namespace FROM ISR$REVIEW$OBSERVER";
        PreparedStatement stmt = connection.prepareStatement(sql);
        ResultSet resultSet = stmt.executeQuery();
        if (resultSet.next()) {
            String host = resultSet.getString(1);
            String port = resultSet.getString(2);
            String context = resultSet.getString(3);
            String namespace = resultSet.getString(4);
            result.put("host", host);
            result.put("port", port);
            result.put("context", context);
            result.put("namespace", namespace);
            return result;
        } else {
            throw new Exception("there is no data for review observer, please check the data in data base.");
        }
    }
}
