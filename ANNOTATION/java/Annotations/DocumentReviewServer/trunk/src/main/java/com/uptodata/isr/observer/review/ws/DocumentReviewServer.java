package com.uptodata.isr.observer.review.ws;

import com.sun.org.apache.xerces.internal.dom.NodeImpl;
import com.uptodata.isr.observer.template.bootstrap.Bootstrap;
import com.uptodata.isr.observer.template.serverData.ServerData;
import com.uptodata.isr.observer.template.ws.ObserverInterfaceImpl;
import com.utd.isr.utils.exceptions.MessageStackException;
import com.utd.isr.utils.fileSystem.FileSystemWorker;
import com.utd.isr.utils.process.ProcessHelper;
import com.utd.isr.utils.ws.WsUtil;
import org.apache.commons.io.FileUtils;
import org.w3c.dom.Node;

import javax.jws.WebService;
import java.io.*;
import java.net.*;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.sql.*;
import java.util.HashMap;
import java.util.Map;
import java.util.Properties;

/**
 * Created by schroedera85 on 02.12.2014.
 */
@WebService(endpointInterface = "com.uptodata.isr.observer.template.ws.ObserverInterface")
public class DocumentReviewServer extends ObserverInterfaceImpl {
    protected String observerConfigFileName = "groupdocs_server_init_config.txt";
    protected String groupDocsJarName = "groupdocs-annotation-dropwizard-1.8.0.jar";
    protected String rootInFileSystem = "reviews";
    protected String pingServletPath = "ping";
    protected String stopServletPath = "stop";
    protected String configTemplateFileName = "configurationTemplate.yml";
    protected String configFileName = "configuration.yml";
    protected String basePathTemplate = "${basePath}";
    protected String serverPortTemplate = "${serverPort}";
    protected String adminPortTemplate = "${adminPort}";
    protected String applicationPath = "${applicationPath}";
    protected String oldAnnotationsFileName = "oldAnnotations.zip";
    protected String documentName = "document.docx";
    protected String annotationDataFolderName = "temp";
    protected String groupDocsDataBaseFileName = "data.db";

    //properties for data base
    protected String dbPropertiesFileName = "dbToken.rsa";
    protected String dbHost;
    protected String dbWsPort;
    protected String dbWsUser;
    protected String dbWsPassword;

    public DocumentReviewServer() {
        File propertiesFile = new File(observerConfigFileName);
        if (!propertiesFile.exists()) {
            log.warn("Properties file \"" + observerConfigFileName + "\" have not found");
        } else {
            Properties properties = new Properties();
            try {
                properties.load(new FileInputStream(propertiesFile));
                initFromProperties(properties);
            } catch (IOException e) {
                log.error(e.getMessage(), e);
            }
        }
    }

    private void initDataBaseProperties() {
        Bootstrap bootstrap = new Bootstrap();
        Properties connProps = bootstrap.getProperties(dbPropertiesFileName);
        dbHost = connProps.getProperty("dbHost");
        dbWsPort = connProps.getProperty("dbWsPort");
        dbWsUser = connProps.getProperty("dbWsUser");
        dbWsPassword = connProps.getProperty("dbWsPassword");
    }

    private void initFromProperties(Properties properties) {
        rootInFileSystem = properties.getProperty("rootInFileSystem", rootInFileSystem);
        pingServletPath = properties.getProperty("pingServletPath", pingServletPath);
        stopServletPath = properties.getProperty("stopServletPath", stopServletPath);
        configTemplateFileName = properties.getProperty("configTemplateFileName", configTemplateFileName);
        configFileName = properties.getProperty("configFileName", configFileName);
        basePathTemplate = properties.getProperty("basePathTemplate", basePathTemplate);
        serverPortTemplate = properties.getProperty("serverPortTemplate", serverPortTemplate);
        adminPortTemplate = properties.getProperty("adminPortTemplate", adminPortTemplate);
        groupDocsJarName = properties.getProperty("groupDocsJarName", groupDocsJarName);
        applicationPath = properties.getProperty("applicationPath", applicationPath);
        dbPropertiesFileName = properties.getProperty("dbPropertiesFileName", dbPropertiesFileName);
        initDataBaseProperties();
    }

    @Override
    public ServerData tryToStartServer(Map<String, String> serverProperties) throws Exception {
        String sessionId = serverProperties.get("sessionId");
        String documentId = serverProperties.get("documentId");
        String serverPort = serverProperties.get("port");
        String adminPort = serverProperties.get("adminPort");
        boolean hasOldAnnotations = prepareFileSystem(sessionId, documentId);
        if(hasOldAnnotations) {
            setAbsolutPathInGroupDocsDbFile(sessionId);
        }
        boolean isStartet = startReviewServer(sessionId, serverPort, adminPort);
        if (isStartet) {
            String host = ProcessHelper.getLocalHostAddress();
            String serverUrl = getServerUrl(host, serverPort);
            ServerData serverData = createServerData(sessionId, documentId, serverUrl, serverPort);
            boolean isAviable = waitForReviewServer(serverData, 20);
            if (isAviable) {
                return serverData;
            } else {
                throw new Exception("Review Server with properties " + serverProperties + " is not startet");
            }
        } else {
            throw new Exception("Review Server with properties " + serverProperties + " is not startet");
        }
    }

    protected boolean waitForReviewServer(ServerData serverData, double maxWaitTime) {
        int maxCounter = 10;
        int counter = 0;
        int waitTime = (int) (maxWaitTime / maxCounter * 1000);

        boolean isServerAviable = isServerAviable(serverData);
        Object lockObject = new Object();
        while (!isServerAviable && counter < maxCounter) {
            synchronized (lockObject) {
                try {
                    Thread.currentThread().sleep(waitTime);
                    counter++;
                    isServerAviable = isServerAviable(serverData);
                } catch (InterruptedException e) {
                    log.error(e.getMessage(), e);
                }
            }
        }
        return isServerAviable;
    }

    protected String getServerUrl(String host, String port) {
        String serverUrl = "http://" + host + ":" + port;
        return serverUrl;
    }

    private ServerData createServerData(String sessionId, String documentId, String url, String serverPort) {
        AnnotationServerData serverData = new AnnotationServerData(sessionId, url, serverPort);
        serverData.setDocumentId(documentId);
        return serverData;
    }

    protected boolean startReviewServer(String sessionId, String serverPort, String adminPort) {
        String configFilePath = createConfigForReviewServer(sessionId, serverPort, adminPort);
        String[] arguments = new String[]{"server", configFilePath};
        try {
            String documentFolderPath = getDocumentFolderPath(sessionId);
            File logFile = new File(documentFolderPath, getLogFileName(sessionId));
            if (!logFile.exists()) {
                File parentFolder = logFile.getAbsoluteFile().getParentFile();
                if (parentFolder != null) {
                    parentFolder.mkdirs();
                }
                logFile.createNewFile();
            }
            Process process = ProcessHelper.startNewJavaProcess("-jar", groupDocsJarName, arguments, logFile, logFile, logFile);
            return process != null;
        } catch (IOException e) {
            log.error(e.getMessage(), e);
            return false;
        }
    }

    protected String getLogFileName(String serverId) {
        return "ReviewServer_" + serverId + ".log";
    }

    protected String createConfigForReviewServer(String sessionId, String serverPort, String adminPort) {
        try {
            File configTemplate = new File(configTemplateFileName);
            String configTemplateText = FileSystemWorker.getFileContent(configTemplate);
            String documentFolderPath = getDocumentFolderPath(sessionId);
            File documentFolder = new File(documentFolderPath);
            configTemplateText = configTemplateText.replace(basePathTemplate, documentFolder.getAbsolutePath());
            configTemplateText = configTemplateText.replace(serverPortTemplate, serverPort);
            configTemplateText = configTemplateText.replace(adminPortTemplate, adminPort);
            String host = ProcessHelper.getLocalHostAddress();
            String serverUrl = getServerUrl(host, serverPort);
            configTemplateText = configTemplateText.replace(applicationPath, serverUrl);
            String configFilePath = documentFolderPath + File.separator + configFileName;

            Path configPath = Paths.get(configFilePath);
            Files.write(configPath, configTemplateText.getBytes());
            return configPath.toAbsolutePath().toString();
        } catch (IOException e) {
            log.error(e.getMessage(), e);
        }
        return null;
    }

    public String getDocumentFolderPath(String sessionId) {
        String documentFolderPath = rootInFileSystem + File.separator + sessionId;
        return documentFolderPath;
    }

    @Override
    public boolean stopServer(Map<String, String> serverProperties) {
        Node method = createMethodXmlNode("stopServer", serverProperties);
        String serverId = serverProperties.get("serverId");
        ServerData serverData = servers.get(serverId);
        log.warn("try to save annotation from server with ServerData: " + serverData);
        boolean result = false;
        try {
            result = saveAnnotationsInDataBase(serverData);
        } catch (Exception e) {
            log.error(e.getMessage(), e);
            return false;
        }

        String shutdown = serverProperties.get("shutdown");
        if (shutdown.equalsIgnoreCase("true")) {
            log.warn("try to stop server with ServerData: " + serverData);
            result = stopServer(serverData);
            addMethodResultToStatusXml(result, method, serverId);
            deleteDocumentFolder(serverData);
        }
        return result;
    }

    protected boolean saveAnnotationsInDataBase(ServerData serverData) throws Exception {
        String serverId = serverData.getServerId();
        String documentFolder = getDocumentFolderPath(serverId);
        File zip = new File(documentFolder, serverId + ".zip");
        File annotationDataFolder = new File(documentFolder, annotationDataFolderName);

        File dataBaseFile = new File(annotationDataFolder, groupDocsDataBaseFileName);
        File reviewsFolder = new File(rootInFileSystem);

        String reviewsFolderPath = reviewsFolder.getAbsoluteFile().getAbsolutePath().replaceAll("\\\\", "/");;
        setRelativPathInGroupDocsDbFile(dataBaseFile, reviewsFolderPath);

        FileSystemWorker.packFolderToZip(zip, annotationDataFolder);
        sendAnnotationDataToDataBase(serverId, zip);
        zip.delete();
        return true;
    }

    protected void sendAnnotationDataToDataBase(String sessionId, File zip) throws Exception {
        Authenticator.setDefault(new Authenticator() {
            protected PasswordAuthentication getPasswordAuthentication() {
                return new PasswordAuthentication(dbWsUser, dbWsPassword.toCharArray());
            }
        });

        String attachmentName = "bitmap";
        String attachmentFileName = "bitmap.bmp";
        String crlf = "\r\n";
        String twoHyphens = "--";
        String boundary = "*****";

        String urlString = getUrlForAnnotationsUpload(sessionId);
        URL url = new URL(urlString);
        HttpURLConnection httpUrlConnection = (HttpURLConnection) url.openConnection();
        httpUrlConnection.setUseCaches(false);
        httpUrlConnection.setDoOutput(true);

        httpUrlConnection.setRequestMethod("POST");
        httpUrlConnection.setRequestProperty("Connection", "Keep-Alive");
        httpUrlConnection.setRequestProperty("Cache-Control", "no-cache");
        httpUrlConnection.setRequestProperty("Content-Type", "multipart/form-data;boundary=" + boundary);
        httpUrlConnection.connect();
        DataOutputStream request = new DataOutputStream(httpUrlConnection.getOutputStream());

        request.writeBytes(twoHyphens + boundary + crlf);
        request.writeBytes("Content-Disposition: form-data; name=\"" + attachmentName + "\";filename=\"" + attachmentFileName + "\"" + crlf);
        request.writeBytes(crlf);
        InputStream inputStream = new FileInputStream(zip);
        int bytesRead = -1;
        byte[] buffer = new byte[1024];
        while ((bytesRead = inputStream.read(buffer)) > 0) {
            request.write(buffer, 0, bytesRead);
        }

        request.writeBytes(crlf);
        request.writeBytes(twoHyphens + boundary + twoHyphens + crlf);
        request.flush();
        request.close();
        httpUrlConnection.getResponseCode();
        httpUrlConnection.disconnect();
        inputStream.close();
    }

    protected void deleteDocumentFolder(ServerData serverData) {
        String serverId = serverData.getServerId();
        String folder = getDocumentFolderPath(serverId);

        try {
            FileUtils.deleteDirectory(new File(folder));
        } catch (IOException e) {
            try {
                FileUtils.deleteDirectory(new File(folder));// wir versuchen  ncoh mal den ordner zu löschen
            } catch (IOException e2) {
                log.error(e2.getMessage(), e2);
            }
        }
    }

    @Override
    public Node createMethodNode(Map<String, String> serverProperties) {
        String documentId = serverProperties.get("documentId");
        String sessionId = serverProperties.get("sessionId");
        String port = serverProperties.get("port");
        log.info("try to start document review server: port " + port + ", sessionId " + sessionId + ", documentId " + documentId);

        Map<String, String> params = new HashMap<String, String>();
        params.put("port", port);
        params.put("sessionId", sessionId);
        params.put("documentId", documentId);
        Node methodNode = createMethodXmlNode("startServer", params);
        return methodNode;
    }

    @Override
    protected boolean isServerAviable(ServerData serverData) {
        String serverUrl = serverData.getUrl();
        String pingServletUrl = serverUrl + "/" + pingServletPath;
        boolean isAvailable = WsUtil.isServerAvailable(pingServletUrl);
        String resultAsString = "available";
        if (!isAvailable) {
            resultAsString = "unavailable";
        }
        log.debug("Ping ReviewServer  " + serverData + ", result " + resultAsString);
        return isAvailable;
    }

    @Override
    public boolean stopServer(ServerData serverData) {
        boolean isServerAviable = isServerAviable(serverData);

        if (!isServerAviable) {
            log.warn("Server with data " + serverData + " had  tried to stop, but the server is already down");
            return false;
        }

        int sendTryCounter = 0;
        while (isServerAviable && sendTryCounter < 3) {
            try {
                sendStopRequest(serverData);
                Thread.currentThread().sleep(3000);
                isServerAviable = isServerAviable(serverData);

            } catch (InterruptedException e) {
                log.error(e.getMessage(), e);
            }
            sendTryCounter++;
        }
        return !isServerAviable;
    }

    protected boolean sendStopRequest(ServerData serverData) {
        String serverUrl = serverData.getUrl();
        String stopServletUrl = serverUrl + "/" + stopServletPath;
        try {
            log.debug("Send stop request to url " + stopServletUrl);
            URL url = new URL(stopServletUrl);
            HttpURLConnection con = (HttpURLConnection) url.openConnection();

            // optional default is GET
            con.setRequestMethod("GET");
            int responseCode = con.getResponseCode();
            return true;

        } catch (MalformedURLException e) {
            return true;
        } catch (IOException e) {
            return true;
        }
    }

    protected boolean prepareFileSystem(String sessionId, String documentId) throws Exception {
        File reviewFolder = FileSystemWorker.createFolder(rootInFileSystem, sessionId);
        downloadWordDocumentToFolder(reviewFolder, documentId);
        File oldAnnotations = downloadOldAnnotationsToFolder(reviewFolder, documentId);
        if (oldAnnotations != null) {
            File annotationsFolder = FileSystemWorker.createFolder(reviewFolder.getAbsolutePath(), annotationDataFolderName);
            FileSystemWorker.unpackZipToFolder(oldAnnotations, annotationsFolder);
            oldAnnotations.delete();
            return true;
        } else {
            return false;
        }
    }

    private File downloadOldAnnotationsToFolder(File reviewFolder, String documentId) throws Exception {
        String urlString = getUrlForDocumentAnnotationsDownload(documentId);
        File oldAnnotationsFile = new File(reviewFolder, oldAnnotationsFileName);
        downloadFile(urlString, oldAnnotationsFile);
        if (FileSystemWorker.isZipFile(oldAnnotationsFile)) {
            return oldAnnotationsFile;
        } else {
            oldAnnotationsFile.delete();
            return null;
        }
    }

    private void setAbsolutPathInGroupDocsDbFile(String sessionId) throws Exception {
        File dataBaseFile = getGroupDocsDataBaseFile(sessionId);
        File reviewsFolder = new File(rootInFileSystem);
        String reviewsFolderPath = reviewsFolder.getAbsoluteFile().getAbsolutePath().replaceAll("\\\\", "/") + "/";
        setAbsolutPathInGroupDocsDbFile(dataBaseFile, reviewsFolderPath);
    }

    private File getGroupDocsDataBaseFile(String sessionId) {
        String documentFolder = getDocumentFolderPath(sessionId);
        File annotationDataFolder = new File(documentFolder, annotationDataFolderName);
        File dataBaseFile = new File(annotationDataFolder, groupDocsDataBaseFileName);
        return dataBaseFile;
    }

    private ResultSet getDataFromGroupDocsDataBase(File dataBaseFile) throws Exception {
        initSqlLiteDriver();
        Connection connection = DriverManager.getConnection("jdbc:sqlite:" + dataBaseFile.getAbsolutePath());
        String sql = "SELECT id, url FROM Links";
        PreparedStatement stmt = connection.prepareStatement(sql);
        ResultSet resultSet = stmt.executeQuery();
        return resultSet;
    }


    private void setRelativPathInGroupDocsDbFile(File dataBaseFile, String pattern)throws Exception {
        ResultSet resultSet = getDataFromGroupDocsDataBase(dataBaseFile);
        String insertSql = "update  Links set url = ? where id = ?";
        Connection connection = DriverManager.getConnection("jdbc:sqlite:" + dataBaseFile.getAbsolutePath());
        PreparedStatement insertStatement = connection.prepareStatement(insertSql);

        while (resultSet.next()) {
            int id = resultSet.getInt(1);
            String filePath = resultSet.getString(2);
            int endIndexOfPattern = filePath.indexOf(pattern) + pattern.length() + 1;
            String lastTailOfPath = filePath.substring(endIndexOfPattern);
            insertStatement.setString(1, lastTailOfPath);
            insertStatement.setInt(2, id);
            insertStatement.addBatch();
        }
        insertStatement.executeBatch();
    }

    private void setAbsolutPathInGroupDocsDbFile(File dataBaseFile, String prefix) throws Exception {
        ResultSet resultSet = getDataFromGroupDocsDataBase(dataBaseFile);
        String insertSql = "update  Links set url = ? where id = ?";
        Connection connection = DriverManager.getConnection("jdbc:sqlite:" + dataBaseFile.getAbsolutePath());
        PreparedStatement insertStatement = connection.prepareStatement(insertSql);

        while (resultSet.next()) {
            int id = resultSet.getInt(1);
            String filePath = resultSet.getString(2);
            String absolutPath = prefix + filePath;
            insertStatement.setString(1, absolutPath);
            insertStatement.setInt(2, id);
            insertStatement.addBatch();
        }
        insertStatement.executeBatch();
    }

    private void initSqlLiteDriver() throws ClassNotFoundException {
            Class.forName("org.sqlite.JDBC");
    }

    private String getUrlForDocumentAnnotationsDownload(String documentId) {
        return "http://localhost:8084/ReviewBackend/review/getAnnotations?documentId=" + documentId;
    }

    private void downloadWordDocumentToFolder(File reviewFolder, String documentId) throws MessageStackException {
        String urlString = getUrlForDocumentDownload(documentId);
        File documentFile = new File(reviewFolder, documentName);
        downloadFile(urlString, documentFile);
    }

    private String getUrlForDocumentDownload(String documentId) {
        return "http://localhost:8084/ReviewBackend/review/document?documentId=" + documentId;
    }

    private String getUrlForAnnotationsUpload(String reviewSessionId) {
        return "http://localhost:8084/ReviewBackend/review/saveAnnotations?sessionId=" + reviewSessionId;
    }

    protected boolean downloadFile(String urlString, File file) throws MessageStackException {
        Authenticator.setDefault(new Authenticator() {
            protected PasswordAuthentication getPasswordAuthentication() {
                return new PasswordAuthentication(dbWsUser, dbWsPassword.toCharArray());
            }
        });
        try {
            URL url = new URL(urlString);
            FileUtils.copyURLToFile(url, file);
            return true;
        } catch (IOException e) {
            log.error(e);
            String userMessage = "can not download file from " + urlString;
            String implStack = "error in DocumentReviewServer:downloadFile() with parameters: urlString = " + urlString + ","
                    + System.lineSeparator() + "file = " + file.getAbsolutePath();
            String devStack = org.apache.commons.lang.exception.ExceptionUtils.getFullStackTrace(e);
            throw new MessageStackException(MessageStackException.SEVERITY_CRITICAL, userMessage, implStack, devStack);
        }
    }
}
