package com.uptodata.isr.observer.review.ws;

import com.uptodata.isr.observer.childServer.ObserverChildServerInterface;
import com.uptodata.isr.observer.template.main.Main;

import javax.jws.WebService;
import javax.jws.soap.SOAPBinding;
import java.io.IOException;

/**
 * Created by schroedera85 on 19.12.2014.
 */
@WebService(name = "ObserverInterface", targetNamespace = "http://ws.observer.review.isr.uptodata.com/")
@SOAPBinding(style = SOAPBinding.Style.DOCUMENT, use = SOAPBinding.Use.LITERAL)
public class ReviewObserverControl implements ObserverChildServerInterface {
    @Override
    public boolean pingMe() {
        return true;
    }

    public ReviewObserverControl() throws IOException {
        String[] args = new String[]{"reviewObserverConfig.txt"};
        new Main().start(args);
    }
}
