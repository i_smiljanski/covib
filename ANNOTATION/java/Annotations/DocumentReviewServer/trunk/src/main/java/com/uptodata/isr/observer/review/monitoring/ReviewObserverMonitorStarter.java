package com.uptodata.isr.observer.review.monitoring;

import com.uptodata.isr.observer.template.monitoring.ObserverWebserviceMonitor;

/**
 * Created by schroedera85 on 22.12.2014.
 */
public class ReviewObserverMonitorStarter {
    public static void main(String[] args) {
        new ObserverWebserviceMonitor().start(args);
    }
}
