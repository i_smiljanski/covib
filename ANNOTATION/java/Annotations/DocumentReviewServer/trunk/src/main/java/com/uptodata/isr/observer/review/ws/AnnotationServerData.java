package com.uptodata.isr.observer.review.ws;

import com.uptodata.isr.observer.template.serverData.ServerData;

/**
 * Created by schroedera85 on 15.12.2014.
 */
public class AnnotationServerData extends ServerData {
    protected String documentId;
    public AnnotationServerData(String serverId, String url, String namespace, String port) {
        super(serverId, url, namespace, port);
    }

    public AnnotationServerData(String serverId, String url, String port) {
        this.serverId = serverId;
        this.url = url;
        this.port = port;
    }

    public AnnotationServerData() {

    }

    public String getDocumentId() {
        return documentId;
    }

    public void setDocumentId(String documentId) {
        this.documentId = documentId;
    }
}
