package com.groupdocs.annotation.dropwizard.resources;

import com.groupdocs.annotation.common.Utils;
import com.groupdocs.annotation.domain.response.StatusResult;
import com.groupdocs.annotation.dropwizard.config.ApplicationConfig;
import com.groupdocs.annotation.dropwizard.localization.LocalizationGE;
import com.groupdocs.annotation.dropwizard.views.AnnotationView;
import com.groupdocs.annotation.exception.AnnotationException;
import com.groupdocs.annotation.handler.AnnotationHandler;
import com.groupdocs.annotation.handler.IGroupDocsAnnotation;
import com.groupdocs.annotation.localization.ILocalization;
import com.groupdocs.annotation.localization.LocalizationRU;
import com.groupdocs.viewer.domain.path.EncodedPath;
import com.groupdocs.viewer.domain.path.GroupDocsPath;
import com.groupdocs.viewer.domain.path.TokenId;
import com.sun.jersey.spi.resource.Singleton;
import org.atmosphere.cpr.AtmosphereResource;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.ws.rs.*;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import static com.groupdocs.annotation.common.Utils.toJson;
import java.io.IOException;

/**
 * @author Alex Bobkov
 */

@Path("/")
@Singleton
public class AnnotationResource implements IGroupDocsAnnotation {
    public static final String SCRIPT_LITERAL = "script";
    public static final String PATH_LITERAL = "path";
    public static final String NAME_LITERAL = "name";
    public static final String GET_PDF_LITERAL = "getPdf";
    public static final String CALLBACK_LITERAL = "callback";
    public static final String DATA_LITERAL = "data";

    protected static AnnotationResource instance;
    @Context
    protected static AnnotationHandler annotationHandler;
    @Context
    protected ApplicationConfig applicationConfig;

    /**
     * Constructs a new AnnotationResource.
     */
    public AnnotationResource() {
        instance = this;
    }

    /**
     * Getter for property 'instance'.
     *
     * @return Value for property 'instance'.
     */
    public static AnnotationResource getInstance() {
        return instance;
    }

    /**
     * Home page request
     *
     * @param request
     * @param file
     * @param userName user name
     * @param tokenId
     * @return rendered page
     */
    @GET
    public AnnotationView getAnnotation(@Context HttpServletRequest request, @QueryParam("file") String file, @QueryParam("tokenId") String tokenId, @QueryParam("userName") String userName) {
        try {
            if (file == null && tokenId == null) {
                file = "document.docx";
            }
            GroupDocsPath path = null;
            if (file != null && !file.isEmpty()) {
                path = new EncodedPath(file, annotationHandler.getConfiguration());
            } else if (tokenId != null && !tokenId.isEmpty()) {
                TokenId tki = new TokenId(tokenId, applicationConfig.getEncryptionKey());
                if (!tki.isExpired()) {
                    path = tki;
                }
            }
            final String initialPath = (path == null ? "" : path.getPath());
            final String usrName = (userName == null ? AnnotationHandler.ANONYMOUS_USERNAME : userName);
            final String userGuid = annotationHandler.getUserGuid(usrName);

//            if (annotationHandler.getUserAvatar(userGuid) == null) {
//                FileInputStream testAvatar = new FileInputStream(new File("E:\\Images\\333.jpeg"));
//                byte[] bytes = new byte[testAvatar.available()];
//                IOUtils.readFully(testAvatar, bytes);
//                Utils.closeStreams(testAvatar);
//                annotationHandler.setUserAvatar(userGuid, bytes);
//            }

            // Configure localization
            ILocalization localization = null;
            if ("RU".equalsIgnoreCase(applicationConfig.getLocalization())) {
                localization = new LocalizationRU();
            } else if ("GE".equalsIgnoreCase(applicationConfig.getLocalization())) {
                localization = new LocalizationGE();
            }

            return new AnnotationView(
                    annotationHandler.getHeader(applicationConfig.getApplicationPath(), request),
                    annotationHandler.getAnnotationScript(initialPath, userName, userGuid, localization)
            );
        } catch (AnnotationException e) {
            Utils.log(AnnotationResource.class, e);
        }
        return null;
    }

    /**
     * Get list of annotations for document [POST request]
     *
     * @param request HTTP servlet request
     * @return response object
     */
    @POST
    @Path(value = LIST_ANNOTATIONS_HANDLER)
    @Produces(MediaType.APPLICATION_JSON)
    @Override
    public Object listAnnotationsHandler(@Context HttpServletRequest request, @Context HttpServletResponse response) {
        try {
            return annotationHandler.listAnnotationsHandler(request, response);
        } catch (AnnotationException e) {
            Utils.log(AnnotationResource.class, e);
            return toJson(new StatusResult(false, e.getMessage()));
        }
    }

    /**
     * Download document with annotations [POST request]
     *
     * @param request HTTP servlet request
     * @return response object
     */
    @POST
    @Path(value = EXPORT_ANNOTATIONS_HANDLER)
    @Produces(MediaType.APPLICATION_JSON)
    @Override
    public Object exportAnnotationsHandler(@Context HttpServletRequest request, @Context HttpServletResponse response) {
        try {
            return annotationHandler.exportAnnotationsHandler(request, response);
        } catch (AnnotationException e) {
            Utils.log(AnnotationResource.class, e);
            return toJson(new StatusResult(false, e.getMessage()));
        }
    }

    /**
     * Download document as PDF file [POST request]
     *
     * @param request HTTP servlet request
     * @return response object
     */
    @POST
    @Path(value = GET_PDF_VERSION_OF_DOCUMENT_HANDLER)
    @Produces(MediaType.APPLICATION_JSON)
    @Override
    public Object getPdfVersionOfDocumentHandler(@Context HttpServletRequest request, @Context HttpServletResponse response) {
        try {
            return annotationHandler.getPdfVersionOfDocumentHandler(request, response);
        } catch (AnnotationException e) {
            Utils.log(AnnotationResource.class, e);
            return toJson(new StatusResult(false, e.getMessage()));
        }
    }

    /**
     * Request to create annotation on document [POST request]
     *
     * @param request HTTP servlet request
     * @return response object
     */
    @POST
    @Path(value = CREATE_ANNOTATION_HANDLER)
    @Produces(MediaType.APPLICATION_JSON)
    @Override
    public Object createAnnotationHandler(@Context HttpServletRequest request, @Context HttpServletResponse response) {
        try {
            return annotationHandler.createAnnotationHandler(request, response);
        } catch (AnnotationException e) {
            Utils.log(AnnotationResource.class, e);
            return toJson(new StatusResult(false, e.getMessage()));
        }
    }

    /**
     * Get avatar for current user [GET request]
     *
     * @param request  HTTP servlet request
     * @param response HTTP servlet response
     * @param userId   user id
     * @return response object
     */
    @GET
    @Path(value = GET_AVATAR_HANDLER)
    @Override
    public Object getAvatarHandler(@Context HttpServletRequest request, @Context HttpServletResponse response, @QueryParam("userId") String userId) {
        try {
            return annotationHandler.getAvatarHandler(request, response, userId);
        } catch (AnnotationException e) {
            Utils.log(AnnotationResource.class, e);
            return toJson(new StatusResult(false, e.getMessage()));
        }
    }

    /**
     * Add reply to annotation [POST request]
     *
     * @param request HTTP servlet request
     * @return response object
     */
    @POST
    @Path(value = ADD_ANNOTATION_REPLY_HANDLER)
    @Produces(MediaType.APPLICATION_JSON)
    @Override
    public Object addAnnotationReplyHandler(@Context HttpServletRequest request, @Context HttpServletResponse response) {
        try {
            return annotationHandler.addAnnotationReplyHandler(request, response);
        } catch (AnnotationException e) {
            Utils.log(AnnotationResource.class, e);
            return toJson(new StatusResult(false, e.getMessage()));
        }
    }

    /**
     * Edit reply for annotation [POST request]
     *
     * @param request HTTP servlet request
     * @return response object
     */
    @POST
    @Path(value = EDIT_ANNOTATION_REPLY_HANDLER)
    @Override
    public Object editAnnotationReplyHandler(@Context HttpServletRequest request, @Context HttpServletResponse response) {
        try {
            return annotationHandler.editAnnotationReplyHandler(request, response);
        } catch (AnnotationException e) {
            Utils.log(AnnotationResource.class, e);
            return toJson(new StatusResult(false, e.getMessage()));
        }
    }

    /**
     * Delete reply from annotation [POST request]
     *
     * @param request HTTP servlet request
     * @return response object
     */
    @POST
    @Path(value = DELETE_ANNOTATION_REPLY_HANDLER)
    @Override
    public Object deleteAnnotationReplyHandler(@Context HttpServletRequest request, @Context HttpServletResponse response) {
        try {
            return annotationHandler.deleteAnnotationReplyHandler(request, response);
        } catch (AnnotationException e) {
            Utils.log(AnnotationResource.class, e);
            return toJson(new StatusResult(false, e.getMessage()));
        }
    }

    /**
     * Delete annotation [POST request]
     *
     * @param request HTTP servlet request
     * @return response object
     */
    @POST
    @Path(value = DELETE_ANNOTATION_HANDLER)
    @Override
    public Object deleteAnnotationHandler(@Context HttpServletRequest request, @Context HttpServletResponse response) {
        try {
            return annotationHandler.deleteAnnotationHandler(request, response);
        } catch (AnnotationException e) {
            Utils.log(AnnotationResource.class, e);
            return toJson(new StatusResult(false, e.getMessage()));
        }
    }

    /**
     * Save text field annotation [POST request]
     *
     * @param request HTTP servlet request
     * @return response object
     */
    @POST
    @Path(value = SAVE_TEXT_FIELD_HANDLER)
    @Override
    public Object saveTextFieldHandler(@Context HttpServletRequest request, @Context HttpServletResponse response) {
        try {
            return annotationHandler.saveTextFieldHandler(request, response);
        } catch (AnnotationException e) {
            Utils.log(AnnotationResource.class, e);
            return toJson(new StatusResult(false, e.getMessage()));
        }
    }

    /**
     * Set color for text field annotation [POST request]
     *
     * @param request HTTP servlet request
     * @return response object
     */
    @POST
    @Path(value = SET_TEXT_FIELD_COLOR_HANDLER)
    @Override
    public Object setTextFieldColorHandler(@Context HttpServletRequest request, @Context HttpServletResponse response) {
        try {
            return annotationHandler.setTextFieldColorHandler(request, response);
        } catch (AnnotationException e) {
            Utils.log(AnnotationResource.class, e);
            return toJson(new StatusResult(false, e.getMessage()));
        }
    }

    /**
     * Set annotation marker position [POST request]
     *
     * @param request HTTP servlet request
     * @return response object
     */
    @POST
    @Path(value = MOVE_ANNOTATION_MARKER_HANDLER)
    @Override
    public Object moveAnnotationMarkerHandler(@Context HttpServletRequest request, @Context HttpServletResponse response) {
        try {
            return annotationHandler.moveAnnotationMarkerHandler(request, response);
        } catch (AnnotationException e) {
            Utils.log(AnnotationResource.class, e);
            return toJson(new StatusResult(false, e.getMessage()));
        }
    }

    /**
     * Set new size for annotation [POST request]
     *
     * @param request HTTP servlet request
     * @return response object
     */
    @POST
    @Path(value = RESIZE_ANNOTATION_HANDLER)
    @Override
    public Object resizeAnnotationHandler(@Context HttpServletRequest request, @Context HttpServletResponse response) {
        try {
            return annotationHandler.resizeAnnotationHandler(request, response);
        } catch (AnnotationException e) {
            Utils.log(AnnotationResource.class, e);
            return toJson(new StatusResult(false, e.getMessage()));
        }
    }

    /**
     * Return list of collaborators [POST request]
     *
     * @param request HTTP servlet request
     * @return object with response parameters
     */
    @POST
    @Path(value = GET_DOCUMENT_COLLABORATORS_HANDLER)
    @Produces(MediaType.APPLICATION_JSON)
    @Override
    public Object getDocumentCollaboratorsHandler(@Context HttpServletRequest request, @Context HttpServletResponse response) {
        try {
            return annotationHandler.getDocumentCollaboratorsHandler(request, response);
        } catch (AnnotationException e) {
            Utils.log(AnnotationResource.class, e);
            return toJson(new StatusResult(false, e.getMessage()));
        }
    }

    /**
     * Upload file to GroupDocs.Annotation [POST request]
     * @param userId
     * @param fld
     * @param fileName
     * @param multiple
     * @param request http request
     * @param response http response
     * @return  token id as json
     */
    @POST
    @Produces(MediaType.APPLICATION_JSON)
    @Path(value = UPLOAD_FILE_HANDLER)
    public Object uploadFileHandler(@QueryParam("userId") String userId, @QueryParam("fld") String fld, @QueryParam("fileName") String fileName, @QueryParam("multiple") boolean multiple, @Context HttpServletRequest request, @Context HttpServletResponse response){
        try {
            if(multiple){
                return annotationHandler.uploadFileHandler(fileName, request.getInputStream());
            }else{
                return null;
            }
        } catch (Exception e) {
            Utils.log(AnnotationResource.class, e);
            return toJson(new StatusResult(false, e.getMessage()));
        }
    }

    /**
     * @param request  http servlet request
     * @param response http servlet response
     * @return object with response parameters
     * @see com.groupdocs.annotation.handler.IGroupDocsAnnotation
     */
    @POST
    @Produces(MediaType.APPLICATION_JSON)
    @Path(value = IMPORT_ANNOTATIONS_HANDLER)
    @Override
    public Object importAnnotationsHandler(@Context HttpServletRequest request, @Context HttpServletResponse response) {
        try {
            return annotationHandler.importAnnotationsHandler(request, response);
        } catch (AnnotationException e) {
            Utils.log(AnnotationResource.class, e);
            return toJson(new StatusResult(false, e.getMessage()));
        }
    }

    /**
     * Get view for print document
     *
     * @param request  HTTP servlet request
     * @param response HTTP servlet response
     * @return response object
     */
    @POST
    @Path(value = GET_PRINT_VIEW_HANDLER)
    @Produces(MediaType.APPLICATION_JSON)
    @Override
    public Object getPrintViewHandler(@Context HttpServletRequest request, @Context HttpServletResponse response) {
        try {
            return annotationHandler.getPrintViewHandler(request, response);
        } catch (AnnotationException e) {
            Utils.log(AnnotationResource.class, e);
            return toJson(new StatusResult(false, e.getMessage()));
        }
    }

    /**
     * Get JavaScript file [GET request]
     *
     * @param script   JavaScript name
     * @param request  HTTP servlet request
     * @param response http servlet response
     * @return JavaScript file content
     */
    @GET
    @Path(value = GET_JS_HANDLER)
    public Object getJsHandler(@QueryParam(SCRIPT_LITERAL) String script, @Context HttpServletRequest request, @Context HttpServletResponse response) {
        long dateSince = request.getDateHeader("If-Modified-Since");
        if (annotationHandler.isResourceModified(dateSince)) {
            response.setDateHeader("Last-Modified", AnnotationHandler.LAST_RESOURCE_MODIFIED);
            return annotationHandler.getJsHandler(script, response);
        } else {
            return Response.status(Response.Status.NOT_MODIFIED).build();
        }
    }


    /**
     * Get JavaScript file [GET request]
     *
     * @param scriptName JavaScript name
     * @param response   http servlet response
     * @return JavaScript file content
     */
    @Override
    @Deprecated
    public Object getJsHandler(@QueryParam(SCRIPT_LITERAL) String scriptName, @Context HttpServletResponse response) {
        return annotationHandler.getJsHandler(scriptName, response);
    }

    /**
     * Get css file [GET request]
     *
     * @param script   CSS name
     * @param request  HTTP servlet request
     * @param response http servlet response
     * @return CSS file content
     */
    @GET
    @Path(value = GET_CSS_HANDLER)
    public Object getCssHandler(@QueryParam(SCRIPT_LITERAL) String script, @Context HttpServletRequest request, @Context HttpServletResponse response) {
        long dateSince = request.getDateHeader("If-Modified-Since");
        if (annotationHandler.isResourceModified(dateSince)) {
            response.setDateHeader("Last-Modified", AnnotationHandler.LAST_RESOURCE_MODIFIED);
            return annotationHandler.getCssHandler(script, response);
        } else {
            return Response.status(Response.Status.NOT_MODIFIED).build();
        }
    }

    /**
     * Get css file [GET request]
     *
     * @param script   CSS name
     * @param response http servlet response
     * @return CSS file content
     */
    @Override
    @Deprecated
    public Object getCssHandler(@QueryParam(SCRIPT_LITERAL) String script, @Context HttpServletResponse response) {
        return annotationHandler.getCssHandler(script, response);
    }

    /**
     * Get image file [GET request]
     *
     * @param name     image name
     * @param request
     * @param response http servlet response
     * @return image content
     */
    @GET
    @Path(value = GET_IMAGE_HANDLER)
    public Object getImageHandler(@PathParam(NAME_LITERAL) String name, @Context HttpServletRequest request, @Context HttpServletResponse response) {
        long dateSince = request.getDateHeader("If-Modified-Since");
        if (annotationHandler.isResourceModified(dateSince)) {
            return annotationHandler.getImageHandler(name, response);
        } else {
            return Response.status(Response.Status.NOT_MODIFIED).build();
        }
    }

    /**
     * Get image file [GET request]
     *
     * @param name     image name
     * @param response http servlet response
     * @return image content
     */
    @Override
    @Deprecated
    public Object getImageHandler(@PathParam(NAME_LITERAL) String name, @Context HttpServletResponse response) {
        return annotationHandler.getImageHandler(name, response);
    }

    /**
     * Get font file
     *
     * @param name     font name
     * @param request
     * @param response http servlet response
     * @return font content
     */
    @GET
    @Path(value = GET_FONT_HANDLER)
    @Produces(MediaType.APPLICATION_OCTET_STREAM)
    public Object getFontHandler(@PathParam(NAME_LITERAL) String name, @Context HttpServletRequest request, @Context HttpServletResponse response) {
        long dateSince = request.getDateHeader("If-Modified-Since");
        if (annotationHandler.isResourceModified(dateSince)) {
            return annotationHandler.getFontHandler(name, response);
        } else {
            return Response.status(Response.Status.NOT_MODIFIED).build();
        }
    }

    /**
     * Get font file
     *
     * @param name     font name
     * @param response http servlet response
     * @return font content
     */
    @Override
    @Deprecated
    public Object getFontHandler(@PathParam(NAME_LITERAL) String name, @Context HttpServletResponse response) {
        return annotationHandler.getFontHandler(name, response);
    }

    /**
     * Get HTML resources
     *
     * @param filePath resource path
     * @param response http servlet response
     * @return return HTML resources
     */
    @GET
    @Override
    @Path(value = GET_HTML_RESOURCES_HANDLER)
    public Object getHtmlResourcesHandler(@PathParam("filePath") String filePath, @Context HttpServletResponse response) {
        return annotationHandler.getHtmlResourcesHandler(filePath, response);
    }

    /**
     * Download file [GET request]
     *
     * @param path     file path
     * @param getPdf   get pdf file
     * @param response http servlet response
     * @return file content
     */
    @GET
    @Override
    @Path(value = GET_FILE_HANDLER)
    @Produces(MediaType.APPLICATION_OCTET_STREAM)
    public Object getFileHandler(@QueryParam(PATH_LITERAL) String path, @QueryParam(GET_PDF_LITERAL) boolean getPdf, @Context HttpServletResponse response) {
        return annotationHandler.getFileHandler(path, getPdf, response);
    }

    /**
     * Get document image files [GET request]
     *
     * @param guid      file guid
     * @param width     file width
     * @param quality   quality
     * @param usePdf    use PDF format
     * @param pageIndex index of page
     * @param response  http servlet response
     * @return null
     */
    @GET
    @Override
    @Path(value = GET_DOCUMENT_PAGE_IMAGE_HANDLER)
    public Object getDocumentPageImageHandler(@QueryParam(PATH_LITERAL) String guid, @QueryParam("width") Integer width, @QueryParam("quality") Integer quality, @QueryParam("usePdf") Boolean usePdf, @QueryParam("pageIndex") Integer pageIndex, @Context HttpServletResponse response) {
        return annotationHandler.getDocumentPageImageHandler(guid, width, quality, usePdf, pageIndex, response);
    }

    /**
     * Generate list of images/pages [POST request]
     *
     * @param request  HTTP servlet request
     * @param response HTTP servlet response
     * @return response object
     */
    @POST
    @Path(value = VIEW_DOCUMENT_HANDLER)
    @Produces(MediaType.APPLICATION_JSON)
    @Override
    public Object viewDocumentHandler(@Context HttpServletRequest request, @Context HttpServletResponse response) {
        return annotationHandler.viewDocumentHandler(request, response);
    }

    /**
     * Generate list of images/pages [GET request]
     *
     * @param callback callback
     * @param data     request data
     * @param request  HTTP servlet request
     * @param response HTTP servlet response
     * @return response object
     */
    @GET
    @Path(value = VIEW_DOCUMENT_HANDLER)
    @Produces(MediaType.APPLICATION_JSON)
    @Override
    public Object viewDocumentHandler(@QueryParam(CALLBACK_LITERAL) String callback, @QueryParam(DATA_LITERAL) String data, @Context HttpServletRequest request, @Context HttpServletResponse response) {
        return annotationHandler.viewDocumentHandler(callback, data, request, response);
    }

    /**
     * Load tree of files from base directory [POST request]
     *
     * @param request  HTTP servlet request
     * @param response HTTP servlet response
     * @return files tree data
     */
    @POST
    @Path(value = LOAD_FILE_BROWSER_TREE_DATA_HANLER)
    @Produces(MediaType.APPLICATION_JSON)
    @Override
    public Object loadFileBrowserTreeDataHandler(@Context HttpServletRequest request, @Context HttpServletResponse response) {
        return annotationHandler.loadFileBrowserTreeDataHandler(request, response);
    }

    /**
     * Load tree of files from base directory [GET request]
     *
     * @param callback callback
     * @param data     request data
     * @param response HTTP servlet response
     * @return files tree data
     */
    @GET
    @Path(value = LOAD_FILE_BROWSER_TREE_DATA_HANLER)
    @Produces(MediaType.APPLICATION_JSON)
    @Override
    public Object loadFileBrowserTreeDataHandler(@QueryParam(CALLBACK_LITERAL) String callback, @QueryParam(DATA_LITERAL) String data, @Context HttpServletResponse response) {
        return annotationHandler.loadFileBrowserTreeDataHandler(callback, data, response);
    }

    /**
     * Get thumbs and other images files [POST request]
     *
     * @param request  HTTP servlet request
     * @param response HTTP servlet response
     * @return response object
     */
    @POST
    @Path(value = GET_IMAGE_URL_HANDLER)
    @Produces(MediaType.APPLICATION_JSON)
    @Override
    public Object getImageUrlsHandler(@Context HttpServletRequest request, @Context HttpServletResponse response) {
        return annotationHandler.getImageUrlsHandler(request, response);
    }

    /**
     * Get thumbs and other images files [GET request]
     *
     * @param callback callback
     * @param data     request data
     * @param request  HTTP servlet request
     * @param response HTTP servlet response
     * @return response object
     */
    @GET
    @Path(value = GET_IMAGE_URL_HANDLER)
    @Produces(MediaType.APPLICATION_JSON)
    @Override
    public Object getImageUrlsHandler(@QueryParam(CALLBACK_LITERAL) String callback, @QueryParam(DATA_LITERAL) String data, @Context HttpServletRequest request, @Context HttpServletResponse response) {
        return annotationHandler.getImageUrlsHandler(callback, data, request, response);
    }

    /**
     * Print document [POST request]
     *
     * @param request  HTTP servlet request
     * @param response HTTP servlet response
     * @return response object
     */
    @POST
    @Path(value = GET_PRINTABLE_HTML_HANDLER)
    @Override
    public Object getPrintableHtmlHandler(@Context HttpServletRequest request, @Context HttpServletResponse response) {
        return annotationHandler.getPrintableHtmlHandler(request, response);
    }

    /**
     * Print document [GET request]
     *
     * @param callback callback
     * @param data     request data
     * @param request  HTTP servlet request
     * @param response HTTP servlet response
     * @return response object
     */
    @GET
    @Path(value = GET_PRINTABLE_HTML_HANDLER)
    @Override
    public Object getPrintableHtmlHandler(@QueryParam(CALLBACK_LITERAL) String callback, @QueryParam(DATA_LITERAL) String data, @Context HttpServletRequest request, @Context HttpServletResponse response) {
        return annotationHandler.getPrintableHtmlHandler(callback, data, request, response);
    }

    /**
     * Get document content as html
     *
     * @param request  HTTP servlet request
     * @param response HTTP servlet response
     * @return response data
     */
    @GET
    @Override
    @Path(value = GET_DOCUMENT_PAGE_HTML_HANDLER)
    public Object getDocumentPageHtmlHandler(@Context HttpServletRequest request, @Context HttpServletResponse response) {
        return annotationHandler.getDocumentPageHtmlHandler(request, response);
    }

    /**
     * Get data for print dialog
     *
     * @param path     file path
     * @param response HTTP servlet response
     * @return response object
     * @see com.groupdocs.annotation.handler.AnnotationHandler
     */
    @POST
    @Path(value = GET_PDF_WITH_PRINT_DIALOG)
    @Override
    public Object getPdfWithPrintDialog(@QueryParam(PATH_LITERAL) String path, @Context HttpServletResponse response) {
        return annotationHandler.getPdfWithPrintDialog(path, response);
    }

    /**
     * Reorder page
     *
     * @param request  HTTP servlet request
     * @param response HTTP servlet response
     * @return response data
     * @see com.groupdocs.annotation.handler.AnnotationHandler
     */
    @POST
    @Path(value = REORDER_PAGE_HANDLER)
    @Override
    public Object reorderPageHandler(@Context HttpServletRequest request, @Context HttpServletResponse response) {
        return annotationHandler.reorderPageHandler(request, response);
    }

    /**
     * {@inheritDoc}
     * @param request
     * @param response
     * @return
     */
    @POST
    @Path(value = ROTATE_PAGE_HANDLER)
    @Override
    public Object rotatePageHandler(@Context HttpServletRequest request, @Context HttpServletResponse response) {
        return annotationHandler.rotatePageHandler(request, response);
    }

    /**
     * Get document image files [GET request]
     *
     * @param guid      file guid
     * @param pageIndex index of page
     * @param response  http servlet response
     * @return null
     */
    @GET
    @Override
    @Produces("image/jpeg")
    @Path(value = GET_PRINT_DOCUMENT_PAGE_IMAGE_HANDLER)
    public Object getPrintDocumentPageImageHandler(@QueryParam(PATH_LITERAL) String guid, @QueryParam("pageIndex") Integer pageIndex, @Context HttpServletResponse response) {
        try {
            return annotationHandler.getPrintDocumentPageImageHandler(guid, pageIndex, response);
        } catch (AnnotationException e) {
            Utils.log(AnnotationResource.class, e);
            return toJson(new StatusResult(false, e.getMessage()));
        }
    }

    /**
     * {@inheritDoc}
     */
    @POST
    @Override
    @Path(value = RESTORE_ANNOTATION_REPLIES_HANDLER)
    public Object restoreAnnotationRepliesHandler(@Context HttpServletRequest request, @Context HttpServletResponse response) {
        try {
            return annotationHandler.restoreAnnotationRepliesHandler(request, response);
        } catch (AnnotationException e) {
            Utils.log(AnnotationResource.class, e);
            return toJson(new StatusResult(false, e.getMessage()));
        }
    }

    /**
     * On ready handler
     *
     * @param resource resource data received from socket
     */
    @Override
    public void onAtmosphereReady(AtmosphereResource resource) {
        try {
            annotationHandler.onAtmosphereReady(resource);
        } catch (AnnotationException e) {
            Utils.log(AnnotationResource.class, e);
        }
    }

    /**
     * On message handler [POST]
     *
     * @param resource resource data received from socket
     */
    @Override
    public void onAtmosphereMessage(AtmosphereResource resource) {
        try {
            annotationHandler.onAtmosphereMessage(resource);
        } catch (AnnotationException e) {
            Utils.log(AnnotationResource.class, e);
        }
    }
}
