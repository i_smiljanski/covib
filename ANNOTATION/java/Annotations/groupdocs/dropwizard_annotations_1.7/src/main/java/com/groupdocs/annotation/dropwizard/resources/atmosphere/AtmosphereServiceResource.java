package com.groupdocs.annotation.dropwizard.resources.atmosphere;

import com.groupdocs.annotation.dropwizard.resources.AnnotationResource;
import org.atmosphere.annotation.Broadcast;
import org.atmosphere.annotation.Suspend;
import org.atmosphere.cpr.AtmosphereResource;

import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.MediaType;

/**
 * @author Aleksey Permyakov
 */
@Path("/")
public class AtmosphereServiceResource {
    // TODO: @Context is not work for servlet
//    @Context
//    protected AnnotationHandler annotationHandler;
//    @Context
//    protected AnnotationResource annotationResource;

    @Suspend(contentType = MediaType.APPLICATION_JSON, period = 600000)
    @GET
    public void suspend(@Context AtmosphereResource atmosphereResource) {
        AnnotationResource.getInstance().onAtmosphereReady(atmosphereResource);
    }

    @POST
    @Broadcast(writeEntity = false)
    @Produces(MediaType.APPLICATION_JSON)
    public void onAtmosphereMessage(@Context AtmosphereResource atmosphereResource) {
        AnnotationResource.getInstance().onAtmosphereMessage(atmosphereResource);
    }
}
