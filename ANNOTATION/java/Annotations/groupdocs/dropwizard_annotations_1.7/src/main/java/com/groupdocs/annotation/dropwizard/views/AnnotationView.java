package com.groupdocs.annotation.dropwizard.views;


import com.yammer.dropwizard.views.View;

import java.nio.charset.Charset;

/**
 * @author Alex Bobkov
 */
public class AnnotationView extends View {
    private String headerElems;
    private String scriptElems;

    public AnnotationView(String headerElems, String scriptElems) {
        super("annotation.ftl", Charset.forName("utf-8"));
        this.headerElems = headerElems;
        this.scriptElems = scriptElems;
    }

    public String getHeaderElems() {
        return headerElems;
    }

    public void setHeaderElems(String headerElems) {
        this.headerElems = headerElems;
    }

    public String getScriptElems() {
        return scriptElems;
    }

    public void setScriptElems(String scriptElems) {
        this.scriptElems = scriptElems;
    }

}
