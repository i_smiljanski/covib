package com.groupdocs.annotation.dropwizard.synchronizer;

import java.util.concurrent.locks.Lock;
import java.util.concurrent.locks.ReentrantLock;

/**
 * Created by schroedera85 on 02.12.2014.
 */
public class Synchronizer {
    final Lock lock = new ReentrantLock();
    private static Synchronizer ourInstance = new Synchronizer();

    public static Synchronizer getInstance() {
        return ourInstance;
    }

    private Synchronizer() {
    }

    public void lock(){
        lock.lock();
    }

    public void unlock(){
        lock.unlock();
    }
}
