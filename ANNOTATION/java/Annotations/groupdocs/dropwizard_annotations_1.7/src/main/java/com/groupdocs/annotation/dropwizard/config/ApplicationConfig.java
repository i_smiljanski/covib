package com.groupdocs.annotation.dropwizard.config;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.groupdocs.annotation.config.IBackEndConfiguration;
import com.groupdocs.annotation.config.IFrontEndConfiguration;
import com.groupdocs.viewer.config.IServiceConfiguration;
import com.yammer.dropwizard.config.Configuration;

import javax.validation.Valid;

/**
 * @author Alex Bobkov
 */
public class ApplicationConfig extends Configuration implements IServiceConfiguration, IBackEndConfiguration, IFrontEndConfiguration {
    @Valid
    @JsonProperty
    protected String licensePath;
    @Valid
    @JsonProperty
    protected String applicationPath;
    @Valid
    @JsonProperty
    protected String basePath;
    @Valid
    @JsonProperty
    protected String storageType;
    @Valid
    @JsonProperty
    protected String dbServer;
    @Valid
    @JsonProperty
    protected Integer dbPort;
    @Valid
    @JsonProperty
    protected String dbName;
    @Valid
    @JsonProperty
    protected String dbUsername;
    @Valid
    @JsonProperty
    protected String dbPassword;
    @Valid
    @JsonProperty
    protected Long checkConnectionsEveryMillis;
    @Valid
    @JsonProperty
    protected Long maxConnectionAgeMillis;
    @Valid
    @JsonProperty
    protected Integer maxConnectionsFree;
    @Valid
    @JsonProperty
    protected Boolean testBeforeGet;
    @Valid
    @JsonProperty
    protected String storagePath;
    @Valid
    @JsonProperty
    protected Boolean useAuthorization;
    @Valid
    @JsonProperty
    protected Boolean useCache;
    @Valid
    @JsonProperty
    protected Integer expirationDate;
    @Valid
    @JsonProperty
    protected String encryptionKey;
    @Valid
    @JsonProperty
    protected String localesPath;
    @Valid
    @JsonProperty
    protected Integer quality;
    @Valid
    @JsonProperty
    protected Boolean showThumbnails;
    @Valid
    @JsonProperty
    protected Boolean openThumbnails;
    @Valid
    @JsonProperty
    protected Integer initialZoom;
    @Valid
    @JsonProperty
    protected Boolean zoomToFitWidth;
    @Valid
    @JsonProperty
    protected Boolean zoomToFitHeight;
    @Valid
    @JsonProperty
    protected Integer width;
    @Valid
    @JsonProperty
    protected Integer height;
    @Valid
    @JsonProperty
    protected Boolean showPrint;
    @Valid
    @JsonProperty
    protected Boolean showZoom;
    @Valid
    @JsonProperty
    protected Boolean showPaging;
    @Valid
    @JsonProperty
    protected Integer preloadPagesCount;
    @Valid
    @JsonProperty
    protected Boolean showHeader;
    @Valid
    @JsonProperty
    protected Boolean showFileExplorer;
    @Valid
    @JsonProperty
    protected Boolean useEmScaling;
    @Valid
    @JsonProperty
    protected String fileDisplayName;
    @Valid
    @JsonProperty
    protected Boolean enableRightClickMenu;
    @Valid
    @JsonProperty
    protected Boolean showToolbar;
    @Valid
    @JsonProperty
    protected Boolean enableSidePanel;
    @Valid
    @JsonProperty
    protected Boolean scrollOnFocus;
    @Valid
    @JsonProperty
    protected String strikeOutColor;
    @Valid
    @JsonProperty
    protected String highlightColor;
    @Valid
    @JsonProperty
    protected String underlineColor;
    @Valid
    @JsonProperty
    protected String textFieldBackgroundColor;
    @Valid
    @JsonProperty
    protected Boolean tabNavigationEnabled;
    @Valid
    @JsonProperty
    protected Integer minimumImageWidth;
    @Valid
    @JsonProperty
    protected Integer areaToolOptionsPenWidth;
    @Valid
    @JsonProperty
    protected String areaToolOptionsPenColor;
    @Valid
    @JsonProperty
    protected Integer areaToolOptionsPenDashStyle;
    @Valid
    @JsonProperty
    protected String areaToolOptionsBrushColor;
    @Valid
    @JsonProperty
    protected Integer polylineToolOptionsPenWidth;
    @Valid
    @JsonProperty
    protected String polylineToolOptionsPenColor;
    @Valid
    @JsonProperty
    protected Integer polylineToolOptionsPenDashStyle;
    @Valid
    @JsonProperty
    protected String polylineToolOptionsBrushColor;
    @Valid
    @JsonProperty
    protected Integer arrowToolOptionsPenWidth;
    @Valid
    @JsonProperty
    protected String arrowToolOptionsPenColor;
    @Valid
    @JsonProperty
    protected Integer arrowToolOptionsPenDashStyle;
    @Valid
    @JsonProperty
    protected String arrowToolOptionsBrushColor;
    @Valid
    @JsonProperty
    protected Integer enabledTools;
    @Valid
    @JsonProperty
    protected Integer connectorPosition;
    @Valid
    @JsonProperty
    protected Boolean saveReplyOnFocusLoss;
    @Valid
    @JsonProperty
    protected Boolean clickableAnnotations;
    @Valid
    @JsonProperty
    protected Boolean disconnectUncommented;
    @Valid
    @JsonProperty
    protected Integer strikeoutMode;
    @Valid
    @JsonProperty
    protected Boolean anyToolSelection;
    @Valid
    @JsonProperty
    protected String sidebarContainerSelector;
    @Valid
    @JsonProperty
    protected Boolean usePageNumberInUrlHash;
    @Valid
    @JsonProperty
    protected Boolean textSelectionSynchronousCalculation;
    @Valid
    @JsonProperty
    protected Boolean variableHeightPageSupport;
    @Valid
    @JsonProperty
    protected Boolean createMarkup;
    @Valid
    @JsonProperty
    protected Boolean use_pdf;
    @Valid
    @JsonProperty
    protected String mode;
    @Valid
    @JsonProperty
    protected String graphicsContainerSelector;
    @Valid
    @JsonProperty
    protected Boolean useBrowserCache;
    @Valid
    @JsonProperty
    protected String widgetId;
    @Valid
    @JsonProperty
    protected Boolean rightPanelEnabled;
    @Valid
    @JsonProperty
    protected Long maxCacheSize;
    @Valid
    @JsonProperty
    protected String uploadPath;
    @Valid
    @JsonProperty
    protected Boolean undoEnabled;
    @Valid
    @JsonProperty
    protected String jqueryFileDownloadCookieName;
    @Valid
    @JsonProperty
    protected Integer watermarkFontSize;
    @Valid
    @JsonProperty
    protected String watermarkPosition;
    @Valid
    @JsonProperty
    protected Boolean convertWordDocumentsCompletely;
    @Valid
    @JsonProperty
    protected Boolean ignoreDocumentAbsence;
    @Valid
    @JsonProperty
    protected Boolean preloadPagesOnBrowserSide;
    @Valid
    @JsonProperty
    protected Boolean printWithWatermark;
    @Valid
    @JsonProperty
    protected Boolean supportPageRotation;
    @Valid
    @JsonProperty
    protected String storeLogic;
    @Valid
    @JsonProperty
    protected Boolean printAnnotations;
    @Valid
    @JsonProperty
    protected String penColor;
    @Valid
    @JsonProperty
    protected Integer penStyle;
    @Valid
    @JsonProperty
    protected Integer penWidth;
    @Valid
    @JsonProperty
    protected String backgroundColor;
    @Valid
    @JsonProperty
    protected String localization;
    @Valid
    @JsonProperty
    private Boolean disableAtmosphere;

    /**
     * {@inheritDoc}
     */
    @Override
    public String getLicensePath() {
        return licensePath;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public String getApplicationPath() {
        return applicationPath;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public String getBasePath() {
        return basePath;
    }

    /**
     * Getter for property 'storageType'.
     *
     * @return Value for property 'storageType'.
     */
    public String getStorageType() {
        return storageType;
    }

    /**
     * Getter for property 'dbServer'.
     *
     * @return Value for property 'dbServer'.
     */
    public String getDbServer() {
        return dbServer;
    }

    /**
     * Getter for property 'dbPort'.
     *
     * @return Value for property 'dbPort'.
     */
    public Integer getDbPort() {
        return dbPort;
    }

    /**
     * Getter for property 'dbName'.
     *
     * @return Value for property 'dbName'.
     */
    public String getDbName() {
        return dbName;
    }

    /**
     * Getter for property 'dbUsername'.
     *
     * @return Value for property 'dbUsername'.
     */
    public String getDbUsername() {
        return dbUsername;
    }

    /**
     * Getter for property 'dbPassword'.
     *
     * @return Value for property 'dbPassword'.
     */
    public String getDbPassword() {
        return dbPassword;
    }

    /**
     * Getter for property 'checkConnectionsEveryMillis'.
     *
     * @return Value for property 'checkConnectionsEveryMillis'.
     */
    public Long getCheckConnectionsEveryMillis() {
        return checkConnectionsEveryMillis;
    }

    /**
     * Getter for property 'maxConnectionAgeMillis'.
     *
     * @return Value for property 'maxConnectionAgeMillis'.
     */
    public Long getMaxConnectionAgeMillis() {
        return maxConnectionAgeMillis;
    }

    /**
     * Getter for property 'maxConnectionsFree'.
     *
     * @return Value for property 'maxConnectionsFree'.
     */
    public Integer getMaxConnectionsFree() {
        return maxConnectionsFree;
    }

    /**
     * Getter for property 'testBeforeGet'.
     *
     * @return Value for property 'testBeforeGet'.
     */
    public Boolean getTestBeforeGet() {
        return testBeforeGet;
    }

    /**
     * Getter for property 'storagePath'.
     *
     * @return Value for property 'storagePath'.
     */
    public String getStoragePath() {
        return "null".equals(storagePath) ? null : storagePath;
    }

    /**
     * Getter for property 'useAuthorization'.
     *
     * @return Value for property 'useAuthorization'.
     */
    public Boolean isUseAuthorization() {
        return useAuthorization;
    }

    /**
     * Getter for property 'useCache'.
     *
     * @return Value for property 'useCache'.
     */
    public Boolean isUseCache() {
        return useCache;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public Integer getExpirationDate() {
        return expirationDate;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public String getEncryptionKey() {
        return encryptionKey;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public String getLocalesPath() {
        return localesPath;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public Integer getQuality() {
        return quality;
    }

    /**
     * Getter for property 'showThumbnails'.
     *
     * @return Value for property 'showThumbnails'.
     */
    public Boolean isShowThumbnails() {
        return showThumbnails;
    }

    /**
     * Getter for property 'openThumbnails'.
     *
     * @return Value for property 'openThumbnails'.
     */
    public Boolean isOpenThumbnails() {
        return openThumbnails;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public Integer getInitialZoom() {
        return initialZoom;
    }

    /**
     * Getter for property 'zoomToFitWidth'.
     *
     * @return Value for property 'zoomToFitWidth'.
     */
    public Boolean isZoomToFitWidth() {
        return zoomToFitWidth;
    }

    /**
     * Getter for property 'zoomToFitHeight'.
     *
     * @return Value for property 'zoomToFitHeight'.
     */
    public Boolean isZoomToFitHeight() {
        return zoomToFitHeight;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public Integer getWidth() {
        return width;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public Integer getHeight() {
        return height;
    }

    /**
     * Getter for property 'showPrint'.
     *
     * @return Value for property 'showPrint'.
     */
    public Boolean isShowPrint() {
        return showPrint;
    }

    /**
     * Getter for property 'showZoom'.
     *
     * @return Value for property 'showZoom'.
     */
    public Boolean isShowZoom() {
        return showZoom;
    }

    /**
     * Getter for property 'showPaging'.
     *
     * @return Value for property 'showPaging'.
     */
    public Boolean isShowPaging() {
        return showPaging;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public Integer getPreloadPagesCount() {
        return preloadPagesCount;
    }

    /**
     * Getter for property 'showHeader'.
     *
     * @return Value for property 'showHeader'.
     */
    public Boolean isShowHeader() {
        return showHeader;
    }

    /**
     * Getter for property 'showFileExplorer'.
     *
     * @return Value for property 'showFileExplorer'.
     */
    public Boolean isShowFileExplorer() {
        return showFileExplorer;
    }

    /**
     * Getter for property 'useEmScaling'.
     *
     * @return Value for property 'useEmScaling'.
     */
    public Boolean isUseEmScaling() {
        return useEmScaling;
    }

    /**
     * Gets file display name.
     * @return file display name.
     */
    @Override
    public String getFileDisplayName() {
        return fileDisplayName;
    }

    /**
     * Sets file display name.
     *
     * @param fileDisplayName the file display name
     */
    public void setFileDisplayName(String fileDisplayName) {
        this.fileDisplayName = fileDisplayName;
    }

    /**
     * Getter for property 'enableRightClickMenu'.
     *
     * @return Value for property 'enableRightClickMenu'.
     */
    public Boolean isEnableRightClickMenu() {
        return enableRightClickMenu;
    }

    /**
     * Getter for property 'showToolbar'.
     *
     * @return Value for property 'showToolbar'.
     */
    public Boolean isShowToolbar() {
        return showToolbar;
    }

    /**
     * Getter for property 'enableSidePanel'.
     *
     * @return Value for property 'enableSidePanel'.
     */
    public Boolean isEnableSidePanel() {
        return enableSidePanel;
    }

    /**
     * Getter for property 'scrollOnFocus'.
     *
     * @return Value for property 'scrollOnFocus'.
     */
    public Boolean isScrollOnFocus() {
        return scrollOnFocus;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public String getStrikeOutColor() {
        return "#" + strikeOutColor;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public String getHighlightColor() {
        return "#" + highlightColor;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public String getUnderlineColor() {
        return "#" + underlineColor;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public String getTextFieldBackgroundColor() {
        return "#" + textFieldBackgroundColor;
    }

    /**
     * Getter for property 'tabNavigationEnabled'.
     *
     * @return Value for property 'tabNavigationEnabled'.
     */
    public Boolean isTabNavigationEnabled() {
        return tabNavigationEnabled;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public Integer getAreaToolOptionsPenWidth() {
        return areaToolOptionsPenWidth;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public String getAreaToolOptionsPenColor() {
        return "#" + areaToolOptionsPenColor;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public Integer getAreaToolOptionsPenDashStyle() {
        return areaToolOptionsPenDashStyle;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public String getAreaToolOptionsBrushColor() {
        return "#" + areaToolOptionsBrushColor;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public Integer getPolylineToolOptionsPenWidth() {
        return polylineToolOptionsPenWidth;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public String getPolylineToolOptionsPenColor() {
        return "#" + polylineToolOptionsPenColor;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public Integer getPolylineToolOptionsPenDashStyle() {
        return polylineToolOptionsPenDashStyle;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public String getPolylineToolOptionsBrushColor() {
        return "#" + polylineToolOptionsBrushColor;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public Integer getArrowToolOptionsPenWidth() {
        return arrowToolOptionsPenWidth;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public String getArrowToolOptionsPenColor() {
        return "#" + arrowToolOptionsPenColor;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public Integer getArrowToolOptionsPenDashStyle() {
        return arrowToolOptionsPenDashStyle;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public String getArrowToolOptionsBrushColor() {
        return "#" + arrowToolOptionsBrushColor;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public Integer getEnabledTools() {
        return enabledTools;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public Integer getConnectorPosition() {
        return connectorPosition;
    }

    /**
     * Getter for property 'saveReplyOnFocusLoss'.
     *
     * @return Value for property 'saveReplyOnFocusLoss'.
     */
    public Boolean isSaveReplyOnFocusLoss() {
        return saveReplyOnFocusLoss;
    }

    /**
     * Getter for property 'clickableAnnotations'.
     *
     * @return Value for property 'clickableAnnotations'.
     */
    public Boolean isClickableAnnotations() {
        return clickableAnnotations;
    }

    /**
     * Getter for property 'disconnectUncommented'.
     *
     * @return Value for property 'disconnectUncommented'.
     */
    public Boolean isDisconnectUncommented() {
        return disconnectUncommented;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public Integer getStrikeoutMode() {
        return strikeoutMode;
    }

    /**
     * Getter for property 'anyToolSelection'.
     *
     * @return Value for property 'anyToolSelection'.
     */
    public Boolean isAnyToolSelection() {
        return anyToolSelection;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public String getSidebarContainerSelector() {
        return sidebarContainerSelector;
    }

    /**
     * Getter for property 'usePageNumberInUrlHash'.
     *
     * @return Value for property 'usePageNumberInUrlHash'.
     */
    public Boolean isUsePageNumberInUrlHash() {
        return usePageNumberInUrlHash;
    }

    /**
     * Getter for property 'textSelectionSynchronousCalculation'.
     *
     * @return Value for property 'textSelectionSynchronousCalculation'.
     */
    public Boolean isTextSelectionSynchronousCalculation() {
        return textSelectionSynchronousCalculation;
    }

    /**
     * Getter for property 'variableHeightPageSupport'.
     *
     * @return Value for property 'variableHeightPageSupport'.
     */
    public Boolean isVariableHeightPageSupport() {
        return variableHeightPageSupport;
    }

    /**
     * Getter for property 'createMarkup'.
     *
     * @return Value for property 'createMarkup'.
     */
    public Boolean isCreateMarkup() {
        return createMarkup;
    }

    /**
     * Getter for property 'use_pdf'.
     *
     * @return Value for property 'use_pdf'.
     */
    public Boolean isUse_pdf() {
        return use_pdf;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public String getMode() {
        return mode;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public String getSelectionContainerSelector() {
        return "[name='selection-content']";
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public String getGraphicsContainerSelector() {
        return graphicsContainerSelector;
    }

    /**
     * Getter for property 'useBrowserCache'.
     *
     * @return Value for property 'useBrowserCache'.
     */
    public Boolean isUseBrowserCache() {
        return useBrowserCache;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public String getWidgetId() {
        return widgetId;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public String getLocalization() {
        return localization;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public Boolean isDisableAtmosphere() {
        return disableAtmosphere;
    }

    /**
     * Getter for property 'rightPanelEnabled'.
     *
     * @return Value for property 'rightPanelEnabled'.
     */
    public Boolean isRightPanelEnabled() {
        return rightPanelEnabled;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public Long getMaxCacheSize() {
        return maxCacheSize;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public String getUploadPath() {
        return uploadPath;
    }

    /**
     * Getter for property 'undoEnabled'.
     *
     * @return Value for property 'undoEnabled'.
     */
    public Boolean isUndoEnabled() {
        return undoEnabled;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public String getJqueryFileDownloadCookieName() {
        return jqueryFileDownloadCookieName;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public Integer getWatermarkFontSize() {
        return watermarkFontSize;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public String getWatermarkPosition() {
        return watermarkPosition;
    }

    /**
     * Getter for property 'convertWordDocumentsCompletely'.
     *
     * @return Value for property 'convertWordDocumentsCompletely'.
     */
    public Boolean isConvertWordDocumentsCompletely() {
        return convertWordDocumentsCompletely;
    }

    /**
     * Getter for property 'ignoreDocumentAbsence'.
     *
     * @return Value for property 'ignoreDocumentAbsence'.
     */
    public Boolean isIgnoreDocumentAbsence() {
        return ignoreDocumentAbsence;
    }

    /**
     * Getter for property 'preloadPagesOnBrowserSide'.
     *
     * @return Value for property 'preloadPagesOnBrowserSide'.
     */
    public Boolean isPreloadPagesOnBrowserSide() {
        return preloadPagesOnBrowserSide;
    }

    /**
     * Getter for property 'printWithWatermark'.
     *
     * @return Value for property 'printWithWatermark'.
     */
    public Boolean isPrintWithWatermark() {
        return printWithWatermark;
    }

    /**
     * Getter for property 'supportPageRotation'.
     *
     * @return Value for property 'supportPageRotation'.
     */
    public Boolean isSupportPageRotation() {
        return supportPageRotation;
    }

    /**
     * Getter for property 'storeLogic'.
     *
     * @return Value for property 'storeLogic'.
     */
    public String getStoreLogic() {
        return storeLogic;
    }

    /**
     * Get background color
     *
     * @return background color
     */
    @Override
    public String getBackgroundColor() {
        return "#" + backgroundColor;
    }

    /**
     * Check that folder button will be showed
     *
     * @return is folder button will be showed
     */
    @Override
    public Boolean isShowFolderBrowser() {
        return true;
    }

    /**
     * Check that download button will be showed
     *
     * @return is download button will be showed
     */
    @Override
    public Boolean isShowDownload() {
        return true;
    }

    /**
     * Is show viewer style controls
     *
     * @return is show viewer style controlss
     */
    @Override
    public Boolean isShowViewerStyleControl() {
        return true;
    }

    /**
     * Check that search field will be showed
     *
     * @return is search field will be showed
     */
    @Override
    public Boolean isShowSearch() {
        return true;
    }

    /**
     * Get viewer style
     *
     * @return viewer style
     */
    @Override
    public Integer getViewerStyle() {
        return 1;
    }

    /**
     * Check that text selection functional will enabled
     *
     * @return is text selection functional will enabled
     */
    @Override
    public Boolean isSupportTextSelection() {
        return true;
    }

    /**
     * Is use pdf printing
     *
     * @return is use pdf printing
     */
    @Override
    public Boolean isUsePdfPrinting() {
        return false;
    }

    /**
     * Toolbar buttons box shadow style
     *
     * @return shadow style
     */
    @Override
    public String getToolbarButtonsBoxShadowStyle() {
        return "";
    }

    /**
     * Toolbar buttons box shadow hower style
     *
     * @return shadow hower style
     */
    @Override
    public String getToolbarButtonsBoxShadowHoverStyle() {
        return "";
    }

    /**
     * Background color of Thumbnails container
     *
     * @return background color
     */
    @Override
    public String getThumbnailsContainerBackgroundColor() {
        return "";
    }

    /**
     * Border color of Thumbnails container
     *
     * @return border color
     */
    @Override
    public String getThumbnailsContainerBorderRightColor() {
        return "";
    }

    /**
     * Color of toolbar bottom border color
     *
     * @return bottom border color
     */
    @Override
    public String getToolbarBorderBottomColor() {
        return "";
    }

    /**
     * Color of input field border
     *
     * @return border color
     */
    @Override
    public String getToolbarInputFieldBorderColor() {
        return "";
    }

    /**
     * Color of toolbar button border
     *
     * @return border color
     */
    @Override
    public String getToolbarButtonBorderColor() {
        return "";
    }

    /**
     * Hover color of toolbar button border
     *
     * @return hover border color
     */
    @Override
    public String getToolbarButtonBorderHoverColor() {
        return "";
    }

    /**
     * Width of thumbnails container
     *
     * @return thumbnails container width
     */
    @Override
    public Integer getThumbnailsContainerWidth() {
        return 0;
    }

    /**
     * Is show download errors in popup
     *
     * @return is show download errors in popup
     */
    @Override
    public Boolean isShowDownloadErrorsInPopup() {
        return true;
    }

    /**
     * Is show width of images
     *
     * @return is show width of images
     */
    @Override
    public Boolean isShowImageWidth() {
        return true;
    }

    /**
     * Get minimum of image width
     *
     * @return minimum image width
     */
    @Override
    public Integer getMinimumImageWidth() {
        return 0;
    }

    /**
     * Is enable standard error handling
     *
     * @return is enable standard error handling
     */
    @Override
    public Boolean isEnableStandardErrorHandling() {
        return true;
    }

    /**
     * Is use html based engine
     *
     * @return is use html based engine
     */
    @Override
    public Boolean isUseHtmlBasedEngine() {
        return false;
    }

    /**
     * Is use image based printing
     *
     * @return is use image based printing
     */
    @Override
    public Boolean isUseImageBasedPrinting() {
        return true;
    }

    /**
     * Is download pdf file
     *
     * @return is download pdf file
     */
    @Override
    public Boolean isDownloadPdfFile() {
        return false;
    }

    /**
     * Is search for separate words
     *
     * @return is search for separate words
     */
    @Override
    public Boolean isSearchForSeparateWords() {
        return false;
    }

    /**
     * Is prevent touch events building
     *
     * @return is prevent touch events building
     */
    @Override
    public Boolean isPreventTouchEventsBubbling() {
        return true;
    }

    /**
     * Is use inner thumbnails
     *
     * @return is use inner thumbnails
     */
    @Override
    public Boolean isUseInnerThumbnails() {
        return true;
    }

    /**
     * Get watermark text
     *
     * @return watermark text
     */
    @Override
    public String getWatermarkText() {
        return "";
    }

    /**
     * Get watermark color
     *
     * @return watermark color
     */
    @Override
    public String getWatermarkColor() {
        return "";
    }

    /**
     * Is support page reordering
     *
     * @return support page reordering
     */
    @Override
    public Boolean isSupportPageReordering() {
        return false;
    }

    /**
     * Is only shrink large pages
     *
     * @return is only shrink large pages
     */
    @Override
    public Boolean isOnlyShrinkLargePages() {
        return false;
    }

    /**
     * Get search highlight color
     *
     * @return search highlight color
     */
    @Override
    public String getSearchHighlightColor() {
        return "";
    }

    /**
     * Get current search highlight color
     *
     * @return current search highlight color
     */
    @Override
    public String getCurrentSearchHighlightColor() {
        return "";
    }

    /**
     * Is treat phrases in double quotes as exact phrases
     *
     * @return is treat phrases in double quotes as exact phrases
     */
    @Override
    public Boolean isTreatPhrasesInDoubleQuotesAsExactPhrases() {
        return false;
    }

    /**
     * Is use png images for html based engine
     *
     * @return is use png images for html based engine
     */
    @Override
    public Boolean isUsePngImagesForHtmlBasedEngine() {
        return true;
    }

    /**
     * Is show one page in row
     *
     * @return is show one page in row
     */
    @Override
    public Boolean isShowOnePageInRow() {
        return false;
    }

    /**
     * Is load all pages on search
     *
     * @return is load all pages on search
     */
    @Override
    public Boolean isLoadAllPagesOnSearch() {
        return false;
    }

    /**
     * Get pen width
     *
     * @return pen width
     */
    @Override
    public Integer getPenWidth() {
        return penWidth;
    }

    /**
     * Get pen color
     *
     * @return pen color
     */
    @Override
    public String getPenColor() {
        return "#" + penColor;
    }

    /**
     * Get pen style
     *
     * @return pen style
     */
    @Override
    public Integer getPenStyle() {
        return penStyle;
    }

    /**
     * Getter for property 'printAnnotations'.
     *
     * @return Value for property 'printAnnotations'.
     */
    public Boolean isPrintAnnotations() {
        return printAnnotations;
    }

}