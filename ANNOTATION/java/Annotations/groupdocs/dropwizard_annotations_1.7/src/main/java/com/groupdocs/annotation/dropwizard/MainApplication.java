package com.groupdocs.annotation.dropwizard;

import com.groupdocs.annotation.common.ICallback;
import com.groupdocs.annotation.common.Pair;
import com.groupdocs.annotation.common.Utils;
import com.groupdocs.annotation.data.common.StorageType;
import com.groupdocs.annotation.data.common.StoreLogic;
import com.groupdocs.annotation.data.connector.IConnector;
import com.groupdocs.annotation.data.connector.data.JsonDataConnector;
import com.groupdocs.annotation.data.connector.data.XmlDataConnector;
import com.groupdocs.annotation.data.connector.db.*;
import com.groupdocs.annotation.data.tables.interfaces.IDocument;
import com.groupdocs.annotation.data.tables.interfaces.IUser;
import com.groupdocs.annotation.dropwizard.config.ApplicationConfig;
import com.groupdocs.annotation.dropwizard.data.connector.CustomXmlDataConnector;
import com.groupdocs.annotation.dropwizard.data.connector.ICustomConnector;
import com.groupdocs.annotation.dropwizard.provider.AnnotationHandlerProvider;
import com.groupdocs.annotation.dropwizard.provider.ApplicationConfigurationProvider;
import com.groupdocs.annotation.dropwizard.resources.AnnotationResource;
import com.groupdocs.annotation.enums.AccessRights;
import com.groupdocs.annotation.exception.AnnotationException;
import com.groupdocs.annotation.handler.AnnotationHandler;
import com.groupdocs.viewer.config.ServiceConfiguration;
import com.yammer.dropwizard.Service;
import com.yammer.dropwizard.config.Bootstrap;
import com.yammer.dropwizard.config.Environment;
import com.yammer.dropwizard.config.FilterBuilder;
import com.yammer.dropwizard.config.HttpConfiguration;
import com.yammer.dropwizard.views.ViewBundle;
import org.atmosphere.cpr.AtmosphereServlet;
import org.eclipse.jetty.servlets.CrossOriginFilter;

import java.awt.*;
import java.util.Locale;

/**
 * @author Alex Bobkov
 */
public class MainApplication extends Service<ApplicationConfig> {

    public static void main(String[] args) throws Exception {
        new MainApplication().run(args);
    }

    @Override
    public void initialize(Bootstrap<ApplicationConfig> bootstrap) {
        bootstrap.addBundle(new ViewBundle());
        bootstrap.setName("GroupDocs.Annotation for Java");
    }

    @Override
    public void run(ApplicationConfig applicationConfig, Environment environment) throws Exception {
        applicationConfig.getHttpConfiguration().setConnectorType(HttpConfiguration.ConnectorType.NONBLOCKING);

        IConnector connector = null;
        String storageTypeString = applicationConfig.getStorageType();
        String dbServer = applicationConfig.getDbServer();
        Integer dbPort = applicationConfig.getDbPort();
        String dbName = applicationConfig.getDbName();
        String dbUsername = applicationConfig.getDbUsername();
        String dbPassword = applicationConfig.getDbPassword();
        ServiceConfiguration serviceConfiguration = new ServiceConfiguration(applicationConfig);
        String tempPath = serviceConfiguration.getTempDir();
        StoreLogic storeLogic = StoreLogic.fromValue(applicationConfig.getStoreLogic());
        String storagePath = Utils.or(applicationConfig.getStoragePath(), tempPath);
        if (storageTypeString != null && !storageTypeString.isEmpty()) {
            StorageType storageType = StorageType.fromValue(storageTypeString);
            if (storageType == null) {
                throw new AnnotationException("Unknown storage type: " + storageTypeString);
            }
            switch (storageType) {
                case DEFAULT:
                    connector = null;
                    break;
                case SQLITE:
                    connector = new SqliteDatabaseConnector(storagePath, "customSQLITEdatabaseStorage.db");
                    break;
                case MYSQL:
                    connector = new MysqlDatabaseConnector(dbServer, dbPort, dbName, dbUsername, dbPassword);
                    break;
                case MSSQL:
                    connector = new MssqlDatabaseConnector(dbServer, dbPort, dbName, dbUsername, dbPassword);
                    break;
                case JSON:
                    connector = new JsonDataConnector(storagePath, storeLogic);
                    break;
                case XML:
                    connector = new XmlDataConnector(storagePath, storeLogic);
                    break;
                case POSTGRE:
                    connector = new PostgresqlDatabaseConnector(dbServer, dbPort, dbName, dbUsername, dbPassword);
                    break;
                case CUSTOM:
                    connector = new CustomXmlDataConnector();
                    break;
            }
        }
        // If connector is database connector - configure pool
        if (connector instanceof AbstractDatabaseConnector) {
            AbstractDatabaseConnector abstractDatabaseConnector = (AbstractDatabaseConnector) connector;
            abstractDatabaseConnector.setCheckConnectionsEveryMillis(applicationConfig.getCheckConnectionsEveryMillis());
            abstractDatabaseConnector.setMaxConnectionAgeMillis(applicationConfig.getMaxConnectionAgeMillis());
            abstractDatabaseConnector.setMaxConnectionsFree(applicationConfig.getMaxConnectionsFree());
            abstractDatabaseConnector.setTestBeforeGet(applicationConfig.getTestBeforeGet());
        }


        // It need for date formatting
        Locale.setDefault(Locale.CANADA);
        //
        AnnotationHandler annotationHandler = new AnnotationHandler(serviceConfiguration, connector);
        if (connector instanceof ICustomConnector){
            ((ICustomConnector)connector).setEnvironmentCreator(annotationHandler);
        }
        // This callback will be called for each case when collaborator should be created
        annotationHandler.setCollaboratorCallback(new ICallback<Pair<Integer, Color>, Pair<IUser, IDocument>>() {
            @Override
            public Pair<Integer, Color> onCallback(Pair<IUser, IDocument> param) {
                return new Pair<Integer, Color>(AccessRights.All.value(), Color.red);
            }
        });



        // Register annotation handler provider for possibility to inject annotationHandler as context
        environment.addProvider(new AnnotationHandlerProvider(annotationHandler));
        // Register application config provider for possibility to inject applicationConfig as context
        environment.addProvider(new ApplicationConfigurationProvider(applicationConfig));

        // Add annotation handler resource
        environment.addResource(new AnnotationResource());

        initializeAtmosphere(applicationConfig, environment);
    }

    void initializeAtmosphere(ApplicationConfig configuration, Environment environment) {
        FilterBuilder fconfig = environment.addFilter(CrossOriginFilter.class, "/annotation");
        fconfig.setInitParam(CrossOriginFilter.ALLOWED_ORIGINS_PARAM, "*");

        AtmosphereServlet atmosphereServlet = new AtmosphereServlet();
        atmosphereServlet.framework().addInitParameter("com.sun.jersey.config.property.packages", "com.groupdocs.annotation.dropwizard.resources.atmosphere");
        atmosphereServlet.framework().addInitParameter("org.atmosphere.websocket.messageContentType", "application/json");
        atmosphereServlet.framework().addInitParameter("org.atmosphere.cpr.AtmosphereInterceptor", "org.atmosphere.client.TrackMessageSizeInterceptor");
        environment.addServlet(atmosphereServlet, "/annotation/*");
    }
}
