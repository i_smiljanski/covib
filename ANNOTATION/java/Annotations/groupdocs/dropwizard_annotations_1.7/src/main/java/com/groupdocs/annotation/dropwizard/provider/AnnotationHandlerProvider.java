package com.groupdocs.annotation.dropwizard.provider;

import com.groupdocs.annotation.handler.AnnotationHandler;
import com.sun.jersey.spi.inject.SingletonTypeInjectableProvider;

import javax.ws.rs.core.Context;
import javax.ws.rs.ext.Provider;

/**
 * @author by Aleksey Permyakov on 23.06.14.
 */
@Provider
public class AnnotationHandlerProvider extends SingletonTypeInjectableProvider<Context, AnnotationHandler> {

    public AnnotationHandlerProvider(AnnotationHandler instance) {
        super(AnnotationHandler.class, instance);
    }
}
