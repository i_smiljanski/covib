package com.groupdocs.annotation.dropwizard.provider;

import com.groupdocs.annotation.dropwizard.config.ApplicationConfig;
import com.sun.jersey.spi.inject.SingletonTypeInjectableProvider;

import javax.ws.rs.core.Context;
import javax.ws.rs.ext.Provider;

/**
 * @author by Aleksey Permyakov on 23.06.14.
 */
@Provider
public class ApplicationConfigurationProvider extends SingletonTypeInjectableProvider<Context, ApplicationConfig> {
    public ApplicationConfigurationProvider(ApplicationConfig instance) {
        super(ApplicationConfig.class, instance);
    }
}
