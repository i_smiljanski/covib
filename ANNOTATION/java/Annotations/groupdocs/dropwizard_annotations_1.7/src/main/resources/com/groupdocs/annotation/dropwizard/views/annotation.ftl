<#-- @ftlvariable name="" type="com.groupdocs.annotation.dropwizard.views.AnnotationView" -->
<!DOCTYPE html>
<html>
<head>
    <title>GroupDocs.Annotation for Java Sample</title>
    <meta charset="utf-8">
${headerElems}
</head>
<body>
<button>Кнопка с текстом</button>
<div id="annotation-widget"
     style="width:100%;height:100%;overflow:hidden;position:relative;margin-bottom:20px;background-color:gray;border:1px solid #ccc;"></div>
${scriptElems}

</body>
</html>