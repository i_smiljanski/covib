package com.uptodata.isr.db.pdf;

import com.uptodata.isr.db.connection.ConnectionUtil;
import com.uptodata.isr.db.trace.DbLogging;
import com.uptodata.isr.utils.ConvertUtils;
import com.uptodata.isr.utils.exceptions.MessageStackException;
import com.uptodata.isr.utils.ws.SOAPUtil;
import com.uptodata.isr.utils.xml.dom.XmlHandler;
import org.apache.commons.lang.StringUtils;

import java.sql.Clob;

import static com.uptodata.isr.db.trace.DbLogging.METHOD_BEGIN;
import static com.uptodata.isr.db.trace.DbLogging.METHOD_END;

/**
 * Created by smiljanskii60 on 26.06.2014.
 */
public class PdfDbClient {
    protected static DbLogging log;
    final static String targetNamespace = "http://ws.pdf.isr.uptodata.com/";
    static XmlHandler inputXmlHandler;


    protected static void initLogger() throws MessageStackException {
        try {
            log = new DbLogging();
        } catch (Exception e) {
            throw new MessageStackException("Logger initialization missing " + e);
        }
    }

    /**
     *
     * @param clobXml xml file with parameters for server method "convertPdfToImages"
     * @param logLevel  job log level
     * @param logReference job log reference
     * @param timeout specifies the connect timeout value in seconds
     * @return null if server returns null or xml with error messages from server
     * @throws MessageStackException any other error converted to MessageStackException
     */
    public static Clob convertPdfToImages(Clob clobXml, String logLevel, String logReference, int timeout)
            throws MessageStackException {
        initLogger();
        log.stat(METHOD_BEGIN, METHOD_BEGIN);
        try {
            byte[] inputXml = ConvertUtils.getBytes(clobXml);
            log.info("inputXml length", inputXml.length);
            String response = convertPdfToImages(inputXml, logLevel, logReference, timeout);

            if (StringUtils.isNotEmpty(response)) {
                return ConvertUtils.getClob(ConnectionUtil.getDefaultConnection(), response.getBytes());
            }
            log.stat(METHOD_END, response);
            return null;
        } catch (Exception e) {
            log.error("error", e);
            throw new MessageStackException(e, MessageStackException.SEVERITY_CRITICAL, e.getMessage(),
                    MessageStackException.getCurrentMethod());
        }
    }

    /**
     *
     * @param inputXml xml file as bytes with parameters for server method "convertPdfToImages"
     * @param logLevel  job log level
     * @param logReference job log reference
     * @param timeout specifies the connect timeout value in seconds
     * @return return value from server
     * @throws MessageStackException exception from server
     */
    public static String convertPdfToImages(byte[] inputXml, String logLevel, String logReference, int timeout)
            throws MessageStackException {
        initLogger();
        inputXmlHandler = new XmlHandler(inputXml);

        String host = getParameterValueForPath("host");
        String port = getParameterValueForPath("port");
        String context = getParameterValueForPath("context");

        String url = "http://" + host + ":" + port + context;
        log.info("Webservice url", url);

        String[] args = new String[14];
        args[0] = getParameterValueForPath("serverName");
        args[1] = getParameterValueForPath("filename");
        args[2] = getParameterValueForPath("bookmark");
        args[3] = getParameterValueForPath("extension");
        args[4] = getParameterValueForPath("jobId");
        args[5] = getParameterValueForPath("transferNumber");
        args[6] = getParameterValueForPath("transfer_input_config");
        args[7] = getParameterValueForPath("transfer_result_config");
        args[8] = getParameterValueForPath("transfer_key");
        args[9] = getParameterValueForPath("keyDelimiter");
        args[10] = getParameterValueForPath("resolution");
        args[11] = getParameterValueForPath("scaling");
        args[12] = logLevel;
        args[13] = logReference;

        log.debug("send request to  ", url + "; method: convertPdfToImages");
        String sent = SOAPUtil.callMethod(url, targetNamespace, args, "convertPdfToImages", timeout);
        log.stat("end", "fileTransferId=" + sent);
        return sent;
    }

    /**
     * gets value for path in xml file with parameters
     * @param path  parameter name
     * @return parameter value
     */
    private static String getParameterValueForPath(String path) {
        return inputXmlHandler.findXpath("//" + path, "STRING").toString();
    }

    public static void main(String[] args) {
        try {
            initLogger();

        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}
