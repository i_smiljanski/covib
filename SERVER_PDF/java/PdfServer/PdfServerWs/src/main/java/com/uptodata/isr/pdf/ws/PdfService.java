package com.uptodata.isr.pdf.ws;

import com.uptodata.isr.observer.transfer.DbFileSupplier;
import com.uptodata.isr.observer.transfer.FileSupplier;
import com.uptodata.isr.observers.base.dbAccess.DbData;
import com.uptodata.isr.observers.childServer.ObserverChildServerImpl;
import com.uptodata.isr.server.utils.fileSystem.FileSystemWorker;
import com.uptodata.isr.utils.ConvertUtils;
import com.uptodata.isr.utils.exceptions.MessageStackException;
import com.uptodata.isr.utils.xml.dom.XmlHandler;
import org.apache.commons.collections4.map.CaseInsensitiveMap;
import org.apache.commons.lang.StringUtils;
import org.apache.logging.log4j.Level;
import org.w3c.dom.Element;

import javax.jws.WebMethod;
import javax.jws.WebService;
import javax.jws.soap.SOAPBinding;
import java.io.File;
import java.util.HashMap;
import java.util.Properties;

/**
 * Created by smiljanskii60 on 16.08.2021.
 */
@WebService(name = "PdfServerInterface", targetNamespace = "http://ws.pdf.isr.uptodata.com/")
@SOAPBinding(style = SOAPBinding.Style.DOCUMENT, use = SOAPBinding.Use.LITERAL)
public class PdfService extends ObserverChildServerImpl {
    DbData dbData;
    private String logLevel;
    private final String workFolderName = "workFolder";

    /**
     * Webservice needs empty constructor
     */
    public PdfService() {
        statusXmlMethod = 0;
        statusXml = initStatusXml();
    }

    /**
     * constructor
     * @param dbData  database connection data to get and bring files from / to the database
     * @param logLevel log level when starting the server
     */
    public PdfService(DbData dbData, String logLevel) {
        this.logLevel = logLevel;
        this.dbData = dbData;
        statusXmlMethod = 0;
    }

    public void init() throws MessageStackException {
        log = initLogger(logLevel, "pdf server  service");
        boolean isWorkFolderCreated = initWorkFolder();
        if (!isWorkFolderCreated) {
            log.error("error==XsltTransformServer can not create work folder with name " + workFolderName);
        }
        statusXml = initStatusXml();  //test case for test exception: comment this row
    }


    /**
     * converts the pdf file to image files, creates xml file with images list
     *   and saves images and xml in the table 'stb$jobtrail'
     * @param serverName server name for error messages
     * @param fileName pdf file to convert
     * @param bookmark bookmark name
     * @param extension  extension of images files
     * @param jobId current jobId for up/download of files
     * @param downlTransferNumber transfer number for downloading of pdf file from the database
     * @param downlConfigName  configuration name (in the table 'ISR$TRANSFER$FILE$CONFIG') for downloading
     * @param configResultName configuration name for uploading of images to the database
     * @param downlTransferKeyId  transferKeyId for downloading of pdf file from the database
     * @param keyDelimiter key delimiter (in the table 'ISR$TRANSFER$FILE$CONFIG') for uploading of images to the database
     * @param resolution  the horizontal and vertical resolution of images
     * @param scaling  scaling in percent
     * @param logLevel  job log level
     * @param logReference job log reference
     * @return null if converting was successful
     * @throws MessageStackException any error converted to MessageStackException
     */
    @WebMethod
    public String convertPdfToImages(String serverName,
                                     String fileName,
                                     String bookmark,
                                     String extension,
                                     String jobId,
                                     String downlTransferNumber,
                                     String downlConfigName,
                                     String configResultName,
                                     String downlTransferKeyId,
                                     String keyDelimiter,
                                     String resolution,
                                     String scaling,
                                     String logLevel,
                                     String logReference)
            throws MessageStackException {
        String currentName = "convertPdfToImages";
        String retStr = "";
        log = initLogger(logLevel, logReference);
        log.stat(METHOD_BEGIN);
        // for details of the node 'Transformation server'
        if (log.getLevel().isLessSpecificThan(Level.DEBUG)) {
            CaseInsensitiveMap<String, String> args = new CaseInsensitiveMap<>();
            args.put("serverName", serverName);
            args.put("fileName", fileName);
            args.put("bookmark", bookmark);
            args.put("logLevel", logLevel);
            args.put("logReference", logReference);
            createMethodNode(currentName, args);
        }
        String folderName = jobId + (StringUtils.isNotEmpty(downlTransferNumber) ? "_" + downlTransferNumber : "");
        File workFolder = new File(workFolderName, folderName);
        log.debug("workFolder==" + workFolder);
        DbFileSupplier fileWorker = new DbFileSupplier(workFolder, dbData);

        try {
            File pdfFile = fileWorker.downloadFile(fileName, workFolder.getAbsolutePath(), downlConfigName, downlTransferKeyId,
                    logLevel, logReference);
            log.debug(pdfFile);

            HashMap<Integer, File> imagesList = PdfToImage.convertPDFtoImageAllPages(fileName, workFolder, extension,
                    Integer.parseInt(resolution), Integer.parseInt(scaling));

            XmlHandler fileListXml = new XmlHandler();
            Element root = fileListXml.nodeRoot("pdf");
            root.setAttribute("bookmark", bookmark);

            for (Integer key : imagesList.keySet()) {
                File imageFile = imagesList.get(key);
                String uplTransferKeyId = jobId + keyDelimiter + imageFile.getName() + keyDelimiter + bookmark + keyDelimiter + "B";
                boolean isUploaded = fileWorker.uploadFile(imageFile, uplTransferKeyId, configResultName, logLevel, logReference);
                if (isUploaded) {
                    log.info("File " + imageFile.getName() + " uploaded successfully");
                    HashMap<String, String> attr = new HashMap<>();
                    attr.put("nr", String.valueOf(key));
                    attr.put("file", imageFile.getName());
                    fileListXml.createElement("page", null, attr, root);
                }
            }
            File xmlFile = ConvertUtils.bytesToFile(fileListXml.documentToByte(), new File(workFolder,
                    "IMAGES_LIST_" + bookmark + ".xml"));
            boolean isUploaded = fileWorker.uploadFile(xmlFile,
                    jobId + keyDelimiter + xmlFile.getName() + keyDelimiter + "IMAGES_LIST_" + bookmark,
                    "LOADER_CLOB_FILE", logLevel, logReference);

            if (isUploaded) {
                log.info("File " + xmlFile.getName() + " uploaded successfully");
                if (log.getLevel().intLevel() < Level.DEBUG.intLevel()) {
                    log.debug("clean workFolder: " + fileWorker.getWorkFolder());
                    clearFileWorker(fileWorker);
                }
            }

        } catch (Exception e) {
            String userStack = "error on the server '" + serverName + "' by get remote xml";
            log.error(userStack + "==" + e);
            onMethodFailed(e, MessageStackException.SEVERITY_CRITICAL, userStack, MessageStackException.getCurrentMethod(), statusXmlMethod);
        }
        log.debug(dbData);
        return retStr;
    }


    /*
     * initiates the working directory if it doesn't exists
     * if wd exists, deletes the wd with the data
     * */
    private boolean initWorkFolder() throws MessageStackException {
        File workFolder = new File(workFolderName);
        if (workFolder.exists()) {
            try {
                boolean isOk = FileSystemWorker.tryToRemoveFolder(workFolder, 5, 500);
                if (!isOk){
                    String warn = "Directory " + workFolder + " can not be deleted";
                    log.warn(warn);
                    throw new RuntimeException(warn);
                }
            } catch (Exception e) {
                String workFolderPath = workFolder.getAbsolutePath();
                String errorMsg = "Can not remove old work directory " + workFolderPath + System.lineSeparator() + " " + e;
                log.error("error==" + errorMsg);
                throw new MessageStackException(e, MessageStackException.SEVERITY_CRITICAL, errorMsg,
                        MessageStackException.getCurrentMethod());
            }
        }
        try {
            boolean isOk = FileSystemWorker.tryToCreateFolder(workFolderName, 5, 500);
            if (!isOk){
                String warn = "Directory " + workFolder + " can not be created";
                log.warn(warn);
                throw new RuntimeException(warn);
            }
        } catch (Exception e) {
            String workFolderPath = workFolder.getAbsolutePath();
            String errorMsg = "Can not create  work directory " + workFolderPath + " " + e;
            log.error("error==" + errorMsg);
            throw new MessageStackException(e, MessageStackException.SEVERITY_CRITICAL, errorMsg,
                    MessageStackException.getCurrentMethod());
        }

        return FileSystemWorker.checkFolder(workFolder);
    }


    private void clearFileWorker(FileSupplier fileWorker) {
        if (fileWorker != null) {
            try {
                fileWorker.clearFileSystem();
            } catch (MessageStackException e) {
                log.error(e);
            }
        }
    }


    public static void main(String[] args) {
        // byte[] inputXml = ConvertUtils.fileToBytes(System.getProperty("user.dir") + "\\input.xml");

        Properties connProps = new Properties();
//            connProps.load(new FileInputStream(System.getProperty("user.dir") + "\\dbConn.properties"));
        DbData dbData = new DbData("isrowner_isrc", "isrc-d-d19", "8082", "isrws_isrc", "isrws_isrc");
        PdfService service = new PdfService(dbData, "DEBUG");

        service.logLevel = "debug";
        try {
            service.init();
        } catch (MessageStackException e) {
            e.printStackTrace();
        }

        log.stat("begin");

        log.stat("end");

    }
}
