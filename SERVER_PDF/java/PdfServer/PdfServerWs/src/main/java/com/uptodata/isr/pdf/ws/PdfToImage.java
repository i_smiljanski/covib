package com.uptodata.isr.pdf.ws;

import com.aspose.pdf.Document;
import com.aspose.pdf.License;
import com.aspose.pdf.devices.ImageDevice;
import com.aspose.pdf.devices.JpegDevice;
import com.aspose.pdf.devices.PngDevice;
import com.aspose.pdf.devices.Resolution;
import com.aspose.pdf.facades.PdfFileEditor;

import java.io.File;
import java.io.InputStream;
import java.util.HashMap;


/**
 * Created by maurere108 on 12.08.2021.
 */
public class PdfToImage {

    public static void main(String[] args) {
        System.out.println("System.getProperty(\"user.dir\")= " + System.getProperty("user.dir"));
        String fileName = "input.pdf";
        File workFolder = new File(System.getProperty("user.dir"), "testPdf");
        String device = "jpg";
        int resolution = 100;
        int scale = 150;
        try {
            convertPDFtoImageAllPages(fileName, workFolder, device, resolution, scale);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    /**
     * converts the pdf file to image files with resolution "resolution_int". Pdf file is scaled first
     * @param fileName  pdf file to convert
     * @param workFolder  working directory for all files during the conversion
     * @param device    image files extension
     * @param resolution_int value which represents the horizontal and vertical resolution
     * @param scale scaling in percent
     * @return  HashMap with all images assigned according to page count
     */
    public static HashMap<Integer, File> convertPDFtoImageAllPages(String fileName, File workFolder, String device,
                                                                   int resolution_int, int scale) throws RuntimeException{
        try {
            loadLicence();
        } catch (Exception e) {
            throw new RuntimeException(e);
        }

        Document pdfDocument = scale(fileName, workFolder, scale);
        HashMap<Integer, File> imagesList = new HashMap<>();

        // Loop through all the pages of PDF file
        for (int pageCount = 1; pageCount <= pdfDocument.getPages().size(); pageCount++) {
            String imageName = fileName.substring(0, fileName.lastIndexOf(".pdf")) + "_" + pageCount + "." + device;
            File outputFile = new File(workFolder, imageName);
            // Create Resolution object
            Resolution resolution = new Resolution(resolution_int);

            // Create Device object with particular resolution
            ImageDevice imageDevice = new PngDevice(resolution);
            switch (device) {
                case "png":
                    imageDevice = new PngDevice(resolution);
                    break;
                case "jpg":
                    imageDevice = new JpegDevice(resolution);
                    break;
            }

            // Convert a particular page and save the image to file
            imagesList.put(pageCount, outputFile);
            imageDevice.process(pdfDocument.getPages().get_Item(pageCount), outputFile.getAbsolutePath());

        }
        return imagesList;
    }

    /**
     *
     * @param fileName  pdf file to convert
     * @param workFolder  working directory for all files during the conversion
     * @param scale scaling in percent
     * @return scaled pdf document
     */
    public static Document scale(String fileName, File workFolder, int scale)  {
        // Open document
        String path = workFolder.getAbsolutePath() + "\\" + fileName;
        Document pdfDocument = new Document(path);

        PdfFileEditor fileEditor = new PdfFileEditor();
        // Specify Parameter to be used for resizing
        PdfFileEditor.ContentsResizeParameters parameters = new PdfFileEditor.ContentsResizeParameters(
                //leftMargin:
                PdfFileEditor.ContentsResizeValue.units(0),
                // contentsWidth:
                PdfFileEditor.ContentsResizeValue.percents(scale),
                //rightMargin:
                PdfFileEditor.ContentsResizeValue.units(0), PdfFileEditor.ContentsResizeValue.units(0),
                // new contents height is calculated automatically (similar to width)
                PdfFileEditor.ContentsResizeValue.percents(scale),
                // bottom margin is 10%
                PdfFileEditor.ContentsResizeValue.units(0));
        // Resize Page Contents
        fileEditor.resizeContents(pdfDocument, parameters);

        return pdfDocument;
    }


    protected static void loadLicence() throws Exception {
        ClassLoader loader = Thread.currentThread().getContextClassLoader();
        InputStream licenseFile = loader.getResourceAsStream("Aspose.Pdf.lic");
        if ((licenseFile != null ? licenseFile.available() : 0) > 0) {
            License license = new License();
            license.setLicense(licenseFile);
        }
    }

}
