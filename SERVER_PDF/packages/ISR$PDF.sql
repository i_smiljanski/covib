CREATE OR REPLACE package isr$pdf
is

-- ***************************************************************************************************
-- Description/Usage:
-- The package contains the functionality for handling PDF files on the PDF server
-- ***************************************************************************************************


FUNCTION convertAttachments(cnJobid in NUMBER) RETURN STB$OERROR$RECORD;
-- ***************************************************************************************************
-- Date and Autor: 24.Aug 2011, IS
-- this function is called by job.
-- converts the pdf files to image files and saves them in stb$jobtrail 
-- prepares xml input file with parameters and calls 'convertPdfToImages' 
--
-- Parameter:
-- cnJobid   the identifier of the iStudyReporter job
--
-- Return:
-- STB$OERROR$RECORD
--
-- ***************************************************************************************************

function getConversionProperties(csPropertyName in varchar2, cnDocid in number) return varchar2;
--******************************************************************************

function convertPdfToImages( csInputXml in clob, csLogLevel in varchar2, csLogReference  in varchar2, cnTimeout in number )  return clob;
-- ***************************************************************************************************
-- Date und Autor: 19. Aug 2021, is
-- calls the function 'convertPdfToImages' on the PDF server that
-- converts the pdf file to image files and saves them in stb$jobtrail 
--
-- Parameter:
-- csInputXml    xml file with necessary input informations: host, port, jobid, file name, extension, bookmark name, etc.
-- csLogLevel    log level for logging to isr$log
-- csLogReference  log reference for logging to isr$log
-- cnTimeout   pdf sever timeout
--
-- Return: exception as string
end isr$pdf;