CREATE OR REPLACE package body isr$pdf
is

function convertAttachments( cnJobid in number)
  return STB$OERROR$RECORD
is
  exPdfServerNotFound     exception;
  exPdfFileNotExists      exception;
  sCurrentName   constant varchar2(100) := $$PLSQL_UNIT||'.convertAttachments (JobID='||cnJobId||')';
  sUserMsg                varchar2(4000);
  oErrorObj               STB$OERROR$RECORD :=STB$OERROR$RECORD();
  sRemoteEx                varchar2(250);
  cnTransferNumber  number := 0;
  xInputXml             xmltype;

  
  cursor cGetPdfServer is
  select observer_host, port, context, timeout, alias
  from isr$serverobserver$v
  where servertypeid = 13 and nvl(runstatus, 'F') = 'T';

  rGetPdfServer cGetPdfServer%rowtype;
  
  cursor cGetInputXml(csFilename in varchar2, cnDocid in number, csBookmark in varchar2) is
    select XMLELEMENT (
         "convertPdf"
       , XMLELEMENT ("jobId", cnJobid)
       , XMLELEMENT ("serverName", rGetPdfServer.alias)
       , XMLELEMENT ("transferNumber", cnTransferNumber)
       , XMLELEMENT ("keyDelimiter", isr$file$transfer$service.getKeyDelimiter('PDF_CONVERT_RESULT'))
       , XMLELEMENT ("host", rGetPdfServer.observer_host)
       , XMLELEMENT ("port", rGetPdfServer.port)
       , XMLELEMENT ("context", rGetPdfServer.context)
       , XMLELEMENT ("extension", isr$pdf.getConversionProperties('ATTACHMENT_CONVERSION_TYPE', cnDocid))
       , XMLELEMENT ("scaling", isr$pdf.getConversionProperties('ATTACHMENT_CONVERSION_SCALING', cnDocid))
       , XMLELEMENT ("resolution", isr$pdf.getConversionProperties('ATTACHMENT_CONVERSION_RESOLUTION', cnDocid))
       , XMLELEMENT ("filename", csFilename)
       , XMLELEMENT ("bookmark", csBookmark)       
       , XMLELEMENT ("transfer_input_config", 'PDF_CONVERT_INPUT')
       , XMLELEMENT ("transfer_result_config", 'PDF_CONVERT_RESULT')
       , XMLELEMENT ("transfer_key", cnDocid)
       ) configXml
     from  dual;
     
  cursor cGetPdfs is  
     select d.docid, d.filename, p.value
     from stb$doctrail d, isr$parameter$values p
    where   p.entity='DOCDEFINITION'
      and   d.doc_definition(+) = p.value
      and   d.doctype(+) = 'C'
      and   d.nodeid(+) = STB$JOB.getJobParameter('REPID', cnJobid)
      and exists (select 1
                  from isr$stylesheet s, isr$output$transform ot
                 where ot.outputid = STB$JOB.getJobParameter ('OUTPUTID', cnJobid)
                   and ot.stylesheetid = s.STYLESHEETID
                  and upper(bookmarkname) = 'S_PDF')
      and dbms_lob.getLength(d.content) > 0;
      
  rGetPdfs    cGetPdfs%rowtype;
     
  
         
begin
  isr$trace.stat('begin', 'Function '||sCurrentName||' started.', sCurrentName); 
  
  open cGetPdfs;
  fetch cGetPdfs into rGetPdfs;
  
  if cGetPdfs%notfound then
    close cGetPdfs;
    sUserMsg :=  Utd$msglib.GetMsg('exPdfFileNotExists',Stb$security.GetCurrentLanguage);
    raise exPdfFileNotExists;
  end if;  
  
  close cGetPdfs;
  
  open cGetPdfServer;
  fetch cGetPdfServer into rGetPdfServer;
  
  if cGetPdfServer%notfound then
    close cGetPdfServer;
    sUserMsg :=  Utd$msglib.GetMsg('exPdfServerNotFoundText',Stb$security.GetCurrentLanguage);
    raise exPdfServerNotFound;
  end if; 
  
  for rGetPdfs1 in cGetPdfs loop
    cnTransferNumber := cnTransferNumber + 1;
    open cGetInputXml(rGetPdfs1.filename, rGetPdfs1.docid, rGetPdfs1.value);
    fetch cGetInputXml into xInputXml;
    
    if cGetInputXml%found then
      isr$trace.debug('xInputXml','s. logclob', sCurrentName, xInputXml);
      
      sRemoteEx := convertPdfToImages( xInputXml.getClobval(), isr$trace.getJavaUserLoglevelStr, isr$trace.getReference, rGetPdfServer.timeout);
      
      if sRemoteEx is not null then
       isr$trace.error('transform error', 'error ', sCurrentName, sRemoteEx);
       oErrorObj.handleJavaError( Stb$typedef.cnSeverityCritical,  sRemoteEx, sCurrentName );
      end if;

      update stb$jobtrail set blobflag = 'C' where jobid = cnJobid and documenttype = 'IMAGES_LIST_' || rGetPdfs1.value;
      
      oErrorObj := isr$transform.callJavaTransformForJob(cnJobid, 'IMAGES_LIST_' || rGetPdfs1.value, null, 
                                'ATTACHMENTHTML', 'TRANSFORM_STYLESHEET', rGetPdfs1.value || '.HTM');
      
    end if;
    close cGetInputXml;
  end loop;
  close cGetPdfServer;
  
  /*
  for rGetImagesXml in (select xmlelement("pdfs", xmlagg(xmltype(textfile))).getClobVal() 
          as xmlList from stb$jobtrail where jobid=cnJobid and documenttype='IMAGES_LIST') loop
    insert into stb$jobtrail(jobid, filename, textfile, documenttype) 
         values(cnJobid, 'images.xml', rGetImagesXml.xmlList, 'ALL_IMAGES_LIST');
    commit;
  end loop;
  --transform XML list of images to S_PDF.HTM
  oErrorObj := isr$transform.callJavaTransformForJob(cnJobid, 'ALL_IMAGES_LIST', null, 'ATTACHMENTHTML');
*/
  isr$trace.stat('end', 'Function '||sCurrentName||' finished',sCurrentName);
  return oErrorObj;

exception
  when exPdfFileNotExists then
    oErrorObj.handleError ( Stb$typedef.cnSeverityMessage, sUserMsg, sCurrentName, 
          dbms_utility.format_error_stack ||chr(10)|| dbms_utility.format_error_backtrace );
    return oErrorObj;
  when exPdfServerNotFound then
    oErrorObj.handleError ( Stb$typedef.cnSeverityCritical, sUserMsg, sCurrentName, 
          dbms_utility.format_error_stack ||chr(10)|| dbms_utility.format_error_backtrace );
    return oErrorObj;
  when others then
    sUserMsg := utd$msglib.getmsg ('exUnexpectedError', stb$security.getCurrentLanguage,  csP1 => sqlerrm(sqlcode));
    oErrorObj.handleError ( Stb$typedef.cnSeverityCritical, sUserMsg, sCurrentName,  dbms_utility.format_error_stack ||chr(10)|| dbms_utility.format_error_backtrace  );
    return oErrorObj;
end convertAttachments;


--***********************************************************************************************************************************
function getConversionProperties(csPropertyName in varchar2, cnDocid in number) return varchar2
is
  sCurrentName   constant varchar2(100) := $$PLSQL_UNIT||'.getConversionProperties ('||csPropertyName||')';
  oErrorObj               STB$OERROR$RECORD :=STB$OERROR$RECORD();
  csPropertyValue    isr$doc$property.parametervalue%type;
begin 
  for rGetValue in (SELECT parametervalue FROM isr$doc$property WHERE  docid = cnDocid  AND parametername = csPropertyName) loop
    csPropertyValue := rGetValue.parametervalue;
  end loop;
  return nvl(csPropertyValue, STB$UTIL.getSystemParameter(csPropertyName));
exception
  when others then
    oErrorObj.handleError ( Stb$typedef.cnSeverityCritical, utd$msglib.getmsg ('exUnexpectedError', stb$security.getCurrentLanguage,  csP1 => sqlerrm(sqlcode)),
       sCurrentName,  dbms_utility.format_error_stack ||chr(10)|| dbms_utility.format_error_backtrace  );
    return null;
end getConversionProperties;

--***********************************************************************************************************************************
function convertPdfToImages( csInputXml in clob, csLogLevel in varchar2, csLogReference  in varchar2, cnTimeout in number )  return clob
as language java
name 'com.uptodata.isr.db.pdf.PdfDbClient.convertPdfToImages( oracle.sql.CLOB, java.lang.String, java.lang.String, int) return oracle.sql.CLOB';
  
     
end isr$pdf;