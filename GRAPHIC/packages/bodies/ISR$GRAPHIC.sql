CREATE OR REPLACE PACKAGE BODY isr$graphic
IS
 cursor cGetServer is
 select  observer_host,   port,  timeout, alias
  from  isr$serverobserver$v
  where servertypeid = 11 and nvl(runstatus, 'F') = 'T';

 rGetServer                 cGetServer%rowtype;
 exGraphicServerNotRunning  exception;
 sDelimiter                 isr$transfer$file$config.key_delimiter%type;

--******************************************************************************************************************************
function createGraphics( cnJobid in     number,  cxXml   in out xmltype )  return STB$OERROR$RECORD
is
  oErrorObj       STB$OERROR$RECORD := STB$OERROR$RECORD();
  sCurrentName    constant varchar2 (100) := $$PLSQL_UNIT || '.createGraphics';

  cXml    clob;
 -- sChartCalcName  isr$stylesheet.filename%type;

  cursor cGetStylesheet(cnOrdernum in number) is
  select  stylesheetid
  from isr$transform$v
  where  jobid = cnJobid
  and  executionorder = cnOrdernum ;
  rGetStylesheet  cGetStylesheet%rowtype;

  cursor cGetTextfile(csReference in varchar2) is
  select textfile
  from stb$jobtrail
  where jobid = cnJobid
  and reference = csReference;

  procedure closeCursor is
    begin
      if cGetStylesheet%isopen then
        close cGetStylesheet;
      end if;
      if cGetTextfile%isopen then
        close cGetTextfile;
      end if;
    end closeCursor;

  function exitFunction return STB$OERROR$RECORD is
    begin
      closeCursor;
      isr$trace.stat('end','end', sCurrentName);
      return oErrorObj;
    end exitFunction;

begin
  isr$trace.stat('begin','begin', sCurrentName);

  open  cGetStylesheet (1);
  fetch cGetStylesheet into rGetStylesheet;  -- find reference for stb$jobtrail

  if cGetStylesheet%notfound then
    isr$trace.info('calculcation xml','there is no calculcation xml for this report type', sCurrentName);
    isr$trace.stat('end', 'end', sCurrentName);
    return exitFunction;
  end if;
  -- 1. transform rowdata with chartcalc.xsl -> chartcalc.xml (executionorder = 1)
  oErrorObj := isr$transform.callJavaTransformForJob(cnJobid, 'ROHDATEN', 1);
  if oErrorObj.sseveritycode != Stb$typedef.cnNoError then
    isr$trace.stat('end', 'end', sCurrentName);
    return exitFunction;
  end if;
  isr$trace.debug('end transform rowdata to chartcalc xml','successefully', sCurrentName);

  -- 2. create calculation for chartcalc.xml  -> calcresult.xml
  open  cGetTextfile (rGetStylesheet.stylesheetid);
  fetch cGetTextfile into cXml;  -- find chartcalc.xml
  if cGetTextfile%notfound then
    isr$trace.info('chartcalc xml','there is no chartcalc xml for this report type', sCurrentName);
    isr$trace.stat('end', 'end', sCurrentName);
    return exitFunction;
  end if;
  oErrorObj := isr$graphic.createCalculations(cnJobid, cXml, rGetStylesheet.stylesheetid);
  if oErrorObj.sseveritycode != Stb$typedef.cnNoError then
    isr$trace.stat('end', 'end', sCurrentName);
    return exitFunction;
  end if;
  isr$trace.debug('end create calculation  for  chartcalc.xml','successefully', sCurrentName);
  closeCursor;

  -- 3. transform calcresult.xml with chartjob.xsl  -> chartjob.xml (executionorder = 2)
  oErrorObj := isr$transform.callJavaTransformForJob(cnJobid, 'CALCRESULT', 2, 'CHARTRESULT');
  isr$trace.debug('end transform calcresult.xml','successefully', sCurrentName);

  -- 4. create plots for chartjob.xml  -> graphics
  open  cGetStylesheet (2);
  fetch cGetStylesheet into rGetStylesheet;  -- find reference for stb$jobtrail
  if cGetStylesheet%notfound then
    isr$trace.info('plot xml','there are no plot xml for this report type', sCurrentName);
    isr$trace.stat('end', 'end', sCurrentName);
    return exitFunction;
  else
    open  cGetTextfile (rGetStylesheet.stylesheetid);
    fetch cGetTextfile into cXml;  -- find chartjob.xml

    if cGetTextfile%notfound then
      isr$trace.info('chartjob xml','there is no chartjob xml for this report type', sCurrentName);
      isr$trace.stat('end', 'end', sCurrentName);
      return exitFunction;
    end if;

  end if;
  oErrorObj := isr$graphic.createPlots(cnJobid, cXml, rGetStylesheet.stylesheetid);
  if oErrorObj.sseveritycode != Stb$typedef.cnNoError then
    isr$trace.stat('end', 'end', sCurrentName);
    return exitFunction;
  end if;
  isr$trace.debug('end create plots for chartjob.xml','successefully', sCurrentName);
  -- 5. transform chartjob.xml with charthtml.xml  -> S_GRAPHICTREND.HTM (executionorder = 3, ..)
  for rGetExecutionOrder in (select executionorder from isr$output$transform
                              where outputid = STB$JOB.getJobParameter ('OUTPUTID', cnJobid)
                              and executionorder >= 3
                              order by executionorder) loop
    oErrorObj := isr$transform.callJavaTransformForJob(cnJobid, 'CHARTRESULT', rGetExecutionOrder.executionorder);
  end loop;
  isr$trace.stat('end', 'end', sCurrentName);
  return exitFunction;
end createGraphics;
--******************************************************************************************************************************
FUNCTION calculation_fTest(clChartCalcResultXml IN OUT CLOB, cnJobId IN NUMBER) RETURN STB$OERROR$RECORD IS

  oErrorObj             STB$OERROR$RECORD := STB$OERROR$RECORD();
  xXml                  XMLTYPE;

  sCurrentName constant varchar2(4000) := $$PLSQL_UNIT||'.calculation_fTest('||cnJobId||')';
  sMsg                  varchar2(4000);

  nCountCalcs           NUMBER;  -- Number of different calculations, e.g. this is the number of components

  sPoolable             VARCHAR2(4000);

  nSlope                NUMBER;
  nIntercept            NUMBER;

  nK                    NUMBER;
  nSxxW                 NUMBER;
  nSyyW                 NUMBER;
  nSxyW                 NUMBER;
  nSSE                  NUMBER;
  nSSEW                 NUMBER;
  nSSm                  NUMBER;
  nN                    NUMBER;
  nMSm                  NUMBER;
  nMSE                  NUMBER;
  nFm                   NUMBER;
  alpha                 NUMBER;
  nfInv                 NUMBER;
  nnbatch               NUMBER;

  nSxxT                 NUMBER;
  nSyyT                 NUMBER;
  nSxyT                 NUMBER;
  nSST                  NUMBER;
  nSSB                  NUMBER;
  nMSB                  NUMBER;
  nFn                   NUMBER;

  sCalcHash             VARCHAR2(4000);

  clPiece               clob;
  sPath                 varchar2(2000);

  nSingleXVar           NUMBER;
  nSingleYVar           NUMBER;
  nSingleXYVar          NUMBER;
  nSingleSumSqrRes      NUMBER;
  nCommonXVar           NUMBER;
  nCommonYVar           NUMBER;
  nCommonXYVar          NUMBER;
  nCalcNr               NUMBER;

BEGIN
  isr$trace.stat('begin', 'begin', sCurrentName);

  -- Load the CLOB into the DOM
  xXml := ISR$XML.ClobToXml (clChartCalcResultXml);

  nCountCalcs := ISR$XML.ValueOf(xXml, 'count(/calcjob/calculationtrend/graph)');
  isr$trace.info('nCountCalcs', nCountCalcs, sCurrentName);

  FOR rGetCommonValues IN ( SELECT EXTRACTVALUE (COLUMN_VALUE, '/graph/@hash') sCalcHash
                                 , (select count(*) from TABLE(XMLSEQUENCE(EXTRACT (COLUMN_VALUE, '/graph/attributes/datasets/cluster')))) nK
                                 , EXTRACTVALUE (COLUMN_VALUE, '/graph/linearRegressionTask/result/basic/regression/@slope') nSlope
                                 , EXTRACTVALUE (COLUMN_VALUE, '/graph/linearRegressionTask/result/basic/regression/@intercept') nIntercept
                                 , EXTRACTVALUE (COLUMN_VALUE, '/graph/linearRegressionTask/result/basic/@xVariance') nCommonXVar
                                 , EXTRACTVALUE (COLUMN_VALUE, '/graph/linearRegressionTask/result/basic/@yVariance') nCommonYVar
                                 , EXTRACTVALUE (COLUMN_VALUE, '/graph/linearRegressionTask/result/basic/@xyVariance') nCommonXYVar
                                 , rownum nCalcNr
                              FROM TABLE(XMLSEQUENCE(EXTRACT (
                                                        xXml
                                                      , '/calcjob/calculationtrend/graph'
                                                     ))) t) LOOP
    sCalcHash := rGetCommonValues.sCalcHash;
    nK := rGetCommonValues.nK;
    nSlope := NVL(rGetCommonValues.nSlope, 0);
    nIntercept := NVL(rGetCommonValues.nIntercept, 0);
    nCommonXVar := rGetCommonValues.nCommonXVar;
    nCommonYVar := rGetCommonValues.nCommonYVar;
    nCommonXYVar := rGetCommonValues.nCommonXYVar;
    nCalcNr := rGetCommonValues.nCalcNr;
    isr$trace.debug('sCalcHash', sCalcHash, sCurrentName);
    isr$trace.debug('nK', nK, sCurrentName);
    isr$trace.debug('nSlope', nSlope, sCurrentName);
    isr$trace.debug('nIntercept', nIntercept, sCurrentName);
    isr$trace.debug('nCommonXVar', nCommonXVar, sCurrentName);
    isr$trace.debug('nCommonYVar', nCommonYVar, sCurrentName);
    isr$trace.debug('nCommonXYVar', nCommonXYVar, sCurrentName);
    isr$trace.debug('nCalcNr', nCalcNr, sCurrentName);

    STB$JOB.setProgress(UTD$MSGLIB.getMsg('GRAPHICSTEXT', STB$SECURITY.getCurrentLanguage)||' '||UTD$MSGLIB.getMsg('CALC_FTEST', STB$SECURITY.getCurrentLanguage)||' '||UTD$MSGLIB.getMsg('GRAPHICNO', STB$SECURITY.getCurrentLanguage)||' '||nCalcNr||' '||UTD$MSGLIB.getMsg('GRAPHICCNT', STB$SECURITY.getCurrentLanguage)||' '||nCountCalcs, cnJobid);

    -- Null all summation values
    nSxxW   := 0;
    nSyyW   := 0;
    nSxyW   := 0;
    nSSE    := 0;
    nN      := 0;

    ISR$XML.CreateNode   (xXml, '/calcjob/calculationtrend/graph[@hash="' || sCalcHash || '"]', 'Poolability', '');

    IF nK > 1 THEN

      FOR rGetSingleValues IN ( SELECT EXTRACTVALUE (COLUMN_VALUE, '/result/@n') nnbatch
                                     , EXTRACTVALUE (COLUMN_VALUE, '/result/basic/@xVariance') nSingleXVar
                                     , EXTRACTVALUE (COLUMN_VALUE, '/result/basic/@yVariance') nSingleYVar
                                     , EXTRACTVALUE (COLUMN_VALUE, '/result/basic/@xyVariance') nSingleXYVar
                                     , EXTRACTVALUE (COLUMN_VALUE, '/result/basic/regression/@sumSqrResidual') nSingleSumSqrRes
                                  FROM TABLE(XMLSEQUENCE(EXTRACT (
                                                            xXml
                                                          , '/calcjob/calculationtrendsingle/graph[@hash=/calcjob/calculationtrend/graph[@hash="'||sCalcHash||'"]/attributes/datasets/cluster/referencehash]/linearRegressionTask/result'
                                                         ))) t) LOOP

        nnbatch := rGetSingleValues.nnbatch;
        nSingleXVar := rGetSingleValues.nSingleXVar;
        nSingleYVar := rGetSingleValues.nSingleYVar;
        nSingleXYVar := rGetSingleValues.nSingleXYVar;
        nSingleSumSqrRes := rGetSingleValues.nSingleSumSqrRes;

        isr$trace.debug('nnbatch', nnbatch, sCurrentName);
        isr$trace.debug('nSingleXVar', nSingleXVar, sCurrentName);
        isr$trace.debug('nSingleYVar', nSingleYVar, sCurrentName);
        isr$trace.debug('nSingleXYVar', nSingleXYVar, sCurrentName);
        isr$trace.debug('nSingleSumSqrRes', nSingleSumSqrRes, sCurrentName);

        -- Sum of x/y/xy Variance Sxx(W), Syy(W), Sxy(W) for the single graphs (each multiplied with nn)
        nSxxW := nSxxW + ( nnbatch * nSingleXVar );
        isr$trace.debug('nSxxW', nSxxW, sCurrentName);
        nSyyW := nSyyW + ( nnbatch * nSingleYVar );
        isr$trace.debug('nSyyW', nSyyW, sCurrentName);
        nSxyW := nSxyW + ( nnbatch * nSingleXYVar );
        isr$trace.debug('nSxyW', nSxyW, sCurrentName);

        -- Sum of sumSqrResidual for each graph
        nSSE := nSSE + nvl(trim(nSingleSumSqrRes),0);
        isr$trace.debug('nSSE', nSSE, sCurrentName);

        -- Sum the datapoints
        nN := nN + nnbatch;
        isr$trace.debug('nN', nN, sCurrentName);

      END LOOP;

      IF nSlope = 0 OR nIntercept = 0 THEN
        sPoolable := 'false';
      ELSE

        IF nN-(2*nK) <= 0 THEN
          sPoolable := 'false';
        ELSE

          -- SSE(W) = Syy(W) - (Sxy(W))^2 / Sxx(W)
          IF nSxxW != 0 THEN
            nSSEW := nSyyW - (nSxyW * nSxyW) / nSxxW;
          ELSE
            nSSEW := null;
          END IF;
          isr$trace.debug('nSSEW', nSSEW, sCurrentName);

          -- SS(m) = SSE(W) - SSE
          nSSm := nSSEW - nSSE;
          isr$trace.debug('nSSm', nSSm, sCurrentName);

          -- MS(m) = SS(m) / (K-1)
          nMSm := nSSm / (nK - 1);
          isr$trace.debug('nMSm', nMSm, sCurrentName);

          -- MSE   = SSE / (n5K)
          nMSE := nSSE / (nN - (2 * nK));
          isr$trace.debug('nMSE', nMSE, sCurrentName);

          -- Fvalue: F(m) = MS(m) / MSE
          IF nMSE != 0 THEN
            nFm := nMSm / nMSE;
          ELSE
            nFm := null;
          END IF;
          isr$trace.debug('nFm', nFm, sCurrentName);

          alpha := to_number(STB$UTIL.getSystemparameter('ALPHA_FTEST'));

          -- Ftest
          isr$trace.debug('alpha', alpha, sCurrentName);
          isr$trace.debug('nN-(2*nK)', nN-(2*nK), sCurrentName);
          isr$trace.debug('nK-1', nK-1, sCurrentName);
          IF (nN-(2*nK)) IS NOT NULL THEN
            nfInv := Stb$java.fInv(alpha, nN-(2*nK), nK-1 );
          END IF;
          isr$trace.debug('nfInv', nfInv, sCurrentName);

          -- Output all values (for testing only)
          ISR$XML.CreateNode   (xXml, '/calcjob/calculationtrend/graph[@hash="' || sCalcHash || '"]/Poolability', 'nK',    nK);
          ISR$XML.CreateNode   (xXml, '/calcjob/calculationtrend/graph[@hash="' || sCalcHash || '"]/Poolability', 'nSxxW', nSxxW);
          ISR$XML.CreateNode   (xXml, '/calcjob/calculationtrend/graph[@hash="' || sCalcHash || '"]/Poolability', 'nSyyW', nSyyW);
          ISR$XML.CreateNode   (xXml, '/calcjob/calculationtrend/graph[@hash="' || sCalcHash || '"]/Poolability', 'nSxyW', nSxyW);
          ISR$XML.CreateNode   (xXml, '/calcjob/calculationtrend/graph[@hash="' || sCalcHash || '"]/Poolability', 'nSSE',  nSSE);
          ISR$XML.CreateNode   (xXml, '/calcjob/calculationtrend/graph[@hash="' || sCalcHash || '"]/Poolability', 'nSSEW', nSSEW);
          ISR$XML.CreateNode   (xXml, '/calcjob/calculationtrend/graph[@hash="' || sCalcHash || '"]/Poolability', 'nSSm',  nSSm);
          ISR$XML.CreateNode   (xXml, '/calcjob/calculationtrend/graph[@hash="' || sCalcHash || '"]/Poolability', 'nN',    nN);
          ISR$XML.CreateNode   (xXml, '/calcjob/calculationtrend/graph[@hash="' || sCalcHash || '"]/Poolability', 'nMSm',  nMSm);
          ISR$XML.CreateNode   (xXml, '/calcjob/calculationtrend/graph[@hash="' || sCalcHash || '"]/Poolability', 'nMSE',  nMSE);
          ISR$XML.CreateNode   (xXml, '/calcjob/calculationtrend/graph[@hash="' || sCalcHash || '"]/Poolability', 'nFm',   nFm);
          ISR$XML.CreateNode   (xXml, '/calcjob/calculationtrend/graph[@hash="' || sCalcHash || '"]/Poolability', 'alpha', alpha);
          ISR$XML.CreateNode   (xXml, '/calcjob/calculationtrend/graph[@hash="' || sCalcHash || '"]/Poolability', 'nfInv', nfInv);

          -- nFm <= nfInv    -> true may be poolable, needs more testing; false not poolable
          IF nFm <= nfInv THEN

            ISR$XML.CreateNode   (xXml, '/calcjob/calculationtrend/graph[@hash="' || sCalcHash || '"]/Poolability', 'commonSlope',  'true');

            -- x/y/xy Variance SXX(T), SYY(T), SXY(T) from overall graph
            nSxxT := nN * nCommonXVar;
            isr$trace.debug('nSxxT', nSxxT, sCurrentName);
            nSyyT := nN * nCommonYVar;
            isr$trace.debug('nSyyT',nSyyT, sCurrentName);
            nSxyT := nN * nCommonXYVar;
            isr$trace.debug('nSxyT', nSxyT, sCurrentName);

            -- SST = Syy(T) - (Sxy(T))² / Sxx(T)
            IF nSxxT != 0 THEN
              nSST := nSyyT - (nSxyT * nSxyT) / nSxxT;
            ELSE
              nSST := null;
            END IF;
            isr$trace.debug('nSST', nSST, sCurrentName);

            -- SSB = SST - SSE(W)
            nSSB := nSST - nSSEW;
            isr$trace.debug('nSSB', nSSB, sCurrentName);

            -- MSB   = SSB / (K-1)
            IF (nK - 1) != 0 THEN
              nMSB := nSSB / (nK - 1);
            ELSE
              nMSB := null;
            END IF;
            isr$trace.debug('nMSB', nMSB, sCurrentName);

            -- Fvalue: F(n) = MSB / MSE
            IF nMSE != 0 THEN
              nFn := nMSB / nMSE;
            ELSE
              nFn := null;
            END IF;
            isr$trace.debug('nFn', nFn, sCurrentName);

            -- Output all values (for testing only)
            ISR$XML.CreateNode   (xXml, '/calcjob/calculationtrend/graph[@hash="' || sCalcHash || '"]/Poolability', 'nSxxT',  nSxxT);
            ISR$XML.CreateNode   (xXml, '/calcjob/calculationtrend/graph[@hash="' || sCalcHash || '"]/Poolability', 'nSyyT',  nSyyT);
            ISR$XML.CreateNode   (xXml, '/calcjob/calculationtrend/graph[@hash="' || sCalcHash || '"]/Poolability', 'nSxyT',  nSxyT);
            ISR$XML.CreateNode   (xXml, '/calcjob/calculationtrend/graph[@hash="' || sCalcHash || '"]/Poolability', 'nSST',   nSST);
            ISR$XML.CreateNode   (xXml, '/calcjob/calculationtrend/graph[@hash="' || sCalcHash || '"]/Poolability', 'nSSB',   nSSB);
            ISR$XML.CreateNode   (xXml, '/calcjob/calculationtrend/graph[@hash="' || sCalcHash || '"]/Poolability', 'nMSB',   nMSB);
            ISR$XML.CreateNode   (xXml, '/calcjob/calculationtrend/graph[@hash="' || sCalcHash || '"]/Poolability', 'nFn',    nFn);

            -- nFn <= nfInv    -> true really poolable, false not poolable
            IF nFn <= nfInv THEN
              ISR$XML.CreateNode   (xXml, '/calcjob/calculationtrend/graph[@hash="' || sCalcHash || '"]/Poolability', 'commonIntercept',  'true');
              sPoolable := 'true';
              isr$trace.debug('sPoolable', sPoolable, sCurrentName);
            ELSE
              sPoolable := 'false';
              ISR$XML.CreateNode   (xXml, '/calcjob/calculationtrend/graph[@hash="' || sCalcHash || '"]/Poolability', 'commonIntercept',  'false');
              isr$trace.debug('sPoolable', sPoolable, sCurrentName);

              IF NVL(STB$UTIL.getReptypeParameter(STB$JOB.getJobParameter('REPORTTYPEID',cnJobId), 'USE_CSSI_MODEL'), 'F') = 'T' THEN
                -- create intersection caluclation with the model CSSI common slope separate intercept
                -- replace the slopes of the single plots with the common slope
                isr$trace.debug('use CSSI model', sCalcHash,sCurrentName);
                declare
                  sReferenceHash varchar2(200);
                begin
                  FOR i IN 1..ISR$XML.valueOf(xXml, 'count(/calcjob/calculationtrend/graph[@hash="' || sCalcHash || '"]/attributes/datasets/cluster)') LOOP
                    sReferenceHash := ISR$XML.valueOf(xXml, '(/calcjob/calculationtrend/graph[@hash="' || sCalcHash || '"]/attributes/datasets/cluster)['||i||']/referencehash/text()');
                    isr$trace.debug('sReferenceHash', sReferenceHash, sCurrentName);
                    sPath := '/calcjob/calculationtrendsingle/graph[@hash="'||sReferenceHash||'"]/linearRegressionTask';
                    -- sPath := '/calcjob/calculationtrendsingle/graph[@hash="'||ISR$XML.valueOf(xXml, '(/calcjob/calculationtrend/graph[@hash="' || sCalcHash || '"]/attributes/datasets/cluster)['||i||']/referencehash')||'"]/linearRegressionTask';
                    ISR$XML.setAttribute (xXml, sPath||'/result/basic/regression', 'slope', nSlope);
                    clPiece := ISR$XML.ValueAsClob(xXml, sPath);
                    isr$trace.debug('XML piece', 'see lobclob -->>', sCurrentName, clPiece);

                  END LOOP;

                end;
              END IF;

            END IF;

          ELSE
            ISR$XML.CreateNode   (xXml, '/calcjob/calculationtrend/graph[@hash="' || sCalcHash || '"]/Poolability', 'commonSlope',  'false');
            sPoolable := 'false';
            isr$trace.debug('sPoolable', sPoolable, sCurrentName);
          END IF;

        END IF;

      END IF;

    ELSE
      sPoolable := 'false';
      isr$trace.debug('sPoolable',sPoolable, sCurrentName);
    END IF;

    -- Output the status
    ISR$XML.SetAttribute (xXml, '/calcjob/calculationtrend/graph[@hash="' || sCalcHash || '"]/Poolability', 'status', sPoolable);

  END LOOP;

  clChartCalcResultXml := ISR$XML.XMLTOCLOB(xXml);

  isr$trace.stat('end', 'end', sCurrentName);

  RETURN (oErrorObj);

exception
  when others then
    sMsg :=  utd$msglib.getmsg ('exUnexpectedError', stb$security.getcurrentlanguage,  sqlerrm(sqlcode));
    oErrorObj.handleError ( Stb$typedef.cnSeverityCritical, sMsg, sCurrentName,
                            dbms_utility.format_error_stack || CHR(10) || dbms_utility.format_error_backtrace  );
    return oErrorObj;
END calculation_fTest;

--******************************************************************************************************************************
procedure insertIntoJobtrail(cnJobID IN NUMBER, clClob IN CLOB, csFileName IN VARCHAR2, csDocumentType IN VARCHAR2, blBlob IN BLOB default null, cnBaseStylesheetId IN NUMBER DEFAULT NULL)
is
  sCurrentName   constant varchar2(4000) := $$PLSQL_UNIT||'.insertIntoJobtrail('||cnJobId||')';
begin
  isr$trace.stat('begin', 'begin', sCurrentName);
  isr$trace.debug('parameter','clClob',sCurrentName,clClob);

  UPDATE stb$jobtrail set reference = null where reference = cnBaseStylesheetId and jobid = cnJobID;

  INSERT INTO STB$JOBTRAIL (JOBID
                          , TEXTFILE
                          , BINARYFILE
                          , DOCUMENTTYPE
                          , MIMETYPE
                          , DOCSIZE
                          , FILENAME
                          , BLOBFLAG
                          , ENTRYID
                          , REFERENCE
                          , LEADINGDOC
                          , DOWNLOAD
                          , TIMESTAMP)
     (SELECT cnJOBID
           , clClob
           , blBlob
           , csDocumentType
           , MIMETYPE
           , case when clClob is null then DBMS_LOB.getLength (blBlob) else DBMS_LOB.getLength (clClob) end
           , csFileName
           , case when clClob is null then 'B' else 'C' end
           , STB$JOBTRAIL$SEQ.NEXTVAL
           , cnBaseStylesheetId
           , LEADINGDOC
           , 'T'
           , sysdate
        FROM STB$JOBTRAIL
       WHERE jobid = cnJobID
         AND documenttype = 'ROHDATEN');

  COMMIT;

  isr$trace.stat('end', 'end', sCurrentName);
end insertIntoJobtrail;

--******************************************************************************************************************************
FUNCTION createCalculations(cnJobid in NUMBER, clData IN CLOB, cnStylesheetID IN NUMBER) RETURN STB$OERROR$RECORD IS

  oErrorObj       STB$OERROR$RECORD := STB$OERROR$RECORD();
  sCurrentName    constant varchar2 (100) := $$PLSQL_UNIT || '.createCalculations';
  sUserMsg        varchar2(4000);
  exfTestCalcErr  exception;
  exNoResultCalcExc  exception;

  clCalcResult    CLOB;
  sRemoteEx   clob;
  
  sCurrentLal   VARCHAR2(100) := STB$UTIL.getCurrentLal;

  cursor cGetCalcResult is
  select textfile from isr$transfer$graphic$tmp
  where jobid = cnJobid and filename = 'calcresult.xml';

BEGIN
  isr$trace.stat('begin','begin',sCurrentName);

  isr$trace.debug('cnJobid',cnJobid,sCurrentName);
  isr$trace.debug('chartcalc_'||cnJobid||'.xml',DBMS_LOB.getLength(clData),sCurrentName,clData);
  isr$trace.debug('cnStylesheetID',cnStylesheetID,sCurrentName);

  sDelimiter := isr$file$transfer$service.getKeyDelimiter( 'GRAPHIC_CALCULATION' );

    -- 1st call for the basic calculation and regression
  BEGIN
    STB$JOB.setProgress(UTD$MSGLIB.getMsg('GRAPHICSTEXT', STB$SECURITY.getCurrentLanguage)||' '||UTD$MSGLIB.getMsg('CALC_REGRESSION', STB$SECURITY.getCurrentLanguage), cnJobid);
    open cGetServer;
    fetch cGetServer into rGetServer;
    if cGetServer%notfound then
      close cGetServer;
      raise exGraphicServerNotRunning;
    end if;

    sRemoteEx := createCalculation(clData, rGetServer.observer_host, rGetServer.port, cnJobId, sDelimiter, 'GRAPHIC_CALCULATION', isr$trace.getJavaUserLoglevelStr, isr$trace.getReference, rGetServer.timeout);
    if sRemoteEx is not null then
      isr$trace.error('calculation error on the server "' || rGetServer.alias || '"', 'error ', sCurrentName, sRemoteEx);
      oErrorObj.handleJavaError( Stb$typedef.cnSeverityWarning,  sRemoteEx, sCurrentName );
    end if;
    close cGetServer;

    open cGetCalcResult;
    fetch cGetCalcResult into clCalcResult;
    if cGetCalcResult%notfound then
      isr$trace.error('error in calculation_regression', 'result file is null', sCurrentName);
      raise exNoResultCalcExc;
    end if;
    close cGetCalcResult;
  EXCEPTION WHEN OTHERS THEN
    if cGetServer%isopen then
      close cGetServer;
    end if;
    isr$trace.error('error in calculation_regression', sqlerrm,sCurrentName);
    isr$trace.error('clData_'||cnJobid||'.xml',DBMS_LOB.getLength(clData), sCurrentName, clData);
    RAISE;
  END;
  isr$trace.debug('clCalcResult_step1_'||cnJobid||'.xml', dbms_lob.getlength(clCalcResult), sCurrentName, clCalcResult);

    -- calculating FTest, if common slope/intercept is activated
    IF NVL(STB$UTIL.getReportParameterValue('COMMON_SLOPE_INTERCEPT', STB$JOB.getJobParameter('REPID',cnJobId)), 'F') = 'T' THEN

      STB$JOB.setProgress(UTD$MSGLIB.getMsg('GRAPHICSTEXT', STB$SECURITY.getCurrentLanguage)||' '||UTD$MSGLIB.getMsg('CALC_FTEST', STB$SECURITY.getCurrentLanguage), cnJobid);

      -- go to lal
      IF STB$UTIL.checkMethodExists('calculation_fTest') = 'T' THEN
        EXECUTE immediate
          'BEGIN
             :1 := '|| sCurrentLal ||'.calculation_fTest(:2, :3);
           END;'
        Using OUT oErrorObj, IN OUT clCalcResult, IN cnJobid;
      ELSE
        oErrorObj := calculation_fTest(clCalcResult, cnJobid);
      END IF;

      IF oErrorObj.sSeverityCode != Stb$typedef.cnNoError THEN
        isr$trace.error('error in calculation_fTest', sqlerrm, sCurrentName);
        isr$trace.error('clCalcResult_step2_'||cnJobid||'.xml', dbms_lob.getlength(clCalcResult), sCurrentName, clCalcResult);
        RAISE exfTestCalcErr;
      END IF;
      isr$trace.debug('clCalcResult_step2_'||cnJobid||'.xml', dbms_lob.getlength(clCalcResult), sCurrentName, clCalcResult);

 --   END IF;

    -- 2nd call for the intersections
    BEGIN
      STB$JOB.setProgress(UTD$MSGLIB.getMsg('GRAPHICSTEXT', STB$SECURITY.getCurrentLanguage)||' '||UTD$MSGLIB.getMsg('CALC_INTERSECTIONS', STB$SECURITY.getCurrentLanguage), cnJobid);
      open cGetServer;
      fetch cGetServer into rGetServer;
      if cGetServer%notfound then
        close cGetServer;
        raise exGraphicServerNotRunning;
      end if;

      isr$trace.debug('2nd call for the intersections', 'clCalcResult', sCurrentName, clCalcResult);
      sRemoteEx := createCalculation(clCalcResult, rGetServer.observer_host, rGetServer.port, cnJobId, sDelimiter, 'GRAPHIC_CALCULATION', isr$trace.getJavaUserLoglevelStr, isr$trace.getReference, rGetServer.timeout);
      if sRemoteEx is not null then
        isr$trace.error('calculation error on the server "' || rGetServer.alias || '"', 'error ', sCurrentName, sRemoteEx);
        oErrorObj.handleJavaError( Stb$typedef.cnSeverityWarning,  sRemoteEx, sCurrentName );
      end if;
      close cGetServer;

      open cGetCalcResult;
      fetch cGetCalcResult into clCalcResult;
      if cGetCalcResult%notfound then
        isr$trace.error('error in calculation_regression', 'result file is null', sCurrentName);
        raise exNoResultCalcExc;
      end if;
      close cGetCalcResult;
    EXCEPTION WHEN OTHERS THEN
      isr$trace.error('error in calculation_intersections', sqlerrm, sCurrentName);
      isr$trace.error('clCalcResult_step3_'||cnJobid||'.xml', dbms_lob.getlength(clCalcResult), sCurrentName, clCalcResult);
      RAISE;
    END;
    isr$trace.debug('clCalcResult_step3_'||cnJobid||'.xml', dbms_lob.getlength(clCalcResult), sCurrentName, clCalcResult);

  END IF;

  insertIntoJobtrail(cnJobId, clCalcResult, 'calcresult.xml', 'CALCRESULT', null, cnStylesheetID);
  delete from isr$transfer$graphic$tmp where jobid = cnJobid;

  isr$trace.stat('end','end',sCurrentName);
  RETURN oErrorObj;

EXCEPTION
  WHEN exfTestCalcErr THEN
   -- oErrorObj.sErrorCode      :=Utd$msglib.GetMsg('exfTestCalcErr',Stb$security.GetCurrentLanguage);
    sUserMsg := utd$msglib.getmsg ('exfTestCalcErrText', stb$security.getcurrentlanguage,  sqlerrm(sqlcode));
    oErrorObj.handleError ( Stb$typedef.cnSeverityCritical, sUserMsg, sCurrentName,
                          dbms_utility.format_error_stack || CHR(10) || dbms_utility.format_error_backtrace);
    RETURN oErrorObj;
  WHEN exGraphicServerNotRunning THEN
    sUserMsg := utd$msglib.getmsg ('exGraphicServerNotRunningError', stb$security.getcurrentlanguage);
    oErrorObj.handleError ( Stb$typedef.cnSeverityCritical, sUserMsg, sCurrentName,
                          dbms_utility.format_error_stack || CHR(10) || dbms_utility.format_error_backtrace);
    RETURN oErrorObj;
  WHEN OTHERS THEN
      if cGetServer%isopen then
        close cGetServer;
      end if;
    sUserMsg := utd$msglib.getmsg ('exUnexpectedError', stb$security.getcurrentlanguage,  sqlerrm(sqlcode));
    oErrorObj.handleError ( Stb$typedef.cnSeverityCritical, sUserMsg, sCurrentName,
                          dbms_utility.format_error_stack || CHR(10) || dbms_utility.format_error_backtrace);
    RETURN oErrorObj;
END createCalculations;

--******************************************************************************************************************************
FUNCTION createPlots(cnJobid in NUMBER, clData IN CLOB, cnStylesheetID IN NUMBER) RETURN STB$OERROR$RECORD IS

  oErrorObj       STB$OERROR$RECORD := STB$OERROR$RECORD();
  sCurrentName    constant varchar2 (100) := $$PLSQL_UNIT || '.createPlots';
  sUserMsg        varchar2(4000);
  sFound          VARCHAR2(1);
  sRemoteEx   clob;

 /* CURSOR cGetBinaryFiles IS
    SELECT binaryfile, filename
      FROM isr$report$file
     WHERE repid = STB$JOB.getJobParameter ('REPID', cnJobid)
       AND NVL(dbms_lob.getlength(binaryfile), 0) > 0; */


BEGIN
  isr$trace.stat('begin', 'cnJobid=' || cnJobid, sCurrentName);

  isr$trace.debug('clData_'||cnJobid||'.xml', dbms_lob.getlength(clData),sCurrentName,clData);

  sFound := 'F';
 /* FOR rGetBinaryFiles IN cGetBinaryFiles LOOP
    insertIntoJobtrail(cnJobId, null, rGetBinaryFiles.filename, 'GRAPHIC', rGetBinaryFiles.binaryfile);
    sFound := 'T';
  END LOOP; */
  isr$trace.debug('sFound', sFound, sCurrentName);

  IF sFound = 'F' THEN

    BEGIN
      STB$JOB.setProgress(UTD$MSGLIB.getMsg('GRAPHICSTEXT', STB$SECURITY.getCurrentLanguage)||' '||UTD$MSGLIB.getMsg('CREATE_GRAPHICS', STB$SECURITY.getCurrentLanguage), cnJobid);
      open cGetServer;
      fetch cGetServer into rGetServer;

      if cGetServer%notfound then
        close cGetServer;
        sUserMsg := utd$msglib.getmsg ('exGraphicServerNotRunningError', stb$security.getcurrentlanguage);
        raise exGraphicServerNotRunning;
      end if;

      sDelimiter := isr$file$transfer$service.getKeyDelimiter( 'GRAPHIC_RESULT' );
      sRemoteEx := createGraphics(clData, rGetServer.observer_host, rGetServer.port, cnJobId, sDelimiter, 'GRAPHIC_RESULT', isr$trace.getJavaUserLoglevelStr, isr$trace.getReference, rGetServer.timeout);
      if sRemoteEx is not null then
        isr$trace.error('create graphics error on the server "' || rGetServer.alias || '"', 'error ', sCurrentName, sRemoteEx);
        oErrorObj.handleJavaError( Stb$typedef.cnSeverityWarning,  sRemoteEx, sCurrentName );
      end if;
      close cGetServer;

      update stb$jobtrail set timestamp = sysdate, blobflag ='B', docsize = DBMS_LOB.getLength (binaryfile), reference = STB$JOB.getJobParameter('REFERENCE', cnJobid)
             where jobid = cnJobid and documenttype = 'GRAPHIC';

     /* INSERT INTO ISR$REPORT$FILE (entryid
                                 , repid
                                 , filename
                                 , binaryfile)
         (SELECT SEQ$ISR$REPORT$FILE.NEXTVAL
               , STB$JOB.getJobParameter ('REPID', cnJobId)
               , filename
               , binaryfile
            FROM stb$jobtrail
           WHERE documenttype = 'GRAPHIC'
             AND jobid = cnJobId); */
    EXCEPTION
     WHEN exGraphicServerNotRunning THEN
      sUserMsg := utd$msglib.getmsg ('exGraphicServerNotRunningError', stb$security.getcurrentlanguage);
      oErrorObj.handleError ( Stb$typedef.cnSeverityCritical, sUserMsg, sCurrentName,
                            dbms_utility.format_error_stack || CHR(10) || dbms_utility.format_error_backtrace);
      RETURN oErrorObj;
    WHEN OTHERS THEN
      isr$trace.error('error in creation of plots', sqlerrm , sCurrentName);
      isr$trace.error('clData_'||cnJobid||'.xml',dbms_lob.getlength(clData), sCurrentName,clData);
      sUserMsg := utd$msglib.getmsg ('exUnexpectedError', stb$security.getcurrentlanguage,  sqlerrm(sqlcode));
      oErrorObj.handleError ( Stb$typedef.cnSeverityCritical, sUserMsg, sCurrentName,
                          dbms_utility.format_error_stack || CHR(10) || dbms_utility.format_error_backtrace);
      RAISE;
    END;

  END IF;

  isr$trace.stat('end', 'end',sCurrentName);
  RETURN oErrorObj;

EXCEPTION
  WHEN OTHERS THEN
      if cGetServer%isopen then
        close cGetServer;
      end if;
  sUserMsg := utd$msglib.getmsg ('exUnexpectedError', stb$security.getcurrentlanguage,  sqlerrm(sqlcode));
  oErrorObj.handleError ( Stb$typedef.cnSeverityCritical, sUserMsg, sCurrentName,
                          dbms_utility.format_error_stack || CHR(10) || dbms_utility.format_error_backtrace);
    RETURN oErrorObj;
END createPlots;

FUNCTION createCalculations(cnJobId IN NUMBER, cxXml IN OUT XMLTYPE) RETURN STB$OERROR$RECORD IS
  sMsg                   varchar2(4000);
  sCurrentName constant  varchar2(100) := $$PLSQL_UNIT||'.createCalculations';
  oErrorObj              STB$OERROR$RECORD := STB$OERROR$RECORD();

cursor cGetStylesheets is
    select s.stylesheetid
         , ot.basestylesheet
         , j.textfile
      from ISR$STYLESHEET s, ISR$OUTPUT$TRANSFORM ot, stb$jobtrail j
     where ot.outputid = STB$JOB.getJobParameter ('OUTPUTID', cnJobId)
       and ot.stylesheetid = s.STYLESHEETID
       and (ot.keyname is null or ot.keyname in
                  (select key
                     from isr$crit
                    where repid = STB$JOB.getJobParameter ('REPID', cnJobId)
                      and entity = STB$UTIL.getReptypeParameter ( STB$JOB.getJobParameter ('REPORTTYPEID', cnJobId), 'STYLESHEETENTITY')))
       and (ot.recreate = 'T' or (ot.recreate = 'F'
         and /*(ot.bookmarkname || '.' || s.extension not in
                   (select filename
                      from isr$report$file
                     where repid in ( select repid
                                        from stb$report
                                      start with repid = STB$JOB.getJobParameter ('REPID', cnJobId)
                                      connect by prior parentrepid = repid)))
              or*/ STB$JOB.getJobParameter ('TOKEN', cnJobId) = 'CAN_OFFSHOOT_DOC')
              or STB$UTIL.getReptypeParameter ( STB$JOB.getJobParameter ('REPORTTYPEID', cnJobId), 'REQUIRES_VERSIONING') = 'F')
         and j.jobid = cnJobId
         and reference = ot.basestylesheet
         and documenttype != 'STYLESHEET';

   rGetStylesheets  cGetStylesheets%rowtype;

begin
  isr$trace.stat('begin', 'cnJobId: '||cnJobId, sCurrentName);

  open cGetStylesheets;
  fetch cGetStylesheets into rGetStylesheets;
  if cGetStylesheets%found then
    oErrorObj := createCalculations(cnJobid, rGetStylesheets.textfile, rGetStylesheets.stylesheetid);
  end if;
  close cGetStylesheets;

  isr$trace.stat('end', 'end', sCurrentName);
  return oErrorObj;
exception
  when others then
    sMsg :=  utd$msglib.getmsg ('exUnexpectedError', stb$security.getcurrentlanguage,  sqlerrm(sqlcode));
    oErrorObj.handleError ( Stb$typedef.cnSeverityWarning, sMsg, sCurrentName,
                            dbms_utility.format_error_stack || CHR(10) || dbms_utility.format_error_backtrace  );
    return oErrorObj;
end createCalculations;

FUNCTION createPlots(cnJobId IN NUMBER, cxXml IN OUT XMLTYPE) RETURN STB$OERROR$RECORD IS
  sMsg                   varchar2(4000);
  sCurrentName constant  varchar2(100) := $$PLSQL_UNIT||'.createPlots';
  oErrorObj              STB$OERROR$RECORD := STB$OERROR$RECORD();


begin
  isr$trace.stat('begin', 'cnJobId: '||cnJobId, sCurrentName);
  --oErrorObj := createPlots(cnJobid, clData, cnStylesheetID);
  isr$trace.stat('end', 'end', sCurrentName);
  return oErrorObj;
exception
  when others then
    sMsg :=  utd$msglib.getmsg ('exUnexpectedError', stb$security.getcurrentlanguage,  sqlerrm(sqlcode));
    oErrorObj.handleError ( Stb$typedef.cnSeverityWarning, sMsg, sCurrentName,
                            dbms_utility.format_error_stack || CHR(10) || dbms_utility.format_error_backtrace  );
    return oErrorObj;
end createPlots;

--******************************************************************************
FUNCTION createCalculation(clXml IN CLOB, csHost IN VARCHAR2, csPort IN VARCHAR2, cnJobid IN NUMBER,  csDelimiter IN VARCHAR2,  csConfigname IN VARCHAR2, csLogLevel IN VARCHAR2, csLogReference IN VARCHAR2, nTimeout NUMBER) return CLOB
AS LANGUAGE JAVA
NAME 'com.uptodata.isr.graphic.db.GraphicGeneration.createCalculation(java.sql.Clob, java.lang.String,java.lang.String, int, java.lang.String, java.lang.String, java.lang.String, java.lang.String, int) return java.sql.Clob';
--******************************************************************************
FUNCTION createGraphics(clXml IN CLOB, csHost IN VARCHAR2, csPort IN VARCHAR2, cnJobid IN NUMBER,  csDelimiter IN VARCHAR2, csConfigname IN VARCHAR2,  csLogLevel IN VARCHAR2, csLogReference IN VARCHAR2, nTimeout NUMBER) return CLOB
AS LANGUAGE JAVA
NAME 'com.uptodata.isr.graphic.db.GraphicGeneration.createGraphics(java.sql.Clob, java.lang.String,java.lang.String, int, java.lang.String, java.lang.String, java.lang.String, java.lang.String, int) return java.sql.Clob';

END isr$graphic;