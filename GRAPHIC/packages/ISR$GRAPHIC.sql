CREATE OR REPLACE PACKAGE isr$graphic is

-- ***************************************************************************************************
-- Description/Usage:
-- the package Stb$graphic includes all functionality dealing the generation of graphics
-- ***************************************************************************************************
function createGraphics( cnJobid in     number,  cxXml   in out xmltype )  return STB$OERROR$RECORD;
--******************************************************************************************************************************

FUNCTION createPlots(cnJobid in NUMBER, clData IN CLOB, cnStylesheetID IN NUMBER) RETURN STB$OERROR$RECORD;
-- ***************************************************************************************************
-- Date und Autor: 21. Oct 2011, MCD
-- builds graphics with xml given in clData
--
-- Parameters:
-- cnJobID        the current jobid
-- clData         the input xml file
-- cnStylesheetID the input stylesheet id
--
-- ***************************************************************************************************
  
FUNCTION createCalculations(cnJobid in NUMBER, clData IN CLOB, cnStylesheetID IN NUMBER) RETURN STB$OERROR$RECORD;
-- ***************************************************************************************************
-- Date und Autor: 21. Oct 2011, MCD
-- performs calculations on the data given in clData
--
-- Parameters:
-- cnJobID        the current jobid
-- clData         the input xml file
-- cnStylesheetID the input stylesheet id
--
-- ***************************************************************************************************
  
FUNCTION createCalculation(clXml IN CLOB, csHost IN VARCHAR2, csPort IN VARCHAR2, cnJobid IN NUMBER,  csDelimiter IN VARCHAR2, csConfigname IN VARCHAR2, csLogLevel IN VARCHAR2, csLogReference IN VARCHAR2, nTimeout NUMBER) return CLOB;
-- ***************************************************************************************************
-- Date und Autor: 05. Mar 2010, MCD
-- creates the calculation for an xml file
--
-- Parameter:
-- clXml        xml
--  
--
-- ***************************************************************************************************

FUNCTION createGraphics(clXml IN CLOB, csHost IN VARCHAR2, csPort IN VARCHAR2, cnJobid IN NUMBER,  csDelimiter IN VARCHAR2,  csConfigname IN VARCHAR2, csLogLevel IN VARCHAR2, csLogReference IN VARCHAR2, nTimeout NUMBER) return CLOB;
-- ***************************************************************************************************
-- Date und Autor: 05. Mar 2010, MCD
-- creates the graphics for an xml file in the specified path
--
-- Parameter:
-- clXml        xml
-- csPath       path name
-- csXmlName    xml name
-- cnJobid      jobid
-- csJREFonts   path the jre fonts on unix dbs
--  
--
-- ***************************************************************************************************

FUNCTION createCalculations(cnJobId IN NUMBER, cxXml IN OUT XMLTYPE) RETURN STB$OERROR$RECORD;
-- ***************************************************************************************************
-- Date und Autor: 17. Dec 2015, IS
-- this function is called from action job for create the  calculation xml for graphics 
--
-- Parameter:
--- cnJobid   the identifier of the iStudyReporter job
-- cxXml     the xml configuration file
--  
--***************************************************************************************************

FUNCTION createPlots(cnJobId IN NUMBER, cxXml IN OUT XMLTYPE) RETURN STB$OERROR$RECORD;
-- ***************************************************************************************************
-- Date und Autor: 17. Dec 2015, IS
-- this function is called from action job for create the graphics 
--
-- Parameter:
--- cnJobid   the identifier of the iStudyReporter job
-- cxXml     the xml configuration file
--  
END isr$graphic;