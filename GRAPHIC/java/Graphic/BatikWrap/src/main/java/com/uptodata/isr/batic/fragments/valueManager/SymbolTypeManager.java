/*****************************************************************************
 * Copyright (C) The Apache Software Foundation. All rights reserved.        *
 * ------------------------------------------------------------------------- *
 * This software is published under the terms of the Apache Software License *
 * version 1.1, a copy of which has been included with this distribution in  *
 * the LICENSE file.                                                         *
 *****************************************************************************/

package com.uptodata.isr.batic.fragments.valueManager;

import org.apache.batik.css.engine.value.IdentifierManager;
import org.apache.batik.css.engine.value.StringMap;
import org.apache.batik.css.engine.value.StringValue;
import org.apache.batik.css.engine.value.Value;
import org.w3c.dom.css.CSSPrimitiveValue;

/**
 * This class provides a manager for the 'stroke-linecap' property values.
 *
 * @author <a href="mailto:stephane@hillion.org">Stephane Hillion</a>
 * @version $Id: StrokeLinecapManager.java,v 1.2 2003/04/11 13:55:58 vhardy Exp $
 */
public class SymbolTypeManager extends IdentifierManager {

    /**
     * The identifier values.
     */
    protected final static StringMap values = new StringMap();
    protected static Value def;

    static {
        String[] syms = new String[]{"plus", "cross", "box", "diamond", "triup", "tridn", "dot", "hline", "vline", "star",};
        for (int i = 0; i < syms.length; i++) {
            String id = syms[i];
            Value v = new StringValue(CSSPrimitiveValue.CSS_IDENT, id);
            if (i == 0) def = v;
            values.put(id, v);
        }
    }

    /*
     * Implements {@link
	 * org.apache.batik.css.engine.value.ValueManager#isInheritedProperty()}.
	 */
    public boolean isInheritedProperty() {
        return false;
    }

    @Override
    public boolean isAnimatableProperty() {
        return false;
    }

    @Override
    public boolean isAdditiveProperty() {
        return false;
    }

    @Override
    public int getPropertyType() {
        return 0;
    }

    /*
     * Implements {@link
     * org.apache.batik.css.engine.value.ValueManager#getPropertyName()}.
     */
    public String getPropertyName() {
        return "symbol-type";
    }

    /*
     * Implements {@link
     * org.apache.batik.css.engine.value.ValueManager#getDefaultValue()}.
     */
    public Value getDefaultValue() {
        return def;
    }

    /*
     * Implements {@link IdentifierManager#getIdentifiers()}.
     */
    public StringMap getIdentifiers() {
        return values;
    }
}
