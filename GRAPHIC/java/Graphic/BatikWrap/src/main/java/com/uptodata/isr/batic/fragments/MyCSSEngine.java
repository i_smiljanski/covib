package com.uptodata.isr.batic.fragments;

import org.apache.batik.css.engine.*;
import org.apache.batik.css.engine.StyleDeclaration;
import org.apache.batik.css.engine.sac.CSSSelectorFactory;
import org.apache.batik.css.engine.value.ShorthandManager;
import org.apache.batik.css.engine.value.Value;
import org.apache.batik.css.engine.value.ValueManager;
import org.apache.batik.css.parser.ExtendedParser;
import org.apache.batik.util.ParsedURL;
import org.w3c.css.sac.LexicalUnit;
import org.w3c.dom.DOMException;
import org.w3c.dom.Document;
import org.w3c.dom.NamedNodeMap;
import org.w3c.dom.Node;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by smiljanskii60 on 10.09.2015.
 * this class is only for search error instead of CSSEngine
 * later todo: drop this class
 */
public class MyCSSEngine extends SVGCSSEngine {
    /**
     * Creates a new CSSEngine.
     *
     * @param doc     The associated document.
     * @param uri     The document URI.
     * @param p       The CSS parser.
     * @param vm      The property value managers.
     * @param sm      The shorthand properties managers.
     * @param pe      The pseudo-element names supported by the associated
     *                XML dialect. Must be null if no support for pseudo-
     *                elements is required.
     * @param sns     The namespace URI of the style attribute.
     * @param sln     The local name of the style attribute.
     * @param cns     The namespace URI of the class attribute.
     * @param cln     The local name of the class attribute.
     * @param hints   Whether the CSS engine should support non CSS
     *                presentational hints.
     * @param hintsNS The hints namespace URI.
     * @param ctx     The CSS context.
     */
    protected MyCSSEngine(Document doc, ParsedURL uri, ExtendedParser p, ValueManager[] vm, ShorthandManager[] sm, String[] pe, String sns, String sln, String cns, String cln, boolean hints, String hintsNS, CSSContext ctx) {
        super(doc, uri, p, vm, sm, pe, sns, sln, cns, cln, hints, hintsNS, ctx);
    }

    public MyCSSEngine(Document doc, ParsedURL uri, ExtendedParser p, CSSContext ctx) {
        super(doc, uri, p, ctx);
    }

    public MyCSSEngine(Document doc, ParsedURL uri, ExtendedParser p, ValueManager[] vms, ShorthandManager[] sms, CSSContext ctx) {
        super(doc, uri, p, vms, sms, ctx);
    }

    public StyleMap getCascadedStyleMap(CSSStylableElement elt,
                                        String pseudo) {
        int props = getNumberOfProperties();
        final StyleMap result = new StyleMap(props);

        // Apply the user-agent style-sheet to the result.
        if (userAgentStyleSheet != null) {
            ArrayList rules = new ArrayList();
            addMatchingRules(rules, userAgentStyleSheet, elt, pseudo);
            addRules(elt, pseudo, result, rules, StyleMap.USER_AGENT_ORIGIN);
        }

        // Apply the user properties style-sheet to the result.
        if (userStyleSheet != null) {
            ArrayList rules = new ArrayList();
            addMatchingRules(rules, userStyleSheet, elt, pseudo);
            addRules(elt, pseudo, result, rules, StyleMap.USER_ORIGIN);
        }

        element = elt;
        try {
            // Apply the non-CSS presentational hints to the result.
            if (nonCSSPresentationalHints != null) {
                ShorthandManager.PropertyHandler ph =
                        new ShorthandManager.PropertyHandler() {
                            public void property(String pname, LexicalUnit lu,
                                                 boolean important) {
                                int idx = getPropertyIndex(pname);
                                if (idx != -1) {
                                    ValueManager vm = valueManagers[idx];
                                    Value v = vm.createValue(lu, MyCSSEngine.this);
                                    putAuthorProperty(result, idx, v, important,
                                            StyleMap.NON_CSS_ORIGIN);
                                    return;
                                }
                                idx = getShorthandIndex(pname);
                                if (idx == -1)
                                    return; // Unknown property...
                                // Shorthand value
                                shorthandManagers[idx].setValues
                                        (MyCSSEngine.this, this, lu, important);
                            }
                        };

                NamedNodeMap attrs = elt.getAttributes();
                int len = attrs.getLength();
                for (int i = 0; i < len; i++) {
                    Node attr = attrs.item(i);
                    String an = attr.getNodeName();
                    if (nonCSSPresentationalHints.contains(an)) {
                        try {
                            LexicalUnit lu;
                            lu = parser.parsePropertyValue(attr.getNodeValue());
                            ph.property(an, lu, false);
                        } catch (Exception e) {
                            String m = e.getMessage();
                            if (m == null) m = "";
                            String u = ((documentURI == null) ? "<unknown>" :
                                    documentURI.toString());
                            String s = Messages.formatMessage
                                    ("property.syntax.error.at",
                                            new Object[]{u, an, attr.getNodeValue(), m});
                            DOMException de = new DOMException(DOMException.SYNTAX_ERR, s);
                            if (userAgent == null) throw de;
                            userAgent.displayError(de);
                        }
                    }
                }
            }

            // Apply the document style-sheets to the result.
            CSSEngine eng = cssContext.getCSSEngineForElement(elt);
            List snodes = eng.getStyleSheetNodes();
            int slen = snodes.size();
            if (slen > 0) {
                ArrayList rules = new ArrayList();
                for (int i = 0; i < slen; i++) {
                    CSSStyleSheetNode ssn = (CSSStyleSheetNode) snodes.get(i);
                    StyleSheet ss = ssn.getCSSStyleSheet();
                    if (ss != null &&
                            (!ss.isAlternate() ||
                                    ss.getTitle() == null ||
                                    ss.getTitle().equals(alternateStyleSheet)) &&
                            mediaMatch(ss.getMedia())) {
                        addMatchingRules(rules, ss, elt, pseudo);
                    }
                }
                addRules(elt, pseudo, result, rules, StyleMap.AUTHOR_ORIGIN);
            }

            // Apply the inline style to the result.
            if (styleLocalName != null) {
                String style = elt.getAttributeNS(styleNamespaceURI,
                        styleLocalName);
                if (style.length() > 0) {
                    try {
                        parser.setSelectorFactory(CSSSelectorFactory.INSTANCE);
                        parser.setConditionFactory(cssConditionFactory);
                        styleDeclarationDocumentHandler.styleMap = result;
                        parser.setDocumentHandler
                                (styleDeclarationDocumentHandler);
                        parser.parseStyleDeclaration(style);
                        styleDeclarationDocumentHandler.styleMap = null;
                    } catch (Exception e) {
                        String m = e.getMessage();
                        if (m == null) m = e.getClass().getName();
                        String u = ((documentURI == null) ? "<unknown>" :
                                documentURI.toString());
                        String s = Messages.formatMessage
                                ("style.syntax.error.at",
                                        new Object[]{u, styleLocalName, style, m});
                        DOMException de = new DOMException(DOMException.SYNTAX_ERR, s);
                        if (userAgent == null) throw de;
                        userAgent.displayError(de);
                    }
                }
            }

            // Apply the override rules to the result.
            StyleDeclarationProvider p =
                    elt.getOverrideStyleDeclarationProvider();
            if (p != null) {
                StyleDeclaration over = p.getStyleDeclaration();
                if (over != null) {
                    int ol = over.size();
                    for (int i = 0; i < ol; i++) {
                        int idx = over.getIndex(i);
                        Value value = over.getValue(i);
                        boolean important = over.getPriority(i);
                        if (!result.isImportant(idx) || important) {
                            result.putValue(idx, value);
                            result.putImportant(idx, important);
                            result.putOrigin(idx, StyleMap.OVERRIDE_ORIGIN);
                        }
                    }
                }
            }
        } finally {
            element = null;
            cssBaseURI = null;
        }

        return result;
    }
}
