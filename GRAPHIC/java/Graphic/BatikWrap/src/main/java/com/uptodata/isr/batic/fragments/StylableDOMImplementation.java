/*****************************************************************************
 * Copyright (C) The Apache Software Foundation. All rights reserved.        *
 * ------------------------------------------------------------------------- *
 * This software is published under the terms of the Apache Software License *
 * version 1.1, a copy of which has been included with this distribution in  *
 * the LICENSE file.                                                         *
 *****************************************************************************/

package com.uptodata.isr.batic.fragments;

import com.uptodata.isr.batic.fragments.valueManager.SimpleLengthManager;
import com.uptodata.isr.batic.fragments.valueManager.SymbolTypeManager;
import org.apache.batik.css.dom.CSSOMSVGViewCSS;
import org.apache.batik.css.engine.CSSContext;
import org.apache.batik.css.engine.value.AbstractValueManager;
import org.apache.batik.css.engine.value.ShorthandManager;
import org.apache.batik.css.engine.value.ValueConstants;
import org.apache.batik.css.engine.value.ValueManager;
import org.apache.batik.css.engine.value.svg.*;
import org.apache.batik.css.parser.ExtendedParser;
import org.apache.batik.css.parser.ExtendedParserWrapper;
import org.apache.batik.dom.AbstractDOMImplementation;
import org.apache.batik.dom.AbstractDocument;
import org.apache.batik.dom.StyleSheetFactory;
import org.apache.batik.dom.events.DocumentEventSupport;
import org.apache.batik.dom.util.CSSStyleDeclarationFactory;
import org.apache.batik.dom.util.HashTable;
import org.apache.batik.i18n.Localizable;
import org.apache.batik.i18n.LocalizableSupport;
import org.apache.batik.util.ParsedURL;
import org.apache.batik.util.SVGConstants;
import org.apache.batik.util.XMLResourceDescriptor;
import org.w3c.css.sac.InputSource;
import org.w3c.css.sac.Parser;
import org.w3c.dom.*;
import org.w3c.dom.css.CSSStyleDeclaration;
import org.w3c.dom.css.CSSStyleSheet;
import org.w3c.dom.css.DOMImplementationCSS;
import org.w3c.dom.css.ViewCSS;
import org.w3c.dom.stylesheets.StyleSheet;

import java.net.URL;
import java.util.*;


/**
 * This class implements the {@link org.w3c.dom.DOMImplementation} interface.
 * It provides support the SVG 1.0 documents.
 *
 * @author <a href="mailto:stephane@hillion.org">Stephane Hillion</a>
 * @version $Id: SVGDOMImplementation.java,v 1.23 2003/04/11 13:56:10 vhardy Exp $
 */
public class StylableDOMImplementation // was SVGDOMImplementation
        extends AbstractDOMImplementation
        implements Localizable,
        DOMImplementationCSS,
        CSSStyleDeclarationFactory,
        StyleSheetFactory
        , SVGConstants {

    /**
     * The SVG namespace uri.
     */
//    public final static String SVG_NAMESPACE_URI = SVGConstants.SVG_NAMESPACE_URI;

    /**
     * The default instance of this class.
     */
    protected final static DOMImplementation DOM_IMPLEMENTATION = new StylableDOMImplementation();

    /**
     * The error messages bundle class name.
     */
    protected final static String RESOURCES = "org.apache.batik.dom.svg.resources.Messages";

    /**
     * The localizable support for the error messages.
     */
    protected LocalizableSupport localizableSupport = new LocalizableSupport(RESOURCES);

    /**
     * Returns the default instance of this class.
     */
    public static DOMImplementation getDOMImplementation() {
        return DOM_IMPLEMENTATION;
    }

    /**
     * Creates a new SVGDOMImplementation object.
     */
    public StylableDOMImplementation() {
        registerFeature("CSS", "2.0");
        registerFeature("StyleSheets", "2.0");
        registerFeature("SVG", "1.0");
        registerFeature("SVGEvents", "1.0");
    }

    /**
     * Creates new CSSEngine and attach it to the document.
     */
    public MyCSSEngine createCSSEngine(StylableDocument doc, CSSContext ctx) {
        String pn = XMLResourceDescriptor.getCSSParserClassName();
        Parser p;
        try {
            p = (Parser) Class.forName(pn).newInstance();
        } catch (ClassNotFoundException e) {
            throw new DOMException(DOMException.INVALID_ACCESS_ERR,
                    formatMessage("css.parser.class",
                            new Object[]{pn}));
        } catch (InstantiationException e) {
            throw new DOMException(DOMException.INVALID_ACCESS_ERR,
                    formatMessage("css.parser.creation",
                            new Object[]{pn}));
        } catch (IllegalAccessException e) {
            throw new DOMException(DOMException.INVALID_ACCESS_ERR,
                    formatMessage("css.parser.access",
                            new Object[]{pn}));
        }
        ExtendedParser ep = ExtendedParserWrapper.wrap(p);

        MyCSSEngine result = createCssEngine(doc, ctx, ep);
        URL url = getClass().getResource("resources/UserAgentStyleSheet.css");
        if (url != null) {
            InputSource is = new InputSource(url.toString());
            result.setUserAgentStyleSheet(result.parseStyleSheet(is, new ParsedURL(url), "all"));
        }
        doc.setCSSEngine(result);
        return result;
    }
    
    /* PEB 10-2008
     * extract 'prefixed stroke styles' to method addPrefixedStrokeManager, add 'error-stroke...' , 'error-cap-stroke...'
     * and 'error-cap-size'
     */

    private MyCSSEngine createCssEngine(StylableDocument doc, CSSContext ctx, ExtendedParser ep) {
        List<AbstractValueManager> managers = new ArrayList<>(4/*misc symbol*/ + 4 * 8/*strokes*/ + 10/*space*/);

        // error extras
        addPrefixedStrokeManager(managers, "error");
        addPrefixedStrokeManager(managers, "error-cap");
        managers.add(new SimpleLengthManager("error-cap-size", 5f));

        // symbol extras
        addPrefixedStrokeManager(managers, "symbol");
        managers.add(new SVGPaintManager("symbol-fill", ValueConstants.NONE_VALUE));
        managers.add(new OpacityManager("symbol-fill-opacity", false));
        managers.add(new SymbolTypeManager());
        managers.add(new SimpleLengthManager("symbol-size", 5f));

        ValueManager[] valueManagers = (ValueManager[]) managers.toArray(new ValueManager[managers.size()]);
        ParsedURL parsedURL = null;
        if (doc.getURLObject() != null) {
            parsedURL = new ParsedURL(doc.getURLObject());
        }
//        return new SVGCSSEngine(doc, parsedURL, ep,valueManagers,new ShorthandManager[0], ctx);
        return new MyCSSEngine(doc, parsedURL, ep, valueManagers, new ShorthandManager[0], ctx);
    }

    /**
     * add all necessary <code>ValueManager</code>s to style a line, prefix the
     * properties with 'prefix' + '-'
     *
     * @param target only <code>ValueManager</code> objects are added
     * @param prefix a prefix like 'symbol', without the following '-'
     */
    private static void addPrefixedStrokeManager(Collection<AbstractValueManager> target, String prefix) {
        final String pfx = prefix + "-";

        target.add(new SVGPaintManager(pfx + "stroke", ValueConstants.NONE_VALUE));
        target.add(new OpacityManager(pfx + "stroke-opacity", false));

        target.add(new StrokeDasharrayManager() {
            public String getPropertyName() {
                return pfx + super.getPropertyName();
            }
        });
        target.add(new StrokeDashoffsetManager() {
            public String getPropertyName() {
                return pfx + super.getPropertyName();
            }
        });
        target.add(new StrokeLinecapManager() {
            public String getPropertyName() {
                return pfx + super.getPropertyName();
            }
        });
        target.add(new StrokeLinejoinManager() {
            public String getPropertyName() {
                return pfx + super.getPropertyName();
            }
        });
        target.add(new StrokeMiterlimitManager() {
            public String getPropertyName() {
                return pfx + super.getPropertyName();
            }
        });
        target.add(new StrokeWidthManager() {
            public String getPropertyName() {
                return pfx + super.getPropertyName();
            }
        });
    }


    /**
     * Creates a ViewCSS.
     */
    public ViewCSS createViewCSS(StylableDocument doc) {
        return new CSSOMSVGViewCSS(doc.getCSSEngine());
    }

    /**
     * <b>DOM</b>: Implements {@link
     * org.w3c.dom.DOMImplementation#createDocumentType(String, String, String)}.
     */
    public DocumentType createDocumentType(String qualifiedName,
                                           String publicId,
                                           String systemId) {
        throw new DOMException(DOMException.NOT_SUPPORTED_ERR,
                formatMessage("doctype.not.supported", null));
    }

    /**
     * <b>DOM</b>: Implements {@link
     * org.w3c.dom.DOMImplementation#createDocument(String, String, org.w3c.dom.DocumentType)}.
     */
    public Document createDocument(String namespaceURI,
                                   String qualifiedName,
                                   DocumentType doctype)
            throws DOMException {
        Document result = new StylableDocument(doctype, this);
        result.appendChild(result.createElementNS(namespaceURI,
                qualifiedName));
        return result;
    }

    // DOMImplementationCSS /////////////////////////////////////////////////

    /**
     * <b>DOM</b>: Implements {@link
     * org.w3c.dom.css.DOMImplementationCSS#createCSSStyleSheet(String, String)}.
     */
    public CSSStyleSheet createCSSStyleSheet(String title, String media) {
        throw new InternalError("Not implemented");
    }

    // CSSStyleDeclarationFactory ///////////////////////////////////////////

    /**
     * Creates a style declaration.
     *
     * @return a CSSOMStyleDeclaration instance.
     */
    public CSSStyleDeclaration createCSSStyleDeclaration() {
        throw new InternalError("Not implemented");
    }

    // Localizable //////////////////////////////////////////////////////

    /**
     * Implements {@link Localizable#setLocale(java.util.Locale)}.
     */
    public void setLocale(Locale l) {
        localizableSupport.setLocale(l);
    }

    /**
     * Implements {@link Localizable#getLocale()}.
     */
    public Locale getLocale() {
        return localizableSupport.getLocale();
    }

    /**
     * Implements {@link Localizable#formatMessage(String, Object[])}.
     */
    public String formatMessage(String key, Object[] args)
            throws MissingResourceException {
        return localizableSupport.formatMessage(key, args);
    }

    // StyleSheetFactory /////////////////////////////////////////////

    /**
     * Creates a stylesheet from the data of an xml-stylesheet
     * processing instruction or return null.
     */
    public StyleSheet createStyleSheet(Node n, HashTable attrs) {
        throw new InternalError("Not implemented");
    }

    /**
     * Returns the user-agent stylesheet.
     */
    public CSSStyleSheet getUserAgentStyleSheet() {
        throw new InternalError("Not implemented");
    }

    /**
     * Implements the behavior of Document.createElementNS() for this
     * DOM implementation.
     */
    public Element createElementNS(AbstractDocument document,
                                   String namespaceURI,
                                   String qualifiedName) {


//      String name = DOMUtilities.getLocalName(qualifiedName);
//      String ns = DOMUtilities.getPrefix(qualifiedName);
//	System.out.println(namespaceURI + "  \t  " + qualifiedName);

        if (namespaceURI == null)
            return new StylableElement(qualifiedName.intern(), document);
        else
            return new StylableElementNS(namespaceURI.intern(), qualifiedName.intern(), document);
    }

    /**
     * Creates an DocumentEventSupport object suitable for use with
     * this implementation.
     */
    public DocumentEventSupport createDocumentEventSupport() {
        DocumentEventSupport result = new DocumentEventSupport();
//        result.registerEventFactory("SVGEvents",
//                                    new DocumentEventSupport.EventFactory() {
//                                            public Event createEvent() {
//                                                return new SVGOMEvent();
//                                            }
//                                        });
        return result;
    }


}
