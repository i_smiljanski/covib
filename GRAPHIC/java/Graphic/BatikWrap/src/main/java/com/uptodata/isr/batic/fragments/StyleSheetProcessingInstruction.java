/*****************************************************************************
 * Copyright (C) The Apache Software Foundation. All rights reserved.        *
 * ------------------------------------------------------------------------- *
 * This software is published under the terms of the Apache Software License *
 * version 1.1, a copy of which has been included with this distribution in  *
 * the LICENSE file.                                                         *
 *****************************************************************************/

package com.uptodata.isr.batic.fragments;

import org.apache.batik.css.engine.CSSEngine;
import org.apache.batik.css.engine.CSSStyleSheetNode;
import org.apache.batik.css.engine.StyleSheet;
import org.apache.batik.dom.AbstractDocument;
import org.apache.batik.dom.StyleSheetFactory;
import org.apache.batik.dom.util.HashTable;
import org.apache.batik.util.ParsedURL;
import org.w3c.dom.DOMException;
import org.w3c.dom.Node;

import java.net.MalformedURLException;
import java.net.URL;

/**
 * This class provides an implementation of the 'xml-stylesheet' processing
 * instructions.
 *
 * @author <a href="mailto:stephane@hillion.org">Stephane Hillion</a>
 * @version $Id: SVGStyleSheetProcessingInstruction.java,v 1.6 2003/04/11 13:56:17 vhardy Exp $
 */
public class StyleSheetProcessingInstruction // was SVGStyleSheetProcessingInstruction
        extends org.apache.batik.dom.StyleSheetProcessingInstruction
        implements CSSStyleSheetNode {

    /**
     * The style-sheet.
     */
    protected StyleSheet styleSheet;

    /**
     * Creates a new ProcessingInstruction object.
     */
    protected StyleSheetProcessingInstruction() {
    }

    /**
     * Creates a new ProcessingInstruction object.
     */
    public StyleSheetProcessingInstruction(String data,
                                           AbstractDocument owner,
                                           StyleSheetFactory f) {
        super(data, owner, f);
    }

    /**
     * Returns the URI of the referenced stylesheet.
     */
    public String getStyleSheetURI() {
        CSSDocument svgDoc = (CSSDocument) getOwnerDocument();
        URL url = svgDoc.getURLObject();
        String href = (String) getPseudoAttributes().get("href");
        if (url != null) {
            try {
                return new URL(url, href).toString();
            } catch (MalformedURLException e) {
            }
        }
        return href;
    }

    /**
     * Returns the associated style-sheet.
     */
    public StyleSheet getCSSStyleSheet() {
        if (styleSheet == null) {
            HashTable attrs = getPseudoAttributes();
            String type = (String) attrs.get("type");

            if ("text/css".equals(type)) {
                String title = (String) attrs.get("title");
                String media = (String) attrs.get("media");
                String href = (String) attrs.get("href");
                String alternate = (String) attrs.get("alternate");
                CSSDocument doc = (CSSDocument) getOwnerDocument();
                URL durl = doc.getURLObject();
                URL burl = durl;
                try {
                    burl = new URL(durl, href);
                } catch (Exception ex) {
                }
                CSSEngine e = doc.getCSSEngine();

                styleSheet = e.parseStyleSheet(new ParsedURL(burl), media);
                styleSheet.setAlternate("yes".equals(alternate));
                styleSheet.setTitle(title);
            }
        }
        return styleSheet;
    }

    /**
     * <b>DOM</b>: Implements {@link
     * org.w3c.dom.ProcessingInstruction#setData(String)}.
     */
    public void setData(String data) throws DOMException {
        super.setData(data);
        styleSheet = null;
    }

    /**
     * Returns a new uninitialized instance of this object's class.
     */
    protected Node newNode() {
        return new StyleSheetProcessingInstruction();
    }
}
