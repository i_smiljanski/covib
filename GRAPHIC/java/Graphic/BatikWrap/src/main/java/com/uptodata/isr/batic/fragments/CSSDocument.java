package com.uptodata.isr.batic.fragments;

import java.net.URL;

/**
 * @author PeterBuettner.de
 */
public interface CSSDocument {
    public URL getURLObject();

    public void setURLObject(URL url);

    public void setCSSEngine(MyCSSEngine ctx);

    public MyCSSEngine getCSSEngine();

}
