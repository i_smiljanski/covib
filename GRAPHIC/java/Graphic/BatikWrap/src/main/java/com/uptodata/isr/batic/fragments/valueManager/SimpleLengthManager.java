package com.uptodata.isr.batic.fragments.valueManager;

import org.apache.batik.css.engine.CSSEngine;
import org.apache.batik.css.engine.value.*;
import org.w3c.css.sac.LexicalUnit;
import org.w3c.dom.DOMException;
import org.w3c.dom.css.CSSPrimitiveValue;

/**
 * for e.g. symbol-size and cap-size
 *
 * @author PeterBuettner.de
 */
public class SimpleLengthManager extends LengthManager {
    private final String propertyName;
    private final float defaultValue;

    /**
     * @param propertyName e.g. 'symbol-size'
     * @param defaultValue e.g. '5f'
     */
    public SimpleLengthManager(String propertyName, float defaultValue) {
        this.propertyName = propertyName;
        this.defaultValue = defaultValue;
    }

    /**
     * Implements {@link ValueManager#isInheritedProperty()}.
     */
    public boolean isInheritedProperty() {
        return true;
    }

    @Override
    public boolean isAnimatableProperty() {
        return false;
    }

    @Override
    public boolean isAdditiveProperty() {
        return false;
    }

    @Override
    public int getPropertyType() {
        return 0;
    }

    /**
     * Implements {@link ValueManager#getPropertyName()}.
     */
    public String getPropertyName() {
        return propertyName;
    }

    /**
     * Implements {@link ValueManager#getDefaultValue()}.
     */
    public Value getDefaultValue() {
        return new FloatValue(CSSPrimitiveValue.CSS_NUMBER, defaultValue);
    }

    /**
     * Implements {@link ValueManager#createValue(LexicalUnit, CSSEngine)}.
     */
    public Value createValue(LexicalUnit lu, CSSEngine engine)
            throws DOMException {
        if (lu.getLexicalUnitType() == LexicalUnit.SAC_INHERIT) {
            return InheritValue.INSTANCE;
        }
        return super.createValue(lu, engine);
    }


    /**
     * Indicates the orientation of the property associated with
     * this manager.
     */
    protected int getOrientation() {
        return BOTH_ORIENTATION;
    }
}
