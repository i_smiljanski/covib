/*****************************************************************************
 * Copyright (C) The Apache Software Foundation. All rights reserved.        *
 * ------------------------------------------------------------------------- *
 * This software is published under the terms of the Apache Software License *
 * version 1.1, a copy of which has been included with this distribution in  *
 * the LICENSE file.                                                         *
 *****************************************************************************/

package com.uptodata.isr.batic.fragments;

import org.apache.batik.css.engine.CSSStylableElement;
import org.apache.batik.css.engine.StyleDeclarationProvider;
import org.apache.batik.css.engine.StyleMap;
import org.apache.batik.dom.AbstractDocument;
import org.apache.batik.dom.GenericElementNS;
import org.apache.batik.util.ParsedURL;
import org.w3c.dom.css.CSSStyleDeclaration;


public class StylableElementNS extends GenericElementNS implements CSSStylableElement {

    /**
     * The computed style map.
     */
    protected StyleMap computedStyleMap;

    private CSSStylableElementSupport ses;

    protected StylableElementNS() {
        ses = new CSSStylableElementSupport(this);
    }

    protected StylableElementNS(String nsURI, String name, AbstractDocument owner) {
        super(nsURI, name, owner);
        ses = new CSSStylableElementSupport(this);
//    	System.out.println(name);
    }

    // CSSStylableElement //////////////////////////////////////////
    public StyleMap getComputedStyleMap(String pseudoElement) {
        return computedStyleMap;
    }

    public void setComputedStyleMap(String pseudoElement, StyleMap sm) {
        computedStyleMap = sm;
    }

    public String getXMLId() {
        return getAttributeNS(null, "id");
    }

    public String getCSSClass() {
        return getAttributeNS(null, "class");
    }

    public ParsedURL getCSSBase() {
        return new ParsedURL(ses.getCSSBase());
    }

    public boolean isPseudoInstanceOf(String pseudoClass) {
        return ses.isPseudoInstanceOf(pseudoClass);
    }

    @Override
    public StyleDeclarationProvider getOverrideStyleDeclarationProvider() {
        return null;
    }

    // SVGStylable support ///////////////////////////////////////////////////

    public CSSStyleDeclaration getStyle() {
        return ses.getStyle();
    }
}
