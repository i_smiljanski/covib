package com.uptodata.isr.batic.fragments;

import org.apache.batik.css.engine.CSSEngine;
import org.apache.batik.css.engine.CSSStylableElement;
import org.apache.batik.css.engine.value.Value;
import org.w3c.dom.Element;
import org.w3c.dom.css.CSSPrimitiveValue;
import org.w3c.dom.css.CSSValue;

import java.awt.*;

/**
 * generate java.awt objects from styles
 * <br>
 * Borowed from org.apache.batik.bridge.PaintServer, removed url() colors, bridge,gvt references,
 * CSSUtilities(that needs a svg diocument) added support for symbol colors
 * <br>
 * us an instance in one thread, dont't create a new instance for each question.
 *
 * @author PeterBuettner.de
 */
public class PaintConverter {

    /**
     * enum surrogate
     */
    public static final class StrokeType {
        private static final int iNORMAL = 0;
        private static final int iSYMBOL = 1;
        private static final int iERROR = 2;
        private static final int iERROR_CAP = 3;
        public static final StrokeType NORMAL = new StrokeType(iNORMAL);
        public static final StrokeType SYMBOL = new StrokeType(iSYMBOL);
        public static final StrokeType ERROR = new StrokeType(iERROR);
        public static final StrokeType ERROR_CAP = new StrokeType(iERROR_CAP);

        private final int type;

        private StrokeType(int type) {
            this.type = type;
        }

        public int getType() {
            return type;
        }
    }

    private MyCSSEngine engine;

    private final int FILL_INDEX;
    private final int FILL_OPACITY_INDEX;

    private final StrokeIndices STROKE;
    private final StrokeIndices SYMBOL_STROKE;
    private final StrokeIndices ERROR_STROKE;
    private final StrokeIndices ERROR_CAP_STROKE;
    private final int ERROR_CAP_SIZE;

    private final int SYMBOL_FILL_INDEX;
    private final int SYMBOL_FILL_OPACITY_INDEX;

    private final int SYMBOL_TYPE_INDEX;
    private final int SYMBOL_SIZE_INDEX;

/*
 * PEB 10-2008, stokes and styles for error (-bars), so introduced an holder since
 * so many style indices here
 * 
 */

    private final static class StrokeIndices {
        private final int PAINT;
        private final int OPACITY;

        private final int WIDTH;
        private final int LINECAP;
        private final int LINEJOIN;
        private final int MITERLIMIT;
        private final int DASHARRAY;
        private final int DASHOFFSET;

        /**
         * Hold some indices for a stroke
         *
         * @param engine
         * @param prefix without the trailing '-', if empty ("") then for the simple 'stroke'
         */
        StrokeIndices(CSSEngine engine, String prefix) {
            if (!"".equals(prefix)) {
                prefix = prefix + "-";
            }

            WIDTH = getPropertyIndex(engine, prefix + "stroke-width");
            LINECAP = getPropertyIndex(engine, prefix + "stroke-linecap");
            LINEJOIN = getPropertyIndex(engine, prefix + "stroke-linejoin");
            MITERLIMIT = getPropertyIndex(engine, prefix + "stroke-miterlimit");
            DASHARRAY = getPropertyIndex(engine, prefix + "stroke-dasharray");
            DASHOFFSET = getPropertyIndex(engine, prefix + "stroke-dashoffset");

            PAINT = getPropertyIndex(engine, prefix + "stroke");
            OPACITY = getPropertyIndex(engine, prefix + "stroke-opacity");
        }

    }


    public PaintConverter(MyCSSEngine engine) {
        this.engine = engine;
        // same as svg:
        FILL_INDEX = getPropertyIndex(engine, "fill");
        FILL_OPACITY_INDEX = getPropertyIndex(engine, "fill-opacity");

        STROKE = new StrokeIndices(engine, "");

        // self defined
        SYMBOL_STROKE = new StrokeIndices(engine, "symbol");
        ERROR_STROKE = new StrokeIndices(engine, "error");
        ERROR_CAP_STROKE = new StrokeIndices(engine, "error-cap");

        ERROR_CAP_SIZE = getPropertyIndex(engine, "error-cap-size");

        SYMBOL_FILL_INDEX = getPropertyIndex(engine, "symbol-fill");
        SYMBOL_FILL_OPACITY_INDEX = getPropertyIndex(engine, "symbol-fill-opacity");

        SYMBOL_TYPE_INDEX = getPropertyIndex(engine, "symbol-type");
        SYMBOL_SIZE_INDEX = getPropertyIndex(engine, "symbol-size");
    }

    // throws error if not found
    private static int getPropertyIndex(CSSEngine engine, String prop) {
        int i = engine.getPropertyIndex(prop);
        if (i == -1) throw new Error("css property not defined in engine, property name: '" + prop + "'");
        return i;
    }

    public float convertErrorCapSize(Element e) {
        return getComputedStyle(e, ERROR_CAP_SIZE).getFloatValue();
    }

    public float convertSymbolSize(Element e) {
        return getComputedStyle(e, SYMBOL_SIZE_INDEX).getFloatValue();
    }

    public String convertSymbolType(Element e) {
        return getComputedStyle(e, SYMBOL_TYPE_INDEX).getStringValue();
    }


    /////////////////////////////////////////////////////////////////////////
    // java.awt.Paint
    /////////////////////////////////////////////////////////////////////////

//    /**
//     * Converts for the specified element, its stroke paint properties
//     * to a Paint object.
//     *
//     * @param strokedElement the element interested in a Paint
//     * @param forSymbol for 'symbol-stroke-...' styles or the 'stroke-...' ones
//     * @return 
//     */
//public Paint convertStrokePaint(Element strokedElement, boolean forSymbol) {
//	return convertStrokePaint(strokedElement, forSymbol? SYMBOL_STROKE : STROKE);
//}

    /**
     * Converts for the specified element, its stroke paint properties to a Paint
     * object.
     *
     * @param e    the element on which the stroke-paint is specified
     * @param type see {@link com.uptodata.isr.batic.fragments.PaintConverter.StrokeType}, styles
     *             'symbol-stroke-...', 'stroke-...', 'error-stroke-...', ...
     * @return
     */
    public Paint convertStrokePaint(Element e, StrokeType type) {
        switch (type.getType()) {
            case StrokeType.iNORMAL:
                return convertStrokePaint(e, STROKE);
            case StrokeType.iSYMBOL:
                return convertStrokePaint(e, SYMBOL_STROKE);
            case StrokeType.iERROR:
                return convertStrokePaint(e, ERROR_STROKE);
            case StrokeType.iERROR_CAP:
                return convertStrokePaint(e, ERROR_CAP_STROKE);
            default:
                throw new Error();
        }
    }

    private Paint convertStrokePaint(Element strokedElement, StrokeIndices idc) {
        return convertPaint(strokedElement, idc.PAINT, idc.OPACITY);
    }

    /**
     * Converts for the specified element, its fill paint properties to a Paint
     * object.
     *
     * @param filledElement the element interested in a Paint
     * @param forSymbol     for 'symbol-fill-...' styles or the 'fill-...' ones
     * @return
     */
    public Paint convertFillPaint(Element filledElement, boolean forSymbol) {
        if (forSymbol) {
            return convertPaint(filledElement, SYMBOL_FILL_INDEX, SYMBOL_FILL_OPACITY_INDEX);
        } else {
            return convertPaint(filledElement, FILL_INDEX, FILL_OPACITY_INDEX);
        }
    }

    /**
     * @param elem
     * @param idxPaint   e.g. for 'stroke', 'fill', 'symbol-stroke', etc.
     * @param idxOpacity
     * @return
     */
    private Paint convertPaint(Element elem, int idxPaint, int idxOpacity) {
        return convertPaint(getComputedStyle(elem, idxPaint), convertOpacity(getComputedStyle(elem, idxOpacity)));
    }

    /**
     * Converts a Paint definition to a concrete <tt>java.awt.Paint</tt>
     * instance according to the specified parameters.
     *
     * @param paintDef the paint definition
     * @param opacity  the opacity to consider for the Paint
     * @return
     */
    protected static Paint convertPaint(Value paintDef, float opacity) {
        if (paintDef.getCssValueType() == CSSValue.CSS_PRIMITIVE_VALUE) {
            switch (paintDef.getPrimitiveType()) {
                case CSSPrimitiveValue.CSS_IDENT:
                    return null; // none

                case CSSPrimitiveValue.CSS_RGBCOLOR:
                    return convertColor(paintDef, opacity);
                default:
                    throw new Error(); // can't be reached
            }
        } else { // List
            throw new Error(); // can't be reached
        }
    }

    /**
     * Converts the given Value and opacity to a Color object.
     *
     * @param c       The CSS color to convert.
     * @param opacity The opacity value (0 <= o <= 1).
     * @return
     */
    public static Color convertColor(Value c, float opacity) {
        int r = resolveColorComponent(c.getRed());
        int g = resolveColorComponent(c.getGreen());
        int b = resolveColorComponent(c.getBlue());
        return new Color(r, g, b, Math.round(opacity * 255f));
    }

    /////////////////////////////////////////////////////////////////////////
    // java.awt.stroke
    /////////////////////////////////////////////////////////////////////////

    /**
     * Converts a <tt>Stroke</tt> object defined on the specified element.
     * StyleMap
     *
     * @param e    the element on which the stroke is specified
     * @param type see {@link com.uptodata.isr.batic.fragments.PaintConverter.StrokeType},  styles 'symbol-stroke-...', 'stroke-...', 'error-stroke-...', ...
     * @return
     */
    public Stroke convertStroke(Element e, StrokeType type) {
        switch (type.getType()) {
            case StrokeType.iNORMAL:
                return convertStroke(e, STROKE);
            case StrokeType.iSYMBOL:
                return convertStroke(e, SYMBOL_STROKE);
            case StrokeType.iERROR:
                return convertStroke(e, ERROR_STROKE);
            case StrokeType.iERROR_CAP:
                return convertStroke(e, ERROR_CAP_STROKE);
            default:
                throw new Error();
        }

    }

    private Stroke convertStroke(Element e, StrokeIndices idc) {
        float width = getComputedStyle(e, idc.WIDTH).getFloatValue();
        if (width == 0.0f) return null; // Stop here no stroke should be painted.

        // TODO 10-2008, noticed that those values may return null? check that later (probably the ValueManagers cares, or  yes BasicStroke does)
        int linecap = convertStrokeLinecap(getComputedStyle(e, idc.LINECAP));
        int linejoin = convertStrokeLinejoin(getComputedStyle(e, idc.LINEJOIN));
        float miterlimit = convertStrokeMiterlimit(getComputedStyle(e, idc.MITERLIMIT));
        float[] dasharray = convertStrokeDasharray(getComputedStyle(e, idc.DASHARRAY));

        float dashoffset = 0;
        if (dasharray != null) {
            dashoffset = getComputedStyle(e, idc.DASHOFFSET).getFloatValue();

            // make the dashoffset positive since BasicStroke cannot handle
            // negative values
            if (dashoffset < 0) {
                float dashpatternlength = 0;
                for (int i = 0; i < dasharray.length; i++) {
                    dashpatternlength += dasharray[i];
                }
                // if the dash pattern consists of an odd number of elements,
                // the pattern length must be doubled
                if ((dasharray.length % 2) != 0)
                    dashpatternlength *= 2;

                if (dashpatternlength == 0) {
                    dashoffset = 0;
                } else {
                    while (dashoffset < 0)
                        dashoffset += dashpatternlength;
                }
            }
        }
        return new BasicStroke(width, linecap, linejoin, miterlimit, dasharray, dashoffset);
    }

    /////////////////////////////////////////////////////////////////////////
    // Stroke utility methods
    /////////////////////////////////////////////////////////////////////////

    /**
     * Converts the 'stroke-dasharray' property to a list of float
     * number in user units.
     *
     * @param v the CSS value describing the dasharray property
     * @return
     */
    public static float[] convertStrokeDasharray(Value v) {
        float[] dasharray = null;
        if (v.getCssValueType() == CSSValue.CSS_VALUE_LIST) {
            int length = v.getLength();
            dasharray = new float[length];
            float sum = 0;
            for (int i = 0; i < dasharray.length; ++i) {
                dasharray[i] = v.item(i).getFloatValue();
                sum += dasharray[i];
            }
            if (sum == 0) {
                /* 11.4 - If the sum of the <length>'s is zero, then
                 * the stroke is rendered as if a value of none were specified.
                 */
                dasharray = null;
            }
        }
        return dasharray;
    }

    /**
     * Converts the 'miterlimit' property to the appropriate float number.
     *
     * @param v the CSS value describing the miterlimit property
     * @return
     */
    public static float convertStrokeMiterlimit(Value v) {
        float miterlimit = v.getFloatValue();
        return (miterlimit < 1f) ? 1f : miterlimit;
    }

    /**
     * Converts the 'linecap' property to the appropriate BasicStroke constant.
     *
     * @param v the CSS value describing the linecap property
     * @return
     */
    public static int convertStrokeLinecap(Value v) {
        String s = v.getStringValue();
        switch (s.charAt(0)) {
            case 'b':
                return BasicStroke.CAP_BUTT;
            case 'r':
                return BasicStroke.CAP_ROUND;
            case 's':
                return BasicStroke.CAP_SQUARE;
            default:
                throw new Error(); // can't be reached
        }
    }

    /**
     * Converts the 'linejoin' property to the appropriate BasicStroke
     * constant.
     *
     * @param v the CSS value describing the linejoin property
     * @return
     */
    public static int convertStrokeLinejoin(Value v) {
        String s = v.getStringValue();
        switch (s.charAt(0)) {
            case 'm':
                return BasicStroke.JOIN_MITER;
            case 'r':
                return BasicStroke.JOIN_ROUND;
            case 'b':
                return BasicStroke.JOIN_BEVEL;
            default:
                throw new Error(); // can't be reached
        }
    }


    /////////////////////////////////////////////////////////////////////////
    // Paint utility methods
    /////////////////////////////////////////////////////////////////////////

    /**
     * Returns the value of one color component (0 <= result <= 255).
     *
     * @param v the value that defines the color component
     * @return
     */
    public static int resolveColorComponent(Value v) {
        float f;
        switch (v.getPrimitiveType()) {
            case CSSPrimitiveValue.CSS_PERCENTAGE:
                f = v.getFloatValue();
                f = (f > 100f) ? 100f : (f < 0f) ? 0f : f;
                return Math.round(255f * f / 100f);
            case CSSPrimitiveValue.CSS_NUMBER:
                f = v.getFloatValue();
                f = (f > 255f) ? 255f : (f < 0f) ? 0f : f;
                return Math.round(f);
            default:
                throw new Error(); // can't be reached
        }
    }

    /**
     * Returns the opacity represented by the specified CSSValue.
     *
     * @param v the value that represents the opacity
     * @return the opacity between 0 and 1
     */
    public static float convertOpacity(Value v) {
        float r = v.getFloatValue();
        return (r < 0f) ? 0f : (r > 1f) ? 1f : r;
    }


    /////////////////////////////////////////////////////////////////////////
    // from CSSUtilities
    /////////////////////////////////////////////////////////////////////////

    public Value getComputedStyle(Element e, int property) {
        return engine.getComputedStyle((CSSStylableElement) e, null, property);
    }
}
