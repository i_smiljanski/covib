/*****************************************************************************
 * Copyright (C) The Apache Software Foundation. All rights reserved.        *
 * ------------------------------------------------------------------------- *
 * This software is published under the terms of the Apache Software License *
 * version 1.1, a copy of which has been included with this distribution in  *
 * the LICENSE file.                                                         *
 *****************************************************************************/

package com.uptodata.isr.batic.fragments;

import org.apache.batik.dom.util.SAXDocumentFactory;
import org.apache.batik.dom.util.XLinkSupport;
import org.w3c.dom.Document;
import org.xml.sax.InputSource;
import org.xml.sax.SAXException;
import org.xml.sax.XMLReader;

import javax.xml.parsers.ParserConfigurationException;
import javax.xml.parsers.SAXParserFactory;
import java.io.IOException;
import java.io.InputStream;
import java.io.InterruptedIOException;
import java.io.StringReader;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.Locale;
import java.util.MissingResourceException;
import java.util.ResourceBundle;

/**
 * This class contains methods for creating SVGDocument instances
 * from an URI using SAX2.
 *
 * @author <a href="mailto:stephane@hillion.org">Stephane Hillion</a>
 * @version $Id: SAXSVGDocumentFactory.java,v 1.21 2003/04/11 13:56:10 vhardy Exp $
 */
public class StylableSAXDocumentFactory // was SAXSVGDocumentFactory
        extends SAXDocumentFactory
//    implements SVGDocumentFactory 
{


    /**
     * overwrite super..., since we don't wan't to set a parser class, use jaxP instead
     * <p>
     * Creates a Document.
     *
     * @param is The document input source.
     * @throws java.io.IOException if an error occured while reading the document.
     */
    protected Document createDocument(InputSource is)
            throws IOException {
        try {
            XMLReader parser = null;//XMLReaderFactory.createXMLReader(parserClassName);

            SAXParserFactory pf = SAXParserFactory.newInstance();
            parser = pf.newSAXParser().getXMLReader();

            parser.setContentHandler(this);
            parser.setDTDHandler(this);
            parser.setEntityResolver(this);
            parser.setErrorHandler((errorHandler == null) ? this : errorHandler);

            parser.setFeature("http://xml.org/sax/features/namespaces", true);
            parser.setFeature("http://xml.org/sax/features/namespace-prefixes", true);
            parser.setFeature("http://xml.org/sax/features/validation", isValidating);
            parser.setProperty("http://xml.org/sax/properties/lexical-handler", this);
            parser.parse(is);
        } catch (SAXException e) {
            Exception ex = e.getException();
            if (ex != null && ex instanceof InterruptedIOException) {
                throw (InterruptedIOException) ex;
            }
            throw new IOException(e.getMessage());
        } catch (ParserConfigurationException e) {
            throw new IOException(e.getMessage());
        }

        currentNode = null;
        Document ret = document;
        document = null;
        return ret;
    }


    /**
     * Key used for public identifiers
     */
    public static final String KEY_PUBLIC_IDS = "publicIds";

    /**
     * Key used for public identifiers
     */
    public static final String KEY_SKIPPABLE_PUBLIC_IDS = "skippablePublicIds";

    /**
     * Key used for the skippable DTD substitution
     */
    public static final String KEY_SKIP_DTD = "skipDTD";

    /**
     * Key used for system identifiers
     */
    public static final String KEY_SYSTEM_ID = "systemId.";

    /**
     * The dtd public IDs resource bundle class name.
     */
    protected final static String DTDIDS = "org.apache.batik.dom.svg.resources.dtdids";

    /**
     * Constant for HTTP content type header charset field.
     */
    protected final static String HTTP_CHARSET = "charset";

    /**
     * The accepted DTD public IDs.
     */
    protected static String dtdids;

    /**
     * The DTD public IDs we know we can skip.
     */
    protected static String skippable_dtdids;

    /**
     * The DTD content to use when skipping
     */
    protected static String skip_dtd;

    /**
     * The ResourceBunder for the public and system ids
     */
    protected static ResourceBundle rb;

    /**
     * Creates a new SVGDocumentFactory object.
     *
     * @param parser The SAX2 parser classname.
     */
    public StylableSAXDocumentFactory(String parser) {
        super(StylableDOMImplementation.getDOMImplementation(), parser);
    }

    /**
     * Creates a new SVGDocumentFactory object.
     *
     * @param parser The SAX2 parser classname.
     * @param dd     Whether a document descriptor must be generated.
     */
    public StylableSAXDocumentFactory(String parser, boolean dd) {
        super(StylableDOMImplementation.getDOMImplementation(), parser, dd);
    }


    /**
     * Creates a SVG Document instance.
     *
     * @param uri The document URI.
     * @param inp The document input stream.
     * @throws java.io.IOException if an error occured while reading the document.
     */
    public Document createDocument(String uri, InputStream inp)
            throws IOException {
        Document doc;
        InputSource is = new InputSource(inp);
        is.setSystemId(uri);

        try {
            doc = createDocument(is); // TODO @@PEB@@ i overwrote it here
//        	doc = super.createDocument(is); 
//            doc = super.createDocument (PEB_SVGDOMImplementation.SVG_NAMESPACE_URI, "svg", uri, is);
            if (uri != null) {
                ((StylableDocument) doc).setURLObject(new URL(uri));
            }
        } catch (MalformedURLException e) {
            throw new IOException(e.getMessage());
        }
        return doc;
    }


    /**
     * <b>SAX</b>: Implements {@link
     * org.xml.sax.ContentHandler#startDocument()}.
     */
    public void startDocument() throws SAXException {
        super.startDocument();
//        namespaces.put("", StylableDOMImplementation.SVG_NAMESPACE_URI); // TODO @@PEB@@
        namespaces.put("xlink", XLinkSupport.XLINK_NAMESPACE_URI);
    }

    /**
     * <b>SAX2</b>: Implements {@link
     * org.xml.sax.EntityResolver#resolveEntity(String, String)}.
     */
    public InputSource resolveEntity(String publicId, String systemId)
            throws SAXException {
        try {
            if (rb == null)
                rb = ResourceBundle.getBundle(DTDIDS, Locale.getDefault());

            if (dtdids == null)
                dtdids = rb.getString(KEY_PUBLIC_IDS);

            if (skippable_dtdids == null)
                skippable_dtdids = rb.getString(KEY_SKIPPABLE_PUBLIC_IDS);
            if (skip_dtd == null)
                skip_dtd = rb.getString(KEY_SKIP_DTD);

            if (publicId != null) {
                if (!isValidating &&
                        (skippable_dtdids.indexOf(publicId) != -1)) {
                    // We are not validating and this is a DTD we can
                    // safely skip so do it...  Here we provide just enough
                    // of the DTD to keep stuff running (set svg and
                    // xlink namespaces).
                    return new InputSource(new StringReader(skip_dtd));
                }

                if (dtdids.indexOf(publicId) != -1) {
                    String localSystemId =
                            rb.getString(KEY_SYSTEM_ID +
                                    publicId.replace(' ', '_'));

                    if (localSystemId != null && !"".equals(localSystemId)) {
                        return new InputSource
                                (getClass().getResource(localSystemId).toString());
                    }
                }
            }
        } catch (MissingResourceException e) {
            throw new SAXException(e);
        }
        // Let the SAX parser find the entity.
        return null;
    }
}
