package com.uptodata.isr.batic.fragments;

import org.apache.batik.css.engine.CSSEngine;
import org.apache.batik.css.engine.CSSStylableElement;
import org.w3c.dom.Node;
import org.w3c.dom.css.CSSStyleDeclaration;

import java.net.MalformedURLException;
import java.net.URL;

/**
 * some of the non Element methods of CSSStylableElement, usable for delegation
 *
 * @author PeterBuettner.de
 */
public class CSSStylableElementSupport {
    private final CSSStylableElement elt;

    public CSSStylableElementSupport(CSSStylableElement elt) {
        this.elt = elt;
    }
// Later?:	
//    protected StyleMap computedStyleMap;
//    public StyleMap getComputedStyleMap(String pseudoElement) {return computedStyleMap;}
//    public void setComputedStyleMap(String pseudoElement, StyleMap sm) {computedStyleMap = sm;  }
//    public String getXMLId() {return elt.getAttributeNS(null, "id");}
//    public String getCSSClass() {return elt.getAttributeNS(null, "class"); }


    /**
     * Returns the CSS base URL of this element.
     */
    public URL getCSSBase() {
        try {
//        todo:    String bu = XMLBaseSupport.getCascadedXMLBase(elt);
            String bu = null;
            if (bu == null) {
                return null;
            }
            return new URL(bu);
        } catch (MalformedURLException e) {
            // !!! TODO
            e.printStackTrace();
            throw new InternalError();
        }
    }

    /**
     * Tells whether this element is an instance of the given pseudo
     * class.
     */
    public boolean isPseudoInstanceOf(String pseudoClass) {
        if (pseudoClass.equals("first-child")) {
            Node n = elt.getPreviousSibling();
            while (n != null && n.getNodeType() != Node.ELEMENT_NODE) {
                n = n.getPreviousSibling();
            }
            return n == null;
        }
        return false;
    }

    // SVGStylable support ///////////////////////////////////////////////////

    /**
     * <b>DOM</b>: Implements .
     */

    public CSSStyleDeclaration getStyle() {
        CSSEngine eng = ((CSSDocument) elt.getOwnerDocument()).getCSSEngine();
        return new StyleDeclaration(elt, eng);

    }
}
