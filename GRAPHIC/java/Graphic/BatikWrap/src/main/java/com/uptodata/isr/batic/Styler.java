package com.uptodata.isr.batic;


import com.uptodata.isr.batic.fragments.*;
import org.apache.batik.css.engine.CSSContext;
import org.apache.batik.css.engine.CSSStyleSheetNode;
import org.apache.batik.css.engine.StyleSheet;
import org.apache.batik.dom.GenericProcessingInstruction;
import org.apache.batik.util.ParsedURL;
import org.w3c.dom.DOMImplementation;
import org.w3c.dom.Node;

import java.net.MalformedURLException;
import java.net.URL;
import java.util.List;


/**
 * ask style for a single element<br>
 * <p>
 * &lt;anyTag style="huhu:12" class="c1 c2 c3"/&gt;
 * <p>
 * Apply css styles to a single Element, this is done by creating a dummy
 * document with such an element, set 'style' and 'class' attributes and compute
 * style, so any parent/sibling/... context is absent. Pay attention if we
 * change this later, using a complete document, inheritance shows up.
 * <p>
 * <b>Usage</b>
 * <pre>
 * 	URL userAgentStyleSheet = new File("style.css").toURL();
 * 	List styles = new ArrayList();
 * 	styles.add(userAgentStyleSheet);
 * 	styles.add("dataset{stroke-dashoffset: 9;}");
 * 	styles.add("dataset{stroke:#f4d;}");
 *
 * 	Styler s = new Styler(styles);
 * 	CSSEngine cssEngine = s.getCSSEngine();
 *
 * 	StylableElement elt = s.setStyle("dataset", "", "");
 *
 * 	Paint paint = pc.convertFillPaint(elt, true);
 * 	pc.convertSymbolType(elt);
 * 	pc.convertSymbolSize(elt);
 *  ...
 *
 * </pre>
 * <p>
 * There are limitations, e.g. only media="all" is hardcoded here
 *
 * @author PeterBuettner.de
 */
public class Styler {

    /**
     * the dummy document
     */
    private StylableDocument doc;

    /**
     * creates a <b>single threadable </b> Styler with user definable stylesheets
     *
     * @param styleSheets if a String is found it is seen as plain style declarations like
     *                    liner{fill:red;}, also <code>URL</code> s are understand and
     *                    parsed
     * @throws java.net.MalformedURLException
     */
    public Styler(List styleSheets) throws MalformedURLException {
        ClassLoader loader = Thread.currentThread().getContextClassLoader();
        URL builtin = loader.getResource("builtin.css");
//        URL builtin = getClass().getResource("builtin.css"); // PEB 10-2006 that had a typo, now it is used

        // create dummy document
        DOMImplementation di = StylableDOMImplementation.getDOMImplementation();
        doc = (StylableDocument) di.createDocument(null /*nsURI*/, "summyRoot", null);

        // user agent style come first
        initializeDocument(new SimpleCssContext(), "all", builtin);

        // stylesheets:
        for (Object o : styleSheets) {
            if (o instanceof URL)
                addStyleNode(doc, (URL) o);
                // we need the css engine here for parsing, so do it after initializeDocument
            else if (o instanceof String) {
                StyleSheet css = getCSSEngine().parseStyleSheet((String) o, null, "all");
                addStyleNode(doc, css);
            } else throw new IllegalArgumentException("One style is not a String or URL, is:" + o.getClass());
        }

        // test created style processing instructions
//	try {
//		OutputStreamWriter osw = new OutputStreamWriter(System.out);
//		osw.write("\n-------------------\n");
//		DOMUtilities.writeDocument(doc, osw);
//		osw.write("\n-------------------\n");
//		osw.flush();
//		}
//	catch (IOException ex) { ex.printStackTrace();}

    }

    /**
     * creates and adds a new StyleSheetProcessingInstruction to the document
     *
     * @param document
     * @param url
     */
    private static void addStyleNode(StylableDocument document, URL url) {
        Node n = new StyleSheetProcessingInstruction(
                "type='text/css' " +
                        "href='" + url.toExternalForm() + "'",
                document, null);
        document.appendChild(n);
    }

    private static void addStyleNode(StylableDocument document, StyleSheet css) {
        Node n = new InlineStyleSheetProcessingInstruction(document, css);
        document.appendChild(n);
    }

    private static class InlineStyleSheetProcessingInstruction extends GenericProcessingInstruction
            implements CSSStyleSheetNode {
        private StyleSheet css;

        InlineStyleSheetProcessingInstruction(StylableDocument doc, StyleSheet styleSheet) {
            super("xml-stylesheet", "", doc);
            css = styleSheet;
        }

        public StyleSheet getCSSStyleSheet() {
            return css;
        }
    }


    /**
     * Initializes the given document, connect cssEngine and doc
     *
     * @param cssContext
     * @param media
     * @param userAgentStyleSheet
     */
    private void initializeDocument(CSSContext cssContext, String media, URL userAgentStyleSheet) {
        MyCSSEngine eng = doc.getCSSEngine();
        if (eng == null) {
            StylableDOMImplementation impl = (StylableDOMImplementation) doc.getImplementation();
            eng = impl.createCSSEngine(doc, cssContext);
            doc.setCSSEngine(eng);
            eng.setMedia(media);
            if (userAgentStyleSheet != null) {
                StyleSheet styleSheet = eng.parseStyleSheet(new ParsedURL(userAgentStyleSheet), "all");
                eng.setUserAgentStyleSheet(styleSheet);
            }

//        eng.setAlternateStyleSheet(userAgent.getAlternateStyleSheet());
        }
    }

    /**
     * the CSSEngine you need to get the style values
     *
     * @return
     */
    public MyCSSEngine getCSSEngine() {
        return doc.getCSSEngine();
    }

    /**
     * sets the dummy element properties to the parameters values, one uses the
     * element afterward for asking the styling engine to get styles
     *
     * @param tagName
     * @param cssStyle
     * @param cssClass
     * @return the element you may ask for styles, use getCSSEngine() and e.g. PaintConverter
     */
    public StylableElement setStyle(String tagName, String cssStyle, String cssClass) {
        StylableElement e = (StylableElement) doc.getDocumentElement();
        e.setNodeName(tagName);
        e.setAttribute("style", cssStyle);
        e.setAttribute("class", cssClass);
        e.setComputedStyleMap(null, null);// clear old style
        return e;
    }
}
