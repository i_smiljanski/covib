package com.uptodata.isr.batic;

import com.uptodata.isr.batic.fragments.StylableDocument;
import org.apache.batik.css.engine.CSSContext;
import org.apache.batik.css.engine.CSSEngine;
import org.apache.batik.css.engine.SystemColorSupport;
import org.apache.batik.css.engine.value.Value;
import org.apache.batik.css.engine.value.css2.FontFamilyManager;
import org.apache.batik.util.ParsedURL;
import org.w3c.dom.Element;


/**
 * Simple css context, doesn't handle things right for svg (parent relative
 * sizes, font-family) stripped down for our needs.
 * <p>
 * Borrowed from UserAgentAdapter, so we don't need that bridge-package
 *
 * @author PeterBuettner.de
 */
public class SimpleCssContext implements CSSContext {

    public Value getSystemColor(String ident) {
        return SystemColorSupport.getSystemColor(ident);
    }

    public Value getDefaultFontFamily() {
        // No cache needed since the default font family is asked only
        // one time on the root element (only if it does not have its
        // own font-family).
        return new FontFamilyManager().getDefaultValue();
        // TODO @@PEB@@ changed lots look here
    }


    public float getMediumFontSize() {
        // 9pt (72pt = 1in)
        return 9f * 25.4f / (72f * getPixelUnitToMillimeter());
    }

    public float getLighterFontWeight(float f) {
        return getStandardLighterFontWeight(f);
    }

    public float getBolderFontWeight(float f) {
        return getStandardBolderFontWeight(f);
    }

    public float getPixelUnitToMillimeter() {
        return 25.4f / 96;
    }// 96dpi

    public float getPixelToMillimeter() {
        return getPixelUnitToMillimeter();
    }

    public float getBlockWidth(Element elt) {
        return 100;//getViewport(elt).getWidth(); TODO @@PEB@@ look here
    }

    public float getBlockHeight(Element elt) {
        return 100;//getViewport(elt).getHeight(); TODO @@PEB@@ look here
    }

    public void checkLoadExternalResource(ParsedURL resourceURL, ParsedURL docURL) throws SecurityException {
//		userAgent.checkLoadExternalResource(resourceURL, docURL); // TODO @@PEB@@ all ok!
    }


    public boolean isDynamic() {
        return false;
    }

    public boolean isInteractive() {
        return false;
    }

    @Override
    public CSSEngine getCSSEngineForElement(Element e) {
        return ((StylableDocument) e.getOwnerDocument()).getCSSEngine();
    }

    // -------------------------------------------------------------------------------------
    private static final float[] standardLighterFontWeight = new float[]{1, 1, 2, 3, 4, 4, 4, 4, 4};
    private static final float[] standardBolderFontWeight = new float[]{6, 6, 6, 6, 6, 7, 8, 9, 9};

    private static float getStandardLighterFontWeight(float f) {
        int weight = (int) ((f + 50) / 100); // Round f to nearest 100...
        if (0 < weight && weight < standardLighterFontWeight.length)
            return standardLighterFontWeight[weight] * 100;
        throw new IllegalArgumentException("Bad Font Weight: " + f);
    }

    private static float getStandardBolderFontWeight(float f) {
        int weight = (int) ((f + 50) / 100); // Round f to nearest 100...
        if (0 < weight && weight < standardBolderFontWeight.length)
            return standardBolderFontWeight[weight] * 100;
        throw new IllegalArgumentException("Bad Font Weight: " + f);
    }


}
