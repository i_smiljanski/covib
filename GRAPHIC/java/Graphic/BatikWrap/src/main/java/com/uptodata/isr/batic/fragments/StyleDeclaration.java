package com.uptodata.isr.batic.fragments;

import org.apache.batik.css.dom.CSSOMSVGStyleDeclaration;
import org.apache.batik.css.engine.CSSEngine;
import org.apache.batik.css.engine.CSSStylableElement;
import org.apache.batik.css.engine.value.Value;
import org.apache.batik.dom.svg.LiveAttributeValue;
import org.apache.batik.util.SVGConstants;
import org.w3c.dom.Attr;
import org.w3c.dom.DOMException;


/**
 * This class represents the 'style' attribute.
 */
public class StyleDeclaration
        extends CSSOMSVGStyleDeclaration
        implements LiveAttributeValue, CSSOMSVGStyleDeclaration.ValueProvider,
        CSSOMSVGStyleDeclaration.ModificationHandler {

    private final CSSStylableElement elt;

    /**
     * The associated CSS object.
     */
    protected org.apache.batik.css.engine.StyleDeclaration declaration;

    /**
     * Whether the mutation comes from this object.
     */
    protected boolean mutate;

    /**
     * Creates a new StyleDeclaration.
     */
    public StyleDeclaration(CSSStylableElement element, CSSEngine eng) {
        super(null, null, eng);
        this.elt = element;
        valueProvider = this;
        setModificationHandler(this);
        declaration = cssEngine.parseStyleDeclaration(new StylableElement(), elt.getAttributeNS(null, SVGConstants.SVG_STYLE_ATTRIBUTE));
    }

    // ValueProvider ////////////////////////////////////////

    /**
     * Returns the current value associated with this object.
     */
    public Value getValue(String name) {
        int idx = cssEngine.getPropertyIndex(name);
        for (int i = 0; i < declaration.size(); i++) {
            if (idx == declaration.getIndex(i)) {
                return declaration.getValue(i);
            }
        }
        return null;
    }

    /**
     * Tells whether the given property is important.
     */
    public boolean isImportant(String name) {
        int idx = cssEngine.getPropertyIndex(name);
        for (int i = 0; i < declaration.size(); i++) {
            if (idx == declaration.getIndex(i)) {
                return declaration.getPriority(i);
            }
        }
        return false;
    }

    /**
     * Returns the text of the declaration.
     */
    public String getText() {
        return declaration.toString(cssEngine);
    }

    /**
     * Returns the length of the declaration.
     */
    public int getLength() {
        return declaration.size();
    }

    /**
     * Returns the value at the given.
     */
    public String item(int idx) {
        return cssEngine.getPropertyName(declaration.getIndex(idx));
    }

    // LiveAttributeValue //////////////////////////////////////

    /**
     * Called when an Attr node has been added.
     */
    public void attrAdded(Attr node, String newv) {
        if (!mutate) {
            declaration = cssEngine.parseStyleDeclaration(new StylableElement(), newv);
        }
    }

    /**
     * Called when an Attr node has been modified.
     */
    public void attrModified(Attr node, String oldv, String newv) {
        if (!mutate) {
            declaration = cssEngine.parseStyleDeclaration(new StylableElement(), newv);
        }
    }

    /**
     * Called when an Attr node has been removed.
     */
    public void attrRemoved(Attr node, String oldv) {
        if (!mutate) {
            declaration =
                    new org.apache.batik.css.engine.StyleDeclaration();
        }
    }

    // ModificationHandler ////////////////////////////////////

    /**
     * Called when the value text has changed.
     */
    public void textChanged(String text) throws DOMException {
        declaration = cssEngine.parseStyleDeclaration(new StylableElement(), text);
        mutate = true;
        elt.setAttributeNS(null, SVGConstants.SVG_STYLE_ATTRIBUTE, text);
        mutate = false;
    }

    /**
     * Called when a property was removed.
     */
    public void propertyRemoved(String name) throws DOMException {
        int idx = cssEngine.getPropertyIndex(name);
        for (int i = 0; i < declaration.size(); i++) {
            if (idx == declaration.getIndex(i)) {
                declaration.remove(i);
                mutate = true;
                elt.setAttributeNS(null, SVGConstants.SVG_STYLE_ATTRIBUTE,
                        declaration.toString(cssEngine));
                mutate = false;
                return;
            }
        }
    }

    /**
     * Called when a property was changed.
     */
    public void propertyChanged(String name, String value, String prio)
            throws DOMException {
        int idx = cssEngine.getPropertyIndex(name);
        for (int i = 0; i < declaration.size(); i++) {
            if (idx == declaration.getIndex(i)) {
                Value v = cssEngine.parsePropertyValue(elt, name, value);
                declaration.put(i, v, idx, prio.length() > 0);
                mutate = true;
                elt.setAttributeNS(null, SVGConstants.SVG_STYLE_ATTRIBUTE, declaration.toString(cssEngine));
                mutate = false;
                return;
            }
        }
        Value v = cssEngine.parsePropertyValue(elt, name, value);
        declaration.append(v, idx, prio.length() > 0);
        mutate = true;
        elt.setAttributeNS(null, SVGConstants.SVG_STYLE_ATTRIBUTE, declaration.toString(cssEngine));
        mutate = false;
    }
}