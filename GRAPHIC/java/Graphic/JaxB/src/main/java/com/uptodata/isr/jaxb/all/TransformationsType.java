package com.uptodata.isr.jaxb.all;

import javax.xml.bind.annotation.*;
import java.util.ArrayList;
import java.util.List;


/**
 * Changes all datapoints, transformations are applied in the order they appear here
 * <p>
 * <p>Java-Klasse f(c)r transformationsType complex type.
 * <p>
 * <p>Das folgende Schemafragment gibt den erwarteten Content an, der in dieser Klasse enthalten ist.
 * <p>
 * <pre>
 * &lt;complexType name="transformationsType">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;choice maxOccurs="unbounded" minOccurs="0">
 *         &lt;element name="addLine">
 *           &lt;complexType>
 *             &lt;complexContent>
 *               &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *                 &lt;attribute name="slope" use="required" type="{http://www.w3.org/2001/XMLSchema}double" />
 *                 &lt;attribute name="intercept" use="required" type="{http://www.w3.org/2001/XMLSchema}double" />
 *               &lt;/restriction>
 *             &lt;/complexContent>
 *           &lt;/complexType>
 *         &lt;/element>
 *         &lt;element name="toResidual">
 *           &lt;complexType>
 *             &lt;complexContent>
 *               &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *                 &lt;attribute name="slope" use="required" type="{http://www.w3.org/2001/XMLSchema}double" />
 *                 &lt;attribute name="intercept" use="required" type="{http://www.w3.org/2001/XMLSchema}double" />
 *               &lt;/restriction>
 *             &lt;/complexContent>
 *           &lt;/complexType>
 *         &lt;/element>
 *       &lt;/choice>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "transformationsType", propOrder = {
        "items"
})
public class TransformationsType {

    @XmlElements({
            @XmlElement(name = "addLine", type = TransformationsType.AddLine.class),
            @XmlElement(name = "toResidual", type = TransformationsType.ToResidual.class)
    })
    protected List<Object> items;

    /**
     * Gets the value of the items property.
     * <p>
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the items property.
     * <p>
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getItems().add(newItem);
     * </pre>
     * <p>
     * <p>
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link TransformationsType.AddLine }
     * {@link TransformationsType.ToResidual }
     */
    public List<Object> getItems() {
        if (items == null) {
            items = new ArrayList<Object>();
        }
        return this.items;
    }


    /**
     * Adds a straight line to the
     * datapoints
     * <p>
     * <p>Java-Klasse f(c)r anonymous complex type.
     * <p>
     * <p>Das folgende Schemafragment gibt den erwarteten Content an, der in dieser Klasse enthalten ist.
     * <p>
     * <pre>
     * &lt;complexType>
     *   &lt;complexContent>
     *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
     *       &lt;attribute name="slope" use="required" type="{http://www.w3.org/2001/XMLSchema}double" />
     *       &lt;attribute name="intercept" use="required" type="{http://www.w3.org/2001/XMLSchema}double" />
     *     &lt;/restriction>
     *   &lt;/complexContent>
     * &lt;/complexType>
     * </pre>
     */
    @XmlAccessorType(XmlAccessType.FIELD)
    @XmlType(name = "")
    public static class AddLine {

        @XmlAttribute(name = "slope", required = true)
        protected double slope;
        @XmlAttribute(name = "intercept", required = true)
        protected double intercept;

        /**
         * Ruft den Wert der slope-Eigenschaft ab.
         */
        public double getSlope() {
            return slope;
        }

        /**
         * Legt den Wert der slope-Eigenschaft fest.
         */
        public void setSlope(double value) {
            this.slope = value;
        }

        /**
         * Ruft den Wert der intercept-Eigenschaft ab.
         */
        public double getIntercept() {
            return intercept;
        }

        /**
         * Legt den Wert der intercept-Eigenschaft fest.
         */
        public void setIntercept(double value) {
            this.intercept = value;
        }

    }


    /**
     * Makes residuals out of the points.
     * iow makes: y_new = y_estimate -y_measure
     * <p>
     * <p>
     * <p>Java-Klasse f(c)r anonymous complex type.
     * <p>
     * <p>Das folgende Schemafragment gibt den erwarteten Content an, der in dieser Klasse enthalten ist.
     * <p>
     * <pre>
     * &lt;complexType>
     *   &lt;complexContent>
     *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
     *       &lt;attribute name="slope" use="required" type="{http://www.w3.org/2001/XMLSchema}double" />
     *       &lt;attribute name="intercept" use="required" type="{http://www.w3.org/2001/XMLSchema}double" />
     *     &lt;/restriction>
     *   &lt;/complexContent>
     * &lt;/complexType>
     * </pre>
     */
    @XmlAccessorType(XmlAccessType.FIELD)
    @XmlType(name = "")
    public static class ToResidual {

        @XmlAttribute(name = "slope", required = true)
        protected double slope;
        @XmlAttribute(name = "intercept", required = true)
        protected double intercept;

        /**
         * Ruft den Wert der slope-Eigenschaft ab.
         */
        public double getSlope() {
            return slope;
        }

        /**
         * Legt den Wert der slope-Eigenschaft fest.
         */
        public void setSlope(double value) {
            this.slope = value;
        }

        /**
         * Ruft den Wert der intercept-Eigenschaft ab.
         */
        public double getIntercept() {
            return intercept;
        }

        /**
         * Legt den Wert der intercept-Eigenschaft fest.
         */
        public void setIntercept(double value) {
            this.intercept = value;
        }

    }

}
