package com.uptodata.isr.jaxb.fTestTask;

import javax.xml.bind.annotation.*;


/**
 * <p>Java-Klasse f(c)r anonymous complex type.
 * <p>
 * <p>Das folgende Schemafragment gibt den erwarteten Content an, der in dieser Klasse enthalten ist.
 * <p>
 * <pre>
 * &lt;complexType>
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;attribute name="version" use="required" type="{http://www.w3.org/2001/XMLSchema}string" fixed="1.0" />
 *       &lt;attribute name="passed" use="required" type="{http://www.w3.org/2001/XMLSchema}boolean" />
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "")
@XmlRootElement(name = "fTestResult")
public class FTestResult {

    @XmlAttribute(name = "version", required = true)
    protected String version;
    @XmlAttribute(name = "passed", required = true)
    protected boolean passed;

    /**
     * Ruft den Wert der version-Eigenschaft ab.
     *
     * @return possible object is
     * {@link String }
     */
    public String getVersion() {
        if (version == null) {
            return "1.0";
        } else {
            return version;
        }
    }

    /**
     * Legt den Wert der version-Eigenschaft fest.
     *
     * @param value allowed object is
     *              {@link String }
     */
    public void setVersion(String value) {
        this.version = value;
    }

    /**
     * Ruft den Wert der passed-Eigenschaft ab.
     */
    public boolean isPassed() {
        return passed;
    }

    /**
     * Legt den Wert der passed-Eigenschaft fest.
     */
    public void setPassed(boolean value) {
        this.passed = value;
    }

}
