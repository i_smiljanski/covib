package com.uptodata.isr.jaxb.all;

import javax.xml.bind.annotation.*;
import java.util.ArrayList;
import java.util.List;


/**
 * The content of the plot, datasets, lines, limits, etc.
 * <p>
 * <p>Java-Klasse f(c)r contentType complex type.
 * <p>
 * <p>Das folgende Schemafragment gibt den erwarteten Content an, der in dieser Klasse enthalten ist.
 * <p>
 * <pre>
 * &lt;complexType name="contentType">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;choice maxOccurs="unbounded" minOccurs="0">
 *         &lt;element name="dataset" type="{}datasetVisualType"/>
 *         &lt;element name="limit" type="{}limitVisualType"/>
 *         &lt;element name="line" type="{}lineVisualType"/>
 *         &lt;element name="verticalLine" type="{}verticalLineVisualType"/>
 *         &lt;element name="confidence" type="{}confidenceVisualType"/>
 *       &lt;/choice>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "contentType", propOrder = {
        "items"
})
public class ContentType {

    @XmlElements({
            @XmlElement(name = "dataset", type = DatasetVisualType.class),
            @XmlElement(name = "limit", type = LimitVisualType.class),
            @XmlElement(name = "line", type = LineVisualType.class),
            @XmlElement(name = "verticalLine", type = VerticalLineVisualType.class),
            @XmlElement(name = "confidence", type = ConfidenceVisualType.class)
    })
    protected List<Object> items;

    /**
     * Gets the value of the items property.
     * <p>
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the items property.
     * <p>
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getItems().add(newItem);
     * </pre>
     * <p>
     * <p>
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link DatasetVisualType }
     * {@link LimitVisualType }
     * {@link LineVisualType }
     * {@link VerticalLineVisualType }
     * {@link ConfidenceVisualType }
     */
    public List<Object> getItems() {
        if (items == null) {
            items = new ArrayList<Object>();
        }
        return this.items;
    }

}
