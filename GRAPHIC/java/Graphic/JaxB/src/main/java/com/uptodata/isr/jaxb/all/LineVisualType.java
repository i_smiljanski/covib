package com.uptodata.isr.jaxb.all;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlType;


/**
 * A straight line, e.g. a linear fit
 * <p>
 * <p>Java-Klasse f(c)r lineVisualType complex type.
 * <p>
 * <p>Das folgende Schemafragment gibt den erwarteten Content an, der in dieser Klasse enthalten ist.
 * <p>
 * <pre>
 * &lt;complexType name="lineVisualType">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;attGroup ref="{}visualAttr"/>
 *       &lt;attribute name="slope" type="{http://www.w3.org/2001/XMLSchema}double" />
 *       &lt;attribute name="intercept" type="{http://www.w3.org/2001/XMLSchema}double" />
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "lineVisualType")
public class LineVisualType {

    @XmlAttribute(name = "slope")
    protected Double slope;
    @XmlAttribute(name = "intercept")
    protected Double intercept;
    @XmlAttribute(name = "title")
    protected String title;
    @XmlAttribute(name = "legendId")
    protected String legendId;
    @XmlAttribute(name = "class")
    protected String classes;
    @XmlAttribute(name = "style")
    protected String style;

    /**
     * Ruft den Wert der slope-Eigenschaft ab.
     *
     * @return possible object is
     * {@link Double }
     */
    public Double getSlope() {
        return slope;
    }

    /**
     * Legt den Wert der slope-Eigenschaft fest.
     *
     * @param value allowed object is
     *              {@link Double }
     */
    public void setSlope(Double value) {
        this.slope = value;
    }

    /**
     * Ruft den Wert der intercept-Eigenschaft ab.
     *
     * @return possible object is
     * {@link Double }
     */
    public Double getIntercept() {
        return intercept;
    }

    /**
     * Legt den Wert der intercept-Eigenschaft fest.
     *
     * @param value allowed object is
     *              {@link Double }
     */
    public void setIntercept(Double value) {
        this.intercept = value;
    }

    /**
     * Ruft den Wert der title-Eigenschaft ab.
     *
     * @return possible object is
     * {@link String }
     */
    public String getTitle() {
        return title;
    }

    /**
     * Legt den Wert der title-Eigenschaft fest.
     *
     * @param value allowed object is
     *              {@link String }
     */
    public void setTitle(String value) {
        this.title = value;
    }

    /**
     * Ruft den Wert der legendId-Eigenschaft ab.
     *
     * @return possible object is
     * {@link String }
     */
    public String getLegendId() {
        return legendId;
    }

    /**
     * Legt den Wert der legendId-Eigenschaft fest.
     *
     * @param value allowed object is
     *              {@link String }
     */
    public void setLegendId(String value) {
        this.legendId = value;
    }

    /**
     * Ruft den Wert der classes-Eigenschaft ab.
     *
     * @return possible object is
     * {@link String }
     */
    public String getClasses() {
        return classes;
    }

    /**
     * Legt den Wert der classes-Eigenschaft fest.
     *
     * @param value allowed object is
     *              {@link String }
     */
    public void setClasses(String value) {
        this.classes = value;
    }

    /**
     * Ruft den Wert der style-Eigenschaft ab.
     *
     * @return possible object is
     * {@link String }
     */
    public String getStyle() {
        return style;
    }

    /**
     * Legt den Wert der style-Eigenschaft fest.
     *
     * @param value allowed object is
     *              {@link String }
     */
    public void setStyle(String value) {
        this.style = value;
    }

}
