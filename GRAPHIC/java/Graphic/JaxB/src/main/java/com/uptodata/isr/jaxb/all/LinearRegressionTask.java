package com.uptodata.isr.jaxb.all;

import javax.xml.bind.annotation.*;
import javax.xml.bind.annotation.adapters.XmlJavaTypeAdapter;


/**
 * <p>Java-Klasse f(c)r anonymous complex type.
 * <p>
 * <p>Das folgende Schemafragment gibt den erwarteten Content an, der in dieser Klasse enthalten ist.
 * <p>
 * <pre>
 * &lt;complexType>
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="dataset" type="{}datasetType"/>
 *       &lt;/sequence>
 *       &lt;attribute name="lowerLimit" type="{}optionalDoubleType" />
 *       &lt;attribute name="upperLimit" type="{}optionalDoubleType" />
 *       &lt;attribute name="confidence" use="required" type="{}probability" />
 *       &lt;attribute name="version" use="required" type="{http://www.w3.org/2001/XMLSchema}string" fixed="1.0" />
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "", propOrder = {
        "dataset"
})
@XmlRootElement(name = "linearRegressionTask")
public class LinearRegressionTask {

    @XmlElement(required = true)
    protected DatasetType dataset;
    @XmlAttribute(name = "lowerLimit")
    @XmlJavaTypeAdapter(Adapter1.class)
    protected Double lowerLimit;
    @XmlAttribute(name = "upperLimit")
    @XmlJavaTypeAdapter(Adapter1.class)
    protected Double upperLimit;
    @XmlAttribute(name = "confidence", required = true)
    protected double confidence;
    @XmlAttribute(name = "version", required = true)
    protected String version;

    public LinearRegressionTask() {

    }

    /**
     * Ruft den Wert der dataset-Eigenschaft ab.
     *
     * @return possible object is
     * {@link DatasetType }
     */
    public DatasetType getDataset() {
        return dataset;
    }

    /**
     * Legt den Wert der dataset-Eigenschaft fest.
     *
     * @param value allowed object is
     *              {@link DatasetType }
     */
    public void setDataset(DatasetType value) {
        this.dataset = value;
    }

    /**
     * Ruft den Wert der lowerLimit-Eigenschaft ab.
     *
     * @return possible object is
     * {@link String }
     */
    public Double getLowerLimit() {
        return lowerLimit;
    }

    /**
     * Legt den Wert der lowerLimit-Eigenschaft fest.
     *
     * @param value allowed object is
     *              {@link String }
     */
    public void setLowerLimit(Double value) {
        this.lowerLimit = value;
    }

    /**
     * Ruft den Wert der upperLimit-Eigenschaft ab.
     *
     * @return possible object is
     * {@link String }
     */
    public Double getUpperLimit() {
        return upperLimit;
    }

    /**
     * Legt den Wert der upperLimit-Eigenschaft fest.
     *
     * @param value allowed object is
     *              {@link String }
     */
    public void setUpperLimit(Double value) {
        this.upperLimit = value;
    }

    /**
     * Ruft den Wert der confidence-Eigenschaft ab.
     */
    public double getConfidence() {
        return confidence;
    }

    /**
     * Legt den Wert der confidence-Eigenschaft fest.
     */
    public void setConfidence(double value) {
        this.confidence = value;
    }

    /**
     * Ruft den Wert der version-Eigenschaft ab.
     *
     * @return possible object is
     * {@link String }
     */
    public String getVersion() {
        if (version == null) {
            return "1.0";
        } else {
            return version;
        }
    }

    /**
     * Legt den Wert der version-Eigenschaft fest.
     *
     * @param value allowed object is
     *              {@link String }
     */
    public void setVersion(String value) {
        this.version = value;
    }

}
