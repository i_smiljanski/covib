package com.uptodata.isr.jaxb.all;

import javax.xml.bind.annotation.*;


/**
 * Confidence area, maybe halfsided
 * <p>
 * <p>Java-Klasse f(c)r confidenceVisualType complex type.
 * <p>
 * <p>Das folgende Schemafragment gibt den erwarteten Content an, der in dieser Klasse enthalten ist.
 * <p>
 * <pre>
 * &lt;complexType name="confidenceVisualType">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;attGroup ref="{}visualAttr"/>
 *       &lt;attribute name="n">
 *         &lt;simpleType>
 *           &lt;restriction base="{http://www.w3.org/2001/XMLSchema}int">
 *             &lt;minInclusive value="3"/>
 *           &lt;/restriction>
 *         &lt;/simpleType>
 *       &lt;/attribute>
 *       &lt;attribute name="slope" type="{http://www.w3.org/2001/XMLSchema}double" />
 *       &lt;attribute name="intercept" type="{http://www.w3.org/2001/XMLSchema}double" />
 *       &lt;attribute name="xMean" type="{http://www.w3.org/2001/XMLSchema}double" />
 *       &lt;attribute name="xVariance" type="{}posDoubleType" />
 *       &lt;attribute name="sumSqrResidual" type="{}posDoubleType" />
 *       &lt;attribute name="confidence" use="required" type="{}probability" />
 *       &lt;attribute name="bounds" default="both">
 *         &lt;simpleType>
 *           &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
 *             &lt;enumeration value="upper"/>
 *             &lt;enumeration value="lower"/>
 *             &lt;enumeration value="both"/>
 *           &lt;/restriction>
 *         &lt;/simpleType>
 *       &lt;/attribute>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "confidenceVisualType")
public class ConfidenceVisualType {

    @XmlAttribute(name = "n")
    protected Integer n;
    @XmlAttribute(name = "slope")
    protected Double slope;
    @XmlAttribute(name = "intercept")
    protected Double intercept;
    @XmlAttribute(name = "xMean")
    protected Double xMean;
    @XmlAttribute(name = "xVariance")
    protected Double xVariance;
    @XmlAttribute(name = "sumSqrResidual")
    protected Double sumSqrResidual;
    @XmlAttribute(name = "confidence", required = true)
    protected double confidence;
    @XmlAttribute(name = "bounds")
    protected ConfidenceVisualType.ConfidenceBounds bounds;
    @XmlAttribute(name = "title")
    protected String title;
    @XmlAttribute(name = "legendId")
    protected String legendId;
    @XmlAttribute(name = "class")
    protected String classes;
    @XmlAttribute(name = "style")
    protected String style;

    /**
     * Ruft den Wert der n-Eigenschaft ab.
     *
     * @return possible object is
     * {@link Integer }
     */
    public Integer getN() {
        return n;
    }

    /**
     * Legt den Wert der n-Eigenschaft fest.
     *
     * @param value allowed object is
     *              {@link Integer }
     */
    public void setN(Integer value) {
        this.n = value;
    }

    /**
     * Ruft den Wert der slope-Eigenschaft ab.
     *
     * @return possible object is
     * {@link Double }
     */
    public Double getSlope() {
        return slope;
    }

    /**
     * Legt den Wert der slope-Eigenschaft fest.
     *
     * @param value allowed object is
     *              {@link Double }
     */
    public void setSlope(Double value) {
        this.slope = value;
    }

    /**
     * Ruft den Wert der intercept-Eigenschaft ab.
     *
     * @return possible object is
     * {@link Double }
     */
    public Double getIntercept() {
        return intercept;
    }

    /**
     * Legt den Wert der intercept-Eigenschaft fest.
     *
     * @param value allowed object is
     *              {@link Double }
     */
    public void setIntercept(Double value) {
        this.intercept = value;
    }

    /**
     * Ruft den Wert der xMean-Eigenschaft ab.
     *
     * @return possible object is
     * {@link Double }
     */
    public Double getXMean() {
        return xMean;
    }

    /**
     * Legt den Wert der xMean-Eigenschaft fest.
     *
     * @param value allowed object is
     *              {@link Double }
     */
    public void setXMean(Double value) {
        this.xMean = value;
    }

    /**
     * Ruft den Wert der xVariance-Eigenschaft ab.
     *
     * @return possible object is
     * {@link Double }
     */
    public Double getXVariance() {
        return xVariance;
    }

    /**
     * Legt den Wert der xVariance-Eigenschaft fest.
     *
     * @param value allowed object is
     *              {@link Double }
     */
    public void setXVariance(Double value) {
        this.xVariance = value;
    }

    /**
     * Ruft den Wert der sumSqrResidual-Eigenschaft ab.
     *
     * @return possible object is
     * {@link Double }
     */
    public Double getSumSqrResidual() {
        return sumSqrResidual;
    }

    /**
     * Legt den Wert der sumSqrResidual-Eigenschaft fest.
     *
     * @param value allowed object is
     *              {@link Double }
     */
    public void setSumSqrResidual(Double value) {
        this.sumSqrResidual = value;
    }

    /**
     * Ruft den Wert der confidence-Eigenschaft ab.
     */
    public double getConfidence() {
        return confidence;
    }

    /**
     * Legt den Wert der confidence-Eigenschaft fest.
     */
    public void setConfidence(double value) {
        this.confidence = value;
    }

    /**
     * Ruft den Wert der bounds-Eigenschaft ab.
     *
     * @return possible object is
     * {@link ConfidenceVisualType.ConfidenceBounds }
     */
    public ConfidenceVisualType.ConfidenceBounds getBounds() {
        if (bounds == null) {
            return ConfidenceVisualType.ConfidenceBounds.BOTH;
        } else {
            return bounds;
        }
    }

    /**
     * Legt den Wert der bounds-Eigenschaft fest.
     *
     * @param value allowed object is
     *              {@link ConfidenceVisualType.ConfidenceBounds }
     */
    public void setBounds(ConfidenceVisualType.ConfidenceBounds value) {
        this.bounds = value;
    }

    /**
     * Ruft den Wert der title-Eigenschaft ab.
     *
     * @return possible object is
     * {@link String }
     */
    public String getTitle() {
        return title;
    }

    /**
     * Legt den Wert der title-Eigenschaft fest.
     *
     * @param value allowed object is
     *              {@link String }
     */
    public void setTitle(String value) {
        this.title = value;
    }

    /**
     * Ruft den Wert der legendId-Eigenschaft ab.
     *
     * @return possible object is
     * {@link String }
     */
    public String getLegendId() {
        return legendId;
    }

    /**
     * Legt den Wert der legendId-Eigenschaft fest.
     *
     * @param value allowed object is
     *              {@link String }
     */
    public void setLegendId(String value) {
        this.legendId = value;
    }

    /**
     * Ruft den Wert der classes-Eigenschaft ab.
     *
     * @return possible object is
     * {@link String }
     */
    public String getClasses() {
        return classes;
    }

    /**
     * Legt den Wert der classes-Eigenschaft fest.
     *
     * @param value allowed object is
     *              {@link String }
     */
    public void setClasses(String value) {
        this.classes = value;
    }

    /**
     * Ruft den Wert der style-Eigenschaft ab.
     *
     * @return possible object is
     * {@link String }
     */
    public String getStyle() {
        return style;
    }

    /**
     * Legt den Wert der style-Eigenschaft fest.
     *
     * @param value allowed object is
     *              {@link String }
     */
    public void setStyle(String value) {
        this.style = value;
    }


    /**
     * <p>Java-Klasse f(c)r null.
     * <p>
     * <p>Das folgende Schemafragment gibt den erwarteten Content an, der in dieser Klasse enthalten ist.
     * <p>
     * <pre>
     * &lt;simpleType>
     *   &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
     *     &lt;enumeration value="upper"/>
     *     &lt;enumeration value="lower"/>
     *     &lt;enumeration value="both"/>
     *   &lt;/restriction>
     * &lt;/simpleType>
     * </pre>
     */
    @XmlType(name = "")
    @XmlEnum
    public enum ConfidenceBounds {

        @XmlEnumValue("upper")
        UPPER("upper"),
        @XmlEnumValue("lower")
        LOWER("lower"),
        @XmlEnumValue("both")
        BOTH("both");
        private final String value;

        ConfidenceBounds(String v) {
            value = v;
        }

        public String value() {
            return value;
        }

        public static ConfidenceVisualType.ConfidenceBounds fromValue(String v) {
            for (ConfidenceVisualType.ConfidenceBounds c : ConfidenceVisualType.ConfidenceBounds.values()) {
                if (c.value.equals(v)) {
                    return c;
                }
            }
            throw new IllegalArgumentException(v);
        }

    }

}
