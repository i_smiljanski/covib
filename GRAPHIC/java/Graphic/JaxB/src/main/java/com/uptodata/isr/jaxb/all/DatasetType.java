package com.uptodata.isr.jaxb.all;

import javax.xml.bind.annotation.*;
import javax.xml.bind.annotation.adapters.XmlJavaTypeAdapter;
import java.util.ArrayList;
import java.util.List;


/**
 * <p>Java-Klasse f(c)r datasetType complex type.
 * <p>
 * <p>Das folgende Schemafragment gibt den erwarteten Content an, der in dieser Klasse enthalten ist.
 * <p>
 * <pre>
 * &lt;complexType name="datasetType">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="transformations" type="{}transformationsType" minOccurs="0"/>
 *         &lt;element name="value" maxOccurs="unbounded" minOccurs="0">
 *           &lt;complexType>
 *             &lt;complexContent>
 *               &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *                 &lt;attribute name="x" use="required" type="{http://www.w3.org/2001/XMLSchema}double" />
 *                 &lt;attribute name="y" use="required" type="{http://www.w3.org/2001/XMLSchema}double" />
 *                 &lt;attribute name="xErrMinus">
 *                   &lt;simpleType>
 *                     &lt;restriction base="{}optionalDoubleType">
 *                       &lt;maxInclusive value="0"/>
 *                     &lt;/restriction>
 *                   &lt;/simpleType>
 *                 &lt;/attribute>
 *                 &lt;attribute name="xErrPlus">
 *                   &lt;simpleType>
 *                     &lt;restriction base="{}optionalDoubleType">
 *                       &lt;minInclusive value="0"/>
 *                     &lt;/restriction>
 *                   &lt;/simpleType>
 *                 &lt;/attribute>
 *                 &lt;attribute name="yErrMinus">
 *                   &lt;simpleType>
 *                     &lt;restriction base="{}optionalDoubleType">
 *                       &lt;maxInclusive value="0"/>
 *                     &lt;/restriction>
 *                   &lt;/simpleType>
 *                 &lt;/attribute>
 *                 &lt;attribute name="yErrPlus">
 *                   &lt;simpleType>
 *                     &lt;restriction base="{}optionalDoubleType">
 *                       &lt;minInclusive value="0"/>
 *                     &lt;/restriction>
 *                   &lt;/simpleType>
 *                 &lt;/attribute>
 *               &lt;/restriction>
 *             &lt;/complexContent>
 *           &lt;/complexType>
 *         &lt;/element>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "datasetType", propOrder = {
        "transformations",
        "values"
})
@XmlSeeAlso({
        DatasetVisualType.class
})
public class DatasetType {

    protected TransformationsType transformations;
    @XmlElement(name = "value")
    protected List<DatasetType.Value> values;

    /**
     * Ruft den Wert der transformations-Eigenschaft ab.
     *
     * @return possible object is
     * {@link TransformationsType }
     */
    public TransformationsType getTransformations() {
        return transformations;
    }

    /**
     * Legt den Wert der transformations-Eigenschaft fest.
     *
     * @param value allowed object is
     *              {@link TransformationsType }
     */
    public void setTransformations(TransformationsType value) {
        this.transformations = value;
    }

    /**
     * Gets the value of the values property.
     * <p>
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the values property.
     * <p>
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getValues().add(newItem);
     * </pre>
     * <p>
     * <p>
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link DatasetType.Value }
     */
    public List<DatasetType.Value> getValues() {
        if (values == null) {
            values = new ArrayList<DatasetType.Value>();
        }
        return this.values;
    }


    /**
     * <p>Java-Klasse f(c)r anonymous complex type.
     * <p>
     * <p>Das folgende Schemafragment gibt den erwarteten Content an, der in dieser Klasse enthalten ist.
     * <p>
     * <pre>
     * &lt;complexType>
     *   &lt;complexContent>
     *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
     *       &lt;attribute name="x" use="required" type="{http://www.w3.org/2001/XMLSchema}double" />
     *       &lt;attribute name="y" use="required" type="{http://www.w3.org/2001/XMLSchema}double" />
     *       &lt;attribute name="xErrMinus">
     *         &lt;simpleType>
     *           &lt;restriction base="{}optionalDoubleType">
     *             &lt;maxInclusive value="0"/>
     *           &lt;/restriction>
     *         &lt;/simpleType>
     *       &lt;/attribute>
     *       &lt;attribute name="xErrPlus">
     *         &lt;simpleType>
     *           &lt;restriction base="{}optionalDoubleType">
     *             &lt;minInclusive value="0"/>
     *           &lt;/restriction>
     *         &lt;/simpleType>
     *       &lt;/attribute>
     *       &lt;attribute name="yErrMinus">
     *         &lt;simpleType>
     *           &lt;restriction base="{}optionalDoubleType">
     *             &lt;maxInclusive value="0"/>
     *           &lt;/restriction>
     *         &lt;/simpleType>
     *       &lt;/attribute>
     *       &lt;attribute name="yErrPlus">
     *         &lt;simpleType>
     *           &lt;restriction base="{}optionalDoubleType">
     *             &lt;minInclusive value="0"/>
     *           &lt;/restriction>
     *         &lt;/simpleType>
     *       &lt;/attribute>
     *     &lt;/restriction>
     *   &lt;/complexContent>
     * &lt;/complexType>
     * </pre>
     */
    @XmlAccessorType(XmlAccessType.FIELD)
    @XmlType(name = "")
    public static class Value {

        @XmlAttribute(name = "x", required = true)
        protected double x;
        @XmlAttribute(name = "y", required = true)
        protected double y;
        @XmlAttribute(name = "xErrMinus")
        @XmlJavaTypeAdapter(Adapter1.class)
        protected Double xErrMinus;
        @XmlAttribute(name = "xErrPlus")
        @XmlJavaTypeAdapter(Adapter1.class)
        protected Double xErrPlus;
        @XmlAttribute(name = "yErrMinus")
        @XmlJavaTypeAdapter(Adapter1.class)
        protected Double yErrMinus;
        @XmlAttribute(name = "yErrPlus")
        @XmlJavaTypeAdapter(Adapter1.class)
        protected Double yErrPlus;

        /**
         * Ruft den Wert der x-Eigenschaft ab.
         */
        public double getX() {
            return x;
        }

        /**
         * Legt den Wert der x-Eigenschaft fest.
         */
        public void setX(double value) {
            this.x = value;
        }

        /**
         * Ruft den Wert der y-Eigenschaft ab.
         */
        public double getY() {
            return y;
        }

        /**
         * Legt den Wert der y-Eigenschaft fest.
         */
        public void setY(double value) {
            this.y = value;
        }

        /**
         * Ruft den Wert der xErrMinus-Eigenschaft ab.
         *
         * @return possible object is
         * {@link String }
         */
        public Double getXErrMinus() {
            return xErrMinus;
        }

        /**
         * Legt den Wert der xErrMinus-Eigenschaft fest.
         *
         * @param value allowed object is
         *              {@link String }
         */
        public void setXErrMinus(Double value) {
            this.xErrMinus = value;
        }

        /**
         * Ruft den Wert der xErrPlus-Eigenschaft ab.
         *
         * @return possible object is
         * {@link String }
         */
        public Double getXErrPlus() {
            return xErrPlus;
        }

        /**
         * Legt den Wert der xErrPlus-Eigenschaft fest.
         *
         * @param value allowed object is
         *              {@link String }
         */
        public void setXErrPlus(Double value) {
            this.xErrPlus = value;
        }

        /**
         * Ruft den Wert der yErrMinus-Eigenschaft ab.
         *
         * @return possible object is
         * {@link String }
         */
        public Double getYErrMinus() {
            return yErrMinus;
        }

        /**
         * Legt den Wert der yErrMinus-Eigenschaft fest.
         *
         * @param value allowed object is
         *              {@link String }
         */
        public void setYErrMinus(Double value) {
            this.yErrMinus = value;
        }

        /**
         * Ruft den Wert der yErrPlus-Eigenschaft ab.
         *
         * @return possible object is
         * {@link String }
         */
        public Double getYErrPlus() {
            return yErrPlus;
        }

        /**
         * Legt den Wert der yErrPlus-Eigenschaft fest.
         *
         * @param value allowed object is
         *              {@link String }
         */
        public void setYErrPlus(Double value) {
            this.yErrPlus = value;
        }

    }

}
