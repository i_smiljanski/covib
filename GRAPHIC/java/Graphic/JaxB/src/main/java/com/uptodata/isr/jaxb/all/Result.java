package com.uptodata.isr.jaxb.all;

import javax.xml.bind.annotation.*;


/**
 * <p>Java-Klasse f(c)r anonymous complex type.
 * <p>
 * <p>Das folgende Schemafragment gibt den erwarteten Content an, der in dieser Klasse enthalten ist.
 * <p>
 * <pre>
 * &lt;complexType>
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="basic" type="{}basicType" minOccurs="0"/>
 *       &lt;/sequence>
 *       &lt;attribute name="n" use="required">
 *         &lt;simpleType>
 *           &lt;restriction base="{http://www.w3.org/2001/XMLSchema}int">
 *             &lt;minInclusive value="0"/>
 *           &lt;/restriction>
 *         &lt;/simpleType>
 *       &lt;/attribute>
 *       &lt;attribute name="version" use="required" type="{http://www.w3.org/2001/XMLSchema}string" fixed="1.0" />
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "", propOrder = {
        "basic"
})
@XmlRootElement(name = "result")
public class Result {

    protected BasicType basic;
    @XmlAttribute(name = "n", required = true)
    protected int n;
    @XmlAttribute(name = "version", required = true)
    protected String version;

    /**
     * Ruft den Wert der basic-Eigenschaft ab.
     *
     * @return possible object is
     * {@link BasicType }
     */
    public BasicType getBasic() {
        return basic;
    }

    /**
     * Legt den Wert der basic-Eigenschaft fest.
     *
     * @param value allowed object is
     *              {@link BasicType }
     */
    public void setBasic(BasicType value) {
        this.basic = value;
    }

    /**
     * Ruft den Wert der n-Eigenschaft ab.
     */
    public int getN() {
        return n;
    }

    /**
     * Legt den Wert der n-Eigenschaft fest.
     */
    public void setN(int value) {
        this.n = value;
    }

    /**
     * Ruft den Wert der version-Eigenschaft ab.
     *
     * @return possible object is
     * {@link String }
     */
    public String getVersion() {
        if (version == null) {
            return "1.0";
        } else {
            return version;
        }
    }

    /**
     * Legt den Wert der version-Eigenschaft fest.
     *
     * @param value allowed object is
     *              {@link String }
     */
    public void setVersion(String value) {
        this.version = value;
    }

}
