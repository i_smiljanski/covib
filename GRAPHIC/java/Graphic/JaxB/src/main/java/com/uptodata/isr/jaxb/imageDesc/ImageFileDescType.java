package com.uptodata.isr.jaxb.imageDesc;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlType;
import javax.xml.bind.annotation.adapters.XmlJavaTypeAdapter;


/**
 * <p>Java-Klasse f(c)r imageFileDescType complex type.
 * <p>
 * <p>Das folgende Schemafragment gibt den erwarteten Content an, der in dieser Klasse enthalten ist.
 * <p>
 * <pre>
 * &lt;complexType name="imageFileDescType">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;attribute name="type" use="required">
 *         &lt;simpleType>
 *           &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
 *             &lt;enumeration value="emf"/>
 *             &lt;enumeration value="svg"/>
 *             &lt;enumeration value="png"/>
 *           &lt;/restriction>
 *         &lt;/simpleType>
 *       &lt;/attribute>
 *       &lt;attribute name="colorDepth" default="32">
 *         &lt;simpleType>
 *           &lt;restriction base="{http://www.w3.org/2001/XMLSchema}int">
 *             &lt;enumeration value="8"/>
 *             &lt;enumeration value="24"/>
 *             &lt;enumeration value="32"/>
 *           &lt;/restriction>
 *         &lt;/simpleType>
 *       &lt;/attribute>
 *       &lt;attribute name="grey" type="{http://www.w3.org/2001/XMLSchema}boolean" default="false" />
 *       &lt;attribute name="compression">
 *         &lt;simpleType>
 *           &lt;restriction base="{}optionalIntegerType">
 *             &lt;minInclusive value="0"/>
 *             &lt;maxInclusive value="9"/>
 *           &lt;/restriction>
 *         &lt;/simpleType>
 *       &lt;/attribute>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "imageFileDescType")
public class ImageFileDescType {

    @XmlAttribute(name = "type", required = true)
    protected String type;
    @XmlAttribute(name = "colorDepth")
    protected Integer colorDepth;
    @XmlAttribute(name = "grey")
    protected Boolean grey;
    @XmlAttribute(name = "compression")
    @XmlJavaTypeAdapter(Adapter2.class)
    protected Integer compression;

    /**
     * Ruft den Wert der type-Eigenschaft ab.
     *
     * @return possible object is
     * {@link String }
     */
    public String getType() {
        return type;
    }

    /**
     * Legt den Wert der type-Eigenschaft fest.
     *
     * @param value allowed object is
     *              {@link String }
     */
    public void setType(String value) {
        this.type = value;
    }

    /**
     * Ruft den Wert der colorDepth-Eigenschaft ab.
     *
     * @return possible object is
     * {@link Integer }
     */
    public int getColorDepth() {
        if (colorDepth == null) {
            return 32;
        } else {
            return colorDepth;
        }
    }

    /**
     * Legt den Wert der colorDepth-Eigenschaft fest.
     *
     * @param value allowed object is
     *              {@link Integer }
     */
    public void setColorDepth(Integer value) {
        this.colorDepth = value;
    }

    /**
     * Ruft den Wert der grey-Eigenschaft ab.
     *
     * @return possible object is
     * {@link Boolean }
     */
    public boolean isGrey() {
        if (grey == null) {
            return false;
        } else {
            return grey;
        }
    }

    /**
     * Legt den Wert der grey-Eigenschaft fest.
     *
     * @param value allowed object is
     *              {@link Boolean }
     */
    public void setGrey(Boolean value) {
        this.grey = value;
    }

    /**
     * Ruft den Wert der compression-Eigenschaft ab.
     *
     * @return possible object is
     * {@link String }
     */
    public Integer getCompression() {
        return compression;
    }

    /**
     * Legt den Wert der compression-Eigenschaft fest.
     *
     * @param value allowed object is
     *              {@link String }
     */
    public void setCompression(Integer value) {
        this.compression = value;
    }

}
