package com.uptodata.isr.jaxb.all;

import javax.xml.bind.annotation.XmlRegistry;


/**
 * This object contains factory methods for each
 * Java content interface and Java element interface
 * generated in the com.uptodata.isr.jaxb.all package.
 * <p>An ObjectFactory allows you to programatically
 * construct new instances of the Java representation
 * for XML content. The Java representation of XML
 * content can consist of schema derived interfaces
 * and classes representing the binding of schema
 * type definitions, element declarations and model
 * groups.  Factory methods for each of these are
 * provided in this class.
 */
@XmlRegistry
public class ObjectFactory {


    /**
     * Create a new ObjectFactory that can be used to create new instances of schema derived classes for package: com.uptodata.isr.jaxb.all
     */
    public ObjectFactory() {
    }

    /**
     * Create an instance of {@link RegressionType }
     */
    public RegressionType createRegressionType() {
        return new RegressionType();
    }

    /**
     * Create an instance of {@link Result }
     */
    public Result createResult() {
        return new Result();
    }

    /**
     * Create an instance of {@link BasicType }
     */
    public BasicType createBasicType() {
        return new BasicType();
    }

    /**
     * Create an instance of {@link XIntersectionType }
     */
    public XIntersectionType createXIntersectionType() {
        return new XIntersectionType();
    }

    /**
     * Create an instance of {@link RegressionType.Intersections }
     */
    public RegressionType.Intersections createRegressionTypeIntersections() {
        return new RegressionType.Intersections();
    }

    public DatasetType.Value createDatasetTypeValue() {
        return new DatasetType.Value();
    }

    public LinearRegressionTask createLinearRegressionTask() {
        return new LinearRegressionTask();
    }

    public Chart createChart() {
        return new Chart();
    }


    public DatasetType createDatasetType() {
        return new DatasetType();
    }


    public DatasetVisualType createDatasetVisualType() {
        return new DatasetVisualType();
    }


    public ContentType createContentType() {
        return new ContentType();
    }


    public TransformationsType createTransformationsType() {
        return new TransformationsType();
    }


    public TransformationsType.ToResidual createTransformationsTypeToResidual() {
        return new TransformationsType.ToResidual();
    }


    public Legend createLegend() {
        return new Legend();
    }


    public TransformationsType.AddLine createTransformationsTypeAddLine() {
        return new TransformationsType.AddLine();
    }


    public LimitVisualType createLimitVisualType() {
        return new LimitVisualType();
    }


    public PlotType createPlotType() {
        return new PlotType();
    }


    public LineVisualType createLineVisualType() {
        return new LineVisualType();
    }


    public VerticalLineVisualType createVerticalLineVisualType() {
        return new VerticalLineVisualType();
    }


    public ConfidenceVisualType createConfidenceVisualType() {
        return new ConfidenceVisualType();
    }


    public AxisType createAxisType() {
        return new AxisType();
    }


}
