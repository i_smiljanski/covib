package com.uptodata.isr.jaxb.all;

import javax.xml.bind.annotation.*;
import javax.xml.bind.annotation.adapters.XmlJavaTypeAdapter;


/**
 * <p>Java-Klasse f(c)r axisType complex type.
 * <p>
 * <p>Das folgende Schemafragment gibt den erwarteten Content an, der in dieser Klasse enthalten ist.
 * <p>
 * <pre>
 * &lt;complexType name="axisType">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;attribute name="title" type="{http://www.w3.org/2001/XMLSchema}string" />
 *       &lt;attribute name="lower" use="required" type="{http://www.w3.org/2001/XMLSchema}double" />
 *       &lt;attribute name="upper" use="required" type="{http://www.w3.org/2001/XMLSchema}double" />
 *       &lt;attribute name="tickWidth" type="{}optionalDoubleType" />
 *       &lt;attribute name="scaleType" default="linear">
 *         &lt;simpleType>
 *           &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
 *             &lt;enumeration value="linear"/>
 *             &lt;enumeration value="log10"/>
 *           &lt;/restriction>
 *         &lt;/simpleType>
 *       &lt;/attribute>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "axisType")
public class AxisType {

    @XmlAttribute(name = "title")
    protected String title;
    @XmlAttribute(name = "lower", required = true)
    protected double lower;
    @XmlAttribute(name = "upper", required = true)
    protected double upper;
    @XmlAttribute(name = "tickWidth")
    @XmlJavaTypeAdapter(Adapter1.class)
    protected Double tickWidth;
    @XmlAttribute(name = "scaleType")
    protected AxisType.AxisScaleType scaleType;

    /**
     * Ruft den Wert der title-Eigenschaft ab.
     *
     * @return possible object is
     * {@link String }
     */
    public String getTitle() {
        return title;
    }

    /**
     * Legt den Wert der title-Eigenschaft fest.
     *
     * @param value allowed object is
     *              {@link String }
     */
    public void setTitle(String value) {
        this.title = value;
    }

    /**
     * Ruft den Wert der lower-Eigenschaft ab.
     */
    public double getLower() {
        return lower;
    }

    /**
     * Legt den Wert der lower-Eigenschaft fest.
     */
    public void setLower(double value) {
        this.lower = value;
    }

    /**
     * Ruft den Wert der upper-Eigenschaft ab.
     */
    public double getUpper() {
        return upper;
    }

    /**
     * Legt den Wert der upper-Eigenschaft fest.
     */
    public void setUpper(double value) {
        this.upper = value;
    }

    /**
     * Ruft den Wert der tickWidth-Eigenschaft ab.
     *
     * @return possible object is
     * {@link String }
     */
    public Double getTickWidth() {
        return tickWidth;
    }

    /**
     * Legt den Wert der tickWidth-Eigenschaft fest.
     *
     * @param value allowed object is
     *              {@link String }
     */
    public void setTickWidth(Double value) {
        this.tickWidth = value;
    }

    /**
     * Ruft den Wert der scaleType-Eigenschaft ab.
     *
     * @return possible object is
     * {@link AxisType.AxisScaleType }
     */
    public AxisType.AxisScaleType getScaleType() {
        if (scaleType == null) {
            return AxisType.AxisScaleType.LINEAR;
        } else {
            return scaleType;
        }
    }

    /**
     * Legt den Wert der scaleType-Eigenschaft fest.
     *
     * @param value allowed object is
     *              {@link AxisType.AxisScaleType }
     */
    public void setScaleType(AxisType.AxisScaleType value) {
        this.scaleType = value;
    }


    /**
     * <p>Java-Klasse f(c)r null.
     * <p>
     * <p>Das folgende Schemafragment gibt den erwarteten Content an, der in dieser Klasse enthalten ist.
     * <p>
     * <pre>
     * &lt;simpleType>
     *   &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
     *     &lt;enumeration value="linear"/>
     *     &lt;enumeration value="log10"/>
     *   &lt;/restriction>
     * &lt;/simpleType>
     * </pre>
     */
    @XmlType(name = "")
    @XmlEnum
    public enum AxisScaleType {

        @XmlEnumValue("linear")
        LINEAR("linear"),
        @XmlEnumValue("log10")
        LOG10("log10");
        private final String value;

        AxisScaleType(String v) {
            value = v;
        }

        public String value() {
            return value;
        }

        public static AxisType.AxisScaleType fromValue(String v) {
            for (AxisType.AxisScaleType c : AxisType.AxisScaleType.values()) {
                if (c.value.equals(v)) {
                    return c;
                }
            }
            throw new IllegalArgumentException(v);
        }

    }

}
