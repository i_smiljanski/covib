package com.uptodata.isr.jaxb.all;

import javax.xml.bind.annotation.*;


/**
 * <p>Java-Klasse f(c)r anonymous complex type.
 * <p>
 * <p>Das folgende Schemafragment gibt den erwarteten Content an, der in dieser Klasse enthalten ist.
 * <p>
 * <pre>
 * &lt;complexType>
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;attribute name="version" type="{http://www.w3.org/2001/XMLSchema}string" fixed="1.0" />
 *       &lt;attribute name="legendId" use="required" type="{http://www.w3.org/2001/XMLSchema}string" />
 *       &lt;attribute name="width" use="required" type="{}posIntType" />
 *       &lt;attribute name="height" use="required" type="{}posIntType" />
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "")
@XmlRootElement(name = "legend")
public class Legend {

    @XmlAttribute(name = "version")
    protected String version;
    @XmlAttribute(name = "legendId", required = true)
    protected String legendId;
    @XmlAttribute(name = "width", required = true)
    protected int width;
    @XmlAttribute(name = "height", required = true)
    protected int height;

    /**
     * Ruft den Wert der version-Eigenschaft ab.
     *
     * @return possible object is
     * {@link String }
     */
    public String getVersion() {
        if (version == null) {
            return "1.0";
        } else {
            return version;
        }
    }

    /**
     * Legt den Wert der version-Eigenschaft fest.
     *
     * @param value allowed object is
     *              {@link String }
     */
    public void setVersion(String value) {
        this.version = value;
    }

    /**
     * Ruft den Wert der legendId-Eigenschaft ab.
     *
     * @return possible object is
     * {@link String }
     */
    public String getLegendId() {
        return legendId;
    }

    /**
     * Legt den Wert der legendId-Eigenschaft fest.
     *
     * @param value allowed object is
     *              {@link String }
     */
    public void setLegendId(String value) {
        this.legendId = value;
    }

    /**
     * Ruft den Wert der width-Eigenschaft ab.
     */
    public int getWidth() {
        return width;
    }

    /**
     * Legt den Wert der width-Eigenschaft fest.
     */
    public void setWidth(int value) {
        this.width = value;
    }

    /**
     * Ruft den Wert der height-Eigenschaft ab.
     */
    public int getHeight() {
        return height;
    }

    /**
     * Legt den Wert der height-Eigenschaft fest.
     */
    public void setHeight(int value) {
        this.height = value;
    }

}
