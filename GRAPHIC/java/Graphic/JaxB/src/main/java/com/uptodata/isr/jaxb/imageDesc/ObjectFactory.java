package com.uptodata.isr.jaxb.imageDesc;

import javax.xml.bind.JAXBElement;
import javax.xml.bind.annotation.XmlElementDecl;
import javax.xml.bind.annotation.XmlRegistry;
import javax.xml.namespace.QName;


/**
 * This object contains factory methods for each
 * Java content interface and Java element interface
 * generated in the com.uptodata.isr.jaxb.imageDesc package.
 * <p>An ObjectFactory allows you to programatically
 * construct new instances of the Java representation
 * for XML content. The Java representation of XML
 * content can consist of schema derived interfaces
 * and classes representing the binding of schema
 * type definitions, element declarations and model
 * groups.  Factory methods for each of these are
 * provided in this class.
 */
@XmlRegistry
public class ObjectFactory {

    private final static QName _ImageFileDesc_QNAME = new QName("", "imageFileDesc");

    /**
     * Create a new ObjectFactory that can be used to create new instances of schema derived classes for package: com.uptodata.isr.jaxb.imageDesc
     */
    public ObjectFactory() {
    }

    /**
     * Create an instance of {@link ImageFileDescType }
     */
    public ImageFileDescType createImageFileDescType() {
        return new ImageFileDescType();
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link ImageFileDescType }{@code >}}
     */
    @XmlElementDecl(namespace = "", name = "imageFileDesc")
    public JAXBElement<ImageFileDescType> createImageFileDesc(ImageFileDescType value) {
        return new JAXBElement<ImageFileDescType>(_ImageFileDesc_QNAME, ImageFileDescType.class, null, value);
    }

}
