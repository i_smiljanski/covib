package com.uptodata.isr.jaxb.all;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlType;


/**
 * can be calculated if xVariance>0 (implies: if n>=2)
 * <p>
 * <p>Java-Klasse f(c)r regressionType complex type.
 * <p>
 * <p>Das folgende Schemafragment gibt den erwarteten Content an, der in dieser Klasse enthalten ist.
 * <p>
 * <pre>
 * &lt;complexType name="regressionType">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="intersections" minOccurs="0">
 *           &lt;complexType>
 *             &lt;complexContent>
 *               &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *                 &lt;all>
 *                   &lt;element name="lowerLinear" type="{}xIntersectionType" minOccurs="0"/>
 *                   &lt;element name="upperLinear" type="{}xIntersectionType" minOccurs="0"/>
 *                   &lt;element name="lowerConfidence" type="{}xIntersectionType" minOccurs="0"/>
 *                   &lt;element name="upperConfidence" type="{}xIntersectionType" minOccurs="0"/>
 *                 &lt;/all>
 *               &lt;/restriction>
 *             &lt;/complexContent>
 *           &lt;/complexType>
 *         &lt;/element>
 *       &lt;/sequence>
 *       &lt;attribute name="slope" use="required" type="{http://www.w3.org/2001/XMLSchema}double" />
 *       &lt;attribute name="intercept" use="required" type="{http://www.w3.org/2001/XMLSchema}double" />
 *       &lt;attribute name="sumSqrResidual" use="required" type="{}doubleNonNegType" />
 *       &lt;attribute name="r" use="required" type="{}doubleNonNegType" />
 *       &lt;attribute name="yMinResidual" use="required" type="{http://www.w3.org/2001/XMLSchema}double" />
 *       &lt;attribute name="yMaxResidual" use="required" type="{http://www.w3.org/2001/XMLSchema}double" />
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "regressionType", propOrder = {
        "intersections"
})
public class RegressionType {

    protected RegressionType.Intersections intersections;
    @XmlAttribute(name = "slope", required = true)
    protected double slope;
    @XmlAttribute(name = "intercept", required = true)
    protected double intercept;
    @XmlAttribute(name = "sumSqrResidual", required = true)
    protected double sumSqrResidual;
    @XmlAttribute(name = "r", required = true)
    protected double r;
    @XmlAttribute(name = "yMinResidual", required = true)
    protected double yMinResidual;
    @XmlAttribute(name = "yMaxResidual", required = true)
    protected double yMaxResidual;

    /**
     * Ruft den Wert der intersections-Eigenschaft ab.
     *
     * @return possible object is
     * {@link RegressionType.Intersections }
     */
    public RegressionType.Intersections getIntersections() {
        return intersections;
    }

    /**
     * Legt den Wert der intersections-Eigenschaft fest.
     *
     * @param value allowed object is
     *              {@link RegressionType.Intersections }
     */
    public void setIntersections(RegressionType.Intersections value) {
        this.intersections = value;
    }

    /**
     * Ruft den Wert der slope-Eigenschaft ab.
     */
    public double getSlope() {
        return slope;
    }

    /**
     * Legt den Wert der slope-Eigenschaft fest.
     */
    public void setSlope(double value) {
        this.slope = value;
    }

    /**
     * Ruft den Wert der intercept-Eigenschaft ab.
     */
    public double getIntercept() {
        return intercept;
    }

    /**
     * Legt den Wert der intercept-Eigenschaft fest.
     */
    public void setIntercept(double value) {
        this.intercept = value;
    }

    /**
     * Ruft den Wert der sumSqrResidual-Eigenschaft ab.
     */
    public double getSumSqrResidual() {
        return sumSqrResidual;
    }

    /**
     * Legt den Wert der sumSqrResidual-Eigenschaft fest.
     */
    public void setSumSqrResidual(double value) {
        this.sumSqrResidual = value;
    }

    /**
     * Ruft den Wert der r-Eigenschaft ab.
     */
    public double getR() {
        return r;
    }

    /**
     * Legt den Wert der r-Eigenschaft fest.
     */
    public void setR(double value) {
        this.r = value;
    }

    /**
     * Ruft den Wert der yMinResidual-Eigenschaft ab.
     */
    public double getYMinResidual() {
        return yMinResidual;
    }

    /**
     * Legt den Wert der yMinResidual-Eigenschaft fest.
     */
    public void setYMinResidual(double value) {
        this.yMinResidual = value;
    }

    /**
     * Ruft den Wert der yMaxResidual-Eigenschaft ab.
     */
    public double getYMaxResidual() {
        return yMaxResidual;
    }

    /**
     * Legt den Wert der yMaxResidual-Eigenschaft fest.
     */
    public void setYMaxResidual(double value) {
        this.yMaxResidual = value;
    }


    /**
     * <p>Java-Klasse f(c)r anonymous complex type.
     * <p>
     * <p>Das folgende Schemafragment gibt den erwarteten Content an, der in dieser Klasse enthalten ist.
     * <p>
     * <pre>
     * &lt;complexType>
     *   &lt;complexContent>
     *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
     *       &lt;all>
     *         &lt;element name="lowerLinear" type="{}xIntersectionType" minOccurs="0"/>
     *         &lt;element name="upperLinear" type="{}xIntersectionType" minOccurs="0"/>
     *         &lt;element name="lowerConfidence" type="{}xIntersectionType" minOccurs="0"/>
     *         &lt;element name="upperConfidence" type="{}xIntersectionType" minOccurs="0"/>
     *       &lt;/all>
     *     &lt;/restriction>
     *   &lt;/complexContent>
     * &lt;/complexType>
     * </pre>
     */
    @XmlAccessorType(XmlAccessType.FIELD)
    @XmlType(name = "", propOrder = {

    })
    public static class Intersections {

        protected XIntersectionType lowerLinear;
        protected XIntersectionType upperLinear;
        protected XIntersectionType lowerConfidence;
        protected XIntersectionType upperConfidence;

        /**
         * Ruft den Wert der lowerLinear-Eigenschaft ab.
         *
         * @return possible object is
         * {@link XIntersectionType }
         */
        public XIntersectionType getLowerLinear() {
            return lowerLinear;
        }

        /**
         * Legt den Wert der lowerLinear-Eigenschaft fest.
         *
         * @param value allowed object is
         *              {@link XIntersectionType }
         */
        public void setLowerLinear(XIntersectionType value) {
            this.lowerLinear = value;
        }

        /**
         * Ruft den Wert der upperLinear-Eigenschaft ab.
         *
         * @return possible object is
         * {@link XIntersectionType }
         */
        public XIntersectionType getUpperLinear() {
            return upperLinear;
        }

        /**
         * Legt den Wert der upperLinear-Eigenschaft fest.
         *
         * @param value allowed object is
         *              {@link XIntersectionType }
         */
        public void setUpperLinear(XIntersectionType value) {
            this.upperLinear = value;
        }

        /**
         * Ruft den Wert der lowerConfidence-Eigenschaft ab.
         *
         * @return possible object is
         * {@link XIntersectionType }
         */
        public XIntersectionType getLowerConfidence() {
            return lowerConfidence;
        }

        /**
         * Legt den Wert der lowerConfidence-Eigenschaft fest.
         *
         * @param value allowed object is
         *              {@link XIntersectionType }
         */
        public void setLowerConfidence(XIntersectionType value) {
            this.lowerConfidence = value;
        }

        /**
         * Ruft den Wert der upperConfidence-Eigenschaft ab.
         *
         * @return possible object is
         * {@link XIntersectionType }
         */
        public XIntersectionType getUpperConfidence() {
            return upperConfidence;
        }

        /**
         * Legt den Wert der upperConfidence-Eigenschaft fest.
         *
         * @param value allowed object is
         *              {@link XIntersectionType }
         */
        public void setUpperConfidence(XIntersectionType value) {
            this.upperConfidence = value;
        }

    }

}
