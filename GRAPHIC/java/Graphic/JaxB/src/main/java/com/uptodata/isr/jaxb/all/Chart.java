package com.uptodata.isr.jaxb.all;

import javax.xml.bind.annotation.*;


/**
 * <p>Java-Klasse f(c)r anonymous complex type.
 * <p>
 * <p>Das folgende Schemafragment gibt den erwarteten Content an, der in dieser Klasse enthalten ist.
 * <p>
 * <pre>
 * &lt;complexType>
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="plot" type="{}plotType"/>
 *       &lt;/sequence>
 *       &lt;attribute name="title" type="{http://www.w3.org/2001/XMLSchema}string" />
 *       &lt;attribute name="version" type="{http://www.w3.org/2001/XMLSchema}string" fixed="1.0" />
 *       &lt;attribute name="width" use="required" type="{}posIntType" />
 *       &lt;attribute name="height" use="required" type="{}posIntType" />
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "", propOrder = {
        "plot"
})
@XmlRootElement(name = "chart")
public class Chart {

    @XmlElement(required = true)
    protected PlotType plot;
    @XmlAttribute(name = "title")
    protected String title;
    @XmlAttribute(name = "version")
    protected String version;
    @XmlAttribute(name = "width", required = true)
    protected int width;
    @XmlAttribute(name = "height", required = true)
    protected int height;

    /**
     * Ruft den Wert der plot-Eigenschaft ab.
     *
     * @return possible object is
     * {@link PlotType }
     */
    public PlotType getPlot() {
        return plot;
    }

    /**
     * Legt den Wert der plot-Eigenschaft fest.
     *
     * @param value allowed object is
     *              {@link PlotType }
     */
    public void setPlot(PlotType value) {
        this.plot = value;
    }

    /**
     * Ruft den Wert der title-Eigenschaft ab.
     *
     * @return possible object is
     * {@link String }
     */
    public String getTitle() {
        return title;
    }

    /**
     * Legt den Wert der title-Eigenschaft fest.
     *
     * @param value allowed object is
     *              {@link String }
     */
    public void setTitle(String value) {
        this.title = value;
    }

    /**
     * Ruft den Wert der version-Eigenschaft ab.
     *
     * @return possible object is
     * {@link String }
     */
    public String getVersion() {
        if (version == null) {
            return "1.0";
        } else {
            return version;
        }
    }

    /**
     * Legt den Wert der version-Eigenschaft fest.
     *
     * @param value allowed object is
     *              {@link String }
     */
    public void setVersion(String value) {
        this.version = value;
    }

    /**
     * Ruft den Wert der width-Eigenschaft ab.
     */
    public int getWidth() {
        return width;
    }

    /**
     * Legt den Wert der width-Eigenschaft fest.
     */
    public void setWidth(int value) {
        this.width = value;
    }

    /**
     * Ruft den Wert der height-Eigenschaft ab.
     */
    public int getHeight() {
        return height;
    }

    /**
     * Legt den Wert der height-Eigenschaft fest.
     */
    public void setHeight(int value) {
        this.height = value;
    }

}
