package com.uptodata.isr.jaxb.all;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlType;
import javax.xml.bind.annotation.adapters.XmlJavaTypeAdapter;


/**
 * no, one or two intersections, x0 <= x1 is enshured
 * <p>
 * <p>Java-Klasse f(c)r xIntersectionType complex type.
 * <p>
 * <p>Das folgende Schemafragment gibt den erwarteten Content an, der in dieser Klasse enthalten ist.
 * <p>
 * <pre>
 * &lt;complexType name="xIntersectionType">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;attribute name="x0" type="{}optionalDoubleType" />
 *       &lt;attribute name="x1" type="{}optionalDoubleType" />
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "xIntersectionType")
public class XIntersectionType {

    @XmlAttribute(name = "x0")
    @XmlJavaTypeAdapter(Adapter1.class)
    protected Double x0;
    @XmlAttribute(name = "x1")
    @XmlJavaTypeAdapter(Adapter1.class)
    protected Double x1;

    /**
     * Ruft den Wert der x0-Eigenschaft ab.
     *
     * @return possible object is
     * {@link String }
     */
    public Double getX0() {
        return x0;
    }

    /**
     * Legt den Wert der x0-Eigenschaft fest.
     *
     * @param value allowed object is
     *              {@link String }
     */
    public void setX0(Double value) {
        this.x0 = value;
    }

    /**
     * Ruft den Wert der x1-Eigenschaft ab.
     *
     * @return possible object is
     * {@link String }
     */
    public Double getX1() {
        return x1;
    }

    /**
     * Legt den Wert der x1-Eigenschaft fest.
     *
     * @param value allowed object is
     *              {@link String }
     */
    public void setX1(Double value) {
        this.x1 = value;
    }

}
