package com.uptodata.isr.jaxb.all;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlType;


/**
 * this can only be calculated if n>=1
 * <p>
 * <p>Java-Klasse f(c)r basicType complex type.
 * <p>
 * <p>Das folgende Schemafragment gibt den erwarteten Content an, der in dieser Klasse enthalten ist.
 * <p>
 * <pre>
 * &lt;complexType name="basicType">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="regression" type="{}regressionType" minOccurs="0"/>
 *       &lt;/sequence>
 *       &lt;attribute name="xMin" use="required" type="{http://www.w3.org/2001/XMLSchema}double" />
 *       &lt;attribute name="xMean" use="required" type="{http://www.w3.org/2001/XMLSchema}double" />
 *       &lt;attribute name="xMax" use="required" type="{http://www.w3.org/2001/XMLSchema}double" />
 *       &lt;attribute name="yMin" use="required" type="{http://www.w3.org/2001/XMLSchema}double" />
 *       &lt;attribute name="yMean" use="required" type="{http://www.w3.org/2001/XMLSchema}double" />
 *       &lt;attribute name="yMax" use="required" type="{http://www.w3.org/2001/XMLSchema}double" />
 *       &lt;attribute name="xVariance" use="required" type="{}doublePosType" />
 *       &lt;attribute name="yVariance" use="required" type="{}doubleNonNegType" />
 *       &lt;attribute name="xyVariance" use="required" type="{http://www.w3.org/2001/XMLSchema}double" />
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "basicType", propOrder = {
        "regression"
})
public class BasicType {

    protected RegressionType regression;
    @XmlAttribute(name = "xMin", required = true)
    protected double xMin;
    @XmlAttribute(name = "xMean", required = true)
    protected double xMean;
    @XmlAttribute(name = "xMax", required = true)
    protected double xMax;
    @XmlAttribute(name = "yMin", required = true)
    protected double yMin;
    @XmlAttribute(name = "yMean", required = true)
    protected double yMean;
    @XmlAttribute(name = "yMax", required = true)
    protected double yMax;
    @XmlAttribute(name = "xVariance", required = true)
    protected double xVariance;
    @XmlAttribute(name = "yVariance", required = true)
    protected double yVariance;
    @XmlAttribute(name = "xyVariance", required = true)
    protected double xyVariance;

    /**
     * Ruft den Wert der regression-Eigenschaft ab.
     *
     * @return possible object is
     * {@link RegressionType }
     */
    public RegressionType getRegression() {
        return regression;
    }

    /**
     * Legt den Wert der regression-Eigenschaft fest.
     *
     * @param value allowed object is
     *              {@link RegressionType }
     */
    public void setRegression(RegressionType value) {
        this.regression = value;
    }

    /**
     * Ruft den Wert der xMin-Eigenschaft ab.
     */
    public double getXMin() {
        return xMin;
    }

    /**
     * Legt den Wert der xMin-Eigenschaft fest.
     */
    public void setXMin(double value) {
        this.xMin = value;
    }

    /**
     * Ruft den Wert der xMean-Eigenschaft ab.
     */
    public double getXMean() {
        return xMean;
    }

    /**
     * Legt den Wert der xMean-Eigenschaft fest.
     */
    public void setXMean(double value) {
        this.xMean = value;
    }

    /**
     * Ruft den Wert der xMax-Eigenschaft ab.
     */
    public double getXMax() {
        return xMax;
    }

    /**
     * Legt den Wert der xMax-Eigenschaft fest.
     */
    public void setXMax(double value) {
        this.xMax = value;
    }

    /**
     * Ruft den Wert der yMin-Eigenschaft ab.
     */
    public double getYMin() {
        return yMin;
    }

    /**
     * Legt den Wert der yMin-Eigenschaft fest.
     */
    public void setYMin(double value) {
        this.yMin = value;
    }

    /**
     * Ruft den Wert der yMean-Eigenschaft ab.
     */
    public double getYMean() {
        return yMean;
    }

    /**
     * Legt den Wert der yMean-Eigenschaft fest.
     */
    public void setYMean(double value) {
        this.yMean = value;
    }

    /**
     * Ruft den Wert der yMax-Eigenschaft ab.
     */
    public double getYMax() {
        return yMax;
    }

    /**
     * Legt den Wert der yMax-Eigenschaft fest.
     */
    public void setYMax(double value) {
        this.yMax = value;
    }

    /**
     * Ruft den Wert der xVariance-Eigenschaft ab.
     */
    public double getXVariance() {
        return xVariance;
    }

    /**
     * Legt den Wert der xVariance-Eigenschaft fest.
     */
    public void setXVariance(double value) {
        this.xVariance = value;
    }

    /**
     * Ruft den Wert der yVariance-Eigenschaft ab.
     */
    public double getYVariance() {
        return yVariance;
    }

    /**
     * Legt den Wert der yVariance-Eigenschaft fest.
     */
    public void setYVariance(double value) {
        this.yVariance = value;
    }

    /**
     * Ruft den Wert der xyVariance-Eigenschaft ab.
     */
    public double getXyVariance() {
        return xyVariance;
    }

    /**
     * Legt den Wert der xyVariance-Eigenschaft fest.
     */
    public void setXyVariance(double value) {
        this.xyVariance = value;
    }

}
