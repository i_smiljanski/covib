package com.uptodata.isr.jaxb.all;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlType;
import javax.xml.bind.annotation.adapters.XmlJavaTypeAdapter;


/**
 * Represents an area, maybe halfsided, confined by 2 horizontal lines
 * <p>
 * <p>Java-Klasse f(c)r limitVisualType complex type.
 * <p>
 * <p>Das folgende Schemafragment gibt den erwarteten Content an, der in dieser Klasse enthalten ist.
 * <p>
 * <pre>
 * &lt;complexType name="limitVisualType">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;attGroup ref="{}visualAttr"/>
 *       &lt;attribute name="lower" type="{}optionalDoubleType" />
 *       &lt;attribute name="upper" type="{}optionalDoubleType" />
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "limitVisualType")
public class LimitVisualType {

    @XmlAttribute(name = "lower")
    @XmlJavaTypeAdapter(Adapter1.class)
    protected Double lower;
    @XmlAttribute(name = "upper")
    @XmlJavaTypeAdapter(Adapter1.class)
    protected Double upper;
    @XmlAttribute(name = "title")
    protected String title;
    @XmlAttribute(name = "legendId")
    protected String legendId;
    @XmlAttribute(name = "class")
    protected String classes;
    @XmlAttribute(name = "style")
    protected String style;

    /**
     * Ruft den Wert der lower-Eigenschaft ab.
     *
     * @return possible object is
     * {@link String }
     */
    public Double getLower() {
        return lower;
    }

    /**
     * Legt den Wert der lower-Eigenschaft fest.
     *
     * @param value allowed object is
     *              {@link String }
     */
    public void setLower(Double value) {
        this.lower = value;
    }

    /**
     * Ruft den Wert der upper-Eigenschaft ab.
     *
     * @return possible object is
     * {@link String }
     */
    public Double getUpper() {
        return upper;
    }

    /**
     * Legt den Wert der upper-Eigenschaft fest.
     *
     * @param value allowed object is
     *              {@link String }
     */
    public void setUpper(Double value) {
        this.upper = value;
    }

    /**
     * Ruft den Wert der title-Eigenschaft ab.
     *
     * @return possible object is
     * {@link String }
     */
    public String getTitle() {
        return title;
    }

    /**
     * Legt den Wert der title-Eigenschaft fest.
     *
     * @param value allowed object is
     *              {@link String }
     */
    public void setTitle(String value) {
        this.title = value;
    }

    /**
     * Ruft den Wert der legendId-Eigenschaft ab.
     *
     * @return possible object is
     * {@link String }
     */
    public String getLegendId() {
        return legendId;
    }

    /**
     * Legt den Wert der legendId-Eigenschaft fest.
     *
     * @param value allowed object is
     *              {@link String }
     */
    public void setLegendId(String value) {
        this.legendId = value;
    }

    /**
     * Ruft den Wert der classes-Eigenschaft ab.
     *
     * @return possible object is
     * {@link String }
     */
    public String getClasses() {
        return classes;
    }

    /**
     * Legt den Wert der classes-Eigenschaft fest.
     *
     * @param value allowed object is
     *              {@link String }
     */
    public void setClasses(String value) {
        this.classes = value;
    }

    /**
     * Ruft den Wert der style-Eigenschaft ab.
     *
     * @return possible object is
     * {@link String }
     */
    public String getStyle() {
        return style;
    }

    /**
     * Legt den Wert der style-Eigenschaft fest.
     *
     * @param value allowed object is
     *              {@link String }
     */
    public void setStyle(String value) {
        this.style = value;
    }

}
