package com.uptodata.isr.jaxb.imageDesc;

import javax.xml.bind.annotation.adapters.XmlAdapter;

public class Adapter2
        extends XmlAdapter<String, Integer> {


    public Integer unmarshal(String value) {
        return ((int) javax.xml.bind.DatatypeConverter.parseDouble(value));
    }

    public String marshal(Integer value) {
        if (value == null) {
            return null;
        }
        return (javax.xml.bind.DatatypeConverter.printDouble((double) (int) value));
    }

}
