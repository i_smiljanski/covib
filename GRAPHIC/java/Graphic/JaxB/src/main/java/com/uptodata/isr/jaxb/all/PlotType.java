package com.uptodata.isr.jaxb.all;

import javax.xml.bind.annotation.*;


/**
 * <p>Java-Klasse f(c)r plotType complex type.
 * <p>
 * <p>Das folgende Schemafragment gibt den erwarteten Content an, der in dieser Klasse enthalten ist.
 * <p>
 * <pre>
 * &lt;complexType name="plotType">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="xAxis" type="{}axisType"/>
 *         &lt;element name="yAxis" type="{}axisType"/>
 *         &lt;element name="content" type="{}contentType"/>
 *       &lt;/sequence>
 *       &lt;attGroup ref="{}stylableAttr"/>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "plotType", propOrder = {
        "xAxis",
        "yAxis",
        "content"
})
public class PlotType {

    @XmlElement(required = true)
    protected AxisType xAxis;
    @XmlElement(required = true)
    protected AxisType yAxis;
    @XmlElement(required = true)
    protected ContentType content;
    @XmlAttribute(name = "class")
    protected String classes;
    @XmlAttribute(name = "style")
    protected String style;

    /**
     * Ruft den Wert der xAxis-Eigenschaft ab.
     *
     * @return possible object is
     * {@link AxisType }
     */
    public AxisType getXAxis() {
        return xAxis;
    }

    /**
     * Legt den Wert der xAxis-Eigenschaft fest.
     *
     * @param value allowed object is
     *              {@link AxisType }
     */
    public void setXAxis(AxisType value) {
        this.xAxis = value;
    }

    /**
     * Ruft den Wert der yAxis-Eigenschaft ab.
     *
     * @return possible object is
     * {@link AxisType }
     */
    public AxisType getYAxis() {
        return yAxis;
    }

    /**
     * Legt den Wert der yAxis-Eigenschaft fest.
     *
     * @param value allowed object is
     *              {@link AxisType }
     */
    public void setYAxis(AxisType value) {
        this.yAxis = value;
    }

    /**
     * Ruft den Wert der content-Eigenschaft ab.
     *
     * @return possible object is
     * {@link ContentType }
     */
    public ContentType getContent() {
        return content;
    }

    /**
     * Legt den Wert der content-Eigenschaft fest.
     *
     * @param value allowed object is
     *              {@link ContentType }
     */
    public void setContent(ContentType value) {
        this.content = value;
    }

    /**
     * Ruft den Wert der classes-Eigenschaft ab.
     *
     * @return possible object is
     * {@link String }
     */
    public String getClasses() {
        return classes;
    }

    /**
     * Legt den Wert der classes-Eigenschaft fest.
     *
     * @param value allowed object is
     *              {@link String }
     */
    public void setClasses(String value) {
        this.classes = value;
    }

    /**
     * Ruft den Wert der style-Eigenschaft ab.
     *
     * @return possible object is
     * {@link String }
     */
    public String getStyle() {
        return style;
    }

    /**
     * Legt den Wert der style-Eigenschaft fest.
     *
     * @param value allowed object is
     *              {@link String }
     */
    public void setStyle(String value) {
        this.style = value;
    }

}
