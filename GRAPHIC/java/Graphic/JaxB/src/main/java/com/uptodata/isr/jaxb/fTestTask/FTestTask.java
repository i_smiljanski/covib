package com.uptodata.isr.jaxb.fTestTask;

import javax.xml.bind.annotation.*;


/**
 * <p>Java-Klasse f(c)r anonymous complex type.
 * <p>
 * <p>Das folgende Schemafragment gibt den erwarteten Content an, der in dieser Klasse enthalten ist.
 * <p>
 * <pre>
 * &lt;complexType>
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;attribute name="version" use="required" type="{http://www.w3.org/2001/XMLSchema}string" fixed="1.0" />
 *       &lt;attribute name="confidence" use="required" type="{}probability" />
 *       &lt;attribute name="variance1" use="required" type="{}doubleNonNegType" />
 *       &lt;attribute name="n1" use="required">
 *         &lt;simpleType>
 *           &lt;restriction base="{http://www.w3.org/2001/XMLSchema}int">
 *             &lt;minInclusive value="2"/>
 *           &lt;/restriction>
 *         &lt;/simpleType>
 *       &lt;/attribute>
 *       &lt;attribute name="variance2" use="required" type="{}doubleNonNegType" />
 *       &lt;attribute name="n2" use="required">
 *         &lt;simpleType>
 *           &lt;restriction base="{http://www.w3.org/2001/XMLSchema}int">
 *             &lt;minInclusive value="2"/>
 *           &lt;/restriction>
 *         &lt;/simpleType>
 *       &lt;/attribute>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "")
@XmlRootElement(name = "fTestTask")
public class FTestTask {

    @XmlAttribute(name = "version", required = true)
    protected String version;
    @XmlAttribute(name = "confidence", required = true)
    protected double confidence;
    @XmlAttribute(name = "variance1", required = true)
    protected double variance1;
    @XmlAttribute(name = "n1", required = true)
    protected int n1;
    @XmlAttribute(name = "variance2", required = true)
    protected double variance2;
    @XmlAttribute(name = "n2", required = true)
    protected int n2;

    /**
     * Ruft den Wert der version-Eigenschaft ab.
     *
     * @return possible object is
     * {@link String }
     */
    public String getVersion() {
        if (version == null) {
            return "1.0";
        } else {
            return version;
        }
    }

    /**
     * Legt den Wert der version-Eigenschaft fest.
     *
     * @param value allowed object is
     *              {@link String }
     */
    public void setVersion(String value) {
        this.version = value;
    }

    /**
     * Ruft den Wert der confidence-Eigenschaft ab.
     */
    public double getConfidence() {
        return confidence;
    }

    /**
     * Legt den Wert der confidence-Eigenschaft fest.
     */
    public void setConfidence(double value) {
        this.confidence = value;
    }

    /**
     * Ruft den Wert der variance1-Eigenschaft ab.
     */
    public double getVariance1() {
        return variance1;
    }

    /**
     * Legt den Wert der variance1-Eigenschaft fest.
     */
    public void setVariance1(double value) {
        this.variance1 = value;
    }

    /**
     * Ruft den Wert der n1-Eigenschaft ab.
     */
    public int getN1() {
        return n1;
    }

    /**
     * Legt den Wert der n1-Eigenschaft fest.
     */
    public void setN1(int value) {
        this.n1 = value;
    }

    /**
     * Ruft den Wert der variance2-Eigenschaft ab.
     */
    public double getVariance2() {
        return variance2;
    }

    /**
     * Legt den Wert der variance2-Eigenschaft fest.
     */
    public void setVariance2(double value) {
        this.variance2 = value;
    }

    /**
     * Ruft den Wert der n2-Eigenschaft ab.
     */
    public int getN2() {
        return n2;
    }

    /**
     * Legt den Wert der n2-Eigenschaft fest.
     */
    public void setN2(int value) {
        this.n2 = value;
    }

}
