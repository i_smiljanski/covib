package com.uptodata.isr.jaxb.all;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java-Klasse f(c)r datasetVisualType complex type.
 * <p>
 * <p>Das folgende Schemafragment gibt den erwarteten Content an, der in dieser Klasse enthalten ist.
 * <p>
 * <pre>
 * &lt;complexType name="datasetVisualType">
 *   &lt;complexContent>
 *     &lt;extension base="{}datasetType">
 *       &lt;attGroup ref="{}visualAttr"/>
 *     &lt;/extension>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "datasetVisualType")
public class DatasetVisualType
        extends DatasetType {

    @XmlAttribute(name = "title")
    protected String title;
    @XmlAttribute(name = "legendId")
    protected String legendId;
    @XmlAttribute(name = "class")
    protected String classes;
    @XmlAttribute(name = "style")
    protected String style;

    /**
     * Ruft den Wert der title-Eigenschaft ab.
     *
     * @return possible object is
     * {@link String }
     */
    public String getTitle() {
        return title;
    }

    /**
     * Legt den Wert der title-Eigenschaft fest.
     *
     * @param value allowed object is
     *              {@link String }
     */
    public void setTitle(String value) {
        this.title = value;
    }

    /**
     * Ruft den Wert der legendId-Eigenschaft ab.
     *
     * @return possible object is
     * {@link String }
     */
    public String getLegendId() {
        return legendId;
    }

    /**
     * Legt den Wert der legendId-Eigenschaft fest.
     *
     * @param value allowed object is
     *              {@link String }
     */
    public void setLegendId(String value) {
        this.legendId = value;
    }

    /**
     * Ruft den Wert der classes-Eigenschaft ab.
     *
     * @return possible object is
     * {@link String }
     */
    public String getClasses() {
        return classes;
    }

    /**
     * Legt den Wert der classes-Eigenschaft fest.
     *
     * @param value allowed object is
     *              {@link String }
     */
    public void setClasses(String value) {
        this.classes = value;
    }

    /**
     * Ruft den Wert der style-Eigenschaft ab.
     *
     * @return possible object is
     * {@link String }
     */
    public String getStyle() {
        return style;
    }

    /**
     * Legt den Wert der style-Eigenschaft fest.
     *
     * @param value allowed object is
     *              {@link String }
     */
    public void setStyle(String value) {
        this.style = value;
    }

}
