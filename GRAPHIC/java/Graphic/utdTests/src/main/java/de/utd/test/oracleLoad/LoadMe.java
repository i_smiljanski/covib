package de.utd.test.oracleLoad;

import java.io.*;
import java.io.IOException;
import java.io.InputStream;

public class LoadMe {
	
	public static byte[] getDataAsByte() throws IOException{
		return "Ich bin geladen".getBytes("iso-8859-1");	
	}

	public static InputStream getDataAsStream() throws IOException{
		return new ByteArrayInputStream(getDataAsByte());	
	}
public static void main(String[] args) throws IOException {
	System.out.println(new String(getDataAsByte(),"iso-8859-1"));
}	
	
}
