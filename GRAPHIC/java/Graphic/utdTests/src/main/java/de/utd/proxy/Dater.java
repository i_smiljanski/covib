package de.utd.proxy;

import java.util.Date;

public interface Dater {

	Date getDate();
}
