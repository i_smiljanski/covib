package de.utd.chartShapes;

import java.awt.*;
import java.awt.geom.*;

import javax.swing.*;

/**
 *
 * @author PeterBuettner.de
 *
 */
public class ShowShapes extends JFrame{

	public static void main(String[] args) {
		new ShowShapes();
	}

ShowShapes(){
	setDefaultCloseOperation(EXIT_ON_CLOSE);
	
	Shape[] s = ChartShapes.getAllShapes();
	Icon [] icons = new Icon[s.length];
	for (int i = 0; i < s.length; i++) icons[i] = new ShIc(s[i]);
	
	JList list = new JList(icons);
	getContentPane().add(new JScrollPane(list));
	Dimension d = list.getPreferredSize();
	d.height +=40;
	d.width +=40;
	setSize(d);
	setLocationRelativeTo(null);
	show();
}

static class ShIc implements Icon{
	final static int S=60;
	final static int S2=S/2;
	final static AffineTransform scale = AffineTransform.getScaleInstance(S*0.5,S*0.5);
	
	final GeneralPath gp;
	public ShIc(Shape shape) {
		gp = new GeneralPath(shape);
		gp.transform(scale);
	}
	public int getIconHeight() {return S;}
	public int getIconWidth() {return 2*S;}

	public void paintIcon(Component c, Graphics g1, int x, int y) {
		Graphics2D g = (Graphics2D)g1;
		g.setRenderingHint(RenderingHints.KEY_ANTIALIASING, RenderingHints.VALUE_ANTIALIAS_ON);
		g.translate(S2 , S2);
		g.setStroke(new BasicStroke());
		
		g.setColor(Color.red);
		g.draw(new Line2D.Float(-S2,0,S*2,0));
		g.draw(new Line2D.Float(0,-S2,0,S2));
		g.draw(new Line2D.Float(S,-S2,S,S2));

		g.setColor(Color.blue);
		g.draw(gp);
		g.translate(S , 0);
		g.fill(gp);
		
		g.translate(-S , -S2);
	}
	
}

}
