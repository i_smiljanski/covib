package de.utd.ftest;

public class Ftest{
	
public static void main(String[] a) {
	System.out.println(				
			"----------------------------------------\n"+
			"            Execute F-Test\n"+
			"----------------------------------------\n"
		);
	if(a.length!=5){
		System.out.println(
				"\n"+
				"Need 5 arguments: alpha variance1 n1 variance2 n2\n"+
				"\n"+
				"alpha: e.g. 0.05\n"+
				"n1,n2: Count, not degrees of freedom\n"+
				"variance1/2 is sorted internaly\n"
				);
		System.exit(1);
		}
	
	FTable.fTest(
		Double.parseDouble(a[0]),
		Double.parseDouble(a[1]),
		Integer.parseInt(a[2]),
		Double.parseDouble(a[3]),
		Integer.parseInt(a[4])
		);
}
}