package de.utd.test;

import java.util.*;
import java.util.List;
import java.util.Properties;

public class Vers {
	public static void main(String[] args) {
		Properties p = System.getProperties();
		List list = new ArrayList(p.keySet());
		Collections.sort(list);
		for (Iterator i = list.iterator(); i.hasNext();) {
			String key = (String)i.next();
			System.out.println(key + "=" +p.getProperty(key));
			}
	}

}
