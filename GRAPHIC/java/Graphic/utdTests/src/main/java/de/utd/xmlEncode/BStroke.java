package de.utd.xmlEncode;

import java.awt.BasicStroke;
import java.beans.*;
import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;

/**
 *
 * @author PeterBuettner.de
 *
 */
public class BStroke {

	
class XMap{
	
Class type = Integer.TYPE; 

}	
	
/*	
stroke 	 
stroke-dasharray 	 
stroke-dashoffset 	 
stroke-linecap 	 
stroke-linejoin 	 
stroke-miterlimit 	 
stroke-opacity 	 
stroke-width	

*/	
public static void main(String[] args) {
	BasicStroke bs = new BasicStroke(0.5f,
			BasicStroke.CAP_BUTT,
			BasicStroke.JOIN_BEVEL,
			0.0f,
			new float[] {2.0f, 2.0f,1f},
			0.0f);
	printBasicStroke(bs);
	
	DefaultPersistenceDelegate dpg = new 
	DefaultPersistenceDelegate(new String[]{"width", "cap", "join", "miterlimit", "dash", "dash_phase"});	
	
	
	
	System.out.println("--------------------------------------");
	XMLEncoder x = new XMLEncoder(System.out);
	x.setPersistenceDelegate(BasicStroke.class, dpg);
	x.writeObject(bs);
//	x.writeObject(new Rectangle2D.Double(1,2,3,4));
	x.flush();
	System.out.println("--------------------------------------");
	
	ByteArrayOutputStream baos = new ByteArrayOutputStream(1000);
	x = new XMLEncoder(baos);
	x.setPersistenceDelegate(BasicStroke.class, dpg);
	x.writeObject(bs);
//	x.writeObject(new Rectangle2D.Double(1,2,3,4));
	x.flush();
	x.close();
	XMLDecoder d = new XMLDecoder(new ByteArrayInputStream(baos.toByteArray()));
	Object o = d.readObject();
	System.out.println(o.getClass());
	printBasicStroke((BasicStroke)o);
	
	
}	

static void printBasicStroke(BasicStroke bs){
	pt(bs.getDashPhase()+"");
	pt(bs.getEndCap()+"");
	pt(bs.getLineJoin()+"");
	pt(bs.getLineWidth()+"");
	pt(bs.getMiterLimit()+"");
	p(bs.getDashArray());
}
	
static void p(String s){System.out.println(s);}	
static void pt(String s){System.out.println("\t" + s);}	
static void p(float[] f){
	for (int i = 0; i < f.length; i++)
		System.out.print( ((i>0)?",":"") + f[i]);
	System.out.println();
	}	


}