package de.utd.proxy;

import java.util.Date;

class TestProxy {

public static void main(String[] args) {
	Hidden base = new Hidden();
	Dater d;
	MyProxy proxy = new MyProxy(base);
	d = (Dater)MyProxy.wrap(Dater.class, proxy);
	System.out.println(d.getDate());
}

public static class Hidden {
	
public 	Date getDate(){return new Date();}
}
}