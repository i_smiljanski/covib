package de.utd.date;

import java.awt.BorderLayout;
import java.awt.Font;
import java.text.SimpleDateFormat;
import java.util.*;

import javax.swing.*;
import javax.swing.event.ChangeEvent;
import javax.swing.event.ChangeListener;
import javax.swing.table.*;
import javax.swing.tree.*;

/**
 *
 * @author PeterBuettner.de
 *
 */
public class TimeZoneGui extends JFrame{
	public static final SimpleDateFormat sdf = new SimpleDateFormat("HH:mm"){
		{super.setTimeZone(TimeZone.getTimeZone("GMT"));}
		public void setTimeZone(TimeZone zone) {}
		};
	
	public static void main(String[] args) {new TimeZoneGui();}
	
	SpinnerModel sm = new SpinnerNumberModel(0,-24,24, 1); 
	JSpinner spOffset = new JSpinner(sm);
	
	OffsetTimezoneTableModel ottm;
	
	TimeZoneGui(){
		super("TimeTest  DST: is Summertime daylight saving");
		setDefaultCloseOperation(EXIT_ON_CLOSE);
		
		// -------------------------------------------------------------
		sm.addChangeListener(new ChangeListener(){
			public void stateChanged(ChangeEvent e) {onOffsetChange();}});
		
		JPanel offsetPanel = new JPanel();
		offsetPanel.setLayout(new BoxLayout(offsetPanel, BoxLayout.PAGE_AXIS));
		offsetPanel.add(new JLabel("Timezone by Offset:"));
		offsetPanel.add(spOffset);
		
		ottm= new OffsetTimezoneTableModel(0);
		offsetPanel.add(new JScrollPane(new MyTable(ottm)));

		// -------------------------------------------------------------
		JPanel allPanel = new JPanel();
		allPanel.setLayout(new BoxLayout(allPanel, BoxLayout.PAGE_AXIS));
		allPanel.add(new JLabel("All Timezones:"));
		allPanel.add(new JScrollPane(new MyTable(new TimezoneTableModel(TimeZone.getAvailableIDs()))));
		// -------------------------------------------------------------
		JPanel pnlTables = new JPanel(new BorderLayout(5,5));
		pnlTables.add(allPanel);
		pnlTables.add(offsetPanel, "East");
		// -------------------------------------------------------------
		
		
		// -------------------------------------------------------------
		JTabbedPane tp = new JTabbedPane();
		tp.add(pnlTables,"Lists");
		tp.add(new JScrollPane(new MyTree(makeTreeModelByDispName())),"Tree: DisplayName");
		tp.add(new JScrollPane(new MyTree(makeTreeModelByOffset())),"Tree: Offset/DisplayName");
		tp.add(new JScrollPane(new MyTree(makeTreeModelByPrefix())),"Tree: Prefix");
		
		
		getContentPane().add(tp);

		pack();
		setLocationRelativeTo(null);
		show();
		onOffsetChange();
	}

class MyTree extends JTree{
		public MyTree(TreeModel newModel) {
		super(newModel);
		setFont(Font.decode("MonoSpaced"));
	}
}	
	
//	 #########################################################################
// like a map, jold multi values in lists	
static class BagMap {
	Map map = new TreeMap();

	public void put(Object key, Object value){
		List list = (List)map.get(key);
		if(list==null) {
			list= new ArrayList();
			map.put(key, list);
			}
		list.add(value);
		}
	public List get(Object key){return (List)map.get(key);}
	public Iterator keys(){return map.keySet().iterator();}
	} 	
	
abstract class TimeZoneGroup{
	abstract Object getKey(TimeZone tz);
	BagMap group(TimeZone[] data){
		BagMap names = new BagMap();
		int len = data.length;
		for (int i=0; i<len; i++) {
			TimeZone timeZone = data[i];
			names.put(getKey(timeZone), timeZone);
			}
		return names;
		}
	}


private TreeModel makeTreeModelByPrefix() {
		BagMap groupMap = new TimeZoneGroup(){
			Object getKey(TimeZone tz) {
				String id = tz.getID();
				if(id.indexOf('/')>=0)return id.substring(0,id.indexOf('/'));
				return "_no Prefix";
				}}.group(getTimeZonesSortedById());
		
		DefaultMutableTreeNode root =  new DefaultMutableTreeNode("Root");		
		for(Iterator keys= groupMap.keys(); keys.hasNext();){
			String prefix =(String)keys.next();
			DefaultMutableTreeNode node = new DefaultMutableTreeNode( prefix);
			root.add(node);
			addAllNodes(node, makeNodesByDisplayName(groupMap.get(prefix)));
			}
		return new DefaultTreeModel(root);
	}


private TreeModel makeTreeModelByDispName() {
		DefaultMutableTreeNode root =  new DefaultMutableTreeNode("Root");
		addAllNodes(root, makeNodesByDisplayName(getTimeZones()));
		return new DefaultTreeModel(root);
	}

// input: Tz[], output:grouped by DisplayName
private DefaultMutableTreeNode[] makeNodesByDisplayName(TimeZone[] allTz) {
	BagMap names = new TimeZoneGroup(){
		Object getKey(TimeZone tz) {return tz.getDisplayName(Locale.US);}}.group(allTz);

	
	List nodes = new ArrayList();
	for(Iterator keys= names.keys(); keys.hasNext();){
		String name =(String)keys.next();
		DefaultMutableTreeNode node = new DefaultMutableTreeNode( name);
		List items = names.get(name);
		for(int i=0;i<items.size();i++)
			node.add(makeTZNode((TimeZone)items.get(i) ));
		nodes.add(node);
		}
		return (DefaultMutableTreeNode[])nodes.toArray(new DefaultMutableTreeNode[0]);
	}
private DefaultMutableTreeNode[] makeNodesByDisplayName(List allTz) {
	return makeNodesByDisplayName( (TimeZone[])allTz.toArray(new TimeZone[0]));
}
private void addAllNodes(DefaultMutableTreeNode parent, DefaultMutableTreeNode[] childs){
	for (int i = 0; i < childs.length; i++) parent.add(childs[i]);
	
}

private TreeModel makeTreeModelByOffset() {
	BagMap off = new TimeZoneGroup(){
		Object getKey(TimeZone tz) {return new Integer(tz.getRawOffset());}}.group(getTimeZones());
	
	// 
	DefaultMutableTreeNode root =  new DefaultMutableTreeNode("Root");
	for(Iterator t= off.keys(); t.hasNext();){
		Integer offset =(Integer)t.next(); 

		DefaultMutableTreeNode offsetNode = new DefaultMutableTreeNode( intOffsetToText(offset.intValue()));
		root.add(offsetNode);
		addAllNodes(offsetNode,makeNodesByDisplayName(off.get(offset)));
	}
	return new DefaultTreeModel(root);
}
// for nodes
static final String pad = "                                                                                                                        ";
String makeTZText(TimeZone timeZone){
	int dst = timeZone.getDSTSavings();
	int raw = timeZone.getRawOffset();
	
	String name = timeZone.getID();
	name = name + pad.substring(0,Math.max(5,45-name.length()));
	
	
	return name + "Raw:" + intOffsetToText(raw)+ (dst==0?"": "     DST:"+intOffsetToText(dst));
}


//for nodes
DefaultMutableTreeNode makeTZNode(TimeZone timeZone){
	return new DefaultMutableTreeNode(makeTZText(timeZone)); 
}
// #########################################################################


static String intOffsetToText(int offset){
	int intkey = offset;
	if(intkey<0) intkey = (24*3600*1000 - intkey); // for display 
	return (offset<0?"-":"") + sdf.format(new Date(intkey));
	}


void onOffsetChange(){
	ottm.setOffsetInHour(((Integer)spOffset.getValue()).intValue());
	}	



static public TimeZone[] getTimeZones() {
	return getTimeZones(TimeZone.getAvailableIDs());
}

static public TimeZone[] getTimeZonesSortedById() {
	return getTimeZonesSortedById(TimeZone.getAvailableIDs());
}

static public TimeZone[] getTimeZones(String[] ids){
	TimeZone[] zones = new TimeZone[ids.length];
	for (int i = 0; i < zones.length; i++)
		zones[i]= TimeZone.getTimeZone(ids[i]);
	return zones;
	}
static public TimeZone[] getTimeZonesSortedById(String[] ids){
	String[] idsort = (String[])ids.clone();
	Arrays.sort(idsort);
	return getTimeZones(idsort);
}

static class OffsetTimezoneTableModel extends TimezoneTableModel{
	private int offsetInHour;
	public OffsetTimezoneTableModel(int offsetInHour) {
		super(TimeZone.getAvailableIDs(offsetInHour*1000*3600));
	}
	public void setOffsetInHour(int offsetInHour) {
		this.offsetInHour = offsetInHour;
		setData(TimeZone.getAvailableIDs(offsetInHour*1000*3600));
		fireTableDataChanged();
	}
}
static class RightString{
	private String s;
	RightString(String s){this.s = s;}
	public String toString() {return s;}
	}
static class MyTable extends JTable{
	public MyTable(TableModel dm) {
		super(dm);
		DefaultTableCellRenderer r = new DefaultTableCellRenderer();
		r.setHorizontalAlignment(JLabel.RIGHT);
		setDefaultRenderer(RightString.class, r);
		}
}
static class TimezoneTableModel extends AbstractTableModel{
	private TimeZone[] zones;
	static final String[] colNames = "ID,DisplayName,DSTSaving,raw GMT Offset".split(",");
	static final Class[] classes = new Class[]{String.class, String.class, RightString.class, RightString.class};
	
	public TimezoneTableModel(String[] ids) {setData(ids);}
	protected void setData(String[] ids) {zones = getTimeZonesSortedById(ids);}
	
	public String getColumnName(int col) { return colNames[col];}
	public int getColumnCount() {return colNames.length;}
	public Class getColumnClass(int col) {return classes[col];}
	public int getRowCount() {return zones.length;}

	public Object getValueAt(int row, int col) {
		TimeZone tz = zones[row];
		if(col==0)return tz.getID();
		if(col==1)return tz.getDisplayName(Locale.US);
//		if(col==1)return tz.getDisplayName(tz.getDSTSavings()!=0,TimeZone.LONG,Locale.GERMAN);
		if(col==2) {
			int i = tz.getDSTSavings();
			return i==0?null: new RightString(intOffsetToText(i));
			}
		if(col==3) return new RightString(intOffsetToText(tz.getRawOffset()));
		
		return null;
	}
	}

}
