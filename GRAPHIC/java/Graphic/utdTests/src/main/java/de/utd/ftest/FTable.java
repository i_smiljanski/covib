package de.utd.ftest;

import java.text.DecimalFormat;

import cern.jet.stat.Gamma;

/**
 <pre>
aus openoffice, FVert
p:	0,9750									
										
	1	2	3	4	5	6	7	8	9	10
1	0,5040	0,5822	0,6142	0,6315	0,6423	0,6497	0,6551	0,6592	0,6624	0,6649
2	0,4275	0,5063	0,5423	0,5631	0,5766	0,5861	0,5932	0,5987	0,6031	0,6066
3	0,3962	0,4718	0,5081	0,5297	0,5443	0,5547	0,5626	0,5688	0,5738	0,5779
4	0,3793	0,4519	0,4876	0,5095	0,5244	0,5353	0,5436	0,5502	0,5555	0,5600
5	0,3688	0,4390	0,4740	0,4957	0,5107	0,5218	0,5304	0,5372	0,5428	0,5474
6	0,3616	0,4299	0,4642	0,4857	0,5007	0,5119	0,5205	0,5275	0,5332	0,5380
7	0,3563	0,4231	0,4568	0,4781	0,4930	0,5042	0,5129	0,5199	0,5258	0,5307
8	0,3524	0,4179	0,4510	0,4720	0,4868	0,4980	0,5067	0,5138	0,5197	0,5247
9	0,3492	0,4137	0,4464	0,4671	0,4818	0,4929	0,5017	0,5088	0,5147	0,5197
10	0,3467	0,4104	0,4426	0,4631	0,4777	0,4887	0,4975	0,5046	0,5105	0,5156


aus openoffice, FInv
p:	0,05
	1	2	3	4	5	6	7	8	9	10
1	161,45	199,50	215,71	224,58	230,16	233,99	236,77	238,88	240,54	241,88
2	18,51	19,00	19,16	19,25	19,30	19,33	19,35	19,37	19,38	19,40
3	10,13	9,55	9,28	9,12	9,01	8,94	8,89	8,85	8,81	8,79
4	7,71	6,94	6,59	6,39	6,26	6,16	6,09	6,04	6,00	5,96
5	6,61	5,79	5,41	5,19	5,05	4,95	4,88	4,82	4,77	4,74
6	5,99	5,14	4,76	4,53	4,39	4,28	4,21	4,15	4,10	4,06
7	5,59	4,74	4,35	4,12	3,97	3,87	3,79	3,73	3,68	3,64
8	5,32	4,46	4,07	3,84	3,69	3,58	3,50	3,44	3,39	3,35
9	5,12	4,26	3,86	3,63	3,48	3,37	3,29	3,23	3,18	3,14
10	4,96	4,10	3,71	3,48	3,33	3,22	3,14	3,07	3,02	2,98

</pre>

 */
public class FTable {
	static final DecimalFormat d = new DecimalFormat("0.0000");
	static {
		d.setMaximumFractionDigits(4);
		d.setMinimumFractionDigits(4);
		d.setGroupingUsed(false);
		}

	public static void main(String[] args) {
		fDist_Table(1.5, 10, 10);
		FInv_Table(0.05, 10, 10);
		
		Beta_Table(10, 10);
		Beta_Inc_Table(0.05, 10, 10);
	}	
	

static void fDist_Table(final double a, int rows, int cols) {
	System.out.println("fDist_Table: alpha=" + a);
new Tabler(){double calc(double d1, double d2) {return fDist(a, d1, d2);}}.run(rows, cols);
}
static void FInv_Table(final double a, int rows, int cols) {
	System.out.println("FInv_Table: alpha=" + a);
new Tabler(){double calc(double d1, double d2) {return fInv(a, d1, d2);}}.run(rows, cols);
}


/* ok as in openoffice */
static void Beta_Table(int rows, int cols) {
	System.out.println("Beta_Table");
new Tabler(){double calc(double d1, double d2) {return Gamma.beta(d1,d2);}}.run(rows, cols);
}

static void Beta_Inc_Table(final double a, int rows, int cols) {
	System.out.println("Beta_Inc_Table: alpha=" + a);
new Tabler(){
	double calc(double d1, double d2) {return Gamma.incompleteBeta(d1,d2, a);}
	}.run(rows, cols);
}

public static double fDist(double x, double d1, double d2){
	if(true){ // das passt zu oo FVert
		// http://www.itl.nist.gov/div898/handbook/eda/section3/eda3665.htm
		double xx = (d1) / (d1 + d2*x);
		return Gamma.incompleteBeta(d1 /2, d2 /2, xx);
		}
	else{ // this is irgendwie wrong
		// http://mathworld.wolfram.com/F-Distribution.html
		// http://en.wikipedia.org/wiki/F-distribution
		double xx = (d1 * x) / (d1 * x + d2);
		return regularizedBeta(d1 /2, d2 /2, xx);
		}
}

static double regularizedBeta(double d1, double d2, double x){
	double inc = Gamma.incompleteBeta(d1,d2,x);// http://mathworld.wolfram.com/IncompleteBetaFunction.html
	double c =  Gamma.beta(d1,d2);// http://mathworld.wolfram.com/BetaFunction.html
	// http://mathworld.wolfram.com/RegularizedBetaFunction.html
	return inc/c;
}


/**
 * Inverse of F-Distibution. Typically d1&gt;d2, note that d1,d2 are the degrees
 * of freedom not the count of values (d=countOfValue-1)
 * 
 * @param alpha
 *            1-confidence (0.05 is a typical value) in [0,1]
 * @param d1
 *            degrees of freedom 'in the nominator'
 * @param d2
 *            degrees of freedom 'in the denominator'
 * @return the inverse F-Distribution
 * @throws ArithmeticException
 *             e.g. when d1,d2&lt;1 alpha not in [0,1], or (hardcoded)
 *             iterations are not enough to find a result
 */
public static double fInv(final double alpha, double d1, double d2) throws ArithmeticException{
	//from
	// http://wwwhomes.uni-bielefeld.de/wdrexler/htmldata/statistik/Applet/Andress/glossar/surfstat/fvert.htm
	/**
	 * Max iterations for interval split/dupe: 2^100 ~ 10^30
	 * note: since any function may not e.g. go down to 0 we need this to 
	 * never have an infinite loop
	 */
	final double ITERMAX = 100;
	/**
	 * Epsilon for rel. accuracy of F-Values<br>
	 * cern.jet.math.Constants uses 1.11022302462515654042E-16
	 */
//	final double EPS = 0.0001; // 
	final double EPS = 1.11022302462515654042E-16; 
//	System.out.println(Long.toBinaryString(Double.doubleToLongBits(1.11022302462515654042E-16))); // see the magic?
	// TODO use params for itermax and eps?
	
	double low; 
	double mid;  
	double top;
	
	// don't check degFreeDenomi, degFreeNom, we don't care it is done elswhere  
	if (alpha < 0 || alpha > 1 ) 
		throw new ArithmeticException("fInv: Domain error, alpha needs to be in [0,1], is:'" + alpha + "'.");

	/*
	 * note: as a cumulative distribution function F is always in [0,1]
	 * F(0,x,x) ==1 (?)
	 */
	if (alpha <= 0.5) {
		/*
		 * find where F gets below alpha, double top until found (or to much iter) 
		 */
		low = 0.5;
		top = 0.5;
		for (int i = 0; i <= ITERMAX; i++) { 
			if(i==ITERMAX) throw new ArithmeticException("fInv: max iterations was to low.");
			top *= 2;
			if (fDist(top, d1, d2) <= alpha) break; 
			low = top;
			}
		}
	else {
		/*
		 * find where F gets above alpha, halve low until found (or to much iter) 
		 */
		top = 2;
		low = 2;
		for (int i = 0; i <= ITERMAX; i++) { 
			if(i==ITERMAX) throw new ArithmeticException("fInv: max iterations was to low.");
			low /= 2;
			if (fDist(low, d1, d2) >= alpha) break;
			top = low;
			}
		}

	mid = (top + low) / 2;
	for (int i = 0; i < ITERMAX && (top - low) > EPS * mid; i++) { 
		mid = (top + low) / 2;
		double p = fDist(mid, d1, d2);
		if (p < alpha)top = mid;
		else if (p > alpha)low = mid;
		else break;
		}
	// TODO throw exception if top-low is larger than eps?
//	return Math.log(top-low);// check how good it is
	return mid;
}


/**
 * print an n x m table 
 *
 */
abstract static class Tabler{
	abstract double calc(double d1, double d2);
	void run(int rows, int cols){
			for (int d1 = 1; d1 <= rows; d1++) {
				for (int d2 = 1; d2 <= cols; d2++) {
					double value = calc(d1,d2);
					System.out.print(d.format(value));
					System.out.print('\t');
				}
				System.out.println();
			}
				System.out.println("-----------------------");
		}
	}


/**
 * dump result to sysout <br>
 * n1,n2 >0; v1,v2 arbitrary
 * @param alpha
 * @param v1 v1 &lt;-&gt; v2 gets sorted in here
 * @param n1 Count, not degrees of freedom
 * @param v2 v1 &lt;-&gt; v2 gets sorted in here
 * @param n2 Count, not degrees of freedom
 */
public static void fTest(double alpha, double v1, int n1, double v2, int n2) {
	if(v1<v2){
		double v=v1;
		v1=v2;
		v2=v;
		int n=n1;
		n1=n2;
		n2=n;
		}
	
	double fInv = FTable.fInv(alpha,n1-1,n2-1);
	double alphaCalc = FTable.fDist(v1/v2,n1-1,n2-1); 
	
	// ... output ..........................................
	p("--- input (sorted v1>v2) ---------------");
	p("alpha: " + alpha);
	p();
	p("v1: " + v1);
	p("n1: " + n1);
	p();
	p("v2: " + v2);
	p("n2: " + n2);
	p();
	p("----------------------------------------");
	p("fInv: " + fInv);
	p("v1/v2: " + v1/v2);
	p("alphaCalc: " + alphaCalc);
	p();
	p("--- output -----------------------------");
	p("v1/v2 < fInv: " + (v1/v2 < fInv)  );
	p();
	p("--- output (alternate) -----------------");
	p("alpha < alphaCalc: " + (alpha < alphaCalc)  );
	p();
	p("----------------------------------------");
	
	p();p();p();
}


public static void p(){System.out.println();}
public static void p(String s){System.out.println(s);}
}
