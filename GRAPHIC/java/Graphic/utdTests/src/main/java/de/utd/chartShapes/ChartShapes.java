package de.utd.chartShapes;

import java.awt.Shape;
import java.awt.geom.*;
import java.awt.geom.GeneralPath;
import java.awt.geom.Rectangle2D;

/**
 *
 * @author PeterBuettner.de
 *
 */
public class ChartShapes {
	
	static final float S = 0.5f; 
	static final float D= S/4;//0.125f;	
	
	static Shape hLine = new Rectangle2D.Float(-S,-D,2*S,2*D);
	static Shape vLine = new Rectangle2D.Float(-D,-S,2*D,2*S);
	static GeneralPath plus;
	static GeneralPath cross;
	static Shape box = new Rectangle2D.Float(-S,-S,2*S,2*S);
	static GeneralPath diamond;
	static GeneralPath triUp;
	static GeneralPath triDn;
	static Shape dot = new Ellipse2D.Float(-S,-S,2*S,2*S);
	static GeneralPath star;
	
static{
	AffineTransform t45 = AffineTransform.getRotateInstance(Math.PI/4);
	AffineTransform t90 = AffineTransform.getRotateInstance(Math.PI/2);
	AffineTransform t180 = AffineTransform.getRotateInstance(Math.PI);
	AffineTransform tupDown = AffineTransform.getScaleInstance(1,-1);
	AffineTransform tscBox = AffineTransform.getScaleInstance(0.8,0.8);
	
	
//	plus =new GeneralPath(GeneralPath.WIND_NON_ZERO);
//	plus.append(hLine, false);
//	plus.append(vLine, false);
	
	plus =new GeneralPath(new Line2D.Float(-S,D,-S,-D));
	plus.append(new Line2D.Float(-D,-D,-D,-S), true);
	plus.append(plus.createTransformedShape(t90),true);
	plus.append(plus.createTransformedShape(t180),true);
	
	cross =new GeneralPath(plus);
	cross.transform(t45);
	
	box = tscBox.createTransformedShape(box);
	diamond =new GeneralPath(box);
	diamond.transform(t45);

/*	
	<!-- 1/sqrt(3) and sqrt(3)/2 - 1/sqrt(3) -->
	<symbol id="triup" viewBox="-50 -50 100 100">
	  <path d="M -50 36 L 0 -50 L 50 36 z"/> 
	</symbol>
*/	
	triUp=new GeneralPath(new Line2D.Float(-S,S*0.72f,0,-S));
	triUp.lineTo(S,S*0.72f);
	triUp.closePath();

//  
//  Gleichwinkliges Dreieck
//  
//               /|\                   |
//              / | \                  |
//             /  |  \                 |
//            /   |   \                |
//           /    |    \               |
//        c /     |     \              |
//         /      |      \             |
//        /       |       \            | h
//       /        CM       \           |
//      /       . |         \          |
//     /      .   |          \         |
//    /30(c) .      |m          \        |
//   /  .         |            \       |
//  /.  30(c)    90(c)|       60(c)   \      |
//  -----------------------------      -
//         b              b
//  
//  CM Center of Mass
//  2*b=c
//  m = c*tan(30)/2 = c*sqrt(3)/6 = c*0,289
//  h = c*sin(60)   = c*sqrt(3)/2 = c*0,866
	double c = 2*S;
	double m = c* Math.sqrt(3)/6;
	double h = c* Math.sqrt(3)/2;
	triUp=new GeneralPath(new Line2D.Double(-c/2,m,c/2,m));
	triUp.lineTo(0,(float)(m-h));
	triUp.closePath();
	
// billig:	
//	triUp=new GeneralPath(new Line2D.Float(-S,-S,S,-S));
//	triUp.lineTo(0,S);
//	triUp.closePath();
	
	triDn =new GeneralPath(triUp);
	triDn.transform(tupDown);
	
//	star = new GeneralPath(GeneralPath.WIND_NON_ZERO);
//	star.append(triUp,false);
//	star.append(triDn,false);
//	star.append(plus,false);
//	star.append(cross,false);
	star = makeStar();
}	


private static GeneralPath makeStar() {
	/*
	 * outer circle, inner circle, 'theta-pointer'
	 * 
	 * add alternating lines while theta rotates
	 * in 2Pi/10 steps
	 * 
	 */
	GeneralPath s = new GeneralPath(GeneralPath.WIND_NON_ZERO);
	final double ro = S*1.4; // radius outer
//	double ri = ra*0.37f; // pentagram
	final double ri = ro*0.3f;  // pretty
	final int STEPS = 10; // even!
	final double dt = 2*Math.PI/STEPS;
	double t =Math.PI;
	for(int i=0;i<STEPS;i++){
		double r = (i%2)==0? ro:ri; 
		float x = (float)(r * Math.sin(t));
		float y = (float)(r * Math.cos(t)); 
		if(i==0) s.moveTo(x,y);
		else s.lineTo(x,y);
		t +=dt;
		}
	s.closePath();
	return s;
}


	public static Shape getPlus() {return plus;}
	public static Shape getCross() {return cross;}
	public static Shape getBox() {return box;}
	public static Shape getDiamond() {return diamond;}
	public static Shape getTriUp() {return triUp;}
	public static Shape getTriDn() {return triDn;}
	public static Shape getDot() {return dot;}
	public static Shape getHline() {return hLine;}
	public static Shape getVline() {return vLine;}
	public static Shape getStar() {return star;}

	public static Shape[] getAllShapes() {
		return new Shape[]{
				plus, cross, box, diamond, triUp, triDn, star, dot, hLine, vLine, 		
		};
		}
}
