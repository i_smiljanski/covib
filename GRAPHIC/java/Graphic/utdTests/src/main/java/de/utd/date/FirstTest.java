package de.utd.date;

import java.awt.Font;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.*;

import javax.swing.*;

public class FirstTest {

	private static final String FMT = "H:mm z";
	
	private static JTextArea taOut = new JTextArea(30,60);
	
	public static void main(String[] args) {
		JFrame f = new JFrame("Some Date Tests");
		f.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		f.getContentPane().add(new JScrollPane(taOut));
		taOut.setFont(Font.decode("Monospaced"));
		f.pack();
		f.setLocationRelativeTo(null);
		f.show();
		
		DateFormat df = new SimpleDateFormat(FMT, Locale.US);
		Date d = new Date(0); // 1970-01-01 00:00:00 GMT
		pl("-----------------------------------------------");
		pl("Date is '0', means: 1970-01-01 00:00:00 GMT");
		pl("Since Date value will always be '1970-01-01'");
		pl("and seconds==0 we show only 'HH:mm' + TimeZone");
		pl("-----------------------------------------------");
		pl("Date.toString():");
		ptl(d);
		pl("SimpleDateFormat with default TZ:");
		ptl(df.format(d));
		
		pl("DateFormat.setTimeZone to GMT");
		df.setTimeZone(TimeZone.getTimeZone("GMT"));
		ptl(df.format(d));
		pl("");
		
		pl("Use new SimpleDateFormat with default TZ:");
		df = new SimpleDateFormat(FMT, Locale.US);
		ptl(df.format(d));
		pl("Set VM-Wide TimeZone.setDefault to GMT");
		TimeZone.setDefault(TimeZone.getTimeZone("GMT"));
		pl("and create new SimpleDateFormat with default TZ");
		df = new SimpleDateFormat(FMT, Locale.US);
		pl("Use new SimpleDateFormat with default TZ:");
		ptl(df.format(d));
		
		//--------------------------------------
		// "Europe/Berlin"	Central European Time	01:00	01:00
		
		pl("----------------------------------------------------");
		pl("Timezone:");
		pl("Europe/Berlin	Central European Time	01:00	01:00");
		pl("Daylight saving time and GMT offset on misc dates:");
		pl("");
		
		TimeZone.setDefault(null);
		TimeZone tz = TimeZone.getTimeZone("Europe/Berlin");
		df = new SimpleDateFormat("yyyy-MM-dd z", Locale.US);
		pl("Date      \tis DST\tGMT offset in h");
		pl("------------------------------------------");
		for(int i=0;i<12;i++){
			Date date = new Date(2004-1900,i,1);
			int offset = tz.getOffset(date.getTime());
			boolean dst = tz.inDaylightTime(date);
			pl(df.format(date) + "\t" + (dst?"[x]":"[ ]") + "\t" + (offset/3600000f));
			}
		pl("------------------------------------------");
		df = new SimpleDateFormat("yyyy-MM-dd HH:mm z", Locale.US);
		df.setTimeZone(tz);
		long start = new Date(2004-1900,10-1,31).getTime()+1000*60*(2*60+57);// 2:57
		for(int i=0;i<6;i++){
			Date date = new Date(start + i*1000*60);
			int offset = tz.getOffset(date.getTime());
			boolean dst = tz.inDaylightTime(date);
			pl(df.format(date) + "\t" + (dst?"[x]":"[ ]") + "\t" + (offset/3600000f));
			}
		
		
	}
static void pl(Object o){p(o.toString()+"\n");}
static void ptl(Object o){pl("\t"+o);}
static void p(Object o){taOut.append(o.toString());}
//static void pl(Object o){System.out.println(o);}
//static void ptl(Object o){System.out.println("\t"+o);}
//static void p(Object o){System.out.print(o);}
}
