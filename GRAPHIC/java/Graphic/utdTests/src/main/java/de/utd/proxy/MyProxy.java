package de.utd.proxy;

import java.beans.Expression;
import java.lang.reflect.*;


/**
 * <code>
 * MyProxy proxy = new MyProxy(base);
 * base = (Base)MyProxy.wrap(Base.class, proxy);
 * </code>
 */
class MyProxy implements InvocationHandler {
	/**
	 * create a new Proxy
	 * @param inter the interface whose calls shoud be wrapped 
	 * @param proxy the prebuild MyProxy
	 * @return the Proxy cast it with your interface
	 */
	public static Object wrap(Class inter, MyProxy proxy) {
		return Proxy.newProxyInstance(inter.getClassLoader(),new Class[]{inter},proxy);
		}

	private Object target;

	/**
	 * 
	 * @param target calls are delegated to this instance of 'your interface'
	 */
	public MyProxy(Object target) {this.target = target;}
	
	public Object invoke(Object proxy, Method m, Object[] args) throws Throwable {
		Object result;
		try {
			Expression e = new Expression(target,m.getName(), args);
			e.execute();
			result = e.getValue();
			}
		catch (InvocationTargetException e) {
			throw e.getTargetException();
			}
		catch (Exception e) {
			throw new RuntimeException("Unexpected invocation exception: " + e.getMessage());
			}
		return result;
		}
    

}