package com.uptodata.isr.graphic.db;

import com.uptodata.isr.db.connection.ConnectionUtil;
import com.uptodata.isr.db.trace.DbLogging;
import com.uptodata.isr.utils.ConvertUtils;
import com.uptodata.isr.utils.exceptions.MessageStackException;
import com.uptodata.isr.utils.ws.SOAPUtil;
import org.apache.commons.codec.binary.Base64;

import java.security.Permission;
import java.sql.Clob;
import java.sql.Connection;
import java.sql.DriverManager;


public class GraphicGeneration {
    static DbLogging log;
    private static final String serverContext = "/ws/graphic/isr/uptodata/com";
    final static String targetNamespace = "http://ws.graphic.isr.uptodata.com/";


    private static void initLogger() throws MessageStackException {
        log = new DbLogging();
    }


    public static Clob createGraphics(Clob clXML, String serverHost, String serverPort, int jobId, String delimiter,
                                      String configName, String logLevel, String logReference, int timeout)
            throws Exception {
        initLogger();
        log.stat("begin", serverHost + " " + serverPort);
        try {
            byte[] inputXml = ConvertUtils.getBytes(clXML);
            createGraphics(inputXml, serverHost, serverPort, jobId, delimiter, configName, logLevel, logReference, timeout);
            return null;
        } catch (MessageStackException e) {
            return onFailCreate(e);
        }
    }


    private static void createGraphics(byte[] xml, String serverHost, String serverPort, int jobId, String delimiter,
                                       String configName, String logLevel, String logReference, int timeout)
            throws MessageStackException {
        initLogger();
        String url = "http://" + serverHost + ":" + serverPort + serverContext;
        log.stat("begin", url);
        String[] args = new String[6];
        args[0] = Base64.encodeBase64String(xml);
        args[1] = String.valueOf(jobId);
        args[2] = delimiter;
        args[3] = configName;
        args[4] = logLevel;
        args[5] = logReference;

        String methodName = "createGraphics";
        log.debug("send request to  ", url + "; method: " + methodName);
        String sent = SOAPUtil.callMethod(url, targetNamespace, args, methodName, timeout);
        log.stat("end", sent);
    }


    private static void createCalculation(byte[] xml, String serverHost, String serverPort, int jobId, String delimiter,
                                          String configName, String logLevel, String logReference, int timeout) throws MessageStackException {
        initLogger();
        String url = "http://" + serverHost + ":" + serverPort + serverContext;
        log.stat("begin", url);
        String[] args = new String[6];
        args[0] = Base64.encodeBase64String(xml);
        args[1] = String.valueOf(jobId);
        args[2] = delimiter;
        args[3] = configName;
        args[4] = logLevel;
        args[5] = logReference;

        String methodName = "createCalculation";
        log.debug("send request to", url + "; method: " + methodName);

        String sent = SOAPUtil.callMethod(url, targetNamespace, args, methodName, timeout);
        log.stat("end", sent);
    }


    public static Clob createCalculation(Clob clXML, String serverHost, String serverPort, int jobId, String delimiter,
                                         String configName, String logLevel, String logReference, int timeout) throws Exception {
        initLogger();
        log.stat("begin", serverHost + " " + serverPort);
        try {
            byte[] inputXml = ConvertUtils.getBytes(clXML);
            createCalculation(inputXml, serverHost, serverPort, jobId, delimiter, configName, logLevel, logReference, timeout);
            return null;
        } catch (MessageStackException e) {
            return onFailCreate(e);
        }
    }


    private static Clob onFailCreate(MessageStackException e) throws Exception {
        Connection conn = ConnectionUtil.getDefaultConnection();
        if (conn == null) {
            throw new MessageStackException(e, MessageStackException.SEVERITY_CRITICAL, "database connection is impossible",
                    MessageStackException.getCurrentMethod());
        }
        byte[] exBytes = e.toXml().xmlToString().getBytes();
        Clob cl = ConvertUtils.getClob(conn, exBytes);
        log.stat("end", "end");
        return cl;
    }


    public static void main(String args[]) {
        try {
            initLogger();
            Connection conn = DriverManager.getConnection(
                    "jdbc:oracle:thin:@10.0.100.213:1521:ora11204", "isrowner_is", "isrowner_is");

//            byte[] xml = ConvertUtils.fileToBytes(System.getProperty("user.dir") + "\\tests\\ISR$WT$CRIB$CHART$PLOT.xml");
//            createGraphics(xml, "10.0.6.188", "2050", 111, "_", "DEBUG", "test graphic", 10);

//            byte[] xml = ConvertUtils.fileToBytes(System.getProperty("user.dir") + "\\tests\\result1.xml");
//            createCalculation(xml, "10.0.6.188", "2050", 682, "_", "GRAPHIC_CALCULATION", "DEBUG", "test calc", 10);
//            createCalculation(xml, "10.0.100.219", "5005", 684, "_", "GRAPHIC_CALCULATION", "DEBUG", "test calc", 60);

            byte[] xml = ConvertUtils.fileToBytes(System.getProperty("user.dir") + "\\tests\\chartcalc2.xml");
            createGraphics(xml, "10.0.6.188", "2050", 111, ";", "GRAPHIC_RESULT", "DEBUG", "test graphic", 60);

//            log.debug(res);

//            String query = "select  textfile  FROM  stb$jobtrail   WHERE entryid=11451";
//            String query = "select  logclob  FROM  isr$log   WHERE entryid=16137111";
//
//            PreparedStatement ps = defaultConnection.prepareStatement(query);
//            ResultSet rs = ps.executeQuery();
//            rs.next();
//            Clob cl = rs.getClob(1);
//
//            ps.close();
//            rs.close();

//            createCalculation(xml, "10.0.6.188", "2050", 684, "_", "GRAPHIC_CALCULATION", "DEBUG", "test calc", 60);

        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}


class ExitTrappedException extends SecurityException {

    static void forbidSystemExitCall() {
        final SecurityManager securityManager = new SecurityManager() {
            public void checkPermission(Permission permission) {
                if ("exitVM".equals(permission.getName())) {
                    throw new ExitTrappedException();
                }
            }
        };
        System.setSecurityManager(securityManager);
    }

    static void enableSystemExitCall() {
        System.setSecurityManager(null);
    }
}

