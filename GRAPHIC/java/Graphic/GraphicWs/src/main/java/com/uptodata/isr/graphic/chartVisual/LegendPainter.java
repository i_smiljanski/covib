package com.uptodata.isr.graphic.chartVisual;

import com.uptodata.isr.graphic.chartVisual.style.J2DStyle;
import org.jfree.chart.plot.XYPlot;

import java.awt.*;
import java.awt.geom.AffineTransform;
import java.awt.geom.GeneralPath;
import java.awt.geom.Line2D.Double;
import java.awt.geom.Rectangle2D;


/**
 * Display legend symbols for our graphic.
 */
abstract class LegendPainter {
    /*
	 * There are two different approaches: 
	 * 
	 * First idea: make shapes and paint them
	 * 
	 * Second Idea: use the drawables we have and paint downscaled
	 * images of the real shapes, so this is MUCH better and easier too
	 * --> JFDrawableLegendPainter 
	 */
// TODOlater cache shapes

    /*
     * Symbols for legend: make them (1 unit)(c) large, centered at (0,0)
     * add border (~0.25 on both sides) later
     * dataset legends are made different
     *
     *
     * dataset: symbol and/or line
     * confidence: area (easy: 2 parallel lines maybe a bit curved
     * line: line
     * limit: 2 h-lines with area in between
     *
     * NOTE: GeneralPath#moveTo() will open a 'new shape'-> filling is not as assumed naively
     *
     */
    protected J2DStyle style;

    private final static boolean DBG_PAINT = false; // paint background, border,...
    /**
     * we clip the unscaled <b>internal</b> shapes to this
     */
    private final static Rectangle2D CLIP = new Rectangle2D.Double(-0.5, -0.5, 1, 1);

    protected LegendPainter(J2DStyle style) {
        this.style = style;
    }

    /**
     * draw the legend symbol, currently it fills the size, line-width/symbols
     * are not scaled, so on a small target there maybe something missing
     *
     * @param g2
     * @param size
     */
    public void draw(Graphics2D g2, Dimension size) {
        AffineTransform at = create01Transform(size, 0);

        if (DBG_PAINT) {
            g2.setColor(Color.blue);
            g2.fill(new Rectangle(size));
        }
        // clip shape to rect: w/h=1/1 around 0,0
        Shape oldClip = g2.getClip();
        g2.setClip(at.createTransformedShape(CLIP));
        if (DBG_PAINT) {
            g2.setColor(Color.white);
            g2.fillRect(-1000, -1000, 2000, 2000);
        }
        // here is the work:
        drawInternal(g2, size, at);

        g2.setClip(oldClip);

    }

    /**
     * clip is set, just paint, you may use from01Shape to translate a shape, see param doc
     *
     * @param g2
     * @param size
     * @param from01Shape use to translate a (0,0) centered shape with size:1,1 to fill all but a defined border
     */
    protected abstract void drawInternal(Graphics2D g2, Dimension size, AffineTransform from01Shape);

    /**
     * creates transform to size/translate our (0,0) centered 1/1 sized symbols
     *
     * @param size   what we want to get, but we let a border
     * @param border <0.5! relative space on left/rigth/top/bottom
     * @return
     */
    protected AffineTransform create01Transform(Dimension size, double border) {
        double scale = 1. - 2 * border;
        AffineTransform at = new AffineTransform();
        at.scale(size.width * scale, size.height * scale);
        at.translate(0.5 + border / scale, 0.5 + border / scale);
        return at;
    }


    /**
     * have dataset legend painted
     */
    static class DatasetLegendPainter extends LegendPainter {

        DatasetLegendPainter(J2DStyle.Dataset style) {
            super(style);
        }

        protected void drawInternal(Graphics2D g2, Dimension size, AffineTransform at) {
            J2DStyle.Dataset dsStyle = (J2DStyle.Dataset) this.style;

            if (dsStyle.hasLine()) {// lines first in background
                dsStyle.getLine().paint(g2, at.createTransformedShape(new Double(-2, 0, 2, 0)));
            }

            if (dsStyle.hasSymbol()) {
                double dx = size.width;
                double dy = size.height;
                AffineTransform dsStyleTranslate = AffineTransform.getTranslateInstance(dx / 2, dy / 2);
                J2DStyle.Symbol symbol = dsStyle.getSymbol();
                Shape s = symbol.getShape();// this is centered at 0,0 but larger than otheres here
                s = dsStyleTranslate.createTransformedShape(s);
                symbol.paint(g2, s);
            }
        }
    }

    /**
     * experimental: show legends EXACTLY as components
     */
    static class JFDrawableLegendPainter extends LegendPainter {
        private JFDrawable drawable;
        private XYPlot plot;

        JFDrawableLegendPainter(XYPlot plot, JFDrawable drawable) {
            super(null);
            this.drawable = drawable;
            this.plot = plot;
        }

        protected void drawInternal(Graphics2D g2, Dimension size, AffineTransform at) {
            Rectangle2D area = at.createTransformedShape(CLIP).getBounds2D();
            drawable.draw(g2, plot, area);
        }
    }

    /**
     * base to have line/limit/confidence/any-other-shaped-legend painted with no effort,
     * dataset is a bit more complex
     */
    private static abstract class ShapeLegendPainter extends LegendPainter {

        ShapeLegendPainter(J2DStyle style) {
            super(style);
        }

        protected void drawInternal(Graphics2D g2, Dimension size, AffineTransform at) {
            style.paint(g2, at.createTransformedShape(getShape()));
        }

        /**
         * a shape that will be scaled, only parts in a rect w/h=1/1 centered at 0,0
         * are displayed (we use clip)
         *
         * @return
         */
        protected abstract Shape getShape();
    }

    static class LimitLegendPainter extends ShapeLegendPainter {
        public LimitLegendPainter(J2DStyle style) {
            super(style);
        }

        protected Shape getShape() {
            Rectangle2D r = new Rectangle2D.Double();
            r.setFrameFromCenter(0, 0, 5, 0.2f);// 5: so also very thick lines will be invisible in the vertical parts!
            return r;
        }

    }

    static class LineLegendPainter extends ShapeLegendPainter {
        public LineLegendPainter(J2DStyle style) {
            super(style);
        }

        protected Shape getShape() {
            return new Double(-0.5, 0.3, 0.5, -0.3);// y goes down on graphics
        }
    }

    static class ConfidenceLegendPainter extends ShapeLegendPainter {
        public ConfidenceLegendPainter(J2DStyle style) {
            super(style);
        }

        protected Shape getShape() {
            GeneralPath gp = new GeneralPath(GeneralPath.WIND_EVEN_ODD);
            gp.moveTo(-2, 0.7f);
            gp.lineTo(0, 0.4f);
            gp.curveTo(0.7f, 0.3f, 1, 0, 1, 0);
            gp.lineTo(3, -2);
            gp.lineTo(3, 0);
            gp.lineTo(1, 0.3f);
            gp.curveTo(0.3f, 0.4f, 0, 0.7f, 0, 0.7f);
            gp.lineTo(-2, 2.7f);
            gp.lineTo(-2, .7f);
            gp.closePath();
            gp.transform(AffineTransform.getTranslateInstance(-0.5, -0.5 + 0.15));
            return gp;


//		 Trumpet in svg, use a rect clip (0,0,1,1)
//		 <svg viewBox="0 0 1 1" >
//		 <g transform="scale(1.1,1) translate(-0.05 0.15)"  >
//		 <path d="m 0,0.4 c 0.7,-0.1 1,-0.4 1,-0.4   l 0,0.3  c -0.7, 0.1 -1,0.4 -1,0.4 z " />
//		 </g>

// another from svg:			
//			<g style="fill:#ddf;stroke-width:0.02;stroke:blue;" transform="translate(0 0.15)"  >
//			  <path d="
//			  m -2,0.7 
//			  L 0 0.4 
//			  c 0.7,-0.1 1,-0.4 1,-0.4   
//			  l 2,-2
//			  l 0,2
//			  l -2,0.3
//			  c -0.7, 0.1 -1,0.4 -1,0.4 
//			  l -2,2
//			  l 0,-2
//			  z " />
//			</g>

        }

    }
}
