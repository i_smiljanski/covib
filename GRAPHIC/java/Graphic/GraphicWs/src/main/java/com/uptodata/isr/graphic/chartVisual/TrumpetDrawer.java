package com.uptodata.isr.graphic.chartVisual;


import org.jfree.chart.axis.ValueAxis;
import org.jfree.chart.plot.XYPlot;
import org.jfree.data.Range;

import java.awt.*;
import java.awt.geom.GeneralPath;
import java.awt.geom.Rectangle2D;


/**
 * Draws the 'confidence intervall' around a linear fit, 'trumpetline'
 *
 * @author PeterBuettner.de
 */
class TrumpetDrawer extends AbstractDrawer {

    /**
     * statistical data
     */
    private final TrumpetDataProvider dataSrc;

    /**
     * Creates a new annotation to be displayed in the whole area with the given parameters
     *
     * @param tdp          not null
     * @param borderStroke null ok
     * @param borderPaint  null ok
     * @param fillPaint    null ok
     */
    public TrumpetDrawer(TrumpetDataProvider tdp, Stroke borderStroke, Paint borderPaint, Paint fillPaint) {
        super(borderStroke, borderPaint, fillPaint);
        if (tdp == null)
            throw new NullPointerException("DataProvider is null");
        this.dataSrc = tdp;
    }

    /**
     * Draws the annotation.
     *
     * @param g2       the graphics device.
     * @param plot     the plot.
     * @param dataArea the data area is in J2D coords
     */
    public void draw(Graphics2D g2, XYPlot plot, Rectangle2D dataArea) {
        ValueAxis domainAxis = plot.getDomainAxis();

        double x0 = domainAxis.getLowerBound();
        double x1 = domainAxis.getUpperBound();

        // ... manage half or both sided intervals ....................
        double y = 0;
        Boolean onlyUpper = dataSrc.getUpperOnly();
        Range r = Range.expand(plot.getRangeAxis().getRange(), 0.1, 0.1);// use neg values for debug
        if (Boolean.TRUE.equals(onlyUpper))
            y = r.getLowerBound();
        else if (Boolean.FALSE.equals(onlyUpper))
            y = r.getUpperBound();
        // ........................................................

//        Shape shape = createPath(75,x0+0.1*(x1-x0),x1-0.1*(x1-x0), onlyUpper, y);
        Shape shape = createPath(75, x0 - 0.1 * (x1 - x0), x1 + 0.1 * (x1 - x0), onlyUpper, y);
        shape = translate(plot, dataArea, shape);

        paintShape(g2, shape);
    }//------------------------------------------------------------

    /**
     * creates the path in 'data values'
     *
     * @param cnt       number of x values
     * @param xStart    first x value
     * @param xEnd      last x value
     * @param onlyUpper if not null then the curve is half sided, the value tells which
     *                  side
     * @param yMargin   if onlyUpper!=null the this is a 'secure' y value that is
     *                  outside of the view; is ignored otherwise
     * @return
     */
    private GeneralPath createPath(final int cnt, final double xStart, final double xEnd,
                                   Boolean onlyUpper, double yMargin) {
        // one less so we nearly match start and end
        final double dx = (xEnd - xStart) / (cnt - 1);
        double x;

        // build the path
        GeneralPath gp = new GeneralPath(GeneralPath.WIND_NON_ZERO, 2 * cnt + 4);
        // first is 'in between'
        gp.moveTo((float) xStart, (float) dataSrc.getLineY(xStart));

        // ... upper side ............................
        x = xStart;
        if (onlyUpper == null || onlyUpper)
            for (int i = 0; i < cnt; i++, x += dx) gp.lineTo((float) x, (float) dataSrc.getTrumpetY(x, true));
        else
            appendLineHLine(gp, xStart, xEnd, yMargin);


        // ... lower side ...........................
        x = xEnd;
        if (onlyUpper == null || !onlyUpper)
            for (int i = cnt - 1; i >= 0; i--, x -= dx) gp.lineTo((float) x, (float) dataSrc.getTrumpetY(x, false));
        else
            appendLineHLine(gp, xEnd, xStart, yMargin);

        gp.closePath();
        return gp;
    }

    private final static void appendLineHLine(GeneralPath gp, double xStart, double xEnd, double y) {
        gp.lineTo((float) xStart, (float) y);
        gp.lineTo((float) xEnd, (float) y);
    }

}
