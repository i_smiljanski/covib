package com.uptodata.isr.graphic.imageFile;


/**
 * When we read in data in a String representation, maybe a Dom tree, we fire
 * this, e.g.&nbsp; NumberFormatException, Wrong NodeName,... missing attribut
 *
 * @author PeterBuettner.de
 */
public class DataFormatException extends Exception {

    public DataFormatException() {
        super();
    }

    public DataFormatException(String message) {
        super(message);
    }

    public DataFormatException(String message, Throwable cause) {
        super(message, cause);
    }

    public DataFormatException(Throwable cause) {
        super(cause);
    }
}
