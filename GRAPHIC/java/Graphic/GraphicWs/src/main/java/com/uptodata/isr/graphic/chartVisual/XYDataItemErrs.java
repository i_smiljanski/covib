package com.uptodata.isr.graphic.chartVisual;

import org.jfree.data.xy.XYDataItem;


/**
 * enhance XYDataItem for err Values (e.g. for errBars)
 *
 * @author PeterBuettner.de
 * @version 2008-10-15
 */
class XYDataItemErrs extends XYDataItem {
    private final double xErrHigh;
    private final double xErrLow;
    private final double yErrHigh;
    private final double yErrLow;
    private final boolean hasErrX;
    private final boolean hasErrY;
    private final boolean hasErr;

    /**
     * any of the errs maybe Double.NaN
     *
     * @param x
     * @param y
     * @param xErrLow  abs value, signed, not:difference
     * @param xErrHigh abs value, not:difference
     * @param yErrLow  abs value, signed, not:difference
     * @param yErrHigh abs value, not:difference
     */
    public XYDataItemErrs(double x, double y, double xErrLow, double xErrHigh, double yErrLow, double yErrHigh) {
        super(x, y);
        this.xErrHigh = xErrHigh;
        this.xErrLow = xErrLow;
        this.yErrHigh = yErrHigh;
        this.yErrLow = yErrLow;


        hasErrX = Double.isNaN(this.xErrHigh) || Double.isNaN(this.xErrLow);
        hasErrY = Double.isNaN(this.yErrHigh) || Double.isNaN(this.yErrLow);
        hasErr = hasErrX || hasErrY;
    }

    public double getXErrLow() {
        return xErrLow;
    }

    public double getXErrHigh() {
        return xErrHigh;
    }

    public double getYErrLow() {
        return yErrLow;
    }

    public double getYErrHigh() {
        return yErrHigh;
    }

    public boolean isHasErr() {
        return hasErr;
    }

    public boolean isHasErrX() {
        return hasErrX;
    }

    public boolean isHasErrY() {
        return hasErrY;
    }
}
