package com.uptodata.isr.graphic.chartVisual;


import org.jfree.chart.axis.LogarithmicAxis;
import org.jfree.chart.axis.ValueAxis;
import org.jfree.chart.plot.XYPlot;
import org.jfree.data.Range;
import org.jfree.ui.RectangleEdge;

import java.awt.*;
import java.awt.geom.Rectangle2D;

/**
 * Draws limits (horizontal lines) maybe one or two, area maybe filled
 *
 * @author PeterBuettner.de
 */
class LimitDrawer extends AbstractDrawer {

    private Double upper;
    private Double lower;

    /**
     * Creates a new annotation to be displayed in the whole area with the given parameters
     *
     * @param lower        maybe null
     * @param upper        maybe null
     * @param borderStroke null ok
     * @param borderPaint  null ok
     * @param fillPaint    null ok
     */
    public LimitDrawer(Double lower, Double upper, Stroke borderStroke, Paint borderPaint, Paint fillPaint) {
        super(borderStroke, borderPaint, fillPaint);
        this.lower = lower;
        this.upper = upper;
    }

    /**
     * Draws
     *
     * @param g2       the graphics device.
     * @param plot     the plot.
     * @param dataArea the data area is in J2D coords
     */
    public void draw(Graphics2D g2, XYPlot plot, Rectangle2D dataArea) {
        if (lower == null && upper == null) return; // nothing to do

        ValueAxis domainAxis = plot.getDomainAxis();

        double x0 = domainAxis.getLowerBound();
        double x1 = domainAxis.getUpperBound();

        // if single sided: hide 'other line' by moving it out of visual area 
        Range r = Range.expand(plot.getRangeAxis().getRange(), 0.1, 0.1);// use neg values for debug
        double y0 = lower != null ? lower.doubleValue() : r.getLowerBound();
        double y1 = upper != null ? upper.doubleValue() : r.getUpperBound();

        // 10-2008 PEB
        if (domainAxis instanceof LogarithmicAxis || plot.getRangeAxis() instanceof LogarithmicAxis) {
            System.err.println("Class: '" + getClass() + "' can't work with LogarithmicAxis, look into code, new version is ther, needs testing for 'not breaking old behaviour'.");
            return;
        }

        if (true) {// NEW behaviour post 10-2008
            // 10-2008 PEB: with log10 Axis 'translate(plot, dataArea, shape)' goes wrong!
            double dxExtra = 0.1 * (x1 - x0);
            Rectangle2D shape = getTranslatedRect(plot, dataArea, x0 - dxExtra, y0, x1 + dxExtra, y1);

            Object hint = g2.getRenderingHint(RenderingHints.KEY_STROKE_CONTROL);
            if (RenderingHints.VALUE_STROKE_NORMALIZE.equals(hint)) {
                paintShape(g2, shape);
            } else {// horizontal lines look much better this way
                g2.setRenderingHint(RenderingHints.KEY_STROKE_CONTROL, RenderingHints.VALUE_STROKE_NORMALIZE);
                paintShape(g2, shape);
                g2.setRenderingHint(RenderingHints.KEY_STROKE_CONTROL, hint);
            }
        } else { // OLD behaviour pre 10-2008
            Rectangle2D shape = new Rectangle2D.Double(x0 - 0.1 * (x1 - x0), y0, (x1 - x0) * 1.2, y1 - y0);
            Object hint = g2.getRenderingHint(RenderingHints.KEY_STROKE_CONTROL);
            if (RenderingHints.VALUE_STROKE_NORMALIZE.equals(hint)) {
                paintShape(g2, translate(plot, dataArea, shape));
            } else {// horizontal lines look much better this way
                g2.setRenderingHint(RenderingHints.KEY_STROKE_CONTROL, RenderingHints.VALUE_STROKE_NORMALIZE);
                paintShape(g2, translate(plot, dataArea, shape));
                g2.setRenderingHint(RenderingHints.KEY_STROKE_CONTROL, hint);
            }
        }
    }

    /**
     * calculate the rect correctly
     * <p>
     * reason:<br>
     * 10-2008 PEB: with log10 Axis 'translate(plot, dataArea, shape)' goes wrong
     * <br>
     * this is unused to not change old behaviour, but is tested with log10 axis
     *
     * @param plot
     * @param dataArea
     * @param x0
     * @param y0
     * @param x1
     * @param y1
     * @return
     */
    private Rectangle2D getTranslatedRect(XYPlot plot, Rectangle2D dataArea, double x0, double y0, double x1, double y1) {
        ValueAxis domainAxis = plot.getDomainAxis();
        ValueAxis rangeAxis = plot.getRangeAxis();

        RectangleEdge xAxisLocation = plot.getDomainAxisEdge();
        RectangleEdge yAxisLocation = plot.getRangeAxisEdge();

        double j2d_x0 = domainAxis.valueToJava2D(x0, dataArea, xAxisLocation);
        double j2d_x1 = domainAxis.valueToJava2D(x1, dataArea, xAxisLocation);
        double j2d_y0 = rangeAxis.valueToJava2D(y0, dataArea, yAxisLocation);
        double j2d_y1 = rangeAxis.valueToJava2D(y1, dataArea, yAxisLocation);

        double height = j2d_y1 - j2d_y0;
        double paintY0 = j2d_y0;
        if (height < 0) {
            /*
			 * bufferedImage.graphics doesn't like that?! (with jre1.3)
			 * since code above we use 'lower' for y0 that will become larger than y1, j2d is 'top->bottom' scaled
			 */
            height = -height;
            paintY0 = j2d_y1;
        }
        Rectangle2D r = new Rectangle2D.Double(j2d_x0, paintY0, j2d_x1 - j2d_x0, height);
        return r;
    }

    private static double[] translateDomainAxis(XYPlot plot, Rectangle2D dataArea, double[] in) {
        return valueToJava2D(plot.getDomainAxis(), dataArea, plot.getDomainAxisEdge(), in);
    }

    private static double[] translateRangeAxis(XYPlot plot, Rectangle2D dataArea, double[] in) {
        return valueToJava2D(plot.getRangeAxis(), dataArea, plot.getRangeAxisEdge(), in);
    }

    private static double[] valueToJava2D(ValueAxis axis, Rectangle2D dataArea, RectangleEdge edge, double[] in) {
        double[] out = new double[in.length];
        for (int i = 0; i < out.length; i++) {
            out[i] = axis.valueToJava2D(in[i], dataArea, edge);
        }
        return out;
    }

}
