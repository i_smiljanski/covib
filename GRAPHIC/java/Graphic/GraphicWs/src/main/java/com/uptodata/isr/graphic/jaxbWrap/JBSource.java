package com.uptodata.isr.graphic.jaxbWrap;


import javax.xml.bind.*;

/**
 * Get your Marshallers here, or build your own JAXBContext with the context
 * path, JAXBContext is thread safe (un)marshallers not. For special purposees
 * contexts maybe created on demand
 *
 * @author PeterBuettner.de
 */
public class JBSource {
    private static JAXBContext jc;

    /**
     * for com.utd.jaxb.isr.all
     */
    public static final String CONTEXT_PATHS = "com.uptodata.isr.jaxb.all";


    /**
     * a new JaxB context with the default paths
     *
     * @return the new JAXBContext
     * @throws javax.xml.bind.JAXBException
     */
    public static JAXBContext newInstance() throws JAXBException {
        return JAXBContext.newInstance(CONTEXT_PATHS);
    }

    /**
     * a new JaxB context with the default paths and your class loader
     *
     * @param classLoader
     * @return the new JAXBContext
     * @throws javax.xml.bind.JAXBException
     */
    public static JAXBContext newInstance(ClassLoader classLoader) throws JAXBException {
        return JAXBContext.newInstance(CONTEXT_PATHS, classLoader);
    }

    /**
     * a formatted marshaller from the shared context
     *
     * @return a new marshaller
     * @throws javax.xml.bind.JAXBException
     * @throws javax.xml.bind.PropertyException
     */
    public static Marshaller getMarshaller() throws JAXBException, PropertyException {
        initSharedContext();
        Marshaller m = jc.createMarshaller();
        m.setProperty(Marshaller.JAXB_FORMATTED_OUTPUT, Boolean.TRUE);
        return m;
    }

    /**
     * a validating unmarshaller from the shared context
     *
     * @return a new validating unmarshaller
     * @throws javax.xml.bind.JAXBException
     */
    public static Unmarshaller getValidatingUnmarshaller() throws JAXBException {
        initSharedContext();
        Unmarshaller u = jc.createUnmarshaller();

//	u.setValidating( true );
        return u;
    }

    private static synchronized void initSharedContext() throws JAXBException {
        if (jc == null)
            jc = JAXBContext.newInstance(CONTEXT_PATHS);
    }
}
