package com.uptodata.isr.graphic.imageFile;

import org.w3c.dom.Node;


/**
 * Tiny Helper to read in data from a DOM
 *
 * @author PeterBuettner.de
 */
class DomDataReader {

    /**
     * Reads attribute, tries to convert to Integer, if no attib, this is empty then
     * return null, also on parse error (then a message is written to syserr)
     *
     * @param node
     * @param attrib
     * @param errAddOn extra message on syserr
     * @return
     * @throws DataFormatException
     */
    static Integer getAttributeInteger(Node node, String attrib, String errAddOn) throws DataFormatException {
        String val = getAttribute(node, attrib);
        if (val != null) val = val.trim();
        if (val == null || val.length() == 0) return null;

        try {
            return Integer.valueOf(val);
        } catch (NumberFormatException e) {
            throw new DataFormatException("Attribute " + val + " has no integer format. " + errAddOn, e);
        }
    }//----------------------------------------------------------------------------

    /**
     * Read attribute, if its there (in format [true|false]) returns proper Boolean
     * if absent returns null
     * <p>
     * attrib is tested if its value.equals(name)
     *
     * @param node
     * @param attrib
     * @param errAddOn extra message on syserr
     * @return
     * @throws DataFormatException if its value not in "true", "false"
     */
    static Boolean getAttributeBoolean(Node node, String attrib, String errAddOn) throws DataFormatException {
        String val = getAttribute(node, attrib);
        if (val == null) return null;

        if (val.equals("true")) return Boolean.TRUE;
        if (val.equals("false")) return Boolean.FALSE;

        throw new DataFormatException("Attribute " + val + " has a value != true/false" + errAddOn);
    }//----------------------------------------------------------------------------


    /**
     * Gets the value of the node (maybe empty),
     * returns null if the attrib isn't there
     *
     * @param node
     * @param attrib
     * @return vale of the attrib
     */
    static String getAttribute(Node node, String attrib) {
        Node attribNode = node.getAttributes().getNamedItem(attrib);
        if (attribNode == null) return null;
        return attribNode.getNodeValue();
    }//-----------------------------------------------------------------------
}
