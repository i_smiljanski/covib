package com.uptodata.isr.graphic.chartVisual;

/**
 * Serves data to paint a confidence area/curve.
 * <p>
 * offset around line is:<br>
 * +-invStudT * sdXY*Math.sqrt(1.0/n + dx*dx/n/vX);
 *
 * @author PeterBuettner.de
 */
class TrumpetDataProvider {

    private final double slope; // steigung
    private final double intercept; // achsenabschnitt
    private final int n;   // number of data pairs
    private final double x_;  // x mean
    private final double vX;     // variance of x
    private final double sdXY; // standartdeviation of x*y

    /**
     * inverse student t value: probability associated with the two-tailed Student's t-distribution
     * with its probability (=confidence, exampl: 95%) degrees_freedom(=n-2)
     */
    private final double invStudT;

    private final Boolean upperOnly;

    /**
     * @param slope     (de:steigung)
     * @param intercept (de:achsenabschnitt)
     * @param n         Number of data points (needs >0)
     * @param x_        x Mean
     * @param vX        variance of x: sumXX/n - sumX*sumX/n/n (needs !=0 )
     * @param sdXY      Math.sqrt(n / (n - 2.0) *(vY - vXY * vXY / vX ) ) (needs !=
     *                  NaN)
     * @param invStudT  inverse-Student-T value
     * @param upperOnly if it is single or both sided: <br>
     *                  null = both <br>
     *                  Boolean.TRUE = upper only <br>
     *                  Boolean.FALSE = lower only <br>
     * @throws IllegalArgumentException if any of the input values is wrong
     */


    public TrumpetDataProvider(double slope, double intercept, int n, double x_, double vX, double sdXY, double invStudT, Boolean upperOnly) throws IllegalArgumentException {
        if (n < 1) throw new IllegalArgumentException("n<1");
        if (vX == 0) throw new IllegalArgumentException("vX==0");
        if (Double.isNaN(sdXY)) throw new IllegalArgumentException("sdXY==NaN");

        this.slope = slope;
        this.intercept = intercept;
        this.n = n;
        this.x_ = x_;
        this.vX = vX;
        this.sdXY = sdXY;
        this.invStudT = invStudT;
        this.upperOnly = upperOnly;
    }

    /**
     * Calculates the edge value of the confidence intervall
     * <br>
     * So: f_err(x) = getTrumpetY(x, true/false) <br>
     *
     * @param x
     * @param upper upper or lower part
     * @return
     */
    public double getTrumpetY(double x, boolean upper) {
        double dx = x - x_;
        if (upper)
            return slope * x + intercept + invStudT * sdXY * Math.sqrt(1.0 / n + dx * dx / n / vX);
        else
            return slope * x + intercept - invStudT * sdXY * Math.sqrt(1.0 / n + dx * dx / n / vX);
    }

    /**
     * Calculates <code>y = m*x + b </code> out of the estimated slope and
     * intercept.
     *
     * @param x
     * @return y value
     */
    public double getLineY(double x) {
        return slope * x + intercept;
    }


    /**
     * if it is single or both sided: <br>
     * null = both <br>
     * Boolean.TRUE = upper only <br>
     * Boolean.FALSE = lower only <br>
     *
     * @return
     */
    public Boolean getUpperOnly() {
        return upperOnly;
    }

}
