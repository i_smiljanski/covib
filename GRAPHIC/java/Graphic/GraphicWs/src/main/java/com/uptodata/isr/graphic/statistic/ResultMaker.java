package com.uptodata.isr.graphic.statistic;


import com.uptodata.isr.graphic.jaxbWrap.DatasetWrap;
import com.uptodata.isr.jaxb.all.*;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import javax.xml.bind.JAXBException;


//import com.utd.isr.graphic.statistic.LRCalculator;

/**
 * Turns a LinearRegressionTask to a Result. No calculation is in here (that is
 * done in {{@link com.uptodata.isr.graphic.statistic.LRCalculator}, here we just stick
 * the values together by the help of jaxB.
 *
 * @author PeterBuettner.de <br>
 *         TODO look for vY==0 and r; linear intersections when slope==0?
 */
class ResultMaker {
    private static Logger log = LogManager.getLogger(ResultMaker.class);

    public static Result createResult(LinearRegressionTask lrt, Result r) throws JAXBException {
        return createResult(r, lrt.getConfidence(), lrt.getLowerLimit(), lrt.getUpperLimit());
    }

    public static Result createResult(LinearRegressionTask lrt) throws JAXBException {
        DatasetWrap data = new DatasetWrap(lrt.getDataset());
        return createResult(data.getX(), data.getY());
    }

    private static Result createResult(Result r, double confidence, Double lowerLimit, Double upperLimit) throws JAXBException {
        if (r.getN() == 0) {
            return r;
        }
        ObjectFactory of = new ObjectFactory();
        LRCalculator lr = new LRCalculator(r);
        if (r.getBasic().getRegression() != null)
            r.getBasic().getRegression().setIntersections(createIntersections(of, lr, confidence, lowerLimit, upperLimit));
        return r;
    }

    private static Result createResult(double[] x, double[] y) throws JAXBException {
        ObjectFactory of = new ObjectFactory();

        Result r = of.createResult();
        r.setVersion("1.0");
        if (x.length == 0) {
            r.setN(0);
            return r; // exit here if no values
        }

        LRCalculator lr = new LRCalculator(x, y);

        r.setN(lr.n);

        // ... basic is possible ..............
        BasicType basic = of.createBasicType();
        r.setBasic(basic);
        basic.setXMin(lr.xMin);
        basic.setXMean(lr.xMean);
        basic.setXMax(lr.xMax);
        basic.setYMin(lr.yMin);
        basic.setYMean(lr.yMean);
        basic.setYMax(lr.yMax);

        basic.setXVariance(lr.vX);
        basic.setYVariance(lr.vY);
        basic.setXyVariance(lr.vXY);

        if (lr.reg == null)
            return r;

        // ... regression is possible ..............
        RegressionType reg = of.createRegressionType();

        basic.setRegression(reg);

        double slope = lr.reg.slope;
        reg.setSlope(slope);
        reg.setIntercept(lr.reg.intercept);

        reg.setYMinResidual(lr.reg.yMinResidual);
        reg.setYMaxResidual(lr.reg.yMaxResidual);

        reg.setR(lr.reg.r);// maybe NaN
        reg.setSumSqrResidual(lr.reg.sumSquaredResiduals);

        return r;
    }

    // null if both limits are null, but note: it maybe empty
    private static RegressionType.Intersections createIntersections(ObjectFactory of, LRCalculator lr, double confidence, Double lowerLimit, Double upperLimit) throws JAXBException {
        if (lowerLimit == null && upperLimit == null) return null;

        RegressionType.Intersections inter = of.createRegressionTypeIntersections();

        if (lr.reg.slope != 0) { // no slope no intersection, or fit is on limit
            inter.setLowerLinear(createLinearIntersection(of, lr, lowerLimit));
            inter.setUpperLinear(createLinearIntersection(of, lr, upperLimit));
        }

        // at least one
        if (lr.n >= 3) { // StudentTValues isn't defined for n<3
            boolean dblSide = (lowerLimit != null && upperLimit != null);
            double invStudT = StudentTValues.invStudT(confidence, lr.n, !dblSide);

            inter.setLowerConfidence(createConfidenceIntersection(of, lr, invStudT, lowerLimit, false));
            inter.setUpperConfidence(createConfidenceIntersection(of, lr, invStudT, upperLimit, true));
        }

        return inter;
    }

    /**
     * null out if null in
     *
     * @param of
     * @param lr
     * @param limit
     * @return
     * @throws javax.xml.bind.JAXBException
     */
    private static XIntersectionType createLinearIntersection(ObjectFactory of, LRCalculator lr, Double limit) throws JAXBException {
        if (limit == null) return null;
        Double x = lr.reg.getLineIntersection(limit.doubleValue());
        if (x == null) return null;
        XIntersectionType i = of.createXIntersectionType();
        i.setX0(x);
        return i;
    }

    /**
     * null out if null in, empty array calculated
     *
     * @param of
     * @param lr
     * @param invStudT
     * @param limit
     * @param upper
     * @return
     * @throws javax.xml.bind.JAXBException
     */
    private static XIntersectionType createConfidenceIntersection(ObjectFactory of, LRCalculator lr, double invStudT, Double limit, boolean upper) throws JAXBException {
        if (limit == null) return null;
        double[] xx = lr.reg.getTrumpetIntersection(invStudT, limit, upper);
        if (xx.length == 0) return null;

        XIntersectionType i = of.createXIntersectionType();
        i.setX0(xx[0]);
        if (xx.length == 1) return i;
        i.setX1(xx[1]);
        return i;
    }
}
