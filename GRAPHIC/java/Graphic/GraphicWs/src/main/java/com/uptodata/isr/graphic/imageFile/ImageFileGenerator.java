package com.uptodata.isr.graphic.imageFile;

import com.keypoint.PngEncoder;
import org.freehep.graphicsio.emf.EMFGraphics2D;
import org.freehep.graphicsio.svg.SVGGraphics2D;
import org.freehep.util.UserProperties;

import java.awt.*;
import java.awt.image.BufferedImage;
import java.io.IOException;
import java.io.OutputStream;
import java.util.zip.Deflater;

/**
 * Easy create image files out of a Drawable, currently wmf, svg and png (non
 * ImageIO, creates always 24/32 bpp images, is slow) are supported ; other
 * ImageIO types maybe integrated easily on jre 1.4+
 * <p>
 * TODO properties like background-color in wmf, others ...
 *
 * @author PeterBuettner.de
 */
public class ImageFileGenerator {


    /**
     * Writes the graphic as a image file into the stream
     *
     * @param out
     * @param drawable what to paint
     * @param size
     * @param desc     defines the format
     * @throws java.io.IOException
     */

    public static void writeImage(OutputStream out, Drawable drawable, Dimension size, ImageFileDesc desc) throws IOException {
//	Dimension size = graphic.getSize();

        if ("png".equals(desc.getType())) {
            writePngImage(out, drawable, size, desc);
            return;
        }
        // emf,svg:
        if (desc.isCompressed()) out = new GZIPOutputStream(out, desc.getCompression().intValue());

        if ("svg".equals(desc.getType())) {
            writeSvg(out, drawable, size, desc);
            return;
        }
        if ("emf".equals(desc.getType())) {
            writeEmf(out, drawable, size, desc);
            return;
        }
        throw new IllegalArgumentException("Can't write image type:" + desc);
    }

    /**
     * Writes the graphic as a png image file into the stream, supports
     *
     * @param out
     * @param drawable
     * @param size
     * @param desc
     * @throws java.io.IOException
     */

    private static void writePngImage(OutputStream out, Drawable drawable, Dimension size, ImageFileDesc desc) throws IOException {

        int imgType = 0;

        int bpp = 32;
        if (desc.getColorDepth() != null) bpp = desc.getColorDepth().intValue();
        switch (bpp) {
            case 32:
                imgType = BufferedImage.TYPE_INT_ARGB;
                break;
            case 24:
                imgType = BufferedImage.TYPE_INT_RGB;
                break;
            case 8:
                if (Boolean.TRUE.equals(desc.getGrey()))
                    imgType = BufferedImage.TYPE_BYTE_GRAY;
                else
                    imgType = BufferedImage.TYPE_BYTE_INDEXED;
                break;

            default:
                throw new IllegalArgumentException("ImageFileDesc has wrong properties for png:" + desc);
        }

        // ... paint ....................................................
        double fac = 1; // ~5 -> test: big image, small paint -> looks good if printed
        int imgWidth = (int) (size.width * fac);
        int imgHeight = (int) (size.height * fac);

        BufferedImage bi = new BufferedImage(imgWidth, imgHeight, imgType);
        boolean hasAlpha = bi.getColorModel().hasAlpha();
        boolean antiAlias = bpp == 32 || bpp == 24 ||
                (bpp == 8 && Boolean.TRUE.equals(desc.getGrey()));

        Graphics2D g = bi.createGraphics();
        try {
            if (hasAlpha) g.setColor(new Color(255, 255, 255, 255));
            else g.setColor(Color.white);
            g.fillRect(0, 0, imgWidth, imgHeight);
            setAA(g, antiAlias);
            g.setRenderingHint(RenderingHints.KEY_TEXT_ANTIALIASING, RenderingHints.VALUE_TEXT_ANTIALIAS_OFF);
            // or round strokes jitter noticable
            g.setRenderingHint(RenderingHints.KEY_STROKE_CONTROL, RenderingHints.VALUE_STROKE_PURE);
            g.scale(fac, fac);
            drawable.draw(g, size);
        } finally {
            g.dispose();
        }

        // ... write ..........................................
        int comp = desc.getCompression() == null ? Deflater.DEFAULT_COMPRESSION : desc.getCompression().intValue();
        if (comp == Deflater.DEFAULT_COMPRESSION) comp = 9;
        if (true) { // jre13
            PngEncoder enc = new PngEncoder(bi, true, 0, comp);
            out.write(enc.pngEncode());
        } else { // jre14 update if we reconvert to 1.4+
            //	ImageWriter iw = new PNGImageWriter(null, comp); // our own incarnation
            //	iw.setOutput(ImageIO.createImageOutputStream(out));
            //	iw.write(bi);

            // the default writer, no compression level
            //	ImageIO.write(bi, "png", out);
        }

        out.flush();
        out.close();

    }

/*
 Info on svg and emf writer in FreeHep
   
they set the background, written in startExport()

if(TRANSPARENT) back=transp. 
else if(BACKGROUND) back=BACKGROUND_COLOR 
else back=white
*/


    /**
     * Writes the graphic as an emf into the stream.
     *
     * @param out
     * @param drawable
     * @param size
     * @param desc
     * @throws java.io.IOException
     */
    private static void writeEmf(OutputStream out, Drawable drawable, Dimension size, ImageFileDesc desc) throws IOException {
        UserProperties p = new UserProperties();
        p.setProperty(EMFGraphics2D.TRANSPARENT, true);
//	 don't compress, we do this in the caller

        EMFGraphics2D.setDefaultProperties(p);
        EMFGraphics2D g = new EMFGraphics2D(out, size);
        g.setDeviceIndependent(true);// so it works on headless

        // this works, but may become 'strange': white-on-white:
//	g.setColorMode(PrintColor.GRAYSCALE);

        g.startExport();
        drawable.draw(g, size);
        g.endExport();
    }

    /**
     * Writes the graphic as a svg into the stream.
     *
     * @param out
     * @param drawable
     * @param size
     * @param desc
     * @throws java.io.IOException
     */
    private static void writeSvg(OutputStream out, Drawable drawable, Dimension size, ImageFileDesc desc) throws IOException {
        UserProperties p = new UserProperties();
        p.setProperty(SVGGraphics2D.TRANSPARENT, true);
        p.setProperty(SVGGraphics2D.COMPRESS, false); // don't compress, we do this in the caller

        SVGGraphics2D.setDefaultProperties(p);
        SVGGraphics2D g = new SVGGraphics2D(out, size);
        g.setDeviceIndependent(true);// so it works on headless

        // this works, but may become 'strange': white-on-white
//	g.setColorMode(PrintColor.GRAYSCALE);

        g.startExport();
        drawable.draw(g, size);
        g.endExport();
    }

    // helpers
    private static boolean isAA(Graphics2D g) {
        return RenderingHints.VALUE_ANTIALIAS_ON.equals(g.getRenderingHint(RenderingHints.KEY_ANTIALIASING));
    }

    private static void setAA(Graphics2D g, boolean value) {
        if (value)
            g.setRenderingHint(RenderingHints.KEY_ANTIALIASING, RenderingHints.VALUE_ANTIALIAS_ON);
        else
            g.setRenderingHint(RenderingHints.KEY_ANTIALIASING, RenderingHints.VALUE_ANTIALIAS_OFF);
    }

}
