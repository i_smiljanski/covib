package com.uptodata.isr.graphic.chartVisual;

import org.jfree.chart.plot.XYPlot;
import org.jfree.data.Range;

import java.awt.*;
import java.awt.geom.Line2D;
import java.awt.geom.Rectangle2D;

/**
 * A simple vertical line at an x-Position that can be placed on an {@link XYPlot},
 * always fills all vertical space
 *
 * @author PeterBuettner.de
 */
class VerticalLineDrawer extends AbstractDrawer {

    /**
     * x-position of the vertical line
     */
    private double x;

    /**
     * Creates a new Drawer to be displayed in the whole area with the given parameters
     *
     * @param x      x-position of the vertical line
     * @param stroke line stroke
     * @param paint  line color
     */
    public VerticalLineDrawer(double x, Stroke stroke, Paint paint) {
        super(stroke, paint, null);// no area fill
        this.x = x;
    }

    /**
     * Draws
     *
     * @param g2       the graphics device.
     * @param plot     the plot.
     * @param dataArea the data area.
     */

    public void draw(Graphics2D g2, XYPlot plot, Rectangle2D dataArea) {
        Range r = Range.expand(plot.getRangeAxis().getRange(), 0.1, 0.1);
        double y1 = r.getLowerBound();
        double y2 = r.getUpperBound();
        paintShape(g2, translate(plot, dataArea, new Line2D.Double(x, y1, x, y2)));
    }


}
