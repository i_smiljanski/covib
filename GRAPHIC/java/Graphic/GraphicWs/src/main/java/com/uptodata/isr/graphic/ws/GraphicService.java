package com.uptodata.isr.graphic.ws;

import com.uptodata.isr.graphic.job.Job;
import com.uptodata.isr.graphic.statistic.LinearRegression;
import com.uptodata.isr.observer.transfer.DbFileSupplier;
import com.uptodata.isr.observer.transfer.FileSupplier;
import com.uptodata.isr.observers.base.dbAccess.DbData;
import com.uptodata.isr.observers.childServer.ObserverChildServerImpl;
import com.uptodata.isr.server.utils.fileSystem.FileSystemWorker;
import com.uptodata.isr.utils.ConvertUtils;
import com.uptodata.isr.utils.exceptions.MessageStackException;
import org.w3c.dom.Document;

import javax.jws.WebService;
import java.io.File;
import java.util.ArrayList;

/**
 * Created by smiljanskii60 on 14.09.2015.
 */
@WebService(name = "GraphicInterface", targetNamespace = "http://ws.graphic.isr.uptodata.com/")
public class GraphicService extends ObserverChildServerImpl {

    private String logLevel;
    DbData dbData;
    private String workFolderName = "workFolder";

    public GraphicService() {
    }

    public GraphicService(DbData dbData, String logLevel) {
        this.dbData = dbData;
        this.logLevel = logLevel;
        log = initLogger(logLevel, "graphic service");
        statusXmlMethod = 0;
        statusXml = initStatusXml();
    }


    public void init() throws MessageStackException {
        boolean isWorkFolderCreated = initWorkFolder();
        if (!isWorkFolderCreated) {
            log.error("XsltTransformServer can not create work folder with name==" + workFolderName);
        }
        log = initLogger(logLevel, "graphic service");
        statusXml = initStatusXml();  //test case for test exception: comment this row
    }


    /*
    * initiates the working directory if it doesn't exists
    * if wd exists, deletes the wd with the data
    * */
    private boolean initWorkFolder() throws MessageStackException {
        File workFolder = new File(workFolderName);
        if (workFolder.exists()) {
            try {
                boolean isOk = FileSystemWorker.tryToRemoveFolder(workFolder, 5, 500);
            } catch (Exception e) {
                String workFolderPath = workFolder.getAbsolutePath();
                String errorMsg = "Can not remove old work directory " + workFolderPath + System.lineSeparator() + " " + e;
                log.error(errorMsg);
                throw new MessageStackException(e, MessageStackException.SEVERITY_CRITICAL, errorMsg,
                        MessageStackException.getCurrentMethod());
            }
        }
        try {
            boolean isOk = FileSystemWorker.tryToCreateFolder(workFolderName, 5, 500);
        } catch (Exception e) {
            String workFolderPath = workFolder.getAbsolutePath();
            String errorMsg = "Can not create  work directory " + workFolderPath + " " + e;
            log.error(errorMsg);
            throw new MessageStackException(e, MessageStackException.SEVERITY_CRITICAL, errorMsg,
                    MessageStackException.getCurrentMethod());
        }

        return FileSystemWorker.checkFolder(workFolder);
    }


    public String createGraphics(byte[] xml, String jobId, String delimiter, String configName,
                                 String logLevel, String logReference) throws MessageStackException {
        log = initLogger(logLevel, logReference);
        log.stat("begin == xml length = " + xml.length);
        DbFileSupplier fileWorker = null;
        try {
            Job job = new Job();
            ArrayList images = job.getImageList(xml);
            if (images == null || images.size() == 0) {
                log.error("no images");
                return "F";
            }

            File jobFolder = new File(workFolderName, jobId + "_graphics");
            fileWorker = new DbFileSupplier(jobFolder, dbData);
            fileWorker.init("", "", "GRAPHIC");

            File inputs = new File(jobFolder, fileWorker.inputFolderName);

            if (logLevel.equalsIgnoreCase("debug")) {
                File inputXml = new File(inputs, "input.xml");
                ConvertUtils.bytesToFile(xml, inputXml.getPath());
            }

            log.debug("images size==" + images.size());
            for (Object imageO : images) {
                ArrayList image = (ArrayList) imageO;
                String imageName = (String) image.get(1);

                File results = new File(jobFolder, fileWorker.resultFolderName);
                File resultImg = new File(results, imageName);
                ConvertUtils.bytesToFile((byte[]) image.get(0), resultImg.getPath());

                String transferKeyId = jobId + delimiter + imageName + delimiter + "GRAPHIC";

                log.debug("vor upload ");
                File resultFile = fileWorker.getResultFile(imageName);
                if (resultFile.exists()) {
                    try {
                        boolean isUploaded = fileWorker.uploadFile(resultFile, transferKeyId, configName, logLevel, logReference);
                        log.debug("upload ==" + isUploaded);
                    } catch (Exception e) {
                        log.error("Could not upload file== " + resultFile.getAbsolutePath() + " " + e);
                    }
                } else {
                    log.error("Could not upload fil== " + resultFile.getAbsolutePath() + ": the file does not exist!");
                }
            }
            log.stat("end");
            return "T";
        } catch (Exception e) {
            String userStack = "error on the server";
            onMethodFailed(e, MessageStackException.SEVERITY_CRITICAL, userStack, MessageStackException.getCurrentMethod(), statusXmlMethod);
            return "F";
        } finally {
            if (!logLevel.equalsIgnoreCase("debug")) {
                clearFileWorker(fileWorker);
            }
        }
    }


    public String createCalculation(byte[] xml, String jobId, String delimiter, String configName,
                                    String logLevel, String logReference) throws MessageStackException {
        log = initLogger(logLevel, logReference, jobId);
        log.stat("begin");
        DbFileSupplier fileWorker = null;
        try {
            Document doc = ConvertUtils.convertByteToDocument(xml);

            File jobFolder = getJobFolder(jobId + "_calc");  //recursive, workaround because the calculation can be called 2 times during job

            log.debug(jobFolder);
            fileWorker = new DbFileSupplier(jobFolder, dbData);
            fileWorker.init("", "", configName);

            File inputs = new File(jobFolder, fileWorker.inputFolderName);

            if (logLevel.equalsIgnoreCase("debug")) {
                File inputXml = new File(inputs, "input.xml");
                ConvertUtils.bytesToFile(xml, inputXml.getPath());
            }

            LinearRegression regression = new LinearRegression();
            regression.taskToResultAll(doc.getDocumentElement(), log);
            byte[] bytes = ConvertUtils.convertDocumentToByte(doc);
            String resultName = "calcresult.xml";

            File results = new File(jobFolder, fileWorker.resultFolderName);
            File result = new File(results, resultName);
            ConvertUtils.bytesToFile(bytes, result.getPath());

            String transferKeyId = jobId + delimiter + resultName;

            log.debug("vor upload ");
            File resultFile = fileWorker.getResultFile(resultName);
            if (resultFile.exists()) {
                try {
                    boolean isUploaded = fileWorker.uploadFile(resultFile, transferKeyId, configName, logLevel, logReference);
                    log.debug("upload ==" + isUploaded);
                } catch (Exception e) {
                    log.error("Could not upload file ==" + resultFile.getAbsolutePath() + " " + e);
                }
            } else {
                log.error("Could not upload file ==" + resultFile.getAbsolutePath() + ": the file does not exist!");
            }

            log.stat("end");
            return "T";
        } catch (Exception e) {
            e.printStackTrace();
            log.debug("error==" + e);
            throw new MessageStackException(e, MessageStackException.SEVERITY_CRITICAL,
                    "error by create calculation " + e.getMessage(), MessageStackException.getCurrentMethod());
        } finally {
            if (!logLevel.equalsIgnoreCase("debug")) {
                clearFileWorker(fileWorker);
            }
        }
    }



    private File getJobFolder(String jobFolderName) {
        File jobFolder = new File(workFolderName, jobFolderName);
        if (jobFolder.exists()) {
            return getJobFolder(jobFolderName + "_1");
        }
        return jobFolder;
    }


    private void clearFileWorker(FileSupplier fileWorker) {
        if (fileWorker != null) {
            try {
                fileWorker.clearFileSystem();
            } catch (MessageStackException e) {
                log.error("error==" + e);
            }
        }
    }


    public static void main(String[] args) {
        try {
            GraphicService graphicService = new GraphicService();
//            byte[] chartjobXml = ConvertUtils.fileToBytes(System.getProperty("user.dir") + "\\tests\\chartjob1.xml");
//            byte[] rawdata = ConvertUtils.fileToBytes(System.getProperty("user.dir") + "\\tests\\ISR$UCIS$CHARTCALC.xml");
//
//            graphicService.dbData = new DbData("ISROWNER_UCIS_DEV", "ucis-d-d", "8082", "isrws_ucis_dev", "isrws_ucis_dev");
////            graphicService.createGraphics(chartjobXml, "7428", ";", "GRAPHIC_RESULT", "DEBUG", "TEST");
//
//            graphicService.createCalculation(rawdata, "6745", "_", "GRAPHIC_CALCULATION", "DEBUG", "TEST");


        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}
