package com.uptodata.isr.graphic.ws;

import com.uptodata.isr.observers.base.dbAccess.DbData;
import com.uptodata.isr.observers.base.secutity.CryptDbPropertiesHandler;
import com.uptodata.isr.server.utils.fileSystem.Constants;
import com.uptodata.isr.server.utils.logging.log4j2.DbDataForLogging;
import com.uptodata.isr.server.utils.logging.log4j2.LogHelper;
import com.uptodata.isr.server.utils.network.UrlUtil;
import com.uptodata.isr.server.utils.security.ProtectionUtil;
import com.uptodata.isr.utils.ConvertUtils;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.util.Properties;

/**
 * Created by smiljanskii60 on 14.09.2015.
 */
public class GraphicServicePublisher {

    public static void main(String[] args) {
        Logger log = null;
        try {
            Properties props = ConvertUtils.convertArrayToProperties(args);

            String logLevel = props.getProperty("logLevel");
            String wsUrl = props.getProperty("wsUrl");
            DbData dbData = new DbData(props);

            CryptDbPropertiesHandler.saveDbPropertiesForLog(args);

            log = LogHelper.initLogger(logLevel, "graphic");
            log.info(ConvertUtils.propertiesWithPasswordToString(props));

            GraphicService graphicService = new GraphicService(dbData, logLevel);
            graphicService.init();

            UrlUtil.createAndPublishEndpoint(wsUrl, graphicService);

            log.info("Service 'GraphicService' is started on '" + wsUrl + "'");

        } catch (Exception e) {
            if (log != null) {
                log.error(e);
            }
            e.printStackTrace();
        }
    }
}
