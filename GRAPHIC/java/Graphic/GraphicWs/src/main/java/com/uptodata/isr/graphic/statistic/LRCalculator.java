package com.uptodata.isr.graphic.statistic;


import com.uptodata.isr.jaxb.all.RegressionType;
import com.uptodata.isr.jaxb.all.Result;

/**
 * Core Linear Regession and statistics part. <br>
 * <p>
 * <p>
 * Sometimes values can't be calculated, e.g. when xi are all the same =&gt;
 * variance(x) ==0, then the dependend Regression is null.
 * <p>
 * <p>
 * Note to maintainers: never use int literals (e.g.: 2, 42, n/2) in math
 * expressions like (1/n) since we may get errors due to integer division
 * (1/10==0 and 1.0/10==0.1), use double (e.g. n/2.0) and never float (e.g.:
 * n/2f) due to rounding errors.
 * </p>
 * <p>
 * <p>
 * We use package accessible fields here to have maximum clearity (no getXYZ()
 * noise). Since all are final there can't be an accidental change in the
 * values: all is readonly. Think of this class as a C/Pascal Record with some
 * functions. <br>
 * Besides only some realy basic classes are used: <br>
 * Double, Math, double[] and some Exceptions
 * <p>
 * </p>
 *
 * @author PeterBuettner.de
 */
class LRCalculator {

    // internal use in Regression only, can't be declared there since Regression
    // isn't static
    private static final double[] EMPTY = new double[0];

    /**
     * number of values
     */
    int n;

    /**
     * min(x)
     */
    double xMin;
    /**
     * max(x)
     */
    double xMax;

    /**
     * min(y)
     */
    double yMin;
    /**
     * max(y)
     */
    double yMax;

    /**
     * average(x)
     */
    double xMean;

    /**
     * average(y)
     */
    double yMean;

    // Variance: vUU = Sum((Ui)(c))/n
    /**
     * Variance(x)
     */
    double vX;
    /**
     * Variance(y)
     */
    double vY;
    /**
     * Variance(xy)
     */
    double vXY;

    /**
     * only set if available: vX>0
     */
    Regression reg;

    LRCalculator(Result r) {
        this.n = r.getN();
        this.xMin = r.getBasic().getXMin();
        this.xMax = r.getBasic().getXMax();
        this.yMin = r.getBasic().getYMin();
        this.yMax = r.getBasic().getYMax();
        this.xMean = r.getBasic().getXMean();
        this.yMean = r.getBasic().getYMean();
        this.vX = r.getBasic().getXVariance();
        this.vY = r.getBasic().getYVariance();
        this.vXY = r.getBasic().getXyVariance();
        if (r.getBasic().getRegression() != null) {
            this.reg = new Regression(r.getBasic().getRegression());
        }
    }

    ;

    /**
     * Both arrays needs to have the same length>0.
     *
     * @param x we don't save or change them
     * @param y we don't save or change them
     * @throws IllegalArgumentException
     */
    LRCalculator(double[] x, double[] y) throws IllegalArgumentException {
        if (x.length != y.length)
            throw new IllegalArgumentException(
                    "Different number of x and y values.");
        if (x.length < 1)
            throw new IllegalArgumentException(
                    "Not enough values, at least 1 (x,y)-pair is needed.");

        n = x.length;

        // temporary vars, all fields are final!
        double sumX = 0;
        double sumY = 0;
        double sumXX = 0;
        double sumYY = 0;
        double sumXY = 0;

        double _xMin = x[0];
        double _xMax = x[0];
        double _yMin = y[0];
        double _yMax = y[0];

        for (int i = 0; i < n; i++) {
            double xi = x[i];
            double yi = y[i];
            sumX += xi;
            sumY += yi;
            sumXX += xi * xi;
            sumYY += yi * yi;
            sumXY += xi * yi;

            if (xi < _xMin)
                _xMin = xi;
            if (_xMax < xi)
                _xMax = xi;
            if (yi < _yMin)
                _yMin = yi;
            if (_yMax < yi)
                _yMax = yi;
        }
        xMean = sumX / n;
        yMean = sumY / n;

        xMin = _xMin;
        xMax = _xMax;
        yMin = _yMin;
        yMax = _yMax;

        // Variance:
        // vUU = Sum((Ui- U_)(c))/n == Sum((Ui)(c))/n - (Sum(Ui))(c)/n/n ==
        // Sum((Ui)(c))/n - U_*U_ ==
        // U_ =mean of Ui
        // note: "(n==1)?0:..." is to absolutely have _NO_ rounding problems at
        // all
        vX = (n == 1) ? 0 : sumXX / n - sumX * sumX / n / n; // sumX*sumX/n/n ==
        // xMean^2
        vY = (n == 1) ? 0 : sumYY / n - sumY * sumY / n / n;
        vXY = (n == 1) ? 0 : sumXY / n - sumX * sumY / n / n;

        // if(false){ // two pass calculation nearly the same as above (tested:
        // 29.3.2005)
        // vX = 0;
        // vY = 0;
        // vXY = 0;
        // for(int i=0;i<n;i++){
        // double dxi = x[i] -xMean;
        // double dyi = y[i] -yMean;
        // vX += dxi*dxi;
        // vY += dyi*dyi;
        // vXY += dxi*dyi;
        // }
        // vX = vX /n;
        // vY = vY /n;
        // vXY = vXY /n;
        //
        // System.out.println( sumXX/n-sumX*sumX/n/n - vX );
        // System.out.println( sumYY/n-sumY*sumY/n/n - vY );
        // System.out.println( sumXY/n-sumX*sumY/n/n - vXY );
        // }

        // if vX==0 then all x are same
        reg = vX == 0 ? null : new Regression(x, y, vX, vY, vXY);
    }

    class Regression {
        /**
         * Slope == de:Steigung
         */
        double slope;

        /**
         * Intercept == de:Achsenabschnitt
         */
        double intercept;

        /**
         * correlation coefficient
         */
        double r;

        /**
         * minimum y Residual
         */
        double yMinResidual;

        /**
         * maximum y Residual
         */
        double yMaxResidual;

        /**
         * sum of the squared residuals
         */
        double sumSquaredResiduals;

        Regression(RegressionType reg) {
            this.slope = reg.getSlope();
            this.intercept = reg.getIntercept();
            this.r = reg.getR();
            this.yMinResidual = reg.getYMinResidual();
            this.yMaxResidual = reg.getYMaxResidual();
            this.sumSquaredResiduals = reg.getSumSqrResidual();
        }

        Regression(double[] x, double[] y, double vX, double vY, double vXY) {
            if (vX == 0)
                throw new IllegalArgumentException("variance X==0");

            slope = vXY / vX;
            intercept = yMean - slope * xMean;

            // we need temp vars since the fields are final
            // min/max residuals and squardSum:
            double min = y[0] - (x[0] * slope + intercept);
            double max = min;
            double sumSqrRes = 0;
            for (int i = 0; i < n; i++) {
                double dy = (x[i] * slope + intercept) - y[i];
                if (dy < min)
                    min = dy;
                if (dy > max)
                    max = dy;
                sumSqrRes += dy * dy;
            }
            yMinResidual = min;
            yMaxResidual = max;
            sumSquaredResiduals = sumSqrRes;

            // TODO -> vXY or vY==0 and what comes out? 0?
            r = vXY / sqrt(vX * vY); // may become NaN
            // r = (vY==0)? Double.NaN : vXY / sqrt( vX*vY ) ; // same as line
            // before
        }

		/*
         * // this old code is more complicated and so error-prone, but it //
		 * was working allright double getSumSquaredResiduals() {
		 * if(Double.isNaN(r))return 0; // is this clean? return n vY (1.0-rr);
		 * 
		 * // or the 'same': // vY(1-r(c)) = vY( vXvY - vXY(c))/(vXvY) =( vXvY -
		 * vXY(c))/vX // return n ( vXvY - sqr(vXY))/vX; }
		 */

        /**
         * x value so that x * slope + intercept=yValue <br>
         * iow: returns (yValue -intercept)/slope <br>
         * or null if slope==0
         *
         * @param yValue
         * @return null if slope==0
         */
        Double getLineIntersection(double yValue) {
            if (slope == 0)
                return null;
            return new Double((yValue - intercept) / slope);
        }

        /**
         * Find the x values so that <code>Trumpet(t,x)=yValue</code>. There are
         * cases where there is no solution and an empty array is returned.
         * <p>
         * <pre>
         * Mathematika Syntax:
         *     Simplify[ Solve[vXY/vX x + Y-vXY/vX X + k Sqrt[1/n + (x-X)&circ;2/(n vX)]== c, x ] ]
         * Note: X=xMean; Y=yMean in the formulas
         *
         * Is as complex as:
         *     Simplify[ Solve[m x + b + k Sqrt[1/n + (x-X)&circ;2/(n vX)]== c, x ] ].
         *
         * </pre>
         *
         * @param invStudT calc it before, we don't care about single/double sided
         * @param yValue   y Value that should be tested for intersections
         * @param upper    if we look for intersections with the upper confidence
         *                 border, else lower
         * @return double[] with x-values <br>
         * length==0 if no solution, or no trumpet is deliverable <br>
         * length==1 with 1 solution <br>
         * length==2 with 2 solutions <br>
         * this array is always sorted
         */
        double[] getTrumpetIntersection(double invStudT, double yValue,
                                        boolean upper) {
            // if(n<=2)return EMPTY; // this is already enshured since vX>0

            // sdXY = sqrt[ n / (n-2) (vY - vXY^2 / vX ) ] if vY==0 -> vXY==0
            final double sdXY = sqrt(sumSquaredResiduals / (n - 2.0));
            if (Double.isNaN(sdXY))
                return EMPTY; // e.g. if n==2, then trumpet==line

			/*
			 * 
			 * Find solutions of: m x + b + k Sqrt[1/n + (x-X)^2/(n vX)] == c m
			 * x + (b-c) + k Sqrt[1/n + (x-X)^2/(n vX)]== 0 (note: k = invStudT
			 * sdXY) d := b-c m x + d + k Sqrt[1/n + (x-X)^2/(n vX)]== 0
			 * 
			 * Mathematica told us that are the solutions in x are:
			 * ----------------(1) (2dmnvX + 2k^2X (c) ((-2dmnvX - 2k^2X)^2 -
			 * 4(k^2 - m^2nvX)(k^2vX - d^2nvX + k^2X^2))^(1/2))/(2(k^2 -
			 * m^2nvX)) now transform: ----------------(2) A := mnvX (2dA +
			 * 2k^2X (c) ((-2dA - 2k^2X)^2 - 4(k^2 - mA)(k^2vX - d^2nvX +
			 * k^2X^2))^(1/2))/(2(k^2 - mA)) ----------------(3) K := k^2 (2dA +
			 * 2KX (c) ((-2dA - 2KX)^2 - 4(K - mA)(KvX - d^2nvX + KX^2))^0.5) /
			 * (2(K - mA)) ----------------(4) (2dA + 2KX (c) 2((dA + KX)^2 - (K -
			 * mA)(KvX - d^2nvX + KX^2))^0.5) / (2(K - mA)) ----------------(5)
			 * (dA + KX (c) ((dA + KX)^2 - (K - mA)(KvX - d^2nvX + KX^2))^0.5) /
			 * (K - mA) ---------------- Note: Mathematica tells us that
			 * (1)==(5) ----------------
			 * 
			 * Solutions: x_1,2 = ( V (c) Sqrt(U) ) / W
			 * 
			 * V := dA + KX; U := (dA + KX)^2 - (K - mA)(KvX - d^2nvX + KX^2); W
			 * := K - mA;
			 */

            final double m = slope;
            final double d = intercept - yValue; // d = b-c
            final double k = invStudT * sdXY;
            final double X = xMean;
            final double A = m * n * vX;
            final double K = k * k;

            final double V = d * A + K * X;
            final double U = sqr(d * A + K * X) - (K - m * A)
                    * (K * vX - sqr(d) * n * vX + K * sqr(X));
            final double W = K - m * A; // ausmulti -> bei r->1 kann W ==0
            // werden

            if (U < 0)
                return EMPTY; // no solution
            if (W == 0)
                return EMPTY; // TODO check. Hmm, kann 0/0 sein also evtl.
            // fester Wert?
            // W==0 -> k^2 = m^2 n vX (... k in Sqrt , raus ...) Sqrt(m^2 vX
            // +dx^2 m^2 ) -> m x +d + m Sqrt(vX+dx^2)

            // ... now real results: one or two solutions.
            // hit upper or lower?

            if (U == 0) { // at most one solution
                double x0 = V / W;
                if (hitTest(x0, k, yValue, upper))
                    return new double[]{x0};
                else
                    return EMPTY; // wrong side
            }

            // test if we matched upper or lower curve!
            final double x0 = (V - sqrt(U)) / W;
            final double x1 = (V + sqrt(U)) / W;
            final boolean h0 = hitTest(x0, k, yValue, upper);
            final boolean h1 = hitTest(x1, k, yValue, upper);

            if (h0 && h1)
                return new double[]{x0, x1}; // since sqrt(U) is >0 we are
            // sorted!
            if (h0)
                return new double[]{x0};
            if (h1)
                return new double[]{x1};

            return EMPTY; // neither h0 nor h1

        }

        /**
         * Find out if the yValue hit upper or lower curve at x, be shure to
         * call this only if all data is right, <b>VERY</b> specialized, assumes
         * that x shurely hit anything. <b>Use only from
         * getTrumpetIntersection()</b>
         *
         * @param x      x value of the hit candidate
         * @param k      as in getTrumpetIntersection(): "invStudT * sdXY"
         * @param yValue y vale of the intersection
         * @param upper  want to hit upper curve?
         * @return
         */
        private boolean hitTest(double x, double k, double yValue, boolean upper) {
			/*
			 * we calculate the difference of the confidence-curve to the yValue
			 * to decide if we hit that yValue Line. We distinguish this by
			 * 'scaling' this difference by the offset of the confidence-curve
			 * to the linear-fit.
			 */
            double offset = k * sqrt(1.0 / n + sqr(x - xMean) / (n * vX));
            // offset==0 if sumSquaredResiduals==0; but shure: offset >=0
            // we have no scaling then, dY = yValue - (slope*x+intercept)
            // but dY maybe != 0 since x may have 'jittered'
            // now since we _know_ that we hit anything we may return true then
            if (offset == 0)
                return true;

            double sgn = upper ? 1 : -1;
            return Math.abs(yValue - (slope * x + intercept + sgn * offset)) < 0.01 * offset; // 1%
        }

        private double sqr(double x) {
            return x * x;
        }

        private double sqrt(double x) {
            return Math.sqrt(x);
        }

    }
	/*
	 * Other Mathematica Notation:
	 * 
	 * f = Function [x, x + a + g Sqrt[1 + h x^2]]
	 * 
	 * Simplify[ Solve[ f[x]==0,x ]] 2 2 2 2 2 2 a - Sqrt[g (1 + a h - g h)] a +
	 * Sqrt[g (1 + a h - g h)] {{x -> ------------------------------}, {x ->
	 * ------------------------------}} 2 2 -1 + g h -1 + g h
	 * 
	 * x0,1 = (a (c) (g^2(1 + a^2h - g^2h))^(1/2))/(g^2h - 1) <=> (g>=0) x0,1 = (a
	 * (c) g(1 + a^2h - g^2h)^(1/2))/(g^2h - 1)
	 */

}
