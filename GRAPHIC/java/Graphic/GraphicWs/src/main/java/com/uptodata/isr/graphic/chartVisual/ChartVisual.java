package com.uptodata.isr.graphic.chartVisual;

import com.uptodata.isr.batic.Styler;
import com.uptodata.isr.batic.fragments.MyCSSEngine;
import com.uptodata.isr.batic.fragments.PaintConverter;
import com.uptodata.isr.batic.fragments.StylableElement;
import com.uptodata.isr.graphic.chartVisual.style.J2DStyle;
import com.uptodata.isr.graphic.jaxbWrap.DatasetWrap;
import com.uptodata.isr.graphic.statistic.StudentTValues;
import com.uptodata.isr.jaxb.all.*;
import org.jfree.chart.JFreeChart;
import org.jfree.chart.LegendItem;
import org.jfree.chart.annotations.XYAnnotation;
import org.jfree.chart.axis.LogarithmicAxis;
import org.jfree.chart.axis.NumberAxis;
import org.jfree.chart.axis.NumberTickUnit;
import org.jfree.chart.axis.ValueAxis;
import org.jfree.chart.event.AnnotationChangeListener;
import org.jfree.chart.plot.*;
import org.jfree.chart.renderer.xy.AbstractXYItemRenderer;
import org.jfree.chart.renderer.xy.StandardXYItemRenderer;
import org.jfree.chart.renderer.xy.XYItemRenderer;
import org.jfree.chart.renderer.xy.XYItemRendererState;
import org.jfree.data.Range;
import org.jfree.data.xy.AbstractXYDataset;
import org.jfree.data.xy.XYDataset;
import org.jfree.data.xy.XYSeries;
import org.jfree.data.xy.XYSeriesCollection;
import org.jfree.text.TextUtilities;
import org.jfree.ui.Layer;

import javax.swing.*;
import java.awt.*;
import java.awt.geom.Rectangle2D;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;


/**
 * Visual graphic that displays datasets, linear regression, confidence intervall,
 * upper/lower limits. <br>
 * This class bind parts together: jaxB created interfaces, jFreeChart, our
 * Style classes, it wraps jFreeChart, so we may remove that and use another
 * implementations that create charts. Also the jaxb interfaces are only(?)
 * used here, so we are not much dependent on this xml wrapper.
 * <p>
 * <p>
 * Paint it with {@link #draw(java.awt.Graphics2D, java.awt.Dimension)}, paint legends with
 * {@link #drawSubImage(String, java.awt.Graphics2D, java.awt.Dimension)}
 * no antialiasing or anything else is done, setup your Graphics2D as needed,
 * ... well, a limit with horizontal lines uses a optimized stroke to have a clearer image
 * </p>
 * <p>
 * <p>
 * <p>
 * there are lots of inner classes here, they are small and mostly for jFreeChart
 * usage/wraping, so don't move them out and make them visible there, but maybe
 * rethink this later.
 * </p>
 *
 * @author PeterBuettner.de
 */

/*
 * save some info
 * 
 * trumpet schnittpunkte zeigen wenn:
 * if (value>0 && lr.getXMax()*2.5>value)// show only if > 0 and not to big
 * 
 * 
 * new content:
 * -----------
 * dataset        type="datasetVisualType"
 * limit          type="limitVisualType"
 * line           type="lineVisualType"
 * confidence     type="confidenceVisualType"
 */


public class ChartVisual {
/*
 * draw order in jfreechart:
 * grid
 * marker-back
 * annotations back
 * data
 * annotations front
 * 
 * So we may paint our content this way:
 * a new (dummy) dataset for each item, to hide them in legens:
 * XYPlot is called through LegendItemCollection getLegendItems()
 * and the renderer may just return null on
 *    LegendItem getLegendItem(int datasetIndex, int series)  
 * 
 */

    private final JFreeChart jFreeChart;
    private final Chart chart;
    private final Dimension size;

    /**
     * set to null after use?
     */
    private final Styler styler;

    /**
     * save legendpainters ~ subimages; keys==legendId value==LegendPainter
     */
    private Map legendMap = new HashMap();


    /**
     * preferred size in px, if not on screen paint 'as it was such big' see: 72px ~ 1 inch
     *
     * @return never null
     */
    public Dimension getSize() {
        return new Dimension(size);
    }

    /**
     * draws it at (0,0)
     *
     * @param g2
     * @param size
     */
    public void draw(Graphics2D g2, Dimension size) {
        jFreeChart.draw(g2, new Rectangle2D.Double(0, 0, size.width, size.height));
    }

    /**
     * Draws the legend at (0,0).
     *
     * @param legendId as defined in graphic
     * @param g
     * @param size     for the subimage
     * @throws IllegalArgumentException if the legendId is not found in the graphic
     */
    public void drawSubImage(String legendId, Graphics2D g, Dimension size) throws IllegalArgumentException {
        LegendPainter painter = (LegendPainter) legendMap.get(legendId);
        if (painter != null) painter.draw(g, size);
        else {
            throw new IllegalArgumentException("LegendId:'" + legendId + "' not found in the graphic.");
//			g.setColor(fakeLegendColor(legendId));
//			g.draw(new Rectangle2D.Double(0,0,size.width-1,size.height-1));
//			g.setFont(new Font("sansserif",Font.PLAIN, 8));
//			g.drawString(legendId, 0,size.height-2);
        }
    }


    /**
     * A new graphic.
     *
     * @param styler used to style the graphic
     * @param aChart what to draw
     */
    public ChartVisual(Styler styler, Chart aChart) {
        this.chart = aChart;
        this.styler = styler;

        size = new Dimension(chart.getWidth(), chart.getHeight());
        TextUtilities.setUseDrawRotatedStringWorkaround(false);
//		TextUtilities.setUseFontMetricsGetStringBounds(true);
        // ........................................

        //... axes ................
        ValueAxis xAxis = createAxis(chart.getPlot().getXAxis(), true);
        ValueAxis yAxis = createAxis(chart.getPlot().getYAxis(), false);

        // ... plot .....................................
        XYPlot plot = new XYPlot(null, xAxis, yAxis, null) {
            // or markers for 'index==0' will be painted twice
            protected void drawDomainMarkers(Graphics2D g2, Rectangle2D dataArea, Layer layer) {
            }
        };

        plot.setDatasetRenderingOrder(DatasetRenderingOrder.FORWARD);//PEB jfree 1.0
        addPlotContent(plot, chart.getPlot().getContent());

        // after plot is complete! whenever we do auto-scaling we need this, if not it does nothing
        yAxis.configure();
        xAxis.configure();

		/*
         * axis lines and plot outline became out of sync: half pixels off
		 * trumpet area paints over axis lines, so: switch axis lines off, make
		 * outline thicker since ticks would paint over line
		 */
        xAxis.setAxisLineVisible(false);
        yAxis.setAxisLineVisible(false);
        plot.setOutlineStroke(new BasicStroke());

        // grid
        plot.setDomainGridlinesVisible(false);
        plot.setRangeGridlinesVisible(false);

        // ... Now create the JFreeChart ...............
        jFreeChart = new JFreeChart(chart.getTitle(), JFreeChart.DEFAULT_TITLE_FONT, plot, false);

/*		// try  the internal legend, maybe used for ease later		
		StandardLegend l = new StandardLegend();
		l.setAnchor(StandardLegend.EAST);
		l.setDisplaySeriesShapes(true);
		
		// hack (dashes!) to have the outlined symbols in word-breaked vertical 
		// legend not paint. basic stroke very small is not enough
		// full transparent color doesn't works in emf 
		l.setShapeOutlinePaint(Color.white);
		l.setShapeOutlineStroke(new BasicStroke(0.001f,0,0,1,new float[]{0.01f,10000},0));
		l.setOutlineShapes(true);
		// .......................................
		jFreeChart.setLegend(l);
*/

        // set background in plot and graphic to transparent:
        plot.setBackgroundPaint(null);
        jFreeChart.setBackgroundPaint(null); // transparent

        RenderingHints rh = new RenderingHints(null);
        jFreeChart.setRenderingHints(rh); // 'null hints'
//		jFreeChart.setAntiAlias(true); // no, don't use ths @!%$&&%(c)$ set to 'none' and define as we draw

    }//----------------------------------------------------------------------

    private void addLegendItem(String legendId, LegendPainter painter) {
//	debugShowLegendIcon(painter);
        legendMap.put(legendId, painter);
    }

    private void addPlotContent(XYPlot plot, ContentType content) {
        MyCSSEngine cssEngine = styler.getCSSEngine();
        PaintConverter pc = new PaintConverter(cssEngine);

        List contentList = content.getItems();
        List list = new ArrayList(contentList.size());


        for (Object o : contentList) {
            // ... confidence .......................................
            if (o instanceof ConfidenceVisualType) {
                ConfidenceVisualType conf = (ConfidenceVisualType) o;
                TrumpetDataProvider dataSrc = createTrumpetDataProvider(conf);
                J2DStyle.Area style = getStyleConfidence(pc, conf);
                TrumpetDrawer td = new TrumpetDrawer(dataSrc, style.getStroke(), style.getStrokePaint(), style.getFillPaint());

                String legendId = conf.getLegendId();
                if (legendId != null)
                    addLegendItem(legendId, new LegendPainter.JFDrawableLegendPainter(plot, td));
//				addLegendItem(legendId, new LegendPainter.ConfidenceLegendPainter(style));

                list.add(td);
            }

            // ... data .....................................
            else if (o instanceof DatasetVisualType) {
                DatasetVisualType dataset = (DatasetVisualType) o;
                String title = dataset.getTitle();

                XYSeries series = new XYSeries(title == null ? "" : title);
                for (Object o1 : new DatasetWrap(dataset).getValues()) {
                    DatasetType.Value v = (DatasetType.Value) o1;
                    double x = v.getX();
                    double y = v.getY();
                    // anything + NaN == NaN:
                    double xErrHigh = x + nanIfNull(v.getXErrPlus());
                    double xErrDown = x + nanIfNull(v.getXErrMinus());
                    double yErrHigh = y + nanIfNull(v.getYErrPlus());
                    double yErrDown = y + nanIfNull(v.getYErrMinus());

                    XYDataItemErrs item = new XYDataItemErrs(x, y, xErrDown, xErrHigh, yErrDown, yErrHigh);
                    series.add(item, false);
                }

                J2DStyle.Dataset style = getStyleDataset(pc, dataset);
                String legendId = dataset.getLegendId();
                if (legendId != null)
                    addLegendItem(legendId, new LegendPainter.DatasetLegendPainter(style));

                list.add(new StyledXYSeriesCollection(series, style));
            }

            // ... Linear .............................
            else if (o instanceof LineVisualType) {
                LineVisualType lin = (LineVisualType) o;
                J2DStyle.Line style = getStyleLine(pc, lin);

                LinearDrawer linearDrawer = new LinearDrawer(lin.getSlope(), lin.getIntercept(), style.getStroke(), style.getStrokePaint());
                String legendId = lin.getLegendId();
                if (legendId != null)
                    addLegendItem(legendId, new LegendPainter.JFDrawableLegendPainter(plot, linearDrawer));
//				addLegendItem(legendId, new LegendPainter.LineLegendPainter(style));

                list.add(linearDrawer);
            }

            // ... vertical Line .............................
            else if (o instanceof VerticalLineVisualType) {
                VerticalLineVisualType vLin = (VerticalLineVisualType) o;
                J2DStyle.Line style = getStyleVerticalLine(pc, vLin);

                VerticalLineDrawer verticalLineDrawer = new VerticalLineDrawer(vLin.getX(), style.getStroke(), style.getStrokePaint());
                String legendId = vLin.getLegendId();
                if (legendId != null)
                    addLegendItem(legendId, new LegendPainter.JFDrawableLegendPainter(plot, verticalLineDrawer));
//				addLegendItem(legendId, new LegendPainter.LineLegendPainter(style));

                list.add(verticalLineDrawer);
            }

            // ... limits ...............................................
            else if (o instanceof LimitVisualType) {
                LimitVisualType limit = (LimitVisualType) o;
                J2DStyle.Area style = getStyleLimits(pc, limit);

                LimitDrawer limitDrawer = new LimitDrawer(limit.getLower(), limit.getUpper(), style.getStroke(), style.getStrokePaint(), style.getFillPaint());
                String legendId = limit.getLegendId();
                if (legendId != null)
                    addLegendItem(legendId, new LegendPainter.JFDrawableLegendPainter(plot, limitDrawer));
//				addLegendItem(legendId, new LegendPainter.LimitLegendPainter(style));
                list.add(limitDrawer);
            }
        }
        addPlotContent(plot, list);
    }//----------------------------------------------------------------------

    private J2DStyle.Dataset getStyleDataset(PaintConverter pc, DatasetVisualType dataset) {
        StylableElement elt = styler.setStyle("dataset", dataset.getStyle(), dataset.getClasses());
        J2DStyle.Line line = createStyleLine(pc, elt);
        J2DStyle.Symbol sym = new J2DStyle.Symbol(createStyleArea(pc, elt, true), pc.convertSymbolType(elt), pc.convertSymbolSize(elt));

        J2DStyle.Line errorLine = createStyleLine(pc, elt, PaintConverter.StrokeType.ERROR);
        PaintConverter.StrokeType stErrorCap = PaintConverter.StrokeType.ERROR_CAP;
        J2DStyle.ErrorCap errorCapLine = new J2DStyle.ErrorCap(pc.convertStroke(elt, stErrorCap), pc.convertStrokePaint(elt, stErrorCap), pc.convertErrorCapSize(elt));

        return new J2DStyle.Dataset(sym, line, errorLine, errorCapLine);
    }

    private J2DStyle.Area getStyleLimits(PaintConverter pc, LimitVisualType limit) {
        return createStyleArea(pc, styler.setStyle("limit", limit.getStyle(), limit.getClasses()), false);
    }

    private J2DStyle.Area getStyleConfidence(PaintConverter pc, ConfidenceVisualType confidence) {
        return createStyleArea(pc, styler.setStyle("confidence", confidence.getStyle(), confidence.getClasses()), false);
    }

    private J2DStyle.Line getStyleLine(PaintConverter pc, LineVisualType line) {
        return createStyleLine(pc, styler.setStyle("line", line.getStyle(), line.getClasses()));
    }

    private J2DStyle.Line getStyleVerticalLine(PaintConverter pc, VerticalLineVisualType line) {
        return createStyleLine(pc, styler.setStyle("verticalLine", line.getStyle(), line.getClasses()));
    }

    private J2DStyle.Line createStyleLine(PaintConverter pc, StylableElement elt) {
        return createStyleLine(pc, elt, PaintConverter.StrokeType.NORMAL);
    }

    private J2DStyle.Line createStyleLine(PaintConverter pc, StylableElement elt, PaintConverter.StrokeType strokeType) {
        return new J2DStyle.Line(pc.convertStroke(elt, strokeType), pc.convertStrokePaint(elt, strokeType));
    }


    private J2DStyle.Area createStyleArea(PaintConverter pc, StylableElement elt, boolean forSymbol) {
        PaintConverter.StrokeType st = forSymbol ? PaintConverter.StrokeType.SYMBOL : PaintConverter.StrokeType.NORMAL;
        return new J2DStyle.Area(pc.convertStroke(elt, st), pc.convertStrokePaint(elt, st), pc.convertFillPaint(elt, forSymbol));
    }


    /**
     * don't call twice. 'cause of indices we have to create for jFreeChart-plot
     *
     * @param plot
     * @param items
     */
    private void addPlotContent(XYPlot plot, List items) {

        for (int i = 0; i < items.size(); i++) {
            Object item = items.get(i);

            XYDataset set = null;
            XYItemRenderer render = null;
            if (item instanceof JFDrawable) {
                set = new DummyData();
                render = new DrawableRenderer((JFDrawable) item);
            } else if (item instanceof StyledXYSeriesCollection) {
                set = (StyledXYSeriesCollection) item;
                render = ((StyledXYSeriesCollection) item).getDataRenderer();
            } else
                throw new IllegalArgumentException("Unknown datatype class for plot, class: '" + item.getClass() + "'.");

            plot.setDataset(i, set);
            plot.setRenderer(i, render);
        }
    }// ---------------------------------------------------------------------------------

    /**
     * convert a jaxB description to a data provider
     *
     * @param convt
     * @return
     */
    private static TrumpetDataProvider createTrumpetDataProvider(ConfidenceVisualType convt) {
        //all is checked in the jaxB implementation

        Boolean upperOnly = null;
        ConfidenceVisualType.ConfidenceBounds b = convt.getBounds();
        if (ConfidenceVisualType.ConfidenceBounds.LOWER.equals(b)) upperOnly = Boolean.FALSE;
        else if (ConfidenceVisualType.ConfidenceBounds.UPPER.equals(b)) upperOnly = Boolean.TRUE;

        int n = convt.getN();

        double invStudT = StudentTValues.invStudT(convt.getConfidence(), n, !ConfidenceVisualType.ConfidenceBounds.BOTH.equals(b));
        double sdXY = Math.sqrt(convt.getSumSqrResidual() / (n - 2));
        double slope = convt.getSlope();
        double intercept = convt.getIntercept();
        double xMean = convt.getXMean();
        double xVariance = convt.getXVariance();
        return new TrumpetDataProvider(slope, intercept, n, xMean, xVariance, sdXY, invStudT, upperOnly);
    }

    /**
     * lightweight dataset, 1 series, 1 value (0,0), unchangable
     */
    private static class DummyData extends AbstractXYDataset {
        public int getSeriesCount() {
            return 1;
        }

        @Override
        public Comparable getSeriesKey(int i) {
            return null;
        }

        public String getSeriesName(int series) {
            return null;
        }

        public int getItemCount(int series) {
            return 1;
        }

        public Number getX(int series, int item) {
            return new Double(0);
        }

        public Number getY(int series, int item) {
            return new Double(0);
        }
    }

    /**
     * Marker to show a Drawable: so we have more influence where (z-layer) it is painted,
     * <b>Note: if you want to use this</b> the renderer has to be ustomized like this:
     * <pre>
     * new ...StandardXYItemRenderer(style){
     *    public void drawDomainMarker(Graphics2D g2, XYPlot plot, ValueAxis domainAxis, Marker marker, Rectangle2D dataArea) {
     *       if(marker instanceof DrawableMarker)
     *          ((DrawableMarker)marker).draw(g2,plot,dataArea,domainAxis);
     *       else
     *          super.drawDomainMarker(g2, plot, domainAxis, marker, dataArea);
     * </pre>
     */
    private static class DrawableMarker extends Marker {
        private final JFDrawable drawable;

        private DrawableMarker(JFDrawable ta) {
            this.drawable = ta;
        }

        public void draw(Graphics2D g2, XYPlot plot, Rectangle2D dataArea, ValueAxis domainAxis) {
            drawable.draw(g2, plot, dataArea);
        }
    }

    /**
     * Annotation to show a Drawable: so we have more influence where (z-layer) it is painted
     */
    private static class DrawableAnnotation implements XYAnnotation {
        private final JFDrawable drawable;

        private DrawableAnnotation(JFDrawable ta) {
            this.drawable = ta;
        }

        public void draw(Graphics2D g2, XYPlot plot, Rectangle2D dataArea, ValueAxis domainAxis, ValueAxis rangeAxis, int rendererIndex, PlotRenderingInfo info) {
            drawable.draw(g2, plot, dataArea);
        }

        @Override
        public void addChangeListener(AnnotationChangeListener annotationChangeListener) {

        }

        @Override
        public void removeChangeListener(AnnotationChangeListener annotationChangeListener) {

        }
    }

    /**
     * Renderer to show a Drawable: so we have more influence where (z-layer) it is painted
     */
    private static class DrawableRenderer extends AbstractXYItemRenderer {
        private final JFDrawable drawer;

        private DrawableRenderer(JFDrawable ta) {
            this.drawer = ta;
        }

        public void drawItem(Graphics2D g2, XYItemRendererState state, Rectangle2D dataArea, PlotRenderingInfo info, XYPlot plot, ValueAxis domainAxis, ValueAxis rangeAxis, XYDataset dataset, int series, int item, CrosshairState crosshairState, int pass) {
            drawer.draw(g2, getPlot(), dataArea); // TODO 10-2008 use plot but getPlot()?
        }

        public Range findDomainBounds(XYDataset dataset) {
            return null;
        }// avoid autorange

        public Range findRangeBounds(XYDataset dataset) {
            return null;
        }// avoid autorange

        public LegendItem getLegendItem(int datasetIndex, int series) {
            return null;
        }// no show in legend
    }// --------------------------------------------------------------------------------

    /**
     * A container to have a style with a XYSeries
     */

    private final static class StyledXYSeriesCollection extends XYSeriesCollection {
        private J2DStyle.Dataset style;

        public StyledXYSeriesCollection(XYSeries series, J2DStyle.Dataset style) {
            super(series);
            this.style = style;
        }

        private XYItemRenderer getDataRenderer() {
            StandardXYItemRenderer dataRenderer = new MyStandardXYItemRenderer(style);
            dataRenderer.setDrawSeriesLineAsPath(true);
            return dataRenderer;
        }

/* Das war eine Funktionierende Idee, kann man so in der Art Recyceln.
 * Im Moment holt sich aber der Renderer den Item einmal und holt sich die Fehler selbst.
 * wenn man ds hier unten verwenden will, z.B. um bargraphen zu machen leite man von
 * XYBarDataset ab oder implementiere IntervalXYDataset
 * 
  
 		// use NaN_DUMMY (only internally!) so we can call methods on it 
		private XYDataItemErrs NaN_DUMMY = new XYDataItemErrs(0d,0d,Double.NaN,Double.NaN,Double.NaN,Double.NaN);
		private XYDataItemErrs getXYDataItemErrs(int series, int item) {
			XYDataItem xy = getSeries(series).getDataItem(item);
			return (xy instanceof XYDataItemErrs)? (XYDataItemErrs)xy : NaN_DUMMY;
			}
		public double getStartXValue(int series, int item) {
			return getXYDataItemErrs(series, item).getXErrDown();
		}
		public double getEndXValue(int series, int item) {
			return getXYDataItemErrs(series, item).getXErrUp();
		}
		public double getStartYValue(int series, int item) {
			return getXYDataItemErrs(series, item).getYErrDown();
		}
		public double getEndYValue(int series, int item) {
			return getXYDataItemErrs(series, item).getYErrUp();
		}
*/
// mal gucken, original und besser, Fuck ist jfreechart scheisse.		
//	    public double getEndYValue(int series, int item) {
//	        double result = Double.NaN;
//	        Number y = getEndY(series, item);
//	        if (y != null) {
//	            result = y.doubleValue();   
//	        }
//	        return result;   
//	    }
//	    public double getEndYValue(int series, int item) {
//			return valOrNaN(getEndY(series, item));   
//	    }
//		private double valOrNaN(Number y) {
//			return (y == null)? Double.NaN : y.doubleValue();
//		}

    }

// Das hier war die version bis vor 10-2008 welche jahrlang verwendet wurde	
//	private final static class StyledXYSeriesCollection extends XYSeriesCollection{
//		private J2DStyle.Dataset style;
//		public StyledXYSeriesCollection(XYSeries series, J2DStyle.Dataset style) {
//			super(series);
//			this.style= style;
//			}
//		private XYItemRenderer getDataRenderer() {
//			StandardXYItemRenderer dataRenderer = new MyStandardXYItemRenderer(style);
//			dataRenderer.setDrawSeriesLineAsPath(true);
//			return dataRenderer;
//			}
//		}
//	

    /**
     * create jFreeChart Axis from definition
     *
     * @param axis
     * @param isX
     * @return
     */
    private ValueAxis createAxis(AxisType axis, boolean isX) {
        boolean LOG10Axis = AxisType.AxisScaleType.LOG10.equals(axis.getScaleType());  // "linear"/"log10"

        NumberAxis a = LOG10Axis ? new LogarithmicAxis(axis.getTitle()) : new NumberAxis(axis.getTitle());
        if (a instanceof LogarithmicAxis) {
            LogarithmicAxis la = ((LogarithmicAxis) a);
            boolean fmt1000 = (axis.getUpper() < 10000d) && (axis.getLower() >= 0.001d); // 9999 will be ok
            la.setLog10TickLabelsFlag(false); // true ~ 10^3, else 1000
            la.setExpTickLabelsFlag(!fmt1000); // true ~ 1e3, else see line above 'setLog10TickLabelsFlag'
		
		/*
		 * since we check the errbar values (infinity) we set this flag to false
		 * since the axis looks and behaves much better
		 */
            la.setAllowNegativesFlag(!true);// true or neg/0 values (even in errorbars) force probs (exception in JRE1.3; wrong paints in JRE1.4)

        }
        a.setAutoRange(false);
        a.setLowerBound(axis.getLower());
        a.setUpperBound(axis.getUpper());
        Double ticks = axis.getTickWidth();

        if (ticks != null)
            a.setTickUnit(new NumberTickUnit(ticks.doubleValue()), true, true);

        if (true) { // TICKS_INSIDE_PLOT
            a.setTickMarkInsideLength(4);
            a.setTickMarkOutsideLength(0);
        }

        // if numeric: show zero always. We have autoRange disabled!
//		if(/*!graphic.getXAxisType().equals("date")*/ true) plot.addVisibleValue(-0.00000001, true);
//		a.setAutoRangeIncludesZero(false);
	
	/* save for later when we use date axis
	if(graphic.getXAxisType().equals("date")){
		domainAxis = new DateAxis(graphic.getXAxisText());
		((DateAxis)domainAxis).setDateFormatOverride(new SimpleDateFormat("yyyy-MM-dd"));
		}
	else*/
        return a;
    }

    private static double nanIfNull(Double d) {
        return (d == null) ? Double.NaN : d.doubleValue();
    }

    // ################# Dummy for dummy legends ####################################
    private static Color[] cols = new Color[]{Color.red, Color.blue, Color.magenta, Color.cyan, Color.green};

    private static Color fakeLegendColor(String legendId) {
        char c = (legendId == null || legendId.length() == 0) ? '0' : legendId.charAt(legendId.length() - 1);
        int i = c - '0';
        while (i < 0) i += cols.length * 1000;//enshure >0
        return cols[i % cols.length];
    }

    private void debugShowLegendIcon(final LegendPainter lp) {
        Icon ic = new Icon() {
            public void paintIcon(Component c, Graphics g, int x, int y) {
                Graphics2D g2 = (Graphics2D) g;
                g2.setRenderingHint(RenderingHints.KEY_ANTIALIASING, RenderingHints.VALUE_ANTIALIAS_ON);
                g2.setRenderingHint(RenderingHints.KEY_STROKE_CONTROL, RenderingHints.VALUE_STROKE_PURE);
                lp.draw(g2, new Dimension(10, 10));
            }

            public int getIconWidth() {
                return 100;
            }

            public int getIconHeight() {
                return 100;
            }
        };
        JOptionPane.showMessageDialog(null, new Object[]{ic});
    }


}
