package com.uptodata.isr.graphic.statistic;

import cern.jet.stat.Probability;

/**
 * get values for Student-T distribution.
 *
 * @author PeterBuettner.de
 */
public class StudentTValues {

    /**
     * @param alpha =1- confidence
     * @param size
     * @return corresponds to usual Student t-distribution lookup table for
     * <tt>t<sub>alpha[size]</sub></tt>
     */
    public static double invStudT(double alpha, int size) {
        return Probability.studentTInverse(alpha, size);
    }

    /**
     * Note: this is special
     *
     * @param confidence  'alpha =1- confidence',  we use (singleSided? 2*alpha :
     *                    alpha)
     * @param n           number of datapoints, we subtract 2 for calculating degrees of
     *                    freedom
     * @param singleSided see <i>alpha</i>
     * @return corresponds to usual Student t-distribution lookup table see
     * {@link #invStudT(double, int)}
     */
    public static double invStudT(double confidence, int n, boolean singleSided) {
        if (n < 3) throw new IllegalArgumentException("n, number of datapoints needs to be >=3.");
        double alpha = 1 - confidence;
        return invStudT(singleSided ? 2 * alpha : alpha, n - 2);
        // TODO check this invStudT
        /* Der Wert P in t(P,n-2) wird anders bestimmt: einseitig P=1-2*Intervall
		 * Beispiel: 5% Intervall
		 * Einseitig: P=0,9 ; Zweiseitg: P=0,95
		 */


    }


}
