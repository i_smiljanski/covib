package com.uptodata.isr.graphic.job;

import com.uptodata.isr.batic.Styler;
import com.uptodata.isr.graphic.chartVisual.ChartVisual;
import com.uptodata.isr.graphic.imageFile.DataFormatException;
import com.uptodata.isr.graphic.imageFile.Drawable;
import com.uptodata.isr.graphic.imageFile.ImageFileDesc;
import com.uptodata.isr.graphic.imageFile.ImageFileGenerator;
import com.uptodata.isr.graphic.jaxbWrap.JBSource;
import com.uptodata.isr.jaxb.all.Chart;
import com.uptodata.isr.jaxb.all.Legend;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;

import javax.xml.bind.JAXBException;
import javax.xml.bind.Unmarshaller;
import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import java.awt.*;
import java.io.*;
import java.net.MalformedURLException;
import java.util.*;
import java.util.List;
import java.util.zip.ZipEntry;
import java.util.zip.ZipOutputStream;

/**
 * creates lots of images (charts and its legends) defined in a xml file that
 * contains all infos (xml, style, files to create) generates emf,svg,png and
 * maybe much more.
 * <p>
 * doesnt stop on error, tries as hard as possible, but on at least one err
 * returns !=0 as exit code
 * <p>
 * <p>
 * timing (athlon 1700XP w2k jre1.3, emf, 1 dataset per graphic)
 * <p>
 * <pre>
 * 1: 2.1s
 * 10: 2.6s
 * 100: 5.7s
 * 1000: 21.2s
 * </pre>
 *
 * @author PeterBuettner.de
 */

public class JobOrg {

    private Element container;

    private Element info;

    private Element resources;

    private Element tasks;

    private File baseOutPath;

    /**
     * if not null write entries here
     */
    private ZipOutputStream outZip;

    private Map resStyleMap;

    private Unmarshaller unmarshaller;

    private static final int EXIT_NO_ERR = 0;

    private static final int EXIT_ANY_ERR = 255;

    private static final int EXIT_AT_LEAST_ONE_ERR = 254;

    /**
     * becomes true if any graphic/legend creation raised an exception
     */
    private boolean anyError;

    public static void main(String[] args) throws Exception {
        DocumentBuilder dbf = DocumentBuilderFactory.newInstance().newDocumentBuilder();
        File f = new File(args[0]);
        // File f = new File("R:\\upToData\\java\\Chart13JF1JaxB\\draftJob.xml");
        Element jobNode = dbf.parse(f.toURI().toURL().toExternalForm()).getDocumentElement();
        long t0 = System.currentTimeMillis();

        JobOrg job = null;
        try {
            job = new JobOrg(jobNode);
        } catch (Exception e) {
            e.printStackTrace();
            System.exit(EXIT_ANY_ERR);
        }
        System.out.println("Needed: " + (System.currentTimeMillis() - t0) + "ms");
        System.exit((job == null || job.anyError) ? EXIT_AT_LEAST_ONE_ERR : EXIT_NO_ERR);
    }

    public JobOrg(Node node) throws DataFormatException, JAXBException, IOException {
        init(node);
        execute(getChildElements(tasks));
    }

    private void init(Node node) throws DataFormatException, JAXBException, IOException {
        checkName(node, "container");
        container = (Element) node;
        checkAttribValue(container, "version", "1.0");

        // ... main sub containers .......................................
        info = getSingleElem(node, "info", true);
        resources = getSingleElem(node, "resources", false);
        tasks = getSingleElem(node, "tasks", true);
        // ..........................................
        baseOutPath = createBaseOutPath(info);

        // experimental, ok, works. do later: abstract to _any_ container
        Element out = getSingleElem(info, "out", false);
        if (out != null && "zip".equals(out.getAttribute("type"))) {
            String outName = out.getAttribute("outName");
            File z = new File(outName);
            z = z.isAbsolute() ? z : new File(baseOutPath, outName);
            if (z.exists())
                throw new IllegalArgumentException("Output ZipFile '" + z.getAbsolutePath()
                        + "' already exists.");
            z.getParentFile().mkdirs();// enshure dir exists
            outZip = new ZipOutputStream(new FileOutputStream(z));
            outZip.setLevel(1);
        }

        resStyleMap = createMapByName(resources, "resource", "style", "name");

        unmarshaller = JBSource.getValidatingUnmarshaller();

        NodeList tasksNL = getChildElements(tasks);
        // PB: Anforderung von MS/MCD: keine aktion + kein Fehler bei 0 Tasks
        // 27.6.2005
        // if(tasksNL.getLength()==0) throw new
        // DataFormatException("Empty <tasks> list.");
    }

    private void execute(NodeList tasksNL) throws DataFormatException, IOException {
        try {
            for (int i = 0; i < tasksNL.getLength(); i++)
                try {
                    doOneTask((Element) tasksNL.item(i));
                } catch (Exception e) {
                    e.printStackTrace();
                    anyError = true;
                }
        } finally {
            if (outZip != null) outZip.close();
        }
    }

    private void doOneTask(Element task) throws DataFormatException, IOException {
        checkAttribValue(task, "type", "chartImage");
        Element data = getSingleElem(task, "data", true);
        Element images = getSingleElem(task, "images", true);
        Element chartNode = getSingleElem(data, "chart", true);
        String taskId = task.getAttribute("id");

        NodeList imagesNL = getChildElements(images);
        if (imagesNL.getLength() == 0)
            throw new DataFormatException("Empty <images> list in task with id: '" + taskId + "'.");

        // ... now the pre-conditions are mostly checked ...........

        Styler styler = null;
        Chart chart = null;
        // ... build stylesheets .....................
        try {
            styler = createStyler(data, resStyleMap);
        } catch (MalformedURLException e) {
            throw new DataFormatException(
                    "Realy unexpected exception in style creation of task with id: '" + taskId + "'.",
                    e);
        }

        try {
            chart = (Chart) unmarshaller.unmarshal(chartNode);
        } catch (JAXBException e) {
            throw new DataFormatException("Could not unmarshall graphic xml in task with id: '" + taskId
                    + "'.", e);
        }

        // ... here is our datasource that we will paint .....................
        ChartVisual chartVisual = new ChartVisual(styler, chart);

        for (int i = 0; i < imagesNL.getLength(); i++) {
            try {
                doOneImage(taskId, i, chartVisual, (Element) imagesNL.item(i));
            } catch (Exception e) {
                e.printStackTrace();
                anyError = true;
            }
        }
    }

    /**
     * produce and write one image, on exception nothing is written, if the file
     * could not be written it maybe broken.
     *
     * @param taskId      only for errorlog
     * @param imgNr       only for errorlog
     * @param chartVisual input data
     * @param image       element that defines the image
     * @throws DataFormatException
     * @throws java.io.IOException
     */
    private void doOneImage(String taskId, int imgNr, final ChartVisual chartVisual, Element image)
            throws DataFormatException, IOException {
        // target info, they throw exceptions if missing
        String outName = getAttribValue(image, "outName");
        ImageFileDesc imageFileDesc = new ImageFileDesc(getSingleElem(image, "imageFileDesc", true));
        NodeList toDo = getChildElements(image, new String[]{"chart", "legend"});
        if (toDo.getLength() != 1)
            throw new DataFormatException(
                    "More than one <graphic> or <legend> in an image of task with id: '" + taskId
                            + "', image node nr: " + imgNr + ".");

        Element outType = (Element) toDo.item(0);

	/*
     * as an intermediate solution we buffer the data, on error don't write, so
	 * no zombie data is produced
	 */
        ByteArrayOutputStream buffer = new ByteArrayOutputStream(10000);
        if ("chart".equals(outType.getNodeName())) {
            Dimension size = chartVisual.getSize();
            Drawable drawable = new Drawable() {
                public void draw(Graphics2D g2, Dimension size) {
                    chartVisual.draw(g2, size);
                }
            };
            ImageFileGenerator.writeImage(buffer, drawable, size, imageFileDesc);
        } else if ("legend".equals(outType.getNodeName())) {
            Legend legend = null;
            try {
                legend = (Legend) unmarshaller.unmarshal(outType);
            } catch (JAXBException e) {
                throw new DataFormatException("Could not unmarshall legend xml in task with id: '"
                        + taskId + "', image node nr: " + imgNr + ".", e);
            }
            final String id = legend.getLegendId();
            Dimension size = new Dimension(legend.getWidth(), legend.getHeight()); // TODO
            // legend
            // needs
            // double
            // height/width?!
            Drawable drawable = new Drawable() {
                public void draw(Graphics2D g2, Dimension size) {
                    chartVisual.drawSubImage(id, g2, size);
                }
            };
            ImageFileGenerator.writeImage(buffer, drawable, size, imageFileDesc);

        } else throw new DataFormatException("Images to draw :'" + outType.getNodeName()
                + "' not supported, only <graphic>/<legend>.");

        OutputStream os = resolveOutput(outName);
        try {
            os.write(buffer.toByteArray());
        } finally {
            os.close();
        }
    }

    /**
     * gets the stream where we should write to, currently uses only File, throws
     * exception if resolved file already exists, all needed path are created if
     * they doesn't exist.
     *
     * @param outName relative or absolute
     * @return stream to write to
     * @throws java.io.IOException
     */
    private OutputStream resolveOutput(String outName) throws IOException {
        if (outZip != null) {
            outZip.putNextEntry(new ZipEntry(outName));

            FilterOutputStream fos = new FilterOutputStream(outZip) {
                public void close() throws IOException {
                    flush();
                    outZip.closeEntry();
                }
            };
            return fos;
        }

        File out = new File(outName);
        File f = out.isAbsolute() ? out : new File(baseOutPath, outName);
        if (f.exists())
            throw new IllegalArgumentException("File '" + f.getAbsolutePath() + "' already exists.");
        f.getParentFile().mkdirs();// enshure dir exists
        return new FileOutputStream(f);
    }

    /**
     * create a Sytler for the chartVisual
     *
     * @param data        the &lt;data&gt; node of the task
     * @param resStyleMap we lookup styles here
     * @return never null
     * @throws java.net.MalformedURLException
     * @throws DataFormatException
     */
    private static Styler createStyler(Element data, Map resStyleMap) throws MalformedURLException,
            DataFormatException {
        List styleList = new ArrayList();
        Element styles = getSingleElem(data, "styleSheets", false);
        if (styles != null) collectStyles(styleList, styles, resStyleMap);
        return new Styler(styleList);
    }

    /**
     * collects styles, resolves links into a resource map, adds styles as String
     * into the styleList
     *
     * @param styleList
     * @param styles      childs of type 'style' and 'link' are visited
     * @param resStyleMap
     * @throws DataFormatException
     */
    private static void collectStyles(List styleList, Element styles, Map resStyleMap)
            throws DataFormatException {
        NodeList styleNL = getChildElements(styles);
        for (int i = 0; i < styleNL.getLength(); i++) {
            Element item = (Element) styleNL.item(i);
            String nodeName = item.getNodeName();
            if ("link".equals(nodeName))
                styleList.add(getElementTextContent(getResource(resStyleMap, item)));
            else if ("style".equals(nodeName))
                styleList.add(getElementTextContent(item));
            else throw new DataFormatException("Elements <" + nodeName
                        + "> not allowed in <styleSheets>");
        }
    }

    /**
     * lookup a resource in the map
     *
     * @param map  keys: String (href w/o '#'), values: Elements
     * @param link we lookup 'href' attribute
     * @return the element
     * @throws DataFormatException if href not found, external ref or resource not found
     */
    private static Element getResource(Map map, Element link) throws DataFormatException {
        String href = getAttribValue(link, "href");
        if (href.charAt(0) != '#')
            throw new DataFormatException("Only internal href to resources allowed, href='" + href
                    + "'.");
        href = href.substring(1);// strip '#'
        Element result = (Element) map.get(href);
        if (result == null) throw new DataFormatException("Resources not found, href='" + href + "'.");
        return result;
    }

    /**
     * get Elements with &lt;elemName&gt; that are childs of node and have
     * typeAttrbute $type, map them by their '$name' attribute, they <b>need </b> to
     * have a $name attribute
     *
     * @param parent      we find childs of this one
     * @param elemName    name of the child element, e.g. 'resource'
     * @param type        type Attib to match, e.g. 'style'
     * @param mapByAttrib attrib by which we map
     * @return never null, mabe empty
     * @throws DataFormatException
     */
    private static Map createMapByName(Element parent, String elemName, String type, String mapByAttrib)
            throws DataFormatException {
        NodeList childs = getChildElements(parent, elemName);
        Map map = new HashMap(childs.getLength());

        for (int i = 0; i < childs.getLength(); i++) {
            Element item = (Element) childs.item(i);
            if (!type.equals(item.getAttribute("type"))) continue;

            String id = item.getAttribute(mapByAttrib);
            if (id == null || id.length() == 0)
                throw new DataFormatException("Element <" + elemName + ">, index:" + i + "  in <"
                        + parent.getNodeName() + "> has no '" + mapByAttrib + "' attribute.");
            map.put(id, item);
        }
        return map;
    }

    /**
     * resolve baseOutPath from &lt;info &gt; if absent use working dir
     *
     * @param infoNode
     * @return never null
     * @throws DataFormatException
     */
    private static File createBaseOutPath(Element infoNode) throws DataFormatException {
        Element base = getSingleElem(infoNode, "baseOutPath", false);
        if (base == null) return new File(".");
        String bp = base.getAttribute("href");
        if (bp == null || bp.length() == 0) return new File(".");

        return new File(bp).getAbsoluteFile();
    }

    /**
     * checks if the node has the specified name
     *
     * @param node
     * @param name
     * @throws DataFormatException
     */
    private static void checkName(Node node, String name) throws DataFormatException {
        if (!node.getNodeName().equals(name))
            throw new DataFormatException("Node is no <" + name + ">, is : " + node.getNodeName());
    }

    /**
     * checks if the node has the specified attrib with needed value
     *
     * @param node
     * @param attribName
     * @param attribValue
     * @throws DataFormatException
     */
    private static void checkAttribValue(Element node, String attribName, String attribValue)
            throws DataFormatException {
        String attribute = node.getAttribute(attribName);
        if (attribute == null || !attribValue.equals(attribute))
            throw new DataFormatException("<" + node.getNodeName() + "> has wrong '" + attribName
                    + "' Attrib, is: '" + attribute + "' needed: '" + attribValue + "'.");
    }

    /**
     * get the specified attrib, throw exception if absent/empty
     *
     * @param node
     * @param attribName
     * @return value, never null never empty
     * @throws DataFormatException
     */
    private static String getAttribValue(Element node, String attribName) throws DataFormatException {
        String attribute = node.getAttribute(attribName);
        if (attribute == null || attribute.length() == 0)
            throw new DataFormatException("<" + node.getNodeName() + "> has empty'" + attribName
                    + "' Attrib.");
        return attribute;
    }

    /**
     * text content of the element (direct childs only, text + cdata)
     *
     * @param node
     * @return the text, never null, mabe empty
     * @throws DataFormatException
     */
    private static String getElementTextContent(Element node) throws DataFormatException {
        StringBuffer sb = new StringBuffer();
        NodeList nl = node.getChildNodes();
        for (int i = 0; i < nl.getLength(); i++) {
            short nodeType = nl.item(i).getNodeType();
            if (nodeType == Node.TEXT_NODE || nodeType == Node.CDATA_SECTION_NODE)
                sb.append(nl.item(i).getNodeValue());
        }
        return sb.toString();
    }

    /**
     * get childs, elements, the one with name and check if there is only one in,
     * may return null if there is no such node
     *
     * @param parent
     * @param name
     * @param isNeeded if needed but absent throw an exception
     * @return null if not found (if isNeeded we throw an exception)
     * @throws DataFormatException
     */
    private static Element getSingleElem(Node parent, String name, boolean isNeeded)
            throws DataFormatException {
        NodeList nl = getChildElements(parent, name);
        if (nl.getLength() == 0) {
            if (isNeeded) throw new DataFormatException("No child of <" + name + "> found.");
            return null;
        }
        if (nl.getLength() > 1)
            throw new DataFormatException("More than one child of <" + name + ">, found : "
                    + nl.getLength() + ".");
        return (Element) nl.item(0);
    }

    /**
     * direct element childs with name as name, sorted as they occure in node
     *
     * @param node
     * @param name
     * @return never null, maybe empty list
     */
    private static NodeList getChildElements(Node node, String name) {
        return getChildElements(node, Collections.singletonMap(name, null).keySet());
    }

    /**
     * direct element childs with name in names, sorted as they occure in node
     *
     * @param node
     * @param names
     * @return never null, maybe empty list
     */
    private static NodeList getChildElements(Node node, String[] names) {
        return getChildElements(node, new HashSet(Arrays.asList(names)));
    }

    /**
     * direct element childs with name in names, sorted as they occure in node
     *
     * @param node
     * @param names
     * @return never null, maybe empty list
     */
    private static NodeList getChildElements(Node node, Set names) {
        NodeList c = node.getChildNodes();
        List list = new ArrayList();
        for (int i = 0; i < c.getLength(); i++) {
            Node item = c.item(i);
            if (item.getNodeType() == Node.ELEMENT_NODE && names.contains(item.getNodeName()))
                list.add(item);
        }
        return new NL(list);
    }

    /**
     * direct element childs, sorted as they occure in node
     *
     * @param node
     * @return never null, maybe empty list
     */
    private static NodeList getChildElements(Node node) {
        NodeList c = node.getChildNodes();
        List list = new ArrayList(c.getLength());
        for (int i = 0; i < c.getLength(); i++)
            if (c.item(i).getNodeType() == Node.ELEMENT_NODE) list.add(c.item(i));

        return new NL(list);
    }

    private final static class NL implements NodeList {
        private final List list;

        NL(List list) {
            this.list = list;
        }

        public int getLength() {
            return list.size();
        }

        public Node item(int index) {
            return (Node) list.get(index);
        }
    }

}
