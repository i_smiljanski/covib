package com.uptodata.isr.graphic.chartVisual;

import org.jfree.chart.axis.ValueAxis;
import org.jfree.chart.plot.XYPlot;

import java.awt.*;
import java.awt.geom.Line2D;
import java.awt.geom.Rectangle2D;

/**
 * A simple line that can be placed on an {@link XYPlot}, for example
 * shows the result of a linear regression
 *
 * @author PeterBuettner.de
 */
class LinearDrawer extends AbstractDrawer {

    /**
     * slope~m y=m*x+b
     */
    private double slope;

    /**
     * intersection~b y=m*x+b
     */
    private double intercept;

    /**
     * Creates a new Drawer to be displayed in the whole area with the given parameters
     *
     * @param slope     y=slope*x+intersection
     * @param intercept y=slope*x+intersection
     * @param stroke    line stroke
     * @param paint     line color
     */
    public LinearDrawer(double slope, double intercept, Stroke stroke, Paint paint) {
        super(stroke, paint, null);// no area fill
        this.slope = slope;
        this.intercept = intercept;
    }

    /**
     * Draws
     *
     * @param g2       the graphics device.
     * @param plot     the plot.
     * @param dataArea the data area.
     */

    public void draw(Graphics2D g2, XYPlot plot, Rectangle2D dataArea) {
        ValueAxis domainAxis = plot.getDomainAxis();
        double x1 = domainAxis.getLowerBound();
        double x2 = domainAxis.getUpperBound();
        double y1 = intercept + slope * x1;
        double y2 = intercept + slope * x2;
        paintShape(g2, translate(plot, dataArea, new Line2D.Double(x1, y1, x2, y2)));
    }


}
