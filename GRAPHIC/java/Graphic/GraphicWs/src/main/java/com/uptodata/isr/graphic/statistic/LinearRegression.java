package com.uptodata.isr.graphic.statistic;

import com.uptodata.isr.server.utils.logging.log4j2.IsrServerLogger;
import com.uptodata.isr.utils.ConvertUtils;
import com.uptodata.isr.utils.exceptions.MessageStackException;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;

import javax.xml.bind.JAXBException;

/**
 * Stable Interface to calculate a regression task DOMNode.
 *
 * @author PeterBuettner.de
 */
public class LinearRegression {
    /**
     * Read task from taskNode, append it as a new child below appendHere, if
     * appendHere is null a new document is created and returned, the return
     * value is always the new &lt;result&gt; Node.
     * <p>
     * taskNode and appendHere maybe the same node, result is appended then
     * <p>
     * Note: if you create a new appendHere element in the taskNode this will be
     * <b>invalid </b>, since the additional element is not in the schema. So
     * better use a parent of this
     *
     * @param taskNode   note that we don't want the Document root but the &lt;task&gt;
     *                   node, for conviniance we work on the DocumentElement if this
     *                   is a Document, so mostly all is ok
     * @param appendHere if null then create and return a new Document, this node maybe
     *                   also from a different document! Same note as for taskNode, we
     *                   silently use the DocumentElement
     * @return the created result node
     * @throws com.uptodata.isr.utils.exceptions.MessageStackException
     */


    public Node taskToResult(Node taskNode, Node appendHere)
            throws MessageStackException {
        LinearRegApi regApi = new LinearRegApi();
        try {
            return regApi.taskToResult(taskNode, appendHere);
        } catch (JAXBException e) {
            throw new MessageStackException(e, MessageStackException.SEVERITY_CRITICAL,
                    "While processing the linear regression task node an error occured.",
                    MessageStackException.getCurrentMethod());
        }
    }

    /**
     * Same as {@link #taskToResult(org.w3c.dom.Node, org.w3c.dom.Node)} but creates a new Document.
     *
     * @param taskNode see {@link #taskToResult(org.w3c.dom.Node, org.w3c.dom.Node)}
     * @return the created result node
     * @throws com.uptodata.isr.utils.exceptions.MessageStackException
     */
    public Node taskToResult(Node taskNode) throws MessageStackException {
        return taskToResult(taskNode, null);
    }

    /**
     * Similar to {@link #taskToResult(org.w3c.dom.Node, org.w3c.dom.Node)}but recurses all sub-elements
     * of rootNode and processes each &lt;linearRegressionTask&gt; node, appends
     * the result as a new node of this current input nodes. Note that after
     * processing once the &lt;linearRegressionTask&gt; are 'invalid' since they
     * have a child node &lt;result&gt; that are not allowed by the schema.
     *
     * @param root any element, it is processed too
     * @throws com.uptodata.isr.utils.exceptions.MessageStackException
     */
    public void taskToResultAll(Element root, IsrServerLogger log) throws MessageStackException {
        log.stat("begin");
        LinearRegApi regApi = new LinearRegApi();
        if ("linearRegressionTask".equals(root.getNodeName())) {
            Node resultNode = null;
            try {
                resultNode = root.getElementsByTagName("result").item(0);
                root.removeChild(resultNode);
            } catch (Exception e) {
                log.error(e);
            }
            regApi.taskToResult(root, root, resultNode);
        }

        NodeList nl = root.getElementsByTagName("linearRegressionTask");
        for (int i = 0; i < nl.getLength(); i++) {
            Node n = nl.item(i);
//            log.debug(i + ": " + n.getNodeName());
            Node resultNode;
            resultNode = ((Element) n).getElementsByTagName("result").item(0);
            if (resultNode != null) {
                n.removeChild(resultNode);
            }
            regApi.taskToResult(n, n, resultNode);
        }
        log.stat("end");
//        } catch (JAXBException e) {
//            throw new MessageStackException(e, MessageStackException.SEVERITY_CRITICAL,
//                    "While processing the linear regression task document an error occured.",
//                    MessageStackException.getCurrentMethod());
//        }
    }

    /**
     * similar to {@link #taskToResult(org.w3c.dom.Node)}, but maybe used as a xslt
     * extension (e.g. oracle xslt), <b>only the first node is calculated! </b>
     *
     * @param singleElemList exactly one task here, or an exception is thrown
     * @return the calculated result wrapped in a nodelist, so first element is
     * the result
     * @throws com.uptodata.isr.utils.exceptions.MessageStackException
     */
    public NodeList taskToResultXslt(NodeList singleElemList)
            throws MessageStackException {
        if (singleElemList.getLength() != 1)
            throw new MessageStackException(
                    "Input 'singleElemList' has to contain exactly one element.");

        final Node n = taskToResult(singleElemList.item(0), null);
        return new NodeList() {
            public int getLength() {
                return 1;
            }

            public Node item(int i) {
                return n;
            }
        };
    }

    public static void main(String[] args) {
        try {
            byte[] xml = ConvertUtils.fileToBytes(System.getProperty("user.dir") + "\\tests\\calc1.xml");
//            byte[] xml = ConvertUtils.fileToBytes(System.getProperty("user.dir") + "\\tests\\ISR$WT$CRIB$CHART$CALC.xml");
            Document doc = ConvertUtils.convertByteToDocument(xml);
            LinearRegression regression = new LinearRegression();
//            regression.taskToResultAll(doc.getDocumentElement());
//            byte[] bytes = ConvertUtils.convertDocumentToByte(doc);
//            ConvertUtils.bytesToFile(bytes, System.getProperty("user.dir") + "\\tests\\output.xml");
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}
