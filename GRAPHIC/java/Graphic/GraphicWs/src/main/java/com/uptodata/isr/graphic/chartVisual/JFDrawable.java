package com.uptodata.isr.graphic.chartVisual;

import org.jfree.chart.plot.XYPlot;

import java.awt.*;
import java.awt.geom.Rectangle2D;

/**
 * Something that could be drawn into a JFreeChart XYPlot
 *
 * @author PeterBuettner.de
 */
interface JFDrawable {
    /**
     * Draws.
     *
     * @param g2       the graphics device.
     * @param plot     the plot.
     * @param dataArea the data area is in J2D coords
     */
    public void draw(Graphics2D g2, XYPlot plot, Rectangle2D dataArea);
}