package com.uptodata.isr.graphic.jaxbWrap;

import com.uptodata.isr.jaxb.all.DatasetType;
import com.uptodata.isr.jaxb.all.DatasetType.Value;
import com.uptodata.isr.jaxb.all.ObjectFactory;
import com.uptodata.isr.jaxb.all.TransformationsType;
import com.uptodata.isr.jaxb.all.TransformationsType.AddLine;
import com.uptodata.isr.jaxb.all.TransformationsType.ToResidual;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Iterator;
import java.util.List;

/**
 * Helper that reads in an unmarshalled DatasetType, delivers data as double[], transformations
 * are already applyed.
 * <p>
 * PEB 10-2008 add Errorbars:<br>
 * {@linkplain #getValues()} has the errorbars if they were in the inputdata, if not
 * all but x,y in thoses values are null.
 *
 * @author PeterBuettner.de
 */
public class DatasetWrap {
    private final double[] x;
    private final double[] y;

    private final List valueClones;

    public double[] getX() {
        return x;
    }

    public double[] getY() {
        return y;
    }

    /**
     * transformations are already applied afterwards
     *
     * @param datasetType input not changed and is not references here afterwards
     */
    public DatasetWrap(DatasetType datasetType) {
        List values = datasetType.getValues();
        if (values == null) values = Collections.EMPTY_LIST;// create dummy
        x = new double[values.size()];
        y = new double[values.size()];
        int i = 0;
        for (Iterator it = values.iterator(); it.hasNext(); ) {
            Value v = (Value) it.next();
            x[i] = v.getX();
            y[i] = v.getY();
            i++;
        }
        TransformationsType t = datasetType.getTransformations();
        if (t != null) applyTransformations(t.getItems());


        // PEB 10-2008  add Errorbars
        valueClones = cloneValues(values, x, y);
    }

    /**
     * @param values
     * @param xTranslated
     * @param yTranslated
     * @return clones
     */
    private static List cloneValues(List values, double[] xTranslated, double[] yTranslated) {
        ObjectFactory of = new ObjectFactory(); // is cheap
        List clones = new ArrayList(values.size());
        int i = 0;
//	try {
        for (Object value : values) {
            Value v = (Value) value;
            // use the translated values
            //x[i] = v.getX();
            //y[i] = v.getY();
            Value clone = of.createDatasetTypeValue();
            clone.setX(xTranslated[i]);
            clone.setY(yTranslated[i]);
            clone.setXErrMinus(v.getXErrMinus());
            clone.setXErrPlus(v.getXErrPlus());
            clone.setYErrMinus(v.getYErrMinus());
            clone.setYErrPlus(v.getYErrPlus());

            clones.add(clone);
            i++;
        }
//		} catch (JAXBException e) { // TODO in Jre1.4 use a wrapped error
//			throw new Error("A JAXBException can't occure here by just calling a constructor. OrigMsg:" + e.getMessage());
//		}
        return clones;
    }

    /**
     * Apply all transformations on the data of this instance.
     * Call this only once!
     *
     * @param trafos
     */
    private void applyTransformations(List trafos) {
        if (trafos == null) return;
        for (Iterator i = trafos.iterator(); i.hasNext(); ) {
            Object t = i.next();
            if (t instanceof AddLine) applyAddLine((AddLine) t);
            else if (t instanceof ToResidual) applyToResidual((ToResidual) t);
            else
                throw new IllegalArgumentException("Can not understand data transformation:" + t.getClass());
        }
    }

    private void applyAddLine(AddLine addLine) {
        double inter = addLine.getIntercept();
        double slope = addLine.getSlope();
        int len = x.length;
        for (int i = 0; i < len; i++)
            y[i] = y[i] + inter + slope * x[i];
    }

    private void applyToResidual(ToResidual toResidual) {
        double inter = toResidual.getIntercept();
        double slope = toResidual.getSlope();
        int len = x.length;
        for (int i = 0; i < len; i++)
            y[i] = (inter + slope * x[i]) - y[i];
    }


    /**
     * clones of the original values, but transformations already applied
     * PEB 10-2008  add Errorbars
     *
     * @return list with Value, never null
     */
    public List getValues() {
        return valueClones;
    }
}




