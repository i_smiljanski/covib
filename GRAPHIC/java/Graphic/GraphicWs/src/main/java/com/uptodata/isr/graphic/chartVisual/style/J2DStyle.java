package com.uptodata.isr.graphic.chartVisual.style;

import java.awt.*;
import java.awt.geom.AffineTransform;

/**
 * Ugly stuff to have containers for style
 *
 * @author PeterBuettner.de
 */
public abstract class J2DStyle {

    private final static Paint NULL_PAINT = new Color(0, 0, 0, 0);
    private final static Stroke NULL_STROKE = new BasicStroke(0);
    private final static Line NULL_LINE = new Line(NULL_STROKE, NULL_PAINT) {
        public boolean isVisible(boolean canAlpha) {
            return false;
        }
    };

    protected J2DStyle() {
    }

    ;

    /**
     * paints the shape with the style, if 'line' only draw(shape) if fill is
     * defined also fill(shape)
     *
     * @param g2
     * @param shape
     */
    public abstract void paint(Graphics2D g2, Shape shape);

    public static class Line extends J2DStyle {
        public Line(Stroke stroke, Paint strokePaint) {
            this.stroke = stroke;
            this.strokePaint = strokePaint;
        }

        private Stroke stroke;
        private Paint strokePaint;

        public Paint getStrokePaint() {
            return strokePaint;
        }

        public Stroke getStroke() {
            return stroke;
        }

        /**
         * prepare stroke and paint, returns false if the result should be invisible (paint null or stroke invisible)
         *
         * @param g2
         * @return
         */
        public boolean prepare(Graphics2D g2) {
            if (strokePaint == null || !isStrokeVisible(stroke)) return false;

            g2.setPaint(strokePaint);
            g2.setStroke(stroke);
            return true;
        }

        public void paint(Graphics2D g2, Shape shape) {
            if (!prepare(g2)) return;
            g2.draw(shape);
        }

        /**
         * true if width>0 and Paint is visible
         *
         * @param canAlpha
         * @return if it is visible
         */
        public boolean isVisible(boolean canAlpha) {
            if (strokePaint instanceof Color)
                return strokePaint != null && ((Color) strokePaint).getAlpha() > 0 && isStrokeVisible(stroke);
            return isStrokeVisible(stroke);
        }

    }// -----------------------------------------------------------

    private static boolean isStrokeVisible(Stroke stroke) {
        if (stroke == null) return false;
        if (stroke instanceof BasicStroke)
            return ((BasicStroke) stroke).getLineWidth() > 0;
        return true;
    }

    /**
     * Has fillPaint + outline (inherited)
     */
    public static class Area extends Line {
        private Paint fillPaint;

        public Area(Stroke stroke, Paint strokePaint, Paint fillPaint) {
            super(stroke, strokePaint);
            this.fillPaint = fillPaint;
        }

        public Paint getFillPaint() {
            return fillPaint;
        }

        public void paint(Graphics2D g2, Shape shape) {
            if (fillPaint != null) {
                g2.setPaint(fillPaint);
                g2.fill(shape);
            }

            super.paint(g2, shape);
        }
    }// -----------------------------------------------------------

    /**
     * Has line and a size for the cap
     */
    public static class ErrorCap extends Line {
        private float size;

        public ErrorCap(Stroke stroke, Paint strokePaint, float size) {
            super(stroke, strokePaint);
            this.size = size;
        }

        public float getSize() {
            return size;
        }

        /**
         * since painting a dataset is more complicated, this throws an UnsupportedOperationException
         * exception
         *
         * @param g2
         * @param shape
         * @throws UnsupportedOperationException
         */
        public void paint(Graphics2D g2, Shape shape) throws UnsupportedOperationException {
            throw new UnsupportedOperationException("Dataset has to be painted by user code.");
        }
    }// -----------------------------------------------------------


    public static class Symbol extends Area {
        private String type;
        private float size;
        private Shape shape;// don't use it for painting internally, since it maybe translated outside

        /**
         * @param stroke
         * @param strokePaint
         * @param fillPaint
         * @param type        one of "none, plus, cross, box, diamond, triup, tridn, dot, hline, vline, star"
         * @param size        size in units (~pixels), 0 will have empty (null) shape and paints nothing
         */
        public Symbol(Stroke stroke, Paint strokePaint, Paint fillPaint, String type, float size) {
            super(stroke, strokePaint, fillPaint);
            this.type = type;
            this.size = size;
            if (size == 0) return;

            shape = ChartShapes.shapeByName(type);
            if (shape != null)
                shape = AffineTransform.getScaleInstance(size, size).createTransformedShape(shape);
        }

        public Symbol(Area semiStyle, String type, float size) {
            this(semiStyle.getStroke(), semiStyle.getStrokePaint(), semiStyle.getFillPaint(), type, size);
        }

        public boolean hasShape() {
            return shape != null;
        }

        /**
         * the correct sized shape of the symbol
         *
         * @return shape, maybe null
         */
        public Shape getShape() {
            return shape;
        }

        public void paint(Graphics2D g2, Shape shape) {
            if (shape != null) super.paint(g2, shape);
        }
    }// -----------------------------------------------------------

    public static class Dataset extends J2DStyle {
        private final Symbol symbol;
        private final Line line;
        private final Line errorLine;
        private final ErrorCap errorCap;

        public Dataset(Symbol symbol, Line line, Line errorLine, ErrorCap errorCap) {
            this.symbol = symbol;
            this.line = line != null ? line : NULL_LINE;
            this.errorLine = errorLine;
            this.errorCap = errorCap;
        }

        public boolean hasSymbol() {
            return symbol != null && symbol.hasShape();
        }

        public boolean hasLine() {
            return (line != null && line.isVisible(true));
        }

        public Symbol getSymbol() {
            return symbol;
        }

        public Line getLine() {
            return line;
        }

        public ErrorCap getErrorCap() {
            return errorCap;
        }

        public Line getErrorLine() {
            return errorLine;
        }

        /**
         * since painting a dataset is more complicated, this throws an UnsupportedOperationException
         * exception
         *
         * @param g2
         * @param shape
         * @throws UnsupportedOperationException
         */
        public void paint(Graphics2D g2, Shape shape) throws UnsupportedOperationException {
            throw new UnsupportedOperationException("Dataset has to be painted by user code.");
        }
    }// -----------------------------------------------------------

}
