package com.uptodata.isr.graphic.chartVisual;


import org.jfree.chart.plot.XYPlot;

import java.awt.*;
import java.awt.geom.AffineTransform;
import java.awt.geom.Rectangle2D;

/**
 * Draws areas and lines: border (color/stroke) fill-color,
 * basic class to have the details done
 * <p>
 * <p>
 * <p>
 * <p>
 * <small>
 * Something went wrong <b>before</b>, if zoomed (much in!) in the left/right border is
 * visible, but by sysout it is verified that the generalPath lies outside of
 * the visible area
 * <br>
 * <b>it's all right now</b>, the simple single AffineTransform doesn't work with
 * such big values: 2004-01-01.getTime -> 10^14, offset too, use:
 * </small>
 * <pre><small>
 * JFreeChartHelper.createTransformValueToJ2DA
 * JFreeChartHelper.createTransformedShape
 * </small></pre>
 *
 * @author PeterBuettner.de
 */
abstract class AbstractDrawer implements JFDrawable {

    /**
     * The line stroke.
     */
    protected final Stroke borderStroke;

    /**
     * The line color.
     */
    protected final Paint borderPaint;

    protected final Paint fillPaint;

    /**
     * let some values out to have them not painted
     *
     * @param borderStroke null ok
     * @param borderPaint  null ok
     * @param fillPaint    null ok
     */
    public AbstractDrawer(Stroke borderStroke, Paint borderPaint, Paint fillPaint) {
        this.borderStroke = borderStroke;
        this.borderPaint = borderPaint;
        this.fillPaint = fillPaint;
    }


    /**
     * helper: draws/fills the shape as defined by the colors/stroke, so open shapes
     * or not bordered shapes work right.
     *
     * @param g2
     * @param j2DShape
     */
    protected void paintShape(Graphics2D g2, Shape j2DShape) {
        // the freeHep exporter doesn't like nulls
        if (fillPaint != null) {
            g2.setPaint(fillPaint);
            g2.fill(j2DShape);
        }
        if (borderPaint != null && borderStroke != null) {
            g2.setPaint(borderPaint);
            g2.setStroke(borderStroke);
            g2.draw(j2DShape);
        }
    }

    /**
     * helper: expensive method to translate the (user-coords) shape into a java2D Shape,
     * it's perfectly right for one, if you need more translations use
     * <pre>
     * JFreeChartHelper.createTransformValueToJ2DA
     * JFreeChartHelper.createTransformedShape
     * </pre>
     * directly to build the matrices only once
     *
     * @param plot
     * @param dataArea
     * @param shape
     * @return
     */
    protected Shape translate(XYPlot plot, Rectangle2D dataArea, Shape shape) {
        AffineTransform[] tt = JFreeJ2D.createTransformValueToJ2DA(plot, dataArea);
        return JFreeJ2D.createTransformedShape(tt, shape);
    }

}
