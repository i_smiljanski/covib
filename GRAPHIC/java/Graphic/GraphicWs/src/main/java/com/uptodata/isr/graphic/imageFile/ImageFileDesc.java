package com.uptodata.isr.graphic.imageFile;


import org.w3c.dom.Node;

import java.util.Arrays;
import java.util.HashSet;
import java.util.Set;


/**
 * contains data that defines an image format.
 * <p>
 * For a DOM-node, currently only thats attributes are used:
 * <p>
 * <pre>
 * &lt;imageFileDesc
 * 	type="[emf|svg|png]"
 * 	colorDepth="8|24|32"
 * 	grey="[true|false]"
 * 	compression="[0..9]"
 * 	/&gt;
 * </pre>
 * <p>
 * <dl>
 * <dt>type
 * <dd>what image type will be produced
 * <ul>
 * <li>required
 * <li>values=[emf|svg|png]
 * </ul>
 * <p>
 * <p>
 * <dt>colorDepth
 * <dd>bits per pixel. <br>
 * <ul style="list-style-type:none">
 * <li> 8 and less create palette images, may have 1 bit transparency</li>
 * <li> 24 direct color images with no alpha channel</li>
 * <li> 32 same as 24 but with alpha channel</li>
 * </ul>
 * <p>
 * <ul>
 * <li>not required, defaults to 32
 * <li>values=[8|24|32]
 * <li>not for vector formats (emf,svg)
 * </ul>
 * <p>
 * <dt>grey
 * <dd>create a greyscaled (palette) image, reduces image-size
 * <ul>
 * <li>not required, defaults to false
 * <li>values=[true|false]
 * <li>used only with colorDepth &lt;=8
 * <li>not for vector formats (emf,svg)
 * </ul>
 * <p>
 * <dt>compression
 * <dd>set the compression strength, maybe not supported by a type, is
 * supported for emf,svg,png; (but png maybe always compressed, and the strength
 * will be a default one). 0=uncompressed, 1=fast, 5=medium, 9=max-but-slowest.
 * <br>
 * If missing: a defaults to: uncompressed for "emf,svg";
 * default compression (~5) for "png"
 * <p>
 * <p>
 * <ul>
 * <li>not required, default see text
 * <li>values=[0..9]
 * </ul>
 * </dl>
 *
 * @author PeterBuettner.de
 */
public class ImageFileDesc {
    /**
     * emf,svg,png
     */
    private String type;
    /**
     * 8,24,32; null means 'use default'
     */
    private Integer colorDepth;

    /**
     * grey scale or not; null means 'use default'
     */
    private Boolean grey;

    /**
     * 0..9; null means 'use default'
     */
    private Integer compression;


    private static String TAG_NAME = "imageFileDesc";

    private static final Set<String> types = new HashSet<>(Arrays.asList(new String[]{"emf", "svg", "png"}));
    private static final Set<Integer> colorDepths = new HashSet<>();

    static {
        colorDepths.add(8);
        colorDepths.add(24);
        colorDepths.add(32);
    }

    /**
     * Reads from a DOM-node, currently only thats attributes are used (see {@link com.uptodata.isr.graphic.imageFile.ImageFileDesc}),
     * node has to be named 'imageFileDesc', parameters are checked for sanity
     *
     * @param node
     * @throws DataFormatException
     */
    public ImageFileDesc(Node node) throws DataFormatException {
        init(node);
    }

    /**
     * read ImageFileDesc from a DOM Node, checks for sanity with
     * {@link #sanityCheck()}
     *
     * @param node
     * @throws DataFormatException if anything (attibutes) is inconsistent, format of numbers can't be read
     */
    private void init(Node node) throws DataFormatException {
        if (!TAG_NAME.equals(node.getNodeName()))
            throw new DataFormatException("Try to create ImageFileDesc with wrong node type:" + node.getNodeName());
        // ... type ........
        type = DomDataReader.getAttribute(node, "type");
        colorDepth = DomDataReader.getAttributeInteger(node, "colorDepth", "In " + getClass().getName());
        compression = DomDataReader.getAttributeInteger(node, "compression", "In " + getClass().getName());
        grey = DomDataReader.getAttributeBoolean(node, "grey", "In " + getClass().getName());

        sanityCheck();
    }

    /**
     * checks the current set parameters for sanity e.g.&nbsp; type not null, in
     * expected range, etc.
     *
     * @throws IllegalArgumentException if anything is inconsistent
     */
    private void sanityCheck() throws IllegalArgumentException {
        if (type == null)
            throw new IllegalArgumentException("No image type defined in node, attrib: 'type'");
        if (!types.contains(type))
            throw new IllegalArgumentException("Unknown image type defined in node, is:'" + type + "' allowed:" + types);

        if (compression != null) {
            int c = compression;
            if (c < 0 || c > 9)
                throw new IllegalArgumentException("Compression needs to be in [0..9], is:'" + c);
        }
        if (colorDepth != null && !colorDepths.contains(colorDepth))
            throw new IllegalArgumentException("colorDepth needs to be in " + colorDepths + ", is:'" + colorDepth);

        if (isVectorFormat(type) && colorDepth != null)
            throw new IllegalArgumentException("colorDepth is unused for vectorformats, " + this);

        if (isVectorFormat(type) && grey != null && grey)
            throw new IllegalArgumentException("grey is unused for vectorformats,: " + this);


    }

    private static boolean isVectorFormat(String type) {
        return "emf".equals(type) || "svg".equals(type);
    }


    /**
     * Bits per Pixel:  8,24,32;
     * see the class description: {@link com.uptodata.isr.graphic.imageFile.ImageFileDesc}
     *
     * @return null means default
     */
    public Integer getColorDepth() {
        return colorDepth;
    }

    /**
     * image should be compressed, this is the strength,
     * see the class description: {@link com.uptodata.isr.graphic.imageFile.ImageFileDesc}
     *
     * @return null means default
     */
    public Integer getCompression() {
        return compression;
    }

    /**
     * if the data shoud be compressed, conveniance if the type won't support
     * detailed strength
     *
     * @return true if compression>0
     */
    public boolean isCompressed() {
        return compression != null && compression > 0;
    }


    /**
     * image should be gray scaled,
     * see the class description: {@link com.uptodata.isr.graphic.imageFile.ImageFileDesc}
     *
     * @return null means default
     */
    public Boolean getGrey() {
        return grey;
    }

    /**
     * "png", "emf", "svg" or such,
     * see the class description: {@link com.uptodata.isr.graphic.imageFile.ImageFileDesc}
     *
     * @return never null
     */
    public String getType() {
        return type;
    }

    /**
     * proposes the extension for a filename for the produced binary, since this
     * may vary a bit by the concrete type. E.g. gzip compressed svg is named
     * *.svgz, *.emf -&gt; *.emz; but i think the readers will also accept
     * compressed *.svg files (if they are be able to read compressed files)
     *
     * @return e.g. "svg" without the dot
     */
    public String getFileExtension() {
        if (compression != null && compression != 0) {
            if ("emf".equals(type)) return "emz";
            if ("svg".equals(type)) return "svgz";
        }

        return type;
    }

    public String toString() {
        return
                "type: " + type +
                        ", colorDepth: " + colorDepth +
                        ", grey: " + grey +
                        ", compression: " + compression
                ;
    }
}
