package com.uptodata.isr.graphic.chartVisual;


import org.jfree.chart.axis.ValueAxis;
import org.jfree.chart.plot.Plot;
import org.jfree.chart.plot.PlotOrientation;
import org.jfree.chart.plot.XYPlot;
import org.jfree.data.Range;
import org.jfree.ui.RectangleEdge;

import java.awt.*;
import java.awt.geom.AffineTransform;
import java.awt.geom.Rectangle2D;

/**
 * Some Helpers to translate between java2D-JFreeChart,
 * Graphics2D-Helper transformations
 * <p>
 * TODO remove obsolete methods (some G2d stuff)
 * <p>
 * TODO PEB 10-2008 This is broken for log Axis
 *
 * @author PeterBuettner.de
 */
class JFreeJ2D {

    /**
     * /**
     * Create a transform to easy paint shapes (in value coords) into a plot:
     * <pre>
     * Shape j2DShape = transform.createTransformedShape(shapeInValueCoords);
     * g2.draw(j2DShape);
     * </pre>
     * j2DShape have coord to paint it directly into a Graphics2D,
     * <b>has problems with big values (2004-1-1 ~ 1E12)</b>
     *
     * @param plot
     * @param dataArea
     * @return
     */
    static AffineTransform[] createTransformValueToJ2DA(XYPlot plot, Rectangle2D dataArea) {
        return createTransformValueToJ2DA(plot, dataArea, plot.getDomainAxis(), plot.getRangeAxis());
    }

    /**
     * Concatenates all <code>AffineTransform</code>s, one after another, so rounding
     * errors are minimized if you use special <code>AffineTransform</code>s
     *
     * @param t
     * @param shape
     * @return
     */
    static Shape createTransformedShape(AffineTransform[] t, Shape shape) {
        for (int i = 0; i < t.length; i++)
            shape = t[i].createTransformedShape(shape);
        return shape;
    }


    /**
     * Create a transform to easy paint shapes (in value coords) into a plot:
     * <pre>
     * Shape j2DShape = transform.createTransformedShape(shapeInValueCoords);
     * g2.draw(j2DShape);
     * </pre>
     * j2DShape have coord to paint it directly into a Graphics2D,
     * <b>has problems with big values (2004-1-1 ~ 1E12)</b>
     *
     * @param plot
     * @param dataArea
     * @param domainAxis
     * @param rangeAxis
     * @return
     */
    private static AffineTransform createTransformValueToJ2D(XYPlot plot, Rectangle2D dataArea, ValueAxis domainAxis, ValueAxis rangeAxis) {
        PlotOrientation orientation = plot.getOrientation();
        RectangleEdge domainEdge = Plot.resolveDomainAxisLocation(plot.getDomainAxisLocation(), orientation);
        RectangleEdge rangeEdge = Plot.resolveRangeAxisLocation(plot.getRangeAxisLocation(), orientation);

        //compute transform matrix elements via sample points
//    double m00 = domainAxis.translateValueToJava2D(1, dataArea, domainEdge);// x-axis scale 
//    double m02 = domainAxis.translateValueToJava2D(0, dataArea, domainEdge);// x-axis translation 
//    double m11 = rangeAxis.translateValueToJava2D(1, dataArea, rangeEdge);// y-axis scale
//    double m12 = rangeAxis.translateValueToJava2D(0, dataArea, rangeEdge);  // y-axis translation

        double m00 = translateValueToJava2D(domainAxis, 1, dataArea, domainEdge);// x-axis scale
        double m02 = translateValueToJava2D(domainAxis, 0, dataArea, domainEdge);// x-axis translation
        double m11 = translateValueToJava2D(rangeAxis, 1, dataArea, rangeEdge);// y-axis scale
        double m12 = translateValueToJava2D(rangeAxis, 0, dataArea, rangeEdge);  // y-axis translation

        return new AffineTransform(m00 - m02, 0, 0, m11 - m12, m02, m12);
    }

    /**
     * Create a transform to easy paint shapes (in value coords) into a plot:
     * <pre>
     * Shape j2DShape = transform.createTransformedShape(shapeInValueCoords);
     * g2.draw(j2DShape);
     * </pre>
     * j2DShape have coord to paint it directly into a Graphics2D,
     * <b>has problems with big values (2004-1-1 ~ 1E12)</b>
     *
     * @param plot
     * @param dataArea
     * @return
     */
    private static AffineTransform createTransformValueToJ2D(XYPlot plot, Rectangle2D dataArea) {
        return createTransformValueToJ2D(plot, dataArea, plot.getDomainAxis(), plot.getRangeAxis());
    }


    /**
     * works with big values too!
     *
     * @param plot
     * @param dataArea
     * @param domainAxis
     * @param rangeAxis
     * @return
     */
    private static AffineTransform[] createTransformValueToJ2DA(XYPlot plot, Rectangle2D dataArea, ValueAxis domainAxis, ValueAxis rangeAxis) {
        PlotOrientation orientation = plot.getOrientation();
        RectangleEdge domainEdge = Plot.resolveDomainAxisLocation(plot.getDomainAxisLocation(), orientation);
        RectangleEdge rangeEdge = Plot.resolveRangeAxisLocation(plot.getRangeAxisLocation(), orientation);

        double[] tX = getTransformMatrix(dataArea, domainAxis, domainEdge);
        double[] tY = getTransformMatrix(dataArea, rangeAxis, rangeEdge);

        AffineTransform[] res = new AffineTransform[3];
        res[0] = AffineTransform.getTranslateInstance(tX[0], tY[0]);
        res[1] = AffineTransform.getScaleInstance(tX[1], tY[1]);
        res[2] = AffineTransform.getTranslateInstance(tX[2], tY[2]);

        return res;
    }

    private static double[] getTransformMatrix(Rectangle2D dataArea, ValueAxis axis, RectangleEdge edge) {
        Range range = axis.getRange();
        double axisMin = range.getLowerBound();
        double axisMax = range.getUpperBound();
        boolean invert = axis.isInverted();

        double min, max;
        if (RectangleEdge.isTopOrBottom(edge)) {
            min = dataArea.getX();
            max = dataArea.getMaxX();
        } else if (RectangleEdge.isLeftOrRight(edge)) {
            min = dataArea.getMinY();
            max = dataArea.getMaxY();
            invert = !invert;
        } else throw new IllegalStateException("Should never happen, RectangleEdge has wrong state:" + edge);

        double[] res = new double[3];
        res[0] = -axisMin;
        res[1] = (max - min) / (axisMax - axisMin) * (invert ? -1 : 1);
        res[2] = (invert ? max : min);
        return res;
    }//-----------------------------------------------------------------------


    /**
     * Translates the data value to the display coordinates (Java 2D User Space)
     * of the graphic.
     *
     * @param axis
     * @param value    the date to be plotted.
     * @param dataArea the rectangle (in Java2D space) where the data is to be plotted.
     * @param edge     the axis location.
     * @return the coordinate corresponding to the supplied data value.
     */
    private static double translateValueToJava2D(ValueAxis axis, double value, Rectangle2D dataArea, RectangleEdge edge) {
        Range range = axis.getRange();
        double axisMin = range.getLowerBound();
        double axisMax = range.getUpperBound();
        boolean invert = axis.isInverted();

        double min, max;
        if (RectangleEdge.isTopOrBottom(edge)) {
            min = dataArea.getX();
            max = dataArea.getMaxX();
        } else if (RectangleEdge.isLeftOrRight(edge)) {
            min = dataArea.getMinY();
            max = dataArea.getMaxY();
            invert = !invert;
        } else return 0.0;

        double f = (value - axisMin) / (axisMax - axisMin);
        if (invert) return max - f * (max - min);
        else return min + f * (max - min);

    }

    /**
     * Translates the data values to the display coordinates (Java 2D User Space)
     * of the graphic, translated inplace!
     *
     * @param axis
     * @param dataArea the rectangle (in Java2D space) where the data is to be plotted.
     * @param edge     axis location.
     * @param data     data to be plotted, translated inplace!
     * @param offset   into data
     * @param length   in data
     */

    private static void translateValueToJava2D(ValueAxis axis, Rectangle2D dataArea, RectangleEdge edge, double[] data, int offset, int length) {
        Range range = axis.getRange();
        double axisMin = range.getLowerBound();
        double axisMax = range.getUpperBound();
        boolean invert = axis.isInverted();

        double min, max;
        if (RectangleEdge.isTopOrBottom(edge)) {
            min = dataArea.getX();
            max = dataArea.getMaxX();
        } else if (RectangleEdge.isLeftOrRight(edge)) {
            min = dataArea.getMinY();
            max = dataArea.getMaxY();
            invert = !invert;
        } else throw new IllegalStateException("Should never happen, RectangleEdge has wrong state:" + edge);

        double dAxis = (axisMax - axisMin);
        double dArea = (max - min);

        int n = Math.min(length, data.length - offset) + offset;
        for (int i = offset; i < n; i++) {
            double f = (data[i] - axisMin) / dAxis;
            if (invert) data[i] = max - f * dArea;
            else data[i] = min + f * dArea;
        }
    }// ------------------------------------------------------------------------------------------

    /**
     * Translates the data values to the display coordinates (Java 2D User Space)
     * of the graphic, translated inplace!
     *
     * @param axis
     * @param dataArea the rectangle (in Java2D space) where the data is to be plotted.
     * @param edge     the axis location.
     * @param value    the date to be plotted, translated inplace!
     */

    private static void translateValueToJava2D(ValueAxis axis, Rectangle2D dataArea, RectangleEdge edge, double[] value) {
        translateValueToJava2D(axis, dataArea, edge, value, 0, value.length);
    }

}
