package com.uptodata.isr.graphic.chartVisual;

import com.uptodata.isr.graphic.chartVisual.style.J2DStyle;
import org.jfree.chart.axis.ValueAxis;
import org.jfree.chart.plot.CrosshairState;
import org.jfree.chart.plot.PlotOrientation;
import org.jfree.chart.plot.PlotRenderingInfo;
import org.jfree.chart.plot.XYPlot;
import org.jfree.chart.renderer.xy.StandardXYItemRenderer;
import org.jfree.chart.renderer.xy.XYItemRendererState;
import org.jfree.data.xy.XYDataItem;
import org.jfree.data.xy.XYDataset;
import org.jfree.data.xy.XYSeriesCollection;
import org.jfree.ui.RectangleEdge;
import org.jfree.util.ShapeUtilities;
import org.jfree.util.UnitType;

import java.awt.*;
import java.awt.geom.Line2D;
import java.awt.geom.Rectangle2D;


/**
 * Some parts deleted to make the changes easier, paints symbols with
 * outline, lines and symbols colored differently, 2-pass for painting the
 * lines below the symbols.
 * <p>
 * also split the <b>huge</b> draw() method into handy blocks
 *
 * @author PeterBuettner.de
 */
class MyStandardXYItemRenderer extends StandardXYItemRenderer {
    private J2DStyle.Dataset style;

    MyStandardXYItemRenderer(J2DStyle.Dataset style) {
        super();
        this.style = style;
        setPlotImages(style.hasSymbol());
        setPlotLines(style.hasLine());
    }


    public int getPassCount() {
        return 2;
    }// first lines, symbols later

    /**
     * Draws the visual representation of a single data item.
     *
     * @param g2             the graphics device.
     * @param state          the renderer state.
     * @param dataArea       the area within which the data is being drawn.
     * @param info           collects information about the drawing.
     * @param plot           the plot (can be used to obtain standard color information
     *                       etc).
     * @param domainAxis     the domain axis.
     * @param rangeAxis      the range axis.
     * @param dataset        the dataset.
     * @param series         the series index (zero-based).
     * @param item           the item index (zero-based).
     * @param crosshairState crosshair information for the plot (<code>null</code>
     *                       permitted).
     * @param pass           the pass index.
     */
    public void drawItem(Graphics2D g2, XYItemRendererState state, Rectangle2D dataArea,
                         PlotRenderingInfo info, XYPlot plot, ValueAxis domainAxis, ValueAxis rangeAxis,
                         XYDataset dataset, int series, int item, CrosshairState crosshairState, int pass) {

        if (!getItemVisible(series, item)) return;
        if (style == null) return;

        // setup for collecting optional entity info...
//		EntityCollection entities = null;
//		if (info != null) entities = info.getOwner().getEntityCollection();


        PlotOrientation orientation = plot.getOrientation();

        // TODO 10-2008 PEB well, we do our own paints/strokes, so see if we can delete that later
        Paint paint = getItemPaint(series, item);
        Stroke seriesStroke = getItemStroke(series, item);
        g2.setPaint(paint);
        g2.setStroke(seriesStroke);


        // get the data point...


        double x = dataset.getXValue(series, item);
        double y = dataset.getYValue(series, item);
        if (anyIsNaN(x, y)) return;

        RectangleEdge xAxisLocation = plot.getDomainAxisEdge();
        RectangleEdge yAxisLocation = plot.getRangeAxisEdge();
        double j2d_X = domainAxis.valueToJava2D(x, dataArea, xAxisLocation);
        double j2d_Y = rangeAxis.valueToJava2D(y, dataArea, yAxisLocation);

        // now start painting:

        if (pass == 0) {
            if (getPlotLines()) {
                plotLines(g2, state, dataArea, domainAxis, rangeAxis, dataset, series, item, orientation, x, xAxisLocation, yAxisLocation, j2d_X, j2d_Y);
            }
            updateCrosshairValues(crosshairState, x, y, 0, 0, j2d_X, j2d_Y, orientation);
        }
        if (pass == 1) {
            XYSeriesCollection xySeries = (dataset instanceof XYSeriesCollection) ? (XYSeriesCollection) dataset : null;
            XYDataItem dataItem = (xySeries == null) ? null : xySeries.getSeries(series).getDataItem(item);
            boolean plotErrorBars = xySeries != null && (dataItem instanceof XYDataItemErrs);
            if (plotErrorBars) {
                XYDataItemErrs xyErrs = (XYDataItemErrs) dataItem;
                // ... is NaN aware:
                double j2d_xErrLow = domainAxis.valueToJava2D(xyErrs.getXErrLow(), dataArea, xAxisLocation);
                double j2d_xErrHigh = domainAxis.valueToJava2D(xyErrs.getXErrHigh(), dataArea, xAxisLocation);
                double j2d_yErrLow = rangeAxis.valueToJava2D(xyErrs.getYErrLow(), dataArea, yAxisLocation);
                double j2d_yErrHigh = rangeAxis.valueToJava2D(xyErrs.getYErrHigh(), dataArea, yAxisLocation);


                try {
                    drawErrorBars(g2, j2d_X, j2d_Y, j2d_xErrHigh, j2d_xErrLow, j2d_yErrHigh, j2d_yErrLow);
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }

            if (getPlotImages()) {
                plotShape(g2, dataArea, orientation, j2d_X, j2d_Y);
            }
            // draw the item label if there is one...
            if (isItemLabelVisible(series, item)) {
                drawItemLabel(g2, orientation, dataset, series, item, j2d_X, j2d_Y, (y < 0.0));
            }
        }
    }

    private void drawErrorBars(Graphics2D g2, double j2d_X, double j2d_Y, double j2d_xErrHigh, double j2d_xErrLow, double j2d_yErrHigh, double j2d_yErrLow) {
        drawErrorBar(g2, j2d_X, j2d_Y, j2d_xErrHigh, j2d_xErrLow, true);
        drawErrorBar(g2, j2d_X, j2d_Y, j2d_yErrHigh, j2d_yErrLow, false);
    }

    /**
     * draw bar and cap, maybe called with NaN as a marker for no-err-value-known<br>
     * <ul>
     * <li> if both errors are NaN then do nothing,
     * <li> if both!= NaN then paint normal,
     * <li> if one is NaN paint the other half part (line ends at Point(x,y), only one cap)
     * </ul>
     *
     * @param g2
     * @param j2d_X
     * @param j2d_Y
     * @param j2d_ErrHigh
     * @param j2d_ErrLow
     * @param isXBar
     */
    private void drawErrorBar(Graphics2D g2, double j2d_X, double j2d_Y, double j2d_ErrHigh, double j2d_ErrLow, boolean isXBar) {
        boolean upNaN = Double.isNaN(j2d_ErrHigh);
        boolean downNaN = Double.isNaN(j2d_ErrLow);
        if (upNaN && downNaN) {
            return;
        }// no errorbars -> do nothing

        double up = upNaN ? (isXBar ? j2d_X : j2d_Y) : j2d_ErrHigh;
        double down = downNaN ? (isXBar ? j2d_X : j2d_Y) : j2d_ErrLow;
        double other = isXBar ? j2d_Y : j2d_X;// better Name?

        if (style.getErrorLine().prepare(g2)) {
            drawLine(g2, other, down, up, !isXBar);
        }

        J2DStyle.ErrorCap cap = style.getErrorCap();
        if (cap.prepare(g2)) {
            double halfWidth = cap.getSize() / 2;
            double start = other - halfWidth;
            double end = other + halfWidth;

            if (!upNaN) {
                drawLine(g2, j2d_ErrHigh, start, end, isXBar);
            }
            if (!downNaN) {
                drawLine(g2, j2d_ErrLow, start, end, isXBar);
            }
        }
    }


    private void plotLines(Graphics2D g2, XYItemRendererState state, Rectangle2D dataArea,
                           ValueAxis domainAxis, ValueAxis rangeAxis,
                           XYDataset dataset, int series, int item,
                           PlotOrientation orientation, double x,
                           RectangleEdge xAxisLocation,
                           RectangleEdge yAxisLocation, double j2d_X, double j2d_Y) {

        if (getDrawSeriesLineAsPath()) {
            if (item == 0) {
                State s = (State) state;
                s.seriesPath.reset();
                s.setLastPointGood(false);
            }
            drawSeriesLineAsPath(g2, state, dataset, series, item, orientation, j2d_X, j2d_Y);
        } else {
            if (item == 0) return;

            // get the previous data point...
            double x0 = dataset.getXValue(series, item - 1);
            double y0 = dataset.getYValue(series, item - 1);
            if (anyIsNaN(x0, y0)) return;

            boolean drawLine = true;
            if (getPlotDiscontinuous()) {
                // only draw a line if the gap between the current and
                // previous data
                // point is within the threshold
                int numX = dataset.getItemCount(series);
                double minX = dataset.getXValue(series, 0);
                double maxX = dataset.getXValue(series, numX - 1);
                if (getGapThresholdType() == UnitType.ABSOLUTE) {
                    drawLine = Math.abs(x - x0) <= getGapThreshold();
                } else {
                    drawLine = Math.abs(x - x0) <= ((maxX - minX) / numX * getGapThreshold());
                }
            }
            if (!drawLine) return;
            double transX0 = domainAxis.valueToJava2D(x0, dataArea, xAxisLocation);
            double transY0 = rangeAxis.valueToJava2D(y0, dataArea, yAxisLocation);

            // only draw if we have good values
            if (anyIsNaN(transX0, transY0, j2d_X, j2d_Y)) return;

            if (orientation == PlotOrientation.HORIZONTAL)
                state.workingLine.setLine(transY0, transX0, j2d_Y, j2d_X);
            else if (orientation == PlotOrientation.VERTICAL)
                state.workingLine.setLine(transX0, transY0, j2d_X, j2d_Y);

            if (state.workingLine.intersects(dataArea))
                style.getLine().paint(g2, state.workingLine);
        }
    }


    private void plotShape(Graphics2D g2, Rectangle2D dataArea, PlotOrientation orientation, double j2d_x, double j2d_y) {
//		Shape shape = getItemShape(series, item); // @@PEB@@ much changes here
        Shape shape = style.getSymbol().getShape();

		/*
		 * PEB 10-2008, info:
		 * 
		 * 
		 * Das [1] translated die shape ganz banal (no magic) und malt sie dann (wenn sichtbar)
		 * dabei werden f(c)r jedes paint eine affineTransform erzeugt (ok, das ist 'billig')
		 * die shape transformed (nicht ganz so billig) und dann getestet und gemalt.
		 * 
		 * Evtl. kann man mal schauen das in schnell zu machen ala:
		 * style.getSymbol().paintAtIf(g2, shape, x, y, area);
		 * das die shape untransformed malt, schlesslich ist das nur eine translation,
		 * und wir brauchen keine allgemeine transformation.
		 * 
		 * F(c)r das intersects k(c)nnte man sich ein eigenes 'Shape' ausdenken
		 * das Generelpath ableitet (besser delegiert) und sich die bounds merkt,
		 * daf(c)r das ding immutable machen 'new MyShape(Shape/GeneralPath)'
		 * 
		 * Aber 'erst messen dann verbessern' bedenken.
		 * 
		 * [1] 'ShapeUtilities.createTranslatedShape'
		 * 
		 */

        if (orientation == PlotOrientation.HORIZONTAL) {
            shape = ShapeUtilities.createTranslatedShape(shape, j2d_y, j2d_x);// Banal: das macht neue transform aus x/y und translated
        } else if (orientation == PlotOrientation.VERTICAL) {
            shape = ShapeUtilities.createTranslatedShape(shape, j2d_x, j2d_y);
        }
        if (shape.intersects(dataArea)) {
            style.getSymbol().paint(g2, shape);
        }
    }


    private void drawSeriesLineAsPath(Graphics2D g2, XYItemRendererState state, XYDataset dataset, int series, int item, PlotOrientation orientation, double j2d_X, double j2d_Y) {
        State s = (State) state;
        // update path to reflect latest point
        if (!anyIsNaN(j2d_X, j2d_Y)) {
            boolean exc = orientation == PlotOrientation.HORIZONTAL;
            float x = (float) (exc ? j2d_Y : j2d_X);
            float y = (float) (exc ? j2d_X : j2d_Y);

            if (s.isLastPointGood()) s.seriesPath.lineTo(x, y);
            else s.seriesPath.moveTo(x, y);
            s.setLastPointGood(true);
        } else {
            s.setLastPointGood(false);
        }

        if (item == dataset.getItemCount(series) - 1) {
            style.getLine().paint(g2, s.seriesPath);
        }
    }

    private static boolean anyIsNaN(double d0, double d1, double d2, double d3) {
        return Double.isNaN(d0) || Double.isNaN(d1) || Double.isNaN(d2) || Double.isNaN(d3);
    }

    private static boolean anyIsNaN(double d0, double d1) {
        return Double.isNaN(d0) || Double.isNaN(d1);
    }

    private static boolean bothAreNaN(double d0, double d1) {
        return Double.isNaN(d0) && Double.isNaN(d1);
    }

    /**
     * draw a line vert or horiz, coords in j2d
     *
     * @param g2
     * @param xy         where (x or y coord)
     * @param start      opposite coord to pos
     * @param end        opposite coord to pos
     * @param isVertical
     */
    private static void drawLine(Graphics2D g2, double xy, double start, double end, boolean isVertical) {
        // TODO PEB 10-2008 quick hack for errors on log axis, review later. if the x axis is log then: rework this, alos if axis are inverted
        // g2.getClipBounds() vielleicht das um die grenze zu erfahren (aber trafo mag die mit -1 spiegeln?)
        double MAX = 1000000d;//Double.MAX_VALUE/1000000; // Max_Value geht schon mal gar nicht ...
        if (xy == Double.POSITIVE_INFINITY) {
            xy = MAX;
        }
        if (start == Double.POSITIVE_INFINITY) {
            start = MAX;
        }
        if (end == Double.POSITIVE_INFINITY) {
            end = MAX;
        }
        // System.err.println(xy + ", [" + start + ", " + end + "]");
        if (isVertical) {
            g2.draw(new Line2D.Double(xy, start, xy, end));
        } else {
            g2.draw(new Line2D.Double(start, xy, end, xy));
        }
    }


    /**
     * @return Returns the style.
     */
    public J2DStyle.Dataset getStyle() {
        return style;
    }
}