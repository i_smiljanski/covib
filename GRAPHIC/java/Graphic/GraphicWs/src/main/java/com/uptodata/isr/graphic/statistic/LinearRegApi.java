package com.uptodata.isr.graphic.statistic;

import com.uptodata.isr.graphic.jaxbWrap.JBSource;
import com.uptodata.isr.jaxb.all.LinearRegressionTask;
import com.uptodata.isr.jaxb.all.Result;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.w3c.dom.Document;
import org.w3c.dom.Node;

import javax.xml.bind.JAXBContext;
import javax.xml.bind.JAXBException;
import javax.xml.bind.Marshaller;
import javax.xml.bind.Unmarshaller;
import javax.xml.transform.dom.DOMResult;


/**
 * Entry point to make statistics from data in {@link org.w3c.dom.Node}s
 *
 * @author PeterBuettner.de
 */
public class LinearRegApi {
    /**
     * Read task from taskNode, append it as a new child below appendHere, if
     * appendHere is null a new document is created and returned, the return
     * value is always the new &lt;result&gt; Node.
     * <p>
     * taskNode and appendHere maybe the same node, result is appended then
     * <p>
     * Note: if you create a new appendHere element in the taskNode this will be
     * <b>invalid </b>, since the additional element is not in the schema. So
     * better use a parent of this
     *
     * @param taskNode
     * note that we don't want the Document root but the &lt;task&gt;
     * node, for conviniance we work on the DocumentElement if this
     * is a Document, so mostly all is ok
     * @param appendHere
     * if null then create and return a new Document, this node maybe
     * also from a different document! Same note as for taskNode, we silently use the DocumentElement
     * @return the created result node
     * @throws javax.xml.bind.JAXBException
     */
    private static Logger log = LogManager.getLogger(LinearRegApi.class);

    public Node taskToResult(Node taskNode, Node appendHere, Node resultNode) {
        try {
            Unmarshaller u = JBSource.getValidatingUnmarshaller();
            Marshaller m = JBSource.getMarshaller();

            if (taskNode instanceof Document)// document-node forces NPE !(?)
                taskNode = ((Document) taskNode).getDocumentElement();

            if (appendHere instanceof Document)// document-node forces error in DomResult
                appendHere = ((Document) appendHere).getDocumentElement();

            LinearRegressionTask lrt = (LinearRegressionTask) u.unmarshal(taskNode);

            Result r = null;
            if (resultNode == null) {
                r = ResultMaker.createResult(lrt);
            } else {
                Unmarshaller u1 = JAXBContext.newInstance("com.uptodata.isr.jaxb.all").createUnmarshaller();
                r = (Result) u1.unmarshal(resultNode);
                r = ResultMaker.createResult(lrt, r);
            }

            DOMResult dr = new DOMResult(appendHere); // null creates a new DOM, marshaller is able to do this
            m.marshal(r, dr);// create into new DOM
            return dr.getNode().getLastChild(); // it is appended as last
        } catch (Exception e) {
            e.printStackTrace();
            return null;
        }
    }

    /**
     * Same as {@link #taskToResult(org.w3c.dom.Node, org.w3c.dom.Node)} but creates a new Document.
     *
     * @param taskNode
     * @return the created result node in the new document
     * @throws javax.xml.bind.JAXBException
     */
    public Node taskToResult(Node taskNode, Node resultNode) throws JAXBException {
        return taskToResult(taskNode, null, resultNode);
    }

    public Node taskToResult(Node taskNode) throws JAXBException {
        return taskToResult(taskNode, null);
    }
}
