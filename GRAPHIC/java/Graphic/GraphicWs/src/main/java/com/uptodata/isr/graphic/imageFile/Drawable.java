package com.uptodata.isr.graphic.imageFile;

import java.awt.*;

/**
 * @author PeterBuettner.de
 */
public interface Drawable {
    /**
     * draws anything into g2 at (0,0) with size
     *
     * @param g2
     * @param size
     */
    public void draw(Graphics2D g2, Dimension size);
    // TODO Dimension2D (for fine sizing if scaling)

}
