CREATE OR REPLACE VIEW ISR$BIO$COVIB$CONDITIONS (ENTITY, CODE, DISPLAY, ORDERNUMBER, RUNID, ANALYTEORDER, CONDITION) AS SELECT 'BIO$COVIB$CONDITIONS' entity
     , k.runanalytecode||'-'||to_number(regexp_replace(key,'^.*[ _\-]\w+(\d+)[ _\-]?.*$', '\1', 1, 1, 'i')) code
     , 'Condition: '||to_number(regexp_replace(key,'^.*[ _\-]\w+(\d+)[ _\-]?.*$', '\1', 1, 1, 'i')) display
     , 0 ordernumber
     , null runid, null analyteorder, null condition
  /*FROM (
        SELECT distinct
               k.runanalytecode,
               to_number(regexp_replace(key,'^.*[ _\-]\w+(\d+)[ _\-]?.*$', '\1', 1, 1, 'i')) condition*/
          FROM ISR$CRIT, TMP$bio$run$ana$known k
         WHERE entity = 'BIO$KNOWN'
           AND masterkey in ('S_WHOLEBLOOD')
           AND regexp_like(key,'^.*[ _\-]\w+(\d+)[ _\-]?.*$','i')
           --AND k.knowntype = 'KNOWN'
           --AND key = k.nameprefix
           AND repid = stb$object.getcurrentrepid
       --) p, TMP$BIO$COVIB$RUN$ANALYTES ra
  --WHERE ra.runanalytecode = p.runanalytecode
  --ORDER BY ra.runid, ra.analyteorder, p.condition;