  SELECT XMLELEMENT("experiment", XMLATTRIBUTES('&&Experiment&' AS "name" ,'&&KnownsCalcType&' AS "type"),
           Xmlagg (XMLConcat(
             Xmlagg (XMLELEMENT("calculation", xmlconcat(targ,val,res)) order by &&OrderBy& &&DilColumnGroupBy& &&RunIDColumn&),
             XMLELEMENT("statistic", XMLATTRIBUTES(acceptedrun AS "acceptedrun", analyteid AS "analyteid", analyteorder as "analyteorder", species as "species", matrix as "matrix", temperature as "temperature", hours as "hours", longtermtime as "longtermtime", longtermunits as "longtermunits", cycles as "cycles", concentrationunits as "unit") &&RunIDInTarget&,
               XMLELEMENT("flagpercent", min(flagpercent)),
               XMLELEMENT("min-bias", FormatRounded(min(bias), &&DecPlPercBias&)),
               XMLELEMENT("max-bias", FormatRounded(max(bias), &&DecPlPercBias&)),
               XMLELEMENT("mean-bias", FormatRounded(avg(bias), &&DecPlPercBias&)),
               XMLELEMENT("min-bias-wo-lloq", FormatRounded(min(bias_wo_lloq), &&DecPlPercBias&)),
               XMLELEMENT("max-bias-wo-lloq", FormatRounded(max(bias_wo_lloq), &&DecPlPercBias&)),
               XMLELEMENT("mean-bias-wo-lloq", FormatRounded(round(avg(bias_wo_lloq), &&DecPlPercBias&), &&DecPlPercBias&)),
               XMLELEMENT("min-bias-wo-uloq", FormatRounded(min(bias_wo_uloq), &&DecPlPercBias&)),
               XMLELEMENT("max-bias-wo-uloq", FormatRounded(max(bias_wo_uloq), &&DecPlPercBias&)),
               XMLELEMENT("mean-bias-wo-uloq", FormatRounded(round(avg(bias_wo_uloq), &&DecPlPercBias&), &&DecPlPercBias&)),
               XMLELEMENT("min-biasonmean", FormatRounded(min(biasonmean), &&DecPlPercBias&)),
               XMLELEMENT("max-biasonmean", FormatRounded(max(biasonmean), &&DecPlPercBias&)),
               XMLELEMENT("mean-biasonmean", FormatRounded(avg(biasonmean), &&DecPlPercBias&)),
               XMLELEMENT("min-biasonmean-wo-lloq", FormatRounded(min(biasonmean_wo_lloq), &&DecPlPercBias&)),
               XMLELEMENT("max-biasonmean-wo-lloq", FormatRounded(max(biasonmean_wo_lloq), &&DecPlPercBias&)),
               XMLELEMENT("min-biasonmean-w-lloq-intra", FormatRounded(min(min_biasonmean_w_lloq_intra), &&DecPlPercBias&)),
               XMLELEMENT("max-biasonmean-w-lloq-intra", FormatRounded(max(max_biasonmean_w_lloq_intra), &&DecPlPercBias&)),
               XMLELEMENT("min-biasonmean-wo-lloq-intra", FormatRounded(min(min_biasonmean_wo_lloq_intra), &&DecPlPercBias&)),
               XMLELEMENT("max-biasonmean-wo-lloq-intra", FormatRounded(max(max_biasonmean_wo_lloq_intra), &&DecPlPercBias&)),
               XMLELEMENT("mean-biasonmean-wo-lloq", FormatRounded(round(avg(biasonmean_wo_lloq), &&DecPlPercBias&), &&DecPlPercBias&)),
               XMLELEMENT("min-biasonmean-wo-uloq", FormatRounded(min(biasonmean_wo_uloq), &&DecPlPercBias&)),
               XMLELEMENT("max-biasonmean-wo-uloq", FormatRounded(max(biasonmean_wo_uloq), &&DecPlPercBias&)),
               XMLELEMENT("mean-biasonmean-wo-uloq", FormatRounded(round(avg(biasonmean_wo_uloq), &&DecPlPercBias&), &&DecPlPercBias&)),
               XMLELEMENT("min-cv", FormatRounded(min(cv), &&DecPlPercCV&)),
               XMLELEMENT("mean-cv", FormatRounded(avg(cv), &&DecPlPercCV&)),
               XMLELEMENT("max-cv", FormatRounded(max(cv), &&DecPlPercCV&)),
               XMLELEMENT("min-cv-wo-lloq", FormatRounded(round(min(cv_wo_lloq), &&DecPlPercCV&), &&DecPlPercCV&)),
               XMLELEMENT("mean-cv-wo-lloq", FormatRounded(round( case when '&&FinalAccuracy&' = 'T' then avg(cv_wo_lloq) else avg(round(cv_wo_lloq,1)) end, &&DecPlPercCV&), &&DecPlPercCV&)),
               XMLELEMENT("max-cv-wo-lloq", FormatRounded(round(max(cv_wo_lloq), &&DecPlPercCV&), &&DecPlPercCV&)),
               XMLELEMENT("min-cv-w-lloq-intra", FormatRounded(round(min(min_cv_w_lloq_intra), &&DecPlPercCV&), &&DecPlPercCV&)),
               XMLELEMENT("max-cv-w-lloq-intra", FormatRounded(round(max(max_cv_w_lloq_intra), &&DecPlPercCV&), &&DecPlPercCV&)),
               XMLELEMENT("min-cv-wo-lloq-intra", FormatRounded(round(min(min_cv_wo_lloq_intra), &&DecPlPercCV&), &&DecPlPercCV&)),
               XMLELEMENT("max-cv-wo-lloq-intra", FormatRounded(round(max(max_cv_wo_lloq_intra), &&DecPlPercCV&), &&DecPlPercCV&)),
               XMLELEMENT("numacc", sum(cntunflagged)), &&InterLotCvStatistic&
               XMLELEMENT("numtotal", sum(cnt)),
               XMLELEMENT("percacc", case when count(unflagged_bias) != 0 then FmtNum(round(sum(unflagged_bias)/count(unflagged_bias)*100,0)) else null end),
               XMLELEMENT("hours", hours),
               XMLELEMENT("longtermunits", longtermunits),
               XMLELEMENT("longtermtime", longtermtime),
               XMLELEMENT("temperature", temperature),
               XMLELEMENT("lloq",
                 XMLELEMENT("name", min(lloqname)),
                 XMLELEMENT("nominalconc", &&ConcFormatFunc&(&&ConcRepresentationFunc&(min(lloq_nominalconc), &&ConcFigures&), &&ConcFigures&))
                 ),
               XMLELEMENT("uloq",
                 XMLELEMENT("name", min(uloqname)),
                 XMLELEMENT("nominalconc", &&ConcFormatFunc&(&&ConcRepresentationFunc&(min(uloq_nominalconc), &&ConcFigures&), &&ConcFigures&))
                 )
             )) order by &&OrderBy&))
  FROM
   (SELECT analyteorder, nominalconc, min(knownorder) knownorder, species, matrix, temperature, longtermtime, longtermunits, hours, cycles, concentrationunits,
      Count(case when anchor = 'T' then null else useconc end) as cnt,
      count(case when anchor = 'T' then null else unflagged end) as cntunflagged,
      case
        when LENGTH(TRIM(TRANSLATE(name, ' +-.0123456789', ' '))) is null then to_number(name)
        else null
      end namenum,
      &&ConcRepresentationFunc&(Avg(case when anchor = 'T' then null
                                    when '&&FinalAccuracy&' = 'T' then useconc else useconc_sigrnd
                                    end), &&ConcFigures&) as mean,
      &&ConcRepresentationFunc&(Stddev(case when anchor = 'T' then null
                                       when '&&FinalAccuracy&' = 'T' then useconc else useconc_sigrnd
                                       end), &&ConcSDFigures&) as sd,
      round(case when min(anchor) = 'T' then null
                 when '&&FinalAccuracy&' = 'T' then Stddev(useconc)/nullif(Avg(useconc),0)*100
                 else &&ConcRepresentationFunc&(Stddev(useconc_sigrnd), &&ConcSDFigures&)/nullif(&&ConcRepresentationFunc&(Avg(useconc_sigrnd), &&ConcFigures&),0)*100
            end , &&DecPlPercCV&) as cv,
      case when name = min(lloqname) then null
           when min(anchor) = 'T' then null
           when '&&FinalAccuracy&' = 'T' then Stddev(useconc)/nullif(Avg(useconc),0)*100
           else &&ConcRepresentationFunc&(Stddev(useconc_sigrnd), &&ConcSDFigures&)/nullif(&&ConcRepresentationFunc&(Avg(useconc_sigrnd), &&ConcFigures&),0)*100
      end as cv_wo_lloq,
      min(case when name != min(lloqname) then null
           when min(anchor) = 'T' then null
           when '&&FinalAccuracy&' = 'T' then Stddev(useconc)/nullif(Avg(useconc),0)*100
           else &&ConcRepresentationFunc&(Stddev(useconc_sigrnd), &&ConcSDFigures&)/nullif(&&ConcRepresentationFunc&(Avg(useconc_sigrnd), &&ConcFigures&),0)*100
      end) over (partition by acceptedrun, analyteid, analyteorder, species, matrix) as min_cv_w_lloq_intra,
      max(case when name != min(lloqname) then null
           when min(anchor) = 'T' then null
           when '&&FinalAccuracy&' = 'T' then Stddev(useconc)/nullif(Avg(useconc),0)*100
           else &&ConcRepresentationFunc&(Stddev(useconc_sigrnd), &&ConcSDFigures&)/nullif(&&ConcRepresentationFunc&(Avg(useconc_sigrnd), &&ConcFigures&),0)*100
      end) over (partition by acceptedrun, analyteid, analyteorder, species, matrix) as max_cv_w_lloq_intra,
      min(case when name = min(lloqname) then null
           when min(anchor) = 'T' then null
           when '&&FinalAccuracy&' = 'T' then Stddev(useconc)/nullif(Avg(useconc),0)*100
           else &&ConcRepresentationFunc&(Stddev(useconc_sigrnd), &&ConcSDFigures&)/nullif(&&ConcRepresentationFunc&(Avg(useconc_sigrnd), &&ConcFigures&),0)*100
      end) over (partition by acceptedrun, analyteid, analyteorder, species, matrix) as min_cv_wo_lloq_intra,
      max(case when name = min(lloqname) then null
           when min(anchor) = 'T' then null
           when '&&FinalAccuracy&' = 'T' then Stddev(useconc)/nullif(Avg(useconc),0)*100
           else &&ConcRepresentationFunc&(Stddev(useconc_sigrnd), &&ConcSDFigures&)/nullif(&&ConcRepresentationFunc&(Avg(useconc_sigrnd), &&ConcFigures&),0)*100
      end) over (partition by acceptedrun, analyteid, analyteorder, species, matrix) as max_cv_wo_lloq_intra,
      round(Avg(case when anchor = 'T' then null when '&&FinalAccuracy&' = 'T' then usebias else usebias_sigrnd end), &&DecPlPercBias&) as bias,
      case when nominalconc = 0 then null
           else round(case when '&&FinalAccuracy&' = 'T' then (Avg(case when anchor = 'T' then null else useconc end)-nominalconc)/nullif(nominalconc,0)*100
                           else (&&ConcRepresentationFunc&(Avg(case when anchor = 'T' then null else useconc_sigrnd end), &&ConcFigures&)-nominalconc_sigrnd)/nullif(nominalconc_sigrnd,0)*100
                      end, &&DecPlPercBias&)
      end as biasonmean,
      case when min(anchor) = 'T' then null --anchor
           when abs(Avg(case when '&&FinalAccuracy&' = 'T' then usebias else usebias_sigrnd end)) > min(flagpercent) then 0
           else 1
      end as unflagged_bias,
      min(flagpercent) flagpercent,
      analyteid, name, acceptedrun &&DilColumnGroupBy& &&RunIDColumn&,
      case when round(min(nominalconc),15) <= round(min(lloqconv),15) then null
      else case when '&&FinalAccuracy&' = 'T' then Avg(usebias)
           else round(Avg(usebias_sigrnd), &&DecPlPercBias&) end
      end as bias_wo_lloq,
      case when name = min(lloqname) then null
           when nominalconc = 0 then null
           else case when '&&FinalAccuracy&' = 'T' then (Avg(case when anchor = 'T' then null else useconc end)-nominalconc)/nullif(nominalconc,0)*100
                     else round((&&ConcRepresentationFunc&(Avg(case when anchor = 'T' then null else useconc_sigrnd end), &&ConcFigures&)-nominalconc_sigrnd)/nullif(nominalconc_sigrnd,0)*100, &&DecPlPercBias&)
                end
      end as biasonmean_wo_lloq,
      case when round(min(nominalconc),15) >= round(min(uloqconv),15) then null
      else case when '&&FinalAccuracy&' = 'T' then Avg(usebias)
           else round(Avg(usebias_sigrnd), &&DecPlPercBias&) end
      end as bias_wo_uloq,
      case when name = min(uloqname) then null
           when nominalconc = 0 then null
           else case when '&&FinalAccuracy&' = 'T' then (Avg(case when anchor = 'T' then null else useconc end)-nominalconc)/nullif(nominalconc,0)*100
                     else round((&&ConcRepresentationFunc&(Avg(case when anchor = 'T' then null else useconc_sigrnd end), &&ConcFigures&)-nominalconc_sigrnd)/nullif(nominalconc_sigrnd,0)*100, &&DecPlPercBias&)
                end
      end as biasonmean_wo_uloq,
      case when '&&FinalAccuracy&' = 'T' then min(lloq_nominalconc) else min(lloq_nominalconc_sigrnd) end lloq_nominalconc,
      case when '&&FinalAccuracy&' = 'T' then min(uloq_nominalconc) else min(uloq_nominalconc_sigrnd) end uloq_nominalconc,
      min(case when name != min(lloqname) then null
           when nominalconc = 0 then null
           else case when '&&FinalAccuracy&' = 'T' then (Avg(case when anchor = 'T' then null else useconc end)-nominalconc)/nullif(nominalconc,0)*100
                     else round((&&ConcRepresentationFunc&(Avg(case when anchor = 'T' then null else useconc_sigrnd end), &&ConcFigures&)-nominalconc_sigrnd)/nullif(nominalconc_sigrnd,0)*100, &&DecPlPercBias&)
                end
      end) over (partition by acceptedrun, analyteid, analyteorder, species, matrix, concentrationunits) as min_biasonmean_w_lloq_intra,
      max(case when name != min(lloqname) then null
           when nominalconc = 0 then null
           else case when '&&FinalAccuracy&' = 'T' then (Avg(case when anchor = 'T' then null else useconc end)-nominalconc)/nullif(nominalconc,0)*100
                     else round((&&ConcRepresentationFunc&(Avg(case when anchor = 'T' then null else useconc_sigrnd end), &&ConcFigures&)-nominalconc_sigrnd)/nullif(nominalconc_sigrnd,0)*100, &&DecPlPercBias&)
                end
      end) over (partition by acceptedrun, analyteid, analyteorder, species, matrix, concentrationunits) as max_biasonmean_w_lloq_intra,
      min(case when name = min(lloqname) then null
           when nominalconc = 0 then null
           else case when '&&FinalAccuracy&' = 'T' then (Avg(case when anchor = 'T' then null else useconc end)-nominalconc)/nullif(nominalconc,0)*100
                     else round((&&ConcRepresentationFunc&(Avg(case when anchor = 'T' then null else useconc_sigrnd end), &&ConcFigures&)-nominalconc_sigrnd)/nullif(nominalconc_sigrnd,0)*100, &&DecPlPercBias&)
                end
      end) over (partition by acceptedrun, analyteid, analyteorder, species, matrix, concentrationunits) as min_biasonmean_wo_lloq_intra,
      max(case when name = min(lloqname) then null
           when nominalconc = 0 then null
           else case when '&&FinalAccuracy&' = 'T' then (Avg(case when anchor = 'T' then null else useconc end)-nominalconc)/nullif(nominalconc,0)*100
                     else round((&&ConcRepresentationFunc&(Avg(case when anchor = 'T' then null else useconc_sigrnd end), &&ConcFigures&)-nominalconc_sigrnd)/nullif(nominalconc_sigrnd,0)*100, &&DecPlPercBias&)
                end
      end) over (partition by acceptedrun, analyteid, analyteorder, species, matrix, concentrationunits) as max_biasonmean_wo_lloq_intra,
      min(lloqconv) lloqconv, min(uloqconv) uloqconv,
      min(lloqname) lloqname, min(uloqname) uloqname, min(anchor) anchor,
      XMLELEMENT("target",
        XMLELEMENT("acceptedrun", acceptedrun),
        XMLELEMENT("analyteid", analyteid),
        XMLELEMENT("species", species),
        XMLELEMENT("matrix", matrix),
        XMLELEMENT("name", NAME),
        XMLELEMENT("nominalconc", &&ConcFormatFunc&(nominalconc_sigrnd, &&ConcFigures&)),
        XMLELEMENT("hours", hours),
        XMLELEMENT("longtermunits", longtermunits),
        XMLELEMENT("longtermtime", longtermtime),
        XMLELEMENT("temperature", temperature),
        XMLELEMENT("cycles", cycles),
        XMLELEMENT("unit", concentrationunits) &&DilTraget& &&RunIDInTarget& &&LevelTarget&) as targ,
      XMLELEMENT("values", Xmlagg(
        XMLELEMENT("sample", XMLATTRIBUTES(sampleid AS "sampleid", sampleresultid AS "sampleresultid" ), &&RunIDInValues&
          XMLELEMENT("runsamplesequencenumber", runsamplesequencenumber),
          XMLELEMENT("conctext", conctext),
          XMLELEMENT("concentration", case when conctext = 'LLOQ' then '< LLOQ' when conctext = 'ULOQ' then '> ULOQ' else &&ConcFormatFunc&(conc_sigrnd,&&ConcFigures&) end ),
          --XMLELEMENT("concentration", &&ConcFormatFunc&(conc_sigrnd, &&ConcFigures&)),
          XMLELEMENT("concval", &&ConcFormatFunc&(conc_sigrnd,&&ConcFigures&)),
          XMLELEMENT("nominalconc", &&ConcFormatFunc&(nominalconc_sigrnd, &&ConcFigures&)),
          XMLELEMENT("subset", subset),
          XMLELEMENT("bias", FormatRounded(bias_sigrnd, &&DecPlPercBias&)),
          XMLELEMENT("accuracy", FormatRounded(accuracy_sigrnd,&&DecPlPercBias&)),
          XMLELEMENT("resultcommenttext", resultcommenttext),
          XMLELEMENT("analytearea", analytearea),
          XMLELEMENT("flag", flagged),
          --XMLELEMENT("cvflag", cvflagged),
          XMLELEMENT("flagpercent", flagpercent),
          XMLELEMENT("cvflagpercent", cvflagpercent),
          XMLELEMENT("lloq", lloqconv),
          XMLELEMENT("uloq", uloqconv),
          XMLELEMENT("deactivated", isdeactivated),
          case
            when isr_deactivation = 'T' then
              XMLELEMENT("deactivationreason", deactivationreason)
            else null
          end
          &&DilValue&)
          order by runnr, runsamplesequencenumber
          )) as val,
      XMLELEMENT("result",
        XMLELEMENT("n", Count(useconc)),
        XMLELEMENT("mean", &&ConcFormatFunc&(&&ConcRepresentationFunc&(Avg(
                           case when '&&FinalAccuracy&' = 'T' then useconc else useconc_sigrnd
                           end), &&ConcFigures&), &&ConcFigures&)),
        XMLELEMENT("sd", &&ConcFormatFunc&(&&ConcRepresentationFunc&(Stddev(
                        case when '&&FinalAccuracy&' = 'T' then useconc
                        else useconc_sigrnd
                        end), &&ConcSDFigures&), &&ConcSDFigures&)),
        XMLELEMENT("cv", case when '&&FinalAccuracy&' = 'T' then FormatRounded(round(Stddev(useconc)/nullif(Avg(useconc),0)*100, &&DecPlPercCV&), &&DecPlPercCV&)
                         else FormatRounded(round(&&ConcRepresentationFunc&(Stddev(useconc_sigrnd), &&ConcFigures&)/nullif(&&ConcRepresentationFunc&(Avg(useconc_sigrnd), &&ConcFigures&),0)*100, &&DecPlPercCV&), &&DecPlPercCV&)
                         end), &&InterLotCvResult&
        XMLELEMENT("bias", FormatRounded(round(Avg(case when '&&FinalAccuracy&' = 'T' then usebias else usebias_sigrnd end), &&DecPlPercBias&), &&DecPlPercBias&)),
        XMLELEMENT("biasonmean", FormatRounded(round(case when nominalconc = 0 then null
                                                    when '&&FinalAccuracy&' = 'T' then (Avg(useconc)-nominalconc)/nullif(nominalconc,0)*100
                                                    else (&&ConcRepresentationFunc&(Avg(useconc_sigrnd), &&ConcFigures&)-nominalconc_sigrnd)/nullif(nominalconc_sigrnd,0)*100
                                               end, &&DecPlPercBias&), &&DecPlPercBias&)),
        XMLELEMENT("accuracy", FormatRounded(round(case when nominalconc = 0 then null
                                                    when '&&FinalAccuracy&' = 'T' then ((Avg(useconc)-nominalconc)/nullif(nominalconc,0)*100)+100
                                                    else ((&&ConcRepresentationFunc&(Avg(useconc_sigrnd), &&ConcFigures&)-nominalconc_sigrnd)/nullif(nominalconc_sigrnd,0)*100)+100
                                               end, &&DecPlPercBias&), &&DecPlPercBias&)),
        XMLELEMENT("totalerror", FormatRounded(round(case when nominalconc = 0 then null
                                                    when '&&FinalAccuracy&' = 'T' then Abs((Avg(useconc)-nominalconc)/nullif(nominalconc,0)*100) +
                                                                                       FormatRounded(round(Stddev(useconc)/nullif(Avg(useconc),0)*100, &&DecPlPercCV&), &&DecPlPercCV&)
                                                    else Abs((&&ConcRepresentationFunc&(Avg(useconc_sigrnd), &&ConcFigures&)-nominalconc_sigrnd)/nullif(nominalconc_sigrnd,0)*100) +
                                                         FormatRounded(round(&&ConcRepresentationFunc&(Stddev(useconc_sigrnd), &&ConcFigures&)/nullif(&&ConcRepresentationFunc&(Avg(useconc_sigrnd), &&ConcFigures&),0)*100, &&DecPlPercCV&), &&DecPlPercCV&)
                                               end, &&DecPlPercTE&), &&DecPlPercTE&)),
        XMLELEMENT("meanofreps", &&ConcFormatFunc&(&&ConcRepresentationFunc&(Avg(useconc), &&ConcFigures&), &&ConcFigures&)),
        XMLELEMENT("biasofreps", FormatRounded(round(case when nominalconc = 0 then null
                                                    else (Avg(useconc)-nominalconc)/nullif(nominalconc,0)*100
                                               end, &&DecPlPercBias&), &&DecPlPercBias&)),
        XMLELEMENT("sdofreps", &&ConcFormatFunc&(&&ConcRepresentationFunc&(Stddev(useconc), &&ConcSDFigures&), &&ConcSDFigures&)),
        XMLELEMENT("cvofreps", FormatRounded(round(Stddev(useconc)/nullif(Avg(useconc),0)*100, &&DecPlPercCV&), &&DecPlPercCV&)),
        XMLELEMENT("flagged",
          case when abs(round(case when nominalconc = 0 then null
               when '&&FinalAccuracy&' = 'T' then (Avg(useconc)-nominalconc)/nullif(nominalconc,0)*100
               else (&&ConcRepresentationFunc&(Avg(useconc_sigrnd), &&ConcFigures&)-nominalconc_sigrnd)/nullif(nominalconc_sigrnd,0)*100
          end, &&DecPlPercBias&)) > min(flagpercent) then 'T' else 'F' end),
        XMLELEMENT("cvflagged",
          case when abs(case when '&&FinalAccuracy&' = 'T' then Stddev(useconc)/NULLIF(Avg(useconc),0)*100
               else round(&&ConcRepresentationFunc&(Stddev(useconc_sigrnd), &&ConcFigures&)/NULLIF(&&ConcRepresentationFunc&(Avg(useconc_sigrnd), &&ConcFigures&),0)*100, &&DecPlPercCV&)
          end) > min(cvflagpercent) then 'T' else 'F' end),
        XMLELEMENT("flagpercent", min(flagpercent)),
        XMLELEMENT("cvflagpercent", min(cvflagpercent)),
        case when min(knowntype) = 'STANDARD' then XMLCONCAT(XMLELEMENT("anchor",min(anchor)),XMLELEMENT("at_lloq",min(at_lloq)),XMLELEMENT("at_uloq",min(at_uloq))) else null end
        ) as res
    FROM
    (SELECT knownID,
     sampleID, sampleresultID, name, subset &&LevelCol&, &&InterLotCvCalc&
     nominalconc, nominalconc_sigrnd, analyteid &&DilColumn&,
     lloqconv, uloqconv, knowntype, resultcommenttext,
     min(case when round(nominalconc,15) = round(lloqconv,15) then name else null end) over (partition by acceptedrun, analyteid, analyteorder, species, matrix) as lloqname,
     min(case when round(nominalconc,15) = round(uloqconv,15) then name else null end) over (partition by acceptedrun, analyteid, analyteorder, species, matrix) as uloqname,
     hours, longtermtime, longtermunits, temperature, cycles,
     concentrationunits,
     case when knowntype = 'STANDARD' and (nominalconc_sigrnd<&&ConcRepresentationFunc&(lloqconv, &&ConcFigures&) or nominalconc_sigrnd>&&ConcRepresentationFunc&(uloqconv, &&ConcFigures&)) then 'T' else 'F' end anchor,
     case when knowntype = 'STANDARD' and (nominalconc_sigrnd=&&ConcRepresentationFunc&(lloqconv, &&ConcFigures&)) then 'T' else 'F' end at_lloq,
     case when knowntype = 'STANDARD' and (nominalconc_sigrnd=&&ConcRepresentationFunc&(uloqconv, &&ConcFigures&)) then 'T' else 'F' end at_uloq,
     /* Added, potentially wrong: */
     MIN(nominalconc_sigrnd) over (partition by acceptedrun, analyteid, analyteorder, species, matrix) lloq_nominalconc_sigrnd,
     MAX(nominalconc_sigrnd) over (partition by acceptedrun, analyteid, analyteorder, species, matrix) uloq_nominalconc_sigrnd,
     MIN(nominalconc) over (partition by acceptedrun, analyteid, analyteorder, species, matrix) lloq_nominalconc,
     MAX(nominalconc) over (partition by acceptedrun, analyteid, analyteorder, species, matrix) uloq_nominalconc,
     /* Added!!!!MIN(nominalconc_sigrnd) over (partition by acceptedrun, analyteid) lloq_nominalconc_sigrnd,
     MAX(nominalconc_sigrnd) over (partition by acceptedrun, analyteid) uloq_nominalconc_sigrnd,
     MIN(nominalconc) over (partition by acceptedrun, analyteid) lloq_nominalconc,
     MAX(nominalconc) over (partition by acceptedrun, analyteid) uloq_nominalconc,
     MIN(name) KEEP (DENSE_RANK FIRST ORDER BY nominalconc) over (partition by acceptedrun, analyteid) as lloqname,
     MIN(name) KEEP (DENSE_RANK LAST ORDER BY nominalconc) over (partition by acceptedrun, analyteid) as uloqname,
!!!!*/
     analyteorder, knownorder, species, matrix,
     conc, conctext, conc_sigrnd, bias, bias_sigrnd, flagpercent, cvflagpercent, accuracy_sigrnd,
     ISDEACTIVATED, isr_deactivation, deactivationreason,
     acceptedrun, runid, runnr, runsamplesequencenumber,
     case when ISDEACTIVATED = 'T' then null else conc_sigrnd end useconc_sigrnd,
     case when ISDEACTIVATED = 'T' then null else bias_sigrnd end usebias_sigrnd,
     case when ISDEACTIVATED = 'T' then null else conc end useconc,
     case when ISDEACTIVATED = 'T' then null else bias end usebias,
     case when abs(case when '&&FinalAccuracy&' = 'T' then bias else bias_sigrnd end) > flagpercent then '&&FlaggedSymbol&' else null end flagged,
     --case when abs(case when '&&FinalAccuracy&' = 'T' then sd / nullif(conc,0) else round(sd_sigrnd / nullif(conc_sigrnd,0),&&DecPlPercCV&) end) > cvflagpercent then '&&FlaggedSymbolCV&' else null end cvflagged,
     case when ISDEACTIVATED <> 'T' and abs(case when '&&FinalAccuracy&' = 'T' then bias else bias_sigrnd end) <= flagpercent then 1 else null end unflagged,
     analytearea,
     Count(conc)
      over (partition by analyteid, analyteorder, species, matrix, name, nominalconc, nominalconc_sigrnd, temperature, hours, longtermtime, longtermunits, cycles, concentrationunits, acceptedrun &&DilColumnGroupBy&, runid&&LevelGroupBy&) nByRun
     FROM
     (SELECT K.ID knownID,
        s.ID sampleID, sr.ID sampleresultID,
        &&KnownNameTargetField& name,
        &&LevelSelect&
        k.namesuffix subset,
        case when ra.RunAnalyteRegressionStatus = &&RejectedRunAnalyte& then 'F' else 'T' end acceptedrun,
        case
          when d.code is not null then 'T'
          else s.ISDEACTIVATED
        end ISDEACTIVATED,
        case
          when d.code is not null then 'T'
          else 'F'
        end isr_deactivation,
        d.reason deactivationreason,
        &&DilColumnQualif&
        s.runsamplesequencenumber,
        ra.analyteid, sa.analyteorder, a.species, a.sampletypeid matrix,
        kwz.ordernumber knownorder,
        &&ConcRepresentationFunc&(sr.nominalconcentrationconv, &&ConcFigures&) nominalconc_sigrnd,
        sr.nominalconcentrationconv nominalconc, sr.resultcommenttext,
        ra.lloqconv, ra.uloqconv, K.knowntype,
        case when sr.nominalconcentrationconv = ra.lloqconv then &&FlagPercentBiasLLOQ&
             when sr.nominalconcentrationconv = ra.uloqconv then &&FlagPercentBiasULOQ&
             when sr.nominalconcentrationconv not between ra.lloqconv and ra.uloqconv then null
             else &&FlagPercentBias&
        end flagpercent,
        case when sr.nominalconcentrationconv = ra.lloqconv then &&FlagPercentCvLLOQ&
             when sr.nominalconcentrationconv = ra.uloqconv then &&FlagPercentCvULOQ&
             when sr.nominalconcentrationconv not between ra.lloqconv and ra.uloqconv then null
             else &&FlagPercentCV&
        end cvflagpercent,
        s.runid, r.runid runnr,
        &&ConcRepresentationFunc&(sr.concentrationconv, &&ConcFigures&) conc_sigrnd,
        sr.concentrationconv conc, &&StabilityInfo&
        case when '&&ExcludeLLOQULOQFromStats&'='F' then null -- disable the LLOQ/LLOQ-feature, treat as numbers
             when sr.concentrationconv/nullif(s.dilutionfactor,0) < ra.lloqconv then 'LLOQ'
             when sr.concentrationconv/nullif(s.dilutionfactor,0) > ra.uloqconv then 'ULOQ'
             else null
        end conctext,
        ra.concentrationunits,
        case when sr.nominalconcentrationconv = 0 then null
             when (sr.concentrationconv/nullif(s.dilutionfactor,0) < ra.lloqconv and '&&ExcludeLLOQULOQFromStats&'='T') then null
             when (sr.concentrationconv/nullif(s.dilutionfactor,0) > ra.uloqconv and '&&ExcludeLLOQULOQFromStats&'='T') then null
             else round((&&ConcRepresentationFunc&(sr.concentrationconv, &&ConcFigures&)-&&ConcRepresentationFunc&(sr.nominalconcentrationconv, &&ConcFigures&))*100/
                         nullif(&&ConcRepresentationFunc&(sr.nominalconcentrationconv, &&ConcFigures&),0), &&DecPlPercBias&)
        end bias_sigrnd,
        case when sr.nominalconcentrationconv = 0 then null
             when (sr.concentrationconv/nullif(s.dilutionfactor,0) < ra.lloqconv and '&&ExcludeLLOQULOQFromStats&'='T') then null
             when (sr.concentrationconv/nullif(s.dilutionfactor,0) > ra.uloqconv and '&&ExcludeLLOQULOQFromStats&'='T') then null
             else round(&&ConcRepresentationFunc&(sr.concentrationconv,&&ConcFigures&)*100/
                        nullif(&&ConcRepresentationFunc&(sr.nominalconcentrationconv,&&ConcFigures&),0),&&DecPlPercBias&)
        end accuracy_sigrnd,
        case when sr.nominalconcentrationconv = 0 then null
             when (sr.concentrationconv/nullif(s.dilutionfactor,0) < ra.lloqconv and '&&ExcludeLLOQULOQFromStats&'='T') then null
             when (sr.concentrationconv/nullif(s.dilutionfactor,0) > ra.uloqconv and '&&ExcludeLLOQULOQFromStats&'='T') then null
             else (sr.concentrationconv-sr.nominalconcentrationconv)*100/
                   nullif(sr.nominalconcentrationconv,0)
        end bias,
        /*case when (sr.concentrationconv/nullif(s.dilutionfactor,0) < ra.lloqconv and '&&ExcludeLLOQULOQFromStats&'='T') then null
             when (sr.concentrationconv/nullif(s.dilutionfactor,0) > ra.uloqconv and '&&ExcludeLLOQULOQFromStats&'='T') then null
             else Stddev(sr.concentrationconv)
        end sd,
        case when (sr.concentrationconv/nullif(s.dilutionfactor,0) < ra.lloqconv and '&&ExcludeLLOQULOQFromStats&'='T') then null
             when (sr.concentrationconv/nullif(s.dilutionfactor,0) > ra.uloqconv and '&&ExcludeLLOQULOQFromStats&'='T') then null
             else &&ConcRepresentationFunc&(Stddev(&&ConcRepresentationFunc&(sr.concentrationconv,&&ConcFigures&)),&&ConcSDFigures&)
        end sd_sigrnd,*/
        srw.analytearea
      FROM &&TempTabPrefix&bio$run$ana$known K,
           &&TempTabPrefix&bio$run r,
           &&TempTabPrefix&bio$run$analytes ra,
           &&TempTabPrefix&bio$study$analytes sa,
           &&TempTabPrefix&bio$assay a,
           &&TempTabPrefix&bio$run$samples s,
           &&TempTabPrefix&bio$run$sample$results sr,
           &&TempTabPrefix&bio$run$sample$result$raw srw,
           &&TempTabPrefix&bio$covib$run$sample$deact d,
           isr$crit kwz,
           table(&&LALPackage&.GetRunStates) rs
      WHERE K.ID = s.runknownID
        AND R.ID = s.runid AND R.ID = sr.runid
        AND r.RuntypeDescription in (&&RunTypes&)
        AND s.ID = sr.runsampleID
        AND K.knowntype = '&&KnownType&'
        AND s.sampletype = 'known'
        AND s.runsamplekind = '&&KnownType&'
        AND K.knowntype = s.runsamplekind
        AND ra.id = sr.runanalyteid
        AND a.id = r.assayid
        AND sa.id = ra.analyteid
        AND ra.runid = s.runid
        AND K.studyid = '&&StudyID&'
        AND s.studyid = '&&StudyID&'
        AND sr.studyID = '&&StudyID&' AND r.studyid = '&&StudyID&'
        AND ra.studyID = '&&StudyID&'
        AND s.studycode || '-' || r.runid || '-' || s.runsamplesequencenumber = d.code (+)
        AND s.ID = srw.runSampleID (+)
        AND rs.column_value = r.runstatusnum
        AND kwz.key = &&KnownNameJoinField&
        AND kwz.entity = '&&KnownEntity&'
        AND kwz.masterkey = '&&ExpMasterkey&'
        AND repid = &&RepID&
        &&DilCond& &&AddCondition&
     ))
    &&GroupingConditions&
    GROUP BY analyteid, analyteorder, species, matrix, name, nominalconc, nominalconc_sigrnd, temperature, hours, longtermtime, longtermunits, cycles, concentrationunits, acceptedrun &&DilColumnGroupBy& &&RunIDColumn&&&LevelGroupBy&)
    GROUP BY analyteid, analyteorder, species, matrix, name, nominalconc, temperature, hours, longtermtime, longtermunits, cycles, concentrationunits, acceptedrun &&RunIDColumn&