  SELECT XMLELEMENT("experiment", XMLATTRIBUTES('&&Experiment&' AS "name" ,'parallelism' AS "type"),
           Xmlagg (XMLConcat(
             Xmlagg (XMLELEMENT("calculation", xmlconcat(targ,val,res)) order by /*assayidentifier, analyteindex, knownorder, */ acceptedrun, runid, sset, uhqc_conc),
             XMLELEMENT("statistic", XMLATTRIBUTES(acceptedrun AS "acceptedrun", analyteid AS "analyteid", analyteorder as "analyteorder", species as "species", matrix as "matrix", uhqc_conc as "uhqc_conc") ,
               XMLELEMENT("mean_recalc", &&ConcFormatFunc&(&&ConcRepresentationFunc&(Min(overall_useconc_recalc),&&ConcFigures&),&&ConcFigures&)),
               XMLELEMENT("mean", &&ConcFormatFunc&(&&ConcRepresentationFunc&(Min(overall_useconc),&&ConcFigures&),&&ConcFigures&)),
               XMLELEMENT("bias", FormatRounded(round((&&ConcRepresentationFunc&(Min(overall_useconc),&&ConcFigures&)-uhqc_conc)/NULLIF(uhqc_conc,0)*100,&&DecPlPercBias&),&&DecPlPercBias&)),
               XMLELEMENT("sd", &&ConcFormatFunc&(&&ConcRepresentationFunc&(Min(overall_sd),&&ConcSDFigures&),&&ConcSDFigures&)),
               XMLELEMENT("cv", FormatRounded(round(&&ConcRepresentationFunc&(Min(overall_sd),&&ConcFigures&)/NULLIF(&&ConcRepresentationFunc&(Min(overall_useconc),&&ConcFigures&),0)*100,&&DecPlPercCV&),&&DecPlPercCV&)),
               XMLELEMENT("runs", Xmlagg(
                  XMLELEMENT("run",
                    XMLELEMENT("runid", runid),
                    XMLELEMENT("sset", sset),
                    XMLELEMENT("lloqconv", lloqconv),
                    XMLELEMENT("uloqconv", uloqconv)
                  )
                  order by runid, sset
                ))
             )) order by analyteorder
           )
         )
  FROM
   (SELECT
      acceptedrun, analyteorder, species, matrix,
      min(flagpercent) flagpercent,
      analyteid, runid, uhqc_conc, sset, lloqconv, uloqconv,
      Avg(case
        when '&&ExcludeLLOQULOQFromStats&'!='F' and overall_mean_useconc_recalc not between lloqconv and uloqconv then null
        else &&ConcRepresentationFunc&(overall_mean_useconc_recalc,&&ConcFigures&)
      end) overall_useconc_recalc,
      Avg(case
        when '&&ExcludeLLOQULOQFromStats&'!='F' and overall_mean_useconc_recalc not between lloqconv and uloqconv then null
        else &&ConcRepresentationFunc&(overall_mean_useconc,&&ConcFigures&)
      end) overall_useconc,
      Stddev(case
        when '&&ExcludeLLOQULOQFromStats&'!='F' and overall_mean_useconc_recalc not between lloqconv and uloqconv then null
        else &&ConcRepresentationFunc&(overall_mean_useconc,&&ConcFigures&)
      end) overall_sd,
      Min(overall_mean_useconc) overall_useconcval,
      XMLELEMENT("target",
        XMLELEMENT("acceptedrun", acceptedrun),
        XMLELEMENT("analyteid", analyteid),
        XMLELEMENT("species", species),
        XMLELEMENT("matrix", matrix),
        XMLELEMENT("uhqc_conc", uhqc_conc),
        XMLELEMENT("runid", runid),
        XMLELEMENT("set", sset) ) as targ,
      XMLELEMENT("values", Xmlagg(
        XMLELEMENT("sample", --XMLATTRIBUTES(sampleid AS "sampleid", sampleresultid AS "sampleresultid" ),
          --XMLELEMENT("runsamplesequencenumber", runsamplesequencenumber),
          XMLELEMENT("name", NAME),
          XMLELEMENT("dilutionfactor", dilutionfactor),
          XMLELEMENT("conctext", conctext),
          XMLELEMENT("concval", &&ConcFormatFunc&(&&ConcRepresentationFunc&(case when '&&FinalAccuracy&'='T' then mean_conc else mean_conc_sigrnd end,&&ConcFigures&),&&ConcFigures&)),
          XMLELEMENT("concentration", case when conctext = 'LLOQ' or (nominalconc_sigrnd < lloqconv and mean_conc is null and mean_response is not null) then '< LLOQ'
                                           when conctext = 'ULOQ' or (nominalconc_sigrnd > uloqconv and mean_conc is null and mean_response is not null) then '> ULOQ'
                                           --when conctext is not null then nonctext
                                           else &&ConcFormatFunc&(&&ConcRepresentationFunc&(case when '&&FinalAccuracy&'='T' then mean_conc else mean_conc_sigrnd end,&&ConcFigures&),&&ConcFigures&)
                                      end),
          XMLELEMENT("recalc-concval", &&ConcFormatFunc&(&&ConcRepresentationFunc&(case when '&&FinalAccuracy&'='T' then mean_conc_recalc else mean_conc_recalc_sigrnd end,&&ConcFigures&),&&ConcFigures&)),
          XMLELEMENT("recalc-concentration", case when conctext = 'LLOQ' then '< LLOQ' when conctext = 'ULOQ' then '> ULOQ'
                                                  else &&ConcFormatFunc&(&&ConcRepresentationFunc&(case when '&&FinalAccuracy&'='T' then mean_conc_recalc else mean_conc_recalc_sigrnd end,&&ConcFigures&),&&ConcFigures&)
                                             end),
          XMLELEMENT("response", &&ConcFormatFunc&(&&ConcRepresentationFunc&(case when '&&FinalAccuracy&'='T' then mean_response else mean_response_sigrnd end,&&ConcFigures&),&&ConcFigures&)),
          XMLELEMENT("lloq", lloqconv),
          XMLELEMENT("uloq", uloqconv),
          XMLELEMENT("nominalconc", &&ConcFormatFunc&(nominalconc_sigrnd,&&ConcFigures&)),
          /*XMLELEMENT("recovery", &&ConcFormatFunc&(&&ConcRepresentationFunc&(
                              case when '&&FinalAccuracy&'='T' then mean_useconc/NULLIF(mean_first_mean,0)*100
                                   else &&ConcRepresentationFunc&(mean_useconc_sigrnd,&&ConcFigures&)/NULLIF(&&ConcRepresentationFunc&(mean_first_mean,&&ConcFigures&),0)*100
                              end,&&ConcFigures&),&&ConcFigures&)),*/
          XMLELEMENT("cv", FormatRounded(cv,&&DecPlPercBias&)),
          XMLELEMENT("bias", FormatRounded(biasonmean,&&DecPlPercBias&)),
          XMLELEMENT("biastouhqc", FormatRounded(biastouhqc,&&DecPlPercBias&)),
          XMLELEMENT("flag", case when flagged is not null then flagged else null end),
          XMLELEMENT("cvflag", case when cvflagged is not null then cvflagged else null end),
          XMLELEMENT("deactivated", min_isdeactivated),
          XMLELEMENT("overall_recalc-useconc",
            case
              when '&&ExcludeLLOQULOQFromStats&'!='F' and overall_mean_useconc_recalc < lloqconv then '< LLOQ'
              when '&&ExcludeLLOQULOQFromStats&'!='F' and overall_mean_useconc_recalc > uloqconv then '> ULOQ'
              else &&ConcFormatFunc&(&&ConcRepresentationFunc&(overall_mean_useconc_recalc,&&ConcFigures&),&&ConcFigures&)
            end
          ),
          XMLELEMENT("overall_recalc-useconcval", &&ConcFormatFunc&(&&ConcRepresentationFunc&(overall_mean_useconc_recalc,&&ConcFigures&),&&ConcFigures&)),
          XMLELEMENT("overall_useconctext",
            case
              when '&&ExcludeLLOQULOQFromStats&'!='F' and overall_mean_useconc_recalc < lloqconv then 'LLOQ'
              when '&&ExcludeLLOQULOQFromStats&'!='F' and overall_mean_useconc_recalc > uloqconv then 'ULOQ'
              else null
            end
          ),
          XMLELEMENT("overall_useconc",
            case
              when '&&ExcludeLLOQULOQFromStats&'!='F' and overall_mean_useconc_recalc < lloqconv then '< LLOQ'
              when '&&ExcludeLLOQULOQFromStats&'!='F' and overall_mean_useconc_recalc > uloqconv then '> ULOQ'
              else &&ConcFormatFunc&(&&ConcRepresentationFunc&(overall_mean_useconc,&&ConcFigures&),&&ConcFigures&)
            end
          ),
          XMLELEMENT("overall_concval", &&ConcFormatFunc&(&&ConcRepresentationFunc&(overall_mean_useconc,&&ConcFigures&),&&ConcFigures&)),
          XMLELEMENT("overall_bias",
            case
              when '&&ExcludeLLOQULOQFromStats&'!='F' and overall_mean_useconc_recalc not between lloqconv and uloqconv then null
              else FormatRounded(round((&&ConcRepresentationFunc&(overall_mean_useconc,&&ConcFigures&)-uhqc_conc)/NULLIF(uhqc_conc,0)*100,&&DecPlPercBias&),&&DecPlPercBias&)
            end
          ),
          XMLELEMENT("overall_sd",
            case
              when '&&ExcludeLLOQULOQFromStats&'!='F' and overall_mean_useconc_recalc not between lloqconv and uloqconv then null
              else &&ConcFormatFunc&(&&ConcRepresentationFunc&(overall_sd,&&ConcSDFigures&),&&ConcSDFigures&)
            end
          ),
          XMLELEMENT("overall_cv",
            case
              when '&&ExcludeLLOQULOQFromStats&'!='F' and overall_mean_useconc_recalc not between lloqconv and uloqconv then null
              else FormatRounded(round(&&ConcRepresentationFunc&(overall_sd,&&ConcFigures&)/NULLIF(&&ConcRepresentationFunc&(overall_mean_useconc,&&ConcFigures&),0)*100,&&DecPlPercCV&),&&DecPlPercCV&)
            end
          ),
          XMLELEMENT("overall_flag",
            case when abs(case
              when '&&ExcludeLLOQULOQFromStats&'!='F' and overall_mean_useconc_recalc not between lloqconv and uloqconv then null
              else round((&&ConcRepresentationFunc&(overall_mean_useconc,&&ConcFigures&)-uhqc_conc)/NULLIF(uhqc_conc,0)*100,&&DecPlPercBias&)
            end) > flagpercent then '&&FlaggedSymbol&' else null end
          ),
          XMLELEMENT("overall_cvflag",
            case when abs(case
              when '&&ExcludeLLOQULOQFromStats&'!='F' and overall_mean_useconc_recalc not between lloqconv and uloqconv then null
              else round(&&ConcRepresentationFunc&(overall_sd,&&ConcFigures&)/NULLIF(&&ConcRepresentationFunc&(overall_mean_useconc,&&ConcFigures&),0)*100,&&DecPlPercCV&)
            end) > cvflagpercent then '&&FlaggedSymbolCV&' else null end
          ),
          XMLELEMENT("flagpercent", flagpercent),
          XMLELEMENT("cvflagpercent", cvflagpercent),
          XMLELEMENT("samples", xml)
        )
        order by dilutionfactor, nominalconc desc, name
      )) as val,
      XMLELEMENT("result",
        XMLELEMENT("n", Count(mean_useconc)),
        --XMLELEMENT("isref", case when dilutionfactor = min(refdilfactor) then 'T' else 'F' end),
        XMLELEMENT("mean_response", &&ConcFormatFunc&(&&ConcRepresentationFunc&(Avg(
                           case when '&&FinalAccuracy&'='T' then mean_response else mean_response_sigrnd
                           end),&&ConcFigures&),&&ConcFigures&)),
        XMLELEMENT("mean_conc_recalc", &&ConcFormatFunc&(&&ConcRepresentationFunc&(Avg(
                           case when '&&FinalAccuracy&'='T' then mean_useconc_recalc else mean_useconc_recalc_sigrnd
                           end),&&ConcFigures&),&&ConcFigures&)),
        XMLELEMENT("mean_conc", &&ConcFormatFunc&(&&ConcRepresentationFunc&(Avg(
                           case when '&&FinalAccuracy&'='T' then mean_useconc else mean_useconc_sigrnd
                           end),&&ConcFigures&),&&ConcFigures&)),
        /*XMLELEMENT("recovery", &&ConcFormatFunc&(&&ConcRepresentationFunc&(
                              case when '&&FinalAccuracy&'='T' then Avg(mean_useconc)/NULLIF(Min(mean_first_mean),0)*100
                                   else &&ConcRepresentationFunc&(Avg(mean_useconc_sigrnd),&&ConcFigures&)/NULLIF(Min(&&ConcRepresentationFunc&(mean_first_mean,&&ConcFigures&)),0)*100
                              end,&&ConcFigures&),&&ConcFigures&)),*/
        XMLELEMENT("sd", case when Count(mean_useconc) < 2 then null else &&ConcFormatFunc&(&&ConcRepresentationFunc&(Stddev(
                        case when '&&FinalAccuracy&'='T' then mean_useconc
                        else mean_useconc_sigrnd
                        end),&&ConcSDFigures&),&&ConcSDFigures&) end),
        XMLELEMENT("cv", case when Count(mean_useconc) < 2 then null when '&&FinalAccuracy&'='T' then FormatRounded(round(Stddev(mean_useconc)/NULLIF(Avg(mean_useconc),0)*100,&&DecPlPercCV&),&&DecPlPercCV&)
                         else FormatRounded(round(&&ConcRepresentationFunc&(Stddev(mean_useconc_sigrnd),&&ConcFigures&)/NULLIF(&&ConcRepresentationFunc&(Avg(mean_useconc_sigrnd),&&ConcFigures&),0)*100,&&DecPlPercCV&),&&DecPlPercCV&)
                         end),
        XMLELEMENT("biastouhqc", FormatRounded(round(case when '&&FinalAccuracy&'='T' then (Avg(mean_useconc)-Min(uhqc_conc))/NULLIF(Min(uhqc_conc),0)*100
                                                    else (&&ConcRepresentationFunc&(Avg(mean_useconc_sigrnd),&&ConcFigures&)-Min(uhqc_conc))/NULLIF(Min(uhqc_conc),0)*100
                                               end,&&DecPlPercBias&),&&DecPlPercBias&)),
        /*XMLELEMENT("bias", FormatRounded(round(Avg(case when '&&FinalAccuracy&'='T' then usebias else usebias_sigrnd end),&&DecPlPercBias&),&&DecPlPercBias&)),
        XMLELEMENT("biasonmean", FormatRounded(round(case when nominalconc = 0 then null
                                                    when '&&FinalAccuracy&'='T' then (Avg(mean_useconc)-nominalconc)/nominalconc*100
                                                    else (&&ConcRepresentationFunc&(Avg(mean_useconc_sigrnd),&&ConcFigures&)-nominalconc_sigrnd)/nominalconc_sigrnd*100
                                               end,&&DecPlPercBias&),&&DecPlPercBias&)),
        XMLELEMENT("accuracy", FormatRounded(round(case when nominalconc = 0 then null
                                                    when '&&FinalAccuracy&'='T' then 100*Avg(mean_useconc)/nominalconc
                                                    else 100*&&ConcRepresentationFunc&(Avg(useconc_sigrnd),&&ConcFigures&)/nominalconc_sigrnd
                                               end,&&DecPlPercBias&),&&DecPlPercBias&)),*/
        XMLELEMENT("flagpercent", min(flagpercent)),
        XMLELEMENT("cvflagpercent", min(cvflagpercent))
        ) as res
    FROM
    (SELECT
      acceptedrun, analyteid, analyteorder, species, matrix, runid, uhqc_conc, name, dilutionfactor, lloqconv, uloqconv, nominalconc, nominalconc_sigrnd, flagpercent, cvflagpercent, sset,
      mean_response, mean_response_sigrnd, conctext, mean_conc_recalc, mean_conc_recalc_sigrnd, mean_conc, mean_conc_sigrnd,
      case when conctext is null then mean_useconc_recalc else null end mean_useconc_recalc,
      case when conctext is null then mean_useconc_recalc_sigrnd else null end mean_useconc_recalc_sigrnd,
      case when conctext is null then mean_useconc else null end mean_useconc,
      case when conctext is null then mean_useconc_sigrnd else null end mean_useconc_sigrnd,
      min_isdeactivated, /*mean_first_mean, */
      case when conctext is null then flagged else null end flagged,
      case when conctext is null and '&&CVFlagOnResultComment&' is not null and regexp_like(resultcommenttext,'&&CVFlagOnResultComment&', 'i') then '&&FlaggedSymbolCV&' when conctext is null then cvflagged else null end cvflagged,
      case when conctext is null then unflagged else 1 end unflagged,
      case when conctext is null then biasonmean else null end biasonmean,
      case when conctext is null then biastouhqc else null end biastouhqc,
      case when conctext is null then cv else null end cv,
      Avg(case
               when conctext is not null then null
               when '&&FinalAccuracy&'='T' then mean_useconc_recalc
               else mean_useconc_recalc_sigrnd
          end)
        over (partition by acceptedrun, analyteid, analyteorder, species, matrix, uhqc_conc, name, dilutionfactor, nominalconc) overall_mean_useconc_recalc,
      Avg(case
               when conctext is not null then null
               when '&&FinalAccuracy&'='T' then mean_useconc
               else mean_useconc_sigrnd
          end)
        over (partition by acceptedrun, analyteid, analyteorder, species, matrix, uhqc_conc, name, dilutionfactor, nominalconc) overall_mean_useconc,
      Stddev(case
               when conctext is not null then null
               when '&&FinalAccuracy&'='T' then mean_useconc
               else mean_useconc_sigrnd
             end)
        over (partition by acceptedrun, analyteid, analyteorder, species, matrix, uhqc_conc, name, dilutionfactor, nominalconc) overall_sd,
      xml
    FROM
    (SELECT
      acceptedrun, analyteid, analyteorder, species, matrix, runid, uhqc_conc, name, dilutionfactor, lloqconv, uloqconv, nominalconc, nominalconc_sigrnd, flagpercent, cvflagpercent, sset,
      Avg(analytearea) mean_response,
      &&ConcRepresentationFunc&(Avg(analytearea),&&ConcFigures&) mean_response_sigrnd,
      Min(resultcommenttext) resultcommenttext,
      case 
        when nominalconc < lloqconv then 'LLOQ'
        when nominalconc > uloqconv then 'ULOQ'
        --when nominalconc < lloqconv or nominalconc > uloqconv then 'NA'
        when '&&ExcludeLLOQULOQFromStats&'='F' then null -- disable the LLOQ/LLOQ-feature, treat as numbers
        when '&&FinalAccuracy&'='T' and Avg(conc / dilutionfactor) < lloqconv then 'LLOQ'
        when '&&FinalAccuracy&'='T' and Avg(conc / dilutionfactor) > uloqconv then 'ULOQ'
        when &&ConcRepresentationFunc&(Avg(conc_sigrnd / dilutionfactor),&&ConcFigures&) < lloqconv then 'LLOQ'
        when &&ConcRepresentationFunc&(Avg(conc_sigrnd / dilutionfactor),&&ConcFigures&) > uloqconv then 'ULOQ'
        else null
      end conctext,
      Avg(conc / dilutionfactor) mean_conc_recalc,
      &&ConcRepresentationFunc&(Avg(conc_sigrnd / dilutionfactor),&&ConcFigures&) mean_conc_recalc_sigrnd,
      Avg(conc) mean_conc,
      &&ConcRepresentationFunc&(Avg(conc_sigrnd),&&ConcFigures&) mean_conc_sigrnd,
      Avg(useconc / dilutionfactor) mean_useconc_recalc,
      &&ConcRepresentationFunc&(&&ConcRepresentationFunc&(Avg(useconc_sigrnd),&&ConcFigures&) / dilutionfactor,&&ConcFigures&) mean_useconc_recalc_sigrnd,
      Avg(useconc) mean_useconc,
      &&ConcRepresentationFunc&(Avg(useconc_sigrnd),&&ConcFigures&) mean_useconc_sigrnd,
      Stddev(useconc) mean_sd,
      &&ConcRepresentationFunc&(Stddev(useconc_sigrnd),&&ConcSDFigures&) mean_sd_sigrnd,
      Min(isdeactivated) min_isdeactivated,
      --Avg(first_mean) mean_first_mean,
      case when abs(case when '&&FinalAccuracy&'='T' then (Avg(useconc)-uhqc_conc)/NULLIF(uhqc_conc,0)*100 else round((&&ConcRepresentationFunc&(Avg(useconc_sigrnd),&&ConcFigures&)-uhqc_conc)/NULLIF(uhqc_conc,0)*100,&&DecPlPercBias&) end) > flagpercent then '&&FlaggedSymbol&' else null end flagged,
      case when Min(isdeactivated) <> 'T' and abs(case when '&&FinalAccuracy&'='T' then Avg(conc)/NULLIF(uhqc_conc,0) else round(Avg(conc_sigrnd)/NULLIF(uhqc_conc,0),&&DecPlPercBias&) end) <= flagpercent then 1 else null end unflagged,
      case
        when '&&CVFlagOnResultComment&' != '' and regexp_like(Min(resultcommenttext),'&&CVFlagOnResultComment&', 'i') then '&&FlaggedSymbolCV&'
        when abs(case when '&&FinalAccuracy&'='T' then Stddev(useconc)/NULLIF(Avg(useconc),0) else round(&&ConcRepresentationFunc&(Stddev(useconc_sigrnd),&&ConcSDFigures&)/NULLIF(&&ConcRepresentationFunc&(Avg(useconc_sigrnd),&&ConcFigures&),0),&&DecPlPercCV&) end) > cvflagpercent then '&&FlaggedSymbolCV&'
        else null
      end cvflagged,
      round(case when '&&FinalAccuracy&'='T' then (Avg(useconc)-nominalconc)/NULLIF(nominalconc,0)*100
                 else (&&ConcRepresentationFunc&(Avg(useconc_sigrnd),&&ConcFigures&)-nominalconc_sigrnd)/NULLIF(nominalconc_sigrnd,0)*100
            end,&&DecPlPercBias&) biasonmean,
      round(case when '&&FinalAccuracy&'='T' then (Avg(useconc)-uhqc_conc)/NULLIF(uhqc_conc,0)*100
                 else (&&ConcRepresentationFunc&(Avg(useconc_sigrnd),&&ConcFigures&)-uhqc_conc)/NULLIF(uhqc_conc,0)*100
            end,&&DecPlPercBias&) biastouhqc,
      round(case when '&&FinalAccuracy&'='T' then Stddev(useconc)/NULLIF(Avg(useconc),0)*100
                 else &&ConcRepresentationFunc&(Stddev(useconc_sigrnd),&&ConcFigures&)/NULLIF(&&ConcRepresentationFunc&(Avg(useconc_sigrnd),&&ConcFigures&),0)*100
            end,&&DecPlPercCV&) cv,
      Xmlagg(
        XMLELEMENT("sample", XMLATTRIBUTES(sampleid AS "sampleid", sampleresultid AS "sampleresultid" ),
          XMLELEMENT("runsamplesequencenumber", runsamplesequencenumber),
          XMLELEMENT("dilutionfactor", dilutionfactor),
          XMLELEMENT("concentration", &&ConcFormatFunc&(&&ConcRepresentationFunc&(case when '&&FinalAccuracy&'='T' then conc else conc_sigrnd end,&&ConcFigures&),&&ConcFigures&)),
          XMLELEMENT("concval", &&ConcFormatFunc&(conc_sigrnd,&&ConcFigures&)), XMLELEMENT("concval-full", FmtNum(conc)),
          XMLELEMENT("recalc-concentration", &&ConcFormatFunc&(&&ConcRepresentationFunc&(&&ConcRepresentationFunc&(case when '&&FinalAccuracy&'='T' then conc else conc_sigrnd end,&&ConcFigures&) / dilutionfactor,&&ConcFigures&),&&ConcFigures&)),
          XMLELEMENT("recalc-concval", &&ConcFormatFunc&(&&ConcRepresentationFunc&(&&ConcRepresentationFunc&(case when '&&FinalAccuracy&'='T' then conc else conc_sigrnd end,&&ConcFigures&) / dilutionfactor,&&ConcFigures&),&&ConcFigures&)),
          XMLELEMENT("recalc-concval-full", FmtNum( conc / dilutionfactor)),
          XMLELEMENT("conc-full", FmtNum( conc_full )),
          XMLELEMENT("replicatenumber", replicatenumber),
          XMLELEMENT("resultcommenttext", resultcommenttext),
          XMLELEMENT("response", analytearea),
          XMLELEMENT("lloq", lloqconv),
          XMLELEMENT("uloq", uloqconv),
          XMLELEMENT("nominalconc", &&ConcFormatFunc&(nominalconc_sigrnd,&&ConcFigures&)),
          XMLELEMENT("subset", subset),
          XMLELEMENT("resultcommenttext", resultcommenttext),
          --XMLELEMENT("flag", flagged),
          XMLELEMENT("deactivated", isdeactivated)
        ) order by runsamplesequencenumber, name
      ) xml
    FROM
    (SELECT knownID, sampleID, sampleresultID, NAME, subset, nominalconc, nominalconc_sigrnd, analyteid, dilutionfactor, uhqc_conc, acceptedrun,
     lloqconv, uloqconv, knowntype, resultcommenttext, analyteorder, knownorder, species, matrix, sset, replicatenumber,
     conc, conc_sigrnd, conc_full, /*bias, bias_sigrnd, accuracy_sigrnd,*/ flagpercent, cvflagpercent, ISDEACTIVATED, runid, runnr, runsamplesequencenumber,
     case when ISDEACTIVATED='T' then null
          --when conctext is not null then null
          --else conc_sigrnd
          else conc
     end useconc_sigrnd,
     case when ISDEACTIVATED='T' then null
          --when conctext is not null then null
          else conc
     end useconc,
     /*avg(case when ISDEACTIVATED='T' then null
              when conctext is not null then null
              when '&&FinalAccuracy&'='T' then conc_sigrnd
              else conc
         end )
       KEEP (DENSE_RANK FIRST ORDER BY usedilfactor)
       OVER (PARTITION BY acceptedrun, analyteid, analyteorder, species, matrix, runid, uhqc_conc) first_mean,
       min(usedilfactor)
       OVER (PARTITION BY acceptedrun, analyteid, analyteorder, species, matrix, runid, uhqc_conc) refdilfactor,*/
     analytearea
     FROM
     (
     SELECT K.ID knownID, s.ID sampleID, sr.ID sampleresultID, &&KnownNameTargetField& name, k.namesuffix subset, s.ISDEACTIVATED, s.dilutionfactor,
        case
          when regexp_like(k.nameprefix, '^DL[-_\ ]\D[-_\ ](\d+).*$', 'i') then to_number(regexp_replace(k.nameprefix, '^DL[-_\ ]\D[-_\ ](\d+).*$', '\1', 1, 1, 'i'))
          else null
        end sset,
        s.runsamplesequencenumber,
        case
          when LENGTH(TRIM(TRANSLATE(k.source, ' +-.0123456789', ' '))) is null then to_number(TRIM(k.source))
          else null
        end uhqc_conc,
        case when ra.RunAnalyteRegressionStatus = &&RejectedRunAnalyte& then 'F' else 'T' end acceptedrun,
        &&ConcRepresentationFunc&(k.concentrationconv,&&ConcFigures&) nominalconc_sigrnd,
        k.concentrationconv nominalconc, sr.resultcommenttext, s.replicatenumber,
        ra.lloqconv, ra.uloqconv, K.knowntype,
        case when k.concentrationconv = ra.lloqconv then &&FlagPercentBiasLLOQ&
             when k.concentrationconv = ra.uloqconv then &&FlagPercentBiasULOQ&
             --when k.concentrationconv not between ra.lloqconv and ra.uloqconv then null
             else &&FlagPercentBias&
        end flagpercent,
        case when k.concentrationconv = ra.lloqconv then &&FlagPercentCvLLOQ&
             when k.concentrationconv = ra.uloqconv then &&FlagPercentCvULOQ&
             --when k.concentrationconv not between ra.lloqconv and ra.uloqconv then null
             else &&FlagPercentCV&
        end cvflagpercent,
        s.runid, r.runid runnr, sa.id analyteid,
        &&ConcRepresentationFunc&(sr.concentrationconv,&&ConcFigures&) conc_sigrnd,
        sr.concentrationconv conc,
        sr.concentration conc_full, &&StabilityInfo&
        case when ISDEACTIVATED='T' then null
          when sr.concentrationconv/s.dilutionfactor not between ra.lloqconv and ra.uloqconv then null
          else s.dilutionfactor
        end usedilfactor,
        sa.analyteorder, kwz.ordernumber knownorder, a.species, a.sampletypeid matrix, srw.analytearea, srw.analytearea_sigrnd analytearea_sigrnd
      FROM &&TempTabPrefix&bio$run$ana$known K,
           &&TempTabPrefix&bio$run r,
           &&TempTabPrefix&bio$run$analytes ra,
           --&&TempTabPrefix&wt$assay$analyte aa,
           &&TempTabPrefix&bio$study$analytes sa,  --neu
           &&TempTabPrefix&bio$assay a,            --neu
           &&TempTabPrefix&bio$run$samples s,
           &&TempTabPrefix&bio$run$sample$results sr,
           --&&TempTabPrefix&bio$run$sample$result$raw srw,
           (select runknownid, Avg(analytearea) analytearea, &&ConcRepresentationFunc&(Avg(analytearea), &&ConcFigures&) analytearea_sigrnd from &&TempTabPrefix&bio$run$sample$result$raw, &&TempTabPrefix&bio$run$samples s where runsampleid = s.id group by runknownid) srw,
           isr$crit kwz,
           table(&&LALPackage&.GetRunStates) rs
      WHERE K.ID=s.runknownID
        AND R.ID = s.runid AND R.ID = sr.runid
        AND r.RuntypeDescription in (&&RunTypes&)
        AND s.ID=sr.runsampleID
        AND K.knowntype = '&&KnownType&'
        AND s.sampletype='known'
        AND s.runsamplekind = '&&KnownType&'
        AND K.knowntype = s.runsamplekind
        AND ra.id = sr.runanalyteid
        AND a.id = r.assayid
        AND sa.id = ra.analyteid
        AND ra.runid = s.runid
        AND K.studyid = '&&StudyID&' AND s.studyid = '&&StudyID&'
        AND sr.studyID = '&&StudyID&' AND r.studyid = '&&StudyID&'
        AND ra.studyID = '&&StudyID&'
        --AND s.ID = srw.runSampleID (+)
        AND s.runknownID = srw.runknownID (+)
        AND rs.column_value=r.runstatusnum
        AND kwz.key = &&KnownNameJoinField&
        AND kwz.entity = '&&KnownEntity&'
        AND kwz.masterkey = '&&ExpMasterkey&'
        AND kwz.repid=&&RepID&
        &&AddCondition&
     )) GROUP BY acceptedrun, analyteid, analyteorder, species, matrix, runid, sset, uhqc_conc, name, dilutionfactor, lloqconv, uloqconv, nominalconc, nominalconc_sigrnd, flagpercent, cvflagpercent
      --GROUP BY acceptedrun, analyteid, analyteorder, species, matrix, runid, uhqc_conc, NAME, knownorder, nominalconc, nominalconc_sigrnd, dilutionfactor, runid
     )
    )
    GROUP BY acceptedrun, analyteid, analyteorder, species, matrix, runid, sset, uhqc_conc, lloqconv, uloqconv)
    GROUP BY acceptedrun, analyteid, analyteorder, species, matrix, uhqc_conc