  SELECT XMLELEMENT("experiment", XMLATTRIBUTES('&&Experiment&' AS "name" ,'&&KnownsCalcType&' AS "type"),
           Xmlagg (XMLConcat(
             Xmlagg (XMLELEMENT("calculation", xmlconcat(targ,val,res)) order by &&OrderBy& &&DilColumnGroupBy& &&RunIDColumn&),
             XMLELEMENT("statistic", XMLATTRIBUTES(acceptedrun AS "acceptedrun", analyteid AS "analyteid", analyteorder as "analyteorder", species as "species", matrix as "matrix", temperature as "temperature", hours as "hours", longtermtime as "longtermtime", longtermunits as "longtermunits", cycles as "cycles", concentrationunits as "unit") &&RunIDInTarget&,
               XMLELEMENT("flagpercent", min(flagpercent)),
               XMLELEMENT("min-bias", FormatRounded(min(bias), &&DecPlPercBias&)),
               XMLELEMENT("max-bias", FormatRounded(max(bias), &&DecPlPercBias&)),
               XMLELEMENT("mean-bias", FormatRounded(avg(bias), &&DecPlPercBias&)),
               XMLELEMENT("min-biasonmean", FormatRounded(min(biasonmean), &&DecPlPercBias&)),
               XMLELEMENT("max-biasonmean", FormatRounded(max(biasonmean), &&DecPlPercBias&)),
               XMLELEMENT("mean-biasonmean", FormatRounded(avg(biasonmean), &&DecPlPercBias&)),
               XMLELEMENT("min-cv", FormatRounded(min(cv), &&DecPlPercCV&)),
               XMLELEMENT("mean-cv", FormatRounded(avg(cv), &&DecPlPercCV&)),
               XMLELEMENT("max-cv", FormatRounded(max(cv), &&DecPlPercCV&)),
               XMLELEMENT("numacc", sum(cntunflagged)), &&InterLotCvStatistic&
               XMLELEMENT("numtotal", sum(cnt)),
               XMLELEMENT("percacc", case when count(unflagged_bias) != 0 then FmtNum(round(sum(unflagged_bias)/nullif(count(unflagged_bias),0)*100,0)) else null end),
               XMLELEMENT("hours", hours),
               XMLELEMENT("longtermunits", longtermunits),
               XMLELEMENT("longtermtime", longtermtime),
               XMLELEMENT("temperature", temperature)
             )) order by analyteorder, temperature, hours, longtermtime, longtermunits, concentrationunits))
  FROM
   (SELECT analyteorder, nominalconc, min(knownorder) knownorder, species, matrix, temperature, longtermtime, longtermunits, hours, cycles, concentrationunits,
      longtermtime * case when longtermunits='D' then 24 when longtermunits='W' then (7*24) when longtermunits='M' then (30*24) when longtermunits='Y' then (360*24) else 1 end longtermorder,
      Count(case when anchor = 'T' then null else useconc end) as cnt,
      count(case when anchor = 'T' then null else unflagged end) as cntunflagged,
      case
        when LENGTH(TRIM(TRANSLATE(name, ' +-.0123456789', ' '))) is null then to_number(name)
        else null
      end namenum,
      &&ConcRepresentationFunc&(Avg(case when anchor = 'T' then null
                                    when '&&FinalAccuracy&' = 'T' then useconc else useconc_sigrnd
                                    end), &&ConcFigures&) as mean,
      &&ConcRepresentationFunc&(Stddev(case when anchor = 'T' then null
                                       when '&&FinalAccuracy&' = 'T' then useconc else useconc_sigrnd
                                       end), &&ConcSDFigures&) as sd,
      round(case when min(anchor) = 'T' then null
                 when '&&FinalAccuracy&' = 'T' then Stddev(useconc)/nullif(Avg(useconc),0)*100
                 else &&ConcRepresentationFunc&(Stddev(useconc_sigrnd), &&ConcSDFigures&)/nullif(&&ConcRepresentationFunc&(Avg(useconc_sigrnd), &&ConcFigures&),0)*100
            end , &&DecPlPercCV&) as cv,
      round(Avg(case when anchor = 'T' then null when '&&FinalAccuracy&' = 'T' then usebias else usebias_sigrnd end), &&DecPlPercBias&) as bias,
      round(case when '&&FinalAccuracy&' = 'T' then (Avg(case when anchor = 'T' then null else useconc end)-nominalconc)/nullif(nominalconc,0)*100
                           else (&&ConcRepresentationFunc&(Avg(case when anchor = 'T' then null else useconc_sigrnd end), &&ConcFigures&)-nominalconc_sigrnd)/nullif(nominalconc_sigrnd,0)*100
                      end, &&DecPlPercBias&) as biasonmean,
      case when min(anchor) = 'T' then null --anchor
           when abs(Avg(case when '&&FinalAccuracy&' = 'T' then usebias else usebias_sigrnd end)) > min(flagpercent) then 0
           else 1
      end as unflagged_bias,
      min(flagpercent) flagpercent,
      analyteid, name, acceptedrun &&DilColumnGroupBy& &&RunIDColumn&,
      min(anchor) anchor,
      XMLELEMENT("target",
        XMLELEMENT("acceptedrun", acceptedrun),
        XMLELEMENT("analyteid", analyteid),
        XMLELEMENT("species", species),
        XMLELEMENT("matrix", matrix),
        XMLELEMENT("name", NAME),
        XMLELEMENT("nominalconc", &&ConcFormatFunc&(nominalconc_sigrnd, &&ConcFigures&)),
        XMLELEMENT("hours", hours),
        XMLELEMENT("longtermunits", longtermunits),
        XMLELEMENT("longtermtime", longtermtime),
        XMLELEMENT("temperature", temperature),
        XMLELEMENT("cycles", cycles),
        XMLELEMENT("conc", FmtNum(nominalconc)),
        XMLELEMENT("unit", concentrationunits) &&DilTraget& &&RunIDInTarget&) as targ,
      XMLELEMENT("values", Xmlagg(
        XMLELEMENT("sample",
          /*XMLATTRIBUTES(sampleid AS "sampleid", sampleresultid AS "sampleresultid" ),*/ &&RunIDInValues&
          --XMLELEMENT("runsamplesequencenumber", runsamplesequencenumber),
          XMLELEMENT("dilutionfactor", dilutionfactor),
          XMLELEMENT("conctext", conctext),
          XMLELEMENT("concval", &&ConcFormatFunc&(conc_sigrnd,&&ConcFigures&)),
          XMLELEMENT("concentration", case when conctext = 'LLOQ' or (nominalconc_sigrnd < lloqconv and conc is null and analytearea is not null) then '< LLOQ' when conctext = 'ULOQ' or (nominalconc_sigrnd > uloqconv and conc is null and analytearea is not null) then '> ULOQ' else &&ConcFormatFunc&(conc_sigrnd,&&ConcFigures&) end ),
          XMLELEMENT("nominalconc", &&ConcFormatFunc&(nominalconc_sigrnd, &&ConcFigures&)),
          XMLELEMENT("subset", subset),
          XMLELEMENT("bias", FormatRounded(bias_sigrnd, &&DecPlPercBias&)),
          XMLELEMENT("base_mean", &&ConcFormatFunc&(base_mean,&&ConcFigures&)),
          XMLELEMENT("difference", FormatRounded(round(case when base_mean = 0 or cycles = '1' then null
                                                    when '&&FinalAccuracy&' = 'T' then (conc-base_mean)/nullif(base_mean,0)*100
                                                    else (&&ConcRepresentationFunc&(conc_sigrnd, &&ConcFigures&)-base_mean)/nullif(base_mean,0)*100
                                               end, &&DecPlPercBias&), &&DecPlPercBias&)),
          XMLELEMENT("accuracy", FormatRounded(accuracy_sigrnd,&&DecPlPercBias&)),
          XMLELEMENT("cv", FormatRounded(cv_sigrnd, &&DecPlPercCV&)),
          XMLELEMENT("rsd", FormatRounded(rsd_sigrnd, &&DecPlPercCV&)),
          XMLELEMENT("analytearea", &&ConcFormatFunc&(&&ConcRepresentationFunc&(analytearea,&&ConcFigures&),&&ConcFigures&)),
          XMLELEMENT("flag", flagged),
          XMLELEMENT("cvflag", cvflagged),
          XMLELEMENT("flagpercent", flagpercent),
          XMLELEMENT("cvflagpercent", cvflagpercent),
          XMLELEMENT("lloq", lloqconv),
          XMLELEMENT("uloq", uloqconv),
          XMLELEMENT("deactivated", isdeactivated),
          XMLELEMENT("samples", xml)
          --&&DilValue&
          )
          order by runnr--, runsamplesequencenumber
          )) as val,
      XMLELEMENT("result",
        XMLELEMENT("n", Count(useconc)),
        XMLELEMENT("mean", &&ConcFormatFunc&(&&ConcRepresentationFunc&(Avg(
                           case when '&&FinalAccuracy&' = 'T' then useconc else useconc_sigrnd
                           end), &&ConcFigures&), &&ConcFigures&)),
        XMLELEMENT("sd", case when Count(useconc) < 2 then null else &&ConcFormatFunc&(&&ConcRepresentationFunc&(Stddev(
                        case when '&&FinalAccuracy&' = 'T' then useconc
                        else useconc_sigrnd
                        end), &&ConcSDFigures&), &&ConcSDFigures&) end),
        XMLELEMENT("cv", case when Count(useconc) < 2 then null when '&&FinalAccuracy&' = 'T' then FormatRounded(round(Stddev(useconc)/nullif(Avg(useconc),0)*100, &&DecPlPercCV&), &&DecPlPercCV&)
                         else FormatRounded(round(&&ConcRepresentationFunc&(Stddev(useconc_sigrnd), &&ConcFigures&)/nullif(&&ConcRepresentationFunc&(Avg(useconc_sigrnd), &&ConcFigures&),0)*100, &&DecPlPercCV&), &&DecPlPercCV&)
                         end),
        XMLELEMENT("rsd", case when Count(useconc) < 2 then null when '&&FinalAccuracy&' = 'T' then FormatRounded(round(Stddev(useconc)/nullif(Abs(Avg(useconc)),0)*100, &&DecPlPercCV&), &&DecPlPercCV&)
                         else FormatRounded(round(&&ConcRepresentationFunc&(Stddev(useconc_sigrnd), &&ConcFigures&)/nullif(&&ConcRepresentationFunc&(Abs(Avg(useconc_sigrnd)), &&ConcFigures&),0)*100, &&DecPlPercCV&), &&DecPlPercCV&)
                         end), &&InterLotCvResult&
        XMLELEMENT("bias", FormatRounded(round(Avg(case when '&&FinalAccuracy&' = 'T' then usebias else usebias_sigrnd end), &&DecPlPercBias&), &&DecPlPercBias&)),
        XMLELEMENT("biasonmean", FormatRounded(round(case when '&&FinalAccuracy&' = 'T' then (Avg(useconc)-nominalconc)/nullif(nominalconc,0)*100
                                                    else (&&ConcRepresentationFunc&(Avg(useconc_sigrnd), &&ConcFigures&)-nominalconc_sigrnd)/nullif(nominalconc_sigrnd,0)*100
                                               end, &&DecPlPercBias&), &&DecPlPercBias&)),
        XMLELEMENT("difference", FormatRounded(round(case when min(base_mean) = 0 or cycles = '1' then null
                                                    when '&&FinalAccuracy&' = 'T' then (Avg(useconc)-min(base_mean))/nullif(min(base_mean),0)*100
                                                    else (&&ConcRepresentationFunc&(Avg(useconc_sigrnd), &&ConcFigures&)-min(base_mean))/nullif(min(base_mean),0)*100
                                               end, &&DecPlPercBias&), &&DecPlPercBias&)),
        XMLELEMENT("accuracy", FormatRounded(round(case when '&&FinalAccuracy&' = 'T' then ((Avg(useconc)-nominalconc)/nullif(nominalconc,0)*100)+100
                                                    else ((&&ConcRepresentationFunc&(Avg(useconc_sigrnd), &&ConcFigures&)-nominalconc_sigrnd)/nullif(nominalconc_sigrnd,0)*100)+100
                                               end, &&DecPlPercBias&), &&DecPlPercBias&)),
        XMLELEMENT("totalerror", FormatRounded(round(case when Count(useconc) < 2 then null when '&&FinalAccuracy&' = 'T' then Abs((Avg(useconc)-nominalconc)/nullif(nominalconc,0)*100) +
                                                                                       FormatRounded(round(Stddev(useconc)/nullif(Avg(useconc),0)*100, &&DecPlPercCV&), &&DecPlPercCV&)
                                                    else Abs((&&ConcRepresentationFunc&(Avg(useconc_sigrnd), &&ConcFigures&)-nominalconc_sigrnd)/nullif(nominalconc_sigrnd,0)*100) +
                                                         FormatRounded(round(&&ConcRepresentationFunc&(Stddev(useconc_sigrnd), &&ConcFigures&)/nullif(&&ConcRepresentationFunc&(Avg(useconc_sigrnd), &&ConcFigures&),0)*100, &&DecPlPercCV&), &&DecPlPercCV&)
                                               end, &&DecPlPercTE&), &&DecPlPercTE&)),
        XMLELEMENT("flagged",
          case when abs(round(case when nominalconc = 0 then null
                                   when '&&FinalAccuracy&' = 'T' then (Avg(useconc)-nominalconc)/nominalconc*100
                                   else (&&ConcRepresentationFunc&(Avg(useconc_sigrnd), &&ConcFigures&)-nominalconc_sigrnd)/nominalconc_sigrnd*100
                              end, &&DecPlPercBias&)) > min(flagpercent) then 'T'
               else 'F'
          end),
        XMLELEMENT("cvflagged",
          case when abs(case when '&&FinalAccuracy&' = 'T' then Stddev(useconc)/nullif(Avg(useconc),0)*100
                             else round(&&ConcRepresentationFunc&(Stddev(useconc_sigrnd), &&ConcFigures&)/nullif(&&ConcRepresentationFunc&(Avg(useconc_sigrnd), &&ConcFigures&),0)*100, &&DecPlPercCV&)
                        end) > min(cvflagpercent) then 'T'
               else 'F'
          end),
        XMLELEMENT("flagpercent", min(flagpercent)),
        XMLELEMENT("cvflagpercent", min(cvflagpercent)),
        XMLELEMENT("analytearea-n", Count(useanalytearea)),
        XMLELEMENT("analytearea-mean", &&ConcFormatFunc&(&&ConcRepresentationFunc&(Avg(
                           case when '&&FinalAccuracy&' = 'T' then useanalytearea else useanalytearea_sigrnd
                           end), &&ConcFigures&), &&ConcFigures&)),
        XMLELEMENT("analytearea-sd", &&ConcFormatFunc&(&&ConcRepresentationFunc&(Stddev(
                        case when '&&FinalAccuracy&' = 'T' then useanalytearea else useanalytearea_sigrnd
                        end), &&ConcSDFigures&), &&ConcSDFigures&)),
        XMLELEMENT("analytearea-cv", case when '&&FinalAccuracy&' = 'T' then FormatRounded(round(Stddev(useanalytearea)/nullif(Avg(useanalytearea),0)*100, &&DecPlPercCV&), &&DecPlPercCV&)
                         else FormatRounded(round(&&ConcRepresentationFunc&(Stddev(useanalytearea_sigrnd), &&ConcFigures&)/nullif(&&ConcRepresentationFunc&(Avg(useanalytearea_sigrnd), &&ConcFigures&),0)*100, &&DecPlPercCV&), &&DecPlPercCV&)
                         end),
        case when min(knowntype) = 'STANDARD' then XMLCONCAT(XMLELEMENT("anchor",min(anchor)),XMLELEMENT("at_lloq",min(at_lloq)),XMLELEMENT("at_uloq",min(at_uloq))) else null end
        ) as res
    FROM
    (SELECT
     name, subset, &&InterLotCvCalc&
     nominalconc, nominalconc_sigrnd, analyteid, dilutionfactor,--&&DilColumn&,
     lloqconv, uloqconv, knowntype, isdeactivated,
     hours, longtermtime, longtermunits, temperature, cycles, concentrationunits,
     anchor, at_lloq, at_uloq, analyteorder, knownorder, species, matrix, flagpercent, cvflagpercent, acceptedrun, runid, runnr, 
     case when '&&ExcludeLLOQULOQFromStats&'='F' then null -- disable the LLOQ/LLOQ-feature, treat as numbers
          when '&&FinalAccuracy&'='T' and (conc / dilutionfactor) < lloqconv then 'LLOQ'
          when '&&FinalAccuracy&'='T' and (conc / dilutionfactor) > uloqconv then 'ULOQ'
          when (conc_sigrnd / dilutionfactor) < lloqconv then 'LLOQ'
          when (conc_sigrnd / dilutionfactor) > uloqconv then 'ULOQ'
          else null
     end conctext,
     -- Full precision
     case when conctext is null then conc else null end conc,
     case when conctext is null then bias else null end bias,
     case when conctext is null then cv else null end cv,
     analytearea,
     case when conctext is null then useconc end useconc,
     case when conctext is null then usebias end usebias,
     useanalytearea,
     -- Sig/Rnd
     case when conctext is null then conc_sigrnd else null end conc_sigrnd,
     case when conctext is null then bias_sigrnd else null end bias_sigrnd,
     case when conctext is null then cv_sigrnd else null end cv_sigrnd,
     case when conctext is null then rsd_sigrnd else null end rsd_sigrnd,
     case when conctext is null then accuracy_sigrnd else null end accuracy_sigrnd,
     analytearea_sigrnd,
     case when conctext is null then useconc_sigrnd else null end useconc_sigrnd,
     case when conctext is null then usebias_sigrnd else null end usebias_sigrnd,
     case when conctext is null then useaccuracy_sigrnd else null end useaccuracy_sigrnd,
     useanalytearea_sigrnd,
     case when conctext is null then flagged else null end flagged,
     case when conctext is null then unflagged else null end unflagged,
     case when conctext is null then cvflagged else null end cvflagged,
     xml, nByRun,
     Avg(
       case when ISDEACTIVATED = 'T' or cycles != '1' then null
            else case when '&&FinalAccuracy&' = 'T' then conc else conc_sigrnd end end
      )
      over (partition by analyteid, analyteorder, species, matrix, name, nominalconc, nominalconc_sigrnd, temperature, hours, longtermtime, longtermunits, concentrationunits, acceptedrun &&DilColumnGroupBy&, runid) base_mean
    FROM
    (SELECT
     name, subset, &&InterLotCvCalc&
     nominalconc, nominalconc_sigrnd, analyteid, dilutionfactor,--&&DilColumn&,
     lloqconv, uloqconv, knowntype, max(isdeactivated) isdeactivated,
     hours, longtermtime, longtermunits, temperature, cycles, concentrationunits,
     case when knowntype = 'STANDARD' and (nominalconc_sigrnd<&&ConcRepresentationFunc&(lloqconv, &&ConcFigures&) or nominalconc_sigrnd>&&ConcRepresentationFunc&(uloqconv, &&ConcFigures&)) then 'T' else 'F' end anchor,
     case when knowntype = 'STANDARD' and (nominalconc_sigrnd=&&ConcRepresentationFunc&(lloqconv, &&ConcFigures&)) then 'T' else 'F' end at_lloq,
     case when knowntype = 'STANDARD' and (nominalconc_sigrnd=&&ConcRepresentationFunc&(uloqconv, &&ConcFigures&)) then 'T' else 'F' end at_uloq,
     analyteorder, min(knownorder) knownorder, species, matrix, flagpercent, cvflagpercent, acceptedrun, runid, runnr, 
     case when '&&ExcludeLLOQULOQFromStats&'='F' then null -- disable the LLOQ/LLOQ-feature, treat as numbers
          when '&&FinalAccuracy&'='T' and Avg(conc / dilutionfactor) < lloqconv then 'LLOQ'
          when '&&FinalAccuracy&'='T' and Avg(conc / dilutionfactor) > uloqconv then 'ULOQ'
          when Avg(conc_sigrnd / dilutionfactor) < lloqconv then 'LLOQ'
          when Avg(conc_sigrnd / dilutionfactor) > uloqconv then 'ULOQ'
          else null
     end conctext,
     -- Full precision
     Avg(conc) conc,
     (Avg(conc)-nominalconc)*100/nullif(nominalconc,0) bias,
     Stddev(conc)/nullif(Avg(conc),0)*100 cv,
     Avg(analytearea) analytearea,
     Avg(case when ISDEACTIVATED = 'T' then null else conc end) useconc,
     (Avg(case when ISDEACTIVATED = 'T' then null else conc end)-nominalconc)*100/nullif(nominalconc,0) usebias,
     Avg(case when ISDEACTIVATED = 'T' then null else analytearea end) useanalytearea,
     -- Sig/Rnd
     &&ConcRepresentationFunc&(Avg(conc), &&ConcFigures&) conc_sigrnd,
     (&&ConcRepresentationFunc&(Avg(conc), &&ConcFigures&)-nominalconc_sigrnd)*100/nullif(nominalconc_sigrnd,0) bias_sigrnd,
     round(&&ConcRepresentationFunc&(Stddev(conc), &&ConcFigures&)/nullif(&&ConcRepresentationFunc&(Avg(conc), &&ConcFigures&),0)*100, &&DecPlPercCV&) cv_sigrnd,
     round(&&ConcRepresentationFunc&(Stddev(conc), &&ConcFigures&)/nullif(&&ConcRepresentationFunc&(Abs(Avg(conc)), &&ConcFigures&),0)*100, &&DecPlPercCV&) rsd_sigrnd,
     round(&&ConcRepresentationFunc&(Avg(conc), &&ConcFigures&)*100/nullif(nominalconc_sigrnd,0),&&DecPlPercBias&) accuracy_sigrnd,
     &&ConcRepresentationFunc&(Avg(analytearea), &&ConcFigures&) analytearea_sigrnd,
     &&ConcRepresentationFunc&(Avg(case when ISDEACTIVATED = 'T' then null else conc end), &&ConcFigures&) useconc_sigrnd,
     (&&ConcRepresentationFunc&(Avg(case when ISDEACTIVATED = 'T' then null else conc end), &&ConcFigures&)-nominalconc_sigrnd)*100/nullif(nominalconc_sigrnd,0) usebias_sigrnd,
     round(&&ConcRepresentationFunc&(Avg(case when ISDEACTIVATED = 'T' then null else conc end), &&ConcFigures&)*100/nullif(nominalconc_sigrnd,0),&&DecPlPercBias&) useaccuracy_sigrnd,
     &&ConcRepresentationFunc&(Avg(case when ISDEACTIVATED = 'T' then null else analytearea end), &&ConcFigures&) useanalytearea_sigrnd,
     case when abs(
       case
         when '&&FinalAccuracy&' = 'T' then
           (Avg(case when ISDEACTIVATED = 'T' then null else conc end)-nominalconc)*100/nullif(nominalconc,0)
         else
           (&&ConcRepresentationFunc&(Avg(case when ISDEACTIVATED = 'T' then null else conc_sigrnd end), &&ConcFigures&)-nominalconc_sigrnd)*100/nullif(nominalconc_sigrnd,0)
       end) > flagpercent then '&&FlaggedSymbol&' else null end flagged,
     case when min(ISDEACTIVATED) <> 'T' and abs(
       case
         when '&&FinalAccuracy&' = 'T' then
           (Avg(case when ISDEACTIVATED = 'T' then null else conc end)-nominalconc)*100/nullif(nominalconc,0)
         else
           (&&ConcRepresentationFunc&(Avg(case when ISDEACTIVATED = 'T' then null else conc_sigrnd end), &&ConcFigures&)-nominalconc_sigrnd)*100/nullif(nominalconc_sigrnd,0)
       end) <= flagpercent then 1 else null end unflagged,
     case when abs(
       case
         when '&&FinalAccuracy&' = 'T' then
           Stddev(case when ISDEACTIVATED = 'T' then null else conc end)/nullif(Avg(case when ISDEACTIVATED = 'T' then null else conc end),0)*100
         else
           round(&&ConcRepresentationFunc&(Stddev(case when ISDEACTIVATED = 'T' then null else conc_sigrnd end), &&ConcFigures&)/nullif(&&ConcRepresentationFunc&(Avg(case when ISDEACTIVATED = 'T' then null else conc_sigrnd end), &&ConcFigures&),0)*100, &&DecPlPercCV&)
       end) > cvflagpercent then '&&FlaggedSymbolCV&' else null end cvflagged,
     Xmlagg(
            XMLELEMENT("sample",
              XMLATTRIBUTES(sampleid AS "sampleid", sampleresultid AS "sampleresultid" ),
              XMLELEMENT("runsamplesequencenumber", runsamplesequencenumber),
              XMLELEMENT("replicatenumber", replicatenumber),
              XMLELEMENT("resultcommenttext", resultcommenttext),
              XMLELEMENT("conc", FmtNum(conc_sigrnd)),
              XMLELEMENT("concval", FmtNum(conc)),
              XMLELEMENT("conctext", conctext),
              XMLELEMENT("analytearea", FmtNum(analytearea)),
              XMLELEMENT("deactivated", isdeactivated),
              case
                when isr_deactivation = 'T' then
                  XMLELEMENT("deactivationreason", deactivationreason)
                else null
              end
            ) order by runsamplesequencenumber
          ) xml,
     Count(1)  nByRun
     FROM
     (SELECT K.ID knownID,
        s.ID sampleID, sr.ID sampleresultID,
        &&KnownNameTargetField& name,
        k.namesuffix subset,
        case when ra.RunAnalyteRegressionStatus = &&RejectedRunAnalyte& then 'F' else 'T' end acceptedrun,
        case
          when d.code is not null then 'T'
          else s.ISDEACTIVATED
        end ISDEACTIVATED,
        case
          when d.code is not null then 'T'
          else 'F'
        end isr_deactivation,
        d.reason deactivationreason,
        s.dilutionfactor,--&&DilColumnQualif&
        s.runsamplesequencenumber, s.replicatenumber,
        ra.analyteid, sa.analyteorder, a.species, a.sampletypeid matrix,
        kwz.ordernumber knownorder,
        &&ConcRepresentationFunc&(sr.nominalconcentrationconv, &&ConcFigures&) nominalconc_sigrnd,
        sr.nominalconcentrationconv nominalconc, sr.resultcommenttext,
        ra.lloqconv, ra.uloqconv, K.knowntype,
        case when sr.nominalconcentrationconv = ra.lloqconv then &&FlagPercentBiasLLOQ&
             when sr.nominalconcentrationconv = ra.uloqconv then &&FlagPercentBiasULOQ&
             when sr.nominalconcentrationconv not between ra.lloqconv and ra.uloqconv and '&&ExcludeAnchorsFromFlags&' = 'T' then null
             else &&FlagPercentBias&
        end flagpercent,
        case when sr.nominalconcentrationconv = ra.lloqconv then &&FlagPercentCvLLOQ&
             when sr.nominalconcentrationconv = ra.uloqconv then &&FlagPercentCvULOQ&
             when sr.nominalconcentrationconv not between ra.lloqconv and ra.uloqconv and '&&ExcludeAnchorsFromFlags&' = 'T' then null
             else &&FlagPercentCV&
        end cvflagpercent,
        s.runid, r.runid runnr,
        sr.concentrationconv conc, &&ConcRepresentationFunc&(sr.concentrationconv, &&ConcFigures&) conc_sigrnd,
        srw.analytearea, &&ConcRepresentationFunc&(srw.analytearea, &&ConcFigures&) analytearea_sigrnd, &&StabilityInfo&
        case when '&&ExcludeLLOQULOQFromStats&'='F' then null -- disable the LLOQ/LLOQ-feature, treat as numbers
             when sr.concentrationconv/nullif(s.dilutionfactor,0) < ra.lloqconv then '< LLOQ'
             when sr.concentrationconv/nullif(s.dilutionfactor,0) > ra.uloqconv then '> ULOQ'
             else null
        end conctext,
        ra.concentrationunits
      FROM &&TempTabPrefix&bio$run$ana$known K,
           &&TempTabPrefix&bio$run r,
           &&TempTabPrefix&bio$run$analytes ra,
           &&TempTabPrefix&bio$study$analytes sa,
           &&TempTabPrefix&bio$assay a,
           &&TempTabPrefix&bio$run$samples s,
           &&TempTabPrefix&bio$run$sample$results sr,
           &&TempTabPrefix&bio$run$sample$result$raw srw,
           &&TempTabPrefix&bio$covib$run$sample$deact d,
           isr$crit kwz,
           table(&&LALPackage&.GetRunStates) rs
      WHERE K.ID = s.runknownID
        AND R.ID = s.runid AND R.ID = sr.runid (+)
        AND r.RuntypeDescription in (&&RunTypes&)
        AND s.ID = sr.runsampleID
        AND K.knowntype = '&&KnownType&'
        AND s.sampletype = 'known'
        AND s.runsamplekind = '&&KnownType&'
        AND K.knowntype = s.runsamplekind
        AND ra.id = sr.runanalyteid (+)
        AND a.id = r.assayid
        AND sa.id = ra.analyteid
        AND ra.runid = s.runid
        AND K.studyid = '&&StudyID&'
        AND s.studyid = '&&StudyID&'
        AND sr.studyID = '&&StudyID&' AND r.studyid = '&&StudyID&'
        AND ra.studyID = '&&StudyID&'
        AND s.studycode || '-' || r.runid || '-' || s.runsamplesequencenumber = d.code (+)
        AND s.ID = srw.runSampleID (+)
        AND rs.column_value = r.runstatusnum
        AND kwz.key = &&KnownNameJoinField&
        AND kwz.entity = '&&KnownEntity&'
        AND kwz.masterkey = '&&ExpMasterkey&'
        AND repid = &&RepID&
        &&DilCond&
        &&AddCondition&
     )
     GROUP BY analyteid, analyteorder, species, matrix, name, nominalconc, nominalconc_sigrnd, dilutionfactor, temperature, hours, longtermtime, longtermunits, cycles, concentrationunits, acceptedrun, runid, runnr, knowntype, lloqconv, uloqconv, flagpercent, cvflagpercent&&DilColumn&, subset &&InterLotCvCalc&
    ))
    &&GroupingConditions&
    GROUP BY analyteid, analyteorder, species, matrix, name, nominalconc, nominalconc_sigrnd, temperature, hours, longtermtime, longtermunits, cycles, concentrationunits, acceptedrun &&DilColumnGroupBy& &&RunIDColumn&)
    GROUP BY analyteid, analyteorder, species, matrix, temperature, hours, longtermorder, longtermtime, longtermunits, cycles, concentrationunits, acceptedrun &&RunIDColumn&
