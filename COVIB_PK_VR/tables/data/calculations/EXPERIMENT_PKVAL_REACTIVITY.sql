  SELECT XMLELEMENT("experiment", XMLATTRIBUTES('&&Experiment&' AS "name" ,'reactivity' AS "type"),
           Xmlagg (XMLConcat(
             Xmlagg (XMLELEMENT("calculation", xmlconcat(targ,val,res)) order by /*assayidentifier, analyteindex, knownorder, */ acceptedrun, /*runid, */&&GroupSet& drug_conc, source),
             XMLELEMENT("statistic", XMLATTRIBUTES(acceptedrun AS "acceptedrun", analyteid AS "analyteid", analyteorder as "analyteorder", species as "species", matrix as "matrix", source as "source") ,
               XMLELEMENT("mean_recalc", &&ConcFormatFunc&(&&ConcRepresentationFunc&(Min(overall_useconc_recalc),&&ConcFigures&),&&ConcFigures&)),
               XMLELEMENT("mean", &&ConcFormatFunc&(&&ConcRepresentationFunc&(Min(overall_useconc),&&ConcFigures&),&&ConcFigures&)),
               --XMLELEMENT("bias", FormatRounded(round((&&ConcRepresentationFunc&(Min(overall_useconc),&&ConcFigures&)-source)/NULLIF(source,0)*100,&&DecPlPercBias&),&&DecPlPercBias&)),
               XMLELEMENT("sd", &&ConcFormatFunc&(&&ConcRepresentationFunc&(Min(overall_sd),&&ConcSDFigures&),&&ConcSDFigures&)),
               XMLELEMENT("cv", FormatRounded(round(&&ConcRepresentationFunc&(Min(overall_sd),&&ConcFigures&)/NULLIF(&&ConcRepresentationFunc&(Min(overall_useconc),&&ConcFigures&),0)*100,&&DecPlPercCV&),&&DecPlPercCV&))
             )) order by analyteorder
           )
         )
  FROM
   (SELECT
      acceptedrun, analyteorder, species, matrix,
      min(flagpercent) flagpercent,
      analyteid, source, drug_conc, &&GroupSet& lloqconv, uloqconv,
      Avg(case
        when '&&ExcludeLLOQULOQFromStats&'!='F' and overall_mean_useconc_recalc not between lloqconv and uloqconv then null
        else &&ConcRepresentationFunc&(overall_mean_useconc_recalc,&&ConcFigures&)
      end) overall_useconc_recalc,
      Avg(case
        when '&&ExcludeLLOQULOQFromStats&'!='F' and overall_mean_useconc_recalc not between lloqconv and uloqconv then null
        else &&ConcRepresentationFunc&(overall_mean_useconc,&&ConcFigures&)
      end) overall_useconc,
      Stddev(case
        when '&&ExcludeLLOQULOQFromStats&'!='F' and overall_mean_useconc_recalc not between lloqconv and uloqconv then null
        else &&ConcRepresentationFunc&(overall_mean_useconc,&&ConcFigures&)
      end) overall_sd,
      Min(overall_mean_useconc) overall_useconcval,
      XMLELEMENT("target",
        XMLELEMENT("acceptedrun", acceptedrun),
        XMLELEMENT("analyteid", analyteid),
        XMLELEMENT("species", species),
        XMLELEMENT("matrix", matrix),
        XMLELEMENT("source", source),
        XMLELEMENT("drug_conc", drug_conc),
        &&TargetSet&) as targ,
      XMLELEMENT("values", Xmlagg(
        XMLELEMENT("sample", --XMLATTRIBUTES(sampleid AS "sampleid", sampleresultid AS "sampleresultid" ),
          --XMLELEMENT("runsamplesequencenumber", runsamplesequencenumber),
          XMLELEMENT("runid", runid),
          XMLELEMENT("runno", runnr),
          XMLELEMENT("nominalconc", &&ConcFormatFunc&(nominalconc_sigrnd,&&ConcFigures&)),
          XMLELEMENT("duplicate", duplicate),
          XMLELEMENT("dilutionfactor", dilutionfactor),
          XMLELEMENT("conctext", conctext),
          XMLELEMENT("concval", &&ConcFormatFunc&(&&ConcRepresentationFunc&(case when '&&FinalAccuracy&'='T' then mean_conc else mean_conc_sigrnd end,&&ConcFigures&),&&ConcFigures&)),
          XMLELEMENT("concentration", case when conctext = 'LLOQ' or (nominalconc_sigrnd < lloqconv and mean_conc is null and mean_response is not null) then '< LLOQ'
                                           when conctext = 'ULOQ' or (nominalconc_sigrnd > uloqconv and mean_conc is null and mean_response is not null) then '> ULOQ'
                                           else &&ConcFormatFunc&(&&ConcRepresentationFunc&(case when '&&FinalAccuracy&'='T' then mean_conc else mean_conc_sigrnd end,&&ConcFigures&),&&ConcFigures&)
                                      end),
          XMLELEMENT("recalc-concval", &&ConcFormatFunc&(&&ConcRepresentationFunc&(case when '&&FinalAccuracy&'='T' then mean_conc_recalc else mean_conc_recalc_sigrnd end,&&ConcFigures&),&&ConcFigures&)),
          XMLELEMENT("recalc-concentration", case when conctext = 'LLOQ' then '< LLOQ' when conctext = 'ULOQ' then '> ULOQ'
                                                  else &&ConcFormatFunc&(&&ConcRepresentationFunc&(case when '&&FinalAccuracy&'='T' then mean_conc_recalc else mean_conc_recalc_sigrnd end,&&ConcFigures&),&&ConcFigures&)
                                             end),
          XMLELEMENT("response", &&ConcFormatFunc&(&&ConcRepresentationFunc&(case when '&&FinalAccuracy&'='T' then mean_response else mean_response_sigrnd end,&&ConcFigures&),&&ConcFigures&)),
          XMLELEMENT("lloq", lloqconv),
          XMLELEMENT("uloq", uloqconv),
          XMLELEMENT("cv", FormatRounded(cv,&&DecPlPercBias&)),
          XMLELEMENT("bias", FormatRounded(biasonmean,&&DecPlPercBias&)),
          XMLELEMENT("flag", case when flagged is not null then flagged else null end),
          XMLELEMENT("cvflag", case when cvflagged is not null then cvflagged else null end),
          XMLELEMENT("deactivated", min_isdeactivated),
          XMLELEMENT("overall_recalc-useconc",
            case
              when '&&ExcludeLLOQULOQFromStats&'!='F' and overall_mean_useconc_recalc < lloqconv then '< LLOQ'
              when '&&ExcludeLLOQULOQFromStats&'!='F' and overall_mean_useconc_recalc > uloqconv then '> ULOQ'
              else &&ConcFormatFunc&(&&ConcRepresentationFunc&(overall_mean_useconc_recalc,&&ConcFigures&),&&ConcFigures&)
            end
          ),
          XMLELEMENT("overall_recalc-useconcval", &&ConcFormatFunc&(&&ConcRepresentationFunc&(overall_mean_useconc_recalc,&&ConcFigures&),&&ConcFigures&)),
          XMLELEMENT("overall_useconctext",
            case
              when '&&ExcludeLLOQULOQFromStats&'!='F' and overall_mean_useconc_recalc < lloqconv then 'LLOQ'
              when '&&ExcludeLLOQULOQFromStats&'!='F' and overall_mean_useconc_recalc > uloqconv then 'ULOQ'
              else null
            end
          ),
          XMLELEMENT("overall_useconc",
            case
              when '&&ExcludeLLOQULOQFromStats&'!='F' and overall_mean_useconc_recalc < lloqconv then '< LLOQ'
              when '&&ExcludeLLOQULOQFromStats&'!='F' and overall_mean_useconc_recalc > uloqconv then '> ULOQ'
              else &&ConcFormatFunc&(&&ConcRepresentationFunc&(overall_mean_useconc,&&ConcFigures&),&&ConcFigures&)
            end
          ),
          XMLELEMENT("overall_concval", &&ConcFormatFunc&(&&ConcRepresentationFunc&(overall_mean_useconc,&&ConcFigures&),&&ConcFigures&)),
          /*XMLELEMENT("overall_bias",
            case
              when '&&ExcludeLLOQULOQFromStats&'!='F' and overall_mean_useconc_recalc not between lloqconv and uloqconv then null
              else FormatRounded(round((&&ConcRepresentationFunc&(overall_mean_useconc,&&ConcFigures&)-source)/NULLIF(source,0)*100,&&DecPlPercBias&),&&DecPlPercBias&)
            end
          ),*/
          XMLELEMENT("overall_sd",
            case
              when '&&ExcludeLLOQULOQFromStats&'!='F' and overall_mean_useconc_recalc not between lloqconv and uloqconv then null
              else &&ConcFormatFunc&(&&ConcRepresentationFunc&(overall_sd,&&ConcSDFigures&),&&ConcSDFigures&)
            end
          ),
          XMLELEMENT("overall_cv",
            case
              when '&&ExcludeLLOQULOQFromStats&'!='F' and overall_mean_useconc_recalc not between lloqconv and uloqconv then null
              else FormatRounded(round(&&ConcRepresentationFunc&(overall_sd,&&ConcFigures&)/NULLIF(&&ConcRepresentationFunc&(overall_mean_useconc,&&ConcFigures&),0)*100,&&DecPlPercCV&),&&DecPlPercCV&)
            end
          ),
          /*XMLELEMENT("overall_flag",
            case when abs(case
              when '&&ExcludeLLOQULOQFromStats&'!='F' and overall_mean_useconc_recalc not between lloqconv and uloqconv then null
              else round((&&ConcRepresentationFunc&(overall_mean_useconc,&&ConcFigures&)-source)/NULLIF(source,0)*100,&&DecPlPercBias&)
            end) > flagpercent then '&&FlaggedSymbol&' else null end
          ),*/
          XMLELEMENT("overall_cvflag",
            case when abs(case
              when '&&ExcludeLLOQULOQFromStats&'!='F' and overall_mean_useconc_recalc not between lloqconv and uloqconv then null
              else round(&&ConcRepresentationFunc&(overall_sd,&&ConcFigures&)/NULLIF(&&ConcRepresentationFunc&(overall_mean_useconc,&&ConcFigures&),0)*100,&&DecPlPercCV&)
            end) > cvflagpercent then '&&FlaggedSymbolCV&' else null end
          ),
          XMLELEMENT("flagpercent", flagpercent),
          XMLELEMENT("cvflagpercent", cvflagpercent),
          XMLELEMENT("samples", xml)
        )
        order by runnr, nominalconc, duplicate, dilutionfactor
      )) as val,
      XMLELEMENT("result",
        XMLELEMENT("n", Count(mean_useconc)),
        XMLELEMENT("mean_response", &&ConcFormatFunc&(&&ConcRepresentationFunc&(Avg(
                           case when '&&FinalAccuracy&'='T' then mean_response else mean_response_sigrnd
                           end),&&ConcFigures&),&&ConcFigures&)),
        XMLELEMENT("mean_conc_recalc", &&ConcFormatFunc&(&&ConcRepresentationFunc&(Avg(
                           case when '&&FinalAccuracy&'='T' then mean_useconc_recalc else mean_useconc_recalc_sigrnd
                           end),&&ConcFigures&),&&ConcFigures&)),
        XMLELEMENT("mean_conc", &&ConcFormatFunc&(&&ConcRepresentationFunc&(Avg(
                           case when '&&FinalAccuracy&'='T' then mean_useconc else mean_useconc_sigrnd
                           end),&&ConcFigures&),&&ConcFigures&)),
        XMLELEMENT("sd", case when Count(mean_useconc) < 2 then null else &&ConcFormatFunc&(&&ConcRepresentationFunc&(Stddev(
                        case when '&&FinalAccuracy&'='T' then mean_useconc
                        else mean_useconc_sigrnd
                        end),&&ConcSDFigures&),&&ConcSDFigures&) end),
        XMLELEMENT("cv", case when Count(mean_useconc) < 2 then null when '&&FinalAccuracy&'='T' then FormatRounded(round(Stddev(mean_useconc)/NULLIF(Avg(mean_useconc),0)*100,&&DecPlPercCV&),&&DecPlPercCV&)
                         else FormatRounded(round(&&ConcRepresentationFunc&(Stddev(mean_useconc_sigrnd),&&ConcFigures&)/NULLIF(&&ConcRepresentationFunc&(Avg(mean_useconc_sigrnd),&&ConcFigures&),0)*100,&&DecPlPercCV&),&&DecPlPercCV&)
                         end),
        /*XMLELEMENT("biasonmean", FormatRounded(round(case when nominalconc = 0 then null
                                                    when '&&FinalAccuracy&'='T' then (Avg(mean_useconc)-nominalconc)/nominalconc*100
                                                    else (&&ConcRepresentationFunc&(Avg(mean_useconc_sigrnd),&&ConcFigures&)-nominalconc_sigrnd)/nominalconc_sigrnd*100
                                               end,&&DecPlPercBias&),&&DecPlPercBias&)),*/
        XMLELEMENT("flagpercent", min(flagpercent)),
        XMLELEMENT("cvflagpercent", min(cvflagpercent))
        ) as res
    FROM
    (SELECT
      acceptedrun, analyteid, analyteorder, species, matrix, runid, runnr, source, drug_conc, duplicate, dilutionfactor, lloqconv, uloqconv, nominalconc, nominalconc_sigrnd, flagpercent, cvflagpercent, sset,
      mean_response, mean_response_sigrnd, conctext, mean_conc_recalc, mean_conc_recalc_sigrnd, mean_conc, mean_conc_sigrnd,
      case when conctext is null then mean_useconc_recalc else null end mean_useconc_recalc,
      case when conctext is null then mean_useconc_recalc_sigrnd else null end mean_useconc_recalc_sigrnd,
      case when conctext is null then mean_useconc else null end mean_useconc,
      case when conctext is null then mean_useconc_sigrnd else null end mean_useconc_sigrnd,
      min_isdeactivated, /*mean_first_mean, */flagged, cvflagged,
      case when conctext is null then biasonmean else null end biasonmean,
      case when conctext is null then cv else null end cv,
      Avg(case
               when '&&FinalAccuracy&'='T' then mean_useconc_recalc
               else mean_useconc_recalc_sigrnd
          end)
        over (partition by acceptedrun, analyteid, analyteorder, species, matrix, source, runid, duplicate, dilutionfactor, nominalconc) overall_mean_useconc_recalc,
      Avg(case
               when '&&FinalAccuracy&'='T' then mean_useconc
               else mean_useconc_sigrnd
          end)
        over (partition by acceptedrun, analyteid, analyteorder, species, matrix, source, runid, duplicate, dilutionfactor, nominalconc) overall_mean_useconc,
      Stddev(case
               when '&&FinalAccuracy&'='T' then mean_useconc
               else mean_useconc_sigrnd
             end)
        over (partition by acceptedrun, analyteid, analyteorder, species, matrix, source, runid, duplicate, dilutionfactor, nominalconc) overall_sd,
      xml
    FROM
    (SELECT
      acceptedrun, analyteid, analyteorder, species, matrix, runid, runnr, source, drug_conc, duplicate, dilutionfactor, lloqconv, uloqconv, nominalconc, nominalconc_sigrnd, flagpercent, cvflagpercent, sset,
      Avg(analytearea) mean_response,
      &&ConcRepresentationFunc&(Avg(analytearea),&&ConcFigures&) mean_response_sigrnd,
      Min(resultcommenttext) resultcommenttext,
      case when '&&ExcludeLLOQULOQFromStats&'='F' then null -- disable the LLOQ/LLOQ-feature, treat as numbers
           when '&&FinalAccuracy&'='T' and Avg(conc / dilutionfactor) < lloqconv then 'LLOQ'
           when '&&FinalAccuracy&'='T' and Avg(conc / dilutionfactor) > uloqconv then 'ULOQ'
           when &&ConcRepresentationFunc&(Avg(conc_sigrnd / dilutionfactor),&&ConcFigures&) < lloqconv then 'LLOQ'
           when &&ConcRepresentationFunc&(Avg(conc_sigrnd / dilutionfactor),&&ConcFigures&) > uloqconv then 'ULOQ'
           else null
      end conctext,
      Avg(conc / dilutionfactor) mean_conc_recalc,
      &&ConcRepresentationFunc&(Avg(conc_sigrnd / dilutionfactor),&&ConcFigures&) mean_conc_recalc_sigrnd,
      Avg(conc) mean_conc,
      &&ConcRepresentationFunc&(Avg(conc_sigrnd),&&ConcFigures&) mean_conc_sigrnd,
      Avg(useconc / dilutionfactor) mean_useconc_recalc,
      &&ConcRepresentationFunc&(Avg(useconc_sigrnd / dilutionfactor),&&ConcFigures&) mean_useconc_recalc_sigrnd,
      Avg(useconc) mean_useconc,
      &&ConcRepresentationFunc&(Avg(useconc_sigrnd),&&ConcFigures&) mean_useconc_sigrnd,
      Stddev(useconc) mean_sd,
      &&ConcRepresentationFunc&(Stddev(useconc_sigrnd),&&ConcSDFigures&) mean_sd_sigrnd,
      Min(isdeactivated) min_isdeactivated,
      case when abs(case when '&&FinalAccuracy&'='T' then (Avg(useconc)-nominalconc)/NULLIF(nominalconc,0)*100 else round((&&ConcRepresentationFunc&(Avg(useconc_sigrnd),&&ConcFigures&)-nominalconc_sigrnd)/NULLIF(nominalconc_sigrnd,0)*100,&&DecPlPercBias&) end) > flagpercent then '&&FlaggedSymbol&' else null end flagged,
      case
        when '&&CVFlagOnResultComment&' is not null and regexp_like(Min(resultcommenttext),'&&CVFlagOnResultComment&', 'i') then '&&FlaggedSymbolCV&'
        when abs(case when '&&FinalAccuracy&'='T' then Stddev(useconc)/NULLIF(Avg(useconc),0) else round(&&ConcRepresentationFunc&(Stddev(useconc_sigrnd),&&ConcSDFigures&)/NULLIF(&&ConcRepresentationFunc&(Avg(useconc_sigrnd),&&ConcFigures&),0),&&DecPlPercCV&) end) > cvflagpercent then '&&FlaggedSymbolCV&'
        else null
      end cvflagged,
      round(case when '&&FinalAccuracy&'='T' then (Avg(useconc)-nominalconc)/NULLIF(nominalconc,0)*100
                 else (&&ConcRepresentationFunc&(Avg(useconc_sigrnd),&&ConcFigures&)-nominalconc_sigrnd)/NULLIF(nominalconc_sigrnd,0)*100
            end,&&DecPlPercBias&) biasonmean,
      round(case when '&&FinalAccuracy&'='T' then Stddev(useconc)/NULLIF(Avg(useconc),0)*100
                 else &&ConcRepresentationFunc&(Stddev(useconc_sigrnd),&&ConcFigures&)/NULLIF(&&ConcRepresentationFunc&(Avg(useconc_sigrnd),&&ConcFigures&),0)*100
            end,&&DecPlPercCV&) cv,
      Xmlagg(
        XMLELEMENT("sample", XMLATTRIBUTES(sampleid AS "sampleid", sampleresultid AS "sampleresultid" ),
          XMLELEMENT("runsamplesequencenumber", runsamplesequencenumber),
          XMLELEMENT("dilutionfactor", dilutionfactor),
          XMLELEMENT("source", source),
          XMLELEMENT("concval", &&ConcFormatFunc&(conc_sigrnd,&&ConcFigures&)),
          XMLELEMENT("concentration", &&ConcFormatFunc&(&&ConcRepresentationFunc&(case when '&&FinalAccuracy&'='T' then conc else conc_sigrnd end,&&ConcFigures&),&&ConcFigures&)),
          XMLELEMENT("recalc-concentration", &&ConcFormatFunc&(&&ConcRepresentationFunc&(case when '&&FinalAccuracy&'='T' then conc else conc_sigrnd end,&&ConcFigures&),&&ConcFigures&)),
          XMLELEMENT("recalc-concval", &&ConcFormatFunc&(&&ConcRepresentationFunc&(case when '&&FinalAccuracy&'='T' then conc else conc_sigrnd end / dilutionfactor,&&ConcFigures&),&&ConcFigures&)),
          XMLELEMENT("replicatenumber", replicatenumber),
          XMLELEMENT("resultcommenttext", resultcommenttext),
          XMLELEMENT("response", analytearea),
          XMLELEMENT("lloq", lloqconv),
          XMLELEMENT("uloq", uloqconv),
          XMLELEMENT("nominalconc", &&ConcFormatFunc&(nominalconc_sigrnd,&&ConcFigures&)),
          XMLELEMENT("subset", subset),
          XMLELEMENT("resultcommenttext", resultcommenttext),
          XMLELEMENT("deactivated", isdeactivated)
        ) order by runsamplesequencenumber, name
      ) xml
    FROM
    (SELECT knownID, sampleID, sampleresultID, duplicate, NAME, subset, nominalconc, nominalconc_sigrnd, analyteid, dilutionfactor, source, drug_conc, acceptedrun,
     lloqconv, uloqconv, knowntype, resultcommenttext, analyteorder, knownorder, species, matrix, sset, replicatenumber,
     conc, conc_sigrnd, flagpercent, cvflagpercent, ISDEACTIVATED, runid, runnr, runsamplesequencenumber,
     case when ISDEACTIVATED='T' then null
          else conc
     end useconc_sigrnd,
     case when ISDEACTIVATED='T' then null
          else conc
     end useconc,
     analytearea
     FROM
     (
     SELECT K.ID knownID, s.ID sampleID, sr.ID sampleresultID, &&KnownNameTargetField& name, k.namesuffix subset, s.ISDEACTIVATED, s.dilutionfactor,
        &&Set& sset,
        &&Duplicate& duplicate,
        s.runsamplesequencenumber,
        k.source,
        &&DrugConcentration& drug_conc,
        case when ra.RunAnalyteRegressionStatus = &&RejectedRunAnalyte& then 'F' else 'T' end acceptedrun,
        &&ConcRepresentationFunc&(sr.nominalconcentrationconv,&&ConcFigures&) nominalconc_sigrnd,
        sr.nominalconcentrationconv nominalconc, sr.resultcommenttext, s.replicatenumber,
        ra.lloqconv, ra.uloqconv, K.knowntype,
        case when sr.nominalconcentrationconv = ra.lloqconv then &&FlagPercentBiasLLOQ&
             when sr.nominalconcentrationconv = ra.uloqconv then &&FlagPercentBiasULOQ&
             --when sr.nominalconcentrationconv not between ra.lloqconv and ra.uloqconv then null
             else &&FlagPercentBias&
        end flagpercent,
        case when sr.nominalconcentrationconv = ra.lloqconv then &&FlagPercentCvLLOQ&
             when sr.nominalconcentrationconv = ra.uloqconv then &&FlagPercentCvULOQ&
             --when sr.nominalconcentrationconv not between ra.lloqconv and ra.uloqconv then null
             else &&FlagPercentCV&
        end cvflagpercent,
        s.runid, r.runid runnr, sa.id analyteid,
        &&ConcRepresentationFunc&(sr.concentrationconv,&&ConcFigures&) conc_sigrnd,
        sr.concentrationconv conc, &&StabilityInfo&
        case when ISDEACTIVATED='T' then null
          when sr.concentrationconv/s.dilutionfactor not between ra.lloqconv and ra.uloqconv then null
          else s.dilutionfactor
        end usedilfactor,
        sa.analyteorder, kwz.ordernumber knownorder, a.species, a.sampletypeid matrix,
        srw.analytearea,
        srw.analytearea_sigrnd analytearea_sigrnd
      FROM &&TempTabPrefix&bio$run$ana$known K,
           &&TempTabPrefix&bio$run r,
           &&TempTabPrefix&bio$run$analytes ra,
           --&&TempTabPrefix&wt$assay$analyte aa,
           &&TempTabPrefix&bio$study$analytes sa,  --neu
           &&TempTabPrefix&bio$assay a,            --neu
           &&TempTabPrefix&bio$run$samples s,
           &&TempTabPrefix&bio$run$sample$results sr,
           --&&TempTabPrefix&bio$run$sample$result$raw srw,
           (select runknownid, Avg(analytearea) analytearea, &&ConcRepresentationFunc&(Avg(analytearea), &&ConcFigures&) analytearea_sigrnd from &&TempTabPrefix&bio$run$sample$result$raw, &&TempTabPrefix&bio$run$samples s where runsampleid = s.id group by runknownid) srw,
           isr$crit kwz,
           table(&&LALPackage&.GetRunStates) rs
      WHERE K.ID=s.runknownID
        AND R.ID = s.runid AND R.ID = sr.runid (+)
        --AND r.studyassayid =s.studyassayid AND s.studyassayid = sr.studyassayid
        AND r.RuntypeDescription in (&&RunTypes&)
        AND s.ID=sr.runsampleID
        AND K.knowntype = '&&KnownType&'
        AND s.sampletype='known'
        AND s.runsamplekind = '&&KnownType&'
        AND K.knowntype = s.runsamplekind
        AND ra.id = sr.runanalyteid (+)
        AND a.id = r.assayid              -- neu
        --AND sa.code = ra.analytecode ---- ids  neu
        AND sa.id=ra.analyteid ---- ids  neu
        --AND s.ID = srw.runSampleID (+)
        AND s.runknownID = srw.runknownID (+)
        --AND ra.assayanalyteid = aa.ID
        AND ra.runid = s.runid
        AND K.studyid='&&StudyID&' AND s.studyid='&&StudyID&'
        AND sr.studyID='&&StudyID&' AND r.studyid = '&&StudyID&'
        AND ra.studyID='&&StudyID&'
        AND rs.column_value=r.runstatusnum
        AND kwz.key=&&KnownNameJoinField&
        AND kwz.entity = '&&KnownEntity&'
        AND kwz.masterkey = '&&ExpMasterkey&'
        AND kwz.repid=&&RepID&
        &&AddCondition&
     )) GROUP BY acceptedrun, analyteid, analyteorder, species, matrix, runid, runnr, sset, source, drug_conc, duplicate, dilutionfactor, lloqconv, uloqconv, nominalconc, nominalconc_sigrnd, flagpercent, cvflagpercent
     )
    )
    GROUP BY acceptedrun, analyteid, analyteorder, species, matrix, /*runid, */&&GroupSet& source, drug_conc, lloqconv, uloqconv)
    GROUP BY acceptedrun, analyteid, analyteorder, species, matrix, source