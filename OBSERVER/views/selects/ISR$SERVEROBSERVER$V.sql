CREATE OR REPLACE VIEW ISR$SERVEROBSERVER$V (SERVERID, ALIAS, SERVERTYPEID, SERVERTYPENAME, HOST, PORT, RUNSTATUS, SERVERSTATUS, DISABLED, CLASSNAME, CONTEXT, TIMEOUT, MAX_JOBS, MODIFIEDON, MODIFIEDBY, OBSERVERID, DESCRIPTION, NAMESPACE, TARGETMODELID, JAVA_STARTOPTIONS, OBSERVER_HOST, OBSERVER_PORT, OBSERVER_RUNSTATUS, OBSERVER_TIMEOUT, SERVER_PATH) AS select x.serverid,x.alias,x.servertypeid,  st.servertypename, x.host,x.port,x.runstatus,x.serverstatus, x.disabled, x.classname,x.context,x.timeout,
x.max_jobs,x.modifiedon,x.modifiedby,x.observerid,x.description,x.namespace, x.targetmodelid, x.java_startoptions,
y.host  observer_host, 
y.port observer_port,
y.runstatus observer_runstatus,
y.timeout observer_timeout,
case when st.servertypeid = 1 then null else st.servertypename|| ' ' || x.serverid end  
from   isr$server x
left outer join isr$server y
on     x.observerid = y.serverid
left outer join isr$validvalue$servertype st
on  x.servertypeid=st.servertypeid