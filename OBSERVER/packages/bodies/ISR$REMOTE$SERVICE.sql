CREATE OR REPLACE PACKAGE BODY ISR$REMOTE$SERVICE IS

--******************************************************************************************************
function getChildServers(sHost IN VARCHAR2, sPort IN VARCHAR2) return XMLTYPE
is
  sCurrentName      CONSTANT VARCHAR2(100) := $$PLSQL_UNIT||'.getChildServers';
  sMsg       VARCHAR2(2000);
  cursor cGetServers is
    select XMLELEMENT("servers",  XMLAGG(XMLELEMENT("server",
           XMLELEMENT( "serverid", o.serverid),
           XMLELEMENT( "observerHost", o.observer_host),
           XMLELEMENT( "serverPort", o.port),
           XMLELEMENT( "context", o.context),
           XMLELEMENT( "namespace", o.namespace),
           XMLELEMENT( "serverPath", o.server_path ),
           XMLELEMENT( "javaVmOptions", o.java_startoptions ),
           XMLELEMENT("webserviceClass", o. classname),
           XMLELEMENT("serverStatus", o. serverstatus)
           )))
    from  isr$serverobserver$v o
    where observer_host=sHost and observer_port=sPort;
   xXml XMLTYPE;
begin
 -- isr$trace.stat('begin', 'begin', sCurrentName);
  open cGetServers;
  fetch cGetServers INTO xXml;
  close cGetServers;
  ISR$TRACE.debug_all('end', 'sHost='||sHost||'; sPort='||sPort, sCurrentName, xXml);
 -- isr$trace.stat('end', 'end', sCurrentName);
  return xXml;
EXCEPTION WHEN OTHERS THEN
  isr$trace.error('error', SQLERRM, sCurrentName);
  return null;
end getChildServers;


--******************************************************************************************************
function setServerStatus(nServerid IN NUMBER, sStatus IN VARCHAR2) return NUMBER is
  sCurrentName      CONSTANT VARCHAR2(100) := $$PLSQL_UNIT||'.setServerStatus';
PRAGMA autonomous_transaction;
cnt NUMBER := -1;
begin
 -- isr$trace.stat('begin', 'begin', sCurrentName);
  update isr$server set runstatus=sStatus where serverid=nServerid;
  cnt := SQL%ROWCOUNT;
  commit;
  isr$trace.debug_all ('status for server '||nServerid, sStatus, sCurrentName);
--  isr$trace.stat('end', 'end', sCurrentName);
  return cnt;
  exception
    when others then
    isr$trace.error('error', SQLERRM, sCurrentName);
    return cnt;
end setServerStatus;

--******************************************************************************************************
function setServerStatus(sHost IN VARCHAR2, sPort IN VARCHAR2, sStatus IN VARCHAR2) return NUMBER is
  sCurrentName      CONSTANT VARCHAR2(100) := $$PLSQL_UNIT||'.setServerStatus';
  PRAGMA autonomous_transaction;
  cnt NUMBER := -1;
begin
 -- isr$trace.stat('begin', 'begin', sCurrentName);
  update isr$server set runstatus=sStatus where port = sPort and host = sHost;
  cnt := SQL%ROWCOUNT;
  commit;
  isr$trace.debug_all ('current user:' || stb$security.getCurrentUser || ' status for server on the host '||sHost || ' and port ' || sport, sStatus || '; rows updated: ' || cnt, sCurrentName);
--  isr$trace.stat('end', 'end', sCurrentName);
  return cnt;
 exception
    when others then
    isr$trace.error('error', SQLERRM, sCurrentName);
    return cnt;
end setServerStatus;

--******************************************************************************************************
function getUserLogLevel return VARCHAR2 is
  sCurrentName      CONSTANT VARCHAR2(100) := $$PLSQL_UNIT||'.getUserLogLevel';
begin
  return isr$trace.getJavaUserLoglevelStr;
end getUserLogLevel;

--******************************************************************************************************
function getSystemParameter(sParamName IN VARCHAR2) return VARCHAR2 is
begin
  return stb$util.getSystemParameter(sParamName);
end getSystemParameter;


--******************************************************************************************************
function getServerId(sHost IN VARCHAR2, sPort IN VARCHAR2) return varchar2 is
   sCurrentName      CONSTANT VARCHAR2(100) := $$PLSQL_UNIT||'.getServerId';
   nServerId   varchar2(10);
begin
  isr$trace.stat('begin', 'begin', sCurrentName);
  select serverId into nServerId from isr$server where host = sHost and port = sPort;
  isr$trace.stat('end', 'nServerId=' || nServerId, sCurrentName);
  return nServerId;
exception
  when others then
  isr$trace.error('error', SQLERRM, sCurrentName);
  return null;
end getServerId;


--******************************************************************************************************
function setServerParameter(cnServerId in number, csParamName in varchar2, csParamValue in varchar2) return varchar2  is
  sCurrentName      CONSTANT VARCHAR2(100) := $$PLSQL_UNIT||'.setServerParameter';
begin
  isr$trace.stat('begin', 'begin', sCurrentName);
  merge into ISR$SERVERPARAMETER a
   using ( select csParamName paramName, csParamValue paramValue, cnServerId serverId from dual) b
   on   ( a.parametername = b.paramName
          and a.serverid = b.serverId )
   when not matched then
     insert (PARAMETERNAME, PARAMETERVALUE, SERVERID)
     values (b.paramName, b.paramValue, b.serverId)
   when matched then
    update set a.parametervalue = b.paramValue;
  commit;
  isr$trace.stat('end', 'end', sCurrentName);
  return 'T';
exception
    when others then
    isr$trace.error('error', SQLERRM, sCurrentName);
    return SQLERRM;
end setServerParameter;

-- ***************************************************************************************************
function initializeSession return clob
is
  sCurrentName         constant varchar2(4000) := $$PLSQL_UNIT||'.initializeSession';
  oErrorObj            STB$OERROR$RECORD := STB$OERROR$RECORD();
  csErrorXml  clob;

begin
  isr$trace.stat('begin', 'begin', sCurrentName);
  oErrorObj := isr$util.initializeSession;
  if csErrorXml is not null then
    csErrorXml := xmltype(oErrorObj).getClobVal();
  end if;
  isr$trace.stat('end', 'end', sCurrentName);
  return csErrorXml;
end initializeSession;

end ISR$REMOTE$SERVICE;