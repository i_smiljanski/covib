CREATE OR REPLACE PACKAGE ISR$REMOTE$SERVICE
IS

function getChildServers(sHost IN VARCHAR2, sPort IN VARCHAR2) return XMLTYPE;

function setServerStatus(nServerid IN NUMBER, sStatus IN VARCHAR2) return NUMBER;

function setServerStatus(sHost IN VARCHAR2, sPort IN VARCHAR2, sStatus IN VARCHAR2) return NUMBER ;

function getUserLogLevel return VARCHAR2;

function getSystemParameter(sParamName IN VARCHAR2) return VARCHAR2;

function getServerId(sHost IN VARCHAR2, sPort IN VARCHAR2) return varchar2;

function setServerParameter(cnServerId in number, csParamName in varchar2, csParamValue in varchar2) return varchar2;
-- ***************************************************************************************************
function initializeSession return clob;

END ISR$REMOTE$SERVICE;