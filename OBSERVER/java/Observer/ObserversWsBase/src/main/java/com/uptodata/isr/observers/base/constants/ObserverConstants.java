package com.uptodata.isr.observers.base.constants;

import java.io.File;
import java.time.format.DateTimeFormatter;

/**
 * Created by smiljanskii60 on 17.03.2015.
 */
public class ObserverConstants {
    public static String observerProtocol = "http";
    public static DateTimeFormatter DATE_FORMATTER = DateTimeFormatter.ISO_LOCAL_DATE_TIME;
    public static String LIB_PATH = "lib";
    public static String JAVA_PATH = "jre";
//    public static String dBTokenChangedFile = "dbTokenChanged.txt";
//    public static String datePattern = "dd.MM.yyyy";
}
