package com.uptodata.isr.observers.base.ws;

import com.uptodata.isr.observers.base.dbAccess.DbAccessConstants;
import com.uptodata.isr.observers.base.dbAccess.DbData;
import com.uptodata.isr.observers.base.dbAccess.DbServletsFunctions;
import com.uptodata.isr.observers.base.serverData.ProcessServerData;
import com.uptodata.isr.observers.base.serverData.ServerData;
import com.uptodata.isr.server.utils.logging.log4j2.DbDataForLogging;
import com.uptodata.isr.server.utils.logging.log4j2.IsrServerLogger;
import com.uptodata.isr.server.utils.logging.log4j2.LoggerInterface;
import com.uptodata.isr.server.utils.network.UrlUtil;
import com.uptodata.isr.utils.ConvertUtils;
import com.uptodata.isr.utils.exceptions.MessageStackException;
import com.uptodata.isr.utils.xml.dom.XmlHandler;
import org.apache.commons.collections4.map.CaseInsensitiveMap;
import org.apache.commons.lang.StringUtils;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;

import javax.jws.WebService;
import javax.jws.soap.SOAPBinding;
import javax.xml.xpath.XPathExpressionException;
import java.time.LocalDateTime;
import java.util.Collections;
import java.util.HashMap;
import java.util.Map;

// Referenced classes of package com.uptodata.isr.observers.base.ws:
//            ObserverInterface
@WebService(name = "ObserverInterfaceImpl", targetNamespace = "http://ws.observer.isr.uptodata.com/")
@SOAPBinding()
public abstract class ObserverInterfaceImpl extends ObserverInterface {

    protected static IsrServerLogger log;
    protected String logLevel;
    protected String localHost;
    protected DbData dbData;
    protected DbDataForLogging dbDataForLogging;
    protected Map<String, ServerData> childServersFromDb;
    private Map<String, ProcessServerData> startedChildServers;
    private LocalDateTime lastPing;
    private XmlHandler statusXml;
    protected int statusXmlMethod;

    protected ObserverInterfaceImpl() {
        init();
    }

    public ObserverInterfaceImpl(String localHost, String logLevel, String LogReference, DbData dbData, DbDataForLogging dbDataForLogging) {
        init();
        this.localHost = localHost;
        this.logLevel = logLevel;
        this.dbData = dbData;
        this.dbDataForLogging = dbDataForLogging;
        log = initLogger(logLevel, LogReference);
    }

    private void init() {
        statusXmlMethod = 0;
        childServersFromDb = new CaseInsensitiveMap<>();
        startedChildServers = new CaseInsensitiveMap<>();
        lastPing = LocalDateTime.now();
        statusXml = initStatusXml();
    }

    public void setStatusXml(XmlHandler statusXml) {
        this.statusXml = statusXml;
    }

    public XmlHandler getStatusXml() {
        return statusXml;
    }

    protected abstract boolean tryToStopServer(ProcessServerData serverdata) throws Exception;

    protected abstract ProcessServerData tryToStartServer(Map<String, String> map) throws Exception;

    protected abstract boolean isServerAvailable(ServerData serverdata, int timeout);

    protected abstract void validateStartServerArgs(CaseInsensitiveMap<String, String> map)
            throws MessageStackException;

    protected abstract void validateStopServerArgs(CaseInsensitiveMap<String, String> map)
            throws MessageStackException;

    @Override
    public void createMethodNode(String methodName, CaseInsensitiveMap<String, String> params) throws MessageStackException {
        statusXmlMethod++;
        createMethodNodeWithNum(methodName, params, statusXmlMethod);
    }

    @Override
    public String startServer(Map<String, String> serverProperties) throws MessageStackException {
        CaseInsensitiveMap<String, String> map = new CaseInsensitiveMap<>(serverProperties);
        IsrServerLogger logger = initLogger(map.get("logLevel"), map.get("logReference"));
        logger.stat(LoggerInterface.METHOD_BEGIN);
        createMethodNode("startServer", map);
        try {
            validateStartServerArgs(map);
            ProcessServerData serverData = tryToStartServer(map);
            onSuccessStartServer(serverData);
            logger.stat(METHOD_END);
            return "T";
        } catch (Exception e) {
            onFailStartServer(map, e);
        }
        logger.stat(METHOD_END);
        return "F";
    }

    public String stopServer(Map<String, String> serverProperties) throws MessageStackException {
        CaseInsensitiveMap<String, String> map = new CaseInsensitiveMap<>(serverProperties);
        log = initLogger(map.get("logLevel"), map.get("logReference"));
        log.stat(METHOD_BEGIN);
        createMethodNode("stopServer", map);
        validateStopServerArgs(map);
        String serverId = map.get("serverid");
        log.debug("serverid= " + serverId + "; childServers=" + startedChildServers);
        ProcessServerData serverData = startedChildServers.get(serverId);
        log.info("try to stop server with serverData=" + serverData);
        boolean stopServer = false;
        try {
            stopServer = tryToStopServer(serverData);
        } catch (Exception e) {
            onFailStopServer(map, e);
        }
        if (stopServer) {
            startedChildServers.remove(serverId);
            onSuccessStopServer(serverId);
            log.stat(METHOD_END);
            return "T";
        } else {
            log.stat(METHOD_END);
            return "F";
        }
    }

    public boolean pingMe() {
        lastPing = LocalDateTime.now();
        Node pingN = statusXml.getNode("//lastping");
        pingN.setTextContent(lastPing.format(DATE_FORMATTER));
        return true;
    }

    //todo das ist 100% copy-past der methode getChildServersFromDbXml! Unklar, ob diese methode notwendig ist. Spaeter loeschen
    @Override
    public String getObserverChildServerXml(String observerPort, String logLevel, String logReference, int timeout)
            throws MessageStackException {
        try {
            log = initLogger(logLevel, logReference);
            log.stat(METHOD_BEGIN);
            XmlHandler serverXml = new XmlHandler();
            log.trace("started child servers number =" + startedChildServers.size());
            Node root = serverXml.nodeRoot("childServers");
            //refresh childServersFromDb
//            findChildServers(observerPort, UrlUtil.getLocalHostAddress());
            boolean areThereNotAvailableChilds = false;
            for (Object serverId : startedChildServers.keySet()) {
                ServerData serverData = startedChildServers.get(serverId.toString());
                if (serverData != null) {
                    Node serverNode = createXmlForChildServer(serverData, serverXml);
                    boolean isSeverAvailable = isServerAvailable(serverData, timeout);
                    serverXml.createElement("available", isSeverAvailable ? "T" : "F", null, serverNode);
                    root.appendChild(serverNode);
                    areThereNotAvailableChilds = areThereNotAvailableChilds || isSeverAvailable;
                } else {
                    log.error("There is serverId " + serverId + " without server data in startedChildServers map!");
                }
            }
            if (areThereNotAvailableChilds) {
                findChildServers(observerPort, localHost);
            }
            String result = serverXml.xmlToString();
            log.stat(METHOD_END);
            return result;
        } catch (Exception e) {
            throw new MessageStackException(e, MessageStackException.SEVERITY_CRITICAL, "get child servers is impossible: " + e.getMessage(),
                    MessageStackException.getCurrentMethod());
        }
    }

    @Override
    public String getChildServersFromDbXml(String observerPort, String logLevel, String logReference, int timeout) throws MessageStackException {
        try {
            log = initLogger(logLevel, logReference);
            log.trace(METHOD_BEGIN);
            XmlHandler serverXml = new XmlHandler();
            log.trace("There are " + childServersFromDb.size() + "child Servers from db");
            Node root = serverXml.nodeRoot("childServers");

            if (childServersFromDb.isEmpty()) {
                log.debug("There are any child servers from db, call findChildServers() method");
                findChildServers(observerPort, localHost);
            }

            HashMap<ServerData, Boolean> childServersAvailabilities = getChildServersAvailabilities(timeout);
            boolean areAllServerAvailable = areAllChildServersAvailable(childServersAvailabilities);
            if (!areAllServerAvailable) {// für den fall, wenn ein server in db geändert wurde
                log.debug("not all servers available, call findChildServers() method");
                findChildServers(observerPort, localHost);
                childServersAvailabilities = getChildServersAvailabilities(timeout);
            }

            for (Map.Entry<ServerData, Boolean> serverAvailability : childServersAvailabilities.entrySet()) {
                ServerData serverData = serverAvailability.getKey();
                Node serverNode = createXmlForChildServer(serverData, serverXml);
                boolean isServerAvailable = serverAvailability.getValue();

                serverXml.createElement("available", isServerAvailable ? "T" : "F", null, serverNode);
                root.appendChild(serverNode);
            }
            log.trace(METHOD_END);
            return serverXml.xmlToString();
        } catch (Exception e) {
            throw new MessageStackException(e, MessageStackException.SEVERITY_CRITICAL, "get child servers is impossible: " + e.getMessage(),
                    MessageStackException.getCurrentMethod());
        }
    }

    private boolean areAllChildServersAvailable(HashMap<ServerData, Boolean> childServersAvailabilities) {
        for (Boolean serverAvailability : childServersAvailabilities.values()) {
            if (!serverAvailability) {
                return false;
            }
        }
        return true;
    }

    private HashMap<ServerData, Boolean> getChildServersAvailabilities(int timeout) {
        log.trace(METHOD_BEGIN);
        HashMap<ServerData, Boolean> result = new HashMap<>();
        for (String serverId : childServersFromDb.keySet()) {
            ServerData serverData = childServersFromDb.get(serverId);
            if (serverData != null) {
                boolean isServerAvailable = isServerAvailable(serverData, timeout);
                result.put(serverData, isServerAvailable);
            } else {
                log.error("There is serverId " + serverId + " without server data in childServersFromDb map!");
            }
        }
        log.trace(METHOD_END);
        return result;
    }

    private Node createXmlForChildServer(ServerData serverData, XmlHandler serverXml) {
        Node serverN = serverXml.createElement("server", null, new HashMap<>());
        serverXml.createElement("serverid", serverData.getServerId(), null, serverN);
        serverXml.createElement("url", serverData.getUrl(), null, serverN);
        return serverN;
    }

    public void fillChildServersFromDb(XmlHandler serversXmlFromDb) throws MessageStackException {
        log.trace("save child servers in observer web service");
        try {
            NodeList servers = serversXmlFromDb.selectNodes("//server");
            for (int i = 0; i < servers.getLength(); i++) {
                Node serverNode = servers.item(i);
                String serverId = XmlHandler.getChildNode(serverNode, "serverid").getTextContent();
                String host = XmlHandler.getChildNode(serverNode, "observerHost").getTextContent();
                String port = XmlHandler.getChildNode(serverNode, "serverPort").getTextContent();
                String context = XmlHandler.getChildNode(serverNode, "context").getTextContent();
                String namespace = XmlHandler.getChildNode(serverNode, "namespace").getTextContent();
                if (StringUtils.isNotEmpty(host) && StringUtils.isNotEmpty(port) && StringUtils.isNotEmpty(context)) {
                    String sUrl = UrlUtil.buildUrl(DbAccessConstants.dbServletProtocol, host, port, context);
                    ServerData server = new ServerData(serverId, sUrl, namespace, port);
                    childServersFromDb.put(serverId, server);
                }
            }

        } catch (XPathExpressionException e) {
            throw new MessageStackException(e, 1, "the servers xml file can't be parsed", MessageStackException.getCurrentMethod());
        } catch (Exception e) {
            log.warn(e);
            throw e;
        }
    }

    public XmlHandler findChildServers(String observerPort, String localHost) {
        log.stat(METHOD_BEGIN);
        XmlHandler serversXml = null;
        try {
            serversXml = DbServletsFunctions.getChildServersFromDb(dbData, observerPort, localHost, log);
            log.trace(serversXml);
            int serversNumber = serversXml.countChildren("//servers");
            log.debug("there are  " + serversNumber + " servers found");
            if (serversNumber > 0) {
                fillChildServersFromDb(serversXml);
            } else {
                log.info(serversXml.xmlToString());
                childServersFromDb.clear();
            }
        } catch (Exception e) {
            log.warn(e);
        }
        log.stat(METHOD_END + " childServersFromDb == " + childServersFromDb);
        return serversXml;
    }


    private void onFailStartServer(Map<String, String> params, Exception e) throws MessageStackException {
        String propertiesAsString = ConvertUtils.mapWithPasswordToString(params);
        String userStack = "server with properties " + propertiesAsString + " can't be started on the Observer. " + e;

        onMethodFailed(e, MessageStackException.SEVERITY_CRITICAL, userStack,
                MessageStackException.getCurrentMethod(), statusXmlMethod);
    }

    private void onFailStopServer(Map<String, String> params, Exception e)
            throws MessageStackException {
        String propertiesAsString = ConvertUtils.mapWithPasswordToString(params);
        String userStack = "server with properties " + propertiesAsString + " can't be started on the Observer. " + e;
        onMethodFailed(e, MessageStackException.SEVERITY_CRITICAL, userStack, MessageStackException.getCurrentMethod(), statusXmlMethod);
    }

    private void onSuccessStartServer(ProcessServerData server) {
        String serverId = server.getServerId();
        startedChildServers.put(serverId, server);
        String port = server.getPort();
//        log.debug((new StringBuilder()).append("childServers=").append(startedChildServers).toString());
        String msg = "web service is started on the port " + port;
        onMethodSuccess(msg, statusXmlMethod);
    }

    private void onSuccessStopServer(String serverId) {
        String msg = "server " + serverId + " is stopped.";
        onMethodSuccess(msg, statusXmlMethod);
    }

    private void onFailGetObserverStatus(Exception e, String errorText)
            throws MessageStackException {
        int severity = 2;
        String implStack = getClass().getSimpleName() + ".onFailGetObserverStatus";
        onMethodFailed(e, severity, errorText, implStack, statusXmlMethod);
    }


}
