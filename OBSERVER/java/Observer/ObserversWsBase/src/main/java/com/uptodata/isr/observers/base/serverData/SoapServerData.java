package com.uptodata.isr.observers.base.serverData;

import com.uptodata.isr.utils.exceptions.MessageStackException;

import javax.xml.ws.Endpoint;

/**
 * Created by schroedera85 on 10.12.2014.
 */
public class SoapServerData extends ServerData {
    protected Endpoint endpoint;

    public SoapServerData(Endpoint endpoint, String serverId, String url, String namespace, String port) throws MessageStackException {
        super(serverId, url, namespace, port);
        this.endpoint = endpoint;
    }

    public SoapServerData(Endpoint endpoint, ServerData server) throws MessageStackException{
        super(server.serverId, server.url, server.namespace, server.port);
        this.endpoint = endpoint;
    }

    public Endpoint getEndpoint() {
        return endpoint;
    }

    public void setEndpoint(Endpoint endpoint) {
        this.endpoint = endpoint;
    }
}
