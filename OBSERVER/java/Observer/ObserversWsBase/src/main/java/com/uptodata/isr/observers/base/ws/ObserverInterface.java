package com.uptodata.isr.observers.base.ws;


import com.uptodata.isr.observer.transfer.FileTransferInterface;
import com.uptodata.isr.observers.childServer.ObserverChildServerImpl;
import com.uptodata.isr.utils.exceptions.MessageStackException;
import com.uptodata.isr.utils.xml.dom.XmlHandler;

import javax.jws.WebMethod;
import javax.jws.WebService;
import javax.jws.soap.SOAPBinding;
import java.util.Map;

/**
 * Created by smiljanskii60 on 17.07.2014.
 */
@WebService(name = "ObserverInterface", targetNamespace = "http://ws.observer.isr.uptodata.com/")
@SOAPBinding(style = SOAPBinding.Style.DOCUMENT, use = SOAPBinding.Use.LITERAL)
public abstract class ObserverInterface extends ObserverChildServerImpl implements FileTransferInterface {

    @WebMethod
    public abstract String startServer(Map<String, String> serverProperties) throws MessageStackException;

    @WebMethod
    public abstract String stopServer(Map<String, String> serverProperties) throws MessageStackException;

    @WebMethod
    public abstract String getObserverChildServerXml(String observerPort, String logLevel, String logReference, int timeout) throws MessageStackException;

    @WebMethod
    //special for monitoring process
    public String getChildServersFromDbXml(String observerPort, String logLevel, String logReference, int timeout) throws MessageStackException {
        return null;
    }

    @WebMethod
    public abstract void fillChildServersFromDb(XmlHandler serversXmlFromDb) throws MessageStackException;

    public abstract XmlHandler findChildServers(String observerPort, String localHost);

    @WebMethod
    public String getPlatformName() throws MessageStackException {
        return System.getProperty("os.name");
    }

    @WebMethod
    public String getPlatformArchitecture() throws MessageStackException {
//        return System.getProperty("sun.arch.data.model");
        return System.getProperty("sun.cpu.isalist");
    }


    @WebMethod
    public abstract void uploadDbTokenFiles(String configName, String observerPort, String localHost, String logLevel,
                                            String logReference) throws MessageStackException;



/*    @WebMethod
    public default void copyDbTokenToDirectory(String directoryName) throws IOException {
        File[] files = new File(directoryName).listFiles();
        File changedFile = new File(ObserverConstants.dBTokenChangedFile);
        System.out.println("changedFile = " + changedFile);
        System.out.println("Arrays.asList(files) = " + Arrays.asList(files));
        if(files != null && Arrays.asList(files).contains(changedFile)) {
            FileSystemWorker.copyFileToDirectory(Constants.JAR_FILE_PATH, directoryName + File.separator + "lib");
            FileSystemWorker.copyFileToDirectory(ObserverConstants.dbPropertiesFileName, directoryName);
            boolean isDelete = changedFile.delete();
            System.out.println("isDelete = " + isDelete);
        }
    }*/

}
