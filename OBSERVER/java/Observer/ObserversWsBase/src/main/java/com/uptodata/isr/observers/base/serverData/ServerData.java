package com.uptodata.isr.observers.base.serverData;



import com.uptodata.isr.server.utils.network.UrlUtil;
import com.uptodata.isr.utils.exceptions.MessageStackException;

/**
 * Created by smiljanskii60 on 28.08.2014.
 */
public class ServerData {
    protected String serverId;
    protected String url;
    protected String namespace;
    protected String port;

    public ServerData() {
    }

    public ServerData(String serverId, String url, String namespace,  String port) throws MessageStackException {
        this.serverId = serverId;
        /*if (!UrlUtil.isUrlValid(url)){
            throw new MessageStackException(MessageStackException.SEVERITY_CRITICAL, "url \"" + url + "\" is not valid", null, null);
        }*/
        this.url = url;
        this.namespace = namespace;
        try {
            Integer.valueOf(port);
        } catch (Exception e){
            //todo man muss exception werfen
        }
        this.port = port;
    }

    public String getUrl() {
        return url;
    }

    public void setUrl(String url) {
        this.url = url;
    }

    public String getNamespace() {
        return namespace;
    }

    public void setNamespace(String namespace) {
        this.namespace = namespace;
    }

    public String getServerId() {
        return serverId;
    }

    public void setServerId(String serverId) {
        this.serverId = serverId;
    }

    public String getPort() {
        return port;
    }

    public void setPort(String port) {
        this.port = port;
    }

    @Override
    public String toString() {
        return "ServerData{" +
                "serverId='" + serverId + '\'' +
                ", url='" + url + '\'' +
                ", namespace='" + namespace + '\'' +
                ", port='" + port + '\'' +
                '}';
    }
}
