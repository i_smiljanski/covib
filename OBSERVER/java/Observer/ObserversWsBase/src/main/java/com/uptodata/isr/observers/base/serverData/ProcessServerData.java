package com.uptodata.isr.observers.base.serverData;

import com.uptodata.isr.utils.exceptions.MessageStackException;

/**
 * Created by schroedera85 on 10.12.2014.
 */
public class ProcessServerData extends ServerData {
    protected Long processId;

    public ProcessServerData(Long endpoint, String serverId, String url, String namespace, String port) throws MessageStackException {
        super(serverId, url, namespace, port);
        this.processId = endpoint;
    }

    public ProcessServerData(Long endpoint, ServerData server) throws MessageStackException {
        super(server.serverId, server.url, server.namespace, server.port);
        this.processId = endpoint;
    }

    public Long getProcessId() {
        return processId;
    }

    public void setProcessId(Long processId) {
        this.processId = processId;
    }

    @Override
    public String toString() {
        return "ProcessServerData{" +
                "processId=" + processId +
                super.toString() +
                '}';
    }
}
