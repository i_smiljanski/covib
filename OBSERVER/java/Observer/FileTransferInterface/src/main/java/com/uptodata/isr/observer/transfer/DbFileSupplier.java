package com.uptodata.isr.observer.transfer;

import com.uptodata.isr.observers.base.dbAccess.DbData;
import com.uptodata.isr.server.utils.logging.log4j2.IsrServerLogger;
import com.uptodata.isr.utils.exceptions.MessageStackException;

import java.io.*;
import java.net.MalformedURLException;
import java.net.URISyntaxException;
import java.net.URL;
import java.util.HashMap;
import java.util.HashSet;

/**
 * Created by schroedera85 on 24.09.2015.
 */
public class DbFileSupplier extends FileSupplier {
    private DbData dbData;
    private String errorMessage;

    // configuration names in DB table 'isr$transfer$file$config'
    HashMap<FileType, String> dbConfigsForTypes = new HashMap<>();
    //HashMap<String, File> : fileName, file (from DB) for fileType
    private HashMap<FileType, HashMap<String, File>> downloadedFiles = new HashMap<>();

    private final HashSet<String> lockObjects = new HashSet<>();

    public DbFileSupplier(File workFolder, DbData dbData) {
        this.workFolder = workFolder;
        this.dbData = dbData;
    }

    public void init(String inputFileConfigName, String transformerConfigName, String resultConfigName) throws MessageStackException {
        createWorkFolder();
        createSubFolder();
        initFoldersForType();
        initMaps();
        initDbConfigsForTypes(inputFileConfigName, transformerConfigName, resultConfigName);
    }


    private void initMaps() {
        downloadedFiles = new HashMap<>();
        downloadedFiles.put(FileType.INPUT, new HashMap<>());
        downloadedFiles.put(FileType.TRANSFORMER, new HashMap<>());
    }


    public void initDbConfigsForTypes(String inputFileConfigName, String transformerConfigName, String resultConfigName) {
        dbConfigsForTypes = new HashMap<>();
        dbConfigsForTypes.put(FileType.INPUT, inputFileConfigName);
        dbConfigsForTypes.put(FileType.TRANSFORMER, transformerConfigName);
        dbConfigsForTypes.put(FileType.RESULT, resultConfigName);
    }


    private void createWorkFolder() throws MessageStackException {
        if (workFolder.exists()) {
            errorMessage = "The folder " + workFolder.getAbsolutePath() + " exist yet!";
//            log.error(errorMessage);
            //todo "new RuntimeException()" ist falsch, was kann man hier tun?
            throw new MessageStackException(new RuntimeException(), MessageStackException.SEVERITY_CRITICAL,
                    errorMessage, MessageStackException.getCurrentMethod());
        }
        boolean isCreated = workFolder.mkdir();
        if (!isCreated) {
            errorMessage = "Can not create job folder " + workFolder.getAbsolutePath();
//            log.error(errorMessage);
            throw new MessageStackException(new RuntimeException(), MessageStackException.SEVERITY_CRITICAL,
                    errorMessage, MessageStackException.getCurrentMethod());
        }
    }

    private void createSubFolder() throws MessageStackException {
        File xmlFolder = createSubFolderInWorkFolder(inputFolderName);
        File xsltFolder = createSubFolderInWorkFolder(transformerFolderName);
        File resultsFolder = createSubFolderInWorkFolder(resultFolderName);
    }


    private void setLock(String fileName, IsrServerLogger log) {
        synchronized (lockObjects) {
            while (lockObjects.contains(fileName)) {
                try {
                    log.debug("Thread wait for file " + fileName);
                    lockObjects.wait();
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
            }
            lockObjects.add(fileName);
        }
    }

    private void removeLock(String fileName) {
        synchronized (lockObjects) {
            lockObjects.remove(fileName);
            lockObjects.notifyAll();
        }
    }

    @Override
    public File getFileFromDb(FileType fileType, String fileName, String logLevel, String logReference)
            throws MessageStackException {
        IsrServerLogger log = initLogger(logLevel, logReference);
        log.stat(METHOD_BEGIN + "==" + fileName);
        File file = null;
        setLock(fileName, log);
        try {
            HashMap<String, File> files = downloadedFiles.get(fileType);
            file = files.get(fileName);
            if (file == null) {
                file = downloadFileForType(fileType, fileName, logLevel, logReference);
                files.put(fileName, file);
            }
        } catch (Exception e) {
            throw new MessageStackException(e, MessageStackException.SEVERITY_CRITICAL, e.getMessage(),
                    MessageStackException.getCurrentMethod());
        } finally {
            log.stat(METHOD_END + "==" + fileName);
            removeLock(fileName);
        }
        return file;
    }


    public File getResultFile(String resultFileName) {
        File resultFolder = new File(workFolder.getAbsolutePath(), resultFolderName);
        File resultFile = new File(resultFolder, resultFileName);
        return resultFile;
    }

    private File downloadFileForType(FileType fileType, String fileName, String logLevel, String logReference)
            throws Exception {
        IsrServerLogger log = initLogger(logLevel, logReference);
        log.stat(METHOD_BEGIN + "==" + fileName);
        String configName = dbConfigsForTypes.get(fileType);
        log.debug("getUrlForDownloadFile: transferKeyId = " + fileName + "; configName = " + configName);
        URL url = getUrlForDownloadFile(dbData, configName, fileName, logLevel, logReference);

        String folderName = foldersForType.get(fileType);
        File folder = new File(workFolder.getAbsolutePath(), folderName);
//        dbData.doAuthenticate();

        File file = downloadFile(fileName, folder.getAbsolutePath(), "false", url, logLevel, logReference);
        log.stat(METHOD_END + "==" + fileName);
        return file;

    }

    @Override
    public boolean uploadFile(File resultFile, String resultId, String configName, String logLevel,
                              String logReference) throws MessageStackException, IOException, URISyntaxException {
        IsrServerLogger log = initLogger(logLevel, logReference);
        log.stat("begin== transferKeyId = " + resultId + "; configName = " + configName);
        try {
            URL url = getUrlForUploadFile(dbData, configName, resultId, logLevel, logReference);
//            dbData.doAuthenticate();
            InputStream inputStream = new FileInputStream(resultFile);
            uploadFile(resultId, inputStream, url, dbData.getDbUser(), dbData.getDbPassword(), logLevel, logReference);
            log.stat("end");
            return true;
        } catch (IOException | URISyntaxException | MessageStackException e) {
            String errorMsg = "Can not upload file '" + resultId + "' " + e;
            log.error(errorMsg);
            throw e;
        }
    }

    public File downloadFile(String fileName, String target, String configName, String transferKeyId, String logLevel, String logReference)
            throws Exception {
        IsrServerLogger log = initLogger(logLevel, logReference);
        log.stat("begin== transferKeyId = " + transferKeyId + "; configName = " + configName);
        URL dbServletUrl = getUrlForDownloadFile(dbData, configName, transferKeyId, logLevel, logReference);
//        dbData.doAuthenticate();
        File resFile = downloadFile(fileName, target, "F", dbServletUrl, logLevel, logReference);
        log.stat("end== file = " + resFile);
        return resFile;
    }

    public String getErrorMessage() {
        return errorMessage;
    }

    public HashMap<FileType, String> getDbConfigsForTypes() {
        return dbConfigsForTypes;
    }

    public static void main(String[] args) {

        try {
            DbData dbData1 = new DbData("ISROWNER_ARDIS_35", "ardis-d-d", "8082", "", "");
//            dbData1.doAuthenticate();
            DbFileSupplier dbFileSupplier = new DbFileSupplier(new File("test"), dbData1);

            File result = dbFileSupplier.downloadFile("result.xml", "result", "F",
                    new URL("http://10.0.100.204:8082/transferfiles/isrowner_ardis_35/download?transferkeyid=6781_yPopXTbp;xmlFile&zipped=F&configname=TRANSFORM"),
                    "DEBUG", "test ref");

        } catch (Exception e) {
            e.printStackTrace();
        }

    }
}
