package com.uptodata.isr.observer.transfer;

import com.uptodata.isr.observers.base.dbAccess.DbAccessConstants;
import com.uptodata.isr.observers.base.dbAccess.DbData;
import com.uptodata.isr.observers.base.dbAccess.DbServletsFunctions;
import com.uptodata.isr.server.utils.fileSystem.FileSystemWorker;
import com.uptodata.isr.server.utils.logging.log4j2.IsrServerLogger;
import com.uptodata.isr.server.utils.logging.log4j2.LoggerInterface;
import com.uptodata.isr.utils.ConvertUtils;
import com.uptodata.isr.utils.exceptions.MessageStackException;
import org.apache.commons.collections4.map.CaseInsensitiveMap;
import org.apache.commons.io.FileUtils;
import org.apache.commons.io.IOUtils;
import org.apache.commons.lang.StringUtils;
import sun.net.www.protocol.http.AuthCacheImpl;
import sun.net.www.protocol.http.AuthCacheValue;

import javax.jws.WebMethod;
import java.io.*;
import java.net.*;
import java.util.Map;


/**
 * Created by smiljanskii60 on 20.04.2015.
 */
public interface FileTransferInterface extends LoggerInterface {
    @WebMethod(exclude = true)
    public default MessageStackException onFailUploadFile(Exception e, String errorMsg) {
        int severity = MessageStackException.SEVERITY_CRITICAL;
        String devStack = e + " (" + e.getStackTrace()[0] + ")";
        return new MessageStackException(severity, errorMsg, "uploadFile", devStack);
    }


    @WebMethod(exclude = true)
    public default URL getUrlForUploadFile(DbData dbData, String configName, String transferKeyId,
                                           String logLevel, String logReference) throws MalformedURLException, MessageStackException, URISyntaxException, UnsupportedEncodingException {
        return getUrlForServlet(dbData, "upload", configName, transferKeyId, logLevel, logReference);
    }


    @WebMethod(exclude = true)
    public default URL getUrlForDownloadFile(DbData dbData, String configName, String transferKeyId, String zipped,
                                             String logLevel, String logReference)
            throws MessageStackException {
        try {
            URL url = getUrlForServlet(dbData, "download", configName, transferKeyId, zipped, logLevel, logReference);
            return url;
        } catch (Exception e) {
            String errorMsg = "url for file download could not be created";
            throw new MessageStackException(e, MessageStackException.SEVERITY_CRITICAL, errorMsg,
                    MessageStackException.getCurrentMethod());
        }
    }

    @WebMethod(exclude = true)
    public default URL getUrlForDownloadFile(DbData dbData, String configName, String transferKeyId,
                                             String logLevel, String logReference)
            throws MessageStackException {
        return getUrlForDownloadFile(dbData, configName, transferKeyId, "F", logLevel, logReference);
    }

    @WebMethod(exclude = true)
    public default URL getUrlForServlet(DbData dbData, String function, String configName, String transferKeyId,
                                        String zipped, String logLevel, String logReference) throws MalformedURLException, URISyntaxException, MessageStackException, UnsupportedEncodingException {
        CaseInsensitiveMap<String, String> params = new CaseInsensitiveMap<>();
        params.put("transferKeyId", transferKeyId);
        params.put("configName", configName);
        params.put("zipped", zipped);
        params.put("logLevel", logLevel);
        params.put("logReference", logReference);
        String validate = ConvertUtils.validateMapForArray(params, new String[]{"transferKeyId", "configName"});
        if (StringUtils.isNotEmpty(validate)) {
            throw new MessageStackException("Params for servlet url are not valid!" + validate);
        }

        return dbData.buildDbServletUrl(DbAccessConstants.transferServlet, function, params);
    }

    @WebMethod(exclude = true)
    public default URL getUrlForServlet(DbData dbData, String function, String configName, String transferKeyId,
                                        String logLevel, String logReference) throws URISyntaxException, MessageStackException, MalformedURLException, UnsupportedEncodingException {
        return getUrlForServlet(dbData, function, configName, transferKeyId, "F", logLevel, logReference);
    }

    @WebMethod
    public default boolean uploadFile(Map<String, Object> fileProperties, Map<String, Object> dbProperties,
                                      Map<String, String> logProperties) throws IOException, MessageStackException, URISyntaxException {
        String logLevel = logProperties.get("logLevel");
        String logReference = logProperties.get("logReference");
        String fileName = fileProperties.get("fileName").toString();
        IsrServerLogger log = initLogger(logLevel, logReference);
        log.stat(METHOD_BEGIN + "==" + fileName);
        InputStream stream = (InputStream) fileProperties.get("fileStream");

        DbData dbData = (DbData) dbProperties.get("dbData");
        String configName = dbProperties.get("configName").toString();
        String transferKeyId = dbProperties.get("transferKeyId").toString();

        URL urlForUpload;
        urlForUpload = getUrlForServlet(dbData, "upload", configName, transferKeyId, logLevel, logReference);
        log.info("urlForUpload == " + urlForUpload);
        uploadFile(fileName, stream, urlForUpload, dbData.getDbUser(), dbData.getDbPassword(), logLevel, logReference);
        DbServletsFunctions.waitAfterRequest(dbData, log);
        log.stat(METHOD_END + "==" + fileName);
        return true;
    }


    //todo: overload with file url instead of stream
    @WebMethod(exclude = true)
    public default boolean uploadFile(String fileName, InputStream stream, URL url, String user, String password, String logLevel,
                                      String logReference) throws IOException {
        IsrServerLogger log = initLogger(logLevel, logReference);
        log.stat(METHOD_BEGIN + "==" + fileName);
        log.info("begin upload file ==" + fileName);

        try {
            CookieHandler.setDefault(new CookieManager(null, CookiePolicy.ACCEPT_ALL));

            HttpURLConnection httpUrlConnection = (HttpURLConnection) url.openConnection();
            uploadFile(fileName, stream, url, httpUrlConnection, log);

            httpUrlConnection.disconnect();
            AuthCacheValue.setAuthCache(new AuthCacheImpl());

            log.stat(METHOD_END + "==" + fileName);

            stream.close();
            log.debug("stream closed");

            return true;
        } catch (IOException e) {
            log.error(e);
            throw e;
        }

    }

    @WebMethod(exclude = true)
    public default String uploadFileWithResult(String fileName, InputStream stream, URL url, String logLevel,
                                               String logReference) throws IOException {
        IsrServerLogger log = initLogger(logLevel, logReference);
        log.stat(METHOD_BEGIN + "==" + fileName);
        log.info("begin upload file ==" + fileName);

        String result = null;
        try {
            CookieHandler.setDefault(new CookieManager(null, CookiePolicy.ACCEPT_ALL));
            HttpURLConnection httpUrlConnection = (HttpURLConnection) url.openConnection();
            uploadFile(fileName, stream, url, httpUrlConnection, log);

            InputStream inputStream = httpUrlConnection.getInputStream();
            if (inputStream != null) {
                result = IOUtils.toString(inputStream);
                inputStream.close();
            }
            httpUrlConnection.disconnect();
            AuthCacheValue.setAuthCache(new AuthCacheImpl());

            log.stat(METHOD_END + "==" + fileName);

            stream.close();
            log.debug("stream closed");
            return result;
        } catch (Exception e) {
            log.error(e);
            throw e;
        }

    }


    @WebMethod(exclude = true)
    default void uploadFile(String fileName, InputStream stream, URL url, HttpURLConnection httpUrlConnection, IsrServerLogger log) throws IOException {
        String attachmentName = "files";
        String crlf = "\r\n";
        String twoHyphens = "--";
        String boundary = "*****";

        log.debug("url==" + url);
        httpUrlConnection.setUseCaches(false);
        httpUrlConnection.setDoOutput(true);
        try {
            httpUrlConnection.setRequestMethod("POST");
            httpUrlConnection.setRequestProperty("Connection", "Keep-Alive");
            httpUrlConnection.setRequestProperty("Cache-Control", "no-cache");
            httpUrlConnection.setRequestProperty("accept-charset", "utf-8");
            httpUrlConnection.setRequestProperty("Content-Type", "multipart/form-data;charset=utf-8;boundary=" + boundary);

            httpUrlConnection.connect();
            DataOutputStream request = new DataOutputStream(httpUrlConnection.getOutputStream());

            request.writeBytes(twoHyphens + boundary + crlf);
            request.writeBytes("Content-Disposition: form-data; name=\"" + attachmentName + "\";filename=\"" + fileName + "\"" + crlf);
            request.writeBytes(crlf);
            int bytesRead;
            byte[] buffer = new byte[1024];
            int allBytes = 0;
            while ((bytesRead = stream.read(buffer)) > 0) {
                allBytes = allBytes + bytesRead;
                request.write(buffer, 0, bytesRead);
            }
            request.writeBytes(crlf);
            request.writeBytes(twoHyphens + boundary + twoHyphens + crlf);
            request.flush();
            request.close();
            log.debug("request closed, sended ==" + allBytes + "  bytes");

            int code = httpUrlConnection.getResponseCode();
            log.debug("Response code from server== " + code);
            if (code != 200) {
                String msg = null;
                InputStream errorStream = httpUrlConnection.getErrorStream();
                if (errorStream != null) {
                    msg = IOUtils.toString(errorStream);
                    log.error("response code== " + code);
                    errorStream.close();
                }
                throw new IOException("Response code from server: " + code + " " + msg);
            }
        } catch (IOException e) {
            log.error(e);
            throw e;
        }
    }

    @WebMethod(exclude = true)
    public default File downloadFile(String fileName, String target, String zipped, URL url, String logLevel,
                                     String logReference) throws Exception {
        IsrServerLogger log = initLogger(logLevel, logReference);
        log.stat(METHOD_BEGIN + "==" + fileName);
        log.info("begin download file ==" + fileName + " from \"" + url + "\"");

        CookieHandler.setDefault(new CookieManager(null, CookiePolicy.ACCEPT_ALL));

        File uploadedFile;
        String errorMsg = "create url for \"" + url + "\" failed";
        log.debug("url==" + url);
        String separator = File.separator;
        try {
            target = target.replace("/", separator).replace("\\", separator);
            File destination = new File(target);
            boolean destExists = destination.exists();
            if (!destExists) {
                destExists = destination.mkdirs();
            }

            if (!destExists) {
                errorMsg = errorMsg + " File target \"" + destination.getCanonicalPath() + "\" does not exist";
                throw new Exception(errorMsg + " url: " + url);
            }

            if (ConvertUtils.convertStringToBoolean(zipped)) {
                File zip = new File(target + separator + fileName);
                log.debug("zip==" + zip);
                errorMsg = errorMsg + " unzip file failed";
                uploadedFile = destination;

                InputStream zipStream = url.openStream();
                FileSystemWorker.copyInputStreamToFile(zipStream, zip);
                zipStream.close();

                FileSystemWorker.unpackZipToFolder(zip, uploadedFile);
                FileSystemWorker.tryToRemoveFolder(zip, 5, 500);
            } else {
                uploadedFile = new File(target + File.separator + fileName);
                errorMsg = errorMsg + " copy file to \"" + uploadedFile + "\" failed";
                log.debug("before open url");
                FileUtils.copyURLToFile(url, uploadedFile); //Copies bytes from the URL source to a file destination.
            }

            if (uploadedFile.length() < 1) {
                errorMsg = errorMsg + " Downloaded file '" + uploadedFile + "' is empty. Download url: \"" + url + "\"";
                log.warn("warn==" + errorMsg);
            }
            AuthCacheValue.setAuthCache(new AuthCacheImpl());
            log.info("end download file " + fileName);
            log.stat(METHOD_END + "==" + fileName);
            return uploadedFile;
        } catch (Exception e) {
            log.error("error==" + errorMsg + " " + e);
            throw e;
        }
    }
}
