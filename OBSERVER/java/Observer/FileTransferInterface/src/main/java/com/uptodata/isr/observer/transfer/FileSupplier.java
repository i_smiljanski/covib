package com.uptodata.isr.observer.transfer;

import com.uptodata.isr.server.utils.fileSystem.FileSystemWorker;
import com.uptodata.isr.utils.exceptions.MessageStackException;

import java.io.File;
import java.io.IOException;
import java.util.HashMap;

/**
 * Created by schroedera85 on 25.09.2015.
 */
public abstract class FileSupplier implements FileTransferInterface {

    protected File workFolder;

    public String inputFolderName = "inputs";
    public String transformerFolderName = "transformers";
    public String resultFolderName = "results";

    protected HashMap<FileType, String> foldersForType = new HashMap<>();

    public enum FileType {INPUT, TRANSFORMER, RESULT}


    public void initFoldersForType() {
        HashMap<FileType, String> foldersForType = new HashMap<>();
        foldersForType.put(FileType.INPUT, inputFolderName);
        foldersForType.put(FileType.TRANSFORMER, transformerFolderName);
        foldersForType.put(FileType.RESULT, resultFolderName);
        this.foldersForType = foldersForType;
    }

    public File getWorkFolder() {
        return workFolder;
    }

    public void clearFileSystem() throws MessageStackException {
        String errorMessage = "can not clear work folder " + workFolder.getAbsolutePath();
        try {
            boolean isRemoved = FileSystemWorker.tryToRemoveFolder(workFolder, 5, 500);
        } catch (Exception e) {
            errorMessage = " " + e;
            throw new MessageStackException(e, MessageStackException.SEVERITY_CRITICAL, errorMessage,
                    MessageStackException.getCurrentMethod());
        }
    }

    public static File createFolder(String parentName, String folderName) throws MessageStackException {
        try {
            File file = FileSystemWorker.createFolder(parentName, folderName);
            return file;
        } catch (IOException e) {
            String errorMessage = "Can not create folder " + folderName + " in parent folder " + parentName + System.lineSeparator() + e;

            throw new MessageStackException(e, MessageStackException.SEVERITY_CRITICAL,
                    errorMessage, MessageStackException.getCurrentMethod());
        }
    }

    protected File createSubFolderInWorkFolder(String subFolderName) throws MessageStackException {
        File subFolder = createFolder(workFolder.getAbsolutePath(), subFolderName);
        if (!FileSystemWorker.checkFolder(subFolder)) {
            String errorMessage = "Error by creating of " + subFolder.getAbsolutePath();
            throw new MessageStackException(new RuntimeException(), MessageStackException.SEVERITY_CRITICAL,
                    errorMessage, MessageStackException.getCurrentMethod());
        }
        return subFolder;
    }

    public HashMap<FileType, String> getFoldersForType() {
        return foldersForType;
    }


    public abstract File getFileFromDb(FileType fileType, String fileName, String logLevel, String logReference)
            throws MessageStackException;

    public abstract boolean uploadFile(File resultFile, String resultId, String configName,
                                       String logLevel, String logReference) throws Exception;

    public abstract File getResultFile(String resultFileName);

}
