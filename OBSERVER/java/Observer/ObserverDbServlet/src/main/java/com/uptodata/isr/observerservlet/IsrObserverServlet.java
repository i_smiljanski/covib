package com.uptodata.isr.observerservlet;

import com.uptodata.isr.db.connection.ConnectionUtil;
import com.uptodata.isr.db.dbservlets.ServletUtil;
import com.uptodata.isr.db.trace.DbLogging;
import oracle.jdbc.OracleCallableStatement;
import oracle.jdbc.OracleTypes;
import oracle.xdb.XMLType;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.sql.Connection;
import java.sql.SQLException;


public class IsrObserverServlet extends ServletUtil {
    private Connection conn;

    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws IOException {
        String servletPath = req.getRequestURI();
        DbLogging log = new DbLogging();
        initLog(log, req);

        String errorMsg = "";
        //log.debug("begin", "servletPath = '" + servletPath + "'", currentName);
        try {
            if (conn == null) {
                conn = ConnectionUtil.getInitializeConnection(log);
            }
            if (checkServletPath(servletPath, "getChildServers")) {
                String observerHost = getRequestRequiredParameter(req, "observerHost");
                String observerPort = getRequestRequiredParameter(req, "observerPort");
                String servers = getChildServers(observerHost, observerPort);
                resp.getWriter().println(servers);
                log.debug_all("end", "getChildServers: " + servers);
            } else if (checkServletPath(servletPath, "setServerStatus")) {
                String status = getRequestRequiredParameter(req, "status");
                try {
                    String host = getRequestRequiredParameter(req, "serverHost");
                    String port = getRequestRequiredParameter(req, "serverPort");
                    int updatedRows = setServerStatus(host, port, status);
                    resp.getWriter().println(updatedRows);
                } catch (Exception e) {
                    errorMsg = e.getMessage();
                    String serverId = getRequestRequiredParameter(req, "serverId");
                    int updatedRows = setServerStatus(serverId, status);
                    resp.getWriter().println(updatedRows);
                }

                log.debug_all("end", "setServerStatus");
            } else if (checkServletPath(servletPath, "getUserLogLevel")) {
                String logLevel = getUserLogLevel();
                resp.getWriter().println(logLevel);
                log.debug("end", "getUserLogLevel: " + logLevel);
            } else if (checkServletPath(servletPath, "getSystemParameter")) {
                String paramName = getRequestRequiredParameter(req, "paramName");
                String paramValue = getSystemParameter(paramName);
                resp.getWriter().println(paramValue);
                log.debug("end", "getSystemParameter: " + paramValue);
            } else if (checkServletPath(servletPath, "getServerId")) {
                String observerHost = getRequestRequiredParameter(req, "observerHost");
                String observerPort = getRequestRequiredParameter(req, "observerPort");
                String serverId = getServerId(observerHost, observerPort);
                resp.getWriter().println(serverId);
                log.debug("end", "getServerId: " + serverId);
            } else if (checkServletPath(servletPath, "setServerParameter")) {
                String serverId = getRequestRequiredParameter(req, "serverId");
                String parameterName = getRequestRequiredParameter(req, "parameterName");
                String parameterValue = req.getParameter("parameterValue");
                String response = setServerParameter(serverId, parameterName, parameterValue);
                resp.getWriter().println(response);
                log.debug("end", "setServerParameter: " + response);

            } else {
                resp.getWriter().println("servlet path " + servletPath + " is unknown!");
                log.info("end", "servlet path " + servletPath + " is unknown!");
            }
            resp.getWriter().close();
        } catch (Exception e) {
            resp.getWriter().println(errorMsg + " " + e);
            resp.getWriter().close();
        }
    }


    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws IOException {
        doGet(req, resp);
    }


    private String getChildServers(String host, String port) throws SQLException {
        String xml = "";
        OracleCallableStatement statement = (OracleCallableStatement) conn.prepareCall("begin ? := isr$remote$service.getChildServers(?, ?); end; ");
        statement.registerOutParameter(1, OracleTypes.OPAQUE, "SYS.XMLTYPE");
        statement.setString(2, host);
        statement.setString(3, port);
        statement.executeQuery();
        XMLType xmlType = (XMLType) statement.getObject(1);
        if (xmlType != null) {
            xml = xmlType.getString();
        }
        statement.close();
        return xml;
    }


    private int setServerStatus(String host, String port, String status) throws SQLException {
        OracleCallableStatement statement = (OracleCallableStatement) conn.prepareCall("begin ? := isr$remote$service.setServerStatus(?, ?, ?); end; ");
        statement.registerOutParameter(1, OracleTypes.NUMBER);
        statement.setString(2, host);
        statement.setString(3, port);
        statement.setString(4, status);
        statement.executeQuery();
        int updatedRows = statement.getInt(1);
        statement.close();
        return updatedRows;
    }


    private String getServerId(String host, String port) throws SQLException {
        OracleCallableStatement statement = (OracleCallableStatement) conn.prepareCall("begin ? := isr$remote$service.getServerId(?, ?); end; ");
        statement.registerOutParameter(1, OracleTypes.VARCHAR);
        statement.setString(2, host);
        statement.setString(3, port);
        statement.executeQuery();
        String serverId = statement.getString(1);
        statement.close();
        return serverId;
    }


    private int setServerStatus(String serverId, String status) throws SQLException {
        OracleCallableStatement statement = (OracleCallableStatement) conn.prepareCall(
                "begin ? := isr$remote$service.setServerStatus(?, ?); end; ");
        statement.registerOutParameter(1, OracleTypes.NUMBER);
        statement.setInt(2, Integer.parseInt(serverId));
        statement.setString(3, status);
        statement.executeQuery();
        int updatedRows = statement.getInt(1);
        ConnectionUtil.closeStatement(statement);
        return updatedRows;
    }

    private String setServerParameter(String serverId, String parameterName, String parameterValue) throws SQLException {
        OracleCallableStatement statement = (OracleCallableStatement) conn.prepareCall(
                "begin ? := isr$remote$service.setServerParameter(?, ?, ?); end; ");
        statement.registerOutParameter(1, OracleTypes.NUMBER);
        statement.setInt(2, Integer.parseInt(serverId));
        statement.setString(3, parameterName);
        statement.setString(4, parameterValue);
        statement.executeQuery();
        String retStr = statement.getString(1);
        ConnectionUtil.closeStatement(statement);
        return retStr;
    }


    private String getUserLogLevel() throws SQLException {
        OracleCallableStatement statement = (OracleCallableStatement) conn.prepareCall("begin ? := isr$remote$service.getUserLogLevel; end; ");
        statement.registerOutParameter(1, OracleTypes.VARCHAR);
        statement.executeQuery();
        String logLevel = statement.getString(1);
        statement.close();
        return logLevel;
    }


    private String getSystemParameter(String paramName) throws SQLException {
        OracleCallableStatement statement = (OracleCallableStatement) conn.prepareCall("begin ? := isr$remote$service.getSystemParameter(?); end; ");
        statement.registerOutParameter(1, OracleTypes.VARCHAR);
        statement.setString(2, paramName);
        statement.executeQuery();
        String paramValue = statement.getString(1);
        statement.close();
        return paramValue;
    }
}