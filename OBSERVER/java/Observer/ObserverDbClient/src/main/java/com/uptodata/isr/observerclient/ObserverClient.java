package com.uptodata.isr.observerclient;

import com.uptodata.isr.db.DbConvertUtils;
import com.uptodata.isr.db.connection.ConnectionUtil;
import com.uptodata.isr.db.trace.DbLogging;
import com.uptodata.isr.utils.ConvertUtils;
import com.uptodata.isr.utils.exceptions.MessageStackException;
import com.uptodata.isr.utils.ws.SOAPUtil;
import com.uptodata.isr.utils.ws.WsUtil;
import com.uptodata.isr.utils.xml.dom.XmlHandler;
import org.apache.commons.collections4.map.CaseInsensitiveMap;
import org.w3c.dom.Node;

import java.sql.Array;
import java.sql.Clob;
import java.sql.Connection;
import java.util.Arrays;
import java.util.Map;


/**
 * Created by smiljanskii60 on 17.07.2014.
 */
public class ObserverClient {
    private static DbLogging log;
    final static String observerContext = "ws/observer/isr/uptodata/com";
    final static String targetNamespace = "http://ws.observer.isr.uptodata.com/";
    static int defaultTimeout = 60;  //sec


    public static Clob startServer(String observerHost, String observerPort, String port, String webserviceClass,
                                   String context, String namespace, String serverId, String serverPath,
                                   String javaVmOptions, String logLevel, String logReference, int timeout) throws Exception {
        initLogger();
        String info = " observerHost=" + observerHost + " observerPort = " + observerPort + " server port=" + port +
                " webserviceClass class=" + webserviceClass + " serverContext= " + context;
        log.stat("begin", "Start server: " + info);
        String url = "";
        try {
            Map<String, Object> map = new CaseInsensitiveMap<String, Object>();
            map.put("observerHost", observerHost);
            map.put("observerPort", observerPort);
            map.put("serverport", port);
            map.put("webserviceClass", webserviceClass);
            map.put("context", context);
            map.put("namespace", namespace);
            map.put("serverId", serverId);
            map.put("serverPath", serverPath);
            map.put("javaVmOptions", javaVmOptions);
            map.put("logLevel", logLevel);
            map.put("logReference", logReference);
            map.put("timeout", String.valueOf(timeout));
            url = "http://" + observerHost + ":" + observerPort + "/" + observerContext;
            SOAPUtil.callMethod(url, targetNamespace, map, "startServer", timeout);
            log.stat("end", "end");
            return null;
        } catch (Exception e) {
            MessageStackException exList = new MessageStackException(e, MessageStackException.SEVERITY_WARNING,
                    "server " + "'" + serverPath + "' couldn't be started", MessageStackException.getCurrentMethod());

            return ConvertUtils.getClob(ConnectionUtil.getDefaultConnection(), exList.toXml().xmlToString().getBytes());
        }
    }


    public static Clob startServer(String observerHost, String observerPort, Array mainParams, Array additionalParams) throws Exception {
        Map<String, Object> map = null;
        initLogger();
        log.stat("begin", observerHost + " " + observerPort);
        try {
            map = DbConvertUtils.anyDataArrayToStringMap(mainParams);
            int timeout = defaultTimeout;
            if (map.containsKey("timeout")) {
                timeout = Integer.parseInt((String) map.get("timeout"));
            }
            if (additionalParams != null) {
                map.putAll(DbConvertUtils.anyDataArrayToStringMap(additionalParams));
            }
            log.debug("Start server with parameters ", map);
            String url = "http://" + observerHost + ":" + observerPort + "/" + observerContext;
            SOAPUtil.callMethod(url, targetNamespace, map, "startServer", timeout);
            log.stat("end", "end");
            return null;
        } catch (Exception e) {
            String serverId = null;
            if (map != null) {
                serverId = (String) map.get("serverId");
            }
            MessageStackException exList = new MessageStackException(e, MessageStackException.SEVERITY_WARNING,
                    "server " + serverId + " couldn't be started " + e.getMessage(), MessageStackException.getCurrentMethod());

            return ConvertUtils.getClob(ConnectionUtil.getDefaultConnection(), exList.toXml().xmlToString().getBytes());
        }
    }


    public static Clob stopServer(String observerHost, String observerPort, String serverId,
                                  String logLevel, int timeout) throws Exception {
        String returnXml;
        String url = "";
        initLogger();
        log.stat("begin", observerHost + " " + observerPort + " " + serverId);
        try {
            Map<String, Object> map = new CaseInsensitiveMap<String, Object>();
            map.put("serverId", serverId);
            map.put("logLevel", logLevel);
            url = "http://" + observerHost + ":" + observerPort + "/" + observerContext;
            returnXml = SOAPUtil.callMethod(url, targetNamespace, map, "stopServer", timeout);
            log.stat("end", returnXml);
            return null;
        } catch (Exception e) {
            MessageStackException exList = new MessageStackException(e, MessageStackException.SEVERITY_WARNING,
                    "Server " + serverId + " couldn't be stopped", MessageStackException.getCurrentMethod());

            return ConvertUtils.getClob(ConnectionUtil.getDefaultConnection(), exList.toXml().xmlToString().getBytes());
        }
    }


    public static Clob getServerStatus(String host, String port, String context, String targetNamespace,
                                       String logLevel, String logReference, int timeout) throws Exception {
        initLogger();
        String url = "http://" + host + ":" + port + context;
        log.stat("begin", url);
        String returnXml;
        XmlHandler xmlHandler = new XmlHandler();
        Node root = xmlHandler.nodeRoot("status");
        String errorMsg = "server status is unknown";

        Connection conn = ConnectionUtil.getDefaultConnection();
        if (conn == null) {
            throw new MessageStackException(MessageStackException.SEVERITY_CRITICAL, errorMsg +
                    "; database connection is impossible",
                    MessageStackException.getCurrentMethod(),
                    null);
        }
        try {
            String wsdlUrl = url + "?wsdl";
            if (!new WsUtil().isServerAvailable(wsdlUrl)) {
                root.setTextContent("server " + wsdlUrl + " is not available");
                returnXml = xmlHandler.xmlToString();
            } else {
                String[] args = new String[]{logLevel, logReference};
                returnXml = SOAPUtil.callMethod(url, targetNamespace, args, "getServerStatus", timeout);
            }
            Clob cl = ConvertUtils.getClob(ConnectionUtil.getDefaultConnection(), returnXml.getBytes());
            log.stat("end", "end");
            return cl;
        } catch (Exception e) {
            try {
                root.setTextContent(errorMsg);
                xmlHandler.createElement("error", String.valueOf(e), null, root);
                if (e instanceof MessageStackException) {
                    returnXml = ((MessageStackException) e).toXml().xmlToString();
                } else {
                    returnXml = xmlHandler.xmlToString();
                }
                Clob cl = ConvertUtils.getClob(ConnectionUtil.getDefaultConnection(), returnXml.getBytes());
                return cl;
            } catch (Exception e1) {
                throw new MessageStackException(e1, MessageStackException.SEVERITY_WARNING, errorMsg,
                        MessageStackException.getCurrentMethod());
            }
        }
    }


    public static String isServerAvailable(String observerHost, String observerPort, String logLevel) throws Exception {
        initLogger();
        String wsdlUrl = "http://" + observerHost + ":" + observerPort + "/" + observerContext + "?wsdl";
        log.stat("begin", wsdlUrl);
        boolean available = new WsUtil().isServerAvailable(wsdlUrl);
        log.stat("end", "server on " + wsdlUrl + " is available? " + available);
        return available ? "T" : "F";
    }

    public static String pingChildServer(String observerHost, String observerPort, String logLevel, int timeout) throws Exception {
        initLogger();
        String url = "http://" + observerHost + ":" + observerPort + "/" + observerContext;
        log.stat("begin", url);
        boolean available = new WsUtil().pingSoapWs(url, targetNamespace, timeout);
        log.stat("end ", available);
        return available ? "T" : "F";
    }


    public static String getPlatformName(String observerHost, String observerPort, String logLevel, int timeout)
            throws Exception {
        initLogger();
        String url = "http://" + observerHost + ":" + observerPort + "/" + observerContext;
        log.stat("begin", url);
        String returnXml = SOAPUtil.callMethod(url, targetNamespace, "getPlatformName", timeout);
        log.stat("end", returnXml);
        return returnXml;
    }

    public static String getPlatformArchitecture(String observerHost, String observerPort, String logLevel, int timeout)
            throws Exception {
        initLogger();
        String url = "http://" + observerHost + ":" + observerPort + "/" + observerContext;
        log.stat("begin", url);
        String returnXml = SOAPUtil.callMethod(url, targetNamespace, "getPlatformArchitecture", timeout);
        log.stat("end", returnXml);
        return returnXml;
    }

    public static Clob createServerDirectory(String observerHost, String observerPort, String dirName,
                                             String folders, String concat, String logLevel, int timeout)
            throws Exception {
        String params = "observerHost=" + observerHost + "; observerPort=" + observerPort + "; dirName=" + dirName +
                "; folders=" + folders + "; concat=" + concat + "; logLevel=" + logLevel;
        initLogger();
        log.stat("begin", params);

        try {
//            Map<String, String> map = new CaseInsensitiveMap<String, String>();
//            map.put("dirName", dirName);
//            map.put("concat", concat);
//            map.put("logLevel", logLevel);
            String[] args = new String[4];
            args[0] = dirName;
            args[1] = folders;
            args[2] = concat;
            args[3] = logLevel;
            String url = "http://" + observerHost + ":" + observerPort + "/" + observerContext;
            String returnXml = SOAPUtil.callMethod(url, targetNamespace, args, "createChildServerDirectory", timeout);
            log.stat("end", returnXml);
            return null;
        } catch (Exception e) {
            MessageStackException exList = new MessageStackException(e, MessageStackException.SEVERITY_WARNING,
                    "directory '" + dirName + "" + "' couldn't be created on server " + observerHost + ":" + observerPort,
                    MessageStackException.getCurrentMethod());
            return ConvertUtils.getClob(ConnectionUtil.getDefaultConnection(), exList.toXml().xmlToString().getBytes());
        }
    }


    public static Clob deleteServerDirectory(String observerHost, String observerPort, String dirName,
                                             String logLevel, int timeout) throws Exception {
        String params = "observerHost=" + observerHost + "; observerPort=" + observerPort + "; dirName=" + dirName +
                "; logLevel=" + logLevel;
        initLogger();
        log.stat("begin", params);

        try {
            String[] args = new String[2];
            args[0] = dirName;
            args[1] = logLevel;
            String url = "http://" + observerHost + ":" + observerPort + "/" + observerContext;
            String returnXml = SOAPUtil.callMethod(url, targetNamespace, args, "deleteChildServerDirectory", timeout);
            log.stat("end", returnXml);
            return null;
        } catch (Exception e) {
            MessageStackException exList = new MessageStackException(e, MessageStackException.SEVERITY_WARNING, "directory " + dirName + "" +
                    " couldn't be deleted on server " + observerHost + ":" + observerPort,
                    MessageStackException.getCurrentMethod());
            return ConvertUtils.getClob(ConnectionUtil.getDefaultConnection(), exList.toXml().xmlToString().getBytes());
        }
    }


    public static Clob deleteFilesInDirectory(String observerHost, String observerPort, String dirName,
                                              String logLevel, int timeout) throws Exception {
        String params = "observerHost=" + observerHost + "; observerPort=" + observerPort + "; dirName=" + dirName +
                "; logLevel=" + logLevel;
        initLogger();
        log.stat("begin", params);

        try {
            String[] args = new String[2];
            args[0] = dirName;
            args[1] = logLevel;
            String url = "http://" + observerHost + ":" + observerPort + "/" + observerContext;
            String returnXml = SOAPUtil.callMethod(url, targetNamespace, args, "deleteFilesInDirectory", timeout);
            log.stat("end", returnXml);
            return null;
        } catch (Exception e) {
            MessageStackException exList = new MessageStackException(e, MessageStackException.SEVERITY_WARNING, "directory " + dirName + "" +
                    " couldn't be deleted on server " + observerHost + ":" + observerPort,
                    MessageStackException.getCurrentMethod());
            return ConvertUtils.getClob(ConnectionUtil.getDefaultConnection(), exList.toXml().xmlToString().getBytes());
        }
    }


    public static Clob sendFileToServer(String observerHost, String observerPort, String fileName, String target,
                                        String zipped, String configName, String keyId, String logLevel,
                                        String logReference, int timeout) throws Exception {
        String[] args = new String[7];
        String userMsg = null;
        initLogger();
        log.stat("begin", "begin");
        try {
            try {
                args[0] = fileName;
                args[1] = target;
                args[2] = zipped;
                args[3] = keyId;
                args[4] = configName;
                args[5] = logLevel;
                args[6] = logReference;
                log.debug("args", Arrays.asList(args));
            } catch (Exception e) {
                userMsg = e.getMessage();
                throw e;
            }
            String url = "http://" + observerHost + ":" + observerPort + "/" + observerContext;
            String returnXml = SOAPUtil.callMethod(url, targetNamespace, args, "downloadAndExtractFile", timeout);
            log.stat("end", "end");
            return null;
        } catch (Exception e) {
            MessageStackException exList = new MessageStackException(e, MessageStackException.SEVERITY_WARNING, userMsg,
                    MessageStackException.getCurrentMethod());
            return ConvertUtils.getClob(ConnectionUtil.getDefaultConnection(), exList.toXml().xmlToString().getBytes());
        }
    }

    public static Clob sendFileToServer(String observerHost, String observerPort, Array params) throws Exception {
        String userMsg = null;
        Map<String, Object> map;
        initLogger();
        log.stat("begin", "begin");
        try {
            try {
                map = DbConvertUtils.anyDataArrayToStringMap(params);

            } catch (Exception e) {
                userMsg = e.getMessage();
                throw e;
            }
            int timeout = defaultTimeout;
            if (map.containsKey("timeout")) {
                timeout = Integer.parseInt((String) map.get("timeout"));
            }
            String url = "http://" + observerHost + ":" + observerPort + "/" + observerContext;
            String returnXml = SOAPUtil.callMethod(url, targetNamespace, map, "downloadAndExtractFile", timeout);
            log.stat("end", "end");
            return null;
        } catch (Exception e) {
            MessageStackException exList = new MessageStackException(e, MessageStackException.SEVERITY_WARNING, userMsg,
                    MessageStackException.getCurrentMethod());

            return ConvertUtils.getClob(ConnectionUtil.getDefaultConnection(), exList.toXml().xmlToString().getBytes());
        }
    }


    public static Clob fetchLogServerFile(String observerHost, String observerPort,
                                          String serverFolder, String startDate, String endDate, String configName,
                                          String keyId, String datePattern, String logLevel, String logReference, int timeout)
            throws Exception {
        initLogger();
        log.stat("begin", "begin");
        String[] args = new String[8];
        args[0] = serverFolder;
        args[1] = startDate;
        args[2] = endDate;
        args[3] = configName;
        args[4] = keyId;
        args[5] = datePattern;
        args[6] = logLevel;
        args[7] = logReference;
        String url = "http://" + observerHost + ":" + observerPort + "/" + observerContext;
        log.info("parameters", Arrays.asList(args).toString());
        try {
            String returnXml = SOAPUtil.callMethod(url, targetNamespace, args, "uploadLogFiles", timeout);
            log.stat("end", "end");
            return null;
        } catch (Exception e) {
            MessageStackException exList = new MessageStackException(e, MessageStackException.SEVERITY_WARNING,
                    "log files couldn't be transfer. Parameters: " + Arrays.asList(args).toString(),
                    MessageStackException.getCurrentMethod());
            return ConvertUtils.getClob(ConnectionUtil.getDefaultConnection(), exList.toXml().xmlToString().getBytes());
        }
    }

    /*public static BLOB getLogFilesAsZip(String observerHost, String observerPort, String logName, DATE beginDate, DATE endDate) {
        String url = "http://" + observerHost + ":" + observerPort + "/" + observerContext;
        String returnXml = SOAPUtil.callMethod(url, targetNamespace, new String[]{port}, "getLogFilesAsZip");
        log.debug("returnXml = " + returnXml);
        return ConvertUtils.getBlob(conn, returnXml.getBytes());
    }*/

    private static void initLogger() throws MessageStackException {
        try {
            log = new DbLogging();
        } catch (Exception e) {
            throw new MessageStackException("Logger initialization missing " + e);
        }
    }

    public void initLogger(String logLevel, String logReference) {
        log = new DbLogging();
        try {
            log.setLogLevel(logLevel);
        } catch (RuntimeException e) {
            log.info("parameter 'logLevel'", e);
        }
        try {
            log.setLogReference(logReference);
        } catch (RuntimeException e) {
            log.info("parameter 'logReference'", e);
        }
    }

    public static void main(String[] args) {
        try {
//            log.debug(startServer("10.0.6.125", "1093", "9095", "com.uptodata.isr.remote.ws.RemoteCollectorService",
//                    "/ws/remote/isr/uptodata/com", "http://ws.remote.isr.uptodata.com/", "1010", "DEBUG"));
            //checkObserverStatus("localhost", "1093");
//            stopServer("10.0.6.125", "1093", "9095");
            //  getObserverStatus("10.0.6.125", "1093");
            // getServerStatus("10.0.6.125", "1093", "2099", "DEBUG");

            //   createServerDirectory("10.0.6.125", "1093", "JDBC Remote Collector 13", "lib, logs, jre", ",", "DEBUG");

            //byte[] bytes = ConvertUtils.fileToBytes(System.getProperty("user.dir") + "\\lib\\commons-codec-1.9.jar");
//            sendFileToServer("10.0.6.125", "1093", "jre", "JDBC Remote Collector 96/lib", "t", "<sdf", "SERVER_FILES", "DEBUG", "test");
//            sendFileToServer("10.0.6.125", "1092", "jre", "JDBC Remote Collector 141/jar", "T", null, "SERVER_FILES", "DEBUG", "NODETYPE SERVER_REMOTE, NODEID 141");
//             System.out.println("isServerAvailable= "+isServerAvailable("isrc-i2-d-rs", "1099","DEBUG"));
            System.out.println(getPlatformArchitecture("isrc-d-rs", "12099", "DEBUG", 100));
            System.out.println("System.getProperty(\"sun.cpu.isalist\") = " + System.getProperty("sun.cpu.isalist"));

        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}