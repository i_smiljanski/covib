package com.uptodata.isr.observers.base.dbAccess;

import com.uptodata.isr.utils.ConvertUtils;
import com.uptodata.isr.utils.exceptions.MessageStackException;
import org.apache.commons.collections4.map.CaseInsensitiveMap;
import org.apache.commons.lang.StringUtils;

import java.io.FileInputStream;
import java.io.UnsupportedEncodingException;
import java.net.*;
import java.util.Properties;

/**
 * Created by smiljanskii60 on 25.03.2015.
 */
public class DbData {

    String dbSchema;
    String dbHost;
    String dbServletPort;
    String dbUser;
    String dbPassword;
    Properties properties = new Properties();

    public DbData() {
    }

    public DbData(String dbSchema, String dbHost, String dbServletPort, String dbUser, String dbPassword) {
        this.dbSchema = dbSchema;
        this.dbHost = dbHost;
        this.dbServletPort = dbServletPort;
        this.dbUser = dbUser;
        this.dbPassword = dbPassword;
        initProperties();
        validate();
        doAuthenticate();
    }

    public DbData(Properties dbConnProperties) {
        this.properties = dbConnProperties;
        initVariable();
        validate();
        doAuthenticate();
    }

    public void initProperties() {
        properties.setProperty("dbSchema", dbSchema);
        properties.setProperty("dbHost", dbHost);
        properties.setProperty("dbWsPort", dbServletPort);
        properties.setProperty("dbWsUser", dbUser);
        properties.setProperty("dbWsPassword", dbPassword);
    }

    public void initVariable() {
        this.dbSchema = properties.getProperty("dbSchema");
        this.dbHost = properties.getProperty("dbHost");
        this.dbServletPort = properties.getProperty("dbWsPort");
        this.dbUser = properties.getProperty("dbWsUser");
        this.dbPassword = properties.getProperty("dbWsPassword");
    }

    public void validate() {
        String validate = ConvertUtils.validateProperties(properties);
        if (StringUtils.isNotEmpty(validate)) {
            throw new RuntimeException(validate);
        }
    }

    public Properties getProperties() {
        return properties;
    }

    public String getDbSchema() {
        return dbSchema;
    }

    public void setDbSchema(String dbSchema) {
        this.dbSchema = dbSchema;
    }

    public String getDbHost() {
        return dbHost;
    }

    public void setDbHost(String dbHost) {
        this.dbHost = dbHost;
    }

    public String getDbServletPort() {
        return dbServletPort;
    }

    public void setDbServletPort(String dbServletPort) {
        this.dbServletPort = dbServletPort;
    }

    public String getDbUser() {
        return dbUser;
    }

    public void setDbUser(String dbUser) {
        this.dbUser = dbUser;
    }

    public String getDbPassword() {
        return dbPassword;
    }

    public void setDbPassword(String dbPassword) {
        this.dbPassword = dbPassword;
    }

    public void doAuthenticate() {
        CookieHandler.setDefault(new CookieManager(null, CookiePolicy.ACCEPT_ALL));
        Authenticator.setDefault(new Authenticator() {
            protected PasswordAuthentication getPasswordAuthentication() {
                PasswordAuthentication auth = new PasswordAuthentication(getDbUser(), getDbPassword().toCharArray());
//                System.out.println("default authentication: " + auth.getUserName() + " " + Arrays.hashCode(auth.getPassword()));
                return auth;
            }
        });
    }


    public URL buildDbServletUrl(String servlet, String function, CaseInsensitiveMap<String, String> params)
            throws MalformedURLException, UnsupportedEncodingException {
        StringBuilder servletUrl = new StringBuilder();
        if (StringUtils.isNotEmpty(servlet)) {
            servletUrl.append("/").append(servlet).append("/").append(dbSchema.toLowerCase());
        }

        if (StringUtils.isNotEmpty(function)) {
            servletUrl.append("/").append(function);
        }
        StringBuilder paramsBuilder = new StringBuilder();
        if (params != null && params.size() > 0) {
            StringBuilder paramBuilder = new StringBuilder();
            for (String key : params.keySet()) {
                String encodedParam = URLEncoder.encode(params.get(key), "UTF-8");
                paramBuilder.append("&").append(key).append("=").append(encodedParam);
            }
            paramBuilder.delete(0, 1);
            paramsBuilder.append(paramBuilder);
        }
//        URI uriWithPort = new URI(DbAccessConstants.dbServletProtocol, null, dbHost, Integer.parseInt(dbServletPort),
//                servletUrl.toString(), paramsBuilder.toString(), null);
//        return uriWithPort.toURL();
        URL url = new URL(DbAccessConstants.dbServletProtocol, dbHost, Integer.parseInt(dbServletPort), servletUrl.toString() + "?" + paramsBuilder.toString(), null);
        return url;
    }


    public URL buildDbServletUrl(String servlet, String function) throws MalformedURLException, URISyntaxException, UnsupportedEncodingException {
        return buildDbServletUrl(servlet, function, null);
    }

    @Override
    public String toString() {
        return "DbData{" +
                "dbSchema='" + dbSchema + '\'' +
                ", dbHost='" + dbHost + '\'' +
                ", dbServletPort='" + dbServletPort + '\'' +
                ", dbUser='" + dbUser + '\'' +
                ", dbPassword='" + dbPassword.replaceAll(".", "*") + '\'' +
                '}';
    }

    public static void main(String[] args) {

        try {
            CaseInsensitiveMap<String, String> params = new CaseInsensitiveMap<>();
            params.put("transferKeyId", "200;ABC+FGH");
            params.put("configName", "configName");
            params.put("zipped", "F");
            params.put("logLevel", "DEBUG");
            params.put("logReference", "test");
            String validate = ConvertUtils.validateMapForArray(params, new String[]{"transferKeyId", "configName"});
            if (StringUtils.isNotEmpty(validate)) {
                throw new MessageStackException("Params for servlet url are not valid!" + validate);
            }

            Properties props = new Properties();
            props.load(new FileInputStream("dbData.properties"));
            CaseInsensitiveMap<String, String> map = new CaseInsensitiveMap<>();
            map.put("observerHost", "10.0.100.207");
            map.put("observerPort", "3099");

            DbData dbData = new DbData("a", "b", "111", "d", "e");
            URL url1 = dbData.buildDbServletUrl(DbAccessConstants.transferServlet, "ffff", params);
//            URL url = dbData.buildDbServletUrl(DbAccessConstants.isrDbServlet, "getChildServers", map);
//            URL url =new URL("http://isrc-d-d12:8082/transferfiles/isrowner_isrc_3508/download?loglevel=DEBUG&transferkeyid=1&zipped=T&configname=SERVER_DEPLOYMENT&logreference=NODETYPE%20SERVER_GRAPHIC,%20NODEID%20200");
            System.out.println("url = " + url1);
//            System.out.println("URLDecoder.decode() = " + URLDecoder.decode("200%3BABC%2BFGH", "UTF-8"));
//            String requestFromServletAsString = UrlUtil.getRequestFromServletAsString(url);


        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}
