package com.uptodata.isr.observers.base.dbAccess;

/**
 * Created by smiljanskii60 on 20.04.2015.
 */
public class DbAccessConstants {
    public static String dbServletProtocol = "http";
    public static String dbFunctionSetServerStatus = "setServerStatus";
    public static final String isrDbServlet = "isrservlet";
    public static final String transferServlet = "transferfiles";
}
