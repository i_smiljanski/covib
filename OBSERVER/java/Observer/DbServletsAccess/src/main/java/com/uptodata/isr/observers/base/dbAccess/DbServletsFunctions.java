package com.uptodata.isr.observers.base.dbAccess;

import com.uptodata.isr.server.utils.logging.log4j2.IsrServerLogger;
import com.uptodata.isr.server.utils.network.UrlUtil;
import com.uptodata.isr.utils.xml.dom.XmlHandler;
import org.apache.commons.collections4.map.CaseInsensitiveMap;
import org.apache.commons.lang3.StringUtils;
import org.apache.commons.lang3.math.NumberUtils;

import java.io.IOException;
import java.io.InputStream;
import java.net.URISyntaxException;
import java.net.URL;

/**
 * Created by smiljanskii60 on 02.09.2015.
 */
public class DbServletsFunctions {

    public static XmlHandler getChildServersFromDb(DbData dbData, String observerPort, String localHost, IsrServerLogger log)
            throws Exception {
//        dbData.doAuthenticate();
        URL dbServletUrl = getUrlForChildServers(dbData, observerPort, localHost);
        log.debug("dbServletUrl==" + dbServletUrl);
        InputStream is = UrlUtil.getRequestFromServletAsIs(dbServletUrl);
        XmlHandler xmlHandler = new XmlHandler(is);
//        log.debug("xmlHandler==" + xmlHandler.xmlToString());
        return xmlHandler;
    }


    public static String getDbLogLevel(DbData dbData) throws IOException, URISyntaxException {
        URL dbServletUrl = dbData.buildDbServletUrl(DbAccessConstants.isrDbServlet, "getUserLogLevel");
        return UrlUtil.getRequestFromServletAsString(dbServletUrl);
    }

    public static String getDbSystemParameter(DbData dbData, String paramName) throws IOException {
        CaseInsensitiveMap<String, String> params = new CaseInsensitiveMap<>();
        params.put("paramName", paramName);
        URL dbServletUrl = dbData.buildDbServletUrl(DbAccessConstants.isrDbServlet, "getSystemParameter", params);
        return UrlUtil.getRequestFromServletAsString(dbServletUrl);
    }


    private static URL getUrlForChildServers(DbData dbData, String observerPort, String localHost)
            throws Exception {
        CaseInsensitiveMap<String, String> params = new CaseInsensitiveMap<>();
        params.put("observerHost", localHost);
        params.put("observerPort", observerPort);
        return dbData.buildDbServletUrl(DbAccessConstants.isrDbServlet, "getChildServers", params);
    }


    public static String getServerId(DbData dbData, String observerPort, String localHost) throws IOException, URISyntaxException {
//        dbData.doAuthenticate();
        CaseInsensitiveMap<String, String> params = new CaseInsensitiveMap<>();
        params.put("observerHost", localHost);
        params.put("observerPort", observerPort);
        URL dbServletUrl = dbData.buildDbServletUrl(DbAccessConstants.isrDbServlet, "getServerId", params);
        return UrlUtil.getRequestFromServletAsString(dbServletUrl);
    }

    
    public static void waitAfterRequest(DbData dbData, IsrServerLogger log) throws IOException {
        String sleepAfterRequest = DbServletsFunctions.getDbSystemParameter(dbData, "SLEEP_AFTER_REQUEST_MILLIS");
        if (StringUtils.isNotEmpty(sleepAfterRequest)) {
            sleepAfterRequest = sleepAfterRequest.replaceAll("\\s+", "");
            if (NumberUtils.isNumber(sleepAfterRequest)) {
                log.debug("sleepAfterRequest: " + sleepAfterRequest + ";");
                try {
                    Thread.sleep(Long.parseLong(sleepAfterRequest));
                } catch (InterruptedException e) {
                    log.warn(e);
                }
            }
        }
    }

}
