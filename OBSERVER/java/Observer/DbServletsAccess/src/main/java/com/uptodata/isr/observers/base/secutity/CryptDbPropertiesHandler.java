package com.uptodata.isr.observers.base.secutity;

import com.uptodata.isr.server.utils.fileSystem.Constants;
import com.uptodata.isr.server.utils.logging.log4j2.DbDataForLogging;
import com.uptodata.isr.server.utils.security.ProtectionUtil;
import com.uptodata.isr.utils.ConvertUtils;
import javassist.CannotCompileException;
import net.lingala.zip4j.exception.ZipException;

import java.io.*;
import java.security.GeneralSecurityException;
import java.util.Properties;

/**
 * Created by schroedera85 on 04.12.2014.
 */
public abstract class CryptDbPropertiesHandler {

    private String dbServletFile;
    private String logFile;

    public CryptDbPropertiesHandler(String dbServletFile, String logFileName) {
        this.dbServletFile = dbServletFile;
        this.logFile = logFileName;
    }


    protected abstract void onServletTokenNotExist() throws FileNotFoundException;

    protected abstract void onLogTokenNotExist() throws FileNotFoundException;

    public Properties getProperties() throws IOException, ZipException, GeneralSecurityException, ClassNotFoundException {
        Properties connProps = new Properties();
        File token = new File(dbServletFile);
        if (!token.exists() || !(token.canRead() && token.length() > 0)) {
            onServletTokenNotExist();
        }
        ByteArrayOutputStream bos = new ByteArrayOutputStream();
        ProtectionUtil.decrypt(new FileInputStream(token), bos, Constants.KEY_CLASS, Constants.JAR_FILE1);
        connProps.load(new InputStreamReader(new ByteArrayInputStream(bos.toByteArray())));

        File logToken = new File(logFile);
        if (!logToken.exists() || !(logToken.canRead() && logToken.length() > 0)) {
            onLogTokenNotExist();
        }
        ByteArrayOutputStream logBos = new ByteArrayOutputStream();
        ProtectionUtil.decrypt(new FileInputStream(logToken), logBos, Constants.KEY_LOG_CLASS, Constants.JAR_FILE2);
        connProps.load(new InputStreamReader(new ByteArrayInputStream(logBos.toByteArray())));
//        System.out.println("connProps = " + connProps);
        return connProps;
    }

    /**
     * saves the db token files for logging, is used from children servers
     *
     * @param args
     */
    public static void saveDbPropertiesForLog(String[] args) throws ClassNotFoundException, GeneralSecurityException, CannotCompileException, ZipException, IOException {
        Properties props = ConvertUtils.convertArrayToProperties(args);
        DbDataForLogging dbDataForLogging = new DbDataForLogging(props);
        //logging in DB
        ProtectionUtil.saveEncryptedProperties(dbDataForLogging.getProperties(), Constants.RSA_LOG_FILE,
                Constants.KEY_LOG_CLASS, Constants.JAR_FILE2);
    }
}
