package com.uptodata.isr.observers.base.dbAccess;

import org.apache.commons.collections4.map.CaseInsensitiveMap;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

import java.net.URL;

public class DbDataTest {
    private DbData dbData;

    @Before
    public void setUp()  {
        dbData = new DbData("ISROWNER_ISRS", "isrc-d-d12", "8082", "ISRWS_ISRS", "ISRWS_ISRS");
    }

    @Test
    public void buildDbServletUrl1() throws Exception {
        CaseInsensitiveMap<String, String> params = new CaseInsensitiveMap<>();
        params.put("serverId", "200");
        params.put("status", "T");
        URL url = dbData.buildDbServletUrl(DbAccessConstants.isrDbServlet, DbAccessConstants.dbFunctionSetServerStatus, params);
        Assert.assertEquals("http://isrc-d-d12:8082/isrservlet/isrowner_isrs/setServerStatus?status=T&serverid=200", url.toString());
    }

}