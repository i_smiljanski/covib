package com.uptodata.isr.observers.monitoring;

import com.uptodata.isr.observers.base.constants.ObserverConstants;
import com.uptodata.isr.observers.base.dbAccess.DbAccessConstants;
import com.uptodata.isr.observers.base.dbAccess.DbData;
import com.uptodata.isr.server.utils.fileSystem.Constants;
import com.uptodata.isr.server.utils.logging.log4j2.DbDataForLogging;
import com.uptodata.isr.server.utils.logging.log4j2.IsrServerLogger;
import com.uptodata.isr.server.utils.logging.log4j2.LogHelper;
import com.uptodata.isr.server.utils.network.UrlUtil;
import com.uptodata.isr.server.utils.security.ProtectionUtil;
import com.uptodata.isr.utils.ConvertUtils;
import com.uptodata.isr.utils.exceptions.MessageStackException;
import com.uptodata.isr.utils.ws.SOAPUtil;
import com.uptodata.isr.utils.ws.WsUtil;
import com.uptodata.isr.utils.xml.dom.XmlHandler;
import org.apache.commons.collections4.map.CaseInsensitiveMap;
import org.apache.commons.lang.StringUtils;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;

import javax.xml.xpath.XPathExpressionException;
import java.io.IOException;
import java.net.MalformedURLException;
import java.net.URISyntaxException;
import java.net.URL;
import java.util.Properties;
import java.util.concurrent.Executors;
import java.util.concurrent.ScheduledExecutorService;
import java.util.concurrent.ScheduledFuture;
import java.util.concurrent.TimeUnit;

/**
 * The main class to send the server status to the database web service
 * Created by smiljanskii60 on 25.08.2014.
 */
public class ObserverWebserviceMonitor {

    private static IsrServerLogger log;
    private String logLevel;
    private static String logReference = "observer webservice monitor";
    private DbData dbData;
    private int failedSendingChildServersStatusCounter;
    private int failedSendingObserverStatusCounter;

    public ObserverWebserviceMonitor(Properties properties) {
        this.logLevel = properties.getProperty("logLevel");
        try {
            dbData = new DbData(properties);
//            ProtectionUtil.saveEncryptedProperties(dbData.getProperties(), Constants.RSA_FILE, Constants.KEY_CLASS, Constants.JAR_FILE1);
        } catch (Exception e) {
            e.printStackTrace();
        }

        try {
            DbDataForLogging dbDataForLogging = new DbDataForLogging(properties);
            ProtectionUtil.saveEncryptedProperties(dbDataForLogging.getProperties(), Constants.RSA_LOG_FILE, Constants.KEY_LOG_CLASS, Constants.JAR_FILE2);
        } catch (Exception e) {
            e.printStackTrace();
        }
        initLogger();
    }

    private void initLogger() {
//        log.debug(logLevel + "; " + logReference);
        log = LogHelper.initLogger(logLevel, logReference);
    }

    private void pingWebservice(Properties properties) {
        String port = properties.getProperty("observerPort");
        String localHost = properties.getProperty("localhost");
        String context = properties.getProperty("context");
        String observerWsNamespace = properties.getProperty("targetNamespace");
        String period = properties.getProperty("pingPeriod");
        String timeoutStr = properties.getProperty("timeout");
        int timeout = 10000;
        if (StringUtils.isNotEmpty(timeoutStr)) {
            timeout = Integer.parseInt(timeoutStr);
        }
        log.info("ObserverWebserviceMonitor start ping with parameters : " + properties);

//        dbData.doAuthenticate();

        final int finalTimeout = timeout;
        Runnable run = () -> {
            try {
                doMonitoring(localHost, port, context, observerWsNamespace, finalTimeout);
            } catch (Exception e) {
                e.printStackTrace();
                log.error(e);
            }
        };
        ScheduledExecutorService scheduler = Executors.newSingleThreadScheduledExecutor();
        ScheduledFuture<?> future = scheduler.scheduleAtFixedRate(run, 1, Long.parseLong(period), TimeUnit.SECONDS);
        log.debug(future.getDelay(TimeUnit.SECONDS));
    }


    public void start(Properties params) {
        params.replace("dbWsPassword", "******");
        params.replace("password", "******");
        log.info("try to start webservice monitor with parameters : " + params);
        pingWebservice(params);
    }


    private void doMonitoring(String localHost, String port, String context, String observerWsNamespace, int timeout)
            throws Exception {
        log.trace("begin; " + localHost);
        String observerWsUrl = null;
        try {
            observerWsUrl = UrlUtil.buildUrl(ObserverConstants.observerProtocol, localHost, port, context);
        } catch (MessageStackException e) {
            log.error(e.getMessage());
        }

        boolean isObserverAvailable = isServerWsAvailable(observerWsUrl, observerWsNamespace, timeout);
        sendObserverStatusToDB(localHost, port, isObserverAvailable ? "T" : "F");

        if (isObserverAvailable) {
            log.trace("observer webservice is available, now send child services status to database");
            String currentServers = getChildServers(observerWsUrl, observerWsNamespace, port, timeout);
            if (StringUtils.isNotEmpty(currentServers)) {
                XmlHandler serversXml = new XmlHandler(currentServers);
                sendAllChildServersStatus(serversXml);
            }
        }
        log.trace("end; observer available - " + isObserverAvailable);
    }


    private void sendAllChildServersStatus(XmlHandler serversXml) throws XPathExpressionException, IOException, URISyntaxException {
        log.trace("serversXml= " + serversXml.xmlToString());
        NodeList servers;
        try {
            servers = serversXml.selectNodes("//server");
        } catch (XPathExpressionException e) {
            log.error(e);
            throw e;
        }
        for (int i = 0; i < servers.getLength(); i++) {
            Node serverNode = servers.item(i);
            String serverId = XmlHandler.getChildNode(serverNode, "serverid").getTextContent();
            String available = XmlHandler.getChildNode(serverNode, "available").getTextContent();
            try {
                sendChildServerStatusToDb(serverId, available);
            } catch (IOException | URISyntaxException e) {
                log.error(e);
                throw e;
            }
        }

    }

    private void sendChildServerStatusToDb(String serverId, String serviceAvailable) throws IOException, URISyntaxException {
        CaseInsensitiveMap<String, String> params = new CaseInsensitiveMap<>();
        params.put("serverId", serverId);
        params.put("status", serviceAvailable);
        String send = sendUrl(params);

        if (StringUtils.isNotEmpty(send)) {
            log.trace("status of child server " + serverId + " (" + serviceAvailable + ") is sent to database; return = " + send);
            failedSendingChildServersStatusCounter = 0;
        } else {
            failedSendingChildServersStatusCounter++;
            String errorText = "status of child server " + serverId + " (" + serviceAvailable + ") can't be send to database";
            if (failedSendingChildServersStatusCounter < 3) {
                log.info(errorText);
            } else {
                log.error(errorText);
            }
        }
    }


    private void sendObserverStatusToDB(String observerHost, String serverPort, String serviceAvailable) throws IOException, URISyntaxException {
        CaseInsensitiveMap<String, String> params = new CaseInsensitiveMap<>();
        params.put("serverHost", observerHost);
        params.put("serverPort", serverPort);
        params.put("status", serviceAvailable);
        String send = sendUrl(params);

        if (StringUtils.isNotEmpty(send)) {
            log.trace("status of  observer " + observerHost + ":" + serverPort + " (" + serviceAvailable + ") is sent to database; return = " + send);
            failedSendingObserverStatusCounter = 0;
        } else {
            failedSendingObserverStatusCounter++;
            String errorText = "status of observer " + observerHost + ":" + serverPort + " (" + serviceAvailable + ") can't be send to database";
            if (failedSendingObserverStatusCounter < 3) {
                log.info(errorText);
            } else {
                log.error(errorText);
            }
        }
    }


    private String sendUrl(CaseInsensitiveMap<String, String> params) throws IOException, URISyntaxException {
        URL fullUrlForSetServerStatus;
        try {
            fullUrlForSetServerStatus = dbData.buildDbServletUrl(DbAccessConstants.isrDbServlet, DbAccessConstants.dbFunctionSetServerStatus, params);
        } catch (MalformedURLException e) {
            log.error(e);
            throw e;
        }
        String send;
        try {
            send = UrlUtil.getRequestFromServletAsString(fullUrlForSetServerStatus);
        } catch (IOException e) {
            log.error( "url = " + fullUrlForSetServerStatus + "   " + e);
            throw e;
        }
        return send;
    }



    private String getChildServers(String url, String observerWsNamespace, String observerPort, int timeout) {
        String retXml = null;
        initLogger();
        try {
            log.trace("getChildServersFromDbXml: " + url + "; " + observerWsNamespace);
            retXml = SOAPUtil.callMethod(url, observerWsNamespace,
                    new String[]{observerPort, logLevel, logReference, String.valueOf(timeout)},
                    "getChildServersFromDbXml", timeout);
        } catch (Exception e) {
            log.warn("Server + '" + url + "' does not reply. " + e);
        }
        return retXml;
    }


    public String callObserverMethod(String url, String observerWsNamespace, String methodName, int timeout) {
        String retXml = null;
        try {
            log.info("Call method \"" + methodName + "\"");
            retXml = SOAPUtil.callMethod(url, observerWsNamespace, methodName, timeout);
            log.debug(retXml);
        } catch (Exception e) {
            log.warn("Server does not reply. " + e.getMessage(), e);
        }
        return retXml;
    }


    private static boolean isServerWsAvailable(String url, String namespace, int timeout) {
        boolean available = false;
        String wsdlUrl = url + "?wsdl";
        if (!new WsUtil().isServerAvailable(wsdlUrl)) {
            log.warn("Connection " + wsdlUrl + " is not available!");
        } else {
            try {
                String pingMe = SOAPUtil.callMethod(url, namespace, "pingMe", timeout);
                available = pingMe.equalsIgnoreCase("true");
            } catch (Exception e) {
                log.warn(e.getMessage());
            }
        }
        return available;
    }

    public static void main(final String[] args) {
        Properties params = null;
        try {
            params = ConvertUtils.convertArrayToProperties(args);
        } catch (Exception e) {
            e.printStackTrace();
        }
        if (params != null) {
            ObserverWebserviceMonitor monitor = new ObserverWebserviceMonitor(params);
            monitor.pingWebservice(params);

        }
    }


}
