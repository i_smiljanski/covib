package com.uptodata.isr.observer.monitoring;

import com.uptodata.isr.observers.monitoring.ObserverWebserviceMonitor;
import com.uptodata.isr.utils.ConvertUtils;

import java.util.Properties;


/**
 * Created by schroedera85 on 22.12.2014.
 */
public class MainObserverMonitorStarter {
    public static void main(String[] args) {

        Properties params = null;
        try {
            params =  ConvertUtils.convertArrayToProperties(args);
        } catch (Exception e) {
            e.printStackTrace();
        }
        if (params != null) {
            ObserverWebserviceMonitor monitor = new ObserverWebserviceMonitor(params);
            monitor.start(params);
        }
    }
}
