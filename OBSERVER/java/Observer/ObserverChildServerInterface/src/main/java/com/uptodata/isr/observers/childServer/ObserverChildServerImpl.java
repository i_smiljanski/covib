package com.uptodata.isr.observers.childServer;

import com.uptodata.isr.server.utils.logging.log4j2.IsrServerLogger;
import com.uptodata.isr.utils.exceptions.MessageStackException;
import com.uptodata.isr.utils.xml.dom.XmlHandler;
import org.apache.commons.collections4.map.CaseInsensitiveMap;

import javax.jws.WebMethod;

/**
 * Created by smiljanskii60 on 17.01.2017.
 */

public class ObserverChildServerImpl implements ObserverChildServerInterface {
    protected int statusXmlMethod;
    protected XmlHandler statusXml;
    static public IsrServerLogger log;

    @WebMethod(exclude = true)
    public void createMethodNode(String methodName, CaseInsensitiveMap<String, String> params) throws MessageStackException {
        statusXmlMethod++;
        createMethodNodeWithNum(methodName, params, statusXmlMethod);
    }

    @WebMethod(exclude = true)
    public void setStatusXml(XmlHandler statusXml) {
        this.statusXml = statusXml;
    }

    @WebMethod(exclude = true)
    public XmlHandler getStatusXml() {
        return statusXml;
    }


    @WebMethod
    public boolean pingMe() {
        return true;
    }


}
