package com.uptodata.isr.observers.childServer;

import com.uptodata.isr.server.utils.logging.log4j2.IsrServerLogger;
import com.uptodata.isr.server.utils.logging.log4j2.LoggerInterface;
import com.uptodata.isr.utils.exceptions.MessageStackException;
import com.uptodata.isr.utils.xml.dom.XmlHandler;
import org.apache.commons.collections4.map.CaseInsensitiveMap;
import org.w3c.dom.Node;

import javax.jws.WebMethod;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.Map;

/**
 * Created by schroedera85 on 19.12.2014.
 */

public interface ObserverChildServerInterface extends LoggerInterface {
    public static DateTimeFormatter DATE_FORMATTER = DateTimeFormatter.ISO_LOCAL_DATE_TIME;

    @WebMethod(exclude = true)
    public default XmlHandler initStatusXml() {
        XmlHandler statusXml = new XmlHandler();
        Node root = statusXml.nodeRoot("status");
        statusXml.createElement("methods", null, root);
        statusXml.createElement("starttime", LocalDateTime.now().format(DATE_FORMATTER), null, root);
        statusXml.createElement("lastping", null, root);
        setStatusXml(statusXml);
        return statusXml;
    }

    @WebMethod(exclude = true)
    public void setStatusXml(XmlHandler statusXml);

    @WebMethod(exclude = true)
    public XmlHandler getStatusXml();

    @WebMethod(exclude = true)
    public void createMethodNode(String methodName, CaseInsensitiveMap<String, String> params)
            throws MessageStackException;

    @WebMethod
    public default boolean pingMe() {
        return true;
    }

    @WebMethod
    public default String getServerStatus(String logLevel, String logReference) throws MessageStackException {
        IsrServerLogger log = initLogger(logLevel, logReference);
        log.stat(METHOD_BEGIN);
        String errorText = "server status is unknown";
        try {
            String serverStatus = tryToGetServerStatus();
            log.stat(METHOD_END + " status: " + serverStatus);
            return serverStatus;
        } catch (Exception e) {
            String implStack = getClass().getSimpleName() + ".onFailGetObserverStatus";
            throw new MessageStackException(e, MessageStackException.SEVERITY_WARNING, errorText, implStack);
        }
    }


    @WebMethod(exclude = true)
    default void createMethodNodeWithNum(String methodName, Map<String, String> params, int methodNum)
            throws MessageStackException {
        try {
            Node methods = getStatusXml().getNode("//methods");
            Map<String, String> attrs = new CaseInsensitiveMap<>();
            attrs.put("timestamp", LocalDateTime.now().format(DATE_FORMATTER));
            attrs.put("name", methodName);
            attrs.put("num", String.valueOf(methodNum));
            Node method = getStatusXml().createElement("method", attrs, methods);
            Node parameters = getStatusXml().createElement("parameters", null, method);
            if (params != null) {
                for (Map.Entry<String, String> param : params.entrySet()) {
                    getStatusXml().createElement(param.getKey(), param.getValue(), null, parameters);
                }
            }
        } catch (Exception e) {
            throw new MessageStackException(e, MessageStackException.SEVERITY_WARNING,
                    "error by create xml node " + methodName + e, MessageStackException.getCurrentMethod());
        }
    }

    @WebMethod(exclude = true)
    public default void onMethodSuccess(String infoText, int methodNum) {
        Node method = getStatusXml().getNode("//method[" + methodNum + "]");
        getStatusXml().createElement("result", infoText, null, method);
    }

    @WebMethod(exclude = true)
    public default void onMethodFailed(Exception e, int severity, String userStack, String implStack, int methodNum)
            throws MessageStackException {
        XmlHandler statusXml = getStatusXml();
        if (statusXml != null) {
            Node method = getStatusXml().getNode("//method[" + methodNum + "]");
            Node error = getStatusXml().createElement("result", "error", null, method);
            getStatusXml().createElement("error", e.toString(), null, error);
        }
        throw new MessageStackException(e, severity, userStack, implStack);
    }

    @WebMethod(exclude = true)
    public default String tryToGetServerStatus() {
        XmlHandler statusXml = getStatusXml();
        for (Node statNode : statusXml.getChildNodesWithoutText(statusXml.getNode("//status"))) {
            if ("heap_statistics".equals(statNode.getNodeName())) {
                statusXml.removeNode(statNode, "heap_statistics");
            }
        }

        Map<String, String> heapAttrs = new CaseInsensitiveMap<>();
        heapAttrs.put("description", "Heap utilization statistics [MB]");
        Node heap = statusXml.createElement("heap_statistics", heapAttrs, statusXml.getNode("status"));

//        log.debug("=====================Observer status begin============================");

//        for(Enumeration p = System.getProperties().propertyNames(); p.hasMoreNodes();){
//            String pName = (String) p.nextNode();
//            log.debug(pName+" "+System.getProperty(pName));
//        }
//        log.debug(ManagementFactory.getRuntimeMXBean().getName());
//        log.debug(ManagementFactory.getMemoryMXBean().getHeapMemoryUsage());
//        log.debug(ManagementFactory.getMemoryMXBean().getNonHeapMemoryUsage());
//        log.debug(ManagementFactory.getMemoryMXBean().getObjectPendingFinalizationCount());
//        log.debug(ManagementFactory.getMemoryMXBean().isVerbose());
        int mb = 1024 * 1024;

        //Getting the runtime reference from system
        Runtime runtime = Runtime.getRuntime();


        //log.debug("##### Heap utilization statistics [MB] #####");

        //Print used memory
        //log.debug("Used Memory:" + (runtime.totalMemory() - runtime.freeMemory()) / mb);
        statusXml.createElement("used_memory", String.valueOf((runtime.totalMemory() - runtime.freeMemory()) / mb), null, heap);

        //Print free memory
        // log.debug("Free Memory:" + runtime.freeMemory() / mb);
        statusXml.createElement("free_memory", String.valueOf(runtime.freeMemory() / mb), null, heap);

        //Print total available memory
        //log.debug("Total Memory:" + runtime.totalMemory() / mb);
        statusXml.createElement("total_memory", String.valueOf(runtime.totalMemory() / mb), null, heap);

        //Print Maximum available memory
        //log.debug("Max Memory:" + runtime.maxMemory() / mb);
        statusXml.createElement("max_memory", String.valueOf(runtime.maxMemory() / mb), null, heap);

//        log.debug("Operating System: " + System.getProperty("os.name") + " Architecture: " + System.getProperty("sun.cpu.isalist"));
//          log.debug("Architecture: " + System.getProperty("sun.cpu.isalist"));
//        log.debug("Java version: " + System.getProperty("java.version"));

        //log.debug("Last polled at " + lastPing.format(ObserverConstants.DATE_FORMATTER));
        String xml = statusXml.xmlToString();
//        log.debug(xml);
        //log.debug("=====================Observer status end============================");
        // byte[] bytes = statusXml.getBytes();//DatatypeConverter.parseBase64Binary(statusXml);
        return xml;
    }
}
