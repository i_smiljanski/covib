package com.uptodata.isr.observers.controlling.main;

import com.uptodata.isr.observers.base.constants.ObserverConstants;
import com.uptodata.isr.observers.base.dbAccess.DbData;
import com.uptodata.isr.observers.base.ws.ObserverInterface;
import com.uptodata.isr.observers.controlling.bootstrap.Bootstrap;
import com.uptodata.isr.observers.controlling.finalize.Finalizer;
import com.uptodata.isr.server.utils.fileSystem.Constants;
import com.uptodata.isr.server.utils.logging.log4j2.DbDataForLogging;
import com.uptodata.isr.server.utils.logging.log4j2.IsrServerLogger;
import com.uptodata.isr.server.utils.network.UrlUtil;
import com.uptodata.isr.server.utils.process.ProcessHelper;
import com.uptodata.isr.utils.ConvertUtils;
import com.uptodata.isr.utils.exceptions.MessageStackException;
import com.uptodata.isr.utils.ws.WsUtil;
import com.uptodata.isr.utils.xml.dom.XmlHandler;
import net.lingala.zip4j.exception.ZipException;
import org.apache.commons.io.FileUtils;
import org.apache.commons.lang.StringUtils;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;

import javax.xml.ws.Endpoint;
import javax.xml.xpath.XPathExpressionException;
import java.io.File;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.Reader;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.security.GeneralSecurityException;
import java.util.Arrays;
import java.util.Map;
import java.util.Properties;
import java.util.concurrent.Executors;
import java.util.concurrent.ScheduledExecutorService;
import java.util.concurrent.TimeUnit;

/**
 * Created by schroedera85 on 08.12.2014.
 */
public abstract class ObserverControlBase {
    protected IsrServerLogger log;
    protected String logLevel;
    protected static final String logReference = "observer control";
    protected Process monitorProcess;
    protected String monitorStarterClassName;
    protected Path monitorStarterUserPath;
    protected DbData dbData;
    protected DbDataForLogging dbDataForLogging;
    protected Finalizer finalizer;
    protected String localHost;
    protected ObserverInterface observerInterface;
    protected String monitoringPath;


    public abstract void start(String[] args) throws Exception;

    public abstract void initLogger();

    protected void bootstrap() throws ClassNotFoundException, GeneralSecurityException, ZipException, IOException {
        Bootstrap bootstrap = new Bootstrap();
        Properties dbConnProperties = bootstrap.getProperties(Constants.RSA_FILE, Constants.RSA_LOG_FILE);

        dbData = new DbData(dbConnProperties);
        dbDataForLogging = new DbDataForLogging(dbConnProperties);
    }


    protected Finalizer initFinalizer(String[] args) {
        Finalizer finalizer = new Finalizer();
        finalizer.setArguments(args);

        finalizer.setMonitorClassName(monitorStarterClassName);
        finalizer.init(logLevel, logReference);
        return finalizer;
    }


    protected void startMonitoringProcess(String[] argsForStart, String[] argsForStop, long pingPeriod) {
        log.info(" try to start a process '" + monitorStarterClassName + "' with arguments " +
                ConvertUtils.arrayWithPasswordToString(argsForStart) + " to monitoring of Observer web service.");
        String error = "";
        Long processId = null;
        String processError = null;
        try {

            Path libPath = Paths.get(monitorStarterUserPath.toString(), ObserverConstants.LIB_PATH);
            Path javaPath = Paths.get(monitorStarterUserPath.toString(), ObserverConstants.JAVA_PATH);
            log.debug("libPath = " + libPath + "; javaPath = " + javaPath);
            monitorProcess = ProcessHelper.stopAndStartProcess(javaPath.toString(), libPath.toString() + File.separator + "*",
                    monitorStarterClassName, monitorStarterUserPath.toFile(), null, argsForStart, argsForStop);
            processId = ProcessHelper.getProcessId(monitorProcess);
            log.debug("New java process " + processId + " is started");
        } catch (Exception e) {
            error = e + "";
        }
        try {
            Thread.sleep(pingPeriod / 2 * 1000);
        } catch (InterruptedException e) {
            log.error(e);
        }
        String userDir = Constants.USER_DIR + File.separator + monitoringPath;
        File errorProcessFile = FileUtils.getFile(userDir, "error.log");
        log.info(errorProcessFile);
        if (errorProcessFile != null && errorProcessFile.exists()) {
            try {
                processError = FileUtils.readFileToString(errorProcessFile);
                log.info(processError);
            } catch (IOException e) {
                log.warn(e);
            }
        }
        error = error + processError;
        if (monitorProcess == null) {
            throw new RuntimeException("A process to monitoring of Observer web service can't be started in " + monitorStarterUserPath
                    + ". " + error);
        } else if (!monitorProcess.isAlive() || StringUtils.isNotEmpty(error)) {
            log.error(error);
            try {
                if (processId != null) {
                    ProcessHelper.killProcessForId(processId);
                }
            } catch (IOException | InterruptedException e) {
                log.warn("Process " + processId + " could not be killed " + e);
            }
            throw new RuntimeException("A process to monitoring of Observer web service can't be started in " + monitorStarterUserPath
                    + ". " + error);
        } else {
            log.info("A process to monitoring of Observer web service is started.");
        }
    }


    protected boolean startObserverWs(String observerPort, String observerContext, ObserverInterface observerInterface)
            throws MessageStackException {
        String serverUrl = UrlUtil.buildUrl("http", localHost, observerPort, observerContext);
        WsUtil wsUtil = new WsUtil();
        if (wsUtil.isServerAvailable(serverUrl + "?wsdl")) {
            log.warn("Address " + observerPort + " already in use. Possible, the web service is running.");
            return false;
        } else {
            log.always("Try to start the Observer web service on '" + serverUrl + "'");
            log.always("DB properties: " + dbData);
            Endpoint endpoint = UrlUtil.createAndPublishEndpoint(serverUrl, observerInterface);
            finalizer.setEndpoint(endpoint);
            log.info("ObserverService published on '" + serverUrl + "'");
            return true;
        }
    }


    public void readFromProcess(Process process) {
        if (process != null) {
            Reader reader = new InputStreamReader(process.getInputStream());
            int ch;
            try {
                while ((ch = reader.read()) != -1) {
                    System.out.print((char) ch);
                }
                reader.close();
            } catch (IOException e) {
                log.error(e.getMessage(), e);
            }
        }
    }


    protected void startPingScheduler(String[] argsForStart, String[] argsForStop, long period) {
        startThreadForPingWebServiceMonitor(argsForStart, argsForStop, period);
    }


    protected void startChildServers(XmlHandler serversXml, String observerPort) throws XPathExpressionException {
        NodeList servers = serversXml.selectNodes("//server");
        int serversNumber = servers.getLength();
        if (serversNumber > 0) {
            log.debug("There are " + serversNumber + " child servers found");
            for (int i = 0; i < serversNumber; i++) {
                try {
                    log.info("Try to start child server: " + i);
                    Node serverNode = servers.item(i);
                    Map<String, String> serverProperties = new XmlHandler().nodeToMap(serverNode);
                    serverProperties.put("logLevel", logLevel);
                    serverProperties.put("logReference", logReference);
                    serverProperties.put("observerPort", observerPort);
                    log.info("serverProperties: " + ConvertUtils.mapToString(serverProperties));
//                        log.debug("copyDbTokenToDirectory '" + serverProperties.get("serverPath") + "'");
//                        observerInterface.copyDbTokenToDirectory(serverProperties.get("serverPath"));
                    if (!"5".equals(serverProperties.get("serverStatus"))) {
                        String started = observerInterface.startServer(serverProperties);
                        log.debug("child server: " + i + "; started: " + started);
                    }
                } catch (Exception e) {
                    log.error("server can't be started " + e);
                }
            }
        }
    }

    private void startThreadForPingWebServiceMonitor(final String[] argsForStart,
                                                     final String[] argsForStop, Long period) {
        ScheduledExecutorService scheduler = Executors.newSingleThreadScheduledExecutor();

        scheduler.scheduleAtFixedRate(
                () -> {
                    try {
                        pingWebServiceMonitor(monitorStarterClassName, argsForStart, argsForStop, period);
                    } catch (Exception e) {
                        log.error(e);
                    }
                },
                0,
                period,
                TimeUnit.SECONDS);
    }


    private void pingWebServiceMonitor(String className, String[] arguments, String[] argsForStop, long period) {
//       log.debug(monitorProcess.isAlive());
        if (!monitorProcess.isAlive()) {
            log.info("Process " + className + " with arguments " + Arrays.toString(arguments) + " is not alive.");
            startMonitoringProcess(arguments, argsForStop, period * 3);
        }
    }

}
