package com.uptodata.isr.observers.controlling.finalize;

import com.uptodata.isr.server.utils.logging.log4j2.IsrServerLogger;
import com.uptodata.isr.server.utils.logging.log4j2.LogHelper;
import com.uptodata.isr.server.utils.process.ProcessHelper;

import javax.xml.ws.Endpoint;

/**
 * Created by schroedera85 on 04.12.2014.
 */
public class Finalizer {
    private Endpoint endpoint;
    private String[] arguments;
    private String[] argsForStop;
    private String monitorClassName;

    public void init(String logLevel, String logReference) {
        final IsrServerLogger log = LogHelper.initLogger(logLevel, logReference);
        Runtime.getRuntime().addShutdownHook(new Thread() {
            public void run() {
                try {
                    log.info("Shutdown!");
                    if (endpoint != null) {
                        log.info("Stop processId..");
                        endpoint.stop();
                    }
                    try {
                        Process process = ProcessHelper.stopAndStartProcess(System.getProperty("java.home"), System.getProperty("java.class.path"),
                                monitorClassName, null, null, arguments, argsForStop);
                        Long processId = ProcessHelper.getProcessId(process);
                        log.debug("New java process " + processId + " is started");

                    } catch (Exception e) {
                        log.error(e.getMessage(), e);
                    }
                    log.info("End shutdown");
                } catch (Exception e) {
                    log.error("error during stop observer: " + e.getMessage(), e);
                }
            }
        });
    }


    public Endpoint getEndpoint() {
        return endpoint;
    }

    public void setEndpoint(Endpoint endpoint) {
        this.endpoint = endpoint;
    }

    public String[] getArguments() {
        return arguments;
    }

    public void setArguments(String[] arguments) {
        this.arguments = arguments;
    }

    public String getMonitorClassName() {
        return monitorClassName;
    }

    public void setMonitorClassName(String monitorClassName) {
        this.monitorClassName = monitorClassName;
    }

    public String[] getArgsForStop() {
        return argsForStop;
    }

    public void setArgsForStop(String[] argsForStop) {
        this.argsForStop = argsForStop;
    }
}
