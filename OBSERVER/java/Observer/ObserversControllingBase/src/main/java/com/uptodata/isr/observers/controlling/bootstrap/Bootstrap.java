package com.uptodata.isr.observers.controlling.bootstrap;


import com.uptodata.isr.observers.base.secutity.CryptDbPropertiesHandler;
import com.uptodata.isr.observers.controlling.tokenGui.ObserverDbToken;
import com.uptodata.isr.server.utils.fileSystem.Constants;
import net.lingala.zip4j.exception.ZipException;
import org.apache.commons.io.FileUtils;

import java.io.File;
import java.io.IOException;
import java.security.GeneralSecurityException;
import java.util.Properties;

/**
 * Created by schroedera85 on 04.12.2014.
 */
public class Bootstrap {
    CryptDbPropertiesHandler cryptDbPropertiesHandler;

    public Properties getProperties(String dbServletFile, String logFileName) throws ClassNotFoundException, GeneralSecurityException, ZipException, IOException {
        cryptDbPropertiesHandler = new CryptDbPropertiesHandler(dbServletFile, logFileName) {
            @Override
            protected void onServletTokenNotExist() {
                new ObserverDbToken().showDialog(false);
            }

            @Override
            protected void onLogTokenNotExist() {
                new ObserverDbToken().showDialog(false);
            }
        };
        return cryptDbPropertiesHandler.getProperties();
    }


    public static void main(String[] args) {
        Bootstrap bootstrap = new Bootstrap();
        try {
            Properties properties = bootstrap.getProperties("E:\\temp\\dbToken.rsa", "E:\\temp\\dbTokenLog.rsa");
            System.out.println(properties);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}
