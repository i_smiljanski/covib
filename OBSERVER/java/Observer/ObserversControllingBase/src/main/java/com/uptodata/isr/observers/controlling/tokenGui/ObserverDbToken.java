package com.uptodata.isr.observers.controlling.tokenGui;

import com.jidesoft.dialog.ButtonPanel;
import com.jidesoft.dialog.JideOptionPane;
import com.jidesoft.swing.TitledSeparator;
import com.uptodata.isr.gui.util.GuiUtility;
import com.uptodata.isr.observers.base.dbAccess.DbData;
import com.uptodata.isr.server.utils.fileSystem.Constants;
import com.uptodata.isr.server.utils.logging.log4j2.DbDataForLogging;
import com.uptodata.isr.server.utils.security.ProtectionUtil;

import javax.swing.*;
import javax.swing.border.EmptyBorder;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.Properties;

/**
 * Created by smiljanskii60 on 20.10.2014.
 */
public class ObserverDbToken {
    // public static final Logger log = LogManager.getLogger(ObserverDbToken.class);

    private JTextField dbHost;
    private JTextField schemaName;

    private JTextField jdbcUrlForLog;
    private JPasswordField passwordForLog;

    private JTextField dbWsPort;
    private JTextField dbWsUser;
    private JPasswordField dbWsPassword;
//    private String tokenFileName;

//    public ObserverDbToken(String tokenFileName) {
//        this.tokenFileName = tokenFileName;
//    }


    public void showDialog(boolean dispose) {

        schemaName = new JTextField();
        dbHost = new JTextField();
        dbWsPort = new JTextField();
        dbWsUser = new JTextField();
        dbWsPassword = new JPasswordField();

        jdbcUrlForLog = new JTextField();
        passwordForLog = new JPasswordField();

        // todo: it's a test, later delete?
        schemaName = new JTextField("ISROWNER");
        dbHost = new JTextField("db_host");
        dbWsPort = new JTextField("8082");
        dbWsUser = new JTextField("isrws");
//        dbWsPassword = new JPasswordField("");
        jdbcUrlForLog = new JTextField("jdbc:oracle:thin:@db_host:1521:sid");
//        passwordForLog = new JPasswordField("");
        //todo: end test

        final JDialog f = new JDialog();
        f.setTitle("Database token");
        f.setModal(true);
        if (dispose) {
            f.setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
        }
//        ClassLoader loader = Thread.currentThread().getContextClassLoader();
//        f.setIconImage(new ImageIcon(loader.getResource("iSR30-48.png")).getImage());

        JPanel form = new JPanel();
        f.getContentPane().setLayout(new BorderLayout());
        f.getContentPane().add(form, BorderLayout.NORTH);

        form.setLayout(new GridBagLayout());
        GuiUtility formUtility = new GuiUtility();

        JLabel label1 = new JLabel("Database connection");
        label1.setForeground(Color.BLUE);
        formUtility.addLastField(new TitledSeparator(label1, SwingConstants.CENTER), form);

        createComponentWithLabel(formUtility, form, "Schema name", schemaName);
        createComponentWithLabel(formUtility, form, "JDBC url", jdbcUrlForLog);
        createComponentWithLabel(formUtility, form, "Password", passwordForLog);

        JLabel label2 = new JLabel("Database web service");
        label2.setForeground(Color.BLUE);
        formUtility.addLastField(new TitledSeparator(label2, SwingConstants.CENTER), form);

        createComponentWithLabel(formUtility, form, "DB host", dbHost);
        createComponentWithLabel(formUtility, form, "DB web service port", dbWsPort);
        createComponentWithLabel(formUtility, form, "DB web service dbWsUser", dbWsUser);
        createComponentWithLabel(formUtility, form, "DB web service dbWsPassword", dbWsPassword);

        JButton button = new JButton("Save");
        button.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent event) {
                try {
                    DbData dbData = new DbData(schemaName.getText(), dbHost.getText(), dbWsPort.getText(),
                            dbWsUser.getText(), String.valueOf(dbWsPassword.getPassword()));
                    Properties servletProps = dbData.getProperties();
                    ProtectionUtil.saveEncryptedProperties(servletProps, Constants.RSA_FILE, Constants.KEY_CLASS, Constants.JAR_FILE1);

                    DbDataForLogging dbDataForLogging = new DbDataForLogging(schemaName.getText(),
                            String.valueOf(passwordForLog.getPassword()), jdbcUrlForLog.getText());
                    ProtectionUtil.saveEncryptedProperties(dbDataForLogging.getProperties(),
                            Constants.RSA_LOG_FILE, Constants.KEY_LOG_CLASS, Constants.JAR_FILE2);


//                    File userDir = new File(Constants.USER_DIR);
//                    File newFile = new File(userDir + File.separator + "dbTokenChanged.txt");
//                    newFile.createNewFile();
//                try {
//                    for (File directory : userDir.listFiles(File::isDirectory)) {
//                        if (!dirctory.getName().equals("logs") && !directory.getName().equals("lib")) {
//                            newFile = new File(directory + File.separator + ObserverConstants.dBTokenChangedFile);
//                            newFile.createNewFile();
//                        }
//                    }
//                } catch (IOException e1) {
//                    e1.printStackTrace();
//                }

//                f.dispose();
                    int confirm = JideOptionPane.showConfirmDialog(f, "Data stored, restart the observer now!",
                            "Database", JOptionPane.DEFAULT_OPTION);
                    if (confirm == JOptionPane.YES_OPTION) {
                        System.exit(0);
                    }

                } catch (Exception e) {
                    e.printStackTrace();
                    int confirm = JideOptionPane.showConfirmDialog(f, "Data could not be stored! " + e,
                            "Database", JOptionPane.DEFAULT_OPTION);
                    if (confirm == JOptionPane.YES_OPTION) {
                        System.exit(0);
                    }
                }
            }
        });

        ButtonPanel buttonPanel = new ButtonPanel();
        buttonPanel.add(button, ButtonPanel.OTHER_BUTTON);
        f.getContentPane().add(buttonPanel, BorderLayout.AFTER_LAST_LINE);
        //  formUtility.addLastField(buttonPanel,  form);

        // Add an little padding around the form
        form.setBorder(new EmptyBorder(2, 2, 2, 2));

        f.setSize(400, 250);
        f.setLocationRelativeTo(null);
        f.setVisible(true);
    }


    private void createComponentWithLabel(GuiUtility formUtility, JPanel form, String labelText, JTextField textField) {
        formUtility.addLabel(labelText, form);
        formUtility.addLastField(textField, form);
    }


    public static void main(String[] args) {
//        ClassLoader loader = Thread.currentThread().getContextClassLoader();
//        InputStream is = loader.getResourceAsStream("dbToken.txt");
        new ObserverDbToken().showDialog(true);
    }
}
