package com.uptodata.isr.observer.ws;

import com.uptodata.isr.observer.transfer.DbFileSupplier;
import com.uptodata.isr.server.utils.fileSystem.FileSystemWorker;
import com.uptodata.isr.server.utils.logging.log4j2.IsrServerLogger;
import com.uptodata.isr.server.utils.logging.log4j2.LogHelper;
import com.uptodata.isr.server.utils.serverProcess.MultipleProcessExecutor;
import com.uptodata.isr.server.utils.serverProcess.ProcessInfoObject;
import com.uptodata.isr.utils.exceptions.MessageStackException;

import java.io.File;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.nio.file.attribute.BasicFileAttributeView;
import java.nio.file.attribute.BasicFileAttributes;
import java.time.LocalDate;
import java.time.ZoneId;
import java.time.format.DateTimeFormatter;
import java.util.Date;
import java.util.Iterator;
import java.util.concurrent.TimeUnit;

/**
 * Created by smiljanskii60 on 12.11.2015.
 */
public class FilesForDateZipper extends MultipleProcessExecutor {
    static IsrServerLogger log;
    DbFileSupplier fileWorker;
    String startDate;
    String endDate;
    String datePattern;
    File sourceFolder;

    public FilesForDateZipper(DbFileSupplier fileWorker, File sourceFolder, String startDate, String endDate,
                              String datePattern, String logLevel, String logReference) {
        super(logLevel, logReference, "");
        log = LogHelper.initLogger(logLevel, logReference);
        this.fileWorker = fileWorker;
        this.sourceFolder = sourceFolder;
        this.startDate = startDate;
        this.endDate = endDate;
        this.datePattern = datePattern;
    }

    @Override
    protected ProcessCallable createThread(ProcessInfoObject processInfoObject) {
        return new ProcessCallable(processInfoObject) {
            @Override
            protected void onExecuteFail(Exception e) {
                //todo
                log.error(e);
            }

            @Override
            protected boolean executeProcess() throws MessageStackException {
                boolean result = false;
                try {
                    log = initLogger(logLevel, logReference);
                    LocalDate startLocalDate = LocalDate.parse(startDate, DateTimeFormatter.ofPattern(datePattern));
                    LocalDate endLocalDate = LocalDate.parse(endDate, DateTimeFormatter.ofPattern(datePattern));
                    log.info("Try to zip files in folder " + sourceFolder + " from " + startLocalDate + " to " + endLocalDate);

                    Path inputFolder = FileSystemWorker.getOrCreateFolders(fileWorker.getWorkFolder().getCanonicalPath(), fileWorker.inputFolderName);
                    log.debug("inputFolder = " + inputFolder);
                    if (inputFolder != null) {
                        saveFilesOfDateToFolder(Paths.get(sourceFolder.getCanonicalPath()), inputFolder, startLocalDate, endLocalDate);

                        Path resultFolder = Paths.get(fileWorker.getWorkFolder().getCanonicalPath(), fileWorker.resultFolderName);
                        File zipFile = new File(resultFolder.toFile(), processInfoObject.getResultFileName());
                        FileSystemWorker.packFolderToZip(zipFile, inputFolder.toFile());
                        result = true;
                    }
                } catch (Exception e) {
//                    onExecuteFail(e);
                    String errorMsg = "Error by zip file " + processInfoObject + System.lineSeparator() + e.getMessage();
                    throw new MessageStackException(e, MessageStackException.SEVERITY_CRITICAL, errorMsg,
                            MessageStackException.getCurrentMethod());
                }
                return result;
            }
        };
    }


    private void saveFilesOfDateToFolder(Path sourceFolder, Path targetFolder, LocalDate startLocalDate,
                                         LocalDate endLocalDate) throws IOException {
        for (Iterator it = Files.walk(sourceFolder).iterator(); it.hasNext(); ) {
            Path logFile = (Path) it.next();
            if (!logFile.toFile().isDirectory()) {
                BasicFileAttributes view = Files.getFileAttributeView(logFile, BasicFileAttributeView.class).readAttributes();
                Date lastModDate = new Date(view.lastModifiedTime().to(TimeUnit.MILLISECONDS));
                LocalDate date = lastModDate.toInstant().atZone(ZoneId.systemDefault()).toLocalDate();

                if (date.compareTo(startLocalDate) > -1 || date.compareTo(endLocalDate) > -1) {
                    Path newFile = Paths.get(targetFolder.toFile().getCanonicalPath(), logFile.toFile().getName());
                    Files.copy(logFile, newFile);
                    log.debug("file " + newFile + " stored in folder " + targetFolder);
                }
            }
        }
    }

    public static void main(String[] args) {
        LocalDate localDate = LocalDate.parse("08.03.2016", DateTimeFormatter.ofPattern("dd.MM.yyyy"));
        log.debug(localDate);
    }

}
