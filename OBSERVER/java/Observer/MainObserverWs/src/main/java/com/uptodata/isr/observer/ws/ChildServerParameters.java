package com.uptodata.isr.observer.ws;

import com.uptodata.isr.observers.base.dbAccess.DbData;
import com.uptodata.isr.server.utils.logging.log4j2.DbDataForLogging;
import org.apache.commons.lang.StringUtils;

import java.util.Properties;

/**
 * Created by smiljanskii60 on 15.06.2015.
 */
public class ChildServerParameters {
    DbData dbData;
    DbDataForLogging dbDataForLogging;
    String wsUrl;
    String logLevel;
    Properties properties = new Properties();

    public ChildServerParameters(DbData dbData, DbDataForLogging dbDataForLogging, String wsUrl, String logLevel)
            throws Exception {
        this.dbData = dbData;
        this.dbDataForLogging = dbDataForLogging;
        this.wsUrl = wsUrl;
        this.logLevel = logLevel;
        validateParams();
        properties.putAll(dbData.getProperties());
        properties.putAll(dbDataForLogging.getProperties());
        properties.setProperty("wsUrl", wsUrl);
        properties.setProperty("logLevel", logLevel);
    }

    public  void validateParams() throws Exception {
        StringBuilder validate = new StringBuilder();
        if (StringUtils.isEmpty(wsUrl)) {
            validate.append("wsUrl is null! ");
        }
        if (StringUtils.isEmpty(logLevel)) {
            validate.append("logLevel is null! ");
        }
        dbData.validate();
        dbDataForLogging.validate();
        if (validate.length() > 0) {
            throw new Exception(validate.toString());
        }
    }

    public Properties getProperties() {
        return properties;
    }
}
