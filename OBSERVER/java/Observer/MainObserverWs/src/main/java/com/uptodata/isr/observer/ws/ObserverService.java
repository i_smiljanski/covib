package com.uptodata.isr.observer.ws;


import com.uptodata.isr.observer.transfer.DbFileSupplier;
import com.uptodata.isr.observers.base.constants.ObserverConstants;
import com.uptodata.isr.observers.base.dbAccess.DbAccessConstants;
import com.uptodata.isr.observers.base.dbAccess.DbData;
import com.uptodata.isr.observers.base.dbAccess.DbServletsFunctions;
import com.uptodata.isr.observers.base.serverData.ProcessServerData;
import com.uptodata.isr.observers.base.serverData.ServerData;
import com.uptodata.isr.observers.base.ws.ObserverInterfaceImpl;
import com.uptodata.isr.server.utils.fileSystem.Constants;
import com.uptodata.isr.server.utils.fileSystem.FileSystemWorker;
import com.uptodata.isr.server.utils.logging.log4j2.DbDataForLogging;
import com.uptodata.isr.server.utils.logging.log4j2.IsrServerLogger;
import com.uptodata.isr.server.utils.logging.log4j2.LogHelper;
import com.uptodata.isr.server.utils.network.UrlUtil;
import com.uptodata.isr.server.utils.process.ProcessHelper;
import com.uptodata.isr.server.utils.serverProcess.ProcessInfoObject;
import com.uptodata.isr.utils.ConvertUtils;
import com.uptodata.isr.utils.exceptions.MessageStackException;
import com.uptodata.isr.utils.ws.WsUtil;
import org.apache.commons.collections4.map.CaseInsensitiveMap;
import org.apache.commons.io.FileUtils;
import org.apache.commons.lang.StringUtils;

import javax.jws.WebService;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.net.URL;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

/**
 * Created by smiljanskii60 on 17.07.2014.
 */
@WebService(endpointInterface = "com.uptodata.isr.observer.ws.ObserverService")
public class ObserverService extends ObserverInterfaceImpl {

    public ObserverService() {
    }

    public ObserverService(String localHost, String logLevel, String logReference, DbData dbData, DbDataForLogging dbDataForLogging) {
        super(localHost, logLevel, logReference, dbData, dbDataForLogging);
    }

    @Override
    public boolean isServerAvailable(ServerData serverData, int timeout) {
        String url = serverData.getUrl();
        String namespace = serverData.getNamespace();
        boolean isAvailable = new WsUtil().pingSoapWs(url + "?wsdl", namespace, timeout);
        if (!isAvailable) {
            log.trace("server is not available; serverData:" + serverData);
        }
        return isAvailable;
    }

    @Override
    protected ProcessServerData tryToStartServer(Map<String, String> serverProperties) throws Exception {
        String webserviceClass = serverProperties.get("webserviceClass");
        String host = serverProperties.get("observerHost");
        String observerPort = serverProperties.get("observerPort");
        String port = serverProperties.get("serverPort");
        String serverId = serverProperties.get("serverid");
        String context = serverProperties.get("context");
        String serverPath = serverProperties.get("serverPath");
        String javaVmOptions = serverProperties.get("javaVmOptions");
        String timeout = serverProperties.get("timeout");

        log.always("try to start server " + webserviceClass + " on port " + port);

        if (host == null) {
            host = localHost;
        }

        if (host == null) {
            String errorString = " host is null!";
            log.warn(errorString);
            int severity = MessageStackException.SEVERITY_CRITICAL;
            String userStack = "server can't be started on the Observer.";
            throw new MessageStackException(severity, userStack, errorString, MessageStackException.getCurrentMethod());
        }

        findChildServers(observerPort, host);
        if (childServersFromDb == null) {
            throw new MessageStackException("there are no child servers!");
        }
        if (childServersFromDb.get(serverId) == null) {
            throw new MessageStackException("child server " + serverId + " is null");
        }


        String sUrl = UrlUtil.buildUrl(DbAccessConstants.dbServletProtocol, host, port, context);


        ChildServerParameters params = new ChildServerParameters(dbData, dbDataForLogging, sUrl, logLevel);
        String[] argsForStart = ConvertUtils.convertPropertiesToArray(params.getProperties());

        String[] argsForStop = new String[]{port};

        log.debug("argsForStart ==" + ConvertUtils.arrayWithPasswordToString(argsForStart));
        Path userDirPath = Paths.get(Constants.USER_DIR, serverPath);
        if (Files.notExists(userDirPath)) {
            throw new FileNotFoundException("Could not find file: " + userDirPath.toString());
        }

        Path libPath = Paths.get(userDirPath.toString(), ObserverConstants.LIB_PATH);
        Path javaPath = Paths.get(userDirPath.toString(), ObserverConstants.JAVA_PATH);
        log.debug("libPath = " + libPath + "; javaPath = " + javaPath);
        Process process = ProcessHelper.stopAndStartProcess(javaPath.toString(), libPath.toString() + File.separator + "*",
                webserviceClass, userDirPath.toFile(),
                javaVmOptions, argsForStart, argsForStop);
        Long processId = ProcessHelper.getProcessId(process);
        log.info("process " + processId + " for server id = " + serverId + " started.");
        long startTime = System.currentTimeMillis();
        long maxTime = 50000;

        if (StringUtils.isNotEmpty(timeout)) {
            try {
                maxTime = Long.parseLong(timeout) * 1000;
                if (maxTime < 1) {
                    maxTime = 500000;
                }
            } catch (NumberFormatException e) {
                log.info(e + " Default timeout: " + maxTime / 1000);
                //maxType stays default
            }
        }

        String error = null;
        boolean isAvailable = false;
        WsUtil wsUtil = new WsUtil();
        while (System.currentTimeMillis() - startTime < maxTime) {
            isAvailable = wsUtil.pingSoapWs(sUrl + "?wsdl", serverProperties.get("namespace"), 10);
            if (isAvailable) {
                log.debug(sUrl + "?wsdl is available!");
                break;
            } else {
                File errorProcessFile = FileUtils.getFile(userDirPath.toFile(), "error.log");
                if (errorProcessFile != null && errorProcessFile.exists()) {
                    try {
                        error = FileUtils.readFileToString(errorProcessFile);
                    } catch (IOException e) {
                        log.warn(e);
                    }
//                    boolean isDeleted = errorProcessFile.delete();
                    if (StringUtils.isNotEmpty(error)) {
                        log.debug("break");
                        break;
                    }
                }
            }
        }
//        isAvailable = wsUtil.pingSoapWs(sUrl + "?wsdl", serverProperties.get("namespace"), 10);
        if (!isAvailable) {
            error = "Server " + sUrl + "?wsdl is not available after " + maxTime / 1000.0 +
                    " sec. Reason: " + StringUtils.defaultIfEmpty(error, "unexpected error");
            log.error("error==" + error);
            try {
                ProcessHelper.killProcessForId(processId);
            } catch (IOException | InterruptedException e) {
                log.warn("Process " + processId + " could not be killed " + e);
            }
            throw new Exception(error);
        } else {
            log.info("Service " + sUrl + " is available");

            ProcessServerData serverData = new ProcessServerData(ProcessHelper.getProcessId(process),
                    childServersFromDb.get(serverId));
            log.always("server " + serverData + " started");
            return serverData;
        }
    }

    @Override
    protected void validateStartServerArgs(CaseInsensitiveMap<String, String> map) throws MessageStackException {
        String errorString = ConvertUtils.validateMapForArray(map,
                new String[]{"webserviceClass", "serverport", "serverId", "namespace", "context", "logLevel",
                        "serverPath", "logReference"});
        if (StringUtils.isNotEmpty(errorString)) {
            log.warn(errorString);
            int severity = MessageStackException.SEVERITY_CRITICAL;
            String userStack = "server  can't be started on the Observer: " + errorString;
            throw new MessageStackException(severity, userStack, errorString, MessageStackException.getCurrentMethod());
        } else {
            log.debug("validate success");
        }
    }

    @Override
    protected void validateStopServerArgs(CaseInsensitiveMap<String, String> map) throws MessageStackException {
        String[] keys = {"serverId", "logLevel"};
        String errorString = ConvertUtils.validateMapForArray(map, keys);

        if (StringUtils.isNotEmpty(errorString)) {
            log.warn(errorString);
            int severity = MessageStackException.SEVERITY_CRITICAL;
            String userStack = "server  can't be stopped on the Observer: " + errorString;
            throw new MessageStackException(severity, userStack, errorString, MessageStackException.getCurrentMethod());
        }
    }

    @Override
    protected boolean tryToStopServer(ProcessServerData serverData) throws Exception {
        if (serverData == null) {
            throw new Exception("serverData is null!");
        }
        Long processId = serverData.getProcessId();
        log.debug("try to stop process id " + processId);
        try {
            ProcessHelper.killProcessForId(processId);
            log.always("process " + processId + " for server " + serverData.getServerId() + " stopped");
        } catch (Exception e) {
            String errorMsg = "process with id = " + processId + " could not be killed. ";
            log.error(errorMsg);
            throw new Exception(errorMsg + e);
        }
        return true;
    }


    public String createChildServerDirectory(String dirName, String folders, String concat, String logLevel, String logReference)
            throws MessageStackException {
        CaseInsensitiveMap<String, String> args = new CaseInsensitiveMap<>();
        args.put("folderName", dirName);
        args.put("folders", folders);
        args.put("logLevel", logLevel);
        args.put("logReference", logReference);
        args.put("concat", concat);
        validateCreateServerDirectory(dirName);
        if (StringUtils.isEmpty(logLevel)) {
            logLevel = LogHelper.defaultLevel;
        }
        IsrServerLogger log = initLogger(logLevel, logReference);
        createMethodNode("createChildServerDirectory", args);
        log.info(args);
        try {
            log.info("try to create directory " + dirName);
            Path serverFolder = Paths.get(Constants.USER_DIR, dirName);
            if (Files.notExists(serverFolder)) {
                serverFolder = Files.createDirectory(serverFolder);
            }
            if (StringUtils.isNotEmpty(folders) && Files.isWritable(serverFolder)) {
                for (String subDir : folders.split(concat)) {
                    Path subDirPath = Paths.get(serverFolder.toFile().getPath(), subDir.trim());
                    if (Files.notExists(subDirPath)) {
                        subDirPath = Files.createDirectory(subDirPath);
                    }
                }
            }

//            log.debug("copyDbTokenToDirectory '" + dirName + "'");
//            copyDbTokenToDirectory(dirName);
            onSuccessCreateDirectory("directory '" + dirName + "' with folders '" + folders + "' created successfully");
            return "T";
        } catch (Exception e) {
            String errorMsg = "directory '" + dirName + "' with folders '" + folders + "' can't be created";
            onMethodFailed(e, MessageStackException.SEVERITY_CRITICAL, errorMsg,
                    MessageStackException.getCurrentMethod(), statusXmlMethod);
        }
        return "F";
    }


    public String deleteChildServerDirectory(String dirName, String logLevel, String logReference) throws MessageStackException {
        CaseInsensitiveMap<String, String> args = getArgsForDeleteDir(dirName, logLevel, logReference);
        log = initLogger(logLevel, logReference);
        createMethodNode("deleteChildServerDirectory", args);
        try {
            log.debug(args);
            log.info("try to delete directory " + dirName);
            Path serverFolder = Paths.get(Constants.USER_DIR, dirName);
            //todo check it
            FileSystemWorker.removeFolder(serverFolder.toFile());
            onMethodSuccess("directory '" + dirName + " deleted successfully", statusXmlMethod);
            return "T";
        } catch (Exception e) {
            onFailDeleteDirectory("directory '" + dirName + "' can't be deleted", e);
        }
        return "F";
    }


    public String deleteFilesInDirectory(String dirName, String logLevel, String logReference) throws MessageStackException {
        CaseInsensitiveMap<String, String> args = getArgsForDeleteDir(dirName, logLevel, logReference);
        createMethodNode("deleteFilesInDirectory", args);
        log = initLogger(logLevel, logReference);
        try {
            log.debug(args);
            log.info("try to delete files in " + dirName);
            Path serverFolder = Paths.get(Constants.USER_DIR, dirName);
            //todo check it
            FileSystemWorker.removeFolder(serverFolder.toFile(), false);
            String successText = "files in '" + dirName + "' deleted successfully";
            log.info(successText);
            return "T";
        } catch (Exception e) {
            String errorText = "files in '" + dirName + "' can't be deleted. ";
            log.error(errorText + e);
        }
        return "F";
    }


    private CaseInsensitiveMap<String, String> getArgsForDeleteDir(String dirName, String logLevel, String logReference) throws MessageStackException {
        CaseInsensitiveMap<String, String> args = new CaseInsensitiveMap<>();
        args.put("folderName", dirName);
        args.put("logLevel", logLevel);
        args.put("logReference", logReference);
        validateDeleteServerDirectory(dirName);
        if (StringUtils.isEmpty(logLevel)) {
            logLevel = LogHelper.defaultLevel;
        }
        return args;
    }


    private void onFailDeleteDirectory(String failText, Exception e) throws MessageStackException {
        String implStack = getClass().getSimpleName() + ".deleteChildServerDirectory";
        onMethodFailed(e, MessageStackException.SEVERITY_WARNING, failText, implStack, statusXmlMethod);
    }


    public void uploadLogFiles(String serverFolder, String startDate, String endDate, String configName, String keyId,
                               String datePattern, String logLevel, String logReference) throws MessageStackException {
        String userErrorMsg = "Upload log file to database is impossible";

        CaseInsensitiveMap<String, String> params = new CaseInsensitiveMap<String, String>();
        params.put("configName", configName);
        params.put("keyId", keyId);
        params.put("serverFolder", serverFolder);
        params.put("startDate", startDate);
        params.put("endDate", endDate);
        params.put("datePattern", datePattern);
        params.put("logLevel", logLevel);
        params.put("logReference", logReference);
        log = initLogger(logLevel, logReference);

        createMethodNode("uploadLogFiles", params);
        DbFileSupplier fileWorker = null;
        try {
            Path logPath = FileSystemWorker.getOrCreateFolders(Constants.USER_DIR, serverFolder, "logs");
            File workFolder = new File(logPath.toFile(), "temp_" + keyId);
            log.debug(workFolder);
            fileWorker = new DbFileSupplier(workFolder, dbData);
            fileWorker.init(null, null, configName);

            FilesForDateZipper fileExecutor = new FilesForDateZipper(fileWorker, logPath.toFile(), startDate,
                    endDate, datePattern, logLevel, logReference);

            String resultFileName = keyId + ".zip";
            ProcessInfoObject processObject = new ProcessInfoObject(resultFileName, keyId);
            ArrayList<ProcessInfoObject> objList = new ArrayList<>();
            objList.add(processObject);
            HashMap<ProcessInfoObject, Boolean> results = fileExecutor.executeProcesses(objList, log);

            for (Map.Entry<ProcessInfoObject, Boolean> resultObject : results.entrySet()) {
                ProcessInfoObject infoObj = resultObject.getKey();
                log.debug("infoObj: " + infoObj);
                Boolean resultSuccess = resultObject.getValue();
                String resultId = infoObj.getResultId();
                File resultFile = fileWorker.getResultFile(resultFileName);
                log.debug("resultFile: " + resultFile);

                boolean isUploaded = false;
                log.debug("resultSuccess: " + resultSuccess);
                if (resultSuccess) {
                    isUploaded = fileWorker.uploadFile(resultFile, resultId, configName, logLevel, logReference);
                }
                boolean fullResult = resultSuccess && isUploaded;
                log.debug(infoObj.getResultId() + " : " + fullResult);
            }

        } catch (Exception e) {
            //  e.printStackTrace();
            log.error("Exception: " + e);
            int severity = MessageStackException.SEVERITY_CRITICAL;
            onMethodFailed(e, severity, userErrorMsg, MessageStackException.getCurrentMethod(), statusXmlMethod);
        } finally {
            try {
                if (fileWorker != null) {
                    fileWorker.clearFileSystem();
                }
            } catch (MessageStackException e) {
                log.error(e);
            }
        }
    }


    public void uploadDbTokenFiles(String configName, String observerPort, String localHost, String logLevel, String logReference) throws MessageStackException {

        CaseInsensitiveMap<String, String> params = new CaseInsensitiveMap<String, String>();
        params.put("configName", configName);
        params.put("observerPort", observerPort);
        params.put("observerhost", localHost);
        params.put("logLevel", logLevel);
        params.put("logReference", logReference);
        log = initLogger(logLevel, logReference);

        createMethodNode("uploadDbTokenFiles", params);
        DbFileSupplier fileWorker = null;
        try {
            String serverId = DbServletsFunctions.getServerId(dbData, observerPort, localHost);
            if (StringUtils.isEmpty(serverId) || serverId.trim().equals("null")) {
                throw new Exception("Observer with host = " + localHost + " and port = " + observerPort + " is unknown in the database. Maybe it is not installed.");
            }
            serverId = serverId.trim();
            log.debug("serverId = " + serverId);

            File rsaFile = FileUtils.getFile(Constants.RSA_FILE);
            File keyFile = FileUtils.getFile(Constants.LIB_PATH, Constants.JAR_FILE1);

            Path tmpPath = FileSystemWorker.getOrCreateFolders(Constants.USER_DIR, "temp");
            File workFolder = tmpPath.toFile();

            log.debug(workFolder);
            fileWorker = new DbFileSupplier(workFolder, dbData);

            boolean isUploaded = fileWorker.uploadFile(rsaFile, serverId + ";" + Constants.RSA_FILE, configName, logLevel, logReference);
            if (isUploaded) {
                log.info("File " + Constants.RSA_FILE + " uploaded successfully");
            }
            isUploaded = fileWorker.uploadFile(keyFile, serverId + ";" + Constants.JAR_FILE1, configName, logLevel, logReference);
            if (isUploaded) {
                log.info("File " + Constants.JAR_FILE1 + " uploaded successfully");
            }
        } catch (Exception e) {
            //  e.printStackTrace();
            log.error("Exception: " + e);
            int severity = MessageStackException.SEVERITY_CRITICAL;
            String userErrorMsg = "Upload db properties file to database is impossible";
            onMethodFailed(e, severity, userErrorMsg, MessageStackException.getCurrentMethod(), statusXmlMethod);
        } finally {
            try {
                if (fileWorker != null) {
                    fileWorker.clearFileSystem();
                }
            } catch (MessageStackException e) {
                log.error(e);
            }
        }
    }


    public String downloadAndExtractFile(CaseInsensitiveMap<String, String> map) throws Exception {
        String logLevel = map.get("logLevel");
        String logReference = map.get("logReference");
        log = initLogger(logLevel, logReference);
        validateDownloadAndExtractFile(map);
        log.info("downloadAndExtractFile params: " + map);
        createMethodNode("downloadAndExtractFile", map);
//        dbData.doAuthenticate();
        URL dbServletUrl = null;
        try {
            dbServletUrl = getUrlForDownloadFile(dbData, map.get("configName"), map.get("keyId"), map.get("zipped"),
                    logLevel, logReference);
        } catch (Exception e) {
            throw new Exception("Create url for file download failed. " + e);
        }
        log.debug("get file from db servlet " + dbServletUrl);
        File downloadFile = downloadFile(map.get("fileName"), map.get("target"), map.get("zipped"),
                dbServletUrl, map.get("logLevel"), map.get("logReference"));
        log.debug("length=" + downloadFile.length());
        try {
            log.info("file " + downloadFile.getCanonicalPath() + " saved.");
        } catch (IOException e) {
            log.info(e);
        }
        onMethodSuccess("success", statusXmlMethod);
        return "T";
    }


    private void validateDownloadAndExtractFile(CaseInsensitiveMap<String, String> map) throws MessageStackException {
        String[] keysToValidate = {"fileName", "target", "zipped", "keyId", "configName", "logReference", "logLevel"};
        String errorString = ConvertUtils.validateMapForArray(map, keysToValidate);
        if (StringUtils.isNotEmpty(errorString)) {
            log.warn(errorString);
            int severity = MessageStackException.SEVERITY_CRITICAL;
            String userStack = "error on remote server. " + errorString;
            throw new MessageStackException(severity, userStack, errorString, MessageStackException.getCurrentMethod());
        } else {
            log.debug("validate success");
        }
    }

    private void validateCreateServerDirectory(String dirName) throws MessageStackException {

        String errorString = null;
        if (StringUtils.isEmpty(dirName)) {
            errorString = "directory name is null!" + System.lineSeparator();
        }

        if (errorString != null) {
            log.warn(errorString);
            int severity = MessageStackException.SEVERITY_CRITICAL;
            String userStack = "directory can't be created: " + errorString;
            throw new MessageStackException(severity, userStack, errorString, MessageStackException.getCurrentMethod());
        }
    }

    private void validateDeleteServerDirectory(String dirName) throws MessageStackException {

        String errorString = null;
        if (StringUtils.isEmpty(dirName)) {
            errorString = "directory name is null!" + System.lineSeparator();
        }

        if (errorString != null) {
            log.warn(errorString);
            int severity = MessageStackException.SEVERITY_CRITICAL;
            String userStack = "directory can't be deleted: " + errorString;
            throw new MessageStackException(severity, userStack, errorString, MessageStackException.getCurrentMethod());
        }
    }

    private void onSuccessCreateDirectory(String successText) {
        onMethodSuccess(successText, statusXmlMethod);
    }

    public static void main(String[] args) {
        try {
            /*ObserverService os = new ObserverService();
            os.dbData = new DbData("ISROWNER_ISRC_RC_4", "isrc-d-d.utddomain.utd", "8082", "isrws_isrc_rc_4", "isrws_isrc_rc_4");
            os.dbData.doAuthenticate();
            URL dbServletUrl;

            dbServletUrl = os.getUrlForDownloadFile(os.dbData, "SERVER_DEPLOYMENT", "1", "T");
            File downloadFile2 = os.downloadFile("test.zip", "JDBC Remote Collector 47/jre", "T", dbServletUrl, "INFO", "test");*/

        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}
