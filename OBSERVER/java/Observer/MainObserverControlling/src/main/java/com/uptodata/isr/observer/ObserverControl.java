package com.uptodata.isr.observer;

import com.uptodata.isr.observer.ws.ObserverService;
import com.uptodata.isr.observers.base.dbAccess.DbServletsFunctions;
import com.uptodata.isr.observers.controlling.main.ObserverControlBase;
import com.uptodata.isr.server.utils.fileSystem.Constants;
import com.uptodata.isr.server.utils.fileSystem.FileSystemWorker;
import com.uptodata.isr.server.utils.logging.log4j2.LogHelper;
import com.uptodata.isr.server.utils.network.UrlUtil;
import com.uptodata.isr.utils.ConvertUtils;
import com.uptodata.isr.utils.exceptions.MessageStackException;
import com.uptodata.isr.utils.xml.dom.XmlHandler;
import org.apache.commons.lang.StringUtils;

import java.nio.file.Paths;
import java.util.Properties;


/**
 * The class starts 1)an Observer web service; 2)a process that monitors it; 3)a process that pings this process.
 * At unexpected stop of the parent process (this class) caused the new start of monitor-process.
 * Created by smiljanskii60 on 16.07.2014.
 */
public class ObserverControl extends ObserverControlBase {

    @Override
    public void initLogger() {
        log = LogHelper.initLogger(logLevel, logReference);
    }


    @Override
    public void start(String[] args) throws Exception {
        Properties mainProps = ConvertUtils.convertArrayToProperties(args);
        String observerPort = mainProps.getProperty("observerPort");
        String context = mainProps.getProperty("context");
        long pingPeriod = Long.parseLong(mainProps.getProperty("pingPeriod"));

        bootstrap();
        Properties startMonitorProps = addDbPropertiesToArgs(mainProps);

        Properties initObserverProperties;
        try {
            initObserverProperties = FileSystemWorker.loadProperties("observer.properties");
            logLevel = initObserverProperties.getProperty("logLevel");
            localHost = initObserverProperties.getProperty("localhost");
            String pingPeriodProp = initObserverProperties.getProperty("pingPeriod");
            if (StringUtils.isNotEmpty(pingPeriodProp)) {
                pingPeriod = Long.parseLong(pingPeriodProp);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        if (StringUtils.isEmpty(logLevel)) {
            //get log level from the database
            logLevel = DbServletsFunctions.getDbLogLevel(dbData);
        }
        if (StringUtils.isEmpty(logLevel)) {
            logLevel = "DEBUG";   //it's a test, todo: IsrLogger.defaultLevel;
        } else if (logLevel.equals("NONE")) {
            logLevel = "ERROR";
        }
        System.out.println("logLevel = " + logLevel);
        initLogger();

        if (StringUtils.isEmpty(localHost)) {
            localHost = UrlUtil.getLocalHostAddress();
        }
        if (StringUtils.isEmpty(localHost)) {
            throw new Exception("local host is not defined!");
        }
        log.info("observer will start on localhost " + localHost);
        String dbPingPeriod = DbServletsFunctions.getDbSystemParameter(dbData, "OBSERVER_PING_PERIOD");
        log.info("dbPingPeriod=" + dbPingPeriod);
        if (StringUtils.isNotEmpty(dbPingPeriod) && StringUtils.isNumeric(dbPingPeriod.trim())) {
            startMonitorProps.setProperty("pingPeriod", dbPingPeriod.trim());
            pingPeriod = Long.parseLong(dbPingPeriod.trim());
        }

        if (StringUtils.isNotEmpty(logLevel)) {
            startMonitorProps.setProperty("logLevel", logLevel);
        }
        startMonitorProps.setProperty("localhost", localHost);
        log.info("argsForStartMonitor: " + ConvertUtils.propertiesWithPasswordToString(startMonitorProps));

        String[] argsForStopMonitor = new String[]{startMonitorProps.getProperty("observerPort")};
        String[] argsForStartMonitor = ConvertUtils.convertPropertiesToArray(startMonitorProps);

        // monitoring of Observer webservice
        finalizer = initFinalizer(argsForStartMonitor);

        //start Observer webservice
        observerInterface = new ObserverService(localHost, logLevel, logReference, dbData, dbDataForLogging);
        boolean serverStarted = startObserverWs(observerPort, context, observerInterface);

        observerInterface.uploadDbTokenFiles("OBSERVER_DBPARAMETERS", observerPort, localHost, logLevel, logReference);

        bootstrapMonitoring();

        startMonitoringProcess(argsForStartMonitor, argsForStopMonitor, pingPeriod);

        if (serverStarted) {
            XmlHandler xmlHandler = observerInterface.findChildServers(observerPort, localHost);
            if (xmlHandler != null) {
                startChildServers(xmlHandler, observerPort);
            }
            startPingScheduler(argsForStartMonitor, argsForStopMonitor, pingPeriod);
        }

        //todo: process output merged with the standard output. This is a workaround
        // for the follows problem: the observer monitoring process hangs after 10 times.
        Runnable runnable = new Runnable() {
            @Override
            public void run() {
                readFromProcess(monitorProcess);
            }
        };
        new Thread(runnable).start();
    }


    private void bootstrapMonitoring() throws MessageStackException {
        monitorStarterClassName = "com.uptodata.isr.observer.monitoring.MainObserverMonitorStarter";
        monitoringPath = "ObserverMonitor";
        monitorStarterUserPath = Paths.get(Constants.USER_DIR, monitoringPath);
    }

    private Properties addDbPropertiesToArgs(Properties mainProps) throws Exception {
        Properties dbConnProperties = dbData.getProperties();

        for (String key : dbConnProperties.stringPropertyNames()) {
            mainProps.setProperty(key, dbConnProperties.getProperty(key));
        }

        return mainProps;
    }

    /**
     * @param args args[0]=observer port; args[1]=context; args[2]=targetNamespace; args[3]=ping period in seconds
     *             example: 1099 ws/observer/isr/uptodata/com http://ws.observer.isr.uptodata.com/ 5
     */
    public static void main(String[] args) throws Exception {
        //todo test error: throw Exception
        ObserverControl control = new ObserverControl();
        control.start(args);
    }
}
