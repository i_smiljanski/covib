package com.uptodata.observer.installer;

import com.jidesoft.dialog.ButtonPanel;
import com.jidesoft.dialog.JideOptionPane;
import com.jidesoft.swing.JideSplitPane;
import com.uptodata.isr.gui.util.GuiUtility;
import com.uptodata.isr.server.utils.fileSystem.Constants;
import com.uptodata.isr.server.utils.network.UrlUtil;
import org.apache.commons.io.FileUtils;
import org.apache.commons.lang.StringUtils;

import javax.swing.*;
import javax.swing.border.EmptyBorder;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.nio.file.StandardCopyOption;

/**
 * Created by smiljanskii60 on 26.05.2015.
 */
public class Installer {

    private JTextField observerPort;
    private JTextField observerPath;
    private JCheckBox installRuntime;


    public void showDialog(boolean dispose) {

        observerPath = new JTextField(Constants.USER_DIR);
        observerPort = new JTextField();
        installRuntime = new JCheckBox();

        observerPath.setPreferredSize(new Dimension(150, 17));

        final JDialog f = new JDialog();
        f.setTitle("Observer Installer");
        f.setModal(true);
        if (dispose) {
            f.setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
        }
//        ClassLoader loader = Thread.currentThread().getContextClassLoader();
//        f.setIconImage(new ImageIcon(loader.getResource("iSR30-48.png")).getImage());

        JPanel form = new JPanel();
        f.getContentPane().setLayout(new BorderLayout());
        f.getContentPane().add(form, BorderLayout.CENTER);

        form.setLayout(new GridBagLayout());

        final JFileChooser fileChooser = new JFileChooser();
        fileChooser.setFileSelectionMode(JFileChooser.DIRECTORIES_ONLY);
        fileChooser.setMultiSelectionEnabled(false);
//        fileChooser.setControlButtonsAreShown(false);
        JButton openButton = new JButton();
//        openButton.setPreferredSize(new Dimension(17, 17));
        openButton.addActionListener(e -> {
            fileChooser.setSelectedFile(new File(observerPath.getText()));
            int returnVal = fileChooser.showOpenDialog(f);
            if (returnVal == JFileChooser.APPROVE_OPTION) {
                File file = fileChooser.getSelectedFile();
                try {
                    observerPath.setText(file.getCanonicalPath());
                } catch (IOException e1) {
                    e1.printStackTrace();
                }
            }
        });
        JideSplitPane pathComp = new JideSplitPane();
        pathComp.setDividerSize(5);
        pathComp.add(observerPath);
        pathComp.add(openButton);

        GuiUtility formUtility = new GuiUtility();

        createComponentWithLabel(formUtility, form, "Path to install", pathComp);
        createComponentWithLabel(formUtility, form, "Observer port", observerPort);
        createComponentWithLabel(formUtility, form, "Install java runtime", installRuntime);

        JButton testButton = new JButton("Test installation");
        testButton.addActionListener(event -> {
            String portText = observerPort.getText();
            String pathText = observerPath.getText();

            if (!UrlUtil.isPortFree(Integer.parseInt(portText))) {
                JideOptionPane.showMessageDialog(f, "The port " + portText + " is busy!", "Observer", JOptionPane.ERROR_MESSAGE);
            } else {
                if (StringUtils.isEmpty(pathText) || StringUtils.isEmpty(portText)) {
                    JideOptionPane.showMessageDialog(f, "Observer path or port is empty!", "Observer",
                            JOptionPane.ERROR_MESSAGE);
                } else {
                    int confirm = JideOptionPane.showConfirmDialog(f, "Test is successfully",
                            "Observer", JOptionPane.DEFAULT_OPTION);

                }

            }
        });

        JButton installButton = new JButton("Install");

        installButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent event) {
                String portText = observerPort.getText();
                String pathText = observerPath.getText();

                if (!UrlUtil.isPortFree(Integer.parseInt(portText))) {
                    JideOptionPane.showMessageDialog(f, "The port " + portText + " is busy!", "Observer", JOptionPane.ERROR_MESSAGE);
                } else {
                    if (StringUtils.isEmpty(pathText) || StringUtils.isEmpty(portText)) {
                        JideOptionPane.showMessageDialog(f, "Observer path or port is empty!", "Observer",
                                JOptionPane.ERROR_MESSAGE);
                    } else {
                        Path observer = Paths.get(pathText, "Observer " + portText);
                        if (Files.notExists(observer)) {
                            try {
                                observer = Files.createDirectory(observer);
                            } catch (IOException e) {
                                JideOptionPane.showMessageDialog(f, "Observer folder could not be created: " + e.getMessage(),
                                        "Observer", JOptionPane.ERROR_MESSAGE);
                            }
                        }
                        try {
                            String projectFileName = "project.xml";
                            InputStream is = Installer.class.getClassLoader().getResourceAsStream(projectFileName);
                            Path projectFile = Paths.get(observer.toFile().getAbsolutePath(), projectFileName);
                            long installFile = Files.copy(is, projectFile, StandardCopyOption.REPLACE_EXISTING);
                            is.close();

//                        File from = new File(Installer.class.getClassLoader().getResource("dist/work").getFile().replace("%20", " "));
                            File from = Paths.get("observer").toFile();
                            FileUtils.copyDirectory(from, observer.toFile());
//                        for (File file : from.listFiles()) {
//                            FileUtils.copyDirectoryToDirectory(file, observer.toFile());
//                        }

                            if (installRuntime.isSelected()) {
                                Path jre = Paths.get("jre");

                                FileUtils.copyDirectoryToDirectory(jre.toFile(), observer.toFile());
                                FileUtils.copyDirectoryToDirectory(jre.toFile(), Paths.get(observer.toFile().getAbsolutePath() +
                                        "/ObserverMonitor").toFile());
//                            FileUtils.copyDirectoryToDirectory(file, observer.toFile());
//                            FileUtils.copyDirectoryToDirectory(file, Paths.get(observer.toFile().getAbsolutePath() +
//                                    "/ObserverMonitor").toFile());
                            }

                            Path wrapperConf = Paths.get(observer.toFile().getAbsolutePath() + "/conf", "wrapper.conf");
                            String content = new String(Files.readAllBytes(wrapperConf));
                            content = content.replace("<port>", portText);
                            Files.write(wrapperConf, content.getBytes());

                            int confirm = JideOptionPane.showConfirmDialog(f, "Observer installed successfully",
                                    "Observer", JOptionPane.DEFAULT_OPTION);

                            if (confirm == JOptionPane.YES_OPTION) {
                                System.exit(0);
                            }
                        } catch (Exception e) {
                            e.printStackTrace();
                            JideOptionPane.showMessageDialog(f, e, "Observer", JOptionPane.ERROR_MESSAGE);
                        }
                    }

                }
            }
        });

        ButtonPanel buttonPanel = new ButtonPanel();
        JButton cancel = new JButton("Cancel");
        cancel.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                System.exit(0);
            }
        });
        buttonPanel.add(testButton, ButtonPanel.OTHER_BUTTON);
        buttonPanel.add(installButton, ButtonPanel.OTHER_BUTTON);
        buttonPanel.add(cancel, ButtonPanel.CANCEL_BUTTON);
        f.getContentPane().add(buttonPanel, BorderLayout.AFTER_LAST_LINE);


        // Add an little padding around the form
        form.setBorder(new EmptyBorder(2, 2, 2, 2));

        f.setSize(600, 200);
        f.setLocationRelativeTo(null);
        f.setVisible(true);

    }

    private void createComponentWithLabel(GuiUtility formUtility, JPanel form, String labelText, Component field) {
        formUtility.addLabel(labelText, form);
        formUtility.addLastField(field, form);
    }


    public static void main(String[] args) {
        new Installer().showDialog(true);
    }
}
