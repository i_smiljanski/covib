CREATE OR REPLACE TYPE                   ISR$SERVER$PARENTSERVER UNDER   ISR$SERVER$RECORD (
static function callFunction( oParameter in STB$MENUENTRY$RECORD, 
                       olNodeList out STB$TREENODELIST )  return STB$OERROR$RECORD,
static function putParameters( oParameter  in STB$MENUENTRY$RECORD, 
                        olParameter in STB$PROPERTY$LIST, 
                        clParameter in clob  default null ) return STB$OERROR$RECORD, 
static function getLov( sEntity        in varchar2,
                 sSuchwert      in varchar2, 
                 oSelectionList out ISR$TLRSELECTION$LIST ) return STB$OERROR$RECORD ,                                             
static function getNodeList ( oSelectedNode in  STB$TREENODE$RECORD, 
                      olNodeList     out STB$TREENODELIST ) return STB$OERROR$RECORD
                          );