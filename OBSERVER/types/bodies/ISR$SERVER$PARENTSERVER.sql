CREATE OR REPLACE TYPE BODY ISR$SERVER$PARENTSERVER as

static function callFunction( oParameter in  STB$MENUENTRY$RECORD, 
                              olNodeList out STB$TREENODELIST )  
  return STB$OERROR$RECORD 
is
  sCurrentName           varchar2(100) := $$PLSQL_UNIT||'.callstatic function ('||oParameter.sToken||')';  
  oErrorObj              STB$OERROR$RECORD := STB$OERROR$RECORD();
  sMsg                   varchar2(4000);
  sCheckForDialog  varchar2(1) ;
  sMenuToken       varchar2 (500)  := upper(oParameter.sToken);
begin
  isr$trace.stat('begin','begin',sCurrentName);
  case
   when upper(sMenuToken) = 'CAN_ADD_CHILD_SERVER' then
          olNodeList := STB$TREENODELIST ();
          olNodeList.extend;
          olNodeList (1) :=
             STB$TREENODE$RECORD (STB$UTIL.getMaskTitle (UPPER (sMenuToken)),
                                  $$PLSQL_UNIT,
                                  'TRANSPORT',
                                  'MASK',
                                  '');
          sMsg := sMsg || 'try to get property list ' || stb$typedef.crlf;
          oErrorObj := STB$UTIL.getPropertyList (sMenuToken, olNodeList (1).tloPropertylist);
          sMsg := sMsg || 'try to build dialog' || stb$typedef.crlf;
          oErrorObj := isr$server$record.checkDependOnMaskForTable (oParameter, olNodeList (1).tloPropertylist, sCheckForDialog);
   else
       oErrorObj :=  isr$server$record.callfunction( oParameter,  olNodeList);
  end case;
  if nvl(sCheckForDialog, 'F') = 'T' then
    oErrorObj.sErrorCode := Utd$msglib.GetMsg('exNotExists',Stb$security.GetCurrentLanguage) ;
  end if;    
  isr$trace.stat('end','end',sCurrentName);
 return oErrorObj;
exception
when others then
    sMsg :=  utd$msglib.getmsg ('exUnexpectedError', stb$security.getcurrentlanguage, sqlerrm(sqlcode));
    oErrorObj.handleError ( Stb$typedef.cnSeverityCritical, sMsg, sCurrentName, 
                            dbms_utility.format_error_stack || CHR(10) || dbms_utility.format_error_backtrace );  
    return oErrorObj;     
end callFunction;


-- ***************************************************************************************************
static function putParameters( oParameter  in STB$MENUENTRY$RECORD, 
                               olParameter in STB$PROPERTY$LIST, 
                               clParameter in clob default null ) 
  return STB$OERROR$RECORD  
is
  exTokenNotExists exception;
  sCurrentName     constant varchar2(100) := $$PLSQL_UNIT||'.putParameters ('||oParameter.sToken||')';
  oErrorObj        STB$OERROR$RECORD      := STB$OERROR$RECORD();
  sMsg             varchar2(4000);  
  sKeyValue1       ISR$DIALOG.PARAMETERVALUE%type;
  
 cursor cGetCurrentServer  is
      select *
      from   isr$server
      where  serverid = stb$object.getCurrentNodeId;

  rGetServer       cGetCurrentServer%rowtype;
  sRemoteEx        clob;

begin
  isr$trace.stat('begin','begin',sCurrentName);
  STB$OBJECT.setCurrentToken(oparameter.stoken);
  case
    when Upper (oParameter.sToken) = 'CAN_ADD_CHILD_SERVER' then   
      sKeyValue1 := STB$UTIL.getProperty (olParameter, 'SERVERID');
      oErrorObj := isr$server$base.saveServerDialogData(oParameter, olParameter);
      oErrorObj := STB$SYSTEM.getAuditType;
    else
        oErrorObj :=  isr$server$record.putParameters( oParameter,  olParameter, clParameter);
  end case;

  isr$trace.stat('end','end',sCurrentName);
  return oErrorObj;
exception
  when exTokenNotExists then
    sMsg :=  utd$msglib.getmsg ('exTokenNotExists', stb$security.getcurrentlanguage, upper(oParameter.sToken));
    oErrorObj.handleError ( Stb$typedef.cnSeverityCritical, sMsg, sCurrentName, 
                            dbms_utility.format_error_stack ||stb$typedef.crlf|| dbms_utility.format_error_backtrace );  
    return oErrorObj;    
  when others then
    sMsg :=  utd$msglib.getmsg ('exUnexpectedError', stb$security.getcurrentlanguage, csP1 => sqlerrm(sqlcode));
    oErrorObj.handleError ( Stb$typedef.cnSeverityCritical, sMsg, sCurrentName, 
                            dbms_utility.format_error_stack ||stb$typedef.crlf|| dbms_utility.format_error_backtrace  );  
    return oErrorObj;    
end putParameters;
static function getLov( sEntity        in varchar2,
                 sSuchwert      in varchar2, 
                 oSelectionList out ISR$TLRSELECTION$LIST ) return STB$OERROR$RECORD 
is  
  sCurrentName constant varchar2(100) := $$PLSQL_UNIT||'.getLov(' || sEntity || ')';
  sMsg         varchar2(4000);
  nCounter     number            := 0;
  oErrorObj    STB$OERROR$RECORD := STB$OERROR$RECORD();   
  --servertype
  cursor cGetServertype is
  select * from isr$validvalue$servertype
  where nodetype not in( 'SERVER_GENERAL', 'OBSERVER');
begin  
  isr$trace.stat('sEntity='||sEntity, 'begin', sCurrentName);
  oSelectionList := ISR$TLRSELECTION$LIST ();
  case
     when Upper (sEntity) = 'SERVERTYPEID' then
      for rGetServertype in cGetServertype loop
        nCounter := nCounter + 1;
        oSelectionList.extend;            
        oSelectionList (nCounter) := ISR$OSELECTION$RECORD(rGetServertype.servertypename, rGetServertype.servertypeid, rGetServertype.servertypename, null, nCounter);
      end loop;  
  end case;
  isr$trace.stat ('end', 'oSelectionList.count() = ' || oSelectionList.count(), sCurrentName);
  return oErrorObj;
  exception
  when others then
    sMsg :=  utd$msglib.getmsg ('exUnexpectedError', stb$security.getcurrentlanguage, csP1 => sqlerrm(sqlcode));
    oErrorObj.handleError ( Stb$typedef.cnSeverityCritical, sMsg, sCurrentName, 
                            substr(dbms_utility.format_error_stack || CHR(10) || dbms_utility.format_error_backtrace, 1,4000)  );  
    return oErrorObj; 
end getLov;         
-- ***************************************************************************************************
static function getNodeList ( oSelectedNode in  STB$TREENODE$RECORD, 
                              olNodeList    out STB$TREENODELIST     ) 
  return STB$OERROR$RECORD
is
  sCurrentName       constant varchar2 (100) := $$PLSQL_UNIT || '.getNodeList';
  oErrorObj          STB$OERROR$RECORD       := STB$OERROR$RECORD ();
  sMsg               varchar2 (4000);
  sObserverRunstatus varchar2 (1);
  nDummy             number;
  nCountChildServers number;
  sStatus      varchar2(1);
  nDateDiff    number;
  nPingPeriod  number;
  
  sSelect       VARCHAR2(4000);
begin
  isr$trace.stat('begin', oSelectedNode.sNodetype, sCurrentName);
  isr$trace.debug('value', 'oSelectedNode', sCurrentName, sys.anydata.convertObject(oSelectedNode));
  
  olNodeList := STB$TREENODELIST ();   -- initialize output parameter
  
  if oSelectedNode.sNodetype = 'OBSERVER' then
    
    for rGetServers in ( select serverid, observer_runstatus, observer_host, port, modifiedon
                         from   isr$serverobserver$v
                         where  observerid = STB$OBJECT.getCurrentNodeId )
    loop
      -- determine runstatus of observer
      sObserverRunstatus := NVL(trim(rGetServers.observer_runstatus),'F');
      nDateDiff := isr$util.getDateDifferenceInSec(rGetServers.modifiedon, sysdate);
      nPingPeriod := to_number(stb$util.getSystemParameter('OBSERVER_PING_PERIOD')); 
      isr$trace.info ('ping date', 'date difference=' || nDateDiff || '; ping period=' || nPingPeriod, sCurrentName);
      --check status if observer monitoring process doesn't work
      if nDateDiff > 2*nPingPeriod then
        sStatus:= isr$server$base.isServerAvailable(rGetServers.observer_host, rGetServers.port,  isr$trace.getUserLoglevelStr);
        nDummy := isr$remote$service.setServerStatus (rGetServers.serverid, sStatus);
        isr$trace.debug ('server ' || rGetServers.serverid || ' status', sStatus, sCurrentName);
      end if;
    end loop;

    select count(1)
    into   nCountChildServers
    from   isr$server$servertype$v
    where  observerid = STB$OBJECT.getCurrentNodeId;

    if nCountChildServers >= 1 then

      -- initialize olNodeList(1), value is subsequently replaced through call of STB$UTIL.getColListMultiRow
      olNodeList.EXTEND;
      olNodeList(1) := STB$TREENODE$RECORD(tloValueList => oSelectedNode.tloValueList);  
       
      sSelect := 'SELECT serverid NODEID
                       , nodetype NODETYPE
                       , alias NODENAME
                       , package PACKAGENAME
                       ,    iconid
                         || ''_''
                         || CASE
                               WHEN runstatus = ''T'' THEN
                                  ''OK''
                               WHEN runstatus = ''F''
                                AND '''||sObserverRunstatus||''' = ''T''
                                AND serverstatus = 3 THEN
                                  ''WARNING''
                               ELSE
                                  ''ERROR''
                            END
                            ICON
                       , HOST
                       , port
                       , servertypename
                       , runstatus
                       , serverid
                       , description
                       , case when s.disabled = ''T'' then ''server disabled'' else (select v.description  from isr$validvalue$serverstatus v where v.serverstatusid = s.serverstatus)end as server_status
                    FROM isr$server$servertype$v s
                   WHERE observerid = '||STB$OBJECT.getCurrentNodeId;
       
      isr$trace.debug ('sSelect', sSelect, sCurrentName);
      -- fetch nodelist of selected node
      oErrorObj := STB$UTIL.getColListMultiRow (sSelect, olNodeList);
    end if;
  end if;
  
  isr$trace.debug ('value', 'olNodeList', sCurrentName, sys.anydata.convertCollection(olNodeList));
  isr$trace.stat ('end', 'olNodeList.count(): '||olNodeList.count(), sCurrentName);
  return (oErrorObj);
exception
  when others then
    sMsg :=  utd$msglib.getmsg ('exUnexpectedError', stb$security.getcurrentlanguage, csP1 => sqlerrm(sqlcode));
    oErrorObj.handleError ( Stb$typedef.cnSeverityCritical, sMsg, sCurrentName, 
                            dbms_utility.format_error_stack || CHR(10) || dbms_utility.format_error_backtrace );  
    return oErrorObj;    
end getNodeList;
   
end;