﻿' <summary>The central sequence handler.</summary>

Imports System.Xml.XPath
Imports System.Xml

Public Class MainControl

    Dim oOptions As Options = Nothing
    Dim oHandlerWord As HandlerWord = Nothing
    Dim oHandlerExcel As HandlerExcel = Nothing
    Dim oMetaData As MetaData = Nothing
    Dim oAdditionalFiles As List(Of String) = Nothing
    Dim oGeneratedPDFs As List(Of String) = Nothing
    Dim oGlobalProperties As Properties = Nothing

    ''' <summary>
    ''' Constructor.
    ''' </summary>
    Public Sub New()
        oTrace.TraceEvent(TraceEventType.Information, 1, "Entering MainControl.New")

        'Initalize some general objects
        oOptions = New Options
        oMetaData = New MetaData
        oGlobalProperties = New Properties
        oAdditionalFiles = New List(Of String)
        oGeneratedPDFs = New List(Of String)

    End Sub

    ''' <summary>
    ''' In absence of a 'real' destructor.
    ''' </summary>
    Public Sub CleanUp()
        oTrace.TraceEvent(TraceEventType.Information, 1, "Entering MainControl.CleanUp")

        'Cleanup
        oOptions = Nothing
        oHandlerWord = Nothing
        oHandlerExcel = Nothing
        oMetaData = Nothing

        'The excel task does not go away if the GC is not called!
        GC.Collect()
        GC.WaitForPendingFinalizers()

    End Sub

    ''' <summary>
    ''' The Mainloop kind thing. Though it is no loop at all.
    ''' </summary>
    Public Function MainSequence() As Integer
        oTrace.TraceEvent(TraceEventType.Information, 1, "Entering MainControl.MainSequence")

        Dim iReturncode As Integer

        'Read the Command line
        oOptions.ParseArgs()

        'Read the Config file
        If oOptions.ReadConfigFile(oGlobalProperties) = False Then Return -1

        'Fill the Metadata table
        AcquireMetadata()

        'Try to convert the additional files
        SaveAdditionalFilesAsPDF()

        'Call the SaveAsPDF Module
        iReturncode = SaveAsPDF()

        'Create an Index of all PDFs files generated
        CreateIndexFile()

        Return iReturncode

    End Function

    ''' <summary>
    ''' Saves the Document as PDF by using the export functionality of Word/Excel
    ''' </summary>
    Public Sub AcquireMetadata()
        oTrace.TraceEvent(TraceEventType.Information, 1, "Entering MainControl.AcquireMetadata")

        'Handle HTML in Word

        For x = 1 To oOptions.File_Sources

            If System.IO.Path.GetFileName(oOptions.File_Source(x)).StartsWith("A_") Then
                oTrace.TraceEvent(TraceEventType.Information, 1, "Filename starting with A_ (Archiving Residue) ignored: " & oOptions.File_Source(x))
            Else
                'oTrace.TraceEvent(TraceEventType.Information, 1, "Collect data from associtated xmls")
                If ReadAssociatedXML(oOptions, oMetaData, x) = True Then
                    oTrace.TraceEvent(TraceEventType.Information, 1, "Read associtated xml successful: " & oOptions.File_Source(x))
                Else
                    oTrace.TraceEvent(TraceEventType.Information, 1, "Read associtated xml unsuccessful, handle as simple file: " & oOptions.File_Source(x))
                    oAdditionalFiles.Add(oOptions.File_Source(x))
                End If
            End If

        Next x

    End Sub

    ''' <summary>
    ''' Saves the Document as PDF by using the export functionality of Word/Excel
    ''' </summary>
    Public Function SaveAsPDF() As Integer
        oTrace.TraceEvent(TraceEventType.Information, 1, "Entering MainControl.SaveAsPDF")

        Dim sCurrentImport As String = String.Empty
        Dim lImportBookmarks As List(Of String) = New List(Of String)
        Dim sFilenamePDF As String = String.Empty

        'Initialize the word handler
        oHandlerWord = New HandlerWord

        'Handle HTML in Word

        For x = 0 To oMetaData.Count - 1

            oTrace.TraceEvent(TraceEventType.Information, 1, "---")

            If oMetaData.ContentType(x) = "PDF" Then

                sFilenamePDF = System.IO.Path.GetDirectoryName(oOptions.CheckPath(oMetaData.Contents(x))) & "\" & _
                               System.IO.Path.GetFileNameWithoutExtension(oOptions.CheckPath(oMetaData.Contents(x))) & ".pdf"

                'Open the source html and get a list of bookmarks
                oTrace.TraceEvent(TraceEventType.Information, 1, "Opening Import File " & oMetaData.Source(x))
                If oMetaData.Source(x) <> sCurrentImport Then

                    If sCurrentImport <> String.Empty Then oHandlerWord.CloseDoc(sCurrentImport)

                    If oHandlerWord.OpenDoc(oMetaData.Source(x), 2) = False Then Return -1

                End If
                sCurrentImport = oMetaData.Source(x)

                'Get a list of all relevant bookmarks
                lImportBookmarks.Clear()
                lImportBookmarks = oHandlerWord.GetListOfBookmarks("T_" & oMetaData.Hash(x) & ".*")

                'Open the Word Template
                oTrace.TraceEvent(TraceEventType.Information, 1, "Opening Template " & oOptions.TemplateFilename)
                If oHandlerWord.OpenDoc(oOptions.TemplateFilename, 1) = False Then Return -1

                oTrace.TraceEvent(TraceEventType.Information, 1, "Transfer the DocProperties to the template")
                oHandlerWord.TransferDocumentProperties(oMetaData.Properties(x), IO.Path.GetFileName(sFilenamePDF))

                'Insert the table into the template
                oTrace.TraceEvent(TraceEventType.Information, 1, "Inserting for " & (oMetaData.Bookmark(x) & ": " & oMetaData.Hash(x)))

                'Update the fields
                oHandlerWord.UpdateFields()

                If lImportBookmarks.Count = 0 Or oMetaData.ConvertAll(x) = True Then

                    If oHandlerWord.InsertComplete(oHandlerWord) = True Then

                        'Save the document
                        oTrace.TraceEvent(TraceEventType.Information, 1, "Saving PDF: " & IO.Path.GetFileName(sFilenamePDF))
                        If oHandlerWord.SaveAsPDF(sFilenamePDF) = True Then
                            oGeneratedPDFs.Add(sFilenamePDF)
                        End If

                    End If

                ElseIf oHandlerWord.InsertTables(lImportBookmarks, oHandlerWord) = True Then

                    'Save the document
                    oTrace.TraceEvent(TraceEventType.Information, 1, "Saving PDF: " & IO.Path.GetFileName(sFilenamePDF))
                    If oHandlerWord.SaveAsPDF(sFilenamePDF) = True Then
                        oGeneratedPDFs.Add(sFilenamePDF)
                    End If

                End If

                'Close the template
                oHandlerWord.CloseDoc(oOptions.TemplateFilename)

            Else
                oTrace.TraceEvent(TraceEventType.Information, 1, "No PDF file, skipping " & oMetaData.Source(x))
            End If

        Next

        'Temporarily disabled. Generates rubbish atm, needs rework.
        'CreateIndexPDF(oHandlerWord)

        'Close Word and stuff
        oTrace.TraceEvent(TraceEventType.Information, 1, "Cleanup...")
        oHandlerWord.CleanUp()

        Return 0

    End Function

    ''' <summary>
    ''' Try to save additional files as pdf. Basically driven by the extension of the file
    ''' </summary>
    Public Sub SaveAdditionalFilesAsPDF()
        oTrace.TraceEvent(TraceEventType.Information, 1, "Entering MainControl.SaveAdditionalFilesAsPDF")

        Dim sFilename As String
        Dim sFileExtension As String = String.Empty
        Dim sFilenamePDF As String = String.Empty

        'Initialize the word handler
        oHandlerWord = New HandlerWord

        For Each sFilename In oAdditionalFiles

            sFileExtension = System.IO.Path.GetExtension(sFilename)
            oTrace.TraceEvent(TraceEventType.Information, 1, "Trying to Convert: " & sFilename)

            sFilenamePDF = System.IO.Path.GetDirectoryName(sFilename) & "\" & _
                           System.IO.Path.GetFileNameWithoutExtension(sFilename) & ".pdf"

            Select Case sFileExtension

                Case ".pdf" ' Leave as is

                    oTrace.TraceEvent(TraceEventType.Information, 1, "PDF, no conversion: " & IO.Path.GetFileName(sFilenamePDF))
                    oGeneratedPDFs.Add(sFilenamePDF)

                Case ".xls", ".xlsx" 'Load it in Excel and save as pdf

                    'Initialize the excel handler
                    oHandlerExcel = New HandlerExcel

                    If oHandlerExcel.OpenWorkbook(sFilename) = True Then

                        oTrace.TraceEvent(TraceEventType.Information, 1, "Saving PDF: " & IO.Path.GetFileName(sFilenamePDF))

                        'Save the document
                        If oHandlerExcel.SaveAsPDF(sFilenamePDF) = False Then
                            oTrace.TraceEvent(TraceEventType.Warning, 10, "Cannot save PDF: " & sFilenamePDF)
                        Else
                            oGeneratedPDFs.Add(sFilenamePDF)
                        End If

                        oHandlerExcel.CloseWorkbook(sFilename)

                    Else
                        oTrace.TraceEvent(TraceEventType.Warning, 10, "Cannot open: " & sFilename)
                    End If

                    'Close Excel and stuff
                    oTrace.TraceEvent(TraceEventType.Information, 1, "Excel Cleanup...")
                    oHandlerExcel.CleanUp()

                Case Else 'try to load it in Word and convert it.

                    'Initialize the word handler
                    oHandlerWord = New HandlerWord

                    If oHandlerWord.OpenDoc(sFilename, 1) = True Then

                        oTrace.TraceEvent(TraceEventType.Information, 1, "Saving PDF: " & IO.Path.GetFileName(sFilenamePDF))

                        'Save the document
                        If oHandlerWord.SaveAsPDF(sFilenamePDF) = False Then
                            oTrace.TraceEvent(TraceEventType.Warning, 10, "Cannot save PDF: " & sFilenamePDF)
                        Else
                            oGeneratedPDFs.Add(sFilenamePDF)
                        End If

                        oHandlerWord.CloseDoc(sFilename)

                    Else
                        oTrace.TraceEvent(TraceEventType.Warning, 10, "Cannot open: " & sFilename)
                    End If

            End Select

        Next

        'Close Word and stuff
        oTrace.TraceEvent(TraceEventType.Information, 1, "Word Cleanup...")
        oHandlerWord.CleanUp()

    End Sub

    ''' <summary>
    ''' In Case the Archivemodule exits due to an error, Main has to have the possibility to close the word instance gracefully.
    ''' </summary>
    ''' <returns>the Word Object</returns>  
    Public Function GetWordBackup() As Microsoft.Office.Interop.Word.Application

        If Not oHandlerWord Is Nothing Then
            Return oHandlerWord.GetWordBackup
        Else
            Return Nothing
        End If

    End Function

    Function ReadAssociatedXML(oOptions As Options, oMetadata As MetaData, iEntryNum As Integer) As Boolean
        oTrace.TraceEvent(TraceEventType.Information, 1, "Entering MainControl.ReadAssociatedXML")

        ' Ja, das kann man bestimmt eleganter lösen. Meh.

        Dim oProperties As Properties
        Dim oXpathDoc As XPathDocument
        Dim oXmlNav As XPathNavigator
        Dim oResult As Object

        Dim MetaSort As List(Of String) = New List(Of String)
        Dim sNumOfEntries As Integer = 0
        Dim sNumOfProperties As Integer = 0
        Dim sXpathBase As String = String.Empty

        Dim sSourceFilename As String = oOptions.File_Source(iEntryNum)

        Try
            oXpathDoc = New XPathDocument(sSourceFilename & ".xml")
            oXmlNav = oXpathDoc.CreateNavigator()
        Catch ex As XPathException
            HandleException(ex, "ReadAssociatedXML, XMLException")
            Return False
        Catch ex As System.IO.FileNotFoundException
            oTrace.TraceEvent(TraceEventType.Warning, 1, "File not found: " & sSourceFilename & ".xml")
            Return False
        Catch ex As Exception
            HandleException(ex, "ReadAssociatedXML")
            Return False
        End Try

        'Number of entries in archiving-metadata
        oResult = oXmlNav.Evaluate("count(/archiving/archiving-metadata)")
        If Not oResult Is Nothing Then
            sNumOfEntries = Convert.ToInt32(oResult.ToString)
            oTrace.TraceEvent(TraceEventType.Information, 11, "sNumOfEntries = " & oResult.ToString)
        End If

        For x = 1 To sNumOfEntries
            sXpathBase = "/archiving/archiving-metadata[" & x & "]/"

            'Build the content string on whats given in sort.
            Try
                oMetadata.AddMetadata(
                    oOptions.File_Source(iEntryNum),
                    oXmlNav.SelectSingleNode(sXpathBase & "../@metafile").ToString,
                    oXmlNav.SelectSingleNode(sXpathBase & "hashbase").ToString,
                    oXmlNav.SelectSingleNode(sXpathBase & "hash").ToString,
                    ReplaceInvalidFileNameChars(oXmlNav.SelectSingleNode(sXpathBase & "content").ToString),
                    oXmlNav.SelectSingleNode(sXpathBase & "../@convertall").ToString,
                    oXmlNav.SelectSingleNode(sXpathBase & "../@indexdesc").ToString(),
                    oXmlNav.SelectSingleNode(sXpathBase & "content-type").ToString,
                    oXmlNav.SelectSingleNode(sXpathBase & "content-path").ToString)
            Catch ex As Exception
                HandleException(ex, "ReadAssociatedXML")
                Return False
            End Try


            'Number of entries in archiving-metadata with attribute docproperty=true()
            oResult = oXmlNav.Evaluate("count(/archiving/archiving-metadata[" & x & "]/metadata/*[@docproperty=true()])")
            If Not oResult Is Nothing Then
                sNumOfProperties = Convert.ToInt32(oResult.ToString)
                oTrace.TraceEvent(TraceEventType.Information, 11, "sNumOfEntries (docproperty=true()) = " & oResult.ToString)
            End If

            oProperties = New Properties

            For y = 1 To sNumOfProperties
                sXpathBase = "/archiving/archiving-metadata[" & x & "]/metadata/*[@docproperty=true()][" & y & "]"

                'Build the content string on whats given in sort.
                Try
                    oProperties.AddProperty(
                        oXmlNav.SelectSingleNode(sXpathBase & "/@name").ToString,
                        oXmlNav.SelectSingleNode(sXpathBase).ToString()
                    )
                Catch sa As System.ArgumentException
                    If sa.Message.StartsWith("Item has already been added.") Then
                        oTrace.TraceEvent(TraceEventType.Warning, 1, "Item has already been added: " & oXmlNav.SelectSingleNode(sXpathBase & "/@name").ToString)
                    Else
                        HandleException(sa, "ReadAssociatedXML")
                        Return False
                    End If
                Catch ex As Exception
                    HandleException(ex, "ReadAssociatedXML")
                    Return False
                End Try


            Next y

            oMetadata.Properties(x) = oProperties
            'oProperties = Nothing

        Next x

        Return True

    End Function

    Private Function ReplaceInvalidFileNameChars(sFilename As String) As String

        For Each invalidChar In IO.Path.GetInvalidFileNameChars
            sFilename = sFilename.Replace(invalidChar, "_")
        Next
        Return sFilename

    End Function

    Private Function CreateIndexPDF(oHandlerWord As HandlerWord) As Integer

        Dim sFilenamePDF As String = String.Empty
        Dim sPreviousBookmark As String = String.Empty

        sFilenamePDF = oOptions.CheckPath("Index.pdf")

        oTrace.TraceEvent(TraceEventType.Information, 1, "Entering MainControl.CreateContentsPDF")

        'Open the Word Template
        oTrace.TraceEvent(TraceEventType.Information, 1, "Opening Template " & oOptions.TemplateFilename)
        If oHandlerWord.OpenDoc(oOptions.TemplateFilename, 1) = False Then Return -1

        'Insert an link entry into the word template
        For x = 0 To oMetaData.Count - 1
            oTrace.TraceEvent(TraceEventType.Information, 1, "Create Link for " & oMetaData.Contents(x) & ": " & oMetaData.Contents(x) & ".pdf")


            If oMetaData.Bookmark(x) <> sPreviousBookmark Then
                If x <> 0 Then oHandlerWord.InsertEmtpyParagraph()
                oHandlerWord.InsertIndexDescription(oMetaData.IndexDesc(x))
            End If

            oHandlerWord.InsertEntryLink(oMetaData.Contents(x), oMetaData.Contents(x) & ".pdf")

            sPreviousBookmark = oMetaData.Bookmark(x)
        Next x

        'Save the document
        oTrace.TraceEvent(TraceEventType.Information, 1, "Saving PDF: Index.pdf")
        If oHandlerWord.SaveAsPDF(sFilenamePDF) = True Then
            oGeneratedPDFs.Add(sFilenamePDF)
        End If

        'Transfer the DocProperties to the template
        oHandlerWord.TransferDocumentProperties(oGlobalProperties, sFilenamePDF)

        'Close the template
        oHandlerWord.CloseDoc(oOptions.TemplateFilename)

        Return 0

    End Function

    Private Sub CreateIndexFile()

        oTrace.TraceEvent(TraceEventType.Information, 1, "Entering MainControl.CreateIndexFile")
        'Dim sFilename As String

        'Save an Index of all files
        Dim writer As New XmlTextWriter(oOptions.CheckPath(sConfigAppName & "_index.xml"), System.Text.Encoding.UTF8)

        writer.WriteStartDocument(True)
        writer.Formatting = Formatting.Indented
        writer.Indentation = 2
        writer.WriteStartElement("files")

        If oMetaData.CheckForPath Then
            ' New filestructure when filepath is given
            For x = 0 To oMetaData.Count - 1
                writer.WriteStartElement("file")
                writer.WriteAttributeString("type", oMetaData.ContentType(x))
                writer.WriteAttributeString("path", oMetaData.ContentPath(x))
                writer.WriteString(oMetaData.Contents(x))
                writer.WriteEndElement()
            Next x

        Else
            ' Old filestructure without filepath for compatibility
            writer.WriteStartElement("index")
            For Each sFilename In oGeneratedPDFs
                writer.WriteStartElement("file")
                writer.WriteString(System.IO.Path.GetFileName(sFilename))
                writer.WriteEndElement()
            Next

        End If

        writer.WriteEndElement()
        writer.WriteEndDocument()
        writer.Close()

    End Sub

End Class