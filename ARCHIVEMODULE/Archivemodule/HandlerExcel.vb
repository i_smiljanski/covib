﻿' <summary>Handels everything Word.</summary>

'Imports System.Xml.XPath
Imports Microsoft.Office.Interop.Excel
Imports Microsoft.Office.Core
Imports System.Text
'Imports System.Security.Cryptography
'Imports System.Text.RegularExpressions


Public Class HandlerExcel

    Private oExcel As Application = Nothing
    Private sWorkbookNameSource As String

    Private oWorkbookSource As Workbook

    ''' <summary>
    ''' Contructor.
    ''' Initializes the Excel Object.
    ''' </summary>
    Public Sub New()
        oTrace.TraceEvent(TraceEventType.Information, 1, "Entering HandlerExcel.New")

        'Initialize the Excel Object
        oExcel = New Application
        oExcel.Visible = False

    End Sub


    ''' <summary>
    ''' In absence of a 'real' destructor.
    ''' </summary>
    Public Sub CleanUp()
        oTrace.TraceEvent(TraceEventType.Information, 1, "Entering HandlerExcel.CleanUp")

        'If the object is already empty, there is nothing to do here
        If oExcel Is Nothing Then Exit Sub

        oExcel.DisplayAlerts = False

        'Now leave Excel.
        oExcel.Quit()

        'Free the Excel object
        oExcel = Nothing

    End Sub

    ''' <summary>
    ''' In Case the Archivemodule exits due to an error, Main has to have the possibility to close the Excel instance gracefully.
    ''' </summary>
    ''' <returns>the Excel Object</returns>  
    Public Function GetExcelBackup() As Application

        Return oExcel

    End Function

    ''' <summary>
    ''' Open the associated workbook.
    ''' </summary>
    ''' <param name="sWorkbookFile">The workbook filename (including path).</param>  
    ''' <returns>true if the file was opend, false if something went wrong.</returns>  
    Public Function OpenWorkbook(ByVal sWorkbookFile As String) As Boolean
        oTrace.TraceEvent(TraceEventType.Information, 1, "Entering HandlerExcel.OpenWorkbook")
        oTrace.Flush()

        oTrace.TraceEvent(TraceEventType.Information, 1, "Opening " & sWorkbookFile)

        Try
            oTrace.TraceEvent(TraceEventType.Information, 1, "Document Size: " & My.Computer.FileSystem.GetFileInfo(sWorkbookFile).Length)
            oWorkbookSource = oExcel.Workbooks.Open(sWorkbookFile)
            sWorkbookNameSource = sWorkbookFile
        Catch ex As Exception
            HandleException(ex, "OpenWorkbook")
            Return False
        End Try

        Return True

    End Function

    ''' <summary>
    ''' Closes a document.
    ''' </summary>
    ''' <param name="sWorkbookFile">The documents filename (including path).</param>  
    Sub CloseWorkbook(ByVal sWorkbookFile As String)

        Try
            oExcel.Workbooks(sWorkbookFile).Close(XlSaveAction.xlDoNotSaveChanges)
        Catch ex As Exception
            HandleException(ex, "CloseWorkbook")
        End Try

    End Sub

    ''' <summary>
    ''' Saves the document as a PDF document
    ''' </summary>
    ''' <param name="sFilename">The pdf filename (including path).</param>  
    ''' <returns>True if the document was exported successfully, false if something went wrong</returns>  
    Public Function SaveAsPDF(ByVal sFilename As String) As Boolean
        oTrace.TraceEvent(TraceEventType.Information, 1, "Entering HandlerExcel.SaveAsPDF")

        Try
            'Save Workbook as PDF
            oWorkbookSource.ExportAsFixedFormat(Filename:=sFilename, Type:=XlFixedFormatType.xlTypePDF, IncludeDocProperties:=True)
            oTrace.TraceEvent(TraceEventType.Information, 1, "Saved PDF File: " & sFilename)

            'Excel adds a .pdf, if no extension is given. So check, if the file exists.
            If Not IO.File.Exists(sFilename) Then

                Try
                    My.Computer.FileSystem.RenameFile(sFilename & ".pdf", IO.Path.GetFileName(sFilename))
                Catch ex As Exception
                    oTrace.TraceEvent(TraceEventType.Critical, 12, "Rename failed. iSR will probably fail to locate the resulting pdf.")
                End Try

            End If

        Catch ex As Exception
            HandleException(ex, "SaveAsPDF")
            Return False
        End Try

        Return True

    End Function

End Class