﻿' <summary>Container for all options from the config.xml.</summary>

Imports System.Xml.XPath
Imports System.IO

Public Class Options

    Private iOrder As Integer

    ' !!! Refactor this as soon as possible 
    ''' <summary>
    ''' Path to the config.xml
    ''' </summary>
    Private sConfigPath As String
    ''' <summary>
    ''' The Path to the config.xml
    ''' </summary>
    Public Property ConfigPath() As String
        Get
            Return sConfigPath
        End Get
        Private Set(ByVal value As String)
            sConfigPath = value
        End Set
    End Property


    ''' <summary>
    ''' Number of File Sources
    ''' </summary>
    Private iFile_Sources As Integer
    ''' <summary>
    ''' Number of File Sources
    ''' </summary>
    Public Property File_Sources() As Integer
        Get
            Return iFile_Sources
        End Get
        Private Set(ByVal value As Integer)
            iFile_Sources = value
        End Set
    End Property

    ''' <summary>
    ''' Array that ontains the Bookmark names
    ''' </summary>
    Private sFile_Source() As String
    ''' <summary>
    ''' The Filenames of the associated files..
    ''' </summary>
    Public Property File_Source(iNum As Integer) As String
        Get
            Return sFile_Source(iNum)
        End Get
        Private Set(ByVal value As String)
            sFile_Source(iNum) = value
        End Set
    End Property

    ''' <summary>
    ''' Array that contains the Filenames of the associated files.
    ''' </summary>
    Private sFile_Bookmark() As String
    ''' <summary>
    ''' The Bookmark names
    ''' </summary>
    Public Property File_Bookmark(iNum As Integer) As String
        Get
            Return sFile_Bookmark(iNum)
        End Get
        Private Set(ByVal value As String)
            sFile_Bookmark(iNum) = value
        End Set
    End Property

    ''' <summary>
    ''' Contains the Filename of the Config.xml.
    ''' </summary>
    Private sConfigFile As String
    ''' <summary>
    ''' The Filename of the Config.xml.
    ''' </summary>
    Public Property ConfigFile() As String
        Get
            Return sConfigFile
        End Get
        Private Set(ByVal value As String)
            sConfigFile = value
        End Set
    End Property

    ''' <summary>
    ''' Contains The Filename of the Template
    ''' </summary>
    Private sTemplateFilename As String
    ''' <summary>
    ''' The Filename of the Document/Workbook.
    ''' </summary>
    Public Property TemplateFilename() As String
        Get
            Return sTemplateFilename
        End Get
        Private Set(ByVal value As String)
            sTemplateFilename = value
        End Set
    End Property

    ''' <summary>
    ''' Contains The Filename of the Index PDF
    ''' </summary>
    Private sIndexPdfFilename As String
    ''' <summary>
    ''' The Filename of the Index PDF.
    ''' </summary>
    Public Property IndexPdfFilename() As String
        Get
            Return sIndexPdfFilename
        End Get
        Private Set(ByVal value As String)
            sIndexPdfFilename = value
        End Set
    End Property

    ''' <summary>
    ''' Contructor.
    ''' </summary>
    ''' <remarks>
    ''' Sets the default values according to the FRS (EXP25), 
    ''' but these will likely be overriden by the values of the config.xml.
    ''' </remarks>
    Public Sub New()
        oTrace.TraceEvent(TraceEventType.Information, 1, "Entering Options.New")

    End Sub

    ''' <summary>
    ''' In absence of a 'real' destructor.
    ''' </summary>
    Public Sub CleanUp()
        oTrace.TraceEvent(TraceEventType.Information, 1, "Entering Options.CleanUp")

    End Sub

    ''' <summary>
    ''' Parses the command line arguments.
    ''' </summary>
    ''' <remarks>
    ''' If no config.xml is given at the command line, it defaults to "config.xml" (in the app.path).
    ''' </remarks>
    Sub ParseArgs()
        oTrace.TraceEvent(TraceEventType.Information, 1, "Entering Options.ParseArgs")

        If Environment.GetCommandLineArgs.Count = 1 Then
            oTrace.TraceEvent(TraceEventType.Warning, 10, "No Config File parameter given. Trying default config.xml.")
            sConfigFile = Environment.CurrentDirectory & "\config.xml"
            oTrace.TraceEvent(TraceEventType.Warning, 10, "No order parameter given. Trying default 1.")
            iOrder = 1 'Default
        Else
            sConfigFile = Environment.GetCommandLineArgs(1)
        End If

        If My.Application.CommandLineArgs.Count > 1 Then
            iOrder = Convert.ToInt32(Environment.GetCommandLineArgs(2))
        End If

        ConfigPath = Path.GetDirectoryName(sConfigFile)

        If ConfigPath = String.Empty Then
            'No path given with the config. Determine the application path instead
            ConfigPath = My.Application.Info.DirectoryPath
        End If

    End Sub

    ''' <summary>
    ''' Reads the config.xml file.
    ''' </summary>
    ''' <remarks>
    ''' With the exception of fullpathname ("/config/apps"), all terms are read from 
    ''' "/config/apps/app[@name='pdfmodul']" from the config.xml.     
    ''' <list type="bullet">
    '''   <item><description>target</description></item>
    '''   <item><description>source</description></item>
    '''   <item><description>sourcepassword</description></item>
    '''   <item><description>pdfpagemode</description></item>
    '''   <item><description>pdffit</description></item>
    '''   <item><description>pdfstartpage</description></item>
    '''   <item><description>pdfpagelayout</description></item>
    '''   <item><description>pdfout</description></item>
    '''   <item><description>pdfcustompdfmark</description></item>
    '''   <item><description>SaveAsPDF</description></item>
    '''   <item><description>ClassicPDFGeneration</description></item>
    ''' </list>
    ''' It also tries to determine the type of the application by analysing the Document Name (.doc? .xls?)
    ''' </remarks>
    ''' <returns>true if the file was opend and could be read, false if something went wrong.</returns>  
    Function ReadConfigFile(oProperties As Properties) As Boolean
        oTrace.TraceEvent(TraceEventType.Information, 1, "Entering Options.ReadConfigFile")

        Dim oXpathDoc As XPathDocument
        Dim oXmlNav As XPathNavigator
        Dim oResult As Object
        Dim sValue As String

        Dim sXpathBase As String = "/config/apps/app[@name='" & Main.sConfigAppName & "' and @order='" & iOrder & "' ]/"

        Try
            oXpathDoc = New XPathDocument(sConfigFile)
            oXmlNav = oXpathDoc.CreateNavigator()
        Catch ex As XPathException
            HandleException(ex, "ReadConfigFile, XMLException")
            Return False
        Catch ex As Exception
            HandleException(ex, "ReadConfigFile")
            Return False
        End Try

        'Read TemplateFilename
        oResult = oXmlNav.SelectSingleNode(sXpathBase & "template")
        If Not oResult Is Nothing Then
            sTemplateFilename = CheckPath(oResult.ToString)
            oTrace.TraceEvent(TraceEventType.Information, 11, "TemplateFilename = " & sTemplateFilename)
        End If

        'Read IndexPdfFilename
        oResult = oXmlNav.SelectSingleNode(sXpathBase & "indexpdf")
        If Not oResult Is Nothing Then
            IndexPdfFilename = CheckPath(oResult.ToString)
            oTrace.TraceEvent(TraceEventType.Information, 11, "IndexPdfFilename = " & sIndexPdfFilename)
        End If

        ' Laden aller Blöcke.
        oResult = oXmlNav.Select(sXpathBase & "blocks/block")
        If Not oResult Is Nothing Then

            'Feldgrösse anpassen
            ReDim sFile_Source(oResult.count)
            ReDim sFile_Bookmark(oResult.count)

            For Each oXmlNav2 In oResult
                iFile_Sources = iFile_Sources + 1
                sFile_Source(iFile_Sources) = CheckPath(oXmlNav2.ToString)
                sFile_Bookmark(iFile_Sources) = oXmlNav2.GetAttribute("name", "")
            Next oXmlNav2

        End If

        'Properties auslesen
        oTrace.TraceEvent(TraceEventType.Information, 1, "Reading Doc Properties from config.")
        oResult = oXmlNav.Select(sXpathBase & "properties/custom-property")
        If Not oResult Is Nothing Then
            For Each oXmlNav2 In oResult

                If oXmlNav2.ToString Is Nothing Or oXmlNav2.ToString = "" Then
                    sValue = " "
                Else
                    sValue = Replace(oXmlNav2.ToString, "\r\n", vbCrLf)
                End If

                'Include replacing of vbCrLf Tokens
                oProperties.AddProperty(oXmlNav2.GetAttribute("name", ""), sValue)
            Next oXmlNav2
        End If

        Return True

    End Function

    ''' <summary>
    ''' Checks if there is an "\" in the path, else the Path to the Config will be added
    ''' </summary>
    ''' <param name="sFile">The filename with or without path.</param>  
    ''' <returns>File with path</returns>  
    Function CheckPath(sFile) As String

        If Not InStr(sFile.ToString, "\") > 0 Then
            CheckPath = ConfigPath & "\" & sFile.ToString
        Else
            CheckPath = sFile.ToString
        End If

    End Function

End Class
