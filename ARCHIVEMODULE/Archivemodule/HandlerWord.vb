﻿' <summary>Handels everything Word.</summary>

Imports System.Xml.XPath
Imports Microsoft.Office.Interop.Word
Imports Microsoft.Office.Core
Imports System.Text
Imports System.Security.Cryptography
Imports System.Text.RegularExpressions

Public Class HandlerWord

    Private oWord As Application = Nothing
    Private sDocumentNameSource As String
    Private sDocumentNameTemplate As String

    Private oDocSource As Document
    Private oDocTemplate As Document

    ''' <summary>
    ''' Contructor.
    ''' Initializes the Word Object.
    ''' </summary>
    Public Sub New()
        oTrace.TraceEvent(TraceEventType.Information, 1, "Entering HandlerWord.New")

        'Initialize the Word Object
        oWord = New Application
        oWord.Visible = False

    End Sub

    ''' <summary>
    ''' In absence of a 'real' destructor.
    ''' </summary>
    Public Sub CleanUp()
        oTrace.TraceEvent(TraceEventType.Information, 1, "Entering HandlerWord.CleanUp")

        'If the object is already empty, there is nothing to do here
        If oWord Is Nothing Then Exit Sub

        'Now leave word.
        oWord.Quit(False)

        'Free the word object
        oWord = Nothing

    End Sub

    ''' <summary>
    ''' In Case the Archivemodule exits due to an error, Main has to have the possibility to close the word instance gracefully.
    ''' </summary>
    ''' <returns>the Word Object</returns>  
    Public Function GetWordBackup() As Application

        Return oWord

    End Function

    ''' <summary>
    ''' Open the associated document.
    ''' </summary>
    ''' <param name="sDocFile">The documents filename (including path).</param>  
    ''' <param name="iType">Which type of document. 1 = Template; 2 = Import</param>  
    ''' <returns>true if the file was opend, false if something went wrong.</returns>  
    Public Function OpenDoc(ByVal sDocFile As String, ByVal iType As Integer) As Boolean
        oTrace.TraceEvent(TraceEventType.Information, 1, "Entering HandlerWord.OpenDoc")
        oTrace.Flush()

        oTrace.TraceEvent(TraceEventType.Information, 1, "Opening " & sDocFile)

        Try
            oTrace.TraceEvent(TraceEventType.Information, 1, "Document Size: " & My.Computer.FileSystem.GetFileInfo(sDocFile).Length)
            If iType = 1 Then
                oDocTemplate = oWord.Documents.OpenNoRepairDialog(sDocFile)
                sDocumentNameTemplate = sDocFile
            Else
                oDocSource = oWord.Documents.OpenNoRepairDialog(sDocFile)
                sDocumentNameSource = sDocFile
            End If
        Catch ex As Exception
            HandleException(ex, "OpenDoc")
            Return False
        End Try

        Return True

    End Function

    ''' <summary>
    ''' Closes a document.
    ''' </summary>
    ''' <param name="sDocFile">The documents filename (including path).</param>  
    Sub CloseDoc(ByVal sDocFile As String)

        Try
            oWord.Documents(sDocFile).Close(WdSaveOptions.wdDoNotSaveChanges)
        Catch ex As Exception
            HandleException(ex, "CloseDoc")
        End Try

    End Sub

    ''' <summary>
    ''' Retrieves all custom Properties from the word documents and writes them to the Properties-Object.
    ''' </summary>
    ''' <param name="oProperties">The Properties Object.</param>  
    Public Sub RetrieveDocumentProperties(ByVal oProperties As Properties)
        oTrace.TraceEvent(TraceEventType.Information, 1, "Entering HandlerWord.RetrieveDocumentProperties")

        Dim oProperty As Object

        'Fetch all Properties to the local Object
        For Each oProperty In oWord.Documents(sDocumentNameTemplate).CustomDocumentProperties
            oProperties.AddProperty(oProperty.Name, oProperty.Value)
        Next oProperty

    End Sub

    ''' <summary>
    ''' writes all custom Properties to the word document
    ''' </summary>
    ''' <param name="oProperties">The Properties Object.</param>  
    Public Sub TransferDocumentProperties(ByVal oProperties As Properties, sFilenamePDF As String)
        oTrace.TraceEvent(TraceEventType.Information, 1, "Entering HandlerWord.TransferDocumentProperties")

        Dim sPropertyName As String
        Dim sPropertyValue As String
        Dim oCustomProperty As Object = Nothing

        Dim oCustomProperties As Object
        Dim oProp As Object

        'Get the Custom Document Properties collection.
        oCustomProperties = oDocTemplate.CustomDocumentProperties

        'Remove all Doc Variables 
        For Each oProp In oDocTemplate.Variables
            oProp.Delete()
        Next

        For Each sPropertyName In oProperties.GetAllKeys
            sPropertyValue = oProperties.GetProperty(sPropertyName)

            'Check if the doc property already exists
            Try
                oCustomProperty = oCustomProperties.item(sPropertyName)
            Catch ex As Exception
                oCustomProperty = Nothing
            End Try

            If oCustomProperty Is Nothing Then
                oCustomProperties.Add(sPropertyName, False, MsoDocProperties.msoPropertyTypeString, sPropertyValue)
            Else
                oCustomProperty.Value = sPropertyValue
            End If

            'Add the same as doc variable 
            oDocTemplate.Variables.Add(Name:=sPropertyName, Value:=sPropertyValue)

            oCustomProperty = Nothing

        Next

        'Add PDF as document property 
        Try
            oCustomProperty = oCustomProperties.Item("filename_pdf")
        Catch ex As Exception
            'oCustomProperty will be nothing when failed.
        End Try

        If oCustomProperty Is Nothing Then
            oCustomProperties.Add("filename_pdf", False, MsoDocProperties.msoPropertyTypeString, sFilenamePDF)
        Else
            oCustomProperty.Value = sFilenamePDF
        End If


    End Sub


    ''' <summary>
    ''' Saves the document as a PDF document
    ''' </summary>
    ''' <param name="sFilename">The pdf filename (including path).</param>  
    ''' <returns>True if the document was exported successfully, false if something went wrong</returns>  
    Public Function SaveAsPDF(ByVal sFilename As String) As Boolean
        oTrace.TraceEvent(TraceEventType.Information, 1, "Entering HandlerWord.SaveAsPDF")

        Try
            'Save Document as PDF/A (UseISO19005_1)
            oDocTemplate.ExportAsFixedFormat(OutputFileName:=sFilename, ExportFormat:=WdExportFormat.wdExportFormatPDF, UseISO19005_1:=True, IncludeDocProps:=True)
            oTrace.TraceEvent(TraceEventType.Information, 1, "Saved PDF File: " & sFilename)

            'Word adds a .pdf, if no extension is given. So check, if the file exists.
            If Not IO.File.Exists(sFilename) Then

                Try
                    My.Computer.FileSystem.RenameFile(sFilename & ".pdf", IO.Path.GetFileName(sFilename))
                Catch ex As Exception
                    oTrace.TraceEvent(TraceEventType.Critical, 12, "Rename failed. iSR will probably fail to locate the resulting pdf.")
                End Try

            End If

        Catch ex As Exception
            HandleException(ex, "SaveAsPDF")
            Return False
        End Try

        Return True

    End Function

    ''' <summary>
    ''' Removes the protection of the document using the given password
    ''' </summary>
    ''' <param name="sPassword">The password for the document.</param>  
    ''' <returns>True if the document was unprotected successfully, false if something went wrong</returns>  
    Public Function UnprotectDocument(ByVal sPassword As String) As Boolean
        oTrace.TraceEvent(TraceEventType.Information, 1, "Entering HandlerWord.UnprotectDocument")

        Try
            If oWord.Documents(sDocumentNameTemplate).ProtectionType <> WdProtectionType.wdNoProtection Then
                oWord.Documents(sDocumentNameTemplate).Unprotect(GenerateMD5Hash(sPassword).Substring(0, 8))
            End If
        Catch ex As Exception
            HandleException(ex, "SaveDocument")
            Return False
        End Try

        Return True

    End Function

    ''' <summary>
    ''' Saves the document
    ''' </summary>
    ''' <param name="sFilename">The pdf filename (including path).</param>  
    ''' <returns>True if the document was saved, false if something went wrong</returns>  
    Public Function SaveDocument(ByVal sFilename As String) As Boolean
        oTrace.TraceEvent(TraceEventType.Information, 1, "Entering HandlerWord.SaveDocument")

        Try
            oWord.Documents(sDocumentNameTemplate).SaveAs(FileName:=sFilename)
        Catch ex As Exception
            HandleException(ex, "SaveDocument")
            Return False
        End Try

        Return True

    End Function

    ''' <summary>
    ''' Generates a MD5 Hash, which can be used for password "decryption"
    ''' </summary>
    ''' <param name="sText">The 'raw' password</param>  
    ''' <returns>The MD5 hash for the given string</returns>  
    Private Function GenerateMD5Hash(ByVal sText As String) As String
        oTrace.TraceEvent(TraceEventType.Information, 1, "Entering HandlerWord.GenerateMD5Hash")

        'Instantiate an MD5 Provider object
        Dim Md5 As New MD5CryptoServiceProvider()

        Dim aHashResult As Byte()
        Dim sTemp As String = ""
        Dim sConvResult As String = ""

        'Compute the hash value from the source
        aHashResult = Md5.ComputeHash(Encoding.ASCII.GetBytes(sText))

        For i As Integer = 0 To aHashResult.Length - 1
            sTemp = Hex(aHashResult(i)).ToLower
            If sTemp.Length = 1 Then sTemp = "0" & sTemp
            sConvResult += sTemp
        Next

        Return sConvResult

    End Function

    ''' <summary>
    ''' Sets the orientation
    ''' </summary>
    ''' <param name="iOrientation">the target orientation.</param>  
    Public Sub SetOrientation(ByVal iOrientation As WdOrientation)
        oTrace.TraceEvent(TraceEventType.Information, 1, "Entering HandlerWord.SaveDocument")

        Try
            oWord.ActiveDocument.PageSetup.Orientation = iOrientation
        Catch ex As Exception
            HandleException(ex, "SetOrientation")
        End Try

    End Sub

    Function InsertTables(sSourceBMnames As List(Of String), oHandlerWordImport As HandlerWord) As Boolean

        Dim oSourceBMrange As Range
        Dim oTargetBMrange As Range
        Dim i As Integer

        oTrace.TraceEvent(TraceEventType.Information, 1, "Entering HandlerWord.InsertTable")

        With oWord.Documents(sDocumentNameTemplate)

            .ActiveWindow.Selection.GoTo(What:=WdGoToItem.wdGoToBookmark, Name:="ARCHIVE")
            .ActiveWindow.Selection.Collapse(Direction:=WdCollapseDirection.wdCollapseStart)

            Try

                For i = 0 To sSourceBMnames.Count() - 1

                    If i <> 0 Then .ActiveWindow.Selection.InsertBreak(WdBreakType.wdPageBreak)

                    oSourceBMrange = oWord.Documents(sDocumentNameSource).Bookmarks(sSourceBMnames(i)).Range
                    oTargetBMrange = .Range

                    .ActiveWindow.Selection.TypeText(Text:="  ")
                    .ActiveWindow.Selection.MoveLeft(Unit:=WdUnits.wdCharacter, Count:=2, Extend:=WdMovementType.wdExtend)

                    'Copy the Bookmark contents from Import file to template
                    oTargetBMrange.FormattedText = oSourceBMrange.FormattedText

                    'Move cursor to end of the ARCHIVE bookmark
                    .ActiveWindow.Selection.Start = oTargetBMrange.End

                Next i

            Catch ex As Exception
                oTrace.TraceEvent(TraceEventType.Warning, 1, "Not possible to insert " & String.Join(", ", sSourceBMnames))
                HandleException(ex, "InsertTables")
                Return False
            End Try

        End With

        oTrace.TraceEvent(TraceEventType.Information, 1, "Leaving HandlerWord.InsertTable")

        Return True

    End Function

    Function InsertComplete(oHandlerWordImport As HandlerWord) As Boolean

        Dim oSourceBMrange As Range
        Dim oTargetBMrange As Range

        oTrace.TraceEvent(TraceEventType.Information, 1, "Entering HandlerWord.InsertComplete")

        With oWord.Documents(sDocumentNameTemplate)

            .ActiveWindow.Selection.GoTo(What:=WdGoToItem.wdGoToBookmark, Name:="ARCHIVE")
            .ActiveWindow.Selection.Collapse(Direction:=WdCollapseDirection.wdCollapseStart)

            Try

                oSourceBMrange = oWord.Documents(sDocumentNameSource).Range
                oSourceBMrange.WholeStory()

                oTargetBMrange = .Bookmarks("ARCHIVE").Range
                
                .ActiveWindow.Selection.TypeText(Text:="  ")
                .ActiveWindow.Selection.MoveLeft(Unit:=WdUnits.wdCharacter, Count:=2, Extend:=WdMovementType.wdExtend)

                'Copy the Bookmark contents from Import file to template
                oTargetBMrange.FormattedText = oSourceBMrange.FormattedText

                'Move cursor to end of the ARCHIVE bookmark
                .ActiveWindow.Selection.Start = oTargetBMrange.End

            Catch ex As Exception
                oTrace.TraceEvent(TraceEventType.Warning, 1, "Not possible to insert.")
                HandleException(ex, "InsertTables")
                Return False
            End Try

        End With

        oTrace.TraceEvent(TraceEventType.Information, 1, "Leaving HandlerWord.InsertTable")

        Return True

    End Function

    Function GetListOfBookmarks(sPattern As String) As List(Of String)

        Dim lBookmarks As List(Of String) = New List(Of String)
        Dim regex As Regex = New Regex(sPattern)

        For Each oBookmark As Bookmark In oWord.ActiveDocument.Bookmarks
            If regex.Match(oBookmark.Name).Success Then lBookmarks.Add(oBookmark.Name)
        Next oBookmark

        Return lBookmarks

    End Function

    ''' <summary>
    ''' Creates a PDF with a list of links to all generated PDF Files
    ''' </summary>
    Public Function InsertEntryLink(sText As String, sLink As String) As Integer

        Dim oParagraph As Paragraph = oWord.Documents(sDocumentNameTemplate).Paragraphs.Add()

        oParagraph.Range.Text = sText
        oParagraph.Range.Hyperlinks.Add(oParagraph.Range, sLink)
        oParagraph.Range.InsertParagraphAfter()

        Return 0

    End Function

    ''' <summary>
    ''' Adds a text to the pdf file
    ''' </summary>
    Public Function InsertIndexDescription(sText As String) As Integer

        Dim oParagraph As Paragraph = oWord.Documents(sDocumentNameTemplate).Paragraphs.Add()

        oParagraph.Range.Text = sText
        oParagraph.Range.InsertParagraphAfter()

        Return 0

    End Function

    ''' <summary>
    ''' Adds a text to the pdf file
    ''' </summary>
    Public Function InsertEmtpyParagraph() As Integer

        Dim oParagraph As Paragraph = oWord.Documents(sDocumentNameTemplate).Paragraphs.Add()

        oParagraph.Range.InsertParagraphAfter()

        Return 0

    End Function

    ''' <summary>
    ''' Updates the target document (esp. fields)
    ''' </summary>
    Public Sub UpdateFields()

        Dim oStory As Object

        'Update fields in all stories
        For Each oStory In oDocTemplate.StoryRanges
            oStory.Fields.Update()
        Next oStory

        oTrace.TraceEvent(TraceEventType.Information, 1, "Stories updated")

    End Sub

End Class
