﻿Public Class MetaData

    ''' <summary>
    ''' Word Properties !
    ''' </summary>
    Private lProperties As List(Of Properties)
    ''' <summary>
    ''' The Bookmark names
    ''' </summary>
    Public Property Properties(iNum As Integer) As Properties
        Get
            Return lProperties(iNum)
        End Get
        Set(ByVal value As Properties)
            lProperties.Add(value)
        End Set
    End Property

    ''' <summary>
    ''' List that contains the Source filenames
    ''' </summary>
    Private lSource As List(Of String)
    ''' <summary>
    ''' The Bookmark names
    ''' </summary>
    Public Property Source(iNum As Integer) As String
        Get
            Return lSource(iNum)
        End Get
        Private Set(ByVal value As String)
            lSource.Add(value)
        End Set
    End Property

    ''' <summary>
    ''' List that contains the Bookmark names
    ''' </summary>
    Private lBookmark As List(Of String)
    ''' <summary>
    ''' The Bookmark names
    ''' </summary>
    Public Property Bookmark(iNum As Integer) As String
        Get
            Return lBookmark(iNum)
        End Get
        Private Set(ByVal value As String)
            lBookmark.Add(value)
        End Set
    End Property

    ''' <summary>
    ''' List that contains the Hashbase 
    ''' </summary>
    Private lHashbase As List(Of String)
    ''' <summary>
    ''' The Hashbase 
    ''' </summary>
    Public Property Hashbase(iNum As Integer) As String
        Get
            Return lHashbase(iNum)
        End Get
        Private Set(ByVal value As String)
            lHashbase.add(value)
        End Set
    End Property

    ''' <summary>
    ''' List that contains the Hash
    ''' </summary>
    Private lHash As List(Of String)
    ''' <summary>
    ''' The Hash
    ''' </summary>
    Public Property Hash(iNum As Integer) As String
        Get
            Return lHash(iNum)
        End Get
        Private Set(ByVal value As String)
            lHash.Add(value)
        End Set
    End Property

    ''' <summary>
    ''' List that contains the Contents
    ''' </summary>
    Private lContents As List(Of String)
    ''' <summary>
    ''' The Contents
    ''' </summary>
    Public Property Contents(iNum As Integer) As String
        Get
            Return lContents(iNum)
        End Get
        Private Set(ByVal value As String)
            lContents.Add(value)
        End Set
    End Property

    ''' <summary>
    ''' List that contains the Contents
    ''' </summary>
    Private lIndexDesc As List(Of String)
    ''' <summary>
    ''' The Contents
    ''' </summary>
    Public Property IndexDesc(iNum As Integer) As String
        Get
            Return lIndexDesc(iNum)
        End Get
        Private Set(ByVal value As String)
            lIndexDesc.Add(value)
        End Set
    End Property

    ''' <summary>
    ''' List that contains the content type
    ''' </summary>
    Private lcontentType As List(Of String)
    ''' <summary>
    ''' The content types
    ''' </summary>
    Public Property ContentType(iNum As Integer) As String
        Get
            Return lcontentType(iNum)
        End Get
        Private Set(ByVal value As String)
            lcontentType.Add(value)
        End Set
    End Property

    ''' <summary>
    ''' List that contains the content path
    ''' </summary>
    Private lcontentPath As List(Of String)
    ''' <summary>
    ''' The content types
    ''' </summary>
    Public Property ContentPath(iNum As Integer) As String
        Get
            Return lcontentPath(iNum)
        End Get
        Private Set(ByVal value As String)
            lcontentPath.Add(value)
        End Set
    End Property

    ''' <summary>
    ''' List that contains the ConvertAll Flag
    ''' </summary>
    Private lConvertAll As List(Of String)
    ''' <summary>
    ''' The Contents
    ''' </summary>
    Public Property ConvertAll(iNum As Integer) As Boolean
        Get
            Return lConvertAll(iNum)
        End Get
        Private Set(ByVal value As Boolean)
            lConvertAll.Add(value)
        End Set
    End Property

    ''' <summary>
    ''' Number of entries
    ''' </summary>
    Public ReadOnly Property Count As Integer
        Get
            Return lSource.Count
        End Get
    End Property

    ''' <summary>
    ''' Contructor.
    ''' </summary>
    Public Sub New()
        oTrace.TraceEvent(TraceEventType.Information, 1, "Entering MetaData.New")

        'Initialize the Lists
        lProperties = New List(Of Properties)
        lSource = New List(Of String)
        lBookmark = New List(Of String)
        lHashbase = New List(Of String)
        lHash = New List(Of String)
        lContents = New List(Of String)
        lConvertAll = New List(Of String)
        lIndexDesc = New List(Of String)
        lcontentType = New List(Of String)
        lcontentPath = New List(Of String)

    End Sub


    ''' <summary>
    ''' In absence of a 'real' destructor.
    ''' </summary>
    Public Sub CleanUp()
        oTrace.TraceEvent(TraceEventType.Information, 1, "Entering MetaData.CleanUp")

        lProperties = Nothing
        lSource = Nothing
        lBookmark = Nothing
        lHashbase = Nothing
        lHash = Nothing
        lContents = Nothing
        lConvertAll = Nothing
        lIndexDesc = Nothing
        lcontentType = Nothing
        lcontentPath = Nothing

    End Sub

    ''' <summary>
    ''' Adds a new entry to every Property
    ''' </summary>
    ''' <param name="sBookmark">The bookmark value.</param>  
    ''' <param name="sHashbase">The hashbase value.</param>  
    ''' <param name="sHash">The hash value.</param>  
    ''' <param name="sContents">The contents value.</param>  
    Public Sub AddMetadata(ByVal sSource As String,
                           ByVal sBookmark As String,
                           ByVal sHashbase As String,
                           ByVal sHash As String,
                           ByVal sContents As String,
                           ByVal sConvertAll As String,
                           ByVal sIndexDesc As String,
                           ByVal sContentType As String,
                           ByVal sContentPath As String)
        oTrace.TraceEvent(TraceEventType.Information, 1, "Entering MetaData.AddMetadata:")

        lSource.Add(sSource)
        lBookmark.Add(sBookmark)
        lHashbase.Add(sHashbase)
        lHash.Add(sHash)
        lContents.Add(sContents)
        lIndexDesc.Add(sIndexDesc)
        lcontentType.Add(sContentType.ToUpper)
        lcontentPath.Add(sContentPath)

        If sConvertAll.Substring(0, 1).ToUpper = "T" Then
            lConvertAll.Add(True)
        Else
            lConvertAll.Add(False)
        End If

        oTrace.TraceEvent(TraceEventType.Information, 10, "Added: " & sBookmark & "; " & sHashbase & "; " & sHash & "; " & sContents & "; ")

    End Sub

    ''' <summary>
    ''' Checks if the path is used
    ''' </summary>
    ''' <returns>true if a set path was found</returns>  
    Public Function CheckForPath() As Boolean

        Dim sPath As String

        For Each sPath In lcontentPath
            If Not String.IsNullOrWhiteSpace(sPath) Then
                Return True
            End If
        Next

        Return False

    End Function

End Class
