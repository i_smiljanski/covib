﻿Imports System.IO
Imports System.Diagnostics

Public Class Logging

    '''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
    ' Klasse            : Logging
    ' Beschreibung      : Stellt den Logging Mechanismus zur Verfügung
    ' Autor             : ms
    ' Datum             : 19.03.2004
    ' Änderungshistorie :
    ' Autor  Datum       Grund            Kommentar
    ' -----  ----------  ---------------  ---------------------------------------
    '
    '''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''

    'Private TraceListener As WordmoduleListener = New WordmoduleListener()
    Private idxLogListener As New Integer()
    Private mvarLogFileOpen As Boolean
    Private sLog As String
    Private oStopWatch As New Stopwatch()

    '''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
    ' Funktion            : OpenLog
    ' Beschreibung        : Öffnet das Logging-File und schreibt einen Header.
    '                       An den übergebenen Filenamen wird ".log" angehängt.
    ' Parameter           : -
    ' wird aufgerufen von : Main
    ' Rückgabewert        : true - wenn File korrekt geöffnet wurde.
    '                       false - wenn beim Öffnen ein Fehler aufgetreten ist.
    ' Änderungshistorie   :
    ' Autor  Datum       Grund            Kommentar
    ' -----  ----------  ---------------  ---------------------------------------
    '
    '''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
    Public Function OpenLog(Optional sLogFileName As String = "") As Boolean

        Dim sFilename = Path.GetFullPath(sLogFileName) & ".log"

        Try

            'Delete an existing log
            If File.Exists(sFilename) Then Kill(sFilename)

            'Configue Trace
            Dim oSwitch = New SourceSwitch("WordModulSwitch", "Information")
            oTrace.Switch = oSwitch
            oTrace.Switch.Level = SourceLevels.Verbose
            'idxLogListener = oTrace.Listeners.Add(TraceListener)
            idxLogListener = oTrace.Listeners.Add(New TextWriterTraceListener(sFilename))

            'Logbeginn
            oStopWatch.Start()
            WriteLog("==[ " & Date.Now & " ]======================================= === == =")

            mvarLogFileOpen = True

            Return True
        Catch ex As Exception
            Return False
        End Try

    End Function

    '''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
    ' Funktion            : CloseLog
    ' Beschreibung        : Schreibt einen Footer und schliesst das Logging-File
    ' Parameter           : -
    ' wird aufgerufen von : Main
    ' Rückgabewert        : true - wenn File korrekt geschlossen wurde.
    '                       false - wenn beim Schliessen ein Fehler aufgetreten ist.
    ' Änderungshistorie   :
    ' Autor  Datum       Grund            Kommentar
    ' -----  ----------  ---------------  ---------------------------------------
    '
    '''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
    Public Function CloseLog() As Boolean

        Try

            'Prüfen, ob überhaupt ein Log offen ist
            If mvarLogFileOpen = False Then Return False

            WriteLog("==[ " & Date.Now & " ]======================================= === == =")

            oStopWatch.Stop()
            oStopWatch = Nothing

            oTrace.Flush()
            oTrace.Close()
            oTrace = Nothing

            Return True

        Catch ex As Exception
            Return False
        End Try

    End Function

    '''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
    ' Funktion            : WriteLog
    ' Beschreibung        : Schreibt einen String in das Logfile
    ' Parameter           : sMsg (String) - der zu schreibende String
    ' wird aufgerufen von : unterschiedlichen Funktionen
    ' Rückgabewert        : true - wenn der String korrekt geschrieben wurde.
    '                       false - wenn beim Schreiben ein Fehler aufgetreten ist.
    ' Änderungshistorie   :
    ' Autor  Datum       Grund            Kommentar
    ' -----  ----------  ---------------  ---------------------------------------
    '
    '''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
    Public Sub WriteLog(sMsg As String)

        oTrace.TraceEvent(TraceEventType.Information, 1, oStopWatch.Elapsed.ToString & " | " & sMsg)
        oTrace.Flush()

        sLog = sLog & oStopWatch.Elapsed.ToString & " | " & sMsg & vbCrLf

    End Sub

    Public Function GetWholeLog() As String

        Return sLog

    End Function

End Class
