CREATE OR REPLACE PACKAGE  ISR$action$log
AS
-- ***************************************************************************************************
-- Description:
-- The package controls the input for special logentries into the table ISR$ACTIONLOG. 
-- The output is intended for administrators and provides compressed system activity information. 
-- Reported actions are logon and logoff, some maintenance activity, shutdown, and audits, as well as
-- unconfirmed double entries (Master Data Management). Such unconfirmed entries cause, that reports cannot be finalized.
-- Another output stream for ISR$ACTIONLOG is part of the handling of an error-object, but that matter
-- is not part of this package.
-- ***************************************************************************************************

PROCEDURE setActionLog ( csAction IN VARCHAR2,
                         csReason IN VARCHAR2);
-- ***************************************************************************************************
-- Date and Author: 03 Feb 2006 JB
-- Revisioned and moved from package STB$TRACE to new package ISR$ACTION$LOG: 2015-02-06 vs
-- Procedure writes an entry into the activity table ISR$ACTIONLOG
--
-- Parameters:
-- csAction       the action to be logged
-- csReason       the reference or reason
-- ***************************************************************************************************

END isr$action$log;