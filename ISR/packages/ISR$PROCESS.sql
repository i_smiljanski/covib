CREATE OR REPLACE PACKAGE ISR$PROCESS
as
-- ***************************************************************************************************
-- Description/Usage:
-- The packages is the wrapper for the java classes used during an iStudyReporter installation
-- ***************************************************************************************************

  -- ***************************************************************************
  FUNCTION getSystemProperty(csHost IN varchar2, cnPort IN number, sPropertyName in VARCHAR2) RETURN VARCHAR2;

  -- ***************************************************************************
  PROCEDURE saveBlob(csHost IN varchar2, cnPort IN number, blob IN BLOB, path IN VARCHAR2, file in VARCHAR2);

  -- ***************************************************************************
  FUNCTION loadBlob(csHost IN varchar2, cnPort IN number, sFileName in VARCHAR2) RETURN BLOB;

  -- ***************************************************************************
  PROCEDURE deleteFile(csHost IN varchar2, cnPort IN number, sFile in VARCHAR2);

  -- ***************************************************************************
  FUNCTION RUN_CMD(csHost IN varchar2, cnPort IN number, p_cmd in VARCHAR2) RETURN CLOB;
  
  -- ***************************************************************************
  FUNCTION getProcesses(csHost IN varchar2, cnPort IN number) RETURN NUMBER;
  
  -- ***************************************************************************
  FUNCTION getJavaExe(csHost IN varchar2, cnPort IN number) RETURN VARCHAR2;
  
  -- ***************************************************************************
  FUNCTION getUserHome(csHost IN varchar2, cnPort IN number) RETURN VARCHAR2;
  
  -- ***************************************************************************
  FUNCTION getFileSeparator(csHost IN varchar2, cnPort IN number) RETURN VARCHAR2;
  
  -- ***************************************************************************
  FUNCTION checkProcessServer(csHost IN varchar2, cnPort IN number) RETURN VARCHAR2;
  
  -- ***************************************************************************
  FUNCTION sSessionPath(csHost IN varchar2, cnPort IN number) RETURN VARCHAR2;

PROCEDURE saveBlob(blob IN BLOB, path IN VARCHAR2, file in VARCHAR2);
-- ***************************************************************************************************
-- Date und Autor: 08. Sep 2011, MCD
-- saves a blob to the process server
--
-- Parameter:
-- blob     the file content
-- path     the file path on the process server
-- file     the file name on the process server
--

-- ***************************************************************************************************

FUNCTION loadBlob(sFileName IN VARCHAR2) RETURN BLOB;
-- ***************************************************************************************************
-- Date und Autor: 08. Sep 2011, MCD
-- loads a blob from the process server
--
-- Parameter:
-- sFileName     the path + file name on the process server
--

-- ***************************************************************************************************

PROCEDURE deleteFile(sFile in VARCHAR2);
-- ***************************************************************************************************
-- Date und Autor: 08. Sep 2011, MCD
-- deletes a file/directory on the process server
--
-- Parameter:
-- sFileName     the path + file name on the process server
--

-- ***************************************************************************************************

FUNCTION RUN_CMD(p_cmd in VARCHAR2) RETURN CLOB;
-- ***************************************************************************************************
-- Date und Autor: 08. Sep 2011, MCD
-- executes a command on the process server
--
-- Parameter:
-- p_cmd     the command that will be executed on the process server 
--
-- Return: 
-- 0 when the command can beexecuted successful otherwise 1 
--
-- ***************************************************************************************************

FUNCTION getProcesses RETURN NUMBER;
-- ***************************************************************************************************
-- Date und Autor: 08. Sep 2011, MCD
-- returns the number of processes on the process server
--
--
-- ***************************************************************************************************

FUNCTION getJavaExe RETURN VARCHAR2;
-- ***************************************************************************************************
-- Date und Autor: 08. Sep 2011, MCD
-- returns the path of the java runtime on the process server
--
--
-- ***************************************************************************************************

FUNCTION getUserHome RETURN VARCHAR2;
-- ***************************************************************************************************
-- Date und Autor: 08. Sep 2011, MCD
-- returns the path of the user home on the process server
--
--
-- ***************************************************************************************************

FUNCTION getFileSeparator RETURN VARCHAR2;
-- ***************************************************************************************************
-- Date und Autor: 08. Sep 2011, MCD
-- returns the file separator on the process server
--
--
-- ***************************************************************************************************

FUNCTION checkProcessServer RETURN VARCHAR2;
-- ***************************************************************************************************
-- Date und Autor: 08. Sep 2011, MCD
-- checks if a process server is available 
--
--
-- ***************************************************************************************************

FUNCTION sSessionPath RETURN VARCHAR2;
-- ***************************************************************************************************
-- Date und Autor: 08. Sep 2011, MCD
-- returns the session path on the process server
--
--
-- ***************************************************************************************************

END ISR$PROCESS;