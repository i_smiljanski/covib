CREATE OR REPLACE PACKAGE ISR$REMOTE$COLLECTOR
  as

-- ***************************************************************************************************
-- Description/Usage:
-- the package iSR$Remote$Collector implements the local remote collector interface of iStudyReporter 

FUNCTION fillTmpTableForEntity (csEntity IN VARCHAR2, coParamList IN iSR$ParamList$Rec, csWizard IN VARCHAR2, csMasterKey IN VARCHAR2 DEFAULT NULL) RETURN STB$OERROR$RECORD;
-- ***************************************************************************************************
-- The function GetDataList has been renamed to fillTmpTableForEntity. 
-- It retrieves data for the entity (specified in csEntity)
-- and stores all results in the mapped temporary table. The name of the temporary table consists of TMP$ followed by the name of the entity, e.g. TMP$SB$LIMITS. 
-- The function fillTmpTableForEntity does not fetch the data from either local or remote systems by itself,
-- in fact it calls the remote collector object to fetch the data.
--
-- Parameters:
-- csEntity        the entity.
-- coParamList     object containing parameters like current report or report type etc
-- csWizard        Flag for identification, if the function is called from the wizard.
-- csMasterKey     There is no processing of this parameter within fillTmpTableForEntity, but it is passed along to the call of /*TODO*/ callRoutine.
--
-- Return:
-- STB$OERROR$RECORD  
--
-- ***************************************************************************************************

FUNCTION GetWizardList( csEntity    IN     VARCHAR2, 
                        coParamList IN     iSR$ParamList$Rec, 
                        oSelection  IN OUT ISR$TLRSELECTION$LIST, 
                        sFlag       OUT    VARCHAR2, 
                        csMasterKey IN     VARCHAR2  DEFAULT NULL, 
                        bDelete            BOOLEAN   DEFAULT TRUE) 
  RETURN STB$OERROR$RECORD;
-- ***************************************************************************************************
-- Retrieves a list of all  selectable entries from sEntity in the underlying LIMS
-- Only the parameters sEntity, oSelection, sFlag, sMaskWZEntity are used since 3.0
--
-- Parameters:
-- csEntity        the entity from which should be selected   
-- coParamList     object containing parameters like current report or report type etc
-- oSelection      object list with the fields key, display, info, selected ,order number
-- sFlag           not filled
-- csMaskWZEntity  the selection mask in the report wizard
-- bDelete         /*TODO*/ 
--
-- Return:
-- STB$OERROR$RECORD    
--
-- ***************************************************************************************************

FUNCTION getList(csEntity IN VARCHAR2, coParamList IN iSR$ParamList$Rec, bDelete BOOLEAN DEFAULT TRUE) RETURN ISR$TLRSELECTION$LIST;
-- ***************************************************************************************************
-- Date und Autor: 05. Mar 2010, MCD
-- function return the same as getWizardList, but it has not to be filled with parameters
--
-- Parameter:
-- csEntity     the entity which the selection list has to be filled for
-- coParamList     object containing parameters like current report or report type etc
--
-- ***************************************************************************************************

END iSR$Remote$Collector;