CREATE OR REPLACE PACKAGE isr$reportfile
is
-- ***************************************************************************************************
-- Description/Usage:
-- The package holds the functinality to report fragments, saved in isr$report$file.
-- ****************************************************************************************************

function copyReportFiles(cnJobid in number, cxXml IN OUT xmltype, oFileTypeList in isr$varchar$list default null) return STB$OERROR$RECORD;
-- ***************************************************************************************************
-- Date and Autor: 13.Sep 2016, IS
-- copies the report fragments (html tables, graphics, rowdata) to isr$report$file
--
-- Parameter:
-- cnJobid   the identifier of the iStudyReporter job
-- cnDomId   the domid of the xml configuration file
--
-- Return:
-- STB$OERROR$RECORD
--
-- ***************************************************************************************************

function isReportFileExist(nRepid in isr$report$file.repid%type, sFilename in isr$report$file.filename%type) return varchar2;
-- ***************************************************************************************************
-- Date and Autor: 16.Sep 2016, IS
-- checks if entry exists in isr$report$file for given repid and filename
-- this function is not used yet, but we can need it later:
--  maybe in some actions the report files should not be overwritten
--
-- Parameter:
-- nRepid
-- sFilename
--
-- Return:
-- 'T' or 'F'
--
-- ***************************************************************************************************
end isr$reportfile;