CREATE OR REPLACE PACKAGE iSR$Local$Entity
is
-- ***************************************************************************************************
-- Description/Usage:
-- The packages holds the code for collecting data for Entities with localEntity='T' in isr$Meta$Entity
-- ***************************************************************************************************


function fillTmpTableForEntity( csEntity    in  varchar2, 
                      coParamList in  isr$ParamList$Rec, 
                      csWizard    in  varchar2, 
                      csMasterKey in  varchar2 default null ) return STB$OERROR$RECORD;
                      
FUNCTION insertUserFileIntoJob(blBinaryfile IN BLOB, csFilename IN VARCHAR2, csMimetype IN VARCHAR2, csBlobFlag IN VARCHAR2) RETURN CHAR;                      


end iSR$Local$Entity;