CREATE OR REPLACE PACKAGE ISR$METADATA$PACKAGE
  AS

-- ***************************************************************************************************
-- Description/Usage:
-- the package ISR$METADATA$PACKAGE is the interface to the meta data tables of iStudyReporter. In the meta 
-- data tables the data entities, the attributes and the relations betwen the entites are defined
--
-- Important: Changes in the function GetBaseEntity may have severe impact to the ISR$MDM$PKG package.
--

function getDefaultColString( csTableType varchar2 default 'TMP' ) return varchar2;
-- ***************************************************************************************************
-- Date and Autor:  18. May 2015 vs
-- Function returns a string to be added as final columns in dynamical sql statement.
-- Contents and order are defined with recDefaultTmpCol record type.
--
-- Parameter:  
--   csTableType   Defines the table type(s) to be considered, default is 'TMP'
--                 'ALL' includes all table types.
--                 'TMP' considers temporary tables only
--                 'MDM' considers Master Data Management tables only
-- Return:
--   VARCHAR2     -- sample return: ', serverid number, CritOrderBy number, EntityOrderBy number, selected varchar2(1) '
-- ***************************************************************************************************

function chkEntityTables  return STB$OERROR$RECORD;
-- ***************************************************************************************************
-- Date and Autor:  19. May 2015 vs
-- Function controls the check of all entities due to definition in the table ISR$META$ENTITY
-- The check involves TMP$-tables as well as MDM$-tables
--
-- Parameter:  
--   no parameter
--
-- Return:
--   STB$OERROR$RECORD
--   Returns the highest received severity from all subsequent calls of chkEntityTable
-- ***************************************************************************************************

function chkEntityTable( csEntityName in isr$meta$entity.entityname%type,
                         csTableType  in varchar2   default 'ALL' )
  return STB$OERROR$RECORD;
-- ***************************************************************************************************
-- Date and Autor:  21. May 2015 vs
-- Function checks one entity due to the definition in the tables ISR$META$ENTITY and ISR$META$ATTRIBUTE 
--
-- Parameter:  
--   csEntityname   The name of the entity, defined in isr$meta$entity.entityname, comparison is case sensitive
--   csTableType    Defines the table type(s) to be ckecked, comparison is case sensitive
--                  'TMP' checks the table 'TMP$<entityName> only
--                  'MDM' checks the table 'MDM$<entityName>, if this table exists, if it does not exist, then cnSeverityWarning is returned.
--                  'ALL' checks table 'TMP$<entityName> and table 'MDM$<entityName>
-- Return:
--   STB$OERROR$RECORD  
--
-- List of defined return values and message(s):
--   STB$TYPEDEF.cnNoError
--      All entities and column definitions, including key columns are successfully checked
--   STB$TYPEDEF.cnSeverityCritical
--      a) exNoTable:               The TMP table of the entity does not exist.
--      b) exMetaNoKey:             No primary key defined for entity.
--      c) exTableColumnMissing:    Column defined in metadata or default does not exist in checked table
--      d) exTableDatatype:         Datatype in checked table differs from metadata definition
--   STB$TYPEDEF.cnSeverityWarning
--      a) exTableColumnUndefined:  Undefined column in table found.
--   WARNING message
--      a) exNoTable:               The MDM table of the entity does not exist (ISR$META$ENTITY.MDM='T')
-- ***************************************************************************************************

FUNCTION GetParameterList(oParamList OUT iSR$AttrList$Rec) RETURN STB$OERROR$RECORD;
-- ***************************************************************************************************
-- Date and Autor:  08.Jul 2008 HR
-- function retrieves parameter for entity in sEntity,  
--
-- Parameter
-- oParamList  parameter list object
--
-- Return
-- STB$OERROR$RECORD    
--
-- ***************************************************************************************************

function CreateTempTables( csPluginList in varchar2 default '%',
                           csEntityList in varchar2 default '%' )  return varchar2;
-- ***************************************************************************************************
-- Date and Autor:  16. Nov 2015 vs
-- function creates one or several global temporary table(s) with the structure as described in Table ISR$META$ATTRIBUTE
-- If a temporary table with the same name already exists, it is dropped beforehand.
--
-- Parameters
--   csPluginList         Comma-separated list of plugins to be handled.  The default value '%' represents ALL plugins.
--   csEntityList         Comma-separated list of entities to be handled. The default value '%' represents ALL enities.
--
-- Return
--   varchar2             'T' --> All temporary tables successfully created.
--                        'F' --> Not all temp tables have been created successfully.
--
-- Sample Call
/*****
      declare
        sResult char(1);
      begin
        sResult := isr$metadata$package.createTemptables('%','TMP$SB$STUDY,SB$symBOL');
        dbms_output.put_line('sResult: '||sResult);
      end;
      /
*****/      
-- ***************************************************************************************************

procedure createTempTables( csPluginList in varchar2 default '%',
                            csEntityList in varchar2 default '%' );
-- ***************************************************************************************************
-- Date and Autor:  18. Nov 2015 vs
-- This procedure is a wrapper and calls the function CreateTempTables. If 'F' is returned, 
-- then an exception is thrown. For there is no error handling, the exception is passed to the calling
-- process. More details about behaviour, see function CreateTempTables.
--
-- Parameters
--   csPluginList         Comma-separated list of plugins to be handled.  The default value '%' represents ALL plugins.
--   csEntityList         Comma-separated list of entities to be handled. The default value '%' represents ALL enities.
--
-- Sample Call
--   exec isr$metadata$package.createTemptables('%','TMP$SB$STUDY,SB$symBOL');
--
-- ***************************************************************************************************

FUNCTION createEntityTable( csEntity    in varchar2,
                            csTableType in varchar2 default 'TMP',
                            csUncatched in char     default 'F' )  RETURN STB$OERROR$RECORD;
-- ***************************************************************************************************
-- Date and Autor:  11.Jul 2008 HR
-- function creates a global temporary table with the structure as described in Table ISR$META$ATTRIBUTE
--  for entity specified in csEntity
--  the temp table name is build by a prefix specified in a constant in the package spec 
--  and the entity specified in csEntity
--
-- Parameter
-- sEntity         the entity 
-- csTableType     either TMP (for temporary tables) or MDM (for MDM-tables)
-- csUncatched     F --> normal processing
--                 T --> no logging no handed (or re-raised) exception
--                 
--
-- Return
-- STB$OERROR$RECORD    
--
-- ***************************************************************************************************

FUNCTION DropTempEntityTable(csEntity IN varchar2) RETURN STB$OERROR$RECORD;
-- ***************************************************************************************************
-- Date and Autor:  11.Jul 2008 HR
-- function drops the temp table for the entity specified in csEntity
-- the temp table name to be dropped is build by a prefix specified in a constant in the package spec 
-- and the entity specified in csEntity
--
-- Parameter
--   sEntity         the entity 
--
-- Return
--   STB$OERROR$RECORD    
--
--*********************************************************************************************

function CreateTempEntityIndexes( csEntity    in varchar2) return STB$OERROR$RECORD;
-- ***************************************************************************************************
-- Date and Autor:  5.Mrz 2020 HR
-- function creates indexes for the attributes with the setting 'T' in INDEXONTEMPTAB 
--  in Table ISR$META$ATTRIBUTE
--  for entity specified in csEntity
--  the index names are built by TX followed by a counter and the entityname
--  and the entity specified in csEntity
--
-- Parameter
-- sEntity         the entity 
--
-- Return
-- STB$OERROR$RECORD    
--
-- ***************************************************************************************************

FUNCTION GetBaseEntity(csCritEntity in varchar2, sBaseEntity out varchar2, bTranslated out boolean) RETURN STB$OERROR$RECORD;
-- ***************************************************************************************************
-- Date and Autor:  15.Sep 2008 HR
-- function retrieves base Entity for crit entity defined in iSR$Meta$Crit
--
-- Parameter
--  csCritEntity    the crit entity in iSR$Meta$Crit.CritEntity 
--  sBaseEntity     the base entity from iSR$Meta$Crit.Entity 
--  bTranslated     true if CritEntity and Entity in iSR$Meta$Crit are different
--                  and false if they are equal
--
-- Return
-- STB$OERROR$RECORD    
--
-- ***************************************************************************************************

FUNCTION GetBaseEntity(csCritEntity in varchar2) RETURN varchar2;
-- ***************************************************************************************************
-- Date and Autor:  15.Sep 2008 HR
-- function retrieves mster entity in a grid defined in iSR$Meta$Crit
--  uses overloaded function and return  the base entity
--
-- Parameter
--  csCritEntity    the crit entity in iSR$Meta$Crit.CritEntity 
--
-- Return
-- the master entity of the grid
--
-- ***************************************************************************************************

procedure installTempTables( csEntity     varchar2 default '%',
                             csDbmsOutput char     default 'F' );
-- ***************************************************************************************************
-- Date and Autor:  24.Feb 2016 vs
-- create one or several temporary table(s) or one or several entities
-- Special qualities:
--   1.) no logging wanted 
--   2.) no catched exceptions
--   3.) program aborts immediately, as soon as any deletion-step or a creation-step was not successful
--   4.) No Error, no creation of TMP-Table, if no Meta-Attributes exist: [ISRC-476] Comment Jörg Servos 25.May 2016
--
-- Parameter
--  csEntity      The name of the entity, as defined in table ISR$META$ENTITY 
--  csDbmsOutput  Allows some output in DBMS_OUTPUT
--
-- Sample Calls:  
--   activate DBMS_OUTPUT 
    /**********
      exec ISR$METADATA$PACKAGE.installTempTables('sb$lot');
      exec ISR$METADATA$PACKAGE.installTempTables('UNITTEST%','T');
      exec ISR$METADATA$PACKAGE.installTempTables('%','T' );
      exec ISR$METADATA$PACKAGE.installTempTables( );
      exec ISR$METADATA$PACKAGE.installTempTables( csDbmsOutput => 'T' );
    **********/
-- ***************************************************************************************************                             
                             

END ISR$METADATA$PACKAGE;