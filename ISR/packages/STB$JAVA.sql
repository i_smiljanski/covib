CREATE OR REPLACE PACKAGE Stb$java
as

-- ****************************************************************************************************
-- Description/Usage:
-- the package STB$JAVA contains the Java Wrappers,
-- needed for the loader, the graphic creation and the transformation in the saxon parser ...
-- ****************************************************************************************************

FUNCTION ipInRange(sTheIp IN VARCHAR2, sMinIp IN VARCHAR2, sMaxIp in VARCHAR2) RETURN VARCHAR2;
-- ***************************************************************************************************
-- Date and Autor:  21.12.04 rb
-- checks wether a given IP adress is within the Range minIp-maxIp
-- Parameter:
-- sThe Ip  ip to test
-- sMinIp   lower border
-- sMaxIp   upper border
--
-- Return:
-- "T" if true, "F" if false
--
--*******************************************************************************************************************

FUNCTION TRANSFORM( clXML IN CLOB, clStyleSheet IN CLOB ) RETURN CLOB;
-- ***************************************************************************************************
-- Date and Autor:  24.Apr 2006 mcd
-- performs a sax transformation with an xml and an stylesheet, returns the result
--
-- Parameter:
-- clXML	  	  the xml clob
-- clStyleSheet	  the stylesheet clob
--
-- Return:
-- the transformatted files
--
--*******************************************************************************************************************

FUNCTION TRANSFORM( clXML IN CLOB, clStyleSheet IN CLOB, clParameters IN VARCHAR2 ) RETURN CLOB;
-- ***************************************************************************************************
-- Date and Autor:  24.Apr 2006 mcd
-- performs a sax transformation with an xml and an stylesheet, returns the result
--
-- Parameter:
-- clXML        the xml clob
-- clStyleSheet    the stylesheet clob
--
-- Return:
-- the transformatted files
--
--*******************************************************************************************************************

FUNCTION fInv(alpha in NUMBER, n1 in NUMBER, n2 in NUMBER) RETURN NUMBER;
-- ***************************************************************************************************
-- Date and Autor:  22.Jan 2007 MS
-- Batchcompare Report - retrieves the value for fInv
--
-- Parameter:
-- alpha           Alpha value 
-- n1              n1-1 value               
-- n2              n2-1 value
--
-- Return:
-- returns the result of the calculation
--
-- ***************************************************************************************************

FUNCTION md5_string (str in varchar2) RETURN VARCHAR2;
-- ***************************************************************************************************
-- Date and Autor:  17.Sep 2009 MCD
-- retrieves a hash value of the input string
--
-- Parameter:
-- str      the input string
--
-- Return:
-- the hash of the input string
--
-- ***************************************************************************************************

FUNCTION encryptPassword ( csPassword IN VARCHAR2 ) RETURN VARCHAR2;
-- ***************************************************************************************************
-- Date und Autor: 05. Mar 2010, MCD
-- encrypts a word with the specified key
--
-- Parameter:
-- csPassword     the word to encrypt
-- csKey          the key
--  
--
-- ***************************************************************************************************

FUNCTION decryptPassword ( csPassword IN VARCHAR2 ) RETURN VARCHAR2;
-- ***************************************************************************************************
-- Date und Autor: 05. Mar 2010, MCD
-- decrypts a word
--
-- Parameter:
-- csPassword     the word to decrypt
--  
--
-- ***************************************************************************************************

FUNCTION zip(cnJobid IN NUMBER) RETURN BLOB;
-- ***************************************************************************************************
-- Date und Autor: 05. Mar 2010, MCD
-- zips all files in the jobtrail whith the specified jobid
--
-- Parameter:
-- cnJobid         jobid
--  
--
-- ***************************************************************************************************
FUNCTION zip(filesToZip IN ISR$PARAMETER$LIST) RETURN BLOB;
-- ***************************************************************************************************
-- Date und Autor: 21. Jun 2017, is
-- zips all files in the list 'filesToZip' to blob
--
-- Parameter:
-- filesToZip     list of pairs 'filename; file as blob or clob' 
--  
--
-- Example:
 /*
  oParamList isr$paramlist$rec := isr$paramlist$rec ();   
  bZipped   blob;
  ----------
  for rGetReportFiles in cGetReportFiles loop
    oParamList.AddParam(rGetReportFiles.filename, anydata.convertBlob(rGetReportFiles.binaryfile));
  end loop;
  bZipped := stb$java.zip(oParamList.lParamList);
 */
  
-- ***************************************************************************************************

PROCEDURE unzip(cnJobid IN NUMBER, blZipFile IN BLOB);
-- ***************************************************************************************************
-- Date und Autor: 05. Mar 2010, MCD
-- unzips all files into the jobtrail
--
-- Parameter:
-- cnJobid         jobid
-- blZipFile       the zip file 
--  
--
-- ***************************************************************************************************

PROCEDURE callWordmodule(cnJobid IN NUMBER);
-- ***************************************************************************************************
-- Date und Autor: 05. Mar 2010, MCD
-- call for the new wordmodule
--
-- Parameter:
-- cnJobId      jobid
--  
--
-- ***************************************************************************************************

FUNCTION getRemoteXml ( csHost IN VARCHAR2, cnPort IN NUMBER, csXml IN CLOB, cnRMITimeout IN NUMBER, cnTimeout IN NUMBER, cnLoglevel IN NUMBER ) RETURN CLOB;
-- ***************************************************************************************************
-- Date und Autor: 05. Mar 2010, MCD
-- gets the remote xml in ROWSET/ROW format on a specific host/port
--
-- Parameter:
-- csHost         host
-- csPort         port
-- csXml          input xml (must contain the servers and parameters)
-- cnRMITimeout   timeout for the iSR connection server (-1 means no timeout)
-- cnTimeout      timeout for data fetching (-1 means no timeout)
-- cnLogLevel     user's loglevel
--  
--
-- ***************************************************************************************************

PROCEDURE testConnection ( csHost IN VARCHAR2, csPort IN varchar2 );
-- ***************************************************************************************************
-- Date und Autor: 05. Mar 2010, MCD
-- tests the connection to an observer
--
-- Parameter:
-- csHost        observer host
-- csPort        observer port
--  
--
-- ***************************************************************************************************

function calculateHash (clClob IN CLOB) return varchar2;
-- ***************************************************************************************************
-- Date und Autor: 05. Mar 2010, MCD
-- calculates an hash of an clob
--
-- Parameter:
-- clClob       the clob
--  
--
-- ***************************************************************************************************

PROCEDURE checkStatus(csObserverHost IN VARCHAR2, csObserverPort IN VARCHAR2);
-- ***************************************************************************************************
-- Date und Autor: 05. Mar 2010, MCD
-- checks the status of the observer
--
-- Parameter:
-- csObserverHost        observer host
-- csObserverPort        observer port
-- cnObserverLogSize     the logfile size when to load the logfile
--  
--
-- ***************************************************************************************************

FUNCTION getServerLog (csObserverHost IN VARCHAR2, csObserverPort IN VARCHAR2) RETURN CLOB;
-- ***************************************************************************************************
-- Date und Autor: 05. Mar 2010, MCD
-- gets the log file of the observer
--
-- Parameter:
-- csObserverHost        observer host
-- csObserverPort        observer port
--  
--
-- ***************************************************************************************************

FUNCTION getBootPath (csObserverHost IN VARCHAR2, csObserverPort IN VARCHAR2) RETURN VARCHAR2;
-- ***************************************************************************************************
-- Date und Autor: 05. Mar 2010, MCD
-- gets the boot path of the observer
--
-- Parameter:
-- csObserverHost        observer host
-- csObserverPort        observer port
--  
--
-- ***************************************************************************************************

FUNCTION getFileSeparator (csObserverHost IN VARCHAR2, csObserverPort IN VARCHAR2) RETURN VARCHAR2;
-- ***************************************************************************************************
-- Date und Autor: 05. Mar 2010, MCD
-- gets the file separator of the observer
--
-- Parameter:
-- csObserverHost        observer host
-- csObserverPort        observer port
--  
--
-- ***************************************************************************************************

procedure sendMail(SMTP_HOST_NAME IN VARCHAR2, SMTP_PORT_NAME IN VARCHAR2, SMTP_AUTH_USER IN VARCHAR2, SMTP_AUTH_PWD IN VARCHAR2, to_user IN VARCHAR2, cc IN VARCHAR2, subject IN VARCHAR2, message IN VARCHAR2, csJobId IN VARCHAR2);
-- ***************************************************************************************************
-- Date und Autor: 16. May 2011, MCD
-- sends a java mail
--
-- Parameter:
-- SMTP_HOST_NAME   the smtp host
-- SMTP_PORT_NAME   the smtp port
-- SMTP_AUTH_USER   the auth user
-- SMTP_AUTH_PWD    the password of the auth user
-- to_user          the recipient list
-- cc               the cc list
-- subject          the subject
-- message          the message
-- csJobId          the jobid to get the attachments from stb$jobtrail
--
-- ***************************************************************************************************

FUNCTION setDocumentProperties(blFile in BLOB, clXML in CLOB) RETURN BLOB;
-- ***************************************************************************************************
-- Date und Autor: 16. May 2011, MCD
-- sets the document properties in a document
--
-- Parameter:
-- blFile     the file
-- clXml      the xml file containing the document properties 
--
-- Returns:
-- the file with the new document properties
--
-- ***************************************************************************************************

FUNCTION getBlobByteString(bDoc in BLOB) RETURN CLOB;
-- ***************************************************************************************************
-- Date und Autor: 16. May 2011, MCD
-- turns a blob in a byte string
--
-- Parameter:
-- bDoc     the blob
--
-- Returns:
-- the byte string
--
-- ***************************************************************************************************

FUNCTION getStringByteBlob(cDoc in CLOB) RETURN BLOB;
-- ***************************************************************************************************
-- Date und Autor: 16. May 2011, MCD
-- turns a blob in a byte string
--
-- Parameter:
-- cDoc     the byte string
--
-- Returns:
-- the blob
--
-- ***************************************************************************************************

FUNCTION zip(blBlob IN BLOB, sFileName in VARCHAR2) RETURN BLOB;
-- ***************************************************************************************************
-- Date and Autor:  05. May 2010, MCD
-- zips a blob
--
-- Parameter:
-- blob       the blob
-- sFileName  the filename
--
-- Return:
-- the zipped blob
--
-- **************************************************************************************************

FUNCTION unzip(blBlob IN BLOB) RETURN BLOB;
-- ***************************************************************************************************
-- Date and Autor:  05. May 2010, MCD
-- unzips a blob
--
-- Parameter:
-- blob       the zipped blob
--
-- Return:
-- the unzipped blob
--
-- **************************************************************************************************

/*PROCEDURE convertPdfToImage(cnJobid IN NUMBER, blPDF IN BLOB, sName IN VARCHAR2);
-- ***************************************************************************************************
-- Date und Autor: 09. Aug 2011, is
-- converts the pdf file to image file and saves it in stb$jobtrail
--
-- Parameter:
-- cnJobid      jobid
-- blPDF        the pdf file to convert
-- sName        the name of pdf file
--
-- ***************************************************************************************************
*/
FUNCTION convertPdfToImage(cnJobid IN NUMBER, pdfArray IN ISR$FILEBOOKMARK$LIST, clXslt IN CLOB, sFileExtension IN VARCHAR2) RETURN VARCHAR2;
-- ***************************************************************************************************
-- Date und Autor: 09. Aug 2011, is
-- converts the pdf file to image file and saves it in stb$jobtrail
--
-- Parameter:
-- cnJobid         jobid
-- pdfArray        pdf files from stb$doctrail
-- clXslt          the stylesheet
-- sFileExtension  the image file extension for pdf files (isr$parameter$values)
--
-- Return:exception
-- ***************************************************************************************************
PROCEDURE addAttachmentsToDoc(files IN ISR$FILEBOOKMARK$LIST, blWordFile OUT BLOB);
-- ***************************************************************************************************
-- Date und Autor: 06. Sep 2011, is
-- adds the images to worddocument
--
-- Parameter:
-- files        all job files from stb$jobtrail 
-- blWordFile   worddocument
--
-- ***************************************************************************************************

FUNCTION concatenatePdfs(blFileList IN ISR$FILEBOOKMARK$LIST, clXml IN CLOB) return BLOB;
-- ***************************************************************************************************
-- Date und Autor: 06. Aug 2012, is
-- concatenates the PDF files listed in blFileList, adds headers according the given configuration XML
--
-- Parameter:
-- blFileList   the PDF files to concatenate 
-- clXml        the configuration XML
--
-- ***************************************************************************************************
--PROCEDURE unprotectDocument(blInputDoc IN BLOB, sPassword IN VARCHAR2, sDocFormat IN VARCHAR2, blOutputDoc OUT BLOB);
-- ***************************************************************************************************
-- Date und Autor: 24. Sep 2014, is
-- removes protection from the document regardless of the password. With Aspose
--
-- Parameter:
-- blInputDoc    protected document
-- sPassword   encoded  password
-- sDocFormat 'DOC' or 'DOCX'
-- blOutputDoc  unprotected document
--
-- ***************************************************************************************************

--PROCEDURE  protectWithTrackChanges(blInputDoc IN BLOB, sPassword IN VARCHAR2, sDocFormat IN VARCHAR2, blOutputDoc OUT BLOB);
-- ***************************************************************************************************
-- Date und Autor: 9. Jun 2015, is
-- protect document regardless of the password. With Aspose
--
-- Parameter:
-- blInputDoc    protected document
-- sPassword   encoded  password
-- sDocFormat 'DOC' or 'DOCX'
-- blOutputDoc  unprotected document
--
-- ***************************************************************************************************

function getMaxMemorySize return number;

--******************************************************************************
function setMaxMemorySize(num number) return number;

--******************************************************************************
PROCEDURE SLEEPIMPL (P_MILLI_SECONDS IN NUMBER);

END Stb$java;