CREATE OR REPLACE PACKAGE iSR$RawData
is
-- ***************************************************************************************************
-- Description/Usage:
-- The packages is the entry point of the xml file creation during the document creation process.
-- The package is the used for all iStudyReporter modules (Stability and Bioanalytics)
-- iSR$RawData replaces together with iSR$ReportData the package ISR$COLLECTOR
-- ***************************************************************************************************

FUNCTION createRawDatFile(cnJobId in NUMBER) RETURN STB$OERROR$RECORD;
-- ***************************************************************************************************
-- Date und Autor: 13.Jun 2008  JB
-- function is the raw data collector called from the package stb$action to collect the LIMS data
-- into the internal xml defintion defined in the table "ISR$META$XML"
-- The LIMS data is read from the in temporary tables.
-- As a result the xml file is saved in the table stb$jobtrail
-- function previously called createReport, copied from ISR$COLLECTOR on 21.Sep 2015 by HR
--
--
--
-- Parameter:
-- nJobId       the jobid of the iStudyReporter job
--
--
-- Return:
-- STB$OERROR$RECORD
--
-- ***************************************************************************************************

FUNCTION adjustRawDatFile(cnJobId IN NUMBER, cxXml IN OUT XMLTYPE, csOutputId in  VARCHAR2 default null,
                         csFileId in  VARCHAR2 default null) RETURN STB$OERROR$RECORD;

END iSR$RawData;