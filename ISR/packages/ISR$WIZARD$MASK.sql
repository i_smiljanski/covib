CREATE OR REPLACE PACKAGE ISR$WIZARD$MASK
IS
-- ***************************************************************************************************
-- Description/Usage:
-- the package ISR$WIZARD$MASK is the interface of selectioin scrrens of the type "SINGLE SELECT,
-- MULTI SELECT or MASTER-DETAIL SELECT. The package is independent of the enities
-- It holds the functionality to retrieve and write the criteria belonging to the passed selection scrren
-- into the database. Further it gets default selections and audits on the selection criteria and writes 
--the criteria during the document creation process in the xml file
-- ***************************************************************************************************

function GetSList (sEntity in STB$TYPEDEF.tLine, sMasterKey in VARCHAR2, sNavigateMode in varchar2, oSelection out ISR$TLRSELECTION$LIST) return STB$OERROR$RECORD;
-- ***************************************************************************************************
-- Date and Autor:   19 Dez 2005 JB
-- All available values of the report wizard selection screen are retrieved into the oSelectionRecord. 
-- Previous selected values have the attribute  selected ='T'
--
-- Parameters:
-- sEntity             the current entity in the selection screen
-- sMasterKey          the master key if the selection is a grid select
-- sNavigateMode       flag to indicate if the scrren is called in the navigation mode
-- oCriteria           the previos selected criteria of entities that have relations to
--                     the entity of the selection screen 
-- oParamList          a parameter list to linit the data 
-- oSelection          object list with the attributes "sValue","sMasterKey", "sDisplay", "sInfo", 
--                    "sSelected" and "order number"
--
-- Return:
-- STB$OERROR$RECORD 
--
-- ***************************************************************************************************

function PutSList (sEntity in STB$TYPEDEF.tLine, oSelection in ISR$TLRSELECTION$LIST) return STB$OERROR$RECORD;
-- ***************************************************************************************************
-- Date and Autor:  19 Dez 2005 JB
-- function writes the selected values of a selection screen of the report wizard  into the database
-- table ISR$CRIT. The changes are commited when the report is saved at the end of the wizard.
--
-- Parameters:
-- sEntity          the entity in the selection screen
-- oSelection           object list of the selected values having the attributes
--                  "sMasterKey", "sValue", "sDisplay", "sInfo", "sSelected" and "order number"
--
-- Return:
-- STB$OERROR$RECORD 
--
-- ***************************************************************************************************

function createAudit(xXml IN OUT XMLTYPE, sAudit out varchar2,  nLastFinalRepId in number, nFormNo in number ) return STB$OERROR$RECORD ;
-- ***************************************************************************************************
-- Date und Autor: 26 Jan 2006 jb
-- function writes the audit file on the selection screen into a xml dom if there have been change
--
-- Parameter:
-- cnDomId               the xml dom id
-- sAudit                flag to identify if there have been changes
-- nLastFinalRepId       the previous report id
-- nFormNo               the number of the selection screen in the report wizard
--
-- Return:
-- STB$OERROR$RECORD   
--
-- ***************************************************************************************************

Function InitOutputOptions RETURN stb$oerror$record;
Function ExitOutputOptions RETURN stb$oerror$record;

-- ***************************************************************************************************
-- Date und Autor: 25 Feb 2017 hr
-- init and cleanup for output options handling
-- ***************************************************************************************************



end ISR$WIZARD$MASK;