CREATE OR REPLACE package iSR$RemoteSnapshot as

-- ***************************************************************************************************
-- Description/Usage:
-- The package holds all functionality for the building of a Remote Snapshot XML holding the data from the 
--  temporary tables and for restoring the data into the temporary tables from the RemoteSnapshot XML
-- ****************************************************************************************************

   
-- ****************************************************************************************************
-- functions and procedures
-- ****************************************************************************************************

function BuildReportDataXML (clSnapshotXML out clob) return STB$oError$Record;
-- ***************************************************************************************************
-- Date and Autor: 20.Aug 2015, HR
-- Creates an XML containing the data for all entities of the "current report"
--  These are the entities based on the view isr$collector$entities 
--  which depends on STB$OBJECT.getCurrentReporttypeid
--  The tables name from which the data is added to the xml depends 
--  on the setting of the system parameter TEMP_TAB_PREFIX  
--
-- Parameter:
-- clSnapshotXML    the resulting snapshot XML
--
-- Return:
-- STB$OERROR$RECORD  
--
-- ***************************************************************************************************


function ReplaceDataFromXML(xSnapshotXML in xmltype) return STB$oError$Record;
-- ***************************************************************************************************
-- Date and Autor: 20.Aug 2015, HR
-- Replaces the data of all entities of the "current report" with the data from the snapshot XML
--  The entities are those based on the view isr$collector$entities 
--  which depends on STB$OBJECT.getCurrentReporttypeid
--  The tables name from which the data is added to the xml depends 
--  on the setting of the system parameter TEMP_TAB_PREFIX  
--
-- Parameter:
-- xSnapshotXML    the snapshot XML containing the data
--
-- Return:
-- STB$OERROR$RECORD  
--
-- ***************************************************************************************************


function createReportRemoteSnapshot(cnJobId IN NUMBER) RETURN STB$OERROR$RECORD;
-- ***************************************************************************************************
-- Date and Autor: 10.Sep 2015, HR
-- 
  
end iSR$RemoteSnapshot;