CREATE OR REPLACE PACKAGE STB$RAL
as

-- ***************************************************************************************************
-- Description/Usage:
-- The package holds all functionality to create a criteria report. 
-- Further the create-report function is in this package, 
-- this function is called during the job process by the oracle jobqueue
-- the create report function calls dynamicly the actions being required to full fill the iStudyReporter
-- job
-- ***************************************************************************************************

procedure createReport(cnJobId in number);
-- ***************************************************************************************************
-- Date and Autor: 31 Jan 2006  JB
-- procedure is the start point called by the oracle jobqueuing mechanism 
-- it intalizes the xml dom of the config file
-- and steers the processing of the actions, 
-- the single action steps are called after each other in a loop
--
-- Parameter:
-- cnJobId         the identifier of the current job
--
-- ***************************************************************************************************

end STB$RAL;