CREATE OR REPLACE PACKAGE ISR$FILE$TRANSFER$SERVICE is
-- ***************************************************************************************************
-- Description/Usage:
-- the package ISR$FILE$TRANSFER$SERVICE implements the required functionality for FileTransferServlet

--function getFileAsBlob (csTransferKeyId in varchar2, csErrorXml out clob) return blob;

function getFileFromDb( csConfigName in  isr$transfer$file$config.configname%type, 
                        csTransferId in varchar2, 
                        csErrorXml   out clob
     ) 
  return anydata;
  -- ***************************************************************************************************
-- Date und Autor: 12. Mar 2015, IS

-- The function getFileFromDb retrieves file as anydata  from responsible table. 
-- The function first looks for the appropriate table and then selects the desired anydata
--
-- Parameters:
-- csConfigName     the name of configuration in the table 'isr$file$transfer$config'.
-- csKey1               the key column in appropriate table 
-- csKey2               optional, the second key column in appropriate table
-- csErrorXml         error as xml
--
-- Return:
-- anydata (blob or clob)
--
-- ***************************************************************************************************

procedure saveFileInDb( csConfigName in  isr$transfer$file$config.configname%type,
                          csTransferId in varchar2, 
                          aFile   in  anydata, 
                          csErrorXml   out clob );  

function getKeyDelimiter( csConfigName in  isr$transfer$file$config.configname%type) 
  return varchar2;
  -- ***************************************************************************************************
-- Date und Autor: 15. Sep 2015, IS

--  retrieves value of spalte 'key_delimiter'  in table 'isr$transfer$file$config' for given configuration
--
-- Parameters:
-- csConfigName     the name of configuration in the table 'isr$file$transfer$config'.
--
-- Return:
-- delimiter or null
--
-- ***************************************************************************************************
function getDataType( csConfigName in  varchar2) return varchar2;
-- ***************************************************************************************************
function getDataType( csConfigName in varchar2, csTransferId in  varchar2) return varchar2;

end ISR$FILE$TRANSFER$SERVICE;