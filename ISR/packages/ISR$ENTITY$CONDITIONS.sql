CREATE OR REPLACE PACKAGE ISR$Entity$Conditions
  as

-- ***************************************************************************************************
-- Description/Usage:
-- the package ISR$Entity$Conditions implements the conditions interface for entities 

procedure AddEntityCondition(csEntity IN VARCHAR2, csAttribute in varchar2, clConditionList in isr$varchar$varray, csRelType in varchar2 default 'FK');
-- ***************************************************************************************************
-- Date und Autor: 21. Nov 2017, HR
-- function stores the conditions given in the parameters in the table tmp$entity$condition 
-- the developer will normally not use this procedure with the conditions in a varray collection 
-- but the overloaded one with the conditions in a string
-- However, this procedure is used by its overloaded sister
-- Use this one if you get the conditions from a cursor or another loop!
--
-- Parameter:
-- csEntity        the entity 
-- csAttribute     the Attribute
-- clConditionList the conditionlist as a varray collection 
-- csRelType       typical 'FK', may be some day OR
--
-- ***************************************************************************************************

procedure AddEntityCondition(csEntity IN VARCHAR2, csAttribute in varchar2, csConditionList in varchar2, csRelType varchar2 default 'FK');
-- ***************************************************************************************************
-- Date und Autor: 21. Nov 2017, HR
-- function stores the conditions given in the parameters in the table tmp$entity$condition 
-- written for simple use by developers without collection in parameters
-- Use this one if you know the conditions and do not get them from a loop
--
-- Parameter:
-- csEntity        the entity 
-- csAttribute     the Attribute
-- csConditionList the conditionlist as comma separated string 
-- csRelType       typical 'FK', may be some day OR
--
-- ***************************************************************************************************

procedure RemoveEntityConditions(csEntity IN VARCHAR2);
-- ***************************************************************************************************
-- Date und Autor: 21. Nov 2017, HR
-- function removes the conditions from table tmp$entity$condition for the given entity in csEntity
--
-- Parameter:
-- csEntity        the entity 
--
-- ***************************************************************************************************

procedure RemoveEntityConditions;
-- ***************************************************************************************************
-- Date und Autor: 21. Nov 2017, HR
-- function removes all conditions from table tmp$entity$condition 
--
-- ***************************************************************************************************

END ISR$Entity$Conditions;