CREATE OR REPLACE package iSR$ReportData
is
-- ***************************************************************************************************
-- Description/Usage:
-- The package fills the temporary tables for the current report Job with the LIMS data 
-- iSR$ReportData replaces together with iSR$RawData the package ISR$COLLECTOR  
-- ***************************************************************************************************

function fillTableModel(cnJobId in number) return STB$OERROR$RECORD;
-- ***************************************************************************************************
-- Date und Autor: 13.Jun 2008  JB
-- function fills the temporary tables for the current report Job with the LIMS data 
-- based on the view isr$collector$entities
-- function copied from the package body ISR$COLLECTOR on 21.Sep 2015 by HR
-- 
--
-- Parameter:
-- nJobId       the jobid of the iStudyReporter job
--
--
-- Return:
-- STB$OERROR$RECORD   
--
-- ***************************************************************************************************

end iSR$ReportData;