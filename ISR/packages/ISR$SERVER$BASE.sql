CREATE OR REPLACE PACKAGE "ISR$SERVER$BASE" is

  oParameterBackup              STB$MENUENTRY$RECORD;
  oNodeBackup                   STB$TREENODE$RECORD;
  -- server constants
  csJavaServerFolders  constant varchar2(32) := 'lib,logs,jre';
  sCheckForDialog     varchar2(1) := 'F';
   
-- ***************************************************************************************************
-- Description/Usage:
-- The package contains all functionality to get, set or modify the servers. 
-- The package handles all command tokens that are set up on the node SERVER.
-- *************

function checkServerStatus return STB$OERROR$RECORD;
-- ***************************************************************************************************
-- Date und Autor: 05. Mar 2010, MCD
-- checks the status of all observers in the server list
-- does a test connection to the available observers
--
-- ***************************************************************************************************

function saveServerDialogData( oParameter in STB$MENUENTRY$RECORD, 
                               olParameter in STB$PROPERTY$LIST  ) return STB$OERROR$RECORD;
-- ***************************************************************************************************
-- Date und Autor: 02 Feb 2006  JB, 19. Nov 2013 IS
-- is the counterpart function of callFunction , using the putParameter a
-- command token is procssed from a called dialog window.The entered information (e.g. esig , filter )
-- whes considered when the command token is processed
--
-- Parameter:
-- oParameter      the command token selected in the context menu
-- olParameter     object list holding all information on the fields of the pop-up mask (Object type has the
--                 attributes sParameter, sPromptDisplay,sValue,sDisplay,sEditable,sRequired,sDisplayed,
--                 hasLov,type,format)
--
-- Return:
-- STB$OERROR$RECORD    
--
function getServerParameter( nServerID      in number, 
                             sParameterName in varchar2 ) return varchar2;
-- ***************************************************************************************************
-- Date and Autor: 30 Mar 2012, MCD, 19. Nov 2013 IS
-- gets the parameter value of the server parameter
--
-- Parameters:
-- nServerID      the server ID
-- sParameterName the parameter
--
-- Return
-- server parameter value
--
-- ***************************************************************************************************

FUNCTION getAuditMask( olNodeList out STB$TREENODELIST )  return STB$OERROR$RECORD;
-- ***************************************************************************************************
-- Date und Autor: 02 Feb 2006  JB
-- retrieves a window, holding the audit dialog, in this window the audit reason 
-- of a prompted audit can be entered 
--
-- Parameter:
-- olNodeList      node object holding all fields of the audit mask (Object type has the
--                 attributes sParameter, sPromptDisplay,sValue,sDisplay,sEditable,sRequired,sDisplayed,
--                 hasLov,type,format)
--
-- Return:
-- STB$OERROR$RECORD    
--
-- ***************************************************************************************************

function putAuditMask( olParameter in STB$PROPERTY$LIST )  return STB$OERROR$RECORD;
-- ***************************************************************************************************
-- Date und Autor: 02 Feb 2006  JB
-- writes the audit reason that was entered in the audit window into the 
-- table STB$AUDITTRAIL
--
-- Parameter:
-- olParameter      node object holding all fields of the audit mask (Object type has the
--                 attributes sParameter, sPromptDisplay,sValue,sDisplay,sEditable,sRequired,sDisplayed,
--                 hasLov,type,format)
--
-- Return:
-- STB$OERROR$RECORD    
--
-- ****

function deleteServer(cnServerId in number, csToken in varchar2)  return  STB$OERROR$RECORD;

-- ***************************************************************************************************

FUNCTION createServerDirectory (sServerId VARCHAR2)  return varchar2;

function downloadLogServerFiles ( oParameter  in STB$MENUENTRY$RECORD, 
                                olParameter in STB$PROPERTY$LIST  ) return STB$OERROR$RECORD;
--****************************************************************************************************
procedure setServerStatus(cnServerId IN number, cnServerStatus IN number, csIsCommit in varchar2 default 'T');
--****************************************************************************************************
procedure switchServerStatus(cnServerId in number, csIsDisabled in varchar2);                                                            
--****************************************************************************************************
function deployJavaOnServer(csObserverHost in varchar2, csObserverPort in varchar2, cnServertypeid number,  csServerPath varchar2, csTarget varchar2,  nTimeout NUMBER) return STB$OERROR$RECORD;

 --*************************************************************************************************************************
function deleteServerDirectory( csObserverHost  in varchar2,  csObserverPort  in varchar2, csDirectoryName in varchar2,  csLoglevel  in varchar2, nTimeout NUMBER ) return clob;

FUNCTION createServerDirectory(csObserverHost IN VARCHAR2, csObserverPort IN VARCHAR2, csDirectoryName  IN VARCHAR2, csFolders IN VARCHAR2, 
                         csConcat IN VARCHAR2, csLoglevel  IN VARCHAR2, nTimeout NUMBER) RETURN CLOB;

FUNCTION isServerAvailable(csObserverHost IN VARCHAR2, csObserverPort IN VARCHAR2, csLoglevel  IN VARCHAR2) RETURN VARCHAR2;

FUNCTION startRemoteServer(csObserverHost IN VARCHAR2, csObserverPort IN VARCHAR2, csPort IN VARCHAR2, csClassName IN VARCHAR2, csContext IN VARCHAR2,
                           csNamespace IN VARCHAR2,  csServerid IN VARCHAR2, csServerPath IN VARCHAR2, csJavaVmOptions  IN VARCHAR, csLoglevel  IN VARCHAR2, csLogReference  IN VARCHAR2, nTimeout NUMBER)  RETURN CLOB;

FUNCTION stopRemoteServer(csObserverHost IN VARCHAR2, csObserverPort IN VARCHAR2,  csServerid IN VARCHAR2,  csLoglevel  IN VARCHAR2, nTimeout NUMBER)  RETURN CLOB;
-- ***************************************************************************************************

FUNCTION sendFileToServer(observerHost IN VARCHAR2, observerPort IN VARCHAR2, oParamList  in   isr$Parameter$list)  return CLOB;

FUNCTION sendFileToServer(observerHost IN VARCHAR2, observerPort IN VARCHAR2, fileName IN VARCHAR2, target  IN VARCHAR2, zipped in VARCHAR2, csConfigName IN VARCHAR2,
                                          csId IN VARCHAR2,  logLevel IN VARCHAR2, csLogReference IN VARCHAR2, nTimeout NUMBER) return CLOB;
-- ***************************************************************************************************
-- Date und Autor: 24. Mar 2015, is
-- copies a file to the observer
--
-- Parameter:
-- observerHost        observer host
-- observerPort        observer port
-- fileName            the file name
-- file                the file
-- zipped              flag if the file is zipped and has to be unzipped        
--  
--
-- ***************************************************************************************************

FUNCTION getObserverPlatformName(csObserverHost IN VARCHAR2, csObserverPort IN VARCHAR2,   csLoglevel  IN VARCHAR2, nTimeout NUMBER)  RETURN VARCHAR2;
-- ***************************************************************************************************

FUNCTION getObserverPlatformArch(csObserverHost IN VARCHAR2, csObserverPort IN VARCHAR2,   csLoglevel  IN VARCHAR2, nTimeout NUMBER)  RETURN VARCHAR2;
-- ***************************************************************************************************

FUNCTION fetchLogServerFile(observerHost IN VARCHAR2, observerPort IN VARCHAR2, sServerFolder  IN VARCHAR2, sStartDate  IN VARCHAR2,  sEndDate in VARCHAR2, 
                                            csConfigName IN VARCHAR2,  csId IN VARCHAR2, sDateFormatter in varchar2, csLogLevel IN VARCHAR2, csLogReference IN VARCHAR2, nTimeout NUMBER) return CLOB;
--**********************************************************************************************************************


end isr$server$base;