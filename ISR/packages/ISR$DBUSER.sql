CREATE OR REPLACE PACKAGE ISR$DBUSER
as
-- ***************************************************************************************************
-- Description/Usage:
-- The packages holds all functionality to creates  database users for iStudyReporter .
-- iStudyReporter can use database users to log-on to the application. Then the Oracle
-- functionality like database profiles rule the password limitations and lieftime of the
-- user account. If the database users are created throuph the application than iStudyReporter
-- reuires further database privileges. 
-- It can be defined in the system parameters if iStudyReporter users tare created through the application 
-- or if the users are created by the DBA
-- If the users are created by the DBA only the package spec of the package exists in the database 
-- Further it can be configured in the system parameters if the system adminstrator can modify 
-- the database password of a user. If that is the casse the function "changePasswordISRUser" is used.
-- ***************************************************************************************************

FUNCTION Createsyn(sRoleName in VARCHAR2, sNewUser in VARCHAR2,  sISROwner in VARCHAR2)  RETURN VARCHAR2;
-- ***************************************************************************************************
-- Date und Autor: 20 Jun 2006 jb
-- function creates the required Oracle synonyms of a new user of iStudyReporter
--
-- Parameters:
-- sRoleName          the rdatabase ole of the iStudyReporter users
-- sNewUser           the new user account
-- sISROwner          the schema owner of the iStudyReporter
--
-- Return:
-- STB$OERROR$RECORD  
--
-- ***************************************************************************************************

FUNCTION createUser(sNewUserName in VARCHAR2, sPassword in VARCHAR2,sAdminUser in varchar2) RETURN VARCHAR2;
-- ***************************************************************************************************
-- Date und Autor: 20 Jun 2006 jb
-- function creates the new Oracle user
--
-- Parameters:
-- sNewUserName           the database user
-- sPassword              the password of the new user
-- sAdminUser             the admin user of the iStudyReporter
--
-- Return:
-- STB$OERROR$RECORD   
--
-- ***************************************************************************************************

FUNCTION createGrants(sRole in VARCHAR2, sNewUser in VARCHAR2, sISROwner in varchar2) RETURN VARCHAR2;
-- ***************************************************************************************************
-- Date und Autor: 20 Jun 2006 jb
-- function creates the required grants of the ISROWNER to the new database user
--
-- Parameter:
-- sRoleName          the role of the iStudyReporter users
-- sNewUser           the new user 
-- sISROwner          the schema owner of the iStudyReporter

--
-- Return:
-- STB$OERROR$RECORD   
--
-- ***************************************************************************************************

PROCEDURE changePasswordISRUser(sIsrUser in varchar2, sNewPassword in varchar2);
-- ***************************************************************************************************
-- Date und Autor: 20 Jun 2006 jb
-- changes the Oracle password of an iStudyReporter user . please note if the database user has a
-- profile the new password has to follow the rules defined in the profile otherwise an database error 
-- is raised
--
-- Parameter:
-- sIsrUser         the database user
-- sNewPassword     the new password
--
-- Return:
-- STB$OERROR$RECORD    
--
-- ***************************************************************************************************

END ISR$DBUSER;