CREATE OR REPLACE PACKAGE iSR$Format
  as

-- ***************************************************************************************************
-- Description/Usage:
-- the package iSR$Format containes common formatting data representaion functionality 
--   such as date and number formatting or significant figures etc

FUNCTION ToTime (csTime IN VARCHAR2, dtDate in date, cnRaiseFormatError in integer default 1) RETURN DATE;
-- ***************************************************************************************************
-- Date and Autor:  05.Dec 2008 HR 
-- function converts time data from the text in csTime with unsure format, to a date and adds the 
--   time part to the date dtDate 
-- Supported formats are 'HH:MI AM', 'HH:MI:SS AM', 'HH24:MI' and 'HH24:MI:SS' 
--
-- Parameter:
-- csTime             the time part as text
-- dtDate             the date to which the time should be added
-- cnRaiseFormatError if this parameter is != 1 then no error is raised if csTime has not one of the 
--                      supported formats. In this case NULL is returned
--                    if this parameter is 1 and csTime has not one of the supported formats, then 
--                      an error wit the number 20100 is raised stating that the format is not supported
--
-- Return:
-- the date build from dtDate and the time part from csTime
--
-- Error behaviour/ particularities:
--  as this function is used in SQL, the error object cannot be used for error trapping
-- ***************************************************************************************************

FUNCTION sigFigure( value in number, nFigures in integer) return  number;
-- ***************************************************************************************************
-- Date und Autor: 2008  HSP
-- function calculates and returns a number with the number of nFigures significant figures
--
-- Parameter:
-- Value       the number which should be recalculated
-- nFigures    the number of significant figures
--
-- Return:
-- the converted number
--
-- ***************************************************************************************************

function FormatSig(nVal IN number, nFig IN integer) RETURN  varchar2;
-- ***************************************************************************************************
-- Date und Autor: 17.Nov 2008  HR
-- function formats a number with regard to the number of significant figures
-- the number in nVal should be the return of a previously call to sigFigure
-- If the number has less digits than nFig, then decimal places will be added 
--  to complete the number of digits to be at least nFig
-- For example, if nFig is 3 and the decimal character is the comma, then 1,00 will be returned for nVal=1
--  and 10,0 will be returned for nVal=10 but 100 will be returned for nVal=100
--
-- Parameter:
-- nVal       the number which should be formatted
-- nFig       the number of significant figures
--
-- Return:
-- the formatted number as text
--
-- ***************************************************************************************************

function FormatRounded(nVal IN number, nDec in number) RETURN  varchar2;
-- ***************************************************************************************************
-- Date und Autor: 17.Nov 2008  HR
-- function formats a number with regard to the number of decimal places
-- the number in nVal should be the return of a previously call to the round function
-- If the number has less digits than nDec, then decimal places will be added 
-- For example, if nDec is 2 and the decimal character is the comma, then 1,00 will be returned for nVal=1
--  and 1,20 will be returned for nVal=1.2 and 0,51 will be returned for nVal=.51 
--  (0 is added here in front of the decimal separator)
--
-- Parameter:
-- nVal       the number which should be formatted
-- nDec       the number of significant figures
--
-- Return:
-- the formatted number as text
--
-- ***************************************************************************************************

function FmtNum(nVal in number) RETURN varchar2;
-- ***************************************************************************************************
-- Date und Autor: 22.Feb 2009  HR
-- function formats a number as a string with TM format but with leading 0 if it is between -1 and 1
--
-- Parameter:
-- nVal       the number which should be formatted
--
-- Return:
-- the formatted number as text
--
-- ***************************************************************************************************


end iSR$Format;