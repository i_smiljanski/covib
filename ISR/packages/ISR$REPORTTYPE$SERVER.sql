CREATE OR REPLACE PACKAGE isr$reporttype$server AS
-- ***************************************************************************************************
-- The package holds the functinality to token 'CAN_REPLACE_REPORT_SERVER'
-- ***************************************************************************************************

--******************************************************************************
FUNCTION getNodeChilds (oSelectedNode IN stb$treenode$record, olchildnodes OUT stb$treenodelist) RETURN STB$OERROR$RECORD;
--******************************************************************************
function getNodeList (oSelectedNode in stb$treenode$record, olNodeList out stb$treenodelist) return stb$oerror$record;  
--******************************************************************************
FUNCTION setCurrentNode (oSelectedNode IN stb$treenode$record) RETURN stb$oerror$record;  
--******************************************************************************
function setMenuAccess( sparameter   in varchar2, sMenuAllowed out varchar2 ) return stb$oerror$record;
--******************************************************************************
FUNCTION getContextMenu( nMenu           in  number, olMenuEntryList out stb$menuentry$list) RETURN stb$oerror$record;
--******************************************************************************
function callFunction( oParameter IN STB$MENUENTRY$RECORD, olNodeList OUT STB$TREENODELIST  ) return stb$oerror$record;
--******************************************************************************
FUNCTION putParameters( oParameter  IN STB$MENUENTRY$RECORD, olParameter IN STB$PROPERTY$LIST ) RETURN stb$oerror$record;
--******************************************************************************
FUNCTION getLov (sEntity IN VARCHAR2, ssuchwert IN VARCHAR2, oSelectionList OUT isr$tlrselection$list)  RETURN stb$oerror$record;
--******************************************************************************
FUNCTION getAuditMask(olNodeList OUT STB$TREENODELIST)  RETURN STB$OERROR$RECORD;
-- ***************************************************************************************************
END isr$reporttype$server;