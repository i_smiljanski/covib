CREATE OR REPLACE package isr$debug
is
-- ***************************************************************************************************
-- Description/Usage:
-- This package handles functionality for debugging purposes.
-- ***************************************************************************************************

function createDebugTable( csSourceTable varchar2,
                           csTargetTable varchar2,
                           csTablespace  varchar2 default null )
  return STB$OERROR$RECORD;
-- ***************************************************************************************************
-- Date und Autor: 12 Nov 2015  vs
-- Create a (target) table as select * from source table. The main purpose is the 
-- creation of a debug table. If no csTablespace is entered, then the tablespace value is fetched 
-- from the systemparameter ISR_TABLE_NOLOG_TABLESPACE. If that systemparameter is not defined,
-- then the user's default tablespace is used. A drop of the target table is not necessary, 
-- because it is inherent in this function.
--
-- Parameter:
--   csSourceTable   The source table to be copied.
--   csTargetTable   The name of the destination table.
--   csTablespace    Optionally, allowed to use a defined tablespace for the target table.
--
-- Return:
--   STB$OERROR$RECORD   
--
-- Sample call(s): 
   /*****
     declare
       oErrorObj STB$OERROR$RECORD := STB$OERROR$RECORD(tloMessageList => ISR$MESSAGE$LIST()); 
     begin
       -- sample call #1
       oErrorObj := isr$debug.createDebugTable('TMP$SB$VERSION','DBG$SB$VERSION');
       -- sample call #2 
       oErrorObj := isr$debug.createDebugTable( csSourceTable => 'TMP$SB$VERSION',
                                                csTargetTable => 'DBG$SB$VERSION_BKUP',
                                                csTablespace  => 'ISR_TABLE');
     end;
     / 
   *****/
-- ***************************************************************************************************

end isr$debug;