CREATE OR REPLACE PACKAGE Stb$audit
is
-- ***************************************************************************************************
-- Description/Usage:
-- The package holds all functions that deal with creation and storage of an audot 
-- iStudyReporter knows several types of audits,i.e . user- , groups-, report- or report type audits.
-- ***************************************************************************************************

FUNCTION CompareObjects (oOldSelection in ISR$TLRSELECTION$LIST,oNewSelection in ISR$TLRSELECTION$LIST,sModifiedFlag OUT VARCHAR2, sEntity in VARCHAR2) RETURN STB$OERROR$RECORD;
-- ***************************************************************************************************
-- Date and Autor:31 Jan 2006 JB
-- function compares two object lists, one contains the old criteria values for one entity and report id.
-- The other list is filled with the modified values after the values where changed in report wizard
-- The function writes the difference : the added or deleted or reordered values into a
-- xml dom.
--
-- Parameter :
-- oOldSelection       object list containing the old values  before modification
-- oNewSelection       object list holding the modified values
-- sModifiedFlag       flag to indicate if there have been changes
-- sEntity             the entity of the selection screen
--
-- Return:
-- STB$OERROR$RECORD    
--
-- ***************************************************************************************************

FUNCTION CreateNewAuditRecord(sReference in VARCHAR2, csAuditType in VARCHAR2) RETURN STB$OERROR$RECORD;
-- ***************************************************************************************************
-- Date and Autor:  31 Jan 2006 JB
-- function creates a new empty audit record in the table stb$audittrail.
--
-- Parameter :
-- sReference          the identifier of the audit record
-- csAuditType         the type of the audit i.e SYSTEMAUDIT ,USERAUDIT etc.
--
-- Return:
-- STB$OERROR$RECORD    
--
-- ***************************************************************************************************

FUNCTION checkAuditExists(sAuditExists OUT VARCHAR2,sReference in VARCHAR2,sType in VARCHAR2) RETURN STB$OERROR$RECORD;
-- ***************************************************************************************************
-- Date and Autor: 31 Jan 2006 JB
-- function checks if an audit file for an audit type and the given reference already exists,
--
-- Parameter:
-- sType            the audit type (REPORTAUDIT; SYSTEMAUDIT etc.)
-- sReference       the identifier of the  audit record within the audittype
-- sAuditExists     flag to indicate if an audit already exists
--
-- Return:
-- STB$OERROR$RECORD    
--
-- ***************************************************************************************************

FUNCTION openAudit(sAuditType in VARCHAR2,sReference in VARCHAR2) RETURN STB$OERROR$RECORD;
-- ***************************************************************************************************
-- Date and Autor: 31 Jan 2006 JB
-- function checks by calling the function "checkAuditExists" ,if an auidit exists.
-- if yes it opens the file into a xml dom and initalizes the global values in the package
-- to enter the next record of the audit file
--
-- Parameter:
-- sAuditType       the audit type (REPORTAUDIT; SYSTEMAUDIT etc.)
-- sReference       the identifier of the  audit record within the audit type
--
-- Return:
-- STB$OERROR$RECORD    
--
-- ***************************************************************************************************

FUNCTION AddAuditRecord(sTransaction in VARCHAR2, csDescription in VARCHAR2 default null)  RETURN STB$OERROR$RECORD;
-- ***************************************************************************************************
-- Date and Autor: 31 Jan 2006 JB
-- function creates a new record set with the new values of the audit in the xml dom element
--
-- Parameter:
-- sTransaction       the kind of the audit entry i.e change, add.. ,remove..
-- sReference         can be set for certain audits to specify the changes in detail
--
-- Return:
-- STB$OERROR$RECORD    
--
-- ***************************************************************************************************

FUNCTION AddAuditEntry(sParametername in VARCHAR2, sOldValue in VARCHAR2, sNewValue in VARCHAR2, nModified in NUMBER) RETURN STB$OERROR$RECORD;
-- ***************************************************************************************************
-- Date and Autor: 31 Jan 2006 JB
-- function creates a new entry in the audit record of the modified parameters
--
-- Parameter:
-- sParametername      the parameter name of the modified value
-- sOldValue           the new value
-- sNewValue           the old value
-- nModified           the general counter to locate  the change
--
-- Return:
-- STB$OERROR$RECORD   
--
-- ***************************************************************************************************
function AddAuditAdditionalElement(sElementName in varchar2, sElementValue in varchar2)  return STB$OERROR$RECORD;

--**********************************************************************************************************************
FUNCTION AddVersionToSrAudit(sVersion in VARCHAR2, sAction in VARCHAR2)  RETURN STB$OERROR$RECORD;
-- ***************************************************************************************************
-- Date and Autor: 31 Jan 2006 JB
-- function adds an "audit" entry to the stability number modification history record
--
-- Parameter:
-- sVersion          the  current version of the sr number
-- sAction           the action being executed last on the report version
--
-- Return:
-- STB$OERROR$RECORD    
--
-- ***************************************************************************************************

FUNCTION createCheckSum( cnAuditID in NUMBER,nCheckSum OUT NUMBER) RETURN STB$OERROR$RECORD;
-- ***************************************************************************************************
-- Datum und Autor: 02 Feb 2006  JB
-- function calculates the checksum of an auditfile
--
-- Parameter:
-- cnAuditID              the unique identifier of the audit record
-- nCheckSum              the calculated checksum
--
-- Return:
-- STB$OERROR$RECORD    
--
-- ***************************************************************************************************

FUNCTION GetAuditIntoDom RETURN STB$OERROR$RECORD;
-- ***************************************************************************************************
-- Date and Autor: 02 Feb 2006  JB
-- function reads an existing audit file into an xml dom
--
-- Return:
-- STB$OERROR$RECORD    error object with the fields (sErrorCode, sErrorMessage, sSeverityCode)
--
-- Error behaviour/ particularities:
--
-- ***************************************************************************************************

FUNCTION DomtoAudit RETURN STB$OERROR$RECORD;
-- ***************************************************************************************************
-- Date and Autor: 02 Feb 2006  JB
-- function turns an xml dom in clob and writes the lob in the audittrail
--
-- Return:
-- STB$OERROR$RECORD    
--
-- ***************************************************************************************************

FUNCTION putAuditMask(olParameter in STB$PROPERTY$LIST) RETURN STB$OERROR$RECORD;
-- ***************************************************************************************************
-- Date und Autor: 02 Feb 2006  JB
-- function writes the audit reason entered in the audit window into the audittrail 
--
-- Parameter:
-- olNodeList      the audit window 
--
-- Return:
-- STB$OERROR$RECORD    
--
-- ***************************************************************************************************

FUNCTION silentAudit RETURN STB$OERROR$RECORD;
-- ***************************************************************************************************
-- Date and Autor: 02 Feb 2006  JB
-- function writes a silent audit and calls the function to calculates the checksum of the audit file
--
-- Return:
-- STB$OERROR$RECORD   
--
-- ***************************************************************************************************

FUNCTION getAuditMask(olNodeList OUT STB$TREENODELIST)  RETURN STB$OERROR$RECORD;
-- ***************************************************************************************************
-- Date und Autor: 02 Feb 2006  JB
-- function retrieves a audit window 
--
-- Parameter:
-- olNodeList      the audit window 
--
-- Return:
-- STB$OERROR$RECORD    
--
-- ***************************************************************************************************

FUNCTION getModifiedFlag RETURN VARCHAR2 ;
-- ***************************************************************************************************
-- Date und Autor: 02 Feb 2006  JB
-- function returns modified flag, if there are differences between the old and the new value sets 
--
-- Return
-- sModified      flag to indicate if there have been changes that have to be added into the audit file
--
-- ***************************************************************************************************

FUNCTION FreeAuditDom RETURN STB$OERROR$RECORD;
-- ***************************************************************************************************
-- Date und Autor: 02 Feb 2006  JB
-- function frees a xml dom in the memory and resets the global variables in the package
--
-- Return:
-- STB$OERROR$RECORD    
--

-- ***************************************************************************************************

FUNCTION getAuditDomId RETURN XMLTYPE;
-- ***************************************************************************************************
-- Date und Autor: 02 Feb 2006  JB
-- function returns the xmldom id of the current audit file
--
-- Return:
-- the xml dom id of the audit
--

-- ***************************************************************************************************

FUNCTION getAudit(csAuditType in VARCHAR2, csReference in VARCHAR2) return XMLTYPE;
-- ***************************************************************************************************
-- Date und Autor: 05. Mar 2010, MCD
-- gets the audit for an audittype and a reference in xml 
--
-- Parameter:
-- csAuditType        audittype
-- csReference        reference
--  
--
-- ***************************************************************************************************

PROCEDURE setcurrentxml (xXml IN XMLTYPE);
-- ***************************************************************************************************
-- Date und Autor: 16. May 2011, MCD
-- set the current xml  
--
-- Parameter:
-- xXml     the xml to set
--  
--
-- ***************************************************************************************************

END Stb$audit;