CREATE OR REPLACE PACKAGE ISR$LOADER
as

-- ****************************************************************************************************
-- Description/Usage:
-- the package ISR$LOADER contains the Java Wrappers for the loader
-- ****************************************************************************************************

FUNCTION ping(sHost in VARCHAR2, sPort IN VARCHAR2, sContext IN VARCHAR2, sNamespace IN VARCHAR2, nTimeout IN NUMBER) RETURN VARCHAR2;

FUNCTION sendJobList(sHost IN VARCHAR2, sPort IN VARCHAR2, sContext IN VARCHAR2, sNamespace IN VARCHAR2, blJobList IN BLOB, nTimeout IN NUMBER) RETURN VARCHAR2;

FUNCTION startJob(sHost in VARCHAR2, sPort IN VARCHAR2, sContext IN VARCHAR2, sNamespace IN VARCHAR2, nJobID IN NUMBER, sFolderName IN VARCHAR2, sReactivate IN VARCHAR2, nTimeout IN NUMBER) RETURN VARCHAR2;

FUNCTION terminateJob(sHost in VARCHAR2, sPort IN VARCHAR2, sContext IN VARCHAR2, sNamespace IN VARCHAR2, nJobID IN NUMBER) RETURN VARCHAR2;

function showTerminateJobSuccess(sHost in varchar2, sPort in varchar2, sContext in varchar2, sNamespace in varchar2, nJobID in number, nTimeout IN NUMBER) return varchar2;

function showTerminateJobSuccess(sHost in varchar2, sPort in varchar2, sContext in varchar2, sNamespace in varchar2, nJobID in number) return varchar2;

function showTerminateJobWarn(sHost in varchar2, sPort in varchar2, sContext in varchar2, sNamespace in varchar2, nJobID in number) return varchar2;

FUNCTION showJobError(sHost IN VARCHAR2, sPort IN VARCHAR2, sContext IN VARCHAR2, sNamespace IN VARCHAR2, nJobID IN NUMBER, sErrorMessage in VARCHAR2, nTimeout IN NUMBER) RETURN VARCHAR2;

FUNCTION showJobProgress(sHost IN VARCHAR2, sPort IN VARCHAR2, sContext IN VARCHAR2, sNamespace IN VARCHAR2, nJobID IN NUMBER, sText in VARCHAR2, nProgress in NUMBER, nTimeout IN NUMBER) RETURN VARCHAR2;

FUNCTION startApp(sHost IN VARCHAR2, sPort IN VARCHAR2, sContext IN VARCHAR2, sNamespace IN VARCHAR2, nJobID IN NUMBER, nTimeout IN NUMBER) RETURN CLOB;

FUNCTION loadFileToLoader(sHost in VARCHAR2, sPort IN VARCHAR2, sContext IN VARCHAR2, sNamespace IN VARCHAR2, nJobID IN NUMBER,  sFileName in VARCHAR2, sConfigName in VARCHAR2, sDocType in VARCHAR2,sLogLevel in varchar2, sLogReference in varchar2, nTimeout IN NUMBER) RETURN CLOB;

FUNCTION loadFileFromLoader(sHost IN VARCHAR2, sPort IN VARCHAR2, sContext IN VARCHAR2, sNamespace IN VARCHAR2, nJobID IN NUMBER, sFileName in VARCHAR2, sConfigName in VARCHAR2, sDocType in VARCHAR2, sLogLevel in varchar2, sLogReference in varchar2, nTimeout IN NUMBER) RETURN VARCHAR2;

FUNCTION loadLoaderDir(sHost IN VARCHAR2, sPort IN VARCHAR2, sContext IN VARCHAR2, sNamespace IN VARCHAR2, nJobID IN NUMBER, sConfigName in VARCHAR2, sLogLevel in varchar2, sLogReference in varchar2, nTimeout IN NUMBER) RETURN VARCHAR2;

FUNCTION getFilesNamesInDirectory(sHost IN VARCHAR2, sPort IN VARCHAR2, sContext IN VARCHAR2, sNamespace IN VARCHAR2, nJobID IN NUMBER, nTimeout IN NUMBER) RETURN VARCHAR2;

FUNCTION updateTree(sHost in VARCHAR2, sPort IN VARCHAR2, sContext IN VARCHAR2, sNamespace IN VARCHAR2, nTimeout IN NUMBER, sNodeType IN VARCHAR2, sNodeId IN VARCHAR2, sSide IN VARCHAR2) RETURN VARCHAR2;

FUNCTION updateRibbon(sHost in VARCHAR2, sPort IN VARCHAR2, sContext IN VARCHAR2, sNamespace IN VARCHAR2, nTimeout IN NUMBER) RETURN VARCHAR2;

FUNCTION sendJobList(sHost in VARCHAR2, sPort in VARCHAR2) RETURN VARCHAR2;

FUNCTION sendErrorObject(sHost IN VARCHAR2, sPort IN VARCHAR2, sContext IN VARCHAR2, sNamespace IN VARCHAR2, sErrorMessage in CLOB, nJobID IN NUMBER, nTimeout IN NUMBER) RETURN CLOB;

procedure sendErrorObjToLoader(sHost in varchar2, sPort in varchar2, oErrorObj in STB$OERROR$RECORD, cnJobID IN NUMBER default null);

function sendDbToken(sHost IN VARCHAR2, sPort IN VARCHAR2, sContext IN VARCHAR2, sNamespace IN VARCHAR2, blFile1 IN BLOB, blFile2 IN BLOB, nTimeout IN NUMBER) RETURN VARCHAR2;

function sendDbToken(sHost in varchar2, sPort in varchar2) return stb$oerror$record;

function removeJobFolder(sHost in varchar2, sPort in varchar2, sContext in varchar2, sNamespace in varchar2, nJobID in number, nTimeout IN NUMBER) return varchar2;

function compareDocumentOID(sHost IN VARCHAR2, sPort IN VARCHAR2, sContext IN VARCHAR2, sNamespace IN VARCHAR2, sDocumentOID in VARCHAR2, blFile IN BLOB, nTimeout IN NUMBER) RETURN VARCHAR2;

function protectAllowOnlyRevisions(sHost IN VARCHAR2, sPort IN VARCHAR2, sContext IN VARCHAR2, sNamespace IN VARCHAR2, nJobID in number, sFileName in VARCHAR2, sPassword in VARCHAR2, blFile IN BLOB, nTimeout IN NUMBER) RETURN VARCHAR2;

END ISR$LOADER;