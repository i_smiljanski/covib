CREATE OR REPLACE PACKAGE Stb$typedef
IS

-- ***************************************************************************************************
-- Description/Usage:
-- The package holds all public types and constants
-- ***************************************************************************************************

   --exNoTokenHandle     exception;

   --  Subtype
   SUBTYPE tLine      is VARCHAR2(500);

   -- Table for strings
   TYPE tlStrings is table of VARCHAR2(2000) index by BINARY_INTEGER;

   -- OrderNumber-Record
   TYPE tOrderNumber is RECORD ( oldOrderNumber NUMBER,
                                 newOrderNumber  NUMBER,
                                 sValue          tLine,
                                 sMasterKey      tLine,
                                 sDisplay        tLine );

   TYPE tlOrderNumber is table of tOrderNumber index by BINARY_INTEGER;

   -- Type holding the information of a selection scree of a report type
   TYPE tFormInfo is RECORD ( nMaskNo         NUMBER,
                              nMaskType       NUMBER,
                              sISRPackage     Stb$typedef.tLine,
                              sHeadline1      Stb$typedef.tLine,
                              sHeadline2      Stb$typedef.tLine,
                              sHeadline3      Stb$typedef.tLine,
                              sEntity         Stb$typedef.tLine,
                              sTitle          Stb$typedef.tLine,
                              sDescription    Stb$typedef.tLine,
                              sTooltip        Stb$typedef.tLine,
                              nTotalFormsNum  number );

   -- Table of FormInfo : used in the package ISR$REPORTTYPE$PACKAGE to store the wizard information
   TYPE tlFormInfo is table of tFormInfo index by BINARY_INTEGER;

   -- Wizard constants
   cnOptionsPage         constant number := -2;     -- mask of the output options
   cnStartPage           constant number := -1;     -- start page
   cnSingleSelect        constant number := 1;      -- single section mask
   cnMultiselect         constant number := 2;      -- multi selection mask
   cnMasterDetail        constant number := 3;      -- selection mask is master detail
   cnSpecial             constant number := 4;      -- selection mask is a grid
   cnOutputOption        constant number := 5;      -- selection of the output options

   -- Job status
   cnReportSubmit        constant number := 1;      -- document creation is started
   cnReportProgress      constant number := 10;     -- document creation is running
   cnReportDone          constant number := 100;    -- document is successfully created
   cnReportAbort         constant number := 999;    -- Document creation aborted due to an error

   -- Report status
   cnNoStatus            constant number := 0;      -- no status required
   cnStatusDraft         constant number := 1;      -- Report is draft
   cnStatusInspection    constant number := 2;      -- Report is in the inspection
   cnStatusFinal         constant number := 3;      -- Report is final
   cnStatusError         constant number := -1;     -- error during create report  --bibc-593

   -- Report conditions
   cnStatusDeactive      constant number := 0;      -- Report is deactive
   cnStatusActivated     constant number := 1;      -- Report is active
   cnStatusDeleted       constant number := -1;     -- Report is deleted
   cnStatusLocked        constant number := -2;     -- Report is locked by an user

   -- Report types
   cnVariation           constant number := 1;      -- Variation of a report
   cnVersion             constant number := 2;      -- New version of a report

   -- Constants of the severity code
   cnFatal               constant number := 999;        -- Fatal Error
   cnNoError             constant number := 0;          -- keine Fehler
   cnSeverityCritical    constant number := 1;        -- critical error , programm will be cancelled
   cnSeverityWarning     constant number := 2;        -- Warning programm will be continued
   cnSeverityMessage     constant number := 3;        -- Debug
   cnMsgWarningTimeout   constant number := 4;          -- show a warning message with timeout

  -- Constants of the severity code that used for program control
   cnAudit               constant number := 5;          -- show audit window
   cnFirstForm           constant number := 6;          -- show wizard window
   cnWizardValidateWarn  constant number := 8;          -- wizard Warning error
   cnWizardValidateStop  constant number := 9;          -- wizard stop mask error

    --cnMsgCritical       constant number := 7;          -- show an error message
   --cnMaxOpenConnectionExceeded constant number := 10; -- maximum opened connections exceeded
   --cnLicenceExpired            constant number := 11; -- licence expired

   -- constants of the display types
   cnMenuDialog          constant number := 1;  -- simple Dialog
   cnMenuDetailView      constant number := 2;  -- table view in the right split window of the explorer
   cnEsigDialog          constant number := 3;    -- esig dialog
   cnOwnPasswordDialog   constant number := 4;   -- password to change own passwords
   cnMenuListDialog      constant number := 5;    --Dialog with selectable list and  two additional buttons: Add and Delete
   cnCallProcess         constant number := 6;    -- function to call different process
   
   csInternalDateFormat  constant varchar2(50) := 'dd.mm.yyyy hh24:mi:ss';

   cr                    constant varchar2(1) := chr(10);
   crlf                  constant varchar2(2) := chr(10)||chr(13);

   exCritical   exception;

END Stb$typedef;