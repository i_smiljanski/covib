CREATE OR REPLACE PACKAGE ISR$TREE$PACKAGE
  as

-- *****************************************************************************
-- Description/Usage:
-- the package ISR$TREE$PACKAGE contains the tree functionality for automatic refreshing and node selection
-- *****************************************************************************

procedure selectLeftNode(oNode IN STB$TREENODE$RECORD);
-- *****************************************************************************
-- Date and Autor: 19.Feb 2019, MCD
-- sets the node and the reload token to left
--
-- *****************************************************************************


procedure selectRightNode(oNode IN STB$TREENODE$RECORD);
-- *****************************************************************************
-- Date and Autor: 19.Feb 2019, MCD
-- sets the node and the reload token to right
--
-- *****************************************************************************


procedure loadNode(oNode in STB$TREENODE$RECORD, oOldNode in STB$TREENODE$RECORD default null, csAppend in varchar2 default 'F');
-- *****************************************************************************
-- Date and Autor: 19.Feb 2019, MCD
-- sets the reload node list
--
-- *****************************************************************************


function buildNode(csNodeType in VARCHAR2, csNodeId in VARCHAR2, oOldNode in STB$TREENODE$RECORD default STB$OBJECT.getCurrentNode) return STB$TREENODE$RECORD;
-- *****************************************************************************
-- Date and Autor: 19.Feb 2019, MCD
-- builds the node based on the oldNode, replaces nodetype, nodeid and directory
--
-- *****************************************************************************


function rebuildReportNode(cnRepid IN NUMBER, oOldNode in STB$TREENODE$RECORD default STB$OBJECT.getCurrentNode) return STB$TREENODE$RECORD;
-- *****************************************************************************
-- Date and Autor: 19.Feb 2019, MCD
-- builds the node based on the repid, replaces valuelist, title, nodeid and directory
--
-- *****************************************************************************


function buildReportNode(cnRepid IN NUMBER, oOldNode in STB$TREENODE$RECORD default STB$OBJECT.getCurrentNode, csCustomNodeBranch in varchar2 default null) return STB$TREENODE$RECORD;
-- *****************************************************************************
-- Date and Autor: 19.Feb 2019, MCD
-- builds the report node based on the repid and the optimal value list path
-- sets nodetype, nodeid, nodepackage, valuelist, title and repid
--
-- *****************************************************************************


procedure reloadPrevNodesAndSelect(cnNodes in NUMBER, csSendSyncCall in VARCHAR2 default 'T');
-- *****************************************************************************
-- Date and Autor: 19.Feb 2019, MCD
-- sets the node, node list and reload token
-- cnNodes is the difference of the current node
-- e.g. you want to show the grand parent node of the current node, cnNodes is -2
-- if sync call is activated the other frontends are informed about the this
--
-- *****************************************************************************


procedure selectCurrent(csSendSyncCall in VARCHAR2 default 'T');
-- *****************************************************************************
-- Date and Autor: 19.Feb 2019, MCD
-- sets the node, node list and reload token
-- if sync call is activated the other frontends are informed about the this
--
-- *****************************************************************************


procedure reloadPrevNodesAndSelectCurren(cnNodes in NUMBER, csSendSyncCall in VARCHAR2 default 'T');
-- *****************************************************************************
-- Date and Autor: 19.Feb 2019, MCD
-- sets the node, node list and reload token
-- cnNodes is the difference of the current node
-- e.g. you want to show the grand parent node of the current node, cnNodes is -2
-- the selected node is the current node
-- if sync call is activated the other frontends are informed about the this
--
-- *****************************************************************************


function searchThisNode(csNodeType in VARCHAR2, csNodeId in VARCHAR2, csCustomNodeBranch in VARCHAR2 default null) return STB$TREENODE$RECORD;
-- *****************************************************************************
-- Date and Autor: 19.Feb 2019, MCD
-- searchs the node in the list of tree nodes (ISR$TREE$NODE)
-- used for searching in the custom tree
--
-- *****************************************************************************


function searchSimiliarNode(csNodeType in VARCHAR2, csNodeId in VARCHAR2, csEqual in VARCHAR2 default 'F', csCustomNodeBranch in VARCHAR2 default null) return STB$TREENODE$RECORD;
-- *****************************************************************************
-- Date and Autor: 19.Feb 2019, MCD
-- searchs a similar node (equal nodetype) in the list of tree nodes (ISR$TREE$NODE)
-- used for searching in the custom tree
--
-- *****************************************************************************


function synchronizeInBackground (sToken OUT VARCHAR2
                                , olTreeReloadList OUT STB$TREENODELIST
                                , oTreeReloadNode OUT STB$TREENODE$RECORD
                                , csNodeType in VARCHAR2
                                , csNodeId in VARCHAR2
                                , csSessionId in VARCHAR2) return STB$OERROR$RECORD;
-- *****************************************************************************
-- Date and Autor: 19.Feb 2019, MCD
-- synchronizes the frontends with the new load list, the reload token and the new node
-- the sessionid contents the id of the session which triggered the synchronizing
-- in some cases it is not necessary that this session is synchronized
--
-- *****************************************************************************


procedure sendSyncCall(csNodeType in VARCHAR2 default STB$OBJECT.getCurrentNodeType, csNodeId in VARCHAR2 default STB$OBJECT.getCurrentNodeId);
-- *****************************************************************************
-- used to inform all frontends about synchronizing
-- The parameters are nodetype and nodeid of the node to synchronize.
-- All frontends which could show this node are informed with the help of the loader.
--
-- *****************************************************************************

function getCustomNodeBranch return varchar2;
-- *****************************************************************************
-- Date and Autor: 19.Feb 2019, MCD
-- return the custom node branch (CUSTOMMYNODE, CUSTOMGROUPNODE, CUSTOMGLOBALNODE)
-- from the current node's value list
-- called from ISR$REPORTTYPE$PACKAGE.putSaveDialog and ISR$CUSTOMER.putParameters (CAN_SEARCH_DOC)
--
-- *****************************************************************************

--function loadTree( oCurrentNode in STB$TREENODE$RECORD, csLeftSide in varchar2 default 'T') return STB$TREENODELIST;
-- *****************************************************************************
-- Date and Autor: 19.Feb 2019, MCD
-- not used
-- loads a node list from ISR$TREE
--
-- *****************************************************************************

procedure saveTree( olNodeList in STB$TREENODELIST, oCurrentNode in STB$TREENODE$RECORD, csLeftSide in varchar2 default 'T');
-- *****************************************************************************
-- Date and Autor: 19.Feb 2019, MCD
-- not used
-- saves a node list in ISR$TREE
-- called from STB$GAL.getNodeChilds and STB$GAL.getNodeList
-- STB$GAL.getNodeList the list is only saved if it is its first load or
-- if the right side of the explorer differs from the left side
--
-- *****************************************************************************

procedure saveTreeSessions( oCurrentNode in STB$TREENODE$RECORD, csLeftSide in varchar2, csSelected in varchar2, csLeft_As_Right in varchar2 default 'U');
-- *****************************************************************************
-- Date and Autor: 19.Feb 2019, MCD
-- saves a node in ISR$TREE$SESSIONS
-- called from STB$GAL.getNodeChilds, STB$GAL.getNodeList and STB$GAL.setCurrentNode
--
-- *****************************************************************************

function checkFirstLoad(oCurrentNode in STB$TREENODE$RECORD) return VARCHAR2;
-- *****************************************************************************
-- Date and Autor: 19.Feb 2019, MCD
-- not used
--
-- *****************************************************************************

--function checkLeftAsRight(oCurrentNode in STB$TREENODE$RECORD) return VARCHAR2;
-- *****************************************************************************
-- Date and Autor: 19.Feb 2019, MCD
-- not used
--
-- *****************************************************************************

procedure saveTreeNode( oCurrentNode in STB$TREENODE$RECORD );
-- *****************************************************************************
-- Date and Autor: 19.Feb 2019, MCD
-- saves a tree node in ISR$TREE$NODES
-- called from STB$OBJECT.setDirectory and STB$OBJECT.setNode
--
-- *****************************************************************************

procedure saveTreeNodeList(olNodeList in STB$TREENODELIST, sAsSql in Clob);
-- *****************************************************************************
-- Date and Autor: 24.Oct 2019, MCD
-- saves a tree node list in ISR$TREE$NODES
-- called from STB$UTIL.getColListMultiRow
--
-- *****************************************************************************

end ISR$TREE$PACKAGE;