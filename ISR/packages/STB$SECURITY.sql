CREATE OR REPLACE PACKAGE Stb$security
as

-- ***************************************************************************************************
-- Description/Usage:
-- The package contains all functions that handle security request within iStudyReporter
-- ***************************************************************************************************

FUNCTION checkContextRep( sParameter in VARCHAR2, sContext in VARCHAR2, sDocDefinition in VARCHAR2, sMenuAllowed OUT VARCHAR2, sLockMe in VARCHAR2) RETURN STB$OERROR$RECORD;
-- ***************************************************************************************************
-- Date and Autor: 03 Feb 2006  JB
-- functions checks if a command token is accessable on the context of the current tree node in the
-- iStudyReporter Explorer when the node belongs to a report
--
-- Parameter
-- sParameter            the command token
-- sContext              the context of the node
-- sDocDefinition        the doc definition
-- sMenuAllowed          flag if the menu entry is accessable
-- sLockMe               flag if the node is locked by the user himself
--
-- Return
-- STB$OERROR$RECORD
--
-- ***************************************************************************************************

FUNCTION checkchildrenStatus( sParameter in VARCHAR2, sContext in VARCHAR2, sDocDefinition in VARCHAR2, sMenuAllowed IN OUT VARCHAR2) RETURN STB$OERROR$RECORD;
-- ***************************************************************************************************
-- Date and Autor: 03 Feb 2006  JB
-- functions checks the status of the children nodes of a report
--
-- Parameter
-- sParameter         the command token
-- sContext           the context of the node
-- sDocDefinition     the doc definition
-- sMenuAllowed       the flag if the functionality is allowed
--
-- Return
-- STB$OERROR$RECORD
--
-- ***************************************************************************************************

FUNCTION checkLockedByMyself(cnlockSession in NUMBER, sLockedMyself OUT VARCHAR2) RETURN STB$OERROR$RECORD;
-- ***************************************************************************************************
-- Date and Autor: 03 Feb 2006  JB
-- functions checks if a report node is locked by the current user himself
--
-- Parameter
-- cnlockSession            the session id of the current user session
-- sLockedMyself            flag to indicate if the node is locked by the user himself
--
-- Return
-- STB$OERROR$RECORD
--
-- ***************************************************************************************************

FUNCTION switchRepSession(cnSessionId in NUMBER, cnReference in NUMBER) RETURN STB$OERROR$RECORD;
-- ***************************************************************************************************
-- Date and Autor: 03 Feb 2006  JB
-- functions locks all reports belonging of the current job
--
-- Parameter
-- cnSessionId            the temporary session id
-- cnReference            the report id
--
-- Return
-- STB$OERROR$RECORD
--
-- ***************************************************************************************************

FUNCTION getCurrentUser RETURN NUMBER;
-- ***************************************************************************************************
-- Date and Autor: 03 Feb 2006  JB
-- access function of the variable nCurrentUser to identify the current user
--
-- Return
-- id of the connected user
--
-- ***************************************************************************************************

FUNCTION getOracleUserName(cnUserNo in NUMBER)
  RETURN VARCHAR2
  result_cache;
-- ***************************************************************************************************
-- Date and Autor: 303 Feb 2006  JB
-- function retrieves the oracle username using the userno
--
-- Parameter
-- cnUserNo   the iStudyReporter  user no
--
-- Return
-- oracle user name
--
-- ***************************************************************************************************

FUNCTION getCurrentLanguage RETURN NUMBER;
-- ***************************************************************************************************
-- Date and Autor: 03 Feb 2006  JB
-- access function of the variable nLangId to identify the language of the front end for current user
--
-- Return
-- id of the language of the connected user
--
-- ***************************************************************************************************

PROCEDURE setCurrentLanguage(cnUserNo in NUMBER);
-- ***************************************************************************************************
-- Date and Autor: 03 Feb 2006  JB
-- procedure sets the language for the job session required for the display texts
--
-- Parameter
-- cnUserNo   the iStudyReporter userid
--
-- ***************************************************************************************************

FUNCTION getCurrentSession RETURN NUMBER;
-- ***************************************************************************************************
-- Date and Autor: 30.03.04 JB
-- access function of the variable nSessionId to identify the session for current user
--
-- Return
-- id of the session of the connected user
--
-- ***************************************************************************************************

function checkGroupRight( csParameter in varchar2, sMenuAllowed out varchar2, cnReporttypeId in number )
  return STB$OERROR$RECORD;
-- ***************************************************************************************************
-- Date and Autor: 03 Feb 2006  JB
-- function checks if the user belongs to function groups,
-- which the report privilege has been granted to
--
-- 03 Jun 2016 - this call should not be used for performance reasons
--
-- Parameter
-- csParameter          the command token of the context menu
-- sMenuAllowed         flag to identify if the functionality is selectable
--
-- Return
-- STB$OERROR$RECORD
--
-- ***************************************************************************************************

function checkRole return STB$OERROR$RECORD;
-- ***************************************************************************************************
-- Date and Autor: 03 Feb 2006  JB
-- checks if the iStudyReporter role is activated for the user
--
-- Return
-- STB$OERROR$RECORD
--
-- ***************************************************************************************************

FUNCTION getClientIp RETURN VARCHAR2;
-- ***************************************************************************************************
-- Date and Autor: 03 Feb 2006  JB
-- function returns the client ip
--
-- Return
-- ip address
--
-- ***************************************************************************************************

FUNCTION getTimeOut(nTimeOutInMinutes OUT NUMBER) RETURN STB$OERROR$RECORD;
-- ***************************************************************************************************
-- Date und Autor: 03 Feb 2006  JB
-- function retrieves the timeout of the application for the connected user
--
-- Parameter
-- sTimeInMinutes         the timeout in minutes
--
-- Return
-- STB$OERROR$RECORD
--
-- ***************************************************************************************************

FUNCTION getEsigMask(olNodeList IN OUT STB$TREENODELIST, sToken IN VARCHAR2)  RETURN STB$OERROR$RECORD;
-- ***************************************************************************************************
-- Date und Autor: 03 Feb 2006  JB
-- function retrieves the mask to enter the electronic signature window
--
-- Parameter
-- olNodeList            node object holding all fields to enter an electronic signature
--
-- Return
-- STB$OERROR$RECORD
--
-- ***************************************************************************************************

FUNCTION putEsigMask (olParameter in STB$PROPERTY$LIST, sEsigAction in VARCHAR2, sEntity in VARCHAR2, sReference in VARCHAR2, sNodeType in VARCHAR2, sSuccess in VARCHAR2 default 'T') RETURN STB$OERROR$RECORD;
-- ***************************************************************************************************
-- Date und Autor:  03 Feb 2006  JB
-- function writes the information of the electronic signature into the database
--
-- Parameter
-- olParameter           node object withall fields of the electronic signature mask
-- sEsigAction           the action for which the electronic signature is required
-- sEntity               the entity of the action
-- sReference            the reference of the action
-- sNodeType             the node type of the reference
-- sSuccess              flag to identify if the electronic signature has been terminated successfully
--
-- Return
-- STB$OERROR$RECORD
--
-- ***************************************************************************************************

FUNCTION checkEsigExists (sSrNumber IN VARCHAR2, sTitle IN VARCHAR2 DEFAULT NULL, sExists OUT VARCHAR2)
RETURN STB$OERROR$RECORD;
-- ***************************************************************************************************
-- Date und Autor: 03 Feb 2006  JB
-- checks if an electronic signature for a stability number exists
--
-- Parameter
-- sSrNumber       the stability number
-- sExists         flag to identify if an entry exists
--
-- Return
-- STB$OERROR$RECORD
--
-- ***************************************************************************************************

FUNCTION getClientPort RETURN INTEGER;
-- ***************************************************************************************************
-- Date und Autor: 03 Feb 2006  mcd
-- returns the port from the global variable nPort
--
-- Return
-- the port number
--
-- ***************************************************************************************************

PROCEDURE setClientIP (sClientIP IN VARCHAR2);

PROCEDURE setClientPort(nClientPort in INTEGER);
-- ***************************************************************************************************
-- Date und Autor: 03 Feb 2006 mcd
-- function sets the global variable nPort
--
-- Parameter
-- nClientPort the port number to set
--
-- ***************************************************************************************************

FUNCTION getCurrentGroups
  return varchar2
  result_cache;
-- ***************************************************************************************************
-- Date und Autor: 11 Jul 2008  HR
-- function retrieves the list of groups for the current user and return it as comma delimited list
-- in sGroupList
--
-- Return
-- to comma-separated list of groups
--
-- ***************************************************************************************************

PROCEDURE initUser(csIsOracleUser IN VARCHAR2);
-- ***************************************************************************************************
-- Date und Autor: 05. Mar 2010, MCD
-- initialize the user with the flag if the user is an oracle or an ldap user
--
-- Parameter:
-- csIsOracleUser   'T' or 'F'
--
-- ***************************************************************************************************

PROCEDURE initJobUser(cnUserNo IN STB$USER.USERNO%TYPE);
-- ***************************************************************************************************
-- Date und Autor: 05. Mar 2010, MCD
-- initialize the user within a job
--
-- Parameter:
-- cnUserNo     the number of the user
--
-- ***************************************************************************************************

FUNCTION checkGroupRight(csParameter IN VARCHAR2, cnReporttypeId IN NUMBER)
  RETURN VARCHAR2 ;
 -- result_cache;
-- ***************************************************************************************************
-- Date und Autor: 05. Mar 2010, MCD
-- initialize the user within a job
--
-- Parameter:
-- cnUserNo     the number of the user
--
-- ***************************************************************************************************


FUNCTION checkGroupRight(csParameter IN VARCHAR2)
  RETURN VARCHAR2;
-- ***************************************************************************************************
-- Date und Autor: 04. Jun 2016, is
-- initialize the user within a job
--
-- Parameter:
-- cnUserNo     the number of the user
--
-- ***************************************************************************************************

FUNCTION checkDatagroups(cnCurrentUser IN NUMBER, cnUserGroup IN NUMBER)
  return varchar2
  result_cache;
-- ***************************************************************************************************
-- Date und Autor: 05. Mar 2010, MCD
-- checks if the user is in the given group
--
-- Parameter:
-- cnCurrentUser    the current user
-- cnUserGroup      the group
--
-- ***************************************************************************************************

  FUNCTION getCurrentUsername RETURN VARCHAR2;
-- ***************************************************************************************************
-- Date und Autor: 28. Oct 2011, MCD
-- returns the username of the connected user LDAP/Oracle
--
--
-- ***************************************************************************************************

  FUNCTION getCurrentPassword RETURN VARCHAR2;
-- ***************************************************************************************************
-- Date und Autor: 28. Oct 2011, MCD
-- returns the password of the connected user LDAP/Oracle
--
--
-- ***************************************************************************************************

  PROCEDURE setCurrentUsername(csUsername IN VARCHAR2);
-- ***************************************************************************************************
-- Date und Autor: 28. Oct 2011, MCD
-- sets the username of the connected user LDAP/Oracle
--
-- Parameter:
-- csUsername    the current username
--
-- ***************************************************************************************************

  PROCEDURE setCurrentPassword(csPassword IN VARCHAR2);
-- ***************************************************************************************************
-- Date und Autor: 28. Oct 2011, MCD
-- sets the password of the connected user LDAP/Oracle
--
-- Parameter:
-- csPassword    the current password
--
-- ***************************************************************************************************

function isLdapUser( cnUserNo number )
  return char
  result_cache;
-- ***************************************************************************************************
-- Date und Autor: 18.Feb 2016, vs
-- Checks, if user is LDAP-User or Oracle-User, Returned is either 'T' or 'F'
--
-- Parameter:
--   cnUserNo    The user-number of the user to be checked
--
-- ***************************************************************************************************
END Stb$security;