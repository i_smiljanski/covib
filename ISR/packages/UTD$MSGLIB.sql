CREATE OR REPLACE PACKAGE UTD$MSGLIB is
-- ***************************************************************************************************
-- Description/Usage:
-- The package is used for translations of messages into other languages
-- In future it would be nice to have a function which gets a list of variables 
-- which are replaced in the message 
-- ***************************************************************************************************

function GetMsg ( csKey     varchar2, 
                  cnLang    integer, 
                  csP1      varchar2  default null,
                  csP2      varchar2  default null,
                  csP3      varchar2  default null,
                  csP4      varchar2  default null,
                  csP5      varchar2  default null ) 
  return varchar2
  result_cache;
-- *********************************************************************************************************************
-- Date and Author: 07.08.02 HR
-- retrieves message in for cnLang corresponding language from UTD$MSG table for csKey
-- if the entry is not found in the table it is inserted with empty values
-- Text replacements are supported since iSRv3.5. Therefore the paramters csIncludeText% are used 
-- for the replacement tokens %1%, %2%, %3%, %4% and %5%.
--
-- Parameter:
-- csKey         key for Message to be retieved
-- cnLang        language from the user parameters
--
-- Return:       
-- VARCHAR2      message in demandet language
--
-- *********************************************************************************************************************
end UTD$MsgLib;