CREATE OR REPLACE PACKAGE ISR$UTD$PACKAGE
as
-- ***************************************************************************************************
-- Description/Usage:
-- the package is the interface for customizing of iStudyReporter report types
-- to full fill customer requitrements. This package is named in the system parameters 
-- if no customization is needed, the base package ISR$UTD$PACKAGE will be used.
-- ***************************************************************************************************

PROCEDURE getCustomIcon (olNodeList IN OUT STB$TREENODELIST, sNodeType IN VARCHAR2 DEFAULT NULL);
-- ***************************************************************************************************
-- Date and Autor:  07.Jul 2006 jb
-- gets a icon id for the node type and node id if customized  or returns the default icon id of the base product
-- Interface to LAL 
--
-- Parameter :
-- sDefaultIcon        the default icon
-- sNodeType           the node type
-- sReference          the nodei d/reference
--
-- Return:
-- the icon id 
--
-- ***************************************************************************************************
FUNCTION getLov(csParameter in VARCHAR2, sFilter in VARCHAR2, oSelectionList OUT ISR$TLRSELECTION$LIST) RETURN STB$OERROR$RECORD;
-- ***************************************************************************************************
-- Date and Autor:  03.Feb 2006 jb
-- retrieves for a entity a list of values
-- Interface to LAL
--
-- Parameter:
-- csParameter       the entity of the list of values 
-- sFilter           a String to filter the list of values 
-- oSelectionList    the list of values
--
-- Return:
-- STB$OERROR$RECORD    
--
-- ***************************************************************************************************
FUNCTION validate(nMaskNo in NUMBER) RETURN STB$OERROR$RECORD;
-- ***************************************************************************************************
-- Date and Autor:  03.Feb 2006 mcd
-- validates the selection the selected data of a report wizard selection screen
-- if the data isn't suffcient the user receives a warning dialog or a sto pmessage and has to 
-- modify the selected data. The dialog window is transported as error object into the Frontend
-- Interface to LAL
--
-- Parameter:
-- nMaskNo           the number of the selection screen t obe validated
--
-- Return:
-- STB$OERROR$RECORD    
--
-- ***************************************************************************************************

FUNCTION createSRNumber RETURN VARCHAR2;
-- ***************************************************************************************************
-- Date and Autor:  03.Feb 2006 jb
-- retrieves the default value for the project in the save-as-dialog
-- Interface to LAL
--
-- Return:
-- the default value of the project in the save-as -dialog
--
-- ***************************************************************************************************

FUNCTION createTitle RETURN VARCHAR2;
-- ***************************************************************************************************
-- Date and Autor:  03.Feb 2006 jb
-- creates a default value of the title in the save-as-dialog
-- Interface to LAL
--
-- Return:
-- the default title in the save-as -dialog
--
-- ***************************************************************************************************

FUNCTION createDocumentName(cnRepid IN NUMBER, cnDocid IN NUMBER) RETURN VARCHAR2;
-- ***************************************************************************************************
-- Date and Autor:  05.Mar 2012, MCD
-- retrieves the default document name for a document
-- Interface to LAL
--
-- Return:
-- the default document name
--
-- ***************************************************************************************************
FUNCTION createNameForParameter(cnRepid IN NUMBER, cnDocid IN NUMBER, csNameParameter in varchar2) RETURN VARCHAR2;

FUNCTION ckeckRemoveEmptyBookmarks(cnJobId IN NUMBER, sRemoveFlag OUT VARCHAR2) RETURN STB$OERROR$RECORD;
-- ***************************************************************************************************
-- Date and Autor: 19. Feb 2009, MCD
-- checks if the wordmodul shall remove empty bookmarks. 
-- called during the document creation process
-- Interface to LAL
--
-- Parameter:
-- cnJobID           the current job id
-- sRemoveflag       the valaue T or F if empty bookmarks should be removed
--
-- Return:
-- STB$OERROR$RECORD    
--
-- ***************************************************************************************************

FUNCTION changeDataForInspectionStatus(nNewRepid IN NUMBER) RETURN STB$OERROR$RECORD;
-- ***************************************************************************************************
-- Date and Autor:  30 Jan 2006 JB
-- interface function called if a report is set into the inspection state. allows to modify sleected data
-- before the report is set into the inspection state.
-- Interface to LAL
--
-- Parameter:
-- nNewRepId            the rep id of the inspection version
--
-- Return:
-- STB$OERROR$RECORD   
--
-- ***************************************************************************************************

FUNCTION validateInspectionData(sValidateFlag OUT VARCHAR2) RETURN STB$OERROR$RECORD;
-- ***************************************************************************************************
-- Date and Autor:  03.Feb 2006 jb
-- checks if the selected criteria is valid that the the report can be set into inspection state
-- Interface to LAL
--
-- Parameter :
-- sValidateFlag     flag to identicate if the report can be released for inspection
--
-- Return:
-- STB$OERROR$RECORD    
--
-- ***************************************************************************************************

FUNCTION getDefaultValues (oSelection in OUT ISR$TLRSELECTION$LIST,csDefaultToken in VARCHAR2, csEntity in VARCHAR2) RETURN STB$OERROR$RECORD;
-- ***************************************************************************************************
-- Date and Autor:  07.Jul 2006 jb
-- retrieves the default values of a selection screen. it is possible to define string filters that are
-- used in a "like" expression or to define "logical" filters that require a function or regular expression
-- to resolve the values
-- of the data
-- Interface to LAL
--
-- Parameter :
-- oSelection         list of values belonging to a wizard entity
-- csDefaultToken    the default token of the wizard selection screen (STRING or Logical)
-- csEntity          the entity of the wizard screen
--
-- Return:
-- STB$OERROR$RECORD    
--
-- ***************************************************************************************************

FUNCTION GetGridMaskAdds(sEntity IN STB$TYPEDEF.tLine, csMasterKey IN varchar2, lAdds OUT iSR$Crit$List) RETURN STB$OERROR$RECORD;
-- ***************************************************************************************************
-- Date and Autor:  03. Nov 2008 HR
-- delivers a collection of the type iSR$Crit$List containing  entries that can be added 
-- to the data coming from the LIMS in a selection screen.
-- The entities entries are  configured in the iSR$Parameter$Values by adding the suffix '_ADD' 
-- to the entity name
-- Interface to LAL
--
-- Parameter :
-- sEntity       the grid master entity for which additional entries can be added
-- csMasterKey   the master key to be used in the returned list elements
-- lAdds         colection of type iSR$Crit$List to be filled
--
-- Return:
-- STB$OERROR$RECORD    
--
-- ***************************************************************************************************

FUNCTION GetEntityDefault(sEntity IN STB$TYPEDEF.tLine, csMasterKey IN varchar2, oRow OUT iSR$Crit$Rec) RETURN STB$OERROR$RECORD;
-- ***************************************************************************************************
-- Date and Autor:  24. Okt 2008 HR
-- is called from package iSR$Grid$Mask to get the default entry in a grid cell 
-- The values for the entry are selected from iSR$Parameter$Values using the condition
-- that entity is the value from sEntity suffixed by '_DEFAULT';
-- Interface to LAL
--
-- Parameter :
-- sEntity      the column entity of the grid
-- csMasterKey  the master key to be used in the returned object
-- oRow         a object of type iSR$Crit$Rec containing the data for the default entry in a grid cell
--
-- Return:
-- STB$OERROR$RECORD    
--
-- ***************************************************************************************************

FUNCTION getGridMaster(cnMaskNo IN NUMBER, sMasterEntity OUT STB$TYPEDEF.tLine) RETURN STB$OERROR$RECORD;
-- ***************************************************************************************************
-- Date and Autor:  24. Okt 2008 HR
-- is called from the package iSR$Grid$Mask if more than one alternatives are found 
-- for the grid master entity. This decision is made depending on the selection in a previous step 
-- in the wizard   
-- Interface to LAL
--
-- Parameter :
-- cnMaskNo      the number of the selection screen 
-- sMasterEntity the grid master entity to be used
--
-- Return:
-- STB$OERROR$RECORD    
--
-- ***************************************************************************************************

FUNCTION prepareForXML(csData IN VARCHAR2, csType IN VARCHAR2) RETURN VARCHAR2;
-- ***************************************************************************************************
-- Date and Autor:  06.Apr 2009 MCD
-- prepares the data value for the xml, this can be e.g. formatting of date and number values, 
-- rounding of number values, removing of disallowed signs ...
-- Interface to LAL 
--
-- Parameter :
-- csData   the value which should be prepared
-- csType   the data type of the value
--
-- Return:
-- the prepared value
--
-- ***************************************************************************************************

FUNCTION preparePostXML(cnJobId IN NUMBER, oParamList IN ISR$ATTRLIST$REC, xXml IN OUT XMLTYPE) RETURN STB$OERROR$RECORD;
-- ***************************************************************************************************
-- Date and Autor:  06.Apr 2009 MCD
-- prepares the generated xml, this can be e.g adding or removing nodes in the dom
-- Interface to LAL
--
-- Parameter :
-- cnJobId    the current jobid
-- oParamList the current parameter list
-- nDomId     the xml in dom format
--
-- Return:
-- STB$OERROR$RECORD
--
-- ***************************************************************************************************

FUNCTION writeAdditionalFinalDocProps(cxXml IN OUT XMLTYPE, cnRepid in NUMBER) RETURN STB$OERROR$RECORD;
-- ***************************************************************************************************
-- Date and Autor: 27.Mar 2006 jb
-- is called during the finalizing of a report in the document creation process
-- it is possible to create additional document properties in the config xml file
-- this could be for example further information of the report creation like additional electronic
-- signatures
-- Interface to LAL
--
-- Parameter:
-- cnDomID           the dom id of the config file
-- cnRepID           the current report id
--
-- Return:
-- STB$OERROR$RECORD  
--
-- ***************************************************************************************************

FUNCTION writeCustomDocProperties(cxXml IN OUT XMLTYPE, cnJobId IN NUMBER) RETURN STB$OERROR$RECORD;
-- ***************************************************************************************************
-- Date and Autor: 09. Sep 2010, MCD
-- is called for writing custom document properties
-- Interface to LAL
--
-- Parameter:
-- cnDomID           the dom id of the config file
-- cnRepID           the current report id
--
-- Return:
-- STB$OERROR$RECORD  
--
-- ***************************************************************************************************

FUNCTION SetWizardAttributes(oSelection in OUT ISR$TLRSELECTION$LIST, csEntity IN VARCHAR2, csMasterKey IN VARCHAR2 DEFAULT NULL) RETURN STB$OERROR$RECORD;
-- ***************************************************************************************************
-- Date und Autor: 30.Sep 2013, HR
-- is called for manipulation on TMP Table
-- Interface to LAL
--
-- Parameter:
-- oSelection   list of values belonging to a wizard entity
-- csEntity     the entity
-- csMasterKey  the masterkey - optional parameter needed in grids for hierarchical lists - see ISRC-764 
--
-- Return:
-- STB$OERROR$RECORD
--
-- ***************************************************************************************************


FUNCTION PostGridSelection(csEntity IN VARCHAR2) RETURN STB$OERROR$RECORD;
-- ***************************************************************************************************
-- Date und Autor: 6.Dec 2013, HR
-- is called when Grid selection is finished. Used to delete content of TMP Table
-- Interface to LAL
--
-- Parameter:
-- csEntity     the entity
--
-- Return:
-- STB$OERROR$RECORD
--
-- ***************************************************************************************************

Function InitOutputOptions RETURN stb$oerror$record;
Function ExitOutputOptions RETURN stb$oerror$record;

-- ***************************************************************************************************
-- Date und Autor: 25 Feb 2017 hr
-- init and cleanup for output options handling
-- ***************************************************************************************************

END ISR$UTD$PACKAGE;