CREATE OR REPLACE package STB$job
as
-- ***************************************************************************************************
-- Description/Usage:
-- The package holds all functionality to accomplish a job request of iStudyReporter.
-- For every kind of job request independant of the aimed output destination or action the information
-- on the job is written in the iStudyReporter jobqueue STB$JOBQUEUE. It is the counterpart of the
-- Oracle jobqueuing mechanism and contains all information on the database job. The Oracle jobqueueing
-- will be called from this package
-- The table STB$JOBTRAIL contains all files such as templates, stylesheets required to full fill
-- the job. the table stb$jobsession holds all information on the actual jobsession and the
-- socket to the client computer

   nJobId                 NUMBER;
   exNotFindAction exception;
-- ****************************************************************************************************
-- functions and procedures
-- ****************************************************************************************************
FUNCTION getJobParameter(csName IN STB$JOBPARAMETER.PARAMETERNAME%TYPE, cnJobid IN STB$JOBPARAMETER.JOBID%TYPE) RETURN STB$JOBPARAMETER.PARAMETERVALUE%TYPE;
-- ***************************************************************************************************
-- Date und Autor: 22.Sep 2006, MCD
-- retrieves the parameter value for the according job parameter name
--
-- Parameter:
-- csName          the parameter name
-- cnJobid         the jobid of the job
--
-- Return:
-- the parameter value or null if the parameter name don't exists
--
-- ***************************************************************************************************

PROCEDURE setJobParameter(csName IN STB$JOBPARAMETER.PARAMETERNAME%TYPE, csValue IN STB$JOBPARAMETER.PARAMETERVALUE%TYPE, cnJobid IN STB$JOBPARAMETER.JOBID%TYPE);
-- ***************************************************************************************************
-- Date und Autor: 26.Sep 2006, MCD
-- sets the parameter value for the according job parameter name
--
-- Parameter:
-- csName          the parameter name
-- csValue         the parameter value
--
-- ***************************************************************************************************

FUNCTION CheckJobExists(csReference in VARCHAR2,sJobExists OUT VARCHAR2) RETURN STB$OERROR$RECORD;
-- ***************************************************************************************************
-- Date and Autor: 30 Jan 2006 JB
-- checks if for a given node reference a job exists that is still running
--
-- Parameter:
-- csReference   the node reference
-- sJobExists    flag to identify if a job exists
--
-- Return:
-- STB$OERROR$RECORD 
--
-- ***************************************************************************************************

FUNCTION CheckJobExists(sJobExists OUT VARCHAR2) RETURN STB$OERROR$RECORD;
-- ***************************************************************************************************
-- Date and Autor: MCD, 27.Sep 2006
-- checks if a job exists which is still running
--
-- Parameter:
-- sJobExists    flag to identify if a job exists
--
-- Return:
-- STB$OERROR$RECORD error object containing (sErrorCode, sErrorMessage, sSeverityCode)
--
-- Error behaviour/ particularities:
-- none
-- ***************************************************************************************************

FUNCTION startJob RETURN STB$OERROR$RECORD;
-- ***************************************************************************************************
-- Date und Autor: 22.Sep 2006, MCD
-- submits the job in the oracle jobqueue
--
-- Return:
-- STB$OERROR$RECORD    
--
-- ***************************************************************************************************

FUNCTION  terminateJob(cnJobid in NUMBER) RETURN STB$OERROR$RECORD;
-- ***************************************************************************************************
-- Date and Autor: 30 Jan 2006 JB
-- sends a success message to the frontend if the jobs finishes
--
-- Parameter:
-- cnJobId    the actual jobid
--
-- Return:
-- STB$OERROR$RECORD 
--
-- ***************************************************************************************************

PROCEDURE markDBJobAsBroken(cnJobId in NUMBER, sHost IN VARCHAR2, sPort IN VARCHAR2, sErrorCode in VARCHAR2, sErrorMessage in VARCHAR2, sSeverity in VARCHAR2, removeJob in BOOLEAN DEFAULT false);
-- ***************************************************************************************************
-- Date und Autor: 30 Jan 2006 JB
-- sends an error message to the frontend if the job process fails
--
-- Parameter:
-- cnJobId        the internal job identifier of iStudyReporter
-- sHost          the host 
-- sPort          the port id
-- sErrorMessage  the error message of the job
-- sErrorCode     the error code 
-- sSeverity      the severity level of the error
--
-- ***************************************************************************************************

PROCEDURE setJobProgress(cnJobId IN NUMBER, nStatus IN NUMBER, sStatusText IN VARCHAR2, nProgress IN NUMBER);
-- ***************************************************************************************************
-- Date and Autor:  30 Jan 2006 JB
-- updates the table stb$jobqueue and adds the actual progress the job process into the table
-- using this information the loader module retrieves its display text and progress bar
--
-- Parameter :
-- nJobId           the job id of the running job
-- nStatus          the status of the job
-- sStatusText      the description text of the status of the job
-- nProgress        the procent of the job process
--

-- ***************************************************************************************************

PROCEDURE setProgress(cnValue IN NUMBER, csStatusText IN VARCHAR2, cnJobid IN NUMBER );
-- ***************************************************************************************************
-- Date und Autor: 05. Mar 2010, MCD
-- sets job progress to the given value and status text for the loader 
--
-- Parameter:
-- cnValue        a value relative to the start and end progress
-- csStatusText   the text on the progress bar
-- cnJobId        the jobid
--  
--
-- ***************************************************************************************************

PROCEDURE logError(sErrorCode IN VARCHAR2, sErrorMessage IN VARCHAR2, sSeverity IN VARCHAR2);
-- ***************************************************************************************************
-- Date und Autor: 17. May 2011, MCD
-- logs an error occurred during a job
--
-- Parameter:
-- sErrorCode      the content of the error object, code
-- sErrorMessage   the content of the error object, message
-- sSeverity       the content of the error object, severity
--  
--
-- ***************************************************************************************************

FUNCTION modifyJob(cnJobId IN NUMBER) RETURN STB$OERROR$RECORD;
-- ***************************************************************************************************
-- Date und Autor: 17. May 2011, MCD
-- modifies the parameter of a job
--
-- Parameter:
-- cnJobId    the jobid
--
-- Return:
-- STB$OERROR$RECORD
--
-- ***************************************************************************************************

PROCEDURE deleteJobs(cnJobid IN NUMBER);
-- ***************************************************************************************************
-- Date und Autor: 17. May 2011, MCD
-- deletes a job with the given jobid
--
-- Parameter:
-- cnJobId      the jobid
--
-- ***************************************************************************************************

PROCEDURE deleteJobs;
-- ***************************************************************************************************
-- Date und Autor: 17. May 2011, MCD
-- is not used
--
-- ***************************************************************************************************

FUNCTION checkLoaderClient(cnJobid IN NUMBER, csClientIp IN VARCHAR2, csClientPort IN VARCHAR2) RETURN STB$OERROR$RECORD;
-- ***************************************************************************************************
-- Date und Autor: 17. May 2011, MCD
-- checks if the own client or a rendering server client is to use
-- if the rendering server is to use the ip/port in stb$jobsession is changed 
--
-- Parameter:
-- cnJobId      the jobid
-- csClientIp   the ip of the own client
-- csClientPort the port of the own client
--
-- Return:
-- STB$OERROR$RECORD
--
-- ***************************************************************************************************

PROCEDURE setProgress(csStatusText IN VARCHAR2, cnJobid IN NUMBER );
-- ***************************************************************************************************
-- Date und Autor: 06. Aug 2012, MCD
-- sets status text for the loader
--
-- Parameter:
-- csStatusText the text for the load
-- cnJobId      the jobid
--
-- ***************************************************************************************************

PROCEDURE waitForRunningDocumentJobs(cnReporttypeId IN NUMBER, csParameter IN VARCHAR2, cnJobId IN NUMBER);
-- ***************************************************************************************************
-- Date und Autor: 06. Aug 2012, MCD
-- checks if the job has to wait 
-- when the reporttype parameter LONG_RUNNING_JOBS for the reporttype id is exceeded
--
-- Parameter:
-- cnReporttypeId the reporttype id
-- csParameter    the parameter
-- cnJobId        the jobid
--
-- ***************************************************************************************************

PROCEDURE waitForRunningISRJobs(cnJobId IN NUMBER);
-- ***************************************************************************************************
-- Date und Autor: 06. Aug 2012, MCD
-- checks if the job has to wait when the system parameter MAX_JOBS is exceeded
--
-- Parameter:
-- cnJobId      the jobid
--
-- ***************************************************************************************************

function isJobSession(cnSessionId in number) return varchar2;

FUNCTION isRunning(csJobName IN VARCHAR2) RETURN VARCHAR2;

function getJobId(cnSessionId in number default userenv('SESSIONID')) return number;
--*********************************************************************************************************
procedure removeJobFolder(cnJobid in number, csHost in varchar2, csPort in varchar2);
--*********************************************************************************************************
FUNCTION sendMail(csRecipient in varchar2,cnJobid IN NUMBER,csNotify_on in varchar2  default null) RETURN STB$OERROR$RECORD;

END STB$job;