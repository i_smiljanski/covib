CREATE OR REPLACE package isr$profiler
as
-- ***************************************************************************************************
-- Description/Usage:
--   The package ISR$PROFILER is used for module runtime profiling of iSR program code. As a basic idea,
--   all logging entries in the table ISR$LOG may be used for the creation of a runtime profile. To analyze
--   data from a different schema or imported data, it is allowed to use another input table as ISR$LOG. 
--   At least the input table needs the same structure definition. The process flow of the profiling 
--   analyzes all available log entries which belong to a program start (=begin) or a normal program
--   termination (=end). The first step (function createLogAggregation) collects all BEGIN- and END-entries 
--   and determines their respective END- or BEGIN-relatives. The result of this aggregation step is 
--   written into the global temporary table (GTT) TMP$PROFILE$AGGR. 
--   The next processing step (function createLogAnalysis) combines some logging text and computes
--   the processing time in milliseconds. Additionally it determines its own net runtime as well as
--   its position (level) within the call tree. The result of this step is written into the GTT TMP$PROFILE$CALC.
--   This table contains the final result of the process flow.
--   Varying interpretations ot the result are provided through diverse table-fuctions in this package.
--   These can be called like views and always produce one line of either HTML or XML output. The output
--   of function getXmlProfile is for a java profiling tool, which is similar to a tree-view-tool.
--   Some functionality in this package supports filtering options. Details about the respective filters
--   can be found in the respective package description.
--   Important consideration: As long as the handling of the date-format isn't unified in the iSR,
--   as well as the date format handling of the dialogs in iSR (including the time), the profiler package
--   uses its own date format! In this context, profiler dialogs require the DD.MM.YYYY format.
--
--   [ISRC-732]
--   The default output is session related. Due ot the request, that all job-related sessions shall be 
--   integrated seamlessly in the profile, some code has been adapetd on 05.Oct.2016. The input-parameter
--   csJobId was introduced and mostly, aggreagations were made without the sessionid.
--
-- Profiling Process Flow (Overview):
--   Name                  Object-Type   Process Step   Supported Filters        Purpose                Output
--   ==============================================================================================================
--   ISR$LOG               table         data source    --                       --                     --
--   createLogAggregation  function      processing     date, user, session,     aggregate source data  TMP$PROFILE$AGGR
--                                                      ipAddress, source table 
--   createLogAnalysis     function      processing     --                       aompute aggregated     TMP$PROFILE$CALC
--                                                                               data
--   getHtmlMetaProfile    function      output         --                       profile meta analysis  HTML
--   getHtmlPeakProfile    function      output         --                       profile peaks          HTML
--   getHtmlStatProfile    function      output         --                       profile statistics     HTML
--   getHtmlProfile        function      output         SID, entryid             runtime profile        HTML
--   getXmlProfile         function      output         SID                      runtime profile        XML                                   
-- 
-- Transaction control
--   Within this package there is no COMMIT statement and no dynamical DDL.
--   Thus no permanent data change is performed. All movement of data is accomplished with
--   global temporary tables (GTTs).
--
-- Related Objects 
--   TMP$PROFILE$AGGR (GTT)
--   TMP$PROFILE$CALC (GTT)
--   TMP$PROFILE$HIER (GTT)
--
-- Prerequisites:
--   The global temporary tables TMP$PROFILE$AGGR, TMP$PROFILE$CALC, TMP$PROFILE$HIER must exist.
-- ***************************************************************************************************

/*****************************************************************************************************
-----  Test-Code Session-related  -----
declare 
  oErrorObj STB$OERROR$RECORD := STB$OERROR$RECORD(tloMessageList => ISR$MESSAGE$LIST()); 
begin
  oErrorObj := isr$profiler.createLogAggregation( csSessionIdList => '6440528,6442277,6440208' );
  oErrorObj := isr$profiler.createLogAnalysis;         
  oErrorObj := isr$profiler.createLogHierarchy;
end;
/

select column_value from table(isr$profiler.getXmlHierProfile());


-----  Test-Code Job-related  -----
declare 
  oErrorObj STB$OERROR$RECORD := STB$OERROR$RECORD(tloMessageList => ISR$MESSAGE$LIST()); 
begin
  oErrorObj := isr$profiler.createLogAggregation( csSessionIdList => '6440528,6442277,6440208' );
  oErrorObj := isr$profiler.createLogAnalysis( csJobId => 10023 );         
  oErrorObj := isr$profiler.createLogHierarchy(  csJobId => 10023 ) ;
end;
/

select * from table(ISR$PROFILER.getXmlHierProfile( csJobId => 10023 ));
select * from TMP$PROFILE$AGGR (GTT);
select * from TMP$PROFILE$CALC (GTT);
select * from tmp$profile$hier order by entryid;

******************************************************************************************************/

  type xml$list is table of xmltype;
  type clob$list is table of clob;
  csIsrLogTable    constant char(7)  := 'ISR$LOG';
  cnHiNum          constant number   := (1-1/.1e41)*1e40;

function createLogAggregation( cnLowEntryid     number,
                               cnHighEntryid    number,
                               cdLowDate        varchar2,
                               cdHighDate       varchar2,
                               csOracleUserList varchar2,
                               csSessionIdList  varchar2,
                               csIpList         varchar2,                               
                               csLogTableName   varchar2 )
  return stb$oerror$record;
-- ***************************************************************************************************
-- Date und Autor: 27 Aug 2015, vs
-- Aggregates existing data from a log-table (default is ISR$LOG) and stores the aggreation results into 
-- the global temporary table TMP$PROFILE$AGGR. It is intended that this function is called from one of its 
-- overload functions only. The aggregation aims at log entries that indicate a module start (begin) or 
-- a normal termination (end). Depending on the sequence of the log-entries, relatives are assigned and
-- their entryid is stored in either column PRED_ID (for predesessor dataset) or SUCC_ID (for successor
-- dataset).
--
-- Parameters:
--   cnLowEntryid     WHERE-condition, the low border value for the ENTRYID
--   cnHighEntryid    WHERE-condition, the high border value for the ENTRYID
--   cdLowDate        WHERE-condition, the low border value for TIMESTAMP
--   cdHighDate       WHERE-condition, the high border value for TIMESTAMP
--   csOracleUserList WHERE-condition, used for filtering by ORACLEUSERNAME, e.g.:    'NAME1, NAME2, NAME5'
--   csSessionIdList  WHERE-condition, used for filtering by SESSIONID,      e.g.:    '763494, 100229'
--   csIpList         WHERE-condition, used for filtering by IP-address,     e.g.:    '10.100.200.34, 10.100.200.108'
--   csLogTableName   the name of the source table,                          e.g.:    'isr$log'
--
-- Return:
--   STB$OERROR$RECORD
-- ***************************************************************************************************  

function createLogAggregation( cnLowEntryid     number   default 0,
                               cnHighEntryid    number   default cnHiNum,
                               csOracleUserList varchar2 default null,
                               csSessionIdList  varchar2 default null,
                               csIpList         varchar2 default null,
                               csLogTableName   varchar2 default csIsrLogTable )
  return stb$oerror$record;
-- ***************************************************************************************************
-- Date und Autor: 27 Aug 2015, vs
-- Call-interface for createLogAggregation
--
-- Parameters:
--   cnLowEntryid     WHERE-condition, the low border value for the ENTRYID
--   cnHighEntryid    WHERE-condition, the high border value for the ENTRYID
--   csOracleUserList WHERE-condition, used for filtering by ORACLEUSERNAME, e.g.:    'NAME1, NAME2, NAME5'
--   csSessionIdList  WHERE-condition, used for filtering by SESSIONID,      e.g.:    '763494, 100229'
--   csIpList         WHERE-condition, used for filtering by IP-address,     e.g.:    '10.100.200.34, 10.100.200.108'
--   csLogTableName   the name of the source table,                          e.g.:    'isr$log'
--
-- Return:
--   STB$OERROR$RECORD
--
-- ***************************************************************************************************  

function createLogAggregation( cdLowDate        varchar2,
                               cdHighDate       varchar2     default trunc(sysdate)+1,
                               csOracleUserList varchar2 default null,
                               csSessionIdList  varchar2 default null,
                               csIpList         varchar2 default null,               
                               csLogTableName   varchar2 default csIsrLogTable )
  return stb$oerror$record;
-- ***************************************************************************************************
-- Date und Autor: 27 Aug 2015, vs
-- Call-interface for createLogAggregation
--
-- Parameters:
--   cdLowDate        WHERE-condition, the low border value for TIMESTAMP
--   cdHighDate       WHERE-condition, the high border value for TIMESTAMP
--   csOracleUserList WHERE-condition, used for filtering by ORACLEUSERNAME, e.g.:    'NAME1, NAME2, NAME5'
--   csSessionIdList  WHERE-condition, used for filtering by SESSIONID,      e.g.:    '763494, 100229'
--   csIpList         WHERE-condition, used for filtering by IP-address,     e.g.:    '10.100.200.34, 10.100.200.108'
--   csLogTableName   the name of the source table,                          e.g.:    'isr$log'
--
-- Return:
--   STB$OERROR$RECORD
--
-- Sample Calls
--   declare
--     oErrorObj STB$OERROR$RECORD := STB$OERROR$RECORD(tloMessageList => ISR$MESSAGE$LIST()); 
--   begin
--     -- Sample Call #1
--     oErrorObj := isr$profiler.createLogAggregation( cdLowDate =>  SYSDATE -2
--                                                     cdHighDate => SYSDATE    );
--     -- Sample Call #2
--     oErrorObj := isr$profiler.createLogAggregation( cdLowDate      => sysdate - 5
--                                                     csLogTableName => 'isr_log_test' );
--   end;
--   /
-- ***************************************************************************************************  

function createLogAnalysis( csJobId stb$jobparameter.parametervalue%type default null ) 
  return stb$oerror$record;
-- ***************************************************************************************************
-- Date und Autor: 27 Aug 2015, vs
--   This function combines some logging text and calculates the processing time in milliseconds. 
--   Additionally it determines its own net runtime as well as
--   its position (level) within the call tree. The result of this step is written into the GTT TMP$PROFILE$CALC.
--
-- Parameters:
-- csTableName   table to be checked
--
-- Return:
--   STB$OERROR$RECORD
-- ***************************************************************************************************  

-- ***************************************************************************************************
function createLogHierarchy( csJobId stb$jobparameter.parametervalue%type default null ) 
  return stb$oerror$record;
-- ***************************************************************************************************
-- Date und Autor: 24 Sept 2015, vs
--   This function prepares all data for a defined hierarchical XML output
--   The result of this step is written into the GTT TMP$PROFILE$HIER.
--
-- Parameters:
-- no parameters
--
-- Return:
--   STB$OERROR$RECORD
-- ***************************************************************************************************

function getHtmlProfile( csSidList            varchar2 default null,
                         cnLowEntryid         number   default 0,
                         cnHighEntryid        number   default cnHiNum,
                         csIncludeSuccEntryid char     default 'N' )
  return xml$list pipelined;
-- ***************************************************************************************************
-- Date und Autor: 02 Sept. 2015, vs
-- Get runtime profile as one HTML expression
--
-- Parameters:
--   csSidList             Optional list of SIDs that appear in ouput.
--   cnLowEntryid          Optional filter EntryID low border.
--   cnHighEntryid         Optional filter EntryID high border.
--   csIncludeSuccEntryid  Output-option, whether column EntryID_end shall appear in output or not.
--
-- Return:
--   table of xmltype  (one line only)
--
-- Sample Calls
--   select column_value from table(isr$profiler.getHtmlProfile( ));                                -- sample call#1
--   select column_value from table(isr$profiler.getHtmlProfile('17072715,17072717,17072720' ));    -- sample call#2
-- ***************************************************************************************************  

function getClobHierProfile( csSidList varchar2 default null,
                             csJobId   stb$jobparameter.parametervalue%type default null)
  return clob$list pipelined;
-- ***************************************************************************************************
-- Date und Autor: 31 May 2017, vs
-- Get runtime profile as a hierarchical CLOB expression.  
-- For debugging only !
-- ***************************************************************************************************

function getXmlHierProfile( csSidList varchar2 default null,
                            csJobId   stb$jobparameter.parametervalue%type default null )
  return xml$list pipelined;
-- ***************************************************************************************************
-- Date und Autor: 26 Sept. 2015, vs
-- Get runtime profile as a hierarchical XML expression.
--
-- Parameters:
--   csSidList          is not implemented
--
-- Return:
--   table of xmltype  (one line only)
--
-- Sample Call
--   select column_value from table(isr$profiler.getXmlHierProfile); 
--
-- ***************************************************************************************************  

function getHtmlMetaProfile  return xml$list pipelined;
function getHtmlPeakProfile  return xml$list pipelined;
function getHtmlStatProfile  return xml$list pipelined;
-- ***************************************************************************************************
-- Date und Autor: 02 Sept. 2015, vs
-- Get meta analysis  or  peak analysis  or statistics  as one HTML expression
--
-- Parameters:
--   none
--
-- Return:
--   table of xmltype  (one line only)
--
-- Sample Calls
--   select column_value from table(isr$profiler.getHtmlMetaProfile());
--   select column_value from table(isr$profiler.getHtmlPeakProfile);
--   select column_value from table(isr$profiler.getHtmlStatProfile);
-- ***************************************************************************************************  
  
function callFunction( oParameter in  STB$MENUENTRY$RECORD, 
                       olNodeList out STB$TREENODELIST      )  return STB$OERROR$RECORD;
-- ***************************************************************************************************
-- Date und Autor: 09 Feb. 2016, vs
-- ***************************************************************************************************

/***  vs 17.02.2016 checkDependenciesOnMask probably not required in Profler Package ?!
function checkDependenciesOnMask( oParameter    in     STB$MENUENTRY$RECORD,
                                  olParameter   in out STB$PROPERTY$LIST,
                                  sModifiedFlag out    varchar2             )  return STB$OERROR$RECORD;
***/                                  
-- ***************************************************************************************************
-- Date und Autor: 09 Feb. 2016, vs
-- ***************************************************************************************************   


function putParameters( oParameter  in STB$MENUENTRY$RECORD, 
                        olParameter in STB$PROPERTY$LIST,
                        clParameter in clob default null   )  return STB$OERROR$RECORD;
-- ***************************************************************************************************
-- Date und Autor: 09 Feb. 2016, vs
-- ***************************************************************************************************   

function setMenuAccess( sParameter   in  varchar2, 
                        sMenuAllowed out varchar2 )
  return STB$OERROR$RECORD;
-- ***************************************************************************************************
-- Date und Autor: 21 Jan. 2016, vs
-- checks if a command token is selectable in the context menu.
--
-- Parameters:
--   sParameter       the command token of the context menu
--   sMenuAllowed     flag to indicate if the context command token is selectable in the context menu.  Values: 'T' and 'F'
--
-- Return:
--   STB$OERROR$RECORD    
-- ***************************************************************************************************   

function createProfile( cnJobId in     number,
                        cxXml   in out xmltype )  return STB$OERROR$RECORD;
-- ***************************************************************************************************
-- Date und Autor: 10 Feb. 2016, vs
-- create a hierarchical XML-runtime profile for the session (FILTER_SESSIONID) belonging to the job.
-- the result is stored into table STB$JOBTRAIL.
--
-- Parameters:
--   cnJobId   the Jobid refering to the profile creation
--   cxXml     the xml configuration file
--
-- Return:
--   STB$OERROR$RECORD   
-- ***************************************************************************************************   

function getLov( csEntity        in  varchar2,   --  = ISR$DIALOG.PARAMETERNAME
                 csSuchwert      in  varchar2,   --  = ISR$DIALOG.PARAMETERVALUE
                 oSelectionList  out ISR$TLRSELECTION$LIST ) 
  return  STB$OERROR$RECORD;
-- ***************************************************************************************************
-- Date und Autor: 16 Mar. 2016, vs
-- ***************************************************************************************************

end isr$profiler;