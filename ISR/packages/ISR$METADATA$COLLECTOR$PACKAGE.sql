CREATE OR REPLACE PACKAGE ISR$METADATA$COLLECTOR$PACKAGE
  AS
-- ***************************************************************************************************
-- Description/Usage:
--
--
--
-- ***************************************************************************************************

FUNCTION getEntities RETURN CLOB;
-- ***************************************************************************************************
-- Date und Autor: 11.Nov 2011, MCD
-- return an XML file with all entities of the reporttype id set in STB$OBJECT.getCurrentReporttypeID
-- 
-- Return:
-- the XML file  
--
-- ***************************************************************************************************

FUNCTION getEntityDescription(csEntity in varchar2) RETURN CLOB;
-- ***************************************************************************************************
-- Date und Autor: 11.Nov 2011, MCD
-- returns an XML file with all attributes of an entity
-- 
-- Parameters:
-- csEntity     the entity
--
-- Return:
-- the XML file  
--
-- ***************************************************************************************************

FUNCTION getEntityRelations(csEntity in varchar2) RETURN CLOB;
-- ***************************************************************************************************
-- Date und Autor: 11.Nov 2011, MCD
-- returns an XML file with all relations of an entity
-- 
-- Parameters:
-- csEntity     the entity
--
-- Return:
-- the XML file  
--
-- ***************************************************************************************************

END ISR$METADATA$COLLECTOR$PACKAGE;