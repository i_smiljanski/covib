CREATE OR REPLACE PACKAGE ISR$CUSTOMER
as
-- ***************************************************************************************************
-- Description/Usage:
-- The package holds all functionality to create a the sub tree below the top level node "MAINCUSTOM"
-- in the custom configuration below the node "MAINCUSTOM" the user can configure their onwn view 
-- on the report data
-- ***************************************************************************************************

PROCEDURE destroyOldValues;
-- ***************************************************************************************************
-- Date and Author: 30 Jan 2006  JB
-- destroys the old values of the node object when a new node is set in the Explorer
--
-- ***************************************************************************************************
    
FUNCTION getSelectStatement(olValueList IN ISR$VALUE$LIST, sStatement OUT VARCHAR2) RETURN STB$OERROR$RECORD;
-- ***************************************************************************************************
-- Date and Autor:  21.Feb 2006, MCD
-- creates a sql select statement to retreives the report ids 
-- out of the a value list, the value list holds the "node path" of the current node in the Explorer 
-- It is checked if the repids are "valid" for the node path in the table ISR$REPORT$GROPUPING 
--
-- Parameter:
-- olValueList         the value list
-- sStatement          the created select statement
--
-- Return:
-- STB$OERROR$RECORD    
--
-- ***************************************************************************************************

FUNCTION setMenuAccess(sParameter in VARCHAR2, sMenuAllowed OUT VARCHAR2)  RETURN STB$OERROR$RECORD;
-- ***************************************************************************************************
-- Date and Autor:  15.Feb 2006, MCD
-- checks if a command token is selectable in the context menu. If a command token can be
-- selected depends on the context of the node and the activiated system privileges of the authorization
-- groups the user belongs to
--
-- Parameter:
-- sParameter            the command token of the context menu
-- sMenuAllowed          flag to indicate if the context command token is selectable in the context menu
--
-- Return:
-- STB$OERROR$RECORD    
--
-- ***************************************************************************************************

FUNCTION SetCurrentNode(oSelectedNode in STB$TREENODE$RECORD )  RETURN STB$OERROR$RECORD;
-- ***************************************************************************************************
-- Date und Autor: 02 Feb 2006  JB
-- sets the information of a cstom tree node  in the iStudyReporter. The information is
-- be written in the database package STB$OBJECT where the global context of the explorer node is stored
--
-- Parameter:
-- oSelectedNode       the selected iStudyReporter Explorer node
--
-- Return:
-- STB$OERROR$RECORD    
--
-- ***************************************************************************************************

FUNCTION GetNodeChilds(oSelectedNode in STB$TREENODE$RECORD, olChildNodes OUT STB$TREENODELIST)  RETURN STB$OERROR$RECORD;
-- ***************************************************************************************************
-- Date und Autor: 02 Feb 2006  JB
-- retrieves all the subnodes of the selected tree node in the iStudyReporter Explorer
--
-- Parameter:
-- oSelectedNode   the Explorer node for which the sub nodes  should be retrieved
-- olChildNodes    the list of children nodes
--
-- Return:
-- STB$OERROR$RECORD    
--
-- ***************************************************************************************************

FUNCTION GetNodeList(oSelectedNode in STB$TREENODE$RECORD, olNodeList OUT STB$TREENODELIST)  RETURN STB$OERROR$RECORD;
-- ***************************************************************************************************
-- Date und Autor: 02 Feb 2006  JB
-- retrieves all nodes that are  displayed as properties of the selected tree node in the 
-- right split screen of the iStudyReporter Explorer
--
-- Parameter:
-- oSelectedNode  the tree node for which the properties should be retrieved
-- olNodeList     the list of nodes to be displayed  as properties 
--
-- Return:
-- STB$OERROR$RECORD    
--
-- ***************************************************************************************************

FUNCTION getContextMenu(nMenu in NUMBER,  olMenuEntryList OUT STB$MENUENTRY$LIST) RETURN STB$OERROR$RECORD;
-- ***************************************************************************************************
-- Date und Autor: 02 Feb 2006  JB
-- returns the context menu for the selected tree node in the iStudyReporter Explorer ,
-- if a menu entry is passed into the function, the returned information will be the sub menu
-- belonging to the passed menu entry
--
-- Parameter:
-- nMenu              optional identifer of the "parent" menu entry
-- olMenuEntryList    the context menu of the current node or the submenu for the provided menu entry
--
-- Return:
-- STB$OERROR$RECORD    
--
-- ***************************************************************************************************

FUNCTION callFunction(oParameter in STB$MENUENTRY$RECORD,  olNodeList OUT STB$TREENODELIST)    RETURN STB$OERROR$RECORD;
-- ***************************************************************************************************
-- Date und Autor: 02 Feb 2006 JB
-- is the interface to process a command token selected in the context menu.
-- The function checks if the command token can be executed and calls the backend functionality
-- to process the token. If further information is required to process the command token a node object
-- is returned to display a dialog window
--
-- Parameter:
-- oParameter            the command token selected in the context menu
-- olNodeList            a node list describing a dialog winodw when further user information is required 
--
-- Return:
-- STB$OERROR$RECORD    
--
-- ***************************************************************************************************

FUNCTION getLov(sEntity in VARCHAR2, sSuchwert in VARCHAR2, oSelectionList OUT ISR$TLRSELECTION$LIST) RETURN STB$OERROR$RECORD;
-- ***************************************************************************************************
-- Date und Autor: 02 Feb 2006  JB
-- returns the list of values of an entity
--
-- Parameter:
-- sEntity            the entity to which the list of values belongs 
-- sSuchwert          the search string to filter / limit the values in the list
-- oSelectionList     the list of values  
--
-- Return:
-- STB$OERROR$RECORD    
--
-- ***************************************************************************************************


FUNCTION putParameters(oParameter in STB$MENUENTRY$RECORD, olParameter in STB$PROPERTY$LIST)  RETURN STB$OERROR$RECORD;
-- ***************************************************************************************************
-- Date und Autor: 02 Feb 2006  JB
-- is the counterpart function of "callFunction", by using "putParameter" a
-- command token is procssed from a called dialog window, 
-- the entered information (e.g. esig , filter ) will be considered when processing the command token
--
-- Parameter:
-- oParameter      the command token selected in the context menu
-- olParameter     the dialog window with all attributes
--
-- Return:
-- STB$OERROR$RECORD   
--
-- ***************************************************************************************************

FUNCTION checkDependenciesOnMask( oParameter IN STB$MENUENTRY$RECORD, olParameter IN OUT STB$PROPERTY$LIST, sModifiedFlag OUT VARCHAR2 ) RETURN STB$OERROR$RECORD;
-- ***************************************************************************************************
-- Date and Autor:  16.Jun 2006, MCD
-- checks the dependencies on the window and acts accordingly
-- e.g.: if one field in the window is selected the next field is not editable
--       or if one field in the window has a certain value the next field is not displayed 
--
-- Parameter :
-- oParameter         the  menu entry that was called 
-- olParameter        the attributes of the dialog window
-- sModifiedFlag      flag to indicate if an attribute  was modiifed
--
-- Return:
-- STB$OERROR$RECORD  
--
-- ***************************************************************************************************

FUNCTION getConditionAsSQL (nNodeId IN ISR$CUSTOM$GROUPING.NODEID%TYPE) RETURN VARCHAR2;
-- ***************************************************************************************************
-- Date and Autor:  16.May 2011, MCD
-- checks if the nodeid has a condition and return an sql statement for this condition
--
-- Parameter :
-- nNodeId         the nodeid
--
-- Return:
-- VARCHAR2 -  the sql condition 
--
-- ***************************************************************************************************

FUNCTION deleteAllSubnodes (cnNodeType IN VARCHAR2) RETURN STB$OERROR$RECORD;
-- ***************************************************************************************************
-- Date and Author: 30 Mar 2012, MCD
-- deletes all grouping nodes with subnodes 
--
-- Parameters:
-- cnNodeType the node type of the node
--
-- ***************************************************************************************************

FUNCTION loadOnlyRight(oSelectedNode in STB$TREENODE$RECORD) RETURN VARCHAR2;
-- ***************************************************************************************************
-- Date and Author: 26 May 2017, MCD
-- checks for the selected node if the node is only displayed in the rigth split
-- screen (return T) or in both screens (return F) 
--
-- Parameters:
-- oSelectedNode  the selected node
--
-- ***************************************************************************************************

end ISR$CUSTOMER; 