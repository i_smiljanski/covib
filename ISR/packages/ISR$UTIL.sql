CREATE OR REPLACE package ISR$UTIL
IS
-- ***************************************************************************************************
-- Description/Usage:
-- This package provides repeatetedly used functionality within iStudyReporter
-- ***************************************************************************************************

function chkTableExists( csTableName  in varchar2,
                         csColumnName in varchar2 default null ) return varchar2;
-- ***************************************************************************************************
-- Date und Author: 08. Mar 2015, vs
-- Checks if a Table or a Table column exists
--
-- Parameter:
-- csTableName     Table to be checked in the user's data dictionary
-- csColumnName    Column to be checked in the table in the user's data dictionary
--
-- Return          NO ERROR:  returns a null-string
--                 ERROR:     returns string 'exTableNotExists' or 'exColumnNotExists'
-- ***************************************************************************************************

procedure truncateTable( csTableName in varchar2 );
-- ***************************************************************************************************
-- Date und Author: 23. Mar 2015, vs
-- Truncates a table in an autonomous transaction
--
-- Parameter:
-- csTableName     table to be truncated
--  
-- ***************************************************************************************************

function getElapsedTime ( cnStartTimeNum  number,
                          cnStopTimeNum   number  default dbms_utility.get_time )
  return varchar2;
-- ***************************************************************************************************
-- Expects a number representing the beginning of a time measure 
-- and a number representing the end of a time measure.
-- Both values must be determined by means of DBMS_UTILITY.GET_TIME
--
-- Return:
-- a string in the output format:   DD HH:MI:SS MS
-- if DD is equal to 0 then the output format is:  HH:MI:SS MS
--
-- Sample Code:
--    IS ...
--         nStartTimeNum  NUMBER := DBMS_UTILITY.GET_TIME;
--         sElapsedTime   VARCHAR2(15);
--       ...
--    BEGIN ...
--       ... program code ...
--       -- Get elapsed time
--       sElapsedTime := STB$UTIL.getElapsedTime(nStartTimeNum);
--       -- some logging with: || 'elapsed time (DD HH:MI:SS MS) = ' || sElapsedTime
-- ***************************************************************************************************

function dsInterval$to$ms( cidtsInterval interval day to second )
  return number;
-- ***************************************************************************************************
-- Expects an interval day to second type, representing a time measure 
-- and retuns the interval in milliseconds. The result is rounded to the second decimal after the floating point.
--
-- Return:
-- number:   which represents the interval in milliseconds
--
-- Sample call:
--    select isr$util.dsInterval$to$ms(  to_dsinterval('0 00:01:12.125555')  ) from dual;   
-- ***************************************************************************************************

function ms$to$dsinterval( cnMilliseconds number )
  return interval day to second;
-- ***************************************************************************************************
-- Expects a number value, which represents a time measure in milliseconds
-- and retuns the respective interval day to second type value.
--
-- Return:
-- interval day to second
--
-- Sample call:
--    select isr$util.ms$to$dsinterval( 7212 ) from dual;
-- ***************************************************************************************************

function getDateDifferenceInSec(dFirstDate in date, dSecondDate in date default sysdate) return number;
-- ***************************************************************************************************


function isr$get_java_property (csProperty in varchar2) 
  return varchar2;

function getQuotedStringList( csInputString  varchar2,
                              csOldSeparator varchar2 default ',',
                              csNewSeparator varchar2 default ',',
                              csTrimSpace    varchar2 default 'F' )
  return varchar2;
-- ***************************************************************************************************
-- -- Date und Autor: 27 Aug 2015, vs
-- Transforms a non-quoted string list into a quoted string list
--
-- Parameter :
--   csInputString   The source string to be transformed
--   csOldSeparator  The separator char (or string) in the source string
--   csNewSeparator  The new separator char (or string) in the transformed string
--   csTrimSpace     'T' or 't' trims all spaces from the transformed string
--
-- Return:
--   varchar2
--
-- Sample calls:
--   select isr$util.getQuotedStringList('abc,defg') from dual; 
--   select isr$util.getQuotedStringList('abc;  defg  ',';', ',', 'T') from dual; 
--   select isr$util.getQuotedStringList('abc,  defg  ', csTrimSpace => 'T') from dual;     
-- ***************************************************************************************************

function getSplit( csInputString varchar2,
                   cnPosition    varchar2 default 1,
                   csSeparator   varchar2 default ',',
                   csTrim        varchar2 default 'F' )
  return varchar2;
-- ***************************************************************************************************
-- -- Date und Autor: 15 Sept 2015, vs
-- Get split value from a string. The functionality is case sensitive.
-- GetSplit now supports negative position. (vs 15.02.2016)
--
-- Parameter :
--   csInputString   The source string
--   cnPosition      The split position for string to be returned
--   csSeparator     The separator char (or string) within the source string
--   csTrim          'T' or 't' trims leading and trailing spaces from the transformed string
--
-- Return:
--   varchar2
--
-- Sample calls:
--   select '*'||isr$util.getSplit('abcD abcD abcD',2,'cD')||'*' from dual;
--   select '*'||isr$util.getSplit('abcdefghikjlabcdefghikjlabcdefghikjl',4,'k')||'*' from dual;
--   select '*'||isr$util.getSplit('abcdefghikjlabcdefghikjlabcdefghikjl',1,'cde')||'*' from dual;
--   select '*'||isr$util.getSplit('abcdefghikjlabcdefghikjlabcdefghikjl',2,'cde')||'*' from dual;
-- ***************************************************************************************************

function reverse( csInputString varchar2 )
  return varchar2;
-- ***************************************************************************************************
-- -- Date und Autor: 15 Feb. 2016, vs
-- Function returns the reverse input string
--
-- Parameter :
--   csInputString   The source string to be returned reverse
--
-- Return:
--   varchar2
-- ***************************************************************************************************

function getFullQualifiedCallName( csInputString varchar2,
                                   csCallName    varchar2,       -- filename without path and without separator sign
                                   csSeparator   varchar2 default '/' )
  return varchar2;

function initializeSession
  return STB$OERROR$RECORD;
-- ***************************************************************************************************
-- Date und Autor: 18.Jun.2019, vs
-- Function Initaializes Session Parameters [IRC-1108] 
--   including: NLS Session Parameters [ISRC-235]
--              Java Memory Settins
--              Session Loglevel
--
-- Parameter :      no parameter
--
-- Return:
--   STB$OERROR$RECORD
-- ***************************************************************************************************
   
 --*********************************************************************************************************************************
 FUNCTION getInternalDate  return varchar2 
    result_cache;

function isNumeric ( cnNumber number ) 
  return varchar2;
    
function isNumeric ( csTestString varchar2,
                     csDecimalSeparator varchar2 default '.' ) 
  return varchar2;

FUNCTION split (csList varchar2, csDelimiter varchar2 := ',') RETURN SYS.ODCIVARCHAR2LIST DETERMINISTIC;
-- ***************************************************************************************************
-- Date and Autor: 18, Jun 2014, HSP
-- splitting a string according to the specified delimiter and return a pipelined table
--
-- Parameter:
-- csList          the string to split
-- csDelimiter     the delimiter
--
-- Return
-- SYS.ODCIVARCHAR2LIST (deterministic)
--
-- ***************************************************************************************************

function splitNumbers (csList varchar2, csDelimiter varchar2 := ',', csRange varchar2 := '-') return SYS.ODCINUMBERLIST pipelined;
-- ***************************************************************************************************
-- Date and Autor: 26, Aug 2021, HSP
-- splitting a string according to the specified delimiter and range and return all values as number in a pipelined table
--
-- Parameter:
-- csList          the string to split
-- csDelimiter     the delimiter
-- csRange         the range marker
--
-- Return
-- SYS.ODCIVARCHAR2LIST (pipelined)
--
-- Example:
-- '1,2,4-7,9' returns (1, 2, 4, 5, 6, 7, 9) through a pipe
-- "runid in (select column_value from splitNumbers('1,2,4-7,9')" will match only the numbers (1, 2, 4, 5, 6, 7, 9)
--
-- ***************************************************************************************************

    
END isr$util;