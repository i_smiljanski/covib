CREATE OR REPLACE package STB$DOCKS
as
-- ***************************************************************************************************
-- Description/Usage:
-- the package STB$DOCKS holds all functionality dealing directly with the documents and their
-- storaging table stb$doctrail. All access functions to retireve document checksum and
-- passwords and is in charge of the copying of needed files into the stb$jobtrail are part of
-- the package
-- ***************************************************************************************************

PROCEDURE createChecksum(cnDocId in NUMBER, sFlagOk OUT VARCHAR2, sBlobflag in varchar2);
-- ***************************************************************************************************
-- Date and Autor:  30 Jan 2006 JB
-- procedure to calculate the checksum of a document
--
-- Parameter:
-- cnDocId      the current document
-- sFlagOk      flag to identify if the job was successful
-- sBlobflag    flag to identify if the file is a blob or clob
--
-- Return:
-- STB$OERROR$RECORD
--
-- ***************************************************************************************************

function CheckDocExistsForNodeId(csNodeId in varchar2, sExists out varchar2) return STB$OERROR$RECORD;
-- ***************************************************************************************************
-- Date and Autor:  30 Jan 2006 JB
-- function checks if a document exists for the current Explorer node
--
-- Parameter:
-- csNodeId     the current nodeid
-- sExists      flag to identify if a document already exists
--
-- Return:
-- STB$OERROR$RECORD
--
-- ***************************************************************************************************

function deleteCommon return STB$OERROR$RECORD;
-- ***************************************************************************************************
-- Date and Autor:  30 Jan 2006 JB
-- function deletes a common document (attachment) from the table STB$DOCTRAIL
--
-- Return:
-- STB$OERROR$RECORD
--
-- ***************************************************************************************************

function getMaxDocIdInSRNumber(cnRepId in number, nDocId out number, csDocType in VARCHAR2, csDocDefinition in VARCHAR2) return STB$OERROR$RECORD;
-- ***************************************************************************************************
-- Date and Autor:  30 Jan 2006 JB
-- function retrieves the maximum document id within a project, title  and document definition
-- starting from the passed report id
--
-- Parameter:
-- nDocId           the maximum document id
-- cnRepId          the current report id
-- csDocType        the document type
-- csDocDefinition  the document definition
--
-- Return:
-- STB$OERROR$RECORD
--
--- ***************************************************************************************************

function getPasswordFromDoc(csReference in varchar2, sPassword out varchar2) return STB$OERROR$RECORD;
-- ***************************************************************************************************
-- Date and Autor:  30 Jan 2006 JB
-- function gets the password for a document
--
-- Parameter:
-- csReference   the document id
-- sPassword     the password
--
-- Return:
-- STB$OERROR$RECORD
--
-- ***************************************************************************************************

procedure controlChecksum(cnDocId in number, sFlagOk out varchar2);
-- ***************************************************************************************************
-- Date and Autor:  30 Jan 2006 JB
-- procedure controls if there have been manipulations on the document, it recalculates the checksum
-- written in the table stb$doctrail
--
-- Parameter:
-- cnDocId     the document identifier
-- sFlagOk     flag to indicate if the checksum is equal to the stored value
--
-- ***************************************************************************************************

function copyClobToDoc(cnEntryid in number, csNodeType in varchar2, csNodeId in varchar2, cnUserNo in number) return STB$OERROR$RECORD;
-- ***************************************************************************************************
-- Date and Autor:  30 Jan 2006 JB
-- function copies a clob as the result of a document creation process into the table stb$doctrail
--
-- Parameter:
-- cnEntryid        the identifier of the file in the table stb$jobtrail
-- csNodeTyp        the nodeType the document belongs to
-- csNodeId         the nodeId the document belongs to
-- cnUserNo         the user of the action
--
-- Return:
-- STB$OERROR$RECORD
--
-- **************************************************************************************************

--FUNCTION copySnapshotToDoc(cnEntryid IN NUMBER,csNodeType IN VARCHAR2 , csNodeId IN VARCHAR2,
  --                      cnUserNo IN NUMBER, cnReportTypeId IN NUMBER) RETURN STB$OERROR$RECORD;


function switchCheckOutFlag(cnDocId in number, csFlag in varchar2, cnUserNo in number, csIP in varchar2 default null, csFullPath in varchar2 default null, csFileName in varchar2 default null) return STB$OERROR$RECORD;
-- ***************************************************************************************************
-- Date and Autor:  30 Jan 2006 JB
-- function switches the checked-out /checked-in flag for a document id
--
-- Parameter:
-- cnDocid          the current docid
-- csFlag           flag to indicate if the document is checked in or out
-- cnUserNo         the user who performs the action
-- csIp             the ip address of the client
-- csFullPath       the folder/path where document is saved to
-- csFileName       the name of the document
--
-- Return:
-- STB$OERROR$RECORD
--
-- ***************************************************************************************************

function lastDocCheckedIn(sCheckededOut out varchar2, nDocId out number, sPath out varchar2, sMachine out varchar2, sOracleuser out varchar2) return STB$OERROR$RECORD;
-- ***************************************************************************************************
-- Date and Autor:  30 Jan 2006 JB
-- function checks for a report that the last
-- document within the  project and title  is checked in.
-- This document will be used as master template for the new document
-- if the document is checked out a message will be displayed
--
-- Parameter:
-- sCheckedOut   flag to identify if the last document is checked in or out
-- nDocId        the document id of the last document
-- sPath         the path of the file if the document is checked-out
-- sMachine      if the client if the document is checked-out
-- sOracleUser   the oracle user who performed the check-out
--
-- Return:
-- STB$OERROR$RECORD
--
-- ***************************************************************************************************

function getDocStatus(sDocStatus out varchar2)  return STB$OERROR$RECORD;
-- ***************************************************************************************************
-- Date and Autor:  30 Jan 2006 JB
-- function retrieves the status of a document "checked-in" or "checked-out"
--
-- Parameter:
-- sDocStatus            the status of the document
--
-- Return:
-- STB$OERROR$RECORD
--
-- ***************************************************************************************************

function DocExists(sFlag out varchar2, sDocType in varchar2, sStatus out varchar2) return STB$OERROR$RECORD;
-- ***************************************************************************************************
-- Date and Autor:  30 Jan 2006 JB
-- function checks if a document already exists for a report id, if a document already exists a
-- new report version has to be created during the document creation process. If there isn't a document
-- the  document can be created below the current report id
--
-- Parameter:
-- sFlag           flag to indicate if a document exists
-- sDocType        Type can be 'C' for Common or 'D' for iStudyReproter documents
-- sStatus         flag to show if the document is checked in or out
--
-- Return:
-- STB$OERROR$RECORD
--
-- ***************************************************************************************************

function copyDocToJob(cnDocId in number, cnJobId in number, cblBlob in stb$doctrail.content%type default EMPTY_BLOB, csAsFilename in varchar2 default null) return STB$OERROR$RECORD;
-- ***************************************************************************************************
-- Date and Autor:  30 Jan 2006 JB
-- function copies a file from the table stb$doctrail into the table stb$jobtrail
-- during the document creation process
--
-- Parameter:
-- cnDocId      the document identifier of the document that will be copied
-- cnJobId      the current job id
--
-- Return:
-- STB$OERROR$RECORD
--
-- ***************************************************************************************************

function copyFileToJob(cnId in number, cnJobId in number, csBlobFlag in varchar2) return STB$OERROR$RECORD;
-- ***************************************************************************************************
-- Date and Autor:  30 Jan 2006 JB
-- function copies the files from the table isr$file into the table stb$jobtrail during the
-- document creation process
--
-- Parameter:
-- cnId             the identifier in the table ISR$FILE of files that have to be copied
-- cnJobId          the current jobid for which the files have to be copied
-- csBlobFlag       flag to identify if the file is a clob or blob
-- cnDomId          identifier of the xmldom
--
-- Return:
-- STB$OERROR$RECORD
--
-- **************************************************************************************************

function copyXMLToDoc(cnEntryid in number, csNodeType in varchar2, csNodeId in varchar2, cnUserNo in number, cnReportTypeId in number, csDoctype in varchar2) return STB$OERROR$RECORD;
-- ***************************************************************************************************
-- Date and Autor:  30 Jan 2006 JB
-- copies the raw data xml file to the table stb$doctrail
--
-- Parameter:
-- cnEntryid        the identifier of the file in the tb$jobtrail
-- csNodeTyp        the nodeType the document belongs to
-- csNodeId         the nodeId the document belongs to
-- cNuserNo         the user of the action
-- cnReportTypeid   the report type id
--
-- Return:
-- STB$OERROR$RECORD
--
-- **************************************************************************************************

function copyXMLToJob(cnDocId in number, cnJobId in number, csDocumenttype in varchar2) return STB$OERROR$RECORD;
-- ***************************************************************************************************
-- Date and Autor: 30 Jan 2006 JB
-- copies an existing raw data xml file from the table stb$doctrail into the table stb$jobtrail
--
-- Parameter;
-- cnReportId   the idenitifer of the file in the table stb$doctrail that needs to be copied
-- cnJobId      the identifier of the current job
--
-- Return:
-- STB$OERROR$RECORD
--
-- ***************************************************************************************************


function copyCommonToDoc(cnEntryid in number, csNodeType in varchar2, csNodeId in varchar2, cnUserNo in number, csFileName in varchar2, cnDocId in number, cnReportTypeId in number, csComment in varchar2, csDoctype in varchar2, csDocDefinition in varchar2) return STB$OERROR$RECORD;
-- ***************************************************************************************************
-- Date and Autor:  30 Jan 2006 JB
-- copies a common document (attachment) from the table stb$jobtrail into the table stb$doctrail
-- during the document creation process
--
-- Parameter:
-- cnEntryid         the identifier of the file in the tb$jobtrail
-- csNodeTyp         the nodeType the document belongs to
-- csNodeId          the nodeId the document belongs to
-- cNuserNo          the user of the action
-- csFileName        the filename of the common document
-- cnReportTypeId    the report type id
-- csComment         the upload or checkin comment of a common document
-- csDoctype         the document type
-- csDocDefinition   the document defintion
--
-- Return:
-- STB$OERROR$RECORD
--
-- **************************************************************************************************

function copyCommonDocs(cnOldRepid in number, cnNewRepId in number) return STB$OERROR$RECORD;
-- ***************************************************************************************************
-- Date and Autor:  30 Jan 2006 JB
-- function gets the of common document ids of a report id and calls the copying function to copy
-- all attachments to the new report id
--
-- Parameter:
-- cnOldRepid           the identifier of the report where the documents are copied from
-- cnNewRepid           the identifier of the report the documents are copied to
--
-- Return:
-- STB$OERROR$RECORD
--
-- **************************************************************************************************

function copyDocToDoc(cnDocid in number, cnRepid in number, nNewDocId out number) return STB$OERROR$RECORD;
-- ***************************************************************************************************
-- Date and Autor: 30 Jan 2006 JB
-- function creates a new entry in the document table and copies an existing document to a new report id
--
-- Parameter:
-- cnDocid          the identifier of the document in the table  stb$doctrail
-- cnRepid          the new report id where the document will be copied to
-- cnNewDocId       the new document id
--
-- Return:
-- STB$OERROR$RECORD
--
-- **************************************************************************************************

function getMaxDocIdForRepId(cnRepId in number, nDocId out number, sCheckedOut out varchar2, csDocType in VARCHAR2, csDocDefinition in VARCHAR2) return STB$OERROR$RECORD;
-- ***************************************************************************************************
-- Date and Autor:  30 Jan 2006 JB
-- function retrieves the highest document id of an iStudyReporter document and gets the flag if
-- the document is checked out
--
-- Parameter:
-- cnRepId          the report id where the document belongs to
-- cnDocId          the highest document id of the report version
-- sCheckedOut      the flag to indicate if the document is checked in or out
-- csDocType        the document type
-- csDocDefinition  the document definition
--
-- Return:
-- STB$OERROR$RECORD
--
-- **************************************************************************************************

function checkDocCheckedOutByUser return STB$OERROR$RECORD;
-- ***************************************************************************************************
-- Date and Autor:  30 Jan 2006 JB
-- function checks if the current document is checked-out by the current user
--
-- Return:
-- STB$OERROR$RECORD
--
-- **************************************************************************************************

FUNCTION updateDoctrail( cnDocid in NUMBER, cnRepId in NUMBER, cnEntryid in NUMBER, csNodeType in VARCHAR2, csAddionalParameter in VARCHAR2) RETURN STB$OERROR$RECORD;
-- ***************************************************************************************************
-- Date and Autor:  22. Jun 2006 MCD
-- function updates the table stb$doctrail with the created document
--
-- Parameter:
-- cnDocId              the document id
-- cnRepId              the report id
-- cnEntryid            entry id of the document in the table stb$jobtrail
-- csNodeType           the node type
-- csDocType            the document type
-- csAddionalParameter  the additional parameters from the table stb$jobparameter
--
-- Return:
-- STB$OERROR$RECORD
--
-- **************************************************************************************************

FUNCTION insertIntoDoctrail( cnRepId in NUMBER, csDocType in VARCHAR2, csFileName in VARCHAR2, csMimeType in VARCHAR2, cnUserId in NUMBER, cnDocId out NUMBER, cnReporttypeId in NUMBER , csDocDefinition in varchar2) RETURN STB$OERROR$RECORD;
-- ***************************************************************************************************
-- Date and Autor:  22. Jun 2006 MCD
-- function inserts a dummy record in the table stb$doctrail
--
-- Parameter:
-- cnRepId              the report id
-- csDocType            the document type
-- csFileName           the document name
-- csMimeType           the mime type of the document
-- cnUserId             the user id
-- cnDocId              the document id
-- cnReporttypeId       the report type
-- csDocDefinition      the document definition
--
-- Return:
-- STB$OERROR$RECORD
--
-- **************************************************************************************************

function getDocDefType( cnDocId in STB$DOCTRAIL.DOCID%type,
                        cnRepid in STB$REPORT.REPID%type    default null )
  return varchar2
  result_cache;
-- ***************************************************************************************************
-- Date and Autor:  22. Jun 2006 MCD
-- function retrieves the doc defintion type for a doc id
--
-- Parameter:
-- cnDocId           the doc id
-- cnRepid           the rep id
--
-- Return:
-- the doc defintion type of the current doc id
--
-- **************************************************************************************************

function getDocType( cnDocId in STB$DOCTRAIL.DOCID%type,
                     cnRepid in STB$REPORT.REPID%type    default null )
  return varchar2
  result_cache;
-- ***************************************************************************************************
-- Date and Autor:  22. Jun 2006 MCD
-- function retrieves the doc type for a doc id
--
-- Parameter:
-- cnDocId           the doc id
-- cnRepid           the rep id
--
-- Return:
-- the doc type of the current doc id
--
-- **************************************************************************************************

FUNCTION saveXMLReport(xXml IN XMLTYPE, cnJobid IN NUMBER, csFileName IN VARCHAR2) RETURN STB$OERROR$RECORD;
-- ***************************************************************************************************
-- Date and Autor:  05. May 2010, MCD
-- adds an header to an xml report, filters the data and translates the entries and saves the report
--
-- Parameter:
-- xXml       the xml report
-- cnJobid    the jobid
-- csFileName the filename
--
-- Return:
-- STB$OERROR$RECORD
--
-- **************************************************************************************************

FUNCTION getOutput (cnRepid IN STB$REPORT.REPID%TYPE, csToken IN VARCHAR2 DEFAULT 'CAN_RUN_REPORT') RETURN number;
-- ***************************************************************************************************
-- Date and Autor:  16. May 2011, MCD
-- function return the output of a repid/token
--
-- Parameter:
-- cnRepid    the repid
-- csToken    the token
--
-- Return:
-- outputid
--
-- **************************************************************************************************

FUNCTION getReportLanguage (cnRepid IN STB$REPORT.REPID%TYPE, csToken IN VARCHAR2 DEFAULT 'CAN_RUN_REPORT') RETURN number;
-- ***************************************************************************************************
-- Date and Autor:  30. Apr 2013, IS
-- function return the report language
--
-- Parameter:
-- cnRepid    the repid
-- csToken    the token
--
-- Return:
-- langid
--
-- **************************************************************************************************

FUNCTION getTemplate (cnRepid IN STB$REPORT.REPID%TYPE) RETURN number;
-- ***************************************************************************************************
-- Date and Autor:  16. May 2011, MCD
-- function return the template of a repid
--
-- Parameter:
-- cnRepid    the repid
--
-- Return:
-- fileid
--
-- **************************************************************************************************

FUNCTION getDocVersionDisplayed(cnDocid IN STB$DOCTRAIL.DOCID%TYPE) RETURN VARCHAR2;

FUNCTION getDateFormat(csToken IN ISR$OVERVIEW$REPORTS.TOKEN%TYPE DEFAULT NULL) RETURN VARCHAR2;

function getDocNodeType(csDoctype in varchar2) return varchar2 result_cache;

--****************************************************************************************************************************
procedure copyDocProperties( cnDocId    in STB$DOCTRAIL.DOCID%TYPE, cnOldDocId in STB$DOCTRAIL.DOCID%TYPE);

end STB$DOCKS;