CREATE OR REPLACE PACKAGE isr$contextmenu$report AS

--******************************************************************************
function setMenuAccess( sparameter   in varchar2, sMenuAllowed out varchar2 ) return stb$oerror$record;
--******************************************************************************
function callFunction( oParameter IN STB$MENUENTRY$RECORD, olNodeList OUT STB$TREENODELIST  ) return stb$oerror$record;
--******************************************************************************
FUNCTION putParameters( oParameter  IN STB$MENUENTRY$RECORD, olParameter IN STB$PROPERTY$LIST ) RETURN stb$oerror$record;
--******************************************************************************
FUNCTION getLov( csParameter        in  varchar2,   sSuchwert      in  varchar2, oSelectionList out ISR$TLRSELECTION$LIST )  RETURN STB$OERROR$RECORD;
--******************************************************************************

FUNCTION checkDependenciesOnMask( oParameter IN STB$MENUENTRY$RECORD, olParameter IN OUT STB$PROPERTY$LIST, sModifiedFlag OUT VARCHAR2 ) RETURN STB$OERROR$RECORD;
-- ***************************************************************************************************

END isr$contextmenu$report;