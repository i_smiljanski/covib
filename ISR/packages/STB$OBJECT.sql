CREATE OR REPLACE PACKAGE Stb$object
is

-- ***************************************************************************************************
-- Description/Usage:
-- an iStudyReporter object is a tree node that holds all information of the selected tree node.
-- the object describes the state of the tree node. the number of variables that set with the information
-- of the tree node.
-- The values describing a node can vary between the report types.
-- All functions dealing with these parameters are bundeled in this package. The main nodes are set
-- within the STB$GAL the gui interface.
-- The report nodes are initalised in package of the report type or the custom tree node packages.
-- ***************************************************************************************************

FUNCTION GetCurrentToken RETURN VARCHAR2;
-- ***************************************************************************************************
-- Date and Autor: 09 Dec 2009 MCD
-- access function for the variable sCurrentToken to retrieve the current chosen token
--
-- Return:
-- the token
--
-- ***************************************************************************************************

PROCEDURE destroyOldValues;
-- ***************************************************************************************************
-- Date and Autor: 31 Jan 2006 JB
-- function resets the object parameters  of the last call "setCurrentNode"
--
-- ***************************************************************************************************

 FUNCTION GetCurrentSrNumber RETURN VARCHAR2;
-- ***************************************************************************************************
-- Date and Autor: 31 Jan 2006 JB
-- access function for the variable sCurrentSrNumber
--
-- Return:
-- the sr number
--
-- ***************************************************************************************************

FUNCTION GetCurrentTitle RETURN VARCHAR2;
-- ***************************************************************************************************
-- Date and Autor: 31 Jan 2006 JB
-- access function for the variable sCurrentTitle
--
-- Return:
-- the report title
--
-- ***************************************************************************************************

FUNCTION GetCurrentVersion RETURN VARCHAR2;
-- ***************************************************************************************************
-- Date and Autor: 31 Jan 2006 JB
-- access function for the variable sCurrentVersion
--
-- Return:
-- the report version
--
-- ***************************************************************************************************

FUNCTION GetCurrentRepStatus RETURN NUMBER;
-- ***************************************************************************************************
-- Date and Autor: 31 Jan 2006 JB
-- access function for the variable nCurrentRepStatus
--
-- Return:
-- the report status
--
-- ***************************************************************************************************

FUNCTION GetCurrentRepCondition RETURN  NUMBER;
-- ***************************************************************************************************
-- Date and Autor: 31 Jan 2006 JB
-- access function for the variable nCurrentRepCondition
--
-- Return:
-- the report condition
--
-- ***************************************************************************************************

FUNCTION GetCurrentDocStatus RETURN VARCHAR2;
-- ***************************************************************************************************
-- Date and Autor: 31 Jan 2006 JB
-- access function for the variable nCurrentDocStatus
--
-- Return:
-- the document status
--
-- ***************************************************************************************************

FUNCTION GetCurrentRepId RETURN NUMBER;
-- ***************************************************************************************************
-- Date and Autor: 31 Jan 2006 JB
-- access function for the variable nCurrentReportId to retrieve the id of the report
--
-- Return:
-- the current reportid
--
-- ***************************************************************************************************

FUNCTION GetCurrentTemplate RETURN NUMBER;
-- ***************************************************************************************************
-- Date and Autor: 03 Nov 2014 MCD
-- access function for the variable nCurrentTemplate to retrieve the template of the report
--
-- Return:
-- the current template
--
-- ***************************************************************************************************

FUNCTION GetCurrentReportTypeId RETURN NUMBER;
-- ***************************************************************************************************
-- Date and Autor: 31 Jan 2006 JB
-- access function for the variable nCurrentReportTypeId to retrieve the id of the report type
--
-- Return:
-- the actual report type id
--
-- ***************************************************************************************************

FUNCTION GetCurrentDocId RETURN  NUMBER;
-- ***************************************************************************************************
-- Date and Autor: 31 Jan 2006 JB
-- access function on the variable nCurrentDocId to retrieve the actual document id
--
-- Return:
-- the actual document id
--
-- ***************************************************************************************************

FUNCTION GetCurrentReportLanguage RETURN  NUMBER;
-- ***************************************************************************************************
-- Date and Autor: 29 Apr 2013 IS
-- access function on the variable nCurrentRepLangID to retrieve the actual report language id
--
-- Return:
-- the actual report language id
--
-- ***************************************************************************************************

FUNCTION GetCurrentMaskType RETURN  NUMBER;
-- ***************************************************************************************************
-- Date and Autor: 26 Sept 2013 IS
-- access function on the variable nCurrentMasktype to retrieve the actual wizard mask type
--
-- Return:
-- the actual wisard mask type
--
-- ***************************************************************************************************

PROCEDURE SetCurrentReportLanguage(cnLangId in NUMBER);
-- ***************************************************************************************************
-- Date and Autor: 29 Apr 2013 IS
-- procedure sets the current rreport language id
--
-- Parameter:
-- cnLangId          the current report language id
--
-- ***************************************************************************************************

PROCEDURE SetCurrentToken(csToken in VARCHAR2);
-- ***************************************************************************************************
-- Date and Autor: 09.Dec 2009, MCD
-- procedure sets the current token
--
-- Parameter:
-- csPackage        the name of the current token
--
-- ***************************************************************************************************

PROCEDURE SetCurrentRepTypePackage(csPackage in VARCHAR2);
-- ***************************************************************************************************
-- Date and Autor: 31 Jan 2006 JB
-- procedure sets the current report type package
--
-- Parameter:
-- csPackage        the name of the current report type package
--
-- ***************************************************************************************************

PROCEDURE SetCurrentSrNumber(csSrNumber in VARCHAR2);
-- ***************************************************************************************************
-- Date and Autor: 31 Jan 2006 JB
-- procedure sets the current sr number
--
-- Parameter:
-- csSrNumber         the sr number
--
-- ***************************************************************************************************

PROCEDURE SetCurrentTitle(csTitle in VARCHAR2);
-- ***************************************************************************************************
-- Date and Autor: 03. Dec 2009, MCD
-- procedure sets the current titlte
--
-- Parameter:
-- csTitle         the title
--
-- ***************************************************************************************************

PROCEDURE SetCurrentTemplate(cnTemplate in NUMBER);
-- ***************************************************************************************************
-- Date and Autor: 03 Nov 2014 MCD
-- procedure sets the current template
--
-- Parameter:
-- cnTemplate          the current template
--
-- ***************************************************************************************************

PROCEDURE SetCurrentRepId(cnRepId in NUMBER);
-- ***************************************************************************************************
-- Date and Autor: 31 Jan 2006 JB
-- procedure sets the current report
--
-- Parameter:
-- cnRepId          the current report id
--
-- ***************************************************************************************************

PROCEDURE SetCurrentReportTypeId(cnReportTypeId in NUMBER);
-- ***************************************************************************************************
-- Date and Autor: 31 Jan 2006 JB
-- procedure sets the current report-type-id
--
-- Parameter:
-- cnReportTypeId    the current report type
--
-- ***************************************************************************************************

PROCEDURE SetCurrentMasktype(cnMasktype in NUMBER);
-- ***************************************************************************************************
-- Date and Autor: 26. Sept 2013 IS
-- procedure sets the current wizard mask type
--
-- Parameter:
-- cnMasktype    the current mask type
--
-- ***************************************************************************************************

FUNCTION initRepType(nRepTypeId in NUMBER, nRepId in NUMBER := null) RETURN STB$OERROR$RECORD;
-- ***************************************************************************************************
-- Date and Autor: 31 Jan 2006 JB
-- function initalizes the report type and report id of the object
-- it simulates a call from the frontend , function is called during the document creation process
-- to reinitalize a istudyreporter session
--
-- Parameter:
-- nRepTypeId              the report type id
-- nRepId                  the report id can be null
--
-- Returnwert:
-- STB$OERROR$RECORD    
--
-- ***************************************************************************************************

PROCEDURE SetCurrentDocId(cnDocId in NUMBER);
-- ***************************************************************************************************
-- Date and Autor: 31 Jan 2006 JB
-- procedure sets the actual document id
--
-- Parameter:
-- cnDocId              the actual document id
--
-- ***************************************************************************************************

PROCEDURE setCurrentNode(oCurNode in STB$TREENODE$RECORD) ;
-- ***************************************************************************************************
-- Date and Autor:  31 Jan 2006 JB
-- procedure sets the global current node object
--
-- Parameter:
-- oCurNode      the object holding the current tree node
--
-- ***************************************************************************************************

FUNCTION getCurrentNode RETURN  STB$TREENODE$RECORD;
-- ***************************************************************************************************
-- Date and Autor:  31 Jan 2006 JB
-- function returns the current node
--
-- Return:
-- the object of the current tree node
--
-- ***************************************************************************************************

function getCurrentNodePackage( csNodeType in varchar2, 
                                csToken    in varchar2  ) return  varchar2 result_cache;
-- ***************************************************************************************************
-- Date and Autor:  21 Jan 2016 vs
--   function returns the current node package defined by NODETYPE and TOKEN
--
-- Return:
--   the package handling all calls of the current node

FUNCTION getCurrentNodePackage(nDelta IN NUMBER DEFAULT 0) RETURN  VARCHAR2;
-- ***************************************************************************************************
-- Date and Autor:  31 Jan 2006 JB
-- function returns the current node package set in the object
--
-- Return:
-- the package handling all calls of the current node
--
-- ***************************************************************************************************

FUNCTION getCurrentNodeType(nDelta IN NUMBER DEFAULT 0) RETURN  VARCHAR2;
-- ***************************************************************************************************
-- Date and Autor:  31 Jan 2006 JB
-- function returns the current node type set in the object
--
-- Return:
-- the current node type
--
-- ***************************************************************************************************

FUNCTION getCurrentNodeId(nDelta IN NUMBER DEFAULT 0) RETURN  VARCHAR2;
-- ***************************************************************************************************
-- Date and Autor:  31 Jan 2006 JB
-- function returns the current node id set in the object
--
-- Return:
-- the current node id
--
-- *************************************************************************************************

FUNCTION getNodeIdForNodeType(sNodeType IN VARCHAR2, oNode IN STB$TREENODE$RECORD DEFAULT NULL) RETURN VARCHAR2;
-- ***************************************************************************************************
-- Date and Autor:  05 Jan 2009 MCD
-- function returns the current node id for the nodetype
--
-- Return:
-- the current node id
--
-- *************************************************************************************************

PROCEDURE setReloadTreeList(olReload in STB$TREENODELIST);
-- ***************************************************************************************************
-- Date and Autor:  31 Jan 2006 JB
-- procedure sets the tree node object list where the tree has to be reloaded from 
-- during the synchronize process with the frontend
--
-- Parameter:
-- olReload       the object list  holding the tree node List from where it has to be reloaded
--
-- *************************************************************************************************

FUNCTION getReloadTreeList RETURN  STB$TREENODELIST;
-- ***************************************************************************************************
-- Date and Autor:  31 Jan 2006 JB
-- function returns the reload list for the synchronize process with the frontend
--
-- Return:
-- the tree list for the synchronize
--
-- *************************************************************************************************

PROCEDURE setReloadToken(sToken in VARCHAR2) ;
-- ***************************************************************************************************
-- Date and Autor:  31 Jan 2006 JB
-- procedure sets the reload token to identify  where the current node is placed "RIGHT" or "LEFT"
-- of the explorer
--
-- Parameter:
-- sToken     the token to identify in which split screen the current node is set
--
-- *************************************************************************************************

FUNCTION getReloadToken RETURN  VARCHAR2;
-- ***************************************************************************************************
-- Date and Autor:  31 Jan 2006 JB
-- function returns the reload token to identify wwhere the current node is placed
--
--
-- Return:
-- the token to identify in which split screen the current node is set
--
-- *************************************************************************************************

PROCEDURE setDirectory (oCurNode IN OUT STB$TREENODE$RECORD, sParameterName IN VARCHAR2 DEFAULT NULL, sParameterValue IN VARCHAR2 DEFAULT NULL);
-- ***************************************************************************************************
-- Date and Autor: 08. Nov 2007 MCD
-- procedure sets the valuelist (=> the directory) for the node
--
-- Parameter:
-- oCurNode         the current node
-- sParameterName   the parameter name
-- sParameterValue  the parameter value
--
-- *************************************************************************************************

PROCEDURE setNode(oCurNode IN OUT STB$TREENODE$RECORD, nDelta IN NUMBER DEFAULT 0);
-- ***************************************************************************************************
-- Date and Autor: 08. Nov 2007 MCD
-- function resets the node with the delta, means if the delta is -1 the parentnode is set
--
-- Parameter:
-- oCurNode   the current node
-- nDelta     the delta
--
-- *************************************************************************************************

FUNCTION getCurrentParameterName(nDelta IN NUMBER DEFAULT 0) RETURN VARCHAR2;
-- ***************************************************************************************************
-- Date and Autor: 08. Nov 2007 MCD
-- function returns the current parameter name set in the object 
-- or the parameter name of the node plus delta (LABONE)
--
-- Parameter:
-- nDelta     the delta
--
-- Return:
-- the parameter name
--
-- *************************************************************************************************

FUNCTION getCurrentParameterValue(nDelta IN NUMBER DEFAULT 0) RETURN VARCHAR2;
-- ***************************************************************************************************
-- Date and Autor:   08 Mar 2006 MCD
-- function returns the current parameter value set in the object 
-- or the parameter value of the node plus delta
--
-- Parameter:
-- nDelta     the delta
--
-- Return:
-- the parameter value
-- *************************************************************************************************

FUNCTION getCurrentValueList RETURN ISR$VALUE$LIST;
-- ***************************************************************************************************
-- Date and Autor:   08 Mar 2006 MCD
-- Function gets the current value list from the selected node
--
-- Return:
-- the current value list
--
-- ***************************************************************************************************

FUNCTION isNodeInPath(oNode in STB$TREENODE$RECORD, sNodeType IN VARCHAR2 DEFAULT NULL, sNodeId IN VARCHAR2 DEFAULT NULL) RETURN VARCHAR2;
-- ***************************************************************************************************
-- Date and Autor:   13.Nov 2007, MCD
-- Function checks if the nodetype and the nodeid is in the path of the node
--
-- Parameter:
-- oSelectedNode        the selected node
-- sNodeType            the nodetype to search
-- sNodeId              the nodeid to search
--
-- Return:
-- 'T' if the nodetype or the nodeid is in the path else 'F'
--
-- ***************************************************************************************************

FUNCTION GetCurrentNumberOfMasters return pls_integer;
-- ***************************************************************************************************
-- Date and Autor:   10.Jun 2010 HR
-- return the current number of masters
--
-- Return:
-- the number of mastere
--
-- ***************************************************************************************************

PROCEDURE SetCurrentNumberOfMasters(cnMasters in pls_integer);
-- ***************************************************************************************************
-- Date and Autor:   10.Jun 2010 HR
-- sets the current number of masters
--
-- Parameter:
-- cnMasters  the master
--
-- ***************************************************************************************************

FUNCTION calculateHash (olPropertyList IN STB$PROPERTY$LIST) RETURN VARCHAR2;
FUNCTION calculateHash (olValueList IN ISR$VALUE$LIST) RETURN VARCHAR2;

END Stb$object; 