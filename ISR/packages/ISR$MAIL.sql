CREATE OR REPLACE PACKAGE ISR$MAIL
as
-- ***************************************************************************************************
-- Description/Usage:
-- The package holds all functionality send notifcation emails and their attachments 
-- for iStudyReporter
-- ***************************************************************************************************
-- Modification history:
-- Ver   Date       Autor    System            Project             Remarks
-- ***************************************************************************************************
-- 1.0   -          JB       iStudyReporter    Version 2           Creation
-- ***************************************************************************************************
-- public declarations
-- ***************************************************************************************************

-- ****************************************************************************************************
-- functions and procedures
-- ****************************************************************************************************

function SendMail(csRecipient in varchar2,sSubject in varchar2,sText in varchar2,olFileList in ISR$FILE$LIST default null)
return STB$OERROR$RECORD; 

FUNCTION SendEmail( csToken in varchar2, csNodeType in varchar2, csNodeId in varchar2, csNotify_on in varchar2,olFileList in ISR$FILE$LIST default null) return STB$OERROR$RECORD;
-- ***************************************************************************************************
-- Date und Autor: 10. Apr 2008, JB
-- function submits tthe email request in the oracle job queue
--
--
-- Return:
-- STB$OERROR$RECORD    error object with the fields (sErrorCode, sErrorMessage, sSeverityCode)
--
-- Error behaviour/ particularities:
-- none
-- ***************************************************************************************************

--FUNCTION sendMail(csRecipient IN VARCHAR2, csJobid IN VARCHAR2) RETURN STB$OERROR$RECORD;
-- ***************************************************************************************************
-- Date und Autor: 16.May 2011, MCD
-- function sends a java mail to the recipient list, attachments can be found in the table stb$jobtrail
--
-- Parameter :
-- csRecipient    recipient list
-- csJobid        jobid
--
-- Return
-- STB$OERROR$RECORD
--
-- ***************************************************************************************************
FUNCTION SendMailJava(SMTP_HOST_NAME VARCHAR2, SMTP_PORT_NAME VARCHAR2, SMTP_AUTH_USER VARCHAR2, SMTP_AUTH_PWD VARCHAR2, to_user VARCHAR2, cc VARCHAR2, subject VARCHAR2, message VARCHAR2,blFileList IN ISR$FILE$LIST)  return Clob;
--*************************************************************************************************************************
function getMailRunStatus(nServerid in number) return varchar2;

END ISR$MAIL;