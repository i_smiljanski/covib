CREATE OR REPLACE PACKAGE ISR$ErrorObj$Accessor
is

-- ***************************************************************************************************
-- Description/Usage:
--   Used to hold the error object with the current error stack in the current session
--   The error object is defined in the body, so it is accessabel only by function/procedure
-- ***************************************************************************************************

-- ***************************************************************************************************
  FUNCTION  GetCurrentErrorObj RETURN STB$OERROR$RECORD;
-- ***************************************************************************************************
-- Date and Autor: 04 Mar 2014 IS
-- access function for the current error object which holds the current message stack
--
-- Return:
--   the error object
--
-- ***************************************************************************************************

  PROCEDURE SetCurrentErrorObj(oErrorRec in STB$OERROR$RECORD);
-- ***************************************************************************************************
-- Date and Autor: 04 Mar 2014 IS
-- procedure sets the current error object which holds the current message stack
--
-- Parameter:
--   oErrorRec  the error object to be set    
--
-- ***************************************************************************************************

FUNCTION  GetWorstSeverityCode return integer;
-- ***************************************************************************************************
-- Date and Autor: 03.Jun 2015 HR
-- access function for the current session worst severity code
--
-- Return:
--   the session worst severity code
--
-- ***************************************************************************************************

End iSR$ErrorObj$Accessor;