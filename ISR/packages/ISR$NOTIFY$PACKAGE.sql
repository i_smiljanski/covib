CREATE OR REPLACE PACKAGE ISR$NOTIFY$PACKAGE AS
/******************************************************************************
   NAME:       ISR$NOTIFY$PACKAGE
   PURPOSE:

   REVISIONS:
   Ver        Date        Author           Description
   ---------  ----------  ---------------  ------------------------------------
   1.0        22.07.2019      MAURERE108       1. Created this package.
******************************************************************************/

  FUNCTION SendMail(csToken in varchar2 ,oParamListRec in  isr$paramlist$rec  )
  return STB$OERROR$RECORD ;

END ISR$NOTIFY$PACKAGE;