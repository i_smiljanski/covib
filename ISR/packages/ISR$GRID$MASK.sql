CREATE OR REPLACE PACKAGE iSR$Grid$Mask
is
-- ***************************************************************************************************
-- Description/Usage:
-- iStudyReporter knows four types of selection screns in the report wizard.
-- The selection scrrens of the type "Grid" are handled by this package.
-- In a grid selection one master entity exists . Together with the master entity one or more 
-- depending entities  are defined in one selection screen. 
-- Grid entities aredefined in the table ISR$META$GRID$ENTITY. 
-- The first coluumn defines the master column

-- ****************************************************************************************************
FUNCTION GetSList (sEntity IN STB$TYPEDEF.tLine, sMasterKey IN VARCHAR2, sNavigateMode IN varchar2, oSelection OUT ISR$TLRSELECTION$LIST) RETURN STB$OERROR$RECORD;
-- ***************************************************************************************************
-- Date and Autor:  17 Sep 2008  JB
-- function gets for all editable entities of the selection screen and the master key of the grid
-- the selectable values. The values are read into an object list and are displayed in the Frontensd.
-- values which have been selectated before are marked as sSelected ='T' in the object list
--
-- Parameter :
-- sEntity          the current entity in the selection screen
-- sMasterkey       the master key of grid
-- sNavigateMode    flag to identify if the screen is called in the navigation mode
-- oCriteria        the list of the criteria that has been selected before and that has relations
--                  defined to the current entity
-- oParamList       a parameter list that can be used to filter or to limit the available values
-- oSelection       list of selectable values of the entity
--
-- Return:
-- STB$OERROR$RECORD    
--
-- ***********************************************************************************************************

FUNCTION PutSList (sEntity IN STB$TYPEDEF.tLine, oSelection IN ISR$TLRSELECTION$LIST) RETURN STB$OERROR$RECORD;
-- ************************************************************************************************************
-- Date and Autor:  17 Sep 2008  JB
-- function writes the selected values of a grid entity into the database table ISR$CRIT
--
-- Parameter :
-- sEntity      the current entity of the selection screen
-- oSelection   the list of values , the selected values are marked as sSelected ='T'
--
-- Return:
-- STB$OERROR$RECORD   
--
-- ***************************************************************************************************

FUNCTION createAudit(xXml IN OUT XMLTYPE, sAudit OUT VARCHAR2, nLastFinalRepId IN number, nFormNo IN number) RETURN STB$OERROR$RECORD;
-- ***************************************************************************************************
-- Date und Autor: 17 Sep 2008  JB
-- function writes the audit file for the entities into a xml dom
-- if there have been change
--
--
-- Parameter:
-- cnDomId               the xml dom id of the audit file 
-- sAudit                flag to identify if there have been change for the selection screen
-- nLastFinalRepId       the previous report id for whichthe diffrence in the data is checked
-- nFormNo               the number of the selection screen in the report wizard
--
-- Return:
-- STB$OERROR$RECORD    
--
-- ***************************************************************************************************

FUNCTION getEntityList( cnMaskNo IN NUMBER, oEntList OUT STB$ENTITY$LIST) RETURN STB$OERROR$RECORD ;
-- ***********************************************************************************************************
-- Date and Autor:  17 Sep 2008  JB
-- function retrieves an object list of all entities defined on the grid selection screen
--
-- Parameters:
-- cnMaskNo        the number of the selection screen in the report wizard
-- oEntList        object list with the attributes entity "sEnt" and preselected "sPreSelect"
--
-- Return:
-- STB$OERROR$RECORD    
--
-- *********************************************************************************************************************

FUNCTION GetGridSelection (sEntity IN STB$TYPEDEF.tLine, sNavigateMode IN varchar2, oSelection OUT ISR$TLRSELECTION$LIST) RETURN STB$OERROR$RECORD;
-- *********************************************************************************************************************
-- Date and Autor:  17 Sep 2008  JB
-- function initalizes the previos selected values in the grid selection screen.
-- if there areno previous sleected values the default values are retrieved
--
-- Parameters:
-- sEntity          the current entity of the screen
-- oParamList       a parameter list that can be used to filter or to limit the available values
-- sNavigateMode    flag to indicate if the application is in the navigation mode
-- oSelection       object list with the previous sleected values
--
-- Return:
-- STB$OERROR$RECORD    
--
-- *********************************************************************************************************************

FUNCTION GetGridDependency(csEntity IN varchar2, oEntityList OUT STB$ENTITY$LIST, csKey IN varchar2 )  RETURN STB$OERROR$RECORD;
-- *********************************************************************************************************************
-- Date and Autor:  17 Sep 2008  JB
-- function retrieves the dependencies between the entities on the grid
-- the denpending values have to be deleted if a value of a master entry changes 
--
-- Parameter:
-- csEntity             the current entity
-- oEntityList          object list with all  entites of the screen
--                      and the attribute if the entit has to be deleted
-- csKey                the key value of the row in the Grid
--
-- Return:
-- STB$OERROR$RECORD    
--
-- *********************************************************************************************************************

END iSR$Grid$Mask;