CREATE OR REPLACE package body stb$job
IS

  sContext   VARCHAR2(500) ;
  sNameSpace  VARCHAR2(500);
  sJobMessage       varchar2(500);
  nCurrentLanguage  number        := stb$security.getcurrentlanguage;
  crlf              constant varchar2(2) := chr(10)||chr(13);

PROCEDURE logError(sErrorCode IN VARCHAR2, sErrorMessage IN VARCHAR2, sSeverity IN VARCHAR2) IS
  PRAGMA AUTONOMOUS_TRANSACTION;
BEGIN
  INSERT INTO TMP$ERRORS (code, message, severity, timestamp)
  VALUES (sErrorCode, sErrorMessage, sSeverity, sysdate);
  COMMIT;
END;

-- *********************************************************************************************************************
PROCEDURE setLoaderVariable
IS
  sCurrentName   constant varchar2(4000) := $$PLSQL_UNIT||'.setLoaderVariable';
BEGIN
  isr$trace.stat('begin', 'begin', sCurrentName);
  if scontext is null then
     sContext   := STB$UTIL.getSystemParameter('LOADER_CONTEXT');
  end if;
  if sNameSpace is null then
     sNameSpace   := STB$UTIL.getSystemParameter('LOADER_NAMESPACE');
  end if;
  isr$trace.stat('end', 'end', sCurrentName);
END;
-- *********************************************************************************************************************
FUNCTION getJobParameter(csName IN STB$JOBPARAMETER.PARAMETERNAME%TYPE, cnJobid IN STB$JOBPARAMETER.JOBID%TYPE)
  RETURN STB$JOBPARAMETER.PARAMETERVALUE%TYPE
IS
  sCurrentName   constant varchar2(4000) := $$PLSQL_UNIT||'.getJobParameter';
  csValue STB$JOBPARAMETER.PARAMETERVALUE%TYPE;

  CURSOR cGetParameterValue IS
    SELECT PARAMETERVALUE
    FROM STB$JOBPARAMETER
    WHERE PARAMETERNAME = csName
    AND JOBID = cnJobid;
BEGIN
   -- [ISRC-672] do not use logging in this function, because getJobParameter is called from ISR$TRACE
  OPEN cGetParameterValue;
  FETCH cGetParameterValue INTO csValue;
  IF cGetParameterValue%NOTFOUND THEN
    CLOSE cGetParameterValue;
  RETURN NULL;
  END IF;
  CLOSE cGetParameterValue;

  RETURN csValue;
END getJobParameter;

-- *********************************************************************************************************************
PROCEDURE setJobParameter(csName IN STB$JOBPARAMETER.PARAMETERNAME%TYPE, csValue IN STB$JOBPARAMETER.PARAMETERVALUE%TYPE, cnJobid IN STB$JOBPARAMETER.JOBID%TYPE)
IS
  PRAGMA AUTONOMOUS_TRANSACTION;
    sCurrentName   constant varchar2(4000) := $$PLSQL_UNIT||'.setJobParameter('||csName||')';
    
BEGIN
  isr$trace.stat('begin', substr('value: ' || csValue, 0, 4000), sCurrentName);
  
  DELETE FROM STB$JOBPARAMETER WHERE jobid = cnJobid AND parametername = csName;
  INSERT INTO STB$JOBPARAMETER(jobid, parametername, parametervalue)
  VALUES (cnJobid, csName, csValue);
  COMMIT;
  isr$trace.stat('end', 'end', sCurrentName);
END setJobParameter;

-- *********************************************************************************************************************
function CheckJobExists( csReference in  varchar2,
                         sJobExists  out varchar2)
  return STB$OERROR$RECORD
is
  sCurrentName        constant varchar2(100) := $$PLSQL_UNIT||'.checkJobExists('||csReference||')';
  oErrorObj           STB$OERROR$RECORD := STB$OERROR$RECORD();
begin
  isr$trace.stat('begin','csReference: '||csReference,sCurrentName);

  sjobExists := 'F';
  for rGetJobs in ( select jobid
                    from   user_scheduler_jobs uj, STB$JOBQUEUE jq
                    where  uj.job_name = jq.oraclejobid )
  loop
    if Nvl(getJobParameter('REFERENCE', rGetJobs.jobid),'') = csReference then
      sjobExists := 'T';
      exit;
    end if;
  end loop;

  isr$trace.stat('end','sJobExists: '||sJobExists,sCurrentName);
  return oErrorObj;

exception
  when others then
      oErrorObj.handleError ( Stb$typedef.cnSeverityCritical, utd$msglib.getmsg ('exUnexpectedError', stb$security.getCurrentlanguage,  csP1 => sqlerrm(sqlcode)), 
        sCurrentName, dbms_utility.format_error_stack ||chr(10)|| dbms_utility.format_error_backtrace );
    return oErrorObj;
end CheckJobExists;

-- *********************************************************************************************************************
FUNCTION CheckJobExists(sJobExists OUT VARCHAR2) RETURN STB$OERROR$RECORD
IS
  oErrorObj           STB$OERROR$RECORD := STB$OERROR$RECORD();
  sCurrentName   constant varchar2(4000) := $$PLSQL_UNIT||'.CheckJobExists';

  CURSOR cGetJobs IS
    SELECT 'T'
      FROM user_scheduler_jobs uj, STB$JOBQUEUE jq
     WHERE uj.job_name(+) = jq.oraclejobid
    UNION
    SELECT 'T'
      FROM user_scheduler_jobs uj, STB$JOBQUEUE jq
     WHERE uj.job_name = jq.oraclejobid(+);

BEGIN
  isr$trace.stat('begin', 'begin', sCurrentName);

  OPEN cGetJobs;
  FETCH cGetJobs INTO sJobExists;
  IF cGetJobs%NOTFOUND OR sJobExists IS NULL THEN
    sJobExists := 'F';
  END IF;
  CLOSE cGetJobs;

  isr$trace.stat('end', 'end', sCurrentName);
  RETURN oErrorObj;

EXCEPTION
  WHEN OTHERS THEN
      oErrorObj.handleError ( Stb$typedef.cnSeverityCritical, utd$msglib.getmsg ('exUnexpectedError', stb$security.getCurrentlanguage,  csP1 => sqlerrm(sqlcode)), 
        sCurrentName, dbms_utility.format_error_stack ||chr(10)|| dbms_utility.format_error_backtrace );
    RETURN oErrorObj;
END CheckJobExists;

-- *********************************************************************************************************************
FUNCTION checkLoaderClient(cnJobid IN NUMBER, csClientIp IN VARCHAR2, csClientPort IN VARCHAR2) RETURN STB$OERROR$RECORD
IS
  sCurrentName   constant varchar2(4000) := $$PLSQL_UNIT||'.checkLoaderClient('||cnJobid||')';
  oErrorObj               STB$OERROR$RECORD := STB$OERROR$RECORD();
  exDocServerNotAvailable EXCEPTION;

  sUseDocServer       VARCHAR2(1) := 'F';
  sDocServerExists    VARCHAR2(1) := 'F';
  sDocServerFound     VARCHAR2(1) := 'F';

  sClientIp           VARCHAR2(500);
  sClientPort         VARCHAR2(500);

  CURSOR cDocServerExists IS
    SELECT 'T'
      FROM isr$server s
     WHERE s.servertypeid = 14
        OR NVL(STB$JOB.getJobParameter('USE_MY_OWN_SERVER', cnJobid), 'F') != 'F';

BEGIN
  isr$trace.stat('begin', 'begin', sCurrentName);

  isr$trace.debug('cnJobid',cnJobid, sCurrentName);
  isr$trace.debug('sClientIp',sClientIp, sCurrentName);
  isr$trace.debug('sClientPort',sClientPort, sCurrentName);

  sUseDocServer := case
                      when NVL(STB$JOB.getJobParameter('CAN_USE_DOC_SERVER', cnJobid), 'F') = 'T' and NVL(NVL(STB$JOB.getJobParameter('INVISIBLE_FILTER_LOCAL', cnJobid), STB$JOB.getJobParameter('FILTER_LOCAL', cnJobid)), 'T') = 'F' then 'T'
                      else 'F'
                   end;
  isr$trace.debug('sUseDocServer',sUseDocServer, sCurrentName);

  OPEN cDocServerExists;
  FETCH cDocServerExists INTO sDocServerExists;
  IF cDocServerExists%NOTFOUND THEN
    sDocServerExists := 'F';
  END IF;
  CLOSE cDocServerExists;
  isr$trace.debug('sDocServerExists',sDocServerExists, sCurrentName);

  IF sUseDocServer = 'T' AND sDocServerExists = 'T' THEN

    FOR rGetDocServer IN (SELECT HOST, basisport, max_jobs
                            FROM (SELECT HOST, basisport, max_jobs, ROWNUM myrow
                                    FROM (SELECT observer_host host, s.port basisport,  s.max_jobs, COUNT (sj.job_name) cntJobs
                                            FROM  isr$serverobserver$v s
                                               , stb$jobqueue jq
                                               , stb$jobsession js
                                               , user_scheduler_jobs sj
                                           WHERE s.servertypeid = NVL(STB$JOB.getJobParameter('USE_MY_OWN_SERVER', cnJobid), 14)
                                             AND jq.jobid(+) = js.jobid
                                             AND jq.oraclejobid = sj.job_name(+)
                                             AND sj.state(+) = 'RUNNING'
                                             AND js.ip(+) = s.observer_host
                                             AND js.port(+) = s.port
                                             and s.runstatus='T'
                                          GROUP BY  observer_host, s.port, s.max_jobs
                                          ORDER BY cntJobs, max_jobs))
                           WHERE myrow = 1) LOOP
      sDocServerFound := 'T';
      sClientIp := rGetDocServer.host;
      sClientPort := rGetDocServer.basisport;
      STB$JOB.setJobParameter('MAX_RS_JOBS', rGetDocServer.max_jobs, cnJobId);
    END LOOP;

    IF sDocServerFound = 'F' THEN
      RAISE exDocServerNotAvailable;
    END IF;

  ELSE
    sClientIp := csClientIp;
    sClientPort := csClientPort;
  END IF;

  update stb$jobsession
     set ip = sClientIp,
       port = sClientPort
   where jobid = cnJobid;
   
  commit;
  
  isr$trace.setSessionInfo;

  isr$trace.debug('end sClientIp',sClientIp, sCurrentName);
  isr$trace.debug('end sClientPort',sClientPort, sCurrentName);

  isr$trace.stat('end', 'end', sCurrentName);
  RETURN oErrorObj;

EXCEPTION
  WHEN exDocServerNotAvailable THEN
    oErrorObj.sErrorCode      :=Utd$msglib.GetMsg('exDocServerNotAvailable',Stb$security.GetCurrentLanguage);
      oErrorObj.handleError ( Stb$typedef.cnSeverityCritical, utd$msglib.getmsg ('exDocServerNotAvailableText', stb$security.getCurrentlanguage,  csP1 => sqlerrm(sqlcode)), 
        sCurrentName, dbms_utility.format_error_stack ||chr(10)|| dbms_utility.format_error_backtrace );
    RETURN oErrorObj;
  WHEN OTHERS THEN
      oErrorObj.handleError ( Stb$typedef.cnSeverityCritical, utd$msglib.getmsg ('exUnexpectedError', stb$security.getCurrentlanguage,  csP1 => sqlerrm(sqlcode)), 
        sCurrentName, dbms_utility.format_error_stack ||chr(10)|| dbms_utility.format_error_backtrace );
    RETURN oErrorObj;
END checkLoaderClient;

--***********************************************************************************************************************************
FUNCTION startJob RETURN STB$OERROR$RECORD IS

  --Variables
  oErrorObj               STB$OERROR$RECORD := STB$OERROR$RECORD();
  exStartJob              EXCEPTION;
  exNotFindAction         EXCEPTION;
  sCurrentName   constant varchar2 (100) := $$PLSQL_UNIT || '.startJob (' || nJobId || ')';
  sStatusText             STB$JOBQUEUE.STATUSTEXT%TYPE := 'START_REPORT_CREATION';
  nStatus                 STB$JOBQUEUE.STATUS%TYPE := Stb$typedef.cnReportSubmit;
  nProgress               STB$JOBQUEUE.PROGRESS%TYPE:=0;
  nJobSession             STB$JOBSESSION.SESSIONNO%TYPE;
  sJobLoglevel            varchar2(100);
  sMsg                  varchar2(4000);

  sJobAction              VARCHAR2(32000);
  sTokenDesc              VARCHAR2(4000);

  CURSOR cGetTokenDesc(csToken IN VARCHAR2, csNodeType IN VARCHAR2) IS
    select Utd$msglib.GetMsg (description,  Stb$security.GetCurrentLanguage)
      from stb$contextmenu
     where token = csToken
       and nodetype = csNodeType;

BEGIN
  isr$trace.stat('begin job','nJobId: '||nJobId,sCurrentName);

  stb$job.setJobParameter ('USER', Stb$security.GetCurrentUser, stb$job.njobid);
  stb$job.setJobParameter ('FOLDERNAME', STB$SECURITY.getOracleUserName(STB$SECURITY.getCurrentUser)||'_'||stb$job.njobid, stb$job.njobid);
  stb$job.setJobParameter ('REFERENCE_DISPLAY', STB$OBJECT.getCurrentNode().sDisplay, stb$job.njobid);

  if stb$job.getJobParameter ('ACTIONID', stb$job.njobid) is null then
     sMsg := utd$msglib.getmsg ('exNotFindAction', stb$security.getCurrentLanguage, STB$JOB.getJobParameter('TOKEN', nJobId));
    raise exNotFindAction;
  end if;
  

  -- 27.Nov.2017 (vs) [ISRC-724]
  -- Determine Loglevel for Job,
  -- Use "hightest" loglevel for  SystemLoglevel or User's parameter: DEBUG_JOB
  select isr$trace.getLogLevelStr( greatest(  ISR$TRACE.getLoglevelNo( STB$USERSECURITY.getUserParameter( Stb$security.GetCurrentUser, 'DEBUG_JOB' ) ),
                                              ISR$TRACE.getLoglevelNo( STB$UTIL.getSystemParameter('LOGLEVEL') ) ) )
  into   sJobLoglevel
  from   dual;
  -- Set Loglevel for Job now
  -- if user's Job Loglevel is undefined or has wrong contents, then it is set to NONE
  stb$job.setJobParameter ('FILTER_LOGLEVELNAME', nvl(sJobLoglevel,'NONE'), stb$job.njobid);


  OPEN cGetTokenDesc(STB$JOB.getJobParameter('TOKEN', nJobId), STB$JOB.getJobParameter('NODETYPE', nJobId));
  FETCH cGetTokenDesc INTO sTokenDesc;
  IF cGetTokenDesc%NOTFOUND OR sTokenDesc IS NULL THEN
    sTokenDesc := Utd$msglib.GetMsg (STB$JOB.getJobParameter('TOKEN', nJobId),  Stb$security.GetCurrentLanguage);
  END IF;
  CLOSE cGetTokenDesc;
  stb$job.setJobParameter ('TOKEN_DESC', sTokenDesc, stb$job.njobid);

  -- create the entry in the  STB$JOBQUEUE
  INSERT INTO STB$JOBQUEUE (JOBID
                          , STATUS
                          , STATUSTEXT
                          , PROGRESS
                          , USERNO)
  VALUES (nJobId
        , nStatus
        , Utd$msglib.GetMsg (sStatusText, Stb$security.GetCurrentLanguage)
        , nProgress
        , Stb$security.GetCurrentUser);

  SELECT SEQ$STB$JOBSESSION.NEXTVAL INTO nJobSession FROM dual;
  INSERT INTO STB$JOBSESSION ( jobid, sessionno, sessionid )
  VALUES ( nJobId, nJobSession, STB$SECURITY.getCurrentSession );

  -- start job
  IF (oErrorObj.sSeverityCode = Stb$typedef.cnNoError) THEN

    --sJobAction :=  'create or replace procedure PROC_JOB_'||nJobId||' as
    sJobAction :=  'declare'||chr(10)||
                   '  oErrorObj STB$OERROR$RECORD := STB$OERROR$RECORD();'||chr(10)||
                   '  exDocServerNotAvailable   EXCEPTION;'||chr(10)||
                   '  exCouldNotCreateReport    EXCEPTION;'||chr(10)||
                   '  sIp   VARCHAR2(50);'||chr(10)||
                   '  sPort VARCHAR2(50);'||chr(10)||
                   'begin'||chr(10)||
                   -- set OracleJobsessionid first
                   '  update STB$JOBSESSION set ORACLEJOBSESSIONID = STB$SECURITY.getCurrentSession where sessionno = '||nJobSession||';'||chr(10)||
                   '  commit;'||chr(10)||
                   -- start logging
                   '  isr$trace.setSessionInfo;'||chr(10)||
                   '  isr$trace.stat(''begin'',''scheduler job '||nJobId||' started'',''DBMS_SCHEDULER('||nJobId||')'');'||chr(10)||
                   -- filter inactive
                   '  isr$trace.debug(''code position'',''before check FILTER_INACTIVE'',''DBMS_SCHEDULER('||nJobId||')'');'||chr(10)||
                   '  IF NVL(TRIM(STB$JOB.getJobParameter(''FILTER_INACTIVE'', '||nJobId||')), ''F'') = ''T'' THEN'||chr(10)||
                   '    isr$trace.error(''error'',''RAISE_NO_DATA_FOUND'',''DBMS_SCHEDULER('||nJobId||')'');'||chr(10)||
                   '    RAISE NO_DATA_FOUND;'||chr(10)||
                   '  END IF;'||chr(10)||
                   '  update STB$JOBQUEUE set SUBMITTIME = sysdate where jobid = '||stb$job.njobid||';'||chr(10)||
                   '  commit;'||chr(10)||
                   '  oErrorObj := STB$JOB.checkLoaderClient('||nJobId||','''||STB$SECURITY.getClientIp||''','''||STB$SECURITY.getClientPort||''');'||chr(10)||
                   -- checking loader
                   '  isr$trace.debug(''code position'',''before checking loader'',''DBMS_SCHEDULER('||nJobId||')'');'||chr(10)||
                   '  IF oErrorObj.sSeverityCode != stb$typedef.cnnoerror THEN'||chr(10)||
                   '    STB$JOB.setJobParameter(''JOB_ERROR'', oErrorObj.sErrorMessage, '||stb$job.njobid||');'||chr(10)||
                   '    STB$JOB.setJobParameter(''FILTER_INACTIVE'', ''T'', '||stb$job.njobid||');'||chr(10)||
                   '    isr$trace.error(''error'',''RAISE exDocServerNotAvailable'',''DBMS_SCHEDULER('||nJobId||')'');'||chr(10)||
                   '    RAISE exDocServerNotAvailable;'||chr(10)||
                   '  END IF;'||chr(10)||
                   '  Stb$security.initJobUser('||Stb$security.GetCurrentUser||');'||chr(10)||
                   -- handle report creation jobs
                   '  isr$trace.debug(''code position'',''before handle report creation jobs'',''DBMS_SCHEDULER('||nJobId||')'');'||chr(10)||
                   CASE WHEN stb$job.getjobparameter ('TOKEN', stb$job.njobid) = 'CAN_RUN_REPORT'
                   AND STB$UTIL.getReptypeParameter(STB$OBJECT.getCurrentReporttypeid, 'REQUIRES_VERSIONING') = 'T' THEN
                   '  oErrorObj := ISR$REPORTTYPE$PACKAGE.createReportFromJob('||STB$OBJECT.getCurrentRepid||', '''||REPLACE(STB$OBJECT.getCurrentTitle,'''','''''')||''', '||STB$OBJECT.getCurrentReporttypeid||');'||chr(10)||
                   '  IF oErrorObj.sSeverityCode != stb$typedef.cnnoerror THEN'||chr(10)||
                   '    STB$JOB.setJobParameter(''JOB_ERROR'', oErrorObj.sErrorMessage, '||stb$job.njobid||');'||chr(10)||
                   '    STB$JOB.setJobParameter(''FILTER_INACTIVE'', ''T'', '||stb$job.njobid||');'||chr(10)||
                   '    isr$trace.error(''error'',''RAISE exCouldNotCreateReport'',''DBMS_SCHEDULER('||nJobId||')'');'||chr(10)||
                   '    RAISE exCouldNotCreateReport;'||chr(10)||
                   '  END IF;'||chr(10)||
                   '  STB$JOB.setJobParameter(''REPID'', STB$OBJECT.getCurrentRepid, '||nJobId||');'||chr(10)
                   END||
                   -- handle locking of reports
                   '  isr$trace.debug(''code position'',''before  handle locking of reports'',''DBMS_SCHEDULER('||nJobId||')'');'||chr(10)||
                   '  IF STB$JOB.getJobParameter(''REPID'', '||nJobId||') IS NOT NULL AND NVL(STB$JOB.getJobParameter(''DO_NOT_SWITCH_REPSESSION'', '||nJobId||'),''F'') = ''F'' THEN'||chr(10)||
                   '    oErrorObj := Stb$security.SwitchRepSession('||nJobSession||',STB$JOB.getJobParameter(''REPID'', '||nJobId||'));'||chr(10)||
                   '  END IF;'||chr(10)||
                   -- handle long running jobs
                   '  isr$trace.debug(''code position'',''before  handle long running jobs'',''DBMS_SCHEDULER('||nJobId||')'');'||chr(10)||
                   CASE WHEN TRIM(stb$job.getjobparameter ('JOBTYPE', stb$job.njobid)) IS NOT NULL THEN
                   '  STB$JOB.waitForRunningDocumentJobs('||STB$OBJECT.getCurrentReporttypeid||', substr(stb$job.getjobparameter (''JOBTYPE'', '||nJobId||'), 0, INSTR(stb$job.getjobparameter (''JOBTYPE'', '||nJobId||'), ''_'', -1) - 1), '||nJobId||');'||chr(10)
                   END||
                   '  STB$JOB.waitForRunningISRJobs('||nJobId||');'||chr(10)||
                   '  STB$RAL.createReport(' || nJobId  ||');'||chr(10)||
                   '  isr$trace.stat(''end'',''Scheduler job '||nJobId||' finished'',''DBMS_SCHEDULER('||nJobId||')'');'||chr(10)||
                 /*'exception'||chr(10)||
                   '  when others then'||chr(10)||
                    '    sUserMsg := utd$msglib.getmsg(''exUnexpectedError'', stb$security.getcurrentlanguage,  csP1 => sqlerrm(sqlcode));'||chr(10)||
                   '    sImplMsg := sCurrentName || rtrim('': ''||sImplMsg,'': '');'||chr(10)||
                   '    sDevMsg  := substr(dbms_utility.format_error_stack ||chr(13)||chr(10)||dbms_utility.format_error_backtrace, 1,4000);'||chr(10)||
                   '    oErrorObj.handleError ( Stb$typedef.cnSeverityCritical, sUserMsg, sImplMsg, sDevMsg );'||chr(10)||
                   '    raise;'||chr(10)||
                 */
                   'end;';

    isr$trace.debug('status sJobAction',sJobAction, sCurrentName);
    dbms_scheduler.create_job( job_name   => 'JOB_'||nJobId ,
                               job_type   => 'PLSQL_BLOCK',
                               job_action => sJobAction,
                               comments   => stb$job.getjobparameter ('JOBTYPE', stb$job.njobid));
    isr$trace.debug('code position','after dbms_scheduler.create_job(...)', sCurrentName);
    oErrorObj := STB$JOB.modifyJob(nJobId);
    isr$trace.debug('code position','after STB$JOB.modifyJob('||nJobId||').', sCurrentName);

    -- write the oracle jobid into the stb$jobqueue
    UPDATE STB$JOBQUEUE
       SET oraclejobid = 'JOB_'||nJobId
     WHERE jobid = nJobId;

    COMMIT;

    isr$trace.debug('end','end', sCurrentName);
    RAISE exStartJob;
  ELSE
    isr$trace.error('rollback','rollback', sCurrentName);
    ROLLBACK;
  END IF;

  isr$trace.stat('end job','end',sCurrentName);
  return oErrorObj;

EXCEPTION
  WHEN exStartJob THEN
    oErrorObj.sErrorCode      :=Utd$msglib.GetMsg('exStartJobCode',Stb$security.GetCurrentLanguage);
    oErrorObj.sErrorMessage   :=Utd$msglib.GetMsg('exStartJobMessage',Stb$security.GetCurrentLanguage);
    IF Trim(sJobMessage) IS NOT NULL THEN
      oErrorObj.sErrorMessage   := oErrorObj.sErrorMessage || Chr(10) || Utd$msglib.GetMsg(sJobMessage,Stb$security.GetCurrentLanguage);
    END IF;
    oErrorObj.sSeverityCode   :=Stb$typedef.cnMsgWarningTimeout;
    RETURN oErrorObj;
  WHEN exNotFindAction THEN
    oErrorObj.handleError( Stb$typedef.cnSeverityCritical, sMsg, 
         sCurrentName, dbms_utility.format_error_stack ||crlf|| dbms_utility.format_error_backtrace ); 
    RETURN oErrorObj;
  WHEN NO_DATA_FOUND THEN
      oErrorObj.handleError ( Stb$typedef.cnSeverityCritical, utd$msglib.getmsg ('exUnexpectedError', stb$security.getCurrentlanguage,  csP1 => sqlerrm(sqlcode)), 
        sCurrentName, dbms_utility.format_error_stack ||chr(10)|| dbms_utility.format_error_backtrace );
    RETURN oErrorObj;
  WHEN OTHERS THEN
      oErrorObj.handleError ( Stb$typedef.cnSeverityCritical, utd$msglib.getmsg ('exUnexpectedError', stb$security.getCurrentlanguage,  csP1 => sqlerrm(sqlcode)), 
        sCurrentName, dbms_utility.format_error_stack ||chr(10)|| dbms_utility.format_error_backtrace );
    Stb$job.setJobProgress(nJobID, Stb$typedef.cnReportAbort, SQLERRM, 100);
    ROLLBACK;
    RETURN oErrorObj;
END startJob;

--***********************************************************************************************************************************
procedure deleteJobs(cnJobid in number)
is
  sCurrentName   constant varchar2(4000) := $$PLSQL_UNIT||'.deleteJobs('||cnJobid||')';
  oErrorObj       STB$OERROR$RECORD := STB$OERROR$RECORD();
  sUserMsg  varchar2(4000);

  cursor cCheckOracleJobExists is
    select js.sessionid
      from user_scheduler_jobs us, STB$JOBQUEUE jq, STB$JOBSESSION js
     where trim (us.job_name) = trim (jq.oraclejobid)
       and jq.jobid = cnJobId
       and js.jobid = jq.jobid;

  rCheckOracleJobExists cCheckOracleJobExists%rowtype;
begin
  isr$trace.stat('begin', 'begin', sCurrentName);
  open cCheckOracleJobExists;
  fetch cCheckOracleJobExists into rCheckOracleJobExists;
  if cCheckOracleJobExists%notfound then
    delete from STB$JOBTRAIL where jobid = cnJobid;
    delete from STB$JOBPARAMETER where jobid = cnJobid;
    delete from STB$JOBSESSION where jobid = cnJobid;
    delete from STB$JOBQUEUE where jobid = cnJobid;
  end if;
  close cCheckOracleJobExists;
  commit;
  isr$trace.stat('end', 'end', sCurrentName);
exception
  when others then
    sUserMsg := utd$msglib.getmsg ('exUnexpectedError', nCurrentLanguage,  csP1 => sqlerrm(sqlcode));
    oErrorObj.handleError( Stb$typedef.cnSeverityCritical, sUserMsg, sCurrentName, dbms_utility.format_error_stack ||crlf|| dbms_utility.format_error_backtrace );
end deleteJobs;


--***********************************************************************************************************************************
procedure deleteJobEntries(cnJobid in number, sHost in varchar2)
is
  sCurrentName   constant varchar2(4000) := $$PLSQL_UNIT||'.deleteJobEntries('||cnJobid||')';
  sPath           STB$DOCTRAIL.Path%TYPE;
  oErrorObj       STB$OERROR$RECORD := STB$OERROR$RECORD();
  sUserMsg  varchar2(4000);

begin
  isr$trace.stat('begin', 'begin', sCurrentName);
  delete from ISR$DOC$PROPERTY
   where docid in ( select docid
                   from   STB$DOCTRAIL d
                   where  upper(job_flag) in ('D')
                   and    ((nodeid = getJobParameter('REFERENCE', cnJobId) and nodetype = getJobParameter('NODETYPE', cnJobId))
                   or      (nodeid = getJobParameter('REPID', cnJobId) and nodetype = 'REPORT')
                   or       docid = getJobParameter('DOCID', cnJobId)));
  delete from STB$DOCTRAIL
   where  upper(job_flag) in ('D')
  and    ((nodeid = getJobParameter('REFERENCE', cnJobId) and nodetype = getJobParameter('NODETYPE', cnJobId))
  or      (nodeid = getJobParameter('REPID', cnJobId) and nodetype = 'REPORT')
  or       docid = getJobParameter('DOCID', cnJobId));
  for rGetDocs in (select docid
                   from stb$doctrail
                   where upper(job_flag) = 'O'
                   and   ((nodeid = getJobParameter('REFERENCE', cnJobId) and nodetype = getJobParameter('NODETYPE', cnJobId))
                   or     (nodeid = getJobParameter('REPID', cnJobId) and nodetype = 'REPORT')
                   or      docid = getJobParameter('DOCID', cnJobId))) loop
    sPath  := substr(STB$JOB.getJobParameter('TO',cnJobId), 0, instr(STB$JOB.getJobParameter('TO',cnJobId), '\', -1) - 1);
    oErrorObj := STB$DOCKS.switchCheckoutFlag(rGetDocs.docid, 'T', STB$JOB.getJobParameter('USER', cnJobid), sHost, sPath);
  end loop;
  for rGetDocs in (select docid
                   from stb$doctrail
                   where upper(job_flag) = 'I'
                   and   ((nodeid = getJobParameter('REFERENCE', cnJobId) and nodetype = getJobParameter('NODETYPE', cnJobId))
                   or     (nodeid = getJobParameter('REPID', cnJobId) and nodetype = 'REPORT')
                   or      docid = getJobParameter('DOCID', cnJobId))) loop
    oErrorObj := STB$DOCKS.switchCheckoutFlag(rGetDocs.docid, 'F', STB$JOB.getJobParameter('USER', cnJobid));
  end loop;

  update STB$DOCTRAIL
  set    job_flag = null
  where  ((nodeid = getJobParameter('REFERENCE', cnJobId) and nodetype = getJobParameter('NODETYPE', cnJobId))
            or (nodeid = getJobParameter('REPID', cnJobId) and nodetype = 'REPORT')
            or docid = getJobParameter('DOCID', cnJobId))
  and    job_flag not in ('R');
  isr$trace.stat('end', 'end', sCurrentName);
exception
  when others then
    sUserMsg := utd$msglib.getmsg ('exUnexpectedError', nCurrentLanguage,  csP1 => sqlerrm(sqlcode));
    oErrorObj.handleError( Stb$typedef.cnSeverityCritical, sUserMsg, sCurrentName, dbms_utility.format_error_stack ||crlf|| dbms_utility.format_error_backtrace );
end deleteJobEntries;

--***********************************************************************************************************************************
PROCEDURE deleteJobs IS
BEGIN
  NULL;
END deleteJobs;

--***********************************************************************************************************************************
FUNCTION terminateJob(cnJobid IN NUMBER) RETURN STB$OERROR$RECORD
IS
  sCurrentName   constant varchar2(4000) := $$PLSQL_UNIT||'.terminateJob('||cnJobid||')';
  oErrorObj       STB$OERROR$RECORD := STB$OERROR$RECORD();
  nSessionCnt     NUMBER := 0;

  CURSOR cGetSessionInfo IS
    SELECT *
    FROM   STB$JOBSESSION js
    WHERE  js.jobid = cnJobId;

  CURSOR cGetSessionsCnt(sIp IN STB$JOBSESSION.ip%TYPE, nPort IN STB$JOBSESSION.port%TYPE) IS
    SELECT COUNT(sessionid)
    FROM   isr$session
    WHERE  ip = sIp
    AND port = nPort;

  CURSOR cGetMarkedSessions(sIp IN STB$JOBSESSION.ip%TYPE, nPort IN STB$JOBSESSION.port%TYPE) IS
    SELECT sessionid
    FROM   isr$session
    WHERE  markedforshutdown = 'T'
    AND    ip = sIp
    AND port = nPort;

  --Records
  rGetSessionInfo    cGetSessionInfo%ROWTYPE;
  rGetMarkedSessions cGetMarkedSessions%ROWTYPE;

BEGIN
  isr$trace.stat('begin', 'begin', sCurrentName);

  OPEN cGetSessionInfo;
  FETCH cGetSessionInfo INTO rGetSessionInfo;
  CLOSE cGetSessionInfo;

  -- unlock the reports
  oErrorObj := Stb$system.unLockReports('and locksession='||rGetSessionInfo.sessionno);

  -- update jobqueue
  update stb$jobqueue set progress = 100 where jobid = cnJobId;

  -- delete  all entries of the actual job
  update stb$doctrail
     set job_flag = null
   where ( (nodeid = getJobParameter ('REFERENCE', cnJobId)
        and nodetype = getJobParameter ('NODETYPE', cnJobId))
       or (nodeid = getJobParameter ('REPID', cnJobId)
       and nodetype like 'REPORT%')
       or docid = getJobParameter ('DOCID', cnJobId));
     
  IF STB$UTIL.getSystemParameter('JAVA_INSIDE_DB') = 'F' AND NVL (isr$process.checkProcessServer, 'F') = 'T' THEN
    BEGIN
      ISR$PROCESS.deleteFile(ISR$PROCESS.getUserHome||ISR$PROCESS.sSessionPath);
    EXCEPTION WHEN OTHERS THEN
      NULL;
    END;
  END IF;

  COMMIT;
  
  for rGetDocs in ( 
      select docid, doctype
      from stb$doctrail
     where doctype in ('C','D','W')
       and getJobParameter ('TOKEN', cnJobId) not like 'CAN_DISPLAY%'
       and (
         (nodeid = getJobParameter ('REFERENCE', cnJobId) and nodetype = getJobParameter ('NODETYPE', cnJobId))
         or (nodeid = getJobParameter ('REPID', cnJobId) and nodetype = case when stb$job.getJobParameter ('NODETYPE', cnJobId)='DOCUMENT_WORD' then 'REPORT_WORD' else 'REPORT' end)
         or docid = getJobParameter ('DOCID', cnJobId)
         ) ) loop
    ISR$TREE$PACKAGE.sendSyncCall(stb$docks.getDocNodetype(rGetDocs.doctype), rGetDocs.docid);                        
  end loop;  

  OPEN cGetSessionsCnt(rGetSessionInfo.ip, rGetSessionInfo.port);
  FETCH cGetSessionsCnt INTO nSessionCnt;
  CLOSE cGetSessionsCnt;

  isr$trace.debug('nSessionCnt',nSessionCnt, sCurrentName);

  IF nSessionCnt = 0 THEN
    deleteJobs(cnJobid);
  END IF;

  OPEN cGetMarkedSessions(rGetSessionInfo.ip, rGetSessionInfo.port);
  FETCH cGetMarkedSessions INTO rGetMarkedSessions;
  IF cGetMarkedSessions%FOUND THEN
     isr$trace.debug('rGetMarkedSessions',rGetSessionInfo.sessionid||' '||rGetMarkedSessions.sessionid, sCurrentName);
     deleteJobs(cnJobid);
     STB$GAL.deleteSessionInfo(rGetSessionInfo.sessionid);
  END IF;
  CLOSE cGetMarkedSessions;

  isr$trace.stat('end', 'end', sCurrentName);
  RETURN oErrorObj;

EXCEPTION
  WHEN OTHERS THEN
      oErrorObj.handleError ( Stb$typedef.cnSeverityCritical, utd$msglib.getmsg ('exUnexpectedError', stb$security.getCurrentlanguage,  csP1 => sqlerrm(sqlcode)), 
        sCurrentName, dbms_utility.format_error_stack ||chr(10)|| dbms_utility.format_error_backtrace );
    ROLLBACK;
  RETURN oErrorObj;
END terminateJob;

--*****************************************************************************************************
procedure markDBJobAsBroken(cnJobId in NUMBER, sHost IN VARCHAR2, sPort IN VARCHAR2, coErrorObj in STB$OERROR$RECORD, removeJob in BOOLEAN DEFAULT false)
  is
  sCurrentName  constant varchar2(100) := $$PLSQL_UNIT||'.markDBJobAsBroken('||cnJobid||')';
  sUserMsg               varchar2(4000);
  oErrorObj              STB$OERROR$RECORD := STB$OERROR$RECORD();
  oParamListRec          ISR$PARAMLIST$REC := ISR$PARAMLIST$REC();


  CURSOR cGetSessionInfo IS
    SELECT *
     FROM  STB$JOBSESSION js
     WHERE js.jobid = cnJobId;

  CURSOR cGetMarkedSessions(sIp IN STB$JOBSESSION.ip%TYPE, nPort IN STB$JOBSESSION.port%TYPE) IS
    SELECT sessionid
    FROM   isr$session
    WHERE  markedforshutdown = 'T'
    AND     ip = sIp
    AND port = nPort;

  CURSOR cGetValidSessions IS
    SELECT ip, port
    FROM   isr$session
    WHERE  nvl(markedforshutdown, 'F') = 'F';

  --Records
  rGetSessionInfo    cGetSessionInfo%ROWTYPE;
  rGetMarkedSessions cGetMarkedSessions%ROWTYPE;


  cursor cGetJobSessions is
  (SELECT DISTINCT ip, port
      FROM (SELECT ip, port
              FROM STB$JOBSESSION
             WHERE jobid = cnJobid
            UNION
            SELECT ip, port
              FROM isr$session s, stb$jobqueue jq
             WHERE UPPER(s.username) = UPPER(STB$SECURITY.getOracleUsername(jq.userno))
               AND jq.jobid = cnJobid)
     WHERE TRIM (ip) IS NOT NULL
       AND TRIM (port) IS NOT NULL
    )
   INTERSECT
    SELECT ip, port
      FROM isr$session s
     WHERE NVL(inactive, 'F') = 'F'
       AND NVL(markedforshutdown, 'F') = 'F';

  sReturn             VARCHAR2(4000);

begin
  isr$trace.stat('begin','removeJob = ' || case when removeJob then 'T' when not removeJob then 'F' else '?' end, sCurrentName);

  setLoaderVariable;

  if not removeJob then
    if NVL(STB$JOB.getJobParameter('ALERTERROR',cnJobid), 'T') = 'F' then
      logError(coErrorObj.sErrorCode, coErrorObj.sErrorMessage, coErrorObj.sSeverityCode);
      return;
    end if;
  end if;

  IF not removeJob THEN
    sReturn := ISR$LOADER.loadLoaderDir(sHost, sPort, sContext, sNamespace, cnJobId, 'LOADER_BLOB_FILE', isr$trace.getJavaUserLoglevelStr, isr$trace.getReference, STB$UTIL.getSystemParameter('TIMEOUT_LOADER_THREAD'));   
  END IF;

  -- get the current session
  OPEN cGetSessionInfo;
  FETCH cGetSessionInfo INTO rGetSessionInfo;
  CLOSE cGetSessionInfo;

  if rGetSessionInfo.sessionno is not null then
  -- unlock the reports
    oErrorObj := Stb$system.unLockReports('and locksession='||rGetSessionInfo.sessionno);
  end if;

   -- send sendErrorObjToLoader to all clients
  for rGetJobSessions in cGetJobSessions loop

    isr$trace.debug('send sendErrorObjToLoader to loader', rGetJobSessions.ip || ' ' || rGetJobSessions.port ||' context, namespace:  ' || sContext || ' ' || sNamespace, sCurrentName);
    isr$loader.sendErrorObjToLoader(rGetJobSessions.ip, rGetJobSessions.port, coErrorObj, cnJobId);
    isr$trace.debug('send terminateJob to loader', rGetJobSessions.ip || ' ' || rGetJobSessions.port ||' context, namespace:  ' || sContext || ' ' || sNamespace, sCurrentName);
    sReturn := isr$loader.terminateJob(rGetJobSessions.ip, rGetJobSessions.port, sContext, sNamespace, cnJobId);
    if sReturn != 'T' then
      isr$trace.error('sReturn', sReturn, sCurrentName);
    end if;
   
  end loop;

  -- insert into the log  
  FOR rGetLoaderFile IN (SELECT filename, binaryfile
                      FROM stb$jobtrail
                     WHERE jobid = cnJobid
                       AND DBMS_LOB.getLength (binaryfile) > 0
                       AND documenttype = 'LOADER_DIR') LOOP
    ISR$TRACE.error(rGetLoaderFile.filename, 'File in LOGBLOB, size ' || DBMS_LOB.GETLENGTH(rGetLoaderFile.binaryfile), sCurrentName, rGetLoaderFile.binaryfile);
  END LOOP;

  IF STB$UTIL.getSystemParameter('NOTIFY') = 'T' THEN
    --oErrorObj := ISR$MAIL.SendEmail(STB$JOB.getJobParameter('TOKEN', cnJobId),STB$JOB.getJobParameter('NODETYPE', cnJobId),cnJobid,'JOB_ERROR');
    --oErrorObj := stb$job.sendMail('NOTIFY', cnJobId,'JOB_ERROR');

      oParamListRec.AddParam('JOBID', TO_CHAR (cnJobId));
      oParamListRec.AddParam('NOTIFY_ON', 'JOB_ERROR');

      oErrorObj := ISR$NOTIFY$PACKAGE.SENDMAIL(STB$JOB.getJobParameter('TOKEN', cnJobId), oParamListRec);
  END IF;

  -- delete all entries of the actual job
  deleteJobEntries(cnJobid, sHost);

  IF removeJob THEN
    deleteJobs(cnJobid);
    for rGetValidSessions in cGetValidSessions loop
      isr$trace.debug('send sendJobList an loader', rGetValidSessions.ip || ' ' || rGetValidSessions.port , sCurrentName);
      sReturn := isr$loader.sendJobList(rGetValidSessions.ip, rGetValidSessions.port);
      if sReturn != 'T' THEN
        ISR$TRACE.error('sReturn', sReturn, sCurrentName);
      end if;
    end loop;
  ELSE
    setJobParameter('END_LOADER', 'F', cnJobid);
    setJobParameter('JOB_ERROR', coErrorObj.sErrorMessage, cnJobid);
    --coErrorObj.GetMessages(sUserMsg, sImplMsg, sDevMsg);
    setJobParameter('JOB_USERSTACK', coErrorObj.GetUserMessages, cnJobid);
    setJobParameter('JOB_IMPLSTACK', coErrorObj.GetImplMessages, cnJobid);
    setJobParameter('JOB_DEVSTACK', coErrorObj.GetDevMessages, cnJobid);
    setJobParameter('FILTER_INACTIVE', 'T', cnJobid);
    oErrorObj := STB$JOB.modifyJob(cnJobid);
  END IF;

  IF STB$UTIL.getSystemParameter('JAVA_INSIDE_DB') = 'F' AND NVL (isr$process.checkProcessServer, 'F') = 'T' THEN
    BEGIN
      ISR$PROCESS.deleteFile(ISR$PROCESS.getUserHome||ISR$PROCESS.sSessionPath);
    EXCEPTION WHEN OTHERS THEN
      NULL;
    END;
  END IF;

  COMMIT;

  open cGetMarkedSessions(rGetSessionInfo.ip, rGetSessionInfo.port);
  fetch cGetMarkedSessions into rGetMarkedSessions;
  if not cGetMarkedSessions%notfound then
    STB$GAL.deleteSessionInfo(rGetSessionInfo.sessionid);
  end if;
  close cGetMarkedSessions;

  isr$trace.stat('end','end',sCurrentName);

exception
  when others then
    sUserMsg := utd$msglib.getmsg ('exUnexpectedError', nCurrentLanguage,  csP1 => sqlerrm(sqlcode));
    oErrorObj.handleError( Stb$typedef.cnSeverityCritical, sUserMsg, sCurrentName, dbms_utility.format_error_stack ||crlf|| dbms_utility.format_error_backtrace );
end markDBJobAsBroken;


--*****************************************************************************************************
procedure markDBJobAsBroken( cnJobId       in number,
                             sHost         in varchar2,
                             sPort         in varchar2,
                             sErrorCode    in varchar2,
                             sErrorMessage in varchar2,
                             sSeverity     in varchar2,
                             removeJob     in BOOLEAN default false)
is
  oErrorObj     STB$OERROR$RECORD := STB$OERROR$RECORD(sErrorCode, sErrorMessage, sSeverity);
  sCurrentName   constant varchar2(4000) := $$PLSQL_UNIT||'.markDBJobAsBroken('||cnJobId||')';
begin
  isr$trace.stat('begin', 'begin', sCurrentName);
  markDBJobAsBroken(cnJobId, sHost, sPort, oErrorObj, removeJob);
  isr$trace.stat('end', 'end', sCurrentName);
end markDBJobAsBroken;

--**********************************************************************************
PROCEDURE setJobProgress(cnJobId IN NUMBER, nStatus IN NUMBER, sStatusText IN VARCHAR2, nProgress IN NUMBER)
IS
  PRAGMA autonomous_transaction;
  sCurrentName   constant varchar2(4000) := $$PLSQL_UNIT||'.setJobProgress('||cnJobid||',nStatus,sStatusText)';
  sReturn     VARCHAR2(4000);
BEGIN
  isr$trace.stat('begin', 'nProgress = ' || nProgress || '; ' || nStatus || '; ' || sStatusText,  sCurrentName);
  setLoaderVariable;

  UPDATE STB$JOBQUEUE
     set STATUS = nStatus,
         STATUSTEXT = sStatusText,
         PROGRESS = nProgress
   WHERE jobid = cnJobId;
  COMMIT;

  FOR rGetJobs IN ( SELECT DISTINCT ip, port
                        FROM (
                        SELECT ip, port
                                FROM STB$JOBSESSION
                               WHERE jobid = cnJobid
                              UNION
                              SELECT ip, port
                                FROM isr$session s, stb$jobqueue jq
                               WHERE UPPER(s.username) = UPPER(STB$SECURITY.getOracleUsername(jq.userno))
                                 AND jq.jobid = cnJobid
                                 AND NVL(inactive, 'F') = 'F'
                                 AND NVL(markedforshutdown, 'F') = 'F')
                       WHERE TRIM (ip) IS NOT NULL
                       AND TRIM (port) IS NOT NULL
                       AND NVL (nProgress, 0) != 0) LOOP
    begin
    sReturn := ISR$LOADER.showJobProgress(rGetJobs.ip, rGetJobs.port, sContext, sNamespace, cnJobId, sStatusText, nProgress, STB$UTIL.getSystemParameter('TIMEOUT_LOADER_VISUAL'));
    IF sReturn != 'T' THEN
        ISR$TRACE.warn('sReturn', sReturn, sCurrentName);
    END IF;
    exception
      when others then
      isr$trace.warn('error', sqlerrm, sCurrentName);
    end;
  END LOOP;

  isr$trace.stat('end', 'end',  sCurrentName);

END setJobProgress;

--==**********************************************************************************************************************************************==--
PROCEDURE setProgress(csStatusText IN VARCHAR2, cnJobid IN NUMBER) IS
  nStatus                STB$JOBQUEUE.STATUS%TYPE;
  nProgress              STB$JOBQUEUE.PROGRESS%TYPE := 0;
  sCurrentName   constant varchar2(4000) := $$PLSQL_UNIT||'.setProgress(csStatusText,'||cnJobid||')';

  CURSOR cGetCurProgress IS
    select PROGRESS
      from stb$jobqueue
     where jobid = cnJobid;
BEGIN
  isr$trace.stat('begin', 'begin', sCurrentName);

  isr$trace.debug('csStatusText', csStatusText, sCurrentName);

  nStatus     := Stb$typedef.cnReportProgress;

  OPEN cGetCurProgress;
  FETCH cGetCurProgress INTO nProgress;
  CLOSE cGetCurProgress;

  isr$trace.debug('nProgress', nProgress, sCurrentName);
  Stb$job.setJobProgress(cnJobid, nStatus, csStatusText, nProgress);

  isr$trace.stat('end', 'end', sCurrentName);

END setProgress;

--==**********************************************************************************************************************************************==--
PROCEDURE setProgress(cnValue IN NUMBER, csStatusText IN VARCHAR2, cnJobid IN NUMBER ) IS
  nStatus                STB$JOBQUEUE.STATUS%TYPE;
  nProgress              STB$JOBQUEUE.PROGRESS%TYPE := 0;
  sCurrentName   constant varchar2(4000) := $$PLSQL_UNIT||'.setProgress(cnValue,csStatusText,'||cnJobid||')';
  nStartProgress NUMBER := STB$JOB.getJobParameter('STARTPROGRESS',cnJobId)+1;
  nEndProgress   NUMBER := STB$JOB.getJobParameter('ENDPROGRESS',cnJobId)-1;
BEGIN
  isr$trace.stat('begin', 'begin', sCurrentName);

  isr$trace.debug('cnValue',  cnValue, sCurrentName);
  isr$trace.debug('csStatusText', csStatusText, sCurrentName);

  nStatus     := Stb$typedef.cnReportProgress;

  nProgress   := Round( nStartProgress + (cnValue * ((nEndProgress - nStartProgress) / 100)));
  isr$trace.debug('nProgress',  nProgress, sCurrentName);
  Stb$job.setJobProgress(cnJobid, nStatus, csStatusText, nProgress);

  isr$trace.stat('end', 'end', sCurrentName);

END setProgress;

--==**********************************************************************************************************************************************==--
FUNCTION isRunning(csJobName IN VARCHAR2) RETURN VARCHAR2 IS
  sIsRunning VARCHAR2(1) := 'F';
  sCurrentName   constant varchar2(4000) := $$PLSQL_UNIT||'.isRunning('||csJobName||')';

  CURSOR csIsRunning IS
    SELECT 'T'
      FROM user_scheduler_jobs
     WHERE state = 'RUNNING'
       AND job_name = csJobName;
BEGIN
  isr$trace.stat('begin',csJobName, sCurrentName);
  OPEN csIsRunning;
  FETCH csIsRunning INTO sIsRunning;
  IF csIsRunning%NOTFOUND THEN
    sIsRunning := 'F';
  END IF;
  CLOSE csIsRunning;
  isr$trace.stat('end',csJobName||' '||sIsRunning, sCurrentName);
  RETURN sIsRunning;
END;

--==**********************************************************************************************************************************************==--
FUNCTION modifyJob(cnJobId IN NUMBER) RETURN STB$OERROR$RECORD
IS
  sCurrentName   constant varchar2(4000) := $$PLSQL_UNIT||'.modifyJob('||cnJobId||')';
  oErrorObj       STB$OERROR$RECORD := STB$OERROR$RECORD();
  sRepeatInterval VARCHAR2(4000);

  sIteration      VARCHAR2(4000) := NVL(TRIM(STB$JOB.getJobParameter('FILTER_ITERATION', cnJobId)), 'ONETIME');
  sDirectStart    VARCHAR2(4000) := NVL(TRIM(STB$JOB.getJobParameter('FILTER_DIRECT_START', cnJobId)), 'F');
  sInactive       VARCHAR2(4000) := NVL(TRIM(STB$JOB.getJobParameter('FILTER_INACTIVE', cnJobId)), 'F');

  sInterval       VARCHAR2(4000) := TRIM(STB$JOB.getJobParameter('FILTER_INTERVAL', cnJobId));
  sJobBy          VARCHAR2(4000) := TRIM(STB$JOB.getJobParameter('FILTER_JOBBY', cnJobId));
  sStart          VARCHAR2(4000) := TRIM(STB$JOB.getJobParameter('FILTER_START', cnJobId));
  sEnd            VARCHAR2(4000) := TRIM(STB$JOB.getJobParameter('FILTER_END', cnJobId));

BEGIN
  isr$trace.stat('begin', 'begin', sCurrentName);

  sRepeatInterval := TRIM(case
                       when sIteration = 'ONETIME' then ''
                       when sInterval IS NULL then 'freq='||sIteration||sJobBy
                       else 'freq='||sIteration||';interval='||sInterval||sJobBy
                     end);
  isr$trace.debug('sRepeatInterval',sRepeatInterval, sCurrentName);

  IF sRepeatInterval is null THEN
    dbms_scheduler.set_attribute_null (name => 'JOB_' || cnJobId,
                                           attribute => 'repeat_interval');
  ELSE
    dbms_scheduler.set_attribute (name => 'JOB_' || cnJobId,
                                      attribute => 'repeat_interval',
                                      value => sRepeatInterval);
  END IF;
  isr$trace.debug('after sRepeatInterval','', sCurrentName);

  --EXECUTE IMMEDIATE 'ALTER SESSION SET time_zone = dbtimezone';
  EXECUTE IMMEDIATE 'ALTER SESSION SET time_zone = '''||EXTRACT(TIMEZONE_HOUR FROM systimestamp)||':'||EXTRACT(TIMEZONE_MINUTE FROM systimestamp)||'''';
  isr$trace.debug('session altered','ALTER SESSION SET time_zone = '''||EXTRACT(TIMEZONE_HOUR FROM systimestamp)||':'||EXTRACT(TIMEZONE_MINUTE FROM systimestamp)||'''', sCurrentName);

  IF sStart IS NULL OR sIteration = 'ONETIME' AND sDirectStart = 'T' THEN
    dbms_scheduler.set_attribute_null (name => 'JOB_' || cnJobId,
                                           attribute => 'start_date');
  ELSE
    dbms_scheduler.set_attribute (name => 'JOB_' || cnJobId,
                                      attribute => 'start_date',
                                      value => CAST(TO_DATE(sStart, STB$UTIL.getSystemParameter('DATEFORMAT')||' HH24:MI') AS TIMESTAMP));
  END IF;
  isr$trace.debug('after startdate','', sCurrentName);

  IF sEnd IS NULL OR sIteration = 'ONETIME' THEN
    dbms_scheduler.set_attribute_null (name => 'JOB_' || cnJobId,
                                       attribute => 'end_date');
  ELSE
    dbms_scheduler.set_attribute (name => 'JOB_' || cnJobId,
                                      attribute => 'end_date',
                                      value => CAST(TO_DATE(sEnd, STB$UTIL.getSystemParameter('DATEFORMAT')||' HH24:MI') AS TIMESTAMP));
  END IF;
  isr$trace.debug('after enddate','', sCurrentName);

  IF sInactive = 'F' THEN
    DBMS_SCHEDULER.ENABLE (name => 'JOB_' || cnJobId);
    STB$JOB.setJobParameter('JOB_ERROR', null, cnJobId);
  ELSE
    IF isRunning('JOB_' || cnJobId) = 'T' THEN
      DBMS_SCHEDULER.STOP_JOB(job_name => 'JOB_' || cnJobId, force => TRUE);
    END IF;
    DBMS_SCHEDULER.DISABLE (name => 'JOB_' || cnJobId);
  END IF;
  isr$trace.debug('after en-/disable','', sCurrentName);

  update stb$jobqueue
     set submittime = TO_DATE(NVL(sStart, to_char(systimestamp, STB$UTIL.getSystemParameter('DATEFORMAT')||' HH24:MI')), STB$UTIL.getSystemParameter('DATEFORMAT')||' HH24:MI')
   where jobid = cnJobId;

  COMMIT;

  isr$trace.debug('after jobqueue update','', sCurrentName);

  IF sDirectStart = 'T' THEN
    DBMS_SCHEDULER.RUN_JOB(job_name => 'JOB_' || cnJobId, use_current_session => false);
  END IF;

  isr$trace.stat('end', 'end', sCurrentName);
  RETURN oErrorObj;

EXCEPTION
  WHEN OTHERS THEN
      oErrorObj.handleError ( Stb$typedef.cnSeverityCritical, utd$msglib.getmsg ('exUnexpectedError', stb$security.getCurrentlanguage,  csP1 => sqlerrm(sqlcode)), 
        sCurrentName, dbms_utility.format_error_stack ||chr(10)|| dbms_utility.format_error_backtrace );
    ROLLBACK;
  RETURN oErrorObj;
END modifyJob;

--******************************************************************************
PROCEDURE waitForRunningDocumentJobs(cnReporttypeId IN NUMBER, csParameter IN VARCHAR2, cnJobId IN NUMBER)
IS
  sCurrentName   constant varchar2(4000) := $$PLSQL_UNIT||'.waitForRunningDocumentJobs('||cnReporttypeId||')';
  nCntJobs  NUMBER := 0;

  CURSOR cGetRunningJobs IS
    SELECT count(*)
      FROM user_scheduler_jobs
     WHERE comments = csParameter||'_'||to_char(cnReporttypeId)
       AND job_name like 'JOB_%'
       AND NVL(STB$JOB.getJobParameter('WAITING_FOR_DOCUMENT', REPLACE(job_name, 'JOB_')), 'T') = 'F';
BEGIN
  isr$trace.stat('begin', 'begin', sCurrentName);
  isr$trace.debug('cnReporttypeId',cnReporttypeId, sCurrentName);
  isr$trace.debug('csParameter',csParameter, sCurrentName);
  isr$trace.debug('cnJobId',cnJobId, sCurrentName);

  STB$JOB.setJobParameter('WAITING_FOR_DOCUMENT', 'F', cnJobId);

  OPEN cGetRunningJobs;
  FETCH cGetRunningJobs INTO nCntJobs;
  IF cGetRunningJobs%NOTFOUND THEN
    nCntJobs := 0;
  END IF;
  CLOSE cGetRunningJobs;
  isr$trace.debug('nCntJobs',nCntJobs,sCurrentName);
  WHILE nCntJobs >= STB$UTIL.getReptypeParameter(cnReporttypeId, csParameter) LOOP
    isr$trace.debug('Waiting for document','Waiting for document ...',sCurrentName);
    STB$JOB.setJobParameter('WAITING_FOR_DOCUMENT', 'T', cnJobId);
    -- cnt jobs until a job place is available
    OPEN cGetRunningJobs;
    FETCH cGetRunningJobs INTO nCntJobs;
    IF cGetRunningJobs%NOTFOUND THEN
      nCntJobs := 0;
    END IF;
    CLOSE cGetRunningJobs;
    isr$trace.debug('nCntJobs',nCntJobs,sCurrentName);
  END LOOP;

  STB$JOB.setJobParameter('WAITING_FOR_DOCUMENT', 'F', cnJobId);

  isr$trace.stat('end', 'end', sCurrentName);

END waitForRunningDocumentJobs;

--******************************************************************************
PROCEDURE waitForRunningISRJobs(cnJobId IN NUMBER)
IS
  sCurrentName   constant varchar2(4000) := $$PLSQL_UNIT||'.waitForRunningISRJobs('||cnJobId||')';
  nCntJobs  NUMBER := 0;


  CURSOR cGetRunningISRJobs IS
    SELECT count(*)
      FROM user_scheduler_jobs
     WHERE job_name like 'JOB_%'
       AND NVL(STB$JOB.getJobParameter('WAITING', REPLACE(job_name, 'JOB_')), 'T') = 'F';
BEGIN
  isr$trace.stat('begin', 'begin jobid ' || cnJobId, sCurrentName);

  STB$JOB.setJobParameter('WAITING', 'T', cnJobId);

  OPEN cGetRunningISRJobs;
  FETCH cGetRunningISRJobs INTO nCntJobs;
  IF cGetRunningISRJobs%NOTFOUND THEN
    nCntJobs := 0;
  END IF;
  CLOSE cGetRunningISRJobs;

  WHILE nCntJobs >= STB$UTIL.getSystemParameter('MAX_JOBS') LOOP
    isr$trace.debug('Waiting jobid ' || cnJobId,nCntJobs,sCurrentName);
    STB$JOB.setJobParameter('WAITING', 'T', cnJobId);

    stb$java.SLEEPIMPL (round(dbms_random.value(40000,70000)));
  
    OPEN cGetRunningISRJobs;
    FETCH cGetRunningISRJobs INTO nCntJobs;
    IF cGetRunningISRJobs%NOTFOUND THEN
      nCntJobs := 0;
    END IF;
    CLOSE cGetRunningISRJobs;
    if nCntJobs < STB$UTIL.getSystemParameter('MAX_JOBS') then
      STB$JOB.setJobParameter('WAITING', 'F', cnJobId);   
    end if;
    isr$trace.info('After waiting ',nCntJobs,sCurrentName);
  END LOOP;

  STB$JOB.setJobParameter('WAITING', 'F', cnJobId);

  isr$trace.stat('end', 'end', sCurrentName);

END waitForRunningISRJobs;

--******************************************************************************
function isJobSession(cnSessionId in number)
  return varchar2
is
  sCurrentName   constant varchar2(4000) := $$PLSQL_UNIT||'.isJobSession('||cnSessionId||')';
  sReturnValue varchar2(1);

  cursor curIsJobSession(cnCursorSessionId number) is
    select 'T'
    from   STB$JOBSESSION
    where  oracleJobSessionId = cnCursorSessionId;
begin
   -- [ISRC-672] do not use logging in this function, because getJobId is called from ISR$TRACE
  open  curIsJobSession(cnSessionId);
  fetch curIsJobSession into sReturnValue;
  if curIsJobSession%NOTFOUND then
    sReturnValue := 'F';
  end if;
  close curIsJobSession;

  return sReturnValue;
end isJobSession;

--******************************************************************************
function getJobId( cnSessionId in number default userenv('SESSIONID') )
  return number
is
  sCurrentName   constant varchar2(4000) := $$PLSQL_UNIT||'.getJobId('||cnSessionId||')';
  nReturnJobid number;
begin
 -- [ISRC-672] do not use logging in this function, because getJobId is used from ISR$TRACE
  select jobid
  into   nReturnJobid
  from   stb$jobsession
  where  oracleJobSessionId = cnSessionId;

  return nReturnJobid;
exception
  when others then
    return null;
end getJobId;

--***********************************************************************************************************************************
procedure dropJob(cnJobid in number)
is
  sCurrentName   constant varchar2(4000) := $$PLSQL_UNIT||'.dropJob('||cnJobid||')';
  oErrorObj       STB$OERROR$RECORD := STB$OERROR$RECORD();
  sUserMsg  varchar2(4000);

begin
  isr$trace.stat('begin', 'begin', sCurrentName);

    delete from STB$JOBTRAIL where jobid = cnJobid;
    delete from STB$JOBPARAMETER where jobid = cnJobid;
    delete from STB$JOBSESSION where jobid = cnJobid;
    delete from STB$JOBQUEUE where jobid = cnJobid;
  commit;
  isr$trace.stat('end', 'end', sCurrentName);
exception
  when others then
    sUserMsg := utd$msglib.getmsg ('exUnexpectedError', nCurrentLanguage,  csP1 => sqlerrm(sqlcode));
    oErrorObj.handleError( Stb$typedef.cnSeverityCritical, sUserMsg, sCurrentName, dbms_utility.format_error_stack ||crlf|| dbms_utility.format_error_backtrace );
end dropJob;

--***********************************************************************************************************************************
procedure removeJobFolder(cnJobid in number, csHost in varchar2, csPort in varchar2)
is
  sCurrentName   constant varchar2(300) := $$PLSQL_UNIT||'.removeJobFolder('||cnJobid||', '||csHost||','||csPort||')';
  oErrorObj       STB$OERROR$RECORD := STB$OERROR$RECORD();
  sUserMsg  varchar2(4000);
  sReturn  varchar2(4000);

begin
  isr$trace.stat('begin', 'begin', sCurrentName);
  sReturn := isr$loader.removeJobFolder( csHost, csPort, STB$UTIL.getSystemParameter('LOADER_CONTEXT'), 
      STB$UTIL.getSystemParameter('LOADER_NAMESPACE'), cnJobid, STB$UTIL.getSystemParameter('TIMEOUT_LOADER_THREAD'));
  if sReturn != 'T' then
    isr$trace.error('sReturn', sReturn, sCurrentName);
  end if;
  isr$trace.stat('end', 'end', sCurrentName);
exception
  when others then
    sUserMsg := utd$msglib.getmsg ('exUnexpectedError', nCurrentLanguage,  csP1 => sqlerrm(sqlcode));
    oErrorObj.handleError( Stb$typedef.cnSeverityCritical, sUserMsg, sCurrentName, dbms_utility.format_error_stack ||crlf|| dbms_utility.format_error_backtrace );
end removeJobFolder; 

--***********************************************************************************************************************************
FUNCTION sendMail(csRecipient in varchar2,cnJobid IN NUMBER,csNotify_on in varchar2  default null) RETURN STB$OERROR$RECORD
IS
  sCurrentName   constant varchar2(4000) := $$PLSQL_UNIT||'.sendMail('||cnJobid||')';
  oErrorObj               STB$OERROR$RECORD := STB$OERROR$RECORD();
  olFileList            ISR$FILE$LIST;
  
  sUserMsg  varchar2(4000);
  
  cursor cGetText is
    select STB$JOB.GETJOBPARAMETER ('TOKEN_DESC', cnJobid)
               || ( select case
                              when TRIM (srnumber) is not null then ': ' || srnumber
                           end
                           || case
                                 when TRIM (title) is not null then ' -> ' || title
                              end
                           || case
                                 when TRIM (srnumber) is not null then ' ' || version
                              end
                    from   stb$report
                    where  repid = STB$JOB.getjobparameter ('REPID', cnJobid) )  ssubject,
           STB$JOB.GETJOBPARAMETER ('MAIL_DESC', cnJobid)                        stext
      from dual;
      sSubject VARCHAR2(4000);                      
      sText    VARCHAR2(4000);

begin

      isr$trace.stat('begin', 'begin', sCurrentName);
      open  cGetText;
      fetch cGetText into sSubject, sText;
      close cGetText;
      oErrorObj := STB$OERROR$RECORD();
      isr$trace.debug('variables','sSubject: '||sSubject||',  sText: '||sText, sCurrentName);
      olFileList := ISR$FILE$LIST();

      for rGetDoc in (select filename,mimetype, binaryfile,jobid
                    from stb$jobtrail 
                    where jobid=cnJobid and ( 
                     documenttype = 'D'
            or documenttype = 'TARGET'
            or documenttype = 'CONFIG'
            or documenttype = 'ROHREPORT') ) loop   
        olFileList.extend();
        olFileList(olFileList.count) := ISR$FILE$REC(rGetDoc.filename, rGetDoc.mimetype, rGetDoc.binaryfile);
      end loop;
      if csRecipient='NOTIFY' then
      oErrorObj := isr$mail.SendEmail(STB$JOB.getJobParameter('TOKEN', cnJobId),STB$JOB.getJobParameter('NODETYPE', cnJobId),cnJobid,csNotify_on,olFileList);
      else
      oErrorObj := ISR$MAIL.SENDMAIL(csRecipient ,sSubject ,sText,olFileList );
      end if;
      RETURN oErrorObj;
      isr$trace.stat('end', 'end', sCurrentName);
exception
  when others then
    sUserMsg := utd$msglib.getmsg ('exUnexpectedError', '',  csP1 => sqlerrm(sqlcode));
    oErrorObj.handleError( Stb$typedef.cnSeverityCritical, sUserMsg, sCurrentName, dbms_utility.format_error_stack ||crlf|| dbms_utility.format_error_backtrace );
  RETURN oErrorObj;

end sendMail;


end STB$job;