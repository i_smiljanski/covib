CREATE OR REPLACE PACKAGE BODY STB$USERSECURITY IS
  -- global variable to store the values of the currrent node"
  oCurrentParameter             STB$PROPERTY$LIST;
  
  --****************************************************************************************************
  FUNCTION Createsyn (sRolename IN VARCHAR2, sNewuser IN VARCHAR2, sISROwner IN VARCHAR2)
    RETURN VARCHAR2 
  IS
    PRAGMA AUTONOMOUS_TRANSACTION;
    sReturn                       VARCHAR2 (4000);
    Sexists                       VARCHAR2 (1) := 'F';
    sCurrentName  constant varchar2(100) := $$PLSQL_UNIT||'.Createsyn'; 

    CURSOR Chassynonym IS
      SELECT 'T'
      FROM   all_synonyms
      WHERE  owner = UPPER(sNewuser)
      AND    table_owner = UPPER(sISROwner)
      AND    synonym_name = 'CHANGEPASSWORD';

    CURSOR Syn_cur IS
      SELECT owner,
             table_name
      FROM   user_tab_privs
      WHERE  grantee = UPPER(sRolename)
      AND    owner = UPPER(sISROwner)
      MINUS
      SELECT table_owner,
             synonym_name
      FROM   all_synonyms
      WHERE  owner = UPPER(sNewuser)
      AND    table_owner = UPPER(sISROwner);

    Retval                        NUMBER;
    Sqlstr                        VARCHAR2 (2000);
    Tcursor                       PLS_INTEGER;
  BEGIN
    isr$trace.stat('begin', 'begin', sCurrentName);
    IF TRIM(sNewuser) IS NOT NULL THEN
    
      OPEN cHasSynonym;
      FETCH cHasSynonym
      INTO  sExists;
      IF cHasSynonym%NOTFOUND THEN
        sExists := 'F';
      END IF;
      CLOSE cHasSynonym;
      
      isr$trace.debug ('sExists '||sRolename,  sExists, sCurrentName);

      IF sExists = 'F' THEN
        begin
          -- create the synonym for the paassword check
          sqlStr := 'CREATE SYNONYM ' || sNewuser || '.CHANGEPASSWORD for ' || sISROwner || '.CHANGEPASSWORD';
          isr$trace.debug ('sqlStr',  sqlStr, sCurrentName);
          EXECUTE IMMEDIATE Sqlstr;
        exception
          when others then
            isr$trace.error ('error',  sqlStr, SQLCODE);
        end;
      END IF;

      -- the other synonyms granted the role
      FOR Syn_rec IN Syn_cur LOOP
        sqlStr := 'CREATE SYNONYM ' || sNewuser || '.' || Syn_rec.Table_name || ' FOR ' || Syn_rec.Owner || '.' || Syn_rec.Table_name;
        isr$trace.info ('sqlStr',  sqlStr, sCurrentName);
        tCursor := Dbms_sql.Open_cursor;
        Dbms_sql.Parse (tCursor, sqlStr, Dbms_sql.Native);
        Retval := Dbms_sql.EXECUTE (Tcursor);
        Dbms_sql.Close_cursor (Tcursor);
      END LOOP;
      
    END IF;
    
    isr$trace.stat('end', 'end', sCurrentName);
    RETURN sReturn;
  EXCEPTION
    WHEN OTHERS THEN
      sReturn := SQLERRM;
      RETURN sReturn;
  END Createsyn;
  
  --*************************************************************************************************************************************
  function putUserParameters( olParameter in STB$PROPERTY$LIST, 
                              sToken      in varchar2 )
    return Stb$oerror$record
  is
    oErrorObj                     Stb$oerror$record := STB$OERROR$RECORD();
    sCurrentName                  constant varchar2(100) := $$PLSQL_UNIT||'.putUserParameters('||sToken||')';
    nLineindex                    NUMBER;
    Scomp1                        VARCHAR2 (4000);
    Scomp1a                       VARCHAR2 (4000);
    Sselect                       VARCHAR2 (4000);
    nModified                     NUMBER := 0;
    sUserNo                       VARCHAR2 (1000);
    
    CURSOR cGetUserCols(csParameter IN VARCHAR2, csTable IN VARCHAR2) IS
      select 'T'
        from user_tab_cols
       where column_name = UPPER(csParameter)
         and table_name = UPPER(csTable);
    
    rGetUserCols cGetUserCols%rowtype;
  BEGIN
    isr$trace.stat('begin', 'olParameter, see column LOGCLOB', sCurrentname);
    
    IF sToken = 'CAN_MODIFY_OWN_USER_DATA' THEN
      sUserNo := TO_CHAR (Stb$security.Getcurrentuser);
    ELSE
      sUserNo := Stb$object.getCurrentNodeId;
    END IF;
    
    oErrorObj := Stb$audit.Openaudit ('USERAUDIT', sUserNo);
    oErrorObj := stb$audit.addauditrecord ( sToken );    
        
    nLineindex := olParameter.FIRST;

    WHILE (olParameter.EXISTS (nLineindex)) LOOP
      IF olparameter(nlineindex).sEditable = 'T' AND olParameter (nLineindex).type NOT IN ('TABSHEET', 'GROUP') THEN
        Scomp1 := NVL (STB$UTIL.getProperty(oCurrentParameter, olParameter (nLineindex).sParameter), '');
        Scomp1a := NVL (olParameter (nLineindex).sValue, '');
        if olParameter (nLineindex).haslov = 'T' then
          Scomp1 := NVL (STB$UTIL.getProperty(oCurrentParameter, olParameter (nLineindex).sParameter, 'SDISPLAY'), '');
          Scomp1a := NVL (olParameter (nLineindex).sDisplay, '');
        end if;
        isr$trace.debug (nLineindex,  nlineindex||' '||olparameter (nlineindex).sParameter||' '||scomp1||' / '||scomp1a, sCurrentName);
        IF (TRIM (Scomp1) != TRIM (Scomp1a)) 
        OR (TRIM (Scomp1) IS NULL AND TRIM (Scomp1a) IS NOT NULL) 
        OR (TRIM (Scomp1) IS NOT NULL AND TRIM (Scomp1a) IS NULL) THEN
          nModified := nModified + 1;
          oErrorObj := Stb$audit.Addauditentry (olParameter (nLineindex).sParameter, sComp1, sComp1a, nModified);
        END IF;
        -- update the tables even if no differences in the object are
        -- the user has chosen the default values        
        OPEN cGetUserCols(olParameter (nLineindex).sParameter, 'STB$USER');
        FETCH cGetUserCols INTO rGetUserCols;
        IF cGetUserCols%NOTFOUND THEN
          sselect :=
               ' update stb$userparameter'
            || ' set parametervalue = '''||REPLACE(olParameter (nLineindex).sValue, '''', '''''')||'''' 
            || ' where userno = '||sUserNo 
            || ' and parametername = '''||olParameter (nLineindex).sParameter||'''';
                  
        ELSE
          sselect :=
               ' update stb$user '
            || ' set '||olParameter (nLineindex).sParameter||' = '''||REPLACE(olParameter (nLineindex).sValue, '''', '''''')
            || ''' , modifiedon = '''||SYSDATE
            || ''' , modifiedby = '''||STB$SECURITY.getOracleUserName(Stb$security.Getcurrentuser)
            || ''' where  userno = '||sUserNo;
        END IF;
        CLOSE cGetUserCols;                
        isr$trace.debug ('sSelect', sSelect, sCurrentName);
        -- execute statement
        EXECUTE IMMEDIATE Sselect;        
      
        Scomp1 := '';
        Scomp1a := '';
        Sselect := '';
      END IF;
      nLineindex := olParameter.NEXT (nLineindex);
    END LOOP;

    isr$trace.stat('end', 'end', sCurrentName);
    RETURN (oErrorObj);
  EXCEPTION
    WHEN OTHERS THEN
      ROLLBACK;
      oErrorObj.handleError ( Stb$typedef.cnSeverityMessage, utd$msglib.getmsg ('exUnexpectedError', stb$security.getCurrentlanguage,  csP1 => sqlerrm(sqlcode)), 
        sCurrentName, dbms_utility.format_error_stack ||chr(10)|| dbms_utility.format_error_backtrace );
      RETURN (oErrorObj);
  END Putuserparameters;

  --*************************************************************************************************************************************
  FUNCTION PutMembers (olParameter IN STB$PROPERTY$LIST, sToken IN VARCHAR2)
    RETURN Stb$oerror$record IS
    sCurrentName   constant varchar2(4000) := $$PLSQL_UNIT||'.PutMembers('||sToken||')';
    oErrorObj       Stb$oerror$record := STB$OERROR$RECORD();
    oCurNode        Stb$treenode$record := STB$TREENODE$RECORD();
    nModified       NUMBER := 1;
    sUserGroupNo    VARCHAR2(100);
    nUserGroupNo    NUMBER;
    sUserNo         VARCHAR2(100);
    nUserNo         NUMBER;
  BEGIN
    isr$trace.stat('begin', 'begin', sCurrentName);
    -- get the current node
    oCurNode := Stb$object.Getcurrentnode;

    sUserGroupNo := case when Stb$object.getNodeIdForNodetype('FUNCTIONGROUP') = 'NOT_FOUND' then Stb$object.getNodeIdForNodetype('DATAGROUP') else Stb$object.getNodeIdForNodetype('FUNCTIONGROUP') end;
    isr$trace.debug ('sUserGroupNo',  sUserGroupNo, sCurrentName);
    sUserNo := Stb$object.getcurrentNodeId;
    isr$trace.debug ('sUserNo',  sUserNo, sCurrentName);

    oErrorObj := Stb$audit.Openaudit ('GROUPAUDIT', sUserGroupNo);
    oErrorObj := Stb$audit.Addauditrecord ( sToken );

    IF oCurNode.tlopropertylist (1).sValue = 'F' THEN
      -- member is added
      isr$trace.debug ('add member', sUserNo, sCurrentName);
      oErrorObj := Stb$audit.Addauditentry (STB$UTIL.getProperty(olParameter, 'FULLNAME'), 'T', 'F', nModified);
      INSERT INTO Stb$userlink
                  (Entryid, Usergroupno, Userno)
           VALUES (Seq$stb$userlink.NEXTVAL, TO_NUMBER (sUserGroupNo), TO_NUMBER (sUserNo));
    ELSE
      isr$trace.debug ('remove member',  sUserNo, sCurrentName);
      oErrorObj := Stb$audit.Addauditentry (STB$UTIL.getProperty(olParameter, 'FULLNAME'), 'F', 'T', nModified);
      -- remove the member
      DELETE FROM Stb$userlink
            WHERE Usergroupno = TO_NUMBER (sUserGroupNo) 
              AND Userno = TO_NUMBER (sUserNo);
    END IF;
    
    nUserNo := TO_NUMBER (sUserNo);
    nUserGroupNo := TO_NUMBER (sUserGroupNo);
    
    -- ISRC-1044 removed

    isr$trace.stat('end', 'end', sCurrentName);
    RETURN (oErrorObj);
  EXCEPTION
    WHEN OTHERS THEN
      ROLLBACK;
      oErrorObj.handleError ( Stb$typedef.cnSeverityMessage, utd$msglib.getmsg ('exUnexpectedError', stb$security.getCurrentlanguage,  csP1 => sqlerrm(sqlcode)), 
        sCurrentName, dbms_utility.format_error_stack ||chr(10)|| dbms_utility.format_error_backtrace );
      RETURN (oErrorObj);
  END Putmembers;

  --*************************************************************************************************************************************
  FUNCTION putGroupParameters (olParameter IN STB$PROPERTY$LIST, sToken IN VARCHAR2)
    RETURN Stb$oerror$record IS
    sCurrentName   constant varchar2(4000) := $$PLSQL_UNIT||'.putGroupParameters('||sToken||')';
    oErrorObj                     Stb$oerror$record := STB$OERROR$RECORD();
    nLineindex                    NUMBER := 0;
    Scomp1                        VARCHAR2 (4000);
    Scomp1a                       VARCHAR2 (4000);
    Sselect                       VARCHAR2 (4000);
    nModified                     NUMBER := 0;
    sNodename                     Isr$custom$grouping.Customtext%TYPE;
    sGroupName                    Stb$usergroup.groupname%TYPE;
    sOldNodename                  Isr$custom$grouping.Customtext%TYPE;
    sOldGroupName                 Stb$usergroup.groupname%TYPE;

    CURSOR cGetgroupdescription (Cnusergroupno IN NUMBER) IS
      SELECT groupname, description
      FROM   Stb$usergroup
      WHERE  Usergroupno = Cnusergroupno;
  BEGIN
    isr$trace.stat('begin', 'begin', sCurrentName);
    
    OPEN cGetgroupdescription (Stb$object.getCurrentNodeId);
    FETCH cGetgroupdescription
    INTO  sOldGroupName, sOldNodename;
    CLOSE cGetgroupdescription;    
      
    oErrorObj := Stb$audit.Openaudit ('GROUPAUDIT', Stb$object.getCurrentNodeId);
    oErrorObj := stb$audit.addauditrecord ( sToken );
    
    nLineindex := olParameter.FIRST;
    
    WHILE (olparameter.EXISTS (nlineindex)) LOOP
      IF olparameter(nlineindex).sEditable = 'T' AND olParameter (nLineindex).type NOT IN ('TABSHEET', 'GROUP') THEN
        scomp1 := nvl (STB$UTIL.getProperty(oCurrentParameter, olParameter (nLineindex).sParameter), '');
        scomp1a := nvl (olparameter (nlineindex).svalue, '');
        isr$trace.debug ('nLineIndex',  nlineindex||' '||olparameter (nlineindex).sParameter||' '||scomp1||' / '||scomp1a, sCurrentName);
        IF (trim (scomp1) != trim (scomp1a)) 
        OR (trim (scomp1) IS NULL AND trim (scomp1a) IS NOT NULL) 
        OR (trim (scomp1) IS NOT NULL AND trim (scomp1a) IS NULL) THEN
          nmodified := nmodified + 1;
          oerrorobj := stb$audit.addauditentry (olparameter (nlineindex).sparameter, sComp1, sComp1A, nmodified);
          Sselect :=
               ' update stb$usergroup '
            || ' set '
            || olParameter (nLineindex).sParameter
            || ' = '''
            || REPLACE(olParameter (nLineindex).sValue, '''', '''''')
            || ''' , modifiedon = '''
            || SYSDATE
            || ''' , modifiedby = '''
            || Stb$security.getOracleUserName(Stb$security.getCurrentUser)
            || ''' where  usergroupno = '
            || Stb$object.getCurrentNodeId;
          isr$trace.debug ('sSelect',  Sselect, sCurrentName);
          -- execute statement
          EXECUTE IMMEDIATE Sselect;
        END IF;
      END IF;
      nlineindex := olparameter.NEXT (nlineindex);
      scomp1 := '';
      scomp1a := '';
    END LOOP; 
    
    OPEN cGetgroupdescription (Stb$object.getCurrentNodeId);
    FETCH cGetgroupdescription
    INTO  sGroupName, sNodename;
    CLOSE cGetgroupdescription;

    UPDATE isr$custom$grouping
       SET Customtext = sNodename
     WHERE Referencetype = 'DATAGROUP'
       AND REFERENCE = Stb$object.getCurrentNodeId
       AND Customtext != sNodename;

    -- ISRC-1044
    UPDATE ISR$REPORT$GROUPING$TABLE
       SET groupingid = sGroupName, groupingvalue = sGroupName
     WHERE groupingname IN ('GRP$DATAGROUP', 'DATAGROUP')
       AND groupingid = sOldGroupName
       AND groupingid != sGroupName;       

    isr$trace.stat('end', 'end', sCurrentName);
    RETURN (oErrorObj);
  EXCEPTION
    WHEN OTHERS THEN
      ROLLBACK;
      oErrorObj.handleError ( Stb$typedef.cnSeverityMessage, utd$msglib.getmsg ('exUnexpectedError', stb$security.getCurrentlanguage,  csP1 => sqlerrm(sqlcode)), 
        sCurrentName, dbms_utility.format_error_stack ||chr(10)|| dbms_utility.format_error_backtrace );
      RETURN (oErrorObj);
  END Putgroupparameters;

  --**********************************************************************************************************************************
  PROCEDURE Destroyoldvalues IS
  BEGIN
    STB$OBJECT.setCurrentToken('');
    oCurrentParameter := STB$PROPERTY$LIST ();
  END Destroyoldvalues;

  --*********************************************************************************************************************************
  function callUserMasterData( olParameter out STB$PROPERTY$LIST, 
                               sToken      in  varchar2 )
    return Stb$oerror$record
  is
    oErrorObj                     Stb$oerror$record := STB$OERROR$RECORD();
    sCurrentName                  constant varchar2(100) := $$PLSQL_UNIT||'.callUserMasterData('||sToken||')';
    sUserNo                       STB$USER.USERNO%TYPE;
    sNewToken                     VARCHAR2(500);
    tloPropertylist1              STB$PROPERTY$LIST := STB$PROPERTY$LIST();
    sParameterAllowed             VARCHAR2(500);
  begin
    isr$trace.stat('begin', 'sToken: '||sToken, 'begin');
    
    sNewToken := sToken;

    IF sToken = 'CAN_MODIFY_OWN_USER_DATA' THEN
      sUserNo := to_char (Stb$security.Getcurrentuser);
    ELSIF sToken = 'CAN_ADD_USER_WITH_DB' THEN
      sUserNo := Stb$object.getCurrentNodeId;
      sNewToken := 'CAN_ADD_USER';
    ELSE
      sUserNo := Stb$object.getCurrentNodeId;
    END IF;
    
    oErrorObj := STB$UTIL.getPropertyList(sNewToken, olParameter);    
    
    --for [ISRC-904] Add group authorization for access to user properties mask
    -- check the GroupRight before adding properties
    sParameterAllowed := Stb$security.checkGroupRight ('CAN_MODIFY_USER_PARAMETER'); 
    
    -- debug only if investigating behaviour 
      -- isr$trace.debug('olParameter', 'olParameter, see column LOGCLOB', sCurrentName, olParameter);
    IF sParameterAllowed = 'T' THEN  
      oErrorObj := STB$UTIL.getPropertyList('CAN_MODIFY_USER_PARAMETER', tloPropertylist1);
            
      FOR i IN 1..tloPropertylist1.COUNT() LOOP
        olParameter.EXTEND;
        olParameter(olParameter.COUNT) := tloPropertylist1(i);
      END LOOP;
      
      -- debug only if investigating behaviour 
        -- isr$trace.debug('olParameter after Parameter', 'olParameter, see column LOGCLOB', sCurrentName, olParameter);
    END IF;    
       
    isr$trace.stat('end', 'end', sCurrentName);
    return oErrorObj;
  EXCEPTION
    WHEN OTHERS THEN
      oErrorObj.handleError ( Stb$typedef.cnSeverityCritical, utd$msglib.getmsg ('exUnexpectedError', stb$security.getCurrentlanguage,  csP1 => sqlerrm(sqlcode)), 
        sCurrentName, dbms_utility.format_error_stack ||chr(10)|| dbms_utility.format_error_backtrace );
      RETURN oErrorObj;
  END callUserMasterData;

  --*********************************************************************************************************************************
  FUNCTION callGroupMasterData (olParameter OUT STB$PROPERTY$LIST, sToken IN VARCHAR2)
    RETURN Stb$oerror$record IS
    sCurrentName   constant varchar2(4000) := $$PLSQL_UNIT||'.callGroupMasterData('||sToken||')';
    oErrorObj                     Stb$oerror$record := STB$OERROR$RECORD();
  BEGIN
    isr$trace.info('begin', 'begin', sCurrentName);

    oErrorObj := STB$UTIL.getPropertyList(sToken, olParameter);    

    isr$trace.info('end', 'end', sCurrentName);
    RETURN oErrorObj;
  EXCEPTION
    WHEN OTHERS THEN
      oErrorObj.handleError ( Stb$typedef.cnSeverityCritical, utd$msglib.getmsg ('exUnexpectedError', stb$security.getCurrentlanguage,  csP1 => sqlerrm(sqlcode)), 
        sCurrentName, dbms_utility.format_error_stack ||chr(10)|| dbms_utility.format_error_backtrace );
      RETURN oErrorObj;
  END callGroupMasterData;

  --**********************************************************************************************************************************
  function Getlov( sEntity        in varchar2, 
                   sSuchwert      in varchar2, 
                   oSelectionList out Isr$tlrselection$list)
    return Stb$oerror$record 
  is
    oErrorObj                     Stb$oerror$record := STB$OERROR$RECORD();
    sCurrentName         constant varchar2(100) := $$PLSQL_UNIT||'.getLov('||sEntity||')';
    sMsg            varchar2(4000);
    
    CURSOR cGetUsers IS
      SELECT   Userno, Fullname
      FROM     Stb$user
      ORDER BY Userno;    
  BEGIN
    isr$trace.stat('begin','sSuchwert: '||sSuchwert, sCurrentName);
    oSelectionList := ISR$TLRSELECTION$LIST ();
    
    CASE
      --***************************************
      -- list of all users------------------------------------------------------
      WHEN UPPER (sEntity) = 'USER' THEN
        for rGetUsers in cGetUsers LOOP
          oSelectionList.EXTEND;
          oSelectionList(oSelectionList.COUNT()) := ISR$OSELECTION$RECORD(rGetUsers.Fullname, rGetUsers.Userno, rGetUsers.Fullname, null, oSelectionList.COUNT());
        END LOOP;
      --***************************************      
      ELSE
        EXECUTE IMMEDIATE    
        'BEGIN
           :1 :='|| stb$util.getcurrentcustompackage || '.getLov(:2,:3,:4);
         END;'
        USING OUT oErrorObj, sEntity, IN sSuchwert, OUT oselectionlist;
    END CASE;
    
    IF sSuchWert IS NOT NULL AND oselectionlist.First IS NOT NULL THEN
      isr$trace.debug ('sSuchWert',  sSuchWert, sCurrentName); 
      FOR i IN 1..oSelectionList.Count() LOOP
        IF oSelectionList(i).sValue = sSuchWert THEN
          isr$trace.debug ('oSelectionList('||i||').sSelected', oSelectionList(i).sValue, sCurrentName);
          oSelectionList(i).sSelected := 'T';
        END IF;
      END LOOP;
    END IF;    

    isr$trace.stat('end', 'end', sCurrentName);
    return oErrorObj;
  EXCEPTION
    WHEN OTHERS THEN
      sMsg := Utd$msglib.Getmsg ('ERROR_PLACE', Stb$security.Getcurrentlanguage, 'STB$USERSECURITY.getLov', sqlerrm(sqlcode));
      oErrorObj.handleError ( Stb$typedef.cnseveritymessage, sMsg, sCurrentName, 
                            substr(dbms_utility.format_error_stack || CHR(10) || dbms_utility.format_error_backtrace, 1,4000)  );   
      RETURN oErrorObj;
  END Getlov;

  --*****************************************************************************************************************************
  FUNCTION setCurrentNode (oSelectedNode IN Stb$treenode$record)
    RETURN Stb$oerror$record IS
    oErrorObj                     Stb$oerror$record := STB$OERROR$RECORD();
    sCurrentName                  constant varchar2(100) := $$PLSQL_UNIT||'.setCurrentNode('||oSelectedNode.sDefaultAction||')';
  BEGIN
    isr$trace.stat('begin', 'oSelectedNode.sNodeType: '||oSelectedNode.sNodeType, sCurrentName, xmltype(oSelectedNode).getClobVal());
    stb$object.destroyoldvalues;
    Stb$object.Setcurrentnode (oSelectedNode);
    isr$trace.stat('end', 'end', sCurrentName);
    RETURN oErrorObj;
  EXCEPTION
    WHEN OTHERS THEN
      oErrorObj.handleError ( Stb$typedef.cnSeverityCritical, utd$msglib.getmsg ('exUnexpectedError', stb$security.getCurrentlanguage,  csP1 => sqlerrm(sqlcode)), 
        sCurrentName, dbms_utility.format_error_stack ||chr(10)|| dbms_utility.format_error_backtrace );
      RETURN oErrorObj;
  END setCurrentNode;

  --****************************************************************************************************************************
  function Getnodechilds( oSelectedNode IN  Stb$treenode$record, 
                          olChildNodes  OUT Stb$treenodelist)
    return Stb$oerror$record 
  is
    oErrorObj                     Stb$oerror$record := STB$OERROR$RECORD();
    sCurrentName                  constant varchar2(100) := $$PLSQL_UNIT||'.GetNodeChilds('||oSelectedNode.sDefaultAction||')';
    
    sNodeType             VARCHAR2(500);
    sPrevNodeType         VARCHAR2(500);
    sNodeIdFunctionGroup  VARCHAR2(500);
    sNodeIdReptypeRights  VARCHAR2(500);    
  begin
    isr$trace.stat('begin', 'oSelectedNode.sNodeType: '||oSelectedNode.sNodeType ,sCurrentName, xmltype(oSelectedNode).getClobVal());
    -- check if this procedure should handle the request
    olChildNodes := Stb$treenodelist ();

    CASE
      --=========================GROUPS=========================
      WHEN oSelectedNode.sNodetype = 'GROUPS' THEN      
              
        olChildNodes := Stb$treenodelist ();
        olChildNodes.EXTEND;
        olChildNodes (olChildNodes.COUNT()) := STB$TREENODE$RECORD(null, 'STB$USERSECURITY', null, 
                                                                   null, null, 'CAN_MODIFY_FUNCTION_GROUP',
                                                                   tloValueList => oSelectedNode.tloValueList);
        oErrorObj := STB$UTIL.getColListMultiRow('select groupname nodename
                                                       , usergroupno nodeid
                                                       , grouptype nodetype
                                                       , grouptype icon
                                                       , description
                                                       , (select COUNT (ul.usergroupno) from stb$userlink ul where ul.usergroupno = g.usergroupno) Members
                                                       , grouptype
                                                    from stb$usergroup g
                                                   order by grouptype desc, upper(groupname)', olChildNodes);  

      --=========================FUNCTIONGROUP=========================
      WHEN oSelectedNode.sNodetype IN ('FUNCTIONGROUP', 'SYSTEMRIGHTS') --, 'REPTYPERIGHTS'
        OR oSelectedNode.sNodetype like 'REPORTTYPERIGHTS%' THEN
        
        sNodeType := oSelectedNode.sNodetype;
        isr$trace.debug ('oSelectedNode.sNodetype', sNodeType, sCurrentName);
        
        sPrevNodeType := STB$OBJECT.getCurrentNodetype (-1);
        isr$trace.debug ('STB$OBJECT.getCurrentNodetype (-1)', sPrevNodeType, sCurrentName);
        
        sNodeIdFunctionGroup := STB$OBJECT.getNodeidForNodetype ('FUNCTIONGROUP');
        isr$trace.debug ('STB$OBJECT.getNodeidForNodetype (''FUNCTIONGROUP'')', sNodeIdFunctionGroup, sCurrentName);
        
        sNodeIdReptypeRights := STB$OBJECT.getNodeidForNodetype ('REPORTTYPERIGHTS');
        isr$trace.debug ('STB$OBJECT.getNodeidForNodetype (''REPORTTYPERIGHTS'')', sNodeIdReptypeRights, sCurrentName);
        
        FOR rGetNodes IN (SELECT UTD$MSGLIB.getMsg (nodename, STB$SECURITY.getCurrentLanguage) nodename
                               , package
                               , nodetype
                               , nodeid
                               , icon
                               , (SELECT COUNT ( * )
                                    FROM isr$right$configuration1
                                   WHERE to_char(id) = sNodeIdReptypeRights
                                     AND usergroupno = sNodeIdFunctionGroup
                                     AND type2 = 'GROUPRIGHTS'
                                     AND CASE
                                            WHEN nodetype = 'REPORTTYPERIGHT' THEN 1
                                            WHEN nodetype = 'REPORTTYPEPARAMETER' THEN 2
                                            WHEN nodetype = 'REPORTPARAMETER' THEN 3
                                            WHEN nodetype = 'TEMPLATE' THEN 4
                                            WHEN nodetype = 'WIZARDSCREEN' THEN 5
                                         END = SUBSTR (orderno, 1, 1)) childcnt                               
                            FROM isr$custom$grouping
                           WHERE ( (referencetype = sNodeType AND sNodeType IN ('FUNCTIONGROUP', 'SYSTEMRIGHTS'))
                               OR (referencetype = sPrevNodeType AND sNodeType LIKE 'REPORTTYPERIGHTS%'))
                          ORDER BY orderno) LOOP
          IF (sNodeType LIKE 'REPORTTYPERIGHTS%' and rGetNodes.childCnt > 0) OR sNodeType NOT LIKE 'REPORTTYPERIGHTS%' THEN
            olChildNodes.EXTEND;
            olChildNodes (olChildNodes.COUNT()) := STB$TREENODE$RECORD(rGetNodes.nodename, rGetNodes.package, rGetNodes.nodetype, 
                                                                     rGetNodes.nodeid, rGetNodes.icon,
                                                                     tloValueList => oSelectedNode.tloValueList);
            STB$OBJECT.setDirectory(olChildNodes (olChildNodes.COUNT()));
          END IF;
        END LOOP;

      --=========================NOT DEFINED=========================
      ELSE
        isr$trace.debug ('not defined',  oSelectedNode.sNodetype, sCurrentName);
    END CASE;

      isr$trace.stat('end', 'olChildNodes, see column LOGCLOB', sCurrentName);
    RETURN (oErrorObj);
  EXCEPTION
    WHEN OTHERS THEN
      ROLLBACK;
      oErrorObj.handleError ( Stb$typedef.cnSeverityCritical, utd$msglib.getmsg ('exUnexpectedError', stb$security.getCurrentlanguage,  csP1 => sqlerrm(sqlcode)), 
        sCurrentName, dbms_utility.format_error_stack ||chr(10)|| dbms_utility.format_error_backtrace );
      RETURN (oErrorObj);
  END Getnodechilds;

  -- ***************************************************************************************************
  FUNCTION getNodeList (oSelectedNode IN Stb$treenode$record, olNodeList OUT Stb$treenodelist)
    RETURN Stb$oerror$record IS
    sCurrentName   constant varchar2(4000) := $$PLSQL_UNIT||'.getNodeList';
    oErrorObj               Stb$oerror$record := STB$OERROR$RECORD();
  BEGIN
    isr$trace.stat('begin', 'begin', sCurrentName);

    CASE
        --=========================USERS=========================
      WHEN oSelectedNode.sNodetype = 'USERS' THEN
      
        olNodeList := Stb$treenodelist ();
        olNodeList.EXTEND;
        olNodeList (olNodeList.COUNT()) := STB$TREENODE$RECORD(null, 'STB$USERSECURITY', 'USER', 
                                                               null, 'USER', 'CAN_MODIFY_USER_DATA',
                                                               tloValueList => oSelectedNode.tloValueList);
        oErrorObj := STB$UTIL.getColListMultiRow('select fullname nodename, userno nodeid, U.Activeuser Active, U.Fullname, NVL(U.LdapUserName,U.Oracleusername) ConnectionName, U.Department
                                                    from Stb$user U, Stb$usergroup G
                                                   where U.Datagroup = G.Usergroupno(+)
                                                   order by fullname', olNodeList);
        
        --=========================GROUPS=========================
      WHEN oSelectedNode.sNodetype = 'GROUPS' THEN
      
        oErrorObj := GetNodeChilds (oSelectedNode, olNodeList);

        --=========================GROUPMEMBERS/DATAGROUP=========================
      WHEN oSelectedNode.sNodetype IN ('GROUPMEMBERS', 'DATAGROUP', 'FUNCTIONGROUP') THEN
        isr$trace.debug ('GROUPMEMBERS/DATAGROUP',  Stb$object.getCurrentNodeId, sCurrentName);
          
        olNodeList := Stb$treenodelist ();
        olNodeList.EXTEND;
        olNodeList (1) := STB$TREENODE$RECORD(Utd$msglib.Getmsg ('MEMBER', Stb$security.Getcurrentlanguage), 
                                              'STB$USERSECURITY', 'MEMBER', 
                                              Stb$object.getCurrentNodeId, '', 'CAN_ALTER_MEMBERSHIP',
                                              tloValueList => oSelectedNode.tloValueList);
    
        oErrorObj := STB$UTIL.getColListMultiRow('select distinct u.userno NODEID 
                                                                , u.fullname NODENAME 
                                                                , DECODE(ul.usergroupno, null, ''F'' ,''T'') ismember '
                                                 || case when NVL(Stb$object.getNodeIdForNodetype('FUNCTIONGROUP'), 'NOT_FOUND') = 'NOT_FOUND' then ' , DECODE(ul.usergroupno,NVL(U.DATAGROUP,-1),''T'',''F'') isDefault ' end ||
                                                 '  from STB$USER u, (select usergroupno,userno 
                                                                        from STB$USERLINK 
                                                                       where usergroupno IN ('||TO_NUMBER(CASE WHEN Stb$object.getNodeIdForNodetype('FUNCTIONGROUP') = 'NOT_FOUND' THEN Stb$object.getNodeIdForNodetype('DATAGROUP') ELSE Stb$object.getNodeIdForNodetype('FUNCTIONGROUP') END)||')) ul 
                                                   where u.userno=ul.userno(+) 
                                                   order by 2', olNodeList);    
    
        --=========================NOT DEFINED=========================
      ELSE
        oErrorObj := Getnodechilds (oSelectedNode, olNodeList);
    END CASE;

    isr$trace.stat('end', 'end', sCurrentName);
    RETURN (oErrorObj);
  EXCEPTION
    WHEN OTHERS THEN
      ROLLBACK;
      oErrorObj.handleError ( Stb$typedef.cnSeverityCritical, utd$msglib.getmsg ('exUnexpectedError', stb$security.getCurrentlanguage,  csP1 => sqlerrm(sqlcode)), 
        sCurrentName, dbms_utility.format_error_stack ||chr(10)|| dbms_utility.format_error_backtrace );
      RETURN (oErrorObj);
  END Getnodelist;

  --************************************************************************************************************************
  FUNCTION CreateNewdbUser (olParameter OUT STB$PROPERTY$LIST)
    RETURN Stb$oerror$record IS
    sCurrentName         constant varchar2(4000) := $$PLSQL_UNIT||'.CreateNewdbUser';
    oErrorObj                     Stb$oerror$record := STB$OERROR$RECORD();
    nCounter                      NUMBER := 0;
  BEGIN
    isr$trace.stat('begin', 'begin', sCurrentName);
    olParameter := STB$PROPERTY$LIST ();
    -- field to enter a new user
    nCounter := nCounter + 1;
    olParameter.EXTEND;            
    olParameter (nCounter) := STB$PROPERTY$RECORD('NEWUSER', Utd$msglib.Getmsg ('NEWUSER', Stb$security.Getcurrentlanguage), 
                                                  '', '', 'T', 'F', 'T', null, 'STRING');          
    -- field to enter a new password
    nCounter := nCounter + 1;
    olParameter.EXTEND;            
    olParameter (nCounter) := STB$PROPERTY$RECORD('NEWPASSWORD', Utd$msglib.Getmsg ('NEWPASSWORD', Stb$security.Getcurrentlanguage), 
                                                  '', '', 'T', 'F', 'T', null, 'NPASSWORD');          
    -- field for reenter new  password
    nCounter := nCounter + 1;
    olParameter.EXTEND;                
    olParameter (nCounter) := STB$PROPERTY$RECORD('NEWPASSWORD_REENTER', Utd$msglib.Getmsg ('NEWPASSWORD_REENTER', Stb$security.Getcurrentlanguage), 
                                                  '', '', 'T', 'F', 'T', null, 'NPASSWORD');  
    isr$trace.stat('end', 'end', sCurrentName);
    RETURN oErrorObj;
  EXCEPTION
    WHEN OTHERS THEN
      oErrorObj.handleError ( Stb$typedef.cnSeverityCritical, utd$msglib.getmsg ('exUnexpectedError', stb$security.getCurrentlanguage,  csP1 => sqlerrm(sqlcode)), 
        sCurrentName, dbms_utility.format_error_stack ||chr(10)|| dbms_utility.format_error_backtrace );
      RETURN oErrorObj;
  END Createnewdbuser;


  --******************************************************************************************************************************
  FUNCTION CreateNewUser (olParameter OUT STB$PROPERTY$LIST, Soracleuser IN VARCHAR2, sToken IN VARCHAR2)
    RETURN Stb$oerror$record IS
    sCurrentName   constant varchar2(4000) := $$PLSQL_UNIT||'.CreateNewUser('||sToken||')';
    oErrorObj                     Stb$oerror$record := STB$OERROR$RECORD();
    nNewUserno                    NUMBER;
  BEGIN
    isr$trace.stat('begin', 'begin', sCurrentName);
    
    -- get the new identifier
    SELECT Seq$stb$user.NEXTVAL
    INTO   nNewUserno
    FROM   DUAL;
    isr$trace.debug ('nNewUserno',  nNewUserno, sCurrentName);
    -- create a new user in the table
    INSERT INTO stb$user (Userno
                        , Oracleusername)
    VALUES (nNewUserno
          , sOracleuser);    
    -- create a own node
    INSERT INTO isr$custom$grouping (Nodeid
                                   , Nodetype
                                   , Parentnode
                                   , REFERENCE
                                   , Referencetype
                                   , Orderno
                                   , Nodename
                                   , Customtext
                                   , Icon)
    VALUES (Seq$stb$customnode.NEXTVAL
          , 'CUSTOMMYNODE'
          , -40
          , nNewUserno
          , 'USER'
          , 0
          , 'CUSTOMMYNODE'
          , 'MY NODES'
          , 'USER');
    isr$trace.debug ('ISR$CUSTOM$GROUPING',  nNewUserno, sCurrentName);
    
    -- DON'T DO THIS LATER, BECAUSE YOU KNOW THE NEW VALUE ONLY HERE AND call...masterdata references the nodeid
    ISR$TREE$PACKAGE.loadNode(STB$OBJECT.getCurrentNode);
    ISR$TREE$PACKAGE.selectRightNode(ISR$TREE$PACKAGE.buildNode('USER', nNewUserno, STB$OBJECT.getCurrentNode)); 

    -- call the headdata function
    oErrorObj := Callusermasterdata (olParameter, sToken);
    isr$trace.stat('end', 'end', sCurrentName);
    RETURN oErrorObj;
  EXCEPTION
    WHEN NO_DATA_FOUND THEN
      oErrorObj.handleError ( Stb$typedef.cnSeverityCritical, utd$msglib.getmsg ('exUnexpectedError', stb$security.getCurrentlanguage,  csP1 => sqlerrm(sqlcode)), 
        sCurrentName, dbms_utility.format_error_stack ||chr(10)|| dbms_utility.format_error_backtrace );
      RETURN oErrorObj;
    WHEN OTHERS THEN
      oErrorObj.handleError ( Stb$typedef.cnSeverityCritical, utd$msglib.getmsg ('exUnexpectedError', stb$security.getCurrentlanguage,  csP1 => sqlerrm(sqlcode)), 
        sCurrentName, dbms_utility.format_error_stack ||chr(10)|| dbms_utility.format_error_backtrace );
      RETURN oErrorObj;
  END Createnewuser;

  --******************************************************************************************************************************
  FUNCTION CreateNewGroup (csParameter IN VARCHAR2, olParameter OUT STB$PROPERTY$LIST)
    RETURN Stb$oerror$record IS
    sCurrentName   constant varchar2(4000) := $$PLSQL_UNIT||'.CreateNewGroup';
    oErrorObj                     Stb$oerror$record := STB$OERROR$RECORD();
    Nnewgroupno                   NUMBER;
    sType                         Stb$usergroup.Grouptype%TYPE;
    Noldgroupid                   NUMBER;
    
    CURSOR cGetmembers IS
      SELECT Userno
      FROM   Stb$userlink
      WHERE  Usergroupno = Noldgroupid;
  BEGIN
    isr$trace.stat('begin', 'begin', sCurrentName);
    
    IF csParameter IN ('CAN_CLONE_FUNCTION_GROUP', 'CAN_CLONE_DATA_GROUP') THEN
      Noldgroupid := Stb$object.getCurrentNodeId;
      isr$trace.debug (' nOldGroupId', Noldgroupid, sCurrentName);
    END IF;

    -- check fpr the type of the group
    IF csParameter = 'CAN_ADD_FUNCTION_GROUP' OR csParameter = 'CAN_CLONE_FUNCTION_GROUP' THEN
      sType := 'FUNCTIONGROUP';
    ELSE
      sType := 'DATAGROUP';
    END IF;

    -- get the new identifier
    SELECT Seq$stb$groups.NEXTVAL
    INTO   Nnewgroupno
    FROM   DUAL;
    isr$trace.info (' nNewGroupNo',  Nnewgroupno, sCurrentName);
    INSERT INTO Stb$usergroup
                (Usergroupno, Groupname, Grouptype, Modifiedon, Modifiedby, Description)
         VALUES (nNewGroupNo, 'newGroup ' || nNewGroupNo, sType, SYSDATE, Stb$security.getOracleUserName(Stb$security.getCurrentUser), 'newGroup ' || nNewGroupNo);
    isr$trace.debug ('nUserNo', Stb$security.getOracleUserName(Stb$security.getCurrentUser), sCurrentName);

    IF sType = 'DATAGROUP' THEN
      INSERT INTO Isr$custom$grouping
                  (Nodeid, Nodetype, Parentnode, REFERENCE, Referencetype, Orderno, Nodename,
                   Customtext, Icon)
           VALUES (Seq$stb$customnode.NEXTVAL, 'CUSTOMGROUPNODE', -40, nNewGroupNo, 'DATAGROUP', (SELECT NVL (MAX (Orderno) + 1, 1) AS Ordnerno
                                                                                                  FROM   Isr$custom$grouping
                                                                                                  WHERE  Referencetype = 'DATAGROUP' AND Parentnode IS NULL), 'CUSTOMGROUPNODE' || nNewGroupNo,
                   'newGroup ' || nNewGroupNo, 'DATAGROUP');
      isr$trace.debug ('ISR$CUSTOM$GROUPING',  'CUSTOMGROUPNODE' || nNewGroupNo, sCurrentName);
    END IF;
    
    -- DON'T DO THIS LATER, BECAUSE YOU KNOW THE NEW VALUE ONLY HERE AND call...masterdata references the nodeid
    if csParameter IN ('CAN_CLONE_FUNCTION_GROUP', 'CAN_CLONE_DATA_GROUP') then
      ISR$TREE$PACKAGE.reloadPrevNodesAndSelect(-1, 'F');
    else
      ISR$TREE$PACKAGE.loadNode(STB$OBJECT.getCurrentNode);
    end if;
    ISR$TREE$PACKAGE.selectLeftNode(ISR$TREE$PACKAGE.buildNode(sType, nNewgroupno, STB$OBJECT.getCurrentNode));
    
    -- call the headdata function
    oErrorObj := Callgroupmasterdata (olParameter, csParameter);
    isr$trace.debug ('new group created',  Nnewgroupno, sCurrentName);

    -- check if the group is a new function group
    IF csParameter IN ('CAN_ADD_FUNCTION_GROUP', 'CAN_CLONE_FUNCTION_GROUP') THEN
    
      isr$trace.debug ('stb$object.getCurrentNodeId',  stb$object.getCurrentNodeId, sCurrentName);

      INSERT INTO Stb$adminrights (Parametername, Parametervalue, Usergroupno)
         (SELECT parametername
               , CASE
                    WHEN csParameter = 'CAN_CLONE_FUNCTION_GROUP' THEN parametervalue
                    ELSE 'T'
                 END
               , nNewgroupno
            FROM stb$adminrights
           WHERE usergroupno =
                    CASE
                       WHEN csParameter = 'CAN_CLONE_FUNCTION_GROUP' THEN nOldGroupId
                       ELSE 1
                    END);
                    
      INSERT INTO Stb$group$reptype$rights (Parametername
                                          , Parametervalue
                                          , Reporttypeid
                                          , Usergroupno)
         (SELECT parametername
               , CASE
                    WHEN csParameter = 'CAN_CLONE_FUNCTION_GROUP' THEN parametervalue
                    ELSE 'T'
                 END
               , reporttypeid
               , nNewgroupno
            FROM stb$group$reptype$rights
           WHERE usergroupno =
                    CASE
                       WHEN csParameter = 'CAN_CLONE_FUNCTION_GROUP' THEN nOldGroupId
                       ELSE 1
                    END);    
    END IF;


    IF csParameter IN ('CAN_CLONE_FUNCTION_GROUP', 'CAN_CLONE_DATA_GROUP') THEN
      -- Copy the members
      FOR rGetmembers IN cGetmembers LOOP
        INSERT INTO Stb$userlink (Entryid, Usergroupno, Userno)
        VALUES (Seq$stb$userlink.NEXTVAL, Nnewgroupno, rGetmembers.Userno);
      END LOOP;
    END IF;

    isr$trace.stat('end', 'end', sCurrentName);
    RETURN oErrorObj;
  EXCEPTION
    WHEN NO_DATA_FOUND THEN
      oErrorObj.handleError ( Stb$typedef.cnSeverityCritical, utd$msglib.getmsg ('exUnexpectedError', stb$security.getCurrentlanguage,  csP1 => sqlerrm(sqlcode)), 
        sCurrentName, dbms_utility.format_error_stack ||chr(10)|| dbms_utility.format_error_backtrace );
      RETURN oErrorObj;
    WHEN OTHERS THEN
      oErrorObj.handleError ( Stb$typedef.cnSeverityCritical, utd$msglib.getmsg ('exUnexpectedError', stb$security.getCurrentlanguage,  csP1 => sqlerrm(sqlcode)), 
        sCurrentName, dbms_utility.format_error_stack ||chr(10)|| dbms_utility.format_error_backtrace );
      RETURN oErrorObj;
  END Createnewgroup;
  

  --**********************************************************************************************************************************
  function callFunction( oParameter in  Stb$menuentry$record, 
                         olNodeList out Stb$treenodelist )
    return Stb$oerror$record 
  is
    oErrorObj                     Stb$oerror$record := STB$OERROR$RECORD();
    sCurrentName                  constant varchar2(100) := $$PLSQL_UNIT||'.callFunction('||oParameter.sToken||')';
    olParameter                   STB$PROPERTY$LIST;
    oCurrentNode                  STB$TREENODE$RECORD;
    exNotExists                   EXCEPTION;
    exNoUserDelete                EXCEPTION;
    exNoGroupDelete               EXCEPTION;
    exNoAudit                     EXCEPTION;
    exAudit                       EXCEPTION;
    Exdefault                     EXCEPTION;
    exLDAPUser                    EXCEPTION;
    Sdeleteflag                   VARCHAR2 (1) := 'F';
    Sauditexists                  VARCHAR2 (1);
    Sdynsql                       VARCHAR2 (2000);
    sUserNo                       VARCHAR2 (1000);
    nUserNo                       NUMBER;
    
    sModifiedFlag                 VARCHAR2(1);
 
    CURSOR cIsLDAPUser IS
      SELECT 'T'
      FROM   stb$user
      WHERE  userno = Stb$object.getCurrentNodeId
      AND    ldapusername IS NOT NULL;
         
    sIsLDAPUser                   VARCHAR2(1);

    -- Cursor to check if there are entires in the activitylog for the current user
    CURSOR cCheckiSRIsUsed IS
      SELECT 'F'
      FROM   ISR$ACTIONLOG
      WHERE  userno = Stb$object.getCurrentNodeId;

    -- cursor to check if the group has members
    CURSOR cCheckMember IS
      SELECT 'F'
      FROM   Stb$userlink
      WHERE  Usergroupno = Stb$object.getCurrentNodeId;
  BEGIN
    isr$trace.stat('begin', 'oParameter.sToken: '||oParameter.sToken||'  currentNodeId: '||Stb$object.getCurrentNodeId, sCurrentName, xmltype(oParameter).getClobVal());
    oCurrentNode := STB$OBJECT.getCurrentNode;
    
    STB$OBJECT.setCurrentToken(oparameter.sToken);

    CASE
      --=========================CAN_ADD_USER_WITH_DB=========================
    WHEN UPPER (oParameter.sToken) = 'CAN_ADD_USER_WITH_DB' THEN
        --Initialization for the mask of the user master data
        olNodeList := Stb$treenodelist ();
        olNodeList.EXTEND;
        olNodeList (1) := STB$TREENODE$RECORD(Utd$msglib.Getmsg ('DBUSER', Stb$security.Getcurrentlanguage), 
                                                'STB$USERSECURITY', 'DBUSER', 
                                                'MASK', '');
        oErrorObj := Createnewdbuser (olParameter);
        olNodeList (1).tlopropertylist := olParameter;
        FOR i IN 1..olParameter.COUNT() LOOP
          IF olParameter(i).sEditable = 'T'
          AND olParameter(i).sDisplayed = 'T' THEN
            olParameter(i).sValue := null;
          END IF;
        END LOOP;
                
        oErrorObj := checkDependenciesOnMask(oParameter, olNodeList (1).tlopropertylist, sModifiedFlag);
      --=========================CAN_ADD_USER=========================
    WHEN UPPER (oParameter.sToken) = 'CAN_ADD_USER' THEN
        --Initialization for the mask of the user master data
        olNodeList := Stb$treenodelist ();
        olNodeList.EXTEND;
        olNodeList (1) := STB$TREENODE$RECORD(Utd$msglib.Getmsg ('User', Stb$security.Getcurrentlanguage), 
                                              'STB$USERSECURITY', 'TRANSPORT', 
                                              'MASK', '');
        oErrorObj := Createnewuser (olParameter, NULL, UPPER (oParameter.sToken));
        olNodeList (1).tlopropertylist := olParameter;
        FOR i IN 1..olParameter.COUNT() LOOP
          IF olParameter(i).sEditable = 'T'
          AND olParameter(i).sDisplayed = 'T' THEN
            olParameter(i).sValue := null;
          END IF;
        END LOOP;
        
        oErrorObj := checkDependenciesOnMask(oParameter, olNodeList (1).tlopropertylist, sModifiedFlag);
      --=========================CAN_DEACTIVATE_USER=========================
    WHEN UPPER (oParameter.sToken) = 'CAN_DEACTIVATE_USER' THEN
        isr$trace.debug ('STB$OBJECT.getCurrentNodeId',  Stb$object.getCurrentNodeId, sCurrentName);
        UPDATE Stb$user
           SET Activeuser = 'F'
         WHERE Userno = Stb$object.getCurrentNodeId;
        FOR rGetOracleUser IN (SELECT oracleusername
                                 FROM STB$USER
                                WHERE Userno = Stb$object.getCurrentNodeId
                                  AND TRIM(oracleusername) IS NOT NULL) LOOP
          Sdynsql := 'revoke ' || Stb$util.Getsystemparameter ('ISRROLE') || ' from ' || rGetOracleUser.oracleusername;
          isr$trace.debug ('sDynSQl', Sdynsql, sCurrentName);
          EXECUTE IMMEDIATE Sdynsql;
        END LOOP;
        oErrorObj := Stb$audit.Openaudit ('USERAUDIT', Stb$object.getCurrentNodeId);
        oErrorObj := Stb$audit.Addauditrecord ( oParameter.sToken );
        oErrorObj := Stb$audit.Addauditentry ('ACTIVEUSER', 'T', 'F', 1);
        oErrorObj := stb$audit.silentaudit;
        COMMIT;
        
        ISR$TREE$PACKAGE.selectCurrent;
      --=========================CAN_DELETE_USER=========================
    WHEN UPPER (oParameter.sToken) = 'CAN_DELETE_USER' THEN
    
        -- check if the user has entries in the activity log
        OPEN cCheckiSRIsUsed;
        FETCH cCheckiSRIsUsed INTO sDeleteFlag;
        IF cCheckiSRIsUsed%NOTFOUND THEN
          sDeleteFlag := 'T';
        END IF;
        CLOSE cCheckiSRIsUsed;

        IF sDeleteFlag = 'T' THEN
          sUserNo := Stb$object.getCurrentNodeId;
          nUserNo := TO_NUMBER (sUserNo);
          -- delete the user and his entries
          -- ISRC-1044 removed   
          DELETE FROM Stb$userlink
                WHERE Userno = nUserNo;                
          DELETE FROM Stb$userparameter
                WHERE Userno = nUserNo;
          DELETE FROM Stb$user
                WHERE Userno = nUserNo;
          DELETE FROM Isr$custom$grouping
                WHERE REFERENCE = sUserNo AND Referencetype = 'USER';
          oErrorObj := Stb$audit.Openaudit ('SYSTEMAUDIT', TO_CHAR (-1));
          oErrorObj := Stb$audit.Addauditrecord ( oParameter.sToken );
          oErrorObj := Stb$audit.Addauditentry (UPPER (oParameter.sToken), 'Delete', STB$OBJECT.getCurrentNode().sDisplay, 1);
          oErrorObj := Stb$audit.Silentaudit;
          COMMIT;
          ISR$TREE$PACKAGE.selectCurrent;
        ELSE
          RAISE exNoUserDelete;
        END IF;
      --=========================CAN_REACTIVATE_USER=========================
    WHEN UPPER (oParameter.sToken) = 'CAN_REACTIVATE_USER' THEN
        UPDATE Stb$user
           SET Activeuser = 'T'
         WHERE Userno = Stb$object.getCurrentNodeId;
        FOR rGetOracleUser IN (SELECT oracleusername
                                 FROM STB$USER
                                WHERE Userno = Stb$object.getCurrentNodeId
                                  AND TRIM(oracleusername) IS NOT NULL) LOOP
          Sdynsql := 'grant ' || Stb$util.Getsystemparameter ('ISRROLE') || ' to ' || rGetOracleUser.oracleusername;
          isr$trace.info ('sDynSQl',  Sdynsql, sCurrentName);
          EXECUTE IMMEDIATE Sdynsql;
        END LOOP;
        oErrorObj := Stb$audit.Openaudit ('USERAUDIT', Stb$object.getCurrentNodeId);
        oErrorObj := Stb$audit.Addauditrecord ( oParameter.sToken );
        oErrorObj := Stb$audit.Addauditentry ('ACTIVEUSER', 'F', 'T', 1);
        oErrorObj := stb$audit.silentaudit;
        COMMIT;
        ISR$TREE$PACKAGE.selectCurrent;
      --=========================CAN_MODIFY_USER_DATA=========================
    WHEN UPPER (oParameter.sToken) = 'CAN_MODIFY_USER_DATA' THEN
        --Initialization for the mask of the user master data
        olNodeList := Stb$treenodelist ();
        olNodeList.EXTEND;
        olNodeList (1) := STB$TREENODE$RECORD(Utd$msglib.Getmsg ('User', Stb$security.Getcurrentlanguage), 
                                              'STB$USERSECURITY', 'TRANSPORT', 
                                              'MASK', '');
        oErrorObj := Callusermasterdata (olParameter, UPPER (oParameter.sToken));
        olNodeList (1).tlopropertylist := olParameter;
        
        oErrorObj := checkDependenciesOnMask(oParameter, olNodeList (1).tlopropertylist, sModifiedFlag);
      --=========================CAN_MODIFY_OWN_USER_DATA=========================
    WHEN UPPER (oParameter.sToken) = 'CAN_MODIFY_OWN_USER_DATA' THEN
        --Intialization for the mask of the user master data
        olNodeList := Stb$treenodelist ();
        olNodeList.EXTEND;
        olNodeList (1) := STB$TREENODE$RECORD(Utd$msglib.Getmsg ('User', Stb$security.Getcurrentlanguage), 
                                              'STB$USERSECURITY', 'TRANSPORT', 
                                              'MASK', '');
        oErrorObj := Callusermasterdata (olParameter, UPPER (oParameter.sToken));
        olNodeList (1).tlopropertylist := olParameter;
        -- debug only if investigating behaviour 
          -- isr$trace.debug('olNodeList', 'olNodeList, see column LOGCLOB', sCurrentName, olNodeList);
      --=========================CAN_CHANGE_USER_PASSWORD=========================
    WHEN UPPER (oParameter.sToken) = 'CAN_CHANGE_USER_PASSWORD'
      OR UPPER (oParameter.sToken) = 'CAN_MODIFY_USER_PARAMETER' THEN
      
        IF UPPER (oParameter.sToken) = 'CAN_CHANGE_USER_PASSWORD' THEN
          OPEN cIsLDAPUser;
          FETCH cIsLDAPUser INTO sIsLDAPUser;
          CLOSE cIsLDAPUser;
          IF sIsLDAPUser = 'T' THEN
            RAISE exLDAPUser;
          END IF;
        END IF;
      
        olNodeList := Stb$treenodelist ();
        olNodeList.EXTEND;
        olNodeList (1) := STB$TREENODE$RECORD(STB$UTIL.getMaskTitle(Upper (oParameter.sToken)), 
                                              'STB$USERSECURITY', 'TRANSPORT', 
                                              'MASK', '');
        oErrorObj := STB$UTIL.getPropertyList(Upper (oParameter.sToken), olParameter);
        olNodeList (1).tlopropertylist := olParameter;
        
      --=========================CAN_DISPLAY_USER/GROUPAUDIT=========================
    WHEN UPPER (oParameter.sToken) = 'CAN_CHANGE_OWN_PASSWORD' THEN
    
        IF UPPER(STB$SECURITY.getOracleUserName(STB$SECURITY.getCurrentUser)) != UPPER(USER) THEN
          RAISE exLDAPUser;
        END IF;
        
        olNodeList := STB$TREENODELIST ();
        olNodeList.EXTEND;
        olNodeList (1) := STB$TREENODE$RECORD(STB$UTIL.getMaskTitle(Upper (oParameter.sToken)), 
                                              'STB$SYSTEM', 'TRANSPORT', 
                                              'MASK', '');
        olNodeList (1).nDisplayType := STB$TYPEDEF.cnOwnPasswordDialog;
        oErrorObj := STB$UTIL.getPropertyList(Upper (oParameter.sToken), olNodeList (1).tlopropertylist);        
      --=========================CAN_DISPLAY_USER/GROUPAUDIT=========================
    WHEN Upper (oparameter.sToken) like 'CAN_DISPLAY_%AUDIT' THEN
      
        IF Upper (oparameter.sToken) like 'CAN_DISPLAY_%AUDIT' THEN
          oErrorObj := stb$audit.checkauditexists (sauditexists, To_Char (stb$object.getCurrentNodeId), REPLACE(Upper (oparameter.sToken), 'CAN_DISPLAY_'));
          IF sauditexists = 'F' THEN
            RAISE exNoAudit;
          END IF;
        END IF;

        olNodeList := Stb$treenodelist ();
        olNodeList.EXTEND;
        olNodeList (1) := STB$TREENODE$RECORD(STB$UTIL.getMaskTitle(Upper (oParameter.sToken)), 
                                              'STB$SYSTEM', 'TRANSPORT', 
                                              'MASK', '');
        oErrorObj := STB$UTIL.getPropertyList(Upper (oParameter.sToken), olnodelist (1).tlopropertylist);
      --=========================CAN_ADD_FUNCTION_GROUP/CAN_ADD_DATA_GROUP=========================
    WHEN UPPER (oParameter.sToken) = 'CAN_ADD_FUNCTION_GROUP' OR UPPER (oParameter.sToken) = 'CAN_ADD_DATA_GROUP' THEN
        --Initialization for the mask of the usergroup master data
        olNodeList := Stb$treenodelist ();
        olNodeList.EXTEND;
        olNodeList (1) := STB$TREENODE$RECORD(Utd$msglib.Getmsg ('GROUP', Stb$security.Getcurrentlanguage), 
                                              'STB$USERSECURITY', 'TRANSPORT', 
                                              'MASK', '');
        oErrorObj := Createnewgroup (oParameter.sToken, olParameter);
        olNodeList (1).tlopropertylist := olParameter;
        FOR i IN 1..olParameter.COUNT() LOOP
          IF olParameter(i).sEditable = 'T'
          AND olParameter(i).sDisplayed = 'T' THEN
            olParameter(i).sValue := null;
          END IF;
        END LOOP;        
      --=========================CAN_CLONE_FUNCTION_GROUP/CAN_CLONE_DATA_GROUP=========================
    WHEN UPPER (oParameter.sToken) = 'CAN_CLONE_FUNCTION_GROUP' OR UPPER (oParameter.sToken) = 'CAN_CLONE_DATA_GROUP' THEN
        --Intialization for the mask of the usergroup master data
        olNodeList := Stb$treenodelist ();
        olNodeList.EXTEND;
        olNodeList (1) := STB$TREENODE$RECORD(Utd$msglib.Getmsg ('GROUP', Stb$security.Getcurrentlanguage), 
                                              'STB$USERSECURITY', 'TRANSPORT', 
                                              'MASK', '');
        oErrorObj := Createnewgroup ((oParameter.sToken), olParameter);
        olNodeList (1).tlopropertylist := olParameter;
        FOR i IN 1..olParameter.COUNT() LOOP
          IF olParameter(i).sEditable = 'T'
          AND olParameter(i).sDisplayed = 'T' THEN
            olParameter(i).sValue := null;
          END IF;
        END LOOP;        
      --=========================CAN_MODIFY_FUNCTION_GROUP/CAN_MODIFY_DATA_GROUP=========================
    WHEN UPPER (oParameter.sToken) = 'CAN_MODIFY_FUNCTION_GROUP' OR UPPER (oParameter.sToken) = 'CAN_MODIFY_DATA_GROUP' THEN
        --Intialization for the mask of the user master data
        olNodeList := Stb$treenodelist ();
        olNodeList.EXTEND;
        olNodeList (1) := STB$TREENODE$RECORD(Utd$msglib.Getmsg ('GROUP', Stb$security.Getcurrentlanguage), 
                                              'STB$USERSECURITY', 'TRANSPORT', 
                                              'MASK', '');
        oErrorObj := Callgroupmasterdata (olParameter, UPPER (oParameter.sToken));
        olNodeList (1).tlopropertylist := olParameter;
        
      --================================ALTER MEMBERSHIP====================
    WHEN UPPER (oParameter.sToken) = 'CAN_ALTER_MEMBERSHIP' THEN
    
        -- check if this is mot the default group of the user (data groups) then the user can't be removed
        IF STB$UTIL.getProperty(STB$OBJECT.getCurrentNode().tlopropertylist, 'ISDEFAULT') = 'T' THEN
          RAISE exDefault;
        END IF;

        oErrorObj := Putmembers (STB$OBJECT.getCurrentNode().tlopropertylist, UPPER (oParameter.sToken));

        IF stb$util.checkpromptedauditrequired ('PROMPTED_GROUP_AUDIT') = 'T' THEN
          ISR$TREE$PACKAGE.selectCurrent('F');
          RAISE exaudit;
        ELSE
          oErrorObj := stb$audit.silentaudit;
          COMMIT;
          ISR$TREE$PACKAGE.selectCurrent;
        END IF;
        
        IF STB$UTIL.checkMethodExists('triggerRibbonUpdate', 'ISR$RIBBON') = 'T' THEN
          EXECUTE immediate 'BEGIN
                               ISR$RIBBON.triggerRibbonUpdate;
                             END;';
        END IF;  
        
      --=========================CAN_DELETE_FUNCTION_GROUP=========================
    WHEN UPPER (oParameter.sToken) in ( 'CAN_DELETE_FUNCTION_GROUP', 'CAN_DELETE_DATA_GROUP') THEN
        
        -- check if the group has members
        OPEN cCheckMember;
        FETCH cCheckMember INTO sDeleteFlag;
        IF cCheckMember%NOTFOUND THEN
          sDeleteFlag := 'T';
        END IF;
        CLOSE cCheckMember;

        IF sDeleteFlag = 'T' THEN
          --delete the usergroup and its entries
          DELETE FROM Stb$adminrights
                WHERE Usergroupno = Stb$object.getCurrentNodeId;
          DELETE FROM Stb$group$reptype$rights
                WHERE Usergroupno = Stb$object.getCurrentNodeId;
          DELETE FROM Stb$usergroup
                WHERE Usergroupno = Stb$object.getCurrentNodeId;
          oErrorObj := Stb$audit.Openaudit ('SYSTEMAUDIT', TO_CHAR (-1));
          oErrorObj := Stb$audit.Addauditrecord ( oParameter.sToken );
          oErrorObj := Stb$audit.Addauditentry (UPPER (oParameter.sToken), 'Delete', STB$OBJECT.getCurrentNode().sDisplay, 1);
          oErrorObj := Stb$audit.Silentaudit;
          COMMIT;
          ISR$TREE$PACKAGE.reloadPrevNodesAndSelect(-1);
        ELSE
          RAISE exNoGroupDelete;
        END IF;
      ELSE
        RAISE exNotExists;
    END CASE;

    -- hold the values in the package to compare them if they have been changed
    oCurrentParameter := olParameter;
    isr$trace.stat('end', 'end', sCurrentName);
    RETURN oErrorObj;
  EXCEPTION
    WHEN exLDAPUser THEN
      oErrorObj.sErrorCode := Utd$msglib.Getmsg ('exLDAPUser', Stb$security.Getcurrentlanguage);     
      oErrorObj.handleError ( Stb$typedef.cnSeverityMessage, utd$msglib.getmsg ('exLDAPUser$DESC', stb$security.getCurrentlanguage,  csP1 => sqlerrm(sqlcode)), 
        sCurrentName, dbms_utility.format_error_stack ||chr(10)|| dbms_utility.format_error_backtrace );
      RETURN oErrorObj;  
    WHEN Exdefault THEN
      oErrorObj.sErrorCode := Utd$msglib.Getmsg ('exDefaultGroup', Stb$security.Getcurrentlanguage);  
      oErrorObj.handleError ( Stb$typedef.cnSeverityMessage, utd$msglib.getmsg ('exDefaultGroupText', stb$security.getCurrentlanguage,  csP1 => sqlerrm(sqlcode)), 
        sCurrentName, dbms_utility.format_error_stack ||chr(10)|| dbms_utility.format_error_backtrace );
      RETURN oErrorObj;
    WHEN exAudit THEN
      oErrorObj.sErrorCode := 'exAudit';
      oErrorObj.handleError ( Stb$typedef.cnAudit, 'Call the mask for the audit window', 
        sCurrentName, dbms_utility.format_error_stack ||chr(10)|| dbms_utility.format_error_backtrace );
      RETURN oErrorObj;
    WHEN exNoAudit THEN
      oErrorObj.sErrorCode := Utd$msglib.Getmsg ('exNoAudit', Stb$security.Getcurrentlanguage);  
      oErrorObj.handleError ( Stb$typedef.cnSeverityMessage, utd$msglib.getmsg ('exNoAuditText', stb$security.getCurrentlanguage,  csP1 => sqlerrm(sqlcode)), 
        sCurrentName, dbms_utility.format_error_stack ||chr(10)|| dbms_utility.format_error_backtrace );
      RETURN oErrorObj;
    WHEN exNotExists THEN
      oErrorObj.sErrorCode := utd$msglib.getmsg ('exNotExists', stb$security.getcurrentlanguage);
      oErrorObj.handleError ( Stb$typedef.cnSeverityMessage, utd$msglib.getmsg ('exNotExistsText', stb$security.getCurrentlanguage,  csP1 => sqlerrm(sqlcode)), 
        sCurrentName, dbms_utility.format_error_stack ||chr(10)|| dbms_utility.format_error_backtrace );
      RETURN oErrorObj;
    WHEN exNoUserDelete THEN
      -- user can't be deleted
      oErrorObj.sErrorCode := Utd$msglib.Getmsg ('exNoUserDelete', Stb$security.Getcurrentlanguage);
      oErrorObj.handleError ( Stb$typedef.cnSeverityMessage, utd$msglib.getmsg ('exNoUserDeleteText', stb$security.getCurrentlanguage,  csP1 => sqlerrm(sqlcode)), 
        sCurrentName, dbms_utility.format_error_stack ||chr(10)|| dbms_utility.format_error_backtrace );
      RETURN oErrorObj;
    WHEN exNoGroupDelete THEN
      -- group can't be deleted
      oErrorObj.sErrorCode := Utd$msglib.Getmsg ('exNoGroupDelete', Stb$security.Getcurrentlanguage);
      oErrorObj.handleError ( Stb$typedef.cnSeverityMessage, utd$msglib.getmsg ('exNoGroupDeleteText', stb$security.getCurrentlanguage,  csP1 => sqlerrm(sqlcode)), 
        sCurrentName, dbms_utility.format_error_stack ||chr(10)|| dbms_utility.format_error_backtrace );
      RETURN oErrorObj;
    WHEN OTHERS THEN
    ROLLBACK;
      oErrorObj.handleError ( Stb$typedef.cnSeverityWarning, utd$msglib.getmsg ('exUnexpectedError', stb$security.getCurrentlanguage,  csP1 => sqlerrm(sqlcode)), 
        sCurrentName, dbms_utility.format_error_stack ||chr(10)|| dbms_utility.format_error_backtrace );
      RETURN oErrorObj;
  END callFunction;

  --*************************************************************************************************************************
  FUNCTION CreateDbUser (olParameter IN STB$PROPERTY$LIST)
    RETURN Stb$oerror$record IS
    sCurrentName   constant varchar2(4000) := $$PLSQL_UNIT||'.CreateDbUser';
    oErrorObj                     Stb$oerror$record := STB$OERROR$RECORD();
    Excreateuser                  EXCEPTION;
    sReturn                       VARCHAR2 (1000);
  BEGIN
    isr$trace.stat('begin', 'begin', sCurrentName);

    --create the user
    sReturn := Isr$dbuser_syn.Createuser (olParameter (1).sValue, olParameter (2).sValue, USER);
    isr$trace.debug ('sReturn createUser',  sReturn, sCurrentName);

    IF TRIM (sReturn) IS NOT NULL THEN
      RAISE Excreateuser;
    END IF;

    -- create the grants
    sReturn := Isr$dbuser_syn.Creategrants (Stb$util.Getsystemparameter ('ISRROLE'), olParameter (1).sValue, Stb$util.Getsystemparameter ('STBOWNER'));
    isr$trace.debug ('sReturn Createsyn',  sReturn, sCurrentName);

    IF TRIM (sReturn) IS NOT NULL THEN
      RAISE Excreateuser;
    END IF;

    -- create the synonyms
    sReturn := Isr$dbuser_syn.Createsyn (Stb$util.Getsystemparameter ('ISRROLE'), olParameter (1).sValue, Stb$util.Getsystemparameter ('STBOWNER'));
    isr$trace.debug ('sReturn createSyn',  sReturn, sCurrentName);

    IF TRIM (sReturn) IS NOT NULL THEN
      RAISE Excreateuser;
    END IF;

    isr$trace.stat('end', 'end', sCurrentName);
    RETURN oErrorObj;
  EXCEPTION
    WHEN Excreateuser THEN
      oErrorObj.handleError ( Stb$typedef.cnSeverityWarning, utd$msglib.getmsg ('exUnexpectedError', stb$security.getCurrentlanguage,  csP1 => sqlerrm(sqlcode)), 
        sCurrentName, dbms_utility.format_error_stack ||chr(10)|| dbms_utility.format_error_backtrace );
      RETURN oErrorObj;
    WHEN OTHERS THEN
      oErrorObj.handleError ( Stb$typedef.cnSeverityWarning, utd$msglib.getmsg ('exUnexpectedError', stb$security.getCurrentlanguage,  csP1 => sqlerrm(sqlcode)), 
        sCurrentName, dbms_utility.format_error_stack ||chr(10)|| dbms_utility.format_error_backtrace );
      RETURN oErrorObj;
  END Createdbuser;

  --**********************************************************************************************************************
  FUNCTION putParameters (oParameter IN Stb$menuentry$record, olParameter IN STB$PROPERTY$LIST)
    RETURN Stb$oerror$record IS
    oErrorObj                     Stb$oerror$record := STB$OERROR$RECORD();
    sCurrentName                  constant varchar2(100) := $$PLSQL_UNIT||'.putParameters('||oParameter.sToken||')';
    exAudit                       EXCEPTION;
    exNotExists                   EXCEPTION;
    exPasswordError               EXCEPTION;
    exSynonymError                EXCEPTION;
    nUserNo                       NUMBER;
    nDataGroup                    NUMBER;
    Sanysynonym                   VARCHAR2 (1);
    sPrompted                     VARCHAR2 (200);
    olParameterout                STB$PROPERTY$LIST;
    Noutputid                     Isr$report$output$type.Outputid%TYPE;
    Nactionid                     Isr$action.Actionid%TYPE;
    Sfunction                     Isr$report$output$type.Actionfunction%TYPE;
    sReturn                       VARCHAR2 (4000);
    sMsg                  varchar2(4000);
    
    CURSOR cHasPrivilege IS
      SELECT 'T'
      FROM   (SELECT PRIVILEGE
              FROM   role_sys_privs
              UNION
              SELECT PRIVILEGE
              FROM   user_sys_privs)
      WHERE  PRIVILEGE = 'CREATE ANY SYNONYM';
      
    rHasPrivilege cHasPrivilege%rowtype;    

    CURSOR cGetjobids (CsToken IN Isr$action.Token%TYPE) IS
      SELECT Ao.Outputid, Ao.Actionid, O.Actionfunction Myfunction
        FROM Isr$action A, Isr$report$output$type O, Isr$action$output Ao
       WHERE (UPPER (O.Token) = UPPER (CsToken)
           OR UPPER (A.Token) = UPPER (CsToken))
         AND O.Outputid = Ao.Outputid
         AND A.Actionid = Ao.Actionid;     
      
    CURSOR cGetUserLink(cnGroupNo IN NUMBER, cnUserNo IN NUMBER) IS
      SELECT *
        FROM stb$userlink
       WHERE usergroupno = cnGroupNo
         AND userno = cnUserNo;
    
    rGetUserLink cGetUserLink%rowtype;        
  BEGIN
    isr$trace.stat('begin', 'oParameter.sToken: '||oParameter.sToken, sCurrentName);
    
    STB$OBJECT.setCurrentToken(oparameter.sToken);

    CASE
      --=========================CAN_ADD_USER_WITH_DB=========================
    WHEN UPPER (oParameter.sToken) = 'CAN_ADD_USER_WITH_DB' THEN
        isr$trace.info ('1',  '1', sCurrentName);
        oErrorObj := Createdbuser (olParameter);
        --write in the activity log and return
        ISR$ACTION$LOG.setActionLog ('New database User created by ISR', olParameter (1).sValue);
        isr$trace.debug ('oErrorObj.sSeverityCode',  'oErrorObj.sSeverityCode: ' || oErrorObj.sSeverityCode, sCurrentName);

        IF oErrorObj.sSeverityCode <> Stb$typedef.cnNoError THEN
          RETURN oErrorObj;
        END IF;

        -- create the user in iStudyReporter
        -- write the properties
        oErrorObj := Createnewuser (olParameterout, UPPER (olParameter (1).sValue), UPPER (oParameter.sToken));
        
        -- write the default group into userlink
        OPEN cGetUserLink(STB$UTIL.getProperty(olParameter, 'DATAGROUP'), STB$UTIL.getProperty(olParameter, 'USERNO'));
        FETCH cGetUserLink INTO rGetUserLink; 
        IF cGetUserLink%NOTFOUND THEN
          oErrorObj := Stb$audit.Openaudit ('GROUPAUDIT', STB$UTIL.getProperty(olParameter, 'DATAGROUP'));
          oErrorObj := Stb$audit.Addauditrecord ( 'CAN_ALTER_MEMBERSHIP' );
          oErrorObj := Stb$audit.Addauditentry (STB$UTIL.getProperty(olParameter, 'FULLNAME'), 'T', 'F', 1);
          oErrorObj := Stb$audit.silentAudit;
          INSERT INTO Stb$userlink (Entryid, Usergroupno, Userno)
               VALUES (Seq$stb$userlink.NEXTVAL, STB$UTIL.getProperty(olParameter, 'DATAGROUP'), STB$UTIL.getProperty(olParameter, 'USERNO'));
          -- ISRC-1044 removed
        END IF; 
        CLOSE cGetUserLink;
        
        COMMIT;
        RETURN oErrorObj;
      --=========================CAN_CHANGE_USER_PASSWORD=========================
    WHEN UPPER (oParameter.sToken) = 'CAN_CHANGE_USER_PASSWORD' THEN
        -- sys function to alter any  user
        isr$trace.debug (olParameter (1).sValue,  olParameter (2).sValue, sCurrentName);

        BEGIN
          Isr$dbuser_syn.Changepasswordisruser (olParameter (1).sValue, olParameter (2).sValue);
          isr$trace.debug (olParameter (1).sValue,  olParameter (2).sValue, sCurrentName);
        EXCEPTION
          WHEN OTHERS THEN
            isr$trace.debug (olParameter (1).sValue, 'STB$USERSECURITY.putParameters3', olParameter (2).sValue, sCurrentName);
            RAISE exPasswordError;
        END;

        oErrorObj := Stb$audit.Openaudit ('USERAUDIT', Stb$object.getCurrentNodeId);
        oErrorObj := Stb$audit.Addauditrecord ( oParameter.sToken );
        oErrorObj := Stb$audit.Addauditentry ('PASSWORD_RESET', 'xxxxxx', 'xxxxxx', 1);

      --=========================CAN_ADD_USER/CAN_MODIFY_USER_DATA/CAN_MODIFY_OWN_USER_DATA=========================
    WHEN UPPER (oParameter.sToken) = 'CAN_ADD_USER' 
      OR UPPER (oParameter.sToken) = 'CAN_MODIFY_USER_DATA' 
      OR UPPER (oParameter.sToken) = 'CAN_MODIFY_OWN_USER_DATA'
      OR UPPER (oParameter.sToken) = 'CAN_MODIFY_USER_PARAMETER' THEN
        
        IF UPPER (oParameter.sToken) IN ('CAN_ADD_USER','CAN_MODIFY_USER_DATA')  THEN
            
          nUserNo := TO_NUMBER (STB$UTIL.getProperty(olParameter, 'USERNO'));
          nDataGroup := TO_NUMBER (STB$UTIL.getProperty(olParameter, 'DATAGROUP'));

          -- write the default group into userlink
          OPEN cGetUserLink(nDataGroup, nUserNo);
          FETCH cGetUserLink INTO rGetUserLink; 
          IF cGetUserLink%NOTFOUND THEN
            oErrorObj := Stb$audit.Openaudit ('GROUPAUDIT', nDataGroup);
            oErrorObj := Stb$audit.Addauditrecord ( 'CAN_ALTER_MEMBERSHIP' );
            oErrorObj := Stb$audit.Addauditentry (STB$UTIL.getProperty(olParameter, 'FULLNAME'), 'T', 'F', 1);
            oErrorObj := Stb$audit.silentAudit;
            
            INSERT INTO Stb$userlink (Entryid, Usergroupno, Userno)
                 VALUES (Seq$stb$userlink.NEXTVAL, nDataGroup, nUserNo);
                 
            -- ISRC-1044 removed                
          END IF; 
          CLOSE cGetUserLink;
        END IF;
        
        -- create user parameter  
        MERGE INTO STB$USERPARAMETER a
        USING (SELECT sParameter 
                 FROM table(olParameter)
                WHERE type != 'TABSHEET'
                MINUS 
               SELECT column_name
                 FROM user_tab_columns
                WHERE table_name = 'STB$USER') b
        ON (a.parametername = b.sParameter and
            a.userno = CASE WHEN UPPER (oParameter.sToken) = 'CAN_MODIFY_OWN_USER_DATA' THEN STB$SECURITY.getCurrentUser ELSE TO_NUMBER(STB$OBJECT.getCurrentNodeid) END)
        WHEN NOT MATCHED THEN
           INSERT (PARAMETERNAME, USERNO)
           VALUES (b.sParameter, CASE WHEN UPPER (oParameter.sToken) = 'CAN_MODIFY_OWN_USER_DATA' THEN STB$SECURITY.getCurrentUser ELSE TO_NUMBER(STB$OBJECT.getCurrentNodeid) END);   
        
        oErrorObj := putuserparameters (olParameter, UPPER (oParameter.sToken));
        IF oErrorObj.sSeverityCode <> Stb$typedef.cnNoError THEN
          RETURN oErrorObj;
        END IF;
        
        sPrompted := 'PROMPTED_USER_AUDIT';

        IF UPPER (oParameter.sToken) = 'CAN_MODIFY_OWN_USER_DATA' THEN
          ISR$TREE$PACKAGE.selectCurrent('F');
        ElSIF UPPER (oParameter.sToken) IN ('CAN_MODIFY_USER_DATA', 'CAN_MODIFY_USER_PARAMETER') THEN  
          ISR$TREE$PACKAGE.selectCurrent;
        END IF;

        -- check if the sysnonyms will be created through iStudyReporter
        IF UPPER (oParameter.sToken) = 'CAN_ADD_USER' THEN
          OPEN cHasPrivilege;
          FETCH cHasPrivilege INTO rHasPrivilege;
          IF cHasPrivilege%FOUND THEN
            sReturn := Createsyn (Stb$util.Getsystemparameter ('ISRROLE'), STB$UTIL.getProperty(olParameter, 'ORACLEUSERNAME'), Stb$util.Getsystemparameter ('STBOWNER'));
            isr$trace.debug (' sReturn',  sReturn, sCurrentName);
            IF sReturn IS NOT NULL THEN
              CLOSE cHasPrivilege;
              RAISE exSynonymError;
            END IF;
          ELSE
            isr$trace.debug ('sAnySynonym',  sAnySynonym, sCurrentName);
          END IF;
          CLOSE cHasPrivilege;
        END IF;
        
      --=========================FUNCTION-/DATAGROUP=========================
    WHEN    UPPER (oParameter.sToken) = 'CAN_ADD_FUNCTION_GROUP'
         OR UPPER (oParameter.sToken) = 'CAN_CLONE_FUNCTION_GROUP'
         OR UPPER (oParameter.sToken) = 'CAN_CLONE_DATA_GROUP'
         OR UPPER (oParameter.sToken) = 'CAN_ADD_DATA_GROUP'
         OR UPPER (oParameter.sToken) = 'CAN_MODIFY_DATA_GROUP'
         OR UPPER (oParameter.sToken) = 'CAN_MODIFY_FUNCTION_GROUP' THEN

        oErrorObj := Putgroupparameters (olParameter, UPPER (oParameter.sToken));
        IF oErrorObj.sSeverityCode <> Stb$typedef.cnNoError THEN
          RETURN oErrorObj;
        END IF;

        sPrompted := 'PROMPTED_GROUP_AUDIT';

        if UPPER (oParameter.sToken) = 'CAN_MODIFY_DATA_GROUP'
        or UPPER (oParameter.sToken) = 'CAN_MODIFY_FUNCTION_GROUP' then
          ISR$TREE$PACKAGE.reloadPrevNodesAndSelectCurren(-1, 'F');
        end if;

      --=========================CAN_DISPLAY_USERAUDIT=========================
    WHEN UPPER (oParameter.sToken) like 'CAN_DISPLAY_%AUDIT' THEN

        OPEN cGetJobIds ('CAN_DISPLAY_AUDIT');
        FETCH cGetJobIds
        INTO  nOutputId, nActionId, sFunction;
        CLOSE cGetJobIds;
        
        if nActionId is null then
          sMsg := utd$msglib.getmsg ('exNotFindAction', stb$security.getcurrentlanguage, oParameter.sToken);
          raise stb$job.exNotFindAction;
        end if;

        SELECT STB$JOB$SEQ.NEXTVAL
        INTO   STB$JOB.nJobid
        FROM   DUAL;
        
        oErrorObj := Stb$util.createFilterParameter (olParameter);

        STB$JOB.setJobParameter ('TOKEN', Upper (oParameter.sToken), STB$JOB.nJobid);
        STB$JOB.setJobParameter ('OUTPUTID', nOutputId, STB$JOB.nJobid);
        STB$JOB.setJobParameter ('ACTIONID', nActionId, STB$JOB.nJobid);
        Stb$job.Setjobparameter ('REFERENCE', Stb$object.getCurrentNodeId, STB$JOB.nJobid);
        Stb$job.Setjobparameter ('FUNCTION', Sfunction, STB$JOB.nJobid);
        oErrorObj := Stb$job.Startjob;
      ELSE
        RAISE exNotExists;
    END CASE;

    -- check if there have been changes
    IF (Stb$audit.Getmodifiedflag = 'F') THEN
      isr$trace.debug (oParameter.sToken,  'no changes', sCurrentName);
      COMMIT;
      ISR$TREE$PACKAGE.sendSyncCall;
      RETURN oErrorObj;
    END IF;

    IF Stb$util.checkPromptedAuditRequired(sPrompted) = 'T' THEN
      RAISE exAudit;
    -- commit will be in the audit window
    ELSE
      --Silent Audit
      -- write the silent audit and calculate the checksum
      oErrorObj := Stb$audit.Silentaudit;
      -- commit the audit
      COMMIT;
      ISR$TREE$PACKAGE.sendSyncCall;
    END IF;

    isr$trace.stat('end', 'olParameter.count: '||olParameter.count, sCurrentName);
    RETURN oErrorObj;
  EXCEPTION
    WHEN exSynonymError THEN
      oErrorObj.sErrorCode := Utd$msglib.Getmsg ('exSynonymError', Stb$security.Getcurrentlanguage);
      oErrorObj.handleError ( Stb$typedef.cnSeverityMessage, utd$msglib.getmsg ('exSynonymErrorText', stb$security.getCurrentlanguage,  csP1 => sqlerrm(sqlcode)), 
        sCurrentName, dbms_utility.format_error_stack ||chr(10)|| dbms_utility.format_error_backtrace );
      RETURN oErrorObj;
    WHEN exPasswordError THEN
      oErrorObj.sErrorCode := Utd$msglib.Getmsg ('exPasswordError', Stb$security.Getcurrentlanguage);
      oErrorObj.handleError ( Stb$typedef.cnSeverityMessage, utd$msglib.getmsg ('exPasswordErrorText', stb$security.getCurrentlanguage,  csP1 => sqlerrm(sqlcode)), 
        sCurrentName, dbms_utility.format_error_stack ||chr(10)|| dbms_utility.format_error_backtrace );
      RETURN oErrorObj;
    WHEN exAudit THEN
      oErrorObj.sErrorCode := 'exAudit';
      oErrorObj.handleError ( Stb$typedef.cnAudit,  'Call the mask for the audit window', 
        sCurrentName, dbms_utility.format_error_stack ||chr(10)|| dbms_utility.format_error_backtrace );
      RETURN oErrorObj;
    WHEN exNotExists THEN
      oErrorObj.sErrorCode := utd$msglib.getmsg ('exNotExists', stb$security.getcurrentlanguage);
      oErrorObj.handleError ( Stb$typedef.cnSeverityMessage, utd$msglib.getmsg ('exNotExistsText', stb$security.getCurrentlanguage,  csP1 => sqlerrm(sqlcode)), 
        sCurrentName, dbms_utility.format_error_stack ||chr(10)|| dbms_utility.format_error_backtrace );
      RETURN oErrorObj;
    when stb$job.exNotFindAction then
      oErrorObj.handleError ( Stb$typedef.cnSeverityCritical, sMsg, sCurrentName, dbms_utility.format_error_stack ||stb$typedef.crlf|| dbms_utility.format_error_backtrace );
      return oErrorObj;
    WHEN OTHERS THEN
    ROLLBACK;
      oErrorObj.handleError ( Stb$typedef.cnSeverityWarning, utd$msglib.getmsg ('exUnexpectedError', stb$security.getCurrentlanguage,  csP1 => sqlerrm(sqlcode)), 
        sCurrentName, dbms_utility.format_error_stack ||chr(10)|| dbms_utility.format_error_backtrace );
      RETURN oErrorObj;
  END Putparameters;

  --*************************************************************************************************************************
  function setMenuAccess( sParameter   in  varchar2, 
                          sMenuAllowed out varchar2 )
    return Stb$oerror$record 
  is
    oErrorObj                     Stb$oerror$record := STB$OERROR$RECORD();
    sCurrentName                  constant varchar2(100) := $$PLSQL_UNIT||'.setMenuAccess('||sParameter||')';
    sActive                       VARCHAR2 (1);
    sIsLDAPSystem                 VARCHAR2 (1);
    sIsLDAPUser                   VARCHAR2 (1);
    sNodetype                     varchar2(2000);
    sSelectedGroup                varchar2(100);
    oCurNode                      STB$TREENODE$RECORD;

    CURSOR cGetactiveuser IS
      SELECT activeuser
      FROM   stb$user
      WHERE  userno = Stb$object.getCurrentNodeId;
      
    CURSOR cIsLDAPSystem IS
      SELECT 'T'
      FROM   isr$server
      WHERE  servertypeid = 5;
      
    CURSOR cIsLDAPUser(cnUserNo in NUMBER) IS
      SELECT 'T'
        FROM stb$user
       WHERE userno = cnUserNo
         AND ldapusername IS NOT NULL;
    
    CURSOR cReportUserGroup(cnDataGroup in NUMBER) IS
      SELECT count(*) 
        FROM stb$report
       WHERE datagroup = cnDataGroup;
     nReportCnt   number;
     
    cursor curUsrGrpMemberCount( ccnGroupName varchar2 ) is 
      select count(1) counted
      from   stb$usergroup  grp
      join   stb$userlink   ulink
      on     ulink.usergroupno =  grp.usergroupno
      and    grp.groupname = ccnGroupName;     
     
 BEGIN
    isr$trace.stat('end','sParameter: '||sParameter, sCurrentName);
    
    sNodeType := stb$object.getCurrentNodetype;
    isr$trace.debug('value','sNodetype: '||sNodeType,sCurrentName);
    
    oCurNode := STB$OBJECT.getCurrentNode;
    sSelectedGroup := oCurNode.sDisplay;
    isr$trace.debug('value','sSelectedGroup: '||sSelectedGroup,sCurrentName);
    
    sMenuAllowed := 'T';
    
    CASE

      WHEN sParameter = 'CAN_DEACTIVATE_USER' THEN

        OPEN cGetActiveUser;
        FETCH cGetActiveUser INTO sMenuAllowed;
        CLOSE cGetActiveUser;

      WHEN sParameter = 'CAN_REACTIVATE_USER' THEN

        OPEN cGetActiveUser;
        FETCH cGetActiveUser INTO sActive;
        CLOSE cGetActiveUser;      
        
        IF sActive = 'F' THEN
          sMenuAllowed := 'T';        
        ELSE
          sMenuAllowed := 'F';
        END IF;
      
      WHEN sParameter = 'CAN_ADD_USER_WITH_DB' THEN
      
        OPEN cIsLDAPSystem;
        FETCH cIsLDAPSystem INTO sIsLDAPSystem;
        CLOSE cIsLDAPSystem;
        IF NVL(sIsLDAPSystem, 'F') = 'T' THEN -- is LDAP system
          sMenuAllowed := 'F';
        ELSE
          sMenuAllowed := NVL(Stb$util.Getsystemparameter ('DATABASEUSER_CREATED_BY_ISR'), 'F');
        END IF;     
             
      --because of BIBC-597
     WHEN sParameter like 'CAN_CHANGE_%_PASSWORD' THEN
       sMenuAllowed := 'F';
  
      WHEN sParameter = 'CAN_DELETE_DATA_GROUP' THEN        
        OPEN cReportUserGroup(Stb$object.getCurrentNodeId);
        FETCH cReportUserGroup INTO nReportCnt;
        CLOSE cReportUserGroup; 
        
        if nReportCnt > 0 then
          sMenuAllowed := 'F';
        elsif sNodetype = 'DATAGROUP' then
          -- if group has any members, then "delete group functionality" is disabled, see [ARDIS-455]
          for rec in curUsrGrpMemberCount(sSelectedGroup) loop
            if rec.counted > 0 then
              sMenuAllowed := 'F';
            end if;
          end loop;
        end if;

      when sNodetype = 'FUNCTIONGROUP' and sParameter = 'CAN_DELETE_FUNCTION_GROUP'  then
        -- if group has any members, then "delete group functionality" is disabled, see [ARDIS-455]
        for rec in curUsrGrpMemberCount(sSelectedGroup) loop
          if rec.counted > 0 then
            sMenuAllowed := 'F';
          end if;
        end loop;

      ELSE
        sMenuAllowed :=  'T';
        
    END CASE;

    isr$trace.stat('end','sMenuAllowed: '||sMenuAllowed, sCurrentName);
    return oErrorObj;
  EXCEPTION
    WHEN OTHERS THEN
      oErrorObj.handleError ( Stb$typedef.cnSeverityCritical, utd$msglib.getmsg ('exUnexpectedError', stb$security.getCurrentlanguage,  csP1 => sqlerrm(sqlcode)), 
        sCurrentName, dbms_utility.format_error_stack ||chr(10)|| dbms_utility.format_error_backtrace );
      RETURN oErrorObj;
  END Setmenuaccess;

  --*****************************************************************************************************************************
  FUNCTION Getauditmask (olNodeList OUT Stb$treenodelist)
    RETURN Stb$oerror$record IS
    sCurrentName   constant varchar2(4000) := $$PLSQL_UNIT||'.Getauditmask';
    oErrorObj                     Stb$oerror$record := STB$OERROR$RECORD();
  BEGIN
    isr$trace.stat('begin', 'begin', sCurrentName);
    oErrorObj := Stb$audit.Getauditmask (olNodeList);
    isr$trace.stat('end', 'end', sCurrentName);
    RETURN (oErrorObj);
  EXCEPTION
    WHEN OTHERS THEN
      ROLLBACK;
      oErrorObj.handleError ( Stb$typedef.cnSeverityCritical, utd$msglib.getmsg ('exUnexpectedError', stb$security.getCurrentlanguage,  csP1 => sqlerrm(sqlcode)), 
        sCurrentName, dbms_utility.format_error_stack ||chr(10)|| dbms_utility.format_error_backtrace );
      RETURN (oErrorObj);
  END Getauditmask;

  --*****************************************************************************************************
  FUNCTION PutAuditmask (olParameter IN STB$PROPERTY$LIST)
    RETURN Stb$oerror$record IS
    oErrorObj                     Stb$oerror$record := STB$OERROR$RECORD();
    sCurrentName   constant varchar2(4000) := $$PLSQL_UNIT||'.PutAuditmask';
  BEGIN
    isr$trace.stat('begin', 'begin', sCurrentName);
    -- AuditReason will be written in the filed reason of the current Audit Id
    oErrorObj := Stb$audit.Putauditmask (olParameter);
    isr$trace.stat('end', 'end', sCurrentName);
    RETURN (oErrorObj);
  EXCEPTION
    WHEN OTHERS THEN
      ROLLBACK;
      oErrorObj.handleError ( Stb$typedef.cnSeverityCritical, utd$msglib.getmsg ('exUnexpectedError', stb$security.getCurrentlanguage,  csP1 => sqlerrm(sqlcode)), 
        sCurrentName, dbms_utility.format_error_stack ||chr(10)|| dbms_utility.format_error_backtrace );
      RETURN (oErrorObj);
  END Putauditmask;
  
  --*****************************************************************************************************
  FUNCTION checkOutDirect(cnUserNo IN NUMBER)  RETURN VARCHAR2 IS
    sCurrentName   constant varchar2(4000) := $$PLSQL_UNIT||'.checkOutDirect('||cnUserNo||')';
    sReturn VARCHAR2(1) := 'F';
    
    CURSOR cGetCheckoutDirect IS
      select NVL(checkoutdoc, 'F')
        from stb$user
       where userno = cnUserNo;
    
  BEGIN
    isr$trace.stat('begin', 'begin', sCurrentName);
    OPEN cGetCheckoutDirect;
    FETCH cGetCheckoutDirect INTO sReturn;
    CLOSE cGetCheckoutDirect;
    isr$trace.stat('end', 'end', sCurrentName);
    RETURN (sReturn);
  END checkOutDirect;  

  --*********************************************************************************************************************************
  FUNCTION Getcontextmenu (Nmenu IN NUMBER, Olmenuentrylist OUT Stb$menuentry$list)
    RETURN Stb$oerror$record IS
    oErrorObj                     Stb$oerror$record := STB$OERROR$RECORD();
    sCurrentName   constant varchar2(4000) := $$PLSQL_UNIT||'.Getcontextmenu';
    sIsLDAPSystem                 VARCHAR2 (1);
    sIsLDAPUser                   VARCHAR2 (1);

    CURSOR cIsLDAPSystem IS
      SELECT 'T'
      FROM   isr$server
      WHERE  servertypeid = 5;
      
    CURSOR cIsLDAPUser(cnUserNo in NUMBER) IS
      SELECT 'T'
        FROM stb$user
       WHERE userno = cnUserNo
         AND ldapusername IS NOT NULL;    
  BEGIN
    isr$trace.stat('begin', 'begin', sCurrentName);
    oErrorObj := Stb$util.Getcontextmenu (Stb$object.Getcurrentnodetype, Nmenu, Olmenuentrylist);
    
    FOR i IN 1..olMenuEntryList.Count() LOOP
    
      CASE
    
        WHEN olMenuEntryList(i).sTOKEN = 'CAN_ADD_USER_WITH_DB' THEN
          OPEN cIsLDAPSystem;
          FETCH cIsLDAPSystem INTO sIsLDAPSystem;
          CLOSE cIsLDAPSystem;
          IF NVL(sIsLDAPSystem, 'F') = 'T'
          OR NVL(Stb$util.Getsystemparameter ('DATABASEUSER_CREATED_BY_ISR'), 'F') = 'F' THEN -- is LDAP system
            olMenuEntryList(i).sType := 'DELETE';
          END IF;          
        WHEN olMenuEntryList(i).sTOKEN = 'CAN_CHANGE_USER_PASSWORD' THEN
          OPEN cIsLDAPUser(Stb$object.getCurrentNodeId);
          FETCH cIsLDAPUser INTO sIsLDAPUser;
          CLOSE cIsLDAPUser;
          IF NVL(sIsLDAPUser, 'F') = 'T'
          OR NVL(Stb$util.Getsystemparameter ('ISRADMIN_CAN_CHANGE_USER_PASSWORD'), 'F') = 'F' THEN -- is LDAP user
            olMenuEntryList(i).sType := 'DELETE';
          END IF;
        WHEN olMenuEntryList(i).sTOKEN = 'CAN_CHANGE_OWN_PASSWORD' THEN          
          OPEN cIsLDAPUser(Stb$security.getCurrentUser);
          FETCH cIsLDAPUser INTO sIsLDAPUser;
          CLOSE cIsLDAPUser;
          IF NVL(sIsLDAPUser, 'F') = 'T' THEN -- is LDAP user
            olMenuEntryList(i).sType := 'DELETE';
          END IF;
          
         ELSE
           NULL;
           
       END CASE;
    END LOOP;
    
    isr$trace.stat('end', 'end', sCurrentName);
    RETURN (oErrorObj);
  EXCEPTION
    WHEN OTHERS THEN
      ROLLBACK;
      oErrorObj.handleError ( Stb$typedef.cnSeverityCritical, utd$msglib.getmsg ('exUnexpectedError', stb$security.getCurrentlanguage,  csP1 => sqlerrm(sqlcode)), 
        sCurrentName, dbms_utility.format_error_stack ||chr(10)|| dbms_utility.format_error_backtrace );
      RETURN (oErrorObj);
  END Getcontextmenu;
  
--**********************************************************************************************************************
FUNCTION checkDependenciesOnMask( oParameter    in     STB$MENUENTRY$RECORD,
                                  olParameter   in out STB$PROPERTY$LIST,
                                  sModifiedFlag out    varchar2 )
  return STB$OERROR$RECORD
is
  oErrorObj           STB$OERROR$RECORD := STB$OERROR$RECORD();
  sCurrentName        constant varchar2(100) := $$PLSQL_UNIT||'.checkDependenciesOnMask('||oParameter.sToken||')';
  olParameterOld      STB$PROPERTY$LIST := STB$PROPERTY$LIST();
 
  CURSOR cGetLDAPServer IS
    SELECT *
      FROM isr$server
     WHERE servertypeid = 5;  
    
  rGetLDAPServer    cGetLDAPServer%rowtype;
             
BEGIN
  isr$trace.stat('begin','oParameter, see column LOGCLOB',xmltype(oParameter).getClobVal());

  olParameterOld := olParameter;
  
    FOR i IN 1..olParameter.COUNT() LOOP
      -- do not open cursor for all parameters but only for affected ones       
      IF olParameter(i).sParameter in ('LDAPUSERNAME', 'ORACLEUSERNAME') THEN        
      OPEN cGetLDAPServer;
      FETCH cGetLDAPServer INTO rGetLDAPServer;
      IF cGetLDAPServer%FOUND THEN
        IF STB$OBJECT.getCurrentNodeid = 1 THEN
          IF olParameter(i).sParameter = 'LDAPUSERNAME' THEN
            olParameter(i).sDisplayed := 'F';
            olParameter(i).sRequired := 'F';
            olParameter(i).sValue := null;
            olParameter(i).sDisplay := null;
          END IF;         
        ELSE      
          IF olParameter(i).sParameter = 'ORACLEUSERNAME' THEN
            olParameter(i).sDisplayed := 'F';
            olParameter(i).sRequired := 'F';        
            olParameter(i).sValue := null;
            olParameter(i).sDisplay := null;
          END IF;
        END IF;
      ELSE
        IF olParameter(i).sParameter = 'LDAPUSERNAME' THEN
          olParameter(i).sDisplayed := 'F';
          olParameter(i).sRequired := 'F';
          olParameter(i).sValue := null;
          olParameter(i).sDisplay := null;
        END IF;    
      END IF;
      CLOSE cGetLDAPServer;
      END IF;
    END LOOP;  
  
  -- compare the objects if something changed only then refresh the mask
  SELECT CASE WHEN COUNT ( * ) = 0 THEN 'F' ELSE 'T' END
    INTO sModifiedFlag
    FROM (SELECT XMLTYPE (VALUE (p)).getStringVal ()
            FROM table (olParameter) p
          MINUS
          SELECT XMLTYPE (VALUE (p)).getStringVal ()
            FROM table (olParameterOld) p); 
  
  isr$trace.stat('end','sModifiedFlag: '||sModifiedFlag, sCurrentName);
  return(oErrorObj);

EXCEPTION
  WHEN OTHERS then
      oErrorObj.handleError ( Stb$typedef.cnSeverityWarning, utd$msglib.getmsg ('exUnexpectedError', stb$security.getCurrentlanguage,  csP1 => sqlerrm(sqlcode)), 
        sCurrentName, dbms_utility.format_error_stack ||chr(10)|| dbms_utility.format_error_backtrace );
    RETURN(oErrorObj);
END checkDependenciesOnMask;


  --*********************************************************************************************************************
  FUNCTION getUserParameter (nUserNo IN NUMBER, sParameterName IN VARCHAR2, sParameterValue OUT VARCHAR2)
    RETURN stb$oerror$record IS
    oerrorobj                     stb$oerror$record := STB$OERROR$RECORD();
    sCurrentName   constant varchar2(4000) := $$PLSQL_UNIT||'.getUserParameter('||nUserNo||'sParameterName,sParameterValue)';
    
    -- cursor to retrieve the value of a single parametervalue
    CURSOR cGetUserParameter (nUserNo IN NUMBER, sparametername IN VARCHAR2) IS
      SELECT parametervalue
        FROM stb$userparameter
       WHERE userno = nUserNo
         AND parametername = sParameterName;
  BEGIN
    isr$trace.stat('begin', 'begin', sCurrentName);
    OPEN cGetUserParameter (nUserNo, sParameterName);
    FETCH cGetUserParameter INTO sparametervalue;
    IF cGetUserParameter%NOTFOUND THEN
      isr$trace.debug ('Parameter not found',  nUserNo||' '||sParameterName, sCurrentName);
    END IF;
    CLOSE cGetUserParameter;
    isr$trace.stat('end', 'end', sCurrentName);
    RETURN oerrorobj;
  EXCEPTION
    WHEN OTHERS THEN
      oErrorObj.handleError ( Stb$typedef.cnSeverityCritical, utd$msglib.getmsg ('exUnexpectedError', stb$security.getCurrentlanguage,  csP1 => sqlerrm(sqlcode)), 
        sCurrentName, dbms_utility.format_error_stack ||chr(10)|| dbms_utility.format_error_backtrace );
      RETURN oerrorobj;
  END getUserParameter;
  
  --*********************************************************************************************************************  
  FUNCTION getUserParameter (nUserNo IN NUMBER, sParameterName IN VARCHAR2) RETURN VARCHAR2 IS    
    sCurrentName   constant varchar2(4000) := $$PLSQL_UNIT||'.getUserParameter('||nUserNo||'sParameterName)';
    sParameterValue  stb$userparameter.parametervalue%TYPE;
    oerrorobj        stb$oerror$record := STB$OERROR$RECORD();
  BEGIN
    isr$trace.stat('begin', 'begin', sCurrentName);  
    oerrorobj := getUserParameter( nUserNo, sParameterName, sparametervalue);
    isr$trace.stat('end', 'end', sCurrentName);
    RETURN sParameterValue;
  END getUserParameter;

  
END STB$USERSECURITY; 