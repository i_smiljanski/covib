CREATE OR REPLACE PACKAGE BODY ISR$LDAP
is

--******************************************************************************
FUNCTION getLDAPInformation(sHost IN VARCHAR2
                          , sPort IN VARCHAR2
                          , sSSLAuth IN VARCHAR2 DEFAULT 0
                          , sSSLWrl IN VARCHAR2 DEFAULT null
                          , sSSLWalletPasswd IN VARCHAR2 DEFAULT null
                          , sGlobUserName IN VARCHAR2
                          , sGlobPassword IN VARCHAR2
                          , sLDAPBase IN VARCHAR2 DEFAULT NULL
                          , sAttributes IN VARCHAR2 DEFAULT '*'
                          , sFilter IN VARCHAR2 DEFAULT 'objectclass=*'
                          , sPassword IN VARCHAR2 DEFAULT NULL) 
return CLOB
is
  oErrorObj         STB$OERROR$RECORD := STB$OERROR$RECORD();

  sCurrentName  constant varchar2(100) := $$PLSQL_UNIT||'.getLDAPInformation';  
  clReturn          CLOB := EMPTY_CLOB();
  
  v_ldap_session    DBMS_LDAP.SESSION;
  v_ldapconn_admin  binary_integer;
  v_ldapconn        binary_integer;
  res_attrs         DBMS_LDAP.STRING_COLLECTION;
  retval            PLS_INTEGER;
  res_message       DBMS_LDAP.MESSAGE;
  
  sUserDn           VARCHAR2(4000);  
  
  nCounter          NUMBER := 0;
  
  my_entry          DBMS_LDAP.message;      

  l_attr_name       VARCHAR2(256);
  l_ber_element     DBMS_LDAP.ber_element;
  l_vals            DBMS_LDAP.string_collection;  
  
  PROCEDURE addToClob(sText IN VARCHAR2) IS
  BEGIN
    DBMS_LOB.WRITEAPPEND(clReturn, LENGTH(sText), sText);
  END;  
  
BEGIN
  isr$trace.stat('begin','Function '||sCurrentName||' started.', sCurrentName);

  DBMS_LOB.createTemporary( clReturn, TRUE );
  DBMS_LOB.open( clReturn, DBMS_LOB.LOB_READWRITE );
      
  ISR$TRACE.debug_all('Starting ...', '', 'ISR$LDAP.getLDAPInformation');
      
  v_ldap_session := DBMS_LDAP.INIT(
      HOSTNAME => sHost
    , PORTNUM  => sPort
  );
  
  isr$trace.info('Opened session to server', sHost||':'||sPort, sCurrentName);
  
  -- SSL
  IF NVL(sSSLAuth, '0') != '0' THEN      
    v_ldapconn_admin := DBMS_LDAP.OPEN_SSL(
        LD              => v_ldap_session
      , SSLWRL          => sSSLWrl                                    --'file:/pfad/zum/Oracle/Wallet/Verzeichnis'
      , SSLWALLETPASSWD => STB$JAVA.decryptPassword(sSSLWalletPasswd) --'{Oracle-Wallet-Passwort}'
      , SSLAUTH         => sSSLAuth                                   -- if 1 then the two parameters above remain empty else 2 or 3
    );
    isr$trace.info('Opened ssl', sSSLWrl, sCurrentName);
  END IF;  
      
  v_ldapconn_admin := DBMS_LDAP.SIMPLE_BIND_S(
      LD => v_ldap_session
    , DN => sGlobUserName
    , PASSWD => STB$JAVA.decryptPassword(sGlobPassword)
  );
  
  isr$trace.info('Connected to global user', sGlobUserName, sCurrentName);
  
  -- further information, otherwise check connection only
  IF sLDAPBase IS NOT NULL THEN
    isr$trace.info('sLDAPBase', sLDAPBase, sCurrentName); 
    isr$trace.info('sFilter', sFilter, sCurrentName);  
    res_attrs(1) := sAttributes;

    retval := DBMS_LDAP.SEARCH_S(
        ld         =>  v_ldap_session
      , base       =>  sLDAPBase
      , scope      =>  DBMS_LDAP.SCOPE_SUBTREE
      , filter     =>  nvl(sFilter, 'objectclass=*')
      , attrs      =>  res_attrs
      , attronly   =>  0
      , res        =>  res_message
    );
    
    isr$trace.info('Got attributes of global user', sAttributes||':'||sFilter, sCurrentName);
    
    nCounter := DBMS_LDAP.COUNT_ENTRIES(v_ldap_session, res_message);
    
    ISR$TRACE.info('Found entries of global user', nCounter, sCurrentName);
        
    IF nCounter > 0 THEN
    
      -- check user and passord
      IF sPassword IS NOT NULL THEN
    
        sUserDn := DBMS_LDAP.get_dn(v_ldap_session, RAWTOHEX(res_message));
      
        ISR$TRACE.info('Long username of connecting user: '|| sUserDn, '', 'ISR$LDAP.getLDAPInformation');
         
        BEGIN 
          v_ldapconn := DBMS_LDAP.SIMPLE_BIND_S(
              LD => v_ldap_session
            , DN => sUserDn
            , PASSWD => sPassword
          );
          
          addToClob('T');
        EXCEPTION WHEN OTHERS THEN
          addToClob('<CONNECTIONERROR>'||sqlerrm||'</CONNECTIONERROR>');
        END;
        
      -- show ads information
      ELSE 
      
        addToClob('<ROWSET>'); 
      
        my_entry := DBMS_LDAP.first_entry (v_ldap_session, res_message);
        WHILE my_entry IS NOT NULL LOOP

          addToClob('<ROW>');
          l_attr_name := DBMS_LDAP.first_attribute(ld        => v_ldap_session,
                                                      ldapentry => my_entry,
                                                      ber_elem  => l_ber_element);
          WHILE l_attr_name IS NOT NULL LOOP
            -- Get all the values for this attribute.
            l_vals := DBMS_LDAP.get_values (ld        => v_ldap_session,
                                               ldapentry => my_entry,
                                               attr      => l_attr_name);
            FOR i IN l_vals.FIRST .. l_vals.LAST LOOP
              IF (l_attr_name not in ('objectGUID', 'objectSid')) THEN
                addToClob('<'||l_attr_name||'><![CDATA['||SUBSTR(l_vals(i),1,200)||']]></'||l_attr_name||'>');
              END IF;
            END LOOP;
            l_attr_name := DBMS_LDAP.next_attribute(ld        => v_ldap_session,
                                                       ldapentry => my_entry,
                                                       ber_elem  => l_ber_element);
          END LOOP;
          addToClob('</ROW>');
          
          -- up to here

          my_entry := DBMS_LDAP.next_entry (v_ldap_session, my_entry);
        END LOOP;     
        
        addToClob('</ROWSET>');  
      
      END IF; 
    ELSE 
      addToClob('<ERROR>No entries found.</ERROR>');
    END IF;
    
          
  ELSE
    addToClob('T');
  END IF;
      
  v_ldapconn_admin := DBMS_LDAP.UNBIND_S(LD => v_ldap_session);
  
  isr$trace.stat('end','end',sCurrentName);
  return clReturn;

exception
  when others then 
    addToClob('<ERROR>'||sqlerrm||'</ERROR>');
    
    oErrorObj.handleError ( Stb$typedef.cnSeverityCritical, 
                     utd$msglib.getmsg ('exUnexpectedError', stb$security.getcurrentlanguage,  sqlerrm(sqlcode)), sCurrentName, 
                     dbms_utility.format_error_stack || CHR(10) || dbms_utility.format_error_backtrace  );                         
    return clReturn;
END getLDAPInformation;

--******************************************************************************
FUNCTION getLDAPStatus(sHost IN VARCHAR2
                          , sPort IN VARCHAR2
                          , sSSLAuth IN VARCHAR2 DEFAULT 0
                          , sSSLWrl IN VARCHAR2 DEFAULT null
                          , sSSLWalletPasswd IN VARCHAR2 DEFAULT null
                          , sGlobUserName IN VARCHAR2
                          , sGlobPassword IN VARCHAR2) 
return CLOB
is
  oErrorObj         STB$OERROR$RECORD := STB$OERROR$RECORD();

  sCurrentName  constant varchar2(100) := $$PLSQL_UNIT||'.getLDAPStatus';  
  clReturn          CLOB := EMPTY_CLOB();
  
  v_ldap_session    DBMS_LDAP.SESSION;
  v_ldapconn_admin  binary_integer;
  
  PROCEDURE addToClob(sText IN VARCHAR2) IS
  BEGIN
    DBMS_LOB.WRITEAPPEND(clReturn, LENGTH(sText), sText);
  END; 
BEGIN
  isr$trace.stat('begin','Function '||sCurrentName||' started.', sCurrentName);

  DBMS_LOB.createTemporary( clReturn, TRUE );
  DBMS_LOB.open( clReturn, DBMS_LOB.LOB_READWRITE );
      
  ISR$TRACE.debug_all('Starting ...', '', sCurrentName);
      
  v_ldap_session := DBMS_LDAP.INIT(
      HOSTNAME => sHost
    , PORTNUM  => sPort
  );
  
  isr$trace.info('Opened session to server', sHost||':'||sPort, sCurrentName);
  
  -- SSL
  IF NVL(sSSLAuth, '0') != '0' THEN      
    v_ldapconn_admin := DBMS_LDAP.OPEN_SSL(
        LD              => v_ldap_session
      , SSLWRL          => sSSLWrl                                    --'file:/pfad/zum/Oracle/Wallet/Verzeichnis'
      , SSLWALLETPASSWD => STB$JAVA.decryptPassword(sSSLWalletPasswd) --'{Oracle-Wallet-Passwort}'
      , SSLAUTH         => sSSLAuth                                   -- if 1 then the two parameters above remain empty else 2 or 3
    );
    isr$trace.info('Opened ssl', sSSLWrl, sCurrentName);
  END IF;  
      
  v_ldapconn_admin := DBMS_LDAP.SIMPLE_BIND_S(
      LD => v_ldap_session
    , DN => sGlobUserName
    , PASSWD => STB$JAVA.decryptPassword(sGlobPassword)
  );
  
  isr$trace.info('Connected to global user', sGlobUserName, sCurrentName);  
  addToClob('T');      
  v_ldapconn_admin := DBMS_LDAP.UNBIND_S(LD => v_ldap_session);
  
  isr$trace.stat('end','end',sCurrentName);
  return clReturn;

exception
  when others then 
    addToClob('<ERROR>'||sqlerrm||'</ERROR>');                      
    return clReturn;
END getLDAPStatus;


END ISR$LDAP;