CREATE OR REPLACE PACKAGE BODY ISR$CUSTOMER IS

  CURSOR cCheckNodeExists(csNodeName IN VARCHAR2) IS
    select 'T'
      from isr$custom$grouping
     where nodename = csNodeName;

  sNodeExists        VARCHAR2(1) := 'F';
  sSystemDateFormat  VARCHAR2(20) := STB$UTIL.getSystemParameter('DATEFORMAT');

  --****************************************************************************************************
  PROCEDURE destroyOldValues IS
  BEGIN
    STB$OBJECT.setCurrentToken('');
  END destroyOldValues;

  --**************************************************************************************************************************
  FUNCTION getConditionAsSQL (nNodeId IN ISR$CUSTOM$GROUPING.NODEID%TYPE)
    RETURN VARCHAR2 IS
    sCurrentName   constant varchar2(4000) := $$PLSQL_UNIT||'.getConditionAsSQL('||' '||')';
    sTemp                         ISR$CUSTOM$GROUPING.CONDITION%TYPE;
    sTemp1                        ISR$CUSTOM$GROUPING.CONDITION%TYPE;
    sTemp2                        ISR$CUSTOM$GROUPING.CONDITION%TYPE;
    sTheFormatMask                ISR$CUSTOM$GROUPING.FORMATMASK%TYPE;
    bCanHaveFormatMask            ISR$GROUPING.CANHAVEFORMATMASK%TYPE;
    sGroupingName                 ISR$REPORT$GROUPING.GROUPINGNAME%TYPE;
    sWhere                        VARCHAR2 (4000);

    TYPE tSampleCursor IS REF CURSOR;

    dTempDate                     DATE := NULL;

    CURSOR cCanHaveFormatMask (cnNodeId IN isr$custom$grouping.nodeid%TYPE) IS
      SELECT canhaveformatmask, groupingname, NVL (formatmask, sSystemDateFormat) formatmask, condition
      FROM   isr$grouping, isr$custom$grouping
      WHERE  nodeid = cnNodeId AND groupingname(+) = nodename;

    FUNCTION getDate (sTemp IN OUT ISR$CUSTOM$GROUPING.CONDITION%TYPE, sFormatMask IN ISR$CUSTOM$GROUPING.FORMATMASK%TYPE)
      RETURN DATE IS
      dTempDate                     DATE := NULL;
    BEGIN
      EXECUTE IMMEDIATE 'BEGIN
       SELECT TRUNC(TO_DATE(' || sTemp || ', ''' || sFormatMask || '''), ''' || sFormatMask || ''')
       INTO :1
       FROM dual;
     END;'
      USING             IN OUT dTempDate;
      sTemp := 'TRUNC(TO_DATE(' || sTemp || ', ''' || sFormatMask || '''), ''' || sFormatMask || ''') ';
      RETURN dTempDate;
    EXCEPTION
      WHEN OTHERS THEN
        IF SQLCODE = '-1898' THEN
          sTemp := 'TO_DATE(' || sTemp || ', ''' || sFormatMask || ''') ';
        END IF;

        RETURN dTempDate;
    END getDate;
  BEGIN
    isr$trace.stat('begin', 'begin', sCurrentName);
    OPEN cCanHaveFormatMask (nNodeId);
    FETCH cCanHaveFormatMask
    INTO  bCanHaveFormatMask, sGroupingName, sTheFormatMask, sWhere;
    CLOSE cCanHaveFormatMask;

    isr$trace.debug ('bCanHaveFormatMask',  bCanHaveFormatMask, sCurrentName);
    isr$trace.debug ('sGroupingName',  sGroupingName, sCurrentName);
    isr$trace.debug ('sTheFormatMask',  sTheFormatMask, sCurrentName);
    isr$trace.debug ('sWhere',  sWhere, sCurrentName);

    IF bCanHaveFormatMask = 'T' AND TRIM (sTheFormatMask) IS NULL THEN
      sTheFormatMask := sSystemDateFormat;
      isr$trace.debug ('sTheFormatMask',  sTheFormatMask, sCurrentName);
    END IF;

    CASE
      --=====================LISTE, semicolon separated==============================
    WHEN INSTR (sWhere, ';') != 0 THEN
        IF bCanHaveFormatMask = 'T' THEN
          sTemp := 'TO_CHAR(groupingdate, ''' || sTheFormatMask || ''') IN (';
        ELSE
          sTemp := 'UPPER(groupingvalue) IN (';
        END IF;

        sTemp2 := sWhere;
        isr$trace.debug ('sTemp2',  sTemp2, 225);

        WHILE (INSTR (sTemp2, ';') != 0) LOOP
          IF bCanHaveFormatMask = 'T' THEN
            sTemp1 := '' || TRIM (SUBSTR (sTemp2, 0, INSTR (sTemp2, ';') - 1)) || '';
          ELSE
            sTemp1 := 'UPPER(''' || TRIM (SUBSTR (sTemp2, 0, INSTR (sTemp2, ';') - 1)) || ''')';
          END IF;
          isr$trace.debug ('sTemp1',  sTemp1, 225);

          sTemp2 := TRIM (SUBSTR (sTemp2, INSTR (sTemp2, ';') + 1));
          isr$trace.debug ('sTemp2',  sTemp2, 225);
          sTemp := sTemp || sTemp1 || ',';
          isr$trace.debug ('sTemp',  sTemp, 225);
        END LOOP;

        IF bCanHaveFormatMask = 'T' THEN
          sTemp1 := '' || sTemp2 || '';
        ELSE
          sTemp1 := 'UPPER(''' || sTemp2 || ''')';
        END IF;

        sTemp := sTemp || sTemp1 || ')';
      --=====================less than==============================
    WHEN INSTR (sWhere, '<') != 0 THEN
        sTemp1 := '' || TRIM (SUBSTR (sWhere, INSTR (sWhere, '<') + 1)) || '';

        IF bCanHaveFormatMask = 'T' THEN
          dTempDate := getDate (sTemp1, sTheFormatMask);

          IF (dTempDate IS NOT NULL) THEN
            sTemp := 'TRUNC(groupingdate, ''' || sTheFormatMask || ''')  < ' || sTemp1;
          ELSE
            sTemp := 'groupingdate  < ' || sTemp1;
          END IF;
        ELSE
          sTemp := 'UPPER(groupingvalue) < UPPER(''' || sTemp1 || ''')';
        END IF;
      --=====================more than==============================
    WHEN INSTR (sWhere, '>') != 0 THEN
        sTemp1 := '' || TRIM (SUBSTR (sWhere, INSTR (sWhere, '>') + 1)) || '';

        IF bCanHaveFormatMask = 'T' THEN
          dTempDate := getDate (sTemp1, sTheFormatMask);

          IF (dTempDate IS NOT NULL) THEN
            sTemp := 'TRUNC(groupingdate, ''' || sTheFormatMask || ''')  > ' || sTemp1;
          ELSE
            sTemp := 'groupingdate  > ' || sTemp1;
          END IF;
        ELSE
          sTemp := 'UPPER(groupingvalue) > UPPER(''' || sTemp1 || ''')';
        END IF;
      --=====================between ... and  ...==============================
    WHEN INSTR (sWhere, ':') != 0 THEN
        sTemp1 := '' || TRIM (SUBSTR (sWhere, 0, INSTR (sWhere, ':') - 1)) || '';
        sTemp2 := '' || TRIM (SUBSTR (sWhere, INSTR (sWhere, ':') + 1)) || '';

        IF bCanHaveFormatMask = 'T' THEN
          dTempDate := getDate (sTemp1, sTheFormatMask);
          dTempDate := getDate (sTemp2, sTheFormatMask);

          IF (dTempDate IS NOT NULL) THEN
            sTemp := 'TRUNC(groupingdate, ''' || sTheFormatMask || ''')  BETWEEN ' || sTemp1 || ' AND ' || sTemp2 || '';
          ELSE
            sTemp := 'groupingdate  BETWEEN ' || sTemp1 || ' AND ' || sTemp2 || '';
          END IF;
        ELSE
          sTemp := 'UPPER(groupingvalue) BETWEEN UPPER(''' || sTemp1 || ''') AND UPPER(''' || sTemp2 || ''')';
        END IF;
      --=====================like==============================
    WHEN LENGTH (sWhere) != 0 THEN
        IF bCanHaveFormatMask = 'T' THEN
          sTemp := 'TO_CHAR(groupingdate, ''' || sTheFormatMask || ''') = ''' || sWhere || ''' ';
        ELSE
          sTemp := 'UPPER(groupingvalue) LIKE UPPER(''%' || sWhere || '%'')';
        END IF;
      --=====================nix==============================
    ELSE
        NULL;
    END CASE;

    sWhere := sTemp;

    /*IF bCanHaveFormatMask = 'T' AND TRIM (sWhere) IS NOT NULL THEN
      BEGIN
        sStatement := 'SELECT groupingname, groupingvalue, repid, groupingdate ';
        sStatement := sStatement || 'FROM isr$report$grouping ';
        sStatement := sStatement || 'WHERE groupingname = ''' || REPLACE(sGroupingName, '''', '''''') || ''' ';
        sStatement := sStatement || 'AND ' || sWhere || ' ';
        isr$trace.debug ('sStatement',  sStatement, 70);
        OPEN cSampleCursor FOR sStatement;
        LOOP
          FETCH cSampleCursor
          INTO  rSampleCursor;
          EXIT WHEN cSampleCursor%NOTFOUND;
        END LOOP;
        CLOSE cSampleCursor;
      EXCEPTION
        WHEN OTHERS THEN
          isr$trace.debug ('error',  SQLCODE || ' ' || SQLERRM, 1);
          sWhere := NULL;
      END;
    END IF;*/

    isr$trace.stat('end', 'end', sCurrentName);
    RETURN sWhere;
  EXCEPTION
    WHEN OTHERS THEN
      RETURN NULL;
  END getConditionAsSQL;

  --**************************************************************************************************************************
  FUNCTION getSelectStatement (olValueList IN ISR$VALUE$LIST, sStatement OUT VARCHAR2)
    RETURN STB$OERROR$RECORD IS
    sCurrentName   constant varchar2(4000) := $$PLSQL_UNIT||'.getSelectStatement('||' '||')';
    oErrorObj                     STB$OERROR$RECORD := STB$OERROR$RECORD();
    sGroupingName                 ISR$REPORT$GROUPING.GROUPINGNAME%TYPE;
    sGroupingValue                ISR$REPORT$GROUPING.GROUPINGVALUE%TYPE;
    sFormatMask                   ISR$CUSTOM$GROUPING.FORMATMASK%TYPE;
    sDatagroup                    ISR$CUSTOM$GROUPING.REFERENCE%TYPE;

    CURSOR cGetFormatMask(csNodeId IN VARCHAR2) IS
      select formatmask
      from   isr$parameter$values, isr$custom$grouping
      where  UPPER(value) = UPPER(formatmask)
      and    entity = 'FORMATMASK'
      and    nodeid = csNodeId;

    CURSOR cGetDataGroup(cnNodeId IN isr$custom$grouping.nodeid%TYPE) IS
      SELECT REFERENCE
      FROM   isr$custom$grouping
      WHERE  nodeid = cnNodeId;

    CURSOR cGetGroupingFlag(csGroupingName IN VARCHAR2) IS
      SELECT flag
      FROM isr$grouping
      where groupingname = csGroupingName;

    sFlag ISR$GROUPING.FLAG%TYPE;

  BEGIN
    isr$trace.stat('begin', 'begin', sCurrentName);

    FOR nElements IN 1 .. olValueList.COUNT () LOOP
      isr$trace.debug ('olValueList ('||nElements||').sNodeType',  olValueList (nElements).sNodeType, sCurrentName);
      /*VALUENODE*/
      IF olValueList (nElements).sNodeType = 'VALUENODE' THEN

        sGroupingName := olValueList (nElements).sParametername;
        OPEN cGetGroupingFlag(sGroupingName);
        FETCH cGetGroupingFlag INTO sFlag;
        IF cGetGroupingFlag%FOUND AND sFlag NOT IN ('E', 'P') THEN
          sGroupingValue := olValueList (nElements).sParametervalue;
          sFormatMask := NULL;
          BEGIN
            OPEN cGetFormatMask(olValueList (nElements).sNodeId);
            FETCH cGetFormatMask INTO sFormatMask;
            CLOSE cGetFormatMask;
          EXCEPTION WHEN OTHERS THEN
            sFormatMask := NULL;
          END;
          isr$trace.debug ('sGroupingName',  sGroupingName, sCurrentName);
          isr$trace.debug ('sGroupingValue',  sGroupingValue, sCurrentName);
          isr$trace.debug ('sFormatMask',  sFormatMask, sCurrentName);

          -- ISRC-1044
          sStatement := sStatement || case when TRIM(sStatement) is not null then 'INTERSECT ' end || 'SELECT repid FROM isr$report$grouping$table WHERE ';

          sStatement := sStatement || 'groupingname = ''' || REPLACE(sGroupingName, '''', '''''') || ''' ';
          IF TRIM (sFormatMask) IS NULL THEN
            sStatement := sStatement || 'AND groupingvalue = ''' || REPLACE(sGroupingValue, '''', '''''') || ''' ';
          ELSE
            sStatement := sStatement || 'AND to_char(groupingdate, ''' || sFormatMask || ''') = ''' || sGroupingValue || ''' ';
          END IF;
        END IF;
        CLOSE cGetGroupingFlag;

      /*CUSTOMGROUPNODE*/
      ELSIF olValueList (nElements).sNodeType like 'CUSTOMGROUPNODE' THEN

        OPEN cGetDataGroup (olValueList (nElements).sNodeId);
        FETCH cGetDataGroup INTO sDatagroup;
        CLOSE cGetDataGroup;

        sStatement := sStatement || case when TRIM(sStatement) is not null then 'INTERSECT ' end || 'SELECT repid FROM stb$report WHERE datagroup = ' || sDatagroup || ' ';

--      /*CUSTOMNODE*/
--      ELSIF olValueList (nElements).sNodeType like 'CUSTOM%NODE' THEN

--        IF TRIM (sStatement) IS NULL AND olValueList.COUNT () = nElements THEN
--          sStatement := 'SELECT repid FROM stb$report ';
--        END IF;

      END IF;
    END LOOP;

    isr$trace.stat('end', 'end', sCurrentName);
    RETURN oErrorObj;
  EXCEPTION
    WHEN OTHERS THEN
      oErrorObj.handleError ( Stb$typedef.cnSeverityCritical, utd$msglib.getmsg ('exUnexpectedError', stb$security.getCurrentlanguage,  csP1 => sqlerrm(sqlcode)),
        sCurrentName, dbms_utility.format_error_stack ||chr(10)|| dbms_utility.format_error_backtrace );
      RETURN oErrorObj;
  END getSelectStatement;

  --**************************************************************************************************************************
  FUNCTION deleteAllSubnodes (nNodeID IN NUMBER)
    RETURN STB$OERROR$RECORD IS
    oErrorObj                     STB$OERROR$RECORD := STB$OERROR$RECORD();
    sCurrentName   constant varchar2(4000) := $$PLSQL_UNIT||'.deleteAllSubnodes('||nNodeId||')';

    CURSOR cGetSubEntries (cnNodeId IN isr$custom$grouping.nodeid%TYPE) IS
      SELECT nodeid
      FROM   isr$custom$grouping
      WHERE  parentnode = cnNodeId;


    CURSOR cGetNodeName (cnNodeId IN isr$custom$grouping.nodeid%TYPE) IS
      SELECT nodename
        FROM isr$custom$grouping
       WHERE nodeid = cnNodeId;

    sNodeName ISR$CUSTOM$GROUPING.NODENAME%TYPE;
  BEGIN
    isr$trace.stat('begin', 'begin', sCurrentName);

    OPEN cGetNodeName(nNodeID);
    FETCH cGetNodeName INTO sNodeName;
    CLOSE cGetNodeName;

    FOR rGetSubEntries IN cGetSubEntries (nNodeID) LOOP
      oErrorObj := deleteAllSubnodes (rGetSubEntries.nodeId);
    END LOOP;

    if nNodeId > 0 then    --ISRC-1238
      DELETE FROM isr$custom$grouping WHERE nodeid = nNodeId;
    end if;

    sNodeExists := 'F';
    OPEN cCheckNodeExists(sNodeName);
    FETCH cCheckNodeExists INTO sNodeExists;
    IF cCheckNodeExists%NOTFOUND THEN
      sNodeExists := 'F';
    END IF;
    CLOSE cCheckNodeExists;

    IF sNodeExists = 'F' AND sNodeName != 'GRP$CONDITION' THEN
      -- ISRC-1044
      DELETE FROM ISR$REPORT$GROUPING$TABLE WHERE groupingname = sNodeName;
    END IF;

    isr$trace.stat('end', 'end', sCurrentName);
    RETURN oErrorObj;
  EXCEPTION
    WHEN OTHERS THEN
      oErrorObj.handleError ( Stb$typedef.cnSeverityCritical, utd$msglib.getmsg ('exUnexpectedError', stb$security.getCurrentlanguage,  csP1 => sqlerrm(sqlcode)),
        sCurrentName, dbms_utility.format_error_stack ||chr(10)|| dbms_utility.format_error_backtrace );
      RETURN oErrorObj;
  END deleteAllSubnodes;

  --**************************************************************************************************************************
  FUNCTION deleteAllSubnodes (cnNodeType IN VARCHAR2)
    RETURN STB$OERROR$RECORD IS
    oErrorObj                     STB$OERROR$RECORD := STB$OERROR$RECORD();
    sCurrentName   constant varchar2(4000) := $$PLSQL_UNIT||'.deleteAllSubnodes('||' '||')';

    CURSOR cGetEntries IS
      SELECT nodeid
        FROM isr$custom$grouping
       WHERE nodename = cnNodeType;
  BEGIN
    isr$trace.stat('begin', 'begin', sCurrentName);

    FOR rGetEntries IN cGetEntries LOOP
      oErrorObj := deleteAllSubnodes (rGetEntries.nodeId);
    END LOOP;

    isr$trace.stat('end', 'end', sCurrentName);
    RETURN oErrorObj;
  EXCEPTION
    WHEN OTHERS THEN
      oErrorObj.handleError ( Stb$typedef.cnSeverityCritical, utd$msglib.getmsg ('exUnexpectedError', stb$security.getCurrentlanguage,  csP1 => sqlerrm(sqlcode)),
        sCurrentName, dbms_utility.format_error_stack ||chr(10)|| dbms_utility.format_error_backtrace );
      RETURN oErrorObj;
  END deleteAllSubnodes;

  --**************************************************************************************************************************
  function setMenuAccess( sParameter   in  varchar2,
                          sMenuAllowed out varchar2 )
    return STB$OERROR$RECORD
  is
    oErrorObj                     STB$OERROR$RECORD := STB$OERROR$RECORD();
    sCurrentName                  constant varchar2(100) := $$PLSQL_UNIT||'.setMenuAccess('||sParameter||')';
    sAction                       VARCHAR2(55);
    sNode                         VARCHAR2(55);
    sNewParameter                 VARCHAR2(255);
    sMsg                          varchar2(4000);
  BEGIN
    isr$trace.stat('begin', 'sParameter: '||sParameter, sCurrentName);

    sMenuAllowed := 'T';

    case
      when Stb$object.getCurrentNodeType = 'VALUENODE' AND sParameter like 'CAN_%_MY_NODES' THEN
        sAction := REPLACE(REPLACE(sParameter, 'CAN_'),'_MY_NODES');
        isr$trace.debug ('sAction', sAction, sCurrentName);
        sNode := REPLACE(REPLACE(STB$OBJECT.getCurrentNodeType(3-STB$OBJECT.getCurrentValueList().count()), 'CUSTOM'),'NODE');
        sNode := case when sNode = 'GROUP' then 'DATA' end || sNode;
        isr$trace.debug ('sNode', sNode, sCurrentName);
        sNewParameter := 'CAN_'||sAction||'_'||sNode||'_NODES';
        isr$trace.debug ('sNewParameter', sNewParameter, sCurrentName);
        /* nb - 3 Jun 16 - allow use of result_cache */
        -- oErrorObj := Stb$security.checkGroupRight (sNewParameter, sMenuAllowed);
        sMenuAllowed := Stb$security.checkGroupRight (sNewParameter);
/*****
      when sParameter = 'CAN_CHANGE_OWN_PASSWORD' then
        if STB$SECURITY.isLdapUser(STB$SECURITY.getCurrentUser) = 'T' then
          sMenuAllowed := 'F';
        else
          sMenuAllowed := 'T';
        end if;
*****/
      else
        -- do nothing
        null;
    end case;

    isr$trace.stat('end', 'sMenuAllowed: '||sMenuAllowed, sCurrentName);
    return oErrorObj;
  exception
    WHEN OTHERS THEN
      oErrorObj.handleError ( Stb$typedef.cnSeverityCritical, utd$msglib.getmsg ('exUnexpectedError', stb$security.getCurrentlanguage,  csP1 => sqlerrm(sqlcode)),
        sCurrentName, dbms_utility.format_error_stack ||chr(10)|| dbms_utility.format_error_backtrace );
      RETURN oErrorObj;
  END setMenuAccess;

  --*****************************************************************************************************************************
  FUNCTION SetCurrentNode (oSelectedNode IN STB$TREENODE$RECORD)
    RETURN STB$OERROR$RECORD IS
    sCurrentName   constant varchar2(4000) := $$PLSQL_UNIT||'.SetCurrentNode('||' '||')';
    oErrorObj                     STB$OERROR$RECORD := STB$OERROR$RECORD();
  BEGIN
    isr$trace.stat('begin', 'begin', sCurrentName);
    Stb$object.destroyOldValues;
    Stb$object.setCurrentNode (oSelectedNode);
    isr$trace.stat('end', 'end', sCurrentName);
    RETURN oErrorObj;
  EXCEPTION
    WHEN OTHERS THEN
      oErrorObj.handleError ( Stb$typedef.cnSeverityCritical, utd$msglib.getmsg ('exUnexpectedError', stb$security.getCurrentlanguage,  csP1 => sqlerrm(sqlcode)),
        sCurrentName, dbms_utility.format_error_stack ||chr(10)|| dbms_utility.format_error_backtrace );
      RETURN oErrorObj;
  END SetCurrentNode;

-- ***************************************************************************************************
function getNodeChilds( oSelectedNode in  STB$TREENODE$RECORD,
                        olChildNodes  out STB$TREENODELIST)
  return STB$OERROR$RECORD
is
  TYPE tCurrentValues IS REF CURSOR;

  sCurrentName                  constant varchar2(100) := $$PLSQL_UNIT||'.getNodeChilds('||oSelectedNode.sNodeType||')';
  oErrorObj                     STB$OERROR$RECORD := STB$OERROR$RECORD();
  bExists                       VARCHAR2 (1) := 'T';
  sCanViewCustomMyNode          VARCHAR2 (1) := 'F';
  sCanViewCustomGroupNode       VARCHAR2 (1) := 'F';
  sCanViewCustomGlobalNode      VARCHAR2 (1) := 'F';
  nParentNode                   NUMBER;
  bCanHaveFormatMask            VARCHAR2(1);
  sParameterValue               VARCHAR2(500);
  sHasRight                     STB$ADMINRIGHTS.PARAMETERVALUE%TYPE;
  sTheFormatMask                ISR$CUSTOM$GROUPING.FORMATMASK%TYPE;
  sGroupingName                 ISR$REPORT$GROUPING.GROUPINGNAME%TYPE;
  sOrderFlag                    ISR$PARAMETER$VALUES.INFO%TYPE;
  sFlag                         ISR$GROUPING.FLAG%TYPE;
  nLevel                        NUMBER;
  sSQL                          VARCHAR2 (4000);
  sSelect                       VARCHAR2 (4000);
  sWhere                        VARCHAR2 (4000);
  sCols                         VARCHAR2 (4000);
  sOrderBy                      VARCHAR2 (4000);
  sStatement                    VARCHAR2 (4000);
  nCnt                          NUMBER := 0;
  cGetCurrentValues             tCurrentValues;
  cGetCurrentName               isr$report$grouping.groupingname%TYPE;
  cGetCurrentValue              isr$report$grouping.groupingvalue%TYPE;

  CURSOR cGetMainCustomNodes IS
    SELECT DISTINCT
           nodeid
         , NVL (
              customtext
            , Utd$msglib.GetMsg (nodename || '$DESC'
                               , Stb$security.GetCurrentLanguage)
           )
              AS nodename
         , nodetype
         , icon
         , orderno
      FROM isr$custom$grouping, stb$userlink
     WHERE UPPER (nodetype) LIKE 'CUSTOM%'
       AND userno = STB$SECURITY.GetCurrentUser
       AND ( (REFERENCE = STB$SECURITY.GetCurrentUser
          AND REFERENCETYPE = 'USER')
         OR (REFERENCE = stb$userlink.usergroupno
         AND REFERENCETYPE = 'DATAGROUP'
         AND stb$userlink.userno = STB$SECURITY.GetCurrentUser)
         OR (REFERENCE IS NULL
         AND REFERENCETYPE IS NULL))
    ORDER BY orderno;

  CURSOR cCanHaveFormatMask (cnNodeId IN isr$custom$grouping.nodeid%TYPE) IS
    SELECT canhaveformatmask
         , nodename
         , NVL (formatmask, sSystemDateFormat) formatmask
      FROM isr$grouping, isr$custom$grouping
     WHERE nodeid = cnNodeId
       AND groupingname(+) = nodename;

  CURSOR cGetOrderFlag (csFormatMask IN varchar2) IS
    SELECT info
      FROM isr$parameter$values
     WHERE entity = 'FORMATMASK'
       AND VALUE = csFormatMask;

  CURSOR cGetTree (sNodeType IN ISR$CUSTOM$GROUPING.NODETYPE%TYPE, sNodeId IN ISR$CUSTOM$GROUPING.NODEID%TYPE) IS
    SELECT LEVEL - 1 currentLevel
         , Utd$msglib.GetMsg (p1.customText, Stb$security.GetCurrentLanguage)
              sDisplay
         , p1.PACKAGE sPackageType
         , p1.nodetype sNodetype
         , p1.nodeid sNodeid
         , p1.icon sIcon
         , NVL (p1.nodeName, p1.nodetype) sParametername
         , NVL (p1.condition, p1.nodeid) sParametervalue
         , p2.nodetype parentnodetype
      FROM isr$custom$grouping p1, isr$custom$grouping p2
     WHERE LEVEL != 1
       AND p1.parentnode = p2.nodeid(+)
       AND (p1.secureModeAllowed = ISR$LICENCEFILE.getMode
         OR ISR$LICENCEFILE.getMode = 'F')
    START WITH p1.nodeid = sNodeId
           AND p1.nodetype = sNodeType
    CONNECT BY PRIOR p1.nodeid = p1.parentnode
    ORDER SIBLINGS BY p1.orderno;

  CURSOR cGetSubTree (sNodeId IN ISR$CUSTOM$GROUPING.NODEID%TYPE) IS
    SELECT p1.nodetype sNodetype
         , p1.nodeid sNodeid
         , p1.icon sIcon
      FROM isr$custom$grouping p1
     WHERE p1.parentnode = sNodeId
    ORDER BY p1.orderno;

  CURSOR cGetGrouping (csNodeType IN isr$custom$grouping.nodetype%TYPE) IS
    SELECT groupingname
         , formatmask
         , info
         , flag
      FROM isr$grouping, isr$custom$grouping, isr$parameter$values
     WHERE nodename = csNodeType
       AND groupingname = nodetype
       AND (VALUE = NVL (formatmask, sSystemDateFormat)
         OR (formatmask IS NULL
         AND info = 'S'))
       AND entity = 'FORMATMASK';

BEGIN
  isr$trace.stat('begin', 'begin', sCurrentName);
  isr$trace.debug('parameter','oSelectedNode',sCurrentName,oSelectedNode);

  -- check if this procedure should handle the request
  olChildNodes := STB$TREENODELIST ();

  IF oSelectedNode.sNodeType LIKE 'CUSTOM%' THEN
    nParentNode := STB$OBJECT.getCurrentNodeId;
  ELSIF oSelectedNode.sNodeType = 'VALUENODE' THEN
    nParentNode := STB$OBJECT.getCurrentNodeId(-1);
  END IF;

  isr$trace.debug('variable','nParentNode: '||nParentNode, sCurrentName);

  CASE
    --=========================CUSTOMMYNODE=============================
    WHEN oSelectedNode.sNodeType like 'CUSTOM%NODE' OR oSelectedNode.sNodeType = 'VALUENODE' THEN
      bExists := 'F';
      FOR rGetTree IN cGetSubTree(STB$OBJECT.getCurrentNodeId) LOOP
        bExists := 'T';
      END LOOP;
    --=========================REPORTTYPE=================================
    WHEN oSelectedNode.sNodeType = 'MAINREPORT' OR oSelectedNode.sNodeType = 'MAINDEACTIVATEDREPORT' THEN
      bExists := 'F';
    --=========================MAINCUSTOM and other nodes====
    ELSE
      bExists := 'T';
  END CASE;

  isr$trace.debug ('bExists',  bExists, sCurrentName);

  ---------------------------------- three start nodes----------------------------------------
  CASE
    --=========================MAINCUSTOM=========================
    WHEN oSelectedNode.sNodeType = 'MAINCUSTOM' THEN
      isr$trace.debug('code position','case MAINCUSTOM',sCurrentName);

      sCanViewCustomMyNode := Stb$security.checkGroupRight ('CAN_VIEW_CUSTOMMYNODE');
      isr$trace.debug ('sCanViewCustomMyNode', sCanViewCustomMyNode, sCurrentName);
      sCanViewCustomGroupNode := Stb$security.checkGroupRight ('CAN_VIEW_CUSTOMGROUPNODE');
      isr$trace.debug ('sCanViewCustomGroupNode', sCanViewCustomGroupNode, sCurrentName);
      sCanViewCustomGlobalNode := Stb$security.checkGroupRight ('CAN_VIEW_CUSTOMGLOBALNODE');
      isr$trace.debug ('sCanViewCustomGlobalNode', sCanViewCustomGlobalNode, sCurrentName);

      FOR rGetMainCustomNodes IN cGetMainCustomNodes LOOP
        isr$trace.debug('code position','MAINCUSTOM - FOR rGetMainCustomNodes',sCurrentName);
        IF     NOT (sCanViewCustomMyNode = 'F' AND rGetMainCustomNodes.nodeType = 'CUSTOMMYNODE')
           AND NOT (sCanViewCustomGroupNode = 'F' AND rGetMainCustomNodes.nodeType = 'CUSTOMGROUPNODE')
           AND NOT (sCanViewCustomGlobalNode = 'F' AND rGetMainCustomNodes.nodeType = 'CUSTOMGLOBALNODE') THEN
          isr$trace.debug('code position','MAINCUSTOM - FOR LOOP - IF .. extend',sCurrentName);
          olChildNodes.EXTEND;
          olChildNodes (olChildNodes.COUNT()) := STB$TREENODE$RECORD(Utd$msglib.GetMsg (rGetMainCustomNodes.nodeName, Stb$security.GetCurrentLanguage),
                                                                     'ISR$CUSTOMER', rGetMainCustomNodes.nodeType, rGetMainCustomNodes.nodeId,
                                                                     rGetMainCustomNodes.icon, tloValueList => oSelectedNode.tloValueList);
          STB$OBJECT.setDirectory(olChildNodes (olChildNodes.COUNT()));
        END IF;
      END LOOP;
    --=========================VALUENODE=========================
    WHEN ( oSelectedNode.sNodeType like 'CUSTOM%NODE' OR oSelectedNode.sNodeType IN ('VALUENODE') )
    AND  bExists = 'T' THEN
      isr$trace.debug('code position','CASE VALUENODE',sCurrentName);
      FOR rGetTree IN cGetSubTree (STB$OBJECT.getCurrentNodeId) LOOP
        isr$trace.debug('code position','VALUENODE - FOR rGetTree',sCurrentName);
        OPEN cCanHaveFormatMask (rGetTree.sNodeId);
        FETCH cCanHaveFormatMask
        INTO  bCanHaveFormatMask, sGroupingName, sTheFormatMask;
        CLOSE cCanHaveFormatMask;
        isr$trace.debug('sGroupingName', sGroupingName, sCurrentName);
        isr$trace.debug('sTheFormatMask', sTheFormatMask, sCurrentName);
        oErrorObj := ISR$CUSTOMER.getSelectStatement (oSelectedNode.tloValueList, sSelect);
        isr$trace.debug('sSelect', 'sSelect', sCurrentName, sSelect);
        sWhere := getConditionAsSQL (rGetTree.sNodeId);
        isr$trace.debug('sWhere', sWhere, sCurrentName);

        IF bCanHaveFormatMask = 'F' THEN
          sCols := 'SELECT distinct groupingname, groupingvalue ';
          sOrderBy := 'ORDER BY case when length(groupingvalue) < 4 then lpad(groupingvalue, 4, ''0'') else groupingvalue end ';
        ELSE
          sCols := 'SELECT distinct groupingname, to_char(groupingdate, ''' || sTheFormatMask || ''') groupingvalue ';

          -- create order by
          OPEN cGetOrderFlag (sTheFormatMask);
          FETCH cGetOrderFlag INTO sOrderFlag;
          CLOSE cGetOrderFlag;
          isr$trace.debug('sOrderFlag', sOrderFlag, sCurrentName);

          IF sOrderFlag IN ('S') THEN
            sOrderBy := 'ORDER BY groupingvalue ';
          ELSIF sOrderFlag IN ('N') THEN
            sOrderBy := 'ORDER BY to_number(groupingvalue) ';
          ELSIF sOrderFlag IN ('D') AND UPPER (sTheFormatMask) = 'MON' THEN
            sOrderBy := 'ORDER BY to_date(''01-''||UPPER(groupingvalue)||''-2000'', ''DD-MON-YYYY'') ';
          END IF;

        END IF;
        isr$trace.debug('sCols', sCols, sCurrentName);
        isr$trace.debug('sOrderBy', sOrderBy, sCurrentName);

        sStatement := sCols;

        -- ISRC-1044
        sStatement := sStatement || 'FROM ( select rgt.*, ul.userno
                                              from isr$report$grouping$table rgt, stb$report r, stb$userlink ul
                                             where rgt.repid = r.repid
                                               and rgt.groupingname = ''' || REPLACE(sGroupingName, '''', '''''') || '''
                                               and ul.usergroupno = r.datagroup
                                               and r.condition != 0
                                               and ul.userno = '|| STB$SECURITY.getCurrentUser ||'
                                               and ul.userno in (select u1.userno
                                                                   from stb$user u1
                                                                  where activeuser = ''T''
                                                                 intersect
                                                                 select ul1.userno
                                                                   from stb$userlink ul1, stb$group$reptype$rights grr1
                                                                  where grr1.parametername = ''SHOW_IN_EXPLORER''
                                                                    and grr1.parametervalue = ''T''
                                                                    and grr1.reporttypeid = r.reporttypeid
                                                                    and grr1.usergroupno = ul1.usergroupno)) g '
                      ||CASE WHEN TRIM (sSelect) IS NOT NULL THEN ', ('||sSelect||') r ' END;

        sStatement := sStatement || 'WHERE 1=1 ';

        IF TRIM (sWhere) IS NOT NULL THEN
          sStatement := sStatement || 'AND ' || sWhere || ' ';
        END IF;

        IF TRIM (sSelect) IS NOT NULL THEN
          sStatement := sStatement || 'AND g.repid = r.repid ';
        END IF;

        sStatement := sStatement || sOrderby;

        isr$trace.debug('dynsql','sStatement', sCurrentName, sStatement);

        nCnt := 0;
        BEGIN
          OPEN cGetCurrentValues FOR sStatement;
          LOOP
            isr$trace.debug('code position','VALUENODE - FOR rGetTree - FOR cGetCurrentValues ',sCurrentName);
            FETCH cGetCurrentValues
            INTO cGetCurrentName, cGetCurrentValue;
            EXIT WHEN cGetCurrentValues%NOTFOUND;   -- vs: das ist ein NO GO
            nCnt := nCnt + 1;
            olChildNodes.EXTEND;
            olChildNodes (olChildNodes.COUNT()) := STB$TREENODE$RECORD(SUBSTR(cGetCurrentValue,0,250),
                                                                       oSelectedNode.sPackageType, 'VALUENODE', rGetTree.sNodeId,
                                                                       rGetTree.sIcon, tloValueList => oSelectedNode.tloValueList);
            STB$OBJECT.setDirectory(olChildNodes (olChildNodes.COUNT()), cGetCurrentName, cGetCurrentValue);
          END LOOP;
          CLOSE cGetCurrentValues;
        EXCEPTION
          WHEN OTHERS THEN
            isr$trace.error('ERROR','sStatement failed', sCurrentName, sStatement||chr(10)||chr(10)||SQLERRM);
        END;


        /*** Deactivated: ISRC-506
        IF nCnt = 0 THEN
          isr$trace.debug('code position','VALUENODE - FOR rGetTree - IF nCnt=0',sCurrentName);
          olChildNodes.EXTEND;
          olChildNodes (olChildNodes.COUNT()) := STB$TREENODE$RECORD(Utd$msglib.GetMsg ('NO_DATA_FOUND_FOR_', Stb$security.GetCurrentLanguage)||Utd$msglib.GetMsg (sGroupingName||'$DESC', Stb$security.GetCurrentLanguage),
                                                                     oSelectedNode.sPackageType, 'VALUENODE', rGetTree.sNodeId,
                                                                     rGetTree.sIcon, tloValueList => oSelectedNode.tloValueList);
          STB$OBJECT.setDirectory(olChildNodes (olChildNodes.COUNT()), Utd$msglib.GetMsg ('NO_DATA_FOUND_FOR_', Stb$security.GetCurrentLanguage)||Utd$msglib.GetMsg (sGroupingName||'$DESC', Stb$security.GetCurrentLanguage), -1);
        END IF;
        ***/

      END LOOP;
    --=========================ALL NODES=========================
    WHEN bExists = 'T' THEN
      isr$trace.debug('code position','ALL NODES',sCurrentName);
      FOR rGetTree IN cGetTree (STB$OBJECT.getCurrentNodeType, STB$OBJECT.getCurrentNodeId) LOOP
        isr$trace.debug('code position','ALL NODES - FOR rGetTree',sCurrentName);
        IF (nLevel IS NULL OR nLevel = rGetTree.currentLevel) AND STB$OBJECT.getCurrentNodeType = rGetTree.parentnodetype THEN
           -- Check the rights of the user
          isr$trace.debug('code position','ALL NODES - FOR rGetTree - IF',sCurrentName);
          oErrorObj := STB$UTIL.getSystemParameter ('CAN_VIEW_' || rGetTree.sNodeType, sParameterValue);
          isr$trace.debug ('sParameterValue',  sParameterValue, sCurrentName);
          IF sParameterValue = 'T' THEN
            isr$trace.debug('code position','ALL NODES - FOR rGetTree - IF - IF',sCurrentName);
            /* nb - 3 Jun 16 - allow use of result_cache */
            -- oErrorObj := Stb$security.checkGroupRight ('CAN_VIEW_' || rGetTree.sNodeType, sHasRight);
            sHasRight := Stb$security.checkGroupRight ('CAN_VIEW_' || rGetTree.sNodeType );
          ELSIF sParameterValue = 'F' THEN
            isr$trace.debug('code position','ALL NODES - FOR rGetTree - IF - ELSIF',sCurrentName);
            sHasRight := 'F';
          ELSE
            isr$trace.debug('code position','ALL NODES - FOR rGetTree - IF - ELSE',sCurrentName);
            sHasRight := 'T';
          END IF;

          IF sHasRight = 'T' THEN
            isr$trace.debug('code position','ALL NODES - FOR rGetTree - IF #2#',sCurrentName);
            olChildNodes.EXTEND;
            olChildNodes (olChildNodes.COUNT ()) := STB$TREENODE$RECORD(rGetTree.sDisplay,
                                                                        rGetTree.sPackageType, rGetTree.sNodeType, rGetTree.sNodeId,
                                                                        rGetTree.sIcon, tloValueList => oSelectedNode.tloValueList);
            STB$OBJECT.setDirectory(olChildNodes (olChildNodes.COUNT()), rGetTree.sParametername, rGetTree.sParametervalue);
            nLevel := rGetTree.currentLevel;
          END IF;
        END IF;
        isr$trace.debug ('CAN_VIEW_'||rGetTree.sNodeType, 'sHasRight: '||sHasRight, sCurrentName);
      END LOOP;
    --=========================REPORTTYPE/TITLE=========================
    WHEN bExists = 'F' THEN
      isr$trace.debug('code position','REPORTTYPE/TITLE',sCurrentName);
      -- go to reporttype package and fetch titles
      IF oSelectedNode.snodetype = 'VALUENODE'
      OR oSelectedNode.snodetype like 'CUSTOM%NODE' THEN
        RETURN ISR$REPORTTYPE$PACKAGE.getNodeChilds(oSelectedNode, olchildnodes);
      END IF;

      oErrorObj := getSelectStatement (oSelectedNode.tloValueList, sStatement);
      -- Deactive report's package killed
      --'FROM (SELECT DISTINCT Utd$msglib.GetMsg(stb$reporttype.reporttypename,Stb$security.GetCurrentLanguage) SDISPLAY, DECODE(''' || oSelectedNode.sNodeType || ''', ''MAINDEACTIVATEDREPORT'', ''STB$DEACTIVEREPORT'', ''ISR$REPORTTYPE$PACKAGE'') SPACKAGETYPE, DECODE(''' || oSelectedNode.sNodeType || ''', ''MAINDEACTIVATEDREPORT'', ''DEACTIVATEREPTYPE'', ''REPORTTYPE'') SNODETYPE, stb$reporttype.reporttypeid SNODEID, ''REPORTTYPE''||stb$reporttype.reporttypeid SICON ' ||
      sSQL :=
           'BEGIN '
        || 'select STB$TREENODE$RECORD( SDISPLAY, SPACKAGETYPE, SNODETYPE, SNODEID, SICON ) '
        || 'BULK COLLECT into :1 '
        || 'FROM (SELECT DISTINCT UTD$MSGLIB.getMsg (stb$reporttype.reporttypename, stb$security.GetCurrentLanguage) SDISPLAY, ''ISR$REPORTTYPE$PACKAGE'' SPACKAGETYPE, ''REPORTTYPE'' SNODETYPE, stb$reporttype.reporttypeid SNODEID, ''REPORTTYPE''||stb$reporttype.reporttypeid SICON '
        || '      FROM   stb$reporttype, stb$report '
        ||  CASE WHEN TRIM (sstatement) IS NOT NULL THEN ', ('||sstatement||') r1 ' END
        || '      WHERE  stb$report.reporttypeid(+) = stb$reporttype.reporttypeid ';

      IF TRIM (sStatement) IS NOT NULL THEN
        sSQL := sSQL || ' AND stb$report.repid = r1.repid ';
      END IF;

      sSQL := sSQL
        || '      AND      ((stb$reporttype.activetype = ' || Stb$typedef.cnStatusActivated || ' '
        || '      AND        stb$reporttype.reporttypeid IN ( SELECT DISTINCT ur.reporttypeid '
        || '                              FROM   stb$userlink u, stb$group$reptype$rights ur  '
        || '                              WHERE  u.userno = ' || Stb$security.getCurrentUser || ' '
        || '                              AND    ur.parametername = ''SHOW_IN_EXPLORER''  '
        || '                              AND    ur.parametervalue = ''T''  '
        || '                              AND    ur.usergroupno = u.usergroupno )) '
        || '      OR      (stb$reporttype.reporttypeid = 999 '
        || '      AND      ''T'' = '''||STB$UTIL.getSystemParameter('SHOW_UNIT_TEST')||''' '
        || '      AND      '''|| oSelectedNode.sNodeType || ''' = ''MAINREPORT'')) '
        || '      ORDER BY ''REPORTTYPE''||stb$reporttype.reporttypeid); '
        || ' END;';
      isr$trace.debug('sSQL','sSQL',sCurrentName,sSQL);
      EXECUTE IMMEDIATE sSQL
      USING             IN OUT olChildNodes;

      IF olChildNodes.COUNT != 0 THEN
        FOR i IN 1 .. olChildNodes.COUNT () LOOP
          olChildNodes (i).tloValueList := oSelectedNode.tloValueList;
          STB$OBJECT.setDirectory(olChildNodes (i));
        END LOOP;
      END IF;
    --=========================NOT DEFINED=========================
    ELSE
      isr$trace.debug('code position','CASE ELSE == NOT DEFINED',sCurrentName);
  END CASE;

  isr$trace.debug('parameter','out olChildNodes',sCurrentName,olChildNodes);
  isr$trace.stat('end', 'olChildNodes.count: '||olChildNodes.COUNT, sCurrentName);
  RETURN (oErrorObj);
EXCEPTION
  WHEN OTHERS THEN
    ROLLBACK;
    oErrorObj.handleError ( Stb$typedef.cnSeverityCritical, utd$msglib.getmsg ('exUnexpectedError', stb$security.getCurrentlanguage,  csP1 => sqlerrm(sqlcode)),
        sCurrentName, dbms_utility.format_error_stack ||chr(10)|| dbms_utility.format_error_backtrace );
    RETURN (oErrorObj);
END GetNodeChilds;

  -- ***************************************************************************************************
  FUNCTION getNodeList (oSelectedNode IN STB$TREENODE$RECORD, olNodeList OUT STB$TREENODELIST)
    RETURN STB$OERROR$RECORD IS
    oErrorObj                     STB$OERROR$RECORD := STB$OERROR$RECORD();
    olParameter                   STB$PROPERTY$LIST;
    sDescription                  STB$REPORTTYPE.DESCRIPTION%TYPE;
    sCurrentName                  constant varchar2(100) := $$PLSQL_UNIT||'.getNodeList('||oSelectedNode.sNodeType||')';

    CURSOR cGetNodeDesc (sGroupingName IN VARCHAR2) IS
      SELECT Utd$msglib.GetMsg (DESCRIPTION, Stb$security.GetCurrentLanguage)
        FROM isr$grouping
       WHERE groupingname = sGroupingName;
  BEGIN
    isr$trace.stat('begin', 'begin', sCurrentName);
    olNodeList := STB$TREENODELIST();

    oErrorObj := GetNodeChilds (oSelectedNode, olNodeList);

    isr$trace.stat('end', 'olNodeList.COUNT: '||olNodeList.COUNT, sCurrentName);
    RETURN (oErrorObj);
  EXCEPTION
    WHEN OTHERS THEN
      ROLLBACK;
      oErrorObj.handleError ( Stb$typedef.cnSeverityCritical, utd$msglib.getmsg ('exUnexpectedError', stb$security.getCurrentlanguage,  csP1 => sqlerrm(sqlcode)),
        sCurrentName, dbms_utility.format_error_stack ||chr(10)|| dbms_utility.format_error_backtrace );
      RETURN (oErrorObj);
  END GetNodeList;

  --*********************************************************************************************************************************
  function getContextMenu( nMenu           in  number,
                           olMenuEntryList OUT STB$MENUENTRY$LIST )
    return STB$OERROR$RECORD
  is
    sCurrentName              constant varchar2(100) := $$PLSQL_UNIT||'.getContextMenu('||nMenu||')';
    oErrorObj                     STB$OERROR$RECORD := STB$OERROR$RECORD();
  begin
    isr$trace.stat('begin', 'nMenu: '||nMenu, sCurrentName);
    olMenuEntryList := STB$MENUENTRY$LIST();
    oErrorObj := Stb$util.getContextMenu (Stb$object.getCurrentNodeType, nMenu, olMenuEntryList);
    isr$trace.stat('end', 'olMenuEntryList.count: '||olMenuEntryList.count, sCurrentName);
    return (oErrorObj);
  exception
    when others then
      rollback;
      oErrorObj.handleError( STB$TYPEDEF.cnSeverityCritical, utd$msglib.getmsg ('exUnexpectedError', stb$security.getCurrentLanguage, sqlerrm(sqlcode)), sCurrentName,
                             substr(dbms_utility.format_error_stack || CHR(10) || dbms_utility.format_error_backtrace, 1,4000));
      return oErrorObj;
  end getContextMenu;

  --**********************************************************************************************************************************
  FUNCTION loadOnlyRight(oSelectedNode in STB$TREENODE$RECORD) RETURN VARCHAR2
  IS
    sCurrentName    constant varchar2(100) := $$PLSQL_UNIT||'.loadOnlyRight('||oSelectedNode.sNodeType||')';
    sLoadOnlyRight  VARCHAR2(1) := 'F';
    tloValueList    ISR$VALUE$LIST;
    oValueRec       ISR$VALUE$RECORD;

    CURSOR cLoadOnlyRight(csNodeId IN VARCHAR2, csNodeType IN VARCHAR2) IS
      SELECT c.load_only_right
        FROM isr$custom$grouping c
       WHERE nodetype = csNodeType/*
         AND nodeid = csNodeId*/;
  BEGIN
    isr$trace.stat('begin', 'oSelectedNode.sNodeType: '||oSelectedNode.sNodeType , sCurrentName, xmltype(oSelectedNode).getClobVal());
    tloValueList := oSelectedNode.tloValueList;
    oValueRec := tloValueList(tloValueList.COUNT());
    OPEN cLoadOnlyRight(oValueRec.sNodeId, oValueRec.sNodeType);
    FETCH cLoadOnlyRight INTO sLoadOnlyRight;
    CLOSE cLoadOnlyRight;
    isr$trace.debug('sLoadOnlyRight', 'sLoadOnlyRight', sCurrentName);
    isr$trace.stat('end', 'end', sCurrentName);
    RETURN sLoadOnlyRight;
  END;

  --**********************************************************************************************************************************
  FUNCTION callFunction (oParameter IN STB$MENUENTRY$RECORD, olNodeList OUT STB$TREENODELIST)
    RETURN STB$OERROR$RECORD IS
    oErrorObj                     STB$OERROR$RECORD := STB$OERROR$RECORD();
    sCurrentName  constant varchar2(100) := $$PLSQL_UNIT||'.callFunction ('||oParameter.sToken||')';
    exNotExists                   EXCEPTION;
    olParameter                   STB$PROPERTY$LIST;
    nSaveNodeId                   ISR$CUSTOM$GROUPING.NODEID%TYPE;
    sModifiedFlag                 VARCHAR2 (1);
  BEGIN
    isr$trace.stat ('begin',  'parameter: ' || oParameter.sToken, sCurrentName);

    STB$OBJECT.setCurrentToken(oparameter.stoken);

    CASE
      ------------ create a subnode  ---------------------
      ----------------------------------------------------
    WHEN UPPER (oParameter.sToken) like 'CAN_CREATE_%_NODES'
      OR UPPER (oParameter.sToken) like 'CAN_MODIFY_%_NODES' THEN
        olNodeList := STB$TREENODELIST ();
        olNodeList.EXTEND;
        olNodeList (1) := STB$TREENODE$RECORD(STB$UTIL.getMaskTitle(Upper (oParameter.sToken)),
                                              'ISR$CUSTOMER', 'TRANSPORT', 'MASK', '' );
        oErrorObj := STB$UTIL.getPropertyList(case when UPPER (oParameter.sToken) like '%CREATE%' then 'CAN_CREATE_MY_NODES' else 'CAN_MODIFY_MY_NODES' end, olNodeList (1).tloPropertylist);
        IF UPPER (oParameter.sToken) like '%MODIFY%' THEN
          oErrorObj := checkDependenciesOnMask(oParameter, olNodeList (1).tloPropertylist, sModifiedFlag);
        END IF;
      ------------ delete a subnode  ---------------------
      ----------------------------------------------------
    WHEN UPPER (oParameter.sToken) like 'CAN_DELETE_%_NODES' THEN
        nSaveNodeId := STB$OBJECT.getCurrentNodeId;
        isr$trace.debug ('nSaveNodeId',nSaveNodeId, sCurrentName);
        -- delete all subnodes
        oErrorObj := deleteAllSubnodes (nSaveNodeId);
        COMMIT;

        ISR$TREE$PACKAGE.reloadPrevNodesAndSelect(-1);

      ------------ search for a doc  ---------------------
      ----------------------------------------------------
    WHEN UPPER (oParameter.sToken) = 'CAN_SEARCH_DOC' THEN
        olNodeList := STB$TREENODELIST ();
        olNodeList.EXTEND;
        olNodeList (1) := STB$TREENODE$RECORD(STB$UTIL.getMaskTitle(Upper (oParameter.sToken)),
                                              'ISR$CUSTOMER', 'TRANSPORT', 'MASK', '' );
        olParameter := STB$PROPERTY$LIST ();
        olParameter.EXTEND;
        olParameter (1) := STB$PROPERTY$RECORD('OID', UTD$MSGLIB.GetMsg ('OID', STB$SECURITY.GetCurrentLanguage) || ' ' || STB$UTIL.getSystemParameter ('SYSTEM_OID') || '.',
                                               null, null, 'T', 'T', 'T', 'F', 'STRING');
        olNodeList (1).tloPropertylist := olParameter;
      ELSE
        RAISE exNotExists;
    END CASE;

    isr$trace.stat ('end', 'end', sCurrentName);
    RETURN oErrorObj;
  EXCEPTION
    WHEN exNotExists THEN
      oErrorObj.handleError ( Stb$typedef.cnSeverityCritical, utd$msglib.getmsg ('exNotExistsText', stb$security.getCurrentlanguage,  csP1 => sqlerrm(sqlcode)),
        sCurrentName, dbms_utility.format_error_stack ||chr(10)|| dbms_utility.format_error_backtrace );
      RETURN oErrorObj;
    WHEN OTHERS THEN
      ROLLBACK;
      oErrorObj.handleError ( Stb$typedef.cnSeverityCritical, utd$msglib.getmsg ('exUnexpectedError', stb$security.getCurrentlanguage,  csP1 => sqlerrm(sqlcode)),
        sCurrentName, dbms_utility.format_error_stack ||chr(10)|| dbms_utility.format_error_backtrace );
      RETURN oErrorObj;
  END callFunction;

  --**********************************************************************************************************************************
  FUNCTION getLov (sEntity IN VARCHAR2, sSuchwert IN VARCHAR2, oSelectionList OUT ISR$TLRSELECTION$LIST)
    RETURN STB$OERROR$RECORD IS
    sCurrentName   constant varchar2(4000) := $$PLSQL_UNIT||'.getLov('||sEntity||')';
    oErrorObj                     STB$OERROR$RECORD := STB$OERROR$RECORD();

    CURSOR cGetGrouping IS
      SELECT groupingname, description, groupingflag
        FROM isr$grouping$view
       WHERE selected = 'T'
      ORDER BY 3, 2;

  BEGIN
    isr$trace.stat('begin', 'begin', sCurrentName);
    oSelectionList := ISR$TLRSELECTION$LIST ();
    CASE
      WHEN UPPER (sEntity) = 'GROUPING' THEN
        FOR rGetGrouping IN cGetGrouping LOOP
          oSelectionList.EXTEND;
          oSelectionList (oSelectionList.COUNT()) := ISR$OSELECTION$RECORD(rGetGrouping.description, rGetGrouping.groupingname, rGetGrouping.groupingflag, null, oSelectionList.COUNT());
        END LOOP;
      --***************************************
      ELSE
        EXECUTE IMMEDIATE
        'DECLARE
           olocError STB$OERROR$RECORD;
         BEGIN
           olocError :='|| stb$util.getcurrentcustompackage || '.getLov(:1,:2,:3);
           :4 :=  olocError;
         END;'
        USING IN sentity, IN ssuchwert, OUT oselectionlist, OUT oErrorObj;
    END CASE;

    IF sSuchWert IS NOT NULL AND oselectionlist.First IS NOT NULL THEN
      isr$trace.debug ('sSuchWert',  sSuchWert, sCurrentName);
      FOR i IN 1..oSelectionList.Count() LOOP
        IF oSelectionList(i).sValue = sSuchWert THEN
          isr$trace.debug ('oSelectionList('||i||').sSelected', oSelectionList(i).sValue, sCurrentName);
          oSelectionList(i).sSelected := 'T';
        END IF;
      END LOOP;
    END IF;

    isr$trace.debug ('status', 'oSelectionList.COUNT(): '||oSelectionList.COUNT(), sCurrentName);
    isr$trace.stat('end', 'end', sCurrentName);
    RETURN oErrorObj;
  EXCEPTION
    WHEN OTHERS THEN
      oErrorObj.handleError ( Stb$typedef.cnSeverityMessage, utd$msglib.getmsg ('exUnexpectedError', stb$security.getCurrentlanguage,  csP1 => sqlerrm(sqlcode)),
        sCurrentName, dbms_utility.format_error_stack ||chr(10)|| dbms_utility.format_error_backtrace );
      RETURN oErrorObj;
  END getLov;

  --**********************************************************************************************************************
  FUNCTION putParameters (oParameter IN STB$MENUENTRY$RECORD, olParameter IN STB$PROPERTY$LIST)
    RETURN STB$OERROR$RECORD IS
    oErrorObj                     STB$OERROR$RECORD := STB$OERROR$RECORD();
    sCurrentName  constant varchar2(100) := $$PLSQL_UNIT||'.putParameters ('||oParameter.sToken||')';
    exNotExists                   EXCEPTION;
    nNewNodeId                    ISR$CUSTOM$GROUPING.NODEID%TYPE;
    sRepId                        VARCHAR2 (20);
    sDocId                        VARCHAR2 (20);
    oSimiliarNode                 STB$TREENODE$RECORD;
    oNewNode                      STB$TREENODE$RECORD;

    cursor cGetDocInfo(sRepId IN VARCHAR2, sDocId IN VARCHAR2) is
      SELECT NVL(d.docid,r.repid) nodeid,
             DECODE(d.doctype,'C','COMMONDOC','D','DOCUMENT','X','RAWDATA','L','LINK','REPORT') nodetype,
             d.docid,
             r.repid,
             r.srnumber,
             r.title,
             r.reporttypeid
      FROM   STB$DOCTRAIL d, STB$REPORT r
      WHERE  (to_char(d.docid) = sDocId or sDocId is null)
      AND    (to_char(r.repid) = sRepId or sRepId = '*')
      AND     to_char(r.repid) = d.nodeid(+);

    rGetDocInfo cGetDocInfo%rowtype;

  BEGIN
    isr$trace.stat('begin', 'begin', sCurrentName);

    STB$OBJECT.setCurrentToken(oparameter.stoken);

    CASE
    --------------------------------------------------------------------
    ----------------- write grouping element to table ------------------
      WHEN UPPER (oParameter.sToken) like 'CAN_CREATE_%_NODES'  THEN
        SELECT SEQ$STB$CUSTOMNODE.NEXTVAL
        INTO   nNewNodeId
        FROM   DUAL;

        sNodeExists := 'F';
        OPEN cCheckNodeExists(olParameter (1).sValue);
        FETCH cCheckNodeExists INTO sNodeExists;
        IF cCheckNodeExists%NOTFOUND THEN
          sNodeExists := 'F';
        END IF;
        CLOSE cCheckNodeExists;

        -- remember the current node and insert the values to the database
        CASE
          --=========================MYNODE=========================
        WHEN STB$OBJECT.getCurrentNodeType = 'VALUENODE' OR STB$OBJECT.getCurrentNodeType  = 'CUSTOMMYNODE' THEN
            INSERT INTO ISR$CUSTOM$GROUPING
                        (NODEID, NODETYPE, REFERENCE, REFERENCETYPE, PARENTNODE, ORDERNO, NODENAME,
                         FORMATMASK, CONDITION, ICON)
                 VALUES (nNewNodeId, 'PROMPTMYNODE', STB$SECURITY.GetCurrentUser, 'USER', STB$OBJECT.getCurrentNodeId, (SELECT NVL (MAX (orderno) + 1, 1)
                                                                                                    FROM   ISR$CUSTOM$GROUPING
                                                                                                    WHERE  parentnode = STB$OBJECT.getCurrentNodeId), olParameter (1).sValue,
                         olParameter (4).sValue, olParameter (3).sValue, olParameter (2).sValue);
          --=========================GROUPNODE=========================
        WHEN STB$OBJECT.getCurrentNodeType  = 'VALUENODE' OR STB$OBJECT.getCurrentNodeType  = 'CUSTOMGROUPNODE' THEN
            INSERT INTO ISR$CUSTOM$GROUPING
                        (NODEID, NODETYPE, REFERENCE, REFERENCETYPE, PARENTNODE, ORDERNO, NODENAME,
                         FORMATMASK, CONDITION, ICON)
                 VALUES (nNewNodeId, 'PROMPTGROUP', (SELECT REFERENCE
                                                     FROM   ISR$CUSTOM$GROUPING
                                                     WHERE  nodeid = STB$OBJECT.getCurrentNodeId), 'DATAGROUP', STB$OBJECT.getCurrentNodeId, (SELECT NVL (MAX (orderno) + 1, 1)
                                                                                                      FROM   ISR$CUSTOM$GROUPING
                                                                                                      WHERE  parentnode = STB$OBJECT.getCurrentNodeId), olParameter (1).sValue,
                         olParameter (4).sValue, olParameter (3).sValue, olParameter (2).sValue);
          --=========================GLOBALNODE=========================
        WHEN STB$OBJECT.getCurrentNodeType  = 'VALUENODE' OR STB$OBJECT.getCurrentNodeType = 'CUSTOMGLOBALNODE' THEN
            INSERT INTO ISR$CUSTOM$GROUPING
                        (NODEID, NODETYPE, PARENTNODE, ORDERNO, NODENAME,
                         FORMATMASK, CONDITION, ICON)
                 VALUES (nNewNodeId, 'PROMPTGLOBAL', STB$OBJECT.getCurrentNodeId, (SELECT NVL (MAX (orderno) + 1, 1)
                                                               FROM   ISR$CUSTOM$GROUPING
                                                               WHERE  referencetype IS NULL AND parentnode = STB$OBJECT.getCurrentNodeId), olParameter (1).sValue,
                         olParameter (4).sValue, olParameter (3).sValue, olParameter (2).sValue);
        END CASE;

        BEGIN
          isr$trace.debug ('sNodeExists',  sNodeExists||' '||olParameter (1).sValue, scurrentName);
          IF sNodeExists = 'F' AND olParameter (1).sValue != 'GRP$CONDITION'  THEN
            -- ISRC-1044
            INSERT INTO ISR$REPORT$GROUPING$TABLE (GROUPINGNAME, GROUPINGID, GROUPINGVALUE, GROUPINGDATE, REPID)
               (SELECT rg.GROUPINGNAME,rg.GROUPINGID,rg.GROUPINGVALUE,rg.GROUPINGDATE,rg.REPID
                  FROM isr$report$grouping rg, stb$report r
                 WHERE rg.repid = r.repid
                   AND rg.groupingname = olParameter (1).sValue
                   AND r.condition != 0);
          END IF;
        EXCEPTION WHEN OTHERS THEN
          isr$trace.error (SQLCODE,  SQLERRM, sCurrentName);
        END;

        COMMIT;

        ISR$TREE$PACKAGE.selectCurrent;

    --------------------------------------------------------------------
    ----------------- update grouping element in table ------------------
      WHEN UPPER (oParameter.sToken) like 'CAN_MODIFY_%_NODES' THEN
        UPDATE ISR$CUSTOM$GROUPING
           SET NODENAME = olParameter (1).sValue,
               FORMATMASK = olParameter (4).sValue,
               CONDITION = olParameter (3).sValue,
               ICON = olParameter (2).sValue
         WHERE NODEID = STB$OBJECT.getCurrentNodeId;
        nNewNodeId := STB$OBJECT.getCurrentNodeId;

        COMMIT;

        ISR$TREE$PACKAGE.reloadPrevNodesAndSelectCurren(-1);

    --------------------------------------------------------------------
    ----------------- find grouping element in tree ------------------
      WHEN UPPER (oParameter.sToken) = 'CAN_SEARCH_DOC' THEN

        IF TRIM(olParameter (1).sValue) IS NOT NULL THEN
          IF INSTR (olParameter (1).sValue, '.') > 0 THEN
            sRepId := SUBSTR (olParameter (1).sValue, 0, INSTR (olParameter (1).sValue, '.') - 1);
            sDocId := SUBSTR (olParameter (1).sValue, INSTR (olParameter (1).sValue, '.') + 1);
            IF sDocId = '*' THEN
              sDocId := NULL;
            END IF;
          ELSE
            sRepId := olParameter (1).sValue;
            sDocId := NULL;
          END IF;

          OPEN cGetDocInfo(sRepId, sDocId);
          FETCH cGetDocInfo INTO rGetDocInfo;
          CLOSE cGetDocInfo;

          oNewNode := ISR$TREE$PACKAGE.searchThisNode('REPORT', rGetDocInfo.repid, ISR$TREE$PACKAGE.getCustomNodeBranch);
          if oNewNode.sNodeId is null then
            oSimiliarNode := ISR$TREE$PACKAGE.searchSimiliarNode('REPORT', rGetDocInfo.repid, 'F', ISR$TREE$PACKAGE.getCustomNodeBranch);
            if oSimiliarNode.sNodeId is not null then
              oNewNode := ISR$TREE$PACKAGE.rebuildReportNode(rGetDocInfo.repid, oSimiliarNode);
            else
              oNewNode := ISR$TREE$PACKAGE.buildReportNode(rGetDocInfo.repid, null, ISR$TREE$PACKAGE.getCustomNodeBranch);
            end if;
          end if;
          ISR$TREE$PACKAGE.loadNode(oNewNode, STB$OBJECT.getCurrentNode);

          if rGetDocInfo.nodetype = 'REPORT' then
            ISR$TREE$PACKAGE.selectLeftNode(oNewNode);
          else
            ISR$TREE$PACKAGE.selectRightNode(ISR$TREE$PACKAGE.buildNode(rGetDocInfo.nodetype, rGetDocInfo.nodeid, oNewNode));
          end if;

        END IF;
    --------------------------------------------------------------------
    ----------------- not defined ------------------
      ELSE
        RAISE exNotExists;
    END CASE;

    isr$trace.stat('end', 'end', sCurrentName);
    RETURN oErrorObj;
  EXCEPTION
    WHEN exNotExists THEN
      oErrorObj.handleError ( Stb$typedef.cnSeverityMessage, utd$msglib.getmsg ('exNotExistsText', stb$security.getCurrentlanguage,  csP1 => sqlerrm(sqlcode)),
        sCurrentName, dbms_utility.format_error_stack ||chr(10)|| dbms_utility.format_error_backtrace );
      RETURN oErrorObj;
    WHEN NO_DATA_FOUND THEN
      oErrorObj.handleError ( Stb$typedef.cnSeverityMessage, utd$msglib.getmsg ('exNoDocumentFoundText', stb$security.getCurrentlanguage,  csP1 => sqlerrm(sqlcode)),
        sCurrentName, dbms_utility.format_error_stack ||chr(10)|| dbms_utility.format_error_backtrace );
      RETURN oErrorObj;
    WHEN OTHERS THEN
      ROLLBACK;
      IF SQLCODE = '-1' THEN
        oErrorObj.handleError ( Stb$typedef.cnSeverityMessage, utd$msglib.getmsg ('exNodeExistsText', stb$security.getCurrentlanguage,  csP1 => sqlerrm(sqlcode)),
        sCurrentName, dbms_utility.format_error_stack ||chr(10)|| dbms_utility.format_error_backtrace );
        RETURN oErrorObj;
      ELSE
        oErrorObj.handleError ( Stb$typedef.cnSeverityWarning, utd$msglib.getmsg ('exUnexpectedError', stb$security.getCurrentlanguage,  csP1 => sqlerrm(sqlcode)),
        sCurrentName, dbms_utility.format_error_stack ||chr(10)|| dbms_utility.format_error_backtrace );
        RETURN oErrorObj;
      END IF;
  END putParameters;

--**********************************************************************************************************************
FUNCTION checkDependenciesOnMask( oParameter IN STB$MENUENTRY$RECORD,
                                  olParameter IN OUT STB$PROPERTY$LIST,
                                  sModifiedFlag OUT VARCHAR2 )   RETURN STB$OERROR$RECORD
is
  sCurrentName   constant varchar2(4000) := $$PLSQL_UNIT||'.checkDependenciesOnMask('||' '||')';
  oErrorObj           STB$OERROR$RECORD := STB$OERROR$RECORD();
  olParameterOld      STB$PROPERTY$LIST := STB$PROPERTY$LIST();

  CURSOR cGetGroupingWithFormatMask(csGroupingName IN ISR$GROUPING.GROUPINGNAME%TYPE) IS
    SELECT TRIM(canHaveFormatMask)
    FROM   isr$grouping
    WHERE  groupingname = csGroupingName;

BEGIN
  isr$trace.stat('begin', 'oParameter.sToken: '||oParameter.sToken, sCurrentName);

  olParameterOld := olParameter;

  CASE

    ------------ create a subnode ----------------------
    ----------------------------------------------------
    WHEN UPPER(oParameter.sToken) like 'CAN_CREATE_%_NODES'
      OR UPPER(oParameter.sToken) like 'CAN_MODIFY_%_NODES' THEN

      OPEN cGetGroupingWithFormatMask(olParameter(1).sValue);
      FETCH cGetGroupingWithFormatMask INTO olParameter(4).sDisplayed;
      CLOSE cGetGroupingWithFormatMask;

      IF olParameter(4).sDisplayed NOT IN ('T', 'F') THEN
        olParameter(4).sDisplayed := 'F';
      END IF;

      IF  olParameter(4).sDisplayed = 'F'
      AND olParameter(4).sFocus = 'T' THEN
        olParameter(1).sFocus := 'T';
        olParameter(4).sFocus := 'F';
      END IF;

    ELSE
      -- all other tokens
      null;
  END CASE;

  -- compare the objects if something changed only then refresh the mask
  SELECT CASE WHEN COUNT ( * ) = 0 THEN 'F' ELSE 'T' END
    INTO sModifiedFlag
    FROM (SELECT XMLTYPE (VALUE (p)).getStringVal ()
            FROM table (olParameter) p
          MINUS
          SELECT XMLTYPE (VALUE (p)).getStringVal ()
            FROM table (olParameterOld) p);

  isr$trace.stat('end', 'sModifiedFlag: '||sModifiedFlag, sCurrentName);
  RETURN(oErrorObj);

EXCEPTION
  WHEN OTHERS then
    oErrorObj.handleError ( Stb$typedef.cnSeverityWarning, utd$msglib.getmsg ('exUnexpectedError', stb$security.getCurrentlanguage,  csP1 => sqlerrm(sqlcode)),
        sCurrentName, dbms_utility.format_error_stack ||chr(10)|| dbms_utility.format_error_backtrace );
    RETURN(oErrorObj);
END checkDependenciesOnMask;

END ISR$CUSTOMER;