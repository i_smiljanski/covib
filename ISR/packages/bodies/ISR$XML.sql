CREATE OR REPLACE PACKAGE BODY ISR$XML
AS

  CSXMLVERSION        VARCHAR2(100) := '1.0';
  CSSTYLESHEETVERSION VARCHAR2(100) := '2.0';
  CSENCODING          VARCHAR2(100) := STB$UTIL.getSystemParameter('XML_ENCODING');
  --cnTransformNumber       NUMBER := 0;

-- **********************************************************************************************************************
FUNCTION getXmlValue(sValue IN CLOB) RETURN CLOB IS
  sCurrentName   constant varchar2(4000) := $$PLSQL_UNIT||'.getXmlValue('||' '||')';
  sChecked      VARCHAR2(4000);
BEGIN
  isr$trace.stat('begin', 'begin', sCurrentName);
  sChecked      := sValue;
 -- sChecked      := REPLACE (sChecked, '&',  '&amp;');
 -- sChecked      := REPLACE (sChecked, '<',  '&lt;' );
  sChecked :=  dbms_xmlgen.convert(sChecked,0);
  isr$trace.stat('end', 'end', sCurrentName);
  RETURN sChecked;

END getXmlValue;

-- **********************************************************************************************************************
FUNCTION getOriginalValue(sValue IN CLOB) RETURN CLOB IS
  sCurrentName   constant varchar2(4000) := $$PLSQL_UNIT||'.getOriginalValue';
  sChecked      VARCHAR2(4000);
BEGIN
  isr$trace.stat('begin', 'begin', sCurrentName);
  sChecked      := sValue;
  sChecked      := REPLACE (sChecked,'&apos;',  '''');
  sChecked      := REPLACE (sChecked,'&quot;', '"' );
  sChecked      := REPLACE (sChecked,'&gt;',   '>' );
  sChecked      := REPLACE (sChecked,'&lt;',   '<' );
  sChecked      := REPLACE (sChecked,'&amp;',  '&');
  isr$trace.stat('end', 'end', sCurrentName);
  RETURN sChecked;

END getOriginalValue;

-- **********************************************************************************************************************
FUNCTION changeEncoding(clXml IN CLOB) RETURN CLOB IS
  sCurrentName   constant varchar2(4000) := $$PLSQL_UNIT||'.changeEncoding';
  clOutXml         CLOB;
  clXml1           CLOB;
  vBuffer          VARCHAR2(32767);
BEGIN
  isr$trace.stat('begin', 'begin', sCurrentName);
  IF DBMS_LOB.getLength(clXml) IS NOT NULL
  AND DBMS_LOB.getLength(clXml) > 0
  AND DBMS_LOB.INSTR (clXml, '?>') > 0
  AND DBMS_LOB.INSTR (clXml, '?>') < 80 THEN

    dbms_lob.createtemporary(clOutXml, TRUE);
    vBuffer := '<?xml version="'||CSXMLVERSION||'" encoding="'||CSENCODING||'"?>';
    dbms_lob.writeappend(clOutXml, LENGTH(vBuffer), vBuffer);

    clXml1 := SUBSTR (
                clXml
              , DBMS_LOB.INSTR (clXml, '?>') + 2
              , DBMS_LOB.getLength(clXml)
             );
    clOutXml := clOutXml||clXml1;

    isr$trace.stat('end', 'end', sCurrentName);
    RETURN clOutXml;
  ELSE
    isr$trace.stat('end', 'end', sCurrentName);
    RETURN clXml;
  END IF;

END changeEncoding;

-- **********************************************************************************************************************
FUNCTION clobToXml (clXml in CLOB) RETURN XMLTYPE
IS
  sCurrentName   constant varchar2(4000) := $$PLSQL_UNIT||'.clobToXml';
  xReturn XMLTYPE;
BEGIN
  --isr$trace.stat('begin', 'begin', sCurrentName);
  if dbms_lob.getlength(clXml) is null then
    xReturn := null;
  else
    xReturn := XMLTYPE(clXml);
  end if;
  isr$trace.stat('end', 'end', sCurrentName);
  
  RETURN xReturn;
END;

-- **********************************************************************************************************************
FUNCTION XMLToClob (xXml in XMLTYPE) RETURN CLOB
IS
  sCurrentName   constant varchar2(4000) := $$PLSQL_UNIT||'.XMLToClob';
  clReturn CLOB;
BEGIN
  isr$trace.stat('begin', 'begin', sCurrentName);
  clReturn := xXml.getClobVal();
  isr$trace.stat('end', 'end', sCurrentName);
  return clReturn;
END;

-- **********************************************************************************************************************
FUNCTION initXml(sRootName in VARCHAR2,
                   sCoding in VARCHAR2 default STB$UTIL.GETSYSTEMPARAMETER('XML_ENCODING'))
  RETURN XMLTYPE
IS
  sCurrentName   constant varchar2(4000) := $$PLSQL_UNIT||'.initXml';
  xReturnXml xmltype;
BEGIN
  isr$trace.stat('begin', 'begin', sCurrentName);
  xReturnXml := ISR$XML.clobToXml('<?xml version="'||CSXMLVERSION||'" encoding="'||sCoding||'"?><' || sRootName || '/>');
  isr$trace.stat('end', 'end', sCurrentName);
  return xReturnXml;
END initXml;

-- **********************************************************************************************************************
function putRowForTransform(csTransformId in varchar2, csFilename  in  varchar2, csClobdata in clob)
  return STB$OERROR$RECORD
is
  pragma autonomous_transaction;
  sCurrentName constant varchar2(200) := $$PLSQL_UNIT||'.putRowForTransform(' || csTransformId || ', ' || csFilename || ')';
  oErrorObj             STB$OERROR$RECORD := STB$OERROR$RECORD();
  sMsg                  varchar2(4000);
begin
  isr$trace.stat('begin', 'begin', sCurrentName);
  insert into isr$transfer$transform$tmp (transformId, filename, clobdata, sessionid)
     values (csTransformId, csFilename, csClobdata, STB$SECURITY.getCurrentSession);
  commit;
  isr$trace.stat('end', 'end', sCurrentName);
  return oErrorObj;
exception
  when others then
    sMsg :=  utd$msglib.getmsg ('exInsertFailed', stb$security.getcurrentlanguage, sqlerrm(sqlcode),  'table = "isr$remote$transform$temp", filename ="' || csFilename || '"');
    oErrorObj.handleError ( Stb$typedef.cnSeverityCritical, sMsg, sCurrentName,
                            dbms_utility.format_error_stack || CHR(10) || dbms_utility.format_error_backtrace  );
    return oErrorObj;
end putRowForTransform;

--**********************************************************************************************************************
function deleteTmpData(csTransformId in varchar2) return STB$OERROR$RECORD
  is pragma autonomous_transaction;
  oErrorObj       STB$OERROR$RECORD  := STB$OERROR$RECORD ();
  sCurrentName    constant varchar2 (100) := $$PLSQL_UNIT || '.deleteTmpData(' || csTransformId || ')';
  sUserMsg        varchar2(4000);
begin
  isr$trace.stat('begin', 'begin', sCurrentName);
  delete from isr$transfer$transform$tmp where transformid = csTransformId;
  commit;
  isr$trace.stat('end', 'end', sCurrentName);
  return oErrorObj;
exception
  when others then
    sUserMsg := utd$msglib.getmsg ('exDeleteTmpDataTxt', stb$security.getcurrentlanguage,  csP1 =>sqlerrm(sqlcode));
    oErrorObj.handleError ( Stb$typedef.cnSeverityCritical, sUserMsg, sCurrentName,
                          dbms_utility.format_error_stack || CHR(10) || dbms_utility.format_error_backtrace );
    return oErrorObj;
end deleteTmpData;

--**********************************************************************************************************************
function deleteTmpDataForSession(cnSessionId in number) return STB$OERROR$RECORD
  is pragma autonomous_transaction;
  oErrorObj       STB$OERROR$RECORD  := STB$OERROR$RECORD ();
  sCurrentName    constant varchar2 (100) := $$PLSQL_UNIT || '.deleteTmpDataForSession(' || cnSessionId || ')';
  sUserMsg        varchar2(4000);
begin
  isr$trace.stat('begin', 'begin', sCurrentName);
  delete from isr$transfer$transform$tmp where sessionid = cnSessionId;
  commit;
  isr$trace.stat('end', 'end', sCurrentName);
  return oErrorObj;
exception
  when others then
    sUserMsg := utd$msglib.getmsg ('exDeleteTmpDataTxt', stb$security.getcurrentlanguage,  csP1 =>sqlerrm(sqlcode));
    oErrorObj.handleError ( Stb$typedef.cnSeverityCritical, sUserMsg, sCurrentName,
                          dbms_utility.format_error_stack || CHR(10) || dbms_utility.format_error_backtrace );
    return oErrorObj;
end deleteTmpDataForSession;
-- **********************************************************************************************************************
FUNCTION XSLTTransform(cXSLT   IN CLOB,
                       cXML    IN CLOB,
                       cResult IN OUT CLOB,
                       sParameter IN VARCHAR2 DEFAULT NULL, cnJobid in number) RETURN VARCHAR2
IS
  sCurrentName          constant varchar2(100) := $$PLSQL_UNIT||'.XSLTTransform (' || sParameter  || ')';
  oErrorObj             STB$OERROR$RECORD :=STB$OERROR$RECORD();
  sMsg                  varchar2(4000);

  cursor cGetTransformServer is
  select observer_host, port, context , timeout, alias
  from isr$serverobserver$v
  where servertypeid = 12 and nvl(runstatus, 'F') = 'T';

  rGetTransformServer cGetTransformServer%rowtype;
  exTransformServerNotFound  exception;

  sDelimiter  isr$transfer$file$config.configname%type;
  xConfigXml             xmltype;
  sTransformId  isr$transfer$transform$tmp.transformId%type;

  cursor cGetResult (csTransformId in isr$transfer$transform$tmp.transformId%type) is
  select clobdata
  from isr$transfer$transform$tmp
  where transformId = csTransformId
  and fileName = 'result';


  sStyleSheet  isr$stylesheet.stylesheet%type;
  sStructured  isr$stylesheet.structured%type;

BEGIN
  isr$trace.stat('begin', 'sParameter: '||sParameter, sCurrentName);
  isr$trace.debug_all('value', 'dbms_lob.getLength(cXSLT): '||dbms_lob.getLength(cXSLT), sCurrentName, cXSLT);
  isr$trace.debug_all('value', 'dbms_lob.getLength(cXML): '||dbms_lob.getLength(cXML), sCurrentName, cXML);
  isr$trace.debug_all('value', 'dbms_lob.getLength(cResult): '||dbms_lob.getLength(cResult), sCurrentName, cResult);

  select dbms_random.string('A', 8) into sTransformId from dual;
  if cnJobid is not null then
    sTransformId := cnJobid || '_'|| sTransformId;
  end if;
  oErrorObj := putRowForTransform(sTransformId, 'xmlFile', cXML);
  oErrorObj := putRowForTransform(sTransformId, 'xsltFile', cXSLT);

  for rGetIncludedStylesheet in (
          select * from(select filename,stylesheet,stylesheetid, structured,SUM(1) OVER (PARTITION BY filename)  count,ROW_NUMBER () OVER ( PARTITION BY filename ORDER BY filename) bytes_rank
          from isr$stylesheet where structured in( 'I', 'B'))
          where bytes_rank < 2) loop

      sStyleSheet := rGetIncludedStylesheet.stylesheet;
      sStructured := rGetIncludedStylesheet.structured;

    if rGetIncludedStylesheet.count > 1 then
      for rGetDoubleStylesheet in ( select stylesheet, structured
                  from isr$stylesheet s, isr$output$transform ot, stb$jobsession j
                  where structured in( 'I', 'B')
                  and ot.stylesheetid = s.stylesheetid
                  and ot.outputid = STB$JOB.getJobParameter ('OUTPUTID', j.jobid)
                  and j.jobid = cnJobid
                  and s.filename = rGetIncludedStylesheet.filename) loop
      sStyleSheet := rGetDoubleStylesheet.stylesheet;
      sStructured := rGetDoubleStylesheet.structured;
      end loop;

    end if;
    if sStructured = 'I' then
      oErrorObj := putRowForTransform(sTransformId, rGetIncludedStylesheet.filename, sStyleSheet);
    elsif cnJobid is not null then
      oErrorObj := putRowForTransform(sTransformId, rGetIncludedStylesheet.filename, isr$transform.generateBStylesheet ( STB$JOB.getJobParameter ('MASTERTEMPLATE', cnJobid)) );
    end if;
  end loop;

 -- cnTransformNumber := cnTransformNumber + 1;
  sDelimiter  :=  isr$file$transfer$service.getKeyDelimiter('TRANSFORM');

  open cGetTransformServer;
  fetch cGetTransformServer into rGetTransformServer;
  if cGetTransformServer%notfound then
    close cGetTransformServer;
     sMsg :=  Utd$msglib.GetMsg('exTransformServerNotFoundText',Stb$security.GetCurrentLanguage);
    raise exTransformServerNotFound;
  end if;

  select xmlelement ("transformations",
             xmlelement ("jobId", sTransformId),
             XMLELEMENT ("serverName", rGetTransformServer.alias),
             xmlelement ("transformNumber", null),
             xmlelement ("keyDelimiter", sDelimiter),
             xmlelement ("HOST", rGetTransformServer.observer_host),
             xmlelement ("PORT", rGetTransformServer.port),
             xmlelement ("CONTEXT", rGetTransformServer.context),
             xmlelement("servletConfiguration",
               xmlelement("xmlConfig", 'TRANSFORM'),
               xmlelement("xsltConfig", 'TRANSFORM'),
               xmlelement("result", 'TRANSFORM_RESULT1')),
             xmlelement ("xsltTransformation",
                                 xmlelement ("xml", sTransformId ||sDelimiter ||'xmlFile'  ),
                                 xmlelement ("xslt", sTransformId ||sDelimiter || 'xsltFile'  ),
                                 xmlelement ("result", sTransformId || sDelimiter || 'result' || sDelimiter || STB$SECURITY.getCurrentSession ),
                                 xmlelement("transformationParameters", sParameter)))
  into xConfigXml
  from dual;

  isr$trace.debug('xConfigXml','s. logclob', sCurrentName, xConfigXml);
  oErrorObj := isr$transform.callJavaTransform(xConfigXml, rGetTransformServer.timeout);

  close cGetTransformServer;

  open cGetResult (sTransformId);
  fetch cGetResult into cResult;
  if cGetResult%notfound then
    close cGetResult;
    isr$trace.stat('end', 'cResult is null', sCurrentName);
    return 'F';
  end if;
  close cGetResult;

  oErrorObj := deleteTmpData(sTransformId);


  isr$trace.stat('end', 'dbms_lob.getLength(cResult): '||dbms_lob.getLength(cResult), sCurrentName, cResult);
  RETURN 'T';

EXCEPTION
  WHEN OTHERS THEN
    --ISR$TRACE.ERROR(SQLCODE, SQLERRM, 'ISR$XML.XSLTTransform');
    oErrorObj.handleError ( Stb$typedef.cnSeverityWarning, sMsg, sCurrentName,
                           dbms_utility.format_error_stack || CHR(10) || dbms_utility.format_error_backtrace);
    RETURN SQLERRM;
END XSLTTransform;

-- **********************************************************************************************************************
FUNCTION getValuesProperties (nJobId IN NUMBER
                            , lXpathes IN ISR$ATTRIBUTE$LIST
                            , xXml IN XMLTYPE) RETURN CLOB
IS
  sCurrentName   constant varchar2(4000) := $$PLSQL_UNIT||'.getValuesProperties('||nJobId||')';
  sStylesheet    VARCHAR2 (32000);
  cResult        CLOB;
  sParameter     VARCHAR2(500);
  sValue         VARCHAR2(500);
  sSeparator     VARCHAR2(50);

  sReturn        VARCHAR2(4000);

BEGIN
  isr$trace.stat('begin', 'begin', sCurrentName);
  sStylesheet := '<?xml version="1.0" encoding="'||CSENCODING||'"?>' ||
                 '<xsl:stylesheet version="2.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform" xmlns:xs="http://www.w3.org/2001/XMLSchema" xmlns:fn="http://www.w3.org/2005/xpath-functions" exclude-result-prefixes="xsl xs fn">' ||
                 '  <xsl:output method="xml" indent="yes"/>' ||
                 '  <xsl:template match="/">' ||
                 '    <properties>';
  IF lXpathes.count > 0 THEN
    FOR i IN lXpathes.first..lXpathes.last LOOP
      sParameter := lXpathes(i).sValue;
      sValue     := lXpathes(i).sParameter;
      IF instr(sValue, '^', -1) > 0 THEN
        sSeparator := substr(sValue, 0, instr(sValue, '^', -1) - 1);
        sValue := substr(sValue, instr(sValue, '^', -1) + 1);
      END IF;

      IF trim(sSeparator) IS NULL THEN
        sSeparator := ' ';
      END IF;

      sStylesheet := sStylesheet ||
                     '<custom-property name="'|| sParameter ||'">' ||
                     '  <xsl:value-of separator="' || sSeparator || '" select="' || REPLACE(getXmlValue(sValue),'"',chr(39)) || '"/>' ||
                     '</custom-property>';
    END LOOP;
  END IF;
  sStylesheet := sStylesheet ||
                 '    </properties>' ||
                 '  </xsl:template>' ||
                 '</xsl:stylesheet>';

  sReturn := XSLTTransform(sStylesheet, XMLToClob(xXml), cResult, null, nJobId);

--  IF lXpathes.count > 0 THEN
--    cResult := '<properties>';
--    FOR i IN lXpathes.First..lXpathes.Last LOOP
--      sParameter := lXpathes(i).sValue;
--      sValue     := lXpathes(i).sParameter;
--      IF INSTR(sValue, '^', -1) > 0 THEN
--        sSeparator := SUBSTR(sValue, 0, INSTR(sValue, '^', -1) - 1);
--        sValue := SUBSTR(sValue, INSTR(sValue, '^', -1) + 1);
--      END IF;
--
--      IF TRIM(sSeparator) IS NULL THEN
--        sSeparator := ' ';
--      END IF;

--      cResult := cResult || '  <custom-property name="'|| sParameter ||'">'||getXmlValue(ISR$XML.valueOf(xXml, sValue))||'</custom-property>';
--    END LOOP;
--    cResult := cResult||'</properties>';
--  END IF;
  isr$trace.stat('end', 'end', sCurrentName);
  RETURN cResult;
END getValuesProperties;


-- **********************************************************************************************************************
FUNCTION ValueAsClob (xXml IN XMLTYPE,
                  sXpath IN VARCHAR2) RETURN clob IS
  sCurrentName   constant varchar2(100) := $$PLSQL_UNIT||'.ValueAsClob';
  oErrorObj      STB$OERROR$RECORD :=STB$OERROR$RECORD();
  sResult     VARCHAR2(4000);
  cResult     CLOB;
  xxquery   xmltype;
  sStylesheet VARCHAR2(4000);
  sReturn   clob;
  changedXpath  varchar2(4000);
BEGIN
  isr$trace.stat('begin', 'begin', sCurrentName);

  with xmldata as (select xxml from dual)
  select xmlquery(sXpath passing xxml returning content)  into xxquery
  from xmldata;

  isr$trace.debug('xXml',sXpath, sCurrentName, xXml);
  if xxquery is not null then
    sReturn := xxquery.getclobval;
    isr$trace.debug('result',sXpath, sCurrentName, sReturn);
  end if;

  isr$trace.stat('end', 'end', sCurrentName);
  return sReturn;
exception
  when others then
    isr$trace.error('error',sXpath, sCurrentName, xXml);
    oErrorObj.handleError ( Stb$typedef.cnSeverityCritical, utd$msglib.getmsg ('valueOfXmlError', stb$security.getcurrentlanguage,  csP1 => sqlerrm(sqlcode)),
    sCurrentName, dbms_utility.format_error_stack || CHR(10) || dbms_utility.format_error_backtrace);
    return null;
END ValueAsClob;


-- **********************************************************************************************************************
FUNCTION ValueOf (xXml IN XMLTYPE,
                  sXpath IN VARCHAR2) RETURN VARCHAR2 IS
  sCurrentName   constant varchar2(100) := $$PLSQL_UNIT||'.ValueOf';
  oErrorObj      STB$OERROR$RECORD :=STB$OERROR$RECORD();
  sResult     VARCHAR2(4000);
  cResult     CLOB;
  xxquery   xmltype;
  sStylesheet VARCHAR2(4000);
  sReturn   varchar2(4000);
  changedXpath  varchar2(4000);
BEGIN
  isr$trace.stat('begin', sXpath, sCurrentName);

  with xmldata as (select xxml from dual)
  select xmlquery(sXpath passing xxml returning content)  into xxquery
  from xmldata;

--  isr$trace.debug('xxquery',sXpath, sCurrentName, xxquery);
  if xxquery is not null then
    sReturn := dbms_xmlgen.convert(xxquery.getclobval,1);
    isr$trace.debug('result',sXpath, sCurrentName, sReturn);
  end if;

  isr$trace.stat('end', 'end', sCurrentName);
  return sReturn;
exception
  when others then
    isr$trace.error('error',sXpath, sCurrentName, xXml);
    oErrorObj.handleError ( Stb$typedef.cnSeverityCritical, utd$msglib.getmsg ('valueOfXmlError', stb$security.getcurrentlanguage,  csP1 => sqlerrm(sqlcode)),
    sCurrentName, dbms_utility.format_error_stack || CHR(10) || dbms_utility.format_error_backtrace);
    return null;
END ValueOf;



-- *[ISRC-1117]*********************************************************************************************************************
FUNCTION InsertChildXMLFragment(xXml in XMLTYPE,
                                     sXpath IN VARCHAR2,
                                     xAppendXML IN XMLTYPE) RETURN XMLTYPE
  IS
    sCurrentName   constant varchar2(4000) := $$PLSQL_UNIT||'.InsertChildXMLFragment';
    oErrorObj      STB$OERROR$RECORD :=STB$OERROR$RECORD();
    xXml1 XMLTYPE;
    sContent varchar2(4000);
  BEGIN
    isr$trace.stat('begin', 'begin', sCurrentName);
    isr$trace.debug('xXmlLoc', 'sXpath: '|| sXpath, sCurrentName, xXml);
    
    if xXml is not null then
     sContent :='copy $tmp := . modify (for $i in $tmp'||sXpath||' return insert node $appendXML into $i) return $tmp';
     select xmlquery(sContent
        passing xAppendXML as "appendXML",
              xXml
              returning content)
      into xXml1 from dual;
    end if;  
    isr$trace.stat('end', 'end', sCurrentName);
    RETURN xXml1;
  EXCEPTION
  WHEN OTHERS THEN
    oErrorObj.handleError ( Stb$typedef.cnSeverityCritical,  utd$msglib.getmsg ('ERROR', stb$security.getCurrentLanguage) || SQLCODE, sCurrentName,
       dbms_utility.format_error_stack ||chr(10)||chr(13)|| dbms_utility.format_error_backtrace);
    RETURN null;
  END InsertChildXMLFragment;

-- **********************************************************************************************************************
PROCEDURE InsertChildXMLFragment(xXml in out XMLTYPE,
                                      sXpath IN VARCHAR2,
                                      clXML IN CLOB)
IS
  sCurrentName   constant varchar2(4000) := $$PLSQL_UNIT||'.InsertChildXMLFragment';
  oErrorObj      STB$OERROR$RECORD :=STB$OERROR$RECORD();
  xFragment      XMLTYPE;
BEGIN
  isr$trace.stat('begin', 'begin', sCurrentName);
  xXml := InsertChildXMLFragment(xXml, sXpath, XMLTYPE(clXML));
  isr$trace.stat('end', 'end', sCurrentName);
  EXCEPTION
  WHEN OTHERS THEN
    oErrorObj.handleError ( Stb$typedef.cnSeverityCritical,  utd$msglib.getmsg ('ERROR', stb$security.getCurrentLanguage) || SQLCODE, sCurrentName,
       dbms_utility.format_error_stack ||chr(10)||chr(13)|| dbms_utility.format_error_backtrace);
END InsertChildXMLFragment;

                                   
-- **********************************************************************************************************************
PROCEDURE CreateNode (xXml in out XMLTYPE,
                      sXpath IN VARCHAR2,
                      sName  IN VARCHAR2,
                      sValue IN VARCHAR2)
IS
  sCurrentName   constant varchar2(100) := $$PLSQL_UNIT||'.CreateNode';
  oErrorObj      STB$OERROR$RECORD :=STB$OERROR$RECORD();
  xFragment      XMLTYPE;
BEGIN
  isr$trace.stat('begin', 'begin', sCurrentName);
  --isr$trace.debug('xXml',sXpath, sCurrentName, xXml);
  select XMLElement ( evalname (sName), sValue)
    into xFragment
    from dual;
  
  xXml := InsertChildXMLFragment(xXml, sXpath, xFragment);
  isr$trace.debug('result',sName || ';' || sValue, sCurrentName, xXml);
  isr$trace.stat('end', 'end', sCurrentName);
exception
  when others then
    isr$trace.error('error','<'||sName||'>'||sValue||'</'||sName||'>', sCurrentName, xXml);
    isr$trace.error('sXpath', sXpath, sCurrentName, dbms_utility.format_error_stack || CHR(10) || dbms_utility.format_error_backtrace);
END CreateNode;

-- **********************************************************************************************************************
PROCEDURE CreateNodeAttr (xXml in out XMLTYPE,
                          sXpath     IN VARCHAR2,
                          sName      IN VARCHAR2,
                          sValue     IN VARCHAR2,
                          sAttrName  IN VARCHAR2,
                          sAttrValue IN VARCHAR2)
IS
  sCurrentName   constant varchar2(4000) := $$PLSQL_UNIT||'.CreateNodeAttr';
  xFragment      XMLTYPE;
BEGIN
  isr$trace.stat('begin', 'begin', sCurrentName);
  select XMLElement (
           evalname (sName),
           XMLAttributes (sAttrvalue as evalname (sAttrName)),
           sValue
         )
    into xFragment
    from dual;
  xXml := InsertChildXMLFragment(xXml, sXpath, xFragment);
  isr$trace.stat('end', 'end', sCurrentName);
END CreateNodeAttr;

-- **********************************************************************************************************************
PROCEDURE SetAttribute (xXml in out XMLTYPE,
                        sXpath IN VARCHAR2,
                        sName  IN VARCHAR2,
                        sValue IN VARCHAR2)
IS
  sCurrentName   constant varchar2(4000) := $$PLSQL_UNIT||'.SetAttribute('||' '||')';
BEGIN
  isr$trace.stat('begin', 'begin', sCurrentName);
  DeleteNode (xXml, sXpath||'/@'||sName);

  SELECT /*ISR$XML.clobToXml(ISR$XML.xmlToClob(*/INSERTCHILDXML (
                                                xXml
                                              , sXpath
                                              , '@' || sName
                                              , DBMS_LOB.SUBSTR (sValue, 4000, 1)
                                             )--))
    INTO xXml
    FROM DUAL;
  isr$trace.stat('end', 'end', sCurrentName);
END SetAttribute;

-- **********************************************************************************************************************
PROCEDURE UpdateNode (xXml in out XMLTYPE,
                      sXpath IN VARCHAR2,
                      sValue IN VARCHAR2)
IS
  sCurrentName   constant varchar2(100) := $$PLSQL_UNIT||'.UpdateNode';
BEGIN
   isr$trace.stat('begin', 'begin', sCurrentName);
   isr$trace.debug('xXml',sValue, sCurrentName, xXml);

  /*SELECT ISR$XML.clobToXml (
            ISR$XML.xmlToClob (UPDATEXML (xXml, sXpath || '/text()', sValue))
         )*/
    select UPDATEXML (xXml, sXpath || '/text()', sValue)
    INTO xXml
    FROM DUAL;

    isr$trace.debug('result',sXpath, sCurrentName, xXml);
    isr$trace.stat('end', 'end', sCurrentName);

END UpdateNode;

-- **********************************************************************************************************************
PROCEDURE DeleteNode (xXml in out XMLTYPE,
                      sXpath IN VARCHAR2)
IS
  sCurrentName   constant varchar2(4000) := $$PLSQL_UNIT||'.DeleteNode('||' '||')';
BEGIN
  isr$trace.stat('begin', 'begin', sCurrentName);

  BEGIN
     --SELECT ISR$XML.clobToXml (ISR$XML.xmlToClob (DELETEXML (xXml, sXpath)))
     SELECT DELETEXML (xXml, sXpath)
       INTO xXml
       FROM DUAL;
  EXCEPTION
     WHEN OTHERS THEN
        NULL;
  END;
  isr$trace.stat('end', 'end', sCurrentName);
END DeleteNode;

-- **********************************************************************************************************************
function getXml( csSelectStatement varchar2 )
  return clob
is  
  sCurrentName   constant varchar2(4000) := $$PLSQL_UNIT||'.getxml('||' '||')';  
  oErrorObj      STB$OERROR$RECORD := STB$OERROR$RECORD();  
  nCtxHandle     number;
  clReturnXml    clob;
begin
  isr$trace.stat('begin', substr(csSelectStatement,1,4000), sCurrentName);
  nCtxHandle :=  DBMS_XMLGEN.newcontext(csSelectStatement);
  DBMS_XMLGEN.setNullHandling (nCtxHandle, DBMS_XMLGEN.empty_tag);
  
  select to_clob( DBMS_XMLGEN.getxml(nCtxHandle) ) 
  into   clReturnXml
  from   dual;
  
  DBMS_XMLGEN.closecontext( nCtxHandle );  
  return clReturnXml;  
  isr$trace.stat('end', 'end', sCurrentName);

exception 
  when others then
    oErrorObj.handleError( STB$TYPEDEF.cnSeverityCritical, utd$msglib.getmsg ('exUnexpectedError', stb$security.getCurrentLanguage, sqlerrm(sqlcode)), sCurrentName, 
                           substr(dbms_utility.format_error_stack || CHR(10) || dbms_utility.format_error_backtrace, 1,4000));    
    return 'ERROR in '||sCurrentName;
end getxml;

-- **********************************************************************************************************************
function getCanonicalDynSql( cclCanonical clob )
  return clob
is
  sSql          varchar2(32000);
  sSqlSnippet#0 varchar2(32000);  
  sSqlSnippet#1 varchar2(32000);
  sSqlSnippet#2 varchar2(32000);
  sSqlSnippet#3 varchar2(32000);  
  sSqlSnippet#4 varchar2(32000);
  sReturnSql    varchar2(32000);
begin

  sSqlSnippet#0 :=     'with base_data as (  select :1 data_col     --  :1  -->  USING xmltype(cclCanonical)'||chr(10)    
                    || '                     from   dual )';

  -- handle snippet#1 and #4
  sSql :=  'with base_data as (  select :1 data_col
                                 from   dual ), 
                 base as ( select distinct xmldata.column_name col
                           from   base_data a,
                                  xmltable( ''for $i in /*/*/* return $i''
                                             passing  data_col
                                             columns column_name           varchar2(4000) path ''./name()''    
                                          ) xmldata )
           select distinct ''select xmldata.''||listagg(lower(b.col),'',''||chr(10)||''       xmldata.'') within group( order by 1 ) over( partition by 1 ),
                  ''     columns ''||listagg(lower(b.col)||'' varchar2(4000) path ''''./''||b.col||''/text()'''''','',''||chr(10)||''            '') within group( order by 1 ) over( partition by 1 )||chr(10)||''            ) xmldata''
           from   base b';
  
  execute immediate sSql
  into    sSqlSnippet#1, sSqlSnippet#4
  using   xmltype(cclCanonical);   
  
  sSqlSnippet#2 := 'from base_data a,';
  
  sSqlSnippet#3 :=     '     xmltable( ''for $i in ROWSET/ROW return$i'''||chr(10)
                    || '     passing  data_col';  
  
  /*** use for debugging
  dbms_output.put_line('-------- sSqlSnippet#0 --------');
  dbms_output.put_line(sSqlSnippet#0);
  dbms_output.put_line('-------- sSqlSnippet#1 --------');
  dbms_output.put_line(sSqlSnippet#1);
  dbms_output.put_line('-------- sSqlSnippet#2 --------');
  dbms_output.put_line(sSqlSnippet#2);
  dbms_output.put_line('-------- sSqlSnippet#3 --------');
  dbms_output.put_line(sSqlSnippet#3);
  dbms_output.put_line('-------- sSqlSnippet#4 --------');
  dbms_output.put_line(sSqlSnippet#4);
  ***/

  sReturnSql := sSqlSnippet#0||chr(10)||sSqlSnippet#1||chr(10)||sSqlSnippet#2||chr(10)||sSqlSnippet#3||chr(10)||sSqlSnippet#4||chr(10);
  return sReturnSql;

exception
  when others then
    return sqlerrm(sqlcode)||' '||chr(10)||chr(10)||dbms_utility.format_error_backtrace;

end getCanonicalDynSql;

-- **********************************************************************************************************************
function generateCanonicalQuery( csTable  varchar2,
                                 csColumn varchar2,
                                 csFilter varchar2 default '1 = 1' )
  return clob
is 
  sSql          varchar2(32000);
  sSqlSnippet#1 varchar2(32000);
  sSqlSnippet#2 varchar2(32000);
  sSqlSnippet#3 varchar2(32000);  
  sSqlSnippet#4 varchar2(32000);
  sSqlSnippet#5 varchar2(32000);
  sReturnSql    clob;
begin
  -- handle snippet#1 and #4
  sSql :=  'with base as (
                           select distinct xmldata.column_name col
                           from   '||csTable||' a,
                                  xmltable( ''for $i in /*/*/* return $i''
                                             passing  xmltype('||csColumn||')
                                             columns column_name           varchar2(4000) path ''./name()''    
                                          ) xmldata
                            where ( '||csFilter||' ) )
           select distinct ''select xmldata.''||listagg(lower(b.col),'',''||chr(10)||''       xmldata.'') within group( order by 1 ) over( partition by 1 ),
                  ''    columns ''||listagg(lower(b.col)||'' varchar2(4000) path ''''./''||b.col||''/text()'''''','',''||chr(10)||''            '') within group( order by 1 ) over( partition by 1 )||chr(10)||''            ) xmldata''
           from   base b';
  
  execute immediate sSql
  into    sSqlSnippet#1, sSqlSnippet#4;   
  
  dbms_output.put_line(sSqlSnippet#1);

  -- handle snippet#2
  sSqlSnippet#2 := 'from '|| csTable ||' a,';

  -- handle snippet#3
    sSql := 'select distinct ''    xmltable( ''''for $i in ''||xmldata.node#1||''/''||xmldata.node#2||'' return $i''''''||chr(10)||''    passing  xmltype('||csColumn||')''
             from   '||csTable||' a,
                     xmltable( ''for $i in /*/* return $i''
                                passing  xmltype('||csColumn||')
                                columns node#2                varchar2(4000) path ''./name()'',
                                        node#1                varchar2(4000) path ''./../name()''
                             ) xmldata
              where ( '||csFilter||' )';

  execute immediate sSql
  into    sSqlSnippet#3;

  -- handle snippet#5
  sSqlSnippet#5 := 'where  ( '||csFilter||' );';

  sReturnSql :=   sSqlSnippet#1||chr(10)||sSqlSnippet#2||chr(10)||sSqlSnippet#3||chr(10)||sSqlSnippet#4||chr(10)||sSqlSnippet#5;
 
  return sReturnSql;

exception
  when others then
    return sqlerrm(sqlcode)||' '||chr(10)||chr(10)||dbms_utility.format_error_backtrace;
end generateCanonicalQuery;


END ISR$XML;