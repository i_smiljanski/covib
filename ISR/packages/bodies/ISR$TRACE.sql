CREATE OR REPLACE package body ISR$trace
is
  oErrorObj               STB$OERROR$RECORD; 

  cclDefaultNull          constant CLOB := '(null)';
  cnDefaultUserLogLevel   constant number := 0;
  --if package called from external session, not from backend (e.g. servlet):
  sExternalReference   isr$log.reference%type;
  sExternalLogLevel    isr$log.logLevelName%type;

  cnLoglevelNo_DEBUG      constant number := getLoglevelNo('DEBUG');
  cnLoglevelNo_DEBUG_ALL  constant number := getLoglevelNo('DEBUG_ALL');
  cnLoglevelNo_INFO       constant number := getLoglevelNo('INFO');
  cnLoglevelNo_INFO_ALL   constant number := getLoglevelNo('INFO_ALL');
  cnLoglevelNo_STATISTIC  constant number := getLoglevelNo('STATISTIC');
  cnLoglevelNo_WARN       constant number := getLoglevelNo('WARN');
  cnLoglevelNo_ERROR      constant number := getLoglevelNo('ERROR');
  cnLoglevelNo_FATAL      constant number := getLoglevelNo('FATAL');
  
  nTargetMemory Number;
  nReturn Number;

  
  nSystemLoglevel                  number;  -- initialized in MAIN
  nSessionLogLevel                 number;  -- vs 09.May.2019 [ISRC-1111], [ABBIS-218]
  bSessionLoglevelChangeAllowed    boolean;
  sCurrentUser                     varchar2(100);
  sUserName                        varchar2 (255);
  
  
  cursor cGetIpAndPort( cnSessionId number ) is
    select jobid, ip, port
    from  stb$jobsession
    where oraclejobsessionid = cnSessionId;

  nJobID     number;  
  sIp        varchar2(4000);
  nPort      number;
  nSessionId number;
  

--**************************************************************************************************************************
function getSystemLoglevel
  return number
is
  nReturnLevel number := cnErrorLoglevelNo;
begin
  for rec in ( select case
                        when stb$util.isNumeric( parametervalue ) = 'T' then getAdjustedLoglevelNo( TO_NUMBER(parametervalue) )
                        else getloglevelno (parametervalue)
                      end parametervalue
               from   stb$systemparameter
               where  parametername = 'LOGLEVEL' )
  loop  -- loops only once
    nReturnlevel := rec.parametervalue;  
  end loop;

  return nReturnlevel;

exception when others then
  return cnErrorLoglevelNo;   -- return default error loglevel in that case
end getSystemLoglevel;

--**************************************************************************************************************************
function getCallStack return varchar2
is
--******************************************************************************
--  Date and Autor: 18 Oct 2011 IS
--  Private. Returns the formated call stack
--  Sample output:
--   4 function ISROWNER35_STAB.TESTCALLSTACK | 18 function ISROWNER35_STAB.GETCALLSTACK
--******************************************************************************
  csEndOfLine   constant char(1) := CHR(10);
  csEndOfField  constant char(1) := CHR(32);
  nPtFinLigne   number;
  nPtDebLigne   number;
  nCpt          number;
  sAllLines     varchar2(4000);
  sResult       varchar2(4000);
  sLine         varchar2(4000);
  sUserCode     varchar2(4000);
begin
  sAllLines  := DBMS_UTILITY.format_call_stack;
  nCpt     := 2;
  nPtFinLigne := LENGTH(sAllLines);
  nPtDebLigne := nPtFinLigne;

  while nPtFinLigne > 0 and nPtDebLigne > 83 loop
    nPtDebLigne := INSTR (sAllLines, csEndOfLine, -1, nCpt) + 1 ;
    nCpt    := nCpt + 1;
    -- process the line
    sLine    := SUBSTR(sAllLines, nPtDebLigne, nPtFinLigne - nPtDebLigne);
    sUserCode := REPLACE(REPLACE(TRIM(STB$UTIL.getrightDelim(sLine, csEndOfField)), 'package body',''), 'anonymous block','');

    if INSTR(sUserCode,'ISR$TRACE') = 0 then
      if nCpt > 3 then
        sResult := sResult ||' | ';
      end if;
      sResult := sResult||sUserCode;
    end if;
    nPtFinLigne := nPtDebLigne - 1;
  end loop;

  return sResult;

end getCallStack;

--********************************************************************************************************
function getLogLevel return number 
 -- This function returns a number that is used as the determined loglevel for a user.
 -- If a package loglevel is defined and the value in isr$pkg$loglevel.force = 'T' then chose this loglevel
 -- If no pkg loglevel applies, then take the greatest of these:
 --  package-loglevel with force = 'F'
 --  JobLoglevel,
 --  JobUserLoglevel,
 --  UserLoglevel
 --  SystemLoglevel
 --
 -- Return:
 --  logLevelNumber that is further interpreted as the userloglevel
is
  nReturnUserLoglevelNo number;
  sUsrPkgLoglevel       isr$loglevel$pkg.loglevel%type;
  sUsrLogPackage        isr$loglevel$pkg.logpackage%type;
  sUsrLogPackageForce   isr$loglevel$pkg.force%type;

  -----------
  -----------
  function getJobLoglevel return number result_cache is
    -- This function returns the loglevel within a job
    -- The former known filters 'INVISIBLE_FILTER_DEBUG_JOB' and 'FILTER_DEBUG_JOB' are not supported
    -- The foglevel is defined by the job-parameter FILTER_LOGLEVELNAME
    -- The default return-level is cnErrorLoglevelNo
    nJobNo                number;
    nFilterLevel          number := cnErrorLoglevelNo;
    nFilterLevelInvisible number := cnErrorLoglevelNo;  -- [ISRC-723]
    nProfilerLevel        number := cnErrorLoglevelNo;
    nReturnLevel          number := cnErrorLoglevelNo;
  begin
    nJobNo := NVL(STB$JOB.getJobid,cnErrorLoglevelNo);
    if stb$job.isJobSession(USERENV('SESSIONID')) = 'T' then
      -- check profiler
      -- [ISRC-723] Invisible filters added
      if STB$JOB.getJobParameter('FILTER_CREATE_RUNTIME_PROFILE', nJobNo) = 'T'                      -- normal filter, if SHOW_DIALOG is true
      or STB$JOB.getJobParameter('INVISIBLE_FILTER_CREATE_RUNTIME_PROFILE', nJobNo) = 'T' then       -- INVISIBLE filter, if SHOW_DIALOG is false
        -- Profiler is set active
        nProfilerLevel := getLoglevelNo('STATISTIC');
      end if;
      -- check filter for job-loglevel for active SHOW_DIALOG
      nFilterLevel := getLoglevelNo(STB$JOB.getJobParameter('FILTER_LOGLEVELNAME', nJobNo));
      -- check filter for job-loglevel for inactive SHOW_DIALOG
      nFilterLevelInvisible := getLoglevelNo(STB$JOB.getJobParameter('INVISIBLE_FILTER_LOGLEVELNAME', nJobNo));
      -- the higher value of profiler level and job-loglevel will be returned
      nReturnLevel := GREATEST(nProfilerLevel, nFilterLevel, nFilterLevelInvisible);
    end if;
    return nReturnLevel;
  exception when others then
    -- return default error loglevel in that case
    return cnErrorLoglevelNo;
  end getJobLoglevel;

  -----------
  -----------
  function getJobUserLoglevel return number result_cache is
    -- This Function returns the job-related user-loglevel if it can be determined, otherwise NULL is returned.
    nReturnlevel number := cnErrorLoglevelNo;
  begin
    for rec in ( select usr.loglevel
                   from     stb$jobsession js
                          join
                            stb$jobparameter jp
                          on jp.jobid = js.jobid
                         and jp.parametername = 'USER'
                        join
                          stb$user usr
                        on jp.parametervalue = usr.userno
                  where js.oraclejobsessionid = USERENV ('SESSIONID') )
    loop
      nReturnlevel := rec.loglevel;
      exit; -- leave loop after first dataset found
    end loop;
    return nReturnlevel;
  exception when others then
    -- return default error loglevel in that case
    return cnErrorLoglevelNo;
  end getJobUserLoglevel;

  -----------
  -----------
  function getUserLoglevel(cnCurrentUser IN NUMBER) return number result_cache is

    -- nicht zu chachen, da der sich innerhalb einer session aendern kann--> DOCH- Regel: User muss sich neu anmelden, damit das greift!!.

    --zukunft: liefert zahl def in ISR$LOGLEVEL  GREATEST PRIO 1
    -- This Function returns the user-loglevel if it can be determined, otherwise NULL is returned.
    nReturnlevelNo number := cnErrorLoglevelNo;
    sLoglevelStr  ISR$LOGLEVEL.loglevel%type;
  begin
    for rec in ( select loglevel, loglevelname
                   from  stb$user
                  where userno = cnCurrentUser )
    loop
      nReturnlevelNo := rec.loglevel;
      sLoglevelStr  := rec.loglevelname;
      exit; -- leave loop after first dataset found
    end loop;

    if sLoglevelStr is not null then
       nReturnlevelNo := GetLoglevelNo(sLoglevelStr);
       -- else nothing to do
    end if;
    
    return nReturnlevelNo;
  exception when others then
    -- return default error loglevel in that case
    return cnErrorLoglevelNo;
  end getUserLoglevel;

  -----------
  -----------
  procedure getUsrPkgLoglevel ( nLoglevelStr out varchar2, sLogPackage out varchar2, sForce out char ) is
    -- This Procedure checks, if a user loglevel or a common loglevel is set for a package.
    -- The respective values are changed, if a definition exists.

    -- Callstack
    -- Sample output:  4 function TESTCALLSTACK | 18 function GETCALLSTACK | 25 function GETLOGLEVEL
    sCallpackage varchar2(4000) := REPLACE(getcallstack, SYS_CONTEXT('USERENV', 'CURRENT_SCHEMA')||'.', '');
  begin
    -- set default values
    nLoglevelStr := null;
    sLogPackage := null;
    sForce    := null;

    -- Check if user-package loglevel applies
    for rec in ( select loglevel, logpackage, force
                   from ISR$LOGLEVEL$PKG
                  where userno = stb$security.getCurrentUser or userno is null
                   -- Order is important: 1st the FORCE=T is preferred,
                   --           2nd here, user-specific setting is rated higher than system setting
                   order by DECODE(force,'T', 1, 2), userno nulls last )
    loop
      -- Compare current callstack with package entry in table ISR$PKG$LOGLEVEL
      if INSTR(sCallpackage, TRIM(UPPER(rec.logpackage))) > 0 then
        -- Package entry found in call stack: assign entries for loglevel handling
        nLoglevelStr := rec.loglevel ;
        sLogPackage := TRIM(UPPER(rec.logpackage));
        sForce    := rec.force;
         -- leave loop after first dataset found
        exit;
      end if;
    end loop;
  exception when others then null;
    -- If package loglevel doesn't apply then ignore possible entries
  end getUsrPkgLoglevel;

  -----------
  -----------
  function usePackageLogLevel(cnCurrentUser IN NUMBER) return VARCHAR2 result_cache is
    sExists VARCHAR2(1);
    cursor cExistsPackageLogLevel is
      select 'T'
        from ISR$LOGLEVEL$PKG
       where userno = cnCurrentUser or userno is null;
  begin
    open cExistsPackageLogLevel;
    fetch cExistsPackageLogLevel into sExists;
    if cExistsPackageLogLevel%notfound then
      sExists := 'F';
    end if;
    close cExistsPackageLogLevel;
    return sExists;
  end usePackageLogLevel;

begin
  if sExternalLogLevel is not null then
    return getLoglevelNo(sExternalLogLevel);
  end if;

  IF usePackageLogLevel(stb$security.getCurrentUser) = 'F' THEN
    nReturnUserLoglevelNo := GREATEST( getJobLoglevel, getJobUserLoglevel, getUserLoglevel(stb$security.getcurrentuser), nSystemLoglevel );
  ELSE

    -- Procedure getUsrPkgLoglevel fetches user-package related loglevel, compares with callstack and assgins values.
    -- If NULL is returned for getUsrPkgLoglevel, then no usr-pkg control applies.
    getUsrPkgLoglevel( sUsrPkgLoglevel, sUsrLogPackage, sUsrLogPackageForce );

    case
      when sUsrLogPackageForce = 'T' then
        nReturnUserLoglevelNo := GetLoglevelNo(sUsrPkgLoglevel);
      else -- sUsrLogPackageForce = 'F'
        nReturnUserLoglevelNo := GREATEST( NVL(GetLoglevelNo(sUsrPkgLoglevel),cnErrorLoglevelNo),
                       NVL(getJobLoglevel,cnErrorLoglevelNo),
                       NVL(getJobUserLoglevel,cnErrorLoglevelNo),
                       NVL(getUserLoglevel(stb$security.getcurrentuser),cnErrorLoglevelNo),
                       NVL(nSystemLoglevel,cnErrorLoglevelNo) );

    end case;

  END IF;

  nReturnUserLoglevelNo := getAdjustedLoglevelNo( nReturnUserLoglevelNo );

  return nReturnUserLoglevelNo;

exception when others then
  -- If anything goes wrong, then return default error loglevel
  return cnErrorLoglevelNo;
end getLogLevel;

--**************************************************************************************************************************
function getAdjustedLoglevelNo ( cnLoglevelno number ) return number result_cache
is
begin
  return getLoglevelNo(getLoglevelStr(cnLoglevelno));
exception when others then
  return cnLoglevelno;
end getAdjustedLoglevelNo;

--**************************************************************************************************************************
function getUserLoglevelNo return number result_cache
is
begin
  return getLogLevel;
exception when others then
  return cnErrorLoglevelNo;
end getUserLoglevelNo;

--**************************************************************************************************************************
function getUserLoglevelStr return varchar2 result_cache
is
begin
  return getLoglevelStr(getLogLevel);
exception when others then
  return csErrorLoglevelName;
end getUserLoglevelStr;

--**************************************************************************************************************************
function getJavaUserLoglevelStr return varchar2 
is
  sLoglevelno         number;
  sRetJavaLoglevelStr isr$loglevel.javaloglevel%type;
begin
  sLoglevelno := getLogLevel;
  select javaloglevel
    into sRetJavaLoglevelStr
    from isr$loglevel
   where sLoglevelno between NVL (loglevel_begin, -99999999) and NVL (loglevel_end, 99999999);
  return sRetJavaLoglevelStr;
exception when others then
  return csErrorLoglevelName;
end getJavaUserLoglevelStr;

--**************************************************************************************************************************
function getLoglevelNo( csLoglevelName in varchar2 default csErrorLoglevelName ) return number result_cache
is
  nReturnLoglevelNo number;
  sLoglevelName     ISR$LOGLEVEL.LOGLEVEL%type := TRIM(UPPER(csLoglevelName));
begin
  select case when sLoglevelName = 'DEBUG_ALL' then loglevel_begin else loglevel_end end
    into nReturnLoglevelNo
    from ISR$LOGLEVEL
   where loglevel = sLoglevelName;
  return nReturnLoglevelNo;
exception when others then -- Return default value
  return cnErrorLoglevelNo;
end getLogLevelNo;

--**************************************************************************************************************************
function getLoglevelStr( cnLoglevelNo in number default cnErrorLoglevelNo ) return varchar2 result_cache
is
  sReturnLoglevelStr         ISR$LOGLEVEL.LOGLEVEL%type;
begin
  select loglevel
    into sReturnLoglevelStr
    from ISR$LOGLEVEL ll
   where cnLoglevelNo between NVL (ll.loglevel_begin, -99999999) and NVL (ll.loglevel_end, 99999999);
  return sReturnLoglevelStr;
exception when others then -- Return default value
  return csErrorLoglevelName;
end getLogLevelStr;

-- ***************************************************************************************************
function getReference return varchar2 is

  sReference varchar2(4000);
  cursor cGetJobID( cnSessionId number ) is
    select jobid, ip, port
    from  stb$jobsession
    where oraclejobsessionid = cnSessionId;

begin

  if sExternalReference is not null then
    return sExternalReference;
  end if;
  /*nSessionId := stb$security.getCurrentSession;

  begin
    open cGetJobID( nSessionId );
    fetch cGetJobID
    into nJobId, sIp, nPort;
    close cGetJobID;
  exception when others then null;
  end;*/

  sReference := case
                  when nJobid is not null then 'JOBID ' || njobid || ' '
                end;

  begin
    case
      when Stb$object.getCurrentDocId is not null then
        sReference := sReference||
              'REPORTTYPEID ' || STB$OBJECT.getCurrentReportTypeId || ', ' ||
              'REPID ' || STB$OBJECT.getCurrentRepId || ', ' ||
              'DOCID ' || STB$OBJECT.getCurrentDocId ;
      when Stb$object.getCurrentRepId is not null then
        sReference := sReference||
              'REPORTTYPEID ' || STB$OBJECT.getCurrentReportTypeId || ', ' ||
              'REPID ' || STB$OBJECT.getCurrentRepId;
      when STB$OBJECT.getCurrentReportTypeId is not null then
        sReference := sReference||
              'REPORTTYPEID ' || STB$OBJECT.getCurrentReportTypeId;
      else
        sReference := sReference||
              case when STB$OBJECT.getCurrentNodeType is not null then
               'NODETYPE ' || STB$OBJECT.getCurrentNodeType || ', '
              end ||
              case when STB$OBJECT.getCurrentNodeId is not null then
               'NODEID ' || STB$OBJECT.getCurrentNodeId
              end;
    end case;
    return sReference;
  exception when others then null;
  end;

end getReference;

--******************************************************************************
procedure setLog ( csActionTaken    in varchar2,
                   csFunctionGroup  in varchar2,
                   csLogEntry       in varchar2,
                   csLoglevelName   in varchar2,
                   cnLoglevelNo     in number,
                   cclClob          in CLOB   default EMPTY_CLOB(),
                   cblBlob          in BLOB   default EMPTY_BLOB(),
                   csReference      in varchar2 default null,
                   csCallpackage    in CLOB default null )
is pragma autonomous_transaction;

  sCallpackage    CLOB;
  nCurrentSession number;
  sReference      varchar2(4000) := csReference;
  sIpField        isr$log.ip%type;

begin

  -- Callstack
  -- Sample output:    4  function TESTCALLSTACK | 18  function GETCALLSTACK | 25 function ISR$TRACE.GETLOGLEVEL
  --if cnLoglevelNo >= cnLoglevelNo_DEBUG then
      if csCallpackage is null then
        sCallpackage := replace(isr$trace.getcallstack, SYS_CONTEXT('USERENV', 'CURRENT_SCHEMA')||'.', '');
      else
        sCallpackage := csCallpackage;
      end if;
  /*else
    sCallpackage := NVL(TRIM(csCallpackage), 'no callpackage without debug');
  end if; */

  begin
    nCurrentSession := STB$SECURITY.getCurrentSession;
  exception when others then null;
  end;

  if sReference is null then
    sReference := getReference;
  end if;

  sIpField := case
          when sIp is not null then
            sIp || case when nPort is not null then ':' || nPort end
          else
            STB$SECURITY.getClientIP
         end;

  insert /*+ append */
        into ISR$LOG (entryid
                    , timestamp
                    , actiontaken
                    , logentry
                    , functiongroup
                    , reference
                    , ip
                    , oracleusername
                    , sessionid
                    , logclob
                    , logblob
                    , callpackage
                    , loglevelname
                    , loglevel)
  values (STB$LOG$SEQ.NEXTVAL
        , SYSTIMESTAMP
        , SUBSTR (csActionTaken, 0, 4000)
        , SUBSTR (csLogEntry, 0, 4000)
        , SUBSTR (csFunctionGroup, 0, 4000)
        , SUBSTR (sReference, 0, 4000)
        , sIpField
        , sUserName||' ('||STB$SECURITY.getCurrentUser||')'
        , nCurrentSession
        , cclClob
        , cblBlob
        , sCallpackage
        , csLoglevelName
        , cnLoglevelNo);
  commit;

 -- No exception handler here - decision made in code review [R-ISRC-11].
 -- setLog is the last instance for logging.
 -- If logging doesn't work here, it wont work anyway.
 -- Exceptions within this procedure must be handled from the calling procedure.

end setLog;

-- ***************************************************************************************************
procedure setExternalReference (csExternalReference isr$log.reference%type) is
begin
  sExternalReference := csExternalReference;
end setExternalReference;

-- ***************************************************************************************************
procedure setExternalLogLevel (csExternalLogLevel isr$log.logLevelName%type) is
begin
  sExternalLogLevel := csExternalLogLevel;
end setExternalLogLevel;

-- ***************************************************************************************************
procedure setLogForJava( csActionTaken    in varchar2,
                         csFunctionGroup  in varchar2,
                         csLogEntry       in varchar2,
                         csLoglevelName   in varchar2,
                         csReference      in varchar2,
                         csCallpackage    in CLOB default null,
                         cclClob          in CLOB   default EMPTY_CLOB(),
                         csLogLevel       in varchar2 default null,
                         cblBlob          in BLOB   default EMPTY_BLOB())
is
  nUserLoglevelNo     number ;
  nLogLevel   number;
begin
  if csLogLevel is not null then
    nUserLoglevelNo := getLogLevelNo(csLogLevel);
  end if;
  nLogLevel := getLoglevelNo(csLoglevelName);
  if nUserLoglevelNo >= nLogLevel then
    setLog(csActionTaken, csFunctionGroup, csLogEntry, csLoglevelName, nLogLevel, cclClob, cblBlob, csReference, csCallpackage);
  end if;
end setLogForJava;

--******************************************************************************
function checkWriteLog(csLoglevel in number)
  return boolean
is
  bReturnWritelog     boolean := false;    -- initialization to FALSE --> to be changed later
begin

  if  nSessionLogLevel is null
  and (    STB$JOB.getJobid is not null
        or STB$SECURITY.getCurrentUser is not null )  then
    nSessionLogLevel := getLogLevel;
    /*** use next lines for debugging, if required *** /
    nJobID           := STB$JOB.getJobid;  
    sCurrentUser     := STB$SECURITY.getCurrentUser;  
    setLog( csActionTaken   => 'CHECK TRACE',
            csFunctionGroup => 'ISR$TRACE.checkWriteLog',
            csLogEntry      => 'nSessionLogLevel: '||nSessionLogLevel||'='||getLoglevelStr(nSessionLogLevel)||',  bSessionLoglevelChangeAllowed: '
                                || case 
                                     when bSessionLoglevelChangeAllowed then 'TRUE'
                                     else 'FALSE'
                                   end
                                ||',  sCurrentUser: '||sCurrentUser||',   nJobID: '||nJobID,
            csLoglevelName  => getLoglevelStr(nSessionLogLevel),
            cnLoglevelNo    => nSessionLogLevel );  
    /*** END use next lines for debugging, if required ***/
  end if;

  -- redesigned, vs 09.May.2019 [ISRC-1111]
  -- for performance reasons, getLogLevel shall be used sparsely
  case
    when nSessionLogLevel >= csLoglevel then
      bReturnWritelog := true;

    when nSystemLogLevel  >= csLoglevel then
      bReturnWritelog := true;      

    when bSessionLoglevelChangeAllowed
    and  STB$SECURITY.getCurrentUser is not null
    and  getLogLevel() >= csLoglevel    then
      bReturnWritelog := true;
      
    when bSessionLoglevelChangeAllowed
    and  STB$JOB.getJobid is not null 
    and  getLogLevel() >= csLoglevel  then
      bReturnWritelog := true;
      
    else
      bReturnWritelog := false;
  end case;

  return bReturnWritelog;

end;

/* DEBUG */
--******************************************************************************
procedure debug ( csActionTaken   in varchar2 default null,
                  csLogEntry      in varchar2,
                  csFunctionGroup in varchar2,
                  cblBlob         in BLOB   default EMPTY_BLOB() )
is
  csLoglevelName constant char(5) := 'DEBUG';
begin
  if checkWriteLog(cnLoglevelNo_DEBUG) then
    setLog( csActionTaken   => csActionTaken,
            csFunctionGroup => csFunctionGroup,
            csLogEntry      => csLogEntry,
            csLoglevelName  => csLoglevelName,
            cnLoglevelNo    => cnLoglevelNo_DEBUG,
            cclClob         => EMPTY_CLOB(),
            cblBlob         => cblBlob );
  end if;
exception when others then
  setLog( 'LOG_ERROR', csFunctionGroup, 'CANNOT LOG cblBlob in PROCEDURE DEBUG - '||csLogEntry, csErrorLoglevelName, cnDefaultUserLogLevel, EMPTY_CLOB(), cblBlob );
end debug;

--******************************************************************************
procedure debug ( csActionTaken   in varchar2 default null,
                  csLogEntry      in varchar2,
                  csFunctionGroup in varchar2,
                  cclClob         in CLOB )
is
  csLoglevelName constant char(5) := 'DEBUG';
begin
  if checkWriteLog(cnLoglevelNo_DEBUG) then
    setLog( csActionTaken   => csActionTaken,
            csFunctionGroup => csFunctionGroup,
            csLogEntry      => csLogEntry,
            csLoglevelName  => csLoglevelName,
            cnLoglevelNo    => cnLoglevelNo_DEBUG,
            cclClob         => cclClob,
            cblBlob         => EMPTY_BLOB() );
  end if;
exception when others then
  setLog( 'LOG_ERROR', csFunctionGroup, 'CANNOT LOG cclClob in PROCEDURE DEBUG - '||csLogEntry, csErrorLoglevelName, cnDefaultUserLogLevel, cclClob, EMPTY_BLOB() );
end debug;

--******************************************************************************
procedure debug ( csActionTaken   in varchar2 default null,
                  csLogEntry      in varchar2,
                  csFunctionGroup in varchar2,
                  cxXml           in xmltype )
is
begin
  debug(csActionTaken, csLogEntry, csFunctionGroup, case when cxXml is null then cclDefaultNull else cxXml.getClobVal() end);
exception when others then
  setLog( 'LOG_ERROR', csFunctionGroup, 'CANNOT LOG cxXML in PROCEDURE DEBUG - '||csLogEntry, csErrorLoglevelName, cnDefaultUserLogLevel, EMPTY_CLOB(), EMPTY_BLOB() );
end debug;


--******************************************************************************
procedure debug ( csActionTaken   in varchar2 default null,
                  csLogEntry      in varchar2,
                  csFunctionGroup in varchar2,
                  caObjData       in anydata )
is
begin
  debug(csActionTaken, csLogEntry, csFunctionGroup, xmltype(caObjData).GetClobVal);
exception when others then
  setLog( 'LOG_ERROR' , csFunctionGroup,  'CANNOT LOG caObjData in PROCEDURE DEBUG - '||csLogEntry,  csErrorLoglevelName, cnDefaultUserLogLevel, EMPTY_CLOB(), EMPTY_BLOB() );
end debug;

--******************************************************************************
procedure debug ( csActionTaken   in varchar2 default null,
                  csLogEntry      in varchar2,
                  csFunctionGroup in varchar2,
                  clTreenodeList  in STB$TREENODELIST )
is
  clClob  clob;
begin
  if not clTreenodeList.exists(1) then
       clClob := xmltype(sys.anydata.convertCollection(STB$TREENODELIST())).getClobVal;
  else
       clClob := xmltype(sys.anydata.convertCollection(clTreenodeList)).getClobVal;
  end if;
  debug(csActionTaken, csLogEntry, csFunctionGroup, clClob);
exception when others then
  setLog( 'LOG_ERROR' , csFunctionGroup,  'CANNOT LOG clTreenodeList in PROCEDURE DEBUG - '||csLogEntry,  csErrorLoglevelName, cnDefaultUserLogLevel, EMPTY_CLOB(), EMPTY_BLOB() );
end debug;

--******************************************************************************
procedure debug ( csActionTaken    in varchar2 default null,
                  csLogEntry       in varchar2,
                  csFunctionGroup  in varchar2,
                  coTreenodeRecord in STB$TREENODE$RECORD )
is
  clClob  clob;
begin
  if coTreenodeRecord is null then
     clClob := xmltype(STB$TREENODE$RECORD()).getClobVal;
   else
     clClob := xmltype(coTreenodeRecord).getClobVal;
   end if;
  debug(csActionTaken, csLogEntry, csFunctionGroup, clClob);
exception when others then
  setLog( 'LOG_ERROR' , csFunctionGroup,  'CANNOT LOG coTreenodeRecord in PROCEDURE DEBUG - '||csLogEntry,  csErrorLoglevelName, cnDefaultUserLogLevel, EMPTY_CLOB(), EMPTY_BLOB() );
end debug;

--******************************************************************************
procedure debug ( csActionTaken    in varchar2 default null,
                  csLogEntry       in varchar2,
                  csFunctionGroup  in varchar2,
                  clMenuEntryList  in STB$MENUENTRY$LIST )
is
  clClob  clob;
begin
  if not clMenuEntryList.exists(1) then
     clClob := xmltype(sys.anydata.convertCollection(STB$MENUENTRY$LIST())).getClobVal;
   else
     clClob := xmltype(sys.anydata.convertCollection(clMenuEntryList)).getClobVal;
   end if;
  debug(csActionTaken, csLogEntry, csFunctionGroup, clClob);
exception when others then
  setLog( 'LOG_ERROR' , csFunctionGroup,  'CANNOT LOG clMenuEntryList in PROCEDURE DEBUG - '||csLogEntry,  csErrorLoglevelName, cnDefaultUserLogLevel, EMPTY_CLOB(), EMPTY_BLOB() );
end debug;

--******************************************************************************
procedure debug ( csActionTaken     in varchar2 default null,
                  csLogEntry        in varchar2,
                  csFunctionGroup   in varchar2,
                  coMenuEntryRecord in STB$MENUENTRY$RECORD )
is
  clClob  clob;
begin
  if coMenuEntryRecord is null then
     clClob := xmltype(STB$MENUENTRY$RECORD()).getClobVal;
   else
     clClob := xmltype(coMenuEntryRecord).getClobVal;
   end if;
  debug(csActionTaken, csLogEntry, csFunctionGroup, clClob);
exception when others then
  setLog( 'LOG_ERROR' , csFunctionGroup,  'CANNOT LOG coMenuEntryRecord in PROCEDURE DEBUG - '||csLogEntry,  csErrorLoglevelName, cnDefaultUserLogLevel, EMPTY_CLOB(), EMPTY_BLOB() );
end debug;

--******************************************************************************
procedure debug ( csActionTaken   in varchar2 default null,
                  csLogEntry      in varchar2,
                  csFunctionGroup in varchar2,
                  coErrorRecord   in STB$OERROR$RECORD )
is
  clClob  clob;
begin
  if coErrorRecord is null then
     clClob := xmltype(sys.anydata.convertObject(STB$OERROR$RECORD())).getClobVal;
   else
     clClob := xmltype(sys.anydata.convertObject(coErrorRecord)).getClobVal;
   end if;
  debug(csActionTaken, csLogEntry, csFunctionGroup, clClob);
exception when others then
  setLog( 'LOG_ERROR' , csFunctionGroup,  'CANNOT LOG coErrorRecord in PROCEDURE DEBUG - '||csLogEntry,  csErrorLoglevelName, cnDefaultUserLogLevel, EMPTY_CLOB(), EMPTY_BLOB() );
end debug;

--******************************************************************************
procedure debug ( csActionTaken    in varchar2   default null,
                  csLogEntry       in varchar2,
                  csFunctionGroup  in varchar2,
                  coPropertyList   in STB$PROPERTY$LIST )
is
  clClob  clob;
begin
  if not coPropertyList.exists(1) then
    clClob := xmltype(sys.anydata.convertCollection(STB$PROPERTY$LIST())).getClobVal;
  else
    clClob := xmltype(sys.anydata.convertCollection(coPropertyList)).getClobVal;
  end if;
  debug(csActionTaken, csLogEntry, csFunctionGroup, clClob);
exception when others then
  setLog( 'LOG_ERROR' , csFunctionGroup,  'CANNOT LOG coPropertyList in PROCEDURE DEBUG - '||csLogEntry,  csErrorLoglevelName, cnDefaultUserLogLevel, EMPTY_CLOB(), EMPTY_BLOB() );
end debug;

--******************************************************************************
procedure debug ( csActionTaken    in varchar2 default null,
                  csLogEntry       in varchar2,
                  csFunctionGroup  in varchar2,
                  coFormInfoRecord in ISR$FORMINFO$RECORD )
is
  clClob  clob;
begin
   if coFormInfoRecord is null then
     clClob := xmltype(sys.anydata.convertObject(STB$OERROR$RECORD())).getClobVal;
   else
     clClob := xmltype(sys.anydata.convertObject(coFormInfoRecord)).getClobVal;
   end if;
  debug(csActionTaken, csLogEntry, csFunctionGroup, clClob);
exception when others then
  setLog( 'LOG_ERROR' , csFunctionGroup,  'CANNOT LOG coFormInfoRecord in PROCEDURE DEBUG - '||csLogEntry,  csErrorLoglevelName, cnDefaultUserLogLevel, EMPTY_CLOB(), EMPTY_BLOB() );
end debug;

--******************************************************************************
procedure debug ( csActionTaken    in varchar2   default null,
                  csLogEntry       in varchar2,
                  csFunctionGroup  in varchar2,
                  coFormInfoList   in ISR$FORMINFO$LIST )
is
  clClob  clob;
begin
   if not coFormInfoList.exists(1) then
    clClob := xmltype(sys.anydata.convertCollection(ISR$FORMINFO$LIST())).getClobVal;
  else
    clClob := xmltype(sys.anydata.convertCollection(coFormInfoList)).getClobVal;
  end if;
  debug(csActionTaken, csLogEntry, csFunctionGroup, clClob);
exception when others then
  setLog( 'LOG_ERROR' , csFunctionGroup,  'CANNOT LOG coFormInfoList in PROCEDURE DEBUG - '||csLogEntry,  csErrorLoglevelName, cnDefaultUserLogLevel, EMPTY_CLOB(), EMPTY_BLOB() );
end debug;

--******************************************************************************
procedure debug ( csActionTaken    in varchar2   default null,
                  csLogEntry       in varchar2,
                  csFunctionGroup  in varchar2,
                  coSelectionList  in ISR$TLRSELECTION$LIST )
is
  clClob  clob;
begin
  if not coSelectionList.exists(1) then
    clClob := xmltype(sys.anydata.convertCollection(ISR$TLRSELECTION$LIST())).getClobVal;
  else
    clClob := xmltype(sys.anydata.convertCollection(coSelectionList)).getClobVal;
  end if;
  debug(csActionTaken, csLogEntry, csFunctionGroup, clClob);
exception when others then
  setLog( 'LOG_ERROR' , csFunctionGroup,  'CANNOT LOG coSelectionList in PROCEDURE DEBUG - '||csLogEntry,  csErrorLoglevelName, cnDefaultUserLogLevel, EMPTY_CLOB(), EMPTY_BLOB() );
end debug;

--******************************************************************************
procedure debug ( csActionTaken    in varchar2   default null,
                  csLogEntry       in varchar2,
                  csFunctionGroup  in varchar2,
                  coParamListRec   in ISR$PARAMLIST$REC )
is
  clClob  clob;
begin
   if coParamListRec is null then
    clClob := xmltype(sys.anydata.convertObject(ISR$PARAMLIST$REC())).getClobVal;
  else
    clClob := xmltype(sys.anydata.convertObject(coParamListRec)).getClobVal;
  end if;
  debug(csActionTaken, csLogEntry, csFunctionGroup, clClob);
exception when others then
  setLog( 'LOG_ERROR' , csFunctionGroup,  'CANNOT LOG coParamListRec in PROCEDURE DEBUG - '||csLogEntry,  csErrorLoglevelName, cnDefaultUserLogLevel, EMPTY_CLOB(), EMPTY_BLOB() );
end debug;

--******************************************************************************
procedure debug ( csActionTaken    in varchar2   default null,
                  csLogEntry       in varchar2,
                  csFunctionGroup  in varchar2,
                  coEntityList     in STB$ENTITY$LIST )
is
  clClob  clob;
begin
   if not coEntityList.exists(1) then
    clClob := xmltype(sys.anydata.convertCollection(STB$ENTITY$LIST())).getClobVal;
  else
    clClob := xmltype(sys.anydata.convertCollection(coEntityList)).getClobVal;
  end if;
  debug(csActionTaken, csLogEntry, csFunctionGroup, clClob);
exception when others then
  setLog( 'LOG_ERROR' , csFunctionGroup,  'CANNOT LOG coEntityList in PROCEDURE DEBUG - '||csLogEntry,  csErrorLoglevelName, cnDefaultUserLogLevel, EMPTY_CLOB(), EMPTY_BLOB() );
end debug;

--****************************************************************************** 
procedure debug ( csActionTaken    in varchar2   default null,  -- [ISRC-852]
                  csLogEntry       in varchar2,
                  csFunctionGroup  in varchar2,
                  coCritRec        in ISR$CRIT$REC )
is 
  clClob  clob;
begin 
  if coCritRec is null then 
    clClob := xmltype(sys.anydata.convertObject(ISR$CRIT$REC())).getClobVal; 
  else 
    clClob := xmltype(sys.anydata.convertObject(coCritRec)).getClobVal; 
  end if;
  debug(csActionTaken, csLogEntry, csFunctionGroup, clClob);
exception when others then 
  setLog( 'LOG_ERROR' , csFunctionGroup,  'CANNOT LOG coCritRec in PROCEDURE DEBUG - '||csLogEntry,  csErrorLoglevelName, cnDefaultUserLogLevel, EMPTY_CLOB(), EMPTY_BLOB() );
end debug;  


--****************************************************************************** 
procedure debug ( csActionTaken    in varchar2   default null,  -- [ISRC-908]
                  csLogEntry       in varchar2,
                  csFunctionGroup  in varchar2,
                  coParamRec       in ISR$PARAMETER$REC )
is 
  clClob  clob;
begin 
  if coParamRec is null then 
    clClob := xmltype(sys.anydata.convertObject(ISR$PARAMETER$REC(null,cast( null as varchar2 )))).getClobVal; 
  else 
    clClob := xmltype(sys.anydata.convertObject(coParamRec)).getClobVal; 
  end if;
  debug(csActionTaken, csLogEntry, csFunctionGroup, clClob);
exception when others then 
  setLog( 'LOG_ERROR' , csFunctionGroup,  'CANNOT LOG coParamRec in PROCEDURE DEBUG - '||csLogEntry,  csErrorLoglevelName, cnDefaultUserLogLevel, EMPTY_CLOB(), EMPTY_BLOB() );
end debug;  


/* DEBUG_ALL */
--******************************************************************************
procedure debug_all ( csActionTaken   in varchar2 default null,
                      csLogEntry      in varchar2,
                      csFunctionGroup in varchar2,
                      cblBlob         in BLOB   default EMPTY_BLOB() )
is
  csLoglevelName constant char(9) := 'DEBUG_ALL';
begin
  if checkWriteLog(cnLoglevelNo_DEBUG_ALL) then
    setLog( csActionTaken   => csActionTaken,
            csFunctionGroup => csFunctionGroup,
            csLogEntry      => csLogEntry,
            csLoglevelName  => csLoglevelName,
            cnLoglevelNo    => cnLoglevelNo_DEBUG_ALL,
            cclClob         => EMPTY_CLOB(),
            cblBlob         => cblBlob );
  end if;
exception when others then
  setLog( 'LOG_ERROR', csFunctionGroup, 'CANNOT LOG cblBlob in PROCEDURE DEBUG_ALL - '||csLogEntry, csErrorLoglevelName, cnDefaultUserLogLevel, EMPTY_CLOB(), cblBlob );
end debug_all;

--******************************************************************************
procedure debug_all ( csActionTaken   in varchar2 default null,
                      csLogEntry      in varchar2,
                      csFunctionGroup in varchar2,
                      cclClob         in CLOB )
is
  csLoglevelName constant char(9) := 'DEBUG_ALL';
begin
  if checkWriteLog(cnLoglevelNo_DEBUG_ALL) then
    setLog( csActionTaken   => csActionTaken,
            csFunctionGroup => csFunctionGroup,
            csLogEntry      => csLogEntry,
            csLoglevelName  => csLoglevelName,
            cnLoglevelNo    => cnLoglevelNo_DEBUG_ALL,
            cclClob         => cclClob,
            cblBlob         => EMPTY_BLOB() );
  end if;
exception when others then
  setLog( 'LOG_ERROR', csFunctionGroup, 'CANNOT LOG cclClob in PROCEDURE DEBUG_ALL - '||csLogEntry, csErrorLoglevelName, cnDefaultUserLogLevel, cclClob, EMPTY_BLOB() );
end debug_all;

--******************************************************************************
procedure debug_all ( csActionTaken   in varchar2 default null,
                      csLogEntry      in varchar2,
                      csFunctionGroup in varchar2,
                      cxXml           in xmltype )
is
begin
  debug_all(csActionTaken, csLogEntry, csFunctionGroup, case when cxXml is null then cclDefaultNull else cxXml.getClobVal() end);
exception when others then
  setLog( 'LOG_ERROR', csFunctionGroup, 'CANNOT LOG cxXML in PROCEDURE DEBUG_ALL - '||csLogEntry, csErrorLoglevelName, cnDefaultUserLogLevel, EMPTY_CLOB(), EMPTY_BLOB() );
end debug_all;

--******************************************************************************
procedure debug_all ( csActionTaken   in varchar2 default null,
                      csLogEntry      in varchar2,
                      csFunctionGroup in varchar2,
                      caObjData       in anydata )
is
begin
  debug_all(csActionTaken, csLogEntry, csFunctionGroup, xmltype(caObjData).GetClobVal);
exception when others then
  setLog( 'LOG_ERROR' , csFunctionGroup,  'CANNOT LOG caObjData in PROCEDURE DEBUG_ALL - '||csLogEntry,  csErrorLoglevelName, cnDefaultUserLogLevel, EMPTY_CLOB(), EMPTY_BLOB() );
end debug_all;

/* ERROR */
--******************************************************************************
procedure error ( csActionTaken   in varchar2 default null,
                  csLogEntry      in varchar2,
                  csFunctionGroup in varchar2,
                  cblBlob         in BLOB   default EMPTY_BLOB() )
is
  csLoglevelName constant char(5) := 'ERROR';
begin
 -- if checkWriteLog(cnLoglevelNo_ERROR) then
    setLog( csActionTaken   => csActionTaken,
            csFunctionGroup => csFunctionGroup,
            csLogEntry      => csLogEntry,
            csLoglevelName  => csLoglevelName,
            cnLoglevelNo    => cnLoglevelNo_ERROR,
            cclClob         => EMPTY_CLOB(),
            cblBlob         => cblBlob );
--end if;
exception when others then
  setLog( 'LOG_ERROR', csFunctionGroup, 'CANNOT LOG cblBlob in PROCEDURE ERROR - '||csLogEntry, csErrorLoglevelName, cnDefaultUserLogLevel, EMPTY_CLOB(), cblBlob );
end ERROR;

--******************************************************************************
procedure error ( csActionTaken   in varchar2 default null,
                  csLogEntry      in varchar2,
                  csFunctionGroup in varchar2,
                  cclClob         in CLOB )
is
  csLoglevelName constant char(5) := 'ERROR';
begin
 -- if checkWriteLog(cnLoglevelNo_ERROR) then
    setLog( csActionTaken   => csActionTaken,
            csFunctionGroup => csFunctionGroup,
            csLogEntry      => csLogEntry,
            csLoglevelName  => csLoglevelName,
            cnLoglevelNo    => cnLoglevelNo_ERROR,
            cclClob         => cclClob,
            cblBlob         => EMPTY_BLOB() );
 -- end if;
exception when others then
  setLog( 'LOG_ERROR', csFunctionGroup, 'CANNOT LOG cclClob in PROCEDURE ERROR - '||csLogEntry, csErrorLoglevelName, cnDefaultUserLogLevel, cclClob, EMPTY_BLOB() );
end error;

--******************************************************************************
procedure error ( csActionTaken   in varchar2 default null,
                  csLogEntry      in varchar2,
                  csFunctionGroup in varchar2,
                  cxXml           in xmltype )
is
begin
  error(csActionTaken, csLogEntry, csFunctionGroup, case when cxXml is null then cclDefaultNull else cxXml.getClobVal() end);
exception when others then
  setLog( 'LOG_ERROR', csFunctionGroup, 'CANNOT LOG cxXML in PROCEDURE ERROR - '||csLogEntry, csErrorLoglevelName, cnDefaultUserLogLevel, EMPTY_CLOB(), EMPTY_BLOB() );
end error;

--******************************************************************************
procedure error ( csActionTaken   in varchar2 default null,
                  csLogEntry      in varchar2,
                  csFunctionGroup in varchar2,
                  coErrorRecord   in STB$OERROR$RECORD )
is
  clClob  clob;
begin
   if coErrorRecord is null then
       clClob := xmltype(sys.anydata.convertObject(STB$OERROR$RECORD())).getClobVal;
     else
       clClob := xmltype(sys.anydata.convertObject(coErrorRecord)).getClobVal;
     end if;
  error(csActionTaken, csLogEntry, csFunctionGroup, clClob);
exception when others then
  setLog( 'LOG_ERROR', csFunctionGroup, 'CANNOT LOG coErrorRecord in PROCEDURE ERROR - '||csLogEntry, csErrorLoglevelName, cnDefaultUserLogLevel, EMPTY_CLOB(), EMPTY_BLOB() );
end error;

/* FATAL */
--******************************************************************************
procedure fatal ( csActionTaken   in varchar2 default null,
                  csLogEntry      in varchar2,
                  csFunctionGroup in varchar2,
                  cblBlob         in BLOB   default EMPTY_BLOB() )
is
  csLoglevelName constant char(5) := 'FATAL';
begin
  if checkWriteLog(cnLoglevelNo_FATAL) then
    setLog( csActionTaken   => csActionTaken,
            csFunctionGroup => csFunctionGroup,
            csLogEntry      => csLogEntry,
            csLoglevelName  => csLoglevelName,
            cnLoglevelNo    => cnLoglevelNo_FATAL,
            cclClob         => EMPTY_CLOB(),
            cblBlob         => cblBlob );
  end if;
exception when others then
  setLog( 'LOG_ERROR', csFunctionGroup, 'CANNOT LOG cblBlob in PROCEDURE FATAL - '||csLogEntry, csErrorLoglevelName, cnDefaultUserLogLevel, EMPTY_CLOB(), cblBlob );
end fatal;

--******************************************************************************
procedure fatal ( csActionTaken   in varchar2 default null,
                  csLogEntry      in varchar2,
                  csFunctionGroup in varchar2,
                  cclClob         in CLOB )
is
 csLoglevelName constant char(5) := 'FATAL';
 nCallLoglevelNo     number;
 nUserLoglevelNo     number;
begin
  if checkWriteLog(cnLoglevelNo_FATAL) then
    setLog( csActionTaken   => csActionTaken,
            csFunctionGroup => csFunctionGroup,
            csLogEntry      => csLogEntry,
            csLoglevelName  => csLoglevelName,
            cnLoglevelNo    => cnLoglevelNo_FATAL,
            cclClob         => cclClob,
            cblBlob         => EMPTY_BLOB() );
  end if;
exception when others then
  setLog( 'LOG_ERROR', csFunctionGroup, 'CANNOT LOG cclClob in PROCEDURE FATAL - '||csLogEntry, csErrorLoglevelName, cnDefaultUserLogLevel, cclClob, EMPTY_BLOB() );
end fatal;

--******************************************************************************
procedure fatal ( csActionTaken   in varchar2 default null,
                  csLogEntry      in varchar2,
                  csFunctionGroup in varchar2,
                  cxXml           in xmltype )
is
begin
  fatal(csActionTaken, csLogEntry, csFunctionGroup, case when cxXml is null then cclDefaultNull else cxXml.getClobVal() end);
exception when others then
  setLog( 'LOG_ERROR', csFunctionGroup, 'CANNOT LOG cxXML in PROCEDURE FATAL - '||csLogEntry, csErrorLoglevelName, cnDefaultUserLogLevel, EMPTY_CLOB(), EMPTY_BLOB() );
end fatal;

/* INFO */
--******************************************************************************
procedure info ( csActionTaken    in varchar2 default null,
                 csLogEntry       in varchar2,
                 csFunctionGroup  in varchar2,
                 cclClob          in CLOB default EMPTY_CLOB(),
                 cblBlob          in BLOB default EMPTY_BLOB() )
is
  csLoglevelName constant char(4) := 'INFO';
begin
  if checkWriteLog(cnLoglevelNo_INFO) then
    setLog( csActionTaken   => csActionTaken,
            csFunctionGroup => csFunctionGroup,
            csLogEntry      => csLogEntry,
            csLoglevelName  => csLoglevelName,
            cnLoglevelNo    => cnLoglevelNo_INFO,
            cclClob         => cclClob,
            cblBlob         => cblBlob );
  end if;
exception when others then
  setLog( 'LOG_ERROR', csFunctionGroup, 'CANNOT LOG cblBlob in PROCEDURE INFO - '||csLogEntry, csErrorLoglevelName, cnDefaultUserLogLevel, cclClob, cblBlob );
end info;
--******************************************************************************
procedure info ( csActionTaken    in varchar2 default null,
                 csLogEntry       in varchar2,
                 csFunctionGroup  in varchar2,
                 cxXml            in xmltype )
is
begin
  info(csActionTaken, csLogEntry, csFunctionGroup, case when cxXml is null then cclDefaultNull else cxXml.getClobVal() end);
exception when others then
  setLog( 'LOG_ERROR', csFunctionGroup, 'CANNOT LOG cxXML in PROCEDURE INFO - '||csLogEntry, csErrorLoglevelName, cnDefaultUserLogLevel, EMPTY_CLOB(), EMPTY_BLOB() );
end info;

/* INFO_ALL */
--******************************************************************************
procedure info_all ( csActionTaken    in varchar2 default null,
                     csLogEntry       in varchar2,
                     csFunctionGroup  in varchar2,
                     cclClob          in CLOB   default EMPTY_CLOB(),
                     cblBlob          in BLOB   default EMPTY_BLOB())
is
  csLoglevelName constant char(8) := 'INFO_ALL';
begin
  if checkWriteLog(cnLoglevelNo_INFO_ALL) then
    setLog( csActionTaken   => csActionTaken,
            csFunctionGroup => csFunctionGroup,
            csLogEntry      => csLogEntry,
            csLoglevelName  => csLoglevelName,
            cnLoglevelNo    => cnLoglevelNo_INFO_ALL,
            cclClob         => cclClob,
            cblBlob         => cblBlob );
  end if;
exception when others then
  setLog( 'LOG_ERROR', csFunctionGroup, 'CANNOT LOG in PROCEDURE INFO_ALL - '||csLogEntry, csErrorLoglevelName, cnDefaultUserLogLevel,cclClob, cblBlob );
end info_all;

/* STAT */
--******************************************************************************
procedure stat ( csActionTaken    in varchar2 default null,
                 csLogEntry       in varchar2,
                 csFunctionGroup  in varchar2,
                 cclClob          in CLOB default EMPTY_CLOB(),
                 cblBlob          in BLOB default EMPTY_BLOB() )
is
  csLoglevelName  constant char(9) := 'STATISTIC';
begin
  if checkWriteLog(cnLoglevelNo_STATISTIC) then
    setLog( csActionTaken   => csActionTaken,
            csFunctionGroup => csFunctionGroup,
            csLogEntry      => csLogEntry,
            csLoglevelName  => csLoglevelName,
            cnLoglevelNo    => cnLoglevelNo_STATISTIC,
            cclClob         => cclClob,
            cblBlob         => cblBlob  );
  end if;
exception when others then
  setLog( 'LOG_ERROR', csFunctionGroup, 'CANNOT LOG in PROCEDURE STAT - '||csLogEntry, csErrorLoglevelName, cnDefaultUserLogLevel, cclClob, cblBlob );
end stat;

--******************************************************************************
procedure stat ( csActionTaken   in varchar2 default null,
                 csFunctionGroup in varchar2,
                 cclClob         in CLOB default EMPTY_CLOB(),
                 cblBlob         in BLOB default EMPTY_BLOB() )
is
begin
  stat(csActionTaken, null, csFunctionGroup, cclClob, cblBlob);
end stat;

--******************************************************************************
procedure stat ( csActionTaken   in varchar2 default null,
                 csLogEntry      in varchar2,
                 csFunctionGroup in varchar2,
                 cxXml           in xmltype )
is
  csLoglevelName  constant char(9) := 'STATISTIC';
  nCallLoglevelNo          number;
  nUserLoglevelNo          number;
  clClob                   CLOB;
begin
  stat(csActionTaken, csLogEntry, csFunctionGroup, case when cxXml is null then cclDefaultNull else cxXml.getClobVal() end);
exception when others then
  setLog( 'LOG_ERROR', csFunctionGroup, 'CANNOT LOG cxXML in PROCEDURE STAT - '||csLogEntry, csErrorLoglevelName, cnDefaultUserLogLevel, EMPTY_CLOB(), EMPTY_BLOB() );
end stat;

--******************************************************************************
procedure stat ( csActionTaken   in varchar2 default null,
                 csLogEntry      in varchar2,
                 csFunctionGroup in varchar2,
                 caObjData       in anydata )
is
  clClob clob := EMPTY_CLOB();
begin
  if caObjData is not null then
    clClob := xmltype(caObjData).GetClobVal;
  end if;
  stat(csActionTaken, csLogEntry, csFunctionGroup, clClob);
exception when others then
  setLog( 'LOG_ERROR' , csFunctionGroup,  'CANNOT LOG caObjData in PROCEDURE STAT - '||csLogEntry,  csErrorLoglevelName, cnDefaultUserLogLevel, EMPTY_CLOB(), EMPTY_BLOB() );
end stat;

/* WARN */
--******************************************************************************
procedure warn ( csActionTaken   in varchar2 default null,
                 csLogEntry      in varchar2,
                 csFunctionGroup in varchar2,
                 cblBlob         in BLOB     default EMPTY_BLOB() )
is
  csLoglevelName  constant char(4) := 'WARN';
begin
  if checkWriteLog(cnLoglevelNo_WARN) then
    setLog( csActionTaken   => csActionTaken,
            csFunctionGroup => csFunctionGroup,
            csLogEntry      => csLogEntry,
            csLoglevelName  => csLoglevelName,
            cnLoglevelNo    => cnLoglevelNo_WARN,
            cclClob         => EMPTY_CLOB(),
            cblBlob         => cblBlob );
  end if;
exception when others then
  setLog( 'LOG_ERROR', csFunctionGroup, 'CANNOT LOG cblBlob in PROCEDURE WARN - '||csLogEntry, csErrorLoglevelName, cnDefaultUserLogLevel, EMPTY_CLOB(), cblBlob );
end warn;

--******************************************************************************
procedure warn ( csActionTaken   in varchar2 default null,
                 csLogEntry      in varchar2,
                 csFunctionGroup in varchar2,
                 cclClob         in CLOB )
is
  csLoglevelName  constant char(4) := 'WARN';
begin
  if checkWriteLog(cnLoglevelNo_WARN) then
    setLog( csActionTaken   => csActionTaken,
            csFunctionGroup => csFunctionGroup,
            csLogEntry      => csLogEntry,
            csLoglevelName  => csLoglevelName,
            cnLoglevelNo    => cnLoglevelNo_WARN,
            cclClob         => cclClob,
            cblBlob         => EMPTY_BLOB() );
  end if;
exception when others then
  setLog( 'LOG_ERROR', csFunctionGroup, 'CANNOT LOG cclClob in PROCEDURE WARN - '||csLogEntry, csErrorLoglevelName, cnDefaultUserLogLevel, cclClob, EMPTY_BLOB() );
end warn;

--******************************************************************************
procedure warn ( csActionTaken   in varchar2 default null,
                 csLogEntry      in varchar2,
                 csFunctionGroup in varchar2,
                 cxXml           in xmltype )
is
begin
  warn(csActionTaken, csLogEntry, csFunctionGroup, case when cxXml is null then cclDefaultNull else  cxXml.getClobVal() end);
exception when others then
  setLog( 'LOG_ERROR', csFunctionGroup, 'CANNOT LOG cxXML in PROCEDURE WARN - '||csLogEntry, csErrorLoglevelName, cnDefaultUserLogLevel, EMPTY_CLOB(), EMPTY_BLOB() );
end warn;

--******************************************************************************
procedure warn ( csActionTaken   in varchar2 default null,
                 csLogEntry      in varchar2,
                 csFunctionGroup in varchar2,
                 coErrorRecord   in STB$OERROR$RECORD )
is
  clClob  clob;
begin
  if coErrorRecord is null then
      clClob := xmltype(sys.anydata.convertObject(STB$OERROR$RECORD())).getClobVal;
    else
      clClob := xmltype(sys.anydata.convertObject(coErrorRecord)).getClobVal;
    end if;
  warn(csActionTaken, csLogEntry, csFunctionGroup, clClob);
exception when others then
  setLog( 'LOG_ERROR', csFunctionGroup, 'CANNOT LOG coErrorRecord in PROCEDURE WARN - '||csLogEntry, csErrorLoglevelName, cnDefaultUserLogLevel, EMPTY_CLOB(), EMPTY_BLOB() );
end warn;

--**************************************************************************************************************************
procedure setSessionLoglevel   -- [ISRC-111], [ISRC-1108]
is
begin
  nSessionLogLevel := getLogLevel;
    /*** use next lines for debugging, if required *** /
    nJobID           := STB$JOB.getJobid;  
    sCurrentUser     := STB$SECURITY.getCurrentUser;  
    setLog( csActionTaken   => 'CHECK TRACE',
            csFunctionGroup => 'ISR$TRACE.setSessionLoglevel',
            csLogEntry      => 'nSessionLogLevel: '||nSessionLogLevel||'='||getLoglevelStr(nSessionLogLevel)||',  bSessionLoglevelChangeAllowed: '
                                || case 
                                     when bSessionLoglevelChangeAllowed then 'TRUE'
                                     else 'FALSE'
                                   end
                                ||',  sCurrentUser: '||sCurrentUser||',   nJobID: '||nJobID,
            csLoglevelName  => getLoglevelStr(nSessionLogLevel),
            cnLoglevelNo    => nSessionLogLevel );  
    /*** END use next lines for debugging, if required ***/
end;  

--******************************************************************************
procedure setSessionInfo is
begin
  sUserName := STB$SECURITY.getOracleUserName (STB$SECURITY.getCurrentUser);
  nSessionId := stb$security.getCurrentSession;
  
  open cGetIpAndPort( nSessionId );
  fetch cGetIpAndPort into nJobId, sIp, nPort;
  close cGetIpAndPort;    
  
end setSessionInfo;

--******************************************************************************
begin
  -- initialize system loglevel
  nSystemLoglevel := getSystemLoglevel;

  -- check, if a change of session loglevel is allowed
  bSessionLoglevelChangeAllowed := false;   -- initialize here, to be eventually replaced in the next lines
  for rec in ( select * from stb$systemparameter where parametername = 'AllowSessionLoglevelChange' )    -- do not use STB$UTIL.getSystemParameter
  loop
    if rec.parametervalue = 'T' then
      bSessionLoglevelChangeAllowed := true;
    end if;
  end loop;
  
  nSessionId := stb$security.getCurrentSession;
  
  nJobId := STB$JOB.getJobid;

  -- initialize session and session loglevel
  oErrorObj := isr$util.initializeSession;  -- [ISRC-1108]
  
  /*** use next lines for debugging, if required *** /
  nJobID           := STB$JOB.getJobid;  
  sCurrentUser     := STB$SECURITY.getCurrentUser;  
  setLog( csActionTaken   => 'CHECK TRACE',
          csFunctionGroup => 'ISR$TRACE.main',
          csLogEntry      => 'nSessionLogLevel: '||nSessionLogLevel||'='||getLoglevelStr(nSessionLogLevel)||',  bSessionLoglevelChangeAllowed: '
                              || case 
                                   when bSessionLoglevelChangeAllowed then 'TRUE'
                                   else 'FALSE'
                                 end
                              ||',  sCurrentUser: '||sCurrentUser||',   nJobID: '||nJobID,
          csLoglevelName  => getLoglevelStr(nSessionLogLevel),
          cnLoglevelNo    => nSessionLogLevel );  
  /*** END use next lines for debugging, if required ***/  
  
--******************************************************************************

end ISR$trace;