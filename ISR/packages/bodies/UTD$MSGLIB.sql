CREATE OR REPLACE PACKAGE BODY UTD$MSGLIB is

  csDefaultNullString constant char(6) := '(null)';
  sAutoSetting varchar2(10);
  
--******************************************************************************
function GetMsg ( csKey     varchar2, 
                  cnLang    integer, 
                  csP1      varchar2  default null,
                  csP2      varchar2  default null,
                  csP3      varchar2  default null,
                  csP4      varchar2  default null,
                  csP5      varchar2  default null ) 
  return varchar2 
  result_cache
is
  pragma AUTONOMOUS_TRANSACTION;
  sCurrentName   constant varchar2(4000) := $$PLSQL_UNIT||'.getMsg';
 
  sMessage      utd$msg.msgText%type;
  sUsed         utd$msg.used%type;
  splugin       utd$msg.plugin%type;
  sP1 varchar2(4000);
  sKey   utd$msg.msgKey%type;
  nKeyLength   number := 100;
  
  cursor cGetTranslation is
    select msgtext, used, plugin
      from utd$msg
     where msgkey = csKey
       AND lang = cnLang;
begin
  open cGetTranslation;
  fetch cGetTranslation into sMessage, sUsed, sPlugin;
    
  sKey  :=  substr(csKey, 0, nKeyLength);
  if (     cGetTranslation%notfound 
       and cnLang != 999 
       and trim(sKey) is not null 
       --and nvl(STB$UTIL.getSystemParameter('AUTO_INSERT_MESSAGE'),'T') = 'T'   ) -- vs 08.Mar.2016 [ISRC-295] 
       and sAutoSetting = 'T')  -- hr 3.Jun 2019 replaced for [ISRC-1033]
  then  
    isr$trace.info('key', sKey, sCurrentName);
    sMessage := case when sKey = UPPER(sKey) then initcap(replace(replace(replace(replace(replace(replace(replace(sKey, 'CAN_'), 'AS$'), 'WT$'), 'SB$'), '$DESC'), '$MASK'), '_', ' ')) else null end; 
   
    merge into utd$msg b
    using (select langid lang, sKey msgkey, sMessage msgtext, 'T' used, sPlugin plugin
             from utd$language
            where langid != 999) e
    on (b.lang = e.lang
    and b.msgkey = e.msgkey)
    when not matched then
       insert (b.lang
             , b.msgkey
             , b.msgtext
             , b.used
             , b.plugin)
       values (e.lang
             , e.msgkey
             , e.msgtext
             , e.used
             , e.plugin);
    commit;
  elsif sUsed = 'F' then
    update utd$msg set used = 'T' where msgkey = sKey;
    commit;
  end if;
  close cGetTranslation;

  -- substitute replacement parameters
  if ( csP1 is not null or csP2 is not null or csP3 is not null or csP4 is not null or csP5 is not null ) then
    sMessage := replace(replace(replace(replace(replace(sMessage,'%1%',csP1),'%2%',csP2),'%3%',csP3),'%4%',csP4),'%5%',csP5);
  end if;
  -- eliminate empty replacement parameters
  sMessage := replace(replace(replace(replace(replace(sMessage,'%1%',null),'%2%',null),'%3%',null),'%4%',null),'%5%',null);
  
  return case when sMessage is null then sKey else sMessage end;
end GetMsg;

begin  
  sAutoSetting := nvl(STB$UTIL.getSystemParameter('AUTO_INSERT_MESSAGE'),'T');
end UTD$MSGLIB;