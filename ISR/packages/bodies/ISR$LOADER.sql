CREATE OR REPLACE PACKAGE BODY ISR$LOADER
is
--******************************************************************************
FUNCTION ping(sHost IN VARCHAR2, sPort IN VARCHAR2, sContext IN VARCHAR2, sNamespace IN VARCHAR2, nTimeout IN NUMBER) RETURN VARCHAR2
AS LANGUAGE JAVA
NAME 'com.uptodata.isr.loader.webservice.client.LoaderWebserviceClient.ping(java.lang.String, java.lang.String, java.lang.String, java.lang.String, int) return java.lang.String';

--******************************************************************************
FUNCTION sendJobList(sHost IN VARCHAR2, sPort IN VARCHAR2, sContext IN VARCHAR2, sNamespace IN VARCHAR2, blJobList IN BLOB, nTimeout IN NUMBER) RETURN VARCHAR2
AS LANGUAGE JAVA
NAME 'com.uptodata.isr.loader.webservice.client.LoaderWebserviceClient.sendJobList(java.lang.String, java.lang.String, java.lang.String, java.lang.String, java.sql.Blob, int) return java.lang.String';

--******************************************************************************
/*FUNCTION terminateJob(sHost in VARCHAR2, sPort IN VARCHAR2, sContext IN VARCHAR2, sNamespace IN VARCHAR2, nJobID IN NUMBER) RETURN VARCHAR2
AS LANGUAGE JAVA
NAME 'com.uptodata.isr.loader.webservice.client.LoaderWebserviceClient.terminateJob(java.lang.String, java.lang.String, java.lang.String, java.lang.String, int) return java.lang.String';*/

--******************************************************************************
FUNCTION showJobError(sHost IN VARCHAR2, sPort IN VARCHAR2, sContext IN VARCHAR2, sNamespace IN VARCHAR2, nJobID IN NUMBER, sErrorMessage in VARCHAR2, nTimeout IN NUMBER) RETURN VARCHAR2
AS LANGUAGE JAVA
NAME 'com.uptodata.isr.loader.webservice.client.LoaderWebserviceClient.showJobError(java.lang.String, java.lang.String, java.lang.String, java.lang.String, int, java.lang.String, int) return java.lang.String';

--******************************************************************************
FUNCTION showJobProgress(sHost IN VARCHAR2, sPort IN VARCHAR2, sContext IN VARCHAR2, sNamespace IN VARCHAR2, nJobID IN NUMBER, sText in VARCHAR2, nProgress in NUMBER, nTimeout IN NUMBER) RETURN VARCHAR2
AS LANGUAGE JAVA
NAME 'com.uptodata.isr.loader.webservice.client.LoaderWebserviceClient.showJobProgress(java.lang.String, java.lang.String, java.lang.String, java.lang.String, int, java.lang.String, int, int) return java.lang.String';

--******************************************************************************
FUNCTION startApp(sHost IN VARCHAR2, sPort IN VARCHAR2, sContext IN VARCHAR2, sNamespace IN VARCHAR2, nJobID IN NUMBER, nTimeout IN NUMBER) RETURN CLOB
AS LANGUAGE JAVA
NAME 'com.uptodata.isr.loader.webservice.client.LoaderWebserviceClient.startApp(java.lang.String, java.lang.String, java.lang.String, java.lang.String, int, int) return java.sql.Clob';

--******************************************************************************
FUNCTION loadFileToLoader(sHost in VARCHAR2, sPort IN VARCHAR2, sContext IN VARCHAR2, sNamespace IN VARCHAR2, nJobID IN NUMBER,  sFileName in VARCHAR2, sConfigName in VARCHAR2, sDocType in VARCHAR2,sLogLevel in varchar2, sLogReference in varchar2, nTimeout IN NUMBER) RETURN CLOB
AS LANGUAGE JAVA
NAME 'com.uptodata.isr.loader.webservice.client.LoaderWebserviceClient.loadFileToLoader(java.lang.String, java.lang.String, java.lang.String, java.lang.String, int, java.lang.String, java.lang.String, java.lang.String, java.lang.String, java.lang.String, int) return java.sql.Clob';

--******************************************************************************
FUNCTION loadFileFromLoader(sHost IN VARCHAR2, sPort IN VARCHAR2, sContext IN VARCHAR2, sNamespace IN VARCHAR2, nJobID IN NUMBER, sFileName in VARCHAR2, sConfigName in VARCHAR2, sDocType in VARCHAR2,sLogLevel in varchar2, sLogReference in varchar2, nTimeout IN NUMBER) RETURN VARCHAR2
AS LANGUAGE JAVA
NAME 'com.uptodata.isr.loader.webservice.client.LoaderWebserviceClient.loadFileFromLoader(java.lang.String, java.lang.String, java.lang.String, java.lang.String, int, java.lang.String, java.lang.String, java.lang.String, java.lang.String, java.lang.String, int) return java.lang.String';

--******************************************************************************
FUNCTION loadLoaderDir(sHost IN VARCHAR2, sPort IN VARCHAR2, sContext IN VARCHAR2, sNamespace IN VARCHAR2, nJobID IN NUMBER, sConfigName in VARCHAR2, sLogLevel in varchar2, sLogReference in varchar2, nTimeout IN NUMBER) RETURN VARCHAR2
AS LANGUAGE JAVA
NAME 'com.uptodata.isr.loader.webservice.client.LoaderWebserviceClient.loadLoaderDir(java.lang.String, java.lang.String, java.lang.String, java.lang.String, int, java.lang.String, java.lang.String, java.lang.String, int) return java.lang.String';

--******************************************************************************
FUNCTION getFilesNamesInDirectory(sHost IN VARCHAR2, sPort IN VARCHAR2, sContext IN VARCHAR2, sNamespace IN VARCHAR2, nJobID IN NUMBER, nTimeout IN NUMBER) RETURN VARCHAR2
AS LANGUAGE JAVA
NAME 'com.uptodata.isr.loader.webservice.client.LoaderWebserviceClient.getFilesNamesInDirectory(java.lang.String, java.lang.String, java.lang.String, java.lang.String, int, int) return java.lang.String';

--******************************************************************************
FUNCTION updateTree(sHost in VARCHAR2, sPort IN VARCHAR2, sContext IN VARCHAR2, sNamespace IN VARCHAR2, nTimeout IN NUMBER, sNodeType IN VARCHAR2, sNodeId IN VARCHAR2, sSide IN VARCHAR2) RETURN VARCHAR2
AS LANGUAGE JAVA
NAME 'com.uptodata.isr.loader.webservice.client.LoaderWebserviceClient.updateTree(java.lang.String, java.lang.String, java.lang.String, java.lang.String, int, java.lang.String, java.lang.String, java.lang.String) return java.lang.String';

--******************************************************************************
FUNCTION updateRibbon(sHost in VARCHAR2, sPort IN VARCHAR2, sContext IN VARCHAR2, sNamespace IN VARCHAR2, nTimeout IN NUMBER) RETURN VARCHAR2
AS LANGUAGE JAVA
NAME 'com.uptodata.isr.loader.webservice.client.LoaderWebserviceClient.updateRibbon(java.lang.String, java.lang.String, java.lang.String, java.lang.String, int) return java.lang.String';

--******************************************************************************
FUNCTION sendErrorObject(sHost IN VARCHAR2, sPort IN VARCHAR2, sContext IN VARCHAR2, sNamespace IN VARCHAR2, sErrorMessage in CLOB, nJobID IN NUMBER, nTimeout IN NUMBER) RETURN CLOB
AS LANGUAGE JAVA
NAME 'com.uptodata.isr.loader.webservice.client.LoaderWebserviceClient.sendErrorObject(java.lang.String, java.lang.String, java.lang.String,  java.lang.String, java.sql.Clob, int, int) return java.sql.Clob';
--******************************************************************************

function sendDbToken(sHost IN VARCHAR2, sPort IN VARCHAR2, sContext IN VARCHAR2, sNamespace IN VARCHAR2, blFile1 IN BLOB, blFile2 IN BLOB, nTimeout IN NUMBER) RETURN VARCHAR2
as language java
name 'com.uptodata.isr.loader.webservice.client.LoaderWebserviceClient.sendDbToken(java.lang.String, java.lang.String, java.lang.String,  java.lang.String, java.sql.Blob, java.sql.Blob, int) return java.lang.String';

--******************************************************************************
FUNCTION sendJobList(sHost in VARCHAR2, sPort in VARCHAR2) RETURN VARCHAR2
IS
  sCurrentName    constant varchar2(100) := $$PLSQL_UNIT||'.sendJobList';  
  sResult         VARCHAR2(4000);
  oErrorObj       STB$OERROR$RECORD := STB$OERROR$RECORD();
  olMenuEntryList STB$MENUENTRY$LIST;
  olNodeList      STB$TREENODELIST;
  l_refcursor     SYS_REFCURSOR;
  clXML           CLOB;
  queryCtx        DBMS_XMLQUERY.ctxType;
  xXml            XMLTYPE;
  
  cursor cGetNodetype is
  select Utd$msglib.GetMsg( description, Stb$security.GetCurrentLanguage ) sDisplay, 
  handledby sPackageType, 
  nodetype sNodetype
  from stb$contextmenu where token='CAN_DISPLAY_USER_JOBS';
  
  olDisplayNodes    STB$TREENODELIST;
  
BEGIN
  isr$trace.stat('begin', 'begin', sCurrentName);

  oErrorObj := STB$GAL.getMenu('CAN_DISPLAY_USER_JOBS', olMenuEntryList );
 
  olDisplayNodes := STB$TREENODELIST();
  olDisplayNodes.extend();
  olDisplayNodes(1) := STB$TREENODE$RECORD();
  open cGetNodetype;
  fetch cGetNodetype 
  into olDisplayNodes(1).sDisplay, olDisplayNodes(1).sPackageType, olDisplayNodes(1).sNodeType;
  close cGetNodetype;
  olDisplayNodes(1).tloValueList := ISR$VALUE$LIST();
     
  STB$OBJECT.setCurrentNode(olDisplayNodes(1));
  isr$trace.debug('status olDisplayNodes.count','olChildNodes.count(): '||olDisplayNodes.count(), sCurrentName);
       
  oErrorObj := STB$GAL.callFunction(olMenuEntryList(1), olNodeList);
  
  OPEN l_refcursor FOR SELECT * from table(olNodeList) t;  

  queryCtx := DBMS_XMLGEN.NEWCONTEXT(l_refcursor);
  DBMS_XMLGEN.setNullHandling (queryCtx, DBMS_XMLGEN.EMPTY_TAG);
  clXML := DBMS_XMLGEN.GetXML (queryCtx);
  DBMS_XMLGEN.closeContext (queryCtx);
  
  xXml := ISR$XML.clobToXml(clXML);
  
  ISR$XML.setAttribute(xXml, '/*', 'SCHEMA', STB$UTIL.getSystemParameter('STBOWNER'));
  
  isr$trace.info('clXML', 'joblist.xml', sCurrentName,  clXML);
          
  sResult := ISR$LOADER.sendJobList(sHost, sPort, STB$UTIL.getSystemParameter('LOADER_CONTEXT'), STB$UTIL.getSystemParameter('LOADER_NAMESPACE'), STB$UTIL.clobToBlob(ISR$XML.XmlToClob(xXml)), STB$UTIL.getSystemParameter('TIMEOUT_LOADER_VISUAL'));
  IF sResult != 'T' THEN
    isr$trace.error('sResult', sResult, sCurrentName);
  END IF;
  
  isr$trace.stat('end', 'end', sCurrentName);
  RETURN sResult;
EXCEPTION WHEN OTHERS THEN
  isr$trace.error(SQLCODE, SQLERRM, sCurrentName);
  RETURN SQLERRM;
END sendJobList;

--******************************************************************************
procedure sendErrorObjToLoader(sHost in varchar2, sPort in varchar2, oErrorObj in STB$OERROR$RECORD,  cnJobID IN NUMBER)
is
  sCurrentName  constant varchar2(100) := $$PLSQL_UNIT||'.sendErrorObjToLoader('||cnJobid||')';  
  sContext   constant VARCHAR2(500) := STB$UTIL.getSystemParameter('LOADER_CONTEXT');
  sNameSpace constant VARCHAR2(500) := STB$UTIL.getSystemParameter('LOADER_NAMESPACE');
  sTimeout   constant VARCHAR2(500) := STB$UTIL.getSystemParameter('TIMEOUT_LOADER_VISUAL');
  sErrMsg             clob;
  sBackErrMsg         clob;
begin
  isr$trace.stat('begin', 'begin', sCurrentName);
  sErrMsg := xmltype(oErrorObj).getClobVal;
  isr$trace.debug('sErrMsg','s. logclob',sCurrentName, sErrMsg);
  sBackErrMsg := ISR$LOADER.sendErrorObject(sHost, sPort, sContext, sNameSpace, sErrMsg, cnJobID, to_number(sTimeout));
  if sBackErrMsg != 'T' then
    ISR$TRACE.error('sReturn', 's. logclob', sCurrentName, sBackErrMsg);
  end if;
  isr$trace.stat('end', 'end', sCurrentName);
exception 
  when others then
    isr$trace.error(sqlcode, sqlerrm, scurrentname);
end sendErrorObjToLoader;

--******************************************************************************
function sendDbToken(sHost in varchar2, sPort in varchar2) return stb$oerror$record
is
  oErrorObj    stb$oerror$record      := STB$OERROR$RECORD();
  sCurrentName  constant varchar2(100) := $$PLSQL_UNIT||'.sendDbToken'; 
  exSendToken      exception; 
  sContext   constant VARCHAR2(500) := STB$UTIL.getSystemParameter('LOADER_CONTEXT');
  sNameSpace constant VARCHAR2(500) := STB$UTIL.getSystemParameter('LOADER_NAMESPACE');
  sTimeout   constant VARCHAR2(500) := STB$UTIL.getSystemParameter('TIMEOUT_LOADER_VISUAL');
  sUserErrMsg         varchar2(4000);
  bRsa            blob;
  bUtil           blob;
  nServerCount    number;
  exServerNotFound exception;
begin
  isr$trace.stat('begin', 'begin', sCurrentName);
  
  select count(*) 
  into nServerCount
  from isr$server
  where servertypeid = 1;
  
  if nServerCount < 1 then
    raise exServerNotFound;
  end if;
  
  select p1.binaryvalue binaryvalue_rsa,  p2.binaryvalue binaryvalue_util 
  into bRsa, bUtil
  from isr$server s, isr$serverparameter p1, isr$serverparameter p2 
  where s.serverid = p1.serverid
  and s.serverid = p2.serverid
  and s.servertypeid=1
  and p1.parametername = 'dbToken.rsa'
  and p2.parametername = 'util1.jar'
  and rownum = 1;
    
  sUserErrMsg := ISR$LOADER.sendDbToken(sHost, sPort, sContext, sNameSpace, bRsa, bUtil, to_number(sTimeout));
  if sUserErrMsg != 'T' then
    isr$trace.error('sUserErrMsg', 's. logclob', sCurrentName, sUserErrMsg);
    raise exSendToken;
  end if;
  isr$trace.stat('end', 'end', sCurrentName);
  return oErrorObj;
exception 
  when exSendToken then
    oErrorObj.handleError ( Stb$typedef.cnSeverityCritical, sUserErrMsg, sCurrentName, dbms_utility.format_error_stack ||stb$typedef.crlf|| dbms_utility.format_error_backtrace );
  return oErrorObj; 
  when exServerNotFound then
    sUserErrMsg := utd$msglib.getmsg ('exObserverNotFound', stb$security.getCurrentLanguage);
    oErrorObj.handleError ( Stb$typedef.cnSeverityWarning, sUserErrMsg, sCurrentName, dbms_utility.format_error_stack ||stb$typedef.crlf|| dbms_utility.format_error_backtrace );
  return oErrorObj;
  when no_data_found then
    sUserErrMsg := utd$msglib.getmsg ('exRestartObserver', stb$security.getCurrentLanguage);
    isr$trace.debug('sUserErrMsg', stb$security.getCurrentLanguage || ', ' ||sUserErrMsg, sCurrentName);
    oErrorObj.handleError ( Stb$typedef.cnSeverityCritical, sUserErrMsg, sCurrentName, dbms_utility.format_error_stack ||stb$typedef.crlf|| dbms_utility.format_error_backtrace );     
    return oErrorObj;
  when others then
    sUserErrMsg := utd$msglib.getmsg ('exUnexpectedError', stb$security.getCurrentLanguage,  csP1 => sqlerrm(sqlcode));
    oErrorObj.handleError ( Stb$typedef.cnSeverityCritical, sUserErrMsg, sCurrentName, dbms_utility.format_error_stack ||stb$typedef.crlf|| dbms_utility.format_error_backtrace );     
    return oErrorObj;
end sendDbToken;

--******************************************************************************
function startJob(sHost in varchar2, sPort in varchar2, sContext in varchar2, sNamespace in varchar2, nJobID in number, sFolderName in varchar2, sReactivate in varchar2, nJobCount in number, nTimeout IN NUMBER) return varchar2
as language java
name 'com.uptodata.isr.loader.webservice.client.LoaderWebserviceClient.startJob(java.lang.String, java.lang.String, java.lang.String, java.lang.String, int, java.lang.String, java.lang.String, int, int) return java.lang.String';

function getRunningJobs(nJobID in number) return number is
  sCurrentName        constant varchar2(100) := $$PLSQL_UNIT||'.getRunningJobs';
  nJobCount   number;
begin
  isr$trace.stat('begin', 'begin', sCurrentName);
  
  select count (*) into nJobCount
  from stb$jobsession s, stb$jobqueue q, user_scheduler_jobs us
  where s.sessionid in (select sessionid
                                from isr$session s, stb$jobqueue jq
                               where upper(s.username) = upper(STB$SECURITY.getOracleUsername(jq.userno))
                                 and jq.jobid = nJobID) 
  and s.jobid = q.jobid 
  and us.state = 'RUNNING'
  and trim (us.job_name(+)) = trim (q.oraclejobid);
  
  isr$trace.debug('nJobCount', nJobCount, sCurrentName); 
  isr$trace.stat('end', 'end', sCurrentName);
  return nJobCount;
exception when others then
  isr$trace.warn(sqlcode, sqlerrm, sCurrentName);
  return 0;
end getRunningJobs;
--******************************************************************************
function startJob(sHost in varchar2, sPort in varchar2, sContext in varchar2, sNamespace in varchar2, nJobID in number, sFolderName in varchar2, sReactivate in varchar2, nTimeout IN NUMBER) return varchar2 is
  sCurrentName        constant varchar2(100) := $$PLSQL_UNIT||'.startJob(' || nJobID || ')';
  nJobCount   number;
begin
  isr$trace.stat('begin', 'begin', sCurrentName);
  nJobCount := getRunningJobs(nJobID); 
  isr$trace.stat('end', 'end', sCurrentName);
  return startJob(sHost, sPort, sContext, sNamespace, nJobID,sFolderName,  sReactivate, nJobCount, nTimeout);
end startJob;

--******************************************************************************
function terminateJob(sHost in varchar2, sPort in varchar2, sContext in varchar2, sNamespace in varchar2, nJobID in number, nJobCount in number, nTimeout IN NUMBER) return varchar2
as language java
name 'com.uptodata.isr.loader.webservice.client.LoaderWebserviceClient.terminateJob(java.lang.String, java.lang.String, java.lang.String, java.lang.String, int, int, int) return java.lang.String';

--******************************************************************************
function showTerminateJobSuccess(sHost in varchar2, sPort in varchar2, sContext in varchar2, sNamespace in varchar2, nJobID in number, nTimeout IN NUMBER) return varchar2
as language java
name 'com.uptodata.isr.loader.webservice.client.LoaderWebserviceClient.showTerminateSuccess(java.lang.String, java.lang.String, java.lang.String, java.lang.String, int, int) return java.lang.String';

--******************************************************************************
function terminateJob(sHost in varchar2, sPort in varchar2, sContext in varchar2, sNamespace in varchar2, nJobID in number) return varchar2 is
  oErrorObj              STB$OERROR$RECORD := STB$OERROR$RECORD();
  sCurrentName        constant varchar2(100) := $$PLSQL_UNIT||'.terminateJob(' || nJobID || ')' ;
  nJobCount   number;
  sReturn    varchar2(4000);
  sTimeout   constant VARCHAR2(500) := STB$UTIL.getSystemParameter('TIMEOUT_LOADER_VISUAL');
begin
  isr$trace.stat('begin', 'begin', sCurrentName);
  nJobCount := getRunningJobs(nJobID);  
  isr$trace.debug('oErrorObj', '->logclob', sCurrentName, oErrorObj);
  sReturn := terminateJob(sHost, sPort, sContext, sNamespace, nJobID, nJobCount, to_number(sTimeout));
  isr$trace.debug('sReturn', sReturn, sCurrentName); 
  isr$trace.stat('end', 'end', sCurrentName);
  return sReturn; 
end terminateJob;

--******************************************************************************
function showTerminateJobSuccess(sHost in varchar2, sPort in varchar2, sContext in varchar2, sNamespace in varchar2, nJobID in number) return varchar2 is
  sCurrentName        constant varchar2(100) := $$PLSQL_UNIT||'.showTerminateJobSuccess(' || nJobID || ')' ;
  sReturn    varchar2(4000);
  sTimeout   constant VARCHAR2(500) := STB$UTIL.getSystemParameter('TIMEOUT_LOADER_VISUAL');
begin
  isr$trace.stat('begin', 'begin', sCurrentName);
  sReturn := showTerminateJobSuccess(sHost, sPort, sContext, sNamespace, nJobID, to_number(sTimeout));
  isr$trace.stat('end', sReturn, sCurrentName);
  return sReturn; 
end showTerminateJobSuccess;

--******************************************************************************
function showTerminateJobWarn(sHost in varchar2, sPort in varchar2, sContext in varchar2, sNamespace in varchar2, nJobID in number, nTimeout IN NUMBER) return varchar2
as language java
name 'com.uptodata.isr.loader.webservice.client.LoaderWebserviceClient.showTerminateWarning(java.lang.String, java.lang.String, java.lang.String, java.lang.String, int, int) return java.lang.String';

--******************************************************************************
function showTerminateJobWarn(sHost in varchar2, sPort in varchar2, sContext in varchar2, sNamespace in varchar2, nJobID in number) return varchar2 is
  sCurrentName        constant varchar2(100) := $$PLSQL_UNIT||'.showTerminateJobWarn(' || nJobID || ')' ;
  sReturn    varchar2(4000);
  sTimeout   constant VARCHAR2(500) := STB$UTIL.getSystemParameter('TIMEOUT_LOADER_VISUAL');
begin
  isr$trace.stat('begin', 'begin', sCurrentName);
  sReturn := showTerminateJobWarn(sHost, sPort, sContext, sNamespace, nJobID, to_number(sTimeout));
  isr$trace.stat('end', sReturn, sCurrentName);
  return sReturn; 
end showTerminateJobWarn;

--******************************************************************************
function removeJobFolder(sHost in varchar2, sPort in varchar2, sContext in varchar2, sNamespace in varchar2, nJobID in number, nTimeout IN NUMBER) return varchar2
as language java
name 'com.uptodata.isr.loader.webservice.client.LoaderWebserviceClient.removeJobFolder(java.lang.String, java.lang.String, java.lang.String, java.lang.String, int, int) return java.lang.String';

--******************************************************************************
function compareDocumentOID(sHost IN VARCHAR2, sPort IN VARCHAR2, sContext IN VARCHAR2, sNamespace IN VARCHAR2,  sDocumentOID in VARCHAR2, blFile IN BLOB, nTimeout IN NUMBER) RETURN VARCHAR2
as language java
name 'com.uptodata.isr.loader.webservice.client.LoaderWebserviceClient.compareDocumentOID(java.lang.String, java.lang.String, java.lang.String, java.lang.String,  java.lang.String, java.sql.Blob, int) return java.lang.String';

--******************************************************************************
function protectAllowOnlyRevisions(sHost IN VARCHAR2, sPort IN VARCHAR2, sContext IN VARCHAR2, sNamespace IN VARCHAR2, nJobID in number, sFileName in VARCHAR2, sPassword in VARCHAR2, blFile IN BLOB, nTimeout IN NUMBER) RETURN VARCHAR2
as language java
name 'com.uptodata.isr.loader.webservice.client.LoaderWebserviceClient.protectAllowOnlyRevisions(java.lang.String, java.lang.String, java.lang.String, java.lang.String, int, java.lang.String, java.lang.String, java.sql.Blob, int) return java.lang.String';


END ISR$LOADER;