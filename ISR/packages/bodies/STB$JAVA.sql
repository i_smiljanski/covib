CREATE OR REPLACE PACKAGE BODY Stb$java
is
--******************************************************************************
FUNCTION ipInRange(sTheIp IN VARCHAR2, sMinIp IN VARCHAR2, sMaxIp in VARCHAR2) RETURN VARCHAR2
as LANGUAGE JAVA
NAME 'com.utd.stb.dbstored.IPTest.IPInRange(java.lang.String, java.lang.String, java.lang.String) return java.lang.String';
--******************************************************************************
FUNCTION TRANSFORM( clXML IN CLOB, clStyleSheet IN CLOB ) RETURN CLOB
as LANGUAGE JAVA
NAME 'com.uptodata.isr.transform.Transform.transform( java.sql.Clob, java.sql.Clob ) return java.sql.Clob';
--******************************************************************************
FUNCTION TRANSFORM( clXML IN CLOB, clStyleSheet IN CLOB, clParameters IN VARCHAR2 ) RETURN CLOB
as LANGUAGE JAVA
NAME 'com.uptodata.isr.transform.Transform.transform( java.sql.Clob, java.sql.Clob, java.lang.String ) return java.sql.Clob';
--******************************************************************************
function md5_string (str in varchar2) return varchar2
as language java
name 'com.uptodata.isr.db.HashCalculation.calculateHash(java.lang.String) return java.lang.String';
--******************************************************************************
function calculateHash (clClob IN CLOB) return varchar2
as language java
name 'com.uptodata.isr.db.HashCalculation.calculateClobHash(java.sql.Clob) return java.lang.String';
--******************************************************************************
FUNCTION zip(cnJobid IN NUMBER) RETURN BLOB
AS LANGUAGE JAVA
NAME 'com.uptodata.isr.zip.Zip.zip(int) return java.sql.Blob';
--******************************************************************************
FUNCTION zip(filesToZip IN ISR$PARAMETER$LIST) RETURN BLOB
AS LANGUAGE JAVA
NAME 'com.uptodata.isr.zip.Zip.zip(java.sql.Array) return java.sql.Blob';
--******************************************************************************
PROCEDURE unzip(cnJobid IN NUMBER, blZipFile IN BLOB)
AS LANGUAGE JAVA
NAME 'com.uptodata.isr.zip.Zip.unzip(int, java.sql.Blob)';
--******************************************************************************
procedure sendMail(SMTP_HOST_NAME VARCHAR2, SMTP_PORT_NAME VARCHAR2, SMTP_AUTH_USER VARCHAR2, SMTP_AUTH_PWD VARCHAR2, to_user VARCHAR2, cc VARCHAR2, subject VARCHAR2, message VARCHAR2, csJobId VARCHAR2)
as language java
name 'com.uptodata.isr.mail.SendMail.sendMail(java.lang.String, java.lang.String, java.lang.String, java.lang.String, java.lang.String, java.lang.String, java.lang.String, java.lang.String, java.lang.String)';
--******************************************************************************
FUNCTION setDocumentProperties(blFile in BLOB, clXML in CLOB) RETURN BLOB
as language java
name 'com.utd.isr.db.SetDocumentProperties.setDocumentProperties(java.sql.Blob, java.sql.Clob) return java.sql.Blob';
--******************************************************************************
FUNCTION getBlobByteString(bDoc in BLOB) RETURN CLOB
as language java
name 'com.uptodata.isr.db.BlobByteString.getBlobByteString(java.sql.Blob) return java.sql.Clob';
--******************************************************************************
FUNCTION getStringByteBlob(cDoc in CLOB) RETURN BLOB
as language java
name 'com.uptodata.isr.db.BlobByteString.getStringByteBlob(java.sql.Clob) return java.sql.Blob';
--******************************************************************************
PROCEDURE testConnection (csHost IN varchar2, csPort IN varchar2)
AS LANGUAGE JAVA
NAME 'com.uptodata.isr.db.TestConnection.test( java.lang.String, java.lang.String )';
--******************************************************************************
PROCEDURE checkStatus(csObserverHost IN VARCHAR2, csObserverPort IN VARCHAR2)
AS LANGUAGE JAVA
NAME 'com.uptodata.isr.db.Observer.checkStatus( java.lang.String, java.lang.String )';
--******************************************************************************
FUNCTION getServerLog (csObserverHost IN VARCHAR2, csObserverPort IN VARCHAR2) RETURN CLOB
AS LANGUAGE JAVA
NAME 'com.uptodata.isr.db.Observer.getLogFile( java.lang.String, java.lang.String ) return java.sql.Clob';
--******************************************************************************
FUNCTION getBootPath (csObserverHost IN VARCHAR2, csObserverPort IN VARCHAR2) RETURN VARCHAR2
AS LANGUAGE JAVA
NAME 'com.uptodata.isr.db.Observer.getBootPath( java.lang.String, java.lang.String ) return java.lang.String';
--******************************************************************************
FUNCTION getFileSeparator (csObserverHost IN VARCHAR2, csObserverPort IN VARCHAR2) RETURN VARCHAR2
AS LANGUAGE JAVA
NAME 'com.uptodata.isr.db.Observer.getFileSeparator( java.lang.String, java.lang.String ) return java.lang.String';
--******************************************************************************
FUNCTION getRemoteXml ( csHost IN VARCHAR2, cnPort IN NUMBER, csXml IN CLOB, cnRMITimeout IN NUMBER, cnTimeout IN NUMBER, cnLoglevel IN NUMBER ) RETURN CLOB
AS LANGUAGE JAVA
NAME 'com.uptodata.isr.db.RMIConnectionServer.getRemoteXml( java.lang.String, int, java.sql.Clob, long, long, int ) return java.sql.Clob';
--******************************************************************************
FUNCTION encryptPassword ( csPassword IN VARCHAR2 ) RETURN VARCHAR2
AS LANGUAGE JAVA
NAME 'com.uptodata.isr.utils.security.SymmetricProtection.encrypt( java.lang.String ) return java.lang.String';
--******************************************************************************
FUNCTION decryptPassword ( csPassword IN VARCHAR2 ) RETURN VARCHAR2
AS LANGUAGE JAVA
NAME 'com.uptodata.isr.utils.security.SymmetricProtection.decrypt( java.lang.String ) return java.lang.String';
--******************************************************************************
PROCEDURE callWordmodule(cnJobid IN NUMBER)
AS LANGUAGE JAVA
NAME 'com.utd.isr.words.WordsBase.callWordmodule(int)';
--******************************************************************************
FUNCTION fInv(alpha in NUMBER, n1 in NUMBER, n2 in NUMBER) RETURN NUMBER
as LANGUAGE JAVA
NAME 'de.utd.ftest.FTable.fInv(double,double,double) return double';
--******************************************************************************
FUNCTION zip(blBlob IN BLOB, sFileName in VARCHAR2) RETURN BLOB
AS LANGUAGE JAVA
NAME 'com.uptodata.isr.zip.Zip.zip(java.sql.Blob, java.lang.String) return java.sql.Blob';
--****************************************************************************************************************************
FUNCTION unzip(blBlob IN BLOB) RETURN BLOB
AS LANGUAGE JAVA
NAME 'com.uptodata.isr.zip.Zip.unzip(java.sql.Blob) return java.sql.Blob'; 
--****************************************************************************************************************************
FUNCTION convertPdfToImage(cnJobid IN NUMBER, pdfArray IN ISR$FILEBOOKMARK$LIST, clXslt IN CLOB, sFileExtension IN VARCHAR2) RETURN VARCHAR2
AS LANGUAGE JAVA
NAME 'com.utd.isr.pdfimg.PdfToImage.convertPdfToImage(int, java.sql.Array, java.sql.Clob, java.lang.String) return java.lang.String';
--****************************************************************************************************************************
PROCEDURE addAttachmentsToDoc(files IN ISR$FILEBOOKMARK$LIST, blWordFile OUT BLOB)
as LANGUAGE JAVA
NAME 'com.utd.isr.aspose.Wordmodule.addAttachmentsToDoc(java.sql.Array, java.sql.Blob[])';

--******************************************************************************
FUNCTION concatenatePdfs(blFileList IN ISR$FILEBOOKMARK$LIST, clXml IN CLOB) return BLOB 
AS LANGUAGE JAVA
NAME 'com.utd.isr.pdfimg.ConcatenateMultiFiles.concatenate(java.sql.Array, java.sql.Clob) return java.sql.Blob';

--*****************************************************************************
/*PROCEDURE  unprotectDocument(blInputDoc IN BLOB, sPassword IN VARCHAR2, sDocFormat IN VARCHAR2, blOutputDoc OUT BLOB)
as LANGUAGE JAVA
NAME 'com.uptodata.isr.words.DocumentProtecting.unprotectDocument(java.sql.Blob, java.lang.String, java.lang.String, java.sql.Blob[])';

--******************************************************************************
PROCEDURE  protectWithTrackChanges(blInputDoc IN BLOB, sPassword IN VARCHAR2, sDocFormat IN VARCHAR2, blOutputDoc OUT BLOB)
as LANGUAGE JAVA
NAME 'com.uptodata.isr.words.DocumentProtecting.protectWithTrackChanges(java.sql.Blob, java.lang.String, java.lang.String, java.sql.Blob[])';
*/
--******************************************************************************
function getMaxMemorySize return number
is language java name
'oracle.aurora.vm.OracleRuntime.getMaxMemorySize() returns long';

--******************************************************************************
function setMaxMemorySize(num number) return number
is language java name
'oracle.aurora.vm.OracleRuntime.setMaxMemorySize(long) returns long';

--******************************************************************************
PROCEDURE SLEEPIMPL (P_MILLI_SECONDS IN NUMBER) 
   AS LANGUAGE JAVA NAME 'java.lang.Thread.sleep(long)';
   
END Stb$java;