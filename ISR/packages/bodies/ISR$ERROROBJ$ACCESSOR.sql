CREATE OR REPLACE PACKAGE BODY iSR$ErrorObj$Accessor
is

  oCurrentErrorRec  STB$OERROR$RECORD := STB$OERROR$RECORD(tloMessageList => ISR$MESSAGE$LIST());
  -- current error object is initialized with a 0 elements message stack
  
  nWorstSeverityCode integer := Stb$typedef.cnNoError;
  -- the worst SeverityCode in the session

-----------------------------------
-- local functions and procedures
-----------------------------------

-- ***************************************************************************************************
PROCEDURE SetWorstSeverityCode(nSeverityCode in integer) IS
-- ***************************************************************************************************
-- Created: 03.Jun 2015 HR
-- Sets the local variable nWorstSeverityCode to the worst of nWorstSeverityCode and nSeverityCode
--
-- Parameter:
-- nSeverityCode the severity code to be compared to nWorstSeverityCode
--
-- ***************************************************************************************************
  
  sCurrentName constant varchar2(100) := $$PLSQL_UNIT||'.SetWorstSeverityCode('||nSeverityCode||')';
  
  function SeverityScore(cnCode in integer) return integer is
    -- local function to score the severity code    
    nScore                integer;
  begin 
    case cnCode
      when Stb$typedef.cnNoError          then nScore :=  0;
      when Stb$typedef.cnSeverityMessage  then nScore := 10;
      when Stb$typedef.cnSeverityWarning  then nScore := 20;
      when Stb$typedef.cnSeverityCritical then nScore := 30;
      when Stb$typedef.cnFatal            then nScore := 40;
      else                                     nScore := -1; 
    end case; 
    return nScore;
  end SeverityScore;
  
BEGIN     
  isr$trace.debug_all('begin', 'nSeverityCode: '||nSeverityCode||'  nWorstSeverityCode: '||nWorstSeverityCode, sCurrentName, xmltype(oCurrentErrorRec).getClobVal());
  -- set the session worst severity code
  if oCurrentErrorRec.TLOMESSAGELIST is null or oCurrentErrorRec.TLOMESSAGELIST.count() = 0 then
    -- No error is recorded yet. The current error rec has been resetted. Reset the worst SeverityCode too
    nWorstSeverityCode := Stb$typedef.cnNoError;
  else
    if SeverityScore(nSeverityCode) > SeverityScore(nWorstSeverityCode) then
      nWorstSeverityCode := nSeverityCode;
      isr$trace.debug_all('nWorstSeverityCode set to', nWorstSeverityCode, sCurrentName);
    end if;  
  end if;
  isr$trace.debug_all('end','nWorstSeverityCode: '||nWorstSeverityCode, sCurrentName, xmltype(oCurrentErrorRec).getClobVal());
END SetWorstSeverityCode;

 
-----------------------------------
-- public functions and procedures
-----------------------------------
--******************************************************************************************************
FUNCTION  GetCurrentErrorObj RETURN STB$OERROR$RECORD IS
  sCurrentName constant varchar2(100) := $$PLSQL_UNIT||'.GetCurrentErrorObj';
BEGIN
  isr$trace.debug_all('values','sSeverityCode: '||oCurrentErrorRec.sSeverityCode||'  sErrorCode: '||oCurrentErrorRec.sErrorCode||'sErrorMessage: '||oCurrentErrorRec.sErrorMessage, sCurrentName /*, sys.anydata.convertObject(oCurrentErrorRec)*/ );
  -- return the session error object 
  RETURN oCurrentErrorRec;
END GetCurrentErrorObj;

-- ***************************************************************************************************
PROCEDURE SetCurrentErrorObj(oErrorRec in STB$OERROR$RECORD) IS
BEGIN     
  -- set the session error object and worst SeverityCode 
  oCurrentErrorRec := oErrorRec;
  SetWorstSeverityCode(oErrorRec.sSeverityCode);
END SetCurrentErrorObj;

--******************************************************************************************************
FUNCTION  GetWorstSeverityCode RETURN integer IS
BEGIN
  -- return the session worst severity code  
  RETURN nWorstSeverityCode;
END GetWorstSeverityCode;



END iSR$ErrorObj$Accessor;