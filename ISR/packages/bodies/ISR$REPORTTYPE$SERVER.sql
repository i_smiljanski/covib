CREATE OR REPLACE package body isr$reporttype$server as
--******************************************************************************
FUNCTION getNodeChilds (oSelectedNode IN stb$treenode$record, olchildnodes OUT stb$treenodelist) RETURN STB$OERROR$RECORD
is
  sCurrentName        constant varchar2(200) := $$PLSQL_UNIT||'.getNodeChilds('||oSelectedNode.sNodeType||')';
  oErrorObj           stb$oerror$record := STB$OERROR$RECORD();
  sMsg   varchar2(4000);
begin
  isr$trace.stat('begin','begin', sCurrentName);
  oErrorObj := isr$reporttype$package.getNodeChilds(oSelectedNode, olChildNodes);
  isr$trace.stat('end','end', sCurrentName);
  return oErrorObj;
exception
  when others then
    sMsg := utd$msglib.getmsg ('exUnexpectedError', stb$security.getCurrentlanguage,  csP1 => sqlerrm(sqlcode));
    oErrorObj.handleError ( Stb$typedef.cnSeverityCritical, sMsg, sCurrentName, dbms_utility.format_error_stack ||chr(10)||chr(13)|| dbms_utility.format_error_backtrace );
    return oErrorObj;
end getNodeChilds;

--******************************************************************************
function getNodeList (oSelectedNode in stb$treenode$record, olNodeList out stb$treenodelist) return stb$oerror$record
is
  sCurrentName        constant varchar2(200) := $$PLSQL_UNIT||'.getNodeList('||oSelectedNode.sNodeType||')';
  oErrorObj           stb$oerror$record := STB$OERROR$RECORD();
  sMsg   varchar2(4000);
begin
  isr$trace.stat('begin','begin', sCurrentName);
  oErrorObj := isr$reporttype$package.getNodeList(oSelectedNode, olNodeList);
  isr$trace.stat('end','end', sCurrentName);
  return oErrorObj;
exception
  when others then
    sMsg := utd$msglib.getmsg ('exUnexpectedError', stb$security.getCurrentlanguage,  csP1 => sqlerrm(sqlcode));
    oErrorObj.handleError ( Stb$typedef.cnSeverityCritical, sMsg, sCurrentName, dbms_utility.format_error_stack ||chr(10)||chr(13)|| dbms_utility.format_error_backtrace );
    return oErrorObj;
end getNodeList;

--******************************************************************************
FUNCTION setCurrentNode (oSelectedNode IN stb$treenode$record) RETURN stb$oerror$record
is
  sCurrentName        constant varchar2(200) := $$PLSQL_UNIT||'.setCurrentNode('||oSelectedNode.sNodeType||')';
  oErrorObj           stb$oerror$record := STB$OERROR$RECORD();
  sMsg   varchar2(4000);
begin
  isr$trace.stat('begin','begin', sCurrentName);
  oErrorObj := isr$reporttype$package.setCurrentNode(oSelectedNode);
  isr$trace.stat('end','end', sCurrentName);
  return oErrorObj;
exception
  when others then
    sMsg := utd$msglib.getmsg ('exUnexpectedError', stb$security.getCurrentlanguage,  csP1 => sqlerrm(sqlcode));
    oErrorObj.handleError ( Stb$typedef.cnSeverityCritical, sMsg, sCurrentName, dbms_utility.format_error_stack ||chr(10)||chr(13)|| dbms_utility.format_error_backtrace );
    return oErrorObj;
end setCurrentNode;

--******************************************************************************
function setMenuAccess( sParameter   in varchar2, sMenuAllowed out varchar2 ) return stb$oerror$record
is
  sCurrentName        constant varchar2(200) := $$PLSQL_UNIT||'.setMenuAccess('||sParameter||')';
  oErrorObj           stb$oerror$record := STB$OERROR$RECORD();
  sMsg   varchar2(4000);
begin
  isr$trace.stat('begin','begin', sCurrentName);
  oErrorObj := isr$reporttype$package.setMenuAccess(sParameter, sMenuAllowed);
  isr$trace.stat('end','end', sCurrentName);
  return oErrorObj;
exception
  when others then
    sMsg := utd$msglib.getmsg ('exUnexpectedError', stb$security.getCurrentlanguage,  csP1 => sqlerrm(sqlcode));
    oErrorObj.handleError ( Stb$typedef.cnSeverityCritical, sMsg, sCurrentName, dbms_utility.format_error_stack ||chr(10)||chr(13)|| dbms_utility.format_error_backtrace );
    return oErrorObj;
end setMenuAccess;

--******************************************************************************
FUNCTION getContextMenu( nMenu           in  number, olMenuEntryList out stb$menuentry$list) RETURN stb$oerror$record
is
  sCurrentName        constant varchar2(200) := $$PLSQL_UNIT||'.getContextMenu('||nMenu||')';
  oErrorObj           stb$oerror$record := STB$OERROR$RECORD();
  sMsg   varchar2(4000);
begin
  isr$trace.stat('begin','begin', sCurrentName);
  oErrorObj := isr$reporttype$package.getContextMenu(nMenu, olMenuEntryList);
  isr$trace.stat('end','end', sCurrentName);
  return oErrorObj;
exception
  when others then
    sMsg := utd$msglib.getmsg ('exUnexpectedError', stb$security.getCurrentlanguage,  csP1 => sqlerrm(sqlcode));
    oErrorObj.handleError ( Stb$typedef.cnSeverityCritical, sMsg, sCurrentName, dbms_utility.format_error_stack ||chr(10)||chr(13)|| dbms_utility.format_error_backtrace );
    return oErrorObj;
end getContextMenu;

--******************************************************************************
function callFunction( oParameter IN STB$MENUENTRY$RECORD, olNodeList OUT STB$TREENODELIST  ) return stb$oerror$record
is
  sCurrentName        constant varchar2(200) := $$PLSQL_UNIT||'.callFunction('||oParameter.sToken||')';
  oErrorObj           stb$oerror$record := STB$OERROR$RECORD();
  sMsg   varchar2(4000);

begin
  isr$trace.stat('begin','begin', sCurrentName);
  olNodeList := STB$TREENODELIST();

  STB$OBJECT.setCurrentToken(oparameter.stoken);

  IF stb$util.getreptypeparameter (stb$object.getCurrentReporttypeid, 'REQUIRES_VERSIONING') = 'T' THEN
    oErrorObj := isr$reporttype$package.createNewReport (stb$typedef.cnversion);
  end if;

  IF oErrorObj.sSeverityCode != stb$typedef.cnnoerror THEN
    RETURN oErrorObj;
  END IF;

  olnodelist := stb$treenodelist ();
  olnodelist.EXTEND;
  olnodelist (1) := STB$TREENODE$RECORD(STB$UTIL.getMaskTitle(Upper (oParameter.sToken)),
                                        $$PLSQL_UNIT, 'TRANSPORT', 'MASK');
  oErrorObj := STB$UTIL.getPropertyList('CAN_CREATE_NEW_REPORT', olNodeList(1).tloPropertylist);
  isr$trace.debug('after getPropertyList','olNodeList(1)',sCurrentName,olNodelist(1));
  isr$trace.debug('current parameters',STB$OBJECT.getCurrentNodeType || '; '||STB$OBJECT.getCurrentNodeId|| '; '||STB$SECURITY.getCurrentUser,sCurrentName);



  FOR i IN 1..olNodeList (1).tloPropertylist.COUNT LOOP
    IF olNodeList (1).tloPropertylist(i).sParameter like 'SERVER_%' THEN
      for rGetServerId in (select key  from isr$crit where repid = STB$OBJECT.getCurrentRepid and entity ='SERVER') loop
        isr$trace.debug('rGetServerId',rGetServerId.key,sCurrentName);
        if olNodeList (1).tloPropertylist(i).sParameter = 'SERVER_' || rGetServerId.key then
          olNodeList (1).tloPropertylist(i).sPromptDisplay := olNodeList (1).tloPropertylist(i).sPromptDisplay ||
             utd$msglib.getmsg ('CURRENTSERVER', stb$security.getCurrentlanguage); -- ' (current server)';
        end if;
      end loop;
    ELSE
      olNodeList (1).tloPropertylist(i).type := 'DELETE';
    END IF;
  END LOOP;
  isr$trace.debug('final PropertyList','olNodeList',sCurrentName,olNodelist);

  isr$trace.stat('end','end', sCurrentName);
  return oErrorObj;
exception
  when others then
    sMsg := utd$msglib.getmsg ('exUnexpectedError', stb$security.getCurrentlanguage,  csP1 => sqlerrm(sqlcode));
    oErrorObj.handleError ( Stb$typedef.cnSeverityCritical, sMsg, sCurrentName, dbms_utility.format_error_stack ||chr(10)||chr(13)|| dbms_utility.format_error_backtrace );
    return oErrorObj;
end callFunction;

--******************************************************************************
FUNCTION putParameters( oParameter  IN STB$MENUENTRY$RECORD, olParameter IN STB$PROPERTY$LIST ) RETURN stb$oerror$record
is
  sCurrentName        constant varchar2(200) := $$PLSQL_UNIT||'.putParameters('||oParameter.sToken||')';
  oErrorObj           stb$oerror$record := STB$OERROR$RECORD();
  sMsg   varchar2(4000);
  oNewNode                      STB$TREENODE$RECORD;
  sAuditFlag                    VARCHAR2(1);
  exaudit                       EXCEPTION;
  
  sServerChangeList             ISR$VALUE$LIST;
  sChanges                      VARCHAR2 (4000);
  nCnt                          number := 0;
begin
  isr$trace.stat('begin','begin', sCurrentName);
  
  sServerChangeList := ISR$VALUE$LIST();
  
  nCnt := 0;
  for rGetDeletedServers in ( select serverid, alias
                                from isr$server, (select key
                                                    from isr$crit
                                                   where repid = STB$OBJECT.getCurrentRepid
                                                     and entity = 'SERVER'
                                                  minus
                                                  select REPLACE (sParameter, 'SERVER_') server
                                                    from table (olParameter)
                                                   where sValue = 'T'
                                                     and sParameter like 'SERVER_%')
                               where key = serverid
                              order by alias) loop
    sServerChangeList.extend();
    sServerChangeList(sServerChangeList.count()) := ISR$VALUE$RECORD(rGetDeletedServers.alias || ' [' || rGetDeletedServers.serverid || ']', utd$msglib.getmsg ('DELETED', stb$security.getCurrentLanguage));                          
    nCnt := nCnt + 1;
    if nCnt = 1 then
      sChanges := utd$msglib.getmsg ('DELETED', stb$security.getCurrentLanguage) || ': ';
    else
      sChanges := SUBSTR(sChanges || ', ', 0, 4000);
    end if;
    sChanges := SUBSTR(sChanges || rGetDeletedServers.alias || ' [' || rGetDeletedServers.serverid || ']', 0, 4000);
  end loop;
  
  nCnt := 0;
  for rGetAddedServers in ( select serverid, alias
                              from isr$server, (select REPLACE (sParameter, 'SERVER_') server
                                                  from table (olParameter)
                                                 where sValue = 'T'
                                                   and sParameter like 'SERVER_%'
                                                 minus
                                                select key
                                                  from isr$crit
                                                 where repid = STB$OBJECT.getCurrentRepid
                                                   and entity = 'SERVER')
                             where server = serverid
                            order by alias) loop
    sServerChangeList.extend();
    sServerChangeList(sServerChangeList.count()) := ISR$VALUE$RECORD(rGetAddedServers.alias || ' [' || rGetAddedServers.serverid || ']', utd$msglib.getmsg ('INSERTED', stb$security.getCurrentLanguage));                          
    nCnt := nCnt + 1;
    if nCnt = 1 then
      sChanges := case when length(trim(sChanges)) > 0 then chr(10) else '' end || SUBSTR(sChanges || utd$msglib.getmsg ('INSERTED', stb$security.getCurrentLanguage) || ': ', 0, 4000);
    else
      sChanges := SUBSTR(sChanges || ', ', 0, 4000);
    end if;
    sChanges := SUBSTR(sChanges || rGetAddedServers.alias || ' [' || rGetAddedServers.serverid || ']', 0, 4000);
  end loop;  
  
  delete from isr$crit where repid = STB$OBJECT.getCurrentRepid and entity ='SERVER';
  insert into ISR$CRIT (ENTITY
                      , KEY
                      , DISPLAY
                      , INFO
                      , ORDERNUMBER
                      , SELECTED
                      , REPID
                      , MASTERKEY)
     (select 'SERVER'
           , server
           , server
           , server
           , 1
           , 'F'
           , STB$OBJECT.getCurrentRepid
           , server
        from (select replace (sParameter, 'SERVER_') server
                from table (olParameter)
               where sValue = 'T'
                 and sParameter like 'SERVER_%'));
  isr$trace.debug('after insert', sql%rowcount  ,sCurrentName);
  isr$reporttype$package.sessionUpdate;

  IF STB$OBJECT.getCurrentREPORTTYPEID != 999 THEN
    oNewNode := ISR$TREE$PACKAGE.rebuildReportNode(STB$OBJECT.getCurrentRepid, STB$OBJECT.getCurrentNode);
    ISR$TREE$PACKAGE.loadNode(oNewNode, STB$OBJECT.getCurrentNode);
    ISR$TREE$PACKAGE.selectLeftNode(oNewNode);
  END IF;
  
  oErrorObj := stb$audit.openaudit ('SRAUDIT', STB$OBJECT.getCurrentTitle);
  oErrorObj := stb$audit.addAuditRecord (STB$OBJECT.getCurrentToken);
  for rGetListOfChanges in (select * from table(sServerChangeList)) loop
    oErrorObj := stb$audit.addVersionToSRAudit (rGetListOfChanges.sParameterValue, rGetListOfChanges.sParameterName);
  end loop;
  --oErrorObj := stb$audit.addVersionToSRAudit (STB$OBJECT.getCurrentTitle||' '||'Version: '||STB$OBJECT.getCurrentVersion, sChanges);
  oErrorObj := stb$audit.domtoaudit;    
  
  oErrorObj := isr$reporttype$package.checkAuditRequired(sAuditFlag);
  isr$trace.debug('sAuditFlag',  sAuditFlag, sCurrentName);

  oErrorObj := isr$reporttype$package.updateCheckSum (STB$OBJECT.getCurrentRepid);
  
  IF sAuditFlag = 'T' THEN
    IF Stb$util.checkPromptedAuditRequired('PROMPTED_REPORTAUDIT') = 'T' THEN
      RAISE exAudit;
    ELSE
      oErrorObj := Stb$audit.silentAudit;
      COMMIT;
      ISR$TREE$PACKAGE.sendSyncCall;
    END IF;
  ELSE
    isr$trace.debug('no audit required', sAuditFlag, sCurrentName);
    COMMIT;
    ISR$TREE$PACKAGE.sendSyncCall;
  END IF;

  isr$trace.stat('end','end', sCurrentName);
  return oErrorObj;
exception
  WHEN exaudit THEN
    oErrorObj.sErrorCode := 'exAudit';
    oErrorObj.handleError (stb$typedef.cnaudit, sMsg, sCurrentName, dbms_utility.format_error_stack ||stb$typedef.crlf || dbms_utility.format_error_backtrace  );
    isr$trace.stat('end','end from exceptionhandling exAudit', sCurrentName);
    RETURN oErrorObj;
  when others then
    sMsg := utd$msglib.getmsg ('exUnexpectedError', stb$security.getCurrentlanguage,  csP1 => sqlerrm(sqlcode));
    oErrorObj.handleError ( Stb$typedef.cnSeverityCritical, sMsg, sCurrentName, dbms_utility.format_error_stack ||chr(10)||chr(13)|| dbms_utility.format_error_backtrace );
    return oErrorObj;
end putParameters;

--******************************************************************************
FUNCTION getLov (sEntity IN VARCHAR2, ssuchwert IN VARCHAR2, oSelectionList OUT isr$tlrselection$list)  RETURN stb$oerror$record
is
  sCurrentName        constant varchar2(200) := $$PLSQL_UNIT||'.getLov('||sEntity||')';
  oErrorObj           stb$oerror$record := STB$OERROR$RECORD();
  sMsg   varchar2(4000);
begin
  isr$trace.stat('begin','begin', sCurrentName);
  oErrorObj := isr$reporttype$package.getLov(sEntity, ssuchwert, oSelectionList);
  isr$trace.stat('end','end', sCurrentName);
  return oErrorObj;
exception
  when others then
    sMsg := utd$msglib.getmsg ('exUnexpectedError', stb$security.getCurrentlanguage,  csP1 => sqlerrm(sqlcode));
    oErrorObj.handleError ( Stb$typedef.cnSeverityCritical, sMsg, sCurrentName, dbms_utility.format_error_stack ||chr(10)||chr(13)|| dbms_utility.format_error_backtrace );
    return oErrorObj;
end getLov;


--******************************************************************************
FUNCTION getAuditMask (olnodelist OUT stb$treenodelist)
  RETURN stb$oerror$record
IS
BEGIN
  RETURN isr$reporttype$package.getauditmask (olnodelist);
END getauditmask;


end isr$reporttype$server;