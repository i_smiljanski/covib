CREATE OR REPLACE PACKAGE BODY ISR$ACTION$LOG
IS

--**************************************************************************************************************************
PROCEDURE SetActionLog ( csAction  IN  VARCHAR2,
                         csReason  IN  VARCHAR2 )
IS
  PRAGMA AUTONOMOUS_TRANSACTION;
  sCurrentName       constant varchar2(4000) := $$PLSQL_UNIT||'.SetActionLog('||' '||')';
  sReference         STB$ACTIONLOG.REFERENCE%TYPE;
  sModuleName        VARCHAR2(31) := $$PLSQL_UNIT||'.SetActionLog';
  sMessage           VARCHAR2(4000);
BEGIN
  isr$trace.stat('begin', 'begin', sCurrentName);
  BEGIN
    -- Rule for building string for sReference
    CASE
      WHEN  Stb$object.getCurrentDocId IS NOT NULL THEN
        sReference := 'REPORTTYPEID ' || STB$OBJECT.getCurrentReportTypeId || ' ' ||
                      'REPID ' || STB$OBJECT.getCurrentRepId || ' ' ||
                      'DOCID ' || STB$OBJECT.getCurrentDocId ;
      WHEN Stb$object.getCurrentRepId  IS NOT NULL THEN
        sReference := 'REPORTTYPEID ' || STB$OBJECT.getCurrentReportTypeId || ' ' ||
                      'REPID ' || STB$OBJECT.getCurrentRepId;
      WHEN STB$OBJECT.getCurrentReportTypeId  IS NOT NULL THEN
        sReference := 'REPORTTYPEID ' || STB$OBJECT.getCurrentReportTypeId;
      ELSE
        sReference := 'NODEID ' || NVL(STB$OBJECT.getCurrentNodeId, -1) || ' ' ||
                      'NODETYPE ' || NVL(STB$OBJECT.getCurrentNodeType,-1);
    END CASE;
  EXCEPTION
    WHEN OTHERS THEN NULL;
  END;

  -- Write dataset into the action log
  INSERT INTO ISR$ACTIONLOG (entryid, timestamp, action, reason,
                             oracleusername, userno, reference, sessionid)
  VALUES ( STB$ACTIONLOG$SEQ.NEXTVAL, SYSDATE, csAction, csReason,
           STB$SECURITY.getOracleUserName(Stb$security.GetCurrentUser), Stb$security.GetCurrentUser, sReference, STB$SECURITY.getCurrentSession);
  COMMIT;
  isr$trace.stat('end', 'end', sCurrentName);
EXCEPTION
  WHEN OTHERS THEN
    sMessage := 'Cannot insert data into table ISR$ACTIONLOG. (csAction='||NVL(csAction,'(null)')||', csReason='||NVL(csReason,'(null)');
    isr$trace.error( csActionTaken   => 'error',
                     csLogEntry      => sMessage,
                     csFunctionGroup => sModuleName );
    COMMIT;
END SetActionLog;

END isr$action$log;