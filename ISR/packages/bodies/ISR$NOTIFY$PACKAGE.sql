CREATE OR REPLACE PACKAGE BODY ISR$NOTIFY$PACKAGE AS
/******************************************************************************
   NAME:       ISR$NOTIFY$PACKAGE
   PURPOSE:

   REVISIONS:
   Ver        Date        Author           Description
   ---------  ----------  ---------------  ------------------------------------
   1.0        22.07.2019      MAURERE108       1. Created this package body.
******************************************************************************/

  
  
  FUNCTION SendMail(csToken in varchar2 ,oParamListRec in  isr$paramlist$rec  )
  return STB$OERROR$RECORD 
  is
  sCurrentName   constant varchar2(4000) := $$PLSQL_UNIT||'.SendMail('||csToken||')';  
  oErrorObj               STB$OERROR$RECORD := STB$OERROR$RECORD();
  spackage                varchar2(100):=null;
  sRecipient              varchar2(1000);
  olFileList              ISR$FILE$LIST;  
  sSubject                varchar2(4000) ;                      
  sText                   varchar2(4000);
  sMsg                    varchar2(2000);
  cnJobId                 varchar2(30);
  result                  varchar2(4000);
  sValue                  varchar2(4000);
  nRet                    pls_integer;
  notifyOn                varchar2(30);

      
      
begin

  isr$trace.stat('begin',  spackage||'    '||csToken||'   '||STB$SECURITY.GetCurrentUser , sCurrentName);
  sMsg:='for token:    '||csToken;
  --list of parameters to add to mail_text
  if oParamListRec.lParamList.count() > 0 then
      for idx in oParamListRec.lParamList.first..oParamListRec.lParamList.last loop
      nRet := oParamListRec.lParamList(idx).aValue.GetVarchar2(sValue);
      result:=result||'    '||oParamListRec.lParamList(idx).sParameter ||' = '||sValue;
      isr$trace.debug('param',  oParamListRec.lParamList(idx).sParameter ||' = '||sValue , sCurrentName);
      end loop;
  end if;

  sSubject := Utd$msglib.GetMsg(csToken || '_MAIL_SUBJECT',Stb$security.GetCurrentLanguage);
  sText := Utd$msglib.GetMsg(csToken || '_MAIL_STEXT',Stb$security.GetCurrentLanguage,result); 
  
  for rGetPackageName in (select distinct handledby  from stb$contextmenu where token=csToken ) loop 
    spackage := rGetPackageName.handledby;
  end loop;
  
  olFileList := ISR$FILE$LIST();
 
  if  spackage is not null then 
      
    IF STB$UTIL.checkMethodExists('DEFINEMAILATTACHMENT', spackage) = 'T' THEN
      isr$trace.debug('begin',  'defineMailAttachment in package' , sCurrentName);
        

      execute immediate
        'DECLARE
           olocError STB$OERROR$RECORD;
         BEGIN
           olocError :='|| spackage  ||'.DEFINEMAILATTACHMENT( :1, :2,:3 );
           :4 :=  olocError;
        END;'
        using in  csToken, in  oParamListRec ,in out olFileList, out oErrorObj;

    END IF;
      
    
  end if;
  
 
  
  oErrorObj := oParamListRec.GetVerfiedParamStr('NOTIFY_ON', notifyOn); 
  
  
  -- sRecipient from ISR$NOTIFY + aktuelle UserMail    check for all user and groups that need to be notified when that token is called
  for rGetDoc in (select distinct u.EMAIL email from ISR$NOTIFY n,STB$USERLINK a, stb$user u 
      where n.USERGROUPNO=a.USERGROUPNO  and n.token =csToken and n.NOTIFY_ON=notifyOn and a.USERNO=u.USERNO
      and u.EMAIL is not null
      union
      select distinct u.EMAIL email from ISR$NOTIFY n,STB$USERLINK a, stb$user u 
      where n.USERNO=u.USERNO  and n.token =csToken and n.NOTIFY_ON=notifyOn 
      and u.EMAIL is not null
      union 
      select email
      from stb$user
      where userno = (select STB$SECURITY.GetCurrentUser from dual)
      and exists ( select userno  from isr$notify
      where userno = 0 and token= csToken and notify_on=notifyOn  )) loop   
    sRecipient := sRecipient||','||rGetDoc.email;
  end loop;
    
  isr$trace.debug('begin_user',  sRecipient||'    '||sSubject ||'    '||sText, sCurrentName);
  sMsg:=sMsg||'  for Recipient:    '||sRecipient;
  IF sRecipient is not null THEN
    oErrorObj :=isr$mail.SendMail(sRecipient,sSubject,sText,olFileList);
  END IF;
  /*union 
  select  email from stb$user where ldapusername = SYS_CONTEXT('USERENV', 'OS_USER')) loop   
        sRecipient := sRecipient||rGetDoc.email||',';
      end loop;  */   

  if oErrorObj.sSeverityCode = Stb$typedef.cnSeverityCritical then
    oErrorObj.sSeverityCode := Stb$typedef.cnSeverityWarning;
  end if;
       
  isr$trace.stat('end', 'end', sCurrentName);
  return oErrorObj;

exception
  when others then
    sMsg :=  utd$msglib.getmsg ('exUnexpectedError', stb$security.getcurrentlanguage, sqlerrm(sqlcode), sMsg);
    oErrorObj.handleError ( Stb$typedef.cnSeverityCritical, sMsg, sCurrentName, 
                            dbms_utility.format_error_stack || CHR(10) || dbms_utility.format_error_backtrace );  
    return oErrorObj;
end SendMail;


END ISR$NOTIFY$PACKAGE;