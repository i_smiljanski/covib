CREATE OR REPLACE PACKAGE BODY ISR$WIZARD$MASK
IS

 nCurrentLanguage     number        := stb$security.getcurrentlanguage;
CURSOR cGetFormInfo(cnFormNo IN number ,cnReportTypeId IN number ) IS
  SELECT entity,
         STB$Util.GetGroupEntity( w.Entity, cnReportTypeId) GroupEntity,
         masktype,
         criteriaheadline1,
         criteriaheadline2,
         criteriaheadline3
    FROM isr$report$wizard w
   WHERE maskno = cnFormNo
     AND reporttypeid = cnReportTypeId;

CURSOR cGetFormInfo1(csEntity IN varchar2) IS
  SELECT entity,
         STB$Util.GetGroupEntity( csEntity, w.reporttypeid) GroupEntity,
         masktype,
         criteriaheadline1,
         criteriaheadline2,
         criteriaheadline3,
         token_default_selection
    FROM isr$report$wizard w
   WHERE entity = csEntity
     AND reporttypeid = STB$OBJECT.GetCurrentReportTypeId;

  rGetFormInfo cGetFormInfo%ROWTYPE;
  rGetFormInfo1 cGetFormInfo1%ROWTYPE;
  rGetFormInfoMaster cGetFormInfo1%ROWTYPE;

--******************************************************************************
FUNCTION GetSList (sEntity IN STB$TYPEDEF.tLine,
                   sMasterKey IN VARCHAR2,
                   sNavigateMode IN varchar2,
                   oSelection OUT ISR$TLRSELECTION$LIST)
RETURN STB$OERROR$RECORD
IS
  --Variables
  sFlag                VARCHAR2(1);
  nRepId               NUMBER;
  sMsg            varchar2(4000);
  oErrorObj       STB$OERROR$RECORD := STB$OERROR$RECORD();
  oParamList           iSR$ParamList$Rec := iSR$ParamList$Rec(); --parameter list
  nCounter             NUMBER;
  sFound               VARCHAR2(1);
  sCurrentName      CONSTANT VARCHAR2(100) := $$PLSQL_UNIT||'.GetSList('||sEntity||')';

  -- Cursor to retrieve the master values if the master is not master detail
  CURSOR cGetMasterValues(sMasterEntity IN varchar2) IS
     SELECT cr2.KEY
       FROM ISR$CRIT cr2
       WHERE  NOT  EXISTS
              (SELECT cr1.KEY
                 FROM ISR$CRIT cr1
                WHERE cr2.KEY = cr1.MASTERKEY
                  AND cr1.repid = nRepId
                  AND cr1.entity = sEntity
              )
          AND cr2.repid = nRepId
          AND cr2.entity = sMasterEntity;

  -- Cursor to retrieve the master values if the master is master detail
  CURSOR cGetMastersMasterValues(sMasterEntity IN varchar2) IS
     SELECT cr2.masterkey KEY -- || '#@#' || cr2.KEY
       FROM ISR$CRIT cr2
       WHERE  NOT  EXISTS
              (SELECT cr1.KEY
                 FROM ISR$CRIT cr1
                WHERE cr2.KEY = cr1.MASTERKEY
                  AND cr1.repid = nRepId
                  AND cr1.entity = sEntity
              )
          AND cr2.repid = nRepId
          AND cr2.entity = sMasterEntity;

  CURSOR cIsScreenVisible IS
    SELECT CASE WHEN STB$SECURITY.checkGroupRight (maskno, STB$OBJECT.getCurrentReporttypeId) = 'T' THEN visible ELSE 'F' END
      FROM isr$report$wizard
     WHERE reporttypeid = STB$OBJECT.getCurrentReporttypeId
       AND entity = sEntity;

  sVisible VARCHAR2(1);

BEGIN
  isr$trace.stat('begin','sMasterKey: '||sMasterKey||',  sNavigateMode: '||sNavigateMode, sCurrentName);

  --Initalize the object list
  oSelection := ISR$TLRSELECTION$LIST ();

  --get current report id
  nRepId := STB$OBJECT.getCurrentRepId;

  -- Initalize sFound
  sFound :='F';

  -- if navigation mode then check if entires in ISR$CRIT, else sFound='F' and Object remains empty
  IF sNavigateMode = 'T' THEN
    oErrorObj := STB$UTIL.checkCriteria(nRepID, sEntity, sFound, oSelection);
    ISR$TRACE.debug('variable','sFound: ' ||sFound, sCurrentName);
  END IF;

  -- get Form Info
  OPEN cGetFormInfo1(sEntity);
  FETCH cGetFormInfo1 INTO rGetFormInfo1;
  CLOSE cGetFormInfo1;

  isr$trace.debug('value','rGetFormInfo1.masktype: '||rGetFormInfo1.masktype,sCurrentName);

  IF rGetFormInfo1.masktype = STB$TYPEDEF.cnMasterDetail THEN

  -- get the master form info
  OPEN cGetFormInfo1(rGetFormInfo1.GroupEntity);
  FETCH cGetFormInfo1 INTO rGetFormInfoMaster;
  CLOSE cGetFormInfo1;

    -- check if master-detail then
    -- check if the master value still exists
  nCounter :=0;
  IF rGetFormInfoMaster.masktype = STB$TYPEDEF.cnMasterDetail THEN
      FOR rGetMasterValues IN cGetMastersMasterValues(rGetFormInfo1.GroupEntity) LOOP
        nCounter := nCounter + 1;
        DELETE FROM ISR$CRIT
        WHERE repid = nRepId
        AND entity = sEntity
        AND KEY = rGetMasterValues.KEY;
        ISR$TRACE.debug_all('removed master values (master/detail)','rGetMasterValues.KEY: ' ||rGetMasterValues.KEY, sCurrentName);
      END LOOP;
  ELSE
      FOR rGetMasterValues IN cGetMasterValues(rGetFormInfo1.GroupEntity) LOOP
        nCounter := nCounter + 1;
        DELETE FROM ISR$CRIT
        WHERE repid = nRepId
        AND entity = sEntity
        AND KEY = rGetMasterValues.KEY;
        ISR$TRACE.debug_all('removed master values','rGetMasterValues.KEY: ' ||rGetMasterValues.KEY, sCurrentName);
      END LOOP;
   END IF;
  END IF;

  IF sFound = 'F' THEN
    isr$trace.debug('code position','before direct call to Collector',sCurrentName);
    -- fill Param List with values
    oParamList.AddParam('REPORTTYPEID', STB$OBJECT.getCurrentReporttypeId);
    oParamList.AddParam('REPID', STB$OBJECT.getCurrentRepid);
    -- other params here ?....
    oErrorObj := ISR$REMOTE$COLLECTOR.GetWizardList(sEntity, oParamList, oSelection, sFlag, sMasterKey);
    IF oErrorObj.sSeverityCode != STB$TYPEDEF.cnNoError THEN
      isr$trace.debug('parameter','oSelection',sCurrentName, oSelection);
      isr$trace.stat('end', 'end', sCurrentName);
      RETURN oErrorObj;
    END IF;
    isr$trace.debug('after direct call to Collector','Call of iSR$Remote$Collector.GetWizardList',sCurrentName, sys.anydata.convertCollection(oSelection));

    OPEN cIsScreenVisible;
    FETCH cIsScreenVisible INTO sVisible;
    CLOSE cIsScreenVisible;

    isr$trace.debug('values','sFlag: '||sFlag||',  sVisible: '||sVisible||',  oSelection - see LOGCLOB',sCurrentName, oSelection);

    -- default selection only for report version 0.1 or invisible screen
    IF  (      ( sFlag != 'T' OR sFlag IS NULL )
           AND ( STB$OBJECT.GetCurrentVersion = '0.1' OR STB$OBJECT.GetCurrentVersion IS NULL ) )
    OR  sVisible = 'F' THEN
      isr$trace.debug('values','STB$OBJECT.GetCurrentVersion: '||STB$OBJECT.GetCurrentVersion||',  STB$UTIL.getCurrentCustomPackage: '||STB$UTIL.getCurrentCustomPackage,sCurrentName);
      EXECUTE IMMEDIATE
        'BEGIN
           :1 := ' || STB$UTIL.getCurrentCustomPackage || '.getDefaultValues(:2,:3,:4);
         END;'
      USING OUT oErrorObj, IN OUT oSelection, IN rGetFormInfo1.token_default_selection, IN sEntity;
    END IF;
  END IF;

  isr$trace.debug('parameter','oSelection',sCurrentName, oSelection);
  isr$trace.stat('end', 'end', sCurrentName);
  RETURN oErrorObj;

EXCEPTION
  WHEN no_data_found THEN
    sMsg :=  utd$msglib.getmsg ('exNoDataFoundText', nCurrentLanguage, csP1 => sqlerrm(sqlcode));
    oErrorObj.handleError ( Stb$typedef.cnSeverityCritical, sMsg, sCurrentName,
                            substr(dbms_utility.format_error_stack ||Stb$typedef.crlf|| dbms_utility.format_error_backtrace, 1,4000)  );
    return oErrorObj;
  WHEN others THEN
    sMsg :=  utd$msglib.getmsg ('exUnexpectedError', nCurrentLanguage, csP1 => sqlerrm(sqlcode));
    oErrorObj.handleError ( Stb$typedef.cnSeverityCritical, sMsg, sCurrentName,
                            substr(dbms_utility.format_error_stack ||Stb$typedef.crlf|| dbms_utility.format_error_backtrace, 1,4000)  );
    return oErrorObj;
END GetSList;

--*****************************************************************************************************************************************
FUNCTION PutSList (sEntity IN STB$TYPEDEF.tLine, oSelection IN ISR$TLRSELECTION$LIST)
RETURN STB$OERROR$RECORD
IS
  sCurrentName   constant varchar2(4000) := $$PLSQL_UNIT||'.PutSList('||sEntity||')';
  oError                  STB$OERROR$RECORD := STB$OERROR$RECORD();
  sUserMsg                varchar2(4000);
  oErrorObj               STB$OERROR$RECORD;
BEGIN
  isr$trace.info('begin', 'begin', sCurrentName);
  -- write the valeus to the database
  oError := STB$UTIL.PutSList (sEntity, oSelection);
  isr$trace.info('end', 'end', sCurrentName);
RETURN oError;

EXCEPTION
  WHEN no_data_found THEN
    sUserMsg := UTD$MSGLIB.getMsg('NO_DATA_FOUND', STB$SECURITY.getCurrentLanguage||stb$typedef.crlf||SQLERRM);
    oErrorObj.handleError ( STB$TYPEDEF.cnSeverityCritical, sUserMsg, sCurrentName,
                            dbms_utility.format_error_stack||STB$TYPEDEF.crlf||dbms_utility.format_error_backtrace );
    RETURN oError;
  WHEN others THEN
    sUserMsg := UTD$MSGLIB.getMsg('exUnexpectedError', STB$SECURITY.getCurrentLanguage,  csP1 => sqlerrm(sqlcode));
    oErrorObj.handleError ( STB$TYPEDEF.cnSeverityCritical, sUserMsg, sCurrentName,
                            dbms_utility.format_error_stack||STB$TYPEDEF.crlf||dbms_utility.format_error_backtrace );
    RETURN oError;
END PutSList;

--*********************************************************************************************************************************
FUNCTION createAudit(xXml IN OUT XMLTYPE, sAudit OUT varchar2,nLastFinalRepId IN number, nFormNo IN number ) RETURN STB$OERROR$RECORD
IS
  sCurrentName   constant varchar2(4000) := $$PLSQL_UNIT||'.createAudit('||' '||')';
  oError          STB$OERROR$RECORD := STB$OERROR$RECORD();
  nRepId          NUMBER;
  sFound          VARCHAR2(1):='F';
  sUserMsg        varchar2(4000);

  oOldSelection        ISR$TLRSELECTION$LIST;
  oNewSelection        ISR$TLRSELECTION$LIST;

BEGIN
   isr$trace.stat('begin', 'nFormNo: '||nFormNo, sCurrentName);

   nRepId := STB$OBJECT.getCurrentRepId;

   OPEN cGetFormInfo(nFormNo,STB$OBJECT.GetCurrentReportTypeId);
   FETCH cGetFormInfo INTO rGetFormInfo;
   CLOSE cGetFormInfo;

   ISR$TRACE.debug('variable','rGetFormInfo.Entity: ' ||rGetFormInfo.Entity, sCurrentName);

   -- create object with the selected values from the criteria table
   oNewSelection := ISR$TLRSELECTION$LIST();
   oError := STB$UTIL.checkCriteria(nRepId,rGetFormInfo.Entity,sFound,oNewSelection);

   -- fill the old values
   oOldSelection := ISR$TLRSELECTION$LIST();

   ISR$TRACE.debug('variable','nLastFinalRepId.Entity: ' ||nLastFinalRepId, sCurrentName);

   IF nLastFinalRepId IS NOT NULL THEN
     oError:= STB$UTIL.checkCriteria(nLastFinalRepId,rGetFormInfo.Entity,sFound,oOldSelection);
   END IF;

   ISR$XML.CreateNode(xXml, '/AUDITFILE', 'AUDITENTRY','');
   ISR$XML.SetAttribute(xXml, '/AUDITFILE/AUDITENTRY[position()=last()]', 'TYP', 'CRITERIA');
   ISR$XML.SetAttribute(xXml, '/AUDITFILE/AUDITENTRY[position()=last()]', 'ENTITY', rGetFormInfo.Entity);

  -- call the general function to compare the selection objects
   stb$audit.setcurrentxml (xXml);
   oError := STB$AUDIT.CompareObjects(oOldSelection, oNewSelection, sAudit, rGetFormInfo.Entity);
   xXml := STB$AUDIT.GETAUDITDOMID;

   ISR$TRACE.debug('variable','sAudit: ' ||sAudit, sCurrentName);
   IF sAudit = 'F' THEN
     ISR$XML.DeleteNode(xXml, '/AUDITFILE/AUDITENTRY[@ENTITY="'|| rGetFormInfo.Entity ||'"]');
   END IF;

   isr$trace.stat('end', 'end', sCurrentName);
RETURN oError;

EXCEPTION
  WHEN others THEN
    sUserMsg := UTD$MSGLIB.getMsg('exUnexpectedError', STB$SECURITY.getCurrentLanguage,  csP1 => sqlerrm(sqlcode));
    oError.handleError ( STB$TYPEDEF.cnSeverityCritical, sUserMsg, sCurrentName,
                         dbms_utility.format_error_stack||STB$TYPEDEF.crlf||dbms_utility.format_error_backtrace );
    RETURN oError;
END createAudit;

Function InitOutputOptions RETURN stb$oerror$record
IS
  sCurrentName   constant varchar2(4000) := $$PLSQL_UNIT||'.InitOutputOptions';
  oErrorObj               STB$OERROR$RECORD := STB$OERROR$RECORD();
  sMsg                    varchar2(4000);
BEGIN
  isr$trace.info('begin', 'begin', sCurrentName);
  EXECUTE IMMEDIATE
        'BEGIN
           :1 := ' || STB$UTIL.getCurrentCustomPackage || '.InitOutputOptions;
         END;'
      USING OUT oErrorObj;

  isr$trace.stat('end', 'end', sCurrentName);
  RETURN oErrorObj;
EXCEPTION
  WHEN others THEN
    sMsg :=  utd$msglib.getmsg ('exUnexpectedError', nCurrentLanguage, csP1 => sqlerrm(sqlcode));
    oErrorObj.handleError ( Stb$typedef.cnSeverityCritical, sMsg, sCurrentName,
                            dbms_utility.format_error_stack ||Stb$typedef.crlf|| dbms_utility.format_error_backtrace );
    return oErrorObj;
END InitOutputOptions;

Function ExitOutputOptions RETURN stb$oerror$record
IS
  sCurrentName   constant varchar2(4000) := $$PLSQL_UNIT||'.ExitOutputOptions';
  oErrorObj               STB$OERROR$RECORD := STB$OERROR$RECORD();
  sMsg                    varchar2(4000);
BEGIN
  isr$trace.info('begin', 'begin', sCurrentName);
  EXECUTE IMMEDIATE
        'BEGIN
           :1 := ' || STB$UTIL.getCurrentCustomPackage || '.ExitOutputOptions;
         END;'
      USING OUT oErrorObj;

  isr$trace.stat('end', 'end', sCurrentName);
  RETURN oErrorObj;
EXCEPTION
  WHEN others THEN
    sMsg :=  utd$msglib.getmsg ('exUnexpectedError', nCurrentLanguage, csP1 => sqlerrm(sqlcode));
    oErrorObj.handleError ( Stb$typedef.cnSeverityCritical, sMsg, sCurrentName,
                            dbms_utility.format_error_stack ||Stb$typedef.crlf|| dbms_utility.format_error_backtrace );
    return oErrorObj;
END ExitOutputOptions;


END ISR$WIZARD$MASK;