CREATE OR REPLACE PACKAGE BODY Stb$security AS
  -- Variables
  nCurrentUser                  NUMBER;
  nCurrentLanguage              NUMBER;
  sIP                           VARCHAR2(255) := SYS_CONTEXT ('USERENV', 'IP_ADDRESS');
  nPort                         INTEGER;

  sCurrentUsername              VARCHAR2(50);
  sCurrentPassword              VARCHAR2(50);
  crlf                          varchar2(2)   := stb$typedef.crlf;
  
  --***************************************************************************************************************************
  PROCEDURE initUser(csIsOracleUser IN VARCHAR2)
  IS
    sCurrentName   constant varchar2(4000) := $$PLSQL_UNIT||'.initUser('||csIsOracleUser||')';
  BEGIN
    -- Never use logging for this function, because it is called before ISR$TRACE !
    SELECT u.userno, u.LANG
      INTO nCurrentUser, nCurrentLanguage
      FROM (SELECT MIN (u.userno) userno
              FROM STB$USER u
             WHERE (UPPER(u.oracleusername) = UPPER(USER)
                AND csIsOracleUser = 'T')
                OR (UPPER(u.ldapusername) =
                       (SELECT UPPER(username)
                          FROM isr$session
                         WHERE sessionid = STB$SECURITY.getCurrentSession)
                AND csIsOracleUser = 'F')) t
         , stb$user u
     WHERE t.userno = u.userno;
    ISR$TRACE.setSessionLoglevel;  -- [ISRC-1108]
  EXCEPTION WHEN OTHERS THEN
    NULL;
  END initUser;

  --*******************************************************************************************************************************
  FUNCTION GetCurrentUser
    RETURN NUMBER IS
  BEGIN
    -- Never use logging for this function, because it is called for ISR$TRACE !
    RETURN nCurrentUser;
  END GetCurrentUser;

  --*******************************************************************************************************************************
  FUNCTION GetCurrentLanguage
    RETURN NUMBER IS
  BEGIN
    RETURN nCurrentLanguage;
  END GetCurrentLanguage;

  --******************************************************************************************************************************
  FUNCTION GetCurrentSession
    RETURN NUMBER IS
  BEGIN
    -- Never use logging for this function, because it is called for ISR$TRACE !
    RETURN USERENV ('SESSIONID');
  END GetCurrentSession;

  --*************************************************************************************************************************
  FUNCTION getCurrentUsername
    RETURN VARCHAR2 IS
  BEGIN
    RETURN sCurrentUsername;
  END getCurrentUsername;

  --*************************************************************************************************************************
  FUNCTION getCurrentPassword
    RETURN VARCHAR2 IS
  BEGIN
    RETURN sCurrentPassword;
  END getCurrentPassword;

  --*************************************************************************************************************************
  PROCEDURE setCurrentUsername(csUsername IN VARCHAR2) IS
  BEGIN
    sCurrentUsername := csUsername;
  END setCurrentUsername;

  --*************************************************************************************************************************
  PROCEDURE setCurrentPassword(csPassword IN VARCHAR2) IS
  BEGIN
    sCurrentPassword := csPassword;
  END setCurrentPassword;

  ---*******************************************************************************************************************************
  FUNCTION getOracleUserName (cnUserNo IN NUMBER)
    RETURN VARCHAR2
    result_cache
    IS
    -- Never use logging for this function, because it is called for ISR$TRACE !
    sUserName   VARCHAR2(30);

    CURSOR cGetUser(ccnUserNo IN NUMBER) IS
      SELECT NVL(LDAPUSERNAME, ORACLEUSERNAME)
        FROM STB$USER
       WHERE userno = ccnUserNo;

    CURSOR cGetSessionUser1 IS
      SELECT UPPER(username)
      FROM   ISR$SESSION
      WHERE  sessionid = STB$SECURITY.getCurrentSession;

    CURSOR cGetSessionUser2 IS
      SELECT UPPER(username)
      FROM   ISR$SESSION s, STB$JOBSESSION j
      WHERE  s.sessionid = j.sessionid
      AND    oraclejobsessionid = STB$SECURITY.getCurrentSession;
  BEGIN
    OPEN cGetUser(cnUserNo);
    FETCH cGetUser INTO sUserName;
    IF cGetUser%NOTFOUND THEN
      OPEN cGetSessionUser1;
      FETCH cGetSessionUser1 INTO sUserName;
      IF cGetSessionUser1%NOTFOUND THEN
        OPEN cGetSessionUser2;
        FETCH cGetSessionUser2 INTO sUserName;
        IF cGetSessionUser2%NOTFOUND THEN
          sUserName := USER;
        END IF;
        CLOSE cGetSessionUser2;
      END IF;
      CLOSE cGetSessionUser1;
    END IF;
    CLOSE cGetUser;

    RETURN sUserName;
  END getOracleUserName;

  --***************************************************************************************************************************
  PROCEDURE initJobUser(cnUserNo IN STB$USER.USERNO%TYPE)
  IS
    sCurrentName   constant varchar2(4000) := $$PLSQL_UNIT||'.initJobUser('||cnUserNo||')';
  BEGIN
    isr$trace.stat('begin', 'begin', sCurrentName);
    SELECT us.userno, us.LANG
    INTO   nCurrentUser, nCurrentLanguage
    FROM   STB$USER us
    WHERE  us.userno = cnUserNo;
    isr$trace.stat('end', 'end', sCurrentName);
  END initJobUser;

  --*************************************************************************************************************************
  function checkContextRep( sParameter     in  varchar2,
                            sContext       in  varchar2,
                            sDocDefinition in  varchar2,
                            sMenuAllowed   out varchar2,
                            sLockMe        in  varchar2 )
    return STB$OERROR$RECORD
  is
    oErrorObj                     STB$OERROR$RECORD := STB$OERROR$RECORD();
    sCurrentName                  constant varchar2(100) := $$PLSQL_UNIT||'.checkContextRep( '||sParameter||' )';

    CURSOR cGetDocDefinition(csNodeType IN STB$CONTEXT.NODETYPE%TYPE,
                             csDocDefinition IN STB$CONTEXT.DOC_DEFINITION%TYPE) is
      select doc_definition
      from   stb$context
      where  nodetype = csNodeType
      and    doc_definition = csDocDefinition;

    rGetDocDefinition cGetDocDefinition%rowtype;

    nCondition                    NUMBER;

    CURSOR cCheckContext(csParameter IN STB$CONTEXT.PARAMETERNAME%TYPE,
                         csNodeType IN STB$CONTEXT.NODETYPE%TYPE) IS
      SELECT c.parametervalue
      FROM   STB$CONTEXT c
      WHERE  c.parametername = csParameter
      AND    c.nodetype = csNodeType
      AND    (c.reporttypeid = Stb$object.getCurrentReportTypeId OR reporttypeid IS NULL)
      AND    (c.repstatus = Stb$object.getcurrentRepStatus OR c.repstatus IS NULL)
      AND    (c.documentstatus = Stb$object.getcurrentDocStatus OR c.documentstatus IS NULL)
      AND    (  (    sLockMe = 'T'
                 AND nCondition != STB$TYPEDEF.cnStatusDeactive
                 AND c.repcondition = Stb$typedef.cnStatusActivated)
              OR (    sLockMe = 'T'
                 AND nCondition = STB$TYPEDEF.cnStatusDeactive
                 AND c.repcondition = nCondition)
              OR (    sLockMe = 'F'
                 AND c.repcondition = nCondition)
              OR c.repcondition IS NULL);

    CURSOR cGetCondition IS
      SELECT condition
      FROM   STB$REPORT
      WHERE  repid = Stb$object.getCurrentRepId;
  BEGIN
    isr$trace.stat('begin', 'sParameter: '||sParameter||'  sContext: '||sContext||'  sDocDefinition: '||sDocDefinition||'  sLockMe: '||sLockMe, sCurrentName);

    OPEN cGetDocDefinition(sContext, sDocDefinition);
    FETCH cGetDocDefinition INTO rGetDocDefinition;
    IF cGetDocDefinition%NOTFOUND THEN
      rGetDocDefinition.doc_definition := null;
    END IF;
    CLOSE cGetDocDefinition;

    OPEN cGetCondition;
    FETCH cGetCondition INTO nCondition;
    CLOSE cGetCondition;

    isr$trace.debug('status','nCondition: '||nCondition, sCurrentName);
    isr$trace.debug('status','Stb$object.getCurrentRepId: '||Stb$object.getCurrentRepId, sCurrentName);
    isr$trace.debug('status','Stb$object.getCurrentReportTypeId: '||Stb$object.getCurrentReportTypeId, sCurrentName);
    isr$trace.debug('status','Stb$object.getcurrentRepStatus: '||Stb$object.getcurrentRepStatus, sCurrentName);
    isr$trace.debug('status','Stb$object.getcurrentDocStatus: '||Stb$object.getcurrentDocStatus, sCurrentName);

    OPEN cCheckContext(sParameter, sContext);
    FETCH cCheckContext
    INTO  sMenuAllowed;
    IF cCheckContext%NOTFOUND THEN
      sMenuAllowed := 'F';
    END IF;
    CLOSE cCheckContext;

    isr$trace.stat('end', 'sMenuAllowed: ' || sMenuAllowed, sCurrentName);
    return oErrorObj;
  EXCEPTION
    WHEN OTHERS THEN
      oErrorObj.handleError ( Stb$typedef.cnSeverityCritical, utd$msglib.getmsg ('exUnexpectedError', stb$security.getCurrentlanguage,  csP1 => sqlerrm(sqlcode)),
        sCurrentName, dbms_utility.format_error_stack ||chr(10)|| dbms_utility.format_error_backtrace );
      RETURN oErrorObj;
  END checkContextRep;

  --**************************************************************************************************************************
  FUNCTION checkchildrenStatus (sParameter in VARCHAR2, sContext in VARCHAR2, sDocDefinition in VARCHAR2, sMenuAllowed IN OUT VARCHAR2) RETURN STB$OERROR$RECORD
  IS
    oErrorObj                     STB$OERROR$RECORD := STB$OERROR$RECORD();
    sCurrentName         constant varchar2(4000) := $$PLSQL_UNIT||'.checkChildrenStatus('||sParameter||')';
    nCurrentRepId                 STB$REPORT.REPID%TYPE;
    nCurrentRepStatus             STB$REPORT.STATUS%TYPE;
    nStartRepId                   STB$REPORT.REPID%TYPE;
    nLastFinalRepId               STB$REPORT.REPID%TYPE;
    nHigherStatus                 STB$REPORT.STATUS%TYPE;
    nDummy                        NUMBER;

    -- cursor to check if the current report id is the parent of another report and to get the status value
    CURSOR cGetHigherStatus (cnRepId IN NUMBER) IS
      SELECT status, repid
        FROM STB$REPORT
      START WITH repid = cnRepId
      CONNECT BY parentrepid = PRIOR repid;

    -- cursor to retrieve the root of a final version
    CURSOR cGetRoot (cnRepId IN NUMBER) IS
      SELECT MIN (repid)
        FROM STB$REPORT
      START WITH repid = cnRepId
      CONNECT BY repid = PRIOR parentrepid;

    -- cursor to retrive if a new version has been created from a final version
    CURSOR cGetRepId IS
      SELECT repid
        FROM STB$REPORT
       WHERE parentRepId = nLastFinalRepId;

    CURSOR cGetDocDefinition(csNodeType IN STB$CONTEXT.NODETYPE%TYPE,
                             csDocDefinition IN STB$CONTEXT.DOC_DEFINITION%TYPE) is
      SELECT doc_definition
        FROM stb$context
       WHERE nodetype = csNodeType
         AND doc_definition = csDocDefinition;

    rGetDocDefinition cGetDocDefinition%rowtype;
    -- cursor to check if the value is activated
    CURSOR cControl (csParameter IN STB$CONTEXT.PARAMETERNAME%TYPE,
                     csNodeType IN STB$CONTEXT.NODETYPE%TYPE,
                     csDocDefinition IN STB$CONTEXT.DOC_DEFINITION%TYPE,
                     cnChildStatus IN NUMBER,
                     cnCurrentStatus IN NUMBER) IS
      SELECT 1
        FROM STB$CONTEXT
       WHERE reporttypeid = STB$OBJECT.GetCurrentReportTypeId
         AND parametername = csParameter
         AND parametervalue = 'T'
         AND nodetype = csNodeType
         AND (doc_definition = csDocDefinition
           OR (doc_definition IS NULL
           AND NVL (csDocDefinition, 'NOT_DETERMINED') = 'NOT_DETERMINED'))
         AND repstatus = cnCurrentStatus
         AND (childrenstatus = cnChildStatus or childrenstatus is null and nStartRepId = nLastFinalRepID);
  BEGIN
    isr$trace.stat('begin', 'begin', sCurrentName);
    isr$trace.debug ('parameters',  'sParameter ' || sParameter || ' sContext ' || sContext || ' sDocDefinition ' || sDocDefinition, sCurrentName);

    nCurrentRepId := Stb$object.getCurrentRepId;
    nCurrentRepStatus := Stb$object.getCurrentRepStatus;
    isr$trace.debug ('nCurrentRepStatus',  nCurrentRepStatus, sCurrentName);

    IF nCurrentRepId IS NOT NULL THEN

      -- check if the current status is final or inspection
      IF nCurrentRepStatus = Stb$typedef.cnStatusFinal
      OR nCurrentRepStatus = Stb$typedef.cnStatusInspection THEN
        nLastFinalRepID := nCurrentRepId;
      ELSE
        -- check if a final version exists
        oErrorObj := ISR$REPORTTYPE$PACKAGE.getLastFinalVersion (nLastFinalRepID);
      END IF;
      isr$trace.debug ('nLastFinalRepID',  nLastFinalRepID, sCurrentName);

      IF nLastFinalRepID = -1 THEN
        -- there is no final version -> get the root of the subtree
        OPEN cGetRoot (nCurrentRepId);
        FETCH cGetRoot INTO nDummy;
        CLOSE cGetRoot;
        isr$trace.debug ('nDummy ',  'nDummy : ' || nDummy, sCurrentName);
        nStartRepId := nDummy;
      ELSE
        -- there is a final version  -> get the root above the final version
        -- if there is no version above get the final version as root
        OPEN cGetRepId;
        FETCH cGetRepId INTO nStartRepId;
        IF nStartRepId IS NULL THEN
          nStartRepId := nLastFinalRepID;
        END IF;
        CLOSE cGetRepId;
      END IF;
      isr$trace.debug ('nStartRepId', 'nStartRepId: ' || nStartRepId, sCurrentName);

      FOR rGetHigherStatus IN cGetHigherStatus (nStartRepId) LOOP
        nHigherStatus := rGetHigherStatus.status;
        isr$trace.debug ('nHigherStatus', nHigherStatus, sCurrentName);
        nDummy := '';

        OPEN cGetDocDefinition(sContext, sDocDefinition);
        FETCH cGetDocDefinition INTO rGetDocDefinition;
        IF cGetDocDefinition%NOTFOUND THEN
          rGetDocDefinition.doc_definition := null;
        END IF;
        CLOSE cGetDocDefinition;
        isr$trace.debug ('rGetDocDefinition.doc_definition',  rGetDocDefinition.doc_definition, sCurrentName);

        OPEN cControl (sParameter, sContext, rGetDocDefinition.doc_definition, nHigherStatus, nCurrentRepStatus);
        FETCH cControl INTO nDummy;
        CLOSE cControl;

        isr$trace.debug ('nDummy',  'nDummy: ' || nDummy, sCurrentName);
        IF nDummy IS NULL THEN
          sMenuAllowed := 'F';
          RETURN oErrorObj;
        ELSE
          sMenuAllowed := 'T';
        END IF;
      END LOOP;

    ELSE
      -- there is no report id return "True" -> no report version node
      sMenuAllowed := 'T';
    END IF;

    isr$trace.stat('end', 'sMenuAllowed :' || sMenuAllowed, sCurrentName);
    RETURN oErrorObj;
  EXCEPTION
    WHEN OTHERS THEN
      oErrorObj.handleError ( Stb$typedef.cnSeverityCritical, utd$msglib.getmsg ('exUnexpectedError', stb$security.getCurrentlanguage,  csP1 => sqlerrm(sqlcode)),
        sCurrentName, dbms_utility.format_error_stack ||chr(10)|| dbms_utility.format_error_backtrace );
      RETURN oErrorObj;
  END CheckchildrenStatus;

  --****************************************************************************************************************************
  FUNCTION SwitchRepSession (cnSessionId IN NUMBER, cnReference IN NUMBER)
    RETURN STB$OERROR$RECORD
  IS
    sCurrentName   constant varchar2(4000) := $$PLSQL_UNIT||'.SwitchRepSession('||cnSessionId||')';
    oErrorObj                     STB$OERROR$RECORD := STB$OERROR$RECORD();
    nCounter                      NUMBER := 0;

    CURSOR cGetRepIdInTitle IS
      SELECT NVL(r.repid, r1.repid) repid
       FROM stb$report r, stb$report r1
      WHERE r.srnumber (+) = r1.srnumber
        AND r.title (+) = r1.title
        AND r1.repid = cnReference;
  BEGIN
    isr$trace.stat('begin','cnSessionId : ' || cnSessionId || ' cnReference : ' || cnReference, sCurrentName);

    FOR rGetRepIdInTitle IN cGetRepIdInTitle LOOP
      nCounter := nCounter + 1;

      UPDATE STB$REPORT
         SET lockedon = SYSDATE
           , lockedby = STB$SECURITY.getCurrentUser
           , locksession = cnSessionId
           , condition = Stb$typedef.cnStatusLocked
       WHERE repid = rGetRepIdInTitle.repid
         AND (condition = Stb$typedef.cnStatusLocked
           OR condition = Stb$typedef.cnStatusActivated);

    END LOOP;

    IF nCounter > 0 THEN
      IF STB$UTIL.checkMethodExists('triggerRibbonUpdate', 'ISR$RIBBON') = 'T' THEN
        EXECUTE immediate 'BEGIN
                             ISR$RIBBON.triggerRibbonUpdate;
                           END;';
      END IF;
    END IF;

    isr$trace.stat('end', 'nCounter: '||nCounter, sCurrentName);
    RETURN oErrorObj;
  EXCEPTION
    WHEN OTHERS THEN
      oErrorObj.handleError ( Stb$typedef.cnSeverityCritical, utd$msglib.getmsg ('exUnexpectedError', stb$security.getCurrentlanguage,  csP1 => sqlerrm(sqlcode)),
        sCurrentName, dbms_utility.format_error_stack ||chr(10)|| dbms_utility.format_error_backtrace );
      RETURN oErrorObj;
  END SwitchRepSession;

  --*************************************************************************************************************************
  FUNCTION CheckLockedByMyself (cnlockSession IN NUMBER, sLockedMyself OUT VARCHAR2)
    RETURN STB$OERROR$RECORD
  IS
    sCurrentName         constant varchar2(4000) := $$PLSQL_UNIT||'.CheckLockedByMyself('||cnlockSession||')';
    oErrorObj                     STB$OERROR$RECORD := STB$OERROR$RECORD();
    nLockedInSession              NUMBER;
    nCondition                    NUMBER;

    -- Cursor to check who locked the report
    CURSOR cGetLockedSession IS
      SELECT locksession, condition
      FROM   STB$REPORT
      WHERE  repid = Stb$object.getCurrentRepId;
  BEGIN
    isr$trace.stat('begin','RepId: ' || Stb$object.getCurrentRepId, sCurrentName);

    OPEN cGetLockedSession;
    FETCH cGetLockedSession
    INTO  nLockedInSession, nCondition;
    CLOSE cGetLockedSession;

    isr$trace.debug ('nLockedinSession',  nLockedinSession, sCurrentName);
    isr$trace.debug ('nCondition',  nCondition, sCurrentName);
    isr$trace.debug ('cnLockSession',  cnLockSession, sCurrentName);

    CASE
      WHEN nLockedinSession IS NULL AND nCondition = Stb$typedef.cnStatusLocked THEN
        sLockedMyself := 'T';
      WHEN nLockedinSession IS NULL AND nCondition != Stb$typedef.cnStatusLocked THEN
        sLockedMyself := 'F';
      WHEN nLockedInSession = cnLockSession THEN
        sLockedMyself := 'T';
      ELSE
        sLockedMyself := 'F';
    END CASE;

    isr$trace.info('end','sLockedMyself : ' || sLockedMyself, sCurrentName);
    RETURN oErrorObj;
  EXCEPTION
    WHEN OTHERS THEN
      oErrorObj.handleError ( Stb$typedef.cnSeverityCritical, utd$msglib.getmsg ('exUnexpectedError', stb$security.getCurrentlanguage,  csP1 => sqlerrm(sqlcode)),
        sCurrentName, dbms_utility.format_error_stack ||chr(10)|| dbms_utility.format_error_backtrace );
      RETURN oErrorObj;
  END CheckLockedByMyself;

  --*******************************************************************************************************************************
  PROCEDURE SetCurrentLanguage (cnUserNo IN NUMBER) IS
  BEGIN
    SELECT us.LANG
    INTO   nCurrentLanguage
    FROM   STB$USER us
    WHERE  us.userno = cnUserNo;
  END SetCurrentLanguage;

  --*************************************************************************************************************************************
  FUNCTION checkGroupRight (csParameter IN VARCHAR2, sMenuAllowed OUT VARCHAR2, cnReporttypeId IN NUMBER )
    RETURN STB$OERROR$RECORD IS
    oErrorObj                     STB$OERROR$RECORD := STB$OERROR$RECORD();
    sCurrentName                  constant varchar2(100) := $$PLSQL_UNIT||'.checkGroupRight(' || csParameter || ')';
    sModule                       VARCHAR2(500);
    nRepTypeId                    NUMBER;
    nUserId                       NUMBER;

    -- cursor to retireve if  one of the groups the user belongs to has access to this privilege
    CURSOR cCheckUserRepParameter (cnRepTypeId IN NUMBER, cnUserGroupNo IN NUMBER) IS
      SELECT urp.ParameterValue, urp.plugin
        FROM STB$GROUP$REPTYPE$RIGHTS urp
       WHERE urp.reporttypeid = cnRepTypeId
         AND urp.usergroupno = cnUserGroupNo
         AND urp.parametername = csParameter
         AND (EXISTS (SELECT parametername, reporttypeid
                        FROM stb$reptypeparameter rp
                       WHERE userparameter = 'T'
                         AND parametervalue = 'T'
                         AND rp.parametername = urp.parametername
                         AND rp.reporttypeid = urp.reporttypeid)
           OR NOT EXISTS (SELECT parametername, reporttypeid
                            FROM stb$reptypeparameter rp
                           WHERE userparameter = 'T'
                             AND rp.parametername = urp.parametername
                             AND rp.reporttypeid = urp.reporttypeid))
      UNION
      SELECT a.ParameterValue, a.plugin
        FROM STB$ADMINRIGHTS a
       WHERE a.usergroupno = cnUserGroupNo
         AND a.parametername = csParameter
         AND (EXISTS (SELECT parametername
                        FROM stb$systemparameter sp
                       WHERE userparameter = 'T'
                         AND parametervalue = 'T'
                         AND sp.parametername = a.parametername)
           OR NOT EXISTS (SELECT parametername
                            FROM stb$systemparameter sp
                           WHERE userparameter = 'T'
                             AND sp.parametername = a.parametername))
      ORDER BY 1 DESC;

    -- cursor to retrieve the groups the user belongs to
    CURSOR cGetUserGroupNo (cnUserId IN NUMBER) IS
      SELECT usergroupno
        FROM STB$USERLINK
       WHERE userno = cnUserid;
  BEGIN
    isr$trace.stat('begin', 'csParameter: '||csParameter||'  cnReporttypeId: '||cnReporttypeId, sCurrentName);

    --get the current user groups of the user and the current report type id
    nUserId := Stb$security.nCurrentUser;
    nReptypeId := NVL(cnReporttypeId,Stb$object.GetCurrentReportTypeId);

    isr$trace.debug('values', 'nUserId: '||nUserId||'  nReptypeId: '||nReptypeId, sCurrentName);

    -- Initalisation
    sMenuAllowed := 'F';

    <<LoopUserGroups>>
    FOR recUserGroup IN cGetUserGroupNo (nUserId)
    LOOP
      OPEN cCheckUserRepParameter (nRepTypeId, recUserGroup.usergroupno);
      FETCH cCheckUserRepParameter INTO sMenuAllowed, sModule;
      CLOSE cCheckUserRepParameter;

      isr$trace.debug('values', 'Usergroup: '||recUserGroup.usergroupno||'  sMenuAllowed: '||sMenuAllowed||'  sModule: '||sModule, sCurrentName);

      IF sMenuAllowed = 'T' THEN
        sMenuAllowed := ISR$LICENCEFILE.ISLICENSED(sModule);
        exit LoopUserGroups;
      END IF;

    END LOOP;

    isr$trace.stat('end', 'sMenuAllowed : '||sMenuAllowed, sCurrentName);
    RETURN oErrorObj;
  EXCEPTION
    WHEN OTHERS THEN
      oErrorObj.handleError ( Stb$typedef.cnSeverityCritical, utd$msglib.getmsg ('exUnexpectedError', stb$security.getCurrentlanguage,  csP1 => sqlerrm(sqlcode)),
        sCurrentName, dbms_utility.format_error_stack ||chr(10)|| dbms_utility.format_error_backtrace );
      RETURN oErrorObj;
  END checkGroupRight;

  --*************************************************************************************************************************************
  function checkGroupRight( csParameter    in varchar2)
    return varchar2
  is
    oErrorObj                     STB$OERROR$RECORD := STB$OERROR$RECORD();
    sMenuAllowed                  varchar2(1);
  begin
    sMenuAllowed := STB$SECURITY.checkGroupRight(csParameter, Stb$object.GetCurrentReportTypeId);
    return sMenuAllowed;
  end checkGroupRight;

--*************************************************************************************************************************************
  function checkGroupRight( csParameter    in varchar2,
                            cnReporttypeId in number )
    return varchar2
   -- result_cache
  is
    oErrorObj                     STB$OERROR$RECORD := STB$OERROR$RECORD();
    sMenuAllowed                  varchar2(1);
  begin
    oErrorObj := STB$SECURITY.checkGroupRight(csParameter, sMenuAllowed, cnReporttypeId);
    return sMenuAllowed;
  end checkGroupRight;

  --*************************************************************************************************************************
  function checkRole
    return STB$OERROR$RECORD
  is
    oErrorObj                     STB$OERROR$RECORD := STB$OERROR$RECORD();
    sCurrentName                  constant varchar2(100) := $$PLSQL_UNIT||'.checkRole';
    exNoISRRole                   exception;
  begin
    isr$trace.stat('begin','begin',sCurrentName);

    if not(DBMS_SESSION.is_role_enabled (Stb$util.getSystemParameter ('ISRROLE'))) then
      raise exNoISRRole;
    end if;

    isr$trace.stat('end','end',sCurrentName);
    return oErrorObj;
  exception
    when exNoISRRole then
      oErrorObj.sErrorCode := Utd$msglib.GetMsg ('exNoISRRole', Stb$security.GetCurrentLanguage);
      oErrorObj.handleError ( Stb$typedef.cnSeverityCritical, utd$msglib.getmsg ('exNoISRRoleText', stb$security.getCurrentlanguage,  csP1 => sqlerrm(sqlcode)),
        sCurrentName, dbms_utility.format_error_stack ||chr(10)|| dbms_utility.format_error_backtrace );
      return oErrorObj;
    when others then
      oErrorObj.handleError ( Stb$typedef.cnSeverityCritical, utd$msglib.getmsg ('exUnexpectedError', stb$security.getCurrentlanguage,  csP1 => sqlerrm(sqlcode)),
        sCurrentName, dbms_utility.format_error_stack ||chr(10)|| dbms_utility.format_error_backtrace );
      return oErrorObj;
  end checkRole;

  --**********************************************************************************************************************************
  FUNCTION getTimeOut (nTimeOutInMinutes OUT NUMBER)
    RETURN STB$OERROR$RECORD
  IS
    sCurrentName   constant varchar2(4000) := $$PLSQL_UNIT||'.getTimeOut';
    oErrorObj                     STB$OERROR$RECORD := STB$OERROR$RECORD();

    CURSOR cGetTimeOut IS
      SELECT TIMEOUT
      FROM   STB$USER
      WHERE  userno = STB$SECURITY.getCurrentUser;
  BEGIN
    isr$trace.stat('begin', 'begin', sCurrentName);

    OPEN cGetTimeOut;
    FETCH cGetTimeOut
    INTO  nTimeOutInMinutes;
    CLOSE cGetTimeOut;

    isr$trace.debug ('nTimeOutInMinutes',  nTimeOutInMinutes, sCurrentName);

    IF nTimeOutInMinutes IS NULL THEN
      nTimeOutInMinutes := STB$UTIL.getSystemParameter('TIMEOUT');
    END IF;

    isr$trace.stat('end','nTimeOutInMinutes '||nTimeOutInMinutes, sCurrentName);
    RETURN oErrorObj;
  EXCEPTION
    WHEN OTHERS THEN
      oErrorObj.handleError ( Stb$typedef.cnSeverityCritical, utd$msglib.getmsg ('exUnexpectedError', stb$security.getCurrentlanguage,  csP1 => sqlerrm(sqlcode)),
        sCurrentName, dbms_utility.format_error_stack ||chr(10)|| dbms_utility.format_error_backtrace );
      RETURN oErrorObj;
  END getTimeOut;

  --*****************************************************************************************************
  FUNCTION getEsigMask (olNodeList IN OUT STB$TREENODELIST, sToken IN VARCHAR2)
    RETURN STB$OERROR$RECORD
  IS
    sCurrentName   constant varchar2(4000) := $$PLSQL_UNIT||'.getEsigMask('||sToken||')';
    oErrorObj                     STB$OERROR$RECORD := STB$OERROR$RECORD();
    olParameter                   STB$PROPERTY$LIST;
    sHasLov                       VARCHAR2 (1) := 'F';

    CURSOR cHasLov (sParameter IN VARCHAR2) IS
      SELECT DISTINCT 'T'
      FROM            isr$parameter$values
      WHERE           entity = sParameter;
  BEGIN
    isr$trace.stat('begin', 'begin', sCurrentName);

    IF (olNodeList.EXISTS (1) AND olNodeList (olNodeList.FIRST).sNodeId = 'MASK') THEN
      olParameter := olNodeList (1).tloPropertylist;
      -- Tab
      olParameter.EXTEND;
      olParameter (olParameter.COUNT ()) := STB$PROPERTY$RECORD('ESIGTAB',
                                                             utd$msglib.getmsg ('ESIGTAB', stb$security.getcurrentlanguage),
                                                             'F', 'F', 'T', 'T', 'T', 'F', 'TABSHEET');
    ELSE
      olNodeList := STB$TREENODELIST ();
      olNodeList.EXTEND;
      olNodeList (1) := STB$TREENODE$RECORD(Utd$msglib.GetMsg ('ESIGMASK_'||sToken, Stb$security.GetCurrentLanguage),
                                            '', 'ESIG', 'MASK');
      -- write the properties
      olParameter := STB$PROPERTY$LIST ();
    END IF;

    -- USER
    olParameter.EXTEND;
    olParameter (olParameter.COUNT ()) := STB$PROPERTY$RECORD('USER', Utd$msglib.GetMsg ('USER', Stb$security.GetCurrentLanguage),
                                                           null, null, null, 'T', null, 'F', 'STRING');

    IF Stb$util.getSystemParameter ('PROMPT_ESIG_USERNAME') = 'T' THEN
      olParameter (olParameter.COUNT ()).sDisplayed := 'T';
      olParameter (olParameter.COUNT ()).sValue := '';
      olParameter (olParameter.COUNT ()).sDisplay := '';
      olParameter (olParameter.COUNT ()).sEditable := 'T';
    ELSE
      olParameter (olParameter.COUNT ()).sDisplayed := 'F';
      olParameter (olParameter.COUNT ()).sValue := STB$SECURITY.getOracleUserName (STB$SECURITY.getCurrentUser);
      olParameter (olParameter.COUNT ()).sDisplay := olParameter (olParameter.COUNT ()).sValue;
      olParameter (olParameter.COUNT ()).sEditable := 'F';
    END IF;

    -- USER PASSWORD
    olParameter.EXTEND;
    olParameter (olParameter.COUNT) := STB$PROPERTY$RECORD('USERPASSWORD',
                                                           utd$msglib.getmsg ('USERPASSWORD', stb$security.getcurrentlanguage),
                                                           null, null, 'T', 'T', 'T', 'F', 'PASSWORD');
    -- ESIG RESAON
    olParameter.EXTEND;
    olParameter (olParameter.COUNT) := STB$PROPERTY$RECORD('ESIGREASON_' || sToken, Utd$msglib.GetMsg ('ESIGREASON', Stb$security.GetCurrentLanguage),
                                                           null, null, 'T', NVL(STB$UTIL.getSystemParameter('USE_ESIG_COMMENT'), 'T'),
                                                           'T', null, 'STRING');

    OPEN cHasLov (olParameter (olParameter.COUNT ()).sParameter);
    FETCH cHasLov
    INTO  sHasLov;
    IF cHasLov%NOTFOUND THEN
      sHasLov := 'F';
    END IF;
    CLOSE cHasLov;

    olParameter (olParameter.COUNT ()).hasLov := sHasLov;

    -- write the property list in the node object
    olNodeList (1).tloPropertylist := olParameter;
    isr$trace.stat('end', 'end', sCurrentName);
    RETURN (oErrorObj);
  EXCEPTION
    WHEN OTHERS THEN
      ROLLBACK;
      oErrorObj.handleError ( Stb$typedef.cnSeverityCritical, utd$msglib.getmsg ('exUnexpectedError', stb$security.getCurrentlanguage,  csP1 => sqlerrm(sqlcode)),
        sCurrentName, dbms_utility.format_error_stack ||chr(10)|| dbms_utility.format_error_backtrace );
      RETURN (oErrorObj);
  END getEsigMask;

  --*****************************************************************************************************
  FUNCTION putEsigMask (
    olParameter                 IN       STB$PROPERTY$LIST,
    sEsigAction                 IN       VARCHAR2,
    sEntity                     IN       VARCHAR2,
    sReference                  IN       VARCHAR2,
    sNodeType                   IN       VARCHAR2,
    sSuccess                    IN       VARCHAR2 DEFAULT 'T')
    RETURN STB$OERROR$RECORD
  IS
    PRAGMA AUTONOMOUS_TRANSACTION;
    sCurrentName         constant varchar2(4000) := $$PLSQL_UNIT||'.putEsigMask';
    oErrorObj                     STB$OERROR$RECORD := STB$OERROR$RECORD();

    CURSOR cGetUserInfo IS
      SELECT userno, fullname, STB$SECURITY.getOracleUserName(userno) username
      FROM   STB$USER
      WHERE  userno = Stb$security.GetCurrentUser;

    rGetUserInfo                  cGetUserInfo%ROWTYPE;

    sValue      VARCHAR2(500);
    sDisplay    VARCHAR2(500);
  BEGIN
    isr$trace.stat('begin', 'begin', sCurrentName);

    OPEN cGetUserInfo;
    FETCH cGetUserInfo
    INTO  rGetUserInfo;
    CLOSE cGetUserInfo;

    FOR i IN 1..olParameter.COUNT LOOP
      IF olParameter(i).sParameter LIKE 'ESIGREASON%' THEN
        sValue := NVL(olParameter(i).sValue,'CANCEL');
        sDisplay := NVL(olParameter(i).sDisplay, sValue);
      END IF;
    END LOOP;

    -- E-signature will be written
    INSERT INTO STB$ESIG (entryid
                        , entity
                        , REFERENCE
                        , reason
                        , reasondisplay
                        , TIMESTAMP
                        , action
                        , success
                        , isrsession
                        , ipaddress
                        , userno
                        , fullname
                        , oracleusername
                        , nodetype
                        , srnumber
                        , title)
    VALUES (SEQ$STB$ESIG.NEXTVAL
          , sEntity
          , sReference
          , sValue
          , sDisplay
          , SYSDATE
          , sEsigAction
          , sSuccess
          , Stb$security.getcurrentsession
          , Stb$security.getClientIp
          , rGetUserInfo.userno
          , rGetUserInfo.fullname
          , rGetUserInfo.username
          , sNodeType
          , Stb$object.GetCurrentSrNumber
          , Stb$object.GetCurrentTitle);

    COMMIT;
    isr$trace.stat('end', 'end', sCurrentName);
    RETURN (oErrorObj);
  EXCEPTION
    WHEN OTHERS THEN
      ROLLBACK;
      oErrorObj.handleError ( Stb$typedef.cnSeverityCritical, utd$msglib.getmsg ('exUnexpectedError', stb$security.getCurrentlanguage,  csP1 => sqlerrm(sqlcode)),
        sCurrentName, dbms_utility.format_error_stack ||chr(10)|| dbms_utility.format_error_backtrace );
      RETURN (oErrorObj);
  END putEsigMask;

  --***********************************************************************************************************************
  function checkEsigExists( sSrNumber in  varchar2,
                            sTitle    in  varchar2  default null,
                            sExists   out varchar2 )
    return STB$OERROR$RECORD
  is
    oErrorObj                     STB$OERROR$RECORD := STB$OERROR$RECORD();
    sCurrentName                  constant varchar2(100) := $$PLSQL_UNIT||'.checkEsigExists';

    cursor cCheckExists IS
      select 'T'
      from   STB$ESIG e, STB$REPORT r
      where  e.srnumber = sSrNumber
      and    (    e.title = sTitle
               or TRIM (sTitle) is null );

    cursor cCheckExistsTitle IS
      select 'T'
      from   STB$ESIG e, STB$REPORT r
      where  e.title = sTitle;

  begin
    isr$trace.stat('begin','sSrNumber: '||sSrNumber||',  sTitle:'||sTitle, sCurrentName);

    open  cCheckExists;
    fetch cCheckExists
    into  sExists;
    close cCheckExists;

    -- ISRC-545, TreenodeList design had changed, there is no sParametername = SRNUMBER in tloValueList (NB/vs)
    if nvl(sExists,'F') <> 'T' then
      open  cCheckExistsTitle;
      fetch cCheckExistsTitle
      into  sExists;
      close cCheckExistsTitle;
    end if;

    if sExists is null then
      sExists := 'F';
    end if;

    isr$trace.stat('end', 'sExists: '||sExists,sCurrentName);
    return (oErrorObj);
  exception
    when others then
      oErrorObj.handleError ( Stb$typedef.cnSeverityCritical, utd$msglib.getmsg ('exUnexpectedError', stb$security.getCurrentlanguage,  csP1 => sqlerrm(sqlcode)),
        sCurrentName, dbms_utility.format_error_stack ||chr(10)|| dbms_utility.format_error_backtrace );
      return (oErrorObj);
  END checkEsigExists;

  --*************************************************************************************************************************
  FUNCTION getClientIP
    RETURN VARCHAR2 IS
  BEGIN
    -- Never use logging for this function, because it is called for ISR$TRACE !
    RETURN sIP;
  END getClientIP;

  --*********************************************************************************************************************************
  FUNCTION getClientPort
    RETURN INTEGER AS
  BEGIN
    RETURN nPort;
  END getClientPort;

  --*********************************************************************************************************************************
  PROCEDURE setClientIP (sClientIP IN VARCHAR2) AS
  BEGIN
    sIP := sClientIP;
  END setClientIP;

  --*********************************************************************************************************************************
  PROCEDURE setClientPort (nClientPort IN INTEGER) AS
  BEGIN
    nPort := nClientPort;
  END setClientPort;

  --*********************************************************************************************************************************
  FUNCTION getCurrentGroups
  return varchar2
    result_cache
  is
    sCurrentName   constant varchar2(4000) := $$PLSQL_UNIT||'.getCurrentGroups';
    oErrorObj                     STB$OERROR$RECORD := STB$OERROR$RECORD();
    cursor cDatagroup is
      select g.UserGroupNo, g.GroupName
      from Stb$User u, Stb$UserLink l, Stb$UserGroup g
      where u.UserNo=l.UserNo and l.UserGroupNo=g.UserGroupNo
        and g.GroupType='DATAGROUP'
        and u.UserNo=Stb$security.getCurrentUser
      order by g.UserGroupNo;
    rDatagroup cDatagroup%rowtype;
    nIx number := 0;
    sGroups varchar2(500);
  begin
    isr$trace.stat('begin', 'begin', sCurrentName);

    open cDatagroup;
    fetch cDatagroup into rDatagroup;
    while cDatagroup%found loop
      nIx := nIx + 1;
      if nIx = 1 then
        sGroups := rDatagroup.GroupName;
      else
        sGroups := sGroups ||','||rDatagroup.GroupName;
      end if;
      fetch cDatagroup into rDatagroup;
    end loop;
    close cDatagroup;
    isr$trace.debug('end','end', sCurrentName);
    return sGroups;
  exception
    when others then
      oErrorObj.handleError ( Stb$typedef.cnSeverityCritical, utd$msglib.getmsg ('exUnexpectedError', stb$security.getCurrentlanguage,  csP1 => sqlerrm(sqlcode)),
        sCurrentName, dbms_utility.format_error_stack ||chr(10)|| dbms_utility.format_error_backtrace );
      raise;
  end getCurrentGroups;


  --*********************************************************************************************************************************
  FUNCTION checkDatagroups(cnCurrentUser IN NUMBER, cnUserGroup IN NUMBER)
  return varchar2
  result_cache
  is
    sCurrentName   constant varchar2(4000) := $$PLSQL_UNIT||'.checkDatagroups('||cnCurrentUser||')';
    sIsMember   VARCHAR2(1) := 'F';
    nDatagroup  NUMBER;

    cursor cGetUsersDatagroups( ccnUserGroup number ) is
      SELECT DISTINCT *
        FROM (SELECT ul.usergroupno
                FROM STB$USERLINK ul, STB$USERGROUP ug
               WHERE ul.userno = cnCurrentUser
                 AND ug.usergroupno = ul.usergroupno
                 AND ug.grouptype = 'DATAGROUP')
       WHERE ccnUserGroup = usergroupno;
  begin
    isr$trace.stat('begin', 'begin', sCurrentName);
    OPEN cGetUsersDatagroups( cnUserGroup );
    FETCH cGetUsersDatagroups INTO nDatagroup;
    IF cGetUsersDatagroups%FOUND THEN
      sIsMember := 'T';
    END IF;
    CLOSE cGetUsersDatagroups;

    isr$trace.stat('end','sIsMember '||sIsMember, sCurrentName);
    return sIsMember;
  end checkDatagroups;

--*************************************************************************************************************************
 function isLdapUser( cnUserNo number )
   return char
   result_cache
 is
   sReturn char(1);
   cursor curUser( ccnUserNo number ) is
     select case
              when count(1) > 0 then 'T'
              else 'F'
            end  isLdapUser
     from   stb$user
     where  userno = ccnUserNo
     and    ldapusername is not null;
 begin
   for rec in curUser( cnUserNo ) loop
     sReturn := rec.isLdapUser;
   end loop;
   return sReturn;
 exception
   when others then
     return 'F';
 end;


END Stb$security;