CREATE OR REPLACE package body ISR$MAIL
is

  type tAttachment is record ( blAttachment blob,
                               sFileName    varchar2(500),
                               sMimeType    varchar2(100) );

  type tlAttachments is table of tAttachment index by BINARY_INTEGER;

  cursor cJobExists(csJobName in varchar2) is
    select job_name
    from user_scheduler_jobs
    where job_name = csJobName;


--***********************************************************************************************************************************
function SendMail(csRecipient in varchar2,sSubject in varchar2,sText in varchar2,olFileList in ISR$FILE$LIST default null)
return STB$OERROR$RECORD
is
  oErrorObj               STB$OERROR$RECORD := STB$OERROR$RECORD();
  sCurrentName   constant varchar2(4000) := $$PLSQL_UNIT||'.SendMail';
  sMsg  varchar2(4000);
  clError  clob;
  exSendMail  exception;

begin

  isr$trace.stat('begin', 'begin', sCurrentName);
  oErrorObj := STB$OERROR$RECORD();
  for recMailSrv in ( select host, port, username, password
                      from   isr$server$mail$v
                      where  runstatus = 'T'  )
  loop
    isr$trace.debug('sendMail','host: '||recMailSrv.host||',  port: '||recMailSrv.port||',  username: '||recMailSrv.username||',  username: '||STB$JAVA.decryptPassword(recMailSrv.password) ,sCurrentName);
   -- , xmltype(sys.anydata.convertCollection(olFileList)).getClobVal
    clError:= SendMailJava(recMailSrv.host
                       , recMailSrv.port
                       , recMailSrv.username
                       , case when recMailSrv.password is not null then STB$JAVA.decryptPassword(recMailSrv.password) else null end    --'Rackspacu17'   --STB$JAVA.decryptPassword(recMailSrv.password)
                       , csRecipient
                       , ' '
                       , Utd$msglib.GetMsg('MAIL_SUBJECT',Stb$security.GetCurrentLanguage) || ' ' ||sSubject
                       , Utd$msglib.GetMsg('MAIL_TEXT',Stb$security.GetCurrentLanguage) || ' ' ||NVL(sText, sSubject)
                       , olFileList);



  if clError is not null then
    isr$trace.error('sendMail', 'error ', sCurrentName, clError);
    sMsg := 'host: '||recMailSrv.host||',  port: '||recMailSrv.port||',  username: '||recMailSrv.username;
    oErrorObj.handleJavaError( Stb$typedef.cnSeverityCritical,  clError, sCurrentName );
    raise exSendMail;
  end if;
  end loop;
    isr$trace.stat('end', 'end', sCurrentName);
    return oErrorObj;
exception
   when others then
    sMsg := utd$msglib.getmsg ('exUnexpectedError',Stb$security.GetCurrentLanguage, csP1 => sqlerrm(sqlcode));
    oErrorObj.handleError ( Stb$typedef.cnSeverityCritical, sMsg, sCurrentName,
                           dbms_utility.format_error_stack || CHR(10) || dbms_utility.format_error_backtrace);
    return oErrorObj;
end SendMail;


function SendEmail( csToken     in varchar2,
                    csNodeType  in varchar2,
                    csNodeId    in varchar2,
                    csNotify_on in varchar2,
                    olFileList in ISR$FILE$LIST default null )
  return STB$OERROR$RECORD
is
  sCurrentName   constant varchar2(4000) := $$PLSQL_UNIT||'.SendEmail('||csToken||')';
  oErrorObj               STB$OERROR$RECORD := STB$OERROR$RECORD();
  exNoSender              exception;
  sRecipient              varchar2(100);
  bSend                   varchar2(1) := 'F';
  sSubject VARCHAR2(4000) := csToken||'   '||csNodeType||'   '||csNodeId||'   '||csNotify_on ;
  sText    VARCHAR2(4000) := csToken||'   '||csNodeType||'   '||csNodeId||'   '||csNotify_on;


  /*cursor cGetNotifierList is
    select u.email
    from   ISR$NOTIFY n,
           STB$USER   u
    where  n.token = csToken
    and    n.userno = u.userno
    and    n.notify_on = csNotify_on;*/
    --union
   -- select u.email
   -- from   ISR$NOTIFY   n,
   --        STB$USERLINK l,
   --        STB$USER     u
   -- where  n.token = csToken
   -- and    n.usergroupno = l.usergroupno
    --and    l.userno = u.userno
    --and    n.notify_on = csNotify_on;

  cursor cSentAttachments is
    select n.attachment
    from   ISR$NOTIFY n
    where  n.token = csToken
    and    n.notify_on = csNotify_on;

  rSentAttachments cSentAttachments%rowtype;

begin

  isr$trace.debug('begin','csNodeType: '||csNodeType||',  csNodeId: '||csNodeId||', csNotify_On: '||csNotify_On, sCurrentName);

  -- check for all user and groups that need to be notified when that token is called

  for rGetNotifierList in ( select u.email ,n.token
    from   ISR$NOTIFY n,
           STB$USER   u
    where  n.token = csToken
    and    n.userno = u.userno
    and    n.notify_on = csNotify_on  ) loop
    isr$trace.debug('variable','variable: '||rGetNotifierList.email||',  token: '||rGetNotifierList.token,sCurrentName);
    bSend := 'T';
    sRecipient := sRecipient||rGetNotifierList.email||',';
  end loop;
  isr$trace.debug('variable','sRecipient, see LOGCLOB', sCurrentName, sRecipient);

  -- vs 26.Apr.2018 !!!!! DIESER CODE IST UNZUREICHEND !!!!! Es könnte z.B. eine Folge von T-F-T-T-F-F-F-F-F-T existieren.
  --                hier wird nur der erste gefundene Datensatz ausgewertet !
  open  cSentAttachments;
  fetch cSentAttachments into rSentAttachments;
  close cSentAttachments;

  /*if rSentAttachments.attachment = 'T' then
    select binaryfile,
           filename,
           mimetype
    bulk collect into attachments
    from   stb$jobtrail
    where  jobid = csNodeId
    and    (   documenttype = 'D'
            or documenttype = 'TARGET'
            or documenttype = 'ROHREPORT')
    and    DBMS_LOB.getlength(BINARYFILE) != 0;

    isr$trace.debug('variable','attachments.COUNT: '||attachments.COUNT(), sCurrentName);

  end if;  */

  isr$trace.debug('variable','bSend: '||bSend, sCurrentName);
  if bSend = 'T' then
    isr$trace.debug('variable','bSend: '||bSend, sCurrentName);
    --oErrorObj := ISR$MAIL.sendMail( sRecipient, case when attachments.COUNT() != 0 then csNodeId else '' end);
    oErrorObj :=SendMail(sRecipient,sSubject,sText,olFileList);
  end if;

  isr$trace.stat('end', 'end', sCurrentName);
  return oErrorObj;

exception
  when exNoSender then
     oErrorObj.handleError ( Stb$typedef.cnSeverityWarning, utd$msglib.getmsg ('exNoSender', stb$security.getCurrentlanguage,  csP1 => sqlerrm(sqlcode)),
        sCurrentName, dbms_utility.format_error_stack ||chr(10)|| dbms_utility.format_error_backtrace );
    return oErrorObj;
    rollback;
  when others then
    oErrorObj.handleError ( Stb$typedef.cnSeverityWarning, utd$msglib.getmsg ('exUnexpectedError', stb$security.getCurrentlanguage,  csP1 => sqlerrm(sqlcode)),
        sCurrentName, dbms_utility.format_error_stack ||chr(10)|| dbms_utility.format_error_backtrace );
    return oErrorObj;
    rollback;
end SendEmail;


 --*************************************************************************************************************************
function getMailRunStatus(nServerid in number) return varchar2  is
  sCurrentName      constant varchar2(100) := $$PLSQL_UNIT||'.getMailRunStatus';
  oErrorObj         STB$OERROR$RECORD := STB$OERROR$RECORD();
  sMsg         varchar2(4000);
  sStatus    varchar2(1):='F';
  clReturn clob;
begin
   isr$trace.stat('begin', 'begin', sCurrentName);

   return sStatus;
 exception
  WHEN OTHERS then
     sMsg :=  nvl(sMsg, utd$msglib.getmsg ('exUnexpectedError', 2, csP1 => sqlerrm(sqlcode)));
     oErrorObj.handleError ( Stb$typedef.cnSeverityCritical, sMsg, sCurrentName,
                            dbms_utility.format_error_stack || CHR(10) || dbms_utility.format_error_backtrace, SQLCODE);
    return 'F';
end getMailRunStatus;


 --*************************************************************************************************************************
FUNCTION SendMailJava(SMTP_HOST_NAME VARCHAR2, SMTP_PORT_NAME VARCHAR2, SMTP_AUTH_USER VARCHAR2, SMTP_AUTH_PWD VARCHAR2, to_user VARCHAR2, cc VARCHAR2, subject VARCHAR2, message VARCHAR2,blFileList IN ISR$FILE$LIST)  return Clob
AS LANGUAGE JAVA
NAME 'com.uptodata.isr.mail.SendMail.sendMail(java.lang.String, java.lang.String, java.lang.String,java.lang.String,java.lang.String, java.lang.String, java.lang.String,java.lang.String,java.sql.Array) return java.sql.Clob';


END ISR$MAIL;