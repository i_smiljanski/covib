CREATE OR REPLACE PACKAGE BODY                ISR$REMOTE$COLLECTOR AS

  nCurrentLanguage    number        := stb$security.getcurrentlanguage;
  csTablePrefix       VARCHAR2(10) := 'ISR$'; -- system variable
  crlf                constant varchar2(2) := chr(10)||chr(13);
  cnSysLogLevel       constant number      := isr$trace.getLogLevel;
  cnDebug             constant number      := isr$trace.getLogLevelNo('DEBUG');
  cnDebugAll          constant number      := isr$trace.getLogLevelNo('DEBUG_ALL');


  nDataSrvRunRequUndecided  constant integer := 0;
  nDataSrvRunRequAtLeastOne constant integer := 1;
  nDataSrvRunRequAll        constant integer := 2;

--******************************************************************************
function GetDataServerRunRequired(csEntity in varchar2, cnRepID in integer) return integer is
  sCurrentName   constant varchar2(4000) := $$PLSQL_UNIT||'.GetDataServerRunRequired('||csEntity||')';
  sServerMustRun varchar2(10);
  nServerMustRun integer;
  cursor cGetServerMustRun is
    select DataServerMustRun
    from iSR$Meta$Entity
    where entityname = csEntity;

begin
  ISR$TRACE.stat('begin', 'begin', sCurrentName);
  open cGetServerMustRun;
  fetch cGetServerMustRun into nServerMustRun;
  close cGetServerMustRun;
  iSR$Trace.debug(csEntity||' nServerMustRun from entity setting', nServerMustRun, sCurrentName);

  if nServerMustRun = nDataSrvRunRequUndecided then
    -- the entity has no specific setting so look for report/reptype setting
    sServerMustRun := STB$UTIL.getReportParameterValue('DATA_SERVER_MUST_RUN', cnRepID);
    iSR$Trace.debug(csEntity||' sServerMustRun from report/reptype setting', sServerMustRun, sCurrentName);
    -- check for parameter existance and convert to number
    if sServerMustRun is null then
      nServerMustRun := nDataSrvRunRequUndecided;
    else
      nServerMustRun := to_number(sServerMustRun);
    end if;

    -- then decide to look further or not
    if nvl(sServerMustRun,nDataSrvRunRequUndecided) = nDataSrvRunRequUndecided then
      -- the report/reptype as no specific setting so look for system parameter
      sServerMustRun := STB$UTIL.getSystemParameter('DATA_SERVER_MUST_RUN');
      iSR$Trace.debug(csEntity||' sServerMustRun from report/reptype setting', sServerMustRun, sCurrentName);
      -- check for parameter existance and convert to number
      if sServerMustRun is null then
        nServerMustRun := nDataSrvRunRequAtLeastOne;
      else
        nServerMustRun := to_number(sServerMustRun);
      end if;
    end if;
  end if;

  iSR$Trace.stat('end','nServerMustRun: '||nServerMustRun, sCurrentName);
  return nServerMustRun;
end GetDataServerRunRequired;

function editDataInTmpTable ( csEntity    IN VARCHAR2,  csWizard    IN VARCHAR2, csLocalEntity in VARCHAR2) return STB$oError$Record
is
  oErrorObj                  STB$OERROR$RECORD := STB$OERROR$RECORD();
  sCurrentName      CONSTANT VARCHAR2(100) := $$PLSQL_UNIT||'.editDataInTmpTable('||csEntity||')';
  sMsg                       VARCHAR2(2000);

  sStatement   VARCHAR2(4000);
  sTmpTable    varchar2(30) := STB$UTIL.GetSystemParameter('TEMP_TAB_PREFIX') || csEntity;

  cursor cGetGridSelectedEntity is
    SELECT parententityname selectedentity, 1
      FROM isr$meta$grid$entity g
         , (SELECT r.parententityname, r.childentityname, rr.reporttypeid
              FROM isr$meta$relation r, isr$meta$relation$report rr
             WHERE r.relationname = rr.relationname) t
     WHERE g.entityname = csEntity
       AND g.reporttypeid = stb$object.getCurrentReporttypeId
       AND g.columnpos = 1
       AND g.entityname = t.childentityname(+)
       AND g.reporttypeid = t.reporttypeid(+);

  rGetGridSelectedEntity   cGetGridSelectedEntity%ROWTYPE;

  cursor cGetGridCriteria(csGridSelectedEntity IN VARCHAR2) is
    SELECT 'translate('||r.childattrname||', "''", ''"'')='''||REPLACE(crit.key, '''', '"')||'''' xpath
         , r.childattrname
         , crit.key
         , crit.ordernumber
      FROM isr$crit crit
         , isr$meta$relation r
         , isr$meta$relation$report rr
     WHERE crit.entity = csGridSelectedEntity
       AND crit.repid = stb$object.getCurrentRepid
       AND r.relationname = rr.relationname
       AND rr.reporttypeid = stb$object.getCurrentReporttypeId
       AND r.parententityname = csGridSelectedEntity
       AND r.childentityname = csEntity;


  cursor cGetCriteria is
    SELECT c.key,  master.childattrname masterkey,
                crit.key critkey,
                -- for MUIB-241:
                -- do not set critmasterkey dependent on master.childattrname (with case) like in SAIB-70!
                -- correct the selection and order of master-details in the statement in GetWizardList instead
                -- see comments in issue MUIB-241
                crit.masterkey critmasterkey,
                crit.ordernumber, crit.selected
      FROM  isr$meta$crit c, isr$crit crit
         , (SELECT r.childattrname, cg.critentity
              FROM isr$meta$crit$grouping cg, isr$meta$relation r
             WHERE cg.reporttypeid = stb$object.getCurrentReporttypeId
               AND cg.relationname = r.relationname) master
     WHERE  crit.entity IN (c.critentity)
     AND  c.critentity = csEntity
     AND crit.repid = stb$object.getCurrentRepid
       AND master.critentity(+) = c.critentity;

  --rGetCriteria cGetCriteria%rowtype;
  sCriteriaFound       varchar2(1) := 'F';

  cursor cOrderExpr is
    select --Orderexpr  --> replaced with expression below for ISRC-1163 (DSIS-240) for wizard default sort
           trim(both ',' from Orderexpr||','|| (SELECT display FROM isr$meta$crit WHERE critentity = csEntity) )
    from isr$meta$entity
    where entityname = csEntity;

  sOrderExpr  isr$meta$entity.Orderexpr%type;

begin
  isr$trace.stat('begin', 'begin', sCurrentName);

  -- filter rows
  OPEN cGetGridSelectedEntity;
  FETCH cGetGridSelectedEntity INTO rGetGridSelectedEntity;

  isr$trace.debug(csEntity||' csWizard', csWizard, sCurrentName);


  -- in wizard: check the grid criteria
  case
  when  csWizard = 'T' and cGetGridSelectedEntity%FOUND then
     isr$trace.debug (csEntity||' rGetGridSelectedEntity.selectedEntity',  rGetGridSelectedEntity.selectedEntity, sCurrentName);
     if TRIM(rGetGridSelectedEntity.selectedEntity) is not null then
        isr$trace.debug(csEntity||' grid1', rGetGridSelectedEntity.selectedEntity, sCurrentName);
        FOR rGetGridCriteria IN cGetGridCriteria(rGetGridSelectedEntity.selectedEntity) LOOP
          isr$trace.debug(csEntity||' rGetGridCriteria.xpath',  rGetGridCriteria.xpath, sCurrentName);
          --ISR$XML.createNode(xXml, '/ROWSET/ROW['||rGetGridCriteria.xpath||'][not(SELECTED)]', 'SELECTED', 'T');
          -- ISR$XML.setAttribute(xXml, '/ROWSET/ROW['||rGetGridCriteria.xpath||']', 'num', rGetGridCriteria.ordernumber);
          -- this should be coded here later. Commented because of release
          /*
          sStatement :=   'update ' || sTmpTable || ' tmp set critorderby = ' || rGetGridCriteria.ordernumber ||
                               ', selected=''' || 'T'
                                ||  ''' where tmp.' || rGetGridCriteria.childattrname  || ' = q''{' || rGetGridCriteria.key || '}'''  ;
          isr$trace.debug(csEntity||'sStatement update' , sStatement, sCurrentName);
          execute immediate sStatement;*/
        END LOOP;
     else
      isr$trace.debug(csEntity||' grid2', rGetGridSelectedEntity.selectedEntity, sCurrentName);
     -- ISR$XML.createNode(xXml, '/ROWSET/ROW[not(SELECTED)]', 'SELECTED', 'T');
     end if;
-- filter rows
  else
    isr$trace.debug(csEntity||' other '||csEntity, '', sCurrentName);

    for rGetCriteria in cGetCriteria loop
      sCriteriaFound := 'T';
      sStatement :=   'update ' || sTmpTable || ' tmp set critorderby = ' || rGetCriteria.ordernumber ||
                             ', selected=''' || 'T'  --rGetCriteria.selected
                              ||  ''' where tmp.' || rGetCriteria.key  || ' = q''{' || rGetCriteria.critkey || '}''' ||
                              case when rGetCriteria.masterkey is not null then
                                ' and  tmp.' ||  rGetCriteria.masterkey || ' = q''{' || rGetCriteria.critmasterkey || '}'''
                              end ;
      isr$trace.debug(csEntity||'sStatement update' , sStatement, sCurrentName);
      execute immediate sStatement;
     -- isr$trace.debug(csEntity||' updated rows:' , sql%rowcount, sCurrentName);
    end loop;

    if  csWizard = 'F' and sCriteriaFound = 'T' then
      sStatement := 'delete from ' || sTmpTable || '  where nvl(selected, ''F'')=''F''';
      isr$trace.debug(csEntity||'sStatement delete' , sStatement, sCurrentName);
      execute immediate sStatement;
      isr$trace.debug(csEntity||' deleted rows:' , sql%rowcount, sCurrentName);
    end if;
--    END IF;
  end case;

  CLOSE cGetGridSelectedEntity;

  -- apply entityorderby for entire entity after retrieving of data from all servers
  if nvl(STB$UTIL.getSystemParameter('REMOTE_ENTITY_ORDER'), 'F') = 'F' or csLocalEntity = 'T' then
    open cOrderExpr;
    fetch cOrderExpr into sOrderExpr;
    close cOrderExpr;
    if trim(sOrderExpr) is null then
      -- no order expression specified. set it arbitrarily
      sOrderExpr := 'rowid';
    end if;

    sStatement := 'update '||sTmpTable||' me '||
                   ' set    entityorderby = ('||
                   ' with rws as ('||
                        'select row_number() over (order by '||sOrderExpr||') r , rowid from '||sTmpTable||
                      ') '||
                        'select r from rws where me.rowid = rws.rowid'||
                      ')';
    isr$trace.debug(csEntity||'sStatement for entityorderby update' , sStatement, sCurrentName);
    execute immediate sStatement;
  end if;
  isr$trace.stat('end', 'end', sCurrentName);
  return oErrorObj;

exception
  when others then
    sMsg :=  utd$msglib.getmsg ('exUnexpectedError', nCurrentLanguage, sqlerrm(sqlcode), sMsg);
    oErrorObj.handleError ( Stb$typedef.cnSeverityCritical, sMsg, sCurrentName,
                            dbms_utility.format_error_stack || CHR(10) || dbms_utility.format_error_backtrace );
    return oErrorObj;

end editDataInTmpTable;

--******************************************************************************
FUNCTION fillTmpTableForEntity ( csEntity    IN VARCHAR2,
                                 coParamList IN iSR$ParamList$Rec,
                                 csWizard    IN VARCHAR2,
                                 csMasterKey IN VARCHAR2  DEFAULT NULL )
  RETURN STB$oError$Record
IS
  oErrorObj                  STB$OERROR$RECORD := STB$OERROR$RECORD();
  sMsg                       VARCHAR2(2000);
  sCurrentName      CONSTANT VARCHAR2(100) := $$PLSQL_UNIT||'.fillTmpTableForEntity('||csEntity||')';
  sStatement                 VARCHAR2(32767);
  bEntityFound               BOOLEAN := FALSE;
  nRepID            constant integer := coParamList.GetParamNum('REPID');
  nServerMustRun             integer;
  nServersRun                integer;
  nServersDown               integer;
  exMisconfigEntity          EXCEPTION;
  oParamList                 iSR$ParamList$Rec := coParamList;

  cursor cServersDown is
  select sstp.serverid, sstp.alias, sstp.package, sstp.runstatus
     from isr$server$servertype$v sstp, isr$server$entity$mapping sem,
          (select distinct key serverid from isr$crit where entity = 'SERVER' and repid = nRepID) rs
    where sstp.serverid = rs.serverid(+)
      and (nRepID is null or sstp.serverid = rs.serverid)
      and sem.serverid = sstp.serverid
      and sstp.runstatus = 'F' -- consider only not running server
      and sem.entityname = csEntity;

  -- for each targetmodel
--  cursor cTargetModelForEntity is
--   select s.package, s.servertypeid, s.timeout, me.targetmodelid
--     from  isr$targetmodel$v me, isr$server$servertype$v s,
--          (select distinct key serverid from isr$crit where entity = 'SERVER' and repid = nRepID) rs
--    where  s.targetmodelid = me.targetmodelid
--        and s.serverid = rs.serverid(+)
--        and (nRepID is null or s.serverid = rs.serverid)
--        and s.runstatus = 'T' -- consider only running server
--        and me.entityname = csEntity;

  sCritBaseEntity varchar2(50);

  -- for each servertype
  cursor cServerTypeForEntity is
   select s.package, s.servertypeid, sum(s.timeout) as timeout
     from  isr$targetmodel$v me, isr$server$servertype$v s,
          (select distinct key serverid from isr$crit where entity = 'SERVER' and repid = nRepID) rs
    where  s.targetmodelid = me.targetmodelid
        and s.serverid = rs.serverid(+)
        and (nRepID is null or s.serverid = rs.serverid)
        and s.runstatus = 'T' -- consider only running server
        and me.entityname = sCritBaseEntity
        group by s.package, s.servertypeid;

  CURSOR cMetaEntity( cslEntity in varchar2) IS
    SELECT DISTINCT me.localEntity
                  , me.isrmanaged
                  , me.pretrigger
                  , me.posttrigger
                  , (SELECT ma.attrname
                       FROM isr$meta$attribute ma
                      WHERE ma.iskey = 'T'
                        AND ma.entityname = me.entityname)
                       keyCol
      FROM ISR$META$ENTITY me
     WHERE me.entityName = cslEntity
       AND (( me.entityName IN (SELECT DISTINCT entityName
                                  FROM ISR$SERVER$ENTITY$MAPPING m,
                                        -- 6.Jun 2019 hr [ISRC-759] limit check to servers used in report
                                       (select distinct key serverid from isr$crit where entity = 'SERVER' and repid = nRepID) rs
                                 WHERE entityname = cslEntity
                                   and m.serverid = rs.serverid(+)
                                   and (nRepID is null or m.serverid = rs.serverid))
          AND me.localentity = 'F')
         OR  (me.localentity = 'T'));

  rMetaEntity cMetaEntity%ROWTYPE;

  -- 26.Aug 2019 hr [ISRC-1162] Pre and Post entity trigger should be called for setting for the crit entity, not for the data entity
  cursor cEntityTrigger(cslEntity in varchar2) is
    select me.pretrigger
         , me.posttrigger
     from ISR$META$ENTITY me
     WHERE me.entityName = cslEntity;

  rEntityTrigger cEntityTrigger%ROWTYPE;

  cursor cCritBaseEntity is
    select entity
    from isr$meta$crit
    where critentity = csEntity;

  exNoRunningServer               exception;
  exNotAllServersRunning          exception;

  cursor cGetServerExist is
  select 'T'
  from (select distinct key serverid from isr$crit where entity = 'SERVER' and repid = nRepID) c,
       isr$server s
  where s.serverid = c.serverid(+)
    and (nRepID is null or s.serverid = c.serverid)
    and s.servertypeid = 2;

  isServerExist        varchar2(1) := 'F';
  exServerNotMatch     exception;

  exMisconfigPreProc              exception;
  exMisconfigPostProc             exception;
  exMisconfigWizardEntity         exception;

BEGIN
  isr$trace.stat('begin', 'csEntity: '||csEntity||',  csMasterKey: '||csMasterKey||',  nRepID: '||nRepID||',  csWizard: '||csWizard, sCurrentName);
  isr$trace.debug(csEntity||' paramlist', 'csEntity: '||csEntity||' see logclob', sCurrentName, sys.anydata.convertObject(coParamList));

  -- modified for DSIS-248 and ISRC-1171
  open cCritBaseEntity;
  fetch cCritBaseEntity into sCritBaseEntity;
  isr$trace.debug('sCritBaseEntity',sCritBaseEntity, sCurrentName);

  if cCritBaseEntity%NOTFOUND then
    if csWizard = 'T' then
      -- bei der Implementierung von ABBIS-72 wurde die Falsche Meldung angezeigt, neue Exception eingeführt --> ISRC-1050
      close cCritBaseEntity;
      raise exMisconfigWizardEntity;
    else
      sCritBaseEntity := csEntity;
    end if;
  end if;
  close cCritBaseEntity;

  OPEN cMetaEntity(sCritBaseEntity);
  FETCH cMetaEntity INTO rMetaEntity;
  isr$trace.debug('values','rMetaEntity --> .localEntity: '||rMetaEntity.localEntity||',  .isrManaged: '||rMetaEntity.isrManaged||'  .keyCol: '||rMetaEntity.keyCol, sCurrentName);
  IF cMetaEntity%NOTFOUND THEN
    RAISE exMisconfigEntity;
  END IF;
  CLOSE cMetaEntity;

  if sCritBaseEntity = csEntity then
    rEntityTrigger.pretrigger := rMetaEntity.pretrigger;
    rEntityTrigger.posttrigger := rMetaEntity.posttrigger;
  else
    open cEntityTrigger(csEntity);
    fetch cEntityTrigger into rEntityTrigger;
    close cEntityTrigger;
  end if;

  -- add additional infos to oParamList
  oParamList.AddParam('ENTITY', csEntity);
  oParamList.AddParam('INWIZARD', csWizard);

  --Pre-entity-processing
  <<PreEntityProcessing>>
  begin
    isr$trace.info_all(csEntity||' begin PreEntityProcessing', 'begin', sCurrentName);
    sStatement := null;
    if rEntityTrigger.pretrigger is not null then
      isr$trace.info_all(csEntity||' pretrigger ',  rEntityTrigger.pretrigger, sCurrentName);
      -- call the handler
      sStatement := 'begin :1 := '||rEntityTrigger.pretrigger||'(:2); end;';
      isr$trace.info(csEntity||' call the pretrigger', 'sStatement in LOGCLOB', sCurrentName, sStatement);
      begin
        execute immediate  sStatement
          using out oErrorObj, in oParamList;
      exception
        when others then
          sMsg := utd$msglib.getmsg('exMisconfigPreEntityProcText', nCurrentLanguage, csEntity );
          raise exMisconfigPreProc;
      end;
    end if;
    isr$trace.info_all(csEntity||' end PreEntityProcessing', 'end', sCurrentName, sStatement);
    -- no exception handling here because it is done locally at execute immediate
  end PreEntityProcessing;

  if rMetaEntity.localEntity = 'T' then   -- local entity

    oErrorObj := iSR$Local$Entity.fillTmpTableForEntity(csEntity, coParamList, csWizard, csMasterKey);

  else      --  remote entity
    isr$trace.debug(csEntity||' repid', nRepID,sCurrentName);

    open cGetServerExist;
    fetch cGetServerExist into isServerExist;
    close cGetServerExist;

    if isServerExist = 'F' then
      sMsg := utd$msglib.getmsg('exServerNotMatchText', nCurrentLanguage );
      raise exServerNotMatch;
    end if;

    -- count the running and down servers configured for the entity, regardless of the server type
    select count(case sstp.runstatus when 'T' then 1 else null end) running,
           count(case sstp.runstatus when 'F' then 1 else null end) down
    into nServersRun, nServersDown
    from isr$server$servertype$v sstp, isr$server$entity$mapping sem,
         (select distinct key serverid from isr$crit where entity = 'SERVER' and repid = nRepID) rs
      where sstp.serverid = rs.serverid(+)
        and (nRepID is null or sstp.serverid = rs.serverid)
        and sem.serverid = sstp.serverid
        and sem.entityname = sCritBaseEntity;
        -- outer join and or-cond needed for entities without repid

    for rTest in (select distinct key serverid  from isr$crit where entity = 'SERVER' and repid = nRepID) loop
      isr$trace.debug(csEntity||' serverid', rTest.serverid,sCurrentName);
    end loop;
    if nServersRun = 0 then
      -- none of the servers for the entity is running
      raise exNoRunningServer;
      --isr$trace.info(csEntity||' no running server found', 'no server configured or not running', sCurrentName);
    elsif nServersDown > 0 then
      -- although at least one server is running, not running servers exist

      -- log the not running servers anyway^. Otherwise for 'all' we could not know which is not running
      for rServersDown in cServersDown loop
        isr$trace.warn(csEntity||' server '||rServersDown.serverid||' not running', 'server '||rServersDown.alias||' not running', sCurrentName);
      end loop;

      -- check for requirement
      nServerMustRun := GetDataServerRunRequired(csEntity, nRepID);

      if nServerMustRun = nDataSrvRunRequAll then
        -- if not, then raise an error
        raise exNotAllServersRunning;
        --isr$trace.error(csEntity||' not all configured servers are running', 'not all configured servers are running', sCurrentName);
      end if;
    end if;

    -- then do the work
    for rServerTypeForEntity in cServerTypeForEntity loop
      sStatement := 'begin :1 := '||rServerTypeForEntity.package||'.fillTmpTableForEntity(:2, :3, :4, :5, :6, :7); end;';
      isr$trace.info(csEntity||'_collectData.sql', 'uses package '||rServerTypeForEntity.package, sCurrentName, sStatement);
      execute immediate  sStatement
        using out oErrorObj, in csEntity, in coParamList, in csWizard, in csMasterKey, in rServerTypeForEntity.timeout, in rServerTypeForEntity.servertypeid ;
        --isr$trace.debug('oErrorObj', 's. logclob->', sCurrentName, oErrorObj);
      bEntityFound := true;
    end loop;

     -- check oErrorObj
      IF oErrorObj.sSeverityCode!=Stb$typedef.cnNoError THEN
        RETURN oErrorObj;
      END IF;

    -- post TargetModel processing for entity
    <<PostTargetModelEntityProc>>
    declare
      cursor cTargetModel is
        select listagg(s.serverid, ',')  WITHIN GROUP (ORDER BY s.serverid) as serverlist,
               me.targetmodelid,
               me.targetmodel_name,
               me.processing_handler
         from  isr$targetmodel$v me, isr$server$servertype$v s,
              (select distinct key serverid from isr$crit where entity = 'SERVER' and repid = nRepID) rs
        where  s.targetmodelid = me.targetmodelid
            and s.serverid = rs.serverid(+)
            and (nRepID is null or s.serverid = rs.serverid)
            and s.runstatus = 'T' -- consider only running server
            and me.entityname = csEntity
            group by me.processing_handler,me.targetmodelid, me.targetmodel_name;
      oParamListModel   iSR$ParamList$Rec;
    begin
      isr$trace.info_all(csEntity||' begin PostTargetModelEntityProc', 'begin', sCurrentName);
      for rTargetModel in cTargetModel loop
        if rTargetModel.processing_handler is not null then
          oParamListModel := oParamList;
          oParamListModel.AddParam('SERVERLIST', rTargetModel.serverlist);
          oParamListModel.AddParam('TARGETMODELID', rTargetModel.targetmodelid);
          oParamListModel.AddParam('TARGETMODEL_NAME', rTargetModel.targetmodel_name);
          sStatement := null;
          isr$trace.info_all(csEntity||' Processing_Handler for target model '||rTargetModel.targetmodel_name, rTargetModel.Processing_Handler, sCurrentName);
          -- call the handler
          sStatement := 'begin :1 := '||rTargetModel.Processing_Handler||'(:2); end;';

          isr$trace.info(csEntity||' call the Processing_Handler', 'statement see logclob -->', sCurrentName, sStatement);
          begin
            execute immediate  sStatement
              using out oErrorObj, in oParamListModel;
          exception
            when others then
              sMsg := utd$msglib.getmsg('exMisconfigPostTargetModelProcText', nCurrentLanguage, csEntity );
              raise exMisconfigPostProc;
          end;

        end if;
      end loop;
      isr$trace.info_all(csEntity||' end PostTargetModelEntityProc', 'end', sCurrentName);
      -- no exception handling here because it is done locally at execute immediate
    end PostTargetModelEntityProc;

    -- temporary workaround for issue ARDIS-514
    -- because of sb$parameter.mlp_component moved after PostProcessing
    if csWizard = 'T' then
      <<WizardWhere>>
      declare
        nReporttypeid constant integer := coParamList.GetParamNum('REPORTTYPEID');
        sWherePiece ISR$REPORT$WIZARD.wherepiece%type;
        cursor cWherePiece is
          select wherepiece
          from ISR$REPORT$WIZARD
          where reporttypeid = nReporttypeid and entity = csEntity;
        cursor cGridWherePiece is
          select wherepiece
          from ISR$META$GRID$ENTITY
          where reporttypeid = nReporttypeid and entityname = csEntity;

        -- procedure to avoid duplicate code
        procedure ApplyWherePiece(csWherePiece in varchar2) is
        begin
          sStatement := 'delete from tmp$'||csEntity||' where not ('||sWherePiece||')';
          execute immediate sStatement;
          isr$trace.debug(csEntity||' deleted rows because of wherepiece', sql%rowcount, sCurrentName, sStatement);
        exception
          when others then
          -- log the error but do not record it in the error object
          isr$trace.error(csEntity||' error while using wherepiece', sWherePiece, sCurrentName, dbms_utility.format_error_stack || CHR(10) || dbms_utility.format_error_backtrace);
        end ApplyWherePiece;
      begin
        open cWherePiece;
        fetch cWherePiece into sWherePiece;
        if cWherePiece%found then
          -- entity is found in ISR$REPORT$WIZARD. Normal wizard step
          sWherePiece := trim(sWherePiece);
          if sWherePiece is not null then
            ApplyWherePiece(sWherePiece);
          end if;
        else
          -- look for grid
          open cGridWherePiece;
          fetch cGridWherePiece into sWherePiece;
          if cGridWherePiece%found and trim(sWherePiece) is not null then
            sWherePiece := trim(sWherePiece);
            ApplyWherePiece(sWherePiece);
          end if;
          close cGridWherePiece;
        end if;
        close cWherePiece;
      end WizardWhere;

    end if;

    IF  NOT bEntityFound  THEN
      NULL; /*TODO: DEcide for error or Warning */
      isr$trace.warn('WARNING:',csEntity||' not found in metadata', sCurrentName);
    END IF;

  end if;

  if oerrorobj.sseveritycode = stb$typedef.cnSeverityCritical then
    isr$trace.debug('oErrorObj.sSeverityCode', oerrorobj.sseveritycode,scurrentname);
    return oErrorObj;
  end if;


  declare
    nCount number;
  begin
    execute immediate 'select count(1) from tmp$'||csEntity
    into nCount;
    isr$trace.debug('count before MDM', nCount, scurrentname);
  end;

  --Datamanager
  oErrorObj := isr$mdm.processFixedRules( csEntity );
  oErrorObj := isr$mdm.processMdm( csEntity );

  --sort and filter the data in tmp tables
  oErrorObj := editDataInTmpTable( csEntity, csWizard, rMetaEntity.localEntity);

  if oerrorobj.sseveritycode = stb$typedef.cnSeverityCritical then
    return oErrorObj;
  end if;

  --Post-entity-processing
  <<PostEntityProcessing>>
  begin
    isr$trace.info_all(csEntity||' begin PostEntityProcessing', 'begin', sCurrentName);
    sStatement := null;
    if rEntityTrigger.posttrigger is not null then
      isr$trace.info_all(csEntity||' posttrigger ',  rEntityTrigger.posttrigger, sCurrentName);
      oParamList.AddParam('MASTERKEY', csMasterKey);          
      -- call the handler
      sStatement := 'begin :1 := '||rEntityTrigger.posttrigger||'(:2); end;';
      isr$trace.info(csEntity||' call the posttrigger', 'sStatement in LOGCLOB', sCurrentName, sStatement);
      begin
        execute immediate  sStatement
          using out oErrorObj, in oParamList;
      exception
        when others then
          sMsg := utd$msglib.getmsg('exMisconfigPostEntityProcText', nCurrentLanguage, csEntity );
          raise exMisconfigPostProc;
      end;
    end if;
    isr$trace.info_all(csEntity||' end PostEntityProcessing', 'end', sCurrentName, sStatement);
    -- no exception handling here because it is done locally at execute immediate
  end PostEntityProcessing;

  -- built the data xml for debug only if loglevel is appropriate
  -- avoid call of DBMS_XMLQUERY.GetXML if not needed
  IF cnSysLogLevel >= cnDebug THEN
    BEGIN
      isr$trace.debug(csEntity||' 20 rows from TMP table as xml', 's. in LOGCLOB', sCurrentName, DBMS_XMLQUERY.GetXML ('select * from '||STB$UTIL.GetSystemParameter('TEMP_TAB_PREFIX')||csEntity||' d where rownum < 21 order by entityorderby'));
    EXCEPTION WHEN OTHERS THEN
      NULL;
    END;
  end if;

  isr$trace.debug(csEntity||' before end', 'oErrorObj.sSeverityCode: '||oErrorObj.sSeverityCode,sCurrentName,sys.anydata.convertObject(oErrorObj));

  isr$trace.stat('end', 'end', sCurrentName);
  RETURN oErrorObj;
EXCEPTION
  when exNoRunningServer then
    sMsg := utd$msglib.getmsg('exNoRunningServerText', nCurrentLanguage, sMsg );
    oErrorObj.handleError ( Stb$typedef.cnSeverityCritical, sMsg, sCurrentName,
                            dbms_utility.format_error_stack || CHR(10) || dbms_utility.format_error_backtrace  );
    return oErrorObj;

  when exServerNotMatch then
    oErrorObj.handleError ( Stb$typedef.cnSeverityCritical, sMsg, sCurrentName,
                            dbms_utility.format_error_stack || CHR(10) || dbms_utility.format_error_backtrace  );
    return oErrorObj;

  when exMisconfigPostProc then
    -- this can be raised within the sections PostTargetModelEntityProc or PostEntityProcessing
    oErrorObj.handleError ( Stb$typedef.cnSeverityCritical, sMsg, sCurrentName,
                            dbms_utility.format_error_stack || CHR(10) || dbms_utility.format_error_backtrace  );
    return oErrorObj;

  when exNotAllServersRunning then
    sMsg := utd$msglib.getmsg('exNotAllServersRunningText', nCurrentLanguage, sMsg );
    oErrorObj.handleError ( Stb$typedef.cnSeverityCritical, sMsg, sCurrentName,
                            dbms_utility.format_error_stack || CHR(10) || dbms_utility.format_error_backtrace  );
    return oErrorObj;

  -- ISRC-1050
  when exMisconfigWizardEntity then
    sMsg := utd$msglib.getmsg('exMisconfigWizardEntity', nCurrentLanguage, csEntity );
    oErrorObj.handleError ( Stb$typedef.cnSeverityCritical, sMsg, sCurrentName,
                            dbms_utility.format_error_stack || CHR(10) || dbms_utility.format_error_backtrace  );
    return oErrorObj;

  when exMisconfigEntity THEN
    sMsg := utd$msglib.getmsg('exMisconfigEntity', nCurrentLanguage, csEntity );
    oErrorObj.handleError ( Stb$typedef.cnSeverityCritical, sMsg, sCurrentName,
                            dbms_utility.format_error_stack || CHR(10) || dbms_utility.format_error_backtrace  );
    RETURN oErrorObj;

  when others then
    sMsg :=  utd$msglib.getmsg ('exUnexpectedError', nCurrentLanguage, sqlerrm(sqlcode), sMsg);
    oErrorObj.handleError ( Stb$typedef.cnSeverityCritical, sMsg, sCurrentName,
                            dbms_utility.format_error_stack || CHR(10) || dbms_utility.format_error_backtrace );
    return oErrorObj;

END fillTmpTableForEntity;

---******************************************************************************
FUNCTION GetWizardList(csEntity IN VARCHAR2,
                       coParamList IN     iSR$ParamList$Rec,
                       oSelection IN OUT ISR$TLRSELECTION$LIST,
                       sFlag OUT VARCHAR2,
                       csMasterKey IN VARCHAR2 DEFAULT NULL,
                       bDelete BOOLEAN DEFAULT TRUE) RETURN STB$OERROR$RECORD IS
  oErrorObj      STB$OERROR$RECORD := STB$OERROR$RECORD();
  sCurrentName   constant varchar2(4000) := $$PLSQL_UNIT||'.GetWizardList('||csEntity||')';
  sMsg           VARCHAR2(2000);
  xXml           XMLTYPE;
  bDeleteTxt     varchar2(5);

  CURSOR cGetCritCols IS
    SELECT UPPER (mc.key)
         , UPPER (mc.display)
         , CASE
              WHEN ma.datatype = 'NUMBER' THEN
                 'to_char(' || UPPER (mc.info) || ')'
              WHEN ma.datatype = 'DATE' THEN
                 'to_char(' || UPPER (mc.info) || ', '''||STB$UTIL.getSystemParameter('DATEFORMAT')||''')'
              ELSE
                 UPPER (mc.info)
           END
      FROM isr$meta$crit mc, isr$meta$attribute ma
     WHERE mc.critentity = csEntity
       AND ma.entityname = mc.critentity
       AND ma.attrname = mc.info;

  sKey              VARCHAR2(500);
  sDisplay          VARCHAR2(500);
  sInfo             VARCHAR2(500);

  CURSOR cGetMasterInfo IS
    SELECT r.parententityname masterEntity
         , r.childattrname masterField
         , CASE
              WHEN c.key = r.parentattrname THEN 'KEY'
              WHEN c.display = r.parentattrname THEN 'DISPLAY'
              WHEN c.info = r.parentattrname THEN 'INFO'
           END
              masterCritField
      FROM isr$meta$crit$grouping cg, isr$meta$relation r, isr$meta$crit c
     WHERE cg.reporttypeid = STB$OBJECT.getCurrentReporttypeID
       AND cg.critentity = csEntity
       AND cg.relationname = r.relationname
       AND r.relationtype != 'ALIAS'
       AND c.critentity = r.parententityname;


  sMasterEntity     ISR$META$ENTITY.ENTITYNAME%TYPE;
  sMasterField      VARCHAR2(30);
  sMasterCritField  VARCHAR2(30);

  sStatement        VARCHAR2(4000);
  sQuery            VARCHAR2(4000);

  exDataError EXCEPTION;

  CURSOR cGetWizardAttributes IS
    SELECT CASE
             WHEN wizardattribute IN ('HIGHLIGHT', 'ICON', 'EDITABLE', 'COLOR') THEN
               wizardattribute
             ELSE
               UTD$MSGLIB.getmsg (wizardattribute , STB$SECURITY.getCurrentLanguage)
           END sParameter
         , CASE WHEN wizardattribute = 'HIGHLIGHT' THEN '''T''' ELSE attrname END sValue
         , CASE
             WHEN wizardattribute IN ('HIGHLIGHT') THEN attrname
             WHEN wizardattribute IN ('ICON', 'EDITABLE', 'COLOR') THEN 'NULL'
             ELSE '''V'''
           END sPrePresent
      FROM isr$meta$attribute
     WHERE entityname = csEntity
       AND wizardattribute IS NOT NULL
  ORDER BY orderno;

  CURSOR cGetEmptyEntries IS
    SELECT *
      FROM table(oSelection)
     WHERE TRIM(sValue) IS NULL
        OR TRIM(sDisplay) IS NULL;

  rGetEmptyEntries      cGetEmptyEntries%rowtype;

  CURSOR cGetSelectedFlag IS
    SELECT sSelected
      FROM table(oSelection)
     WHERE sSelected = 'T';

  nRepID                     INTEGER := coParamList.GetParamNum('REPID');
  nRepTypeID                 INTEGER := coParamList.GetParamNum('REPORTTYPEID');
  sIsGrid                    VARCHAR2(1) := coParamList.GetParamStr('ISGRID');

  --oParamList iSR$ParamList$Rec := iSR$ParamList$Rec(); --TODO !!!!! muss auch als Param hereinkommen. Hier nur um zu kompilieren def und noch leer!!!!

BEGIN
    if bDelete then bDeleteTxt := 'TRUE'; else bDeleteTxt := 'FALSE';  end if;
    isr$trace.stat('begin', 'csEntity: '||csEntity||',  csMasterKey: '||csMasterKey||',  bDelete: '||bDeleteTxt, sCurrentName);
    isr$trace.debug('parameter','coParamList IN',sCurrentName,coParamList);
    isr$trace.debug('parameter','oSelection  IN OUT',sCurrentName,oSelection);

    oErrorObj := fillTmpTableForEntity(csEntity, coParamList, 'T', csMasterKey);
    isr$trace.debug('oErrorObj.sSeverityCode after filling of temporary table', oErrorObj.sSeverityCode, sCurrentName, oErrorObj);

    IF oErrorObj.sSeverityCode != Stb$typedef.cnNoError THEN
      IF oErrorObj.sSeverityCode = Stb$typedef.cnSeverityCritical THEN
        oErrorObj.sSeverityCode := STB$TYPEDEF.cnWizardValidateStop;
      END IF;
      RETURN oErrorObj;

    ELSIF STB$UTIL.GetSystemParameter('CREATEDEBUGTAB') ='T' THEN
      -- [ISRC-397]  Very important: Debugging SHALL NOT COMMIT here !!!
      -- 7.Apr 2016 HR workaround for empty DBG tables due to autonomous_transaction
      -- as autonomous_transaction has been removed, DBG$WZ$ tables can not be created!
      --oErrorObj := isr$debug.createDebugTable( csSourceTable => STB$UTIL.GetSystemParameter('TEMP_TAB_PREFIX')||csEntity,
      --                                         csTargetTable => 'DBG$WZ$'||csEntity  );
      null;
    END IF;

    OPEN cGetCritCols;
    FETCH cGetCritCols INTO sKey, sDisplay, sInfo;
    CLOSE cGetCritCols;

    isr$trace.debug(csEntity||' sKey',sKey,sCurrentName);
    isr$trace.debug(csEntity||' sDisplay',sDisplay,sCurrentName);
    isr$trace.debug(csEntity||' sInfo',sInfo,sCurrentName);


    OPEN cGetMasterInfo;
    FETCH cGetMasterInfo INTO sMasterEntity, sMasterField, sMasterCritField;
    CLOSE cGetMasterInfo;

    isr$trace.debug(csEntity||' sMasterEntity',sMasterEntity,sCurrentName);
    isr$trace.debug(csEntity||' sMasterField',sMasterField,sCurrentName);
    isr$trace.debug(csEntity||' sMasterCritField',sMasterCritField,sCurrentName);
    isr$trace.debug(csEntity||' sIsGrid',sIsGrid,sCurrentName);

    isr$trace.debug(stb$object.getCurrentMasktype,'mask type',sCurrentName);

    FOR rGetWizardAttributes IN cGetWizardAttributes LOOP
      sQuery := sQuery
      || 'select '''||rGetWizardAttributes.sParameter||''' sParameter
               , '||rGetWizardAttributes.sValue||' sValue
               , '||rGetWizardAttributes.sPrePresent||' sPrepresent
               , null bBlobValue
               , '||sKey||'
            from '||STB$UTIL.GetSystemParameter('TEMP_TAB_PREFIX')||csEntity
      || ' UNION ';
    END LOOP;
    sQuery := SUBSTR(sQuery, 1, length(sQuery)-7);

   sStatement := 'BEGIN
                  SELECT ISR$OSELECTION$RECORD (sDisplay
                                              , sValue
                                              , sInfo
                                              , sSelected
                                              , nOrderNum
                                              , sMasterKey
                                              , tloAttributeList)
                    BULK COLLECT INTO :1
                    FROM (SELECT sDisplay
                               , sValue
                               , sInfo
                               , sSelected
                               , MIN (nOrderNum) nOrderNum
                               , sMasterkey
                               , '||case
                                      when TRIM(sQuery) is null then 'ISR$ATTRIBUTE$LIST ()'
                                      else 'CAST ( MULTISET (SELECT sParameter, sValue, sPrePresent, bBlobValue FROM ('||sQuery||') d1 WHERE d1.'||sKey||' = data.sValue) AS ISR$ATTRIBUTE$LIST )'
                                    end ||' tloAttributeList
                           FROM ( SELECT '||sKey||' sValue
                                       , NVL (to_char('||sDisplay||'), to_char('||sKey||')) sDisplay
                                       , NVL ('||sInfo||', '||sKey||') sInfo
                                       , NVL ( (SELECT distinct MIN(selected)
                                                  FROM isr$crit c
                                                 WHERE c.entity = '''||csEntity||'''
                                                   AND c.repid = '||NVL(STB$OBJECT.getCurrentRepid,0)||'
                                                   AND c.key = '||sKey||'
                                                   AND c.masterkey = '||case when sMasterField is not null then 'nvl(masterkeys.masterKey, '||sKey||')' else sKey end||')
                                             , TRIM(d.selected)
                                            ) sSelected
                                       , '|| case when nvl(sIsGrid, 'F') = 'T' then 'd.entityorderby '
                                                  else ' NVL(NVL((SELECT distinct MIN(ordernumber)
                                                                    FROM isr$crit c
                                                                   WHERE c.entity = '''||csEntity||'''
                                                                     AND c.repid = '||NVL(STB$OBJECT.getCurrentRepid,0)||'
                                                                     AND c.key = '||sKey||'
                                                                     AND c.masterkey = '||case when sMasterField is not null then 'nvl(masterkeys.masterKey, '||sKey||')' else sKey end||')
                                                               , TRIM(d.critorderby) ), d.entityorderby)'
                                             end || ' nOrderNum
                                       , '||case when sMasterField is not null then 'nvl(masterkeys.masterKey, '||sKey||')' else sKey end||' sMasterKey
                                    FROM '||STB$UTIL.GetSystemParameter('TEMP_TAB_PREFIX')||csEntity||' d'||
                                       case
                                          when sMasterField is not null then
                                      ', (select '||sMasterCritField||' masterValue, key masterKey, ordernumber crit_master_order
                                            from isr$crit
                                           where repid = '||STB$OBJECT.getCurrentRepid||'
                                             and entity = '''||sMasterEntity||''') masterkeys
                                   WHERE masterkeys.masterValue(+) = '||NVL(sMasterField,sKey)
                                        end ||') data
                          GROUP BY sValue
                                 , sDisplay
                                 , sInfo
                                 , sSelected
                                 , sMasterkey
                          ORDER BY 5);
                  END;';

  isr$trace.debug('variable','sStatement',sCurrentName,sStatement);

  oSelection := ISR$TLRSELECTION$LIST();

  EXECUTE IMMEDIATE sStatement USING OUT oSelection;

  isr$trace.debug('after select','oselection see logclob->',sCurrentName, oSelection);

  OPEN cGetEmptyEntries;
  FETCH cGetEmptyEntries INTO rGetEmptyEntries;

  IF cGetEmptyEntries%FOUND THEN
     isr$trace.debug(' rGetEmptyEntries',rGetEmptyEntries.svalue||';'||rGetEmptyEntries.sdisplay||';'||rGetEmptyEntries.sinfo, sCurrentName);
    CLOSE cGetEmptyEntries;
    RAISE exDataError;
  END IF;
  CLOSE cGetEmptyEntries;

  OPEN cGetSelectedFlag;
  FETCH cGetSelectedFlag INTO sFlag;
  CLOSE cGetSelectedFlag;

  --callback to custom package
  isr$trace.debug('before callback', oSelection.count, sCurrentName);
  -- ISRC-764: Add csMasterKey to the parameter list of SetWizardAttributes
  execute immediate
    'BEGIN
       :1 :='|| STB$UTIL.getCurrentCustomPackage||'.SetWizardAttributes(:2, :3, :4);
     END;'
  USING out oErrorObj, IN OUT oSelection, in csEntity, in csMasterKey;
  isr$trace.debug('after callback',oSelection.count, sCurrentName, sys.anydata.convertObject(oErrorObj));

  IF cnSysLogLevel >= cnDebugAll THEN
    FOR i IN 1..oSelection.COUNT() LOOP
      isr$trace.debug_all(csEntity||'_oSelection'||i||'.xml',DBMS_LOB.getLength(ISR$XML.XMLToClob(XMLTYPE(oSelection(i)))), sCurrentName,ISR$XML.XMLToClob(XMLTYPE(oSelection(i))));
    END LOOP;
  END IF;

  FOR rGetBlobData IN (select attrname from isr$meta$attribute where entityname = csEntity and datatype ='BLOB') LOOP
    FOR i IN 1..oSelection.COUNT() LOOP

        oSelection(i).tloAttributeList.EXTEND;
        oSelection(i).tloAttributeList(oSelection(i).tloAttributeList.COUNT()) := ISR$ATTRIBUTE$RECORD();
        oSelection(i).tloAttributeList(oSelection(i).tloAttributeList.COUNT()).sParameter := rGetBlobData.attrname;
        isr$trace.debug_all(csEntity||' oSelection(i).sValue',oSelection(i).sValue||'; '||oSelection(i).sselected, sCurrentName);
        sQuery := 'select ' || rGetBlobData.attrname ||' from ' || STB$UTIL.GetSystemParameter('TEMP_TAB_PREFIX')|| csEntity ||' where '
            ||sKey||' = ''' || oSelection(i).sValue ||'''';
          --   and ' || sMasterField || ' = ''' || oSelection(i).sMasterKey ||'''';
      --  isr$trace.setLog(csEntity||' sQuery',sQuery,250);

        EXECUTE IMMEDIATE sQuery
          INTO oSelection(i).tloAttributeList(oSelection(i).tloAttributeList.COUNT()).bBlobValue;

      END LOOP;

  END LOOP;


  IF bDelete THEN
    EXECUTE IMMEDIATE  'BEGIN
                          DELETE FROM '||STB$UTIL.GetSystemParameter('TEMP_TAB_PREFIX')||csEntity||';
                        END;';
    isr$trace.debug(csEntity||' delete from tab', 'Yes!', sCurrentName);
  else
    isr$trace.debug(csEntity||' do not delete from tab', 'No!', sCurrentName);
  END IF;

  --isr$trace.debug(csEntity||' before end', 'oErrorObj.sSeverityCode: '||oErrorObj.sSeverityCode,sCurrentName,sys.anydata.convertObject(oErrorObj));
  isr$trace.debug('parameter','oSelection  IN OUT',sCurrentName,oSelection);
  isr$trace.debug('status','oErrorObj before end',sCurrentName,oErrorObj);
  isr$trace.stat('end', 'sFlag: '||sFlag, sCurrentName);
  RETURN oErrorObj;

EXCEPTION
  WHEN exDataError THEN
    sMsg := utd$msglib.getmsg('exDataErrorText', nCurrentLanguage, sMsg );
    oErrorObj.handleError ( Stb$typedef.cnSeverityWarning, sMsg, sCurrentName,
                            dbms_utility.format_error_stack || CHR(10) || dbms_utility.format_error_backtrace);
    return oErrorObj;
  WHEN others THEN
    sMsg :=  utd$msglib.getmsg ('exUnexpectedError', nCurrentLanguage, sqlerrm(sqlcode), sMsg);
    oErrorObj.handleError ( Stb$typedef.cnSeverityCritical, sMsg, sCurrentName,
                            dbms_utility.format_error_stack || CHR(10) || dbms_utility.format_error_backtrace );
    return oErrorObj;
END GetWizardList;

--******************************************************************************
FUNCTION getList(csEntity IN VARCHAR2, coParamList IN iSR$ParamList$Rec, bDelete BOOLEAN DEFAULT TRUE) RETURN ISR$TLRSELECTION$LIST
is
  oErrorObj       STB$OERROR$RECORD := STB$OERROR$RECORD();
  sCurrentName    constant varchar2(4000) := $$PLSQL_UNIT||'.getList('||csEntity||')';
  olSelectionList ISR$TLRSELECTION$LIST := ISR$TLRSELECTION$LIST();
  sFlag           VARCHAR2(1);
BEGIN
  isr$trace.stat('begin', 'begin', sCurrentName);
  oErrorObj := getWizardList(csEntity, coParamList, olSelectionList, sFlag, null, bDelete);
  isr$trace.stat('end', 'end', sCurrentName);
  RETURN olSelectionList;
END getList;

END ISR$REMOTE$COLLECTOR;