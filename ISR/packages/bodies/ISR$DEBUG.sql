CREATE OR REPLACE package body isr$debug
is
  TABLE_DOES_NOT_EXIST    exception;
  pragma exception_init(  TABLE_DOES_NOT_EXIST, -942);
  
  nCurrentLanguage        constant number       := stb$security.getCurrentLanguage;  

--*********************************************************************************************************************************  
function createDebugTable( csSourceTable  varchar2,
                           csTargetTable  varchar2,
                           csTablespace   varchar2 default null )
  return STB$OERROR$RECORD
is
  --pragma    autonomous_transaction;
  -- 7.Apr 2016 HR workaround for empty DBG tables 
  sCurrentName           constant varchar2(100) := $$PLSQL_UNIT||'.createDebugTable('||csTargetTable||')';
  oErrorObj              STB$OERROR$RECORD := STB$OERROR$RECORD();    -- [ISRC-721]
  sMsg                   varchar2(4000);
  sSql                   varchar2(32000);
  sTablespace            varchar2(30);
begin
  isr$trace.stat('begin', 'csSourceTable: '||csSourceTable||'  csTargetTable: '||csTargetTable||'  csTablespace: '||csTablespace, sCurrentName ); 

  sTablespace := nvl(csTablespace, stb$system.getNologTablespace );

  sSql := 'drop table '||csTargetTable||' purge';
  isr$trace.debug('sSql', sSql, sCurrentName ); 
  begin
    execute immediate sSql; 
  exception
    when TABLE_DOES_NOT_EXIST then 
    -- nothing to do, accept this exeption 
    null;
  end;
  
  sSql := 'create table '||csTargetTable||' tablespace '||sTablespace||' as select * from '||csSourceTable ;
  isr$trace.debug('sSql', sSql, sCurrentName ); 
  execute immediate sSql;
    
  isr$trace.stat('end', 'end', sCurrentName );  
  return oErrorObj;
exception
  when others then
    sMsg :=  utd$msglib.getmsg ('exUnexpectedError', nCurrentLanguage, csP1 => sqlerrm(sqlcode));
    oErrorObj.handleError ( Stb$typedef.cnSeverityCritical, sMsg, sCurrentName, 
                            substr(dbms_utility.format_error_stack || CHR(10) || dbms_utility.format_error_backtrace, 1,4000)  );  
    return oErrorObj;  
end createDebugTable;


end isr$debug;