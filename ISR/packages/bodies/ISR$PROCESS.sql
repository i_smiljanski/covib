CREATE OR REPLACE PACKAGE BODY ISR$PROCESS
is

  sHost VARCHAR2(500); 
  nPort NUMBER;

  CURSOR cGetProcessServer IS 
    SELECT host, to_number(port)
      FROM isr$server
     WHERE runstatus = 'T'
       AND servertypeid = 4;

  -- ***************************************************************************
  FUNCTION getSystemProperty(csHost IN varchar2, cnPort IN number, sPropertyName in VARCHAR2) RETURN VARCHAR2
  AS LANGUAGE JAVA
  NAME 'com.uptodata.isr.process.server.db.ProcessServer.getSystemProperty(java.lang.String, int, java.lang.String) return java.lang.String';

  -- ***************************************************************************
  PROCEDURE saveBlob(csHost IN varchar2, cnPort IN number, blob IN BLOB, path IN VARCHAR2, file in VARCHAR2)
  AS LANGUAGE JAVA
  NAME 'com.uptodata.isr.process.server.db.ProcessServer.saveBlob(java.lang.String, int, java.sql.Blob, java.lang.String, java.lang.String)';

  -- ***************************************************************************
  FUNCTION loadBlob(csHost IN varchar2, cnPort IN number, sFileName in VARCHAR2) RETURN BLOB
  AS LANGUAGE JAVA
  NAME 'com.uptodata.isr.process.server.db.ProcessServer.loadBlob(java.lang.String, int, java.lang.String) return java.sql.Blob';

  -- ***************************************************************************
  PROCEDURE deleteFile(csHost IN varchar2, cnPort IN number, sFile in VARCHAR2)
  AS LANGUAGE JAVA
  NAME 'com.uptodata.isr.process.server.db.ProcessServer.deleteFile(java.lang.String, int, java.lang.String)';

  -- ***************************************************************************
  FUNCTION RUN_CMD(csHost IN varchar2, cnPort IN number, p_cmd in VARCHAR2) RETURN CLOB
  as LANGUAGE JAVA
  NAME 'com.uptodata.isr.process.server.db.ProcessServer.runThis(java.lang.String, int, java.lang.String) return java.sql.Clob';
  
  -- ***************************************************************************
  FUNCTION getProcesses(csHost IN varchar2, cnPort IN number) RETURN NUMBER
  as LANGUAGE JAVA
  NAME 'com.uptodata.isr.process.server.db.ProcessServer.getProcesses(java.lang.String, int) return int';
  
  -- ***************************************************************************
  FUNCTION getJavaExe(csHost IN varchar2, cnPort IN number) RETURN VARCHAR2 IS
    sCurrentName   constant varchar2(4000) := $$PLSQL_UNIT||'.getJavaExe('||csHost||','||cnPort||')';
    sBootPath      VARCHAR2(500);
    sFileSep       VARCHAR2(1);
  BEGIN
    isr$trace.stat('begin', 'begin', sCurrentName);
    sBootPath := getSystemProperty(csHost, cnPort, 'sun.boot.library.path');
    sFileSep  := getSystemProperty(csHost, cnPort, 'file.separator');
    isr$trace.stat('end', 'end', sCurrentName);  
    RETURN SUBSTR ( sBootPath
                  , 0
                  , INSTR (LOWER (sBootPath), 'jre')
                    + INSTR (
                         SUBSTR (sBootPath
                               , INSTR (LOWER (sBootPath), 'jre'))
                       , sFileSep
                      )
                    - 1
                 )
                 || 'bin'
                 || sFileSep
                 || 'java';  
  END;

  -- ***************************************************************************
  FUNCTION getUserHome(csHost IN varchar2, cnPort IN number) RETURN VARCHAR2 IS
  BEGIN
    return getSystemProperty(csHost, cnPort, 'user.dir')||getSystemProperty(csHost, cnPort, 'file.separator');
  END;  
  
  -- ***************************************************************************
  FUNCTION getFileSeparator(csHost IN varchar2, cnPort IN number) RETURN VARCHAR2 IS
  BEGIN
    return getSystemProperty(csHost, cnPort, 'file.separator');
  END;
  
  -- ***************************************************************************
  FUNCTION checkProcessServer(csHost IN varchar2, cnPort IN number) RETURN VARCHAR2 IS
  BEGIN
    return case when csHost is null or cnPort is null then 'F' else 'T' end;
  END;    
  
  -- ***************************************************************************
  FUNCTION sSessionPath(csHost IN varchar2, cnPort IN number) RETURN VARCHAR2 IS
  BEGIN
    return 'external_java'||ISR$PROCESS.getFileSeparator(csHost, cnPort)||STB$SECURITY.getCurrentSession||ISR$PROCESS.getFileSeparator(csHost, cnPort);
  END;       
  
  -- ***************************************************************************
  -- ***************************************************************************
  PROCEDURE saveBlob(blob IN BLOB, path IN VARCHAR2, file in VARCHAR2) IS
  BEGIN
    saveBlob(sHost, nPort, blob, path, file);
  END;

  -- ***************************************************************************
  FUNCTION loadBlob(sFileName IN VARCHAR2) RETURN BLOB IS
  BEGIN
    RETURN loadBlob(sHost, nPort, sFileName);
  END;

  -- ***************************************************************************
  PROCEDURE deleteFile(sFile in VARCHAR2) IS
  BEGIN
    deleteFile(sHost, nPort, sFile);
  END;

  -- ***************************************************************************
  FUNCTION RUN_CMD(p_cmd in VARCHAR2) RETURN CLOB IS
  BEGIN
    RETURN RUN_CMD(sHost, nPort, p_cmd);
  END; 
  
  -- ***************************************************************************
  FUNCTION getProcesses RETURN NUMBER IS
  BEGIN
    return getProcesses(sHost, nPort);
  END;  

  -- ***************************************************************************
  FUNCTION getJavaExe RETURN VARCHAR2 IS
    sCurrentName   constant varchar2(4000) := $$PLSQL_UNIT||'.getJavaExe';
    sBootPath      VARCHAR2(500);
    sFileSep       VARCHAR2(1);
  BEGIN
    isr$trace.stat('begin', 'begin', sCurrentName);
    sBootPath := getSystemProperty(sHost, nPort, 'sun.boot.library.path');
    sFileSep  := getSystemProperty(sHost, nPort, 'file.separator');
    isr$trace.stat('end', 'end', sCurrentName);
    RETURN SUBSTR ( sBootPath
                  , 0
                  , INSTR (LOWER (sBootPath), 'jre')
                    + INSTR (
                         SUBSTR (sBootPath
                               , INSTR (LOWER (sBootPath), 'jre'))
                       , sFileSep
                      )
                    - 1
                 )
                 || 'bin'
                 || sFileSep
                 || 'java';  
  END;

  -- ***************************************************************************
  FUNCTION getUserHome RETURN VARCHAR2 IS
  BEGIN
    return getUserHome(sHost, nPort);
  END;
  
  -- ***************************************************************************
  FUNCTION getFileSeparator RETURN VARCHAR2 IS
  BEGIN
    return getFileSeparator(sHost, nPort);
  END;  
  
  -- ***************************************************************************
  FUNCTION checkProcessServer RETURN VARCHAR2 IS
  BEGIN
    return checkProcessServer(sHost, nPort);
  END;    
  
  -- ***************************************************************************
  FUNCTION sSessionPath RETURN VARCHAR2 IS
  BEGIN
    return sSessionPath(sHost, nPort);
  END;  
    
BEGIN

  OPEN cGetProcessServer;
  FETCH cGetProcessServer INTO sHost, nPort;
  CLOSE cGetProcessServer; 
  
END ISR$PROCESS;