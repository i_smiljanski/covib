CREATE OR REPLACE package body iSR$RemoteSnapshot as

  -- local constants and variables
  nCurrentLanguage constant number        := stb$security.getcurrentlanguage;
  sDateFormat      constant varchar2(50)  := stb$typedef.csInternalDateFormat;
  csTempTabPrefix  constant varchar2(500) := STB$UTIL.GetSystemParameter('TEMP_TAB_PREFIX');

  sSaveNLSDateFormat   nls_session_parameters.value%type;
  sSaveNLSNumChars     nls_session_parameters.value%type; 
  
  cursor cEntities is
    -- current report entities -used in different functions
    select distinct entityname
    from isr$collector$entities
    order by 1;

-- ****************************************************************************************************
-- local functions and procedures
-- ****************************************************************************************************

function BulidTableXML(csTableName in varchar2) return xmltype is
-- ****************************************************************************************************
-- Date and Autor: 25.Aug 2015 HR
--  local function builds an XML containing the data in the table and return it in the xmltype out parameter 
--  
-- Parameter :
-- csTableName   the table name (full name, not only entityname)     
--
-- Return:
--  xmltype - the resulting xml with the data
-- ***************************************************************************************************

  sCurrentName constant varchar2(100) := $$PLSQL_UNIT||'.BulidTableXML';    
    
  ctxContext            dbms_xmlgen.ctxType;
  xTableXML             xmltype;
  sSelStr               varchar2(32000);
  nRows                 number;
  cursor cCols is 
    -- column names and data types
    select column_name, data_type 
    from user_tab_columns 
    where table_name = upper(csTableName)
    order by column_id;   
    
begin      
  isr$trace.stat ('begin', 'csTableName: '||csTableName, sCurrentName);
  -- build the select list
  for rCols in cCols loop
    if rCols.data_type = 'DATE' then 
      sSelStr := sSelStr||', to_char('||rCols.column_name||', '''||sDateFormat||''') '||rCols.column_name ;
    else 
      sSelStr := sSelStr||', '||rCols.column_name;
    end if;        
  end loop;
    
  -- avoid unexistent table. check if sSelStr is null.
  if sSelStr is not null then
    -- then build the string and build up the XML
    sSelStr := 'select '||ltrim(sSelStr, ', ')||' from '||csTableName;
    isr$trace.debug('sSelStr in BulidTableXML', sSelStr, sCurrentName);
            
    -- collect the data in an xml using dbms_xmlgen
    ctxContext := dbms_xmlgen.newContext(sSelStr);
    xTableXML   := dbms_xmlgen.getxmltype(ctxContext); 
    nRows := dbms_xmlgen.getNumRowsProcessed(ctxContext);
    isr$trace.debug('xml built from '||nRows||' rows','xml in logblob ->', sCurrentName,  xTableXML);
    dbms_xmlgen.closeContext(ctxContext);
  end if; 
  
  isr$trace.stat ('end', 'csTableName: '||csTableName, sCurrentName );  
  return xTableXML;
      
end  BulidTableXML;

procedure SaveAndSetSessionNLSSettings is
-- ****************************************************************************************************
-- Date and Autor: 25.Aug 2015 HR
--  The local procedure saves the session NLS settings for date format and decimal separator 
--  in package body variables. 
--  Then sets the session date format to the internal format and separator to .
--  Saved settings will be set back later in the flow using procedure ResetSavedSessionNLSSettings
-- ***************************************************************************************************
  sCurrentName constant varchar2(100) := $$PLSQL_UNIT||'.SaveAndSetSessionNLSSettings';
  cursor cGetNLSVal(csParam in varchar2) is
    select value 
    from nls_session_parameters
    where parameter = csParam;
begin
  isr$trace.stat ('begin', 'begin', sCurrentName);
  -- first save the session settings in package body variables
  open cGetNLSVal('NLS_DATE_FORMAT');
  fetch cGetNLSVal into sSaveNLSDateFormat;
  close cGetNLSVal;
  open cGetNLSVal('NLS_NUMERIC_CHARACTERS');
  fetch cGetNLSVal into sSaveNLSNumChars;
  close cGetNLSVal;
  
  -- then 
  execute immediate 'alter session set nls_date_format='''||sDateFormat||'''';
  execute immediate 'alter session set nls_numeric_characters=''.,'''; 
  
  isr$trace.stat ('end', 'sSaveNLSDateFormat: '||sSaveNLSDateFormat||', sSaveNLSNumChars: '||sSaveNLSNumChars, sCurrentName);
end SaveAndSetSessionNLSSettings;
 
Procedure ResetSavedSessionNLSSettings is  
-- ****************************************************************************************************
-- Date and Autor: 25.Aug 2015 HR
--  The local procedure sets the session NLS settings for date format and decimal separator to the values
--   stored in package body variables earlier in the flow by the procedure SaveAndSetSessionNLSSettings.
-- ***************************************************************************************************
  sCurrentName constant varchar2(100) := $$PLSQL_UNIT||'.SaveAndSetSessionNLSSettings';
begin
  isr$trace.stat ('begin', 'begin', sCurrentName);
  execute immediate 'alter session set nls_date_format='''||sSaveNLSDateFormat||'''';
  execute immediate 'alter session set nls_numeric_characters='''||sSaveNLSNumChars||'''';
  isr$trace.stat ('end', 'sSaveNLSDateFormat: '||sSaveNLSDateFormat||', sSaveNLSNumChars: '||sSaveNLSNumChars, sCurrentName);
end ResetSavedSessionNLSSettings; 
  
  
procedure InsertTableDataFromXML(csTableName in varchar2, xDataXML in xmltype, bForamtIsSet in boolean default false) is  
-- ****************************************************************************************************
-- Date and Autor: 27.Aug 2015 HR
--  The local procedure replaces the contents of the table in csTableName with the snapshot data from xDataXML
--  Depending on the parameter bForamtIsSet, the nls settings must be set within the procedure or not:
--  - for batch flow (the procedure is called for more tables in succession) nls settings should be set
--    outside the procedure and bForamtIsSet shoud be true
--  - for singel table flow (the procedure is called for only one table) bForamtIsSet can be omitted,
--    so it is false and the nls settings are set and reset within the prodedure
--  
-- Parameter :
-- csTableName   the table name (full name, not only entityname)  
-- xDataXML      the snapshot data  
-- bForamtIsSet  if true then the nls settings are already set to the right values, 
--               if false, then this must be done within the procedure 
--
-- ***************************************************************************************************
  sCurrentName constant varchar2(100) := $$PLSQL_UNIT||'.SaveAndSetSessionNLSSettings';
  ctxContext            dbms_xmlstore.ctxType;
  cursor cGetNLSVal(csParam in varchar2) is
    select value 
    from nls_session_parameters
    where parameter = csParam;
  sNLSDateFormat        nls_session_parameters.value%type;
  sNLSNumChars          nls_session_parameters.value%type; 
  nRowsInserted         number;  
begin
  isr$trace.stat('begin','csTableName: '||csTableName, sCurrentName);
    
  --  set the nls params if needed
  if not bForamtIsSet then 
    SaveAndSetSessionNLSSettings;
  end if;
    
  -- write the data into the destination table using dbms_xmlstore
  ctxContext := dbms_xmlstore.newContext(csTableName);                           
  -- insert the row and close the context
  nRowsInserted := dbms_xmlstore.insertXML(ctxContext, xDataXML);
    
  dbms_xmlstore.closeContext(ctxContext); 
  isr$trace.debug('rows inserted', nRowsInserted, sCurrentName);
    
  -- reset the nls params to old vals if needed
  if not bForamtIsSet then 
    ResetSavedSessionNLSSettings;
  end if;
  isr$trace.stat('end','nRowsInserted: '||nRowsInserted, sCurrentName);    
end InsertTableDataFromXML;


----

-- ****************************************************************************************************
-- public functions and procedures
-- ****************************************************************************************************

-- ****************************************************************************************************
function BuildReportDataXML (clSnapshotXML out clob) return STB$oError$Record is
  sCurrentName constant   varchar2(100) := $$PLSQL_UNIT||'.BuildReportDataXML';
  oErrorObj               STB$oError$Record := STB$oError$Record();
  sMsg                    varchar2(4000);
    
  xSnapshotXML            xmltype;
  xEntityXML              xmltype;
  
  clXML                   clob;
  nBOffset                number := 1;
  nCOffset                number := 1;
  nLangContext            number := dbms_lob.default_lang_ctx;
  nWarning                number;
begin
  isr$trace.stat('begin', 'begin', sCurrentName);
  
  -- initialize the xml
  select xmlelement("entities") 
  into xSnapshotXML
  from dual;     
  
  -- then add the entities data  
  for rEntity in cEntities loop   
    xEntityXML := BulidTableXML(csTempTabPrefix||rEntity.entityname);
    isr$trace.debug('snapshot xml for entity', rEntity.entityname, sCurrentName, xEntityXML);     
    select insertchildxml(xSnapshotXML, '/entities', replace(rEntity.entityname,'$','_x0024_'), xmlelement(evalname(replace(rEntity.entityname,'$','_x0024_')),xEntityXML))
    into xSnapshotXML from dual;   
    isr$trace.debug('snapshot xml with entity', rEntity.entityname, sCurrentName, xSnapshotXML);
  end loop; 
    
  -- then convert the XML to a cLOB and make sure the encoding is utf-8  
  -- connot use stb$util.blobToClob because of encoding
  dbms_lob.createtemporary(clXML, TRUE);
  dbms_lob.open(clXML, dbms_lob.lob_readwrite);  
  dbms_lob.converttoclob(clXML, xmltype.getBlobVal(xSnapshotXML, nls_charset_id('AL32UTF8')), dbms_lob.lobmaxsize, nCOffset, nBOffset, nls_charset_id('AL32UTF8'), nLangContext, nWarning );
  isr$trace.debug('snapshot xml as clob','snapshot xml before end', sCurrentName,  clXML); 
  
  clSnapshotXML := clXML;
  isr$trace.stat('end', 'end', sCurrentName);
  return oErrorObj;
  
exception
  when others then
    rollback; 
    sMsg :=  utd$msglib.getmsg ('exUnexpectedError', nCurrentLanguage, csP1 => sqlerrm(sqlcode));
    oErrorObj.handleError ( Stb$typedef.cnSeverityCritical, sMsg, sCurrentName, 
                            dbms_utility.format_error_stack || CHR(10) || dbms_utility.format_error_backtrace  );  
    return oErrorObj;  
end BuildReportDataXML;
-- Aug 2015

function ReplaceDataFromXML(xSnapshotXML in xmltype) return STB$oError$Record is
  sCurrentName constant   varchar2(100) := $$PLSQL_UNIT||'.ReplaceDataFromXML';
  oErrorObj               STB$oError$Record := STB$oError$Record();
  sMsg                    varchar2(4000);

  xTableXML xmltype;
  sEntityname varchar2(100);
  sStatement varchar2(4000);
begin
  isr$trace.stat('begin', 'begin', sCurrentName);
  isr$trace.debug('xSnapshotXML','xSnapshotXML', sCurrentName, xSnapshotXML);
  --  save some current NLS settings and set to required ones
  SaveAndSetSessionNLSSettings;
  
  -- then replace the tables data
  for rEntity in cEntities loop
    isr$trace.debug('Entity name', rEntity.entityname, sCurrentName);
    
    -- extract the data for the entity from the snapshot xml
    sEntityname := replace(rEntity.entityname,'$','_x0024_');     
    sStatement := 
    'select column_value tablexml
    from xmltable(''/entities/'||sEntityname||''' passing :1 )';
    isr$trace.debug('sStatement',sStatement, sCurrentName);
    execute immediate sStatement into xTableXML using in xSnapshotXML;
    isr$trace.debug('xTableXML','xTableXML', sCurrentName, xTableXML);
    
    -- then replace the tables data with the data from the xml
    execute immediate 'delete from '||csTempTabPrefix||rEntity.entityname into xTableXML using in xSnapshotXML;
    isr$trace.debug('Rows deleted from '||csTempTabPrefix||rEntity.entityname, sql%rowcount, sCurrentName);
      
    InsertTableDataFromXML(csTempTabPrefix||rEntity.entityname, xTableXML, true);    
  end loop; 
  
  -- restore the NLS settings  
  ResetSavedSessionNLSSettings;
  
  isr$trace.stat('end', 'end', sCurrentName);
  return oErrorObj;  
exception
  when others then
    rollback; 
    sMsg :=  utd$msglib.getmsg ('exUnexpectedError', nCurrentLanguage, csP1 => sqlerrm(sqlcode));
    oErrorObj.handleError ( Stb$typedef.cnSeverityCritical, sMsg, sCurrentName, 
                            dbms_utility.format_error_stack || CHR(10) || dbms_utility.format_error_backtrace  );  
    return oErrorObj;     
end ReplaceDataFromXML;

function createReportRemoteSnapshot(cnJobId IN NUMBER) RETURN STB$OERROR$RECORD IS
  sCurrentName constant  varchar2(100) := $$PLSQL_UNIT||'.createReportRemoteSnapshot';
  oErrorObj              STB$oError$Record := STB$oError$Record();
  sMsg                   varchar2(4000);
  
  sIP                    VARCHAR2(16);
  sPort                  VARCHAR2(5); 
 
  clSnapshotXML          clob;
  
begin  
  isr$trace.stat('begin', 'cnJobId: '||cnJobId, sCurrentName);
    
  -- Initialize parameters for this job session
  oErrorObj := Stb$object.initRepType(STB$JOB.getJobParameter('REPORTTYPEID',cnJobId), STB$JOB.getJobParameter('REPID',cnJobId));

  oErrorObj := BuildReportDataXML (clSnapshotXML);
  -- das sollte noch anders werden!?
  IF oErrorObj.sSeverityCode != Stb$typedef.cnNoError THEN
    RETURN oErrorObj;
  END IF;
  
  -- create entry in STB$JOBTRAIL
  --setProgress(100,UTD$MSGLIB.getMsg('save xml in jobtrail', STB$SECURITY.getCurrentLanguage));    
  isr$trace.debug('after snapshot data', 'cnJobId: '||cnJobId, sCurrentName, clSnapshotXML);
  
  --SELECT STB$JOBTRAIL$SEQ.NEXTVAL INTO nRecordID FROM DUAL;

  INSERT INTO STB$JOBTRAIL (ENTRYID
                          , JOBID
                          , REFERENCE
                          , TEXTFILE
                          , DOCUMENTTYPE
                          , MIMETYPE
                          , TIMESTAMP
                          , FILENAME
                          , BLOBFLAG
                          , DOWNLOAD)
  VALUES (STB$JOBTRAIL$SEQ.NEXTVAL
        , cnJobId
        , cnJobId
        , clSnapshotXML
        , 'REMOTESNAPSHOT'
        , 'text/xml'
        , SYSDATE
        , 'RemoteSnapshot.xml'
        , 'C'
        , 'F');

  COMMIT;  
  
  isr$trace.stat('end', 'end', sCurrentName);
  RETURN oErrorObj;


exception
  when others then
    rollback; 
    sMsg :=  utd$msglib.getmsg ('exUnexpectedError', nCurrentLanguage, csP1 => sqlerrm(sqlcode));
    oErrorObj.handleError ( Stb$typedef.cnSeverityCritical, sMsg, sCurrentName, 
                            dbms_utility.format_error_stack || CHR(10) || dbms_utility.format_error_backtrace  );
    SELECT ip, to_char(port) INTO sIp, sPort FROM STB$JOBSESSION WHERE jobid = cnJobId;
    Stb$job.markDBJobAsBroken(cnJobId,sIp, sPort, oErrorObj.sErrorCode, oErrorObj.sErrorMessage,oErrorObj.sSeverityCode);                              
    return oErrorObj;
    
end createReportRemoteSnapshot;  

end iSR$RemoteSnapshot;