CREATE OR REPLACE PACKAGE body iSR$Local$Entity
is
  csTablePrefix       VARCHAR2(10) := 'ISR$'; -- system variable
  
--***********************************************************************************************************************************************
FUNCTION insertUserFileIntoJob(blBinaryfile IN BLOB, csFilename IN VARCHAR2, csMimetype IN VARCHAR2, csBlobFlag IN VARCHAR2) RETURN CHAR
IS
  PRAGMA autonomous_transaction;
  oErrorObj    STB$OERROR$RECORD := STB$OERROR$RECORD();
  sCurrentName constant varchar2(4000) := $$PLSQL_UNIT||'.insertUserFileIntoJob('||csFilename||')';
  sMsg                  varchar2(2000);
  nCurrentLanguage      number         := stb$security.getcurrentlanguage;

BEGIN
  isr$trace.stat('begin', 'begin', sCurrentName);
  FOR rGetJobid IN (SELECT jobid FROM stb$jobsession where oraclejobsessionid = STB$SECURITY.getCurrentSession) LOOP
      isr$trace.debug('begin loop','csFilename : ' || csFilename || ' csBlobFlag: ' || csBlobFlag || ' jobId ' || rGetJobid.jobid, sCurrentName);

    --BLOB----------------------------------------------------------
      IF csBlobFlag='B' THEN
        INSERT INTO STB$JOBTRAIL (
           JOBID, DOCUMENTTYPE,
           MIMETYPE,DOCSIZE, TIMESTAMP, BINARYFILE,
           FILENAME, BLOBFLAG,
           ENTRYID, REFERENCE, DOWNLOAD,DOC_DEFINITION)
         VALUES(
           rGetJobid.jobid, 'USERFILE', 
           csMimetype, dbms_lob.getlength(blBinaryfile),
           Sysdate,blBinaryfile,csFilename,'B' ,
           STB$JOBTRAIL$SEQ.NEXTVAL,rGetJobid.jobid, 'T',''
         );
       END IF;
      COMMIT;
      isr$trace.debug('end loop','end loop', sCurrentName);
  END LOOP;
  
  isr$trace.stat('end', 'end', sCurrentName);
  RETURN 'T';

EXCEPTION  
  when others then
    sMsg :=  utd$msglib.getmsg ('exUnexpectedError', nCurrentLanguage, sqlerrm(sqlcode), sMsg);
    oErrorObj.handleError ( Stb$typedef.cnSeverityCritical, sMsg, sCurrentName, 
                            dbms_utility.format_error_stack || CHR(10) || dbms_utility.format_error_backtrace );  
    ROLLBACK;
    RETURN 'F'; 
END insertUserFileIntoJob;

--******************************************************************************
FUNCTION getAnsiiSql(csEntity IN VARCHAR2, csWizard IN VARCHAR2 DEFAULT 'F', csMasterKey IN VARCHAR2 default null) 
   RETURN CLOB 
IS
  oErrorObj       STB$OERROR$RECORD := STB$OERROR$RECORD();
  sCurrentName   constant varchar2(4000) := $$PLSQL_UNIT||'.getAnsiiSql('||csEntity||')';
  
  sStatement    CLOB;
  sTempTabSql   VARCHAR2(32767);
  
  sColumns      CLOB;
  sTables       CLOB;
  sConditions   CLOB;
  
  nCounter      NUMBER := 0;
  
  cursor cGetEntity is
    SELECT entity, isrmanaged
      FROM isr$meta$crit c, isr$meta$entity e
     WHERE c.critentity = csEntity
       AND e.entityname = csEntity;
     
  sEntity       VARCHAR2(500); 
  sEntityType   VARCHAR2(1); -- T isrmanaged, F non isrmanaged
  
  cursor cGetColumns(csCritEntity IN VARCHAR2) is
    SELECT CASE
              WHEN datatype = 'NUMBER' THEN
                 'to_char('||attrname||')' || ' ' || attrname 
              WHEN datatype = 'DATE' THEN
                 'to_char('||attrname||', '''||Stb$typedef.csInternalDateFormat||''')' || ' ' || attrname 
              WHEN datatype = 'BLOB' AND entityname like '%USER$FILE%' THEN           -- 13.Mar 2013, IS: save files in stb$jobtrail for tables with images in report 
                 'iSR$Local$Entity.insertUserFileIntoJob('||attrname||', FILENAME, MIMETYPE,''B'')' || ' ' || attrname
              WHEN datatype = 'BLOB' AND entityname not like '%USER$FILE' THEN 
                 'STB$JAVA.getBlobByteString('||attrname||')' || ' ' || attrname
              ELSE
                 case when description is not null then description||' ' end || attrname
           END attrname
      FROM isr$meta$attribute a
     WHERE entityName = csCritEntity
       AND istemp = 'F'
    ORDER BY orderno;  
    
  rGetColumns cGetColumns%rowtype;

  cursor cGetRelations(csCritEntity IN VARCHAR2) is
    SELECT r.parententityname, r.parentattrname, r.childentityname, r.childattrname,  r.relationtype
      FROM isr$meta$relation r, isr$meta$relation$report rr
     WHERE r.relationname = rr.relationname
       AND r.childentityname IN (csCritEntity, csEntity)
       AND rr.reporttypeid = STB$OBJECT.getCurrentReporttypeId
       AND (csWizard = 'F' OR (csWizard = 'T' AND r.relationtype != 'ALIAS'))
       AND r.relationtype NOT IN ('SK')
     ORDER BY 4;
  
  TYPE tGetTempValues IS REF CURSOR;
  cGetTempValues      tGetTempValues;
  sTempValue          VARCHAR(24000);
  
  CURSOR cGetDisplayValue IS
    SELECT display
      FROM ISR$META$CRIT
     WHERE entity = sEntity;
    
  sDisplayValue ISR$META$CRIT.DISPLAY%TYPE;
  sSearchValue  VARCHAR2(4000); 
 
  nNonSimpleAttrCount        number;
    
  procedure writeCriteriaCondition(csParentEntity IN VARCHAR2, csParentAttr IN VARCHAR2, csChildAttr IN VARCHAR2) is
  
    sCurrentName      CONSTANT VARCHAR2(100) := $$PLSQL_UNIT||'.getAnsiiSql#writeCriteriaCondition';       
    cursor cGetCritKeyColumn(csCritEntity IN VARCHAR2) is
      SELECT case when key = csParentAttr then 'KEY' 
                  when display = csParentAttr then 'DISPLAY'
                  when info = csParentAttr then 'INFO'
             end
        FROM isr$meta$crit 
       WHERE critentity = csCritEntity;
    
    sCritKeyColumn VARCHAR2(500);  
    
    CURSOR cGetCriteria(csCritEntity IN VARCHAR2, csCritKey IN VARCHAR2, csColumn IN VARCHAR2) is
    
      SELECT CASE WHEN csColumn = 'KEY' THEN c.key
                  WHEN csColumn = 'DISPLAY' THEN c.display
                  WHEN csColumn = 'INFO' THEN c.info
             END key, c.masterkey
        FROM isr$crit c
       WHERE c.repid = STB$OBJECT.getCurrentRepid
         AND c.entity = csCritEntity;
     
    rGetCriteria cGetCriteria%ROWTYPE;

  BEGIN
    
    isr$trace.stat('begin', 'begin', sCurrentName);
    
    isr$trace.debug('csParentEntity', csParentEntity, sCurrentName);
    isr$trace.debug('csParentAttr', csParentAttr, sCurrentName);
    isr$trace.debug('csChildAttr', csChildAttr, sCurrentName);

    sCritKeyColumn := null;
    
    OPEN cGetCritKeyColumn(csParentEntity);
    FETCH cGetCritKeyColumn INTO sCritKeyColumn;
    CLOSE cGetCritKeyColumn;
    
    isr$trace.debug('sCritKeyColumn', sCritKeyColumn, sCurrentName);    
      
    OPEN cGetCriteria(csParentEntity, csParentAttr, sCritKeyColumn);
    FETCH cGetCriteria INTO rGetCriteria;
    IF cGetCriteria%FOUND AND TRIM(rGetCriteria.key) IS NOT NULL /*for grid?!?, but should never be null*/  THEN
      sConditions :=  sConditions||csChildAttr||' IN ( ';
      WHILE cGetCriteria%FOUND LOOP
        isr$trace.debug('rGetCriteria.key', rGetCriteria.key, sCurrentName);
        isr$trace.debug('rGetCriteria.masterkey', rGetCriteria.masterkey, sCurrentName);
        sConditions :=  sConditions||
                        ''''||REPLACE(rGetCriteria.key, '''', '''''')||''''
                        ||',';
        FETCH cGetCriteria INTO rGetCriteria;
      END LOOP;
      sConditions :=  Rtrim(sConditions, ',')||' ) AND ';
    END IF;
    CLOSE cGetCriteria;     
    
    isr$trace.stat('end', 'end', sCurrentName);
  end writeCriteriaCondition;  
  
BEGIN
  isr$trace.stat('begin', 'begin', sCurrentName);
  
  isr$trace.debug(csEntity||' csEntity', csEntity, sCurrentName);
  isr$trace.debug(csEntity||' csWizard', csWizard, sCurrentName);
  isr$trace.debug(csEntity||' csMasterKey', csMasterKey, sCurrentName);  
  
  OPEN cGetEntity;
  FETCH cGetEntity INTO sEntity, sEntityType;
  isr$trace.debug(csEntity||' sEntity', sEntity, sCurrentName);
  isr$trace.debug(csEntity||' sEntityType', sEntityType, sCurrentName);
  IF cGetEntity%NOTFOUND THEN
    sEntity := csEntity;
    isr$trace.debug(csEntity||' sEntity', sEntity, sCurrentName);
  END IF;
  CLOSE cGetEntity;
  
  sTables := csTablePrefix||sEntity||' ';
  
  -- columns
  
  -- check attribute types
  select count(1) into nNonSimpleAttrCount
  from isr$meta$attribute 
  where entityname = sEntity
    and datatype not in ('NUMBER','DATE','VARCHAR2');
  
  if nNonSimpleAttrCount = 0 then
  
    select listagg(attrname, ', ') within group (order by orderno) into sColumns
    from isr$meta$attribute
    where entityname = sEntity and IsTemp='F';
 
  else
  
    OPEN cGetColumns(sEntity);
    FETCH cGetColumns INTO rGetColumns;
    IF cGetColumns%FOUND THEN
      WHILE cGetColumns%FOUND LOOP
        sColumns := sColumns||rGetColumns.attrname||',';
        FETCH cGetColumns INTO rGetColumns;
      END LOOP;
    END IF;
    CLOSE cGetColumns;
  
  end if;

  -- additional conditions
  for cCoditions in (select attrname, conditionlist, relationtype
                     from tmp$entity$condition
                     where entityname = csEntity) loop
    -- if we have an entry then we also have values. Do not veridy it again
    sConditions :=  sConditions||'(';
    nCounter := 0;                 
    for cGetCondValues in (select column_value as condval FROM table(cCoditions.conditionlist)) loop
      nCounter := nCounter + 1;
      isr$trace.debug(sEntity||' nCounter/condval', nCounter||'/'||cGetCondValues.condval, sCurrentName);
      -- list may not contain more than 1000 entries
      IF MOD(nCounter, 1000) = 1 THEN
        IF nCounter > 1 THEN
          sConditions :=  sConditions||') OR ';
        END IF;
        sConditions :=  sConditions|| cCoditions.attrname ||' IN (';
      ELSE
        sConditions :=  sConditions||',';
      END IF;
      sConditions :=  sConditions||
                      ''''||REPLACE(cGetCondValues.condval, '''', '''''')||'''';      
    end loop; -- finished Conditions 
    sConditions :=  sConditions||')';
    sConditions :=  sConditions||')' || case when cCoditions.relationtype = 'OR' then ' OR ' else ' AND ' end ;  
         
  end loop; -- finished GetCondValues
      
  -- Condition through parent criteria/temporary tables
  FOR rGetRelations IN cGetRelations(sEntity) LOOP
    -- temporary tables
    BEGIN
      sTempTabSql := 
        'SELECT TO_CHAR('||rGetRelations.parentattrname||') FROM TMP$'||rGetRelations.parententityname;-- || 
      isr$trace.debug(sEntity||' sTempTabSql', sTempTabSql, sCurrentName);
      OPEN cGetTempValues FOR sTempTabSql;
      FETCH cGetTempValues INTO sTempValue;
      IF cGetTempValues%FOUND AND TRIM(sTempValue) IS NOT NULL THEN
        sConditions :=  sConditions||'(';
        nCounter := 0;
        WHILE cGetTempValues%FOUND LOOP
          isr$trace.debug(sEntity||' sTempValue '||sEntity, sTempValue, sCurrentName);
          nCounter := nCounter + 1;  
          isr$trace.debug(sEntity||' nCounter'||sEntity, nCounter, sCurrentName);
          -- list may not contain more than 1000 entries
          IF MOD(nCounter, 1000) = 1 THEN
            IF nCounter > 1 THEN
              sConditions :=  sConditions||') OR ';
            END IF;
            sConditions :=  sConditions||rGetRelations.childattrname||' IN (';
          ELSE
            sConditions :=  sConditions||',';
          END IF;
          sConditions :=  sConditions||
                          ''''||REPLACE(sTempValue, '''', '''''')||'''';
          FETCH cGetTempValues INTO sTempValue;
        END LOOP;
        sConditions :=  sConditions||')';
        sConditions :=  sConditions||')' || case when rGetRelations.relationtype = 'OR' then ' OR ' else ' AND ' end ;
      ELSE
        -- criteria
        writeCriteriaCondition(rGetRelations.parententityname, rGetRelations.parentattrname, rGetRelations.childattrname);    
      END IF;
      CLOSE cGetTempValues;   
    EXCEPTION WHEN OTHERS THEN
      -- criteria
      writeCriteriaCondition(rGetRelations.parententityname, rGetRelations.parentattrname, rGetRelations.childattrname);     
    END;
    -- GRID reference for masterkey 
    IF csWizard = 'T'/* AND TRIM(csMasterKey) IS NOT NULL */THEN   
      FOR rGetMasterKeyCol IN ( SELECT childattrname
                                  FROM isr$meta$relation r
                                     , isr$meta$relation$report rr
                                     , isr$meta$grid$entity ge1
                                     , isr$meta$grid$entity ge2
                                 WHERE r.relationname = rr.relationname
                                   AND rr.reporttypeid = STB$OBJECT.getCurrentReporttypeId
                                   AND ge1.entityname = sEntity
                                   AND ge2.columnpos = 1
                                   AND ge1.entityname = r.childentityname
                                   AND ge2.entityname = r.parententityname
                                   AND ge1.wizardentity = ge2.wizardentity
                                   AND ge1.reporttypeid = ge2.reporttypeid
                                   AND ge2.reporttypeid = rr.reporttypeid) LOOP
        isr$trace.debug(sEntity||' rGetMasterKeyCol.childattrname', rGetMasterKeyCol.childattrname, sCurrentName);
        sConditions :=  sConditions||'(';
        sConditions :=  sConditions||rGetMasterKeyCol.childattrname||' IN (';
        sConditions :=  sConditions||
                        ''''||REPLACE(csMasterKey, '''', '''''')||'''';
        sConditions :=  sConditions||')';
        sConditions :=  sConditions||') AND ';
      END LOOP;
    END IF;
  END LOOP;
      
  sColumns := Rtrim(sColumns, ',');
  sTables := Rtrim(sTables, ',');
  sConditions := Rtrim(sConditions,  ' AND ');
  sConditions := Rtrim(sConditions,  ' OR ');
    
  IF Trim(sColumns) IS NOT NULL AND Trim(sTables) IS NOT NULL THEN 
    sStatement := sStatement||' select '||sColumns||Chr(10);
    sStatement := sStatement||' from '||sTables||Chr(10);
    IF Trim(sConditions) IS NOT NULL THEN
      sStatement := sStatement||' where '||sConditions||Chr(10);
    END IF;
  END IF;

  isr$trace.info(sEntity||'_sStatement.sql', length(sStatement), sCurrentName, sStatement);
  
  isr$trace.stat('end', 'end', sCurrentName);
  RETURN sStatement;

END getAnsiiSql;

--******************************************************************************
function fillTmpTableForEntity( csEntity    in  varchar2, 
                      coParamList in  isr$ParamList$Rec, 
                      csWizard    in  varchar2, 
                      csMasterKey in  varchar2 default null ) return STB$OERROR$RECORD
is
  oErrorObj                  STB$OERROR$RECORD := STB$OERROR$RECORD();

  clStatement                CLOB;
  clXML                      CLOB;
  
  sCurrentName      CONSTANT VARCHAR2(100) := $$PLSQL_UNIT||'.fillTmpTableForEntity('||csEntity||')';
  xDataSetXML                XMLTYPE;     -- XML data set definition for the creation of a table row in the TMP$ target table

  sAttrList1                 VARCHAR2(32767); 
  sAttrList2                 VARCHAR2(32767);
  clInsert                   CLOB;
  sMsg                       VARCHAR2(2000);
  nCurrentLanguage           number        := stb$security.getcurrentlanguage;
  nNonSimpleAttrCount        number;
begin
  isr$trace.stat('begin', 'begin', sCurrentName);

  clStatement := getAnsiiSql (csEntity, csWizard, csMasterKey);
  
  -- check attribute types
  select count(1) into nNonSimpleAttrCount
  from isr$meta$attribute 
  where entityname = csEntity
    and datatype not in ('NUMBER','DATE','VARCHAR2');
  
  if nNonSimpleAttrCount = 0 then
    -- go the short way because only simple datatypes are used: insert directly with subselect
    
    -- first build the columns list
    select listagg(attrname, ', ') within group (order by orderno) into sAttrList1
    from isr$meta$attribute
    where entityname = csEntity and IsTemp='F';
    
    -- then the statement
    clInsert := 'INSERT INTO '||STB$UTIL.GetSystemParameter('TEMP_TAB_PREFIX')||csEntity||' ('||sAttrList1||') '||
                  '('||clStatement||')';
    
    isr$trace.debug(csEntity||' sStatement', '--> see LOGCLOB', sCurrentName, clInsert);
    
    -- then execute the insert              
    EXECUTE IMMEDIATE clInsert;                  
    isr$trace.debug(csEntity||' inserted rows', sql%rowcount, sCurrentName);
    
  else
    -- go the long way over XML because of BLOBS or complex datatypes
  
    -- first collect the data into an XML
    clXML := DBMS_XMLQUERY.GetXML (clStatement);    
    isr$trace.info(csEntity ||'_collected_clXML.xml', dbms_lob.getLength(clXML), sCurrentName, clXML);  
    
    -- then check if selected data exist and insert it
    if dbms_lob.instr(clXML,'<ROWSET/>') > 0 then
      isr$trace.warn (csEntity ||' no rows selected', 'no rows selected for entity', sCurrentName);  
    else
      xDataSetXML := ISR$XML.clobToXML(clXML);
      
      -- then build the insert statement 
      FOR rGetCols IN (
        SELECT attrname colName
             ,    CASE
                     WHEN datatype = 'BLOB' THEN
                        'STB$JAVA.getStringByteBlob (EXTRACT (COLUMN_VALUE, ''/ROW/'||UPPER(attrname)||'/text()'').getClobVal())'
                     WHEN datatype = 'CLOB' THEN
                        'REGEXP_REPLACE(EXTRACT (COLUMN_VALUE, ''/ROW/'||UPPER(attrname)||'/text()'').getclobval(), ''^\<\!\[CDATA\[(.*)\]\]\>$'', ''\1'', 1, 1, ''n'')'
                     WHEN datatype = 'NUMBER' THEN
                        'TO_NUMBER(EXTRACTVALUE (COLUMN_VALUE, ''/ROW/'||UPPER(attrname)||'/text()''))'
                     WHEN datatype = 'DATE' THEN
                        'TO_DATE(EXTRACTVALUE (COLUMN_VALUE, ''/ROW/'||UPPER(attrname)||'/text()''), ''' || Stb$typedef.csInternalDateFormat || ''')'
                     ELSE
                        'EXTRACTVALUE (COLUMN_VALUE, ''/ROW/'||UPPER(attrname)||'/text()'')'
                 END || '"'||UPPER(attrname)||'"' col
             , orderno
          FROM isr$meta$attribute
         WHERE entityName = csEntity
           AND istemp = 'F'
         ORDER BY orderno ) LOOP
        sAttrList1 := sAttrList1||','||rGetCols.colName;
        sAttrList2 := sAttrList2||','||REPLACE(REPLACE(rgetCols.col, '<![CDATA['), ']]>');
      END LOOP;
      sAttrList1 := LTRIM(sAttrList1, ',');
      sAttrList2 := LTRIM(sAttrList2, ',');
              
      clInsert := 'INSERT INTO '||STB$UTIL.GetSystemParameter('TEMP_TAB_PREFIX')||csEntity||' ('||sAttrList1||') '||
                    '(select '||sAttrList2||' from XMLTABLE (''for $root in $xml return $root/ROWSET/ROW'' PASSING :1 AS "xml") )';
      isr$trace.debug(csEntity||'_sStatement.sql', dbms_lob.getlength(clInsert),sCurrentName,clInsert);
      isr$trace.debug('value','xDataSetXml',sCurrentName,xDataSetXml); 
      
      -- finally execute it      
      EXECUTE IMMEDIATE clInsert USING xDataSetXML;         
    end if;
    
  end if;  
  
  -- do no longer order the data when it is selected [ISRC-806] because this is done later

  isr$trace.stat('end', 'end', sCurrentName);  
  return oErrorObj;

exception  
  when others then
    sMsg :=  utd$msglib.getmsg ('exUnexpectedError', nCurrentLanguage, sqlerrm(sqlcode), sMsg);
    oErrorObj.handleError ( Stb$typedef.cnSeverityCritical, sMsg, sCurrentName, 
                            dbms_utility.format_error_stack || CHR(10) || dbms_utility.format_error_backtrace );  
    return oErrorObj;  

end fillTmpTableForEntity;


end iSR$Local$Entity;