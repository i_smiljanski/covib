CREATE OR REPLACE PACKAGE BODY ISR$LICENCEFILE AS

  cnLicences                CONSTANT NUMBER      := 10000000000;

  -- If the licence duration is 0, the duration is unlimited
  cnLicenceDuration         CONSTANT NUMBER      := 0; -- since activation
  cnInstallLicenceDuration  CONSTANT NUMBER      := 0; -- since installation
  cnGraceTime               CONSTANT NUMBER      := 20;
  cdEndDate                 CONSTANT DATE        := null;

  csLicensedModules         CONSTANT VARCHAR2(4000) := 'FULL';--'ISR,STABILITY,UTD_WORD,UTD_EXCEL'; -- FULL if all is licened

  cnSecureMode              VARCHAR2(1) := 'F';

-- ****************************************************************************
FUNCTION getLicensedModules RETURN VARCHAR2
IS
  sCurrentName   constant varchar2(4000) := $$PLSQL_UNIT||'.getLicensedModules('||' '||')';
  sReturn VARCHAR2(4000);
BEGIN
  isr$trace.stat('begin', 'begin', sCurrentName);
  /*SELECT replace(replace(rtrim(xmlagg(xmlelement(e,description,' ').extract('//text()') order by levelno, description).GetStringVal(),' '), '&gt;', '>' ), '&lt;', '<')
    INTO sReturn
    FROM (SELECT description, LEVEL levelno
            FROM isr$plugin
           WHERE ','||csLicensedModules||',' like '%,'||plugin||',%' or csLicensedModules = 'FULL'
          START WITH parentplugin IS NULL
          CONNECT BY parentplugin = PRIOR plugin
          ORDER SIBLINGS BY description);*/
  sReturn := sReturn||'<table>';
  sReturn := sReturn||'<tr>';
  sReturn := sReturn||'<th colspan="2">';
  sReturn := sReturn|| UTD$MSGLIB.getMsg('Contents of iStudyReporter', STB$SECURITY.getCurrentLanguage);
  sReturn := sReturn||'</th>';
  sReturn := sReturn||'</tr>';
  FOR rGetData IN ( SELECT 'style="padding-left: '||level*5||'px;"' col1_style, plugin col1, NVL(description, plugin) col2, LEVEL levelno
                      FROM isr$plugin
                     WHERE ','||csLicensedModules||',' like '%,'||plugin||',%' or csLicensedModules = 'FULL'
                    START WITH parentplugin IS NULL
                    CONNECT BY parentplugin = PRIOR plugin
                    ORDER SIBLINGS BY description) LOOP
    sReturn := sReturn||'<tr>';
    sReturn := sReturn||'<td '||rGetData.col1_style||'>';
    sReturn := sReturn|| rGetData.col1;
    sReturn := sReturn||'</td>';
    sReturn := sReturn||'<td>';
    sReturn := sReturn|| rGetData.col2;
    sReturn := sReturn||'</td>';
    sReturn := sReturn||'</tr>';
  END LOOP;
  sReturn := sReturn||'</table>';
  isr$trace.stat('end', 'end', sCurrentName);
  RETURN sReturn;
END getLicensedModules;

-- ****************************************************************************
FUNCTION isLicensed(csModule IN VARCHAR2) RETURN VARCHAR2
IS
BEGIN
  IF ','||csLicensedModules||',' like '%,'||csModule||',%'
  OR csLicensedModules = 'FULL'
  OR TRIM(csModule) IS NULL THEN
    RETURN 'T';
  ELSE
    RETURN 'F';
  END IF;
END isLicensed;

-- ****************************************************************************
FUNCTION getMode RETURN VARCHAR2
IS
BEGIN
  RETURN cnSecureMode;
END getMode;

-- ****************************************************************************
FUNCTION checkLicenceWillExpire RETURN STB$OERROR$RECORD
IS
  oErrorObj               STB$OERROR$RECORD := STB$OERROR$RECORD();
  sCurrentName   constant varchar2(4000) := $$PLSQL_UNIT||'.checkLicenceWillExpire';
  sUserMsg                varchar2(4000);  
  exLicenceWillExpire     EXCEPTION;
BEGIN

  oErrorObj := checkSessionAllowed;
  IF oErrorObj.sSeverityCode = Stb$typedef.cnSeverityMessage THEN
    RAISE exLicenceWillExpire;
  END IF;
  RETURN STB$OERROR$RECORD();

EXCEPTION
  WHEN exLicenceWillExpire then
    sUserMsg :=UTD$MSGLIB.GetMsg('LICENCE_WILL_EXPIRE',STB$SECURITY.GetCurrentLanguage);
    oErrorObj.handleError ( STB$TYPEDEF.cnSeverityMessage, sUserMsg, sCurrentName, 
                            dbms_utility.format_error_stack||stb$typedef.crlf||dbms_utility.format_error_backtrace );  
    RETURN (oErrorObj);
END checkLicenceWillExpire;

-- ****************************************************************************
FUNCTION checkSessionAllowed RETURN STB$OERROR$RECORD
IS
  oErrorObj           STB$OERROR$RECORD := STB$OERROR$RECORD();
  nSessionCnt         NUMBER;
  sFlag               VARCHAR2(1);
  sUserMsg            varchar2(4000);  

  exMaxOpenConnectionExceeded EXCEPTION;
  exLicenceExpired            EXCEPTION;
  exLicenceActivated          EXCEPTION;
  sCurrentName       constant varchar2(100) := $$PLSQL_UNIT||'.checkSessionAllowed';

  cursor cGetSessionCnt is
    select count(*)
      from isr$session;

  cursor cGetSecureModeRights is
    select 'CAN_VIEW_'||nodetype adminright
    from isr$custom$grouping
    where securemodeallowed = 'T';

  cursor cGetActivateDate is
    select min(timestamp)
    from stb$actionlog ;

  cursor cGetInstallationDate is
    select created
    from user_objects
    where object_type = 'PACKAGE BODY'
    and object_name = 'ISR$LICENCEFILE';

  dActivateDate     STB$ACTIONLOG.TIMESTAMP%TYPE;
  dInstallationDate USER_OBJECTS.CREATED%TYPE;
BEGIN
  isr$trace.stat('begin', 'begin', sCurrentName);

  OPEN cGetSessionCnt;
  FETCH cGetSessionCnt INTO nSessionCnt;
  CLOSE cGetSessionCnt;

  IF nSessionCnt >= cnLicences THEN
    sFlag := 'F';
    FOR rGetSecureModeRights IN cGetSecureModeRights LOOP
      sFlag :=Stb$security.checkGroupRight(rGetSecureModeRights.adminright);
      IF sFlag = 'F' OR oErrorObj.sSeverityCode != STB$TYPEDEF.cnNoError THEN
        EXIT;
      END IF;
    END LOOP;
    cnSecureMode := sFlag;
    IF sFlag = 'F' THEN
      RAISE exMaxOpenConnectionExceeded;
    END IF;
  END IF;

  IF cnLicenceDuration > 0 THEN
    open cGetActivateDate;
    fetch cGetActivateDate into dActivateDate;
    IF cGetActivateDate%NOTFOUND OR dActivateDate IS NULL THEN
      close cGetActivateDate;
      RAISE exLicenceActivated;
    ELSIF dActivateDate + cnLicenceDuration < sysdate THEN
      close cGetActivateDate;
      RAISE exLicenceExpired;
    ELSIF dActivateDate + cnLicenceDuration - cnGraceTime < sysdate THEN
      close cGetActivateDate;
      oErrorObj.sSeverityCode := Stb$typedef.cnSeverityMessage;
    ELSE
      open cGetInstallationDate;
      fetch cGetInstallationDate into dInstallationDate;
      close cGetInstallationDate;
      IF dInstallationDate + cnInstallLicenceDuration < sysdate THEN
        close cGetActivateDate;
        RAISE exLicenceExpired;
      ELSIF dInstallationDate + cnInstallLicenceDuration - cnGraceTime < sysdate THEN
        close cGetActivateDate;
        oErrorObj.sSeverityCode := Stb$typedef.cnSeverityMessage;
      END IF;
    END IF;
  END IF;

  IF cdEndDate IS NOT NULL THEN
    IF cdEndDate < TRUNC(sysdate) THEN
      RAISE exLicenceExpired;
    ELSIF cdEndDate - cnGraceTime < TRUNC(sysdate) THEN
      oErrorObj.sSeverityCode := Stb$typedef.cnSeverityMessage;
    END IF;
  END IF;

  oErrorObj.sErrorCode := 'CRACK_CODE';
  isr$trace.stat('end', 'end', sCurrentName);
  RETURN oErrorObj;

EXCEPTION
  WHEN exMaxOpenConnectionExceeded then
    sUserMsg :=UTD$MSGLIB.GetMsg('MAXIMUM_OPEN_CONNECTIONS_EXCEEDED',STB$SECURITY.GetCurrentLanguage);
    oErrorObj.handleError ( STB$TYPEDEF.cnSeverityCritical, sUserMsg, sCurrentName, 
                            dbms_utility.format_error_stack||stb$typedef.crlf||dbms_utility.format_error_backtrace );  
    RETURN (oErrorObj);
  WHEN exLicenceExpired then
    sUserMsg :=UTD$MSGLIB.GetMsg('LICENCE_EXPIRED',STB$SECURITY.GetCurrentLanguage);
    oErrorObj.handleError ( STB$TYPEDEF.cnSeverityCritical, sUserMsg, sCurrentName, 
                            dbms_utility.format_error_stack||stb$typedef.crlf||dbms_utility.format_error_backtrace );  
    RETURN (oErrorObj);
  WHEN exLicenceActivated then
    sUserMsg :=UTD$MSGLIB.GetMsg('LICENCE_ACTIVATED',STB$SECURITY.GetCurrentLanguage);
    oErrorObj.handleError ( STB$TYPEDEF.cnSeverityCritical, sUserMsg, sCurrentName, 
                            dbms_utility.format_error_stack||stb$typedef.crlf||dbms_utility.format_error_backtrace );  
    RETURN (oErrorObj);
  WHEN OTHERS THEN
    sUserMsg := UTD$MSGLIB.getMsg('exUnexpectedError', STB$SECURITY.getCurrentLanguage,  csP1 => sqlerrm(sqlcode));
    oErrorObj.handleError ( STB$TYPEDEF.cnSeverityCritical, sUserMsg, sCurrentName, 
                            dbms_utility.format_error_stack||STB$TYPEDEF.crlf||dbms_utility.format_error_backtrace );  
    RETURN oErrorObj;
END checkSessionAllowed;

END ISR$LICENCEFILE;