CREATE OR REPLACE package body isr$document
is
  exNoTokenHandle     exception;

  sSql                varchar2(32000);
  nCurrentLanguage    number        := stb$security.getcurrentlanguage;    
  crlf                varchar2(2)   := stb$typedef.crlf;
  csISRTempPath       constant varchar2(100) :='[ISR.TEMP.PATH]';
  sIP                 STB$JOBSESSION.ip%type;
  sPort               STB$JOBSESSION.port%type;
  csOffshoot          varchar2(100) :='CAN_OFFSHOOT_DOC';
  csDocPropOid        varchar2(100) :='DOC_OID'; 
  csConfig            varchar2(100) :='CONFIG';

--******************************************************************************
function callFunction( oParameter in  STB$MENUENTRY$RECORD, 
                       olNodeList out STB$TREENODELIST )
  return STB$OERROR$RECORD 
is
  exNoTokenHandle      exception;
  sCurrentName         constant varchar2(100) := $$PLSQL_UNIT||'.callFunction('||oParameter.sToken||')';
  oErrorObj            STB$OERROR$RECORD      := STB$OERROR$RECORD();
  sMsg                 varchar2(4000); 
 
begin
  isr$trace.stat('begin', 'oParameter.sToken: '||oParameter.sToken ,sCurrentName);    
  isr$trace.debug('value', 'oParameter IN', sCurrentName, oParameter);
  olNodeList := STB$TREENODELIST();   -- initialize output parameter
  case
    -- ================== Dialogs
    when oParameter.sToken = 'CAN_COMPARE_DOCUMENT_PREVIOUS' then
         olNodeList.extend;
         olNodeList(1) := STB$TREENODE$RECORD(STB$UTIL.getMaskTitle(oParameter.sToken), $$PLSQL_UNIT, 'TRANSPORT', 'MASK', '');
         isr$trace.debug('before fetch properties','olNodeList',sCurrentName,olNodeList);

         oErrorObj := STB$UTIL.getPropertyList(oParameter.sToken, olNodeList(1).tloPropertylist);
         isr$trace.debug('after fetch properties','olNodeList',sCurrentName,olNodeList);
    else
      raise exNoTokenHandle;
  end case;
  isr$trace.debug('value','olNodeList OUT',sCurrentName,olNodeList);
  isr$trace.stat('end', 'olNodeList.count: '||olNodeList.count, sCurrentName);
  return oErrorObj;
exception
 when exNoTokenHandle then
   sMsg := utd$msglib.getmsg ('exNoTokenHandle', stb$security.getcurrentlanguage, oParameter.sToken);
   oErrorObj.handleError ( Stb$typedef.cnSeverityWarning, sMsg, sCurrentName, 'user defined exception: exNoTokenHandle'||stb$typedef.crlf||dbms_utility.format_error_stack ||stb$typedef.crlf|| dbms_utility.format_error_backtrace );
   rollback;
   return oErrorObj;
 when others then
   sMsg :=  utd$msglib.getmsg ('exUnexpectedError', nCurrentLanguage, csP1 => sqlerrm(sqlcode));
   oErrorObj.handleError ( Stb$typedef.cnSeverityWarning, sMsg, sCurrentName,  dbms_utility.format_error_stack ||stb$typedef.crlf|| dbms_utility.format_error_backtrace  );  
   rollback;
   return oErrorObj;
end callFunction;

--*************************************************************************************************************************
function setMenuAccess( sParameter   in  varchar2, 
                        sMenuAllowed out varchar2 )
  return STB$OERROR$RECORD 
is
  oErrorObj          STB$OERROR$RECORD      := STB$OERROR$RECORD();
  sCurrentName       constant varchar2(100) := $$PLSQL_UNIT||'.setMenuAccess('||sParameter||')';  
  nDocId             number;
  recReportDocument  curReportDocument%rowtype;
  sDocIsCheckedOut   STB$DOCTRAIL.checkedout%type;
  nCurrentNodeId     number;
  sJobRunningOnNode  varchar2(1);
  
begin
  isr$trace.stat('begin', 'sParameter: ' || sParameter, sCurrentName);

  nDocId         := STB$OBJECT.GetCurrentDocId;
  nCurrentNodeId := STB$OBJECT.getCurrentNodeId;
  oErrorObj      := STB$DOCKS.getDocStatus(sDocIsCheckedOut);
  oErrorObj      := STB$JOB.checkJobExists( csReference => nCurrentNodeId,         -- IN parameter
                                            sJobExists  => sJobRunningOnNode  );   -- OUT parameter

  case
    --------- CAN_COMPARE_DOCUMENT_PREVIOUS
    when sParameter in ('CAN_COMPARE_DOCUMENT_PREVIOUS') then
      -- check, if a predecessing document exists
      open  curReportDocument( ccnDocId => nDocid );
      fetch curReportDocument
      into  recReportDocument;

      if sDocIsCheckedOut = 'T' then      -- [ISRC-616]
        sMenuAllowed := 'F';
      elsif sJobRunningOnNode = 'T' then  -- [ISRC-616]
        sMenuAllowed := 'F'; 
      elsif curReportDocument%notfound or recReportDocument.predecessing_docid is null then
        -- no Predecessing document
        sMenuAllowed := 'F';
      else
        -- a Predecessing document exists
        sMenuAllowed := 'T';
      end if;
      
      close curReportDocument;
    --------- wrong token
    else 
      raise exNoTokenHandle;
  end case;

  isr$trace.stat('end', 'sMenuAllowed: ' || sMenuAllowed, sCurrentName);
  return oErrorObj;
exception
  when exNoTokenHandle then
    oErrorObj.handleError( Stb$typedef.cnSeverityWarning, utd$msglib.getmsg ('exNoTokenHandle', nCurrentLanguage,  csP1 => sParameter), sCurrentName,
                           substr(dbms_utility.format_error_stack ||crlf|| dbms_utility.format_error_backtrace, 1,4000) );    
    sMenuallowed := 'F';
    return oErrorObj;
  when others then
    oErrorObj.handleError( Stb$typedef.cnSeverityWarning, utd$msglib.getmsg ('exUnexpectedError', nCurrentLanguage,  csP1 => sqlerrm(sqlcode)),sCurrentName,
                           substr(dbms_utility.format_error_stack ||crlf|| dbms_utility.format_error_backtrace, 1,4000) );                                
   sMenuAllowed := 'F';
   return oErrorObj;
end setMenuAccess;

--*************************************************************************************************************************
function putParameters( oParameter  in STB$MENUENTRY$RECORD, 
                        olParameter in STB$PROPERTY$LIST,
                        clParameter in clob default null   )  
  return STB$OERROR$RECORD
is
  sCurrentName          constant varchar2(100) := $$PLSQL_UNIT||'.putParameters('||oParameter.sToken||')';    
  oErrorObj             STB$OERROR$RECORD      := STB$OERROR$RECORD(tloMessageList => ISR$MESSAGE$LIST());  --initialize the message list too
  sMsg                  varchar2(4000);
  sWhereClause          varchar2(4000);
  sUserName             varchar2(30);
  sStartDate            varchar2(1000);
  sEndDate              varchar2(1000);
  recJobIds             STB$SYSTEM.tJobIdRecord;
  
  recReportDocument     curReportDocument%rowtype;
  csApplicationName     constant char(9)  := 'wordmodul';
  csApplicationCommand  constant char(21) := 'CAN_COMPARE_DOCUMENTS';
  
  nDocIdTarget          stb$doctrail.docid%type;      -- current document:  Document ID
  sPasswordTarget       stb$report.password%type;     -- current document:  Password
  sFilenameTarget       stb$doctrail.filename%type;   -- current document:  Filename
  
  nDocIdTemplate        stb$doctrail.docid%type;      -- older document:    Document ID
  sPasswordTemplate     stb$report.password%type;     -- older document:    Password
  sFilenameTemplate     stb$doctrail.filename%type;   -- older document:    Filename  
  
  
begin
  isr$trace.stat('begin','begin',sCurrentName);
  isr$trace.debug('value','oParameter',sCurrentName,oParameter);
  isr$trace.debug('value','olParameter',sCurrentName,olParameter);  
  isr$trace.debug('value','clParameter',sCurrentName,clParameter);

  nDocIdTarget := STB$OBJECT.GetCurrentDocId;
  isr$trace.debug('value','nDocIdTarget: '||nDocIdTarget,sCurrentName);

  case
    when  oParameter.sToken = 'CAN_COMPARE_DOCUMENT_PREVIOUS'  THEN
      -- build where clause for job
      sUserName  := STB$SECURITY.getOracleUsername(STB$SECURITY.getCurrentUser);
      sStartDate := 'to_date('''||to_char(trunc(sysdate,'MI'),'dd.mm.yyyy hh24:mi')||''',''dd.mm.yyyy hh24:mi'')';
      sEndDate   := 'to_date('''||to_char(trunc(sysdate,'MI'),'dd.mm.yyyy hh24:mi')||''',''dd.mm.yyyy hh24:mi'')';
      sWhereClause := ' where oracleusername = '''||sUserName||''' and timestamp beetween '||sStartDate||' and '||sEndDate;    
      isr$trace.debug('value','sWhereClause: '||sWhereClause,sCurrentname);      

      -- fetch primarily available values
      open  curReportDocument( ccnDocId => nDocIdTarget );
      fetch curReportDocument
      into  recReportDocument;
      sPasswordTarget := recReportDocument.rep_password;
      sFilenameTarget := recReportDocument.filename;
      nDocIdTemplate  := recReportDocument.predecessing_docid;
      close curReportDocument;
    
      -- fetch additional values for the template
      open  curReportDocument( ccnDocId => nDocIdTemplate );
      fetch curReportDocument
      into  recReportDocument;
      sPasswordTemplate := recReportDocument.rep_password;
      sFilenameTemplate := recReportDocument.filename;
      close curReportDocument;      
      
    else
      raise exNoTokenHandle;
  end case;     
 
  open  STB$SYSTEM.cGetJobIds(oParameter.sToken, null);
  fetch STB$SYSTEM.cGetJobIds
  into  recJobIds;
  close STB$SYSTEM.cGetJobIds;
  isr$trace.debug('values','recJobIds.nOutputId: '||recJobIds.nOutputId||',  recJobIds.nActionId: '||recJobIds.nActionId||',  recJobIds.sFunction: '||recJobIds.sFunction,sCurrentname);       

  if recJobIds.nActionId is null then
    sMsg := utd$msglib.getmsg ('exNotFindAction', stb$security.getcurrentlanguage, oParameter.sToken);
    raise stb$job.exNotFindAction;
  end if;
  
  -- increment and fetch from job sequence 
  select STB$JOB$SEQ.nextval
  into   STB$JOB.nJobid
  from   dual;

  oErrorObj := Stb$util.createFilterParameter(olParameter);
  
  STB$JOB.setJobParameter ('ACTIONTYPE',         'DISPLAY',                              STB$JOB.nJobid);  
  STB$JOB.setJobParameter ('TOKEN',              oParameter.sToken,                      STB$JOB.nJobid);     -- Token
  STB$JOB.setJobParameter ('OUTPUTID',           recJobIds.nOutputId,                    STB$JOB.nJobid);     -- Output ID
  STB$JOB.setJobParameter ('ACTIONID',           recJobIds.nActionId,                    STB$JOB.nJobid);     -- Action ID
  STB$JOB.setJobParameter ('WHERE',              sWhereClause,                           STB$JOB.nJobid);     -- Where Clause for Job
  STB$JOB.setJobParameter ('REFERENCE',          STB$OBJECT.getCurrentNodeId,            STB$JOB.nJobid);     -- Job ID
  STB$JOB.setJobParameter ('FUNCTION',           recJobIds.sFunction,                    STB$JOB.nJobid);     -- Individual ActionFunction              
  STB$JOB.setJobParameter ('APP_NAME',           csApplicationName,                      STB$JOB.nJobid);     -- wordmodul application
  STB$JOB.setJobParameter ('APP_COMMAND',        csApplicationCommand,                   STB$JOB.nJobid);     -- command for wordmodul
  STB$JOB.setJobParameter ('APP_TARGET_DOCID',   nDocIdTarget,                           STB$JOB.nJobid);     -- target document Id
  STB$JOB.setJobParameter ('APP_TARGETPASSWORD', sPasswordTarget,                        STB$JOB.nJobid);     -- target password
  STB$JOB.setJobParameter ('APP_TARGET',         nDocIdTarget||'_'||sFilenameTarget,     STB$JOB.nJobid);     -- target filename
  STB$JOB.setJobParameter ('APP_TEMPLATE_DOCID', nDocIdTemplate,                         STB$JOB.nJobid);     -- template document id
  STB$JOB.setJobParameter ('APP_SOURCEPASSWORD', sPasswordTemplate,                      STB$JOB.nJobid);     -- template password
  STB$JOB.setJobParameter ('APP_TEMPLATE',       nDocIdTemplate||'_'||sFilenameTemplate, STB$JOB.nJobid);     -- template filename

  oErrorObj := STB$JOB.startJob;     
  
  isr$trace.stat('end','end',sCurrentName);  
  return oErrorObj;
exception
  when exNoTokenHandle then
    sMsg := utd$msglib.getmsg ('exNoTokenHandle', stb$security.getcurrentlanguage, oParameter.sToken);
    oErrorObj.handleError ( Stb$typedef.cnSeverityWarning, sMsg, sCurrentName, dbms_utility.format_error_stack ||stb$typedef.crlf|| dbms_utility.format_error_backtrace );
    rollback;
    return oErrorObj;
  when stb$job.exNotFindAction then
    oErrorObj.handleError ( Stb$typedef.cnSeverityCritical, sMsg, sCurrentName, dbms_utility.format_error_stack ||stb$typedef.crlf|| dbms_utility.format_error_backtrace );
    rollback;
    return oErrorObj;
  when others then
    sMsg :=  utd$msglib.getmsg ('exUnexpectedError', nCurrentLanguage, csP1 => sqlerrm(sqlcode));
    oErrorObj.handleError ( Stb$typedef.cnSeverityWarning, sMsg, sCurrentName,  dbms_utility.format_error_stack ||stb$typedef.crlf|| dbms_utility.format_error_backtrace  );  
    rollback;
    return oErrorObj;
end putParameters;

--*************************************************************************************************************************
function prepareDocCompare( cnJobId in     number,
                            xXml    in out xmltype )
  return STB$OERROR$RECORD
is
  sCurrentName          constant varchar2(100) := $$PLSQL_UNIT||'.prepareDocCompare('||cnJobId||')';    
  oErrorObj             STB$OERROR$RECORD      := STB$OERROR$RECORD(tloMessageList => ISR$MESSAGE$LIST());  --initialize the message list too
  sMsg                  varchar2(4000);
  nJobId                number;
  nAppId                number;
  sTargetFileName       varchar2(500);
  sTemplateFileName     varchar2(500);
  
  cursor curDocTrail( ccnDocid number ) is
    select *
    from   stb$doctrail
    where  docid = ccnDocid;

begin
  isr$trace.stat('begin','begin',sCurrentName);
  isr$trace.debug('parameter','xXml',sCurrentName,xXml);

  sTargetFileName   := STB$JOB.getJobParameter('APP_TARGET', cnJobid);
  sTemplateFileName := STB$JOB.getJobParameter('APP_TEMPLATE', cnJobid);

  ------------------------------------------------------------------------
  -- fetch data for target document (one loop only)
  -- and insert data into table STB$JOBTRAIL
  ------------------------------------------------------------------------  
  for rec in curDocTrail( STB$JOB.getJobParameter('APP_TARGET_DOCID', cnJobid) ) 
  loop
    -- add target document into jobtrail
    oErrorObj := STB$DOCKS.copyDocToJob( cnDocId      => STB$JOB.getJobParameter('APP_TARGET_DOCID', cnJobid), 
                                         cnJobId      => cnJobId, 
                                         csAsFilename => sTargetFileName );
  end loop;

  -- fetch data for template document (one loop only)
  -- and insert data into table STB$JOBTRAIL
  for rec in curDocTrail( STB$JOB.getJobParameter('APP_TEMPLATE_DOCID', cnJobid) ) 
  loop
    -- add template document into jobtrail
    oErrorObj := STB$DOCKS.copyDocToJob( cnDocId      => STB$JOB.getJobParameter('APP_TEMPLATE_DOCID', cnJobid), 
                                         cnJobId      => cnJobId, 
                                         csAsFilename => sTemplateFileName );
  end loop;  

  -- create parameters for doc compare (wordmodule doc compare)
  nAppId := isr$XML.valueOf(xXml, 'count(/config/apps/app)') + 1;
  isr$XML.CreateNodeAttr(xXml,  '/config/apps', 'app', '', 'order', nAppId);
  isr$XML.SetAttribute(xXml, '/config/apps/app[@order='||nAppId||']', 'name',             STB$JOB.getJobParameter('APP_NAME',           cnJobid) );
  isr$XML.createNode(xXml, '/config/apps/app[@order='||nAppId||']',   'executable', 'wordmodul.exe');  
  isr$XML.createNode(xXml, '/config/apps/app[@order='||nAppId||']',   'wait_for_process', 'true');
  isr$XML.CreateNode(xXml, '/config/apps/app[@order='||nAppId||']',   'command',          STB$JOB.getJobParameter('APP_COMMAND',        cnJobid) );
  isr$XML.CreateNode(xXml, '/config/apps/app[@order='||nAppId||']',   'target',           STB$JOB.getJobParameter('APP_TARGET',         cnJobid) );  
  isr$XML.CreateNode(xXml, '/config/apps/app[@order='||nAppId||']',   'template',         STB$JOB.getJobParameter('APP_TEMPLATE',       cnJobid) );  
  isr$XML.CreateNode(xXml, '/config/apps/app[@order='||nAppId||']',   'sourcepassword',   STB$JOB.getJobParameter('APP_SOURCEPASSWORD', cnJobid) );  
  isr$XML.CreateNode(xXml, '/config/apps/app[@order='||nAppId||']',   'targetpassword',   STB$JOB.getJobParameter('APP_TARGETPASSWORD', cnJobid) );  

  -- open the document, that has the differences
  nAppId := isr$XML.valueOf(xXml, 'count(/config/apps/app)') + 1;
  isr$XML.CreateNodeAttr(xXml,  '/config/apps', 'app', '', 'order', nAppId);
  isr$XML.setAttribute(xXML, '/config/apps/app[@order='||nAppId||']', 'name', 'ME');
  isr$XML.createNode(xXml, '/config/apps/app[@order='||nAppId||']',   'executable', '');    
  isr$XML.createNode(xXml, '/config/apps/app[@order='||nAppId||']',   'wait_for_process', 'true');
  isr$XML.createNode(xXml, '/config/apps/app[@order='||nAppId||']',   'command', 'DISPLAY');
  isr$XML.CreateNode(xXML, '/config/apps', 'fullpathname', csISRTempPath);
  isr$XML.CreateNode(xXML, '/config/apps', 'target', STB$JOB.getJobParameter('APP_TARGET',cnJobid));
  isr$XML.CreateNode(xXML, '/config/apps', 'extension', 'doc');

  isr$trace.debug('parameter','xXml',sCurrentName,xXml);
  isr$trace.stat('end','end',sCurrentName);  
  return oErrorObj;
exception
  when others then
    sMsg :=  utd$msglib.getmsg ('exUnexpectedError', nCurrentLanguage, csP1 => sqlerrm(sqlcode));
    oErrorObj.handleError ( Stb$typedef.cnSeverityWarning, sMsg, sCurrentName,  dbms_utility.format_error_stack ||stb$typedef.crlf|| dbms_utility.format_error_backtrace  );  
    rollback;
    return oErrorObj;  
end   prepareDocCompare;

--***********************************************************************************************************************
function saveConfig( cnJobid in     number, 
                     xXml    in out xmltype)
  return STB$OERROR$RECORD
is
  sCurrentName   constant varchar2(100) := $$PLSQL_UNIT||'.saveConfig('||cnJobId||')';  
  sUserMsg               varchar2(4000);
  sDevMsg                varchar2(4000);  
  sImplMsg               varchar2(4000);  
  oErrorObj               STB$OERROR$RECORD :=STB$OERROR$RECORD();
  nRecordID            number;

  nDocId                STB$DOCTRAIL.DOCID%type;
  sDocOid               STB$DOCTRAIL.DOC_OID%type;
  sIsrDocType           STB$DOCTRAIL.DOCTYPE%type;
  sFileName             STB$DOCTRAIL.FILENAME%type;
  sMimetype             STB$DOCTRAIL.MIMETYPE%type;
  sDocDefinition        STB$DOCTRAIL.DOC_DEFINITION%type;
  
  cursor cGetDocOid(cnDocId in STB$DOCTRAIL.DOCID%type) is
    select doc_oid
    from   stb$doctrail
    where  docid = cnDocId;
       
  cursor cGetPropertiesSetByUser(cnDocOid in STB$DOCTRAIL.DOC_OID%TYPE,
                                 cnDocId in STB$DOCTRAIL.DOCID%type) is
    select p.parametername,
           NVL(parameterdisplay, parametervalue) parametervalue
    from   isr$doc$property P,
           stb$doctrail d,
           isr$doc$prop$template t
    where  P.docid = d.docid
    and    (   d.doc_oid = Nvl(cnDocOid,'DOC_OID_NOT_SET')
            or d.docid = Nvl(cnDocId,0))
    and    t.parametername = p.parametername
    and    t.parametertype != 'PARAMETERS'
    and    t.reporttypeid = STB$JOB.getJobParameter('REPORTTYPEID',cnJobid)
    union
    select v.value, utd$msglib.getMsg(v.msgkey, stb$object.GetCurrentReportLanguage, v.plugin) 
    from   isr$doc$property P,
           stb$doctrail d,
           isr$doc$prop$template t,
           isr$parameter$values v,
           utd$msg m
    where  P.docid = d.docid
    and    t.parametername = p.parametername
    and    (   d.doc_oid = Nvl(cnDocOid,'DOC_OID_NOT_SET')
            or d.docid = Nvl(cnDocId,0))
    and    t.parametertype = 'PARAMETERS'
    and    t.reporttypeid = STB$JOB.getJobParameter('REPORTTYPEID',cnJobid)
    and    v.entity = p.parametervalue;            

  cursor cGetApplicationPathes(cnUserId in STB$USER.USERNO%type) is
    select applicationpathes
    from   stb$user
    where  userno = cnUserId;
    
  sApplicationPathes STB$USER.APPLICATIONPATHES%type;
begin
  isr$trace.stat('begin','begin',sCurrentName);
  isr$trace.debug('value','xXml',sCurrentName,xXml);

  -- over the config xml file get the targets and create an entry in the doctrail
  for i in 1..ISR$XML.valueOf(xXml,'count(/config/apps/app[@name="wordmodul" and command="CAN_RUN_REPORT"])') loop
    isr$trace.debug_all('for i in 1..ISR$XML.valueOf()','i: '||i, sCurrentName);
    
    if STB$JOB.getJobParameter('TOKEN',cnJobid) = csOffshoot then
      exit;
   end if;
    isr$trace.debug_all('status docid', 'STB$JOB.getJobParameter(DOCID,cnJobid): '||STB$JOB.getJobParameter('DOCID',cnJobid), sCurrentName);

    sIsrDocType     := isr$XML.valueOf(xXml, '/config/apps/app[@name="wordmodul" and command="CAN_RUN_REPORT"]['|| i ||']/isrdoctype/text()');
    sFileName       := isr$XML.valueOf(xXml, '/config/apps/app[@name="wordmodul" and command="CAN_RUN_REPORT"]['|| i ||']/target/text()');
    sMimetype       := isr$XML.valueOf(xXml, '/config/apps/app[@name="wordmodul" and command="CAN_RUN_REPORT"]['|| i ||']/mimetype/text()');
    sDocDefinition  := isr$XML.valueOf(xXml, '/config/apps/app[@name="wordmodul" and command="CAN_RUN_REPORT"]['|| i ||']/doc_definition/text()');

    isr$trace.debug_all('status sDocDefinition', 'sDocDefinition: '||sDocDefinition, sCurrentName);
    isr$trace.debug_all('status sMimetype', 'sMimetype: '||sMimetype, sCurrentName);
    
    oErrorObj := STB$DOCKS.insertIntoDoctrail( STB$JOB.getJobParameter('REPID',cnJobid),
                                            sIsrDocType,
                                            sFileName,
                                            sMimetype,
                                            STB$JOB.getJobParameter('USER',cnJobId),
                                            nDocid,
                                            STB$JOB.getJobParameter('REPORTTYPEID',cnJobid) ,
                                            sDocDefinition);
    
    --mark the document to be deleted when job fails
    update stb$doctrail
       set job_flag = 'D'
     where docid = nDocId;
    commit;

    if isr$XML.ValueOf(xXml, 'count(/config/apps/app[@name="wordmodul" and command="CAN_RUN_REPORT"]['|| i ||']/properties)') = 0 then
      isr$XML.CreateNode(xXml, '/config/apps/app[@name="wordmodul" and command="CAN_RUN_REPORT"]['|| i ||']','properties', '');
   end if;
      
    -- get oid
    open cGetDocOid(nDocId);
   fetch cGetDocOid into sDocOid;
    close cGetDocOid;
    
    isr$XML.CreateNode(xXml, '/config/apps/app[@name="wordmodul" and command="CAN_RUN_REPORT"]['|| i ||']/properties', 'custom-property', sDocOid );
    isr$XML.SetAttribute(xXml, '/config/apps/app[@name="wordmodul" and command="CAN_RUN_REPORT"]['|| i ||']/properties/custom-property[position()=last()]', 'name', csDocPropOid);
    
    if  isr$XML.ValueOf(xXml, '/config/apps/app[@name="wordmodul" and command="CAN_RUN_REPORT"]['||i ||']/doc_definition/text()') = 'esig' then
      isr$trace.debug_all('esig doc - master oid','sDoc0id: '||sDocOid, sCurrentName);
      -- get the master oid of all other documents in the config xml. file
      isr$XML.CreateNode(xXml, '/config/apps/app[@name="wordmodul" and command="CAN_RUN_REPORT"]['|| i ||']/properties', 'custom-property',  isr$XML.ValueOf(xXml, '/config/apps/app[@name="wordmodul" and command="CAN_RUN_REPORT" and doc_definition != "esig"]/properties/custom-property[@name="DOC_OID"]/text()') );
      isr$XML.SetAttribute(xXml, '/config/apps/app[@name="wordmodul" and command="CAN_RUN_REPORT"]['|| i ||']/properties/custom-property[position()=last()]', 'name', 'MASTER_OID');
   end if;
    
      
  end loop;
    
  -- check out write the new doc oid's
  for i in 1..ISR$XML.valueOf(xXml,'count(/config/apps/app[@name="wordmodul" and command="CAN_CHECKOUT"])') loop
    
    nDocId := STB$JOB.getJobParameter('REFERENCE',cnJobid);
    
    isr$XML.CreateNode(xXml, '/config/apps/app[@name="wordmodul" and command="CAN_CHECKOUT"]['|| i ||']', 'docid', nDocId);

    -- get oid
    open cGetDocOid(STB$JOB.getJobParameter('REFERENCE',cnJobid));
   fetch cGetDocOid into sDocOid;
    close cGetDocOid;

    if isr$XML.ValueOf(xXml, 'count(/config/apps/app[@name="wordmodul" and command="CAN_CHECKOUT"]['|| i ||']/properties)') = 0 then
      isr$XML.CreateNode(xXml, '/config/apps/app[@name="wordmodul" and command="CAN_CHECKOUT"]['|| i ||']','properties', '');
   end if;
      
    isr$XML.CreateNode(xXml, '/config/apps/app[@name="wordmodul" and command="CAN_CHECKOUT"]['|| i ||']/properties', 'custom-property', sDocOid );
    isr$XML.SetAttribute(xXml, '/config/apps/app[@name="wordmodul" and command="CAN_CHECKOUT"]['|| i ||']/properties/custom-property[position()=last()]', 'name', csDocPropOid);
     
  end loop;
   
  -- write user's properties
  for i in 1..ISR$XML.valueOf(xXml,'count(/config/apps/app[@name="wordmodul"])') loop
  
    -- if not final then write all other document properties
    if STB$JOB.getJobParameter('FINALFLAG', cnJobid) is  null  then
  
      -- check if the doc id isn't null  added JB 17 Nov 2008
      if STB$JOB.getJobParameter('DOCID',cnJobid) is not null then
        isr$trace.debug_all('status docid', 'STB$JOB.getJobParameter(DOCID,cnJobid): '||STB$JOB.getJobParameter('DOCID',cnJobid), sCurrentName);
        
        for rGetPropertiesSetByUser in cGetPropertiesSetByUser(ISR$XML.valueOf(xXml,'/config/apps/app[@name="wordmodul"]['||i||']/properties/custom-property[@name="'||csDocPropOid||'"]/text()'),
                                                             STB$JOB.getJobParameter('DOCID',cnJobid)) loop
          if isr$XML.ValueOf(xXml, 'count(/config/apps/app[@name="wordmodul"]['||i||']/properties)') = 0 then
            isr$XML.CreateNode(xXml, '/config/apps/app[@name="wordmodul"]['||i||']','properties/text()', '');
         end if;
          isr$XML.CreateNode(xXml, '/config/apps/app[@name="wordmodul"]['||i||']/properties', 'custom-property', rGetPropertiesSetByUser.parametervalue);
          isr$XML.SetAttribute(xXml, '/config/apps/app[@name="wordmodul"]['||i||']/properties/custom-property[position()=last()]', 'name', rGetPropertiesSetByUser.parametername);
        end loop;   
      
      elsif nDocid is not null then
         isr$trace.debug_all('status nDocid', 'nDocid: '||nDocid, sCurrentName);
         for rGetPropertiesSetByUser in cGetPropertiesSetByUser(ISR$XML.valueOf(xXml,'/config/apps/app[@name="wordmodul"]['||i||']/properties/custom-property[@name="'||csDocPropOid||'"]/text()'),
                                                            nDocid ) loop
          if isr$XML.ValueOf(xXml, 'count(/config/apps/app[@name="wordmodul"]['||i||']/properties)') = 0 then
            isr$XML.CreateNode(xXml, '/config/apps/app[@name="wordmodul"]['||i||']','properties', '');
         end if;
          if isr$XML.valueOf(xXml, 'count((/config/apps/app[@name="wordmodul"])['||i||']/properties/custom-property[@name="'||rGetPropertiesSetByUser.parametername||'"])') = 0 then
            isr$XML.CreateNode(xXml, '/config/apps/app[@name="wordmodul"]['||i||']/properties', 'custom-property', rGetPropertiesSetByUser.parametervalue);
            isr$XML.SetAttribute(xXml, '/config/apps/app[@name="wordmodul"]['||i||']/properties/custom-property[position()=last()]', 'name', rGetPropertiesSetByUser.parametername);
         end if;
        end loop;   
      
     end if;

      -- call the customer package
      execute immediate
        'BEGIN
          :1 := ' || STB$UTIL.getCurrentCustomPackage || '.writeCustomDocProperties(:2, :3);
         end;'
      using out oErrorObj, in out xXml, in cnJobid;
       
   end if;

  end loop;
  
  -- over the config xml file get the targets and create an entry in the doctrail
  for i in 1..ISR$XML.valueOf(xXml,'count(/config/apps/app[@name="wordmodul" and command="CAN_MODIFY_WITH_COMMON_DOC"])') loop
    isr$trace.debug_all('for i in 1..ISR$XML.valueOf()','i: '||i, sCurrentName);
    
    if STB$JOB.getJobParameter('TOKEN',cnJobid) = csOffshoot then
      exit;
   end if;
    isr$trace.debug_all('status docid', 'STB$JOB.getJobParameter(DOCID,cnJobid): '||STB$JOB.getJobParameter('DOCID',cnJobid), sCurrentName);

    sIsrDocType     := isr$XML.valueOf(xXml, '/config/apps/app[@name="wordmodul" and command="CAN_MODIFY_WITH_COMMON_DOC"]['|| i ||']/isrdoctype/text()');
    sFileName       := isr$XML.valueOf(xXml, '/config/apps/app[@name="wordmodul" and command="CAN_MODIFY_WITH_COMMON_DOC"]['|| i ||']/target/text()');
    sMimetype       := isr$XML.valueOf(xXml, '/config/apps/app[@name="wordmodul" and command="CAN_MODIFY_WITH_COMMON_DOC"]['|| i ||']/mimetype/text()');
    sDocDefinition  := isr$XML.valueOf(xXml, '/config/apps/app[@name="wordmodul" and command="CAN_MODIFY_WITH_COMMON_DOC"]['|| i ||']/doc_definition/text()');   

    isr$trace.debug_all('status sDocDefinition', 'sDocDefinition: '||sDocDefinition, sCurrentName);
    isr$trace.debug_all('status sMimetype', 'sMimetype: '||sMimetype, sCurrentName);
    
    oErrorObj := STB$DOCKS.insertIntoDoctrail( STB$JOB.getJobParameter('REPID',cnJobid),
                                            sIsrDocType,
                                            sFileName,
                                            sMimetype,
                                            STB$JOB.getJobParameter('USER',cnJobId),
                                            nDocid,
                                            STB$JOB.getJobParameter('REPORTTYPEID',cnJobid) ,
                                            sDocDefinition);  
    
    --mark the document to be deleted when job fails
    update stb$doctrail
       set job_flag = 'D'
     where docid = nDocId;
    commit;

    if isr$XML.ValueOf(xXml, 'count(/config/apps/app[@name="wordmodul" and command="CAN_MODIFY_WITH_COMMON_DOC"]['|| i ||']/properties)') = 0 then
      isr$XML.CreateNode(xXml, '/config/apps/app[@name="wordmodul" and command="CAN_MODIFY_WITH_COMMON_DOC"]['|| i ||']','properties', '');
   end if;
      
    -- get oid
    open  cGetDocOid(nDocId);
    fetch cGetDocOid into sDocOid;
    close cGetDocOid;
    
    isr$XML.CreateNode(xXml, '/config/apps/app[@name="wordmodul" and command="CAN_MODIFY_WITH_COMMON_DOC"]['|| i ||']/properties', 'custom-property', sDocOid );
    isr$XML.SetAttribute(xXml, '/config/apps/app[@name="wordmodul" and command="CAN_MODIFY_WITH_COMMON_DOC"]['|| i ||']/properties/custom-property[position()=last()]', 'name', csDocPropOid);
      
  end loop;
  
  open  cGetApplicationPathes(To_Number(STB$JOB.getJobParameter('USER',cnJobid)));
  fetch cGetApplicationPathes into sApplicationPathes;
  close cGetApplicationPathes;
  
  if Trim(sApplicationPathes) is not null then
    isr$XML.createNode(xXml, '/config/apps', 'applicationpathes', sApplicationPathes);
 end if;

  if Trim(ISR$XML.valueOf(xXml, '/config/apps/fullpathname/text()')) is null then
    isr$XML.deleteNode(xXml, '/config/apps/fullpathname');
 end if;
  if Trim(ISR$XML.valueOf(xXml, '/config/apps/target/text()')) is null then
    isr$XML.deleteNode(xXml, '/config/apps/target');
 end if;
  if Trim(ISR$XML.valueOf(xXml, '/config/apps/extension/text()')) is null then
    isr$XML.deleteNode(xXml, '/config/apps/extension');
 end if;
  if Trim(ISR$XML.valueOf(xXml, '/config/apps/overwrite/text()')) is null then
    isr$XML.deleteNode(xXml, '/config/apps/overwrite');
 end if;
  
  isr$trace.debug('xXml', 's. logclob', sCurrentName, xXml);
  -- create entry in STB$JOBTRAIL
  select STB$JOBTRAIL$SEQ.NEXTVAL into nRecordID from dual;
  
  
  insert into STB$JOBTRAIL (
      ENTRYID,
      JOBID,
      REFERENCE,
      TEXTFILE,
      DOCUMENTTYPE,
      MIMETYPE,
      TIMESTAMP,
      FILENAME,
      BLOBFLAG,
      DOWNLOAD
     ) values (
      nRecordID,
      cnJobid,
      cnJobid,
      xXml.getClobVal(),
      csConfig,
      'text/xml',
       sysdate ,
      'config.xml',
      'C',
      'T'
     );

  commit;

  isr$trace.debug('parameter','xXml',sCurrentName,xXml);
  isr$trace.stat('end','end',sCurrentName);
  return oErrorObj;

exception
  when others then
    sUserMsg := utd$msglib.getmsg ('exUnexpectedError', nCurrentLanguage,  csP1 => sqlerrm(sqlcode));
    sImplMsg := sCurrentName || rtrim(': '||sImplMsg,': ');
    sDevMsg  := substr(dbms_utility.format_error_stack ||crlf|| dbms_utility.format_error_backtrace, 1,4000);
    oErrorObj.handleError ( Stb$typedef.cnSeverityCritical, sUserMsg, sImplMsg, sDevMsg );  
    Stb$job.markDBJobAsBroken(cnJobId, sIp, sPort, oErrorObj.sErrorCode, oErrorObj.sErrorMessage, oErrorObj.sSeverityCode);
    return oErrorObj;
end saveConfig;


end isr$document;