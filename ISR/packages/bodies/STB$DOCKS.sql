CREATE OR REPLACE PACKAGE BODY Stb$docks
IS

-- constants
 csMasterTemplate       VARCHAR2(100) :='MASTERTEMPLATE';
 csTarget               VARCHAR2(100) :='TARGET';
 csNodeTypeReport       VARCHAR2(100) :='REPORT';
 
 sOid         STB$SYSTEMPARAMETER.PARAMETERVALUE%TYPE := STB$UTIL.getSystemParameter('SYSTEM_OID');
 sCustomer    STB$SYSTEMPARAMETER.PARAMETERVALUE%TYPE := STB$UTIL.getSystemParameter('CUSTOMER');
 sIsrVersion  STB$SYSTEMPARAMETER.PARAMETERVALUE%TYPE := STB$UTIL.getSystemParameter('ISR_VERSION');
 sIsrSystem   STB$SYSTEMPARAMETER.PARAMETERVALUE%TYPE := STB$UTIL.getSystemParameter('ISR_SYSTEM');
 sUserName    varchar2 (255) := STB$SECURITY.getOracleUserName (STB$SECURITY.getCurrentUser); 
--********************************************************************************************************************************
FUNCTION getDateFormat(csToken IN ISR$OVERVIEW$REPORTS.TOKEN%TYPE DEFAULT NULL) RETURN VARCHAR2
IS
  sCurrentName   constant varchar2(4000) := $$PLSQL_UNIT||'.getDateFormat('||csToken||')';
  sDateFormat VARCHAR2(30);

  CURSOR cGetDateFormatWithToken IS
    SELECT value
      FROM ISR$OVERVIEW$REPORTS ior
     WHERE ior.token = csToken
       AND ior.parameter = 'DATEFORMAT'
       AND ior.path = '/*/*';

  CURSOR cGetDateFormat IS
    SELECT value
      FROM ISR$OVERVIEW$REPORTS ior, STB$JOBPARAMETER jp, STB$JOBSESSION js
     WHERE js.oracleJobSessionId = STB$SECURITY.getCurrentSession
       AND js.jobid = jp.jobid
       AND jp.parametername = 'TOKEN'
       AND ior.token = jp.parametervalue
       AND ior.parameter = 'DATEFORMAT'
       AND ior.path = '/*/*';
BEGIN
  isr$trace.stat('begin', 'begin', sCurrentName);
  IF csToken IS NULL THEN
    OPEN cGetDateFormat;
    FETCH cGetDateFormat INTO sDateFormat;
    CLOSE cGetDateFormat;
  ELSE
    OPEN cGetDateFormatWithToken;
    FETCH cGetDateFormatWithToken INTO sDateFormat;
    CLOSE cGetDateFormatWithToken;
  END IF;
  IF TRIM(sDateFormat) IS NULL THEN
   -- sDateFormat := STB$UTIL.GetSystemParameter('DATEFORMAT');
   sDateFormat := ISR$UTIL.getInternalDate;
  END IF;
  isr$trace.stat('end', sDateFormat, sCurrentName);
  RETURN sDateFormat;
END;

--********************************************************************************************************************************
function saveXMLReport( xXml       in xmltype,
                        cnJobid    in number,
                        csFileName in varchar2 )
  return STB$OERROR$RECORD
  is pragma autonomous_transaction;
  oErrorObj             STB$OERROR$RECORD := STB$OERROR$RECORD();
  sCurrentName          constant varchar2(100) := $$PLSQL_UNIT||'.saveXMLReport('||cnJobid||')';
  xNewXml               xmltype;
  xDataXml              xmltype;
  xXml1                 xmltype;
  xHead                 xmltype;
  clHead                clob;
  sGroupStatement       varchar2(4000);
  sGroupOrderStatement  varchar2(4000);
  sOrderStatement       varchar2(4000);
  sXMLSqlStatement      varchar2(4000);
begin
  isr$trace.stat('begin','csFileName: '||csFileName,sCurrentName);
  isr$trace.debug('value','xXml IN',sCurrentName,xXml);

  xNewXml := xXml;

  IF UPPER(NVL(STB$JOB.getJobParameter('FILTER_AUDITAPPLICATION', cnJobId), STB$UTIL.getSystemParameter('AUDITAPPLICATION'))) =  UPPER('excel.exe') THEN
    ISR$XML.setAttribute(xNewXml, '/*[1]', 'customer', sCustomer);
    ISR$XML.setAttribute(xNewXml, '/*[1]', 'isr_version', sIsrVersion);
    ISR$XML.setAttribute(xNewXml, '/*[1]', 'enviroment', sIsrSystem);
  END IF;
  ISR$XML.setAttribute(xNewXml, '/*[1]', 'createdon', TO_CHAR (SYSDATE, STB$DOCKS.getDateFormat(STB$JOB.getJobParameter('TOKEN', cnJobId))));
  ISR$XML.setAttribute(xNewXml, '/*[1]', 'createdby', sUserName);
  ISR$XML.setAttribute(xNewXml, '/*[1]', 'title',  UTD$MSGLIB.getMsg ('REPORT_TITLE_PART1', STB$SECURITY.getCurrentLanguage)
                                               || TO_CHAR (SYSDATE, STB$DOCKS.getDateFormat(STB$JOB.getJobParameter('TOKEN', cnJobId)))
                                               || UTD$MSGLIB.getMsg ('REPORT_TITLE_PART2', STB$SECURITY.getCurrentLanguage)
                                               || sUserName
                                               || UTD$MSGLIB.getMsg ('REPORT_TITLE_PART3', STB$SECURITY.getCurrentLanguage));

  ISR$XML.deleteNode(xNewXml, '//@num');

  -- display unfiltered report
  isr$trace.debug('overview_report_unfiltered.xml', 'xNewXml',sCurrentName,xNewXml);

  -- filter tags
  isr$trace.debug('filter tags', 'INVISIBLE_FILTER_REPORT_ALL: ' ||STB$JOB.getJobParameter('INVISIBLE_FILTER_REPORT_ALL', cnJobid) || '; FILTER_REPORT_ALL ' ||
                STB$JOB.getJobParameter('FILTER_REPORT_ALL', cnJobid), sCurrentName);
  IF NVL(NVL(STB$JOB.getJobParameter('INVISIBLE_FILTER_REPORT_ALL', cnJobid), STB$JOB.getJobParameter('FILTER_REPORT_ALL', cnJobid)), 'F') = 'F' THEN
    FOR rGetFilterParameter IN ( -- string comparison
                                 SELECT '//'||REPLACE (REPLACE (parametername , 'FILTER_'), 'INVISIBLE_')||'[text()!="'||parametervalue||'"]'||
                                              CASE
                                                WHEN REPLACE (REPLACE (parametername , 'FILTER_'), 'INVISIBLE_') LIKE '@%' THEN ''
                                                ELSE '/..'
                                              END xpath
                                     , '//'||REPLACE (REPLACE (parametername , 'FILTER_'), 'INVISIBLE_') hideXpath
                                  FROM stb$jobparameter
                                 WHERE parametername LIKE '%FILTER_%'
                                   AND parametername NOT LIKE '%DATE%'
                                   AND parametername NOT LIKE '%TIMESTAMP%'
                                   AND parametername NOT LIKE '%LOGLEVELNAME%'
                                   AND parametername NOT LIKE 'INVISIBLE_FILTER_BEGIN%RAW'
                                   AND parametername NOT LIKE 'INVISIBLE_FILTER_END%RAW'
                                   AND TRIM(parametervalue) IS NOT NULL
                                   AND jobid = cnJobid
                                 UNION
                                -- date comparison
                                SELECT '//'||REPLACE(REPLACE(REPLACE(NVL(j1.parametername, j2.parametername), 'INVISIBLE_FILTER_'), 'BEGIN'), 'END')||'[not('
                                            ||case
                                                when j1.parametervalue is not null then 'text() >= "'||j1.parametervalue||'"'
                                                else ''
                                              end
                                            ||case
                                                when j1.parametervalue is not null and j2.parametervalue is not null then ' and '
                                                else ''
                                              end
                                            ||case
                                                when j2.parametervalue is not null then 'text() <= "'||j2.parametervalue||'"'
                                                else ''
                                              end||')]'||
                                              CASE
                                                WHEN REPLACE (NVL(j1.parametername, j2.parametername) , 'INVISIBLE_FILTER_') LIKE '@%' THEN ''
                                                ELSE '/..'
                                              END
                                      , null hideXpath
                                  FROM stb$jobparameter j1, stb$jobparameter j2
                                 WHERE j1.parametername like 'INVISIBLE_FILTER_BEGIN%RAW'
                                   AND TRIM(j1.parametervalue) IS NOT NULL
                                   AND j1.jobid = cnJobid
                                   AND j2.parametername like 'INVISIBLE_FILTER_END%RAW'
                                   AND TRIM(j2.parametervalue) IS NOT NULL
                                   AND j2.jobid = cnJobid) LOOP
      BEGIN
        isr$trace.debug('rGetFilterParameter.xpath',rGetFilterParameter.xpath,sCurrentName,xNewXml);
        ISR$XML.deleteNode(xNewXml, rGetFilterParameter.xpath);
        -- think about this
        IF rGetFilterParameter.hideXpath IS NOT NULL THEN
          isr$trace.debug('rGetFilterParameter.hideXpath',rGetFilterParameter.hideXpath,sCurrentName,xNewXml);
          ISR$XML.deleteNode(xNewXml, rGetFilterParameter.hideXpath);
        END IF;
      EXCEPTION
        WHEN others THEN
        -- doesn't matter, filter not set
        isr$trace.debug(sqlerrm(sqlcode),'others',sCurrentName);
      END;
    END LOOP;

    -- display filtered report
    isr$trace.debug('overview_report_filtered.xml','display filtered report',sCurrentName,xNewXml);

    SELECT XMLELEMENT (
               "FILTER"
             , (SELECT XMLAGG(XMLELEMENT (
                                 evalname (parametername)
                               , XMLATTRIBUTES (visible AS "visible"
                                             , (select UTD$MSGLIB.getMsg (description, STB$SECURITY.getCurrentLanguage) 
                                                  from isr$dialog d1
                                                 where d1.token IN (STB$JOB.getJobParameter ('TOKEN', cnJobid))
                                                   and d1.parametername = jp.parametername) AS "translation")
                               , parametervalue
                              ))
                  FROM (SELECT DISTINCT
                               REPLACE(REPLACE(REPLACE ( REPLACE (parametername, 'INVISIBLE_') , 'FILTER_' ), '@'), '$') parametername
                             , parametervalue
                             , CASE WHEN INSTR (parametername, 'INVISIBLE') = 0 THEN 'T'
                                    ELSE 'F'
                               END visible
                          FROM stb$jobparameter
                         WHERE jobid = cnJobid
                           AND parametername LIKE '%FILTER_%'
                           AND parametername NOT LIKE '%LOGLEVELNAME%'
                        ORDER BY 1) jp)
            )
       INTO xXml1
       FROM DUAL;

    BEGIN
      ISR$XML.InsertChildXMLFragment(xNewXml, '/*[1]', ISR$XML.XMLToClob(xXml1));
    EXCEPTION WHEN OTHERS THEN
      isr$trace.debug(SQLCODE,'EXCEPTION WHEN OTHERS: '||SQLERRM,sCurrentName);
    END;
  END IF;

  isr$trace.debug('select cols','select cols '||STB$JOB.getJobParameter('TOKEN', cnJobid),sCurrentName,xNewXml);
  FOR rGetPath IN (SELECT distinct path, SUBSTR (path, 2, INSTR (path, '/', -1) - 2) tag1, SUBSTR (path, INSTR (path, '/', -1) + 1) tag2
                     FROM isr$overview$reports
                    WHERE token = STB$JOB.getJobParameter('TOKEN', cnJobid)
                      AND parameter != 'DATEFORMAT') LOOP
    isr$trace.debug('tag1',rGetPath.tag1,sCurrentName);
    isr$trace.debug('tag2',rGetPath.tag2,sCurrentName);

    xDataXml := null;

    sGroupStatement := null;
    FOR rGetGroupStatement IN ( SELECT VALUE, NVL(info, ''' %'||value||'% ''') text, visible
                                  FROM isr$overview$reports
                                 WHERE token = STB$JOB.getJobParameter ('TOKEN', cnJobid)
                                   AND parameter IN ('GROUP')
                                   AND PATH = rGetPath.PATH
                                ORDER BY ordernumber ) LOOP
      IF rGetGroupStatement.visible = 'T' THEN
        sGroupStatement := sGroupStatement || REPLACE(rGetGroupStatement.text, '%'||rGetGroupStatement.value||'%', '''|| EXTRACTVALUE (t3.COLUMN_VALUE, ''/'||rGetPath.tag2||'/'||rGetGroupStatement.value||''') ||''')||'||';
        isr$trace.debug('sGroupStatement',sGroupStatement,sCurrentName);
      END IF;
      sGroupOrderStatement := sGroupOrderStatement || REPLACE(rGetGroupStatement.text, '%'||rGetGroupStatement.value||'%', '''|| EXTRACTVALUE (t3.COLUMN_VALUE, ''/'||rGetPath.tag2||'/'||rGetGroupStatement.value||''') ||''')||'||';
      isr$trace.debug('sGroupOrderStatement',sGroupOrderStatement,sCurrentName);
    END LOOP;

    sOrderStatement := null;
    FOR rGetOrderStatement IN ( SELECT VALUE
                                  FROM isr$overview$reports
                                 WHERE token = STB$JOB.getJobParameter ('TOKEN', cnJobid)
                                   AND parameter IN ('ORDER')
                                   AND PATH = rGetPath.PATH
                                ORDER BY ordernumber ) LOOP
      sOrderStatement := sOrderStatement || 'EXTRACTVALUE (t3.COLUMN_VALUE, ''/'||rGetPath.tag2||'/'||rGetOrderStatement.value||'''),';
      isr$trace.debug('value','sOrderStatement',sCurrentName,sOrderStatement);
    END LOOP;

    sXMLSqlStatement :=

    'SELECT XMLELEMENT (
              "'||rGetPath.tag1||'"
            , (SELECT XMLAGG(XMLELEMENT (
                                "'||rGetPath.tag2||'"
                              '|| case when trim(sGroupStatement) is not null then ', XMLATTRIBUTES('||RTRIM(sGroupStatement, '||')||' AS "REFERENCE")' end ||'
                              , (SELECT XMLAGG (
                                           XMLELEMENT (
                                              evalname (VALUE)
                                            , CASE
                                                WHEN VALUE = ''MODIFIEDON'' AND token LIKE ''%AUDIT'' THEN
                                                  TO_CHAR (TO_DATE (''01.01.2000'', ''dd.mm.yyyy'') + TO_NUMBER(EXTRACTVALUE (t2.COLUMN_VALUE, ''/*'')) / 60 / 60 / 24, STB$DOCKS.getDateFormat (token))
                                                  -- do no longer display the date twice
                                                  --|| '' ''
                                                  --|| TO_CHAR (TO_DATE (''01.01.2000'', ''dd.mm.yyyy'') + TO_NUMBER(EXTRACTVALUE (t2.COLUMN_VALUE, ''/*'')) / 60 / 60 / 24)
                                                -- [ISRC-1206] for esig
                                                WHEN VALUE = ''DATE'' AND token LIKE ''%ESIG%'' THEN
                                                   EXTRACTVALUE (t2.COLUMN_VALUE, ''/*'')||'' ''||extract(TIMEZONE_REGION from (cast(sysdate as timestamp) at local))                                              
                                                ELSE
                                                  EXTRACTVALUE (t2.COLUMN_VALUE, ''/*'')
                                              END
                                           )
                                              ORDER BY ordernumber
                                        )
                                   FROM (SELECT value, NVL(info, ''%''||value||''%'') info, ordernumber, token
                                           FROM isr$overview$reports
                                          WHERE token = STB$JOB.getJobParameter(''TOKEN'', '||cnJobid||')
                                            AND visible = ''T''
                                            AND parameter = ''COL''
                                            AND path = '''||rGetPath.path||''') t1
                                      , table(XMLSEQUENCE(EXTRACT (t3.COLUMN_VALUE, ''/'||rGetPath.tag2||'/*''))) t2
                                  WHERE (t2.COLUMN_VALUE.getrootelement () = t1.VALUE AND (t1.VALUE != ''MODIFIEDON'' OR t1.token NOT LIKE ''%AUDIT''))
                                     OR (t2.COLUMN_VALUE.getrootelement () = ''TIMESTAMPRAW'' AND t1.VALUE = ''MODIFIEDON'' AND t1.token LIKE ''%AUDIT''))
                             )
                                '|| case when trim(sOrderStatement) is not null or trim(sGroupOrderStatement) is not null then 'ORDER BY ' end
                                 ||RTRIM(sGroupOrderStatement, '||')
                                 || case when trim(sGroupOrderStatement) is not null and trim(sOrderStatement) is not null then ',' end
                                 ||RTRIM(sOrderStatement, ',')||'
                      )
                 FROM table(XMLSEQUENCE(EXTRACT (:1, '''||rGetPath.path||'''))) t3)
           )
              AS xml
      FROM DUAL';

    isr$trace.debug('dynSql','sXMLSqlStatement',sCurrentName,sXMLSqlStatement);

    EXECUTE IMMEDIATE sXMLSqlStatement INTO xDataXml USING IN xNewXml;

    isr$trace.debug('code position','xDataXml after execution',sCurrentName,xDataXml);

    ISR$XML.deleteNode(xNewXml, rGetPath.path);
    isr$trace.debug('code position','xNewXml after deletion',sCurrentName,xNewXml);

    SELECT INSERTCHILDXML (xNewXml
                         , '/*[1]'
                         , rGetPath.tag2
                         , EXTRACT (xDataXml, rGetPath.PATH))
      INTO xNewXml
      FROM DUAL;
    --isr$trace.debug('code position','xNewXml after insertion',sCurrentName,xNewXml);

  END LOOP;

  -- display ordered report
 -- isr$trace.debug('overview_report_ordered.xml','xNewXml',sCurrentName,xNewXml);

  isr$trace.debug('code position','before translate tags',sCurrentName);
  SELECT XMLELEMENT ("TRANSLATION", (SELECT XMLAGG (XMLELEMENT ("TRANSLATIONENTRY", XMLATTRIBUTES (parametername AS "KEY"), parametervalue))
                                       FROM (SELECT VALUE parametername, UTD$MSGLIB.getMsg (VALUE||'$DESC', STB$SECURITY.getCurrentLanguage) parametervalue
                                               FROM isr$overview$reports
                                              WHERE token = STB$JOB.getJobParameter ('TOKEN', cnJobid)
                                                AND visible = 'T'
                                                AND parameter = 'COL'
                                             UNION
                                             SELECT parametername, UTD$MSGLIB.getMsg (description, STB$SECURITY.getCurrentLanguage) parametervalue
                                               FROM ISR$DIALOG
                                              WHERE token IN (STB$JOB.getJobParameter ('TOKEN', cnJobid))
                                                AND NVL (displayed, 'T') = 'T'
                                             UNION
                                             SELECT parametername||'_DISPLAY', UTD$MSGLIB.getMsg (parametername||'_DISPLAY', STB$SECURITY.getCurrentLanguage) parametervalue
                                               FROM ISR$DIALOG
                                              WHERE token IN (STB$JOB.getJobParameter ('TOKEN', cnJobid))
                                                AND NVL (displayed, 'T') = 'T'
                                                AND hasLov = 'T'
                                             UNION
                                             SELECT 'FILTER' parametername, UTD$MSGLIB.getMsg ('FILTER', STB$SECURITY.getCurrentLanguage) parametervalue FROM DUAL
                                             UNION
                                             SELECT 'MASTERDATA' parametername, UTD$MSGLIB.getMsg ('MASTERDATA', STB$SECURITY.getCurrentLanguage) parametervalue FROM DUAL)))
    INTO xXml1
    FROM DUAL;

  ISR$XML.InsertChildXMLFragment(xNewXml, '/*[1]', ISR$XML.XMLToClob(xXml1));

  isr$trace.debug('overview_report_complete.xml','xNewXml',sCurrentName,xNewXml);

  IF TRIM(SUBSTR (ISR$XML.xmlToClob(xNewXml), 0, DBMS_LOB.INSTR (ISR$XML.xmlToClob(xNewXml), '?>'))) IS NULL THEN
    xHead := ISR$XML.initXml('test');
    clHead := ISR$XML.xmlToClob(xHead);
    clHead := SUBSTR (clHead, 0, DBMS_LOB.INSTR (clHead, '?>') + 1);
  END IF;
  isr$trace.debug('clHead',clHead, sCurrentName);
  
  -- create entry in STB$JOBTRAIL
  INSERT INTO STB$JOBTRAIL (ENTRYID
                          , JOBID
                          , REFERENCE
                          , TEXTFILE
                          , DOCUMENTTYPE
                          , MIMETYPE
                          , TIMESTAMP
                          , FILENAME
                          , BLOBFLAG
                          , DOWNLOAD)
     (SELECT STB$JOBTRAIL$SEQ.NEXTVAL
           , cnJobid
           , cnJobid
           , clHead||ISR$XML.xmlToClob(xNewXml)
           , 'ROHDATEN'
           , 'text/xml'
           , SYSDATE
           , UPPER(csFileName)
           , 'C'
           , 'T'
        FROM DUAL);
  isr$trace.debug('insert rows for ' || csFileName,sql%rowcount,sCurrentName);
  commit;
  isr$trace.stat('end','end',sCurrentName);
  return oErrorObj;
exception
  when others then
      oErrorObj.handleError ( Stb$typedef.cnSeverityCritical, utd$msglib.getmsg ('exUnexpectedError', stb$security.getCurrentlanguage,  csP1 => sqlerrm(sqlcode)), 
        sCurrentName, dbms_utility.format_error_stack ||chr(10)|| dbms_utility.format_error_backtrace );
    return oErrorObj;
end saveXMLReport;

--****************************************************************************************************************************
procedure copyDocProperties( cnDocId    in STB$DOCTRAIL.DOCID%TYPE,
                             cnOldDocId in STB$DOCTRAIL.DOCID%TYPE)
is
  sCurrentName     constant varchar2(100) := $$PLSQL_UNIT||'.copyDocProperties('||cnDocId||')';
  sParameterValue  varchar2(4000);
  nStartTimeNum    number      := DBMS_UTILITY.GET_TIME;

  cursor curGetProperties( ccnDocId STB$DOCTRAIL.DOCID%TYPE )  is
    select *
    from   isr$doc$property
    where  docid = ccnDocId;

begin
  isr$trace.stat('begin','cnDocId: '||cnDocId||',  cnOldDocId: '||cnOldDocId,sCurrentName);
  IF cnOldDocId != 0 THEN
    INSERT INTO ISR$DOC$PROPERTY (DOCID
                                , PARAMETERNAME
                                , PARAMETERVALUE
                                , PARAMETERDISPLAY
                                , MODIFIEDON
                                , MODIFIEDBY)
       (SELECT cnDocId
             , PARAMETERNAME
             , PARAMETERVALUE
             , PARAMETERDISPLAY
             , SYSDATE
             , sUserName
          FROM ISR$DOC$PROPERTY
         WHERE docid = cnOldDocId);
  ELSE
    -- get the default values
    INSERT INTO ISR$DOC$PROPERTY (DOCID
                                , PARAMETERNAME
                                , PARAMETERVALUE
                                , PARAMETERDISPLAY
                                , MODIFIEDON
                                , MODIFIEDBY)
       (SELECT cnDocId
             , PARAMETERNAME
             , PARAMETERDEFAULT
             , PARAMETERDEFAULT
             , SYSDATE
             , sUserName
          FROM ISR$DOC$PROP$TEMPLATE p, STB$DOCTRAIL d, STB$REPORT r
         WHERE d.docid = cnDocId
           AND d.nodeid = r.repid
           AND r.reporttypeid = p.reporttypeid);

    FOR rGetDocProperties IN curGetProperties(cnDocId) LOOP
      IF nvl(instr(rGetDocProperties.parametervalue,':exec:'),0) > 0 THEN
        BEGIN
          execute immediate REPLACE(rGetDocProperties.parametervalue, ':exec:') INTO sParameterValue;
        EXCEPTION WHEN OTHERS THEN
          sParameterValue := null;
          isr$trace.warn(rGetDocProperties.parametername, SQLERRM||' '||REPLACE(rGetDocProperties.parametervalue, ':exec:'), sCurrentName);
        END;
        UPDATE isr$doc$property
           SET parametervalue = sParameterValue, parameterdisplay = sParameterValue
         WHERE docid = cnDocid
           AND parametername = rGetDocProperties.parametername;
      END IF;
    END LOOP;
  END IF;

  -- ISRC-1044
  for rGetRepid in (select TO_NUMBER (nodeid) repid, 'GRP$' || dp.parametername groupingname, r.condition
                      from stb$doctrail d, isr$doc$property dp, stb$report r
                     where r.repid = TO_NUMBER (nodeid)
                       and dp.docid = cnDocid
                       and dp.docid = d.docid) loop
    isr$trace.debug ('rGetRepid.repid '||rGetRepid.repid||' rGetRepid.groupingname '||rGetRepid.groupingname, 'rGetRepid.condition '||rGetRepid.condition, sCurrentName);                       
    delete from ISR$REPORT$GROUPING$TABLE
     where repid = rGetRepid.repid
       and groupingname = rGetRepid.groupingname; 
    if rGetRepid.condition != 0 then            
      insert into ISR$REPORT$GROUPING$TABLE (GROUPINGNAME
                                           , GROUPINGID
                                           , GROUPINGVALUE
                                           , GROUPINGDATE
                                           , REPID)
        (select rg.GROUPINGNAME
              , rg.GROUPINGID
              , rg.GROUPINGVALUE
              , rg.GROUPINGDATE
              , rg.REPID
           from isr$report$grouping rg
          where rg.repid = rGetRepid.repid
            and groupingname = rGetRepid.groupingname);
    end if;        
  end loop;         
         
  isr$trace.stat('end','elapsed time: '||isr$util.getElapsedTime(nStartTimeNum),sCurrentName);
end copyDocProperties;

--****************************************************************************************************************************
PROCEDURE createChecksum(cnDocId IN NUMBER, sFlagOk OUT VARCHAR2, sBlobflag IN VARCHAR2)
IS
  sCurrentName      constant varchar2(4000) := $$PLSQL_UNIT||'.createChecksum('||cnDocId||')';
  oErrorObj         STB$OERROR$RECORD := STB$OERROR$RECORD();
  sBlobSelected     BLOB;
  sClobSelected     CLOB;
  nCheckSum         NUMBER;

BEGIN
  isr$trace.stat('begin', 'begin', sCurrentName);

  sFlagOK := 'T';

  IF sBlobFlag = 'B' THEN

    --  get the blob
    SELECT CONTENT INTO sBlobSelected
      FROM STB$DOCTRAIL
     WHERE docId = cnDocId;

  isr$trace.debug('after blob', 'after lob', sCurrentName);

    -- create checksum
    oErrorObj := Stb$util.checksum(sBlobSelected,nCheckSum);

  ELSE

    SELECT XMLFILE INTO sClobSelected
     FROM STB$DOCTRAIL
    WHERE docId=cnDocId;

  isr$trace.debug('after clob',  'after lob', sCurrentName);

    -- create checksum
    oErrorObj := Stb$util.checksum(sClobSelected,nCheckSum);

  END IF;

  isr$trace.debug('nCheckSum',  'nCheckSum: '|| nCheckSum , sCurrentName);

  BEGIN

  UPDATE STB$DOCTRAIL
       set checksum = nCheckSum
     WHERE docid = cnDocId;

    EXCEPTION
      WHEN OTHERS THEN
       oErrorObj.handleError ( Stb$typedef.cnSeverityCritical, utd$msglib.getmsg ('exUnexpectedError', stb$security.getCurrentlanguage,  csP1 => sqlerrm(sqlcode)), 
         sCurrentName, dbms_utility.format_error_stack ||chr(10)|| dbms_utility.format_error_backtrace );
        sFlagOK  := 'F';
        ROLLBACK;
  END;

  COMMIT;
  isr$trace.stat('end', 'end', sCurrentName);

EXCEPTION
WHEN OTHERS THEN
      oErrorObj.handleError ( Stb$typedef.cnSeverityCritical, utd$msglib.getmsg ('exUnexpectedError', stb$security.getCurrentlanguage,  csP1 => sqlerrm(sqlcode)), 
        sCurrentName, dbms_utility.format_error_stack ||chr(10)|| dbms_utility.format_error_backtrace );
  sFlagOK  :='F';
  ROLLBACK;
END createChecksum;

--******************************************************************************************************************************
FUNCTION CheckDocExistsForNodeId(csNodeId IN VARCHAR2,sExists OUT VARCHAR2) RETURN STB$OERROR$RECORD
IS
  sCurrentName   constant varchar2(4000) := $$PLSQL_UNIT||'.CheckDocExistsForNodeId('||csNodeId||')';
  oErrorObj               STB$OERROR$RECORD := STB$OERROR$RECORD();

      -- Cursor to get the nodeid the docid belongs to
  CURSOR cGetNodeIdOfDoc IS
    SELECT 'T'
      FROM STB$DOCTRAIL
     WHERE nodeid = csNodeId
     AND doctype='D';

BEGIN
  isr$trace.stat('begin', 'begin', sCurrentName);

  OPEN cGetNodeIdOfDoc;
  FETCH cGetNodeIdOfDoc INTO sExists;
  IF cGetNodeIdOfDoc%NOTFOUND  OR sExists  IS NULL THEN
    isr$trace.debug('cGetNodeIdOfDoc%notFound ','cGetNodeIdOfDoc%notFound for : ' ||csNodeId, sCurrentName);

  sExists :='F';
  END IF;
  CLOSE cGetNodeIdOfDoc;
  isr$trace.stat('end', 'end', sCurrentName);
  RETURN oErrorObj;
EXCEPTION
  WHEN OTHERS THEN
      oErrorObj.handleError ( Stb$typedef.cnSeverityCritical, utd$msglib.getmsg ('exUnexpectedError', stb$security.getCurrentlanguage,  csP1 => sqlerrm(sqlcode)), 
        sCurrentName, dbms_utility.format_error_stack ||chr(10)|| dbms_utility.format_error_backtrace );
  RETURN oErrorObj;
END  CheckDocExistsForNodeId;

--******************************************************************************************************************************
FUNCTION deleteCommon RETURN STB$OERROR$RECORD
IS
  sCurrentName   constant varchar2(4000) := $$PLSQL_UNIT||'.deleteCommon';
  nDocId                  NUMBER;
  oErrorObj               STB$OERROR$RECORD := STB$OERROR$RECORD();
BEGIN
isr$trace.stat('begin', 'begin', sCurrentName);

  nDocId  := Stb$object.GetCurrentDocId;

  DELETE FROM ISR$DOC$PROPERTY WHERE docid =nDocId;
  DELETE FROM STB$DOCTRAIL WHERE docid =nDocId;

  isr$trace.stat('end', 'end', sCurrentName);
  RETURN oErrorObj;
EXCEPTION
  WHEN OTHERS THEN
      oErrorObj.handleError ( Stb$typedef.cnSeverityCritical, utd$msglib.getmsg ('exUnexpectedError', stb$security.getCurrentlanguage,  csP1 => sqlerrm(sqlcode)), 
        sCurrentName, dbms_utility.format_error_stack ||chr(10)|| dbms_utility.format_error_backtrace );
  RETURN oErrorObj;
END deleteCommon;

--******************************************************************************************************************************
FUNCTION getMaxDocIdInSRNumber(cnRepId IN NUMBER,
                               nDocId OUT NUMBER,
                               csDocType IN VARCHAR2,
                               csDocDefinition IN VARCHAR2) RETURN STB$OERROR$RECORD
IS
  sCurrentName   constant varchar2(4000) := $$PLSQL_UNIT||'.getMaxDocIdInSRNumber('||cnRepId||')';
  oErrorObj               STB$OERROR$RECORD := STB$OERROR$RECORD();
  nDocIdTemp              NUMBER;

  -- Cursor to retrieve the maximum doc id
  CURSOR cGetHighestDoc(cnRepid IN NUMBER) IS
    SELECT Max (docId) docId
    FROM   STB$DOCTRAIL
    WHERE  nodeid = To_Char (cnRepid)
    AND    (doctype = csDocType OR csDocType IS NULL)
    AND    (doc_definition = csDocDefinition OR csDocDefinition IS NULL);


----------------------- altered JB  30 Aug 2008 ---------------------------------------------------
    CURSOR cGetSrNo  IS
    SELECT title, srnumber
    FROM   STB$REPORT
    WHERE  repId = cnRepId;

     rGetSrNo cGetSrNo%ROWTYPE;

BEGIN
  isr$trace.stat('begin','csDocType: '||csDocType||' csDocDefinition: '||csDocDefinition, sCurrentName);

  -- initalize the docid
  nDocId := 0;

  OPEN cGetSRNo;
  FETCH cGetSRNo INTO  rGetSrNo ;-- sSrNumber;
  IF cGetSRNo%NOTFOUND THEN
    CLOSE cGetSRNo;
    isr$trace.debug('cGetSRNo%notFound ','cGetSRNo%notFound for : ' ||cnRepId, sCurrentName);
  RETURN oErrorObj;
  END IF;
  CLOSE cGetSRNo;

  isr$trace.debug('sTitle','sTitle : '|| rGetSrNo.Title, sCurrentName);

    FOR rRepSRNumbers IN
     (SELECT repid
        FROM STB$REPORT
       WHERE title = rGetSrNo.Title
         AND srnumber =rGetSrNo.srnumber
        ORDER BY repid)
      LOOP

     --get the highest docid
     OPEN cGetHighestDoc(rRepSRNumbers.repid);
     FETCH cGetHighestDoc INTO nDocIdTemp;
     CLOSE cGetHighestDoc;

     isr$trace.debug('nDocIdTemp','nDocIdTemp : '|| nDocIdTemp, sCurrentName);

     IF nDocIdTemp> nDocId THEN
       nDocId := nDocIdTemp;
     END IF;

       nDocIdTemp :='';
     END LOOP;

  isr$trace.stat('end', 'end', sCurrentName);
  RETURN oErrorObj;
EXCEPTION
  WHEN OTHERS THEN
      oErrorObj.handleError ( Stb$typedef.cnSeverityCritical, utd$msglib.getmsg ('exUnexpectedError', stb$security.getCurrentlanguage,  csP1 => sqlerrm(sqlcode)), 
        sCurrentName, dbms_utility.format_error_stack ||chr(10)|| dbms_utility.format_error_backtrace );
  RETURN oErrorObj;
END getMaxDocIdInSRNumber;

--******************************************************************************************************************************
FUNCTION getMaxDocIdForRepId(cnRepId IN NUMBER,
                             nDocId OUT NUMBER,
                             sCheckedOut OUT VARCHAR2,
                             csDocType IN VARCHAR2,
                             csDocDefinition IN VARCHAR2) RETURN STB$OERROR$RECORD
IS
  sCurrentName   constant varchar2(4000) := $$PLSQL_UNIT||'.getMaxDocIdForRepId('||cnRepId||')';
  oErrorObj               STB$OERROR$RECORD := STB$OERROR$RECORD();

  -- Cursor to retrieve the highest docId
  CURSOR cGetHighestDoc IS
    SELECT docid, checkedout
    FROM   STB$DOCTRAIL
    WHERE  docid =
             (SELECT Max (docId) docId
              FROM   STB$DOCTRAIL
              WHERE  To_Char (nodeid) = cnRepId
              AND    (doctype = csDocType OR csDocType IS NULL)
              AND    (doc_definition = csDocDefinition OR csDocDefinition IS NULL));

BEGIN
  isr$trace.stat('begin', 'csDocType: ' || csDocType || ' csDocDefinition: '|| csDocDefinition, sCurrentName);

  OPEN cGetHighestDoc;
  FETCH cGetHighestDoc INTO nDocId,sCheckedOut;
  IF cGetHighestDoc%NOTFOUND THEN
    isr$trace.debug('cGetHighestDoc%notFound ','not found', sCurrentName);
  END IF;
  CLOSE cGetHighestDoc;

  isr$trace.stat('end', 'end', sCurrentName);
  RETURN oErrorObj;
EXCEPTION
  WHEN OTHERS THEN
      oErrorObj.handleError ( Stb$typedef.cnSeverityCritical, utd$msglib.getmsg ('exUnexpectedError', stb$security.getCurrentlanguage,  csP1 => sqlerrm(sqlcode)), 
        sCurrentName, dbms_utility.format_error_stack ||chr(10)|| dbms_utility.format_error_backtrace );
  RETURN oErrorObj;
END getMaxDocIdForRepId;

--************************************************************************************************************************
PROCEDURE controlChecksum(cnDocId IN NUMBER, sFlagOk OUT VARCHAR2)
IS
  sCurrentName   constant varchar2(4000) := $$PLSQL_UNIT||'.controlChecksum('||cnDocId||')';

  oErrorObj         STB$OERROR$RECORD := STB$OERROR$RECORD();
  sBlobSelected     BLOB;
  nCheckSum         NUMBER;
  nOldChecksum      NUMBER;

BEGIN
  isr$trace.stat('begin', 'begin', sCurrentName);

  sFlagOK :='F';

  -- create lob
  SELECT CONTENT, checksum INTO sBlobSelected, nOldCheckSum
    FROM STB$DOCTRAIL
   WHERE docId=cnDocId;

    isr$trace.debug('nOldCheckSum',' nOldCheckSum: '  || nOldCheckSum , sCurrentName);

   --create the checksum
  oErrorObj :=Stb$util.checksum(sBlobSelected, nCheckSum);

  isr$trace.debug('nCheckSum',' nCheckSum: '|| nCheckSum  , sCurrentName);

  IF To_Char(Nvl(nOldChecksum,0))=To_Char(Nvl(nCheckSum,0)) THEN
    sFlagOK  :='T';
  ELSE
    sFlagOK  :='F';
  END IF;

  isr$trace.stat('end', 'sFlagOk: '||sFlagOk, sCurrentName);

EXCEPTION
WHEN OTHERS THEN
      oErrorObj.handleError ( Stb$typedef.cnSeverityCritical, utd$msglib.getmsg ('exUnexpectedError', stb$security.getCurrentlanguage,  csP1 => sqlerrm(sqlcode)), 
        sCurrentName, dbms_utility.format_error_stack ||chr(10)|| dbms_utility.format_error_backtrace );
  sFlagOK  :='F';
  ROLLBACK;
END controlChecksum;

--*************************************************************************************************************************
function getDocNodeType(csDoctype in varchar2) return varchar2 result_cache
is

  sCurrentName   constant varchar2(4000) := $$PLSQL_UNIT||'.getDocNodeType('||csDoctype||')';
  sReturn        varchar2(500);
 
  cursor cGetNodeType(sDoctype in varchar2) is
    select DECODE(sDoctype,'C','COMMONDOC','D','DOCUMENT','X','RAWDATA','L','LINK','S','REMOTESNAPSHOT', 'W', 'DOCUMENT_WORD', 'DOCUMENT')
      from dual;
begin

  isr$trace.stat('begin', 'csDoctype: '||csDoctype, sCurrentName);

  open cGetNodeType(csDoctype);
  fetch cGetNodeType into sReturn;
  close cGetNodeType;
  
  isr$trace.stat('end', 'sReturn: '||sReturn, sCurrentName);
  
  return sReturn;

end getDocNodeType; 

--*************************************************************************************************************************
FUNCTION switchCheckOutFlag(cnDocId IN number,
                            csFlag IN varchar2,
                            cnUserNo IN number,
                            csIP IN varchar2 DEFAULT NULL,
                            csFullPath IN varchar2 DEFAULT NULL,
                            csFileName IN varchar2 DEFAULT NULL) RETURN STB$OERROR$RECORD
IS
  sCurrentName      constant varchar2(4000) := $$PLSQL_UNIT||'.switchCheckOutFlag('||cnDocId||')';
  oErrorObj         STB$OERROR$RECORD := STB$OERROR$RECORD();
BEGIN
  isr$trace.stat('begin', 'begin', sCurrentName);

  UPDATE STB$DOCTRAIL
     set checkedout = csFlag,
         machine = csIP,
         Path = csFullPath,
         filename = Nvl(csFileName,filename),
         modifiedon = sysdate,
         modifiedby = Stb$security.getOracleUserName(cnUserNo),
         oracleuser = Stb$security.getOracleUserName(cnUserNo)
   WHERE DocId = cnDocId;

  COMMIT;
  
  for rGetDocs in ( select docid, doctype
                      from stb$doctrail
                     where docid = cnDocId) loop
    ISR$TREE$PACKAGE.sendSyncCall(stb$docks.getDocNodetype(rGetDocs.doctype), rGetDocs.docid);                        
  end loop; 

  isr$trace.stat('end', 'end', sCurrentName);
  RETURN oErrorObj;
EXCEPTION
WHEN OTHERS THEN
  ROLLBACK;
      oErrorObj.handleError ( Stb$typedef.cnSeverityCritical, utd$msglib.getmsg ('exUnexpectedError', stb$security.getCurrentlanguage,  csP1 => sqlerrm(sqlcode)), 
        sCurrentName, dbms_utility.format_error_stack ||chr(10)|| dbms_utility.format_error_backtrace );
  RETURN oErrorObj;
END switchCheckOutFlag;

--************************************************************************************************************************
FUNCTION LastDocCheckedIn(sCheckededOut OUT VARCHAR2, nDocId OUT NUMBER, sPath OUT VARCHAR2, sMachine OUT VARCHAR2, sOracleuser OUT VARCHAR2) RETURN STB$OERROR$RECORD
IS
  sCurrentName   constant varchar2(4000) := $$PLSQL_UNIT||'.LastDocCheckedIn('||' '||')';
  oError         STB$OERROR$RECORD := STB$OERROR$RECORD();
  sSrnumber      STB$REPORT.SRNUMBER%TYPE;
  sTitle         STB$REPORT.TITLE%TYPE;

  CURSOR cGetStatus(csSrnumber IN VARCHAR2, csTitle IN VARCHAR2) IS
    SELECT checkedout
         , PATH
         , machine
         , oracleuser
         , docid
      FROM STB$DOCTRAIL
     WHERE nodeid IN (SELECT TO_CHAR (repid)
                        FROM stb$report
                       WHERE srnumber = csSrnumber
                         AND title = csTitle)
       AND checkedout = 'T'
       AND doctype = 'D';

  -- Cursor to retrieve the srnumber/title
  CURSOR cGetReport IS
    SELECT DISTINCT srnumber, title
      FROM STB$REPORT
     WHERE title = Stb$object.GetCurrentTitle
       AND reporttypeid = Stb$object.GetCurrentReporttypeid;

  CURSOR cGetrMAxDocID(csSrnumber IN VARCHAR2, csTitle IN VARCHAR2) IS
    SELECT MAX (docid) docid
      FROM STB$DOCTRAIL
     WHERE nodeid IN (SELECT TO_CHAR (repid)
                        FROM stb$report
                       WHERE srnumber = csSrnumber
                         AND title = csTitle)
       AND doctype = 'D';

BEGIN
  isr$trace.stat('begin','begin', sCurrentName);

  --get sr number
  OPEN cGetReport;
  FETCH cGetReport INTO sSrnumber, sTitle;
  CLOSE cGetReport;

  isr$trace.debug('nDocId','sSrnumber:' || sSrnumber, sCurrentName);

  -- get checked out doc if existing
  OPEN cGetStatus(sSrnumber, sTitle);
  FETCH cGetStatus INTO sCheckededOut,sPath,sMachine,sOracleuser,nDocId ;
  CLOSE cGetStatus;
  isr$trace.debug(nDocId,'sOracleuser:' || sOracleuser ||'   sMachine:' || sMachine ||'  sPath:' || sPath, sCurrentName);

  IF nDocId IS NULL OR nDocId = 0 THEN
    sCheckededOut := 'F';

    -- get the max Doc of the repid
    OPEN cGetrMAxDocID(sSrnumber, sTitle);
    FETCH cGetrMAxDocID INTO nDocId;
    CLOSE cGetrMAxDocID;

  ELSE
    sCheckededOut := 'T';
  END IF;

  isr$trace.stat('end', 'end', sCurrentName);
  RETURN oError;

  EXCEPTION
    WHEN OTHERS THEN
      oError.handleError ( Stb$typedef.cnSeverityCritical, utd$msglib.getmsg ('exUnexpectedError', stb$security.getCurrentlanguage,  csP1 => sqlerrm(sqlcode)), 
        sCurrentName, dbms_utility.format_error_stack ||chr(10)|| dbms_utility.format_error_backtrace );
      RETURN oError;
  END LastDocCheckedIn;

--**********************************************************************************************************************************
FUNCTION getDocStatus(sDocStatus OUT VARCHAR2)  RETURN STB$OERROR$RECORD
IS
  sCurrentName   constant varchar2(4000) := $$PLSQL_UNIT||'.getDocStatus('||' '||')';
  oError               STB$OERROR$RECORD := STB$OERROR$RECORD();
  nDocId               NUMBER;

  CURSOR cGetDocStatus IS
    SELECT checkedout
      FROM STB$DOCTRAIL
     WHERE docId =nDocId;

BEGIN
  isr$trace.stat('begin', 'begin', sCurrentName);
  nDocId := Stb$object.GetCurrentDocId;

  isr$trace.debug('nDocId ','nDocId: ' || nDocId, sCurrentName );

  OPEN cGetDocStatus;
  FETCH cGetDocStatus INTO sDocStatus;
  CLOSE cGetDocStatus;

  isr$trace.stat('end', 'end', sCurrentName);

RETURN oError;
EXCEPTION
  WHEN NO_DATA_FOUND THEN
      oError.handleError ( Stb$typedef.cnSeverityCritical, utd$msglib.getmsg ('exUnexpectedError', stb$security.getCurrentlanguage,  csP1 => sqlerrm(sqlcode)), 
        sCurrentName, dbms_utility.format_error_stack ||chr(10)|| dbms_utility.format_error_backtrace );
    RETURN oError;
  WHEN OTHERS THEN
      oError.handleError ( Stb$typedef.cnSeverityCritical, utd$msglib.getmsg ('exUnexpectedError', stb$security.getCurrentlanguage,  csP1 => sqlerrm(sqlcode)), 
        sCurrentName, dbms_utility.format_error_stack ||chr(10)|| dbms_utility.format_error_backtrace );
    RETURN oError;
END getDocStatus;

--*********************************************************************************************************************************
FUNCTION DocExists(sFlag OUT VARCHAR2, sDocType IN VARCHAR2, sStatus OUT VARCHAR2) RETURN STB$OERROR$RECORD
IS
  sCurrentName   constant varchar2(4000) := $$PLSQL_UNIT||'.DocExists('||' '||')';
  oError                  STB$OERROR$RECORD := STB$OERROR$RECORD();
  nDocId                  NUMBER;
  nStartTimeNum    number      := DBMS_UTILITY.GET_TIME;

  CURSOR cGetDocId IS
    SELECT Max(docId) docid
    FROM STB$DOCTRAIL
    WHERE nodeid = To_Char(Stb$object.GetCurrentRepId)
    AND doctype = sDocType;

  CURSOR cGetStatus IS
    SELECT checkedout
    FROM STB$DOCTRAIL
   WHERE docid=nDocId;

BEGIN
  isr$trace.stat('begin','sDocType: ' ||  sDocType, sCurrentName);

  OPEN cGetDocId;
  FETCH cGetDocId INTO nDocId;
  CLOSE cGetDocId;

  isr$trace.debug('nDocId','nDocId : '|| nDocId, sCurrentName);

  -- check if exists if no then nDocid =0
  IF nDocId IS NULL THEN
    sFlag :='F';
  ELSE
    sFlag :='T';
    OPEN cGetStatus;
    FETCH cGetStatus INTO sStatus;
    CLOSE cGetStatus;
  END IF;

  isr$trace.stat('end','elapsed time: '||isr$util.getElapsedTime(nStartTimeNum), sCurrentName);
RETURN oError;

EXCEPTION
 WHEN NO_DATA_FOUND THEN
      oError.handleError ( Stb$typedef.cnSeverityCritical, utd$msglib.getmsg ('exUnexpectedError', stb$security.getCurrentlanguage,  csP1 => sqlerrm(sqlcode)), 
        sCurrentName, dbms_utility.format_error_stack ||chr(10)|| dbms_utility.format_error_backtrace );
   RETURN oError;
 WHEN OTHERS THEN
      oError.handleError ( Stb$typedef.cnSeverityCritical, utd$msglib.getmsg ('exUnexpectedError', stb$security.getCurrentlanguage,  csP1 => sqlerrm(sqlcode)), 
        sCurrentName, dbms_utility.format_error_stack ||chr(10)|| dbms_utility.format_error_backtrace );
   RETURN oError;
END DocExists;

--***********************************************************************************************************************************************
function copyDocToJob( cnDocId      in number,
                       cnJobId      in number,
                       cblBlob      in stb$doctrail.content%type default EMPTY_BLOB,
                       csAsFilename in varchar2 default null )
  return STB$OERROR$RECORD
is
  oError STB$OERROR$RECORD := STB$OERROR$RECORD();
  sCurrentName       constant varchar2(100) := $$PLSQL_UNIT||'.copyDocToJob('||cnJobId||')';
  sMsg               varchar2(4000);
  sDocType              STB$JOBTRAIL.DOCUMENTTYPE%TYPE:=csTarget;
  blBlob                STB$DOCTRAIL.CONTENT%TYPE;
  sFoundType            STB$JOBTRAIL.DOCUMENTTYPE%TYPE;
  
  cursor cGetDoc (cnSearchDocid number) is
    select content, doctype  
    from STB$DOCTRAIL 
    where docid = cnSearchDocid;
    
    
begin
  isr$trace.stat('begin','cnDocId : '||cnDocId || ',  cnJobId: '||cnJobId||',  length(cblBlob): '||length(cblBlob)||',  csAsFilename: '||csAsFilename, sCurrentName);

  -- UNZIP
    if DBMS_LOB.GETLENGTH(cblBlob) < 1 then

      -- select content into blBlob from STB$DOCTRAIL where docid = cnDocid and doctype = 'D';
      open cGetDoc( cnDocid );
      fetch cGetDoc into blBlob, sFoundType;
      
      if  cGetDoc%FOUND then

        if sFoundType in('D', 'W') then
            isr$trace.debug('before unzip', DBMS_LOB.GETLENGTH(blBlob), sCurrentName);
            blBlob := STB$JAVA.unzip(blBlob);
            IF DBMS_LOB.GETLENGTH(blBlob) = 0 THEN
              blBlob := null;
            END IF;
            isr$trace.debug('after unzip', DBMS_LOB.GETLENGTH(blBlob), sCurrentName);
        ELSE -- somthing else is done, not a .DOC file
            blBLob := null;
        end if;       
     else -- nothing found
        isr$trace.error(SQLCODE,  SQLERRM, sCurrentName);
        blBLob := null;
     end if;    
     close cGetDoc;
  else -- a file was passed as argument
       blBlob := cblBlob;
  end if;


  INSERT INTO STB$JOBTRAIL( JOBID, DOCUMENTTYPE, MIMETYPE, DOCSIZE,
                            TIMESTAMP, BINARYFILE, FILENAME, BLOBFLAG,
     ENTRYID, REFERENCE,LEADINGDOC, DOWNLOAD,DOC_DEFINITION)
   ( select cnJobId, sDocType, MIMETYPE, DOCSIZE,
            sysdate, nvl(blBlob,CONTENT), nvl(csAsFilename,FILENAME), 'B',
                     STB$JOBTRAIL$SEQ.NEXTVAL,to_char(DocId), 
                     case when DOCTYPE in ('D','W') then 'T' when DOCTYPE = 'C' then 'F' else 'F' end,
                      'T', DOC_DEFINITION
     from   STB$DOCTRAIL
     where  docid = cnDocid );

  isr$trace.stat('end','end ', sCurrentName);
  return oError;

EXCEPTION
WHEN OTHERS THEN
  sMsg := utd$msglib.getmsg ('exUnexpectedError', stb$security.getcurrentlanguage,  sqlerrm(sqlcode));
  oError.handleError ( Stb$typedef.cnSeverityCritical, sMsg, sCurrentName, dbms_utility.format_error_stack ||chr(10)|| dbms_utility.format_error_backtrace );
  RETURN oError;
END copyDocToJob;

---***********************************************************************************************************************************************
FUNCTION getPasswordFromDoc(csReference IN VARCHAR2,sPassword OUT VARCHAR2) RETURN STB$OERROR$RECORD
IS
  sCurrentName   constant varchar2(4000) := $$PLSQL_UNIT||'.getPasswordFromDoc('||' '||')';
  oError        STB$OERROR$RECORD := STB$OERROR$RECORD();
  nRepid        NUMBER;

   CURSOR cGetRepid(sReference IN VARCHAR2) IS
    SELECT To_Number(nodeid)
      FROM STB$DOCTRAIL
     WHERE To_Char(docId) = sReference;
BEGIN
   isr$trace.stat('begin', 'begin', sCurrentName);

   OPEN cGetRepid(csReference);
   FETCH cGetRepid INTO nRepid;
   CLOSE cGetRepid;

   -- the password, reference is the report-id
   sPassword := STB$UTIL.getPassword(To_Number(nRepid));

   isr$trace.stat('end', 'end', sCurrentName);
  RETURN oError;

EXCEPTION
WHEN NO_DATA_FOUND THEN
      oError.handleError ( Stb$typedef.cnSeverityCritical, utd$msglib.getmsg ('exUnexpectedError', stb$security.getCurrentlanguage,  csP1 => sqlerrm(sqlcode)), 
        sCurrentName, dbms_utility.format_error_stack ||chr(10)|| dbms_utility.format_error_backtrace );
  RETURN oError;
WHEN OTHERS THEN
      oError.handleError ( Stb$typedef.cnSeverityCritical, utd$msglib.getmsg ('exUnexpectedError', stb$security.getCurrentlanguage,  csP1 => sqlerrm(sqlcode)), 
        sCurrentName, dbms_utility.format_error_stack ||chr(10)|| dbms_utility.format_error_backtrace );
  RETURN oError;
END getPasswordFromDoc;

--***********************************************************************************************************************************************
FUNCTION copyFileToJob(cnId IN NUMBER,cnJobId IN NUMBER, csBlobFlag IN VARCHAR2) RETURN STB$OERROR$RECORD
IS
  sCurrentName   constant varchar2(4000) := $$PLSQL_UNIT||'.copyFileToJob('||cnId||')';
  oError STB$OERROR$RECORD := STB$OERROR$RECORD();
BEGIN
  isr$trace.stat('begin', 'begin', sCurrentName);
  isr$trace.debug('parameters','cnId : ' || cnId || ' csBlobFlag: ' || csBlobFlag || ' cnJobId ' || cnJobId, sCurrentname);

--BLOB----------------------------------------------------------
  IF csBlobFlag='B' THEN
    INSERT INTO STB$JOBTRAIL (
       JOBID, DOCUMENTTYPE,
       MIMETYPE,DOCSIZE, TIMESTAMP, BINARYFILE,
       FILENAME, BLOBFLAG,
       ENTRYID, REFERENCE, DOWNLOAD,DOC_DEFINITION)
     (SELECT
       cnJobId, USAGE,MIMETYPE, DOCSIZE,
       Sysdate,BINARYFILE,FILENAME,'B' ,
       STB$JOBTRAIL$SEQ.NEXTVAL,To_Char(fileid), DOWNLOAD,DOC_DEFINITION
     FROM ISR$FILE
     WHERE fileid=cnId);
   END IF;
 --CLOB------------------------------------------------------------
   IF csBlobFlag='C' THEN
    INSERT INTO STB$JOBTRAIL (
       JOBID, DOCUMENTTYPE,
       MIMETYPE,DOCSIZE, TIMESTAMP, TEXTFILE,
       FILENAME, BLOBFLAG,
       ENTRYID, REFERENCE, DOWNLOAD,DOC_DEFINITION)
     (SELECT
       cnJobId, USAGE,MIMETYPE, DOCSIZE,
       Sysdate, TEXTFILE,FILENAME,'C' ,
       STB$JOBTRAIL$SEQ.NEXTVAL,To_Char(fileid), DOWNLOAD,DOC_DEFINITION
     FROM ISR$FILE
     WHERE fileid=cnId);
   END IF;

  isr$trace.stat('end', 'end', sCurrentName);
  RETURN oError;

EXCEPTION
WHEN OTHERS THEN
      oError.handleError ( Stb$typedef.cnSeverityCritical, utd$msglib.getmsg ('exUnexpectedError', stb$security.getCurrentlanguage,  csP1 => sqlerrm(sqlcode)), 
        sCurrentName, dbms_utility.format_error_stack ||chr(10)|| dbms_utility.format_error_backtrace );
  RETURN oError;
END copyFileToJob;

--***********************************************************************************************************************************************
FUNCTION copyXMLToDoc(cnEntryid IN NUMBER,csNodeType IN VARCHAR2 , csNodeId IN VARCHAR2,
                        cnUserNo IN NUMBER, cnReportTypeId IN NUMBER, csDoctype in varchar2) RETURN STB$OERROR$RECORD
IS
  sCurrentName    constant varchar2(100) := $$PLSQL_UNIT||'.copyXMLToDoc';
  sMsg                     varchar2(4000);
  oError                   STB$OERROR$RECORD := STB$OERROR$RECORD();
  sCurrentUser             VARCHAR2(100);
  sFlagOk                  VARCHAR2(1):='T';
  exCheckSumError          EXCEPTION;
  nDocId                   NUMBER;
  sExists                  VARCHAR2(1):='F';
  nMaxDocId                NUMBER;

  clClob                   STB$JOBTRAIL.TEXTFILE%TYPE;
  blBlob                   STB$JOBTRAIL.BINARYFILE%TYPE;
  sFileName                STB$JOBTRAIL.FILENAME%TYPE;
  sBlobFlag                STB$JOBTRAIL.BLOBFLAG%TYPE := 'C';

  CURSOR cGetNodeIdOfDoc IS
    SELECT 'T'
    FROM   STB$DOCTRAIL
    WHERE  nodeid = csNodeId
    AND    doctype = csDoctype;

BEGIN
  iSR$Trace.stat('begin', 'csNodeId  : ' ||csNodeId  || ' cnEntryid ' || cnEntryid ||' csNodeType ' || csNodeType , sCurrentName);

  -- first check if there is not a raw data file already for the nodeid---
  OPEN cGetNodeIdOfDoc;
  FETCH cGetNodeIdOfDoc INTO sExists;
  IF cGetNodeIdOfDoc%NOTFOUND  OR sExists  IS NULL THEN
  sExists :='F';
  END IF;
  CLOSE cGetNodeIdOfDoc;

  iSR$Trace.Debug('sExists',sExists, sCurrentName);

  -- get the highest base Docid
  oError := getMaxDocIdInSRNumber(csNodeId, nMaxDocId, csDoctype, NULL);

  iSR$Trace.Info('nMaxDocId', nMaxDocId, sCurrentName);

  IF sExists ='F' THEN
    SELECT STB$DOC$SEQ.NEXTVAL INTO nDocId FROM dual;

    sCurrentUser := Stb$security.getOracleUserName(cnUserNo);
    iSR$Trace.Info('nDocId', nDocId, sCurrentName);

    -- ZIP
    begin
      SELECT TEXTFILE, FILENAME INTO clClob, sFileName FROM STB$JOBTRAIL WHERE entryid = cnEntryid;
      blBlob := STB$UTIL.clobToBlob(clClob);
      iSR$Trace.Info('before zip '||sFileName, DBMS_LOB.GETLENGTH(blBlob), sCurrentName);
      blBlob := STB$JAVA.zip(blBlob, sFileName);
      IF DBMS_LOB.GETLENGTH(blBlob) = 0 THEN
        blBlob := null;
      END IF;
      sBlobFlag := 'B';
      iSR$Trace.Info('after zip '||sFileName, DBMS_LOB.GETLENGTH(blBlob), sCurrentName);
    exception when others then
      isr$Trace.error(SQLCODE, SQLERRM, sCurrentName);
      blBLob := null;
    end;

    INSERT INTO STB$DOCTRAIL
                (DOCID, MIMETYPE, DOCSIZE, modifiedON, modifiedBY, XMLFILE, CONTENT, CHECKEDOUT, NODEID, NODETYPE, DOCVERSION,
                 FILENAME, DOCTYPE, VERSIONDISPLAYED, DOC_OID)
      (SELECT nDocId,
              MIMETYPE,
              NVL(DBMS_LOB.GETLENGTH(blBlob), DBMS_LOB.GETLENGTH (TEXTFILE)),
              Sysdate,
              sCurrentUser,
              DECODE(DBMS_LOB.GETLENGTH(blBlob), null, TEXTFILE, EMPTY_CLOB()),
              blBlob,
              'F',
              csNodeId,
              csNodeType,
              '0',
              filename,
              csDoctype,
              '0',
              sOid || '.' || csnodeid || '.' || nDocId
       FROM   STB$JOBTRAIL
       WHERE  entryid = cnEntryid);

    -- calcualte the checksum
    createChecksum(nDocId, sFlagOk, sBlobFlag);

    IF sFlagOk='F'THEN
      RAISE exCheckSumError;
    END IF;

    copyDocProperties(nDocId, nMaxDocId);

    COMMIT;
    
    ISR$TREE$PACKAGE.sendSyncCall('RAWDATA', nDocId);
  END IF;
  iSR$Trace.stat('end', 'end', sCurrentName);
  RETURN oError;

exception
  when exCheckSumError then
    --oError.sErrorCode      :='exCheckSumError';
    --oError.sErrorMessage   :='Error on checksum calculation';
    sMsg :=  utd$msglib.getmsg ('exCheckSumError', Stb$security.GetCurrentLanguage);
    oError.handleError(Stb$typedef.cnSeverityCritical, sMsg, sCurrentName,
                          dbms_utility.format_error_stack || CHR(10) || dbms_utility.format_error_backtrace);
    return oError;
  when no_data_found then
    sMsg :=  utd$msglib.getmsg ('exUnexpectedError', Stb$security.GetCurrentLanguage, csP1 => sqlerrm(sqlcode));
    oError.handleError(Stb$typedef.cnSeverityCritical, sMsg, sCurrentName,
                          dbms_utility.format_error_stack || CHR(10) || dbms_utility.format_error_backtrace);
    return oError;
  when others then
    sMsg :=  utd$msglib.getmsg ('exUnexpectedError', Stb$security.GetCurrentLanguage, csP1 => sqlerrm(sqlcode));
    oError.handleError(Stb$typedef.cnSeverityCritical, sMsg, sCurrentName,
                          dbms_utility.format_error_stack || CHR(10) || dbms_utility.format_error_backtrace);
    return oError;
END  copyXMLToDoc;

--**********************************************************************************************************************
FUNCTION copyXMLToJob(cnDocId IN NUMBER,cnJobId IN NUMBER, csDocumenttype in varchar2) RETURN STB$OERROR$RECORD
IS
  sCurrentName   constant varchar2(4000) := $$PLSQL_UNIT||'.copyXMLToJob('||cnDocId||')';
  oError                  STB$OERROR$RECORD := STB$OERROR$RECORD();
  --csRohdaten              VARCHAR2(100) :='ROHDATEN';

  clClob                  STB$JOBTRAIL.TEXTFILE%TYPE;
  blBlob                  STB$JOBTRAIL.BINARYFILE%TYPE;
  sBlobFlag               STB$JOBTRAIL.BLOBFLAG%TYPE := 'C';
BEGIN
  isr$trace.stat('begin', 'begin', sCurrentName);

  -- UNZIP
  begin
    select content into blBlob from STB$DOCTRAIL where docid = cnDocid;
    if blBlob is not null then
      isr$trace.debug('before unzip', DBMS_LOB.GETLENGTH(blBlob), sCurrentName);
      blBlob := STB$JAVA.unzip(blBlob);
      IF DBMS_LOB.GETLENGTH(blBlob) = 0 THEN
        blBlob := null;
      END IF;
      clClob := STB$UTIL.blobToClob(blBlob);
    end if;
    sBlobFlag := 'C';
    isr$trace.debug('after unzip', DBMS_LOB.GETLENGTH(clClob), sCurrentName);
  exception when others then
    isr$trace.error(SQLCODE,  SQLERRM, sCurrentName);
    blBLob := null;
  end;

  INSERT INTO STB$JOBTRAIL (
     JOBID, DOCUMENTTYPE,
     MIMETYPE,DOCSIZE, TIMESTAMP, TEXTFILE, BINARYFILE,
      FILENAME, BLOBFLAG,
     ENTRYID, REFERENCE, DOWNLOAD)
   (SELECT
     cnJobId, csDocumenttype, MIMETYPE, DOCSIZE,
     Sysdate, NVL(clClob, XMLFILE), blBlob, FILENAME, sBlobFlag,
     STB$JOBTRAIL$SEQ.NEXTVAL,NODEID, 'F'
  FROM STB$DOCTRAIL
  WHERE docid = cnDocId);

  COMMIT;
  isr$trace.stat('end', 'end', sCurrentName);
  RETURN oError;

EXCEPTION
WHEN NO_DATA_FOUND THEN
      oError.handleError ( Stb$typedef.cnSeverityCritical, utd$msglib.getmsg ('exUnexpectedError', stb$security.getCurrentlanguage,  csP1 => sqlerrm(sqlcode)), 
        sCurrentName, dbms_utility.format_error_stack ||chr(10)|| dbms_utility.format_error_backtrace );
  RETURN oError;
WHEN OTHERS THEN
      oError.handleError ( Stb$typedef.cnSeverityCritical, utd$msglib.getmsg ('exUnexpectedError', stb$security.getCurrentlanguage,  csP1 => sqlerrm(sqlcode)), 
        sCurrentName, dbms_utility.format_error_stack ||chr(10)|| dbms_utility.format_error_backtrace );
  RETURN oError;
END copyXMLToJob;

--***********************************************************************************************************************************************
FUNCTION copyClobToDoc(cnEntryid IN NUMBER, csNodeType IN VARCHAR2, csNodeId IN VARCHAR2, cnUserNo IN NUMBER) RETURN STB$OERROR$RECORD
IS
  sCurrentName   constant varchar2(4000) := $$PLSQL_UNIT||'.copyClobToDoc('||cnEntryid||')';
  oError                   STB$OERROR$RECORD := STB$OERROR$RECORD();
  sFlagOk                  VARCHAR2(1):='T';
  exCheckSumError          EXCEPTION;
  sDocVersion              STB$DOCTRAIL.DOCVERSION%TYPE;
  sDocVersionExt           STB$DOCTRAIL.VERSIONDISPLAYED%TYPE;
  nMaxDocId                NUMBER;
  nDocId                   STB$DOCTRAIL.DOCID%TYPE;
  nDocIdRep                STB$DOCTRAIL.DOCID%TYPE;
  sCurrentUser             VARCHAR2(100);
  sCheckOutFlag            STB$DOCTRAIL.CHECKEDOUT%TYPE;
  sContent                 BLOB;
  theblob                  BLOB;
  slXML                    CLOB;
  sltempXML                CLOB;
  BUFFER                   VARCHAR2(32000);
  v_ChunkSize              INTEGER := 32000;
  v_Offset                 INTEGER := 1;

  CURSOR cGetMaxDocVersion IS
    SELECT To_Char (To_Number (docversion) + 1) docversion
    FROM   STB$DOCTRAIL
    WHERE  docid = nMaxDocId;

  CURSOR cGetMaxDocRep IS
    SELECT To_Char (To_Number (docversion) + 1) versiondisplayed
    FROM   STB$DOCTRAIL
    WHERE  docid = nDocIdRep;

BEGIN
  isr$trace.stat('begin', 'begin', sCurrentName);
  isr$trace.debug('parameters','csNodeId  : ' ||csNodeId  || ' cnEntryid ' || cnEntryid ||' csNodeType ' || csNodeType , sCurrentName);
  oError := getMaxDocIdInSRNumber(csNodeId,nMaxDocId,STB$DOCKS.getDocType(NULL,To_Number(csNodeId)),STB$DOCKS.getDocDefType(NULL,To_Number(csNodeId)));
  isr$trace.debug('nMaxDocId',nMaxDocId, sCurrentName);

  --get the next version
  IF nMaxDocId <> 0 THEN
    OPEN cGetMaxDocVersion;
    FETCH cGetMaxDocVersion INTO sDocVersion;
    CLOSE cGetMaxDocVersion;
  ELSE
    sDocVersion :=0;
  END IF;
  isr$trace.debug('sDocVersion', sDocVersion, sCurrentName);

  -- get the next external docversion
  oError := getMaxDocIdForRepId(csNodeId,nDocIdRep,sCheckOutFlag,STB$DOCKS.getDocType(NULL,To_Number(csNodeId)),STB$DOCKS.getDocDefType(NULL,To_Number(csNodeId)));

  --get the next version
  IF nDocIdRep <>0 THEN
    OPEN cGetMaxDocRep;
    FETCH cGetMaxDocRep INTO sDocVersionExt;
    CLOSE cGetMaxDocRep;
  ELSE
    sDocVersionExt :=0;
  END IF;
  isr$trace.debug('sDocVersion',sDocVersionExt, sCurrentName);

  SELECT STB$DOC$SEQ.NEXTVAL INTO nDocId FROM dual;

  sCurrentUser := Stb$security.getOracleUserName(cnUserNo);
  isr$trace.debug('nDocId',nDocId,sCurrentName);

  INSERT INTO STB$DOCTRAIL
              (DOCID, MIMETYPE, DOCSIZE, modifiedON, modifiedBY, CONTENT, CHECKEDOUT, NODEID, NODETYPE, DOCVERSION,
               FILENAME, DOCTYPE, VERSIONDISPLAYED, DOC_OID)
    (SELECT nDocId,
            MIMETYPE,
            DBMS_LOB.GETLENGTH (TEXTFILE),
            Sysdate,
            sCurrentUser,
            Empty_Blob,
            'F',
            csNodeId,
            csNodeType,
            sDocVersion,
            FILENAME,
            'D',
            sDocVersionExt,
            sOid || '.' || csnodeid || '.' || nDocId
     FROM   STB$JOBTRAIL
     WHERE  entryid = cnEntryid);

  -- copy the clob into a temp clob
  SELECT jt.Textfile INTO slXML
    FROM STB$JOBTRAIL jt
   WHERE entryid =cnEntryid;

  dbms_lob.OPEN(slXML, dbms_lob.lob_readonly);
  dbms_lob.createTemporary( sltempXML, TRUE );
  dbms_lob.copy(sltempXML, slXML, DBMS_LOB.GETLENGTH(slXML),1, 1);
  dbms_lob.CLOSE(slXML);

  isr$trace.debug('1', DBMS_LOB.GETLENGTH(slXML), sCurrentName);

  SELECT jt.Textfile INTO slXML
    FROM STB$JOBTRAIL jt
   WHERE entryid =cnEntryid;

  DBMS_LOB.CREATETEMPORARY(theBlob, TRUE, DBMS_LOB.SESSION);
  DBMS_LOB.OPEN(theBlob, DBMS_LOB.lob_readwrite);
  isr$trace.debug('2','before loop', sCurrentName);

  WHILE v_Offset < DBMS_LOB.GETLENGTH(slXML) LOOP
    DBMS_LOB.READ (sltempXML, v_ChunkSize, v_Offset, BUFFER);
    DBMS_LOB.WRITEAPPEND(theBlob, v_ChunkSize, utl_raw.cast_to_raw(BUFFER));
    v_Offset := v_Offset + v_ChunkSize;
    isr$trace.debug('loop', v_Offset, sCurrentName);
  END LOOP;
  isr$trace.debug('3', '3', sCurrentName);

  SELECT CONTENT INTO sContent
          FROM STB$DOCTRAIL
         WHERE docId = nDocId
               FOR UPDATE;

  dbms_lob.OPEN(sContent, dbms_lob.lob_readwrite);
  dbms_lob.copy( sContent, theBlob, DBMS_LOB.GETLENGTH(theBlob),1, 1);
  isr$trace.debug('4', '4', sCurrentName);

  DBMS_LOB.CLOSE(sContent);

  isr$trace.debug('before checksum','before checksum', sCurrentName);

  -- calcualte the checksum
  createChecksum(nDocId, sFlagOk,'B');

  IF sFlagOk='F'THEN
    RAISE exCheckSumError;
  END IF;

  -- update the target reference in the jobtrail with the doc id
  UPDATE STB$JOBTRAIL
     set REFERENCE=nDociD
   WHERE entryid =cnEntryid;

  copyDocProperties(nDocId, nMaxDocId);

  COMMIT;
  isr$trace.stat('end', 'end', sCurrentName);
  RETURN oError;

EXCEPTION
WHEN exCheckSumError THEN
  oError.sErrorCode      :='exCheckSumError';
      oError.handleError ( Stb$typedef.cnSeverityCritical, 'Error on checksum calculation', 
        sCurrentName, dbms_utility.format_error_stack ||chr(10)|| dbms_utility.format_error_backtrace );
  RETURN oError;
WHEN NO_DATA_FOUND THEN
      oError.handleError ( Stb$typedef.cnSeverityCritical, utd$msglib.getmsg ('exUnexpectedError', stb$security.getCurrentlanguage,  csP1 => sqlerrm(sqlcode)), 
        sCurrentName, dbms_utility.format_error_stack ||chr(10)|| dbms_utility.format_error_backtrace );
  RETURN oError;
WHEN OTHERS THEN
      oError.handleError ( Stb$typedef.cnSeverityCritical, utd$msglib.getmsg ('exUnexpectedError', stb$security.getCurrentlanguage,  csP1 => sqlerrm(sqlcode)), 
        sCurrentName, dbms_utility.format_error_stack ||chr(10)|| dbms_utility.format_error_backtrace );
  RETURN oError;
END copyClobToDoc;

--***********************************************************************************************************************************************
FUNCTION copyCommonToDoc( cnEntryid IN number,
                         csNodeType IN varchar2,
                         csNodeId IN varchar2,
                         cnUserNo IN number,
                         csFileName IN varchar2,
                         cnDocId IN number,
                         cnReportTypeId IN number,
                         csComment IN varchar2,
                         csDoctype IN varchar2,
                         csDocDefinition IN varchar2 ) RETURN STB$OERROR$RECORD
IS
  sCurrentName   constant varchar2(4000) := $$PLSQL_UNIT||'.copyCommonToDoc('||' '||')';
  oError                   STB$OERROR$RECORD := STB$OERROR$RECORD();
  nDocId                   STB$DOCTRAIL.DOCID%TYPE;
  sCurrentUser             VARCHAR2(100);
  sIsNew                   VARCHAR2(1):='F';

  CURSOR cGetRepid(csRepid IN VARCHAR2) IS
    SELECT repid FROM stb$report WHERE repid = To_Number(csRepid);

BEGIN
  isr$trace.stat('begin', 'begin', sCurrentName);
  isr$trace.debug('parameters','csNodeId  : ' ||csNodeId  || ' cnEntryid ' || cnEntryid ||' csNodeType ' || csNodeType  , sCurrentname);

  sCurrentUser := Stb$security.getOracleUserName(cnUserNo);

  IF Trim(cnDocId) IS NOT NULL THEN
  
    nDocId := cnDocId;

    UPDATE STB$DOCTRAIL
    SET CONTENT = (SELECT BINARYFILE FROM STB$JOBTRAIL WHERE entryid = cnEntryid),
        MODIFIEDON = Sysdate,
        MODIFIEDBY = sCurrentUser,
        CHECKEDOUT = 'F',
        DOCSIZE = (SELECT DBMS_LOB.GETLENGTH(BINARYFILE) FROM STB$JOBTRAIL WHERE entryid = cnEntryid),
        CHECKIN_COMMENT = csComment,
        VERSIONDISPLAYED = (SELECT To_Char(To_Number(VERSIONDISPLAYED)+1) FROM stb$doctrail WHERE docid = cnDocId),
        DOCVERSION = (SELECT To_Char(To_Number(DOCVERSION)+1) FROM stb$doctrail WHERE docid = cnDocId),
        DOCTYPE = csDocType,
        DOC_DEFINITION = csDocDefinition
    WHERE docid = cnDocId;

    sIsNew :='F';

  ELSE

    SELECT STB$DOC$SEQ.NEXTVAL INTO nDocId FROM dual;
    sIsNew :='T';
    isr$trace.debug('nDocId',nDocId  , sCurrentName);

    INSERT INTO STB$DOCTRAIL (
      DOCID, MIMETYPE, DOCSIZE,
      MODIFIEDON, MODIFIEDBY,
      CONTENT, CHECKEDOUT, NODEID,
      NODETYPE, DOCVERSION, FILENAME,
      DOCTYPE, VERSIONDISPLAYED,
      CHECKIN_COMMENT, DOC_OID, DOC_DEFINITION )
    (SELECT nDocId, MIMETYPE, DBMS_LOB.GETLENGTH(BINARYFILE),
            Sysdate, sCurrentUser, BINARYFILE,
            'F', csNodeId, csNodeType, '0',
            FILENAME, csDocType, '0', csComment,
            sOid || '.' ||csNodeId|| '.' || nDocId, csDocDefinition
     FROM STB$JOBTRAIL
     WHERE entryid = cnEntryid);

  END IF;

  COMMIT;
  ISR$TREE$PACKAGE.sendSyncCall('COMMONDOC', nDocId);
  
  isr$trace.stat('end', 'end', sCurrentName);
  RETURN oError;

EXCEPTION
WHEN NO_DATA_FOUND THEN
      oError.handleError ( Stb$typedef.cnSeverityCritical, utd$msglib.getmsg ('exUnexpectedError', stb$security.getCurrentlanguage,  csP1 => sqlerrm(sqlcode)), 
        sCurrentName, dbms_utility.format_error_stack ||chr(10)|| dbms_utility.format_error_backtrace );
  RETURN oError;
WHEN OTHERS THEN
      oError.handleError ( Stb$typedef.cnSeverityCritical, utd$msglib.getmsg ('exUnexpectedError', stb$security.getCurrentlanguage,  csP1 => sqlerrm(sqlcode)), 
        sCurrentName, dbms_utility.format_error_stack ||chr(10)|| dbms_utility.format_error_backtrace );
  RETURN oError;
END copyCommonToDoc;

--***********************************************************************************************************************************************
FUNCTION copyCommonDocs(cnOldRepid IN NUMBER, cnNewRepId IN NUMBER) RETURN STB$OERROR$RECORD
IS
  sCurrentName       constant varchar2(4000) := $$PLSQL_UNIT||'.copyCommonDocs('||' '||')';
  oError             STB$OERROR$RECORD := STB$OERROR$RECORD();
  exCopyCommonError  EXCEPTION;
  nNewDocId          STB$DOCTRAIL.DOCID%TYPE;

  --cursor to get the docid of the commnon documents
  CURSOR cGetCommonDocs IS
    SELECT docid
    FROM STB$DOCTRAIL
   WHERE nodeid=To_Char(cnOldRepid)
     AND nodeType=csNodeTypeReport
     AND docType ='C'
     AND checkedout='F';

BEGIN
  isr$trace.stat('begin', 'begin', sCurrentName);
  isr$trace.debug('parameters','cnRepoldid ' || cnoldRepid || ' cnNewRepid: ' || cnNewRepId, sCurrentName);

  FOR rGetCommonDocs IN cGetCommonDocs LOOP
     oError := Stb$docks.copyDocToDoc(rGetCommonDocs.docid, cnNewRepId, nNewDocId);
     isr$trace.debug('nNewDocId',nNewDocId, sCurrentName);

   IF oError.sSeverityCode <> Stb$typedef.cnNoError THEN
     RAISE exCopyCommonError;
   END IF;
  END LOOP;
  isr$trace.stat('end', 'end', sCurrentName);
  RETURN oError;

EXCEPTION
WHEN exCopyCommonError THEN
  oError.sErrorCode      :=Utd$msglib.GetMsg('exCopyCommonError',Stb$security.GetCurrentLanguage);
  oError.handleError ( Stb$typedef.cnSeverityCritical, utd$msglib.getmsg ('exCopyCommonErrorText', stb$security.getCurrentlanguage,  csP1 => sqlerrm(sqlcode)), 
        sCurrentName, dbms_utility.format_error_stack ||chr(10)|| dbms_utility.format_error_backtrace );
  RETURN oError;
WHEN OTHERS THEN
      oError.handleError ( Stb$typedef.cnSeverityCritical, utd$msglib.getmsg ('exUnexpectedError', stb$security.getCurrentlanguage,  csP1 => sqlerrm(sqlcode)), 
        sCurrentName, dbms_utility.format_error_stack ||chr(10)|| dbms_utility.format_error_backtrace );
  RETURN oError;
END copyCommonDocs;

--***********************************************************************************************************************************************
FUNCTION copyDocToDoc(cnDocid IN NUMBER, cnRepid IN NUMBER, nNewDocId OUT NUMBER) RETURN STB$OERROR$RECORD
IS
  sCurrentName    constant varchar2(4000) := $$PLSQL_UNIT||'.copyDocToDoc('||' '||')';
  oError                   STB$OERROR$RECORD := STB$OERROR$RECORD();
  sDocVersion              STB$DOCTRAIL.DOCVERSION%TYPE;
  sDocVersionExt           STB$DOCTRAIL.VERSIONDISPLAYED%TYPE;
  sType                    STB$DOCTRAIL.DOCTYPE%TYPE;
  sDocDefinition           STB$DOCTRAIL.DOC_DEFINITION%TYPE;
  nMaxDocId                NUMBER;
  nDocIdRep                NUMBER;
  sCheckOutFlag            VARCHAR2(1);

  CURSOR cGetDocType IS
    SELECT doctype, doc_definition FROM stb$doctrail  --JB 07 Nov 2007
    WHERE docid= cnDocid;

   CURSOR cGetMaxDocVersion IS
    SELECT To_Char(To_Number(docversion)+1) docversion
     FROM STB$DOCTRAIL
    WHERE docid =nMaxDocId;

  CURSOR cGetMaxDocRep IS
    SELECT To_Char(To_Number(versiondisplayed)+1) versiondisplayed
     FROM STB$DOCTRAIL
    WHERE docid =nDocIdRep;

BEGIN
  isr$trace.stat('begin', 'begin', sCurrentName);
  isr$trace.debug('parameters','cnDocid : ' || cnDocid || ' cnRepid ' || cnRepid, sCurrentName);

  OPEN cGetDocType;
  FETCH cGetDocType INTO sType,sDocDefinition;
  CLOSE cGetDocType;
  isr$trace.debug(' sType', sType, sCurrentName);
  isr$trace.debug(' sDocDefinition', sDocDefinition, sCurrentName);

  IF sType in ('D', 'W')  THEN
    oError :=getMaxDocIdInSRNumber(cnRepId,nMaxDocId,sType,sDocDefinition);

   isr$trace.debug('nMaxDocId',nMaxDocId, sCurrentName);

   --get the next version
   IF nMaxDocId <>0 THEN
     OPEN cGetMaxDocVersion;
   FETCH cGetMaxDocVersion INTO sDocVersion;
   CLOSE cGetMaxDocVersion;
   ELSE
     sDocVersion :=0;
   END IF;
   isr$trace.debug('sDocVersion',sDocVersion, sCurrentName);

  -- get the next external docversion
   oError :=getMaxDocIdForRepId(cnRepId,nDocIdRep,sCheckOutFlag,sType,sDocDefinition);

  --get the next version
   IF nDocIdRep <>0 THEN
     OPEN cGetMaxDocRep;
     FETCH cGetMaxDocRep INTO sDocVersionExt;
     CLOSE cGetMaxDocRep;
   ELSE
     sDocVersionExt :=0;
   END IF;
   isr$trace.debug('sDocVersionExt',sDocVersionExt, sCurrentName);
 END IF;
  --get the newDocId
  SELECT STB$DOC$SEQ.NEXTVAL INTO nNewDocid FROM dual ;
  isr$trace.debug('nNewDocid',nNewDocid, sCurrentName);

  INSERT INTO STB$DOCTRAIL (
    DOCID, MIMETYPE, DOCSIZE,
    modifiedON ,modifiedBY,
    CONTENT,XMLFILE, MACHINE,
    Path, CHECKEDOUT, ORACLEUSER,
    NODEID,NODETYPE, DOCVERSION,
    CHECKSUM,
    FILENAME ,
    DOCTYPE,
    VERSIONDISPLAYED,
    DOC_OID,
    DOC_DEFINITION,
    EXTERNALREFERENCE)
 (SELECT nNewDocid,
    MIMETYPE, DOCSIZE,
    Sysdate,modifiedBY,
    content, XMLFILE,MACHINE,
    Path, CHECKEDOUT, ORACLEUSER,
    To_Char(cnRepid),NODETYPE, case when DOCTYPE in ('D', 'W') then sDocVersion else '0'  end,
    CHECKSUM,
    FILENAME ,
    DOCTYPE,
    case when DOCTYPE in ('D', 'W') then  sDocVersionExt else '0' end,
    STB$UTIL.getSystemParameter('SYSTEM_OID') || '.' ||cnRepid || '.' ||  nNewDocId,
    DOC_DEFINITION,
    EXTERNALREFERENCE
  FROM STB$DOCTRAIL
  WHERE docid=cnDocid);

  copyDocProperties(nNewDocid, cnDocId);

  isr$trace.stat('end', 'end', sCurrentName);
  RETURN oError;

EXCEPTION
WHEN NO_DATA_FOUND THEN
      oError.handleError ( Stb$typedef.cnSeverityCritical, utd$msglib.getmsg ('exUnexpectedError', stb$security.getCurrentlanguage,  csP1 => sqlerrm(sqlcode)), 
        sCurrentName, dbms_utility.format_error_stack ||chr(10)|| dbms_utility.format_error_backtrace );
  RETURN oError;
WHEN OTHERS THEN
      oError.handleError ( Stb$typedef.cnSeverityCritical, utd$msglib.getmsg ('exUnexpectedError', stb$security.getCurrentlanguage,  csP1 => sqlerrm(sqlcode)), 
        sCurrentName, dbms_utility.format_error_stack ||chr(10)|| dbms_utility.format_error_backtrace );
  RETURN oError;
END copyDocToDoc;

--***********************************************************************************************************************************************
FUNCTION checkDocCheckedOutByUser RETURN STB$OERROR$RECORD
IS
  sCurrentName constant VARCHAR2(100) := $$PLSQL_UNIT||'.checkDocCheckedOutByUser';
  oErrorObj                  STB$OERROR$RECORD := STB$OERROR$RECORD();
  sUserMsg         VARCHAR2(4000);
  exCheckedOutByOtherUser    EXCEPTION;
  sPath                      STB$DOCTRAIL.Path%TYPE;
  sMachine                   STB$DOCTRAIL.MACHINE%TYPE;
  sOracleuser                STB$DOCTRAIL.ORACLEUSER%TYPE;

  CURSOR cGetInfo IS
    SELECT PATH, machine, oracleuser
      FROM STB$DOCTRAIL
     WHERE docid = STB$OBJECT.GetCurrentDocId
       AND checkedout = 'T';

BEGIN
  isr$trace.stat('begin', 'begin', sCurrentName);

  OPEN cGetInfo;
  FETCH cGetInfo INTO sPath,sMachine,sOracleuser;
  CLOSE cGetInfo;

  CASE
  WHEN sOracleUser = sUserName
  AND sMachine=Stb$security.getClientIp
  AND STB$UTIL.getSystemParameter('STATIC_IP_ADDRESSES') = 'T' THEN
    -- document is checked by current user from the current machine
    isr$trace.stat('end', 'end', sCurrentName);
    RETURN oErrorObj;
  WHEN sOracleUser = sUserName
  AND STB$UTIL.getSystemParameter('STATIC_IP_ADDRESSES') = 'F' THEN
    isr$trace.stat('end', 'end', sCurrentName);
    RETURN oErrorObj;
  ELSE
    RAISE exCheckedOutByOtherUser;
  END CASE;

EXCEPTION
  WHEN exCheckedOutByOtherUser THEN

    sUserMsg := utd$msglib.getmsg ('exDocCheckedout', stb$security.getcurrentlanguage);
    oErrorObj.handleError ( Stb$typedef.cnSeverityMessage, sUserMsg, sCurrentName,
              dbms_utility.format_error_stack || CHR(10) || dbms_utility.format_error_backtrace);
    oErrorObj.sErrorMessage := utd$msglib.getmsg ('exDocCheckedoutText', stb$security.getcurrentlanguage)||' \\'||smachine||'\'||spath||' by '||soracleuser;
    RETURN oErrorObj;
  WHEN OTHERS THEN
    sUserMsg  :=Utd$msglib.GetMsg('ERROR',Stb$security.GetCurrentLanguage) || SQLCODE || ' ' || SQLERRM;
    oErrorObj.handleError ( Stb$typedef.cnSeverityCritical, sUserMsg, sCurrentName,
              dbms_utility.format_error_stack || CHR(10) || dbms_utility.format_error_backtrace);
    RETURN oErrorObj;
END checkDocCheckedOutByUser;

--***********************************************************************************************************************************************
FUNCTION insertIntoDoctrail( cnRepId IN NUMBER,
                    csDocType IN VARCHAR2,
                    csFileName IN VARCHAR2,
                    csMimeType IN VARCHAR2,
                    cnUserId IN NUMBER,
                    cnDocId OUT NUMBER,
                    cnReporttypeId IN NUMBER,
                    csDocDefinition IN varchar2 ) RETURN STB$OERROR$RECORD
IS
  sCurrentName    constant varchar2(4000) := $$PLSQL_UNIT||'.insertIntoDoctrail('||cnRepId||')';
  oError                   STB$OERROR$RECORD := STB$OERROR$RECORD();
  nMaxDocId                STB$DOCTRAIL.DOCID%TYPE;
  nDocIdRep                STB$DOCTRAIL.DOCID%TYPE;
  sDocVersion              STB$DOCTRAIL.DOCVERSION%TYPE;
  sDocVersionExt           STB$DOCTRAIL.VERSIONDISPLAYED%TYPE;
  sCheckOutFlag            STB$DOCTRAIL.CHECKEDOUT%TYPE;

  CURSOR cGetMaxDocVersion(csDocDef IN varchar2) IS
     SELECT Nvl(To_Char(To_Number(docversion)+1), 0) docversion
     FROM STB$DOCTRAIL
    WHERE docid = nMaxDocId
      AND doc_definition =csDocDef;

  CURSOR cGetMaxDocRep(csDocDef IN varchar2) IS
    SELECT Nvl(To_Char(To_Number(versiondisplayed)+1),0) versiondisplayed
     FROM STB$DOCTRAIL
    WHERE docid = nDocIdRep
    AND doc_definition =csDocDef;

BEGIN
  isr$trace.stat('begin', 'begin', sCurrentName);
  isr$trace.debug('parameters','cnRepId ' || cnRepId || ' csDocType ' || csDocType  || ' docdefinition ' || csDocDefinition, sCurrentName);

  oError := getMaxDocIdInSRNumber( cnRepId, nMaxDocId, csDocType, csDocDefinition);
  isr$trace.debug('nMaxDocId',  nMaxDocId, sCurrentName);

  --get the next version
  IF nMaxDocId <> 0 THEN
    OPEN cGetMaxDocVersion(csDocDefinition);
    FETCH cGetMaxDocVersion INTO sDocVersion;
    CLOSE cGetMaxDocVersion;
  ELSE
    sDocVersion := 0;
  END IF;
  isr$trace.debug('sDocVersion',  sDocVersion, sCurrentName);

  -- get the next external docversion
  oError := getMaxDocIdForRepId(cnRepId, nDocIdRep, sCheckOutFlag, csDocType, csDocDefinition);
  isr$trace.debug('nDocIdRep',  nDocIdRep, sCurrentName);

  --get the next version
  IF nDocIdRep <> 0 THEN
    OPEN cGetMaxDocRep(csDocDefinition);
    FETCH cGetMaxDocRep INTO sDocVersionExt;
    CLOSE cGetMaxDocRep;
  ELSE
    sDocVersionExt := 0;
  END IF;
  isr$trace.debug('sDocVersionExt',  sDocVersionExt, sCurrentName);

  SELECT STB$DOC$SEQ.NEXTVAL INTO cnDocId FROM dual;
  isr$trace.debug('cnDocId',  cnDocId, sCurrentName);

  INSERT INTO STB$DOCTRAIL (
      DOCID,
      CHECKEDOUT,
      NODEID,
      NODETYPE,
      DOC_OID,
      FILENAME,
      DOCTYPE,
      MIMETYPE,
      MODIFIEDON,
      MODIFIEDBY,
      DOCVERSION,
      VERSIONDISPLAYED,
      DOC_DEFINITION)
  VALUES ( cnDocId,
           'F',
           cnRepId,
           'REPORT',
           sOid || '.' ||cnrepid || '.' || cnDocId,
           csFileName,
           csDocType,
           csMimeType,
           Sysdate,
           Stb$security.getOracleUserName(cnUserId),
           sDocVersion,
           sDocVersionExt,
           csDocDefinition );
  isr$trace.debug('INSERTED',  'INSERTED', sCurrentName);

  copyDocProperties(cnDocId, nMaxDocId);

  isr$trace.stat('end', 'end', sCurrentName);
  RETURN oError;

EXCEPTION
WHEN NO_DATA_FOUND THEN
      oError.handleError ( Stb$typedef.cnSeverityCritical, utd$msglib.getmsg ('exUnexpectedError', stb$security.getCurrentlanguage,  csP1 => sqlerrm(sqlcode)), 
        sCurrentName, dbms_utility.format_error_stack ||chr(10)|| dbms_utility.format_error_backtrace );
  RETURN oError;
WHEN OTHERS THEN
      oError.handleError ( Stb$typedef.cnSeverityCritical, utd$msglib.getmsg ('exUnexpectedError', stb$security.getCurrentlanguage,  csP1 => sqlerrm(sqlcode)), 
        sCurrentName, dbms_utility.format_error_stack ||chr(10)|| dbms_utility.format_error_backtrace );
  RETURN oError;
END  insertIntoDoctrail;

--***********************************************************************************************************************************************
FUNCTION updateDoctrail( cnDocid IN NUMBER,
                         cnRepId IN NUMBER,
                         cnEntryid IN NUMBER,
                         csNodeType IN VARCHAR2 ,
                         csAddionalParameter IN VARCHAR2) RETURN STB$OERROR$RECORD
IS
    sCurrentName   constant varchar2(4000) := $$PLSQL_UNIT||'.updateDoctrail('||cnDocid||')';
  oError                   STB$OERROR$RECORD := STB$OERROR$RECORD();
  sFlagOk                  VARCHAR2(1) := 'T';
  exCheckSumError          EXCEPTION;

  blBlob                   STB$JOBTRAIL.BINARYFILE%TYPE;
  sFileName                STB$JOBTRAIL.FILENAME%TYPE;

  CURSOR cGetRepid(csRepid IN VARCHAR2) IS
    SELECT repid FROM stb$report WHERE repid = To_Number(csRepid);

BEGIN
  isr$trace.stat('begin', 'begin', sCurrentName);
  isr$trace.debug('parameters',' cnRepId ' || cnRepId ||
           ' csDocId ' || cnDocId ||
           ' cnEntryid ' || cnEntryid ||
           ' csNodeType ' || csNodeType ||
           ' csAddionalParameter ' || csAddionalParameter, sCurrentName);

  -- ZIP
  begin
    SELECT BINARYFILE, FILENAME INTO blBlob, sFileName FROM STB$JOBTRAIL WHERE entryid = cnEntryid;
    isr$trace.debug('before zip '||sFileName, DBMS_LOB.GETLENGTH(blBlob), sCurrentName);
    blBlob := STB$JAVA.zip(blBlob, sFileName);
    IF DBMS_LOB.GETLENGTH(blBlob) = 0 THEN
      blBlob := null;
    END IF;
    isr$trace.debug('after zip '||sFileName, DBMS_LOB.GETLENGTH(blBlob), sCurrentName);

  exception when others then
    isr$trace.error(SQLCODE,  SQLERRM, sCurrentName);
    blBLob := null;
  end;

  UPDATE stb$doctrail
  set DOCSIZE         = (SELECT DBMS_LOB.GETLENGTH(NVL(blBlob, BINARYFILE)) FROM STB$JOBTRAIL WHERE entryid = cnEntryid),
      CONTENT         = (SELECT NVL(blBlob, BINARYFILE) FROM STB$JOBTRAIL WHERE entryid = cnEntryid),
      MODIFIEDON      = Sysdate,
      MODIFIEDBY      = sUserName,
      NODETYPE        = csNodeType,
      CHECKIN_COMMENT = csAddionalParameter
  WHERE docid         = cnDocId;
  isr$trace.debug('UPDATE', 'UPDATE', sCurrentName);

  -- calcualte the checksum
  createChecksum(cnDocId, sFlagOk, 'B');

  IF sFlagOk = 'F' THEN
    NULL;
    RAISE exCheckSumError;
  END IF;
  isr$trace.debug('checksum calculated','cnDocId ' || cnDocId, sCurrentName);

  COMMIT;
  for rGetDocs in ( select docid, doctype
                      from stb$doctrail
                     where docid = cnDocId) loop
    ISR$TREE$PACKAGE.sendSyncCall(stb$docks.getDocNodetype(rGetDocs.doctype), rGetDocs.docid);                        
  end loop;  
  
  isr$trace.stat('end', 'end', sCurrentName);
  RETURN oError;

EXCEPTION
WHEN exCheckSumError THEN
  ROLLBACK;
  oError.sErrorCode      :='exCheckSumError';
  oError.handleError ( Stb$typedef.cnSeverityCritical, 'Error on checksum calculation', 
        sCurrentName, dbms_utility.format_error_stack ||chr(10)|| dbms_utility.format_error_backtrace );
  RETURN oError;
WHEN NO_DATA_FOUND THEN
  ROLLBACK;
      oError.handleError ( Stb$typedef.cnSeverityCritical, utd$msglib.getmsg ('exUnexpectedError', stb$security.getCurrentlanguage,  csP1 => sqlerrm(sqlcode)), 
        sCurrentName, dbms_utility.format_error_stack ||chr(10)|| dbms_utility.format_error_backtrace );
  RETURN oError;
WHEN OTHERS THEN
  ROLLBACK;
      oError.handleError ( Stb$typedef.cnSeverityCritical, utd$msglib.getmsg ('exUnexpectedError', stb$security.getCurrentlanguage,  csP1 => sqlerrm(sqlcode)), 
        sCurrentName, dbms_utility.format_error_stack ||chr(10)|| dbms_utility.format_error_backtrace );
  RETURN oError;
END  updateDoctrail;


--***********************************************************************************************************************************************
FUNCTION getOutput (cnRepid IN STB$REPORT.REPID%TYPE, csToken IN VARCHAR2 DEFAULT 'CAN_RUN_REPORT')
  RETURN number
is
  sCurrentName   constant varchar2(4000) := $$PLSQL_UNIT||'.getOutput('||cnRepid||')';
  nReturn NUMBER;

  CURSOR cGetOutput(cnTemplate in number) is
    select iof.outputid
      from isr$output$file iof, isr$action$output iao, isr$action ia
     where fileid = cnTemplate
       and iof.outputid = iao.outputid
       and iao.actionid = ia.actionid
       and ia.token = csToken;

BEGIN
  isr$trace.stat('begin', 'begin', sCurrentName);
  OPEN cGetOutput (stb$docks.getTemplate(cnRepid));
  FETCH cGetOutput INTO nReturn;
  CLOSE cGetOutput;

  isr$trace.stat('end', 'outputId = ' || nReturn, sCurrentName);
  RETURN nReturn;

END getOutput;

--***********************************************************************************************************************************************
FUNCTION getReportLanguage (cnRepid IN STB$REPORT.REPID%TYPE, csToken IN VARCHAR2 DEFAULT 'CAN_RUN_REPORT') RETURN number IS
    nReturn NUMBER;
    nOutputId   number;
    
    CURSOR cGetLanguage IS
    SELECT lang
      FROM isr$report$output$type
     WHERE outputid = nOutputId;
BEGIN
  nOutputId := getOutput(cnRepid, csToken);
  OPEN cGetLanguage;
  FETCH cGetLanguage INTO nReturn;
  CLOSE cGetLanguage;

  RETURN nReturn;

END getReportLanguage;

--***********************************************************************************************************************************************
FUNCTION getTemplate (cnRepid IN STB$REPORT.REPID%TYPE) RETURN number IS
  sCurrentName   constant varchar2(4000) := $$PLSQL_UNIT||'.getTemplate('||cnRepid||')';
  nReturn NUMBER;
  nStartTimeNum    number      := DBMS_UTILITY.GET_TIME;

  CURSOR cGetTemplate is
    select template
      from stb$report
     where repid = cnRepid;

BEGIN
  isr$trace.stat('begin', 'begin', sCurrentName);
  OPEN cGetTemplate;
  FETCH cGetTemplate INTO nReturn;
  CLOSE cGetTemplate;

  isr$trace.stat('end','nReturn: '||nReturn||' -- elapsed time: '||isr$util.getElapsedTime(nStartTimeNum), sCurrentName);
  RETURN nReturn;

END getTemplate;

--***********************************************************************************************************************************************
function getDocDefType( cnDocId in STB$DOCTRAIL.DOCID%type,
                        cnRepid in STB$REPORT.REPID%type   default null )
  return  varchar2
  result_cache       -- vs 03.Mar.2018 [ISRC-954],[BISOS-43]
IS
  sCurrentName   constant varchar2(4000) := $$PLSQL_UNIT||'.getDocDefType('||cnDocId||','||cnRepId||')';
  nStartTimeNum    number      := DBMS_UTILITY.GET_TIME;

  CURSOR cGetDocDefType(nDocId IN STB$DOCTRAIL.DOCID%TYPE) IS
    SELECT doc_definition
    FROM   stb$doctrail
    WHERE  docid = nDocId;

  CURSOR cGetDefaultDocDefType(nRepId IN STB$REPORT.REPID%TYPE) IS
    SELECT doc_definition
    FROM   isr$report$output$type ro
    WHERE  ro.outputid = STB$DOCKS.getOutput(nRepId);

  sDocDefinition   STB$DOCTRAIL.DOC_DEFINITION%TYPE;
BEGIN
  isr$trace.stat('begin', 'begin', sCurrentName);
  OPEN cGetDocDefType(cnDocId);
  FETCH cGetDocDefType INTO sDocDefinition;
  CLOSE cGetDocDefType;

  IF (cnDocId IS NULL OR sDocDefinition IS NULL) AND cnRepid IS NOT NULL THEN
    OPEN cGetDefaultDocDefType(cnRepid);
    FETCH cGetDefaultDocDefType INTO sDocDefinition;
    CLOSE cGetDefaultDocDefType;
  END IF;

  isr$trace.stat('end','sDocDefinition: '||sDocDefinition||' -- elapsed time: '||isr$util.getElapsedTime(nStartTimeNum), sCurrentName);
  RETURN sDocDefinition;

END getDocDefType;

--***********************************************************************************************************************************************
function getDocType( cnDocId in STB$DOCTRAIL.DOCID%type,
                     cnRepid in STB$REPORT.REPID%type    default null)
  return varchar2
  result_cache       -- vs 03.Mar.2018 [BISOS-43]
IS

  sCurrentName   constant varchar2(4000) := $$PLSQL_UNIT||'.getDocType('||cnDocId||','||cnRepid||')';
  nStartTimeNum    number      := DBMS_UTILITY.GET_TIME;

  CURSOR cGetDocType(nDocId IN STB$DOCTRAIL.DOCID%TYPE) IS
    SELECT doctype
    FROM   stb$doctrail
    WHERE  docid = nDocId;

  CURSOR cGetDefaultDocType(nRepId IN STB$REPORT.REPID%TYPE) IS
    SELECT doc_type
    FROM   isr$report$output$type ro
    WHERE  ro.outputid = STB$DOCKS.getOutput(nRepId);

  sDocType  STB$DOCTRAIL.DOCTYPE%TYPE;
BEGIN
  isr$trace.stat('begin', 'begin', sCurrentName);

  OPEN cGetDocType(cnDocId);
  FETCH cGetDocType INTO sDocType;
  CLOSE cGetDocType;

  IF (cnDocId IS NULL OR sDocType IS NULL) AND cnRepid IS NOT NULL THEN
    OPEN cGetDefaultDocType(cnRepid);
    FETCH cGetDefaultDocType INTO sDocType;
    CLOSE cGetDefaultDocType;
  END IF;

  isr$trace.stat('end','sDocType: '||sDocType||' -- elapsed time: '||isr$util.getElapsedTime(nStartTimeNum), sCurrentName);
  RETURN sDocType;

END getDocType;

--***********************************************************************************************************************
FUNCTION getDocVersionDisplayed(cnDocid IN STB$DOCTRAIL.DOCID%TYPE)
  RETURN VARCHAR2
is
  sCurrentName   constant varchar2(4000) := $$PLSQL_UNIT||'.getDocVersionDisplayed('||cnDocid||')';
  CURSOR cGetVersion IS
    SELECT versiondisplayed
      FROM stb$doctrail
     WHERE docid = cnDocid;

  sReturn STB$DOCTRAIL.VERSIONDISPLAYED%TYPE;
BEGIN
  isr$trace.stat('begin', 'begin', sCurrentName);
  OPEN cGetVersion;
  FETCH cGetVersion INTO sReturn;
  CLOSE cGetVersion;

  isr$trace.stat('end', 'end', sCurrentName);
  RETURN sReturn;
END getDocVersionDisplayed;

END Stb$docks;