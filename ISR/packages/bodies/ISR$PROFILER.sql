CREATE OR REPLACE package body ISR$profiler
is

  exCreateLogAggregation   exception;
  sSql                     varchar2(32000);
  nCurrentLanguage         number        := stb$security.getcurrentlanguage;    
  crlf                     varchar2(2)   := stb$typedef.crlf;

-- ***************************************************************************************************
function createLogAggregation( cnLowEntryid     number,
                               cnHighEntryid    number,
                               cdLowDate        varchar2,
                               cdHighDate       varchar2,
                               csOracleUserList varchar2,
                               csSessionIdList  varchar2,
                               csIpList         varchar2,                               
                               csLogTableName   varchar2 )
  return stb$oerror$record                                
is
  sCurrentName              constant varchar2(100) := $$PLSQL_UNIT||'.createLogAggregation'; 
  oErrorObj                 STB$OERROR$RECORD := STB$OERROR$RECORD();
  sOracleUserList           varchar2(4000) := ISR$UTIL.getQuotedStringList(csOracleUserList);
  sSessionIdList            varchar2(4000) := ISR$UTIL.getQuotedStringList(csSessionIdList);
  sIpList                   varchar2(4000) := ISR$UTIL.getQuotedStringList(csIpList);
  nProfilerAggregateLevelNo number;  -- [ISRC-803]
begin
  isr$trace.stat('begin', 'begin', sCurrentName, 'cnLowEntryid: '||cnLowEntryid||crlf||'cnHighEntryid: '||cnHighEntryid||crlf||'cdLowDate: '||cdLowDate||crlf||'cdHighDate: '||cdHighDate||crlf||'csOracleUserList: '||csOracleUserList||crlf||'csSessionIdList: '||csSessionIdList||crlf||'csIpList: '||csIpList||crlf||'csLogTableName: '||csLogTableName);
  isr$trace.debug('cdLowDate', cdLowDate, sCurrentName);
  isr$trace.debug('cdHighDate',cdHighDate,sCurrentName);
  
  nProfilerAggregateLevelNo := greatest( isr$trace.getLoglevelNo(STB$UTIL.getSystemParameter('PROFILER_AGGREGATE_LOGLEVEL')),
                                         isr$trace.getLoglevelNo('STATISTIC') );  -- STATISTIC is default setting
  
  delete from tmp$profile$aggr;

  sSql :=  'insert into tmp$profile$aggr ( entryid, sessionid, sid_rank, timestamp, action, functiongroup, pred_id, pred_action, pred_timestamp, succ_id, succ_action, 
                                       succ_timestamp, reference, callpackage, logclob, actiontaken, logentry, pred_actiontaken, succ_logentry )
            select distinct a.entryid, a.sessionid, 
                   rank() over(partition by sessionid order by entryid) sid_rank,
                   timestamp, action, functiongroup,
                   lead(entryid,1,0) over(partition by sessionid,functiongroup order by entryid desc) pred_id,
                   lead(action,1,0) over(partition by sessionid,functiongroup order by entryid desc) pred_action,
                   lead(timestamp,1,null) over(partition by sessionid,functiongroup order by entryid desc) pred_timestamp,
                   lag(entryid,1,0) over(partition by sessionid,functiongroup order by entryid desc) succ_id,
                   lag(action,1,0) over(partition by sessionid,functiongroup order by entryid desc) succ_action,
                   lag(timestamp,1,null) over(partition by sessionid,functiongroup order by entryid desc) succ_timestamp,
                   reference, callpackage, logclob,
                   actiontaken, logentry, 
                   lead(actiontaken,1,0) over(partition by sessionid,functiongroup order by entryid desc) pred_actiontaken, 
                   lag(logentry,1,0) over(partition by sessionid,functiongroup order by entryid desc) succ_logentry
            from   ( select entryid, sessionid, timestamp, ip, oracleusername,
                         case
                           when actiontaken = ''begin end'' then actiontaken
                           when lower(trim(actiontaken)) like ''begin%'' then ''begin''
                           when lower(trim(actiontaken)) like ''%begin'' then ''begin''
                           when lower(trim(actiontaken)) like ''end%''  then ''end''
                           when lower(trim(actiontaken)) like ''%end'' then ''end''
                           when (     functiongroup <> ''STB$OERROR$RECORD.handleError''
                                  and (   trim(lower(actiontaken)) like ''error%'' 
                                       or trim(lower(actiontaken)) like ''%error'') )
                             then ''error''               
                           else actiontaken
                         end action, 
                         substr(functiongroup,1,200) functiongroup, 
                         reference, 
                         dbms_lob.substr(callpackage,200) callpackage,
                         dbms_lob.substr(logclob,200)     logclob,
                         actiontaken, 
                         substr(logentry,1,200) logentry,
                         loglevel
                     from '||csLogTableName||' ) a
            where  a.action in (''begin'',''end'',''error'')
            and    a.loglevel <= '||nProfilerAggregateLevelNo||'
            and    a.entryid between '||cnLowEntryid||' and '||cnHighEntryid||'
            and    a.timestamp between to_timestamp('''||cdLowDate||''', isr$util.getInternalDate) and to_timestamp('''||cdHighDate||''', isr$util.getInternalDate)
            and    (    oracleusername in ('||sOracleUserList||') 
                     or '||length(sOracleUserList)||'=2 )                
            and    (    cast(sessionid as varchar2(4000)) in ('||sSessionIdList||') 
                     or '||length(sSessionIdList)||'=2 )                     
            and    (    ip in ('||sIpList||') 
                     or '||length(sIpList)||'=2 )                
            order by entryid';
  isr$trace.debug('sSql', 'length: '||length(sSql),sCurrentName,to_clob(sSql));
  execute immediate sSql;

  isr$trace.stat('end', 'end', sCurrentName);
  return oErrorObj;
exception
  when others then
    oErrorObj.handleError ( Stb$typedef.cnSeverityCritical, 
                            utd$msglib.getmsg ('exUnexpectedError', nCurrentLanguage,  csP1 => sqlerrm(sqlcode)), 
                            sCurrentName||crlf||'sSql:'||crlf||sSql,
                            substr(dbms_utility.format_error_stack ||crlf|| dbms_utility.format_error_backtrace, 1,4000) );                                
    raise exCreateLogAggregation;    
end createLogAggregation;

-- ***************************************************************************************************
function createLogAggregation( cnLowEntryid     number   default 0,
                               cnHighEntryid    number   default cnHiNum,
                               csOracleUserList varchar2 default null,
                               csSessionIdList  varchar2 default null,
                               csIpList         varchar2 default null,
                               csLogTableName   varchar2 default csIsrLogTable )
  return stb$oerror$record                         
is
  sCurrentName    constant varchar2(100) := $$PLSQL_UNIT||'.createLogAggregation (overload#1)'; 
  oErrorObj       STB$OERROR$RECORD := STB$OERROR$RECORD();       
begin
  isr$trace.info('begin', 'begin', sCurrentName, 'cnLowEntryid: '||cnLowEntryid||crlf||'cnHighEntryid: '||cnHighEntryid||crlf||'csOracleUserList: '||csOracleUserList||crlf||'csSessionIdList: '||csSessionIdList||crlf||'csIpList: '||csIpList||crlf||'csLogTableName: '||csLogTableName);
  oErrorObj := createLogAggregation( trunc(cnLowEntryid), round(cnHighEntryid), 
                                     '01.01.1800', '31.12.9999',
                                     upper(csOracleUserList), upper(csSessionIdList), upper(csIpList),
                                     csLogTableName );
  isr$trace.info('end', 'end', sCurrentName);
  return oErrorObj;
exception
  when exCreateLogAggregation then
    raise;
  when others then
    oErrorObj.handleError ( Stb$typedef.cnSeverityCritical, 
                            utd$msglib.getmsg ('exUnexpectedError', nCurrentLanguage,  csP1 => sqlerrm(sqlcode)), 
                            sCurrentName||crlf||'sSql:'||crlf||sSql,
                            substr(dbms_utility.format_error_stack ||crlf|| dbms_utility.format_error_backtrace, 1,4000) );                                
    raise;    
end createLogAggregation;


-- ***************************************************************************************************
function createLogAggregation( cdLowDate        varchar2,
                               cdHighDate       varchar2 default trunc(sysdate)+1,
                               csOracleUserList varchar2 default null,
                               csSessionIdList  varchar2 default null,
                               csIpList         varchar2 default null,                     
                               csLogTableName   varchar2 default csIsrLogTable )
  return stb$oerror$record                                
is
  sCurrentName    constant varchar2(100) := $$PLSQL_UNIT||'.createLogAggregation (overload#2)'; 
  oErrorObj       STB$OERROR$RECORD := STB$OERROR$RECORD();       
begin
  isr$trace.info('begin', 'begin' ,sCurrentName, 'cdLowDate: '||cdLowDate||crlf||'cdHighDate: '||cdHighDate||crlf||'csOracleUserList: '||csOracleUserList||crlf||'csSessionIdList: '||csSessionIdList||crlf||'csIpList: '||csIpList||crlf||'csLogTableName: '||csLogTableName);
  oErrorObj := createLogAggregation( 0, cnHiNum, cdLowDate, cdHighDate, 
                                     upper(csOracleUserList), upper(csSessionIdList), upper(csIpList),
                                     csLogTableName );
  isr$trace.info('end', 'end', sCurrentName);
  return oErrorObj;
exception
  when exCreateLogAggregation then
    raise;
  when others then
    oErrorObj.handleError ( Stb$typedef.cnSeverityCritical, 
                            utd$msglib.getmsg ('exUnexpectedError', nCurrentLanguage,  csP1 => sqlerrm(sqlcode)), 
                            sCurrentName||crlf||'sSql:'||crlf||sSql,
                            substr(dbms_utility.format_error_stack ||crlf|| dbms_utility.format_error_backtrace, 1,4000) );                                
    raise;    
end createLogAggregation;

-- ***************************************************************************************************
function createLogAnalysis( csJobId stb$jobparameter.parametervalue%type default null )     -- [ISRC-732]
  return stb$oerror$record
is
  sCurrentName    constant varchar2(100) := $$PLSQL_UNIT||'.createLogAnalysis('||nvl(csJobId,' ')||')'; 
  oErrorObj       STB$OERROR$RECORD := STB$OERROR$RECORD();       
begin
  isr$trace.stat('begin', 'csJobId: '||nvl(csJobId,'(null)'),sCurrentName);
  isr$trace.debug('code position','before: delete from TMP$PROFILE$CALC', sCurrentName);
  delete from tmp$profile$calc;

  if csJobId is null then
    ---------------------------------------------------
    --      Default is aggregation by sessionid      --
    ---------------------------------------------------
    isr$trace.debug('code position','before insert', sCurrentName);
    insert into tmp$profile$calc ( entryid, succ_id, sessionid, lvl, lvl_node, functiongroup, durationtotalms, durationms,
                                   duration, durationtotal, action, context, timestamp, reference, callpackage, compound_logentry ) 
    with dur as ( select sid_rank, pred_action, action, succ_action, pred_id, entryid, succ_id, pred_timestamp, timestamp, succ_timestamp, sessionid, functiongroup,
                         case 
                           when action = 'end' and pred_action = 'begin' then  
                             'end'  
                           when action = 'end' and pred_action = '0' then 
                             'not_logged'
                           when action = 'end'  then                          
                             'probably_recursion' 
                           ---                         
                           when action = 'begin' and succ_action = 'end' then 
                             'begin'
                           when action = 'begin' and succ_action = 'error' then 
                             'error'  
                           when action = 'begin' and succ_action = '0' then 
                             'not_logged' 
                           when action = 'begin' then
                             'probably_recursion'
                           ---
                           when action = 'error' then
                             'error'
                           else 'unknown'
                         end context,
                         trim(    substr(actiontaken,1,30)||'||'
                               || substr(logentry,1,60)||'||'
                               || substr(succ_logentry,1,60)||'||'
                               || substr(succ_action,1,30)         ) compound_logentry,
                         isr$util.dsInterval$to$ms( succ_timestamp - timestamp ) durationtotalms,
                         reference, callpackage,
                         logentry, actiontaken, pred_actiontaken, succ_logentry
                  from   tmp$profile$aggr
                  where action = 'begin')
    select distinct d1.entryid,
           d1.succ_id,
           --d1.pred_id,
           d1.sessionid, 
           case -- determine level by counting encompassing funciongroups
             when d1.context <> 'begin' then 0
             when d2.entryid is null then 1
             else count(1) over( partition by d1.entryid, d1.sessionid) + 1
           end  lvl,
           case -- determine level node (required for order within this level)
             when d1.context <> 'begin' then 0
             when d2.entryid is null then 1                                      -- level = 1 --> calculate later, level_node is min entryid of all level 1 entries
             else max(d2.entryid) over( partition by d1.entryid, d1.sessionid)   -- level > 1 --> current entry is parent of level_node
           end  lvl_node,
           d1.functiongroup, 
           case when d1.action='begin' and d1.context='begin'
             then d1.durationtotalms
             else 0
           end durationtotalms,
           cast( null as number )       durationms,
           cast( null as varchar2(20) ) duration,
           cast( null as varchar2(20) ) durationtotal,
           d1.action, 
           d1.context, 
           d1.timestamp, 
           d1.reference, d1.callpackage , d1.compound_logentry
    from   dur d1 
    -- join encompassing functiongroups
    left outer join dur d2
    on     d1.entryid  > d2.entryid
    and    d1.succ_id  < d2.succ_id
    and    d1.sessionid = d2.sessionid
    and    d1.context   = 'begin'
    and    d2.context   = 'begin'
    where d1.context not in ('end')
    order by d1.sessionid, d1.entryid; 
    
    isr$trace.debug('code position','before update loop', sCurrentName);
    -- set lvl_node for level 1
    for rec in ( select distinct entryid, lvl, sessionid,
                        min(entryid) over(partition by sessionid, lvl) lvl_node 
                 from   tmp$profile$calc
                 where  lvl = 1
                 and    action  = 'begin'
                 and    context = 'begin' )
    loop
      update tmp$profile$calc
      set    lvl_node = rec.lvl_node
      where  lvl = 1
      and    sessionid = rec.sessionid;
    end loop;
    
    isr$trace.debug('code position','before merge', sCurrentName);
    -- Calculate some duration values  
    merge into  tmp$profile$calc  target
    using       ( select distinct
                         a.entryid, a.sessionid, a.lvl, a.functiongroup,  
                         a.durationtotalms - nvl( sum(b.durationtotalms) over (partition by a.entryid, a.sessionid), 0) durationms,
                         --a.durationtotalms,
                         isr$util.getElapsedTime(0, a.durationtotalms - nvl( sum(b.durationtotalms) over (partition by a.entryid, a.sessionid), 0))   duration,
                         isr$util.getElapsedTime(0,a.durationtotalms)   durationtotal
                  from   tmp$profile$calc a
                  left outer join tmp$profile$calc b
                  on     a.entryid < b.entryid
                  and    a.succ_id > b.succ_id
                  and    a.sessionid = b.sessionid
                  and    a.context   = 'begin'
                  and    b.context   = 'begin' 
                  and    a.lvl       = b.lvl - 1
                  order by a.sessionid, a.entryid  ) src
    on          (target.entryid = src.entryid)
    when matched then 
    update set target.durationms    = src.durationms,
               target.duration      = src.duration,
               target.durationtotal = src.durationtotal;  
  else    -- [ISRC-732]
    ---------------------------------------------------
    --  Job-Related --> no aggregation by sessionid  --
    ---------------------------------------------------  
    isr$trace.debug('code position','before insert', sCurrentName);
    insert into tmp$profile$calc ( entryid, succ_id, sessionid, lvl, lvl_node, functiongroup, durationtotalms, durationms,
                                   duration, durationtotal, action, context, timestamp, reference, callpackage, compound_logentry ) 
    with dur as ( select sid_rank, pred_action, action, succ_action, pred_id, entryid, succ_id, pred_timestamp, timestamp, succ_timestamp, sessionid, functiongroup,
                         case 
                           when action = 'end' and pred_action = 'begin' then  
                             'end'  
                           when action = 'end' and pred_action = '0' then 
                             'not_logged'
                           when action = 'end'  then                          
                             'probably_recursion' 
                           ---                         
                           when action = 'begin' and succ_action = 'end' then 
                             'begin'
                           when action = 'begin' and succ_action = 'error' then 
                             'error'  
                           when action = 'begin' and succ_action = '0' then 
                             'not_logged' 
                           when action = 'begin' then
                             'probably_recursion'
                           ---
                           when action = 'error' then
                             'error'
                           else 'unknown'
                         end context,
                         trim(    substr(actiontaken,1,30)||'||'
                               || substr(logentry,1,60)||'||'
                               || substr(succ_logentry,1,60)||'||'
                               || substr(succ_action,1,30)         ) compound_logentry,
                         isr$util.dsInterval$to$ms( succ_timestamp - timestamp ) durationtotalms,
                         reference, callpackage,
                         logentry, actiontaken, pred_actiontaken, succ_logentry
                  from   tmp$profile$aggr
                  where action = 'begin')
    select distinct d1.entryid,
           d1.succ_id,
           d1.sessionid, 
           case -- determine level by counting encompassing funciongroups
             when d1.context <> 'begin' then 0
             when d2.entryid is null then 1
             else count(1) over( partition by d1.entryid/*, d1.sessionid*/) + 1
           end  lvl,
           case -- determine level node (required for order within this level)
             when d1.context <> 'begin' then 0
             when d2.entryid is null then 1                                          -- level = 1 --> calculate later, level_node is min entryid of all level 1 entries
             else max(d2.entryid) over( partition by d1.entryid/*, d1.sessionid*/)   -- level > 1 --> current entry is parent of level_node
           end  lvl_node,
           d1.functiongroup, 
           case when d1.action='begin' and d1.context='begin'
             then d1.durationtotalms
             else 0
           end durationtotalms,
           cast( null as number )       durationms,
           cast( null as varchar2(20) ) duration,
           cast( null as varchar2(20) ) durationtotal,
           d1.action, 
           d1.context, 
           d1.timestamp, 
           d1.reference, d1.callpackage , d1.compound_logentry
    from   dur d1 
    -- join encompassing functiongroups
    left outer join dur d2
    on     d1.entryid  > d2.entryid
    and    d1.succ_id  < d2.succ_id
    /*and    d1.sessionid = d2.sessionid*/
    and    d1.context   = 'begin'
    and    d2.context   = 'begin'
    where d1.context not in ('end')
    order by /*d1.sessionid,*/ d1.entryid; 
    
    isr$trace.debug('code position','before update loop', sCurrentName);
    -- set lvl_node for level 1
    for rec in ( select distinct entryid, lvl, /*sessionid,*/
                        min(entryid) over(partition by /*sessionid,*/ lvl) lvl_node 
                 from   tmp$profile$calc
                 where  lvl = 1
                 and    action  = 'begin'
                 and    context = 'begin' )
    loop
      update tmp$profile$calc
      set    lvl_node = rec.lvl_node
      where  entryid = rec.entryid;
    end loop;    
    
    isr$trace.debug('code position','before merge', sCurrentName);
    -- Calculate some duration values  
    merge into  tmp$profile$calc  target
    using       ( select distinct
                         a.entryid, a.sessionid, a.lvl, a.functiongroup,  
                         a.durationtotalms - nvl( sum(b.durationtotalms) over (partition by a.entryid /*, a.sessionid*/), 0) durationms,
                         --a.durationtotalms,
                         isr$util.getElapsedTime(0, a.durationtotalms - nvl( sum(b.durationtotalms) over (partition by a.entryid/*, a.sessionid*/), 0))   duration,
                         isr$util.getElapsedTime(0,a.durationtotalms)   durationtotal
                  from   tmp$profile$calc a
                  left outer join tmp$profile$calc b
                  on     a.entryid < b.entryid
                  and    a.succ_id > b.succ_id
                  /*and    a.sessionid = b.sessionid*/
                  and    a.context   = 'begin'
                  and    b.context   = 'begin' 
                  and    a.lvl       = b.lvl - 1
                  order by /*a.sessionid,*/ a.entryid  ) src
    on          (target.entryid = src.entryid)
    when matched then 
    update set target.durationms    = src.durationms,
               target.duration      = src.duration,
               target.durationtotal = src.durationtotal;  
  end if;
  
  isr$trace.stat('end', 'end', sCurrentName);
  return oErrorObj;
exception
  when others then
    oErrorObj.handleError ( Stb$typedef.cnSeverityCritical, 
                            utd$msglib.getmsg ('exUnexpectedError', nCurrentLanguage,  csP1 => sqlerrm(sqlcode)), 
                            sCurrentName,
                            substr(dbms_utility.format_error_stack ||crlf|| dbms_utility.format_error_backtrace, 1,4000) );                                
    raise;    
end createLogAnalysis;

-- ***************************************************************************************************
function createLogHierarchy( csJobId stb$jobparameter.parametervalue%type default null )    -- [ISRC-732]
  return stb$oerror$record
is
  sCurrentName    constant varchar2(100) := $$PLSQL_UNIT||'.createLogHierarchy('||nvl(csJobId,' ')||')';
  oErrorObj       STB$OERROR$RECORD := STB$OERROR$RECORD();       
begin
  isr$trace.stat('begin', 'begin', sCurrentName);
  isr$trace.debug('code position','before: delete from TMP$PROFILE$HIER', sCurrentName);
  delete from tmp$profile$hier;

  if csJobId is null then
    ---------------------------------------------------
    --      Default is aggregation by sessionid      --
    ---------------------------------------------------
    isr$trace.debug('code position','before insert', sCurrentName);
    -- Insert data with hierarchical structure into table TMP$PROFILE$HIER. Data source is TMP$PROFILE$CALC only.
    insert into tmp$profile$hier( entryid, succ_id, sessionid, pred_sessionid, succ_sessionid, lvl, succ_lvl, pred_lvl, lvlorder, functiongroup, action, durationms, durationtotalms)
    select entryid, succ_id, sessionid, pred_sessionid,
                    lead(sessionid) over( order by sessionid, entryid) succ_sessionid, lvl,
                    case
                      when action = 'begin' then 
                        succ_lvl
                      else
                        lead(lvl) over( partition by sessionid order by sessionid, entryid )
                    end succ_lvl, 
                    pred_lvl, -- to be changed as below ?!
                    lvlorder, functiongroup, 
                    action, durationms, durationtotalms
             from ( with calc as ( select entryid, succ_id, sessionid, 
                                          lag(sessionid) over( order by sessionid, entryid) pred_sessionid,   -- First entry within session, if sessionid <> nvl(pred_sessionid,-1)
                                          cast(null as number) succ_sessionid,  
                                          lvl, 
                                          lead(lvl) over( partition by sessionid order by sessionid, entryid) succ_lvl,   
                                          lag(lvl)  over( partition by sessionid order by sessionid, entryid) pred_lvl,
                                          rank() over(partition by sessionid, lvl, lvl_node order by entryid) lvlorder,
                                          functiongroup, action, durationms, durationtotalms
                                   from   tmp$profile$calc 
                                   where  action  = 'begin'
                                   and    context = 'begin'       
                                   order by sessionid, entryid )
                    -- begin entries
                    select cb.sessionid, cb.pred_sessionid, cb.succ_sessionid, cb.entryid, cb.action, cb.succ_id, cb.lvl, cb.succ_lvl, cb.pred_lvl, cb.lvlorder, cb.functiongroup, cb.durationms, cb.durationtotalms
                    from   calc cb  -- all begin entries
                    union 
                    -- represent end entries
                    select ce.sessionid, null, ce.succ_sessionid, ce.succ_id /*=entryid*/, 'end', null, ce.lvl, ce.succ_lvl, ce.pred_lvl, null, null, null, null
                    from   calc ce
                    order by sessionid, entryid )
             order by sessionid, entryid;
  else   -- [ISRC-732]
    ---------------------------------------------------
    --  Job-Related --> no aggregation by sessionid  --
    ---------------------------------------------------  
      isr$trace.debug('code position','before insert', sCurrentName);
      -- Insert data with hierarchical structure into table TMP$PROFILE$HIER. Data source is TMP$PROFILE$CALC only.
      insert into tmp$profile$hier( entryid, succ_id, sessionid, pred_sessionid, succ_sessionid, lvl, succ_lvl, pred_lvl, lvlorder, functiongroup, action, durationms, durationtotalms)
      select entryid, succ_id, sessionid, pred_sessionid,
                      lead(sessionid) over( order by sessionid, entryid) succ_sessionid, 
                      lvl,
                      case
                        when action = 'begin' then 
                          succ_lvl
                        else
                          lead(lvl) over( partition by 1 /*sessionid*/ order by /*sessionid,*/ entryid )
                      end succ_lvl, 
                                            case
                        when (     succ_lvl is null               
                               and entryid = lag(succ_id) over( order by entryid )  ) then
                          -- set pred_lvl to NULL --> means: there is no child-call for the last entry
                          null
                        else
                          pred_lvl
                      end pred_lvl, 
                      lvlorder, functiongroup, 
                      action, durationms, durationtotalms
               from ( with calc as ( select entryid, succ_id, sessionid, 
                                            lag(sessionid) over( order by sessionid, entryid) pred_sessionid,   -- First entry within session, if sessionid <> nvl(pred_sessionid,-1)
                                            cast(null as number) succ_sessionid,  
                                            lvl, 
                                            lead(lvl) over( partition by 1 /*sessionid*/ order by /*sessionid,*/ entryid) succ_lvl,   
                                            lag(lvl)  over( partition by 1 /*sessionid*/ order by /*sessionid,*/ entryid) pred_lvl,
                                            rank() over(partition by /*sessionid,*/ lvl, lvl_node order by entryid) lvlorder,
                                            functiongroup, action, durationms, durationtotalms
                                     from   tmp$profile$calc
                                     where  action  = 'begin'
                                     and    context = 'begin'       
                                     order by /*sessionid,*/ entryid )
                      -- begin entries
                      select cb.sessionid, cb.pred_sessionid, cb.succ_sessionid, cb.entryid, cb.action, cb.succ_id, cb.lvl, cb.succ_lvl, cb.pred_lvl, cb.lvlorder, cb.functiongroup, cb.durationms, cb.durationtotalms
                      from   calc cb  -- all begin entries
                      union 
                      -- represent end entries
                      select ce.sessionid, null, ce.succ_sessionid, ce.succ_id, 'end', null, ce.lvl, ce.succ_lvl, ce.pred_lvl, null, null, null, null
                      from   calc ce
                      order by /*sessionid,*/ entryid )
               order by /*sessionid,*/ entryid;
  end if;

  isr$trace.stat('end', 'end', sCurrentName);
  return oErrorObj;
exception
  when others then
    oErrorObj.handleError ( Stb$typedef.cnSeverityCritical, 
                            utd$msglib.getmsg ('exUnexpectedError', nCurrentLanguage,  csP1 => sqlerrm(sqlcode)), 
                            sCurrentName,
                            substr(dbms_utility.format_error_stack ||crlf|| dbms_utility.format_error_backtrace, 1,4000) );                                
    raise;    
end createLogHierarchy;

-- ***************************************************************************************************
function getHtmlProfile( csSidList            varchar2 default null,
                         cnLowEntryid         number   default 0,
                         cnHighEntryid        number   default cnHiNum,
                         csIncludeSuccEntryid char     default 'N' )
  return xml$list pipelined
is
  sCurrentName  constant varchar2(100) := $$PLSQL_UNIT||'.getHtmlProfile'; 
  oErrorObj       STB$OERROR$RECORD    := STB$OERROR$RECORD();        
  sSidList      varchar2(4000) := ISR$UTIL.getQuotedStringList(csSidList);
  xReturnXml    xmltype;
begin
  isr$trace.stat('begin', 'begin', sCurrentName);

  sSql := 'select xmlelement("table", xmlattributes(''table1'' "id",  ''controller'' "class" ),
                  xmlelement("tr",    xmlattributes(''header'' "data-level",  ''header'' "class" ),
                                      xmlelement("td", ''FunctionGroup''),
                                      xmlelement("td", ''lvl''),
                                      xmlelement("td", ''timestamp''),
                                      xmlelement("td", ''EntryID''),'||
                                      case
                                        when csIncludeSuccEntryid = 'Y' then
                                          'xmlelement("td", ''EntryID_end''),'
                                        else ' '
                                      end||'
                                      xmlelement("td", ''SessionID''),
                                      xmlelement("td", ''DurationMs''),
                                      xmlelement("td", ''DurationTotalMs''),
                                      xmlelement("td", ''Compound_Logentry''),
                  xmlagg( xmlelement("tr", xmlattributes( lvl "data-level", entryid "id"),
                          xmlelement("td",functiongroup),
                          xmlelement("td", xmlattributes( ''data'' "class"), lvl ),
                          xmlelement("td", xmlattributes( ''data'' "class"), timestamp ),
                          xmlelement("td", xmlattributes( ''data'' "class"), entryid ),'||
                          case
                            when csIncludeSuccEntryid = 'Y' then
                              'xmlelement("td", xmlattributes( ''data'' "class"), succ_id ),'
                            else ' '
                          end||'
                          xmlelement("td", xmlattributes( ''data'' "class"), sessionid ),
                          xmlelement("td", xmlattributes( ''data'' "class"), durationms ),
                          xmlelement("td", xmlattributes( ''data'' "class"), durationtotalms ),
                          xmlelement("td", xmlattributes( ''data'' "class"), compound_logentry )
                          )  order by sessionid, entryid  ) ) ) col               
           from   tmp$profile$calc
           where  lvl > 0
           and    (   sessionid in ('||sSidList||')
                    or '||length(sSidList)||'=2 )  
           and    entryid between '||cnLowEntryid||' and '||cnHighEntryid||'
           order by sessionid, entryid';
  
  isr$trace.debug('sSql', 'length: '||length(sSql)||', details see column LOGCLOB.',sCurrentName,to_clob(sSql));
  execute immediate sSql
  into   xReturnXml;
  
  pipe row( xReturnXml );
  isr$trace.stat('end', 'end',sCurrentName);  
  return;
  
exception
  when others then
    oErrorObj.handleError ( Stb$typedef.cnSeverityCritical, 
                            utd$msglib.getmsg ('exUnexpectedError', nCurrentLanguage,  csP1 => sqlerrm(sqlcode)), 
                            sCurrentName,
                            substr(dbms_utility.format_error_stack ||crlf|| dbms_utility.format_error_backtrace, 1,4000) );                                
    raise;    
end getHtmlProfile;

-- ***************************************************************************************************
function getXmlHierProfile( csSidList varchar2 default null,
                            csJobId   stb$jobparameter.parametervalue%type default null)  -- [ISRC-732]
  return xml$list pipelined
is
  sCurrentName  constant varchar2(100) := $$PLSQL_UNIT||'.getXmlHierProfile'; 
  oErrorObj     STB$OERROR$RECORD      := STB$OERROR$RECORD();
  sSidList      varchar2(4000)         := ISR$UTIL.getQuotedStringList(csSidList);
  clXmlClob     clob;
  xReturnXml    xmltype;
begin
  isr$trace.stat('begin', 'begin', sCurrentName);

  -- Begin with XML-Header
  clXmlClob :=     '<?xml version="1.0"?><profiler>'
                || '<profile name="isr-log-profile" title="iSR Profile for Schema '||sys_context('USERENV','CURRENT_SCHEMA')||'" date ="' 
                ||  upper(to_char(sysdate,'dd.mon.yyyy')) ||'" reporter="'||user||'">';
 
  -- Bulk of XML 
  if csJobId is null then
    ---------------------------------------------------
    --      Default is aggregation by sessionid      --
    ---------------------------------------------------
    isr$trace.debug('code position', 'before loop recSnip', sCurrentName);
    -- Add data bulk
    for recSnip in (with ds as (  select  h.entryid, h.sessionid,
                                          case
                                          -- BEGIN: first entry of a session, child levels following
                                          when (     h.lvl = 1 
                                                 and h.action = 'begin'
                                                 and h.pred_lvl is null 
                                                 and nvl(h.succ_lvl,h.lvl) > h.lvl
                                                 and ( h.pred_sessionid <> h.sessionid or h.pred_sessionid is null ) ) then
                                            '<session sessionid="'||h.sessionid||'">'
                                            || '<call entryid="'||h.entryid||'" orderno="'||h.lvlorder||'" DurationTotalMs="'||h.durationtotalms||'">'
                                            || '<StartTime>'||to_char(ca.timestamp,stb$typedef.csInternalDateFormat)||'</StartTime><Level>'||h.lvl||'</Level><FunctionGroup>'||h.FunctionGroup||'</FunctionGroup><DurationMs>'||h.durationms||'</DurationMs>'
                                            || '<child-calls>'
                                          -- BEGIN: first session entry, no child levels following
                                          when (     h.lvl = 1 
                                                 and h.action = 'begin'
                                                 and h.pred_lvl is null 
                                                 and (h.pred_sessionid <> h.sessionid or h.pred_sessionid is null ) ) then
                                            '<session sessionid="'||h.sessionid||'">'
                                            || '<call entryid="'||h.entryid||'" orderno="'||h.lvlorder||'" DurationTotalMs="'||h.durationtotalms||'">'
                                            || '<StartTime>'||to_char(ca.timestamp,stb$typedef.csInternalDateFormat)||'</StartTime><Level>'||h.lvl||'</Level><FunctionGroup>'||h.FunctionGroup||'</FunctionGroup><DurationMs>'||h.durationms||'</DurationMs>'
                                          -- BEGIN, child levels following
                                          when (     h.action = 'begin'
                                                 and nvl(h.succ_lvl,h.lvl) > h.lvl  ) then
                                            lpad(' ', h.lvl*3,' ')
                                            ||'<call entryid="'||h.entryid||'" orderno="'||h.lvlorder||'" DurationTotalMs="'||h.durationtotalms||'">'
                                            || '<StartTime>'||to_char(ca.timestamp,stb$typedef.csInternalDateFormat)||'</StartTime><Level>'||h.lvl||'</Level><FunctionGroup>'||h.FunctionGroup||'</FunctionGroup><DurationMs>'||h.durationms||'</DurationMs>'
                                            || '<child-calls>'
                                          -- BEGIN, no child levels following
                                          when ( h.action = 'begin' ) then
                                            lpad(' ', h.lvl*3,' ')
                                            ||'<call entryid="'||h.entryid||'" orderno="'||h.lvlorder||'" DurationTotalMs="'||h.durationtotalms||'">'
                                            || '<StartTime>'||to_char(ca.timestamp,stb$typedef.csInternalDateFormat)||'</StartTime><Level>'||h.lvl||'</Level><FunctionGroup>'||h.FunctionGroup||'</FunctionGroup><DurationMs>'||h.durationms||'</DurationMs>'
                                          -- END: last session entry, last child
                                          when (     h.action = 'end' 
                                                 and nvl(h.succ_lvl,h.lvl) <> h.lvl
                                                 and nvl(h.succ_sessionid,h.sessionid-1) <> h.sessionid ) then
                                            lpad(' ', h.lvl*3,' ')
                                            ||'</call></child-calls></session>'
                                          -- END: last session entry, no child
                                          when (     h.action = 'end' 
                                                 and nvl(h.succ_sessionid,h.sessionid-1) <> h.sessionid ) then
                                            lpad(' ', h.lvl*3,' ')
                                            ||'</call></session>'
                                          -- END: last child
                                          when (     h.action = 'end' 
                                                 and nvl(h.succ_lvl,h.lvl) < h.lvl ) then
                                            lpad(' ', h.lvl*3,' ')
                                            ||'</call></child-calls>'
                                          -- END
                                          when ( h.action = 'end' ) then
                                            lpad(' ', h.lvl*3,' ')
                                            ||'</call>'
                                          else
                                            'ERROR'
                                        end xmlsnippet        
                                 from tmp$profile$hier h
                                 left outer join tmp$profile$calc ca
                                 on     h.entryid = ca.entryid
                                 and    ca.context = 'begin')
                    select xmlsnippet
                    from   ds
                    order by sessionid, entryid )
    loop
      clXmlClob := clXmlClob ||chr(10)|| recSnip.xmlsnippet;
    end loop;
  else    -- [ISRC-732]
    ---------------------------------------------------
    --  Job-Related --> no aggregation by sessionid  --
    ---------------------------------------------------     
    isr$trace.debug('code position', 'before loop recSnip', sCurrentName);
    -- Add data bulk
    for recSnip in (with ds as (  select  h.entryid, h.sessionid,
                                          case
                                          -- BEGIN: first entry child levels following
                                          when (     h.lvl = 1 
                                                 and h.action = 'begin'
                                                 and h.pred_lvl is null 
                                                 and nvl(h.succ_lvl,h.lvl) > h.lvl
                                                 /*and ( h.pred_sessionid <> h.sessionid or h.pred_sessionid is null )*/ ) then
                                            '<job jobid="'||nvl(csJobId,'JOBID')||'">'
                                            || '<call sessionid="'||h.sessionid||'" entryid="'||h.entryid||'" orderno="'||h.lvlorder||'" DurationTotalMs="'||h.durationtotalms||'">'
                                            || '<StartTime>'||to_char(ca.timestamp,stb$typedef.csInternalDateFormat)||'</StartTime><Level>'||h.lvl||'</Level><FunctionGroup>'||h.FunctionGroup||'</FunctionGroup><DurationMs>'||h.durationms||'</DurationMs>'
                                            || '<child-calls>'
                                          -- BEGIN: first no child levels following
                                          when (     h.lvl = 1 
                                                 and h.action = 'begin'
                                                 and h.pred_lvl is null 
                                                 /*and (h.pred_sessionid <> h.sessionid or h.pred_sessionid is null )*/ ) then
                                            '<job jobid="'||nvl(csJobId,'JOBID')||'">'
                                            || '<call sessionid="'||h.sessionid||'" entryid="'||h.entryid||'" orderno="'||h.lvlorder||'" DurationTotalMs="'||h.durationtotalms||'">'
                                            || '<StartTime>'||to_char(ca.timestamp,stb$typedef.csInternalDateFormat)||'</StartTime><Level>'||h.lvl||'</Level><FunctionGroup>'||h.FunctionGroup||'</FunctionGroup><DurationMs>'||h.durationms||'</DurationMs>'
                                          -- BEGIN, child levels following
                                          when (     h.action = 'begin'
                                                 and nvl(h.succ_lvl,h.lvl) > h.lvl  ) then
                                            lpad(' ', h.lvl*3,' ')
                                            ||'<call sessionid="'||h.sessionid||'" entryid="'||h.entryid||'" orderno="'||h.lvlorder||'" DurationTotalMs="'||h.durationtotalms||'">'
                                            || '<StartTime>'||to_char(ca.timestamp,stb$typedef.csInternalDateFormat)||'</StartTime><Level>'||h.lvl||'</Level><FunctionGroup>'||h.FunctionGroup||'</FunctionGroup><DurationMs>'||h.durationms||'</DurationMs>'
                                            || '<child-calls>'
                                          -- BEGIN, no child levels following
                                          when ( h.action = 'begin' ) then
                                            lpad(' ', h.lvl*3,' ')
                                            ||'<call sessionid="'||h.sessionid||'" entryid="'||h.entryid||'" orderno="'||h.lvlorder||'" DurationTotalMs="'||h.durationtotalms||'">'
                                            || '<StartTime>'||to_char(ca.timestamp,stb$typedef.csInternalDateFormat)||'</StartTime><Level>'||h.lvl||'</Level><FunctionGroup>'||h.FunctionGroup||'</FunctionGroup><DurationMs>'||h.durationms||'</DurationMs>'
                                          -- END: last job entry, no child-calls
                                          when (     h.action = 'end' 
                                                 and h.lvl = 1
                                                 and h.pred_lvl is null
                                                 and h.succ_lvl is null ) then
                                            lpad(' ', h.lvl*3,' ')
                                            ||'</call></job>'
                                          -- END: last job entry, last child                                           
                                          when (     h.action = 'end' 
                                                 and h.lvl = 1
                                                 and h.pred_lvl is not null
                                                 and h.succ_lvl is null ) then
                                            lpad(' ', h.lvl*3,' ')
                                            ||'</call></job>'
                                          -- END: last child
                                          when (     h.action = 'end' 
                                                 and nvl(h.succ_lvl,h.lvl) < h.lvl ) then
                                            lpad(' ', h.lvl*3,' ')
                                            ||'</call></child-calls>'
                                          -- END
                                          when ( h.action = 'end' ) then
                                            lpad(' ', h.lvl*3,' ')
                                            ||'</call>'
                                          else
                                            'ERROR'
                                        end xmlsnippet        
                                  from tmp$profile$hier h
                                  left outer join tmp$profile$calc ca
                                  on     h.entryid = ca.entryid
                                  and    ca.context = 'begin'  )
                    select xmlsnippet
                    from   ds
                    order by entryid )
    loop
      clXmlClob := clXmlClob || chr(13)||chr(10) || recSnip.xmlsnippet;
    end loop;
  end if;

  isr$trace.debug('code position', 'before add trailer', sCurrentName);
  -- Append XML-Trailer
  clXmlClob := clXmlClob || '</profile></profiler>';
  isr$trace.debug('variable', 'clXmlClob', sCurrentName, clXmlClob);  

  -- Convert clob to xmltype
  xReturnXml := ISR$XML.clobToXml(clXmlClob);

  pipe row( xReturnXml );
  isr$trace.stat('end', 'end', sCurrentName);  
  return;
  
exception
  when others then
    ISR$TRACE.error('ERROR',clXmlClob,sCurrentName,clXmlClob);
    oErrorObj.handleError ( Stb$typedef.cnSeverityCritical, 
                            utd$msglib.getmsg ('exUnexpectedError', nCurrentLanguage,  csP1 => sqlerrm(sqlcode)), 
                            sCurrentName,
                            substr(dbms_utility.format_error_stack ||crlf|| dbms_utility.format_error_backtrace, 1,4000) );                                
    raise;        
end getXmlHierProfile;

-- ***************************************************************************************************
function getClobHierProfile( csSidList varchar2 default null,
                            csJobId   stb$jobparameter.parametervalue%type default null)  -- [ISRC-732]
  return clob$list pipelined
is
  sCurrentName  constant varchar2(100) := $$PLSQL_UNIT||'.getXmlHierProfile'; 
  oErrorObj     STB$OERROR$RECORD      := STB$OERROR$RECORD();
  sSidList      varchar2(4000)         := ISR$UTIL.getQuotedStringList(csSidList);
  clXmlClob     clob;
begin
  isr$trace.stat('begin', 'begin', sCurrentName);

  -- Begin with XML-Header
  clXmlClob :=     '<?xml version="1.0"?><profiler>'
                || '<profile name="isr-log-profile" title="iSR Profile for Schema '||sys_context('USERENV','CURRENT_SCHEMA')||'" date ="' 
                ||  upper(to_char(sysdate,'dd.mon.yyyy')) ||'" reporter="'||user||'">';
 
  -- Bulk of XML 
  if csJobId is null then
    ---------------------------------------------------
    --      Default is aggregation by sessionid      --
    ---------------------------------------------------
    isr$trace.debug('code position', 'before loop recSnip', sCurrentName);
    -- Add data bulk
    for recSnip in (with ds as (  select  h.entryid, h.sessionid,
                                          case
                                          -- BEGIN: first entry of a session, child levels following
                                          when (     h.lvl = 1 
                                                 and h.action = 'begin'
                                                 and h.pred_lvl is null 
                                                 and nvl(h.succ_lvl,h.lvl) > h.lvl
                                                 and ( h.pred_sessionid <> h.sessionid or h.pred_sessionid is null ) ) then
                                            '<session sessionid="'||h.sessionid||'">'
                                            || '<call entryid="'||h.entryid||'" orderno="'||h.lvlorder||'" DurationTotalMs="'||h.durationtotalms||'">'
                                            || '<StartTime>'||to_char(ca.timestamp,stb$typedef.csInternalDateFormat)||'</StartTime><Level>'||h.lvl||'</Level><FunctionGroup>'||h.FunctionGroup||'</FunctionGroup><DurationMs>'||h.durationms||'</DurationMs>'
                                            || '<child-calls>'
                                          -- BEGIN: first session entry, no child levels following
                                          when (     h.lvl = 1 
                                                 and h.action = 'begin'
                                                 and h.pred_lvl is null 
                                                 and (h.pred_sessionid <> h.sessionid or h.pred_sessionid is null ) ) then
                                            '<session sessionid="'||h.sessionid||'">'
                                            || '<call entryid="'||h.entryid||'" orderno="'||h.lvlorder||'" DurationTotalMs="'||h.durationtotalms||'">'
                                            || '<StartTime>'||to_char(ca.timestamp,stb$typedef.csInternalDateFormat)||'</StartTime><Level>'||h.lvl||'</Level><FunctionGroup>'||h.FunctionGroup||'</FunctionGroup><DurationMs>'||h.durationms||'</DurationMs>'
                                          -- BEGIN, child levels following
                                          when (     h.action = 'begin'
                                                 and nvl(h.succ_lvl,h.lvl) > h.lvl  ) then
                                            lpad(' ', h.lvl*3,' ')
                                            ||'<call entryid="'||h.entryid||'" orderno="'||h.lvlorder||'" DurationTotalMs="'||h.durationtotalms||'">'
                                            || '<StartTime>'||to_char(ca.timestamp,stb$typedef.csInternalDateFormat)||'</StartTime><Level>'||h.lvl||'</Level><FunctionGroup>'||h.FunctionGroup||'</FunctionGroup><DurationMs>'||h.durationms||'</DurationMs>'
                                            || '<child-calls>'
                                          -- BEGIN, no child levels following
                                          when ( h.action = 'begin' ) then
                                            lpad(' ', h.lvl*3,' ')
                                            ||'<call entryid="'||h.entryid||'" orderno="'||h.lvlorder||'" DurationTotalMs="'||h.durationtotalms||'">'
                                            || '<StartTime>'||to_char(ca.timestamp,stb$typedef.csInternalDateFormat)||'</StartTime><Level>'||h.lvl||'</Level><FunctionGroup>'||h.FunctionGroup||'</FunctionGroup><DurationMs>'||h.durationms||'</DurationMs>'
                                          -- END: last session entry, last child
                                          when (     h.action = 'end' 
                                                 and nvl(h.succ_lvl,h.lvl) <> h.lvl
                                                 and nvl(h.succ_sessionid,h.sessionid-1) <> h.sessionid ) then
                                            lpad(' ', h.lvl*3,' ')
                                            ||'</call></child-calls></session>'
                                          -- END: last session entry, no child
                                          when (     h.action = 'end' 
                                                 and nvl(h.succ_sessionid,h.sessionid-1) <> h.sessionid ) then
                                            lpad(' ', h.lvl*3,' ')
                                            ||'</call></session>'
                                          -- END: last child
                                          when (     h.action = 'end' 
                                                 and nvl(h.succ_lvl,h.lvl) < h.lvl ) then
                                            lpad(' ', h.lvl*3,' ')
                                            ||'</call></child-calls>'
                                          -- END
                                          when ( h.action = 'end' ) then
                                            lpad(' ', h.lvl*3,' ')
                                            ||'</call>'
                                          else
                                            'ERROR'
                                        end xmlsnippet        
                                 from tmp$profile$hier h
                                 left outer join tmp$profile$calc ca
                                 on     h.entryid = ca.entryid
                                 and    ca.context = 'begin')
                    select xmlsnippet
                    from   ds
                    order by sessionid, entryid )
    loop
      clXmlClob := clXmlClob ||chr(10)|| recSnip.xmlsnippet;
    end loop;
  else    -- [ISRC-732]
    ---------------------------------------------------
    --  Job-Related --> no aggregation by sessionid  --
    ---------------------------------------------------     
    isr$trace.debug('code position', 'before loop recSnip', sCurrentName);
    -- Add data bulk
    for recSnip in (with ds as (  select  h.entryid, h.sessionid,
                                          case
                                          -- BEGIN: first entry child levels following
                                          when (     h.lvl = 1 
                                                 and h.action = 'begin'
                                                 and h.pred_lvl is null 
                                                 and nvl(h.succ_lvl,h.lvl) > h.lvl
                                                 /*and ( h.pred_sessionid <> h.sessionid or h.pred_sessionid is null )*/ ) then
                                            '<job jobid="'||nvl(csJobId,'JOBID')||'">'
                                            || '<call sessionid="'||h.sessionid||'" entryid="'||h.entryid||'" orderno="'||h.lvlorder||'" DurationTotalMs="'||h.durationtotalms||'">'
                                            || '<StartTime>'||to_char(ca.timestamp,stb$typedef.csInternalDateFormat)||'</StartTime><Level>'||h.lvl||'</Level><FunctionGroup>'||h.FunctionGroup||'</FunctionGroup><DurationMs>'||h.durationms||'</DurationMs>'
                                            || '<child-calls>'
                                          -- BEGIN: first no child levels following
                                          when (     h.lvl = 1 
                                                 and h.action = 'begin'
                                                 and h.pred_lvl is null 
                                                 /*and (h.pred_sessionid <> h.sessionid or h.pred_sessionid is null )*/ ) then
                                            '<job jobid="'||nvl(csJobId,'JOBID')||'">'
                                            || '<call sessionid="'||h.sessionid||'" entryid="'||h.entryid||'" orderno="'||h.lvlorder||'" DurationTotalMs="'||h.durationtotalms||'">'
                                            || '<StartTime>'||to_char(ca.timestamp,stb$typedef.csInternalDateFormat)||'</StartTime><Level>'||h.lvl||'</Level><FunctionGroup>'||h.FunctionGroup||'</FunctionGroup><DurationMs>'||h.durationms||'</DurationMs>'
                                          -- BEGIN, child levels following
                                          when (     h.action = 'begin'
                                                 and nvl(h.succ_lvl,h.lvl) > h.lvl  ) then
                                            lpad(' ', h.lvl*3,' ')
                                            ||'<call sessionid="'||h.sessionid||'" entryid="'||h.entryid||'" orderno="'||h.lvlorder||'" DurationTotalMs="'||h.durationtotalms||'">'
                                            || '<StartTime>'||to_char(ca.timestamp,stb$typedef.csInternalDateFormat)||'</StartTime><Level>'||h.lvl||'</Level><FunctionGroup>'||h.FunctionGroup||'</FunctionGroup><DurationMs>'||h.durationms||'</DurationMs>'
                                            || '<child-calls>'
                                          -- BEGIN, no child levels following
                                          when ( h.action = 'begin' ) then
                                            lpad(' ', h.lvl*3,' ')
                                            ||'<call sessionid="'||h.sessionid||'" entryid="'||h.entryid||'" orderno="'||h.lvlorder||'" DurationTotalMs="'||h.durationtotalms||'">'
                                            || '<StartTime>'||to_char(ca.timestamp,stb$typedef.csInternalDateFormat)||'</StartTime><Level>'||h.lvl||'</Level><FunctionGroup>'||h.FunctionGroup||'</FunctionGroup><DurationMs>'||h.durationms||'</DurationMs>'
                                          -- END: last job entry, no child-calls
                                          when (     h.action = 'end' 
                                                 and h.lvl = 1
                                                 and h.pred_lvl is null
                                                 and h.succ_lvl is null ) then
                                            lpad(' ', h.lvl*3,' ')
                                            ||'</call></job>'
                                          -- END: last job entry, last child                                           
                                          when (     h.action = 'end' 
                                                 and h.lvl = 1
                                                 and h.pred_lvl is not null
                                                 and h.succ_lvl is null ) then
                                            lpad(' ', h.lvl*3,' ')
                                            ||'</call></child-calls></job>'
                                          -- END: last child
                                          when (     h.action = 'end' 
                                                 and nvl(h.succ_lvl,h.lvl) < h.lvl ) then
                                            lpad(' ', h.lvl*3,' ')
                                            ||'</call></child-calls>'
                                          -- END
                                          when ( h.action = 'end' ) then
                                            lpad(' ', h.lvl*3,' ')
                                            ||'</call>'
                                          else
                                            'ERROR'
                                        end xmlsnippet        
                                  from tmp$profile$hier h
                                  left outer join tmp$profile$calc ca
                                  on     h.entryid = ca.entryid
                                  and    ca.context = 'begin'  )
                    select xmlsnippet
                    from   ds
                    order by entryid )
    loop
      clXmlClob := clXmlClob || chr(13)||chr(10) || recSnip.xmlsnippet;
    end loop;
  end if;

  isr$trace.debug('code position', 'before add trailer', sCurrentName);
  -- Append XML-Trailer
  clXmlClob := clXmlClob || '</profile></profiler>';
  isr$trace.debug('variable', 'clXmlClob', sCurrentName, clXmlClob);  

  pipe row( clXmlClob );
  isr$trace.stat('end', 'end', sCurrentName);  
  return;
  
exception
  when others then
    ISR$TRACE.error('ERROR',clXmlClob,sCurrentName,clXmlClob);
    oErrorObj.handleError ( Stb$typedef.cnSeverityCritical, 
                            utd$msglib.getmsg ('exUnexpectedError', nCurrentLanguage,  csP1 => sqlerrm(sqlcode)), 
                            sCurrentName,
                            substr(dbms_utility.format_error_stack ||crlf|| dbms_utility.format_error_backtrace, 1,4000) );                                
    raise;        
end getClobHierProfile;

-- ***************************************************************************************************
function getHtmlMetaProfile
  return xml$list pipelined
is
  sCurrentName  constant varchar2(100) := $$PLSQL_UNIT||'.getMetaAnalysis'; 
  oErrorObj     STB$OERROR$RECORD      := STB$OERROR$RECORD();
  xReturnXml    xmltype;
begin
  isr$trace.stat('begin', 'begin',sCurrentName);

  sSql :=  'select  xmlelement("table", xmlelement("tr", xmlelement("td", ''FunctionGroup''),
                                                        xmlelement("td", ''Count Begin''),
                                                        xmlelement("td", ''Count End''),
                                                        xmlelement("td", ''Diff Begin-End''),
                                                        xmlelement("td", ''Count Errors'')     ),
                                       xmlagg( xmlelement("tr", xmlelement("td", substr(functiongroup,1,100)),
                                                                xmlelement("td", cou_begin ),
                                                                xmlelement("td", cou_end ),
                                                                xmlelement("td", diff_begin_end ),
                                                                xmlelement("td", cou_error )     ) )      )  col
            from   ( select distinct functiongroup,
                                     sum(cou_begin) over( partition by functiongroup) cou_begin,
                                     sum(cou_end)   over( partition by functiongroup) cou_end,
                                     abs(sum(cou_begin) over( partition by functiongroup) -  sum(cou_end) over( partition by functiongroup)) diff_begin_end,
                                     sum(cou_error) over( partition by functiongroup) cou_error
                            from   ( select distinct functiongroup,
                                             case when action = ''begin'' then count(1) over(partition by functiongroup, action )
                                                  else 0
                                             end  cou_begin,
                                             case when action = ''end'' then count(1) over(partition by functiongroup, action )
                                                  else 0
                                             end  cou_end,
                                             case when action = ''error'' then count(1) over(partition by functiongroup, action )
                                                  else 0
                                             end  cou_error                                 
                                     from   tmp$profile$aggr ) )
            order by cou_error desc, diff_begin_end desc, cou_begin desc';
  
  isr$trace.debug('sSql', 'length: '||length(sSql)||', details see column LOGCLOB.',sCurrentName,sSql);
  execute immediate sSql
  into   xReturnXml;
  
  pipe row( xReturnXml );
  isr$trace.stat('end', 'end',sCurrentName);  
  return;

exception
  when others then
    oErrorObj.handleError ( Stb$typedef.cnSeverityCritical, 
                            utd$msglib.getmsg ('exUnexpectedError', nCurrentLanguage,  csP1 => sqlerrm(sqlcode)), 
                            sCurrentName,
                            substr(dbms_utility.format_error_stack ||crlf|| dbms_utility.format_error_backtrace, 1,4000) );                                
    raise;        
end getHtmlMetaProfile;

-- ***************************************************************************************************
function getHtmlPeakProfile 
  return xml$list pipelined
is
  sCurrentName  constant varchar2(100) := $$PLSQL_UNIT||'.getPeakAnalysis'; 
  oErrorObj     STB$OERROR$RECORD      := STB$OERROR$RECORD();
  xReturnXml    xmltype;
begin
  isr$trace.stat('begin', 'begin', sCurrentName);

  sSql := ' select xmlelement("table", xmlelement("tr", xmlelement("td", ''SessionID_____''),
                                                        xmlelement("td", ''SessionDurationMs''),
                                                        xmlelement("td", ''MaxDurationFuncCumulated''),
                                                        xmlelement("td", ''FunctiongroupTotal''),
                                                        xmlelement("td", ''MaxDurationFuncIsolated''),
                                                        xmlelement("td", ''Functiongroup'')             ),
                                       xmlagg( xmlelement("tr", xmlelement("td", sessionid),
                                                                xmlelement("td", sessdurationms ),
                                                                xmlelement("td", maxdurtotalms ),
                                                                xmlelement("td", functiongroup_total ),
                                                                xmlelement("td", maxdurationms ),
                                                                xmlelement("td", functiongroup )     ) )      )  col
            from  ( select  distinct
                           a.sessionid, a.sessdurationms,
                           b.maxdurtotalms, b.functiongroup functiongroup_total,
                           c.maxdurationms, c.functiongroup
                    from   (  select distinct sessionid,
                                     sum(durationtotalms) over( partition by sessionid,lvl) sessdurationms
                              from tmp$profile$calc
                              where lvl = 1 ) a
                    left outer join ( select distinct sessionid, functiongroup,
                                             max(durationtotalms) over ( partition by sessionid) maxdurtotalms,
                                             durationtotalms
                                      from tmp$profile$calc ) b
                    on     a.sessionid = b.sessionid
                    and    b.maxdurtotalms = durationtotalms
                    left outer join ( select distinct sessionid, functiongroup,
                                             max(durationms) over ( partition by sessionid) maxdurationms,
                                             durationms
                                      from tmp$profile$calc ) c
                    on     a.sessionid = c.sessionid
                    and    c.maxdurationms = c.durationms
                    where  a.sessdurationms > 0
                    order by sessdurationms desc, maxdurtotalms desc, maxdurationms desc )';
  
  isr$trace.debug('sSql', 'length: '||length(sSql)||', details see column LOGCLOB.',sCurrentName,to_clob(sSql));
  execute immediate sSql
  into   xReturnXml;
  
  pipe row( xReturnXml );
  isr$trace.stat('end', 'end',sCurrentName);  
  return;

exception
  when others then
    oErrorObj.handleError ( Stb$typedef.cnSeverityCritical, 
                            utd$msglib.getmsg ('exUnexpectedError', nCurrentLanguage,  csP1 => sqlerrm(sqlcode)), 
                            sCurrentName,
                            substr(dbms_utility.format_error_stack ||crlf|| dbms_utility.format_error_backtrace, 1,4000) );                                
    raise;      
end getHtmlPeakProfile;

-- ***************************************************************************************************
function getHtmlStatProfile   
  return xml$list pipelined
is
  sCurrentName  constant varchar2(100) := $$PLSQL_UNIT||'.getStatistics'; 
  oErrorObj     STB$OERROR$RECORD      := STB$OERROR$RECORD();
  xReturnXml    xmltype;
begin
  isr$trace.stat('begin', 'begin', sCurrentName);

  sSql := ' select  xmlelement("table", xmlelement("tr", xmlelement("td", ''FunctionGroup''),
                                                         xmlelement("td", ''Count all''),
                                                         xmlelement("td", ''Minimum(ms)''),
                                                         xmlelement("td", ''Maximum(ms)''),
                                                         xmlelement("td", ''Average''),
                                                         xmlelement("td", ''Median''),
                                                         xmlelement("td", ''Standard Deviation''),
                                                         xmlelement("td", ''Aggregated List''),
                                                         xmlelement("td", ''Entryids of Maximum'')     ),
                                        xmlagg( xmlelement("tr", xmlelement("td", substr(functiongroup,1,100)),
                                                                 xmlelement("td", cou_all ),
                                                                 xmlelement("td", min_ms ),
                                                                 xmlelement("td", max_ms ),
                                                                 xmlelement("td", avg_ms ),
                                                                 xmlelement("td", median_ms ),
                                                                 xmlelement("td", stddev_ms ),
                                                                 xmlelement("td", list_ms_sequential ),
                                                                 xmlelement("td", entryid_max_list ) ) order by stddev_ms desc ) )  col
            from   (  with calc as ( select rowid, 
                                            entryid,
                                            case
                                              when      durationms = max(durationms) over(partition by functiongroup) 
                                                   and  count(1) over ( partition by functiongroup,durationms) < 11 then
                                                entryid||'',''||succ_id
                                              else
                                                null
                                            end                                                                entryid_max_list,
                                            durationms, functiongroup, 
                                            rank() over ( partition by functiongroup order by durationms)      rank_asc,
                                            rank() over ( partition by functiongroup order by durationms desc) rank_desc,     
                                            count(1) over ( partition by functiongroup)                        cou_all,
                                            count(1) over ( partition by functiongroup,durationms)             cou_group,
                                            min(durationms) over(partition by functiongroup)                   min_ms,
                                            max(durationms) over(partition by functiongroup)                   max_ms,
                                            round(avg(durationms) over(partition by functiongroup),2)          avg_ms,
                                            round(median(durationms) over(partition by functiongroup),2)       median_ms,
                                            --round(variance(durationms) over(partition by functiongroup),2)   variance_ms,
                                            round(stddev(durationms) over(partition by functiongroup),2)       stddev_ms
                                     from   (  select  rowid, entryid, succ_id, durationms,
                                                       trim( case 
                                                              when instr(functiongroup,''('') > 0 then substr(functiongroup,1,instr(functiongroup,''('')-1)
                                                              else functiongroup
                                                            end  )functiongroup
                                               from    tmp$profile$calc ) ),
                           valuelist as ( select distinct functiongroup,
                                                 listagg(durationms,'','') within group (order by durationms) over (partition by functiongroup) distinct_value_list
                                          from   ( select distinct round(durationms,-3) durationms,
                                                          trim ( case 
                                                                    when instr(functiongroup,''('') > 0 then substr(functiongroup,1,instr(functiongroup,''('')-1)
                                                                    else functiongroup
                                                                  end  ) functiongroup
                                                   from   tmp$profile$calc ) )
                      select distinct 
                             a.functiongroup, --a.durationms, a.rank_asc, a.rank_desc,
                             --b.functiongroup,
                             --a.cou_all * length(a.max_ms) + 1  aggregation_length,
                             a.cou_all, --cou_group,
                             a.min_ms, a.max_ms,
                             a.avg_ms, a.median_ms,
                             a.stddev_ms, -- a.variance_ms,
                             --listagg(a.durationms,'','') within group (order by a.entryid) over (partition by a.functiongroup) list_ms_sequential,
                             nvl( listagg( case
                                             when a.cou_all * length(a.max_ms) + 1 <= 4000 then
                                               a.durationms
                                             else null
                                           end,'','')  within group (order by case
                                                                              when a.cou_all * length(a.max_ms) + 1 <= 4000 then
                                                                                a.entryid
                                                                              else
                                                                                durationms
                                                                              end ) over (partition by a.functiongroup),
                                        ''distinct: ''||b.distinct_value_list ) list_ms_sequential,
                             nvl( listagg(entryid_max_list,'','') within group (order by a.entryid) over (partition by a.functiongroup), ''more than 10 maximums found'') entryid_max_list
                      from   calc a
                      left outer join   valuelist b
                      on     a.functiongroup = b.functiongroup
                      order by stddev_ms desc )';
  
  isr$trace.debug('sSql', 'length: '||length(sSql)||', details see column LOGCLOB.',sCurrentName,to_clob(sSql));
  execute immediate sSql
  into   xReturnXml;
  
  pipe row( xReturnXml );
  isr$trace.stat('end', 'end',sCurrentName);  
  return;

exception
  when others then
    oErrorObj.handleError ( Stb$typedef.cnSeverityCritical, 
                            utd$msglib.getmsg ('exUnexpectedError', nCurrentLanguage,  csP1 => sqlerrm(sqlcode)), 
                            sCurrentName,
                            substr(dbms_utility.format_error_stack ||crlf|| dbms_utility.format_error_backtrace, 1,4000) );                                
    raise;      
end getHtmlStatProfile;

--******************************************************************************
function callFunction( oParameter in  STB$MENUENTRY$RECORD, 
                       olNodeList out STB$TREENODELIST )
  return STB$OERROR$RECORD 
is
  exNoTokenHandle      exception;
  sCurrentName         constant varchar2(100) := $$PLSQL_UNIT||'.callFunction('||oParameter.sToken||')';
  oErrorObj            STB$OERROR$RECORD      := STB$OERROR$RECORD();
  sMsg                 varchar2(4000); 
  sExists                       VARCHAR2(1);
 
begin
  isr$trace.stat('begin', 'oParameter.sToken: '||oParameter.sToken ,sCurrentName);    
  isr$trace.debug('value', 'oParameter IN', sCurrentName, oParameter);
  olNodeList := STB$TREENODELIST();   -- initialize output parameter

  case
    -- ================== Dialogs
    when oParameter.sToken in ('CAN_VIEW_JOB_PROFILE','CAN_DOWNLOAD_PROFILE') then
         olNodeList.extend;
         olNodeList(1) := STB$TREENODE$RECORD(STB$UTIL.getMaskTitle(oParameter.sToken), $$PLSQL_UNIT, 'TRANSPORT', 'MASK', '');
         isr$trace.debug('value','olNodeList',sCurrentName,olNodeList);
  
         oErrorObj := STB$UTIL.getPropertyList(oParameter.sToken, olNodeList(1).tloPropertylist);
         isr$trace.debug('value','olNodeList after getPropertyList',sCurrentName,olNodeList);
    else
      raise exNoTokenHandle;
  end case;

  isr$trace.debug('value','olNodeList',sCurrentName,olNodeList);
  isr$trace.stat('end', 'olNodeList.count: '||olNodeList.count, sCurrentName);
  return oErrorObj;
exception
 when exNoTokenHandle then
   sMsg := utd$msglib.getmsg ('exNoTokenHandle', stb$security.getcurrentlanguage, oParameter.sToken);
   oErrorObj.handleError ( Stb$typedef.cnSeverityWarning, sMsg, sCurrentName, 'user defined exception: exNoTokenHandle'||stb$typedef.crlf||dbms_utility.format_error_stack ||stb$typedef.crlf|| dbms_utility.format_error_backtrace );
   rollback;
   return oErrorObj;
 when others then
   sMsg :=  utd$msglib.getmsg ('exUnexpectedError', nCurrentLanguage, csP1 => sqlerrm(sqlcode));
   oErrorObj.handleError ( Stb$typedef.cnSeverityWarning, sMsg, sCurrentName,  dbms_utility.format_error_stack ||stb$typedef.crlf|| dbms_utility.format_error_backtrace  );  
   rollback;
   return oErrorObj;
end callFunction;

--******************************************************************************
-- vs 17.02.2016 checkDependenciesOnMask probably not required in Profler Package ?!
function checkDependenciesOnMask( oParameter    in     STB$MENUENTRY$RECORD,
                                  olParameter   in out STB$PROPERTY$LIST,
                                  sModifiedFlag out    varchar2 )
  return STB$OERROR$RECORD
is
  sCurrentName      constant varchar2(100)  := $$PLSQL_UNIT||'.checkDependenciesOnMask('||oParameter.sToken||')';
  oErrorObj         STB$OERROR$RECORD       := STB$OERROR$RECORD(null,null,Stb$typedef.cnNoError);
  olParameterOld    STB$PROPERTY$LIST       := STB$PROPERTY$LIST();
  sMsg              varchar2(4000);

begin
  isr$trace.stat('begin','begin',sCurrentName);
  isr$trace.debug('value','oParameter IN',sCurrentName,oParameter);
  isr$trace.debug('value','olParameter IN OUT',sCurrentName,olParameter);

  olParameterOld := olParameter;
   
  -- compare the objects if something changed only then refresh the mask
  select case when count(1) = 0 
           then 'F' 
           else 'T' 
         end
  into   sModifiedFlag
  from  ( select xmltype(value(p)).getStringVal()
          from   table(olParameter) p
          minus  --
          select xmltype(value(p)).getStringVal ()
          from   table(olParameterOld) p);    
        
  isr$trace.debug('value','olParameter',sCurrentName,olParameter);
  isr$trace.stat('end','sModifiedFlag: '||sModifiedFlag,sCurrentName);
  return oErrorObj;
exception
  when others then
    sMsg :=  utd$msglib.getmsg ('exUnexpectedError', nCurrentLanguage, csP1 => sqlerrm(sqlcode));
    oErrorObj.handleError ( Stb$typedef.cnSeverityWarning, sMsg, sCurrentName,  dbms_utility.format_error_stack ||stb$typedef.crlf|| dbms_utility.format_error_backtrace  );  
    return oErrorObj;
end checkDependenciesOnMask;

--*************************************************************************************************************************
function putParameters( oParameter  in STB$MENUENTRY$RECORD, 
                        olParameter in STB$PROPERTY$LIST,
                        clParameter in clob default null   )  
  return STB$OERROR$RECORD
is
  exNoTokenHandle     exception;
  sCurrentName        constant varchar2(100) := $$PLSQL_UNIT||'.putParameters('||oParameter.sToken||')';    
  oErrorObj           STB$OERROR$RECORD      := STB$OERROR$RECORD(tloMessageList => ISR$MESSAGE$LIST());  --initialize the message list too
  sMsg                varchar2(4000);
  sWhereClause        varchar2(4000);
  sUserName           varchar2(30);
  sStartDate          varchar2(1000);
  sEndDate            varchar2(1000);
  recJobIds           STB$SYSTEM.tJobIdRecord;
  sTargetFileNameFull varchar2(1000);              -- Full qualified target filename including drive and path, e.g.  C:\temp\YYYY.MM.DD_HH24MI-JOB_1234_OID.ISRPROF
  sTargetFilename     varchar2(1000);              -- Target filename only, e.g.  YYYY.MM.DD_HH24MI-JOB_1234_OID.ISRPROF
  sTargetPath         varchar2(2000);              -- Target path only, e.g.  C:\temp\
  
begin
  isr$trace.stat('begin','begin',sCurrentName);
  isr$trace.debug('value','oParameter',sCurrentName,oParameter);
  isr$trace.debug('value','olParameter',sCurrentName,olParameter);
  isr$trace.debug('value','length(clParameter): '||length(clParameter),sCurrentName,clParameter);

  case
    when  oParameter.sToken = 'CAN_VIEW_JOB_PROFILE' then
      -- build where clause for job
      sUserName  := STB$SECURITY.getOracleUsername(STB$SECURITY.getCurrentUser);
      sStartDate := 'to_date('''||to_char(trunc(sysdate,'MI'),'dd.mm.yyyy hh24:mi')||''',''dd.mm.yyyy hh24:mi'')';
      sEndDate   := 'to_date('''||to_char(trunc(sysdate,'MI'),'dd.mm.yyyy hh24:mi')||''',''dd.mm.yyyy hh24:mi'')';
      sWhereClause := ' where oracleusername = '''||sUserName||''' and timestamp beetween '||sStartDate||' and '||sEndDate;    
      isr$trace.debug('value','sWhereClause: '||sWhereClause,sCurrentname);       
    when  oParameter.sToken = 'CAN_DOWNLOAD_PROFILE'  then
      -- build where clause for job
      sUserName  := STB$SECURITY.getOracleUsername(STB$SECURITY.getCurrentUser);
      sStartDate := 'to_date('''||to_char(trunc(sysdate,'MI'),'dd.mm.yyyy hh24:mi')||''',''dd.mm.yyyy hh24:mi'')';
      sEndDate   := 'to_date('''||to_char(trunc(sysdate,'MI'),'dd.mm.yyyy hh24:mi')||''',''dd.mm.yyyy hh24:mi'')';
      sWhereClause := ' where oracleusername = '''||sUserName||''' and timestamp beetween '||sStartDate||' and '||sEndDate;    
      isr$trace.debug('value','sWhereClause: '||sWhereClause,sCurrentname); 
    else
      raise exNoTokenHandle;
  end case;     
    
  open  STB$SYSTEM.cGetJobIds( oParameter.sToken, null );
  fetch STB$SYSTEM.cGetJobIds
  into  recJobIds;
  close STB$SYSTEM.cGetJobIds;
  isr$trace.debug('values','recJobIds.nOutputId: '||recJobIds.nOutputId||',  recJobIds.nActionId: '||recJobIds.nActionId||',  recJobIds.sFunction: '||recJobIds.sFunction,sCurrentname);       
 
  if recJobIds.nActionId is null then
    sMsg := utd$msglib.getmsg ('exNotFindAction', stb$security.getcurrentlanguage, oParameter.sToken);
    raise stb$job.exNotFindAction;
  end if;
  
  select STB$JOB$SEQ.nextval
  into   STB$JOB.nJobid
  from   dual;

  -- Get full target filename from job parameters
  sTargetFilenameFull := STB$UTIL.getProperty(olParameter, 'TO');      
  -- Extract filename only (no drive, no path)
  sTargetFilename := ISR$UTIL.getSplit(sTargetFilenameFull, -1, '\');
  -- Extract path only
  sTargetPath := substr(sTargetFileNameFull,1,instr(sTargetFileNameFull,'\',-1));
  
  oErrorObj := Stb$util.createFilterParameter(olParameter);

  -- Process properties of CAN_DOWNLOAD_PROFILE
  if  oParameter.sToken = 'CAN_DOWNLOAD_PROFILE' then
    STB$JOB.setJobParameter ('LOG_BEGINDATE',STB$UTIL.getProperty(olParameter, 'BEGINDATE'), STB$JOB.nJobid);
    STB$JOB.setJobParameter ('LOG_ENDDATE',  STB$UTIL.getProperty(olParameter, 'ENDDATE'), STB$JOB.nJobid);       
    STB$JOB.setJobParameter ('LOG_USERID',   STB$UTIL.getProperty(olParameter, 'USERNAME'), STB$JOB.nJobid);      
  end if;

  STB$JOB.setJobParameter ('TOKEN', oParameter.sToken, STB$JOB.nJobid);
  STB$JOB.setJobParameter ('OUTPUTID', recJobIds.nOutputId, STB$JOB.nJobid);
  STB$JOB.setJobParameter ('ACTIONID', recJobIds.nActionId, STB$JOB.nJobid);
  STB$JOB.setJobParameter ('WHERE', sWhereClause, STB$JOB.nJobid);
  STB$JOB.setJobParameter ('REFERENCE', STB$OBJECT.getCurrentNodeId, STB$JOB.nJobid);
  STB$JOB.setJobParameter ('FUNCTION', recJobIds.sFunction, STB$JOB.nJobid);           -- defines the individual ActionFunction for the profiler            
  STB$JOB.setJobParameter ('TO', sTargetFilename, STB$JOB.nJobid);
  STB$JOB.setJobParameter ('TO_PATH', sTargetPath, STB$JOB.nJobid);
  STB$JOB.setJobParameter ('ACTIONTYPE', 'SAVE', STB$JOB.nJobid);        
  STB$JOB.setJobParameter ('OVERWRITE', 'T', STB$JOB.nJobid);    

  oErrorObj := STB$JOB.startJob;     
  
  isr$trace.stat('end','end',sCurrentName);  
  return oErrorObj;
exception
  when exNoTokenHandle then
    sMsg := utd$msglib.getmsg ('exNoTokenHandle', stb$security.getcurrentlanguage, oParameter.sToken);
    oErrorObj.handleError ( Stb$typedef.cnSeverityWarning, sMsg, sCurrentName, 'user defined exception: exNoTokenHandle'||stb$typedef.crlf||dbms_utility.format_error_stack ||stb$typedef.crlf|| dbms_utility.format_error_backtrace );
    rollback;
    return oErrorObj;
  when stb$job.exNotFindAction then
    oErrorObj.handleError ( Stb$typedef.cnSeverityCritical, sMsg, sCurrentName, dbms_utility.format_error_stack ||stb$typedef.crlf|| dbms_utility.format_error_backtrace );
    rollback;
    return oErrorObj;
  when others then
    sMsg :=  utd$msglib.getmsg ('exUnexpectedError', nCurrentLanguage, csP1 => sqlerrm(sqlcode));
    oErrorObj.handleError ( Stb$typedef.cnSeverityWarning, sMsg, sCurrentName,  dbms_utility.format_error_stack ||stb$typedef.crlf|| dbms_utility.format_error_backtrace  );  
    rollback;
    return oErrorObj;
end putParameters;

--*************************************************************************************************************************
function createProfile( cnJobId in     number,
                        cxXml   in out xmltype )
  return STB$OERROR$RECORD 
is
  oErrorObj         STB$OERROR$RECORD      := STB$OERROR$RECORD();
  sCurrentName      constant varchar2(100) := $$PLSQL_UNIT||'.createProfile('||cnJobId||')';  
  sMsg              varchar2(4000);
  xXmlProfile       xmltype;
  nFilterSessionid  number;                                 -- Sessionid from the Job to be investigated
  nReferencedJobid  stb$jobparameter.parametervalue%type;   -- Jobid of the Job to be Investigated
  sSessionIdList    varchar2(4000);                         -- List of all Sessionids involved with  nReferencedJobid
  sToken            varchar2(100);
  sUserNo           stb$user.userno%type;
  sOracleUsername   isr$log.oracleusername%type;
begin
  isr$trace.info('begin', 'cnJobId: '||cnJobId, sCurrentName);

  sToken := STB$JOB.getJobParameter('TOKEN', cnJobId);
  isr$trace.debug('value', 'sToken: '||sToken, sCurrentName);
  
  case
    when sToken = 'CAN_VIEW_JOB_PROFILE' then
      -- get SessionId to be investigated
      nFilterSessionid := STB$JOB.getJobParameter('FILTER_SESSIONID', cnJobId);  
      isr$trace.debug('variable', 'nFilterSessionid: '||nFilterSessionid, sCurrentName);
      
      -- get JobId to be investigated  [ISRC-708]
      nReferencedJobid := STB$JOB.getJobParameter('REFERENCE', cnJobId);  
      isr$trace.debug('variable', 'nReferencedJobid: '||nReferencedJobid, sCurrentName);     
      
      -- get involved Sessionids in referenced job  [ISRC-708]
      select distinct listagg( sessionid,',' ) within group( order by 1) over( partition by 1)
      into   sSessionIdList
      from   ( select distinct sessionid 
               from   isr$log 
               where  reference like 'JOBID '||nReferencedJobid||' %' );
      isr$trace.debug('variable', 'sSessionIdList: '||sSessionIdList, sCurrentName);         
      
      -- Aggregate log data for profile now
      oErrorObj := isr$profiler.createLogAggregation( csSessionIdList => sSessionIdList );
      -- Analyze data from aggregated data
      oErrorObj := isr$profiler.createLogAnalysis( nReferencedJobid );       -- [ISRC-732]
      -- Build Profiler log hierarchy for the final hierarchical XML-profile output
      oErrorObj := isr$profiler.createLogHierarchy( nReferencedJobid );      -- [ISRC-732]
      
    when sToken = 'CAN_DOWNLOAD_PROFILE' then
      -- clarify user first
      sUserno := STB$JOB.getJobParameter('LOG_USERID', cnJobId);
      if sUserno is null then
        sOracleUserName := null;
      else
        sOracleUserName := STB$SECURITY.getOracleUserName(sUserno);
      end if;
      -- Aggregate log data for profile    
      oErrorObj := isr$profiler.createLogAggregation( cnLowEntryid     => 0,
                                                      cnHighEntryid    => cnHiNum,
                                                      cdLowDate        => nvl(STB$JOB.getJobParameter('LOG_BEGINDATE', cnJobId),'01.01.1800'),
                                                      cdHighdate       => nvl(STB$JOB.getJobParameter('LOG_ENDDATE', cnJobId), '31.12.9999'),
                                                      csOracleUserList => sOracleUserName,  -- if null, then all user entries are selected
                                                      csSessionIdList  => null,
                                                      csIpList         => null,
                                                      csLogTableName   => csIsrLogTable );   
      -- Analyze data from aggregated data
      oErrorObj := isr$profiler.createLogAnalysis();
      -- Build Profiler log hierarchy for the final hierarchical XML-profile output
      oErrorObj := isr$profiler.createLogHierarchy;      
    else 
      isr$trace.warn('WARNING','unknown token',sCurrentName);
      oErrorObj := isr$profiler.createLogAggregation( );    -- [ISRC-732]
      oErrorObj := isr$profiler.createLogAnalysis();
      oErrorObj := isr$profiler.createLogHierarchy;      
  end case;

  if sToken = 'CAN_VIEW_JOB_PROFILE' then  
    -- create hierarchical XML-profile
    select column_value 
    into   xXmlProfile
    from   table(ISR$PROFILER.getXmlHierProfile( csJobId => nReferencedJobid ));
  else
    -- create hierarchical XML-profile
    select column_value 
    into   xXmlProfile
    from   table(ISR$PROFILER.getXmlHierProfile());
  end if;
  isr$trace.debug('value','xXmlProfile',sCurrentName,xXmlProfile);
  
   --  Add XML profile into STB$JOBTRAIL
  insert into stb$jobtrail( jobid, documenttype, filename, 
                            blobflag, reference, mimetype, leadingdoc, download, textfile )
  values                  ( cnJobId, 'PROFILE', STB$JOB.getJobParameter('TO', cnJobId),
                            'C', cnJobId, 'text/xml', 'F', 'T', xXmlProfile.getClobVal()  );
  commit;

  isr$trace.info('end', 'end', sCurrentName);
  return oErrorObj;
exception
  when others then
    sMsg :=  utd$msglib.getmsg ('exUnexpectedError', nCurrentLanguage, csP1 => sqlerrm(sqlcode));
    oErrorObj.handleError ( Stb$typedef.cnSeverityWarning, sMsg, sCurrentName,  dbms_utility.format_error_stack ||stb$typedef.crlf|| dbms_utility.format_error_backtrace  );  
    rollback;
    return oErrorObj;
end createProfile;

--*************************************************************************************************************************
function setMenuAccess( sParameter   in  varchar2, 
                        sMenuAllowed out varchar2 )
  return STB$OERROR$RECORD 
is
  oErrorObj         STB$OERROR$RECORD      := STB$OERROR$RECORD();
  sCurrentName      constant varchar2(100) := $$PLSQL_UNIT||'.setMenuAccess('||sParameter||')';  
  oCurNode          STB$TREENODE$RECORD    := STB$TREENODE$RECORD();
  nJobId            number;
  nSessionId        number;
  
  cursor curJobSession( ccnJobId number ) is
    select oraclejobsessionid
    from   stb$jobsession 
    where  jobid = ccnJobId;  
    
  cursor curLogSession( ccnSessionId number ) is
    select 'T'
    from   isr$log
    where  sessionid = ccnSessionId;      
    
begin
  isr$trace.stat('begin', 'sParameter: ' || sParameter, sCurrentName);
  
  oCurNode := Stb$object.getCurrentNode; 
  isr$trace.debug('value','oCurNode',sCurrentName,oCurNode);
  
  -- offen: Menu ausblenden(Grau), wenn im Trace die Sessionid des Jobs nicht mehr vorkommt
  sMenuallowed := 'T';
  case
    when sParameter  = 'CAN_VIEW_JOB_PROFILE' then
      for dummy in 1..1 loop
        -- check parameters
        sMenuAllowed := STB$UTIL.getSystemParameter('CAN_VIEW_JOB_PROFILE');
        exit when sMenuAllowed = 'F';
        
        nJobid := oCurNode.sDisplay;
        isr$trace.debug('value','nJobId: '||nJobId,sCurrentName,oCurNode);
        
        sMenuAllowed := nvl(STB$JOB.getJobParameter('FILTER_CREATE_RUNTIME_PROFILE', nJobId)
                            , nvl(STB$JOB.getJobParameter('INVISIBLE_FILTER_CREATE_RUNTIME_PROFILE', nJobId)
                            , 'F')
                           );      -- oCurNode.sDisplay == jobid == STB$OBJECT.getCurrentNodeId
        exit when sMenuAllowed = 'F';
        
        -- get sessionid of oracle jobsession
        open curJobSession(nJobId);
        fetch curJobSession into nSessionId;
        close curJobSession;
        isr$trace.debug('value','nSessionId: '||nSessionId,sCurrentName);
        -- if there is no oracle job session id found, then there is no chance in the log
        if nSessionId is null then
          sMenuAllowed := 'F';
          exit;
        end if;
       
        -- check if sessionid exists in trace log
        open curLogSession(nSessionId);
        fetch curLogSession into sMenuAllowed;
        if curLogSession%notfound then
          sMenuAllowed := 'F';
        end if;
        close curLogSession;
      end loop;

    when sParameter  = 'CAN_DOWNLOAD_PROFILE' then
      -- check parameter
      sMenuallowed := nvl(STB$UTIL.getSystemParameter('CAN_DOWNLOAD_PROFILE'),'F');
   
    else 
      sMenuallowed := 'F';
    end case;

  isr$trace.stat('end', 'sMenuAllowed: ' || sMenuAllowed, sCurrentName);
  return oErrorObj;
exception
  when others then
    oErrorObj.handleError ( Stb$typedef.cnSeverityWarning, 
                            utd$msglib.getmsg ('exUnexpectedError', nCurrentLanguage,  csP1 => sqlerrm(sqlcode)), 
                            sCurrentName,
                            substr(dbms_utility.format_error_stack ||crlf|| dbms_utility.format_error_backtrace, 1,4000) );                                
   sMenuAllowed := 'F';
   return oErrorObj;
end setMenuAccess;

-- ***************************************************************************************************
function getLov( csEntity        in  varchar2,   --  = ISR$DIALOG.PARAMETERNAME
                 csSuchwert      in  varchar2,
                 oSelectionList  out ISR$TLRSELECTION$LIST ) 
  return STB$OERROR$RECORD 
is
  sCurrentName constant varchar2(100) := $$PLSQL_UNIT||'.getLov('||csEntity||')';
  sMsg         varchar2(4000);
  nCounter     number            := 0;
  oErrorObj    STB$OERROR$RECORD := STB$OERROR$RECORD();

  cursor curUser is
    select coalesce(oracleusername, ldapusername, fullname, to_char(userno)) username, 
           userno,
           coalesce(fullname, to_char(userno)) fullname
    from   stb$user 
    where  activeuser = 'T'
    order by 1;

begin
  isr$trace.stat('begin','csEntity: '||csEntity||',  csSuchwert: '||csSuchwert,sCurrentName);
  oSelectionList := ISR$TLRSELECTION$LIST ();

  case
    when csEntity = 'USERNAME' then
      for recUser in curUser loop
        oSelectionList.extend;
        oSelectionList(oSelectionList.count()) := ISR$OSELECTION$RECORD(recUser.username, recUser.userno, recUser.fullname, null, oSelectionList.COUNT());
      end loop;
  end case;

  isr$trace.debug('value','oSelectionList',sCurrentName,oSelectionList);
  isr$trace.stat('end','end', sCurrentName);
  return oErrorObj;
exception
  when others then
    sMsg :=  utd$msglib.getmsg ('exUnexpectedError', stb$security.getcurrentlanguage, csP1 => sqlerrm(sqlcode));
    oErrorObj.handleError ( Stb$typedef.cnSeverityCritical, sMsg, sCurrentName, 
                            substr(dbms_utility.format_error_stack || CHR(10) || dbms_utility.format_error_backtrace, 1,4000)  );  
    return oErrorObj;    
end getLov;

end ISR$profiler;