CREATE OR REPLACE package body stb$system is
  -- local variable to store the values of the current node"
  oCurrentParameter             STB$PROPERTY$LIST;
  olNodeListBackup              STB$TREENODELIST;
  nOutputId                     ISR$REPORT$OUTPUT$TYPE.OUTPUTID%type;
  nActionId                     ISR$ACTION.ACTIONID%type;
  sFunction                     ISR$REPORT$OUTPUT$TYPE.ACTIONFUNCTION%type;
  sDefaultTablespace            user_users.default_tablespace%type;
  sNologTablespace              user_tablespaces.tablespace_name%type;
  nCurrentLanguage              constant number       := stb$security.getCurrentLanguage;

  cursor cPrimaryKeys(sTableName varchar2) is
    select distinct cols.column_name
      from all_constraints cons, all_cons_columns cols
     where cols.table_name = sTableName
       and cons.constraint_type = 'P'
       and cons.constraint_name = cols.constraint_name
       and cons.owner = cols.owner
     order by 1;

  cursor cGetJobIds( cstoken         in isr$action.token%type,
                     csdocdefinition in ISR$ACTION$OUTPUT.DOC_DEFINITION%type) return tJobIdRecord is
     select ao.outputid, ao.actionid, o.actionfunction myfunction
     from   isr$action A, isr$report$output$type o, isr$action$output ao
     where  (UPPER (o.token) = UPPER (cstoken) or UPPER (A.token) = UPPER (cstoken))
     and o.reporttypeid is null
     and o.outputid = ao.outputid
     and A.actionid = ao.actionid
     and ao.doc_definition = NVL(csdocdefinition,'NOT_DETERMINED');

  sUnlockAsDBA                    varchar2(1) := 'F';

  number_of_rows    number := 0;
  sNewLoad          varchar2(2) := 'T';
  sCurrentNodetype  varchar2(500);
  sUserName         varchar2 (255) := STB$SECURITY.getOracleUserName (STB$SECURITY.getCurrentUser);

  --****************************************************************************************************
  procedure destroyOldValues
  is
    sCurrentName   constant varchar2(4000) := $$PLSQL_UNIT||'.destroyOldValues';
  begin
    isr$trace.stat('begin', 'begin', sCurrentName);
    STB$OBJECT.setCurrentToken('');
    oCurrentParameter := STB$PROPERTY$LIST ();
    isr$trace.stat('end', 'end', sCurrentName);
  end destroyOldValues;
  --**********************************************************************************************************************************
  function SwitchCurrentGroupRight(sToken in varchar2)
    return STB$OERROR$RECORD is
    sCurrentName         constant varchar2(100) := $$PLSQL_UNIT||'SwitchCurrentGroupRight('||sToken||')';
    oErrorObj                     STB$OERROR$RECORD := STB$OERROR$RECORD();
    nUserGroupNo                  number;
    sParameter                    STB$ADMINRIGHTS.PARAMETERNAME%type;
    nModified                     number;
    oCurNode                      STB$TREENODE$RECORD := STB$TREENODE$RECORD();
  begin
    isr$trace.stat('begin', 'begin', sCurrentName);
    oCurNode := Stb$object.getCurrentNode;
    nUserGroupNo := STB$OBJECT.getNodeIdForNodetype('FUNCTIONGROUP');
    sParameter := SUBSTR(stb$object.getCurrentNodeId, 1, INSTR(stb$object.getCurrentNodeId, '#@#')-1);
    isr$trace.debug ('nUserGroupNo/sParameter',  nUserGroupNo || ' / ' || sParameter, sCurrentName);

    oErrorObj := Stb$audit.openAudit ('GROUPAUDIT', TO_CHAR (nUserGroupNo));
    oErrorObj := stb$audit.addauditrecord ( sToken );

    nModified := 1;
    isr$trace.debug ('old value',  oCurNode.tloPropertyList (1).sValue, sCurrentName);

    -- update the table and write the switch in the auditfile
    if oCurNode.tloPropertyList (1).sValue = 'T' then
      -- Right is enabled will be switched to disabled
      update STB$ADMINRIGHTS
         set parametervalue = 'F'
       where parametername = sParameter and usergroupno = nUsergRoupNo;

      -- write the audit entry
      oErrorObj := Stb$audit.AddAuditEntry (sParameter, 'T', 'F', nModified);
    else
      -- Right is disabled will be switched to enabled
      update STB$ADMINRIGHTS
         set parametervalue = 'T'
       where parametername = sParameter and usergroupno = nUsergRoupNo;

      -- write the audit entry
      oErrorObj := Stb$audit.AddAuditEntry (sParameter, 'F', 'T', nModified);
    end if;

    isr$trace.debug ('end', 'end', sCurrentName);
    return oErrorObj;
  exception
    when others then
      oErrorObj.handleError ( Stb$typedef.cnSeverityCritical, utd$msglib.getmsg ('exUnexpectedError', stb$security.getCurrentlanguage,  csP1 => sqlerrm(sqlcode)),
        sCurrentName, dbms_utility.format_error_stack ||chr(10)|| dbms_utility.format_error_backtrace );
      return oErrorObj;
  end SwitchCurrentGroupRight;

  --***********************************************************************************************************
  function unLockReports (sWhereClause in varchar2)
    return STB$OERROR$RECORD is
    pragma autonomous_transaction;
    oErrorObj              STB$OERROR$RECORD := STB$OERROR$RECORD();
    sDynSQL                       varchar2 (4000);
    sCurrentName   varchar2(100) := $$PLSQL_UNIT||'.unLockReports';
    sMsg           varchar2(4000);

    type tGetReports is ref cursor;
    cGetReports                 tGetReports;
    rGetReports                 STB$REPORT%rowtype;
  begin
    isr$trace.stat('begin', 'begin', sCurrentName);
    isr$trace.debug('input parameter', 'sWhereClause: ' || sWhereClause, sCurrentName);
    sDynSQL :=
         ' update stb$report '
      || ' set condition = '
      || Stb$typedef.cnStatusActivated
      || ', lockedby = null '
      || ', locksession = null '
      || ', lockedOn = null '
      || ' where condition = '
      || Stb$typedef.cnStatusLocked
      || ' '
      || sWhereClause;
    isr$trace.debug_all ('sDynSQL', sDynSQL, sCurrentName);

    if sUnlockAsDBA = 'T' then
      open cGetReports for 'SELECT * FROM STB$REPORT where condition = '||Stb$typedef.cnStatusLocked||' '||sWhereClause;
      loop
        fetch cGetReports
        into rGetReports;
        exit when cGetReports%notfound;
        ISR$ACTION$LOG.setActionLog('MAINTENANCE', Utd$msglib.GetMsg ('UNLOCK_REPORT', Stb$security.GetCurrentLanguage)||' '||rGetReports.title||' ['||rGetReports.repid||']');
      end loop;
      close cGetReports;
    end if;

    execute immediate sDynSQL;
    number_of_rows := sql%rowcount;

    commit;

    if number_of_rows > 0 and sUnlockAsDBA = 'F' then
      if STB$UTIL.checkMethodExists('triggerRibbonUpdate', 'ISR$RIBBON') = 'T' then
        execute immediate 'BEGIN
                             ISR$RIBBON.triggerRibbonUpdate;
                           END;';
      end if;
    end if;

    isr$trace.stat ('end', 'end', sCurrentName);
    return oErrorObj;
  exception
    when others then
      oErrorObj.handleError ( Stb$typedef.cnSeverityWarning, utd$msglib.getmsg ('exUnexpectedError', stb$security.getCurrentlanguage,  csP1 => sqlerrm(sqlcode)),
        sCurrentName, dbms_utility.format_error_stack ||chr(10)|| dbms_utility.format_error_backtrace );
      return oErrorObj;
  end unLockReports;

  --***********************************************************************************************************
  function undoCheckOutReports (sWhereClause in varchar2)
    return STB$OERROR$RECORD
  is
    pragma autonomous_transaction;
    sCurrentName   varchar2(100) := $$PLSQL_UNIT||'.undoCheckOutReports';
    oErrorObj                     STB$OERROR$RECORD := STB$OERROR$RECORD();
    sDynSQL                       varchar2 (4000);

    type tGetDocuments is ref cursor;
    cGetDocuments                 tGetDocuments;
    rGetDocuments                 STB$DOCTRAIL%rowtype;
  begin
    isr$trace.stat('begin', 'begin', sCurrentName);
    isr$trace.debug ('parameter',  'sWhereClause:' || sWhereClause, sCurrentName);
    sDynSQL :=
         'begin  delete from ISR$DOC$PROPERTY  '
      || sWhereClause
      || ';'
      || ' delete from STB$DOCTRAIL '
      || sWhereClause
      || ' and doctype =''D'';'
      || ' update stb$doctrail '
      || ' set checkedout = ''F'' '
      || ', path = null '
      || ', machine = null '
      || ', oracleuser = null '
      || sWhereClause
      || ' and doctype =''C'';'
      || ' end;';
    isr$trace.debug ('sDynSQL',  sDynSQL, sCurrentName);

    open cGetDocuments for 'SELECT * FROM STB$DOCTRAIL '||sWhereClause;
    loop
      fetch cGetDocuments
      into rGetDocuments;
      exit when cGetDocuments%notfound;
      ISR$ACTION$LOG.setActionLog('MAINTENANCE', Utd$msglib.GetMsg ('UNDOCHECKOUT_REPORT', Stb$security.GetCurrentLanguage)||' '||rGetDocuments.filename||' ['||rGetDocuments.docid||']');
    end loop;
    close cGetDocuments;

    execute immediate sDynSQL;

    commit;

    isr$trace.stat('end', 'end', sCurrentName);
    return oErrorObj;
  exception
    when others then
      oErrorObj.handleError ( Stb$typedef.cnSeverityWarning, utd$msglib.getmsg ('exUnexpectedError', stb$security.getCurrentlanguage,  csP1 => sqlerrm(sqlcode)),
        sCurrentName, dbms_utility.format_error_stack ||chr(10)|| dbms_utility.format_error_backtrace );
      return oErrorObj;
  end undoCheckOutReports;

  --***********************************************************************************************************
  function checkInReports (sWhereClause in varchar2)
    return STB$OERROR$RECORD
  is
    pragma autonomous_transaction;
    sCurrentName         constant varchar2(4000) := $$PLSQL_UNIT||'.checkInReports';
    oErrorObj                     STB$OERROR$RECORD := STB$OERROR$RECORD();
    sDynSQL                       varchar2 (4000);
    nReporttypeid                 number;

    type tGetDocuments is ref cursor;
    cGetDocuments                 tGetDocuments;
    rGetDocuments                 STB$DOCTRAIL%rowtype;

    sUserMsg         VARCHAR2(4000);

  begin
    isr$trace.stat('begin', 'begin', sCurrentName);
    isr$trace.debug ('parameter', 'sWhereClause:' || sWhereClause, sCurrentName);

    sDynSQL := 'SELECT * FROM stb$doctrail ' || sWhereClause;
    isr$trace.debug ('sDynSQL', sDynSQL, sCurrentName);

    open cGetDocuments for sDynSQL;
    loop
      fetch cGetDocuments
      into  rGetDocuments;
      exit when cGetDocuments%notfound;

      isr$trace.debug ('rGetDocuments.docid',  rGetDocuments.docid, sCurrentName);

      execute immediate 'update stb$doctrail set job_flag = ''O'' where docid = '||rGetDocuments.docid;
      commit;

      select stb$job$seq.NEXTVAL
      into   stb$job.njobid
      from   DUAL;

      isr$trace.debug ('stb$job.njobid',  stb$job.njobid, sCurrentName);

      if rGetDocuments.doctype = 'D' and rGetDocuments.nodetype = 'REPORT' then
        stb$job.setJobParameter ('TOKEN', 'CAN_CHECKIN', stb$job.njobid);
        open cGetJobIds ('CAN_CHECKIN', rGetDocuments.doc_definition);
        fetch cGetJobIds
        into  nOutputId, nActionId, sFunction;
        close cGetJobIds;
        stb$job.setJobParameter ('ACTIONTYPE', 'CHECKIN', stb$job.njobid);
      elsif rGetDocuments.doctype = 'C' then
        stb$job.setJobParameter ('TOKEN', 'CAN_CHECKIN_COMMON_DOC', stb$job.njobid);
        open cGetJobIds ('CAN_CHECKIN_COMMON_DOC', rGetDocuments.doc_definition);
        fetch cGetJobIds
        into  nOutputId, nActionId, sFunction;
        close cGetJobIds;
        stb$job.setJobParameter ('ACTIONTYPE', 'CHECKIN', stb$job.njobid);
      else -- LABONE
        stb$job.setJobParameter ('TOKEN', 'CAN_UPLOAD_EXCEL_TEMPLATE', stb$job.njobid);
        open cGetJobIds ('CAN_UPLOAD_EXCEL_TEMPLATE', rGetDocuments.doc_definition);
        fetch cGetJobIds
        into  nOutputId, nActionId, sFunction;
        close cGetJobIds;

        stb$job.setJobParameter ('ACTIONTYPE', 'CHECKIN_DOCUMENT', stb$job.njobid);
        begin
          select reporttypeid into nReporttypeid from stb$report where TO_CHAR(repid) = rGetDocuments.nodeid and rGetDocuments.nodetype in ('WORKSHEET');
        exception when others then
          begin
            select reporttypeid into nReporttypeid from stb$reporttype where (rGetDocuments.nodetype like reporttypename || '%');
          exception when others then
            null;
          end;
        end;
        stb$job.setJobParameter ('REPORTTYPEID', nReporttypeid, stb$job.njobid);
      end if;

      if nActionId is null then
        sUserMsg := utd$msglib.getmsg ('exNotFindAction', stb$security.getcurrentlanguage, stb$job.getJobParameter ('TOKEN',  stb$job.njobid));
        raise stb$job.exNotFindAction;
      end if;

      isr$trace.debug ('nOutputId',  nOutputId, sCurrentName);
      isr$trace.debug ('nActionId',  nActionId, sCurrentName);
      isr$trace.debug ('sFunction',  sFunction, sCurrentName);

      stb$job.setJobParameter ('DELETE_FILE', stb$util.getSystemParameter ('DELETE_FILE'), stb$job.njobid);

      stb$job.setJobParameter ('ACTIONID', nActionId, stb$job.njobid);
      stb$job.setJobParameter ('OUTPUTID', nOutputId, stb$job.njobid);
      stb$job.setJobParameter ('FUNCTION', sFunction, stb$job.nJobid);
      stb$job.setJobParameter ('TO', rGetDocuments.PATH||'\'||rGetDocuments.filename, stb$job.njobid);
      stb$job.setJobParameter ('REFERENCE', rGetDocuments.docid, stb$job.njobid);
      stb$job.setJobParameter ('DOCID', rGetDocuments.docid, stb$job.njobid);
      stb$job.setJobParameter ('DOCTYPE', rGetDocuments.doctype, stb$job.njobid);
      stb$job.setJobParameter ('NODEID', rGetDocuments.nodeid, stb$job.njobid);
      stb$job.setJobParameter ('NODETYPE', rGetDocuments.nodetype, stb$job.njobid);

      oErrorObj := stb$job.startjob;

      ISR$ACTION$LOG.setActionLog('MAINTENANCE', Utd$msglib.GetMsg ('CHECKIN_REPORT', Stb$security.GetCurrentLanguage)||' '||rGetDocuments.filename||' ['||rGetDocuments.docid||']');

    end loop;
    close cGetDocuments;

    isr$trace.stat('end', 'end', sCurrentName);
    return oErrorObj;
  exception
     when stb$job.exNotFindAction then
      oErrorObj.handleError ( Stb$typedef.cnSeverityCritical, sUserMsg, sCurrentName, dbms_utility.format_error_stack ||stb$typedef.crlf|| dbms_utility.format_error_backtrace );
      return oErrorObj;
    when others then
      sUserMsg  :=Utd$msglib.GetMsg('ERROR',Stb$security.GetCurrentLanguage) || SQLCODE || ' ' || SQLERRM;
      oErrorObj.handleError ( Stb$typedef.cnSeverityWarning, sUserMsg, 'STB$SYSTEM.checkInReports',
              dbms_utility.format_error_stack || CHR(10) || dbms_utility.format_error_backtrace);
      return oErrorObj;
  end checkInReports;

   --***********************************************************************************************************
  function reactivateReports (sWhereClause in varchar2)
    return STB$OERROR$RECORD
  is
    pragma autonomous_transaction;
    sCurrentName        constant varchar2(4000) := $$PLSQL_UNIT||'.reactivateReports';
    oErrorObj                     STB$OERROR$RECORD := STB$OERROR$RECORD();
    sDynSQL                       varchar2 (4000);

    type tGetReports is ref cursor;
    cGetReports                 tGetReports;
    rGetReports                 STB$REPORT%rowtype;
  begin
    isr$trace.stat('begin', 'begin', sCurrentName);
    isr$trace.debug ('parameter', 'sWhereClause:' || sWhereClause, sCurrentName);

    sDynSQL := 'update stb$report set modifiedon = sysdate, modifiedby = '''||sUserName||''', condition = ' || STB$TYPEDEF.cnStatusActivated || ' ' || sWhereClause;
    isr$trace.debug ('sDynSQL', sDynSQL, sCurrentName);

    open cGetReports for 'SELECT * FROM STB$REPORT '||sWhereClause;
    loop
      fetch cGetReports
      into rGetReports;
      exit when cGetReports%notfound;
      ISR$ACTION$LOG.setActionLog('MAINTENANCE', Utd$msglib.GetMsg ('REACTIVATE_REPORT', Stb$security.GetCurrentLanguage)||' '||rGetReports.title||' ['||rGetReports.repid||']');
    end loop;
    close cGetReports;

    execute immediate sDynSQL;

    -- ISRC-1044
    sDynSQL :=
      ' DECLARE
          oErrorObj STB$OERROR$RECORD := STB$OERROR$RECORD();
        BEGIN
          FOR rGetReports IN (select repid, srnumber from stb$report '||sWhereClause||') LOOP
            STB$OBJECT.setCurrentRepid(rGetReports.repid);
            oErrorObj := isr$reporttype$package.createsraudit (rGetReports.srnumber);

            DELETE FROM ISR$REPORT$GROUPING$TABLE
             WHERE (repid) = STB$OBJECT.getCurrentRepid;

            INSERT INTO ISR$REPORT$GROUPING$TABLE (GROUPINGNAME, GROUPINGID, GROUPINGVALUE, GROUPINGDATE, REPID)
               (SELECT rg.GROUPINGNAME,rg.GROUPINGID,rg.GROUPINGVALUE,rg.GROUPINGDATE,rg.REPID
                  FROM isr$report$grouping rg
                 WHERE rg.repid = STB$OBJECT.getCurrentRepid);

            STB$OBJECT.setCurrentRepid(null);
          END LOOP;
        END;';
    isr$trace.debug ('sDynSQL',  sDynSQL, sCurrentName);
    execute immediate sDynSQL;

    commit;

    isr$trace.stat('end', 'end', sCurrentName);
    return oErrorObj;
  exception
    when others then
      oErrorObj.handleError ( Stb$typedef.cnSeverityWarning, utd$msglib.getmsg ('exUnexpectedError', stb$security.getCurrentlanguage,  csP1 => sqlerrm(sqlcode)),
        sCurrentName, dbms_utility.format_error_stack ||chr(10)|| dbms_utility.format_error_backtrace );
      return oErrorObj;
  end reactivateReports;

  --***********************************************************************************************************
  function restoreReports (sWhereClause in varchar2, olParameter in STB$PROPERTY$LIST)
    return STB$OERROR$RECORD
  is
    pragma autonomous_transaction;
    sCurrentName   constant varchar2(4000) := $$PLSQL_UNIT||'.restoreReports';
    oErrorObj                     STB$OERROR$RECORD := STB$OERROR$RECORD();
    sDynSQL                       varchar2 (4000);

    type tGetZipFiles is ref cursor;
    cGetZipFiles                 tGetZipFiles;
    rGetZipFiles                 ISR$ARCHIVED$REPORTS%rowtype;
    sMsg                  varchar2(4000);

  begin
    isr$trace.stat('begin', 'begin', sCurrentName);
    isr$trace.debug ('parameter', 'sWhereClause:' || sWhereClause, sCurrentName);

    select stb$job$seq.NEXTVAL
    into   stb$job.njobid
    from   DUAL;

    open cGetJobIds ('CAN_CONTROL', null);
    fetch cGetJobIds
    into  nOutputId, nActionId, sFunction;
    close cGetJobIds;

    stb$job.setJobParameter ('TOKEN', 'CAN_CONTROL', stb$job.njobid);
    stb$job.setJobParameter ('OUTPUTID', nOutputId, stb$job.njobid);
    stb$job.setJobParameter ('ACTIONID', nActionId, stb$job.njobid);
    stb$job.setJobParameter ('FUNCTION', sFunction, stb$job.nJobid);

    stb$job.setJobParameter ('ACTIONTYPE', 'UPLOAD', stb$job.njobid);

    sDynSQL := 'SELECT * FROM ISR$ARCHIVED$REPORTS '||sWhereClause;
    isr$trace.debug ('sDynSQL',  sDynSQL, sCurrentName);

    open cGetZipFiles for sDynSQL;
    loop
      fetch cGetZipFiles
      into  rGetZipFiles;
      exit when cGetZipFiles%notfound;

        stb$job.setJobParameter ('TOKEN@'||rGetZipFiles.jobid, 'CAN_EXTRACT_REPORTS', stb$job.njobid);

        oErrorObj := Stb$audit.Openaudit ('SYSTEMAUDIT', TO_CHAR (-1));
        oErrorObj := Stb$audit.Addauditrecord ( 'CAN_EXTRACT_REPORTS' );
        oErrorObj := Stb$audit.Addauditentry (UPPER ('CAN_EXTRACT_REPORTS'), null, utd$msglib.getmsg (rGetZipFiles.nodeType, stb$security.getcurrentlanguage)||': '||rGetZipFiles.nodeId, 1);
        oErrorObj := Stb$audit.Silentaudit;

        open cGetJobIds ('CAN_EXTRACT_REPORTS', null);
        fetch cGetJobIds
        into  nOutputId, nActionId, sFunction;
        close cGetJobIds;

        stb$job.setJobParameter ('ACTIONID@'||rGetZipFiles.jobid, nActionId, stb$job.njobid);
        stb$job.setJobParameter ('OUTPUTID@'||rGetZipFiles.jobid, nOutputId, stb$job.njobid);
        stb$job.setJobParameter ('FUNCTION@'||rGetZipFiles.jobid, sFunction, stb$job.nJobid);
        stb$job.setJobParameter ('TO@'||rGetZipFiles.jobid, STB$UTIL.getProperty(olParameter, 'TO'), stb$job.njobid);
        stb$job.setJobParameter ('DELETE_FILE@'||rGetZipFiles.jobid, STB$UTIL.getProperty(olParameter, 'DELETE_FILE'), stb$job.njobid);
        stb$job.setJobParameter ('CHECKSUM@'||rGetZipFiles.jobid, rGetZipFiles.checksum, stb$job.njobid);

        ISR$ACTION$LOG.setActionLog('MAINTENANCE', Utd$msglib.GetMsg ('RESTORE_ZIPFILE', Stb$security.GetCurrentLanguage)||' '||rGetZipFiles.directory||' ['||rGetZipFiles.nodetype||'/'||rGetZipFiles.nodeid||']');
    end loop;
    close cGetZipFiles;

    oErrorObj := stb$job.startjob;

    isr$trace.stat('end', 'end', sCurrentName);
    return oErrorObj;
  exception
    when stb$job.exNotFindAction then
      oErrorObj.handleError ( Stb$typedef.cnSeverityCritical, sMsg, sCurrentName, dbms_utility.format_error_stack ||stb$typedef.crlf|| dbms_utility.format_error_backtrace );
      return oErrorObj;
    when others then
      oErrorObj.handleError ( Stb$typedef.cnSeverityWarning, utd$msglib.getmsg ('exUnexpectedError', stb$security.getCurrentlanguage,  csP1 => sqlerrm(sqlcode)),
        sCurrentName, dbms_utility.format_error_stack ||chr(10)|| dbms_utility.format_error_backtrace );
      return oErrorObj;
  end restoreReports;

 --***********************************************************************************************************
  function removeJob( nJobId in STB$JOBSESSION.JOBID%type )
    return STB$OERROR$RECORD
  is
    pragma autonomous_transaction;
    sCurrentName                  constant varchar2(100) := $$PLSQL_UNIT||'.removeJob('||nJobId||')';
    oErrorObj                     STB$OERROR$RECORD := STB$OERROR$RECORD();
    sIp                           STB$JOBSESSION.IP%type;
    sPort                         STB$JOBSESSION.PORT%type;
    sOracleJobid2                 STB$JOBQUEUE.ORACLEJOBID%type;

    cursor cGetJobSession (sJobId in varchar2) is
      select js.PORT, js.IP, jq.oraclejobid
      from   stb$jobsession js, stb$jobqueue jq
      where  TO_CHAR (js.jobid) = sJobId
      and    js.jobid = jq.jobid;

    cursor cCheckExists(sJobName in varchar2) is
      select 'T', case when STATE = 'RUNNING' then 'T' else 'F' end
      from   USER_SCHEDULER_JOBS
      where  UPPER(job_name) = UPPER(sJobName);

    sExists       varchar2(1) := 'F';
    sIsRunning    varchar2(1) := 'F';
    sTitle        STB$REPORT.TITLE%TYPE;
  begin
    isr$trace.stat ('begin','nJobId: '||nJobId,sCurrentName);

    open cGetJobSession (nJobId);
    fetch cGetJobSession into sPort, sIp, sOracleJobid2;
    close cGetJobSession;
    isr$trace.debug ('sIP',  sIp,sCurrentName);
    isr$trace.debug ('sPort',  sPort,sCurrentName);
    isr$trace.debug ('sOracleJobid2',  sOracleJobid2,sCurrentName);

    if sOracleJobid2 is not null then
      open cCheckExists(sOracleJobid2);
      fetch cCheckExists into sExists, sIsRunning;
      close cCheckExists;
      if sExists = 'T' then
        if sIsRunning = 'T' then
          begin
            DBMS_SCHEDULER.stop_job(sOracleJobid2, true);
          exception when others then
            null;
          end;
        end if;
        begin
          DBMS_SCHEDULER.drop_job(sOracleJobid2, true);
        exception when others then
          null;
        end;
      end if;
    end if;

    if nJobid is not null then
      if STB$JOB.GETJOBPARAMETER ('FILTER_ITERATION', nJobid) IS NOT NULL then
        sTitle := STB$JOB.getJobParameter('TITLE', nJobid);
      end if;
      STB$JOB.markDBJobAsBroken (nJobid, sIp, sPort, -1, UTD$MSGLIB.GetMsg ('exJobKilledText', Stb$security.GetCurrentLanguage), Stb$typedef.cnSeverityMessage, true);
    end if;

    ISR$ACTION$LOG.setActionLog('MAINTENANCE', Utd$msglib.GetMsg ('REMOVE_JOB', Stb$security.GetCurrentLanguage)||' '||nJobid);

    commit;
    
    if trim(sTitle) is not null then
      ISR$TREE$PACKAGE.sendSyncCall('TITLE', sTitle);
    end if;

    isr$trace.stat('end','end',sCurrentName);
    return oErrorObj;
  exception
    when others then
      oErrorObj.handleError ( Stb$typedef.cnSeverityWarning, utd$msglib.getmsg ('exUnexpectedError', stb$security.getCurrentlanguage,  csP1 => sqlerrm(sqlcode)),
        sCurrentName, dbms_utility.format_error_stack ||chr(10)|| dbms_utility.format_error_backtrace );
      return oErrorObj;
  end removeJob;

  --***********************************************************************************************************
  function removeConnection (nSessionId in number)
    return STB$OERROR$RECORD is
    pragma autonomous_transaction;
    sCurrentName                  constant varchar2(100) := $$PLSQL_UNIT||'.removeConnection('||nSessionId||')';
    oErrorObj                     STB$OERROR$RECORD := STB$OERROR$RECORD();
    sReturn                       varchar2(4000);
  begin
    isr$trace.stat('begin', 'nSessionId: '||nSessionId, sCurrentName );

    if nSessionId != STB$SECURITY.getCurrentSession then

      -- delete all running jobs which don't use the rendering server
      for rGetJobs in ( select distinct js.jobid
                          from STB$JOBSESSION js, STB$JOBQUEUE jq, user_scheduler_jobs us
                         where TRIM(jq.oraclejobid) = TRIM(us.job_name)
                           and js.jobid = jq.jobid
                           and js.sessionid = nSessionId
                           and us.state = 'RUNNING'
                           and (js.ip, js.port) not in (select distinct HOST,  isr$server$base.getServerParameter(serverid,'BASISPORT')  from isr$server where runstatus = 'T' and servertypeid = 6) ) loop
      isr$trace.debug('remove job', 'jobid: '||rGetJobs.jobid, sCurrentName);
        oErrorObj := removeJob (rGetJobs.jobid);
      end loop;

      -- send eot for all running rendering server jobs
      for rGetJobs in ( select distinct s.ip, s.port, js.jobid
                          from STB$JOBSESSION js, STB$JOBQUEUE jq, user_scheduler_jobs us, isr$session s
                         where TRIM(jq.oraclejobid) = TRIM(us.job_name)
                           and js.jobid = jq.jobid
                           and js.sessionid = nSessionId
                           and s.sessionid = js.sessionid
                           and us.state = 'RUNNING'
                           and (js.ip, js.port) in (select distinct HOST, isr$server$base.getServerParameter(serverid,'BASISPORT') from isr$server where runstatus = 'T' and servertypeid = 6) ) loop
      isr$trace.debug('stop loader progress', rGetJobs.ip||' '||rGetJobs.port||' '||rGetJobs.jobid, sCurrentName);
        sReturn := ISR$LOADER.showJobError(rGetJobs.ip, rGetJobs.port, STB$UTIL.getSystemParameter('LOADER_CONTEXT'), STB$UTIL.getSystemParameter('LOADER_NAMESPACE'), rGetJobs.jobid,
           Utd$msglib.GetMsg ('exJobSessionKilledText', Stb$security.GetCurrentLanguage), STB$UTIL.getSystemParameter('TIMEOUT_LOADER_VISUAL'));
      end loop;

      STB$GAL.deleteSessionInfo (nSessionId);

      ISR$ACTION$LOG.setActionLog('MAINTENANCE', Utd$msglib.GetMsg ('REMOVE_SESSION', Stb$security.GetCurrentLanguage)||' '||nSessionId);

    end if;

    isr$trace.stat('end', 'end', sCurrentName);
    return oErrorObj;
  exception
    when others then
      oErrorObj.handleError ( Stb$typedef.cnSeverityWarning, utd$msglib.getmsg ('exUnexpectedError', stb$security.getCurrentlanguage,  csP1 => sqlerrm(sqlcode)),
        sCurrentName, dbms_utility.format_error_stack ||chr(10)|| dbms_utility.format_error_backtrace );
      return oErrorObj;
  end removeConnection;

  --*******************************************************************************************************
  function getSystemParameters (olParameter out STB$PROPERTY$LIST)
    return STB$OERROR$RECORD
  is
    sCurrentName   constant varchar2(4000) := $$PLSQL_UNIT||'.getSystemParameters';
    ncounter                      number := 0;
    oErrorObj                     STB$OERROR$RECORD := STB$OERROR$RECORD();
    oSelectionList                ISR$TLRSELECTION$LIST := ISR$TLRSELECTION$LIST();

    --cursor t oretrieve all editable system parameters that aren't user parameter
    cursor cGetParameter is
      select TRIM (s.parametername) parametername
           , s.parametervalue
           , s.description
           , s.editable
           , s.parametertype
           , s.haslov
           , s.REQUIRED
           , case when parametertype in ('GROUP','TABSHEET') then 'T' when STB$SECURITY.checkGroupRight (s.parametername) = 'T' then s.VISIBLE else 'F' end visible
        from STB$SYSTEMPARAMETER s
       where s.userparameter = 'F'
      order by s.orderno;
  begin
    isr$trace.stat('begin', 'begin', sCurrentName);
    olParameter := STB$PROPERTY$LIST ();

    for rGetParameter in cGetparameter loop
      nCounter := nCounter + 1;
      olParameter.EXTEND;
      olParameter (nCounter) := STB$PROPERTY$RECORD(rGetParameter.parametername, utd$msglib.getmsg (rgetparameter.description, stb$security.getcurrentlanguage),
                                                    rGetParameter.parametervalue, rGetParameter.parametervalue,
                                                    rGetParameter.editable, rGetParameter.REQUIRED, rGetParameter.VISIBLE,
                                                    rGetParameter.haslov, rGetParameter.parametertype);
      if rGetParameter.haslov = 'T'
      and rGetParameter.parametertype != 'FILE'
      and TRIM(rGetParameter.parametervalue) is not null then
        begin
          oErrorObj := STB$SYSTEM.getLov (rGetParameter.parametername, rGetParameter.parametervalue, oSelectionList);

          if oSelectionList.FIRST is not null then
            for j in 1..oSelectionList.COUNT() loop
              if oSelectionList(j).sSelected = 'T' then
                olparameter (olparameter.COUNT()).sDisplay := oSelectionList(j).sDisplay;
              end if;
            end loop;
            if TRIM(olParameter(olparameter.COUNT()).sValue) is null
            and oSelectionList.COUNT() >= 1
            and olParameter(olparameter.COUNT()).sRequired = 'T' then
              olParameter(olparameter.COUNT()).sValue := oSelectionList(1).sValue;
              olParameter(olparameter.COUNT()).sDisplay := oSelectionList(1).sDisplay;
            end if;
          -- no lov
          else
            olparameter (olparameter.COUNT()).sValue := null;
            olparameter (olparameter.COUNT()).sDisplay := null;
          end if;
        exception when others then
          isr$trace.error ('could not set value from lov', 'parameter '||rGetParameter.parametername||' parameter '||rGetParameter.parametervalue,sCurrentName);
        end;
      end if;

      -- display parameters which are null, invisible and required
      if olparameter (nCounter).sValue is null
      and olparameter (nCounter).sDisplay = 'F'
      and olparameter (nCounter).sRequired = 'T' then
        olparameter (nCounter).sDisplay := 'T';
        olparameter (nCounter).sEditable := 'T';
      end if;
    end loop;

    -- store the values localy in the package  for the audit
    oCurrentParameter := olParameter;
    isr$trace.stat('end', 'nCounter: '||nCounter, sCurrentName);
    return (oErrorObj);
  exception
    when others then
      rollback;
      oErrorObj.handleError ( Stb$typedef.cnSeverityCritical, utd$msglib.getmsg ('exUnexpectedError', stb$security.getCurrentlanguage,  csP1 => sqlerrm(sqlcode)),
        sCurrentName, dbms_utility.format_error_stack ||chr(10)|| dbms_utility.format_error_backtrace );
      return (oErrorObj);
  end getSystemParameters;

  --*************************************************************************************************************************************
  function putSystemParameters (olParameter in STB$PROPERTY$LIST, sToken in varchar2)
    return STB$OERROR$RECORD
  is
    sCurrentName   constant varchar2(4000) := $$PLSQL_UNIT||'.putSystemParameters('||sToken||')';
    oErrorObj                     STB$OERROR$RECORD := STB$OERROR$RECORD();
    nLineIndex                    number;
    sComp1                        varchar2 (4000);
    sComp1A                       varchar2 (4000);
    nModified                     number := 0;
  begin
    isr$trace.stat('begin', 'begin', sCurrentName);

    oErrorObj := Stb$audit.openAudit ('SYSTEMAUDIT', TO_CHAR (-1));
    oErrorObj := stb$audit.addauditrecord ( sToken );

    nLineIndex := olParameter.FIRST;

    while (olParameter.EXISTS (nLineIndex)) loop
      if olparameter(nlineindex).sEditable = 'T' and olParameter (nLineindex).TYPE not in ('TABSHEET', 'GROUP') then
        sComp1 := NVL (STB$UTIL.getSystemParameter(olParameter (nLineIndex).sParameter), '');
        sComp1A := NVL (olParameter (nLineIndex).sValue, '');
        isr$trace.debug (nLineIndex, nlineindex||' '||olparameter (nlineindex).sParameter||' '||scomp1||' / '||scomp1a, sCurrentName);
        if (TRIM (sComp1) != TRIM (sComp1A))
        or (TRIM (sComp1) is null and TRIM (sComp1A) is not null)
        or (TRIM (sComp1) is not null and TRIM (sComp1A) is null) then
          nModified := nModified + 1;
          oErrorObj := Stb$audit.AddAuditEntry (olParameter (nLineIndex).sParameter, sComp1, sComp1A, nModified);
          update STB$SYSTEMPARAMETER
             set parametervalue = olParameter (nLineIndex).sValue,
                 modifiedon = SYSDATE,
                 modifiedby = Stb$security.GetCurrentUser
           where parametername = olParameter (nLineIndex).sParameter;
        end if;
      end if;
      nLineIndex := olParameter.NEXT (nLineIndex);
      sComp1 := '';
      sComp1A := '';
    end loop;

    isr$trace.stat('end', 'end', sCurrentName);
    return (oErrorObj);
  exception
    when others then
      rollback;
      oErrorObj.handleError ( Stb$typedef.cnSeverityCritical, utd$msglib.getmsg ('exUnexpectedError', stb$security.getCurrentlanguage,  csP1 => sqlerrm(sqlcode)),
        sCurrentName, dbms_utility.format_error_stack ||chr(10)|| dbms_utility.format_error_backtrace );
      return (oErrorObj);
  end putSystemParameters;

  --*************************************************************************************************************************
  function getLockedReports (olLockedReportsList in out STB$TREENODELIST)
    return STB$OERROR$RECORD
  is
    sCurrentName   constant varchar2(4000) := $$PLSQL_UNIT||'.getLockedReports';
    oErrorObj                     STB$OERROR$RECORD := STB$OERROR$RECORD();
    sSelect                       varchar2 (4000);
  begin
    isr$trace.stat('begin', 'begin', sCurrentName);
    sSelect :=
         '  select rep.repid NODEID, '
      || ' ''F'' marked, '
      || ' rep.srnumber, '
      || ' rep.title || '' '' || rep.version NODENAME, '
      || ' rep.locksession, '
      || ' NVL(us.ldapusername, us.oracleusername) lockedby, '
      || ' rep.lockedOn '
      || ' from stb$report rep , '
      || ' stb$user us '
      || ' where rep.condition = '
      || Stb$typedef.cnStatusLocked
      || ' and rep.lockedby=us.userno '
      || ' order by 3, 4';
    isr$trace.debug (' sSelect',  sSelect, sCurrentName);
    oErrorObj := Stb$util.getColListMultiRow (sSelect, olLockedReportsList);
    isr$trace.debug ('end',  'end', sCurrentName);
    return oErrorObj;
  exception
    when others then
      oErrorObj.handleError ( Stb$typedef.cnSeverityWarning, utd$msglib.getmsg ('exUnexpectedError', stb$security.getCurrentlanguage,  csP1 => sqlerrm(sqlcode)),
        sCurrentName, dbms_utility.format_error_stack ||chr(10)|| dbms_utility.format_error_backtrace );
      return oErrorObj;
  end getLockedReports;

  --*************************************************************************************************************************
  function getCheckedOutReports (olCheckedOutReportsList in out STB$TREENODELIST)
    return STB$OERROR$RECORD is
    sCurrentName   constant varchar2(4000) := $$PLSQL_UNIT||'.getCheckedOutReports';
    oErrorObj                     STB$OERROR$RECORD := STB$OERROR$RECORD();
    sSelect                       varchar2 (4000);
  begin
    isr$trace.stat('begin', 'begin', sCurrentName);
    sSelect :=
         'SELECT doc.docid NODEID, '
      || '''F'' marked, '
      || 'doc.filename NODENAME, '
      || 'doc.docversion as v, '
      || 'DECODE(doc.doctype ,''C'',''COMMONDOC'',''D'',''DOCUMENT'',''X'', ''RAWDATA'',''DOCUMENT'') as doctype, '
      || 'doc.doc_definition, doc.machine, doc.path, doc.modifiedby, '
      || 'doc.modifiedon as modifiedon '
      || 'FROM STB$DOCTRAIL doc '
      || 'WHERE CHECKEDOUT = ''T'' '
      || 'AND JOB_FLAG IS NULL '
      || 'AND (nodetype,nodeid) not in (select ''REPORT'',repid from stb$report where condition = '||Stb$typedef.cnStatusLocked||' ) ';
    if STB$OBJECT.getCurrentNodeType != 'CHECKEDOUT_REPORTS' then
      sSelect := sSelect
               ||'AND oracleuser = '''||sUserName||''' '
               ||'AND path IS NOT NULL '
               ||'AND ((machine = '''||Stb$security.getClientIp||''' AND '''||STB$UTIL.getSystemParameter('STATIC_IP_ADDRESSES')||''' = ''T'')'
               ||' OR '''||STB$UTIL.getSystemParameter('STATIC_IP_ADDRESSES')||''' = ''F'') ';
    end if;
    sSelect := sSelect || 'ORDER BY 6, 7, 2, 3';
    isr$trace.debug (' sSelect',  sSelect, sCurrentName);
    oErrorObj := Stb$util.getColListMultiRow (sSelect, olCheckedOutReportsList);
    isr$trace.stat('end', 'end', sCurrentName);
    return oErrorObj;
  exception
    when others then
      oErrorObj.handleError ( Stb$typedef.cnSeverityCritical, utd$msglib.getmsg ('exUnexpectedError', stb$security.getCurrentlanguage,  csP1 => sqlerrm(sqlcode)),
        sCurrentName, dbms_utility.format_error_stack ||chr(10)|| dbms_utility.format_error_backtrace );
      return oErrorObj;
  end getCheckedOutReports;

  --*************************************************************************************************************************
  function getDeactivatedReports (olDeactivatedReportsList in out STB$TREENODELIST)
    return STB$OERROR$RECORD
  is
    sCurrentName   constant varchar2(4000) := $$PLSQL_UNIT||'.getDeactivatedReports';
    oErrorObj                     STB$OERROR$RECORD := STB$OERROR$RECORD();
    sSelect                       varchar2 (4000);
  begin
    isr$trace.stat('begin', 'begin', sCurrentName);
    sSelect :=
         '  select rep.repid NODEID, '
      || ' ''F'' marked, '
      || ' rep.srnumber, '
      || ' rep.title NODENAME, '
      || ' rep.version, '
      || ' rep.modifiedby, '
      || ' rep.modifiedon as modifiedon '
      || ' from stb$report rep '
      || ' where rep.condition = '
      || Stb$typedef.cnStatusDeactive
      || ' order by 4, 2, 3, 1';
    isr$trace.debug (' sSelect',  sSelect, sCurrentName);
    oErrorObj := Stb$util.getColListMultiRow (sSelect, olDeactivatedReportsList);
    isr$trace.stat('end', 'end', sCurrentName);
    return oErrorObj;
  exception
    when others then
      oErrorObj.handleError ( Stb$typedef.cnSeverityWarning, utd$msglib.getmsg ('exUnexpectedError', stb$security.getCurrentlanguage,  csP1 => sqlerrm(sqlcode)),
        sCurrentName, dbms_utility.format_error_stack ||chr(10)|| dbms_utility.format_error_backtrace );
      return oErrorObj;
  end getDeactivatedReports;

  --*************************************************************************************************************************
  function getArchivedReports (olArchivedReportsList in out STB$TREENODELIST)
    return STB$OERROR$RECORD is
    sCurrentName   constant varchar2(4000) := $$PLSQL_UNIT||'.getArchivedReports';
    oErrorObj                     STB$OERROR$RECORD := STB$OERROR$RECORD();
    sSelect                       varchar2 (4000);
  begin
    isr$trace.stat('begin', 'begin', sCurrentName);
    sSelect :=
         'select jobid NODEID '
      ||      ', jobid NODENAME '
      ||      ', ''F'' marked '
      ||      ', machine '
      ||      ', nodetype onNodeType '
      ||      ', nodeid onNodeId  '
      ||      ', directory '
      ||      ', restored '
      ||      ', modifiedby '
      ||      ', modifiedon '
      || '  from isr$archived$reports '
      || ' order by modifiedon desc';
    isr$trace.debug (' sSelect',  sSelect, sCurrentName);
    oErrorObj := Stb$util.getColListMultiRow (sSelect, olArchivedReportsList);
    isr$trace.stat('end', 'end', sCurrentName);
    return oErrorObj;
  exception
    when others then
      oErrorObj.handleError ( Stb$typedef.cnSeverityWarning, utd$msglib.getmsg ('exUnexpectedError', stb$security.getCurrentlanguage,  csP1 => sqlerrm(sqlcode)),
        sCurrentName, dbms_utility.format_error_stack ||chr(10)|| dbms_utility.format_error_backtrace );
      return oErrorObj;
  end getArchivedReports;

 --*************************************************************************************************************************
function getJobIcon(cnJobId in number, cnProgress in number, csState in varchar2) return varchar2 is
  sCurrentName   constant varchar2(4000) := $$PLSQL_UNIT||'.getJobIcon(' || cnJobId || ')';
  sIcon   varchar2(100);
begin
  isr$trace.stat('begin', 'cnProgress=' || cnProgress, sCurrentName);
  CASE 
    WHEN TRIM(STB$JOB.GETJOBPARAMETER ('JOB_ERROR', cnJobId)) IS NOT NULL then 
      sIcon := 'error';
    WHEN TRIM(STB$JOB.GETJOBPARAMETER ('JOB_WARN', cnJobId)) IS NOT NULL then 
      sIcon := 'warn';
    WHEN cnProgress = 100 then 
      sIcon := 'stop';
    WHEN NVL(csState, 'NO_JOB') = 'SCHEDULED' THEN 
      sIcon := 'time';
    ELSE sIcon := 'run';
  END CASE;
  isr$trace.stat('end', 'sIcon=' || 'gear_' || sIcon, sCurrentName);
  return 'gear_' || sIcon;
end getJobIcon;

  --*************************************************************************************************************************
  function getCurrentJobs (olCurrentJobsList in out STB$TREENODELIST, userJobs in varchar2 default 'F', scheduledJobs in varchar2 default '', titleJobs in varchar2 default 'F')
    return STB$OERROR$RECORD is
    sCurrentName   constant varchar2(4000) := $$PLSQL_UNIT||'.getCurrentJobs';
    oErrorObj                     STB$OERROR$RECORD := STB$OERROR$RECORD();
    sSelect                       varchar2 (4000);
  begin
    isr$trace.stat('begin', 'begin', sCurrentName);
    sSelect :=
         ' select    jq.jobid NODEID '
              || ' , jq.jobid NODENAME '
              || ' , stb$system.getJobIcon(jq.jobid, jq.progress, us.state) icon '
              || ' , NVL(STB$JOB.GETJOBPARAMETER (''PACKAGENAME'', jq.jobid), ''STB$SYSTEM'') PACKAGENAME '
              || ' , jq.jobid ISRJOBID '
              || ' , ''F'' marked '
              || ' , jq.submittime as isrStartTime '
              || ' , jq.progress || '' %'' progress '
              || ' , jq.statustext '
              || ' , STB$JOB.GETJOBPARAMETER(''TOKEN_DESC'', jq.jobid) '
              || ' || (SELECT case when trim(srnumber) is not null then '': '' || srnumber end ||
                              case when trim(title) is not null then '' -> '' || title end  ||
                              case when trim(srnumber) is not null then '' '' || version end
                         FROM stb$report
                        WHERE repid = STB$JOB.getjobparameter (''REPID'', jq.jobid)) description '
              || ' , STB$JOB.GETJOBPARAMETER(''JOB_ERROR'', jq.jobid) isrErrorcode '
              || ' , js.ip, js.port'
              || ' , NVL((select distinct ''T'' from isr$server where runstatus = ''T'' and servertypeid = 6 and host = js.ip and port = js.port), ''F'') RenderingServer '
              || ' , STB$SECURITY.getOracleUserName(jq.userno) job_creator '
              || ' , case when jq.userno = '||STB$SECURITY.getCurrentUser||' then ''T'' else ''F'' end  OwnJob '
      || '  from user_scheduler_jobs us, STB$JOBQUEUE jq, STB$JOBSESSION js '
      || ' where TRIM(us.job_name(+)) = TRIM(jq.oraclejobid) '
      || '   and jq.jobid = js.jobid '
      || case when titleJobs = 'T' and STB$OBJECT.getCurrentTitle is not null then 'AND jq.jobid IN (select jobid from stb$jobparameter where parametername = ''TITLE'' and parametervalue = '''||STB$OBJECT.getCurrentTitle||''') ' end
      || case when userJobs = 'T' then 'AND jq.userno = '||STB$SECURITY.getCurrentUser||' ' end
      || case when scheduledJobs = 'T' then 'AND STB$JOB.GETJOBPARAMETER (''FILTER_ITERATION'', jq.jobid) IS NOT NULL '
              when scheduledJobs = 'F' then 'AND STB$JOB.GETJOBPARAMETER (''FILTER_ITERATION'', jq.jobid) IS NULL ' end
      || ' order by 1 desc';

    isr$trace.debug ('sSelect',  sSelect, sCurrentName);
    oErrorObj := Stb$util.getColListMultiRow (sSelect, olCurrentJobsList);

    begin
      if olCurrentJobsList.COUNT() > 0 then
        for i in 1..olCurrentJobsList.COUNT() loop
          olCurrentJobsList (i).tloPropertyList(5).sValue := TRIM(REPLACE(olCurrentJobsList (i).tloPropertyList(4).sValue, '%'));
          --IF olCurrentJobsList (i).sIcon like '%run' THEN
            olCurrentJobsList (i).tloPropertyList(5).TYPE := 'PROGRESSBAR';
          --END IF;
          isr$trace.debug ('olCurrentJobsList ('||i||')',  'olCurrentJobsList (i)', sCurrentName, xmltype(olCurrentJobsList (i)).getClobVal());
        end loop;
      end if;
    exception when others then
      null;
    end;

    isr$trace.stat('end', 'end', sCurrentName);
    return oErrorObj;
  exception
    when others then
      oErrorObj.handleError ( Stb$typedef.cnSeverityWarning, utd$msglib.getmsg ('exUnexpectedError', stb$security.getCurrentlanguage,  csP1 => sqlerrm(sqlcode)),
        sCurrentName, dbms_utility.format_error_stack ||chr(10)|| dbms_utility.format_error_backtrace );
      return oErrorObj;
  end getCurrentJobs;

 --*************************************************************************************************************************
  function getCurrentConnections (olCurrentConnectionsList in out STB$TREENODELIST)
    return STB$OERROR$RECORD is
    sCurrentName   constant varchar2(4000) := $$PLSQL_UNIT||'.getCurrentConnections';
    oErrorObj                     STB$OERROR$RECORD := STB$OERROR$RECORD();
    sSelect                       varchar2 (4000);
  begin
    isr$trace.stat('begin', 'begin', sCurrentName);
    sSelect :=
         'SELECT s.sessionid NODEID
               , s.sessionid NODENAME
               , ''F'' marked
               , s.username
               , (SELECT MIN (fullname)
                    FROM stb$user
                   WHERE UPPER (s.username) = UPPER (oracleusername)
                      OR UPPER (s.username) = UPPER (ldapusername))
                    FULLNAME
               , s.ip
               , s.port
               , (SELECT COUNT ( * )
                    FROM user_scheduler_jobs us, stb$jobqueue jq
                   WHERE UPPER(STB$SECURITY.getOracleUsername(jq.userno)) = UPPER(s.username)
                     AND TRIM(us.job_name) = TRIM(jq.oraclejobid)
                     AND us.state = ''RUNNING'')
                    runningJobs
               , (SELECT COUNT ( * )
                    FROM stb$jobqueue jq
                   WHERE UPPER(STB$SECURITY.getOracleUsername(jq.userno)) = UPPER(s.username)
                     AND TRIM (STB$JOB.getJobParameter (''JOB_ERROR'', jq.jobid)) IS NOT NULL)
                    failedJobs
               , s.markedforshutdown
               , s.inactive
               , s.securemode
               , CASE
                    WHEN s.sessionid = STB$SECURITY.getCurrentSession THEN ''T''
                    ELSE ''F''
                 END
                    ownSession
/*               , TO_CHAR (s.createdon
                        , STB$UTIL.getSystemParameter (''DATEFORMAT'') || '' HH24:MI:SS'') */
                 , s.createdon   START_TIME
             /*  , TO_CHAR (s.modifiedon
                        , STB$UTIL.getSystemParameter (''DATEFORMAT'') || '' HH24:MI:SS'')*/
                  , s.modifiedon  LAST_ACTIVE_TIME
            FROM isr$session s
           WHERE s.ip IS NOT NULL
          ORDER BY s.createdon DESC ';
    isr$trace.debug ('sSelect',  sSelect, sCurrentName);
    oErrorObj := Stb$util.getColListMultiRow (sSelect, olCurrentConnectionsList);
    isr$trace.stat('end', 'end', sCurrentName);
    return oErrorObj;
  exception
    when others then
      oErrorObj.handleError ( Stb$typedef.cnSeverityWarning, utd$msglib.getmsg ('exUnexpectedError', stb$security.getCurrentlanguage,  csP1 => sqlerrm(sqlcode)),
        sCurrentName, dbms_utility.format_error_stack ||chr(10)|| dbms_utility.format_error_backtrace );
      return oErrorObj;
  end getCurrentConnections;

--*********************************************************************************************************************************
function checkDependenciesOnMask( oParameter    in     STB$MENUENTRY$RECORD,
                                  olParameter   in out STB$PROPERTY$LIST,
                                  sModifiedFlag out    varchar2 )
return STB$OERROR$RECORD
is
  sCurrentName      constant varchar2(100)  := $$PLSQL_UNIT||'.checkDependenciesOnMask('||oParameter.sToken||')';
  oErrorObj         STB$OERROR$RECORD       := STB$OERROR$RECORD(null,null,Stb$typedef.cnNoError);
  olParameterOld    STB$PROPERTY$LIST       := STB$PROPERTY$LIST();

begin
  isr$trace.stat('begin','begin',sCurrentName);
  isr$trace.debug('value','oParameter IN',sCurrentName,oParameter);
  isr$trace.debug('value','olParameter IN OUT',sCurrentName,olParameter);

  olParameterOld := olParameter;

  case

    ------------ cun upload common doc ------------------------
    -------------------------------------------------------------
    when UPPER(oParameter.sToken) = 'CAN_UPLOAD_COMMON_DOC' then
      for i in 1..olParameter.COUNT() loop
        isr$trace.debug('olParameter('||i||').sDisplay',
        olParameter(i).sDisplay||';'||olParameter(i).svalue||';'||olParameter(i).sDisplayed,sCurrentName);
      end loop;


    else
      -- all other tokens
      null;
  end case;

  -- compare the objects if something changed only then refresh the mask
  select case when COUNT ( * ) = 0 then 'F' else 'T' end
      into sModifiedFlag
      from (select *
              from table (olParameter) p
            minus
            select *
              from table (olParameterOld) p);

  isr$trace.debug('value','olParameter',sCurrentName,olParameter);
  isr$trace.stat('end','sModifiedFlag: '||sModifiedFlag,sCurrentName);
  return oErrorObj;
exception
  when others then
      oErrorObj.handleError ( Stb$typedef.cnSeverityWarning, utd$msglib.getmsg ('exUnexpectedError', stb$security.getCurrentlanguage,  csP1 => sqlerrm(sqlcode)),
        sCurrentName, dbms_utility.format_error_stack ||chr(10)|| dbms_utility.format_error_backtrace );
    return oErrorObj;
end checkDependenciesOnMask;

 --**********************************************************************************************************************************
  function getAuditType  return STB$OERROR$RECORD is
    oErrorObj                     STB$OERROR$RECORD := STB$OERROR$RECORD();
    exAudit                       exception;
    sCurrentName      constant varchar2(100) := $$PLSQL_UNIT||'.getAuditType';
    sMsg       varchar2(2000);
  begin
    isr$trace.stat('begin', 'begin', sCurrentName);
    if Stb$util.checkPromptedAuditRequired ('PROMPTED_REP_TYPE_AUDIT') = 'T' and STB$OBJECT.getCurrentReporttypeId is not null
    or Stb$util.checkPromptedAuditRequired ('PROMPTED_SYSTEM_AUDIT') = 'T' then
      raise exAudit;
    -- commit will be in the audit window
    else
      -- Silent Audit
      -- write the silent audit and calculate the checksum
      oErrorObj := Stb$audit.silentAudit;
      -- commit the audit
      commit;
    end if;
    isr$trace.stat('end', 'end', sCurrentName);
    return oErrorObj;
  exception
    when exAudit then
      oErrorObj.handleError ( Stb$typedef.cnAudit, '', sCurrentName,
                             'Errorcode = '||Utd$msglib.GetMsg ('exAudit', Stb$security.GetCurrentLanguage)||
                             ', Errormessage = '||'Call the mask for the audit window');
                             -- call of DBMS_UTILITY.format_error_backtrace is not needed here . See comment in ISRC-1011
      isr$trace.stat('end', 'end', sCurrentName);
      return oErrorObj;
    when others then
       oErrorObj.handleError ( Stb$typedef.cnseverityWarning, '', sCurrentName,
                                     'Errorcode = '||SQLCODE||
                                     ', Errormessage = '||SQLERRM||
                                     ' '||DBMS_UTILITY.format_error_backtrace);
      return oErrorObj;
  end getAuditType;

--******************************************************************************
  function callFunction (oParameter in STB$MENUENTRY$RECORD, olNodeList out STB$TREENODELIST)
    return STB$OERROR$RECORD is
    sCurrentName         constant varchar2(100) := $$PLSQL_UNIT||'.callFunction('||oParameter.sToken||')';
    oErrorObj                     STB$OERROR$RECORD := STB$OERROR$RECORD();
    oCurNode                      STB$TREENODE$RECORD := STB$TREENODE$RECORD();
    exNoAudit                     exception;
    exNoEsig                      exception;
    exAudit                       exception;
    exNoReportsLocked             exception;
    exNoReportsCheckedOut         exception;
    exNoReportsDeactivated        exception;
    exNoReportsArchived           exception;
    exNoJobsExist                 exception;
    exNoTokenHandle               exception;
    exOwnSessionError             exception;
    exPortsNotAvailable           exception;
    exRenderingServerNotRunning   exception;
    exServerKillNotAllowed        exception;
    olParameter                   STB$PROPERTY$LIST;
    sAuditExists                  varchar2 (1) := 'F';
    sEsigExists                   varchar2 (1) := 'F';
    sExists                       varchar2(1);
    sToken                        varchar2(500);
    sNodeId                       varchar2(4000);
    nNewFileId                    number;
    sMsg                          varchar2(2000);
    olNodeListLocal               STB$TREENODELIST := olNodeListBackup;  -- vs 18.Mar.2016 workaround for CAN_REMOVE_JOBS%
    olNodeList1                   STB$TREENODELIST;

  begin
    isr$trace.stat('begin', 'oParameter.sToken: '||oParameter.sToken ,sCurrentName);
    isr$trace.debug('parameter', 'oParameter', sCurrentName, oParameter);
    olNodeList := STB$TREENODELIST();

    STB$OBJECT.setCurrentToken(oparameter.stoken);

    -- get the current node
    oCurNode := Stb$object.getCurrentNode;
    isr$trace.debug('value', 'oCurNode', sCurrentName, oCurnode);

    case

      -- ================== shows the mask of the jobs
    when UPPER (oParameter.sConfirm) = 'J' then

      sNewLoad := 'T';
      oErrorObj := getNodeList(
                    STB$TREENODE$RECORD('DUMMY', 'STB$SYSTEM', REPLACE(oParameter.sToken, 'CAN_DISPLAY_'), 'DUMMY'),
                    olNodeList
                   );

      update isr$tree$sessions
         set left_as_right = 'J'
       where nodetype = oCurNode.sNodetype
         and nodeid = oCurNode.sNodeid
         and selected = 'T'
         and sessionid = STB$SECURITY.getCurrentSession;

      commit;

      -- ================== delete log
    when UPPER (oParameter.sToken) = 'CAN_DELETE_LOG' then
      isr$util.truncateTable('isr$log');  -- includes commit within autonomous transaction

    -- ================== Dialogs
    when UPPER (oParameter.sToken) = 'CAN_MODIFY_WIZARD'
      or UPPER (oParameter.sToken) = 'CAN_UPLOAD_COMMON_DOC'
      or UPPER (oParameter.sToken) = 'CAN_RESTORE_REPORT'
      or UPPER (oParameter.sToken) = 'CAN_CONFIGURE_ORACLE_JOBS'
      or UPPER (oParameter.sToken) = 'CAN_MODIFY_GROUPING'
      or UPPER (oParameter.sToken) = 'CAN_MODIFY_OVERVIEW_REPORTS'
      or oparameter.sToken         = 'CAN_DISPLAY_TEMPLATE_REPORT'         -- [ISRC-463]
      or oParameter.sToken         = 'CAN_EXPORT_JOB_TRACE_LOG'
      or oParameter.sToken         = 'CAN_DISPLAY_PARAMETER_VALUES_REPORT' -- [ISRC-494]
      or UPPER (oParameter.sToken) = 'SHOW_JOB_MESSAGE'                    -- [ISRC-491]
      or oParameter.sToken         = 'CAN_EXPORT_SESSION_TRACE_LOG'
      or oParameter.sToken         = 'CAN_ZIP_LOGTABLE'                    -- [ISRC-383]
      or UPPER (oParameter.sToken) like 'CAN_DISPLAY_%AUDIT'
      or UPPER (oParameter.sToken) like 'CAN_DISPLAY_ESIG'
      or upper (oParameter.sToken) like 'CAN_DISPLAY_%AUDIT'
      or oParameter.sToken         like 'CAN_DISPLAY_ESIG_ALL'             -- [ISRC-457]
      or upper (oParameter.sToken) like 'CAN_DISPLAY_CONFIGURATION'        -- [ISRC-545] und [ARDIS-652]
      or oParameter.stoken         =    'CAN_DISPLAY_GROUP_CONFIGURATION'  -- [ISRC-581] und [ARDIS-667]
      or oParameter.stoken         =    'CAN_DISPLAY_ACTIVITY_LOG'         -- [ARDIS-669]
      or oParameter.stoken         =    'CAN_DISPLAY_TRANSLATION_REPORT'   -- [ARDIS-669]
      or oParameter.stoken         =    'CAN_DISPLAY_INVENTORY'            -- [ARDIS-669]  -- Works, even if no dialog exists. No error, no warning logged. Jobid = 17  (vs) 22.Jul2016  but this is not the final solution
      or oParameter.stoken         =    'CAN_DISPLAY_CUR_TEMPLATE_REP'     -- [ISRC-545]   -- no Dialog
      or oParameter.stoken         =    'CAN_DISPLAY_ARCHIVE_LOG'          -- [ISRC-545]
      or oParameter.stoken         =    'CAN_DISPLAY_TEMPLATE_DIFF'        -- [ISRC-578]
      or oParameter.stoken         =    'CAN_EXPORT_LOADER_DIR'
      or upper (oParameter.sToken) like 'CAN_DISPLAY_ESIG'    then

        sToken := UPPER (oParameter.sToken);
        if UPPER (oparameter.stoken) like 'CAN_DISPLAY_%AUDIT' then
          oErrorObj := stb$audit.checkauditexists (sauditexists, -1, REPLACE(sToken, 'CAN_DISPLAY_'));
          if sauditexists = 'F' then
            raise exNoAudit;
          end if;
        elsif UPPER (oparameter.stoken) like 'CAN_DISPLAY_ESIG' then
          oErrorObj := stb$security.checkesigexists (stb$object.getcurrentsrnumber, stb$object.getcurrenttitle, sesigexists);
          if sesigexists = 'F' then
            raise exNoEsig;
          end if;
        end if;

        olNodeList.EXTEND;
        olNodeList (1) := STB$TREENODE$RECORD(STB$UTIL.getMaskTitle(UPPER (oParameter.sToken)), 'STB$SYSTEM', 'TRANSPORT', 'MASK', '');
        isr$trace.debug('value','olNodeList',sCurrentName,olNodeList);

     /*   if UPPER (oParameter.sToken) in ('CAN_MODIFY_REPTYPE_PARAMETER_VALUES'
                                        ,'CAN_MODIFY_REPTYPE_CALC_VALUES','CAN_MODIFY_REPTYPE_STYLE_VALUES','CAN_DISPLAY_CONFIGURATION'
                                        ,'CAN_MODIFY_WIZARD','CAN_MODIFY_GROUPING','CAN_MODIFY_OVERVIEW_REPORTS') then
          oErrorObj := checkDependenciesOnMaskForClob(oParameter, olNodeList (1).clPropertyList, sExists);

        else*/
          oErrorObj := STB$UTIL.getPropertyList(sToken, olNodeList (1).tloPropertylist);
          oErrorObj := checkDependenciesOnMask(oParameter, olNodeList (1).tloPropertylist, sExists);
       -- end if;

      -- ================== show the mask of system parameters
    when UPPER (oParameter.sToken) = 'CAN_MODIFY_SYSTEM_PARAMETERS' then
        --Intialization for the parameter 'CAN_MODIFY_SYSTEM_PARAMETERS'
        olNodeList.EXTEND;
        olNodeList (1) := STB$TREENODE$RECORD(STB$UTIL.getMaskTitle(UPPER (oParameter.sToken)),
                                              'STB$SYSTEM', 'TRANSPORT', 'MASK', '');
        -- write the properties
        oErrorObj := Stb$system.getSystemparameters (olParameter);
        -- write the property list in the node object
        olNodeList (1).tloPropertylist := olParameter;

      -- ================== create audit on system parameters
    when UPPER (oParameter.sToken) = 'CAN_SWITCH_RIGHT' then

        oErrorObj := STB$SYSTEM.SwitchCurrentGroupRight(UPPER (oParameter.sToken));

        ISR$TREE$PACKAGE.selectCurrent;
        olNodeList := null; --ISRC-768

        if STB$UTIL.checkPromptedAuditRequired ('PROMPTED_SWITCH_GROUP_RIGHT') = 'T' then
          raise exAudit;
        else
          oErrorObj := Stb$audit.Silentaudit;
          commit;
        end if;

      -- ================== unlock the reports of one user
    when UPPER (oParameter.sToken) = 'CAN_UNLOCK_USER_REPORTS' then
        sUnlockAsDBA := 'T';
        oErrorObj := unLockReports ('and lockedby in (select userno from STB$USER where fullname='''||REPLACE(STB$UTIL.getProperty(oCurNode.tloPropertyList, 'FULLNAME'), '''', '''''')||''')');
        sUnlockAsDBA := 'F';
        sNewLoad := 'T';
        ISR$TREE$PACKAGE.selectCurrent;

      -- ================== unlock users reports
    when UPPER (oParameter.sToken) = 'CAN_UNLOCK_USER_SESSION' then
        sUnlockAsDBA := 'T';
        oErrorObj := unLockReports ('and locksession = ' || STB$UTIL.getProperty(oCurNode.tloPropertyList, 'LOCKSESSION'));
        sUnlockAsDBA := 'F';
        sNewLoad := 'T';
        ISR$TREE$PACKAGE.selectCurrent;

      -- ================== unlock one title
    when UPPER (oParameter.sToken) = 'CAN_UNLOCK_TITLE' then
        sUnlockAsDBA := 'T';
        oErrorObj := unLockReports ('and title= ''' || REPLACE(oCurNode.sDisplay, '''', '''''') || '''');
        sUnlockAsDBA := 'F';
        sNewLoad := 'T';
        ISR$TREE$PACKAGE.selectCurrent;

      -- ================== unlock one srnumber
    when UPPER (oParameter.sToken) = 'CAN_UNLOCK_SRNUMBER' then
        sUnlockAsDBA := 'T';
        oErrorObj := unLockReports ('and srnumber= ''' || REPLACE(STB$UTIL.getProperty(oCurNode.tloPropertyList, 'SRNUMBER'), '''', '''''') || '''');
        sUnlockAsDBA := 'F';
        sNewLoad := 'T';
        ISR$TREE$PACKAGE.selectCurrent;

      -- ================== select
    when UPPER (oParameter.sToken) = 'CAN_SELECT' then

        if olNodeListBackup is not null then
         isr$trace.debug('olNodeListBackup.COUNT()' , '' || olNodeListBackup.COUNT(), sCurrentName);
          for i in 1..olNodeListBackup.COUNT() loop
            if STB$OBJECT.getCurrentNodeId = olNodeListBackup(i).tloValueList(olNodeListBackup(i).tloValueList.COUNT()).sNodeID then
              for j in 1..olNodeListBackup(i).tloPropertyList.COUNT() loop
                if olNodeListBackup(i).tloPropertyList (j).sParameter = 'MARKED' then
                  isr$trace.debug ('switch',  'switch', sCurrentName);
                  if olNodeListBackup(i).tloPropertyList (j).sValue = 'T' then
                    isr$trace.debug ('switch', 'to false', sCurrentName);
                    olNodeListBackup(i).tloPropertyList (j).sValue := 'F';
                  else
                    isr$trace.debug ('switch',  'to true', sCurrentName);
                    olNodeListBackup(i).tloPropertyList (j).sValue := 'T';
                  end if;
                  olNodeListBackup (i).tloPropertyList(j).sDisplay := olNodeListBackup (i).tloPropertyList(j).sValue;
                  exit;
                end if;
              end loop;
              isr$trace.debug ('olNodeListBackup('||i||')', 'node.xml in logclob', sCurrentName, xmltype(olNodeListBackup(i)).getClobVal());
            end if;
          end loop;
          sNewLoad := 'F';
          sCurrentNodetype := STB$OBJECT.getCurrentNodeType;
          ISR$TREE$PACKAGE.selectCurrent;
          isr$trace.debug('status','oErrorObj',sCurrentName,oErrorObj);
          olNodeList := null; -- ISRC-612 for CAN_SELECT return null in nodelist
        else
          isr$trace.error('olNodeListBackup in CAN_SELECT is null', sCurrentName, 'check it', sCurrentName);
        end if;
        isr$trace.debug('end CAN_SELECT' , '' , sCurrentName);

      -- ================== remove a connection
      when UPPER (oParameter.sToken) like 'CAN_REMOVE_CONNECTION%' then

        if STB$OBJECT.getCurrentNodeId = STB$SECURITY.getCurrentSession and UPPER (oParameter.sToken) = 'CAN_REMOVE_CONNECTION' then
          raise exOwnSessionError;
        end if;

        for i in 1..olNodeListBackup.COUNT() loop
          sNodeId := olNodeListBackup(i).tloValueList(olNodeListBackup(i).tloValueList.COUNT()).sNodeId;
          if sNodeId = STB$OBJECT.getCurrentNodeId and UPPER (oParameter.sToken) = 'CAN_REMOVE_CONNECTION'
          or olNodeListBackup(i).tloPropertyList (1).sValue = 'T' and UPPER (oParameter.sToken) = 'CAN_REMOVE_CONNECTIONS_SELECTED'
          or UPPER (oParameter.sToken) = 'CAN_REMOVE_CONNECTIONS_ALL' then
            oErrorObj := removeConnection(TO_NUMBER(sNodeId));
          end if;
        end loop;

        sNewLoad := 'T';
        ISR$TREE$PACKAGE.selectCurrent;

    -- ================== remove a job
    when UPPER (oParameter.sToken) like 'CAN_REMOVE_JOB%' then
        isr$trace.debug('value','olNodeListLocal',sCurrentName,olNodeListLocal);
        if olNodeListLocal.exists(1) then
          for i in 1..olNodeListLocal.count() loop
            -- unfortunately olNodeListBackup changes within this loop, therefore
            --isr$trace.debug('vs i='||i,'olNodeListBackup',sCurrentName,olNodeListBackup);
            if olNodeListLocal(i).tloValueList is not null then
              -- sNodeID = JOBID
              sNodeId := olNodeListLocal(i).tloValueList(olNodeListLocal(i).tloValueList.COUNT()).sNodeId;
              isr$trace.debug('olNodeListLocal('||i||')','sNodeId: '||sNodeId,sCurrentName);
              if     sNodeId = STB$OBJECT.getCurrentNodeId  and  upper(oParameter.sToken) = 'CAN_REMOVE_JOB'
                  or olNodeListLocal(i).tloPropertyList(2).sValue = 'T' and upper(oParameter.sToken) = 'CAN_REMOVE_JOBS_SELECTED'
                  or upper(oParameter.sToken) = 'CAN_REMOVE_JOBS_ALL'
              then
                oErrorObj := removeJob(TO_NUMBER(sNodeId));
              end if;
            end if;
          end loop;
        end if;

        sNewLoad := 'T';
        ISR$TREE$PACKAGE.selectCurrent;

      -- ================== reactivate a report
    when UPPER (oParameter.sToken) like 'CAN_REACTIVATE_%' then

        for i in 1..olNodeListBackup.COUNT() loop
          sNodeId := olNodeListBackup(i).tloValueList(olNodeListBackup(i).tloValueList.COUNT()).sNodeId;
          isr$trace.debug('sNodeId for '||UPPER (oParameter.sToken),  sNodeId, sCurrentName);
          if sNodeId = STB$OBJECT.getCurrentNodeId and UPPER (oParameter.sToken) = 'CAN_REACTIVATE_REPORT'
          or olNodeListBackup(i).tloPropertyList (1).sValue = 'T' and UPPER (oParameter.sToken) = 'CAN_REACTIVATE_SELECTED'
          or UPPER (oParameter.sToken) = 'CAN_REACTIVATE_ALL' then
            oErrorObj := reactivateReports ('where repid = ' || sNodeId);
            /* ISRC-774
            TODO: check for errors*/
            oErrorObj := isr$reporttype$package.updateChecksum(sNodeId);
          end if;
        end loop;

        sNewLoad := 'T';
        ISR$TREE$PACKAGE.selectCurrent;

      -- ================== undo checkout a report
    when UPPER (oParameter.sToken) like 'CAN_UNDO_CHECKOUT_%' then

        for i in 1..olNodeListBackup.COUNT() loop
          sNodeId := olNodeListBackup(i).tloValueList(olNodeListBackup(i).tloValueList.COUNT()).sNodeId;
          isr$trace.debug('sNodeId for '||UPPER (oParameter.sToken), sNodeId, sCurrentName);
          if sNodeId = STB$OBJECT.getCurrentNodeId and UPPER (oParameter.sToken) = 'CAN_UNDO_CHECKOUT_REPORT'
          or olNodeListBackup(i).tloPropertyList (1).sValue = 'T' and UPPER (oParameter.sToken) = 'CAN_UNDO_CHECKOUT_SELECTED'
          or UPPER (oParameter.sToken) = 'CAN_UNDO_CHECKOUT_ALL' then
            oErrorObj := undoCheckOutReports ('where docid = ' || sNodeId);
          end if;
        end loop;

        sNewLoad := 'T';
        ISR$TREE$PACKAGE.selectCurrent;

      -- ================== checkin a report
    when UPPER (oParameter.sToken) like 'CAN_CHECKIN_%' then

        for i in 1..olNodeListBackup.COUNT() loop
          sNodeId := olNodeListBackup(i).tloValueList(olNodeListBackup(i).tloValueList.COUNT()).sNodeId;
          isr$trace.debug('sNodeId for '||UPPER (oParameter.sToken), sNodeId, sCurrentName);
          if sNodeId = STB$OBJECT.getCurrentNodeId and UPPER (oParameter.sToken) = 'CAN_CHECKIN_REPORT'
          or olNodeListBackup(i).tloPropertyList (1).sValue = 'T' and UPPER (oParameter.sToken) = 'CAN_CHECKIN_SELECTED'
          or UPPER (oParameter.sToken) = 'CAN_CHECKIN_ALL' then
            oErrorObj := checkInReports ('where docid = ' || sNodeId);
          end if;
        end loop;

        sNewLoad := 'T';
        ISR$TREE$PACKAGE.selectCurrent;

      -- ================== unlock a report
    when UPPER (oParameter.sToken) like 'CAN_UNLOCK_%' then

        for i in 1..olNodeListBackup.COUNT() loop
          sNodeId := olNodeListBackup(i).tloValueList(olNodeListBackup(i).tloValueList.COUNT()).sNodeId;
          isr$trace.debug('sNodeId for '||UPPER (oParameter.sToken),  sNodeId, sCurrentName);
          if sNodeId = STB$OBJECT.getCurrentNodeId and UPPER (oParameter.sToken) = 'CAN_UNLOCK_REP'
          or olNodeListBackup(i).tloPropertyList (1).sValue = 'T' and UPPER (oParameter.sToken) = 'CAN_UNLOCK_SELECTED'
          or UPPER (oParameter.sToken) = 'CAN_UNLOCK_ALL' then
            sUnlockAsDBA := 'T';
            oErrorObj := unLockReports ('and repid = ' || sNodeId);
            sUnlockAsDBA := 'F';
          end if;
        end loop;

        sNewLoad := 'T';
        ISR$TREE$PACKAGE.selectCurrent;

    else
        raise exNoTokenHandle;
    end case;

    if olNodeList is not null then -- ISRC-612 for CAN_SELECT return null in nodelist
      isr$trace.debug('value', 'olNodeList OUT olNodeList.count: '||olNodeList.COUNT, sCurrentName, olNodeList);
    end if;
    isr$trace.stat('end', 'end', sCurrentName);
    return oErrorObj;
  exception
    when exServerKillNotAllowed then
      sMsg := utd$msglib.getmsg ('exServerKillNotAllowedText', stb$security.getcurrentlanguage, SQLERRM);
      oErrorObj.handleError ( Stb$typedef.cnSeverityMessage, sMsg, sCurrentName,
                            DBMS_UTILITY.format_error_stack || CHR(10) || DBMS_UTILITY.format_error_backtrace, SQLCODE);
      return oErrorObj;
    when exPortsNotAvailable then
      sMsg := utd$msglib.getmsg ('exPortsNotAvailableText', stb$security.getcurrentlanguage, SQLERRM);
      oErrorObj.handleError ( Stb$typedef.cnSeverityMessage, sMsg, sCurrentName,
                            DBMS_UTILITY.format_error_stack || CHR(10) || DBMS_UTILITY.format_error_backtrace, SQLCODE);
      return oErrorObj;
    when exRenderingServerNotRunning then
      sMsg := utd$msglib.getmsg ('exRenderingServerNotRunningText', stb$security.getcurrentlanguage, SQLERRM);
      oErrorObj.handleError ( Stb$typedef.cnSeverityMessage, sMsg, sCurrentName,
                            DBMS_UTILITY.format_error_stack || CHR(10) || DBMS_UTILITY.format_error_backtrace, SQLCODE);
      return oErrorObj;
    when exOwnSessionError then
      sMsg := Utd$msglib.GetMsg ('exOwnSessionErrorText', Stb$security.GetCurrentLanguage, SQLERRM);
      oErrorObj.handleError ( Stb$typedef.cnSeverityMessage, sMsg, sCurrentName,
                            DBMS_UTILITY.format_error_stack || CHR(10) || DBMS_UTILITY.format_error_backtrace, SQLCODE);
      return oErrorObj;
    when exNoReportsLocked then
      sMsg := Utd$msglib.GetMsg ('exNoReportsLockedText', Stb$security.GetCurrentLanguage, SQLERRM);
      oErrorObj.handleError ( Stb$typedef.cnSeverityMessage, sMsg, sCurrentName,
                            DBMS_UTILITY.format_error_stack || CHR(10) || DBMS_UTILITY.format_error_backtrace, SQLCODE);
      return oErrorObj;
    when exNoReportsCheckedOut then
      smsg := Utd$msglib.GetMsg ('exNoReportsCheckedOutText', Stb$security.GetCurrentLanguage, SQLERRM);
      oErrorObj.handleError ( Stb$typedef.cnSeverityMessage, sMsg, sCurrentName,
                            DBMS_UTILITY.format_error_stack || CHR(10) || DBMS_UTILITY.format_error_backtrace, SQLCODE);
      return oErrorObj;
    when exNoReportsDeactivated then
      sMsg := Utd$msglib.GetMsg ('exNoReportsDeactivatedText', Stb$security.GetCurrentLanguage, SQLERRM);
      oErrorObj.handleError ( Stb$typedef.cnSeverityMessage, sMsg, sCurrentName,
                            DBMS_UTILITY.format_error_stack || CHR(10) || DBMS_UTILITY.format_error_backtrace, SQLCODE);
      return oErrorObj;
    when exNoReportsArchived then
      sMsg := Utd$msglib.GetMsg ('exNoReportsArchivedText', Stb$security.GetCurrentLanguage, SQLERRM);
      oErrorObj.handleError ( Stb$typedef.cnSeverityMessage, sMsg, sCurrentName,
                            DBMS_UTILITY.format_error_stack || CHR(10) || DBMS_UTILITY.format_error_backtrace, SQLCODE);
      return oErrorObj;
    when exNoJobsExist then
      sMsg := Utd$msglib.GetMsg ('exNo'||REPLACE(INITCAP(REPLACE(UPPER (oParameter.sToken), 'CAN_DISPLAY_')),'_')||'ExistText', Stb$security.GetCurrentLanguage);
      oErrorObj.handleError ( Stb$typedef.cnSeverityMessage, sMsg, sCurrentName,
                            DBMS_UTILITY.format_error_stack || CHR(10) || DBMS_UTILITY.format_error_backtrace, SQLCODE);
      return oErrorObj;
    when exAudit then
      sMsg := 'Call the mask for the audit window';
      oErrorObj.handleError ( Stb$typedef.cnAudit, sMsg, sCurrentName,
                            DBMS_UTILITY.format_error_stack || CHR(10) || DBMS_UTILITY.format_error_backtrace, SQLCODE);
      oErrorObj.sErrorCode := 'exAudit';
      return oErrorObj;
    when exNoAudit then
      sMsg := Utd$msglib.GetMsg ('exNoAuditText', Stb$security.GetCurrentLanguage, SQLERRM);
      oErrorObj.handleError ( Stb$typedef.cnSeverityMessage, sMsg, sCurrentName,
                            DBMS_UTILITY.format_error_stack || CHR(10) || DBMS_UTILITY.format_error_backtrace, SQLCODE);
      return oErrorObj;
    when exNoEsig then
      sMsg := Utd$msglib.GetMsg ('exNoEsigText', Stb$security.GetCurrentLanguage, SQLERRM);
      oErrorObj.handleError ( Stb$typedef.cnSeverityMessage, sMsg, sCurrentName,
                            DBMS_UTILITY.format_error_stack || CHR(10) || DBMS_UTILITY.format_error_backtrace, SQLCODE);
      return oErrorObj;
    when exNoTokenHandle then
      sMsg := utd$msglib.getmsg ('exNoTokenHandle', stb$security.getcurrentlanguage, oParameter.sToken);
      oErrorObj.handleError ( Stb$typedef.cnSeverityWarning, sMsg, sCurrentName, 'user defined exception: exNoTokenHandle'||stb$typedef.crlf||DBMS_UTILITY.format_error_stack ||stb$typedef.crlf|| DBMS_UTILITY.format_error_backtrace );
      rollback;
      return oErrorObj;
    when others then
     sMsg :=  utd$msglib.getmsg ('ERROR_PLACE', stb$security.getcurrentlanguage, SQLERRM(SQLCODE));
      oErrorObj.handleError ( Stb$typedef.cnSeverityWarning, sMsg, sCurrentName,  DBMS_UTILITY.format_error_stack ||stb$typedef.crlf|| DBMS_UTILITY.format_error_backtrace  );
      rollback;
      return oErrorObj;
  end callFunction;


  --**********************************************************************************************************************
  function putParameters( oParameter  in STB$MENUENTRY$RECORD,
                          olParameter in STB$PROPERTY$LIST,
                          clParameter in clob default null)
    return STB$OERROR$RECORD is
    sCurrentName        constant varchar2(100) := $$PLSQL_UNIT||'.putParameters('||oParameter.sToken||')';
    oErrorObj           STB$OERROR$RECORD := STB$OERROR$RECORD();
    exNoTokenHandle     exception;
    exJobSumError       exception;
    exPortsNotAvailable exception;

    sUserName         varchar2 (4000);
    sStartDate        varchar2 (4000);
    sEndDate          varchar2 (4000);
    sWhere            varchar2 (4000);
    sMsg              varchar2(4000);

    xXml              xmltype;
    sAction           varchar2(500);
    sRowId            varchar2(500);
    sStmt             varchar2(4000);

    sTable            varchar2(32);
    sParameter        varchar2(500);
    sKey1             ISR$DIALOG.PARAMETERNAME%type;
    sKey2             ISR$DIALOG.PARAMETERNAME%type;
    sKeyValue1        ISR$DIALOG.PARAMETERVALUE%type;
    sKeyValue2        ISR$DIALOG.PARAMETERVALUE%type;
    sKeys             varchar2(4000);
    sKeyValues        varchar2(4000);
    sAuditKeys        varchar2(4000);

    sOldValue         varchar2(4000);
    sNewValue         varchar2(4000);

    sOldValueString   varchar2(4000);
    sNewValueString   varchar2(4000);

    sEditable         varchar2(1);

    nCount            number;
    cursor cGetParameterInfo(csToken in varchar2, csParameter in varchar2, csValue in varchar2) is
      select utd$msglib.getmsg (description, stb$security.getcurrentlanguage)
           , case
                when haslov in ('T', 'W') then
                   NVL(ISR$REPORTTYPE$PACKAGE.getSelectedLovDisplay (parametername, csValue, 'STB$SYSTEM'), csValue)
                else
                   csValue
             end
           , editable
        from isr$dialog
       where token = csToken
         and parametername = csParameter;

    nJobSum   number;

    cursor cGetUserCols(csParameter in varchar2, csTable in varchar2) is
      select 'T'
        from user_tab_cols
       where column_name = UPPER(csParameter)
         and table_name = UPPER(csTable);


  begin
    isr$trace.stat('begin','begin',sCurrentName);
    isr$trace.debug('value','oParameter',sCurrentName,oParameter);
    isr$trace.debug('value','olParameter.count: '||olParameter.COUNT,sCurrentName,olParameter);
    isr$trace.debug('value','length(clParameter): '||LENGTH(clParameter),sCurrentName,clParameter);

    STB$OBJECT.setCurrentToken(oparameter.stoken);

    case

      -- ================== oracle job start
    when UPPER (oParameter.sToken) = 'CAN_CONFIGURE_ORACLE_JOBS' then

        STB$JOB.nJobId := STB$UTIL.getProperty(olParameter, 'ISRJOBID');

        oErrorObj := Stb$util.createFilterParameter (olParameter);
        
        oErrorObj := STB$JOB.modifyJob(STB$JOB.nJobId);
        
        delete from isr$tree$nodes where nodetype = 'TITLE' and parametername = STB$JOB.getJobParameter('TITLE', STB$JOB.nJobId);
        commit;
        ISR$TREE$PACKAGE.sendSyncCall('TITLE', STB$JOB.getJobParameter('TITLE', STB$JOB.nJobId)); 

    -- ================== modify grid
    when UPPER (oParameter.sToken) = 'CAN_MODIFY_REPTYPE_PARAMETER_VALUES'
    or UPPER (oParameter.sToken) = 'CAN_MODIFY_REPTYPE_CALC_VALUES'
    or UPPER (oParameter.sToken) = 'CAN_MODIFY_REPTYPE_STYLE_VALUES'
    or UPPER (oParameter.sToken) = 'CAN_MODIFY_WIZARD'
    or UPPER (oParameter.sToken) = 'CAN_MODIFY_GROUPING'
    or UPPER (oParameter.sToken) = 'CAN_MODIFY_OVERVIEW_REPORTS' then

      xXml := ISR$XML.clobToXML(clParameter);

      if ISR$XML.valueOf(xXml, 'count(//ROW)') > 0 then

        case
          when UPPER(oParameter.sToken) = 'CAN_MODIFY_REPTYPE_PARAMETER_VALUES' then
            sTable := 'ISR$FILE$PARAMETER$VALUES';
          when UPPER(oParameter.sToken) = 'CAN_MODIFY_REPTYPE_CALC_VALUES' then
            sTable := 'ISR$FILE$CALC$VALUES';
          when UPPER(oParameter.sToken) = 'CAN_MODIFY_REPTYPE_STYLE_VALUES' then
            sTable := 'ISR$FILE$STYLE$VALUES';
          when UPPER(oParameter.sToken) = 'CAN_MODIFY_WIZARD' then
            sTable := 'ISR$REPORT$WIZARD';
          when UPPER(oParameter.sToken) = 'CAN_MODIFY_GROUPING' then
            sTable := 'ISR$GROUPING$TAB';
          when UPPER(oParameter.sToken) = 'CAN_MODIFY_OVERVIEW_REPORTS' then
            sTable := 'ISR$OVERVIEW$REPORTS';
          else
            null;
        end case;
        isr$trace.debug ('sTable', sTable, sCurrentName, clParameter);

        if UPPER(oParameter.sToken) in ('CAN_MODIFY_OVERVIEW_REPORTS') then
          oErrorObj := Stb$audit.openAudit ('SYSTEMAUDIT', TO_CHAR (-1));
        elsif UPPER(oParameter.sToken) in ('CAN_MODIFY_REPTYPE_PARAMETER_VALUES', 'CAN_MODIFY_REPTYPE_CALC_VALUES', 'CAN_MODIFY_REPTYPE_STYLE_VALUES') then
          oErrorObj := Stb$audit.openAudit ('TEMPLATEAUDIT', STB$OBJECT.getCurrentNode().sDisplay);
        else
          oErrorObj := Stb$audit.openAudit ('REPORTTYPEAUDIT', TO_CHAR(STB$OBJECT.getCurrentReporttypeid));
        end if;
        oErrorObj := Stb$audit.AddAuditRecord (oParameter.sToken);

        for i in 1..ISR$XML.valueOf(xXml, 'count(//ROW)') loop
          sOldValueString := null;
          sNewValueString := null;

          sAction := ISR$XML.valueOf(xXml, '(//ROW)['||i||']/../name()');
          sRowId := ISR$XML.valueOf(xXml, '(//ROW)['||i||']/ROWID/text()');
          if TRIM(sRowId) is null then
            sAction := 'INSERT';
          end if;

          isr$trace.debug ('sAction',  sAction, sCurrentName);
          isr$trace.debug ('sRowId',  sRowId, sCurrentName);

          nCount := 0;
          sKeys := '';
          sKeyValues := '';
          sAuditKeys := '';
          for rGetPks in cPrimaryKeys(sTable) loop
            nCount := nCount + 1;
            sKeys := sKeys||rGetPks.column_name||', ';
            sKeyValues := sKeyValues||''''||REPLACE(ISR$XML.valueOf(xXml, '(//ROW)['||i||']/'||rGetPks.column_name||'/text()'), '''', '''''')||''', ';
            sAuditKeys := sAuditKeys||rGetPks.column_name||': '||REPLACE(ISR$XML.valueOf(xXml, '(//ROW)['||i||']/'||rGetPks.column_name||'/text()'), '''', '''''')||CHR(10);
            if nCount = 1 then
              sKey1 := rGetPks.column_name;
              isr$trace.debug ('sKey1',  sKey1, sCurrentName);
              sKeyValue1 := ISR$XML.valueOf(xXml, '(//ROW)['||i||']/'||sKey1||'/text()');
              isr$trace.debug ('sKeyValue1',  sKeyValue1, sCurrentName);
            else
              sKey2 := rGetPks.column_name;
              isr$trace.debug ('sKey2',  sKey2, sCurrentName);
              sKeyValue2 := ISR$XML.valueOf(xXml, '(//ROW)['||i||']/'||sKey2||'/text()');
              isr$trace.debug ('sKeyValue2',  sKeyValue2, sCurrentName);
            end if;
          end loop;
          sKeys := RTRIM(sKeys, ', ');
          sKeyValues := RTRIM(sKeyValues, ', ');
          sAuditKeys := RTRIM(sAuditKeys, CHR(10));
          isr$trace.debug ('sKeys',  sKeys, sCurrentName);
          isr$trace.debug ('sKeyValues',  sKeyValues, sCurrentName);
          isr$trace.debug ('sAuditKeys',  sAuditKeys, sCurrentName);

          if UPPER(oParameter.sToken) = 'CAN_MODIFY_GROUPING' then
            if ISR$XML.valueOf(xXml, '(//ROW)['||i||']/SELECTED/text()') = 'T' then
              sAction := 'INSERT';
            else
              sAction := 'DELETE';
            end if;
            --sStmt := 'SELECT ROWID FROM '||sTable||' WHERE '||sKey1||' = '''||REPLACE(sKeyValue1, '''', '''''')||''''|| case when TRIM(sKey2) is not null then ' and '||sKey2||' = '''||REPLACE(sKeyValue2, '''', '''''')||'''' end ;
            sStmt := 'SELECT ROWID FROM '||sTable||' WHERE ('||sKeys||') IN ('||sKeyValues||')';
            isr$trace.debug ('sStmt',  sStmt, sCurrentName);
            begin
              execute immediate sStmt into sRowid;
              if sAction = 'INSERT' and sRowid is not null then
                sAction := 'DO_NOTHING';
              end if;
            exception when others then
              if sAction = 'DELETE' then
                sAction := 'DO_NOTHING';
              end if;
            end;
            if sAction = 'DELETE' then
              oErrorObj := ISR$CUSTOMER.deleteAllSubnodes (sKeyValue1);
            end if;
          end if;

          if sAction = 'INSERT' then
            sStmt := 'INSERT INTO '||sTable||' '||
                     '   ('||sKeys||') '||
                     'VALUES '||
                     --'  ('''||REPLACE(sKeyValue1, '''', '''''')||''''|| case when TRIM(sKey2) is not null then ', '''||REPLACE(sKeyValue2, '''', '''''')||'''' end ||')';
                     '  ('||sKeyValues||')';
            isr$trace.debug ('sStmt',  sStmt, sCurrentName);
            execute immediate sStmt;
            sStmt := 'SELECT ROWID FROM '||sTable||' WHERE '||sKey1||' = '''||REPLACE(sKeyValue1, '''', '''''')||''''|| case when TRIM(sKey2) is not null then ' and '||sKey2||' = '''||REPLACE(sKeyValue2, '''', '''''')||'''' end;
            isr$trace.debug ('sStmt',  sStmt, sCurrentName);
            execute immediate sStmt into sRowid;
          end if;

          if UPPER(oParameter.sToken) != 'CAN_MODIFY_GROUPING' then
            for j in 1..ISR$XML.valueOf(xXml, 'count((//ROW)['||i||']/*[name()!= "ROWID"])') loop
              sParameter := ISR$XML.valueOf(xXml, '((//ROW)['||i||']/*[name()!= "ROWID"])['||j||']/name()');

              sOldValue := '';
              sNewValue := '';
              sEditable := 'F';
              open cGetParameterInfo(UPPER(oParameter.sToken), sParameter, sNewValue);
              fetch cGetParameterInfo into sNewValue, sOldValue, sEditable;
              close cGetParameterInfo;
              sOldValue := '';
              sNewValue := '';

              sNewValue := ISR$XML.valueOf(xXml, '((//ROW)['||i||']/*[name()!= "ROWID"])['||j||']/text()');

              if sEditable = 'T' then

                sOldValue := null;
                if sAction in ('UPDATE', 'DELETE') then
                  if UPPER (oParameter.sToken) = 'CAN_MODIFY_WIZARD' and sParameter in ('KEY', 'DISPLAY', 'INFO') then
                    sStmt := 'SELECT '||sParameter||' FROM ISR$META$CRIT WHERE CRITENTITY = (select entity from isr$report$wizard where rowid = ''' || sRowId || ''') ';
                  else
                    sStmt := 'SELECT '||sParameter||' FROM '||sTable||' WHERE ROWID = '''||sRowId||'''';
                  end if;
                  isr$trace.debug ('sStmt',  sStmt, sCurrentName);
                  execute immediate sStmt into sOldValue;
                end if;

                if sAction in ('INSERT', 'UPDATE') then
                  if UPPER (oParameter.sToken) = 'CAN_MODIFY_WIZARD' and sParameter in ('TITLE','DESCRIPTION','TOOLTIP','HEADLINE1','HEADLINE2','HEADLINE3') then
                    if NVL(UTD$MSGLIB.getMsg(sOldValue, STB$SECURITY.getCurrentLanguage), '#@#') != NVL(sNewValue, '#@#') then
                      sStmt := 'UPDATE UTD$MSG SET MSGTEXT = '''||REPLACE(sNewValue, '''', '''''')||''' WHERE msgkey = '''||sOldValue||''' AND lang = '||STB$SECURITY.getCurrentLanguage||' ';
                      isr$trace.debug ('sStmt',  sStmt, sCurrentName);
                      execute immediate sStmt;
                    end if;
                    sOldValue := UTD$MSGLIB.getMsg(sOldValue, STB$SECURITY.getCurrentLanguage);
                  elsif UPPER (oParameter.sToken) = 'CAN_MODIFY_WIZARD' and sParameter in ('KEY', 'DISPLAY', 'INFO') then
                    if NVL(sOldValue, '#@#') != NVL(sNewValue, '#@#') then
                      sStmt := 'UPDATE ISR$META$CRIT SET '||sParameter||' = '''||REPLACE(sNewValue, '''', '''''')||''' WHERE CRITENTITY = (select entity from isr$report$wizard where rowid = ''' || sRowId || ''') ';
                      isr$trace.debug ('sStmt',  sStmt, sCurrentName);
                      execute immediate sStmt;
                    end if;
                  else
                    if NVL(sOldValue, '#@#') != NVL(sNewValue, '#@#') then
                      sStmt := 'UPDATE '||sTable||' SET '||sParameter||' = '''||REPLACE(sNewValue, '''', '''''')||''' WHERE ROWID = ''' || sRowId || ''' ';
                      isr$trace.debug ('sStmt',  sStmt, sCurrentName);
                      execute immediate sStmt;
                    end if;
                  end if;
                end if;

                if (sAction in ('DELETE')
                or (sAction in ('UPDATE') and NVL(sOldValue, '#@#') != NVL(sNewValue, '#@#') )) then
                  open cGetParameterInfo(UPPER(oParameter.sToken), sParameter, sOldValue);
                  fetch cGetParameterInfo into sParameter, sOldValue, sEditable;
                  close cGetParameterInfo;
                  sOldValueString := SUBSTR(sOldValueString||sParameter||':'||sOldValue||CHR(10), 0, 4000);
                end if;
                if (sAction in ('INSERT')
                or (sAction in ('UPDATE') and NVL(sOldValue, '#@#') != NVL(sNewValue, '#@#') )) then
                  open cGetParameterInfo(UPPER(oParameter.sToken), sParameter, sNewValue);
                  fetch cGetParameterInfo into sParameter, sNewValue, sEditable;
                  close cGetParameterInfo;
                  sNewValueString := SUBSTR(sNewValueString||sParameter||':'||sNewValue||CHR(10), 0, 4000);
                end if;

              end if;

            end loop;
          end if;

          if sAction in ('DELETE') then
            sStmt := 'DELETE FROM '||sTable||' WHERE ROWID = ''' || sRowId || ''' ';
            isr$trace.debug ('sStmt',  sStmt, sCurrentName);
            execute immediate sStmt;
          end if;

          open cGetParameterInfo(UPPER(oParameter.sToken), sKey1, sKeyValue1);
          fetch cGetParameterInfo into sKey1, sKeyValue1, sEditable;
          close cGetParameterInfo;

          if sKeyValue2 is not null then
            open cGetParameterInfo(UPPER(oParameter.sToken), sKey2, sKeyValue2);
            fetch cGetParameterInfo into sKey2, sKeyValue2, sEditable;
            close cGetParameterInfo;
          end if;

          case
            when sAction = 'INSERT' then
              oErrorObj := Stb$audit.AddAuditEntry (sAuditKeys/*sKeys||': '||sKeyValues*/, 'Insert', RTRIM(sNewValueString, CHR(10)), 1);
            when sAction = 'DELETE' then
              oErrorObj := Stb$audit.AddAuditEntry (sAuditKeys/*sKeys||': '||sKeyValues*/, 'Delete', RTRIM(sOldValueString,CHR(10)), 1);
            when sAction = 'UPDATE' and sOldValueString != sNewValueString then
              oErrorObj := Stb$audit.AddAuditEntry (sAuditKeys/*sKeys||': '||sKeyValues*/, RTRIM(sOldValueString, CHR(10)), RTRIM(sNewValueString, CHR(10)), 1);
            else
              null;
          end case;

        end loop;

        oErrorObj := getAuditType;

       if UPPER (oParameter.sToken) = 'CAN_MODIFY_REPTYPE_PARAMETER_VALUES'
        or UPPER (oParameter.sToken) = 'CAN_MODIFY_REPTYPE_CALC_VALUES'
        or UPPER (oParameter.sToken) = 'CAN_MODIFY_REPTYPE_STYLE_VALUES' then
          update isr$template
             set modifiedon = SYSDATE
               , modifiedby = sUserName
           where fileid = STB$OBJECT.getCurrentNodeId;
        end if;

      end if;

      -- ================== UPLOAD ATTACHMENT (POSSIBLE ON EACH NODE)
    when UPPER (oParameter.sToken) in ('CAN_UPLOAD_COMMON_DOC') then
        open cGetJobIds (oParameter.sToken, null);
        fetch cGetJobIds
        into nOutputId, nActionId, sFunction;
        close cGetJobIds;

        if nActionId is null then
          sMsg := utd$msglib.getmsg ('exNotFindAction', stb$security.getcurrentlanguage, oParameter.sToken);
          raise stb$job.exNotFindAction;
        end if;

        select STB$JOB$SEQ.NEXTVAL
        into   STB$JOB.nJobId
        from   DUAL;

        STB$JOB.setJobParameter ('TOKEN', UPPER (oParameter.sToken), STB$JOB.nJobid);
        STB$JOB.setJobParameter ('OUTPUTID', nOutputId, STB$JOB.nJobid);
        STB$JOB.setJobParameter ('ACTIONID', nActionId, STB$JOB.nJobid);
        STB$JOB.setJobParameter ('NODETYPE', STB$OBJECT.getCurrentNodeType, STB$JOB.nJobid);
        STB$JOB.setJobParameter ('REFERENCE', TO_CHAR (-1), STB$JOB.nJobid);
        STB$JOB.setJobParameter ('FUNCTION', sFunction, STB$JOB.nJobid);
        STB$JOB.setJobParameter ('ACTIONTYPE', 'UPLOAD', STB$JOB.nJobid);

        if STB$OBJECT.getCurrentNodeType = 'REPORT' then
          STB$JOB.setJobParameter ('REPORTTYEPID', STB$OBJECT.getCurrentReporttypeId, STB$JOB.nJobid);
          STB$JOB.setJobParameter ('SRNUMBER', STB$OBJECT.getCurrentSRNumber, STB$JOB.nJobid);
          STB$JOB.setJobParameter ('REPID', STB$OBJECT.getCurrentRepid, STB$JOB.nJobid);
          STB$JOB.setJobParameter ('NODEID', STB$OBJECT.getCurrentNodeId, STB$JOB.nJobid);
        else
          -- take the hash nodeid so that you can display a common doc on every node
          -- if you take the "normal" nodeid the common doc is e.g. displayed
          -- on MAINREPORT/REPORTTYPE201 and CUSTOMTREE/REPORTTYPE201
          STB$JOB.setJobParameter ('NODEID', STB$OBJECT.getCurrentNode().sNodeId, STB$JOB.nJobid);
        end if;

        for i in 1 .. olParameter.COUNT () loop
          STB$JOB.setJobParameter (olParameter (i).sParameter, olParameter (i).sValue, STB$JOB.nJobid);
        end loop;

        oErrorObj := STB$JOB.startJob;
      -- ================== set the new system parameters
    when UPPER (oParameter.sToken) = 'CAN_MODIFY_SYSTEM_PARAMETERS' then

        begin
          select SUM (TO_NUMBER (parametervalue))
            into nJobSum
            from stb$reptypeparameter
           where parametername like '%JOB%'
             and userparameter = 'F';
        exception when others then
          nJobSum := null;
        end;
        if nJobSum is not null and nJobSum > TO_NUMBER(STB$UTIL.getProperty(olParameter, 'MAX_JOBS')) then
          raise exJobSumError;
        end if;

        oErrorObj := Stb$system.putSystemparameters (olParameter, UPPER (oParameter.sToken));

        -- check if there have been changes
        if (Stb$audit.getModifiedFlag = 'F') then
          -- no changes -> free the dom and return OError;
          isr$trace.debug ('no changes',  'no changes', sCurrentName);
          oErrorObj := Stb$audit.FreeAuditDom;
          return oErrorObj;
        end if;

        oErrorObj := getAuditType;
      -- ================== restore one report
    when UPPER (oParameter.sToken) = 'CAN_RESTORE_REPORT' then
        oErrorObj := restoreReports ('where jobid = ' || STB$UTIL.getProperty(olParameter, 'JOBID'), olParameter);
        ISR$TREE$PACKAGE.selectCurrent;

      -- ================== zip log table
    when UPPER (oParameter.sToken) = 'CAN_ZIP_LOGTABLE'
      or UPPER (oParameter.sToken) = 'CAN_EXPORT_JOB_TRACE_LOG'
      or UPPER (oParameter.sToken) = 'CAN_EXPORT_SESSION_TRACE_LOG' then

        -- build the where clause
        if UPPER (oParameter.sToken) = 'CAN_ZIP_LOGTABLE' then

         -- oracle user
          if TRIM(olParameter(1).sValue) is null then
            sUserName :=  'like ''%'' or oracleusername is null';
          else
            sUserName := '=''' ||upper(olParameter(1).sValue)||'''';     -- vs 08. Jul. 2016 [ISRC-546]: upper added
          end if;

          -- startDate
          if  TRIM(olParameter(2).sValue)  is null then
            sStartDate := 'to_date(''01.01.2003 00:00:00'', ''DD.MM.YYYY HH24:MI:SS'')';
          else
            sStartDate := 'to_date(''' ||olParameter(2).sValue  ||''', ''DD.MM.YYYY HH24:MI:SS'')';
          end if;

          --endDate
          if TRIM(olParameter(3).sValue) is null then
            sEndDate := 'sysdate';
          else
            sEndDate :=  'to_date(''' ||olParameter(3).sValue||''', ''DD.MM.YYYY HH24:MI:SS'')';
          end if;

          sWhere :=   ' where (UPPER(ORACLEUSERNAME) ' || sUserName || ') '   -- vs 08. Jul. 2016 [ISRC-546]: upper added
                                ||' and TIMESTAMP>=' || sStartDate
                                ||' and TIMESTAMP<='  || sEndDate;

        elsif UPPER (oParameter.sToken) = 'CAN_EXPORT_JOB_TRACE_LOG'
           or UPPER (oParameter.sToken) = 'CAN_EXPORT_SESSION_TRACE_LOG' then

          sWhere :=   ' where sessionid = '||STB$UTIL.getProperty(olParameter, 'SESSIONID');

        end if;

        open cGetJobIds ('CAN_ZIP_LOGTABLE', null);
        fetch cGetJobIds
        into  nOutputId, nActionId, sFunction;
        close cGetJobIds;

        if nActionId is null then
          sMsg := utd$msglib.getmsg ('exNotFindAction', stb$security.getcurrentlanguage, oParameter.sToken);
          raise stb$job.exNotFindAction;
        end if;

        select STB$JOB$SEQ.NEXTVAL
        into   STB$JOB.nJobid
        from   DUAL;

        oErrorObj := Stb$util.createFilterParameter (olParameter);

        STB$JOB.setJobParameter ('TOKEN', UPPER (oParameter.sToken), STB$JOB.nJobid);
        STB$JOB.setJobParameter ('OUTPUTID', nOutputId, STB$JOB.nJobid);
        STB$JOB.setJobParameter ('ACTIONID', nActionId, STB$JOB.nJobid);
        STB$JOB.setJobParameter ('WHERE', sWhere, STB$JOB.nJobid);
        STB$JOB.setJobParameter ('REFERENCE', STB$OBJECT.getCurrentNodeId, STB$JOB.nJobid);
        STB$JOB.setJobParameter ('FUNCTION', sFunction, STB$JOB.nJobid);
        STB$JOB.setJobParameter ('TO', STB$UTIL.getProperty(olParameter, 'TO'), STB$JOB.nJobid);
        STB$JOB.setJobParameter ('ACTIONTYPE', 'SAVE', STB$JOB.nJobid);
        oErrorObj := STB$JOB.startJob;

       -- ================== export loader
    when UPPER (oParameter.sToken) = 'CAN_EXPORT_LOADER_DIR' then
      isr$trace.debug('getCurrentNodeId', stb$object.getCurrentNodeId, sCurrentName);

      open cGetJobIds ('CAN_EXPORT_LOADER_DIR', null);
      fetch cGetJobIds
      into  nOutputId, nActionId, sFunction;
      close cGetJobIds;

      if nActionId is null then
        sMsg := utd$msglib.getmsg ('exNotFindAction', stb$security.getcurrentlanguage, oParameter.sToken);
        raise stb$job.exNotFindAction;
      end if;

      select STB$JOB$SEQ.NEXTVAL
      into   STB$JOB.nJobid
      from   DUAL;

      oErrorObj := Stb$util.createFilterParameter (olParameter);

      STB$JOB.setJobParameter ('TOKEN', UPPER (oParameter.sToken), STB$JOB.nJobid);
      STB$JOB.setJobParameter ('OUTPUTID', nOutputId, STB$JOB.nJobid);
      STB$JOB.setJobParameter ('ACTIONID', nActionId, STB$JOB.nJobid);
      STB$JOB.setJobParameter ('REFERENCE', STB$OBJECT.getCurrentNodeId, STB$JOB.nJobid);
      STB$JOB.setJobParameter ('FUNCTION', sFunction, STB$JOB.nJobid);
      STB$JOB.setJobParameter ('TO', STB$UTIL.getProperty(olParameter, 'TO'), STB$JOB.nJobid);
      STB$JOB.setJobParameter ('OVERWRITE', STB$UTIL.getProperty(olParameter, 'OVERWRITE'), STB$JOB.nJobid);
      STB$JOB.setJobParameter ('ACTIONTYPE', 'SAVE', STB$JOB.nJobid);
      oErrorObj := STB$JOB.startJob;

       -- ================== no actions
    when upper(oParameter.sToken) = 'SHOW_JOB_MESSAGE' then  -- vs 08.Mar.2016 [ISRC-491]
        -- nothing to do
        null;

      -- ================== display the system report
    when upper(oParameter.sToken) like 'CAN_DISPLAY_%' then

        open cGetJobIds (case
                           when UPPER (oParameter.sToken) like 'CAN_DISPLAY_%AUDIT' then 'CAN_DISPLAY_AUDIT'
                           when UPPER (oParameter.sToken) like 'CAN_DISPLAY_TEMPLATE_DIFF' then UPPER (oParameter.sToken)
                           else 'CAN_DISPLAY_CONFIGURATION'
                         end, null);
        fetch cGetJobIds
        into  nOutputId, nActionId, sFunction;
        close cGetJobIds;

        if nActionId is null then
          sMsg := utd$msglib.getmsg ('exNotFindAction', stb$security.getcurrentlanguage, oParameter.sToken);
          raise stb$job.exNotFindAction;
        end if;


          -- continue processing
        select STB$JOB$SEQ.NEXTVAL
        into   STB$JOB.nJobid
        from   DUAL;

        oErrorObj := Stb$util.createFilterParameter (olParameter);

        STB$JOB.setJobParameter ('TOKEN', upper (oParameter.sToken), STB$JOB.nJobid);
        STB$JOB.setJobParameter ('OUTPUTID', nOutputId, STB$JOB.nJobid);
        STB$JOB.setJobParameter ('ACTIONID', nActionId, STB$JOB.nJobid);
        STB$JOB.setJobParameter ('REFERENCE', case when UPPER (oParameter.sToken) like 'CAN_DISPLAY_%AUDIT' then TO_CHAR (-1) else STB$OBJECT.getCurrentNodeId end, STB$JOB.nJobid);
        STB$JOB.setJobParameter ('FUNCTION', sFunction, STB$JOB.nJobid);
        oErrorObj := STB$JOB.startJob;

      -- ==================  not existing
    else
        raise exNoTokenHandle;
    end case;

    isr$trace.stat('end','end',sCurrentName);
    return oErrorObj;
  exception
    when exPortsNotAvailable then
      sMsg := utd$msglib.getmsg ('exPortsNotAvailableText', stb$security.getcurrentlanguage);
      oErrorObj.handleError ( Stb$typedef.cnSeverityWarning, sMsg, sCurrentName,
               dbms_utility.format_error_stack || CHR(10) || dbms_utility.format_error_backtrace);
      return oErrorObj;
    when exJobSumError then
       sMsg  :=Utd$msglib.GetMsg('exJobSumErrorText',Stb$security.GetCurrentLanguage) || nJobSum;
       oErrorObj.handleError ( Stb$typedef.cnSeverityWarning, sMsg, sCurrentName,
               dbms_utility.format_error_stack || CHR(10) || dbms_utility.format_error_backtrace);
       return oErrorObj;
    when exNoTokenHandle then
      sMsg := utd$msglib.getmsg ('exNoTokenHandle', stb$security.getcurrentlanguage, oParameter.sToken);
      oErrorObj.handleError ( Stb$typedef.cnSeverityWarning, sMsg, sCurrentName, 'user defined exception: exNoTokenHandle'||stb$typedef.crlf||DBMS_UTILITY.format_error_stack ||stb$typedef.crlf|| DBMS_UTILITY.format_error_backtrace );
      rollback;
      return oErrorObj;
    when stb$job.exNotFindAction then
      oErrorObj.handleError ( Stb$typedef.cnSeverityCritical, sMsg, sCurrentName, dbms_utility.format_error_stack ||stb$typedef.crlf|| dbms_utility.format_error_backtrace );
      rollback;
      return oErrorObj;
    when others then
   rollback;
      oErrorObj.handleError ( Stb$typedef.cnSeverityWarning, utd$msglib.getmsg ('exUnexpectedError', stb$security.getCurrentlanguage,  csP1 => sqlerrm(sqlcode)),
        sCurrentName, dbms_utility.format_error_stack ||chr(10)|| dbms_utility.format_error_backtrace );
      return oErrorObj;
  end putParameters;

  --**********************************************************************************************************************************
  FUNCTION getLov( sEntity        in  varchar2,
                   sSuchwert      in  varchar2,
                   oSelectionList out ISR$TLRSELECTION$LIST )
    RETURN STB$OERROR$RECORD
    is
    oErrorObj           STB$OERROR$RECORD := STB$OERROR$RECORD();
    sCurrentName        constant varchar2(100) := $$PLSQL_UNIT||'.getLov('||sEntity||')';

    cursor cGetReporttypes is
      select reporttypeid
           , UTD$MSGLIB.getMsg (reporttypename, stb$security.GetCurrentLanguage)
                reporttypename
           , UTD$MSGLIB.getMsg (description, stb$security.GetCurrentLanguage)
                description
        from stb$reporttype
       where activetype = 1
         and (reporttypeid <> 999
           or (reporttypeid = 999
           and STB$UTIL.getSystemParameter ('SHOW_UNIT_TEST') = 'T'))
      union
      select 0
           , UTD$MSGLIB.getMsg ('NO_REPORTTYPE_NAME'
                              , stb$security.GetCurrentLanguage)
                reporttypename
           , UTD$MSGLIB.getMsg ('NO_REPORTTYPE_DESC'
                              , stb$security.GetCurrentLanguage)
                description
        from DUAL
      order by 1;

    cursor cGetEntity is
      select entity
           , UTD$MSGLIB.getMsg (entity, stb$security.GetCurrentLanguage) translation
        from isr$parameter$entities
       where editable = 'T'
      order by 1;

    cursor cGetPlugins is
      select distinct
             p.plugin
           , UTD$MSGLIB.getMsg (p.plugin, stb$security.GetCurrentLanguage)
                translation
        from isr$plugin p, isr$parameter$values pv
       where pv.entity = 'SERVERTYPEPLUGIN'
         and (pv.VALUE = p.plugin
          and STB$OBJECT.getCurrentToken like '%MODIFY%SERVER%')
          or STB$OBJECT.getCurrentToken not like '%MODIFY%SERVER%'
      order by 1;

    cursor cGetSrNumber is
      select distinct srnumber
        from STB$REPORT
       where parentrepid in (select repid from stb$report)
          or parentrepid is null
      order by srnumber asc;

    cursor cGetTitle is
      select distinct title
        from STB$REPORT r
       where (parentrepid in (select repid from stb$report)
           or parentrepid is null)
         and STB$UTIL.getReptypeParameter (r.reporttypeid, 'HAS_SRNUMBER') = 'T'
         and title is not null
      order by title asc;

  begin
    isr$trace.stat('begin','sEntity: '||sEntity||',  sSuchwert: '||sSuchwert,sCurrentName);
    oSelectionList := ISR$TLRSELECTION$LIST ();

    case
      --***************************************
      when UPPER (sEntity) = 'ENTITY' then
          for rGetEntity in cGetEntity loop
            oSelectionList.EXTEND;
            oSelectionList (oSelectionList.COUNT()) := ISR$OSELECTION$RECORD(rGetEntity.entity, rGetEntity.entity, rGetEntity.translation, null, oSelectionList.COUNT());
          end loop;
      --***************************************
      when UPPER (sEntity) = 'REPORTTYPEID' then
        for rGetReporttypes in cGetReporttypes loop
          oSelectionList.EXTEND;
          oSelectionList (oSelectionList.COUNT()) := ISR$OSELECTION$RECORD(rGetReporttypes.reporttypename, rGetReporttypes.reporttypeid, rGetReporttypes.description, null, oSelectionList.COUNT());
        end loop;
      --***************************************
      when UPPER (sEntity) = 'PLUGIN'then
        for rGetPlugins in cGetPlugins loop
          oSelectionList.EXTEND;
          oSelectionList (oSelectionList.COUNT()) := ISR$OSELECTION$RECORD(rGetPlugins.plugin, rGetPlugins.plugin, rGetPlugins.translation, null, oSelectionList.COUNT());
        end loop;
      --***************************************
        -- all srnumbers
      when UPPER (sEntity) = 'SRNUMBER' then
          for rGetSrNumber in cGetSrNumber loop
            oSelectionList.EXTEND;
            oSelectionList (oSelectionList.COUNT()) := ISR$OSELECTION$RECORD(rGetSrNumber.srnumber, rGetSrNumber.srnumber, rGetSrNumber.srnumber, null, oSelectionList.COUNT());
          end loop;
      --***************************************
        -- all titles
      when UPPER (sEntity) = 'TITLE' then
          for rGetTitle in cGetTitle loop
            oSelectionList.EXTEND;
            oSelectionList (oSelectionList.COUNT()) := ISR$OSELECTION$RECORD(rGetTitle.title, rGetTitle.title, rGetTitle.title, null, oSelectionList.COUNT());
          end loop;
      --***************************************
      else
        execute immediate
        'BEGIN
           :1 :='|| stb$util.getcurrentcustompackage || '.getLov(:2,:3,:4);
         END;'
        using out oErrorObj,  in sentity, in ssuchwert, out oselectionlist;
    end case;

    if sSuchWert is not null and oselectionlist.FIRST is not null then
      isr$trace.debug ('sSuchWert',  sSuchWert, sCurrentName);
      for i in 1..oSelectionList.COUNT() loop
        if oSelectionList(i).sValue = sSuchWert then
          isr$trace.debug ('oSelectionList('||i||').sSelected', oSelectionList(i).sValue, sCurrentName);
          oSelectionList(i).sSelected := 'T';
        end if;
      end loop;
    end if;

    isr$trace.debug('status','oSelectionList.count: '||oSelectionList.count(),sCurrentName,oSelectionList);
    isr$trace.stat('end','end',sCurrentName);
    return oErrorObj;
  exception
    when others then
      oErrorObj.handleError ( Stb$typedef.cnSeverityMessage, utd$msglib.getmsg ('exUnexpectedError', stb$security.getCurrentlanguage,  csP1 => sqlerrm(sqlcode)),
        sCurrentName, dbms_utility.format_error_stack ||chr(10)|| dbms_utility.format_error_backtrace );
      return oErrorObj;
  end getLov;

  --*****************************************************************************************************************************
  function SetCurrentNode (oSelectedNode in STB$TREENODE$RECORD)
    return STB$OERROR$RECORD is
    sCurrentName   constant varchar2(4000) := $$PLSQL_UNIT||'.SetCurrentNode';
    oErrorObj                     STB$OERROR$RECORD := STB$OERROR$RECORD();
  begin
    isr$trace.stat('begin', 'begin', sCurrentName);
    isr$trace.debug ('parameter',  'oSelectedNode.sNodetype '||oSelectedNode.sNodetype||' sCurrentNodeType '||sCurrentNodeType||' sNewLoad '||sNewLoad, sCurrentName, xmltype(oSelectedNode).getClobVal());
    stb$object.destroyoldvalues;
    if sNewLoad = 'F'
    and (   oSelectedNode.sNodetype like '%JOBS' and 'CURRENTJOBS' != sCurrentNodeType
         or oSelectedNode.sNodetype not like '%JOBS' and sCurrentNodeType not in (REPLACE(oSelectedNode.sNodetype, '_'))) then
      sNewLoad := 'T';
      sCurrentNodeType := null;
      isr$trace.debug ('set values sNewLoad and sCurrentNodeType',  'oSelectedNode.sNodetype '||oSelectedNode.sNodetype||' sCurrentNodeType '||sCurrentNodeType||' sNewLoad '||sNewLoad, sCurrentName);
    end if;
    Stb$object.setCurrentNode (oSelectedNode);
    isr$trace.stat('end', 'end', sCurrentName);
    return oErrorObj;
  exception
    when others then
      oErrorObj.handleError ( Stb$typedef.cnSeverityCritical, utd$msglib.getmsg ('exUnexpectedError', stb$security.getCurrentlanguage,  csP1 => sqlerrm(sqlcode)),
        sCurrentName, dbms_utility.format_error_stack ||chr(10)|| dbms_utility.format_error_backtrace );
      return oErrorObj;
  end SetCurrentNode;

  --***********************************************************************************************************
  function lockReports (sWhereClause in varchar2)
    return STB$OERROR$RECORD is
    pragma autonomous_transaction;
    sCurrentName         constant varchar2(4000) := $$PLSQL_UNIT||'.lockReports';
    oErrorObj                     STB$OERROR$RECORD := STB$OERROR$RECORD();
    sDynSQL                       varchar2 (4000);
  begin
    isr$trace.stat('begin', 'begin', sCurrentName);
    if Stb$security.getcurrentuser is not null then
      isr$trace.debug ('begin',  'sWhereClause:' || sWhereClause, sCurrentName);
      sDynSQL :=
           ' update stb$report '
        || ' set condition = '
        || Stb$typedef.cnStatusLocked
        || ', lockedby = '
        || Stb$security.getcurrentuser
        || ', locksession =  '
        || Stb$security.getCurrentSession
        || ', lockedOn = sysdate '
        || ' where condition = '
        || Stb$typedef.cnStatusActivated
        || ' '
        || sWhereClause;
      isr$trace.debug ('sDynSQL',  sDynSQL, sCurrentName);

      execute immediate sDynSQL;
      number_of_rows := sql%rowcount;

      commit;

      if number_of_rows > 0 then
        if STB$UTIL.checkMethodExists('triggerRibbonUpdate', 'ISR$RIBBON') = 'T' then
          execute immediate 'BEGIN
                               ISR$RIBBON.triggerRibbonUpdate;
                             END;';
        end if;
      end if;

      isr$trace.debug ('end',  'end', sCurrentName);
    end if;

    isr$trace.stat('end', 'end', sCurrentName);
    return oErrorObj;
  exception
    when others then
--      oErrorObj.sErrorCode := Utd$msglib.GetMsg ('ERROR', Stb$security.GetCurrentLanguage) || SQLCODE || ' ' || SQLERRM;
--      oErrorObj.sErrorMessage := Utd$msglib.GetMsg ('ERROR_PLACE', Stb$security.GetCurrentLanguage) || ' STB$SYSTEM.lockReports';
--      oErrorObj.sSeverityCode := Stb$typedef.cnSeverityWarning;
--      isr$trace.debug (oErrorObj);
      return oErrorObj;
  end lockReports;

  --*************************************************************************************************************************
  function setMenuAccess( sParameter   in  varchar2,  -- sParameter is the token
                          sMenuAllowed out varchar2 )
    return STB$OERROR$RECORD
  is
    oErrorObj         STB$OERROR$RECORD := STB$OERROR$RECORD();
    sCurrentName      constant varchar2(100) := $$PLSQL_UNIT||'.setMenuAccess('||sParameter||')';
    oSelectedNode     STB$TREENODE$RECORD;
    sOwnSession       char(1) := 'F';
    sRepId            number;

    cursor cGetOracleJobState is
      select state
        from user_scheduler_jobs
       where job_name = 'JOB_' || STB$OBJECT.getCurrentNodeId;

    sOracleJobState varchar2(500) := '';

    cursor cisHighestVersion(cnFileid in number) is
    select 'T'
    from isr$template
    where fileid = cnFileid
     and (templatename, version) in (select templatename, max (version)
                                       from isr$template
                                     group by templatename);

  begin
    isr$trace.stat('begin', 'sParameter: ' || sParameter, sCurrentName);

    oSelectedNode := STB$OBJECT.getCurrentNode;
    isr$trace.debug('status','oSelectedNode',sCurrentname,oSelectedNode);
    sRepId := STB$OBJECT.getCurrentRepid;
    isr$trace.debug('value','sRepId: '||sRepId,sCurrentname);

    sMenuAllowed := 'T';

    case
      when sParameter IN ('CAN_REMOVE_CONNECTION') THEN
        sOwnSession := oSelectedNode.getPropertyParameter('OWNSESSION');
        isr$trace.debug('value','sOwnSession: '||sOwnSession,sCurrentname);

        if sOwnSession = 'T' then
          sMenuAllowed := 'F';
        end if;
      when sParameter in ('CAN_CONFIGURE_ORACLE_JOBS') then
        open cGetOracleJobState;
        fetch cGetOracleJobState into sOracleJobState;
        if cGetOracleJobState%notfound
        or sOracleJobState != 'RUNNING' and sParameter = 'CAN_SUSPEND_JOB'
        or sOracleJobState != 'PAUSED' and sParameter = 'CAN_UNSUSPEND_JOB'
        or sOracleJobState not in ('DISABLED','SCHEDULED') and sParameter = 'CAN_CONFIGURE_ORACLE_JOBS' then
          sMenuAllowed := 'F';
        end if;
        close cGetOracleJobState;

      when sParameter in ('CAN_EXPORT_JOB_TRACE_LOG', 'CAN_SELECT', 'SHOW_JOB_MESSAGE') or sParameter like 'CAN_REMOVE_JOB%' then
        if olNodeListBackup is null and (sParameter in ('CAN_SELECT') or sParameter like 'CAN_REMOVE_JOB%') then
          sMenuAllowed :=  'F';
        else
          sMenuAllowed :=  'T';
        end if;

      when sParameter = 'CAN_EXPORT_LOADER_DIR' then
        if stb$job.isRunning('JOB_' || STB$OBJECT.getCurrentNodeId) = 'T' then
          sMenuAllowed := 'F';
        end if;

      when STB$OBJECT.getCurrentNodetype = 'CURRENTJOBS' and sParameter not in ('CAN_DISPLAY_USER_JOBS') then  --is: why for all another 'CURRENTJOBS'  sMenuAllowed=F?
        sMenuAllowed := 'F';
      when sParameter in ('CAN_RESTORE_REPORT') then
        select case when restored = 'T' then 'F' else 'T' end
        into   sMenuAllowed
        from   isr$archived$reports
        where  jobid = STB$OBJECT.getCurrentNodeId;
      when sParameter in ('CAN_DISPLAY_ESIG') then
        -- ISRC-1031: getNodeList leads to performance issues, use of the context is faster
        oErrorObj :=  STB$SECURITY.checkEsigExists( sSrNumber => STB$OBJECT.getCurrentSRNumber,
                                                    sTitle    => STB$OBJECT.getCurrentTitle,
                                                    sExists   => sMenuAllowed );
      --=====CAN_MODIFY_REPTYPE_PARAMETER_VALUES,CAN_MODIFY_REPTYPE_CALC_VALUES,CAN_MODIFY_REPTYPE_STYLE_VALUES
      WHEN sPARAMETER IN ('CAN_MODIFY_REPTYPE_PARAMETER_VALUES', 'CAN_MODIFY_REPTYPE_CALC_VALUES', 'CAN_MODIFY_REPTYPE_STYLE_VALUES') THEN
        isr$trace.debug ('STB$OBJECT.getCurrentNodeId', STB$OBJECT.getCurrentNodeId, sCurrentName);

        OPEN cIsHighestVersion(TO_NUMBER(STB$OBJECT.getCurrentNodeId));
        FETCH cIsHighestVersion INTO sMenuAllowed;
        IF cIsHighestVersion%NOTFOUND THEN
          sMenuAllowed := 'F';
        END IF;
        CLOSE cIsHighestVersion;
        isr$trace.debug('sMenuAllowed', sMenuAllowed  , sCurrentName);

        IF sMenuAllowed = 'T' AND sParameter <> 'CAN_UPLOAD_NEW_TEMPLATE' THEN
          EXECUTE IMMEDIATE 'select case when count(*) > 0 then ''T'' else ''F'' end from isr$file$'||REPLACE(REPLACE(sPARAMETER, 'CAN_MODIFY_REPTYPE_'), '_VALUES')||'$values where fileid = '||STB$OBJECT.getCurrentNodeId
          INTO sMenuAllowed;
        END IF;

        isr$trace.stat('end', sMenuAllowed  , sCurrentName);
        RETURN oErrorObj;

      else
        sMenuAllowed :=  'T';
    end case;

    isr$trace.stat('end', 'sMenuAllowed:' || sMenuAllowed, sCurrentName);
    return oErrorObj;
  exception
    when others then
      oErrorObj.handleError ( Stb$typedef.cnSeverityCritical, utd$msglib.getmsg ('exUnexpectedError', stb$security.getCurrentlanguage,  csP1 => sqlerrm(sqlcode)),
        sCurrentName, dbms_utility.format_error_stack ||chr(10)|| dbms_utility.format_error_backtrace );
      return oErrorObj;
  end setMenuAccess;

  --*****************************************************************************************************************************
  function getAuditMask (olNodeList out STB$TREENODELIST)
    return STB$OERROR$RECORD is
    sCurrentName   constant varchar2(4000) := $$PLSQL_UNIT||'.getAuditMask';
    oErrorObj                     STB$OERROR$RECORD := STB$OERROR$RECORD();
  begin
    isr$trace.stat('begin', 'begin', sCurrentName);
    oErrorobj := Stb$audit.getAuditMask (olNodeList);
    isr$trace.stat('end', 'end', sCurrentName);
    return (oErrorObj);
  exception
    when others then
      rollback;
      oErrorObj.handleError ( Stb$typedef.cnSeverityCritical, utd$msglib.getmsg ('exUnexpectedError', stb$security.getCurrentlanguage,  csP1 => sqlerrm(sqlcode)),
        sCurrentName, dbms_utility.format_error_stack ||chr(10)|| dbms_utility.format_error_backtrace );
      return (oErrorObj);
  end getAuditMask;

  --*****************************************************************************************************
  function putAuditMask (olParameter in STB$PROPERTY$LIST)
    return STB$OERROR$RECORD is
    sCurrentName   constant varchar2(4000) := $$PLSQL_UNIT||'.putAuditMask';
    oErrorObj                     STB$OERROR$RECORD := STB$OERROR$RECORD();
  begin
    isr$trace.stat('begin', 'begin', sCurrentName);
    oErrorObj := Stb$audit.putAuditMask (olParameter);
    isr$trace.stat('end', 'end', sCurrentName);
    return (oErrorObj);
  exception
    when others then
      rollback;
      oErrorObj.handleError ( Stb$typedef.cnSeverityCritical, utd$msglib.getmsg ('exUnexpectedError', stb$security.getCurrentlanguage,  csP1 => sqlerrm(sqlcode)),
        sCurrentName, dbms_utility.format_error_stack ||chr(10)|| dbms_utility.format_error_backtrace );
      return (oErrorObj);
  end putAuditMask;

  --*****************************************************************************************************
  function GetNodeChilds (oSelectedNode in STB$TREENODE$RECORD, olNodeList out STB$TREENODELIST)
    return STB$OERROR$RECORD is
    sCurrentName   constant varchar2(4000) := $$PLSQL_UNIT||'.xxxxxxxxxx';
    oErrorObj         STB$OERROR$RECORD := STB$OERROR$RECORD();
  begin
    isr$trace.stat('begin', 'begin', sCurrentName);
    olNodeList := STB$TREENODELIST ();
    isr$trace.stat('end', 'end', sCurrentName);
    return (oErrorObj);
  exception
    when others then
      rollback;
      oErrorObj.handleError ( Stb$typedef.cnSeverityCritical, utd$msglib.getmsg ('exUnexpectedError', stb$security.getCurrentlanguage,  csP1 => sqlerrm(sqlcode)),
        sCurrentName, dbms_utility.format_error_stack ||chr(10)|| dbms_utility.format_error_backtrace );
      return (oErrorObj);
  end GetNodeChilds;

  -- ***************************************************************************************************
  function GetNodeList (oSelectedNode in STB$TREENODE$RECORD, olNodeList out STB$TREENODELIST)
    return STB$OERROR$RECORD is
    sCurrentName         constant varchar2(4000) := $$PLSQL_UNIT||'.GetNodeList';
    oErrorObj                     STB$OERROR$RECORD := STB$OERROR$RECORD();
    olParameter                   STB$PROPERTY$LIST;
    sMsg               varchar2(4000);
  begin
    isr$trace.stat('begin', 'begin', sCurrentName);
    isr$trace.debug ('parameter', oSelectedNode.sNodeType||' sNewLoad '||sNewLoad, sCurrentName);
    --=========================SYSTEMRIGHTS=====================================
    if oSelectedNode.sNodeType in ('SYSTEMRIGHT', 'SYSTEMPARAMETER') then

      olNodeList := STB$TREENODELIST ();
      for rRights in (select distinct parametername
                                    , case when right = 'x' then 'T' else 'F' end parametervalue
                                    , UTD$MSGLIB.getMsg(description, stb$security.getcurrentlanguage) display
                                    , SUBSTR (orderno, 1, 1) flag
                                    , orderno
                                    , UTD$MSGLIB.getMsg(GROUPING, stb$security.getcurrentlanguage) grouping_desc
                                    , folder
                                    , folderid
                                    , NVL(icon, 'SYSTEMRIGHT'||SUBSTR (orderno, 1, 1)) icon
                                    , GROUPING
                        from isr$right$configuration1
                       where id = 0
                         and usergroupno = STB$OBJECT.getNodeidForNodetype ('FUNCTIONGROUP')
                         and type2 = 'GROUPRIGHTS'
                         and case
                                when STB$OBJECT.getCurrentNodetype = 'SYSTEMRIGHT' then 1
                                when STB$OBJECT.getCurrentNodetype = 'SYSTEMPARAMETER' then 2
                             end = SUBSTR (orderno, 1, 1)
                      order by 6, 8, 5, 3) loop

        olnodelist.EXTEND;
        olnodelist (olNodeList.COUNT()) := STB$TREENODE$RECORD(SUBSTR(rrights.display, 0, 255),
                                                               'ISR$REPORTTYPE$PACKAGE', 'SYSTEMRIGHTLIST',
                                                               rrights.parametername||'#@#'||rrights.GROUPING||rrights.folder, 'SYSTEMRIGHT'||rRights.flag,
                                                               'CAN_SWITCH_RIGHT', tloValueList => oSelectedNode.tloValueList);
        STB$OBJECT.setDirectory(olNodeList (olNodeList.COUNT()));
        -- add additional parameters to display in data area
        olparameter := stb$property$list ();
        olparameter.EXTEND;
        olParameter (olparameter.COUNT) := STB$PROPERTY$RECORD(rrights.parametername, utd$msglib.getmsg ('SYSTEMRIGHT_ENABLED', stb$security.getcurrentlanguage),
                                                               rrights.parametervalue, rrights.parametervalue,
                                                               'F', TYPE => 'BOOLEAN');
        if STB$OBJECT.getCurrentNodetype = 'SYSTEMRIGHT' then
          olparameter.EXTEND;
          olParameter (olparameter.COUNT) := STB$PROPERTY$RECORD(rrights.parametername, utd$msglib.getmsg ('GROUPINGRIGHTS', stb$security.getcurrentlanguage),
                                                                 rrights.grouping_desc, rrights.grouping_desc,
                                                                 'F', TYPE => 'STRING');
          olparameter.EXTEND;
          olParameter (olparameter.COUNT) := STB$PROPERTY$RECORD(rrights.parametername, utd$msglib.getmsg ('FOLDERRIGHTS', stb$security.getcurrentlanguage),
                                                                 rrights.folder, rrights.folder,
                                                                 'F', TYPE => 'STRING');
        end if;

        -- add the propoerty-list to the node object
        olnodelist (olNodeList.COUNT()).tlopropertylist := olparameter;
      end loop;

    --=========================MENU_DETAIL======================================
    elsif oSelectedNode.sNodetype in ('LOCKED_REPORTS', 'CHECKEDOUT_REPORTS', 'DEACTIVATED_REPORTS', 'ARCHIVED_REPORTS', 'CURRENT_CONNECTIONS')
       or oSelectedNode.sNodetype like '%JOBS' then

        if sNewLoad = 'T' then
          olNodeListBackup := null;

          olNodeList := STB$TREENODELIST ();
          olNodeList.EXTEND;
          olNodeList (1) := STB$TREENODE$RECORD(null,
                                           'STB$SYSTEM', case when oSelectedNode.sNodetype like '%JOBS' then 'CURRENTJOBS' else REPLACE(oSelectedNode.sNodetype, '_') end,
                                           null, SUBSTR(oSelectedNode.sNodetype, 1, LENGTH(oSelectedNode.sNodetype)-1),
                                           'CAN_SELECT', tloValueList => oSelectedNode.tloValueList);
          if oSelectedNode.sNodetype like '%JOBS' then
            olNodeList (1).nDisplayType := STB$TYPEDEF.cnMenuDetailView;
          end if;

          if oSelectedNode.sNodetype = 'LOCKED_REPORTS' then
            oErrorObj := Stb$system.getLockedReports (olNodeList);
          elsif oSelectedNode.sNodetype = 'CHECKEDOUT_REPORTS' then
            oErrorObj := Stb$system.getCheckedOutReports (olNodeList);
          elsif oSelectedNode.sNodetype = 'DEACTIVATED_REPORTS' then
            oErrorObj := Stb$system.getDeactivatedReports (olNodeList);
          elsif oSelectedNode.sNodetype = 'ARCHIVED_REPORTS' then
            oErrorObj := Stb$system.getArchivedReports (olNodeList);
          elsif oSelectedNode.sNodetype = 'CURRENT_CONNECTIONS' then
            oErrorObj := Stb$system.getCurrentConnections (olNodeList);
          elsif oSelectedNode.sNodetype like '%JOBS' then
            oErrorObj := Stb$system.getCurrentJobs (olNodeList
                  , case when oSelectedNode.sNodetype = 'USER_JOBS' then 'T' else 'F' end
                  , case when oSelectedNode.sNodetype = 'SCHEDULED_JOBS' then 'T'
                         when oSelectedNode.sNodetype = 'JOBS' then 'F' else '' end
                  , case when oSelectedNode.sNodetype = 'TITLE_JOBS' then 'T' else 'F' end);
          end if;

          if olNodeList (1).tloPropertyList.COUNT() > 0 then
            olNodeListBackup := olNodeList;
          elsif oSelectedNode.sNodetype like '%JOBS' then
            olNodeList (1).tloPropertyList.EXTEND;
            olNodeList (1).tloPropertyList(olNodeList (1).tloPropertyList.COUNT()) := STB$PROPERTY$RECORD('NO_DATA_FOUND', '', --Utd$msglib.GetMsg ('general.table.empty.label.text', Stb$security.GetCurrentLanguage),
                                                                                                            utd$msglib.GetMsg ('exNo'||REPLACE(INITCAP(UPPER (oSelectedNode.sNodetype)),'_')||'ExistText', Stb$security.GetCurrentLanguage),
                                                                                                            utd$msglib.GetMsg ('exNo'||REPLACE(INITCAP(UPPER (oSelectedNode.sNodetype)),'_')||'ExistText', Stb$security.GetCurrentLanguage),
                                                                                                            TYPE => 'STRING');
            olNodeList (1).sNodeId := oSelectedNode.sNodetype||'_NO_DATA_FOUND';
            olNodeList (1).sNodeType := 'NO_DATA_FOUND';
            olNodeList (1).sIcon := 'CONNECTION_WARNING';
          else
            olNodeList := STB$TREENODELIST ();
          end if;

        else
          olNodeList := olNodeListBackup;
        end if;


    --=========================NOT DEFINED======================================
    else
      oErrorObj := Getnodechilds (oSelectedNode, olNodeList);
    end if;

    isr$trace.debug('value','olNodeList OUT','STB$SYSTEM.GetNodeList', olNodeList);
    isr$trace.stat('end', 'olNodeList.count: '||olNodeList.COUNT, sCurrentName);
    return (oErrorObj);
  exception
    when others then
      rollback;
      sMsg := utd$msglib.getmsg ('ERROR', stb$security.getcurrentlanguage,  sqlerrm(sqlcode));
      oErrorObj.handleError ( Stb$typedef.cnSeverityCritical, sMsg, sCurrentName, dbms_utility.format_error_stack ||chr(10)|| dbms_utility.format_error_backtrace );

      return (oErrorObj);
  end GetNodeList;

--*********************************************************************************************************************************
function getContextMenu( nMenu           in  number,
                         olMenuEntryList out STB$MENUENTRY$LIST )
  return STB$OERROR$RECORD is
  oErrorObj    STB$OERROR$RECORD := STB$OERROR$RECORD();
  sCurrentName           constant varchar2(100) := $$PLSQL_UNIT||'.getContextMenu('||nMenu||')';
begin
  isr$trace.stat ('begin', 'nMenu: '||nMenu, 'sCurrentName');
  olMenuEntryList := STB$MENUENTRY$LIST();
  oErrorObj       := Stb$util.getContextMenu(Stb$object.getCurrentNodeType, nMenu, olMenuEntryList);
  isr$trace.stat ('end', 'olMenuEntryList.count: '||olMenuEntryList.COUNT, 'sCurrentName');
  return (oErrorObj);
exception
  when others then
    rollback;
      oErrorObj.handleError ( Stb$typedef.cnSeverityCritical, utd$msglib.getmsg ('exUnexpectedError', stb$security.getCurrentlanguage,  csP1 => sqlerrm(sqlcode)),
        sCurrentName, dbms_utility.format_error_stack ||chr(10)|| dbms_utility.format_error_backtrace );
    return (oErrorObj);
end getContextMenu;

--*********************************************************************************************************************************
function getDefaultTablespace
  return varchar2
is
  oErrorObj              STB$OERROR$RECORD := STB$OERROR$RECORD();
  sCurrentName           constant varchar2(100) := $$PLSQL_UNIT||'.getDefaultTablespace';
  sMsg                   varchar2(4000);
begin
  isr$trace.stat('begin', 'begin', sCurrentName );

  if sDefaultTablespace is null then
    -- fetch default tablespace
    select default_tablespace
    into   sDefaultTablespace
    from   user_users;
  end if;

  return sDefaultTablespace;
  isr$trace.stat('end', 'end', sCurrentName );
exception
  when others then
    sMsg :=  utd$msglib.getmsg ('exUnexpectedError', nCurrentLanguage, csP1 => SQLERRM(SQLCODE));
    oErrorObj.handleError ( Stb$typedef.cnSeverityCritical, sMsg, sCurrentName,
                            SUBSTR(DBMS_UTILITY.format_error_stack || CHR(10) || DBMS_UTILITY.format_error_backtrace, 1,4000)  );
    return sDefaultTablespace;
end getDefaultTablespace;

--*********************************************************************************************************************************
function getNologTablespace( csForceRead varchar2 default 'F' )
  return varchar2
is
  oErrorObj              STB$OERROR$RECORD := STB$OERROR$RECORD();
  sCurrentName           constant varchar2(100) := $$PLSQL_UNIT||'.getNologTablespace';
  sMsg                   varchar2(4000);
begin
  isr$trace.stat('begin', 'begin', sCurrentName );

  if (    sNologTablespace is null
       or UPPER(csForceRead) = 'T' ) then
    -- fetch nolog tablespace
    sNologTablespace := stb$util.getSystemParameter('ISR_TABLE_NOLOG_TABLESPACE');
    if sNologTablespace is null then
      -- if systemparameter ISR_TABLE_NOLOG_TABLESPACE does not exist, then use default tablespace
      sNologTablespace := stb$system.getDefaultTablespace();
    end if;
  end if;

  return sNologTablespace;
  isr$trace.stat('end', 'end', sCurrentName );
exception
  when others then
    sMsg :=  utd$msglib.getmsg ('exUnexpectedError', nCurrentLanguage, csP1 => SQLERRM(SQLCODE));
    oErrorObj.handleError ( Stb$typedef.cnSeverityCritical, sMsg, sCurrentName,
                            SUBSTR(DBMS_UTILITY.format_error_stack || CHR(10) || DBMS_UTILITY.format_error_backtrace, 1,4000)  );
    return sNologTablespace;
end getNologTablespace;



end stb$system; 