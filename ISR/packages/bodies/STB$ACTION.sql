CREATE OR REPLACE PACKAGE BODY STB$ACTION
is

  sIP                    STB$JOBSESSION.IP%type;
  sPort                  STB$JOBSESSION.PORT%type;
  sTimeout               varchar2(10);

  --constants
  csOffshoot            varchar2(100) :='CAN_OFFSHOOT_DOC';
  csTarget              varchar2(100) :='TARGET';
  csConfig              varchar2(100) :='CONFIG';
  csMasterTemplate      varchar2(100) :='MASTERTEMPLATE';
  csTemplate            varchar2(100) :='TEMPLATE';
  csRohReport           varchar2(100) :='ROHREPORT';
  csRohDaten            varchar2(100) :='ROHDATEN';
  csRemoteSnapshot      varchar2(100) :='REMOTESNAPSHOT';
  csDocPropOid          varchar2(100) :='DOC_OID';
  csMimeType            varchar2(100) :='application/octet-stream';
  csISRTempPath         varchar2(100) :='[ISR.TEMP.PATH]';

  sContext              varchar2(500) := STB$UTIL.getSystemParameter('LOADER_CONTEXT');
  sNameSpace            varchar2(500) := STB$UTIL.getSystemParameter('LOADER_NAMESPACE');
  nCurrentLanguage      number        := stb$security.getcurrentlanguage;
  crlf                  constant varchar2(2) := chr(10)||chr(13);
  sJavaLogLevel      isr$loglevel.javaloglevel%type;

--***********************************************************************************************************************
function XMLFileExists(cnJobid in number, csDoctype in STB$DOCTRAIL.DOCTYPE%type, nDocIdXML out STB$DOCTRAIL.DOCID%type) return boolean is
-- function to check if the rawdata or snapshot xml-file already exists in the table stb$doctrail
  sCurrentName   constant varchar2(4000) := $$PLSQL_UNIT||'.XMLFileExists('||cnJobid||')';
  cursor cCheckXmlExists is
    select d.docid
      from STB$DOCTRAIL d
     where d.nodeid = STB$JOB.getJobParameter('REPID', cnJobid)
       and d.doctype = csDoctype;
  bExists boolean;
begin
  isr$trace.stat('begin', 'begin', sCurrentName);
  open cCheckXmlExists;
  fetch cCheckXmlExists into nDocIdXML;
  if cCheckXmlExists%found then
    bExists := true;
  else
    bExists := false;
    nDocIdXML := 0;
  end if;
  close cCheckXmlExists;
  isr$trace.stat('end', 'end', sCurrentName);
  return bExists;
end XMLFileExists;

--***********************************************************************************************************************
function getPath( csFullFileName in varchar2) return varchar2
is
begin
  return substr(csFullFileName,0,Instr(csFullFileName,'\',-1));
end;

--***********************************************************************************************************************
function getFilename(csFullFileName in varchar2) return varchar2
is
begin
  return substr(csFullFileName,Instr(csFullFileName,'\',-1)+1);
end;

--***********************************************************************************************************************
function getExtension(csFullFileName in varchar2) return varchar2
is
begin
  if instr(csFullFileName,'.',-1) = 0 then
    return null;
 end if;
  return substr(csFullFileName,Instr(csFullFileName,'.',-1)+1);
end;

--***********************************************************************************************************************
function initialiseJob( cnJobid in     number,
                        cxXml   in out xmltype )
  return STB$OERROR$RECORD
is
  exInitFailed           exception;
  sCurrentName   constant varchar2(100) := $$PLSQL_UNIT||'.initialiseJob (JobID='||cnJobId||')';
  sUserMsg               varchar2(4000);
  sDevMsg                varchar2(4000);
  sImplMsg               varchar2(4000);
  sJobid                 varchar2(100);
  oErrorObj               STB$OERROR$RECORD :=STB$OERROR$RECORD();
  oParamListRec           ISR$PARAMLIST$REC := ISR$PARAMLIST$REC();

  cursor cGetSessionId(csIp in varchar2, csPort in varchar2) is
    select sessionid
      from isr$session
     where upper(ip) = upper(csIp)
       and port = csPort;

  nSessionId             number;
  sReturn                varchar2(4000);

  nDocIdXML  STB$DOCTRAIL.DOCID%type;

begin
  isr$trace.stat('begin', 'cnJobid: '||cnJobid, sCurrentName);
  --isr$trace.error('test', 'test loader error', sCurrentName);  --only for test ISRC-1225

  if XMLFileExists(cnJobid, 'X', nDocIdXML) then
    isr$trace.warn('warning','XMLFileExists = TRUE, docid: '||nDocIdXML, sCurrentName);
  end if;

  -- this is the start point to create an istudyReporter document
  -- if there are already files belonging to the jobid in the table stb$jobtrail
  -- because a database job has been broken delete the files
  delete from STB$JOBTRAIL where jobid=cnjobid;
  commit;

  STB$SECURITY.initJobUser(STB$JOB.getJobParameter('USER', cnJobid));

  -- initialize the loader

  select ip, To_Char(js.port), nvl(so.timeout, STB$UTIL.getSystemParameter('TIMEOUT_LOADER_THREAD'))
  into sIp, sPort, sTimeout
  from STB$JOBSESSION js, isr$serverobserver$v so
  where js.ip = so.observer_host (+)
  and JS.PORT = so.port (+)
  and jobid = cnJobId;

  isr$trace.debug('IP/PORT','IP und Port: ' || sIP || ':' || sPort, sCurrentName);

  if Nvl(STB$JOB.getJobParameter('START_LOADER', cnJobId), 'T') = 'T' then
    -- send it to all clients
    for rGetJobs in ( select distinct ip, port
                        from (select ip, port
                                from STB$JOBSESSION
                               where jobid = cnJobid
                              union
                              select ip, port
                                from isr$session s, stb$jobqueue jq
                               where upper(s.username) = upper(STB$SECURITY.getOracleUsername(jq.userno))
                                 and jq.jobid = cnJobid
                                 and NVL(inactive, 'F') = 'F'
                                 and NVL(markedforshutdown, 'F') = 'F')
                       where trim (ip) is not null
                         and trim (port) is not null ) loop
      ISR$TRACE.debug('try to contact ...','rGetJobs.ip: ' || rGetJobs.ip || ', rGetJobs.port: '||rGetJobs.port,sCurrentName);
      sReturn := STB$ACTION.isDocServerAvailable(rGetJobs.ip, rGetJobs.port);
      IF  sReturn is null THEN
        ISR$TRACE.debug('ok','rGetJobs.ip: ' || rGetJobs.ip || ', rGetJobs.port: '||rGetJobs.port,sCurrentName);
        sReturn := ISR$LOADER.sendJobList(rGetJobs.ip, rGetJobs.port);
        IF sReturn != 'T' THEN
          ISR$TRACE.error('sReturn', sReturn, sCurrentName);
        END IF;
        sReturn := ISR$LOADER.startJob(rGetJobs.ip, rGetJobs.port, sContext, sNamespace, cnJobId, STB$JOB.getJobParameter('FOLDERNAME', cnJobId), 'F', to_number(stimeout));
        IF sReturn != 'T' THEN
          ISR$TRACE.error('sReturn', sReturn, sCurrentName);
        END IF;
        FOR rGetNodes IN (SELECT cg.nodeid, cg.nodetype
                            FROM isr$custom$grouping cg, stb$contextmenu c, isr$tree$sessions ts
                           WHERE cg.nodetype = REPLACE (c.token, 'CAN_DISPLAY_')
                             AND c.confirm = 'J'
                             AND ts.selected = 'T'
                             AND ts.nodetype = cg.nodetype) LOOP
          sReturn := ISR$LOADER.updateTree(rGetJobs.ip, rGetJobs.port, sContext, sNamespace, Stb$util.getSystemParameter('loader.socketTimeout'), rGetNodes.nodetype, rGetNodes.nodeid, 'RIGHT');
          IF sReturn != 'T' THEN
            ISR$TRACE.error('sReturn', sReturn, sCurrentName);
          END IF;
        END LOOP;
      ELSE
        isr$trace.warn('not ok','IP und Port: ' || rGetJobs.ip || ':' || rGetJobs.port||' -> sReturn, see LOGCLOB', sCurrentName, sReturn);
        -- Due to OPPIS-54
        -- update isr$session set inactive = 'T' where ip = rGetJobs.ip and port = rGetJobs.port;
      END IF;

      if sReturn != 'T' then
        nSessionId := null;
        open cGetSessionId(rGetJobs.ip, rGetJobs.port);
        fetch cGetSessionId into nSessionId;
        close cGetSessionId;
        if trim(nSessionId) is not null then
          isr$trace.debug('delete session with sessionId=',nSessionId,sCurrentName);
          STB$GAL.deleteSessionInfo(nSessionId);
        else
         -- raise exInitFailed;
         isr$trace.warn('no session for',rGetJobs.ip || '  ' || rGetJobs.port,sCurrentName);
       end if;
     end if;
    end loop;
 end if;

  -- create a config record
  cxXml := isr$XML.InitXml('config');
  isr$XML.SetAttribute(cxXML, '/config', 'job-id', To_Char(cnJobId));
  isr$XML.CreateNode(cxXML, '/config', 'apps', '');

  isr$trace.debug('jobid',ISR$XML.valueOf(cxXML,'/config/@job-id'),sCurrentName);

  if STB$UTIL.getSystemParameter('NOTIFY') = 'T' then
    --oErrorObj := isr$MAIL.SendEmail(STB$JOB.getJobParameter('TOKEN', cnJobId),STB$JOB.getJobParameter('NODETYPE', cnJobId),cnJobid,'JOB_BEGIN');
    --oErrorObj := stb$job.sendMail('NOTIFY', cnJobId,'JOB_BEGIN');
    oParamListRec.AddParam('JOBID', TO_CHAR (cnJobId));
    --oParamListRec.AddParam('JOBID', TO_CHAR (0));
    oParamListRec.AddParam('NOTIFY_ON', 'JOB_BEGIN');

    oErrorObj := ISR$NOTIFY$PACKAGE.SENDMAIL(STB$JOB.getJobParameter('TOKEN', cnJobId), oParamListRec);

 end if;

  isr$trace.stat('end','end',sCurrentName);
  return oErrorObj;

exception
  when exInitFailed then
    sUserMsg := utd$msglib.getmsg ('exInitFailedText', nCurrentLanguage,  csP1 => sqlerrm(sqlcode));
    sImplMsg := sCurrentName || rtrim(': '||sImplMsg,': ');
    sDevMsg  := substr(dbms_utility.format_error_stack ||crlf|| dbms_utility.format_error_backtrace, 1,4000);
    oErrorObj.handleError ( Stb$typedef.cnSeverityCritical, sUserMsg, sImplMsg, sDevMsg );
    Stb$job.markDBJobAsBroken(cnJobId, sIp, sPort, oErrorObj.sErrorCode, oErrorObj.sErrorMessage, oErrorObj.sSeverityCode);
    return oErrorObj;
  when others then
    sUserMsg := utd$msglib.getmsg ('exUnexpectedError', nCurrentLanguage,  csP1 => sqlerrm(sqlcode));
    sImplMsg := sCurrentName || rtrim(': '||sImplMsg,': ');
    sDevMsg  := substr(dbms_utility.format_error_stack ||crlf|| dbms_utility.format_error_backtrace, 1,4000);
    oErrorObj.handleError ( Stb$typedef.cnSeverityCritical, sUserMsg, sImplMsg, sDevMsg );
    Stb$job.markDBJobAsBroken(cnJobId, sIp, sPort, oErrorObj.sErrorCode, oErrorObj.sErrorMessage, oErrorObj.sSeverityCode);
    return oErrorObj;
end initialiseJob;

--***********************************************************************************************************************
function terminateJob( cnJobid in     number,
                       cxXml   in out xmltype)
  return STB$OERROR$RECORD
IS
  sCurrentName   constant varchar2(100) := $$PLSQL_UNIT||'.terminateJob (JobID='||cnJobId||')';
  sUserMsg               varchar2(4000);
  sDevMsg                varchar2(4000);
  sImplMsg               varchar2(4000);
  oErrorObj              STB$OERROR$RECORD :=STB$OERROR$RECORD();
  nJavaRet               number;
  sReturn                varchar2(4000);
  nErrorLog              number;
  oParamListRec          ISR$PARAMLIST$REC := ISR$PARAMLIST$REC();
  sActionTaken           isr$log.actionTaken%type;
  sLogEntry              isr$log.logEntry%type;
  sFunctiongroup         isr$log.functiongroup%type;
  sWarnChar              varchar2(10) := '-' || chr(32);


  cursor cGetJob is
    select *
    from stb$jobsession
    where jobid = cnJobId;

  rGetJob cGetJob%ROWTYPE;

  cursor cGetUsersMail is
    select email
      from stb$user
     where userno = STB$JOB.getJobParameter('USER', cnJobId);

  sUsersMail    STB$USER.EMAIL%type;
begin
  isr$trace.stat('begin', 'Function '||sCurrentName||' started.', sCurrentName);

  open cGetJob;
  fetch cGetJob into rGetJob;

  if nvl(STB$JOB.getJobParameter('END_LOADER', cnJobId), 'T') = 'T' and cGetJob%FOUND then
  
    select count(*) into nErrorLog from isr$log where LOGLEVELNAME = 'ERROR'  AND REFERENCE  LIKE '%JOBID ' || cnJobId  ||'%';
    if nErrorLog > 0 then
     STB$JOB.setJobProgress( cnJobid,
                            Stb$typedef.cnReportProgress,
                            Utd$msglib.GetMsg ('LOADER_TEXT_WARNING', Stb$security.GetCurrentLanguage),
                            100 );
    else
     STB$JOB.setJobProgress( cnJobid,
                            Stb$typedef.cnReportProgress,
                            Utd$msglib.GetMsg ('LOADER_TEXT_SUCCESS', Stb$security.GetCurrentLanguage),
                            100 );
    end if;                                              

    -- send it to all clients
    for rGetJobSessions in ( select distinct ip, port
                        from (select ip, port
                                from STB$JOBSESSION
                               where jobid = cnJobid
                              union
                              select ip, port
                                from isr$session s, stb$jobqueue jq
                               where upper(s.username) = upper(STB$SECURITY.getOracleUsername(jq.userno))
                                 and jq.jobid = cnJobid
                                 and NVL(inactive, 'F') = 'F'
                                 and NVL(markedforshutdown, 'F') = 'F')
                       where trim (ip) is not null
                         and trim (port) is not null ) loop
      sReturn := ISR$LOADER.terminateJob(rGetJobSessions.ip, rGetJobSessions.port, sContext, sNamespace, cnJobId);
      if nErrorLog > 0 then
        sReturn := ISR$LOADER.showTerminateJobWarn(rGetJobSessions.ip, rGetJobSessions.port, sContext, sNamespace, cnJobId);
        for rGetWarn in (select actiontaken, logentry, functiongroup from isr$log where LOGLEVELNAME = 'ERROR'  AND REFERENCE  LIKE '%JOBID ' || cnJobId  ||'%') loop
          sActionTaken := substr(sActionTaken || chr(10) || sWarnChar ||rGetWarn.actiontaken, 0, 3999);
          sLogentry := substr(sLogentry || chr(10) || sWarnChar ||rGetWarn.logentry, 0, 3999);
          sFunctiongroup := substr(sFunctiongroup || chr(10) || sWarnChar ||rGetWarn.functiongroup, 0, 3999);
        end loop;
        stb$job.setJobParameter('JOB_WARN', sActionTaken, cnJobid);
        stb$job.setJobParameter('JOB_USERSTACK', sActionTaken, cnJobid);
        stb$job.setJobParameter('JOB_IMPLSTACK', sFunctiongroup, cnJobid);
        stb$job.setJobParameter('JOB_DEVSTACK', sLogentry, cnJobid);
      else
        sReturn := ISR$LOADER.showTerminateJobSuccess(rGetJobSessions.ip, rGetJobSessions.port, sContext, sNamespace, cnJobId);
      end if;

      if sReturn != 'T' then
        ISR$TRACE.error('sReturn', sReturn, sCurrentName);
      end if;

      for rGetNodes in (select cg.nodeid, cg.nodetype
                          from isr$custom$grouping cg, stb$contextmenu c, isr$tree$sessions ts
                         where cg.nodetype = replace (c.token, 'CAN_DISPLAY_')
                           and c.confirm = 'J'
                           and ts.selected = 'T'
                           and ts.nodetype = cg.nodetype) loop
        sReturn := ISR$LOADER.updateTree(rGetJobSessions.ip, rGetJobSessions.port, sContext, sNamespace,
                                        Stb$util.getSystemParameter('loader.socketTimeout'), rGetNodes.nodetype, rGetNodes.nodeid, 'RIGHT');
        if sReturn != 'T' then
          ISR$TRACE.error('sReturn', sReturn, sCurrentName);
        end if;
      end loop;

    end loop;

    if STB$UTIL.getSystemParameter('NOTIFY') = 'T' then
      --oErrorObj := isr$mail.SendEmail(STB$JOB.getJobParameter('TOKEN', cnJobId),STB$JOB.getJobParameter('NODETYPE', cnJobId),cnJobid,'JOB_END');
      --oErrorObj := stb$job.sendMail('NOTIFY', cnJobId,'JOB_END');
      oParamListRec.AddParam('JOBID', TO_CHAR (cnJobId));
      oParamListRec.AddParam('NOTIFY_ON', 'JOB_END');

      oErrorObj := ISR$NOTIFY$PACKAGE.SENDMAIL(STB$JOB.getJobParameter('TOKEN', cnJobId), oParamListRec);

    end if;

    if NVL(STB$JOB.getJobParameter('FILTER_PARA_DISPLAYED_NOTIFY', cnJobId), 'F') = 'T' then
      open cGetUsersMail;
     fetch cGetUsersMail into sUsersMail;
      if cGetUsersMail%FOUND then
        oErrorObj := stb$job.sendMail(sUsersMail, cnJobId);
     end if;
      close cGetUsersMail;
    end if;

    oErrorObj := Stb$job.terminateJob(cnJobId);

  end if;

  close cGetJob;

  isr$trace.stat('end', 'Function '||sCurrentName||' finished',sCurrentName);
  return oErrorObj;

exception
  when others then
    sUserMsg := utd$msglib.getmsg ('exUnexpectedError', nCurrentLanguage,  csP1 => sqlerrm(sqlcode));
    sImplMsg := sCurrentName || rtrim(': '||sImplMsg,': ');
    sDevMsg  := substr(dbms_utility.format_error_stack ||crlf|| dbms_utility.format_error_backtrace, 1,4000);
    oErrorObj.handleError ( Stb$typedef.cnSeverityCritical, sUserMsg, sImplMsg, sDevMsg );
    Stb$job.markDBJobAsBroken(cnJobId, sIp, sPort, oErrorObj.sErrorCode, oErrorObj.sErrorMessage, oErrorObj.sSeverityCode);
    return oErrorObj;
end terminateJob;

--*******************************************************************************************************************
function startCollector( cnJobid in     number,
                         cxXml   in out xmltype)
  return STB$OERROR$RECORD
is
  exCollectorError        exception;
  exCollectorDeprecated   exception;
  sCurrentName   constant varchar2(100) := $$PLSQL_UNIT||'.startCollector (JobID='||cnJobId||')';
  sUserMsg                varchar2(4000);
  sDevMsg                 varchar2(4000);
  sImplMsg                varchar2(4000);
  oErrorObj               STB$OERROR$RECORD :=STB$OERROR$RECORD();
  nRepiD                  STB$REPORT.REPID%type;
  nDocIdXML               STB$DOCTRAIL.DOCID%type := 0;
  nNewDocId               STB$DOCTRAIL.DOCID%type;

  -- cursor for final documents to retrieve the raw data xml file of the inspection version
  cursor cGetXMLInspection is
    select d.docid ,
         r.repid
    from STB$REPORT r,
       STB$DOCTRAIL d
   where r.repid = STB$JOB.getJobParameter('REPID', cnJobid)
     and r.parentrepid = d.nodeid
     and d.doctype = 'X';

  -- cursor to check if the xml-file already exists in the table stb$doctrail
  cursor cCheckXmlExists is
    select d.docid
      from STB$DOCTRAIL d
     where d.nodeid = STB$JOB.getJobParameter('REPID', cnJobid)
       and d.doctype = 'X';

begin
  Raise exCollectorDeprecated;

  isr$trace.stat('begin', 'Function '||sCurrentName||' started.', sCurrentName);

  -- check if the report has the status final (addionalparameter is 'FINAL') and the command is CAN_RUN_REPORT
  -- then do nothing -> don't create an xml file for word reports
  -- for fda reports copy the xml file from the previous version
  -- Word reports or fda reports
  if STB$JOB.getJobParameter('FINALFLAG', cnJobid) is not null then

  -- check if there is not already a xml file, this can becaused when for example the wordmodule fails
  -- before the file is written to the database
  -- check if XML-File exist then copy the xml file
    open  cCheckXmlExists;
    fetch cCheckXmlExists into nDocIdXML;
    close cCheckXmlExists;
    isr$trace.debug('nDocIdXML',nDocIdXML,sCurrentName);

    if nDocIdXML = 0 then
      open cGetXMLInspection;
     fetch cGetXMLInspection into nDocIdXML, nRepID;
      close cGetXMLInspection;
      isr$trace.debug('nDocIdXML/nRepID',nDocIdXML||'/'||nRepId, sCurrentName);
      oErrorObj:= STB$DOCKS.copyDocToDoc(nDocIdXML, nRepId, nNewDocId);
     isr$trace.debug('nNewDocId',nNewDocId,sCurrentName);
      return oErrorObj;
   end if;

 end if;

  -- check if XML-File exist then copy the xml file
  open cCheckXmlExists;
 fetch cCheckXmlExists into nDocIdXML;
  close cCheckXmlExists;
  isr$trace.debug('nDocIdXML',nDocIdXML,sCurrentName);

  if nDocIdXML = 0 then

    --> create raw data file
    execute immediate
      'DECLARE
        olocError STB$OERROR$RECORD;
      begin
        olocError:= '||STB$UTIL.getReptypeParameter(STB$JOB.getJobParameter('REPORTTYPEID',cnJobId), 'COLLECTORGROUP')||'(:1);
        :2 := olocError;
      end;'
    using in cnJobId, out oErrorObj;

  else

    -- XML exists in the table  STB$DOCTRAIL -> copy File into STB$JOBTRAIL and transform
    oErrorObj := Stb$docks.copyXMLToJob(nDocIdXML,cnJobId, csRohDaten);

 end if;

  if oErrorObj.sSeverityCode <> stb$typedef.cnNoError then
    STB$JOB.setJobParameter('ALERTERROR', 'T', cnJobid);
    raise exCollectorError;
 end if;

  isr$trace.stat('end', 'Function '||sCurrentName||' finished',sCurrentName);
  return oErrorObj;

exception
  when exCollectorError then
    sUserMsg := utd$msglib.getmsg ('exCollectorError', nCurrentLanguage,  csP1 => sqlerrm(sqlcode));
    sImplMsg := sCurrentName || rtrim(': '||sImplMsg,': ');
    sDevMsg  := substr(dbms_utility.format_error_stack ||crlf|| dbms_utility.format_error_backtrace, 1,4000);
    oErrorObj.handleError ( Stb$typedef.cnSeverityCritical, sUserMsg, sImplMsg, sDevMsg );
    Stb$job.markDBJobAsBroken(cnJobId, sIp, sPort, oErrorObj.sErrorCode, oErrorObj.sErrorMessage, oErrorObj.sSeverityCode);
    return oErrorObj;
  when exCollectorDeprecated then
    sUserMsg := utd$msglib.getmsg ('exCollectorDeprecated', nCurrentLanguage,  csP1 => sqlerrm(sqlcode));
    sImplMsg := sCurrentName || rtrim(': '||sImplMsg,': ');
    sDevMsg  := substr(dbms_utility.format_error_stack ||crlf|| dbms_utility.format_error_backtrace, 1,4000);
    oErrorObj.handleError ( Stb$typedef.cnSeverityCritical, sUserMsg, sImplMsg, sDevMsg );
    Stb$job.markDBJobAsBroken(cnJobId, sIp, sPort, oErrorObj.sErrorCode, oErrorObj.sErrorMessage, oErrorObj.sSeverityCode);
    return oErrorObj;

  when others then
    sUserMsg := utd$msglib.getmsg ('exUnexpectedError', nCurrentLanguage,  csP1 => sqlerrm(sqlcode));
    sImplMsg := sCurrentName || rtrim(': '||sImplMsg,': ');
    sDevMsg  := substr(dbms_utility.format_error_stack ||crlf|| dbms_utility.format_error_backtrace, 1,4000);
    oErrorObj.handleError ( Stb$typedef.cnSeverityCritical, sUserMsg, sImplMsg, sDevMsg );
    Stb$job.markDBJobAsBroken(cnJobId, sIp, sPort, oErrorObj.sErrorCode, oErrorObj.sErrorMessage, oErrorObj.sSeverityCode);
    return oErrorObj;
end startCollector;

--*******************************************************************************************************************
function copyFinalRawFile( cnJobid in     number,
                         cxXml   in out xmltype)
  return STB$OERROR$RECORD
is
  sCurrentName   constant varchar2(100) := $$PLSQL_UNIT||'.copyFinalRawFile (JobID='||cnJobId||')';
  sUserMsg                varchar2(4000);
  sDevMsg                 varchar2(4000);
  sImplMsg                varchar2(4000);
  oErrorObj               STB$OERROR$RECORD :=STB$OERROR$RECORD();
  nRepiD                  STB$REPORT.REPID%type;
  nDocIdXML               STB$DOCTRAIL.DOCID%type := 0;
  nNewDocId               STB$DOCTRAIL.DOCID%type;

  -- cursor for final documents to retrieve the raw data xml file of the inspection version
  cursor cGetXMLInspection is
    select d.docid ,
         r.repid
    from STB$REPORT r,
       STB$DOCTRAIL d
   where r.repid = STB$JOB.getJobParameter('REPID', cnJobid)
     and r.parentrepid = d.nodeid
     and d.doctype = 'X';

  -- cursor to check if the xml-file already exists in the table stb$doctrail
  cursor cCheckXmlExists is
    select d.docid
      from STB$DOCTRAIL d
     where d.nodeid = STB$JOB.getJobParameter('REPID', cnJobid)
       and d.doctype = 'X';

begin
  isr$trace.stat('begin', 'Function '||sCurrentName||' started.', sCurrentName);

  -- check if there is not already a xml file,
  open  cCheckXmlExists;
  fetch cCheckXmlExists into nDocIdXML;
  close cCheckXmlExists;
  isr$trace.debug('nDocIdXML',nDocIdXML,sCurrentName);

  -- then copy the xml file
  if nDocIdXML = 0 then
    open cGetXMLInspection;
    fetch cGetXMLInspection into nDocIdXML, nRepID;
    close cGetXMLInspection;
    isr$trace.debug('nDocIdXML/nRepID',nDocIdXML||'/'||nRepId, sCurrentName);
    oErrorObj:= STB$DOCKS.copyDocToDoc(nDocIdXML, nRepId, nNewDocId);
    isr$trace.debug('nNewDocId', nNewDocId, sCurrentName);
  end if;

  -- ??
  if oErrorObj.sSeverityCode <> stb$typedef.cnNoError then
    STB$JOB.setJobParameter('ALERTERROR', 'T', cnJobid);
    --raise exCollectorError;
    --??
  end if;

  isr$trace.stat('end', 'Function '||sCurrentName||' finished',sCurrentName);
  return oErrorObj;

exception
  when others then
    sUserMsg := utd$msglib.getmsg ('exUnexpectedError', nCurrentLanguage,  csP1 => sqlerrm(sqlcode));
    sImplMsg := sCurrentName || rtrim(': '||sImplMsg,': ');
    sDevMsg  := dbms_utility.format_error_stack ||crlf|| dbms_utility.format_error_backtrace;
    oErrorObj.handleError ( Stb$typedef.cnSeverityCritical, sUserMsg, sImplMsg, sDevMsg );
    Stb$job.markDBJobAsBroken(cnJobId, sIp, sPort, oErrorObj.sErrorCode, oErrorObj.sErrorMessage, oErrorObj.sSeverityCode);
    return oErrorObj;
end copyFinalRawFile;

--*******************************************************************************************************************
/*function copyFinalRawFile( cnJobid in     number,
                         cxXml   in out xmltype)
  return STB$OERROR$RECORD
is
  sCurrentName   constant varchar2(100) := $$PLSQL_UNIT||'.startCollector (JobID='||cnJobId||')';
  sUserMsg                varchar2(4000);
  sDevMsg                 varchar2(4000);
  sImplMsg                varchar2(4000);
  oErrorObj               STB$OERROR$RECORD :=STB$OERROR$RECORD();
  nRepiD                  STB$REPORT.REPID%type;
  nDocIdXML               STB$DOCTRAIL.DOCID%type := 0;
  nNewDocId               STB$DOCTRAIL.DOCID%type;

  -- cursor for final documents to retrieve the raw data xml file of the inspection version
  cursor cGetXMLInspection is
    select d.docid ,
         r.repid
    from STB$REPORT r,
       STB$DOCTRAIL d
   where r.repid = STB$JOB.getJobParameter('REPID', cnJobid)
     and r.parentrepid = d.nodeid
     and d.doctype = 'X';

  -- cursor to check if the xml-file already exists in the table stb$doctrail
  cursor cCheckXmlExists is
    select d.docid
      from STB$DOCTRAIL d
     where d.nodeid = STB$JOB.getJobParameter('REPID', cnJobid)
       and d.doctype = 'X';

begin
  isr$trace.info('begin', 'Function '||sCurrentName||' started.', sCurrentName);

  -- check if there is not already a xml file,
  open  cCheckXmlExists;
  fetch cCheckXmlExists into nDocIdXML;
  close cCheckXmlExists;
  isr$trace.debug('nDocIdXML',nDocIdXML,sCurrentName);

  -- then copy the xml file
  if nDocIdXML = 0 then
    open cGetXMLInspection;
    fetch cGetXMLInspection into nDocIdXML, nRepID;
    close cGetXMLInspection;
    isr$trace.debug('nDocIdXML/nRepID',nDocIdXML||'/'||nRepId, sCurrentName);
    oErrorObj:= STB$DOCKS.copyDocToDoc(nDocIdXML, nRepId, nNewDocId);
    isr$trace.debug('nNewDocId', nNewDocId, sCurrentName);
  end if;

  -- ??
  if oErrorObj.sSeverityCode <> stb$typedef.cnNoError then
    STB$JOB.setJobParameter('ALERTERROR', 'T', cnJobid);
    --raise exCollectorError;
    --??
  end if;

  isr$trace.info('end', 'Function '||sCurrentName||' finished',sCurrentName);
  return oErrorObj;

exception
  when others then
    sUserMsg := utd$msglib.getmsg ('exUnexpectedError', nCurrentLanguage,  csP1 => sqlerrm(sqlcode));
    sImplMsg := sCurrentName || rtrim(': '||sImplMsg,': ');
    sDevMsg  := dbms_utility.format_error_stack ||crlf|| dbms_utility.format_error_backtrace;
    oErrorObj.handleError ( Stb$typedef.cnSeverityCritical, sUserMsg, sImplMsg, sDevMsg );
    Stb$job.markDBJobAsBroken(cnJobId, sIp, sPort, oErrorObj.sErrorCode, oErrorObj.sErrorMessage, oErrorObj.sSeverityCode);
    return oErrorObj;
end copyFinalRawFile;*/

--*******************************************************************************************************************
function fillTableModel( cnJobid in     number,
                         cxXml   in out xmltype)
  return STB$OERROR$RECORD
is
  exCollectorError        exception; --??
  sCurrentName   constant varchar2(100) := $$PLSQL_UNIT||'.fillTableModel (JobID='||cnJobId||')';
  sUserMsg                varchar2(4000);
  sDevMsg                 varchar2(4000);
  sImplMsg                varchar2(4000);
  oErrorObj               STB$OERROR$RECORD :=STB$OERROR$RECORD();
  nDocIdXML               STB$DOCTRAIL.DOCID%type;

begin
  isr$trace.stat('begin', 'Function '||sCurrentName||' started.', sCurrentName);

  -- check if the rawdata XML-File exist. Then bypass this action
  if not XMLFileExists(cnJobid, 'X', nDocIdXML) then
    -- the file does not exist yet. So do the action
    oErrorObj := iSR$ReportData.fillTableModel(cnJobid);
  else
    isr$trace.warn('warning','XMLFileExists = TRUE, iSR$ReportData.fillTableModel('||cnJobid||') is not called', sCurrentName);
  end if;

  -- noch klaeren ob man das hier braucht
  if oErrorObj.sSeverityCode <> stb$typedef.cnNoError then
    STB$JOB.setJobParameter('ALERTERROR', 'T', cnJobid);
    raise exCollectorError;
  end if;

  isr$trace.stat('end', 'Function '||sCurrentName||' finished',sCurrentName);
  return oErrorObj;

exception
  when exCollectorError then
    sUserMsg := utd$msglib.getmsg ('exCollectorError', nCurrentLanguage,  csP1 => sqlerrm(sqlcode));
    sImplMsg := sCurrentName || rtrim(': '||sImplMsg,': ');
    sDevMsg  := substr(dbms_utility.format_error_stack ||crlf|| dbms_utility.format_error_backtrace, 1,4000);
    oErrorObj.handleError ( Stb$typedef.cnSeverityCritical, sUserMsg, sImplMsg, sDevMsg );
    Stb$job.markDBJobAsBroken(cnJobId, sIp, sPort, oErrorObj.sErrorCode, oErrorObj.sErrorMessage, oErrorObj.sSeverityCode);
    return oErrorObj;
  when others then
    sUserMsg := utd$msglib.getmsg ('exUnexpectedError', nCurrentLanguage,  csP1 => sqlerrm(sqlcode));
    sImplMsg := sCurrentName || rtrim(': '||sImplMsg,': ');
    sDevMsg  := substr(dbms_utility.format_error_stack ||crlf|| dbms_utility.format_error_backtrace, 1,4000);
    oErrorObj.handleError ( Stb$typedef.cnSeverityCritical, sUserMsg, sImplMsg, sDevMsg );
    Stb$job.markDBJobAsBroken(cnJobId, sIp, sPort, oErrorObj.sErrorCode, oErrorObj.sErrorMessage, oErrorObj.sSeverityCode);
    return oErrorObj;
end fillTableModel;


FUNCTION createRemoteSnapshot (cnJobid in NUMBER, cxXml IN OUT XMLTYPE) RETURN STB$OERROR$RECORD is

  exSnapshotError         exception; --??
  sCurrentName   constant varchar2(100) := $$PLSQL_UNIT||'.createRemoteSnapshot (JobID='||cnJobId||')';
  sUserMsg                varchar2(4000);
  sDevMsg                 varchar2(4000);
  sImplMsg                varchar2(4000);
  oErrorObj               STB$OERROR$RECORD :=STB$OERROR$RECORD();
  nDocIdXML               STB$DOCTRAIL.DOCID%type;

begin
  isr$trace.stat('begin', 'Function '||sCurrentName||' started.', sCurrentName);

  -- check if the snapshot XML-File exist. Then bypass this action
  if XMLFileExists(cnJobid, 'S', nDocIdXML) then
    -- Snapshot XML exists in the table  STB$DOCTRAIL -> copy File into STB$JOBTRAIL and go to next step
    -- as the DOCUMENTTYPE is not hardcoded in copyXMLToJob, it can be used also for snapshot files
    oErrorObj := Stb$docks.copyXMLToJob(nDocIdXML,cnJobId, csRemoteSnapshot);
  else
    -- the file does not exist yet. So do the action
    oErrorObj := iSR$RemoteSnapshot.createReportRemoteSnapshot(cnJobid);
  end if;

  -- noch klaeren ob man das hier braucht
  if oErrorObj.sSeverityCode <> stb$typedef.cnNoError then
    STB$JOB.setJobParameter('ALERTERROR', 'T', cnJobid);
    raise exSnapshotError;
  end if;

  isr$trace.stat('end', 'Function '||sCurrentName||' finished',sCurrentName);
  return oErrorObj;

exception
  when exSnapshotError then
    sUserMsg := utd$msglib.getmsg ('exSnapshotError', nCurrentLanguage,  csP1 => sqlerrm(sqlcode));
    sImplMsg := sCurrentName || rtrim(': '||sImplMsg,': ');
    sDevMsg  := substr(dbms_utility.format_error_stack ||crlf|| dbms_utility.format_error_backtrace, 1,4000);
    oErrorObj.handleError ( Stb$typedef.cnSeverityCritical, sUserMsg, sImplMsg, sDevMsg );
    Stb$job.markDBJobAsBroken(cnJobId, sIp, sPort, oErrorObj.sErrorCode, oErrorObj.sErrorMessage, oErrorObj.sSeverityCode);
    return oErrorObj;
  when others then
    sUserMsg := utd$msglib.getmsg ('exUnexpectedError', nCurrentLanguage,  csP1 => sqlerrm(sqlcode));
    sImplMsg := sCurrentName || rtrim(': '||sImplMsg,': ');
    sDevMsg  := substr(dbms_utility.format_error_stack ||crlf|| dbms_utility.format_error_backtrace, 1,4000);
    oErrorObj.handleError ( Stb$typedef.cnSeverityCritical, sUserMsg, sImplMsg, sDevMsg );
    Stb$job.markDBJobAsBroken(cnJobId, sIp, sPort, oErrorObj.sErrorCode, oErrorObj.sErrorMessage, oErrorObj.sSeverityCode);
    return oErrorObj;
end createRemoteSnapshot;

--*******************************************************************************************************************
function createRawDatFile( cnJobid in     number,
                         cxXml   in out xmltype)
  return STB$OERROR$RECORD
is
  exRawDataError          exception; --??
  sCurrentName   constant varchar2(100) := $$PLSQL_UNIT||'.createRawDatFile (JobID='||cnJobId||')';
  sUserMsg                varchar2(4000);
  sDevMsg                 varchar2(4000);
  sImplMsg                varchar2(4000);
  oErrorObj               STB$OERROR$RECORD :=STB$OERROR$RECORD();
  nDocIdXML               STB$DOCTRAIL.DOCID%type;

 begin
  isr$trace.stat('begin', 'Function '||sCurrentName||' started.', sCurrentName);

  -- check if the rawdata XML-File exist. Then bypass this action
  if XMLFileExists(cnJobid, 'X', nDocIdXML) then
    -- XML exists in the table  STB$DOCTRAIL -> copy File into STB$JOBTRAIL and go to next step
    oErrorObj := Stb$docks.copyXMLToJob(nDocIdXML,cnJobId, csRohDaten);
  else
    -- the file does not exist yet. So do the action
    --oErrorObj := iSR$ReportData.fillTableModel(cnJobid);
    oErrorObj := iSR$RawData.createRawDatFile(cnJobid);
  end if;

  -- noch klaeren ob man das hier braucht
  if oErrorObj.sSeverityCode <> stb$typedef.cnNoError then
    STB$JOB.setJobParameter('ALERTERROR', 'T', cnJobid);
    raise exRawDataError;
  end if;

  isr$trace.stat('end', 'Function '||sCurrentName||' finished',sCurrentName);
  return oErrorObj;

exception
  when exRawDataError then
    sUserMsg := utd$msglib.getmsg ('exRawDataError', nCurrentLanguage,  csP1 => sqlerrm(sqlcode));
    sImplMsg := sCurrentName || rtrim(': '||sImplMsg,': ');
    sDevMsg  := substr(dbms_utility.format_error_stack ||crlf|| dbms_utility.format_error_backtrace, 1,4000);
    oErrorObj.handleError ( Stb$typedef.cnSeverityCritical, sUserMsg, sImplMsg, sDevMsg );
    Stb$job.markDBJobAsBroken(cnJobId, sIp, sPort, oErrorObj.sErrorCode, oErrorObj.sErrorMessage, oErrorObj.sSeverityCode);
    return oErrorObj;
  when others then
    sUserMsg := utd$msglib.getmsg ('exUnexpectedError', nCurrentLanguage,  csP1 => sqlerrm(sqlcode));
    sImplMsg := sCurrentName || rtrim(': '||sImplMsg,': ');
    sDevMsg  := substr(dbms_utility.format_error_stack ||crlf|| dbms_utility.format_error_backtrace, 1,4000);
    oErrorObj.handleError ( Stb$typedef.cnSeverityCritical, sUserMsg, sImplMsg, sDevMsg );
    Stb$job.markDBJobAsBroken(cnJobId, sIp, sPort, oErrorObj.sErrorCode, oErrorObj.sErrorMessage, oErrorObj.sSeverityCode);
    return oErrorObj;
end createRawDatFile;

--***********************************************************************************************************************
function callActionFunction( cnJobid in     number,
                             cxXml   in out xmltype)
  return STB$OERROR$RECORD
is
  sCurrentName   constant varchar2(100) := $$PLSQL_UNIT||'.callActionFunction(JobID='||cnJobId||')';
  sUserMsg               varchar2(4000);
  sDevMsg                varchar2(4000);
  sImplMsg               varchar2(4000);
  oErrorObj               STB$OERROR$RECORD :=STB$OERROR$RECORD();
  sStatement STB$JOBPARAMETER.PARAMETERVALUE%type;
begin
  isr$trace.stat('begin','begin', sCurrentName);
  isr$trace.debug('value','cxXml',sCurrentName,cxXml);

  if trim(STB$JOB.getJobParameter('FUNCTION', cnJobID)) is not null then

    sStatement := 'BEGIN '
                ||'  :1 := '||STB$JOB.getJobParameter('FUNCTION', cnJobID)||'(:2, :3); '
                ||'  END;';

    isr$trace.debug('sStatement',sStatement,sCurrentName);

    execute immediate sStatement using out oErrorObj, in cnJobId, in out cxXML;

    if oErrorObj.sSeverityCode != stb$typedef.cnNoError then
      Stb$job.markDBJobAsBroken(cnJobId, sIp, sPort, oErrorObj.sErrorCode, oErrorObj.sErrorMessage, oErrorObj.sSeverityCode);
   end if;

 end if;

  isr$trace.debug('value','cxXml',sCurrentName,cxXml);
  isr$trace.stat('end','end',sCurrentName);
  return oErrorObj;

exception
  when others then
    sUserMsg := utd$msglib.getmsg ('exUnexpectedError', nCurrentLanguage,  csP1 => sqlerrm(sqlcode));
    sImplMsg := sCurrentName || rtrim(': '||sImplMsg,': ');
    sDevMsg  := substr(dbms_utility.format_error_stack ||crlf|| dbms_utility.format_error_backtrace, 1,4000);
    oErrorObj.handleError ( Stb$typedef.cnSeverityCritical, sUserMsg, sImplMsg, sDevMsg );
    Stb$job.markDBJobAsBroken(cnJobId, sIp, sPort, oErrorObj.sErrorCode, oErrorObj.sErrorMessage, oErrorObj.sSeverityCode);
    return oErrorObj;
end callActionFunction;

--***********************************************************************************************************************
function startApp( cnJobid in     number,
                   cxXml   in out xmltype)
  return STB$OERROR$RECORD
is
  sCurrentName   constant varchar2(100) := $$PLSQL_UNIT||'.startApp('||cnJobId||')';
  sUserMsg               varchar2(4000);
  sDevMsg                varchar2(4000);
  sImplMsg               varchar2(4000);
  oErrorObj               STB$OERROR$RECORD :=STB$OERROR$RECORD();
  nJavaRet                   number;
  exApplicationError         exception;
  exFileExistsError          exception;
  exUserCanceledJobError     exception;
  exPDFCreationCanceledError exception;

  xXml                    xmltype;

  sJobSequence            STB$JOBTRAIL.ENTRYID%type;
  sclDom                  STB$JOBTRAIL.TEXTFILE%type;

  bWait                  varchar2(50);
  sDocOid                 STB$DOCTRAIL.DOC_OID%type;

  /*cursor cGetEntryIdConfig is
    select jt.entryid
    from STB$JOBTRAIL jt
   where jt.jobid = cnJobId
     and jt.documenttype = csConfig;*/

  cursor cGetUserData(cnUserNo in STB$USER.USERNO%type) is
    select applicationpathes
    from   STB$USER
    where  userno = cnUserNo;

  rGetUserData cGetUserData%ROWTYPE;

  nModified              number := 0;
  sApplicationPathes     STB$USER.APPLICATIONPATHES%type;

  cursor cGetRunningRSApplication is
    select count ( * ), progress
      from stb$jobparameter jp, stb$jobsession js1, stb$jobsession js2, stb$jobqueue jq
     where jp.parametername = 'EXECUTE_APP'
       and jp.parametervalue = 'T'
       and jp.jobid = js1.jobid
       and js1.ip = js2.ip
       and js1.port = js2.port
       and js2.jobid = cnJobid
       and js2.jobid = jq.jobid
     group by progress;

  nCntJobs      number := 0;
  nProgress     number := 0;
  sOldProgress varchar2(4000);
  sReturn      clob;
begin
  isr$trace.stat('begin', 'begin', sCurrentName);
  isr$trace.debug('value','cxXml IN OUT',sCurrentName,cxXml);

  select ip, To_Char(js.port), nvl(so.timeout, STB$UTIL.getSystemParameter('TIMEOUT_LOADER_THREAD'))
  into sIp, sPort, sTimeout
  from STB$JOBSESSION js, isr$serverobserver$v so
  where js.ip = so.observer_host (+)
  and JS.PORT = so.port (+)
  and jobid = cnJobId;


  if TRIM(STB$JOB.getJobParameter('MAX_RS_JOBS', cnJobId)) is not null then
    begin
      select statustext into sOldProgress from STB$JOBQUEUE where jobid = cnJobId;
    exception when others then
      sOldProgress := null;
    end;
    open cGetRunningRSApplication;
   fetch cGetRunningRSApplication into nCntJobs, nProgress;
    if cGetRunningRSApplication%NOTFOUND then
      nCntJobs := 0;
   end if;
    close cGetRunningRSApplication;
    while nCntJobs >= STB$JOB.getJobParameter('MAX_RS_JOBS', cnJobId) loop
      open cGetRunningRSApplication;
     fetch cGetRunningRSApplication into nCntJobs, nProgress;
      if cGetRunningRSApplication%NOTFOUND then
        nCntJobs := 0;
     end if;
      close cGetRunningRSApplication;
      STB$JOB.setProgress(nProgress, Utd$msglib.GetMsg ('WAITING_FOR_RENDERING_SERVER_APPLICATIONS', Stb$security.GetCurrentLanguage), cnJobId);
    end loop;
    STB$JOB.setJobParameter('EXECUTE_APP', 'T', cnJobId);
    if TRIM(sOldProgress) is not null then
      STB$JOB.setProgress(nProgress, sOldProgress, cnJobId);
   end if;
 end if;

  isr$trace.debug('values','sIp: '||sIp||',  sPort: '||sPort||',  sContext: '||sContext||',  sNamespace: '||sNamespace||',  cnJobid: '||cnJobid, sCurrentName);
  isr$trace.debug('values','Stb$util.getSystemParameter(''TIMEOUT_LOADER_THREAD''): '||Stb$util.getSystemParameter('TIMEOUT_LOADER_THREAD'), sCurrentName);
  sReturn := ISR$LOADER.startApp(sIp, sPort, sContext, sNamespace, cnJobid, Stb$util.getSystemParameter('TIMEOUT_LOADER_THREAD'));
  /*IF sReturn != 'T' THEN
    ISR$TRACE.error('sReturn', sReturn, sCurrentName);
    sUserMsg := sReturn;
    raise exApplicationError;
  END IF;*/
  if sReturn is not null then
      isr$trace.warn('exApplicationError', 'exApplicationError', sCurrentName, sReturn);
      sUserMsg := utd$msglib.getmsg('exApplicationErrorText', nCurrentLanguage, sIp || ' ' || sPort );
      oErrorObj.handleJavaError( Stb$typedef.cnSeverityCritical,  sReturn, sCurrentName );
      raise exApplicationError;
  end if;

  if TRIM(STB$JOB.getJobParameter('MAX_RS_JOBS', cnJobId)) is not null then
    STB$JOB.setJobParameter('EXECUTE_APP', 'F', cnJobId);
  end if;

  -- 3. get the configFile (here config.xml)  
  sReturn := ISR$LOADER.loadFileFromLoader(sIp, sPort, sContext, sNameSpace, cnJobId, 'config.xml', 'LOADER_CLOB_FILE', csConfig, sJavaLogLevel, isr$trace.getReference, to_number(stimeout));
  IF sReturn != 'T' THEN
    ISR$TRACE.error('sReturn', sReturn, sCurrentName);
  END IF;
  if sReturn != 'T' then
    isr$trace.warn('warning config file load', 'Could not load config file from loader to write the applicationpathes'||crlf||'sReturn: '||sReturn, sCurrentName);
  else

    select textfile into sclDom
    from   STB$JOBTRAIL
    where  jobid = cnjobid
    and    documenttype = csConfig;

    xXml := isr$XML.ClobToXml(sclDom);
    sApplicationPathes := isr$XML.valueOf(xXml, '/config/apps/applicationpathes[last()]/text()');
    isr$trace.debug('status sApplicationPathes', 'sApplicationPathes: '||sApplicationPathes, sCurrentName);

    open  cGetUserData(STB$JOB.getJobParameter('USER', cnJobid));
    fetch cGetUserData into rGetUserData;
    close cGetUserData;

    nModified := 0;
    oErrorObj := Stb$audit.Openaudit ('USERAUDIT', STB$JOB.getJobParameter('USER', cnJobid));

    update stb$user
    set applicationpathes = sApplicationPathes
    where sApplicationPathes is not null and (applicationpathes is null or sApplicationPathes != applicationpathes)
    and userno = STB$JOB.getJobParameter('USER', cnJobid);

    if sApplicationPathes is not null
    and (rGetUserData.applicationpathes is null or sApplicationPathes != rGetUserData.applicationpathes) then
      nModified := nModified + 1;
      isr$trace.debug('status modified', 'nModified: '||nModified, sCurrentName);
      if nModified = 1 then
        oErrorObj := Stb$audit.Addauditrecord ('CHANGE');
      end if;
      oErrorObj := Stb$audit.Addauditentry ('APPLICATIONPATHES', rGetUserData.applicationpathes, sApplicationPathes, nModified);
    end if;

    oErrorObj := Stb$audit.Silentaudit;
    commit;

  end if;

  isr$trace.debug('value','cxXml IN OUT',sCurrentName,cxXml);
  isr$trace.stat('end','end',sCurrentName);
  return STB$OERROR$RECORD();

exception
  when exApplicationError then
    /*sUserMsg := utd$msglib.getmsg ('exApplicationErrorText', nCurrentLanguage,  csP1 => sqlerrm(sqlcode)) ||crlf|| sUserMsg;
    sImplMsg := sCurrentName || rtrim(': '||sImplMsg,': ');
    sDevMsg  := substr(dbms_utility.format_error_stack ||crlf|| dbms_utility.format_error_backtrace, 1,4000);
    oErrorObj.handleError ( Stb$typedef.cnSeverityCritical, sUserMsg, sImplMsg, sDevMsg );*/
    Stb$job.markDBJobAsBroken(cnJobId, sIp, sPort, oErrorObj.sErrorCode, oErrorObj.sErrorMessage, oErrorObj.sSeverityCode);
    return oErrorObj;
  when others then
    sUserMsg := utd$msglib.getmsg ('exUnexpectedError', nCurrentLanguage,  csP1 => sqlerrm(sqlcode));
    sImplMsg := sCurrentName || rtrim(': '||sImplMsg,': ');
    sDevMsg  := substr(dbms_utility.format_error_stack ||crlf|| dbms_utility.format_error_backtrace, 1,4000);
    oErrorObj.handleError ( Stb$typedef.cnSeverityCritical, sUserMsg, sImplMsg, sDevMsg );
    Stb$job.markDBJobAsBroken(cnJobId, sIp, sPort, oErrorObj.sErrorCode, oErrorObj.sErrorMessage, oErrorObj.sSeverityCode);
    return oErrorObj;
end startApp;

--***********************************************************************************************************************
function loadCommonFromClient( cnJobid in     number,
                               cxXml   in out xmltype )
  return STB$OERROR$RECORD
is
  exLoadFileError        exception;
  sCurrentName   constant varchar2(200) := $$PLSQL_UNIT||'.loadCommonFromClient (JobID='||cnJobId||')';
  sUserMsg               varchar2(4000);
  sDevMsg                varchar2(4000);
  sImplMsg               varchar2(4000);
  oErrorObj               STB$OERROR$RECORD :=STB$OERROR$RECORD();
  sReturn                varchar2(4000);
  sFileName            STB$JOBTRAIL.FILENAME%type;
begin
  isr$trace.stat('begin', 'Function '||sCurrentName||' started.', sCurrentName);


  sFileName := STB$JOB.getJobParameter('TO', cnJobId);
  isr$trace.debug('sFileName', sFileName, sCurrentName);
  sReturn := ISR$LOADER.loadFileFromLoader(sIp, sPort, sContext, sNameSpace, cnJobId, sFileName, 'LOADER_BLOB_FILE', csTarget, sJavaLogLevel, isr$trace.getReference, to_number(stimeout));
  IF sReturn != 'T' THEN
    ISR$TRACE.error('sReturn', sReturn, sCurrentName);
    raise exLoadFileError;
  end if;

  isr$trace.debug_all('after load', 'after load', sCurrentName);
  isr$trace.debug('getFileName(sFileName)', getFileName(sFileName), sCurrentName);

   update STB$JOBTRAIL
     set docsize = DBMS_LOB.GETLENGTH(BINARYFILE),
      mimetype = csMimeType,
      documenttype = csTarget,
      blobflag = 'B',
      leadingdoc = 'T',
      download = 'T',
      filename = getFileName(sFileName)
   where jobid = cnJobId
   and documenttype = csTarget
   and filename = sFileName;

  isr$trace.debug_all('after update', 'after update', sCurrentName);
  commit;

  isr$trace.stat('end', 'Function '||sCurrentName||' finished',sCurrentName);
  return oErrorObj;

exception
  when exLoadFileError then
    sUserMsg := utd$msglib.getmsg ('exLoadFileError', nCurrentLanguage,  csP1 => sqlerrm(sqlcode), csP2 => '(ISR$LOADER.loadFileFromLoader failed)');
    sImplMsg := sCurrentName || rtrim(': '||sImplMsg,': ');
    sDevMsg  := substr(dbms_utility.format_error_stack ||crlf|| dbms_utility.format_error_backtrace, 1,4000);
    oErrorObj.handleError ( Stb$typedef.cnSeverityCritical, sUserMsg, sImplMsg, sDevMsg );
    Stb$job.markDBJobAsBroken(cnJobId, sIp, sPort, oErrorObj.sErrorCode, oErrorObj.sErrorMessage, oErrorObj.sSeverityCode);
    return oErrorObj;
  when others then
    sUserMsg := utd$msglib.getmsg ('exUnexpectedError', nCurrentLanguage,  csP1 => sqlerrm(sqlcode));
    sImplMsg := sCurrentName || rtrim(': '||sImplMsg,': ');
    sDevMsg  := substr(dbms_utility.format_error_stack ||crlf|| dbms_utility.format_error_backtrace, 1,4000);
    oErrorObj.handleError ( Stb$typedef.cnSeverityCritical, sUserMsg, sImplMsg, sDevMsg );
    Stb$job.markDBJobAsBroken(cnJobId, sIp, sPort, oErrorObj.sErrorCode, oErrorObj.sErrorMessage, oErrorObj.sSeverityCode);
    return oErrorObj;
end loadCommonFromClient;

--***********************************************************************************************************************
function loadFilesFromClient( cnJobid in     number,
                              cxXml   in out xmltype)
  return STB$OERROR$RECORD
is
  exLoadFileError         exception;
  sCurrentName   constant varchar2(100) := $$PLSQL_UNIT||'.loadFilesFromClient (JobID='||cnJobId||')';
  sUserMsg                varchar2(4000);
  sDevMsg                 varchar2(4000);
  sImplMsg                varchar2(4000);
  oErrorObj               STB$OERROR$RECORD :=STB$OERROR$RECORD();
  sReturn                 varchar2(4000);
  xXml                    xmltype;
  sTargetType             varchar2(1);
  sFileName               STB$DOCTRAIL.FILENAME%type;
  sMimetype               STB$DOCTRAIL.MIMETYPE%type;
  sIsrDocType             STB$JOBTRAIL.DOCUMENTTYPE%type;
  sDocDefinition          STB$DOCTRAIL.DOC_DEFINITION%type;
  --sJobSequence            STB$JOBTRAIL.ENTRYID%type;
  sclDom                  STB$JOBTRAIL.TEXTFILE%type;
  sReference            STB$JOBTRAIL.REFERENCE%type;

  /*cursor cGetEntryIdConfig is
    select jt.entryid
    from   STB$JOBTRAIL jt
    where  jt.jobid = cnJobId
    and    jt.documenttype = csConfig;*/

begin
  isr$trace.stat('begin', 'Function '||sCurrentName||' started.', sCurrentName);

  /*open  cGetEntryIdConfig;
  fetch cGetEntryIdConfig into sJobSequence;
  close cGetEntryIdConfig;
  isr$trace.debug('status sJobSequence', 'sJobSequence: '||sJobSequence, sCurrentName);*/

  sFileName := 'config.xml';
  sReturn := ISR$LOADER.loadFileFromLoader(sIp, sPort, sContext, sNameSpace, cnJobId, sFileName, 'LOADER_BLOB_FILE', csConfig,  sJavaLogLevel, isr$trace.getReference, to_number(stimeout));
  IF sReturn != 'T' THEN
    ISR$TRACE.error('sReturn', sReturn, sCurrentName);
  END IF;
  if sReturn != 'T' then
    raise exLoadFileError;
  end if;

  select textfile into sclDom
  from   STB$JOBTRAIL
  where  jobid = cnjobid
  and    documenttype = csConfig;
  isr$trace.debug('status sclDom', 'DBMS_LOB.GETLENGTH(sclDom): '||DBMS_LOB.GETLENGTH(sclDom), sCurrentName, 'sclDom: '||sclDom);

  xXml := isr$XML.ClobToXml(sclDom);

  -- get the target files
  -- if new files where created depends on the action/command
  for i in 1..ISR$XML.valueOf(xXml, '/config/apps/@num') loop

    -- over the config xml file get the targets and create an entry in the jobtrail
    sTargetType := isr$XML.valueOf(xXml, '/config/apps/app[@order="' || i || '"]/targettype/text()');
    sFileName   := isr$XML.valueOf(xXml, '/config/apps/app[@order="' || i ||'"]/target/text()');
    sMimetype   := isr$XML.valueOf(xXml, '/config/apps/app[@order="' || i ||'"]/mimetype/text()');
    sIsrDocType := isr$XML.valueOf(xXml, '/config/apps/app[@order="' || i ||'"]/isrdoctype/text()');
    sDocDefinition := isr$XML.valueOf(xXml, '/config/apps/app[@order="' || i ||'"]/doc_definition/text()');
    if upper(sDocDefinition) like '%PDF%' then
      if STB$JOB.getJobParameter('NODETYPE', cnJobId) = 'COMMONDOC' then
        sReference := STB$JOB.getJobParameter('DOCID', cnJobId);
      end if;
      if upper(sFilename) not like '%.PDF' then
        sFileName := sFileName||'.'||sDocDefinition;
      end if;
    else
      sReference := STB$JOB.getJobParameter('REPID', cnJobId);
    end if;

    --select STB$JOBTRAIL$SEQ.NEXTVAL into sJobSequence from dual;
    --isr$trace.debug('status sJobSequence', 'sJobSequence: '||sJobSequence, sCurrentName);

    if upper(sDocDefinition) like '%PDF%' then
      stb$job.setjobparameter ('DOC_DEFINITION_PDF', case when upper(sFilename) not like '%.PDF' then sFileName||'.'||sDocDefinition else sFileName end, cnJobId);
   end if;

   /* insert into STB$JOBTRAIL (
       ENTRYID,
       JOBID,
       REFERENCE,
       DOCUMENTTYPE,
       MIMETYPE,
       TIMESTAMP,
       BINARYFILE,
       TEXTFILE,
       FILENAME,
       BLOBFLAG,
       LEADINGDOC,
       DOWNLOAD,
       DOC_DEFINITION)
    values (
       To_Number(sJobSequence),
       cnJobId,
       case when upper(sDocDefinition) like '%PDF%' then
         case when STB$JOB.getJobParameter('NODETYPE', cnJobId) = 'COMMONDOC' then
           STB$JOB.getJobParameter('DOCID', cnJobId)
           else null
         end
         else STB$JOB.getJobParameter('REPID', cnJobId)
       end,
       sIsrDocType,
       sMimeType,
        sysdate ,
       Empty_Blob(),
       Empty_Clob(),
       case when upper(sDocDefinition) like '%PDF%' and upper(sFilename) not like '%.PDF' then sFileName||'.'||sDocDefinition else sFileName end,
       sTargetType,
       'T',
       'T',
       sDocDefinition
     );
     commit;*/


    isr$trace.debug('status sFileName/sTargetType','sFileName/sTargetType: '||sFileName||'/'||sTargetType, sCurrentName);

    if sTargetType = 'C' then

      sReturn := ISR$LOADER.loadFileFromLoader(sIp, sPort, sContext, sNameSpace, cnJobId, sFileName, 'LOADER_CLOB_FILE', sIsrDocType, sJavaLogLevel, isr$trace.getReference, to_number(stimeout));
      IF sReturn != 'T' THEN
        ISR$TRACE.error('sReturn', sReturn, sCurrentName);
      END IF;
      if sReturn != 'T' then
        raise exLoadFileError;
     end if;


    elsif sTargetType = 'B' then

      sReturn := ISR$LOADER.loadFileFromLoader(sIp, sPort, sContext, sNameSpace, cnJobId, sFileName, 'LOADER_BLOB_FILE', sIsrDocType, sJavaLogLevel, isr$trace.getReference, to_number(stimeout));
      IF sReturn != 'T' THEN
        ISR$TRACE.error('sReturn', sReturn, sCurrentName);
        raise exLoadFileError;
     end if;

   end if;

  update STB$JOBTRAIL
     set docsize = case when sTargetType = 'C' then DBMS_LOB.GETLENGTH(TEXTFILE)
                        when sTargetType = 'B' then DBMS_LOB.GETLENGTH(BINARYFILE) end,
      reference = sReference,
      mimetype = sMimeType,
      blobflag = sTargetType,
      leadingdoc = 'T',
      download = 'T',
      doc_definition = sDocDefinition
   where jobid = cnJobId
   and documenttype = sIsrDocType
   and filename = sFileName;

  end loop;

  commit;

  isr$trace.stat('end', 'Function '||sCurrentName||' finished',sCurrentName);
  return oErrorObj;

exception
  when exLoadFileError then
    sUserMsg := utd$msglib.getmsg ('exLoadFileError', nCurrentLanguage,  csP1 => sqlerrm(sqlcode), csP2 => crlf||'ISR$LOADER.loadFileFromLoader failed', csP3 => crlf||'filename: '||sFileName);
    sImplMsg := sCurrentName || rtrim(': '||sImplMsg,': ');
    sDevMsg  := substr(dbms_utility.format_error_stack ||crlf|| dbms_utility.format_error_backtrace, 1,4000);
    oErrorObj.handleError ( Stb$typedef.cnSeverityCritical, sUserMsg, sImplMsg, sDevMsg );
    Stb$job.markDBJobAsBroken(cnJobId, sIp, sPort, oErrorObj.sErrorCode, oErrorObj.sErrorMessage, oErrorObj.sSeverityCode);
    return oErrorObj;
  when others then
    sUserMsg := utd$msglib.getmsg ('exUnexpectedError', nCurrentLanguage,  csP1 => sqlerrm(sqlcode));
    sImplMsg := sCurrentName || rtrim(': '||sImplMsg,': ');
    sDevMsg  := substr(dbms_utility.format_error_stack ||crlf|| dbms_utility.format_error_backtrace, 1,4000);
    oErrorObj.handleError ( Stb$typedef.cnSeverityCritical, sUserMsg, sImplMsg, sDevMsg );
    Stb$job.markDBJobAsBroken(cnJobId, sIp, sPort, oErrorObj.sErrorCode, oErrorObj.sErrorMessage, oErrorObj.sSeverityCode);
    return oErrorObj;
end loadFilesFromClient;

--***********************************************************************************************************************
function loadFilesFromClientAfterError( cnJobid     in number,
                                        csFileNames in varchar2)
  return STB$OERROR$RECORD
is
  sCurrentName  constant varchar2(100) := $$PLSQL_UNIT||'.loadFilesFromClientAfterError (JobID='||cnJobId||')';
  sUserMsg               varchar2(4000);
  sDevMsg                varchar2(4000);
  sImplMsg               varchar2(4000);
  oErrorObj              STB$OERROR$RECORD :=STB$OERROR$RECORD();
  sReturn                varchar2(4000);
  sFileNames             varchar2(4000);
  sFileName              STB$DOCTRAIL.FILENAME%type;
  sEntryId               isr$log.ENTRYID%type;

begin
  isr$trace.stat('begin','Function '||sCurrentName||' started','begin',sCurrentName);

  sFileNames := csFileNames;

  -- get allthe target files
  while (Instr(sFileNames, ', ') != 0) loop

    sFileName  := Substr(sFileNames, 0, Instr(sFileNames, ',' ) - 1);
    sFileNames := Substr(sFileNames,    Instr(sFileNames, ', ') + 2);
    isr$trace.debug('status sFileName', 'sFileName: '||sFileName, sCurrentName);
    isr$trace.debug('status sFileNames', 'sFileNames: '||sFileNames, sCurrentName);

    sReturn := ISR$LOADER.loadFileFromLoader(sIp, sPort, sContext, sNameSpace, cnJobId, sFileName, 'LOADER_BLOB_FILE', null, sJavaLogLevel, isr$trace.getReference, to_number(stimeout));
    IF sReturn != 'T' THEN
      ISR$TRACE.error('sReturn', sReturn, sCurrentName);
    END IF;
    if sReturn != 'T' then
      isr$trace.debug('status sFilename sReturn','sFilename: '||sFilename||'  sReturn: '||sReturn, sCurrentName);
    end if;

  end loop;

  commit;

  isr$trace.stat('end', 'Function '||sCurrentName||' finished',sCurrentName);
  return oErrorObj;

exception
  when others then
    sUserMsg := utd$msglib.getmsg ('exUnexpectedError', nCurrentLanguage,  csP1 => sqlerrm(sqlcode));
    sImplMsg := sCurrentName || rtrim(': '||sImplMsg,': ');
    sDevMsg  := substr(dbms_utility.format_error_stack ||crlf|| dbms_utility.format_error_backtrace, 1,4000);
    oErrorObj.handleError ( Stb$typedef.cnSeverityCritical, sUserMsg, sImplMsg, sDevMsg );
    return oErrorObj;
end loadFilesFromClientAfterError;

--***********************************************************************************************************************
function loadFileFromClient( cnJobid in     number,
                             cxXml   in out xmltype)
  return STB$OERROR$RECORD
is
  exLoadFileError         exception;
  sCurrentName   constant varchar2(100) := $$PLSQL_UNIT||'.loadFileFromClient (JobID='||cnJobId||')';
  sUserMsg                varchar2(4000);
  sDevMsg                 varchar2(4000);
  sImplMsg                varchar2(4000);
  oErrorObj               STB$OERROR$RECORD :=STB$OERROR$RECORD();
  sReturn                 varchar2(4000);
  sFileName               STB$JOBTRAIL.FILENAME%type;
begin
  isr$trace.stat('begin', 'Function '||sCurrentName||' started.', sCurrentName);


  sFileName := STB$JOB.getJobParameter('TO', cnJobId);
  sReturn := ISR$LOADER.loadFileFromLoader(sIp, sPort, sContext, sNameSpace, cnJobId, sFileName, 'LOADER_BLOB_FILE', csTarget, sJavaLogLevel, isr$trace.getReference, to_number(stimeout));
  IF sReturn != 'T' THEN
    ISR$TRACE.error('sReturn', sReturn, sCurrentName);
    raise exLoadFileError;
  end if;

  isr$trace.debug('sFileName', sFileName, sCurrentName);
  isr$trace.debug('getFileName(sFileName)', getFileName(sFileName), sCurrentName);

  update STB$JOBTRAIL
   set docsize = DBMS_LOB.GETLENGTH(BINARYFILE),
    reference = STB$JOB.getJobParameter('DOCID', cnJobId),
    mimetype = csMimeType,
    blobflag = 'B',
    leadingdoc = 'T',
    download = 'T',
    filename = getFileName(sFileName)
  where jobid = cnJobId
  and documenttype = csTarget
  and filename = sFileName;

  commit;

  isr$trace.stat('end', 'Function '||sCurrentName||' finished',sCurrentName);
  return oErrorObj;

exception
  when exLoadFileError then
    sUserMsg := utd$msglib.getmsg ('exLoadFileError', nCurrentLanguage,  csP1 => sqlerrm(sqlcode), csP2 => crlf||'ISR$LOADER.loadFileFromLoader failed');
    sImplMsg := sCurrentName || rtrim(': '||sImplMsg,': ');
    sDevMsg  := substr(dbms_utility.format_error_stack ||crlf|| dbms_utility.format_error_backtrace, 1,4000);
    oErrorObj.handleError ( Stb$typedef.cnSeverityCritical, sUserMsg, sImplMsg, sDevMsg );
    Stb$job.markDBJobAsBroken(cnJobId, sIp, sPort, oErrorObj.sErrorCode, oErrorObj.sErrorMessage, oErrorObj.sSeverityCode);
    return oErrorObj;
  when others then
    sUserMsg := utd$msglib.getmsg ('exUnexpectedError', nCurrentLanguage,  csP1 => sqlerrm(sqlcode));
    sImplMsg := sCurrentName || rtrim(': '||sImplMsg,': ');
    sDevMsg  := substr(dbms_utility.format_error_stack ||crlf|| dbms_utility.format_error_backtrace, 1,4000);
    oErrorObj.handleError ( Stb$typedef.cnSeverityCritical, sUserMsg, sImplMsg, sDevMsg );
    Stb$job.markDBJobAsBroken(cnJobId, sIp, sPort, oErrorObj.sErrorCode, oErrorObj.sErrorMessage, oErrorObj.sSeverityCode);
    return oErrorObj;
end loadFileFromClient;

--***********************************************************************************************************************
function copyFilesIntoJobtrail( cnJobid in     number,
                                cxXml   in out xmltype)
  return STB$OERROR$RECORD
is
  sCurrentName   constant varchar2(100) := $$PLSQL_UNIT||'.copyFilesIntoJobtrail (JobID='||cnJobId||')';
  sUserMsg               varchar2(4000);
  sDevMsg                varchar2(4000);
  sImplMsg               varchar2(4000);
  oErrorObj               STB$OERROR$RECORD :=STB$OERROR$RECORD();
  sDocType                STB$JOBTRAIL.DOCUMENTTYPE%type;
  nDocId                  STB$DOCTRAIL.DOCID%type;


  -- get doc defintions
  cursor cgetDocDef is
    select distinct f.fileid, f.doc_definition, f.blobflag
      from isr$FILE f, isr$OUTPUT$FILE iof
     where f.download = 'T'
       and f.fileid = iof.fileid
       and iof.fileid = STB$JOB.getJobParameter ('MASTERTEMPLATE', cnJobid)
       and f.USAGE = csMasterTemplate
       and iof.outputid = STB$JOB.getJobParameter ('OUTPUTID', cnJobid)
    union
    select f2.fileid, f2.doc_definition, f2.blobflag
      from isr$file f1
         , isr$OUTPUT$FILE iof1
         , isr$file f2
         , isr$OUTPUT$FILE iof2
         , (select t.fileid, t.templatename, t.release, t.version
                  from (select max (version) version, templatename
                          from ( select *
                                   from isr$template
                                  where case
                                          when STB$SECURITY.checkGroupRight ('TEMPLATE'||fileid, STB$OBJECT.getCurrentReporttypeId) = 'T' then VISIBLE
                                          else 'F'
                                        end = 'T')
                         where ( (release = 'T'
                              and STB$SECURITY.checkGroupRight ('SHOW_UNRELEASED_TEMPLATES', STB$OBJECT.getCurrentReporttypeId) = 'F')
                             or (STB$SECURITY.checkGroupRight ('SHOW_UNRELEASED_TEMPLATES', STB$OBJECT.getCurrentReporttypeId) = 'T'))
                        group by templatename) vt, isr$template t
                 where vt.version = t.version
                   and vt.templatename = t.templatename) t
     where f1.fileid = STB$JOB.getJobParameter ('MASTERTEMPLATE', cnJobid)
       and f1.fileid = iof1.fileid
       and f2.fileid = iof2.fileid
       and f1.doc_definition != f2.doc_definition
       and iof1.outputid = iof2.outputid
       and iof1.outputid = STB$JOB.getJobParameter ('OUTPUTID', cnJobid)
       and t.templatename = iof2.templatename
       and t.fileid = iof2.fileid ;

  -- all files without the master template, parent document in stb$doctrail exists
  cursor cGetId is
    select f.fileid,
           f.BLOBFLAG
      from isr$OUTPUT$FILE iof,
           isr$FILE f
     where iof.outputid = STB$JOB.getJobParameter('OUTPUTID', cnJobid)
       and iof.fileid = f.fileid
       and f.USAGE != csMasterTemplate
       and f.download = 'T';
begin
  isr$trace.stat('begin', 'Function '||sCurrentName||' started.', sCurrentName);

  -- set reporttypeid for security
  STB$OBJECT.setCurrentReporttypeid(STB$JOB.getJobParameter('REPORTTYPEID', cnJobid));

  -- get all files apart from the master templates
  for rGetFiles in cGetId loop
    oErrorObj := Stb$docks.copyFileToJob(rGetFiles.fileid,cnJobId,rGetFiles.blobflag);
  end loop;

  -- OFFSHOOT-----------------
  -- get the master template
  if STB$JOB.getJobParameter('TOKEN',cnJobid) = csOffshoot  then

    for rgetDocDef in cgetDocDef loop
      oErrorObj := Stb$docks.copyFileToJob(rgetDocDef.fileid,cnJobId,rgetDocDef.blobflag);
    end loop;

    sDocType  := csMasterTemplate;

  -- NO OFFSHOOT-----------
  -- get the max document id for each doc defintion
  -- the doc defintions come out of the file table
  else
    for rgetDocDef in cgetDocDef loop
      if STB$UTIL.getReptypeParameter ( STB$OBJECT.getCurrentReporttypeId, 'REQUIRES_VERSIONING') != 'F' then
        nDocId := null;
        oErrorObj := Stb$docks.getMaxDocIdInSrnumber(STB$JOB.getJobParameter('REPID', cnJobid), nDocId,'D',rgetDocDef.doc_definition);
      else
        nDocId := 0;
      end if;
      -- there is no previous document in the document trail
      if nDocId = 0  then
        oErrorObj := Stb$docks.copyFileToJob(rgetDocDef.fileid,cnJobId,rgetDocDef.blobflag);
      -- there is a prevoius document use that one instaed of the master template
      else
        oErrorObj := Stb$docks.copyDocToJob(nDocId,cnJobId);

        update STB$JOBTRAIL
           set documenttype = csTemplate,
               leadingdoc = null
         where jobid = cnJobId
           and documenttype = csTarget
           and leadingdoc = 'T';

         commit;
      end if;
    end loop;
  end if;

  isr$trace.stat('end', 'Function '||sCurrentName||' finished',sCurrentName);
  return oErrorObj;
exception
  when others then
    sUserMsg := utd$msglib.getmsg ('exUnexpectedError', nCurrentLanguage,  csP1 => sqlerrm(sqlcode));
    sImplMsg := sCurrentName || rtrim(': '||sImplMsg,': ');
    sDevMsg  := substr(dbms_utility.format_error_stack ||crlf|| dbms_utility.format_error_backtrace, 1,4000);
    oErrorObj.handleError ( Stb$typedef.cnSeverityCritical, sUserMsg, sImplMsg, sDevMsg );
    Stb$job.markDBJobAsBroken(cnJobId, sIp, sPort, oErrorObj.sErrorCode, oErrorObj.sErrorMessage, oErrorObj.sSeverityCode);
    return oErrorObj;
end copyFilesIntoJobtrail;


-- ***************************************************************************************************
FUNCTION convertAttachments(cnJobid in NUMBER, cxXml IN OUT XMLTYPE) RETURN STB$OERROR$RECORD
is
  sCurrentName   constant varchar2(100) := $$PLSQL_UNIT||'.convertAttachments (JobID='||cnJobId||')';
  sUserMsg                varchar2(4000);
  oErrorObj               STB$OERROR$RECORD :=STB$OERROR$RECORD();
      
begin
  isr$trace.stat('begin', 'begin', sCurrentName);
  oErrorObj := isr$pdf.convertAttachments(cnJobid);
  if oErrorObj.isExceptionCritical then
    raise stb$typedef.exCritical;
  end if;
  isr$trace.stat('end', 'end', sCurrentName);
return oErrorObj;

exception
  when others then
    sUserMsg := utd$msglib.getmsg ('exUnexpectedError', stb$security.getCurrentLanguage,  csP1 => sqlerrm(sqlcode));
    oErrorObj.handleError ( Stb$typedef.cnSeverityCritical, sUserMsg, sCurrentName,  dbms_utility.format_error_stack ||chr(10)|| dbms_utility.format_error_backtrace  );
    Stb$job.markDBJobAsBroken(cnJobId, sIp, sPort, oErrorObj.sErrorCode, oErrorObj.sErrorMessage, oErrorObj.sSeverityCode);
    return oErrorObj;
end convertAttachments;

--***********************************************************************************************************************************
function writeAttachmentInfo( cnJobid in     number,
                              cxXml   in out xmltype)
  return STB$OERROR$RECORD
is
  sCurrentName  constant varchar2(100) := $$PLSQL_UNIT||'.writeAttachmentInfo (JobID='||cnJobId||')';
  sUserMsg               varchar2(4000);
  sDevMsg                varchar2(4000);
  sImplMsg               varchar2(4000);
  oErrorObj              STB$OERROR$RECORD :=STB$OERROR$RECORD();
  nExistsNode            number;

  cursor cGetFiles  is
    select filename
         , v.value
         , case
              when (select parametervalue
                      from isr$doc$property dp, stb$doctrail d
                     where d.nodeid = reference
                       and d.nodetype = 'REPORT'
                       and d.docid = dp.docid
                       and d.doctype = 'C'
                       and d.doc_definition = v.value
                       and parametername = 'PROTECTED') = 'T' then
                 'true'
              else
                 'false'
           end
              protected
         , NVL ( (select parametervalue
                    from isr$doc$property dp, stb$doctrail d
                   where d.nodeid = reference
                     and d.nodetype = 'REPORT'
                     and d.docid = dp.docid
                     and d.doctype = 'C'
                     and d.doc_definition = v.value
                     and parametername = 'SCALE'), NVL ( (select parameterdefault
                                                            from isr$doc$prop$template
                                                           where reporttypeid = 0
                                                             and parametername = 'SCALE'), 1))
              scale
      from stb$jobtrail j, isr$parameter$values v
     where documenttype(+) = 'ATTACHMENTHTML'
       and jobid(+) = cnJobid
       and V.ENTITY = 'DOCDEFINITION'
       and V.value || '.HTM' = filename(+)
       and exists
              (select 1
                 from isr$STYLESHEET s, isr$OUTPUT$TRANSFORM ot
                where ot.outputid = STB$JOB.getJobParameter ('OUTPUTID', cnJobid)
                  and ot.stylesheetid = s.STYLESHEETID
                  and UPPER (bookmarkname) = 'S_PDF')
       /*AND NVL (DBMS_LOB.getlength (j.textfile), 0) > 0*/;

  cursor cExistsNode  is
    select EXISTSNODE (cxXml, '/config/apps/app/blocks')
    from dual;

begin
  isr$trace.stat('begin', 'Function '||sCurrentName||' started.', sCurrentName);

  open  cExistsNode;
  fetch cExistsNode into nExistsNode;
  close cExistsNode;

  isr$trace.debug('status nExistsNode', 'nExistsNode: '||nExistsNode, sCurrentName);

  if (nExistsNode = 0) then
    isr$XML.CreateNode(cxXml, '/config/apps/app[@name="wordmodul"]', 'blocks', '');
 end if;

  isr$XML.CreateNodeAttr(cxXml, '/config/apps/app/blocks', 'block', 'empty', 'name', 'empty');

  for rGetFiles in cGetFiles loop
    isr$XML.CreateNodeAttr(cxXml, '/config/apps/app/blocks', 'block', rGetFiles.filename, 'name', rGetFiles.value);
    isr$XML.SetAttribute(cxXml, '/config/apps/app/blocks/block[@name="'||rGetFiles.value||'"]', 'protected', rGetFiles.protected);
    isr$XML.SetAttribute(cxXml, '/config/apps/app/blocks/block[@name="'||rGetFiles.value||'"]', 'scale', rGetFiles.scale);
  end loop;

  open  cExistsNode;
  fetch cExistsNode into nExistsNode;
  close cExistsNode;

  isr$trace.debug('final status cxXml', 'cxXml: see column LOGCLOB', sCurrentName, cxXml);
  isr$trace.stat('end', 'Function '||sCurrentName||' finished',sCurrentName);
  return oErrorObj;

exception
  when others then
    sUserMsg := utd$msglib.getmsg ('exUnexpectedError', nCurrentLanguage,  csP1 => sqlerrm(sqlcode));
    sImplMsg := sCurrentName || rtrim(': '||sImplMsg,': ');
    sDevMsg  := substr(dbms_utility.format_error_stack ||crlf|| dbms_utility.format_error_backtrace, 1,4000);
    oErrorObj.handleError ( Stb$typedef.cnSeverityCritical, sUserMsg, sImplMsg, sDevMsg );
    Stb$job.markDBJobAsBroken(cnJobId, sIp, sPort, oErrorObj.sErrorCode, oErrorObj.sErrorMessage, oErrorObj.sSeverityCode);
    return oErrorObj;
end writeAttachmentInfo;

--***********************************************************************************************************************************
function saveAttachmentsInDocument( cnJobid in     number,
                                    cxXml   in out xmltype )
  return STB$OERROR$RECORD
is
  sCurrentName  constant varchar2(100) := $$PLSQL_UNIT||'.saveAttachmentsInDocument (JobID='||cnJobId||')';
  sUserMsg               varchar2(4000);
  sDevMsg                varchar2(4000);
  sImplMsg               varchar2(4000);
  oErrorObj              STB$OERROR$RECORD :=STB$OERROR$RECORD();
  olFileList             isr$FILEBOOKMARK$LIST;
  blFile                 blob;
  nrowcount              number:= 1;

  cursor cGetFiles is
    select blobflag
         , binaryfile
         , filename
      from STB$JOBTRAIL
     where jobid = cnJobId
       and download = 'T'
       and NVL(dbms_lob.getlength(binaryfile), 0) > 0;
begin
  isr$trace.stat('begin', 'Function '||sCurrentName||' started.', sCurrentName);

  olFileList := isr$FILEBOOKMARK$LIST();
  for rGetFiles in cGetFiles loop
   if rGetFiles.blobflag = 'B' then
     isr$trace.debug('status filename', 'rGetFiles.filename: '||rGetFiles.filename, sCurrentName);
     olFileList.extend();
     olFileList(nrowcount) := isr$FILEBOOKMARK$REC(null, null, null);
     olFileList(nrowcount).sfilename := rGetFiles.filename;
     olFileList(nrowcount).blfile := rGetFiles.binaryfile;
     nrowcount := nrowcount + 1;
   end if;
  end loop;

  STB$JAVA.addAttachmentstoDoc(olFileList, blFile);

  if dbms_lob.getlength(blFile) > 0 then
    update stb$jobtrail
         set binaryfile = blFile
         , docsize = dbms_lob.getlength(blFile)
         , timestamp =  sysdate
       where jobid = cnJobid
         and documenttype in ( 'TARGET')
         and leadingdoc = 'T';
    commit;
 end if;

  isr$trace.debug('status blFile length', 'dbms_lob.getlength(blFile): '||dbms_lob.getlength(blFile), sCurrentName);
  isr$trace.stat('end', 'Function '||sCurrentName||' finished',sCurrentName);
  return oErrorObj;

exception
  when others then
    sUserMsg := utd$msglib.getmsg ('exUnexpectedError', nCurrentLanguage,  csP1 => sqlerrm(sqlcode));
    sImplMsg := sCurrentName || rtrim(': '||sImplMsg,': ');
    sDevMsg  := substr(dbms_utility.format_error_stack ||crlf|| dbms_utility.format_error_backtrace, 1,4000);
    oErrorObj.handleError ( Stb$typedef.cnSeverityCritical, sUserMsg, sImplMsg, sDevMsg );
    Stb$job.markDBJobAsBroken(cnJobId, sIp, sPort, oErrorObj.sErrorCode, oErrorObj.sErrorMessage, oErrorObj.sSeverityCode);
    return oErrorObj;
end saveAttachmentsInDocument;

--***********************************************************************************************************************************
function copyFiles2IntoJobtrail( cnJobid in     number,
                                 cxXml   in out xmltype)
  return STB$OERROR$RECORD
is
  sCurrentName   constant varchar2(100) := $$PLSQL_UNIT||'.copyFiles2IntoJobtrail (JobID='||cnJobId||')';
  sUserMsg                varchar2(4000);
  sDevMsg                 varchar2(4000);
  sImplMsg                varchar2(4000);
  oErrorObj               STB$OERROR$RECORD :=STB$OERROR$RECORD();
  nCounter                number;

  -- all files including the master template
  -- no parent document in the table stb$doctrail
  cursor cGetIdAll(cnOutputId in number) is
    select f.fileid
         , f.BLOBFLAG
         , f.USAGE
         , f.FILENAME
      from isr$OUTPUT$FILE iof, isr$FILE f
     where iof.outputid = cnOutputId
       and iof.fileid = f.fileid;
begin
  isr$trace.stat('begin', 'Function '||sCurrentName||' started.', sCurrentName);

  nCounter := 0;
  for rGetFiles  in cGetIdAll(STB$JOB.getJobParameter('OUTPUTID', cnJobid)) loop
    nCounter := nCounter + 1;
    oErrorObj := Stb$docks.copyFileToJob(rGetFiles.fileid,cnJobId,rGetFiles.blobflag);

    isr$trace.debug('status file', 'rGetFiles.fileid: '||rGetFiles.fileid||'  rGetFiles.blobFlag: '||rGetFiles.blobflag, sCurrentName);

    -- added JB for non versioned word reports
    if rGetFiles.USAGE = csMasterTemplate then
      isr$XML.CreateNode(cxXML, '/config/apps/app[@order=1]', 'template', rGetFiles.Filename);
   end if;

  end loop;

  isr$trace.stat('end', 'Function '||sCurrentName||' finished',sCurrentName);
  return oErrorObj;

exception
  when others then
    sUserMsg := utd$msglib.getmsg ('exUnexpectedError', nCurrentLanguage,  csP1 => sqlerrm(sqlcode));
    sImplMsg := sCurrentName || rtrim(': '||sImplMsg,': ');
    sDevMsg  := substr(dbms_utility.format_error_stack ||crlf|| dbms_utility.format_error_backtrace, 1,4000);
    oErrorObj.handleError ( Stb$typedef.cnSeverityCritical, sUserMsg, sImplMsg, sDevMsg );
    Stb$job.markDBJobAsBroken(cnJobId, sIp, sPort, oErrorObj.sErrorCode, oErrorObj.sErrorMessage, oErrorObj.sSeverityCode);
    return oErrorObj;
end copyFiles2IntoJobtrail;

--***********************************************************************************************************************************
function copyFiles3IntoJobtrail( cnJobid in     number,
                                 cxXml   in out xmltype)
  return STB$OERROR$RECORD
is
  sCurrentName   constant varchar2(100) := $$PLSQL_UNIT||'.copyFiles3IntoJobtrail (JobID='||cnJobId||')';
  sUserMsg               varchar2(4000);
  sDevMsg                varchar2(4000);
  sImplMsg               varchar2(4000);
  oErrorObj               STB$OERROR$RECORD :=STB$OERROR$RECORD();
  nCounter               number;

  cursor cGetIdAll is
    select rf.filename
         , rf.textfile
         , rf.binaryfile
         , STB$UTIL.getReportParameterValue (
             'GRAPHIC_HEIGHT'
            , STB$JOB.getJobParameter ('REPID', cnJobid)
           )
              height
         , case when parametervalue != 'T' then 'F' else 'T' end display
      from isr$REPORT$FILE rf, stb$jobparameter jp
     where repid = STB$JOB.getJobParameter ('REPID', cnJobid)
       and jp.jobid = cnjobid
       and (parametername = 'FILTER_' || REPLACE(REPLACE(filename, '@'), '$')
        and parametervalue = 'T'
         or rf.binaryfile is not null
        and parametername = 'REPID')
    order by SUBSTR (rf.filename, INSTR (rf.filename, '.') + 1), rf.filename;

  clXSLT      clob;
  clTextFile  clob;
  clXML       clob;
  xHtmlFile   xmltype;
  sLobFlag   varchar2(1);
  sReturn   varchar2(1);

begin
  isr$trace.stat('begin', 'Function '||sCurrentName||' started.', sCurrentName);

  clXSLT :=  '<?xml version="1.0" encoding="UTF-8"?>
              <xsl:stylesheet version="2.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform" xmlns:xs="http://www.w3.org/2001/XMLSchema" xmlns:fn="http://www.w3.org/2005/xpath-functions">
                <xsl:output method="html" version="4.01" encoding="UTF-8" indent="yes" media-type="text/html"/>
                <xsl:template match="/">
                  <html>
                    <head>
                      <title><xsl:value-of select="//@title"/></title>
                    </head>
                    <body>
                      <table>
              ';

  nCounter := 0;
  for rGetFiles  in cGetIdAll loop
    isr$trace.debug('status filename', 'rGetFiles.filename: '||rGetFiles.filename, sCurrentName);
    isr$trace.debug('status display', 'rGetFiles.display: '||rGetFiles.display, sCurrentName);

    sLobFlag := case
                   when NVL (DBMS_LOB.getlength (rGetFiles.textfile), 0) = 0 then
                      'B'
                   else
                      'C'
                end;
    isr$trace.debug('status sLobFlag', 'sLobFlag: '||sLobFlag, sCurrentName);

    if rGetFiles.display = 'T' then

      clXSLT :=  clXSLT ||
                 '<tr><td'||case when rGetFiles.height is not null then ' height="'||rGetFiles.height||'"' end ||'>' ||
                 case
                   when sLobFlag = 'B' then '<img src="'||rGetFiles.filename||'" title="'||rGetFiles.filename||'" alt="'||rGetFiles.filename||'"/>'
                   else '<xsl:value-of select="document('''||rGetFiles.filename||''')/dummy" disable-output-escaping="yes"/>'
                 end ||
                 '</td></tr>';

   end if;

    xHtmlFile := isr$XML.ClobToXML('<?xml version="handled by isr$XML" encoding="handled by isr$XML"?><dummy><![CDATA['||rGetFiles.textfile||']]></dummy>');

    insert into STB$JOBTRAIL (JOBID
                            , DOCUMENTTYPE
                            , MIMETYPE
                            , DOCSIZE
                            , TIMESTAMP
                            , BINARYFILE
                            , TEXTFILE
                            , FILENAME
                            , BLOBFLAG
                            , ENTRYID
                            , REFERENCE
                            , LEADINGDOC
                            , DOWNLOAD)
    values (
              cnJobId
            , 'REPORTFILE'
            , null
            , case
                 when sLobFlag = 'B' then
                    DBMS_LOB.getlength (rGetFiles.binaryfile)
                 else
                    DBMS_LOB.getlength (rGetFiles.textfile)
              END
            ,  sysdate
            , rGetFiles.binaryfile
            , isr$XML.XMLToClob(xHtmlFile)
            , rGetFiles.filename
            , sLobFlag
            , STB$JOBTRAIL$SEQ.NEXTVAL
            , STB$JOB.getJobParameter ('REPID', cnJobid)
            , 'F'
            , 'T'
           );

    if STB$UTIL.getSystemParameter('JAVA_INSIDE_DB') = 'F' and NVL (isr$process.checkProcessServer, 'F') = 'T' and sLobFlag != 'B' then
      isr$PROCESS.saveBlob(STB$UTIL.clobToBlob(ISR$XML.XMLToClob(xHtmlFile)), isr$PROCESS.getUserHome, isr$PROCESS.sSessionPath||rGetFiles.filename);
   end if;
  end loop;

  clXSLT := clXSLT ||
            '         </table>
                    </body>
                  </html>
                </xsl:template>
              </xsl:stylesheet>
              ';

  isr$trace.debug_all('status clXSLT', 'clXSLT: '||clXSLT, sCurrentName);

  oErrorObj := STB$DOCKS.saveXMLReport( isr$XML.clobToXml('<dummy/>'), cnJobid, 'REPORTFILES.XML');
  select textfile
    into clXML
    from stb$jobtrail
   where documenttype = csRohDaten
     and jobid = cnJobId;

  sReturn := isr$xml.XSLTTransform(clXSLT, clXML, clTextFile, null, cnJobId);
  if sReturn != 'T' then
    Stb$job.markDBJobAsBroken(cnJobId, sIp, sPort, oErrorObj.sErrorCode, oErrorObj.sErrorMessage, oErrorObj.sSeverityCode);
    return oErrorObj;
 end if;

  insert into STB$JOBTRAIL (JOBID
                          , DOCUMENTTYPE
                          , MIMETYPE
                          , DOCSIZE
                          , TIMESTAMP
                          , TEXTFILE
                          , FILENAME
                          , BLOBFLAG
                          , ENTRYID
                          , REFERENCE
                          , LEADINGDOC
                          , DOWNLOAD)
  values (
            cnJobId
          , csTarget
          , null
          , DBMS_LOB.getlength (clTextFile)
          ,  sysdate
          , clTextFile
          , 'REPORTFILES.HTM'
          , 'C'
          , STB$JOBTRAIL$SEQ.NEXTVAL
          , STB$JOB.getJobParameter ('REPID', cnJobid)
          , 'F'
          , 'T'
         );


  isr$trace.stat('end', 'Function '||sCurrentName||' finished',sCurrentName);
  return oErrorObj;

exception
  when others then
    sUserMsg := utd$msglib.getmsg ('exUnexpectedError', nCurrentLanguage,  csP1 => sqlerrm(sqlcode));
    sImplMsg := sCurrentName || rtrim(': '||sImplMsg,': ');
    sDevMsg  := substr(dbms_utility.format_error_stack ||crlf|| dbms_utility.format_error_backtrace, 1,4000);
    oErrorObj.handleError ( Stb$typedef.cnSeverityCritical, sUserMsg, sImplMsg, sDevMsg );
    Stb$job.markDBJobAsBroken(cnJobId, sIp, sPort, oErrorObj.sErrorCode, oErrorObj.sErrorMessage, oErrorObj.sSeverityCode);
    return oErrorObj;
end copyFiles3IntoJobtrail;

--****************************************************************************************************************************
-- ISRC-550
FUNCTION copyReportFileIntoJobtrail(cnJobid IN NUMBER) RETURN STB$OERROR$RECORD
IS
  oError               STB$OERROR$RECORD :=STB$OERROR$RECORD();
  sCurrentName         constant varchar2(100) := $$PLSQL_UNIT||'.copyReportFileIntoJobtrail (JobID='||cnJobId||')';
  sMsg                 varchar2(4000);
BEGIN
  isr$trace.stat('begin','begin',sCurrentName);

  insert into STB$JOBTRAIL (JOBID
                          , DOCUMENTTYPE
                          , MIMETYPE
                          , DOCSIZE
                          , TIMESTAMP
                          , TEXTFILE
                          , BINARYFILE
                          , FILENAME
                          , BLOBFLAG
                          , ENTRYID
                          , REFERENCE
                          , DOWNLOAD)
    (select cnJobId
          , 'TARGET'
          , 'unknown'
          , case when binaryfile is null or dbms_lob.getLength(binaryfile) <= 0 then dbms_lob.getLength(textfile) else dbms_lob.getLength(binaryfile) end
          , SYSDATE
          , textfile
          , binaryfile
          , filename
          , case when binaryfile is null or dbms_lob.getLength(binaryfile) <= 0 then 'C' else 'B' end
          , STB$JOBTRAIL$SEQ.NEXTVAL
          , repid
          , 'F'
       from ISR$REPORT$FILE
      where entryid = STB$JOB.getJobParameter('REFERENCE',cnJobId)
        and STB$JOB.getJobParameter('NODETYPE',cnJobId) like '%REPORT_FILE');

  isr$trace.stat('end','end',sCurrentName);
  RETURN oError;

EXCEPTION
WHEN OTHERS THEN
  sMsg := utd$msglib.getmsg ('exUnexpectedError', nCurrentLanguage,  sqlerrm(sqlcode));
  oError.handleError ( Stb$typedef.cnSeverityCritical, sMsg, sCurrentName, dbms_utility.format_error_stack ||crlf|| dbms_utility.format_error_backtrace );
  Stb$job.markDBJobAsBroken(cnJobId, sIp, sPort, oError.sErrorCode, oError.sErrorMessage, oError.sSeverityCode);
  RETURN oError;
END copyReportFileIntoJobtrail;


--****************************************************************************************************************************
FUNCTION saveWordDocument(cnJobid IN NUMBER, nDocId in STB$DOCTRAIL.DOCID%TYPE, sPassword in STB$REPORT.PASSWORD%TYPE) RETURN STB$OERROR$RECORD
IS
  oError          STB$OERROR$RECORD := STB$OERROR$RECORD();
  sCurrentName    constant varchar2(100) := $$PLSQL_UNIT||'.saveWordDocument (JobID='||cnJobId||')';
  blBlob          BLOB;
  sFileName       varchar2(200);
  sReturn         varchar2(4000);
 begin
    select content, filename
    into blBlob, sFileName from STB$DOCTRAIL 
    where docid = nDocid;
    isr$trace.debug('before unzip', DBMS_LOB.GETLENGTH(blBlob), sCurrentName);
    blBlob := STB$JAVA.unzip(blBlob);
    IF DBMS_LOB.GETLENGTH(blBlob) = 0 THEN
      blBlob := null;
     END IF;
     isr$trace.debug('after unzip', DBMS_LOB.GETLENGTH(blBlob), sCurrentName);
  
    sReturn := ISR$LOADER.protectAllowOnlyRevisions(sIp, sPort, sContext, sNameSpace,cnJobId, sFileName,
       sPassword, blBlob, to_number(stimeout));
       
    IF sReturn = 'T' THEN
      sReturn := ISR$LOADER.loadFileFromLoader(sIp, sPort, sContext, sNameSpace, cnJobId, sFileName, 
         'LOADER_BLOB_FILE', csTarget, sJavaLogLevel, isr$trace.getReference, to_number(stimeout));
         
      update STB$JOBTRAIL
       set docsize = DBMS_LOB.GETLENGTH(BINARYFILE),
        mimetype = csMimeType,
        blobflag = 'B'
       where jobid = cnJobId
       and documenttype = csTarget
       and filename = sFileName;
    END IF; 
    
    IF sReturn != 'T' THEN
      ISR$TRACE.error('sReturn', sReturn, sCurrentName);
      oError := Stb$docks.copyDocToJob(nDocId, cnJobId, blBlob);
    END IF;
    
    return oError;
    
  exception when others then
    blBLob := null;
    oError.handleError ( Stb$typedef.cnSeverityCritical, utd$msglib.getmsg ('exUnexpectedError', nCurrentLanguage,  sqlerrm(sqlcode)),
        sCurrentName, dbms_utility.format_error_stack ||crlf|| dbms_utility.format_error_backtrace );
    return oError;
end saveWordDocument;

--****************************************************************************************************************************
FUNCTION copyUnprotectDocIntoJobtrail(cnJobid IN NUMBER, cxXml IN OUT XMLTYPE) RETURN STB$OERROR$RECORD
IS
  oError          STB$OERROR$RECORD := STB$OERROR$RECORD();
  sCurrentName    constant varchar2(100) := $$PLSQL_UNIT||'.copyUnprotectDocIntoJobtrail (JobID='||cnJobId||')';
  sMsg            varchar2(4000);
  nDocId          STB$DOCTRAIL.DOCID%TYPE;
  sPassword       STB$REPORT.PASSWORD%TYPE;

BEGIN
  isr$trace.stat('begin','begin',sCurrentName);

  -- ISRC-550
  if STB$JOB.getJobParameter('NODETYPE',cnJobId) like '%REPORT_FILE' then
    oError := copyReportFileIntoJobtrail(cnJobId);
  -- ISRC-772
  elsif STB$JOB.getJobParameter('NODETYPE',cnJobId) in ('RAWDATA','REMOTESNAPSHOT') then
    oError := copyXMLIntoJobtrail(cnJobId, cxXml);
  else

    nDocId := STB$JOB.getJobParameter('DOCID',cnJobId);

    oError := Stb$docks.getPasswordFromDoc(nDocId, sPassword);
    isr$trace.debug ('sPassword',  nDocId|| '; ' ||sPassword, sCurrentName);
    IF sPassword is not null and STB$JOB.getJobParameter('NODETYPE',cnJobId) = 'DOCUMENT' THEN
      oError := saveWordDocument(cnJobId, nDocId, sPassword);
    ELSE
     oError := Stb$docks.copyDocToJob(nDocId, cnJobId);
    END IF;
    
    isr$trace.debug('stb$object.getcurrentsrnumber',  stb$object.getcurrentsrnumber, sCurrentName);
    STB$OBJECT.setCurrentRepid(STB$JOB.getJobParameter('REPID',cnJobId));
    STB$OBJECT.setCurrentDocid(STB$JOB.getJobParameter('DOCID',cnJobId));
    oError := isr$reporttype$package.createSrAudit (stb$object.getcurrentsrnumber, STB$JOB.getJobParameter('COMMENT',cnJobId));

  end if;
  isr$trace.stat('end','end',sCurrentName);
  RETURN oError;

EXCEPTION
WHEN OTHERS THEN
  sMsg := utd$msglib.getmsg ('exUnexpectedError', nCurrentLanguage,  sqlerrm(sqlcode));
  oError.handleError ( Stb$typedef.cnSeverityCritical, sMsg, sCurrentName, dbms_utility.format_error_stack ||crlf|| dbms_utility.format_error_backtrace );
  Stb$job.markDBJobAsBroken(cnJobId, sIp, sPort, oError.sErrorCode, oError.sErrorMessage, oError.sSeverityCode);
  RETURN oError;
END copyUnprotectDocIntoJobtrail;


--****************************************************************************************************************************
function copyWordmoduleIntoJobtrail( cnJobid in     number,
                                     cxXml   in out xmltype)
  return STB$OERROR$RECORD
is
  sCurrentName   constant varchar2(100) := $$PLSQL_UNIT||'.copyWordmoduleIntoJobtrail (JobID='||cnJobId||')';
  sUserMsg               varchar2(4000);
  sDevMsg                varchar2(4000);
  sImplMsg               varchar2(4000);
  oErrorObj               STB$OERROR$RECORD :=STB$OERROR$RECORD();

  cursor cGetId is
    select f.fileid, f.BLOBFLAG
      from isr$FILE f, isr$OUTPUT$FILE iof
     where iof.outputid = STB$JOB.getJobParameter ('OUTPUTID', cnJobid)
       and iof.fileid = f.fileid
       and f.USAGE = 'START';

  rGetFiles cGetId%ROWTYPE;

begin
  isr$trace.stat('begin', 'Function '||sCurrentName||' started.', sCurrentName);

  open cGetId;
 fetch cGetId into rGetFiles;
  close cGetId;

  isr$trace.debug('status file','rGetFiles.fileid: '||rGetFiles.fileid||'  rGetFiles.blobFlag: '||rGetFiles.blobFlag, sCurrentName);

  oErrorObj := Stb$docks.copyFileToJob(rGetFiles.fileid,cnJobId,rGetFiles.blobflag);

  isr$trace.stat('end', 'Function '||sCurrentName||' finished',sCurrentName);
  return oErrorObj;

exception
  when others then
    sUserMsg := utd$msglib.getmsg ('exUnexpectedError', nCurrentLanguage,  csP1 => sqlerrm(sqlcode));
    sImplMsg := sCurrentName || rtrim(': '||sImplMsg,': ');
    sDevMsg  := substr(dbms_utility.format_error_stack ||crlf|| dbms_utility.format_error_backtrace, 1,4000);
    oErrorObj.handleError ( Stb$typedef.cnSeverityCritical, sUserMsg, sImplMsg, sDevMsg );
    Stb$job.markDBJobAsBroken(cnJobId, sIp, sPort, oErrorObj.sErrorCode, oErrorObj.sErrorMessage, oErrorObj.sSeverityCode);
    return oErrorObj;
END copyWordmoduleIntoJobtrail;


--*******************************************************************************************************************
function loadFilesToClient( cnJobid in     number,
                            cxXml   in out xmltype)
  return STB$OERROR$RECORD
is
  exUploadFileError exception;
  sCurrentName      constant varchar2(100) := $$PLSQL_UNIT||'.loadFilesToClient (JobID='||cnJobId||')';
  sUserMsg          varchar2(4000);
  sDevMsg           varchar2(4000);
  sImplMsg          varchar2(4000);
  oErrorObj         STB$OERROR$RECORD :=STB$OERROR$RECORD();
  cBlob             STB$JOBTRAIL.BINARYFILE%type;
  sRemoteEx     clob;
  sConfigName    isr$transfer$file$config.configname%type;

  cursor cGetFiles(nJobId in number) is
    select blobflag
         , binaryfile
         , filename
         , documenttype
      from STB$JOBTRAIL
     where jobid = nJobId
       and download = 'T'
       and (NVL(dbms_lob.getlength(binaryfile), 0) > 0
            or NVL(dbms_lob.getlength(textfile), 0) > 0);

  rFiles cGetFiles%ROWTYPE;

  cursor cGetLog is
    select isr$XML.xmlToClob (
              xmlelement (
                 "ENTRIES"
               , (select xmlagg(xmlelement (
                                   "ENTRY"
                                 , xmlattributes (
                                      TO_CHAR (timestamp, STB$UTIL.getSystemParameter('DATEFORMAT')) as "DATE"
                                   )
                                 , xmlelement ("ACTIONTAKEN", ACTIONTAKEN)
                                 , xmlelement ("FUNCTIONGROUP", FUNCTIONGROUP)
                                 , xmlelement ("LOGENTRY", LOGENTRY)
                                 , xmlelement ("USERNAME", ORACLEUSERNAME)
                                 , xmlelement ("LOGLEVEL", LOGLEVEL)
                                 , xmlelement ("REFERENCE", REFERENCE)
                                ))
                    from isr$log)
              )
           )
      from DUAL;

begin
  isr$trace.stat('begin', 'Function '||sCurrentName||' started.', sCurrentName);
  sJavaLogLevel := isr$trace.getJavaUserLoglevelStr;

  if Stb$util.getSystemParameter('DOWNLOADFLAG') = 'T' then

    update STB$JOBTRAIL
       set DOWNLOAD = 'T'
     where jobid = cnJobId
       /*AND (DBMS_LOB.getlength (rFiles.binaryfile) > 0
         or DBMS_LOB.getlength (rFiles.textfile) > 0)*/;

    insert into STB$JOBTRAIL (JOBID
                            , DOCUMENTTYPE
                            , MIMETYPE
                            , DOCSIZE
                            , TIMESTAMP
                            , TEXTFILE
                            , FILENAME
                            , BLOBFLAG
                           -- , ENTRYID
                            , REFERENCE
                            , LEADINGDOC
                            , DOWNLOAD)
       (select cnJobId
             , 'STYLESHEET'
             , EXTENSION
             , DOCSIZE
             ,  sysdate
             , STYLESHEET
             , FILENAME
             , 'C'
         --    , STB$JOBTRAIL$SEQ.NEXTVAL
             , TO_CHAR (s.stylesheetid)
             , 'F'
             , 'T'
          from isr$stylesheet s,
            (select distinct s.stylesheetid
          from isr$STYLESHEET s, isr$OUTPUT$TRANSFORM ot
         where ot.outputid = STB$JOB.getJobParameter ('OUTPUTID', cnJobId)
           and ot.stylesheetid = s.STYLESHEETID
           and stylesheet is not null
             and DBMS_LOB.getLength (stylesheet) > 0) so
         where so.stylesheetid =s.stylesheetid) ;

    commit;
  end if;


  for rGetClobFiles in (select *
                          from stb$jobtrail
                         where blobflag = 'C'
                           and NVL(dbms_lob.getlength(textfile), 0) > 0
                           and jobid = cnJobid) loop
    cBlob := STB$UTIL.clobToBlob (rGetClobFiles.textfile);
    update STB$JOBTRAIL
       set binaryfile = cBlob,
           blobflag ='B',
           textfile = Empty_Clob()
     where entryid = rGetClobFiles.entryid;
    commit;
    DBMS_LOB.FREETEMPORARY(cBlob);
  end loop;

  open cGetFiles(cnJobId);
  loop
   fetch cGetFiles into rFiles;
    exit when cGetFiles%NOTFOUND;
    isr$trace.debug('status filename', 'rFiles.filename: '||rFiles.filename, sCurrentName);
    if dbms_lob.getlength(rFiles.binaryfile) > 0 then
      isr$trace.debug('action load file', 'load '||rFiles.filename, sCurrentName);
      sConfigName := case when rFiles.blobflag = 'B' then 'LOADER_BLOB_FILE' when rFiles.blobflag = 'C' then 'LOADER_CLOB_FILE' end;
        sImplMsg :=    'sIP: '||sIP ||crlf
                    || 'sPort: '||sPort||crlf
                    || 'sContext: '||sContext||crlf
                    || 'sNamespace: '||sNamespace||crlf
                    || 'cnJobid: '||cnJobid||crlf
                    || 'filename: '||rFiles.filename||crlf
                    || 'sConfigName: '||sConfigName;
      sRemoteEx := ISR$LOADER.loadFileToLoader(sIp, sPort, sContext, sNamespace, cnJobid, rFiles.filename, sConfigName, rFiles.documenttype, sJavaLogLevel, isr$trace.getReference, to_number(stimeout));

     if sRemoteEx is not null then
      isr$trace.warn(' error', 'error ', sCurrentName, sRemoteEx);
      sUserMsg := utd$msglib.getmsg('exUploadFileErrorText', nCurrentLanguage, rFiles.filename,  sIp || ' ' || sPort );
      oErrorObj.handleJavaError( Stb$typedef.cnSeverityCritical,  sRemoteEx, sCurrentName );
      raise exUploadFileError;
    end if;
   end if;
  end loop;
  close cGetFiles;

  isr$trace.stat('end', 'Function '||sCurrentName||' finished',sCurrentName);
  return oErrorObj;

exception
  when exUploadFileError then
    oErrorObj.handleError ( Stb$typedef.cnSeverityCritical, sUserMsg, sImplMsg, sDevMsg );
    oErrorObj.handleError ( Stb$typedef.cnSeverityCritical, sUserMsg, sCurrentName,
                            substr(dbms_utility.format_error_stack || CHR(10) || dbms_utility.format_error_backtrace, 1,4000)  );
    Stb$job.markDBJobAsBroken(cnJobId, sIp, sPort, oErrorObj.sErrorCode, oErrorObj.sErrorMessage, oErrorObj.sSeverityCode);
    return oErrorObj;
  when others then
    sUserMsg := utd$msglib.getmsg ('exUnexpectedError', nCurrentLanguage,  csP1 => sqlerrm(sqlcode));
    sImplMsg := sCurrentName || rtrim(': '||sImplMsg,': ');
    sDevMsg  := substr(dbms_utility.format_error_stack ||crlf|| dbms_utility.format_error_backtrace, 1,4000);
    oErrorObj.handleError ( Stb$typedef.cnSeverityCritical, sUserMsg, sImplMsg, sDevMsg );
    Stb$job.markDBJobAsBroken(cnJobId, sIp, sPort, oErrorObj.sErrorCode, oErrorObj.sErrorMessage, oErrorObj.sSeverityCode);
    return oErrorObj;
end loadFilesToClient;

function createParamsForNewDoc( cnJobid in     number,
                                     cxXml   in out xmltype,
                                     csActionType in isr$action.token%type,
                                     coMastertemplateList in isr$varchar$list,
                                     sPassword out STB$REPORT.PASSWORD%type)
  return STB$OERROR$RECORD is
  sCurrentName   constant varchar2(100) := $$PLSQL_UNIT||'.createParamsForNewDoc (JobID='||cnJobId||')';
  sUserMsg                varchar2(4000);

  oErrorObj               STB$OERROR$RECORD :=STB$OERROR$RECORD();
  nLineIndex              number;

  sFileName               varchar2(500);
  sFileExtension          varchar2(50);
  sPasswordTarget         STB$REPORT.PASSWORD%type;

  cXml                    clob;
  cResult                 clob;
  clProperties            clob;

  sReturn                 varchar2(1);
  sStmt                   varchar2(4000);
  sProtectionEntity       varchar2(100);

  cursor cgetTargetInfo is
    select doc_definition,
           MIMETYPE,
           blobflag,
           documenttype,
           REFERENCE,
           filename
    from   STB$JOBTRAIL
    where  jobid = cnJobId
    and    documenttype in (select column_value from table(coMastertemplateList));

 cursor cGetProperties is
    select textfile
      from stb$jobtrail
     where jobid = cnJobid
       and documenttype = 'PROPERTIES';

  cursor cGetBlocks(csDocDefinition in varchar2, csProtectEntity in varchar2) is
    select s.STYLESHEETID
         , jt.filename
         , s.structured
         , NVL (fpv.display, 'noName') bookmarkname
         , s.extension
         , NVL (
              /*(select info
                 from isr$file$parameter$values
                where fileid = STB$JOB.getJobParameter ('MASTERTEMPLATE', cnJobId)
                  and entity = 'STYLESHEET$PROTECTED'
                  and display = fpv.display)*/
               (SELECT case when pv.info = 'F' then 'F' 
                            when (select key from isr$crit crit, isr$output$transform t
                                       where crit.entity = csProtectEntity
                                         AND repid = STB$JOB.getJobParameter('REPID', cnJobid)
                                         AND crit.key = t.bookmarkname
                                         AND t.outputid = STB$JOB.getJobParameter ('OUTPUTID', cnJobId)
                                         AND pv.display = t.bookmarkname) is not null then 'F' 
                            else 'T' 
                        end protected    
                  FROM isr$file$parameter$values pv
                 WHERE pv.fileid = STB$JOB.getJobParameter ('MASTERTEMPLATE', cnJobId)
                       AND pv.entity = 'STYLESHEET$PROTECTED' 
                       AND pv.display = fpv.display)
    
            , ot.PROTECTED
           )
              protected
         , (select info
                 from isr$file$parameter$values
                where fileid = STB$JOB.getJobParameter ('MASTERTEMPLATE', cnJobId)
                  and entity = 'STYLESHEET$SETCAPTION'
                  and display = fpv.display)
              setcaption
         , s.VERSION
      from isr$STYLESHEET s, isr$OUTPUT$TRANSFORM ot, STB$JOBTRAIL jt, isr$FILE$PARAMETER$VALUES fpv
     where ot.outputid = STB$JOB.getJobParameter ('OUTPUTID', cnJobId)
       and ot.stylesheetid = s.STYLESHEETID
       and ot.DOC_DEFINITION = csDocDefinition
       and jt.jobid = cnJobId
       and jt.filename =
              NVL (bookmarkname
                 , SUBSTR (s.filename, 0, INSTR (s.filename, '.') - 1))
              || '.'
              || extension
       and bookmarkname is not null
       and s.structured != 'A'
       and fpv.entity = 'STYLESHEET$BOOKMARK'
       and to_number(fpv.info) = s.stylesheetid
       and fpv.value = bookmarkname
       and fpv.fileid = STB$JOB.getJobParameter ('MASTERTEMPLATE', cnJobId)
       and NVL (DBMS_LOB.getlength (jt.textfile), 0) > 0
       order by ot.orderno;

begin
  isr$trace.stat('begin', 'begin', sCurrentName);
    -- write into the steering file
    nLineIndex :=0;

    -- for each application  write the target and application information
    for rgetTargetInfo in cgetTargetInfo loop
      nLineIndex :=nLineIndex +1;
      isr$trace.debug('cgetTargetInfo loop','nLineIndex: '||nLineIndex, sCurrentName);

      isr$XML.CreateNodeAttr(cxXML,  '/config/apps', 'app', '', 'order', nLineIndex);
      isr$XML.SetAttribute(cxXML, '/config/apps/app[@order='||nLineIndex||']', 'name', 'wordmodul');
      isr$XML.CreateNode(cxXML, '/config/apps/app[@order='||nLineIndex||']', 'executable', 'wordmodul.exe');
      isr$XML.CreateNode(cxXML, '/config/apps/app[@order='||nLineIndex||']', 'wait_for_process', 'true');
      isr$XML.CreateNode(cxXML, '/config/apps/app[@order='||nLineIndex||']', 'parameter', '[config]');
      isr$XML.CreateNode(cxXML, '/config/apps/app[@order='||nLineIndex||']', 'targettype', rgetTargetInfo.blobflag);
      isr$XML.CreateNode(cxXML, '/config/apps/app[@order='||nLineIndex||']', 'mimetype', rgetTargetInfo.mimetype);
      isr$XML.CreateNode(cxXML, '/config/apps/app[@order='||nLineIndex||']', 'doc_definition', rgetTargetInfo.doc_definition);   --JB 07 Nov 2007
      isr$XML.CreateNode(cxXML, '/config/apps/app[@order='||nLineIndex||']', 'isrdoctype', 'D');
      isr$trace.debug('status doc_Definition','rgetTargetInfo.doc_definition: '||rgetTargetInfo.doc_definition, sCurrentName);

      if csActionType != 'CAN_MODIFY_WITH_COMMON_DOC' then
        isr$XML.CreateNode(cxXML, '/config/apps/app[@order='||nLineIndex||']', 'command', csActionType);
        isr$XML.CreateNode(cxXML, '/config/apps/app[@order='||nLineIndex||']', 'template', rgetTargetInfo.Filename);
      end if;

      --set the file name of the target file
      if rgetTargetInfo.documenttype = csMasterTemplate then
        --souce password
        isr$XML.CreateNode(cxXML, '/config/apps/app[@order='||nLineIndex||']', 'sourcepassword', '');

        --get extension from template
        sFileExtension := SUBSTR(rgetTargetInfo.filename,(INSTR(rgetTargetInfo.filename,'.',-1,1)+1),length(rgetTargetInfo.filename));

        --target file
        sstmt := 'select '||STB$UTIL.getCurrentCustomPackage||'.createDocumentName(STB$JOB.getJobParameter(''REPID'', '||cnJobId||'), STB$JOB.getJobParameter(''DOCID'', '||cnJobId||')) from dual';
        isr$trace.debug('sstmt', sstmt, sCurrentName);
        execute immediate sstmt into sFileName;
        isr$trace.debug('sFileName', sFileName, sCurrentName);

        -- MCD, 09. Apr 2014, append doc_definition to the filename for 2..n-th app
        if nLineIndex > 1 then
          sFileName := sFileName|| '_' || rgetTargetInfo.doc_definition;
        end if;

        isr$XML.CreateNode(cxXML, '/config/apps/app[@order='||nLineIndex||']', 'target', NVL(sFileName, cnJobId || '_' ||TO_CHAR(nLineIndex)) || '.' || sFileExtension);

      else
       -- source password
        oErrorObj := Stb$docks.getPasswordFromDoc(rgetTargetInfo.reference,sPassword);
        isr$XML.CreateNode(cxXML, '/config/apps/app[@order='||nLineIndex||']' , 'sourcepassword', sPassword);

        -- target file name
        isr$XML.CreateNode(cxXML, '/config/apps/app[@order='||nLineIndex||']', 'target', rgetTargetInfo.filename);
     end if;

     -- error for test
    -- isr$XML.CreateNode(cxXML, '/config/apps/app[@order='||nLineIndex||']', 'error', 'T');

       -- no offshoot
      if STB$JOB.getJobParameter('TOKEN',cnJobid) != csOffshoot then
        --target password
        -- get the password using the reference (repid)
        isr$trace.debug_all('status token <> CAN_OFFSHOOT_DOC', 'STB$JOB.getJobParameter(REPID, cnJobId): '||STB$JOB.getJobParameter('REPID', cnJobId), sCurrentName);
        isr$trace.debug_all('status token <> CAN_OFFSHOOT_DOC', 'STB$UTIL.getPassword(STB$JOB.getJobParameter(REPID, cnJobId): *****', sCurrentName);
        sPassword := STB$UTIL.getPassword(STB$JOB.getJobParameter('REPID', cnJobId));
        isr$trace.debug_all('status token <> CAN_OFFSHOOT_DOC', 'sPasswordTarget: '||sPasswordTarget, sCurrentName);
        isr$XML.CreateNode(cxXML, '/config/apps/app[@order='||nLineIndex||']', 'targetpassword', sPassword);
     end if;

      isr$trace.debug_all('status doc_definition', 'rgetTargetInfo.doc_definition: '||rgetTargetInfo.doc_definition, sCurrentName);

      isr$XML.CreateNode(cxXML, '/config/apps/app[@order='||nLineIndex||']', 'blocks','');
      sProtectionEntity := STB$UTIL.getReptypeParameter(STB$JOB.getJobParameter('REPORTTYPEID',cnJobId), 'PROTECTION_ENTITY');
  
      for rGetBlocks in  cGetBlocks( rgetTargetInfo.doc_definition, sProtectionEntity) loop
        isr$trace.debug_all('status bookmarkname', 'rGetBlocks.bookmarkname: '||rGetBlocks.bookmarkname, sCurrentName);
        isr$XML.CreateNode(cxXML, '/config/apps/app[@order='||nLineIndex||']/blocks', 'block', rGetBlocks.filename);
        isr$XML.SetAttribute(cxXML, '/config/apps/app[@order='||nLineIndex||']/blocks/block[position()=last()]', 'name', rGetBlocks.bookmarkname);
        isr$XML.SetAttribute(cxXML, '/config/apps/app[@order='||nLineIndex||']/blocks/block[position()=last()]', 'protected', case when UPPER(rGetBlocks.protected) in ('T', 'TRUE') then 'true' else 'false' end);
        isr$XML.SetAttribute(cxXML, '/config/apps/app[@order='||nLineIndex||']/blocks/block[position()=last()]', 'setcaption', case when UPPER(rGetBlocks.SETCAPTION) in ('T', 'TRUE') then 'true' else 'false' end);
        isr$XML.SetAttribute(cxXML, '/config/apps/app[@order='||nLineIndex||']/blocks/block[position()=last()]', 'version', rGetBlocks.version);
      end loop;

      if STB$JOB.getJobParameter('FINALFLAG', cnJobid) is null then

       open cGetProperties;
       fetch cGetProperties into clProperties;
       if cGetProperties%found then
          isr$XML.InsertChildXMLFragment(cxXML, '/config/apps/app[@order='||nLineIndex||']', clProperties);
       end if;
       close cGetProperties;

       if STB$JOB.getJobParameter('TOKEN', cnJobid) = csOffshoot then
        isr$XML.deleteNode(cxXML, '/config/apps/app[@order='||nLineIndex||']/properties/custom-property[@name="REPSTATUS"]');
        isr$XML.createNode(cxXML, '/config/apps/app[@order='||nLineIndex||']/properties', 'custom-property', 2);
        isr$XML.setAttribute(cxXML, '/config/apps/app[@order='||nLineIndex||']/properties/custom-property[position()=last()]', 'name', 'REPSTATUS');
       end if;

      end if;

    end loop;
      -- error block
      for rGetErrorBlock in ( select stylesheet
                                   , 'ERRORS' bookmarkname
                                   , extension
                                   , 'F' protected
                                   , version
                                   , filename
                                from isr$stylesheet--, (select * from tmp$errors where rownum = 1)
                               where structured = 'E') loop
        cXml := DBMS_XMLQUERY.GetXML ('select * from tmp$errors');
        isr$trace.debug('status '||SUBSTR(rGetErrorBlock.filename, 0, INSTR(rGetErrorBlock.filename, '.')) || 'XML', 'dbms_lob.getlength(cXml): '||DBMS_LOB.getlength(cXml), sCurrentName, cXml);

        if DBMS_LOB.getlength(cXml) > 0 then
          sReturn := isr$xml.XSLTTransform(rGetErrorBlock.stylesheet, cXML, cResult, null, cnJobid);
          isr$trace.debug('status '||rGetErrorBlock.bookmarkname|| '.' || rGetErrorBlock.extension, 'dbms_lob.getlength(cResult) :'||DBMS_LOB.getlength(cResult), sCurrentName, cXml);
        end if;

        for rGetFiles in ( select 'S' flag, rGetErrorBlock.filename filename from DUAL
                           union
                            select 'X' flag, SUBSTR (rGetErrorBlock.filename, 0, INSTR (rGetErrorBlock.filename, '.')) || 'XML' filename
                              from DUAL
                            union
                            select 'H' flag, rGetErrorBlock.bookmarkname || '.' || rGetErrorBlock.extension filename
                              from DUAL
                          ) loop
          insert into STB$JOBTRAIL (JOBID
                                  , DOCUMENTTYPE
                                  , DOCSIZE
                                  , TIMESTAMP
                                  , TEXTFILE
                                  , FILENAME
                                  , BLOBFLAG
                                  , ENTRYID
                                  , REFERENCE
                                  , LEADINGDOC
                                  , DOWNLOAD)
          values (
                    cnJobId
                  , 'ERRORFILES'
                  , DBMS_LOB.getlength ( case
                                              when rGetFiles.flag = 'S' then rGetErrorBlock.stylesheet
                                              when rGetFiles.flag = 'X' then cXML
                                              when rGetFiles.flag = 'H' then cResult
                                            end)
                  ,  sysdate
                  , case
                      when rGetFiles.flag = 'S' then rGetErrorBlock.stylesheet
                      when rGetFiles.flag = 'X' then cXML
                      when rGetFiles.flag = 'H' then cResult
                    end
                  , rGetFiles.filename
                  , 'C'
                  , STB$JOBTRAIL$SEQ.NEXTVAL
                  , STB$JOB.getJobParameter ('REPID', cnJobid)
                  , 'F'
                  , 'T'
                 );
        end loop;

        if cResult  is not null and DBMS_LOB.getLength(cResult) > 0 then
          isr$XML.CreateNode(cxXML, '/config/apps/app[@order='||nLineIndex||']/blocks', 'block',rGetErrorBlock.bookmarkname||'.'||rGetErrorBlock.extension);
          isr$XML.SetAttribute(cxXML, '/config/apps/app[@order='||nLineIndex||']/blocks/block[position()=last()]', 'name', rGetErrorBlock.bookmarkname);
          isr$XML.SetAttribute(cxXML, '/config/apps/app[@order='||nLineIndex||']/blocks/block[position()=last()]', 'protected', rGetErrorBlock.protected);
          isr$XML.SetAttribute(cxXML, '/config/apps/app[@order='||nLineIndex||']/blocks/block[position()=last()]', 'version', rGetErrorBlock.version);
       end if;
      end loop;


    if NVL(NVL(STB$JOB.getJobParameter('INVISIBLE_FILTER_PARA_DISPLAYED_PDF_CREATION_ALLOWED', cnJobid), STB$JOB.getJobParameter('FILTER_PARA_DISPLAYED_PDF_CREATION_ALLOWED', cnJobid)), 'F') = 'T' then
      oErrorObj := createParametersPDF(cnJobid, cxXML);
    end if;
    isr$trace.debug('final status cxXml', 's. -> LOGCLOB', sCurrentName, cxXml);
    isr$trace.stat('end', 'end', sCurrentName);
    return oErrorObj;
exception
  when others then
    sUserMsg := utd$msglib.getmsg ('exUnexpectedError', nCurrentLanguage,  csP1 => sqlerrm(sqlcode));
    oErrorObj.handleError ( Stb$typedef.cnSeverityCritical, sUserMsg, sCurrentName, dbms_utility.format_error_stack ||crlf|| dbms_utility.format_error_backtrace );
    Stb$job.markDBJobAsBroken(cnJobId, sIp, sPort, oErrorObj.sErrorCode, oErrorObj.sErrorMessage, oErrorObj.sSeverityCode);
    return oErrorObj;
end createParamsForNewDoc;
--***********************************************************************************************************************
function createParametersWordmodule( cnJobid in     number,
                                     cxXml   in out xmltype)
  return STB$OERROR$RECORD
is
  sCurrentName   constant varchar2(100) := $$PLSQL_UNIT||'.createParametersWordmodule (JobID='||cnJobId||')';
  sUserMsg                varchar2(4000);
  sDevMsg                 varchar2(4000);
  sImplMsg                varchar2(4000);
  oErrorObj               STB$OERROR$RECORD :=STB$OERROR$RECORD();
  nLineIndex              number;
  sPassword               STB$REPORT.PASSWORD%type;
  sRemoveEmpty            varchar2(1);
  sDisableClear           varchar2(1);
  sActionType             isr$ACTION.TOKEN%type;
  nAppId                  number;

  cursor cGetDocInfo(cnDocid in STB$DOCTRAIL.DOCID%type) is
    select nodeid,
           nodetype,
           mimetype,
           filename,
           doc_definition
    from   STB$DOCTRAIL
    where  docId = cnDocid;

  rGetDocInfo cGetDocInfo%ROWTYPE;

  cursor cGetProtected(cnRepid in STB$REPORT.REPID%type)  is
    select case when STB$UTIL.GetReportParameterValue('PROTECTED', cnRepid) = 'T' then null else 'T' end
    from   dual;

  sUnProtected varchar2(1);

  cursor cGetAction is
    select Nvl (STB$JOB.getJobParameter ('COMMAND', cnJobId), token) token
    from   isr$ACTION
    where  actionid = STB$JOB.getJobParameter ('ACTIONID', cnJobId);

  coMastertemplateList  constant   isr$varchar$list := isr$varchar$list(csTemplate, csMasterTemplate); --UCIS-256: wrong order

BEGIN
  isr$trace.stat('begin', 'Function '||sCurrentName||' started.', sCurrentName);
  isr$trace.debug('status cxXml', 'cxXml: see column LOGCLOB', sCurrentName, cxXml);

  -- entspricht CAN_RUN_REPORT or OFFSHOOT
  if STB$JOB.getJobParameter('DOCID', cnJobId) is null or STB$JOB.getJobParameter('TOKEN',cnJobid) = csOffshoot then

    open  cGetAction;
    fetch cGetAction into sActionType;
    close cGetAction;

    oErrorObj := createParamsForNewDoc(cnJobId, cxXml, sActionType, coMastertemplateList, sPassword);

  else

    -- DISPLAY/DOCUMENT PROPERTIES
    isr$trace.debug('status docid', 'STB$JOB.getJobParameter(DOCID, cnJobId): '||STB$JOB.getJobParameter('DOCID', cnJobId), sCurrentName);

    -- the reference is the doc id -> get the report id (node id)
    open  cGetDocInfo(STB$JOB.getJobParameter('DOCID', cnJobId));
    fetch cGetDocInfo into rGetDocInfo;
    close cGetDocInfo;

    isr$trace.debug('status filename', 'rGetDocInfo.filename: '||rGetDocInfo.filename, sCurrentName);

    isr$XML.CreateNode(cxXML, '/config/apps', 'app', '');
    isr$XML.SetAttribute(cxXML, '/config/apps/app', 'order', '1');
    isr$XML.SetAttribute(cxXML, '/config/apps/app[@order=1]', 'name', 'wordmodul');
    isr$XML.CreateNode(cxXML, '/config/apps/app[@order=1]', 'executable', 'wordmodul.exe');
    isr$XML.CreateNode(cxXML, '/config/apps/app[@order=1]', 'wait_for_process', 'true');
    isr$XML.CreateNode(cxXML, '/config/apps/app[@order=1]', 'parameter', '[config]');
    isr$XML.CreateNode(cxXML, '/config/apps/app[@order=1]', 'target', rGetDocInfo.filename);
    isr$XML.CreateNode(cxXML, '/config/apps/app[@order=1]', 'targettype', 'B');
    isr$XML.CreateNode(cxXML, '/config/apps/app[@order=1]', 'mimetype', rGetDocInfo.mimetype);
    isr$XML.CreateNode(cxXML, '/config/apps/app[@order=1]', 'doc_definition', rGetDocInfo.doc_definition);   --JB 07 Nov 2007
    isr$XML.CreateNode(cxXML, '/config/apps/app[@order=1]', 'isrdoctype', 'D');


    open  cGetAction;
    fetch cGetAction into sActionType;
    close cGetAction;

    if sActionType != 'CAN_MODIFY_WITH_COMMON_DOC' then
      isr$XML.CreateNode(cxXML, '/config/apps/app[@order=1]', 'command', sActionType);
   end if;
    oErrorObj := Stb$docks.getPasswordFromDoc(STB$JOB.getJobParameter('DOCID', cnJobId),sPassword);
    isr$trace.debug('status sPassword', 'sPassword: *****', sCurrentName);

    -- write the source password -> this is for the action "checkout" identical to the target password
    -- in the case of CAN_RUN_REPORT  the password will be written with the template action
    isr$XML.CreateNode(cxXML, '/config/apps/app[@order=1]', 'sourcepassword', sPassword);
    isr$XML.CreateNode(cxXML, '/config/apps/app[@order=1]', 'targetpassword', sPassword);

    isr$trace.debug('status sPassword2', 'sPassword2: *****', sCurrentName);

    case
      when sActionType = 'CAN_REFRESH_DOC_PROPERTIES' then
        isr$XML.CreateNode(cxXML, '/config/apps/app[@order=1]', 'template', rGetDocInfo.filename);
      when sActionType = 'CAN_DISPLAY_REPORT' and NVL(STB$JOB.getJobParameter('DISPLAY_DOC', cnJobId), 'T') = 'T' then
        -- General parameters
        isr$XML.CreateNode(cxXML, '/config/apps', 'fullpathname', csISRTempPath);
        isr$XML.CreateNode(cxXML, '/config/apps', 'target', rGetDocInfo.filename);
        isr$XML.CreateNode(cxXML, '/config/apps', 'extension', 'doc');
        nAppId := isr$XML.valueOf(cxXML, 'count(/config/apps/app)') + 1;
        isr$XML.CreateNodeAttr(cxXML,  '/config/apps', 'app', '', 'order', nAppId);
        isr$XML.setAttribute(cxXML, '/config/apps/app[@order='||nAppId||']', 'name', 'ME');
        isr$XML.createNode(cxXML, '/config/apps/app[@order='||nAppId||']', 'wait_for_process', 'false');
        isr$XML.createNode(cxXML, '/config/apps/app[@order='||nAppId||']', 'command', 'DISPLAY');
      when sActionType = 'CAN_MODIFY_WITH_COMMON_DOC' then
        isr$XML.CreateNode(cxXML, '/config/apps/app[@order=1]', 'command', 'CAN_RUN_REPORT');
        isr$XML.CreateNode(cxXML, '/config/apps/app[@order=1]', 'template', rGetDocInfo.Filename);
   end case;

 end if;

  -- MCD, 08.Jan 2006
  -- PROTECTED or UNPROTECTED
  open cGetProtected(STB$JOB.getJobParameter('REPID',cnJobid));
 fetch cGetProtected into sUnProtected;
  close cGetProtected;
  isr$trace.debug('status sUnProtected', 'sUnProtected: '||sUnProtected, sCurrentName);

  if TRIM(sUnProtected) is not null then
    isr$XML.CreateNode(cxXML, '/config/apps/app[@name="wordmodul"]', 'mode', sUnProtected);
 end if;

  execute immediate
      'BEGIN
        :1 := ' || STB$UTIL.getCurrentCustomPackage || '.ckeckRemoveEmptyBookmarks(:2, :3);
      end;'
    using out oErrorObj, in cnJobid, out sRemoveEmpty;

  isr$XML.CreateNode(cxXML, '/config/apps/app[@name="wordmodul"]', 'removeNonReferencedBookmarks', sRemoveEmpty);

  sDisableClear := STB$UTIL.getReptypeParameter(STB$JOB.getJobParameter('REPORTTYPEID',cnJobId), 'DEACTIVATE_CLEAR_STATIC_BOOKMARKS');
  isr$XML.CreateNode(cxXML, '/config/apps/app[@name="wordmodul"]', 'deactivateClearStaticBookmarks', sDisableClear);

  isr$XML.CreateNode(cxXML, '/config/apps/app[@name="wordmodul"]', 'isr-bookmarks', '');
  -- MS 2014.12.17 - Exclude "Create once"-Bookmarks from this list. Otherwise these would be removed during "Remove Empty Bookmarks"
  for rGetAllBookmarks in ( select distinct display, fpv.ordernumber
                              from isr$file$parameter$values fpv,
                                   isr$output$transform ot
                             where fpv.fileid  = STB$JOB.getJobParameter ('MASTERTEMPLATE', cnJobId)
                               and fpv.entity  = 'STYLESHEET$BOOKMARK'
                               and fpv.info    = ot.stylesheetid
                               and ot.recreate = 'T'
                             order by fpv.ordernumber ) loop
    isr$XML.CreateNode(cxXML, '/config/apps/app[@name="wordmodul"]/isr-bookmarks', 'bookmark', rGetAllBookmarks.display);
  end loop;


  -- number OF APPS
  isr$XML.SetAttribute(cxXML, '/config/apps', 'num', isr$XML.valueOf(cxXML, 'count(/config/apps/app)'));

  isr$trace.debug('final status cxXml', 'cxXml: see column LOGCLOB', sCurrentName, cxXml);
  isr$trace.stat('end', 'Function '||sCurrentName||' finished',sCurrentName);
  return oErrorObj;

exception
  when others then
    sUserMsg := utd$msglib.getmsg ('exUnexpectedError', nCurrentLanguage,  csP1 => sqlerrm(sqlcode));
    sImplMsg := sCurrentName || rtrim(': '||sImplMsg,': ');
    sDevMsg  := substr(dbms_utility.format_error_stack ||crlf|| dbms_utility.format_error_backtrace, 1,4000);
    oErrorObj.handleError ( Stb$typedef.cnSeverityCritical, sUserMsg, sImplMsg, sDevMsg );
    Stb$job.markDBJobAsBroken(cnJobId, sIp, sPort, oErrorObj.sErrorCode, oErrorObj.sErrorMessage, oErrorObj.sSeverityCode);
    return oErrorObj;
end createParametersWordmodule;

--***********************************************************************************************************************
function createParametersCheckout( cnJobid in     number,
                                   cxXml   in out xmltype)
  return STB$OERROR$RECORD
is
  sCurrentName   constant varchar2(100) := $$PLSQL_UNIT||'.createParametersCheckout (JobID='||cnJobId||')';
  sUserMsg               varchar2(4000);
  sDevMsg                varchar2(4000);
  sImplMsg               varchar2(4000);
  oErrorObj               STB$OERROR$RECORD :=STB$OERROR$RECORD();
  sPasswordTarget        STB$REPORT.PASSWORD%type;
  sPasswordSource        STB$REPORT.PASSWORD%type;
  sActionType            isr$ACTION.TOKEN%type;

  cursor cGetLeadingDoc is
    select jt.filename,
           jt.leadingdoc,
           jt.REFERENCE,
           jt.documenttype,
           jt.mimetype,
           jt.DOC_DEFINITION
    from   STB$JOBTRAIL jt
    where  (   jt.documenttype = 'D'
            or jt.documenttype = csTarget)
    and    jt.jobid = cnJobid
    and    jt.leadingdoc = 'T';

  cursor cGetAttachmentsDoc is
    select jt.filename
    from   STB$JOBTRAIL jt
    where  (   jt.documenttype = 'D'
            or jt.documenttype = csTarget)
    and    jt.jobid = cnJobid
    and    (   jt.leadingdoc = 'F'
            or jt.leadingdoc is null);

    cursor cGetAction is
    select Nvl (STB$JOB.getJobParameter ('COMMAND', cnJobId), token) token
    from   isr$ACTION
    where  actionid = STB$JOB.getJobParameter ('ACTIONID', cnJobId);

  rGetLeadingDoc cGetLeadingDoc%ROWTYPE;

  nAppId number;

begin
  isr$trace.stat('begin', 'Function '||sCurrentName||' started.', sCurrentName);
  isr$trace.debug('status cxXml', 'cxXml: see column LOGCLOB', sCurrentName, cxXml);

  open  cGetLeadingDoc;
  fetch cGetLeadingDoc into rGetLeadingDoc;
  close cGetLeadingDoc;

  -- get passwords
  oErrorObj := Stb$docks.getPasswordFromDoc(STB$JOB.getJobParameter('OLDDOCID', cnJobId),sPasswordSource);
  isr$trace.debug('status sPasswordSource', 'sPasswordSource: '||sPasswordSource, sCurrentName);
  oErrorObj := Stb$docks.getPasswordFromDoc(STB$JOB.getJobParameter('REFERENCE', cnJobId),sPasswordTarget);
  isr$trace.debug('status sPasswordTarget', 'xxxsPasswordTargetxx: '||sPasswordTarget, sCurrentName);

  -- General parameters
  -- Append doc ending
  if getExtension(STB$JOB.getJobParameter('TO', cnJobId)) is null then
    STB$JOB.setJobParameter('TO', STB$JOB.getJobParameter('TO', cnJobId)||'.'||getExtension(rGetLeadingDoc.filename), cnJobId);
  end if;
  isr$XML.CreateNode(cxXML, '/config/apps', 'fullpathname', getPath(STB$JOB.getJobParameter('TO', cnJobId)));
  isr$XML.CreateNode(cxXML, '/config/apps', 'target', getFileName(STB$JOB.getJobParameter('TO', cnJobId)));
  isr$XML.CreateNode(cxXML, '/config/apps', 'extension', getExtension(STB$JOB.getJobParameter('TO', cnJobId)));
  isr$XML.CreateNode(cxXML, '/config/apps', 'overwrite', STB$JOB.getJobParameter('OVERWRITE', cnJobId));

  -- ========= SAVE
  -- =======================
  nAppId := isr$XML.valueOf(cxXML, 'count(/config/apps/app)') + 1;
  isr$XML.CreateNodeAttr(cxXML,  '/config/apps', 'app', '', 'order', nAppId);
  isr$XML.SetAttribute(cxXML, '/config/apps/app[@order='||nAppId||']', 'name', 'ME');
  isr$XML.CreateNode(cxXML, '/config/apps/app[@order='||nAppId||']', 'wait_for_process', 'false');
  isr$XML.CreateNode(cxXML, '/config/apps/app[@order='||nAppId||']', 'command', 'SAVE');
  isr$XML.CreateNode(cxXML, '/config/apps/app[@order='||nAppId||']', 'sources', ' ');
  isr$XML.CreateNode(cxXML,'/config/apps/app[@order='||nAppId||']/sources','source','');
  isr$XML.setAttribute(cxXML, '/config/apps/app[@order='||nAppId||']/sources/source[position()=last()]', 'name', rGetLeadingDoc.filename);
  isr$XML.setAttribute(cxXML, '/config/apps/app[@order='||nAppId||']/sources/source[position()=last()]', 'leading', 'T');
  -- write the common documents for the saving
  for rGetAttachmentsDoc in cGetAttachmentsDoc loop
    isr$XML.CreateNode(cxXML,'/config/apps/app[@order='||nAppId||']/sources','source','');
    isr$XML.setAttribute(cxXML, '/config/apps/app[@order='||nAppId||']/sources/source[position()=last()]', 'name', rGetAttachmentsDoc.filename);
  end loop;

  -- ========= WORDMODUL
  -- =======================

    open cGetAction;
  fetch cGetAction into sActionType;
   close cGetAction;

  nAppId := isr$XML.valueOf(cxXML, 'count(/config/apps/app)') + 1;
  isr$XML.CreateNodeAttr(cxXML,  '/config/apps', 'app', '', 'order', nAppId);
  isr$XML.SetAttribute(cxXML, '/config/apps/app[@order='||nAppId||']', 'name', 'wordmodul');
  isr$XML.CreateNode(cxXML, '/config/apps/app[@order='||nAppId||']', 'wait_for_process', 'true');
  isr$XML.CreateNode(cxXML, '/config/apps/app[@order='||nAppId||']', 'executable', 'wordmodul.exe');
  isr$XML.CreateNode(cxXML, '/config/apps/app[@order='||nAppId||']', 'parameter', '[config]');
  isr$XML.CreateNode(cxXML, '/config/apps/app[@order='||nAppId||']', 'command', sActionType);
  isr$XML.CreateNode(cxXML, '/config/apps/app[@order='||nAppId||']', 'target', NVL(ISR$XML.valueOf(cxXML, '/config/apps/target/text()'), rGetLeadingDoc.filename));
  isr$XML.CreateNode(cxXML, '/config/apps/app[@order='||nAppId||']', 'targettype', 'B');
  isr$XML.CreateNode(cxXML, '/config/apps/app[@order='||nAppId||']', 'mimetype', rGetLeadingDoc.mimetype);
  isr$XML.CreateNode(cxXML, '/config/apps/app[@order='||nAppId||']', 'doc_definition', rGetLeadingDoc.doc_definition);   --JB 07 Nov 2007
  isr$XML.CreateNode(cxXML, '/config/apps/app[@order='||nAppId||']', 'isrdoctype', 'D');
  isr$XML.CreateNode(cxXML, '/config/apps/app[@order='||nAppId||']', 'sourcepassword', sPasswordSource);
  isr$XML.CreateNode(cxXML, '/config/apps/app[@order='||nAppId||']', 'targetpassword', sPasswordTarget);
  if TRIM(STB$JOB.getJobParameter('USE_PROTECTION', cnJobId)) is not null then
    isr$XML.CreateNode(cxXML, '/config/apps/app[@order='||nAppId||']', 'use-protection', STB$JOB.getJobParameter('USE_PROTECTION', cnJobId));
 end if;

  -- ========= DISPLAY
  -- =======================
  if NVL(STB$JOB.getJobParameter('DISPLAY_DOC', cnJobId), 'T') = 'T' then
    nAppId := isr$XML.valueOf(cxXML, 'count(/config/apps/app)') + 1;
    isr$XML.CreateNodeAttr(cxXML,  '/config/apps', 'app', '', 'order', nAppId);
    isr$XML.setAttribute(cxXML, '/config/apps/app[@order='||nAppId||']', 'name', 'ME');
    isr$XML.createNode(cxXML, '/config/apps/app[@order='||nAppId||']', 'wait_for_process', 'false');
    isr$XML.createNode(cxXML, '/config/apps/app[@order='||nAppId||']', 'command', 'DISPLAY');
 end if;

  -- number OF APPS
  isr$XML.SetAttribute(cxXML, '/config/apps', 'num', isr$XML.valueOf(cxXML, 'count(/config/apps/app)'));

  isr$trace.debug('final status cxXml', 'cxXml: see column LOGCLOB', sCurrentName, cxXml);
  isr$trace.stat('end', 'Function '||sCurrentName||' finished',sCurrentName);
  return oErrorObj;

exception
  when others then
    sUserMsg := utd$msglib.getmsg ('exUnexpectedError', nCurrentLanguage,  csP1 => sqlerrm(sqlcode));
    sImplMsg := sCurrentName || rtrim(': '||sImplMsg,': ');
    sDevMsg  := substr(dbms_utility.format_error_stack ||crlf|| dbms_utility.format_error_backtrace, 1,4000);
    oErrorObj.handleError ( Stb$typedef.cnSeverityCritical, sUserMsg, sImplMsg, sDevMsg );
    Stb$job.markDBJobAsBroken(cnJobId, sIp, sPort, oErrorObj.sErrorCode, oErrorObj.sErrorMessage, oErrorObj.sSeverityCode);
    return oErrorObj;
end createParametersCheckout;


--**********************************************************************************************************************
function createParametersCommon( cnJobid in     number,
                                 cxXml   in out xmltype)
  return STB$OERROR$RECORD
is
  sCurrentName           constant varchar2(100) := $$PLSQL_UNIT||'.createParametersCommon('||cnJobId||')';
  sUserMsg               varchar2(4000);
  sDevMsg                varchar2(4000);
  sImplMsg               varchar2(4000);
  oErrorObj              STB$OERROR$RECORD :=STB$OERROR$RECORD();
  sFileEnding            varchar2(500);

  cursor cGetFileNames( ccnJobId number ) is
    select jt.mimetype,
           jt.filename,
           STB$JOB.getJobParameter('ACTIONTYPE', ccnJobId) actiontype
    from   STB$JOBTRAIL jt, isr$action A
    where  jt.jobid = ccnJobId
    and    jt.documenttype in ('TARGET', 'ROHREPORT', 'PROFILE')
    and    A.actionid = STB$JOB.getJobParameter('ACTIONID', ccnJobId);

  rGetFileNames cGetFileNames%ROWTYPE;

  cursor cGetAttachmentsDoc is
    select jt.filename
    from   STB$JOBTRAIL jt
    where  (jt.documenttype = 'D' or jt.documenttype = csTarget)
    and    jt.jobid = cnJobid
    and    (jt.leadingdoc = 'F' or jt.leadingdoc is null);

  nAppId     number;
  sValueOf   clob;

begin
  isr$trace.stat('begin','begin',sCurrentName);
  isr$trace.debug('value','cxXml IN OUT',sCurrentName, cxXml);

  open  cGetFileNames(cnJobid);
  fetch cGetFileNames into rGetFileNames;
  close cGetFileNames;

  isr$trace.debug('rGetFileNames.filename: ', rGetFileNames.filename||',  rGetFileNames.mimetype: '||rGetFileNames.mimetype||',  rGetFilenames.actiontype: '||rGetFileNames.actiontype, sCurrentName);

  -- General parameters
  sFileEnding := Nvl(getExtension(STB$JOB.getJobParameter('TO', cnJobId)),getExtension(rGetFileNames.filename));
  if isr$XML.valueOf(cxXML, 'count(/config/apps/fullpathname)') = 0 then
    isr$XML.CreateNode(cxXML, '/config/apps', 'fullpathname', coalesce(STB$JOB.getJobParameter('TO_PATH',cnJobId),getPath(STB$JOB.getJobParameter('TO', cnJobId)),csISRTempPath));
  end if;
  if isr$XML.valueOf(cxXML, 'count(/config/apps/target)') = 0 then
    isr$XML.CreateNode(cxXML, '/config/apps', 'target', Nvl(getFileName(STB$JOB.getJobParameter('TO', cnJobId)),rGetFileNames.filename));
  end if;

  sValueOf := ISR$XML.valueOf(cxXML, '/config/apps/target/text()');
  if sFileEnding is not null and (upper(sValueOf) not like '%.'||UPPER(sFileEnding)) then
    isr$XML.UPDATENODE(cxXML, '/config/apps/target', sValueOf||'.'||sFileEnding);
  end if;
  isr$XML.CreateNode(cxXML, '/config/apps', 'extension', case when lower(sFileEnding) != 'zip' then sFileEnding else 'undefined' end);
  isr$XML.CreateNode(cxXML, '/config/apps', 'overwrite', Nvl(STB$JOB.getJobParameter('OVERWRITE', cnJobId),'F'));

  -- the display/save application
  nAppId := isr$XML.valueOf(cxXML, 'count(/config/apps/app)') + 1;
  isr$XML.CreateNodeAttr(cxXML,  '/config/apps', 'app', '', 'order', nAppId);
  isr$XML.SetAttribute(cxXML, '/config/apps/app[@order='||nAppId||']', 'name', 'ME');
  isr$XML.CreateNode(cxXML, '/config/apps/app[@order='||nAppId||']', 'wait_for_process', 'false');
  isr$XML.CreateNode(cxXML, '/config/apps/app[@order='||nAppId||']', 'command', Nvl(rGetFileNames.actiontype, 'DISPLAY'));

  if rGetFileNames.actiontype = 'SAVE' then
    -- save
    isr$XML.CreateNode(cxXML, '/config/apps/app[@order='||nAppId||']', 'sources', ' ');
    isr$XML.CreateNode(cxXML,'/config/apps/app[@order='||nAppId||']/sources','source','');
    isr$XML.setAttribute(cxXML, '/config/apps/app[@order='||nAppId||']/sources/source', 'name',rGetFileNames.filename);
    isr$XML.createNode(cxXML, '/config/apps', 'docid', STB$JOB.getJobParameter('DOCID', cnJobId));
    -- write the common documents for the saving
    for rGetAttachmentsDoc in cGetAttachmentsDoc loop
      if rGetFileNames.filename != rGetAttachmentsDoc.filename then
        isr$XML.CreateNode(cxXML,'/config/apps/app[@order='||nAppId||']/sources','source','');
        isr$XML.setAttribute(cxXML, '/config/apps/app[@order='||nAppId||']/sources/source[position()=last()]', 'name', rGetAttachmentsDoc.filename);
      end if;
    end loop;
    -- display
    if     nvl(STB$JOB.getJobParameter('DISPLAY_DOC', cnJobId), 'T') = 'T'
       and nvl(STB$JOB.getJobParameter('FILTER_DISPLAY_PROFILE',cnJobId),'T') = 'T'
    then
      nAppId := isr$XML.valueOf(cxXML, 'count(/config/apps/app)') + 1;
      isr$XML.CreateNodeAttr(cxXML,  '/config/apps', 'app', '', 'order', nAppId);
      isr$XML.SetAttribute(cxXML, '/config/apps/app[@order='||nAppId||']', 'name', 'ME');
      isr$XML.CreateNode(cxXML, '/config/apps/app[@order='||nAppId||']', 'wait_for_process', 'false');
      isr$XML.CreateNode(cxXML, '/config/apps/app[@order='||nAppId||']', 'command', 'DISPLAY');
    end if;
  end if;

  -- number OF APPS
  isr$XML.SetAttribute(cxXML, '/config/apps', 'num', isr$XML.valueOf(cxXML, 'count(/config/apps/app)'));

  isr$trace.debug('value','cxXml',sCurrentName,cxXml);
  isr$trace.stat('end','end',sCurrentName);
  return oErrorObj;

exception
  when others then
    sUserMsg := utd$msglib.getmsg ('exUnexpectedError', nCurrentLanguage,  csP1 => sqlerrm(sqlcode));
    sImplMsg := sCurrentName || rtrim(': '||sImplMsg,': ');
    sDevMsg  := substr(dbms_utility.format_error_stack ||crlf|| dbms_utility.format_error_backtrace, 1,4000);
    oErrorObj.handleError ( Stb$typedef.cnSeverityCritical, sUserMsg, sImplMsg, sDevMsg );
    Stb$job.markDBJobAsBroken(cnJobId, sIp, sPort, oErrorObj.sErrorCode, oErrorObj.sErrorMessage, oErrorObj.sSeverityCode);
    return oErrorObj;
end createParametersCommon;

--**********************************************************************************************************************
FUNCTION createParametersUpload( cnJobid in     number,
                                 cxXml   in out xmltype)
  return STB$OERROR$RECORD
is
  sCurrentName   constant varchar2(100) := $$PLSQL_UNIT||'.createParametersUpload (JobID='||cnJobId||')';
  sUserMsg               varchar2(4000);
  sDevMsg                varchar2(4000);
  sImplMsg               varchar2(4000);
  oErrorObj               STB$OERROR$RECORD :=STB$OERROR$RECORD();
  nAppId     number;
begin
  isr$trace.stat('begin', 'Function '||sCurrentName||' started.', sCurrentName);
  isr$trace.debug('status cxXml', 'cxXml: see column LOGCLOB', sCurrentName, cxXml);

  -- General parameters
  isr$XML.CreateNode(cxXML, '/config/apps', 'fullpathname', getPath(STB$JOB.getJobParameter('TO', cnJobId)));
  isr$XML.CreateNode(cxXML, '/config/apps', 'target', getFileName(STB$JOB.getJobParameter('TO', cnJobId)));

  -- delete application
  if STB$JOB.getJobParameter('DELETE_FILE', cnJobId) = 'T' then
    nAppId := isr$XML.valueOf(cxXML, 'count(/config/apps/app)') + 1;
    isr$XML.CreateNodeAttr(cxXML,  '/config/apps', 'app', '', 'order', nAppId);
    isr$XML.SetAttribute(cxXML, '/config/apps/app[@order='||nAppId||']', 'name', 'ME');
    isr$XML.CreateNode(cxXML, '/config/apps/app[@order='||nAppId||']', 'command', 'DELETE');
    isr$XML.CreateNode(cxXML, '/config/apps/app[@order='||nAppId||']', 'wait_for_process', 'false');
    isr$XML.CreateNode(cxXML, '/config/apps/app[@order='||nAppId||']', 'sources', ' ');
    isr$XML.CreateNode(cxXML, '/config/apps/app[@order='||nAppId||']/sources','source','');
    isr$XML.setAttribute(cxXML, '/config/apps/app[@order='||nAppId||']/sources/source[position()=last()]', 'name', getFileName(STB$JOB.getJobParameter('TO', cnJobId)));
 end if;

  -- number OF APPS
  isr$XML.SetAttribute(cxXML, '/config/apps', 'num', isr$XML.valueOf(cxXML, 'count(/config/apps/app)'));

  isr$trace.debug('final status cxXml', 'cxXml: see column LOGCLOB', sCurrentName, cxXml);
  isr$trace.stat('end', 'Function '||sCurrentName||' finished',sCurrentName);
  return oErrorObj;

exception
  when others then
    sUserMsg := utd$msglib.getmsg ('exUnexpectedError', nCurrentLanguage,  csP1 => sqlerrm(sqlcode));
    sImplMsg := sCurrentName || rtrim(': '||sImplMsg,': ');
    sDevMsg  := substr(dbms_utility.format_error_stack ||crlf|| dbms_utility.format_error_backtrace, 1,4000);
    oErrorObj.handleError ( Stb$typedef.cnSeverityCritical, sUserMsg, sImplMsg, sDevMsg );
    Stb$job.markDBJobAsBroken(cnJobId, sIp, sPort, oErrorObj.sErrorCode, oErrorObj.sErrorMessage, oErrorObj.sSeverityCode);
    return oErrorObj;
end createParametersUpload;

--**********************************************************************************************************************
function createParametersCheckin( cnJobid in number,
                                  cxXml   in out xmltype )
  return STB$OERROR$RECORD
is
  sCurrentName   constant varchar2(100) := $$PLSQL_UNIT||'.createParametersCheckin (JobID='||cnJobId||')';
  sUserMsg               varchar2(4000);
  sDevMsg                varchar2(4000);
  sImplMsg               varchar2(4000);
  oErrorObj               STB$OERROR$RECORD :=STB$OERROR$RECORD();

  -- get the information of the leading document
  cursor cGetLeadingDocInfo(cnDocId in number) is
  select filename,
         docid,
         nodeid,
         nodeType,
         docType,
         mimetype,
         doc_oid
    from STB$DOCTRAIL
   where docid = cnDocId;

  rGetLeadingInfo cGetLeadingDocInfo%ROWTYPE;

  cursor cGetFiles is
    select f.fileid,
           f.blobflag,
           f.filename
      from isr$OUTPUT$FILE iof,
         isr$FILE f
     where iof.outputid = STB$JOB.GETJOBPARAMETER('OUTPUTID', cnJobId)
     and iof.fileid = f.fileid
     and f.download = 'T';

  sCheckOid    varchar2(1) := 'F';
  sWordmodule  varchar2(1) := 'F';
  nAppId       number;

  sProtected            varchar2(1);
  sPasswordTarget        STB$REPORT.PASSWORD%type;

begin
  isr$trace.stat('begin', 'Function '||sCurrentName||' started.', sCurrentName);
  isr$trace.debug('status cxXml', 'cxXml: see column LOGCLOB', sCurrentName, cxXml);

  sCheckOid := 'F';
  sWordmodule := 'F';
  for rGetFiles in cGetFiles loop
    oErrorObj := Stb$docks.copyFileToJob(rGetFiles.fileid,cnJobId,rGetFiles.blobflag);
    if lower(rGetFiles.filename) = 'checkoid.exe' then
      sCheckOid := 'T';
    elsif lower(rGetFiles.filename) = 'wordmodul.exe' then
      sWordmodule := 'T';
   end if;
  end loop;

  open cGetLeadingDocInfo(STB$JOB.getJobParameter('DOCID',cnJobId));
 fetch cGetLeadingDocInfo into rGetLeadingInfo;
  close cGetLeadingDocInfo;

  -- General parameters
  isr$XML.CreateNode(cxXML, '/config/apps', 'fullpathname', getPath(STB$JOB.getJobParameter('TO', cnJobId)));
  isr$XML.CreateNode(cxXML, '/config/apps', 'target', getFileName(STB$JOB.getJobParameter('TO', cnJobId)));

  -- ========= CHECKOID / WORDMODULE
  -- =======================
  if sCheckOid = 'T' then
    nAppId := isr$XML.valueOf(cxXML, 'count(/config/apps/app)') + 1;
    isr$XML.CreateNodeAttr(cxXML, '/config/apps', 'app', null, 'order', nAppId);
    isr$XML.CreateNode(cxXML, '/config/apps/app[@order='||nAppId||']', 'wait_for_process', 'true');
    isr$XML.SetAttribute(cxXML, '/config/apps/app[@order='||nAppId||']', 'name', 'APPL');
    isr$XML.CreateNode(cxXML, '/config/apps/app[@order='||nAppId||']', 'executable', 'checkoid.exe');
    isr$XML.CreateNode(cxXML, '/config/apps/app[@order='||nAppId||']', 'command', 'START');
    isr$XML.CreateNode(cxXML, '/config/apps/app[@order='||nAppId||']', 'parameter', rGetLeadingInfo.doc_oid);
    isr$XML.SetAttribute(cxXML, '/config/apps/app[@order='||nAppId||']/parameter[position()=last()]', 'order', 1);
    isr$XML.CreateNode(cxXML, '/config/apps/app[@order='||nAppId||']', 'parameter', STB$JOB.getJobParameter('TO', cnJobId));
    isr$XML.SetAttribute(cxXML, '/config/apps/app[@order='||nAppId||']/parameter[position()=last()]', 'order', 2);
 end if;

  if sWordmodule = 'T' then
    sProtected := STB$UTIL.GetReportParameterValue('PROTECTED', STB$JOB.getJobParameter ('REPID', cnJobId));

    if NVL(sProtected, 'F') = 'T' then

      nAppId := isr$XML.valueOf(cxXML, 'count(/config/apps/app)') + 1;
      isr$XML.CreateNodeAttr(cxXML,  '/config/apps', 'app', '', 'order', nAppId);
      isr$XML.SetAttribute(cxXML, '/config/apps/app[@order='||nAppId||']', 'name', 'wordmodul');
      isr$XML.CreateNode(cxXML, '/config/apps/app[@order='||nAppId||']', 'wait_for_process', 'true');
      isr$XML.CreateNode(cxXML, '/config/apps/app[@order='||nAppId||']', 'executable', 'wordmodul.exe');
      isr$XML.CreateNode(cxXML, '/config/apps/app[@order='||nAppId||']', 'parameter', '[config]');
      isr$XML.CreateNode(cxXML, '/config/apps/app[@order='||nAppId||']', 'command', 'CAN_CHECKIN');
      oErrorObj := Stb$docks.getPasswordFromDoc(STB$JOB.getJobParameter('DOCID', cnJobId), sPasswordTarget);
      isr$XML.CreateNode(cxXML, '/config/apps/app[@order='||nAppId||']', 'targetpassword', sPasswordTarget);
      isr$XML.CreateNode(cxXML, '/config/apps/app[@order='||nAppId||']', 'use-protection', 'T');

   end if;
 end if;

  -- number OF APPS
  isr$XML.SetAttribute(cxXML, '/config/apps', 'num', isr$XML.valueOf(cxXML, 'count(/config/apps/app)'));

  isr$trace.debug('final status cxXml', 'cxXml: see column LOGCLOB', sCurrentName, cxXml);
  isr$trace.stat('end', 'Function '||sCurrentName||' finished',sCurrentName);
  return oErrorObj;

exception
  when others then
    sUserMsg := utd$msglib.getmsg ('exUnexpectedError', nCurrentLanguage,  csP1 => sqlerrm(sqlcode));
    sImplMsg := sCurrentName || rtrim(': '||sImplMsg,': ');
    sDevMsg  := substr(dbms_utility.format_error_stack ||crlf|| dbms_utility.format_error_backtrace, 1,4000);
    oErrorObj.handleError ( Stb$typedef.cnSeverityCritical, sUserMsg, sImplMsg, sDevMsg );
    Stb$job.markDBJobAsBroken(cnJobId, sIp, sPort, oErrorObj.sErrorCode, oErrorObj.sErrorMessage, oErrorObj.sSeverityCode);
    return oErrorObj;
end createParametersCheckin;

--**********************************************************************************************************************
function checkout( cnJobid in     number,
                   cxXml   in out xmltype)
  return STB$OERROR$RECORD
is
  sCurrentName   constant varchar2(100) := $$PLSQL_UNIT||'.checkout (JobID='||cnJobId||')';
  sUserMsg               varchar2(4000);
  sDevMsg                varchar2(4000);
  sImplMsg               varchar2(4000);
  oErrorObj               STB$OERROR$RECORD :=STB$OERROR$RECORD();
  sPath                  STB$DOCTRAIL.Path%type;
  sName                  STB$DOCTRAIL.FILENAME%type;
begin
  isr$trace.stat('begin', 'Function '||sCurrentName||' started.', sCurrentName);

  -- update doctrail
  -- set the document in the stb$doctrail to identify that the document is checked out
  sPath  := getPath(STB$JOB.getJobParameter('TO',cnJobId));
  sName  := getFileName(STB$JOB.getJobParameter('TO',cnJobId));
  oErrorObj := STB$DOCKS.switchCheckoutFlag(STB$JOB.getJobParameter('DOCID',cnJobId),
                                         'T',
                                         STB$JOB.getJobParameter('USER', cnJobid),
                                         sIP,
                                         sPath,
                                         sName);

  isr$trace.stat('end', 'Function '||sCurrentName||' finished',sCurrentName);
  return oErrorObj;

exception
  when others then
    sUserMsg := utd$msglib.getmsg ('exUnexpectedError', nCurrentLanguage,  csP1 => sqlerrm(sqlcode));
    sImplMsg := sCurrentName || rtrim(': '||sImplMsg,': ');
    sDevMsg  := substr(dbms_utility.format_error_stack ||crlf|| dbms_utility.format_error_backtrace, 1,4000);
    oErrorObj.handleError ( Stb$typedef.cnSeverityCritical, sUserMsg, sImplMsg, sDevMsg );
    Stb$job.markDBJobAsBroken(cnJobId, sIp, sPort, oErrorObj.sErrorCode, oErrorObj.sErrorMessage, oErrorObj.sSeverityCode);
    return oErrorObj;
end checkout;

--******************************************************************************************************************************
function checkoutundo( cnJobid in     number,
                       cxXml   in out xmltype)
  return STB$OERROR$RECORD
is
  sCurrentName   constant varchar2(100) := $$PLSQL_UNIT||'.checkoutundo (JobID='||cnJobId||')';
  sUserMsg       varchar2(4000);
  sDevMsg        varchar2(4000);
  sImplMsg       varchar2(4000);
  oErrorObj      STB$OERROR$RECORD :=STB$OERROR$RECORD();
  sDocType       STB$DOCTRAIL.DOCTYPE%type;

  cursor cGetDocType is
    select doctype
    from stb$doctrail
    where docid = STB$JOB.getJobParameter('DOCID',cnJobId);

begin
  isr$trace.stat('begin', 'Function '||sCurrentName||' started.', sCurrentName);

  open cGetDocType;
 fetch cGetDocType into sDocType;
  close cGetDocType;

  if sDocType in ('D', 'W') then
    delete from isr$DOC$PROPERTY
    where  docid = STB$JOB.getJobParameter('DOCID', cnJobId);
    delete from STB$DOCTRAIL
    where  docid = STB$JOB.getJobParameter('DOCID', cnJobId);
  else
    oErrorObj := STB$DOCKS.switchCheckOutFlag(STB$JOB.getJobParameter('DOCID',cnJobId), 'F', STB$JOB.getJobParameter('USER', cnJobid));
 end if;

  isr$trace.stat('end', 'Function '||sCurrentName||' finished',sCurrentName);
  return oErrorObj;

exception
  when others then
    sUserMsg := utd$msglib.getmsg ('exUnexpectedError', nCurrentLanguage,  csP1 => sqlerrm(sqlcode));
    sImplMsg := sCurrentName || rtrim(': '||sImplMsg,': ');
    sDevMsg  := substr(dbms_utility.format_error_stack ||crlf|| dbms_utility.format_error_backtrace, 1,4000);
    oErrorObj.handleError ( Stb$typedef.cnSeverityCritical, sUserMsg, sImplMsg, sDevMsg );
    Stb$job.markDBJobAsBroken(cnJobId, sIp, sPort, oErrorObj.sErrorCode, oErrorObj.sErrorMessage, oErrorObj.sSeverityCode);
    return oErrorObj;
end checkoutundo;

--******************************************************************************************************************************
function checkin( cnJobid in     number,
                  cxXml   in out xmltype)
  return STB$OERROR$RECORD
is
  sCurrentName   constant varchar2(100) := $$PLSQL_UNIT||'.checkin (JobID='||cnJobId||')';
  sUserMsg       varchar2(4000);
  sDevMsg        varchar2(4000);
  sImplMsg       varchar2(4000);
  oErrorObj      STB$OERROR$RECORD :=STB$OERROR$RECORD();
  sName          STB$DOCTRAIL.FILENAME%type;
begin
  isr$trace.stat('begin', 'Function '||sCurrentName||' started.', sCurrentName);

  sName  := getFileName(STB$JOB.getJobParameter('TO',cnJobId));
  oErrorObj := STB$DOCKS.switchCheckOutFlag(STB$JOB.getJobParameter('DOCID',cnJobId), 'F', STB$JOB.getJobParameter('USER', cnJobid), null, null, sName);

  isr$trace.stat('end', 'Function '||sCurrentName||' finished',sCurrentName);
  return oErrorObj;

exception
  when others then
    sUserMsg := utd$msglib.getmsg ('exUnexpectedError', nCurrentLanguage,  csP1 => sqlerrm(sqlcode));
    sImplMsg := sCurrentName || rtrim(': '||sImplMsg,': ');
    sDevMsg  := substr(dbms_utility.format_error_stack ||crlf|| dbms_utility.format_error_backtrace, 1,4000);
    oErrorObj.handleError ( Stb$typedef.cnSeverityCritical, sUserMsg, sImplMsg, sDevMsg );
    Stb$job.markDBJobAsBroken(cnJobId, sIp, sPort, oErrorObj.sErrorCode, oErrorObj.sErrorMessage, oErrorObj.sSeverityCode);
    return oErrorObj;
end checkin;

--***********************************************************************************************************************
function deleteCommon( cnJobid in     number,
                       cxXml   in out xmltype)
  return STB$OERROR$RECORD
is
  sCurrentName   constant varchar2(100) := $$PLSQL_UNIT||'.deleteCommon (JobID='||cnJobId||')';
  sUserMsg               varchar2(4000);
  sDevMsg                varchar2(4000);
  sImplMsg               varchar2(4000);
  oErrorObj               STB$OERROR$RECORD :=STB$OERROR$RECORD();
begin
  isr$trace.stat('begin', 'Function '||sCurrentName||' started.', sCurrentName);
  isr$trace.debug('status Jobparameter', 'STB$JOB.getJobParameter(DOCID,cnJobId): '||STB$JOB.getJobParameter('DOCID',cnJobId), sCurrentName);

  delete from isr$DOC$PROPERTY where docid =STB$JOB.getJobParameter('DOCID',cnJobId);
  delete from STB$DOCTRAIL where docid =STB$JOB.getJobParameter('DOCID',cnJobId);
  commit;

  isr$trace.stat('end', 'Function '||sCurrentName||' finished',sCurrentName);
  return oErrorObj;

exception
  when others then
    sUserMsg := utd$msglib.getmsg ('exUnexpectedError', nCurrentLanguage,  csP1 => sqlerrm(sqlcode));
    sImplMsg := sCurrentName || rtrim(': '||sImplMsg,': ');
    sDevMsg  := substr(dbms_utility.format_error_stack ||crlf|| dbms_utility.format_error_backtrace, 1,4000);
    oErrorObj.handleError ( Stb$typedef.cnSeverityCritical, sUserMsg, sImplMsg, sDevMsg );
    Stb$job.markDBJobAsBroken(cnJobId, sIp, sPort, oErrorObj.sErrorCode, oErrorObj.sErrorMessage, oErrorObj.sSeverityCode);
    return oErrorObj;
end deleteCommon;

--***********************************************************************************************************************
function createParametersPdf( cnJobid in     number,
                              cxXml   in out xmltype)
  return STB$OERROR$RECORD
is
  sCurrentName   constant varchar2(100) := $$PLSQL_UNIT||'.createParametersPdf (JobID='||cnJobId||')';
  sUserMsg               varchar2(4000);
  sDevMsg                varchar2(4000);
  sImplMsg               varchar2(4000);
  oErrorObj               STB$OERROR$RECORD :=STB$OERROR$RECORD();
  sPassword              STB$REPORT.PASSWORD%type;

  cursor cGetDocInfo(cnDocId in STB$DOCTRAIL.DOCID%type) is
    select d.filename
      from STB$DOCTRAIL d, STB$JOBTRAIL jt
     where d.docId = cnDocId
       and jt.filename = d.filename
       and jt.jobid = cnJobid;

  sFileName    varchar2(500);
  sNewFileName varchar2(500);
  sPdfCreationMode varchar2(500);

  nAppId       number;

  sPDFOut      varchar2(4000);

begin
  isr$trace.stat('begin', 'Function '||sCurrentName||' started.', sCurrentName);
  isr$trace.debug('status cxXml', 'cxXml: see column LOGCLOB', sCurrentName, cxXml);

  -- ========= PDFMODUL
  -- =======================

  if isr$XML.valueOf(cxXML, 'count(/config/apps/fullpathname)') = 0 then
    isr$XML.CreateNode(cxXML, '/config/apps', 'fullpathname', csISRTempPath);
 end if;

  nAppId := isr$XML.valueOf(cxXML, 'count(/config/apps/app)') + 1;
  isr$XML.CreateNodeAttr(cxXML,  '/config/apps', 'app', '', 'order', nAppId);
  isr$XML.SetAttribute(cxXML, '/config/apps/app[@order='||nAppId||']', 'name', 'pdfmodul');
  isr$XML.CreateNode(cxXML, '/config/apps/app[@order='||nAppId||']', 'executable', 'pdfmodul.exe');
  isr$XML.CreateNode(cxXML, '/config/apps/app[@order='||nAppId||']', 'wait_for_process', 'true');
  isr$XML.CreateNode(cxXML, '/config/apps/app[@order='||nAppId||']', 'parameter', '[config]');

  open cGetDocInfo(STB$JOB.getJobParameter('DOCID', cnJobId));
 fetch cGetDocInfo into sFileName;
  if cGetDocInfo%NOTFOUND or TRIM(sFileName) is null then
    sFileName := NVL(NVL(REPLACE(ISR$XML.valueOf(cxXML, '/config/apps/app/parameter[@order=1][.!='''']'), csISRTempPath),
          isr$XML.valueOf(cxXML, '(/config/apps/app/target[.!=""])[1]/text()')), STB$JOB.getJobParameter('TO', cnJobId));
    isr$trace.debug('sFileName', sFileName, sCurrentName);
 end if;
  close cGetDocInfo;

  isr$XML.CreateNode(cxXML, '/config/apps/app[@order='||nAppId||']', 'command', Nvl(STB$JOB.getJobParameter('COMMAND', cnJobId), 'CAN_CREATE_PDF'));
  isr$XML.CreateNode(cxXML, '/config/apps/app[@order='||nAppId||']', 'source', sFileName);
  isr$XML.CreateNode(cxXML, '/config/apps/app[@order='||nAppId||']', 'tempfile', 'temp'||sFileName||'.doc' );
  isr$XML.CreateNode(cxXML, '/config/apps/app[@order='||nAppId||']', 'sourcetype', 'B');
  sNewFileName := case when upper(STB$JOB.getJobParameter('TO', cnJobId)) LIKE '%.PDF' then STB$JOB.getJobParameter('TO', cnJobId) else sFileName end;
  isr$XML.CreateNode(cxXML, '/config/apps/app[@order='||nAppId||']', 'target', SUBSTR(sNewFileName, 0, INSTR(sNewFileName, '.', -1) - 1)||'.pdf');
  if isr$XML.valueOf(cxXML, 'count(/config/apps/target)') = 0 then
    isr$XML.CreateNode(cxXML, '/config/apps', 'target', isr$XML.valueOf(cxXML, '/config/apps/app[@order='||nAppId||']/target/text()'));
 end if;
  isr$XML.CreateNode(cxXML, '/config/apps/app[@order='||nAppId||']', 'targettype', isr$XML.valueOf(cxXML, '/config/apps/app[@order='||nAppId||']/sourcetype/text()'));
  isr$XML.CreateNode(cxXML, '/config/apps/app[@order='||nAppId||']', 'mimetype', 'application/pdf');
  isr$XML.CreateNode(cxXML, '/config/apps/app[@order='||nAppId||']', 'isrdoctype', csTarget);
  isr$XML.CreateNode(cxXML, '/config/apps/app[@order='||nAppId||']', 'doc_definition', 'pdf');
  isr$XML.CreateNode(cxXML, '/config/apps/app[@order='||nAppId||']', 'append', null);

  for rGetMainPDF in (select d2.docid, d2.filename
                        from STB$DOCTRAIL d2, STB$DOCTRAIL d1
                       where d1.nodeid = d2.nodeid
                         and d1.nodetype = d2.nodetype
                         and d2.doc_definition = 'document_pdf'
                         and d1.docid = STB$JOB.getJobParameter('DOCID', cnJobId) ) loop
    isr$XML.CreateNode(cxXML, '/config/apps/app[@order='||nAppId||']/append', 'append', null);
    isr$XML.CreateNode(cxXML, '/config/apps/app[@order='||nAppId||']/append/append[position()=last()]', 'filename', rGetMainPDF.filename);
    isr$XML.CreateNode(cxXML, '/config/apps/app[@order='||nAppId||']/append/append[position()=last()]', 'headline', null);
    isr$XML.CreateNode(cxXML, '/config/apps/app[@order='||nAppId||']/append/append[position()=last()]', 'headlinepos', null);
  end loop;

  for rGetAppendPDFs in ( select d2.docid, d2.filename, headline.parametervalue headline, headlinepos.parametervalue headlinepos
                            from STB$DOCTRAIL d2, STB$DOCTRAIL d1, isr$DOC$PROPERTY position, isr$DOC$PROPERTY headline, isr$DOC$PROPERTY headlinepos
                           where d1.nodeid = d2.nodeid
                             and d1.nodetype = d2.nodetype
                             and d2.doc_definition = 'pdf_appendix'
                             and d1.docid = STB$JOB.getJobParameter('DOCID', cnJobId)
                             and d2.docid = headline.docid(+)
                             and headline.parametername(+) = 'HEADLINE_COMMON'
                             and d2.docid = headlinepos.docid(+)
                             and headlinepos.parametername(+) = 'HEADLINEPOS_COMMON'
                             and d2.docid = position.docid(+)
                             and position.parametername(+) = 'POSITION_COMMON'
                           order by TO_NUMBER(position.parametervalue) ) loop
    isr$XML.CreateNode(cxXML, '/config/apps/app[@order='||nAppId||']/append', 'append', null);
    isr$XML.CreateNode(cxXML, '/config/apps/app[@order='||nAppId||']/append/append[position()=last()]', 'filename', rGetAppendPDFs.filename);
    isr$XML.CreateNode(cxXML, '/config/apps/app[@order='||nAppId||']/append/append[position()=last()]', 'headline', rGetAppendPDFs.headline);
    isr$XML.CreateNode(cxXML, '/config/apps/app[@order='||nAppId||']/append/append[position()=last()]', 'headlinepos', rGetAppendPDFs.headlinepos);
  end loop;

  oErrorObj := Stb$docks.getPasswordFromDoc(STB$JOB.getJobParameter('DOCID', cnJobId),sPassword);
  if TRIM(sPassword) is null then
    sPassword := isr$XML.valueOf(cxXML, '(/config/apps/app/targetpassword[.!=""])[1]/text()');
 end if;
  isr$XML.CreateNode(cxXML, '/config/apps/app[@order='||nAppId||']', 'sourcepassword', sPassword);

  -- PDF properties
--  for rGetPDFProps in ( select *
--                          from isr$REPORT$GROUPING$TAB
--                         where groupingname LIKE 'pdf%'
--                           and repid = STB$JOB.getJobParameter('REPID', cnJobId) ) loop
--    isr$XML.CreateNode(cxXML, '/config/apps/app[@order='||nAppId||']', rGetPDFProps.groupingname, rGetPDFProps.groupingvalue);
--  end loop;

  begin
    execute immediate 'select '||STB$UTIL.getCurrentCustomPackage||'.createPDFOut(STB$JOB.getJobParameter(''REPID'', '||cnJobId||')) from dual' into sPDFOut;
    isr$XML.CreateNode(cxXML, '/config/apps/app[@order='||nAppId||']', 'pdfout', sPDFOut);
  exception when others then
    null;
  end;

  -- set the PDF Creation mode from the SystemParameter PDF_CREATION_MODE (W = Word; D=Distiller (Print); A = Acrobat (Default)
  sPdfCreationMode :=  Stb$util.getSystemParameter('PDF_CREATION_MODE');
  if sPdfCreationMode = 'W' then
    isr$XML.CreateNode(cxXML, '/config/apps/app[@order='||nAppId||']', 'SaveAsPDF', 'T');
    isr$XML.CreateNode(cxXML, '/config/apps/app[@order='||nAppId||']', 'ClassicPDFGeneration', 'F');
  elsif sPdfCreationMode = 'D' then
    isr$XML.CreateNode(cxXML, '/config/apps/app[@order='||nAppId||']', 'SaveAsPDF', 'F');
    isr$XML.CreateNode(cxXML, '/config/apps/app[@order='||nAppId||']', 'ClassicPDFGeneration', 'F');
  else
    -- Mode 'A' is default
    isr$XML.CreateNode(cxXML, '/config/apps/app[@order='||nAppId||']', 'SaveAsPDF', 'F');
    isr$XML.CreateNode(cxXML, '/config/apps/app[@order='||nAppId||']', 'ClassicPDFGeneration', 'T');
 end if;

  -- number OF APPS
  isr$XML.SetAttribute(cxXML, '/config/apps', 'num', isr$XML.valueOf(cxXML, 'count(/config/apps/app)'));

  isr$trace.debug('final status cxXml', 'cxXml: see column LOGCLOB', sCurrentName, cxXml);
  isr$trace.stat('end', 'Function '||sCurrentName||' finished',sCurrentName);
  return oErrorObj;

exception
  when others then
    sUserMsg := utd$msglib.getmsg ('exUnexpectedError', nCurrentLanguage,  csP1 => sqlerrm(sqlcode));
    sImplMsg := sCurrentName || rtrim(': '||sImplMsg,': ');
    sDevMsg  := dbms_utility.format_error_stack ||crlf|| dbms_utility.format_error_backtrace;
    oErrorObj.handleError ( Stb$typedef.cnSeverityCritical, sUserMsg, sImplMsg, sDevMsg );
    Stb$job.markDBJobAsBroken(cnJobId, sIp, sPort, oErrorObj.sErrorCode, oErrorObj.sErrorMessage, oErrorObj.sSeverityCode);
    return oErrorObj;
end createParametersPDF;

--***********************************************************************************************************************
function createParametersGeneral( cnJobid in     number,
                                  cxXml   in out xmltype)
  return STB$OERROR$RECORD
is
  sCurrentName   constant varchar2(100) := $$PLSQL_UNIT||'.createParametersGeneral (JobID='||cnJobId||')';
  sUserMsg               varchar2(4000);
  sDevMsg                varchar2(4000);
  sImplMsg               varchar2(4000);
  oErrorObj               STB$OERROR$RECORD :=STB$OERROR$RECORD();
  nCount                number;
  sNumberApps           varchar2(4000);
  sApp                   STB$SYSTEMPARAMETER.PARAMETERVALUE%type;
  csViewer              varchar2(200) :='viewer.exe';
  csExcel               varchar2(200) :='excel.exe';

  -- Get the application
  cursor cGetApp is
    select jt.filename, jt.documenttype
      from STB$JOBTRAIL jt
     where jt.jobid = cnJobId
       and jt.documenttype = 'APPL'
    order by entryid;

  cursor cGetFiles is
    select jt.filename, jt.documenttype
      from STB$JOBTRAIL jt
     where jt.jobid = cnJobId
       and (jt.documenttype = csRohReport
         or jt.documenttype = csTemplate
         or jt.documenttype = csTarget
         or jt.documenttype = 'TRANSFORMATION')
    order by entryid;

  cursor cGetNumberofApp is
    select TO_CHAR (COUNT (jt.filename)) app
      from STB$JOBTRAIL jt
     where jt.jobid = cnJobId
       and jt.documenttype = 'APPL';

begin
  isr$trace.stat('begin', 'Function '||sCurrentName||' started.', sCurrentName);
  isr$trace.debug('status cxXml', 'cxXml: see column LOGCLOB', sCurrentName, cxXml);

  -- number of applications comes out of the number of templates
  open  cGetNumberofApp ;
  fetch cGetNumberofApp  into sNumberApps;
  close cGetNumberofApp ;

  nCount :=0;
  -- use the general application set in the system parameters
  if sNumberApps = 0 then

    sApp := NVL(NVL(STB$JOB.getJobParameter('INVISIBLE_FILTER_AUDITAPPLICATION', cnJobId), STB$JOB.getJobParameter('FILTER_AUDITAPPLICATION', cnJobId)), STB$UTIL.getSystemParameter('AUDITAPPLICATION'));

    if sAPP = 'PDF' then

      for rGetFiles in cGetFiles loop
        --IF rGetFiles.documenttype in (csRohReport, csTarget) then
          STB$JOB.setJobParameter('TO', rGetFiles.FileName, cnJobId);
        --END IF;
      end loop;

      oErrorObj := copyFilesIntoJobtrail(cnJobid, cxXML);
      oErrorObj := createParametersPDF(cnJobid, cxXML);

      for rGetFiles in cGetFiles loop
        --IF rGetFiles.documenttype in (csRohReport, csTarget) then
          STB$JOB.setJobParameter('TO', getFileName(rGetFiles.FileName)||'.pdf', cnJobId);
        --END IF;
      end loop;

      oErrorObj := createParametersCommon(cnJobid, cxXML);

    else

      nCount := isr$XML.valueOf(cxXML, 'count(/config/apps/app)') + 1;

      isr$XML.CreateNodeAttr(cxXML,  '/config/apps', 'app', '', 'order', nCount);
      isr$XML.SetAttribute(cxXML, '/config/apps/app[@order='||nCount||']', 'name', 'APPL');
      isr$XML.CreateNode(cxXML, '/config/apps/app[@order='||nCount||']', 'wait_for_process', 'false');
      isr$XML.CreateNode(cxXML, '/config/apps/app[@order='||nCount||']', 'executable', sApp);
      isr$XML.CreateNode(cxXML, '/config/apps/app[@order='||nCount||']', 'command', 'START');

      for rGetFiles in cGetFiles loop
        if upper(sAPP) = upper(csExcel) or (UPPER(sAPP) != upper(csExcel) and rGetFiles.documenttype in (csRohReport, 'TRANSFORMATION', csTarget)) then
          isr$XML.CreateNode(cxXML, '/config/apps/app[@order='||nCount||']', 'parameter', csISRTempPath || rGetFiles.FileName);
          isr$XML.SetAttribute(cxXML, '/config/apps/app[@order='||nCount||']/parameter[position()=last()]', 'order', isr$XML.valueOf(cxXML, 'count(/config/apps/app[@order='||nCount||']/parameter)'));
       end if;
      end loop;

   end if;

  else

    -- application is set in the file table
    for rGetApp in cGetApp loop

      nCount := isr$XML.valueOf(cxXML, 'count(/config/apps/app)') + 1;

      isr$XML.CreateNodeAttr(cxXML,  '/config/apps', 'app', '', 'order', nCount);
      isr$XML.SetAttribute(cxXML, '/config/apps/app[@order='||nCount||']', 'name', 'APPL');
      if nCount < To_Number(sNumberApps) then
        isr$XML.CreateNode(cxXML, '/config/apps/app[@order='||nCount||']', 'wait_for_process', 'true');
      else
        isr$XML.CreateNode(cxXML, '/config/apps/app[@order='||nCount||']', 'wait_for_process', 'false');
     end if;
      isr$XML.CreateNode(cxXML, '/config/apps/app[@order='||nCount||']', 'executable',rGetApp.filename);
      isr$XML.CreateNode(cxXML, '/config/apps/app[@order='||nCount||']', 'command', 'START');

      -- Emailjob
      if STB$JOB.getJobParameter('EMAILADDRESS', cnJobId) is not null then
        isr$XML.CreateNode(cxXML, '/config/apps/app[@order='||nCount||']', 'parameter', '-to');
        isr$XML.SetAttribute(cxXML, '/config/apps/app[@order='||nCount||']/parameter[position()=last()]', 'order', isr$XML.valueOf(cxXML, 'count(/config/apps/app[@order='||nCount||']/parameter)'));
        isr$XML.CreateNode(cxXML, '/config/apps/app[@order='||nCount||']', 'parameter', STB$JOB.getJobParameter('EMAILADDRESS', cnJobId));
        isr$XML.SetAttribute(cxXML, '/config/apps/app[@order='||nCount||']/parameter[position()=last()]', 'order', isr$XML.valueOf(cxXML, 'count(/config/apps/app[@order='||nCount||']/parameter)'));
        isr$XML.CreateNode(cxXML, '/config/apps/app[@order='||nCount||']', 'parameter', '-files');
        isr$XML.SetAttribute(cxXML, '/config/apps/app[@order='||nCount||']/parameter[position()=last()]', 'order', isr$XML.valueOf(cxXML, 'count(/config/apps/app[@order='||nCount||']/parameter)'));
     end if;

      -- get the files
      for rGetFiles in cGetFiles loop
        if rGetApp.filename <> csViewer then
          isr$XML.CreateNode(cxXML, '/config/apps/app[@order='||nCount||']', 'parameter', csISRTempPath || rGetFiles.FileName);
        else
          if rGetFiles.documenttype != csRohReport then
            isr$XML.CreateNode  (cxXML, '/config/apps/app[@order='||nCount||']', 'parameter', '"dataUrl=file:/'||csISRTempPath || rGetFiles.FileName|| '"' );
         end if;
       end if;
        isr$XML.SetAttribute(cxXML, '/config/apps/app[@order='||nCount||']/parameter[position()=last()]', 'order', isr$XML.valueOf(cxXML, 'count(/config/apps/app[@order='||nCount||']/parameter)'));
      end loop;

    end loop;
 end if;

  -- Emailjob
  if STB$JOB.getJobParameter('EMAILADDRESS', cnJobId) is not null then
    for i in 1..ISR$XML.valueOf(cxXML, 'count(/config/apps/app[@name="ME"])') loop
      isr$XML.DeleteNode(cxXML, '/config/apps/app[@name="ME"]');
    end loop;
    if STB$JOB.getJobParameter('NODETYPE', cnJobId) != 'DOCUMENT'
    or upper(ISR$XML.valueOf(cxXML, '/config/apps/app[@name="wordmodul"]/target')) NOT LIKE '%.DOC' then
      for i in 1..ISR$XML.valueOf(cxXML, 'count(/config/apps/app[@name="wordmodul"])') loop
        isr$XML.DeleteNode(cxXML, '/config/apps/app[@name="wordmodul"]');
      end loop;
   end if;
 end if;

  -- number OF APPS
  isr$XML.SetAttribute(cxXML, '/config/apps', 'num', isr$XML.valueOf(cxXML, 'count(/config/apps/app)'));

  isr$trace.debug('final status cxXml', 'cxXml: see column LOGCLOB', sCurrentName, cxXml);
  isr$trace.stat('end', 'Function '||sCurrentName||' finished',sCurrentName);
  return oErrorObj;

exception
  when others then
    sUserMsg := utd$msglib.getmsg ('exUnexpectedError', nCurrentLanguage,  csP1 => sqlerrm(sqlcode));
    sImplMsg := sCurrentName || rtrim(': '||sImplMsg,': ');
    sDevMsg  := substr(dbms_utility.format_error_stack ||crlf|| dbms_utility.format_error_backtrace, 1,4000);
    oErrorObj.handleError ( Stb$typedef.cnSeverityCritical, sUserMsg, sImplMsg, sDevMsg );
    Stb$job.markDBJobAsBroken(cnJobId, sIp, sPort, oErrorObj.sErrorCode, oErrorObj.sErrorMessage, oErrorObj.sSeverityCode);
    return oErrorObj;
end createParametersGeneral;

--***********************************************************************************************************************
function createParametersArbitraryApp( cnJobid in     number,
                                       cxXml   in out xmltype)
  return STB$OERROR$RECORD
is
  sCurrentName constant varchar2(100) := $$PLSQL_UNIT||'.createParametersArbitraryApp (JobID='||cnJobId||')';
  oErrorObj             STB$OERROR$RECORD :=STB$OERROR$RECORD();
  sUserMsg              varchar2(4000);
  sDevMsg               varchar2(4000);
  sImplMsg              varchar2(4000);
  nCount                number;
  sAppsPath    constant varchar2(100) := '/config/apps';
  xAppPiece             xmltype;
  sDocType              varchar2(100);

  cursor cGetFile is
  select jt.filename, jt.documenttype
    from STB$JOBTRAIL jt
   where jt.jobid = cnJobId
     and jt.documenttype = sDocType
  order by entryid;

  rGetFile cGetFile%rowtype;
begin
  isr$trace.stat('begin', 'Function '||sCurrentName||' started.', sCurrentName);
  isr$trace.debug('status cxXml', 'cxXml: see column LOGCLOB', sCurrentName, cxXml);

  --the xml may already have app entries? Count them
  nCount := isr$XML.valueOf(cxXML, 'count('||sAppsPath||'/app)') + 1;
  isr$trace.debug('nCount', nCount, sCurrentName);

  -- build the app xml piece
  SELECT xmlelement("app", xmlattributes('ME' as "name", nCount as "order"),
           xmlelement("wait_for_process", 'false'),
           xmlelement("command", 'DISPLAY'))
         INTO xAppPiece
  from dual;

  -- append the app xml piece into the config xml
  -- this function should have another name
  cxXML := isr$XML.InsertChildXMLFragment(cxXML, sAppsPath, xAppPiece);

  -- add the general parameters like target, fullpathname
  isr$XML.CreateNode(cxXML, sAppsPath, 'fullpathname', csISRTempPath);
  -- for target: set doctype first
  sDocType := STB$JOB.getJobParameter('NODETYPE',cnJobId);
  if sDocType = 'RAWDATA' then
    sDocType := csRohReport;
  else
    sDocType := csTarget;
  end if;
  open cGetFile;
  fetch cGetFile into rGetFile;
  close cGetFile;
  isr$XML.CreateNode(cxXML, sAppsPath, 'target', rGetFile.filename);

  -- number OF APPS
  isr$XML.SetAttribute(cxXML, sAppsPath, 'num', nCount);

  isr$trace.debug('final status cxXml', 'cxXml: see column LOGCLOB', sCurrentName, cxXml);
  isr$trace.stat('end', 'Function '||sCurrentName||' finished',sCurrentName);
  return oErrorObj;

exception
  when others then
    sUserMsg := utd$msglib.getmsg ('exUnexpectedError', nCurrentLanguage,  csP1 => sqlerrm(sqlcode));
    sImplMsg := sCurrentName || rtrim(': '||sImplMsg,': ');
    sDevMsg  := substr(dbms_utility.format_error_stack ||crlf|| dbms_utility.format_error_backtrace, 1,4000);
    oErrorObj.handleError ( Stb$typedef.cnSeverityCritical, sUserMsg, sImplMsg, sDevMsg );
    Stb$job.markDBJobAsBroken(cnJobId, sIp, sPort, oErrorObj.sErrorCode, oErrorObj.sErrorMessage, oErrorObj.sSeverityCode);
    return oErrorObj;
end createParametersArbitraryApp;

--***********************************************************************************************************************
function copyDocumentIntoDoctrail( cnJobid in     number,
                                   cxXml   in out xmltype)
  return STB$OERROR$RECORD
is
  sCurrentName   constant varchar2(100) := $$PLSQL_UNIT||'.copyDocumentIntoDoctrail (JobID='||cnJobId||')';
  sUserMsg               varchar2(4000);
  sDevMsg                varchar2(4000);
  sImplMsg               varchar2(4000);
  oErrorObj               STB$OERROR$RECORD :=STB$OERROR$RECORD();
  sNodeTyp               varchar2(100) := 'REPORT';

  cursor cGetDocIds is
  select max(dt.DOCID) docid, jt.ENTRYID
  from   STB$DOCTRAIL dt, STB$JOBTRAIL jt
  where  dt.nodeid = to_char(STB$JOB.getJobParameter('REPID',cnJobId))
  and    jt.DOCUMENTTYPE IN('D',csTarget)
  and    ( jt.doc_definition = dt.doc_definition )
  and    jt.jobid = cnJobId
  group by jt.ENTRYID
  order by entryid;

begin
  isr$trace.stat('begin', 'Function '||sCurrentName||' started.', sCurrentName);

  if STB$JOB.getJobParameter('TOKEN',cnJobid) = csOffshoot then
    isr$trace.debug('end - offshoot', 'end - offshoot', sCurrentName);
    return oErrorObj;
 end if;

  isr$trace.debug('status repid', 'STB$JOB.getJobParameter(REPID,cnJobId): '||STB$JOB.getJobParameter('REPID',cnJobId), sCurrentName);

  for rGetDocIds in cGetDocIds loop

    oErrorObj := Stb$docks.updateDoctrail( rGetDocIds.DOCID,
                                        STB$JOB.getJobParameter('REPID',cnJobId),
                                        rGetDocIds.ENTRYID,
                                        sNodeTyp,
                                        STB$JOB.getJobParameter('COMMENT',cnJobId) );

  end loop;

  isr$trace.stat('end', 'Function '||sCurrentName||' finished',sCurrentName);
  return oErrorObj;

exception
  when others then
    rollback;
    sUserMsg := utd$msglib.getmsg ('exUnexpectedError', nCurrentLanguage,  csP1 => sqlerrm(sqlcode));
    sImplMsg := sCurrentName || rtrim(': '||sImplMsg,': ');
    sDevMsg  := substr(dbms_utility.format_error_stack ||crlf|| dbms_utility.format_error_backtrace, 1,4000);
    oErrorObj.handleError ( Stb$typedef.cnSeverityCritical, sUserMsg, sImplMsg, sDevMsg );
    Stb$job.markDBJobAsBroken(cnJobId, sIp, sPort, oErrorObj.sErrorCode, oErrorObj.sErrorMessage, oErrorObj.sSeverityCode);
    return oErrorObj;
end copyDocumentIntoDoctrail;

--*****************************************************************************************************************
function copySnapshotIntoDoctrail( cnJobid in     number,
                                   cxXml   in out xmltype)
  return STB$OERROR$RECORD
is
  sCurrentName   constant varchar2(100) := $$PLSQL_UNIT||'.copySnapshotIntoDoctrail (JobID='||cnJobId||')';
  sUserMsg                varchar2(4000);
  sDevMsg                 varchar2(4000);
  sImplMsg                varchar2(4000);
  oErrorObj               STB$OERROR$RECORD :=STB$OERROR$RECORD();
  exNoSnapshotFileCreated exception;
  sNodeTyp                varchar2(100) := 'REPORT';

  cursor cGetEntryId is
    select jt.entryid
      from STB$JOBTRAIL jt
     where jt.documenttype = csRemoteSnapshot
       and jt.jobid = cnJobId
       and dbms_lob.getlength(textfile) > 0
       and textfile is not null;

  rGetEntryId cGetEntryId%ROWTYPE;

begin
  isr$trace.stat('begin', 'Function '||sCurrentName||' started.', sCurrentName);

  if STB$JOB.getJobParameter('TOKEN',cnJobid) = csOffshoot then
    isr$trace.debug('end - offshoot','end - offshoot', sCurrentName);
    return oErrorObj;
  end if;

  open cGetEntryId;
  fetch cGetEntryId into rGetEntryId;
  if cGetEntryId%NOTFOUND and STB$JOB.getJobParameter('FINALFLAG', cnJobid) is null then
    close cGetEntryId;
    raise exNoSnapshotFileCreated;
  end if;
  close cGetEntryId;
  isr$trace.debug('status entryid', 'rGetEntryId.entryid: '||rGetEntryId.entryid, sCurrentName);

  -- copy the snapshot xml file
  oErrorObj := Stb$docks.copyXMLToDoc( rGetEntryId.entryid,
                                    sNodeTyp,
                                    STB$JOB.getJobParameter('REPID',cnJobId),
                                    STB$JOB.getJobParameter('USER',cnJobId),
                                    STB$JOB.getJobParameter('REPORTTYPEID',cnJobId),
                                    'S' );

  isr$trace.stat('end', 'Function '||sCurrentName||' finished',sCurrentName);
  return oErrorObj;

exception
  when exNoSnapshotFileCreated then
    sUserMsg := utd$msglib.getmsg ('exNoSnapshotFileCreatedText', nCurrentLanguage,  csP1 => sqlerrm(sqlcode));
    sImplMsg := sCurrentName || rtrim(': '||sImplMsg,': ');
    sDevMsg  := substr(dbms_utility.format_error_stack ||crlf|| dbms_utility.format_error_backtrace, 1,4000);
    oErrorObj.handleError ( Stb$typedef.cnSeverityCritical, sUserMsg, sImplMsg, sDevMsg );
    Stb$job.markDBJobAsBroken(cnJobId, sIp, sPort, oErrorObj.sErrorCode, oErrorObj.sErrorMessage, oErrorObj.sSeverityCode);
    return oErrorObj;
  when others then
    sUserMsg := utd$msglib.getmsg ('exUnexpectedError', nCurrentLanguage,  csP1 => sqlerrm(sqlcode));
    sImplMsg := sCurrentName || rtrim(': '||sImplMsg,': ');
    sDevMsg  := substr(dbms_utility.format_error_stack ||crlf|| dbms_utility.format_error_backtrace, 1,4000);
    oErrorObj.handleError ( Stb$typedef.cnSeverityCritical, sUserMsg, sImplMsg, sDevMsg );
    Stb$job.markDBJobAsBroken(cnJobId, sIp, sPort, oErrorObj.sErrorCode, oErrorObj.sErrorMessage, oErrorObj.sSeverityCode);
    return oErrorObj;
end copySnapshotIntoDoctrail;

--*****************************************************************************************************************
function copyXmlIntoDoctrail( cnJobid in     number,
                              cxXml   in out xmltype)
  return STB$OERROR$RECORD
is
  sCurrentName   constant varchar2(100) := $$PLSQL_UNIT||'.copyXmlIntoDoctrail (JobID='||cnJobId||')';
  sUserMsg               varchar2(4000);
  sDevMsg                varchar2(4000);
  sImplMsg               varchar2(4000);
  oErrorObj               STB$OERROR$RECORD :=STB$OERROR$RECORD();
  exNoRawdataFileCreated exception;
  sNodeTyp               varchar2(100) := 'REPORT';

  cursor cGetEntryId is
    select jt.entryid
      from STB$JOBTRAIL jt
     where jt.documenttype = csRohDaten
       and jt.jobid = cnJobId
       and dbms_lob.getlength(textfile) > 0
       and textfile is not null;

  rGetEntryId cGetEntryId%ROWTYPE;

begin
  isr$trace.stat('begin', 'Function '||sCurrentName||' started.', sCurrentName);

  if STB$JOB.getJobParameter('TOKEN',cnJobid) = csOffshoot then
    isr$trace.debug('end - offshoot','end - offshoot', sCurrentName);
    return oErrorObj;
 end if;

  open cGetEntryId;
 fetch cGetEntryId into rGetEntryId;
  if cGetEntryId%NOTFOUND and STB$JOB.getJobParameter('FINALFLAG', cnJobid) is null then
    close cGetEntryId;
    raise exNoRawdataFileCreated;
 end if;
  close cGetEntryId;
  isr$trace.debug('status entryid', 'rGetEntryId.entryid: '||rGetEntryId.entryid, sCurrentName);

  -- copy the xml file
  oErrorObj := Stb$docks.copyXMLToDoc( rGetEntryId.entryid,
                                    sNodeTyp,
                                    STB$JOB.getJobParameter('REPID',cnJobId),
                                    STB$JOB.getJobParameter('USER',cnJobId),
                                    STB$JOB.getJobParameter('REPORTTYPEID',cnJobId),
                                    'X' );

  isr$trace.stat('end', 'Function '||sCurrentName||' finished',sCurrentName);
  return oErrorObj;

exception
  when exNoRawdataFileCreated then
    sUserMsg := utd$msglib.getmsg ('exNoRawdataFileCreatedText', nCurrentLanguage,  csP1 => sqlerrm(sqlcode));
    sImplMsg := sCurrentName || rtrim(': '||sImplMsg,': ');
    sDevMsg  := substr(dbms_utility.format_error_stack ||crlf|| dbms_utility.format_error_backtrace, 1,4000);
    oErrorObj.handleError ( Stb$typedef.cnSeverityCritical, sUserMsg, sImplMsg, sDevMsg );
    Stb$job.markDBJobAsBroken(cnJobId, sIp, sPort, oErrorObj.sErrorCode, oErrorObj.sErrorMessage, oErrorObj.sSeverityCode);
    return oErrorObj;
  when others then
    sUserMsg := utd$msglib.getmsg ('exUnexpectedError', nCurrentLanguage,  csP1 => sqlerrm(sqlcode));
    sImplMsg := sCurrentName || rtrim(': '||sImplMsg,': ');
    sDevMsg  := substr(dbms_utility.format_error_stack ||crlf|| dbms_utility.format_error_backtrace, 1,4000);
    oErrorObj.handleError ( Stb$typedef.cnSeverityCritical, sUserMsg, sImplMsg, sDevMsg );
    Stb$job.markDBJobAsBroken(cnJobId, sIp, sPort, oErrorObj.sErrorCode, oErrorObj.sErrorMessage, oErrorObj.sSeverityCode);
    return oErrorObj;
end copyXmlIntoDoctrail;

--*****************************************************************************************************************
function copyClobIntoDoctrail( cnJobid in     number,
                               cxXml   in out xmltype)
  return STB$OERROR$RECORD
is
  sCurrentName   constant varchar2(100) := $$PLSQL_UNIT||'.copyClobIntoDoctrail (JobID='||cnJobId||')';
  sUserMsg               varchar2(4000);
  sDevMsg                varchar2(4000);
  sImplMsg               varchar2(4000);
  oErrorObj               STB$OERROR$RECORD :=STB$OERROR$RECORD();
  sNodeTyp               varchar2(100) := 'REPORT';

  cursor cGetDocId is
    select jt.entryid
    from STB$JOBTRAIL jt
   where jt.documenttype = csRohReport
     and jt.jobid= cnJobId;

begin
  isr$trace.stat('begin', 'Function '||sCurrentName||' started.', sCurrentName);

  if STB$JOB.getJobParameter('TOKEN',cnJobid) = csOffshoot then
    isr$trace.debug('end - offshoot','end - offshoot', sCurrentName);
    return oErrorObj;
 end if;

  for rGetDocId in cGetDocid loop
    oErrorObj := Stb$docks.copyClobToDoc(rGetDocId.entryid, sNodeTyp, STB$JOB.getJobParameter('REPID',cnJobId), STB$JOB.getJobParameter('USER',cnJobId));
  end loop;

  isr$trace.stat('end', 'Function '||sCurrentName||' finished',sCurrentName);
  return oErrorObj;

exception
  when others then
    rollback;
    sUserMsg := utd$msglib.getmsg ('exUnexpectedError', nCurrentLanguage,  csP1 => sqlerrm(sqlcode));
    sImplMsg := sCurrentName || rtrim(': '||sImplMsg,': ');
    sDevMsg  := substr(dbms_utility.format_error_stack ||crlf|| dbms_utility.format_error_backtrace, 1,4000);
    oErrorObj.handleError ( Stb$typedef.cnSeverityCritical, sUserMsg, sImplMsg, sDevMsg );
    Stb$job.markDBJobAsBroken(cnJobId, sIp, sPort, oErrorObj.sErrorCode, oErrorObj.sErrorMessage, oErrorObj.sSeverityCode);
    return oErrorObj;
end copyClobIntoDoctrail;

--***********************************************************************************************************************
function copyCommonIntoDoctrail( cnJobid in     number,
                                 cxXml   in out xmltype)
  return STB$OERROR$RECORD
is
  exWrongDoc              exception;
  exErrorInComp           exception;
  sCurrentName   constant varchar2(100) := $$PLSQL_UNIT||'.copyCommonIntoDoctrail (JobID='||cnJobId||')';
  sUserMsg                varchar2(4000);
  sDevMsg                 varchar2(4000);
  sImplMsg                varchar2(4000);
  oErrorObj               STB$OERROR$RECORD :=STB$OERROR$RECORD();
  sDocOid                 STB$DOCTRAIL.DOC_OID%type;
  sNodeTyp                varchar2(100) := Nvl(STB$JOB.getJobParameter('NODETYPE',cnJobId),'REPORT');
  sNodeId                 varchar2(100) := Nvl(STB$JOB.getJobParameter('REPID',cnJobId),STB$JOB.getJobParameter('NODEID',cnJobId));

  cursor cGetFiles is
    select jt.entryid,
           jt.filename,
           jt.reference,
           jt.binaryfile,
           d.doctype,
           d.nodeid
      from STB$JOBTRAIL jt, STB$DOCTRAIL d
     where jt.documenttype = csTarget
       and jt.jobid = cnJobId
       and d.docid(+) = jt.reference;

  cursor cGetDocOid(csReference in varchar2) is
    select doc_oid
      from stb$doctrail
     where docid = csReference;

  cursor cGetDocID(csFileName in varchar2) is
    select docid
      from stb$doctrail
     where filename = csFileName
       and nodeid = sNodeId
       and nodetype = case when sNodeTyp in ('COMMONDOC', 'DOCUMENT') then 'REPORT' else sNodeTyp END
       and doctype = 'C';

  nDocId    number;

  sMatchDocs varchar2(4000);
begin
  isr$trace.stat('begin', 'Function '||sCurrentName||' started.', sCurrentName);

  if STB$JOB.getJobParameter('TOKEN',cnJobid) = csOffshoot then
    isr$trace.debug('end - offshoot','end - offshoot', sCurrentName);
    return oErrorObj;
 end if;
  isr$trace.debug('status sNodeTyp sNodeId','sNodeTyp: '||sNodeTyp||'  sNodeId: '||sNodeId, sCurrentName);

  for rGetFiles in cGetFiles loop
    isr$trace.debug('status filename', 'rGetFiles.filename: '||rGetFiles.filename, sCurrentName);
    isr$trace.debug('status doctype', 'rGetFiles.doctype: '||rGetFiles.doctype, sCurrentName);
    if STB$JOB.getJobParameter('TOKEN',cnJobId) = 'CAN_CREATE_PDF' then
      if NVL(rGetFiles.doctype, 'C') = 'C' then
        open cGetDocID(rGetFiles.filename);
       fetch cGetDocID into nDocId;
        close cGetDocID;
        isr$trace.debug('status docid', 'nDocId: '||nDocId, sCurrentName);
        oErrorObj := Stb$docks.copyCommonToDoc( rGetFiles.entryid,
                                             case when sNodeTyp in ('COMMONDOC', 'DOCUMENT') then 'REPORT' else sNodeTyp end,
                                             sNodeId,
                                             STB$JOB.getJobParameter('USER',cnJobId),
                                             rGetFiles.filename,
                                             nDocId,
                                             STB$JOB.getJobParameter('REPORTTYPEID',cnJobId),
                                             STB$JOB.getJobParameter('COMMENT',cnJobId),
                                             'C',
                                             'document_pdf');
     end if;
    elsif rGetFiles.doctype = 'D' or (STB$JOB.getJobParameter('DOCID',cnJobId) is not null and NVL(rGetFiles.doctype, 'D') != 'C') then
      open  cGetDocOid(rGetFiles.reference);
      fetch cGetDocOid into sDocOid;
      close cGetDocOid;
      isr$trace.debug('status sDocOid', 'sDocOid: '||sDocOid, sCurrentName);

      sMatchDocs := ISR$LOADER.compareDocumentOID(sip, sport, sContext, sNamespace, 
            sDocOid, rGetFiles.binaryfile, to_number(stimeout));
       
      if sMatchDocs = 'T' or rGetFiles.doctype is null then
        oErrorObj := Stb$docks.updateDoctrail( STB$JOB.getJobParameter('DOCID',cnJobId),
                                            sNodeId,
                                            rGetFiles.entryid,
                                            sNodeTyp,
                                            STB$JOB.getJobParameter('COMMENT',cnJobId) );
      elsif sMatchDocs = 'F' then
        sImplMsg :=    'sDocOid: '||sDocOid||crlf
                    || 'sMatchDocs: '||sMatchDocs;
        raise exWrongDoc;
      else
        sImplMsg := 'sDocOid: '||sDocOid;
        raise exErrorInComp;
     end if;
    else
      if STB$JOB.getJobParameter('DOCID',cnJobId) is null then
        isr$trace.debug('status filename', 'rGetFiles.filename: '||rGetFiles.filename, sCurrentName);
        open  cGetDocID(rGetFiles.filename);
        fetch cGetDocID into nDocId;
        close cGetDocID;
      else
        nDocId := STB$JOB.getJobParameter('DOCID',cnJobId);
     end if;
      isr$trace.debug('status nDocId', 'nDocId: '||nDocId, sCurrentName);
      isr$trace.debug('status doctype', NVL(STB$JOB.getJobParameter('DOCTYPE',cnJobId),'C'), sCurrentName);
      isr$trace.debug('status DOCDEFINITION', NVL(STB$JOB.getJobParameter('DOCDEFINITION',cnJobId), case when NVL(STB$JOB.getJobParameter('DOC_DEFINITION_PDF',cnJobId), 'F') = rGetFiles.filename then 'document_pdf' else '' end), sCurrentName);

      oErrorObj := Stb$docks.copyCommonToDoc( rGetFiles.entryid,
                                           sNodeTyp,
                                           sNodeId,
                                           STB$JOB.getJobParameter('USER',cnJobId),
                                           rGetFiles.filename,
                                           nDocId,
                                           STB$JOB.getJobParameter('REPORTTYPEID',cnJobId),
                                           STB$JOB.getJobParameter('COMMENT',cnJobId),
                                           NVL(STB$JOB.getJobParameter('DOCTYPE',cnJobId),'C'),
                                           NVL(STB$JOB.getJobParameter('DOCDEFINITION',cnJobId), case when NVL(STB$JOB.getJobParameter('DOC_DEFINITION_PDF',cnJobId), 'F') = rGetFiles.filename then 'document_pdf' else '' end));
   end if;
  end loop;

  isr$trace.stat('end', 'Function '||sCurrentName||' finished',sCurrentName);
  return oErrorObj;

exception
  when exWrongDoc then
    rollback;
    sUserMsg := utd$msglib.getmsg ('exWrongDocText', nCurrentLanguage);
    sImplMsg := sCurrentName || rtrim(': '||sImplMsg,': ');
    sDevMsg  := substr(dbms_utility.format_error_stack ||crlf|| dbms_utility.format_error_backtrace, 1,4000);
    oErrorObj.handleError ( Stb$typedef.cnSeverityCritical, sUserMsg, sImplMsg, sDevMsg );
    Stb$job.markDBJobAsBroken(cnJobId, sIp, sPort, oErrorObj.sErrorCode, oErrorObj.sErrorMessage, oErrorObj.sSeverityCode);
    Stb$job.markDBJobAsBroken(cnJobId, sIp, sPort, oErrorObj.sErrorCode, oErrorObj.sErrorMessage, oErrorObj.sSeverityCode);
    return oErrorObj;
  when exErrorInComp then
    rollback;
    sUserMsg := utd$msglib.getmsg ('exErrorInCompText', nCurrentLanguage,  csP1 => sqlerrm(sqlcode));
    sImplMsg := sCurrentName || rtrim(': '||sImplMsg,': ');
    sDevMsg  := substr(dbms_utility.format_error_stack ||crlf|| dbms_utility.format_error_backtrace, 1,4000);
    oErrorObj.handleError ( Stb$typedef.cnSeverityCritical, sUserMsg, sImplMsg, sDevMsg );
    Stb$job.markDBJobAsBroken(cnJobId, sIp, sPort, oErrorObj.sErrorCode, oErrorObj.sErrorMessage, oErrorObj.sSeverityCode);
    return oErrorObj;
  when others then
    rollback;
    sUserMsg := utd$msglib.getmsg ('exUnexpectedError', nCurrentLanguage,  csP1 => sqlerrm(sqlcode));
    sImplMsg := sCurrentName || rtrim(': '||sImplMsg,': ');
    sDevMsg  := substr(dbms_utility.format_error_stack ||crlf|| dbms_utility.format_error_backtrace, 1,4000);
    oErrorObj.handleError ( Stb$typedef.cnSeverityCritical, sUserMsg, sImplMsg, sDevMsg );
    Stb$job.markDBJobAsBroken(cnJobId, sIp, sPort, oErrorObj.sErrorCode, oErrorObj.sErrorMessage, oErrorObj.sSeverityCode);
    return oErrorObj;
end copyCommonIntoDoctrail;

--****************************************************************************************************************************
function copyCommonIntoJobtrail( cnJobid in     number,
                                 cxXml   in out xmltype)
  return STB$OERROR$RECORD
is
  sCurrentName   constant varchar2(100) := $$PLSQL_UNIT||'.copyCommonIntoJobtrail (JobID='||cnJobId||')';
  sUserMsg               varchar2(4000);
  sDevMsg                varchar2(4000);
  sImplMsg               varchar2(4000);
  oErrorObj               STB$OERROR$RECORD :=STB$OERROR$RECORD();

  -- the common documents to a docid are copied
  cursor cGetAllDocs(cnDocid in STB$DOCTRAIL.DOCID%type) is
    select d2.docid
    from STB$DOCTRAIL d1,
         STB$DOCTRAIL d2
   where d1.nodetype = d2.nodetype
     and ( d1.nodeid = d2.nodeid or d2.nodeid = STB$JOB.getJobParameter('HASHVALUE',cnJobId))
     and To_Char(d1.docid) = cnDocid
     and d2.doctype = 'C';

begin
  isr$trace.stat('begin', 'Function '||sCurrentName||' started.', sCurrentName);

  -- get the ids of all common documents and copy them into the jobtrail
  for rGetAllDocs in cGetAllDocs(STB$JOB.getJobParameter('DOCID',cnJobId)) loop
    oErrorObj := Stb$docks.copyDocToJob(rGetAllDocs.docid,cnJobId);
  end loop;

  isr$trace.stat('end', 'Function '||sCurrentName||' finished',sCurrentName);
  return oErrorObj;

exception
  when others then
    sUserMsg := utd$msglib.getmsg ('exUnexpectedError', nCurrentLanguage,  csP1 => sqlerrm(sqlcode));
    sImplMsg := sCurrentName || rtrim(': '||sImplMsg,': ');
    sDevMsg  := substr(dbms_utility.format_error_stack ||crlf|| dbms_utility.format_error_backtrace, 1,4000);
    oErrorObj.handleError ( Stb$typedef.cnSeverityCritical, sUserMsg, sImplMsg, sDevMsg );
    Stb$job.markDBJobAsBroken(cnJobId, sIp, sPort, oErrorObj.sErrorCode, oErrorObj.sErrorMessage, oErrorObj.sSeverityCode);
    return oErrorObj;
end copyCommonIntoJobtrail;

--**************************************************************************************************************************
function startAppCond( cnJobid in     number,
                       cxXml   in out xmltype)
  return STB$OERROR$RECORD
is
  exUploadFileError  exception;
  sCurrentName       constant varchar2(100) := $$PLSQL_UNIT||'.startAppCond (JobID='||cnJobId||')';
  sUserMsg           varchar2(4000);
  sDevMsg            varchar2(4000);
  sImplMsg           varchar2(4000);
  oErrorObj          STB$OERROR$RECORD :=STB$OERROR$RECORD();

  -- offshoot
  sclDom             STB$JOBTRAIL.TEXTFILE%type;
  nJavaRet           number;
  sReturn            varchar2(4000);
  nOutputid          isr$REPORT$OUTPUT$TYPE.OUTPUTID%type;
  nNewDocId          STB$DOCTRAIL.DOCID%type;
  nActionid          isr$ACTION.ACTIONID%type;
  sFileName          varchar2(4000);
  sFileExtension     VARCHAR2(10);

  cursor cGetDocId is
  select dt.DOCID
  from   STB$DOCTRAIL dt, STB$JOBTRAIL jt
  where  dt.nodeid = To_Char(STB$JOB.getJobParameter('REPID',cnJobId))
  and    jt.DOCUMENTTYPE IN('D',csTarget)
  and    ( jt.doc_definition = dt.doc_definition )
  and    jt.jobid = cnJobId;

  cursor cGetJobIDs is
    select ao.outputid,
           ao.actionid
    from   isr$action A, isr$report$output$type o, isr$action$output ao
    where  ( upper(o.token) = upper('CAN_CHECKOUT') or upper( A.token ) = upper('CAN_CHECKOUT') )
      and  o.reporttypeid is null
      and  o.outputid = ao.outputid
      and  A.actionid = ao.actionid
      and ao.doc_definition = nvl(STB$DOCKS.getDocDefType(STB$JOB.getJobParameter('DOCID',cnJobid)),'NOT_DETERMINED');

begin
  isr$trace.stat('begin', 'Function '||sCurrentName||' started.', sCurrentName);

  if Nvl(STB$JOB.getJobParameter('ACTIONTYPE',cnJobid),'NOT_DEFINED') = 'SAVE' then

    -- get the doc id
    for rGetDocId in cGetDocId  loop

      oErrorObj := STB$DOCKS.copyDocToDoc(rGetDocId.docid, STB$JOB.getJobParameter('REPID',cnJobid), nNewDocId);
      isr$trace.debug('status nNewDocId', 'nNewDocId: '||nNewDocId, sCurrentName);

      Stb$object.SetCurrentDocId(nNewDocId);

      update stb$doctrail set job_flag = 'D' where docid = nNewDocId;
      commit;

      -- start checkout job
      STB$JOB.setJobParameter('TO', STB$JOB.getJobParameter('FILTER_TO',cnJobid), cnJobid);
      STB$JOB.setJobParameter('OVERWRITE', STB$JOB.getJobParameter('FILTER_OVERWRITE',cnJobid), cnJobid);
      STB$JOB.setJobParameter('DISPLAY_DOC', STB$JOB.getJobParameter('FILTER_DISPLAY_DOC',cnJobid), cnJobid);
      STB$JOB.setJobParameter('REFERENCE', nNewDocId, cnJobid);
      STB$JOB.setJobParameter('OLDDOCID', rGetDocId.docid, cnJobid);
      STB$JOB.setJobParameter('DOCID', nNewDocId, cnJobid);
      STB$JOB.setJobParameter('ACTIONTYPE', 'SAVE', cnJobid);
      STB$JOB.setJobParameter('TOKEN', 'CAN_CHECKOUT', cnJobid);
      STB$JOB.setJobParameter('START_LOADER', 'F', cnJobid);
      STB$JOB.setJobParameter('END_LOADER', 'F', cnJobid);

      open cGetJobIDs;
     fetch cGetJobIDs into nOutputid, nActionid;
      close cGetJobIDs;

      STB$JOB.setJobParameter('OUTPUTID', nOutputid, cnJobid);
      STB$JOB.setJobParameter('ACTIONID', nActionid, cnJobid);
      STB$RAL.createReport(cnJobid);
    end loop;

  elsif STB$JOB.getJobParameter('TOKEN',cnJobid) = csOffshoot then

    -- get extension from template
    sFileExtension := getExtension(ISR$XML.valueOf(cxXML, '/config/apps/app[1]/template/text()'));--'doc';

    -- create a new config
    cxXML := isr$XML.InitXml('config');
    isr$XML.SetAttribute(cxXML, '/config', 'job-id', To_Char(cnJobId));
    isr$XML.CreateNode(cxXML, '/config', 'apps', '');

    execute immediate 'select '||STB$UTIL.getCurrentCustomPackage||'.createDocumentName(STB$JOB.getJobParameter(''REPID'', '||cnJobId||'), STB$JOB.getJobParameter(''DOCID'', '||cnJobId||')) from dual' into sFileName;
    sFileName := NVL(sFileName, cnJobId || '_1') || '.' || sFileExtension;

    --global settings
    isR$XML.CreateNode(cxXML, '/config/apps', 'fullpathname',csISRTempPath);
    isR$XML.CreateNode(cxXML, '/config/apps', 'target', sFileName);
    --isR$XML.CreateNode(cxXML, '/config/apps', 'extension', sFileExtension);
    isr$XML.CreateNode(cxXML, '/config/apps', 'overwrite','T');

    -- display
    isr$XML.CreateNodeAttr(cxXML,  '/config/apps', 'app', '', 'order', '1');
    isr$XML.SetAttribute(cxXML, '/config/apps/app[@order=1]', 'name', 'ME');
    isr$XML.CreateNode(cxXML, '/config/apps/app[@order=1]', 'wait_for_process', 'false');
    isr$XML.CreateNode(cxXML, '/config/apps/app[@order=1]', 'command', 'DISPLAY');
    isr$XML.CreateNode(cxXML, '/config/apps/app[@order=1]', 'target',sFileName);
    isr$XML.CreateNode(cxXML, '/config/apps/app[@order=1]', 'extension',  'doc');

    -- turn config dom into clob
    update stb$jobtrail
       set textfile = isr$XML.XmlToClob(cxXML)
     where jobid = cnJobId
       and documenttype = 'CONFIG';
    commit;

    sclDom := isr$XML.XMLToClob(cxXML);
    sJavaLogLevel := isr$trace.getJavaUserLoglevelStr;

    -- upload the new config to the loader
    sReturn := ISR$LOADER.loadFileToLoader(sIp, sPort, sContext, sNamespace, cnJobid,  'config.xml', 'LOADER_CLOB_FILE', csConfig, sJavaLogLevel, isr$trace.getReference, to_number(stimeout));
    IF sReturn != 'T' THEN
      ISR$TRACE.error('sReturn', sReturn, sCurrentName);
    END IF;
    if sReturn != 'T' then
      raise exUploadFileError;
   end if;

    oErrorObj := startApp(cnJobid, cxXML);

  elsif nvl(STB$JOB.getJobParameter('ACTIONTYPE',cnJobid),'NOT_DEFINED') = 'CHECKIN_DOCUMENT'
  and STB$JOB.getJobParameter('DELETE_FILE', cnJobId) = 'T' then

    -- create a config record
    cxXML := isr$XML.InitXml('config');
    isr$XML.SetAttribute(cxXML, '/config', 'job-id', to_char(cnJobId));
    isr$XML.CreateNode(cxXML, '/config', 'apps', '');

    --global settings
    isr$XML.CreateNode(cxXML, '/config/apps', 'fullpathname', getPath(STB$JOB.getJobParameter('TO', cnJobId)));
    isr$XML.CreateNode(cxXML, '/config/apps', 'target', getFileName(STB$JOB.getJobParameter('TO', cnJobId)));

    -- delete
    isr$XML.CreateNodeAttr(cxXML, '/config/apps', 'app', null, 'order', '1');
    isr$XML.SetAttribute(cxXML, '/config/apps/app[@order=1]', 'name', 'ME');
    isr$XML.CreateNode(cxXML, '/config/apps/app[@order=1]', 'wait_for_process', 'false');
    isr$XML.CreateNode(cxXML, '/config/apps/app[@order=1]', 'command', 'DELETE');
    isr$XML.CreateNode(cxXML, '/config/apps/app[@order=1]', 'sources', ' ');
    isr$XML.CreateNode(cxXML,'/config/apps/app[@order=1]/sources','source','');
    isr$XML.setAttribute(cxXML, '/config/apps/app[@order=1]/sources/source[position()=last()]', 'leading', 'T'); --leading document
    isr$XML.setAttribute(cxXML, '/config/apps/app[@order=1]/sources/source[position()=last()]', 'name', isr$XML.valueOf(cxXML, '/config/apps/target/text()'));

    -- turn config dom into clob
    update stb$jobtrail
       set textfile = isr$XML.XMLToClob(cxXML)
     where jobid = cnJobId
       and documenttype = 'CONFIG';
    commit;

    sclDom := isr$XML.XMLToClob(cxXML);

    -- upload the new config to the loader
    sReturn := ISR$LOADER.loadFileToLoader(sIp, sPort, sContext, sNamespace, cnJobid,  'config.xml', 'LOADER_CLOB_FILE', csConfig, sJavaLogLevel, isr$trace.getReference, to_number(stimeout));
    IF sReturn != 'T' THEN
      ISR$TRACE.error('sReturn', sReturn, sCurrentName);
    END IF;
    if sReturn != 'T' then
      sImplMsg :=    'sIP: '||sIP ||crlf
                  || 'sPort: '||sPort||crlf
                  || 'sContext: '||sContext||crlf
                  || 'sNamespace: '||sNamespace||crlf
                  || 'cnJobid: '||cnJobid||crlf;
      raise exUploadFileError;
   end if;

    oErrorObj := startApp(cnJobid, cxXML);

 end if;

  STB$JOB.setJobParameter('END_LOADER', 'T', cnJobid);

  isr$trace.stat('end', 'Function '||sCurrentName||' finished',sCurrentName);
  return oErrorObj;

exception
  when exUploadFileError then
    sUserMsg := utd$msglib.getmsg ('exUploadFileErrorText', nCurrentLanguage);
    sImplMsg := sCurrentName ||crlf|| sImplMsg;
    sDevMsg  := substr(dbms_utility.format_error_stack ||crlf|| dbms_utility.format_error_backtrace, 1,4000);
    oErrorObj.handleError ( Stb$typedef.cnSeverityCritical, sUserMsg, sImplMsg, sDevMsg );
    Stb$job.markDBJobAsBroken(cnJobId, sIp, sPort, oErrorObj.sErrorCode, oErrorObj.sErrorMessage, oErrorObj.sSeverityCode);
    return oErrorObj;
  when others then
    sUserMsg := utd$msglib.getmsg ('exUnexpectedError', nCurrentLanguage,  csP1 => sqlerrm(sqlcode));
    sImplMsg := sCurrentName || rtrim(': '||sImplMsg,': ');
    sDevMsg  := substr(dbms_utility.format_error_stack ||crlf|| dbms_utility.format_error_backtrace, 1,4000);
    oErrorObj.handleError ( Stb$typedef.cnSeverityCritical, sUserMsg, sImplMsg, sDevMsg );
    Stb$job.markDBJobAsBroken(cnJobId, sIp, sPort, oErrorObj.sErrorCode, oErrorObj.sErrorMessage, oErrorObj.sSeverityCode);
    return oErrorObj;
end startAppCond;

--***************************************************************************************************************************
function checkAdditionalFinalDocProps( cnJobid in     number,
                                       cxXml   in out xmltype)
  return STB$OERROR$RECORD
is
  sCurrentName   constant varchar2(100) := $$PLSQL_UNIT||'.checkAdditionalFinalDocProps (JobID='||cnJobId||')';
  sUserMsg               varchar2(4000);
  sDevMsg                varchar2(4000);
  sImplMsg               varchar2(4000);
  oErrorObj               STB$OERROR$RECORD :=STB$OERROR$RECORD();

  cursor cGetRepValues is
    select r.repid, r.version repversion, r.status
      from STB$REPORT r
         , (select repid, status
              from stb$report r
            START WITH repid = STB$JOB.getJobParameter ('REPID', cnJobid)
            CONNECT BY PRIOR parentrepid = repid) final_version
     where final_version.status = STB$TYPEDEF.cnStatusFinal
       and final_version.repid = r.repid;

 rGetRepValues  cGetRepValues%rowtype;

begin
  isr$trace.stat('begin', 'Function '||sCurrentName||' started.', sCurrentName);

  open cGetRepValues;
 fetch cGetRepValues into rGetRepValues ;
  close cGetRepValues;

  isr$trace.debug('status rGetRepValues.status', 'rGetRepValues.status: '||rGetRepValues.status, sCurrentName);

  --general document properties of the report version
  if isr$XML.valueOf(cxXML, 'count(/config/apps/app[@order=1]/properties)') = 0 then
    isr$XML.CreateNode(cxXML, '/config/apps/app[@order=1]','properties', '');
 end if;
  if isr$XML.valueOf(cxXML, 'count(/config/apps/app[@order=1]/properties/custom-property[@name="REPID"])') = 0 then
    isr$XML.CreateNode(cxXML, '/config/apps/app[@order=1]/properties', 'custom-property', rGetRepValues.repid);
    isr$XML.SetAttribute(cxXML, '/config/apps/app[@order=1]/properties/custom-property[position()=last()]', 'name', 'REPID');
 end if;
  if isr$XML.valueOf(cxXML, 'count(/config/apps/app[@order=1]/properties/custom-property[@name="REPVERSION"])') = 0 then
    isr$XML.CreateNode(cxXML, '/config/apps/app[@order=1]/properties', 'custom-property', rGetRepValues.repversion);
    isr$XML.SetAttribute(cxXML, '/config/apps/app[@order=1]/properties/custom-property[position()=last()]', 'name', 'REPVERSION');
 end if;

  -- MCD, 09. Apr 2014, REPSTATUS is needed by all wordmodul apps
  for i in 1..ISR$XML.valueOf(cxXML, 'count(/config/apps/app[@name="wordmodul"])') loop
    if isr$XML.valueOf(cxXML, 'count((/config/apps/app[@name="wordmodul"])['||i||']/properties)') = 0 then
      isr$XML.CreateNode(cxXML, '/config/apps/app[@name="wordmodul"]['||i||']','properties', '');
   end if;
   isr$trace.debug('STB$JOB.getJobParameter(TOKEN)', STB$JOB.getJobParameter('TOKEN', cnJobId), sCurrentName);
   if /*blocks are otherwise not updated with status 3*/  STB$JOB.getJobParameter('TOKEN', cnJobId) in ('CAN_CREATE_REPORT_FINAL_VERSION') then
      isr$XML.deleteNode(cxXML, '(/config/apps/app[@name="wordmodul" and doc_definition != "esig"])['||i||']/properties/custom-property[@name="REPSTATUS"]');
      isr$XML.createNode(cxXML, '/config/apps/app[@name="wordmodul" and doc_definition != "esig"]['||i||']/properties', 'custom-property', 2);
      isr$XML.setAttribute(cxXML, '(/config/apps/app[@name="wordmodul" and doc_definition != "esig"])['||i||']/properties/custom-property[position()=last()]', 'name', 'REPSTATUS');
   else
    if isr$XML.valueOf(cxXML, 'count((/config/apps/app[@name="wordmodul"])['||i||']/properties/custom-property[@name="REPSTATUS"])') = 0 then
      isr$XML.CreateNode(cxXML, '/config/apps/app[@name="wordmodul"]['||i||']/properties', 'custom-property', rGetRepValues.status);
      isr$XML.SetAttribute(cxXML, '/config/apps/app[@name="wordmodul"]['||i||']/properties/custom-property[position()=last()]', 'name', 'REPSTATUS');
      isr$trace.debug('value','cxXml',sCurrentName,cxXml);
    end if;
   end if;
  end loop;

  --call the customer package
  execute immediate
     'BEGIN
        :1:= ' || STB$UTIL.getCurrentCustomPackage || '.writeAdditionalFinalDocProps(:2, :3);
      end;'
    using out oErrorObj, in out cxXML, in STB$JOB.getJobParameter('REPID',cnJobid);

  isr$trace.stat('end', 'Function '||sCurrentName||' finished',sCurrentName);
  return oErrorObj;

exception
  when others then
    sUserMsg := utd$msglib.getmsg ('exUnexpectedError', nCurrentLanguage,  csP1 => sqlerrm(sqlcode));
    sImplMsg := sCurrentName || rtrim(': '||sImplMsg,': ');
    sDevMsg  := substr(dbms_utility.format_error_stack ||crlf|| dbms_utility.format_error_backtrace, 1,4000);
    oErrorObj.handleError ( Stb$typedef.cnSeverityCritical, sUserMsg, sImplMsg, sDevMsg );
    Stb$job.markDBJobAsBroken(cnJobId, sIp, sPort, oErrorObj.sErrorCode, oErrorObj.sErrorMessage, oErrorObj.sSeverityCode);
    return oErrorObj;
end checkAdditionalFinalDocProps;

--****************************************************************************************************************************
function copyDocumentIntoJobtrail( cnJobid in     number,
                                   cxXml   in out xmltype)
  return STB$OERROR$RECORD
is
  sCurrentName   constant varchar2(100) := $$PLSQL_UNIT||'.copyDocumentIntoJobtrail (JobID='||cnJobId||')';
  sUserMsg               varchar2(4000);
  sDevMsg                varchar2(4000);
  sImplMsg               varchar2(4000);
  oErrorObj               STB$OERROR$RECORD :=STB$OERROR$RECORD();
begin
  isr$trace.stat('begin', 'Function '||sCurrentName||' started.', sCurrentName);

  -- ISRC-550
  if STB$JOB.getJobParameter('NODETYPE',cnJobId) like '%REPORT_FILE' then
    oErrorObj := copyReportFileIntoJobtrail(cnJobId);
  else
    -- get the one document named in the jobqueue reference
    oErrorObj := Stb$docks.copyDocToJob(STB$JOB.getJobParameter('DOCID',cnJobId),cnJobId);
  end if;

  isr$trace.stat('end', 'Function '||sCurrentName||' finished',sCurrentName);
  return oErrorObj;

exception
  when others then
    sUserMsg := utd$msglib.getmsg ('exUnexpectedError', nCurrentLanguage,  csP1 => sqlerrm(sqlcode));
    sImplMsg := sCurrentName || rtrim(': '||sImplMsg,': ');
    sDevMsg  := substr(dbms_utility.format_error_stack ||crlf|| dbms_utility.format_error_backtrace, 1,4000);
    oErrorObj.handleError ( Stb$typedef.cnSeverityCritical, sUserMsg, sImplMsg, sDevMsg );
    Stb$job.markDBJobAsBroken(cnJobId, sIp, sPort, oErrorObj.sErrorCode, oErrorObj.sErrorMessage, oErrorObj.sSeverityCode);
    return oErrorObj;
end copyDocumentIntoJobtrail;

--****************************************************************************************************************************
function copyXmlIntoJobtrail( cnJobid in     number,
                              cxXml   in out xmltype)
  return STB$OERROR$RECORD
is
  sCurrentName   constant varchar2(100) := $$PLSQL_UNIT||'.copyXmlIntoJobtrail (JobID='||cnJobId||')';
  sUserMsg       varchar2(4000);
  sDevMsg        varchar2(4000);
  sImplMsg       varchar2(4000);
  oErrorObj      STB$OERROR$RECORD :=STB$OERROR$RECORD();

  sDocType       varchar2(100);

begin
  isr$trace.stat('begin', 'Function '||sCurrentName||' started.', sCurrentName);

  sDocType := STB$JOB.getJobParameter('NODETYPE',cnJobId);

  if sDocType = 'RAWDATA' then
    sDocType := csRohDaten;
  end if;

  oErrorObj := Stb$docks.copyXMLToJob(STB$JOB.getJobParameter('DOCID',cnJobId),cnJobId, sDocType);

  case sDocType

    when csRohDaten then
      -- only formats and dynamic parameter needed here. No transformation
      oErrorObj := iSR$RawData.adjustRawDatFile(cnJobid, cxXML);

      --now rename the raw data file to raw report
      update stb$jobtrail
         set documenttype = csRohReport, download = 'T'
       where documenttype = csRohDaten
         and jobid = cnJobid;

    when 'REMOTESNAPSHOT' then
      update stb$jobtrail
         set documenttype = 'TARGET', download = 'T'
       where documenttype = 'REMOTESNAPSHOT'
         and jobid = cnJobid;
  else
      null;
  end case;

  commit;

  isr$trace.stat('end', 'Function '||sCurrentName||' finished',sCurrentName);
  return oErrorObj;

exception
  when others then
    sUserMsg := utd$msglib.getmsg ('exUnexpectedError', nCurrentLanguage,  csP1 => sqlerrm(sqlcode));
    sImplMsg := sCurrentName || rtrim(': '||sImplMsg,': ');
    sDevMsg  := substr(dbms_utility.format_error_stack ||crlf|| dbms_utility.format_error_backtrace, 1,4000);
    oErrorObj.handleError ( Stb$typedef.cnSeverityCritical, sUserMsg, sImplMsg, sDevMsg );
    Stb$job.markDBJobAsBroken(cnJobId, sIp, sPort, oErrorObj.sErrorCode, oErrorObj.sErrorMessage, oErrorObj.sSeverityCode);
    return oErrorObj;
end copyXmlIntoJobtrail;

--***********************************************************************************************************************
function saveConfig( cnJobid in     number,
                     cxXml   in out xmltype)
  return STB$OERROR$RECORD
is
  sCurrentName   constant varchar2(100) := $$PLSQL_UNIT||'.saveConfig('||cnJobId||')';
  sUserMsg               varchar2(4000);
  sDevMsg                varchar2(4000);
  sImplMsg               varchar2(4000);
  oErrorObj               STB$OERROR$RECORD :=STB$OERROR$RECORD();
  nRecordID            number;

  nDocId                STB$DOCTRAIL.DOCID%type;
  sDocOid               STB$DOCTRAIL.DOC_OID%type;
  sIsrDocType           STB$DOCTRAIL.DOCTYPE%type;
  sFileName             STB$DOCTRAIL.FILENAME%type;
  sMimetype             STB$DOCTRAIL.MIMETYPE%type;
  sDocDefinition        STB$DOCTRAIL.DOC_DEFINITION%type;

  cursor cGetDocOid(cnDocId in STB$DOCTRAIL.DOCID%type) is
    select doc_oid
    from   stb$doctrail
    where  docid = cnDocId;

  cursor cGetPropertiesSetByUser(cnDocOid in STB$DOCTRAIL.DOC_OID%TYPE,
                                 cnDocId in STB$DOCTRAIL.DOCID%type) is
    select p.parametername,
           NVL(parameterdisplay, parametervalue) parametervalue
    from   isr$doc$property P,
           stb$doctrail d,
           isr$doc$prop$template t
    where  P.docid = d.docid
    and    (   d.doc_oid = Nvl(cnDocOid,'DOC_OID_NOT_SET')
            or d.docid = Nvl(cnDocId,0))
    and    t.parametername = p.parametername
    and    t.parametertype != 'PARAMETERS'
    and    t.reporttypeid = STB$JOB.getJobParameter('REPORTTYPEID',cnJobid)
    union
    select v.value, utd$msglib.getMsg(v.msgkey, stb$object.GetCurrentReportLanguage, v.plugin)
    from   isr$doc$property P,
           stb$doctrail d,
           isr$doc$prop$template t,
           isr$parameter$values v,
           utd$msg m
    where  P.docid = d.docid
    and    t.parametername = p.parametername
    and    (   d.doc_oid = Nvl(cnDocOid,'DOC_OID_NOT_SET')
            or d.docid = Nvl(cnDocId,0))
    and    t.parametertype = 'PARAMETERS'
    and    t.reporttypeid = STB$JOB.getJobParameter('REPORTTYPEID',cnJobid)
    and    v.entity = p.parametervalue;

  cursor cGetApplicationPathes(cnUserId in STB$USER.USERNO%type) is
    select applicationpathes
    from   stb$user
    where  userno = cnUserId;

  sApplicationPathes STB$USER.APPLICATIONPATHES%type;
begin
  isr$trace.stat('begin','begin',sCurrentName);
  isr$trace.debug('value', 'cxXml: see column LOGCLOB', sCurrentName, cxXml);

  -- over the config xml file get the targets and create an entry in the doctrail
  for i in 1..ISR$XML.valueOf(cxXML,'count(/config/apps/app[@name="wordmodul" and command="CAN_RUN_REPORT"])') loop
    isr$trace.debug_all('for i in 1..ISR$XML.valueOf()','i: '||i, sCurrentName);

    if STB$JOB.getJobParameter('TOKEN',cnJobid) = csOffshoot then
      exit;
    end if;
    isr$trace.debug_all('status docid', 'STB$JOB.getJobParameter(DOCID,cnJobid): '||STB$JOB.getJobParameter('DOCID',cnJobid), sCurrentName);

    sIsrDocType     := isr$XML.valueOf(cxXML, '/config/apps/app[@name="wordmodul" and command="CAN_RUN_REPORT"]['|| i ||']/isrdoctype/text()');
    sFileName       := isr$XML.valueOf(cxXML, '/config/apps/app[@name="wordmodul" and command="CAN_RUN_REPORT"]['|| i ||']/target/text()');
    sMimetype       := isr$XML.valueOf(cxXML, '/config/apps/app[@name="wordmodul" and command="CAN_RUN_REPORT"]['|| i ||']/mimetype/text()');
    sDocDefinition  := isr$XML.valueOf(cxXML, '/config/apps/app[@name="wordmodul" and command="CAN_RUN_REPORT"]['|| i ||']/doc_definition/text()');   -- JB 07 Nov 2007

    isr$trace.debug_all('status sDocDefinition', 'sDocDefinition: '||sDocDefinition, sCurrentName);
    isr$trace.debug_all('status sMimetype', 'sMimetype: '||sMimetype, sCurrentName);

    oErrorObj := STB$DOCKS.insertIntoDoctrail( STB$JOB.getJobParameter('REPID',cnJobid),
                                            sIsrDocType,
                                            sFileName,
                                            sMimetype,
                                            STB$JOB.getJobParameter('USER',cnJobId),
                                            nDocid,
                                            STB$JOB.getJobParameter('REPORTTYPEID',cnJobid) ,
                                            sDocDefinition);  --JB 07 Nov 2007

    --mark the document to be deleted when job fails
    update stb$doctrail
       set job_flag = 'D'
     where docid = nDocId;
    commit;

    if isr$XML.ValueOf(cxXML, 'count(/config/apps/app[@name="wordmodul" and command="CAN_RUN_REPORT"]['|| i ||']/properties)') = 0 then
      isr$XML.CreateNode(cxXML, '/config/apps/app[@name="wordmodul" and command="CAN_RUN_REPORT"]['|| i ||']','properties', '');
   end if;

    -- get oid
    open cGetDocOid(nDocId);
    fetch cGetDocOid into sDocOid;
    close cGetDocOid;

    isr$XML.CreateNode(cxXML, '/config/apps/app[@name="wordmodul" and command="CAN_RUN_REPORT"]['|| i ||']/properties', 'custom-property', sDocOid );
    isr$XML.SetAttribute(cxXML, '/config/apps/app[@name="wordmodul" and command="CAN_RUN_REPORT"]['|| i ||']/properties/custom-property[position()=last()]', 'name', csDocPropOid);

    if  isr$XML.ValueOf(cxXML, '/config/apps/app[@name="wordmodul" and command="CAN_RUN_REPORT"]['||i ||']/doc_definition/text()') = 'esig' then
      isr$trace.debug('esig doc - master oid','sDoc0id: '||sDocOid, sCurrentName, cxXML);
      -- get the master oid of all other documents in the config xml. file
      isr$XML.CreateNode(cxXML, '/config/apps/app[@name="wordmodul" and command="CAN_RUN_REPORT"]['|| i ||']/properties', 'custom-property',
                  isr$XML.ValueOf(cxXML, '/config/apps/app[@name="wordmodul" and command="CAN_RUN_REPORT" and doc_definition != "esig"]/properties/custom-property[@name="DOC_OID"]/text()') );
      isr$XML.SetAttribute(cxXML, '/config/apps/app[@name="wordmodul" and command="CAN_RUN_REPORT"]['|| i ||']/properties/custom-property[position()=last()]', 'name', 'MASTER_OID');
      isr$trace.debug('esig doc - after master oid','logclob', sCurrentName, cxXML);
    end if;

  end loop;

  -- check out write the new doc oid's
  for i in 1..ISR$XML.valueOf(cxXML,'count(/config/apps/app[@name="wordmodul" and command="CAN_CHECKOUT"])') loop

    nDocId := STB$JOB.getJobParameter('REFERENCE',cnJobid);

    isr$XML.CreateNode(cxXML, '/config/apps/app[@name="wordmodul" and command="CAN_CHECKOUT"]['|| i ||']', 'docid', nDocId);

    -- get oid
    open cGetDocOid(STB$JOB.getJobParameter('REFERENCE',cnJobid));
   fetch cGetDocOid into sDocOid;
    close cGetDocOid;

    if isr$XML.ValueOf(cxXML, 'count(/config/apps/app[@name="wordmodul" and command="CAN_CHECKOUT"]['|| i ||']/properties)') = 0 then
      isr$XML.CreateNode(cxXML, '/config/apps/app[@name="wordmodul" and command="CAN_CHECKOUT"]['|| i ||']','properties', '');
   end if;

    isr$XML.CreateNode(cxXML, '/config/apps/app[@name="wordmodul" and command="CAN_CHECKOUT"]['|| i ||']/properties', 'custom-property', sDocOid );
    isr$XML.SetAttribute(cxXML, '/config/apps/app[@name="wordmodul" and command="CAN_CHECKOUT"]['|| i ||']/properties/custom-property[position()=last()]', 'name', csDocPropOid);

  end loop;

  -- write user's properties
  for i in 1..ISR$XML.valueOf(cxXML,'count(/config/apps/app[@name="wordmodul"])') loop

    -- if not final then write all other document properties
    if STB$JOB.getJobParameter('FINALFLAG', cnJobid) is  null  then

      -- check if the doc id isn't null  added JB 17 Nov 2008
      if STB$JOB.getJobParameter('DOCID',cnJobid) is not null then
        isr$trace.debug_all('status docid', 'STB$JOB.getJobParameter(DOCID,cnJobid): '||STB$JOB.getJobParameter('DOCID',cnJobid), sCurrentName);

        for rGetPropertiesSetByUser in cGetPropertiesSetByUser(ISR$XML.valueOf(cxXML,'/config/apps/app[@name="wordmodul"]['||i||']/properties/custom-property[@name="'||csDocPropOid||'"]/text()'),
                                                             STB$JOB.getJobParameter('DOCID',cnJobid)) loop
          if isr$XML.ValueOf(cxXML, 'count(/config/apps/app[@name="wordmodul"]['||i||']/properties)') = 0 then
            isr$XML.CreateNode(cxXML, '/config/apps/app[@name="wordmodul"]['||i||']','properties', '');
         end if;
          isr$XML.CreateNode(cxXML, '/config/apps/app[@name="wordmodul"]['||i||']/properties', 'custom-property', rGetPropertiesSetByUser.parametervalue);
          isr$XML.SetAttribute(cxXML, '/config/apps/app[@name="wordmodul"]['||i||']/properties/custom-property[position()=last()]', 'name', rGetPropertiesSetByUser.parametername);
        end loop;

      elsif nDocid is not null then
         isr$trace.debug_all('status nDocid', 'nDocid: '||nDocid, sCurrentName);
         for rGetPropertiesSetByUser in cGetPropertiesSetByUser(ISR$XML.valueOf(cxXML,'/config/apps/app[@name="wordmodul"]['||i||']/properties/custom-property[@name="'||csDocPropOid||'"]/text()'),
                                                            nDocid ) loop
          if isr$XML.ValueOf(cxXML, 'count(/config/apps/app[@name="wordmodul"]['||i||']/properties)') = 0 then
            isr$XML.CreateNode(cxXML, '/config/apps/app[@name="wordmodul"]['||i||']','properties', '');
         end if;
          if isr$XML.valueOf(cxXML, 'count((/config/apps/app[@name="wordmodul"])['||i||']/properties/custom-property[@name="'||rGetPropertiesSetByUser.parametername||'"])') = 0 then
            isr$XML.CreateNode(cxXML, '/config/apps/app[@name="wordmodul"]['||i||']/properties', 'custom-property', rGetPropertiesSetByUser.parametervalue);
            isr$XML.SetAttribute(cxXML, '/config/apps/app[@name="wordmodul"]['||i||']/properties/custom-property[position()=last()]', 'name', rGetPropertiesSetByUser.parametername);
         end if;
        end loop;

     end if;

      -- call the customer package
      execute immediate
        'BEGIN
          :1 := ' || STB$UTIL.getCurrentCustomPackage || '.writeCustomDocProperties(:2, :3);
         end;'
      using out oErrorObj, in out cxXML, in cnJobid;

   end if;

  end loop;


  -- over the config xml file get the targets and create an entry in the doctrail
  for i in 1..ISR$XML.valueOf(cxXML,'count(/config/apps/app[@name="wordmodul" and command="CAN_MODIFY_WITH_COMMON_DOC"])') loop
    isr$trace.debug_all('for i in 1..ISR$XML.valueOf()','i: '||i, sCurrentName);

    if STB$JOB.getJobParameter('TOKEN',cnJobid) = csOffshoot then
      exit;
   end if;
    isr$trace.debug_all('status docid', 'STB$JOB.getJobParameter(DOCID,cnJobid): '||STB$JOB.getJobParameter('DOCID',cnJobid), sCurrentName);

    sIsrDocType     := isr$XML.valueOf(cxXML, '/config/apps/app[@name="wordmodul" and command="CAN_MODIFY_WITH_COMMON_DOC"]['|| i ||']/isrdoctype/text()');
    sFileName       := isr$XML.valueOf(cxXML, '/config/apps/app[@name="wordmodul" and command="CAN_MODIFY_WITH_COMMON_DOC"]['|| i ||']/target/text()');
    sMimetype       := isr$XML.valueOf(cxXML, '/config/apps/app[@name="wordmodul" and command="CAN_MODIFY_WITH_COMMON_DOC"]['|| i ||']/mimetype/text()');
    sDocDefinition  := isr$XML.valueOf(cxXML, '/config/apps/app[@name="wordmodul" and command="CAN_MODIFY_WITH_COMMON_DOC"]['|| i ||']/doc_definition/text()');

    isr$trace.debug_all('status sDocDefinition', 'sDocDefinition: '||sDocDefinition, sCurrentName);
    isr$trace.debug_all('status sMimetype', 'sMimetype: '||sMimetype, sCurrentName);

    oErrorObj := STB$DOCKS.insertIntoDoctrail( STB$JOB.getJobParameter('REPID',cnJobid),
                                            sIsrDocType,
                                            sFileName,
                                            sMimetype,
                                            STB$JOB.getJobParameter('USER',cnJobId),
                                            nDocid,
                                            STB$JOB.getJobParameter('REPORTTYPEID',cnJobid) ,
                                            sDocDefinition);

    --mark the document to be deleted when job fails
    update stb$doctrail
       set job_flag = 'D'
     where docid = nDocId;
    commit;

    if isr$XML.ValueOf(cxXML, 'count(/config/apps/app[@name="wordmodul" and command="CAN_MODIFY_WITH_COMMON_DOC"]['|| i ||']/properties)') = 0 then
      isr$XML.CreateNode(cxXML, '/config/apps/app[@name="wordmodul" and command="CAN_MODIFY_WITH_COMMON_DOC"]['|| i ||']','properties', '');
   end if;

    -- get oid
    open  cGetDocOid(nDocId);
    fetch cGetDocOid into sDocOid;
    close cGetDocOid;

    isr$XML.CreateNode(cxXML, '/config/apps/app[@name="wordmodul" and command="CAN_MODIFY_WITH_COMMON_DOC"]['|| i ||']/properties', 'custom-property', sDocOid );
    isr$XML.SetAttribute(cxXML, '/config/apps/app[@name="wordmodul" and command="CAN_MODIFY_WITH_COMMON_DOC"]['|| i ||']/properties/custom-property[position()=last()]', 'name', csDocPropOid);

  end loop;



  open  cGetApplicationPathes(To_Number(STB$JOB.getJobParameter('USER',cnJobid)));
  fetch cGetApplicationPathes into sApplicationPathes;
  close cGetApplicationPathes;

  if Trim(sApplicationPathes) is not null then
    isr$XML.createNode(cxXML, '/config/apps', 'applicationpathes', sApplicationPathes);
 end if;

  if Trim(ISR$XML.valueOf(cxXML, '/config/apps/fullpathname/text()')) is null then
    isr$XML.deleteNode(cxXML, '/config/apps/fullpathname');
 end if;
  if Trim(ISR$XML.valueOf(cxXML, '/config/apps/target/text()')) is null then
    isr$XML.deleteNode(cxXML, '/config/apps/target');
 end if;
  if Trim(ISR$XML.valueOf(cxXML, '/config/apps/extension/text()')) is null then
    isr$XML.deleteNode(cxXML, '/config/apps/extension');
 end if;
  if Trim(ISR$XML.valueOf(cxXML, '/config/apps/overwrite/text()')) is null then
    isr$XML.deleteNode(cxXML, '/config/apps/overwrite');
 end if;

  isr$trace.debug('cxXml', 's. logclob', sCurrentName, cxXml);
  -- create entry in STB$JOBTRAIL
  select STB$JOBTRAIL$SEQ.NEXTVAL into nRecordID from dual;


  insert into STB$JOBTRAIL (
      ENTRYID,
      JOBID,
      REFERENCE,
      TEXTFILE,
      DOCUMENTTYPE,
      MIMETYPE,
      TIMESTAMP,
      FILENAME,
      BLOBFLAG,
      DOWNLOAD
     ) values (
      nRecordID,
      cnJobid,
      cnJobid,
      cxXML.getClobVal(),
      csConfig,
      'text/xml',
       sysdate ,
      'config.xml',
      'C',
      'T'
     );

  commit;

  isr$trace.stat('end', 'Function '||sCurrentName||' finished',sCurrentName);
  return oErrorObj;

exception
  when others then
    sUserMsg := utd$msglib.getmsg ('exUnexpectedError', nCurrentLanguage,  csP1 => sqlerrm(sqlcode));
    sImplMsg := sCurrentName || rtrim(': '||sImplMsg,': ');
    sDevMsg  := substr(dbms_utility.format_error_stack ||crlf|| dbms_utility.format_error_backtrace, 1,4000);
    oErrorObj.handleError ( Stb$typedef.cnSeverityCritical, sUserMsg, sImplMsg, sDevMsg );
    Stb$job.markDBJobAsBroken(cnJobId, sIp, sPort, oErrorObj.sErrorCode, oErrorObj.sErrorMessage, oErrorObj.sSeverityCode);
    return oErrorObj;
end saveConfig;

--***********************************************************************************************************************
function getClientIp return varchar2 is
begin
    return sIP;
end getClientIp;

--***********************************************************************************************************************
FUNCTION getLoaderPort return varchar2 is
begin
    return sPort;
end getLoaderPort;

--***********************************************************************************************************************
function isDocServerAvailable( sDocServerIp   in varchar2,
                               sDocServerPort in varchar2)
  return varchar2
is
  sCurrentName   constant varchar2(100) := $$PLSQL_UNIT||'.isDocServerAvailable';
  sUserMsg       varchar2(4000);
  sDevMsg        varchar2(4000);
  sImplMsg       varchar2(4000);
  oErrorObj      STB$OERROR$RECORD :=STB$OERROR$RECORD();
  sReturn        VARCHAR2(4000);
begin
  isr$trace.stat('begin end', 'Function '||sCurrentName||' passed.', sCurrentName);
  isr$trace.debug('status DocServerIp/DocServerPort', 'DocServerIp/DocServerPort: '||sDocServerIp || '/' || sDocServerPort, sCurrentName);

  sContext := STB$UTIL.getSystemParameter('LOADER_CONTEXT');
  sNameSpace := STB$UTIL.getSystemParameter('LOADER_NAMESPACE');

  sReturn := ISR$LOADER.ping(sDocServerIp, sDocServerPort, sContext, sNamespace, STB$UTIL.getSystemParameter('TIMEOUT_LOADER_PING'));
  IF sReturn is not null THEN
    isr$trace.debug('params',  'sDocServerIp=' || sDocServerIp ||'; sDocServerPort=' || sDocServerPort ||'; sContext=' || sContext || '; sNamespace=' || sNamespace, sCurrentName);
    isr$trace.error('sReturn', sReturn, sCurrentName);
  END IF;

  isr$trace.stat('end', 'end', sCurrentName);
  return sReturn;

exception
  when others then
    sUserMsg := utd$msglib.getmsg ('exUnexpectedError', nCurrentLanguage,  csP1 => sqlerrm(sqlcode));
    sImplMsg := sCurrentName || rtrim(': '||sImplMsg,': ');
    sDevMsg  := dbms_utility.format_error_stack ||crlf|| dbms_utility.format_error_backtrace;
    oErrorObj.handleError ( Stb$typedef.cnSeverityCritical, sUserMsg, sImplMsg, sDevMsg );
    return sUserMsg;
end isDocServerAvailable;

--**************************************************************************************************************************
function controlJobs( cnJobid in     number,
                      cxXml   in out xmltype)
  return STB$OERROR$RECORD
is
  sCurrentName   constant varchar2(100) := $$PLSQL_UNIT||'.controlJobs (JobID='||cnJobId||')';
  sUserMsg               varchar2(4000);
  sDevMsg                varchar2(4000);
  sImplMsg               varchar2(4000);
  oErrorObj               STB$OERROR$RECORD :=STB$OERROR$RECORD();
begin
  isr$trace.stat('begin', 'Function '||sCurrentName||' started.', sCurrentName);

  STB$JOB.setJobParameter('START_LOADER', 'F', cnJobid);
  STB$JOB.setJobParameter('END_LOADER', 'F', cnJobid);

  for rGetParameterReference in ( select distinct TRIM(SUBSTR(parametername, INSTR(parametername,'@',-1) + 1)) reference
                                  from stb$jobparameter
                                  where jobid = cnJobid
                                  and parametername like '%@%') loop
    isr$trace.debug('status rGetParameterReference.reference', 'rGetParameterReference.reference: '||rGetParameterReference.reference, sCurrentName);

    for rGetParameters in ( select distinct TRIM(SUBSTR(parametername, 0, INSTR(parametername,'@',-1) - 1)) parameter
                            from stb$jobparameter
                            where jobid = cnJobid
                            and parametername like '%@'||rGetParameterReference.reference) loop
      STB$JOB.setJobParameter(rGetParameters.parameter, STB$JOB.getJobParameter (rGetParameters.parameter||'@'||rGetParameterReference.reference, cnJobid), cnJobid);
      isr$trace.debug('status rGetParameterReference.parameter', 'STB$JOB.getJobParameter(rGetParameters.parameter,cnJobid): '||STB$JOB.getJobParameter(rGetParameters.parameter, cnJobid), sCurrentName);
    end loop;

    STB$RAL.createReport(cnJobid);

    -- empty the job flag, this action was successful
    update stb$doctrail set job_flag = null where docid = STB$JOB.getJobParameter ('DOCID', cnJobid);
  end loop;

  STB$JOB.setJobParameter('END_LOADER', 'T', cnJobid);

  isr$trace.stat('end', 'Function '||sCurrentName||' finished',sCurrentName);
  return oErrorObj;

exception
  when others then
    sUserMsg := utd$msglib.getmsg ('exUnexpectedError', nCurrentLanguage,  csP1 => sqlerrm(sqlcode));
    sImplMsg := sCurrentName || rtrim(': '||sImplMsg,': ');
    sDevMsg  := substr(dbms_utility.format_error_stack ||crlf|| dbms_utility.format_error_backtrace, 1,4000);
    oErrorObj.handleError ( Stb$typedef.cnSeverityCritical, sUserMsg, sImplMsg, sDevMsg );
    Stb$job.markDBJobAsBroken(cnJobId, sIp, sPort, oErrorObj.sErrorCode, oErrorObj.sErrorMessage, oErrorObj.sSeverityCode);
    return oErrorObj;
end controlJobs;

--******************************************************************************************************************************
function createCriteriaReport( cnJobid in     number,
                               cxXml   in out xmltype)
  return STB$OERROR$RECORD
 is
  sCurrentName   constant varchar2(100) := $$PLSQL_UNIT||'.createCriteriaReport (JobID='||cnJobId||')';
  sUserMsg               varchar2(4000);
  sDevMsg                varchar2(4000);
  sImplMsg               varchar2(4000);
  oErrorObj               STB$OERROR$RECORD :=STB$OERROR$RECORD();
  xNewXml             xmltype;
  xXml1               xmltype;
begin
  isr$trace.stat('begin', 'Function '||sCurrentName||' started.', sCurrentName);
  isr$trace.debug('status Jobparameter WHERE', 'STB$JOB.getJobParameter(WHERE,cnJobid): '||STB$JOB.getJobParameter('WHERE',cnJobid), sCurrentName);

  oErrorObj := Stb$object.initRepType(STB$JOB.getJobParameter('REPORTTYPEID',cnJobId), STB$JOB.getJobParameter('REPID',cnJobId));

  select (xmlelement (
             "CRITERIAREPORT"
           , (select (xmlagg(xmlelement (
                                "REPORT"
                              , xmlelement ("TITLE", TITLE)
                              , xmlelement ("VERSION", VERSION)
                              , xmlelement (
                                   "STATUS"
                                 , case
                                      when STATUS = stb$typedef.cnstatusdraft then 'Draft'
                                      when STATUS = stb$typedef.cnstatusinspection then 'Inspection'
                                      when STATUS = stb$typedef.cnstatusfinal then 'Final'
                                      else 'No Status'
                                   end
                                )
                              , xmlelement ("MODIFIEDON", TO_CHAR (MODIFIEDON, STB$DOCKS.getDateFormat ('CAN_CREATE_CRITERIA')|| ' HH24:MI:SS'))
                              , xmlelement ("MODIFIEDBY", MODIFIEDBY)
                              , xmlelement ("SERVER", (select listagg (alias || ' (' || serverid || ')', ', ') WITHIN GROUP (ORDER BY ordernumber) from isr$server, (select * from isr$crit where repid = STB$JOB.getJobParameter ('REPID', cnJobid) and entity = 'SERVER') where to_char(serverid) = key))
                              , xmlelement ("REPID", REPID)
                              , xmlelement ("REPORTTYPEID", REPORTTYPEID)
                              , xmlelement ("REMARKS", REMARKS)
                              , xmlelement ("WORDREPORT", isr$excel$reporttype.isWordReportExists(REPID))
                              , xmlelement ("WORDDOCUMENT", isr$excel$reporttype.isWordDocumentExists(REPID))
                             )))
                from stb$report
               where repid = STB$JOB.getJobParameter ('REPID', cnJobid))
           , (select (xmlagg(xmlelement (
                                "CRITERIA"
                              , xmlattributes (t1.entity as "entity")
                              , (select xmlagg(xmlelement (
                                                  "ENTITYTABLE"
                                                , case
                                                    when masktype in (3, 4) then
                                                      xmlelement ("masterkey", masterkey)
                                                    else
                                                      xmlelement ("empty", ' ')
                                                  end
                                                , case
                                                    when NVL(STB$JOB.getJobParameter ('FILTER_DISPLAY_ALL', cnJobid),'F') = 'T' then
                                                      xmlelement ("key", NVL (key, ' '))
                                                    else
                                                      xmlelement ("empty", ' ')
                                                  end
                                                , xmlelement ("item", NVL (display, ' '))
                                                , xmlelement ("info", NVL (info, ' '))
                                                , case
                                                    when NVL(STB$JOB.getJobParameter ('FILTER_DISPLAY_ALL', cnJobid),'F') = 'T' then
                                                      xmlelement ("order", NVL (TO_CHAR(ordernumber), ' '))
                                                    else
                                                      xmlelement ("empty", ' ')
                                                  end
                                               ))
                                   from (select crit.ordernumber
                                              , crit.key
                                              , crit.display
                                              , crit.info
                                              , crit.masterkey
                                              , crit.entity
                                           from isr$crit crit
                                          where crit.repid = STB$JOB.getJobParameter ('REPID', cnJobid)
                                         union
                                         select 1000+rtp.orderno
                                              , rtp.parametername
                                              , Utd$msglib.GetMsg (rtp.description, Stb$security.GetCurrentLanguage)
                                              , NVL(case when rtp.haslov = 'T' then isr$REPORTTYPE$PACKAGE.getSelectedLovDisplay(rp.parametername, rp.parametervalue)
                                                         when rtp.parametertype = 'BOOLEAN' and rp.parametervalue = 'T' then 'True'
                                                         when rtp.parametertype = 'BOOLEAN' and rp.parametervalue = 'F' then 'False' end, rp.parametervalue)
                                              , null display3
                                              , 'STB$OUTPUT$OPTION' entity
                                           from stb$reportparameter rp
                                              , stb$reptypeparameter rtp
                                              , stb$report r
                                          where rp.repid = STB$JOB.getJobParameter ('REPID', cnJobid)
                                            and rtp.parametername = rp.parametername
                                            and rtp.repparameter in ('T')
                                            and rtp.reporttypeid = r.reporttypeid
                                            and rp.repid = r.repid
                                            and rtp.visible = case when NVL(STB$JOB.getJobParameter ('FILTER_DISPLAY_ALL', cnJobid),'F') = 'T' then rtp.visible else 'T' end
                                         union
                                         select 2000+rt.orderno
                                              , rt.parametername
                                              , Utd$msglib.GetMsg (rt.description, Stb$security.GetCurrentLanguage)
                                              , NVL(case when rt.haslov = 'T' then isr$REPORTTYPE$PACKAGE.getSelectedLovDisplay(rpa.parametername, rpa.parametervalue)
                                                         when rt.parametertype = 'BOOLEAN' and rpa.parametervalue = 'T' then 'True'
                                                         when rt.parametertype = 'BOOLEAN' and rpa.parametervalue = 'F' then 'False' end, rpa.parametervalue)
                                              , null display3
                                              , 'STB$OUTPUT$OPTION$WORD' entity
                                            FROM stb$reportparameter rpa
                                               , stb$reptypeparameter rt
                                               , isr$parameter$values pv
                                           WHERE rpa.repid(+) = STB$JOB.getJobParameter ('REPID', cnJobid)
                                             AND rpa.parametername(+) = case when rt.repparameter = 'W' then 'W_' || rt.parameterName else rt.parameterName end
                                             AND rt.reporttypeid = STB$JOB.getJobParameter ('REPORTTYPEID', cnJobid)
                                             AND rt.repparameter in ('W')
                                             AND rpa.parameterName = pv.entity(+)
                                             AND rpa.parametervalue = pv.VALUE(+)                                          
                                         order by 1) t
                                  where t1.entity = t.entity)
                             )))
                from (select NVL (g.entityname, w.entity) entity
                           , REPLACE (NVL (g.criteriaheadline1, w.criteriaheadline1), '$', '_') headline1
                           , REPLACE (NVL (g.criteriaheadline2, w.criteriaheadline2), '$', '_') headline2
                           , REPLACE (NVL (g.criteriaheadline3, w.criteriaheadline3), '$', '_') headline3
                           , masktype
                           , w.reporttypeid
                           , w.maskno
                           , g.columnpos
                        from isr$report$wizard w, isr$meta$grid$entity g
                       where w.reporttypeid = STB$JOB.getJobParameter ('REPORTTYPEID', cnJobid)
                         and w.visible = case when NVL (STB$JOB.getJobParameter ('FILTER_DISPLAY_ALL', cnJobid), 'F') = 'T' then w.visible else 'T' end
                         and g.wizardentity(+) = w.entity
                         and g.reporttypeid(+) = w.reporttypeid
                         and g.columnpos(+) > 1
                      union
                      select w.entity || '$WORD'
                           , REPLACE (w.criteriaheadline1, '$', '_') headline1
                           , REPLACE (w.criteriaheadline2, '$', '_') headline2
                           , REPLACE (w.criteriaheadline3, '$', '_') headline3
                           , masktype
                           , w.reporttypeid
                           , 100
                           , 0
                        from isr$report$wizard w
                       where w.reporttypeid = STB$JOB.getJobParameter ('REPORTTYPEID', cnJobid)
                         and w.visible = case when NVL (STB$JOB.getJobParameter ('FILTER_DISPLAY_ALL', cnJobid), 'F') = 'T' then w.visible else 'T' end
                         and w.masktype = 5
                      order by 7, 8 ) t1)
          ))
    into xNewXml
    from DUAL;


  select xmlelement (
           "TRANSLATION"
         , (select xmlagg(xmlelement ("TRANSLATIONENTRY"
                                     , xmlattributes ( parametername as "KEY")
                                     , parametervalue))
              from (select DISTINCT
                           EXTRACTVALUE (COLUMN_VALUE, '/name/@attr') parametername
                         , UTD$MSGLIB.getMsg (EXTRACTVALUE (COLUMN_VALUE, '/name/@attr'), STB$SECURITY.getCurrentLanguage) parametervalue
                      from table(xmlsequence(extract (
                                                (select XMLQUERY ('for $i in //* return <name attr="{$i/name()}"/>'
                                                           PASSING by value xNewXml returning CONTENT)
                                                   from DUAL)
                                              , '/*'
                                             ))))
            where parametername != parametervalue)
        )
   into xXml1
   from DUAL;
  isr$XML.InsertChildXMLFragment(xNewXml, '/*[1]', isr$XML.XMLToClob(xXml1));

  oErrorObj := STB$DOCKS.saveXmlReport(xNewXml, cnJobid, 'criteria.xml');
  if oErrorObj.sSeverityCode != stb$typedef.cnNoError then
    Stb$job.markDBJobAsBroken(cnJobId, sIp, sPort, oErrorObj.sErrorCode, oErrorObj.sErrorMessage, oErrorObj.sSeverityCode);
  end if;
  isr$trace.stat('end', 'Function '||sCurrentName||' finished',sCurrentName);
  return oErrorObj;

exception
  when others then
    sUserMsg := utd$msglib.getmsg ('exUnexpectedError', nCurrentLanguage,  csP1 => sqlerrm(sqlcode));
    sImplMsg := sCurrentName || rtrim(': '||sImplMsg,': ');
    sDevMsg  := substr(dbms_utility.format_error_stack ||crlf|| dbms_utility.format_error_backtrace, 1,4000);
    oErrorObj.handleError ( Stb$typedef.cnSeverityCritical, sUserMsg, sImplMsg, sDevMsg );
    Stb$job.markDBJobAsBroken(cnJobId, sIp, sPort, oErrorObj.sErrorCode, oErrorObj.sErrorMessage, oErrorObj.sSeverityCode);
    return oErrorObj;
end createCriteriaReport;

--***********************************************************************************************************************************
function createConfigurationFile( cnJobid in number,
                                  csToken in varchar2)
  return STB$OERROR$RECORD
  is
  sCurrentName   constant varchar2(100) := $$PLSQL_UNIT||'.createConfigurationFile('||csToken||')';
  sUserMsg       varchar2(4000);
  sDevMsg        varchar2(4000);
  sImplMsg       varchar2(4000);
  oErrorObj      STB$OERROR$RECORD :=STB$OERROR$RECORD();
  xXml           xmltype;
  xDataXml       xmltype;
  clXML          clob;
begin
  isr$trace.stat('begin', 'cnJobId: '||cnJobId, sCurrentName);

  isr$trace.debug ('select statement', 'select xml from '||SUBSTR(csToken, 0, 30), sCurrentName);
  execute immediate 'select xml from '||SUBSTR(csToken, 0, 30)
  into xXml;
  isr$trace.debug('code position', 'after dyn sql', sCurrentName);

/* (NB) 20160517 - old handling of XML removed
  if NVL(DBMS_LOB.INSTR (clXml, '<?xml'),0) = 0 then
    clXml := '<?xml version="handled by isr$XML" encoding="handled by isr$XML"?>'||clXml;
  end if;
*/
  isr$trace.debug(substr(csToken, 0, 30)||'_output.xml', 'file in logclob', sCurrentName, xXml);
  oErrorObj := STB$DOCKS.saveXmlReport(xXml, cnJobid, lower(csToken)||'.xml');
  isr$trace.stat('end', 'end',sCurrentName);
  return oErrorObj;
exception
  when others then
    sUserMsg := utd$msglib.getmsg ('exUnexpectedError', nCurrentLanguage,  csP1 => sqlerrm(sqlcode));
    sImplMsg := sCurrentName || rtrim(': '||sImplMsg,': ');
    sDevMsg  := substr(dbms_utility.format_error_stack ||crlf|| dbms_utility.format_error_backtrace, 1,4000);
    oErrorObj.handleError ( Stb$typedef.cnSeverityCritical, sUserMsg, sImplMsg, sDevMsg );
    return oErrorObj;
end createConfigurationFile;

--***********************************************************************************************************************************
function createConfigurationFile( cnJobid in     number,
                                  cxXml   in out xmltype)
  return STB$OERROR$RECORD
is
  sCurrentName   constant varchar2(100) := $$PLSQL_UNIT||'.createConfigurationFile (JobID='||cnJobId||')';
  sUserMsg       varchar2(4000);
  sDevMsg        varchar2(4000);
  sImplMsg       varchar2(4000);
  oErrorObj      STB$OERROR$RECORD :=STB$OERROR$RECORD();
begin
  isr$trace.info('begin', 'Function '||sCurrentName||' started.', sCurrentName);

  oErrorObj := createConfigurationFile(cnJobId, STB$JOB.getJobParameter ('TOKEN', cnJobId));

  isr$trace.info('end', 'Function '||sCurrentName||' finished',sCurrentName);
  return oErrorObj;
exception
  when others then
    sUserMsg := utd$msglib.getmsg ('exUnexpectedError', nCurrentLanguage,  csP1 => sqlerrm(sqlcode));
    sImplMsg := sCurrentName || rtrim(': '||sImplMsg,': ');
    sDevMsg  := substr(dbms_utility.format_error_stack ||crlf|| dbms_utility.format_error_backtrace, 1,4000);
    oErrorObj.handleError ( Stb$typedef.cnSeverityCritical, sUserMsg, sImplMsg, sDevMsg );
    Stb$job.markDBJobAsBroken(cnJobId, sIp, sPort, oErrorObj.sErrorCode, oErrorObj.sErrorMessage, oErrorObj.sSeverityCode);
    return oErrorObj;
end createConfigurationFile;


--*******************************************************************************************************************
function createAuditFile( cnJobid in     number,
                          cxXml   in out xmltype)
  return STB$OERROR$RECORD
is
  sCurrentName   constant varchar2(100) := $$PLSQL_UNIT||'.createAuditFile (JobID='||cnJobId||')';
  sUserMsg       varchar2(4000);
  sDevMsg        varchar2(4000);
  sImplMsg       varchar2(4000);
  oErrorObj      STB$OERROR$RECORD :=STB$OERROR$RECORD();
  sAuditType     varchar2(500);
  xXml           xmltype;
  xXml1          xmltype;
  sRepid         varchar2(500);
  cResult        clob;
  sReturn        varchar2(4000);
  xAuditPiece    xmltype;
  sRepAuditVers  varchar2(50);
  sRepAuditRepid varchar2(50);

  function GetTitleForREPID( sRepID in varchar2 )
  return varchar2 is

     cursor cGetTitle is
       select Title
       from STB$REPORT
       where REPID=sRepID;

     rGetTitle cGetTitle%RowType;
     sTitle varchar2(4000);

  begin
    open cGetTitle;
    fetch cGetTitle into rGetTitle;

    if cGetTitle%found then
      sTitle := rGetTitle.Title;
    end if;

    Close cGetTitle;
    Return sTitle;
  end;

begin
  isr$trace.stat('begin', 'Function '||sCurrentName||' started.', sCurrentName);

  sAuditType := NVL(REPLACE(STB$JOB.getJobParameter('TOKEN', cnJobid), 'CAN_DISPLAY_'),STB$JOB.getJobParameter('NODETYPE', cnJobid));
/*
   Issues ISRC-786 and BIBC-295, SRAUDIT-Report raise an exception when on version node (available from the ribbon)
   introduce complete case-when structure for all AUDIT-Reports
*/
  xXml := stb$audit.getAudit( sAuditType,
        case when sAuditType = 'TEMPLATEAUDIT' then STB$JOB.getJobParameter('REFERENCE_DISPLAY', cnJobid)
             when sAuditType = 'SRAUDIT' then
                  case when STB$JOB.getJobParameter('NODETYPE', cnJobid) = 'REPORT' then GetTitleForRepID(STB$JOB.getJobParameter('REFERENCE', cnJobid))
                       else STB$JOB.getJobParameter('REFERENCE', cnJobid) -- assuming that NODETYPE will be 'TITLE'
                  end
             when sAuditType = 'REPORTTYPEAUDIT' then STB$JOB.getJobParameter('REFERENCE', cnJobid)
             else STB$JOB.getJobParameter('REFERENCE', cnJobid) END); -- assuming that AUITTYPE will only be "REPORT" here
  isr$trace.debug('xXml', 'after getAudit', sCurrentName, xXml);
  isr$XML.setAttribute(xXml, '/*[1]', 'display', STB$JOB.getJobParameter('REFERENCE_DISPLAY', cnJobid));

  if sAuditType = 'SRAUDIT' then
    for rSRAuditRow in
      ( select Ordernum, Reference, Version, TheType,
               case when substr(REFERENCE,1,7) = 'REPORT ' then substr(REFERENCE, 8) else null end repid
        from xmltable('/AUDITFILE/AUDIT'
                       passing xXml
                       columns
                         ORDERNUM integer     path '@NUMBER',
                         REFERENCE varchar2(50) path '@REFERENCE',
                         Version   varchar2(50) path 'DATA[1]/VERSION',
                         THETYPE varchar2(30) path 'TYPE')
        order by ORDERNUM ) loop
      isr$trace.debug('current sraudit row', 'Row: '||rSRAuditRow.ORDERNUM||'Repid: '||rSRAuditRow.repid||', Reference: '||rSRAuditRow.Reference||
                      ', Version: '||rSRAuditRow.Version||', Type: '||rSRAuditRow.TheType, sCurrentName);
      begin
        If rSRAuditRow.repid is not null then
          select distinct to_char(min(repid)) into sRepid from stb$report where to_char(parentrepid) = rSRAuditRow.repid;
          -- we may have a reportaudit piece in xAuditPiece from a previous loop step where the version was wrong. Otherwise retrieve it
          if xAuditPiece is null then
            xAuditPiece := stb$audit.getAudit('REPORTAUDIT', sRepid);
            if xAuditPiece is not null then
              sRepAuditRepid := sRepid;
              isr$trace.debug('found reportaudit', 'reportaudit Repid: '||sRepAuditRepid, sCurrentName, xAuditPiece);
              sRepAuditVers := isr$XML.valueOf(xAuditPiece, '/AUDITFILE/@VERSION');
            end if;
          end if;
          -- we got the audit report or have it from previous loop step
          if xAuditPiece is not null and sRepAuditRepid = sRepid and sRepAuditVers = rSRAuditRow.Version then
            xXml := isr$XML.InsertChildXMLFragment(xXml, '/AUDITFILE/AUDIT['||rSRAuditRow.ORDERNUM||']', xAuditPiece);
            isr$trace.debug('xXml', 'after InsertChildXMLFragment', sCurrentName, xXml);
            xAuditPiece := null;
            sRepAuditVers := null;
            sRepAuditRepid := null;
          else
            isr$trace.debug('Report audit version not fitting', 'sRepAuditRepid: '||sRepAuditRepid||', sRepAuditVers: '||sRepAuditVers, sCurrentName);
          end if;
        end if;
      exception when others then
        isr$trace.error('error', sqlerrm(sqlcode), sCurrentName, xXml);
      end;

    end loop;
    isr$trace.debug('xXml', 'after finished the loop', sCurrentName, xXml);

 end if;

  select xmlelement ("TRANSLATION"
                   , (select XMLAGG (XMLELEMENT ("TRANSLATIONENTRY", xmlattributes (parametername as "KEY"), parametervalue))
                        from (select distinct parametername, UTD$MSGLIB.getMsg (description, STB$SECURITY.getCurrentLanguage) parametervalue from STB$SYSTEMPARAMETER
                              union
                              select distinct parametername, UTD$MSGLIB.getMsg (description, STB$SECURITY.getCurrentLanguage) parametervalue from STB$REPTYPEPARAMETER
                              union
                              select distinct parametername, UTD$MSGLIB.getMsg (description, STB$SECURITY.getCurrentLanguage) parametervalue from isr$DIALOG)))
    into xXml1
    from DUAL;

  isr$XML.InsertChildXMLFragment(xXml, '/*[1]', isr$XML.XMLToClob(xXml1));

  for rGetAuditStylesheet in ( select stylesheet
                                 from isr$stylesheet
                                where structured = 'A') loop
    sReturn := isr$xml.XSLTTransform(rGetAuditStylesheet.stylesheet, xXml.getClobVal(), cResult, null, cnJobid);
  end loop;
  
  if cResult is not null then
    oErrorObj := STB$DOCKS.saveXmlReport(XMLTYPE(cResult), cnJobid, sAuditType||'_audit.xml');
    if oErrorObj.sSeverityCode != stb$typedef.cnNoError then
      Stb$job.markDBJobAsBroken(cnJobId, sIp, sPort, oErrorObj.sErrorCode, oErrorObj.sErrorMessage, oErrorObj.sSeverityCode);
    end if;
  end if;

  isr$trace.stat('end', 'Function '||sCurrentName||' finished', sCurrentName);
  return oErrorObj;

exception
  when others then
    sUserMsg := utd$msglib.getmsg ('exUnexpectedError', nCurrentLanguage,  csP1 => sqlerrm(sqlcode));
    sImplMsg := sCurrentName || rtrim(': '||sImplMsg,': ');
    sDevMsg  := substr(dbms_utility.format_error_stack ||crlf|| dbms_utility.format_error_backtrace, 1,4000);
    oErrorObj.handleError ( Stb$typedef.cnSeverityCritical, sUserMsg, sImplMsg, sDevMsg );
    Stb$job.markDBJobAsBroken(cnJobId, sIp, sPort, oErrorObj.sErrorCode, oErrorObj.sErrorMessage, oErrorObj.sSeverityCode);
    return oErrorObj;
end createAuditFile;

--*******************************************************************************************************************
function archiveReports( cnJobid in     number,
                         cxXml   in out xmltype)
  return STB$OERROR$RECORD
is
  exFilesAlreadyArchived  exception;
  exCouldNotCreateZipFile exception;
  sCurrentName            constant varchar2(100) := $$PLSQL_UNIT||'.archiveReports (JobID='||cnJobId||')';
  sUserMsg                varchar2(4000);
  sDevMsg                 varchar2(4000);
  sImplMsg                varchar2(4000);
  oErrorObj               STB$OERROR$RECORD :=STB$OERROR$RECORD();
  sColumns                varchar2(4000);
  sInsertColumns          varchar2(4000);
  sPKColumns              varchar2(4000);
  sPKValues               varchar2(4000);
  sCondition              varchar2(4000);
  sValues                 varchar2(32767);
  sSqlStatement           varchar2(32767);
  sFilename               varchar2(256);
  sFilename2              varchar2(256);
  clScript                clob;
  clDeleteScript          clob;
  clDisableScript         clob;
  clEnableScript          clob;
  blBlob                  blob;
  clClob                  clob;
  blUnzippedBlob          blob;
  bZip                    boolean;
  blZipFile               blob;
  sReturn                 varchar2(4000);
  nEntryid                number;

  type tCursor is ref cursor;
  cCursor tCursor;

begin
  isr$trace.stat('begin', 'Function '||sCurrentName||' started.', sCurrentName);

  sCondition := '(select repid from stb$report where STATUS != '||STB$TYPEDEF.cnStatusFinal||' and '||STB$JOB.getJobParameter('CONDITION', cnJobid)||')';

  for rGetTables in ( select 1, 'STB$REPORT' table_name, 'REPID in '||sCondition condition from dual union
                      select 2, 'STB$REPORTPARAMETER', 'REPID in '||sCondition from dual union
                      select 3, 'ISR$CRIT', 'REPID in '||sCondition from dual union
                      select 4, 'ISR$REPORT$FILE', 'REPID in '||sCondition from dual union
                      select 5, 'ISR$REPORT$GROUPING$TAB', 'REPID in '||sCondition from dual union
                      select 6, 'STB$AUDITTRAIL', 'audittype = ''REPORTAUDIT'' and reference in '||sCondition from dual union
                      select 7, 'STB$DOCTRAIL', 'nodetype = ''REPORT'' and nodeid in '||sCondition from dual union
                      select 8, 'ISR$DOC$PROPERTY', 'docid in (select docid from stb$doctrail where nodetype = ''REPORT'' and nodeid in '||sCondition||' )' from dual union
                      select 9, 'STB$ESIG', 'nodetype = ''REPORT'' and reference in '||sCondition from dual union
                      -- ISRC-1044
                      select 10, 'ISR$REPORT$GROUPING$TABLE', 'REPID in '||sCondition from dual
                      order by 1) loop

    sColumns := null;
    sInsertColumns := null;
    for rGetTableCols in (select utc.column_name, utc.data_type
                          from   user_tab_cols utc
                          where  utc.table_name = rGetTables.table_name
                          and    utc.data_type NOT in ('BLOB','CLOB')) loop

      sColumns := sColumns ||
                  case
                    when rGetTableCols.data_type = 'DATE' then '''TO_DATE(''''''||TO_CHAR('||rGetTableCols.column_name||',''YYYYMMDDHH24MISS'')||'''''',''''YYYYMMDDHH24MISS'''')'''
                    else '''''''''||REPLACE('||rGetTableCols.column_name||','''''''','''''''''''')||'''''''''
                  end || ', ';
      sInsertColumns := sInsertColumns || rGetTableCols.column_name || ', ';
    end loop;
    sColumns := RTRIM (sColumns, ', ');
    sInsertColumns := RTRIM (sInsertColumns, ', ');

    sSqlStatement := 'SELECT '||REPLACE(sColumns, ', ', '||'', ''||')||
                      ' from '||UPPER(rGetTables.table_name)||
                     ' where '||rGetTables.condition;
    isr$trace.debug('sSqlStatement'||rGetTables.table_name||'.sql', 'sSqlStatement: '||sSqlStatement, sCurrentName);

    open cCursor for sSqlStatement;
    loop
     fetch cCursor into sValues;
      exit when cCursor%NOTFOUND;
      clScript := clScript || '  insert into '||rGetTables.table_name||'('||sInsertColumns||')' || chr(10);
      clScript := clScript || '  values ('||sValues||');' || chr(10);
      clScript := clScript || '  clLogFile := clLogFile||''Restored '||rGetTables.table_name||': '||REPLACE(sValues, '''', '''''')||'''||chr(10);'||chr(10);
    end loop;
    close cCursor;

    sPKColumns := null;
    for rGetTablePkCols in (select distinct ucc.column_name
                            from   user_cons_columns ucc,
                                   user_constraints uc
                            where  uc.table_name = rGetTables.table_name
                            and    uc.constraint_name = ucc.constraint_name
                            and    uc.constraint_type = 'P'
                            and    uc.status = 'ENABLED'
                            order by 1) loop
      sPKColumns := sPKColumns || rGetTablePkCols.column_name || ',';
    end loop;
    sPKColumns := RTRIM (sPKColumns, ',');

    for rGetTableCols in (select utc.column_name
                               , utc.data_type
                               , NVL ( (select 'T'
                                          from user_tab_cols
                                         where table_name = utc.table_name
                                           and column_name = 'FILENAME'), 'F') namedCol
                            from user_tab_cols utc
                           where utc.table_name = rGetTables.table_name
                             and utc.data_type in ('BLOB', 'CLOB')) loop

      sSqlStatement := 'SELECT '||'''''''''||replace('||replace(sPKColumns, ',', ', '''''''', '''''''''''' )||'''''',''''''||REPLACE(')||', '''''''', '''''''''''' )||'''''''''||'
                              ,'||case when rGetTableCols.data_type = 'CLOB' then rGetTableCols.column_name else 'EMPTY_CLOB()' END||'
                              ,'||case when rGetTableCols.data_type = 'BLOB' then rGetTableCols.column_name else 'EMPTY_BLOB()' END||'
                              ,'||case when rGetTableCols.namedCol = 'T' then 'FILENAME' else 'null FILENAME' end||'
                          from '||UPPER(rGetTables.table_name)||'
                         where '||rGetTables.condition||'
                           and '||rGetTableCols.column_name||' is not null
                           and DBMS_LOB.getLength('||rGetTableCols.column_name||') > 0';
      isr$trace.debug('sSqlStatement'||rGetTables.table_name||'.sql', 'sSqlStatement: '||sSqlStatement, sCurrentName);

      open cCursor for sSqlStatement;
      loop
       fetch cCursor into sPKValues, clClob, blBlob, sFilename2;
        exit when cCursor%NOTFOUND;
        select STB$JOBTRAIL$SEQ.NEXTVAL into nEntryid from DUAL;
        sFilename := TO_CHAR(nEntryid)||'.'||rGetTableCols.data_type||'.'||sFilename2;
        if DBMS_LOB.getLength(blBlob) > 0 then
          begin
            blUnzippedBlob := stb$java.unzip(blBlob);
            if DBMS_LOB.GETLENGTH(blUnzippedBlob) != 0 then
              blBlob := blUnzippedBlob;
              bZip := true;
            else
              bZip := false;
           end if;
          exception when others then
            bZip := false;
          end;
       end if;
        insert into STB$JOBTRAIL (ENTRYID
                                , JOBID
                                , DOCUMENTTYPE
                                , REFERENCE
                                , FILENAME
                                , TIMESTAMP
                                , DOWNLOAD
                                , TEXTFILE
                                , BINARYFILE)
           (select nEntryid
                 , cnJobId
                 , rGetTableCols.data_type
                 , rGetTables.table_name||'.'||rGetTableCols.column_name||'_'||sPKValues
                 , sFilename
                 ,  sysdate
                 , 'F'
                 , clClob
                 , blBlob
              from DUAL);

        clScript := clScript || '  update '||rGetTables.table_name|| chr(10);
        clScript := clScript || '     set '||rGetTableCols.column_name||' = (select '||case when rGetTableCols.data_type = 'CLOB' then 'TEXTFILE' else case when bZip then 'STB$JAVA.zip(BINARYFILE, '''||sFilename2||''')' else 'BINARYFILE' end END||' from stb$jobtrail where jobid = &&jobid and filename = '''||sFileName||''' )' || chr(10);
        clScript := clScript || '   where ('||sPKColumns||') in (select '||sPKValues||' from dual);' || chr(10);
        clScript := clScript || '  clLogFile := clLogFile||''Restored '||rGetTables.table_name||'.'||rGetTableCols.column_name||':'||REPLACE(sPKValues, '''', '''''')||'''||chr(10);'||chr(10);
        if bZip and rGetTables.table_name = 'STB$DOCTRAIL' then
          clScript := clScript || '  update '||rGetTables.table_name|| chr(10);
          clScript := clScript || '     set checksum = STB$UTIL.getCheckSum('||rGetTableCols.column_name||')' || chr(10);
          clScript := clScript || '   where ('||sPKColumns||') in (select '||sPKValues||' from dual);' || chr(10);
          clScript := clScript || '  clLogFile := clLogFile||''Checksum calculated '||rGetTables.table_name||'.'||rGetTableCols.column_name||':'||REPLACE(sPKValues, '''', '''''')||'''||chr(10);'||chr(10);
       end if;
      end loop;
      close cCursor;

    end loop;

  end loop;

  for rGetTables in ( select 1, 'STB$REPORT' table_name, 'REPID in '||sCondition condition from dual union
                      select 2, 'STB$REPORTPARAMETER', 'REPID in '||sCondition from dual union
                      select 3, 'ISR$CRIT', 'REPID in '||sCondition from dual union
                      select 4, 'ISR$REPORT$FILE', 'REPID in '||sCondition from dual union
                      select 5, 'ISR$REPORT$GROUPING$TAB', 'REPID in '||sCondition from dual union
                      select 6, 'STB$AUDITTRAIL', 'audittype = ''REPORTAUDIT'' and reference in '||sCondition from dual union
                      select 7, 'STB$DOCTRAIL', 'nodetype = ''REPORT'' and nodeid in '||sCondition from dual union
                      select 8, 'ISR$DOC$PROPERTY', 'docid in (select docid from stb$doctrail where nodetype = ''REPORT'' and nodeid in '||sCondition||' )' from dual union
                      select 9, 'STB$ESIG', 'nodetype = ''REPORT'' and reference in '||sCondition from dual union
                      -- ISRC-1044
                      select 10, 'ISR$REPORT$GROUPING$TABLE', 'REPID in '||sCondition from dual
                      order by 1 DESC) loop


    for rGetConstraints in (select constraint_name,
                                   table_name
                            from   user_constraints
                            where  table_name = UPPER (rGetTables.table_name)
                            and    constraint_type in ('R')
                            and    status = 'ENABLED') loop
      clDisableScript := clDisableScript || '  execute immediate ''ALTER TABLE ' || rGetConstraints.table_name || ' DISABLE CONSTRAINT ' || rGetConstraints.constraint_name || ''';' || chr(10);
      clEnableScript := clEnableScript || '  execute immediate ''ALTER TABLE ' || rGetConstraints.table_name || ' ENABLE CONSTRAINT ' || rGetConstraints.constraint_name || ''';' || chr(10);
    end loop;

    clDeleteScript := clDeleteScript || '  delete from '||rGetTables.table_name||' where '||rGetTables.condition||';' || chr(10);
    clDeleteScript := clDeleteScript || '  clLogFile := clLogFile||''Archived '||rGetTables.table_name||':'||REPLACE(rGetTables.condition, '''', '''''')||'''||chr(10);'||chr(10);

  end loop;

  clDeleteScript := clDeleteScript || '  update STB$REPORT set STATUS = '||STB$TYPEDEF.cnReportAbort||' where status = '||STB$TYPEDEF.cnStatusFinal||' and '||STB$JOB.getJobParameter('CONDITION', cnJobid)||';'|| chr(10);


  if TRIM(clScript) is null then
    raise exFilesAlreadyArchived;
 end if;

  clScript := clScript || '  update STB$REPORT set STATUS = '||to_char(STB$TYPEDEF.cnStatusFinal)||' where status = '||to_char(STB$TYPEDEF.cnReportAbort)||' and '||STB$JOB.getJobParameter('CONDITION', cnJobid)||';'|| chr(10);
  clScript := clScript || '  update isr$ARCHIVED$REPORTS set RESTORED = ''T'' where JOBID = '||TO_CHAR(cnJobid)||';'|| chr(10);
  --clScript := clScript || '  delete from isr$ARCHIVED$REPORTS where JOBID = '||TO_CHAR(cnJobid)||';'|| chr(10);

  clScript := 'DECLARE'||chr(10)||
              '  clLogFile  clob;'||chr(10)||
              '  oErrorObj STB$OERROR$RECORD := STB$OERROR$RECORD();'||chr(10)||
              'BEGIN'||chr(10)||
              '  begin'||chr(10)||
                   clScript||chr(10)||
              '    for rGetReports in (select repid from stb$report where '||STB$JOB.getJobParameter('CONDITION', cnJobid)||') loop'||chr(10)||
              '      oErrorObj := isr$REPORTTYPE$PACKAGE.updateCheckSum(rGetReports.repid);'||chr(10)||
              '    end loop;'||chr(10)||
              '    commit;'||chr(10)||
              '  exception when others then'||chr(10)||
              '    rollback;'||chr(10)||
              '    clLogFile := clLogFile||''Error during restoring: ''||SQLERRM||chr(10);'||chr(10)||
              '  end;'||chr(10)||
              '  update isr$ARCHIVED$REPORTS set LOGFILE = LOGFILE || clLogFile where JOBID = '||TO_CHAR(cnJobid)||';'||chr(10)||
              'END;';
  isr$trace.debug('script.sql', 'clScript', sCurrentName, clScript);

  insert into STB$JOBTRAIL (ENTRYID
                          , JOBID
                          , DOCUMENTTYPE
                          , FILENAME
                          , TIMESTAMP
                          , DOWNLOAD
                          , TEXTFILE
                          , BINARYFILE)
     (select STB$JOBTRAIL$SEQ.NEXTVAL
           , cnJobId
           , 'SCRIPT'
           , STB$UTIL.getSystemParameter('SYSTEM_OID')||'_statements.sql'
           ,  sysdate
           , 'F'
           , clScript
           , null
        from DUAL);

  commit;

  -- zip file
  begin
    blZipFile := STB$JAVA.zip(cnJobid);
    insert into STB$JOBTRAIL (ENTRYID
                            , JOBID
                            , DOCUMENTTYPE
                            , FILENAME
                            , TIMESTAMP
                            , DOWNLOAD
                            , BLOBFLAG
                            , TEXTFILE
                            , BINARYFILE)
       (select STB$JOBTRAIL$SEQ.NEXTVAL
             , cnJobId
             , csTarget
             , getFileName(STB$JOB.getJobParameter('TO', cnJobId))
             ,  sysdate
             , 'T'
             , 'B'
             , EMPTY_CLOB()
             , blZipFile
          from DUAL);

    delete from STB$JOBTRAIL
     where jobid = cnJobid
       and documenttype != csTarget;

  exception when others then
    raise exCouldNotCreateZipFile;
  end;

  insert into isr$archived$reports (JOBID
                                  , DIRECTORY
                                  , ZIPFILE
                                  , NODETYPE
                                  , NODEID
                                  , MACHINE
                                  , CHECKSUM)
  values (cnJobid
        , STB$JOB.getJobParameter ('TO', cnJobId)
        , EMPTY_BLOB() --blZipFile
        , STB$JOB.getJobParameter ('NODETYPE', cnJobId)
        , STB$JOB.getJobParameter ('REFERENCE', cnJobId)
        , sIp
        , STB$UTIL.getChecksum(blZipFile));
  commit;

  -- delete entries
  clDeleteScript := 'DECLARE'||chr(10)||
                    '  clLogFile  clob;'||chr(10)||
                    '  oErrorObj STB$OERROR$RECORD := STB$OERROR$RECORD();'||chr(10)||
                    'BEGIN'||chr(10)||
                    '  begin'||chr(10)||
                         clDisableScript||chr(10)||
                         clDeleteScript||chr(10)||
                    '    for rGetReports in (select repid from stb$report where '||STB$JOB.getJobParameter('CONDITION', cnJobid)||') loop'||chr(10)||
                    '      oErrorObj := isr$REPORTTYPE$PACKAGE.updateCheckSum(rGetReports.repid);'||chr(10)||
                    '    end loop;'||chr(10)||
                    '    commit;'||chr(10)||
                         clEnableScript||chr(10)||
                    '  exception when others then'||chr(10)||
                    '    rollback;'||chr(10)||
                    '    clLogFile := clLogFile||''Error during archiving: ''||SQLERRM||chr(10);'||chr(10)||
                    '  end;'||chr(10)||
                    '  update isr$ARCHIVED$REPORTS set LOGFILE = clLogFile where JOBID = '||TO_CHAR(cnJobid)||';'||chr(10)||
                    'END;';
  isr$trace.debug('clDeleteScript', 'clDeleteScript', sCurrentName, clDeleteScript);


  update isr$ARCHIVED$REPORTS set deletescript = clDeleteScript where JOBID = TO_CHAR(cnJobid);

  sReturn := STB$UTIL.execute_Clob(clDeleteScript);

  <<WriteActionLog>>
  declare
    sNodeType      constant stb$jobparameter.parametervalue%type := STB$JOB.getJobParameter('NODETYPE', cnJobid);
    sTitle         constant stb$jobparameter.parametervalue%type := STB$JOB.getJobParameter('TITLE', cnJobid);
    sReference     constant stb$jobparameter.parametervalue%type := STB$JOB.getJobParameter('REFERENCE', cnJobid);
    sReferenceDisp constant stb$jobparameter.parametervalue%type := STB$JOB.getJobParameter('REFERENCE_DISPLAY', cnJobid);
    sLogText                varchar2(4000);
  begin
    sLogText := sTitle||' ['||sNodeType|| case when sNodeType = 'TITLE' then null else '/'||sReferenceDisp||'/'||sReference end||']';
    ISR$ACTION$LOG.setActionLog('MAINTENANCE', Utd$msglib.GetMsg ('ARCHIVE_REPORTS', Stb$security.GetCurrentLanguage)||' '||sLogText);
  end WriteActionLog;

  isr$trace.stat('end', 'Function '||sCurrentName||' finished',sCurrentName);
  return oErrorObj;

exception
  when exCouldNotCreateZipFile then
    sUserMsg := utd$msglib.getmsg ('exCouldNotCreateZipFileText', nCurrentLanguage);
    sImplMsg := sCurrentName || rtrim(': '||sImplMsg,': ');
    sDevMsg  := substr(dbms_utility.format_error_stack ||crlf|| dbms_utility.format_error_backtrace, 1,4000);
    oErrorObj.handleError ( Stb$typedef.cnSeverityMessage, sUserMsg, sImplMsg, sDevMsg );
    Stb$job.markDBJobAsBroken(cnJobId, sIp, sPort, oErrorObj.sErrorCode, oErrorObj.sErrorMessage, oErrorObj.sSeverityCode);
    return oErrorObj;
  when exFilesAlreadyArchived then
    sUserMsg := utd$msglib.getmsg ('exFilesAlreadyArchivedText', nCurrentLanguage);
    sImplMsg := sCurrentName || rtrim(': '||sImplMsg,': ');
    sDevMsg  := substr(dbms_utility.format_error_stack ||crlf|| dbms_utility.format_error_backtrace, 1,4000);
    oErrorObj.handleError ( Stb$typedef.cnSeverityMessage, sUserMsg, sImplMsg, sDevMsg );
    Stb$job.markDBJobAsBroken(cnJobId, sIp, sPort, oErrorObj.sErrorCode, oErrorObj.sErrorMessage, oErrorObj.sSeverityCode);
    return oErrorObj;
  when others then
    sUserMsg := utd$msglib.getmsg ('exUnexpectedError', nCurrentLanguage,  csP1 => sqlerrm(sqlcode));
    sImplMsg := sCurrentName || rtrim(': '||sImplMsg,': ');
    sDevMsg  := substr(dbms_utility.format_error_stack ||crlf|| dbms_utility.format_error_backtrace, 1,4000);
    oErrorObj.handleError ( Stb$typedef.cnSeverityCritical, sUserMsg, sImplMsg, sDevMsg );
    Stb$job.markDBJobAsBroken(cnJobId, sIp, sPort, oErrorObj.sErrorCode, oErrorObj.sErrorMessage, oErrorObj.sSeverityCode);
    return oErrorObj;
end archiveReports;

--*******************************************************************************************************************
function extractReports( cnJobid in     number,
                         cxXml   in out xmltype)
  return STB$OERROR$RECORD
is
  sCurrentName   constant varchar2(100) := $$PLSQL_UNIT||'.extractReports (JobID='||cnJobId||')';
  sUserMsg               varchar2(4000);
  sDevMsg                varchar2(4000);
  sImplMsg               varchar2(4000);
  oErrorObj               STB$OERROR$RECORD :=STB$OERROR$RECORD();

  exManipulatedFile exception;
  exAnotherISRFiles exception;

  cursor cGetZipFile is
    select binaryfile
      from stb$jobtrail
     where documenttype = csTarget
       and jobid = cnJobId
     order by ENTRYID;

  cursor cCheckOid is
    select 'T'
      from stb$jobtrail
     where jobid = cnJobId
       and filename like STB$UTIL.getSystemParameter('SYSTEM_OID')||'%'
       and documenttype not in (csTarget, csConfig);

  sExists      varchar2(1);

  cursor cGetScript is
    select REPLACE(textfile, '&&jobid', jobid) textfile
      from stb$jobtrail
     where filename = STB$UTIL.getSystemParameter('SYSTEM_OID')||'_statements.sql'
       and jobid = cnJobId
     order by ENTRYID;

  sReturn      varchar2(4000);
begin
  isr$trace.stat('begin', 'Function '||sCurrentName||' started.', sCurrentName);

  -- check manipulation
  for rGetZipFile in cGetZipFile loop
    if STB$UTIL.getChecksum(rGetZipFile.binaryfile) != STB$JOB.getJobParameter ('CHECKSUM', cnJobId) then
      raise exManipulatedFile;
   end if;
  end loop;

  -- unzip files
  for rGetZipFile in cGetZipFile loop
    STB$JAVA.unzip(cnJobid, rGetZipFile.binaryfile);
  end loop;

  -- check oid
  sExists := 'F';
  open cCheckOid;
 fetch cCheckOid into sExists;
  close cCheckOid;

  if sExists = 'F' then
    raise exAnotherISRFiles;
 end if;

  -- execute script
  for rGetScript in cGetScript loop
    isr$trace.debug('textfile.sql', sReturn, sCurrentName, rGetScript.textfile);
    sReturn := STB$UTIL.execute_Clob(rGetScript.textfile);
    isr$trace.debug('status sReturn', 'sReturn: ', sReturn, sCurrentName);
  end loop;

  commit;

  isr$trace.stat('end', 'Function '||sCurrentName||' finished',sCurrentName);
  return oErrorObj;

exception
  when exManipulatedFile then
    sUserMsg := utd$msglib.getmsg ('exManipulatedFileText', nCurrentLanguage);
    sImplMsg := sCurrentName || rtrim(': '||sImplMsg,': ');
    sDevMsg  := substr(dbms_utility.format_error_stack ||crlf|| dbms_utility.format_error_backtrace, 1,4000);
    oErrorObj.handleError ( Stb$typedef.cnSeverityMessage, sUserMsg, sImplMsg, sDevMsg );
    Stb$job.markDBJobAsBroken(cnJobId, sIp, sPort, oErrorObj.sErrorCode, oErrorObj.sErrorMessage, oErrorObj.sSeverityCode);
    return oErrorObj;
  when exAnotherISRFiles then
    sUserMsg := utd$msglib.getmsg ('exAnotherISRFilesText', nCurrentLanguage);
    sImplMsg := sCurrentName || rtrim(': '||sImplMsg,': ');
    sDevMsg  := substr(dbms_utility.format_error_stack ||crlf|| dbms_utility.format_error_backtrace, 1,4000);
    oErrorObj.handleError ( Stb$typedef.cnSeverityMessage, sUserMsg, sImplMsg, sDevMsg );
    Stb$job.markDBJobAsBroken(cnJobId, sIp, sPort, oErrorObj.sErrorCode, oErrorObj.sErrorMessage, oErrorObj.sSeverityCode);
    return oErrorObj;
  when others then
    sUserMsg := utd$msglib.getmsg ('exUnexpectedError', nCurrentLanguage,  csP1 => sqlerrm(sqlcode));
    sImplMsg := sCurrentName || rtrim(': '||sImplMsg,': ');
    sDevMsg  := substr(dbms_utility.format_error_stack ||crlf|| dbms_utility.format_error_backtrace, 1,4000);
    oErrorObj.handleError ( Stb$typedef.cnSeverityCritical, sUserMsg, sImplMsg, sDevMsg );
    Stb$job.markDBJobAsBroken(cnJobId, sIp, sPort, oErrorObj.sErrorCode, oErrorObj.sErrorMessage, oErrorObj.sSeverityCode);
    return oErrorObj;
end extractReports;

--*******************************************************************************************************************
function zipLogTable( cnJobid in     number,
                      cxXml   in out xmltype)
  return STB$OERROR$RECORD
is
  exCouldNotCreateZipFile exception;
  sCurrentName   constant varchar2(100) := $$PLSQL_UNIT||'.zipLogTable (JobID='||cnJobId||')';
  sUserMsg                varchar2(4000);
  sDevMsg                 varchar2(4000);
  sImplMsg                varchar2(4000);
  oErrorObj               STB$OERROR$RECORD :=STB$OERROR$RECORD();
  sPKValues               varchar2(4000);
  sFilename               varchar2(200);
  sSqlStatement           varchar2(32676);
  blBlob                  blob;
  clClob                  clob;
  blZipFile               blob;
  nEntryid                number;

  type tCursor is ref cursor;
  cCursor tCursor;

begin
  isr$trace.stat('begin', 'Function '||sCurrentName||' started.', sCurrentName);

  -- get server log
  if NVL(STB$JOB.getJobParameter('FILTER_DISPLAY_SERVER_LOG', cnJobId),'F') = 'T' then
    begin
      oErrorObj := createConfigurationFile(cnJobId, 'CAN_DISPLAY_SERVER_LOG');
      if NVL(STB$JOB.getJobParameter('FILTER_TRANSFORMATION', cnJobId),'T') = 'T' then
        oErrorObj := ISR$TRANSFORM.performTransformation(cnJobid, cxXml);
     end if;
      insert into STB$JOBTRAIL (ENTRYID
                              , JOBID
                              , DOCUMENTTYPE
                              , REFERENCE
                              , FILENAME
                              , TIMESTAMP
                              , DOWNLOAD
                              , TEXTFILE
                              , BINARYFILE)
         (select STB$JOBTRAIL$SEQ.NEXTVAL
               , cnJobId
               , 'CLOB'
               , reference
               , 'CAN_DISPLAY_SERVER_LOG.'
                 || case when documenttype = 'ROHDATEN' then 'XML' else 'HTM' end
               ,  sysdate
               , 'F'
               , textfile
               , binaryfile
            from stb$jobtrail
           where jobid = cnJobid
             and (documenttype = 'ROHDATEN' or documenttype = 'TRANSFORMATION'));
      delete from stb$jobtrail where jobid = cnJobid and (documenttype = 'ROHDATEN' or documenttype = 'TRANSFORMATION');
    exception when others then
      isr$trace.error('error',substr(dbms_utility.format_error_stack ||crlf|| dbms_utility.format_error_backtrace, 1,4000), sCurrentName);
    end;
 end if;

  -- get trace log
  begin
    oErrorObj := createConfigurationFile(cnJobId, 'CAN_DISPLAY_LOG_REPORT');
    if NVL(STB$JOB.getJobParameter('FILTER_TRANSFORMATION', cnJobId),'T') = 'T' then
      oErrorObj := ISR$TRANSFORM.performTransformation(cnJobid, cxXml);     -- vs 08 Jul. 2016 changed from STB$TRANSFORM to ISR$TRANSFORM [ISRC546]
   end if;
    insert into STB$JOBTRAIL (ENTRYID
                            , JOBID
                            , DOCUMENTTYPE
                            , REFERENCE
                            , FILENAME
                            , TIMESTAMP
                            , DOWNLOAD
                            , TEXTFILE
                            , BINARYFILE)
       (select STB$JOBTRAIL$SEQ.NEXTVAL
             , cnJobId
             , 'CLOB'
             , reference
             , 'CAN_DISPLAY_LOG_REPORT.'
               || case when documenttype = 'ROHDATEN' then 'XML' else 'HTM' end
             ,  sysdate
             , 'F'
             , textfile
             , binaryfile
          from stb$jobtrail
         where jobid = cnJobid
             and (documenttype = 'ROHDATEN' or documenttype = 'TRANSFORMATION'));
    delete from stb$jobtrail where jobid = cnJobid and (documenttype = 'ROHDATEN' or documenttype = 'TRANSFORMATION');
  exception when others then
    isr$trace.error('error',substr(dbms_utility.format_error_stack ||crlf|| dbms_utility.format_error_backtrace, 1,4000), sCurrentName);
  end;

  if NVL(STB$JOB.getJobParameter('FILTER_DISPLAY_LOG_FILES', cnJobId),'T') = 'T' then
    for rGetTableCols in (select utc.column_name
                               , utc.data_type
                            from user_tab_cols utc
                           where utc.table_name = 'ISR$LOG'
                             and utc.data_type in ('BLOB', 'CLOB')
                             and utc.column_name like 'LOG%') loop

      sSqlStatement := 'SELECT entryid
                              ,'||case when rGetTableCols.data_type = 'CLOB' then rGetTableCols.column_name else 'EMPTY_CLOB()' end||'
                              ,'||case when rGetTableCols.data_type = 'BLOB' then rGetTableCols.column_name else 'EMPTY_BLOB()' end||'
                              ,entryid||''_'||rGetTableCols.data_type||'_''||actiontaken filename
                          from isr$LOG
                           '||STB$JOB.getJobParameter('WHERE',cnJobid)||'
                           and '||rGetTableCols.column_name||' is not null
                           and DBMS_LOB.getLength('||rGetTableCols.column_name||') > 0'
                           || 'and entryid > ( select last_number from user_sequences where sequence_name=''STB$LOG$SEQ'') - STB$UTIL.getSystemParameter(''MAX_LOG_REPORT_ENTRIES'')'; -- vs: [ISRC-546]
      isr$trace.debug('sSqlStatement', 'sSqlStatement: '||sSqlstatement, sCurrentName);

      open cCursor for sSqlStatement;
      loop
       fetch cCursor into sPKValues, clClob, blBlob, sFilename;
        exit when cCursor%NOTFOUND;
        select STB$JOBTRAIL$SEQ.NEXTVAL into nEntryid from DUAL;
        insert into STB$JOBTRAIL (ENTRYID
                                , JOBID
                                , DOCUMENTTYPE
                                , REFERENCE
                                , FILENAME
                                , TIMESTAMP
                                , DOWNLOAD
                                , TEXTFILE
                                , BINARYFILE)
           (select nEntryid
                 , cnJobId
                 , rGetTableCols.data_type
                 , sPKValues
                 , sFilename
                 ,  sysdate
                 , 'F'
                 , clClob
                 , blBlob
              from DUAL);
      end loop;
      close cCursor;

    end loop;
 end if;

  -- zip file
  begin
    blZipFile := STB$JAVA.zip(cnJobid);
    insert into STB$JOBTRAIL (ENTRYID
                            , JOBID
                            , DOCUMENTTYPE
                            , FILENAME
                            , TIMESTAMP
                            , DOWNLOAD
                            , TEXTFILE
                            , BLOBFLAG
                            , DOCSIZE
                            , BINARYFILE)
       (select STB$JOBTRAIL$SEQ.NEXTVAL
             , cnJobId
             , csTarget
             , getFileName(STB$JOB.getJobParameter('TO', cnJobId))
             ,  sysdate
             , 'T'
             , EMPTY_CLOB()
             , 'B'
             , dbms_lob.getLength(blZipFile)
             , blZipFile
          from DUAL);

    delete from STB$JOBTRAIL
     where jobid = cnJobid
       and documenttype != csTarget;

  exception when others then
    raise exCouldNotCreateZipFile;
  end;

  commit;

  isr$trace.stat('end', 'Function '||sCurrentName||' finished',sCurrentName);
  return oErrorObj;

exception
  when exCouldNotCreateZipFile then
    sUserMsg := utd$msglib.getmsg ('exCouldNotCreateZipFileText', nCurrentLanguage);
    sImplMsg := sCurrentName || rtrim(': '||sImplMsg,': ');
    sDevMsg  := substr(dbms_utility.format_error_stack ||crlf|| dbms_utility.format_error_backtrace, 1,4000);
    oErrorObj.handleError ( Stb$typedef.cnSeverityMessage, sUserMsg, sImplMsg, sDevMsg );
    Stb$job.markDBJobAsBroken(cnJobId, sIp, sPort, oErrorObj.sErrorCode, oErrorObj.sErrorMessage, oErrorObj.sSeverityCode);
    return oErrorObj;
  when others then
    sUserMsg := utd$msglib.getmsg ('exUnexpectedError', nCurrentLanguage,  csP1 => sqlerrm(sqlcode));
    sImplMsg := sCurrentName || rtrim(': '||sImplMsg,': ');
    sDevMsg  := substr(dbms_utility.format_error_stack ||crlf|| dbms_utility.format_error_backtrace, 1,4000);
    oErrorObj.handleError ( Stb$typedef.cnSeverityCritical, sUserMsg, sImplMsg, sDevMsg );
    Stb$job.markDBJobAsBroken(cnJobId, sIp, sPort, oErrorObj.sErrorCode, oErrorObj.sErrorMessage, oErrorObj.sSeverityCode);
    return oErrorObj;
end zipLogTable;

--*******************************************************************************************************************
FUNCTION saveLoaderDirIntoJob (cnJobid IN NUMBER, cxXml IN OUT XMLTYPE) RETURN STB$OERROR$RECORD is
  sCurrentName   constant varchar2(100) := $$PLSQL_UNIT||'.saveLoaderDirIntoJob (JobID='||cnJobId||')';
  sUserMsg                varchar2(4000);
  oErrorObj               STB$OERROR$RECORD :=STB$OERROR$RECORD();
  bLoaderDir     blob := empty_blob();
  sReturn        varchar2(32000);
  exNoLoaderDir  exception;
  nLoaderJobid   number;

begin
  isr$trace.stat('begin', 'begin', sCurrentName);
  nLoaderJobid := STB$JOB.getJobParameter('REFERENCE',cnJobid);
  isr$trace.debug('nLoaderJobid', nLoaderJobid, sCurrentName);

  for rGetBinary in (select binaryfile from stb$jobtrail where jobid = nLoaderJobid and documenttype = 'LOADER_DIR') loop
    bLoaderDir := rGetBinary.binaryfile;
  end loop;

  if dbms_lob.getLength(bLoaderDir) < 1 then
    sReturn := ISR$LOADER.loadLoaderDir(sIp, sPort, sContext, sNamespace, nLoaderJobid, 'LOADER_BLOB_FILE', isr$trace.getJavaUserLoglevelStr, isr$trace.getReference, STB$UTIL.getSystemParameter('TIMEOUT_LOADER_THREAD'));

    if sReturn != 'T' then
      sUserMsg := utd$msglib.getmsg ('exNoLoaderDir', nCurrentLanguage, nLoaderJobid,  sReturn);
      raise exNoLoaderDir;
    end if;

    for rGetBinary in (select binaryfile from stb$jobtrail where jobid = nLoaderJobid and documenttype = 'LOADER_DIR') loop
      bLoaderDir := rGetBinary.binaryfile;
    end loop;

  end if;

  if dbms_lob.getLength(bLoaderDir) > 0 then
    insert into STB$JOBTRAIL (ENTRYID
                            , JOBID
                            , DOCUMENTTYPE
                            , FILENAME
                            , TIMESTAMP
                            , DOWNLOAD
                            , TEXTFILE
                            , BLOBFLAG
                            , DOCSIZE
                            , BINARYFILE)
       (select STB$JOBTRAIL$SEQ.NEXTVAL
             , cnJobId
             , csTarget
             , getFileName(STB$JOB.getJobParameter('TO', cnJobId))
             ,  sysdate
             , 'T'
             , EMPTY_CLOB()
             , 'B'
             , dbms_lob.getLength(bLoaderDir)
             , bLoaderDir
          from DUAL);
  else
    sUserMsg := utd$msglib.getmsg ('exNoLoaderDir', nCurrentLanguage, nLoaderJobid);
    raise exNoLoaderDir;
  end if;
  return oErrorObj;
  isr$trace.stat('end', 'end', sCurrentName);
exception
  when exNoLoaderDir then
    oErrorObj.handleError ( Stb$typedef.cnSeverityCritical, sUserMsg, sCurrentName, dbms_utility.format_error_stack ||crlf|| dbms_utility.format_error_backtrace );
    return oErrorObj;
  when others then
    sUserMsg := utd$msglib.getmsg ('exUnexpectedError', nCurrentLanguage,  csP1 => sqlerrm(sqlcode));
    oErrorObj.handleError ( Stb$typedef.cnSeverityCritical, sUserMsg, sCurrentName, dbms_utility.format_error_stack ||crlf|| dbms_utility.format_error_backtrace );
    Stb$job.markDBJobAsBroken(cnJobId, sIp, sPort, oErrorObj.sErrorCode, oErrorObj.sErrorMessage, oErrorObj.sSeverityCode);
    return oErrorObj;
end saveLoaderDirIntoJob;

--*******************************************************************************************************************
function getServerLogFiles( cnJobid in     number,   cxXml   in out xmltype)
  return STB$OERROR$RECORD
is
  exRemote   exception;
  sCurrentName   constant varchar2(100) := $$PLSQL_UNIT||'.getServerLogFiles (JobID='||cnJobId||')';
  sUserMsg                varchar2(4000);
  sDevMsg                 varchar2(4000);
  sImplMsg                varchar2(4000);
  oErrorObj               STB$OERROR$RECORD :=STB$OERROR$RECORD();
  sPKValues               varchar2(4000);
  sFilename               varchar2(200);
  sSqlStatement           varchar2(32676);
  blBlob                  blob;
  clClob                  clob;
  blZipFile               blob;
  nEntryid                number;
  sRemoteEx       clob;
  nSeverity       number              := stb$typedef.cnNoError;
  sFileTranserId    isr$transfer$file$tmp.filetransferid%type;

  type tCursor is ref cursor;
  cCursor tCursor;

begin
  isr$trace.stat('begin', 'Function '||sCurrentName||' started.', sCurrentName);

  -- get server log
  sJavaLogLevel := isr$trace.getJavaUserLoglevelStr;
  select dbms_random.string('A', 8) into sFileTranserId from dual;
  for rGetServer in ( select serverid,  nvl(observer_host, host) observer_host, nvl(observer_port, port) observer_port, servertypename, server_path, timeout
                            from isr$serverobserver$v s
                            where serverid = stb$job.getJobParameter ('SERVERID', cnJobid)) loop
    isr$trace.info('rGetServer', rGetServer.observer_host|| '   rGetServer.observer_port=' || rGetServer.observer_port, sCurrentName);

    if rGetServer.observer_host is not null and  rGetServer.observer_port is not null then
      --oErrorObj := isr$file$transfer$service.saveKeysForServlet('SERVER_LOG_FILES', sFileTranserId, sTransferKeyId);
      sRemoteEx :=  isr$server$base.fetchLogServerFile( rGetServer.observer_host, rGetServer.observer_port,  rGetServer.server_path,
                       stb$job.getJobParameter ('BEGINDATE', cnJobid), stb$job.getJobParameter ('ENDDATE', cnJobid), 'SERVER_FILES', sFileTranserId,
                       stb$gal.getParameterValue('JAVA_DATE_FORMAT'), sJavaLogLevel, isr$trace.getReference, rGetServer.timeout);
      if sRemoteEx is not null then
        isr$trace.debug ('error',  'see sRemoteEx in logclob column',  sCurrentName,  sRemoteEx);
        nSeverity := Stb$typedef.cnSeverityWarning;
        oErrorObj.handleJavaError (nSeverity, sRemoteEx, sCurrentName);
        raise exRemote;
      end if;
    end if;
  end loop;

  insert into STB$JOBTRAIL (ENTRYID
                              , JOBID
                              , DOCUMENTTYPE
                              , REFERENCE
                              , FILENAME
                              , TIMESTAMP
                              , DOWNLOAD
                              , TEXTFILE
                              , BINARYFILE
                              , BLOBFLAG)
         (select STB$JOBTRAIL$SEQ.NEXTVAL
               , cnJobId
               , 'TARGET'
               , cnJobId
               , 'CAN_DISPLAY_SERVER_LOG.zip'
               ,  sysdate
               , 'F'
               , null
               , binaryfile
               , 'B'
            from isr$transfer$file$tmp
           where filetransferId = sFileTranserId);

  delete from isr$transfer$file$tmp where filetransferId = sFileTranserId;
  commit;

  isr$trace.stat('end', 'Function '||sCurrentName||' finished',sCurrentName);
  return oErrorObj;

exception
  when exRemote then
    sUserMsg := utd$msglib.getmsg ('exRemoteFetchLogFileText', nCurrentLanguage,  csP1 => sqlerrm(sqlcode));
    sImplMsg := sCurrentName || rtrim(': '||sImplMsg,': ');
    sDevMsg  := substr(dbms_utility.format_error_stack ||crlf|| dbms_utility.format_error_backtrace, 1,4000);
    oErrorObj.handleError ( Stb$typedef.cnSeverityCritical, sUserMsg, sImplMsg, sDevMsg );
    return oErrorObj;
  when others then
    sUserMsg := utd$msglib.getmsg ('exUnexpectedError', nCurrentLanguage,  csP1 => sqlerrm(sqlcode));
    sImplMsg := sCurrentName || rtrim(': '||sImplMsg,': ');
    sDevMsg  := substr(dbms_utility.format_error_stack ||crlf|| dbms_utility.format_error_backtrace, 1,4000);
    oErrorObj.handleError ( Stb$typedef.cnSeverityCritical, sUserMsg, sImplMsg, sDevMsg );
    Stb$job.markDBJobAsBroken(cnJobId, sIp, sPort, oErrorObj.sErrorCode, oErrorObj.sErrorMessage, oErrorObj.sSeverityCode);
    return oErrorObj;
end getServerLogFiles;


--*******************************************************************************************************************
function callWordmodule( cnJobid in     number,
                         cxXml   in out xmltype)
  return STB$OERROR$RECORD
is
  sCurrentName   constant varchar2(100) := $$PLSQL_UNIT||'.callWordmodule (JobID='||cnJobId||')';
  sUserMsg               varchar2(4000);
  sDevMsg                varchar2(4000);
  sImplMsg               varchar2(4000);
  oErrorObj               STB$OERROR$RECORD :=STB$OERROR$RECORD();
begin
  isr$trace.info('begin', 'Function '||sCurrentName||' started.', sCurrentName);

  STB$JAVA.callWordmodule(cnJobid);

  isr$trace.info('end', 'Function '||sCurrentName||' finished',sCurrentName);
  return oErrorObj;

exception
  when others then
    sUserMsg := utd$msglib.getmsg ('exUnexpectedError', nCurrentLanguage,  csP1 => sqlerrm(sqlcode));
    sImplMsg := sCurrentName || rtrim(': '||sImplMsg,': ');
    sDevMsg  := substr(dbms_utility.format_error_stack ||crlf|| dbms_utility.format_error_backtrace, 1,4000);
    oErrorObj.handleError ( Stb$typedef.cnSeverityCritical, sUserMsg, sImplMsg, sDevMsg );
    Stb$job.markDBJobAsBroken(cnJobId, sIp, sPort, oErrorObj.sErrorCode, oErrorObj.sErrorMessage, oErrorObj.sSeverityCode);
    return oErrorObj;
end callWordmodule;

--*******************************************************************************************************************
function concatenatePdfFiles( cnJobid in     number,
                              cxXml   in out xmltype)
  return STB$OERROR$RECORD
is
  exConcatenationFailed exception;
  sCurrentName          constant varchar2(100) := $$PLSQL_UNIT||'.concatenatePdfFiles (JobID='||cnJobId||')';
  sUserMsg              varchar2(4000);
  sDevMsg               varchar2(4000);
  sImplMsg              varchar2(4000);
  oErrorObj             STB$OERROR$RECORD :=STB$OERROR$RECORD();
  sErrorMessage         varchar2(4000);
  cmd                   varchar2(4000);
  blResult              blob;
  olFileList            ISR$FILEBOOKMARK$LIST;
begin
  isr$trace.stat('begin', 'Function '||sCurrentName||' started.', sCurrentName);

  if NVL(STB$JOB.getJobParameter('CURRENT_ACTION', cnJobId), 'NOT_DETERMINED') = 'CAN_MERGE_COMMON' then

    olFileList := ISR$FILEBOOKMARK$LIST();
    for rGetAppendPDFs in ( select 0, d2.docid, d2.filename, d2.doc_definition
                              from STB$DOCTRAIL d2, STB$DOCTRAIL d1
                             where d1.nodeid = d2.nodeid
                               and d1.nodetype = d2.nodetype
                               and d2.doc_definition = 'document_pdf'
                               and d1.docid = STB$JOB.getJobParameter ('DOCID', cnJobId)
                            union
                            select TO_NUMBER (position.parametervalue), d2.docid, d2.filename, d2.doc_definition
                              from STB$DOCTRAIL d2
                                 , STB$DOCTRAIL d1
                                 , isr$DOC$PROPERTY position
                                 , isr$DOC$PROPERTY headline
                                 , isr$DOC$PROPERTY headlinepos
                             where d1.nodeid = d2.nodeid
                               and d1.nodetype = d2.nodetype
                               and d2.doc_definition = 'pdf_appendix'
                               and d1.docid = STB$JOB.getJobParameter ('DOCID', cnJobId)
                               and d2.docid = headline.docid(+)
                               and headline.parametername(+) = 'HEADLINE_COMMON'
                               and d2.docid = headlinepos.docid(+)
                               and headlinepos.parametername(+) = 'HEADLINEPOS_COMMON'
                               and d2.docid = position.docid(+)
                               and position.parametername(+) = 'POSITION_COMMON'
                            order by 1 ) loop
      olFileList.extend();
      olFileList(olFileList.count) := isr$FILEBOOKMARK$REC(rGetAppendPDFs.filename, null, EMPTY_BLOB());
      select content into olFileList(olFileList.count).BLFILE from STB$DOCTRAIL where DOCID = rGetAppendPDFs.docid;
      if rGetAppendPDFs.doc_definition != 'document_pdf' then
        oErrorObj := Stb$docks.copyDocToJob(rGetAppendPDFs.docid,cnJobId);
     end if;
    end loop;

    if STB$UTIL.getSystemParameter('JAVA_INSIDE_DB') = 'F' and NVL (isr$process.checkProcessServer, 'F') = 'T' then

      for rGetFile in (select entryid, filename, binaryfile
                         from stb$jobtrail
                        where jobid = cnJobid
                          and documenttype = 'TARGET'
                          and doc_definition like 'pdf%') loop
        isr$PROCESS.saveBlob(rGetFile.binaryfile, isr$PROCESS.getUserHome, isr$PROCESS.sSessionPath||ISR$PROCESS.getFileSeparator||rGetFile.filename);
        delete from stb$jobtrail where entryid = rGetFile.entryid and doc_definition = 'pdf_appendix';
      end loop;
      isr$PROCESS.saveBlob(STB$UTIL.ClobToBlob(ISR$XML.XMLToClob(cxXml)), isr$PROCESS.getUserHome, isr$PROCESS.sSessionPath||ISR$PROCESS.getFileSeparator||'config.xml');

      if isr$PROCESS.getFileSeparator = '/' then
        -- UNIX
        cmd := isr$PROCESS.getJavaExe||' -jar '||ISR$PROCESS.getUserHome||'lib'||ISR$PROCESS.getFileSeparator||'asposePdf.jar'||' '||'path="'||ISR$PROCESS.getUserHome||ISR$PROCESS.sSessionPath||'" outputfile="concatenate.pdf" configfile="config.xml"';
      else
        -- WINDOWS
        cmd := '"'||ISR$PROCESS.getJavaExe||'" -jar "'||ISR$PROCESS.getUserHome||'lib'||ISR$PROCESS.getFileSeparator||'asposePdf.jar"'||' '||'path="\"'||ISR$PROCESS.getUserHome||ISR$PROCESS.sSessionPath||'\"" outputfile="concatenate.pdf" configfile="config.xml"';
     end if;
      isr$trace.debug('cmd','cmd: '||cmd, sCurrentName);
      sErrorMessage := SUBSTR(ISR$PROCESS.run_cmd(cmd),0,4000);
      if TRIM(sErrorMessage) is null then
        blResult := isr$PROCESS.loadBlob(ISR$PROCESS.getUserHome||ISR$PROCESS.sSessionPath||ISR$PROCESS.getFileSeparator||'concatenate.pdf');
      else
        sImplMsg := 'sErrorMessage: '||sErrorMessage;
        raise exConcatenationFailed;
     end if;

    else

      blResult := STB$JAVA.concatenatePdfs(olFileList, isr$XML.XMLToClob(cxXml));

   end if;

    update stb$jobtrail
       set binaryfile = blResult
         , docsize = dbms_lob.getLength(blResult)
         , reference = STB$JOB.getJobParameter('REFERENCE', cnJobId)
     where jobid = cnJobid
       and documenttype = 'TARGET'
       and doc_definition = 'pdf';

    commit;

 end if;

  isr$trace.stat('end', 'Function '||sCurrentName||' finished',sCurrentName);
  return oErrorObj;

exception
  when exConcatenationFailed then
    sUserMsg := utd$msglib.getmsg ('exConcatenationFailedText', nCurrentLanguage);
    sImplMsg := sCurrentName || rtrim(': '||sImplMsg,': ');
    sDevMsg  := substr(dbms_utility.format_error_stack ||crlf|| dbms_utility.format_error_backtrace, 1,4000);
    oErrorObj.handleError ( Stb$typedef.cnSeverityMessage, sUserMsg, sImplMsg, sDevMsg );
    Stb$job.markDBJobAsBroken(cnJobId, sIp, sPort, oErrorObj.sErrorCode, oErrorObj.sErrorMessage, oErrorObj.sSeverityCode);
    Stb$job.markDBJobAsBroken(cnJobId, sIp, sPort, oErrorObj.sErrorCode, oErrorObj.sErrorMessage, oErrorObj.sSeverityCode);
    return oErrorObj;
  when others then
    rollback;
    sUserMsg := utd$msglib.getmsg ('exUnexpectedError', nCurrentLanguage,  csP1 => sqlerrm(sqlcode));
    sImplMsg := sCurrentName || rtrim(': '||sImplMsg,': ');
    sDevMsg  := substr(dbms_utility.format_error_stack ||crlf|| dbms_utility.format_error_backtrace, 1,4000);
    oErrorObj.handleError ( Stb$typedef.cnSeverityCritical, sUserMsg, sImplMsg, sDevMsg );
    Stb$job.markDBJobAsBroken(cnJobId, sIp, sPort, oErrorObj.sErrorCode, oErrorObj.sErrorMessage, oErrorObj.sSeverityCode);
    return oErrorObj;
end concatenatePDFFiles;

end Stb$action;