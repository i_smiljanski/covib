CREATE OR REPLACE package body isr$contextmenu$report as

--******************************************************************************
function setMenuAccess (sParameter   in varchar2, sMenuAllowed out varchar2) return stb$oerror$record is
   sCurrentName                  constant varchar2(200) := $$PLSQL_UNIT||'.setMenuAccess('||sParameter||')';
   oErrorObj                     stb$oerror$record := STB$OERROR$RECORD();
begin
  isr$trace.stat('begin','begin', sCurrentName);
  case
  
    when upper (sParameter) = 'TODO' then
      null;
      
  else
    sMenuAllowed := 'T';
  end case;
  
  isr$trace.stat('end', 'sParameter: '||sParameter||'  sMenuAllowed: '||sMenuAllowed  , sCurrentName);
  return oErrorObj;
exception
  when others then
    oErrorObj.handleError ( Stb$typedef.cnSeverityCritical,  utd$msglib.getmsg ('ERROR', stb$security.getCurrentLanguage) || sqlcode, sCurrentName,
       dbms_utility.format_error_stack ||chr(10)||chr(13)|| dbms_utility.format_error_backtrace);
    return oErrorObj;
end setMenuAccess;


--******************************************************************************
function callFunction( oParameter in STB$MENUENTRY$RECORD, olNodeList out STB$TREENODELIST  ) return stb$oerror$record
  is
  sCurrentName                  constant varchar2(200) := $$PLSQL_UNIT||'.callFunction('||oParameter.sToken||')';
  oErrorObj                     stb$oerror$record := STB$OERROR$RECORD();
  exNotExists  exception;
  sExists                       VARCHAR2(1);
begin
  isr$trace.stat('begin','begin', sCurrentName);
  
  olNodeList := STB$TREENODELIST ();
  
  case
  
    when upper (oParameter.sToken) = 'CAN_UPLOAD_COMMON_DOC' then
      olNodeList.EXTEND;
      olNodeList (1) := STB$TREENODE$RECORD(STB$UTIL.getMaskTitle(Upper (oParameter.sToken)));
      oErrorObj := STB$UTIL.getPropertyList(oParameter.sToken, olNodeList (1).tloPropertylist);
      oErrorObj := checkDependenciesOnMask(oParameter, olNodeList (1).tloPropertylist, sExists);
  
    when upper (oParameter.sToken) = 'CAN_ADD_PDF_CONVERSION_PROPERTIES' then
      olNodeList.EXTEND;
      olNodeList (1) := STB$TREENODE$RECORD(STB$UTIL.getMaskTitle(Upper (oParameter.sToken)));
      oErrorObj := STB$UTIL.getPropertyList(oParameter.sToken, olNodeList (1).tloPropertylist);
  
    else
      raise exNotExists;
  end case;
  isr$trace.stat('end','end', sCurrentName);
  return oErrorObj;
EXCEPTION
  WHEN OTHERS THEN
    oErrorObj.handleError ( Stb$typedef.cnSeverityCritical,  utd$msglib.getmsg ('ERROR', stb$security.getCurrentLanguage) || SQLCODE, sCurrentName,
       dbms_utility.format_error_stack ||chr(10)||chr(13)|| dbms_utility.format_error_backtrace);
    RETURN oErrorObj; 
end callFunction;

--******************************************************************************
function startUploadDocumentJob( oParameter  in STB$MENUENTRY$RECORD, olParameter in STB$PROPERTY$LIST ) return stb$oerror$record
  is
  sCurrentName                  constant varchar2(200) := $$PLSQL_UNIT||'.startUploadDocumentJob('||oParameter.sToken||')';
  oErrorObj                     stb$oerror$record := STB$OERROR$RECORD();
  sMsg                          varchar2(2000);
  
  nOutputId                     ISR$REPORT$OUTPUT$TYPE.OUTPUTID%type;
  nActionId                     ISR$ACTION.ACTIONID%type;
  sFunction                     ISR$REPORT$OUTPUT$TYPE.ACTIONFUNCTION%type;
  
  cursor cGetJobIds( cstoken         in isr$action.token%type) is
     select ao.outputid, ao.actionid, o.actionfunction myfunction
     from   isr$action A, isr$report$output$type o, isr$action$output ao
     where  (UPPER (o.token) = UPPER (cstoken) or UPPER (A.token) = UPPER (cstoken))
     and o.reporttypeid is null
     and o.outputid = ao.outputid
     and A.actionid = ao.actionid
     and ao.doc_definition = 'NOT_DETERMINED';
begin
  isr$trace.stat('begin','begin', sCurrentName);
  open cGetJobIds (oParameter.sToken);
  fetch cGetJobIds
  into nOutputId, nActionId, sFunction;
  close cGetJobIds;

  if nActionId is null then
    sMsg := utd$msglib.getmsg ('exNotFindAction', stb$security.getcurrentlanguage, oParameter.sToken);
    raise stb$job.exNotFindAction;
  end if;

  select STB$JOB$SEQ.NEXTVAL
  into   STB$JOB.nJobId
  from   DUAL;

  STB$JOB.setJobParameter ('TOKEN', UPPER (oParameter.sToken), STB$JOB.nJobid);
  STB$JOB.setJobParameter ('OUTPUTID', nOutputId, STB$JOB.nJobid);
  STB$JOB.setJobParameter ('ACTIONID', nActionId, STB$JOB.nJobid);
  STB$JOB.setJobParameter ('NODETYPE', STB$OBJECT.getCurrentNodeType, STB$JOB.nJobid);
  STB$JOB.setJobParameter ('REFERENCE', TO_CHAR (-1), STB$JOB.nJobid);
  STB$JOB.setJobParameter ('FUNCTION', sFunction, STB$JOB.nJobid);
  STB$JOB.setJobParameter ('ACTIONTYPE', 'UPLOAD', STB$JOB.nJobid);

  if STB$OBJECT.getCurrentNodeType like 'REPORT%' then
    STB$JOB.setJobParameter ('REPORTTYEPID', STB$OBJECT.getCurrentReporttypeId, STB$JOB.nJobid);
    STB$JOB.setJobParameter ('SRNUMBER', STB$OBJECT.getCurrentSRNumber, STB$JOB.nJobid);
    STB$JOB.setJobParameter ('REPID', STB$OBJECT.getCurrentRepid, STB$JOB.nJobid);
    STB$JOB.setJobParameter ('NODEID', STB$OBJECT.getCurrentNodeId, STB$JOB.nJobid);
  else
    STB$JOB.setJobParameter ('NODEID', STB$OBJECT.getCurrentNode().sNodeId, STB$JOB.nJobid);
  end if;

  for i in 1 .. olParameter.COUNT () loop
    STB$JOB.setJobParameter (olParameter (i).sParameter, olParameter (i).sValue, STB$JOB.nJobid);
  end loop;

  oErrorObj := STB$JOB.startJob;
  isr$trace.stat('end','end', sCurrentName);
  return oErrorObj;
EXCEPTION
  WHEN OTHERS THEN
    sMsg := utd$msglib.getmsg ('exUnexpectedError', stb$security.getCurrentLanguage, sqlerrm(sqlcode), sMsg);
    oErrorObj.handleError ( Stb$typedef.cnSeverityCritical,  sMsg, sCurrentName,
       dbms_utility.format_error_stack ||chr(10)||chr(13)|| dbms_utility.format_error_backtrace);
    RETURN oErrorObj; 
end startUploadDocumentJob;

--******************************************************************************
function putParameters( oParameter  in STB$MENUENTRY$RECORD, olParameter in STB$PROPERTY$LIST ) return stb$oerror$record
  is
  sCurrentName                  constant varchar2(200) := $$PLSQL_UNIT||'.putParameters('||oParameter.sToken||')';
  oErrorObj                     stb$oerror$record := STB$OERROR$RECORD();
  sToken                        varchar2(255);
begin
  isr$trace.stat('begin','begin', sCurrentName);
  sToken := case when oParameter.sToken = 'CAN_MODIFY_WORD_OUTPUTOPTIONS' then 'CAN_CREATE_WORD_DOCUMENT' else oParameter.sToken end;
  STB$OBJECT.setCurrentToken(sToken);
  case
  
    when upper (oParameter.sToken) = 'CAN_UPLOAD_COMMON_DOC' then
      oErrorObj := startUploadDocumentJob(oParameter, olParameter);
      
    when upper (oParameter.sToken) = 'CAN_ADD_PDF_CONVERSION_PROPERTIES' then
      for i in 1 .. olParameter.COUNT () loop
        merge into isr$doc$property dp 
        using (select STB$OBJECT.getCurrentDocId docid, olParameter (i).sParameter parametername, olParameter (i).sValue parameterValue from dual) j
        on ( dp.docid = j.docid and dp.parametername = j.parametername )
        when matched then update set dp.parameterValue = j.parameterValue
        when not matched then insert (docid, parametername, parametervalue)
           values(j.docid, j.parametername, j.parametervalue);
      end loop;
      commit;
      
    else
      isr$trace.debug ( 'oParameter.sToken', oParameter.sToken, sCurrentName);
  end case;
  --oErrorObj := isr$customer.putParameters(oParameter, olParameter);
  isr$trace.stat('end','end', sCurrentName);
  return oErrorObj;
EXCEPTION
  WHEN OTHERS THEN
    oErrorObj.handleError ( Stb$typedef.cnSeverityCritical,  utd$msglib.getmsg ('ERROR', stb$security.getCurrentLanguage) || SQLCODE, sCurrentName,
       dbms_utility.format_error_stack ||chr(10)||chr(13)|| dbms_utility.format_error_backtrace);
    RETURN oErrorObj; 
end putParameters;


  --**********************************************************************************************************************************
  FUNCTION getLov( csParameter        in  varchar2,
                   sSuchwert      in  varchar2,
                   oSelectionList out ISR$TLRSELECTION$LIST )
    RETURN STB$OERROR$RECORD
    is
    oErrorObj           STB$OERROR$RECORD := STB$OERROR$RECORD();
    sCurrentName        constant varchar2(100) := $$PLSQL_UNIT||'.getLov('||csParameter||')';
    
    nReportType     NUMBER;
    
    CURSOR cGetDocdefinition(nReportType NUMBER) IS
      SELECT VALUE
       , display display
       , info
       , ordernumber
      FROM isr$parameter$values p
     WHERE entity = 'DOCDEFINITION'
       AND NVL(STB$UTIL.getReptypeParameter(stb$object.getCurrentReporttypeId, 'USE_APPENDIX'), 'F') = 'T'
       AND not exists (select 1 from stb$doctrail where nodeid=''||stb$object.GetCurrentRepId||'' and doc_definition=p.value)
  ORDER BY ordernumber;    
                               
BEGIN
  isr$trace.stat('begin', 'begin', sCurrentName);
  
  oSelectionList :=  ISR$TLRSELECTION$LIST();
  case
  
   --***************************************   
    WHEN UPPER(csParameter) = 'DOCDEFINITION' then
    
      IF stb$object.GetCurrentDocid IS NOT null THEN
        FOR rReportType IN (SELECT reporttypeid FROM stb$report r, stb$doctrail d where r.repid = d.nodeid and d.docid = stb$object.GetCurrentDocid) LOOP
          nReportType := rReportType.reporttypeid;
        END LOOP; 
      ELSE
        nReportType := stb$object.getcurrentreporttypeid;
      END IF;
      FOR rGetDocdefinition in cGetDocdefinition(nReportType) LOOP
        oSelectionList.EXTEND;
        oSelectionList(oSelectionList.COUNT()) := ISR$OSELECTION$RECORD(rGetDocdefinition.display, rGetDocdefinition.VALUE, rGetDocdefinition.info, null, oSelectionList.COUNT());
      END LOOP;
    else
      null;
    end case;
    --todo: isr$util
    if sSuchWert is not null and oselectionlist.FIRST is not null then
      isr$trace.info ('sSuchWert', sSuchWert, sCurrentName);
      for i in 1..oSelectionList.COUNT() loop
        if oSelectionList(i).sValue = sSuchWert then
          isr$trace.debug ('oSelectionList('||i||').sSelected', oSelectionList(i).sValue, sCurrentName);
          oSelectionList(i).sSelected := 'T';
        end if;
      end loop;
    end if;
    RETURN oErrorObj;

EXCEPTION
  WHEN OTHERS then    
      oErrorObj.handleError ( Stb$typedef.cnSeverityCritical, utd$msglib.getmsg ('exUnexpectedError', stb$security.getCurrentlanguage,  csP1 => sqlerrm(sqlcode)), 
        sCurrentName, dbms_utility.format_error_stack ||chr(10)|| dbms_utility.format_error_backtrace );
    RETURN oErrorObj;
  end getLov;
  
--*********************************************************************************************************************************
function handleUploadDocumentMask( oParameter    in     STB$MENUENTRY$RECORD,
                                  olParameter   in out STB$PROPERTY$LIST)
return STB$OERROR$RECORD
is
  sCurrentName      constant varchar2(100)  := $$PLSQL_UNIT||'.handleUploadDocumentMask('||oParameter.sToken||')';
  oErrorObj         STB$OERROR$RECORD       := STB$OERROR$RECORD(null,null,Stb$typedef.cnNoError);
 

begin
  isr$trace.stat('begin','begin',sCurrentName); 
  
  IF STB$OBJECT.getCurrentNodeType like 'REPORT%' 
  AND STB$UTIL.getReptypeParameter(STB$OBJECT.getCurrentReporttypeid, 'REQUIRES_VERSIONING') = 'T' 
  AND NVL(STB$UTIL.getReptypeParameter(stb$object.getCurrentReporttypeId, 'USE_APPENDIX'), 'F') = 'T' 
  THEN
    stb$util.setProperty(olParameter, 'APPENDIX', 'SDISPLAYED', 'T');
    --stb$util.setProperty(olParameter, 'APPEND_TO_ORG', 'SDISPLAYED', 'T');
    
    IF stb$util.getProperty(olParameter, 'APPENDIX', 'SVALUE') = 'T' THEN
      stb$util.setProperty(olParameter, 'APPENDIX', 'SDISPLAY', 'T');
      stb$util.setProperty(olParameter, 'DOCDEFINITION', 'SDISPLAYED', 'T');
      stb$util.setProperty(olParameter, 'DOCDEFINITION', 'SVALUE', stb$util.getProperty(olParameter, 'DOCDEFINITION', 'SDISPLAY'));
     -- stb$util.setProperty(olParameter, 'APPEND_TO_ORG', 'SDISPLAYED', 'F');
    ELSE
      stb$util.setProperty(olParameter, 'APPENDIX', 'SDISPLAY', 'F');
      stb$util.setProperty(olParameter, 'DOCDEFINITION', 'SDISPLAYED', 'F');
      stb$util.setProperty(olParameter, 'DOCDEFINITION', 'SVALUE', '');
     -- stb$util.setProperty(olParameter, 'APPEND_TO_ORG', 'SDISPLAYED', 'T');
    END IF;
      
   /* IF stb$util.getProperty(olParameter, 'APPEND_TO_ORG', 'SVALUE') = 'T' THEN
      stb$util.setProperty(olParameter, 'APPEND_TO_ORG', 'SDISPLAY', 'T');
      stb$util.setProperty(olParameter, 'DOCDEFINITION', 'SDISPLAY', 'pdf_appendix');
      stb$util.setProperty(olParameter, 'DOCDEFINITION', 'SVALUE', 'pdf_appendix');
      stb$util.setProperty(olParameter, 'APPENDIX', 'SDISPLAYED', 'F');
    ELSE
      stb$util.setProperty(olParameter, 'APPEND_TO_ORG', 'SDISPLAY', 'F');*/
      IF olParameter(5).sValue = 'pdf_appendix' THEN
        stb$util.setProperty(olParameter, 'DOCDEFINITION', 'SVALUE', null);
        stb$util.setProperty(olParameter, 'DOCDEFINITION', 'SDISPLAY', null);
      END IF;
      stb$util.setProperty(olParameter, 'APPENDIX', 'SDISPLAYED', 'T');
    --END IF;      
    isr$trace.debug('value','olParameter IN OUT',sCurrentName,olParameter);
        
  ELSE
    stb$util.setProperty(olParameter, 'APPENDIX', 'SDISPLAYED', 'F');
    --stb$util.setProperty(olParameter, 'APPEND_TO_ORG', 'SDISPLAYED', 'F');
  END IF;
    
  isr$trace.stat('end','end',sCurrentName);
  return oErrorObj;
exception
  when others then
      oErrorObj.handleError ( Stb$typedef.cnSeverityWarning, utd$msglib.getmsg ('exUnexpectedError', stb$security.getCurrentlanguage,  csP1 => sqlerrm(sqlcode)),
        sCurrentName, dbms_utility.format_error_stack ||chr(10)|| dbms_utility.format_error_backtrace );
    return oErrorObj;
end handleUploadDocumentMask;

--*********************************************************************************************************************************
function checkDependenciesOnMask( oParameter    in     STB$MENUENTRY$RECORD,
                                  olParameter   in out STB$PROPERTY$LIST,
                                  sModifiedFlag OUT VARCHAR2 )
return STB$OERROR$RECORD
is
  sCurrentName      constant varchar2(100)  := $$PLSQL_UNIT||'.checkDependenciesOnMask('||oParameter.sToken||')';
  oErrorObj         STB$OERROR$RECORD       := STB$OERROR$RECORD(null,null,Stb$typedef.cnNoError);
  olParameterOld    STB$PROPERTY$LIST       := STB$PROPERTY$LIST();

begin
  isr$trace.stat('begin','begin',sCurrentName);
  isr$trace.debug('value','oParameter IN',sCurrentName,oParameter);
  isr$trace.debug('value','olParameter IN OUT',sCurrentName,olParameter);

  olParameterOld := olParameter;

  case

    ------------ cun upload common doc ------------------------
    when UPPER(oParameter.sToken) = 'CAN_UPLOAD_COMMON_DOC' then
      
      oErrorObj := handleUploadDocumentMask(oParameter, olParameter);

    else
      -- all other tokens
      null;
  end case;

  -- compare the objects if something changed only then refresh the mask
  SELECT CASE WHEN COUNT ( * ) = 0 THEN 'F' ELSE 'T' END
      INTO sModifiedFlag
      FROM (SELECT XMLTYPE (VALUE (p)).getStringVal ()
              FROM table (olParameter) p
            MINUS
            SELECT XMLTYPE (VALUE (p)).getStringVal ()
              FROM table (olParameterOld) p);    
        
  isr$trace.debug('value','olParameter',sCurrentName,olParameter);
  isr$trace.stat('end','end',sCurrentName);
  return oErrorObj;
exception
  when others then
      oErrorObj.handleError ( Stb$typedef.cnSeverityWarning, utd$msglib.getmsg ('exUnexpectedError', stb$security.getCurrentlanguage,  csP1 => sqlerrm(sqlcode)),
        sCurrentName, dbms_utility.format_error_stack ||chr(10)|| dbms_utility.format_error_backtrace );
    return oErrorObj;
end checkDependenciesOnMask;

end isr$contextmenu$report;