CREATE OR REPLACE PACKAGE BODY ISR$Entity$Conditions AS

  nCurrentLanguage    number        := stb$security.getcurrentlanguage;    


--***************************************************************************************************
procedure AddEntityCondition(csEntity IN VARCHAR2, csAttribute in varchar2, clConditionList in isr$varchar$varray, csRelType in varchar2 default 'FK') IS
  oErrorObj      STB$OERROR$RECORD := STB$OERROR$RECORD();
  sCurrentName   constant varchar2(4000) := $$PLSQL_UNIT||'.AddEntityCondition('||csEntity||')';
  sMsg           VARCHAR2(2000);
  lCondList      isr$varchar$varray := isr$varchar$varray();
  cursor cCondForEntity is
    select conditionlist
    from tmp$entity$condition
    where entityname = upper(csEntity)
      and attrname = upper(csAttribute)
      and relationtype  = upper(csRelType);
  lCondWorkTab    isr$varchar$list;
  lCondWorkHere   isr$varchar$list; 
begin
  ISR$TRACE.stat('begin', 'begin', sCurrentName);
  -- first check if conditions are already stored for this entity
  open cCondForEntity;
  fetch cCondForEntity into lCondList; 
  close cCondForEntity;  
  
  if lCondList.count > 0 then
    -- we found an entry, so we have to merge the data
    -- as MULTISET UNION DISTINCT does not work for varrays, we have to cast the collections first to nested tables
    select cast(lCondList as isr$varchar$list) into lCondWorkTab from dual;
    select cast(clConditionList as isr$varchar$list) into lCondWorkHere from dual;
    -- then merge it
    lCondWorkHere := lCondWorkHere MULTISET UNION DISTINCT lCondWorkTab;    
    isr$trace.debug(csEntity||' we got entries in lCondWorkHere', lCondWorkHere.count, sCurrentName);
    
    -- now the data is merged. Cast it back and save it in the found db record 
    select cast(lCondWorkHere as isr$varchar$varray) into lCondList from dual;
    update tmp$entity$condition set conditionlist = lCondList
    where entityname = upper(csEntity)
      and attrname = upper(csAttribute)
      and relationtype  = upper(csRelType);
    isr$trace.debug(csEntity||' updated conditions entry', sql%rowcount||' rows updated', sCurrentName);
  else
    insert into tmp$entity$condition(ENTITYNAME, ATTRNAME, CONDITIONLIST, RELATIONTYPE)
    values(upper(csEntity), upper(csAttribute), clConditionList, upper(csRelType));
    isr$trace.debug(csEntity||' conditions inserted for parameters:','csEntity:'||csEntity||', csAttribute:'||csAttribute, sCurrentName);
  end if;
  
  ISR$TRACE.stat('end', 'end', sCurrentName);
EXCEPTION  
  WHEN others THEN
    sMsg :=  utd$msglib.getmsg ('exUnexpectedError', nCurrentLanguage, sqlerrm(sqlcode), sMsg);
    oErrorObj.handleError ( Stb$typedef.cnSeverityCritical, sMsg, sCurrentName, 
                            dbms_utility.format_error_stack || CHR(10) || dbms_utility.format_error_backtrace );  
    raise;
END AddEntityCondition;

--***************************************************************************************************
procedure AddEntityCondition(csEntity IN VARCHAR2, csAttribute in varchar2, csConditionList in varchar2, csRelType in varchar2 default 'FK') IS

  oErrorObj      STB$OERROR$RECORD := STB$OERROR$RECORD();
  sCurrentName   constant varchar2(4000) := $$PLSQL_UNIT||'.AddEntityCondition('||csEntity||')';
  sMsg           VARCHAR2(2000);
  lConditions    isr$varchar$varray := isr$varchar$varray();

begin
  ISR$TRACE.stat('begin', 'begin', sCurrentName);
  
  if trim(csEntity) is not null and trim(csAttribute) is not null and trim(csConditionList) is not null then
    -- get the conditions from the string
    with condlist as
      (select csConditionList cond FROM dual)
       select trim(regexp_substr(cond, '[^,]+', 1, LEVEL)) condval
       bulk collect into lConditions
       from condlist
       connect by regexp_substr(cond , '[^,]+', 1, LEVEL) is not null;
    isr$trace.debug(csEntity||' conditions count', lConditions.count, sCurrentName);
    -- add them to the condition table by calling the oveload func
    AddEntityCondition(csEntity, csAttribute, lConditions, csRelType);
    isr$trace.debug(csEntity||' condition inserted for parameters:','csEntity:'||csEntity||', csAttribute:'||csAttribute||', csConditionList:'||csConditionList, sCurrentName);
  else
    isr$trace.warn(csEntity||' Conditions cannot be considered because at least one of the parameters is null', 'csEntity:'||csEntity||', csAttribute:'||csAttribute||', csConditionList:'||csConditionList, sCurrentName);
  end if;
  
  ISR$TRACE.stat('end', 'end', sCurrentName);

EXCEPTION  
  WHEN others THEN
    sMsg :=  utd$msglib.getmsg ('exUnexpectedError', nCurrentLanguage, sqlerrm(sqlcode), sMsg);
    oErrorObj.handleError ( Stb$typedef.cnSeverityCritical, sMsg, sCurrentName, 
                            dbms_utility.format_error_stack || CHR(10) || dbms_utility.format_error_backtrace );  
    raise;
END AddEntityCondition;

--***************************************************************************************************
procedure RemoveEntityConditions(csEntity IN VARCHAR2) IS

  oErrorObj      STB$OERROR$RECORD := STB$OERROR$RECORD();
  sCurrentName   constant varchar2(4000) := $$PLSQL_UNIT||'.RemoveEntityConditions('||csEntity||')';
  sMsg           VARCHAR2(2000);

begin
  ISR$TRACE.stat('begin', 'begin', sCurrentName);
  
  if trim(csEntity) is not null  then
    delete from tmp$entity$condition where ENTITYNAME=trim(upper(csEntity));
    isr$trace.debug(csEntity||'conditions removed',sql%rowcount||' conditions deleted' , sCurrentName);
  else
    isr$trace.warn('entity not specified', 'entity to remove its condition was not specified', sCurrentName);
  end if;
  
  
  ISR$TRACE.stat('end', 'end', sCurrentName);

EXCEPTION  
  WHEN others THEN
    sMsg :=  utd$msglib.getmsg ('exUnexpectedError', nCurrentLanguage, sqlerrm(sqlcode), sMsg);
    oErrorObj.handleError ( Stb$typedef.cnSeverityCritical, sMsg, sCurrentName, 
                            dbms_utility.format_error_stack || CHR(10) || dbms_utility.format_error_backtrace );  
    raise;
END RemoveEntityConditions;

procedure RemoveEntityConditions is

  sCurrentName   constant varchar2(4000) := $$PLSQL_UNIT||'.RemoveEntityConditions';

begin

  ISR$TRACE.stat('begin', 'begin', sCurrentName);

  delete from tmp$entity$condition; 
  
  ISR$TRACE.stat('end', 'end', sCurrentName);

end RemoveEntityConditions;


end ISR$Entity$Conditions;