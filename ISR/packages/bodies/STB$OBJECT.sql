CREATE OR REPLACE PACKAGE BODY STB$OBJECT IS
  -- Variables
  nCurrentRepTypeID             NUMBER;                                                                                                                                                  -- Report type
  nCurrentTemplate              NUMBER;
  sCurrentRepTypePackage        VARCHAR2 (500);
  sCurrentRepTypeCalcPackage    VARCHAR2 (500);                                                                                                                              -- Report type package
  nCurrentRepID                 NUMBER;                                                                                                                                                    -- Report Id
  nCurrentRepStatus             NUMBER;                                                                                                                                                -- Report status
  nCurrentRepCondition          NUMBER;
  nCurrentRepLangID             NUMBER;                                                                                                                                                     -- Report condition
  nCurrentDocId                 NUMBER;
  nCurrentMasktype              NUMBER;                                                                                                                                               -- Document ID
  sCurrentDocStatus             VARCHAR2 (1);                                                                                             -- Document Status  (CheckedOut ='T' , CheckedIn ='F')
  sCurrentSrNumber              STB$REPORT.SRNUMBER%TYPE;
  sCurrentTitle                 STB$REPORT.TITLE%TYPE;                                                                                                                                  -- Report title
  sCurrentVersion               STB$REPORT.VERSION%TYPE;                                                                                                                              -- report version
  sCurrentToken                 STB$CONTEXTMENU.TOKEN%TYPE;
  oCurrentNode                  STB$TREENODE$RECORD := STB$TREENODE$RECORD();
  olTreeNodeReloadFrom          STB$TREENODELIST := STB$TREENODELIST();
  sReloadToken                  VARCHAR2 (100);                                                                                                   -- token to identify where the current node is placed
  nNumberOfMasters              NUMBER;  -- number of masters retrieved in initmasterlist in isr$reporttype$package
  olMessageStack                ISR$MESSAGE$LIST := ISR$MESSAGE$LIST();
  oCurrentErrorRec              STB$OERROR$RECORD := STB$OERROR$RECORD(tloMessageList => ISR$MESSAGE$LIST());


  --*************************************************************************************************
  FUNCTION calculateHash (olPropertyList IN STB$PROPERTY$LIST) RETURN VARCHAR2 IS
    sCurrentName   constant varchar2(4000) := $$PLSQL_UNIT||'.calculateHash(olPropertyList)';
    sTemp                         VARCHAR2 (32767);
    sHash                         VARCHAR2 (1024);
  BEGIN
    --isr$trace.stat('begin', 'begin', sCurrentName);
    --FOR nElements IN 1 .. olPropertyList.COUNT () LOOP
    --  sTemp := sTemp || XMLTYPE(olPropertyList (nElements)).getStringVal;
    --END LOOP;
    sTemp := xmltype(ANYDATA.convertCollection(olPropertyList)).GetClobVal;
    sHash := STB$JAVA.md5_string (sTemp);
    --isr$trace.stat('end', 'end', sCurrentName);
    RETURN sHash;
  END;

  --*************************************************************************************************
  FUNCTION calculateHash (olValueList IN ISR$VALUE$LIST) RETURN VARCHAR2 IS
    sCurrentName   constant varchar2(4000) := $$PLSQL_UNIT||'.calculateHash(olValueList)';
    sTemp                         VARCHAR2 (32767);
    sHash                         VARCHAR2 (1024);
  BEGIN
    --isr$trace.stat('begin', 'begin', sCurrentName);
    --FOR nElements IN 1 .. olValueList.COUNT () LOOP
    --  sTemp := sTemp || XMLTYPE(olValueList (nElements)).getStringVal;
    --END LOOP;
    sTemp := xmltype(ANYDATA.convertCollection(olValueList)).GetClobVal;
    sHash := STB$JAVA.md5_string (sTemp);
    --isr$trace.stat('end', 'end', sCurrentName);
    RETURN sHash;
  END;

  --***********************************************************************************************
  PROCEDURE setdependingValues (cnRepId IN NUMBER) IS
    sCurrentName   constant varchar2(4000) := $$PLSQL_UNIT||'.setdependingValues('||cnRepId||')';
    CURSOR cGetValues IS
      SELECT r.condition
           , r.status
           , r.reporttypeid
           , STB$UTIL.getReptypeParameter (r.reporttypeid, 'REP_TYPE_PACKAGE')
           , r.VERSION
           , r.title
           , r.srnumber
           , r.template
        FROM stb$report r
       WHERE r.repid = cnRepId;
  BEGIN
    isr$trace.stat('begin', 'begin', sCurrentName);
    SetCurrentReportLanguage(STB$DOCKS.getReportLanguage(cnRepId));
    OPEN cGetValues;
    FETCH cGetValues
    INTO  nCurrentRepCondition, nCurrentRepStatus, nCurrentRepTypeID, sCurrentRepTypePackage, sCurrentVersion, sCurrentTitle, sCurrentSrnumber, nCurrentTemplate;
    CLOSE cGetValues;
    isr$trace.stat('end', 'end', sCurrentName);
  END setdependingValues;

  --******************************************************************************************************
  PROCEDURE destroyOldValues IS
  BEGIN
    nCurrentRepTypeID := NULL;
    nCurrentTemplate := NULL;
    sCurrentRepTypePackage := NULL;
    sCurrentRepTypeCalcPackage := NULL;
    nCurrentRepID := NULL;
    nCurrentRepStatus := NULL;
    nCurrentRepCondition := NULL;
    nCurrentDocId := NULL;
    sCurrentDocStatus := NULL;
    sCurrentSrNumber := NULL;
    sCurrentTitle := NULL;
    sCurrentVersion := NULL;
    sCurrentToken := NULL;
    olTreeNodeReloadFrom := STB$TREENODELIST();
  END destroyOldValues;

  --******************************************************************************************************
  FUNCTION GetCurrentToken
    RETURN VARCHAR2 IS
  BEGIN
    RETURN sCurrentToken;
  END GetCurrentToken;

  --******************************************************************************************************
  FUNCTION GetCurrentDocStatus
    RETURN VARCHAR2 IS
  BEGIN
    RETURN sCurrentDocStatus;
  END GetCurrentDocStatus;

  --****************************************************************************************************
  FUNCTION GetCurrentSrNumber
    RETURN VARCHAR2 IS
  BEGIN
    RETURN sCurrentSrNumber;
  END GetCurrentSrNumber;

  --****************************************************************************************************
  FUNCTION GetCurrentTitle
    RETURN VARCHAR2 IS
  BEGIN
    RETURN sCurrentTitle;
  END GetCurrentTitle;

  --****************************************************************************************************
  FUNCTION GetCurrentVersion
    RETURN VARCHAR2 IS
  BEGIN
    RETURN sCurrentVersion;
  END GetCurrentVersion;

  --******************************************************************************************************
  FUNCTION GetCurrentTemplate
    RETURN NUMBER IS
  BEGIN
    RETURN nCurrentTemplate;
  END GetCurrentTemplate;

  --******************************************************************************************************
  FUNCTION GetCurrentRepId
    RETURN NUMBER IS
  BEGIN
    RETURN nCurrentRepId;
  END GetCurrentRepId;

  --******************************************************************************************************
  FUNCTION GetCurrentReportLanguage
    RETURN  NUMBER IS
  BEGIN
    IF nCurrentRepLangId IS NOT NULL THEN
        RETURN nCurrentRepLangId;
    ELSE
        RETURN stb$security.getCurrentLanguage;
    END IF;
  END GetCurrentReportLanguage;

  --******************************************************************************************************
  FUNCTION GetCurrentRepStatus
    RETURN NUMBER IS
  BEGIN
    RETURN nCurrentRepStatus;
  END GetCurrentRepStatus;

  --******************************************************************************************************
  FUNCTION GetCurrentRepCondition
    RETURN NUMBER IS
  BEGIN
    RETURN nCurrentRepCondition;
  END GetCurrentRepCondition;

  --*******************************************************************************************************
  FUNCTION GetCurrentReportTypeId
    RETURN NUMBER IS
  BEGIN
    RETURN nCurrentRepTypeId;
  END GetCurrentReportTypeId;

  --*******************************************************************************************************
  FUNCTION GetCurrentDocId
    RETURN NUMBER IS
  BEGIN
    RETURN nCurrentDocId;
  END GetCurrentDocId;

 --******************************************************************************************************
  FUNCTION GetCurrentMaskType
    RETURN NUMBER IS
  BEGIN
    RETURN nCurrentMasktype;
  END GetCurrentMaskType;

  --******************************************************************************************************
  PROCEDURE SetCurrentToken (csToken IN VARCHAR2) IS
  BEGIN
    sCurrentToken := csToken;
  END SetCurrentToken;

  --******************************************************************************************************
  PROCEDURE SetCurrentRepTypePackage (csPackage IN VARCHAR2) IS
  BEGIN
    sCurrentRepTypePackage := csPackage;
  END SetCurrentRepTypePackage;

  --******************************************************************************************************
  PROCEDURE SetCurrentRepTypeCalcPackage (csPackage IN VARCHAR2) IS
  BEGIN
    sCurrentRepTypeCalcPackage := csPackage;
  END SetCurrentRepTypeCalcPackage;

  --******************************************************************************************************
  PROCEDURE SetCurrentSrNumber (csSrNumber IN VARCHAR2) IS
  BEGIN
    sCurrentSrNumber := csSrNumber;
  END SetCurrentSrNumber;

  --******************************************************************************************************
  PROCEDURE SetCurrentTitle (csTitle IN VARCHAR2) IS
  BEGIN
    sCurrentTitle := csTitle;
  END SetCurrentTitle;

  --********************************************************************************************************
  PROCEDURE SetCurrentTemplate (cnTemplate IN NUMBER) IS
    sCurrentName   constant varchar2(4000) := $$PLSQL_UNIT||'.SetCurrentTemplate('||cnTemplate||')';
    cursor cGetReporttypeId IS
      SELECT reporttypeid
        FROM isr$report$output$type rot, isr$output$file iof
       WHERE iof.outputid = rot.outputid
         AND iof.fileid = nCurrentTemplate
         AND rot.visible = 'T';
  BEGIN
    isr$trace.stat('begin', 'begin', sCurrentName);
    nCurrentTemplate := cnTemplate;
    OPEN cGetReporttypeId;
    FETCH cGetReporttypeId INTO nCurrentRepTypeId;
    CLOSE cGetReporttypeId;
    isr$trace.stat('end', 'end', sCurrentName);
  END SetCurrentTemplate;

  --********************************************************************************************************
  PROCEDURE SetCurrentRepId (cnRepId IN NUMBER) IS
  BEGIN
    isr$trace.debug ('begin', 'cnRepId=' || cnRepId, $$PLSQL_UNIT||'.SetCurrentRepId');
    nCurrentRepId := cnRepId;
    -- set the depending values
    setdependingValues (nCurrentRepid);
  END SetCurrentRepId;

   --********************************************************************************************************
  PROCEDURE SetCurrentReportLanguage(cnLangId in NUMBER) IS
  BEGIN
    nCurrentRepLangId := cnLangId;
  END SetCurrentReportLanguage;

  --*******************************************************************************************************
  PROCEDURE SetCurrentReportTypeId (cnReportTypeId IN NUMBER) IS
  BEGIN
    nCurrentRepTypeId := cnReportTypeId;
  END SetCurrentReportTypeId;

  --**************************************************************************************************************************************
  FUNCTION initRepType (nRepTypeId IN NUMBER, nRepId IN NUMBER := NULL)
    RETURN STB$OERROR$RECORD IS
    sCurrentName      constant varchar2(100) := $$PLSQL_UNIT||'.initRepType('||nRepTypeId||')';
    sUserMsg          varchar2(4000);
    oErrorObj         STB$OERROR$RECORD := STB$OERROR$RECORD();
  BEGIN
    destroyOldValues;
    STB$OBJECT.SetCurrentReportTypeId (nRepTypeId);
    STB$OBJECT.SetCurrentRepId (nRepId);

    IF STB$OBJECT.GetCurrentReportTypeId IS NOT NULL THEN
      sCurrentRepTypePackage := STB$UTIL.getRepTypeParameter(STB$OBJECT.GetCurrentReportTypeId, 'REP_TYPE_PACKAGE');
    END IF;

    RETURN oErrorObj;
  EXCEPTION
    WHEN OTHERS THEN
      sUserMsg := utd$msglib.getmsg( 'exUnexpectedError', stb$security.getCurrentLanguage,  csP1 => sqlerrm(sqlcode) );
      oErrorObj.handleError( Stb$typedef.cnSeverityCritical, sUserMsg, sCurrentName,
                             dbms_utility.format_error_stack||stb$typedef.crlf||dbms_utility.format_error_backtrace );
      return oErrorObj;
  END initRepType;

  --**************************************************************************************************************************************
  PROCEDURE SetCurrentDocId (cnDocId IN NUMBER) IS
    oErrorObj                     STB$OERROR$RECORD := STB$OERROR$RECORD();
    sUserMsg                      varchar2(4000);
     sCurrentName        constant varchar2(100) := $$PLSQL_UNIT||'.SetCurrentDocId('||cnDocId||')';

    CURSOR cGetDocStatus IS
      SELECT CHECKEDOUT
      FROM   stb$doctrail
      WHERE  docid = cnDocId;
  BEGIN
    nCurrentDocId := cnDocId;
    OPEN cGetDocStatus;
    FETCH cGetDocStatus
    INTO  sCurrentDocStatus;
    CLOSE cGetDocStatus;
  EXCEPTION
    WHEN OTHERS THEN
      sUserMsg := utd$msglib.getmsg( 'exUnexpectedError', stb$security.getCurrentLanguage,  csP1 => sqlerrm(sqlcode) );
      oErrorObj.handleError( Stb$typedef.cnSeverityCritical, sUserMsg, sCurrentName,
                             dbms_utility.format_error_stack||stb$typedef.crlf||dbms_utility.format_error_backtrace );
  END SetCurrentDocId;

  --*************************************************************************************************
  PROCEDURE setCurrentNode (oCurNode IN STB$TREENODE$RECORD) IS
     sCurrentName    constant varchar2(100) := $$PLSQL_UNIT||'.setCurrentNode('||oCurNode.sNodeType||')';
  BEGIN
    isr$trace.stat('begin', 'begin', sCurrentName);
    isr$trace.debug('parameter', 'oCurNode, see column LOGCLOB', sCurrentName, oCurNode );
    oCurrentNode := oCurNode;
    isr$trace.debug('status', 'oCurrentNode, see column LOGCLOB', sCurrentName, oCurrentNode );
    isr$trace.stat('end', 'end', sCurrentName);
  END setCurrentNode;

  --*************************************************************************************************
  PROCEDURE SetCurrentMasktype(cnMasktype in NUMBER)IS
  BEGIN
    nCurrentMasktype := cnMasktype;
  END SetCurrentMasktype;

  --*************************************************************************************************
  PROCEDURE setDirectory (oCurNode IN OUT STB$TREENODE$RECORD, sParameterName IN VARCHAR2 DEFAULT NULL, sParameterValue IN VARCHAR2 DEFAULT NULL) IS
    sCurrentName   constant varchar2(4000) := $$PLSQL_UNIT||'.setDirectory';
    tloValueList ISR$VALUE$LIST;
  BEGIN
    --isr$trace.stat('begin', 'begin', sCurrentName);
    tloValueList := oCurNode.tloValueList;
    IF tloValueList IS NULL THEN
      tloValueList := ISR$VALUE$LIST();
    END IF;
    tloValueList.EXTEND;
    tloValueList(tloValueList.COUNT()) := ISR$VALUE$RECORD(case when sParameterName IS NOT NULL then sParameterName else oCurNode.sNodeType end
                                                          ,case when sParameterName IS NOT NULL then sParameterValue else oCurNode.sNodeId end
                                                          ,oCurNode.sNodeType
                                                          ,oCurNode.sNodeid
                                                          ,oCurNode.sPackageType);
    oCurNode.sNodeId := STB$OBJECT.calculateHash (tloValueList)||'#@#'||STB$SECURITY.getCurrentUser;
    --oCurNode.clPropertyList := XMLTYPE(sys.anydata.convertCollection(tloValueList)).getClobVal;
    oCurNode.tloValueList := tloValueList;
    ISR$TREE$PACKAGE.saveTreeNode(oCurNode);
    --isr$trace.debug('status', 'oCurNode', sCurrentName, XMLTYPE(oCurNode).getClobVal());
    --isr$trace.stat('end', 'end', sCurrentName);
  END setDirectory;

  --*************************************************************************************************
  PROCEDURE setNode (oCurNode IN OUT STB$TREENODE$RECORD, nDelta IN NUMBER DEFAULT 0) IS
    sCurrentName      CONSTANT VARCHAR2(100) := $$PLSQL_UNIT||'.setNode';
  BEGIN
    isr$trace.stat('begin', 'begin', sCurrentName);
    IF oCurNode IS NULL THEN
      oCurNode := STB$OBJECT.getCurrentNode;
    END IF;
    isr$trace.debug_all ('oCurNode','oCurNode in logclob', sCurrentName, XMLTYPE(oCurNode).getClobVal());
    IF oCurNode.tloValueList IS NULL or NOT(oCurNode.tloValueList.EXISTS(ABS(nDelta)+1)) THEN
      RETURN;
    END IF;
    IF abs(nDelta) != 0 THEN
      oCurNode.tloValueList.TRIM(abs(nDelta));
      isr$trace.debug ('deleted',  abs(nDelta), sCurrentName);
    END IF;
    oCurNode.sNodeId := STB$OBJECT.calculateHash (oCurNode.tloValueList)||'#@#'||STB$SECURITY.getCurrentUser;
    oCurNode.sNodeType := oCurNode.tloValueList(oCurNode.tloValueList.count()).sNodeType;
    oCurNode.sPackageType := oCurNode.tloValueList(oCurNode.tloValueList.count()).sNodePackage;
    ISR$TREE$PACKAGE.saveTreeNode(oCurNode);
    isr$trace.debug_all ('oCurNode',  'oCurNode in logclob', sCurrentName, XMLTYPE(oCurNode).getClobVal());
    isr$trace.stat('end', 'end', sCurrentName);
  END setNode;

  --*************************************************************************************************
  FUNCTION getCurrentNode
    RETURN STB$TREENODE$RECORD IS
  BEGIN
  for rec in (select 1 from dual where oCurrentNode is null) loop
    isr$trace.warn('warning','current node is null',$$PLSQL_UNIT||'.getCurrentNode');
  end loop;

    RETURN oCurrentNode;
  END getCurrentNode;

  --*************************************************************************************************
  FUNCTION getCurrentNodePackage(nDelta IN NUMBER DEFAULT 0)
    RETURN VARCHAR2
  IS
    sCurrentName      constant varchar2(100) := $$PLSQL_UNIT||'.getCurrentNodePackage('||nDelta||')';
    sReturnHandledBy  stb$contextmenu.handledby%type;
  BEGIN
    isr$trace.stat('begin', oCurrentNode.sNodeType, sCurrentName);
    IF oCurrentNode.tloValueList IS NULL
    OR NOT(oCurrentNode.tloValueList.EXISTS(oCurrentNode.tloValueList.COUNT() + nDelta)) THEN
      sReturnHandledBy := oCurrentNode.sPackageType;
    ELSE
      sReturnHandledBy := oCurrentNode.tloValueList(oCurrentNode.tloValueList.COUNT() + nDelta).sNodePackage;
    END IF;
    isr$trace.stat('end',sReturnHandledBy, sCurrentName);
    return sReturnHandledBy;
  END getCurrentNodePackage;

--*************************************************************************************************
function getCurrentNodePackage( csNodeType in varchar2,
                                csToken    in varchar2  )
  return varchar2 result_cache
is
  sCurrentName      constant varchar2(200) := $$PLSQL_UNIT||'.getCurrentNodePackage('||csNodeType||','||csToken||')';
  sReturnHandledBy  stb$contextmenu.handledby%type;
  nCnt              number := 0;

  cursor cGetTokenPackCnt ( cpsToken varchar2 ) is
    select count(distinct handledby)
      from stb$contextmenu
     where token = cpsToken
     group by token
     order by 1;

  cursor curHandleBy ( cpsToken varchar2, cpsNodeType varchar2 ) is
    select distinct handledby
      from stb$contextmenu
     where token = cpsToken
       and (nodetype = cpsNodetype or cpsNodetype is null);

begin
  isr$trace.stat('begin', 'begin', sCurrentName);
  open cGetTokenPackCnt(csToken);
  fetch cGetTokenPackCnt into nCnt;
  close cGetTokenPackCnt;

  if nCnt = 1 then
    open curHandleBy(csToken, null);
    fetch curHandleBy into sReturnHandledBy;
    close curHandleBy;
  else
    open curHandleBy(csToken, csNodetype);
    fetch curHandleBy into sReturnHandledBy;
    close curHandleBy;
  end if;
  isr$trace.stat('end', sReturnHandledBy, sCurrentName);
  return sReturnHandledBy;
exception
  when others then
    isr$trace.warn('warning','no data found, NULL returned',sCurrentName);
    return null;
end getCurrentNodePackage;

  --*************************************************************************************************
  FUNCTION getCurrentNodeType(nDelta IN NUMBER DEFAULT 0)
    RETURN VARCHAR2
  IS
    sCurrentName   constant varchar2(4000) := $$PLSQL_UNIT||'.getCurrentNodeType';
  BEGIN
    --[ISRC-672] do not use logging in this function, because it is called from ISR$TRACE
    IF oCurrentNode.tloValueList IS NULL
    OR NOT(oCurrentNode.tloValueList.EXISTS(oCurrentNode.tloValueList.COUNT() + nDelta)) THEN
      RETURN oCurrentNode.sNodeType;
    ELSE
      RETURN oCurrentNode.tloValueList(oCurrentNode.tloValueList.COUNT() + nDelta).sNodeType;
    END IF;

  END getCurrentNodeType;

  --*************************************************************************************************
  FUNCTION getCurrentNodeId(nDelta IN NUMBER DEFAULT 0)
    RETURN VARCHAR2
  IS
    sCurrentName   constant varchar2(4000) := $$PLSQL_UNIT||'.getCurrentNodeId';
  BEGIN
    -- [ISRC-672] do not use logging in this function, because it is called from ISR$TRACE
    IF oCurrentNode.tloValueList IS NULL
    OR NOT(oCurrentNode.tloValueList.EXISTS(oCurrentNode.tloValueList.COUNT() + nDelta)) THEN
      RETURN oCurrentNode.sNodeId;
    ELSE
      RETURN oCurrentNode.tloValueList(oCurrentNode.tloValueList.COUNT() + nDelta).sNodeId;
    END IF;

  END;

  --*************************************************************************************************
  FUNCTION getCurrentParameterName (nDelta IN NUMBER DEFAULT 0)
    RETURN VARCHAR2
  IS
    sCurrentName   constant varchar2(4000) := $$PLSQL_UNIT||'.getCurrentParameterName';
  BEGIN
    isr$trace.stat('begin', 'begin', sCurrentName);
    IF oCurrentNode.TLOVALUELIST.EXISTS (oCurrentNode.TLOVALUELIST.COUNT () + nDelta) THEN
      RETURN oCurrentNode.TLOVALUELIST (oCurrentNode.TLOVALUELIST.COUNT () + nDelta).sParameterName;
    END IF;
    isr$trace.stat('end', 'end', sCurrentName);
    RETURN NULL;
  END;

  --*************************************************************************************************
  FUNCTION getCurrentParameterValue (nDelta IN NUMBER DEFAULT 0)
    RETURN VARCHAR2
  IS
    sCurrentName   constant varchar2(4000) := $$PLSQL_UNIT||'.getCurrentParameterValue';
  BEGIN
    isr$trace.stat('begin', 'begin', sCurrentName);
    IF oCurrentNode.TLOVALUELIST.EXISTS (oCurrentNode.TLOVALUELIST.COUNT () + nDelta) THEN
      RETURN oCurrentNode.TLOVALUELIST (oCurrentNode.TLOVALUELIST.COUNT () + nDelta).sParameterValue;
    END IF;
    isr$trace.stat('end', 'end', sCurrentName);
    RETURN NULL;
  END;

  --*************************************************************************************************
  PROCEDURE setReloadTreeList (olReload IN STB$TREENODELIST) IS
  BEGIN
    olTreeNodeReloadFrom := olReload;
  END;

  --*************************************************************************************************
  FUNCTION getReloadTreeList
    RETURN STB$TREENODELIST IS
  BEGIN
    RETURN olTreeNodeReloadFrom;
  END getReloadTreeList;

  --*************************************************************************************************
  PROCEDURE setReloadToken (sToken IN VARCHAR2) IS
  BEGIN
    -- MENU_DETAIL_VIEW / RIGHT / NULL means LEFT
    sReloadToken := sToken;
  END setReloadToken;

  --*************************************************************************************************
  FUNCTION getReloadToken
    RETURN VARCHAR2 IS
  BEGIN
    RETURN sReloadToken;
  END getReloadToken;

  --*************************************************************************************************
  FUNCTION getCurrentValueList
    RETURN ISR$VALUE$LIST IS
  BEGIN
    RETURN oCurrentNode.TLOVALUELIST;
  END;

  --*************************************************************************************************
  FUNCTION isNodeInPath(oNode in STB$TREENODE$RECORD, sNodeType IN VARCHAR2 DEFAULT NULL, sNodeId IN VARCHAR2 DEFAULT NULL)
    RETURN VARCHAR2
  IS
    sCurrentName   constant varchar2(4000) := $$PLSQL_UNIT||'.isNodeInPath('||' '||')';
  BEGIN
    isr$trace.stat('begin', 'begin', sCurrentName);
    IF (TRIM(sNodeType) IS NULL AND TRIM(sNodeId) IS NULL)
    OR oNode.tloValueList.COUNT() = 0 THEN
      return 'F';
    END IF;
    FOR i IN 1..oNode.tloValueList.COUNT() LOOP
      IF TRIM(sNodeType) IS NOT NULL
      AND TRIM(sNodeId) IS NOT NULL
      AND oNode.tloValueList(i).sNodeType = sNodeType
      AND oNode.tloValueList(i).sNodeId = sNodeId THEN
        return 'T';
      END IF;
      IF TRIM(sNodeType) IS NOT NULL
      AND TRIM(sNodeId) IS NULL
      AND oNode.tloValueList(i).sNodeType = sNodeType THEN
        return 'T';
      END IF;
      IF TRIM(sNodeType) IS NULL
      AND TRIM(sNodeId) IS NOT NULL
      AND oNode.tloValueList(i).sNodeId = sNodeId THEN
        return 'T';
      END IF;
    END LOOP;
    return 'F';
  END;

  --*************************************************************************************************
  FUNCTION getNodeIdForNodeType(sNodeType IN VARCHAR2, oNode IN STB$TREENODE$RECORD DEFAULT NULL)
    RETURN VARCHAR2
  IS
    sCurrentName   constant varchar2(4000) := $$PLSQL_UNIT||'.getNodeIdForNodeType(' || sNodeType || ')';
    oCurrentNode STB$TREENODE$RECORD;
    sNodeid   varchar2(1024) := 'NOT_FOUND';
  BEGIN
    isr$trace.stat('begin', 'begin', sCurrentName);
    IF oNode IS NULL THEN
      oCurrentNode := STB$OBJECT.getCurrentNode;
    ELSE
      oCurrentNode := oNode;
    END IF;
    IF Trim(sNodeType) IS NULL OR oCurrentNode.tloValueList.Count() = 0 THEN
      isr$trace.stat('end', sNodeid, sCurrentName);
      RETURN sNodeid;
    END IF;
    FOR i IN 1..oCurrentNode.tloValueList.Count() LOOP
      --isr$trace.debug(i || '; ' || sNodeType,oCurrentNode.tloValueList(i).sNodeType,  sCurrentName);
      IF Trim(sNodeType) IS NOT NULL
      AND oCurrentNode.tloValueList(i).sNodeType = sNodeType THEN
      
        IF oCurrentNode.tloValueList.EXISTS(i-1)
        AND oCurrentNode.tloValueList(i).sNodeType = oCurrentNode.tloValueList(i-1).sNodeType THEN
          sNodeid := oCurrentNode.tloValueList(i).sNodeId;
          isr$trace.stat('end', sNodeid, sCurrentName);
          RETURN sNodeid;
        END IF;
        IF oCurrentNode.tloValueList.EXISTS(i+1)
        AND oCurrentNode.tloValueList(i).sNodeType != oCurrentNode.tloValueList(i+1).sNodeType THEN
          sNodeid :=  oCurrentNode.tloValueList(i).sNodeId;
          isr$trace.stat('end', sNodeid, sCurrentName);
          RETURN sNodeid;
        END IF;
        IF NOT(oCurrentNode.tloValueList.EXISTS(i+1))
        OR NOT(oCurrentNode.tloValueList.EXISTS(i-1)) THEN
          sNodeid :=  oCurrentNode.tloValueList(i).sNodeId;
          isr$trace.stat('end', sNodeid, sCurrentName);
          RETURN sNodeid;
        END IF;
        
      END IF;
    END LOOP;
    isr$trace.stat('end', sNodeid, sCurrentName);
    RETURN sNodeid;
  END getNodeIdForNodeType;


  --*************************************************************************************************
  FUNCTION GetCurrentNumberOfMasters return pls_integer is
  begin
    return nNumberOfMasters;
  end GetCurrentNumberOfMasters;

  --*************************************************************************************************
  PROCEDURE SetCurrentNumberOfMasters(cnMasters in pls_integer) is
  begin
    nNumberOfMasters := cnMasters;
  end SetCurrentNumberOfMasters;

END stb$object;