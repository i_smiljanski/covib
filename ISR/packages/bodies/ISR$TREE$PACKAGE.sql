CREATE OR REPLACE package body ISR$TREE$PACKAGE
is

  sLeftSide      VARCHAR2(1) := 'T';
  sJobListOpen   VARCHAR2(1) := 'F';
  

  cursor cCheckSide is
    select leftside, case when NVL(left_as_right, 'F') = 'J' then 'T' else 'F' end 
      from isr$tree$sessions
     where sessionid = STB$SECURITY.getCurrentSession
       and selected = 'T'
       and nodetype = STB$OBJECT.getCurrentNodeType;

  cursor cGetValueForReport(sNodeId in VARCHAR2, sNodeType in VARCHAR2, nRepid in NUMBER) is
    select case
              when TRIM(c.formatMask) is not null then
                 TO_CHAR (groupingdate, c.formatMask)
              else
                 groupingvalue
           end
      from isr$report$grouping$table rg, isr$custom$grouping c
     where rg.repid = nRepid
       and rg.groupingname = sNodeType
       and c.nodeid = sNodeId
       and c.nodename = rg.groupingname;

  cursor cGetTitle(nRepid in NUMBER) is
    select title, title
      from stb$report
     where repid = nRepid;

  cursor cLoadOnlyRight(sNodeType in VARCHAR2) is
    select c.load_only_right
      from isr$custom$grouping c
     where nodetype = sNodeType;

  sLoadOnlyRight VARCHAR2(1);

  cursor cCheckIsNodeLoaded(csNodeType in varchar2, csNodeId in varchar2) is
    select 'T'
      from isr$tree$sessions
     where nodetype = csNodetype
       and nodeid = csNodeId
       and sessionid = STB$SECURITY.getCurrentSession
       and leftside = 'T';

  sNodeIsLoaded             VARCHAR2(1);
  
  sLoaderContext        VARCHAR2(500) := STB$UTIL.getSystemParameter('LOADER_CONTEXT');
  sLoaderNamespace      VARCHAR2(500) := STB$UTIL.getSystemParameter('LOADER_NAMESPACE');
  

--==****************************************************************************
procedure selectLeftNode(oNode in STB$TREENODE$RECORD)
is
begin
  STB$OBJECT.setCurrentNode(oNode);
  STB$OBJECT.setReloadToken(null);
end;

-- *****************************************************************************
procedure selectRightNode(oNode in STB$TREENODE$RECORD)
is
begin
  STB$OBJECT.setCurrentNode(oNode);
  STB$OBJECT.setReloadToken('RIGHT');
end;

-- *****************************************************************************
function rebuildReportNode(cnRepid in NUMBER, oOldNode in STB$TREENODE$RECORD default STB$OBJECT.getCurrentNode) return STB$TREENODE$RECORD
is
  sCurrentName              constant VARCHAR2(100) := $$PLSQL_UNIT||'.rebuildReportNode(cnRepid=>'||cnRepid||')';
  oRebuildedNode            STB$TREENODE$RECORD;
  olNodeList                STB$TREENODELIST;
  sRepTypePackage           VARCHAR2(100);

begin
  isr$trace.stat('begin','begin', sCurrentName);
  oRebuildedNode := oOldNode;
  isr$trace.debug('node determined', 'node determined', sCurrentName);

  -- rebuild the node value list
  for i in 1..oRebuildedNode.tloValueList.COUNT loop
    if oRebuildedNode.tloValueList(i).sNodeType = 'VALUENODE' then
      open cGetValueForReport(oRebuildedNode.tloValueList(i).sNodeId, oRebuildedNode.tloValueList(i).sParameterName, cnRepid);
      fetch cGetValueForReport into oRebuildedNode.tloValueList(i).sParameterValue;
      close cGetValueForReport;
      isr$trace.debug('value param '|| oRebuildedNode.tloValueList(i).sParameterName, oRebuildedNode.tloValueList(i).sParameterValue, sCurrentName);
    elsif oRebuildedNode.tloValueList(i).sNodeType = 'TITLE' then
      if cnRepid != -1 then
        open cGetTitle(cnRepid);
        fetch cGetTitle into oRebuildedNode.tloValueList(i).sParameterValue, oRebuildedNode.tloValueList(i).sNodeId;
        close cGetTitle;
        sRepTypePackage := nvl(STB$UTIL.getReptypeParameter (stb$util.getReportTypeForTitle(oRebuildedNode.tloValueList(i).sParameterValue), 'REP_TYPE_PACKAGE'),'ISR$REPORTTYPE$PACKAGE');
        oRebuildedNode.sPackageType := sRepTypePackage;
      end if;
      olNodeList := STB$TREENODELIST(oRebuildedNode);
      ISR$UTD$PACKAGE.getCustomIcon(olNodeList, 'TITLE');
      oRebuildedNode := olNodeList(1);
    elsif oRebuildedNode.tloValueList(i).sNodeType = 'REPORT' then
      oRebuildedNode.tloValueList(i).sParameterValue := cnRepid;
      oRebuildedNode.tloValueList(i).sNodeId := cnRepid;
      oRebuildedNode.tloValueList(i).sNodePackage := sRepTypePackage;
      olNodeList := STB$TREENODELIST(oRebuildedNode);
      ISR$UTD$PACKAGE.getCustomIcon(olNodeList, 'REPORT');
      oRebuildedNode := olNodeList(1);
    end if;
  end loop;
  oRebuildedNode.sNodeId := STB$OBJECT.calculateHash (oRebuildedNode.tloValueList)||'#@#'||STB$SECURITY.getCurrentUser;
  isr$trace.debug('node rebuilt see logclob', 'node rebuilt', sCurrentName, oRebuildedNode);

  isr$trace.stat('end','end', sCurrentName);

  return oRebuildedNode;

end;

-- *****************************************************************************
function buildReportNode(cnRepid in NUMBER, oOldNode in STB$TREENODE$RECORD default STB$OBJECT.getCurrentNode, csCustomNodeBranch in varchar2 default null) return STB$TREENODE$RECORD
is
  sCurrentName              constant VARCHAR2(100) := $$PLSQL_UNIT||'.buildReportNode(cnRepid=>'||cnRepid||', csCustomNodeBranch=>'||csCustomNodeBranch||')';
  oRebuildedNode            STB$TREENODE$RECORD;

  nStartNodeid              NUMBER;
  sCustomNodeBranch         VARCHAR2(500);

--all case expressions in this cursor are probably not used
  cursor cGetOptimalPath(csSubNode varchar2 default 'CUSTOMGLOBALNODE') is
    with tree as (select PACKAGE sPackageType
                       , case when NVL(referencetype, '') = 'DATAGROUP' then 'CUSTOMGROUPNODE' when nodetype != nodename then 'VALUENODE' else nodetype end sNodetype
                       , nodeid sNodeid
                       , case when NVL(referencetype, '') = 'DATAGROUP' then 'CUSTOMGROUPNODE' else nodename end sParametername
                       , case when NVL(referencetype, '') = 'DATAGROUP' then to_char(nodeid) when nodetype != nodename then 'TODO' else TO_CHAR (nodeid) end sParametervalue
                       , LPAD (' ', 2 * LEVEL - 1) || SYS_CONNECT_BY_PATH (nodeName, '/') PATH
                       , LEVEL mylevel
                       , referencetype
                       , reference
                    from isr$custom$grouping
                   where (   NVL(referencetype, '') = 'USER' and reference = TO_CHAR(stb$security.getCurrentuser) 
                          or NVL(referencetype, '') = 'DATAGROUP' and reference in (select TO_CHAR(datagroup) from stb$report where repid = cnRepid)
                          or NVL(referencetype, '') not in ('USER', 'DATAGROUP')
                          or referencetype is null)
                  start with nodetype = 'TOPNODE'
                  connect by prior nodeid = parentnode
                  order siblings by orderno)
    select t1.sNodeId
      from tree t1, (select MAX (mylevel) mylevel
                       from tree
                      where PATH like '%' || csSubNode || '%') t2
     where t1.mylevel = t2.mylevel
       and PATH like '%' || csSubNode || '%';

begin
  isr$trace.stat('begin','begin', sCurrentName);
  oRebuildedNode := STB$TREENODE$RECORD();
  oRebuildedNode.sNodeType := 'REPORT';
  oRebuildedNode.sPackageType := 'ISR$REPORTTYPE$PACKAGE';

  isr$trace.debug('sCustomNodeBranch', sCustomNodeBranch, sCurrentName);

  open cGetOptimalPath(NVL(csCustomNodeBranch, 'CUSTOMGLOBALNODE'));
  fetch cGetOptimalPath into nStartNodeid;
  close cGetOptimalPath;

  isr$trace.debug('nStartNodeid', nStartNodeid, sCurrentName);

  --ABBIS-324: condition "and nodetype = 'CUSTOMGROUPNODE'" added 
  --because the subnodes from DATAGROUP node also have the referencetype = 'DATAGROUP'
  --and the subnodes must have the nodetype and nodename as in the table 
  for rGetPath in ( select *
                      from (select PACKAGE sNodePackage
                                 , case when NVL(referencetype, '') = 'DATAGROUP' and nodetype = 'CUSTOMGROUPNODE' then 'CUSTOMGROUPNODE' when nodetype != nodename then 'VALUENODE' else nodetype end sNodetype
                                 , nodeid sNodeid
                                 , case when NVL(referencetype, '') = 'DATAGROUP' and nodetype = 'CUSTOMGROUPNODE' then 'CUSTOMGROUPNODE' else nodename end sParametername
                                 , case when NVL(referencetype, '') = 'DATAGROUP' then to_char(nodeid) when nodetype != nodename then 'TODO' else TO_CHAR (nodeid) end sParametervalue
                                 , ROWNUM myrow
                              from isr$custom$grouping
                            start with nodeid = nStartNodeid
                            connect by nodeid = prior parentnode
                            order siblings by orderno)
                    order by myrow desc ) loop
    isr$trace.debug('extend', rGetPath.sParameterName||' -> '||rGetPath.sParametervalue, sCurrentName);
    oRebuildedNode.tloValueList.EXTEND();
    oRebuildedNode.tloValueList(oRebuildedNode.tloValueList.COUNT()) := ISR$VALUE$RECORD();
    oRebuildedNode.tloValueList(oRebuildedNode.tloValueList.COUNT()).sNodePackage := rGetPath.sNodePackage;
    oRebuildedNode.tloValueList(oRebuildedNode.tloValueList.COUNT()).sNodeType := rGetPath.sNodetype;
    oRebuildedNode.tloValueList(oRebuildedNode.tloValueList.COUNT()).sNodeid := rGetPath.sNodeid;
    oRebuildedNode.tloValueList(oRebuildedNode.tloValueList.COUNT()).sParameterName := rGetPath.sParameterName;
    oRebuildedNode.tloValueList(oRebuildedNode.tloValueList.COUNT()).sParametervalue := rGetPath.sParametervalue;

  end loop;

  oRebuildedNode.tloValueList.EXTEND();
  oRebuildedNode.tloValueList(oRebuildedNode.tloValueList.COUNT()) := ISR$VALUE$RECORD();
  oRebuildedNode.tloValueList(oRebuildedNode.tloValueList.COUNT()).sNodeType := 'TITLE';
  oRebuildedNode.tloValueList(oRebuildedNode.tloValueList.COUNT()).sParameterName := 'TITLE';
  oRebuildedNode.tloValueList(oRebuildedNode.tloValueList.COUNT()).sNodePackage := 'ISR$REPORTTYPE$PACKAGE';

  oRebuildedNode.tloValueList.EXTEND();
  oRebuildedNode.tloValueList(oRebuildedNode.tloValueList.COUNT()) := ISR$VALUE$RECORD();
  oRebuildedNode.tloValueList(oRebuildedNode.tloValueList.COUNT()).sNodeType := 'REPORT';
  oRebuildedNode.tloValueList(oRebuildedNode.tloValueList.COUNT()).sParameterName := 'REPORT';
  oRebuildedNode.tloValueList(oRebuildedNode.tloValueList.COUNT()).sNodePackage := 'ISR$REPORTTYPE$PACKAGE';

  oRebuildedNode := rebuildReportNode(cnRepid, oRebuildedNode);

  isr$trace.stat('end','end', sCurrentName);

  return oRebuildedNode;

end;

-- *****************************************************************************
function buildNode(csNodeType in VARCHAR2, csNodeId in VARCHAR2, oOldNode in STB$TREENODE$RECORD default STB$OBJECT.getCurrentNode) return STB$TREENODE$RECORD
is
  sCurrentName              constant VARCHAR2(100) := $$PLSQL_UNIT||'.buildNode(csNodeType=>'||csNodeType||', csNodeId=>'||csNodeId||')';
  oRebuildedNode            STB$TREENODE$RECORD;
begin
  isr$trace.stat('begin','begin', sCurrentName);

  oRebuildedNode := oOldNode;
  isr$trace.debug('node determined', 'node determined', sCurrentName);

  oRebuildedNode.sNodeType := csNodeType;
  oRebuildedNode.sNodeId := csNodeId;
  STB$OBJECT.setDirectory(oRebuildedNode);
  isr$trace.debug('node rebuilt see logclob', 'node rebuilt', sCurrentName, oRebuildedNode);

  isr$trace.stat('end','end', sCurrentName);

  return oRebuildedNode;

end;

-- *****************************************************************************
function searchSimiliarNode(csNodeType in VARCHAR2, csNodeId in VARCHAR2, csEqual in VARCHAR2 default 'F', csCustomNodeBranch in VARCHAR2 default null) return STB$TREENODE$RECORD
is
  sCurrentName              constant VARCHAR2(200) := $$PLSQL_UNIT||'.searchSimiliarNode(csNodeType=>'||csNodeType||', csNodeId=>'||csNodeId||', csEqual=>'||csEqual||', csCustomNodeBranch=>'||csCustomNodeBranch||')';
  oNode                     STB$TREENODE$RECORD := STB$TREENODE$RECORD ();
  sDisplay                  VARCHAR2(4000);
  
  
  cursor cGetNode is 
    select SPACKAGETYPE
         , SNODETYPE
         , SNODEID
         , display_path
      from (select packagename SPACKAGETYPE
                 , nodetype SNODETYPE
                 , nodeid SNODEID
                 , (select MAX (SYS_CONNECT_BY_PATH (t1.nodetype, '@#@')) 
                      from isr$tree$nodes t1
                    start with t1.nodeid = t.nodeid
                    connect by t1.nodeid = prior t1.parentnodeid) display_path
              from ISR$TREE$NODES t
             where nodeid like '%' || STB$SECURITY.getCurrentUser
               and (clearNodeid = csNodeId
                and csEqual = 'T'
                 or csEqual = 'F')
               and nodeType = csNodeType)
     where (csNodeType = 'REPORT'
        and display_path like '%' || NVL (csCustomNodeBranch, 'CUSTOMGLOBALNODE') || '%'
         or csNodeType != 'REPORT');
  
begin
  isr$trace.stat('begin','begin', sCurrentName);
  
  open cGetNode;
  fetch cGetNode into oNode.sPackageType, oNode.sNodetype, oNode.sNodeid, sDisplay;
  close cGetNode;
     
  select ISR$VALUE$RECORD ( sParametername
                        , sParametervalue
                        , sNodetype
                        , sNodeid
                        , sNodePackage)
  bulk collect
  into oNode.tloValueList
  from (select parametername sParametername
             , parametervalue sParametervalue
             , nodetype sNodetype
             , clearnodeid sNodeid
             , packagename sNodePackage
          from (select parametername
                     , parametervalue
                     , nodetype
                     , clearnodeid
                     , packagename
                     , ROWNUM r
                  from isr$tree$nodes t1
                start with nodeid = oNode.sNodeid
                connect by t1.nodeid = prior t1.parentnodeid)
        order by r desc);

  isr$trace.stat('end','oNode in logclob', sCurrentName, xmltype(oNode));

  return oNode;

end;

-- *****************************************************************************
function searchThisNode(csNodeType in VARCHAR2, csNodeId in VARCHAR2, csCustomNodeBranch in VARCHAR2 default null) return STB$TREENODE$RECORD
is
  sCurrentName              constant VARCHAR2(4000) := $$PLSQL_UNIT||'.searchThisNode(csNodeType=>'||csNodeType||', csNodeId=>'||csNodeId||', csCustomNodeBranch=>'||csCustomNodeBranch||')';
  oNode                     STB$TREENODE$RECORD;
  olNodeList                STB$TREENODELIST;
begin

  isr$trace.stat('begin','begin', sCurrentName);
  oNode := searchSimiliarNode(csNodeType, csNodeId, 'T', csCustomNodeBranch);
  isr$trace.stat('end','end', sCurrentName);

  return oNode;

end;

-- *****************************************************************************
procedure loadNode(oNode in STB$TREENODE$RECORD, oOldNode in STB$TREENODE$RECORD default null, csAppend in varchar2 default 'F')
is
  sCurrentName              constant VARCHAR2(100) := $$PLSQL_UNIT||'.loadNode(csAppend=>'||csAppend||')';
  olTreeLoadList            STB$TREENODELIST;
  olOldValueList            ISR$VALUE$LIST;
  olNewValueList            ISR$VALUE$LIST;
  nCount                    NUMBER;
  sNodeId                   VARCHAR2(100);
  sReload                   VARCHAR2(1) := 'F';
begin
  isr$trace.stat('begin','begin', sCurrentName);

  -- build the load tree list

  if oOldNode is null then
    -- the list consists only of the node
    olTreeLoadList := STB$TREENODELIST(oNode);
    isr$trace.debug('olTreeLoadList ('||olTreeLoadList.COUNT ()||')', 'in logclob (oOldNode is null)', sCurrentName, olTreeLoadList(olTreeLoadList.COUNT ()));
  else
    -- the list must contain the last common node of old and new node plus all subnodes
    olTreeLoadList := STB$TREENODELIST ();
    if csAppend = 'T' then
      olTreeLoadList := STB$OBJECT.getReloadTreeList;
    end if;

    olNewValueList := ISR$VALUE$LIST ();
    olOldValueList := oOldNode.tloValueList;

    nCount := oNode.tloValueList.COUNT ();
    if nCount < olOldValueList.COUNT () then
      nCount := olOldValueList.COUNT ();
    end if;
    isr$trace.debug('nCount', nCount, sCurrentName);

    for nElements in 1 .. nCount loop  
      if olOldValueList.EXISTS (nElements) then
        isr$trace.debug(nElements||'. element of olOldValueList in logclob', olOldValueList (nElements).sParameterName || ' ' || olOldValueList (nElements).sParameterValue, sCurrentName);
      end if;
      
      if oNode.tloValueList.EXISTS (nElements) then  --ISRC-1245
        isr$trace.debug(nElements||'. element of oNode.tloValueList in logclob', oNode.tloValueList (nElements).sParameterName || ' ' || oNode.tloValueList (nElements).sParameterValue, sCurrentName);

        if olOldValueList.EXISTS (nElements)
        and xmltype(olOldValueList (nElements)).getClobVal() = xmltype(oNode.tloValueList (nElements)).getClobVal()
        and olTreeLoadList.EXISTS(1)
        and sReload = 'F' then
          olTreeLoadList.TRIM();
        end if;

        if olOldValueList.EXISTS (nElements)
        and xmltype(olOldValueList (nElements)).getClobVal() != xmltype(oNode.tloValueList (nElements)).getClobVal() then
          sReload := 'T';
        end if;

        isr$trace.debug('sReload '||oNode.tloValueList (nElements).sNodeType, sReload, sCurrentName);

        open cCheckIsNodeLoaded(oNode.tloValueList (nElements).sNodeType, sNodeId);
        fetch cCheckIsNodeLoaded into sNodeIsLoaded;
        close cCheckIsNodeLoaded;
        isr$trace.debug('sNodeIsLoaded '||oNode.tloValueList (nElements).sNodeType, sNodeIsLoaded, sCurrentName);

        olNewValueList.EXTEND;
        olNewValueList (olNewValueList.COUNT()) := oNode.tloValueList (nElements);

        sNodeId := STB$OBJECT.calculateHash (olNewValueList)||'#@#'||STB$SECURITY.getCurrentUser;
        isr$trace.debug('sNodeId', sNodeId, sCurrentName);

        open cLoadOnlyRight(oNode.tloValueList (nElements).sNodeType);
        fetch cLoadOnlyRight into sLoadOnlyRight;
        close cLoadOnlyRight;

        olTreeLoadList.EXTEND;
        olTreeLoadList (olTreeLoadList.COUNT ()) := STB$TREENODE$RECORD( case when sLoadOnlyRight = 'T' then 'DONT_RELOAD' else null end
                                                                      , oNode.tloValueList (nElements).sNodePackage
                                                                      , oNode.tloValueList (nElements).sNodeType
                                                                      , sNodeId
                                                                      , tloValueList => olNewValueList );
        isr$trace.debug('olTreeLoadList ('||olTreeLoadList.COUNT ()||')', 'in logclob', sCurrentName, olTreeLoadList(olTreeLoadList.COUNT ()));
      
      end if;

    end loop;

  end if;

  isr$trace.debug('olTreeLoadList ('||olTreeLoadList.COUNT ()||')', 'in logclob', sCurrentName, olTreeLoadList);
  STB$OBJECT.setReloadTreeList(olTreeLoadList);

  isr$trace.stat('end','end', sCurrentName);

end;

-- *****************************************************************************
procedure reloadNodesAndSelect(cnNodes in NUMBER default 0, csSelectNew in VARCHAR2 default 'T')
is
  sCurrentName         constant VARCHAR2(100) := $$PLSQL_UNIT||'.reloadNodesAndSelect(cnNodes=>'||cnNodes||', csSelectNew=>'||csSelectNew||')';
  oNewNode                      STB$TREENODE$RECORD;
  oOldNode                      STB$TREENODE$RECORD;
  oSelectNode                   STB$TREENODE$RECORD;
  nNodes                        NUMBER := cnNodes;

begin
  isr$trace.stat('begin','begin', sCurrentName);

  open cCheckSide;
  fetch cCheckSide into sLeftSide, sJobListOpen;
  close cCheckSide;
  isr$trace.debug('sLeftSide '||sLeftSide, 'sJobListOpen '||sJobListOpen, sCurrentName);
  if sLeftSide = 'F' then
    nNodes := nNodes - 1;
  end if;
  isr$trace.debug('nNodes', nNodes, sCurrentName);

  oOldNode := STB$OBJECT.getCurrentNode;
  oNewNode := STB$OBJECT.getCurrentNode;
  if nNodes < 0 then
    STB$OBJECT.setNode (oNewNode, nNodes);
  end if;
  ISR$TREE$PACKAGE.loadNode(oNewNode);

  oSelectNode := case when csSelectNew = 'T' then oNewNode else oOldNode end;
  isr$trace.debug('oSelectNode', 'in logclob', sCurrentName, oSelectNode);
  if sLeftSide = 'T' then
    ISR$TREE$PACKAGE.selectLeftNode(oSelectNode);
  else
    ISR$TREE$PACKAGE.selectRightNode(oSelectNode);
  end if;
  isr$trace.stat('end','end', sCurrentName);
end;

-- *****************************************************************************
procedure selectCurrent(csSendSyncCall in VARCHAR2 default 'T')
is
  sCurrentName         constant VARCHAR2(100) := $$PLSQL_UNIT||'.selectCurrent';
  oNewNode                      STB$TREENODE$RECORD;
begin
  isr$trace.stat('begin','begin', sCurrentName);
  reloadNodesAndSelect(0, 'F');
  if (csSendSyncCall = 'T') then
    ISR$TREE$PACKAGE.sendSyncCall;
  end if;
  isr$trace.stat('end','end', sCurrentName);
end;

-- *****************************************************************************
procedure reloadPrevNodesAndSelect(cnNodes in NUMBER, csSendSyncCall in VARCHAR2 default 'T')
is
  sCurrentName         constant VARCHAR2(100) := $$PLSQL_UNIT||'.reloadPrevNodesAndSelect(cnNodes=>'||cnNodes||')';
  oNewNode                      STB$TREENODE$RECORD;
begin
  isr$trace.stat('begin','begin', sCurrentName);
  reloadNodesAndSelect(cnNodes);
  if (csSendSyncCall = 'T') then
    ISR$TREE$PACKAGE.sendSyncCall;
  end if;
  isr$trace.stat('end','end', sCurrentName);
end;

-- *****************************************************************************
procedure reloadPrevNodesAndSelectCurren(cnNodes in NUMBER, csSendSyncCall in VARCHAR2 default 'T')
is
  sCurrentName         constant VARCHAR2(100) := $$PLSQL_UNIT||'.reloadPrevNodesAndSelectCurren(cnNodes=>'||cnNodes||')';
  oNewNode                      STB$TREENODE$RECORD;
begin
  isr$trace.stat('begin','begin', sCurrentName);
  reloadNodesAndSelect(cnNodes, 'F');
  if (csSendSyncCall = 'T') then
    ISR$TREE$PACKAGE.sendSyncCall;
  end if;
  isr$trace.stat('end','end', sCurrentName);
end;

--*********************************************************************************************************************************
procedure changeLoadFlag(oCurrentNode in STB$TREENODE$RECORD)
is
  pragma autonomous_transaction;

  oErrorObj     STB$OERROR$RECORD := STB$OERROR$RECORD();
  sCurrentName  constant varchar2(100) := $$PLSQL_UNIT||'.changeLoadFlag('||oCurrentNode.sNodeType||')';
begin
  isr$trace.stat('begin', 'begin', sCurrentName);

  update isr$tree$sessions
     set first_load = 'T'
   where (nodetype, nodeid) not in
               (select oCurrentNode.sNodetype
                     , oCurrentNode.sNodeid
                  from DUAL)
     and sessionid = STB$SECURITY.getCurrentSession;

  isr$trace.stat('end', 'end', sCurrentName);
  commit;

end changeLoadFlag;

-- *****************************************************************************
function synchronizeInBackground(sToken out varchar2, olTreeReloadList out STB$TREENODELIST, oTreeReloadNode out STB$TREENODE$RECORD, csNodeType in VARCHAR2, csNodeId in VARCHAR2, csSessionId in VARCHAR2)
return STB$OERROR$RECORD
is
  sCurrentName   constant varchar2(255) := $$PLSQL_UNIT||'.synchronizeInBackground(csNodeType=>'||csNodeType||', csNodeId=>'||csNodeId||', csSessionId=>'||csSessionId||')';
  sUserMsg       varchar2(4000);
  sDevMsg        varchar2(4000);
  sImplMsg       varchar2(4000);
  oErrorObj      STB$OERROR$RECORD := STB$OERROR$RECORD(tloMessageList => ISR$MESSAGE$LIST()); --initialize the message list too
  nStartTimeNum  number      := DBMS_UTILITY.GET_TIME;
  olTreeReloadList1 STB$TREENODELIST;  
  --olPropertyList  STB$PROPERTY$LIST;  
  sIsOwnSession        varchar2(1);
  sNodeId   isr$tree$nodes.nodeid%type;

  cursor cGetNodesToLoad(cnSession number) is
    select MIN(case 
                 when NVL(sToken,'') = 'RIGHT' 
                  and t.nodeid in (select tn.parentnodeid 
                                     from isr$tree$nodes tn, isr$tree$sessions ts 
                                    where ts.selected = 'T' 
                                      and ts.nodeid = tn.nodeid 
                                      and ts.sessionid = cnSession) then 10000 
                 else 1
               end 
               * ordernumber)
         , nodetype
         , t.nodeid
         , packagename
         , display
         , display_path
      from (select t1.nodetype
                 , t1.nodeid
                 , t1.packagename
                 , t1.parametervalue display
                 , SYS_CONNECT_BY_PATH(t1.parametervalue, '@#@') display_path
                 , LEVEL levelnumber
                 , ROWNUM ordernumber
              from isr$tree$nodes t1
            start with nodetype = 'TOPNODE'
            connect by nocycle prior t1.nodeid = t1.parentnodeid) t, (select ts1.nodeid
                                                                        from isr$tree$sessions ts1, isr$tree$dependencies td
                                                                       where ts1.nodetype = td.dependend_type
                                                                         and (td.nodetype = csNodeType /*or td.dependend_type = csNodeType and force_nodetype = 'T'*/)
                                                                         and (csSessionid != cnSession and ownsession = 'F' or ownsession = 'T')
                                                                         and ts1.sessionid = cnSession) ts
     where t.nodeid = ts.nodeid
    group by t.nodetype
           , t.nodeid
           , packagename
           , display
           , display_path
    order by 1;

  olValueList     ISR$VALUE$LIST;
  oValueRec       ISR$VALUE$RECORD;
  sNewValue       VARCHAR2(255);
  sOldDisplayPath VARCHAR2(4000);
  sOldLevelNumber VARCHAR2(3);
  sOldIcon        VARCHAR2(255);

  cursor cGetversion(nRepid in NUMBER) is
    select case when version is null then title else 'Version: ' || VERSION end
      from stb$report
     where repid = nRepid;

  --**********

  function checkIsInTree(csDisplayPath in varchar2, cnCurrentUser in number) return varchar2 is
    sReturn VARCHAR2(1) := 'F';
    cursor cCheckIsInTree is
      select 'T'
        from (select t1.nodeid
                   , SYS_CONNECT_BY_PATH(t1.parametervalue, '@#@') display_path
                from isr$tree$nodes t1
               where nodeid like '%'||cnCurrentUser
              start with nodetype = 'TOPNODE'
            connect by nocycle prior t1.nodeid = t1.parentnodeid)
       where display_path = csDisplayPath;
  begin
    open cCheckIsInTree;
    fetch cCheckIsInTree into sReturn;
    close cCheckIsInTree;
    isr$trace.stat('cCheckIsInTree '||csDisplayPath, 'return '||sReturn, sCurrentName);
    return sReturn;
  end;

  --**********

  function checkIsToUpdate(csCurrentNodetype in varchar2) return varchar2 is
    sReturn VARCHAR2(1) := 'F';
    cursor cCheckIsToUpdate is
      select distinct 'T'
        from isr$tree$dependencies td
       where csCurrentNodetype in (nodetype, dependend_type)
         and (td.nodetype = csNodeType /*or td.dependend_type = csNodeType and force_nodetype = 'T'*/);
  begin
    open cCheckIsToUpdate;
    fetch cCheckIsToUpdate into sReturn;
    close cCheckIsToUpdate;
    isr$trace.stat('cCheckIsToUpdate '||csCurrentNodetype, 'return '||sReturn, sCurrentName);
    return sReturn;
  end;
begin
  isr$trace.stat('begin', 'begin', sCurrentName);

  open cCheckSide;
  fetch cCheckSide into sLeftSide, sJobListOpen;
  close cCheckSide;
  isr$trace.debug('sLeftSide '||sLeftSide, 'sJobListOpen '||sJobListOpen, sCurrentName);

  sToken := case when sLeftSide = 'T' or sJobListOpen = 'T' then null else 'RIGHT' end;
  isr$trace.debug('sToken', sToken, sCurrentName);
  oTreeReloadNode := stb$object.getCurrentNode;
  if sJobListOpen = 'T' THEN
    oTreeReloadNode := null;
  end if;
  olTreeReloadList := STB$TREENODELIST();

  /*if oTreeReloadNode.sNodeType = 'TOPNODE' then
    olTreeReloadList := STB$TREENODELIST(oTreeReloadNode);  
  end if;*/

  for rGetNodesToLoad in cGetNodesToLoad( stb$security.getCurrentSession ) loop

    isr$trace.debug('rGetNodesToLoad.nodetype ' || rGetNodesToLoad.nodetype ||' '|| rGetNodesToLoad.nodeid, rGetNodesToLoad.display_path, sCurrentName);

    select ISR$VALUE$RECORD ( sParametername
                            , sParametervalue
                            , sNodetype
                            , sNodeid
                            , sNodePackage)
      bulk collect
      into olValueList
      from (select parametername sParametername
                 , parametervalue sParametervalue
                 , nodetype sNodetype
                 , clearnodeid sNodeid
                 , packagename sNodePackage
              from (select parametername
                         , parametervalue
                         , nodetype
                         , clearnodeid
                         , packagename
                         , ROWNUM r
                      from isr$tree$nodes t1
                    start with nodeid = rGetNodesToLoad.nodeid
                    connect by t1.nodeid = prior t1.parentnodeid)
            order by r desc);

    oValueRec := olValueList(olValueList.COUNT);
    
    case
      when csNodeType like 'REPORT_%' then

        if rGetNodesToLoad.nodetype = 'VALUENODE' then
          open cGetValueForReport(oValueRec.sNodeId, oValueRec.sParameterName, TO_NUMBER(csNodeId));
          fetch cGetValueForReport into sNewValue;
          close cGetValueForReport;
          isr$trace.debug('value param '|| oValueRec.sParameterName, sNewValue||' '||oValueRec.sParameterValue, sCurrentName);
        elsif rGetNodesToLoad.nodetype = 'TITLE' then
          open cGetTitle(TO_NUMBER(csNodeId));
          fetch cGetTitle into sNewValue, sNewValue;
          close cGetTitle;
          isr$trace.debug('title '|| oValueRec.sParameterName, sNewValue||' '||rGetNodesToLoad.display, sCurrentName);
        elsif rGetNodesToLoad.nodetype like 'REPORT_%' then
          open cGetVersion(TO_NUMBER(csNodeId));
          fetch cGetVersion into sNewValue;
          close cGetVersion;
          isr$trace.debug('version '|| oValueRec.sParameterName, sNewValue||' '||rGetNodesToLoad.display, sCurrentName);
        else
          sNewValue := rGetNodesToLoad.display;
        end if;

      else

        null;

    end case;

    if (checkIsToUpdate(rGetNodesToLoad.nodetype) = 'T') then
      if rGetNodesToLoad.nodetype = 'TITLE' and rGetNodesToLoad.display = sNewValue and csNodeType like 'REPORT_%'
      or rGetNodesToLoad.nodetype != 'TITLE' then
        olTreeReloadList.EXTEND;
        open cLoadOnlyRight(rGetNodesToLoad.nodetype);
        fetch cLoadOnlyRight into sLoadOnlyRight;
        close cLoadOnlyRight;
        olTreeReloadList (olTreeReloadList.COUNT ()) := STB$TREENODE$RECORD( case 
                                                                                when sLoadOnlyRight = 'T'
                                                                                or csNodeType in ('DOCUMENT','RAWDATA','COMMONDOC','REMOTESNAPSHOT','LINK')
                                                                                or csNodeType like '%_REPORT_FILE' 
                                                                                or csNodeType like 'DOCUMENT_%'
                                                                                  then 'DONT_RELOAD' 
                                                                                else /*for debugging*/ 
                                                                                  'set in the next steps' 
                                                                             end
                                                                      , rGetNodesToLoad.packagename
                                                                      , rGetNodesToLoad.nodetype
                                                                      , rGetNodesToLoad.nodeid
                                                                      , TLOVALUELIST => olValueList
                                                                      );
        changeLoadFlag(olTreeReloadList (olTreeReloadList.COUNT ()));
        isr$trace.debug('olTreeReloadList ('||olTreeReloadList.COUNT ()||')', 'in logclob', sCurrentName, olTreeReloadList (olTreeReloadList.COUNT ()));
        if rGetNodesToLoad.nodetype = 'TOPNODE' then
          exit;
        end if;
      end if;
    end if;
    
  end loop;

  isr$trace.debug('olTreeReloadList ('||olTreeReloadList.COUNT ()||')', 'in logclob', sCurrentName, olTreeReloadList);

  /*  set display, for report updates drop unnecessary updates */
  for i in 1..olTreeReloadList.COUNT () loop
    sOldIcon := olTreeReloadList(i).sIcon;
    
    if csNodeType like 'REPORT_%' then
      olTreeReloadList(i) := rebuildReportNode(csNodeId, olTreeReloadList(i));
    end if;

    if csNodeType = 'TITLE' then 
      olTreeReloadList(i) := rebuildReportNode(-1, olTreeReloadList(i));
    end if;
    
    if olTreeReloadList(i).sDisplay != 'DONT_RELOAD' then
      select '@#@' || RTRIM (XMLAGG (XMLELEMENT (e, sParameterValue, '@#@').EXTRACT ('//text()')).GetStringVal (), '@#@')
        into olTreeReloadList(i).sDisplay 
        from table(olTreeReloadList(i).tloValueList);
    end if;
    
    if csNodeType like 'REPORT_%' then  
      olTreeReloadList(i).sTooltip := checkIsInTree(olTreeReloadList(i).sDisplay, STB$SECURITY.getCurrentUser);
      select count(REGEXP_SUBSTR (olTreeReloadList(i).sDisplay
                      , '[^@#@]+'
                      , 1
                      , LEVEL))
        into olTreeReloadList(i).sDefaultAction
        from DUAL
      connect by REGEXP_SUBSTR (olTreeReloadList(i).sDisplay
                              , '[^@#@]+'
                              , 1
                              , LEVEL) is not null;  
      isr$trace.debug('olTreeReloadList('||i||').sTooltip isInTree', olTreeReloadList(i).sTooltip, sCurrentName);                        
      isr$trace.debug('olTreeReloadList('||i||').sDisplay displayPath sOldDisplayPath', olTreeReloadList(i).sDisplay||' '||sOldDisplayPath, sCurrentName);
      isr$trace.debug('olTreeReloadList('||i||').sDefaultAction level sOldLevelNumber', olTreeReloadList(i).sDefaultAction||' '||sOldLevelNumber, sCurrentName);
      isr$trace.debug('olTreeReloadList('||i||').sIcon sOldIcon', olTreeReloadList(i).sIcon||' '||sOldIcon, sCurrentName);
      
      if i>1 and olTreeReloadList(i).sNodeId = olTreeReloadList(i-1).sNodeId then
        olTreeReloadList(i-1).sDisplay := 'TO_DELETE';
        isr$trace.debug('olTreeReloadList.delete ('||i||'-1)', 'in logclob', sCurrentName, olTreeReloadList (olTreeReloadList.COUNT ())); 
      end if;
      
      if i>1  and olTreeReloadList(i).sTooltip = 'T' -- check is in tree 
              and olTreeReloadList(i).sDisplay like sOldDisplayPath || '%' 
              and to_number(sOldLevelNumber) < to_number(olTreeReloadList(i).sDefaultAction)
              and olTreeReloadList(i).sIcon = sOldIcon
              and olTreeReloadList(i).sNodeType = 'TITLE' then
        olTreeReloadList(i-1).sDisplay := 'TO_DELETE';
        isr$trace.debug('olTreeReloadList.delete ('||i||'-1)', 'in logclob', sCurrentName, olTreeReloadList (olTreeReloadList.COUNT ())); 
      end if;  
      
      sOldDisplayPath := olTreeReloadList(i).sDisplay;
      sOldLevelNumber := olTreeReloadList(i).sDefaultAction;                            
    end if;
    
    if csNodeType in ('DOCUMENT','RAWDATA','COMMONDOC','REMOTESNAPSHOT','LINK') 
      or csNodeType like 'DOCUMENT_%'
      or csNodeType like '%_REPORT_FILE' then
      -- delete all nodes except for the father node
      isr$trace.debug('olTreeReloadList('||i||').sNodetype', olTreeReloadList(i).sNodetype, sCurrentName, olTreeReloadList(i));
      isr$trace.debug('STB$OBJECT.getCurrentNodeId(-1)', STB$OBJECT.getCurrentNodeId(-1), sCurrentName);
      isr$trace.debug('olTreeReloadList(i).tloValueList(olTreeReloadList(i).tloValueList.count()).sNodeId', olTreeReloadList(i).tloValueList(olTreeReloadList(i).tloValueList.count()).sNodeId, sCurrentName);
      if olTreeReloadList(i).sNodetype like 'REPORT_%'
          and olTreeReloadList(i).tloValueList(olTreeReloadList(i).tloValueList.count()).sNodeId != STB$OBJECT.getCurrentNodeId(-1) then
        olTreeReloadList(i).sDisplay := 'TO_DELETE';
      end if;
    end if;
    
    --ABBIS-324: update only nodes with children, i.e. those that are or were open
    sNodeId := olTreeReloadList(i).sNodeid;
    for rGetCountChildren in (select count(*) cnt from isr$tree$nodes where parentnodeid = sNodeId) loop
      isr$trace.debug('rGetCountChildren',  rGetCountChildren.cnt, sCurrentName);
      if rGetCountChildren.cnt < 1 then
        olTreeReloadList(i).sDisplay := 'TO_DELETE';
      end if;
    end loop;
    
  end loop;
  if csNodeType like 'REPORT_%'
      or csNodeType in ('DOCUMENT','RAWDATA','COMMONDOC','REMOTESNAPSHOT','LINK') 
      or csNodeType like 'DOCUMENT_%'
      or csNodeType like '%_REPORT_FILE' then
    select STB$TREENODE$RECORD (SDISPLAY
                            , SPACKAGETYPE
                            , SNODETYPE
                            , SNODEID
                            , SICON
                            , SDEFAULTACTION
                            , TLOPROPERTYLIST
                            , TLOVALUELIST
                            , nDisplayType
                            , clPropertyList
                            , STOOLTIP
                           )
      bulk collect into olTreeReloadList1
      from table(olTreeReloadList)
     where sDisplay != 'TO_DELETE';
    olTreeReloadList := olTreeReloadList1;
  end if;

  --ABBIS-324: only scroll to the report if it was created in an own session.
  sIsOwnSession := case when stb$security.getCurrentSession = csSessionId then 'T' else 'F' end;
  isr$trace.debug('sIsOwnSession', stb$security.getCurrentSession || ' ' || sIsOwnSession, sCurrentName);
  
  
  if oTreeReloadNode is not null then
    if oTreeReloadNode.TLOPROPERTYLIST is null then
      oTreeReloadNode.TLOPROPERTYLIST := STB$PROPERTY$LIST();
    end if;
    
    oTreeReloadNode.TLOPROPERTYLIST.extend;
    oTreeReloadNode.TLOPROPERTYLIST(oTreeReloadNode.TLOPROPERTYLIST.count) := STB$PROPERTY$RECORD('OWNSESSION', 'OWNSESSION', sIsOwnSession);
    
    isr$trace.debug('oTreeReloadNode', 's. logclob', sCurrentName, oTreeReloadNode);
  end if;
  
  for i in 1..olTreeReloadList.COUNT () loop
    --olPropertyList := olTreeReloadList(i).TLOPROPERTYLIST;
    olTreeReloadList(i).TLOPROPERTYLIST.extend;
    olTreeReloadList(i).TLOPROPERTYLIST(olTreeReloadList(i).TLOPROPERTYLIST.count) := STB$PROPERTY$RECORD('OWNSESSION', 'OWNSESSION', sIsOwnSession);
   -- olTreeReloadList(i).TLOPROPERTYLIST := olPropertyList;
  end loop;
  
  isr$trace.debug('final olTreeReloadList ('||olTreeReloadList.COUNT ()||')', 'in logclob', sCurrentName, olTreeReloadList);
  isr$trace.stat('end', 'end', sCurrentName);

  return (oErrorObj);
exception
  when others then
    sUserMsg := utd$msglib.getmsg ('exUnexpectedError', stb$security.getCurrentLanguage,  csP1 => SQLERRM(SQLCODE));
    sImplMsg := sCurrentName || RTRIM(': '||sImplMsg,': ');
    sDevMsg  := SUBSTR(DBMS_UTILITY.format_error_stack ||stb$typedef.crlf|| DBMS_UTILITY.format_error_backtrace, 1,4000);
    oErrorObj.handleError ( Stb$typedef.cnSeverityCritical, sUserMsg, sImplMsg, sDevMsg );
    return oErrorObj;
end synchronizeInBackground;

--******************************************************************************
procedure sendSyncCall(csNodeType in VARCHAR2 default STB$OBJECT.getCurrentNodeType, csNodeId in VARCHAR2 default STB$OBJECT.getCurrentNodeId)
is
  sCurrentName   constant varchar2(4000) := $$PLSQL_UNIT||'.sendSyncCall(csNodeType=>'||csNodeType||', csNodeId=>'||csNodeId||')';   -- vs [ISRC-1152]
  sResult        VARCHAR2(4000);
  sNodeId        VARCHAR2(100);

  cursor cGetClientInfo is
    select distinct s.ip, TO_CHAR (s.port) port--, case when force_nodetype = 'T' then ts.nodetype else csNodeType end nodetype
      from isr$session s, (select distinct sessionid, ts1.nodetype--, NVL(td.force_nodetype, 'F') force_nodetype
                             from isr$tree$sessions ts1, isr$tree$dependencies td
                            where ts1.nodetype = td.dependend_type
                              and td.nodetype = csNodeType
                              and (ts1.sessionid != STB$SECURITY.getCurrentSession and ownsession = 'F' or ownsession = 'T')) ts
     where NVL (s.markedForShutdown, 'F') = 'F'
       and NVL (s.inactive, 'F') = 'F'
       and s.sessionid = ts.sessionid;
begin
  isr$trace.stat('begin', 'begin', sCurrentName);

  for rGetFrontendSessions in cGetClientInfo loop
    --ISR$TRACE.debug('frontends', rGetFrontendSessions.ip||':'||rGetFrontendSessions.port||' session '|| STB$SECURITY.getCurrentSession, 'nodetype '||rGetFrontendSessions.nodetype, sCurrentName);
    ISR$TRACE.debug('frontends', rGetFrontendSessions.ip||':'||rGetFrontendSessions.port ||' csNodeType '||csNodeType, sCurrentName);
    sResult := ISR$LOADER.updateTree(rGetFrontendSessions.ip, rGetFrontendSessions.port
                                  , sLoaderContext
                                  , sLoaderNamespace
                                  , STB$UTIL.getSystemParameter('TIMEOUT_LOADER_THREAD')
                                  , csNodeType
                                  --, rGetFrontendSessions.nodetype
                                  , csNodeId
                                  , STB$SECURITY.getCurrentSession);
    ISR$TRACE.debug('sResult', sResult, sCurrentName);
  end loop;

  isr$trace.stat('end', 'end', sCurrentName);
end;

-- *****************************************************************************
function getCustomNodeBranch return varchar2
is
  sCurrentName              constant VARCHAR2(100) := $$PLSQL_UNIT||'.getCustomNodeBranch';
  sCustomNodeBranch         VARCHAR2(500);

  cursor cGetCustomNodeBranch is
    select sNodeType
      from table(STB$OBJECT.getCurrentValueList)
     where sNodeType like 'CUSTOM%';
begin
  isr$trace.stat('begin','begin', sCurrentName);

  open cGetCustomNodeBranch;
  fetch cGetCustomNodeBranch into sCustomNodeBranch;
  close cGetCustomNodeBranch;
  ISR$TRACE.debug('sCustomNodeBranch', sCustomNodeBranch, sCurrentName);
  
  if TRIM(sCustomNodeBranch) is null then
    sCustomNodeBranch := 'CUSTOMMYNODE';
  end if;

  isr$trace.stat('end','return '||sCustomNodeBranch, sCurrentName);

  return sCustomNodeBranch;

end;

--*********************************************************************************************************************************
function checkLeftAsRight(oCurrentNode in STB$TREENODE$RECORD)
  return VARCHAR2
is
  sCurrentName   constant varchar2(100) := $$PLSQL_UNIT||'.checkLeftAsRight';
  sReturn        varchar2(1) := 'T';

  cursor cCheckLeftAsRight is
    select left_as_right
      from isr$tree$sessions
     where sessionid = STB$SECURITY.getCurrentSession
       and nodeid = oCurrentNode.sNodeId
       and nodetype = oCurrentNode.sNodeType;
begin
  isr$trace.stat('begin', 'oCurrentNode.sNodetype: '||oCurrentNode.sNodetype||' oCurrentNode.sNodeid: '||oCurrentNode.sNodeid, sCurrentName);

  open cCheckLeftAsRight;
  fetch cCheckLeftAsRight into sReturn;
  if cCheckLeftAsRight%notfound then
    sReturn := 'T';
  end if;
  close cCheckLeftAsRight;

  isr$trace.stat('end', 'end: '||sReturn, sCurrentName);
  return sReturn;

end checkLeftAsRight;

--*********************************************************************************************************************************
function checkFirstLoad(oCurrentNode in STB$TREENODE$RECORD)
  return VARCHAR2
is
  sCurrentName   constant varchar2(100) := $$PLSQL_UNIT||'.checkFirstLoad';
  sReturn        varchar2(1) := 'T';

  cursor cCheckFirstLoad is
    select first_load
      from isr$tree$sessions
     where sessionid = STB$SECURITY.getCurrentSession
       and nodeid = oCurrentNode.sNodeId
       and nodetype = oCurrentNode.sNodeType;
begin
  isr$trace.stat('begin', 'oCurrentNode.sNodetype: '||oCurrentNode.sNodetype||' oCurrentNode.sNodeid: '||oCurrentNode.sNodeid, sCurrentName);

  open cCheckFirstLoad;
  fetch cCheckFirstLoad into sReturn;
  if cCheckFirstLoad%notfound then
    sReturn := 'T';
  end if;
  close cCheckFirstLoad;

  isr$trace.stat('end', 'end: '||sReturn, sCurrentName);
  return sReturn;

end checkFirstLoad;

--*********************************************************************************************************************************
function loadTree(oCurrentNode in STB$TREENODE$RECORD, csLeftSide in varchar2 default 'T')
  return STB$TREENODELIST
is
  exNodeNotFound exception;
  olNodeList     STB$TREENODELIST;
  oErrorObj      STB$OERROR$RECORD := STB$OERROR$RECORD();
  sCurrentName   constant varchar2(100) := $$PLSQL_UNIT||'.loadTree';
  sUserMsg       varchar2(4000);
  sDevMsg        varchar2(4000);
  sImplMsg       varchar2(4000);
  nStartTimeNum  number      := DBMS_UTILITY.GET_TIME;
  sLeftSide      varchar2(1);
begin
  isr$trace.stat('begin', 'csLeftSide: '||csLeftSide||' oCurrentNode.sNodetype: '||oCurrentNode.sNodetype||' oCurrentNode.sNodeid: '||oCurrentNode.sNodeid, sCurrentName);

  /*sLeftSide := case when checkLeftAsRight(oCurrentNode) = 'T' then 'T' else csLeftSide end;

  select STB$TREENODE$RECORD (SDISPLAY
                            , SPACKAGETYPE
                            , SNODETYPE
                            , SNODEID
                            , SICON
                            , SDEFAULTACTION
                            , TLOPROPERTYLIST
                            , TLOVALUELIST
                            , nDisplayType
                            , clPropertyList
                            , STOOLTIP
                           )
    bulk collect
    into olNodeList
    from (select display SDISPLAY
               , packagename SPACKAGETYPE
               , nodetype SNODETYPE
               , nodeid SNODEID
               , icon SICON
               , defaultaction SDEFAULTACTION
               , propertylist TLOPROPERTYLIST
               , valuelist TLOVALUELIST
               , displaytype nDisplayType
               , EMPTY_CLOB() clPropertyList
               , tooltip STOOLTIP
            from ISR$TREE
           where parentnodetype = oCurrentNode.sNodetype
             and parentnodeid = oCurrentNode.sNodeid
             and deleted = 'F'
             and leftSide = sLeftSide
           order by ordernumber);

  if (    olNodeList is null
       or olNodeList.COUNT() = 0  ) then
    raise exNodeNotFound;
  end if;*/

  --isr$trace.debug('status','olNodeList.count: '||olNodeList.count, sCurrentName, sys.anydata.convertCollection(olNodeList));
  isr$trace.stat('end', 'end', sCurrentName);
  return olNodeList;
exception
  when exNodeNotFound then
    sDevMsg  := SUBSTR(DBMS_UTILITY.format_error_stack ||stb$typedef.crlf|| DBMS_UTILITY.format_error_backtrace, 1,4000);
    sUserMsg := utd$msglib.getmsg ('exNodeNotFound', stb$security.getCurrentLanguage, csP1 => SQLERRM(SQLCODE));
    sImplMsg := sCurrentName || RTRIM(': '||sImplMsg,': ');
    oErrorObj.handleError ( Stb$typedef.cnSeverityCritical, sUserMsg, sImplMsg, sDevMsg );
    raise;
  when others then
    sUserMsg :=  utd$msglib.getmsg ('exUnexpectedError', stb$security.getCurrentLanguage,  csP1 => SQLERRM(SQLCODE));
    sImplMsg := sCurrentName || RTRIM(': '||sImplMsg,': ');
    sDevMsg  := SUBSTR(DBMS_UTILITY.format_error_stack ||stb$typedef.crlf|| DBMS_UTILITY.format_error_backtrace, 1,4000);
    oErrorObj.handleError ( Stb$typedef.cnSeverityCritical, sUserMsg, sImplMsg, sDevMsg );
    raise;
end loadTree;

--*********************************************************************************************************************************
procedure saveTree(olNodeList in STB$TREENODELIST, oCurrentNode in STB$TREENODE$RECORD, csLeftSide in varchar2 default 'T')
is
  pragma autonomous_transaction;

  oErrorObj     STB$OERROR$RECORD := STB$OERROR$RECORD();
  sCurrentName  constant varchar2(100) := $$PLSQL_UNIT||'.saveTree('||oCurrentNode.sNodeType||')';
  sUserMsg      varchar2(4000);
  sDevMsg       varchar2(4000);
  sImplMsg      varchar2(4000);
  csJobName     varchar2(30);
  nStartTimeNum number      := DBMS_UTILITY.GET_TIME;
  olLoadNodeList  STB$TREENODELIST;

begin

  /*if checkFirstLoad(oCurrentNode) = 'T' then
  
    isr$trace.stat('begin','csLeftSide: '||csLeftSide||',  csApplicationCall: '||csLeftSide,sCurrentName);
    isr$trace.debug('value','oCurrentNode',sCurrentName,oCurrentNode);
--  
--    BEGIN
--      olLoadNodeList := loadTree(oCurrentNode, csLeftSide);
--    EXCEPTION WHEN OTHERS THEN
--      olLoadNodeList := STB$TREENODELIST();
--    END ;
--    
--    isr$trace.debug('olLoadNodeList',olLoadNodeList.count,sCurrentName,olLoadNodeList);
--    isr$trace.debug('olNodeList',olNodeList.count,sCurrentName,olNodeList);
--    
--    if (STB$JAVA.calculateHash(xmltype(sys.anydata.convertCollection(olLoadNodeList)).getClobVal) = 
--        STB$JAVA.calculateHash(xmltype(sys.anydata.convertCollection(olNodeList)).getClobVal)) then
--    
--      isr$trace.debug('tree table == nodelist','olNodelist.count: '||olNodeList.count, sCurrentName);
--    
--    else  
    
      isr$trace.debug('tree table != nodelist','olNodelist.count: '||olNodeList.count, sCurrentName);

      update ISR$TREE
         set deleted = 'T'
       where parentnodetype = oCurrentNode.sNodetype
         and parentnodeid = oCurrentNode.sNodeid
         and leftside = csLeftSide
         and (nodetype, nodeid) not in (select SNODETYPE, SNODEID from table (olNodeList));
      isr$trace.debug('after update isr$tree', 'rowcount= '||SQL%ROWCOUNT ,sCurrentName);

      merge into ISR$TREE t
      using (select SDISPLAY display
                  , SPACKAGETYPE packagename
                  , SNODETYPE nodetype
                  , SNODEID nodeid
                  , oCurrentNode.sNodetype parentnodetype
                  , oCurrentNode.sNodeid parentnodeid
                  , csleftSide leftSide
                  , SICON icon
                  , SDEFAULTACTION defaultaction
                  , STOOLTIP tooltip
                  , nDisplayType displayType
                  , tlopropertyList propertyList
                  --, clPropertyList
                  , tloValueList valueList
                  , rownum ordernumber
                  , 'F' deleted
               from table (olNodeList)) d
      on (t.nodetype = d.nodetype
      and t.nodeid = d.nodeid
      and t.leftside = d.leftside
      )
      when not matched then
         insert (t.display
               , t.packagename
               , t.nodetype
               , t.nodeid
               , t.parentnodetype
               , t.parentnodeid
               , t.leftSide
               , t.icon
               , t.defaultaction
               , t.tooltip
               , t.displayType
               , t.propertyList
               --, t.clPropertyList
               , t.valueList
               , t.ordernumber)
         values (d.display
               , d.packagename
               , d.nodetype
               , d.nodeid
               , d.parentnodetype
               , d.parentnodeid
               , d.leftSide
               , d.icon
               , d.defaultaction
               , d.tooltip
               , d.displayType
               , d.propertyList
               --, d.clPropertyList
               , d.valueList
               , d.ordernumber)
      when matched then
        update set t.display = d.display
                 , t.packagename = d.packagename
                 , t.parentnodetype = d.parentnodetype
                 , t.parentnodeid = d.parentnodeid
                 , t.icon = d.icon
                 , t.defaultaction = d.defaultaction
                 , t.tooltip = d.tooltip
                 , t.displayType = d.displayType
                 , t.propertyList = d.propertyList
                 --, t.clPropertyList = d.clPropertyList
                 , t.valueList = d.valueList
                 , t.ordernumber = d.ordernumber
                 , t.deleted = d.deleted;
      isr$trace.debug('after merge isr$tree', 'rowcount= '||SQL%ROWCOUNT ,sCurrentName);

      isr$trace.stat('end','end',sCurrentName);
      commit;
    
--  end if;

    end if;*/

    null;

exception
  when others then
    rollback;
    sUserMsg := utd$msglib.getmsg ('exUnexpectedError', stb$security.getCurrentLanguage,  csP1 => SQLERRM(SQLCODE));
    sImplMsg := sCurrentName || RTRIM(': '||sImplMsg,': ');
    sDevMsg  := SUBSTR(DBMS_UTILITY.format_error_stack ||stb$typedef.crlf|| DBMS_UTILITY.format_error_backtrace, 1,4000);
    oErrorObj.handleError ( Stb$typedef.cnSeverityCritical, sUserMsg, sImplMsg, sDevMsg );
end saveTree;

--*********************************************************************************************************************************
procedure saveTreeSessions(oCurrentNode in STB$TREENODE$RECORD, csLeftSide in varchar2, csSelected in varchar2, csLeft_As_Right in varchar2 default 'U' /*unknown*/)
is
  pragma autonomous_transaction;

  oErrorObj     STB$OERROR$RECORD := STB$OERROR$RECORD();
  sCurrentName  constant varchar2(100) := $$PLSQL_UNIT||'.saveTreeSessions('||oCurrentNode.sNodeType||')';
  sUserMsg      varchar2(4000);
  sDevMsg       varchar2(4000);
  sImplMsg      varchar2(4000);
  nStartTimeNum number      := DBMS_UTILITY.GET_TIME;
begin
  isr$trace.stat('begin', 'csSelected: '||csSelected, sCurrentName);
  isr$trace.debug('oCurrentNode', 'Nodeid '||oCurrentNode.sNodeid||' - '||stb$security.getCurrentUser, sCurrentName, oCurrentNode);
  
  -- columns NODEID, NODETYPE and SESSIONID in the table ISR$TREE$SESSIONS are not null columns. So that there is no error, first check it: 
  if oCurrentNode.sNodeid is null or oCurrentNode.sNodetype is null or STB$SECURITY.getCurrentSession is null then
    return;
  end if;
  
  if oCurrentNode.sNodetype = 'TOPNODE' then
    delete from isr$tree$sessions
     where sessionid = STB$SECURITY.getCurrentSession;  
  end if;  
  
  update isr$tree$sessions
     set selected = 'F', left_as_right = case when left_as_right = 'J' then 'U' else left_as_right end
   where (nodetype, nodeid) not in
               (select oCurrentNode.sNodetype
                     , oCurrentNode.sNodeid
                  from DUAL)
     and selected = 'T'
     and sessionid = STB$SECURITY.getCurrentSession;

  merge into ISR$TREE$SESSIONS ts
  using (select oCurrentNode.sNodetype nodetype
              , oCurrentNode.sNodeid nodeid
              , STB$SECURITY.getCurrentSession sessionid
              , csLeftSide leftside
              , csSelected selected
              , csLeft_As_Right left_as_right
           from DUAL) d
  on (ts.nodetype = d.nodetype
  and ts.nodeid = d.nodeid
  and ts.sessionid = d.sessionid)
  when not matched then
     insert (ts.nodetype
           , ts.nodeid
           , ts.sessionid
           , ts.leftside
           , ts.selected
           , ts.left_as_right
           , first_load)
     values (d.nodetype
           , d.nodeid
           , d.sessionid
           , d.leftside
           , d.selected
           , d.left_as_right
           , 'T')
  when matched then
    update set ts.selected = d.selected
      , ts.leftside = d.leftside
      , ts.left_as_right = d.left_as_right
      , first_load = 'F';

  isr$trace.stat('end', 'end', sCurrentName);
  commit;

exception
  when others then
    rollback;
    sUserMsg := utd$msglib.getmsg ('exUnexpectedError', stb$security.getCurrentLanguage,  csP1 => SQLERRM(SQLCODE));
    sImplMsg := sCurrentName || RTRIM(': '||sImplMsg,': ');
    sDevMsg  := SUBSTR(DBMS_UTILITY.format_error_stack ||stb$typedef.crlf|| DBMS_UTILITY.format_error_backtrace, 1,4000);
    oErrorObj.handleError ( Stb$typedef.cnSeverityCritical, sUserMsg, sImplMsg, sDevMsg );
end saveTreeSessions;

--*********************************************************************************************************************************
procedure saveTreeNode(oCurrentNode in STB$TREENODE$RECORD)
is
  pragma autonomous_transaction;

  oErrorObj     STB$OERROR$RECORD := STB$OERROR$RECORD();
  sCurrentName  constant varchar2(100) := $$PLSQL_UNIT||'.saveTreeNode('||oCurrentNode.sNodeType||')';
  sUserMsg      varchar2(4000);
  sDevMsg       varchar2(4000);
  sImplMsg      varchar2(4000);
  csJobName     varchar2(30);
  nStartTimeNum number      := DBMS_UTILITY.GET_TIME;
  tloValueList  ISR$VALUE$LIST;
  oValueRec     ISR$VALUE$RECORD;
  sParentNodeId   varchar2(255) := null;

  sFound          varchar2(1) := 'F';

  cursor cCheckIsInTable is
    select 'T'
      from isr$tree$nodes
     where nodeid = oCurrentNode.sNodeId;

begin

    --isr$trace.stat('begin','begin',sCurrentName);

    open cCheckIsInTable;
    fetch cCheckIsInTable into sFound;
    if cCheckIsInTable%notfound then
      sFound := 'F';
    end if;
    close cCheckIsInTable;

    if sFound = 'F' then

      tloValueList := oCurrentNode.tloValueList;
      oValueRec := tloValueList(tloValueList.COUNT);
      tloValueList.trim();
      sParentNodeId := STB$OBJECT.calculateHash (tloValueList)||'#@#'||STB$SECURITY.getCurrentUser;
      if (sParentNodeId is not null and oCurrentNode.sNodeId != sParentNodeId) then
        insert into isr$tree$nodes (nodeid
                                  , nodetype
                                  , packagename
                                  , parentnodeid
                                  , parameterName
                                  , parametervalue
                                  , clearnodeid
                                  , icon
                                  , userno)
        values (oCurrentNode.sNodeId
              , oCurrentNode.sNodeType
              , oCurrentNode.sPackageType
              , sParentNodeId
              , oValueRec.sParameterName
              , oValueRec.sParameterValue
              , oValueRec.sNodeId
              , oCurrentNode.sIcon
              , STB$SECURITY.getCurrentUser);
        commit;
      end if;
    end if;
    --isr$trace.stat('end','end',sCurrentName);

exception
  when others then
    rollback;
    sUserMsg := utd$msglib.getmsg ('exUnexpectedError', stb$security.getCurrentLanguage,  csP1 => SQLERRM(SQLCODE));
    sImplMsg := sCurrentName || RTRIM(': '||sImplMsg,': ');
    sDevMsg  := SUBSTR(DBMS_UTILITY.format_error_stack ||stb$typedef.crlf|| DBMS_UTILITY.format_error_backtrace, 1,4000);
    oErrorObj.handleError ( Stb$typedef.cnSeverityCritical, sUserMsg, sImplMsg, sDevMsg );
end saveTreeNode;

--*********************************************************************************************************************************
procedure saveTreeNodeList(olNodeList in STB$TREENODELIST, sAsSql in Clob)
is
  pragma autonomous_transaction;

  oErrorObj     STB$OERROR$RECORD := STB$OERROR$RECORD();
  sCurrentName  constant varchar2(100) := $$PLSQL_UNIT||'.saveTreeNodeList('||olNodeList.Count()||')';
  sUserMsg      varchar2(4000);
  sDevMsg       varchar2(4000);
  sImplMsg      varchar2(4000);
  csJobName     varchar2(30);
  nStartTimeNum number      := DBMS_UTILITY.GET_TIME;
  oValueRec     ISR$VALUE$RECORD;
  sParentNodeId   varchar2(255) := null;
  sCurrentUser    NUMBER;

begin

  isr$trace.stat('begin','begin',sCurrentName);

  sParentNodeId := STB$OBJECT.getCurrentNode().sNodeId;
  if (sParentNodeId is not null) then
    delete from isr$tree$nodes
     where parentNodeId = sParentNodeId;
     
    execute immediate sAsSql using in STB$OBJECT.getCurrentNode().tloValueList;
    /* 
    sCurrentUser := STB$SECURITY.getCurrentUser;

    insert into isr$tree$nodes (nodeid
                              , nodetype
                              , packagename
                              , parentnodeid
                              , parameterName
                              , parametervalue
                              , clearnodeid
                              , icon
                              , userno)
      (select nl.sNodeId
            , nl.sNodeType
            , nl.sPackageType
            , sParentNodeId
            , vl.sParameterName
            , vl.sParameterValue
            , vl.sNodeId
            , nl.sIcon
            , sCurrentUser
         from table (olNodeList) nl, table(nl.tloValueList) vl
        where nl.sNodeType = vl.sNodeType
          and nl.sNodeid != sParentNodeId);
    */
    commit;
  end if;

  isr$trace.stat('end','end',sCurrentName);

exception
  when others then
    rollback;
    sUserMsg := utd$msglib.getmsg ('exUnexpectedError', stb$security.getCurrentLanguage,  csP1 => SQLERRM(SQLCODE));
    sImplMsg := sCurrentName || RTRIM(': '||sImplMsg,': ');
    sDevMsg  := SUBSTR(DBMS_UTILITY.format_error_stack ||stb$typedef.crlf|| DBMS_UTILITY.format_error_backtrace, 1,4000);
    oErrorObj.handleError ( Stb$typedef.cnSeverityCritical, sUserMsg, sImplMsg, sDevMsg );
end saveTreeNodeList;


end ISR$TREE$PACKAGE;