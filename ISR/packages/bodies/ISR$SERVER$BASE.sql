CREATE OR REPLACE PACKAGE BODY "ISR$SERVER$BASE" is
  -- local variable to store the values of the current node"
 
  nCurrentLanguage   number  := stb$security.getcurrentlanguage;
  sJavaLogLevel      isr$loglevel.javaloglevel%type;
  crlf               constant varchar2(2) := chr(10)||chr(13); 

--******************************************************************************
function deleteServer(cnServerId in number, csToken in varchar2)  return  STB$OERROR$RECORD is
  sCurrentName constant varchar2(100) := $$PLSQL_UNIT||'.deleteServer';
  oErrorObj    STB$OERROR$RECORD   := STB$OERROR$RECORD();
  sRemoteEx    clob;
  sMsg         varchar2(4000);
  exRemoteServerDir   exception;
begin
  isr$trace.stat('begin', 'cnServerId=' || cnServerId, sCurrentName);
  for rGetServer in ( select observer_host, observer_port, servertypename, server_path, observer_runstatus, timeout, serverstatus, runstatus
                         from   isr$serverobserver$v s  
                         where  serverid = cnServerId) loop
                   
    if rGetServer.observer_runstatus is not null then
      if nvl(rGetServer.runstatus, 'F') = 'T' then
        sRemoteEx :=  stopRemoteServer (rGetServer.observer_host, rGetServer.observer_port,  cnServerId, sJavaLogLevel, rGetServer.timeout);                 
      end if;
      if nvl(rGetServer.serverstatus, -1) >= 0 then
        begin
          sRemoteEx :=  deleteServerDirectory (rGetServer.observer_host, rGetServer.observer_port,  rGetServer.server_path, sJavaLogLevel, rGetServer.timeout);
        exception when others then
          sMsg :=  utd$msglib.getmsg ('exRemoveDirectory', stb$security.getcurrentlanguage); 
          oErrorObj.handleError ( Stb$typedef.cnSeverityWarning, sMsg, sCurrentName,  dbms_utility.format_call_stack || CHR(10) || dbms_utility.format_error_backtrace); 
        end;
      end if;
      if sRemoteEx is not null and sRemoteEx != 'T' then
         isr$trace.debug ('error',  'see sRemoteEx in logclob column',  sCurrentName,  sRemoteEx);
         oErrorObj.handleJavaError (Stb$typedef.cnSeverityWarning, sRemoteEx, sCurrentName);  
         sMsg :=  utd$msglib.getmsg ('exRemoveDirectory', stb$security.getcurrentlanguage); -- 'Remove directory for new server failed';
         oErrorObj.handleError ( Stb$typedef.cnSeverityWarning, sMsg, sCurrentName,  dbms_utility.format_call_stack ); 
       --  raise exRemoteServerDir;
       end if;
     end if;
  end loop;  
      
  --delete from ISR$SERVER$ENTITY$ATTR$TRANSL where original_serverid = cnServerId;
  delete from ISR$SERVER$ENTITY$MAPPING where serverid = cnServerId;
  delete from ISR$SERVERPARAMETER where SERVERID = cnServerId;
  delete from ISR$SERVER where serverid = cnServerId; 
  --todo
  delete from STB$USERPARAMETER where PARAMETERNAME = 'SERVER_'||cnServerId;
  delete from ISR$DIALOG where TOKEN = 'CAN_MODIFY_USER_PARAMETER' and PARAMETERNAME = 'SERVER_'||cnServerId;
  delete from ISR$DIALOG where TOKEN = 'CAN_CREATE_NEW_REPORT' and PARAMETERNAME = 'SERVER_'||cnServerId;
  delete from STB$ADMINRIGHTS where PARAMETERNAME = 'SERVER_'||cnServerId;
  delete from STB$SYSTEMPARAMETER where PARAMETERNAME = 'SERVER_'||cnServerId;
  delete from STB$ADMINRIGHTS where PARAMETERNAME = 'PARA_DISPLAYED_SERVER_'||cnServerId;
  delete from STB$SYSTEMPARAMETER where PARAMETERNAME = 'PARA_DISPLAYED_SERVER_'||cnServerId;  
  
  ISR$TREE$PACKAGE.selectCurrent;
   
  oErrorObj := Stb$audit.openAudit ('SYSTEMAUDIT', To_Char (-1));
  oErrorObj := Stb$audit.AddAuditRecord ( csToken );       
  oErrorObj := Stb$audit.AddAuditEntry (cnServerId, 'Delete', STB$OBJECT.getCurrentNode().sDisplay, 1);
  oErrorObj := stb$system.getAuditType;
  
  ISR$TREE$PACKAGE.sendSyncCall('OBSERVER', STB$OBJECT.getCurrentNodeId);
    
  isr$trace.stat('end', 'server ' || cnServerId || ' deleted', sCurrentName);
  return oErrorObj;
exception
  when exRemoteServerDir then    
    sMsg :=  utd$msglib.getmsg ('exRemoveDirectory', stb$security.getcurrentlanguage); -- 'Remove directory for new server failed';
    oErrorObj.handleError ( Stb$typedef.cnSeverityWarning, sMsg, sCurrentName,  dbms_utility.format_call_stack ); 
    return oErrorObj; 
  when others then
    rollback;
    sMsg :=  utd$msglib.getmsg ('exUnexpectedError', nCurrentLanguage, csP1 => 'serverid='||cnServerId, csP2 => sqlerrm(sqlcode));
    oErrorObj.handleError ( Stb$typedef.cnSeverityCritical, sMsg, sCurrentName, 
                            substr(dbms_utility.format_error_stack || CHR(10) || dbms_utility.format_error_backtrace, 1,4000)  );  
    return oErrorObj;    
  end deleteServer;
 

--****************************************************************************************************
FUNCTION createServerDirectory(sServerId VARCHAR2) return varchar2 is
  sCurrentName     constant varchar2(100) := $$PLSQL_UNIT||'.createServerDirectory';
  oErrorObj        STB$OERROR$RECORD      := STB$OERROR$RECORD();
  sMsg             varchar2(4000);  
 cursor cGetCurrentServer  is
      select *
      from   isr$serverobserver$v
      where  serverid = sServerId;
  rGetServer          cGetCurrentServer%rowtype;  
  sRemoteEx        clob;
  sReturn varchar2(1) := 'F';
begin
 isr$trace.stat('begin', 'sServerId=' || sServerId, sCurrentName);
 open cGetCurrentServer;
 fetch cGetCurrentServer into rGetServer;
  sRemoteEx := isr$server$base.createServerDirectory( rGetServer.observer_host,
                                                      rGetServer.observer_port,
                                                      rGetServer.server_path,
                                                      isr$server$base.csJavaServerFolders,
                                                      ',',
                                                      isr$trace.getUserLoglevelStr, rGetServer.timeout);

  if sRemoteEx is not null and sRemoteEx != 'T' then
     isr$trace.debug ('error',  'see sRemoteEx in logclob column', sCurrentName,  sRemoteEx);
     oErrorObj.handleJavaError (Stb$typedef.cnSeverityWarning, sRemoteEx, sCurrentName);
     sMsg :=  utd$msglib.getmsg ('exCreatingDirectory', stb$security.getcurrentlanguage); -- 'Creating directory for new server failed';
     oErrorObj.handleError ( Stb$typedef.cnSeverityWarning, sMsg, sCurrentName,  dbms_utility.format_call_stack );  
  else
      sReturn := 'T';
  end if;
  close cGetCurrentServer;   
  isr$trace.stat('end', 'end', sCurrentName);
  return sReturn;
end createServerDirectory;
--******************************************************************************
function checkServerStatus return STB$OERROR$RECORD 
is
  sCurrentName constant varchar2(100) := $$PLSQL_UNIT||'.checkServerStatus';  
  oErrorObj    stb$oerror$record      := STB$OERROR$RECORD();
  sMsg         varchar2(4000);         

begin
  isr$trace.info('begin', 'begin', sCurrentName);
  
  isr$trace.info('end', 'end', sCurrentName);    
  return oErrorObj;
exception
  when others then
    sMsg := utd$msglib.getmsg('exOracleError', nCurrentLanguage );
    -- handleError includes logging, therefore no explicit trace message
    oErrorObj.handleError( STB$TYPEDEF.cnSeverityCritical, sMsg, sCurrentName,
                           substr(dbms_utility.format_error_stack || CHR(10) || dbms_utility.format_error_backtrace, 1,4000)); 
    return (oErrorObj);
end checkServerStatus;  

--**********************************************************************************************************************
function saveServerDialogData ( oParameter  in STB$MENUENTRY$RECORD, 
                                olParameter in STB$PROPERTY$LIST  ) return STB$OERROR$RECORD 
is
  sCurrentName constant varchar2 (100) := $$PLSQL_UNIT || '.saveServerDialogData (' || oParameter.sToken || ')';
  sKey1           ISR$DIALOG.PARAMETERNAME%type;
  sKeyValue1      ISR$DIALOG.PARAMETERVALUE%type; 
  sMsg            varchar2 (4000);
  sParameter      varchar2 (500);
  sOldAlias       varchar2 (4000);
  sNewAlias       varchar2 (4000);
  sOldValue       varchar2 (4000);
  sNewValue       varchar2 (4000);
  sStmt           varchar2 (4000);
  sEditable       varchar2 (1);
  sOldValueString varchar2 (4000);
  sNewValueString varchar2 (4000);
  nNewMdmMasterServerId number := 0;   -- 0 = no change (default)  
  oErrorObj       STB$OERROR$RECORD  := STB$OERROR$RECORD ();  

  cursor cGetUserCols (csParameter in varchar2, csTable in varchar2) is
     select 'T'
       from user_tab_cols
      where column_name = UPPER (csParameter) and table_name = UPPER (csTable);

  rGetUserCols  cGetUserCols%rowtype;

  cursor cGetParameterInfo (
     csToken             in varchar2,
     csParameter         in varchar2,
     csValue             in varchar2) is
     select utd$msglib.getmsg (description, nCurrentLanguage),
          --  case when haslov in ('T', 'W') then NVL (ISR$REPORTTYPE$PACKAGE.getSelectedLovDisplay (parametername, csValue, 'isr$server$base'), csValue) else csValue end,
          csValue,
          editable
       from isr$dialog
      where token = csToken and parametername = csParameter;
begin
  isr$trace.stat('begin','begin', sCurrentName);
  isr$trace.debug('parameter','oParameter',sCurrentName,oParameter);    
  isr$trace.debug('parameter','olParameter',sCurrentName,olParameter);     
  sKey1 := 'SERVERID';

  if sKeyValue1 is null then
     sKeyValue1 := STB$UTIL.getProperty (olParameter, 'SERVERID');
  end if;

  isr$trace.debug ('sKeyValue1', sKeyValue1, sCurrentName);

  -- try to insert the values if they exists an exception is raised
  begin
     if sKeyValue1 not in (0,1) then
       insert into isr$server (SERVERID) values (TO_NUMBER (sKeyValue1));
     end if;
  exception
     when others then
        null;
  end;

  -- create server parameter
  if (     upper (oParameter.sToken) = 'CAN_MODIFY_SERVER' and sKeyValue1 not in (0,1) ) then
     merge into ISR$SERVERPARAMETER a
     using ( select sParameter
             from   table (olParameter)
             where  type != 'TABSHEET'
             minus
             select column_name
             from   user_tab_columns
             where  table_name = 'isr$server' ) b   -- vs: Lowercase wird hier nie eintreten !
     on   (     a.parametername = b.sParameter 
            and a.serverid = sKeyValue1 )
     when not matched then
     insert (PARAMETERNAME, SERVERID)
     values (b.sParameter, sKeyValue1);
  end if;

  sOldValueString := null;
  sNewValueString := null;

  for i in 3 .. olParameter.count () loop
    isr$trace.debug('code position','olParameter.count: '||i||'/'||olParameter.count,sCurrentName);  

     -- handling of ISR$SERVERPARAMETER
     if olParameter (i).type not in ('TABSHEET', 'GROUP', 'BLOB') then
        isr$trace.debug ('code position', 'IF no TABSHEET/GROUP/BLOB', sCurrentName);
        isr$trace.debug ('olParameter(' || i || ').sParameter', olParameter (i).sParameter, sCurrentName);
        sParameter := olParameter (i).sParameter;
        sNewValue := olParameter (i).sValue;
        
        open cGetUserCols (sParameter, 'isr$server');
        fetch cGetUserCols into rGetUserCols;

        if cGetUserCols%notfound then
           isr$trace.debug ('code position', 'IF: cGetUserCols%notfound', sCurrentName);
           begin
              execute immediate
                 'SELECT parametervalue FROM ISR$SERVERPARAMETER WHERE parametername = ''' || sParameter || ''' and SERVERID = ''' || replace (sKeyValue1, '''', '''''') || ''''
                                 into sOldValue;
           exception
              when NO_DATA_FOUND then
                 sOldValue := '';
           end;
           isr$trace.debug ('old ' || sParameter, sOldValue, sCurrentName);
           
           if olParameter (i).TYPE like '%PASSWORD%' and TRIM (sNewValue) is not null and (sOldValue is null or sNewValue != sOldValue) then
             sNewValue := stb$java.encryptpassword (sNewValue);
           end if;
           isr$trace.debug ('new ' || sParameter, sNewValue, sCurrentName);
           
           if sKeyValue1 not in (0,1) then
             sStmt :=
                'UPDATE ISR$SERVERPARAMETER SET parametervalue = ''' || replace (sNewValue, '''', '''''') || ''' ' || 'WHERE parametername = ''' || sParameter || ''' and SERVERID = ''' ||
                sKeyValue1 || '''';
             isr$trace.debug ('statement', sStmt, sCurrentName);
  
             execute immediate sStmt;
           end if;  

        else
           isr$trace.debug ('code position', 'ELSE: cGetUserCols%notfound', sCurrentName);
           begin
              execute immediate 'SELECT ' || sParameter || ' FROM isr$server WHERE SERVERID = ''' || sKeyValue1 || ''''                 into sOldValue;
           exception
              when NO_DATA_FOUND then
                 sOldValue := '';
           end;

           isr$trace.debug ('old ' || sParameter, sOldValue, sCurrentName);
           if olParameter (i).TYPE like '%PASSWORD%' and TRIM (sNewValue) is not null and sNewValue != sOldValue then
             sNewValue := stb$java.encryptpassword (sNewValue);
           end if;
           isr$trace.debug ('new ' || sParameter, sNewValue, sCurrentName);
           sStmt := 'UPDATE isr$server SET ' || sParameter || ' = ''' || replace (sNewValue, '''', '''''') || ''' WHERE SERVERID = ''' || sKeyValue1 || '''';
           isr$trace.debug ('statement', sStmt, sCurrentName);

           execute immediate sStmt;

        end if;

        close cGetUserCols;

        if (UPPER (oParameter.sToken) like 'CAN_DELETE%' or (UPPER (oParameter.sToken) like 'CAN_MODIFY%' and NVL (sOldValue, '#@#') != NVL (sNewValue, '#@#'))) then
           isr$trace.debug ('code position', 'IF: #10', sCurrentName);
           open cGetParameterInfo (UPPER (oParameter.sToken), sParameter, sOldValue);
           fetch cGetParameterInfo into sParameter, sOldValue, sEditable;
           close cGetParameterInfo;

           isr$trace.debug ('sParameter', sParameter, sCurrentName);
           isr$trace.debug ('sOldValue', sOldValue, sCurrentName);
           isr$trace.debug ('sEditable', sEditable, sCurrentName);
           sOldValueString := SUBSTR (sOldValueString || sParameter || ':' || sOldValue || CHR (10), 0, 4000);
        end if;

        if (UPPER (oParameter.sToken) like 'CAN_ADD%' or (UPPER (oParameter.sToken) like 'CAN_MODIFY%' and NVL (sOldValue, '#@#') != NVL (sNewValue, '#@#'))) then
           isr$trace.debug ('code position', 'IF: #20', sCurrentName);
           open cGetParameterInfo (UPPER (oParameter.sToken), sParameter, sNewValue);
           fetch cGetParameterInfo into sParameter, sNewValue, sEditable;
           close cGetParameterInfo;

           isr$trace.debug ('sParameter', sParameter, sCurrentName);
           isr$trace.debug ('sNewValue', sNewValue, sCurrentName);
           isr$trace.debug ('sEditable', sEditable, sCurrentName);
           sNewValueString := SUBSTR (sNewValueString || sParameter || ':' || sNewValue || CHR (10), 0, 4000);

           if sParameter = 'ALIAS' then  -- required for MDM_MASTER
             sOldAlias := sOldValue;
             sNewAlias := sNewValue;
           end if;
           
           -- check MDM_MASTER [ISRC-814]
           case
             when sParameter = 'MDM_MASTER' 
             and  isr$util.isNumeric( sNewValue ) = 'T'  then     
               nNewMdmMasterServerId := sNewValue;
             when sParameter = 'MDM_MASTER' then
               nNewMdmMasterServerId := 0;  -- no change
             else
               null;
           end case;
          
        end if;
     end if;
  end loop;

  isr$trace.debug ('before general part', UPPER (oParameter.sToken), sCurrentName);
  
  if  upper( oParameter.sToken ) like 'CAN_MODIFY%' and nvl(nNewMdmMasterServerId,0) <> 0 then
    -- MDM_MASTER setting has changed
    if nNewMdmMasterServerId = -1 then
      -- disable settings for MDM_MASTER
      update isr$server$entity$mapping
      set    mdmmaster = 'F';
    else
      -- set MDM_MASTER
      update isr$server$entity$mapping
      set    mdmmaster = 'F'
      where  serverid <> nNewMdmMasterServerId;
      --
      update isr$server$entity$mapping
      set    mdmmaster = 'T'
      where  serverid = nNewMdmMasterServerId;
    end if;
  end if;
  
  if UPPER (oParameter.sToken) like 'CAN_DELETE%' then
     execute immediate 'DELETE FROM isr$server WHERE SERVERID = ''' || replace (sKeyValue1, '''', '''''') || '''';
  end if;

  if UPPER (oParameter.sToken) like 'CAN_ADD%' then
     isr$trace.debug ('sKeyValue1', sKeyValue1  , sCurrentName);
     ISR$TREE$PACKAGE.selectCurrent;
  end if;

  oErrorObj := Stb$audit.openAudit ('SYSTEMAUDIT', TO_CHAR (-1));
  oErrorObj := Stb$audit.AddAuditRecord (oParameter.sToken);

  open cGetParameterInfo (UPPER (oParameter.sToken), 'SERVERID', sKeyValue1);
  fetch cGetParameterInfo into sKey1, sKeyValue1, sEditable;
  close cGetParameterInfo;

  if UPPER (oParameter.sToken) like 'CAN_ADD_%' then
     oErrorObj :=
        Stb$audit.AddAuditEntry (STB$OBJECT.getCurrentNode ().sDisplay || ' [' || sKey1 || ':' || sKeyValue1 || ']',
                                 'Insert',
                                 RTRIM (sNewValueString, CHR (10)),
                                 1);
  elsif UPPER (oParameter.sToken) like 'CAN_MODIFY_%' then
     oErrorObj :=
        Stb$audit.AddAuditEntry (STB$OBJECT.getCurrentNode ().sDisplay || ' [' || sKey1 || ':' || sKeyValue1 || ']',
                                 RTRIM (sOldValueString, CHR (10)),
                                 RTRIM (sNewValueString, CHR (10)),
                                 1);
  elsif UPPER (oParameter.sToken) like 'CAN_DELETE_%' then
     oErrorObj :=
        Stb$audit.AddAuditEntry (STB$OBJECT.getCurrentNode ().sDisplay || ' [' || sKey1 || ':' || sKeyValue1 || ']',
                                 'Delete',
                                 RTRIM (sOldValueString, CHR (10)),
                                 1);
  end if;
  
  ISR$TREE$PACKAGE.sendSyncCall('OBSERVER', STB$OBJECT.getCurrentNodeId);
  isr$trace.stat('end','end', sCurrentName);
  return oErrorObj;
exception
  when others then
    sMsg :=  utd$msglib.getmsg ('exUnexpectedError', nCurrentLanguage, csP1 => sqlerrm(sqlcode));
    oErrorObj.handleError ( Stb$typedef.cnSeverityCritical, sMsg, sCurrentName, 
                            substr(dbms_utility.format_error_stack || CHR(10) || dbms_utility.format_error_backtrace, 1,4000)  );  
    return oErrorObj;    
end saveServerDialogData;
 
 

--*********************************************************************************************************************
function getServerParameter ( nServerID       in  number, 
                              sParameterName  in  varchar2, 
                              sParameterValue out varchar2 )  RETURN stb$oerror$record 
is
  sCurrentName constant varchar2(100) := $$PLSQL_UNIT||'.getServerParameter';  
  oerrorobj    stb$oerror$record      := STB$OERROR$RECORD();
  sMsg         varchar2(4000);
  sStm         varchar2(4000);

  -- cursor to retrieve the value of a single parametervalue
  cursor cGetServerParameter (nServerID in number, sParameterName in varchar2) is
    select parametervalue
    from   isr$serverparameter
    where  serverid      = nServerID
    and    parametername = sParameterName;
begin
  isr$trace.stat('begin', 'begin', sCurrentName);
  open cGetServerParameter (nServerID, sParameterName);
  fetch cGetServerParameter into sparametervalue;
  if cGetServerParameter%notfound then
    isr$trace.warn ('Parameter not found', nServerID||' '||sParameterName, sCurrentName);
  end if;
  close cGetServerParameter;
  if sParameterValue is null then
    begin
      sStm := 'select  ' || sParametername || ' from isr$server where serverid = ' || nserverid;   
      execute immediate sStm into sParameterValue;
    exception when others then
       isr$trace.warn ('Parameter not found', nServerID||' '||sParameterName, sCurrentName);
  end;
  end if;
  isr$trace.stat('end', 'end', sCurrentName);
  return oerrorobj;
exception
  when others then
    sMsg :=  utd$msglib.getmsg ('exUnexpectedError', nCurrentLanguage, csP1 => sqlerrm(sqlcode));
    oErrorObj.handleError ( Stb$typedef.cnSeverityCritical, sMsg, sCurrentName, 
                            substr(dbms_utility.format_error_stack || CHR(10) || dbms_utility.format_error_backtrace, 1,4000)  );  
    return oErrorObj;    
end getServerParameter;
  
--*********************************************************************************************************************  
function getServerParameter ( nServerID      in number, 
                              sParameterName in varchar2 ) return varchar2 
is    
  sCurrentName     constant varchar2(4000) := $$PLSQL_UNIT||'.getServerParameter('||nServerID||')';
  sParameterValue  stb$userparameter.parametervalue%type;
  oerrorobj        stb$oerror$record := STB$OERROR$RECORD();
begin
  isr$trace.info('begin', 'begin', sCurrentName);
  oerrorobj := getServerParameter( nServerID, sParameterName, sParameterValue);
  isr$trace.info('end', 'end', sCurrentName);
  return sParameterValue;
end getServerParameter;
  
--*****************************************************************************************************************************
function getAuditMask (olNodeList out STB$TREENODELIST) return STB$OERROR$RECORD 
is
  sCurrentName constant varchar2(100) := $$PLSQL_UNIT||'.getAuditMask';  
  oErrorObj    stb$oerror$record      := STB$OERROR$RECORD();
  sMsg         varchar2(4000);
begin
  isr$trace.info('begin', 'begin', sCurrentName);
  oErrorobj := Stb$audit.getAuditMask (olNodeList);
  isr$trace.info('end', 'end', sCurrentName);
  return (oErrorObj);
exception
  when others then
    rollback;
    sMsg :=  utd$msglib.getmsg ('exUnexpectedError', nCurrentLanguage, csP1 => sqlerrm(sqlcode));
    oErrorObj.handleError ( Stb$typedef.cnSeverityCritical, sMsg, sCurrentName, 
                            substr(dbms_utility.format_error_stack || CHR(10) || dbms_utility.format_error_backtrace, 1,4000)  );  
    return oErrorObj;    
end getAuditMask;
  
--*****************************************************************************************************
function putAuditMask (olParameter in STB$PROPERTY$LIST ) return STB$OERROR$RECORD 
is
  sCurrentName constant varchar2(100) := $$PLSQL_UNIT||'.putAuditMask';  
  oErrorObj    stb$oerror$record      := STB$OERROR$RECORD();
  sMsg         varchar2(4000);
begin
  isr$trace.info('begin', 'Function '||sCurrentName||' started.', sCurrentName);  
  oErrorObj := Stb$audit.putAuditMask (olParameter);
  isr$trace.info('end', 'end', sCurrentName);
  return (oErrorObj);
exception
  when others then
    rollback;
    sMsg :=  utd$msglib.getmsg ('exUnexpectedError', nCurrentLanguage, csP1 => sqlerrm(sqlcode));
    oErrorObj.handleError ( Stb$typedef.cnSeverityCritical, sMsg, sCurrentName, 
                            substr(dbms_utility.format_error_stack || CHR(10) || dbms_utility.format_error_backtrace, 1,4000)  );  
    return oErrorObj;    
end putAuditMask;
--*************************************************************************************************************************

function deleteFilesInDirectory( csObserverHost  in varchar2,  csObserverPort  in varchar2, csDirectoryName in varchar2,  csLoglevel  in varchar2, nTimeout NUMBER ) return clob
as language java
name 'com.uptodata.isr.observerclient.ObserverClient.deleteFilesInDirectory( java.lang.String, java.lang.String, java.lang.String, java.lang.String, int ) return java.sql.Clob';
 
 --****************************************************************************************************
function deployJavaOnServer(csObserverHost in varchar2, csObserverPort in varchar2, cnServertypeid number, csServerPath varchar2, csTarget varchar2, nTimeout NUMBER) return STB$OERROR$RECORD 
is
  sCurrentName constant varchar2(100) := $$PLSQL_UNIT||'.deployJavaOnServer';
  oErrorObj             STB$OERROR$RECORD := STB$OERROR$RECORD();
  sMsg                  varchar2(4000);
  sPlatformName    varchar2(32);
  sPlatformArch     varchar2(32);
  sRemoteEx              clob;
  oParamList     isr$Parameter$list;
  exRemoteSendFile    exception;
  exFilesNotFound    exception;
  
  cursor cGetDeployFiles(csPlatformName varchar2, csPlatformArch varchar2) is
    select fileid, filename,  binaryfile, java_version, unzip 
    from isr$server$deployment$v
    where  servertypeid=cnServertypeid 
  --  and  upper(csPlatformName) like upper('%' || platform_name|| '%')    
  --  and  upper(csPlatformArch) like upper('%' || platform_architecture || '%') 
    and target = csTarget;
   rGetDeployFiles1 cGetDeployFiles%ROWTYPE; 
begin
  isr$trace.stat('begin', 'csTarget= ' || csTarget || 'csServerPath='||csServerPath, sCurrentName, sRemoteEx);
  sJavaLogLevel := isr$trace.getJavaUserLoglevelStr;
  
  -- that does not work anymore because System.getProperty("sun.cpu.isalist") returns 
  -- 'pentium_pro+mmx pentium_pro pentium+mmx pentium i486 i386 i86' and not 'amd64'. 
  -- we have not used that so far anyway
  --sPlatformName := getObserverPlatformName(csObserverHost, csObserverPort, sJavaLogLevel, nTimeout);
 -- sPlatformArch := getObserverPlatformArch(csObserverHost, csObserverPort, sJavaLogLevel , nTimeout);
  
  isr$trace.debug('platform', sPlatformName|| ' ' ||sPlatformArch, sCurrentName, sRemoteEx);
 
  open cGetDeployFiles(sPlatformName, sPlatformArch);
  fetch cGetDeployFiles into rGetDeployFiles1;
  if cGetDeployFiles%notfound then
    sMsg := utd$msglib.getmsg('exNoDataFoundText', nCurrentLanguage, 'No deploy files for servertypeid "' || cnServertypeid || '",  target  "' || csTarget || '",  platform name "' || sPlatformName||'" and platform architecture "' ||sPlatformArch || '"');
    raise exFilesNotFound;
  end if;
  close cGetDeployFiles;
  
  -- test case for exception:  fileName := null;
  
  sRemoteEx := deleteFilesInDirectory(csObserverHost, csObserverPort,  csServerPath || '/' ||csTarget, sJavaLogLevel, nTimeout);
  if sRemoteEx is not null then      
      sMsg := utd$msglib.getmsg('exRemoteDeleteFileText', nCurrentLanguage,  csServerPath || '/' || csTarget );
      oErrorObj.handleJavaError( Stb$typedef.cnSeverityWarning,  sRemoteEx, sCurrentName );
  end if;
  for rGetDeployFiles in cGetDeployFiles(sPlatformName, sPlatformArch) loop
    isr$trace.debug('keyId', rGetDeployFiles.fileid, sCurrentName, sRemoteEx);
    --oErrorObj := isr$file$transfer$service.saveKeysForServlet('SERVER_DEPLOYMENT', rGetDeployFiles.fileid, sTransferKeyId);
    oParamList := isr$Parameter$list(iSR$Parameter$Rec('fileName', rGetDeployFiles.filename),
                               iSR$Parameter$Rec('target', csServerPath || '/' || csTarget),
                               iSR$Parameter$Rec('zipped', rGetDeployFiles.unzip),
                               iSR$Parameter$Rec('keyId', rGetDeployFiles.fileid),
                               iSR$Parameter$Rec('configName', 'SERVER_DEPLOYMENT'),
                               iSR$Parameter$Rec('logReference', isr$trace.getReference),
                               iSR$Parameter$Rec('logLevel', sJavaLogLevel),
                               iSR$Parameter$Rec('timeout', nTimeout)
                               );
    sRemoteEx := sendFileToServer(csObserverHost, csObserverPort,  oParamList);
    if sRemoteEx is not null then      
      isr$trace.warn(' error', 'error ', sCurrentName, sRemoteEx);
      sMsg := utd$msglib.getmsg('exRemoteSendFileText', nCurrentLanguage, rGetDeployFiles.filename,  csServerPath || '/' || csTarget );
      oErrorObj.handleJavaError( Stb$typedef.cnSeverityCritical,  sRemoteEx, sCurrentName );             
      raise exRemoteSendFile;
    end if; 
  end loop;
  isr$trace.stat('end', 'end', sCurrentName);
  return oErrorObj;
exception   
  when others then
    sMsg :=  utd$msglib.getmsg ('exUnexpectedError', nCurrentLanguage, csP1 => sqlerrm(sqlcode)) || sMsg;
    oErrorObj.handleError ( Stb$typedef.cnSeverityCritical, sMsg, sCurrentName, 
                            dbms_utility.format_error_stack || CHR(10) || dbms_utility.format_error_backtrace);  
    return oErrorObj; 
end deployJavaOnServer;


--**********************************************************************************************************************
function downloadLogServerFiles( oParameter  in STB$MENUENTRY$RECORD, 
                                 olParameter in STB$PROPERTY$LIST  ) 
  return STB$OERROR$RECORD 
is
  sCurrentName    constant varchar2 (100) := $$PLSQL_UNIT || '.downloadLogServerFiles (' || oParameter.sToken || ')';
  sUserMsg        varchar2(4000);
  sDevMsg         varchar2(4000);  
  sImplMsg        varchar2(4000);
  oErrorObj       STB$OERROR$RECORD  := STB$OERROR$RECORD ();  
  
  nOutputId                     ISR$REPORT$OUTPUT$TYPE.OUTPUTID%TYPE;
  nActionId                     ISR$ACTION.ACTIONID%TYPE;
  sFunction                     ISR$REPORT$OUTPUT$TYPE.ACTIONFUNCTION%TYPE;  
  sWhere                        VARCHAR2(4000);

begin
  isr$trace.stat('begin', 'Function '||sCurrentName||' started.', sCurrentName);
  isr$trace.debug('status oParameter', oParameter.toString('oParameter'), sCurrentName);  
 

  OPEN  stb$system.cGetJobIds ('CAN_DISPLAY_SERVER_LOG_LIST', null);
  FETCH stb$system.cGetJobIds
  INTO  nOutputId, nActionId, sFunction;
  CLOSE stb$system.cGetJobIds;
  
  if nActionId is null then
    sUserMsg := utd$msglib.getmsg ('exNotFindAction', stb$security.getcurrentlanguage, oParameter.sToken);
    raise stb$job.exNotFindAction;
  end if;  

  SELECT STB$JOB$SEQ.NEXTVAL
  INTO   STB$JOB.nJobid
  FROM   DUAL;

  oErrorObj := Stb$util.createFilterParameter (olParameter);
  
  STB$JOB.setJobParameter ('TOKEN', Upper (oParameter.sToken), STB$JOB.nJobid);
  STB$JOB.setJobParameter ('OUTPUTID', nOutputId, STB$JOB.nJobid);
  STB$JOB.setJobParameter ('ACTIONID', nActionId, STB$JOB.nJobid);
  STB$JOB.setJobParameter ('WHERE', sWhere, STB$JOB.nJobid);
  STB$JOB.setJobParameter ('REFERENCE', STB$OBJECT.getCurrentNodeId, STB$JOB.nJobid);
  STB$JOB.setJobParameter ('FUNCTION', sFunction, STB$JOB.nJobid);
  STB$JOB.setJobParameter ('TO', STB$UTIL.getProperty(olParameter, 'TO'), STB$JOB.nJobid);
  STB$JOB.setJobParameter ('ACTIONTYPE', 'SAVE', STB$JOB.nJobid);        
  STB$JOB.setJobParameter ('SERVERID',  STB$UTIL.getProperty(olParameter, 'SERVERID'), STB$JOB.nJobid);       
  STB$JOB.setJobParameter ('BEGINDATE',  STB$UTIL.getProperty(olParameter, 'BEGINDATE'), STB$JOB.nJobid);       
  STB$JOB.setJobParameter ('ENDDATE',   STB$UTIL.getProperty(olParameter, 'ENDDATE'), STB$JOB.nJobid);        
  oErrorObj := STB$JOB.startJob;      
     
  isr$trace.stat('end', 'Function '||sCurrentName||' finished.', sCurrentName);
  return oErrorObj;   
exception
  when stb$job.exNotFindAction then
    oErrorObj.handleError ( Stb$typedef.cnSeverityCritical, sUserMsg, sCurrentName, dbms_utility.format_error_stack ||stb$typedef.crlf|| dbms_utility.format_error_backtrace );
    rollback;
    return oErrorObj;
  when others then
    rollback;
    sUserMsg := utd$msglib.getmsg ('exUnexpectedError', nCurrentLanguage,  csP1 => sqlerrm(sqlcode));
    sImplMsg := sCurrentName || rtrim(': '||sImplMsg,': ');
    sDevMsg  := substr(dbms_utility.format_error_stack ||crlf|| dbms_utility.format_error_backtrace, 1,4000);
    oErrorObj.handleError ( Stb$typedef.cnSeverityCritical, sUserMsg, sImplMsg, sDevMsg );  
    return oErrorObj;  
end downloadLogServerFiles;
 
--****************************************************************************************************
procedure setServerStatus(cnServerId IN number, cnServerStatus IN number, csIsCommit in varchar2 default 'T') 
is
  --pragma autonomous_transaction; 
  sCurrentName     constant varchar2(4000) := $$PLSQL_UNIT||'.setServerStatus('||cnServerId||', ' || cnServerStatus || ')';
 -- oErrorObj        STB$OERROR$RECORD := STB$OERROR$RECORD();
begin
  isr$trace.stat('begin', 'begin', sCurrentName); 
  update isr$server set serverstatus = cnServerStatus  where serverid = cnServerId;
  if csIsCommit = 'T' then
    commit;
  end if; 
  isr$trace.stat('end', 'end', sCurrentName);
end setServerStatus;

--****************************************************************************************************
procedure switchServerStatus(cnServerId in number, csIsDisabled in varchar2)
is
  pragma autonomous_transaction; 
  sCurrentName     constant varchar2(4000) := $$PLSQL_UNIT||'.switchServerStatus('||cnServerId||', ' || csIsDisabled || ')';
  oErrorObj        STB$OERROR$RECORD := STB$OERROR$RECORD();
begin
  isr$trace.stat('begin', 'begin', sCurrentName); 
  update isr$server set disabled = csIsDisabled  where serverid = cnServerId;
  update isr$dialog set displayed = case when csIsDisabled = 'T' then 'F' else 'T' end
     where parametername = 'SERVER_' || cnServerId and token in ('CAN_CREATE_NEW_REPORT', 'CAN_MODIFY_USER_PARAMETER');
  if csIsDisabled = 'T' then
    delete from stb$userparameter where parametername = 'SERVER_' || cnServerId;
  else
    merge into  STB$USERPARAMETER d
    using (select 'SERVER_'||cnServerId PARAMETERNAME, userno from stb$user) S
    on ( d.parametername = s.parametername)
    when not matched then insert (USERNO, PARAMETERNAME, PARAMETERVALUE)
    values( s.userno, 'SERVER_'||cnServerId, 'T');
  end if;
  commit; 
  isr$trace.stat('end', 'end', sCurrentName);
exception
  when others then
    rollback;
    oErrorObj.handleError ( Stb$typedef.cnSeverityCritical, utd$msglib.getmsg ('exUnexpectedError', nCurrentLanguage,  csP1 => sqlerrm(sqlcode)), 
        sCurrentName, dbms_utility.format_error_stack ||crlf|| dbms_utility.format_error_backtrace );     
end switchServerStatus;


 --*************************************************************************************************************************
function deleteServerDirectory( csObserverHost  in varchar2,  csObserverPort  in varchar2, csDirectoryName in varchar2,  csLoglevel  in varchar2, nTimeout NUMBER ) return clob
as language java
name 'com.uptodata.isr.observerclient.ObserverClient.deleteServerDirectory( java.lang.String, java.lang.String, java.lang.String, java.lang.String, int ) return java.sql.Clob';
  
--****************************************************************************************************
function createServerDirectory(csObserverHost in varchar2, csObserverPort in varchar2, csDirectoryName  in varchar2, csFolders in varchar2, csConcat in varchar2, csLoglevel  in varchar2, nTimeout NUMBER) return clob
as language java
name 'com.uptodata.isr.observerclient.ObserverClient.createServerDirectory( java.lang.String, java.lang.String, java.lang.String, java.lang.String, java.lang.String, java.lang.String, int ) return java.sql.Clob';

--****************************************************************************************************
function isServerAvailable(csObserverHost in varchar2, csObserverPort in varchar2, csLoglevel  in varchar2) return varchar2
AS LANGUAGE JAVA
NAME 'com.uptodata.isr.observerclient.ObserverClient.isServerAvailable( java.lang.String, java.lang.String, java.lang.String) return java.lang.String';

--******************************************************************************
FUNCTION startRemoteServer(csObserverHost IN VARCHAR2, csObserverPort IN VARCHAR2, csPort IN VARCHAR2, csClassName IN VARCHAR2,
                           csContext IN VARCHAR2,  csNamespace IN VARCHAR2,  csServerid IN VARCHAR2, csServerPath IN VARCHAR2,  csJavaVmOptions  IN VARCHAR, csLoglevel  IN VARCHAR2, csLogReference  IN VARCHAR2, nTimeout NUMBER) RETURN CLOB
AS LANGUAGE JAVA
NAME 'com.uptodata.isr.observerclient.ObserverClient.startServer( java.lang.String, java.lang.String, java.lang.String, java.lang.String, java.lang.String, java.lang.String, java.lang.String, 
                         java.lang.String,  java.lang.String,  java.lang.String,  java.lang.String, int ) return java.sql.Clob';

--******************************************************************************
FUNCTION stopRemoteServer(csObserverHost IN VARCHAR2, csObserverPort IN VARCHAR2,  csServerid IN VARCHAR2,  csLoglevel  IN VARCHAR2, nTimeout NUMBER)  RETURN CLOB
AS LANGUAGE JAVA
NAME 'com.uptodata.isr.observerclient.ObserverClient.stopServer( java.lang.String, java.lang.String, java.lang.String,  java.lang.String, int) return java.sql.Clob';
--******************************************************************************************************

FUNCTION sendFileToServer(observerHost IN VARCHAR2, observerPort IN VARCHAR2, fileName IN VARCHAR2, target  IN VARCHAR2, zipped in VARCHAR2, csConfigName IN VARCHAR2,
                                          csId IN VARCHAR2,  logLevel IN VARCHAR2, csLogReference IN VARCHAR2, nTimeout NUMBER) return CLOB
AS LANGUAGE JAVA
NAME 'com.uptodata.isr.observerclient.ObserverClient.sendFileToServer( java.lang.String, java.lang.String, java.lang.String,  java.lang.String, java.lang.String, 
                     java.lang.String,  java.lang.String, java.lang.String,  java.lang.String, int) return java.sql.Clob';
--**************************************************************************************************************

FUNCTION sendFileToServer(observerHost IN VARCHAR2, observerPort IN VARCHAR2, oParamList  in   isr$Parameter$list) return CLOB
AS LANGUAGE JAVA
NAME 'com.uptodata.isr.observerclient.ObserverClient.sendFileToServer( java.lang.String, java.lang.String, oracle.sql.ARRAY) return java.sql.Clob';
--**************************************************************************************************************

FUNCTION getObserverPlatformName(csObserverHost IN VARCHAR2, csObserverPort IN VARCHAR2,   csLoglevel  IN VARCHAR2, nTimeout NUMBER)  RETURN VARCHAR2
AS LANGUAGE JAVA
NAME 'com.uptodata.isr.observerclient.ObserverClient.getPlatformName( java.lang.String, java.lang.String,  java.lang.String, int) return  java.lang.String';

-- ***********************************************************************************************************

FUNCTION getObserverPlatformArch(csObserverHost IN VARCHAR2, csObserverPort IN VARCHAR2,   csLoglevel  IN VARCHAR2, nTimeout NUMBER)  RETURN VARCHAR2
AS LANGUAGE JAVA
NAME 'com.uptodata.isr.observerclient.ObserverClient.getPlatformArchitecture( java.lang.String, java.lang.String, java.lang.String, int) return  java.lang.String';
-- *******************************************************************************************************************

FUNCTION fetchLogServerFile(observerHost IN VARCHAR2, observerPort IN VARCHAR2, sServerFolder  IN VARCHAR2, sStartDate  IN VARCHAR2,  sEndDate in VARCHAR2, 
                                           csConfigName IN VARCHAR2,  csId IN VARCHAR2,  sDateFormatter in varchar2, csLogLevel IN VARCHAR2, csLogReference IN VARCHAR2, nTimeout NUMBER) return CLOB
 AS LANGUAGE JAVA
NAME 'com.uptodata.isr.observerclient.ObserverClient.fetchLogServerFile( java.lang.String, java.lang.String, java.lang.String,  java.lang.String, java.lang.String, 
                               java.lang.String, java.lang.String, java.lang.String, java.lang.String, java.lang.String, int) return java.sql.Clob';
--**********************************************************************************************************************

END isr$server$base;