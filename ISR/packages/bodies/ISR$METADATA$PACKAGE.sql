CREATE OR REPLACE package body ISR$METADATA$PACKAGE  as

  type defaultColTyp  is record  ( colName     varchar2(30),
                                   colDataType varchar2(30),
                                   colType     varchar2(30));
                              
  type DefaultColTab is table of defaultColTyp index by pls_integer;

  nCountDefaultCol    number;
  recDefaultCol       DefaultColTab;
  csTempTabPrefix     constant varchar2(50) := 'TMP$';
  csMdmTabPrefix      constant varchar2(50) := 'MDM$';
  nCurrentLanguage    number        := stb$security.getcurrentlanguage;    
  crlf                constant varchar2(2) := chr(10)||chr(13); 

  cursor cGetEntity is
    select entityname, mdm
    from   isr$meta$entity;
    
  recEntity cGetEntity%rowtype;

  cursor cGetEntityQA( csEntityName in varchar2 ) is
    select meta.entityname META_ENTITYNAME, meta.attrname META_ATTRNAME, meta.datatype META_DATATYPE, meta.orderno META_ORDERNO, nvl(meta.iskey,'F') META_ISKEY,
           max(nvl(meta.iskey,'F')) over(partition by meta.entityname) META_KEY_EXISTS,
           tabtmp.table_name TMP_TABLENAME, tmp.column_name TMP_COLUMNNAME, tmp.data_type TMP_DATATYPE,
           decode(tmp.column_name,null,'F','T')        TMP_COL_EXISTS,
           decode(meta.datatype,tmp.data_type,'T','F') TMP_TYPE_OK,           
           tabmdm.table_name MDM_TABLENAME, mdm.column_name MDM_COLUMNNAME, mdm.data_type MDM_DATATYPE,
           decode(mdm.column_name,null,'F','T')        MDM_COL_EXISTS,
           decode(meta.datatype,mdm.data_type,'T','F') MDM_TYPE_OK
    from   isr$meta$attribute  meta
    left join user_tables      tabtmp
    on     tabtmp.table_name = 'TMP$'||meta.entityname   
    left join user_tables      tabmdm
    on     tabmdm.table_name = 'MDM$'||meta.entityname  
    left join user_tab_columns tmp
    on     tmp.table_name = 'TMP$'||meta.entityname
    and    tmp.column_name = meta.attrname
    left join user_tab_columns mdm
    on     mdm.table_name = 'MDM$'||meta.entityname
    and    mdm.column_name = meta.attrname
    where  csEntityName = meta.entityname;
  
  recEntityAttrib cGetEntityQA%rowtype;
  
  cursor cGetEntityDefaultQA( csEntityName in varchar2 ) is  
    select *
    from   ( select substr(table_name,1,3) DEFAULT_TAB_TYPE, table_name, column_name, data_type
             from   user_tab_columns tabcol
             where  tabcol.table_name = 'TMP$'||csEntityName
             or     tabcol.table_name = 'MDM$'||csEntityName )
    where (default_tab_type, table_name, column_name) in ( select substr(table_name,1,3) DEFAULT_TAB_TYPE, table_name, column_name--, data_type
                                                           from   user_tab_columns tabcol
                                                           where  tabcol.table_name = 'TMP$'||csEntityName
                                                           or     tabcol.table_name = 'MDM$'||csEntityName
                                                           minus   ----
                                                           select 'TMP', 'TMP$'||entityname, attrname--, datatype
                                                           from   isr$meta$attribute
                                                           where  entityname = csEntityName
                                                           minus  ----
                                                           select 'MDM', 'MDM$'||entityname, attrname--, datatype
                                                           from   isr$meta$attribute
                                                           where  entityname = csEntityName );
    
  recEntityDefault cGetEntityDefaultQA%rowtype;

--******************************************************************************   
function getDefaultColString( csTableType varchar2  default 'TMP' )  -- 'TMP', 'MDM', 'ALL'
  return varchar2
is
  sCurrentName  constant varchar2(100) := $$PLSQL_UNIT||'.getDefaultColString';  
  cReturnString varchar2(4000);
begin
  isr$trace.stat('begin', 'csTableType: '||csTableType, sCurrentName);
  for i in 1 .. nCountDefaultCol 
  loop
    if ( csTableType = 'ALL' or csTableType = recDefaultCol(i).colType )  then
      cReturnString := cReturnString||', '||recDefaultCol(i).colName||' '|| recDefaultCol(i).colDataType;
    end if;
  end loop;
 
  cReturnString := cReturnString||' ';
  isr$trace.stat('end', 'end',sCurrentName);
  return cReturnString;   -- sample: ', serverid number, CritOrderBy number, EntityOrderBy number, selected varchar2(1) '
end getDefaultColString;

--******************************************************************************   
function chkEntityTables  
  return STB$OERROR$RECORD
is
  oErrorObj        STB$OERROR$RECORD := STB$OERROR$RECORD(tloMessageList => ISR$MESSAGE$LIST());
  -- replaced declaration bacause after declaring constants in STB$TYPEDEF this throws error:
  -- PLS-00206: %TYPE must be applied to a variable, column, field or attribute, not to "STB$TYPEDEF.CNSEVERITYMESSAGE"
  --nMaxSeverityCode STB$TYPEDEF.cnSeverityMessage%type := STB$TYPEDEF.cnNoError;   -- initial value is cnNoError
  nMaxSeverityCode number := STB$TYPEDEF.cnNoError;   -- initial value is cnNoError
  sCurrentName     constant varchar2(100) := $$PLSQL_UNIT||'.chkEntityTables';    
begin  
  isr$trace.stat('begin', 'begin',sCurrentName);

  for recEntity in cGetEntity loop
    oErrorObj := chkEntityTable( recEntity.entityName, case recEntity.mdm when 'F' then 'TMP' else 'ALL' end );
  end loop;

  nMaxSeverityCode := iSR$ErrorObj$Accessor.GetWorstSeverityCode;

  -- communicate highest severity to calling function
  if nMaxSeverityCode > STB$TYPEDEF.cnNoError then
    oErrorObj.handleError ( csSeveritycode => nMaxSeverityCode,  
                            csUserInfo     => utd$msglib.getmsg ('msgMaxSeverity', nCurrentLanguage, nMaxSeverityCode ),
                            csImplInfo     => sCurrentName,
                            csDevInfo      => 'End of error stack in '||sCurrentName);
  end if;

  isr$trace.stat('end', 'end', sCurrentName);
  return oErrorObj;
exception
  when others then
    oErrorObj.handleError ( Stb$typedef.cnSeverityCritical, 
                            utd$msglib.getmsg ('exUnexpectedError', nCurrentLanguage, sqlerrm(sqlcode)), 
                            sCurrentName, 
                            substr(dbms_utility.format_error_stack ||crlf|| dbms_utility.format_error_backtrace, 1,4000) );                                
    raise;    
end chkEntityTables;

--******************************************************************************   
function chkEntityTable( csEntityName in isr$meta$entity.entityname%type,
                         csTableType  in varchar2   default 'ALL' )
  return STB$OERROR$RECORD
is
  oErrorObj        STB$OERROR$RECORD := STB$OERROR$RECORD();
  sCurrentName     constant varchar2(100) := $$PLSQL_UNIT||'.chkEntityTable ('||csEntityName||')';    
  -- replaced declaration bacause after declaring constants in STB$TYPEDEF this throws error:
  -- PLS-00206: %TYPE must be applied to a variable, column, field or attribute, not to "STB$TYPEDEF.CNSEVERITYMESSAGE"
  --nMaxSeverityCode STB$TYPEDEF.cnSeverityMessage%type := STB$TYPEDEF.cnNoError;  -- initial value is cnNoError
  nMaxSeverityCode number := STB$TYPEDEF.cnNoError;  -- initial value is cnNoError
  sTmpTableExists  char(1);
  sMdmTableExists  char(1);
  sMetaKeyExists   char(1) := 'T';
  bTmpDefaultHit   boolean;
  bMdmDefaultHit   boolean;
  bTmpTableHit     boolean;
  bMdmTableHit     boolean;
begin
  isr$trace.stat('begin', 'csTableType: '||csTableType, sCurrentName);
  ---------------------------------------------------------------------------------- check Metadata in TMP and MDM tables   ------------------------------------
  for recEntityQA in cGetEntityQA( csEntityName ) loop
     isr$trace.debug_all('code position loop#1', 'Check metadata.  recEntityQA.meta_attrname: '||recEntityQA.meta_attrname,sCurrentName);
     ---------------------------------------------------------------------------------------------------------
     -- Columns:    META_ENTITYNAME, META_ATTRNAME,  META_DATATYPE, META_ORDERNO, META_ISKEY, META_KEY_EXITS, 
     --             TMP_TABLENAME,   TMP_COLUMNNAME, TMP_DATATYPE,  TMP_COL_EXISTS,  TMP_TYPE_OK,
     --             MDM_TABLENAME,   MDM_COLUMNNAME, MDM_DATATYPE,  MDM_COL_EXISTS,  MDM_TYPE_OK
     ---------------------------------------------------------------------------------------------------------
     if csTableType in  ('TMP','ALL') then
       if recEntityQA.tmp_tablename is null then 
         -- The temporary table does not exist
         sTmpTableExists := 'F';  -- processing after loop
       else
          sTmpTableExists := 'T';
          if recEntityQA.tmp_columnname is null then
            -- Column defined in metadata does not exist in TMP$-table
            oErrorObj.handleError ( Stb$typedef.cnSeverityCritical, 
                                    utd$msglib.getmsg('exTableColumnMissing', nCurrentLanguage, recEntityQA.tmp_tablename, recEntityQA.meta_attrname),
                                    sCurrentName, 'code position #10' );    
          else
            if recEntityQA.meta_datatype <> recEntityQA.tmp_datatype then
              -- Dattype in TMP$table differs from metadata definition
              oErrorObj.handleError ( Stb$typedef.cnSeverityCritical, 
                                      utd$msglib.getmsg('exTableDatatype', nCurrentLanguage, recEntityQA.tmp_tablename, recEntityQA.tmp_columnname, recEntityQA.tmp_datatype, csP4 => recEntityQA.meta_datatype ),
                                      sCurrentName, 'code position #15' );   
            end if;
          end if;
       end if;  
     end if;
    
     -- check metadata key definition
     if recEntityQA.meta_key_exists = 'F' then 
       sMetaKeyExists := 'F';  -- processing after loop
     end if;  
     
     -- checks MDM$ table
     if csTableType in ('MDM','ALL') then
       if recEntityQA.mdm_tablename is null then 
         -- The MDM table does not exist
         sMdmTableExists := 'F';  -- processing after loop
       else
          sMdmTableExists := 'T';
          if recEntityQA.mdm_columnname is null then
            -- Column defined in metadata does not exist in MDM$-table
            oErrorObj.handleError ( Stb$typedef.cnSeverityCritical, 
                                    utd$msglib.getmsg('exTableColumnMissing', nCurrentLanguage, recEntityQA.mdm_tablename, recEntityQA.meta_attrname), 
                                    sCurrentName, 'code position #20' );  
          else
            if recEntityQA.meta_datatype <> recEntityQA.mdm_datatype then
              -- Dattype in MDM$table differs from metadata definition
              oErrorObj.handleError ( Stb$typedef.cnSeverityCritical, 
                                      utd$msglib.getmsg('exTableDatatype', nCurrentLanguage, recEntityQA.mdm_tablename, recEntityQA.mdm_columnname,  recEntityQA.mdm_datatype, recEntityQA.meta_datatype ), 
                                      sCurrentName, 'code position #25' );    
            end if;
          end if;
       end if;  
     end if;
  end loop;

  ---------------------------------------------------------------------------------- check tables for undefined columns  and  check default columns ------------------------------------ 
  -- check default columns, which are in checked tables
  for recEntityDefault in cGetEntityDefaultQA( csEntityName ) loop
      ---------------------------------------------------------------------
      -- columns:   DEFAULT_TAB_TYPE, TABLE_NAME, COLUMN_NAME, DATA_TYPE
      ---------------------------------------------------------------------      
    isr$trace.debug_all('code position loop#2', 'Check columns.  recEntityDefault.column_name: '||recEntityDefault.column_name,sCurrentName);
    bTmpTableHit   := false;
    bMdmTableHit   := false;
    for i in 1..nCountDefaultCol loop
      isr$trace.debug_all('code position loop#2.1', 'Check default columns.  i='||i,sCurrentName);
      -- Check TMP$table. Every entity needs a TMP$-table
      if (     csTableType in  ('TMP','ALL')    
           and recEntityDefault.default_tab_type = 'TMP'  
           and recDefaultCol(i).colType = 'TMP' 
           and recEntityDefault.column_name = recDefaultCol(i).colName ) then
        bTmpTableHit   := true;
        if recEntityDefault.data_type <> substr( recDefaultCol(i).colDataType,1,length(recEntityDefault.data_type)  ) then                
          -- wrong datatype in TMP$-table for default column found
          oErrorObj.handleError ( Stb$typedef.cnSeverityCritical, 
                                  utd$msglib.getmsg('exTableDatatype', nCurrentLanguage, recEntityDefault.table_name, recEntityDefault.column_name, recEntityDefault.data_type, recDefaultCol(i).colDataType  ), 
                                  sCurrentName, 'code position #40' );
        end if;
      end if;
      
      -- Check MDM$-table defaults. Every entity may have a MDM$-table
      if sMdmTableExists = 'T' then
        -- the following checks (within if-statement) are only necessary, if the MDM$-table exists
        if (     csTableType in  ('MDM','ALL','TMP')    
             and recEntityDefault.default_tab_type = 'MDM'  
             and recDefaultCol(i).colType in ('MDM','TMP')
             and recEntityDefault.column_name = recDefaultCol(i).colName ) then
          bMdmDefaultHit := true; 
          bMdmTableHit   := true;
          if recEntityDefault.data_type <> substr( recDefaultCol(i).colDataType,1,length(recEntityDefault.data_type)  )  then                
            -- wrong datatype in MDM$-table for default column found
            oErrorObj.handleError ( Stb$typedef.cnSeverityCritical, 
                                    utd$msglib.getmsg('exTableDatatype', nCurrentLanguage, recEntityDefault.table_name, recEntityDefault.column_name, recEntityDefault.data_type, recDefaultCol(i).colDataType ),  
                                    sCurrentName, 'code position #50' );  
          end if;
        end if;
      end if;
    end loop;
  
    if sMdmTableExists = 'T' then
      if csTableType in ('MDM','ALL','TMP') and not bMdmDefaultHit then
        -- defined default column not found in MDM$-table
        oErrorObj.handleError ( Stb$typedef.cnSeverityCritical, 
                                utd$msglib.getmsg('exTableColumnMissing', nCurrentLanguage, recEntityDefault.table_name, recEntityDefault.column_name),
                                sCurrentName, 'code position #60' );  
      end if;
    end if;

    if (     csTableType in ('TMP','ALL')
         and not bTmpTableHit
         and recEntityDefault.default_tab_type = 'TMP' ) then
      -- Undefined column in TMP$-table found
      oErrorObj.handleError ( Stb$typedef.cnSeverityWarning, 
                              utd$msglib.getmsg('exTableColumnUndefined', nCurrentLanguage, recEntityDefault.table_name, recEntityDefault.column_name),
                              sCurrentName, 'code position #65' );
    end if;

    if (     sMdmTableExists = 'T'
         and not bMdmTableHit
         and  csTableType in ('MDM','ALL')
         and  recEntityDefault.default_tab_type = 'MDM' ) then
        -- Undefined column in MDM$-table found
        oErrorObj.handleError ( Stb$typedef.cnSeverityWarning, 
                                utd$msglib.getmsg('exTableColumnUndefined', nCurrentLanguage, recEntityDefault.table_name, recEntityDefault.column_name),
                                sCurrentName, 'code position #70' );  
    end if;

  end loop;

  ---------------------------------------------------------------------------------- check default columns, which are not in checked table ------------------------------------
  for i in 1..nCountDefaultCol loop
    isr$trace.debug_all('code position loop#3', 'i='||i||' check default columns, which are not in checked table',sCurrentName);
    -- check default columns, which are not in checked table
    bTmpDefaultHit := false;
    bMdmDefaultHit := false;   
    -- determine output settings
    for recEntityDefault in cGetEntityDefaultQA( csEntityName ) loop
      isr$trace.debug_all('code position loop#3.1', 'recEntityDefault.column_name='||recEntityDefault.column_name,sCurrentName);
      if (     csTableType in  ('TMP','ALL')    
           and recEntityDefault.default_tab_type = 'TMP'  
           and recDefaultCol(i).colType = 'TMP' 
           and recEntityDefault.column_name = recDefaultCol(i).colName ) then
        bTmpDefaultHit := true; 
      end if;    
      if (     sMdmTableExists = 'T'
           and csTableType in  ('MDM','ALL','TMP')    
           and recEntityDefault.default_tab_type = 'MDM'  
           and recDefaultCol(i).colType in ('MDM','TMP')
           and recEntityDefault.column_name = recDefaultCol(i).colName ) then
          bMdmDefaultHit := true; 
      end if;    
    end loop;
    
    -- log errors
    isr$trace.debug_all('code position', 'loop#3 log errors',sCurrentName);
    if (     sTmpTableExists = 'T'
         and csTableType in ('TMP','ALL') 
         and not bTmpDefaultHit  
         and recDefaultCol(i).colType = 'TMP'   ) then
      -- defined default column not found in TMP$-table
      oErrorObj.handleError ( Stb$typedef.cnSeverityCritical,
                              utd$msglib.getmsg('exTableColumnMissing', nCurrentLanguage, 'TMP$'||csEntityName, recDefaultCol(i).colName),
                              sCurrentName, 'code position #80' );  
    end if;
    if (     sMdmTableExists = 'T'
         and not bMdmDefaultHit
         and csTableType in ('MDM','ALL') ) then
      -- defined default column not found in MDM$-table
      oErrorObj.handleError ( Stb$typedef.cnSeverityCritical,
                              utd$msglib.getmsg('exTableColumnMissing', nCurrentLanguage, 'MDM$'||csEntityName, recDefaultCol(i).colName),
                              sCurrentName, 'code position #90' );  
    end if;
  end loop;    

  ---------------------------------------------------------------------------------- final error analysis and recording ----------------------
  isr$trace.debug_all('code position', 'final analysis',sCurrentName);
  if sTmpTableExists = 'F' and csTableType in ('TMP','ALL') then
    oErrorObj.handleError ( Stb$typedef.cnSeverityCritical, 
                            utd$msglib.getmsg('exNoTable', nCurrentLanguage, 'TMP$-', 'TMP$'||csEntityname),
                            sCurrentName, 'code position #100' );  
  end if;
  
  if sMetaKeyExists = 'F' then
    oErrorObj.handleError ( Stb$typedef.cnSeverityCritical, 
                            utd$msglib.getmsg('exMetaNoKey', nCurrentLanguage, csEntityName ),
                            sCurrentName, 'code position #110' );  
  end if;
  
  if sMdmTableExists = 'F' and csTableType in ('MDM','ALL') then
    isr$trace.warn('WARNING exNoTable', utd$msglib.getmsg('exNoTable', nCurrentLanguage,  'MDM$-', 'MDM$'||csEntityname),sCurrentName);
  end if;

  isr$trace.stat('end', 'end', sCurrentName);
  return oErrorObj;
exception
  when others then
    oErrorObj.handleError ( Stb$typedef.cnSeverityCritical, 
                            utd$msglib.getmsg ('exUnexpectedError', nCurrentLanguage, sqlerrm(sqlcode)), 
                            sCurrentName, 
                            substr(dbms_utility.format_error_stack ||crlf|| dbms_utility.format_error_backtrace, 1,4000) );  
    return oErrorObj;                            
end chkEntityTable;

--******************************************************************************    
function GetParameterList( oParamList out iSR$AttrList$Rec )
  return STB$OERROR$RECORD 
is
  sCurrentName     constant varchar2(100) := $$PLSQL_UNIT||'.GetParameterList';      
  oErrorObj STB$OERROR$RECORD := STB$OERROR$RECORD();         
  nIx number;
  sValue varchar2(500);
  sSql             varchar2(32000);
  
  cursor cParameter IS
    select ParameterName, Expression
    from   isr$meta$parameter
    where  reporttypeid = STB$OBJECT.GetCurrentReportTypeId;
    
  rParameter cParameter%ROWTYPE;

begin
  isr$trace.stat('begin', 'begin', sCurrentName);
  oParamList := iSR$AttrList$Rec();
  open cParameter;
  fetch cParameter into rParameter;
  while cParameter%FOUND loop
    
    -- MCD, 01. Apr 2009, for select statements
    sSql := 'select ('|| rParameter.Expression||') from dual';
    isr$trace.debug('sSql',sSql, sCurrentName);
    execute immediate sSql into /*using out*/ sValue;
  
    oParamList.lAttributes.extend;
    nIx := oParamList.lAttributes.last;
    oParamList.lAttributes(nIx) := iSR$Attribute$Record(rParameter.ParameterName, sValue, null );
    isr$trace.debug('parameter '||nIx, 'sParameter: '||rParameter.ParameterName||' sValue: '||sValue, sCurrentName);
      
    fetch cParameter into rParameter;    
  end loop;
  close cParameter;
   
  isr$trace.stat('end','oParamlist', sCurrentName, xmltype(oParamList).getClobVal());
  return oErrorObj;
exception
  when others then
    oErrorObj.handleError( STB$TYPEDEF.cnSeverityCritical, utd$msglib.getmsg ('exUnexpectedError', stb$security.getCurrentLanguage, sqlerrm(sqlcode)), sCurrentName, 
                           substr(dbms_utility.format_error_stack || CHR(10) || dbms_utility.format_error_backtrace, 1,4000));   
    return oErrorObj;  -- DSIS-207 and ISRC-1156
end GetParameterList;

--******************************************************************************
function createTempTables( csPluginList in varchar2 default '%',
                           csEntityList in varchar2 default '%' )  
  return varchar2
is
  sCurrentName     constant varchar2(100) := $$PLSQL_UNIT||'.createTempTables(function)';   
  oErrorObj        STB$OERROR$RECORD      := STB$OERROR$RECORD(tloMessageList => ISR$MESSAGE$LIST());
  sPluginList      varchar2(4000)         := replace(upper(csPluginList),' ','');
  sEntityList      varchar2(4000)         := replace(upper(csEntityList),' ',''); 
  sReturn          char(1);
begin
  isr$trace.stat('begin','begin', sCurrentName, 'csPluginList: '||csPluginList||crlf||'csEntityList:'||csEntityList);
  isr$trace.debug('values','sPluginList, sEntityList, see column LOGCLOB', sCurrentName, 'sPluginList: '||sPluginList||crlf||'sEntityList: '||sEntityList);  
  for rec_entity in ( select entityname, plugin
                      from   isr$meta$entity
                      where  (    sPluginList = '%'  
                               or instr(sPluginList||',', plugin||',')  > 0 )
                      and    (    sEntityList = '%'
                               or instr( sEntityList||',', entityname||',') > 0 ) )
  loop
    isr$trace.debug('value','rec_entity.entityname: '||rec_entity.entityname, sCurrentName);
    oErrorObj := isr$metadata$package.DropTempEntityTable( rec_entity.entityname );
    oErrorObj := isr$metadata$package.CreateEntityTable( rec_entity.entityname );
  end loop;
  
  if iSR$ErrorObj$Accessor.GetWorstSeverityCode = stb$typedef.cnNoError then 
    -- no error occurred
    sReturn := 'T';
  else  
    -- at least one error occurred, even if processing continued
    sReturn := 'F';
  end if;
  
  isr$trace.stat('end','end', sCurrentName);
  return sReturn;
exception
  when others then
  dbms_output.put_line('error');
    oErrorObj.handleError( STB$TYPEDEF.cnSeverityCritical, utd$msglib.getmsg ('exUnexpectedError', nCurrentLanguage, sqlerrm(sqlcode)), sCurrentName, 
                           substr(dbms_utility.format_error_stack || CHR(10) || dbms_utility.format_error_backtrace, 1,4000));  
    return 'F';   
end CreateTempTables;

--******************************************************************************
procedure createTempTables( csPluginList in varchar2 default '%',
                            csEntityList in varchar2 default '%'  )
is
  sCurrentName           constant varchar2(100) := $$PLSQL_UNIT||'.createTempTables(procedure)';   
  exCannotCreateTable    exception;
  sSuccessfulExecution   char(1);
  pragma exception_init( exCannotCreateTable, -20942);
begin
  isr$trace.stat('begin','begin', sCurrentName, 'csPluginList: '||csPluginList||crlf||'  csEntityList: '||csEntityList);
  
  sSuccessfulExecution := createTempTables( csPluginList => csPluginList,
                                            csEntityList => csEntityList );
  if sSuccessfulExecution = 'F' then
    raise_application_error(-20942,'exCannotCreateTable');
  end if;

  isr$trace.stat('end','end', sCurrentName);
  -- No exception handler intended, pass control to calling environment
end createTempTables;  

--******************************************************************************
function CreateEntityTable( csEntity    in varchar2,
                            csTableType in varchar2 default 'TMP',
                            csUncatched in char     default 'F' ) 
  return STB$OERROR$RECORD
is
  exCannotCreateTable  exception;
  exInstallCreateTable exception;
  pragma exception_init(exInstallCreateTable, -20002);
  
  sCurrentName         constant varchar2(100) := $$PLSQL_UNIT||'.CreateEntityTable('||csEntity||')'; 
  oErrorObj            STB$OERROR$RECORD := STB$OERROR$RECORD();  
  sColumnlist          varchar2(4000);
  sStatement           varchar2(4000);
  sDataType            varchar2(100);
  sMsg                 varchar2(4000);
  nIdx                 integer := 0;
  bMetaAttributesExist boolean := false; -- [ISRC-476] Comment Jörg Servos 25.May 2016
  
  cursor cEntityAttr is  
    select AttrName, DataType,
           case
              when NVL (AttrSize, 4000) = 0 then 1
              else NVL (AttrSize, 4000)
           end  AttrSize,
           scale, precision
    from   isr$meta$attribute
    where  entityname = csEntity
    order by OrderNo;
    
  rEntityAttr cEntityAttr%ROWTYPE;      
  
BEGIN
  isr$trace.stat('begin', 'begin', sCurrentName);
  if csUncatched = 'F' then
    isr$trace.stat('begin','csEntity: '||csEntity||'  csTableType: '||csTableType, sCurrentName);
  end if;
  
  open  cEntityAttr;
  fetch cEntityAttr into rEntityAttr;
  while cEntityAttr%FOUND loop
    nIdx := nIdx + 1;
    if Upper(rEntityAttr.DataType) = 'VARCHAR2' then
      sDataType :=  rEntityAttr.DataType||'('||Nvl(rEntityAttr.AttrSize,'4000')||')';
    else
      -- scale and precision is not evaluated yet
      sDataType := rEntityAttr.DataType;
    end if;
    sColumnList := sColumnList||','||rEntityAttr.AttrName||' '||sDataType;
    if csUncatched = 'F' then
      isr$trace.debug('value','sColumnList: '||sColumnList, sCurrentname);
    end if;
    fetch cEntityAttr into rEntityAttr;      
  end loop;  
  close cEntityAttr; 
  sColumnList := ltrim(sColumnList, ',');
  -- add sort columns
            
  if sColumnList is not null then
    bMetaAttributesExist := true;
  end if;
  sColumnList := sColumnList||getDefaultColString('TMP');  -- required for TMP and MDM

  -- create temporary table, but only, if meta attributes exists [ISRC-476], else do nothing
  if (     csTableType = 'TMP' 
       and bMetaAttributesExist = true )then  
    sStatement := 'create global temporary table '||csTempTabPrefix||csEntity||' ('||sColumnList||
                  ') on commit preserve rows';
    begin                  
      execute immediate sStatement;
    exception
      when others then
        if csUncatched = 'F' then  
          sMsg := Utd$msglib.GetMsg('exCannotCreateTable',nvl(Stb$security.GetCurrentLanguage,2),csTableType, csTempTabPrefix||csEntity );
          isr$trace.error('ERROR', sMsg, sCurrentname, sStatement||crlf||dbms_utility.format_error_stack ||crlf|| dbms_utility.format_error_backtrace);
        else
          sMsg := Utd$msglib.GetMsg('exCannotCreateTable',nvl(Stb$security.GetCurrentLanguage,2),csTableType, csTempTabPrefix||csEntity );
          isr$trace.error('ERROR', sMsg, sCurrentname, sStatement||crlf||dbms_utility.format_error_stack ||crlf|| dbms_utility.format_error_backtrace);        
          raise_application_error(-20002, 'Cannot create temporary table for entity: <'||csEntity||'>');
        end if; 
    end; 
    oErrorObj := CreateTempEntityIndexes(csEntity);
  end if;
  
  -- create MDM table
  if csTableType = 'MDM' then  
    sColumnList := sColumnList||getDefaultColString('MDM'); 
    sStatement := 'create table '||csMdmTabPrefix||csEntity||' ('||sColumnList||' )';
    begin                  
      execute immediate sStatement;
    exception
      when others then
        raise exCannotCreateTable;
    end;                   
  end if;
  
  isr$trace.stat('end', 'end', sCurrentName);
  return oErrorObj;
exception
  when exInstallCreateTable then
    raise;
  when exCannotCreateTable then
    oErrorObj.handleError( STB$TYPEDEF.cnSeverityCritical, utd$msglib.getmsg('exCannotCreateTable', nCurrentLanguage, csTableType,csTempTabPrefix||csEntity ), sCurrentName, 
                           substr(dbms_utility.format_error_stack || CHR(10) || dbms_utility.format_error_backtrace, 1,4000));  
    return oErrorObj; 
  when others then
    oErrorObj.handleError( STB$TYPEDEF.cnSeverityCritical, utd$msglib.getmsg ('exUnexpectedError', nCurrentLanguage, sqlerrm(sqlcode)), sCurrentName, 
                           substr(dbms_utility.format_error_stack || CHR(10) || dbms_utility.format_error_backtrace, 1,4000));  
    return oErrorObj; 
end CreateEntityTable;

--******************************************************************************
function DropTempEntityTable( csEntity in varchar2 ) 
  return STB$OERROR$RECORD 
is
  sCurrentName           constant varchar2(100) := $$PLSQL_UNIT||'.DropTempEntityTable('||csEntity||')';    
  oErrorObj STB$OERROR$RECORD := STB$OERROR$RECORD();
  TABLE_MISSING          exception;
  pragma exception_init( TABLE_MISSING, -942); 
  sSql                   varchar2(4000);
begin
  isr$trace.stat('begin','begin', sCurrentName);
  if csEntity is not null then
    begin
      sSql := 'drop table '||csTempTabPrefix||csEntity;
      isr$trace.debug('sSql', sSql, sCurrentName);
      execute immediate sSql;
      isr$trace.debug('drop table', 'table dropped: '||csTempTabPrefix||csEntity, sCurrentName);
    exception when TABLE_MISSING then
      -- ignore the error if the table to be dropped does not exist
      null;
    end;
  else
    isr$trace.warn('warn',utd$msglib.getmsg ('exParameterIsNull', 1, csp1 => 'csEntity'), sCurrentName);
  end if;
  isr$trace.stat('end','end',sCurrentName);
  return oErrorObj;
exception
  when others then
    oErrorObj.handleError( STB$TYPEDEF.cnSeverityCritical, utd$msglib.getmsg ('exUnexpectedError', nCurrentLanguage, sqlerrm(sqlcode)), sCurrentName, 
                           substr(dbms_utility.format_error_stack || CHR(10) || dbms_utility.format_error_backtrace, 1,4000));  
    return oErrorObj; 
end DropTempEntityTable;

--******************************************************************************
function CreateTempEntityIndexes( csEntity    in varchar2) 
  return STB$OERROR$RECORD 
is
  sCurrentName constant varchar2(100) := $$PLSQL_UNIT||'.CreateTempEntityIndexes('||csEntity||')';    
  oErrorObj             STB$OERROR$RECORD := STB$OERROR$RECORD();
  exIndexOnColumnExists exception;
  pragma exception_init(exIndexOnColumnExists, -1408);
  sStatement            varchar2(4000);
  nCnt                  integer := 0;
begin
  isr$trace.stat('begin','begin', sCurrentName);
  
  for rIndexToCreate in (select attrname from isr$meta$attribute
                         where entityname = csEntity
                           and indexontemptab = 'T'
                         order by orderno) 
  loop
    nCnt := nCnt+1;    
    sStatement :=  'create index TX'||nCnt||csEntity||' ON '||csTempTabPrefix||csEntity||'('||rIndexToCreate.attrname||')';
    isr$trace.debug('index on '||rIndexToCreate.attrname, sStatement, sCurrentName);
    begin
      execute immediate sStatement;
    exception when exIndexOnColumnExists then
      isr$trace.debug('index on '||rIndexToCreate.attrname,'already exists', sCurrentName);
    end;
  end loop;
  
  isr$trace.stat('end','end',sCurrentName);
  return oErrorObj;
exception
  when others then    
    oErrorObj.handleError( STB$TYPEDEF.cnSeverityCritical, utd$msglib.getmsg ('exUnexpectedError', nCurrentLanguage, sqlerrm(sqlcode)), sCurrentName, 
                           dbms_utility.format_error_stack || CHR(10) || dbms_utility.format_error_backtrace);  
    return oErrorObj; 
end CreateTempEntityIndexes;

--******************************************************************************  
FUNCTION GetBaseEntity(csCritEntity in varchar2, sBaseEntity out varchar2, bTranslated out boolean) 
         RETURN STB$OERROR$RECORD is
  oErrorObj STB$OERROR$RECORD := STB$OERROR$RECORD();
  sCurrentName   constant varchar2(4000) := $$PLSQL_UNIT||'.GetBaseEntity('||' '||')';
  cursor cBaseEntity is
    select entity 
    from iSR$Meta$Crit
    where critEntity = csCritEntity;
  rBaseEntity cBaseEntity%rowtype;
BEGIN
  isr$trace.stat('begin', 'begin', sCurrentName);
  open cBaseEntity;
  fetch cBaseEntity into rBaseEntity;
  close cBaseEntity;
  sBaseEntity := rBaseEntity.Entity;
  if upper(rBaseEntity.Entity) = upper(csCritEntity) then 
    bTranslated := false;
  else
    bTranslated := true;
  end if;
  isr$trace.stat('end', 'end', sCurrentName);
  RETURN oErrorObj;
EXCEPTION
  WHEN others THEN
    oErrorObj.handleError( STB$TYPEDEF.cnSeverityCritical, utd$msglib.getmsg ('exUnexpectedError', stb$security.getCurrentLanguage, sqlerrm(sqlcode)), sCurrentName, 
                           substr(dbms_utility.format_error_stack || CHR(10) || dbms_utility.format_error_backtrace, 1,4000));   
END GetBaseEntity;   

--******************************************************************************  
procedure installTempTables( csEntity     varchar2 default '%',
                             csDbmsOutput char     default 'F' )
is
  pragma autonomous_transaction;
  
  exWrongEntityParameter exception;
  pragma exception_init(exWrongEntityParameter, -20001);
  
  oErrorObj     STB$OERROR$RECORD                := STB$OERROR$RECORD(tloMessageList => ISR$MESSAGE$LIST());
  vTmpTable     varchar2(30);  -- real name of the temporary table to be handled
  nEntityFound  number;        -- counter, to check if entity parameter csEntity exists in table isr$meta$entity
  nMaxSeverity  number;

  cursor curEntityTable( ccsEntityFilter varchar2 default '%' ) is
    select upper(entityname) entityname
    from   isr$meta$entity
    where  entityname like upper(ccsEntityfilter);
  
  ------ Procedure DropTable
  procedure DropTable( cvTmpTable varchar2 ) is  
  begin 
    execute immediate 'drop table '||cvTmpTable; 
  end DropTable;
  
  ------ Function TableExists
  function TableExists( cvTmpTable varchar2 ) 
    return boolean 
  is 
    nCounted number;
    nReturn  boolean := FALSE;
  begin 
     execute immediate 'select count(1) from user_tables where table_name = '''||cvTmpTable||''''
     into nCounted;
     if nCounted = 1 then
       nReturn := TRUE;
     end if;
     return nReturn;
  end TableExists;

begin
  -- no logging here !
  -- check parameter csEntity first
  case
    when csEntity is null then
      raise_application_error(-20101, 'Wrong parameter input for csEntity: <'||csEntity||'>');
    when nvl(length(replace(csEntity,'%','' )),0) > 0 then
      -- csEntity consists one or more characters, other than '%'
      nEntityFound := 0;
      for rec in curEntityTable(csEntity)
      loop
        nEntityFound := nEntityFound + 1;
        -- process entity: delete temp table first, create gtt after deletion
        -- ..... code
      end loop;     
      
      if nEntityFound = 0 then
        -- wrong parameter input for csEntity
         raise_application_error(-20001, 'Wrong parameter input for csEntity: <'||csEntity||'>');
      end if;
    else 
      -- csEntity consists of exactly one or more '%'-characters and has no other character signs, e.g.  '%' or '%%%%'
      null;  -- nothing to do
  end case;

  -- process entities
  nEntityFound := 0;
  for rec in curEntityTable(csEntity)
  loop
    vTmpTable := 'TMP$'||rec.entityname;
    if csDbmsOutput in ('T','t') then
      dbms_output.put_line('-----------------  processing '||vTmpTable||'  ---');   
    end if;  

    -- check if tmp-table exists
    if TableExists(vTmpTable) then
      -- table exists, delete it now
      DropTable(vTmpTable);
      if csDbmsOutput in ('T','t') then
        dbms_output.put_line(vTmpTable||' dropped');
      end if;  
    -- else nothing to do
    end if;
    
    oErrorObj := ISR$METADATA$PACKAGE.CreateEntityTable(rec.entityname,'TMP','T');
    if csDbmsOutput in ('T','t') then
      dbms_output.put_line(vTmpTable||' created');
    end if;  
  end loop;

  -- no exception handler here !
end installTempTables;


          
--******************************************************************************
FUNCTION GetBaseEntity(csCritEntity in varchar2) RETURN varchar2 is
  sCurrentName   constant varchar2(4000) := $$PLSQL_UNIT||'.GetBaseEntity('||' '||')';
  oErrorObj      STB$OERROR$RECORD := STB$OERROR$RECORD();
  bTranslated    boolean;
  sBaseEntity    varchar2(50);
begin
  isr$trace.stat('begin', 'begin', sCurrentName);
  oErrorObj := GetBaseEntity(csCritEntity, sBaseEntity, bTranslated);
  isr$trace.stat('end', 'end', sCurrentName);
  return sBaseEntity;
end GetBaseEntity;

begin -- main
  recDefaultCol(1).colName     := 'SERVERID';
  recDefaultCol(1).colDataType := 'NUMBER';
  recDefaultCol(1).colType     := 'TMP';
  
  recDefaultCol(2).colName     := 'CRITORDERBY';
  recDefaultCol(2).colDataType := 'NUMBER';
  recDefaultCol(2).colType     := 'TMP';
  
  recDefaultCol(3).colName     := 'ENTITYORDERBY';
  recDefaultCol(3).colDataType := 'NUMBER';  
  recDefaultCol(3).colType     := 'TMP';
  
  recDefaultCol(4).colName     := 'SELECTED';
  recDefaultCol(4).colDataType := 'VARCHAR2(1)';  
  recDefaultCol(4).colType     := 'TMP';

  recDefaultCol(5).colName     := 'CHECKSUM';
  recDefaultCol(5).colDataType := 'NUMBER';  
  recDefaultCol(5).colType     := 'TMP';

  recDefaultCol(6).colName     := 'MDM_RESOLUTION';
  recDefaultCol(6).colDataType := 'VARCHAR2(1)';  
  recDefaultCol(6).colType     := 'MDM';

  recDefaultCol(7).colName     := 'MDM_USER_RECORD';
  recDefaultCol(7).colDataType := 'VARCHAR2(1)';  
  recDefaultCol(7).colType     := 'MDM';

  recDefaultCol(8).colName     := 'MDM_GROUP_ACCEPTED';
  recDefaultCol(8).colDataType := 'VARCHAR2(1)';  
  recDefaultCol(8).colType     := 'MDM';

  recDefaultCol(9).colName     := 'MODIFIEDON';
  recDefaultCol(9).colDataType := 'DATE';  
  recDefaultCol(9).colType     := 'MDM';

  recDefaultCol(10).colName     := 'MODIFIEDBY';
  recDefaultCol(10).colDataType := 'VARCHAR2(30)';  
  recDefaultCol(10).colType     := 'MDM';

  nCountDefaultCol := 10;   -- [ISRC-1125]

END ISR$METADATA$PACKAGE;