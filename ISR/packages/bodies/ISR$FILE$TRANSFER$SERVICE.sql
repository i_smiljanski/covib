CREATE OR REPLACE package body isr$file$transfer$service is

  cursor cFileTransferConfig( csConfigName in isr$transfer$file$config.configname%type ) is
    select targetTable, targetTablePk, blobColumn, clobColumn, key_delimiter,  regexp_count(targetTablePk, key_delimiter,1)  nDelimiters
    from   isr$transfer$file$config  
    where  configname = csConfigName;
    
  rFileTransferConfig  cFileTransferConfig%rowtype;
     
  nCurrentLanguage    number        := stb$security.getcurrentlanguage;    

--*********************************************************************************************************************************
function getFileFromDb( csConfigName in  isr$transfer$file$config.configname%type, 
                        csTransferId in varchar2, 
                        csErrorXml   out clob     ) 
  return anydata
is  
  exConfigName        exception;
  dataTypeNotDefinedException  exception;
  sMsg    varchar2(4000);
  sCurrentName   CONSTANT VARCHAR2(100) := $$PLSQL_UNIT||'.getFileFromDb';
  oErrorObj      STB$OERROR$RECORD      := STB$OERROR$RECORD(tloMessageList => ISR$MESSAGE$LIST());
  sStmt          varchar2 (2000);
  aFile          anydata;
  sWhere     varchar2(2000);
  sDataType   varchar2(1);
begin
  isr$trace.stat('begin', 'begin: csConfigName: ' || csConfigName || '  csTransferId: ' || csTransferId, sCurrentName);
  
  open cFileTransferConfig(csConfigName);
  fetch cFileTransferConfig into rFileTransferConfig;
    
  if cFileTransferConfig%notfound then
    close cFileTransferConfig;
    sMsg := utd$msglib.getmsg ('exConfigNameError', nCurrentLanguage) || ' ' || csConfigName;
    raise exConfigName;
  end if; 
  
  close cFileTransferConfig;
 
  
  if rFileTransferConfig.key_delimiter is not null  then
    for i in 1..rFileTransferConfig.ndelimiters+1 loop
     sWhere := sWhere || ' nvl (' || isr$util.getSplit(rFileTransferConfig.targetTablePk, i,  rFileTransferConfig.key_delimiter) || ', ''-9999'') = nvl (''' || isr$util.getSplit(csTransferId, i , rFileTransferConfig.key_delimiter) ||''', ''-9999'')';
     if i < rFileTransferConfig.ndelimiters+1 then
      sWhere := sWhere || ' AND ';
     end if;
    end loop;
  else
    sWhere :=  rFileTransferConfig.targetTablePk || ' = ''' || csTransferId ||'''';
  end if;
  isr$trace.debug ('sWhere', sWhere, sCurrentName);
  
  sDataType := isr$file$transfer$service.getDataType(csConfigName);
  
  case
   when sDataType = 'B'  then
     sStmt := 'select anydata.convertBlob(' || rFileTransferConfig.blobcolumn || ') from ' || rFileTransferConfig.targetTable || ' where ' || sWhere;
   when sDataType = 'C'  then
     sStmt := 'select anydata.convertClob(' ||  rFileTransferConfig.clobColumn || ') from ' || rFileTransferConfig.targetTable || ' where ' || sWhere;
   else
    sMsg :=  utd$msglib.getmsg ('dataTypeNotDefinedError', nCurrentLanguage);
    raise dataTypeNotDefinedException;
  end case;
    
  isr$trace.debug ('sStmt', sStmt, sCurrentName);
  execute immediate sStmt into aFile;

  if sDataType = 'B' then
    isr$trace.debug ('aFile', 'aFile in logblob', sCurrentName,  anydata.AccessBlob(aFile));
  elsif sDataType = 'C' then
    isr$trace.debug ('aFile', 'aFile in logclob', sCurrentName,  anydata.AccessClob(aFile));
  end if;
  
  isr$trace.stat('end', 'end', sCurrentName);
  return aFile;
exception
  when others then
    oErrorObj.handleError ( Stb$typedef.cnSeverityCritical, 'get the file failed ' || sMsg, sCurrentName, 
                           ' the statement is: ' || sStmt || ' ' || dbms_utility.format_error_stack || CHR(10) || dbms_utility.format_error_backtrace );   
    csErrorXml := xmltype(oErrorObj).getClobVal();
    return null;
end getFileFromDb;

--*********************************************************************************************************************************
procedure saveFileInDb( csConfigName in  isr$transfer$file$config.configname%type,
                        csTransferId in  varchar2, 
                        aFile        in  anydata, 
                        csErrorXml   out clob )
is
  exConfigName        exception;
  dataTypeNotDefinedException  exception;
  sCurrentName        constant varchar2(100) := $$PLSQL_UNIT||'.saveFileInDb';
  sUserMsg            varchar2(4000);
  oErrorObj           STB$OERROR$RECORD      := STB$OERROR$RECORD(tloMessageList => ISR$MESSAGE$LIST());
  sStmt               varchar2 (2000);
  sStmtPart1             varchar2 (100);
  sStmtPart2             varchar2 (100);
  sSelect               varchar2(2000);
  sOnStmt             varchar2(2000);
  sWhere              varchar2(2000);
  sInsert               varchar2(2000);
  sInsertValues     varchar2(2000);
  
  sMultipleKeyColumn   varchar2(50);
  sMultipleKeyValue   varchar2(4000);
  nKeyNumber           number;
  sDataType     varchar2(1);
  
begin
  isr$trace.stat('begin', 'begin: csConfigName: ' || csConfigName || '  csTransferId:' || csTransferId , sCurrentName);
  
  open cFileTransferConfig(csConfigName);
  fetch cFileTransferConfig into rFileTransferConfig;
    
  if cFileTransferConfig%notfound then
    close cFileTransferConfig;
    sUserMsg := utd$msglib.getmsg ('exConfigName', nCurrentLanguage,  csP1 => csConfigName);
    raise exConfigName;
  end if;
            
  close cFileTransferConfig;
  
  sDataType := isr$file$transfer$service.getDataType(csConfigName, csTransferId);
 
  if rFileTransferConfig.key_delimiter is not null  then
    nKeyNumber := rFileTransferConfig.ndelimiters+1;
    isr$trace.debug ('nKeyNumber', nKeyNumber, sCurrentName);
    for i in 1..nKeyNumber loop
     sMultipleKeyColumn := isr$util.getSplit(rFileTransferConfig.targetTablePk, i,  rFileTransferConfig.key_delimiter);
     sMultipleKeyValue := isr$util.getSplit(csTransferId, i , rFileTransferConfig.key_delimiter);
    
     sSelect :=  sSelect  || '''' || sMultipleKeyValue || ''' ' || sMultipleKeyColumn;
     sOnStmt := sOnStmt || 'trg.'|| sMultipleKeyColumn ||' = src.'|| sMultipleKeyColumn;
     sWhere :=  sWhere || 'trg.'||sMultipleKeyColumn||'='''||sMultipleKeyValue||'''';
     sInsert := sInsert || 'trg.'||sMultipleKeyColumn;
     sInsertValues := sInsertValues || 'src.'||sMultipleKeyColumn;
     
--     isr$trace.debug ('sSelect ' || i, sSelect, sCurrentName);
--     isr$trace.debug ('sOnStmt ' || i, sOnStmt, sCurrentName);
--     isr$trace.debug ('sWhere ' || i, sWhere, sCurrentName);
     if i < nKeyNumber then
      sSelect := sSelect || ' , ';
      sOnStmt := sOnStmt || ' and ';
      sWhere := sWhere || ' and ';
      sInsert := sInsert || ' , ';
      sInsertValues := sInsertValues || ' , ';
     end if;
    end loop;
  else
    sSelect :=  '''' || csTransferID||''' ' || rFileTransferConfig.targetTablePk;
    sOnStmt :=  'trg.'|| rFileTransferConfig.targetTablePk ||' = src.'|| rFileTransferConfig.targetTablePk;
    sWhere :=  'trg.'||rFileTransferConfig.targetTablePk||'='''||csTransferId||'''';
    sInsert := 'trg.'||rFileTransferConfig.targetTablePk;
    sInsertValues := 'src.'||rFileTransferConfig.targetTablePk;
  end if;
  
  case
  when sDataType = 'B'  then
    sStmtPart1 := 'anydata.accessBlob';
    sStmtPart2 := rFileTransferConfig.BlobColumn;
  when sDataType = 'C'  then
    sStmtPart1 := 'anydata.accessClob';
    sStmtPart2 := rFileTransferConfig.ClobColumn;
  else 
    sUserMsg :=  utd$msglib.getmsg ('dataTypeNotDefinedError', nCurrentLanguage);
    raise dataTypeNotDefinedException;
  end case;
    
  sStmt :=    'merge into  '||rFileTransferConfig.targetTable||' trg'||chr(10)            
           || 'using (select '|| sSelect || ', ' || sStmtPart1 || ' (:1) ' ||sStmtPart2|| ' from dual) src' ||chr(10)
           || 'on (' ||sOnStmt||')'||chr(10)
           || 'when matched then'||chr(10)
           || '  update set trg.'||sStmtPart2||'= src.' ||sStmtPart2  ||chr(10)
           || '  where  ' || sWhere||chr(10)
           || 'when not matched then'||chr(10)
           || 'insert ( ' || sInsert ||', trg.'||sStmtPart2 || ')' ||chr(10)
                  || 'values ( '|| sInsertValues ||', src.'||sStmtPart2 || ')' ;      
   
  isr$trace.debug('sStmt', sStmt, sCurrentName);
  
  if sDataType = 'B' then
    isr$trace.debug ('aFile', 'aFile in logblob', sCurrentName,  anydata.AccessBlob(aFile));
  elsif sDataType = 'C' then
    isr$trace.debug ('aFile', 'aFile in logclob', sCurrentName,  anydata.AccessClob(aFile));
  end if;
 
  execute immediate sStmt using in aFile;
  commit;
 
  isr$trace.stat ('end', 'file is saved', sCurrentName);
exception
  when others then
    sUserMsg := sUserMsg || '  The statement is: ' || sStmt ;
    oErrorObj.handleError ( Stb$typedef.cnSeverityCritical, 'save the file failed. Error: ' || sqlerrm || sUserMsg, sCurrentName, 
                          dbms_utility.format_error_stack || CHR(10) || dbms_utility.format_error_backtrace );   
    csErrorXml := xmltype(oErrorObj).getClobVal();
end saveFileInDb;


--*********************************************************************************************************************************
function getKeyDelimiter( csConfigName in  isr$transfer$file$config.configname%type) 
  return varchar2 is
  sDelim     isr$transfer$file$config.key_delimiter%type;
begin
  select key_delimiter
  into sDelim
  from  isr$transfer$file$config
  where configname = csConfigName;
  return sDelim;
exception when others then 
  return null;
end;


function getDataType( csConfigName in varchar2) return varchar2 is
  sDataType    varchar2(1);
begin
  select 
    case 
      when blobcolumn is not null and clobcolumn is not null then 'N'
      when blobcolumn is not null then 'B' 
      when clobcolumn is not null then 'C' 
      else 'F' 
    end 
    into sDataType
    from   isr$transfer$file$config  
    where  configname = csConfigName;
  return sDataType;
exception when others then 
  return null;
end;



function getDataType( csConfigName in varchar2, csTransferId in  varchar2) return varchar2 is
  sDataType    varchar2(1);
  sMultipleKeyColumn   varchar2(50);
  sMultipleKeyValue   varchar2(4000);
  nKeyNumber           number;
begin
  sDataType := getDataType(csConfigName);
  
  if sDataType = 'N' then
  
    open cFileTransferConfig(csConfigName);
    fetch cFileTransferConfig into rFileTransferConfig;
    close cFileTransferConfig;
    
    if rFileTransferConfig.key_delimiter is not null  then
      nKeyNumber := rFileTransferConfig.ndelimiters+1;
      
      for i in 1..nKeyNumber loop
        sMultipleKeyColumn := isr$util.getSplit(rFileTransferConfig.targetTablePk, i,  rFileTransferConfig.key_delimiter);
        sMultipleKeyValue := isr$util.getSplit(csTransferId, i , rFileTransferConfig.key_delimiter);
     
        if sMultipleKeyColumn = 'BLOBFLAG' then
          case when sMultipleKeyValue is null then
            sDataType := 'C';
          else
            sDataType := sMultipleKeyValue;
          end case;
            return sDataType;
        end if;
      end loop;
    end if;
  end if;
  return sDataType;
  
exception when others then 
  return null;
end;
  
end ISR$FILE$TRANSFER$SERVICE;