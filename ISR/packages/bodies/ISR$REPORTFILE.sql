CREATE OR REPLACE PACKAGE BODY isr$reportfile
is

coFileTypeList  constant   isr$varchar$list := isr$varchar$list('GRAPHIC','TRANSFORMATION', 'ATTACHMENTHTML') ;

function copyReportFiles(cnJobid in number, cxXml IN OUT xmltype, oFileTypeList in isr$varchar$list default null) return STB$OERROR$RECORD is
  sCurrentName constant varchar2(100) := $$PLSQL_UNIT||'.copyReportFiles';
  oErrorObj               STB$OERROR$RECORD := STB$OERROR$RECORD();
  sMsg    varchar2(4000);
begin
  isr$trace.stat('begin', 'begin', sCurrentName);

  merge into  ISR$REPORT$FILE rf
  using (select jobId
                , filename
                , textfile
                , binaryfile
                , STB$JOB.getJobParameter ('REPID', cnJobId) repid
                , doc_definition
         from stb$jobtrail
        where jobid = cnJobId
          and docsize != 0
          and documenttype in (select column_value from table(nvl(oFileTypeList, coFileTypeList)))) j
  on ( rf.filename = j.filename
       and rf.repid = j.repid )
  when matched then update set rf.textfile = j.textfile
                               , rf.binaryfile = j.binaryfile
                               , rf.jobid = j.jobid
  when not matched then insert ( rf.entryid
                               , rf.repid
                               , rf.jobid
                               , rf.filename
                               , rf.textfile
                               , rf.binaryfile
                               , rf.doc_definition)
  values( SEQ$ISR$REPORT$FILE.NEXTVAL
         , j.repid
         , j.jobid
         , j.filename
         , j.textfile
         , j.binaryfile
         , j.doc_definition);
  commit;
  ISR$TREE$PACKAGE.sendSyncCall('REPORT_WORD', STB$JOB.getJobParameter ('REPID', cnJobId));
  for rGetReportFiles in (select entryid from isr$report$file where repid = STB$JOB.getJobParameter ('REPID', cnJobId)) loop
    ISR$TREE$PACKAGE.sendSyncCall('HTM_REPORT_FILE', rGetReportFiles.entryid);         
  end loop;
  isr$trace.stat('end', 'end',sCurrentName);
  return oErrorObj;
exception
   when others then
    oErrorObj.handleError ( Stb$typedef.cnSeverityCritical, sMsg, sCurrentName,
                           dbms_utility.format_error_stack || CHR(10) || dbms_utility.format_error_backtrace);
    return oErrorObj;
end copyReportFiles;

function isReportFileExist(nRepid in isr$report$file.repid%type, sFilename in isr$report$file.filename%type) return varchar2 is
  sCurrentName constant varchar2(100) := $$PLSQL_UNIT||'.isReportFileExist';
  oErrorObj               STB$OERROR$RECORD := STB$OERROR$RECORD();
  sMsg    varchar2(4000);
  sIsExist varchar2(1) := 'F';
begin
  select 'T'
  into sIsExist
  from isr$report$file
  where repid = nRepid and filename = sFilename;
  return sIsExist;
exception
   when others then
    oErrorObj.handleError ( Stb$typedef.cnSeverityCritical, sMsg, sCurrentName,
                           dbms_utility.format_error_stack || CHR(10) || dbms_utility.format_error_backtrace);
    return sIsExist;
end isReportFileExist;


end isr$reportfile; 