CREATE OR REPLACE package body               iSR$RawData
is

-- variables and constants
  csTempTabPrefix        constant varchar2(500) := STB$UTIL.GetSystemParameter('TEMP_TAB_PREFIX');

  nCurrentJobID                   number;           -- current JobID
  clXMLQuery                      clob;             -- clob for xml query

-- ****************************************************************************************************
-- local functions and procedures
-- ****************************************************************************************************


--==**********************************************************************************************************************************************==--
PROCEDURE setProgress(cnValue IN number,csStatusText IN VARCHAR2) IS
BEGIN
  STB$JOB.setProgress(cnValue,csStatusText,nCurrentJobID);
END setProgress;


--==**********************************************************************************************************************************************==--
FUNCTION createXMLEntity(cnEntry IN number, cnParentEntry IN number) RETURN STB$OERROR$RECORD
IS
  sMsg                   varchar2(4000);
  sCurrentName constant  varchar2(100) := $$PLSQL_UNIT||'.createXMLEntity('||cnEntry||',paramVersion)';
  oErrorObj STB$OERROR$RECORD := STB$OERROR$RECORD();

  nCounter  NUMBER := 0;

  sXMLQuery VARCHAR2(32767);
  sReturn   VARCHAR2(4000);

  cursor cGetAttributes is
    SELECT entityname
         , tagname1
         , tagname2
         , handlingtype
         , dynamicblock
         , (SELECT entityname
              FROM isr$meta$xml
             WHERE entryid = cnParentEntry)
              parententity
      FROM isr$meta$xml
     WHERE entryid = cnEntry;

  rGetAttributes cGetAttributes%ROWTYPE;

  cursor cGetDirectChildEntities is
    SELECT x.*
      FROM isr$meta$xml x
     WHERE parententry = cnEntry
       AND reported = 'T'
    ORDER BY ordernumber;
    
  sDynBlock  VARCHAR2(4000);
BEGIN
  isr$trace.stat('begin', 'cnEntry: '||cnEntry||',  cnParentEntry: '||cnParentEntry, sCurrentName);

  OPEN cGetAttributes;
  FETCH cGetAttributes INTO rGetAttributes;
  CLOSE cGetAttributes;

  -- (nb) 15.04.2016 setProgress(60,UTD$MSGLIB.getMsg('xml query', STB$SECURITY.getCurrentLanguage)||' '||rGetAttributes.entityname);

  IF cnParentEntry IS NOT NULL THEN
    sXMLQuery := sXMLQuery || ', ';
  END IF;

  IF rGetAttributes.tagname1 IS NOT NULL THEN
    IF cnParentEntry IS NULL THEN
      sXMLQuery := sXMLQuery || '( select';
    END IF;
    sXMLQuery := sXMLQuery || ' XMLElement ("'||rGetAttributes.tagname1||'"';
    sXMLQuery := sXMLQuery || ', ';
  END IF;

  sXMLQuery := sXMLQuery || '( select XMLAgg (';
  sXMLQuery := sXMLQuery || ' XMLElement ("'||rGetAttributes.tagname2||'"';

  IF NVL(rGetAttributes.handlingtype, 'NOT S') != 'S' THEN
    sXMLQuery := sXMLQuery || ', ';
  END IF;

  -- normal/relation
  IF rGetAttributes.handlingtype IS NULL OR rGetAttributes.handlingtype = 'R' THEN

    -- write attributes
    sXMLQuery := sXMLQuery || 'XMLAttributes ( ';
    FOR rMetaAttribute IN (SELECT ATTRNAME, DATATYPE
                           FROM ISR$META$ATTRIBUTE
                           WHERE ENTITYNAME = rGetAttributes.entityname
                           AND XMLREPRESENTATION = 'A'
                           ORDER BY ORDERNO ASC) LOOP
      EXECUTE IMMEDIATE
        'select '||STB$UTIL.getCurrentCustomPackage||'.prepareForXML('''||rGetAttributes.entityname||'.'||rMetaAttribute.attrname||''','''||rMetaAttribute.DATATYPE||''') from dual'
      INTO sReturn;
      --Replacement removed due to AMGIS-17 10.Sep 2016 HR
      --sReturn := REPLACE(REPLACE(REPLACE(REPLACE(REPLACE(sReturn,'"','&quot;'),'''','&apos;'),'<','&lt;'),'>','&gt;'),'&','&amp;');
      sXMLQuery := sXMLQuery || sReturn || ' as "'||Lower(rMetaAttribute.attrname)||'", ';
    END LOOP;
    sXMLQuery := sXMLQuery || 'null as "null")';

    -- write elements
    FOR rMetaAttribute IN ( SELECT ATTRNAME, DATATYPE
                            FROM ISR$META$ATTRIBUTE
                            WHERE ENTITYNAME = rGetAttributes.entityname
                            AND ((XMLREPRESENTATION = 'T' AND rGetAttributes.handlingtype is null)
                            OR (ISKEY = 'T' AND XMLREPRESENTATION = 'T' AND rGetAttributes.handlingtype = 'R'))
                            ORDER BY ORDERNO ASC) LOOP
      sXMLQuery := sXMLQuery || ', ';
      EXECUTE IMMEDIATE
        'select '||STB$UTIL.getCurrentCustomPackage||'.prepareForXML('''||rGetAttributes.entityname||'.'||rMetaAttribute.attrname||''','''||rMetaAttribute.DATATYPE||''') from dual'
      INTO sReturn;
      sXMLQuery := sXMLQuery || 'XMLElement ("'||lower(rMetaAttribute.attrname)||'",' || sReturn || ')';
    END LOOP;

    -- Backup
    DBMS_LOB.WRITEAPPEND(clXMLQuery, LENGTH(sXMLQuery), sXMLQuery);
    sXMLQuery := NULL;

    -- Reference
    FOR rGetChildEntities IN cGetDirectChildEntities LOOP
      oErrorObj := createXMLEntity(rGetChildEntities.entryid, cnEntry);
    END LOOP;

    sXMLQuery := sXMLQuery || ') ';
    sXMLQuery := sXMLQuery || 'order by CRITORDERBY, ENTITYORDERBY ';
    sXMLQuery := sXMLQuery || ') ';
    sXMLQuery := sXMLQuery || 'from '||csTempTabPrefix||rGetAttributes.entityname||' '||rGetAttributes.entityname||' ';
    nCounter := 0;
    FOR rMetaRelation IN (SELECT mr.PARENTATTRNAME, mr.CHILDATTRNAME, mr.RELATIONNAME
                            FROM ISR$META$RELATION mr
                               , ISR$META$RELATION$REPORT mrr
                               , ISR$META$ATTRIBUTE ma1
                               , ISR$META$ATTRIBUTE ma2
                           WHERE mr.PARENTENTITYNAME = rGetAttributes.parententity
                             AND mr.CHILDENTITYNAME = rGetAttributes.entityname
                             AND mr.RELATIONNAME = mrr.RELATIONNAME
                             AND mrr.REPORTTYPEID = STB$OBJECT.getCurrentReporttypeId
                             AND ma1.entityname = mr.PARENTENTITYNAME
                             AND ma1.attrname = mr.PARENTATTRNAME
                             AND ma1.istemp = 'F'
                             AND ma2.entityname = mr.CHILDENTITYNAME
                             AND ma2.attrname = mr.CHILDATTRNAME
                             AND ma2.istemp = 'F'
                          --AND ROWNUM < 2
                          ) LOOP
      nCounter := nCounter + 1;
      sXMLQuery := sXMLQuery||' '||case when nCounter = 1 then 'where' else 'and' end||' ('|| rGetAttributes.parententity ||'.'||rMetaRelation.parentattrname|| ' = '||rGetAttributes.entityname||'.'||rMetaRelation.childattrname;

      sXMLQuery := sXMLQuery||')';
    END LOOP;
    
    sDynBlock := rGetAttributes.dynamicBlock;
    IF TRIM(sDynBlock) IS NOT NULL THEN
      nCounter := nCounter + 1;
      sDynBlock := trim(sDynBlock);
      sDynBlock := regexp_replace(sDynBlock,'^and ',null,1,1,'i');
      sDynBlock := regexp_replace(sDynBlock,'^where ',null,1,1,'i');
      sXMLQuery := sXMLQuery || ' '||case when nCounter = 1 then 'where' else 'and' end||' ' ||sDynBlock;
    END IF;

  -- dynamic
  ELSIF rGetAttributes.handlingtype = 'D' THEN

    sXMLQuery := sXMLQuery || rGetAttributes.dynamicBlock;
    sXMLQuery := sXMLQuery || ') ';
    sXMLQuery := sXMLQuery || ') ';
    sXMLQuery := sXMLQuery || 'from dual ';

  -- substitute
  ELSIF rGetAttributes.handlingtype = 'S' THEN

    -- Backup
    DBMS_LOB.WRITEAPPEND(clXMLQuery, LENGTH(sXMLQuery), sXMLQuery);
    sXMLQuery := NULL;

    -- Reference
    FOR rGetChildEntities IN cGetDirectChildEntities LOOP
      oErrorObj := createXMLEntity(rGetChildEntities.entryid, cnEntry);
    END LOOP;
    sXMLQuery := sXMLQuery || ') ';
    sXMLQuery := sXMLQuery || ') ';
    sXMLQuery := sXMLQuery || 'from dual ';

  END IF;

  sXMLQuery := sXMLQuery || ') '; -- end tag 2

  IF rGetAttributes.tagname1 IS NOT NULL THEN
    sXMLQuery := sXMLQuery || ') '; -- end tag 1
    IF cnParentEntry IS NULL THEN
      sXMLQuery := sXMLQuery || 'from dual ';
      sXMLQuery := sXMLQuery || ') ';
    END IF;
  END IF;

  -- Backup
  DBMS_LOB.WRITEAPPEND(clXMLQuery, LENGTH(sXMLQuery), sXMLQuery);
  sXMLQuery := NULL;

  isr$trace.stat('end','rGetAttributes.entityname: '||rGetAttributes.entityname, sCurrentName);
  RETURN oErrorObj;

exception
  when others then
    sMsg :=  utd$msglib.getmsg ('exUnexpectedError', stb$security.getcurrentlanguage, csP1 => sqlerrm(sqlcode));
    oErrorObj.handleError ( Stb$typedef.cnSeverityCritical, sMsg, sCurrentName,
                            dbms_utility.format_error_stack || CHR(10) || dbms_utility.format_error_backtrace  );
    return oErrorObj;
END createXMLEntity;

--==**********************************************************************************************************************************************==--
FUNCTION createXMLEntity(csEntityList IN VARCHAR2, csParentEntity IN VARCHAR2) RETURN STB$OERROR$RECORD
IS
  sMsg                   varchar2(4000);
  sCurrentName constant  varchar2(100) := $$PLSQL_UNIT||'.createXMLEntity('||csEntityList||',varchar2Version)';
  oErrorObj STB$OERROR$RECORD := STB$OERROR$RECORD();

  nCounter  NUMBER := 0;

  sTagName1 VARCHAR2(400);
  sTagName2 VARCHAR2(400);

  csEntity  VARCHAR2(400);

  sXMLQuery VARCHAR2(32767);
  sReturn   VARCHAR2(4000);
BEGIN
  isr$trace.stat('begin', 'csEntityList: '||csEntityList||' csParentEntity: '||csParentEntity, sCurrentName);

  csEntity := NVL(substr(csEntityList, 0, INSTR(csEntityList, ',')-1), csEntityList);

  sTagName1 := REPLACE(csEntity, '$', '_');
  sTagName2 := REPLACE(csEntity, '$', '_');

  IF csParentEntity IS NULL THEN
    sXMLQuery := sXMLQuery || ', ';
  END IF;

  IF sTagName1 IS NOT NULL THEN
    IF csParentEntity IS NOT NULL THEN
      sXMLQuery := sXMLQuery || '( select';
    END IF;
    sXMLQuery := sXMLQuery || ' XMLElement ("'||sTagName1||'"';
    sXMLQuery := sXMLQuery || ', ';
  END IF;

  sXMLQuery := sXMLQuery || '( select XMLAgg (';
  sXMLQuery := sXMLQuery || ' XMLElement ("'||sTagName2||'"';

  sXMLQuery := sXMLQuery || ', ';

  -- write elements
  FOR rMetaAttribute IN ( SELECT ATTRNAME, DATATYPE
                          FROM ISR$META$ATTRIBUTE
                          WHERE ENTITYNAME = csEntity
                          AND isTemp = 'F'
                          ORDER BY ORDERNO ASC) LOOP
    sXMLQuery := sXMLQuery || ', ';
    EXECUTE IMMEDIATE
      'select '||STB$UTIL.getCurrentCustomPackage||'.prepareForXML('''||csEntity||'.'||rMetaAttribute.attrname||''','''||rMetaAttribute.DATATYPE||''') from dual'
    INTO sReturn;
    sXMLQuery := sXMLQuery || 'XMLElement ("'||lower(rMetaAttribute.attrname)||'",' || sReturn || ')';
  END LOOP;

  -- Backup
  DBMS_LOB.WRITEAPPEND(clXMLQuery, LENGTH(sXMLQuery), sXMLQuery);
  sXMLQuery := NULL;

  -- Reference
  oErrorObj := createXMLEntity(substr(csEntityList, INSTR(csEntityList, ',')+1), csEntity);

  sXMLQuery := sXMLQuery || ') ';
  sXMLQuery := sXMLQuery || 'order by CRITORDERBY, ENTITYORDERBY ';
  sXMLQuery := sXMLQuery || ') ';
  sXMLQuery := sXMLQuery || 'from '||csTempTabPrefix||csEntity||' '||csEntity||' ';
  nCounter := 0;
  FOR rMetaRelation IN (SELECT mr.PARENTATTRNAME, mr.CHILDATTRNAME, mr.RELATIONNAME
                          FROM ISR$META$RELATION mr
                             , ISR$META$RELATION$REPORT mrr
                             , ISR$META$ATTRIBUTE ma1
                             , ISR$META$ATTRIBUTE ma2
                         WHERE mr.PARENTENTITYNAME = csParentEntity
                           AND mr.CHILDENTITYNAME = csEntity
                           AND mr.RELATIONNAME = mrr.RELATIONNAME
                           AND mrr.REPORTTYPEID = STB$OBJECT.getCurrentReporttypeId
                           AND ma1.entityname = mr.PARENTENTITYNAME
                           AND ma1.attrname = mr.PARENTATTRNAME
                           AND ma1.istemp = 'F'
                           AND ma2.entityname = mr.CHILDENTITYNAME
                           AND ma2.attrname = mr.CHILDATTRNAME
                           AND ma2.istemp = 'F'
                        ) LOOP
    nCounter := nCounter + 1;
    sXMLQuery := sXMLQuery||' '||case when nCounter = 1 then 'where' else 'and' end||' ('|| csParentEntity ||'.'||rMetaRelation.parentattrname|| ' = '||csEntity||'.'||rMetaRelation.childattrname;

    sXMLQuery := sXMLQuery||')';
  END LOOP;

  sXMLQuery := sXMLQuery || ') '; -- end tag 2

  IF sTagName1 IS NOT NULL THEN
    sXMLQuery := sXMLQuery || ') '; -- end tag 1
    IF csParentEntity IS NULL THEN
      sXMLQuery := sXMLQuery || 'from dual ';
      sXMLQuery := sXMLQuery || ') ';
    END IF;
  END IF;

  -- Backup
  DBMS_LOB.WRITEAPPEND(clXMLQuery, LENGTH(sXMLQuery), sXMLQuery);
  sXMLQuery := NULL;

  iSR$Trace.stat('end', 'end', sCurrentName);
  RETURN oErrorObj;
exception
  when others then
    sMsg :=  utd$msglib.getmsg ('exUnexpectedError', stb$security.getcurrentlanguage, csP1 => sqlerrm(sqlcode));
    oErrorObj.handleError ( Stb$typedef.cnSeverityCritical, sMsg, sCurrentName,
                            dbms_utility.format_error_stack || CHR(10) || dbms_utility.format_error_backtrace  );
    return oErrorObj;
END createXMLEntity;

--==**********************************************************************************************************************************************==--
FUNCTION createXML(cnJobId IN NUMBER default null, xXml IN OUT XMLTYPE) RETURN STB$OERROR$RECORD
IS
  sMsg                  varchar2(4000);
  sCurrentName constant  varchar2(100) := $$PLSQL_UNIT||'.createXML';
  oErrorObj              STB$OERROR$RECORD := STB$OERROR$RECORD();

  clXML                  CLOB;

  sReturn                VARCHAR2(4000);
  sPLSQLBlock            VARCHAR2(4000);

  -- for temp tables
  oParamList             iSR$AttrList$Rec;

  cursor cGetStartEntities is
    SELECT REPLACE (
              REPLACE (
                 REPLACE (
                    REPLACE (
                       SYS_CONNECT_BY_PATH (
                          DECODE (tagname1, NULL, '', tagname1 || '&') || tagname2
                        , '/'
                       )
                     , '&'
                     , '/'
                    )
                  , tagname1
                 )
               , tagname2
              )
            , '//'
            , '/'
           )
              xpath
         , x.entryid
         , x.entityname
      FROM isr$meta$xml x
     WHERE parententry = STB$UTIL.getRepTypeParameter(STB$OBJECT.getCurrentReporttypeId, 'XMLSTARTENTRYID')
       AND reported = 'T'
     START WITH parententry = STB$UTIL.getRepTypeParameter(STB$OBJECT.getCurrentReporttypeId, 'XMLSTARTENTRYID')
     CONNECT BY parententry = PRIOR entryid
     ORDER SIBLINGS BY ordernumber;

  CURSOR cGetStartNode IS
    SELECT NVL (tagname1, tagname2)
      FROM isr$meta$xml
     WHERE entryid =
              STB$UTIL.getRepTypeParameter (STB$OBJECT.getCurrentReporttypeId, 'XMLSTARTENTRYID');

  sStartNode  VARCHAR2(500);

BEGIN
  iSR$Trace.stat('begin', 'cnJobId: '||cnJobId, sCurrentName);

  -- create XMLDOM
  OPEN cGetStartNode;
  FETCH cGetStartNode INTO sStartNode;
  CLOSE cGetStartNode;

  xXml := ISR$XML.initXML(sStartNode, STB$UTIL.GETSYSTEMPARAMETER('XML_ENCODING'));

  -- generate XML
  iSR$Trace.debug('starting', 'xml', sCurrentName);
  FOR rGetEntities IN cGetStartEntities LOOP
    DBMS_LOB.createTemporary(clXMLQuery, CACHE => TRUE);

    -- sql
    setProgress(60,UTD$MSGLIB.getMsg('Entity to XML -', STB$SECURITY.getCurrentLanguage)||' '||rGetEntities.entityname);
    iSR$Trace.debug('starting', 'xml query '||rGetEntities.entityname, sCurrentName);
    delete from TMP$XML;

    sPLSQLBlock := 'begin '||
                   chr(10)||
                   '  insert into TMP$XML(xml) '||
                   chr(10)||
                   '  (';
    DBMS_LOB.WRITEAPPEND(clXMLQuery, LENGTH(sPLSQLBlock), sPLSQLBlock);
    oErrorObj := createXMLEntity(rGetEntities.entryid, null);
    IF oErrorObj.sSeverityCode != Stb$typedef.cnNoError THEN
      RETURN oErrorObj;
    END IF;
    sPLSQLBlock := '  ); '||
                   chr(10)||
                   'end;';
    DBMS_LOB.WRITEAPPEND(clXMLQuery, LENGTH(sPLSQLBlock), sPLSQLBlock);

    iSR$Trace.debug('sPLSQLBlock '||rGetEntities.entityname||'.sql', 'clXMLQuery', sCurrentName, clXMLQuery);
    -- xml query into xmltype
  -- (nb) 15.04.2016     setProgress(60,UTD$MSGLIB.getMsg('xml query into xmltype', STB$SECURITY.getCurrentLanguage)||' '||rGetEntities.entityname);
    iSR$Trace.debug('starting', 'xml query into xmltype '||rGetEntities.entityname, sCurrentName);
    sReturn := STB$UTIL.execute_clob(clXMLQuery);

    IF sReturn != 'T' THEN
      iSR$Trace.error('sReturn sPLSQLBlock '||rGetEntities.entityname||'.sql', sReturn, sCurrentName, clXMLQuery);
      sMsg :=  utd$msglib.getmsg ('exUnexpectedError', stb$security.getcurrentlanguage);
      oErrorObj.handleError ( Stb$typedef.cnSeverityCritical, sMsg, sCurrentName, 'for entity '||rGetEntities.entityname||': '||chr(10)||sReturn );
      RETURN oErrorObj;
    END IF;

    SELECT ISR$XML.xmlToClob(e.xml)
      INTO clXML
      FROM tmp$xml e;

    iSR$Trace.debug('clXML '||rGetEntities.entityname||'.xml', 'clXML', sCurrentName, clXML);

    -- xmltype into dom
  -- (nb) 15.04.2016     setProgress(60,UTD$MSGLIB.getMsg('xml', STB$SECURITY.getCurrentLanguage)||' '||rGetEntities.entityname);
    iSR$Trace.debug('xXml.xml', 'xXml', sCurrentName, xXml);
    ISR$XML.InsertChildXMLFragment(xXml, '/'||sStartNode||SUBSTR(rGetEntities.xpath, 0, LENGTH(rGetEntities.xpath)-1), clXML);

    DBMS_LOB.freeTemporary(clXMLQuery);
  END LOOP;
  iSR$Trace.debug('xXml1.xml', 'xXml', sCurrentName, xXml);

  -- post XML - used normally for calculations
  setProgress(80,UTD$MSGLIB.getMsg('post xml', STB$SECURITY.getCurrentLanguage));
  EXECUTE immediate
    'BEGIN
       :1 := '|| STB$UTIL.GetCurrentCustomPackage ||'.preparePostXML(:2, :3, :4);
     END;'
  Using OUT oErrorObj, IN cnJobId, IN oParamList, IN OUT xXml;
  IF oErrorObj.sSeverityCode != Stb$typedef.cnNoError THEN
    RETURN oErrorObj;
  END IF;

  iSR$Trace.stat('end', 'end', sCurrentName);
  RETURN oErrorObj;

exception
  when others then
    sMsg :=  utd$msglib.getmsg ('exUnexpectedError', stb$security.getcurrentlanguage, csP1 => sqlerrm(sqlcode));
    oErrorObj.handleError ( Stb$typedef.cnSeverityCritical, sMsg, sCurrentName,
                            dbms_utility.format_error_stack || CHR(10) || dbms_utility.format_error_backtrace  );
    return oErrorObj;
END createXML;


-- ****************************************************************************************************
-- public functions and procedures
-- ****************************************************************************************************


--==**********************************************************************************************************************************************==--
FUNCTION adjustRawDatFile(cnJobId IN NUMBER, cxXml IN OUT XMLTYPE, csOutputId in  VARCHAR2 default null,
                         csFileId in  VARCHAR2 default null) RETURN STB$OERROR$RECORD IS
  sMsg                   varchar2(4000);
  sCurrentName constant  varchar2(100) := $$PLSQL_UNIT||'.adjustRawDatFile';
  oErrorObj              STB$OERROR$RECORD := STB$OERROR$RECORD();

  csRohData    constant  VARCHAR2(100) := 'ROHDATEN';

  clRawData              CLOB;
  xRawData               XMLTYPE;
  xWorkData              XMLTYPE;
  oParamList             iSR$AttrList$Rec;

  sOutputId VARCHAR2(50);
  sFileId   VARCHAR2(50);

  -- information for error case
  sIp                     STB$JOBSESSION.IP%TYPE;
  sPort                   STB$JOBSESSION.PORT%TYPE;

begin
  ISR$TRACE.stat('begin', 'cnJobId: '||cnJobId, sCurrentName);

    SELECT jt.Textfile
    INTO clRawData
    FROM STB$JOBTRAIL jt
   WHERE jt.jobid = cnJobID
     AND jt.documenttype = csRohData;

  /* update xml with the current formats and job information */
  IF TRIM(STB$JOB.getJobParameter ('TOKEN', cnJobId)) not like 'CAN_CREATE_CRITERIA'
  AND TRIM(STB$JOB.getJobParameter ('TOKEN', cnJobId)) not like 'CAN_DISPLAY%AUDIT'
  AND STB$JOB.getJobParameter('REPID',cnJobId) IS NOT NULL THEN

    oErrorObj := Stb$object.initRepType(STB$JOB.getJobParameter('REPORTTYPEID',cnJobId), STB$JOB.getJobParameter('REPID',cnJobId));
    oErrorObj := iSR$Metadata$Package.GetParameterList(oParamList);

    if csOutputId is null then
      SELECT CASE
                WHEN STB$JOB.getJobParameter ('TOKEN', cnJobId) = 'CAN_OFFSHOOT_DOC' THEN
                   STB$JOB.getJobParameter ('OUTPUTID', cnJobid)
                ELSE TO_CHAR(STB$DOCKS.getOutput(STB$JOB.getJobParameter ('REPID', cnJobId)))
             END
        INTO sOutputId
        FROM DUAL;
     else
      sOutputId := csOutputId;
     end if;
    if csFileId is null then
      SELECT STB$JOB.getJobParameter ('MASTERTEMPLATE', cnJobid)
        INTO sFileId
        FROM DUAL;
     else
      sFileId := csFileId;
     end if;
     isr$trace.debug('sOutputId, sFileId',sOutputId || ', ' || sFileId, sCurrentName, xWorkData);

    xRawData := ISR$XML.clobToXML(clRawData);

    SELECT XMLELEMENT ("dynamicparameter"
            , (SELECT XMLAGG (XMLELEMENT (evalname (LOWER (sParameter)), sValue))
                 FROM (SELECT sParameter, sValue
                         FROM table (oParamList.lAttributes)
                       UNION
                       SELECT 'jobid', TO_CHAR (cnJobid) FROM DUAL
                       UNION
                       SELECT 'outputid', sOutputId FROM DUAL
                       UNION
                       SELECT 'fileid', sFileId FROM DUAL
                       ORDER BY 1))
           )
      INTO xWorkData
      FROM DUAL;
    ISR$XML.deleteNode(xRawData, '/*/attributes/dynamicparameter');
    isr$trace.Debug('dynamicparameter.xml','dynamicparameter.xml', sCurrentName, xWorkData);
    ISR$XML.InsertChildXMLFragment(xRawData, '/*/attributes', ISR$XML.XMLToClob(xWorkData));

    SELECT XMLELEMENT (
              "formats"
            , (SELECT XMLAGG(XMLELEMENT (
                                "experiment"
                              , XMLAttributes (VALUE AS "name")
                              , (SELECT XMLAGG(XMLELEMENT (
                                                  "entity"
                                                , XMLAttributes (entity AS "name")
                                                , XMLELEMENT ("key", display)
                                                , XMLELEMENT ("value", info)
                                                , XMLELEMENT ("use", use)
                                                , XMLELEMENT ("order", ordernumber)
                                               ))
                                   FROM (SELECT REPLACE (
                                                   REPLACE (
                                                      rpv.entity
                                                    , STB$UTIL.GETSYSTEMPARAMETER (
                                                         'STYLE_ENTITY_PREFIX'
                                                      )
                                                   )
                                                 , '$'
                                                 , '_'
                                                )
                                                   entity
                                              , display
                                              , info
                                              , use
                                              , ordernumber
                                              , value
                                           FROM ( SELECT entity
                                                       , VALUE
                                                       , display
                                                       , info
                                                       , ordernumber
                                                       , use
                                                       , fileid
                                                    FROM isr$file$parameter$values
                                                  UNION
                                                  SELECT entity || '$STYLE'
                                                       , VALUE
                                                       , display
                                                       , style
                                                       , ordernumber
                                                       , use
                                                       , fileid
                                                    FROM isr$file$parameter$values
                                                   WHERE style IS NOT NULL
                                                  UNION
                                                  SELECT entity || '$CLASS'
                                                       , VALUE
                                                       , display
                                                       , class
                                                       , ordernumber
                                                       , use
                                                       , fileid
                                                    FROM isr$file$parameter$values
                                                   WHERE class IS NOT NULL) rpv
                                          WHERE rpv.fileid = sFileId
                                            AND rpv.ENTITY LIKE STB$UTIL.GETSYSTEMPARAMETER ('STYLE_ENTITY_PREFIX') || '%'
                                         ORDER BY rpv.entity
                                                , rpv.VALUE
                                                , rpv.ordernumber) e1
                                  WHERE e1.VALUE = e.VALUE)
                             ))
                 FROM (SELECT DISTINCT VALUE
                         FROM isr$file$parameter$values
                        WHERE reporttypeid = (SELECT reporttypeid
                                                FROM isr$report$output$type
                                               WHERE outputid = sOutputId)
                          AND (fileid = sFileId)
                          AND ENTITY LIKE STB$UTIL.GETSYSTEMPARAMETER ('STYLE_ENTITY_PREFIX') || '%'
                       ORDER BY VALUE) e)
           )
      INTO xWorkData
      FROM DUAL;
    ISR$XML.deleteNode(xRawData, '/*/formats');
    isr$trace.Debug('formats.xml','formats.xml', sCurrentName, xWorkData);
    ISR$XML.InsertChildXMLFragment(xRawData, '/*', ISR$XML.XMLToClob(xWorkData));

    clRawData := ISR$XML.XMLToClob(xRawData);

    UPDATE STB$JOBTRAIL jt
       SET jt.Textfile = clRawData
     WHERE jt.jobid = cnJobID
       AND jt.documenttype = csRohData;
    commit;

  END IF;

  isr$trace.Debug('rohdata.xml','after get raw report', sCurrentName, clRawData);

  ISR$TRACE.stat('end', 'end', sCurrentName);
  RETURN oErrorObj;

exception
  when others then
    sMsg :=  utd$msglib.getmsg ('exUnexpectedError', stb$security.getcurrentlanguage, csP1 => sqlerrm(sqlcode));
    oErrorObj.handleError ( Stb$typedef.cnSeverityCritical, sMsg, sCurrentName,
                            dbms_utility.format_error_stack || CHR(10) || dbms_utility.format_error_backtrace  );
    SELECT ip, to_char(port) INTO sIp, sPort FROM STB$JOBSESSION WHERE jobid = cnJobId;
    Stb$job.markDBJobAsBroken(cnJobId,sIp, sPort, oErrorObj.sErrorCode, oErrorObj.sErrorMessage,oErrorObj.sSeverityCode);
    return oErrorObj;
end adjustRawDatFile;

--==**********************************************************************************************************************************************==--
FUNCTION createRawDatFile(cnJobId IN NUMBER) RETURN STB$OERROR$RECORD IS
  sMsg                   varchar2(4000);
  sCurrentName constant  varchar2(100) := $$PLSQL_UNIT||'.createRawDatFile';
  oErrorObj              STB$OERROR$RECORD := STB$OERROR$RECORD();
  sIP                    VARCHAR2(16);
  sPort                  VARCHAR2(5);

  oParamList             iSR$AttrList$Rec;

  nRecordID              NUMBER;

  xXml        XMLTYPE;
  clXml       CLOB;

  CURSOR cGetCritEntities IS
    SELECT distinct ce.entityname, idprefix
      FROM (SELECT DISTINCT branch, MAX (ce.ordercol) ordercol
              FROM isr$collector$entities ce, isr$crit crit
             WHERE crit.repid = STB$OBJECT.getCurrentRepid
               AND crit.entity = ce.entityname
            GROUP BY branch) max_entities
         , isr$collector$entities ce
         , isr$meta$xml x
     WHERE max_entities.branch = ce.branch
       AND max_entities.ordercol = ce.ordercol
       AND ce.entityname = x.entityname;

  CURSOR cGetEntitiesSingle IS
    SELECT entityname, idprefix
      FROM isr$meta$xml
     WHERE entityname IN
                 (SELECT branch
                    FROM isr$collector$entities ce
                   WHERE branch NOT IN
                               (SELECT DISTINCT branch
                                  FROM isr$collector$entities ce, isr$crit crit
                                 WHERE crit.repid = STB$OBJECT.getCurrentRepid
                                   AND crit.entity = ce.entityname)
                  GROUP BY branch);

BEGIN
  ISR$TRACE.stat('begin', 'cnJobId: '||cnJobId, sCurrentName);

  nCurrentJobID   := cnJobID;

  -- Initialize parameters for this job session
  oErrorObj := Stb$object.initRepType(STB$JOB.getJobParameter('REPORTTYPEID',cnJobId), STB$JOB.getJobParameter('REPID',cnJobId));

  -- create the xml of the temporary data
  oErrorObj := createXML(cnJobId, xXml);
  IF oErrorObj.sSeverityCode != Stb$typedef.cnNoError THEN
    RETURN oErrorObj;
  END IF;

  -- create entry in STB$JOBTRAIL
  setProgress(100,UTD$MSGLIB.getMsg('save xml in jobtrail', STB$SECURITY.getCurrentLanguage));
  iSR$Trace.debug('xXml2.xml', 'xXml', sCurrentName, xXml);

  SELECT STB$JOBTRAIL$SEQ.NEXTVAL INTO nRecordID FROM DUAL;

  INSERT INTO STB$JOBTRAIL (ENTRYID
                          , JOBID
                          , REFERENCE
                          , TEXTFILE
                          , DOCUMENTTYPE
                          , MIMETYPE
                          , TIMESTAMP
                          , FILENAME
                          , BLOBFLAG
                          , DOWNLOAD)
  VALUES (nRecordID
        , cnJobId
        , cnJobId
        , ISR$XML.XMLToClob(xXml)
        , 'ROHDATEN'
        , 'text/xml'
        , SYSDATE
        , cnJobId || '.xml'
        , 'C'
        , 'F');

  COMMIT;
  ISR$TRACE.stat('end', 'end', sCurrentName);
  RETURN oErrorObj;

exception
  when others then
    rollback;
    sMsg :=  utd$msglib.getmsg ('exUnexpectedError', stb$security.getcurrentlanguage, csP1 => sqlerrm(sqlcode));
    oErrorObj.handleError ( Stb$typedef.cnSeverityCritical, sMsg, sCurrentName,
                            dbms_utility.format_error_stack || CHR(10) || dbms_utility.format_error_backtrace  );
    SELECT ip, to_char(port) INTO sIp, sPort FROM STB$JOBSESSION WHERE jobid = cnJobId;
    Stb$job.markDBJobAsBroken(cnJobId,sIp, sPort, oErrorObj.sErrorCode, oErrorObj.sErrorMessage,oErrorObj.sSeverityCode);
    return oErrorObj;
END createRawDatFile;


end iSR$RawData;