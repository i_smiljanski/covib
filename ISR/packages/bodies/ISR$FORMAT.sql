CREATE OR REPLACE PACKAGE BODY                   ISR$FORMAT IS


--==**********************************************************************************************************************************************==--
FUNCTION ToTime (csTime IN VARCHAR2, dtDate in date, cnRaiseFormatError in integer default 1) RETURN DATE
IS
  TYPE tlMask IS TABLE OF VARCHAR2 (30) INDEX BY BINARY_INTEGER;

  lMasks        tlMask;
  dtRetDate     DATE    := NULL;   
  nIdx          INTEGER := 1;   -- Loop index for the scan through the masks
  bDtConverted  BOOLEAN := FALSE; -- Boolean to terminate loop if date was converted

  PROCEDURE InitFormats
  IS
  BEGIN
    lMasks(1) := 'HH:MI AM';
    lMasks(2) := 'HH:MI:SS AM';
    lMasks(3) := 'HH24:MI';
    lMasks(4) := 'HH24:MI:SS';
  END;
BEGIN
  InitFormats;

  WHILE  nIdx IS NOT NULL AND NOT bDtConverted LOOP
    BEGIN         
      dtRetDate := TO_DATE (csTime, lMasks(nIdx));
      bDtConverted := TRUE;
    EXCEPTION
      WHEN OTHERS THEN        
        nIdx := lMasks.NEXT(nIdx);
        IF nIdx IS NULL THEN
          if cnRaiseFormatError = 1 then
            raise_application_error(-20100, 'format for '||csTime||' is not supported') ;
          else
            return null;
          end if;
        END IF;
        
    END;
  END LOOP;

  RETURN dtDate + (dtRetDate - trunc(dtRetDate));
END ToTime;

--==**********************************************************************************************************************************************==--
FUNCTION sigFigure( value IN number, nFigures IN integer) RETURN number
IS
   nNumberSignificant NUMBER; 
   sIsNegativeFlag    VARCHAR2(1):='F';
   nValue             NUMBER;       
BEGIN
   IF value IS NULL THEN
     RETURN NULL;
   END IF;
   
   IF value = 0 THEN
     RETURN 0;
   END IF;
   
   BEGIN
     IF value < 0 THEN
       sIsNegativeFlag := 'T';
       nValue := value * -1;
     ELSE 
       nValue := value;    
     END IF ;
     
     nNumberSignificant  := round(nValue * power(10,-1 * floor(log(10, nValue)-(nFigures-1)))) * power(10,floor(log(10, nValue)-(nFigures-1)));
   EXCEPTION WHEN others THEN
     nNumberSignificant := '';
   END;     
   
   IF sIsNegativeFlag = 'T' THEN
     nNumberSignificant := nNumberSignificant * -1;
   END IF;
   
   RETURN nNumberSignificant;
END sigFigure;

--==**********************************************************************************************************************************************==--
FUNCTION FormatSig(nVal IN number, nFig IN integer) RETURN  varchar2 IS
  sText varchar2(100);
  sSep varchar2(1) := trim(to_char(1/10,'D'));
  nRmd number;
  sMinus varchar2(1);
  nAbs number := abs(nVal);
  nDig number;
  sSig varchar2(100);  
  sAdd varchar2(100);
BEGIN
  IF nVal < 0 THEN 
    sMinus := '-';
  END IF;
  sText := trim(to_char(nAbs, 'TM')); 
  IF nAbs < 1 THEN
    IF substr(trim(to_char(nAbs)),1,1) != '0' THEN 
      sText:= '0'||sText;
    ELSIF nVal = 0 THEN
      sText := rpad(sText||sSep,nFig+1,'0');
    END IF;
  ELSE
    nRmd := REMAINDER(nVal, trunc(nAbs));
    IF nRmd = 0 THEN
      IF length(REPLACE(sText, sSep, NULL)) < nFig THEN
        sText := sText||sSep;
      END IF;
    END IF;
  END IF;
  IF instr(sText, sSep) != 0 and nVal != 0 THEN
    nDig := length(REPLACE(sText, sSep, NULL));
    
    sSig := trim(substr(lpad(REPLACE(sText, sSep, NULL),greatest(nFig,nDig),' '), -nFig));
    for i in 1..nFig loop
      if substr(sSig, 1,1) = '0' then
        sSig := substr(sSig,2);
      else
        exit;
      end if;
    end loop;
    sAdd := substr(rpad('a',nFig-length(sSig)+1,'0'),2);
    sText := sText||sAdd;
  END IF;  
  RETURN sMinus||sText;  
END FormatSig;

--==**********************************************************************************************************************************************==--
FUNCTION FormatRounded(nVal IN number, nDec IN number) RETURN  varchar2 IS
  sFmt varchar2(64) := '0';
BEGIN
  IF nDec  > 0 THEN
    sFmt := rpad('0D',2+nDec,'9');
  END IF;
  sFmt := lpad(sFmt,63,'9');
  RETURN trim(to_char(nVal, sFmt));
END FormatRounded;

--==**********************************************************************************************************************************************==--
FUNCTION FmtNum(nVal IN number) RETURN VARCHAR2 IS  
  nAbs number := abs(nVal);
  sText varchar2(100);
  sMinus varchar2(1);
begin
  if nAbs < 1 and nAbs > 0 then
    IF nVal < 0 THEN 
      sMinus := '-';
    END IF;
    sText := sMinus||'0'||trim(to_char(nAbs, 'TM'));     
  else
    sText := trim(to_char(nVal, 'TM')); 
  end if;
  return sText;
end FmtNum;


end iSR$Format;