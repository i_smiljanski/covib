CREATE OR REPLACE PACKAGE BODY isr$reporttype$package AS

  oFormInfo            Stb$typedef.tlFormInfo;
  sCurrentFormPackage  Stb$typedef.tLine;
  nCurrentFormNumber   INTEGER := 1;


  FirstFormSubmitted   BOOLEAN := FALSE;

  oCurrentParameter    stb$property$list;
  sFlagReportAudit     VARCHAR2 (1) := 'F';
  nCurrentLanguage     number        := stb$security.getCurrentLanguage;
  crlf                 constant varchar2(2) := chr(10)||chr(13);


  CURSOR cGetRenderingServerStatus IS
    SELECT runstatus
      FROM isr$server
     WHERE servertypeid = 14
     ORDER BY runstatus desc;

  sRSStatus ISR$SERVER.RUNSTATUS%TYPE;

  sSystemDateFormat  VARCHAR2(20) := STB$UTIL.getSystemParameter('DATEFORMAT');
  sUserName          VARCHAR2(255) := STB$SECURITY.getOracleUserName(STB$SECURITY.getCurrentUser);

--******************************************************************************
PROCEDURE sessionUpdate
IS
  sCurrentName    constant varchar2(100) := $$PLSQL_UNIT||'.sessionUpdate';
BEGIN
  isr$trace.stat('begin','begin', sCurrentName);

  -- ISRC-1044
  for rGetRepid in (select r2.repid, r2.condition
                      from stb$report r1, stb$report r2
                     where r1.title = r2.title
                       and r1.repid = STB$OBJECT.getCurrentRepid) loop
    isr$trace.debug ('rGetRepid.repid '||rGetRepid.repid,  'rGetRepid.condition '||rGetRepid.condition, sCurrentName);
    delete from ISR$REPORT$GROUPING$TABLE
     where repid = rGetRepid.repid;
    if rGetRepid.condition != 0 then
      insert into ISR$REPORT$GROUPING$TABLE (GROUPINGNAME
                                           , GROUPINGID
                                           , GROUPINGVALUE
                                           , GROUPINGDATE
                                           , REPID)
        (select rg.GROUPINGNAME
              , rg.GROUPINGID
              , rg.GROUPINGVALUE
              , rg.GROUPINGDATE
              , rg.REPID
           from isr$report$grouping rg
          where rg.repid = rGetRepid.repid);
    end if;
  end loop;

  isr$trace.stat('end','end', sCurrentName);
EXCEPTION
  WHEN OTHERS THEN
    isr$trace.error(SQLCODE, SQLERRM,sCurrentName);
END sessionUpdate;

--******************************************************************************
-- WIZARD FUNCTIONS
--******************************************************************************

--******************************************************************************
FUNCTION getNumberOfForms(csVisibleFlag IN VARCHAR2 DEFAULT null)
  RETURN NUMBER
IS
  sCurrentName    constant varchar2(100) := $$PLSQL_UNIT||'.getNumberOfForms('||csVisibleFlag||')';
  nNumberMasks NUMBER;
BEGIN
  isr$trace.stat('begin','begin', sCurrentName);
  SELECT count ( * )
    INTO nNumberMasks
    FROM isr$report$wizard
   WHERE reporttypeid = STB$OBJECT.getCurrentReporttypeId
     AND (CASE WHEN STB$SECURITY.checkGroupRight (maskno, reporttypeid) = 'T' THEN visible ELSE 'F' END = csVisibleFlag
       OR csVisibleFlag IS NULL);
  isr$trace.stat('end','nNumberMasks: '||nNumberMasks,sCurrentName);
  RETURN nNumberMasks;
END getNumberOfForms;

--******************************************************************************
FUNCTION getBackendMaskNo(nMaskNo IN NUMBER)
  RETURN NUMBER
IS
  sCurrentName    constant varchar2(100) := $$PLSQL_UNIT||'.getBackendMaskNo('||nMaskNo||')';
  nBackendMaskNo NUMBER;
BEGIN
  isr$trace.stat('begin','begin', sCurrentName);
  SELECT maskno
    INTO nBackendMaskNo
    FROM (SELECT ROWNUM currentrow, maskno
            FROM (SELECT maskno
                    FROM isr$report$wizard
                   WHERE reporttypeid = STB$OBJECT.getCurrentReporttypeId
                     AND CASE WHEN STB$SECURITY.checkGroupRight (maskno, reporttypeid) = 'T' THEN visible ELSE 'F' END = 'T'
                  ORDER BY maskno))
   WHERE currentrow = nMaskNo;
  isr$trace.stat('end','end', sCurrentName);
  RETURN nBackendMaskNo;
END getBackendMaskNo;

--******************************************************************************
FUNCTION getFrontendMaskNo(nMaskNo IN NUMBER)
  RETURN NUMBER
IS
  sCurrentName      constant varchar2(100) := $$PLSQL_UNIT||'.getFrontendMaskNo('||nMaskNo||')';
  nFrontendMaskNo NUMBER;
BEGIN
  isr$trace.stat('begin','begin', sCurrentName);
  SELECT currentrow
    INTO nFrontendMaskNo
    FROM (SELECT ROWNUM currentrow, maskno
            FROM (SELECT maskno
                    FROM isr$report$wizard
                   WHERE reporttypeid = STB$OBJECT.getCurrentReporttypeId
                     AND CASE WHEN STB$SECURITY.checkGroupRight (maskno, reporttypeid) = 'T' THEN visible ELSE 'F' END = 'T'
                  ORDER BY maskno))
   WHERE maskno = nMaskNo;
  isr$trace.stat('end','end', sCurrentName);
  RETURN nFrontendMaskNo;
END getFrontendMaskNo;

--******************************************************************************
FUNCTION isVisible(nMaskNo IN NUMBER)
  RETURN VARCHAR2
IS
  sCurrentName    constant varchar2(100) := $$PLSQL_UNIT||'.isVisible('||nMaskNo||')';
  sVisible VARCHAR2(1);
BEGIN
  isr$trace.stat('begin','begin', sCurrentName);
  SELECT CASE WHEN STB$SECURITY.checkGroupRight (maskno, reporttypeid) = 'T' THEN visible ELSE 'F' END
    INTO sVisible
    FROM isr$report$wizard
   WHERE reporttypeid = STB$OBJECT.getCurrentReporttypeId
     AND maskno = nMaskNo;
  isr$trace.stat('end','end', sCurrentName);
  RETURN sVisible;
END isVisible;

--******************************************************************************
function getCurrentFormInfo( oFormInfoRecord out isr$forminfo$record,
                             oEntityList     out stb$entity$list,
                             cnMaskNo        in  number,
                             csFrontend      in  varchar2 default 'F')
  return stb$oerror$record
is
  oErrorObj stb$oerror$record := STB$OERROR$RECORD();
  sCurrentName      CONSTANT VARCHAR2(100) := $$PLSQL_UNIT||'.GetCurrentFormInfo';

  CURSOR cGetGroupEntity (nFormNo IN NUMBER) IS
    SELECT r.parentEntityName
      FROM ISR$META$RELATION r
         , ISR$META$CRIT$GROUPING cg
         , ISR$META$CRIT cc
         , ISR$REPORT$WIZARD w
     WHERE r.ChildEntityName IN (cc.Entity, cc.critentity)
       AND r.relationtype != 'ALIAS'
       AND w.reporttypeid = STB$OBJECT.getCurrentReportTypeId
       AND w.maskno = nFormNo
       AND w.entity = cc.critentity
       AND cg.reporttypeid = w.reporttypeid
       AND cg.relationName = r.RelationName
       AND cg.critentity = cc.critentity
    ORDER BY cg.ordernumber;


  nMaskNo NUMBER;
BEGIN
  isr$trace.stat('begin','cnMaskNo: '||cnMaskNo||'  csFrontend: '||csFrontend, sCurrentName);
  oFormInfoRecord := isr$forminfo$record();
  oEntityList     := stb$entity$list();

  IF csFrontend = 'T' THEN
    nMaskNo := getBackendMaskNo(cnMaskNo);
  ELSE
    nMaskNo := cnMaskNo;
  END IF;
  isr$trace.info ('nMaskNo', nMaskNo, sCurrentName);

  sCurrentFormPackage := oFormInfo (nMaskNo).sISRPackage;
  isr$trace.info ('sCurrentFormPackage',  sCurrentFormPackage, sCurrentName);
  isr$trace.info ('oFormInfo('||nMaskNo||').sEntity', oFormInfo (nMaskNo).sEntity, sCurrentName);
  isr$trace.info ('oFormInfo('||nMaskNo||').nMaskType', oFormInfo (nMaskNo).nMaskType, sCurrentName);
  stb$object.setCurrentMasktype(oFormInfo (nMaskNo).nMaskType);

  oFormInfoRecord := ISR$FORMINFO$RECORD( cnMaskNo,
                                          oFormInfo (nMaskNo).nMaskType,
                                          oFormInfo (nMaskNo).sISRPackage,
                                          oFormInfo (nMaskNo).sheadline1,
                                          oFormInfo (nMaskNo).sheadline2,
                                          oFormInfo (nMaskNo).sheadline3,
                                          oFormInfo (nMaskNo).sEntity,
                                          oFormInfo (nMaskNo).sTitle,
                                          oFormInfo (nMaskNo).sdescription,
                                          oFormInfo (nMaskNo).stooltip,
                                          getNumberOfForms('T'));

  -- =============
  -- if the form type is  "MasterDetail" or "Special" (Grid) then retireve all entities on the selection mask
  -- with their flags Preselected = 'Y' or 'N''
  oEntityList := stb$entity$list ();

  -- GRID get all entities in a entity list
  IF oFormInfo (nMaskNo).nMaskType = stb$typedef.cnspecial THEN
    -- HR 23.Okt 2008 added in parameter nMaskNo to getEntityList
    EXECUTE IMMEDIATE 'BEGIN
                         :1 := ' || sCurrentFormPackage || '.getEntityList(:2, :3);
                       END;'
    using OUT oErrorObj, IN nMaskNo, OUT oEntityList;
  END IF;

  -- MASTER -DETAIL get the values of the master and the detail entity
  IF oFormInfo (nMaskNo).nMaskType = stb$typedef.cnmasterdetail THEN
    -- get the master entity
    oEntityList.EXTEND;
    oEntityList (1) := STB$ENTITY$RECORD();
    OPEN cGetGroupEntity (nMaskNo);
    FETCH cGetGroupEntity INTO oEntityList (1).sEnt;
    CLOSE cGetGroupEntity;
    oEntityList (1).sPreSelected := 'T';
    -- detail entity
    oEntityList.EXTEND;
    oEntityList (2) := STB$ENTITY$RECORD(oFormInfo (nMaskNo).sEntity, 'F', '');
  END IF;

  --isr$trace.debug('status','oEntityList see column LOGCLOB', sCurrentName, sys.anydata.convertCollection(oEntityList));
  --isr$trace.debug('status','oFormInfoRecord see column LOGCLOB', sCurrentName, sys.anydata.convertObject(oFormInfoRecord));
  isr$trace.stat('end','end', sCurrentName);
  RETURN oErrorObj;
EXCEPTION
  WHEN NO_DATA_FOUND THEN
     oErrorObj.handleError ( Stb$typedef.cnSeverityWarning, utd$msglib.getmsg ('exNoWizardMaskText', stb$security.getCurrentLanguage,  csP1 => sqlerrm(sqlcode)),
        sCurrentName, dbms_utility.format_error_stack ||chr(10)|| dbms_utility.format_error_backtrace );
    RETURN oErrorObj;
  WHEN OTHERS THEN
     oErrorObj.handleError ( Stb$typedef.cnSeverityCritical, utd$msglib.getmsg ('exUnexpectedError', stb$security.getCurrentLanguage,  csP1 => sqlerrm(sqlcode)),
        sCurrentName, dbms_utility.format_error_stack ||chr(10)|| dbms_utility.format_error_backtrace );
    RETURN oErrorObj;
END getCurrentFormInfo;

--******************************************************************************
FUNCTION firstform
  RETURN stb$oerror$record
IS
  oErrorObj   stb$oerror$record := STB$OERROR$RECORD();
  sCurrentName      CONSTANT VARCHAR2(100) := $$PLSQL_UNIT||'.firstform';
  nCounter    NUMBER := 0;
BEGIN
  isr$trace.stat('begin','begin', sCurrentName);

  firstFormSubmitted := FALSE;

  FOR rGetForms IN (SELECT maskno
                         , isrpackage
                         , masktype
                         , entity
                         , title
                         , headline1
                         , headline2
                         , headline3
                         , description
                         , tooltip
                      FROM isr$report$wizard
                     WHERE reporttypeid = stb$object.getCurrentReporttypeid
                    ORDER BY maskno) LOOP
    nCounter := nCounter + 1;
    oFormInfo (nCounter).nMaskType := rGetForms.masktype;
    oFormInfo (nCounter).sEntity := rGetForms.entity;
    oFormInfo (nCounter).sISRPackage := rGetForms.isrpackage;
    oFormInfo (nCounter).sTitle := utd$msglib.getmsg (rGetForms.title, stb$security.getCurrentLanguage);
    oFormInfo (nCounter).sHeadline1 := utd$msglib.getmsg (rGetForms.headline1, stb$security.getCurrentLanguage);
    oFormInfo (nCounter).sHeadline2 := utd$msglib.getmsg (rGetForms.headline2, stb$security.getCurrentLanguage);
    oFormInfo (nCounter).sHeadline3 := utd$msglib.getmsg (rGetForms.headline3, stb$security.getCurrentLanguage);
    oFormInfo (nCounter).sDescription := utd$msglib.getmsg (rGetForms.description, stb$security.getCurrentLanguage);
    oFormInfo (nCounter).sTooltip := utd$msglib.getmsg (rGetForms.tooltip, stb$security.getCurrentLanguage);
  END LOOP;

  nCurrentFormNumber := 1;
  sCurrentFormPackage := oFormInfo (1).sISRPackage;

  sFlagReportAudit := 'F';

  isr$trace.stat('end','end', sCurrentName);
  RETURN (oErrorObj);
EXCEPTION
  WHEN OTHERS THEN
     oErrorObj.handleError ( Stb$typedef.cnSeverityCritical, utd$msglib.getmsg ('exUnexpectedError', stb$security.getCurrentLanguage,  csP1 => sqlerrm(sqlcode)),
        sCurrentName, dbms_utility.format_error_stack ||chr(10)|| dbms_utility.format_error_backtrace );
    RETURN oErrorObj;
END firstform;

--******************************************************************************
function nextForm(sNavigateFlag in varchar2)
  return stb$oerror$record
IS
  sCurrentName      constant varchar2(100) := $$PLSQL_UNIT||'.nextForm('||sNavigateFlag||')';
  oErrorObj                     stb$oerror$record := STB$OERROR$RECORD();
  vCurrentCustomPkg varchar2(30);

  cursor cGetFormInfo is
    select validateFunction
    from   isr$report$wizard
    where  maskno = nCurrentFormNumber
    and    reporttypeid = stb$object.getCurrentReporttypeid
    and    'T' = case
                   when STB$SECURITY.checkGroupRight (maskno, reporttypeid) = 'T' then
                     visible
                   else 'F'
                 end
    and    trim(validateFunction) is not null;

  rGetFormInfo  cGetFormInfo%ROWTYPE;

begin
  isr$trace.stat('begin','nCurrentFormNumber: '||nCurrentFormNumber||',  sNavigateFlag: '||sNavigateFlag,sCurrentName);
  vCurrentCustomPkg := stb$util.getCurrentCustomPackage;
  isr$trace.debug('value','vCurrentCustomPkg: '||vCurrentCustomPkg,sCurrentName);

  open cGetFormInfo;
  fetch cGetFormInfo into rGetFormInfo;
  if cGetFormInfo%FOUND and UPPER(rGetFormInfo.validatefunction) in ('T', 'VALIDATE') then
    -- validate if the selection is okay
    execute immediate 'BEGIN
                          :1 :='|| vCurrentCustomPkg
                                || '.validate(:2);
                       END;'
    using out oErrorObj, in nCurrentFormNumber;
  end if;
  close cGetFormInfo;
  isr$trace.debug('code position#01','after dyn sql oErrorObj.sSeverityCode =' || oErrorObj.sSeverityCode,sCurrentName );

  if (oErrorObj.sSeverityCode in (stb$typedef.cnWizardValidateStop)) then
    isr$trace.warn('stop','validation failed',sCurrentName);
    return oErrorObj;
  end if;

  if nvl(nCurrentFormNumber,-1) < getNumberOfForms then
  -- case 1 : current and next form is a criteria form
    nCurrentFormNumber := nCurrentFormNumber + 1;
    sCurrentFormPackage := oFormInfo(nCurrentFormNumber).sISRPackage;
  elsif nvl(nCurrentFormNumber,-1) = getNumberOfForms then
  -- case 2 : Last form of the selection set null, next form are the output options
    nCurrentFormNumber := stb$typedef.cnOptionsPage;
    sCurrentFormPackage := null;
  else
    isr$trace.error('error','current formnumber is invalid',sCurrentName);
  end if;

  isr$trace.stat('end', 'nCurrentFormNumber: '||nCurrentFormNumber, sCurrentName);
  return oErrorObj;
exception
  when others then
     oErrorObj.handleError ( Stb$typedef.cnSeverityCritical, utd$msglib.getmsg ('exUnexpectedError', stb$security.getCurrentLanguage,  csP1 => sqlerrm(sqlcode)),
        sCurrentName, dbms_utility.format_error_stack ||chr(10)|| dbms_utility.format_error_backtrace );
    return oErrorObj;
END nextform;

--******************************************************************************
FUNCTION previousform (sNavigateFlag IN VARCHAR2)
  RETURN stb$oerror$record
IS
  sCurrentName      CONSTANT VARCHAR2(100) := $$PLSQL_UNIT||'.previousform('||sNavigateFlag||')';
  oErrorObj                     stb$oerror$record := STB$OERROR$RECORD();
BEGIN
  isr$trace.stat('begin','begin', sCurrentName);

  -- case  1 : criteria form > 1
  IF nvl(nCurrentFormNumber,-1) > 1 THEN
    nCurrentFormNumber := nCurrentFormNumber - 1;
    sCurrentFormPackage := oFormInfo (nCurrentFormNumber).sISRPackage;
    RETURN oErrorObj;
  --  case 2 : first Form -> back to explorer
  ELSIF nvl(nCurrentFormNumber,-1) = 1 THEN
    nCurrentFormNumber := stb$typedef.cnstartpage;
    sCurrentFormPackage := NULL;
    ROLLBACK;
  -- case 3 : from options back to criteria selection
  ELSIF nvl(nCurrentFormNumber,-1) = stb$typedef.cnoptionspage THEN
    nCurrentFormNumber := getNumberOfForms;
    sCurrentFormPackage := oFormInfo (nCurrentFormNumber).sISRPackage;
  ELSE
    isr$trace.info ('error', 'current formnumber is invalid', sCurrentName);
  END IF;

  isr$trace.stat('end','end', sCurrentName);
  RETURN oErrorObj;
EXCEPTION
  WHEN OTHERS THEN
     oErrorObj.handleError ( Stb$typedef.cnSeverityCritical, utd$msglib.getmsg ('exUnexpectedError', stb$security.getCurrentLanguage,  csP1 => sqlerrm(sqlcode)),
        sCurrentName, dbms_utility.format_error_stack ||chr(10)|| dbms_utility.format_error_backtrace );
    RETURN oErrorObj;
END previousform;

--******************************************************************************
FUNCTION initMasterList (sEntity IN VARCHAR2, lsList OUT ISR$TLRSELECTION$LIST)
  RETURN stb$oerror$record
IS
  sCurrentName      CONSTANT VARCHAR2(100) := $$PLSQL_UNIT||'.initMasterList('||sEntity||')';
  oErrorObj                     stb$oerror$record := STB$OERROR$RECORD();

  CURSOR cGetParentEntity IS
    SELECT r.parententityname, (SELECT hidemaster
                                  FROM isr$report$wizard
                                 WHERE entity = sEntity
                                   AND reporttypeid = STB$OBJECT.getCurrentReporttypeId)
      FROM isr$meta$crit$grouping mcg
         , isr$meta$relation$report rr
         , isr$meta$relation r
         , isr$meta$crit c
     WHERE mcg.critentity = sEntity
       AND mcg.reporttypeid = STB$OBJECT.getCurrentReporttypeId
       AND mcg.reporttypeid = rr.reporttypeid
       AND mcg.relationname = rr.relationname
       AND r.relationname = rr.relationname
       AND r.relationtype != 'ALIAS'
       AND r.parententityname IN (c.critentity);

  sParentEntity ISR$CRIT.ENTITY%TYPE;

  CURSOR cGetWizardAdditionalColumns IS
    SELECT a.wizardattribute
      FROM ISR$REPORT$WIZARD w, ISR$META$ATTRIBUTE a
     WHERE sEntity = a.entityname
       AND a.wizardattribute IS NOT NULL
       AND a.wizardattribute NOT IN ('HIGHLIGHT', 'ICON', 'EDITABLE', 'COLOR')
    ORDER BY orderno;

  bHide             BOOLEAN := FALSE;
  sParentHideString VARCHAR2(4000);
  sHideString       VARCHAR2(4000);
BEGIN
  isr$trace.stat('begin','begin', sCurrentName);
  lsList := ISR$TLRSELECTION$LIST();

  OPEN cGetParentEntity;
  FETCH cGetParentEntity INTO sParentEntity, sParentHideString;
  CLOSE cGetParentEntity;

  isr$trace.debug ('sParentEntity',  sParentEntity, sCurrentName);
  isr$trace.debug ('sParentHideString',  sParentHideString, sCurrentName);

  -- 08.Jun 2010 HR select entry only once for first ordernumber
  FOR rGetParentCriteria IN ( SELECT distinct entity, repid, key,
                                     min(masterkey) KEEP (dense_rank first ORDER BY ordernumber) over (partition by entity, repid, key, display) masterkey,
                                     display, info,
                                     selected,
                                     min (ordernumber) over (partition by entity, repid, key, display) ordernumber
                                FROM isr$crit
                               WHERE entity = sParentEntity
                                 AND repid = STB$OBJECT.getCurrentRepid
                              ORDER BY 8 /*ordernumber*/ ) LOOP

    bHide := FALSE;
    IF sParentHideString IS NOT NULL THEN
      sHideString := sParentHideString;
      WHILE instr(sHideString, '^') > 0 LOOP
        IF rGetParentCriteria.key like REPLACE(substr(sHideString, 0, instr(sHideString, '^') - 1),',',''',''') THEN
          bHide := TRUE;
          EXIT;
        END IF;
        sHideString := substr(sHideString, instr(sHideString, '^') + 1);
      END LOOP;
      IF length(sHideString) > 0 AND rGetParentCriteria.key like REPLACE(sHideString,',',''',''') THEN
        bHide := TRUE;
      END IF;
    END IF;

    isr$trace.debug ('bHide',  case when bHide then 'TRUE '||rGetParentCriteria.key else 'FALSE' end, sCurrentName);

    IF NOT(bHide) THEN

      lslist.EXTEND;
      lslist(lslist.count()) := ISR$OSELECTION$RECORD (rGetParentCriteria.display, rGetParentCriteria.key, rGetParentCriteria.info, 'T', rGetParentCriteria.orderNumber, rGetParentCriteria.masterkey);

      -- attributelist
      FOR rGetAdditionalColumns IN cGetWizardAdditionalColumns LOOP
        lslist(lslist.count()).TLOATTRIBUTELIST.EXTEND();
        lslist(lslist.count()).TLOATTRIBUTELIST(lslist(lslist.count()).TLOATTRIBUTELIST.count()) := ISR$ATTRIBUTE$RECORD(rGetAdditionalColumns.wizardattribute, null, 'V');
      END LOOP;

      -- additional master info in Attributes
      isr$trace.debug ('sEntity',  sEntity, sCurrentName);
      isr$trace.debug ('rGetParentCriteria.masterkey',  rGetParentCriteria.masterkey, sCurrentName);
      BEGIN
        FOR rGetAdditionalColumns IN (SELECT c.entity, c.display, t1.maskno
                                        FROM (
                                          SELECT parententityname, entity, maskno
                                          FROM (
                                            SELECT DISTINCT r.parententityname, rw.entity, rw.maskno
                                                FROM isr$report$wizard rw
                                                   , isr$meta$crit$grouping mcg
                                                   , isr$meta$relation$report rr
                                                   , isr$meta$relation r
                                               WHERE rw.reporttypeid = STB$OBJECT.getCurrentReportTypeid
                                                 AND rw.masktype = 3
                                                 AND rw.entity = mcg.critentity
                                                 AND rw.reporttypeid = mcg.reporttypeid
                                                 AND mcg.reporttypeid = rr.reporttypeid
                                                 AND mcg.relationname = rr.relationname
                                                 AND r.relationname = rr.relationname )
                                            START WITH entity = sEntity
                                            CONNECT BY PRIOR parententityname = entity
                                            ORDER BY maskno) t1, isr$crit c
                                     WHERE t1.parententityname = c.entity
                                         AND c.repid = STB$OBJECT.getCurrentRepid
                                      START WITH c.key = rGetParentCriteria.key
                                      CONNECT BY c.key = PRIOR c.masterkey
                                             AND c.repid = PRIOR c.repid
                                             AND c.entity != PRIOR c.entity
                                      ORDER BY t1.maskno) LOOP
          lslist(lslist.count()).TLOATTRIBUTELIST.EXTEND;
          lslist(lslist.count()).TLOATTRIBUTELIST(lslist(lslist.count()).TLOATTRIBUTELIST.count()) := ISR$ATTRIBUTE$RECORD(utd$msglib.getmsg ('DISPLAY_OF_'||rGetAdditionalColumns.entity, stb$security.getCurrentLanguage), rGetAdditionalColumns.display, 'V');
        END LOOP;

      EXCEPTION WHEN OTHERS THEN
        -- forget the additional columns
        isr$trace.debug (SQLCODE,  SQLERRM, 95);
      END;

    END IF;

  END LOOP;

  -- added again for ISRC-1166 (DSIS-246) because lost in dec 2015
  stb$object.SetCurrentNumberOfMasters(lslist.count);
  isr$trace.stat('end','lsList.count: '||lsList.count, sCurrentName);
  RETURN oErrorObj;
EXCEPTION
  WHEN OTHERS THEN
     oErrorObj.handleError ( Stb$typedef.cnSeverityCritical, utd$msglib.getmsg ('exUnexpectedError', stb$security.getCurrentLanguage,  csP1 => sqlerrm(sqlcode)),
        sCurrentName, dbms_utility.format_error_stack ||chr(10)|| dbms_utility.format_error_backtrace );
    RETURN oErrorObj;
END initmasterlist;

Function InitOutputOptions RETURN stb$oerror$record
IS
  sCurrentName                  CONSTANT VARCHAR2(100) := $$PLSQL_UNIT||'.InitOutputOptions';
  oErrorObj                     stb$oerror$record := STB$OERROR$RECORD();
begin

 isr$trace.stat('begin','begin', sCurrentName);

  EXECUTE IMMEDIATE 'BEGIN
                       :1 := '||sCurrentFormPackage||'.InitOutputOptions;
                     END;'
  using OUT oErrorObj;

  isr$trace.stat('end','end', sCurrentName);
  RETURN oErrorObj;
EXCEPTION
  WHEN OTHERS THEN
     oErrorObj.handleError ( Stb$typedef.cnSeverityCritical, utd$msglib.getmsg ('exUnexpectedError', stb$security.getCurrentLanguage,  csP1 => sqlerrm(sqlcode)),
        sCurrentName, dbms_utility.format_error_stack ||chr(10)|| dbms_utility.format_error_backtrace );
    RETURN oErrorObj;
end InitOutputOptions;

Function ExitOutputOptions RETURN stb$oerror$record
IS
  sCurrentName                  CONSTANT VARCHAR2(100) := $$PLSQL_UNIT||'.ExitOutputOptions';
  oErrorObj                     stb$oerror$record := STB$OERROR$RECORD();
begin

 isr$trace.stat('begin','begin', sCurrentName);

  EXECUTE IMMEDIATE 'BEGIN
                       :1 := '||sCurrentFormPackage||'.ExitOutputOptions;
                     END;'
  using OUT oErrorObj;

  isr$trace.stat('end','end', sCurrentName);
  RETURN oErrorObj;
EXCEPTION
  WHEN OTHERS THEN
     oErrorObj.handleError ( Stb$typedef.cnSeverityCritical, utd$msglib.getmsg ('exUnexpectedError', stb$security.getCurrentLanguage,  csP1 => sqlerrm(sqlcode)),
        sCurrentName, dbms_utility.format_error_stack ||chr(10)|| dbms_utility.format_error_backtrace );
    RETURN oErrorObj;
end ExitOutputOptions;

--******************************************************************************
FUNCTION getPut( nextNavigationMask IN  number,
                 oselection         OUT isr$tlrselection$list,
                 oFormInforecord    OUT isr$forminfo$record,
                 oEntityList        OUT stb$entity$list )
  RETURN stb$oerror$record
IS
  sCurrentName                  CONSTANT VARCHAR2(100) := $$PLSQL_UNIT||'.getPut('||nextNavigationMask||')';
  oErrorObj                     stb$oerror$record := STB$OERROR$RECORD();
  oWizardErrorObj               stb$oerror$record := STB$OERROR$RECORD();
  oWorstWizardErrorObj          stb$oerror$record := STB$OERROR$RECORD();
  nNextMask                     NUMBER := nextNavigationMask;
  exGenericError                exception;
BEGIN
  isr$trace.stat('begin','nCurrentFormNumber = ' || nCurrentFormNumber || ' nNextMask = ' || nNextMask, sCurrentName);
  oSelection       := ISR$TLRSELECTION$LIST();
  oFormInfoRecord  := ISR$FORMINFO$RECORD();
  oEntityList      := STB$ENTITY$LIST();

  -- ======================== navigate forwards ========================
  WHILE nCurrentFormNumber <= nNextMask LOOP
    isr$trace.debug('code position','NEXT MASK',sCurrentName);

    IF nCurrentFormNumber < nNextMask  THEN
      IF NOT(nCurrentFormNumber=1 and isVisible(nCurrentFormNumber) = 'F')
      OR FirstFormSubmitted THEN
        oWizardErrorObj := nextform ('T');

        --ISRC-1122; IS, 9.Jul 2019
        if oWizardErrorObj.sSeverityCode > oWorstWizardErrorObj.sSeverityCode then
          oWorstWizardErrorObj := oWizardErrorObj;
        end if;

        isr$trace.debug ('oWizardErrorObj', oWizardErrorObj.sSeverityCode, sCurrentName, oWizardErrorObj);
        IF oWizardErrorObj.sSeverityCode = stb$typedef.cnWizardValidateStop THEN
          nNextMask := nCurrentFormNumber;
        END IF;
      ELSIF (nCurrentFormNumber=1 and isVisible(nCurrentFormNumber) = 'F') THEN
        FirstFormSubmitted := TRUE;
      END IF;
    END IF;

    -- get current form info
    oErrorObj := getCurrentFormInfo (oFormInforecord, oEntityList, nCurrentFormNumber);
    isr$trace.debug ('MaskNo',  oFormInforecord.sEntity || ' ' || oFormInforecord.nMaskNo|| '  ' || nCurrentFormNumber, sCurrentName);

    IF oFormInforecord.nMaskType = stb$typedef.cnOutputOption THEN
      -- see [BIBC-393] and [BIBC-60]
      oErrorObj := InitOutputOptions;
      RETURN oErrorObj;
    END IF;

    stb$object.SetCurrentNumberOfMasters(0);
    CASE
      -- GRID, NOT TARGET --
      WHEN oFormInfo (nCurrentFormNumber).nMaskType = stb$typedef.cnspecial AND nCurrentFormNumber != nNextMask THEN
        isr$trace.debug('code position','GRID, NOT TARGET', sCurrentName);
        -- loop over the entities
        FOR nlineindex IN 1..oEntityList.count LOOP
          oErrorObj := getgridselection (oEntityList (nlineindex).sEnt, 'F', oselection);
        END LOOP;
      -- GRID --
      WHEN oFormInfo (nCurrentFormNumber).nMaskType = stb$typedef.cnspecial AND nCurrentFormNumber = nNextMask THEN
        isr$trace.debug('code position','GRID', sCurrentName);
        oErrorObj := getgridselection (oEntityList (1).sEnt, 'F', oselection);
      -- MASTER DETAIL, NOT TARGET  --
      WHEN oFormInfo (nCurrentFormNumber).nMaskType = stb$typedef.cnmasterdetail AND nCurrentFormNumber != nNextMask THEN
        isr$trace.debug('code position','MASTER DETAIL, NOT TARGET', sCurrentName);
        oErrorObj := initmasterlist (oEntityList (2).sEnt, oselection);
        oErrorObj := getslist (oEntityList (2).sEnt, '', 'F', oselection, nCurrentFormNumber, oFormInforecord, oEntityList);
        oErrorObj := putslist (oEntityList (2).sEnt, oselection);
      -- MASTER DETAIL --
      WHEN oFormInfo (nCurrentFormNumber).nMaskType = stb$typedef.cnmasterdetail AND nCurrentFormNumber = nNextMask THEN
        isr$trace.debug('code position','MASTER DETAIL', sCurrentName);
        oErrorObj := initmasterlist (oEntityList (2).sEnt, oselection);
      -- GENERAL --
      ELSE
        isr$trace.debug('code position','GENERAL', sCurrentName);
        oErrorObj := getslist (oFormInfo (nCurrentFormNumber).sEntity, '', 'F', oselection, nCurrentFormNumber, oFormInforecord, oEntityList);

        -- if target mask then don't write the values just display them
        IF nCurrentFormNumber != nNextMask THEN
          isr$trace.debug ('PutSList',  oFormInfo (nCurrentFormNumber).sEntity, 21);
          oErrorObj := putslist (oFormInfo (nCurrentFormNumber).sEntity, oselection);
        END IF;
    END CASE;

    -- Next
    IF nCurrentFormNumber = nNextMask THEN
      isr$trace.debug ('oWorstWizardErrorObj', oWorstWizardErrorObj.sSeverityCode, sCurrentName, oWorstWizardErrorObj);
      IF oErrorObj.sSeverityCode = stb$typedef.cnNoError THEN
        RETURN oWorstWizardErrorObj;
      ELSE
        isr$trace.debug('parameter','oSelection',sCurrentName,oSelection);
        isr$trace.debug('parameter','oFormInfoRecord',sCurrentName,oFormInfoRecord);
        isr$trace.debug('parameter','oEntityList',sCurrentName,oEntityList);
        isr$trace.stat('end','end', sCurrentName);
        RETURN oErrorObj;
      END IF;
    END IF;

  END LOOP;

  -- ======================== navigate backwards ========================
  WHILE nCurrentFormNumber >= nNextMask LOOP
    isr$trace.debug('code position','PREVIOUS MASK',sCurrentName);

    -- cleanup output options when this is current form
    oErrorObj := getCurrentFormInfo (oFormInforecord, oEntityList, nCurrentFormNumber);
    IF oFormInforecord.nMaskType = stb$typedef.cnOutputOption THEN
      oErrorObj := ExitOutputOptions;
      if oErrorObj.sSeverityCode = stb$typedef.cnseveritycritical then
         -- something unexpected happened
         raise exGenericError;
      end if;
    END IF;

    -- start with the previous mask
    -- because we did this things for the current mask already
    oWizardErrorObj := previousform ('T');
    -- get current form info
    oErrorObj := getCurrentFormInfo (oFormInforecord, oEntityList, nCurrentFormNumber);
    isr$trace.debug ('MaskNo',  oFormInforecord.sEntity || ' ' || oFormInforecord.nMaskNo, sCurrentName);

    CASE
      -- GRID --
      WHEN oFormInfo (nCurrentFormNumber).nMaskType = stb$typedef.cnspecial AND nCurrentFormNumber = nNextMask THEN
          isr$trace.debug ('GRID',  'GRID', sCurrentName);
          oErrorObj := getgridselection (oEntityList (1).sEnt, 'F', oselection);
      -- MASTER DETAIL --
      WHEN oFormInfo (nCurrentFormNumber).nMaskType = stb$typedef.cnmasterdetail AND nCurrentFormNumber = nNextMask THEN
          isr$trace.debug ('MASTER DETAIL',  'MASTER DETAIL', sCurrentName);
          oErrorObj := initmasterlist (oEntityList (2).sEnt, oselection);
      -- GENERAL --
      WHEN (oFormInfo (nCurrentFormNumber).nMaskType = stb$typedef.cnmultiselect OR oFormInfo (nCurrentFormNumber).nMaskType = stb$typedef.cnsingleselect) AND nCurrentFormNumber = nNextMask THEN
          isr$trace.debug ('GENERAL',  'GENERAL', sCurrentName);
          oErrorObj := getslist (oFormInfo (nCurrentFormNumber).sEntity, '', 'F', oselection, nCurrentFormNumber, oFormInforecord, oEntityList);
      ELSE
        NULL;
    END CASE;

    -- Previous
    IF nCurrentFormNumber = nNextMask THEN
      isr$trace.debug('parameter','oSelection',sCurrentName,oSelection);
      isr$trace.debug('parameter','oFormInfoRecord',sCurrentName,oFormInfoRecord);
      isr$trace.debug('parameter','oEntityList',sCurrentName,oEntityList);
      isr$trace.stat('end','end', sCurrentName);
      RETURN oErrorObj;
    END IF;
  END LOOP;

  isr$trace.debug('parameter','oSelection',sCurrentName,oSelection);
  isr$trace.debug('parameter','oFormInfoRecord',sCurrentName,oFormInfoRecord);
  isr$trace.debug('parameter','oEntityList',sCurrentName,oEntityList);
  isr$trace.debug('oErrorObj',oErrorObj.sSeverityCode,sCurrentName,oErrorObj);
  isr$trace.stat('end','end', sCurrentName);
  RETURN oErrorObj;
EXCEPTION
  WHEN OTHERS THEN
     oErrorObj.handleError ( Stb$typedef.cnSeverityCritical, utd$msglib.getmsg ('exUnexpectedError', stb$security.getCurrentLanguage,  csP1 => sqlerrm(sqlcode)),
        sCurrentName, dbms_utility.format_error_stack ||chr(10)|| dbms_utility.format_error_backtrace );
    RETURN oErrorObj;
END getput;

--******************************************************************************
FUNCTION getSlist ( sEntity                     IN       stb$typedef.tline,
                    sMasterKey                  IN       VARCHAR2,
                    sNavigateMode               IN       VARCHAR2,
                    oSelectionList              OUT      isr$tlrselection$list,
                    cnNextNavigationMask        IN       NUMBER,
                    oFormInforecord             IN OUT   isr$forminfo$record,
                    oEntityList                 IN OUT   stb$entity$list,
                    csFrontend                  IN       VARCHAR2 DEFAULT 'F' ) RETURN stb$oerror$record IS
  oErrorObj                     stb$oerror$record := STB$OERROR$RECORD();
  sPackageName                  stb$typedef.tline;
  sCurrentName                  CONSTANT VARCHAR2(100) := $$PLSQL_UNIT||'.getSlist('||sEntity||')';
  sMsg   varchar2(4000);

  CURSOR cGetFormPackage IS
    SELECT isrpackage
      FROM isr$report$wizard
     WHERE maskno = nCurrentFormNumber
       AND reporttypeid = stb$object.getCurrentReporttypeid;

  nMaskNo NUMBER;
BEGIN
  isr$trace.stat('begin','sEntity: '||sEntity||',  sMasterKey: '||sMasterKey||',  sNavigateMode: '||sNavigateMode||',  cnNextNavigationMask: '||cnNextNavigationMask||',  csFrontend: '||csFrontend, sCurrentName);
  isr$trace.debug('parameter','oFormInforecord',sCurrentName,oFormInforecord);
  isr$trace.debug('parameter','oEntityList',sCurrentName,oEntityList);

  oSelectionList   := ISR$TLRSELECTION$LIST();
  if not oEntityList.exists(1) then
    oEntityList := STB$ENTITY$LIST();
  end if;

  IF sNavigateMode = 'F' THEN
    OPEN cGetFormPackage;
    FETCH cGetFormPackage INTO sPackageName;
    CLOSE cGetFormPackage;
    isr$trace.debug('value','sPackageName: '||sPackageName,sCurrentName);

    EXECUTE IMMEDIATE
       'BEGIN
          :1 := '|| sPackageName ||'.GetSList(:2, :3, :4, :5);
        END;'
    using OUT oErrorObj, IN sEntity, IN sMasterKey, IN sNavigateMode, OUT oSelectionList;
    isr$trace.debug ('oErrorObj', oErrorObj.sSeverityCode, sCurrentName, oErrorObj);

  ELSE
    -- get the real mask number and navigate to it
    IF csFrontend = 'T' THEN
      nMaskNo := getBackendMaskNo(cnNextNavigationMask);
    ELSE
      nMaskNo := cnNextNavigationMask;
    END IF;
    isr$trace.debug('value','nMaskNo: '||nMaskNo,sCurrentName);
    oErrorObj := getput (nMaskNo, oSelectionList, oFormInfoRecord, oEntityList);
    isr$trace.debug('oErrorObj',oErrorObj.sSeverityCode,sCurrentName,oErrorObj);

    -- set the visible maskno number
    IF oErrorObj.sSeverityCode not in (STB$TYPEDEF.cnWizardValidateStop)  THEN
      oFormInfoRecord.nMaskNo := cnNextNavigationMask;
    ELSE
      oFormInfoRecord.nMaskNo := getFrontendMaskno(nCurrentFormNumber);
    END IF;
  END IF;

  isr$trace.debug('parameter','oSelectionList',sCurrentName,oSelectionList);
  isr$trace.debug('parameter','oFormInforecord',sCurrentName,oFormInforecord);
  isr$trace.debug('parameter','oEntityList',sCurrentName,oEntityList);
  isr$trace.debug ('oErrorObj', oErrorObj.sSeverityCode, sCurrentName, oErrorObj);
  isr$trace.stat('end','end', sCurrentName);
  RETURN oErrorObj;
EXCEPTION
  WHEN OTHERS THEN
    sMsg :=  utd$msglib.getmsg ('exUnexpectedError', nCurrentLanguage, sqlerrm(sqlcode), sMsg);
    oErrorObj.handleError ( Stb$typedef.cnSeverityCritical, sMsg, sCurrentName,
                            dbms_utility.format_error_stack || CHR(10) || dbms_utility.format_error_backtrace );
    return oErrorObj;
END getslist;

--******************************************************************************
function putSlist( sEntity    in STB$TYPEDEF.tline,
                   oSelection in ISR$TLRSELECTION$LIST )
  return stb$oerror$record
is
  sCurrentName                  Constant varchar2(100) := $$PLSQL_UNIT||'.putSlist('||sEntity||')';
  sPackageName                  stb$typedef.tline;
  oErrorObj                     stb$oerror$record := STB$OERROR$RECORD();
begin
  isr$trace.stat('begin','begin', sCurrentName);

  -- Loop over the forms list to retrieve the package
  FOR i IN 1 .. oFormInfo.count () - 1 LOOP
    IF oFormInfo (i).sEntity = sEntity THEN
      sPackageName := oFormInfo (i).sISRPackage;
      EXIT;
    END IF;
  END LOOP;

  -- on the grid several entities can be on one mask,
  -- not every one must have a single package,
  -- then set the current package
  IF sPackageName IS NULL THEN
    sPackageName := sCurrentFormPackage;
  END IF;
  isr$trace.debug ('sPackageName', 'sPackageName: ' || sPackageName, sCurrentName);


  EXECUTE IMMEDIATE 'BEGIN
                       :1 := '||sPackageName||'.PutSList(:2, :3);
                     END;'
  using OUT oErrorObj, IN sEntity, IN oselection;

  isr$trace.stat('end','end', sCurrentName);
  RETURN oErrorObj;
EXCEPTION
  WHEN OTHERS THEN
     oErrorObj.handleError ( Stb$typedef.cnSeverityCritical, utd$msglib.getmsg ('exUnexpectedError', stb$security.getCurrentLanguage,  csP1 => sqlerrm(sqlcode)),
        sCurrentName, dbms_utility.format_error_stack ||chr(10)|| dbms_utility.format_error_backtrace );
    RETURN oErrorObj;
end putslist;

--******************************************************************************
FUNCTION initList (sEntity IN VARCHAR2, lsList OUT isr$tlrselection$list)
  RETURN stb$oerror$record
IS
  sCurrentName                  CONSTANT VARCHAR2(100) := $$PLSQL_UNIT||'.initList('||sEntity||')';
  oErrorObj                     stb$oerror$record := STB$OERROR$RECORD();
  sFound                        VARCHAR2 (1) := 'F';
BEGIN
  isr$trace.stat('begin','begin', sCurrentName);
  lsList := isr$tlrselection$list ();
  oErrorObj := stb$util.checkCriteria (STB$OBJECT.getCurrentRepid, sEntity, sFound, lsList);
  isr$trace.stat ('end',  'end', sCurrentName);
  RETURN oErrorObj;
EXCEPTION
  WHEN OTHERS THEN
     oErrorObj.handleError ( Stb$typedef.cnSeverityCritical, utd$msglib.getmsg ('exUnexpectedError', stb$security.getCurrentLanguage,  csP1 => sqlerrm(sqlcode)),
        sCurrentName, dbms_utility.format_error_stack ||chr(10)|| dbms_utility.format_error_backtrace );
    RETURN oErrorObj;
END initlist;

--******************************************************************************
FUNCTION getGridSelection (sEntity IN stb$typedef.tline, sNavigateMode IN VARCHAR2, oselection OUT isr$tlrselection$list)
  RETURN stb$oerror$record
IS
  sCurrentName                  CONSTANT VARCHAR2(100) := $$PLSQL_UNIT||'.getGridSelection('||sEntity||')';
  oErrorObj                     stb$oerror$record := STB$OERROR$RECORD();
BEGIN
  isr$trace.stat('begin','begin', sCurrentName);
  oselection := isr$tlrselection$list ();
  EXECUTE IMMEDIATE 'BEGIN
                       :1 := '||sCurrentFormPackage||'.GetGridSelection(:2, :3, :4);
                     END;'
  using OUT oErrorObj, IN sEntity, IN sNavigateMode, OUT oselection;
  isr$trace.stat('end','end', sCurrentName);
  RETURN oErrorObj;
EXCEPTION
  WHEN OTHERS THEN
     oErrorObj.handleError ( Stb$typedef.cnSeverityCritical, utd$msglib.getmsg ('exUnexpectedError', stb$security.getCurrentLanguage,  csP1 => sqlerrm(sqlcode)),
        sCurrentName, dbms_utility.format_error_stack ||chr(10)|| dbms_utility.format_error_backtrace );
    RETURN oErrorObj;
END getgridselection;

--******************************************************************************
FUNCTION getGridDependency (csEntity IN VARCHAR2, oEntityList OUT stb$entity$list, cskey IN VARCHAR2)
  RETURN stb$oerror$record
IS
  sCurrentName                  CONSTANT VARCHAR2(100) := $$PLSQL_UNIT||'.getGridDependency('||csEntity||')';
  oErrorObj                     stb$oerror$record := STB$OERROR$RECORD();
  sPackageName                  stb$typedef.tline;
BEGIN
  isr$trace.stat('begin','begin', sCurrentName);

  -- loop over forms list
  FOR i IN 1 .. oFormInfo.count () - 1 LOOP
    IF oFormInfo (i).sEntity = csEntity THEN
      sPackageName := oFormInfo (i).sISRPackage;
      EXIT;
    END IF;
  END LOOP;

  -- on the grid several entities can be on one mask,
  -- not every one must have a single package,
  -- then set the current package
  IF sPackageName IS NULL THEN
    sPackageName := sCurrentFormPackage;
  END IF;
  isr$trace.debug ('sPackageName', 'sPackageName: ' || sPackageName, sCurrentName);

  EXECUTE IMMEDIATE 'BEGIN
                       :1 := '||sPackageName||'.GetGridDependency(:2, :3, :4);
                     END;'
  using OUT oErrorObj, IN csEntity, OUT oEntityList, IN cskey;

  isr$trace.stat('end','end', sCurrentName);
  RETURN oErrorObj;
EXCEPTION
  WHEN OTHERS THEN
     oErrorObj.handleError ( Stb$typedef.cnSeverityCritical, utd$msglib.getmsg ('exUnexpectedError', stb$security.getCurrentLanguage,  csP1 => sqlerrm(sqlcode)),
        sCurrentName, dbms_utility.format_error_stack ||chr(10)|| dbms_utility.format_error_backtrace );
    RETURN oErrorObj;
END getgriddependency;

--******************************************************************************
-- only needed in criteria report
--******************************************************************************
FUNCTION initForms (oform OUT isr$forminfo$list)
  RETURN stb$oerror$record
IS
  sCurrentName                  CONSTANT VARCHAR2(100) := $$PLSQL_UNIT||'.initForms';
  oErrorObj stb$oerror$record := STB$OERROR$RECORD();
BEGIN
  isr$trace.stat('begin','begin', sCurrentName);

  oform := isr$forminfo$list ();
  FOR rGetForms IN (SELECT maskno, isrpackage
                      FROM isr$report$wizard
                     WHERE reporttypeid = stb$object.getCurrentReporttypeid
                       AND entity != 'STB$OUTPUT$OPTION'
                    ORDER BY maskno) LOOP
    oform.EXTEND ();
    oform (oform.count()) := ISR$FORMINFO$RECORD(rGetForms.maskno, null, rGetForms.isrpackage);
  END LOOP;

  isr$trace.stat('end','end', sCurrentName);
  RETURN oErrorObj;
EXCEPTION
  WHEN OTHERS THEN
     oErrorObj.handleError ( Stb$typedef.cnSeverityCritical, utd$msglib.getmsg ('exUnexpectedError', stb$security.getCurrentLanguage,  csP1 => sqlerrm(sqlcode)),
        sCurrentName, dbms_utility.format_error_stack ||chr(10)|| dbms_utility.format_error_backtrace );
    RETURN oErrorObj;
END initforms;

--******************************************************************************
-- OTHER FUNCTIONS
--******************************************************************************

--*************************************************************************************************************
FUNCTION createReportSum (cnRepId IN NUMBER)
  RETURN NUMBER
IS
  sCurrentName                  CONSTANT VARCHAR2(100) := $$PLSQL_UNIT||'.createReportSum('||cnRepId||')';
  oErrorObj                     stb$oerror$record := STB$OERROR$RECORD();
  nchecksum                     NUMBER;
  resultlob                     CLOB;
  nlineindex                    NUMBER;
  nleng                         NUMBER;
  sstring                       VARCHAR2 (32767);

  CURSOR cgetcriteria (nrepid IN NUMBER) IS
    SELECT   c.entity, c.KEY, c.display, c.info, c.ordernumber, c.masterkey
    FROM     isr$crit c
    WHERE    c.repid = nrepid
    ORDER BY 1, 5, 2, 3, 4, 6;

  CURSOR cgetreportoutput (nrepid IN NUMBER) IS
    SELECT   parametername, parametervalue
    FROM     stb$reportparameter
    WHERE    repid = nrepid
    ORDER BY parametername;

  CURSOR cgetreportheaddata (nrepid IN NUMBER) IS
    SELECT VERSION, repid, title, srnumber, template, modifiedby, to_char (modifiedon, stb$typedef.csInternalDateFormat) lastupdateon
    FROM   stb$report
    WHERE  repid = nrepid;
BEGIN
  isr$trace.stat('begin','begin', sCurrentName);

  oErrorObj := isr$util.initializeSession;   -- [ISRC-1108]

  sstring := '';
  nleng := 0;
  nlineindex := 1;
  dbms_lob.createtemporary (resultlob, TRUE);

  FOR rgetcriteria IN cgetcriteria (cnrepid) LOOP
    sstring :=
         sstring
      || nvl (trim (rgetcriteria.entity), '')
      || nvl (trim (rgetcriteria.KEY), '')
      || nvl (trim (rgetcriteria.display), '')
      || nvl (trim (rgetcriteria.info), '')
      || nvl (trim (rgetcriteria.ordernumber), '')
      || nvl (trim (rgetcriteria.masterkey), '');
    dbms_lob.open (resultlob, dbms_lob.lob_readwrite);
    dbms_lob.writeappend (resultlob, length (sstring), sstring);
    dbms_lob.close (resultlob);
    isr$trace.debug_all (nlineindex, sstring, 'ISR$REPORTTYPE$PACKAGE.createReportSum');
    -- empty string
    sstring := '';
    nlineindex := nlineindex + 1;
  END LOOP;

  nleng := dbms_lob.getlength (resultlob);
  isr$trace.debug ('value','nLineIndex: '||nlineindex||',   nleng: '||nleng,sCurrentName);

  -- added checksum taking the output options and the report headdata into account JB 21.03.05
  FOR rgetreportoutput IN cgetreportoutput (cnrepid) LOOP
    sstring := sstring || nvl (trim (rgetreportoutput.parametername), '') || nvl (trim (rgetreportoutput.parametervalue), '');
    dbms_lob.open (resultlob, dbms_lob.lob_readwrite);
    dbms_lob.writeappend (resultlob, length (sstring), sstring);
    dbms_lob.close (resultlob);
    isr$trace.debug_all('value','sString: '||sString,sCurrentName);
    -- empty string
    sstring := '';
    nlineindex := nlineindex + 1;
  END LOOP;

  isr$trace.debug ('2 output option ','nLineIndex: '||nlineindex||',   nleng: '||nleng,sCurrentName);

  FOR rgetreportheaddata IN cgetreportheaddata (cnrepid) LOOP
    isr$trace.debug_all('UpdatedBy',rgetreportheaddata.modifiedby, sCurrentName);
    isr$trace.debug_all('UpdatedOn',rgetreportheaddata.lastupdateon, sCurrentName);
    sstring :=
         sstring
      || nvl (trim (rgetreportheaddata.repid), '')
      || nvl (trim (rgetreportheaddata.VERSION), '')
      || nvl (trim (rgetreportheaddata.title), '')
      || nvl (trim (rgetreportheaddata.srnumber), '')
      || nvl (trim (rgetreportheaddata.template), '')
      || nvl (trim (rgetreportheaddata.modifiedby), '')
      || nvl (trim (rgetreportheaddata.lastupdateon), '');
    dbms_lob.open (resultlob, dbms_lob.lob_readwrite);
    dbms_lob.writeappend (resultlob, length (sstring), sstring);
    dbms_lob.close (resultlob);
    isr$trace.debug_all('head data',sString,sCurrentName);
    -- empty string
    sstring := '';
    nlineindex := nlineindex + 1;
  END LOOP;

  --call function to calculate the checksum
  oerrorobj := stb$util.checksum(resultlob, nchecksum);
  -- debug manipulation error
  isr$trace.debug('value', 'nCheckSum: '||nchecksum,sCurrentName, resultlob);
  dbms_lob.freetemporary (resultlob);
  isr$trace.stat('end','end', sCurrentName);
  RETURN nchecksum;

END createReportSum;

--***********************************************************************************************************************
FUNCTION updateChecksum (cnRepId IN NUMBER)
  RETURN stb$oerror$record
IS
  sCurrentName                  CONSTANT VARCHAR2(100) := $$PLSQL_UNIT||'.updateChecksum('||cnRepId||')';
  oerrorobj                     stb$oerror$record := STB$OERROR$RECORD();
  nCheckSum                     NUMBER;
BEGIN
  isr$trace.stat('begin','begin', sCurrentName);
  nCheckSum :=  createReportSum(cnRepid);
  UPDATE stb$report
     SET checksum = nCheckSum
   WHERE repid = cnrepid;
  isr$trace.stat('end','end', sCurrentName);
  RETURN oerrorobj;
EXCEPTION
  WHEN OTHERS THEN
     oErrorObj.handleError ( Stb$typedef.cnSeverityCritical, utd$msglib.getmsg ('exUnexpectedError', stb$security.getCurrentLanguage,  csP1 => sqlerrm(sqlcode)),
        sCurrentName, dbms_utility.format_error_stack ||chr(10)|| dbms_utility.format_error_backtrace );
    RETURN oerrorobj;
END updatechecksum;

--*******************************************************************************************************************************************
FUNCTION createPassword (cnRepId IN NUMBER, spassword OUT VARCHAR2)
  RETURN stb$oerror$record
IS
  sCurrentName                  CONSTANT VARCHAR2(100) := $$PLSQL_UNIT||'.createPassword('||cnRepId||')';
  oerrorobj                     stb$oerror$record := STB$OERROR$RECORD();
  ndummy                        NUMBER;
BEGIN
  isr$trace.stat('begin','begin', sCurrentName);
  spassword := '';

  -- generate password
  FOR i IN 1 .. 8 LOOP
    SELECT abs (mod (dbms_random.random, 9)) + 1
    INTO   ndummy
    FROM   DUAL;
    spassword := spassword || to_char (ndummy);
  END LOOP;

  isr$trace.debug ('sPassword',  'sPassword ' || spassword, sCurrentName);
  spassword := STB$JAVA.md5_string (spassword);
  isr$trace.stat('end','end', sCurrentName);
  RETURN oerrorobj;
EXCEPTION
  WHEN OTHERS THEN
     oErrorObj.handleError ( Stb$typedef.cnSeverityCritical, utd$msglib.getmsg ('exUnexpectedError', stb$security.getCurrentLanguage,  csP1 => sqlerrm(sqlcode)),
        sCurrentName, dbms_utility.format_error_stack ||chr(10)|| dbms_utility.format_error_backtrace );
    RETURN oerrorobj;
END createpassword;

--*************************************************************************************************
FUNCTION changeCondition (cnCondition IN NUMBER)
  RETURN STB$OERROR$RECORD
IS
  sCurrentName                  CONSTANT VARCHAR2(100) := $$PLSQL_UNIT||'.changeCondition('||cnCondition||')';
  oError                        STB$OERROR$RECORD := STB$OERROR$RECORD();
  nRepId                        NUMBER;
BEGIN
  isr$trace.stat('begin','begin', sCurrentName);
  nRepId := STB$OBJECT.getCurrentRepId;
  isr$trace.debug (' nRepId', ' nRepId: ' || nRepId, sCurrentName);
  UPDATE stb$report
     SET condition = cnCondition,
         modifiedby = sUsername,
         modifiedon = sysdate
   WHERE repid = nRepId;
  -- checksum
  oError := updateCheckSum (nRepID);
  COMMIT;
  isr$trace.stat('end','end', sCurrentName);
  RETURN oError;
EXCEPTION
  WHEN OTHERS THEN
     oError.handleError ( Stb$typedef.cnSeverityCritical, utd$msglib.getmsg ('exUnexpectedError', stb$security.getCurrentLanguage,  csP1 => sqlerrm(sqlcode)),
        sCurrentName, dbms_utility.format_error_stack ||chr(10)|| dbms_utility.format_error_backtrace );
    RETURN oError;
END changeCondition;

--*******************************************************************************************
FUNCTION checkTitleAudit( xXml IN OUT XMLTYPE,
                          sAuditFlag OUT VARCHAR2)
  RETURN stb$oerror$record
IS
  sCurrentName                  CONSTANT VARCHAR2(100) := $$PLSQL_UNIT||'.checkTitleAudit';
  nrepid                        NUMBER;
  nlineindex                    NUMBER;
  oerrorobj                     stb$oerror$record := STB$OERROR$RECORD();

  --cursor
  -- cursor cGetChangedParameter to retrieve the modified report parameter
  CURSOR cGetChangedParameter (cnrepid IN NUMBER) IS
    SELECT nvl (r1.datagroup, 1) new_datagroup, nvl (r2.datagroup, 1) old_datagroup, nvl (r1.title, ' ') new_title, nvl (r2.title, ' ') old_title, nvl (r1.remarks, ' ') new_remarks,
           nvl (r2.remarks, ' ') old_remarks
    FROM   stb$report r1, stb$report r2
    WHERE  r1.repid = cnrepid AND r2.repid = r1.parentrepid;

  rGetChangedParameter          cGetChangedParameter%ROWTYPE;
BEGIN
  isr$trace.stat('begin','begin', sCurrentName);
  --current report id
  nrepid := stb$object.getcurrentrepid;
  nlineindex := 0;
  sAuditFlag := 'F';
  isr$trace.debug ('nRepId', nrepid, sCurrentName);
  OPEN cGetChangedParameter (nrepid);
  FETCH cGetChangedParameter
  INTO  rGetChangedParameter;
  CLOSE cGetChangedParameter;
  isr$trace.debug ('rGetChangedParameter', 'rGetChangedParameter: ' || rGetChangedParameter.new_title || ' ' || rGetChangedParameter.old_title, sCurrentName);
  isr$trace.debug ('rGetChangedParameter', 'rGetChangedParameter: ' || rGetChangedParameter.new_datagroup || ' ' || rGetChangedParameter.old_datagroup, sCurrentName);
  isr$trace.debug ('rGetChangedParameter', 'rGetChangedParameter: ' || rGetChangedParameter.new_remarks || ' ' || rGetChangedParameter.old_remarks, sCurrentName);

  IF rGetChangedParameter.new_datagroup != rGetChangedParameter.old_datagroup THEN
    sAuditFlag := 'T';
    IF ISR$XML.valueOf(xXml, 'count(/AUDITFILE/AUDITENTRY[@TYP="PARAMETER"])') = 0 THEN
      ISR$XML.createnode (xXml, '/AUDITFILE', 'AUDITENTRY', '');
      ISR$XML.setAttribute (xXml, '/AUDITFILE/AUDITENTRY[position()=last()]', 'TYP', 'PARAMETER');
    END IF;
    ISR$XML.createnode (xXml, '/AUDITFILE/AUDITENTRY[@TYP="PARAMETER" and position()=last()]', 'NAME', 'DATAGROUP');
    ISR$XML.createnode (xXml, '/AUDITFILE/AUDITENTRY[@TYP="PARAMETER" and position()=last()]', 'OLD_VALUE', rGetChangedParameter.old_datagroup);
    ISR$XML.createnode (xXml, '/AUDITFILE/AUDITENTRY[@TYP="PARAMETER" and position()=last()]', 'NEW_VALUE', rGetChangedParameter.new_datagroup);
  END IF;

  IF rGetChangedParameter.new_title != rGetChangedParameter.old_title THEN
    sAuditFlag := 'T';
    IF ISR$XML.valueOf(xXml, 'count(/AUDITFILE/AUDITENTRY[@TYP="PARAMETER"])') = 0 THEN
      ISR$XML.createnode (xXml, '/AUDITFILE', 'AUDITENTRY', '');
      ISR$XML.setAttribute (xXml, '/AUDITFILE/AUDITENTRY[position()=last()]', 'TYP', 'PARAMETER');
    END IF;
    ISR$XML.createnode (xXml, '/AUDITFILE/AUDITENTRY[@TYP="PARAMETER" and position()=last()]', 'NAME', 'TITLE');
    ISR$XML.createnode (xXml, '/AUDITFILE/AUDITENTRY[@TYP="PARAMETER" and position()=last()]', 'OLD_VALUE', rGetChangedParameter.old_title);
    ISR$XML.createnode (xXml, '/AUDITFILE/AUDITENTRY[@TYP="PARAMETER" and position()=last()]', 'NEW_VALUE', rGetChangedParameter.new_title);
  END IF;

  IF rGetChangedParameter.new_remarks != rGetChangedParameter.old_remarks THEN
    sAuditFlag := 'T';
    IF ISR$XML.valueOf(xXml, 'count(/AUDITFILE/AUDITENTRY[@TYP="PARAMETER"])') = 0 THEN
      ISR$XML.createnode (xXml, '/AUDITFILE', 'AUDITENTRY', '');
      ISR$XML.setAttribute (xXml, '/AUDITFILE/AUDITENTRY[position()=last()]', 'TYP', 'PARAMETER');
    END IF;
    ISR$XML.createnode (xXml, '/AUDITFILE/AUDITENTRY[@TYP="PARAMETER" and position()=last()]', 'NAME', 'REMARKS');
    ISR$XML.createnode (xXml, '/AUDITFILE/AUDITENTRY[@TYP="PARAMETER" and position()=last()]', 'OLD_VALUE', rGetChangedParameter.old_remarks);
    ISR$XML.createnode (xXml, '/AUDITFILE/AUDITENTRY[@TYP="PARAMETER" and position()=last()]', 'NEW_VALUE', rGetChangedParameter.new_remarks);
  END IF;

  isr$trace.debug ('end ', 'number of lines in cGetChangedParameter ' || nlineindex, sCurrentName);
  isr$trace.stat('end','end', sCurrentName);
  RETURN oerrorobj;
EXCEPTION
  WHEN NO_DATA_FOUND THEN
     oErrorObj.handleError ( Stb$typedef.cnSeverityCritical, utd$msglib.getmsg ('exUnexpectedError', stb$security.getCurrentLanguage,  csP1 => sqlerrm(sqlcode)),
        sCurrentName, dbms_utility.format_error_stack ||chr(10)|| dbms_utility.format_error_backtrace );
    RETURN oerrorobj;
  WHEN OTHERS THEN
     oErrorObj.handleError ( Stb$typedef.cnSeverityCritical, utd$msglib.getmsg ('exUnexpectedError', stb$security.getCurrentLanguage,  csP1 => sqlerrm(sqlcode)),
        sCurrentName, dbms_utility.format_error_stack ||chr(10)|| dbms_utility.format_error_backtrace );
    RETURN oerrorobj;
END checktitleaudit;

--*******************************************************************************************
FUNCTION checkParameter( xXml IN OUT XMLTYPE, sAuditFlag OUT VARCHAR2)
  RETURN stb$oerror$record
IS
  sCurrentName                  CONSTANT VARCHAR2(100) := $$PLSQL_UNIT||'.checkParameter';
  oErrorObj                     stb$oerror$record := STB$OERROR$RECORD();
  
  CURSOR cGetWordExcelType IS
    SELECT 'T'
      FROM stb$reptypeparameter
     WHERE userparameter = 'F'
       AND reporttypeid = stb$object.getCurrentReporttypeid
       AND repparameter in ('W','A')
       AND visible = 'T'
    ORDER BY orderno;
    
  sWordExcelType VARCHAR2(1) := 'F';   

  -- cursor cGetChangedParameter to retrieve the modified report parameter
  CURSOR cGetChangedParameter(csOption in varchar2 default 'standard') IS
    select rp.parametername parametername, NVL (srp1.parametervalue, rp.parametervalue) old_value, srp2.parametervalue new_value
      from stb$report rep
         , stb$reptypeparameter rp
         , stb$reportparameter srp1
         , stb$reportparameter srp2
     where rep.repid = stb$object.getCurrentRepid
       and rep.reporttypeid = rp.reporttypeid
       and srp2.parametername = case when csOption = 'word' then 'W_' end ||rp.parametername
       and srp2.repid = rep.repid
       and (repparameter in ('T') and csOption = 'standard'
         or repparameter in ('T', 'A') and csOption = 'excel'
         or repparameter in ('W', 'A') and csOption = 'word')
       and srp1.repid(+) = rep.parentrepid
       and srp1.parametername(+) = srp2.parametername
       and ( (srp1.parametervalue != srp2.parametervalue)
         or (TRIM (srp1.parametervalue) is null
         and TRIM (srp2.parametervalue) is not null)
         or (TRIM (srp1.parametervalue) is not null
         and TRIM (srp2.parametervalue) is null))
    order by rp.orderno;

  procedure writeAudit(sparametername varchar2, sold_value varchar2, snew_value varchar2, sAuditType varchar2 default 'PARAMETER') is
  begin
    isr$trace.debug('writeAudit '||sAuditType,sparametername||' '||sold_value||' '||snew_value, sCurrentName);
    sAuditFlag := 'T';
    ISR$XML.createnode (xXml, '/AUDITFILE', 'AUDITENTRY', '');
    ISR$XML.setAttribute (xXml, '/AUDITFILE/AUDITENTRY[position()=last()]', 'TYP', sAuditType);
    ISR$XML.createnode (xXml, '/AUDITFILE/AUDITENTRY[@TYP="'||sAuditType||'" and position()=last()]', 'NAME', sparametername);
    ISR$XML.createnode (xXml, '/AUDITFILE/AUDITENTRY[@TYP="'||sAuditType||'" and position()=last()]', 'OLD_VALUE', sold_value);
    ISR$XML.createnode (xXml, '/AUDITFILE/AUDITENTRY[@TYP="'||sAuditType||'" and position()=last()]', 'NEW_VALUE', snew_value);
  end;

BEGIN
  isr$trace.stat('begin','begin', sCurrentName);
  
  open cGetWordExcelType;
  fetch cGetWordExcelType into sWordExcelType;
  if cGetWordExcelType%notfound then
    sWordExcelType := 'F';
  end if;
  close cGetWordExcelType;

  sAuditFlag := 'F';
  
  if (sWordExcelType = 'T') then
    isr$trace.debug('excel','excel', sCurrentName);
    FOR rGetChangedParameter IN cGetChangedParameter('excel') LOOP
      writeAudit(rGetChangedParameter.parametername, rGetChangedParameter.old_value, rGetChangedParameter.new_value, 'PARAMETER_EXCEL');
    END LOOP;

    isr$trace.debug('word','word', sCurrentName);
    FOR rGetChangedParameter IN cGetChangedParameter('word') LOOP
      writeAudit(rGetChangedParameter.parametername, rGetChangedParameter.old_value, rGetChangedParameter.new_value, 'PARAMETER_WORD');
    END LOOP;  
  else
    isr$trace.debug('general','general', sCurrentName);
    FOR rGetChangedParameter IN cGetChangedParameter LOOP
      writeAudit(rGetChangedParameter.parametername, rGetChangedParameter.old_value, rGetChangedParameter.new_value);
    END LOOP;
  end if;

  isr$trace.stat('end','end', sCurrentName);
  RETURN oerrorobj;
EXCEPTION
  WHEN NO_DATA_FOUND THEN
     oErrorObj.handleError ( Stb$typedef.cnSeverityCritical, utd$msglib.getmsg ('exUnexpectedError', stb$security.getCurrentLanguage,  csP1 => sqlerrm(sqlcode)),
        sCurrentName, dbms_utility.format_error_stack ||chr(10)|| dbms_utility.format_error_backtrace );
    RETURN oerrorobj;
  WHEN OTHERS THEN
    oErrorObj.handleError ( Stb$typedef.cnSeverityCritical, utd$msglib.getmsg ('exUnexpectedError', stb$security.getCurrentLanguage,  csP1 => sqlerrm(sqlcode)),
        sCurrentName, dbms_utility.format_error_stack ||chr(10)|| dbms_utility.format_error_backtrace );
    RETURN oerrorobj;
END checkParameter;

--******************************************************************************
FUNCTION setJobParameter (cstoken IN isr$action.token%TYPE)
  RETURN stb$oerror$record
IS
  oErrorObj                     stb$oerror$record := STB$OERROR$RECORD();
  sCurrentName      CONSTANT VARCHAR2(100) := $$PLSQL_UNIT||'.setJobParameter (' || cstoken || ')';
  cstokentemp                   isr$action.token%TYPE;
  noutputid                     isr$report$output$type.outputid%TYPE;
  nactionid                     isr$action.actionid%TYPE;
  sfunction                     isr$report$output$type.actionfunction%TYPE;
  nRepid                        STB$REPORT.REPID%TYPE;

  olValueList ISR$VALUE$LIST;
  sMsg                  varchar2(4000);

  CURSOR cGetJobIdsWithReptype (cstoken IN isr$action.token%TYPE, csdocdefinition IN ISR$ACTION$OUTPUT.DOC_DEFINITION%TYPE) IS
    SELECT o.outputid, A.actionid
      FROM isr$action A, isr$report$output$type o, isr$action$output ao
     WHERE A.token = cstoken
       AND o.reporttypeid = stb$object.getCurrentReporttypeid
       AND A.actionid = ao.actionid
       AND o.outputid = ao.outputid
       AND ao.doc_definition = NVL (csdocdefinition, 'NOT_DETERMINED');

  CURSOR cGetJobIdsWithoutReptype (cstoken IN isr$action.token%TYPE, csdocdefinition IN ISR$ACTION$OUTPUT.DOC_DEFINITION%TYPE) IS
    SELECT ao.outputid, ao.actionid, o.actionfunction myfunction
      FROM isr$action A, isr$report$output$type o, isr$action$output ao
     WHERE (UPPER (o.token) = UPPER (cstoken)
         OR UPPER (A.token) = UPPER (cstoken))
       AND o.reporttypeid IS NULL
       AND o.outputid = ao.outputid
       AND A.actionid = ao.actionid
       AND (ao.doc_definition = csdocdefinition
         OR csdocdefinition IS NULL);

  CURSOR cGetJobIdsForRunReport IS
    SELECT DISTINCT o.outputid, a.actionid, r.repid
      FROM isr$action a
         , isr$report$output$type o
         , isr$action$output ao
         , (SELECT max(repid) repid, reporttypeid, title FROM stb$report GROUP BY reporttypeid, title) r
     WHERE r.title = STB$OBJECT.getCurrentTitle
       AND r.reporttypeid = STB$OBJECT.getCurrentReporttypeid
       AND o.reporttypeid = r.reporttypeid
       AND o.outputid = ao.outputid
       AND a.token = 'CAN_RUN_REPORT'
       AND a.actionid = ao.actionid
       AND STB$DOCKS.getOutput(r.repid) = o.outputid;

   sDocDefinition   STB$DOCTRAIL.DOC_DEFINITION%TYPE;
BEGIN
  isr$trace.stat('begin','begin', sCurrentName);

  -- call of the general job package
  SELECT stb$job$seq.NEXTVAL
  INTO   stb$job.njobid
  FROM   DUAL;

  IF cstoken  like 'CAN_DISPLAY_%AUDIT' THEN
    cstokentemp := 'CAN_DISPLAY_AUDIT';
  ELSIF cstoken IN ('CAN_RELEASE_FOR_INSPECTION') THEN
    cstokentemp := 'CAN_RUN_REPORT';
  ELSE
    cstokentemp := cstoken;
  END IF;

  IF cstokentemp = 'CAN_RUN_REPORT' THEN
    OPEN cGetJobIdsForRunReport;
    FETCH cGetJobIdsForRunReport
    INTO  noutputid, nactionid, nRepid;
    CLOSE cGetJobIdsForRunReport;
    IF STB$OBJECT.getCurrentRepid IS NULL THEN
      STB$OBJECT.setCurrentRepid(nRepid);
    END IF;
    IF STB$UTIL.getReptypeParameter(stb$object.getCurrentReporttypeid, 'LONG_RUNNING_JOB') IS NOT NULL THEN
      stb$job.setjobparameter ('JOBTYPE', 'LONG_RUNNING_JOB_'||stb$object.getCurrentReporttypeid, stb$job.njobid);
    END IF;
  ELSE
    sDocDefinition := STB$DOCKS.getDocDefType(STB$OBJECT.getCurrentDocid, Stb$object.getCurrentRepId);
    isr$trace.debug('sDocDefinition', sDocDefinition, sCurrentName);

    OPEN cGetJobIdsWithReptype (cstokentemp, sDocDefinition);
    FETCH cGetJobIdsWithReptype
    INTO  noutputid, nactionid;
    isr$trace.debug('nactionid, noutputid', nactionid || ', ' || noutputid, sCurrentName);
    IF cGetJobIdsWithReptype%NOTFOUND OR noutputid IS NULL OR nactionid IS NULL THEN
      OPEN cGetJobIdsWithoutReptype (cstokentemp, sDocDefinition);
      FETCH cGetJobIdsWithoutReptype
      INTO  noutputid, nactionid, sfunction;
      isr$trace.debug('nactionid, noutputid, sfunction', nactionid || ', ' || noutputid || ', ' ||sfunction, sCurrentName);
      CLOSE cGetJobIdsWithoutReptype;

      IF noutputid IS NULL OR nactionid IS NULL THEN
        OPEN cGetJobIdsWithoutReptype (cstokentemp, null);
        FETCH cGetJobIdsWithoutReptype
        INTO  noutputid, nactionid, sfunction;
        CLOSE cGetJobIdsWithoutReptype;
      END IF;

    END IF;
    CLOSE cGetJobIdsWithReptype;
  END IF;

  if csToken!= 'CAN_OFFSHOOT_DOC' and nactionid is null then
    sMsg := utd$msglib.getmsg ('exNotFindAction', stb$security.getCurrentLanguage, cstokentemp);
    raise stb$job.exNotFindAction;
  end if;

  stb$job.setjobparameter ('TOKEN', cstoken, stb$job.njobid);
  stb$job.setjobparameter ('OUTPUTID', noutputid, stb$job.njobid);
  stb$job.setjobparameter ('MASTERTEMPLATE', STB$OBJECT.getCurrentTemplate, stb$job.njobid);

  stb$job.setjobparameter ('ACTIONID', nactionid, stb$job.njobid);

  isr$trace.debug('ACTIONID', nactionid, sCurrentName);
  stb$job.setjobparameter ('REFERENCE', stb$object.getCurrentNodeId, stb$job.njobid);
  --  added JB   for notification pack
  stb$job.setjobparameter ('NODETYPE', STB$OBJECT.getCurrentNodeType, stb$job.njobid);
  --
  stb$job.setjobparameter ('REPORTTYPEID', stb$object.getCurrentReporttypeid, stb$job.njobid);

  IF trim (sfunction) IS NOT NULL THEN
    stb$job.setjobparameter ('FUNCTION', sfunction, stb$job.njobid);
  END IF;

  IF cstoken NOT IN ('CAN_CLONE_OUTPUT', 'CAN_DOWNLOAD_TEMPLATE', 'CAN_UPLOAD_NEW_TEMPLATE', 'CAN_DISPLAY_TYPE_AUDIT') THEN
    stb$job.setjobparameter ('SRNUMBER', stb$object.getcurrentsrnumber, stb$job.njobid);

    IF cstoken NOT IN ('CAN_DISPLAY_SRAUDIT', 'CAN_DISPLAY_HEADDATA_REPORT') THEN
      stb$job.setjobparameter ('TITLE', stb$object.getcurrenttitle, stb$job.njobid);

      IF cstoken NOT IN ('CAN_DISPLAY_ESIG') THEN
        stb$job.setjobparameter ('REPID', stb$object.getCurrentRepid, stb$job.njobid);

        IF cstoken NOT IN ('CAN_RUN_REPORT', 'CAN_CREATE_CRITERIA', 'CAN_UPLOAD_COMMON_DOC','CAN_DISPLAY_REPORT_AUDITS', 'CAN_RELEASE_FOR_INSPECTION', 'CAN_FINALIZE_REPORT') THEN
          stb$job.setjobparameter ('DOCID', stb$object.getcurrentdocid, stb$job.njobid);
        END IF;
      END IF;
    END IF;
  END IF;

  olValueList := STB$OBJECT.getCurrentValueList;
  FOR i IN 1..olValueList.Count() LOOP
    STB$JOB.setJobParameter('olValueList('||To_Char(i)||').sNodeType', olValueList(i).sNodeType, STB$JOB.nJobId);
    STB$JOB.setJobParameter('olValueList('||To_Char(i)||').sNodeId', olValueList(i).sNodeId, STB$JOB.nJobId);
    STB$JOB.setJobParameter('olValueList('||To_Char(i)||').sNodePackage', olValueList(i).sNodePackage, STB$JOB.nJobId);
    STB$JOB.setJobParameter('olValueList('||To_Char(i)||').sParameterName', olValueList(i).sParameterName, STB$JOB.nJobId);
    STB$JOB.setJobParameter('olValueList('||To_Char(i)||').sParameterValue', olValueList(i).sParameterValue, STB$JOB.nJobId);
  END LOOP;

  STB$JOB.SETJOBPARAMETER('SNODEID', STB$OBJECT.getCurrentNode().snodeid, STB$JOB.nJobId);

  isr$trace.stat('end','end', sCurrentName);
  RETURN oErrorObj;
EXCEPTION
  when stb$job.exNotFindAction then
    oErrorObj.handleError ( Stb$typedef.cnSeverityCritical, sMsg, sCurrentName, dbms_utility.format_error_stack ||stb$typedef.crlf|| dbms_utility.format_error_backtrace );
    return oErrorObj;
  WHEN OTHERS THEN
    oErrorObj.handleError ( Stb$typedef.cnSeverityCritical,  utd$msglib.getmsg ('ERROR', stb$security.getCurrentLanguage) || SQLCODE, sCurrentName, substr(dbms_utility.format_error_stack ||crlf|| dbms_utility.format_error_backtrace, 1,4000) );
    RETURN oErrorObj;
END setjobparameter;

--******************************************************************************
FUNCTION createAudit (sAuditFlag OUT VARCHAR2, xXml OUT XMLTYPE, nLastFinalRepid IN NUMBER)
  RETURN stb$oerror$record
IS
  sCurrentName                  CONSTANT VARCHAR2(100) := $$PLSQL_UNIT||'.createAudit';
  oErrorObj                     stb$oerror$record := STB$OERROR$RECORD();
  sAuditFlagHold                VARCHAR2 (1) := 'F';
  sAuditFlagtitle               VARCHAR2 (1) := 'F';
  sstatus                       VARCHAR2 (25);
  nPos                          number;
BEGIN
  isr$trace.stat('begin','begin', sCurrentName);
  sAuditFlag := 'F';
  -- create a new XML dom
  -- there is no audit it has to be created
  xXml := ISR$XML.initXml ('AUDITFILE');

  -- set the new domid in the package stb$audit
  ISR$XML.setAttribute (xXml, '/AUDITFILE', 'AUDITTYPE', 'Reportaudit');
  ISR$XML.setAttribute (xXml, '/AUDITFILE', 'REPORTTYPEID', to_char (stb$object.getCurrentReporttypeid));
  ISR$XML.setAttribute (xXml, '/AUDITFILE', 'REPID', to_char (stb$object.getCurrentRepid));

  CASE
    WHEN stb$object.getCurrentRepstatus = stb$typedef.cnStatusdraft THEN
      sstatus := 'Draft';
    WHEN stb$object.getCurrentRepstatus = stb$typedef.cnStatusInspection THEN
      sstatus := 'Inspection';
    WHEN stb$object.getCurrentRepstatus = stb$typedef.cnStatusfinal THEN
      sstatus := 'Final';
    ELSE
      sstatus := 'No Status';
  END CASE;

  ISR$XML.setAttribute (xXml, '/AUDITFILE', 'TITLE', stb$object.getCurrentTitle);
  ISR$XML.setAttribute (xXml, '/AUDITFILE', 'STATUS', sstatus);
  ISR$XML.setAttribute (xXml, '/AUDITFILE', 'VERSION', stb$object.getcurrentversion);
  ISR$XML.setAttribute (xXml, '/AUDITFILE', 'DATE', to_char (sysdate,stb$typedef.csInternalDateFormat));
  ISR$XML.setAttribute (xXml, '/AUDITFILE', 'TIMESTAMPRAW', to_char((sysdate-to_Date('01.01.2000','dd.mm.yyyy'))*24*60*60));
  ISR$XML.setAttribute (xXml, '/AUDITFILE', 'BY', sUsername);

  isr$trace.debug ('before data audit', 'sAuditFlag : ' || sAuditFlag, sCurrentName, ISR$XML.XMLToClob(xXml));
  FOR rGetForms IN (SELECT   maskno, isrpackage
                    FROM     isr$report$wizard
                    WHERE    reporttypeid = stb$object.getcurrentreporttypeid AND entity != 'STB$OUTPUT$OPTION'
                    ORDER BY maskno) LOOP
    EXECUTE IMMEDIATE
          'BEGIN
             :1:= '|| rGetForms.isrpackage || '.createAudit(:2,:3,:4,:5);
           END;'
    using OUT oErrorObj, IN OUT xXml, OUT sAuditFlagHold, IN nLastFinalRepid, IN rGetForms.maskno;

    IF oErrorObj.sSeverityCode <> stb$typedef.cnNoError THEN
      RETURN oErrorObj;
    END IF;

    IF sAuditFlagHold = 'T' THEN
      sAuditFlag := 'T';
    END IF;
  END LOOP;

  -- Server ---
  isr$trace.debug ('before server audit',  'sAuditFlag : ' || sAuditFlag, sCurrentName, ISR$XML.XMLToClob(xXml));
  nPos := 0;
  FOR rGetModifiedServer IN ( select 'DELETED' changeFlag, s.serverid key, s.alias display
                                from (select key
                                        from isr$crit c, stb$report rep
                                       where rep.repid = stb$object.getCurrentRepid
                                         and c.repid = rep.parentrepid
                                         and entity = 'SERVER'
                                      minus
                                      select key
                                        from isr$crit
                                       where repid = stb$object.getCurrentRepid
                                         and entity = 'SERVER'), isr$server s
                               where s.serverid = key
                              union
                              select 'INSERTED' changeFlag, s.serverid key, s.alias display
                                from (select key
                                        from isr$crit
                                       where repid = stb$object.getCurrentRepid
                                         and entity = 'SERVER'
                                      minus
                                      select key
                                        from isr$crit c, stb$report rep
                                       where rep.repid = stb$object.getCurrentRepid
                                         and c.repid = rep.parentrepid
                                         and entity = 'SERVER'), isr$server s
                               where s.serverid = key) LOOP
    isr$trace.debug ('rGetModifiedServer.changeFlag : ' || rGetModifiedServer.changeFlag,  'rGetModifiedServer.SERVERID : ' || rGetModifiedServer.key, sCurrentName);
    nPos := nPos + 1;
    sAuditFlag := 'T';
    if nPos = 1 then
      ISR$XML.createnode (xXml, '/AUDITFILE', 'AUDITENTRY', '');
      ISR$XML.setAttribute (xXml, '/AUDITFILE/AUDITENTRY[position()=last()]', 'TYP', 'CRITERIA');
      ISR$XML.setAttribute (xXml, '/AUDITFILE/AUDITENTRY[position()=last()]', 'ENTITY', 'SERVER');
    end if;
    ISR$XML.createnode (xXml, '/AUDITFILE/AUDITENTRY[@ENTITY="SERVER"]', rGetModifiedServer.changeFlag, '');
    ISR$XML.createnode (xXml, '/AUDITFILE/AUDITENTRY[@ENTITY="SERVER"]/'||rGetModifiedServer.changeFlag||'[position()=last()]', 'KEY', rGetModifiedServer.key);
    ISR$XML.createnode (xXml, '/AUDITFILE/AUDITENTRY[@ENTITY="SERVER"]/'||rGetModifiedServer.changeFlag||'[position()=last()]', 'DISPLAY', rGetModifiedServer.display);
  END LOOP;


  -- Report Parameter .---
  isr$trace.debug ('before parameter audit',  'sAuditFlag : ' || sAuditFlag, sCurrentName, ISR$XML.XMLToClob(xXml));
  IF (oErrorObj.sSeverityCode = stb$typedef.cnNoError) THEN
    -- check if there are changes on the report parameters
    oErrorObj := checkParameter (xXml, sAuditFlagHold);

    IF sAuditFlagHold = 'T' THEN
      sAuditFlag := 'T';
    END IF;
  END IF;

  -- check if for a final version the report group otr the title have been switched
  isr$trace.debug ('before title audit',  'sAuditFlag : ' || sAuditFlag, sCurrentName, ISR$XML.XMLToClob(xXml));
  IF (oErrorObj.sSeverityCode = stb$typedef.cnNoError) THEN
    oErrorObj := checktitleaudit (xXml, sAuditFlagtitle);

    IF sAuditFlagtitle = 'T' THEN
      sAuditFlag := 'T';
    END IF;
  END IF;

  stb$audit.setcurrentxml (xXml);

  isr$trace.stat('end','end', sCurrentName);
  RETURN oErrorObj;
EXCEPTION
  WHEN OTHERS THEN
     oErrorObj.handleError ( Stb$typedef.cnSeverityCritical, utd$msglib.getmsg ('exUnexpectedError', stb$security.getCurrentLanguage,  csP1 => sqlerrm(sqlcode)),
        sCurrentName, dbms_utility.format_error_stack ||chr(10)|| dbms_utility.format_error_backtrace );
    RETURN oErrorObj;
END createaudit;

--***************************************************************************************************
FUNCTION checkAuditRequired (saudit OUT VARCHAR2, nAuditRepid OUT NUMBER)
  RETURN stb$oerror$record
IS
  sCurrentName                  CONSTANT VARCHAR2(100) := $$PLSQL_UNIT||'.checkAuditRequired';
  oerror                        stb$oerror$record := STB$OERROR$RECORD();
  nParentRepid                  NUMBER;
  nstatus                       NUMBER;

  CURSOR cGetParentRepid IS
    SELECT parentrepid, status, lastFinalVersion
    FROM   stb$report
    WHERE  repid = stb$object.getCurrentRepid;
BEGIN
  isr$trace.stat('begin','begin', sCurrentName);
  -- check if an parent reportid exists -> only then an audit file can be created
  OPEN cGetParentRepid;
  FETCH cGetParentRepid INTO nParentRepid, nstatus, nAuditRepid;
  CLOSE cGetParentRepid;
  isr$trace.debug ('nParentRepid/nStatus', 'nParentRepid' || nParentRepid || '  nStatus: ' || nstatus || ' nAuditRepid: ' || nAuditRepid, sCurrentName);

  CASE
    -- ISRC-930
    WHEN nParentRepid IS NULL THEN
      -- no parent version
      saudit := 'T';
      RETURN oerror;
    WHEN nAuditRepid = -1 AND nstatus = stb$typedef.cnStatusfinal THEN
      saudit := 'F';
      RETURN oerror;
    WHEN nAuditRepid != -1 AND nstatus = stb$typedef.cnStatusfinal THEN
      saudit := 'T';
    ELSE
      -- draft report and has parent version
      -- if Audit required check the audit is between draft versions
      saudit := STB$UTIL.getReptypeParameter(stb$object.getCurrentReporttypeid, 'AUDIT_BETWEEN_DRAFT');
      --get the parentReportId into the auditReportId
      nAuditRepid := nParentRepid;
  END CASE;

  isr$trace.stat('end','end', sCurrentName);
  RETURN oerror;
EXCEPTION
  WHEN OTHERS THEN
     oError.handleError ( Stb$typedef.cnSeverityCritical, utd$msglib.getmsg ('exUnexpectedError', stb$security.getCurrentLanguage,  csP1 => sqlerrm(sqlcode)),
        sCurrentName, dbms_utility.format_error_stack ||chr(10)|| dbms_utility.format_error_backtrace );
    RETURN oerror;
END checkauditrequired;

--**********************************************************************************************************
FUNCTION checkVersionRequired( cnReportTypeId IN NUMBER, sRequired OUT VARCHAR2)
  RETURN stb$oerror$record
IS
  sCurrentName                  CONSTANT VARCHAR2(100) := $$PLSQL_UNIT||'.checkVersionRequired';
  oerror                        stb$oerror$record := STB$OERROR$RECORD();
BEGIN
  isr$trace.stat('begin','begin', sCurrentName);

  -- check if the report type requires versioning
  srequired := nvl(STB$UTIL.getReptypeParameter(cnreporttypeid, 'REQUIRES_VERSIONING'), 'F');

  isr$trace.stat('end','end', sCurrentName);
  RETURN oerror;
EXCEPTION
  WHEN OTHERS THEN
     oError.handleError ( Stb$typedef.cnSeverityCritical, utd$msglib.getmsg ('exUnexpectedError', stb$security.getCurrentLanguage,  csP1 => sqlerrm(sqlcode)),
        sCurrentName, dbms_utility.format_error_stack ||chr(10)|| dbms_utility.format_error_backtrace );
    RETURN oerror;
END checkversionrequired;

--**********************************************************************************************************
FUNCTION getlastFinalVersion (nLastFinalRepid OUT NUMBER)
  RETURN stb$oerror$record
IS
  sCurrentName                  CONSTANT VARCHAR2(100) := $$PLSQL_UNIT||'.getlastFinalVersion';
  oerrorobj                     stb$oerror$record := STB$OERROR$RECORD();
  nrepid                        NUMBER;
  nstatus                       NUMBER;
  nparentid                     NUMBER;

  -- Cursors
  CURSOR cgetparentid (nrepid IN NUMBER) IS
    SELECT parentrepid
    FROM   stb$report
    WHERE  repid = nrepid;

  CURSOR cgetstatus (nparentid IN NUMBER) IS
    SELECT repid, status, parentrepid
    FROM   stb$report
    WHERE  repid = nparentid;
BEGIN
  isr$trace.stat('begin','begin', sCurrentName);
  nrepid := stb$object.getCurrentRepid;
  OPEN cgetparentid (nrepid);
  FETCH cgetparentid INTO nparentid;

  IF cgetparentid%NOTFOUND OR nparentid IS NULL THEN
    isr$trace.debug ('cGetParentId%notfound ', 'cGetParentId%notfound for nRepId: ' || nrepid, sCurrentName);
    nLastFinalRepid := -1;
  END IF;

  CLOSE cgetparentid;
  isr$trace.debug ('nParentID', 'nParentID: ' || nparentid, sCurrentName);

  WHILE nparentid >= 1 LOOP
    OPEN cgetstatus (nparentid);
    FETCH cgetstatus INTO nrepid, nstatus, nparentid;

    IF cgetstatus%NOTFOUND THEN
      isr$trace.debug ('cGetStatus%notfound ', 'cGetStatus%notfound for nParentId: ' || nparentid, sCurrentName);
      CLOSE cgetstatus;
      EXIT;
    END IF;

    CLOSE cgetstatus;

    IF nstatus = stb$typedef.cnStatusfinal THEN
      nLastFinalRepid := nrepid;
      EXIT;
    ELSE
      nLastFinalRepid := -1;
    END IF;
  END LOOP;
  
  nLastFinalRepid := nvl(nLastFinalRepid, -1);

  isr$trace.stat('end','nLastFinalRepid='||nLastFinalRepid, sCurrentName);
  RETURN oerrorobj;
EXCEPTION
  WHEN OTHERS THEN
     oErrorObj.handleError ( Stb$typedef.cnSeverityCritical, utd$msglib.getmsg ('exUnexpectedError', stb$security.getCurrentLanguage,  csP1 => sqlerrm(sqlcode)),
        sCurrentName, dbms_utility.format_error_stack ||chr(10)|| dbms_utility.format_error_backtrace );
    RETURN oerrorobj;
END getlastFinalVersion;

--********************************************************************************************************
FUNCTION createNewReport (nart IN NUMBER, nnewrepid OUT NUMBER)
  RETURN stb$oerror$record
IS
  sCurrentName                  CONSTANT VARCHAR2(100) := $$PLSQL_UNIT||'.createNewReport ('||nart||', '|| nnewrepid||')';
  oerrorobj                     stb$oerror$record := STB$OERROR$RECORD();
  nparentid                     stb$report.parentrepid%TYPE;
  ncount                        NUMBER;
  sversionrequired              VARCHAR2 (1);
  sversion                      stb$report.VERSION%TYPE;
  nversioncounter               NUMBER;
  ndummy                        NUMBER;
  nLastFinalRepid               NUMBER;
  spassword                     stb$report.PASSWORD%TYPE;
  noldrepid                     NUMBER;
  ncurrentreptype               NUMBER;
  nCurrentTemplate              NUMBER;

  -- Cursor
   -- cursor to retrieve the parameters which have to be copied ReportFlag ='T'
  CURSOR cgetReptypeParameter (cnreptype IN NUMBER) IS
    SELECT parametername, parametervalue
    FROM   stb$reptypeparameter
    WHERE  reporttypeid = cnreptype AND repparameter = 'T';

  --Cursor to retrieve the report parameters
  CURSOR cgetrepparameter (cnoldrepid IN NUMBER) IS
    SELECT parametername, parametervalue
    FROM   stb$reportparameter
    WHERE  repid = cnoldrepid;

   -- for variation  --added JB 28 Aug 2008
  CURSOR cgetReptypeParameter2(cnreptype IN NUMBER,cnoldrepid IN NUMBER) IS
    SELECT rtp.parametername
         , NVL (rp.parametervalue, rtp.parametervalue) parametervalue
      FROM stb$reportparameter rp, stb$reptypeparameter rtp
     WHERE rp.repid(+) = cnoldrepid
       AND rtp.parametername = rp.parametername(+)
       AND rtp.reporttypeid = cnreptype
       AND rtp.repparameter = 'T';

  -- cursor to retrieve the criteria
  CURSOR cgetcriteria (cnoldrepid IN NUMBER) IS
    SELECT entity, KEY, masterkey, display, selected, ordernumber, info
    FROM   isr$crit
    WHERE  repid = cnoldrepid;

  -- cursor to get the master data of the old report id
  CURSOR cgetoldversion (cnoldrepid IN NUMBER) IS
    SELECT VERSION, datagroup, status, remarks, oldstatus, title, srnumber, checksum
    FROM   stb$report
    WHERE  repid = cnoldrepid;

  -- cursor to retrieve the max RepId and its version
  CURSOR cgethighestversion (cnrepid IN NUMBER) IS
    SELECT     max (to_number (substr (VERSION, (instr (VERSION, '.')) + 1)) + 1) VERSION
    FROM       stb$report
    START WITH repid = cnrepid
    CONNECT BY parentrepid = PRIOR repid;

  -- cursor to get the  root below a final vewrsion
  CURSOR cgetroot (cnrepid IN NUMBER) IS
    SELECT     min (repid)
    FROM       stb$report
    START WITH repid = cnrepid
    CONNECT BY repid = PRIOR parentrepid;

  -- records
  rgetoldversion                cgetoldversion%ROWTYPE;
BEGIN
  isr$trace.stat('begin','begin', sCurrentName);
  noldrepid := stb$object.getCurrentRepid;
  ncurrentreptype := stb$object.getCurrentReporttypeid;
  nCurrentTemplate := stb$object.getCurrentTemplate;

  -- get next RepId
  SELECT seq$stb$reports.NEXTVAL
  INTO   nnewrepid
  FROM   DUAL;
  isr$trace.debug ('nRepId',  'nNewRepId: ' || nnewrepid || '/  ' || ncurrentreptype || ' /  ' || stb$typedef.cnStatusactivated || ' / ' || stb$typedef.cnStatusdraft, sCurrentName);
  -- create new record for the report
  INSERT INTO stb$report
              (repid, status, createdon, createdby, reporttypeid, checksum, modifiedon, modifiedby, condition, template)
         VALUES (nnewrepid, stb$typedef.cnStatusdraft, sysdate, sUserName, ncurrentreptype, 0, sysdate, sUserName, stb$typedef.cnStatusactivated, nCurrentTemplate);
  isr$trace.debug ('after insert into stb$report',  'after insert into stb$report', sCurrentName);

  IF noldrepid IS NULL THEN
    isr$trace.debug ('nOldRepId is null ',  'nOldRepId is null ', sCurrentName);
    -- copy parameters
    ncount := 0;

    FOR rgetrepparameter IN cgetReptypeParameter (ncurrentreptype) LOOP
      ncount := ncount + 1;
      INSERT INTO stb$reportparameter
                  (parametername, parametervalue, modifiedon, modifiedby, repid)
           VALUES (rgetrepparameter.parametername, rgetrepparameter.parametervalue, sysdate, sUserName, nnewrepid);
      isr$trace.debug ('nCount',  'number of parameter inserted in stb$reportparameter: ' || ncount, sCurrentName);
    END LOOP;

    isr$trace.debug ('nCount',  'number of parameter inserted in stb$reportparameter: ' || ncount, sCurrentName);
  ELSE
    isr$trace.debug ('nOldRepId is not null ',  noldrepid, sCurrentName);
    -- report id exists-> copy parameters from  STB$Reportparameter except for variations

    ncount := 0;
    IF nArt != STB$TYPEDEF.cnVariation THEN

    isr$trace.debug ('nArt ',  nArt, sCurrentName);
      FOR rgetrepparameter IN cgetrepparameter (noldrepid) LOOP
        ncount := ncount + 1;
        INSERT INTO stb$reportparameter
                    (parametername, parametervalue, modifiedon, modifiedby, repid)
             VALUES (rgetrepparameter.parametername, rgetrepparameter.parametervalue, sysdate, sUserName, nnewrepid);
      END LOOP;

    ELSE
    isr$trace.debug ('nArt ',  nArt, sCurrentName);
      FOR rgetrepparameter IN cgetReptypeParameter2 (ncurrentreptype,noldrepid) LOOP
        ncount := ncount + 1;
        INSERT INTO stb$reportparameter
                    (parametername, parametervalue, modifiedon, modifiedby, repid)
             VALUES (rgetrepparameter.parametername, rgetrepparameter.parametervalue, sysdate, sUserName, nnewrepid);
      END LOOP;
       isr$trace.debug ('nCount',  'number of parameter inserted in stb$reportparameter: ' || ncount, sCurrentName);
    END IF;

    isr$trace.debug ('nCount',  'number of parameter inserted in stb$reportparameter: ' || ncount, sCurrentName);
    ncount := 0;

    -- Kriterien kopieren
    FOR rgetcriteria IN cgetcriteria (noldrepid) LOOP
      ncount := ncount + 1;
      INSERT INTO isr$crit
                  (entity, KEY, masterkey, display, repid, selected, ordernumber, info)
           VALUES (rgetcriteria.entity, rgetcriteria.KEY, rgetcriteria.masterkey, rgetcriteria.display, nnewrepid, rgetcriteria.selected, rgetcriteria.ordernumber, rgetcriteria.info);
    END LOOP;

    isr$trace.debug ('nCount',  'number of values inserted in isr$crit: ' || ncount, sCurrentName);
  END IF;

  -- check if report type is versioned
  oerrorobj := checkversionrequired (stb$object.getCurrentReporttypeid, sversionrequired);

  IF sversionrequired = 'T' THEN
    -- check if the report is a variation or version -> for update of the report version
    IF noldrepid IS NOT NULL THEN
      isr$trace.debug ('nOldRepId',  'nOldRepId: ' || noldrepid, sCurrentName);

      -- check variation then no parent id
      IF nart = stb$typedef.cnversion THEN
        isr$trace.debug ('nArt',  'sArt: ' || nart, sCurrentName);
        nparentid := noldrepid;
        isr$trace.debug ('nParentID',  'nParentID: ' || nparentid, sCurrentName);
        OPEN cgetoldversion (noldrepid);
        FETCH cgetoldversion
        INTO  rgetoldversion;

        IF cgetoldversion%NOTFOUND THEN
          isr$trace.debug ('cGetOldVersion%notfound',  'sArt: ' || nart, sCurrentName);
        END IF;

        CLOSE cgetoldversion;
        -- create the version number i.e "0.1"
        -- first counter remains
        sversion := substr (rgetoldversion.VERSION, 1, (instr (rgetoldversion.VERSION, '.', 1)));
        isr$trace.debug ('sVersion',  'sVersion: ' || sversion, sCurrentName);

        -- second counter has to be increased,therefore get the root (check if reprot is below or above a final report)
        IF rgetoldversion.status = stb$typedef.cnStatusfinal THEN
          -- above a final version
          nversioncounter := 1;
        ELSE
          -- check if final version exists
          oerrorobj := getlastFinalVersion (nLastFinalRepid);

          IF nLastFinalRepid = -1 THEN
            -- no final version   -> get the root of the subtrees
            OPEN cgetroot (noldrepid);
            FETCH cgetroot
            INTO  ndummy;

            IF cgetroot%NOTFOUND THEN
              isr$trace.debug ('cGetRoot%notFound ',  'cGetRoot%notFound for : ' || ndummy, sCurrentName);
            END IF;

            CLOSE cgetroot;
            OPEN cgethighestversion (ndummy);
            FETCH cgethighestversion
            INTO  nversioncounter;

            IF cgethighestversion%NOTFOUND THEN
              isr$trace.debug ('cGetHighestVersion%notFound ',  'cGetHighestVersion%notFound for : ' || noldrepid, sCurrentName);
            END IF;

            CLOSE cgethighestversion;
          ELSE
            -- there is a final version   -> root is the subtree
            OPEN cgethighestversion (nLastFinalRepid);
            FETCH cgethighestversion
            INTO  nversioncounter;

            IF cgethighestversion%NOTFOUND THEN
              isr$trace.debug ('cGetHighestVersion%notFound ',  'cGetHighestVersion%notFound for : ' || noldrepid, sCurrentName);
            END IF;

            CLOSE cgethighestversion;
          END IF;
        END IF;

        --create the new reort version
        sversion := sversion || nversioncounter;
        isr$trace.debug ('sVersion',  'sVersion: ' || sversion, sCurrentName);
      ELSE
        -- Variation
        sversion := '0.1';
        isr$trace.debug ('sVersion',  'sVersion: ' || sversion, sCurrentName);
      END IF;
    ELSE
      -- 1. Versionnumber "0.1" first report version
      sversion := '0.1';
      isr$trace.debug ('sVersion1',  'sVersion: ' || sversion, sCurrentName);
    END IF;

    -- generate password
    oerrorobj := createpassword (nnewrepid, spassword);
    isr$trace.debug ('Title/SrNumber',  'title: ' || rgetoldversion.title || '  srnumber: ' || rgetoldversion.srnumber || '   status: ' || rgetoldversion.status || '  checksum: ' || rgetoldversion.checksum, sCurrentName);
    UPDATE stb$report
       SET VERSION = sversion,
           parentrepid = nparentid,
           datagroup = rgetoldversion.datagroup,
           remarks = rgetoldversion.remarks,
           oldstatus = rgetoldversion.status,
           title = rgetoldversion.title,
           srnumber = rgetoldversion.srnumber,
           PASSWORD = spassword,
           modifiedon = sysdate,
           modifiedby = sUserName
     WHERE repid = nnewrepid;
  END IF;

  -- checksum
  oerrorobj := updateCheckSum (nnewrepid);

  isr$trace.stat('end','end', sCurrentName);
  RETURN (oerrorobj);
EXCEPTION
  WHEN OTHERS THEN
    ROLLBACK;
     oErrorObj.handleError ( Stb$typedef.cnSeverityCritical, utd$msglib.getmsg ('exUnexpectedError', stb$security.getCurrentLanguage,  csP1 => sqlerrm(sqlcode)),
        sCurrentName, dbms_utility.format_error_stack ||chr(10)|| dbms_utility.format_error_backtrace );
    RETURN (oerrorobj);
END createnewreport;


--*********************************************************************************************************************************
FUNCTION checkDraftVersionsExists (sexists OUT VARCHAR2)
  RETURN stb$oerror$record
IS
  sCurrentName                  CONSTANT VARCHAR2(100) := $$PLSQL_UNIT||'.checkDraftVersionsExists';
  nLastFinalRepid               NUMBER := -1;
  oerrorobj                     stb$oerror$record := STB$OERROR$RECORD();

  -- Cursor to check if there is a draft version between the final version and its previous final version
  CURSOR cgetrepid IS
    SELECT 'T'
      FROM stb$report
     WHERE repid < stb$object.getCurrentRepid
       AND repid > nLastFinalRepid
       AND status = stb$typedef.cnStatusDraft
       AND condition <> stb$typedef.cnStatusDeleted
       AND srnumber = stb$object.getCurrentSRNumber
       AND title = stb$object.getCurrentTitle;

BEGIN
  isr$trace.stat('begin','begin', sCurrentName);
  isr$trace.debug ('begin', stb$object.getcurrentrepid || ' / ' || stb$object.getcurrentsrnumber, sCurrentName);
  sexists := 'F';
  -- if there is a final version before
  oerrorobj := getlastFinalVersion (nLastFinalRepid);
  isr$trace.debug ('nLastFinalRepid', 'nLastFinalRepid : ' || nLastFinalRepid, sCurrentName);
  -- check if a version with trhe passed status exists
  OPEN cgetrepid;
  FETCH cgetrepid INTO sexists;
  CLOSE cgetrepid;
  isr$trace.stat('end','end', sCurrentName);
  RETURN oerrorobj;
EXCEPTION
  WHEN OTHERS THEN
     oErrorObj.handleError ( Stb$typedef.cnSeverityCritical, utd$msglib.getmsg ('exUnexpectedError', stb$security.getCurrentLanguage,  csP1 => sqlerrm(sqlcode)),
        sCurrentName, dbms_utility.format_error_stack ||chr(10)|| dbms_utility.format_error_backtrace );
    RETURN oerrorobj;
END checkdraftversionsexists;

--*********************************************************************************************************************************
FUNCTION checkArchivedReportsExists (sexists OUT VARCHAR2)
  RETURN stb$oerror$record
IS
  sCurrentName                  CONSTANT VARCHAR2(100) := $$PLSQL_UNIT||'.checkArchivedReportsExists';
  nLastFinalRepid               NUMBER := -1;
  oerrorobj                     stb$oerror$record := STB$OERROR$RECORD();

  -- Cursor to check if there is a reports between the final version and its previous final version which are archived
  CURSOR cgetrepid IS
    SELECT 'T'
    FROM   stb$report
    WHERE  repid < stb$object.getCurrentRepid AND repid > nLastFinalRepid AND condition = stb$typedef.cnStatusdeleted AND srnumber = stb$object.getcurrentsrnumber;
BEGIN
  isr$trace.stat('begin','begin', sCurrentName);
  -- if there is a final version before
  oerrorobj := getlastFinalVersion (nLastFinalRepid);
  isr$trace.debug ('nLastFinalRepid', 'nLastFinalRepid : ' || nLastFinalRepid, sCurrentName);
  -- check if a archived versions exists
  OPEN cgetrepid;
  FETCH cgetrepid
  INTO  sexists;
  CLOSE cgetrepid;

  IF sexists IS NULL THEN
    sexists := 'F';
  END IF;

  isr$trace.stat('end','end', sCurrentName);
  RETURN oerrorobj;
EXCEPTION
  WHEN OTHERS THEN
     oErrorObj.handleError ( Stb$typedef.cnSeverityCritical, utd$msglib.getmsg ('exUnexpectedError', stb$security.getCurrentLanguage,  csP1 => sqlerrm(sqlcode)),
        sCurrentName, dbms_utility.format_error_stack ||chr(10)|| dbms_utility.format_error_backtrace );
    RETURN oerrorobj;
END checkarchivedreportsexists;

--*******************************************************************************************************
FUNCTION checkSrNumberExists (ssrnumber IN VARCHAR2, sexists OUT VARCHAR2)
  RETURN stb$oerror$record
IS
  sCurrentName                  CONSTANT VARCHAR2(100) := $$PLSQL_UNIT||'.checkSrNumberExists('||sSrNumber||')';
  oerrorobj                     stb$oerror$record := STB$OERROR$RECORD();
  exSRNumberExists              EXCEPTION;

  -- Cursor to check if the entered srnumber already exists
  CURSOR cchecksrnumberexists IS
    SELECT 'T'
    FROM   stb$report
    WHERE  upper (srnumber) = upper (ssrnumber);
BEGIN
  isr$trace.stat('begin','begin', sCurrentName);
  -- check if a  sr number already exists
  OPEN cchecksrnumberexists;
  FETCH cchecksrnumberexists
  INTO  sexists;

  IF cchecksrnumberexists%NOTFOUND THEN
    sexists := 'F';
  END IF;

  CLOSE cchecksrnumberexists;
  isr$trace.stat ('end', 'sExists : ' || sexists, sCurrentName);

  IF sexists = 'T' THEN
    RAISE exSRNumberExists;
  END IF;

  isr$trace.stat('end','end', sCurrentName);
  RETURN oerrorobj;
EXCEPTION
  WHEN exSRNumberExists THEN
    oErrorObj.handleError ( Stb$typedef.cnSeverityMessage, utd$msglib.getmsg ('exSRNumberExistsText', stb$security.getCurrentLanguage,  csP1 => sqlerrm(sqlcode)),
        sCurrentName, dbms_utility.format_error_stack ||chr(10)|| dbms_utility.format_error_backtrace );
    RETURN oerrorobj;
  WHEN OTHERS THEN
    oErrorObj.handleError ( Stb$typedef.cnSeverityCritical, utd$msglib.getmsg ('exUnexpectedError', stb$security.getCurrentLanguage,  csP1 => sqlerrm(sqlcode)),
        sCurrentName, dbms_utility.format_error_stack ||chr(10)|| dbms_utility.format_error_backtrace );
    RETURN oerrorobj;
END checksrnumberexists;

--*******************************************************************************************************
FUNCTION checkTitleExists( sTitle IN VARCHAR2)
  RETURN VARCHAR2
IS
  sCurrentName                  CONSTANT VARCHAR2(100) := $$PLSQL_UNIT||'.checkTitleExists';
  sexists     VARCHAR2(1);

  CURSOR cchecktitleexists IS
    SELECT 'T'
      FROM stb$report
     WHERE upper (title) = upper (stitle)
       -- titles have to be unique within the system
       --AND reporttypeid = STB$OBJECT.getCurrentReporttypeId
       AND repid != STB$OBJECT.getCurrentRepId;

BEGIN
  isr$trace.stat('begin','begin', sCurrentName);

  -- check if the title already exists within the report type
  OPEN cchecktitleexists;
  FETCH cchecktitleexists INTO sexists;
  IF cchecktitleexists%NOTFOUND THEN
    sexists := 'F';
  END IF;
  CLOSE cchecktitleexists;

  isr$trace.stat('end','end', sCurrentName);
  RETURN sexists;

END checktitleexists;

--*******************************************************************************************************
FUNCTION getReptypeParameters (olparameter OUT stb$property$list)
  RETURN stb$oerror$record
IS
  sCurrentName                  CONSTANT VARCHAR2(100) := $$PLSQL_UNIT||'.getReptypeParameters';
  ncounter                      NUMBER := 0;
  oerrorobj                     stb$oerror$record := STB$OERROR$RECORD();
  oSelectionList                ISR$TLRSELECTION$LIST := ISR$TLRSELECTION$LIST();

  CURSOR cgetparameter(csRepParameter in varchar2) IS
    SELECT parametername
         , parametervalue
         , description
         , parametertype
         , editable
         , haslov
         , REQUIRED
         , CASE WHEN parametertype IN ('GROUP','TABSHEET') THEN 'T' WHEN STB$SECURITY.checkGroupRight (parametername, reporttypeid) = 'T' THEN VISIBLE ELSE 'F' END visible
      FROM stb$reptypeparameter
     WHERE userparameter = 'F'
       AND reporttypeid = stb$object.getCurrentReporttypeid
       AND (nvl(repparameter, 'F') = csRepParameter OR (parametertype in ('GROUP') and csRepParameter = 'T'))
    ORDER BY orderno;
BEGIN
  isr$trace.stat('begin','begin', sCurrentName);
  olparameter := stb$property$list ();

  olparameter.EXTEND;
  olParameter (olParameter.COUNT ()) := STB$PROPERTY$RECORD('REPTYPEPARAMETER$TAB', Utd$msglib.GetMsg ('REPTYPEPARAMETER$TAB', Stb$security.getCurrentLanguage),
                                                             null, null, null, null, 'T', null, 'TABSHEET');
  --write the report type id first
  olparameter.EXTEND;
  olParameter (olparameter.count()) := STB$PROPERTY$RECORD('REPORTTYPEID', utd$msglib.getmsg ('REPORTTYPEID', stb$security.getCurrentLanguage),
                                                           stb$object.getCurrentReporttypeid, stb$object.getCurrentReporttypeid,
                                                           'F', 'T', 'T', 'F', 'NUMBER');

  --loop over the parameters
  FOR rGetParameter IN cGetParameter('F') LOOP
    olparameter.EXTEND;
    olParameter (olparameter.count()) := STB$PROPERTY$RECORD(rGetParameter.parametername, utd$msglib.getmsg (rgetparameter.description, stb$security.getCurrentLanguage),
                                                             rGetParameter.parametervalue, rGetParameter.parametervalue,
                                                             rGetParameter.editable, rGetParameter.REQUIRED, rGetParameter.VISIBLE,
                                                             rGetParameter.haslov, rGetParameter.parametertype);

    IF instr (rGetParameter.description, '(') - 1 != -1 THEN
      olparameter (olparameter.count()).sPromptDisplay := utd$msglib.getmsg (trim (substr (rGetParameter.description, 1, instr (rgetparameter.description, '(') - 1)), stb$security.getCurrentLanguage);
    END IF;

    IF rGetParameter.haslov = 'T'
    AND rGetParameter.parametertype != 'FILE'
    AND trim(rGetParameter.parametervalue) IS NOT NULL THEN
      BEGIN
        oErrorObj := ISR$REPORTTYPE$PACKAGE.getLov (rGetParameter.parametername, rGetParameter.parametervalue, oSelectionList);

        IF oSelectionList.first IS NOT NULL THEN
          FOR j IN 1..oSelectionList.count() LOOP
            IF oSelectionList(j).sSelected = 'T' THEN
              olparameter (olparameter.count()).sDisplay := oSelectionList(j).sDisplay;
            END IF;
          END LOOP;
          IF trim(olParameter(olparameter.count()).sValue) IS NULL
          AND oSelectionList.count() >= 1
          AND olParameter(olparameter.count()).sRequired = 'T' THEN
            olParameter(olparameter.count()).sValue := oSelectionList(1).sValue;
            olParameter(olparameter.count()).sDisplay := oSelectionList(1).sDisplay;
          END IF;
        -- no lov
        ELSE
          olparameter (olparameter.count()).sValue := null;
          olparameter (olparameter.count()).sDisplay := null;
        END IF;
      EXCEPTION WHEN OTHERS THEN
        isr$trace.error ('could not set value from lov',  'parameter '||rGetParameter.parametername||' value '||rGetParameter.parametervalue, sCurrentName);
      END;
    END IF;
  END LOOP;

  --loop over the output options
  FOR rGetParameter IN cGetParameter('T') LOOP
    ncounter := ncounter + 1;
    -- Display a new tabsheet
    IF ncounter = 1 THEN
      olparameter.EXTEND;
      olParameter (olParameter.COUNT ()) := STB$PROPERTY$RECORD('OUTPUTOPTIONS$TAB', Utd$msglib.GetMsg ('OUTPUTOPTIONS$TAB', Stb$security.getCurrentLanguage),
                                                                 null, null, null, null, 'T', null, 'TABSHEET');
    END IF;
    olparameter.EXTEND;
    olParameter (olparameter.count()) := STB$PROPERTY$RECORD(rGetParameter.parametername, utd$msglib.getmsg (rgetparameter.description, stb$security.getCurrentLanguage),
                                                             rGetParameter.parametervalue, rGetParameter.parametervalue,
                                                             rGetParameter.editable, case when rGetParameter.parametertype != 'BOOLEAN' then 'F' else rGetParameter.REQUIRED end,
                                                             rGetParameter.VISIBLE, rGetParameter.haslov, rGetParameter.parametertype);

    IF instr (rGetParameter.description, '(') - 1 != -1 THEN
      olparameter (olparameter.count()).sPromptDisplay := utd$msglib.getmsg (trim (substr (rGetParameter.description, 1, instr (rgetparameter.description, '(') - 1)), stb$security.getCurrentLanguage);
    END IF;

    IF rGetParameter.haslov = 'T'
    AND rGetParameter.parametertype != 'FILE'
    AND trim(rGetParameter.parametervalue) IS NOT NULL THEN
      BEGIN
        oErrorObj := ISR$REPORTTYPE$PACKAGE.getLov (rGetParameter.parametername, rGetParameter.parametervalue, oSelectionList);

        IF oSelectionList.first IS NOT NULL THEN
          FOR j IN 1..oSelectionList.count() LOOP
            IF oSelectionList(j).sSelected = 'T' THEN
              olparameter (olparameter.count()).sDisplay := oSelectionList(j).sDisplay;
            END IF;
          END LOOP;
          IF trim(olParameter(olparameter.count()).sValue) IS NULL
          AND oSelectionList.count() >= 1
          AND olParameter(olparameter.count()).sRequired = 'T' THEN
            olParameter(olparameter.count()).sValue := oSelectionList(1).sValue;
            olParameter(olparameter.count()).sDisplay := oSelectionList(1).sDisplay;
          END IF;
        -- no lov
        ELSE
          olparameter (olparameter.count()).sValue := null;
          olparameter (olparameter.count()).sDisplay := null;
        END IF;
      EXCEPTION WHEN OTHERS THEN
        isr$trace.error ('could not set value from lov',  'parameter '||rGetParameter.parametername||' value '||rGetParameter.parametervalue, sCurrentName);
      END;
    END IF;
  END LOOP;

  IF olParameter.count() > 0 THEN
    FOR i IN 1..olParameter.count() LOOP
      IF instr(olParameter(i).sValue,':exec:') IS NOT NULL AND instr(olParameter(i).sValue,':exec:') > 0 THEN
        isr$trace.debug ('olParameter(i).sParameter', olParameter(i).sParameter,sCurrentName);
        olParameter(i).sDisplay := olParameter(i).sValue;
        olParameter(i).TYPE := 'LONGSTRING';
        olParameter(i).sEditable := 'F';
        olParameter(i).hasLov := 'F';
      END IF;

      -- display parameters which are null, invisible and required
      IF olparameter (i).sValue IS NULL
      AND olparameter (i).sDisplay = 'F'
      AND olparameter (i).sRequired = 'T' THEN
        olparameter (i).sDisplay := 'T';
        olparameter (i).sEditable := 'T';
      END IF;
    END LOOP;
  END IF;

  -- store the values localy in the package  for the audit
  ocurrentparameter := olparameter;
  isr$trace.stat('end','end', sCurrentName);
  RETURN (oerrorobj);
EXCEPTION
  WHEN OTHERS THEN
    ROLLBACK;
     oErrorObj.handleError ( Stb$typedef.cnSeverityCritical, utd$msglib.getmsg ('exUnexpectedError', stb$security.getCurrentLanguage,  csP1 => sqlerrm(sqlcode)),
        sCurrentName, dbms_utility.format_error_stack ||chr(10)|| dbms_utility.format_error_backtrace );
    RETURN (oerrorobj);
END getReptypeParameters;

--*******************************************************************************************************************************
FUNCTION putRepTypeParameters (olparameter IN stb$property$list)
  RETURN stb$oerror$record
IS
  sCurrentName                  CONSTANT VARCHAR2(100) := $$PLSQL_UNIT||'.putRepTypeParameters';
  oerrorobj                     stb$oerror$record := STB$OERROR$RECORD();
  nlineindex                    NUMBER;
  scomp1                        VARCHAR2 (4000);
  scomp1a                       VARCHAR2 (4000);
  nmodified                     NUMBER := 0;
BEGIN
  isr$trace.stat('begin','begin', sCurrentName);

  oerrorobj := stb$audit.openaudit ('REPORTTYPEAUDIT', to_char (stb$object.getcurrentreporttypeid));
  oErrorObj := stb$audit.addauditrecord ( 'CAN_MODIFY_REPTYPE_PARAMETERS');

  nlineindex := olparameter.first;

  WHILE (olparameter.EXISTS (nlineindex)) LOOP
    IF olparameter(nlineindex).sEditable = 'T' AND olParameter (nLineindex).type NOT IN ('TABSHEET', 'GROUP') THEN
      scomp1 := nvl (STB$UTIL.getReptypeParameter(stb$object.getCurrentReporttypeid, olparameter (nlineindex).sParameter), '');
      scomp1a := nvl (olparameter (nlineindex).svalue, '');
      isr$trace.debug ('nLineIndex', nlineindex||' '||olparameter (nlineindex).sParameter||' '||scomp1||' / '||scomp1a, sCurrentName);
      IF (trim (scomp1) != trim (scomp1a))
      OR (trim (scomp1) IS NULL AND trim (scomp1a) IS NOT NULL)
      OR (trim (scomp1) IS NOT NULL AND trim (scomp1a) IS NULL) THEN
        nmodified := nmodified + 1;
        oerrorobj := stb$audit.addauditentry (olparameter (nlineindex).sparameter, sComp1, sComp1A, nmodified);
        UPDATE stb$reptypeparameter
           SET parametervalue = olparameter (nlineindex).svalue,
               modifiedon = sysdate,
               modifiedby = stb$security.getcurrentuser
         WHERE parametername = olparameter (nlineindex).sparameter AND reporttypeid = stb$object.getCurrentReporttypeid;
      END IF;
    END IF;
    nlineindex := olparameter.NEXT (nlineindex);
    scomp1 := '';
    scomp1a := '';
  END LOOP;

  isr$trace.stat('end','end', sCurrentName);
  RETURN (oerrorobj);
EXCEPTION
  WHEN OTHERS THEN
    ROLLBACK;
     oErrorObj.handleError ( Stb$typedef.cnSeverityCritical, utd$msglib.getmsg ('exUnexpectedError', stb$security.getCurrentLanguage,  csP1 => sqlerrm(sqlcode)),
        sCurrentName, dbms_utility.format_error_stack ||chr(10)|| dbms_utility.format_error_backtrace );
    RETURN (oerrorobj);
END putreptypeparameters;

--*******************************************************************************************************************************
FUNCTION moveParameters (olparameter IN stb$property$list)
  RETURN stb$oerror$record
IS
  sCurrentName                  CONSTANT VARCHAR2(100) := $$PLSQL_UNIT||'.moveParameters';
  oerrorobj                     stb$oerror$record := STB$OERROR$RECORD();
  nlineindex                    NUMBER;
  scomp1                        VARCHAR2 (4000);
  scomp1a                       VARCHAR2 (4000);
  nmodified                     NUMBER := 0;
  
  sParameter                    STB$REPTYPEPARAMETER.PARAMETERNAME%TYPE;
  sParameterValue               STB$REPTYPEPARAMETER.PARAMETERVALUE%TYPE;
  sRepType                      VARCHAR2(10);

  CURSOR cGetRepParameter(cnReporttypeId IN NUMBER, csParameterName IN VARCHAR2) IS
    SELECT repparameter
      FROM stb$reptypeparameter
     WHERE reporttypeid = cnReporttypeId
       AND parametername = csParameterName;
BEGIN
  isr$trace.stat('begin','begin', sCurrentName);

  oerrorobj := stb$audit.openaudit ('REPORTTYPEAUDIT', to_char (stb$object.getCurrentReporttypeid));
  oErrorObj := stb$audit.addauditrecord ( 'CAN_MOVE_PARAMETERS');

  nlineindex := olparameter.first;

  WHILE (olparameter.EXISTS (nlineindex)) LOOP
    IF olparameter(nlineindex).sEditable = 'T' AND olParameter (nLineindex).type NOT IN ('TABSHEET', 'GROUP') THEN
      sParameter := olparameter (nlineindex).sParameter;
      sRepType := case  when sParameter like 'WORD_%' then 'WORD' 
                          when sParameter like 'EXCEL_%' then 'EXCEL'
                          else '' end;
      -- stop loop because already executed with excel
      if sReptype = 'WORD' then
        exit;
      end if;
      sParameter := case  when sParameter like 'WORD_%' then substr(sParameter, 6)
                          when sParameter like 'EXCEL_%' then substr(sParameter, 7)
                          else sParameter end;
      OPEN cGetRepParameter(stb$object.getcurrentreporttypeid, sParameter);
      FETCH cGetRepParameter INTO scomp1;
      IF cGetRepParameter%NOTFOUND THEN
        sComp1 := '';
      END IF;
      CLOSE cGetRepParameter;
      sParameterValue := olparameter (nlineindex).svalue;
      isr$trace.debug ('value',  sRepType||' '||sParameter||' '||sParameterValue, sCurrentName);
      if sParameterValue = 'T' and sRepType = 'EXCEL' and stb$util.getProperty(olparameter, 'WORD_'||sParameter) = 'T'
      or sParameterValue = 'T' and sRepType = 'WORD' and stb$util.getProperty(olparameter, 'EXCEL_'||sParameter) = 'T' then
        sParameterValue := 'A';
      elsif sParameterValue = 'T' and sRepType = 'WORD' and stb$util.getProperty(olparameter, 'EXCEL_'||sParameter) = 'F' 
         or sParameterValue = 'F' and sRepType = 'EXCEL' and stb$util.getProperty(olparameter, 'WORD_'||sParameter) = 'T' then
        sParameterValue := 'W';
      elsif sParameterValue = 'F' and sRepType = 'WORD' and stb$util.getProperty(olparameter, 'EXCEL_'||sParameter) = 'T' then
        sParameterValue := 'T';
      end if;          
      sComp1a := sParameterValue;
      isr$trace.debug ('nLineIndex',  nlineindex||' '||sParameter||' '||scomp1||' / '||scomp1a, sCurrentName);
      IF (trim (sComp1) != trim (sComp1a))
      OR (trim (sComp1) IS NULL AND trim (sComp1a) IS NOT NULL)
      OR (trim (sComp1) IS NOT NULL AND trim (sComp1a) IS NULL) THEN
        nmodified := nmodified + 1;
        isr$trace.debug ('update',  sParameter||' '||sParameterValue, sCurrentName);
        oerrorobj := stb$audit.addauditentry (sParameter, sComp1, sComp1a, nmodified);
        UPDATE stb$reptypeparameter
           SET repparameter = sParameterValue,
               modifiedon = sysdate,
               modifiedby = stb$security.getcurrentuser
         WHERE parametername = sParameter 
           AND reporttypeid = stb$object.getcurrentreporttypeid;
      END IF;
    END IF;
    nlineindex := olparameter.NEXT (nlineindex);
    scomp1 := '';
    scomp1a := '';
  END LOOP;

  isr$trace.stat('end','end', sCurrentName);
  RETURN (oerrorobj);
EXCEPTION
  WHEN OTHERS THEN
    ROLLBACK;
     oErrorObj.handleError ( Stb$typedef.cnSeverityCritical, utd$msglib.getmsg ('exUnexpectedError', stb$security.getCurrentLanguage,  csP1 => sqlerrm(sqlcode)),
        sCurrentName, dbms_utility.format_error_stack ||chr(10)|| dbms_utility.format_error_backtrace );
    RETURN (oerrorobj);
END moveparameters;

--*********************************************************************************************************************************
FUNCTION getDocumentProperties (olparameter OUT stb$property$list)
  RETURN stb$oerror$record
IS
  sCurrentName                  CONSTANT VARCHAR2(100) := $$PLSQL_UNIT||'.getDocumentProperties';
  ncounter                      NUMBER := 0;
  oerrorobj                     stb$oerror$record := STB$OERROR$RECORD();
  oSelectionList                ISR$TLRSELECTION$LIST := ISR$TLRSELECTION$LIST();

  CURSOR cgetparameter IS
    SELECT   T.parametername,
             nvl ((SELECT parametervalue
                   FROM   isr$doc$property
                   WHERE  docid = stb$object.getCurrentDocId
                   AND    parametername = T.parametername), T.parameterdefault) parametervalue,
             T.parametertype,
             T.editable,
             T.haslov,
             T.REQUIRED,
             T.VISIBLE
    FROM     isr$doc$prop$template T
    WHERE    T.reporttypeid = nvl (STB$OBJECT.getCurrentReporttypeId, 0)
    ORDER BY T.orderno;
BEGIN
  isr$trace.stat('begin','begin', sCurrentName);
  olparameter := stb$property$list ();

  FOR rgetparameter IN cgetparameter LOOP
    ncounter := ncounter + 1;
    olparameter.EXTEND;
    olParameter (olparameter.count()) := STB$PROPERTY$RECORD(rGetParameter.parametername, utd$msglib.getmsg (rgetparameter.parametername, stb$security.getCurrentLanguage),
                                                             rGetParameter.parametervalue, rGetParameter.parametervalue,
                                                             rGetParameter.editable, rgetparameter.REQUIRED,
                                                             rGetParameter.VISIBLE, rGetParameter.haslov, rGetParameter.parametertype);

    IF nvl(instr(olParameter(ncounter).sValue,':exec:'),0) > 0 THEN
      BEGIN
        execute immediate REPLACE(olParameter(ncounter).sValue, ':exec:') INTO olParameter(ncounter).sValue;
        olParameter(ncounter).sDisplay := olParameter(ncounter).sValue;
        isr$trace.debug('dynamic property', olParameter(ncounter).sValue, sCurrentName);
      EXCEPTION WHEN OTHERS THEN
        olParameter(ncounter).sValue := null;
        olParameter(ncounter).sDisplay := null;
        isr$trace.debug (olParameter(ncounter).sParameter,  SQLERRM||' '||REPLACE(olParameter(ncounter).sValue, ':exec:'), sCurrentName);
      END;
    END IF;

    IF olParameter(ncounter).hasLov = 'T'
    AND olParameter(ncounter).TYPE != 'FILE' /* open lovs
    AND trim(olParameter(i).sValue) IS NOT NULL */ THEN
      BEGIN
        oErrorObj := STB$GAL.getLov(olParameter(ncounter).sParameter, olParameter(ncounter).sValue, oSelectionList);

        IF oSelectionList IS NOT NULL AND oSelectionList.first IS NOT NULL THEN
          FOR j IN 1..oSelectionList.count() LOOP
            IF oSelectionList(j).sSelected = 'T' THEN
              olParameter(ncounter).sDisplay := oSelectionList(j).sDisplay;
            END IF;
          END LOOP;
          IF trim(olParameter(ncounter).sValue) IS NULL
          AND oSelectionList.count() >= 1
          AND olParameter(ncounter).sRequired = 'T'
          AND olParameter(ncounter).sDisplayed != 'S' THEN
            olParameter(ncounter).sValue := oSelectionList(1).sValue;
            olParameter(ncounter).sDisplay := oSelectionList(1).sDisplay;
          END IF;
        -- no lov
        ELSE
          olParameter(ncounter).sValue := null;
          olParameter(ncounter).sDisplay := null;
        END IF;
      EXCEPTION WHEN OTHERS THEN
        isr$trace.error ('could not set value from lov',  'parameter '||olParameter(ncounter).sParameter||' value '||olParameter(ncounter).sValue, sCurrentName);
      END;
    END IF;
  END LOOP;

  -- store the values localy in the package  for the audit
  ocurrentparameter := olparameter;
  isr$trace.stat('end','end', sCurrentName);
  RETURN (oerrorobj);
EXCEPTION
  WHEN OTHERS THEN
    ROLLBACK;
     oErrorObj.handleError ( Stb$typedef.cnSeverityCritical, utd$msglib.getmsg ('exUnexpectedError', stb$security.getCurrentLanguage,  csP1 => sqlerrm(sqlcode)),
        sCurrentName, dbms_utility.format_error_stack ||chr(10)|| dbms_utility.format_error_backtrace );
    RETURN (oerrorobj);
END getdocumentproperties;

--*******************************************************************************************************************************
FUNCTION putDocumentProperties (olparameter IN stb$property$list)
  RETURN stb$oerror$record
IS
  sCurrentName                  CONSTANT VARCHAR2(100) := $$PLSQL_UNIT||'.putDocumentProperties';
  oerrorobj                     stb$oerror$record := STB$OERROR$RECORD();
  nlineindex                    NUMBER;
  scomp1                        VARCHAR2 (4000);
  scomp1a                       VARCHAR2 (4000);
  nmodified                     NUMBER := 0;
  sChanges                      VARCHAR2 (4000);
BEGIN
  isr$trace.stat('begin','begin', sCurrentName);

  INSERT INTO ISR$DOC$PROPERTY (DOCID, PARAMETERNAME, PARAMETERVALUE,
                                MODIFIEDON, MODIFIEDBY)
  ( SELECT DISTINCT STB$OBJECT.getCurrentDocId,
                    T.PARAMETERNAME,
                    T.PARAMETERDEFAULT,
                    sysdate,
                    user
    FROM            isr$doc$prop$template T
    WHERE           T.reporttypeid = nvl (STB$OBJECT.getCurrentReporttypeId, 0)
    AND             T.parametername NOT IN (SELECT parametername
                                            FROM   isr$doc$property
                                            WHERE  docid = STB$OBJECT.getCurrentDocId) );


  -- check for differences and update the report type parameters
  nlineindex := olparameter.first;

  WHILE (olparameter.EXISTS (nlineindex)) LOOP
    scomp1 := nvl (ocurrentparameter (nlineindex).svalue, '');
    scomp1a := nvl (olparameter (nlineindex).svalue, '');
    isr$trace.debug ('nLineIndex',  nlineindex || scomp1 || ' / ' || scomp1a, sCurrentName);

    IF (trim (scomp1) != trim (scomp1a)) OR (trim (scomp1) IS NULL AND trim (scomp1a) IS NOT NULL) OR (trim (scomp1) IS NOT NULL AND trim (scomp1a) IS NULL) THEN
      nmodified := nmodified + 1;
      -- write the audit entry

      IF oerrorobj.sseveritycode != stb$typedef.cnNoError THEN
        RETURN oerrorobj;
      END IF;

      isr$trace.debug ('parameter',  olparameter (nlineindex).sparameter, sCurrentName);
      isr$trace.debug ('docid',  stb$object.getcurrentdocid, sCurrentName);
      isr$trace.debug ('value',  olparameter (nlineindex).svalue, sCurrentName);
      -- update the table    of the doc properties
      UPDATE isr$doc$property
         SET parametervalue = olparameter (nlineindex).svalue,
             parameterdisplay = case when olparameter (nlineindex).hasLov = 'T' then olparameter (nlineindex).sdisplay else null end,
             modifiedon = sysdate,
             modifiedby = user
       WHERE parametername = olparameter (nlineindex).sparameter AND docid = stb$object.getcurrentdocid;
      
      sChanges := SUBSTR(sChanges || utd$msglib.getmsg (olparameter (nlineindex).sparameter, stb$security.getCurrentLanguage) || ': ' || olparameter (nlineindex).sdisplay || chr(10), 0, 4000);
       
    END IF;

    nlineindex := olparameter.NEXT (nlineindex);
    scomp1 := '';
    scomp1a := '';
  END LOOP;

  -- ISRC-1044
  DELETE FROM ISR$REPORT$GROUPING$TABLE
   WHERE (repid, groupingname) IN
               (SELECT to_number(nodeid), 'GRP$'||dp.parametername
                  FROM stb$doctrail d, isr$doc$property dp
                 WHERE dp.docid = stb$object.getcurrentdocid
                   AND dp.docid = d.docid);

  INSERT INTO ISR$REPORT$GROUPING$TABLE (GROUPINGNAME, GROUPINGID, GROUPINGVALUE, GROUPINGDATE, REPID)
     (SELECT rg.GROUPINGNAME,rg.GROUPINGID,rg.GROUPINGVALUE,rg.GROUPINGDATE,rg.REPID
        FROM isr$report$grouping rg, stb$report r, (select * from stb$doctrail where nodetype = 'REPORT') d, isr$doc$property dp
       WHERE r.repid = to_number(nodeid)
         AND rg.repid = r.repid

         AND rg.groupingname = 'GRP$'||dp.parametername
         AND dp.docid = stb$object.getcurrentdocid
         AND dp.docid = d.docid
         AND r.condition != 0);
         
  
  oErrorObj := stb$audit.openaudit ('SRAUDIT', STB$OBJECT.getCurrentTitle);
  oErrorObj := stb$audit.addAuditRecord (STB$OBJECT.getCurrentToken);
  oErrorObj := stb$audit.addVersionToSRAudit (stb$object.getCurrentNode().sDisplay ||' ['||utd$msglib.getmsg ('DOCID', stb$security.getCurrentLanguage)||' '|| stb$object.getcurrentdocid || ']', sChanges);
  oErrorObj := stb$audit.domtoaudit;

  isr$trace.stat('end','end', sCurrentName);
  RETURN (oerrorobj);
EXCEPTION
  WHEN OTHERS THEN
    ROLLBACK;
     oErrorObj.handleError ( Stb$typedef.cnSeverityCritical, utd$msglib.getmsg ('exUnexpectedError', stb$security.getCurrentLanguage,  csP1 => sqlerrm(sqlcode)),
        sCurrentName, dbms_utility.format_error_stack ||chr(10)|| dbms_utility.format_error_backtrace );
    RETURN (oerrorobj);
END putdocumentproperties;

--**********************************************************************************************************************************
FUNCTION switchCurrentGroupRight (nreporttypeid IN NUMBER, ocurrentnode IN stb$treenode$record, sToken IN VARCHAR2)
  RETURN stb$oerror$record
IS
  sCurrentName                  CONSTANT VARCHAR2(100) := $$PLSQL_UNIT||'.switchCurrentGroupRight';
  oerrorobj                     stb$oerror$record := STB$OERROR$RECORD();
  nusergroupno                  NUMBER;
  sparameter                    stb$adminrights.parametername%TYPE;
  nmodified                     NUMBER;
  sReporttypeName               STB$REPORTTYPE.REPORTTYPENAME%TYPE;

  CURSOR cGetReporttypeName IS
    select UTD$MSGLIB.getMsg (reporttypename, stb$security.getCurrentLanguage)
    from stb$reporttype
    where reporttypeid = nReporttypeId;
BEGIN
  isr$trace.stat('begin','begin', sCurrentName);
  isr$trace.debug ('report type id',  stb$object.getcurrentreporttypeid, sCurrentName);
  nusergroupno := STB$OBJECT.getNodeIdForNodetype('FUNCTIONGROUP');
  sparameter := SUBSTR(stb$object.getCurrentNodeId, 1, INSTR(stb$object.getCurrentNodeId, '#@#')-1);
  OPEN cGetReporttypeName;
  FETCH cGetReporttypeName INTO sReporttypeName;
  CLOSE cGetReporttypeName;
  isr$trace.debug ('nUserGroupNo/sParameter',  nusergroupno || ' / ' || sparameter, sCurrentName);

  oerrorobj := stb$audit.openaudit ('GROUPAUDIT', to_char (nusergroupno));
  oErrorObj := Stb$audit.Addauditrecord (sToken);

  nmodified := 1;
  isr$trace.debug ('old value',  ocurrentnode.tlopropertylist (1).svalue, sCurrentName);

  -- update the table and write the switch in the auditfile
  IF ocurrentnode.tlopropertylist (1).svalue = 'T' THEN
    -- Right is enabled will be switched to disabled
    UPDATE stb$group$reptype$rights
       SET parametervalue = 'F'
     WHERE parametername = sparameter AND reporttypeid = nreporttypeid AND usergroupno = nusergroupno;
    -- write the audit entry
    oerrorobj := stb$audit.addauditentry (sparameter, 'T', 'F', nmodified);
  ELSE
    -- Right is disabled will be switched to enabled
    UPDATE stb$group$reptype$rights
       SET parametervalue = 'T'
     WHERE parametername = sparameter AND reporttypeid = nreporttypeid AND usergroupno = nusergroupno;
    -- write the audit entry
    oerrorobj := stb$audit.addauditentry (sparameter, 'F', 'T', nmodified);
  END IF;

  -- ISRC-1044 removed

  isr$trace.stat('end','end', sCurrentName);
  RETURN oerrorobj;
EXCEPTION
  WHEN OTHERS THEN
     oErrorObj.handleError ( Stb$typedef.cnSeverityCritical, utd$msglib.getmsg ('exUnexpectedError', stb$security.getCurrentLanguage,  csP1 => sqlerrm(sqlcode)),
        sCurrentName, dbms_utility.format_error_stack ||chr(10)|| dbms_utility.format_error_backtrace );
    RETURN oerrorobj;
END switchcurrentgroupright;

--***************************************************************************************************************************
FUNCTION deactivateInspectionStatus
  RETURN stb$oerror$record
IS
  sCurrentName                  CONSTANT VARCHAR2(100) := $$PLSQL_UNIT||'.deactivateInspectionStatus';
  oerrorobj                     stb$oerror$record := STB$OERROR$RECORD();
  nrepid                        NUMBER;
  scurrentuser                  stb$report.modifiedby%TYPE;
BEGIN
  isr$trace.stat('begin','begin', sCurrentName);
  nrepid := stb$object.getCurrentRepid;
  SELECT user
  INTO   scurrentuser
  FROM   DUAL;
  UPDATE stb$report
     SET status = stb$typedef.cnStatusdraft,
         modifiedon = sysdate,
         modifiedby = sUsername
   WHERE repid = nrepid;

  -- checksum
  oerrorobj := updateCheckSum (nrepid);
  isr$trace.stat('end','end', sCurrentName);
  RETURN oerrorobj;
EXCEPTION
  WHEN OTHERS THEN
     oErrorObj.handleError ( Stb$typedef.cnSeverityCritical, utd$msglib.getmsg ('exUnexpectedError', stb$security.getCurrentLanguage,  csP1 => sqlerrm(sqlcode)),
        sCurrentName, dbms_utility.format_error_stack ||chr(10)|| dbms_utility.format_error_backtrace );
    RETURN oerrorobj;
END deactivateinspectionstatus;

--***********************************************************************************************************************
FUNCTION getOffshootList (ssuchwert IN VARCHAR2, olparameter OUT isr$tlrselection$list)
  RETURN stb$oerror$record
IS
  sCurrentName                  CONSTANT VARCHAR2(100) := $$PLSQL_UNIT||'.getOffshootList';
  oerrorobj                     stb$oerror$record := STB$OERROR$RECORD();

  CURSOR cGetOffshootList IS
    select fileid, templatename, description
    from (SELECT t.fileid, t.templatename, t.templatename||' '||t.version description, t.version, rot.reporttypeid,
                 max(t.version) over (partition by t.templatename) max_version
            FROM isr$offshoot o
               , isr$report$output$type rot
               , isr$output$file iof
               , isr$template t
           WHERE o.offshoot_to_template = t.templatename
             AND o.reporttypeid = stb$object.getCurrentReporttypeid
             AND STB$SECURITY.checkGroupRight ('CAN_RUN_REPORT',rot.reporttypeid) = 'T' --target reporttype
             AND rot.VISIBLE = 'T'
             AND rot.outputid = iof.outputid
             AND iof.templatename = t.templatename
             AND iof.fileid = t.fileid
             and t.visible = 'T'
             -- make the checks on target reporttype, not the current
             AND STB$SECURITY.checkGroupRight ('TEMPLATE'||t.fileid, rot.reporttypeid) = 'T'
             AND ( (t.release = 'T'
                    AND STB$SECURITY.checkGroupRight ('SHOW_UNRELEASED_TEMPLATES_OFFSHOOT', rot.reporttypeid) = 'F')
                   OR (STB$SECURITY.checkGroupRight ('SHOW_UNRELEASED_TEMPLATES_OFFSHOOT', rot.reporttypeid) = 'T'))
         )
    where version = max_version
    ORDER BY reporttypeid, 2;

BEGIN
  isr$trace.stat('begin','begin', sCurrentName);
  olparameter := isr$tlrselection$list ();

  FOR rgetoffshootlist IN cgetoffshootlist LOOP
    olparameter.EXTEND;
    olparameter (olparameter.count()) := ISR$OSELECTION$RECORD(rgetoffshootlist.templatename, rgetoffshootlist.fileid, rgetoffshootlist.description, null, olparameter.count());
  END LOOP;

  isr$trace.stat('end','end', sCurrentName);
  RETURN oerrorobj;
EXCEPTION
  WHEN OTHERS THEN
     oErrorObj.handleError ( Stb$typedef.cnSeverityCritical, utd$msglib.getmsg ('exUnexpectedError', stb$security.getCurrentLanguage,  csP1 => sqlerrm(sqlcode)),
        sCurrentName, dbms_utility.format_error_stack ||chr(10)|| dbms_utility.format_error_backtrace );
    RETURN oerrorobj;
END getoffshootlist;

--******************************************************************************************************
FUNCTION putSaveDialog1 (olsavelist IN stb$property$list, scurrenttoken IN VARCHAR2)
  RETURN stb$oerror$record
IS
  sCurrentName                  CONSTANT VARCHAR2(100) := $$PLSQL_UNIT||'.putSaveDialog1';
  oerrorobj                     stb$oerror$record := STB$OERROR$RECORD();
  exTitleExists                 EXCEPTION;
  sdynsql                       VARCHAR2 (4000) := '';
BEGIN
  isr$trace.stat('begin','begin', sCurrentName);

  IF scurrenttoken IN ('CAN_CREATE_NEW_REPORT', 'CAN_CREATE_NEW_REP', 'CAN_CREATE_REPORT_VARIANT')
  AND checktitleexists (STB$UTIL.getProperty(olSaveList, 'TITLE')) = 'T' THEN
    RAISE exTitleExists;
  END IF;

  -- save
  FOR ncounter IN 1..olsavelist.COUNT() LOOP
    IF UPPER(olsavelist (ncounter).sparameter) IN ('SRNUMBER', 'TITLE', 'DATAGROUP', 'REMARKS') THEN
      isr$trace.debug (ncounter,  olsavelist (ncounter).sparameter || ' ' || olsavelist (ncounter).svalue, sCurrentName);
      sdynsql := 'update stb$report set ' || olsavelist (ncounter).sparameter || ' = ''' || REPLACE(olsavelist (ncounter).svalue, '''', '''''') || ''' where repid = :nRepId';
      isr$trace.debug ('sDynSql',  sdynsql, sCurrentName);
      EXECUTE IMMEDIATE (sdynsql) using STB$OBJECT.getCurrentRepid;
    END IF;
  END LOOP;

  --ISRC-1326
  oerrorobj := isr$reporttype$package.createSraudit (STB$OBJECT.getCurrentSrNumber);
  -- checksum
  oerrorobj := updateCheckSum (STB$OBJECT.getCurrentRepid);

  isr$trace.stat('end', 'end', sCurrentName);
  RETURN oerrorobj;

EXCEPTION
  WHEN exTitleExists THEN
  --cnSeverityCritical: ISRC-1332
     oErrorObj.handleError ( Stb$typedef.cnSeverityCritical, utd$msglib.getmsg ('exTitleExistsText', stb$security.getCurrentLanguage,  csP1 => sqlerrm(sqlcode)),
        sCurrentName, dbms_utility.format_error_stack ||chr(10)|| dbms_utility.format_error_backtrace );
    RETURN oerrorobj;
  WHEN OTHERS THEN
    ROLLBACK;
     oErrorObj.handleError ( Stb$typedef.cnSeverityCritical, utd$msglib.getmsg ('exUnexpectedError', stb$security.getCurrentLanguage,  csP1 => sqlerrm(sqlcode)),
        sCurrentName, dbms_utility.format_error_stack ||chr(10)|| dbms_utility.format_error_backtrace );
    RETURN oerrorobj;
END putsavedialog1;

--******************************************************************************************************
FUNCTION putSaveDialog2 (olsavelist IN stb$property$list, scurrenttoken IN VARCHAR2)
  RETURN stb$oerror$record
IS
  sCurrentName                  CONSTANT VARCHAR2(100) := $$PLSQL_UNIT||'.putSaveDialog2';
  oerrorobj                     stb$oerror$record := STB$OERROR$RECORD();
  exTitleExists                 EXCEPTION;
  sdynsql                       VARCHAR2 (4000) := '';
BEGIN
  isr$trace.stat('begin','begin', sCurrentName);

  IF trim (STB$OBJECT.getCurrentVersion) = '0.1' AND checktitleexists (STB$UTIL.getProperty(olSaveList, 'TITLE')) = 'T' THEN
    RAISE exTitleExists;
  END IF;

  -- save
  FOR ncounter IN 1..olsavelist.COUNT() LOOP
    IF UPPER(olsavelist (ncounter).sparameter) IN ('SRNUMBER', 'TITLE', 'DATAGROUP', 'REMARKS') THEN
      isr$trace.debug (ncounter, olsavelist (ncounter).sparameter || ' ' || olsavelist (ncounter).svalue, sCurrentName);
      -- MCD, 10.Apr 2009, Error No. ISR024: Replace added for apostrophs in the text fields of the saving dialog
      sdynsql := 'update stb$report set ' || olsavelist (ncounter).sparameter || ' = ''' || REPLACE(olsavelist (ncounter).svalue, '''','''''') || ''' where repid = :nRepId';
      isr$trace.debug ('sDynSql', sdynsql, sCurrentName);
      EXECUTE IMMEDIATE (sdynsql) using STB$OBJECT.getCurrentRepid;
    END IF;
  END LOOP;

  -- audit
  oerrorobj := isr$reporttype$package.createsraudit (STB$OBJECT.getCurrentSrNumber);

  -- checksum
  oerrorobj := updateCheckSum (STB$OBJECT.getCurrentRepid);

  isr$trace.stat('end','end', sCurrentName);
  RETURN oerrorobj;

EXCEPTION
  WHEN exTitleExists THEN
    oerrorobj.sErrorCode := utd$msglib.getmsg ('exTitleExists', stb$security.getCurrentLanguage);
    oErrorObj.handleError ( Stb$typedef.cnSeverityMessage, utd$msglib.getmsg ('exTitleExistsText', stb$security.getCurrentLanguage,  csP1 => sqlerrm(sqlcode)),
        sCurrentName, dbms_utility.format_error_stack ||chr(10)|| dbms_utility.format_error_backtrace );
    RETURN oerrorobj;
  WHEN OTHERS THEN
    ROLLBACK;
     oErrorObj.handleError ( Stb$typedef.cnSeverityCritical, utd$msglib.getmsg ('exUnexpectedError', stb$security.getCurrentLanguage,  csP1 => sqlerrm(sqlcode)),
        sCurrentName, dbms_utility.format_error_stack ||chr(10)|| dbms_utility.format_error_backtrace );
    RETURN oerrorobj;
END putsavedialog2;

--***********************************************************************************************************************************
FUNCTION putSaveDialogUnitTc (olsavelist IN stb$property$list, scurrenttoken IN VARCHAR2)
  RETURN stb$oerror$record
IS
  sCurrentName                  CONSTANT VARCHAR2(100) := $$PLSQL_UNIT||'.putSaveDialogUnitTc';
  oerrorobj                     stb$oerror$record := STB$OERROR$RECORD();
  nOutputId                     NUMBER;
  nActionId                     NUMBER;
  sFunction                     VARCHAR2(100);
  sMsg                  varchar2(4000);
BEGIN
  isr$trace.stat('begin','begin', sCurrentName);

  -- start job
  FOR rGetJobIds IN ( SELECT ao.outputid, ao.actionid, o.actionfunction myfunction
                        FROM isr$action A, isr$report$output$type o, isr$action$output ao
                       WHERE (upper (o.token) = upper (scurrenttoken)
                           OR upper (A.token) = upper (scurrenttoken))
                         AND o.reporttypeid IS NULL
                         AND o.outputid = ao.outputid
                         AND A.actionid = ao.actionid) LOOP
    nOutputId := rGetJobIds.outputid;
    nActionId := rGetJobIds.actionid;
    sFunction := rGetJobIds.myfunction;
  END LOOP;

  if nActionId is null then
    sMsg := utd$msglib.getmsg ('exNotFindAction', stb$security.getCurrentLanguage, scurrenttoken);
    raise stb$job.exNotFindAction;
  end if;


  SELECT STB$JOB$SEQ.NEXTVAL
  INTO   STB$JOB.nJobid
  FROM   DUAL;

  STB$JOB.setJobParameter ('TOKEN', upper (scurrenttoken), STB$JOB.nJobid);
  STB$JOB.setJobParameter ('OUTPUTID', nOutputId, STB$JOB.nJobid);
  STB$JOB.setJobParameter ('ACTIONID', nActionId, STB$JOB.nJobid);
  STB$JOB.setJobParameter ('FUNCTION', sFunction, STB$JOB.nJobid);

  STB$JOB.setJobParameter ('REPORTTYPEID', STB$OBJECT.getCurrentReporttypeid, STB$JOB.nJobid);
  STB$JOB.setJobParameter ('REPID', STB$OBJECT.getCurrentRepId, STB$JOB.nJobid);
  STB$JOB.setJobParameter ('REFERENCE', STB$OBJECT.getCurrentRepId, STB$JOB.nJobid);
  oErrorObj := STB$JOB.startJob;

  --ROLLBACK;

  isr$trace.stat('end','end', sCurrentName);
  RETURN oerrorobj;
EXCEPTION
 when stb$job.exNotFindAction then
    oErrorObj.handleError ( Stb$typedef.cnSeverityCritical, sMsg, sCurrentName, dbms_utility.format_error_stack ||stb$typedef.crlf|| dbms_utility.format_error_backtrace );
    rollback;
    return oErrorObj;
  WHEN OTHERS THEN
    ROLLBACK;
    sMsg := utd$msglib.getmsg ('ERROR', stb$security.getCurrentLanguage) || SQLCODE || ' ' || SQLERRM;
    oErrorObj.handleError ( Stb$typedef.cnSeverityCritical, sMsg, sCurrentName, dbms_utility.format_error_stack ||stb$typedef.crlf|| dbms_utility.format_error_backtrace );
    rollback;
    return oErrorObj;
END putsavedialogunittc;

--************************************************************************************************************
FUNCTION getOutputOption (olparameter OUT stb$property$list)
  RETURN stb$oerror$record
IS
  sCurrentName                  CONSTANT VARCHAR2(100) := $$PLSQL_UNIT||'.getOutputOption';
  oerrorobj                     stb$oerror$record := STB$OERROR$RECORD();
  oSelectionList                ISR$TLRSELECTION$LIST := ISR$TLRSELECTION$LIST();
  oselectiondummy               ISR$TLRSELECTION$LIST := ISR$TLRSELECTION$LIST();
  oFormInforecord               ISR$FORMINFO$RECORD;
  oEntityList                   STB$ENTITY$LIST;
  nDocid                        NUMBER;
  nrepid                        NUMBER;
  ncounter                      NUMBER;

  -- Cursor
  CURSOR cgetparameter IS
    SELECT rt.parameterName
         , utd$msglib.getmsg (TRIM (SUBSTR (rt.description, 1, DECODE (INSTR (rt.description, '(') - 1, -1, LENGTH (rt.description), INSTR (rt.description, '(') - 1)))
                            , stb$security.getCurrentLanguage)
              promptDisplay
         , NVL (rpa.parameterValue, rt.parameterValue) parametervalue
         , CASE
              WHEN rt.parametertype = 'STRING'
               AND rt.haslov = 'T'
               AND pv.msgkey IS NULL THEN
                 pv.display
              WHEN rt.parametertype = 'STRING'
               AND rt.haslov = 'T'
               AND pv.msgkey IS NOT NULL THEN
                 utd$msglib.getmsg (pv.msgkey, stb$object.getCurrentReportlanguage, pv.plugin)
              ELSE
                 NVL (rpa.parameterValue, rt.parameterValue)
           END
              parameterDisplay
         , rt.parameterType
         , rt.editable
         , rt.haslov
         , rt.REQUIRED
         , TRIM (SUBSTR (rt.description, DECODE (INSTR (rt.description, '('), 0, LENGTH (rt.description) + 1, INSTR (rt.description, '(')))) sqlstatement
         , CASE WHEN STB$SECURITY.checkGroupRight (rt.parametername, rt.reporttypeid) = 'T' THEN rt.VISIBLE ELSE 'F' END visible
      FROM stb$reportparameter rpa, stb$reptypeparameter rt, isr$parameter$values pv
     WHERE rpa.repid(+) = stb$object.getCurrentRepid
       AND rpa.parametername(+) = rt.parametername
       AND rt.reporttypeid = stb$object.getCurrentReporttypeid
       AND (rt.repparameter in ('T', 'A')
         OR parametertype IN ('GROUP'))
       AND rpa.parameterName = pv.entity(+)
       AND rpa.parametervalue = pv.VALUE(+)
    ORDER BY rt.orderno;

BEGIN
  isr$trace.stat('begin','begin', sCurrentName);

  IF nCurrentFormNumber != getNumberOfForms THEN
    oErrorObj := getput (getNumberOfForms, oselectiondummy, oFormInforecord, oEntityList);

    IF (oErrorObj.sSeverityCode in (stb$typedef.cnwizardvalidateStop, stb$typedef.cnWizardValidateWarn)) THEN
      isr$trace.info ('error', 'ISR$REPORTTYPE$PACKAGE.getOutputOption', 'validation failed', sCurrentName);
      RETURN oErrorObj;
    END IF;
  END IF;

  -- get the current Report id
  nrepid := stb$object.getcurrentrepid;
  ncounter := 0;
  olparameter := stb$property$list ();

  FOR rgetparameter IN cgetparameter LOOP
    ncounter := ncounter + 1;
    olparameter.EXTEND;
    olParameter (ncounter) := STB$PROPERTY$RECORD(rGetParameter.parametername);
    IF trim(rgetparameter.sqlstatement) IS NOT NULL THEN
      isr$trace.debug ('rgetparameter.sqlstatement',  rgetparameter.sqlstatement, sCurrentName);
      EXECUTE IMMEDIATE rgetparameter.sqlstatement INTO olparameter (ncounter).spromptdisplay;
      isr$trace.debug ('olparameter (ncounter).spromptdisplay',  olparameter (ncounter).spromptdisplay, sCurrentName);
    END IF;
    olparameter (ncounter).spromptdisplay := rgetparameter.promptdisplay || olparameter (ncounter).spromptdisplay;
    isr$trace.debug('value', 'olparameter ('||ncounter||').spromptdisplay: '||olparameter(ncounter).spromptdisplay, sCurrentName );

    olparameter (ncounter).svalue := rgetparameter.parametervalue;
    isr$trace.debug('value', 'olparameter ('||ncounter||').svalue: '||olparameter(ncounter).svalue, sCurrentName);

    olparameter (ncounter).seditable := rgetparameter.editable;
    olparameter (ncounter).TYPE := rgetparameter.parametertype;
    olparameter (ncounter).haslov := rgetparameter.haslov;
    olparameter (ncounter).srequired := rgetparameter.REQUIRED;
    olparameter (ncounter).sdisplayed := rgetparameter.VISIBLE;

    IF rGetParameter.hasLov = 'T'
    AND rGetParameter.parametertype != 'FILE'
    AND trim(olparameter (ncounter).svalue) IS NOT NULL THEN
      BEGIN
        oErrorObj := ISR$REPORTTYPE$PACKAGE.getLov (rGetParameter.parametername, olparameter (ncounter).svalue, oSelectionList);

        IF oSelectionList.first IS NOT NULL THEN
          FOR j IN 1..oSelectionList.count() LOOP
            IF oSelectionList(j).sSelected = 'T' THEN
              olparameter (olparameter.count()).sDisplay := oSelectionList(j).sDisplay;
            END IF;
          END LOOP;
          IF trim(olParameter(olparameter.count()).sValue) IS NULL
          AND oSelectionList.count() >= 1
          AND olParameter(olparameter.count()).sRequired = 'T' THEN
            olParameter(olparameter.count()).sValue := oSelectionList(1).sValue;
            olParameter(olparameter.count()).sDisplay := oSelectionList(1).sDisplay;
          END IF;
        -- no lov
        ELSE
          olparameter (olparameter.count()).sValue := null;
          olparameter (olparameter.count()).sDisplay := null;
        END IF;
      EXCEPTION WHEN OTHERS THEN
        isr$trace.error ('could not set value from lov', 'parameter '||rGetParameter.parametername||' value '||olParameter(ncounter).sValue, sCurrentName);
      END;
    ELSE
      olParameter (nCounter).sDisplay := rGetParameter.parameterDisplay;
    END IF;

    IF rGetParameter.parameterType = 'DATE' THEN
      olparameter (ncounter).format := STB$UTIL.getSystemParameter('JAVA_DATE_FORMAT');
      IF (instr(olParameter(ncounter).sValue,':exec:') IS NULL OR instr(olParameter(ncounter).sValue,':exec:') = 0)
      AND olparameter (ncounter).sValue IS NOT NULL THEN
        BEGIN
          olparameter (ncounter).sValue := to_char(to_date(olparameter (ncounter).sValue, sSystemDateFormat), sSystemDateFormat);
        EXCEPTION WHEN OTHERS THEN
          olparameter (ncounter).sValue := to_char(to_date(olparameter (ncounter).sValue, sSystemDateFormat, 'nls_date_language=german'), sSystemDateFormat);
        END;
        olparameter (ncounter).sDisplay := olParameter(ncounter).sValue;
      END IF;
    END IF;

  END LOOP;

  IF olParameter.count() > 0 THEN
    FOR i IN 1..olParameter.count() LOOP
      IF instr(olParameter(i).sValue,':exec:') IS NOT NULL AND instr(olParameter(i).sValue,':exec:') > 0 THEN
        execute immediate REPLACE(olParameter(i).sValue, ':exec:') INTO olParameter(i).sValue;
        olParameter(i).sDisplay := olParameter(i).sValue;
      END IF;
      IF olParameter(i).TYPE = 'DATE' AND olparameter (i).sValue IS NOT NULL THEN
        BEGIN
          olparameter (i).sValue := to_char(to_date(olparameter (i).sValue, sSystemDateFormat), sSystemDateFormat);
        EXCEPTION WHEN OTHERS THEN
          olparameter (i).sValue := to_char(to_date(olparameter (i).sValue, sSystemDateFormat, 'nls_date_language=german'), sSystemDateFormat);
        END;
        olParameter(i).sDisplay := olParameter(i).sValue;
      END IF;

      -- OUTPUT/PROTECTED in higher versions not editable
      IF STB$OBJECT.getCurrentToken LIKE 'CAN_MODIFY%'
      OR STB$OBJECT.getCurrentToken LIKE 'CAN_CREATE_REPORT_VERSION' THEN
        IF olparameter (i).sParameter = 'PROTECTED' THEN
          -- check if an document already exists then the default output id can't be changed
          oErrorObj := stb$docks.getMaxDocidInSRNumber (stb$object.getcurrentrepid, ndocid, STB$DOCKS.getDocType(NULL, STB$OBJECT.getCurrentRepid),STB$DOCKS.getDocDefType(NULL, STB$OBJECT.getCurrentRepid));

          IF nvl (ndocid, 0) <> 0 THEN
            olparameter (i).sEditable := 'F';
            isr$trace.debug ('DocExists',  'DocExists: ' || ndocid, sCurrentName);
          END IF;
        END IF;
      END IF;

      -- display parameters which are null, invisible and required
      IF olparameter (i).sValue IS NULL
      AND olparameter (i).sDisplay = 'F'
      AND olparameter (i).sRequired = 'T' THEN
        olparameter (i).sDisplay := 'T';
        olparameter (i).sEditable := 'T';
      END IF;

    END LOOP;
  END IF;

  FOR i IN 1..olparameter.count() LOOP
    IF olparameter(i).type IN ('TABSHEET', 'GROUP') THEN
      -- if the last position is tab/group, delete it
      IF i = olparameter.count() THEN
        olparameter(i).type := 'DELETE';
      END IF;
      -- if the next visible is tab/group, delete it: exception if group follows tab
      IF i >= 1  AND i < olparameter.count() THEN
        FOR j IN i+1..olparameter.count() LOOP
          IF olparameter(j).type IN ('TABSHEET', 'GROUP')
          AND (olparameter(i).type = olparameter(j).type
              OR olparameter(i).type = 'GROUP' AND olparameter(j).type = 'TABSHEET') THEN
            olparameter(i).type := 'DELETE';
          END IF;
          IF olparameter(j).sDisplayed = 'T'
          AND (olparameter(j).type NOT IN ('TABSHEET', 'GROUP') OR olparameter(i).type != olparameter(j).type) THEN
            EXIT;
          END IF;
        END LOOP;
      END IF;
    END IF;
  END LOOP;

  nCurrentFormNumber := getNumberOfForms;
  isr$trace.stat('end','end', sCurrentName);
  RETURN oerrorobj;
EXCEPTION
  WHEN OTHERS THEN
     oErrorObj.handleError ( Stb$typedef.cnSeverityCritical, utd$msglib.getmsg ('exUnexpectedError', stb$security.getCurrentLanguage,  csP1 => sqlerrm(sqlcode)),
        sCurrentName, dbms_utility.format_error_stack ||chr(10)|| dbms_utility.format_error_backtrace );
    RETURN oerrorobj;
END getoutputoption;

--*************************************************************************************************************************
FUNCTION putOutputOption (olparameter IN stb$property$list)
  RETURN stb$oerror$record
IS
  sCurrentName                  CONSTANT VARCHAR2(100) := $$PLSQL_UNIT||'.putOutputOption';
  oerrorobj                     stb$oerror$record := STB$OERROR$RECORD();

  CURSOR cgetforminfo IS
    SELECT validatefunction, isrpackage
      FROM isr$report$wizard
     WHERE masktype = STB$TYPEDEF.cnOutputOption
       AND reporttypeid = stb$object.getCurrentReporttypeid;
       -- select it anyway to get also the Current Form Package
       --AND TRIM(validatefunction) IS NOT NULL;

   rGetFormInfo cgetforminfo%ROWTYPE;
BEGIN
  isr$trace.stat('begin','begin', sCurrentName);

  -- validate the output options
  OPEN cgetforminfo;
  FETCH cgetforminfo INTO rGetFormInfo;
  sCurrentFormPackage := rGetFormInfo.isrpackage; -- ISRC-792 set the package variable before it is used in ExitOutputOptions
  IF cgetforminfo%FOUND AND UPPER(rGetFormInfo.validatefunction) IN ('T', 'VALIDATE') THEN
    -- validate if the selection is okay
    EXECUTE IMMEDIATE
      'BEGIN
         :1 :='||stb$util.getcurrentcustompackage||'.validate(:2);
       END;'
    using OUT oErrorObj, IN nCurrentFormNumber;
  END IF;
  CLOSE cgetforminfo;

  DELETE FROM STB$REPORTPARAMETER
   WHERE repid = STB$OBJECT.getCurrentRepid
     AND parametername NOT IN (SELECT sParameter FROM table(olParameter));

  INSERT INTO STB$REPORTPARAMETER(REPID, PARAMETERNAME, PARAMETERVALUE, MODIFIEDON, MODIFIEDBY)
    (SELECT STB$OBJECT.getCurrentRepid, sParameter, sValue, sysdate, sUserName
       FROM table(olParameter)
      WHERE sParameter NOT IN (SELECT parametername FROM STB$REPORTPARAMETER WHERE repid = STB$OBJECT.getCurrentRepid)
        AND sDisplayed = 'T');

  FOR i IN 1..olParameter.count() LOOP
    UPDATE stb$reportparameter
       SET parametervalue = olparameter (i).sValue,
           modifiedon = sysdate,
           modifiedby = sUserName
     WHERE parametername = olparameter (i).sParameter
       AND repid = STB$OBJECT.getCurrentRepid;
  END LOOP;

  UPDATE stb$report
     SET modifiedon = sysdate
       , modifiedby = sUserName
   WHERE repid = STB$OBJECT.getCurrentRepid;

  oErrorObj := updateCheckSum (STB$OBJECT.getCurrentRepid);

   -- Expected end of ouput option handling
   -- cleanup
   oerrorobj := ExitOutputOptions;

  isr$trace.stat('end','end', sCurrentName);
  RETURN oerrorobj;
EXCEPTION
  WHEN OTHERS THEN
     oErrorObj.handleError ( Stb$typedef.cnSeverityCritical, utd$msglib.getmsg ('exUnexpectedError', stb$security.getCurrentLanguage,  csP1 => sqlerrm(sqlcode)),
        sCurrentName, dbms_utility.format_error_stack ||chr(10)|| dbms_utility.format_error_backtrace );
    RETURN oerrorobj;
END putoutputoption;

--***********************************************************************************************************************************************
FUNCTION copyReportParameter (cnnewrepid IN NUMBER)
  RETURN stb$oerror$record
IS
  sCurrentName                  CONSTANT VARCHAR2(100) := $$PLSQL_UNIT||'.copyReportParameter';
  oerrorobj                     stb$oerror$record := STB$OERROR$RECORD();
  noldrepid                     NUMBER;
  ncount                        NUMBER;

  CURSOR cgetrepparameter (cnoldrepid IN NUMBER) IS
    SELECT *
    FROM   stb$reportparameter
    WHERE  repid = cnoldrepid;

  CURSOR cgetcriteria (cnoldrepid IN NUMBER) IS
    SELECT *
    FROM   isr$crit
    WHERE  repid = cnoldrepid;
BEGIN
  isr$trace.stat('begin','begin', sCurrentName);

  -- save old repid
  nOldRepid := stb$object.getCurrentRepid;

  -- copy reportparameter
  ncount := 0;
  FOR rgetrepparameter IN cgetrepparameter (noldrepid) LOOP
    ncount := ncount + 1;
    INSERT INTO stb$reportparameter (parametername
                                   , parametervalue
                                   , modifiedon
                                   , modifiedby
                                   , repid)
    VALUES (rgetrepparameter.parametername
          , rgetrepparameter.parametervalue
          , SYSDATE
          , sUserName
          , cnnewrepid);
  END LOOP;

  isr$trace.debug ('nCount', 'number of parameter inserted in stb$reportparameter: ' || ncount, sCurrentName);

  -- copy criteria
  ncount := 0;
  FOR rgetcriteria IN cgetcriteria (noldrepid) LOOP
    ncount := ncount + 1;
    INSERT INTO isr$crit (entity
                        , KEY
                        , masterkey
                        , display
                        , repid
                        , selected
                        , ordernumber
                        , info)
    VALUES (rgetcriteria.entity
          , rgetcriteria.KEY
          , rgetcriteria.masterkey
          , rgetcriteria.display
          , cnnewrepid
          , rgetcriteria.selected
          , rgetcriteria.ordernumber
          , rgetcriteria.info);
  END LOOP;

  isr$trace.debug ('nCount',  'number of criteria inserted in isr$crit: ' || ncount, sCurrentName);

  isr$trace.stat('end','end', sCurrentName);
  RETURN oerrorobj;
EXCEPTION
  WHEN OTHERS THEN
     oErrorObj.handleError ( Stb$typedef.cnSeverityCritical, utd$msglib.getmsg ('exUnexpectedError', stb$security.getCurrentLanguage,  csP1 => sqlerrm(sqlcode)),
        sCurrentName, dbms_utility.format_error_stack ||chr(10)|| dbms_utility.format_error_backtrace );
    RETURN oerrorobj;
END copyreportparameter;

--******************************************************************************
FUNCTION createNewReport (nart IN NUMBER DEFAULT NULL)
  RETURN stb$oerror$record
IS
  sCurrentName                  CONSTANT VARCHAR2(100) := $$PLSQL_UNIT||'.createNewReport ('||nart||')';
  oErrorObj                     stb$oerror$record := STB$OERROR$RECORD();
  nnewrepid                     NUMBER;
  nParentRepid                  NUMBER;
BEGIN
  isr$trace.stat('begin','begin', sCurrentName);
  nParentRepid := stb$object.getCurrentRepid;
  -- call of the general function
  oErrorObj := createnewreport (nart, nnewrepid);
  isr$trace.debug ('nNewRepId Result',  'nNewRepId: ' || nnewrepid, sCurrentName);

  -- if the flag is set copy the common documents
  IF nvl(stb$util.getReptypeParameter (stb$object.getCurrentReporttypeid, 'COPY_COMMON_DOCS'), 'F') = 'T' THEN
    -- copy the common docs
    oErrorObj := stb$docks.copycommondocs (nParentRepid, nnewrepid);
  END IF;

  -- set the new ReportId and the package
  stb$object.setcurrentrepid (nnewrepid);
  isr$trace.stat('end','end', sCurrentName);
  RETURN oErrorObj;
EXCEPTION
  WHEN OTHERS THEN
     oErrorObj.handleError ( Stb$typedef.cnSeverityCritical, utd$msglib.getmsg ('exUnexpectedError', stb$security.getCurrentLanguage,  csP1 => sqlerrm(sqlcode)),
        sCurrentName, dbms_utility.format_error_stack ||chr(10)|| dbms_utility.format_error_backtrace );
    RETURN oErrorObj;
END createnewreport;

--******************************************************************************
FUNCTION createReportFromJob(cnRepid IN NUMBER, csTitle IN VARCHAR2, cnReporttypeid IN NUMBER)
  RETURN stb$oerror$record
IS
  sCurrentName    CONSTANT VARCHAR2(100) := $$PLSQL_UNIT||'.createReportFromJob';
  oErrorObj       stb$oerror$record := STB$OERROR$RECORD();

  nReporttypeid   STB$REPORT.REPORTTYPEID%TYPE;
  sSrNumber       STB$REPORT.SRNUMBER%TYPE;
  sTitle          STB$REPORT.TITLE%TYPE;
  nRepid          STB$REPORT.REPID%TYPE;

  ndocid                        NUMBER;
  sexists                       VARCHAR2 (1) := 'F';
  scheckedout                   stb$doctrail.checkedout%TYPE;
  spath                         stb$doctrail.path%TYPE;
  smachine                      stb$doctrail.machine%TYPE;
  soracleuser                   stb$doctrail.oracleuser%TYPE;

  CURSOR cGetReportInfo IS
    SELECT reporttypeid
         , srnumber
         , title
         , repid
      FROM stb$report
     WHERE ( (repid = cnRepid
          AND cnRepid IS NOT NULL)
         OR (title = csTitle
         AND reporttypeid = cnReporttypeid
         AND cnRepid IS NULL));

  CURSOR cGetLockSession IS
    SELECT locksession
      FROM STB$REPORT
     WHERE REPID = STB$OBJECT.getCurrentRepid;

  nLockedInSession STB$REPORT.LOCKSESSION%TYPE;

  CURSOR cIsJobScheduled IS
    SELECT case when /*NVL(jp.parametervalue, 'ONETIME') != 'ONETIME' and*/ us.job_creator is not null THEN 'T' else 'F' end
      FROM stb$jobparameter jp
         , stb$jobqueue jq
         , user_scheduler_jobs us
         , stb$report r
         , stb$jobsession js
     WHERE r.repid = stb$object.getCurrentRepId
       AND jp.parametername = 'FILTER_ITERATION'
       AND jq.jobid in (jp.jobid)
       AND TRIM(us.job_name(+)) = TRIM(jq.oraclejobid)
       AND js.jobid = jq.jobid
       AND js.oraclejobsessionid = STB$SECURITY.getCurrentSession;

  sScheduled VARCHAR2(1) := 'F';

  CURSOR cGetJobFlag(cnDocId IN NUMBER) IS
    SELECT NVL(job_Flag, 'OK')
      FROM stb$doctrail
     WHERE docid = cnDocId;

  nJobFlag   VARCHAR2(10);
BEGIN
  isr$trace.stat('begin','begin', sCurrentName);

  OPEN cGetReportInfo;
  FETCH cGetReportInfo INTO nReporttypeid, sSrNumber, sTitle, nRepid;
  CLOSE cGetReportInfo;

  Stb$object.setCurrentRepid(nRepid);

  isr$trace.debug ('stb$object.getCurrentRepId',  stb$object.getcurrentrepid, sCurrentName);
  isr$trace.debug ('stb$object.getCurrentTitle',  stb$object.getCurrentTitle, sCurrentName);
  isr$trace.debug ('stb$object.getCurrentSRNumber',  stb$object.getCurrentSRNumber, sCurrentName);
  isr$trace.debug ('stb$object.getCurrentReporttypeId',  stb$object.getCurrentReporttypeId, sCurrentName);
  isr$trace.debug ('stb$object.getcurrentrepstatus',  stb$object.getcurrentrepstatus, sCurrentName);
  isr$trace.debug ('stb$typedef.cnStatusdraft',  stb$typedef.cnStatusdraft, sCurrentName);

  IF stb$object.getCurrentRepstatus != stb$typedef.cnStatusdraft THEN

    OPEN cIsJobScheduled;
    FETCH cIsJobScheduled INTO sScheduled;
    IF cIsJobScheduled%NOTFOUND THEN
      sScheduled := 'F';
    END IF;
    CLOSE cIsJobScheduled;
    isr$trace.debug ('sScheduled',  sScheduled, sCurrentName);

    IF sScheduled = 'T' THEN
      oErrorObj.sErrorCode := utd$msglib.getmsg ('exReportNotInDraft', stb$security.getCurrentLanguage);

     oErrorObj.handleError ( Stb$typedef.cnSeverityCritical, utd$msglib.getmsg ('exReportNotInDraftText', stb$security.getCurrentLanguage,  csP1 => sqlerrm(sqlcode)),
        sCurrentName, dbms_utility.format_error_stack ||chr(10)|| dbms_utility.format_error_backtrace );
      RETURN oErrorObj;
    END IF;
  END IF;

  OPEN cGetLockSession;
  FETCH cGetLockSession INTO nLockedInSession;
  CLOSE cGetLockSession;

  -- Check if the document is locked
  IF Stb$object.getCurrentRepCondition = STB$TYPEDEF.cnStatusLocked AND nLockedInSession != STB$SECURITY.getCurrentSession THEN
    oErrorObj.handleError ( Stb$typedef.cnSeverityCritical, utd$msglib.getmsg ('exEntryNotAllowedText', stb$security.getCurrentLanguage,  csP1 => sqlerrm(sqlcode)),
        sCurrentName, dbms_utility.format_error_stack ||chr(10)|| dbms_utility.format_error_backtrace );
    RETURN oErrorObj;
  END IF;

  -- Check if a document already exists and if the document is checked in
  oErrorObj := stb$docks.LastDocCheckedIn (scheckedout, ndocid, spath, smachine, soracleuser);
  IF scheckedout = 'T' THEN
    oErrorObj.sErrorCode := utd$msglib.getmsg ('exDocCheckedout', stb$security.getCurrentLanguage);
     oErrorObj.handleError ( Stb$typedef.cnSeverityCritical, utd$msglib.getmsg ('exDocCheckedoutText', stb$security.getCurrentLanguage,  csP1 => sqlerrm(sqlcode)),
        sCurrentName, dbms_utility.format_error_stack ||chr(10)|| dbms_utility.format_error_backtrace );
    RETURN oErrorObj;
  END IF;

  -- create a new report id for the document if a document already exists for the current node else just run the job
  isr$trace.debug ('ndocid',  ndocid, sCurrentName);
  IF ndocid <> 0 AND stb$object.getCurrentRepstatus = stb$typedef.cnStatusdraft THEN
    oErrorObj := stb$docks.checkDocExistsForNodeid (to_char (stb$object.getCurrentRepid), sexists);
    isr$trace.debug ('sexists',  sexists, sCurrentName);
    IF sexists = 'T' THEN
      OPEN cGetJobFlag(ndocid);
      FETCH cGetJobFlag INTO nJobFlag;
      CLOSE cGetJobFlag;
      IF nJobFlag = 'D' THEN
        DELETE FROM ISR$DOC$PROPERTY
         WHERE docid IN (SELECT docid
                           FROM STB$DOCTRAIL d
                          WHERE UPPER (job_flag) IN ('D')
                            AND docid = ndocid);
        DELETE FROM STB$DOCTRAIL
         WHERE UPPER (job_flag) IN ('D')
           AND docid = ndocid;
      ELSE
        oErrorObj := createnewreport (stb$typedef.cnversion);
        oErrorObj := isr$reporttype$package.createsraudit (stb$object.getcurrentsrnumber);
        sessionUpdate;
      END IF;
    END IF;
  END IF;

  COMMIT;
  ISR$TREE$PACKAGE.sendSyncCall('REPORT', STB$OBJECT.getCurrentRepid);

  isr$trace.stat('end','end', sCurrentName);
  RETURN oErrorObj;

EXCEPTION WHEN OTHERS THEN
     oErrorObj.handleError ( Stb$typedef.cnSeverityCritical, utd$msglib.getmsg ('exUnexpectedError', stb$security.getCurrentLanguage,  csP1 => sqlerrm(sqlcode)),
        sCurrentName, dbms_utility.format_error_stack ||chr(10)|| dbms_utility.format_error_backtrace );
  RETURN oErrorObj;
END createReportFromJob;

--******************************************************************************
function setMenuAccessForModifyReport(sparameter   in varchar2, sMenuAllowed in out varchar2 )
  return stb$oerror$record
is
  oErrorObj                     stb$oerror$record := STB$OERROR$RECORD();
  sCurrentName      CONSTANT VARCHAR2(100) := $$PLSQL_UNIT||'.setMenuAccessForModifyReport (' || sparameter || ')';
  sDocFlag                      VARCHAR2 (1);
  sCurrentDocId                 number := STB$OBJECT.GetCurrentDocId;
  sExists                       VARCHAR2 (1);

  CURSOR ccheckXmlexists (cnrepid IN stb$report.repid%type) IS
    SELECT 'T'
    FROM   stb$doctrail T
    WHERE  T.nodeid = cnrepid
    AND    T.doctype = 'X';

  CURSOR cCheckHigherDraftsExists(cnrepid IN stb$report.repid%type) IS
    SELECT 'T'
    FROM   stb$report
    WHERE  repid > cnRepid
    AND    title = (select title from stb$report where repid = cnRepid)
    AND    srnumber = (select srnumber from stb$report where repid = cnRepid)
    AND    status = stb$typedef.cnStatusDraft
    AND    condition not in (stb$typedef.cnStatusDeleted, stb$typedef.cnStatusDeactive);

begin
  isr$trace.stat('begin','sCurrentDocId: '||sCurrentDocId, sCurrentName);
  IF stb$util.getReptypeParameter (stb$object.getCurrentReporttypeid, 'REQUIRES_VERSIONING') = 'F' THEN
    -- check if xml file exists
    OPEN cCheckXMLExists (stb$object.getCurrentRepid);
    FETCH cCheckXMLExists INTO  sDocFlag;
    IF cCheckXMLExists%NOTFOUND OR sDocFlag IS NULL THEN
      sMenuAllowed := 'T';
    ELSE
      sMenuAllowed := 'F';
      CLOSE ccheckxmlexists;
      isr$trace.stat('end', 'sParameter: '||sparameter||'  sMenuAllowed: '||sMenuAllowed  , sCurrentName);
      RETURN oErrorObj;
    END IF;
    CLOSE cCheckXMLExists;
    END IF;

    OPEN cCheckHigherDraftsExists(STB$OBJECT.getCurrentRepid);
    FETCH cCheckHigherDraftsExists INTO sExists;
    IF cCheckHigherDraftsExists%NOTFOUND OR sExists = 'F' THEN
    sMenuAllowed := 'T';
    ELSE
    sMenuAllowed := 'F';
    END IF;
    CLOSE cCheckHigherDraftsExists;
  return oErrorObj;
end setMenuAccessForModifyReport;
--******************************************************************************
function setMenuAccess( sparameter   in varchar2,
                        sMenuAllowed out varchar2 )
  return stb$oerror$record
is
  oErrorObj                     stb$oerror$record := STB$OERROR$RECORD();
  sCurrentName      CONSTANT VARCHAR2(100) := $$PLSQL_UNIT||'.setMenuAccess (' || sparameter || ')';
  slockme                       VARCHAR2 (1);
  sdocflag                      VARCHAR2 (1);
  scheckedout                   VARCHAR2 (1);
  sjobexists                    VARCHAR2 (1);
  sexists                       VARCHAR2 (1);
  ndocid                        NUMBER;
  ndocid1                       NUMBER;
  ndocid2                       NUMBER;
  csIsrDocType                  VARCHAR2(1):='D';
  sDocType                      stb$doctrail.doctype%TYPE := csIsrDocType;
  sDocDefinition                STB$DOCTRAIL.DOC_DEFINITION%TYPE :='stability';
  nDocversion                   NUMBER;
  nLastFinalRepid               NUMBER := -1;
  sCurrentRepId                 number := STB$OBJECT.getCurrentRepId;
  sCurrentNodeType              STB$CONTEXTMENU.nodetype%TYPE;

  CURSOR cGetReportStatus (cnRepid IN NUMBER) IS
    SELECT status
    FROM   stb$report
    WHERE  repid = cnRepid;

  nReportStatus                 STB$REPORT.STATUS%TYPE;

  CURSOR cGetDocVersion (cndocid IN NUMBER) IS
    SELECT versiondisplayed
    FROM   stb$doctrail
    WHERE  docid = cndocid;

  CURSOR cisattachmentcheckedout (cndocid IN NUMBER) IS
    SELECT checkedout
    FROM   stb$doctrail
    WHERE  docid = cndocid;


  CURSOR cCheckFilesExists (cnrepid IN stb$report.repid%type) IS
    SELECT case when count(*) = 0 then 'F' else 'T' end
    FROM   isr$report$file
    WHERE  repid = cnrepid;

  CURSOR cCheckHigherDraftsExists(cnrepid IN stb$report.repid%type) IS
    SELECT 'T'
    FROM   stb$report
    WHERE  repid > cnRepid
    AND    title = (select title from stb$report where repid = cnRepid)
    AND    srnumber = (select srnumber from stb$report where repid = cnRepid)
    AND    status = stb$typedef.cnStatusDraft
    AND    condition not in (stb$typedef.cnStatusDeleted, stb$typedef.cnStatusDeactive);

  nEntryid        STB$REPORT.PARENTREPID%TYPE;

  cursor cIsArchived is
    SELECT 'T'
      FROM STB$REPORT
     WHERE repid = STB$OBJECT.getCurrentRepid
       AND status = 999;

  CURSOR cIsMenuAllowed IS
    SELECT CASE WHEN release = 'T' THEN 'F' ELSE 'T' END
      FROM isr$template
     WHERE fileid = STB$OBJECT.getCurrentNodeId;

  sIsArchived     VARCHAR2(1) := 'F';

  CURSOR cCheckRenderingStatus IS
    SELECT runstatus
      FROM isr$server
     WHERE servertypeid = 14
       AND runstatus = 'T';

  sStatus ISR$SERVER.runstatus%TYPE;

  CURSOR cCheckDoc IS
    select docid
      from stb$doctrail
     where (checkedout = 'T' or job_flag IN ('D','O','I','R'))
       and docid = STB$OBJECT.getCurrentDocid;

  rCheckDoc cCheckDoc%rowtype;


  CURSOR cHasArchivedReports IS
    SELECT NVL ( (SELECT 'T'
                    FROM STB$REPORT r1
                   WHERE TO_CHAR (r1.repid) = STB$OBJECT.getCurrentRepid
                     AND r1.status = 999), 'F')
      FROM DUAL;

  CURSOR cGetHighestStatus IS
    SELECT status
      FROM stb$report
     WHERE repid IN (SELECT max(repid) FROM STB$REPORT WHERE title = STB$OBJECT.getCurrentTitle);

  sHighestStatus STB$REPORT.STATUS%TYPE := 0;


  CURSOR cIsHighestVersion(cnFileid in NUMBER) IS
    SELECT 'T'
      FROM isr$template
     WHERE fileid = cnFileid
       AND (templatename, version) IN (SELECT templatename, MAX (version)
                                         FROM isr$template
                                       GROUP BY templatename);

BEGIN
  isr$trace.stat('begin','sCurrentRepId: '||sCurrentRepId, sCurrentName);

  sMenuAllowed := 'T';

  IF sparameter IN ('CAN_SWITCH_REPRIGHT','CAN_UPLOAD_COMMON_DOC','CAN_CREATE_REPORT_VARIANT','CAN_CREATE_CRITERIA','CAN_OFFSHOOT_DOC') THEN
    isr$trace.stat('end', 'sParameter: '||sparameter||'  sMenuAllowed: '||sMenuAllowed  , sCurrentName);
    RETURN oErrorObj;
  END IF;

  if sparameter like 'CAN_DISPLAY_%' and sparameter != 'CAN_DISPLAY_REPORT_FILES' and sparameter not like 'CAN_DISPLAY_%AUDIT' THEN
    open cCheckDoc;
    fetch cCheckDoc into rCheckDoc;
    if cCheckDoc%FOUND then
      sMenuAllowed := 'F';
    end if;
    close cCheckDoc;
    isr$trace.stat('end', 'sParameter: '||sparameter||'  sMenuAllowed: '||sMenuAllowed  , sCurrentName);
    return oErrorObj;
  end if;

  IF sParameter like 'CAN_DISPLAY_%AUDIT' THEN
    oErrorObj := stb$audit.checkauditexists (sMenuAllowed,
                            case when sParameter = 'CAN_DISPLAY_SRAUDIT' then STB$OBJECT.getCurrentTitle
                                 when sParameter = 'CAN_DISPLAY_REPORTAUDIT' then TO_CHAR(STB$OBJECT.getCurrentRepid)
                                 when sParameter = 'CAN_DISPLAY_REPORTTYPEAUDIT' then TO_CHAR(STB$OBJECT.getCurrentReporttypeId)
                                 when sParameter = 'CAN_DISPLAY_TEMPLATEAUDIT' then STB$OBJECT.getCurrentNode().sDisplay
                                 else stb$object.getCurrentNodeId end, REPLACE(sParameter, 'CAN_DISPLAY_'));
    -- if no audit exists in table STB$AUDIT, then no further processing necessary
    return oErrorObj;
  END IF;

  sDocDefinition := STB$DOCKS.getDocDefType (Stb$object.GetCurrentDocId, Stb$object.getCurrentRepId) ;
  isr$trace.info('sDocDefinition', sDocDefinition, sCurrentName);

  sDocType := STB$DOCKS.getDocType(Stb$object.GetCurrentDocId, Stb$object.getCurrentRepId);
  isr$trace.info('sDocType',sDocType, sCurrentName);
  
  sCurrentNodeType := stb$object.getCurrentNodeType;

  isr$trace.debug ('sCurrentNodeType',  sCurrentNodeType, sCurrentName);
  isr$trace.debug ('stb$object.getCurrentRepstatus', stb$object.getCurrentRepstatus, sCurrentName);

  -- check if the functionality is accessable in the current context
  -- when the node type is in REPORT, DOCUMENT, RAWDATA, REMOTESNAPSHOT        -- RAWDATA added in ISRC-1002, vs 05.Jun.2018   
  --'DOCUMENT%' added in NUVIB-105, NUVIB-19 for nodetypes 'DOCUMENT_WORD' and 'DOCUMENT_EXCEL'
  IF sMenuAllowed = 'T' AND (sCurrentNodeType like 'DOCUMENT%' or sCurrentNodeType IN ('RAWDATA', 'REMOTESNAPSHOT')
                   or sCurrentNodeType like 'REPORT%'
                   OR (sparameter IN ('CAN_RUN_REPORT') AND sCurrentNodeType IS NULL) /*from job*/) THEN

    -- check if the report is locked by the user himself
    oErrorObj := stb$security.checklockedbymyself (STB$SECURITY.getCurrentSession, slockme);
    isr$trace.debug ('sLockMe',  'sLockMe: ' || slockme, sCurrentName);

    -- check if an iStudyReporter document exists
    oErrorObj := stb$docks.docexists (sdocflag, csIsrDocType, scheckedout);
    isr$trace.debug ('sDocFlag', sDocFlag, sCurrentName);
    isr$trace.debug ('scheckedout', scheckedout, sCurrentName);

    -- if a report has the status final or inspection there has to be a document,
    -- if there is not a document all function apart from creating that document are deactivated
    -- if the node is in the inspection state then the inspection status can be deactivated
    IF sDocFlag = 'F' THEN

      OPEN cIsArchived;
      FETCH cIsArchived INTO sIsArchived;
      CLOSE cIsArchived;
     isr$trace.debug ('sIsArchived',  sIsArchived, sCurrentName);

      CASE
        WHEN  (stb$object.getCurrentRepstatus = stb$typedef.cnStatusfinal) AND sIsArchived = 'T' THEN
          sMenuAllowed := 'F';
          isr$trace.stat('end', 'sParameter: '||sparameter||'  sMenuAllowed: '||sMenuAllowed  , sCurrentName);
          RETURN oErrorObj;

        WHEN  (stb$object.getCurrentRepstatus = stb$typedef.cnStatusfinal OR stb$object.getCurrentRepstatus = stb$typedef.cnStatusInspection)
        AND sparameter NOT IN ('CAN_RUN_REPORT', 'CAN_DEACTIVATE_INSPECTION') THEN
          isr$trace.debug('noDocument', 'noDocument and not run_report ', sCurrentName);
          sMenuAllowed := 'F';
          isr$trace.stat('end', 'sParameter: '||sparameter||'  sMenuAllowed: '||sMenuAllowed  , sCurrentName);
          RETURN oErrorObj;

        -- 'CAN_RUN_REPORT'
        WHEN (stb$object.getCurrentRepstatus = stb$typedef.cnStatusfinal OR stb$object.getCurrentRepstatus = stb$typedef.cnStatusInspection)
        AND sparameter = 'CAN_RUN_REPORT' THEN
          isr$trace.debug ('noDocument',  'noDocument and run report', sCurrentName);
          -- first check if not just before a job has been started
          oErrorObj := stb$job.checkjobexists (to_char (stb$object.getCurrentRepid), sjobexists);
          IF sjobexists = 'T' THEN
            sMenuAllowed := 'F';
          ELSE
            sMenuAllowed := 'T';
          END IF;
          isr$trace.stat('end', 'sParameter: '||sparameter||'  sMenuAllowed: '||sMenuAllowed  , sCurrentName);
          RETURN oErrorObj;

        -- 'CAN_DEACTIVATE_INSPECTION'
        WHEN stb$object.getCurrentRepstatus = stb$typedef.cnStatusInspection
        AND sparameter = 'CAN_DEACTIVATE_INSPECTION' THEN
          -- first check if not just before a job has been started
          oErrorObj := stb$job.checkjobexists (to_char (stb$object.getCurrentRepid), sjobexists);
          IF sjobexists = 'T' THEN
            sMenuAllowed := 'F';
          ELSE
            sMenuAllowed := 'T';
          END IF;
          isr$trace.stat('end', 'sParameter: '||sparameter||'  sMenuAllowed: '||sMenuAllowed  , sCurrentName);
          RETURN oErrorObj;
        WHEN stb$object.getCurrentRepstatus = stb$typedef.cnStatusfinal
        AND sparameter = 'CAN_DEACTIVATE_INSPECTION' THEN
            sMenuAllowed := 'F';
            isr$trace.stat('end', 'sParameter: '||sparameter||'  sMenuAllowed: '||sMenuAllowed  , sCurrentName);
            RETURN oErrorObj;
        ELSE
            sMenuAllowed := 'T';
      END CASE;
    END IF;

    -- check of the current combination
    isr$trace.debug ('before checkcontextrep', 'sMenuAllowed: '||sMenuAllowed, sCurrentName);
    oErrorObj := stb$security.checkcontextrep (sparameter, NVL(sCurrentNodeType, 'REPORT'), sDocDefinition, sMenuAllowed, slockme);
    isr$trace.debug ('after checkcontextrep', 'sMenuAllowed: '||sMenuAllowed, sCurrentName);

    IF sMenuAllowed = 'T' THEN
      -- check the status of the children nodes
      oErrorObj := stb$security.checkchildrenstatus (sparameter, NVL(sCurrentNodeType, 'REPORT'), sDocDefinition, sMenuAllowed);
      isr$trace.debug ('after checkchildrenstatus', 'sMenuAllowed '||sMenuAllowed, sCurrentName);

      IF sMenuAllowed = 'F' THEN
        isr$trace.stat('end', 'sParameter: '||sparameter||'  sMenuAllowed: '||sMenuAllowed  , sCurrentName);
        RETURN oErrorObj;
      END IF;
    ELSE
      isr$trace.stat('end', 'sParameter: '||sparameter||'  sMenuAllowed: '||sMenuAllowed  , sCurrentName);
      RETURN oErrorObj;                                                                                                                                                      -- not the right context
    END IF;

  END IF;

  IF sMenuAllowed = 'T' THEN

    isr$trace.debug ('start special cases', 'sMenuAllowed '||sMenuAllowed, sCurrentName);

    IF STB$OBJECT.getCurrentNodeType = 'REPORT' THEN

      OPEN cHasArchivedReports;
      FETCH cHasArchivedReports INTO sExists;
      CLOSE cHasArchivedReports;

      isr$trace.debug ('cHasArchivedReports',  'sExists '||sExists, sCurrentName);

      IF sExists = 'T' THEN
        sMenuAllowed := 'F';
        isr$trace.stat('end', 'sParameter: '||sparameter||'  sMenuAllowed: '||sMenuAllowed  , sCurrentName);
        RETURN oErrorObj;
      ELSE
        sMenuAllowed := 'T';
      END IF;

    END IF;

    FOR rGetDocument IN ( SELECT job_flag
                            FROM stb$doctrail
                           WHERE docid = STB$OBJECT.getCurrentDocid
                             AND job_flag is not null ) LOOP
      sMenuAllowed := 'F';
      isr$trace.debug ('has job flag',  'rGetDocument.job_flag '||rGetDocument.job_flag, sCurrentName);
      isr$trace.stat('end', 'sParameter: '||sparameter||'  sMenuAllowed: '||sMenuAllowed  , sCurrentName);
      RETURN oErrorObj;
    END LOOP;

    --  special cases
    CASE

      --=====CAN_RUN_REPORT
      WHEN STB$OBJECT.getCurrentRepid is null AND sParameter = 'CAN_RUN_REPORT' THEN
        isr$trace.info('CAN_CONFIGURE_ORACLE_JOBS', STB$SECURITY.checkGroupRight ('CAN_CONFIGURE_ORACLE_JOBS') , sCurrentName);
        IF STB$SECURITY.checkGroupRight ('CAN_CONFIGURE_ORACLE_JOBS') = 'T' THEN
          OPEN cGetHighestStatus;
          FETCH cGetHighestStatus INTO sHighestStatus;
          CLOSE cGetHighestStatus;
          OPEN cCheckRenderingStatus;
          FETCH cCheckRenderingStatus INTO sStatus;
          isr$trace.debug('sHighestStatus', sHighestStatus, sCurrentName);
          isr$trace.debug('sStatus', sStatus, sCurrentName);
          IF (cCheckRenderingStatus%FOUND or sStatus = 'T') AND sHighestStatus = STB$TYPEDEF.CNSTATUSDRAFT THEN
            sMenuAllowed := 'T';
          ELSE
            sMenuAllowed := 'F';
          END IF;
          CLOSE cCheckRenderingStatus;
        ELSE
          sMenuAllowed := 'F';
        END IF;


      --=====CAN_MODIFY_REPTYPE_PARAMETER_VALUES,CAN_MODIFY_REPTYPE_CALC_VALUES,CAN_MODIFY_REPTYPE_STYLE_VALUES
      WHEN sPARAMETER IN ('CAN_MODIFY_REPTYPE_PARAMETER_VALUES', 'CAN_MODIFY_REPTYPE_CALC_VALUES', 'CAN_MODIFY_REPTYPE_STYLE_VALUES', 'CAN_UPLOAD_NEW_TEMPLATE') THEN

        isr$trace.debug ('STB$OBJECT.getCurrentNodeId', STB$OBJECT.getCurrentNodeId, sCurrentName);

        OPEN cIsHighestVersion(TO_NUMBER(STB$OBJECT.getCurrentNodeId));
        FETCH cIsHighestVersion INTO sMenuAllowed;
        IF cIsHighestVersion%NOTFOUND THEN
          sMenuAllowed := 'F';
        END IF;
        CLOSE cIsHighestVersion;
        isr$trace.debug('sMenuAllowed', sMenuAllowed  , sCurrentName);

        IF sMenuAllowed = 'T' AND sParameter <> 'CAN_UPLOAD_NEW_TEMPLATE' THEN
          EXECUTE IMMEDIATE 'select case when count(*) > 0 then ''T'' else ''F'' end from isr$file$'||REPLACE(REPLACE(sPARAMETER, 'CAN_MODIFY_REPTYPE_'), '_VALUES')||'$values where fileid = '||STB$OBJECT.getCurrentNodeId
          INTO sMenuAllowed;
        END IF;

        isr$trace.stat('end', sMenuAllowed  , sCurrentName);
        RETURN oErrorObj;

      --=====CAN_DISPLAY_REPORT_FILES
      WHEN sparameter IN ('CAN_DISPLAY_REPORT_FILES') THEN

          OPEN cCheckFilesExists (stb$object.getCurrentRepid);
          FETCH cCheckFilesExists INTO sMenuAllowed;
          CLOSE cCheckFilesExists;
          isr$trace.stat('end', 'sParameter: '||sparameter||'  sMenuAllowed: '||sMenuAllowed  , sCurrentName);
          RETURN oErrorObj;

      --=====CAN_MODIFY_REPORT
      WHEN sparameter IN ('CAN_MODIFY_REPORT', 'CAN_MODIFY_OUTPUTOPTIONS', 'CAN_REPLACE_REPORT_SERVER') THEN

        oErrorObj := setMenuAccessForModifyReport(sparameter, sMenuAllowed);

      --=====CAN_RUN_REPORT
      WHEN sParameter IN ('CAN_RUN_REPORT','CAN_RELEASE_FOR_INSPECTION') THEN

          OPEN cCheckHigherDraftsExists(STB$OBJECT.getCurrentRepid);
          FETCH cCheckHigherDraftsExists INTO sExists;
          IF cCheckHigherDraftsExists%NOTFOUND OR sExists = 'F' THEN
            sMenuAllowed := 'T';
          ELSE
            sMenuAllowed := 'F';
          END IF;
          CLOSE cCheckHigherDraftsExists;

          -- additional check for sMenuAllowed, tests document checkout state
          sMenuAllowed := checkDocCheckout( sMenuAllowed, STB$OBJECT.getCurrentRepid );

      --=====CAN_RELEASE_MASTERTEMPLATE
      WHEN sparameter IN ('CAN_RELEASE_MASTERTEMPLATE') THEN

        OPEN cIsMenuAllowed;
        FETCH cIsMenuAllowed INTO sMenuAllowed;
        CLOSE cIsMenuAllowed;

      --=====CAN_LOCK_REPORT
      WHEN sparameter IN ('CAN_LOCK_REPORT')
      AND stb$object.getCurrentRepstatus = stb$typedef.cnStatusInspection
      AND STB$UTIL.getReptypeParameter(stb$object.getCurrentReporttypeId, 'CAN_LOCK_REPORT') = 'F' THEN
        sMenuAllowed := 'F';

      --=====CAN_UNLOCK_REPORT
      WHEN sparameter IN ('CAN_UNLOCK_REPORT')
      AND stb$object.getCurrentRepstatus = stb$typedef.cnStatusInspection
      AND STB$UTIL.getReptypeParameter(stb$object.getCurrentReporttypeId, 'CAN_UNLOCK_REPORT') = 'F' THEN
        sMenuAllowed := 'F';

      --=====CAN_LOCK_REPORT/CAN_DEACTIVATE_INSPECTION
      WHEN sparameter IN ('CAN_LOCK_REPORT','CAN_DEACTIVATE_INSPECTION')
      AND stb$object.getCurrentRepstatus = stb$typedef.cnStatusInspection
      AND STB$UTIL.getReptypeParameter(stb$object.getCurrentReporttypeId, 'CAN_LOCK_REPORT') = 'T' THEN

        -- check if the lock/unlock count is even
        SELECT count(*)
          INTO nEntryId
          FROM stb$esig e
         WHERE to_char (stb$object.getCurrentRepid) = e.REFERENCE
           AND e.action LIKE 'CAN_%LOCK_REPORT'
           AND e.success = 'T';

        IF nEntryId mod 2 = 0 THEN
          sMenuAllowed := 'T';
        else
          sMenuAllowed := 'F';
        end if;

        -- additional check for sMenuAllowed, tests document checkout state
        sMenuAllowed := checkDocCheckout( sMenuAllowed, STB$OBJECT.getCurrentRepid );

      --=====CAN_UNLOCK_REPORT
      WHEN sparameter IN ('CAN_UNLOCK_REPORT')
      AND stb$object.getCurrentRepstatus = stb$typedef.cnStatusInspection
      AND STB$UTIL.getReptypeParameter(stb$object.getCurrentReporttypeId, 'CAN_UNLOCK_REPORT') = 'T' THEN

        -- check if the lock/unlock count is not even
        SELECT count(*)
          INTO nEntryId
          FROM stb$esig e
         WHERE to_char (stb$object.getCurrentRepid) = e.REFERENCE
           AND e.action LIKE 'CAN_%LOCK_REPORT'
           AND e.success = 'T';

        IF nEntryId mod 2 = 0 THEN
          sMenuAllowed := 'F';
        else
          sMenuAllowed := 'T';
        end if;

        -- additional check for sMenuAllowed, tests document checkout state
        sMenuAllowed := checkDocCheckout( sMenuAllowed, STB$OBJECT.getCurrentRepid );

      --=====CAN_FINALIZE_REPORT
      WHEN sparameter = 'CAN_FINALIZE_REPORT'
      AND stb$object.getCurrentRepstatus = stb$typedef.cnStatusInspection
      AND STB$UTIL.getReptypeParameter(stb$object.getCurrentReporttypeId, 'CAN_LOCK_REPORT') = 'T' THEN

        -- check if the lock/unlock count is not even
        SELECT count(*)
          INTO nEntryId
          FROM stb$esig e
         WHERE to_char (stb$object.getCurrentRepid) = e.REFERENCE
           AND e.action LIKE 'CAN_%LOCK_REPORT'
           AND e.success = 'T';

        IF nEntryId mod 2 = 0 THEN
          sMenuAllowed := 'F';
        else
          sMenuAllowed := 'T';
        end if;

        -- additional check for sMenuAllowed, tests document checkout state
        sMenuAllowed := checkDocCheckout( sMenuAllowed, STB$OBJECT.getCurrentRepid );

      --=====CAN_CHECKOUT_COMMON_DOC/CAN_SEND_AS_EMAIL_CHECKOUT
      WHEN sparameter IN ('CAN_CHECKOUT_COMMON_DOC','CAN_SEND_AS_EMAIL_CHECKOUT')
      AND stb$object.getcurrentnodetype = 'COMMONDOC' THEN

        IF STB$DOCKS.getDocDefType(stb$object.getCurrentNodeId) is not null THEN
          sMenuAllowed := 'F';
          isr$trace.stat('end', 'sParameter: '||sparameter||'  sMenuAllowed: '||sMenuAllowed  , sCurrentName);
          RETURN oErrorObj;
        ELSE
          OPEN cisattachmentcheckedout (stb$object.getCurrentNodeId);
          FETCH cisattachmentcheckedout
          INTO  scheckedout;
          CLOSE cisattachmentcheckedout;
          isr$trace.debug ('CAN_CHECKOUT_COMMON_DOC', 'scheckedout:' || scheckedout, sCurrentName);

          IF scheckedout = 'F' THEN
            sMenuAllowed := 'T';
          ELSE
            sMenuAllowed := 'F';
            isr$trace.stat('end', 'sParameter: '||sparameter||'  sMenuAllowed: '||sMenuAllowed  , sCurrentName);
            RETURN oErrorObj;
          END IF;
        END IF;

      --=====CAN_DELETE_COMMON, CAN_MODIFY_COMMON_DOC
      WHEN sParameter IN ('CAN_DELETE_COMMON', 'CAN_MODIFY_COMMON_DOC')
      AND stb$object.getcurrentnodetype = 'COMMONDOC' THEN

        IF STB$DOCKS.getDocDefType(stb$object.getCurrentNodeId) IS NOT NULL THEN

         isr$trace.debug ('repid', stb$object.getCurrentNodeId(-1), sCurrentName);

          OPEN cGetReportStatus(TO_NUMBER(stb$object.getCurrentNodeId(-1)));
          FETCH cGetReportStatus INTO nReportStatus;
          CLOSE cGetReportStatus;
          isr$trace.debug ('nReportStatus',  nReportStatus, sCurrentName);

          oErrorObj:= stb$docks.getMaxDocidInSRNumber(TO_NUMBER(stb$object.getCurrentNodeId(-1)), ndocid1, 'D', null);
          isr$trace.debug ('ndocid1', ndocid1, sCurrentName);

          oErrorObj := stb$docks.getmaxdocidforrepid (TO_NUMBER(stb$object.getCurrentNodeId(-1)), ndocid2, scheckedout, 'D', null);
          isr$trace.debug ('ndocid2', ndocid2, sCurrentName);

          IF nReportStatus = STB$TYPEDEF.cnStatusFinal OR ndocid1 != ndocid2 THEN
            sMenuAllowed := 'F';
            isr$trace.stat('end', 'sParameter: '||sparameter||'  sMenuAllowed: '||sMenuAllowed  , sCurrentName);
            RETURN oErrorObj;
          ELSE
            sMenuAllowed := 'T';
          END IF;
        ELSE
          sMenuAllowed := 'T';
        END IF;

      --=====CAN_DELETE_DOCUMENT
      WHEN sParameter IN ('CAN_DELETE_DOCUMENT')
      AND stb$object.getcurrentnodetype = 'DOCUMENT' THEN

        OPEN cGetDocVersion(STB$OBJECT.getCurrentDocid);
        FETCH cGetDocVersion INTO nDocVersion;
        CLOSE cGetDocVersion;

        -- check if the document is the highest docid for the node
        oErrorObj:= stb$docks.GETMAXDOCIDINSRNUMBER(stb$object.getCurrentRepid, ndocid, sDocType, sDocDefinition);
        isr$trace.info('nDocId','nDocId:' || nDocId, sCurrentName);

        IF Stb$object.GetCurrentDocId != nDocid
        OR nDocVersion = 0 THEN
          sMenuAllowed := 'F';
          isr$trace.stat('end', 'sParameter: '||sparameter||'  sMenuAllowed: '||sMenuAllowed  , sCurrentName);
          RETURN oErrorObj;
        END IF;

      --=====CAN_CHECKOUT/CAN_CHECKOUT_UNPROTECTED/CAN_SEND_AS_EMAIL_CHECKOUT/CAN_ADD_DOCPROPERTY
      WHEN sParameter IN ('CAN_CHECKOUT','CAN_CHECKOUT_UNPROTECTED','CAN_SEND_AS_EMAIL_CHECKOUT','CAN_ADD_DOCPROPERTY','CAN_MODIFY_WITH_COMMON_DOC')
      AND stb$object.getcurrentnodetype like 'DOCUMENT%' THEN

        -- check if the document is the highest docid for the node
        oErrorObj:= stb$docks.GETMAXDOCIDINSRNUMBER(stb$object.getCurrentRepid, ndocid, sDocType, sDocDefinition);
        isr$trace.debug('nDocId','nDocId:' || nDocId, sCurrentName);

        IF Stb$object.GetCurrentDocId != nDocid THEN
          sMenuAllowed := 'F';
          isr$trace.stat('end', 'sParameter: '||sparameter||'  sMenuAllowed: '||sMenuAllowed  , sCurrentName);
          RETURN oErrorObj;
        END IF;

        -- first check if the report is in the inspection state
        if stb$object.getCurrentRepStatus = Stb$typedef.cnStatusInspection
        and STB$UTIL.getReptypeParameter(stb$object.getCurrentReporttypeId, 'CAN_LOCK_REPORT') = 'T' then
          -- check if the lock/unlock count is even
          SELECT count(*)
            INTO nEntryId
            FROM stb$esig e
           WHERE to_char (stb$object.getCurrentRepid) = e.REFERENCE
             AND e.action LIKE 'CAN_%LOCK_REPORT'
             AND e.success = 'T';

          IF nEntryId mod 2 = 0 THEN
            sMenuAllowed := 'T';
          else
            sMenuAllowed := 'F';
          end if;
        end if;

        IF sMenuAllowed = 'T' THEN

          IF sParameter = 'CAN_CHECKOUT_UNPROTECTED' THEN
            sMenuAllowed := STB$UTIL.GetReportParameterValue('PROTECTED', STB$OBJECT.getCurrentRepid);
          END IF;

          IF sParameter = 'CAN_MODIFY_WITH_COMMON_DOC' THEN
            sMenuAllowed := NVL(STB$UTIL.getReptypeParameter(stb$object.getCurrentReporttypeId, 'USE_APPENDIX'), 'F');
          END IF;

        END IF;

      --=====CAN_CHECKOUT/CAN_CHECKOUT_UNPROTECTED/CAN_SEND_AS_EMAIL_CHECKOUT/CAN_ADD_DOCPROPERTY
      WHEN sParameter IN ('CAN_DELETE_DOCUMENT')
      AND stb$object.getcurrentnodetype = 'RAWDATA' THEN

        -- check if the document is the highest docid for the node
        oErrorObj:= stb$docks.GETMAXDOCIDINSRNUMBER(stb$object.getCurrentRepid, ndocid, sDocType, null);
        isr$trace.info('nDocId','nDocId:' || nDocId, sCurrentName);

        IF Stb$object.GetCurrentDocId != nDocid THEN
          sMenuAllowed := 'F';
          isr$trace.stat('end', 'sParameter: '||sparameter||'  sMenuAllowed: '||sMenuAllowed  , sCurrentName);
          RETURN oErrorObj;
        END IF;

        -- check if the document is the highest docid for the node
        oErrorObj:= stb$docks.GETMAXDOCIDINSRNUMBER(stb$object.getCurrentRepid, ndocid, 'D', sDocDefinition);
        isr$trace.info('nDocId','nDocId:' || nDocId, sCurrentName);

        IF Stb$object.GetCurrentDocId < nDocid THEN
          sMenuAllowed := 'F';
          isr$trace.stat('end', 'sParameter: '||sparameter||'  sMenuAllowed: '||sMenuAllowed  , sCurrentName);
          RETURN oErrorObj;
        END IF;


      --=====CAN_CHECKIN_COMMON_DOC/CAN_UNDO_CHECKOUT_COMMON_DOC
      WHEN sparameter IN ('CAN_CHECKIN_COMMON_DOC','CAN_UNDO_CHECKOUT_COMMON_DOC') THEN
          OPEN cisattachmentcheckedout (stb$object.getCurrentNodeId);
          FETCH cisattachmentcheckedout
          INTO  scheckedout;
          CLOSE cisattachmentcheckedout;
          isr$trace.debug ('CAN_CHECKIN_COMMON_DOC/CAN_UNDO_CHECKOUT_COMMON_DOC', 'scheckedout:' || scheckedout, sCurrentName);

          IF scheckedout = 'T' THEN
            sMenuAllowed := 'T';
          ELSE
            sMenuAllowed := 'F';
            isr$trace.stat('end', 'sParameter: '||sparameter||'  sMenuAllowed: '||sMenuAllowed  , sCurrentName);
            RETURN oErrorObj;
          END IF;

      --=====CAN_CREATE_REPORT_VARIANT_DOC
      WHEN sparameter = 'CAN_CREATE_REPORT_VARIANT_DOC' THEN
        -- check if the last document is checked in
        oErrorObj := stb$docks.getmaxdocidforrepid (stb$object.getCurrentRepid, ndocid, scheckedout, sDocType, sDocDefinition);
        isr$trace.debug ('scheckedout',  'scheckedout:' || scheckedout, sCurrentName);

        IF ndocid IS NOT NULL AND scheckedout = 'F' THEN
          sMenuAllowed := 'T';
        ELSE
          sMenuAllowed := 'F';
          isr$trace.stat('end', 'sParameter: '||sparameter||'  sMenuAllowed: '||sMenuAllowed  , sCurrentName);
          RETURN oErrorObj;
        END IF;

      --=====CAN_RESTORE_REPORT
      WHEN sparameter = 'CAN_RESTORE_REPORT' THEN
          -- check if draft versions exists
          oErrorObj := checkArchivedReportsExists (sexists);
          isr$trace.debug ('end',  ' sExists:' || sexists, sCurrentName);

          IF sexists = 'F' THEN
            sMenuAllowed := 'F';
            isr$trace.stat('end', 'sParameter: '||sparameter||'  sMenuAllowed: '||sMenuAllowed  , sCurrentName);
            RETURN oErrorObj;
          ELSE
            sMenuAllowed := 'T';
          END IF;

      --=====CAN_ARCHIVE_REPORTS
      WHEN sparameter = 'CAN_ARCHIVE_REPORTS'
       AND STB$OBJECT.getCurrentNodetype = 'REPORT' THEN

          sMenuAllowed := STB$UTIL.getSystemParameter('ARCHIVING_ALLOWED');
          isr$trace.debug ('ARCHIVING_ALLOWED', 'sMenuAllowed:' || sMenuAllowed, sCurrentName);
          IF sMenuAllowed = 'F' THEN
            isr$trace.stat('end', 'sParameter: '||sparameter||'  sMenuAllowed: '||sMenuAllowed  , sCurrentName);
            RETURN oErrorObj;
          END IF;

          oErrorObj := checkDraftVersionsExists (sexists);
          isr$trace.debug ('checkDraftVersionsExists', 'sExists:' || sexists, sCurrentName);

          oErrorObj := getlastFinalVersion (nLastFinalRepid);
          isr$trace.debug ('getlastFinalVersion',  'nLastFinalRepid:' || nLastFinalRepid, sCurrentName);

          IF (sexists = 'F'
          AND STB$UTIL.getReptypeParameter(STB$OBJECT.getCurrentReporttypeId, 'HAS_SRNUMBER') = 'T'
          AND nLastFinalRepid = -1) THEN
            sMenuAllowed := 'F';
            isr$trace.stat('end', 'sParameter: '||sparameter||'  sMenuAllowed: '||sMenuAllowed  , sCurrentName);
            RETURN oErrorObj;
          ELSE
            sMenuAllowed := 'T';
          END IF;

      --=====CAN_ARCHIVE_REPORTS
      WHEN sparameter = 'CAN_ARCHIVE_REPORTS'
       AND STB$OBJECT.getCurrentNodetype != 'REPORT' THEN

          sMenuAllowed := STB$UTIL.getSystemParameter('ARCHIVING_ALLOWED');
          isr$trace.debug ('ARCHIVING_ALLOWED', 'sMenuAllowed:' || sMenuAllowed, sCurrentName);
          IF sMenuAllowed = 'F' THEN
            isr$trace.stat('end', 'sParameter: '||sparameter||'  sMenuAllowed: '||sMenuAllowed  , sCurrentName);
            RETURN oErrorObj;
          END IF;

          isr$trace.debug ('look for locked reports', 'look for locked reports', sCurrentName);
          FOR rGetLockedReports IN (SELECT repid
                                      FROM stb$report
                                     WHERE ( (srnumber = STB$OBJECT.getCurrentSRNumber
                                          AND STB$OBJECT.getCurrentNodetype = 'SRNUMBER')
                                         OR (title = STB$OBJECT.getCurrentTitle
                                         AND STB$OBJECT.getCurrentNodetype = 'TITLE'))
                                       AND (condition IN (STB$TYPEDEF.cnStatusDeleted) -- [ISRC-866] removed STB$TYPEDEF.cnStatusDeactive from list to allow archiving if deactivated versions exist
                                         OR (condition = STB$TYPEDEF.cnStatusLocked
                                         AND NVL (locksession, STB$SECURITY.getCurrentSession) != STB$SECURITY.getCurrentSession))) LOOP
            sMenuAllowed := 'F';
            isr$trace.stat('end', 'sParameter: '||sparameter||'  sMenuAllowed: '||sMenuAllowed  , sCurrentName);
            RETURN oErrorObj;
          END LOOP;

          isr$trace.debug ('look for archived reports', 'look for locked reports', sCurrentName);
          FOR rGetArchivedReports IN (SELECT count ( * )
                                        FROM (SELECT repid
                                                FROM STB$REPORT
                                               WHERE (srnumber = STB$OBJECT.getCurrentSRNumber
                                                   OR title = STB$OBJECT.getCurrentTitle)
                                              MINUS
                                              SELECT repid
                                                FROM STB$REPORT
                                               WHERE (srnumber = STB$OBJECT.getCurrentSRNumber
                                                   OR title = STB$OBJECT.getCurrentTitle)
                                                 AND parentrepid NOT IN (SELECT repid FROM stb$report))
                                      HAVING count ( * ) = 0) LOOP
            sMenuAllowed := 'F';
            isr$trace.stat('end', 'sParameter: '||sparameter||'  sMenuAllowed: '||sMenuAllowed  , sCurrentName);
            RETURN oErrorObj;
          END LOOP;

      --=====CAN_CREATE_PDF
      WHEN sparameter = 'CAN_CREATE_PDF' THEN

          BEGIN
            SELECT 'T'
              INTO sExists
              FROM stb$doctrail d1, stb$doctrail d2
             WHERE d1.doc_definition = 'document_pdf'
               AND d1.nodeid = d2.nodeid
               AND d1.nodetype = d2.nodetype
               AND d2.docid = STB$OBJECT.getCurrentDocid;
          EXCEPTION WHEN OTHERS THEN
            sExists := 'F';
          END;

          sMenuAllowed := case when sExists = 'T' then 'F' else stb$util.getsystemparameter ('PDF_CREATION_ALLOWED') end;

      --=====CAN_MERGE_COMMON
      WHEN sparameter = 'CAN_MERGE_COMMON' THEN

          BEGIN
            SELECT DISTINCT 'T'
              INTO sExists
              FROM stb$doctrail d1, stb$doctrail d2
             WHERE d1.doc_definition = 'pdf_appendix'
               AND d1.nodeid = d2.nodeid
               AND d1.nodetype = d2.nodetype
               AND d2.docid = STB$OBJECT.getCurrentDocid;
          EXCEPTION WHEN OTHERS THEN
            sExists := 'F';
          END;

          sMenuAllowed := case
                            when NVL(STB$DOCKS.getDocDefType(STB$OBJECT.getCurrentDocid, Stb$object.getCurrentRepId), 'NOT_DETERMINED') != 'document_pdf' OR sExists = 'F' then 'F'
                            else stb$util.getsystemparameter ('PDF_CREATION_ALLOWED')
                          end;

      ELSE

        sMenuAllowed :=  'T';

    END CASE;
  END IF;

  isr$trace.stat('end','sMenuAllowed: '||sMenuAllowed, sCurrentName);
  return oErrorObj;
EXCEPTION
  WHEN OTHERS THEN
    oErrorObj.handleError ( Stb$typedef.cnSeverityCritical,  utd$msglib.getmsg ('ERROR', stb$security.getCurrentLanguage) || SQLCODE, sCurrentName,
       dbms_utility.format_error_stack ||crlf|| dbms_utility.format_error_backtrace);

    RETURN oErrorObj;
END setMenuAccess;

--******************************************************************************
FUNCTION setInspectionStatus
  RETURN stb$oerror$record
IS
  sCurrentName                  CONSTANT VARCHAR2(100) := $$PLSQL_UNIT||'.setInspectionStatus';
  oErrorObj                     stb$oerror$record := STB$OERROR$RECORD();
  nNewRepid                     stb$report.REPID%TYPE;
  nOldRepid                     stb$report.REPID%TYPE;
  nParentRepid                  stb$report.REPID%TYPE;
  sVersion                      stb$report.VERSION%TYPE;
  nChecksumReport               stb$report.CHECKSUM%TYPE;
  sPassword                     stb$report.PASSWORD%TYPE;
  nVersionCounter               NUMBER;

  CURSOR cGetReportValues (cnoldrepid IN NUMBER) IS
    SELECT *
      FROM stb$report
     WHERE repid = cnoldrepid;

  rGetReportValues                cGetReportValues%ROWTYPE;

  CURSOR cgethighestversion (cnrepid IN NUMBER) IS
    SELECT MAX (TO_NUMBER (SUBSTR (VERSION, (INSTR (VERSION, '.')) + 1)) + 1)
              VERSION
      FROM stb$report
    START WITH repid = cnrepid
    CONNECT BY parentrepid = PRIOR repid;

BEGIN
  isr$trace.stat('begin','begin', sCurrentName);

  -- save old repid
  nParentRepid := stb$object.getCurrentRepid;
  nOldRepid := stb$object.getCurrentRepid;

  -- get the new report id
  SELECT seq$stb$reports.NEXTVAL
  INTO   nnewrepid
  FROM   DUAL;
  isr$trace.debug ('nNewRepId',  'nNewRepId: ' || nnewrepid, sCurrentName);

  -- get the values of the parent report
  OPEN cGetReportValues (nOldRepid);
  FETCH cGetReportValues INTO rGetReportValues;
  CLOSE cGetReportValues;

  -- build up the new version
  OPEN cGetHighestVersion (nParentRepid);
  FETCH cGetHighestVersion INTO nVersionCounter;
  CLOSE cGetHighestVersion;
  isr$trace.debug ('nVersionCounter',  'nVersionCounter: ' || nVersionCounter, sCurrentName);

  sVersion := substr (rGetReportValues.VERSION, 1, (instr (rGetReportValues.VERSION, '.', 1)));
  isr$trace.debug ('sVersion',  'sVersion: ' || sVersion, sCurrentName);

  sVersion := sVersion || nVersionCounter;
  isr$trace.debug ('sVersion',  'sVersion: ' || sVersion, sCurrentName);

  --  create password
  oErrorObj := createpassword (nnewrepid, spassword);

  -- create new report
  INSERT INTO stb$report (repid
                        , status
                        , title
                        , createdon
                        , createdby
                        , reporttypeid
                        , parentrepid
                        , datagroup
                        , remarks
                        , oldstatus
                        , VERSION
                        , srnumber
                        , PASSWORD
                        , checksum
                        , modifiedon
                        , modifiedby
                        , condition
                        , lockedby
                        , locksession
                        , lockedon
                        , template)
  VALUES (nnewrepid
        , stb$typedef.cnStatusInspection
        , rgetreportvalues.title
        , SYSDATE
        , sUsername
        , rgetreportvalues.reporttypeid
        , noldrepid
        , rgetreportvalues.datagroup
        , rgetreportvalues.remarks
        , rgetreportvalues.status
        , sversion
        , rgetreportvalues.srnumber
        , spassword
        , nchecksumreport
        , SYSDATE
        , sUsername
        , stb$typedef.cnStatusactivated
        , rgetreportvalues.lockedby
        , rgetreportvalues.locksession
        , rgetreportvalues.lockedon
        , rgetreportvalues.template);

  isr$trace.debug ('after stb$report', 'after stb$report', sCurrentName);

  oErrorObj := copyreportparameter (nnewrepid);

  EXECUTE immediate
    'BEGIN
       :1 := '|| STB$UTIL.GetCurrentCustomPackage ||'.changeDataForInspectionStatus(:2);
     END;'
  using OUT oErrorObj, IN nnewrepid;

  -- checksum
  oErrorObj := ISR$REPORTTYPE$PACKAGE.updateCheckSum (nNewRepid);

  -- setting of the new report id
  stb$object.setcurrentrepid (nNewRepid);

  -- copy the common docs
  -- if the flag is set copy the common documents
  IF nvl(stb$util.getReptypeParameter (stb$object.getcurrentreporttypeid, 'COPY_COMMON_DOCS'), 'F') = 'T' THEN
    oErrorObj := stb$docks.copycommondocs (nParentRepid, nnewrepid);
  END IF;

  isr$trace.stat('end','end', sCurrentName);
  RETURN oErrorObj;
EXCEPTION
  WHEN OTHERS THEN
     oErrorObj.handleError ( Stb$typedef.cnSeverityCritical, utd$msglib.getmsg ('exUnexpectedError', stb$security.getCurrentLanguage,  csP1 => sqlerrm(sqlcode)),
        sCurrentName, dbms_utility.format_error_stack ||chr(10)|| dbms_utility.format_error_backtrace );
    RETURN oErrorObj;
END setinspectionstatus;

--******************************************************************************
FUNCTION setFinalStatus
  RETURN stb$oerror$record
IS
  sCurrentName                  CONSTANT VARCHAR2(100) := $$PLSQL_UNIT||'.setFinalStatus';
  oErrorObj                     stb$oerror$record := STB$OERROR$RECORD();
  nNewRepid                     stb$report.REPID%TYPE;
  nOldRepid                     stb$report.REPID%TYPE;
  nParentRepid                  stb$report.REPID%TYPE;
  sVersion                      stb$report.VERSION%TYPE;
  nChecksumReport               stb$report.CHECKSUM%TYPE;
  sPassword                     stb$report.PASSWORD%TYPE;
  nVersionCounter               NUMBER;

  CURSOR cGetReportValues (cnoldrepid IN NUMBER) IS
    SELECT *
      FROM stb$report
     WHERE repid = cnoldrepid;

  rGetReportValues                cGetReportValues%ROWTYPE;

BEGIN
  isr$trace.stat('begin','begin', sCurrentName);

  -- save old repid
  nParentRepid := stb$object.getCurrentRepid;
  nOldRepid := stb$object.getCurrentRepid;

  -- get the new report id
  SELECT seq$stb$reports.NEXTVAL
  INTO   nNewRepid
  FROM   DUAL;
  isr$trace.debug ('nNewRepid',  'nNewRepid: ' || nNewRepid, sCurrentName);

  -- get the values of the parent report
  OPEN cGetReportValues (nOldRepid);
  FETCH cGetReportValues INTO rGetReportValues;
  CLOSE cGetReportValues;

  -- build up the new version
  sVersion := rGetReportValues.version;
  nVersionCounter := to_number (substr (sVersion, 1, instr (sVersion, '.') - 1));
  nVersionCounter := nVersionCounter + 1;
  sVersion := to_char (nVersionCounter) || '.0';
  isr$trace.debug ('sVersion',  'sVersion: ' || sVersion, sCurrentName);

  --  create password
  oErrorObj := ISR$REPORTTYPE$PACKAGE.createpassword (nNewRepid, sPassword);

  -- create new report
  INSERT INTO stb$report (repid
                        , status
                        , oldstatus
                        , createdon
                        , createdby
                        , reporttypeid
                        , parentrepid
                        , datagroup
                        , remarks
                        , title
                        , srnumber
                        , VERSION
                        , lastFinalVersion
                        , lockedby
                        , condition
                        , locksession
                        , lockedon
                        , PASSWORD
                        , modifiedby
                        , modifiedon
                        , checksum
                        , template)
  VALUES (nNewRepid
        , stb$typedef.cnStatusfinal
        , stb$typedef.cnStatusInspection
        , SYSDATE
        , sUsername
        , rGetReportValues.reporttypeid
        , nOldRepid
        , rGetReportValues.datagroup
        , rGetReportValues.remarks
        , rGetReportValues.title
        , rGetReportValues.srnumber
        , sVersion
        , nOldRepid
        , rGetReportValues.lockedby
        , stb$typedef.cnStatusactivated
        , rGetReportValues.locksession
        , rGetReportValues.lockedon
        , sPassword
        , sUsername
        , SYSDATE
        , nChecksumReport
        , rGetReportValues.template);

  isr$trace.debug ('after stb$report',  'after stb$report', sCurrentName);

  oErrorObj := copyreportparameter (nnewrepid);

  -- checksum
  oErrorObj := updateCheckSum (nnewrepid);

  -- set the new Report id
  stb$object.setcurrentrepid (nnewrepid);
  isr$trace.debug ('before copy common',  'before copy common', sCurrentName);
  -- copy the common docs
  -- if the flag is set copy the common documents
  IF nvl(stb$util.getReptypeParameter (stb$object.getCurrentReporttypeid, 'COPY_COMMON_DOCS'), 'F') = 'T' THEN
    oErrorObj := stb$docks.copycommondocs (nParentRepid, nnewrepid);
  END IF;
  isr$trace.stat('end','end', sCurrentName);
  RETURN oErrorObj;
EXCEPTION
  WHEN OTHERS THEN
     oErrorObj.handleError ( Stb$typedef.cnSeverityCritical, utd$msglib.getmsg ('exUnexpectedError', stb$security.getCurrentLanguage,  csP1 => sqlerrm(sqlcode)),
        sCurrentName, dbms_utility.format_error_stack ||chr(10)|| dbms_utility.format_error_backtrace );
    RETURN oErrorObj;
END setfinalstatus;

--******************************************************************************
FUNCTION createSrAudit (ssrnumber IN VARCHAR2, sReason IN VARCHAR2 DEFAULT NULL)
  RETURN stb$oerror$record
IS
  sCurrentName                  CONSTANT VARCHAR2(100) := $$PLSQL_UNIT||'.createSrAudit';
  oErrorObj                     stb$oerror$record := STB$OERROR$RECORD();
  nrepid                        NUMBER;

  CURSOR cGetReportInformation IS
    SELECT *
    FROM   stb$report
    WHERE  repid = nrepid;

  rGetReportInformation cGetReportInformation%rowtype;

  nDocid                        NUMBER;

  CURSOR cGetDocInformation IS
    SELECT *
    FROM   stb$doctrail
    WHERE  docid = nDocid;

  rGetDocInformation cGetDocInformation%rowtype;

  nJobId            NUMBER;

  CURSOR cGetJobID IS
    SELECT jobid
      FROM STB$JOBSESSION
     WHERE oraclejobsessionid = STB$SECURITY.getCurrentSession;

  xAuditXml             XMLTYPE;
BEGIN
  isr$trace.stat('begin','begin', sCurrentName);

  nrepid := stb$object.getCurrentRepid;
  isr$trace.debug ('nRepId',  nrepid, sCurrentName);

  -- create Frame
  OPEN cGetReportInformation;
  FETCH cGetReportInformation INTO rGetReportInformation;
  CLOSE cGetReportInformation;

  isr$trace.debug ('rGetReportInformation.title',  rGetReportInformation.title, sCurrentName);

  -- open Audit
  oErrorObj := stb$audit.openaudit ('SRAUDIT', rGetReportInformation.title);


  oErrorObj := stb$audit.addAuditRecord (STB$OBJECT.getCurrentToken);

  -- create Entry
  IF STB$OBJECT.getCurrentDocid IS NOT NULL THEN
    nDocid := STB$OBJECT.getCurrentDocid;
    OPEN cGetDocInformation;
    FETCH cGetDocInformation INTO rGetDocInformation;
    CLOSE cGetDocInformation;
    OPEN cGetJobID;
    FETCH cGetJobID INTO nJobId;
    CLOSE cGetJobID;
    oErrorObj := stb$audit.addVersionToSRAudit (rGetDocInformation.versiondisplayed, NVL(rGetDocInformation.filename, STB$JOB.getJobParameter('FILENAME', nJobId)));
  ELSE
    oErrorObj := stb$audit.addVersionToSRAudit (rGetReportInformation.version, rGetReportInformation.title);
  END IF;

  IF TRIM(sReason) IS NOT NULL THEN
    xAuditXml := STB$AUDIT.getAuditDomId;
    ISR$XML.CreateNode(xAuditXml, '/AUDITFILE/AUDIT[@NUMBER='||ISR$XML.valueOf(xAuditXml, 'count(/AUDITFILE/AUDIT)')|| ']', 'REASON', sReason);
    STB$AUDIT.setCurrentXml(xAuditXml);
  END IF;

  -- close dom and write a clob into the audittrail
  oErrorObj := stb$audit.domtoaudit;

  isr$trace.stat('end','end', sCurrentName);
  RETURN oErrorObj;
EXCEPTION
  WHEN OTHERS THEN
     oErrorObj.handleError ( Stb$typedef.cnSeverityCritical, utd$msglib.getmsg ('exUnexpectedError', stb$security.getCurrentLanguage,  csP1 => sqlerrm(sqlcode)),
        sCurrentName, dbms_utility.format_error_stack ||chr(10)|| dbms_utility.format_error_backtrace );
    RETURN oErrorObj;
END createsraudit;

--******************************************************************************
FUNCTION getSaveDialog (olsavelist OUT stb$treenodelist)
  RETURN stb$oerror$record
IS
  sCurrentName                  CONSTANT VARCHAR2(100) := $$PLSQL_UNIT||'.getSaveDialog';
  oErrorObj                     stb$oerror$record := STB$OERROR$RECORD();

  cursor cGetReportData is
    select r.datagroup, r.srnumber, r.title, (select status from stb$report where repid = r.parentrepid)
      from stb$report r
     where r.repid = STB$OBJECT.getCurrentRepid;

  nDatagroup  STB$REPORT.DATAGROUP%TYPE;
  sSrnumber   STB$REPORT.SRNUMBER%TYPE;
  sTitle      STB$REPORT.TITLE%TYPE;
  nRepStatus  STB$REPORT.STATUS%TYPE;
BEGIN
  isr$trace.stat('begin','begin', sCurrentName);

  olsavelist := stb$treenodelist ();
  olsavelist.EXTEND;
  olsavelist (1) := STB$TREENODE$RECORD(Utd$msglib.GetMsg ('Save', Stb$security.getCurrentLanguage),
                                          'ISR$REPORTTYPE$PACKAGE', 'TRANSPORT', 'MASK');
  oErrorObj := STB$UTIL.getPropertyList('SAVE_DIALOG_'||to_char(STB$OBJECT.getCurrentReporttypeId), olsavelist (1).tloPropertyList);

  OPEN cGetReportData;
  FETCH cGetReportData INTO nDatagroup, sSrnumber, sTitle, nRepStatus;
  CLOSE cGetReportData;

  IF nvl(STB$UTIL.getReptypeParameter(STB$OBJECT.getCurrentReporttypeId, 'REQUIRES_VERSIONING'), 'F') = 'T' THEN
    FOR i IN 1..olsavelist (1).tloPropertyList.count() LOOP
      IF sSrnumber IS NOT NULL THEN
        IF olsavelist (1).tloPropertyList (i).sParameter = 'SRNUMBER' THEN
          olsavelist (1).tloPropertyList (i).sEditable := 'F';
        END IF;
        IF nDatagroup IS NOT NULL
        AND olsavelist (1).tloPropertyList (i).sParameter = 'DATAGROUP' THEN
          olsavelist (1).tloPropertyList (i).sEditable := 'F';
        END IF;
      END IF;
      IF sTitle IS NOT NULL
      AND olsavelist (1).tloPropertyList (i).sParameter = 'TITLE'
      /*AND not(    nvl(STB$UTIL.getReptypeParameter(STB$OBJECT.getCurrentReporttypeId, 'ENTER_TITLE_FOR_NEW_VERSION'), 'F') = 'T'
              AND nRepStatus = STB$TYPEDEF.cnStatusFinal)*/ THEN
        olsavelist (1).tloPropertyList (i).sEditable := 'F';
      END IF;
    END LOOP;
  END IF;

  isr$trace.debug('olsavelist','s. logclob', sCurrentName, olsavelist);
  isr$trace.stat('end','end', sCurrentName);
  RETURN oErrorObj;
EXCEPTION
  WHEN OTHERS THEN
     oErrorObj.handleError ( Stb$typedef.cnSeverityCritical, utd$msglib.getmsg ('exUnexpectedError', stb$security.getCurrentLanguage,  csP1 => sqlerrm(sqlcode)),
        sCurrentName, dbms_utility.format_error_stack ||chr(10)|| dbms_utility.format_error_backtrace );
    RETURN oErrorObj;
END getsavedialog;

--******************************************************************************
FUNCTION putSaveDialog (olsavelist IN stb$property$list)
  RETURN stb$oerror$record
IS
  sCurrentName                  CONSTANT VARCHAR2(100) := $$PLSQL_UNIT||'.putSaveDialog('||null||')';
  oErrorObj                     stb$oerror$record := STB$OERROR$RECORD();
  oErrorObj1                    stb$oerror$record := STB$OERROR$RECORD();
  sUserMsg         VARCHAR2(4000);
  oNewNode         STB$TREENODE$RECORD;
BEGIN
  isr$trace.stat('begin','begin', sCurrentName);

  begin
      STB$OBJECT.setCurrentSRNumber(STB$UTIL.getProperty(olSaveList, 'SRNUMBER'));
  exception
   when others then
     oErrorObj.handleError( STB$TYPEDEF.cnSeverityWarning, utd$msglib.getmsg('exSrnumberTooLong', nCurrentLanguage), sCurrentName,
         substr(dbms_utility.format_error_stack || CHR(10) || dbms_utility.format_error_backtrace, 1,4000));
     return oErrorObj;
  end;

  begin
     STB$OBJECT.setCurrentTitle(STB$UTIL.getProperty(olSaveList, 'TITLE'));
  exception
   when others then
     oErrorObj.handleError( STB$TYPEDEF.cnSeverityWarning, utd$msglib.getmsg('exTitleTooLong', nCurrentLanguage), sCurrentName,
         substr(dbms_utility.format_error_stack || CHR(10) || dbms_utility.format_error_backtrace, 1,4000));
     return oErrorObj;
  end;

  EXECUTE IMMEDIATE 'BEGIN
                       :1 :=' || STB$UTIL.getReptypeParameter(STB$OBJECT.getCurrentReporttypeId, 'PUTSAVEDIALOG') || '(:2,:3);
                     END;'
  using OUT oErrorObj, IN olsavelist, IN STB$OBJECT.getCurrentToken;

  IF oErrorObj.sSeverityCode != stb$typedef.cnNoError THEN
    RETURN oErrorObj;
  END IF;

  sessionUpdate;

  -- MCD, 16.Dec 2008, do not search the node for UNITTEST because there is none
  IF STB$OBJECT.getCurrentREPORTTYPEID != 999 THEN
    if STB$OBJECT.getCurrentToken in ('CAN_CREATE_REPORT_VARIANT', 'CAN_CREATE_NEW_REP', 'CAN_CREATE_NEW_REPORT') then
      oErrorObj := isr$reporttype$package.createsraudit (stb$object.getcurrentsrnumber);
    end if;
    if STB$OBJECT.getCurrentToken in ('CAN_CREATE_REPORT_VARIANT', 'CAN_CREATE_REPORT_VERSION', 'CAN_MODIFY_REPORT') then
      oNewNode := ISR$TREE$PACKAGE.rebuildReportNode(STB$OBJECT.getCurrentRepid, STB$OBJECT.getCurrentNode);
    else
      oNewNode := ISR$TREE$PACKAGE.buildReportNode(STB$OBJECT.getCurrentRepid, null, ISR$TREE$PACKAGE.getCustomNodeBranch);
    end if;
    ISR$TREE$PACKAGE.loadNode(oNewNode, STB$OBJECT.getCurrentNode);
    ISR$TREE$PACKAGE.selectLeftNode(oNewNode);
  END IF;

  isr$trace.stat('end','end', sCurrentName);
  RETURN oErrorObj;
EXCEPTION
  WHEN OTHERS THEN
    ROLLBACK;
    sUserMsg := utd$msglib.getmsg ('exUnexpectedError', stb$security.getCurrentLanguage, sqlerrm(sqlcode));
    oErrorObj.handleError ( Stb$typedef.cnSeverityWarning, sUserMsg, sCurrentName,
              dbms_utility.format_error_stack || CHR(10) || dbms_utility.format_error_backtrace);
    RETURN oErrorObj;
END putsavedialog;


--******************************************************************************
FUNCTION validateInspectionData (svalidateflag OUT VARCHAR2)
  RETURN stb$oerror$record
IS
  sCurrentName                  CONSTANT VARCHAR2(100) := $$PLSQL_UNIT||'.validateInspectionData('||null||')';
  oErrorObj                     stb$oerror$record := STB$OERROR$RECORD();
BEGIN
  isr$trace.stat('begin','begin', sCurrentName);
  EXECUTE IMMEDIATE
    'BEGIN
       :1 := '||stb$util.getCurrentCustomPackage||'.validateInspectionData(:2);
     END;'
  using OUT oErrorObj, OUT svalidateflag;

  isr$trace.stat('end','end', sCurrentName);
  RETURN oErrorObj;
EXCEPTION
  WHEN OTHERS THEN
     oErrorObj.handleError ( Stb$typedef.cnSeverityCritical, utd$msglib.getmsg ('exUnexpectedError', stb$security.getCurrentLanguage,  csP1 => sqlerrm(sqlcode)),
        sCurrentName, dbms_utility.format_error_stack ||chr(10)|| dbms_utility.format_error_backtrace );
    RETURN oErrorObj;
END validateinspectiondata;

--******************************************************************************
FUNCTION checkManipulation (smanipulation OUT VARCHAR2)
  RETURN stb$oerror$record
IS
  sCurrentName                  CONSTANT VARCHAR2(100) := $$PLSQL_UNIT||'.checkManipulation('||null||')';
  --Variables
  oErrorObj                     stb$oerror$record := STB$OERROR$RECORD();
  noldrepid                     NUMBER;
  nnewrepid                     NUMBER;
  nchecksum                     NUMBER := 0;
  nauditid                      NUMBER;
  nchecksum1                    NUMBER := 0;
  nchecksumreport               NUMBER := 0;
  noldchecksum                  NUMBER := 0;
  ndocid                        NUMBER := 0;
  sflagok                       VARCHAR2 (1);

  -- Cursor to get the ParentRepId
  CURSOR cgetoldrepid (cnrepid IN NUMBER) IS
    SELECT parentrepid
    FROM   stb$report
    WHERE  repid = cnrepid;

  -- Cursor to get the checksum of the audit file
  CURSOR cgetaudit (cnrepid NUMBER) IS
    SELECT checksum, auditid
    FROM   stb$audittrail aud
    WHERE  aud.REFERENCE = to_char (cnrepid) AND aud.audittype = 'REPORTAUDIT';

  CURSOR cgetchecksum (cnrepid IN NUMBER) IS
    SELECT checksum
    FROM   stb$report
    WHERE  repid = cnrepid;
BEGIN
  isr$trace.stat('begin','begin', sCurrentName);
  smanipulation := 'F';
  nnewrepid := stb$object.getCurrentRepid;

  IF nnewrepid IS NULL THEN
    -- no report node
    RETURN oErrorObj;
  END IF;

  isr$trace.debug ('nNewRepId', 'nNewRepId: ' || nnewrepid, sCurrentName);
  OPEN cgetoldrepid (nnewrepid);
  FETCH cgetoldrepid
  INTO  noldrepid;
  CLOSE cgetoldrepid;
  isr$trace.debug ('nOldRepId', 'nOldRepId: ' || noldrepid, sCurrentName);

  IF noldrepid IS NOT NULL THEN
    -- first check
    -- check if there is entry in the audittrail
    -- if so compare the checksums
    OPEN cgetaudit (noldrepid);
    FETCH cgetaudit
    INTO  nchecksum, nauditid;

    IF cgetaudit%NOTFOUND THEN
      isr$trace.debug ('kein Auditeintrag', 'kein Auditeintrag ' || noldrepid, sCurrentName);
    END IF;

    CLOSE cgetaudit;
    isr$trace.debug ('kein Auditeintrag', 'nAuditId: ' || nauditid || '   nCheckSum: ' || nchecksum, sCurrentName);

    IF nauditid IS NOT NULL THEN
      oErrorObj := stb$audit.createchecksum (nauditid, nchecksum1);
      isr$trace.debug ('nCheckSum1', 'nCheckSum1 ' || nchecksum1, sCurrentName);

      IF (to_char (nchecksum1) = to_char (nchecksum)) THEN
        smanipulation := 'F';
      ELSE
        smanipulation := 'T';
        RETURN oErrorObj;
      END IF;
    END IF;
  END IF;

  IF smanipulation = 'F' AND oErrorObj.sSeverityCode = stb$typedef.cnNoError THEN
    -- second check
    -- check if the checksum of report paremnt version is the same as the compare lob over the criteria of the previous version
    isr$trace.debug ('nNewRepID', 'nNewRepID: ' || nnewrepid, sCurrentName);
    --get the old checksum
    OPEN cgetchecksum (nnewrepid);
    FETCH cgetchecksum INTO  noldchecksum;
    IF cgetchecksum%NOTFOUND THEN
      isr$trace.debug ('checksum%notfound', 'checksum%notfound for repid:' || nnewrepid, sCurrentName);
    END IF;
    CLOSE cgetchecksum;
    isr$trace.debug('nOldCheckSum', 'nOldCheckSum: ' || noldchecksum, sCurrentName);

    nchecksumreport := createReportSum (nnewrepid);
    isr$trace.debug ('nCheckSumReport',  'nCheckSumReport: ' || nchecksumreport, sCurrentName);

    IF (to_char (noldchecksum) = to_char (nchecksumreport)) THEN
      smanipulation := 'F';
    ELSE
      smanipulation := 'T';
      RETURN oErrorObj;
    END IF;
  END IF;

  -- third check
  -- check if there have been manipulations on an exsting doc (max docid within srnumber)
  IF smanipulation = 'F' AND oErrorObj.sSeverityCode = stb$typedef.cnNoError THEN
    -- get the maximum doc id
    oErrorObj := stb$docks.getmaxdocidinsrnumber (nnewrepid, ndocid, STB$DOCKS.getDocType(NULL, nnewrepid),STB$DOCKS.getDocDefType(NULL, nnewrepid));
    isr$trace.debug ('nDocId', 'nDocId: ' || ndocid, sCurrentName);

    IF ndocid != 0 AND ndocid IS NOT NULL THEN
      --check if there have been manipulations
      stb$docks.controlchecksum (ndocid, sflagok);
      isr$trace.debug ('sFlagOk', 'sFlagOk: ' || sflagok, sCurrentName);

      IF sflagok = 'T' THEN
        smanipulation := 'F';
      ELSE
        smanipulation := 'T';
        RETURN oErrorObj;
      END IF;
    END IF;
  END IF;

  isr$trace.stat('end','end', sCurrentName);
  RETURN oErrorObj;
EXCEPTION
  WHEN OTHERS THEN
     oErrorObj.handleError ( Stb$typedef.cnSeverityCritical, utd$msglib.getmsg ('exUnexpectedError', stb$security.getCurrentLanguage,  csP1 => sqlerrm(sqlcode)),
        sCurrentName, dbms_utility.format_error_stack ||chr(10)|| dbms_utility.format_error_backtrace );
    RETURN oErrorObj;
END checkmanipulation;

--******************************************************************************
FUNCTION showCheckinComment (olpopertitylist OUT stb$property$list)
  RETURN stb$oerror$record
IS
  sCurrentName                  CONSTANT VARCHAR2(100) := $$PLSQL_UNIT||'.showCheckinComment('||null||')';
  oErrorObj                     stb$oerror$record := STB$OERROR$RECORD();
  sstatement                    VARCHAR2 (4000);
BEGIN
  isr$trace.stat('begin','begin', sCurrentName);
  sstatement :=
       ' select to_char(d.docid) docid,'
    || ' d.modifiedon, '
    || ' d.MODIFIEDBY, '
    || ' d.CHECKIN_COMMENT '
    || ' from STB$DOCTRAIL  d '
    || ' where d.docid = '
    || stb$object.getcurrentdocid;
  -- call of the general function to turn a select statement into an object list
  oErrorObj := stb$util.getcolpropertylist (sstatement, olpopertitylist);
  isr$trace.stat('end','end', sCurrentName);
  RETURN oErrorObj;
EXCEPTION
  WHEN OTHERS THEN
    oErrorObj.handleError ( Stb$typedef.cnSeverityWarning, utd$msglib.getmsg ('exUnexpectedError', stb$security.getCurrentLanguage,  csP1 => sqlerrm(sqlcode)),
        sCurrentName, dbms_utility.format_error_stack ||chr(10)|| dbms_utility.format_error_backtrace );
    RETURN oErrorObj;
END showcheckincomment;

--******************************************************************************
FUNCTION showHeadData (olpopertitylist OUT stb$property$list)
  RETURN stb$oerror$record
IS
  sCurrentName                  CONSTANT VARCHAR2(100) := $$PLSQL_UNIT||'.showHeadData('||null||')';
  oErrorObj                     stb$oerror$record := STB$OERROR$RECORD();
  sstatement                    VARCHAR2 (4000);
BEGIN
  isr$trace.stat('begin','begin', sCurrentName);
  sstatement :=
       ' select '
    || ' UTD$MSGLIB.getMsg (rt.REPORTTYPENAME, stb$security.getCurrentLanguage) REPORTTYPENAME, '
    || ' r.SRNUMBER, '
    || ' r.TITLE, '
    || ' (select t.templatename||'' ''||t.version from isr$template t where t.fileid = r.template) TEMPLATE, '
    || ' (select listagg (alias || '' ('' || serverid || '')'', '', '') WITHIN GROUP (ORDER BY ordernumber) from isr$server, (select * from isr$crit where repid = '||stb$object.getCurrentRepid||' and entity = ''SERVER'') where to_char(serverid) = key) SERVER, '
    || ' to_char(r.repid) repid,'
    || ' r.VERSION, '
    || ' to_char(DECODE(r.STATUS, '
    || stb$typedef.cnStatusdraft
    || ' ,''Draft'''
    || ' , '
    || stb$typedef.cnStatusInspection
    || ' ,''Inspection'''
    || ' , '
    || stb$typedef.cnStatusfinal
    || ',''Final'''
    || ',''No Status'' )) repstatus,'
    || ' ug.groupname datagroup, '
    || ' r.REMARKS, '
    || ' to_char(r.PARENTREPID) parentrepid, '
    || ' r.modifiedBY, '
    || ' r.modifiedON, '
    || ' u.Fullname lockedby, '
    || ' r.LOCKEDON, '
    || ' to_char(r.LOCKSESSION) locksession '
    || ' from STB$REPORT  r, '
    || ' STB$REPORTTYPE rt, '
    || ' STB$USER    u, '
    || ' STB$USERGROUP ug '
    || ' where r.repid = '
    || stb$object.getCurrentRepid
    || ' and r.reporttypeid =rt.reporttypeid '
    || ' and r.lockedby = u.userno(+) '
    || ' and ug.usergroupno =r.datagroup';
  isr$trace.debug (' sStatement',  sstatement, sCurrentName);
  -- call of the general function to turn a select statement into an object list
  oErrorObj := stb$util.getcolpropertylist (sstatement, olpopertitylist);
  isr$trace.stat('end','end', sCurrentName);
  RETURN oErrorObj;
EXCEPTION
  WHEN OTHERS THEN
     oErrorObj.handleError ( Stb$typedef.cnSeverityWarning, utd$msglib.getmsg ('exUnexpectedError', stb$security.getCurrentLanguage,  csP1 => sqlerrm(sqlcode)),
        sCurrentName, dbms_utility.format_error_stack ||chr(10)|| dbms_utility.format_error_backtrace );
    RETURN oErrorObj;
END showheaddata;

--******************************************************************************
FUNCTION moveParametersList (olpopertylist IN OUT stb$property$list)
  RETURN stb$oerror$record
IS
  sCurrentName                  CONSTANT VARCHAR2(100) := $$PLSQL_UNIT||'.moveParametersList('||null||')';
  oErrorObj                     stb$oerror$record := STB$OERROR$RECORD();
  
  CURSOR cGetWordExcelType IS
    SELECT 'T'
      FROM stb$reptypeparameter
     WHERE userparameter = 'F'
       AND reporttypeid = stb$object.getCurrentReporttypeid
       AND repparameter in ('W','A')
       AND visible = 'T'
    ORDER BY orderno;  

  CURSOR cGetParameters(csOption in varchar2 default 'standard') IS
    SELECT parametername
         , description
         , editable
         , repparameter
         , CASE WHEN STB$SECURITY.checkGroupRight (parametername, reporttypeid) = 'T' THEN VISIBLE ELSE 'F' END visible
         , parametertype
      FROM stb$reptypeparameter
     WHERE userparameter = 'F'
       AND reporttypeid = stb$object.getCurrentReporttypeid
       /*AND (repparameter IN ('T','F') and csOption = 'standard'
        or repparameter IN ('T','A','F') and csOption = 'excel'
        or repparameter IN ('W','A','F') and csOption = 'word')*/
       AND (repparameter is not null or parametertype in ('TABSHEET', 'GROUP'))
       AND visible = 'T'
    ORDER BY orderno;
    
  sSeparateTabs VARCHAR2(1) := 'F';   
   

BEGIN
  isr$trace.stat('begin','begin', sCurrentName);
  
  open cGetWordExcelType;
  fetch cGetWordExcelType into sSeparateTabs;
  if cGetWordExcelType%notfound then
    sSeparateTabs := 'F';
  end if;
  close cGetWordExcelType;
  
  isr$trace.debug('sSeparateTabs',sSeparateTabs, sCurrentName);
  
  if sSeparateTabs = 'T' then
  
    olpopertylist.EXTEND;
    olpopertylist (olpopertylist.COUNT ()) := STB$PROPERTY$RECORD('OUTPUTOPTION$EXCEL$TAB', Utd$msglib.GetMsg ('OUTPUTOPTION$EXCEL$TAB', Stb$security.getCurrentLanguage),
                                                           null, null, null, null, 'T', null, 'TABSHEET');
  
    FOR rGetParameters IN cGetParameters('excel') LOOP
      olpopertylist.EXTEND;
      olpopertylist (olpopertylist.count) := STB$PROPERTY$RECORD('EXCEL_'||rGetParameters.parametername);
      IF instr (rGetParameters.description, '(') - 1 != -1 THEN
        olpopertylist (olpopertylist.count).sPromptDisplay := utd$msglib.getmsg (trim (substr (rGetParameters.description, 1, instr (rGetParameters.description, '(') - 1)), stb$security.getCurrentLanguage);
      ELSE
        olpopertylist (olpopertylist.count).sPromptDisplay := utd$msglib.getmsg (rGetParameters.description, stb$security.getCurrentLanguage);
      END IF;
      olpopertylist (olpopertylist.count).svalue := case when rGetParameters.repparameter in ('T', 'A') then 'T' else 'F' end;
      olpopertylist (olpopertylist.count).sdisplay := olpopertylist (olpopertylist.count).svalue;
      olpopertylist (olpopertylist.count).seditable := rGetParameters.editable;
      olpopertylist (olpopertylist.count).srequired := 'T';
      olpopertylist (olpopertylist.count).sdisplayed := rGetParameters.visible;
      olpopertylist (olpopertylist.count).haslov := 'F';
      olpopertylist (olpopertylist.count).TYPE := case when rGetParameters.parametertype in ('TABSHEET', 'GROUP') then rGetParameters.parametertype else 'BOOLEAN' end;
    END LOOP;  
  
    olpopertylist.EXTEND;
    olpopertylist (olpopertylist.COUNT ()) := STB$PROPERTY$RECORD('OUTPUTOPTION$WORD$TAB', Utd$msglib.GetMsg ('OUTPUTOPTION$WORD$TAB', Stb$security.getCurrentLanguage),
                                                           null, null, null, null, 'T', null, 'TABSHEET');
      
    FOR rGetParameters IN cGetParameters('word') LOOP
      olpopertylist.EXTEND;
      olpopertylist (olpopertylist.count) := STB$PROPERTY$RECORD('WORD_'||rGetParameters.parametername);
      IF instr (rGetParameters.description, '(') - 1 != -1 THEN
        olpopertylist (olpopertylist.count).sPromptDisplay := utd$msglib.getmsg (trim (substr (rGetParameters.description, 1, instr (rGetParameters.description, '(') - 1)), stb$security.getCurrentLanguage);
      ELSE
        olpopertylist (olpopertylist.count).sPromptDisplay := utd$msglib.getmsg (rGetParameters.description, stb$security.getCurrentLanguage);
      END IF;
      olpopertylist (olpopertylist.count).svalue := case when rGetParameters.repparameter in ('W', 'A') then 'T' else 'F' end;
      olpopertylist (olpopertylist.count).sdisplay := olpopertylist (olpopertylist.count).svalue;
      olpopertylist (olpopertylist.count).seditable := rGetParameters.editable;
      olpopertylist (olpopertylist.count).srequired := 'T';
      olpopertylist (olpopertylist.count).sdisplayed := rGetParameters.visible;
      olpopertylist (olpopertylist.count).haslov := 'F';
      olpopertylist (olpopertylist.count).TYPE := case when rGetParameters.parametertype in ('TABSHEET', 'GROUP') then rGetParameters.parametertype else 'BOOLEAN' end;
    END LOOP;
  
  else

    FOR rGetParameters IN cGetParameters LOOP
      olpopertylist.EXTEND;
      olpopertylist (olpopertylist.count) := STB$PROPERTY$RECORD(rGetParameters.parametername);
      IF instr (rGetParameters.description, '(') - 1 != -1 THEN
        olpopertylist (olpopertylist.count).sPromptDisplay := utd$msglib.getmsg (trim (substr (rGetParameters.description, 1, instr (rGetParameters.description, '(') - 1)), stb$security.getCurrentLanguage);
      ELSE
        olpopertylist (olpopertylist.count).sPromptDisplay := utd$msglib.getmsg (rGetParameters.description, stb$security.getCurrentLanguage);
      END IF;
      olpopertylist (olpopertylist.count).svalue := rGetParameters.repparameter;
      olpopertylist (olpopertylist.count).sdisplay := olpopertylist (olpopertylist.count).svalue;
      olpopertylist (olpopertylist.count).seditable := rGetParameters.editable;
      olpopertylist (olpopertylist.count).srequired := 'T';
      olpopertylist (olpopertylist.count).sdisplayed := rGetParameters.visible;
      olpopertylist (olpopertylist.count).haslov := 'F';
      olpopertylist (olpopertylist.count).TYPE := case when rGetParameters.parametertype in ('TABSHEET', 'GROUP') then rGetParameters.parametertype else 'BOOLEAN' end;
    END LOOP;
  
  end if;

  -- store the former values
  ocurrentparameter := olpopertylist;

  isr$trace.stat('end','end', sCurrentName);
  RETURN oErrorObj;
EXCEPTION
  WHEN OTHERS THEN
     oErrorObj.handleError ( Stb$typedef.cnSeverityWarning, utd$msglib.getmsg ('exUnexpectedError', stb$security.getCurrentLanguage,  csP1 => sqlerrm(sqlcode)),
        sCurrentName, dbms_utility.format_error_stack ||chr(10)|| dbms_utility.format_error_backtrace );
    RETURN oErrorObj;
END moveParametersList;

--******************************************************************************
FUNCTION bookmarkList (olpopertylist IN OUT stb$property$list)
  RETURN stb$oerror$record
IS
  sCurrentName                  CONSTANT VARCHAR2(100) := $$PLSQL_UNIT||'.bookmarkList('||null||')';
  oErrorObj                     stb$oerror$record := STB$OERROR$RECORD();
  sFile varchar2(10);

  CURSOR cGetParameters IS
    select executionorder, nvl(value, parametername) parametername from  
      (SELECT distinct min(ot.executionorder) executionorder, ot.bookmarkname parametername
        FROM isr$stylesheet s, isr$report$output$type rot, isr$output$file iof, isr$output$transform ot      
       WHERE s.structured IN ('S')
         AND rot.reporttypeid = STB$OBJECT.getCurrentReporttypeId
         AND rot.outputid = ot.outputid
         AND rot.outputid = iof.outputid
         AND ot.stylesheetid = s.stylesheetid
         AND iof.fileid = sFile
      GROUP BY bookmarkname)    
      full outer join 
      (select distinct value from isr$file$parameter$values where fileid = sFile) pv 
      on parametername=pv.value
    ORDER BY 1, 2;
    
    -- cursor cGetParameters modified and cIsBookmarkSelected removed because of UCIS-263
    
BEGIN
  isr$trace.stat('begin','begin', sCurrentName);

  sFile := STB$OBJECT.getCurrentNodeId;
  isr$trace.debug('Template',sFile,sCurrentName);
  
  olpopertylist.EXTEND;
  olpopertylist (olpopertylist.count) := STB$PROPERTY$RECORD('BOOKMARK', Utd$msglib.GetMsg ('BOOKMARK$DESC', Stb$security.getCurrentLanguage),
                                                             null, null, null, null, 'T', null, 'TABSHEET');

  FOR rGetParameters IN cGetParameters LOOP

    olpopertylist.EXTEND;
    olpopertylist (olpopertylist.count) := STB$PROPERTY$RECORD('BOOKMARK_'||rGetParameters.parametername, rGetParameters.parametername,
                                                               'T', 'T', 'T', 'T', 'T', 'F', 'BOOLEAN');
    -- preselection code removed because of UCIS-263

  END LOOP;

  -- store the former values
  ocurrentparameter := olpopertylist;

  isr$trace.stat('end','end', sCurrentName);
  RETURN oErrorObj;
EXCEPTION
  WHEN OTHERS THEN
     oErrorObj.handleError ( Stb$typedef.cnSeverityWarning, utd$msglib.getmsg ('exUnexpectedError', stb$security.getCurrentLanguage,  csP1 => sqlerrm(sqlcode)),
        sCurrentName, dbms_utility.format_error_stack ||chr(10)|| dbms_utility.format_error_backtrace );
    RETURN oErrorObj;
END bookmarkList;

--******************************************************************************
FUNCTION mergeCommonList (olpopertylist IN OUT stb$property$list)
  RETURN stb$oerror$record
IS
  sCurrentName                  CONSTANT VARCHAR2(100) := $$PLSQL_UNIT||'.mergeCommonList('||null||')';
  oErrorObj                     stb$oerror$record := STB$OERROR$RECORD();
  nCnt                          NUMBER := 0;

  CURSOR cGetCommons IS
    SELECT d1.docid, d1.filename, d1.doc_definition
      FROM stb$doctrail d1, stb$doctrail d2
     WHERE d1.doc_definition in ('pdf_appendix', 'document_pdf')
       AND d1.nodeid = d2.nodeid
       AND d1.nodetype = d2.nodetype
       AND d2.docid = STB$OBJECT.getCurrentDocid
  ORDER BY 3, 2;

BEGIN
  isr$trace.stat('begin','begin', sCurrentName);

  FOR rGetParameters IN cGetCommons LOOP

    nCnt := nCnt + 1;

    olpopertylist.EXTEND;
    olpopertylist (olpopertylist.count) := STB$PROPERTY$RECORD('COMMON_'||rGetParameters.docid, rGetParameters.filename,
                                                               nCnt, nCnt, 'T', case when rGetParameters.doc_definition = 'document_pdf' then 'T' else 'F' end,
                                                               'T','F', 'NUMBER');

    olpopertylist.EXTEND;
    olpopertylist (olpopertylist.count) := STB$PROPERTY$RECORD('HEADLINE_COMMON_'||rGetParameters.docid,
                                                               utd$msglib.getmsg ('HEADLINE_COMMON_', stb$security.getCurrentLanguage) || rGetParameters.filename,
                                                               '', '', 'T', 'F', 'T', 'F', 'LONGSTRING');

    olpopertylist.EXTEND;
    olpopertylist (olpopertylist.count) := STB$PROPERTY$RECORD('HEADLINEPOS_COMMON_'||rGetParameters.docid,
                                                               utd$msglib.getmsg ('HEADLINEPOS_COMMON_', stb$security.getCurrentLanguage) || rGetParameters.filename,
                                                               '', '', 'T', 'F', 'T', 'T', 'NUMBER');
  END LOOP;

  -- store the former values
  ocurrentparameter := olpopertylist;

  isr$trace.stat('end','end', sCurrentName);
  RETURN oErrorObj;
EXCEPTION
  WHEN OTHERS THEN
     oErrorObj.handleError ( Stb$typedef.cnSeverityWarning, utd$msglib.getmsg ('exUnexpectedError', stb$security.getCurrentLanguage,  csP1 => sqlerrm(sqlcode)),
        sCurrentName, dbms_utility.format_error_stack ||chr(10)|| dbms_utility.format_error_backtrace );
    RETURN oErrorObj;
END mergeCommonList;

--******************************************************************************
FUNCTION showReportFiles (olpopertylist IN OUT stb$property$list)
  RETURN stb$oerror$record
IS
  sCurrentName                  CONSTANT VARCHAR2(100) := $$PLSQL_UNIT||'.showReportFiles('||null||')';
  oErrorObj                     stb$oerror$record := STB$OERROR$RECORD();

  CURSOR cgetFiles IS
    SELECT filename, 'F'/*case when lower(filename) like '%.xml' then 'F' else 'T' end*/ isSelected
      FROM isr$report$file
     WHERE repid = STB$OBJECT.getCurrentRepid
       AND (dbms_lob.getlength(binaryfile) != 0 OR dbms_lob.getlength(textfile) != 0)
  ORDER BY SUBSTR(filename, instr(filename, '.',-1)+1), filename;

BEGIN
  isr$trace.stat('begin','begin', sCurrentName);

  FOR rgetFiles IN cgetFiles LOOP
    olpopertylist.EXTEND;
    olpopertylist (olpopertylist.count) := STB$PROPERTY$RECORD(rgetFiles.filename, rgetFiles.filename,
                                                               rgetFiles.isselected, rgetFiles.isselected,
                                                               'T', 'T', 'T', 'F', 'BOOLEAN');
  END LOOP;

  -- store the former values
  ocurrentparameter := olpopertylist;

  isr$trace.stat('end','end', sCurrentName);
  RETURN oErrorObj;
EXCEPTION
  WHEN OTHERS THEN
     oErrorObj.handleError ( Stb$typedef.cnSeverityWarning, utd$msglib.getmsg ('exUnexpectedError', stb$security.getCurrentLanguage,  csP1 => sqlerrm(sqlcode)),
        sCurrentName, dbms_utility.format_error_stack ||chr(10)|| dbms_utility.format_error_backtrace );
    RETURN oErrorObj;
END showreportfiles;

--******************************************************************************
FUNCTION getOffshootConfList (olOffshootList OUT stb$property$list)
  RETURN stb$oerror$record
IS
  sCurrentName                  CONSTANT VARCHAR2(1000) := $$PLSQL_UNIT||'.getOffshootConfList';
  oErrorObj                     stb$oerror$record := STB$OERROR$RECORD();

  CURSOR cgetoffshootlist IS
    SELECT DISTINCT  t.templatename
                   , decode (o.offshoot_to_template, NULL, 'F', 'T') isSelected
      FROM isr$report$output$type rot
         , isr$output$file iof
         , isr$template t
         , stb$reporttype r
         , isr$offshoot o
         , isr$plugin p1
         , isr$plugin p2
     WHERE r.reporttypeid = rot.reporttypeid
       AND rot.reporttypeid IS NOT NULL
       AND rot.VISIBLE = 'T'
       AND rot.outputid = iof.outputid
       AND iof.templatename = t.templatename
       AND iof.fileid = t.fileid
       AND r.offshootlevel <= (SELECT offshootlevel
                                 FROM stb$reporttype
                                WHERE reporttypeid = stb$object.getCurrentReporttypeid)
       AND o.reporttypeid(+) = stb$object.getCurrentReporttypeid
       AND o.offshoot_to_template(+) = t.templatename
       AND p1.plugin = ( SELECT plugin
                           FROM stb$reporttype
                          WHERE reporttypeid = stb$object.getCurrentReporttypeid)
       AND p2.plugin = rot.plugin
       AND p1.parentplugin = p2.parentplugin
    ORDER BY 1;

BEGIN
  isr$trace.stat('begin', 'begin', sCurrentName);
  olOffshootList := stb$property$list ();

  FOR rgetoffshoot IN cgetoffshootlist LOOP
    olOffshootList.EXTEND;
    olOffshootList (olOffshootList.count) := STB$PROPERTY$RECORD(rgetoffshoot.templatename, rgetoffshoot.templatename,
                                                               rgetoffshoot.isselected, rgetoffshoot.isselected,
                                                               'T', 'T', 'T', 'F', 'BOOLEAN');
  END LOOP;

  -- store the former values
  ocurrentparameter := oloffshootlist;

  isr$trace.stat('end','end', sCurrentName);
  RETURN oErrorObj;
EXCEPTION
  WHEN OTHERS THEN
     oErrorObj.handleError ( Stb$typedef.cnSeverityWarning, utd$msglib.getmsg ('exUnexpectedError', stb$security.getCurrentLanguage,  csP1 => sqlerrm(sqlcode)),
        sCurrentName, dbms_utility.format_error_stack ||chr(10)|| dbms_utility.format_error_backtrace );
    RETURN oErrorObj;
END getOffshootConfList;

--******************************************************************************
function getRenderingServerStatus return varchar2
IS
CURSOR cGetRenderingServerStatus IS
    SELECT runstatus
      FROM isr$server
     WHERE servertypeid = 14
     ORDER BY runstatus desc;

  sRSStatus ISR$SERVER.RUNSTATUS%TYPE := 'T';
begin
  OPEN cGetRenderingServerStatus;
  FETCH cGetRenderingServerStatus INTO sRSStatus;
  IF cGetRenderingServerStatus%FOUND AND sRSStatus = 'F' OR cGetRenderingServerStatus%NOTFOUND THEN
    sRSStatus := 'F';
  end if;
  CLOSE cGetRenderingServerStatus;
  return sRSStatus;
end getRenderingServerStatus;

--******************************************************************************
function callFunction( oParameter IN STB$MENUENTRY$RECORD,
                       olNodeList OUT STB$TREENODELIST  )
  return stb$oerror$record
IS
  sCurrentName                  CONSTANT VARCHAR2(100) := $$PLSQL_UNIT||'.callFunction('||oParameter.sToken||')';
  oErrorObj                     stb$oerror$record := STB$OERROR$RECORD();
  exmanipulation                EXCEPTION;
  exNoDocument                  EXCEPTION;
  exnotexists                   EXCEPTION;
  exnoaudit                     EXCEPTION;
  exdoccheckedout               EXCEPTION;
  exfirstform                   EXCEPTION;
  exaudit                       EXCEPTION;
  exnoesig                      EXCEPTION;
  exServerNotRunning            EXCEPTION;
  ocurnode                      stb$treenode$record;
  olparameter                   stb$property$list;
  ndocid                        NUMBER;
  svalidateflag                 VARCHAR2 (1) := 'F';
  smanipulation                 VARCHAR2 (1) := 'F';
  sAuditExists                  VARCHAR2 (1) := 'F';
  scheckedout                   stb$doctrail.checkedout%TYPE;
  spath                         stb$doctrail.path%TYPE;
  smachine                      stb$doctrail.machine%TYPE;
  soracleuser                   stb$doctrail.oracleuser%TYPE;
  -- sreporttypename               stb$reporttype.reporttypename%TYPE;    -- unused!! vs 17.02.2016
  sPackageName                  stb$reptypeparameter.handledby%TYPE;
  sMsg             varchar2(4000);
  sToken            varchar2(4000);

  nGetActiveServers             NUMBER;

  tloPropertylist1              STB$PROPERTY$LIST := STB$PROPERTY$LIST();
  tloPropertylist2              STB$PROPERTY$LIST := STB$PROPERTY$LIST();
  tloPropertylist3              STB$PROPERTY$LIST := STB$PROPERTY$LIST();

  /*  -- unused!! vs 17.02.2016
  CURSOR cgetreporttypename IS
    SELECT UTD$MSGLIB.getMsg (REPORTTYPENAME, stb$security.getCurrentLanguage)
    FROM   stb$reporttype
    WHERE  reporttypeid = stb$object.getCurrentReporttypeid;
  */

  cursor cGetRepidFromDoc is
    SELECT repid
      FROM stb$report, stb$doctrail
     WHERE nodeid = repid
       AND nodetype IN ('REPORT', 'WORKSHEET')
       AND docid = STB$OBJECT.getCurrentDocid;

  nRepidFromDoc STB$REPORT.repid%TYPE;

  CURSOR cGetFile(cnFileId IN NUMBER) IS
    SELECT filename, binaryfile, (SELECT t1.fileid
                                    FROM ISR$TEMPLATE t1, ISR$TEMPLATE t2
                                   WHERE t2.fileid = cnFileId
                                     AND t2.templatename = t1.templatename
                                     AND (t1.templatename, t1.version) IN (SELECT t3.templatename, MAX (t3.version)
                                                                             FROM isr$template t3
                                                                           GROUP BY t3.templatename))
      FROM isr$file
     WHERE fileid = cnFileId;

  nPrevFileId                   ISR$FILE.FILEID%TYPE;
  nFileId                       ISR$FILE.FILEID%TYPE;
  sFilename                     ISR$FILE.FILENAME%TYPE;
  blBinaryfile                  ISR$FILE.BINARYFILE%TYPE;
  nNewFileId                    ISR$FILE.FILEID%TYPE;
  xXml                          XMLTYPE;
  sOrigToken   varchar2(100);

  cursor cNotRunningServer is -- 7.Jun 2019 hr added for [ISRC-1119] ... also exception above
    select servername from
      (select alias || ' (' || serverid || ')' servername, serverid
       from isr$server
       where servertypeid = 2
         and runstatus = 'F') s,
      (select key
       from isr$crit
       where repid = stb$object.getCurrentRepid
         and entity = 'SERVER') repserver
    where to_char(serverid) = key;
  sNotRunning                   varchar2(550);

BEGIN
  isr$trace.stat('begin','begin', sCurrentName);
  isr$trace.debug('value','oParameter',sCurrentName,oParameter);
  olNodeList := STB$TREENODELIST();
  oCurNode := stb$object.getcurrentnode;

  sOrigToken := STB$OBJECT.getCurrentToken;
  STB$OBJECT.setCurrentToken(oparameter.stoken);

  -- general check if this is a report node check for msnipulations
  -- check for manipulations
  oErrorObj := checkmanipulation (smanipulation);
  isr$trace.info ('sManipulation',  'sManipulation: ' || smanipulation, sCurrentName);

  IF smanipulation = 'T' THEN

    -- For LabOne
    OPEN cGetRepidFromDoc;
    FETCH cGetRepidFromDoc INTO nRepidFromDoc;
    CLOSE cGetRepidFromDoc;

    IF STB$OBJECT.getCurrentRepid = nRepidFromDoc THEN
      RAISE exmanipulation;
    END IF;

  END IF;

  sPackageName := STB$UTIL.gethandler (upper (oparameter.stoken));
  isr$trace.info ('sPackageName',  sPackageName, sCurrentName);
  IF trim (sPackageName) IS NOT NULL AND UPPER(sPackageName) != 'ISR$REPORTTYPE$PACKAGE' THEN
    EXECUTE IMMEDIATE 'DECLARE
        olocError STB$OERROR$RECORD;
      BEGIN
        olocError:='||sPackageName||'.callFunction(:1, :2);
        :3 := olocError;
      END;'
    using IN oparameter, OUT olnodelist, OUT oErrorObj;
    RETURN oErrorObj;
  END IF;

  /*  -- unused!! vs 17.02.2016
  -- get the report type name
  OPEN  cgetreporttypename;
  FETCH cgetreporttypename
  INTO  sReportTypeName;
  CLOSE cgetreporttypename;
  isr$trace.debug('value','sReportTypeName: '||sReportTypeName,sCurrentName);
  */

  IF STB$OBJECT.getCurrentNodeType = 'DOCUMENT' AND upper (oparameter.stoken) IN ('CAN_DISPLAY_COMMENT', 'CAN_DISPLAY_REPORT', 'CAN_SAVE_DOCUMENT')
  OR STB$OBJECT.getCurrentNodeType = 'REPORT' AND upper (oparameter.stoken) IN ('CAN_CREATE_CRITERIA', 'CAN_CREATE_REPORT_VARIANT', 'CAN_DISPLAY_HEADDATA', 'CAN_DISPLAY_REPORTAUDIT')
  OR STB$OBJECT.getCurrentNodeType = 'TITLE' AND upper (oparameter.stoken) IN ('CAN_DISPLAY_ESIG', 'CAN_DISPLAY_TITLE_JOBS', 'CAN_RUN_REPORT') THEN
    -- unlock all reports from this usersession
    oErrorObj := stb$system.unlockreports ('and locksession = ' || to_char(stb$security.getcurrentsession));
  END IF;

  -- Processing takes place in the current package
  CASE


    ----------------- Dialogs ---------------------------------------------------------------------
    ---------------------------------------------------------------------------------------------------------
  WHEN upper (oparameter.stoken) = 'CAN_CHECKIN'
    OR upper (oparameter.stoken) = 'CAN_CHECKIN_COMMON_DOC'
    OR upper (oparameter.stoken) = 'CAN_CHECKOUT'
    OR upper (oparameter.stoken) = 'CAN_CHECKOUT_UNPROTECTED'
    OR upper (oparameter.stoken) = 'CAN_SAVE_DOCUMENT'
    OR upper (oparameter.stoken) = 'CAN_MODIFY_WITH_COMMON_DOC'
    OR upper (oparameter.stoken) = 'CAN_CHECKOUT_COMMON_DOC'
    OR upper (oparameter.stoken) = 'CAN_MODIFY_COMMON_DOC'
    OR upper (oparameter.stoken) = 'CAN_UPLOAD_COMMON_DOC'
    OR upper (oparameter.stoken) = 'CAN_UPLOAD_NEW_TEMPLATE'
    OR upper (oparameter.stoken) = 'CAN_DOWNLOAD_TEMPLATE'
    OR upper (oparameter.stoken) = 'CAN_RELEASE_MASTERTEMPLATE'
    OR upper (oparameter.stoken) = 'CAN_DISPLAY_COMMON_ATTRIBUTES'
    OR upper (oparameter.stoken) = 'CAN_UNDO_CHECKOUT'
    OR upper (oparameter.stoken) = 'CAN_UNDO_CHECKOUT_COMMON_DOC'
    OR upper (oparameter.stoken) = 'CAN_OFFSHOOT_DOC'
    OR upper (oparameter.stoken) like 'CAN_SEND_AS_EMAIL%'
    OR upper (oparameter.stoken) like 'CAN_DISPLAY_%AUDIT'
    OR upper (oparameter.stoken) = 'CAN_CREATE_PDF'
    OR upper (oparameter.stoken) = 'CAN_ARCHIVE_REPORTS'
    OR upper (oparameter.stoken) = 'CAN_EXTRACT_REPORTS'
    OR upper (oparameter.stoken) = 'CAN_CREATE_CRITERIA'
    OR upper (oparameter.stoken) = 'CAN_SEND_TO_DMS' THEN

      IF upper (oparameter.stoken) like 'CAN_DISPLAY_%AUDIT' THEN
        oErrorObj := stb$audit.checkauditexists (sAuditExists,
                                case when oparameter.stoken = 'CAN_DISPLAY_SRAUDIT' then STB$OBJECT.getCurrentTitle
                                     when oparameter.stoken = 'CAN_DISPLAY_REPORTAUDIT' then TO_CHAR(STB$OBJECT.getCurrentRepid)
                                     when oparameter.stoken = 'CAN_DISPLAY_REPORTTYPEAUDIT' then TO_CHAR(STB$OBJECT.getCurrentReporttypeId)
                                     when oparameter.stoken = 'CAN_DISPLAY_TEMPLATEAUDIT' then STB$OBJECT.getCurrentNode().sDisplay
                                     else stb$object.getCurrentNodeId end, REPLACE(upper (oparameter.stoken), 'CAN_DISPLAY_'));
        IF sAuditExists = 'F' THEN
          RAISE exnoaudit;
        END IF;
      END IF;

      IF upper (oparameter.stoken) IN ('CAN_CHECKIN','CAN_UNDO_CHECKOUT') THEN
        -- check that the document is checked out by the current user
        oErrorObj := stb$docks.checkdoccheckedoutbyuser;

        IF oErrorObj.sSeverityCode <> stb$typedef.cnNoError THEN
          RETURN oErrorObj;
        END IF;
      END IF;

      sToken := oParameter.sToken;
      olNodeList := STB$TREENODELIST ();
      olNodeList.EXTEND;
      olNodeList (1) := STB$TREENODE$RECORD(STB$UTIL.getMaskTitle(Upper (sToken)),
                                            'ISR$REPORTTYPE$PACKAGE', 'TRANSPORT', 'MASK');
      oErrorObj := STB$UTIL.getPropertyList(upper (sToken), olNodeList (1).tloPropertylist);

      IF upper (oparameter.stoken) IN ('CAN_UNDO_CHECKOUT','CAN_UNDO_CHECKOUT_COMMON_DOC') THEN
        IF trim(STB$UTIL.getProperty(olnodelist(1).tloPropertyList, 'TO')) IS NULL THEN
          oErrorObj := putParameters(oparameter, olnodelist(1).tloPropertyList);
        END IF;
      END IF;

      IF upper (oparameter.stoken) IN ('CAN_CREATE_PDF') THEN
        OPEN cGetRenderingServerStatus;
        FETCH cGetRenderingServerStatus INTO sRSStatus;
        IF cGetRenderingServerStatus%FOUND AND sRSStatus = 'F' OR cGetRenderingServerStatus%NOTFOUND THEN
          FOR i IN 1..olnodelist(1).tloPropertyList.COUNT() LOOP
            IF olnodelist(1).tloPropertyList(i).sParameter = 'LOCAL' THEN
              olnodelist(1).tloPropertyList(i).sValue := 'T';
              olnodelist(1).tloPropertyList(i).sEditable := 'F';
            END IF;
          END LOOP;
        END IF;
        CLOSE cGetRenderingServerStatus;
      END IF;

    ---------------- move parameter --------------------------------------------------------------------------
    ---------------------------------------------------------------------------------------------------------
  WHEN upper (oparameter.stoken) = 'CAN_MOVE_PARAMETERS' THEN
      olnodelist := stb$treenodelist ();
      olnodelist.EXTEND;
      olnodelist (1) := STB$TREENODE$RECORD(STB$UTIL.getMaskTitle(Upper (oParameter.sToken)),
                                            'ISR$REPORTTYPE$PACKAGE', 'TRANSPORT', 'MASK');
      oErrorObj := STB$UTIL.getPropertyList(upper (oParameter.sToken), olNodeList (1).tloPropertylist);
      oErrorObj := moveParametersList(olnodelist (1).tlopropertylist);

    ---------------- move parameter --------------------------------------------------------------------------
    ---------------------------------------------------------------------------------------------------------
  WHEN upper (oparameter.stoken) = 'CAN_CREATE_NEW_OUTPUT'
    OR upper (oparameter.stoken) = 'CAN_CLONE_OUTPUT' THEN
      olnodelist := stb$treenodelist ();
      olnodelist.EXTEND;
      olnodelist (1) := STB$TREENODE$RECORD(STB$UTIL.getMaskTitle(Upper (oParameter.sToken)),
                                            'ISR$REPORTTYPE$PACKAGE', 'TRANSPORT', 'MASK');
      oErrorObj := STB$UTIL.getPropertyList(upper (oParameter.sToken), olNodeList (1).tloPropertylist);
      oErrorObj := bookmarkList(olnodelist (1).tlopropertylist);

    ---------------- show reportfiles --------------------------------------------------------------------------
    ---------------------------------------------------------------------------------------------------------
  WHEN upper (oparameter.stoken) = 'CAN_DISPLAY_REPORT_FILES' THEN
      olnodelist := stb$treenodelist ();
      olnodelist.EXTEND;
      olnodelist (1) := STB$TREENODE$RECORD(STB$UTIL.getMaskTitle(Upper (oParameter.sToken)),
                                            'ISR$REPORTTYPE$PACKAGE', 'TRANSPORT', 'MASK');
      oErrorObj := STB$UTIL.getPropertyList(upper (oParameter.sToken), olNodeList (1).tloPropertylist);
      oErrorObj := showreportfiles (olnodelist (1).tlopropertylist);

    ---------------- show headdata --------------------------------------------------------------------------
    ---------------------------------------------------------------------------------------------------------
  WHEN upper (oparameter.stoken) = 'CAN_DISPLAY_HEADDATA' THEN
      olnodelist := stb$treenodelist ();
      olnodelist.EXTEND;
      olnodelist (1) := STB$TREENODE$RECORD(STB$UTIL.getMaskTitle(Upper (oParameter.sToken)),
                                            'ISR$REPORTTYPE$PACKAGE', 'TRANSPORT', 'MASK');
      oErrorObj := showheaddata (olnodelist (1).tlopropertylist);
    ---------------- show comment --------------------------------------------------------------------------
    ---------------------------------------------------------------------------------------------------------
  WHEN upper (oparameter.stoken) = 'CAN_DISPLAY_COMMENT' THEN
      olnodelist := stb$treenodelist ();
      olnodelist.EXTEND;
      olnodelist (1) := STB$TREENODE$RECORD(STB$UTIL.getMaskTitle(Upper (oParameter.sToken)),
                                            'ISR$REPORTTYPE$PACKAGE', 'TRANSPORT', 'MASK');
      oErrorObj := showcheckincomment (olnodelist (1).tlopropertylist);
    ------------------ configure offshoot list--------------------------------------------------------------
    --------------------------------------------------------------------------------------------------------
  WHEN upper (oparameter.stoken) = 'CAN_CONFIGURE_OFFSHOOT' THEN
      olnodelist := stb$treenodelist ();
      olnodelist.EXTEND;
      olnodelist (1) := STB$TREENODE$RECORD(STB$UTIL.getMaskTitle(Upper (oParameter.sToken)),
                                            'ISR$REPORTTYPE$PACKAGE', 'TRANSPORT', 'MASK');
      oErrorObj := getOffshootConfList (olnodelist (1).tlopropertylist);
    ---------------- display report type parameter ----------------------------------------------------------
    ---------------------------------------------------------------------------------------------------------
  WHEN upper (oparameter.stoken) = 'CAN_MODIFY_REPTYPE_PARAMETERS' THEN
      olnodelist := stb$treenodelist ();
      olnodelist.EXTEND;
      olnodelist (1) := STB$TREENODE$RECORD(STB$UTIL.getMaskTitle(Upper (oParameter.sToken)),
                                            'ISR$REPORTTYPE$PACKAGE', 'TRANSPORT', 'MASK');
      oErrorObj := getReptypeParameters (olnodelist (1).tlopropertylist);

    ------------ display the document properties ------------------------------------------------------------
    ---------------------------------------------------------------------------------------------------------
  WHEN upper (oparameter.stoken) = 'CAN_ADD_DOCPROPERTY'
    OR upper (oparameter.stoken) = 'CAN_ADD_DOCPROPERTY_COMMON' THEN
      olnodelist := stb$treenodelist ();
      olnodelist.EXTEND;
      olnodelist (1) := STB$TREENODE$RECORD(STB$UTIL.getMaskTitle(Upper (oParameter.sToken)),
                                            'ISR$REPORTTYPE$PACKAGE', 'TRANSPORT', 'MASK');
      oErrorObj := getdocumentproperties (olnodelist (1).tlopropertylist);

    --------- new report  -----------------------------------------------------------------------------------
    ---------------------------------------------------------------------------------------------------------
  WHEN upper (oparameter.stoken) IN ('CAN_CREATE_NEW_REPORT','CAN_TEST_FRONTEND','CAN_CREATE_REPORT_VARIANT', 'CAN_CREATE_NEW_REP') THEN

      olnodelist := stb$treenodelist ();
      olnodelist.EXTEND;
      olnodelist (1) := STB$TREENODE$RECORD(STB$UTIL.getMaskTitle(Upper (oParameter.sToken)),
                                            'ISR$REPORTTYPE$PACKAGE', 'TRANSPORT', 'MASK');
      oErrorObj := STB$UTIL.getPropertyList('CAN_CREATE_NEW_REPORT', olNodeList(1).tloPropertylist);
      isr$trace.debug('after getPropertyList','olNodeList(1)',sCurrentName,olNodelist(1));

      -- hide the servers not
      FOR rGetInactiveServers IN ( SELECT parametername
                                     FROM stb$userparameter
                                    WHERE parametervalue = 'F'
                                      AND parametername LIKE 'SERVER_%'
                                      AND userno = STB$SECURITY.getCurrentUser) LOOP
        FOR i IN 1..olNodeList (1).tloPropertylist.COUNT LOOP
          IF olNodeList(1).tloPropertylist(i).sParameter = rGetInactiveServers.parametername THEN
            olNodeList(1).tloPropertylist(i).sDisplayed := 'F';
          END IF;
        END LOOP;
      END LOOP;

      isr$trace.debug('after GetInactiveServers','olNodeList(1)',sCurrentName,olNodelist(1));

      select count(*)
      into   nGetActiveServers
      from   table(olNodeList(1).tloPropertylist)
      where  sDisplayed = 'T'
      and    sParameter like 'SERVER_%';
      isr$trace.debug('value','nGetActiveServers: '||nGetActiveServers,sCurrentName);

--      IF upper (oparameter.stoken) IN ('CAN_CREATE_NEW_REP', 'CAN_CREATE_NEW_REPORT') /*AND TRIM(olNodeList (1).tloPropertylist(1).sValue) is null*/ THEN
--        isr$trace.debug (TRIM(olNodeList (1).tloPropertylist(1).sParameter), 'ISR$REPORTTYPE$PACKAGE.callFunction', TRIM(olNodeList (1).tloPropertylist(1).sValue), sCurrentName);
--        olNodeList (1).tloPropertylist(1).sDisplayed := 'T';
--      END IF;

      FOR i IN 1..olNodeList (1).tloPropertylist.COUNT LOOP
        IF olNodeList (1).tloPropertylist(i).sParameter like 'SERVER_%' THEN
          olNodeList (1).tloPropertylist(i).sDisplayed :=
             case when (     STB$USERSECURITY.getUserParameter (STB$SECURITY.getCurrentUser, 'SHOW_SERVER_IN_WIZARD') = 'F'
                          or nGetActiveServers IN (0,1) )
               then 'F'
               else olNodeList (1).tloPropertylist(i).sDisplayed
             end;
        END IF;
        IF olNodeList (1).tloPropertylist(i).sParameter like 'DATA_SELECTION_%' THEN
          olNodeList (1).tloPropertylist(i).sDisplayed := STB$USERSECURITY.getUserParameter (STB$SECURITY.getCurrentUser, 'DIALOG_BEFORE_WIZARD');
        END IF;
      END LOOP;
      isr$trace.debug('final PropertyList','olNodeList',sCurrentName,olNodelist);

    -------- replace server ---------------------------------------------------------------------------------
    ---------------------------------------------------------------------------------------------------------
  WHEN upper (oparameter.stoken) = 'CAN_REPLACE_REPORT_SERVER' THEN
      oErrorObj := createnewreport (stb$typedef.cnversion);
      oErrorObj := isr$reporttype$package.createsraudit (stb$object.getcurrentsrnumber);

      IF oErrorObj.sSeverityCode != stb$typedef.cnnoerror THEN
        RETURN oErrorObj;
      END IF;

      olnodelist := stb$treenodelist ();
      olnodelist.EXTEND;
      olnodelist (1) := STB$TREENODE$RECORD(STB$UTIL.getMaskTitle(Upper (oParameter.sToken)),
                                            'ISR$REPORTTYPE$PACKAGE', 'TRANSPORT', 'MASK');
      oErrorObj := STB$UTIL.getPropertyList('CAN_CREATE_NEW_REPORT', olNodeList(1).tloPropertylist);
      isr$trace.debug('after getPropertyList','olNodeList(1)',sCurrentName,olNodelist(1));

      FOR i IN 1..olNodeList (1).tloPropertylist.COUNT LOOP
        IF olNodeList (1).tloPropertylist(i).sParameter not like 'SERVER_%' THEN
          olNodeList (1).tloPropertylist(i).type := 'DELETE';
        END IF;
      END LOOP;
      isr$trace.debug('final PropertyList','olNodeList',sCurrentName,olNodelist);

     ---!!!!!!!

    -------- new version-------------------------------------------------------------------------------------
    ---------------------------------------------------------------------------------------------------------
  WHEN upper (oparameter.stoken) = 'CAN_CREATE_REPORT_VERSION' THEN
      oErrorObj := createnewreport (stb$typedef.cnversion);
      oErrorObj := isr$reporttype$package.createsraudit (stb$object.getcurrentsrnumber);

      IF oErrorObj.sSeverityCode != stb$typedef.cnNoError THEN
        RETURN oErrorObj;
      END IF;

      -- call of the first from in the wizzard
      oErrorObj := firstform;
      -- tell the frontend to go to the first firm
      RAISE exfirstform;
    ----------- modify criteria (implizit Version)-----------------------------------------------------------
    ---------------------------------------------------------------------------------------------------------
  WHEN upper (oparameter.stoken) = 'CAN_MODIFY_REPORT' THEN

      IF stb$util.getReptypeParameter (stb$object.getCurrentReporttypeid, 'REQUIRES_VERSIONING') = 'T' THEN
        oErrorObj := createnewreport (stb$typedef.cnversion);
        oErrorObj := isr$reporttype$package.createsraudit (stb$object.getcurrentsrnumber);
      END IF;

      IF oErrorObj.sSeverityCode != stb$typedef.cnNoError THEN
        RETURN oErrorObj;
      END IF;

      -- call of the first from in the wizzard
      oErrorObj := firstform;
      -- tell the frontend to go to the first firm
      RAISE exfirstform;

    ----------- modify output opbions-------------------------------------------
    ----------------------------------------------------------------------------
  WHEN upper (oparameter.stoken) = 'CAN_MODIFY_OUTPUTOPTIONS' THEN

      IF stb$util.getReptypeParameter (stb$object.getCurrentReporttypeid, 'REQUIRES_VERSIONING') = 'T' THEN
        oErrorObj := createnewreport (stb$typedef.cnversion);
        oErrorObj := isr$reporttype$package.createsraudit (stb$object.getcurrentsrnumber);
      END IF;

      IF oErrorObj.sSeverityCode != stb$typedef.cnNoError THEN
        RETURN oErrorObj;
      END IF;

      -- (NB) - wir sind nicht im wizard, deshalb direkt die letzte Maske setzen ARDIS-491
      nCurrentFormnumber := getNumberOfForms;

      olnodelist := stb$treenodelist ();
      olnodelist.EXTEND;
      olnodelist (1) := STB$TREENODE$RECORD(STB$UTIL.getMaskTitle(Upper (oParameter.sToken)),
                                            'ISR$REPORTTYPE$PACKAGE', 'TRANSPORT', 'MASK');
      oErrorObj := getOutputOption (olnodelist (1).tlopropertylist);

      isr$trace.debug('olnodelist(1)','olnodelist(1)',sCurrentName,olnodelist(1));

    --=========================CAN_DEACTIVATE_REPORT=========================
  WHEN upper (oparameter.stoken) = 'CAN_DEACTIVATE_REPORT' THEN
      oErrorObj := changecondition (stb$typedef.cnStatusdeactive);
      oErrorObj := isr$reporttype$package.createsraudit (stb$object.getcurrentsrnumber);
      sessionUpdate;
      COMMIT;
      ISR$TREE$PACKAGE.reloadPrevNodesAndSelect(-2);

    ---------------- deactivate Inspection status------------------------------------------------------------
    ---------------------------------------------------------------------------------------------------------
  WHEN upper (oparameter.stoken) = 'CAN_DEACTIVATE_INSPECTION' THEN

      -- Check if a document already exists and if the document is checked in
      oErrorObj := stb$docks.lastdoccheckedin (scheckedout, ndocid, spath, smachine, soracleuser);
      isr$trace.debug ('sCheckedout',  scheckedout || ' nDocId: ' || ndocid, sCurrentName);
      IF scheckedout = 'T' THEN
        RAISE exdoccheckedout;
      END IF;

    ---------------- delete document------------------------------------------------------------
    ---------------------------------------------------------------------------------------------------------
  WHEN upper (oparameter.stoken) = 'CAN_DELETE_DOCUMENT' THEN
      delete from isr$doc$property where docid = stb$object.getCurrentDocid;
      delete from stb$doctrail where docid = stb$object.getCurrentDocid;
      COMMIT;
      ISR$TREE$PACKAGE.selectCurrent;

    --------- DELETE COMMON DOCUMENTS------------------------------------------------------------------------
    ---------------------------------------------------------------------------------------------------------
  WHEN upper (oparameter.stoken) = 'CAN_DELETE_COMMON' THEN
      oErrorObj := stb$docks.deletecommon;
      COMMIT;
      ISR$TREE$PACKAGE.selectCurrent;

    ----------------- display ----------------------------------------------------------------------------------
    ---------------------------------------------------------------------------------------------------------
  WHEN upper (oparameter.stoken) = 'CAN_DISPLAY_REPORT'
    OR upper (oparameter.stoken) = 'CAN_DISPLAY_COMMON_DOC'
    OR upper (oparameter.stoken) = 'CAN_DISPLAY_REMOTESNAPSHOT'THEN

      oErrorObj := setjobparameter (upper (oparameter.stoken));

      IF upper (oparameter.stoken) = 'CAN_DISPLAY_REPORT' THEN
        stb$job.setjobparameter ('DO_NOT_SWITCH_REPSESSION', 'T', stb$job.njobid);
      END IF;

--      IF upper (oparameter.stoken) = 'CAN_DISPLAY_REPORT' THEN
--        DECLARE
--          CURSOR cGetCheckoutPath(cnDocid IN STB$DOCTRAIL.DOCID%TYPE) IS
--            SELECT path|| '\' || filename
--              FROM stb$doctrail
--             WHERE docid = cnDocid
--               AND checkedout = 'T';
--        BEGIN
--          OPEN cGetCheckoutPath(STB$OBJECT.getCurrentDocid);
--          FETCH cGetCheckoutPath INTO sPath;
--          IF cGetCheckoutPath%FOUND THEN
--            stb$job.setjobparameter ('TO', sPath, stb$job.njobid);
--          END IF;
--          CLOSE cGetCheckoutPath;
--        EXCEPTION WHEN OTHERS THEN
--          NULL; -- doesn't matter
--        END;
--      END IF;

      oErrorObj := stb$job.startjob;

    ----------------- jobs ----------------------------------------------------------------------------------
    ---------------------------------------------------------------------------------------------------------
  WHEN upper (oparameter.stoken) = 'CAN_DISPLAY_RAWDATA'
    OR upper (oparameter.stoken) = 'CAN_DISPLAY_IN_VIEWER'
    OR upper (oparameter.stoken) = 'CAN_DISPLAY_ESIG_DOC'
    OR upper (oparameter.stoken) = 'CAN_CREATE_CRITERIA' THEN
      oErrorObj := setjobparameter (upper (oparameter.stoken));
      IF upper (oparameter.stoken) = 'CAN_CREATE_CRITERIA' THEN
        stb$job.setjobparameter ('DO_NOT_SWITCH_REPSESSION', 'T', stb$job.njobid);
      END IF;
      oErrorObj := stb$job.startjob;
    --------- HEADDATA REPORT---------------------------------------------------------------------------------------
    ---------------------------------------------------------------------------------------------------------
  WHEN upper (oparameter.stoken) = 'CAN_DISPLAY_HEADDATA_REPORT' THEN

      oErrorObj := setjobparameter (upper (oparameter.stoken));
      oErrorObj := Stb$util.createFilterParameter (olParameter);
      oErrorObj := stb$job.startjob;

    ----------------- create document -----------------------------------------------------------------------
    ---------------------------------------------------------------------------------------------------------
  WHEN upper (oparameter.stoken) = 'CAN_RUN_REPORT' THEN
      -- Check if a document already exists and if the document is checked in
      oErrorObj := stb$docks.lastdoccheckedin (scheckedout, ndocid, spath, smachine, soracleuser);
      isr$trace.debug ('sCheckedout',  scheckedout || ' nDocId: ' || ndocid, sCurrentName);
      IF scheckedout = 'T' THEN
        RAISE exdoccheckedout;
      END IF;

      -- check for not running servers   -- 7.Jun 2019 hr added for [ISRC-1119]
      isr$trace.debug('sOrigToken', sOrigToken, sCurrentName);
      if nvl(sOrigToken, 'CAN_RUN_REPORT') = 'CAN_RUN_REPORT' then
        open cNotRunningServer;
        fetch cNotRunningServer into sNotRunning;
        if cNotRunningServer%notfound then
          sNotRunning := null;
        end if;
        close cNotRunningServer;
        if sNotRunning is not null then
          -- not running server found. Raise to stop execution
          raise exServerNotRunning;
        end if;
      end if;

      -- special case  when a final reportexists with no document ,then create report is allowed

      IF stb$object.getCurrentRepstatus = stb$typedef.cnStatusfinal THEN
        isr$trace.debug ('final create doc after system failure', 'final create doc after system failure', sCurrentName);
        oErrorObj := setjobparameter (upper ('CAN_FINALIZE_REPORT'));
        stb$job.setjobparameter ('COMMAND', 'CAN_RUN_REPORT', stb$job.njobid);
        stb$job.setjobparameter ('FINALFLAG', nvl(stb$util.getReptypeParameter (stb$object.getCurrentReporttypeid, 'FINALIZE_FLAG'), 'FINAL'), stb$job.njobid);
        stb$job.setjobparameter ('CAN_USE_DOC_SERVER', 'T', stb$job.njobid);
        OPEN cGetRenderingServerStatus;
        FETCH cGetRenderingServerStatus INTO sRSStatus;
        IF cGetRenderingServerStatus%FOUND AND sRSStatus = 'F' OR cGetRenderingServerStatus%NOTFOUND THEN
          stb$job.setjobparameter ('FILTER_LOCAL', 'T', stb$job.njobid);
        END IF;
        CLOSE cGetRenderingServerStatus;
        oErrorObj := stb$job.startjob;
        RETURN oErrorObj;
      END IF;

      olnodelist := stb$treenodelist ();
      olnodelist.EXTEND;
      olnodelist (1) := STB$TREENODE$RECORD(STB$UTIL.getMaskTitle(Upper (oParameter.sToken)),
                                            'ISR$REPORTTYPE$PACKAGE', 'TRANSPORT', 'MASK');

      oErrorObj := STB$UTIL.getPropertyList('CAN_RUN_REPORT_SERVER', tloPropertylist1);

      OPEN cGetRenderingServerStatus;
      FETCH cGetRenderingServerStatus INTO sRSStatus;
      IF cGetRenderingServerStatus%FOUND AND sRSStatus = 'F' OR cGetRenderingServerStatus%NOTFOUND THEN
        FOR i IN 1..tloPropertylist1.COUNT() LOOP
          IF tloPropertylist1(i).sParameter = 'LOCAL' THEN
            tloPropertylist1(i).sValue := 'T';
            tloPropertylist1(i).sDisplay := 'T';
            tloPropertylist1(i).sEditable := 'F';
          END IF;
        END LOOP;
      END IF;
      CLOSE cGetRenderingServerStatus;

      IF STB$USERSECURITY.checkoutDirect(stb$security.getcurrentuser) = 'T'
      AND STB$UTIL.getReptypeParameter (stb$object.getCurrentReporttypeid, 'REQUIRES_VERSIONING') = 'T' THEN
        oErrorObj := STB$UTIL.getPropertyList('CAN_RUN_REPORT_CHECKOUT', tloPropertylist2);
      END IF;


      IF STB$SECURITY.checkGroupRight ('CAN_CONFIGURE_ORACLE_JOBS') = 'T'
      AND STB$OBJECT.getCurrentRepid is null THEN
        FOR i IN 1..tloPropertylist1.COUNT() LOOP
          IF tloPropertylist1(i).sParameter = 'LOCAL' THEN
            tloPropertylist1(i).sValue := 'F';
            tloPropertylist1(i).sDisplay := 'F';
            tloPropertylist1(i).sEditable := 'F';
          END IF;
        END LOOP;
        oErrorObj := STB$UTIL.getPropertyList('CAN_CONFIGURE_ORACLE_JOBS', tloPropertylist3);
      END IF;


      FOR i IN 1..tloPropertylist3.COUNT() LOOP
        olnodelist(1).tloPropertyList.EXTEND;
        olnodelist(1).tloPropertyList(olnodelist(1).tloPropertyList.COUNT) := tloPropertylist3(i);
      END LOOP;

      FOR i IN 1..tloPropertylist1.COUNT() LOOP
        olnodelist(1).tloPropertyList.EXTEND;
        olnodelist(1).tloPropertyList(olnodelist(1).tloPropertyList.COUNT) := tloPropertylist1(i);
        IF NVL(tloPropertylist1(i).sDisplayed, 'T') = 'T' THEN
          olnodelist(1).tloPropertyList(olnodelist(1).tloPropertyList.COUNT).sDisplayed := STB$USERSECURITY.getUserParameter (STB$SECURITY.getCurrentUser, 'CAN_RUN_REPORT_SERVER');
        END IF;
        IF STB$UTIL.getReptypeParameter (stb$object.getCurrentReporttypeid, 'REQUIRES_VERSIONING') = 'F'
        AND tloPropertylist1(i).sParameter = 'PARA_DISPLAYED_PDF_CREATION_ALLOWED' THEN
          olnodelist(1).tloPropertyList(olnodelist(1).tloPropertyList.COUNT).sDisplayed := 'F';
        END IF;
      END LOOP;

      FOR i IN 1..tloPropertylist2.COUNT() LOOP
        olnodelist(1).tloPropertyList.EXTEND;
        olnodelist(1).tloPropertyList(olnodelist(1).tloPropertyList.COUNT) := tloPropertylist2(i);
      END LOOP;


      RETURN oErrorObj;

   ---------------- set Inspection status-------------------------------------------------------------------
    ---------------------------------------------------------------------------------------------------------
  WHEN upper (oparameter.stoken) = 'CAN_RELEASE_FOR_INSPECTION' THEN

      -- Check if a document already exists and if the document is checked in
      oErrorObj := stb$docks.lastdoccheckedin (scheckedout, ndocid, spath, smachine, soracleuser);
      isr$trace.debug ('sCheckedout', scheckedout || ' nDocId: ' || ndocid, sCurrentName);
      IF scheckedout = 'T' THEN
        RAISE exdoccheckedout;
      END IF;

      -- validate the inspection data first
      oErrorObj := validateinspectiondata (svalidateflag);

      IF svalidateflag = 'T' THEN
        -- check if the last document is checked in
        oErrorObj := stb$docks.lastdoccheckedin (scheckedout, ndocid, spath, smachine, soracleuser);

        IF scheckedout = 'T' THEN
          RAISE exdoccheckedout;
        END IF;

        -- set report into the inspection status
        oErrorObj := setinspectionstatus;
        oErrorObj := isr$reporttype$package.createsraudit (stb$object.getcurrentsrnumber);

        IF oErrorObj.sSeverityCode = stb$typedef.cnNoError THEN

          olnodelist := stb$treenodelist ();
          olnodelist.EXTEND;
          olnodelist (1) := STB$TREENODE$RECORD(STB$UTIL.getMaskTitle(Upper (oParameter.sToken)),
                                            'ISR$REPORTTYPE$PACKAGE', 'TRANSPORT', 'MASK');

          oErrorObj := STB$UTIL.getPropertyList('CAN_RUN_REPORT_SERVER', tloPropertylist1);
          OPEN cGetRenderingServerStatus;
          FETCH cGetRenderingServerStatus INTO sRSStatus;
          IF cGetRenderingServerStatus%FOUND AND sRSStatus = 'F' OR cGetRenderingServerStatus%NOTFOUND THEN
            IF STB$USERSECURITY.getUserParameter (STB$SECURITY.getCurrentUser, 'LOCAL') = 'F' THEN
            olnodelist(1).tloPropertyList.EXTEND;
            olnodelist(1).tloPropertyList(olnodelist(1).tloPropertyList.COUNT) :=
                   STB$PROPERTY$RECORD ('CONFIRM_TEXT_'||upper (oparameter.stoken)
                                        , utd$msglib.getmsg ('INFO', stb$security.getCurrentLanguage)
                                        , utd$msglib.getmsg ('CONFIRM_TEXT_'||upper (oparameter.stoken), stb$security.getCurrentLanguage)
                                        , utd$msglib.getmsg ('CONFIRM_TEXT_'||upper (oparameter.stoken), stb$security.getCurrentLanguage)
                                        , 'F'
                                        , 'T'
                                        , 'T'
                                        , 'F'
                                        , 'LONGSTRING'
                                        , null
                                        , 'F');
            END IF;
            FOR i IN 1..tloPropertylist1.COUNT() LOOP
              IF tloPropertylist1(i).sParameter = 'LOCAL' THEN
                tloPropertylist1(i).sValue := 'T';
                tloPropertylist1(i).sDisplay := 'T';
                tloPropertylist1(i).sEditable := 'F';
              END IF;
            END LOOP;
          END IF;
          CLOSE cGetRenderingServerStatus;

          IF STB$USERSECURITY.checkoutDirect(stb$security.getcurrentuser) = 'T'
          AND STB$UTIL.getReptypeParameter (stb$object.getCurrentReporttypeid, 'REQUIRES_VERSIONING') = 'T' THEN
            oErrorObj := STB$UTIL.getPropertyList('CAN_RUN_REPORT_CHECKOUT', tloPropertylist2);
          END IF;

          FOR i IN 1..tloPropertylist1.COUNT() LOOP
            olnodelist(1).tloPropertyList.EXTEND;
            olnodelist(1).tloPropertyList(olnodelist(1).tloPropertyList.COUNT) := tloPropertylist1(i);
            IF NVL(tloPropertylist1(i).sDisplayed, 'T') = 'T' THEN
              olnodelist(1).tloPropertyList(olnodelist(1).tloPropertyList.COUNT).sDisplayed := STB$USERSECURITY.getUserParameter (STB$SECURITY.getCurrentUser, 'CAN_RUN_REPORT_SERVER');
            END IF;
            IF STB$UTIL.getReptypeParameter (stb$object.getCurrentReporttypeid, 'REQUIRES_VERSIONING') = 'F'
            AND tloPropertylist1(i).sParameter = 'PARA_DISPLAYED_PDF_CREATION_ALLOWED' THEN
              olnodelist(1).tloPropertyList(olnodelist(1).tloPropertyList.COUNT).sDisplayed := 'F';
            END IF;
          END LOOP;
          FOR i IN 1..tloPropertylist2.COUNT() LOOP
            olnodelist(1).tloPropertyList.EXTEND;
            olnodelist(1).tloPropertyList(olnodelist(1).tloPropertyList.COUNT) := tloPropertylist2(i);
          END LOOP;

        ELSE
          RETURN oErrorObj;
        END IF;

      END IF;

    --------------------------------------------------------------------------
    ------------------------------------CAN_LOCK_REPORT-----------------------
  WHEN upper (oparameter.stoken) = 'CAN_LOCK_REPORT' THEN

      -- Check if a document already exists and if the document is checked in
      oErrorObj := stb$docks.lastdoccheckedin (scheckedout, ndocid, spath, smachine, soracleuser);
      isr$trace.debug ('sCheckedout',  scheckedout || ' nDocId: ' || ndocid, sCurrentName);
      IF scheckedout = 'T' THEN
        RAISE exdoccheckedout;
      END IF;

    --------------------------------------------------------------------------
    ------------------------------------CAN_UNLOCK_REPORT---------------------
  WHEN upper (oparameter.stoken) = 'CAN_UNLOCK_REPORT' THEN
      NULL;

    --------------------------------------------------------------------------
    ------------------------------------CAN_CREATE_REPORT_%_VERSION-----------
  WHEN upper (oparameter.stoken) like 'CAN_CREATE_REPORT_%_VERSION' THEN
      NULL;

    --------------------------------------------------------------------------
    ------------------------------------CAN_USE_TEMPLATE_AS_CURRENT-----------
  WHEN upper (oparameter.stoken) = 'CAN_USE_TEMPLATE_AS_CURRENT' THEN

      nFileId := TO_NUMBER(STB$OBJECT.getCurrentNodeId);

      OPEN cGetFile(nFileId);
      FETCH cGetFile INTO sFileName, blBinaryFile, nPrevFileId;
      CLOSE cGetFile;

      oErrorObj := ISR$TEMPLATE$HANDLER.updateTemplate(nFileId, null, null, sFileName, blBinaryFile, nNewFileid, 'T');

      xXml := ISR$TEMPLATE$HANDLER.compareTemplates (nPrevFileId, nNewFileId, 'T');

      FOR rGetAuditTokens IN ( SELECT DISTINCT EXTRACTVALUE (COLUMN_VALUE, '/*/ORIGIN/text()') AS TOKEN
                                 FROM TABLE (XMLSEQUENCE (EXTRACT (xXml, '/*/*')))
                                WHERE EXTRACTVALUE (COLUMN_VALUE, '/*/ORIGIN/text()') != 'TEMPLATE'
                                  AND EXTRACTVALUE (COLUMN_VALUE, '/*/IS_DIFFERENT/text()') = 'T') LOOP
        oErrorObj := Stb$audit.AddAuditRecord (rGetAuditTokens.TOKEN);
        FOR rGetAuditEntries IN ( SELECT EXTRACTVALUE (COLUMN_VALUE, '/*/ORIGIN/text()') AS TOKEN
                                       , EXTRACTVALUE (COLUMN_VALUE, '/*/PARAMETER/text()')||chr(10)||'FILEID: '||nNewFileId AS PK_VALUE
                                       , EXTRACTVALUE (COLUMN_VALUE, '/*/FIRST_TEMPLATE/text()') AS OLD_VALUE
                                       , EXTRACTVALUE (COLUMN_VALUE, '/*/SECOND_TEMPLATE/text()') AS NEW_VALUE
                                    FROM TABLE (XMLSEQUENCE (EXTRACT (xXml, '/*/*')))
                                   WHERE EXTRACTVALUE (COLUMN_VALUE, '/*/ORIGIN/text()') != 'TEMPLATE'
                                     AND EXTRACTVALUE (COLUMN_VALUE, '/*/IS_DIFFERENT/text()') = 'T'
                                     AND EXTRACTVALUE (COLUMN_VALUE, '/*/ORIGIN/text()') = rGetAuditTokens.TOKEN) LOOP
          IF rGetAuditEntries.OLD_VALUE IS NULL THEN
            oErrorObj := Stb$audit.AddAuditEntry (rGetAuditEntries.PK_VALUE, 'Insert', rGetAuditEntries.NEW_VALUE, 1);
          ELSIF rGetAuditEntries.NEW_VALUE IS NULL THEN
            oErrorObj := Stb$audit.AddAuditEntry (rGetAuditEntries.PK_VALUE, 'Delete', rGetAuditEntries.OLD_VALUE, 1);
          ELSE
            oErrorObj := Stb$audit.AddAuditEntry (rGetAuditEntries.PK_VALUE, rGetAuditEntries.OLD_VALUE, rGetAuditEntries.NEW_VALUE, 1);
          END IF;
        END LOOP;
      END LOOP;

      ISR$TREE$PACKAGE.reloadPrevNodesAndSelect(-1, 'F');
      ISR$TREE$PACKAGE.selectRightNode(ISR$TREE$PACKAGE.buildNode(csNodeType => 'CURRENTTEMPLATE', csNodeId => nNewFileId));

      IF stb$util.checkpromptedauditrequired ('PROMPTED_REP_TYPE_AUDIT') = 'T' THEN
        RAISE exaudit;
      ELSE
        oErrorObj := stb$audit.silentaudit;
        COMMIT;
      END IF;

    ------- finalize report----------------------------------------------------------------------------------
    ---------------------------------------------------------------------------------------------------------
  WHEN upper (oparameter.stoken) = 'CAN_FINALIZE_REPORT' THEN
      -- Check if a document already exists and if the document is checked in
      oErrorObj := stb$docks.lastdoccheckedin (scheckedout, ndocid, spath, smachine, soracleuser);

      IF trim (ndocid) IS NOT NULL AND scheckedout <> 'F' THEN
        RAISE exdoccheckedout;
      END IF;

      -- call function to set the final status and create the document
      oErrorObj := setfinalstatus;
      oErrorObj := isr$reporttype$package.createsraudit (stb$object.getcurrentsrnumber);

      IF oErrorObj.sSeverityCode = stb$typedef.cnNoError THEN

        olnodelist := stb$treenodelist ();
        olnodelist.EXTEND;
        olnodelist (1) := STB$TREENODE$RECORD(STB$UTIL.getMaskTitle(Upper (oParameter.sToken)),
                                            'ISR$REPORTTYPE$PACKAGE', 'TRANSPORT', 'MASK');

        oErrorObj := STB$UTIL.getPropertyList('CAN_RUN_REPORT_SERVER', tloPropertylist1);
        OPEN cGetRenderingServerStatus;
        FETCH cGetRenderingServerStatus INTO sRSStatus;
        IF cGetRenderingServerStatus%FOUND AND sRSStatus = 'F' OR cGetRenderingServerStatus%NOTFOUND THEN
          IF STB$USERSECURITY.getUserParameter (STB$SECURITY.getCurrentUser, 'LOCAL') = 'F' THEN
          olnodelist(1).tloPropertyList.EXTEND;
          olnodelist(1).tloPropertyList(olnodelist(1).tloPropertyList.COUNT) :=
                 STB$PROPERTY$RECORD ('CONFIRM_TEXT_'||upper (oparameter.stoken)
                                      , utd$msglib.getmsg ('INFO', stb$security.getCurrentLanguage)
                                      , utd$msglib.getmsg ('CONFIRM_TEXT_'||upper (oparameter.stoken), stb$security.getCurrentLanguage)
                                      , utd$msglib.getmsg ('CONFIRM_TEXT_'||upper (oparameter.stoken), stb$security.getCurrentLanguage)
                                      , 'F'
                                      , 'T'
                                      , 'T'
                                      , 'F'
                                      , 'LONGSTRING'
                                      , null
                                      , 'F');
          END IF;
          FOR i IN 1..tloPropertylist1.COUNT() LOOP
            IF tloPropertylist1(i).sParameter = 'LOCAL' THEN
              tloPropertylist1(i).sValue := 'T';
              tloPropertylist1(i).sDisplay := 'T';
              tloPropertylist1(i).sEditable := 'F';
            END IF;
          END LOOP;
        END IF;
        CLOSE cGetRenderingServerStatus;

        FOR i IN 1..tloPropertylist1.COUNT() LOOP
          olnodelist(1).tloPropertyList.EXTEND;
          olnodelist(1).tloPropertyList(olnodelist(1).tloPropertyList.COUNT) := tloPropertylist1(i);
          IF NVL(tloPropertylist1(i).sDisplayed, 'T') = 'T' THEN
            olnodelist(1).tloPropertyList(olnodelist(1).tloPropertyList.COUNT).sDisplayed := STB$USERSECURITY.getUserParameter (STB$SECURITY.getCurrentUser, 'CAN_RUN_REPORT_SERVER');
          END IF;
          IF STB$UTIL.getReptypeParameter (stb$object.getCurrentReporttypeid, 'REQUIRES_VERSIONING') = 'F'
          AND tloPropertylist1(i).sParameter = 'PARA_DISPLAYED_PDF_CREATION_ALLOWED' THEN
            olnodelist(1).tloPropertyList(olnodelist(1).tloPropertyList.COUNT).sDisplayed := 'F';
          END IF;
          IF NVL(NVL(STB$USERSECURITY.getUserParameter (STB$SECURITY.getCurrentUser, 'CREATE_FINAL_PDF'), STB$UTIL.getReptypeParameter (stb$object.getCurrentReporttypeid, 'CREATE_FINAL_PDF')), 'F') = 'T'
          AND tloPropertylist1(i).sParameter = 'PARA_DISPLAYED_PDF_CREATION_ALLOWED' THEN
            olnodelist(1).tloPropertyList(olnodelist(1).tloPropertyList.COUNT).sValue := 'T';
            olnodelist(1).tloPropertyList(olnodelist(1).tloPropertyList.COUNT).sDisplay := 'T';
            olnodelist(1).tloPropertyList(olnodelist(1).tloPropertyList.COUNT).sDisplayed := 'F';
          END IF;
        END LOOP;

      ELSE
        RETURN oErrorObj;
      END IF;

    --------- SWITCH REPORTTYPE RIGHT----------------------------------------------------------------
    -------------------------------------------------------------------------------------------------
  WHEN upper (oparameter.stoken) = 'CAN_SWITCH_REPRIGHT' THEN

      oErrorObj := switchcurrentgroupright (stb$object.getCurrentReporttypeid, stb$object.getcurrentnode, upper (oparameter.stoken));

      olNodeList := null; --ISRC-768

      IF stb$util.checkpromptedauditrequired ('PROMPTED_SWITCH_GROUP_RIGHT') = 'T' THEN
        ISR$TREE$PACKAGE.selectCurrent('F');
        RAISE exaudit;
      ELSE
        oErrorObj := stb$audit.silentaudit;
        COMMIT;
        ISR$TREE$PACKAGE.selectCurrent;
      END IF;

    -- if the parameter is not found
  ELSE
      RAISE exnotexists;
  END CASE;

  isr$trace.debug('olNodeList', '-> logclob', sCurrentName, olNodeList);
  isr$trace.stat('end','end', sCurrentName);
  RETURN oErrorObj;
EXCEPTION
  WHEN exfirstform THEN
    oErrorObj.sErrorCode := 'exFirstForm';
    oErrorObj.handleError (stb$typedef.cnfirstform, sMsg, sCurrentName, dbms_utility.format_error_stack ||stb$typedef.crlf || dbms_utility.format_error_backtrace  );
    isr$trace.stat('end','end from exceptionhandling exfirstform', sCurrentName);
    RETURN oErrorObj;
  WHEN exaudit THEN
    oErrorObj.sErrorCode := 'exAudit';
    oErrorObj.handleError (stb$typedef.cnaudit, sMsg, sCurrentName, dbms_utility.format_error_stack ||stb$typedef.crlf || dbms_utility.format_error_backtrace  );
    isr$trace.stat('end','end from exceptionhandling exAudit', sCurrentName);
    RETURN oErrorObj;
  WHEN exNoEsig THEN
    oErrorObj.sErrorCode := utd$msglib.getmsg ('exNoEsig', stb$security.getCurrentLanguage);
    sMsg := utd$msglib.getmsg ('exNoEsigText', stb$security.getCurrentLanguage);
    oErrorObj.handleError (stb$typedef.cnseveritymessage, sMsg, sCurrentName, dbms_utility.format_error_stack ||stb$typedef.crlf || dbms_utility.format_error_backtrace  );
    RETURN oErrorObj;
  WHEN exNoDocument THEN
    sMsg := utd$msglib.getmsg ('exNoDocumentText', stb$security.getCurrentLanguage);
    oErrorObj.handleError (stb$typedef.cnseveritymessage, sMsg, sCurrentName, dbms_utility.format_error_stack ||stb$typedef.crlf || dbms_utility.format_error_backtrace  );
    RETURN oErrorObj;
  WHEN exDocCheckedout THEN
    sMsg := utd$msglib.getmsg ('exDocCheckedoutText', stb$security.getCurrentLanguage)||' \\'||smachine||'\'||spath||' by '||soracleuser;
    oErrorObj.handleError (stb$typedef.cnseveritymessage, sMsg, sCurrentName, dbms_utility.format_error_stack ||stb$typedef.crlf || dbms_utility.format_error_backtrace  );
   RETURN oErrorObj;
  WHEN exNoAudit THEN
    sMsg := utd$msglib.getmsg ('exNoAuditText', stb$security.getCurrentLanguage);
    oErrorObj.handleError (stb$typedef.cnseveritymessage, sMsg, sCurrentName, dbms_utility.format_error_stack ||stb$typedef.crlf || dbms_utility.format_error_backtrace  );
    RETURN oErrorObj;
  WHEN exNotExists THEN
    sMsg := utd$msglib.getmsg ('exNotExistsText', stb$security.getCurrentLanguage);
    oErrorObj.handleError (stb$typedef.cnseveritymessage, sMsg, sCurrentName, dbms_utility.format_error_stack ||stb$typedef.crlf || dbms_utility.format_error_backtrace  );
    RETURN oErrorObj;
  WHEN exServerNotRunning THEN -- 7.Jun 2019 hr added for [ISRC-1119]
    sMsg := utd$msglib.getmsg ('exServerNotRunningText', stb$security.getCurrentLanguage, sNotRunning);
    oErrorObj.handleError (stb$typedef.cnseveritymessage, sMsg, sCurrentName, dbms_utility.format_error_stack ||stb$typedef.crlf || dbms_utility.format_error_backtrace  );
    RETURN oErrorObj;
  WHEN exManipulation THEN
    sMsg := utd$msglib.getmsg ('exManipulationText', stb$security.getCurrentLanguage);
    oErrorObj.sSeverityCode := stb$typedef.cnseveritymessage;
    oErrorObj.handleError (stb$typedef.cnseveritymessage, sMsg, sCurrentName, dbms_utility.format_error_stack ||stb$typedef.crlf || dbms_utility.format_error_backtrace  );
    ROLLBACK;
    RETURN oErrorObj;
  WHEN OTHERS THEN
  ROLLBACK;
    sMsg := utd$msglib.getmsg ('ERROR_PLACE', stb$security.getCurrentLanguage) || ' ISR$REPORTTYPE$PACKAGE.callFunction';
    oErrorObj.sSeverityCode := stb$typedef.cnSeverityWarning;
    oErrorObj.handleError (stb$typedef.cnSeverityWarning, sMsg, sCurrentName, dbms_utility.format_error_stack ||stb$typedef.crlf || dbms_utility.format_error_backtrace  );
    RETURN oErrorObj;
END callfunction;

--******************************************************************************
FUNCTION getLov (sEntity IN VARCHAR2, ssuchwert IN VARCHAR2, oSelectionList OUT isr$tlrselection$list)
  RETURN stb$oerror$record
IS
  sCurrentName                  CONSTANT VARCHAR2(100) := $$PLSQL_UNIT||'.getLov('||null||')';
  oErrorObj                     stb$oerror$record := STB$OERROR$RECORD();
  shandledby                    stb$reptypeparameter.handledby%TYPE;

  CURSOR cGetMasterTemplates IS
    SELECT DISTINCT
           f.fileid
         , t.templatename display
         ,    utd$msglib.getmsg ('VERSION', stb$security.getCurrentLanguage)
           || t.version
           || CASE WHEN t.release = 'T' THEN utd$msglib.getmsg ('RELEASED', stb$security.getCurrentLanguage) END
           || utd$msglib.getmsg ('TEMPLATEFILE', stb$security.getCurrentLanguage)
           || f.filename
              description
         , t.version
         , t.release
         , t.icon
      FROM isr$file f
         , (SELECT t.*
              FROM (SELECT templatename, NVL (release, 'F') release, MAX (version) version
                      FROM isr$template
                    GROUP BY templatename, release) t1, isr$template t
             WHERE t.templatename = t1.templatename
               AND t.version = t1.version) t
         , isr$output$file iof
         , isr$report$output$type rot
         , (SELECT reporttypeid
                 , STB$SECURITY.checkGroupRight ('SHOW_IN_EXPLORER', reporttypeid) GRANT_SHOW_IN_EXPLORER
                 , STB$SECURITY.checkGroupRight ('CAN_CREATE_NEW_REPORT', reporttypeid) GRANT_CAN_CREATE_NEW_REPORT
                 , STB$SECURITY.checkGroupRight ('SHOW_UNRELEASED_TEMPLATES', reporttypeid) GRANT_SHOW_UNRELEASED_TEMPL
              FROM stb$reporttype
             WHERE activetype = 1) r
     WHERE (rot.reporttypeid = STB$OBJECT.getCurrentReporttypeId
         OR STB$OBJECT.getCurrentReporttypeId IS NULL)
       AND rot.VISIBLE = 'T'
       AND rot.reporttypeid = r.reporttypeid
       AND f.usage IN ('TEMPLATE', 'MASTERTEMPLATE')
       AND iof.fileid = t.fileid
       AND iof.outputid = rot.outputid
       AND t.fileid = f.fileid
       AND GRANT_SHOW_IN_EXPLORER = 'T'
       AND GRANT_CAN_CREATE_NEW_REPORT = 'T'
       AND (t.release = 'F' AND GRANT_SHOW_UNRELEASED_TEMPL = 'T' OR t.release = 'T')
       AND CASE WHEN STB$SECURITY.checkGroupRight ('TEMPLATE' || t.fileid, rot.reporttypeid) = 'T' THEN t.VISIBLE ELSE 'F' END = 'T'
    ORDER BY 2, 4;

  CURSOR cgethandledby (csEntity IN VARCHAR2) IS
    SELECT handledby
      FROM stb$reptypeparameter
     WHERE parametername = csEntity
       AND reporttypeid = stb$object.getCurrentReporttypeid;
BEGIN
  isr$trace.stat('begin','begin', sCurrentName);
  OPEN cgethandledby (sEntity);
  FETCH cgethandledby
  INTO  shandledby;
  CLOSE cgethandledby;
  isr$trace.debug ('sHandledBy', shandledby || ' ' || sEntity || ' ' || stb$object.getcurrentreporttypeid, sCurrentName);

  CASE
    --***************************************
    WHEN upper (sEntity) = 'MASTERTEMPLATE' THEN
      oSelectionList := isr$tlrselection$list ();
      FOR rGetMasterTemplates IN cGetMasterTemplates LOOP
        oSelectionList.EXTEND;
        oSelectionList (oSelectionList.count()) := ISR$OSELECTION$RECORD(rGetMasterTemplates.display, rGetMasterTemplates.fileid, rGetMasterTemplates.description, null, oSelectionList.COUNT());

        SELECT ISR$ATTRIBUTE$RECORD (sParameter, sValue, sPrepresent)
          BULK COLLECT INTO oSelectionList (oSelectionList.COUNT ()).tloAttributeList
          FROM (SELECT 'ICON' sParameter
                     , rGetMasterTemplates.icon sValue
                     , NULL sPrepresent
                  FROM DUAL);
      END LOOP;
    --***************************************
    WHEN upper (sEntity) = 'OFFSHOOT' THEN
      oErrorObj := getoffshootlist (ssuchwert, oSelectionList);
    --***************************************
    WHEN trim (shandledby) IS NOT NULL THEN
      EXECUTE IMMEDIATE
      'DECLARE
         olocError STB$OERROR$RECORD;
       BEGIN
         olocError :=' || shandledby || '.getLov(:1,:2,:3);
         :4 :=  olocError;
       END;'
      using IN sEntity, IN ssuchwert, OUT oSelectionList, OUT oErrorObj;
    --***************************************
    ELSE
      EXECUTE IMMEDIATE
      'DECLARE
         olocError STB$OERROR$RECORD;
       BEGIN
         olocError :='|| stb$util.getcurrentcustompackage || '.getLov(:1,:2,:3);
         :4 :=  olocError;
       END;'
      using IN CASE
                WHEN upper (sEntity) = 'DATAGROUP' THEN 'DATAGROUP_ONLY_USER'
                WHEN upper (sEntity) like 'HEADLINEPOS_COMMON_%' THEN 'HEADLINEPOS_COMMON'
                ELSE sEntity END
          , IN ssuchwert
          , OUT oSelectionList
          , OUT oErrorObj;
  END CASE;

  IF sSuchWert IS NOT NULL AND oselectionlist.first IS NOT NULL THEN
    isr$trace.debug ('sSuchWert',  sSuchWert, sCurrentName);
    FOR i IN 1..oSelectionList.count() LOOP
      IF oSelectionList(i).sValue = sSuchWert THEN
        isr$trace.debug ('oSelectionList('||i||').sSelected', oSelectionList(i).sValue, sCurrentName);
        oSelectionList(i).sSelected := 'T';
      END IF;
    END LOOP;
  END IF;

  isr$trace.stat('end','end', sCurrentName);
  RETURN oErrorObj;
EXCEPTION
  WHEN OTHERS THEN
     oErrorObj.handleError ( Stb$typedef.cnSeverityMessage, utd$msglib.getmsg ('exUnexpectedError', stb$security.getCurrentLanguage,  csP1 => sqlerrm(sqlcode)),
        sCurrentName, dbms_utility.format_error_stack ||chr(10)|| dbms_utility.format_error_backtrace );
    RETURN oErrorObj;
END getlov;

--******************************************************************************
FUNCTION setCurrentNode (oSelectedNode IN stb$treenode$record)
  RETURN stb$oerror$record
IS
  sCurrentName                  CONSTANT VARCHAR2(200) := $$PLSQL_UNIT||'.setCurrentNode('||oSelectedNode.snodetype||')';
  oErrorObj                     stb$oerror$record := STB$OERROR$RECORD();
  nReporttypeid   STB$REPORT.REPORTTYPEID%TYPE;
  sSrNumber       STB$REPORT.SRNUMBER%TYPE;
  nTemplate       STB$REPORT.TEMPLATE%TYPE;

  CURSOR cGetReportInfo (csTitle varchar2) IS
    select reporttypeid, srnumber
      from stb$report
     where title = csTitle;

  CURSOR cGetReportTemplate (cnRepID Number) IS
    select template
      from stb$report
     where repid = cnRepID;

BEGIN
  isr$trace.stat('begin','begin', sCurrentName);

  stb$object.destroyoldvalues;
  stb$object.setcurrentnode (oSelectedNode);
  stb$object.setcurrentreptypepackage ('ISR$REPORTTYPE$PACKAGE');

  isr$trace.debug ('oSelectedNode.snodetype', oSelectedNode.snodetype, sCurrentName);

  CASE
    --=========================REPORTTYPERIGHT/TESTUNITFRONTEND=========================
  WHEN oSelectedNode.snodetype = 'REPORTTYPE' OR oSelectedNode.snodetype = 'TESTUNITFRONTEND' THEN
      stb$object.setcurrentreporttypeid (stb$object.getNodeIdForNodeType('REPORTTYPE'));
    --=========================SRNUMBER=========================
  WHEN oSelectedNode.snodetype = 'SRNUMBER' THEN
      stb$object.setcurrentreporttypeid (stb$object.getNodeIdForNodeType('REPORTTYPE'));
      stb$object.setcurrentsrnumber (stb$object.getNodeIdForNodeType('SRNUMBER'));
    --=========================TITLE=========================
  WHEN oSelectedNode.snodetype = 'TITLE' THEN
      OPEN cGetReportInfo(stb$object.getNodeIdForNodeType('TITLE'));
      FETCH cGetReportInfo INTO nReporttypeid, sSrNumber;
      CLOSE cGetReportInfo;
      stb$object.setcurrentreporttypeid (nReporttypeid);
      stb$object.setcurrentsrnumber (sSrNumber);
      stb$object.setcurrenttitle (stb$object.getNodeIdForNodeType('TITLE'));
    --=========================REPORT=========================
  WHEN oSelectedNode.snodetype = 'REPORT' THEN
      OPEN cGetReportInfo(stb$object.getNodeIdForNodeType('TITLE'));
      FETCH cGetReportInfo INTO nReporttypeid, sSrNumber;
      CLOSE cGetReportInfo;
      OPEN cGetReportTemplate( TO_NUMBER(stb$object.getNodeIdForNodeType('REPORT')));
      FETCH cGetReportTemplate INTO nTemplate;
      CLOSE cGetReportTemplate;
      stb$object.setcurrentreporttypeid (nReporttypeid);
      stb$object.setcurrentsrnumber (sSrNumber);
      stb$object.setcurrenttitle (stb$object.getNodeIdForNodeType('TITLE'));
      stb$object.setcurrentrepid (stb$object.getNodeIdForNodeType('REPORT'));
      stb$object.setcurrenttemplate (nTemplate);
    --=========================DOCUMENT/RAWDATA=========================
  WHEN oSelectedNode.snodetype = 'DOCUMENT'
    OR oSelectedNode.snodetype = 'RAWDATA'
    OR oSelectedNode.snodetype = 'REMOTESNAPSHOT' THEN
      -- exception will happen if a attachment on any node (exception for srnumber or report) is chosen
      BEGIN
        OPEN cGetReportInfo(stb$object.getNodeIdForNodeType('TITLE'));
        FETCH cGetReportInfo INTO nReporttypeid, sSrNumber;
        CLOSE cGetReportInfo;
        OPEN cGetReportTemplate( TO_NUMBER(stb$object.getNodeIdForNodeType('REPORT')));
        FETCH cGetReportTemplate INTO nTemplate;
        CLOSE cGetReportTemplate;
        stb$object.setcurrentreporttypeid (nReporttypeid);
        stb$object.setcurrentsrnumber (sSrNumber);
        stb$object.setcurrenttitle (stb$object.getNodeIdForNodeType('TITLE'));
        stb$object.setcurrentrepid (stb$object.getNodeIdForNodeType('REPORT'));
        stb$object.setcurrenttemplate (nTemplate);
        stb$object.setcurrentdocid (stb$object.getNodeIdForNodeType(oSelectedNode.sNodeType));
      EXCEPTION
        WHEN OTHERS THEN
          stb$object.setcurrentdocid (stb$object.getNodeIdForNodeType(oSelectedNode.sNodeType));
      END;
    --=========================COMMONDOC=========================
  WHEN oSelectedNode.snodetype = 'COMMONDOC' THEN
      stb$object.setcurrentdocid (stb$object.getNodeIdForNodeType(oSelectedNode.sNodeType));
    --=========================REPORTTYPERIGHTS=========================
  WHEN STB$OBJECT.getCurrentNodetype(-2) LIKE 'REPORTTYPERIGHTS%' THEN
      stb$object.setcurrentreporttypeid (REPLACE (STB$OBJECT.getCurrentNodeid(-2), 'REPORTTYPERIGHTS'));
    --=========================REPORTTYPERIGHTS=========================
  WHEN STB$OBJECT.getCurrentNodetype(-1) LIKE 'REPORTTYPERIGHTS%' THEN
      stb$object.setcurrentreporttypeid (REPLACE (STB$OBJECT.getCurrentNodeid(-1), 'REPORTTYPERIGHTS'));
    --=========================REPORTTYPERIGHTS=========================
  WHEN oSelectedNode.snodetype LIKE 'REPORTTYPERIGHTS%' THEN
      stb$object.setcurrentreporttypeid (REPLACE (STB$OBJECT.getCurrentNodeid, 'REPORTTYPERIGHTS'));
    --=========================CURRENTTEMPLATE=========================
  WHEN  oSelectedNode.snodetype = 'CURRENTTEMPLATE' THEN
      stb$object.setcurrentreporttypeid (REPLACE (stb$object.getNodeIdForNodeType('ENTITYREPORTTYPE'), 'ENTITYREPORTTYPE'));
    --=========================ENTITYREPORTTYPE=========================
  WHEN oSelectedNode.snodetype LIKE 'ENTITYREPORTTYPE%' THEN
      stb$object.setcurrentreporttypeid (REPLACE (STB$OBJECT.getCurrentNodeid, 'ENTITYREPORTTYPE'));
    --=========================NOT DEFINED=========================
  ELSE
      isr$trace.debug ('not defined',  oSelectedNode.sNodeType, sCurrentName);
  END CASE;

  isr$trace.stat('end','end', sCurrentName);
  RETURN (oErrorObj);
EXCEPTION
  WHEN OTHERS THEN
    ROLLBACK;
     oErrorObj.handleError ( Stb$typedef.cnSeverityCritical, utd$msglib.getmsg ('exUnexpectedError', stb$security.getCurrentLanguage,  csP1 => sqlerrm(sqlcode)),
        sCurrentName, dbms_utility.format_error_stack ||chr(10)|| dbms_utility.format_error_backtrace );
    RETURN (oErrorObj);
END setcurrentnode;

--******************************************************************************
FUNCTION getNodeList (oSelectedNode IN stb$treenode$record, olnodelist OUT stb$treenodelist)
  RETURN stb$oerror$record
IS
  sCurrentName                  CONSTANT VARCHAR2(100) := $$PLSQL_UNIT||'.getNodeList('||null||')';
  oErrorObj                     stb$oerror$record := STB$OERROR$RECORD();
  olparameter                   stb$property$list;
  sstatement                    VARCHAR2 (4000);
  nrepid                        STB$REPORT.REPID%TYPE;
  sdocid                        stb$doctrail.docid%TYPE;
  sdocversion                   stb$doctrail.docversion%TYPE;
  sfilename                     stb$doctrail.filename%TYPE;
  scheckedout                   stb$doctrail.checkedout%TYPE;
  sdoctype                      stb$doctrail.doctype%TYPE;
  sdoc_definition               stb$doctrail.doc_definition%TYPE;
  sjobflag                      stb$doctrail.job_flag%TYPE;
  sfulldoctype                  VARCHAR2 (500);
  sdefaultaction                VARCHAR2 (500);
  sshowdocversionsseparately    VARCHAR2 (500);
  TYPE tcurrentvalues IS ref CURSOR;

  cgetcurrentvalues             tcurrentvalues;

  cursor cGetRepStatus(cnRepid IN NUMBER) is
    select status
      from stb$report
     where repid = cnRepid;

  sStatus                       STB$REPORT.STATUS%TYPE;

  sNodeType             VARCHAR2(500);
  sNodeIdFunctionGroup  VARCHAR2(500);
  nReporttypeId         NUMBER;
BEGIN
  isr$trace.stat('begin','begin', sCurrentName);

  CASE
      --=========================REPORTTYPE=========================
    WHEN oSelectedNode.snodetype IN ('REPORTTYPE', 'SRNUMBER', 'TITLE') THEN

      oErrorObj := GetNodeChilds (oSelectedNode, olNodeList);

      --=========================REPORT=========================
    WHEN oSelectedNode.snodetype = 'REPORT' THEN

      nrepid := to_number (STB$OBJECT.getCurrentNodeId);
      olNodeList := STB$TREENODELIST ();

      if nRepId is not null then  --ISRC-1245
        isr$trace.debug ('nrepid',  nrepid, sCurrentName);

        olNodeList.EXTEND;
        olNodeList (1) := STB$TREENODE$RECORD(null, 'ISR$REPORTTYPE$PACKAGE', null,
                                              null, null, 'CAN_SELECT', tloValueList => oSelectedNode.tloValueList);

        sStatement := 'select nodename, nodeid, nodetype, icon, defaultaction, docid, versiondisplayed, doctype, doc_definition, checkedout, modifiedby, modifiedon from (';

        sStatement := sStatement ||
                      ' select filename nodename
                             , docid nodeid
                             , DECODE(doc.doctype,''C'',''COMMONDOC'',''D'',''DOCUMENT'',''X'',''RAWDATA'',''L'',''LINK'',''S'',''REMOTESNAPSHOT'',''DOCUMENT'') nodetype
                             , DECODE(doc.doctype,''C'',''COMMONDOC'',''D'',''DOCUMENT'',''X'',''RAWDATA'',''L'',''LINK'',''S'',''REMOTESNAPSHOT'',''DOCUMENT'') || ''_'' ||
                               case when doc_definition is not null and job_flag is null and checkedout = ''F'' and doctype != ''C'' then doc_definition
                                    when doc_definition is not null and job_flag is null and checkedout = ''F'' and doctype = ''C'' then ''document_pdf'' end ||
                               case when job_flag is not null then job_flag else case when checkedout = ''T'' then checkedout end end icon
                             , DECODE(doc.doctype,''C'',''CAN_DISPLAY_COMMON_DOC'',''D'',''CAN_DISPLAY_REPORT'',''X'',''CAN_DISPLAY_RAWDATA'',''L'',''CAN_DISPLAY_LINK'',''S'',''CAN_DISPLAY_REMOTESNAPSHOT'',''CAN_DISPLAY_REPORT'') defaultaction
                             , docid
                             , versiondisplayed
                             , DECODE (doctype, ''C'',''COMMONDOC'', ''D'', ''DOCUMENT'', ''X'', ''RAWDATA'', ''L'', ''LINK'', ''S'', ''REMOTESNAPSHOT'', ''DOCUMENT'') doctype
                             , doc_definition
                             , checkedout
                             , modifiedby
                             , TO_CHAR (modifiedon, '''||sSystemDateFormat||' HH24:MI:SS'') modifiedon
                             , ''1'' || DECODE(doc.doctype,''C'',''COMMONDOC'',''D'',''DOCUMENT'',''X'',''RAWDATA'',''L'',''LINK'',''S'',''REMOTESNAPSHOT'',''DOCUMENT'')
                               || doc_definition || docid || versiondisplayed ordered
                          FROM STB$DOCTRAIL doc
                         WHERE nodeid = TO_CHAR(' || nrepid || ') ';
        IF nvl(STB$USERSECURITY.getUserParameter (stb$security.getCurrentUser, 'SHOW_REPORT_FILES'), 'F') = 'T' THEN
          sStatement := sStatement
                    || 'UNION
                        SELECT filename nodename
                             , entryid nodeid
                             , UPPER(substr(filename, instr(filename, ''.'', -1)+1))||''_REPORT_FILE'' nodetype
                             , UPPER(substr(filename, instr(filename, ''.'', -1)+1))||''_REPORT_FILE'' icon
                             , ''CAN_DISPLAY_COMMON_DOC'' defaultaction
                             , entryid docid
                             , '''' versiondisplayed
                             , UPPER(substr(filename, instr(filename, ''.'', -1)+1))||''_REPORT_FILE'' doctype
                             , doc_definition doc_definition
                             , ''F'' checkedout
                             , modifiedby
                             , TO_CHAR (modifiedon, '''||sSystemDateFormat||' HH24:MI:SS'') modifiedon
                             , ''2'' || filename
                          FROM isr$report$file
                         WHERE repid = '||nRepid||'
                         ORDER BY ordered';
        END IF;

        sStatement := sStatement || ') order by ordered';

        isr$trace.debug('sStatement', sStatement, sCurrentName, sStatement);

        oErrorObj := STB$UTIL.getColListMultiRow(sStatement, olnodelist);
      end if;
      --=========================REPORTTYPERIGHTS=========================
    WHEN STB$OBJECT.getCurrentNodetype(-1) LIKE 'REPORTTYPERIGHTS%' THEN

      sNodeType := STB$OBJECT.getCurrentNodetype;
      isr$trace.debug ('STB$OBJECT.getCurrentNodetype', sNodeType, sCurrentName);

      nReporttypeId := STB$OBJECT.getCurrentReporttypeId;
      isr$trace.debug ('nReporttypeId', nReporttypeId, sCurrentName);

      sNodeIdFunctionGroup := STB$OBJECT.getNodeidForNodetype ('FUNCTIONGROUP');
      isr$trace.debug ('STB$OBJECT.getNodeidForNodetype (''FUNCTIONGROUP'')', sNodeIdFunctionGroup, sCurrentName);

      olNodeList := STB$TREENODELIST ();
      FOR rRights IN (SELECT DISTINCT parametername
                                    , CASE WHEN right = 'x' THEN 'T' ELSE 'F' END parametervalue
                                    , UTD$MSGLIB.getMsg(description, stb$security.getCurrentLanguage) display
                                    , substr (orderno, 1, 1) flag
                                    , orderno
                                    , UTD$MSGLIB.getMsg(grouping, stb$security.getCurrentLanguage) grouping_desc
                                    , folder
                                    , folderid
                                    , case
                                        when sNodeType = 'TEMPLATE' then  (select icon from isr$template where fileid = to_number(replace(parametername, 'TEMPLATE')))
                                        else NVL(icon, 'REPORTTYPERIGHT'||substr (orderno, 1, 1))
                                      end icon
                                    , grouping
                        FROM isr$right$configuration1
                       WHERE id = nReporttypeId
                         AND usergroupno = sNodeIdFunctionGroup
                         AND type2 = 'GROUPRIGHTS'
                         AND CASE
                                WHEN sNodeType = 'REPORTTYPERIGHT' THEN 1
                                WHEN sNodeType = 'REPORTTYPEPARAMETER' THEN 2
                                WHEN sNodeType = 'REPORTPARAMETER' THEN 3
                                WHEN sNodeType = 'TEMPLATE' THEN 4
                                WHEN sNodeType = 'WIZARDSCREEN' THEN 5
                             END = substr (orderno, 1, 1)
                      ORDER BY 6, 8, 5, 3) LOOP

        olnodelist.EXTEND;
        olnodelist (olNodeList.count()) := STB$TREENODE$RECORD(SUBSTR(rrights.display, 0, 255),
                                                               'ISR$REPORTTYPE$PACKAGE', 'REPORTTYPERIGHTLIST',
                                                               rrights.parametername||'#@#'||rrights.grouping||rrights.folder,
                                                               'REPORTTYPERIGHT'||rRights.flag, 'CAN_SWITCH_REPRIGHT',
                                                               tloValueList => oSelectedNode.tloValueList );
        STB$OBJECT.setDirectory(olNodeList (olNodeList.count()));
        -- add additional parameters to display in data area
        olparameter := stb$property$list ();
        olparameter.EXTEND;
        olparameter (olparameter.count) := STB$PROPERTY$RECORD(rrights.parametername,
                                                               utd$msglib.getmsg ('REPORTRIGHT_ENABLED', stb$security.getCurrentLanguage),
                                                               rrights.parametervalue, rrights.parametervalue,
                                                               'F', Type => 'BOOLEAN');

        IF STB$OBJECT.getCurrentNodetype = 'REPORTTYPERIGHT' THEN
          olparameter.EXTEND;
          olparameter (olparameter.count) := STB$PROPERTY$RECORD(rrights.parametername,
                                                                 utd$msglib.getmsg ('GROUPINGRIGHTS', stb$security.getCurrentLanguage),
                                                                 rrights.grouping_desc, rrights.grouping_desc,
                                                                 'F', Type => 'STRING');
          olparameter.EXTEND;
          olparameter (olparameter.count) := STB$PROPERTY$RECORD(rrights.parametername,
                                                                 utd$msglib.getmsg ('FOLDERRIGHTS', stb$security.getCurrentLanguage),
                                                                 rrights.folder, rrights.folder,
                                                                 'F', Type => 'STRING');
        END IF;

        -- add the propoerty-list to the node object
        olnodelist (olNodeList.count()).tlopropertylist := olparameter;
      END LOOP;

      --=========================ENTITYREPORTTYPE=========================
    WHEN oSelectedNode.snodetype LIKE 'ENTITYREPORTTYPE%' THEN

      if STB$OBJECT.getCurrentReporttypeid is null --ISRC-1327/ISRC-1318
      or STB$OBJECT.getCurrentReporttypeid != STB$OBJECT.getCurrentNodeId then
        STB$OBJECT.setCurrentReporttypeid(STB$OBJECT.getCurrentNodeId);
      end if;

      olNodeList := STB$TREENODELIST ();
      olNodeList.EXTEND;
      olNodeList (1) := STB$TREENODE$RECORD(Utd$msglib.GetMsg ('CURRENTTEMPLATE', Stb$security.getCurrentLanguage),
                                            'ISR$REPORTTYPE$PACKAGE', 'CURRENTTEMPLATE',
                                            '-1', '', tloValueList => oSelectedNode.tloValueList );

      sStatement := ' SELECT DISTINCT f.fileid NODEID
                           , t.templatename NODENAME
                           , t.version
                           , (select t1.templatename from isr$template t1 where t1.fileid = t.copied_from_fileid) copied_from_template
                           , f.filename
                           , t.exchange_comment
                           , t.release
                           , t.installer
                           , t.modifiedon
                           , t.modifiedby
                           , t.icon
                        FROM isr$template t
                           , isr$file f
                           , isr$output$file iof
                           , (SELECT reporttypeid, iof.fileid
                                FROM isr$report$output$type rot, isr$output$file iof
                               WHERE iof.outputid = rot.outputid
                                 AND rot.visible = ''T'') rt
                       WHERE rt.reporttypeid = '||STB$OBJECT.getCurrentReporttypeid||'
                         AND rt.fileid = t.fileid
                         AND f.fileid = t.fileid
                         AND CASE
                                WHEN STB$SECURITY.checkGroupRight (''TEMPLATE''||t.fileid, rt.reporttypeid) = ''T'' THEN NVL(t.VISIBLE, ''T'')
                                ELSE ''F''
                             END = ''T''
                         AND ( (t.release = ''T''
                            AND STB$SECURITY.checkGroupRight (''SHOW_UNRELEASED_TEMPLATES'', rt.reporttypeid) = ''F'')
                           OR STB$SECURITY.checkGroupRight (''SHOW_UNRELEASED_TEMPLATES'', rt.reporttypeid) = ''T'')
                       ORDER BY 2, 3';

      oErrorObj := Stb$util.getColListMultiRow (sStatement, olnodelist);

      --=========================NOT DEFINED=========================
    ELSE
      oErrorObj := Getnodechilds (oSelectedNode, olNodeList);
  END CASE;

  isr$trace.stat('end','end', sCurrentName);
  RETURN (oErrorObj);
EXCEPTION
  WHEN OTHERS THEN
    ROLLBACK;
     oErrorObj.handleError ( Stb$typedef.cnSeverityCritical, utd$msglib.getmsg ('exUnexpectedError', stb$security.getCurrentLanguage,  csP1 => sqlerrm(sqlcode)),
        sCurrentName, dbms_utility.format_error_stack ||chr(10)|| dbms_utility.format_error_backtrace );
    RETURN (oErrorObj);
END getnodelist;

-- ***************************************************************************************************
FUNCTION getTreeStructure(nParentNode IN ISR$CUSTOM$GROUPING.PARENTNODE%TYPE DEFAULT NULL,
                          sNodeType IN ISR$CUSTOM$GROUPING.NODETYPE%TYPE,
                          olTreeNodeList IN OUT STB$TREENODELIST) RETURN STB$OERROR$RECORD
IS
  sCurrentName                  CONSTANT VARCHAR2(100) := $$PLSQL_UNIT||'.getTreeStructure('||null||')';
  oErrorObj STB$OERROR$RECORD := STB$OERROR$RECORD();

  CURSOR cGetTree(cnParentNode IN ISR$CUSTOM$GROUPING.PARENTNODE%TYPE) IS
    SELECT Utd$msglib.GetMsg (customText, Stb$security.getCurrentLanguage)
              sDisplay
         , PACKAGE sPackageType
         , nodetype sNodeType
         , nodeid sNodeid
         , NVL(reporttypeid, 0) reporttypeid
         , icon sIcon
      FROM isr$custom$grouping, stb$reporttype
     WHERE parentnode = NVL (cnParentNode, -4711)
       AND nodeid <= -50 -- sorry
       AND nodetype LIKE reporttypename(+) || '%'
    ORDER BY orderno;

BEGIN
  isr$trace.stat('begin','begin', sCurrentName);

  FOR rGetTree IN cGetTree(nParentNode) LOOP
    olTreeNodeList.EXTEND;
    olTreeNodeList (olTreeNodeList.Count()) := STB$TREENODE$RECORD(rGetTree.sDisplay,
                                                                   STB$OBJECT.getCurrentNodePackage, sNodetype,
                                                                   rGetTree.reporttypeid, rGetTree.sIcon,
                                                                   tloValueList => STB$OBJECT.getCurrentValueList);
    STB$OBJECT.setDirectory(olTreeNodeList (olTreeNodeList.Count()), rGetTree.sNodeType, rGetTree.sNodeId);
  END LOOP;

  isr$trace.stat('end','end', sCurrentName);
  RETURN oErrorObj;

EXCEPTION
  WHEN OTHERS THEN
     oErrorObj.handleError ( Stb$typedef.cnSeverityCritical, utd$msglib.getmsg ('exUnexpectedError', stb$security.getCurrentLanguage,  csP1 => sqlerrm(sqlcode)),
        sCurrentName, dbms_utility.format_error_stack ||chr(10)|| dbms_utility.format_error_backtrace );
    RETURN oErrorObj;
END getTreeStructure;

--******************************************************************************
FUNCTION getNodeChilds (oSelectedNode IN stb$treenode$record, olchildnodes OUT stb$treenodelist)
  RETURN stb$oerror$record
IS
  sCurrentName        CONSTANT VARCHAR2(100) := $$PLSQL_UNIT||'.getNodeChilds('||null||')';
  oErrorObj           stb$oerror$record := STB$OERROR$RECORD();
  exChildDisplayError EXCEPTION;
  nreptypeid          STB$REPORT.reporttypeid%TYPE;
  ssrnumber           STB$REPORT.srnumber%TYPE;
  sTitle              STB$REPORT.title%TYPE;

  ssql                CLOB;
  sstatement          VARCHAR2 (4000);

  sExists             VARCHAR2(1) := 'F';

  olValueList         ISR$VALUE$LIST;
  olchildnodes1       STB$TREENODELIST;

  sUserSql            VARCHAR2 (1000);

  nCurUser            NUMBER := STB$SECURITY.getCurrentUser;
  sRepTypePackage     varchar2(200);
  sNodeType             VARCHAR2(500);
  sNodeIdFunctionGroup  VARCHAR2(500);
  sShowUnitTest         VARCHAR2(500);


  function BuildPropRecPiece(csColName in varchar2, csType in varchar2) return varchar2 is
    -- Builds the STB$PROPERTY$RECORD piece
    sExpr varchar2(1000);
  begin
    sExpr := 'STB$PROPERTY$RECORD(  '''||upper(csColName)||'''
                                  , utd$msglib.getmsg ('''||upper(csColName)||''', '||stb$security.getCurrentLanguage||')
                                  , '||csColName||'
                                  , '||csColName||'
                                  , ''F'', ''F'', ''T'', ''F''
                                  ,'''||csType||''')';
    return sExpr;
  end BuildPropRecPiece;

BEGIN
  isr$trace.stat('begin','begin', sCurrentName);
  isr$trace.debug ('reporttypeid', stb$object.getcurrentreporttypeid, sCurrentName);

  olchildnodes := stb$treenodelist ();

  if STB$SECURITY.checkGroupRight('CAN_OVERRULE_REMOTE_SECURITY_CHECK') = 'F'
    and STB$UTIL.getSystemparameter('USE_REMOTE_SECURITY_PRIVS') = 'T' then
    sUserSql := '      AND (r.secid is null
                        or r.secid in (select rightskey
                                         from ISR$REMOTE$SECURITY$KEYS
                                        where userno = '||STB$SECURITY.getCurrentUser||'
                                          and sessionid = '||STB$SECURITY.getCurrentSession||'))';
  end if;

  CASE

    --=========================REPTYPERIGHTS=========================
    WHEN oSelectedNode.sNodetype IN ('REPTYPERIGHTS', 'ENTITYREPTYPE', 'REPORTTYPERIGHTS', 'ENTITYREPORTTYPE') THEN

      sNodeType := oSelectedNode.sNodetype;
      isr$trace.debug ('STB$OBJECT.getCurrentNodetype', sNodeType, sCurrentName);

      sNodeIdFunctionGroup := STB$OBJECT.getNodeidForNodetype ('FUNCTIONGROUP');
      isr$trace.debug ('STB$OBJECT.getNodeidForNodetype (''FUNCTIONGROUP'')', sNodeIdFunctionGroup, sCurrentName);

      sShowUnitTest := STB$UTIL.getSystemParameter ('SHOW_UNIT_TEST');
      isr$trace.debug ('sShowUnitTest', sShowUnitTest, sCurrentName);

      olChildNodes1 := stb$treenodelist ();

      IF oSelectedNode.sNodetype IN ('REPTYPERIGHTS', 'ENTITYREPTYPE') THEN
        -- then add nodes for all group privileges for each individual report type
        FOR rReptypes IN (SELECT reporttypeid
                               , UTD$MSGLIB.getMsg (reporttypename, stb$security.getCurrentLanguage) reporttypename
                            FROM stb$reporttype
                           WHERE activetype = 1
                             AND ISR$LICENCEFILE.isLicensed (plugin) = 'T'
                             AND (((SELECT COUNT ( * )
                                    FROM isr$right$configuration1
                                   WHERE id = reporttypeid
                                     AND to_char(usergroupno) = sNodeIdFunctionGroup
                                     AND type2 = 'GROUPRIGHTS') > 0 AND sNodeType IN ('REPTYPERIGHTS'))
                                   OR sNodeType IN ('ENTITYREPTYPE'))
                             AND ( (reporttypeid = 999 AND sShowUnitTest = 'T')
                               OR (reporttypeid != 999 AND (STB$SECURITY.checkGroupRight ('SHOW_IN_EXPLORER', reporttypeid) = 'T' AND sNodeType = 'ENTITYREPTYPE' OR sNodeType = 'REPTYPERIGHTS')))
                          ORDER BY Reporttypeid) LOOP
          olChildNodes.EXTEND;
          olChildNodes (olChildNodes.count()) := STB$TREENODE$RECORD(rReptypes.Reporttypename,
                                                                   case when oSelectedNode.sNodetype IN ('REPTYPERIGHTS') then 'STB$USERSECURITY' else 'ISR$REPORTTYPE$PACKAGE' end,
                                                                   case when oSelectedNode.sNodetype IN ('REPTYPERIGHTS') then 'REPORTTYPERIGHTS' else 'ENTITYREPORTTYPE' end,
                                                                   rReptypes.reporttypeid, 'REPORTTYPE' || rReptypes.Reporttypeid,
                                                                   tloValueList => oSelectedNode.tloValueList);
          STB$OBJECT.setDirectory(olChildNodes (olChildNodes.count()));
        END LOOP;

        IF oSelectedNode.sNodetype = 'ENTITYREPTYPE' THEN
          oErrorObj := getTreeStructure(NULL, 'ENTITYREPORTTYPE', olChildNodes1);
        END IF;
      ELSIF oSelectedNode.sNodetype IN ('ENTITYREPORTTYPE') THEN
        oErrorObj := getTreeStructure(STB$OBJECT.getCurrentParameterValue(), oSelectedNode.sNodetype, olChildNodes1);
      END IF;

      FOR i in 1..olchildnodes1.COUNT() LOOP
        olChildNodes.EXTEND;
        olChildNodes (olChildNodes.count()) := olchildnodes1(i);
      END LOOP;
      isr$trace.debug('olchildnodes','olchildnodes.count() '||olchildnodes.count(),sCurrentName,olchildnodes);

    --=========================REPORTTYPE=========================
    WHEN oSelectedNode.snodetype = 'REPORTTYPE' THEN
      nreptypeid := STB$OBJECT.getCurrentNodeId;
      isr$trace.debug ('nRepTypeId',  nreptypeid, sCurrentName);
      olValueList := oSelectedNode.tloValueList;
      FOR i IN 1..olValueList.count() LOOP
        IF olValueList(i).sParameterName = 'GRP$CONDITION' THEN
          sExists := 'T';
        END IF;
      END LOOP;
      IF sExists = 'F' THEN
        olValueList(olValueList.count()).sParameterName := 'GRP$CONDITION';
        olValueList(olValueList.count()).sParameterValue := 'Active';
        olValueList(olValueList.count()).sNodeType := 'VALUENODE';
      END IF;
      oErrorObj := isr$customer.getselectstatement (olValueList, sStatement);
      isr$trace.debug ('sStatement',  sstatement, sCurrentName);

      IF stb$util.getReptypeParameter (nreptypeid, 'HAS_SRNUMBER') = 'T' THEN

        ssql :=
             'select STB$TREENODE$RECORD (SDISPLAY, SPACKAGETYPE, SNODETYPE, SNODEID, SICON, null, STB$PROPERTY$LIST(
                           '||buildPropRecPiece('reporttypename', 'STRING')||',
                           '||buildPropRecPiece('srnumber', 'STRING')||',
                           '||buildPropRecPiece('reportname', 'STRING')||',
                           '||buildPropRecPiece('modifiedby', 'STRING')||',
                           '||buildPropRecPiece('modifiedon', 'STRING')||'
                         ), ISR$VALUE$LIST (), 1, null, null) '
          || 'FROM (SELECT DISTINCT r.srnumber SDISPLAY
                         , ''ISR$REPORTTYPE$PACKAGE'' SPACKAGETYPE
                         , ''SRNUMBER'' SNODETYPE
                         , r.SRNUMBER SNODEID
                         , ''SRNUMBER'' SICON
                         , (select UTD$MSGLIB.getMsg (rt.reporttypename, '||stb$security.getCurrentLanguage||') from stb$reporttype rt where r.reporttypeid = rt.reporttypeid) reporttypename
                         , r2.title
                         , case when r2.version is not null then ''Version: '' || r2.version end REPORTNAME
                         , r2.modifiedby
                         , TO_CHAR (r2.modifiedon, '''||sSystemDateFormat||' HH24:MI:SS'') modifiedon '
          || '      FROM   stb$report r, (select max(repid) repid, srnumber from stb$report group by srnumber) max_r, stb$report r2 '
          ||  CASE WHEN TRIM (sstatement) IS NOT NULL THEN ', ('||sstatement||') r1 ' END
          || '      WHERE r.reporttypeid = ' || nreptypeid || ' '
          || '        and r.srnumber = max_r.srnumber '
          || '        and max_r.repid = r2.repid '
          ||  CASE WHEN stb$security.getcurrentuser IS NOT NULL THEN
             '        AND (r.status != '||STB$TYPEDEF.cnReportAbort||' or r.status = '||STB$TYPEDEF.cnReportAbort||' and NVL(STB$SECURITY.checkGroupRight (''SHOW_ARCHIVED_IN_EXPLORER'', r.reporttypeid), ''F'') = ''T'' ) '
          || '        AND r.datagroup IN ( SELECT usergroupno '
          || '                               FROM stb$userlink '
          || '                              WHERE userno = '|| stb$security.getcurrentuser || ' ) ' END;

        IF trim (sstatement) IS NOT NULL THEN
          ssql := ssql || ' AND r.repid = r1.repid ';
        END IF;

        IF trim (sUserSql) IS NOT NULL THEN
          ssql := ssql || sUserSql;
        END IF;

        ssql := ssql || '      ORDER  BY 5, 1) ';
      ELSE

        ssql :=
             'select STB$TREENODE$RECORD (SDISPLAY, SPACKAGETYPE, SNODETYPE, SNODEID, SICON, null, STB$PROPERTY$LIST(
                           '||buildPropRecPiece('reporttypename', 'STRING')||',
                           '||buildPropRecPiece('reportname', 'STRING')||',
                           '||buildPropRecPiece('modifiedby', 'STRING')||',
                           '||buildPropRecPiece('modifiedon', 'STRING')||'
                         ), ISR$VALUE$LIST (), 1, null, null) '
          || 'FROM (SELECT r.title SDISPLAY
                         , ''ISR$REPORTTYPE$PACKAGE'' SPACKAGETYPE
                         , ''TITLE'' SNODETYPE
                         , r.title SNODEID
                        -- , (select icon from isr$template where fileid = r.template) SICON
                         , ''TITLE_'' || r.reporttypeid SICON
                         , (select UTD$MSGLIB.getMsg (rt.reporttypename, '||stb$security.getCurrentLanguage||') from stb$reporttype rt where r.reporttypeid = rt.reporttypeid) reporttypename
                         , case when r2.version is not null then ''Version: '' || r2.version end REPORTNAME
                         , r2.modifiedby
                         , TO_CHAR (r2.modifiedon, '''||sSystemDateFormat||' HH24:MI:SS'') modifiedon '
          || '      FROM   stb$report r, (select max(repid) repid, title from stb$report group by title) max_r, stb$report r2 '
          ||  CASE WHEN TRIM (sstatement) IS NOT NULL THEN ', ('||sstatement||') r1 ' END
          || '      WHERE r.reporttypeid = ' || nreptypeid || ' '
          || '        and r.title = max_r.title '
          || '        and max_r.repid = r2.repid '
          ||  CASE WHEN stb$security.getcurrentuser IS NOT NULL THEN
             '      AND (r.status != '||STB$TYPEDEF.cnReportAbort||' or r.status = '||STB$TYPEDEF.cnReportAbort||' and NVL(STB$SECURITY.checkGroupRight (''SHOW_ARCHIVED_IN_EXPLORER'', r.reporttypeid), ''F'') = ''T'' ) '
          || '      AND    r.datagroup IN ( SELECT usergroupno '
          || '                            FROM   stb$userlink '
          || '                            WHERE  userno = ' || stb$security.getcurrentuser || ' )' END;

        IF trim (sstatement) IS NOT NULL THEN
          ssql := ssql || ' AND r.repid = r1.repid ';
        END IF;

        IF trim (sUserSql) IS NOT NULL THEN
          ssql := ssql || sUserSql;
        END IF;

        ssql := ssql || '      ORDER  BY 1) ';
      END IF;

      BEGIN
        isr$trace.debug('sSQL',sSQL,sCurrentName,sSQL);
        EXECUTE IMMEDIATE ssql bulk collect into olchildnodes;

        EXECUTE IMMEDIATE
          'BEGIN
             '|| STB$UTIL.getCurrentCustomPackage  ||'.getCustomIcon(:1, :2);
           END;'
        USING IN OUT olchildnodes, IN CASE WHEN stb$util.getReptypeParameter (nreptypeid, 'HAS_SRNUMBER') = 'T' THEN 'SRNUMBER' ELSE 'TITLE' END;

        FOR i IN 1 .. olchildnodes.count () LOOP
          olchildnodes (i).tlovaluelist := STB$OBJECT.getCurrentValueList;
          STB$OBJECT.setDirectory(olchildnodes (i));
        END LOOP;
        isr$trace.debug('olchildnodes','olchildnodes.count() '||olchildnodes.count(),sCurrentName,olchildnodes);
      EXCEPTION WHEN OTHERS THEN
        isr$trace.error ('sSQL '||SQLCODE,  ssql, sCurrentName);
        RAISE exChildDisplayError;
      END;

    --=========================SRNUMBER=========================
    WHEN oSelectedNode.snodetype = 'SRNUMBER'
      OR oSelectedNode.snodetype = 'VALUENODE'
      OR oSelectedNode.snodetype like 'CUSTOM%NODE' THEN
      IF oSelectedNode.snodetype = 'SRNUMBER' THEN
        ssrnumber := STB$OBJECT.getCurrentNodeId;
        isr$trace.debug ('sSRNumber',  ssrnumber, sCurrentName);
        nreptypeid := STB$OBJECT.getCurrentNodeId(-1);
        isr$trace.debug ('nRepTypeId',  nreptypeid, sCurrentName);
      END IF;
      olValueList := oSelectedNode.tloValueList;
      FOR i IN 1..olValueList.count() LOOP
        IF olValueList(i).sParameterName = 'GRP$CONDITION' THEN
          sExists := 'T';
        END IF;
      END LOOP;
      IF sExists = 'F' THEN
        olValueList.extend;
        olValueList(olValueList.count()) := ISR$VALUE$RECORD('GRP$CONDITION', 'Active', 'VALUENODE');
      END IF;
      oErrorObj := isr$customer.getselectstatement (olValueList, sstatement);
      isr$trace.debug ('sStatement',  sstatement, sCurrentName);

      ssql :=
           'select STB$TREENODE$RECORD (SDISPLAY
                                      , SPACKAGETYPE
                                      , SNODETYPE
                                      , SNODEID
                                      , SICON
                                      , null
                                      , STB$PROPERTY$LIST(
                                           '||buildPropRecPiece('reporttypename', 'STRING')||',
                                           '||buildPropRecPiece('srnumber', 'STRING')||',
                                           '||buildPropRecPiece('reportname', 'STRING')||',
                                           '||buildPropRecPiece('modifiedby', 'STRING')||',
                                           '||buildPropRecPiece('modifiedon', 'STRING')||'
                                         )
                                      , ISR$VALUE$LIST()
                                      , 1
                                      , null
                                      , null) '
        || 'FROM (SELECT distinct r.TITLE  SDISPLAY
                       , ''ISR$REPORTTYPE$PACKAGE'' SPACKAGETYPE
                       , ''TITLE'' SNODETYPE
                       , r.TITLE SNODEID
                       --, (select icon from isr$template where fileid = r.template) SICON
                       , ''TITLE_'' || r.reporttypeid SICON
                       , (select UTD$MSGLIB.getMsg (rt.reporttypename, '||stb$security.getCurrentLanguage||') from stb$reporttype rt where r.reporttypeid = rt.reporttypeid) reporttypename
                       , r.srnumber
                       , case when r2.version is not null then ''Version: '' || r2.version else r2.title end REPORTNAME
                       , r2.modifiedby
                       , TO_CHAR (r2.modifiedon, '''||sSystemDateFormat||' HH24:MI:SS'') modifiedon '
        || '        FROM stb$report r, (select max(repid) repid, title from stb$report group by title) max_r, stb$report r2 '
        ||  CASE WHEN TRIM (sstatement) IS NOT NULL THEN ', ('||sstatement||') r1 ' END
        ||  CASE WHEN stb$security.getcurrentuser IS NOT NULL THEN
           '       WHERE r.datagroup IN ( SELECT usergroupno '
        || '                                FROM stb$userlink '
        || '                               WHERE userno = ' || stb$security.getcurrentuser || ' ) '
        || '         AND r.reporttypeid in ( select grr1.reporttypeid
                                               from stb$userlink ul1, stb$group$reptype$rights grr1
                                              where grr1.parametername = ''SHOW_IN_EXPLORER''
                                                and grr1.parametervalue = ''T''
                                                and grr1.usergroupno = ul1.usergroupno
                                                and ul1.userno = ' || stb$security.getcurrentuser || ') '
        || '         AND (r.status != '||STB$TYPEDEF.cnReportAbort||' or r.status = '||STB$TYPEDEF.cnReportAbort||' and NVL(STB$SECURITY.checkGroupRight (''SHOW_ARCHIVED_IN_EXPLORER'', r.reporttypeid), ''F'') = ''T'' ) '
           ELSE
           '       WHERE 1=1' END
        || '         and r.title = max_r.title '
        || '         and max_r.repid = r2.repid '
        || case
             when oSelectedNode.snodetype = 'SRNUMBER' then
               '     AND r.reporttypeid = ' || nreptypeid || ' '
             ||'     AND r.srnumber = ''' || REPLACE(ssrnumber, '''', '''''') || ''' '
           end;

      IF trim (sstatement) IS NOT NULL THEN
        ssql := ssql || ' AND r.repid = r1.repid ';
      END IF;

      IF trim (sUserSql) IS NOT NULL THEN
        ssql := ssql || sUserSql;
      END IF;

      ssql := ssql || '      ORDER BY 1) ';

      BEGIN
        isr$trace.debug('sSQL',sSQL,sCurrentName,sSQL);
        EXECUTE IMMEDIATE ssql bulk collect into olChildNodes;

        EXECUTE IMMEDIATE
          'BEGIN
             '|| STB$UTIL.getCurrentCustomPackage  ||'.getCustomIcon(:1, :2);
           END;'
        USING IN OUT olchildnodes, IN 'TITLE';

        FOR i IN 1 .. olchildnodes.count () LOOP
          olchildnodes (i).tlovaluelist := STB$OBJECT.getCurrentValueList;
          STB$OBJECT.setDirectory(olchildnodes (i));
        END LOOP;
        isr$trace.debug('olchildnodes','olchildnodes.count() '||olchildnodes.count(),sCurrentName,olchildnodes);
      EXCEPTION WHEN OTHERS THEN
        isr$trace.error ('sSQL '||SQLCODE,  ssql, sCurrentName);
        RAISE exChildDisplayError;
      END;

    --=========================TITLE=========================
    WHEN oSelectedNode.snodetype = 'TITLE' THEN
      IF STB$OBJECT.getCurrentNodetype(-1) = 'SRNUMBER' THEN
        ssrnumber := STB$OBJECT.getCurrentNodeId(-1);
        isr$trace.debug ('sSRNumber', ssrnumber, sCurrentName);
        nreptypeid := STB$OBJECT.getCurrentNodeId(-2);
        isr$trace.debug ('nRepTypeId',  nreptypeid, sCurrentName);
      END IF;
      sTitle := STB$OBJECT.getCurrentNodeId;
      isr$trace.debug ('sTitle',  sTitle, sCurrentName);
      olValueList := oSelectedNode.tloValueList;
      FOR i IN 1..olValueList.count() LOOP
        IF olValueList(i).sParameterName = 'GRP$CONDITION' THEN
          sExists := 'T';
        END IF;
      END LOOP;
      IF sExists = 'F' THEN
        olValueList(olValueList.count()).sParameterName := 'GRP$CONDITION';
        olValueList(olValueList.count()).sParameterValue := 'Active';
        olValueList(olValueList.count()).sNodeType := 'VALUENODE';
      END IF;
      oErrorObj := isr$customer.getselectstatement (olValueList, sstatement);
      isr$trace.debug ('sStatement',  sstatement, sCurrentName);

      sRepTypePackage := nvl(STB$UTIL.getReptypeParameter (stb$util.getReportTypeForTitle(sTitle), 'REP_TYPE_PACKAGE'),'ISR$REPORTTYPE$PACKAGE');
      isr$trace.debug ('sRepTypePackage',  sRepTypePackage, sCurrentName);
      ssql :=
           'select STB$TREENODE$RECORD (SDISPLAY, SPACKAGETYPE, SNODETYPE, SNODEID, SICON, null, STB$PROPERTY$LIST(
                           '||buildPropRecPiece('reporttypename', 'STRING')||',
                           '||buildPropRecPiece('srnumber', 'STRING')||',
                           '||buildPropRecPiece('repid', 'STRING')||',
                           '||buildPropRecPiece('reportname', 'STRING')||',
                           '||buildPropRecPiece('modifiedby', 'STRING')||',
                           '||buildPropRecPiece('modifiedon', 'STRING')||'
                         ), ISR$VALUE$LIST (), 1, null, null) '
        || 'FROM (SELECT case when version is null then title else ''Version: '' || version end SDISPLAY
                       , ''' || sRepTypePackage ||''' SPACKAGETYPE
                       , ''REPORT'' SNODETYPE
                       , r.REPID SNODEID
                       , ''REPORT_''||STATUS SICON
                       , (select UTD$MSGLIB.getMsg (rt.reporttypename, '||stb$security.getCurrentLanguage||') from stb$reporttype rt where r.reporttypeid = rt.reporttypeid) reporttypename
                       , r.srnumber
                       , r.repid
                       , case when r.version is not null then ''Version: '' || r.version else r.title end reportname
                       , r.modifiedby
                       , to_char(r.modifiedon, '''||sSystemDateFormat||' HH24:MI:SS'') modifiedon '
        || '        FROM stb$report r '
        ||  CASE WHEN TRIM (sstatement) IS NOT NULL THEN ', ('||sstatement||') r1 ' END
        || '       WHERE title = ''' || REPLACE(sTitle, '''', '''''') || ''' '
        ||  CASE WHEN stb$security.getcurrentuser IS NOT NULL THEN
           '         AND (status != '||STB$TYPEDEF.cnReportAbort||' or status = '||STB$TYPEDEF.cnReportAbort||' and NVL(STB$SECURITY.checkGroupRight (''SHOW_ARCHIVED_IN_EXPLORER'', r.reporttypeid), ''F'') = ''T'' ) '
        || '         AND datagroup IN ( SELECT usergroupno '
        || '                              FROM stb$userlink '
        || '                             WHERE userno = ' || stb$security.getcurrentuser || ' ) ' end
        || case
             when STB$OBJECT.getCurrentNodetype(-1) = 'SRNUMBER' then
               '     AND reporttypeid = ' || nreptypeid || ' '
             ||'     AND srnumber = ''' || REPLACE(ssrnumber, '''', '''''') || ''' '
           end;

      IF trim (sstatement) IS NOT NULL THEN
        ssql := ssql || ' AND r.repid = r1.repid ';
      END IF;

      IF trim (sUserSql) IS NOT NULL THEN
        ssql := ssql || sUserSql;
      END IF;

      ssql := ssql || '      ORDER BY r.REPID) ';

      BEGIN
        isr$trace.debug('sSQL',sSQL,sCurrentName,sSQL);
        EXECUTE IMMEDIATE ssql bulk collect into olchildnodes;

        EXECUTE IMMEDIATE
          'BEGIN
             '|| STB$UTIL.getCurrentCustomPackage  ||'.getCustomIcon(:1, :2);
           END;'
        USING IN OUT olchildnodes, IN 'REPORT';

        FOR i IN 1 .. olchildnodes.count () LOOP
          olchildnodes (i).tlovaluelist := STB$OBJECT.getCurrentValueList;
          STB$OBJECT.setDirectory(olchildnodes (i));
        END LOOP;
        isr$trace.debug('olchildnodes','olchildnodes.count() '||olchildnodes.count(),sCurrentName,olchildnodes);
      EXCEPTION WHEN OTHERS THEN
        isr$trace.error ('sSQL '||SQLCODE,  ssql, sCurrentName);
        RAISE exChildDisplayError;
      END;

    --=========================NOT DEFINED=========================
    ELSE
      isr$trace.debug ('not defined',  oSelectedNode.sNodeType, sCurrentName);
  END CASE;

  isr$trace.stat('end','end', sCurrentName);
  RETURN (oErrorObj);
EXCEPTION
  WHEN exChildDisplayError THEN
     oErrorObj.handleError ( Stb$typedef.cnSeverityMessage, 'Sorry, could not show you the childs.',
        sCurrentName, dbms_utility.format_error_stack ||chr(10)|| dbms_utility.format_error_backtrace );
    RETURN (oErrorObj);
  WHEN OTHERS THEN
    ROLLBACK;
     oErrorObj.handleError ( Stb$typedef.cnSeverityCritical, utd$msglib.getmsg ('exUnexpectedError', stb$security.getCurrentLanguage,  csP1 => sqlerrm(sqlcode)),
        sCurrentName, dbms_utility.format_error_stack ||chr(10)|| dbms_utility.format_error_backtrace );
    RETURN (oErrorObj);
END getnodechilds;

--******************************************************************************
FUNCTION putOffshoot (olparameter IN stb$property$list, sToken IN VARCHAR2)
  RETURN stb$oerror$record
IS
  sCurrentName                  CONSTANT VARCHAR2(100) := $$PLSQL_UNIT||'.putOffshoot('||null||')';
  oErrorObj                     stb$oerror$record := stb$oerror$record (NULL, NULL, stb$typedef.cnNoError);
  nlineindex                    NUMBER;
  scomp1                        VARCHAR2 (4000);
  scomp1a                       VARCHAR2 (4000);
  nmodified                     NUMBER := 0;
BEGIN
  isr$trace.stat('begin','begin', sCurrentName);

  oErrorObj := stb$audit.openaudit ('REPORTTYPEAUDIT', to_char (stb$object.getCurrentReporttypeid));
  oErrorObj := stb$audit.addauditrecord ( sToken );

  -- check for differences and update the group parameters
  nlineindex := olparameter.first;

  WHILE (olparameter.EXISTS (nlineindex)) LOOP
    scomp1 := nvl (ocurrentparameter (nlineindex).svalue, '');
    scomp1a := nvl (olparameter (nlineindex).svalue, '');
    isr$trace.debug ('nLineIndex', nlineindex || scomp1 || ' / ' || scomp1a, sCurrentName);

    IF (trim (scomp1) != trim (scomp1a)) THEN
      nmodified := nmodified + 1;
      -- write the audit entry
      oErrorObj := stb$audit.addauditentry (olparameter (nlineindex).sparameter, ocurrentparameter (nlineindex).svalue, olparameter (nlineindex).svalue, nmodified);
      isr$trace.debug ('parameter',  olparameter (nlineindex).sparameter, sCurrentName);
      isr$trace.debug ('value',  olparameter (nlineindex).svalue, sCurrentName);

      IF olparameter (nlineindex).svalue = 'T' THEN
        -- offshoot is added
        INSERT INTO isr$offshoot
                    (reporttypeid, offshoot_to_template, modifiedby, modifiedon)
             VALUES (stb$object.getCurrentReporttypeid, olparameter (nlineindex).sparameter, sUsername, sysdate);
      END IF;

      IF olparameter (nlineindex).svalue = 'F' THEN
        -- remove the offshoot
        DELETE FROM isr$offshoot
              WHERE reporttypeid = stb$object.getCurrentReporttypeid AND offshoot_to_template = olparameter (nlineindex).sparameter;
      END IF;
    END IF;

    nlineindex := olparameter.NEXT (nlineindex);
    scomp1 := '';
    scomp1a := '';
  END LOOP;

  isr$trace.stat('end','end', sCurrentName);
  RETURN (oErrorObj);
EXCEPTION
  WHEN OTHERS THEN
    ROLLBACK;
     oErrorObj.handleError ( Stb$typedef.cnSeverityCritical, utd$msglib.getmsg ('exUnexpectedError', stb$security.getCurrentLanguage,  csP1 => sqlerrm(sqlcode)),
        sCurrentName, dbms_utility.format_error_stack ||chr(10)|| dbms_utility.format_error_backtrace );
    RETURN (oErrorObj);
END putoffshoot;

--******************************************************************************
FUNCTION putParameters( oParameter  IN STB$MENUENTRY$RECORD,
                        olParameter IN STB$PROPERTY$LIST )
  RETURN stb$oerror$record
IS
  sCurrentName                  CONSTANT VARCHAR2(100) := $$PLSQL_UNIT||'.putParameters('||oParameter.sToken||')';
  oErrorObj                     stb$oerror$record := STB$OERROR$RECORD();
  oErrorObj1                    stb$oerror$record := STB$OERROR$RECORD();
  sMsg       VARCHAR2(2000);
  srequired                     VARCHAR2 (1) := 'F';
  exnotexists                   EXCEPTION;
  exaudit                       EXCEPTION;
  exfirstform                   EXCEPTION;
  exReporttypeJobSumError       EXCEPTION;
  sPackageName                  stb$reptypeparameter.handledby%TYPE;
  nactionid                     isr$action.actionid%TYPE;
  nOutputId                     NUMBER;
  onode                         stb$treenode$record;
  nnewdocid                     NUMBER;
  sAuditFlag                    VARCHAR2(1);
  nReporttypeSum                NUMBER;
  nJobSum                       NUMBER;
  nDocId                        NUMBER;
  scheckedout                   VARCHAR2 (1) := 'F';

  oNewNode                      STB$TREENODE$RECORD;

  CURSOR cGetNodeInfo IS
    SELECT nodetype, nodeid
      FROM stb$doctrail
     WHERE docid = STB$OBJECT.getCurrentDocid;

  rGetNodeInfo cGetNodeInfo%rowtype;

  csMimeType                    STB$DOCTRAIL.MIMETYPE%TYPE;

  cursor cGetMimeType(cnDocId in STB$DOCTRAIL.DOCID%TYPE) is
    select lower(mimetype)
    from stb$doctrail
    where docid = cnDocId;

  oParameter1                   STB$MENUENTRY$RECORD := STB$MENUENTRY$RECORD();
  sValue                        STB$ESIG.REASON%TYPE;
  sDisplay                      STB$ESIG.REASONDISPLAY%TYPE;

BEGIN
  isr$trace.stat('begin','begin', sCurrentName);
  isr$trace.debug('parameter', 'oParameter', sCurrentName, oParameter);
  isr$trace.debug('parameter', 'olParameter', sCurrentName, olParameter);

  STB$OBJECT.setCurrentToken(oParameter.sToken);

  sPackageName := STB$UTIL.gethandler (upper (oParameter.sToken));
  isr$trace.debug ('variable', 'sPackageName: '||sPackageName, sCurrentName );

  IF trim (sPackageName) IS NOT NULL AND UPPER(sPackageName) != 'ISR$REPORTTYPE$PACKAGE' THEN
    EXECUTE IMMEDIATE 'DECLARE
        olocError STB$OERROR$RECORD;
      BEGIN
        olocError:=' || sPackageName || '.putParameters(:1, :2);
        :3 := olocError;
      END;'
    using  IN oparameter, IN olparameter, OUT oErrorObj;
    RETURN oErrorObj;  -- no go code
  END IF;

  if STB$OBJECT.getCurrentNodeType = 'DOCUMENT' and upper (oparameter.stoken) IN ('CAN_DISPLAY_COMMENT', 'CAN_DISPLAY_REPORT', 'CAN_SAVE_DOCUMENT')
  or STB$OBJECT.getCurrentNodeType = 'REPORT'   and upper (oparameter.stoken) IN ('CAN_CREATE_CRITERIA', 'CAN_CREATE_REPORT_VARIANT', 'CAN_DISPLAY_HEADDATA', 'CAN_DISPLAY_REPORTAUDIT')
  or STB$OBJECT.getCurrentNodeType = 'TITLE'    and upper (oparameter.stoken) IN ('CAN_DISPLAY_ESIG', 'CAN_DISPLAY_TITLE_JOBS', 'CAN_RUN_REPORT')  then
    -- unlock all reports from this usersession
    oErrorObj := stb$system.unlockreports ('and locksession = ' || to_char(stb$security.getcurrentsession));
  end if;

  -- Processing takes place in  the current package
  CASE
    ----------- modify output opbions-------------------------------------------
    ----------------------------------------------------------------------------
  WHEN upper (oparameter.stoken) = 'CAN_MODIFY_OUTPUTOPTIONS' THEN

    oErrorObj := putoutputoption(olParameter);

    IF oErrorObj.sSeverityCode != stb$typedef.cnNoError THEN
      RETURN oErrorObj;
    END IF;

    EXECUTE IMMEDIATE 'BEGIN
                         :1 :=' || STB$UTIL.getReptypeParameter(STB$OBJECT.getCurrentReporttypeId, 'PUTSAVEDIALOG') || '(:2,:3);
                       END;'
    using OUT oErrorObj, IN olParameter, IN STB$OBJECT.getCurrentToken;

    sessionUpdate;

    IF STB$OBJECT.getCurrentREPORTTYPEID != 999 THEN
      oNewNode := ISR$TREE$PACKAGE.rebuildReportNode(STB$OBJECT.getCurrentRepid, STB$OBJECT.getCurrentNode);
      ISR$TREE$PACKAGE.loadNode(oNewNode, STB$OBJECT.getCurrentNode);
      ISR$TREE$PACKAGE.selectLeftNode(oNewNode);
    END IF;

    oErrorObj := checkAuditRequired(sAuditFlag);
    isr$trace.debug('sAuditFlag',  sAuditFlag, sCurrentName);

    IF sAuditFlag = 'T' THEN
      IF Stb$util.checkPromptedAuditRequired('PROMPTED_REPORTAUDIT') = 'T' THEN
        RAISE exAudit;
      ELSE
        oErrorObj := Stb$audit.silentAudit;
        COMMIT;
        ISR$TREE$PACKAGE.sendSyncCall;
      END IF;
    ELSE
      isr$trace.debug('no audit required', sAuditFlag, sCurrentName);
      COMMIT;
      ISR$TREE$PACKAGE.sendSyncCall;
    END IF;

    --------------------- move parameters------------------------
    --------------------------------------------------------------------------
  WHEN upper (oparameter.stoken) = 'CAN_MOVE_PARAMETERS' THEN
      oErrorObj := moveparameters (olparameter);

      -- check if there have been changes
      IF (stb$audit.getmodifiedflag = 'F') THEN
        -- no changes -> free the dom and return OError;
        isr$trace.debug ('no changes',  'no changes', sCurrentName);
        oErrorObj := stb$audit.freeAuditdom;
        RETURN oErrorObj;
      END IF;

      IF stb$util.checkpromptedauditrequired ('PROMPTED_REP_TYPE_AUDIT') = 'T' THEN
        RAISE exaudit;
      -- commit will be in the audit window
      ELSE
        --Silent Audit
        -- write the silent audit and calculate the checksum
        oErrorObj := stb$audit.silentaudit;
        COMMIT;
      END IF;

    --------------------- modify report type parameter------------------------
    --------------------------------------------------------------------------
  WHEN upper (oparameter.stoken) = 'CAN_MODIFY_REPTYPE_PARAMETERS' THEN
      isr$trace.debug('value','olParameter.count: '||olParameter.count, sCurrentName);

      BEGIN
        SELECT SUM (TO_NUMBER (sValue))
          INTO nReporttypeSum
          FROM table(olParameter)
         WHERE sParameter LIKE '%JOB%';

         isr$trace.debug('value','nReporttypeSum: '||nReporttypeSum, sCurrentName);
         isr$trace.debug('value','STB$UTIL.getSystemParameter(MAX_JOBS): '||STB$UTIL.getSystemParameter('MAX_JOBS'), sCurrentName);

        SELECT SUM (TO_NUMBER (parametervalue))+nReporttypeSum-TO_NUMBER (STB$UTIL.getSystemParameter('MAX_JOBS'))
          INTO nJobSum
          FROM stb$reptypeparameter
         WHERE parametername LIKE '%JOB%'
           AND userparameter = 'F'
           AND reporttypeid != STB$OBJECT.getCurrentReporttypeid;
      EXCEPTION WHEN OTHERS THEN
        nJobSum := 0;
      END;

      isr$trace.debug('value','nJobSum: '||nJobSum, sCurrentName);
      IF nJobSum > 0 THEN
        nJobSum := nReporttypeSum-nJobSum;
        sMsg :=  utd$msglib.getmsg ('exReporttypeJobSumErrorText', stb$security.getCurrentLanguage, STB$UTIL.getSystemParameter('MAX_JOBS'));
        RAISE exReporttypeJobSumError;
      END IF;

      oErrorObj := putreptypeparameters (olparameter);

      -- check if there have been changes
      IF (stb$audit.getmodifiedflag = 'F') THEN
        -- no changes -> free the dom and return OError;
        isr$trace.debug ('no changes',  'no changes', sCurrentName);
        oErrorObj := stb$audit.freeAuditdom;
        RETURN oErrorObj;
      END IF;

      IF stb$util.checkpromptedauditrequired ('PROMPTED_REP_TYPE_AUDIT') = 'T' THEN
        RAISE exaudit;
      -- commit will be in the audit window
      ELSE
        --Silent Audit
        -- write the silent audit and calculate the checksum
        oErrorObj := stb$audit.silentaudit;
        COMMIT;
      END IF;
    --------------------------------------------------------------------------
    ---------------------------------- doc properties-------------------------
  WHEN upper (oparameter.stoken) = 'CAN_ADD_DOCPROPERTY' OR upper (oparameter.stoken) = 'CAN_ADD_DOCPROPERTY_COMMON' THEN

      oErrorObj := putdocumentproperties (olparameter);

      COMMIT;

      DECLARE
        blBlob    STB$DOCTRAIL.CONTENT%TYPE;
        sFileName STB$DOCTRAIL.FILENAME%TYPE;
        sFlagOk   VARCHAR2(1);
      BEGIN

        begin
          select content into blBlob from STB$DOCTRAIL where docid = STB$OBJECT.getCurrentDocid;
          isr$trace.debug('before unzip', DBMS_LOB.GETLENGTH(blBlob), sCurrentName);
          blBlob := STB$JAVA.unzip(blBlob);
          IF DBMS_LOB.GETLENGTH(blBlob) = 0 THEN
            blBlob := null;
          END IF;
          isr$trace.debug('after unzip', DBMS_LOB.GETLENGTH(blBlob), sCurrentName);
        exception when others then
          blBLob := null;
        end;

        UPDATE stb$doctrail
           SET content =
                  STB$JAVA.setDocumentProperties (
                     NVL (blBlob, CONTENT)
                   , (SELECT XMLELEMENT (
                                "properties"
                              , XMLAGG(XMLELEMENT (
                                          "property"
                                        , xmlattributes (parametername AS "name")
                                        , NVL (parameterdisplay, parametervalue)
                                       ))
                             ).getClobVal()
                        FROM isr$doc$property
                       WHERE docid = STB$OBJECT.getCurrentDocid)
                  )
             , modifiedon = SYSDATE
             , modifiedby = sUsername
         WHERE docid = STB$OBJECT.getCurrentDocid;
        isr$trace.debug('doc properties updated', DBMS_LOB.GETLENGTH(blBlob), sCurrentName);

        -- ZIP
        begin
          SELECT CONTENT, FILENAME INTO blBlob, sFileName FROM STB$DOCTRAIL WHERE docid = STB$OBJECT.getCurrentDocid;
          isr$trace.debug('before zip '||sFileName, DBMS_LOB.GETLENGTH(blBlob), sCurrentName);
          blBlob := STB$JAVA.zip(blBlob, sFileName);
          IF DBMS_LOB.GETLENGTH(blBlob) = 0 THEN
            blBlob := null;
          END IF;
          isr$trace.debug('after zip '||sFileName, DBMS_LOB.GETLENGTH(blBlob), sCurrentName);
        exception when others then
          blBLob := null;
        end;

        UPDATE stb$doctrail
           SET DOCSIZE = DBMS_LOB.GETLENGTH (NVL (blBlob, CONTENT))
             , CONTENT = NVL (blBlob, CONTENT)
         WHERE docid = STB$OBJECT.getCurrentDocid;

        -- calcualte the checksum
        STB$DOCKS.createChecksum(STB$OBJECT.getCurrentDocid, sFlagOk, 'B');

        delete from stb$doctrail where doc_definition = 'document_pdf' and nodeid = TO_CHAR(STB$OBJECT.getCurrentRepid) and nodetype = 'REPORT';

        COMMIT;

      EXCEPTION WHEN OTHERS THEN

        ROLLBACK;

        open cGetMimeType(STB$OBJECT.getCurrentDocid);
        fetch cGetMimeType into csMimeType;
        close cGetMimeType;

        IF csMimeType = 'application/msword' THEN

          delete from stb$doctrail where doc_definition = 'document_pdf' and nodeid = TO_CHAR(STB$OBJECT.getCurrentRepid) and nodetype = 'REPORT';

          oErrorObj := setjobparameter ('CAN_REFRESH_DOC_PROPERTIES');
          oErrorObj := stb$job.startjob;
        END IF;

      END;

    --------------------------------------------------------------------------
    --------------------------------- configure offshoot ---------------------
  WHEN upper (oparameter.stoken) = 'CAN_CONFIGURE_OFFSHOOT' THEN
      oErrorObj := putoffshoot (olparameter, upper (oparameter.stoken));

      -- check if there have been changes
      IF (stb$audit.getmodifiedflag = 'F') THEN
        -- no changes -> free the dom and return OError;
        isr$trace.debug ('no changes',  'no changes', sCurrentName);
        oErrorObj := stb$audit.freeAuditdom;
        RETURN oErrorObj;
      END IF;

      IF stb$util.checkpromptedauditrequired ('PROMPTED_REP_TYPE_AUDIT') = 'T' THEN
        RAISE exaudit;
      -- commit will be in the audit window
      ELSE
        --Silent Audit
        -- write the silent audit and calculate the checksum
        oErrorObj := stb$audit.silentaudit;
        COMMIT;
      END IF;
    --------------------------------------------------------------------------
    --------------------------------------------------------------------------
  WHEN upper (oparameter.stoken) IN ('CAN_CREATE_NEW_REP', 'CAN_CREATE_NEW_REPORT', 'CAN_CREATE_REPORT_VARIANT') THEN
      stb$object.setCurrentTemplate(to_number (olparameter (1).svalue));
      IF upper (oparameter.stoken) IN ('CAN_CREATE_NEW_REP') THEN
        onode := stb$object.getcurrentnode;
        onode.spackagetype := 'ISR$REPORTTYPE$PACKAGE';
        stb$object.setcurrentnode (onode);
        oErrorObj := createnewreport;
      ELSIF upper (oparameter.stoken) = 'CAN_CREATE_NEW_REPORT' THEN
        oErrorObj := createnewreport;
      ELSE
        oErrorObj := createnewreport (stb$typedef.cnvariation);
      END IF;

      -- call of the first from in the wizard
      oErrorObj := firstform;

      -- removed for loop and merge statement because of ISRC-1172 (Userparameter and server runstatus are already considered in sValue in
--      FOR rGetDialog IN ( SELECT COUNT ( * )
--                            FROM table (olParameter)
--                           WHERE sParameter LIKE 'SERVER_%'
--                             AND sDisplayed = 'T'
--                          HAVING COUNT ( * ) > 0) LOOP

        DELETE FROM ISR$CRIT
              WHERE entity = 'SERVER'
                AND repid = STB$OBJECT.getCurrentRepid;

--      END LOOP;

      isr$trace.debug('code position', 'before first insert into ISR$CRIT' ,sCurrentName, olParameter);

-----------
--      merge into  isr$crit  target
--      using       ( select replace (parametername, 'SERVER_') server
--                    from STB$USERPARAMETER
--                    where userno = STB$SECURITY.getCurrentUser
--                    and parametervalue = 'T'
--                    and parametername like 'SERVER_%'
--                    and not exists ( select replace (sParameter, 'SERVER_') server
--                                     FROM   table (olParameter)
--                                     WHERE  sValue = 'T'
--                                     AND    sDisplayed = 'T' --ISRC-982 and DSIS-236
--                                     AND    sParameter like 'SERVER_%') ) src
--      on          (     target.entity    = 'SERVER'
--                   and (target.key is   null or target.key      = src.server)
--                    and ( target.repid is  null or target.repid     = STB$OBJECT.getCurrentRepid)
--                    and  (target.masterkey is  null or  target.masterkey = src.server))
--      when not matched then insert( target.entity, target.key, target.display, target.info, target.ordernumber, target.selected, target.repid, target.masterkey )
--                            values('SERVER', server, server, server, 1, 'F', STB$OBJECT.getCurrentRepid, server );
--
--      isr$trace.debug('after merge', sql%rowcount ||'  before second insert into ISR$CRIT' ,sCurrentName);
-----------


      INSERT INTO ISR$CRIT (ENTITY
                          , KEY
                          , DISPLAY
                          , INFO
                          , ORDERNUMBER
                          , SELECTED
                          , REPID
                          , MASTERKEY)
         (SELECT 'SERVER'
               , server
               , server
               , server
               , 1
               , 'F'
               , STB$OBJECT.getCurrentRepid
               , server
            FROM (SELECT REPLACE (sParameter, 'SERVER_') server
                    FROM table (olParameter)
                   WHERE sValue = 'T'
                     -- removed again for ISRC-1172
                     --AND sDisplayed = 'T'  -- ISRC-982 and DSIS-236
                     AND sParameter LIKE 'SERVER_%'));
      isr$trace.debug('after insert', sql%rowcount  ,sCurrentName);

      FOR i IN 1..olparameter.COUNT LOOP
        IF NVL(olparameter(i).sDisplayed, 'T') = 'T'
        AND olparameter(i).sParameter like 'DATA_SELECTION_%'
        AND TRIM(olparameter(i).sValue) IS NOT NULL THEN
          DELETE FROM stb$reportparameter WHERE parametername = olparameter(i).sParameter AND repid = STB$OBJECT.getCurrentRepid;
          INSERT INTO stb$reportparameter
                      (parametername, parametervalue, modifiedon, modifiedby, repid)
               VALUES (olparameter(i).sParameter, olparameter(i).sValue, sysdate, sUsername, STB$OBJECT.getCurrentRepid);
        END IF;
      END LOOP;

      -- tell the frontend to go to the first form
      RAISE exfirstform;
    --------------------------------------------------------------------------
    ------------------------------------CHECKIN-------------------------------
  WHEN upper (oparameter.stoken) = 'CAN_CHECKIN'
    OR upper (oparameter.stoken) = 'CAN_CHECKIN_COMMON_DOC' THEN

      IF upper (oparameter.stoken) = 'CAN_CHECKIN' THEN
        oErrorObj := isr$reporttype$package.createsraudit (stb$object.getcurrentsrnumber, STB$UTIL.getProperty(olparameter, 'COMMENT'));
      END IF;

      UPDATE stb$doctrail
       SET job_flag = 'O'
      WHERE docid = stb$object.getcurrentdocid;
      COMMIT;

      oErrorObj := setjobparameter (upper (oparameter.stoken));
      stb$job.setjobparameter ('ACTIONTYPE', 'CHECKIN_DOCUMENT', stb$job.njobid);

      FOR i IN 1 .. olparameter.count () LOOP
        stb$job.setjobparameter (olparameter (i).sparameter, olparameter (i).svalue, stb$job.njobid);
      END LOOP;

      OPEN cGetNodeInfo;
      FETCH cGetNodeInfo INTO rGetNodeInfo;
      IF cGetNodeInfo%FOUND THEN
        STB$JOB.SETJOBPARAMETER ('NODETYPE', rGetNodeInfo.nodetype, stb$job.njobid);
        STB$JOB.SETJOBPARAMETER ('NODEID', rGetNodeInfo.nodeid, STB$JOB.nJobId);
      END IF;
      CLOSE cGetNodeInfo;

      oErrorObj := stb$job.startjob;

    ------------------------------------CAN_MODIFY_WITH_COMMON_DOC--------------
  WHEN upper (oparameter.stoken) = 'CAN_MODIFY_WITH_COMMON_DOC' THEN

      oErrorObj := setjobparameter (upper (oparameter.stoken));
      stb$job.setjobparameter ('ACTIONTYPE', 'SAVE', stb$job.njobid);

      FOR i IN 1 .. olparameter.count () LOOP
        stb$job.setjobparameter (olparameter (i).sparameter, olparameter (i).svalue, stb$job.njobid);
      END LOOP;

      OPEN cGetNodeInfo;
      FETCH cGetNodeInfo INTO rGetNodeInfo;
      IF cGetNodeInfo%FOUND THEN
        STB$JOB.SETJOBPARAMETER ('NODETYPE', rGetNodeInfo.nodetype, stb$job.njobid);
        STB$JOB.SETJOBPARAMETER ('NODEID', rGetNodeInfo.nodeid, STB$JOB.nJobId);
      END IF;
      CLOSE cGetNodeInfo;

      oErrorObj := stb$job.startjob;

    ------------------------------------MODIFY_COMMON_DOC-----------------------
  WHEN  upper (oparameter.stoken) = 'CAN_MODIFY_COMMON_DOC' THEN

      UPDATE stb$doctrail
         SET job_flag = 'O'
       WHERE docid = stb$object.getcurrentdocid;
      COMMIT;

      oErrorObj := setjobparameter (upper (oparameter.stoken));
      stb$job.setjobparameter ('ACTIONTYPE', 'MODIFY_DOCUMENT', stb$job.njobid);

      FOR i IN 1 .. olparameter.count () LOOP
        stb$job.setjobparameter (olparameter (i).sparameter, olparameter (i).svalue, stb$job.njobid);
      END LOOP;

      OPEN cGetNodeInfo;
      FETCH cGetNodeInfo INTO rGetNodeInfo;
      IF cGetNodeInfo%FOUND THEN
        STB$JOB.SETJOBPARAMETER ('NODETYPE', rGetNodeInfo.nodetype, stb$job.njobid);
        STB$JOB.SETJOBPARAMETER ('NODEID', rGetNodeInfo.nodeid, STB$JOB.nJobId);
      END IF;
      CLOSE cGetNodeInfo;

      oErrorObj := stb$job.startjob;

    --------------------------------------------------------------------------
    ------------------------------------UNDO CHECKOUT-------------------------
  WHEN upper (oparameter.stoken) = 'CAN_UNDO_CHECKOUT'
    OR upper (oparameter.stoken) = 'CAN_UNDO_CHECKOUT_COMMON_DOC' THEN

      IF upper (oparameter.stoken) = 'CAN_UNDO_CHECKOUT' THEN
        oErrorObj := isr$reporttype$package.createsraudit (stb$object.getcurrentsrnumber);
      END IF;

      oErrorObj := setjobparameter (upper (oparameter.stoken));
      stb$job.setjobparameter ('ACTIONTYPE', 'UNDOCHECKOUT', stb$job.njobid);

      FOR i IN 1 .. olparameter.count () LOOP
        stb$job.setjobparameter (olparameter (i).sparameter, olparameter (i).svalue, stb$job.njobid);
      END LOOP;

      oErrorObj := stb$job.startjob;

    --------------------------------------------------------------------------
    ------------------------------------Save document-------------------------
  WHEN upper (oparameter.stoken) in ('CAN_SAVE_DOCUMENT', 'CAN_SEND_TO_DMS') THEN

      oErrorObj := setjobparameter (upper (oparameter.stoken));
      stb$job.setjobparameter ('ACTIONTYPE', 'SAVE', stb$job.njobid);
      stb$job.setjobparameter ('DOCID', STB$OBJECT.getCurrentNodeId, stb$job.njobid);

      FOR i IN 1 .. olparameter.count () LOOP
        stb$job.setjobparameter (olparameter (i).sparameter, olparameter (i).svalue, stb$job.njobid);
      END LOOP;

      oErrorObj := stb$job.startjob;

    --------------------------------------------------------------------------
    ------------------------------------- download template-------------------
  WHEN upper (oparameter.stoken) = 'CAN_DOWNLOAD_TEMPLATE'
    OR upper (oparameter.stoken) = 'CAN_ARCHIVE_REPORTS' THEN
      oErrorObj := setjobparameter (upper (oparameter.stoken));
      stb$job.setjobparameter ('ACTIONTYPE', 'SAVE', stb$job.njobid);

      IF upper (oparameter.stoken) = 'CAN_ARCHIVE_REPORTS' THEN
        CASE
          WHEN STB$OBJECT.getCurrentNodetype = 'REPORTTYPE' THEN
            stb$job.setjobparameter ('CONDITION', 'reporttypeid = '||STB$OBJECT.getCurrentReporttypeId, stb$job.njobid);
          WHEN STB$OBJECT.getCurrentNodetype = 'SRNUMBER' THEN
            stb$job.setjobparameter ('CONDITION', 'srnumber = '''||REPLACE(STB$OBJECT.getCurrentSRNumber, '''', '''''')||'''', stb$job.njobid);
          WHEN STB$OBJECT.getCurrentNodetype = 'TITLE' THEN
            stb$job.setjobparameter ('CONDITION', 'title = '''||REPLACE(STB$OBJECT.getCurrentTitle, '''', '''''')||'''', stb$job.njobid);
          WHEN STB$OBJECT.getCurrentNodetype = 'REPORT' THEN
            stb$job.setjobparameter ('CONDITION', 'repid in ( select repid
                                                                from stb$report
                                                               start with repid = '||STB$OBJECT.getCurrentRepid||'
                                                             connect by prior parentrepid = repid )', stb$job.njobid);
        END CASE;
        oErrorObj := Stb$audit.Openaudit ('SYSTEMAUDIT', to_char (-1));
        oErrorObj := Stb$audit.Addauditrecord ( oParameter.sToken );
        oErrorObj := Stb$audit.Addauditentry (upper (oParameter.Stoken), NULL, utd$msglib.getmsg (STB$OBJECT.getCurrentNodeType, stb$security.getCurrentLanguage)||': '||STB$OBJECT.getCurrentNodeId, 1);
        oErrorObj := Stb$audit.Silentaudit;
      END IF;

      FOR i IN 1 .. olparameter.count () LOOP
        stb$job.setjobparameter (olparameter (i).sparameter, olparameter (i).svalue, stb$job.njobid);
      END LOOP;

      oErrorObj := stb$job.startjob;
    --------------------------------------------------------------------------
    ------------------------------------CHECKOUT------------------------------
  WHEN upper (oparameter.stoken) = 'CAN_CHECKOUT'
    OR upper (oparameter.stoken) = 'CAN_CHECKOUT_UNPROTECTED'
    OR upper (oparameter.stoken) = 'CAN_CHECKOUT_COMMON_DOC' THEN
      isr$trace.debug ('olParameter(1).sValue CAN_CHECKOUT',  olparameter (1).svalue, sCurrentName);

      IF upper (oparameter.stoken) != 'CAN_CHECKOUT_COMMON_DOC' THEN
        oErrorObj := isr$reporttype$package.createsraudit (stb$object.getcurrentsrnumber);
        -- copy the doc and set the new docid
        oErrorObj := stb$docks.copydoctodoc (stb$object.getcurrentdocid, stb$object.getCurrentRepid, nnewdocid);
        stb$object.setcurrentdocid (nnewdocid);
        UPDATE stb$doctrail
           SET job_flag = 'D'
         WHERE docid = nnewdocid;
        COMMIT;
      ELSE
        UPDATE stb$doctrail
           SET job_flag = 'I'
         WHERE docid = stb$object.getcurrentdocid;
        COMMIT;
      END IF;

      oErrorObj := setjobparameter (case when upper (oparameter.stoken) = 'CAN_CHECKOUT_UNPROTECTED' then 'CAN_CHECKOUT' else upper (oparameter.stoken) end);
      stb$job.setjobparameter ('ACTIONTYPE', 'SAVE', stb$job.njobid);
      stb$job.setjobparameter ('REFERENCE', stb$object.getCurrentDocid, stb$job.njobid);
      stb$job.setjobparameter ('OLDDOCID', stb$object.getCurrentNodeId, stb$job.njobid);
      stb$job.setjobparameter ('HASHVALUE', stb$object.getCurrentParameterValue, stb$job.njobid);
      IF upper (oparameter.stoken) = 'CAN_CHECKOUT_UNPROTECTED' THEN
        stb$job.setjobparameter ('USE_PROTECTION', 'F', stb$job.njobid);
      END IF;


      FOR i IN 1 .. olparameter.count () LOOP
        stb$job.setjobparameter (olparameter (i).sparameter, olparameter (i).svalue, stb$job.njobid);
      END LOOP;

      oErrorObj := stb$job.startjob;

    --------------------------------------------------------------------------
    ----------------------------------------------UPLOAD COMMON---------------
  WHEN upper (oparameter.stoken) = 'CAN_UPLOAD_COMMON_DOC'
    OR upper (oparameter.stoken) = 'CAN_CLONE_OUTPUT'
    OR upper (oparameter.stoken) = 'CAN_CREATE_NEW_OUTPUT'
    OR upper (oparameter.stoken) = 'CAN_UPLOAD_NEW_TEMPLATE'
    OR upper (oparameter.stoken) = 'CAN_EXTRACT_REPORTS' THEN

      IF upper (oparameter.stoken) = 'CAN_EXTRACT_REPORTS' THEN
        oErrorObj := Stb$audit.Openaudit ('SYSTEMAUDIT', to_char (-1));
        oErrorObj := Stb$audit.Addauditrecord ( oParameter.sToken );
        oErrorObj := Stb$audit.Addauditentry (upper (oParameter.Stoken), NULL, utd$msglib.getmsg (STB$OBJECT.getCurrentNodeType, stb$security.getCurrentLanguage)||': '||STB$OBJECT.getCurrentNodeId, 1);
        oErrorObj := Stb$audit.Silentaudit;
      END IF;

      oErrorObj := setjobparameter (case when upper (oparameter.stoken) = 'CAN_CREATE_NEW_OUTPUT' then 'CAN_CLONE_OUTPUT' else upper (oparameter.stoken) end);
      stb$job.setjobparameter ('ACTIONTYPE', 'UPLOAD', stb$job.njobid);

      FOR i IN 1 .. olparameter.count () LOOP
        stb$job.setjobparameter (olparameter (i).sparameter, olparameter (i).svalue, stb$job.njobid);
      END LOOP;

      oErrorObj := stb$job.startjob;

    --------------------------------------------------------------------------
    ------------------------------------- release template -------------------
  WHEN upper (oparameter.stoken) = 'CAN_RELEASE_MASTERTEMPLATE' THEN

      update isr$template
         set release = 'T'
       where fileid = STB$OBJECT.getCurrentNodeId;
      COMMIT;

      DECLARE
        sParameter  VARCHAR2(500);
        sValue      VARCHAR2(500);
        sDisplay    VARCHAR2(500);
      BEGIN
        FOR i IN 1..olParameter.COUNT LOOP
          IF olParameter(i).sParameter LIKE 'ESIGREASON%' THEN
            sParameter := olParameter(i).sParameter;
            sValue := NVL(olParameter(i).sValue,'CANCEL');
            sDisplay := NVL(olParameter(i).sDisplay, sValue);
          END IF;
        END LOOP;

        oErrorObj := Stb$audit.openAudit ('TEMPLATEAUDIT', stb$object.getCurrentNode().sDisplay);
        oErrorObj := Stb$audit.AddAuditRecord (upper (oparameter.stoken));
        oErrorObj := Stb$audit.AddAuditEntry ('FILEID'||': '||stb$object.getCurrentNodeId, 'Release', sParameter||': '||sDisplay, 1);
        oErrorObj := Stb$audit.silentAudit;
      END;

      ISR$TREE$PACKAGE.sendSyncCall;

    --------------------------------------------------------------------------
    ----------------------------------------------CREATE DOCUMENT-------------
  WHEN upper (oparameter.stoken) IN ('CAN_RUN_REPORT', 'CAN_RELEASE_FOR_INSPECTION' ) THEN

      IF upper (oparameter.stoken) IN ('CAN_RELEASE_FOR_INSPECTION' ) THEN
        sessionUpdate;
        isr$trace.debug('STB$UTIL.getProperty(olParameter, ''REPID'')', STB$UTIL.getProperty(olParameter, 'REPID'), sCurrentName);
        STB$OBJECT.setCurrentRepid(STB$UTIL.getProperty(olParameter, 'REPID'));
      END IF;

      oErrorObj := setJobParameter (upper (oparameter.stoken));
      if oErrorObj.isExceptionCritical then
        raise stb$typedef.exCritical;
      end if;
      stb$job.setjobparameter ('REFERENCE', stb$object.getCurrentRepid, stb$job.njobid);
      oErrorObj := Stb$util.createFilterParameter (olParameter);

      IF STB$USERSECURITY.checkoutDirect(stb$security.getcurrentuser) = 'F' THEN
        stb$job.setjobparameter ('CAN_USE_DOC_SERVER', 'T', stb$job.njobid);
      ELSE
        stb$job.setjobparameter ('ACTIONTYPE', 'SAVE', stb$job.njobid);
        stb$job.setjobparameter ('CAN_USE_DOC_SERVER', 'F', stb$job.njobid);
      END IF;

      -- unlock the reports because a scheduled job is started
      oErrorObj := stb$system.unlockreports ('and title = '''||REPLACE(STB$OBJECT.getCurrentTitle, '''', '''''')||''' and reporttypeid = ''' || stb$object.getCurrentReporttypeid || ''' and locksession = ' || to_char(stb$security.getcurrentsession));

      oErrorObj := stb$job.startjob;

      ISR$TREE$PACKAGE.sendSyncCall('REPORT', STB$OBJECT.getCurrentRepid);

    --------------------------------------------------------------------------
    ----------------------------------------------CAN_DEACTIVATE_INSPECTION---
  WHEN upper (oparameter.stoken) IN ('CAN_DEACTIVATE_INSPECTION' ) THEN

      oErrorObj := deactivateinspectionStatus;
      oErrorObj := isr$reporttype$package.createsraudit (stb$object.getcurrentsrnumber);

      sessionUpdate;

      oErrorObj := stb$docks.getmaxdocidforrepid (stb$object.getcurrentrepid, ndocid, scheckedout, STB$DOCKS.getDocType(NULL, STB$OBJECT.getCurrentRepid),STB$DOCKS.getDocDefType(NULL, STB$OBJECT.getCurrentRepid));

      open cGetMimeType(ndocid);
      fetch cGetMimeType into csMimeType;
      close cGetMimeType;

      IF csMimeType = 'application/msword' THEN
        delete from stb$doctrail where doc_definition = 'document_pdf' and nodeid = TO_CHAR(STB$OBJECT.getCurrentRepid) and nodetype = 'REPORT';
        oErrorObj := setjobparameter ('CAN_REFRESH_DOC_PROPERTIES');
        stb$job.setjobparameter ('DOCID', ndocid, stb$job.njobid);
        oErrorObj := stb$job.startjob;

        ISR$TREE$PACKAGE.selectCurrent;
      END IF;

    --------------------------------------------------------------------------
    ------------------------------------CAN_LOCK_REPORT-----------------------
  WHEN upper (oparameter.stoken) IN ('CAN_LOCK_REPORT', 'CAN_UNLOCK_REPORT') THEN

      oErrorObj := isr$reporttype$package.createsraudit (stb$object.getcurrentsrnumber);

      sessionUpdate;

      oErrorObj := stb$docks.getmaxdocidforrepid (stb$object.getcurrentrepid, ndocid, scheckedout, STB$DOCKS.getDocType(NULL, STB$OBJECT.getCurrentRepid),STB$DOCKS.getDocDefType(NULL, STB$OBJECT.getCurrentRepid));

      open cGetMimeType(ndocid);
      fetch cGetMimeType into csMimeType;
      close cGetMimeType;

      IF csMimeType = 'application/msword' THEN
        delete from stb$doctrail where doc_definition = 'document_pdf' and nodeid = TO_CHAR(STB$OBJECT.getCurrentRepid) and nodetype = 'REPORT';
        oErrorObj := setjobparameter ('CAN_REFRESH_DOC_PROPERTIES');
        stb$job.setjobparameter ('DOCID', ndocid, stb$job.njobid);
        oErrorObj := stb$job.startjob;

        ISR$TREE$PACKAGE.selectCurrent;
      END IF;

    --------------------------------------------------------------------------
    ----------------------------------CAN_FINALIZE_REPORT---------------------
  WHEN upper (oparameter.stoken) IN ('CAN_FINALIZE_REPORT', 'CAN_FINALIZE_REPORT_VERSION') THEN

      isr$trace.debug('STB$UTIL.getProperty(olParameter, ''REPID'')', STB$UTIL.getProperty(olParameter, 'REPID'), sCurrentName);
      STB$OBJECT.setCurrentRepid(STB$UTIL.getProperty(olParameter, 'REPID'));

      -- check audit required?
      oErrorObj := isr$reporttype$package.checkauditrequired (srequired);
      isr$trace.debug ('sRequired',  srequired, sCurrentName);

      -- required
      IF srequired = 'T' THEN
        -- do a silent audit
        oErrorObj := stb$audit.silentaudit;
        isr$trace.debug ('silent audit',  'silent audit', sCurrentName);
        COMMIT;
      END IF;

      sessionUpdate;

      isr$trace.debug('STB$UTIL.getProperty(olParameter, ''REPID'')', STB$UTIL.getProperty(olParameter, 'REPID'), sCurrentName);
      STB$OBJECT.setCurrentRepid(STB$UTIL.getProperty(olParameter, 'REPID'));

      oErrorObj := setjobparameter (case when upper (oparameter.stoken) = 'CAN_FINALIZE_REPORT' then 'CAN_FINALIZE_REPORT' else 'CAN_CREATE_REPORT_FINAL_VERSION' end);
      stb$job.setjobparameter ('CAN_USE_DOC_SERVER', 'T', stb$job.njobid);
      IF upper (oparameter.stoken) IN ('CAN_FINALIZE_REPORT') THEN
        stb$job.setjobparameter ('FINALFLAG', 'CAN_FINALIZE_REPORT', stb$job.njobid);
      END IF;
      stb$job.setjobparameter ('COMMAND', 'CAN_RUN_REPORT', stb$job.njobid);
      -- write reference, node has changed
      stb$job.setjobparameter ('REFERENCE', stb$object.getCurrentRepid, stb$job.njobid);
      oErrorObj := Stb$util.createFilterParameter (olParameter);
      oErrorObj := stb$job.startjob;

      ISR$TREE$PACKAGE.sendSyncCall('REPORT', STB$OBJECT.getCurrentRepid);

  ------------ create a inspection version  ----------
  ----------------------------------------------------
  WHEN UPPER (oParameter.sToken) IN ('CAN_CREATE_REPORT_INSPECTION_VERSION', 'CAN_CREATE_REPORT_LOCK_VERSION') THEN

    oErrorObj := setInspectionStatus;
    oErrorObj := createsraudit (stb$object.getcurrentsrnumber);

    IF UPPER (oParameter.sToken) = 'CAN_CREATE_REPORT_LOCK_VERSION' THEN

      sValue := UTD$MSGLIB.getMsg(UPPER (oParameter.sToken)||'$DESC', STB$SECURITY.getCurrentUser);
      sDisplay := sValue;
      FOR i IN 1..olParameter.COUNT LOOP
        IF olParameter(i).sParameter LIKE 'ESIGREASON%' THEN
          sValue := NVL(olParameter(i).sValue,UTD$MSGLIB.getMsg(UPPER (oParameter.sToken)||'$DESC', STB$SECURITY.getCurrentUser));
          sDisplay := NVL(olParameter(i).sDisplay, sValue);
        END IF;
      END LOOP;

      INSERT INTO STB$ESIG (entryid
                  , entity
                  , REFERENCE
                  , reason
                  , reasondisplay
                  , TIMESTAMP
                  , action
                  , success
                  , isrsession
                  , ipaddress
                  , userno
                  , fullname
                  , oracleusername
                  , nodetype
                  , srnumber
                  , title)
      VALUES (SEQ$STB$ESIG.NEXTVAL
            , 'ISR$REPORTTYPE$PACKAGE'
            , STB$OBJECT.getCurrentRepid
            , sValue
            , sDisplay
            , SYSDATE
            , 'CAN_LOCK_REPORT'
            , 'T'
            , Stb$security.getCurrentSession
            , Stb$security.getClientIp
            , STB$SECURITY.getCurrentUser
            , (select fullname from stb$user where userno = STB$SECURITY.getCurrentUser)
            , sUsername
            , STB$OBJECT.getCurrentNodeType
            , Stb$object.GetCurrentSrNumber
            , Stb$object.GetCurrentTitle);
    END IF;

    oParameter1.sToken := Upper('CAN_RELEASE_FOR_INSPECTION');
    oErrorObj := putParameters(oParameter1, STB$PROPERTY$LIST(STB$PROPERTY$RECORD ('REPID', '', STB$OBJECT.getCurrentRepid, STB$OBJECT.getCurrentRepid)));

  ------------ create a final version  ---------------
  ----------------------------------------------------
  WHEN UPPER (oParameter.sToken) IN ('CAN_CREATE_REPORT_FINAL_VERSION') THEN

    oErrorObj := setFinalStatus;
    oErrorObj := createsraudit (stb$object.getcurrentsrnumber);
    oParameter1.sToken := Upper('CAN_FINALIZE_REPORT_VERSION');
    oErrorObj := putParameters(oParameter1, STB$PROPERTY$LIST(STB$PROPERTY$RECORD ('REPID', '', STB$OBJECT.getCurrentRepid, STB$OBJECT.getCurrentRepid)));
    oErrorObj := Stb$util.createFilterParameter (olParameter);

    -----------------------------------CAN_CREATE_PDF_------------------------
    ----------------------------------CAN_SEND_AS_EMAIL-----------------------
  WHEN upper (oparameter.stoken) like 'CAN_SEND_AS_EMAIL%'
    OR upper (oparameter.stoken) IN ('CAN_CREATE_PDF', 'CAN_MERGE_COMMON') THEN
      IF upper (oparameter.stoken) = 'CAN_SEND_AS_EMAIL_CHECKOUT' THEN
        IF STB$OBJECT.getCurrentNodeType = 'DOCUMENT' THEN
          -- copy the doc and set the new docid
          oErrorObj := stb$docks.copydoctodoc (stb$object.getcurrentdocid, stb$object.getCurrentRepid, nnewdocid);
          stb$object.setcurrentdocid (nnewdocid);
          UPDATE stb$doctrail
             SET job_flag = 'D'
           WHERE docid = nnewdocid;
          COMMIT;
        ELSE
          UPDATE stb$doctrail
             SET job_flag = 'I'
           WHERE docid = stb$object.getcurrentdocid;
          COMMIT;
        END IF;

        oErrorObj := setjobparameter (upper (oparameter.stoken));
        stb$job.setjobparameter ('ACTIONTYPE', 'SAVE', stb$job.njobid);
        stb$job.setjobparameter ('REFERENCE', stb$object.getCurrentDocid, stb$job.njobid);
        stb$job.setjobparameter ('OLDDOCID', stb$object.getCurrentNodeId, stb$job.njobid);
        stb$job.setjobparameter ('HASHVALUE', stb$object.getCurrentParameterValue, stb$job.njobid);
      ELSE
        IF upper (oparameter.stoken)  IN ('CAN_MERGE_COMMON') THEN
          oErrorObj := setjobparameter (upper ('CAN_CREATE_PDF'));
          --stb$job.setjobparameter ('REPORTTYPEID', null, stb$job.njobid);
          --stb$job.setjobparameter ('SRNUMBER', null, stb$job.njobid);
          stb$job.setjobparameter ('TITLE', stb$object.getNodeIdForNodeType('TITLE'), stb$job.njobid);
          stb$job.setjobparameter ('REPID', stb$object.getNodeIdForNodeType('REPORT'), stb$job.njobid);
          stb$job.setjobparameter ('CURRENT_ACTION', 'CAN_MERGE_COMMON', stb$job.njobid);
          SELECT MAX(d1.docid)
            INTO nDocId
            FROM stb$doctrail d1, stb$doctrail d2
           WHERE d1.doctype = 'D'
             AND d1.nodetype = d2.nodetype
             AND d1.nodeid = d2.nodeid
             AND d2.docid = STB$OBJECT.getCurrentDocid;
          stb$job.setjobparameter ('DOCID', nDocId, stb$job.njobid);
        ELSE
          oErrorObj := setjobparameter (upper (oparameter.stoken));
        END IF;
        stb$job.setjobparameter ('DO_NOT_SWITCH_REPSESSION', 'T', stb$job.njobid);
        IF upper (oparameter.stoken) != 'CAN_CREATE_PDF' THEN
          stb$job.setjobparameter ('COMMAND', 'CAN_DISPLAY_REPORT', stb$job.njobid);
        END IF;
      END IF;
      IF upper (oparameter.stoken)  IN ('CAN_CREATE_PDF', 'CAN_MERGE_COMMON') THEN
        stb$job.setjobparameter ('CAN_USE_DOC_SERVER', 'T', stb$job.njobid);
      END IF;
      IF olParameter IS NOT NULL THEN
        FOR i IN 1..olParameter.count() LOOP
          STB$JOB.setJobParameter(olParameter(i).sParameter, olParameter(i).sValue, STB$JOB.nJobid);
        END LOOP;
      END IF;
      oErrorObj := Stb$util.createFilterParameter (olParameter);
      oErrorObj := stb$job.startjob;

    --------------------------------------------------------------------------
    ------------------------------------CAN_DISPLAY_HEADDATA------------------
  WHEN upper (oparameter.stoken) = 'CAN_DISPLAY_HEADDATA' THEN
      NULL;
    --------------------------------------------------------------------------
    ------------------------------------CAN_DISPLAY_COMMENT-------------------
  WHEN upper (oparameter.stoken) = 'CAN_DISPLAY_COMMENT' THEN
      NULL;
    --------------------------------------------------------------------------
    ------------------------------------CAN_DISPLAY_ATTRIBUTES----------------
  WHEN upper (oparameter.stoken) = 'CAN_DISPLAY_ATTRIBUTES' THEN
      NULL;
    --------------------------------------------------------------------------
    ------------------------------------CAN_DISPLAY_COMMON_ATTRIBUTES---------
  WHEN upper (oparameter.stoken) = 'CAN_DISPLAY_COMMON_ATTRIBUTES' THEN
      NULL;

    ---------------- criteria report/display report ---------------------------------------------------------
    ---------------------------------------------------------------------------------------------------------
  WHEN upper (oparameter.stoken) = 'CAN_CREATE_CRITERIA'THEN
      oErrorObj := setjobparameter (upper (oparameter.stoken));
      stb$job.setjobparameter ('DO_NOT_SWITCH_REPSESSION', 'T', stb$job.njobid);
      oErrorObj := Stb$util.createFilterParameter (olParameter);
      oErrorObj := stb$job.startjob;
    --------------------------------------------------------------------------
    ------------------------------------CAN_DISPLAY_%AUDIT----------------
  WHEN upper (oparameter.stoken) like 'CAN_DISPLAY_%AUDIT' THEN
      oErrorObj := setjobparameter (upper (oparameter.stoken));
      oErrorObj := Stb$util.createFilterParameter (olParameter);
      oErrorObj := stb$job.startjob;
    --------------------------------------------------------------------------
    ----------------------------------CAN_DISPLAY_REPORT_FILES------------------------
  WHEN upper (oparameter.stoken) = 'CAN_DISPLAY_REPORT_FILES' THEN
      oErrorObj := setjobparameter (upper (oparameter.stoken));
      oErrorObj := Stb$util.createFilterParameter (olParameter);
      oErrorObj := stb$job.startjob;
    --------------------------------------------------------------------------
    ----------------------------------CAN_OFFSHOOT_DOC------------------------
  WHEN upper (oparameter.stoken) = 'CAN_OFFSHOOT_DOC' THEN
      oErrorObj := setjobparameter (upper (oparameter.stoken));
      stb$job.setjobparameter ('CAN_USE_DOC_SERVER', 'F', stb$job.njobid);
      stb$job.setjobparameter ('MASTERTEMPLATE', olparameter (1).svalue, stb$job.njobid);
      SELECT iao.actionid, iof.outputid
        INTO nActionId, nOutputId
        FROM isr$output$file iof, isr$action$output iao, isr$action ia
       WHERE iof.fileid = olparameter (1).svalue
         AND iof.outputid = iao.outputid
         AND iao.actionid = ia.actionid
         AND ia.token = 'CAN_RUN_REPORT';

      if nActionId is null then
        sMsg := utd$msglib.getmsg ('exNotFindAction', stb$security.getCurrentLanguage, oParameter.sToken);
        raise stb$job.exNotFindAction;
      end if;

      stb$job.setjobparameter ('OUTPUTID', nOutputId, stb$job.njobid);
      stb$job.setjobparameter ('ACTIONID', nactionid, stb$job.njobid);
      oErrorObj := stb$job.startjob;
  ELSE
      RAISE exnotexists;
  END CASE;

  -- common settings for logging
  for i in 1..olParameter.count() -- 28.Nov.2017 vs [ISRC-724]
  loop
    case
      when olParameter(i).sParameter = 'LOGLEVELNAME' then
        stb$job.setjobparameter ('FILTER_LOGLEVELNAME', olParameter(i).sValue, stb$job.njobid);
      when olParameter(i).sParameter = 'CREATE_RUNTIME_PROFILE' then
        stb$job.setjobparameter ('FILTER_CREATE_RUNTIME_PROFILE', olParameter(i).sValue, stb$job.njobid);
      else
        null;
    end case;
  end loop;

  isr$trace.stat('end','end', sCurrentName);
  RETURN oErrorObj;
EXCEPTION
  WHEN exReporttypeJobSumError THEN
    oErrorObj.handleError ( Stb$typedef.cnSeverityMessage, sMsg, sCurrentName,  dbms_utility.format_error_stack ||stb$typedef.crlf|| dbms_utility.format_error_backtrace  );
    RETURN oErrorObj;
  WHEN exFirstForm THEN
    -- 19.May 2016 HR: exFirstForm is not an error but needed for Wizard control!
    -- sMsg := 'exFirstForm';
    --oErrorObj.handleError ( stb$typedef.cnfirstform, sMsg, sCurrentName, 'SQLCODE = '||SQLCODE||', SQLERRM = '||SQLERRM||' '||dbms_utility.format_error_backtrace);
    -- set instead only sErrorCode and sSeverityCode like for Audit below
    isr$trace.stat('end','end from exceptionhandling exFirstForm', sCurrentName);   -- vs 20.Nov.2017
    oErrorObj.sErrorCode := 'exFirstForm';
    oErrorObj.sSeverityCode := stb$typedef.cnfirstform;
    RETURN oErrorObj;
  WHEN exAudit THEN
    oErrorObj.sErrorCode := 'exAudit';
    oErrorObj.sSeverityCode := stb$typedef.cnaudit;
    isr$trace.stat('end','end from exceptionhandling exAudit', sCurrentName);
    RETURN oErrorObj;
  WHEN exNotExists THEN
    sMsg :=  utd$msglib.getmsg ('exNotExistsText', stb$security.getCurrentLanguage, sqlerrm(sqlcode));
    oErrorObj.handleError ( Stb$typedef.cnSeverityMessage, sMsg, sCurrentName,  dbms_utility.format_error_stack ||stb$typedef.crlf|| dbms_utility.format_error_backtrace  );
    RETURN oErrorObj;
  when stb$job.exNotFindAction then
    oErrorObj.handleError ( Stb$typedef.cnSeverityCritical, sMsg, sCurrentName, dbms_utility.format_error_stack ||stb$typedef.crlf|| dbms_utility.format_error_backtrace );
    rollback;
    return oErrorObj;
  WHEN OTHERS THEN
    ROLLBACK;
    sMsg :=  utd$msglib.getmsg ('ERROR_PLACE', stb$security.getCurrentLanguage, sqlerrm(sqlcode));
    oErrorObj.handleError ( Stb$typedef.cnSeverityWarning, sMsg, sCurrentName,  dbms_utility.format_error_stack ||stb$typedef.crlf|| dbms_utility.format_error_backtrace  );
    RETURN oErrorObj;
END putparameters;

--******************************************************************************
FUNCTION getAuditMask (olnodelist OUT stb$treenodelist)
  RETURN stb$oerror$record
IS
  sCurrentName                  CONSTANT VARCHAR2(100) := $$PLSQL_UNIT||'.getAuditMask('||null||')';
  oErrorObj                     stb$oerror$record := STB$OERROR$RECORD();
BEGIN
  isr$trace.stat('begin','begin', sCurrentName);
  oErrorObj := stb$audit.getauditmask (olnodelist);
  isr$trace.stat('end','end', sCurrentName);
  RETURN (oErrorObj);
EXCEPTION
  WHEN OTHERS THEN
    ROLLBACK;
     oErrorObj.handleError ( Stb$typedef.cnSeverityCritical, utd$msglib.getmsg ('exUnexpectedError', stb$security.getCurrentLanguage,  csP1 => sqlerrm(sqlcode)),
        sCurrentName, dbms_utility.format_error_stack ||chr(10)|| dbms_utility.format_error_backtrace );
    RETURN (oErrorObj);
END getauditmask;

--******************************************************************************
FUNCTION putAuditMask (olparameter IN stb$property$list)
  RETURN stb$oerror$record IS
  sCurrentName                  CONSTANT VARCHAR2(100) := $$PLSQL_UNIT||'.putAuditMask('||null||')';
  oErrorObj                     stb$oerror$record := STB$OERROR$RECORD();
BEGIN
  oErrorObj := Stb$audit.putAuditMask (olParameter);
  RETURN (oErrorObj);
EXCEPTION
  WHEN OTHERS THEN
    ROLLBACK;
     oErrorObj.handleError ( Stb$typedef.cnSeverityCritical, utd$msglib.getmsg ('exUnexpectedError', stb$security.getCurrentLanguage,  csP1 => sqlerrm(sqlcode)),
        sCurrentName, dbms_utility.format_error_stack ||chr(10)|| dbms_utility.format_error_backtrace );
    RETURN (oErrorObj);
END putauditmask;

--******************************************************************************
FUNCTION checkAuditRequired (sAuditRequired OUT VARCHAR2)
  RETURN stb$oerror$record
IS
  sCurrentName                  CONSTANT VARCHAR2(100) := $$PLSQL_UNIT||'.checkAuditRequired('||null||')';
  oErrorObj                     stb$oerror$record := STB$OERROR$RECORD();
  xXml                          XMLTYPE;
  nLastFinalRepid               NUMBER;
  sAuditExists                  VARCHAR2 (1) := 'F';
BEGIN
  isr$trace.stat('begin','begin', sCurrentName);

  sAuditRequired := nvl(stb$util.getReptypeParameter (stb$object.getcurrentreporttypeid, 'REQUIRES_AUDITING'), 'F');

  -- if reptype has no audit return oErrorObj
  IF sAuditRequired = 'F' THEN
    RETURN oErrorObj;
  END IF;

  -- why? we did this already
  /*-- checksum
  oErrorObj := updateCheckSum (stb$object.getCurrentRepid);

  IF oErrorObj.sSeverityCode != stb$typedef.cnNoError THEN
    RETURN oErrorObj;
  END IF;*/

  -- write the audit depending on the status of the current report ,
  -- check audit between draft versions required etc.
  oErrorObj := checkauditrequired (sAuditRequired, nLastFinalRepid);
  isr$trace.debug ('sAuditRequired',  sAuditRequired, sCurrentName);

  IF oErrorObj.sSeverityCode != stb$typedef.cnNoError THEN
    RETURN oErrorObj;
  END IF;

  -- there should be an audit on the report criteria
  IF sAuditRequired = 'T' THEN
    --check if there have been changes on the criteria data
    oErrorObj := isr$reporttype$package.createaudit (sAuditExists, xXml, nLastFinalRepid);

    IF oErrorObj.sSeverityCode != stb$typedef.cnNoError THEN
      isr$trace.debug ('createAuditError',  'createAuditError', sCurrentName);
      isr$trace.error ('error', 'oErrorObj',sCurrentName,oErrorObj);
      RETURN oErrorObj;
    END IF;

    IF sAuditExists = 'T' THEN
      -- there have been changes write them into the database
      -- create new record
      oErrorObj := stb$audit.createNewAuditRecord (to_char (stb$object.getCurrentRepid), 'REPORTAUDIT');
      -- set the flag that this an audit on criteria in the package that if a prompted audit is required
      -- not the general mechanism is called and  the reason is written in the file in a different way
      sFlagReportAudit := 'T';
    ELSE
      -- no changes return to the frontend
      isr$trace.debug ('no values changed',  'no values changed', sCurrentName);
      sAuditRequired := 'F';
      -- release the memory
      oErrorObj := stb$audit.freeAuditdom;
    END IF;
  END IF;

  isr$trace.stat('end','end', sCurrentName);
  RETURN (oErrorObj);
EXCEPTION
  WHEN OTHERS THEN
    ROLLBACK;
     oErrorObj.handleError ( Stb$typedef.cnSeverityCritical, utd$msglib.getmsg ('exUnexpectedError', stb$security.getCurrentLanguage,  csP1 => sqlerrm(sqlcode)),
        sCurrentName, dbms_utility.format_error_stack ||chr(10)|| dbms_utility.format_error_backtrace );
    RETURN (oErrorObj);
END checkauditrequired;

--******************************************************************************
FUNCTION getContextMenu( nMenu           in  number,
                         olMenuEntryList out stb$menuentry$list)
  RETURN stb$oerror$record
IS
  sCurrentName                  CONSTANT VARCHAR2(100) := $$PLSQL_UNIT||'.getContextMenu('||nMenu||')';
  oErrorObj                     stb$oerror$record := STB$OERROR$RECORD();
BEGIN
  isr$trace.stat('begin','begin', sCurrentName);
  oErrorObj := stb$util.getContextMenu (stb$object.getcurrentnodetype, nmenu, olMenuEntryList);

  -- lock the report(s)
  IF STB$OBJECT.getCurrentNodetype IN ('REPORT','DOCUMENT') THEN
    IF stb$util.getReptypeParameter (stb$object.getCurrentReporttypeid, 'HAS_SRNUMBER') = 'T' THEN
      oErrorObj := stb$system.lockreports (' and title= ''' || REPLACE(stb$object.getNodeIdForNodeType('TITLE'), '''', '''''')|| '''');
    ELSE
      oErrorObj := stb$system.lockreports (' and repid= ''' || stb$object.getCurrentRepid || '''');
    END IF;
  END IF;

  isr$trace.debug('value','olMenuentryList OUT',sCurrentName,olMenuEntryList);
  isr$trace.stat('end','olMenuentryList.count: '||olMenuEntryList.count, sCurrentName);
  RETURN (oErrorObj);
EXCEPTION
  WHEN OTHERS THEN
    ROLLBACK;
     oErrorObj.handleError ( Stb$typedef.cnSeverityCritical, utd$msglib.getmsg ('exUnexpectedError', stb$security.getCurrentLanguage,  csP1 => sqlerrm(sqlcode)),
        sCurrentName, dbms_utility.format_error_stack ||chr(10)|| dbms_utility.format_error_backtrace );
    RETURN (oErrorObj);
END getContextMenu;

--******************************************************************************
PROCEDURE destroyOldValues
IS
  sCurrentName                  CONSTANT VARCHAR2(100) := $$PLSQL_UNIT||'.destroyOldValues('||null||')';
  oErrorObj                     stb$oerror$record := STB$OERROR$RECORD();
BEGIN
  isr$trace.stat('begin','begin', sCurrentName);
  STB$OBJECT.setCurrentToken('');
  sFlagReportAudit := '';
  stb$object.destroyoldvalues;
  isr$trace.stat('end','end', sCurrentName);
EXCEPTION
  WHEN OTHERS THEN
     oErrorObj.handleError ( Stb$typedef.cnSeverityCritical, utd$msglib.getmsg ('exUnexpectedError', stb$security.getCurrentLanguage,  csP1 => sqlerrm(sqlcode)),
        sCurrentName, dbms_utility.format_error_stack ||chr(10)|| dbms_utility.format_error_backtrace );

END destroyoldvalues;

--*********************************************************************************************************************************
FUNCTION checkDependenciesOnMask( oParameter IN STB$MENUENTRY$RECORD,
                                  olParameter IN OUT STB$PROPERTY$LIST,
                                  sModifiedFlag OUT VARCHAR2 )
RETURN STB$OERROR$RECORD
IS
  sCurrentName                  CONSTANT VARCHAR2(100) := $$PLSQL_UNIT||'.checkDependenciesOnMask('||null||')';
  oErrorObj         STB$OERROR$RECORD := STB$OERROR$RECORD(NULL,NULL,Stb$typedef.cnNoError);
  olParameterOld    STB$PROPERTY$LIST := STB$PROPERTY$LIST();
  oSelectionList    ISR$TLRSELECTION$LIST := ISR$TLRSELECTION$LIST();
BEGIN
  isr$trace.stat('begin','begin', sCurrentName);

  olParameterOld := olParameter;

  IF STB$UTIL.getProperty(olParameter, 'ITERATION') IS NOT NULL AND STB$UTIL.getProperty(olParameter, 'ITERATION') IN ('DAILY','WEEKLY','MONTHLY') THEN
    isr$trace.debug('sKeyValue1',STB$UTIL.getProperty(olParameter, 'ITERATION') ,sCurrentName);
    FOR i IN 1..olParameter.Count() LOOP
      isr$trace.debug('olParameter(i).sParameter',olParameter(i).sParameter,sCurrentName);
      IF olParameter(i).hasLov = 'T' AND olParameter(i).sDisplayed = 'T' AND olParameter(i).sParameter like 'JOBBY%' THEN
        olParameter(i).sParameter := 'JOBBY_'||STB$UTIL.getProperty(olParameter, 'ITERATION') ;
        olParameter(i).sFocus := 'T';
        isr$trace.debug('olParameter(i).sParameter',olParameter(i).sParameter,sCurrentName);
        oErrorObj := getLov (olParameter(i).sParameter, olParameter(i).sValue, oSelectionList);
        IF oSelectionList.First IS NOT NULL THEN
          FOR j IN 1..oSelectionList.Count() LOOP
            IF oSelectionList(j).sSelected = 'T' THEN
              olParameter(i).sDisplay := oSelectionList(j).sDisplay;
            END IF;
          END LOOP;
        END IF;
      END IF;
    END LOOP;
  END IF;

  -- compare the objects if something changed only then refresh the mask
  SELECT CASE WHEN count ( * ) = 0 THEN 'F' ELSE 'T' END
      INTO sModifiedFlag
      FROM (SELECT XMLTYPE (value (p)).getStringVal ()
              FROM table (olParameter) p
            MINUS
            SELECT XMLTYPE (value (p)).getStringVal ()
              FROM table (olParameterOld) p);

  isr$trace.stat('end','end', sCurrentName);
  RETURN oErrorObj;
EXCEPTION
  WHEN OTHERS THEN
     oErrorObj.handleError ( Stb$typedef.cnSeverityWarning, utd$msglib.getmsg ('exUnexpectedError', stb$security.getCurrentLanguage,  csP1 => sqlerrm(sqlcode)),
        sCurrentName, dbms_utility.format_error_stack ||chr(10)|| dbms_utility.format_error_backtrace );
    RETURN oErrorObj;
END checkDependenciesOnMask;

--*********************************************************************************************************************************
FUNCTION getSelectedLovDisplay(csParameter IN VARCHAR2, csValue IN VARCHAR2, csPackage IN VARCHAR2 DEFAULT 'ISR$REPORTTYPE$PACKAGE')
  RETURN VARCHAR2
IS
  sCurrentName                  CONSTANT VARCHAR2(100) := $$PLSQL_UNIT||'.getSelectedLovDisplay('||csParameter||')';
  PRAGMA AUTONOMOUS_TRANSACTION;
  oErrorObj         STB$OERROR$RECORD := STB$OERROR$RECORD(NULL,NULL,Stb$typedef.cnNoError);

  oSelectionList    ISR$TLRSELECTION$LIST := ISR$TLRSELECTION$LIST();
  sDisplay          VARCHAR2(4000);

  CURSOR cGetTempLovValue IS
    SELECT display
      FROM tmp$lov$value
     WHERE parameter = csParameter
       AND VALUE = csValue
       AND package = csPackage;
BEGIN
  isr$trace.stat('begin','begin', sCurrentName);

  IF TRIM(csValue) IS NOT NULL THEN

    OPEN cGetTempLovValue;
    FETCH cGetTempLovValue INTO sDisplay;
    CLOSE cGetTempLovValue;

    isr$trace.debug('display from backup',sDisplay,sCurrentName);

    IF sDisplay IS NULL THEN

      EXECUTE IMMEDIATE
      'BEGIN
        :1 := '||csPackage||'.getLov (:2, :3, :4);
       END;' USING OUT oErrorObj, IN csParameter, IN csValue, OUT oSelectionList;

      SELECT DISTINCT MIN(sDisplay)
        INTO sDisplay
        FROM table (oSelectionList)
       WHERE sSelected = 'T';

      INSERT INTO TMP$LOV$VALUE VALUES(csParameter, csValue, csPackage, sDisplay);
      COMMIT;
      isr$trace.debug('display from list',sDisplay,sCurrentName);

    END IF;

  END IF;

  isr$trace.stat('end','end', sCurrentName);
  RETURN sDisplay;
EXCEPTION
  WHEN OTHERS THEN
     oErrorObj.handleError ( Stb$typedef.cnSeverityCritical, utd$msglib.getmsg ('exUnexpectedError', stb$security.getCurrentLanguage,  csP1 => sqlerrm(sqlcode)),
        sCurrentName, dbms_utility.format_error_stack ||chr(10)|| dbms_utility.format_error_backtrace );
    RETURN sDisplay;
END getSelectedLovDisplay;

--******************************************************************************
function checkDocCheckout( csMenuAllowed char,
                           cnRepId       stb$report.repid%type )
  return char
is
  sCurrentName       constant varchar2(100) := $$PLSQL_UNIT||'.checkDocCheckout('||csMenuAllowed||','||cnRepId||')';
  oErrorObj          STB$OERROR$RECORD    := STB$OERROR$RECORD();
  sExists            char(1);
  sReturnMenuAllowed char(1);

  cursor curReportDocIsCheckedOut( ccnRepId stb$report.repid%type ) is
    select checkedOut
    from   stb$doctrail  doc
    join   stb$report    rep
    on     doc.nodetype = 'REPORT'
    and    doc.doctype = 'D'
    and    rep.repid = doc.nodeid
    where  doc.checkedout = 'T'
    and    rep.repid = ccnRepId;

begin
  isr$trace.stat('begin','csMenuAllowed: '||csMenuAllowed||',  cnRepId: '||cnRepId,sCurrentName);
  if csMenuAllowed = 'T' then
    -- do not allow menu, if the word document is in checked out state
    open curReportDocIsCheckedOut(STB$OBJECT.getCurrentRepid);
    fetch curReportDocIsCheckedOut into sExists;
    if curReportDocIsCheckedOut%FOUND then
      sReturnMenuAllowed := 'F';
    else
      sReturnMenuAllowed := 'T';
    end if;
    close curReportDocIsCheckedOut;
  else
    -- no check, menu is not allowed
    sReturnMenuAllowed := 'F';
  end if;

  isr$trace.stat('end','csMenuAllowed: '||csMenuAllowed,sCurrentName);
  return sReturnMenuAllowed;

exception
  when others then
    oErrorObj.handleError ( Stb$typedef.cnSeverityCritical,
                            utd$msglib.getmsg ('exUnexpectedError', nCurrentLanguage,  csP1 => sqlerrm(sqlcode)),
                            'sCurrentName',
                            substr(dbms_utility.format_error_stack ||crlf|| dbms_utility.format_error_backtrace, 1,4000) );
      return 'F';
end checkDocCheckout;

--******************************************************************************
FUNCTION defineMailText(csToken in varchar2,oParamListRec in  isr$paramlist$rec,  sText out varchar2 )
return STB$OERROR$RECORD
IS
  sCurrentName    constant varchar2(100) := $$PLSQL_UNIT||'.defineMailText';
  oErrorObj           STB$OERROR$RECORD := STB$OERROR$RECORD();
BEGIN
  sText:= csToken;
  isr$trace.stat('begin',' Text  '||sText,sCurrentName);
  return oErrorObj;
END defineMailText;

--******************************************************************************
FUNCTION defineMailSubject(csToken in varchar2,oParamListRec in  isr$paramlist$rec,  sSubject   out  varchar2 )
return STB$OERROR$RECORD
IS
  sCurrentName    constant varchar2(100) := $$PLSQL_UNIT||'.defineMailSubject';
  oErrorObj           STB$OERROR$RECORD := STB$OERROR$RECORD();
BEGIN
  sSubject:= csToken||'ISR$REPORTTYPE$PACKAGE';
  isr$trace.stat('begin',' Subject  '||sSubject,sCurrentName);
  return oErrorObj;
END defineMailSubject ;
--******************************************************************************
FUNCTION defineMailAttachment(csToken in varchar2 ,oParamListRec in  isr$paramlist$rec,olFileList  in  out  ISR$FILE$LIST  )
return STB$OERROR$RECORD
IS
  sCurrentName    constant varchar2(100) := $$PLSQL_UNIT||'.defineMailAttachment';
  cnJobId         varchar2(30);
  oErrorObj       STB$OERROR$RECORD := STB$OERROR$RECORD();
BEGIN

  isr$trace.stat('begin',' csToken  '||csToken,sCurrentName);
  cnJobId := oParamListRec.GetParamStr('JOBID');

--If cnJobid is not null, a file list is generated

  if (cnJobid is not null) then

      for rGetDoc in (select filename,mimetype, binaryfile,jobid
                    from stb$jobtrail
                    where jobid=cnJobid and (
                     documenttype = 'D'
            or documenttype = 'TARGET'
            or documenttype = 'CONFIG'
            or documenttype = 'ROHREPORT') ) loop
        olFileList.extend();
        olFileList(olFileList.count) := ISR$FILE$REC(rGetDoc.filename, rGetDoc.mimetype, rGetDoc.binaryfile);
      end loop;
  end if;


  isr$trace.stat('end',' cnJobid  '||cnJobid,sCurrentName);
  return oErrorObj;
END defineMailAttachment;


-- ***************************************************************************************************  
procedure setCurrentParameter(coCurrentParameter stb$property$list) is
begin
  oCurrentParameter := coCurrentParameter;
end setCurrentParameter;

--******************************************************************************************************
function getCurrentParameter return stb$property$list is
begin
  return oCurrentParameter;
end getCurrentParameter;

--******************************************************************************
function dropReport( cnRepid in number, csDropReportSelf in varchar2 default 'T') return stb$oerror$record   
IS
  sCurrentName                  CONSTANT VARCHAR2(100) := $$PLSQL_UNIT||'.dropReport('||cnRepid||')';
  oErrorObj                     stb$oerror$record := STB$OERROR$RECORD();
begin
  isr$trace.stat('begin','begin', sCurrentName);
  DELETE FROM isr$doc$property
        WHERE docid IN (SELECT docid
                          FROM stb$doctrail
                         WHERE nodeid = TO_CHAR (cnRepid));
  DELETE FROM stb$doctrail
        WHERE docid IN (SELECT docid
                          FROM stb$doctrail
                         WHERE nodeid = TO_CHAR (cnRepid));
  DELETE FROM isr$report$grouping$tab WHERE repid = cnRepid;
  DELETE FROM isr$report$file WHERE repid = cnRepid;
  if csDropReportSelf = 'T' then
    DELETE FROM isr$crit
         WHERE repid = cnRepid;    
    DELETE FROM stb$reportparameter
        WHERE repid = cnRepid;
    DELETE FROM stb$report
          WHERE repid = cnRepid;   
  end if;      
  COMMIT;
  isr$trace.stat('end','end', sCurrentName);
  return oErrorObj; 
exception
  when others then
    oErrorObj.handleError ( Stb$typedef.cnSeverityCritical, 
            utd$msglib.getmsg ('exUnexpectedError', nCurrentLanguage,  csP1 => sqlerrm(sqlcode)), 
            sCurrentName,
           dbms_utility.format_error_stack ||crlf|| dbms_utility.format_error_backtrace );                               
      return oErrorObj;
end dropReport; 

END isr$reporttype$package; 