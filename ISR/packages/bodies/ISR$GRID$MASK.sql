CREATE OR REPLACE PACKAGE BODY ISR$GRID$MASK 
IS

  -- common declarations
  exMisconfigEntity EXCEPTION;

  sCurrentWZEntity         STB$TYPEDEF.tLine;
  oCurrentEntList          STB$ENTITY$LIST;
  
  lTypedef1 Stb$typedef.tlStrings;
  lTypedef2 Stb$typedef.tlStrings;
  lTypedef3 Stb$typedef.tlStrings;
  
  lSringsNull Stb$typedef.tlStrings;
  
  lOldSelectionList        iSR$SelectionData$List := iSR$SelectionData$List();
  lNewSelectionList        iSR$SelectionData$List := iSR$SelectionData$List();
  
  exNoMasterkey               EXCEPTION;

-- local functions
-- *****************************************************************************
FUNCTION initListe(cnRepId IN number) RETURN STB$OERROR$RECORD
IS
  sCurrentName    constant varchar2(4000) := $$PLSQL_UNIT||'.initListe('||cnRepId||')';
  oError          STB$OERROR$RECORD := STB$OERROR$RECORD();
  sFound          VARCHAR2(1):='F';
  nIx             PLS_INTEGER;
  nCount          INTEGER;
BEGIN
  isr$trace.stat('begin', 'begin', sCurrentName);

  lOldSelectionList := iSR$SelectionData$List();
  
  lOldSelectionList.Extend;
  nIx := 1;
  nCount := oCurrentEntList.count();
  
  while nIx < nCount loop
    lOldSelectionList.Extend;
    nIx := lOldSelectionList.Last;
    lOldSelectionList(nIx) := iSR$SelectionData$Rec();
    isr$trace.debug('before ...','nIx:'||nIx||' nCount:'||nCount, sCurrentName);
    oError:=Stb$util.checkCriteria(cnRepId,oCurrentEntList(nIx).sEnt,sFound, lOldSelectionList(nIx).lSelectionList);
    isr$trace.debug('AFTER oCurrentEntList(2)','AFTER'|| oCurrentEntList(nIx).sEnt, sCurrentName);
  end loop;
  
  isr$trace.stat('end', 'end', sCurrentName);
RETURN oError;
EXCEPTION
  WHEN OTHERS THEN
   oError.handleError ( Stb$typedef.cnSeverityCritical, utd$msglib.getmsg ('exUnexpectedError', stb$security.getCurrentlanguage,  csP1 => sqlerrm(sqlcode)), 
        sCurrentName, dbms_utility.format_error_stack ||chr(10)|| dbms_utility.format_error_backtrace );
    RETURN oError;
END initListe;

-- *****************************************************************************
FUNCTION GetGridMaskAdds(sEntity IN STB$TYPEDEF.tLine, csMasterKey IN varchar2, lAdds OUT iSR$Crit$List) RETURN STB$OERROR$RECORD IS
  oError         STB$OERROR$RECORD := STB$OERROR$RECORD(); 
  sCurrentName   constant varchar2(4000) := $$PLSQL_UNIT||'.GetGridMaskAdds('||sEntity||')';
begin
  isr$trace.stat('begin', 'begin', sCurrentName);
  lAdds := iSR$Crit$List();
  execute immediate
    'DECLARE
       olocError STB$OERROR$RECORD;
     BEGIN
       olocError :='|| STB$UTIL.getCurrentCustomPackage||'.GetGridMaskAdds(:1,:2,:3);
       :4 :=  olocError;
     END;'
    USING in sEntity, in csMasterKey, out lAdds, out oError;
  isr$trace.stat('end', 'end', sCurrentName);
  RETURN oError;
EXCEPTION
  WHEN OTHERS THEN
     oError.handleError ( Stb$typedef.cnSeverityCritical, utd$msglib.getmsg ('exUnexpectedError', stb$security.getCurrentlanguage,  csP1 => sqlerrm(sqlcode)), 
        sCurrentName, dbms_utility.format_error_stack ||chr(10)|| dbms_utility.format_error_backtrace );
    RETURN oError;
end GetGridMaskAdds;

-- *****************************************************************************
FUNCTION GetEntityDefault(sEntity IN STB$TYPEDEF.tLine, csMasterKey in varchar2) return ISR$CRIT$REC is
  oErrorObj STB$OERROR$RECORD := STB$OERROR$RECORD(); 
  sCurrentName   constant varchar2(4000) := $$PLSQL_UNIT||'.GetEntityDefault('||sEntity||')';
  oRow      ISR$CRIT$REC; 
begin
  isr$trace.stat('begin', 'begin', sCurrentName);
  execute immediate
    'BEGIN
       :1 :='|| STB$UTIL.getCurrentCustomPackage||'.GetEntityDefault(:2,:3,:4);
     END;'
  USING out oErrorObj, in sEntity, in csMasterKey, out oRow;
  isr$trace.stat('end', 'end', sCurrentName);
  return oRow;
end GetEntityDefault;

-- public functions 
-- *****************************************************************************
FUNCTION GetSList (sEntity IN STB$TYPEDEF.tLine,
                   sMasterKey IN VARCHAR2,
                   sNavigateMode IN varchar2,
                   oSelection OUT ISR$TLRSELECTION$LIST) RETURN STB$OERROR$RECORD
IS
  sCurrentName         constant varchar2(4000) := $$PLSQL_UNIT||'.GetSList('||sEntity||')';
  nRepId               NUMBER;
  oErrorObj            STB$OERROR$RECORD := STB$OERROR$RECORD();
  sFound               VARCHAR2(1);
  nIx                  PLS_INTEGER;

  sFlag                VARCHAR2(1);  -- dummy, is not evaluated
  sConsMasterKey       VARCHAR2(1);
  oParamList iSR$ParamList$Rec := iSR$ParamList$Rec();

BEGIN
  isr$trace.stat('begin', 'begin', sCurrentName);
  isr$trace.debug('sMasterKey',sMasterKey ,sCurrentName);

  nRepId := Stb$object.getCurrentRepId;

  IF Trim(sMasterKey) IS NULL THEN
    RAISE exNoMasterkey;
  END IF;

  oSelection := ISR$TLRSELECTION$LIST();
  
  -- fill Param List with values
  oParamList.AddParam('REPORTTYPEID', STB$OBJECT.getCurrentReporttypeId);
  oParamList.AddParam('REPID', nRepId);
  oParamList.AddParam('ISGRID', 'T');
  -- other params here ?....
  
  oErrorObj := ISR$REMOTE$COLLECTOR.GetWizardList(sEntity, oParamList, oSelection, sFlag, sMasterKey);
  IF oErrorObj.sSeverityCode != STB$TYPEDEF.cnNoError THEN
    RETURN oErrorObj;
  END IF;    
  
  sFound :='F';
  IF sNavigateMode = 'T' THEN
    sFound := 'T';
    isr$trace.debug('sNavigateMode=T','sFound: ' ||sFound, sCurrentName);
  END IF;
 
  IF sFound='F' THEN
    sConsMasterKey := STB$UTIL.getReptypeParameter(STB$OBJECT.getCurrentReporttypeId, 'CONSIDER_MASTERKEY_'||sEntity);
    IF sEntity != oCurrentEntList(1).sEnt THEN
      nIx := oSelection.first;
      while nIx is not null loop
        --isr$trace.debug('sMasterKey',sMasterKey, sCurrentName);
        --isr$trace.debug('oSelection(nIx).sMasterKey',oSelection(nIx).sMasterKey, sCurrentName);
        IF sConsMasterKey = 'T' THEN
          IF oSelection(nIx).sMasterKey != sMasterKey
          OR oSelection(nIx).sMasterKey IS NULL THEN
            oSelection.DELETE(nIx);
          END IF;
        ELSE         
          oSelection(nIx).sMasterKey := sMasterKey;
        END IF;
        nIx := oSelection.next(nIx);
      end loop;
    END IF;
  END IF;

  isr$trace.stat('end', 'end', sCurrentName);
  RETURN oErrorObj;

EXCEPTION
  WHEN exNoMasterkey THEN
    oErrorObj.handleError ( Stb$typedef.cnSeverityMessage, utd$msglib.getmsg ('exNoMasterkeyText', stb$security.getCurrentlanguage,  csP1 => sqlerrm(sqlcode)), 
        sCurrentName, dbms_utility.format_error_stack ||chr(10)|| dbms_utility.format_error_backtrace );
    RETURN oErrorObj;
  WHEN OTHERS THEN
     oErrorObj.handleError ( Stb$typedef.cnSeverityCritical, utd$msglib.getmsg ('exUnexpectedError', stb$security.getCurrentlanguage,  csP1 => sqlerrm(sqlcode)), 
        sCurrentName, dbms_utility.format_error_stack ||chr(10)|| dbms_utility.format_error_backtrace );
    RETURN oErrorObj;
END GetSList;

--******************************************************************************
FUNCTION PutSList (sEntity IN Stb$typedef.tLine,  oSelection IN ISR$TLRSELECTION$LIST)
RETURN STB$OERROR$RECORD IS
  sCurrentName   constant varchar2(4000) := $$PLSQL_UNIT||'.PutSList('||sEntity||')';
  oErrorObj   STB$OERROR$RECORD := STB$OERROR$RECORD();
BEGIN
  isr$trace.stat('begin', 'begin', sCurrentName);
  isr$trace.debug('BEGIN','sEntity: ' ||sEntity||' o='||oSelection(1).nordernum, sCurrentName);
  oErrorObj := STB$UTIL.PutSListGrid (sEntity, '', oSelection);
  isr$trace.stat('end', 'end', sCurrentName);
  RETURN oErrorObj;
EXCEPTION
  WHEN NO_DATA_FOUND THEN
     oErrorObj.handleError ( Stb$typedef.cnSeverityCritical, utd$msglib.getmsg ('exUnexpectedError', stb$security.getCurrentlanguage,  csP1 => sqlerrm(sqlcode)), 
        sCurrentName, dbms_utility.format_error_stack ||chr(10)|| dbms_utility.format_error_backtrace );
    RETURN oErrorObj;
  WHEN OTHERS THEN
     oErrorObj.handleError ( Stb$typedef.cnSeverityCritical, utd$msglib.getmsg ('exUnexpectedError', stb$security.getCurrentlanguage,  csP1 => sqlerrm(sqlcode)), 
        sCurrentName, dbms_utility.format_error_stack ||chr(10)|| dbms_utility.format_error_backtrace );
    RETURN oErrorObj;
END PutSList;

--******************************************************************************
FUNCTION createAudit(xXml IN OUT XMLTYPE, sAudit OUT VARCHAR2,
                     nLastFinalRepId IN number, nFormNo IN number) RETURN STB$OERROR$RECORD
IS
  sCurrentName   constant varchar2(4000) := $$PLSQL_UNIT||'.createAudit('||' '||')';
  oError          STB$OERROR$RECORD := STB$OERROR$RECORD();
  nRepId          NUMBER;
  sFound          VARCHAR2(1):='F';
  nIx             PLS_INTEGER;
  nCount          INTEGER;
  sLoopAudit      varchar2(1);

BEGIN
  isr$trace.stat('begin', 'begin', sCurrentName);

  nRepId := Stb$object.getCurrentRepId;
  
  oError := GetEntityList(nFormNo, oCurrentEntList);
  isr$trace.debug('after GetEntityList','count is: ' || oCurrentEntList.count , sCurrentName);
  
  -- intialize the old values
  oError :=  initListe(nLastFinalRepId);
       
  --Initalisierung der Objektlisten    
  lNewSelectionList := iSR$SelectionData$List();
  lNewSelectionList.Extend;
  nIx := 1;
  nCount := oCurrentEntList.count();
  
  sAudit := 'F';
  
  while nIx < nCount loop
    lNewSelectionList.Extend;
    nIx := lNewSelectionList.Last;
    lNewSelectionList(nIx) := iSR$SelectionData$Rec();
    
    oError := Stb$util.checkCriteria(nRepId,oCurrentEntList(nIx).sEnt,sFound, lNewSelectionList(nIx).lSelectionList);
    isr$trace.debug('AFTER oCurrentEntList(2)','AFTER'|| oCurrentEntList(nIx).sEnt, sCurrentName);
    
    ISR$XML.CreateNode(xXml, '/AUDITFILE', 'AUDITENTRY','');
    ISR$XML.SetAttribute(xXml, '/AUDITFILE/AUDITENTRY[position()=last()]', 'TYP', 'CRITERIA');
    ISR$XML.SetAttribute(xXml, '/AUDITFILE/AUDITENTRY[position()=last()]', 'ENTITY', oCurrentEntList(nIx).sEnt);

    -- call the general function to compare the selection objects
    stb$audit.setcurrentxml (xXml);
    oError := STB$AUDIT.CompareObjects(lOldSelectionList(nIx).lSelectionList, lNewSelectionList(nIx).lSelectionList, sLoopAudit, oCurrentEntList(nIx).sEnt);
    xXml := STB$AUDIT.GETAUDITDOMID;

    isr$trace.debug('sAudit', sAudit , sCurrentName);
    IF sLoopAudit = 'F' THEN
      ISR$XML.DeleteNode(xXml, '/AUDITFILE/AUDITENTRY[@ENTITY="'|| oCurrentEntList(nIx).sEnt ||'"]');
    ELSE
      sAudit := 'T';
    END IF;    
  end loop;
  
  isr$trace.stat('end', 'end', sCurrentName);
RETURN oError;

EXCEPTION
  WHEN OTHERS THEN
     oError.handleError ( Stb$typedef.cnSeverityCritical, utd$msglib.getmsg ('exUnexpectedError', stb$security.getCurrentlanguage,  csP1 => sqlerrm(sqlcode)), 
        sCurrentName, dbms_utility.format_error_stack ||chr(10)|| dbms_utility.format_error_backtrace );
    RETURN oError;
END createAudit;

-- *****************************************************************************
FUNCTION GetEntityList( cnMaskNo IN NUMBER, oEntList OUT STB$ENTITY$LIST) RETURN STB$OERROR$RECORD is
  oError     STB$OERROR$RECORD := STB$OERROR$RECORD();
  sCurrentName   constant varchar2(4000) := $$PLSQL_UNIT||'.GetEntityList('||cnMaskNo||')';
  nRepType number := stb$object.getcurrentreporttypeid;
  
  cursor cCountMasterAlternatives is
    select G.Wizardentity, count(1) 
    from ISR$META$GRID$ENTITY g,
         ISR$REPORT$WIZARD w
    where W.ENTITY = G.WIZARDENTITY
      and W.REPORTTYPEID = G.REPORTTYPEID
      and W.REPORTTYPEID = nRepType
      and W.MASKNO = cnMaskNo
      and columnpos = 1
    group by G.Wizardentity;
  nCountAlternatives number;
  
  cursor cEntities( sFirstEntity in STB$TYPEDEF.tLine) is
    select G.EntityName, PreSelect, Editable, CRITERIAHEADLINE1, CRITERIAHEADLINE2, CRITERIAHEADLINE3
    from ISR$META$GRID$ENTITY g
    where G.WIZARDENTITY = sCurrentWZEntity
      and G.REPORTTYPEID = nRepType
      and ((columnpos =1 and EntityName = nvl(sFirstEntity, EntityName)) or columnpos > 1)
    order by columnpos;
  rEntities cEntities%rowtype;

  sEntity STB$TYPEDEF.tLine;
  nInd pls_integer;

begin
  isr$trace.stat('begin', 'begin', sCurrentName);
  -- check for alternatives
  open cCountMasterAlternatives;
  fetch cCountMasterAlternatives into sCurrentWZEntity, nCountAlternatives;
  close cCountMasterAlternatives;
  
  if nCountAlternatives = 0 then
    -- grid is not configured properly
    RAISE exMisconfigEntity;
  elsif nCountAlternatives > 1 then
    -- alternatives
    execute immediate
      'DECLARE
         olocError STB$OERROR$RECORD;
       BEGIN
         olocError :='|| STB$UTIL.getCurrentCustomPackage||'.getGridMaster(:1,:2);
         :3 :=  olocError;
       END;'
      USING in cnMaskNo, out sEntity, out oError;
    isr$trace.debug( 'after getGridMaster ' || sEntity, sEntity, sCurrentName );      
  end if;    
  
  -- Initalisierung der Objektlisten
  oEntList := STB$ENTITY$LIST();
  lTypedef1 := lSringsNull;
  lTypedef2 := lSringsNull;
  lTypedef3 := lSringsNull;
   
  open cEntities(sEntity); -- is null if no alternatives are defined or set if alternatives have been found
  fetch cEntities into rEntities;

  while cEntities%found loop
    oEntList.EXTEND;
    nInd := oEntList.Last();
    oEntList(nInd) :=  STB$ENTITY$RECORD();
    isr$trace.debug('Entity '||rEntities.EntityName, 'nInd:'||nInd, sCurrentName);
    oEntList(nInd).sEnt := rEntities.EntityName;
    oEntList(nInd).sPreSelected := rEntities.PreSelect;
    lTypedef1(nInd) := rEntities.CRITERIAHEADLINE1;
    lTypedef2(nInd) := rEntities.CRITERIAHEADLINE2;
    lTypedef3(nInd) := rEntities.CRITERIAHEADLINE3;
    fetch cEntities into rEntities;
  end loop;
  close cEntities;
  oCurrentEntList := oEntList;

  isr$trace.debug('status','number OF entities'||nInd, sCurrentName);
  isr$trace.stat('end', 'end', sCurrentName);
  return oError;

exception
  when exMisconfigEntity then
     oError.handleError ( Stb$typedef.cnSeverityCritical, utd$msglib.getmsg ('exMisconfigEntityText', stb$security.getCurrentlanguage,  csP1 => sqlerrm(sqlcode)), 
        sCurrentName, dbms_utility.format_error_stack ||chr(10)|| dbms_utility.format_error_backtrace );
    RETURN oError;   
  when others then
     oError.handleError ( Stb$typedef.cnSeverityCritical, utd$msglib.getmsg ('exUnexpectedError', stb$security.getCurrentlanguage,  csP1 => sqlerrm(sqlcode)), 
        sCurrentName, dbms_utility.format_error_stack ||chr(10)|| dbms_utility.format_error_backtrace );
    RETURN oError;
end GetEntityList;

-- *****************************************************************************
FUNCTION GetGridSelection (sEntity IN Stb$typedef.tLine,
                           sNavigateMode IN VARCHAR2,
                           oSelection OUT ISR$TLRSELECTION$LIST) RETURN STB$OERROR$RECORD
IS
  sCurrentName                constant varchar2(4000) := $$PLSQL_UNIT||'.GetGridSelection('||sEntity||')';
  nLineIndex                  NUMBER;
  nRepId                      NUMBER;
  oError                      STB$OERROR$RECORD := STB$OERROR$RECORD();
  nCounter                    NUMBER;
  sFound                      VARCHAR2(1);
  
  oCritRec ISR$CRIT$REC;

  CURSOR cGetCriteria(cnRepid NUMBER, csEntity VARCHAR2, csMasterkey VARCHAR2) IS
    SELECT stc.KEY, stc.display, stc.info
      FROM isr$crit stc
     WHERE stc.entity = csEntity
       AND stc.repid = cnRepId
       AND stc.MASTERKEY = csMasterkey;
       
  rGetCriteria cGetCriteria%rowtype;

  CURSOR cGetOrdernumber IS
    SELECT ordernumber, KEY, masterkey
      FROM isr$crit
     WHERE repid = nRepId
       AND entity = oCurrentEntList (1).sEnt
    ORDER BY ordernumber;

  sFlag                VARCHAR2(1);  -- dummy, is not evaluated
  
  lToAdds iSR$Crit$List;
  sMaster varchar2(500);
  
  nIx pls_integer;
  
  CURSOR cIsEditable IS
    SELECT editable
      FROM isr$meta$grid$entity
     WHERE entityname = sEntity
       AND reporttypeid = STB$OBJECT.getCurrentReporttypeId;
    
  sEditable ISR$META$GRID$ENTITY.EDITABLE%TYPE;
  oParamList iSR$ParamList$Rec := iSR$ParamList$Rec(); --parameter list

BEGIN
  isr$trace.stat('begin', 'begin', sCurrentName);
  
  nRepId := Stb$object.getCurrentRepId;

  oSelection := ISR$TLRSELECTION$LIST();

  if sEntity = oCurrentEntList(1).sEnt then
   ------------------------- master entity -------------------------------------

    nLineIndex := 0;
    isr$trace.debug('direct call to Collector','Call of iSR$Remote$Collector.GetWizardList '||sEntity,sCurrentName);
    
    -- fill Param List with values
    oParamList.AddParam('REPORTTYPEID', STB$OBJECT.getCurrentReporttypeId);
    oParamList.AddParam('REPID', STB$OBJECT.getCurrentRepid);
    -- other params here ?....
    oError := ISR$REMOTE$COLLECTOR.GetWizardList(sEntity, oParamList, oSelection, sFlag);
    
    isr$trace.debug('after direct call to Collector','Call of iSR$Remote$Collector.GetWizardList '||sEntity,sCurrentName);
    IF oError.sSeverityCode!=Stb$typedef.cnNoError THEN      
      RETURN oError;
    END IF;

    nLineIndex := oSelection.count;
    
    -- extract parent master from last masterkey
    if oSelection.count > 0 then
      sMaster := oSelection(oSelection.last).sMasterKey;
      sMaster := stb$util.getRightDelim(sMaster, '#@#');
    end if;    
    
    oError := GetGridMaskAdds(sEntity, sMaster, lToAdds);
    
    if lToAdds.count > 0 then
      for nI in lToAdds.first..lToAdds.last loop
        nLineIndex := nLineIndex + 1;
        oSelection.extend;
        oSelection(oSelection.last) := ISR$OSELECTION$RECORD(lToAdds(nI).Display, lToAdds(nI).Key, lToAdds(nI).Info, 'T', 1000000+nLineIndex, lToAdds(nI).masterkey/*Key||'#@#'||sMaster*/, ISR$ATTRIBUTE$LIST());
      end loop;
    end if;
    
    isr$trace.debug('before insert','nLineIndex : ' ||nLineIndex ,sCurrentName);
   
    DELETE FROM isr$crit
     WHERE entity = oCurrentEntList (1).sEnt
       AND repid = nRepId;       
     
    nCounter := oSelection.First;
    WHILE oSelection.EXISTS(nCounter) LOOP
      nLineIndex := nCounter;
      isr$trace.debug('loop','nCounterx : ' ||nCounter,sCurrentName);
      isr$trace.debug('master key',Trim(oSelection(nCounter).sMasterkey),sCurrentName);
      isr$trace.debug('key',Trim(oSelection(nCounter).sValue),sCurrentName);
      isr$trace.debug('ordernumber',oSelection(nCounter).nOrderNum,sCurrentName);
  
      INSERT INTO isr$crit (entity
                          , MASTERKEY
                          , KEY
                          , display
                          , info
                          , repid
                          , selected
                          , ordernumber)
      VALUES (sEntity
            , TRIM (oSelection (nCounter).sMasterkey)
            , TRIM (oSelection (nCounter).sValue)
            , TRIM (oSelection (nCounter).sDisplay)
            , TRIM (oSelection (nCounter).sInfo)
            , nRepID
            , NVL (oSelection (nCounter).sSelected, 'T')
            , oSelection (nCounter).nOrderNum);
              
      nCounter := oSelection.NEXT(nCounter);
    END LOOP;
  
    sFound := 'T';

    isr$trace.debug('number of entries','nLineIndex : ' ||nLineIndex ,100);
    
    IF nLineIndex=0 THEN
      isr$trace.debug('NO entries','DELETE ALL dependent ', 50);
      nIx := oCurrentEntList.first;
      WHILE nIx is not null LOOP
        IF nIx > 1 THEN
          DELETE FROM isr$crit
           WHERE entity = oCurrentEntList (nIx).sEnt
             AND repid = nRepId;
        END IF;
        nIx := oCurrentEntList.next(nIx);
      END LOOP;
    ELSE
      nIx := oCurrentEntList.first;
      WHILE nIx is not null LOOP
        IF nIx > 1 THEN
          DELETE FROM isr$crit
           WHERE repid = nRepId
             AND entity = oCurrentEntList (nIx).sEnt
             AND MASTERKEY NOT IN (SELECT cr.MASTERKEY
                                     FROM isr$crit cr
                                    WHERE repid = nRepid
                                      AND entity = oCurrentEntList (1).sEnt);

          --update the order number of the  not modified values
          FOR rGetOrderNumber IN cGetOrdernumber LOOP
            isr$trace.debug('masterkey',rGetOrderNumber.masterkey || ' mit KEY: ' ||rGetOrderNumber.KEY, 70);
            isr$trace.debug('orderno',rGetOrderNumber.ordernumber, 70);

            UPDATE isr$crit
               SET ordernumber = rGetOrderNumber.ordernumber
             WHERE entity = oCurrentEntList (nIx).sEnt
               AND MASTERKEY||'#@#'||key = rGetOrderNumber.masterkey||'#@#'||rGetOrderNumber.KEY
               AND repid = nRepId;
          END LOOP;
        END IF;
        nIx := oCurrentEntList.next(nIx);
      END LOOP;
    END IF;     
     
  ELSE
     ------------------------- subsequent entities ---------------------------------------------------------------------------------    
    nLineIndex := oSelection.count();
    
    OPEN cIsEditable;
    FETCH cIsEditable INTO sEditable;
    CLOSE cIsEditable;    
    
    FOR rGetGridCol1 IN ( SELECT masterkey, selected, ordernumber
                            FROM isr$crit
                           WHERE entity = oCurrentEntList (1).sEnt
                             AND repid = nRepId
                           ORDER BY 3 ) LOOP
    
      isr$trace.debug('masterkey ' || oCurrentEntList (1).sEnt,rGetGridCol1.masterkey||'/'||rGetGridCol1.ordernumber ,70);
            
      oSelection.EXTEND;
      oSelection (oSelection.COUNT()) := ISR$OSELECTION$RECORD(null, null, null, 'F', rGetGridCol1.ordernumber, rGetGridCol1.masterkey);

      oSelection(oSelection.COUNT()).tloAttributeList.EXTEND();
      isr$trace.debug('sEditable '||sEntity,sEditable,70);
      oSelection(oSelection.COUNT()).tloAttributeList(oSelection(oSelection.COUNT()).tloAttributeList.COUNT()) := ISR$ATTRIBUTE$RECORD('EDITABLE', replace(sEditable, 'C', 'T'), null);
      oSelection(oSelection.COUNT()).tloAttributeList.EXTEND();
      oSelection(oSelection.COUNT()).tloAttributeList(oSelection(oSelection.COUNT()).tloAttributeList.COUNT()) := ISR$ATTRIBUTE$RECORD('CHANGEABLE',  case when sEditable = 'C' then 'T' else 'F' end, null);
      isr$trace.debug('is editable?',oSelection.COUNT(),155,ISR$XML.XMLToClob(XMLTYPE(oSelection(oSelection.COUNT()))));

    END LOOP;
    
    execute immediate
          'BEGIN
             :1 :='
       || STB$UTIL.getCurrentCustomPackage
       || '.'
       || 'getDefaultValues'
       || '(:2,:3,:4);
           END;'
          using OUT oError, IN OUT oSelection, IN 'DEFAULT_SELECTION_'||sEntity, IN sEntity;

    isr$trace.debug('sEntity',oSelection.COUNT(),70);
    
    FOR nLineIndex IN 1..oSelection.COUNT() LOOP
    
      isr$trace.debug('oSelection('||nLineIndex||').sMasterkey',oSelection(nLineIndex).sMasterkey,70);

      OPEN cGetCriteria(nRepid, sEntity, oSelection(nLineIndex).sMasterkey);
      FETCH cGetCriteria INTO rGetCriteria;
      IF cGetCriteria%FOUND THEN      
        isr$trace.debug('oSelection('||nLineIndex||')','Found crit for sEntity '||sEntity||'/masterkey '||oSelection(nLineIndex).sMasterkey||' '||rGetCriteria.key,70);
        oSelection(nLineIndex).sValue := rGetCriteria.key;
        oSelection(nLineIndex).sDisplay := rGetCriteria.display;
        oSelection(nLineIndex).sInfo := rGetCriteria.info;
      ELSE
        isr$trace.debug('before GetEntityDefault','oselection see logclob->',sCurrentName, oSelection);
        IF NVL(oSelection (nLineIndex).sSelected, 'F') = 'F' THEN       
          -- check if entity default is filled
          oCritRec := GetEntityDefault (sEntity, oSelection(nLineIndex).sMasterkey);

          oSelection(nLineIndex).sValue := oCritRec.key;
          oSelection(nLineIndex).sDisplay := oCritRec.display;
          oSelection(nLineIndex).sInfo := oCritRec.info;
          -- TR, 201512111748: specification was not saved by default
          oSelection(nLineIndex).sSelected := 'T';
        END IF;
      
        -- insert into criteria
        nCounter := nCounter + 1;
        INSERT INTO isr$crit (entity
                            , MASTERKEY
                            , KEY
                            , display
                            , info
                            , repid
                            , selected
                            , ordernumber)
        VALUES (sEntity
              , TRIM (oSelection (nLineIndex).sMasterkey)
              , TRIM (oSelection (nLineIndex).sValue)
              , TRIM (oSelection (nLineIndex).sDisplay)
              , TRIM (oSelection (nLineIndex).sInfo)
              , nRepID
              , oSelection (nLineIndex).sSelected
              , oSelection (nLineIndex).nOrderNum);
      END IF;
      CLOSE cGetCriteria;
    
    END LOOP;
    
    --callback to custom package
    isr$trace.debug('before callback',null,150);
    execute immediate
      'BEGIN
         :1 :='|| STB$UTIL.getCurrentCustomPackage||'.PostGridSelection(:2);
       END;'
    USING out oError, in sEntity;
    isr$trace.debug('after callback',null,150);
    
    isr$trace.debug('number of entries in current column','nLineIndex : ' ||nLineIndex ,100);
   END IF;

   
  isr$trace.stat('end', 'end', sCurrentName);
  RETURN oError;

EXCEPTION
  WHEN NO_DATA_FOUND THEN
    oError.handleError ( Stb$typedef.cnSeverityCritical, utd$msglib.getmsg ('exUnexpectedError', stb$security.getCurrentlanguage,  csP1 => sqlerrm(sqlcode)), 
        sCurrentName, dbms_utility.format_error_stack ||chr(10)|| dbms_utility.format_error_backtrace );
    RETURN oError;
  WHEN OTHERS THEN
     oError.handleError ( Stb$typedef.cnSeverityCritical, utd$msglib.getmsg ('exUnexpectedError', stb$security.getCurrentlanguage,  csP1 => sqlerrm(sqlcode)), 
        sCurrentName, dbms_utility.format_error_stack ||chr(10)|| dbms_utility.format_error_backtrace );
    RETURN oError;
END GetGridSelection;

--******************************************************************************
FUNCTION GetGridDependency(csEntity IN VARCHAR2 , oEntityList OUT STB$ENTITY$LIST, csKey IN VARCHAR2)  RETURN STB$OERROR$RECORD
IS
  oError                 STB$OERROR$RECORD := STB$OERROR$RECORD();
  sCurrentName   constant varchar2(4000) := $$PLSQL_UNIT||'.GetGridDependency('||csEntity||')';
  
  cursor cGetDependendCols(csColEntity IN VARCHAR2) is
    select distinct childentityname
    from   (  SELECT DISTINCT r.childentityname, r.parententityname
                FROM isr$meta$relation r, isr$meta$relation$report rr
               WHERE r.childentityname IN
                           (SELECT entityname
                              FROM isr$meta$grid$entity
                             WHERE reporttypeid = STB$OBJECT.getCurrentReporttypeid
                               AND wizardentity = sCurrentWZEntity
                               AND columnpos > 2)
                 AND r.parententityname IN
                           (SELECT entityname
                              FROM isr$meta$grid$entity
                             WHERE reporttypeid = STB$OBJECT.getCurrentReporttypeid
                               AND wizardentity = sCurrentWZEntity)
                 AND rr.relationname = r.relationname
                 AND rr.reporttypeid = STB$OBJECT.getCurrentReporttypeid  )
      START WITH parententityname = csColEntity
      CONNECT BY parententityname = PRIOR childentityname;

  rGetDependendCols       cGetDependendCols%rowtype;
BEGIN
  isr$trace.stat('begin', 'begin', sCurrentName);
  isr$trace.debug('PARAMETERS','csEntity: ' || csEntity || ' csKey: ' || csKey || ' sCurrentWZEntity: ' || sCurrentWZEntity, sCurrentName);

  oEntityList := STB$ENTITY$LIST();

  FOR i IN 1..oCurrentEntList.Count()  LOOP
    isr$trace.debug('oCurrentEntList(i).sEnt',oCurrentEntList(i).sEnt,sCurrentName);
    oEntityList.EXTEND;
    oEntityList(i) :=  STB$ENTITY$RECORD(oCurrentEntList(i).sEnt, 'F', '');
  END LOOP;

  FOR rGetDependendCols IN cGetDependendCols(csEntity) LOOP
    isr$trace.debug('delete from isr$crit',rGetDependendCols.childEntityName,sCurrentName);

    DELETE FROM ISR$CRIT
     WHERE entity IN (rGetDependendCols.childentityname)
       AND masterkey = csKey
       AND repid = STB$OBJECT.getCurrentRepid;
    FOR i IN 1..oEntityList.Count()  LOOP
      IF rGetDependendCols.childentityname = oEntityList(i).sEnt THEN
        isr$trace.debug('preselect '||i,oEntityList(i).sEnt,sCurrentName);
        oEntityList(i).sPreSelected := 'T';
      END IF;
    END LOOP;

  END LOOP;

  isr$trace.stat('end', 'end', sCurrentName);
  RETURN oError;

EXCEPTION
  WHEN NO_DATA_FOUND THEN
     oError.handleError ( Stb$typedef.cnSeverityCritical, utd$msglib.getmsg ('exUnexpectedError', stb$security.getCurrentlanguage,  csP1 => sqlerrm(sqlcode)), 
        sCurrentName, dbms_utility.format_error_stack ||chr(10)|| dbms_utility.format_error_backtrace );
    RETURN oError;
  WHEN OTHERS THEN
    oError.handleError ( Stb$typedef.cnSeverityCritical, utd$msglib.getmsg ('exUnexpectedError', stb$security.getCurrentlanguage,  csP1 => sqlerrm(sqlcode)), 
        sCurrentName, dbms_utility.format_error_stack ||chr(10)|| dbms_utility.format_error_backtrace );
    RETURN oError;
END GetGridDependency;

end ISR$GRID$MASK;