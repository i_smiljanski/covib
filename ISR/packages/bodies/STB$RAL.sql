CREATE OR REPLACE PACKAGE BODY Stb$ral
is
-- *********************************************************************************************************************
  PROCEDURE saveJobProgress(cnTime IN NUMBER, csText IN VARCHAR2, cnActionid IN NUMBER)
  is
    PRAGMA autonomous_transaction;
    sCurrentName   constant varchar2(4000) := $$PLSQL_UNIT||'.saveJobProgress';
  BEGIN
    isr$trace.stat('begin', 'begin', sCurrentName);
    INSERT INTO ISR$JOB$PROGRESS(TIME, TEXT, ACTIONID) VALUES (cnTime, csText, cnActionid);
    commit;
    isr$trace.stat('end', 'end', sCurrentName);
  END saveJobProgress;
-- *********************************************************************************************************************
  PROCEDURE createReport(cnJobId in NUMBER)
  is
  --Variables
    oErrorObj               STB$OERROR$RECORD := STB$OERROR$RECORD();
    sCurrentName    constant varchar2 (100) := $$PLSQL_UNIT || '.createReport('||cnJobId||')';
    sUserMsg        varchar2(4000);  
    exActionError           EXCEPTION;
    xXml                    XMLTYPE;
    
    sStatement              VARCHAR2(4000);
    dStartdate              DATE;

    -- ***************************************      XMLDOM und Dyn. SQL       ************************************
    -- XMLDOM is not a sql type and can't be passed as parameter in Dyn. SQL
    -- Workaround define the dom as number (in rhe dom definition ID NUMBER NOT NULL := -1 )

    CURSOR cGetAction is
      SELECT functiontocall
           , description
           , (SELECT SUM (progress_estimated)
                FROM ISR$JOB$PROGRESS$VIEW
               WHERE actionid = jpv.actionid
                 AND actionorder < jpv.actionorder)
                progress
           , (SELECT SUM (progress_estimated)
                FROM ISR$JOB$PROGRESS$VIEW
               WHERE actionid = jpv.actionid
                 AND actionorder <= jpv.actionorder)
                endprogress
           , time_estimated
           , (select alertError from isr$action$job aj where actionid = jpv.actionid and aj.functiontocall = jpv.functiontocall 
              and aj.actionorder=jpv.actionorder) alertError  --IS, 30.01.2019: isr$action$job kann mehrere gleiche FUNCTIONCALL haben, mit unterschiedlichen ACTIONORDER 
        FROM ISR$JOB$PROGRESS$VIEW jpv
       WHERE actionid = STB$JOB.getJobParameter('ACTIONID',cnJobid)
      ORDER BY actionorder;

  BEGIN
   isr$trace.stat('begin','cnjobId: ' || cnJobId, sCurrentName);

   oErrorObj := isr$util.initializeSession;  -- [ISRC-1108]
    EXECUTE immediate 'alter session set EVENTS ''44410 trace name context forever, level 8''';    

    IF STB$JOB.getJobParameter('USER',cnJobid) is not null then
      Stb$security.SetCurrentLanguage(STB$JOB.getJobParameter('USER',cnJobid));
    END IF;

   --  set the application and system in v$session added JB 27 May 2008
    STB$GAL.setApplicationInfo;
   -- set the oracle jobid
    dbms_application_info.set_client_info('ISR_JOBID: '  || cnJobid ||  ' / REFERENCE: '|| STB$JOB.getJobParameter('REFERENCE',cnJobid));  
    dbms_application_info.set_action(STB$JOB.getJobParameter('TOKEN',cnJobid));  
  
    -- get the actions
    for rGetAction in cGetAction LOOP
      dStartdate := sysdate;
      isr$trace.debug('next action','action: ' ||rGetAction.functiontocall, sCurrentName);

      STB$JOB.setJobProgress( cnJobid,
                              STB$TYPEDEF.cnReportProgress,
                              rGetAction.description,
                              rGetAction.progress );
                                
      STB$JOB.setJobParameter('STARTPROGRESS', rGetAction.progress, cnJobid);
      STB$JOB.setJobParameter('ENDPROGRESS', rGetAction.endprogress, cnJobid);
      STB$JOB.setJobParameter('TIME_ESTIMATED', rGetAction.time_estimated, cnJobid);
      STB$JOB.setJobParameter('ALERTERROR', rGetAction.alertError, cnJobid);

      BEGIN
          sStatement := 'BEGIN '
                     || '  :1 := '||rGetAction.functiontocall||'(:2, :3); '
                     || 'END; ';

       --isr$trace.debug('rGetAction.functiontocall', rGetAction.functiontocall, sCurrentName, sStatement);

        EXECUTE immediate sStatement USING OUT oErrorObj, IN cnJobId, IN OUT xXml;

      EXCEPTION
       WHEN OTHERS then
         isr$trace.error('oErrorObj.sSeverityCode', SQLCODE|| ' ' || SQLERRM, sCurrentName);
      END;

      isr$trace.debug('oErrorObj', rGetAction.functiontocall || ' ' || oErrorObj.sSeverityCode || ' ' || rGetAction.alertError, sCurrentName, oErrorObj);
      
      IF (oErrorObj.sSeverityCode =Stb$typedef.cnSeverityCritical or oErrorObj.sSeverityCode is null) then
        IF NVL(rGetAction.alertError, 'F') = 'F' THEN
          stb$job.logError(oErrorObj.sErrorCode, oErrorObj.sErrorMessage, oErrorObj.sSeverityCode);
          stb$job.setJobParameter('JOB_USERSTACK', oErrorObj.GetUserMessages, cnJobid);
          stb$job.setJobParameter('JOB_IMPLSTACK', oErrorObj.GetImplMessages, cnJobid);
          stb$job.setJobParameter('JOB_DEVSTACK', oErrorObj.GetDevMessages, cnJobid);
        else
          RAISE exActionError;
        end if;
      END IF;
      
      saveJobProgress(sysdate-dStartdate, rGetAction.functiontocall, STB$JOB.getJobParameter('ACTIONID',cnJobid));
    END LOOP;

   isr$trace.stat('end','end', sCurrentName);

  EXCEPTION
    WHEN exActionError then 
      sUserMsg := utd$msglib.getmsg ('exActionError', stb$security.getcurrentlanguage,  sqlerrm(sqlcode));
      oErrorObj.handleError ( Stb$typedef.cnSeverityCritical, sUserMsg, sCurrentName, 
                dbms_utility.format_error_stack || CHR(10) || dbms_utility.format_error_backtrace);  
    WHEN OTHERS then
      sUserMsg := utd$msglib.getmsg ('exUnexpectedError', stb$security.getcurrentlanguage,  sqlerrm(sqlcode));
      oErrorObj.handleError ( Stb$typedef.cnSeverityCritical, sUserMsg, sCurrentName, 
                dbms_utility.format_error_stack || CHR(10) || dbms_utility.format_error_backtrace);  
END createReport;

END Stb$ral;