CREATE OR REPLACE PACKAGE BODY ISR$METADATA$COLLECTOR$PACKAGE AS

--******************************************************************************
FUNCTION getEntities RETURN CLOB is
  sCurrentName   constant varchar2(4000) := $$PLSQL_UNIT||'.getEntities';
begin
  isr$trace.stat('begin', 'begin', sCurrentName);
  IF stb$object.getCurrentReporttypeid IS NULL THEN 
    stb$object.setCurrentReporttypeid(0);
  END IF;  

  return DBMS_XMLQUERY.GetXML ('SELECT DISTINCT d.entityname, leaf, MAX (orderno) orderno
                                    FROM (SELECT ROWNUM orderno, entityname, leaf
                                            FROM (SELECT *
                                                    FROM isr$collector$entities
                                                  ORDER BY branch, ordercol, leaf desc)) d
                                  GROUP BY d.entityname, leaf
                                  ORDER BY 3');
                                  
  /*return DBMS_XMLQUERY.GetXML ('SELECT *
                                    FROM isr$collector$entities
                                  ORDER BY branch, ordercol, leaf desc');         */     
  isr$trace.stat('end', 'end', sCurrentName);                                  
end getEntities;  
          
--******************************************************************************
FUNCTION getEntityDescription(csEntity in varchar2) RETURN CLOB is
  sCurrentName   constant varchar2(4000) := $$PLSQL_UNIT||'.getEntityDescription('||csEntity||')';
begin
  isr$trace.stat('begin', 'begin', sCurrentName);
  return DBMS_XMLQUERY.GetXML ('SELECT attrname, datatype, iskey
                                    FROM isr$meta$attribute 
                                   WHERE entityname = '''||csEntity||''' 
                                  ORDER BY orderno');
  isr$trace.stat('end', 'end', sCurrentName);                                  
end getEntityDescription;

--******************************************************************************
FUNCTION getEntityRelations(csEntity in varchar2) RETURN CLOB is
  sCurrentName   constant varchar2(4000) := $$PLSQL_UNIT||'.getEntityRelations('||csEntity||')';
  clReturn       clob;
begin
  isr$trace.stat('begin', 'begin', sCurrentName);
  clReturn :=  DBMS_XMLQUERY.GetXML ('SELECT DISTINCT parententityname
                                                    , parentattrname
                                                    , childentityname
                                                    , childattrname
                                        FROM isr$meta$relation$report rr, isr$meta$relation r
                                       WHERE rr.reporttypeid = STB$OBJECT.getCurrentReporttypeid
                                         AND rr.relationname = r.relationname
                                         AND r.parententityname = '''||csEntity||'''');
  isr$trace.stat('end', 'end', sCurrentName);
  return clReturn;                                      
end getEntityRelations;

END ISR$METADATA$COLLECTOR$PACKAGE;