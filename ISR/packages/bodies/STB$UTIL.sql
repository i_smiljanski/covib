CREATE OR REPLACE PACKAGE BODY stb$util IS

  crlf                constant varchar2(2) := chr(10)||chr(13);
  sJavaDateFormat     constant VARCHAR2(100) := STB$UTIL.getSystemParameter ('JAVA_DATE_FORMAT');
  -- for UCIS-198 and ISRC-1306
  --sCurrentLal         constant VARCHAR2(100) := STB$UTIL.getCurrentLal;

  --***********************************************************************************************************
  FUNCTION putslist (sentity IN stb$typedef.tline, oselection IN isr$tlrselection$list)
    RETURN stb$oerror$record
  IS
    sCurrentName   constant varchar2(4000) := $$PLSQL_UNIT||'.putslist('||sentity||')';
    oerror                        stb$oerror$record := STB$OERROR$RECORD();
    exNoLines                     EXCEPTION;
    nlineindex                    NUMBER;
    nrepid                        NUMBER;
  BEGIN
    isr$trace.stat('begin', 'begin', sCurrentName);
    --get the actual report id
    nrepid := stb$object.getcurrentrepid;

    IF (oselection.count = 0) THEN
      isr$trace.debug ('no values in the record',  'no values in the record ', sCurrentName);
      DELETE FROM isr$crit
            WHERE repid = nrepid AND entity = sentity;
      isr$trace.debug ('after deleting',  'after deleting old values ', sCurrentName);
      isr$trace.debug ('no values end',  'no values end ', sCurrentName);
      RETURN oerror;
    ELSE
      DELETE FROM isr$crit
            WHERE repid = nrepid AND entity = sentity;
      isr$trace.debug ('after deleting',  'after deleting old values', sCurrentName);
      -- write the new values into isr$crit
      nlineindex := oselection.first;
      isr$trace.debug ('nLineIndex',  'nLineIndex :' || nlineindex, sCurrentName);

      WHILE (oselection.EXISTS (nlineindex)) AND (oerror.serrormessage IS NULL) LOOP
        isr$trace.debug ('before ' || trim (oselection (nlineindex).svalue) || '+' || oselection (nlineindex).sdisplay,  trim (oselection (nlineindex).smasterkey) || ' ' || trim (oselection (nlineindex).sselected), sCurrentName);

        IF (oselection (nlineindex).svalue IS NOT NULL) AND (oselection (nlineindex).sdisplay IS NOT NULL) THEN
          IF (oselection (nlineindex).sselected = 'T') THEN
            isr$trace.debug (trim (oselection (nlineindex).svalue),  trim (oselection (nlineindex).smasterkey) || ' ' || trim (oselection (nlineindex).nordernum), sCurrentName);
            INSERT INTO isr$crit
                        (entity, KEY, masterkey, display, info,
                         repid, selected, ordernumber)
                 VALUES (sentity, trim (oselection (nlineindex).svalue), trim (oselection (nlineindex).smasterkey), trim (oselection (nlineindex).sdisplay), trim (oselection (nlineindex).sinfo),
                         nrepid, trim (oselection (nlineindex).sselected), nvl (trim (oselection (nlineindex).nordernum), nlineindex));
          END IF;
        ELSE
          isr$trace.debug ('null value',  'null value', sCurrentName);
          EXIT;
        END IF;

        nlineindex := oselection.NEXT (nlineindex);
      END LOOP;

      isr$trace.debug ('oSelection',  'number of lines in oSelection: ' || oselection.count (), sCurrentName);
    END IF;

    isr$trace.stat('end', 'nLineIndex: '||nLineIndex, sCurrentName);
    RETURN oerror;
  EXCEPTION
    WHEN exNoLines THEN
      oerror.sErrorCode := utd$msglib.getmsg ('exNoLines', stb$security.getcurrentlanguage);
      oError.handleError ( Stb$typedef.cnSeverityCritical, utd$msglib.getmsg ('exNoLinesText', stb$security.getCurrentlanguage,  csP1 => sqlerrm(sqlcode)),
        sCurrentName, dbms_utility.format_error_stack ||chr(10)|| dbms_utility.format_error_backtrace );
      RETURN oerror;
    WHEN OTHERS THEN
      oError.handleError ( Stb$typedef.cnSeverityCritical, utd$msglib.getmsg ('exUnexpectedError', stb$security.getCurrentlanguage,  csP1 => sqlerrm(sqlcode)),
        sCurrentName, dbms_utility.format_error_stack ||chr(10)|| dbms_utility.format_error_backtrace );
      RETURN oerror;
  END putslist;

  --***********************************************************************************************************
  FUNCTION putSListGrid (sentity IN stb$typedef.tline, ssuchwert IN stb$typedef.tline, oselection IN isr$tlrselection$list)
    RETURN stb$oerror$record IS
    --Variables
    oerror                        stb$oerror$record := STB$OERROR$RECORD();
    sCurrentName        constant varchar2 (100) := $$PLSQL_UNIT || '.putSListGrid('||sEntity||')';
    exNoLines                     EXCEPTION;
    nlineindex                    NUMBER;
    nrepid                        NUMBER;
  BEGIN
    isr$trace.stat('begin', 'begin', sCurrentName);
    --get the actual report id
    nrepid := stb$object.getcurrentrepid;
    -- write the new values into isr$crit
    nlineindex := oselection.first;
    isr$trace.debug ('nLineIndex',  'nLineIndex :' || nlineindex, sCurrentName);

    WHILE (oselection.EXISTS (nlineindex)) AND (oerror.serrormessage IS NULL) LOOP
      IF (oselection (nlineindex).svalue IS NOT NULL) AND (oselection (nlineindex).sdisplay IS NOT NULL) THEN
        IF (oselection (nlineindex).sselected = 'T') THEN
          -- always one vlaue -> delete the old value and re-write the field
          DELETE FROM isr$crit
                WHERE repid = nrepid AND entity = sentity AND masterkey = trim (oselection (nlineindex).smasterkey);
          isr$trace.debug ('MasterKey',  oselection (nlineindex).smasterkey, sCurrentName);
          isr$trace.debug('Key',  trim (oselection (nlineindex).svalue), sCurrentName, oselection);
          INSERT INTO isr$crit
                      (entity, KEY, masterkey, display, info,
                       repid, selected, ordernumber)
               VALUES (sentity, trim (oselection (nlineindex).svalue), trim (oselection (nlineindex).smasterkey), trim (oselection (nlineindex).sdisplay), trim (oselection (nlineindex).sinfo),
                       nrepid, trim (oselection (nlineindex).sselected), nvl (trim (oselection (nlineindex).nordernum), nlineindex));
        END IF;
      ELSE
        isr$trace.debug ('null value',  'null value', sCurrentName);
        EXIT;
      END IF;

      nlineindex := oselection.NEXT (nlineindex);
    END LOOP;

    isr$trace.stat('end', 'nLineIndex: '||nLineIndex, sCurrentName);
    RETURN oerror;
  EXCEPTION
    WHEN exNoLines THEN
      oerror.sErrorCode := utd$msglib.getmsg ('exNoLines', stb$security.getcurrentlanguage);
      oError.handleError ( Stb$typedef.cnSeverityCritical, utd$msglib.getmsg ('exNoLinesText', stb$security.getCurrentlanguage,  csP1 => sqlerrm(sqlcode)),
        sCurrentName, dbms_utility.format_error_stack ||chr(10)|| dbms_utility.format_error_backtrace );
      RETURN oerror;
    WHEN OTHERS THEN
      oError.handleError ( Stb$typedef.cnSeverityCritical, utd$msglib.getmsg ('exUnexpectedError', stb$security.getCurrentlanguage,  csP1 => sqlerrm(sqlcode)),
        sCurrentName, dbms_utility.format_error_stack ||chr(10)|| dbms_utility.format_error_backtrace );
      RETURN oerror;
  END putslistgrid;

  --********************************************************************************************************************
  FUNCTION checkcriteria (cnrepid IN NUMBER, csentity IN VARCHAR2, sfound OUT VARCHAR2, oselection OUT isr$tlrselection$list)
    RETURN stb$oerror$record
  IS
    sCurrentName   constant varchar2(4000) := $$PLSQL_UNIT||'.checkcriteria('||csEntity||','||cnRepid||')';
    oerror                        stb$oerror$record := STB$OERROR$RECORD();

    --cursor
    CURSOR cgetexisting IS
      SELECT DISTINCT stc.KEY KEY, stc.display display, stc.masterkey masterkey, stc.info info, stc.selected selected, stc.ordernumber ordernum
      FROM            iSR$Crit stc
      WHERE           stc.entity = csentity AND stc.repid = cnrepid AND stc.selected = 'T'
      ORDER BY        ordernum;

    CURSOR cgetexistingmasterdetail (csmasterentity IN VARCHAR2) IS
      SELECT DISTINCT stc.KEY KEY, stc.display display, stc.masterkey masterkey, stc.info info, stc.selected selected, stc.ordernumber ordernum, stc1.ordernumber ordernummaster
      FROM            iSR$Crit stc, iSR$Crit stc1
      WHERE           stc.entity = csentity
      AND             stc.repid = cnrepid
      AND             stc.selected = 'T'
      AND             stc1.entity = csmasterentity
      AND             (stc.masterkey = stc1.KEY OR instr(stc.masterkey, stc1.masterkey|| '#@#') = 1 )
      AND             stc.repid = stc1.repid
      ORDER BY        ordernummaster, ordernum;

    CURSOR cgetmasktype (sentity IN isr$report$wizard.entity%TYPE) IS
      SELECT GetGroupEntity( sentity, w.reporttypeid) GroupEntity,
             w.masktype,
             w.reporttypeid,
             w.maskno
      FROM   isr$report$wizard w, stb$report r
      WHERE  w.entity = sentity
        AND w.reporttypeid = r.reporttypeid
        AND r.repid = cnrepid;

    rgetmasktype                  cgetmasktype%ROWTYPE;
  BEGIN
    isr$trace.stat('begin', 'begin', sCurrentName);
    oselection := isr$tlrselection$list ();
    sfound := 'F';
    --check selection Type
    OPEN cgetmasktype (csentity);
    FETCH cgetmasktype
    INTO  rgetmasktype;
    CLOSE cgetmasktype;
    isr$trace.debug ('entity/masktype/master entity', 'STB$UTIL.checkCriteria', csentity || '/' || rgetmasktype.masktype || '/' || rgetmasktype.GroupEntity || '/' || stb$util.getcurrentcustompackage, sCurrentName);

    IF rgetmasktype.masktype = stb$typedef.cnmasterdetail THEN
      FOR rgetexist IN cgetexistingmasterdetail (rgetmasktype.GroupEntity) LOOP
        sfound := 'T';
        oselection.EXTEND;
        oselection (oselection.COUNT()) := ISR$OSELECTION$RECORD(rgetexist.display, rgetexist.KEY, rgetexist.info, rgetexist.selected, oselection.COUNT(), rgetexist.masterkey || '#@#' || rgetexist.KEY);
      END LOOP;
      isr$trace.debug ('end', 'STB$UTIL.checkCriteria', 'number of lines in cGetExisting : ' || oselection.COUNT(), sCurrentName);
    ELSE
      FOR rgetexist IN cgetexisting LOOP
        sfound := 'T';
        oselection.EXTEND;
        oselection (oselection.COUNT()) := ISR$OSELECTION$RECORD(rgetexist.display, rgetexist.KEY, rgetexist.info, rgetexist.selected, oselection.COUNT(), rgetexist.masterkey);
      END LOOP;
      isr$trace.debug ('end', 'STB$UTIL.checkCriteria', 'number of lines in cGetExisting : ' || oselection.COUNT(), sCurrentName);
    END IF;

    isr$trace.stat('end', 'end', sCurrentName);
    RETURN oerror;
  EXCEPTION
    WHEN OTHERS THEN
      oError.handleError ( Stb$typedef.cnSeverityCritical, utd$msglib.getmsg ('exUnexpectedError', stb$security.getCurrentlanguage,  csP1 => sqlerrm(sqlcode)),
        sCurrentName, dbms_utility.format_error_stack ||chr(10)|| dbms_utility.format_error_backtrace );
      RETURN (oerror);
  END checkcriteria;

  --********************************************************************************************************************
  FUNCTION getcriteria (cnrepid IN NUMBER, csentity IN VARCHAR2, sfound OUT VARCHAR2, oselection OUT isr$tlrselection$list)
    RETURN stb$oerror$record
  IS
    sCurrentName   constant varchar2(4000) := $$PLSQL_UNIT||'.getcriteria('||csEntity||','||cnRepid||')';
    oerror                        stb$oerror$record := stb$oerror$record ();

    --cursor
    CURSOR cgetexisting IS
      SELECT   stc.KEY KEY, stc.display display, masterkey masterkey, stc.info info, stc.selected selected, stc.ordernumber ordernum
      FROM     iSR$Crit stc
      WHERE    stc.entity = csentity AND stc.repid = cnrepid AND stc.selected = 'T'
      ORDER BY ordernum;

    CURSOR cgetexistingmasterdetail (csmasterentity IN VARCHAR2) IS
      SELECT DISTINCT stc.KEY KEY, stc.display display, stc.masterkey masterkey, stc.info info, stc.selected selected, stc.ordernumber ordernum, stc1.ordernumber ordernummaster
      FROM            iSR$Crit stc, iSR$Crit stc1
      WHERE           stc.entity = csentity
      AND             stc.repid = cnrepid
      AND             stc.selected = 'T'
      AND             stc1.entity = csmasterentity
      AND             (stc.masterkey = stc1.KEY OR instr(stc.masterkey, stc1.masterkey|| '#@#') = 1 )
      AND             stc.repid = stc1.repid
      ORDER BY        ordernummaster, ordernum;

    CURSOR cgetmasktype  IS
      SELECT GetGroupEntity( csEntity, w.reporttypeid) GroupEntity,
             w.masktype,
             w.reporttypeid,
             w.maskno
      FROM   isr$report$wizard w, stb$report r
      WHERE  w.entity = csEntity
        AND w.reporttypeid = r.reporttypeid
        AND r.repid = cnrepid;

    rgetmasktype                  cgetmasktype%ROWTYPE;
  BEGIN
    isr$trace.stat('begin', 'begin', sCurrentName);
    oselection := isr$tlrselection$list ();
    sfound := 'F';
    --check selection Type
    OPEN cgetmasktype;
    FETCH cgetmasktype
    INTO  rgetmasktype;
    CLOSE cgetmasktype;
    isr$trace.debug ('masktype/master entity', 'STB$UTIL.getCriteria', rgetmasktype.masktype || ' /  ' || rgetmasktype.GroupEntity, sCurrentName);

    IF rgetmasktype.masktype = stb$typedef.cnmasterdetail THEN
      FOR rgetexist IN cgetexistingmasterdetail (rgetmasktype.GroupEntity) LOOP
        sfound := 'T';
        oselection.EXTEND;
        oSelection(oSelection.COUNT()) := ISR$OSELECTION$RECORD(rgetexist.display, rgetexist.KEY, rgetexist.info, rgetexist.selected, oselection.COUNT(), rgetexist.masterkey);
      END LOOP;
    ELSE
      isr$trace.debug ('end', 'STB$UTIL.getCriteria', 'number of lines in cGetExisting : ' || oSelection.COUNT(), sCurrentName);

      FOR rgetexist IN cgetexisting LOOP
        sfound := 'T';
        oselection.EXTEND;
        oselection (oSelection.COUNT()) := ISR$OSELECTION$RECORD(rgetexist.display, rgetexist.KEY, rgetexist.info, rgetexist.selected, oselection.COUNT(), rgetexist.masterkey);
      END LOOP;

      isr$trace.debug ('end', 'STB$UTIL.getCriteria', 'number of lines in cGetExisting : ' || oSelection.COUNT(), sCurrentName);
    END IF;

    isr$trace.stat('end', 'end', sCurrentName);
    RETURN oerror;
  EXCEPTION
    WHEN OTHERS THEN
      oError.handleError ( Stb$typedef.cnSeverityCritical, utd$msglib.getmsg ('exUnexpectedError', stb$security.getCurrentlanguage,  csP1 => sqlerrm(sqlcode)),
        sCurrentName, dbms_utility.format_error_stack ||chr(10)|| dbms_utility.format_error_backtrace );
      RETURN (oerror);
  END getcriteria;

--*************************************************************************************************************
FUNCTION checkpromptedauditrequired (stype IN VARCHAR2) RETURN VARCHAR2 IS
BEGIN
  RETURN nvl(NVL(STB$UTIL.getReptypeParameter(stb$object.getcurrentreporttypeid, stype), STB$UTIL.getSystemParameter(sType)),'F');
END checkpromptedauditrequired;

  --***********************************************************************************************************************
  FUNCTION checksum (cscheck IN BLOB, nsum OUT NUMBER)
    RETURN stb$oerror$record IS
    sCurrentName constant varchar2 (100) := $$PLSQL_UNIT || '.checksum(BLOB)';
    oerrorobj                     stb$oerror$record := STB$OERROR$RECORD();
  BEGIN
    isr$trace.stat ('begin', 'function started', sCurrentName);
    nsum := getchecksum (cscheck);
    isr$trace.stat ('end', 'end of function, nSum = ' || nsum, sCurrentName);
    RETURN oerrorobj;
  EXCEPTION
    WHEN OTHERS THEN
      oErrorObj.handleError ( Stb$typedef.cnSeverityCritical, utd$msglib.getmsg ('exUnexpectedError', stb$security.getCurrentlanguage,  csP1 => sqlerrm(sqlcode)),
        sCurrentName, dbms_utility.format_error_stack ||chr(10)|| dbms_utility.format_error_backtrace );
      RETURN oerrorobj;
  END checksum;

  --************************************************************************************************************************
  FUNCTION checksum (cscheck IN CLOB, nsum OUT NUMBER)
    RETURN stb$oerror$record IS
    sCurrentName constant varchar2 (100) := $$PLSQL_UNIT || '.checksum(CLOB)';
    oerrorobj                     stb$oerror$record := STB$OERROR$RECORD();
  BEGIN
    isr$trace.stat('begin', 'begin', sCurrentName);
    nsum := getchecksum (cscheck);
    isr$trace.debug ('status', 'STB$UTIL.checksum', 'nSum: ' || nsum, sCurrentName);
    isr$trace.stat('end', 'end', sCurrentName);
    RETURN oerrorobj;
  EXCEPTION
    WHEN OTHERS THEN
      oErrorObj.handleError ( Stb$typedef.cnSeverityCritical, utd$msglib.getmsg ('exUnexpectedError', stb$security.getCurrentlanguage,  csP1 => sqlerrm(sqlcode)),
        sCurrentName, dbms_utility.format_error_stack ||chr(10)|| dbms_utility.format_error_backtrace );
      RETURN oerrorobj;
  END checksum;

  --******************************************************************************************************************************
  FUNCTION getchecksum (obj IN BLOB)
    RETURN NUMBER AS
    LANGUAGE JAVA
    NAME 'com.uptodata.isr.dbstored.GetChecksum.printChecksum(java.sql.Blob) return java.lang.long';

  --*******************************************************************************************************************************
  FUNCTION getchecksum (obj IN CLOB)
    RETURN NUMBER AS
    LANGUAGE JAVA
    NAME 'com.uptodata.isr.dbstored.GetChecksum.printChecksum(java.sql.Clob) return java.lang.long';

  --******************************************************************************************************************************************
  FUNCTION checkfirstsign (cslimit IN VARCHAR2, strimlimit OUT VARCHAR2, sequalflag OUT VARCHAR2, slogoperator OUT VARCHAR2)
    RETURN stb$oerror$record IS
    sCurrentName   constant varchar2(4000) := $$PLSQL_UNIT||'.checkfirstsign';
    oerrorobj                     stb$oerror$record := STB$OERROR$RECORD();
    slogoperator1                 VARCHAR2 (1);
  BEGIN
    isr$trace.stat('begin','csLimit: ' || cslimit, sCurrentName);

    --check  if the first sign ">"  or"<"   are in the string
    slogoperator := substr (trim (cslimit), 1, 1);
    isr$trace.debug ('sLogOperator', 'sLogOperator: ' || slogoperator, 150);

    IF (slogoperator = '<') OR (slogoperator = '>') THEN
      strimlimit := substr (trim (cslimit), 2);
      isr$trace.debug ('1) sTrimLimit', 'sTrimLimit: ' || strimlimit, 150);
      --check  if the second sign "="  in the string
      slogoperator1 := substr (trim (strimlimit), 1, 1);
      isr$trace.debug ('sLogOperator1', 'sLogOperator1: ' || slogoperator1, 150);

      IF (slogoperator1 = '=') THEN
        strimlimit := substr (trim (strimlimit), 2);
        isr$trace.debug ('sTrimLimit', 'sTrimLimit: ' || strimlimit, 150);
        sequalflag := 'T';
      ELSE
        -- there is only one sign that has to be cut off
        strimlimit := strimlimit;
        sequalflag := 'F';
      END IF;
    ELSE
      strimlimit := cslimit;
      sequalflag := 'F';
      slogoperator := 'K';
      isr$trace.debug ('2) sTrimLimit', 'sTrimLimit: ' || strimlimit || '  sEqualFlag: ' || sequalflag, 150);
    END IF;

    isr$trace.debug ('stutus', 'sTrimLimit: ' || strimlimit || ' sEqualFlag ' || sequalflag || '  sLogOperator  ' || slogoperator, sCurrentName);
    isr$trace.stat('end', 'end', sCurrentName);
    RETURN oerrorobj;
  EXCEPTION
    WHEN OTHERS THEN
      oErrorObj.handleError ( Stb$typedef.cnSeverityCritical, utd$msglib.getmsg ('exUnexpectedError', stb$security.getCurrentlanguage,  csP1 => sqlerrm(sqlcode)),
        sCurrentName, dbms_utility.format_error_stack ||chr(10)|| dbms_utility.format_error_backtrace );
      RETURN oerrorobj;
  END checkfirstsign;

  --*************************************************************************************************************************************************
  FUNCTION getColPropertyList (sselect IN VARCHAR2, olPropertyList OUT stb$property$list)
    RETURN stb$oerror$record IS
    sCurrentName   constant varchar2(4000) := $$PLSQL_UNIT||'.getColPropertyList';
    oerrorobj                     stb$oerror$record := STB$OERROR$RECORD();
    ccursor                       INTEGER;
    rdescription                  dbms_sql.desc_tab;
    ncolcount                     NUMBER;
    ncurcol                       NUMBER;
    ndummy                        NUMBER;
    sbuffer                       VARCHAR2 (4000);
    sDateFormat                   VARCHAR2(100) := stb$typedef.csInternalDateFormat;
    sbufferdate                   DATE;
  BEGIN
    isr$trace.stat('begin', 'sSelect ' || sselect, sCurrentName);
    ccursor := dbms_sql.open_cursor;
    dbms_sql.parse (ccursor, sselect, dbms_sql.native);
    -- Execute
    ndummy := dbms_sql.execute (ccursor);
    -- get the fileds
    dbms_sql.describe_columns (ccursor, ncolcount, rdescription);

    FOR ncurcol IN 1 .. ncolcount LOOP
      IF rdescription (ncurcol).col_type = 12 THEN
        dbms_sql.define_column (ccursor, ncurcol, sbufferdate);
      ELSE
        dbms_sql.define_column (ccursor, ncurcol, sbuffer, rdescription (ncurcol).col_max_len);
      END IF;
    END LOOP;

    -- initalize the list
    olPropertyList := stb$property$list ();
    isr$trace.debug ('before', 'before', sCurrentName);

    WHILE dbms_sql.fetch_rows (ccursor) > 0 LOOP
      ncurcol := rdescription.first;

      FOR ncurcol IN 1 .. ncolcount LOOP
        IF rdescription (ncurcol).col_type = 12 THEN
          dbms_sql.column_value (ccursor, ncurcol, sbufferdate);
          sbuffer := to_char (sbufferdate, sDateFormat);
        ELSE
          dbms_sql.column_value (ccursor, ncurcol, sbuffer);
        END IF;

        isr$trace.debug (ncurcol||': '||rdescription (ncurcol).col_name, sbuffer, sCurrentName);

        olPropertyList.EXTEND;
        olPropertyList (ncurcol) := STB$PROPERTY$RECORD(rdescription (ncurcol).col_name,
                                                        utd$msglib.getmsg (rdescription (ncurcol).col_name, stb$security.getcurrentlanguage),
                                                        sbuffer, sbuffer, 'F', 'F', 'T', 'F');



        IF rdescription (ncurcol).col_max_len = 1 AND sbuffer IN ('T', 'F') THEN
          olPropertyList (ncurcol).TYPE := 'BOOLEAN';
          olPropertyList (ncurcol).srequired := 'T';
        ELSIF rdescription (ncurcol).col_max_len > 2000 THEN
          olPropertyList (ncurcol).TYPE := 'LONGSTRING';
        elsif rdescription (ncurcol).col_type = 12 THEN
          olPropertyList (ncurcol).TYPE := 'DATE';
          olPropertyList (ncurcol).FORMAT := sJavaDateFormat;
        ELSE
          olPropertyList (ncurcol).TYPE := 'STRING';
        END IF;
        isr$trace.debug ('olPropertyList('||ncurcol||').xml', 'xml in logclob', sCurrentName, olPropertyList );
      END LOOP;
    END LOOP;

    dbms_sql.close_cursor (ccursor);

    isr$trace.stat('end', 'end', sCurrentName);
    RETURN oerrorobj;
  EXCEPTION
    WHEN OTHERS THEN
      oErrorObj.handleError ( Stb$typedef.cnSeverityCritical, utd$msglib.getmsg ('exUnexpectedError', stb$security.getCurrentlanguage,  csP1 => sqlerrm(sqlcode)),
        sCurrentName, dbms_utility.format_error_stack ||chr(10)|| dbms_utility.format_error_backtrace );
      RETURN oerrorobj;
  END getcolpropertylist;

  --*********************************************************************************************************************
  function getColListMultiRow( sSelect    in     varchar2,
                               olNodeList in out stb$treenodelist)
    RETURN stb$oerror$record
                            --
                            -- take care to create the first node of the treenodelist as a "copy-template" for the
                            -- successors
                            --
  IS
    sCurrentName                  constant varchar2(100) := $$PLSQL_UNIT||'.getColListMultiRow';
    oerrorobj                     stb$oerror$record := STB$OERROR$RECORD();
    --olPropertyList                stb$property$list;
    tloValueList                  ISR$VALUE$LIST;
    olNodeListBackup              STB$TREENODELIST;
    ccursor                       INTEGER;
    rdescription                  dbms_sql.desc_tab;
    nPropsCnt                     NUMBER;
    ncolcount                     NUMBER;
    ndummy                        NUMBER;
    sbuffer                       VARCHAR2 (4000);
    sbufferdate                   DATE;
    sDateFormat                   VARCHAR2(100) := stb$typedef.csInternalDateFormat;
    nRowCount                     NUMBER;
    sSqlProperties                CLOB;
    sType                         VARCHAR2(15);
    sFormat                       VARCHAR2(30);
    sIcon                         VARCHAR2(100);
    sNodeId                       VARCHAR2(100);
    sNodeType                     VARCHAR2(100);
    sNodeName                     VARCHAR2(100);
    sPackageName                  VARCHAR2(100);
    sDefaultAction                VARCHAR2(100);
    sSqlTreeNodes                 CLOB;
    nCurUser                      NUMBER;

    function BuildPropRecPiece(csColName in varchar2, csType in varchar2, csFormat in varchar2) return varchar2 is
      -- Builds the STB$PROPERTY$RECORD piece for a MDM table attribute
      sExpr varchar2(1000);
    begin
      sExpr := 'STB$PROPERTY$RECORD(  '''||upper(csColName)||'''
                                    , utd$msglib.getmsg ('''||upper(csColName)||''', '||stb$security.getCurrentLanguage||')
                                    , '||csColName||'
                                    , '||csColName||'
                                    , ''F'', ''F'', ''T'', ''F''
                                    ,'''||csType||'''
                                    ,'''||csFormat||''')';
      return sExpr;
    end BuildPropRecPiece;

  BEGIN
    isr$trace.stat('begin','begin',sCurrentName);
    isr$trace.debug('value','sSelect', sCurrentName,sSelect);
    isr$trace.debug('value','olNodeList',sCurrentName,olNodeList);
    ccursor := dbms_sql.open_cursor;
    dbms_sql.parse (ccursor, sselect, dbms_sql.native);
    -- Execute
    ndummy := dbms_sql.execute (ccursor);
    -- get the fileds
    dbms_sql.describe_columns (ccursor, ncolcount, rdescription);

    FOR ncurcol IN 1 .. ncolcount LOOP
      isr$trace.debug('col_type for '|| rdescription (ncurcol).col_name,rdescription (ncurcol).col_type,sCurrentName);
      IF rdescription (ncurcol).col_type = 12 THEN  --DATE
        dbms_sql.define_column (ccursor, ncurcol, sbufferdate);
      ELSE
        dbms_sql.define_column (ccursor, ncurcol, sbuffer, rdescription (ncurcol).col_max_len);
      END IF;
    END LOOP;

    tloValueList := olNodeList(1).tloValueList;

    nRowCount := 1;

    WHILE dbms_sql.fetch_rows (ccursor) > 0 LOOP


      if nRowCount = 1 then
        -- attach the properties for this treenode
        FOR ncurcol IN 1 .. ncolcount LOOP

          -- get the column value
          IF rdescription (ncurcol).col_type = 12 THEN
            dbms_sql.column_value (ccursor, ncurcol, sbufferdate);
            sbuffer := to_char (sbufferdate, sDateFormat);
          -- STRING
          ELSE
            dbms_sql.column_value (ccursor, ncurcol, sbuffer);
          END IF;

        IF UPPER(rdescription (ncurcol).col_name) = 'ICON' THEN
            sIcon := 'T';
          ELSIF UPPER(rdescription (ncurcol).col_name) = 'NODEID' THEN
            sNodeId := 'T';
          ELSIF UPPER(rdescription (ncurcol).col_name) = 'NODETYPE' THEN -- use only in getNodeChilds
            sNodeType := 'T';
          ELSIF UPPER(rdescription (ncurcol).col_name) = 'NODENAME' THEN
            sNodeName := 'T';
          ELSIF UPPER(rdescription (ncurcol).col_name) = 'PACKAGENAME' THEN
            sPackageName := 'T';
          ELSIF UPPER(rdescription (ncurcol).col_name) = 'DEFAULTACTION' THEN
            sDefaultAction := 'T';
          else
            sType := 'STRING';
            IF rdescription (ncurcol).col_max_len = 1 AND sbuffer IN ('T', 'F') THEN
              sType := 'BOOLEAN';
            ELSIF rdescription (ncurcol).col_max_len > 2000 THEN
              sType := 'LONGSTRING';
            ELSIF rdescription (ncurcol).col_type = 12 THEN
              sType := 'DATE';
              sFormat := sJavaDateFormat;
            END IF;
            sSqlProperties := sSqlProperties || ', ' || BuildPropRecPiece(rdescription (ncurcol).col_name, sType, sFormat);
          END IF;
        END LOOP;
        sSqlProperties := substr(sSqlProperties, 2, length(sSqlProperties));
      end if;

      nRowCount := nRowCount + 1;

    END LOOP;

    sNodeName := case when sNodeName = 'T' then 'nodename' else ''''||olnodelist (1).sDisplay||'''' end;
    sPackageName := case when sPackageName = 'T' then 'packagename' else ''''||olnodelist (1).sPackageType||'''' end;
    sNodeType := case when sNodeType = 'T' then 'nodetype' else ''''||olnodelist (1).sNodeType||'''' end;
    sNodeId := case when sNodeid = 'T' then 'nodeid' else ''''||olnodelist (1).sNodeid||'''' end;
    sIcon := case when sIcon = 'T' then 'icon' else ''''||olnodelist (1).sIcon||'''' end;
    sDefaultAction := case when sDefaultAction = 'T' then 'defaultaction' else ''''||olnodelist (1).sDefaultAction||'''' end;
    nCurUser := STB$SECURITY.getCurrentUser;

    sSqlTreeNodes := 'select STB$TREENODE$RECORD( '||sNodeName||'
                                                , '||sPackageName||'
                                                , '||sNodeType||'
                                                , STB$OBJECT.calculateHash(:1 multiset union all ISR$VALUE$LIST(ISR$VALUE$RECORD('||sNodeType||','||sNodeId||','||sNodeType||','||sNodeId||','||sPackageName||'))) ||''#@#''||'||nCurUser||'
                                                , '||sIcon||'
                                                , '||sDefaultAction||'
                                                , null, STB$PROPERTY$LIST('||sSqlProperties||')
                                                , :1 multiset union all ISR$VALUE$LIST(ISR$VALUE$RECORD('||sNodeType||','||sNodeId||','||sNodeType||','||sNodeId||','||sPackageName||'))
                                                , '||olnodelist (1).ndisplaytype||')
                     from ('|| sSelect ||')';

    isr$trace.debug('sSqlTreeNodes',sSqlTreeNodes,sCurrentName,sSqlTreeNodes);

    dbms_sql.close_cursor (ccursor);

    olNodeListBackup := olNodeList;
    olNodeList := STB$TREENODELIST();
    execute immediate sSqlTreeNodes bulk collect into olNodeList using in tloValueList, in tloValueList;
    if not(olNodeList.exists(1)) then
      olNodeList := olNodeListBackup;
    end if;

    isr$trace.debug('sSqlTreeNodes executed',sSqlTreeNodes,sCurrentName);
    
    ISR$TREE$PACKAGE.saveTreeNodeList(olNodeList, 'insert into isr$tree$nodes 
                               (nodeid
                              , nodetype
                              , packagename
                              , parentnodeid
                              , parameterName
                              , parametervalue
                              , clearnodeid
                              , icon
                              , userno) 
                           (select STB$OBJECT.calculateHash(:1 multiset union all ISR$VALUE$LIST(ISR$VALUE$RECORD('||sNodeType||','||sNodeId||','||sNodeType||','||sNodeId||','||sPackageName||'))) ||''#@#''||'||nCurUser||'
                                 , '||sNodeType||'
                                 , '||sPackageName||'
                                 , '''||STB$OBJECT.getCurrentNode().sNodeId||'''
                                 , '||sNodeType||'
                                 , '||sNodeId||'
                                 , '||sNodeId||'
                                 , '||sIcon||'
                                 , '''||STB$SECURITY.getCurrentUser||'''
                              from ('|| sSelect ||'))');

    --isr$trace.debug('value','olNodeList',sCurrentName,olNodeList);
    isr$trace.stat('end','olNodeList.count(): '|| olNodeList.count(), sCurrentName);
    RETURN oerrorobj;
  EXCEPTION
    WHEN OTHERS THEN
      oErrorObj.handleError ( Stb$typedef.cnSeverityCritical, utd$msglib.getmsg ('ERROR', stb$security.getcurrentlanguage), sCurrentName,
                            DBMS_UTILITY.format_error_stack || CHR(10) || DBMS_UTILITY.format_error_backtrace, SQLCODE);
      RETURN oerrorobj;
  END getcollistmultirow;

  --*********************************************************************************************************************
  FUNCTION getreptypeparameter (nreptype IN NUMBER, sparametername IN VARCHAR2, sparametervalue OUT VARCHAR2)
    RETURN stb$oerror$record
  IS
    sCurrentName   constant varchar2(4000) := $$PLSQL_UNIT||'.getreptypeparameter(' || nreptype || ', ' || sparametername || ')';
    oerrorobj                     stb$oerror$record := STB$OERROR$RECORD();

    -- cursor to retrieve the value of a single parametervalue
    CURSOR creptypeparam (nreptype IN NUMBER, sparametername IN VARCHAR2) IS
      SELECT parametervalue
      FROM   stb$reptypeparameter
      WHERE  reporttypeid = nreptype AND parametername = sparametername;

    rreptypeparam                 creptypeparam%ROWTYPE;
  BEGIN
    isr$trace.stat('begin', 'begin', sCurrentName);
    OPEN creptypeparam (nreptype, sparametername);
    FETCH creptypeparam INTO  rreptypeparam;
    IF creptypeparam%FOUND THEN
      sparametervalue := rreptypeparam.parametervalue;
    ELSE
      isr$trace.debug ('Parameter not found',  nreptype||' '||sparametername, sCurrentName);
    END IF;
    CLOSE creptypeparam;
    isr$trace.stat('end', 'end', sCurrentName);
    RETURN oerrorobj;
  EXCEPTION
    WHEN OTHERS THEN
      oErrorObj.handleError ( Stb$typedef.cnSeverityCritical, utd$msglib.getmsg ('exUnexpectedError', stb$security.getCurrentlanguage,  csP1 => sqlerrm(sqlcode)),
        sCurrentName, dbms_utility.format_error_stack ||chr(10)|| dbms_utility.format_error_backtrace );
      RETURN oerrorobj;
  END getreptypeparameter;

  --*********************************************************************************************************************
  function getreptypeparameter( nreptype       in number,
                                sparametername in varchar2 )
  return varchar2
  result_cache             -- JS/vs  [OPPIS-140], [ISRC-731]
  is
    sparametervalue  stb$reptypeparameter.parametervalue%TYPE;
    oerrorobj        stb$oerror$record := STB$OERROR$RECORD();
  begin
    oerrorobj := getreptypeparameter( nreptype, sparametername, sparametervalue);
    return sparametervalue;
  end getreptypeparameter;

  --*********************************************************************************************************************
  FUNCTION getSystemParameter (sparametername IN VARCHAR2, sparametervalue OUT VARCHAR2)
    RETURN stb$oerror$record
  IS
    sCurrentName   constant varchar2(4000) := $$PLSQL_UNIT||'.getSystemParameter(' || sparametername || ')';
    oerrorobj                     stb$oerror$record := STB$OERROR$RECORD();

    -- cursor to retrieve the value of a single parametervalue
    CURSOR csystemparam (sparametername IN VARCHAR2) IS
      SELECT parametervalue
      FROM   stb$systemparameter
      WHERE  trim (parametername) = sparametername;

    rsystemparam                  csystemparam%ROWTYPE;
  BEGIN
    isr$trace.stat('begin', 'begin', sCurrentName);
    OPEN csystemparam (sparametername);
    FETCH csystemparam INTO  rsystemparam;
    IF csystemparam%FOUND THEN
      sparametervalue := rsystemparam.parametervalue;
    ELSE
      isr$trace.debug ('Parameter not found', sparametername, sCurrentName);
    END IF;
    CLOSE csystemparam;
    isr$trace.stat('end', 'end', sCurrentName);
    RETURN oerrorobj;
  EXCEPTION
    WHEN OTHERS THEN
      oErrorObj.handleError ( Stb$typedef.cnSeverityCritical, utd$msglib.getmsg ('exUnexpectedError', stb$security.getCurrentlanguage,  csP1 => sqlerrm(sqlcode)),
        sCurrentName, dbms_utility.format_error_stack ||chr(10)|| dbms_utility.format_error_backtrace );
  END getsystemparameter;

  --*********************************************************************************************************************************
  FUNCTION getSystemParameter (sParametername IN VARCHAR2)
    return varchar2
    result_cache
  is
    oerrorobj                  stb$oerror$record := STB$OERROR$RECORD();
    sParameterValue            stb$systemparameter.parametervalue%TYPE;

  BEGIN
    oerrorobj := GetSystemParameter(sParametername, sParameterValue);
    RETURN sParameterValue;
  END getsystemparameter;

  --*********************************************************************************************************************************
  function getContextMenu( sNodeType       in  varchar2,
                           nMenu           in  number,
                           olMenuEntryList out STB$MenuEntry$List )
    return stb$oerror$record
  is
    sCurrentName                  constant varchar2(100) := $$PLSQL_UNIT||'.getContextMenu('||sNodeType||','||nMenu||')';
    oerrorobj                     stb$oerror$record      := STB$OERROR$RECORD();
    ncounter                      NUMBER;
    nRepid                        stb$report.repid%TYPE;
    sLeftSide                     VARCHAR2(1) := 'T';

    CURSOR cGetRenderingServerStatus IS
      SELECT runstatus
        FROM isr$server
       WHERE servertypeid = 14
       ORDER BY 1 desc;

    sRSStatus ISR$SERVER.RUNSTATUS%TYPE;

    CURSOR cGetRepid IS
      SELECT DISTINCT r.repid
        FROM STB$REPORT r, STB$DOCTRAIL d
       WHERE d.nodetype = 'WORKSHEET'
         AND d.doctype = 'L'
         AND r.srnumber = STB$OBJECT.getCurrentSRNumber
         AND r.repid = STB$OBJECT.getCurrentRepid
         AND r.repid = d.externalreference;

    CURSOR cMenu(csNodeType IN STB$CONTEXTMENU.NODETYPE%TYPE,
                 cnMenu IN STB$CONTEXTMENU.PARENTNODE%TYPE,
                 csLeftSide IN VARCHAR2 /*left or right side, in the right explorer side not 0*/) IS
      SELECT menu.entryid
           , menu.token
           , utd$msglib.getmsg (NVL(menu.description, menu.token), stb$security.getcurrentlanguage) display
           , menu.ATTRIBUTE
           , menu.handledby
           , menu.confirm
           , menu.icon
        FROM stb$contextmenu menu
       WHERE ( (menu.nodetype = csNodeType AND NVL (menu.parentnode, 0) = NVL (cnMenu, 0))
              OR (menu.nodetype IS NULL AND token IN ('SEPARATOR') AND csLeftSide = 'T' AND NVL (cnMenu, 0) = 0))
         AND ( ( (STB$OBJECT.getCurrentReportTypeId IS NULL
              AND menu.ATTRIBUTE = 'TOKEN'
              AND menu.token IN (SELECT parametername
                                   FROM stb$systemparameter
                                  WHERE userparameter = 'T'
                                    AND visible = 'T'
                                    AND parametervalue = 'T'))
             OR (STB$OBJECT.getCurrentReportTypeId IS NOT NULL
             AND menu.ATTRIBUTE = 'TOKEN'
             AND menu.token IN (SELECT parametername
                                  FROM stb$reptypeparameter
                                 WHERE userparameter = 'T'
                                   AND visible = 'T'
                                   AND parametervalue = 'T'
                                   AND reporttypeid = STB$OBJECT.getCurrentReportTypeId
                                 UNION
                                SELECT parametername
                                   FROM stb$systemparameter
                                  WHERE userparameter = 'T'
                                    AND visible = 'T'
                                        AND parametervalue = 'T'))
                )
           OR menu.ATTRIBUTE != 'TOKEN'
           OR csNodeType = 'COMMONDOC'
           OR csNodeType like '%REPORT_FILE'
           OR menu.nodetype IS NULL)
         AND isr$licencefile.getMode IN (menu.securemodeallowed, 'F')
         AND ( (menu.token = 'CAN_CREATE_PDF' AND NVL (visible, 'T') = STB$UTIL.getSystemParameter ('PDF_CREATION_ALLOWED'))
           OR (menu.token != 'CAN_CREATE_PDF' AND NVL (visible, 'T') = 'T'))
         AND ( (menu.token = 'CAN_CHANGE_OWN_PASSWORD' AND NVL (visible, 'T') = (SELECT case when ldapusername IS NOT NULL THEN 'F' ELSE 'T' END FROM stb$user WHERE userno = STB$SECURITY.getCurrentUser))
           OR (menu.token != 'CAN_CHANGE_OWN_PASSWORD' AND NVL (visible, 'T') = 'T'))
      ORDER BY menu.ordernum;

  BEGIN
    isr$trace.stat('begin', 'sNodeType: '||sNodeType||'  nMenu: '||nMenu, sCurrentName);
    olMenuEntryList := STB$MenuEntry$List();

    IF sNodeType = 'REPORT' THEN
      OPEN cGetRepid;
      FETCH cGetRepid INTO nRepid;
      CLOSE cGetRepid;
    END IF;

    ncounter := 0;
    isr$trace.debug ('status', 'STB$OBJECT.getCurrentReportTypeId: '||STB$OBJECT.getCurrentReportTypeId, sCurrentName);
    isr$trace.debug ('value', 'isr$licencefile.getMode: '||isr$licencefile.getMode, sCurrentName);

    begin
      isr$trace.debug('value', 'STB$OBJECT.getCurrentNode().sNodeId: '||STB$OBJECT.getCurrentNode().sNodeId, sCurrentName);
      select leftside 
        into sLeftSide
        from isr$tree$sessions
       where sessionid = STB$SECURITY.getCurrentSession
         and selected = 'T'
         and nodetype = STB$OBJECT.getCurrentNodeType;         
    exception when others then
      isr$trace.debug('code position','exception',sCurrentName,SQLERRM );
      sLeftSide := case when STB$OBJECT.getCurrentNode().tloPropertyList.count() = 0 then 'T' else 'F' end;
    end;

    isr$trace.debug('value', 'sLeftSide: '||sLeftSide, sCurrentName);

    FOR rMenu IN cMenu (sNodeType, nMenu, sLeftSide) LOOP
      isr$trace.debug('token -> ' || rMenu.token, 'display -> ' || rMenu.display, sCurrentName);
      IF (    nRepid IS NULL
           OR (     rMenu.token NOT IN ('MODIFY', 'STATUS', 'CREATE')
                and STB$OBJECT.getCurrentRepStatus != STB$TYPEDEF.CNSTATUSFINAL )
           OR STB$OBJECT.getCurrentRepStatus = STB$TYPEDEF.CNSTATUSFINAL            ) THEN
        olMenuentryList.EXTEND ();
        ncounter := ncounter + 1;
        olMenuEntryList (ncounter) := STB$MENUENTRY$RECORD(rMenu.ATTRIBUTE, rMenu.display, rMenu.token, rMenu.handledby, rMenu.icon, rMenu.confirm);
        IF (     rMenu.token IN ('CAN_RUN_REPORT', 'CAN_RELEASE_FOR_INSPECTION', 'CAN_FINALIZE_REPORT', 'CAN_CREATE_PDF')
             AND NVL(rMenu.confirm, 'F') = 'F'
             AND STB$UTIL.getReptypeParameter(STB$OBJECT.getCurrentReporttypeId, 'REQUIRES_VERSIONING') = 'T'   ) THEN
          OPEN cGetRenderingServerStatus;
          FETCH cGetRenderingServerStatus INTO sRSStatus;
          IF ( (      cGetRenderingServerStatus%FOUND
                  AND    sRSStatus = 'F'
                      OR cGetRenderingServerStatus%NOTFOUND )
               AND STB$USERSECURITY.getUserParameter (STB$SECURITY.getCurrentUser, 'LOCAL') = 'F' ) THEN
            olMenuEntryList (ncounter).sconfirm := 'T';
          END IF;
          CLOSE cGetRenderingServerStatus;
        END IF;
        IF olMenuEntryList (ncounter).sconfirm = 'T' THEN
          olMenuEntryList (ncounter).sconfirmtext := utd$msglib.getmsg ('CONFIRM_TEXT_' || rmenu.token, stb$security.getcurrentlanguage);
        END IF;
        olMenuEntryList (ncounter).nid := rMenu.entryid;
        olMenuEntryList (ncounter).sIcon := case when rMenu.attribute != 'TOKEN' then 'T' else ISR$RIBBON.isParameterAllowed(olMenuEntryList (ncounter).sToken, null, olMenuEntryList (ncounter).sHandledby) end;
      END IF;
    END LOOP;

    isr$trace.debug('status','olMenuentryList OUT', sCurrentName, olMenuentryList);
    isr$trace.stat('end', 'olMenuEntryList.count: '||olMenuEntryList.count, sCurrentName);
    RETURN oerrorobj;
  EXCEPTION
    WHEN OTHERS THEN
      oErrorObj.handleError ( Stb$typedef.cnSeverityCritical, utd$msglib.getmsg ('exUnexpectedError', stb$security.getCurrentlanguage,  csP1 => sqlerrm(sqlcode)),
        sCurrentName, dbms_utility.format_error_stack ||chr(10)|| dbms_utility.format_error_backtrace );
      RETURN oerrorobj;
  END getcontextmenu;

  --***************************************************************************************************************************
  FUNCTION createFilterParameter (olParameter IN stb$property$list)
    RETURN stb$oerror$record IS
    oerrorobj            stb$oerror$record := STB$OERROR$RECORD();
    sCurrentName         constant varchar2(100) := $$PLSQL_UNIT||'.createFilterParameter';
    sParameter           VARCHAR2(500);
    sUserMsg             varchar2(4000);
    sImplMsg             varchar2(4000);
    sDevMsg              varchar2(4000);

    sDateValue           varchar2(4000);
    sJobParameterValue   varchar2(4000);
  BEGIN
    isr$trace.stat('begin','begin',sCurrentName);
    isr$trace.debug('parameter','olParameter',sCurrentName,olParameter);
    isr$trace.debug('olParameter', 's. logclob', sCurrentName, olParameter);

    IF olParameter.exists(1) THEN
      FOR i IN 1 .. olparameter.count () LOOP
        IF olParameter(i).sDisplayed = 'F' THEN
          sParameter := 'INVISIBLE_FILTER_'||olparameter (i).sparameter;
        ELSE
          sParameter := 'FILTER_'||olparameter (i).sparameter;
        END IF;
        IF olparameter(i).sParameter like '%RAW' THEN
          sDateValue := STB$UTIL.getProperty(olparameter, replace(olparameter(i).sParameter, 'RAW'));
          if trim(sDateValue) is null then
            sJobParameterValue := null;
          else
            if instr(sDateValue, ':') = 0 then
              sDateValue := sDateValue||' '||case when olparameter(i).sParameter like 'BEGIN%' then '00:00:00' else '23:59:59' end;
            end if;
            sJobParameterValue := TO_CHAR((to_date(sDateValue, isr$util.getInternaldate) - to_date('01.01.2000', isr$util.getInternaldate)) * 24 * 60 * 60);
          end if;
          stb$job.setjobparameter (sParameter, sJobParameterValue, stb$job.njobid);
        ELSE
          stb$job.setjobparameter (sParameter, case
                                   when olparameter (i).type = 'DATE' then to_char(to_date(olparameter (i).sValue, stb$typedef.csInternalDateFormat), STB$UTIL.getSystemParameter('DATEFORMAT') || ' HH24:MI')
                                   else olparameter (i).sValue
                                               end, stb$job.njobid);
        END IF;
       -- isr$trace.debug(sParameter, stb$job.getjobparameter(sParameter, stb$job.njobid), sCurrentName);
      END LOOP;
    END IF;

    isr$trace.stat ('end', 'end', sCurrentName);
    RETURN oerrorobj;
  exception
    when others then
      sUserMsg := utd$msglib.getmsg ('exUnexpectedError', stb$security.getcurrentlanguage,  csP1 => sqlerrm(sqlcode));
      sImplMsg := sCurrentName || rtrim(': '||sImplMsg,': ');
      sDevMsg  := substr(dbms_utility.format_error_stack ||crlf|| dbms_utility.format_error_backtrace, 1,4000);
      oErrorObj.handleError ( Stb$typedef.cnSeverityCritical, sUserMsg, sImplMsg, sDevMsg );
      return oerrorobj;
  END createFilterParameter;

--************************************************************************************************************************************
FUNCTION getHandler (csToken IN VARCHAR2)
  RETURN VARCHAR2
IS
  sCurrentName   constant varchar2(4000) := $$PLSQL_UNIT||'.getHandler('||csToken||')';
  sPackage VARCHAR2(30);

  CURSOR cGetHandler IS
    SELECT handledby
      FROM stb$reptypeparameter
     WHERE reporttypeid = stb$object.getcurrentreporttypeid
       AND parametername = cstoken
       AND stb$object.getcurrentreporttypeid IS NOT NULL;

  CURSOR cSystemGetHandler IS
    SELECT handledby
      FROM stb$contextmenu
     WHERE token = cstoken;

BEGIN
  isr$trace.stat('begin', 'begin', sCurrentName);
  OPEN cGetHandler;
  FETCH cGetHandler INTO sPackage;
  IF cGetHandler%NOTFOUND OR TRIM(sPackage) IS NULL THEN
    OPEN cSystemGetHandler;
    FETCH cSystemGetHandler INTO sPackage;
    CLOSE cSystemGetHandler;
  END IF;
  CLOSE cGetHandler;

  isr$trace.stat('end', 'end', sCurrentName);
  RETURN sPackage;
END gethandler;


--************************************************************************************************************************************
function getCurrentLal
  return varchar2
is
  sCurrentName   constant varchar2(4000) := $$PLSQL_UNIT||'.getCurrentLal';
  sCurrentLal  varchar2(100);
begin
  isr$trace.stat('begin', 'begin', sCurrentName);
  sCurrentLal := STB$UTIL.getReptypeParameter(STB$OBJECT.getCurrentReportTypeId, 'LALPACKAGE');
  isr$trace.stat('end', sCurrentLal, sCurrentName);
  return sCurrentLal;
end getCurrentLal;

--************************************************************************************************************************************
FUNCTION getCurrentCustomPackage
  RETURN VARCHAR2
IS
  sCurrentName   constant varchar2(4000) := $$PLSQL_UNIT||'.getCurrentCustomPackage';
  sCurrentLal  varchar2(100);
BEGIN
  isr$trace.stat('begin', 'begin', sCurrentName);
  sCurrentLal := STB$UTIL.getSystemParameter('MAIN_CUSTOM_PACKAGE');
  isr$trace.stat('end', sCurrentLal, sCurrentName);
  return sCurrentLal;
END getcurrentcustompackage;

--********************************************************************************************************************************
function getPropertyList( sToken      in   varchar2,
                          olParameter out stb$property$list)
  return STB$OERROR$RECORD
is
  oErrorObj       STB$OERROR$RECORD := STB$OERROR$RECORD();
  sCurrentName    constant varchar2(100) := $$PLSQL_UNIT||'.getPropertyList('||sToken||')';
  oSelectionList  ISR$TLRSELECTION$LIST := ISR$TLRSELECTION$LIST();
begin
  isr$trace.stat('begin', 'sToken: '||sToken, sCurrentName);

  SELECT STB$PROPERTY$RECORD (sParameter
                            , sPromptDisplay
                            , sValue
                            , sDisplay
                            , sEditable
                            , sRequired
                            , sDisplayed
                            , hasLov
                            , TYPE
                            , format
                            , sFocus)
    BULK COLLECT
    INTO olParameter
    FROM (SELECT PARAMETERNAME sParameter
               , utd$msglib.getmsg (DESCRIPTION
                                  , stb$security.getCurrentLanguage)
                    sPromptDisplay
               , PARAMETERVALUE sValue
               , PARAMETERVALUE sDisplay
               , EDITABLE sEditable
               , case
                   when sToken like '%UPLOAD%' and parametername = 'COMMENT' then STB$UTIL.getSystemParameter ('USE_UPLOAD_COMMENT')
                   when sToken like '%CHECKIN%' and parametername = 'COMMENT' then STB$UTIL.getSystemParameter ('USE_CHECKIN_COMMENT')
                   else REQUIRED
                 end sRequired
               , CASE
                   WHEN parametername like 'PARA_DISPLAYED_%' THEN STB$UTIL.getSystemParameter(REPLACE(parametername, 'PARA_DISPLAYED_'))
                   WHEN NVL(STB$UTIL.getSystemParameter('PARA_DISPLAYED_'||parametername), 'F') = 'T' THEN STB$SECURITY.checkGroupRight('PARA_DISPLAYED_'||parametername)
                   ELSE DISPLAYED
                 END sDisplayed
               , NVL(STB$UTIL.getSystemParameter(token||'_'||parametername||'_LOV'), HASLOV) hasLov
               , PARAMETERTYPE TYPE
               , case
                   when parametertype = 'DATE' then nvl(FORMAT, sJavaDateFormat)
                   else format
                 end format
               , FOCUS sFocus
            FROM ISR$DIALOG
           WHERE token = sToken
             AND ((nvl (DISPLAYED, 'T') = 'T'
              AND parametertype = 'TABSHEET')
              OR parametertype != 'TABSHEET')
          ORDER BY decode (tab, NULL, orderno, tab), orderno);

  IF olParameter.count() > 0 THEN
    isr$trace.debug('olParameter after selection',  'olParameter.count: '||olParameter.count(), sCurrentName,  olParameter);
    FOR i IN 1..olParameter.count() LOOP
      IF nvl(instr(olParameter(i).sValue,':exec:'),0) > 0 THEN
        BEGIN
          execute immediate REPLACE(olParameter(i).sValue, ':exec:') INTO olParameter(i).sValue;
          olParameter(i).sDisplay := olParameter(i).sValue;
          isr$trace.debug(olParameter(i).sParameter,  olParameter(i).sValue, sCurrentName,  olParameter);
        EXCEPTION WHEN OTHERS THEN
          olParameter(i).sValue := null;
          olParameter(i).sDisplay := null;
          isr$trace.error (olParameter(i).sParameter, SQLERRM||' '||REPLACE(olParameter(i).sValue, ':exec:'), sCurrentName);
        END;
      END IF;

      IF olParameter(i).hasLov = 'T'
      AND olParameter(i).TYPE != 'FILE' /* open lovs
      AND trim(olParameter(i).sValue) IS NOT NULL */ THEN
        BEGIN
          oErrorObj := STB$GAL.getLov(olParameter(i).sParameter, olParameter(i).sValue, oSelectionList);

          IF oSelectionList IS NOT NULL AND oSelectionList.first IS NOT NULL THEN
            FOR j IN 1..oSelectionList.count() LOOP
              IF oSelectionList(j).sSelected = 'T' THEN
                olParameter(i).sDisplay := oSelectionList(j).sDisplay;
              END IF;
            END LOOP;
            IF trim(olParameter(i).sValue) IS NULL
            AND oSelectionList.count() >= 1
            AND olParameter(i).sRequired = 'T'
            AND olParameter(i).sDisplayed != 'S' THEN
              olParameter(i).sValue := oSelectionList(1).sValue;
              olParameter(i).sDisplay := oSelectionList(1).sDisplay;
            END IF;
          -- no lov
          ELSE
            olParameter(i).sValue := null;
            olParameter(i).sDisplay := null;
          END IF;
        EXCEPTION WHEN OTHERS THEN
          isr$trace.error ('could not set value from lov',  'parameter '||olParameter(i).sParameter||' parameter '||olParameter(i).sValue, sCurrentName);
        END;
      END IF;

    END LOOP;
  END IF;
  isr$trace.debug('value',  'olParameter OUT',  sCurrentName,  olParameter);
  isr$trace.stat('end', 'end', sCurrentName);
  RETURN oErrorObj;
EXCEPTION
  WHEN OTHERS THEN
      oErrorObj.handleError ( Stb$typedef.cnSeverityCritical, utd$msglib.getmsg ('exUnexpectedError', stb$security.getCurrentlanguage,  csP1 => sqlerrm(sqlcode)),
        sCurrentName, dbms_utility.format_error_stack ||chr(10)|| dbms_utility.format_error_backtrace );
    RETURN oErrorObj;
END getPropertyList;

--********************************************************************************************************************************
function getProperty(olParameter in stb$property$list,
                      csParameter   in varchar2,
                      csOutputValue in varchar2 default 'SVALUE')
  return varchar2
is
  sCurrentName    constant varchar2(100) := $$PLSQL_UNIT||'.getProperty('||csParameter||')';
  sReturnValue    varchar2(4000);
  sOutputValue    varchar2(500) := upper(csOutputValue);
  sParameter      varchar2(500) := upper(csParameter);
begin
  isr$trace.stat('begin', 'csParameter: '||csParameter||',  csOutputValue: '||csOutputValue ,sCurrentName);
  isr$trace.debug('parameter','olParameter',sCurrentName,olParameter);

  if olParameter.exists(1) then
    for i in 1..olParameter.count() loop
      if upper(olParameter(i).sParameter) = sParameter then
        case
          when sOutputValue = 'SVALUE' then
            sReturnValue := olParameter(i).sValue;
          when sOutputValue = 'SPROMPTDISPLAY' then
            sReturnValue := olParameter(i).sPromptDisplay;
          when sOutputValue = 'SDISPLAY' then
            sReturnValue := olParameter(i).sDisplay;
          when sOutputValue = 'SEDITABLE' then
            sReturnValue := olParameter(i).sEditable;
          when sOutputValue = 'SREQUIRED' then
            sReturnValue := olParameter(i).sRequired;
          when sOutputValue = 'SDISPLAYED' then
            sReturnValue := olParameter(i).sDisplayed;
          when sOutputValue = 'HASLOV' then
            sReturnValue := olParameter(i).hasLov;
          when sOutputValue = 'TYPE' then
            sReturnValue := olParameter(i).type;
          when sOutputValue = 'FORMAT' then
            sReturnValue := olParameter(i).format;
          when sOutputValue = 'SFOCUS' then
            sReturnValue := olParameter(i).sFocus;
          else
            sReturnValue := null;
        end case
        exit;
      end if;
    end loop;
  end if;

  isr$trace.stat('end', 'sReturnValue: '||sReturnValue, sCurrentname);
  return sReturnValue;
END getProperty;


--********************************************************************************************************************************
procedure setProperty(olParameter in out stb$property$list,
                      csParameter   in varchar2,
                      csOutputValue in varchar2 default 'SVALUE',
                      csNewValue in varchar2)
is
  sCurrentName    constant varchar2(100) := $$PLSQL_UNIT||'.setProperty('||csParameter||')';
  sOutputValue    varchar2(500) := upper(csOutputValue);
  sParameter      varchar2(500) := upper(csParameter);
begin
  isr$trace.stat('begin', 'csParameter: '||csParameter||',  csOutputValue: '||csOutputValue ,sCurrentName);
  isr$trace.debug('parameter','olParameter',sCurrentName,olParameter);

  if olParameter.exists(1) then
    for i in 1..olParameter.count() loop
      if upper(olParameter(i).sParameter) = sParameter then
        case
          when sOutputValue = 'SVALUE' then
            olParameter(i).sValue := csNewValue;
          when sOutputValue = 'SPROMPTDISPLAY' then
            olParameter(i).sPromptDisplay := csNewValue;
          when sOutputValue = 'SDISPLAY' then
            olParameter(i).sDisplay := csNewValue;
          when sOutputValue = 'SEDITABLE' then
            olParameter(i).sEditable := csNewValue;
          when sOutputValue = 'SREQUIRED' then
            olParameter(i).sRequired := csNewValue;
          when sOutputValue = 'SDISPLAYED' then
            olParameter(i).sDisplayed := csNewValue;
          when sOutputValue = 'HASLOV' then
            olParameter(i).hasLov := csNewValue;
          when sOutputValue = 'TYPE' then
            olParameter(i).type := csNewValue;
          when sOutputValue = 'FORMAT' then
            olParameter(i).format := csNewValue;
          when sOutputValue = 'SFOCUS' then
            olParameter(i).sFocus := csNewValue;
        end case
        exit;
      end if;
    end loop;
  end if;

  isr$trace.debug('parameter','olParameter',sCurrentName,olParameter);
  isr$trace.stat('end', 'end', sCurrentname);
END setProperty;
--********************************************************************************************************************************
FUNCTION clobToBlob (clob_in IN CLOB) RETURN BLOB
IS
  sCurrentName   constant varchar2(4000) := $$PLSQL_UNIT||'.clobToBlob';
  v_blob    BLOB;
  v_blob_offset NUMBER := 1;
  v_clob_offset NUMBER := 1;
  v_lang_context NUMBER := dbms_lob.default_lang_ctx;
  v_warning NUMBER;
BEGIN
  isr$trace.stat('begin', 'begin', sCurrentName);
  dbms_lob.createtemporary(v_blob, TRUE);
  dbms_lob.open(v_blob, dbms_lob.lob_readwrite);
  --use nls_charset_id('WE8ISO8859P1') because of Umlauten
  dbms_lob.converttoblob(v_blob, clob_in, dbms_lob.lobmaxsize, v_blob_offset, v_clob_offset, nls_charset_id(STB$UTIL.getSystemParameter('NLS_CHARACTERSET')), v_lang_context, v_warning );
  isr$trace.stat('end', 'end', sCurrentName);
  RETURN v_blob;
END clobToBlob;

--********************************************************************************************************************************
FUNCTION blobToClob (blob_in IN BLOB) RETURN CLOB
IS
    sCurrentName   constant varchar2(4000) := $$PLSQL_UNIT||'.blobToClob';
  v_clob    CLOB;
  v_blob_offset NUMBER := 1;
  v_clob_offset NUMBER := 1;
  v_lang_context NUMBER := dbms_lob.default_lang_ctx;
  v_warning NUMBER;
BEGIN
  isr$trace.stat('begin', 'begin', sCurrentName);
  dbms_lob.createtemporary(v_clob, TRUE);
  dbms_lob.open(v_clob, dbms_lob.lob_readwrite);
  --use nls_charset_id('WE8ISO8859P1') because of Umlauten
  dbms_lob.converttoclob(v_clob, blob_in, dbms_lob.lobmaxsize, v_clob_offset, v_blob_offset, nls_charset_id(STB$UTIL.getSystemParameter('NLS_CHARACTERSET')), v_lang_context, v_warning );

  isr$trace.stat('end', 'end', sCurrentName);
  RETURN v_clob;

END blobToClob;

--********************************************************************************************************************************
FUNCTION getLeftDelim(csText IN VARCHAR2, csDelim IN VARCHAR2 DEFAULT '_') RETURN varchar2 IS
  sLeft varchar2(4000);
  nPos integer;
BEGIN
  nPos := instr( csText, csDelim);
  IF nPos > 0 THEN
    sLeft := substr(csText,1,nPos-1);
  ELSE
    sLeft := csText;
  END IF;
  RETURN sLeft;
END getLeftDelim;

--********************************************************************************************************************************
FUNCTION getRightDelim(csText IN VARCHAR2, csDelim IN VARCHAR2 DEFAULT '_') RETURN varchar2 IS
  sRight varchar2(4000);
  nPos integer;
BEGIN
  -- [ISRC-672] no logging in this function, because it is called from ISR$TRACE !
  nPos := instr( csText, csDelim);
  IF nPos > 0 THEN
    sRight := substr(csText,-(length(csText)-nPos-length(csDelim)+1),length(csText)-nPos-length(csDelim)+1);
  ELSE
    sRight := NULL;
  END IF;
  RETURN sRight;
END getRightDelim;

  --*********************************************************************************************************************************
FUNCTION GetGroupEntity(csEntity IN isr$report$wizard.entity%TYPE, cnReportTypeId IN isr$report$wizard.ReportTypeId%TYPE)
  RETURN varchar2
IS
  sCurrentName   constant varchar2(4000) := $$PLSQL_UNIT||'.GetGroupEntity('||csEntity||')';
  CURSOR cGetGroupEntity IS
      -- fetch once! First fetch return master entity
      SELECT r.ParentEntityName GroupEntity
        FROM iSR$Meta$Relation r,
             iSR$Meta$Relation$Report rr,
             iSR$Meta$Crit pc,
             iSR$Report$Wizard w, -- wz of child cc
             iSR$Meta$Crit cc,
             iSR$Meta$Crit$Grouping G
        WHERE cc.critentity = csEntity
          AND r.ChildEntityName IN (cc.Entity, cc.critentity)
          AND r.RelationName = rr.RelationName
          AND pc.CritEntity = r.ParentEntityName
          AND G.RelationName = r.RelationName
          AND G.critentity = cc.critentity
          AND w.entity = cc.critentity
          AND w.reporttypeid = cnReportTypeId
          AND rr.reporttypeid = cnReportTypeId
          AND G.reporttypeid = cnReportTypeId
          AND r.RelationType != 'ALIAS'
        ORDER BY G.ordernumber;
  sGroupEntity iSR$Meta$Relation.ParentEntityName%TYPE;
BEGIN
  isr$trace.stat('begin', 'begin', sCurrentName);
  OPEN cGetGroupEntity;
  FETCH cGetGroupEntity INTO sGroupEntity;
  CLOSE cGetGroupEntity;
  isr$trace.stat('end', 'end', sCurrentName);
RETURN sGroupEntity;

END GetGroupEntity;

--==**********************************************************************************************************************************************==--
function  execute_clob(clCode CLOB)
  return VARCHAR2
IS
  sCurrentName   constant varchar2(4000) := $$PLSQL_UNIT||'.execute_clob';
  ds_cur    PLS_INTEGER := dbms_sql.open_cursor;
  sql_table dbms_sql.varchar2a; --use VARCHAR2A because of Umlauten

  c_buf_len CONSTANT BINARY_INTEGER := 256;
  v_accum   INTEGER := 0;
  v_beg     INTEGER := 1;
  v_end     INTEGER := 256;
  v_loblen  PLS_INTEGER;
  v_RetVal  PLS_INTEGER;

  ---------------------------
  -- local function to the execute_plsql_block procedure
  FUNCTION next_row(
   clob_in IN CLOB,
   len_in IN INTEGER,
   off_in IN INTEGER) RETURN VARCHAR2 IS
  BEGIN
    RETURN dbms_lob.substr(clob_in, len_in, off_in);
  END next_row;
  ---------------------------

BEGIN
  isr$trace.stat('begin', 'begin', sCurrentName);
  v_loblen := dbms_lob.getlength(clCode);

  LOOP
    -- Set the length to the remaining size
    -- if there are < c_buf_len characters remaining.
    IF v_accum + c_buf_len > v_loblen THEN
      v_end := v_loblen - v_accum;
    END IF;

    sql_table(nvl(sql_table.last, 0) + 1) := next_row(clCode, v_end, v_beg);

    v_beg := v_beg + c_BUF_LEN;
    v_accum := v_accum + v_end;

    IF v_accum >= v_loblen THEN
      EXIT;
    END IF;
  END LOOP;

  -- Parse the pl/sql ...
  dbms_sql.parse(ds_cur, sql_table, sql_table.first, sql_table.last, false, dbms_sql.native);

  -- ... execute ...
  v_RetVal := dbms_sql.execute(ds_cur);

  -- ... and close it
  dbms_sql.close_cursor(ds_cur);

  isr$trace.stat('end', 'end', sCurrentName);
  return 'T';

EXCEPTION WHEN others then
  dbms_sql.close_cursor(ds_cur);
  RETURN SQLERRM;
END execute_clob;

--******************************************************************************
FUNCTION GetReportParameterValue (csName IN VARCHAR2, cnRepid IN NUMBER)
  RETURN VARCHAR2
IS
  sCurrentName   constant varchar2(4000) := $$PLSQL_UNIT||'.GetReportParameterValue(' || csName || ')';
  sReturn VARCHAR2(4000);
  -- try to etrieve parameter from report
  CURSOR cGetParameter IS
    SELECT parametervalue
      FROM stb$reportparameter
     WHERE repid = cnRepid
       AND PARAMETERNAME = csName;
  -- retrieve parameter from report type
  cursor cGetRepTypeParameter is
    select STB$UTIL.getReptypeParameter(r.reporttypeid, csName)
    from stb$report r
    where r.repid = cnRepid;
BEGIN
  isr$trace.stat('begin', 'begin', sCurrentName);

  OPEN   cGetParameter;
  FETCH  cGetParameter INTO sReturn;
  if cGetParameter%notfound then
    open cGetRepTypeParameter;
    fetch cGetRepTypeParameter into sReturn;
    close cGetRepTypeParameter;
  end if;
  CLOSE  cGetParameter;
  isr$trace.stat('end', sReturn, sCurrentName);
  RETURN sReturn;
END GetReportParameterValue;

procedure setReportParameterValue(csParameterName IN VARCHAR2, csParameterValue IN VARCHAR2, cnRepid IN NUMBER)
is
  pragma autonomous_transaction;
  sCurrentName     constant varchar2(4000) := $$PLSQL_UNIT||'.setReportParameterValue('||cnRepid||')';
  oErrorObj        STB$OERROR$RECORD := STB$OERROR$RECORD();
begin
  isr$trace.stat('begin', 'cnRepid= ' || cnRepid || ',  csParameterName=' || csParameterName, sCurrentName);
  if cnRepid is not null then
    merge into stb$reportparameter a
    using (select cnRepid repid, csParameterName parametername, csParameterValue parametervalue from dual) b
          on (a.repid = b.repid and a.parametername = b.parametername )
          when not matched then
             insert (repid, parametername, parametervalue)
             values (b.repid, b.parametername, b.parametervalue);
     commit;
  else
    isr$trace.warn('repid is null', 'parameter could not be saved', sCurrentName);
  end if;

  isr$trace.stat('end', 'end', sCurrentName);
exception
  when others then
  isr$trace.warn('error', utd$msglib.getmsg ('exUnexpectedError', stb$security.getcurrentlanguage, sqlerrm(sqlcode)), sCurrentName);
end setReportParameterValue;

-- ***************************************************************************************************
FUNCTION getReportTypeForTitle (csTitle IN VARCHAR2) RETURN NUMBER IS
  sCurrentName   constant varchar2(200) := $$PLSQL_UNIT||'.getReportTypeForTitle(' || csTitle || ')';
  cursor cGetReporttype is
  select reporttypeid
      from stb$report
     where title = csTitle;
  nReporttype    number;
begin
  isr$trace.stat('begin', 'begin', sCurrentName);

  OPEN   cGetReporttype;
  FETCH  cGetReporttype INTO nReporttype;
  CLOSE  cGetReporttype;
  isr$trace.stat('end', 'nReporttype=' || nReporttype, sCurrentName);
  return nReporttype;
end getReportTypeForTitle;

--***************************************************************************************************************
function getPassword(cnRepId in number) RETURN VARCHAR2
is
  sCurrentName   constant varchar2(4000) := $$PLSQL_UNIT||'.getPassword('||cnRepid||')';

  CURSOR cGetPassword is
    SELECT PASSWORD
      FROM STB$REPORT
     WHERE repid = cnRepid;

  sPassword VARCHAR2(100);

BEGIN
  isr$trace.stat('begin', 'begin', sCurrentName);
  OPEN cGetPassword;
  FETCH cGetPassword into sPassword;
  CLOSE cGetPassword;
  isr$trace.stat('end', 'end', sCurrentName);
  RETURN sPassword;

END getPassword;

-- ***************************************************************************************************
function getMaskTitle(csToken IN VARCHAR2) RETURN VARCHAR2
is
  sCurrentName   constant varchar2(4000) := $$PLSQL_UNIT||'.getMaskTitle('||csToken||')';
  sReturn        varchar2(32000);
BEGIN
  isr$trace.stat('begin', 'begin', sCurrentName);
  sReturn := TRIM(Utd$msglib.GetMsg (csToken||'$MASK', Stb$security.GetCurrentLanguage)||' '||STB$UTIL.getContext);
  isr$trace.stat('end', 'end', sCurrentName);
  RETURN sReturn;

END getMaskTitle;

-- ***************************************************************************************************
function getContext RETURN VARCHAR2
is
  sCurrentName   constant varchar2(4000) := $$PLSQL_UNIT||'.getContext';
  sContext VARCHAR2(4000);

  CURSOR cGetReporttypeName IS
    select Utd$msglib.GetMsg (reporttypename, Stb$security.GetCurrentLanguage)
    from stb$reporttype
    where reporttypeid = STB$OBJECT.getCurrentReporttypeid;

BEGIN
  isr$trace.stat('begin', 'begin', sCurrentName);
  OPEN cGetReporttypeName;
  FETCH cGetReporttypeName INTO sContext;
  CLOSE cGetReporttypeName;

  sContext := TRIM(sContext || ' ' || STB$OBJECT.getCurrentSRnumber) || ' ' || STB$OBJECT.getCurrentTitle;
  isr$trace.stat('end', 'sContext: '||sContext, sCurrentName);
  RETURN TRIM(sContext);

END getContext;

-- ***************************************************************************************************
FUNCTION isNumeric ( cnCheckString  VARCHAR2 DEFAULT NULL )
  RETURN CHAR
IS
  nTestNumber   NUMBER;
  bReturnResult CHAR;
BEGIN
  -- [ISRC-672] no logging in this function, because it is called from ISR$TRACE !
  SELECT TO_NUMBER( cnCheckString )
  INTO   nTestNumber
  FROM   dual;

  IF nTestNumber IS NULL THEN
    bReturnResult := NULL;
  ELSE
    bReturnResult := 'T';
  END IF;

  RETURN bReturnResult;
EXCEPTION
  -- ORA-01722: "invalid number"
  WHEN INVALID_NUMBER THEN
    RETURN 'F';
  WHEN OTHERS THEN
    RETURN NULL;
END isNumeric;

--*********************************************************************************************
FUNCTION strtokenize (stotokenize IN VARCHAR2, sdelimiter IN VARCHAR2, ltokensarray OUT stb$typedef.tlstrings)
  RETURN stb$oerror$record
IS
  sCurrentName         constant varchar2(4000) := $$PLSQL_UNIT||'.strTokenize';
  oerrorobj                     stb$oerror$record := STB$OERROR$RECORD();
  nanzahl                       INTEGER := 1;
  nposition                     INTEGER := 1;
  nalteposition                 INTEGER := 0;
BEGIN
  isr$trace.stat('begin','sDelimiter: ' || sdelimiter, sCurrentName);
  nposition := instr (stotokenize, sdelimiter, 1, nanzahl);

  WHILE nposition > 0 LOOP
    ltokensarray (nanzahl) := substr (stotokenize, nalteposition + 1, nposition - nalteposition - 1);
    nalteposition := nposition;
    nanzahl := nanzahl + 1;
    nposition := instr (stotokenize, sdelimiter, 1, nanzahl);
  END LOOP;

  ltokensarray (nanzahl) := substr (stotokenize, nalteposition + 1);
  isr$trace.stat('end','number of tokens: ' || nAnzahl, sCurrentName);
  RETURN (oerrorobj);
EXCEPTION
  WHEN OTHERS THEN
      oErrorObj.handleError ( Stb$typedef.cnSeverityCritical, utd$msglib.getmsg ('exUnexpectedError', stb$security.getCurrentlanguage,  csP1 => sqlerrm(sqlcode)),
        sCurrentName, dbms_utility.format_error_stack ||chr(10)|| dbms_utility.format_error_backtrace );
    RETURN (oerrorobj);
END strtokenize;

--*********************************************************************************************
FUNCTION checkMethodExists (sMethod IN VARCHAR2, sPackage  IN VARCHAR2 default null)
  return varchar2
  result_cache
IS
  sCurrentName   constant varchar2(100) := $$PLSQL_UNIT||'.checkMethodExists('||sMethod||')';
  nStartTimeNum  number                 := DBMS_UTILITY.GET_TIME;

  CURSOR cMethodExists(csMethod IN VARCHAR2, csPackage IN VARCHAR2 DEFAULT NULL) IS
    SELECT DISTINCT 'T'
      FROM user_arguments
     WHERE object_name  = csMethod
       AND package_name = csPackage;

   -- (NB/vs) [ISRC-730]  [OPPIS-140]
  sLocMethod   varchar2(128);
  sLocPackage  varchar2(128);

  sMethodExists  VARCHAR2(1) := 'F';
BEGIN
  isr$trace.stat('begin','sMethod: '||sMethod||', sPackage: '||sPackage ,sCurrentName);

  sLocMethod := upper(sMethod);
  -- for UCIS-198 and ISRC-1306
  --sLocPackage := upper(nvl(sPackage, sCurrentLal));
  sLocPackage := upper(nvl(sPackage, STB$UTIL.getCurrentLal));
  isr$trace.debug('sLocPackage', sLocPackage, sCurrentName);
  
  if sLocMethod is not null and sLocPackage is not null then  --ISRC-1423
    OPEN cMethodExists(sLocMethod, sLocPackage);
    FETCH cMethodExists INTO sMethodExists;
    CLOSE cMethodExists;
  end if;
  
  isr$trace.stat('end','sMethodExists: '||sMethodExists||',  elapsed time: '||isr$util.getElapsedTime(nStartTimeNum), sCurrentName);
  RETURN sMethodExists;
END checkMethodExists;

--*********************************************************************************************
FUNCTION checkObjectExists (sMethod IN VARCHAR2, sPackage IN VARCHAR2 )
  return varchar2
  result_cache
IS
  sCurrentName   constant varchar2(100) := $$PLSQL_UNIT||'.checkObjectExists('||sMethod||')';
  nStartTimeNum  number                 := DBMS_UTILITY.GET_TIME;

  cursor cMethodTypeExists(csMethod in varchar2, csPackage in varchar2 default null) is
  select distinct 'T'
  from user_type_methods
  where method_name = csMethod
    and type_name = csPackage;

   -- (NB/vs) [ISRC-730]  [OPPIS-140]
  sLocMethod   VARCHAR2(128);
  sLocPackage  VARCHAR2(128);

  sMethodExists  VARCHAR2(1) := 'F';
BEGIN
  isr$trace.stat('begin','sMethod: '||sMethod||', sPackage: '||sPackage ,sCurrentName);

  sLocMethod := upper(sMethod);
  -- for UCIS-198 and ISRC-1306
  --sLocPackage := upper(NVL (sPackage, sCurrentLal));
  sLocPackage := upper(nvl(sPackage, STB$UTIL.getCurrentLal));
  
  if sMethodExists = 'F' and sLocMethod is not null and sLocPackage is not null then  --ISRC-1423
    open cMethodTypeExists( sLocMethod , sLocPackage);
    fetch cMethodTypeExists into sMethodExists;
    close cMethodTypeExists;
  end if;

  isr$trace.stat('end','sMethodExists: '||sMethodExists||',  elapsed time: '||isr$util.getElapsedTime(nStartTimeNum), sCurrentName);
  RETURN sMethodExists;


END checkObjectExists;
FUNCTION getClientcontext(v_attribute varchar2)
  RETURN VARCHAR2
IS
  sCurrentName     constant varchar2(4000) := $$PLSQL_UNIT||'.setClientcontext';
  sReturn VARCHAR2(40);
BEGIN
  isr$trace.stat('begin', 'begin', sCurrentName);
  sReturn :=SYS_CONTEXT ('CLIENTCONTEXT', v_attribute);
  isr$trace.stat('end', 'end', sCurrentName);
  RETURN sReturn;
END getClientcontext;
FUNCTION setClientcontext(v_attribute varchar2,v_value varchar2 default null)
  return boolean
is
  sCurrentName     constant varchar2(4000) := $$PLSQL_UNIT||'.setClientcontext';
  v_context varchar2(10);
begin
  isr$trace.stat('begin', 'Attribute= ' || v_attribute || ',  value=' || v_value, sCurrentName);
  v_context :=SYS_CONTEXT ('CLIENTCONTEXT', v_attribute);
  if v_context is not null  then
  null;
  end if;
  DBMS_SESSION.SET_CONTEXT ( 'CLIENTCONTEXT', v_attribute, v_value );
  isr$trace.stat('end', 'end', sCurrentName);
  return true;
exception
  when others then
  isr$trace.warn('error', utd$msglib.getmsg ('exUnexpectedError', stb$security.getcurrentlanguage, sqlerrm(sqlcode)), sCurrentName);
end setClientcontext;

END stb$util;