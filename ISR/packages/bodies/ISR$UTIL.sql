CREATE OR REPLACE PACKAGE BODY ISR$UTIL is

  nCurrentLanguage       number       := stb$security.getcurrentlanguage;
  bSessionIsInitialized  boolean      := false;
  sSessionUser           varchar2(50);

--******************************************************************************
function chkTableExists( csTableName  in varchar2,
                         csColumnName in varchar2 default null )
  return varchar2
is
  sCurrentName    constant varchar2(100)   := $$PLSQL_UNIT||'.chkTableExists('||csTableName||','||nvl(csColumnName,'(null)')||')';
  nCount          number;
  sTableName      varchar2(30) := trim(upper(csTableName));
  sColumnName     varchar2(30) := trim(upper(csColumnName));
  sReturnErrorMsg varchar2(100);
  sSql            varchar2(4000);
begin
  isr$trace.stat('begin', 'Function '||sCurrentName||' started.',sCurrentName);
  sSql := 'select count(1) from user_tables where table_name = '''||sTableName||'''';
  execute immediate sSql
  into nCount;
  if ( nCount = 1 ) and ( sColumnName is not null ) then
    sSql := 'select count(1) from user_tab_columns where table_name = '''||sTableName||''' and column_name = '''||sColumnName||'''';
    execute immediate sSql
    into nCount;
    if nCount = 0 then
      sReturnErrorMsg := 'exColumnNotExists';
    end if;
  elsif nCount <> 1 then
    sReturnErrorMsg := 'exTableNotExists';
  end if;
  isr$trace.stat('end', 'Function '||sCurrentName||' finished.',sCurrentName);
  return sReturnErrorMsg; -- returns null, if there is no error
end chkTableExists;

--******************************************************************************
procedure truncateTable( csTableName in varchar2 )
is
  pragma autonomous_transaction;
  sCurrentName constant varchar2(100)   := $$PLSQL_UNIT||'.truncateTable('||csTableName||')';
  oErrorObj    stb$oerror$record        := STB$OERROR$RECORD();
  sMsg         varchar2(4000);
begin
  isr$trace.stat('begin', 'begin', sCurrentName);
  execute immediate 'truncate table ' || csTableName;
  isr$trace.stat('end', 'end', sCurrentName);
exception
  when others then
    sMsg :=  utd$msglib.getmsg ('exUnexpectedError', nCurrentLanguage, csP1 => sqlerrm(sqlcode));
    oErrorObj.handleError ( Stb$typedef.cnSeverityCritical, sMsg, sCurrentName,
                            substr(dbms_utility.format_error_stack || CHR(10) || dbms_utility.format_error_backtrace, 1,4000)  );
end truncateTable;


-- ***************************************************************************************************
function getElapsedTime ( cnStartTimeNum  number,
                          cnStopTimeNum   number  default dbms_utility.get_time )
   return varchar2
is
   -- Time difference in 1/100 sec
   nTimeDiffMsec number := cnStopTimeNum - cnStartTimeNum;
begin

   return  trim( trim(leading '0' from  TO_CHAR(TRUNC(nTimeDiffMsec/8640000)) )  || ' ' ||
                 lpad(TO_CHAR(TRUNC(mod(nTimeDiffMsec,86400000)/360000)),2,'0')  || ':' ||
                 lpad(TO_CHAR(TRUNC(mod(nTimeDiffMsec,3600000)/6000)),2,'0')     || ':' ||
                 lpad(TO_CHAR(TRUNC(mod(nTimeDiffMsec,60000)/100)),2,'0')        || ' ' ||
                 lpad(TO_CHAR(mod(nTimeDiffMsec,100)),2,'0')   );
END getElapsedTime;

-- ***************************************************************************************************
function dsInterval$to$ms( cidtsInterval interval day to second  )
  return number
is
  sCurrentName constant varchar2(100)   := $$PLSQL_UNIT||'.dsInterval$to$ms';
  oErrorObj    stb$oerror$record        := STB$OERROR$RECORD();
  nReturnMilliseconds number;
begin
  isr$trace.debug('begin', 'begin', sCurrentName);
  -- nReturnMilliseconds := round(extract(day from idtsInterval*1000*60*60*24)/10);  -- problem: ORA-01873 overflow might occur

  -- calculate seconds from interval
  nReturnMilliseconds := (   extract( second from cidtsInterval)
                           + extract( minute from cidtsInterval) * 60        --       60
                           + extract( hour   from cidtsInterval) * 3600      --    60*60
                           + extract( day    from cidtsInterval) * 86400 );  -- 24*60*60
  nReturnMilliseconds := round(nReturnMilliseconds * 1000);
  isr$trace.debug('end', 'end', sCurrentName);
  return nReturnMilliseconds;
exception
  when others then
    oErrorObj.handleError( STB$TYPEDEF.cnSeverityCritical, utd$msglib.getmsg ('exUnexpectedError', nCurrentLanguage, sqlerrm(sqlcode)), sCurrentName,
                           substr(dbms_utility.format_error_stack || CHR(10) || dbms_utility.format_error_backtrace, 1,4000));
    raise;
end dsInterval$to$ms;

-- ***************************************************************************************************
function ms$to$dsinterval( cnMilliseconds number )
  return interval day to second
is
  sCurrentName constant varchar2(100)   := $$PLSQL_UNIT||'.ms$to$dsinterval';
  oErrorObj    stb$oerror$record        := STB$OERROR$RECORD();
  idtsReturnInterval interval day(9) to second(9);
begin
  isr$trace.stat('begin', 'begin', sCurrentName);
  idtsReturnInterval := numtodsinterval(cnMilliseconds/100, 'SECOND');
  isr$trace.stat('end', 'end', sCurrentName);
  return idtsReturnInterval;
exception
  when others then
    oErrorObj.handleError( STB$TYPEDEF.cnSeverityCritical, utd$msglib.getmsg ('exUnexpectedError', nCurrentLanguage, sqlerrm(sqlcode)), sCurrentName,
                           substr(dbms_utility.format_error_stack || CHR(10) || dbms_utility.format_error_backtrace, 1,4000));
    raise;
end ms$to$dsinterval;


function getDateDifferenceInSec(dFirstDate in date, dSecondDate in date default sysdate) return number is
  sCurrentName constant varchar2(100)   := $$PLSQL_UNIT||'.getDateDifferenceInSec';
  oErrorObj    stb$oerror$record        := STB$OERROR$RECORD();
  dDifferenceSec   number;
begin
  isr$trace.stat('begin', 'begin', sCurrentName);
  dDifferenceSec := (dSecondDate - dFirstDate) * 86400;
  isr$trace.stat('end', 'end', sCurrentName);
  return dDifferenceSec;
exception
  when others then
    oErrorObj.handleError( STB$TYPEDEF.cnSeverityCritical, utd$msglib.getmsg ('exUnexpectedError', nCurrentLanguage, sqlerrm(sqlcode)), sCurrentName,
                           dbms_utility.format_error_stack || CHR(10) || dbms_utility.format_error_backtrace);
    raise;
end getDateDifferenceInSec;
-- ***************************************************************************************************
function isr$get_java_property (csproperty in varchar2)
  return varchar2
is language java
name 'java.lang.System.getProperty(java.lang.String) return java.lang.String';

-- ***************************************************************************************************
function getQuotedStringList( csInputString  varchar2,
                              csOldSeparator varchar2 default ',',
                              csNewSeparator varchar2 default ',',
                              csTrimSpace    varchar2 default 'F' )
  return varchar2
is
  sCurrentName   constant varchar2(4000) := $$PLSQL_UNIT||'.getQuotedStringList('||' '||')';
  sReturnString varchar2(30000);
begin
  isr$trace.stat('begin', 'begin', sCurrentName);
  sReturnString := '''' || replace( csInputString, csOldSeparator, ''''||csNewSeparator||'''') || '''';

  if upper(csTrimSpace) = 'T' then
    sReturnString := translate( sReturnString, '* ', '*');
  end if;

  isr$trace.stat('end', 'end', sCurrentName);
  return sReturnString;
end getQuotedStringList;


-- ***************************************************************************************************
function getSplit( csInputString varchar2,
                   cnPosition    varchar2 default 1,
                   csSeparator   varchar2 default ',',
                   csTrim        varchar2 default 'F' )
  return varchar2
is
  sCurrentName   constant varchar2(4000) := $$PLSQL_UNIT||'.getSplit';
  sReturnString varchar2(30000);    -- string to be returned from this function
  sReverseInput varchar2(2000);     -- reverse input string, needed for support of negative positions
  nPosBegin     number;             -- first position of the string to be returned (un-trimmed, if applicable)
  nPosEnd       number;             -- last position of the string to be returned (un-trimmed, if applicable)
  nLength       number;             -- length of the string to be returned (un-trimmed, if applicable)
  nCouSeparator number;             -- the number of appearances of the separator within the input-string
begin
  isr$trace.stat('begin', 'begin', sCurrentName);
  -- count separators in input string
  nCouSeparator := regexp_count(csInputString,csSeparator,1);
  -- get reverse input string
  sReverseInput := ISR$UTIL.reverse(csInputString);

  case
    -- calculate nPosBegin and nPosEnd
    when (    cnPosition = 0
           or abs(cnPosition) > nCouSeparator + 1  ) then
      nPosBegin := 0;
      nPosEnd   := 0;
    when cnPosition = 1 then
      nPosBegin := 1;
      nPosEnd   := instr(csInputString||csSeparator,csSeparator,1,1);
    when cnPosition = -1 then
      nPosBegin := 1;
      nPosEnd   := instr(sReverseInput||csSeparator,csSeparator,1,1);
    when cnPosition < -1  then
      nPosBegin := instr(sReverseInput,csSeparator,1,abs(cnPosition)-1) + length(csSeparator);
      nPosEnd   := instr(sReverseInput||csSeparator,csSeparator,1,abs(cnPosition));
    else
      nPosBegin := instr(csInputString,csSeparator,1,cnPosition-1) + length(csSeparator);
      nPosEnd   := instr(csInputString||csSeparator,csSeparator,1,cnPosition);
  end case;

  -- calculate length of return-string, based on begin and end
  nLength       := nPosEnd - nPosBegin;

  -- get the return-string
  if cnPosition >= 0 then
    sReturnString := substr(csInputString,nPosBegin,nLength);
  else
    sReturnString := ISR$UTIL.reverse(substr(sReverseInput,nPosBegin,nLength));
  end if;

  -- optionally trim the return-string
  if upper(csTrim) = 'T' then
    sReturnString := trim(sReturnString);
  end if;

  isr$trace.stat('end', 'end', sCurrentName);
  return sReturnString;
end getSplit;

-- ***************************************************************************************************
function reverse( csInputString varchar2 )
  return varchar2
is
  sReturnString varchar2(32000);
  cursor curGetReverseString( csInput  varchar2 ) is
    select reverse( csInput ) reverse_string
    from   dual;
begin
  for recReverse in curGetReverseString(csInputString)
  loop
    sReturnString := recReverse.reverse_string;
  end loop;
  return sReturnString;
end reverse;

-- ***************************************************************************************************
function getFullQualifiedCallName( csInputString varchar2,
                                   csCallName    varchar2,       -- filename without path and without separator sign
                                   csSeparator   varchar2 default '/' )
  return varchar2
is
  sCurrentName   constant varchar2(4000) := $$PLSQL_UNIT||'.getFullQualifiedCallName('||' '||')';
  sInputString   varchar2(4000);
  sCallName       varchar2(4000);
  sSeparator    varchar2(100);
  sReturnString  varchar2(4000);
begin
  isr$trace.stat('begin', 'begin', sCurrentName);
  -- (vs): final improvement for ARDIS 02. May 2016
  -- code has no special trimming for leading '.' or leading csSeparator
  -- neither is there any code for case sensitivity
  sInputString := trim(csInputString);
  sCallName    := trim(csCallName);
  sSeparator   := trim(csSeparator);
  if isr$util.getSplit(sInputString,-1,sSeparator,'T') = sCallName then
    -- csCallname is final part of csInputString
    sReturnString := sInputString;
  else
    -- csCallname differs from final part of csInputString
    sReturnString := trim(trailing sSeparator from sInputString)||sSeparator||sCallName;
  end if;

  isr$trace.stat('end', 'end', sCurrentName);
  return sReturnString;
exception
  when others then
    return 'ERROR';
end getFullQualifiedCallName;

-- ***************************************************************************************************
function initializeSession    -- [ISRC-1108]
  return STB$OERROR$RECORD
is
  sCurrentName                 constant varchar2(4000) := $$PLSQL_UNIT||'.initializeSession';
  oErrorObj                    STB$OERROR$RECORD := STB$OERROR$RECORD();
  sUserMsg                     varchar2(4000);
  sImplMsg                     varchar2(4000);
  sDevMsg                      varchar2(4000);
  nTargetMemory                number;
  nReturn                      number;
  sCurrentUser                 varchar2(50);
  bReInitializeSessionLoglevel boolean := false;
begin
  -- do not call ISR$TRACE.begin here

  sCurrentUser := STB$SECURITY.getCurrentUser;
  -- Re-Initialize session, if Session-User has changed
  if sSessionUser is null
  or sSessionUser <> sCurrentUser then
    bReInitializeSessionLoglevel := true;
    sSessionUser := sCurrentUser;
  end if;

  -- initialize session once only
  if not bSessionIsInitialized then

    begin execute immediate 'alter session set nls_language    = '''||STB$UTIL.getSystemParameter('NLS_LANGUAGE')||''''; end;
    begin execute immediate 'alter session set nls_sort        = '''||STB$UTIL.getSystemParameter('NLS_SORT')||''''; end;
    begin execute immediate 'alter session set nls_territory   = '''||STB$UTIL.getSystemParameter('NLS_TERRITORY')||''''; end;
    begin execute immediate 'alter session set nls_date_format = '''||stb$typedef.csInternalDateFormat||''''; end;
    begin execute immediate 'alter session set time_zone       = '''||STB$UTIL.getSystemParameter('SERVER_TIMEZONE')||''''; end;

    -- [ISRC-1087], [ISRC-1108]
    nTargetMemory := NVL(STB$UTIL.getSystemParameter('JAVA_MAXMEMORY'),stb$java.getMaxMemorySize);
    isr$trace.debug_all('Java Memory','MaxMemorySize-Target: '||nTargetMemory, sCurrentName);
    nReturn := stb$java.setMaxMemorySize (nTargetMemory);

    ISR$TRACE.setSessionLoglevel;
    isr$trace.debug('nReturn',nReturn || ' MaxMemorySize: ' || stb$java.getMaxMemorySize, sCurrentName);

    bSessionIsInitialized := true;
    isr$trace.debug('status', 'session initialized now', sCurrentName);
  elsif bReInitializeSessionLoglevel then
    ISR$TRACE.setSessionLoglevel;
  else
    isr$trace.debug('status', 'NO ACTION --> session for user '||sSessionUser||' was initialized before', sCurrentName);
  end if;




  oErrorObj.sSeverityCode := STB$TYPEDEF.cnNoError;
  isr$trace.stat('end', 'Session initialization passed', sCurrentName);
  return oErrorObj;
exception
  when others then
    rollback;
    sUserMsg := utd$msglib.getmsg ('exUnexpectedError', stb$security.getCurrentLanguage,  csP1 => sqlerrm(sqlcode));
    sImplMsg := 'Error in '||sCurrentName||':'||chr(10)||sqlcode||chr(10)||sqlerrm;
    sDevMsg  := substr(dbms_utility.format_error_stack||chr(10)||chr(10)||dbms_utility.format_error_backtrace, 1,4000);
    oErrorObj.handleError ( Stb$typedef.cnSeverityCritical, sUserMsg, sImplMsg, sDevMsg );
end initializeSession;


 --*********************************************************************************************************************************
 FUNCTION getInternalDate  return varchar2
    result_cache
  is
  BEGIN
    RETURN stb$typedef.csInternalDateFormat;
  END getInternalDate;

 --*********************************************************************************************************************************
function isNumeric ( csTestString varchar2,
                     csDecimalSeparator varchar2 default '.' )
  return varchar2
is
begin
  if length((translate(trim(csTestString),'+-0123456789'||csDecimalSeparator, ' '))) is null then
    return 'T';
  else
    return 'F';
  end if;
end isNumeric;

 --*********************************************************************************************************************************
function isNumeric ( cnNumber number )
  return varchar2
is
begin
  return 'T';
end isNumeric;

--***********************************************************************************************************************
FUNCTION split (csList varchar2, csDelimiter varchar2 := ',') RETURN SYS.ODCIVARCHAR2LIST DETERMINISTIC
IS
    lResult       SYS.ODCIVARCHAR2LIST := SYS.ODCIVARCHAR2LIST();
    nStart        NUMBER(5) := 1;
    nEnd          NUMBER(5);
    cnLen CONSTANT NUMBER(5) := LENGTH( csList );
    cnLd  CONSTANT NUMBER(5) := LENGTH( csDelimiter );
    
BEGIN
  IF cnLen > 0 THEN
    nEnd := INSTR(csList, csDelimiter, nStart);
    WHILE nEnd > 0 LOOP
      lResult.EXTEND;
      lResult(lResult.COUNT) := SUBSTR(csList, nStart, nEnd - nStart);
      nStart := nEnd + cnLd;
      nEnd := INSTR(csList, csDelimiter, nStart);
    END LOOP;
    IF nStart <= cnLen +1 THEN
      lResult.EXTEND;
      lResult(lResult.COUNT) := SUBSTR(csList, nStart, cnLen - nStart + 1);
    END IF;
  END IF;
  RETURN lResult;
END split;

--******************************************************************************
function splitNumbers (csList varchar2, csDelimiter varchar2 := ',', csRange varchar2 := '-') return SYS.ODCINUMBERLIST pipelined
IS
    sList         varchar2(2000) := csList || csDelimiter; -- Add delimiter at the end to loop through all values, including the last one
    nStart        number(10) := 1;
    nEnd          number(10);
    cnLen         constant number(5) := length( sList );
    cnLd          constant number(5) := length( csDelimiter );
    lNums         SYS.ODCINUMBERLIST := SYS.ODCINUMBERLIST();
    
    function processValue (csValue varchar2) return SYS.ODCINUMBERLIST deterministic
    is
      sSplit        varchar2(200);
      nRangeStart   number(10);
      nRangeEnd     number(10);
      cnRangeLd     constant number(5) := length( csRange );
      lNums         SYS.ODCINUMBERLIST := SYS.ODCINUMBERLIST();
    begin
      if (validate_conversion(csValue as number) = 1) then
        if (to_number(csValue) is not null) then
          lNums.extend;
          lNums(lNums.count) := to_number(csValue);
        end if;
      elsif instr(csValue, csRange) != 0 then
        -- Range identificator found
        sSplit := substr(csValue, 1, instr(csValue, csRange) - cnRangeLd);
        nRangeStart := case when (validate_conversion(sSplit as number) = 1) then to_number(sSplit) else null end;
        sSplit := substr(csValue, instr(csValue, csRange) + cnRangeLd);
        nRangeEnd := case when (validate_conversion(sSplit as number) = 1) then to_number(sSplit) else null end;
        if (nRangeStart is not null and nRangeEnd is not null) then
          for nNum in nRangeStart .. nRangeEnd loop
            lNums.extend;
            lNums(lNums.count) := nNum;
          end loop;
        end if;
      end if;
      return lNums;
    end;
begin
  if cnLen > 0 then
    nEnd := instr(sList, csDelimiter, nStart);
    while nEnd > 0 loop
      lNums := processValue( SUBSTR(sList, nStart, nEnd - nStart) );
      for i in 1 .. lNums.count loop
        pipe row(lNums(i));
      end loop;
      nStart := nEnd + cnLd;
      nEnd := instr(sList, csDelimiter, nStart);
    end loop;
  end if;
  return;
end splitNumbers;

end isr$util;