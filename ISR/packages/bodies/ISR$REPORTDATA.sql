CREATE OR REPLACE package body ISR$reportData
is

  -- Variables and constants
  csTempTabPrefix        CONSTANT varchar2(500) := STB$UTIL.GetSystemParameter('TEMP_TAB_PREFIX');
  csCreateDbgTab         CONSTANT varchar2(500) := STB$UTIL.GetSystemParameter('CREATEDEBUGTAB');
  csRECreateDbgTab       CONSTANT varchar2(500) := STB$UTIL.GetSystemParameter('RECREATE_DEBUGTAB_POST_MODEL');
  
  nCurrentJobID                   number;           -- current JobID

-- ****************************************************************************************************
-- local functions and procedures
-- ****************************************************************************************************

--==**********************************************************************************************************************************************==--
PROCEDURE setProgress(cnValue IN number,csStatusText IN VARCHAR2) IS
BEGIN
  STB$JOB.setProgress(cnValue,csStatusText,nCurrentJobID);
END setProgress;

--==**********************************************************************************************************************************************==--
FUNCTION fillTableModelEntity( csEntity   IN VARCHAR2, 
                               csIDPrefix IN VARCHAR2   DEFAULT NULL) 
   RETURN STB$OERROR$RECORD 
IS
  sMsg                  varchar2(4000);
  sCurrentName constant varchar2(100) := $$PLSQL_UNIT||'.fillTableModelEntity('||csEntity||')';
  oErrorObj             STB$OERROR$RECORD := STB$OERROR$RECORD();
  oParamList            iSR$ParamList$Rec := iSR$ParamList$Rec(); --parameter list

  CURSOR cParents IS
    SELECT DISTINCT parentEntityName
                  , parentAttrName
                  , childAttrName
                  , ma.attrname
      FROM ISR$META$RELATION r, ISR$META$RELATION$REPORT rr, ISR$COLLECTOR$ENTITIES ce, ISR$META$ATTRIBUTE ma
     WHERE childEntityName = csEntity
       AND r.relationName = rr.RelationName
       AND rr.reporttypeid = STB$OBJECT.getCurrentReporttypeId
       AND relationType IN ('FK', 'SK')
       AND ce.entityname = parentEntityName
       AND ma.entityname = childEntityName
       AND ma.attrname = SUBSTR(childAttrName, 1, INSTR(childAttrName, 'CODE', -1) - 1)||'ID';

  CURSOR cGetAdditionalWhereParents(csParententity IN VARCHAR2) IS
    SELECT ' AND par.' || parentAttrName || ' = me.' || childAttrName condition
      FROM ISR$META$RELATION r, ISR$META$RELATION$REPORT rr
     WHERE childEntityName = csEntity
       AND parentEntityName = csParententity
       AND r.relationName = rr.RelationName
       AND rr.reporttypeid = STB$OBJECT.getCurrentReporttypeId
       AND relationType IN ('SK');

  CURSOR cChilds IS
    SELECT DISTINCT childEntityName
                  , childAttrName
                  , parentAttrName
                  , ma.attrname
      FROM ISR$META$RELATION r, ISR$META$RELATION$REPORT rr, ISR$COLLECTOR$ENTITIES ce, ISR$META$ATTRIBUTE ma
     WHERE parentEntityName = csEntity
       AND r.relationName = rr.RelationName
       AND rr.reporttypeid = STB$OBJECT.getCurrentReporttypeId
       AND relationType IN ('FK', 'SK')
       AND ce.entityname = childEntityName
       AND ma.entityname = childEntityName
       AND ma.attrname = SUBSTR(childAttrName, 1, INSTR(childAttrName, 'CODE', -1) - 1)||'ID';  

  CURSOR cGetAdditionalWhereChilds(csChildentity IN VARCHAR2) IS
    SELECT ' AND par.' || childAttrName || ' = me.' || parentAttrName condition
      FROM ISR$META$RELATION r, ISR$META$RELATION$REPORT rr
     WHERE parentEntityName = csEntity
       AND childEntityName = csChildentity
       AND r.relationName = rr.RelationName
       AND rr.reporttypeid = STB$OBJECT.getCurrentReporttypeId
       AND relationType IN ('SK');              
      
  rParents       cParents%ROWTYPE;
  rChilds        cChilds%ROWTYPE;
     
  exChildCantIdentifyParent EXCEPTION;  

  sUpdateStatm   varchar2(2000);
    
BEGIN
  iSR$Trace.stat(csEntity||' begin', 'csIDPrefix: '||csIDPrefix, sCurrentName);
  
  -- fill Param List with values
  oParamList.AddParam('REPORTTYPEID', STB$OBJECT.getCurrentReporttypeId);
  oParamList.AddParam('REPID', STB$OBJECT.getCurrentRepid);
  -- other params here ?....
  
  -- collect data for entity
  oErrorObj := ISR$REMOTE$COLLECTOR.fillTmpTableForEntity(csEntity, oParamList, 'F');
  IF oErrorObj.sSeverityCode!=Stb$typedef.cnNoError THEN      
    RETURN oErrorObj;
  END IF;    

  -- update own id
  sUpdateStatm := 'update '||csTempTabPrefix||csEntity||' set id='''||csIDPrefix||'''||entityorderby';
  iSR$Trace.debug(csEntity||' sUpdateStatm', sUpdateStatm, sCurrentName);
  execute immediate sUpdateStatm;
 
  -- update ids for referenced parents
  OPEN cParents;
  FETCH cParents INTO rParents;
  WHILE cParents%FOUND LOOP
    sUpdateStatm := 'update '||csTempTabPrefix||csEntity||' me 
                     set '||rParents.attrname||' = NVL( '||
                        '(select distinct id 
                            from '||csTempTabPrefix||rParents.parentEntityName||' par 
                          where par.'||rParents.parentAttrName||' = me.'||rParents.childAttrName;
    FOR rGetAdditionalWhere IN cGetAdditionalWhereParents(rParents.ParentEntityName) LOOP
      sUpdateStatm := sUpdateStatm||rGetAdditionalWhere.condition;
    END LOOP;
    sUpdateStatm := sUpdateStatm||'), ' ||rParents.attrname|| ')';
    iSR$Trace.debug(csEntity||' sUpdateStatm', sUpdateStatm, sCurrentName);
    
    <<ImmediateExecution>>
    BEGIN 
      execute immediate sUpdateStatm;
    EXCEPTION WHEN OTHERS THEN
      if sqlcode = -1427 then
        -- track error ORA-01427: single-row subquery returns more than one row
        iSR$Trace.error(csEntity||' could not update the parent-ID column '||rParents.attrname||' from '||rParents.parentEntityName||' for current child entity '||csEntity, sUpdateStatm, sCurrentName);  
        ---
        --- Prinzipiell ist etwas it den Daten oder dem definierten Modell nict in Ordnung, wenn diese exception auftritt
        --- unter normalen Umstaenden (keine Umgebungsfehler) heisst das hier, das ein CHILD mehrere PARENTS hat
        --- dies kann in einem solchen relationalen Modell nicht sein (unser PK kommt im gelieferten Recodset mehrfach vor und ist damit kein PK!!!!)
        --- dann ist entweder der Entityview falsch oder die Daten sind nicht nach den Regeln aufgesetzt
        --- (z.B. zwei identische Conditions (5 Grad, wobei die Conditionbezeichnung der PK ist - um eine Umlagerungsstudie abzubilde ist das inhaltlich ok, dann ist aber die Bezeichnung kein gute PK!!. )
        ---
        sMsg :=  utd$msglib.getmsg ('exChildCantIdentifyParentText', stb$security.getcurrentlanguage, csP1 => csEntity, csP2 => rParents.parentEntityName );
        raise exChildCantIdentifyParent; 
      
      else
        -- unexpected error. Let it go the normal way
        raise;
      end if;   
    END ImmediateExecution;
    
    FETCH cParents INTO rParents;
  END LOOP;
  CLOSE cParents;
  
  -- update ids for referenced childs
  OPEN cChilds;
  FETCH cChilds INTO rChilds;
  WHILE cChilds%FOUND LOOP
    sUpdateStatm := 'update '||csTempTabPrefix||rChilds.childEntityName||' me 
                     set '||rChilds.attrname||'  = '||
                        '(select distinct id
                            from '||csTempTabPrefix||csEntity||' par 
                          where par.'||rChilds.parentAttrName||' = me.'||rChilds.childAttrName;
    FOR rGetAdditionalWhere IN cGetAdditionalWhereChilds(rChilds.ChildEntityName) LOOP
      sUpdateStatm := sUpdateStatm||rGetAdditionalWhere.condition;
    END LOOP;
    sUpdateStatm := sUpdateStatm||')';
    iSR$Trace.debug(csEntity||' inverse sUpdateStatm', sUpdateStatm, sCurrentName);     
    BEGIN
      execute immediate sUpdateStatm;
    EXCEPTION WHEN OTHERS THEN
      iSR$Trace.warn(csEntity||' could not update the childs Parent-ID in '||csTempTabPrefix||rChilds.childEntityName, sUpdateStatm, sCurrentName);
      --null; -- nb: warum ok ? was wird erwartet
      -- HR: weiss auch nicht wann das auftritt, habe aber versucht obige Warnung aussagekraeftiger zu machen
    END;
    FETCH cChilds INTO rChilds;
  END LOOP;
  CLOSE cChilds;  
    
  -- create debug table
  if csCreateDbgTab = 'T' then
    -- [ISRC-397]
    oErrorObj := isr$debug.createDebugTable( csSourceTable => csTempTabPrefix||csEntity,
                                             csTargetTable => 'DBG$'||csEntity  );   
  end if;
  
  iSR$Trace.stat(csEntity||' end', 'end', sCurrentName);
  RETURN oErrorObj;

exception
  when exChildCantIdentifyParent then
    oErrorObj.handleError ( Stb$typedef.cnSeverityCritical, sMsg, sCurrentName, 
                            dbms_utility.format_error_stack || CHR(10) || dbms_utility.format_error_backtrace );  
      RETURN oErrorObj;
  when others then                                                     
    sMsg :=  utd$msglib.getmsg ('exUnexpectedError', stb$security.getcurrentlanguage, csP1 => sqlerrm(sqlcode));
    oErrorObj.handleError ( Stb$typedef.cnSeverityCritical, sMsg, sCurrentName, 
                            dbms_utility.format_error_stack || CHR(10) || dbms_utility.format_error_backtrace );  
    return oErrorObj;  

END fillTableModelEntity;  

-- ****************************************************************************************************
-- public functions and procedures
-- ****************************************************************************************************

--==**********************************************************************************************************************************************==--
FUNCTION fillTableModel(cnJobId IN NUMBER) RETURN STB$OERROR$RECORD 
IS
  sMsg                  varchar2(4000);
  sCurrentName constant varchar2(100) := $$PLSQL_UNIT||'.fillTableModelEntity';
  oErrorObj             STB$OERROR$RECORD := STB$OERROR$RECORD();
  
  sPreModelFunc         varchar2(400);
  sPostModelFunc        varchar2(400);
    
  -- for temp tables
  oParamList            iSR$AttrList$Rec;

  cursor cGetEntities is
    SELECT DISTINCT MAX (orderno), d.entityname, x.idprefix
      FROM (SELECT ROWNUM orderno, entityname
              FROM (SELECT *
                      FROM isr$collector$entities
                    ORDER BY branch, ordercol, leaf desc)) d, isr$meta$entity e, isr$meta$xml x
     WHERE  d.entityname = e.entityname
        and d.entityname = x.entityname(+)
        and e.critonly = 'F'
     GROUP BY d.entityname, x.idprefix
     ORDER BY 1;
     
  sRightsEntity varchar2(100);
  sRightsColumn varchar2(100);
  sSQL varchar2(500);
  sRightsVal varchar2(100);
  
BEGIN
  iSR$Trace.stat('begin', 'cnJobId: '||cnJobId, sCurrentName);
  
  nCurrentJobID   := cnJobID;
  
  -- Initialize parameters for this job session
  oErrorObj := Stb$object.initRepType(STB$JOB.getJobParameter('REPORTTYPEID',cnJobId), STB$JOB.getJobParameter('REPID',cnJobId));
  
  IF stb$object.getCurrentReporttypeid IS NULL THEN 
    stb$object.setCurrentReporttypeid(0);
  END IF;  
  
  -- Fill temporary tables with data = standard processing
  -- Bypass if fast debugging is active and log only
  IF UPPER(csTempTabPrefix) != 'DBG$' THEN 
  
    -- call pre model trigger
    sPreModelFunc := STB$UTIL.getRepTypeParameter(STB$OBJECT.getCurrentReporttypeId, 'PRE_MODEL_TRIGGER');
    
    if sPreModelFunc IS NOT NULL THEN   
      begin
        EXECUTE immediate
          'BEGIN
             :1 :='|| sPreModelFunc || '(:2, :3);
           END;'
        using OUT oErrorObj, IN STB$OBJECT.getCurrentReporttypeId, IN STB$OBJECT.getCurrentRepId;
      exception
        when others then
          iSR$Trace.error('error when executing pre model trigger'||sPreModelFunc, SQLERRM, sCurrentName);
      end;  
    end if;  
  
    -- fill model tmp tables
    FOR rGetEntities IN cGetEntities LOOP
      setProgress(20, UTD$MSGLIB.getMsg('temporary table', STB$SECURITY.getCurrentLanguage)||' '||rGetEntities.entityname);
      -- Process all entities, that are retrieved in the cursor
      oErrorObj := fillTableModelEntity(rGetEntities.entityname, rGetEntities.idPrefix);
      -- In case of error: Leave with returning eErrorObj
      IF oErrorObj.sSeverityCode != Stb$typedef.cnNoError THEN
        RETURN oErrorObj;
      END IF;
    END LOOP;
  
    -- call post model trigger if defined
    sPostModelFunc := STB$UTIL.getRepTypeParameter(STB$OBJECT.getCurrentReporttypeId, 'POST_MODEL_TRIGGER');
    
    if sPostModelFunc IS NOT NULL THEN   
      begin
        EXECUTE immediate
          'BEGIN
             :1 :='|| sPostModelFunc || ';
           END;'
        using OUT oErrorObj;
      exception
        when others then
          iSR$Trace.error('error when executing post model trigger '||sPreModelFunc, SQLERRM, sCurrentName);
      end;  
    end if; 
    
    -- recreate debug tables
    if csRECreateDbgTab = 'T' then
      FOR rGetEntities IN cGetEntities LOOP      
        -- [ISRC-397]
        oErrorObj := isr$debug.createDebugTable( csSourceTable => csTempTabPrefix||rGetEntities.entityname,
                                                 csTargetTable => 'DBG$'||rGetEntities.entityname );  
      END LOOP;  
    end if; 
    
  end if;
  
  --
  sRightsEntity := STB$UTIL.getReptypeParameter(STB$OBJECT.getCurrentReporttypeid, 'SECURITY_ENTITY');
  sRightsColumn := STB$UTIL.getReptypeParameter(STB$OBJECT.getCurrentReporttypeid, 'SECURITY_KEY');
  if sRightsEntity is not null and sRightsColumn is not null then
    sSQL := 'select trim('||sRightsColumn||') from tmp$'||sRightsEntity||' order by '||sRightsColumn;
    iSR$Trace.debug('sSQL', sSQL, sCurrentName);
    execute immediate sSQL into sRightsVal;
    iSR$Trace.debug('sRightsVal', sRightsVal, sCurrentName);
    
    if sRightsVal is not null then
      update stb$report 
      set secid = sRightsVal
      where repid = STB$OBJECT.getCurrentRepid;
    end if; 
  end if;   
  
  iSR$Trace.stat('end', 'end', sCurrentName);
  RETURN oErrorObj;

exception
  when others then                                                     
    sMsg :=  utd$msglib.getmsg ('exUnexpectedError', stb$security.getcurrentlanguage, csP1 => sqlerrm(sqlcode));
    oErrorObj.handleError ( Stb$typedef.cnSeverityCritical, sMsg, sCurrentName, 
                            substr(dbms_utility.format_error_stack || CHR(10) || dbms_utility.format_error_backtrace, 1,4000)  );  
    return oErrorObj;    


END fillTableModel;

end ISR$reportData;