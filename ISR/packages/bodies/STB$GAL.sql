CREATE OR REPLACE PACKAGE BODY STB$GAL
is
  sPackage            varchar2(200);
  sCreateTheDoc       varchar2(1)   := 'F';
  crlf                constant varchar2(2)  := chr(10)||chr(13);

  sMethodExists varchar2(1) := 'F';
  exNoTokenHandle exception;

  sOracleUserName           varchar2 (255);

--*********************************************************************************************************************************
function isSubnetAllowed( sIp       in varchar2,
                          sSubnetNr in varchar2,
                          sAllowed  out varchar2 )
  return STB$OERROR$RECORD
is
  sCurrentName    constant varchar2(100) := $$PLSQL_UNIT||'.isSubnetAllowed';
  sUserMsg        varchar2(4000);
  sDevMsg         varchar2(4000);
  sImplMsg        varchar2(4000);
  sSubnetAllowed  varchar2(64);
  oErrorObj       STB$OERROR$RECORD := STB$OERROR$RECORD(tloMessageList => ISR$MESSAGE$LIST()); --initialize the message list too
  sMinIp          varchar2(64);
  sMaxIp          varchar2(64);
  nStartTimeNum   number      := DBMS_UTILITY.GET_TIME;
begin
  isr$trace.stat('begin', 'sIp: '||sIp||'  sSubnetNr: '||sSubnetNr, sCurrentName);

  oErrorObj := Stb$util.getSystemParameter('SUBNET' || sSubnetNr, sSubnetAllowed);
  isr$trace.debug('status sSubnetNr sSubnetAllowed sIp', 'sSubnetNr:'||sSubnetNr||' sSubnetAllowed:'||sSubnetAllowed||' sIp:'||sIp, sCurrentName);
  if Trim(sSubnetAllowed) is null then
    sAllowed := 'N'; -- only if all the pars null it is true
  else
    sMinIp := Trim(Substr(sSubnetAllowed,1,Instr(sSubnetAllowed,'-',1,1)-1));
    sMaxIp := Trim(Substr(sSubnetAllowed,Instr(sSubnetAllowed,'-',1,1)+1));
    isr$trace.debug('status sMinIP sMaxIP', 'sMinIp: '||sMinIp||' sMaxIp: '||sMaxIp, sCurrentName);
    sAllowed := Stb$java.ipInRange(sIp,sMinIp, sMaxIp);
  end if;

  -- reset important settings in error object
  oErrorObj.SetSeverityCodeToWorst;
  oErrorObj.SetMessageListFromAccessor;

  isr$trace.stat('end', 'sAllowed: '||sAllowed, sCurrentName);
  return oErrorObj;
exception
  when others then
    sUserMsg := utd$msglib.getmsg ('exUnexpectedError', stb$security.getCurrentLanguage,  csP1 => sqlerrm(sqlcode));
    sImplMsg := sCurrentName || rtrim(': '||sImplMsg,': ');
    sDevMsg  := substr(dbms_utility.format_error_stack ||stb$typedef.crlf|| dbms_utility.format_error_backtrace, 1,4000);
    oErrorObj.handleError ( Stb$typedef.cnSeverityCritical, sUserMsg, sImplMsg, sDevMsg );
  return oErrorObj;
end isSubnetAllowed;

--************************************************************************************************************************************
function verifyLDAPConnection( sUsername in varchar2,
                               sPassword in varchar2 )
  return STB$OERROR$RECORD
is
  oErrorObj     STB$OERROR$RECORD := STB$OERROR$RECORD(tloMessageList => ISR$MESSAGE$LIST()); --initialize the message list too
  sCurrentName  constant varchar2(100) := $$PLSQL_UNIT||'.verifyLDAPConnection';

  sUserMsg      varchar2(4000);
  sDevMsg       varchar2(4000);
  sImplMsg      varchar2(4000);
  nStartTimeNum number      := DBMS_UTILITY.GET_TIME;

  /*exLdapConnection   exception;
  exNoLDAPServer     exception;

  clReturn      clob;

  cursor cGetLDAPServer is
    select HOST
         , port
         , isr$server$base.getServerParameter (serverid, 'USERNAME') username
         , isr$server$base.getServerParameter (serverid, 'PASSWORD') password
         , isr$server$base.getServerParameter (serverid, 'LDAPBASE') ldapbase
         , isr$server$base.getServerParameter (serverid, 'LDAPSEARCH') ldapsearch
         , isr$server$base.getServerParameter (serverid, 'SSLWRL') sslWrl
         , isr$server$base.getServerParameter (serverid, 'SSLWALLETPASSWD') sslWalletPasswd
         , isr$server$base.getServerParameter (serverid, 'SSLAUTH') sslAuth
      from isr$server
     where servertypeid = 5;

  rGetLDAPServer cGetLDAPServer%rowtype;

  cursor cGetConnectionError is
    select EXTRACTVALUE (xmltype (clReturn), '/CONNECTIONERROR')
      from dual;

  sGetConnectionError   varchar2(4000);      */

begin
  isr$trace.info('begin', 'Function '||sCurrentName||' started',sCurrentName);

  oErrorObj := ISR$SERVER$LDAP.verifyLDAPConnection (sUsername, sPassword);

  /*
  open cGetLDAPServer;
  fetch cGetLDAPServer into rGetLDAPServer;
  if cGetLDAPServer%FOUND then
    while cGetLDAPServer%FOUND loop

      clReturn := ISR$LDAP.getLDAPInformation ( sHost => rGetLDAPServer.HOST
                                              , sPort => rGetLDAPServer.port
                                              , sSSLAuth => rGetLDAPServer.sslauth
                                              , sSSLWrl => rGetLDAPServer.sslWrl
                                              , sSSLWalletPasswd => rGetLDAPServer.sslWalletPasswd
                                              , sGlobUserName => rGetLDAPServer.username
                                              , sGlobPassword => rGetLDAPServer.password
                                              , sLDAPBase => rGetLDAPServer.ldapbase
                                              , sAttributes => rGetLDAPServer.ldapsearch
                                              , sFilter => rGetLDAPServer.ldapsearch || '=' || sUsername
                                              , sPassword => sPassword
                                              );

      if clReturn = 'T' then
        STB$SECURITY.setCurrentUsername(sUsername);
        STB$SECURITY.setCurrentPassword(sPassword);
      else
        open cGetConnectionError;
        fetch cGetConnectionError into sGetConnectionError;
        close cGetConnectionError;
        if sGetConnectionError is not null then
          raise exLdapConnection;
        end if;
      end if;

      fetch cGetLDAPServer into rGetLDAPServer;
    end loop;
  else
    raise exNoLDAPServer;
  end if;
  close cGetLDAPServer;

  -- reset important settings in error object
  oErrorObj.SetSeverityCodeToWorst;
  oErrorObj.SetMessageListFromAccessor;
  */

  isr$trace.info('end', 'Function '||sCurrentName||' finished',sCurrentName);
  return oErrorObj;

exception
  /*when exNoLDAPServer then
    sUserMsg := utd$msglib.getmsg ('exNoLDAPServer', nCurrentLanguage,  csP1 => sqlerrm(sqlcode));
    sImplMsg := sCurrentName || rtrim(': '||sImplMsg,': ');
    sDevMsg  := substr(dbms_utility.format_error_stack ||stb$typedef.crlf|| dbms_utility.format_error_backtrace, 1,4000);
    oErrorObj.handleError ( Stb$typedef.cnSeverityCritical, sUserMsg, sImplMsg, sDevMsg );
    return oErrorObj;
  when exLdapConnection then
    sUserMsg := utd$msglib.getmsg ('exLdapConnection', nCurrentLanguage,  csP1 => sqlerrm(sqlcode));
    sImplMsg := sCurrentName || rtrim(': '||sImplMsg,': ');
    sDevMsg  := substr(dbms_utility.format_error_stack ||stb$typedef.crlf|| dbms_utility.format_error_backtrace, 1,4000);
    oErrorObj.handleError ( Stb$typedef.cnSeverityCritical, sUserMsg, sImplMsg, sDevMsg );
    return oErrorObj;*/
  when others then
    sUserMsg := utd$msglib.getmsg ('exUnexpectedError', stb$security.getCurrentLanguage,  csP1 => sqlerrm(sqlcode));
    sImplMsg := sCurrentName || rtrim(': '||sImplMsg,': ');
    sDevMsg  := substr(dbms_utility.format_error_stack ||stb$typedef.crlf|| dbms_utility.format_error_backtrace, 1,4000);
    oErrorObj.handleError ( Stb$typedef.cnSeverityCritical, sUserMsg, sImplMsg, sDevMsg );
    return oErrorObj;
END verifyLDAPConnection;

--****************************************************************************************************
function insertSessionInfo(csUserName in varchar2)
  return STB$OERROR$RECORD
is
  pragma autonomous_transaction;

  exNoLicence        exception;
  exWrongLicenceFile exception;
  sCurrentName       constant varchar2(100) := $$PLSQL_UNIT||'.insertSessionInfo('||csUserName||')';
  sUserMsg           varchar2(4000);
  sDevMsg            varchar2(4000);
  sImplMsg           varchar2(4000);
  oErrorObj          STB$OERROR$RECORD := STB$OERROR$RECORD(tloMessageList => ISR$MESSAGE$LIST()); --initialize the message list too
  rSessionid         ISR$SESSION.sessionid%TYPE;
  nStartTimeNum      number      := DBMS_UTILITY.GET_TIME;

  cursor cGetInactiveSessions is
    select sessionid
      from isr$session
     where ip = STB$SECURITY.getClientIp
       and username = csUserName
       and inactive = 'T';
begin
  isr$trace.stat('begin', 'csUserName: '||csUserName, sCurrentName);

  oErrorObj := isr$licencefile.checkSessionAllowed;

  if oErrorObj.sSeverityCode = STB$TYPEDEF.cnSeverityCritical then
    raise exNoLicence;
  elsif oErrorObj.sErrorCode != 'CRACK_CODE' then
    raise exWrongLicenceFile;
  else
    oErrorObj := STB$OERROR$RECORD(tloMessageList => ISR$MESSAGE$LIST()); --initialize the message list too
  end if;

  open cGetInactiveSessions;
  fetch cGetInactiveSessions into rSessionid;
  if cGetInactiveSessions%FOUND then
    deleteSessionInfo(rSessionid);
  end if;

  insert into ISR$SESSION (SESSIONID
                         , IP
                         , PORT
                         , CREATEDON
                         , MODIFIEDON
                         , SECUREMODE
                         , USERNAME
                         , USERNO)
  values (STB$SECURITY.getCurrentSession
        , STB$SECURITY.getClientIp
        , STB$SECURITY.getClientPort
        , sysdate
        , sysdate
        , ISR$LICENCEFILE.getMode
        , csUserName
        , STB$SECURITY.getCurrentUser );

  oErrorObj := isr$util.initializeSession;    -- [ISRC-1108]

  commit;

  -- reset important settings in error object
  oErrorObj.SetSeverityCodeToWorst;
  oErrorObj.SetMessageListFromAccessor;

  isr$trace.stat('end', 'end', sCurrentName);
  return (oErrorObj);

exception
  when exNoLicence then
    sUserMsg := utd$msglib.getmsg ('LICENCE_EXPIRED', stb$security.getCurrentLanguage,  csP1 => sqlerrm(sqlcode));
    sImplMsg := sCurrentName || rtrim(': '||sImplMsg,': ');
    sDevMsg  := substr(dbms_utility.format_error_stack ||stb$typedef.crlf|| dbms_utility.format_error_backtrace, 1,4000);
    oErrorObj.handleError ( Stb$typedef.cnSeverityCritical, sUserMsg, sImplMsg, sDevMsg );
    return (oErrorObj);
  when exWrongLicenceFile then
    sUserMsg := utd$msglib.getmsg ('WRONG_LICENCE_FILE', stb$security.getCurrentLanguage,  csP1 => sqlerrm(sqlcode));
    sImplMsg := sCurrentName || rtrim(': '||sImplMsg,': ');
    sDevMsg  := substr(dbms_utility.format_error_stack ||stb$typedef.crlf|| dbms_utility.format_error_backtrace, 1,4000);
    oErrorObj.handleError ( Stb$typedef.cnSeverityCritical, sUserMsg, sImplMsg, sDevMsg );
    return (oErrorObj);
  when others then
    rollback;
    sUserMsg := utd$msglib.getmsg ('exUnexpectedError', stb$security.getCurrentLanguage,  csP1 => sqlerrm(sqlcode));
    sImplMsg := sCurrentName || rtrim(': '||sImplMsg,': ');
    sDevMsg  := substr(dbms_utility.format_error_stack ||stb$typedef.crlf|| dbms_utility.format_error_backtrace, 1,4000);
    oErrorObj.handleError ( Stb$typedef.cnSeverityCritical, sUserMsg, sImplMsg, sDevMsg );
    return oErrorObj;
end insertSessionInfo;

--****************************************************************************************************
function updateSessionInfo(csCheckOnly in varchar2 default 'T')
  return STB$OERROR$RECORD
is
  pragma autonomous_transaction;
  exSessionKilled exception;
  sCurrentName    constant varchar2(100) := $$PLSQL_UNIT||'.updateSessionInfo';
  sUserMsg        varchar2(4000);
  sDevMsg         varchar2(4000);
  sImplMsg        varchar2(4000);
  oErrorObj       STB$OERROR$RECORD := STB$OERROR$RECORD(tloMessageList => ISR$MESSAGE$LIST()); --initialize the message list too  oErrorObj       STB$OERROR$RECORD := STB$OERROR$RECORD(tloMessageList => ISR$MESSAGE$LIST());
  nStartTimeNum   number      := DBMS_UTILITY.GET_TIME;
  sSessionid      ISR$SESSION.SESSIONID%type;

  cursor cGetSession is
    select sessionid
      from isr$session
     where sessionid = STB$SECURITY.getCurrentSession;
begin
  isr$trace.stat('begin', 'csCheckOnly: '||csCheckOnly, sCurrentName);

  open cGetSession;
  fetch cGetSession into sSessionid;
  if cGetSession%NOTFOUND or Trim(sSessionid) is null then
    close cGetSession;
    raise exSessionKilled;
  end if;
  close cGetSession;

-- (NB) sessiontabelle nicht aktualisiern! verlagert nach CallFunction 26.2.2016

  -- reset important settings in error object
  oErrorObj.SetSeverityCodeToWorst;
  oErrorObj.SetMessageListFromAccessor;

  isr$trace.stat('end', 'end', sCurrentName);
  return (oErrorObj);

exception
  when exSessionKilled then
    sUserMsg := utd$msglib.getmsg ('exSessionKilled', stb$security.getCurrentLanguage,  csP1 => sqlerrm(sqlcode));
    sImplMsg := sCurrentName || rtrim(': '||sImplMsg,': ');
    sDevMsg  := substr(dbms_utility.format_error_stack ||stb$typedef.crlf|| dbms_utility.format_error_backtrace, 1,4000);
    oErrorObj.handleError ( Stb$typedef.cnSeverityCritical, sUserMsg, sImplMsg, sDevMsg );
    return oErrorObj;
  when others then
    rollback;
    sUserMsg := utd$msglib.getmsg ('exUnexpectedError', stb$security.getCurrentLanguage,  csP1 => sqlerrm(sqlcode));
    sImplMsg := sCurrentName || rtrim(': '||sImplMsg,': ');
    sDevMsg  := substr(dbms_utility.format_error_stack ||stb$typedef.crlf|| dbms_utility.format_error_backtrace, 1,4000);
    oErrorObj.handleError ( Stb$typedef.cnSeverityCritical, sUserMsg, sImplMsg, sDevMsg );
    return oErrorObj;
end updateSessionInfo;

--****************************************************************************************************
procedure updateSessionWizardInfo(csWizardInUse in varchar2 default 'F')
is
  pragma autonomous_transaction;
  sCurrentName  constant varchar2(100) := $$PLSQL_UNIT||'.updateSessionWizardInfo';
  oErrorObj     STB$OERROR$RECORD := STB$OERROR$RECORD();
  sUserMsg      varchar2(4000);
  sDevMsg       varchar2(4000);
  sImplMsg      varchar2(4000);
  nStartTimeNum number      := DBMS_UTILITY.GET_TIME;
begin
  isr$trace.stat('begin', 'csWizardInUse: '||csWizardInUse, sCurrentName);

  isr$trace.debug('vor update isr$session', STB$SECURITY.getCurrentSession , sCurrentName);
  update isr$session
     set wizardInUse = csWizardInUse
   where sessionid = STB$SECURITY.getCurrentSession
     and wizardInUse != csWizardInUse;
  commit;

  isr$trace.stat('end', 'end', sCurrentName);

exception
  when others then
    rollback;
    sUserMsg := utd$msglib.getmsg ('exUnexpectedError', stb$security.getCurrentLanguage,  csP1 => sqlerrm(sqlcode));
    sImplMsg := sCurrentName || rtrim(': '||sImplMsg,': ');
    sDevMsg  := substr(dbms_utility.format_error_stack ||stb$typedef.crlf|| dbms_utility.format_error_backtrace, 1,4000);
    oErrorObj.handleError ( Stb$typedef.cnSeverityCritical, sUserMsg, sImplMsg, sDevMsg );
END updateSessionWizardInfo;


--**********************************************************************************************************************
function deleteTmpTransferData(cnSessionId in number) return STB$OERROR$RECORD
is
sCurrentName  constant varchar2(100) := $$PLSQL_UNIT||'.deleteTmpTransferData('||cnSessionId||')';
oErrorObj     STB$OERROR$RECORD := STB$OERROR$RECORD();
begin
  isr$trace.stat('begin', 'begin', sCurrentName);
  oErrorObj := isr$server$collector$rec.deleteTmpDataForSession(cnSessionId);
  oErrorObj := isr$xml.deleteTmpDataForSession(cnSessionId);
  isr$trace.stat('end', 'end', sCurrentName);
  return oErrorObj;
end deleteTmpTransferData;

--****************************************************************************************************
procedure deleteSessionInfo(nSessionId in ISR$SESSION.SESSIONID%type, csUserNo in number default null)
is
  pragma autonomous_transaction;
  sCurrentName  constant varchar2(100) := $$PLSQL_UNIT||'.deleteSessionInfo('||nSessionId||')';
  sUserMsg      varchar2(4000);
  sDevMsg       varchar2(4000);
  sImplMsg      varchar2(4000);
  oErrorObj     STB$OERROR$RECORD := STB$OERROR$RECORD(tloMessageList => ISR$MESSAGE$LIST()); --initialize the message list too
  nSessionCnt   number := 0;
  nStartTimeNum number      := DBMS_UTILITY.GET_TIME;

  cursor cGetSessionUser(cnSessionId in number) is
    select stb$user.userno
      from stb$user, isr$session
     where sessionid = cnSessionId
       and (UPPER(username) = UPPER(ldapusername) or UPPER(username) = UPPER(oracleusername));

  nSessionUser        STB$USER.USERNO%type;

  -- check for all running jobs which don't use the rendering server
  cursor cGetJobSessionCnt is
    select count (sessionid)
      from STB$JOBSESSION js, STB$JOBQUEUE jq, user_scheduler_jobs us
     where TRIM (jq.oraclejobid) = TRIM (us.job_name)
       and js.jobid = jq.jobid
       and js.sessionid = nSessionId
       and us.state = 'RUNNING'
       and (js.ip, js.port) not in (select distinct isr$server.host, isr$server$base.getServerParameter(serverid, 'BASISPORT')
                                      from isr$server
                                     where isr$server.runstatus = 'T'
                                       and isr$server.servertypeid = 6);
  cursor cGetSessionIp(cnSessionId in number) is
  select ip
  from isr$session
  where sessionid = cnSessionId;
  sIp   isr$session.ip%type;

begin
  isr$trace.stat('begin', 'nSessionId: '||nSessionId || ' csUserNo: ' || csUserNo, sCurrentName);

  open cGetSessionUser(nSessionId);
  fetch cGetSessionUser into nSessionUser;
  close cGetSessionUser;

  isr$trace.debug('current session', stb$security.GetCurrentSession, sCurrentName);
  open cGetSessionIp(nSessionId);
  fetch cGetSessionIp into sIp;
  close cGetSessionIp;

  oErrorObj := deleteTmpTransferData(nSessionId);

  for rGetJobs in ( select jq.jobid, js.ip, js.port, js.oraclejobsessionid
                      from STB$JOBQUEUE jq, STB$JOBSESSION js
                     where TRIM(STB$JOB.GETJOBPARAMETER ('JOB_ERROR', jq.jobid)) is null
                       and jq.jobid = js.jobid
                       and js.sessionid = nSessionId
                       and TRIM (jq.oraclejobid) not in (select trim(job_name) from user_scheduler_jobs)
                     union
                       select jq.jobid, js.ip, js.port, js.oraclejobsessionid
                       from STB$JOBQUEUE jq, STB$JOBSESSION js
                     where TRIM(STB$JOB.GETJOBPARAMETER ('JOB_ERROR', jq.jobid)) is null
                       and jq.jobid = js.jobid
                       and jq.userno = csUserNo
                       and TRIM (jq.oraclejobid) not in (select trim(job_name) from user_scheduler_jobs)) loop
    STB$JOB.deleteJobs(rGetJobs.jobid);
    oErrorObj := deleteTmpTransferData(rGetJobs.oraclejobsessionid);
    if rGetJobs.ip != sIp then
      STB$JOB.removeJobFolder(rGetJobs.jobid, rGetJobs.ip, rGetJobs.port);
    end if;
    isr$trace.debug('job deleted', 'jobid '||rGetJobs.jobid, sCurrentName);
  end loop;

  open cGetJobSessionCnt;
  fetch cGetJobSessionCnt into nSessionCnt;
  if cGetJobSessionCnt%NOTFOUND or nSessionCnt = 0 then
    delete from isr$session
    where sessionid = nSessionId;
    isr$trace.debug('session deleted', 'nSessionId '||nSessionId, sCurrentName);
  else
    isr$trace.debug('vor update isr$session', STB$SECURITY.getCurrentSession , sCurrentName);
    update isr$session
       set modifiedon = sysdate
         , markedforshutdown = 'T'
     where sessionid = nSessionId;
    isr$trace.debug('session updated', 'nSessionId '||nSessionId, sCurrentName);
  end if;
  close cGetJobSessionCnt;

  delete from isr$tree$sessions
   where sessionid = nSessionId
      or sessionid not in (select sessionid from isr$session)
      or sessionid in (select sessionid
                         from isr$session
                        where markedforshutdown = 'T');
  isr$trace.debug('tree session deleted','', sCurrentName);

  /* how can we clean up here? delete it all */
  delete from ISR$TREE$NODES
  where SUBSTR (nodeid, INSTR (nodeid, '#@#') + 3)||'T' in (SELECT userno||markedforshutdown FROM isr$session)
     or SUBSTR (nodeid, INSTR (nodeid, '#@#') + 3) not in (SELECT to_char(userno) FROM isr$session);

  delete from ISR$REMOTE$SECURITY$KEYS
  where sessionid = STB$SECURITY.getCurrentSession
     OR sessionid NOT IN (SELECT sessionid FROM isr$session)
     OR sessionid IN (SELECT sessionid
                        FROM isr$session
                       WHERE markedforshutdown = 'T');

  if STB$UTIL.getSystemParameter('JAVA_INSIDE_DB') = 'F' AND NVL (isr$process.checkProcessServer, 'F') = 'T' AND nSessionId = STB$SECURITY.getCurrentSession THEN
    begin
      ISR$PROCESS.deleteFile(ISR$PROCESS.getUserHome||ISR$PROCESS.sSessionPath);
    exception when others then
      null;
    end;
  end if;

  oErrorObj := stb$system.unlockreports ('and locksession = ' || to_char(nSessionId));
  commit;

  ISR$TREE$PACKAGE.sendSyncCall('CURRENTCONNECTIONS', STB$SECURITY.getCurrentSession);

  isr$trace.stat('end', 'end', sCurrentName);

exception
  when others then
    rollback;
    sUserMsg := utd$msglib.getmsg ('exUnexpectedError', stb$security.getCurrentLanguage,  csP1 => sqlerrm(sqlcode));
    sImplMsg := sCurrentName || rtrim(': '||sImplMsg,': ');
    sDevMsg  := substr(dbms_utility.format_error_stack ||stb$typedef.crlf|| dbms_utility.format_error_backtrace, 1,4000);
    oErrorObj.handleError ( Stb$typedef.cnSeverityCritical, sUserMsg, sImplMsg, sDevMsg );
end deleteSessionInfo;

--************************************************************************************************************************************
function checkIsrOwner return varchar2
is
  exIsrOwnerUnknown exception;
  sCurrentName      constant varchar2(100) := $$PLSQL_UNIT||'.checkIsrOwner';
  sUserMsg          varchar2(4000);
  sDevMsg           varchar2(4000);
  sImplMsg          varchar2(4000);
  oErrorObj         STB$OERROR$RECORD := STB$OERROR$RECORD(tloMessageList => ISR$MESSAGE$LIST()); --initialize the message list too
  nStartTimeNum     number      := DBMS_UTILITY.GET_TIME;
begin
  isr$trace.stat('begin', 'begin', sCurrentName);

  if Stb$util.getSystemParameter('STBOWNER') is null then
    raise exIsrOwnerUnknown;
  end if;

  isr$trace.stat('end', 'end', sCurrentName);
  return Stb$util.getSystemParameter('STBOWNER');

exception
  when exIsrOwnerUnknown then
    sUserMsg := utd$msglib.getmsg ('exIsrOwnerUnknown', stb$security.getCurrentLanguage,  csP1 => sqlerrm(sqlcode));
    sImplMsg := sCurrentName || rtrim(': '||sImplMsg,': ');
    sDevMsg  := substr(dbms_utility.format_error_stack ||stb$typedef.crlf|| dbms_utility.format_error_backtrace, 1,4000);
    oErrorObj.handleError ( Stb$typedef.cnSeverityCritical, sUserMsg, sImplMsg, sDevMsg );
    return null;
  when others then
    sUserMsg := utd$msglib.getmsg ('exUnexpectedError', stb$security.getCurrentLanguage,  csP1 => sqlerrm(sqlcode));
    sImplMsg := sCurrentName || rtrim(': '||sImplMsg,': ');
    sDevMsg  := substr(dbms_utility.format_error_stack ||stb$typedef.crlf|| dbms_utility.format_error_backtrace, 1,4000);
    oErrorObj.handleError ( Stb$typedef.cnSeverityCritical, sUserMsg, sImplMsg, sDevMsg );
    return null;
end checkIsrOwner;


--****************************************************************************************************
procedure updateSessionUser is
pragma autonomous_transaction;
begin
  update isr$session set userno=STB$SECURITY.getCurrentUser where sessionid = STB$SECURITY.getCurrentSession;
  commit;
exception
  when others then
  isr$trace.warn('exception', sqlerrm, 'updateSessionUser');
end;

--************************************************************************************************************************************
function checkIsrUser(sUsername in varchar2, sPassword in varchar2, sFullUserName out varchar2)
  return STB$OERROR$RECORD
is
  exClientConnection  exception;
  exUserNotFound      exception;
  exUserDeactivated   exception;
  sCurrentName        constant varchar2(100) := $$PLSQL_UNIT||'.checkIsrUser';
  sUserMsg            varchar2(4000);
  sDevMsg             varchar2(4000);
  sImplMsg            varchar2(4000);
  oErrorObj           STB$OERROR$RECORD := STB$OERROR$RECORD(tloMessageList => ISR$MESSAGE$LIST()); --initialize the message list too
  sFlag               varchar2(1);
  sIp                 varchar2(64);
  nStartTimeNum       number      := DBMS_UTILITY.GET_TIME;

  cursor cIsOracleUser is
    select 'T'
      from stb$user
     where UPPER (oracleusername) = UPPER (sUsername);

  sIsOracleuser varchar2(1);

  cursor cGetUserName IS
    select fullname, activeuser
      from STB$USER
     where UPPER (sUsername) in (UPPER (oracleusername), UPPER (ldapUserName));
begin
  isr$trace.stat('begin', 'sUsername: '||sUserName||'  sPassword: '||case when sPassword is null then null else '*****' end, sCurrentName);

  -- check licence file
  oErrorObj := insertSessionInfo(sUsername);

  if oErrorObj.sSeverityCode != Stb$typedef.cnNoError then
    deleteSessionInfo(STB$SECURITY.getCurrentSession);
    return oErrorObj;
  end if;

  -- check ldap
  open cIsOracleUser;
  fetch cIsOracleUser into sIsOracleuser;
  if cIsOracleUser%NOTFOUND then
    sIsOracleuser := 'F';
    oErrorObj := verifyLDAPConnection(sUsername, sPassword);
  end if;
  close cIsOracleUser;

  if oErrorObj.sSeverityCode != Stb$typedef.cnNoError then
    deleteSessionInfo(STB$SECURITY.getCurrentSession);
    return oErrorObj;
  end if;

  -- initalize of the user
  if sIsOracleuser = 'F' then
    Stb$security.initUser(sIsOracleuser);
    stb$gal.updateSessionUser;
    sOracleUserName := STB$SECURITY.getOracleUserName (STB$SECURITY.getCurrentUser);
  end if;

  ISR$TRACE.setSessionInfo;

  -- setting of the application info
  setApplicationInfo;

  -- check if subnet ok
  sIp := Stb$security.getClientIp;
  for i in 1..3 loop
    oErrorObj := isSubnetAllowed(sIp, i, sFlag);
    if(sFlag = 'T') or oErrorObj.sSeverityCode != Stb$typedef.cnNoError then
      exit;
    end if;
  end loop;

  if sFlag = 'F' or oErrorObj.sSeverityCode != Stb$typedef.cnNoError then
    raise exClientConnection;
  end if;

  -- check if user active
  open cGetUserName;
  fetch cGetUserName into sFullUserName, sFlag;
  close cGetUserName;

  if TRIM(sFullUserName) is null then
    raise exUserNotFound;
  end if;

  if sFlag = 'F' then
    raise exUserDeactivated;
  end if;

  isr$action$log.SetActionLog('LOGON', sUsername);

  -- reset important settings in error object
  oErrorObj.SetSeverityCodeToWorst;
  oErrorObj.SetMessageListFromAccessor;

  isr$trace.stat('end', 'sFullUserName: '||sFullUserName, sCurrentName);
  return oErrorObj;

exception
  when exClientConnection then
    sUserMsg := utd$msglib.getmsg ('exClientConnection', stb$security.getCurrentLanguage,  csP1 => sqlerrm(sqlcode));
    sImplMsg := sCurrentName || rtrim(': '||sImplMsg,': ');
    sDevMsg  := substr(dbms_utility.format_error_stack ||stb$typedef.crlf|| dbms_utility.format_error_backtrace, 1,4000);
    oErrorObj.handleError ( Stb$typedef.cnSeverityCritical, sUserMsg, sImplMsg, sDevMsg );
    deleteSessionInfo(STB$SECURITY.getCurrentSession);
    return oErrorObj;
  when exUserNotFound then
    sUserMsg := utd$msglib.getmsg ('exUserNotFound', stb$security.getCurrentLanguage,  csP1 => sqlerrm(sqlcode));
    sImplMsg := sCurrentName || rtrim(': '||sImplMsg,': ');
    sDevMsg  := substr(dbms_utility.format_error_stack ||stb$typedef.crlf|| dbms_utility.format_error_backtrace, 1,4000);
    oErrorObj.handleError ( Stb$typedef.cnSeverityCritical, sUserMsg, sImplMsg, sDevMsg );
    deleteSessionInfo(STB$SECURITY.getCurrentSession);
    return oErrorObj;
  when exUserDeactivated then
    sUserMsg := utd$msglib.getmsg ('exUserDeactivated', stb$security.getCurrentLanguage,  csP1 => sqlerrm(sqlcode));
    sImplMsg := sCurrentName || rtrim(': '||sImplMsg,': ');
    sDevMsg  := substr(dbms_utility.format_error_stack ||stb$typedef.crlf|| dbms_utility.format_error_backtrace, 1,4000);
    oErrorObj.handleError ( Stb$typedef.cnSeverityCritical, sUserMsg, sImplMsg, sDevMsg );
    deleteSessionInfo(STB$SECURITY.getCurrentSession);
    return oErrorObj;
  when others then
    sUserMsg := utd$msglib.getmsg ('exUnexpectedError', stb$security.getCurrentLanguage,  csP1 => sqlerrm(sqlcode));
    sImplMsg := sCurrentName || rtrim(': '||sImplMsg,': ');
    sDevMsg  := substr(dbms_utility.format_error_stack ||stb$typedef.crlf|| dbms_utility.format_error_backtrace, 1,4000);
    oErrorObj.handleError ( Stb$typedef.cnSeverityCritical, sUserMsg, sImplMsg, sDevMsg );
    return oErrorObj;
end checkISRUser;

--****************************************************************************************************
procedure destroyOldValues(reason in number default null)
is
  sCurrentName  constant varchar2(100) := $$PLSQL_UNIT||'.destroyOldValues('||reason||')';
  sUserMsg      varchar2(4000);
  sDevMsg       varchar2(4000);
  sImplMsg      varchar2(4000);
  oErrorObj     STB$OERROR$RECORD := STB$OERROR$RECORD();
  nSessionId    STB$JOBSESSION.sessionid%type;
  nStartTimeNum number      := DBMS_UTILITY.GET_TIME;
  sHandledBy    VARCHAR2(50);
begin
  isr$trace.stat('begin', 'reason: '||reason, sCurrentName);

  sHandledBy := Stb$object.getCurrentNodePackage;
  
  
  if trim(sHandledBy) is not null and sHandledBy <> 'STB$GAL' then
    sMethodExists := STB$UTIL.CHECKMETHODEXISTS('destroyOldValues', sHandledBy);
    if sMethodExists = 'F' then    
      sMethodExists := stb$util.checkObjectExists('destroyOldValues', sHandledBy);
    end if;

    isr$trace.debug('metod destroyOldValues in ' || sHandledBy, 'sMethodExists = ' || sMethodExists, sCurrentName);

    if sMethodExists = 'T'  and trim(sHandledBy) is not null then
      execute immediate 'begin '||sHandledBy||'.destroyOldValues; end; ';
    end if;

  end if;

  sPackage  := '';

  -- reset the currentNode
  Stb$object.setCurrentNode(STB$TREENODE$RECORD());
  Stb$object.setReloadToken('');
  Stb$object.setReloadTreeList(STB$TREENODELIST());

  if reason is not null then

     if reason = 0 then
       isr$action$log.SetActionLog('SHUTDOWN', 'NORMAL');
     elsif reason = 1 then
       isr$action$log.SetActionLog('SHUTDOWN', 'TIMEOUT');
     elsif reason = 2 then
       isr$action$log.SetActionLog('SHUTDOWN', 'EXCEPTION');
     else
       isr$action$log.SetActionLog('SHUTDOWN', '');
     end if;

  end if;

  nSessionId := STB$SECURITY.getCurrentSession;
  deleteSessionInfo(nSessionId);

  isr$trace.stat('end', 'end', sCurrentName);
exception
  when others then
    sUserMsg := utd$msglib.getmsg ('exUnexpectedError', stb$security.getCurrentLanguage,  csP1 => sqlerrm(sqlcode));
    sImplMsg := sCurrentName || rtrim(': '||sImplMsg,': ');
    sDevMsg  := substr(dbms_utility.format_error_stack ||stb$typedef.crlf|| dbms_utility.format_error_backtrace, 1,4000);
    oErrorObj.handleError ( Stb$typedef.cnSeverityCritical, sUserMsg, sImplMsg, sDevMsg );
    raise;
end destroyOldValues;

--******************************************************************************
procedure cancelMask
is
  sCurrentName  constant varchar2(100) := $$PLSQL_UNIT||'.cancelMask';
  sUserMsg      varchar2(4000);
  sDevMsg       varchar2(4000);
  sImplMsg      varchar2(4000);
  oErrorObj     STB$OERROR$RECORD := STB$OERROR$RECORD();
  nStartTimeNum number      := DBMS_UTILITY.GET_TIME;
begin
  isr$trace.stat('begin', 'begin', sCurrentName);
  rollback;
  updateSessionWizardInfo('F');
  oErrorObj := setCurrentNode(STB$OBJECT.getCurrentNode, 'F');

  isr$trace.stat('end', 'end', sCurrentName);
exception
  when others then
    sUserMsg := utd$msglib.getmsg ('exUnexpectedError', stb$security.getCurrentLanguage,  csP1 => sqlerrm(sqlcode));
    sImplMsg := sCurrentName || rtrim(': '||sImplMsg,': ');
    sDevMsg  := substr(dbms_utility.format_error_stack ||stb$typedef.crlf|| dbms_utility.format_error_backtrace, 1,4000);
    oErrorObj.handleError ( Stb$typedef.cnSeverityCritical, sUserMsg, sImplMsg, sDevMsg );
    raise;
end cancelMask;

--******************************************************************************
procedure cancelWizzard(oCurrentTreeNode out STB$TREENODE$RECORD)
is
  sCurrentName  constant varchar2(100) := $$PLSQL_UNIT||'.cancelWizzard';
  sUserMsg      varchar2(4000);
  sDevMsg       varchar2(4000);
  sImplMsg      varchar2(4000);
  oErrorObj     STB$OERROR$RECORD := STB$OERROR$RECORD(tloMessageList => ISR$MESSAGE$LIST()); --initialize the message list too
  nStartTimeNum number      := DBMS_UTILITY.GET_TIME;
begin
  isr$trace.stat('begin', 'begin', sCurrentName);

  -- unlock all reports from this usersession
  oErrorObj := Stb$system.unlockreports('and locksession = ' || Stb$security.getcurrentsession);

  rollback;
  updateSessionWizardInfo('F');
  oErrorObj := setCurrentNode(STB$OBJECT.getCurrentNode, 'F');
  oCurrentTreeNode := STB$OBJECT.getCurrentNode;
  isr$trace.stat('end', 'end', sCurrentName);

exception
  when others then
    sUserMsg := utd$msglib.getmsg ('exUnexpectedError', stb$security.getCurrentLanguage,  csP1 => sqlerrm(sqlcode));
    sImplMsg := sCurrentName || rtrim(': '||sImplMsg,': ');
    sDevMsg  := substr(dbms_utility.format_error_stack ||stb$typedef.crlf|| dbms_utility.format_error_backtrace, 1,4000);
    oErrorObj.handleError ( Stb$typedef.cnSeverityCritical, sUserMsg, sImplMsg, sDevMsg );
    raise;
END cancelWizzard;

--******************************************************************************
function GetCurrentFormInfo( lsFormInfo          out ISR$FORMINFO$RECORD,
                             lsEntity            out STB$ENTITY$LIST,
                             nNextNavigationMask in  integer)
  return STB$OERROR$RECORD
is
  oErrorObj       STB$OERROR$RECORD := STB$OERROR$RECORD(tloMessageList => ISR$MESSAGE$LIST());
  sCurrentName    constant varchar2(100) := $$PLSQL_UNIT||'.GetCurrentFormInfo';
  sUserMsg        varchar2(4000);
  sDevMsg         varchar2(4000);
  sImplMsg        varchar2(4000);
  nStartTimeNum   number      := DBMS_UTILITY.GET_TIME;
begin
  isr$trace.stat('begin', 'nNextNavigationMask: '||nNextNavigationMask, sCurrentName);
  lsFormInfo := ISR$FORMINFO$RECORD();
  lsEntity   := STB$ENTITY$LIST();
  isr$trace.debug_all('status sSeverityCode sErrorCode', 'oErrorObj.sSeverityCode: '||oErrorObj.sSeverityCode|| ' oErrorObj.sErrorCode: '||oErrorObj.sErrorCode  , sCurrentName);
  oErrorObj := ISR$REPORTTYPE$PACKAGE.GetCurrentFormInfo(lsFormInfo,lsEntity,nNextNavigationMask,'T');
  isr$trace.debug('status nMaskNo','lsFormInfo.nMaskNo: '||lsFormInfo.nMaskNo, sCurrentName);
  isr$trace.debug_all('status sSeverityCode sErrorCode', 'oErrorObj.sSeverityCode: '||oErrorObj.sSeverityCode|| ' oErrorObj.sErrorCode: '||oErrorObj.sErrorCode  , sCurrentName);
  -- reset important settings in error object
  oErrorObj.SetSeverityCodeToWorst;
  oErrorObj.SetMessageListFromAccessor;
  --isr$trace.debug('status', 'lsEntity, see column LOGCLOB', sCurrentName, sys.anydata.convertCollection(lsEntity));
  isr$trace.stat('end', 'end', sCurrentName);
  return (oErrorObj);
exception
  when others then
    sUserMsg := utd$msglib.getmsg ('exUnexpectedError', stb$security.getCurrentLanguage,  csP1 => sqlerrm(sqlcode));
    sImplMsg := sCurrentName || rtrim(': '||sImplMsg,': ');
    sDevMsg  := substr(dbms_utility.format_error_stack ||stb$typedef.crlf|| dbms_utility.format_error_backtrace, 1,4000);
    oErrorObj.handleError ( Stb$typedef.cnSeverityCritical, sUserMsg, sImplMsg, sDevMsg );
    return (oErrorObj);
end GetCurrentFormInfo;

-- *****************************************************************************
function GetSList (sEntity            in varchar2,
                   sMasterKey         in varchar2,
                   sNavigateMode      in varchar2,
                   nextNavigationMask in  integer,
                   oSelectionList     out ISR$TLRSELECTION$LIST,
                   oFormInfoRecord    out ISR$FORMINFO$RECORD,
                   oEntityList        out STB$ENTITY$LIST)
  return STB$OERROR$RECORD
IS
  oErrorObj         STB$OERROR$RECORD := STB$OERROR$RECORD(tloMessageList => ISR$MESSAGE$LIST()); --initialize the message list too
  sErrorMessage     varchar2(4000);
  sCurrentName      constant varchar2(100) := $$PLSQL_UNIT||'.GetSList ('||sEntity||')';
  sUserMsg          varchar2(4000);
  sDevMsg           varchar2(4000);
  sImplMsg          varchar2(4000);
  nStartTimeNum     number      := DBMS_UTILITY.GET_TIME;
begin
  isr$trace.stat('begin', 'sEntity: '||sEntity||'  sMasterKey: '||sMasterKey||'  sNavigateMode: '||sNavigateMode||'  nextNavigationMask: '||nextNavigationMask, sCurrentName);

  --Initalisierung the object list
  oSelectionList := ISR$TLRSELECTION$LIST ();
  oFormInfoRecord := ISR$FORMINFO$RECORD();
  oEntityList := STB$ENTITY$LIST();

  oErrorObj := ISR$REPORTTYPE$PACKAGE.GetSList(sEntity,sMasterKey,sNavigateMode,oSelectionList,
                                               nextNavigationMask,oFormInfoRecord,oEntityList, 'T');
 isr$trace.debug('oErrorObj.sSeverityCode', oErrorObj.sSeverityCode,sCurrentName);
  if oErrorObj.sSeverityCode in (stb$typedef.cnSeverityCritical,stb$typedef.cnSeverityWarning) then
    oErrorObj.sSeverityCode := Stb$typedef.cnWizardValidateStop;
    if length(oErrorObj.sErrorMessage) < length(oErrorObj.sErrorCode) then
      sErrorMessage := oErrorObj.sErrorMessage;
      oErrorObj.sErrorMessage := oErrorObj.sErrorCode;
      oErrorObj.sErrorCode := sErrorMessage;
    end if;
  end if;
  -- reset important settings in error object
  oErrorObj.SetSeverityCodeToWorst;
  oErrorObj.SetMessageListFromAccessor;

  isr$trace.debug('parameter','oSelectionList',sCurrentName,oSelectionList);
  isr$trace.debug('parameter','oFormInforecord',sCurrentName,oFormInforecord);
  isr$trace.debug('parameter','oEntityList',sCurrentName,oEntityList);
  isr$trace.stat('end', 'end', sCurrentName);
  return oErrorObj;

exception
  when others then
    sUserMsg := utd$msglib.getmsg ('exUnexpectedError', stb$security.getCurrentLanguage,  csP1 => sqlerrm(sqlcode));
    sImplMsg := sCurrentName || rtrim(': '||sImplMsg,': ');
    sDevMsg  := substr(dbms_utility.format_error_stack ||stb$typedef.crlf|| dbms_utility.format_error_backtrace, 1,4000);
    oErrorObj.handleError ( Stb$typedef.cnSeverityCritical, sUserMsg, sImplMsg, sDevMsg );
    return (oErrorObj);
end GetSList;

--******************************************************************************
function PutSList (sEntity in  varchar2,
                   lSList  in  ISR$TLRSELECTION$LIST)
return STB$OERROR$RECORD
IS
  oErrorObj         STB$OERROR$RECORD := STB$OERROR$RECORD(tloMessageList => ISR$MESSAGE$LIST()); --initialize the message list too
  sErrorMessage     varchar2(4000);
  sCurrentName      constant varchar2(100) := $$PLSQL_UNIT||'.PutSList ('||sEntity||')';
  sUserMsg          varchar2(4000);
  sDevMsg           varchar2(4000);
  sImplMsg          varchar2(4000);
  nStartTimeNum     number      := DBMS_UTILITY.GET_TIME;
begin
  isr$trace.stat('begin', 'sEntity: '||sEntity , sCurrentName);

  oErrorObj := ISR$REPORTTYPE$PACKAGE.PutSList(sEntity, lsList);
  if oErrorObj.sSeverityCode in (stb$typedef.cnSeverityCritical,stb$typedef.cnSeverityWarning) then
    oErrorObj.sSeverityCode := Stb$typedef.cnWizardValidateStop;
    if length(oErrorObj.sErrorMessage) < length(oErrorObj.sErrorCode) then
      sErrorMessage := oErrorObj.sErrorMessage;
      oErrorObj.sErrorMessage := oErrorObj.sErrorCode;
      oErrorObj.sErrorCode := sErrorMessage;
    end if;
  end if;

  -- reset important settings in error object
  oErrorObj.SetSeverityCodeToWorst;
  oErrorObj.SetMessageListFromAccessor;

  isr$trace.stat('end', 'end', sCurrentName);
  return oErrorObj;

exception
  when others then
    sUserMsg := utd$msglib.getmsg ('exUnexpectedError', stb$security.getCurrentLanguage,  csP1 => sqlerrm(sqlcode));
    sImplMsg := sCurrentName || rtrim(': '||sImplMsg,': ');
    sDevMsg  := substr(dbms_utility.format_error_stack ||stb$typedef.crlf|| dbms_utility.format_error_backtrace, 1,4000);
    oErrorObj.handleError ( Stb$typedef.cnSeverityCritical, sUserMsg, sImplMsg, sDevMsg );
    return oErrorObj;
end PutSList;

--***************************************************************************************************
function getOutputOption(olParameter OUT STB$PROPERTY$LIST)
  return STB$OERROR$RECORD
is
--Variables
  oErrorObj         STB$OERROR$RECORD := STB$OERROR$RECORD(tloMessageList => ISR$MESSAGE$LIST()); --initialize the message list too
  sCurrentName      constant varchar2(100) := $$PLSQL_UNIT||'.getOutputOption';
  sUserMsg          varchar2(4000);
  sDevMsg           varchar2(4000);
  sImplMsg          varchar2(4000);
  nStartTimeNum     number      := DBMS_UTILITY.GET_TIME;
begin
  isr$trace.stat('begin', 'begin', sCurrentName);
  olParameter := STB$PROPERTY$LIST();
  oErrorObj := ISR$REPORTTYPE$PACKAGE.getOutputOption(olParameter);
  -- reset important settings in error object
  oErrorObj.SetSeverityCodeToWorst;
  oErrorObj.SetMessageListFromAccessor;
  isr$trace.stat('end', 'end', sCurrentName);
  return oErrorObj;
exception
  when others then
    sUserMsg := utd$msglib.getmsg ('exUnexpectedError', stb$security.getCurrentLanguage,  csP1 => sqlerrm(sqlcode));
    sImplMsg := sCurrentName || rtrim(': '||sImplMsg,': ');
    sDevMsg  := substr(dbms_utility.format_error_stack ||stb$typedef.crlf|| dbms_utility.format_error_backtrace, 1,4000);
    oErrorObj.handleError ( Stb$typedef.cnSeverityCritical, sUserMsg, sImplMsg, sDevMsg );
    return oErrorObj;
end getOutputOption;

--***************************************************************************************************
FUNCTION putOutputOption(olParameter in STB$PROPERTY$LIST)
  return STB$OERROR$RECORD
is
  sCurrentName      constant varchar2(100) := $$PLSQL_UNIT||'.putOutputOption';
  sUserMsg          varchar2(4000);
  sDevMsg           varchar2(4000);
  sImplMsg          varchar2(4000);
  oerrorobj         STB$OERROR$RECORD := STB$OERROR$RECORD(tloMessageList => ISR$MESSAGE$LIST()); --initialize the message list too
  nStartTimeNum     number      := DBMS_UTILITY.GET_TIME;
BEGIN
  isr$trace.stat('begin', 'begin',sCurrentName);
  oErrorObj := ISR$REPORTTYPE$PACKAGE.putOutputOption(olParameter);
  -- reset important settings in error object
  oErrorObj.SetSeverityCodeToWorst;
  oErrorObj.SetMessageListFromAccessor;
  isr$trace.stat('end','end', sCurrentName);
  return oErrorObj;
exception
  when others then
    sUserMsg := utd$msglib.getmsg ('exUnexpectedError', stb$security.getCurrentLanguage,  csP1 => sqlerrm(sqlcode));
    sImplMsg := sCurrentName || rtrim(': '||sImplMsg,': ');
    sDevMsg  := substr(dbms_utility.format_error_stack ||stb$typedef.crlf|| dbms_utility.format_error_backtrace, 1,4000);
    oErrorObj.handleError ( Stb$typedef.cnSeverityCritical, sUserMsg, sImplMsg, sDevMsg );
    return oErrorObj;
end putOutputOption;

--****************************************************************************************************
function setMenuAccess( sParameter   in varchar2,
                        sMenuAllowed out varchar2 )
  return STB$OERROR$RECORD
is
  oErrorObj        STB$OERROR$RECORD := STB$OERROR$RECORD(tloMessageList => ISR$MESSAGE$LIST()); --initialize the message list too
  sCurrentName     constant varchar2(100) := $$PLSQL_UNIT||'.setMenuAccess('||sParameter||')';
  sUserMsg         varchar2(4000);
  sDevMsg          varchar2(4000);
  sImplMsg         varchar2(4000);
  nStartTimeNum    number      := DBMS_UTILITY.GET_TIME;
  sCurrentNodePackage varchar2(100);
  sCurrentNodeType    varchar2(100);
  nCntActiveToken  number := 0;
  sCurrentParameter   varchar(100);

  cursor cCheckNodetype(csParameter in varchar2, csCurrentNodeType in varchar2) is
    select 'T'
      from stb$contextmenu
     where token = csParameter
       and (nodetype = csCurrentNodeType or nodetype is null);

  /*cursor cGetTokenFromCache(sCurrentParameter in varchar2, sCurrentNodePackage in varchar2) is
    select allowed
      from tmp$token$cache
     where token = sCurrentParameter
       and packagename = sCurrentNodePackage;*/

begin
  isr$trace.stat('begin', 'sParameter IN: '||sParameter, sCurrentName);


  if INSTR (sParameter, '#@#') > 0 then
    sCurrentParameter := SUBSTR (sParameter, 0, INSTR (sParameter, '#@#') - 1);
    sCurrentNodePackage := SUBSTR (sParameter, INSTR (sParameter, '#@#') + 3);
  else
    sCurrentParameter := sParameter;
  end if;

  if trim(sCurrentNodePackage) is null then
    sCurrentNodePackage := STB$OBJECT.getCurrentNodePackage;
  end if;
  isr$trace.debug('sCurrentParameter',sCurrentParameter, sCurrentName);
  isr$trace.debug('sCurrentNodePackage',sCurrentNodePackage, sCurrentName);

 /* open cGetTokenFromCache(sCurrentParameter, sCurrentNodePackage);
  fetch cGetTokenFromCache into sMenuAllowed;
  if cGetTokenFromCache%found then
    isr$trace.stat('end', 'sMenuAllowed: '||sMenuAllowed||', from cache, elapsed time: '||isr$util.getElapsedTime(nStartTimeNum), sCurrentName);
    return oErrorObj;
  end if;
  close cGetTokenFromCache;*/


  --if sMenuAllowed = 'T' then
    -- wenn der aktuelle user keine Berechtigung fuer den Token hat, muss nichts weiter geprueft werden! (NB, 18.Feb 2016
    -- check of group-rights first.
    oErrorObj :=  Stb$security.checkGroupRight(sCurrentParameter, sMenuAllowed, STB$OBJECT.getCurrentReporttypeId);
    -- now sMenuallowd is either 'T' or 'F'
  --end if;

  sCurrentNodeType    := Stb$object.getCurrentNodeType;
  isr$trace.debug('sCurrentNodeType',sCurrentNodeType, sCurrentName);

  case
    when sCurrentParameter = 'CAN_CHANGE_OWN_PASSWORD' then  -- 08.Feb.2018 [ISRC-925]
      if STB$SECURITY.isLdapUser(STB$SECURITY.getCurrentUser) = 'T' then
        sMenuAllowed := 'F';
      else
        sMenuAllowed := 'T';
      end if;

    when sMenuAllowed <> 'T' then
      -- no further actions
      null;

    when sCurrentNodePackage = 'STB$GAL' then
      -- nothing more to do, call of Stb$security.checkGroupRight is already done before
      null;

    when sCurrentParameter = 'CAN_ADD_USER_WITH_DB'   then       -- special case database user not created by isr
     sMenuAllowed := STB$UTIL.getSystemParameter('DATABASEUSER_CREATED_BY_ISR');
     if Trim(sMenuAllowed) is null or sMenuAllowed ='F' then
       isr$trace.stat('end', 'sCurrentParameter: '||sCurrentParameter||'  sMenuAllowed: '||sMenuAllowed, sCurrentName);
     end if;

    when sCurrentNodePackage is not null then
      execute immediate 'DECLARE
                           olocError STB$OERROR$RECORD;
                         BEGIN
                           olocError:='|| sCurrentNodePackage  ||'.setMenuAccess(:1, :2);
                           :3 := olocError;
                         END;'
      using in sCurrentParameter, out sMenuAllowed, out oErrorObj;
      isr$trace.debug_all('after dyn sql','after dyn sql',sCurrentName);
      -- reset important settings in error object
      oErrorObj.SetSeverityCodeToWorst;
      oErrorObj.SetMessageListFromAccessor;
    else
      isr$trace.warn('warning', 'cannot handle sCurrentNodePackage, because value is NULL', sCurrentName);
      -- reset important settings in error object
      oErrorObj.SetSeverityCodeToWorst;
      oErrorObj.SetMessageListFromAccessor;
  end case;

  /*for rGetCacheToken in (select sCurrentParameter, sCurrentNodePackage, sMenuAllowed
                           from stb$contextmenu
                          where allowCaching = 'T'
                            and token = sCurrentParameter) loop                        
    insert into tmp$token$cache
    values (sCurrentParameter, sCurrentNodePackage, sMenuAllowed);
  end loop;*/

  isr$trace.stat('end', 'sMenuAllowed: '||sMenuAllowed||',  elapsed time: '||isr$util.getElapsedTime(nStartTimeNum), sCurrentName);
  return oErrorObj;

exception
  when others then
    sUserMsg := utd$msglib.getmsg ('exUnexpectedError', stb$security.getCurrentLanguage,  csP1 => sqlerrm(sqlcode));
    sImplMsg := sCurrentName || rtrim(': '||sImplMsg,': ');
    sDevMsg  := substr(dbms_utility.format_error_stack ||stb$typedef.crlf|| dbms_utility.format_error_backtrace, 1,4000);
    oErrorObj.handleError ( Stb$typedef.cnSeverityCritical, sUserMsg, sImplMsg, sDevMsg );
    return oErrorObj;
end setMenuAccess;

--************************************************************************************************************
function getSaveDialog(olSaveList OUT STB$TREENODELIST)
  return STB$OERROR$RECORD
is
  sCurrentName     constant varchar2(100) := $$PLSQL_UNIT||'.getSaveDialog';
  sUserMsg         varchar2(4000);
  sDevMsg          varchar2(4000);
  sImplMsg         varchar2(4000);
  oErrorObj        STB$OERROR$RECORD := STB$OERROR$RECORD(tloMessageList => ISR$MESSAGE$LIST()); --initialize the message list too
  nStartTimeNum    number      := DBMS_UTILITY.GET_TIME;
begin
  isr$trace.stat('begin', 'begin', sCurrentName);
  olSaveList := STB$TREENODELIST();

  oErrorObj :=  ISR$REPORTTYPE$PACKAGE.getSaveDialog(olSaveList);
  -- reset important settings in error object
  oErrorObj.SetSeverityCodeToWorst;
  oErrorObj.SetMessageListFromAccessor;

  isr$trace.stat('end', 'end', sCurrentName);
  return oErrorObj;
exception
  when others then
    sUserMsg := utd$msglib.getmsg ('exUnexpectedError', stb$security.getCurrentLanguage,  csP1 => sqlerrm(sqlcode));
    sImplMsg := sCurrentName || rtrim(': '||sImplMsg,': ');
    sDevMsg  := substr(dbms_utility.format_error_stack ||stb$typedef.crlf|| dbms_utility.format_error_backtrace, 1,4000);
    oErrorObj.handleError ( Stb$typedef.cnSeverityCritical, sUserMsg, sImplMsg, sDevMsg );
    return oErrorObj;
end getSaveDialog;

--****************************************************************************************************
function putSaveDialog(olSaveList in STB$PROPERTY$LIST)
  return STB$OERROR$RECORD
is
  exAudit          exception;
  sCurrentName     constant varchar2(100) := $$PLSQL_UNIT||'.putSaveDialog';
  sUserMsg         varchar2(4000);
  sDevMsg          varchar2(4000);
  sImplMsg         varchar2(4000);
  oErrorObj        STB$OERROR$RECORD := STB$OERROR$RECORD(tloMessageList => ISR$MESSAGE$LIST()); --initialize the message list too
  oErrorObj1       STB$OERROR$RECORD := STB$OERROR$RECORD();
  sAuditFlag       varchar2(1):='F';
  oMenuRunReport   STB$MENUENTRY$RECORD;
  sNodePackage     varchar2(200);
  exCallFunction   exception;

  -- TR, 201602091654, fuer die unsichtbare "CAN_RUN_REPORT" maske
  olnodelist      STB$TREENODELIST := STB$TREENODELIST();

begin
  isr$trace.stat('begin', 'end', sCurrentName);

  sPackage := 'ISR$REPORTTYPE$PACKAGE'; -- NVL(substr(STB$UTIL.getReptypeParameter (STB$OBJECT.getCurrentReporttypeId, 'PUTSAVEDIALOG'), 0, instr(STB$UTIL.getReptypeParameter (STB$OBJECT.getCurrentReporttypeId, 'PUTSAVEDIALOG'), '.')-1), 'ISR$REPORTTYPE$PACKAGE')
  isr$trace.info('sPackage set', sPackage, sCurrentName);

  oErrorObj :=  ISR$REPORTTYPE$PACKAGE.putSaveDialog(olSaveList);
  isr$trace.debug('after save-as-dialog','after save-as-dialog',sCurrentName);

  if oErrorObj.sSeverityCode  <> Stb$typedef.cnNoError then
    return oErrorObj;
  end if;

  --======CHECK AUDIT REQUIRED======
  oErrorObj := ISR$REPORTTYPE$PACKAGE.checkAuditRequired(sAuditFlag);
  isr$trace.debug('status sAuditFlag','sAuditFlag: '||sAuditFlag, sCurrentName);

  --======AUDIT REQUIRED======
  if sAuditFlag ='T' then
    -- check if prompted audit is required
    --======PROMTED AUDIT======
    if Stb$util.checkPromptedAuditRequired('PROMPTED_REPORTAUDIT') = 'T' then
      if NVL(STB$UTIL.getProperty(olSaveList, 'CREATE_DOC'), 'F') = 'T' then
        sCreateTheDoc := 'T';
      end if;
      raise exAudit;
      -- commit will be in the audit window
    --======Silent Audit======
    else
      -- write the silent audit and calculate the checksum
      oErrorObj := Stb$audit.silentAudit;
      -- commit the audit
      commit;
    end if;
  else
    -- no audit required commit the data
    isr$trace.debug('no audit required','sAuditFlag: '||sAuditFlag, sCurrentName);
    commit;
  end if;
  isr$trace.debug('after commit', 'commit', sCurrentName);

  updateSessionWizardInfo('F');

  if NVL(STB$UTIL.getProperty(olSaveList, 'CREATE_DOC'), 'F') = 'T' then
    if sAuditFlag = 'F' or Stb$util.checkPromptedAuditRequired('PROMPTED_REPORTAUDIT') = 'F' and sAuditFlag = 'T' then

      sNodePackage := nvl(Stb$object.getCurrentNodePackage,'ISR$REPORTTYPE$PACKAGE');
      isr$trace.debug('sNodePackage', sNodePackage, sCurrentName);
      isr$trace.debug('stb$object.getCurrentToken', stb$object.getCurrentToken, sCurrentName);
      oMenuRunReport := STB$MENUENTRY$RECORD('TOKEN','CAN_RUN_REPORT', 'CAN_RUN_REPORT',sNodePackage,'','F');

      begin
        execute immediate
            'DECLARE
               olocError STB$OERROR$RECORD;
             BEGIN
               olocError:='||  sNodePackage  ||'.callFunction(:1, :2);
               :4 := olocError;
             END;'
             Using in oMenuRunReport, out olNodeList,  out oErrorObj1;
      exception
        when others then
        sUserMsg := '''callFunction''-action can''t be executed because calling the function from package ''' || sNodePackage || ''' is impossible ('||sqlerrm||')';
        raise exCallFunction;
      end;

      begin
        execute immediate
            'DECLARE
               olocError STB$OERROR$RECORD;
             BEGIN
               olocError:='||  sNodePackage  ||'.putParameters(:1, :2);
               :4 := olocError;
             END;'
             Using in oMenuRunReport, in olnodelist(1).tloPropertyList,  out oErrorObj1;
      exception
        when others then
        sUserMsg := '''putParameters'' - action can''t be executed because calling the function from package ''' || sNodePackage || ''' is impossible ('||sqlerrm||')';
        raise exCallFunction;
      end;
     -- oErrorObj1 := ISR$REPORTTYPE$PACKAGE.callFunction(oMenuRunReport, olnodelist);
     -- oErrorObj1 := ISR$REPORTTYPE$PACKAGE.putParameters(oMenuRunReport, olnodelist(1).tloPropertyList);
    end if;
  end if;

  -- reset important settings in error object
  -- 14.Apr.2016 (vs): Achtung Reihenfolge der naechsten beiden Zeilen macht beim alten ErrorObject-Handling den Worst Code kaputt !
  oErrorObj.SetSeverityCodeToWorst;
  oErrorObj.SetMessageListFromAccessor;

  isr$trace.stat('end', 'end', sCurrentName);
  return oErrorObj;

exception
  when exAudit then
    updateSessionWizardInfo('F');
    sUserMsg := 'Call the mask for the audit window.' || CHR(10) || sqlerrm(sqlcode);
    sImplMsg := sCurrentName || rtrim(': '||sImplMsg,': ');
    sDevMsg  := substr(dbms_utility.format_error_stack ||stb$typedef.crlf|| dbms_utility.format_error_backtrace, 1,4000);
    oErrorObj.handleError ( Stb$typedef.cnAudit, sUserMsg, sImplMsg, sDevMsg );
    return oErrorObj;
  when exCallFunction then
    sImplMsg := sCurrentName || rtrim(': '||sImplMsg,': ');
    sDevMsg  := substr(dbms_utility.format_error_stack ||stb$typedef.crlf|| dbms_utility.format_error_backtrace, 1,4000);
    oErrorObj.handleError ( Stb$typedef.cnSeverityCritical, sUserMsg, sImplMsg, sDevMsg );
    return oErrorObj;
  when others then
    sUserMsg := utd$msglib.getmsg ('exUnexpectedError', stb$security.getCurrentLanguage,  csP1 => sqlerrm(sqlcode));
    sImplMsg := sCurrentName || rtrim(': '||sImplMsg,': ');
    sDevMsg  := substr(dbms_utility.format_error_stack ||stb$typedef.crlf|| dbms_utility.format_error_backtrace, 1,4000);
    oErrorObj.handleError ( Stb$typedef.cnSeverityCritical, sUserMsg, sImplMsg, sDevMsg );
    return oErrorObj;
end putSaveDialog;

-- ********************************************************************************************************
function getGridSelection (sEntity       in  varchar2,
                           sNavigateMode in  varchar2,
                           lSList        out ISR$TLRSELECTION$LIST)
  return STB$OERROR$RECORD
IS
  sCurrentName     constant varchar2(100) := $$PLSQL_UNIT||'.getGridSelection ('||sEntity||')';
  sUserMsg         varchar2(4000);
  sDevMsg          varchar2(4000);
  sImplMsg         varchar2(4000);
  oErrorObj        STB$OERROR$RECORD := STB$OERROR$RECORD(tloMessageList => ISR$MESSAGE$LIST()); --initialize the message list too
  nStartTimeNum    number      := DBMS_UTILITY.GET_TIME;
begin
  isr$trace.stat('begin', 'sEntity: '||sEntity||'  sNavigateMode: '||sNavigateMode, sCurrentName);
  --Initalisierung der Objektliste
  lsList := ISR$TLRSELECTION$LIST ();

  oErrorObj := ISR$REPORTTYPE$PACKAGE.GetGridSelection(sEntity,sNavigateMode,lsList);

  -- reset important settings in error object
  oErrorObj.SetSeverityCodeToWorst;
  oErrorObj.SetMessageListFromAccessor;

  isr$trace.debug('oErrorObj.sseverityCode',oErrorObj.sseverityCode, sCurrentName);
  if oErrorObj.sseverityCode = Stb$typedef.cnSeverityCritical then
      stb$object.destroyoldvalues;
  end if;

  isr$trace.stat('end', 'end', sCurrentName);
  return oErrorObj;

exception
  when others then
    sUserMsg := utd$msglib.getmsg ('exUnexpectedError', stb$security.getCurrentLanguage,  csP1 => sqlerrm(sqlcode));
    sImplMsg := sCurrentName || rtrim(': '||sImplMsg,': ');
    sDevMsg  := substr(dbms_utility.format_error_stack ||stb$typedef.crlf|| dbms_utility.format_error_backtrace, 1,4000);
    oErrorObj.handleError ( Stb$typedef.cnSeverityCritical, sUserMsg, sImplMsg, sDevMsg );
    return oErrorObj;
end getGridSelection;

--**********************************************************************************************************************************
function getGridDependency(csEntity in varchar2 , lsEntity out STB$ENTITY$LIST, csKey in varchar2)
  return STB$OERROR$RECORD
is
  sCurrentName     constant varchar2(100) := $$PLSQL_UNIT||'.getGridDependency('||csEntity||')';
  sUserMsg         varchar2(4000);
  sDevMsg          varchar2(4000);
  sImplMsg         varchar2(4000);
  oErrorObj        STB$OERROR$RECORD := STB$OERROR$RECORD(tloMessageList => ISR$MESSAGE$LIST()); --initialize the message list too
  nStartTimeNum    number      := DBMS_UTILITY.GET_TIME;
begin
  isr$trace.stat('begin', 'csEntity: '||csEntity||'  csKey: '||csKey, sCurrentName);

  lsEntity := STB$ENTITY$LIST();
  oErrorObj := ISR$REPORTTYPE$PACKAGE.GetGridDependency(csEntity,lsEntity,csKey);

  -- reset important settings in error object
  oErrorObj.SetSeverityCodeToWorst;
  oErrorObj.SetMessageListFromAccessor;

  isr$trace.stat('end', 'end', sCurrentName);
  return oErrorObj;

exception
  when others then
    sUserMsg := utd$msglib.getmsg ('exUnexpectedError', stb$security.getCurrentLanguage,  csP1 => sqlerrm(sqlcode));
    sImplMsg := sCurrentName || rtrim(': '||sImplMsg,': ');
    sDevMsg  := substr(dbms_utility.format_error_stack ||stb$typedef.crlf|| dbms_utility.format_error_backtrace, 1,4000);
    oErrorObj.handleError ( Stb$typedef.cnSeverityCritical, sUserMsg, sImplMsg, sDevMsg );
    return oErrorObj;
end getGridDependency;

--**********************************************************************************************************************************
function getTimeOut(nTimeOutInMinutes out number)
  return STB$OERROR$RECORD
is
  sCurrentName     constant varchar2(100) := $$PLSQL_UNIT||'.getTimeOut';
  sUserMsg         varchar2(4000);
  sDevMsg          varchar2(4000);
  sImplMsg         varchar2(4000);
  oErrorObj        STB$OERROR$RECORD := STB$OERROR$RECORD(tloMessageList => ISR$MESSAGE$LIST()); --initialize the message list too
  nStartTimeNum    number      := DBMS_UTILITY.GET_TIME;
begin
  isr$trace.stat('begin', 'nTimeOutInMinutes: '||nTimeOutInMinutes, sCurrentName);
  oErrorObj := Stb$security.getTimeOut(nTimeOutInMinutes);
  isr$trace.debug('status out nTimeOutInMinutes','nTimeOutInMinutes : ' || nTimeOutInMinutes, sCurrentName);
  -- reset important settings in error object
  oErrorObj.SetSeverityCodeToWorst;
  oErrorObj.SetMessageListFromAccessor;
  isr$trace.stat('end', 'end', sCurrentName);
  return oErrorObj;
exception
  when others then
    sUserMsg := utd$msglib.getmsg ('exUnexpectedError', stb$security.getCurrentLanguage,  csP1 => sqlerrm(sqlcode));
    sImplMsg := sCurrentName || rtrim(': '||sImplMsg,': ');
    sDevMsg  := substr(dbms_utility.format_error_stack ||stb$typedef.crlf|| dbms_utility.format_error_backtrace, 1,4000);
    oErrorObj.handleError ( Stb$typedef.cnSeverityCritical, sUserMsg, sImplMsg, sDevMsg );
    return oErrorObj;
end getTimeOut;

--*********************************************************************************************************************************
function initList (sEntity IN  VARCHAR2, lSList OUT ISR$TLRSELECTION$LIST)
  return STB$OERROR$RECORD
is
  sCurrentName     constant varchar2(100) := $$PLSQL_UNIT||'.initList('||sEntity||')';
  sUserMsg         varchar2(4000);
  sDevMsg          varchar2(4000);
  sImplMsg         varchar2(4000);
  oErrorObj           STB$OERROR$RECORD := STB$OERROR$RECORD(tloMessageList => ISR$MESSAGE$LIST()); --initialize the message list too
  nStartTimeNum    number      := DBMS_UTILITY.GET_TIME;
begin
  isr$trace.stat('begin', 'sEntity: '||sEntity, sCurrentName);
  lSList := ISR$TLRSELECTION$LIST();

  --call the package of the current node
  execute immediate
    'DECLARE
       olocError STB$OERROR$RECORD;
     BEGIN
       olocError :='|| STB$OBJECT.getCurrentNodePackage ||'.initList(:1, :2);
       :3 :=  olocError;
     END;'
    Using IN sEntity,OUT lSList, OUT oErrorObj;

  -- reset important settings in error object
  oErrorObj.SetSeverityCodeToWorst;
  oErrorObj.SetMessageListFromAccessor;

  isr$trace.stat('end', 'end', sCurrentName);
  return(oErrorObj);
exception
when others then
    sUserMsg := utd$msglib.getmsg ('exUnexpectedError', stb$security.getCurrentLanguage,  csP1 => sqlerrm(sqlcode));
    sImplMsg := sCurrentName || rtrim(': '||sImplMsg,': ');
    sDevMsg  := substr(dbms_utility.format_error_stack ||stb$typedef.crlf|| dbms_utility.format_error_backtrace, 1,4000);
    oErrorObj.handleError ( Stb$typedef.cnSeverityCritical, sUserMsg, sImplMsg, sDevMsg );
  return(oErrorObj);
end initList;

--**********************************************************************************************************************************
function getLov( sEntity        in  varchar2,
                 sSuchwert      in  varchar2,
                 oSelectionList out ISR$TLRSELECTION$LIST)
  return STB$OERROR$RECORD
is
  sCurrentName     constant varchar2(100) := $$PLSQL_UNIT||'.getLov('||sEntity||')';
  sUserMsg         varchar2(4000);
  sDevMsg          varchar2(4000);
  sImplMsg         varchar2(4000);
  oErrorObj        STB$OERROR$RECORD := STB$OERROR$RECORD(tloMessageList => ISR$MESSAGE$LIST()); --initialize the message list too
  nStartTimeNum    number      := DBMS_UTILITY.GET_TIME;
begin
  isr$trace.stat('begin', 'sEntity: '||sEntity||'  sSuchwert: '||sSuchwert, sCurrentName);
  oSelectionList := ISR$TLRSELECTION$LIST();

  sPackage := NVL(TRIM(sPackage), 'ISR$REPORTTYPE$PACKAGE');
  isr$trace.info('sPackage set', sPackage, sCurrentName);

  isr$trace.debug('parameter sSuchwert sPackage', 'sSuchwert: ' || sSuchwert|| '  sPackage: ' || sPackage, sCurrentName);

  execute immediate
      'BEGIN
         :1:='||sPackage||'.getLov(:2, :3, :4);
       END;'
       Using OUT oErrorObj, IN sEntity, IN sSuchwert, OUT oSelectionList;

  -- reset important settings in error object
  oErrorObj.SetSeverityCodeToWorst;
  oErrorObj.SetMessageListFromAccessor;

  isr$trace.debug('value','oSelectionList.count: '||oSelectionList.count(),sCurrentName,oSelectionList);
  isr$trace.stat('end', 'end',sCurrentName);
  return(oErrorObj);
exception
  when others then
    sUserMsg := utd$msglib.getmsg ('exUnexpectedError', stb$security.getCurrentLanguage,  'sPackage='|| sPackage, sqlerrm(sqlcode));
    sImplMsg := sCurrentName || rtrim(': '||sImplMsg,': ');
    sDevMsg  := substr(dbms_utility.format_error_stack ||stb$typedef.crlf|| dbms_utility.format_error_backtrace, 1,4000);
    oErrorObj.handleError ( Stb$typedef.cnSeverityCritical, sUserMsg, sImplMsg, sDevMsg );
    return(oErrorObj);
end getLov;

--**********************************************************************************************************************************
function callFunction( oParameter in  STB$MENUENTRY$RECORD,
                       olNodeList out STB$TREENODELIST)
  return STB$OERROR$RECORD
is
  sCurrentName          constant varchar2(100) := $$PLSQL_UNIT||'.callFunction('||oParameter.sToken ||')';
  nStartTimeNum         number      := DBMS_UTILITY.GET_TIME;
  sUserMsg              varchar2(4000);
  sDevMsg               varchar2(4000);
  sImplMsg              varchar2(4000);
  oErrorObj             STB$OERROR$RECORD := STB$OERROR$RECORD(tloMessageList => ISR$MESSAGE$LIST()); --initialize the message list too
  oErrorObj1            STB$OERROR$RECORD := STB$OERROR$RECORD();
  exCannotPerformAction exception;
  exUserAreEqual        exception;
  sParameterCnt         number := 0;
  sCheckForDialog       varchar2(1);
  exCallFunction        exception;
  sCurrentNodeType      varchar2(100);
  oParamListRec         ISR$PARAMLIST$REC := ISR$PARAMLIST$REC();


  cursor cGetEsigUser is
    select oracleusername
      from stb$esig
     where entryid = (select max (entryid)
                        from stb$esig
                       where nodetype = stb$object.getCurrentNodetype
                         and (reference = stb$object.getCurrentNodeid
                           or title = stb$object.getCurrentTitle)
                         and NVL(success, 'F') = 'T'
                         and action != oParameter.sToken
                         and action not like '%DEACTIVATE%'
                         and action != 'CAN_UNLOCK_REPORT');

  sEsigUser             varchar2(30);
begin
  isr$trace.stat('begin', 'begin', sCurrentName);
  isr$trace.debug('parameter', 'oParameter', sCurrentName, oParameter);
  sCreateTheDoc := 'F';
  -- (NB) Benutzer war aktiv, vermerken
  -- (IS) Bitte nicht, das sorgt fuer deadlock error
--  isr$trace.debug('vor update isr$session', STB$SECURITY.getCurrentSession , sCurrentName);
 -- update isr$session set modifiedon = sysdate where sessionid = STB$SECURITY.getCurrentSession;

  ISR$TREE$PACKAGE.selectCurrent('F');
  olNodeList := STB$TREENODELIST();

  -- 18. Nov 2015, MCD : this check should prevent from access this function directly without context
  -- do not use in putParameters because call function can already change the context in a way that the function return false
  if ISR$RIBBON.isParameterAllowed(oParameter.sToken, stb$object.getCurrentNodetype) != 'T' then
    raise exCannotPerformAction;
  end if;

  isr$trace.debug('value','STB$OBJECT.getCurrentNode in CLOB',sCurrentName, STB$OBJECT.getCurrentNode);

  -- get the package that handles the request
  if Trim(oParameter.sHandledBy) is null then
    sCurrentNodeType := Stb$object.getCurrentNodeType;
    sPackage := nvl(STB$OBJECT.getCurrentNodePackage(sCurrentNodeType,oParameter.sToken),STB$OBJECT.getCurrentNodePackage);
    isr$trace.debug('value','sPackage: '||sPackage||',  sCurrentNodeType: '||sCurrentNodeType, sCurrentName);
  else
    sPackage := oParameter.sHandledBy;
  end if;
  isr$trace.info('sPackage set', sPackage, sCurrentName);

  if oParameter.sConfirm = 'E'
  and STB$UTIL.getSystemParameter('CHANGE_USER_BETWEEN_ESIG_STATES') = 'T'
  and oParameter.sToken not like '%DEACTIVATE%'
  and oParameter.sToken != 'CAN_UNLOCK_REPORT' then
    open cGetEsigUser;
    fetch cGetEsigUser into sEsigUser;
    if sEsigUser = sOracleUserName then
      sUserMsg := Utd$msglib.GetMsg('exUserAreEqualText',Stb$security.GetCurrentLanguage);
      raise exUserAreEqual;
    end if;
    close cGetEsigUser;
  end if;

  -- call of the package which handles the request
  isr$trace.debug('status','before call of '||sPackage||'.callFunction', sCurrentName);
  begin
    execute immediate
        'DECLARE
           olocError STB$OERROR$RECORD;
         BEGIN
           olocError:='||  sPackage  ||'.callFunction(:1, :2);
           :4 := olocError;
         END;'
         Using in oParameter, out olNodeList,  out oErrorObj;
  exception
     /*
        (NB), 2016.07.22 ISRC-545
        exNoTokenHandle muss in CallFunction so behandlet werden, das es hier ankommt:
        -> weiter unten werden alle Token behandelt die nicht bahandelt wurden
    */
    --when stb$typedef.exNoTokenHandle then
    --  sCheckForDialog := 'T';
    when others then
    sUserMsg := 'The ''' ||  Utd$msglib.GetMsg( oParameter.sToken || '$DESC',Stb$security.GetCurrentLanguage) ||  ''' - action can''t be executed because calling the function from package ''' || sPackage || ''' is impossible ('||sqlerrm||')';
    raise exCallFunction;
  end;

  if oErrorObj.sSeverityCode = Stb$typedef.cnSeverityCritical then
    raise exCallFunction;
  end if;

  if oErrorObj.sSeverityCode in (Stb$typedef.cnFirstForm)
      or upper (oparameter.stoken) in ('CAN_MODIFY_OUTPUTOPTIONS'/*,'CAN_RELEASE_FOR_INSPECTION','CAN_FINALIZE_REPORT'*/) then
    updateSessionWizardInfo('T');
  end if;

  if oParameter.sConfirm = 'E' and oErrorObj.sSeverityCode = STB$TYPEDEF.cnNoError then

    if olNodeList.exists(1) and olNodeList(1).sNodeid = 'MASK' then

      begin
        select count(*)
          into sParameterCnt
          from table(olNodeList (1).tloPropertylist)
         where NVL(sDisplayed, 'T') = 'T';
      exception when others then
        sParameterCnt := 0;
      end;

      isr$trace.debug('sParameterCnt',sParameterCnt,sCurrentName);

      if NVL(sParameterCnt,0) != 0 then
        olNodeList(1).tloPropertyList.extend();
        for i in 2..olNodeList(1).tloPropertyList.count() loop
          olNodeList(1).tloPropertyList(olNodeList(1).tloPropertyList.count()-i+2) := olNodeList(1).tloPropertyList(olNodeList(1).tloPropertyList.count()-i+1);
        end loop;
        olNodeList(1).tloPropertyList (1) := STB$PROPERTY$RECORD(oParameter.sToken, Utd$msglib.GetMsg (oParameter.sToken||'$MASK', Stb$security.GetCurrentLanguage),
                                                                 null, null, null, null, 'T', null, 'TABSHEET');
      end if;
    end if;
    oErrorObj := STB$SECURITY.getEsigmask(olNodeList, oParameter.sToken);
    if oParameter.sToken in ('CAN_RELEASE_FOR_INSPECTION', 'CAN_FINALIZE_REPORT', 'CAN_FINALIZE_EXCEL_REPORT') then
      olNodeList(1).tloPropertyList.EXTEND;
      olNodeList(1).tloPropertyList (olNodeList(1).tloPropertyList.count) := STB$PROPERTY$RECORD('REPID',
                                                                 utd$msglib.getmsg ('REPID', stb$security.getcurrentlanguage),
                                                                 STB$OBJECT.getCurrentRepid, STB$OBJECT.getCurrentRepid,
                                                                 'F', 'T', 'F', 'F', 'INTEGER');
    end if;
    olNodeList (1).nDisplayType := STB$TYPEDEF.cnEsigDialog;
  end if;

 isr$trace.debug('before email', 'oErrorObj', sCurrentName, oErrorObj);
  --------------------------
  if oErrorObj.sSeverityCode in (Stb$typedef.cnNoError, Stb$typedef.cnAudit, Stb$typedef.cnFirstForm, Stb$typedef.cnMsgWarningTimeout) then
    if STB$UTIL.getSystemParameter('NOTIFY') = 'T' then
      oParamListRec.AddParam('CURRENTNODETYPE', STB$OBJECT.getCurrentNodeType);
      oParamListRec.AddParam('CURRENTNODEID', STB$OBJECT.getCurrentNodeId);
      oParamListRec.AddParam('NOTIFY_ON', 'BEGIN');

      oErrorObj1 := ISR$NOTIFY$PACKAGE.SENDMAIL(oparameter.stoken, oParamListRec);
    end if;
  end if;
  ---------------------------

  /*
    (NB), 2016.07.22

    Ab hier werden alle Tokens verarbeitet die NICHT in den jeweiligen "CallFunction" bearbeitet wurden!!

    RISIKO - es kann passieren das wir auf diese Art "vergessenen" Code nicht mitbekommen
    Ziel fuer spaeter:
    - in diesem Teil explizit alle Tokens aufzaehlen die "bestaetigt" keinen eigenen Handler haben (oder besser alle Tokens da wo sie hingehoeren behandeln)
    - in den Fuellen die nicht "bestaetigt" sind, eine Message in's LOG schreiben (erzwingen, d.h. als isr$log.Error fuer interne Test, isr$log.Warning fuer's release)

  */
  if olNodeList.exists(1) = false then
    --sCheckForDialog := 'T';
    --oErrorObj := STB$OERROR$RECORD(tloMessageList => ISR$MESSAGE$LIST());
    if oErrorObj.sErrorCode = Utd$msglib.GetMsg('exNotExists',Stb$security.GetCurrentLanguage) then
      sCheckForDialog := 'T';
    end if;

    if nvl(sCheckForDialog, 'F') = 'F' then
      begin
        execute immediate
          'DECLARE
             intCheckForDialog varchar2(1);
           BEGIN
             intCheckForDialog:='||  sPackage  ||'.getCheckForDialog();
             :1 := intCheckForDialog;
           END;'
           Using   out sCheckForDialog;
        isr$trace.debug('sCheckForDialog','sCheckForDialog: '||sCheckForDialog,sCurrentName);
      exception when others then
        isr$trace.warn('sCheckForDialog','Procedure getCheckForDialog does not exist in package '||sPackage,sCurrentName);  -- vs: [ISRC-650]
      end;
    else  -- ELSE clause resurrected (vs 14.Aug.2016)
       sCheckForDialog := 'F';
    end if;
    isr$trace.debug('after dyn getCheckForDialog', sCheckForDialog, sCurrentName, oErrorObj);

    if nvl(sCheckForDialog, 'F') = 'T'  then
      olNodeList := STB$TREENODELIST ();
      olNodeList.extend;
      olNodeList (1) := STB$TREENODE$RECORD(STB$UTIL.getMaskTitle(Upper (oParameter.sToken)),
                                            null, 'TRANSPORT', 'MASK');
      oErrorObj := STB$UTIL.getPropertyList(Upper (oParameter.sToken), olNodeList (1).tloPropertylist);
    end if;

  elsif olNodeList(1).sNodeid = 'MASK' and olNodeList(1).clPropertyList is null then
    begin
      select count(*)
        into sParameterCnt
        from table(olNodeList (1).tloPropertylist)
       where NVL(sDisplayed, 'T') = 'T';
    exception when others then
      sParameterCnt := 0;
    end;

    -- check if the function is implemented in putParameters
    if sParameterCnt = 0 and oParameter.sConfirm != 'E' then
      isr$trace.info('Dialog does not exist','dialog',sCurrentName);
      oErrorObj := putParameters(olNodeList (1).tloPropertylist, oParameter);
    end if;
  end if;

  if olNodeList.exists(1) and olNodeList(1).sNodeid = 'MASK' and olNodeList(1).tloPropertylist is not null then

    for i in 1..olNodeList(1).tloPropertylist.count() loop
      if olNodeList(1).tloPropertylist(i).type in ('TABSHEET', 'GROUP') then
        -- if the last position is tab/group, delete it
        if i = olNodeList(1).tloPropertylist.count() then
          olNodeList(1).tloPropertylist(i).type := 'DELETE';
        end if;
        -- if the next visible is tab/group, delete it: exception if group follows tab
        if i > 1  and i < olNodeList(1).tloPropertylist.count() then
          for j in i+1..olNodeList(1).tloPropertylist.count() loop
            if olNodeList(1).tloPropertylist(j).type in ('TABSHEET', 'GROUP')
            and (olNodeList(1).tloPropertylist(i).type = olNodeList(1).tloPropertylist(j).type
                or olNodeList(1).tloPropertylist(i).type = 'GROUP' and olNodeList(1).tloPropertylist(j).type = 'TABSHEET') then
              olNodeList(1).tloPropertylist(i).type := 'DELETE';
            end if;
            if olNodeList(1).tloPropertylist(j).sDisplayed = 'T'
            and (olNodeList(1).tloPropertylist(j).type not in ('TABSHEET', 'GROUP') OR olNodeList(1).tloPropertylist(i).type != olNodeList(1).tloPropertylist(j).type) then
              exit;
            end if;
          end loop;
        end if;
      end if;
    end loop;

    for i in 1..olNodeList(1).tloPropertylist.count() loop
      IF olNodeList(1).tloPropertylist(i).type in ('DELETE') then
        olNodeList(1).tloPropertylist.delete(i);
      end if;
    end loop;

  end if;


  -- reset important settings in error object
  oErrorObj.SetSeverityCodeToWorst;
  oErrorObj.SetMessageListFromAccessor;

  isr$trace.debug('status', 'olNodeList OUT', sCurrentName, olNodeList);
  isr$trace.debug('status', 'oErrorObj', sCurrentName, oErrorObj);

  isr$trace.stat('end', 'elapsed time: '|| isr$util.getElapsedTime(nStartTimeNum), sCurrentName);
  return(oErrorObj);

exception
  when exUserAreEqual then
    sUserMsg := utd$msglib.getmsg ('exUserAreEqualText', stb$security.getCurrentLanguage,  csP1 => sqlerrm(sqlcode));
    sImplMsg := sCurrentName || rtrim(': '||sImplMsg,': ');
    sDevMsg  := substr(dbms_utility.format_error_stack ||stb$typedef.crlf|| dbms_utility.format_error_backtrace, 1,4000);
    oErrorObj.handleError ( Stb$typedef.cnSeverityMessage, sUserMsg, sImplMsg, sDevMsg );
    return oErrorObj;
  when exCannotPerformAction then
    sUserMsg := utd$msglib.getmsg ('exCannotPerformAction', stb$security.getCurrentLanguage,  csP1 => sqlerrm(sqlcode));
    sImplMsg := sCurrentName || rtrim(': '||sImplMsg,': ');
    sDevMsg  := 'User defined exception raised: exCannotPerformAction'||crlf||substr(dbms_utility.format_error_backtrace, 1,4000);
    oErrorObj.handleError( Stb$typedef.cnSeverityWarning, sUserMsg, sImplMsg, sDevMsg );
    return oErrorObj;
  when others then
    rollback;
    sUserMsg := sUserMsg || Stb$typedef.cr || utd$msglib.getmsg ('exUnexpectedError', stb$security.getCurrentLanguage,  csP1 => sqlerrm(sqlcode));
    sImplMsg := sCurrentName || rtrim(': '||sImplMsg,': ');
    sDevMsg  := substr(dbms_utility.format_error_stack ||stb$typedef.crlf|| dbms_utility.format_error_backtrace, 1,4000);
    oErrorObj.handleError ( Stb$typedef.cnSeverityCritical, sUserMsg, sImplMsg, sDevMsg );
    return oErrorObj;
end callFunction;

--**********************************************************************************************************************************
function checkDependenciesOnMask( oParameter    in     STB$MENUENTRY$RECORD,
                                  olParameter   in out STB$PROPERTY$LIST,
                                  sModifiedFlag out    varchar2 )
  return STB$OERROR$RECORD
is
  sCurrentName        constant varchar2(100) := $$PLSQL_UNIT||'.checkDependenciesOnMask (' || oParameter.sToken  || ')';
  sUserMsg            varchar2(4000);
  sDevMsg             varchar2(4000);
  sImplMsg            varchar2(4000);
  oErrorObj           STB$OERROR$RECORD := STB$OERROR$RECORD(tloMessageList => ISR$MESSAGE$LIST()); --initialize the message list too
  sHandlePackage      varchar2(100);
  nStartTimeNum       number      := DBMS_UTILITY.GET_TIME;

  cursor cGetPackage(csPackage in varchar2) is
    select distinct package_name
    from            user_arguments
    where           object_name = Upper ('checkDependenciesOnMask')
    and             package_name = csPackage;

begin
  isr$trace.stat('begin', 'begin', sCurrentName);
  isr$trace.debug('status', 'oParameter IN', sCurrentName, oParameter);
  --isr$trace.debug('status', 'olParameter IN OUT', sCurrentName, sys.anydata.convertCollection(olParameter));

  sModifiedFlag := 'F';

  open cGetPackage(Nvl(Trim(oParameter.sHandledBy), STB$UTIL.getCurrentCustomPackage));
  fetch cGetPackage into sHandlePackage;
  isr$trace.debug('status sHandlePackage','sHandlePackage: '||sHandlePackage,sCurrentName);
  if cGetPackage%FOUND then
    -- call of the package which handels the request
    execute immediate
        'DECLARE
           olocError STB$OERROR$RECORD;
         BEGIN
           olocError:='|| sHandlePackage ||'.checkDependenciesOnMask(:1, :2, :3);
           :4 := olocError;
         END;'
    Using in oParameter, in out olParameter, in out sModifiedFlag, out oErrorObj;
  end if;
  close cGetPackage;

  -- reset important settings in error object
  oErrorObj.SetSeverityCodeToWorst;
  oErrorObj.SetMessageListFromAccessor;

  isr$trace.stat('end', 'sModifiedFlag: '||sModifiedFlag, sCurrentName);
  return(oErrorObj);

exception
  when others then
    sUserMsg := utd$msglib.getmsg ('exUnexpectedError', stb$security.getCurrentLanguage,  csP1 => sqlerrm(sqlcode));
    sImplMsg := sCurrentName || rtrim(': '||sImplMsg,': ');
    sDevMsg  := substr(dbms_utility.format_error_stack ||stb$typedef.crlf|| dbms_utility.format_error_backtrace, 1,4000);
    oErrorObj.handleError ( Stb$typedef.cnSeverityCritical, sUserMsg, sImplMsg, sDevMsg );
    return oErrorObj;
end checkDependenciesOnMask;

--********************************************************************************************************************************
procedure callPutParameters ( olParameter in STB$PROPERTY$LIST,
                        oParameter  in STB$MENUENTRY$RECORD,
                        clParameter in clob default null,
                        oErrorObj in out STB$OERROR$RECORD)
is
  sCurrentName    constant varchar2(100) := $$PLSQL_UNIT||'.callPutParameters(' || oParameter.sToken  || ')';
  sUserMsg              varchar2(4000);
  nParameterCount       number;
  nStartTimeNum  number                  := DBMS_UTILITY.GET_TIME;
   /*cursor cParameterCount(csPackage in varchar2 default null) is
    select count(*)
      from user_arguments
     where UPPER (object_name) = 'PUTPARAMETERS'
       and UPPER (package_name) = upper (csPackage)
      -- and UPPER (argument_name) = upper (csParameter)
       and argument_name is not null;*/
begin
  isr$trace.stat('begin','begin',sCurrentName);
  isr$trace.debug('parameter','olParameter',sCurrentName,olParameter);
  if Trim(oParameter.sHandledBy) is null then
    sPackage := Stb$object.getCurrentNodePackage;
    if Trim(sPackage) is null then
      sPackage := STB$UTIL.getCurrentLal;
    end if;
  else
    sPackage := oParameter.sHandledBy;
  end if;
  isr$trace.debug('sPackage set', sPackage, sCurrentName);

  -- ESig has to be saved first because of the error object
  -- In case of errors in the putParameters function the eSig entry should be deleted !!!
  if oParameter.sConfirm = 'E' then
    oErrorObj := STB$SECURITY.putEsigMask(olParameter,oParameter.sToken,sPackage,STB$OBJECT.getCurrentNodeId,STB$OBJECT.getCurrentNodeType);
  end if;

  /*open cParameterCount(sPackage);
  fetch cParameterCount into nParameterCount;
  close cParameterCount;*/   -- ASTIB-18
  select count(1) cnt into nParameterCount
      from user_arguments
     where UPPER (object_name) = 'PUTPARAMETERS'
       and UPPER (package_name) = upper (sPackage)
       and argument_name is not null;
  -- call of the package which handles the request
  if nParameterCount = 3 then
    execute immediate
     'DECLARE
        olocError STB$OERROR$RECORD;
      BEGIN
        olocError:='|| sPackage ||'.putParameters(:1, :2, :3);
        :4 := olocError;
      END;'
      using in oParameter, in olParameter, in clParameter, out oErrorObj;
  elsif nParameterCount = 2 then
    execute immediate
     'DECLARE
        olocError STB$OERROR$RECORD;
      BEGIN
        olocError:='|| sPackage ||'.putParameters(:1, :2);
        :3 := olocError;
      EXCEPTION  WHEN OTHERS THEN
       olocError.handleError ( Stb$typedef.cnSeverityWarning, :4, :5, ''SQLCODE = ''||SQLCODE||'', SQLERRM = ''||SQLERRM||'' ''||dbms_utility.format_error_backtrace);
      END;'
      using in oParameter, in olParameter, in out oErrorObj, in sUserMsg, in sCurrentName;
  end if;
  isr$trace.debug('runtime', 'elapsed time: '||isr$util.getElapsedTime(nStartTimeNum), sCurrentName);
  isr$trace.stat('end','end',sCurrentName);
exception
  when others then
    sUserMsg := utd$msglib.getmsg ('exUnexpectedError', stb$security.getCurrentLanguage,  csP1 => sqlerrm(sqlcode));
    oErrorObj.handleError ( Stb$typedef.cnSeverityCritical, sUserMsg, sCurrentName, dbms_utility.format_error_stack ||stb$typedef.crlf|| dbms_utility.format_error_backtrace );

end callPutParameters;
--********************************************************************************************************************************
function putParameters( olParameter in STB$PROPERTY$LIST,
                        oParameter  in STB$MENUENTRY$RECORD,
                        clParameter in clob default null)
  return STB$OERROR$RECORD
is
  exCannotPerformAction exception;
  sCurrentName          constant varchar2(100) := $$PLSQL_UNIT||'.putParameters(' || oParameter.sToken  || ')';
  oErrorObj             STB$OERROR$RECORD := STB$OERROR$RECORD(tloMessageList => ISR$MESSAGE$LIST()); --initialize the message list too
  oErrorObj1            STB$OERROR$RECORD := STB$OERROR$RECORD();
  sMenuAllowed          varchar2(1) := 'F';
  sUserMsg              varchar2(4000);
  nStartTimeNum         number      := DBMS_UTILITY.GET_TIME;
  oParamListRec         ISR$PARAMLIST$REC := ISR$PARAMLIST$REC();


begin
  isr$trace.stat('begin','begin',sCurrentName);
  isr$trace.debug('parameter','olParameter',sCurrentName,olParameter);
  isr$trace.debug('parameter','oParameter',sCurrentName,oParameter);
  isr$trace.debug('parameter','length(clParameter): '||length(clParameter),sCurrentName,clParameter);

ISR$TREE$PACKAGE.selectCurrent('F');
  callPutParameters(olParameter, oParameter, clParameter, oErrorObj);
  isr$trace.debug('oErrorObj.sSeverityCode after callPutParameters ' , oErrorObj.sSeverityCode, sCurrentName);

  if oErrorObj.sSeverityCode in (Stb$typedef.cnFirstForm) then
    updateSessionWizardInfo('T');
  elsif upper (oparameter.stoken) IN ('CAN_MODIFY_OUTPUTOPTIONS'/*,'CAN_RELEASE_FOR_INSPECTION','CAN_FINALIZE_REPORT'*/) then
    updateSessionWizardInfo('F');
  end if;

  ---------------------------------------------------------------------------
  -- Emails are not sent if an audit or a wizard shall be displayed !!!
  if oErrorObj.sSeverityCode in (Stb$typedef.cnNoError, Stb$typedef.cnAudit, Stb$typedef.cnMsgWarningTimeout)
  and oErrorObj.sErrorCode != Utd$msglib.GetMsg('exStartJobCode',Stb$security.GetCurrentLanguage) then
    if STB$UTIL.getSystemParameter('NOTIFY') = 'T' then
      --oErrorObj1 := ISR$MAIL.SendEmail(oparameter.stoken,STB$OBJECT.getCurrentNodeType,STB$OBJECT.getCurrentNodeId,'END');
      oParamListRec.AddParam('CURRENTNODETYPE', STB$OBJECT.getCurrentNodeType);
      oParamListRec.AddParam('CURRENTNODEID', STB$OBJECT.getCurrentNodeId);
      oParamListRec.AddParam('NOTIFY_ON', 'END');

      oErrorObj1 := ISR$NOTIFY$PACKAGE.SENDMAIL(oparameter.stoken, oParamListRec);
    end if;
  end if;
  ---------------------------------------------------------------------------
  isr$trace.debug('oErrorObj.sSeverityCode before ' , oErrorObj.sSeverityCode, sCurrentName);
  -- reset important settings in error object
  oErrorObj.SetSeverityCodeToWorst;
  oErrorObj.SetMessageListFromAccessor;

  isr$trace.stat('end', 'end', sCurrentName);
  return (oErrorObj);
exception
  when exCannotPerformAction then
    rollback;
    sUserMsg := utd$msglib.getmsg ('exCannotPerformAction', stb$security.getCurrentLanguage,  csP1 => sqlerrm(sqlcode));
    oErrorObj.handleError ( Stb$typedef.cnSeverityCritical, sCurrentName, sCurrentName, dbms_utility.format_error_stack ||stb$typedef.crlf|| dbms_utility.format_error_backtrace );
    return oErrorObj;
  when others then
    rollback;
    sUserMsg := utd$msglib.getmsg ('exUnexpectedError', stb$security.getCurrentLanguage,  csP1 => sqlerrm(sqlcode));
    oErrorObj.handleError ( Stb$typedef.cnSeverityCritical, sUserMsg, sCurrentName, dbms_utility.format_error_stack ||stb$typedef.crlf|| dbms_utility.format_error_backtrace );
    return oErrorObj;
end putParameters;

-- ***************************************************************************************************
function putEsigFailure(olParameter in STB$PROPERTY$LIST)
  return STB$OERROR$RECORD
is
  sCurrentName   constant varchar2(100) := $$PLSQL_UNIT||'.putEsigFailure';
  sUserMsg       varchar2(4000);
  sDevMsg        varchar2(4000);
  sImplMsg       varchar2(4000);
  oErrorObj      STB$OERROR$RECORD    := STB$OERROR$RECORD(tloMessageList => ISR$MESSAGE$LIST()); --initialize the message list too
  nStartTimeNum  number      := DBMS_UTILITY.GET_TIME;
begin
  isr$trace.stat('begin', 'begin', sCurrentName);

  oErrorObj := stb$security.putesigmask (olparameter, 'ESIG_NO_SUCCCESS', 'ISR$REPORTTYPE$PACKAGE', STB$OBJECT.getCurrentNodeId, STB$OBJECT.getCurrentNodeType, 'F');

  isr$trace.stat('end', 'end', sCurrentName);
  return (oErrorObj);
exception
  when others then
    rollback;
    sUserMsg := utd$msglib.getmsg ('exUnexpectedError', stb$security.getCurrentLanguage,  csP1 => sqlerrm(sqlcode));
    sImplMsg := sCurrentName || rtrim(': '||sImplMsg,': ');
    sDevMsg  := substr(dbms_utility.format_error_stack ||stb$typedef.crlf|| dbms_utility.format_error_backtrace, 1,4000);
    oErrorObj.handleError ( Stb$typedef.cnSeverityCritical, sUserMsg, sImplMsg, sDevMsg );
    return oErrorObj;
end putEsigFailure;

-- ***************************************************************************************************
function GetNodeChilds( oSelectedNode     in  STB$TREENODE$RECORD,
                        olChildNodes      out STB$TREENODELIST,
                        csApplicationCall in  varchar2 default 'T',
                        csTransActionNo   in  varchar2 default 'F')
  return STB$OERROR$RECORD
is
  sCurrentName       constant varchar2(100) := $$PLSQL_UNIT||'.GetNodeChilds('||oSelectedNode.sNodeType||')';
  oErrorObj          STB$OERROR$RECORD      := STB$OERROR$RECORD(tloMessageList => ISR$MESSAGE$LIST()); --initialize the message list too
  nStartTimeNum      number                 := DBMS_UTILITY.GET_TIME;
  sUserMsg           varchar2(4000);
  sDevMsg            varchar2(4000);
  sImplMsg           varchar2(4000);

  cursor cGetTopNode is
    select   Utd$msglib.GetMsg( customText, Stb$security.GetCurrentLanguage ) sDisplay,
             package sPackageType,
             nodetype sNodetype,
             nodeid sNodeid
    from     isr$custom$grouping
    where    parentnode is null
    order by orderno;


begin
  isr$trace.stat('begin', 'csApplicationCall: '||csApplicationCall||',  csTransActionNo: '||csTransActionNo, sCurrentName);
  isr$trace.debug('parameter','oSelectedNode',sCurrentName,oSelectedNode);

  olChildNodes := STB$TREENODELIST();

  if Trim(oSelectedNode.sNodeId) is null then

    olChildNodes := STB$TREENODELIST();
    olChildNodes.extend();
    olChildNodes(1) := STB$TREENODE$RECORD();
    open cGetTopNode;
    fetch cGetTopNode
    into olChildNodes(1).sDisplay, olChildNodes(1).sPackageType, olChildNodes(1).sNodeType, olChildNodes(1).sNodeId;
    close cGetTopNode;
    olChildNodes(1).tloValueList := ISR$VALUE$LIST();
    STB$OBJECT.setDirectory(olChildNodes(1));
    oErrorObj := STB$GAL.setCurrentNode(oSelectedNode, 'T');
    ISR$TREE$PACKAGE.saveTree(olChildNodes, oSelectedNode, 'T');
    oErrorObj := isr$server$base.checkServerStatus;

    <<LimsUserRights>>
    declare
      sUseLIMSAccessPrivs varchar2(100);
      sRightsEntity varchar2(100);
      sRightsKeyCol varchar2(100);
      sRightsUserCol varchar2(100);
      /*type rRight is record( sKey varchar2(100),
                             sUser varchar2(100));*/
      oSaveErrorObj STB$OERROR$RECORD := oErrorObj;
      nSaveWorstError integer := iSR$ErrorObj$Accessor.GetWorstSeverityCode;
      sSQL varchar2(32000);
      oParamList iSR$ParamList$Rec := iSR$ParamList$Rec(); --parameter list
      sDummy varchar2(100);
    begin
      sDummy :=  STB$SECURITY.checkGroupRight('CAN_OVERRULE_REMOTE_SECURITY_CHECK');
      isr$trace.debug('CAN_OVERRULE_REMOTE_SECURITY_CHECK', sDummy, sCurrentName||' - LimsUserRights');
      if STB$SECURITY.checkGroupRight('CAN_OVERRULE_REMOTE_SECURITY_CHECK') = 'F' then
        isr$trace.debug('code position','#2#', sCurrentName);
        sUseLIMSAccessPrivs := STB$UTIL.getSystemparameter('USE_REMOTE_SECURITY_PRIVS');
        sRightsEntity := STB$UTIL.getSystemparameter('REMOTE_SECURITY_ENTITY');
        sRightsKeyCol:= STB$UTIL.getSystemparameter('REMOTE_SECURITY_KEY');
        sRightsUserCol := STB$UTIL.getSystemparameter('REMOTE_SECURITY_USER');
        -- check if user rights are configured
        if sRightsEntity is null or sRightsKeyCol is null or sRightsUserCol is null or NVL(sUseLIMSAccessPrivs, 'F') ='F' then
          isr$trace.debug('code position','#3#', sCurrentName);
          isr$trace.debug('LimsUserRights not active', 'Lims User Rights functionality is disabled or not configured', sCurrentName||' - LimsUserRights');
        else
          isr$trace.debug('code position','#4#', sCurrentName);
          oErrorObj := ISR$REMOTE$COLLECTOR.fillTmpTableForEntity(sRightsEntity, oParamList, 'F');
          if oErrorObj.sSeverityCode = Stb$typedef.cnSeverityWarning then
            isr$trace.debug('code position','#5#', sCurrentName);
            -- suppress not running serwer warning.
            -- write another warning  into the log (remote collector has already writte one)
            isr$trace.warn('rights entity not loaded', 'no group rights retrieved', sCurrentName);
            -- Remove error from error stack. Isn't this a hack?
            -- re-initialize the error object
            oErrorObj := STB$OERROR$RECORD(tloMessageList => ISR$MESSAGE$LIST());
            -- then restore it from the saved error obj
            oErrorObj := oSaveErrorObj;
            -- use the saved error obj to restore the worst severity in the error obj accessor package
            oSaveErrorObj.sSeverityCode := nSaveWorstError;
            iSR$ErrorObj$Accessor.SetCurrentErrorObj(oSaveErrorObj);
            -- now all should be like it was before trying to fill the table
          end if;

          delete from ISR$REMOTE$SECURITY$KEYS where sessionid = STB$SECURITY.getCurrentSession;
          sSQL := 'insert into ISR$REMOTE$SECURITY$KEYS(RIGHTSKEY,USERNO,SESSIONID) '||
                  'select '||sRightsKeyCol||', '||STB$SECURITY.getCurrentUser||','||STB$SECURITY.getCurrentSession||
                  ' from TMP$'||sRightsEntity||
                  ' where '||sRightsUserCol||'='||
                  '    (select limsusername from STB$USER where userno = '||STB$SECURITY.getCurrentUser||')';
          isr$trace.debug('sSQL', sSQL, sCurrentName||' - LimsUserRights');
          execute immediate sSql;
          isr$trace.debug('rows inserted into ISR$USER$LIMS$RIGHTS', sql%rowcount, sCurrentName||' - LimsUserRights');
          execute immediate 'delete from TMP$'||sRightsEntity;
          commit;

        end if;
      end if;

    end LimsUserRights;

    isr$trace.debug('code position','#6#', sCurrentName);
    return (oErrorObj);
  end if;

  if (    trim(oSelectedNode.sPackageType) is null
       or ISR$CUSTOMER.loadOnlyRight(oSelectedNode) = 'T' ) then
    isr$trace.debug('code position','#7#', sCurrentName);
    -- in case of "TRANSPORT"-Nodes do nothing
    olChildNodes := STB$TREENODELIST();
  else
    isr$trace.debug('code position','#8#', sCurrentName);
    STB$OBJECT.setCurrentNode(oSelectedNode);

    IF STB$UTIL.checkMethodExists('getNodeChilds', oSelectedNode.sPackageType) = 'T' THEN

      sUserMsg := sUserMsg || 'Try to retrieve the subnodes of the selected tree node in the package  ' || oSelectedNode.sPackageType || Stb$typedef.cr;
      -- this request is handled by  another package, call it dynamically
      execute immediate
      'DECLARE
         olocError STB$OERROR$RECORD;
       BEGIN
         olocError :='|| oSelectedNode.sPackageType  ||'.GetNodeChilds( :1, :2 );
         :3 :=  olocError;
      END;'
      using in oSelectedNode, out olChildNodes, out oErrorObj;

      isr$trace.debug('status olChildNodes.count','olChildNodes.count: '||olChildNodes.count(), sCurrentName);

      ISR$TREE$PACKAGE.saveTree(olChildNodes, oSelectedNode, 'T');

    ELSE

      olChildNodes := STB$TREENODELIST();
    END IF;

    if csApplicationCall = 'T' then
      ISR$TREE$PACKAGE.saveTreeSessions(oSelectedNode, 'T', 'T');
    end if;

  end if;

  isr$trace.debug('parameter','out olChildNodes',sCurrentName,olChildNodes);
  isr$trace.stat('end', oSelectedNode.sNodeType||' has childs: '||olChildNodes.count()||' -- elapsed time: '||isr$util.getElapsedTime(nStartTimeNum), sCurrentName);
  return (oErrorObj);

exception
  when others then
    rollback;
    sUserMsg := utd$msglib.getmsg ('exUnexpectedError', stb$security.getCurrentLanguage,  csP1 => sqlerrm(sqlcode));
    sImplMsg := sCurrentName || rtrim(': '||sImplMsg,': ');
    sDevMsg  := substr(dbms_utility.format_error_stack ||stb$typedef.crlf|| dbms_utility.format_error_backtrace, 1,4000);
    oErrorObj.handleError ( Stb$typedef.cnSeverityCritical, sUserMsg, sImplMsg, sDevMsg );
    return oErrorObj;
end GetNodeChilds;

-- ***************************************************************************************************
function getNodeList( oSelectedNode     in  STB$TREENODE$RECORD,
                      olNodeList        out STB$TREENODELIST,
                      csApplicationCall in  varchar2 default 'T',
                      csTransActionNo IN VARCHAR2 DEFAULT 'F')
  return STB$OERROR$RECORD
is
  oErrorObj        STB$OERROR$RECORD := STB$OERROR$RECORD(tloMessageList => ISR$MESSAGE$LIST()); --initialize the message list too
  olParameter      STB$PROPERTY$LIST;
  sCurrentName     constant varchar2(100) := $$PLSQL_UNIT||'.getNodeList('||oSelectedNode.sNodeType||')';
  sUserMsg         varchar2(4000);
  sDevMsg          varchar2(4000);
  sImplMsg         varchar2(4000);
  nCols            number;
  sAttachments     varchar2(1) := 'F';
  nStartTimeNum    number      := DBMS_UTILITY.GET_TIME;


  sLeft_As_Right VARCHAR2(1) := 'T';

begin
  isr$trace.stat('begin', 'oSelectedNode.sNodeType: '||oSelectedNode.sNodeType||'  csApplicationCall: '||csApplicationCall ,sCurrentName);
  isr$trace.debug('parameter','oSelectedNode',sCurrentName,oSelectedNode);
  olNodeList := STB$TREENODELIST();

  -- ISRC-1205
  STB$OBJECT.setCurrentNode(oSelectedNode);

  if oSelectedNode.sPackageType = '' then
    -- in case of "TRANSPORT"-Nodes do nothing
    olNodeList := null;
  else

    sMethodExists := STB$UTIL.CHECKMETHODEXISTS('getNodeList', oSelectedNode.sPackageType);
    if sMethodExists = 'F' then
      sMethodExists := stb$util.checkObjectExists('getNodeList', oSelectedNode.sPackageType);
    end if;
    IF sMethodExists = 'T' THEN
      isr$trace.debug('other package then STB$GAL', oSelectedNode.sPackageType, sCurrentName);
      -- this request is handled by  another package, call it dynamically
      execute immediate
      'DECLARE
         olocError STB$OERROR$RECORD;
       BEGIN
         olocError :='|| oSelectedNode.sPackageType  ||'.GetNodeList( :1, :2 );
         :3 :=  olocError;
      END;'
      using in oSelectedNode, out olNodeList, out oErrorObj;

      if olNodeList.exists(olNodeList.count())
      and olNodeList(olNodeList.count()).tloPropertyList.EXISTS(olNodeList(olNodeList.count()).tloPropertyList.count()) then
        sLeft_As_Right := 'F';
        nCols := olNodeList(olNodeList.count()).tloPropertyList.count();
      end if;



      -- decide, what icon to display
      if sAttachments = 'T' then
        execute immediate
          'BEGIN
             '|| STB$UTIL.getCurrentCustomPackage  ||'.getCustomIcon(:1);
           END;'
        using in out olNodeList;
      end if;

      if sLeft_As_Right = 'F' or ISR$TREE$PACKAGE.checkFirstLoad(oSelectedNode) = 'T' then
        ISR$TREE$PACKAGE.saveTree(olNodeList, oSelectedNode, sLeft_As_Right);
      end if;

    ELSE
      olNodeList := STB$TREENODELIST();
    END IF;

    if csApplicationCall = 'T' then
      ISR$TREE$PACKAGE.saveTreeSessions(oSelectedNode, 'T', 'T', sLeft_As_Right);
    end if;

  end if;

  -- reset important settings in error object
  oErrorObj.SetSeverityCodeToWorst;
  oErrorObj.SetMessageListFromAccessor;

  isr$trace.debug('parameter','olNodeList OUT',sCurrentName,olNodeList);
  isr$trace.stat('end', 'end '||oSelectedNode.sDisplay||' has childs: '||olNodeList.count()||' -- elapsed time: '||isr$util.getElapsedTime(nStartTimeNum), sCurrentName );
  return (oErrorObj);

exception
  when others then
    rollback;
    sUserMsg := utd$msglib.getmsg ('exUnexpectedError', stb$security.getCurrentLanguage,  csP1 => sqlerrm(sqlcode));
    sImplMsg := sCurrentName || rtrim(': '||sImplMsg,': ');
    sDevMsg  := substr(dbms_utility.format_error_stack ||stb$typedef.crlf|| dbms_utility.format_error_backtrace, 1,4000);
    oErrorObj.handleError ( Stb$typedef.cnSeverityCritical, sUserMsg, sImplMsg, sDevMsg );
    return oErrorObj;
end GetNodeList;

-- ***************************************************************************************************
function SetCurrentNode(oSelectedNode in STB$TREENODE$RECORD, csLeftSide IN VARCHAR2, csApplicationCall varchar2 default 'T')
  return STB$OERROR$RECORD
is
  oErrorObj         STB$OERROR$RECORD := STB$OERROR$RECORD(tloMessageList => ISR$MESSAGE$LIST()); --initialize the message list too
  sCurrentName      constant varchar2(100) := $$PLSQL_UNIT||'.SetCurrentNode('||oSelectedNode.sNodeType||')';
  sUserMsg          varchar2(4000);
  sDevMsg           varchar2(4000);
  sImplMsg          varchar2(4000);
  sWizardInUse      varchar2(1) := 'F';
  nStartTimeNum     number      := DBMS_UTILITY.GET_TIME;
  sOldPackage       varchar2(50);

  cursor cIsWizardInUse is
    select NVL(wizardInUse, 'F')
      from isr$session
     where sessionid = STB$SECURITY.getCurrentSession;
begin
  /* execute only if the current node is different from the set node  */
  if trim(STB$OBJECT.getCurrentNode().sNodeId) is null or oSelectedNode.sNodeId != STB$OBJECT.getCurrentNode().sNodeId then
    isr$trace.stat('begin', 'csApplicationCall: '||csApplicationCall||',  csLeftSide: '||csLeftSide, sCurrentName);
    isr$trace.debug('parameter','oSelectedNode',sCurrentName,oSelectedNode);

    open cIsWizardInUse;
    fetch cIsWizardInUse into sWizardInUse;
    if cIsWizardInUse%NOTFOUND then
      sWizardInUse := 'F';
    end if;
    close cIsWizardInUse;

    isr$trace.debug('status sWizardInUse', 'sWizardInUse: '||sWizardInUse, sCurrentName);

    if sWizardInUse = 'F' then

      -- unlock all reports from this usersession
      oErrorObj := stb$system.unlockreports ('and locksession = ' || to_char(stb$security.getcurrentsession));

      -- reset the context in stb$object
      sOldPackage := Stb$object.getCurrentNodePackage;
      if sOldPackage != '' then
        execute immediate
        'BEGIN
           '||sOldPackage||'.destroyOldValues;
         EXCEPTION WHEN OTHERS THEN
            NULL;
         END;';
      end if;

      -- call the correct "handler":
      if Trim(oSelectedNode.sPackageType) is not null then
        sUserMsg := sUserMsg || 'Try to setting the information on a tree node in the package  ' || oSelectedNode.sPackageType || Stb$typedef.cr;
        -- this request is handled by  another package, call it dynamically
        execute immediate
        'BEGIN
           :1 :='|| oSelectedNode.sPackageType  ||'.SetCurrentNode( :2 );
         END;'
        using out oErrorObj, in oSelectedNode;
        if csApplicationCall = 'T' then
          ISR$TREE$PACKAGE.saveTreeSessions(oSelectedNode, csLeftSide, 'T');
        end if;
      else
        -- store the selected node
        Stb$object.setCurrentNode(oSelectedNode);
      end if;
    end if;

    -- reset important settings in error object
    oErrorObj.SetSeverityCodeToWorst;
    oErrorObj.SetMessageListFromAccessor;

    isr$trace.stat('end','elapsed time: '||isr$util.getElapsedTime(nStartTimeNum), sCurrentName);
  end if;
  return (oErrorObj);

exception
  when others then
    rollback;
    sUserMsg := utd$msglib.getmsg ('exUnexpectedError', stb$security.getCurrentLanguage,  csP1 => sqlerrm(sqlcode));
    sImplMsg := sCurrentName || rtrim(': '||sImplMsg,': ');
    sDevMsg  := substr(dbms_utility.format_error_stack ||stb$typedef.crlf|| dbms_utility.format_error_backtrace, 1,4000);
    oErrorObj.handleError ( Stb$typedef.cnSeverityCritical, sUserMsg, sImplMsg, sDevMsg );
    return oErrorObj;
end SetCurrentNode;

--*****************************************************************************************************
function getAuditMask(olNodeList out STB$TREENODELIST)
  return STB$OERROR$RECORD
is
  sCurrentName   constant varchar2(100) := $$PLSQL_UNIT||'.getAuditMask';
  sUserMsg       varchar2(4000);
  sDevMsg        varchar2(4000);
  sImplMsg       varchar2(4000);
  oErrorObj      STB$OERROR$RECORD := STB$OERROR$RECORD(tloMessageList => ISR$MESSAGE$LIST()); --initialize the message list too
  nStartTimeNum  number      := DBMS_UTILITY.GET_TIME;
begin
  isr$trace.stat('begin', 'begin', sCurrentName);
  execute immediate
     'DECLARE
        olocError STB$OERROR$RECORD;
      BEGIN
         olocError := '||sPackage||'.getAuditMask(:1);
        :2 := olocError;
      END;'
    Using OUT olNodeList, OUT oErrorObj;

  -- reset important settings in error object
  oErrorObj.SetSeverityCodeToWorst;
  oErrorObj.SetMessageListFromAccessor;

  isr$trace.stat('end', 'end', sCurrentName);
  return (oErrorObj);
exception
  when others then
    rollback;
    sUserMsg := utd$msglib.getmsg ('exUnexpectedError', stb$security.getCurrentLanguage,  csP1 => sqlerrm(sqlcode));
    sImplMsg := sCurrentName || rtrim(': '||sImplMsg,': ');
    sDevMsg  := substr(dbms_utility.format_error_stack ||stb$typedef.crlf|| dbms_utility.format_error_backtrace, 1,4000);
    oErrorObj.handleError ( Stb$typedef.cnSeverityCritical, sUserMsg, sImplMsg, sDevMsg );
    return oErrorObj;
END getAuditMask;

--******************************************************************************
function putAuditMask(olParameter in STB$PROPERTY$LIST)  return STB$OERROR$RECORD
is
  sCurrentName   constant varchar2(100) := $$PLSQL_UNIT||'.putAuditMask';
  sUserMsg       varchar2(4000);
  sDevMsg        varchar2(4000);
  sImplMsg       varchar2(4000);
  oErrorObj      STB$OERROR$RECORD := STB$OERROR$RECORD(tloMessageList => ISR$MESSAGE$LIST()); --initialize the message list too

  nStartTimeNum  number      := DBMS_UTILITY.GET_TIME;

  -- TR, 201602091654, fuer die unsichtbare "CAN_RUN_REPORT" maske
  olnodelist      STB$TREENODELIST := STB$TREENODELIST();

begin
  isr$trace.stat('begin', 'begin', sCurrentName);

  sPackage := 'ISR$REPORTTYPE$PACKAGE';
  isr$trace.info('sPackage set', sPackage, sCurrentName);

  execute immediate
     'DECLARE
        olocError STB$OERROR$RECORD;
      BEGIN
         olocError :='|| sPackage ||'.putAuditMask(:1);
        :2 :=  olocError;
      END;'
    using in olParameter , out oErrorObj;

  commit;
  ISR$TREE$PACKAGE.sendSyncCall;

-- evaluate "Flag" from putsavedialog wether to create the document right now
  if sCreateTheDoc = 'T' then
    oErrorObj := ISR$REPORTTYPE$PACKAGE.callFunction(STB$MENUENTRY$RECORD('TOKEN','CAN_RUN_REPORT','CAN_RUN_REPORT','ISR$REPORTTYPE$PACKAGE','','F'), olnodelist);
    if oErrorObj.isExceptionCritical then
        raise stb$typedef.exCritical;
    end if;
    oErrorObj := ISR$REPORTTYPE$PACKAGE.putParameters(STB$MENUENTRY$RECORD('TOKEN','CAN_RUN_REPORT','CAN_RUN_REPORT','ISR$REPORTTYPE$PACKAGE','','F'), olnodelist(1).tloPropertyList);
  end if;

  -- reset important settings in error object
  oErrorObj.SetSeverityCodeToWorst;
  oErrorObj.SetMessageListFromAccessor;
  isr$trace.stat('end', 'end', sCurrentName);
  return (oErrorObj);
exception
  when others then
    rollback;
    sUserMsg := utd$msglib.getmsg ('exUnexpectedError', stb$security.getCurrentLanguage,  csP1 => sqlerrm(sqlcode));
    sImplMsg := sCurrentName || rtrim(': '||sImplMsg,': ');
    sDevMsg  := dbms_utility.format_error_stack ||stb$typedef.crlf|| dbms_utility.format_error_backtrace;
    oErrorObj.handleError ( Stb$typedef.cnSeverityCritical, sUserMsg, sImplMsg, sDevMsg );
    return oErrorObj;
end putAuditMask;

--******************************************************************************
function getContextMenu( nMenu           in  number,
                         olMenuEntryList out STB$MENUENTRY$LIST )
  return STB$OERROR$RECORD
is
  oErrorObj        STB$OERROR$RECORD := STB$OERROR$RECORD(tloMessageList => ISR$MESSAGE$LIST()); --initialize the message list too
  olMenuEntryList1 STB$MENUENTRY$LIST := STB$MENUENTRY$LIST();
  sCurrentName     constant varchar2(100) := $$PLSQL_UNIT||'.getContextMenu('||nMenu||')';
  sUserMsg         varchar2(4000);
  sDevMsg          varchar2(4000);
  sImplMsg         varchar2(4000);
  sIsLDAPUser         varchar2 (1);
  sCurrentNodePackage varchar2(100);
  nStartTimeNum       number      := DBMS_UTILITY.GET_TIME;

  cursor cIsLDAPUser(cnUserNo in number) is
    select 'T'
      from stb$user
     where userno = cnUserNo
       and ldapusername is not null;
begin
  isr$trace.stat('begin', 'nMenu: '||nMenu||'  Stb$object.getCurrentNodeType: '||Stb$object.getCurrentNodeType, sCurrentName);
  olMenuEntryList := null;
  sCurrentNodePackage := STB$OBJECT.getCurrentNodePackage;
  isr$trace.debug('value', 'sCurrentNodePackage: '||sCurrentNodePackage, sCurrentName);

  if sCurrentNodePackage is null then
    -- in case of "TRANSPORT"-Nodes do nothing, no contextmenu for a form
    olMenuEntryList := null;
  elsif sCurrentNodePackage = 'STB$GAL' then
    -- in case of GAL, NodeList and NodeChilds are identical:
    oErrorObj := Stb$util.getContextMenu( Stb$object.getCurrentNodeType, nMenu, olMenuEntryList );
  else
    -- this request is handled by  another package, call it dynamically
    execute immediate
    'DECLARE
       olocError STB$OERROR$RECORD;
     BEGIN
       olocError :='|| sCurrentNodePackage  ||'.getContextMenu( :1, :2 );
       :3 :=  olocError;
     END;'
    using in nMenu, out olMenuEntryList, out oErrorObj;
  end if;

  select STB$MENUENTRY$RECORD (STYPE
                             , SDISPLAY
                             , STOKEN
                             , SHANDLEDBY
                             , SICON
                             , SCONFIRM
                             , SCONFIRMTEXT
                             , nID)
    bulk collect
    into olMenuEntryList1
    from (select *
            from table (olMenuEntryList)
           where sType != 'delete');

  -- delete separators if more than one are displayed behind
  for i in 1..olMenuEntryList1.count loop
    IF olMenuEntryList1(i).sTOKEN = 'CAN_CHANGE_OWN_PASSWORD' then
      open cIsLDAPUser(Stb$security.getCurrentUser);
      fetch cIsLDAPUser into sIsLDAPUser;
      close cIsLDAPUser;
      if NVL(sIsLDAPUser, 'F') = 'T' then -- is LDAP user
        olMenuEntryList1(i).sType := 'DELETE';
      end if;
    end if;
    IF i > 1
    and 'SEPARATOR' = olMenuEntryList1(i).sType
    and 'SEPARATOR' = olMenuEntryList1(i-1).sType then
      olMenuEntryList1(i-1).sType := 'DELETE';
    end if;
    if i in (1, olMenuEntryList1.count)
    and 'SEPARATOR' = olMenuEntryList1(i).sType then
      olMenuEntryList1(i).sType := 'DELETE';
    end if;
  end loop;

  select STB$MENUENTRY$RECORD (STYPE
                             , SDISPLAY
                             , STOKEN
                             , SHANDLEDBY
                             , SICON
                             , SCONFIRM
                             , SCONFIRMTEXT
                             , nid)
    bulk collect
    into olMenuEntryList
    from (select *
            from table (olMenuEntryList1)
           where sType != 'DELETE');
  for i in 1..  olMenuEntryList.count() loop
       isr$trace.debug_all('status token', 'olMenuEntryList('||i||').sToken: '||olMenuEntryList(i).sToken, sCurrentName);
  end loop;

  -- reset important settings in error object
  oErrorObj.SetSeverityCodeToWorst;
  oErrorObj.SetMessageListFromAccessor;

  isr$trace.debug('status','olMenuEntryList OUT',sCurrentName,olMenuentryList);
  isr$trace.stat('end', 'olMenuEntryList.count: '||olMenuEntryList.count, sCurrentName);
  return (oErrorObj);
exception
  when others then
    rollback;
    sUserMsg := utd$msglib.getmsg ('exUnexpectedError', stb$security.getCurrentLanguage,  csP1 => sqlerrm(sqlcode));
    sImplMsg := sCurrentName || rtrim(': '||sImplMsg,': ');
    sDevMsg  := substr(dbms_utility.format_error_stack ||stb$typedef.crlf|| dbms_utility.format_error_backtrace, 1,4000);
    oErrorObj.handleError ( Stb$typedef.cnSeverityCritical, sUserMsg, sImplMsg, sDevMsg );
    return oErrorObj;
end getContextMenu;

--******************************************************************************
function synchronize( sToken           out varchar2,
                      olNodeReloadFrom out STB$TREENODELIST,
                      oNewCurrentNode  out STB$TREENODE$RECORD )
  return STB$OERROR$RECORD
is
  sCurrentName   constant varchar2(100) := $$PLSQL_UNIT||'.synchronize';
  sUserMsg       varchar2(4000);
  sDevMsg        varchar2(4000);
  sImplMsg       varchar2(4000);
  oErrorObj      STB$OERROR$RECORD := STB$OERROR$RECORD(tloMessageList => ISR$MESSAGE$LIST()); --initialize the message list too
  nStartTimeNum  number      := DBMS_UTILITY.GET_TIME;

begin
  isr$trace.stat('begin', 'begin', sCurrentName);
  olNodeReloadFrom := STB$TREENODELIST();
  oNewCurrentNode  := STB$TREENODE$RECORD();

  oNewCurrentNode  := Stb$object.getCurrentNode;
  isr$trace.debug('oNewCurrentNode.xml','s.logclob', sCurrentName, xmltype(oNewCurrentNode) );

  sToken := Stb$object.getReloadToken;
  if sToken is null then
    isr$trace.warn('warning','sToken is null', sCurrentName);
  else
    isr$trace.debug('status','sToken:'||sToken, sCurrentName);
  end if;

  olNodeReloadFrom := Stb$object.getReloadTreeList;
  if olNodeReloadFrom is not null and olNodeReloadFrom.exists(olNodeReloadFrom.count()) then
    isr$trace.debug('status','olNodeReloadFrom', sCurrentName, sys.anydata.convertCollection(olNodeReloadFrom));
  else
    isr$trace.error('error', 'empty TreeNodeList: olNodeReloadFrom', sCurrentName);
  end if;

  -- reset important settings in error object
  oErrorObj.SetSeverityCodeToWorst;
  oErrorObj.SetMessageListFromAccessor;

  --isr$trace.debug('staus', 'olNodeReloadFrom, see column LOGCLOB', sCurrentName, sys.anydata.convertCollection(olNodeReloadFrom));
  isr$trace.stat('end', 'end', sCurrentName);

  return (oErrorObj);
exception
  when others then
    sUserMsg := utd$msglib.getmsg ('exUnexpectedError', stb$security.getCurrentLanguage,  csP1 => sqlerrm(sqlcode));
    sImplMsg := sCurrentName || rtrim(': '||sImplMsg,': ');
    sDevMsg  := substr(dbms_utility.format_error_stack ||stb$typedef.crlf|| dbms_utility.format_error_backtrace, 1,4000);
    oErrorObj.handleError ( Stb$typedef.cnSeverityCritical, sUserMsg, sImplMsg, sDevMsg );
    return oErrorObj;
end synchronize;

--******************************************************************************
function GetCurrentLanguage return number
is
begin
  return Stb$security.GetCurrentLanguage;
end GetCurrentLanguage;

--******************************************************************************
procedure getGuiPersistance(clobPersistance out clob)
IS
  sCurrentName   constant varchar2(100) := $$PLSQL_UNIT||'.getGuiPersistance';
  sUserMsg       varchar2(4000);
  sDevMsg        varchar2(4000);
  sImplMsg       varchar2(4000);
  oErrorObj      stb$oerror$record      := STB$OERROR$RECORD();
  nStartTimeNum  number      := DBMS_UTILITY.GET_TIME;

  cursor cGetPers is
    select persistance
      from STB$GUI$PERSISTANCE
     where userno = Stb$security.GetCurrentUser;

begin
  isr$trace.stat('begin', 'begin', sCurrentName);
  open cGetPers;
  fetch cGetPers into clobPersistance;
  close cGetPers;
  isr$trace.stat('end', 'clobPersistance, see column LOGCLOB', sCurrentName, clobPersistance);
exception
  when others then
    sUserMsg := utd$msglib.getmsg ('exUnexpectedError', stb$security.getCurrentLanguage,  csP1 => sqlerrm(sqlcode));
    sImplMsg := sCurrentName || rtrim(': '||sImplMsg,': ');
    sDevMsg  := substr(dbms_utility.format_error_stack ||stb$typedef.crlf|| dbms_utility.format_error_backtrace, 1,4000);
    oErrorObj.handleError ( Stb$typedef.cnSeverityCritical, sUserMsg, sImplMsg, sDevMsg );
    raise;
end getGuiPersistance;

--******************************************************************************
function getJobParameter( csName  in STB$JOBPARAMETER.PARAMETERNAME%type,
                          cnJobid in STB$JOBPARAMETER.JOBID%type)
  return STB$JOBPARAMETER.PARAMETERVALUE%type
is
begin
  return stb$job.getJobParameter(csName, cnJobid);
end getJobParameter;

-- *********************************************************************************************************************
procedure setGuiPersistance(clobPersistance in clob)
is
  pragma autonomous_transaction;
  sCurrentName constant varchar2(100) := $$PLSQL_UNIT||'.setGuiPersistance';
  oErrorObj    stb$oerror$record      := STB$OERROR$RECORD();
  sUserMsg     varchar2(4000);
  sDevMsg      varchar2(4000);
  sImplMsg     varchar2(4000);
  nStartTimeNum number      := DBMS_UTILITY.GET_TIME;
begin
  isr$trace.stat('begin', 'clobPersistance, see column LOGCLOB', sCurrentName, clobPersistance);
  delete from STB$GUI$PERSISTANCE WHERE userno =Stb$security.GetCurrentUser;
  insert into STB$GUI$PERSISTANCE (userno, persistance)
  values(Stb$security.GetCurrentUser, clobPersistance);
  commit;
  isr$trace.stat('end', 'end', sCurrentName);
exception when others then
    rollback;
    sUserMsg := utd$msglib.getmsg ('exUnexpectedError', stb$security.getCurrentLanguage,  csP1 => sqlerrm(sqlcode));
    sImplMsg := sCurrentName || rtrim(': '||sImplMsg,': ');
    sDevMsg  := substr(dbms_utility.format_error_stack ||stb$typedef.crlf|| dbms_utility.format_error_backtrace, 1,4000);
    oErrorObj.handleError ( Stb$typedef.cnSeverityCritical, sUserMsg, sImplMsg, sDevMsg );
end setGuiPersistance;

--******************************************************************************
function getMenu( sToken          in  varchar2,
                  olMenuEntryList out STB$MENUENTRY$LIST )
  return STB$OERROR$RECORD
is
  sCurrentName constant varchar2(100) := $$PLSQL_UNIT||'.getMenu('||sToken||')';
  oErrorObj    stb$oerror$record      := STB$OERROR$RECORD();
  sUserMsg     varchar2(4000);
  sDevMsg      varchar2(4000);
  sImplMsg     varchar2(4000);
  nStartTimeNum number      := DBMS_UTILITY.GET_TIME;

  -- THE TOKEN MUST BE DEFINED ONLY ONCE  
  cursor cMenu is
    select ENTRYID
         , TOKEN
         , attribute
         , HANDLEDBY
         , CONFIRM
      from STB$CONTEXTMENU
     where token = sToken
       and attribute = 'TOKEN';
   
  -- when the same token for different nodetypes, 
  -- e.g tokens for 'DOCUMENT', 'DOCUMENT_EXCEL', 'DOCUMENT_WORD'  NUVIB-19, -105
   cursor cMenuForNodetype is
    select ENTRYID
         , TOKEN
         , attribute
         , HANDLEDBY
         , CONFIRM
      from STB$CONTEXTMENU
     where token = sToken
       and attribute = 'TOKEN'
       and nodetype = stb$object.getCurrentNodeType;
       

begin
  isr$trace.stat('begin', 'sToken: '||sToken || '; sCurrentNodeType: ' || stb$object.getCurrentNodeType, sCurrentName);
    
  olMenuEntryList :=  STB$MENUENTRY$LIST();
  
  for rMenu in cMenuForNodetype loop
    olMenuEntryList.extend();
    olMenuEntryList(1) := STB$MENUENTRY$RECORD(rMenu.attribute, Utd$msglib.GetMsg(rMenu.Token,Stb$security.GetCurrentLanguage), 
         rMenu.token, rMenu.handledby, null, rMenu.confirm, utd$msglib.getmsg ('CONFIRM_TEXT_' || rMenu.token, stb$security.getcurrentlanguage));
  end loop;
  
  if olMenuEntryList.count = 0 then
    for rMenu in cMenu loop
    olMenuEntryList.extend();
    olMenuEntryList(1) := STB$MENUENTRY$RECORD(rMenu.attribute, Utd$msglib.GetMsg(rMenu.Token,Stb$security.GetCurrentLanguage), 
         rMenu.token, rMenu.handledby, null, rMenu.confirm, utd$msglib.getmsg ('CONFIRM_TEXT_' || rMenu.token, stb$security.getcurrentlanguage));
  end loop;
  
  end if;
  
  
  isr$trace.debug('olMenuEntryList(1)', 's. logclob', sCurrentName, olMenuEntryList);
  isr$trace.stat('end', 'end', sCurrentName);
  return (oErrorObj);
exception
 when others then
   rollback;
    sUserMsg := utd$msglib.getmsg ('exUnexpectedError', stb$security.getCurrentLanguage,  csP1 => sqlerrm(sqlcode));
    sImplMsg := sCurrentName || rtrim(': '||sImplMsg,': ');
    sDevMsg  := substr(dbms_utility.format_error_stack ||stb$typedef.crlf|| dbms_utility.format_error_backtrace, 1,4000);
    oErrorObj.handleError ( Stb$typedef.cnSeverityCritical, sUserMsg, sImplMsg, sDevMsg );
    return oErrorObj;
end getMenu;

--******************************************************************************
FUNCTION setClientPort(nClientPort in INTEGER) RETURN STB$OERROR$RECORD
is
  pragma autonomous_transaction;
  sCurrentName constant varchar2(100) := $$PLSQL_UNIT||'.setClientPort('||nClientPort||')';
  oErrorObj    stb$oerror$record      := STB$OERROR$RECORD();
  sUserMsg     varchar2(4000);
  sDevMsg      varchar2(4000);
  sImplMsg     varchar2(4000);
  sReturn      VARCHAR2(4000);
  rSessionid   ISR$SESSION.sessionid%TYPE;
  nStartTimeNum number      := DBMS_UTILITY.GET_TIME;
  exSendTokenFunction   exception;

  cursor cGetInactiveSessions is
    select sessionid
      from isr$session
     where ip = STB$SECURITY.getClientIp
       and port = nClientPort;
begin
  isr$trace.stat('begin', 'nClientPort: '||nClientPort, sCurrentName);
  isr$trace.debug('status getCurrentSession', 'STB$SECURITY.getCurrentSession: '||STB$SECURITY.getCurrentSession, sCurrentName);

  open cGetInactiveSessions;
  fetch cGetInactiveSessions into rSessionid;
  if cGetInactiveSessions%FOUND then
    isr$trace.debug('deleted inactive connection', 'rSessionId: '||rSessionId, sCurrentName);
    deleteSessionInfo(rSessionid);
  end if;
  close cGetInactiveSessions;

  Stb$security.setClientPort(nClientPort);

  isr$trace.debug('vor update isr$session', STB$SECURITY.getCurrentSession , sCurrentName);
  update isr$session set port = nClientPort where sessionid = STB$SECURITY.getCurrentSession;

-- here send dbToken to loader
  oErrorObj := ISR$LOADER.sendDbToken(STB$SECURITY.getClientIp, nClientPort);
  isr$trace.debug('oErrorObj', oErrorObj.sSeverityCode, sCurrentName, oErrorObj);
  if oErrorObj.sSeverityCode = Stb$typedef.cnSeverityCritical then
    raise exSendTokenFunction;
  end if;

  commit;

  ISR$TREE$PACKAGE.sendSyncCall('CURRENTCONNECTIONS', STB$SECURITY.getCurrentSession);

  -- check for homeless jobs
  update stb$jobsession
     set sessionid = STB$SECURITY.getCurrentSession
   where ip = STB$SECURITY.getClientIp
     and port = STB$SECURITY.getClientPort
     and sessionid != STB$SECURITY.getCurrentSession
     and sessionid not in (select sessionid
                             from isr$session
                            where markedforshutdown = 'F'
                              and ip = STB$SECURITY.getClientIp
                              and port = STB$SECURITY.getClientPort);

  update stb$jobsession
     set sessionid = STB$SECURITY.getCurrentSession
   where ip is null
     and port is null
     and sessionid != STB$SECURITY.getCurrentSession
     and sessionid in (select sessionid
                         from isr$session
                        where markedforshutdown = 'T'
                          and ip = STB$SECURITY.getClientIp
                          --AND port = STB$SECURITY.getClientPort
                          and UPPER(username) = UPPER(sOracleUserName));

  commit;

  FOR rGetAdoptedJobs IN (select js.ip, js.port, jq.jobid
                            from stb$jobsession js, stb$jobqueue jq, user_scheduler_jobs sj
                           where sessionid = STB$SECURITY.getCurrentSession
                             and js.ip = STB$SECURITY.getClientIp
                             and js.port = STB$SECURITY.getClientPort
                             and js.jobid = jq.jobid
                             and jq.oraclejobid = sj.job_name
                             and sj.state = 'RUNNING'
                             and jq.progress > 0) LOOP
    sReturn := ISR$LOADER.startJob(rGetAdoptedJobs.ip, rGetAdoptedJobs.port, STB$UTIL.getSystemParameter('LOADER_CONTEXT'),
          STB$UTIL.getSystemParameter('LOADER_NAMESPACE'), rGetAdoptedJobs.jobid, STB$JOB.getJobParameter('FOLDERNAME', rGetAdoptedJobs.jobid), 'T',
          STB$UTIL.getSystemParameter('TIMEOUT_LOADER_THREAD'));
  END LOOP;

  for rGetLostConnections in (select sessionid
                                from isr$session
                               where (markedforshutdown = 'T' or inactive = 'T' or port is null)  --ISRC-988
                                 and sessionid not in (select sessionid from stb$jobsession)
                                 and sessionid != STB$SECURITY.getCurrentSession) loop
    isr$trace.debug('delete lost connection', 'rGetLostConnections.sessionid: '||rGetLostConnections.sessionid, sCurrentName);
    STB$GAL.deleteSessionInfo (rGetLostConnections.sessionid);
  end loop;
  for rGetConnections in (select ip, port, sessionid
                                from isr$session
                               where sessionid not in (select sessionid from stb$jobsession)
                                 and sessionid != STB$SECURITY.getCurrentSession) loop
    sReturn := STB$ACTION.isDocServerAvailable(rGetConnections.ip, rGetConnections.port);
    if sReturn != 'T' then
      isr$trace.debug('delete lost connection', 'rGetConnections.sessionid: '||rGetConnections.sessionid, sCurrentName);
      STB$GAL.deleteSessionInfo (rGetConnections.sessionid);
    end if;
  end loop;  --ISRC-988
  isr$trace.stat('end', 'end', sCurrentName);
  return oErrorObj;
exception
  when exSendTokenFunction then
    rollback;
    sUserMsg := utd$msglib.getmsg ('exSendTokenFunction', stb$security.getCurrentLanguage,  csP1 => sqlerrm(sqlcode));
    sImplMsg := sCurrentName || rtrim(': '||sImplMsg,': ');
    sDevMsg  := dbms_utility.format_error_stack ||stb$typedef.crlf|| dbms_utility.format_error_backtrace;
    oErrorObj.handleError ( Stb$typedef.cnSeverityCritical, sUserMsg, sImplMsg, sDevMsg );
  return oErrorObj;
  when others then
    rollback;
    sUserMsg := utd$msglib.getmsg ('exUnexpectedError', stb$security.getCurrentLanguage,  csP1 => sqlerrm(sqlcode));
    sImplMsg := sCurrentName || rtrim(': '||sImplMsg,': ');
    sDevMsg  := dbms_utility.format_error_stack ||stb$typedef.crlf|| dbms_utility.format_error_backtrace;
    oErrorObj.handleError ( Stb$typedef.cnSeverityCritical, sUserMsg, sImplMsg, sDevMsg );
  return oErrorObj;
END setClientPort;

--******************************************************************************
function getParameterValue( sParameter in varchar2 )
  return varchar2
is
  sCurrentName     constant varchar2(100) := $$PLSQL_UNIT||'.getParameterValue('||sParameter||')';
  oErrorObj         stb$oerror$record      := STB$OERROR$RECORD();
  sParameterValue   VARCHAR2(4000);
  nStartTimeNum     number      := DBMS_UTILITY.GET_TIME;

  cursor cGetMaxJarfileVersion is
    select MAX (jarfileversion)
      from (select jarfileversion
              from isr$installtable
            order by completedon desc);
begin
  isr$trace.stat('begin', 'sParameter: '||sParameter, sCurrentName);
  if sParameter = 'ISR_MODULES' then
    sParameterValue := ISR$LICENCEFILE.getLicensedModules;
  elsif sParameter = 'ISR_JARFILES' then
    sParameterValue := sParameterValue||'<table width="100%">';
    sParameterValue := sParameterValue||'<tr>';
    sParameterValue := sParameterValue||'<th>';
    sParameterValue := sParameterValue|| UTD$MSGLIB.getMsg('Jarfile', STB$SECURITY.getCurrentLanguage);
    sParameterValue := sParameterValue||'</th>';
    sParameterValue := sParameterValue||'<th>';
    sParameterValue := sParameterValue|| UTD$MSGLIB.getMsg('Jarfileversion', STB$SECURITY.getCurrentLanguage);
    sParameterValue := sParameterValue||'</th>';
    sParameterValue := sParameterValue||'<th>';
    sParameterValue := sParameterValue|| UTD$MSGLIB.getMsg('Installdate', STB$SECURITY.getCurrentLanguage);
    sParameterValue := sParameterValue||'</th>';
    sParameterValue := sParameterValue||'</tr>';
    FOR rGetData IN ( select jarfile col1, jarfileversion col2, TO_CHAR (install_date, isr$util.getInternalDate) as col3, install_date
                        from (select jarfile, jarfileversion, max(loadedon) as install_date
                                from isr$installtable
                                group by jarfile, jarfileversion)
                        order by install_date desc) LOOP
      sParameterValue := sParameterValue||'<tr>';
      sParameterValue := sParameterValue||'<td>';
      sParameterValue := sParameterValue|| rGetData.col1;
      sParameterValue := sParameterValue||'</td>';
      sParameterValue := sParameterValue||'<td>';
      sParameterValue := sParameterValue|| rGetData.col2;
      sParameterValue := sParameterValue||'</td>';
      sParameterValue := sParameterValue||'<td>';
      sParameterValue := sParameterValue|| rGetData.col3;
      sParameterValue := sParameterValue||'</td>';
      sParameterValue := sParameterValue||'</tr>';
    END LOOP;
    sParameterValue := sParameterValue||'</table>';
  elsif sParameter = 'ISR_MAXJARFILEVERSION' then
    OPEN cGetMaxJarfileVersion;
    FETCH cGetMaxJarfileVersion INTO sParameterValue;
    CLOSE cGetMaxJarfileVersion;
  elsif sParameter = 'IP_ADDRESS' then
      sParameterValue := stb$security.getClientIP;
  else
    --sParameterValue := STB$USERSECURITY.getUserParameter(sParameter, STB$SECURITY.getCurrentUser);
    --IF TRIM(sParameterValue) IS NULL THEN
      sParameterValue := STB$UTIL.getSystemParameter(sParameter);
    --END IF;
  end if;
  isr$trace.stat('end', 'end', sCurrentName);
  return sParameterValue;
end getParameterValue;

--******************************************************************************
function getIcon( sSize in varchar2, sIconid in varchar2 )
  return blob
is
  pragma autonomous_transaction;
  sCurrentName constant varchar2(100) := $$PLSQL_UNIT||'.getIcon('||sIconid||')';
  oErrorObj    stb$oerror$record      := STB$OERROR$RECORD();
  blIcon       blob;
  nStartTimeNum number      := DBMS_UTILITY.GET_TIME;

  cursor cGetIcon is
    select icon
      from ISR$ICON
     where iconid = sIconid
       and iconsize = sSize;
begin
  isr$trace.debug_all('begin', 'sSize: '||sSize||'  sIconid: '||sIconid, sCurrentName);
  open cGetIcon;
  fetch cGetIcon into blIcon;
  if cGetIcon%NOTFOUND then
    insert into ISR$ICON (ICONID, ICONSIZE, SYSTEMICON, USED)
    values (sIconid, sSize, 'T', 'T');
    commit;
  else
    update isr$icon set used = 'T' where iconid = sIconId and NVL(used, 'F') = 'F';
    commit;
  end if;
  close cGetIcon;
  isr$trace.debug_all('end', 'end', sCurrentName);
  return blIcon;
end getIcon;

--******************************************************************************
function checkReload(reload out varchar2)
  return STB$OERROR$RECORD
is
  oErrorObj     STB$OERROR$RECORD := STB$OERROR$RECORD();
begin
  /* not used */
  reload := 'F';
  return oErrorObj;
end checkReload;

--******************************************************************************
procedure setApplicationInfo is
sCurrentName   constant varchar2(4000) := $$PLSQL_UNIT||'.setApplicationInfo';
begin
  isr$trace.stat('begin', 'begin', sCurrentName);
  -- system info
  dbms_application_info.set_module( STB$UTIL.GETSYSTEMPARAMETER('ISR_NAME')||' '||
                                    STB$UTIL.GETSYSTEMPARAMETER('ISR_SYSTEM')||' '||
                                    STB$UTIL.GETSYSTEMPARAMETER('ISR_VERSION')||' '||
                                    STB$UTIL.GETSYSTEMPARAMETER('ISR_SERVICEPACK'),
                                    STB$UTIL.GETSYSTEMPARAMETER('SYSTEM_OID'));
  -- client info
  dbms_application_info.set_client_info(sOracleUserName||' '||STB$SECURITY.getClientIp);
  isr$trace.stat('end', 'end', sCurrentName);
end setApplicationInfo;

--*****************************************************************************************************
function getXML( oParameter in  STB$MENUENTRY$RECORD,
                 clXML      out clob,
                 clParamXml in  clob default null)
  return STB$OERROR$RECORD
is
  sCurrentName constant varchar2(100) := $$PLSQL_UNIT||'.getXML ('||oParameter.sToken||')';
  oErrorObj    stb$oerror$record      := STB$OERROR$RECORD();
  sUserMsg     varchar2(4000);
  sDevMsg      varchar2(4000);
  sImplMsg     varchar2(4000);
  nStartTimeNum number      := DBMS_UTILITY.GET_TIME;
  sPackageGetXML  varchar2(200);
begin
  isr$trace.stat('begin','begin', sCurrentName);

  -- get the package that handles the request
  if Trim(oParameter.sHandledBy) is null then
    sPackageGetXML := Stb$object.getCurrentNodePackage;
  else
    sPackageGetXML := oParameter.sHandledBy;
  end if;
  isr$trace.info('sPackageGetXML set', sPackageGetXML ,sCurrentName);


  if TRIM(sPackageGetXML) is not null then
    execute immediate
       'BEGIN
          :1 :='||sPackageGetXML||'.getXML(:2, :3, :4);
        END;'
      using out oErrorObj, in oParameter, out clXML, in clParamXml;
  end if;

  isr$trace.stat('end', 'elapsed time: '||isr$util.getElapsedTime(nStartTimeNum), sCurrentName, clXML);
  return (oErrorObj);
exception
 when others then
   rollback;
    sUserMsg := utd$msglib.getmsg ('exUnexpectedError', stb$security.getCurrentLanguage,  csP1 => sqlerrm(sqlcode));
    sImplMsg := sCurrentName || rtrim(': '||sImplMsg,': ');
    sDevMsg  := substr(dbms_utility.format_error_stack ||stb$typedef.crlf|| dbms_utility.format_error_backtrace, 1,4000);
    oErrorObj.handleError ( Stb$typedef.cnSeverityCritical, sUserMsg, sImplMsg, sDevMsg );
    return oErrorObj;
end getXML;

--*****************************************************************************************************
function putXML( oParameter in STB$MENUENTRY$RECORD,
                 clXML      in clob )
  return STB$OERROR$RECORD
is
  sCurrentName constant varchar2(100) := $$PLSQL_UNIT||'.putXML ('||oParameter.sToken||')';
  oErrorObj    stb$oerror$record      := STB$OERROR$RECORD();
  sUserMsg     varchar2(4000);
  sDevMsg      varchar2(4000);
  sImplMsg     varchar2(4000);
  nStartTimeNum number      := DBMS_UTILITY.GET_TIME;
begin
  isr$trace.stat('begin', 'begin', sCurrentName);
  isr$trace.debug('status','clXML: see column LOGCLOB', sCurrentName, clXML);

  -- get the package that handles the request
  if Trim(oParameter.sHandledBy) is null then
    sPackage := Stb$object.getCurrentNodePackage;
  else
    sPackage := oParameter.sHandledBy;
  end if;
  isr$trace.info('sPackage set', sPackage, sCurrentName);

  if trim(sPackage) is not null then
    execute immediate
       'BEGIN
          :1 :='||sPackage||'.putXML(:2, :3);
        END;'
      using out oErrorObj, in oParameter, in clXML;
  end if;

  isr$trace.stat('end', 'end', sCurrentName);
  return (oErrorObj);
exception
 when others then
   rollback;
    sUserMsg := utd$msglib.getmsg ('exUnexpectedError', stb$security.getCurrentLanguage,  csP1 => sqlerrm(sqlcode));
    sImplMsg := sCurrentName || rtrim(': '||sImplMsg,': ');
    sDevMsg  := substr(dbms_utility.format_error_stack ||stb$typedef.crlf|| dbms_utility.format_error_backtrace, 1,4000);
    oErrorObj.handleError ( Stb$typedef.cnSeverityCritical, sUserMsg, sImplMsg, sDevMsg );
    return oErrorObj;
end putXML;

--*****************************************************************************************************
FUNCTION getJavaLogLevel RETURN VARCHAR2
IS
BEGIN
  RETURN ISR$TRACE.getJavaUserLoglevelStr;
END getJavaLogLevel;


--*****************************************************************************************************
function getNodeDetails(  clXML      out clob) return STB$OERROR$RECORD
is
  sCurrentName constant varchar2(100) := $$PLSQL_UNIT||'.getNodeDetails';
  oErrorObj    stb$oerror$record      := STB$OERROR$RECORD();
  sUserMsg     varchar2(4000);
  nStartTimeNum number      := DBMS_UTILITY.GET_TIME;
begin
  isr$trace.stat('begin', 'begin', sCurrentName);

  sPackage := Stb$object.getCurrentNodePackage;
  isr$trace.info('sPackage set', sPackage, sCurrentName);


  sMethodExists := STB$UTIL.CHECKMETHODEXISTS('getNodeDetails', sPackage);
  if sMethodExists = 'F' then    -- [ISRC-233]
    sMethodExists := stb$util.checkObjectExists('getNodeDetails', sPackage);
  end if;

  isr$trace.debug('metod getNodeDetails in ' || sPackage, 'sMethodExists = ' || sMethodExists, sCurrentName);

  if sMethodExists = 'T'  and trim(sPackage) is not null then
    execute immediate
       'BEGIN
          :1 :='||sPackage||'.getNodeDetails(:2);
        END;'
      using out oErrorObj, out clXML;
  end if;


  isr$trace.stat('end', 'clXML, see column LOGCLOB', sCurrentName, clXML);
  return (oErrorObj);
exception
 when others then
    if sqlcode = -6550 then   --PL/SQL compilation error
      oErrorObj.sseveritycode := Stb$typedef.cnNoError;
      return oErrorObj;
    end if;
  rollback;
  isr$trace.error('sqlcode','sqlcode: '||sqlcode, sCurrentName);
  sUserMsg := utd$msglib.getmsg ('exUnexpectedError', stb$security.getCurrentLanguage, sqlerrm(sqlcode));
  oErrorObj.handleError ( Stb$typedef.cnSeverityCritical, sUserMsg, sCurrentName, substr(dbms_utility.format_error_stack ||stb$typedef.crlf|| dbms_utility.format_error_backtrace, 1,4000) );
  return oErrorObj;
end getNodeDetails;

--*****************************************************************************************************
function setLoaderConnection (csPort in varchar2, csUserName in varchar2, csStart in varchar2 default 'T')
   return STB$OERROR$RECORD
is
   oErrorObj      STB$OERROR$RECORD := STB$OERROR$RECORD ();
   sCurrentName        constant varchar2 (100) := $$PLSQL_UNIT || '.setLoaderConnection(' || csUserName || ')';
   sUserMsg            varchar2 (4000);
   exNoInsertSession   exception;
   nStartTimeNum       number                  := DBMS_UTILITY.GET_TIME;
begin
   isr$trace.stat ('begin', 'csPort: ' || csPort || '  csUserName: ' || csUserName || '  csStart: ' || csStart, sCurrentName);

   if csStart = 'T' then
      ISR$ACTION$LOG.setActionLog ('LOGON', 'Loader of user ' || csUserName || ' with port ' || csPort);

      oErrorObj := insertSessionInfo (csUserName);

      Stb$security.initUser('F');
      stb$gal.updateSessionUser;

      if oErrorObj.sSeverityCode = stb$typedef.cnSeverityCritical then
        sUserMsg := utd$msglib.getmsg ('exNoInsertSession', stb$security.getCurrentLanguage, SQLERRM (SQLCODE));
        raise exNoInsertSession;
      end if;
      oErrorObj := setClientPort (TO_NUMBER (csPort));
   else
      -- is, 09. Mar 2016: deleteSessionInfo only by terminate session
      for rGetSession in (select sessionid, userno
                            from ISR$SESSION
                           where port = csPort and UPPER (username) = UPPER (csUserName))
      loop
         deleteSessionInfo (rGetSession.sessionid, rGetSession.userno);
      end loop;

      ISR$ACTION$LOG.setActionLog ('SHUTDOWN', 'Loader of user ' || csUserName || ' with port ' || csPort);
   end if;

   isr$trace.debug('runtime', 'elapsed time: '||isr$util.getElapsedTime(nStartTimeNum), sCurrentName);
   isr$trace.stat ('end', 'end', sCurrentName);
   return oErrorObj;
exception
   when others then
      sUserMsg := utd$msglib.getmsg ('exUnexpectedError', stb$security.getCurrentLanguage, sUserMsg, SQLERRM (SQLCODE));
      oErrorObj.handleError (Stb$typedef.cnSeverityCritical,
                             sUserMsg,
                             sCurrentName,
                             DBMS_UTILITY.format_error_stack || stb$typedef.crlf || DBMS_UTILITY.format_error_backtrace);
      return oErrorObj;
end setLoaderConnection;

FUNCTION getRecordTimestamp( csTableName in varchar2, csTableKey in varchar2, csKeyValue in varchar2, sModifiedOn out varchar2)  RETURN STB$OERROR$RECORD is
 oErrorObj      STB$OERROR$RECORD := STB$OERROR$RECORD ();
 sCurrentName        constant varchar2 (200) := $$PLSQL_UNIT || '.getRecordTimestamp(' || csTableName || ')';
 sUserMsg            varchar2 (4000);
 sStmt      varchar2(2000);
begin
  isr$trace.stat ('begin', csTableKey || ' ' || csKeyValue, sCurrentName);
  sStmt := 'select to_char(modifiedon,isr$util.getInternalDate) from ' || csTableName || ' where lower(' || csTableKey || ') = lower(''' || csKeyValue || ''')';
  isr$trace.debug ('sStmt', sStmt, sCurrentName);
  execute immediate sStmt into sModifiedOn;
  isr$trace.stat ('end', 'dModifiedOn ' || sModifiedOn, sCurrentName);
  return oErrorObj;
exception
   when no_data_found then
    isr$trace.warn('no_data_found', sStmt, sCurrentName);
    return oErrorObj;
   when others then
      sUserMsg := utd$msglib.getmsg ('exUnexpectedError', stb$security.getCurrentLanguage, sStmt, SQLERRM (SQLCODE));
      oErrorObj.handleError (Stb$typedef.cnSeverityCritical,
                             sUserMsg,
                             sCurrentName,
                             DBMS_UTILITY.format_error_stack || stb$typedef.crlf || DBMS_UTILITY.format_error_backtrace);
      return oErrorObj;
end getRecordTimestamp;

function synchronizeInBackground(sToken out varchar2, olTreeReloadList out STB$TREENODELIST, oTreeReloadNode out STB$TREENODE$RECORD, csNodeType in VARCHAR2, csNodeId in VARCHAR2, csSessionId in VARCHAR2)
return STB$OERROR$RECORD
is
  sCurrentName   constant varchar2(255) := $$PLSQL_UNIT||'.synchronizeInBackground(csNodeType=>'||csNodeType||', csNodeId=>'||csNodeId||', csSessionId=>'||csSessionId||')';
  oErrorObj      STB$OERROR$RECORD;
begin
  isr$trace.stat('begin', 'begin', sCurrentName);
  oErrorObj := isr$tree$package.synchronizeInBackground(sToken, olTreeReloadList, oTreeReloadNode, csNodeType, csNodeId, csSessionId);
  isr$trace.stat ('end', 'end', sCurrentName);
  return oErrorObj;
end synchronizeInBackground;

begin
--*********************************************************************************************************************************
  -- Initalize of the Oracle-User, user Id and Language
  Stb$security.initUser('T');
  setApplicationInfo;
--***********************************************************
end STB$GAL;