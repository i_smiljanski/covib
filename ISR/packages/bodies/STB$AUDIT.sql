CREATE OR REPLACE PACKAGE BODY STB$AUDIT
is
   nCurrentAudit            number;
   xAuditXml                xmltype;  -- the dom for the current audit
   sModified                varchar2(1) :='F';
   nCurrentLanguage         number      := stb$security.getcurrentlanguage;
   crlf                     constant varchar2(2) := chr(10)||chr(13);

--*******************************************************************************************************************************************
function getAudit(csAuditType in varchar2, csReference in varchar2)
  return xmltype
is
  sCurrentName constant varchar2(100) := $$PLSQL_UNIT||'.getAudit';
  sUserMsg     varchar2(4000);
  sImplMsg     varchar2(4000);
  sDevMsg      varchar2(4000);
  oErrorObj    STB$OERROR$RECORD := STB$OERROR$RECORD();
  xXml         xmltype;

  cursor cAudit is
    select ISR$XML.clobToXml (audittext)
    from STB$AUDITTRAIL aud
   where aud.reference = csReference
     and aud.audittype = csAuditType
     and checksum != 0
   /* in case there is more than one audit */
   order by audittimestamp desc;

begin
  isr$trace.stat('begin','csReference= '||csReference||' csAuditType= ' || csAuditType,sCurrentName);

  -- moved the query to a named cursor to avoid no_data_found error
  open cAudit;
  fetch cAudit into xXml;
  close cAudit;

  isr$trace.stat('end', 'FUNCTION '||sCurrentName||' finished',sCurrentName);
  return xXml;

exception
  when others then
    sUserMsg := utd$msglib.getmsg ('exUnexpectedError', nCurrentLanguage,  csP1 => sqlerrm(sqlcode));
    sImplMsg := sCurrentName || rtrim(': '||sImplMsg,': ');
    sDevMsg  := substr(dbms_utility.format_error_stack ||crlf|| dbms_utility.format_error_backtrace, 1,4000);
    oErrorObj.handleError ( Stb$typedef.cnSeverityCritical, sUserMsg, sImplMsg, sDevMsg );
    return null;
end getAudit;

--*******************************************************************************************************************************************
function CompareObjects (oOldSelection in ISR$TLRSELECTION$LIST,oNewSelection in ISR$TLRSELECTION$LIST, sModifiedFlag out varchar2, sEntity in varchar2)
  return STB$OERROR$RECORD
is
  sCurrentName                constant varchar2(100) := $$PLSQL_UNIT||'.CompareObjects';
  sUserMsg                    varchar2(4000);
  sImplMsg                    varchar2(4000);
  sDevMsg                     varchar2(4000);
  oErrorObj                   STB$OERROR$RECORD := STB$OERROR$RECORD();
  sFLagDeleted                varchar2(1) :='F';
  sFlagInserted               varchar2(1) :='F';
  nCounterDeleted             number :=0;
  nCounterInserted            number :=0;
  nCounterModified            number :=0;
  tlOrderNumberListe          STB$TYPEDEF.tlOrderNumber;
  nIndex                      number;
  sOldFlag                    varchar2(1) :='F';
  sNewFlag                    varchar2(1) :='F';

  nCount1                     NUMBER;
  nCount1A                    NUMBER;
  sComp1                      varchar2(4000);
  sComp2                      varchar2(4000);
  sComp1A                     varchar2(4000);
  sComp2A                     varchar2(4000);
  sMaster2                    varchar2(4000);
  sMaster2A                   varchar2(4000);
  -- ATTENTION  JB 20.03.2004:
  -- it is nessary to compare strings , cause the try to compare object elements failed even when the lements have been trimed before

begin
  isr$trace.stat('begin', 'FUNCTION '||sCurrentName||' started',sCurrentName);
  --intialize  OutParameter
  sModifiedFlag :='F';

  -- check if both object lists are holding data
  if oNewSelection.Count()>=1 then
    sNewFlag :='T';
      --there are values in the new record
      isr$trace.debug('oNewSelection.Count()','oNewSelection.Count(): '|| oNewSelection.Count(), sCurrentName);
  else
    -- No values in therecord
      isr$trace.debug('NoValues in oOldSelection','NoValues in oOldSelection', sCurrentName);
  end if;

  if oOldSelection.Count()>=1 then
    sOldFlag :='T';
     --there are values in the old record
    isr$trace.debug('oOldSelection.Count()','oOldSelection.Count(): '|| oOldSelection.Count(), sCurrentName);
  else
    -- No values in therecord
     isr$trace.debug('NoValues in oOldSelection','NoValues in oOldSelection' , sCurrentName);
  end if;

  isr$trace.debug('sOldFlag','sOldFlag : '|| sOldFlag  ||'  sNewFlag : '|| sNewFlag, sCurrentName);

  -- if both records hold values they can be compared
  if sNewFlag='T' and sOldFlag='T' then

-- get the deleted values   and the modified order-numbers
    nIndex :=0;
    for i in 1..oOldSelection.Count() loop
      sComp1 := oOldSelection(i).sDisplay;
      sComp2 := oOldSelection(i).sValue;
      sMaster2 := oOldSelection(i).sMasterKey;
      nCount1 := oOldSelection(i).nOrderNum;

      for j in 1..oNewSelection.Count() loop
        sComp1A := oNewSelection(j).sDisplay;
        sComp2A := oNewSelection(j).sValue;
        sMaster2A := oNewSelection(j).sMasterKey;
        nCount1A := oNewSelection(j).nOrderNum;

        if ((trim(sComp1) =trim(sComp1A))
        and (trim(sComp2) =trim(sComp2A))
        and (trim(sMaster2)= trim(sMaster2A)))  then

           -- check order numbers
          if(nCount1 != nCount1A) then
            nIndex := nIndex +1;
            tlOrderNUMBERListe(nIndex).oldOrderNUMBER := oOldSelection(i).nOrderNum;
            tlOrderNUMBERListe(nIndex).newOrderNUMBER := oNewSelection(j).nOrderNum;
            tlOrderNUMBERListe(nIndex).sValue := oOldSelection(i).sValue;
            tlOrderNUMBERListe(nIndex).sDisplay := oOldSelection(i).sDisplay;
            tlOrderNUMBERListe(nIndex).sMasterKey := oOldSelection(i).sMasterKey;
          end if;

          sFlagDeleted :='F';
          exit;
        else
          sFlagDeleted :='T';
        end if;
      end loop;

      if sFlagDeleted='T' then
        nCounterDeleted :=nCounterDeleted+1;

        isr$trace.debug( nCounterDeleted,oOldSelection(i).sValue || ' ' || sEntity, sCurrentName);

        sModifiedFlag :='T';

        ISR$XML.CreateNode(xAuditXml, '/AUDITFILE/AUDITENTRY[@ENTITY="' || sEntity || '"]', 'DELETED',' ');
        ISR$XML.CreateNode(xAuditXml, '/AUDITFILE/AUDITENTRY[@ENTITY="' || sEntity || '"]/DELETED[position()=last()]', 'MASTERKEY', oOldSelection(i).sMasterKey);
        ISR$XML.CreateNode(xAuditXml, '/AUDITFILE/AUDITENTRY[@ENTITY="' || sEntity || '"]/DELETED[position()=last()]', 'KEY', oOldSelection(i).sValue);
        ISR$XML.CreateNode(xAuditXml, '/AUDITFILE/AUDITENTRY[@ENTITY="' || sEntity || '"]/DELETED[position()=last()]', 'DISPLAY', oOldSelection(i).sDisplay);
      end if;
    end loop;

    isr$trace.debug('deleted records','number of deleted records: '|| nCounterDeleted, sCurrentName);

    -- get the inserted values
    for i in 1..oNewSelection.Count() loop
      sComp1A := oNewSelection(i).sDisplay;
      sComp2A := oNewSelection(i).sValue;
      sMaster2A :=oNewSelection(i).sMasterKey;

      for j in 1..oOldSelection.Count() loop
        sComp1 := oOldSelection(j).sDisplay;
        sComp2 := oOldSelection(j).sValue;
        sMaster2 :=oOldSelection(j).sMasterKey;

        if ((trim(sComp1) =trim(sComp1A)) and
              (trim(sComp2) =trim(sComp2A)) and
        (trim(sMaster2)= trim(sMaster2A)))  then

          sFlagInserted := 'F';
          exit;
        else
          sFlagInserted := 'T';
        end if;
      end loop;
      if sFlagInserted = 'T'  then
        nCounterInserted :=nCounterInserted+1;
        isr$trace.debug( nCounterInserted, oNewSelection(i).sValue || ' ' || sEntity, sCurrentName);

        sModifiedFlag :='T';

        ISR$XML.CreateNode(xAuditXml, '/AUDITFILE/AUDITENTRY[@ENTITY="' || sEntity || '"]', 'INSERTED','');
        ISR$XML.CreateNode(xAuditXml, '/AUDITFILE/AUDITENTRY[@ENTITY="' || sEntity || '"]/INSERTED[position()=last()]', 'MASTERKEY', oNewSelection(i).sMasterKey);
        ISR$XML.CreateNode(xAuditXml, '/AUDITFILE/AUDITENTRY[@ENTITY="' || sEntity || '"]/INSERTED[position()=last()]', 'KEY', oNewSelection(i).sValue);
        ISR$XML.CreateNode(xAuditXml, '/AUDITFILE/AUDITENTRY[@ENTITY="' || sEntity || '"]/INSERTED[position()=last()]', 'DISPLAY', oNewSelection(i).sDisplay);
      end if;
    end loop;
    isr$trace.debug('inserted records','number of inserted records: '|| nCounterInserted, sCurrentName);

    -- get the modified values
    -- the orderNUMBERS have been determined in the deleted loop s.a.
    for i in 1..tlOrderNUMBERListe.Count() loop
      nCounterModified :=nCounterModified+1;

      sModifiedFlag :='T';

      ISR$XML.CreateNode(xAuditXml, '/AUDITFILE/AUDITENTRY[@ENTITY="' || sEntity || '"]', 'REORDERED', '');
      ISR$XML.CreateNode(xAuditXml, '/AUDITFILE/AUDITENTRY[@ENTITY="' || sEntity || '"]/REORDERED[position()=last()]', 'MASTERKEY', tlOrderNUMBERListe(i).sMasterKey);
      ISR$XML.CreateNode(xAuditXml, '/AUDITFILE/AUDITENTRY[@ENTITY="' || sEntity || '"]/REORDERED[position()=last()]', 'KEY', tlOrderNUMBERListe(i).sValue);
      ISR$XML.CreateNode(xAuditXml, '/AUDITFILE/AUDITENTRY[@ENTITY="' || sEntity || '"]/REORDERED[position()=last()]', 'DISPLAY', tlOrderNUMBERListe(i).sDisplay);
      ISR$XML.CreateNode(xAuditXml, '/AUDITFILE/AUDITENTRY[@ENTITY="' || sEntity || '"]/REORDERED[position()=last()]', 'OLD', tlOrderNUMBERListe(i).oldOrderNUMBER);
      ISR$XML.CreateNode(xAuditXml, '/AUDITFILE/AUDITENTRY[@ENTITY="' || sEntity || '"]/REORDERED[position()=last()]', 'NEW', tlOrderNUMBERListe(i).newOrderNUMBER);
    end loop;
  else
    if sNewFlag='T' and sOldFlag='F' then
      -- all values are new
      -- get the inserted values
      for i in 1..oNewSelection.Count() loop
        nCounterInserted :=nCounterInserted+1;

        sModifiedFlag :='T';

        ISR$XML.CreateNode(xAuditXml, '/AUDITFILE/AUDITENTRY[@ENTITY="' || sEntity || '"]', 'INSERTED','');
        ISR$XML.CreateNode(xAuditXml, '/AUDITFILE/AUDITENTRY[@ENTITY="' || sEntity || '"]/INSERTED[position()=last()]', 'MASTERKEY', oNewSelection(i).sMasterKey);
        ISR$XML.CreateNode(xAuditXml, '/AUDITFILE/AUDITENTRY[@ENTITY="' || sEntity || '"]/INSERTED[position()=last()]', 'KEY', oNewSelection(i).sValue);
        ISR$XML.CreateNode(xAuditXml, '/AUDITFILE/AUDITENTRY[@ENTITY="' || sEntity || '"]/INSERTED[position()=last()]', 'DISPLAY', oNewSelection(i).sDisplay);
      end loop;
      isr$trace.debug('inserted records','number of inserted records: '|| nCounterInserted, sCurrentName);
    end if;

    if sNewFlag='F' and sOldFlag='T' then
      -- all values have been deleted
      for i in 1..oOldSelection.Count() loop
        nCounterDeleted :=nCounterDeleted+1;

        sModifiedFlag :='T';

        ISR$XML.CreateNode(xAuditXml, '/AUDITFILE/AUDITENTRY[@ENTITY="' || sEntity || '"]', 'DELETED','');
        ISR$XML.CreateNode(xAuditXml, '/AUDITFILE/AUDITENTRY[@ENTITY="' || sEntity || '"]/DELETED[position()=last()]', 'MASTERKEY', oOldSelection(i).sMasterKey);
        ISR$XML.CreateNode(xAuditXml, '/AUDITFILE/AUDITENTRY[@ENTITY="' || sEntity || '"]/DELETED[position()=last()]', 'KEY', oOldSelection(i).sValue);
        ISR$XML.CreateNode(xAuditXml, '/AUDITFILE/AUDITENTRY[@ENTITY="' || sEntity || '"]/DELETED[position()=last()]', 'DISPLAY', oOldSelection(i).sDisplay);
      end loop;

     isr$trace.debug('deleted records','Number of deleted records: '|| nCounterDeleted, sCurrentName);
    end if;
  end if;

  isr$trace.debug('status sModifiedFlag','sModifiedFlag: ' || sModifiedFlag, sCurrentName);
  isr$trace.stat('end', 'FUNCTION '||sCurrentName||' finished',sCurrentName);
  return oErrorObj;

exception
  when others then
    sUserMsg := utd$msglib.getmsg ('exUnexpectedError', nCurrentLanguage,  csP1 => sqlerrm(sqlcode));
    sImplMsg := sCurrentName || rtrim(': '||sImplMsg,': ');
    sDevMsg  := substr(dbms_utility.format_error_stack ||crlf|| dbms_utility.format_error_backtrace, 1,4000);
    oErrorObj.handleError ( Stb$typedef.cnSeverityCritical, sUserMsg, sImplMsg, sDevMsg );
    return oErrorObj;
end CompareObjects;

--*****************************************************************************************************************************
function CreateNewAuditRecord( sReference in varchar2, csAuditType in varchar2)
return STB$OERROR$RECORD
is
  sCurrentName constant varchar2(100) := $$PLSQL_UNIT||'.CreateNewAuditRecord';
  sUserMsg     varchar2(4000);
  sImplMsg     varchar2(4000);
  sDevMsg      varchar2(4000);
  oErrorObj    stb$oerror$record      := STB$OERROR$RECORD();
  nAuditId     number;
begin
  isr$trace.stat('begin', 'FUNCTION '||sCurrentName||' started',sCurrentName);
  isr$trace.debug('new audit record', 'sReference : ' || sReference || ' / csAuditType: ' || csAuditType , sCurrentName);

  select SEQ$STB$AUDITTRAIL.NextVal into nAuditID from dual;

  insert into STB$AUDITTRAIL
    ( AUDITID, REFERENCE, USERNO, AUDITTIMESTAMP, CHECKSUM, AUDITTEXT ,AUDITTYPE)
  values
    ( nAuditId, sReference , STB$SECURITY.getCurrentUser, sysdate, 0, empty_clob(), csAuditType);

--  set dummy variables in the package to have them for further calls
  nCurrentAudit := nAuditId;
  sModified     :='F';

  isr$trace.debug('status nAuditID','nAuditID: ' || nAuditID, sCurrentName);
  isr$trace.stat('end', 'FUNCTION '||sCurrentName||' finished',sCurrentName);
  return oErrorObj;
exception
  WHEN others then
    sUserMsg := utd$msglib.getmsg ('exUnexpectedError', nCurrentLanguage,  csP1 => sqlerrm(sqlcode));
    sImplMsg := sCurrentName || rtrim(': '||sImplMsg,': ');
    sDevMsg  := substr(dbms_utility.format_error_stack ||crlf|| dbms_utility.format_error_backtrace, 1,4000);
    oErrorObj.handleError ( Stb$typedef.cnSeverityCritical, sUserMsg, sImplMsg, sDevMsg );
    return oErrorObj;
end CreateNewAuditRecord;

--********************************************************************************************************************
function putAuditMask(olParameter in STB$PROPERTY$LIST)
  return STB$OERROR$RECORD
is
  sCurrentName constant varchar2(100) := $$PLSQL_UNIT||'.putAuditMask';
  sUserMsg     varchar2(4000);
  sImplMsg     varchar2(4000);
  sDevMsg      varchar2(4000);
  oErrorObj    stb$oerror$record      := STB$OERROR$RECORD();
begin
  isr$trace.stat('begin', 'FUNCTION '||sCurrentName||' started',sCurrentName);
  isr$trace.debug('status nAuditID','nAuditID: '|| nCurrentAudit, sCurrentName);

  -- Write the reason in the dom
  ISR$XML.CreateNode(xAuditXml, '/AUDITFILE/AUDIT[@NUMBER='||ISR$XML.valueOf(xAuditXml, 'count(/AUDITFILE/AUDIT)')|| ']', 'REASON', olParameter(1).sValue);
  -- ... and also for the report audit
  ISR$XML.CreateNode(xAuditXml, '/AUDITFILE/AUDITENTRY', 'REASON', olParameter(1).sValue);

  -- write the dom into the audit
  oErrorObj := DomtoAudit;

  isr$trace.stat('end', 'FUNCTION '||sCurrentName||' finished',sCurrentName);
  return oErrorObj;

exception
  WHEN others then
    rollback;
    sUserMsg := utd$msglib.getmsg ('exUnexpectedError', nCurrentLanguage,  csP1 => sqlerrm(sqlcode));
    sImplMsg := sCurrentName || rtrim(': '||sImplMsg,': ');
    sDevMsg  := substr(dbms_utility.format_error_stack ||crlf|| dbms_utility.format_error_backtrace, 1,4000);
    oErrorObj.handleError ( Stb$typedef.cnSeverityCritical, sUserMsg, sImplMsg, sDevMsg );
    return oErrorObj;
end putAuditMask;

--********************************************************************************************************************
function silentAudit return STB$OERROR$RECORD
is
  sCurrentName constant varchar2(100) := $$PLSQL_UNIT||'.silentAudit';
  sUserMsg     varchar2(4000);
  sImplMsg     varchar2(4000);
  sDevMsg      varchar2(4000);
  oErrorObj    stb$oerror$record      := STB$OERROR$RECORD();
begin
  isr$trace.stat('begin', 'FUNCTION '||sCurrentName||' started',sCurrentName);

  --write the dom to clob free the xml dom and reset the global variables
  oErrorObj := STB$AUDIT.DomtoAudit;

  isr$trace.stat('end', 'FUNCTION '||sCurrentName||' finished',sCurrentName);
  return oErrorObj;

exception
  when others then
    rollback;
    sUserMsg := utd$msglib.getmsg ('exUnexpectedError', nCurrentLanguage,  csP1 => sqlerrm(sqlcode));
    sImplMsg := sCurrentName || rtrim(': '||sImplMsg,': ');
    sDevMsg  := substr(dbms_utility.format_error_stack ||crlf|| dbms_utility.format_error_backtrace, 1,4000);
    oErrorObj.handleError ( Stb$typedef.cnSeverityCritical, sUserMsg, sImplMsg, sDevMsg );
    return oErrorObj;
end silentAudit;

--**********************************************************************************************************************
function checkAuditExists( sAuditExists out varchar2,
                           sReference   in  varchar2,
                           sType        in varchar2  )
  return STB$OERROR$RECORD
is
  sCurrentName constant varchar2(300) := $$PLSQL_UNIT||'.checkAuditExists (' || sType || ', ' || sReference || ')';
  sUserMsg     varchar2(4000);
  sImplMsg     varchar2(4000);
  sDevMsg      varchar2(4000);
  oErrorObj    stb$oerror$record      := STB$OERROR$RECORD();
  bIsEmpty     varchar2(1);

  cursor cCheckAuditExists(csReference in varchar2,csType in varchar2) is
   select auditId
     from stb$audittrail
    where reference = csReference
      and audittype = csType;

  cursor cCheckAuditIsEmpty(cnAuditId in number) is
   select DECODE(length(audittext), 0, 'T', 'F') isEmpty
     from stb$audittrail
    where auditId = cnAuditId;

begin
  isr$trace.stat('begin','sType: '||sType||',  sReference: '||sReference,sCurrentName);

  open cCheckAuditExists(sReference,sType);
  fetch cCheckAuditExists into nCurrentAudit;
  if cCheckAuditExists%notfound then
    nCurrentAudit :='';
  else
    open cCheckAuditIsEmpty(nCurrentAudit);
    fetch cCheckAuditIsEmpty into bIsEmpty;
    close cCheckAuditIsEmpty;
    IF bIsEmpty = 'T' THEN
      delete from stb$audittrail where auditid = nCurrentAudit;
      ISR$ACTION$LOG.setActionLog('Auditid '||nCurrentAudit||' from STB$AUDITTRAIL deleted.', 'Audittext was empty.');
      nCurrentAudit :='';
    END IF;
  end if;
  close cCheckAuditExists;

  isr$trace.debug('status nCurrentAudit','nCurrentAudit: '|| nCurrentAudit, sCurrentName);

  if trim(nCurrentAudit) is null then
    sAuditExists := 'F';
    -- dom will be emptied by the function createNewAuditRecord
    nCurrentAudit := '';
  else
    sAuditExists := 'T';
  end if;

  isr$trace.stat('end', 'sAuditExists: '||sAuditExists,sCurrentName);
  return oErrorObj;

exception
  when others then
    sUserMsg := utd$msglib.getmsg ('exUnexpectedError', nCurrentLanguage,  csP1 => sqlerrm(sqlcode));
    sImplMsg := sCurrentName || rtrim(': '||sImplMsg,': ');
    sDevMsg  := substr(dbms_utility.format_error_stack ||crlf|| dbms_utility.format_error_backtrace, 1,4000);
    oErrorObj.handleError ( Stb$typedef.cnSeverityCritical, sUserMsg, sImplMsg, sDevMsg );
    return oErrorObj;
end checkAuditExists;

--****************************************************************************************************************************
function openAudit(sAuditType in varchar2,sReference in varchar2) return STB$OERROR$RECORD
is
  sCurrentName   constant varchar2(100) := $$PLSQL_UNIT||'.openAudit';
  sUserMsg       varchar2(4000);
  sImplMsg       varchar2(4000);
  sDevMsg        varchar2(4000);
  oErrorObj      STB$OERROR$RECORD := STB$OERROR$RECORD();
  sAuditExists   varchar2(1):='F';

begin
  isr$trace.stat('begin', 'FUNCTION '||sCurrentName||' started',sCurrentName);
  isr$trace.debug('status sAuditType','sAuditType: ' || sAuditType  || ' sReference:' ||sReference , sCurrentName);

  --check if an audit entry in the table stb$audittrail exists the key of an auditentry is the reference and the audittype
  oErrorObj := STB$AUDIT.checkAuditExists(sAuditExists ,sReference ,sAuditType);
  isr$trace.debug('status sAuditExists (1)','sAuditExists: '|| sAuditExists , sCurrentName);

  if oErrorObj.sSeverityCode != STB$Typedef.cnNoError then
    return oErrorObj;
  end if;

  isr$trace.debug('status sAuditExists (2)','sAuditExists: '|| sAuditExists , sCurrentName);

  if sAuditExists ='F' then
    -- create a new audit record
    oErrorObj := STB$AUDIT.CreateNewAuditRecord(sReference,sAuditType);

    if oErrorObj.sSeverityCode != STB$Typedef.cnNoError then
      return oErrorObj;
    end if;

    -- create an new XML Dom and the frame
    xAuditXml := ISR$XML.initXml('AUDITFILE');

    ISR$XML.SetAttribute(xAuditXml, '/AUDITFILE', 'TYPE', sAuditType);
    ISR$XML.SetAttribute(xAuditXml, '/AUDITFILE', 'REFERENCE', sReference);
    ISR$XML.SetAttribute(xAuditXml, '/AUDITFILE', 'NCOUNT', '0');

  else
    -- audit exists retrieve the audit into the xmldom and get the count
    oErrorObj := STB$AUDIT.GetAuditIntoDom;

    isr$trace.debug('after audit in dom','after audit in dom' , sCurrentName);

    if oErrorObj.sSeverityCode != STB$Typedef.cnNoError then
      return oErrorObj;
    end if;

  end if;

  isr$trace.debug('status xAuditXml', 'see xAuditXml in column logclob', sCurrentName, ISR$XML.XMLToClob(xAuditXml));

  isr$trace.stat('end', 'FUNCTION '||sCurrentName||' finished',sCurrentName);
  return oErrorObj ;

exception
  when others then
    sUserMsg := utd$msglib.getmsg ('exUnexpectedError', nCurrentLanguage,  csP1 => sqlerrm(sqlcode));
    sImplMsg := sCurrentName || rtrim(': '||sImplMsg,': ');
    sDevMsg  := substr(dbms_utility.format_error_stack ||crlf|| dbms_utility.format_error_backtrace, 1,4000);
    oErrorObj.handleError ( Stb$typedef.cnSeverityCritical, sUserMsg, sImplMsg, sDevMsg );
    return oErrorObj;
end openAudit;

--**********************************************************************************************************************
function AddAuditRecord(sTransaction in varchar2, csDescription in VARCHAR2)  return STB$OERROR$RECORD
is
  sCurrentName      constant varchar2(100) := $$PLSQL_UNIT||'.AddAuditRecord';
  sUserMsg          varchar2(4000);
  sImplMsg          varchar2(4000);
  sDevMsg           varchar2(4000);
  oErrorObj         STB$OERROR$RECORD := STB$OERROR$RECORD();
  nNumber           number;
  nJobId            number;
  sReference        varchar2(4000);
  sType             varchar2(4000);
  sDescription      varchar2(4000);
  sNodeType         varchar2(4000);
  sNodeId           varchar2(4000);
  sToken            varchar2(4000);
  sDisplay          varchar2(4000);

  cursor cGetJobID is
    select jobid
      from STB$JOBSESSION
     where oraclejobsessionid = STB$SECURITY.getCurrentSession;

begin
  isr$trace.stat('begin', 'FUNCTION '||sCurrentName||' started',sCurrentName);
  isr$trace.debug('status sTransaction','sTransaction: ' || sTransaction, sCurrentName);

  nNumber := ISR$XML.valueOf(xAuditXml, 'count(/AUDITFILE/AUDIT)') + 1;

  --ISR$XML.CreateNode(xAuditXml, '/AUDITFILE', 'AUDIT', '');

  if TRIM(sTransaction) is null then
    open cGetJobID;
    fetch cGetJobID into nJobId;
    close cGetJobID;
    sNodeType := STB$JOB.getJobParameter('NODETYPE', nJobId);
    sNodeId := STB$JOB.getJobParameter('REFERENCE', nJobId);
    sToken := STB$JOB.getJobParameter('TOKEN', nJobId);
    sDisplay := STB$JOB.getJobParameter('REFERENCE_DISPLAY', nJobId);
  else
    sNodeType := STB$OBJECT.getCurrentNodetype;
    sNodeId := STB$OBJECT.getCurrentNodeId;
    sToken := sTransaction;
    sDisplay := STB$OBJECT.getCurrentNode().sDisplay;
  end if;
  sReference := sNodeType||' '||sNodeId;
  sType := sToken;
  sDescription := nvl (csDescription, 
                    case
                      when sNodeType = 'CURRENTTEMPLATE' then ISR$TEMPLATE$HANDLER.getTemplateDescription(TO_NUMBER(sNodeId))
                      else sDisplay
                    end);
                    
  ISR$XML.InsertChildXMLFragment(xAuditXml, '/AUDITFILE', '<AUDIT NUMBER="'||nNumber||'" REFERENCE="'||sReference||'">
                                                              <TYPE>'||sType||'</TYPE>
                                                              <DESCRIPTION>'||sDescription||'</DESCRIPTION>
                                                              <BY>'||STB$SECURITY.getOracleUserName(STB$SECURITY.GetCurrentUser)||'</BY>
                                                              <TIMESTAMP>'||to_char(sysdate, ISR$UTIL.getInternalDate)||'</TIMESTAMP>
                                                              <TIMESTAMPRAW visible="F">'||to_char((sysdate-to_Date('01.01.2000','dd.mm.yyyy'))*24*60*60)||'</TIMESTAMPRAW>
                                                            </AUDIT>');                   

  /*ISR$XML.SetAttribute(xAuditXml, '/AUDITFILE/AUDIT[position()=last()]', 'NUMBER',  nNumber);
  ISR$XML.SetAttribute(xAuditXml, '/AUDITFILE/AUDIT[position()=last()]', 'REFERENCE', sReference);

  ISR$XML.CreateNode(xAuditXml, '/AUDITFILE/AUDIT[position()=last()]', 'TYPE', sType);
  ISR$XML.CreateNode(xAuditXml, '/AUDITFILE/AUDIT[position()=last()]', 'DESCRIPTION', sDescription);
  ISR$XML.CreateNode(xAuditXml, '/AUDITFILE/AUDIT[position()=last()]', 'BY', STB$SECURITY.getOracleUserName(STB$SECURITY.GetCurrentUser));
  ISR$XML.CreateNode(xAuditXml, '/AUDITFILE/AUDIT[position()=last()]', 'TIMESTAMP', to_char(sysdate, ISR$UTIL.getInternalDate));
  ISR$XML.CreateNodeAttr(xAuditXml, '/AUDITFILE/AUDIT[position()=last()]', 'TIMESTAMPRAW', to_char((sysdate-to_Date('01.01.2000','dd.mm.yyyy'))*24*60*60), 'visible' , 'F');*/

  ISR$XML.SetAttribute(xAuditXml, '/AUDITFILE', 'NCOUNT', nNumber);

  isr$trace.stat('end', 'FUNCTION '||sCurrentName||' finished',sCurrentName);
  return oErrorObj ;

exception
  when others then
    sUserMsg := utd$msglib.getmsg ('exUnexpectedError', nCurrentLanguage,  csP1 => sqlerrm(sqlcode));
    sImplMsg := sCurrentName || rtrim(': '||sImplMsg,': ');
    sDevMsg  := substr(dbms_utility.format_error_stack ||crlf|| dbms_utility.format_error_backtrace, 1,4000);
    oErrorObj.handleError ( Stb$typedef.cnSeverityCritical, sUserMsg, sImplMsg, sDevMsg );
    return oErrorObj;
end AddAuditRecord;

--**********************************************************************************************************************
function AddAuditEntry(sParametername in varchar2, sOldValue in varchar2, sNewValue in varchar2, nModified in number)  return STB$OERROR$RECORD
is
  sCurrentName constant varchar2(100) := $$PLSQL_UNIT||'.AddAuditEntry';
  sUserMsg     varchar2(4000);
  sImplMsg     varchar2(4000);
  sDevMsg      varchar2(4000);
  oErrorObj    STB$OERROR$RECORD := STB$OERROR$RECORD();
begin
  isr$trace.stat('begin', 'FUNCTION '||sCurrentName||' started',sCurrentName);
  isr$trace.debug('status sParmetername sOldValue sNewValue','sParametername: ' || sParametername || ' /  sOldValue: '||  sOldValue || ' /  sNewValue: '|| sNewValue, sCurrentName);

  --set the modified flag in the package
  sModified := 'T';
  isr$trace.debug('status nModified (1)', 'nModified: '||nModified, sCurrentName);
  
  /*ISR$XML.InsertChildXMLFragment(xAuditXml, '/AUDITFILE/AUDIT[position()=last()]', '<DATA>
                                                                                      <NAME>'||sParametername||'</NAME>
                                                                                      <OLDVALUE>'||NVL(sOldValue, ' ')||'</OLDVALUE>
                                                                                      <NEWVALUE>'||sNewValue||'</NEWVALUE>
                                                                                    </DATA>');  */                          

  -- create the frame for the new entries
  ISR$XML.CreateNode(xAuditXml, '/AUDITFILE/AUDIT[position()=last()]','DATA','');
  ISR$XML.CreateNode(xAuditXml, '/AUDITFILE/AUDIT[position()=last()]/DATA[position()=last()]', 'NAME', sParametername);
  ISR$XML.CreateNode(xAuditXml, '/AUDITFILE/AUDIT[position()=last()]/DATA[position()=last()]', 'OLDVALUE', NVL(sOldValue, ' '));
  ISR$XML.CreateNode(xAuditXml, '/AUDITFILE/AUDIT[position()=last()]/DATA[position()=last()]', 'NEWVALUE', sNewValue);

  isr$trace.debug('status nModified (2)', 'nModified: '||nModified, sCurrentName, xAuditXml);
  isr$trace.stat('end', 'FUNCTION '||sCurrentName||' finished',sCurrentName);
  return oErrorObj ;

exception
  when others then
    sUserMsg := utd$msglib.getmsg ('exUnexpectedError', nCurrentLanguage,  csP1 => sqlerrm(sqlcode));
    sImplMsg := sCurrentName || rtrim(': '||sImplMsg,': ');
    sDevMsg  := substr(dbms_utility.format_error_stack ||crlf|| dbms_utility.format_error_backtrace, 1,4000);
    oErrorObj.handleError ( Stb$typedef.cnSeverityCritical, sUserMsg, sImplMsg, sDevMsg );
    return oErrorObj;
end AddAuditEntry;


--**********************************************************************************************************************
function AddAuditAdditionalElement(sElementName in varchar2, sElementValue in varchar2)  return STB$OERROR$RECORD
is
  sCurrentName constant varchar2(200) := $$PLSQL_UNIT||'.AddAuditAdditionalElement';
  sUserMsg     varchar2(4000);
  sImplMsg     varchar2(4000);
  sDevMsg      varchar2(4000);
  oErrorObj    STB$OERROR$RECORD := STB$OERROR$RECORD();
begin
  isr$trace.stat('begin', 'FUNCTION '||sCurrentName||' started',sCurrentName);
  isr$trace.debug('status','sElementName: ' || sElementName || ' /  sElementValue: '||  sElementValue, sCurrentName);

  --set the modified flag in the package
  sModified :='T';

  -- create the frame for the new entries
  ISR$XML.CreateNode(xAuditXml, '/AUDITFILE/AUDIT[position()=last()]/DATA[position()=last()]', sElementName, sElementValue);

  isr$trace.stat('end', 'FUNCTION '||sCurrentName||' finished',sCurrentName);
  return oErrorObj ;

exception
  when others then
    sUserMsg := utd$msglib.getmsg ('exUnexpectedError', nCurrentLanguage,  csP1 => sqlerrm(sqlcode));
    sImplMsg := sCurrentName || rtrim(': '||sImplMsg,': ');
    sDevMsg  := dbms_utility.format_error_stack ||crlf|| dbms_utility.format_error_backtrace;
    oErrorObj.handleError ( Stb$typedef.cnSeverityCritical, sUserMsg, sImplMsg, sDevMsg );
    return oErrorObj;
end AddAuditAdditionalElement;

--**********************************************************************************************************************
function AddVersionToSrAudit(sVersion in varchar2, sAction in varchar2)
  return STB$OERROR$RECORD
is
  sCurrentName constant varchar2(100) := $$PLSQL_UNIT||'.AddVersionToSrAudit';
  sUserMsg     varchar2(4000);
  sImplMsg     varchar2(4000);
  sDevMsg      varchar2(4000);
  oErrorObj    stb$oerror$record      := STB$OERROR$RECORD();
begin
  isr$trace.stat('begin', 'FUNCTION '||sCurrentName||' started',sCurrentName);
  isr$trace.debug('status sVersion sAction','sVersion: ' || sVersion || ' /  sAction: '||  sAction, sCurrentName);

  --set the modified flag in the package
  sModified :='T';

  -- create the frame for the new entries
  ISR$XML.CreateNode(xAuditXml, '/AUDITFILE/AUDIT[position()=last()]','DATA','');
  ISR$XML.CreateNode(xAuditXml, '/AUDITFILE/AUDIT[position()=last()]/DATA[position()=last()]', 'VERSION', sVersion);
  ISR$XML.CreateNode(xAuditXml, '/AUDITFILE/AUDIT[position()=last()]/DATA[position()=last()]', 'ACTION', sAction );

  isr$trace.stat('end', 'FUNCTION '||sCurrentName||' finished',sCurrentName);
  return oErrorObj ;

exception
  when others then
    sUserMsg := utd$msglib.getmsg ('exUnexpectedError', nCurrentLanguage,  csP1 => sqlerrm(sqlcode));
    sImplMsg := sCurrentName || rtrim(': '||sImplMsg,': ');
    sDevMsg  := substr(dbms_utility.format_error_stack ||crlf|| dbms_utility.format_error_backtrace, 1,4000);
    oErrorObj.handleError ( Stb$typedef.cnSeverityCritical, sUserMsg, sImplMsg, sDevMsg );
    return oErrorObj;
end AddVersionToSrAudit;

--************************************************************************************************************************************
function createCheckSum( cnAuditID in number,nCheckSum out number) return STB$OERROR$RECORD
is
  sCurrentName constant varchar2(100) := $$PLSQL_UNIT||'.createCheckSum';
  sUserMsg     varchar2(4000);
  sImplMsg     varchar2(4000);
  sDevMsg      varchar2(4000);
  oErrorObj    stb$oerror$record      := STB$OERROR$RECORD();
  sClobSelected     CLOB;

begin
  isr$trace.stat('begin', 'FUNCTION '||sCurrentName||' started',sCurrentName);
  isr$trace.debug('status cnAuditID',' cnAuditID : ' ||  cnAuditID , sCurrentName);

  select audittext
    into sClobSelected
    from STB$AUDITTRAIL
   where auditId = cnAuditID;

  isr$trace.debug('after lob',' after lob '  , sCurrentName);

  oErrorObj := STB$UTIL.checksum(sClobSelected , nCheckSum);

  isr$trace.debug('status nChecksum','nCheckSum : ' || nCheckSum , sCurrentName);
  isr$trace.stat('end', 'FUNCTION '||sCurrentName||' finished',sCurrentName);
  return oErrorObj;
exception
  when others then
    sUserMsg := utd$msglib.getmsg ('exUnexpectedError', nCurrentLanguage,  csP1 => sqlerrm(sqlcode));
    sImplMsg := sCurrentName || rtrim(': '||sImplMsg,': ');
    sDevMsg  := substr(dbms_utility.format_error_stack ||crlf|| dbms_utility.format_error_backtrace, 1,4000);
    oErrorObj.handleError ( Stb$typedef.cnSeverityCritical, sUserMsg, sImplMsg, sDevMsg );
    return oErrorObj;
end createCheckSum;

--****************************************************************************************************************************
function DomtoAudit return STB$OERROR$RECORD
is
  -- variables
  oErrorObj           STB$OERROR$RECORD := STB$OERROR$RECORD();
  sCurrentName        constant varchar2(100) := $$PLSQL_UNIT||'.DomtoAudit';
  nCheckSum           number;
  sUserMsg            varchar2(4000);
  sImplMsg            varchar2(4000);
  sDevMsg             varchar2(4000);

begin
  isr$trace.stat('begin', 'Function '||sCurrentName||' started',sCurrentName);

  -- update the audittrail
  update STB$AUDITTRAIL
     set audittext = ISR$XML.XMLToClob(xAuditXml)
   where AuditID = nCurrentAudit;

  -- create checksum
  oErrorObj := createCheckSum(nCurrentAudit,nCheckSum);
  -- TODO: Auswertung und Reaktion auf Severity fehlt hier

  -- update the audittrail
  update STB$AUDITTRAIL
     set checkSum = nCheckSum
   where AuditID = nCurrentAudit;

  oErrorObj:= FreeAuditDom;
  -- TODO: Auswertung und Reaktion auf Severity fehlt hier

  isr$trace.stat('end', 'FUNCTION '||sCurrentName||' finished',sCurrentName);
  return oErrorObj;
exception
 when others then
    sUserMsg := utd$msglib.getmsg ('exUnexpectedError', nCurrentLanguage,  csP1 => sqlerrm(sqlcode));
    sImplMsg := sCurrentName || rtrim(': '||sImplMsg,': ');
    sDevMsg  := substr(dbms_utility.format_error_stack ||crlf|| dbms_utility.format_error_backtrace, 1,4000);
    oErrorObj.handleError ( Stb$typedef.cnSeverityCritical, sUserMsg, sImplMsg, sDevMsg );
    return oErrorObj;
end DomtoAudit;

--************************************************************************************************************
function GetAuditIntoDom
  return STB$OERROR$RECORD
is
  sCurrentName constant varchar2(100) := $$PLSQL_UNIT||'.GetAuditIntoDom';
  sUserMsg     varchar2(4000);
  sImplMsg     varchar2(4000);
  sDevMsg      varchar2(4000);
  oErrorObj    stb$oerror$record      := STB$OERROR$RECORD();
  oError              STB$OERROR$RECORD := STB$OERROR$RECORD();
begin
  isr$trace.stat('begin', 'FUNCTION '||sCurrentName||' started',sCurrentName);
  isr$trace.debug('status nAuditID', 'nAuditID: ' ||nCurrentAudit, sCurrentName);

  select ISR$XML.clobToXML (audittext)
    into xAuditXml
    from stb$audittrail
   where auditid = nCurrentAudit;

  isr$trace.stat('end', 'FUNCTION '||sCurrentName||' finished',sCurrentName);
  return oErrorObj;

exception
 when others then
    sUserMsg := utd$msglib.getmsg ('exUnexpectedError', nCurrentLanguage,  csP1 => sqlerrm(sqlcode));
    sImplMsg := sCurrentName || rtrim(': '||sImplMsg,': ');
    sDevMsg  := substr(dbms_utility.format_error_stack ||crlf|| dbms_utility.format_error_backtrace, 1,4000);
    oErrorObj.handleError ( Stb$typedef.cnSeverityCritical, sUserMsg, sImplMsg, sDevMsg );
    return oErrorObj;
end GetAuditIntoDom;

--*****************************************************************************************************************************
function getAuditMask(olNodeList out STB$TREENODELIST)
  return STB$OERROR$RECORD
is
  sCurrentName constant varchar2(100) := $$PLSQL_UNIT||'.getAuditMask';
  sUserMsg     varchar2(4000);
  sImplMsg     varchar2(4000);
  sDevMsg      varchar2(4000);
  oErrorObj    stb$oerror$record      := STB$OERROR$RECORD();
begin
  isr$trace.stat('begin', 'FUNCTION '||sCurrentName||' started',sCurrentName);

  olNodeList := STB$TREENODELIST ();
  olNodeList.extend;
  olNodeList (1) := STB$TREENODE$RECORD(Utd$msglib.GetMsg ('AUDIT_MASK', Stb$security.GetCurrentLanguage),
                                        'STB$SYSTEM', 'TRANSPORT', 'MASK', '');
  oErrorObj := STB$UTIL.getPropertyList('AUDIT_MASK', olNodeList (1).tloPropertylist);

  isr$trace.stat('end', 'FUNCTION '||sCurrentName||' finished',sCurrentName);
  return (oErrorObj);
exception
 when others then
   rollback;
    sUserMsg := utd$msglib.getmsg ('exUnexpectedError', nCurrentLanguage,  csP1 => sqlerrm(sqlcode));
    sImplMsg := sCurrentName || rtrim(': '||sImplMsg,': ');
    sDevMsg  := substr(dbms_utility.format_error_stack ||crlf|| dbms_utility.format_error_backtrace, 1,4000);
    oErrorObj.handleError ( Stb$typedef.cnSeverityCritical, sUserMsg, sImplMsg, sDevMsg );
    return oErrorObj;
end getAuditMask;

--**********************************************************************************************************************
function getModifiedFlag
  return varchar2
is
begin
  return sModified;
end getModifiedFlag;

--**********************************************************************************************************************
function FreeAuditDom return STB$OERROR$RECORD
is
  sCurrentName constant varchar2(100) := $$PLSQL_UNIT||'.FreeAuditDom';
  sUserMsg     varchar2(4000);
  sImplMsg     varchar2(4000);
  sDevMsg      varchar2(4000);
  oErrorObj    stb$oerror$record      := STB$OERROR$RECORD();
begin
  isr$trace.stat('begin', 'FUNCTION '||sCurrentName||' started',sCurrentName);

  delete from stb$audittrail
  where auditid = nCurrentAudit
  and length(audittext) = 0;

  -- empty the global variables
  nCurrentAudit := '';
  xAuditXml := null;
  sModified := 'F';

  isr$trace.stat('end', 'FUNCTION '||sCurrentName||' finished',sCurrentName);
  return oErrorObj;

exception
 when others then
   rollback;
    sUserMsg := utd$msglib.getmsg ('exUnexpectedError', nCurrentLanguage,  csP1 => sqlerrm(sqlcode));
    sImplMsg := sCurrentName || rtrim(': '||sImplMsg,': ');
    sDevMsg  := substr(dbms_utility.format_error_stack ||crlf|| dbms_utility.format_error_backtrace, 1,4000);
    oErrorObj.handleError ( Stb$typedef.cnSeverityCritical, sUserMsg, sImplMsg, sDevMsg );
    return oErrorObj;
end FreeAuditDom;

--**********************************************************************************************************************
function getAuditDomId
  return xmltype
is
begin
  return xAuditXml;
end getAuditDomId;

--**********************************************************************************************************************
procedure setcurrentxml (xXml in xmltype)
is
begin
  xAuditXml := xXml;
end;

end stb$audit; 