CREATE OR REPLACE PACKAGE BODY ISR$TEMPLATE$HANDLER
is

  sIP                    STB$JOBSESSION.IP%TYPE;
  sPort                  STB$JOBSESSION.PORT%TYPE;

  csOnlyDifferences      VARCHAR2(1);
  csDelimiter            VARCHAR2(100) := chr(10);
  csImpTablePrefix  constant varchar2(10) := 'TMP$IMP$';

--******************************************************************************
FUNCTION copyTemplateFileToJobTrail(cnJobid IN NUMBER, cxXml IN OUT XMLTYPE) RETURN STB$OERROR$RECORD
IS
  oErrorObj              STB$OERROR$RECORD := STB$OERROR$RECORD();
  sCurrentName           constant varchar2(4000) := $$PLSQL_UNIT||'.copyTemplateFileToJobTrail('||cnJobid||')';
BEGIN
  isr$trace.stat('begin', 'begin', sCurrentName);

  oErrorObj := STB$DOCKS.copyFileToJob(STB$JOB.getJobParameter('MASTERTEMPLATE',cnJobid), cnJobId, 'B') ;

  -- update the target type
  UPDATE stb$jobtrail
     SET documenttype = 'TARGET', leadingdoc = 'T'
   WHERE jobid = cnJobId
     AND (documenttype like 'MASTERTEMPLATE%'          --NUVIB-172
         or documenttype = 'ADDITIONAL_TEMPLATE');

  isr$trace.stat('end', 'end', sCurrentName);
  RETURN oErrorObj;

EXCEPTION
  WHEN OTHERS THEN
    oErrorObj.handleError ( Stb$typedef.cnSeverityCritical, utd$msglib.getmsg ('exReportNotInDraftText', stb$security.getCurrentlanguage,  csP1 => sqlerrm(sqlcode)),
        sCurrentName, dbms_utility.format_error_stack ||chr(10)|| dbms_utility.format_error_backtrace );
    SELECT ip, To_Char(port) INTO sIp, sPort FROM STB$JOBSESSION WHERE jobid = cnJobId;
    Stb$job.markDBJobAsBroken(cnJobId,sIp, sPort, oErrorObj.sErrorCode, oErrorObj.sErrorMessage,oErrorObj.sSeverityCode);
  RETURN oErrorObj;
END copyTemplateFileToJobTrail;

--******************************************************************************
FUNCTION copyFile(nOldFileId IN NUMBER, nFileId OUT NUMBER, sNewFileName IN VARCHAR2 DEFAULT null, sComment IN VARCHAR2 DEFAULT NULL, sIcon IN VARCHAR2 DEFAULT NULL, csForceTemplateCreation IN VARCHAR2 DEFAULT 'F') RETURN STB$OERROR$RECORD
IS
  sCurrentName            constant varchar2(4000) := $$PLSQL_UNIT||'.copyFile('||nOldFileId||')';
  oErrorObj               STB$OERROR$RECORD := STB$OERROR$RECORD();
  nInventoryVersion       ISR$INVENTORY.OBJECTVERSION%TYPE;
  sInventoryName          ISR$INVENTORY.OBJECTDESCRIPTION%TYPE;
  sOldFilename            ISR$FILE.FILENAME%TYPE;
  xXml                    XMLTYPE;

  CURSOR cGetOldFileName IS
    SELECT f.fileName
      FROM ISR$FILE f
     WHERE f.fileid = nOldFileId;

  CURSOR cGetInventoryVersion(csobjectDescription IN VARCHAR2, csobjectType IN VARCHAR2) IS
    SELECT objectversion + 1
      FROM isr$inventory
     WHERE UPPER (objectdescription) = UPPER (csobjectDescription)
       AND objecttype = csobjectType;

  CURSOR cIsOldTemplateRelease IS
    SELECT NVL (release, 'F')
      FROM isr$template
     WHERE fileid = nOldFileId;

  sOldReleased VARCHAR2(1) := 'F';

BEGIN
  isr$trace.stat('begin', 'begin', sCurrentName);

  select SEQ$STB$FILE.NEXTVAL into nFileId FROM dual;
  isr$trace.debug('nFileId', nFileId, sCurrentName);

  INSERT INTO ISR$FILE (FILEID
                      , FILENAME
                      , USAGE
                      , TEXTFILE
                      , DESCRIPTION
                      , BLOBFLAG
                      , BINARYFILE
                      , MIMETYPE
                      , DOCSIZE
                      , DOWNLOAD
                      , DOC_DEFINITION)
     (SELECT nFileId
           , filename
           , usage
           , textfile
           , description
           , blobflag
           , binaryfile
           , mimetype
           , docsize
           , download
           , doc_definition
        FROM ISR$FILE
       WHERE fileid = nOldFileId);
  isr$trace.debug('after ISR$FILE', 'after ISR$FILE', sCurrentName);

  OPEN cIsOldTemplateRelease;
  FETCH cIsOldTemplateRelease INTO sOldReleased;
  CLOSE cIsOldTemplateRelease;
  isr$trace.debug('sOldReleased',sOldReleased, sCurrentName);

  IF sOldReleased = 'T' OR csForceTemplateCreation = 'T' THEN
    INSERT INTO ISR$TEMPLATE (TEMPLATENAME
                            , FILEID
                            , RELEASE
                            , VERSION
                            , VISIBLE
                            , EXCHANGE_COMMENT
                            , INSTALLER
                            , ICON)
       (SELECT templatename
             , nFileId
             , 'F'
             , (select MAX(t1.version) + 1 from isr$template t1 where t1.templatename = t.templatename group by t1.templatename)
             , 'T'
             , sComment
             , 'F'
             , NVL(sIcon, icon)
          FROM isr$template t
         WHERE fileid = nOldFileId);
  ELSE
    UPDATE isr$template
       SET EXCHANGE_COMMENT = sComment
         , ICON = NVL(sIcon, icon)
         , fileid = nFileId
     WHERE fileid = nOldFileId;
  END IF;
  isr$trace.debug('after ISR$TEMPLATE', 'after ISR$TEMPLATE', sCurrentName);

  INSERT INTO ISR$OUTPUT$FILE (OUTPUTID, FILEID, TEMPLATENAME)
     (SELECT outputid
           , nFileId
           , templatename
        FROM isr$output$file
       WHERE fileid = nOldFileId);

  IF sOldReleased = 'F' AND csForceTemplateCreation = 'F' THEN
    DELETE FROM ISR$OUTPUT$FILE
     WHERE (templatename, fileid) IN (SELECT templatename, nOldFileId
                                        FROM isr$template
                                       WHERE fileid = nFileId);
  END IF;
  isr$trace.debug('after ISR$OUTPUT$FILE', 'after ISR$OUTPUT$FILE', sCurrentName);

  INSERT INTO STB$GROUP$REPTYPE$RIGHTS (REPORTTYPEID, PARAMETERNAME, PARAMETERVALUE, USERGROUPNO)
  (SELECT REPORTTYPEID, REPLACE(PARAMETERNAME, nOldFileId, nFileId) PARAMETERNAME, PARAMETERVALUE, USERGROUPNO
   FROM STB$GROUP$REPTYPE$RIGHTS
   WHERE parametername = 'TEMPLATE'||nOldFileId);
  isr$trace.debug('after STB$GROUP$REPTYPE$RIGHTS', 'after STB$GROUP$REPTYPE$RIGHTS', sCurrentName);

  INSERT INTO ISR$FILE$PARAMETER$VALUES (REPORTTYPEID, FILEID, ENTITY, DISPLAY, VALUE, INFO, ORDERNUMBER, USE, VISIBLE, EDITABLE, STYLE, CLASS)
  (SELECT REPORTTYPEID,nFileId, ENTITY, DISPLAY, VALUE, INFO, ORDERNUMBER, USE, VISIBLE, EDITABLE, STYLE, CLASS
   FROM ISR$FILE$PARAMETER$VALUES
   WHERE fileid = nOldFileId);
  isr$trace.debug('after ISR$FILE$PARAMETER$VALUES', 'after ISR$FILE$PARAMETER$VALUES', sCurrentName);

  INSERT INTO ISR$FILE$CALC$VALUES (REPORTTYPEID, FILEID, ENTITY, DISPLAY, VALUE, INFO, ORDERNUMBER, USE, VISIBLE, EDITABLE)
  (SELECT REPORTTYPEID,nFileId, ENTITY, DISPLAY, VALUE, INFO, ORDERNUMBER, USE, VISIBLE, EDITABLE
   FROM ISR$FILE$CALC$VALUES
   WHERE fileid = nOldFileId);
  isr$trace.debug('after ISR$FILE$CALC$VALUES', 'after ISR$FILE$CALC$VALUES', sCurrentName);

  INSERT INTO ISR$FILE$STYLE$VALUES (REPORTTYPEID, FILEID, ENTITY, DISPLAY, VALUE, INFO, ORDERNUMBER, USE, VISIBLE, EDITABLE)
  (SELECT REPORTTYPEID,nFileId, ENTITY, DISPLAY, VALUE, INFO, ORDERNUMBER, USE, VISIBLE, EDITABLE
   FROM ISR$FILE$STYLE$VALUES
   WHERE fileid = nOldFileId);
  isr$trace.debug('after ISR$FILE$STYLE$VALUES', 'after ISR$FILE$STYLE$VALUES', sCurrentName);

  --  update inventory
  OPEN cGetOldFileName;
  FETCH cGetOldFileName INTO sOldFilename;
  CLOSE cGetOldFileName;
  isr$trace.debug('sOldFilename',sOldFilename,sCurrentName);

  IF sNewFileName IS NULL THEN
    sInventoryName := sOldFilename;
  ELSE
    sInventoryName := sNewFileName;
  END IF;
  isr$trace.debug('sInventoryName',sInventoryName,sCurrentName);

  OPEN cGetInventoryVersion(sOldFilename,'FILE');
  FETCH cGetInventoryVersion INTO nInventoryVersion;
  IF cGetInventoryVersion%FOUND THEN
    update isr$inventory
       set objectDescription = sInventoryName,
           modifiedon = sysdate,
           modifiedby = Stb$security.getOracleUserName(STB$SECURITY.getCurrentUser)
     where objectType = 'FILE'
       and objectDescription = sOldFilename;
  END IF;
  CLOSE cGetInventoryVersion;
  isr$trace.debug('after update inventory', 'after  update inventory', sCurrentName);

  isr$trace.stat('end', 'end', sCurrentName);
  RETURN oErrorObj;

EXCEPTION
  WHEN OTHERS THEN
    ROLLBACK;
    oErrorObj.handleError ( Stb$typedef.cnSeverityCritical, utd$msglib.getmsg ('exReportNotInDraftText', stb$security.getCurrentlanguage,  csP1 => sqlerrm(sqlcode)),
        sCurrentName, dbms_utility.format_error_stack ||chr(10)|| dbms_utility.format_error_backtrace );
    RETURN oErrorObj;
END copyFile;

--******************************************************************************
FUNCTION updateTemplate(cnMasterTemplate IN NUMBER, csComment IN VARCHAR2, csIcon IN VARCHAR2, csFilename IN VARCHAR2, blBinaryFile IN BLOB, nNewFileId OUT NUMBER, csForceCopy IN VARCHAR2 DEFAULT 'F') RETURN STB$OERROR$RECORD
IS
  oErrorObj               STB$OERROR$RECORD := STB$OERROR$RECORD();
  exFileCopyError         EXCEPTION;
  sCurrentName   constant varchar2(4000) := $$PLSQL_UNIT||'.updateTemplate(cnMasterTemplate:'||cnMasterTemplate||')';

  CURSOR cGetValues(cnFileid IN NUMBER) IS
    SELECT templatename
         , version
         , t.fileid
         , t.exchange_comment
         , icon
         , docsize
         , filename
         , binaryfile
      FROM ISR$TEMPLATE t, ISR$FILE f
     WHERE t.fileid = f.fileid
       AND t.fileid = cnFileid;

  rGetOldValues             cGetValues%rowtype;
  rGetNewValues             cGetValues%rowtype;

  sOldChecksum              VARCHAR2(100);
  sNewChecksum              VARCHAR2(100);

BEGIN
  isr$trace.stat('begin', 'begin', sCurrentName);

  OPEN cGetValues(cnMasterTemplate);
  FETCH cGetValues INTO rGetOldValues;
  CLOSE cGetValues;

  isr$trace.debug('rGetOldValues.TEMPLATENAME',  rGetOldValues.TEMPLATENAME, sCurrentName);
  isr$trace.debug('rGetOldValues.VERSION',  rGetOldValues.VERSION, sCurrentName);
  isr$trace.debug('rGetOldValues.FILEID',  rGetOldValues.FILEID, sCurrentName);
  isr$trace.debug('rGetOldValues.FILENAME',  rGetOldValues.FILENAME, sCurrentName);
  isr$trace.debug('rGetOldValues.DOCSIZE',  rGetOldValues.DOCSIZE, sCurrentName);
  isr$trace.debug('rGetOldValues.EXCHANGE_COMMENT',  rGetOldValues.EXCHANGE_COMMENT, sCurrentName);
  isr$trace.debug('rGetOldValues.ICON',  rGetOldValues.ICON, sCurrentName);

  isr$trace.debug('file in logblob [docsize='||DBMS_LOB.getLength(blBinaryfile)||']',csFilename, sCurrentName,  blBinaryfile);

  oErrorObj := STB$UTIL.checksum(rGetOldValues.binaryfile, sOldChecksum);
  isr$trace.debug('sOldChecksum',  sOldChecksum, sCurrentName);
  oErrorObj := STB$UTIL.checksum(blBinaryfile, sNewChecksum);
  isr$trace.debug('sNewChecksum',  sNewChecksum, sCurrentName);

  IF sOldChecksum != sNewChecksum OR csForceCopy = 'T' THEN
    oErrorObj := copyFile(cnMasterTemplate, nNewFileId, csFilename, csComment, csIcon, csForceCopy);
    IF oErrorObj.sSeverityCode != STB$TYPEDEF.cnNoError THEN
      RAISE exFileCopyError;
    END IF;

    UPDATE ISR$FILE
       SET binaryfile = blBinaryfile
         , docsize = DBMS_LOB.getLength(blBinaryfile)
         , filename = csFilename
     WHERE fileid = nNewFileId;
  ELSE

    nNewFileId := cnMasterTemplate;

    UPDATE isr$template
       SET EXCHANGE_COMMENT = csComment
         , ICON = NVL(csIcon, icon)
     WHERE fileid = nNewFileId;

    IF csFilename != rGetOldValues.filename THEN
      UPDATE ISR$FILE
         SET filename = csFilename
       WHERE fileid = nNewFileId;
    END IF;

  END IF;

  OPEN cGetValues(nNewFileId);
  FETCH cGetValues INTO rGetNewValues;
  CLOSE cGetValues;

  isr$trace.debug('rGetNewValues.TEMPLATENAME',  rGetNewValues.TEMPLATENAME, sCurrentName);
  isr$trace.debug('rGetNewValues.VERSION',  rGetNewValues.VERSION, sCurrentName);
  isr$trace.debug('rGetNewValues.FILEID',  rGetNewValues.FILEID, sCurrentName);
  isr$trace.debug('rGetNewValues.FILENAME',  rGetNewValues.FILENAME, sCurrentName);
  isr$trace.debug('rGetNewValues.DOCSIZE',  rGetNewValues.DOCSIZE, sCurrentName);
  isr$trace.debug('rGetNewValues.EXCHANGE_COMMENT',  rGetNewValues.EXCHANGE_COMMENT, sCurrentName);
  isr$trace.debug('rGetNewValues.ICON',  rGetNewValues.ICON, sCurrentName);

  oErrorObj := Stb$audit.openAudit ('TEMPLATEAUDIT', rGetNewValues.TEMPLATENAME);
  oErrorObj := Stb$audit.AddAuditRecord (case when STB$JOB.isJobSession(STB$SECURITY.getCurrentSession) = 'F' THEN 'CAN_USE_TEMPLATE_AS_CURRENT' END);
  oErrorObj := Stb$audit.AddAuditEntry ('TEMPLATENAME'||': '||rGetNewValues.templatename||chr(10)
                                      ||'VERSION'||': '||rGetNewValues.version||chr(10)
                                      , case when rGetOldValues.VERSION != rGetNewValues.VERSION then 'VERSION'||': '||rGetOldValues.VERSION ||chr(10) else '' end
                                      || case when rGetOldValues.FILEID != rGetNewValues.FILEID then 'FILEID'||': '||rGetOldValues.FILEID ||chr(10) else '' end
                                      --||case when sOldChecksum != sNewChecksum then 'FILECONTENT CHECKSUM'||': '||sOldChecksum||chr(10) else '' end
                                      ||case when rGetOldValues.FILENAME != rGetNewValues.FILENAME then 'FILENAME'||': '||rGetOldValues.FILENAME ||chr(10) else '' end
                                      ||case when rGetOldValues.DOCSIZE != rGetNewValues.DOCSIZE then 'DOCSIZE'||': '||rGetOldValues.DOCSIZE ||chr(10) else '' end
                                      ||case when NVL(rGetOldValues.EXCHANGE_COMMENT, '') != NVL(rGetNewValues.EXCHANGE_COMMENT, '') then 'EXCHANGE_COMMENT'||': '||rGetOldValues.EXCHANGE_COMMENT ||chr(10) else '' end
                                      ||case when NVL(rGetOldValues.ICON, '') != NVL(rGetNewValues.ICON, '') then 'ICON'||': '||rGetOldValues.ICON ||chr(10) else '' end
                                      , case when rGetOldValues.version != rGetNewValues.version then 'VERSION'||': '||rGetNewValues.version ||chr(10) else '' end
                                      ||case when rGetOldValues.FILEID != rGetNewValues.FILEID then 'FILEID'||': '||rGetNewValues.FILEID ||chr(10) else '' end
                                      --||case when sOldChecksum != sNewChecksum then 'FILECONTENT CHECKSUM'||': '||sNewChecksum||chr(10) else '' end
                                      ||case when rGetOldValues.FILENAME != rGetNewValues.FILENAME then 'FILENAME'||': '||rGetNewValues.FILENAME ||chr(10) else '' end
                                      ||case when rGetOldValues.DOCSIZE != rGetNewValues.DOCSIZE then 'DOCSIZE'||': '||rGetNewValues.DOCSIZE ||chr(10) else '' end
                                      ||case when NVL(rGetOldValues.EXCHANGE_COMMENT, '') != NVL(rGetNewValues.EXCHANGE_COMMENT, '') then 'EXCHANGE_COMMENT'||': '||rGetNewValues.EXCHANGE_COMMENT ||chr(10) else '' end
                                      ||case when NVL(rGetOldValues.ICON, '') != NVL(rGetNewValues.ICON, '') then 'ICON'||': '||rGetNewValues.ICON ||chr(10) else '' end
                                      , 1);

  isr$trace.stat('end', 'end', sCurrentName);
  RETURN oErrorObj;
EXCEPTION
  WHEN exFileCopyError THEN
    ROLLBACK;
    oErrorObj.handleError ( Stb$typedef.cnSeverityCritical, utd$msglib.getmsg ('exReportNotInDraftText', stb$security.getCurrentlanguage,  csP1 => sqlerrm(sqlcode)),
        sCurrentName, dbms_utility.format_error_stack ||chr(10)|| dbms_utility.format_error_backtrace );
    RETURN oErrorObj;
  WHEN OTHERS THEN
    ROLLBACK;
    oErrorObj.handleError ( Stb$typedef.cnSeverityCritical, utd$msglib.getmsg ('exReportNotInDraftText', stb$security.getCurrentlanguage,  csP1 => sqlerrm(sqlcode)),
        sCurrentName, dbms_utility.format_error_stack ||chr(10)|| dbms_utility.format_error_backtrace );
    RETURN oErrorObj;
END updateTemplate;

--******************************************************************************
FUNCTION updateTemplate(cnJobid IN NUMBER, cxXml IN OUT XMLTYPE) RETURN STB$OERROR$RECORD
IS
  sCurrentName   constant varchar2(4000) := $$PLSQL_UNIT||'.updateTemplate(cnJobid:'||cnJobid||')';
  oErrorObj               STB$OERROR$RECORD := STB$OERROR$RECORD();

  CURSOR cGetFile IS
   SELECT j.filename, binaryfile, DBMS_LOB.GETLENGTH(BINARYFILE) docsize
   FROM   STB$JOBTRAIL j
   WHERE  j.jobid = cnJobId
   AND    j.DOCUMENTTYPE = 'TARGET';

  rGetFile                cGetFile%rowtype;

  nNewFileId              ISR$FILE.FILEID%TYPE;
BEGIN
  isr$trace.stat('begin', 'begin', sCurrentName);

  OPEN cGetFile;
  FETCH cGetFile INTO rGetFile;
  CLOSE cGetFile;

  oErrorObj := updateTemplate( TO_NUMBER(STB$JOB.getJobParameter ('MASTERTEMPLATE', cnJobid))
                             , STB$JOB.getJobParameter ('COMMENT', cnJobid)
                             , STB$JOB.getJobParameter ('ICON', cnJobid)
                             , rGetFile.filename
                             , rGetFile.binaryfile
                             , nNewFileId);

  IF oErrorObj.sSeverityCode != STB$TYPEDEF.cnNoError THEN
    SELECT ip, To_Char(port) INTO sIp, sPort FROM STB$JOBSESSION WHERE jobid = cnJobId;
    Stb$job.markDBJobAsBroken(cnJobId,sIp, sPort, oErrorObj.sErrorCode, oErrorObj.sErrorMessage,oErrorObj.sSeverityCode);
  ELSE
    oErrorObj := Stb$audit.silentAudit;
    COMMIT;
  END IF;

  isr$trace.stat('begin', 'begin', sCurrentName);
  RETURN oErrorObj;

END updateTemplate;

--******************************************************************************
FUNCTION copyTemplate(cnJobid IN NUMBER, cxXml IN OUT XMLTYPE) RETURN STB$OERROR$RECORD
IS
  oErrorObj               STB$OERROR$RECORD := STB$OERROR$RECORD();
  sCurrentName            constant varchar2(4000) := $$PLSQL_UNIT||'.copyTemplate('||cnJobid||')';

  CURSOR cGetFile IS
   SELECT j.filename, binaryfile, DBMS_LOB.GETLENGTH(BINARYFILE) docsize
   FROM   STB$JOBTRAIL j
   WHERE  j.jobid = cnJobId
   AND    j.DOCUMENTTYPE = 'TARGET';

  rGetFile                cGetFile%rowtype;

  CURSOR cGetOldOutput IS
    SELECT iof.outputid, iof.fileid, iof.templatename
      FROM isr$output$file iof, isr$report$output$type rot
     WHERE iof.outputid = rot.outputid
       AND visible = 'T'
       AND fileid = STB$JOB.getJobParameter ('MASTERTEMPLATE', cnJobid);

  nOldOutputId      ISR$REPORT$OUTPUT$TYPE.OUTPUTID%TYPE;
  nOldFileId        ISR$FILE.FILEID%TYPE;
  sOldTemplateName  ISR$TEMPLATE.TEMPLATENAME%TYPE;
  nOutPutId         ISR$REPORT$OUTPUT$TYPE.OUTPUTID%TYPE;
  nNewFileId        ISR$FILE.FILEID%TYPE;

  sDataBlocks       VARCHAR2(4000);

BEGIN
  isr$trace.stat('begin', 'begin', sCurrentName);

  OPEN cGetOldOutput;
  FETCH cGetOldOutput INTO nOldOutputId, nOldFileId, sOldTemplateName;
  CLOSE cGetOldOutput;
  isr$trace.debug('nOldFileId',nOldFileId, sCurrentName);
  isr$trace.debug('nOldOutputId',nOldOutputId, sCurrentName);

  OPEN cGetFile;
  FETCH cGetFile INTO rGetFile;
  CLOSE cGetFile;
  isr$trace.debug('file in logblob',rGetFile.filename, sCurrentName,  rGetFile.binaryfile);

  SELECT SEQ$STB$FILE.NEXTVAL INTO nNewFileId FROM dual;
  isr$trace.debug('nNewFileId',nNewFileId, sCurrentName);

  INSERT INTO ISR$FILE (FILEID
                      , FILENAME
                      , USAGE
                      , DESCRIPTION
                      , DOC_DEFINITION
                      , BLOBFLAG
                      , MIMETYPE
                      , DOWNLOAD
                      , MODIFIEDON
                      , MODIFIEDBY
                      , BINARYFILE
                      , DOCSIZE)
     (SELECT nNewFileId
           , rGetFile.filename
           , f.usage
           , f.description
           , f.doc_definition
           , f.blobflag
           , f.mimetype
           , 'T'
           , SYSDATE
           , Stb$security.getOracleUserName(STB$JOB.getJobParameter('USER',cnJobid))
           , rGetFile.binaryfile
           , rGetFile.docsize
        FROM ISR$FILE f
       WHERE f.fileid = nOldFileId);
  isr$trace.debug('after ISR$FILE', 'after ISR$FILE',sCurrentName);

  INSERT INTO ISR$TEMPLATE (TEMPLATENAME
                          , FILEID
                          , EXCHANGE_COMMENT
                          , RELEASE
                          , VERSION
                          , VISIBLE
                          , ICON
                          , INSTALLER
                          , COPIED_FROM_FILEID)
   (SELECT STB$JOB.getJobParameter ('TEMPLATENAME', cnJobid)
         , nNewFileId
         , STB$JOB.getJobParameter ('COMMENT', cnJobid)
         , 'F'
         , 1
         , 'T'
         , STB$JOB.getJobParameter ('ICON', cnJobid)
         , 'F'
         , nOldFileId
      FROM dual);
  isr$trace.debug('after ISR$TEMPLATE', 'after ISR$TEMPLATE',sCurrentName);

  INSERT INTO STB$GROUP$REPTYPE$RIGHTS (REPORTTYPEID, PARAMETERNAME, PARAMETERVALUE, USERGROUPNO)
  (SELECT REPORTTYPEID, REPLACE(PARAMETERNAME, nOldFileId, nNewFileId) PARAMETERNAME, PARAMETERVALUE, USERGROUPNO
   FROM STB$GROUP$REPTYPE$RIGHTS
   WHERE parametername = 'TEMPLATE'||nOldFileId);
  isr$trace.debug('after STB$GROUP$REPTYPE$RIGHTS', 'after STB$GROUP$REPTYPE$RIGHTS', sCurrentName);

  -- self join removed for UCIS-263
  INSERT INTO ISR$FILE$PARAMETER$VALUES (REPORTTYPEID, FILEID, ENTITY, DISPLAY, VALUE, INFO, ORDERNUMBER, USE, VISIBLE, EDITABLE, STYLE, CLASS)
  ( SELECT DISTINCT f1.REPORTTYPEID
                  , TO_CHAR (nNewFileId)
                  , f1.ENTITY
                  , f1.DISPLAY
                  , f1.VALUE
                  , f1.INFO
                  , f1.ORDERNUMBER
                  , f1.USE
                  , f1.VISIBLE
                  , f1.EDITABLE
                  , f1.STYLE
                  , f1.CLASS
      FROM ISR$FILE$PARAMETER$VALUES f1
         , (SELECT REPLACE (parametername, 'BOOKMARK_') parametername
                 , parametervalue
              FROM STB$JOBPARAMETER
             WHERE jobid = cnJobid
               AND parametervalue = 'T'
               AND parametername LIKE 'BOOKMARK_%')
     WHERE f1.fileid = nOldFileId
       AND f1.value in (select display from isr$file$parameter$values where value = parametername and entity = 'STYLESHEET$BOOKMARK')
    );
  isr$trace.debug('after ISR$FILE$PARAMETER$VALUES', 'after ISR$FILE$PARAMETER$VALUES', sCurrentName);

  INSERT INTO ISR$FILE$CALC$VALUES (REPORTTYPEID, FILEID, ENTITY, DISPLAY, VALUE, INFO, ORDERNUMBER, USE, VISIBLE, EDITABLE)
  (SELECT DISTINCT f1.REPORTTYPEID
                  , TO_CHAR (nNewFileId)
                  , f1.ENTITY
                  , f1.DISPLAY
                  , f1.VALUE
                  , f1.INFO
                  , f1.ORDERNUMBER
                  , f1.USE
                  , f1.VISIBLE
                  , f1.EDITABLE
      FROM ISR$FILE$CALC$VALUES f1
         , (SELECT REPLACE (parametername, 'BOOKMARK_') parametername
                 , parametervalue
              FROM STB$JOBPARAMETER
             WHERE jobid = cnJobid
               AND parametervalue = 'T'
               AND parametername LIKE 'BOOKMARK_%')
     WHERE f1.fileid(+) = nOldFileId
       AND f1.VALUE = parametername
    );
  isr$trace.debug('after ISR$FILE$CALC$VALUES', 'after ISR$FILE$CALC$VALUES', sCurrentName);

  INSERT INTO ISR$FILE$CALC$VALUES (REPORTTYPEID, FILEID, ENTITY, DISPLAY, VALUE, INFO, ORDERNUMBER, USE, VISIBLE, EDITABLE)
  (SELECT REPORTTYPEID, nNewFileId, ENTITY, DISPLAY, VALUE, INFO, ORDERNUMBER, USE, VISIBLE, EDITABLE
   FROM ISR$FILE$CALC$VALUES
   WHERE fileid = nOldFileId
     AND value not in (select value from isr$file$parameter$values where fileid = nOldFileId and ENTITY = 'STYLESHEET$BOOKMARK'));
  isr$trace.debug('after ISR$FILE$CALC$VALUES2', 'after ISR$FILE$CALC$VALUES', sCurrentName);

  INSERT INTO ISR$FILE$STYLE$VALUES (REPORTTYPEID, FILEID, ENTITY, DISPLAY, VALUE, INFO, ORDERNUMBER, USE, VISIBLE, EDITABLE)
  (SELECT REPORTTYPEID, nNewFileId, ENTITY, DISPLAY, VALUE, INFO, ORDERNUMBER, USE, VISIBLE, EDITABLE
   FROM ISR$FILE$STYLE$VALUES
   WHERE fileid = nOldFileId);
  isr$trace.debug('after ISR$FILE$STYLE$VALUES', 'after ISR$FILE$STYLE$VALUES', sCurrentName);

  -- insert into isr$report$outputtype
  -- the customer output id 's that can be uploaded start with 100.000
  SELECT SEQ$STB$OUTPUTID.NEXTVAL INTO nOutPutId FROM dual;
  isr$trace.debug('nOutPutId',nOutPutId, sCurrentName);

  INSERT INTO ISR$REPORT$OUTPUT$TYPE (OUTPUTID
                                    , DESCRIPTION
                                    , REPORTTYPEID
                                    , LANG
                                    , VISIBLE
                                    , DOC_TYPE
                                    , DOC_DEFINITION
                                    , PLUGIN)
     (SELECT nOutPutId
           , STB$JOB.getJobParameter ('TEMPLATENAME', cnJobid)
           , STB$JOB.getJobParameter ('REPORTTYPEID', cnJobid)
           , lang
           , 'T'
           , doc_type
           , doc_definition
           , plugin -- for offshoot very important, see ISR$REPORTTYPE$PACKAGE.getOffshootList
        FROM ISR$REPORT$OUTPUT$TYPE
       WHERE outputid = nOldOutputId);
  isr$trace.debug('after ISR$REPORT$OUTPUT$TYPE', 'after ISR$REPORT$OUTPUT$TYPE',sCurrentName);

  INSERT INTO ISR$OUTPUT$TRANSFORM ( OUTPUTID, STYLESHEETID, RECREATE, PROTECTED, DOC_DEFINITION, PARAMETERS, EXECUTIONORDER, BASESTYLESHEET, ACTION, BOOKMARKNAME, KEYNAME)
  (SELECT DISTINCT  nOutPutId
                  , ot.STYLESHEETID
                  , ot.recreate
                  , NVL(STB$JOB.getJobParameter('PROTECTED_BOOKMARK_'||ot.bookmarkname, cnJobID), 'T')
                  , (select doc_definition from isr$report$output$type where outputid = nOutPutId)
                  , ot.parameters
                  , ot.executionorder
                  , ot.basestylesheet
                  , ot.action
                  , ot.bookmarkname
                  , ot.keyname
    FROM ISR$OUTPUT$TRANSFORM ot
       , ISR$STYLESHEET s
       , (SELECT REPLACE (parametername, 'BOOKMARK_') parametername
               , parametervalue
            FROM STB$JOBPARAMETER
           WHERE jobid = cnJobid
             AND parametervalue = 'T'
             AND parametername LIKE 'BOOKMARK_%'
          UNION
          SELECT bookmarkname, 'T'
            FROM isr$stylesheet s, isr$output$transform ot
           WHERE structured NOT IN ('G', 'S')
             AND s.stylesheetid = ot.stylesheetid
             AND ot.outputid = nOldOutputId)
   WHERE ot.stylesheetid = s.stylesheetid
     AND (ot.bookmarkname = parametername or ot.bookmarkname = 'NOT_DEFINED')
     AND ot.outputid = nOldOutputId
            /*(SELECT iof.outputid
               FROM isr$output$file iof, isr$report$output$type rot
              WHERE rot.visible = 'T'
                AND rot.outputid = iof.outputid
                AND fileid = STB$UTIL.getReptypeParameter (STB$JOB.getJobParameter ('REPORTTYPEID', cnJobid), 'FULL_TEMPLATE_FILEID'))*/);
  isr$trace.debug('after ISR$OUTPUT$TRANSFORM', 'after ISR$OUTPUT$TRANSFORM',sCurrentName);

  INSERT INTO ISR$OUTPUT$PROPERTY ( OUTPUTID, IDENTIFIER, XPATH, SEPARATOR, DOC_DEFINITION)
  (SELECT nOutPutId, IDENTIFIER, XPATH, separator,  doc_definition
   FROM ISR$OUTPUT$PROPERTY
   WHERE outputid = nOldOutputId);
  isr$trace.debug('after ISR$OUTPUT$PROPERTY', 'after ISR$OUTPUT$PROPERTY', sCurrentName);

  -- for final
  INSERT INTO ISR$OUTPUT$FILE (OUTPUTID, FILEID, TEMPLATENAME)
     (SELECT outputid, nNewFileId, STB$JOB.getJobParameter ('TEMPLATENAME', cnJobid)
        FROM ISR$OUTPUT$FILE
       WHERE fileid = nOldFileId
         AND outputid != nOldOutputId);
  isr$trace.debug('after ISR$OUTPUT$FILE 0', 'after ISR$OUTPUT$FILE 0', sCurrentName);

  -- for all files except the template
  INSERT INTO ISR$OUTPUT$FILE (OUTPUTID, FILEID, TEMPLATENAME)
     (SELECT nOutPutId, FILEID, templatename
        FROM ISR$OUTPUT$FILE
       WHERE outputid = nOldOutputId
         AND NVL(templatename, ' ') != sOldTemplateName);
  isr$trace.debug('after ISR$OUTPUT$FILE 1', 'after ISR$OUTPUT$FILE 1', sCurrentName);

  -- for the template
  INSERT INTO ISR$OUTPUT$FILE (OUTPUTID, FILEID, TEMPLATENAME)
  VALUES(nOutPutId, nNewFileId, STB$JOB.getJobParameter ('TEMPLATENAME', cnJobid));
  isr$trace.debug('after ISR$OUTPUT$FILE 2', 'after ISR$OUTPUT$FILE 2', sCurrentName);

  INSERT INTO ISR$ACTION$OUTPUT (ACTIONID, OUTPUTID, DOC_DEFINITION)
  (SELECT actionid, nOutPutId, doc_definition
   FROM ISR$ACTION$OUTPUT
   WHERE outputid = nOldOutputId);
  isr$trace.debug('after ISR$ACTION$OUTPUT', 'after ISR$ACTION$OUTPUT', sCurrentName);


  FOR rGetDatablocks IN (SELECT REPLACE (parametername, 'BOOKMARK_') parametername
                              , parametervalue
                           FROM STB$JOBPARAMETER
                          WHERE jobid = cnJobid
                            AND parametervalue = 'T'
                            AND parametername LIKE 'BOOKMARK_%'
                          UNION
                         SELECT bookmarkname, 'T'
                           FROM isr$stylesheet s, isr$output$transform ot
                          WHERE structured NOT IN ('G', 'S')
                            AND s.stylesheetid = ot.stylesheetid
                            AND ot.outputid = nOldOutputId) LOOP
    sDataBlocks := sDataBlocks||rGetDatablocks.parametername||', ';
  END LOOP;


  oErrorObj := Stb$audit.openAudit ('TEMPLATEAUDIT', STB$JOB.getJobParameter ('TEMPLATENAME', cnJobid));
  oErrorObj := Stb$audit.AddAuditRecord (null);
  oErrorObj := Stb$audit.AddAuditEntry ('FILEID'||': '||nNewFileId, 'Insert', 'TEMPLATENAME: '|| STB$JOB.getJobParameter ('TEMPLATENAME', cnJobid) ||chr(10)
                                                                            ||'FILENAME: '|| rGetFile.filename ||chr(10)
                                                                            ||'DOCSIZE: '|| rGetFile.docsize ||chr(10)
                                                                            ||'COMMENT: '|| STB$JOB.getJobParameter ('COMMENT', cnJobid) ||chr(10)
                                                                            ||'ICON: '|| STB$JOB.getJobParameter ('ICON', cnJobid) ||chr(10)
                                                                            ||CASE WHEN TRIM(sDataBlocks) IS NOT NULL THEN 'DATABLOCKS: '||RTRIM(sDataBlocks, ', ') ELSE '' END, 1);
  oErrorObj := Stb$audit.silentAudit;

  COMMIT;
  
  ISR$TREE$PACKAGE.sendSyncCall('CURRENTTEMPLATE', nNewFileId);

  isr$trace.stat('end', 'end', sCurrentName);
  RETURN oErrorObj;
EXCEPTION
  WHEN OTHERS THEN
    ROLLBACK;
    oErrorObj.handleError ( Stb$typedef.cnSeverityCritical, utd$msglib.getmsg ('exReportNotInDraftText', stb$security.getCurrentlanguage,  csP1 => sqlerrm(sqlcode)),
        sCurrentName, dbms_utility.format_error_stack ||chr(10)|| dbms_utility.format_error_backtrace );
    SELECT ip, To_Char(port) INTO sIp, sPort FROM STB$JOBSESSION WHERE jobid = cnJobId;
    Stb$job.markDBJobAsBroken(cnJobId,sIp, sPort, oErrorObj.sErrorCode, oErrorObj.sErrorMessage,oErrorObj.sSeverityCode);
    RETURN oErrorObj;
END copyTemplate;

--******************************************************************************
PROCEDURE addComparisonEntry(xXml IN OUT XMLTYPE, csParameterName IN VARCHAR2, csParameter1Value IN VARCHAR2, csParameter2Value IN VARCHAR2, csType IN VARCHAR2 DEFAULT 'TEMPLATE') IS
  sCurrentName         constant varchar2(4000) := $$PLSQL_UNIT||'.addComparisonEntry('||' '||')';
  sDiffType                     VARCHAR2(20);
  sOldValue                     VARCHAR2(4000);
  sNewValue                     VARCHAR2(4000);
BEGIN
  isr$trace.stat('begin', 'begin', sCurrentName);
  sDiffType := CASE
                 WHEN NVL(csParameter1Value, '') = NVL(csParameter2Value, '')
                   OR csParameter1Value IS NULL AND csParameter2Value IS NULL THEN '-'
                 ELSE
                   CASE
                     WHEN csParameter1Value IS NULL THEN 'Insert'
                     WHEN csParameter2Value IS NULL THEN 'Delete'
                     ELSE 'Update'
                   END
               END;
  sOldValue := csParameter1Value;
  sNewValue := csParameter2Value;
  IF sDiffType = 'Update' AND csOnlyDifferences = 'T' THEN
    ISR$TEMPLATE$HANDLER.showOnlyDiffs(sOldValue, sNewValue);
  END IF;

  ISR$XML.CreateNode(xXml, '/COMPARISON', 'ENTRY','');
  ISR$XML.CreateNode(xXml, '/COMPARISON/ENTRY[position()=last()]', 'ORIGIN', csType);
  ISR$XML.CreateNode(xXml, '/COMPARISON/ENTRY[position()=last()]', 'PARAMETER', csParameterName);
  ISR$XML.CreateNode(xXml, '/COMPARISON/ENTRY[position()=last()]', 'FIRST_TEMPLATE', sOldValue);
  ISR$XML.CreateNode(xXml, '/COMPARISON/ENTRY[position()=last()]', 'SECOND_TEMPLATE', sNewValue);
  ISR$XML.CreateNode(xXml, '/COMPARISON/ENTRY[position()=last()]', 'IS_DIFFERENT', CASE WHEN sDiffType = '-' THEN 'F' ELSE 'T' END);
  ISR$XML.CreateNode(xXml, '/COMPARISON/ENTRY[position()=last()]', 'DIFF_TYPE', sDiffType);
  ISR$XML.CreateNode(xXml, '/COMPARISON/ENTRY[position()=last()]', 'ORDERNUMBER', LPAD(ISR$XML.valueOf(xXml, 'count(/COMPARISON/ENTRY)'), 4, '0'));

  isr$trace.stat('end', 'end', sCurrentName);
END;

--******************************************************************************
FUNCTION getFileParameterValues(cnFileid IN NUMBER, csEntity IN VARCHAR2, csValue IN VARCHAR2, csDisplay IN VARCHAR2) RETURN VARCHAR2
IS
  sCurrentName   constant varchar2(4000) := $$PLSQL_UNIT||'.getFileParameterValues('||' '||')';
  sReturn VARCHAR2(4000);
  CURSOR cGetValues IS
    SELECT *
      FROM isr$file$parameter$values
     WHERE fileid = cnFileid
       AND entity = csEntity
       AND value = csValue
       AND display = csDisplay;
BEGIN
  isr$trace.stat('begin', 'begin', sCurrentName);
  FOR rGetValues IN cGetValues LOOP
    sReturn := UTD$MSGLIB.getMsg('INFO_FILEPARA$DESC', STB$SECURITY.getCurrentLanguage) || ': ' || rGetValues.INFO || chr(10)
            || UTD$MSGLIB.getMsg('ORDERNUMBER$DESC', STB$SECURITY.getCurrentLanguage) || ': ' || rGetValues.ORDERNUMBER || chr(10)
            || UTD$MSGLIB.getMsg('USE$DESC', STB$SECURITY.getCurrentLanguage) || ': ' || rGetValues.USE || chr(10)
            || UTD$MSGLIB.getMsg('VISIBLE$DESC', STB$SECURITY.getCurrentLanguage) || ': ' || rGetValues.VISIBLE || chr(10)
            || UTD$MSGLIB.getMsg('EDITABLE$DESC', STB$SECURITY.getCurrentLanguage) || ': ' || rGetValues.EDITABLE || chr(10)
            || UTD$MSGLIB.getMsg('STYLE_FILEPARA$DESC', STB$SECURITY.getCurrentLanguage) || ': ' || rGetValues.STYLE || chr(10)
            || UTD$MSGLIB.getMsg('CLASS_FILEPARA$DESC', STB$SECURITY.getCurrentLanguage) || ': ' || rGetValues.CLASS;
  END LOOP;
  isr$trace.stat('end', 'end', sCurrentName);
  RETURN sReturn;
END getFileParameterValues;

--******************************************************************************
PROCEDURE compareFileParameterValues(xXml IN OUT XMLTYPE, cnFileid1 IN NUMBER, cnFileid2 IN NUMBER, cnCheckOnFileid1 IN NUMBER, cnCheckOnFileid2 IN NUMBER)
IS
  sCurrentName   constant varchar2(4000) := $$PLSQL_UNIT||'.compareFileParameterValues('||' '||')';
BEGIN
  isr$trace.stat('begin', 'begin', sCurrentName);
  FOR rGetDiffs IN (SELECT *
                      FROM ( (SELECT VALUE, ENTITY, DISPLAY
                                FROM ISR$FILE$PARAMETER$VALUES
                               WHERE fileid = NVL (cnCheckOnFileid1, cnCheckOnFileid2))
                            MINUS
                            (SELECT VALUE, ENTITY, DISPLAY
                               FROM ISR$FILE$PARAMETER$VALUES
                              WHERE fileid = cnFileid1
                             INTERSECT
                             SELECT VALUE, ENTITY, DISPLAY
                               FROM ISR$FILE$PARAMETER$VALUES
                              WHERE fileid = cnFileid2))
                     WHERE (cnCheckOnFileid1 IS NULL
                         OR cnCheckOnFileid2 IS NULL)
                       --AND visible = 'T'
                       --AND editable = 'T'
                    UNION
                    SELECT *
                      FROM (SELECT VALUE, ENTITY, DISPLAY
                              FROM ISR$FILE$PARAMETER$VALUES
                             WHERE fileid = cnFileid1
                            INTERSECT
                            SELECT VALUE, ENTITY, DISPLAY
                              FROM ISR$FILE$PARAMETER$VALUES
                             WHERE fileid = cnFileid2)
                     WHERE cnCheckOnFileid1 IS NOT NULL
                       AND cnCheckOnFileid2 IS NOT NULL
                       --AND visible = 'T'
                       --AND editable = 'T'
                    ORDER BY 1, 2, 3) LOOP
    addComparisonEntry(xXml
                      , UTD$MSGLIB.getMsg('VALUE_FILEPARA$DESC', STB$SECURITY.getCurrentLanguage) || ': ' || rGetDiffs.VALUE || chr(10)
                     || UTD$MSGLIB.getMsg('ENTITY_FILEPARA$DESC', STB$SECURITY.getCurrentLanguage) || ': ' || rGetDiffs.ENTITY || chr(10)
                     || UTD$MSGLIB.getMsg('DISPLAY_FILEPARA$DESC', STB$SECURITY.getCurrentLanguage) || ': ' || rGetDiffs.DISPLAY
                      , case when cnCheckOnFileid1 is null then null else getFileParameterValues(cnFileid1, rGetDiffs.ENTITY, rGetDiffs.VALUE, rGetDiffs.DISPLAY) end
                      , case when cnCheckOnFileid2 is null then null else getFileParameterValues(cnFileid2, rGetDiffs.ENTITY, rGetDiffs.VALUE, rGetDiffs.DISPLAY) end
                      , 'ISR$FILE$PARAMETER$VALUES' );
  END LOOP;
  isr$trace.stat('end', 'end', sCurrentName);
END compareFileParameterValues;

--******************************************************************************
FUNCTION getFileCalcValues(cnFileid IN NUMBER, csEntity IN VARCHAR2, csValue IN VARCHAR2, csDisplay IN VARCHAR2) RETURN VARCHAR2
IS
  sCurrentName   constant varchar2(4000) := $$PLSQL_UNIT||'.getFileCalcValues('||' '||')';
  sReturn VARCHAR2(4000);
  CURSOR cGetValues IS
    SELECT *
      FROM isr$file$calc$values
     WHERE fileid = cnFileid
       AND entity = csEntity
       AND value = csValue
       AND display = csDisplay;
BEGIN
  isr$trace.stat('begin', 'begin', sCurrentName);
  FOR rGetValues IN cGetValues LOOP
    sReturn := UTD$MSGLIB.getMsg('INFO_FILEPARA$DESC', STB$SECURITY.getCurrentLanguage) || ': ' || rGetValues.INFO || chr(10)
            || UTD$MSGLIB.getMsg('ORDERNUMBER$DESC', STB$SECURITY.getCurrentLanguage) || ': ' || rGetValues.ORDERNUMBER || chr(10)
            || UTD$MSGLIB.getMsg('USE$DESC', STB$SECURITY.getCurrentLanguage) || ': ' || rGetValues.USE|| chr(10)
            || UTD$MSGLIB.getMsg('VISIBLE$DESC', STB$SECURITY.getCurrentLanguage) || ': ' || rGetValues.VISIBLE || chr(10)
            || UTD$MSGLIB.getMsg('EDITABLE$DESC', STB$SECURITY.getCurrentLanguage) || ': ' || rGetValues.EDITABLE ;
  END LOOP;
  isr$trace.stat('end', 'end', sCurrentName);
  RETURN sReturn;
END getFileCalcValues;

--******************************************************************************
PROCEDURE compareFileCalcValues(xXml IN OUT XMLTYPE, cnFileid1 IN NUMBER, cnFileid2 IN NUMBER, cnCheckOnFileid1 IN NUMBER, cnCheckOnFileid2 IN NUMBER)
IS
  sCurrentName   constant varchar2(4000) := $$PLSQL_UNIT||'.compareFileCalcValues('||' '||')';
BEGIN
  isr$trace.stat('begin', 'begin', sCurrentName);
  FOR rGetDiffs IN (SELECT *
                      FROM ( (SELECT VALUE, ENTITY, DISPLAY
                                FROM ISR$FILE$CALC$VALUES
                               WHERE fileid = NVL (cnCheckOnFileid1, cnCheckOnFileid2))
                            MINUS
                            (SELECT VALUE, ENTITY, DISPLAY
                               FROM ISR$FILE$CALC$VALUES
                              WHERE fileid = cnFileid1
                             INTERSECT
                             SELECT VALUE, ENTITY, DISPLAY
                               FROM ISR$FILE$CALC$VALUES
                              WHERE fileid = cnFileid2))
                     WHERE (cnCheckOnFileid1 IS NULL
                         OR cnCheckOnFileid2 IS NULL)
                       --AND visible = 'T'
                       --AND editable = 'T'
                    UNION
                    SELECT *
                      FROM (SELECT VALUE, ENTITY, DISPLAY
                              FROM ISR$FILE$CALC$VALUES
                             WHERE fileid = cnFileid1
                            INTERSECT
                            SELECT VALUE, ENTITY, DISPLAY
                              FROM ISR$FILE$CALC$VALUES
                             WHERE fileid = cnFileid2)
                     WHERE cnCheckOnFileid1 IS NOT NULL
                       AND cnCheckOnFileid2 IS NOT NULL
                       --AND visible = 'T'
                       --AND editable = 'T'
                    ORDER BY 1, 2, 3) LOOP
    addComparisonEntry(xXml
                      , UTD$MSGLIB.getMsg('CALC_BLOCK$DESC', STB$SECURITY.getCurrentLanguage) || ': ' || rGetDiffs.VALUE || chr(10)
                     || UTD$MSGLIB.getMsg('ENTITY_FILEPARA$DESC', STB$SECURITY.getCurrentLanguage) || ': ' || rGetDiffs.ENTITY || chr(10)
                     || UTD$MSGLIB.getMsg('DISPLAY_FILEPARA$DESC', STB$SECURITY.getCurrentLanguage) || ': ' || rGetDiffs.DISPLAY
                      , case when cnCheckOnFileid1 is null then null else getFileCalcValues(cnFileid1, rGetDiffs.ENTITY, rGetDiffs.VALUE, rGetDiffs.DISPLAY) end
                      , case when cnCheckOnFileid2 is null then null else getFileCalcValues(cnFileid2, rGetDiffs.ENTITY, rGetDiffs.VALUE, rGetDiffs.DISPLAY) end
                      , 'ISR$FILE$CALC$VALUES' );
  END LOOP;
  isr$trace.stat('end', 'end', sCurrentName);
END compareFileCalcValues;

--******************************************************************************
FUNCTION getFileStyleValues(cnFileid IN NUMBER, csEntity IN VARCHAR2, csValue IN VARCHAR2) RETURN VARCHAR2
IS
  sCurrentName   constant varchar2(4000) := $$PLSQL_UNIT||'.getFileStyleValues('||' '||')';
  sReturn VARCHAR2(4000);
  CURSOR cGetValues IS
    SELECT *
      FROM isr$file$style$values
     WHERE fileid = cnFileid
       AND entity = csEntity
       AND value = csValue;
BEGIN
  isr$trace.stat('end', 'end', sCurrentName);
  FOR rGetValues IN cGetValues LOOP
    sReturn := UTD$MSGLIB.getMsg('STYLEVALUE$DESC', STB$SECURITY.getCurrentLanguage) || ': ' || rGetValues.DISPLAY || chr(10)
            || UTD$MSGLIB.getMsg('ORDERNUMBER$DESC', STB$SECURITY.getCurrentLanguage) || ': ' || rGetValues.ORDERNUMBER || chr(10)
            || UTD$MSGLIB.getMsg('USE$DESC', STB$SECURITY.getCurrentLanguage) || ': ' || rGetValues.USE|| chr(10)
            || UTD$MSGLIB.getMsg('VISIBLE$DESC', STB$SECURITY.getCurrentLanguage) || ': ' || rGetValues.VISIBLE || chr(10)
            || UTD$MSGLIB.getMsg('EDITABLE$DESC', STB$SECURITY.getCurrentLanguage) || ': ' || rGetValues.EDITABLE ;
  END LOOP;
  isr$trace.stat('end', 'end', sCurrentName);
  RETURN sReturn;
END getFileStyleValues;

--******************************************************************************
PROCEDURE compareFileStyleValues(xXml IN OUT XMLTYPE, cnFileid1 IN NUMBER, cnFileid2 IN NUMBER, cnCheckOnFileid1 IN NUMBER, cnCheckOnFileid2 IN NUMBER)
IS
  sCurrentName   constant varchar2(4000) := $$PLSQL_UNIT||'.compareFileStyleValues('||' '||')';
BEGIN
  isr$trace.stat('begin', 'begin', sCurrentName);
  FOR rGetDiffs IN (SELECT *
                      FROM ( (SELECT VALUE, ENTITY
                                FROM ISR$FILE$STYLE$VALUES
                               WHERE fileid = NVL (cnCheckOnFileid1, cnCheckOnFileid2))
                            MINUS
                            (SELECT VALUE, ENTITY
                               FROM ISR$FILE$STYLE$VALUES
                              WHERE fileid = cnFileid1
                             INTERSECT
                             SELECT VALUE, ENTITY
                               FROM ISR$FILE$STYLE$VALUES
                              WHERE fileid = cnFileid2))
                     WHERE (cnCheckOnFileid1 IS NULL
                         OR cnCheckOnFileid2 IS NULL)
                       --AND visible = 'T'
                       --AND editable = 'T'
                    UNION
                    SELECT *
                      FROM (SELECT VALUE, ENTITY
                              FROM ISR$FILE$STYLE$VALUES
                             WHERE fileid = cnFileid1
                            INTERSECT
                            SELECT VALUE, ENTITY
                              FROM ISR$FILE$STYLE$VALUES
                             WHERE fileid = cnFileid2)
                     WHERE cnCheckOnFileid1 IS NOT NULL
                       AND cnCheckOnFileid2 IS NOT NULL
                       --AND visible = 'T'
                       --AND editable = 'T'
                    ORDER BY 1, 2) LOOP
    addComparisonEntry(xXml
                      , UTD$MSGLIB.getMsg('CLASS$DESC', STB$SECURITY.getCurrentLanguage) || ': ' || rGetDiffs.ENTITY || chr(10)
                     || UTD$MSGLIB.getMsg('STYLENAME$DESC', STB$SECURITY.getCurrentLanguage) || ': ' || rGetDiffs.VALUE
                      , case when cnCheckOnFileid1 is null then null else getFileStyleValues(cnFileid1, rGetDiffs.ENTITY, rGetDiffs.VALUE) end
                      , case when cnCheckOnFileid2 is null then null else getFileStyleValues(cnFileid2, rGetDiffs.ENTITY, rGetDiffs.VALUE) end
                      , 'ISR$FILE$STYLE$VALUES' );
  END LOOP;
  isr$trace.stat('end', 'end', sCurrentName);
END compareFileStyleValues;

--******************************************************************************
FUNCTION compareTemplates(cnFileid1 IN NUMBER, cnFileid2 IN NUMBER, csOnlyDiffs IN VARCHAR2 DEFAULT 'F') RETURN XMLTYPE
IS
  oErrorObj               STB$OERROR$RECORD := STB$OERROR$RECORD();
  sCurrentName   constant varchar2(4000) := $$PLSQL_UNIT||'.compareTemplates('||' '||')';

  CURSOR cGetValues(cnFileid IN NUMBER) IS
    SELECT templatename
         , version
         , t.fileid
         , t.exchange_comment
         , icon
         , installer
         , (select t1.templatename || ' ' || t1.version from isr$template t1 where t1.fileid = t.copied_from_fileid) copied_from_template
         , docsize
         , filename
         , binaryfile
      FROM ISR$TEMPLATE t, ISR$FILE f
     WHERE t.fileid = f.fileid
       AND t.fileid = cnFileid;

  rGetOldValues             cGetValues%rowtype;
  rGetNewValues             cGetValues%rowtype;

  sOldChecksum              VARCHAR2(100);
  sNewChecksum              VARCHAR2(100);

  xXml                      XMLTYPE;

BEGIN
  isr$trace.stat('begin', 'begin', sCurrentName);

  csOnlyDifferences := csOnlyDiffs;

  isr$trace.debug('cnFileid1',  cnFileid1, sCurrentName);

  OPEN cGetValues(cnFileid1);
  FETCH cGetValues INTO rGetOldValues;
  CLOSE cGetValues;

  isr$trace.debug('cnFileid2',  cnFileid2, sCurrentName);

  OPEN cGetValues(cnFileid2);
  FETCH cGetValues INTO rGetNewValues;
  CLOSE cGetValues;

  oErrorObj := STB$UTIL.checksum(rGetOldValues.binaryfile, sOldChecksum);
  isr$trace.debug('sOldChecksum',  sOldChecksum, sCurrentName);
  oErrorObj := STB$UTIL.checksum(rGetNewValues.binaryfile, sNewChecksum);
  isr$trace.debug('sNewChecksum',  sNewChecksum, sCurrentName);

  xXml := ISR$XML.initXml('COMPARISON');

  addComparisonEntry(xXml, 'TEMPLATENAME', rGetOldValues.TEMPLATENAME, rGetNewValues.TEMPLATENAME );
  addComparisonEntry(xXml, 'VERSION', rGetOldValues.VERSION, rGetNewValues.VERSION );
  addComparisonEntry(xXml, 'FILEID', rGetOldValues.FILEID, rGetNewValues.FILEID );
  addComparisonEntry(xXml, 'FILENAME', rGetOldValues.FILENAME, rGetNewValues.FILENAME );
  addComparisonEntry(xXml, 'DOCSIZE', rGetOldValues.DOCSIZE, rGetNewValues.DOCSIZE );
  addComparisonEntry(xXml, 'FILECONTENT', sOldChecksum, sNewChecksum );
  addComparisonEntry(xXml, 'EXCHANGE_COMMENT', rGetOldValues.EXCHANGE_COMMENT, rGetNewValues.EXCHANGE_COMMENT );
  addComparisonEntry(xXml, 'ICON', rGetOldValues.ICON, rGetNewValues.ICON );
  addComparisonEntry(xXml, 'INSTALLER', rGetOldValues.INSTALLER, rGetNewValues.INSTALLER );
  addComparisonEntry(xXml, 'COPIED_FROM_TEMPLATE', rGetOldValues.COPIED_FROM_TEMPLATE, rGetNewValues.COPIED_FROM_TEMPLATE );

  compareFileParameterValues(xXml, cnFileid1, cnFileid2, null, cnFileid2);
  compareFileParameterValues(xXml, cnFileid1, cnFileid2, cnFileid1, null);
  compareFileParameterValues(xXml, cnFileid1, cnFileid2, cnFileid1, cnFileid2);

  compareFileCalcValues(xXml, cnFileid1, cnFileid2, null, cnFileid2);
  compareFileCalcValues(xXml, cnFileid1, cnFileid2, cnFileid1, null);
  compareFileCalcValues(xXml, cnFileid1, cnFileid2, cnFileid1, cnFileid2);

  compareFileStyleValues(xXml, cnFileid1, cnFileid2, null, cnFileid2);
  compareFileStyleValues(xXml, cnFileid1, cnFileid2, cnFileid1, null);
  compareFileStyleValues(xXml, cnFileid1, cnFileid2, cnFileid1, cnFileid2);

  isr$trace.debug ('status', 'result of file compare',sCurrentname, xXml);
  isr$trace.stat('end', 'end', sCurrentName);
  return xXml;

EXCEPTION
  WHEN OTHERS THEN
    oErrorObj.handleError ( Stb$typedef.cnSeverityCritical, utd$msglib.getmsg ('exReportNotInDraftText', stb$security.getCurrentlanguage,  csP1 => sqlerrm(sqlcode)),
        sCurrentName, dbms_utility.format_error_stack ||chr(10)|| dbms_utility.format_error_backtrace );
    RETURN NULL;
END compareTemplates;

--******************************************************************************
FUNCTION compareTemplates(cnJobid IN NUMBER, cxXml IN OUT XMLTYPE) RETURN STB$OERROR$RECORD
IS
  oErrorObj               STB$OERROR$RECORD := STB$OERROR$RECORD();
  sCurrentName   constant varchar2(4000) := $$PLSQL_UNIT||'.compareTemplates('||cnJobid||')';
  xXml                      XMLTYPE;

  nFileid1                  NUMBER;
  nFileid2                  NUMBER;

BEGIN
  isr$trace.stat('begin', 'begin', sCurrentName);

--ISRC-1353, order of nFileid1 and nFileid2 was wrong
  nFileid2 := TO_NUMBER(STB$JOB.getJobParameter ('REFERENCE', cnJobid));
  isr$trace.debug('nFileid1',  nFileid1, sCurrentName);

  nFileid1 := TO_NUMBER(STB$JOB.getJobParameter ('FILTER_COMPARE_WITH_TEMPLATE', cnJobid));
  isr$trace.debug('nFileid2',  nFileid2, sCurrentName);

  xXml := compareTemplates(nFileid1, nFileid2, STB$JOB.getJobParameter ('IS_DIFFERENT', cnJobid));

  oErrorObj := STB$DOCKS.saveXmlReport(xXml, cnJobid, lower(STB$JOB.getJobParameter ('TOKEN', cnJobId))||'.xml');

  isr$trace.stat('end', 'end', sCurrentName);
  RETURN oErrorObj;
EXCEPTION
  WHEN OTHERS THEN
    ROLLBACK;
    oErrorObj.sErrorCode      :=SQLCODE;
    oErrorObj.handleError ( Stb$typedef.cnSeverityCritical, utd$msglib.getmsg ('exReportNotInDraftText', stb$security.getCurrentlanguage,  csP1 => sqlerrm(sqlcode)),
        sCurrentName, dbms_utility.format_error_stack ||chr(10)|| dbms_utility.format_error_backtrace );
    SELECT ip, To_Char(port) INTO sIp, sPort FROM STB$JOBSESSION WHERE jobid = cnJobId;
    Stb$job.markDBJobAsBroken(cnJobId,sIp, sPort, oErrorObj.sErrorCode, oErrorObj.sErrorMessage,oErrorObj.sSeverityCode);
    RETURN oErrorObj;
END compareTemplates;

--******************************************************************************
PROCEDURE showOnlyDiffs(sOldValue IN OUT VARCHAR2, sNewValue IN OUT VARCHAR2)
IS
  sCurrentName   constant varchar2(4000) := $$PLSQL_UNIT||'.showOnlyDiffs('||' '||')';
  oerrorobj      stb$oerror$record := STB$OERROR$RECORD();
  sOldValuesList stb$typedef.tlstrings;
  sNewValuesList stb$typedef.tlstrings;
BEGIN
  isr$trace.stat('begin', 'begin', sCurrentName);
  oerrorobj := STB$UTIL.strtokenize (sOldValue, csDelimiter, sOldValuesList);
  oerrorobj := STB$UTIL.strtokenize (sNewValue, csDelimiter, sNewValuesList);
  isr$trace.debug('sOldValue',sOldValue,sCurrentName);
  isr$trace.debug('sNewValue',sNewValue,sCurrentName);
  FOR i IN 1..sOldValuesList.COUNT LOOP
    isr$trace.debug('sOldValuesList(i)',sOldValuesList(i),sCurrentName);
    isr$trace.debug('sNewValuesList(i)',sNewValuesList(i),sCurrentName);
    IF sOldValuesList(i) = sNewValuesList(i) THEN
      sOldValue := REPLACE(sOldValue, sOldValuesList(i) || case when i < sOldValuesList.COUNT then csDelimiter end);
      sNewValue := REPLACE(sNewValue, sNewValuesList(i) || case when i < sOldValuesList.COUNT then csDelimiter end);
    END IF;
  END LOOP;
  isr$trace.debug('sOldValue',sOldValue,sCurrentName);
  isr$trace.debug('sNewValue',sNewValue,sCurrentName);
  isr$trace.stat('end', 'end', sCurrentName);
END showOnlyDiffs;

--******************************************************************************
FUNCTION getTemplateDescription(cnFileId IN VARCHAR2) RETURN VARCHAR2
IS
  sCurrentName   constant varchar2(4000) := $$PLSQL_UNIT||'.getTemplateDescription('||cnFileId||')';

  CURSOR cGetTemplateDescription IS
    SELECT templatename || ' ' || version
      FROM ISR$TEMPLATE
     WHERE fileid = cnFileId;
  sReturnValue VARCHAR2(4000);
BEGIN
  isr$trace.info('begin', 'begin', sCurrentName);
  OPEN cGetTemplateDescription;
  FETCH cGetTemplateDescription INTO sReturnValue;
  CLOSE cGetTemplateDescription;
  isr$trace.info('end', 'end', sCurrentName);
  RETURN sReturnValue;
END getTemplateDescription;

-- *****************************************************************************
procedure deleteTemplate (nFileId in isr$template.fileid%type) is

  oErrorObj         stb$oerror$record := STB$OERROR$RECORD();
  sCurrentName      CONSTANT VARCHAR2(100) := $$PLSQL_UNIT||'.deleteTemplate (' || nFileId || ')';
  sTemplateName     isr$template.templatename%type;
  nOutputId         isr$output$file.outputid%type;
  nRepTypeId        isr$report$output$type.reporttypeid%type;
  nCnt              number;

  cursor cGetOutput is
    select distinct iof.outputid, rot.reporttypeid, count(*)
    from isr$output$file iof, isr$report$output$type rot
    where iof.outputid = rot.outputid
    and iof.outputid in (select iof2.outputid from isr$output$file iof2 where templatename = sTemplateName)
    and rot.visible = 'T'
    group by iof.outputid, rot.reporttypeid;

begin
  isr$trace.stat('begin','begin', sCurrentName);

  sTemplateName := stb$object.getCurrentNode().sDisplay;
  open cGetOutput;
  fetch cGetOutput into nOutputId, nRepTypeId, nCnt;
  close cGetOutput;
  isr$trace.debug('sTemplateName, nFileId, nOutputId, nRepTypeId', sTemplateName || ', ' || nFileId || ', ' || nOutputId || ', ' || nRepTypeId, sCurrentName);

  if nCnt = 1 then
    delete from isr$action$output where outputid = nOutputId;
    delete from isr$output$property where outputid = nOutputId;
    delete from isr$output$transform where outputid = nOutputId;
    delete from isr$output$file where outputid = nOutputId;
    delete from isr$report$output$type where outputid = nOutputId;
  end if;

  delete from isr$output$file where fileid = nFileId;
  delete from isr$file$style$values where fileid = nFileId;
  delete from isr$file$calc$values where fileid = nFileId;
  delete from isr$file$parameter$values where fileid = nFileId;
  delete from stb$group$reptype$rights where parametername = 'TEMPLATE'||nFileId;
  delete from isr$template where fileid = nFileId;
  delete from isr$file where fileid = nFileId;

  oErrorObj := Stb$audit.openAudit ('REPORTTYPEAUDIT', TO_CHAR (nRepTypeId));
  oErrorObj := Stb$audit.addAuditRecord ( STB$OBJECT.getCurrentToken );
  oErrorObj := Stb$audit.addAuditEntry (UPPER (STB$OBJECT.getCurrentToken), 'Delete', STB$OBJECT.getCurrentNode().sDisplay, 1);
  oErrorObj := Stb$audit.silentAudit;

  commit;

  isr$trace.stat('end','end', sCurrentName);
end;

-- *****************************************************************************
function setMenuAccess( sparameter   in varchar2,
                        sMenuAllowed out varchar2 )
  return stb$oerror$record
is
  oErrorObj         stb$oerror$record := STB$OERROR$RECORD();
  sCurrentName      CONSTANT VARCHAR2(100) := $$PLSQL_UNIT||'.setMenuAccess (' || sparameter || ')';

  cursor cisHighestVersion(cnFileid in number) is
  select 'T'
    from isr$template
   where fileid = cnFileid
     and (templatename, version) in (select templatename, max (version)
                                       from isr$template
                                     group by templatename);

  cursor cGetRelease(cnFileid in number) is
  select release from isr$template
  where  fileid = cnFileid;
  sRelease   varchar2(1) := 'F';
begin
  isr$trace.stat('begin','begin', sCurrentName);

  sMenuAllowed := 'T';
  case
    when upper(sparameter) = 'CAN_EXPORT_TEMPLATE' then
      null;

    when upper(sparameter) in ('CAN_IMPORT_TEMPLATE', 'CAN_DELETE_TEMPLATE') then

      open cIsHighestVersion(to_number(STB$OBJECT.getCurrentNodeId));
      fetch cIsHighestVersion into sMenuAllowed;
      if cIsHighestVersion%notfound then
        sMenuAllowed := 'F';
      end if;
      close cIsHighestVersion;
      isr$trace.debug('sMenuAllowed', sMenuAllowed  , sCurrentName);

      if sMenuAllowed = 'T' and upper(sparameter) = 'CAN_DELETE_TEMPLATE' then
        open cGetRelease(to_number(STB$OBJECT.getCurrentNodeId));
        fetch cGetRelease into sRelease;
        close  cGetRelease;
        if sRelease = 'T' then
          sMenuAllowed := 'F';
        end if;
      end if;
      
    else     
      null;

  end case;
  isr$trace.stat('end','sMenuAllowed: '||sMenuAllowed, sCurrentName);
  return oErrorObj;
EXCEPTION
  WHEN OTHERS THEN
    oErrorObj.handleError ( Stb$typedef.cnSeverityCritical,  utd$msglib.getmsg ('exUnexpectedError', stb$security.getcurrentlanguage, sqlerrm(sqlcode)), sCurrentName,
       dbms_utility.format_error_stack ||stb$typedef.crlf|| dbms_utility.format_error_backtrace);
    RETURN oErrorObj;
end setMenuAccess;

-- *****************************************************************************
function callFunction( oParameter IN STB$MENUENTRY$RECORD,
                       olNodeList OUT STB$TREENODELIST  )
  return stb$oerror$record
is
  sCurrentName                  CONSTANT VARCHAR2(100) := $$PLSQL_UNIT||'.callFunction('||oParameter.sToken||')';
  oErrorObj                     stb$oerror$record := STB$OERROR$RECORD();
  oNodeBackup                   stb$treenode$record;
  exNotExists     exception;
  nFileId  isr$template.fileid%type;
  sChildrenTemplates varchar2(1000);
  childrenTemplatesExist  exception;

begin
  isr$trace.stat('begin','begin', sCurrentName);
  isr$trace.debug('value','oParameter',sCurrentName,oParameter);

  STB$OBJECT.setCurrentToken(oparameter.stoken);
  case
    -- *********************************
    when upper (oparameter.stoken) in ('CAN_EXPORT_TEMPLATE', 'CAN_IMPORT_TEMPLATE') then
      olnodelist := stb$treenodelist ();
      olnodelist.EXTEND;
      olnodelist (1) := STB$TREENODE$RECORD(STB$UTIL.getMaskTitle(Upper (oParameter.sToken)),
                                            'ISR$TEMPLATE$HANDLER', 'TRANSPORT', 'MASK');
      oErrorObj := STB$UTIL.getPropertyList(upper (oParameter.sToken), olNodeList (1).tloPropertylist);

    -- *********************************
    when upper (oparameter.stoken) = 'CAN_DELETE_TEMPLATE' then
      nFileId := to_number(stb$object.getCurrentNodeId);

      for cGetChildrenTemplates in (select templatename from isr$template where copied_from_fileid = nFileId) loop
        sChildrenTemplates := ', ''' || sChildrenTemplates || cGetChildrenTemplates.templatename || '''';
      end loop;
      if sChildrenTemplates is not null then
        raise childrenTemplatesExist;
      end if;

      deleteTemplate(nFileId);

      ISR$TREE$PACKAGE.selectCurrent();

    -- *********************************
    else
      raise exNotExists;
  end case;
  isr$trace.stat('end','end', sCurrentName);
  return oErrorObj;
exception
  when childrenTemplatesExist then
    oErrorObj.handleError (stb$typedef.cnseveritymessage, utd$msglib.getmsg ('childrenTemplatesExistText', stb$security.getcurrentlanguage, substr(sChildrenTemplates, 3)),
        sCurrentName, dbms_utility.format_error_stack ||stb$typedef.crlf || dbms_utility.format_error_backtrace  );
    return oErrorObj;
  when exNotExists then
    oErrorObj.handleError (stb$typedef.cnseveritymessage, utd$msglib.getmsg ('exNotExistsText', stb$security.getcurrentlanguage),
        sCurrentName, dbms_utility.format_error_stack ||stb$typedef.crlf || dbms_utility.format_error_backtrace  );
    return oErrorObj;
  when others then
    rollback;
    oErrorObj.handleError (stb$typedef.cnSeverityCritical, utd$msglib.getmsg ('exUnexpectedError', stb$security.getcurrentlanguage, sqlerrm(sqlcode)),
         sCurrentName, dbms_utility.format_error_stack ||stb$typedef.crlf || dbms_utility.format_error_backtrace  );
    return oErrorObj;
end callfunction;

-- *****************************************************************************
function putParameters (oparameter in stb$menuentry$record, olparameter in stb$property$list)
  return stb$oerror$record
is
  sCurrentName                  constant varchar2(100) := $$PLSQL_UNIT||'.putParameters('||oparameter.stoken||')';
  oErrorObj                     stb$oerror$record := STB$OERROR$RECORD();
  sMsg       varchar2(2000);
  exnotexists                   exception;
  exaudit                       exception;

begin
  isr$trace.stat('begin','begin', sCurrentName);
  isr$trace.debug('status oParameter', oParameter.toString('oParameter') ,sCurrentName, olparameter);

  stb$object.setCurrentToken(oparameter.stoken);

  oErrorObj := isr$reporttype$package.setjobparameter (upper (oparameter.stoken) );

  case
    -- *********************************
    when upper (oparameter.stoken) = 'CAN_EXPORT_TEMPLATE' then
      stb$job.setjobparameter ('ACTIONTYPE', 'SAVE', stb$job.njobid);
    -- *********************************
    when upper (oparameter.stoken) = 'CAN_IMPORT_TEMPLATE' then
      stb$job.setjobparameter ('ACTIONTYPE', 'UPLOAD', stb$job.njobid);
    -- *********************************
    else
      raise exnotexists;
  end case;

  FOR i IN 1 .. olparameter.count () LOOP
    stb$job.setjobparameter (olparameter (i).sparameter, olparameter (i).svalue, stb$job.njobid);
  END LOOP;

  oErrorObj := stb$job.startjob;

  isr$trace.stat('end','end', sCurrentName);
  return oErrorObj;
exception
  when exAudit then
    oErrorObj.sErrorCode := 'exAudit';
    oErrorObj.sSeverityCode := stb$typedef.cnaudit;
    return oErrorObj;
  when exNotExists then
    sMsg :=  utd$msglib.getmsg ('exNotExistsText', stb$security.getcurrentlanguage, sqlerrm(sqlcode));
    oErrorObj.handleError ( Stb$typedef.cnSeverityMessage, sMsg, sCurrentName,  dbms_utility.format_error_stack ||stb$typedef.crlf|| dbms_utility.format_error_backtrace  );
    return oErrorObj;
  when others then
    rollback;
    sMsg :=  utd$msglib.getmsg ('ERROR_PLACE', stb$security.getcurrentlanguage, sqlerrm(sqlcode));
    oErrorObj.handleError ( Stb$typedef.cnSeverityWarning, sMsg, sCurrentName,  dbms_utility.format_error_stack ||stb$typedef.crlf|| dbms_utility.format_error_backtrace  );
    return oErrorObj;
end putparameters;

-- *****************************************************************************
procedure setParentTemplate(cnFileId in isr$template.fileid%type, csTemplatename  in isr$template.templatename%type,
                            nParentFileId out isr$template.fileid%type, sParentTemplatename out isr$template.templatename%type)
is
  sCurrentName constant varchar2(150) := $$PLSQL_UNIT||'.setParentTemplate';

  cursor cGetCopiedTemplate(nCurFileid in number, sCurTemplatename in varchar2) is
  select fileid, templatename
  from isr$template t
  where connect_by_isleaf = 1
  start with fileid = nCurFileid and templatename = sCurTemplatename
  connect by prior copied_from_fileid = fileid;

  cursor cGetParentTemplate(nCurFileid in number, sCurTemplatename in varchar2) is
  select t2.fileid, t2.templatename
  from isr$template t1
  left outer join
  (select templatename, fileid, row_number() over(partition by templatename order by version) rn
   from isr$template )  t2
  on t1.templatename = t2.templatename
  where t1.fileid = nCurFileid
  and t1.templatename = sCurTemplatename
  and t2.rn=1;

begin
  isr$trace.stat('begin','begin', sCurrentName);

  nParentFileId := cnFileId;
  sParentTemplatename := csTemplatename;

  open cGetCopiedTemplate(cnFileId, csTemplatename);
  fetch cGetCopiedTemplate into nParentFileId, sParentTemplatename;
  close cGetCopiedTemplate;

  open cGetParentTemplate(nParentFileId, sParentTemplatename);
  fetch cGetParentTemplate into nParentFileId, sParentTemplatename;
  close cGetParentTemplate;

  isr$trace.stat('end','end', sCurrentName);

end setParentTemplate;

-- *****************************************************************************
/**
 this function is not used currently, but possibly later.
**/
function exportTemplateAsSnapshot(cnJobid IN number, cxXml IN OUT XMLTYPE) return STB$OERROR$RECORD
is
    sCurrentName                  constant varchar2(100) := $$PLSQL_UNIT||'.exportTemplateAsSnapshot('||cnJobid||')';
    oErrorObj                     stb$oerror$record := STB$OERROR$RECORD();
    sMsg       varchar2(2000);
    sFileId    stb$jobparameter.parametervalue%type;
    olTableList  isr$varchar$list;
    olCreateStmtList  isr$varchar$list;
    exCannotCreateTable   exception;
    cIncludeList   clob;
    cExcludeList    clob;
    bSnapshot   blob;
    sFileExtension  varchar2(32) := 'ISRTEMPLATE';

 --   local functions    ----

  function getTemplateTablesList return isr$varchar$list
    is
    sCurrentName  constant varchar2(100) := $$PLSQL_UNIT||'.getTemplateTablesList';
    olTableList   isr$varchar$list := isr$varchar$list();
  begin
    isr$trace.stat('begin','begin', sCurrentName);
    olTableList.extend;
    olTableList(1) := 'isr$file$imp';
    olTableList.extend;
    olTableList(2) := 'isr$template$imp';
    olTableList.extend;
    olTableList(3) := 'stb$group$reptype$rights$imp';
    olTableList.extend;
    olTableList(4) := 'isr$file$parameter$values$imp';
    olTableList.extend;
    olTableList(5) := 'isr$file$calc$values$imp';
    olTableList.extend;
    olTableList(6) := 'isr$file$style$values$imp';
    olTableList.extend;
    olTableList(7) := 'isr$report$output$type$imp';
    olTableList.extend;
    olTableList(8) := 'isr$output$transform$imp';
    olTableList.extend;
    olTableList(9) := 'isr$output$property$imp';
    olTableList.extend;
    olTableList(10) := 'isr$output$file$imp';
    olTableList.extend;
    olTableList(11) := 'isr$action$output$imp';

    isr$trace.stat('end','end', sCurrentName);
    return olTableList;

  end getTemplateTablesList;
-- **************************************************************************************************

  function getTemplateStmtList(cnJobid IN number, sFileId in varchar2)  return isr$varchar$list
    is
    sCurrentName  constant varchar2(100) := $$PLSQL_UNIT||'.getTemplateStmtList';
    olCreateStmtList   isr$varchar$list := isr$varchar$list();

    sReportTypeId   stb$jobparameter.parametervalue%type;

    CURSOR cGetOutput IS
      SELECT iof.outputid, iof.templatename
        FROM isr$output$file iof, isr$report$output$type rot
       WHERE iof.outputid = rot.outputid
         AND visible = 'T'
         AND fileid = sFileId;

    nOutputId      ISR$REPORT$OUTPUT$TYPE.OUTPUTID%TYPE;
    sTemplateName  ISR$TEMPLATE.TEMPLATENAME%TYPE;
  begin
    isr$trace.stat('begin','begin', sCurrentName);

    OPEN cGetOutput;
    FETCH cGetOutput INTO nOutputId, sTemplateName;
    CLOSE cGetOutput;

    sReportTypeId := STB$JOB.getJobParameter ('REPORTTYPEID', cnJobid);

    olCreateStmtList.extend;
    olCreateStmtList(1) := 'create table isr$file$imp as select * from isr$file where fileid = ' || sFileId;

    olCreateStmtList.extend;
    olCreateStmtList(2) := 'create table isr$template$imp as select * from isr$template where fileid = ' || sFileId ||
          ' and templatename = ''' || STB$JOB.getJobParameter ('REFERENCE_DISPLAY', cnJobid) || '''';
    olCreateStmtList.extend;
    olCreateStmtList(3) := 'create table stb$group$reptype$rights$imp as select * from stb$group$reptype$rights
           where parametername = ''TEMPLATE' || sFileId || '''';

    olCreateStmtList.extend;
    olCreateStmtList(4) := 'create table isr$file$parameter$values$imp  as select * from isr$file$parameter$values where fileid = ' || sFileId;

    olCreateStmtList.extend;
    olCreateStmtList(5) := 'create table isr$file$calc$values$imp as select * from isr$file$calc$values where fileid = ' || sFileId ;

    olCreateStmtList.extend;
    olCreateStmtList(6) := 'create table isr$file$style$values$imp as select * from isr$file$style$values where fileid = ' || sFileId;
    olCreateStmtList.extend;
    olCreateStmtList(7) := 'create table isr$report$output$type$imp as ' ||
        'select * from isr$report$output$type where outputid = ' || nOutputId;
    olCreateStmtList.extend;
    olCreateStmtList(8) := 'create table isr$output$transform$imp as select * from isr$output$transform where outputid = ' || nOutputId;
    olCreateStmtList.extend;
    olCreateStmtList(9) := 'create table isr$output$property$imp as select * from isr$output$property where outputid = ' || nOutputId;
    olCreateStmtList.extend;
    olCreateStmtList(10) := 'create table isr$output$file$imp as select * from isr$output$file where fileid = ' || sFileId || ' and outputid = ' || nOutputId;
    olCreateStmtList.extend;
    olCreateStmtList(11) := 'create table isr$action$output$imp as select * from isr$action$output where outputid = ' || nOutputId;

    isr$trace.stat('end','end', sCurrentName);
    return olCreateStmtList;
  end getTemplateStmtList;
  -- **************************************************************************************************

  function getIncludeList (cOlTableList in isr$varchar$list default null) return clob
    is
    sCurrentName  constant varchar2(100) := $$PLSQL_UNIT||'.getIncludeList';
    xList   xmltype;
    olTableList  isr$varchar$list;
  begin
    isr$trace.stat('end','end', sCurrentName);

    olTableList := cOlTableList;
    if olTableList is null then
      olTableList := getTemplateTablesList;
    end if;

    select xmlelement("ITEMS",
              xmlagg(xmlelement("ITEM",
              xmlattributes ('ISR' AS "MODULE", 'TABLE_DATA' AS "TYPE", upper(column_value) AS "NAME", 'NONE' AS "ACTION")
            )))
    into xList
    from table(olTableList);

    isr$trace.stat('end','end', sCurrentName);
    return xList.getClobVal();
  end getIncludeList;
  -- **************************************************************************************************

  function getExcludeList return clob
    is
    sCurrentName  constant varchar2(100) := $$PLSQL_UNIT||'.getExcludeList';
    xList   xmltype;
  begin
    isr$trace.stat('end','end', sCurrentName);

    select xmlelement("ITEMS", xmlelement("ITEM",
              xmlattributes ('ISR' AS "MODULE", 'DUMMY' AS "TYPE", 'DUMMY'  AS "NAME")
            ))
    into xList
    from dual;

    isr$trace.stat('end','end', sCurrentName);
    return xList.getClobVal();
  end getExcludeList;
  -- **************************************************************************************************

  procedure dropTemplateTables(olTableList in isr$varchar$list)
    is
    sCurrentName constant varchar2(100) := $$PLSQL_UNIT||'.dropTemplateTables';
  begin
    isr$trace.stat('begin','begin', sCurrentName);
    for i in 1..olTableList.count() loop
      begin
        execute immediate 'drop table ' || olTableList(i);
      exception
        when others then
          isr$trace.info('error in statement', 'drop table ' || olTableList(i), sCurrentName);
      end;
    end loop;
    isr$trace.stat('end','end', sCurrentName);
  end dropTemplateTables;
begin
  isr$trace.stat('begin','begin', sCurrentName);
  sFileId := STB$JOB.getJobParameter ('MASTERTEMPLATE', cnJobid);

  olTableList := getTemplateTablesList;
  cIncludeList := getIncludeList(olTableList);

  dropTemplateTables(olTableList);

  olCreateStmtList := getTemplateStmtList (cnJobId, sFileId);
  for i in 1..olCreateStmtList.count() loop
    begin
      isr$trace.debug('statement',olCreateStmtList(i), sCurrentName);
      execute immediate olCreateStmtList(i);
    exception
      when others then
        sMsg := 'statement: ' || olCreateStmtList(i) || '. ';
        raise exCannotCreateTable;
    end;
  end loop;

  cExcludeList := getExcludeList;

  isr$trace.debug('cIncludeList','', sCurrentName, cIncludeList);
  isr$trace.debug('cExcludeList','', sCurrentName, cExcludeList);

  bSnapshot := isr$snapshot.getSnapshotAsBlob(cIncludeList, cExcludeList);
  isr$trace.debug('bSnapshot','', sCurrentName, bSnapshot);

  insert into STB$JOBTRAIL (
       JOBID, DOCUMENTTYPE,
       MIMETYPE,DOCSIZE, TIMESTAMP, BINARYFILE,
       FILENAME, BLOBFLAG,
       ENTRYID, REFERENCE, DOWNLOAD,DOC_DEFINITION)
     (select
       cnJobId, 'TARGET','application/zip', DBMS_LOB.getlength(bSnapshot),
       sysdate, bSnapshot, regexp_replace(FILENAME, '\.[^.\\:*?"<>|\r\n]+$', '.' || sFileExtension), 'B' ,
       STB$JOBTRAIL$SEQ.nextval, to_char(fileid), 'T', DOC_DEFINITION
   from isr$file where fileid = sFileid);

  dropTemplateTables(olTableList);

  isr$trace.stat('end','end', sCurrentName);
  return oErrorObj;
exception
  when others then
    sMsg :=  sMsg || utd$msglib.getmsg ('ERROR_PLACE', stb$security.getcurrentlanguage, sqlerrm(sqlcode));
    oErrorObj.handleError ( Stb$typedef.cnSeverityCritical, sMsg, sCurrentName,  dbms_utility.format_error_stack ||stb$typedef.crlf|| dbms_utility.format_error_backtrace  );
    oErrorObj.sErrorMessage   := SQLERRM;
    SELECT ip, To_Char(port) INTO sIp, sPort FROM STB$JOBSESSION WHERE jobid = cnJobId;
    Stb$job.markDBJobAsBroken(cnJobId,sIp, sPort, oErrorObj.sErrorCode, oErrorObj.sErrorMessage,oErrorObj.sSeverityCode);
    return oErrorObj;
end exportTemplateAsSnapshot;

-- *****************************************************************************
function exportTemplate(cnJobid IN number, cxXml IN OUT XMLTYPE) return STB$OERROR$RECORD
is
  sCurrentName                  constant varchar2(100) := $$PLSQL_UNIT||'.exportTemplate('||cnJobid||')';
  oErrorObj                     stb$oerror$record := STB$OERROR$RECORD();
  sMsg       varchar2(2000);

  sFileId    stb$jobparameter.parametervalue%type;
  sTemplatename    stb$jobparameter.parametervalue%type;

  sParentFileId    isr$template.fileid%type;
  sParentTemplatename    isr$template.templatename%type;

  olTableList  isr$varchar$list;
  olCreateStmtList  isr$varchar$list;
  exCannotCreateTable   exception;
  cIncludeList   clob;
  cExcludeList    clob;
  xSnapshot    xmltype;
  bSnapshot   blob;
  sFileExtension  varchar2(32) := 'ISRTEMPLATE';

  CURSOR cGetOutput IS
  SELECT iof.outputid
    FROM isr$output$file iof, isr$report$output$type rot
   WHERE iof.outputid = rot.outputid
     AND visible = 'T'
     AND fileid = sFileId;

  nOutputId      ISR$REPORT$OUTPUT$TYPE.OUTPUTID%TYPE;

begin
  isr$trace.stat('begin','begin', sCurrentName);
  sFileId := STB$JOB.getJobParameter ('MASTERTEMPLATE', cnJobid);
  sTemplatename := STB$JOB.getJobParameter ('REFERENCE_DISPLAY', cnJobid);

  setParentTemplate(sFileId, sTemplatename, sParentFileId, sParentTemplatename);
  isr$trace.debug('sParentFileId, sParentTemplatename', sParentFileId || '; ' || sParentTemplatename, sCurrentName);

  OPEN cGetOutput;
  FETCH cGetOutput INTO nOutputId;
  CLOSE cGetOutput;

  select xmlelement (
    "EXPORT",
    xmlelement ("PARENTTEMPLATE", xmlelement("FILEID", sParentFileId), xmlelement("TEMPLATENAME", sParentTemplatename)),
    xmlelement ("TABLE", xmlattributes ('ISR$FILE' as "NAME"), dbms_xmlgen.getxmltype ('select * from isr$file where fileid = ' || sFileId)),
    xmlelement ("TABLE", xmlattributes ('ISR$TEMPLATE' as "NAME"), dbms_xmlgen.getxmltype ('select * from isr$template where fileid = ' || sFileId ||
               ' and templatename = ''' || sTemplatename || '''')),
    xmlelement ("TABLE", xmlattributes ('STB$GROUP$REPTYPE$RIGHTS' as "NAME"), dbms_xmlgen.getxmltype ('select * from stb$group$reptype$rights where parametername = ''TEMPLATE' || sFileId || '''')),
    xmlelement ("TABLE", xmlattributes ('ISR$FILE$PARAMETER$VALUES' as "NAME"), dbms_xmlgen.getxmltype ('select * from isr$file$parameter$values where fileid = ' || sFileId)),
    xmlelement ("TABLE", xmlattributes ('ISR$FILE$CALC$VALUES' as "NAME"), dbms_xmlgen.getxmltype ('select * from isr$file$calc$values where fileid = ' || sFileId )),
    xmlelement ("TABLE", xmlattributes ('ISR$FILE$STYLE$VALUES' as "NAME"), dbms_xmlgen.getxmltype ('select * from isr$file$style$values where fileid = ' || sFileId )),
    --xmlelement ("TABLE", xmlattributes ('ISR$REPORT$OUTPUT$TYPE' as "NAME"), dbms_xmlgen.getxmltype ('select * from isr$report$output$type where outputid = ' || nOutputId )),
    --xmlelement ("TABLE", xmlattributes ('ISR$OUTPUT$TRANSFORM' as "NAME"), dbms_xmlgen.getxmltype ('select * from isr$output$transform where outputid = ' || nOutputId )),
    --xmlelement ("TABLE", xmlattributes ('ISR$OUTPUT$PROPERTY' as "NAME"), dbms_xmlgen.getxmltype ('select * from isr$output$property where outputid = ' || nOutputId )),
    xmlelement ("TABLE", xmlattributes ('ISR$OUTPUT$FILE' as "NAME"), dbms_xmlgen.getxmltype ('select * from isr$output$file where fileid = ' || sFileId || ' and outputid = ' || nOutputId ))
   -- xmlelement ("TABLE", xmlattributes ('ISR$ACTION$OUTPUT' as "NAME"), dbms_xmlgen.getxmltype ('select * from isr$action$output where outputid = ' || nOutputId ))
    )
  into xSnapshot
  from dual;
  isr$trace.debug('bSnapshot','', sCurrentName, xSnapshot.getclobval());
  bSnapshot := xSnapshot.getBlobVal(nls_charset_id('AL32UTF8'));
  bSnapshot := stb$java.zip(bSnapshot, 'template.xml');

  insert into STB$JOBTRAIL (
       JOBID, DOCUMENTTYPE,
       MIMETYPE,DOCSIZE, TIMESTAMP, BINARYFILE,
       FILENAME, BLOBFLAG,
       ENTRYID, REFERENCE, DOWNLOAD,DOC_DEFINITION)
     (select
       cnJobId, 'TARGET','application/zip', DBMS_LOB.getlength(bSnapshot),
       sysdate, bSnapshot, regexp_replace(FILENAME, '\.[^.\\:*?"<>|\r\n]+$', '.' || sFileExtension), 'B' ,
       STB$JOBTRAIL$SEQ.nextval, to_char(fileid), 'T', DOC_DEFINITION
   from isr$file where fileid = sFileid);

  isr$trace.stat('end','end', sCurrentName);
  return oErrorObj;
exception
  when others then
    sMsg :=  sMsg || utd$msglib.getmsg ('ERROR_PLACE', stb$security.getcurrentlanguage, sqlerrm(sqlcode));
    oErrorObj.handleError ( Stb$typedef.cnSeverityCritical, sMsg, sCurrentName,  dbms_utility.format_error_stack ||stb$typedef.crlf|| dbms_utility.format_error_backtrace  );
    oErrorObj.sErrorMessage   := SQLERRM;
    SELECT ip, To_Char(port) INTO sIp, sPort FROM STB$JOBSESSION WHERE jobid = cnJobId;
    Stb$job.markDBJobAsBroken(cnJobId,sIp, sPort, oErrorObj.sErrorCode, oErrorObj.sErrorMessage,oErrorObj.sSeverityCode);
    return oErrorObj;
end exportTemplate;

-- *****************************************************************************
function insertDataToTmpTable(csTableName in varchar2, xSnapshot in xmltype) return stb$oerror$record
is
  sCurrentName constant varchar2(150) := $$PLSQL_UNIT||'.insertDataToTmpTable(' || csTableName || ')';
  oErrorObj                     stb$oerror$record := STB$OERROR$RECORD();
  sMsg       varchar2(2000);
  ctxContext            dbms_xmlstore.ctxType;
  nRowsInserted         number;
  sTmpTableName   varchar2(30) ;
  sForStmt   varchar2(500);
  xDataXML xmltype;

  cursor cIsTableExist is
  select 'T'
  from user_tables
  where table_name = upper(sTmpTableName);

  sIsTableExist  varchar2(1) := 'F';
  sStmt  varchar2(500);
  exCannotExecuteStmt  exception;
begin
  isr$trace.stat('begin','begin', sCurrentName);
  sTmpTableName := substr(csImpTablePrefix || csTableName, 0, 30);
  open cIsTableExist;
  fetch cIsTableExist into sIsTableExist;
  close cIsTableExist;

  if sIsTableExist = 'F' then
    sStmt := 'create global temporary table ' || sTmpTableName || ' as select * from ' || csTableName;
   -- sStmt := 'create table ' || sTmpTableName || ' as select * from ' || csTableName || ' where rownum=1';
    begin
      execute immediate sStmt;
    exception
      when others then
        sMsg := 'statement: ' || sStmt || '. Error: ' || sqlerrm;
        raise exCannotExecuteStmt;
    end;
  end if;

  /*sStmt := 'delete from ' || sTmpTableName;
  begin
      execute immediate sStmt;
    exception
      when others then
        sMsg := 'statement: ' || sStmt || '. Error: ' || sqlerrm;
        raise exCannotExecuteStmt;
    end; */

  sForStmt := 'for $row in /EXPORT/TABLE[@NAME="' || csTableName || '"]/ROWSET return $row';
  select COLUMN_VALUE 
    into xDataXML 
     from /*xmltable ('for $row in /EXPORT/TABLE[@NAME="' || csTableName || '"]/ROWSET return $row'
                      passing xSnapshot
                     );*/
          table (XMLSEQUENCE (EXTRACT ( (select XMLQUERY (sForStmt passing by value xSnapshot returning content) from DUAL), '/*')));               
  ctxContext := dbms_xmlstore.newContext(sTmpTableName);
  nRowsInserted := dbms_xmlstore.insertXML(ctxContext, xDataXML);

  dbms_xmlstore.closeContext(ctxContext);
  isr$trace.debug('rows inserted', nRowsInserted, sCurrentName);

  isr$trace.stat('end','end', sCurrentName);
  return oErrorObj;
exception when others then
    sMsg :=  sMsg || utd$msglib.getmsg ('ERROR_PLACE', stb$security.getcurrentlanguage, sqlerrm(sqlcode));
    oErrorObj.handleError ( Stb$typedef.cnSeverityCritical, sMsg, sCurrentName,
        dbms_utility.format_error_stack ||stb$typedef.crlf|| dbms_utility.format_error_backtrace  );
    return oErrorObj;
end insertDataToTmpTable;

-- *****************************************************************************
function importTemplate(cnJobid IN number, cxXml IN OUT XMLTYPE) return STB$OERROR$RECORD
is
  sCurrentName                  constant varchar2(100) := $$PLSQL_UNIT||'.exportTemplate('||cnJobid||')';
  oErrorObj                     stb$oerror$record := STB$OERROR$RECORD();
  sMsg       varchar2(4000);

  sStmt  varchar2(4000);
 -- olTableList  isr$varchar$list;
 -- cIncludeList   clob;
  bUnzipSnapshot   blob;
  xSnapshot  xmltype;
  sTableName varchar2(30);

  sCurrentFileId    stb$jobparameter.parametervalue%type;
  sCurrentTemplatename    stb$jobparameter.parametervalue%type;

  sIsTemplateValid  varchar2(1) := 'F';
  exParentTemplateNotMatch  exception;

  nParentFileId isr$template.fileid%type;
  sParentTemplatename isr$template.templatename%type;

  CURSOR cGetFile IS
  SELECT j.filename, binaryfile, dbms_lob.getlength(BINARYFILE) docsize
  FROM   STB$JOBTRAIL j
  WHERE  j.jobid = cnJobId
  AND    j.DOCUMENTTYPE = 'TARGET';

  rGetFile                cGetFile%rowtype;
  nNewFileId        isr$template.fileid%type;
  sNewTemplatename  isr$template.templatename%type;
  nNewVersion       isr$template.version%type;

  -- **** local functions ********************************************************************************
  procedure insertData
  is
    sLocalCurrentName varchar2(100) := sCurrentName || '.insertData';
  begin
    isr$trace.stat('begin', 'begin', sLocalCurrentName);
    isr$trace.debug('fileid', nNewFileId, sLocalCurrentName);

    if sTableName is not null then
      sStmt := sStmt || substr(csImpTablePrefix || sTableName, 0, 30);
    end if;
    isr$trace.debug('sStmt', sStmt, sLocalCurrentName);
    execute immediate sStmt;

    isr$trace.stat('end', 'end', sLocalCurrentName);
  exception
    when others then
    rollback;
    sMsg :=  sStmt || ' ' || utd$msglib.getmsg ('ERROR_PLACE', stb$security.getcurrentlanguage, sqlerrm(sqlcode));
    oErrorObj.handleError ( Stb$typedef.cnSeverityCritical, sMsg, sCurrentName,
        dbms_utility.format_error_stack ||stb$typedef.crlf|| dbms_utility.format_error_backtrace  );
  end insertData;

  -- *****************************************************************************
  procedure validateImport
  is
    sLocalCurrentName varchar2(100) := sCurrentName || '.validateImport';

    nParentFileId isr$template.fileid%type;
    sParentTemplatename isr$template.templatename%type;

    nExpFileId isr$template.fileid%type;
    sExpTemplatename isr$template.templatename%type;
  begin
    isr$trace.stat('begin', 'begin', sLocalCurrentName);

    setParentTemplate(sCurrentFileId, sCurrentTemplatename, nParentFileId, sParentTemplatename);
    isr$trace.debug('sParentFileId, sParentTemplatename', nParentFileId || '; ' || sParentTemplatename, sCurrentName);
    nExpFileId := isr$xml.ValueOf(xSnapshot, '/EXPORT/PARENTTEMPLATE/FILEID/text()');
    sExpTemplatename := isr$xml.ValueOf(xSnapshot, '/EXPORT/PARENTTEMPLATE/TEMPLATENAME/text()');
    isr$trace.debug('nExpFileId, sExpTemplatename', nExpFileId || '; ' || sExpTemplatename, sCurrentName);

    if nParentFileId = nExpFileId and sParentTemplatename = sExpTemplatename then
      sIsTemplateValid := 'T';
    end if;

    isr$trace.stat('end', sIsTemplateValid, sLocalCurrentName);
  end validateImport;

-- *****************************************************************************
begin
  isr$trace.stat('begin','begin', sCurrentName);

  sCurrentFileId := STB$JOB.getJobParameter ('MASTERTEMPLATE', cnJobid);
  sCurrentTemplatename := STB$JOB.getJobParameter ('REFERENCE_DISPLAY', cnJobid);
  sNewTemplatename := STB$JOB.getJobParameter ('TEMPLATENAME', cnJobid);

  open cGetFile;
  fetch cGetFile into rGetFile;
  close cGetFile;

  bUnzipSnapshot := stb$java.unzip(rGetFile.binaryfile);
  xSnapshot := xmltype (stb$util.blobToClob(bUnzipSnapshot));
  isr$trace.debug('bUnzipSnapshot as xmltype',rGetFile.filename || ' ' || rGetFile.docsize, sCurrentName, xSnapshot);

  validateImport;

  if sIsTemplateValid = 'F' then
    sMsg :=  utd$msglib.getmsg ('exParentTemplateNotMatch', stb$security.getcurrentlanguage);
    raise exParentTemplateNotMatch;
  end if;


  select SEQ$STB$FILE.NEXTVAL into nNewFileId FROM dual;

  sTableName := 'ISR$FILE';
  oErrorObj := insertDataToTmpTable(sTableName, xSnapshot);
  sStmt := 'INSERT INTO ISR$FILE (FILEID, FILENAME, USAGE, TEXTFILE, DESCRIPTION, BLOBFLAG, BINARYFILE, MIMETYPE, DOCSIZE, DOWNLOAD, DOC_DEFINITION)
            SELECT ' || nNewFileId || ', filename, usage, textfile, description, blobflag, binaryfile, mimetype, docsize, download, doc_definition
            FROM ';
  insertData;

  sTableName := 'ISR$TEMPLATE';
  oErrorObj := insertDataToTmpTable(sTableName, xSnapshot);
  
  select NVL(max(version), 0) + 1
    into nNewVersion
    from isr$template 
   where templatename = sNewTemplatename;
 
  sStmt := 'INSERT INTO ISR$TEMPLATE (TEMPLATENAME, FILEID, RELEASE, VERSION, VISIBLE, INSTALLER, ICON, EXCHANGE_COMMENT)
            SELECT ''' || sNewTemplatename || ''', ' || nNewFileId || ', ''F'', ' || nNewVersion || ', VISIBLE, ''F'', ICON,
                 ''' || STB$JOB.getJobParameter ('COMMENT', cnJobid) || '''
            FROM ' ;
  insertData;

  sTableName := 'STB$GROUP$REPTYPE$RIGHTS';
  oErrorObj := insertDataToTmpTable(sTableName, xSnapshot);
  sStmt := 'INSERT INTO STB$GROUP$REPTYPE$RIGHTS (REPORTTYPEID, PARAMETERNAME, PARAMETERVALUE, USERGROUPNO)
            SELECT REPORTTYPEID, ''TEMPLATE' || nNewFileId || ''', PARAMETERVALUE, USERGROUPNO
            FROM ';
  insertData;

  sTableName := 'ISR$FILE$PARAMETER$VALUES';
  oErrorObj := insertDataToTmpTable(sTableName, xSnapshot);
  sStmt := 'INSERT INTO ISR$FILE$PARAMETER$VALUES (REPORTTYPEID, FILEID, ENTITY, DISPLAY, VALUE, INFO, ORDERNUMBER, USE, VISIBLE, EDITABLE, STYLE, CLASS)
            SELECT DISTINCT REPORTTYPEID
                  , ' || nNewFileId || '
                  , ENTITY, DISPLAY, VALUE, INFO, ORDERNUMBER, USE, VISIBLE, EDITABLE, STYLE, CLASS
             FROM ';
  insertData;

  sTableName := 'ISR$FILE$CALC$VALUES';
  oErrorObj := insertDataToTmpTable(sTableName, xSnapshot);
  sStmt := 'INSERT INTO ISR$FILE$CALC$VALUES (REPORTTYPEID, FILEID, ENTITY, DISPLAY, VALUE, INFO, ORDERNUMBER, USE, VISIBLE, EDITABLE)
            SELECT DISTINCT REPORTTYPEID
                        , ' || nNewFileId || ', ENTITY, DISPLAY, VALUE, INFO, ORDERNUMBER, USE, VISIBLE, EDITABLE
            FROM  ';
  insertData;

  sTableName := 'ISR$FILE$STYLE$VALUES';
  oErrorObj := insertDataToTmpTable(sTableName, xSnapshot);
   sStmt := 'INSERT INTO ISR$FILE$STYLE$VALUES (REPORTTYPEID, FILEID, ENTITY, DISPLAY, VALUE, INFO, ORDERNUMBER, USE, VISIBLE, EDITABLE)
             SELECT REPORTTYPEID, ' || nNewFileId || ', ENTITY, DISPLAY, VALUE, INFO, ORDERNUMBER, USE, VISIBLE, EDITABLE
             FROM ';
  insertData;


 /* sTableName := 'ISR$OUTPUT$TRANSFORM';
  oErrorObj := insertDataToTmpTable(sTableName, xSnapshot);
  sStmt := 'INSERT INTO ISR$OUTPUT$TRANSFORM ( OUTPUTID, STYLESHEETID, RECREATE, PROTECTED, DOC_DEFINITION, PARAMETERS, EXECUTIONORDER, BASESTYLESHEET, ACTION, BOOKMARKNAME, KEYNAME)
            SELECT DISTINCT  outputid, STYLESHEETID, recreate, PROTECTED,
                    (select doc_definition from isr$report$output$type where outputid = outputid),
                    parameters, executionorder, basestylesheet, action, bookmarkname, keyname
            FROM ';
  insertData;

  sTableName := 'ISR$OUTPUT$PROPERTY';
  oErrorObj := insertDataToTmpTable(sTableName, xSnapshot);
  sStmt := 'INSERT INTO ISR$OUTPUT$PROPERTY ( OUTPUTID, IDENTIFIER, XPATH, SEPARATOR, DOC_DEFINITION)
            SELECT outputid, IDENTIFIER, XPATH, separator,  doc_definition
            FROM ';
  insertData;*/

  sTableName := 'ISR$OUTPUT$FILE';
  oErrorObj := insertDataToTmpTable(sTableName, xSnapshot);
  sStmt := 'INSERT INTO ISR$OUTPUT$FILE (OUTPUTID, FILEID, TEMPLATENAME)
            SELECT outputid, ' || nNewFileId || ', ''' || sNewTemplatename || '''
            FROM ';
  insertData;

 /* sTableName := 'ISR$ACTION$OUTPUT';
  oErrorObj := insertDataToTmpTable(sTableName, xSnapshot);
  sStmt := 'INSERT INTO ISR$ACTION$OUTPUT (ACTIONID, OUTPUTID, DOC_DEFINITION)
            SELECT actionid, outputid, doc_definition
            FROM ';
  insertData;*/
  
  commit;
  
  ISR$TREE$PACKAGE.sendSyncCall('CURRENTTEMPLATE', nNewFileId);

  isr$trace.stat('end','end', sCurrentName);
  return oErrorObj;
exception
  when others then
    rollback;
    --sMsg :=  utd$msglib.getmsg ('ERROR_PLACE', stb$security.getcurrentlanguage, sqlerrm(sqlcode));
    oErrorObj.handleError ( Stb$typedef.cnSeverityCritical, sMsg, sCurrentName,  dbms_utility.format_error_stack ||stb$typedef.crlf|| dbms_utility.format_error_backtrace  );
    oErrorObj.sErrorMessage   := sqlerrm;
    SELECT ip, To_Char(port) INTO sIp, sPort FROM STB$JOBSESSION WHERE jobid = cnJobId;
    Stb$job.markDBJobAsBroken(cnJobId,sIp, sPort, oErrorObj.sErrorCode, oErrorObj.sErrorMessage,oErrorObj.sSeverityCode);
    return oErrorObj;
end importTemplate;

procedure setAllowedTockenParameters (oParamListRec in out isr$paramlist$rec)
  is
    oErrorObj stb$oerror$record := STB$OERROR$RECORD();
    sCurrentName  constant varchar2(100) := $$PLSQL_UNIT||'.setAllowedTockenParameters';
    sIsHighestVersion number;
    sMsg              varchar2(4000);
  begin
    isr$trace.stat('begin', 'begin', sCurrentName);

  select count(*) into sIsHighestVersion
  from isr$template
  where fileid = TO_NUMBER(STB$OBJECT.getCurrentNodeId)
  and (templatename, version) in (select templatename, MAX (version)
                                     from isr$template
                                   group by templatename);


  if sIsHighestVersion<1 then
      oParamListRec.AddParam('sInsertAllowed', 'F');
      oParamListRec.AddParam('sUpdateAllowed', 'F');
      oParamListRec.AddParam('sDeleteAllowed', 'F');
  end if;

    isr$trace.stat('end', 'end', sCurrentName);
  exception
    when others then

    sMsg :=   ' ' || utd$msglib.getmsg ('ERROR_PLACE', stb$security.getcurrentlanguage, sqlerrm(sqlcode));
    oErrorObj.handleError ( Stb$typedef.cnSeverityCritical, sMsg, sCurrentName,
        dbms_utility.format_error_stack ||stb$typedef.crlf|| dbms_utility.format_error_backtrace  );
  end setAllowedTockenParameters;

procedure setMenuAllowed (sParameter   in  varchar2,sMenuAllowed  in out varchar2 )
  is
    oErrorObj stb$oerror$record := STB$OERROR$RECORD();
    sCurrentName  constant varchar2(100) := $$PLSQL_UNIT||'.setMenuAllowed';
    sIsHighestVersion number;
    sMsg              varchar2(4000);

  begin
    isr$trace.stat('begin','STB$OBJECT.getCurrentNodeId'|| STB$OBJECT.getCurrentNodeId,sCurrentName);

    sMenuAllowed := 'T';

    select count(*) into sIsHighestVersion
    from isr$template
    where fileid = TO_NUMBER(STB$OBJECT.getCurrentNodeId)
    and (templatename, version) in (select templatename, MAX (version)
                                     from isr$template
                                   group by templatename);


    if sIsHighestVersion<1 then
      sMenuAllowed := 'F';
    end if;
    IF sMenuAllowed = 'T' AND sParameter <> 'CAN_UPLOAD_NEW_TEMPLATE' THEN
          EXECUTE IMMEDIATE 'select case when count(*) > 0 then ''T'' else ''F'' end from isr$file$'||REPLACE(REPLACE(sPARAMETER, 'CAN_MODIFY_REPTYPE_'), '_VALUES')||'$values where fileid = '||STB$OBJECT.getCurrentNodeId
          INTO sMenuAllowed;
        END IF;

    isr$trace.debug('sMenuAllowed', sMenuAllowed  , sCurrentName);
    isr$trace.stat('end', 'end', sCurrentName);
  exception
    when others then
    sMsg :=   ' ' || utd$msglib.getmsg ('ERROR_PLACE', stb$security.getcurrentlanguage, sqlerrm(sqlcode));
    oErrorObj.handleError ( Stb$typedef.cnSeverityCritical, sMsg, sCurrentName,
        dbms_utility.format_error_stack ||stb$typedef.crlf|| dbms_utility.format_error_backtrace  );
  end setMenuAllowed;
END ISR$TEMPLATE$HANDLER;