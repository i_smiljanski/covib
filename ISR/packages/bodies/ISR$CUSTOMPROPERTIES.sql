CREATE OR REPLACE package body iSR$CustomProperties
is

-- variables and constants


-- ****************************************************************************************************
-- local functions and procedures
-- ****************************************************************************************************

-- **********************************************************************************************************************
FUNCTION getValuesProperties (lXpathes IN ISR$ATTRIBUTE$LIST
                            , sXml IN CLOB, cnJobId in NUMBER) RETURN CLOB 
-- ***************************************************************************************************
-- Date and Author: 17. May 2011, MCD
-- retrieves values for xpathes given in the list and return them in a clob
--
-- Parameter:
-- lXpathes     xpathes in a list
-- xXml         the XML from which the values are retrieved
--
-- ***************************************************************************************************                            
IS
  sCurrentName constant  varchar2(100) := $$PLSQL_UNIT||'.getValuesProperties';

  sStylesheet    VARCHAR2 (32000);
  
  cResult        CLOB;
  sParameter     VARCHAR2(500);
  sValue         VARCHAR2(500);
  sSeparator     VARCHAR2(50);
 
  sReturn        VARCHAR2(4000);
  
BEGIN
  iSR$Trace.stat('begin', 'begin', sCurrentName);

  sStylesheet := '<?xml version="1.0" encoding="UTF-8"?>' ||
                 '<xsl:stylesheet version="3.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform" xmlns:xs="http://www.w3.org/2001/XMLSchema" xmlns:fn="http://www.w3.org/2005/xpath-functions" exclude-result-prefixes="xsl xs fn">' ||
                 '  <xsl:output method="xml" indent="yes"/>' ||
                 '  <xsl:template match="/">' ||
                 '    <properties>';
  IF lXpathes.count > 0 THEN
    FOR i IN lXpathes.first..lXpathes.last LOOP
      sParameter := lXpathes(i).sValue;
      sValue     := lXpathes(i).sParameter;
      IF instr(sValue, '^', -1) > 0 THEN
        sSeparator := substr(sValue, 0, instr(sValue, '^', -1) - 1);
        sValue := substr(sValue, instr(sValue, '^', -1) + 1);
      END IF;
      
      IF instr(sValue, '[#94]', -1) > 0 THEN
        sValue := replace(substr(sValue, instr(sValue, '^', -1) + 1),'[#94]','^');
      END IF;
                  
      IF trim(sSeparator) IS NULL THEN 
        sSeparator := ' ';
      END IF;

      sStylesheet := sStylesheet ||
                     '<custom-property name="'|| sParameter ||'">' || 
                     '  <xsl:value-of separator="' || sSeparator || '" select="' || REPLACE(dbms_xmlgen.convert(sValue),'"',chr(39)) || '"/>' ||
                     '</custom-property>';
    END LOOP;
  END IF;               
  sStylesheet := sStylesheet ||
                 '    </properties>' ||
                 '  </xsl:template>' ||
                 '</xsl:stylesheet>';  
   isr$trace.debug('sStylesheet', 's. logclob', sCurrentName, sStylesheet);                
  sReturn := ISR$XML.XSLTTransform(sStylesheet, sXml, cResult, null, cnJobid);                   

  iSR$Trace.stat('end', 'end', sCurrentName);  
  RETURN cResult;
END getValuesProperties;

-- ****************************************************************************************************
-- public functions and procedures
-- ****************************************************************************************************

FUNCTION createPropertiesFile(cnJobId in NUMBER, cxXml IN OUT XMLTYPE) RETURN STB$OERROR$RECORD is

  sMsg                   varchar2(4000);
  sCurrentName constant  varchar2(100) := $$PLSQL_UNIT||'.createPropertiesFile';
  oErrorObj              STB$OERROR$RECORD := STB$OERROR$RECORD();
  
  csRohData    constant  VARCHAR2(100) := 'ROHDATEN';
  
  clRawData              CLOB;
  lXPathes               ISR$ATTRIBUTE$LIST := ISR$ATTRIBUTE$LIST();
  clProperties           CLOB;  
  
  nRecordID               NUMBER;
  
  -- information for error case
  sIp                     STB$JOBSESSION.IP%TYPE;
  sPort                   STB$JOBSESSION.PORT%TYPE;
  xProperties xmltype;
    
begin 
  iSR$Trace.stat('begin', 'cnJobId: '||cnJobId, sCurrentName);
  
  SELECT jt.Textfile
    INTO clRawData
    FROM STB$JOBTRAIL jt
   WHERE jt.jobid = cnJobID
     AND jt.documenttype = csRohData;

  -- *** custom properties
  SELECT CAST (
            MULTISET (
               SELECT P.separator||'^'||replace(P.xpath,'^','[#94]') key, P.identifier, '', null
                 FROM ISR$OUTPUT$PROPERTY P
                WHERE P.outputid = STB$JOB.getJobParameter ('OUTPUTID', cnJobid)
            ) AS ISR$ATTRIBUTE$LIST
         )
    INTO lXPathes
    FROM DUAL;
          
  IF lXPathes.COUNT() > 0 THEN
    isr$trace.debug('before get properties','xRawData in clob', sCurrentName, clRawData);
    clProperties := getValuesProperties (lXPathes, clRawData, cnJobId);
    isr$trace.debug('properties.xml','properties.xml', sCurrentName, clProperties);
    
    select STB$JOBTRAIL$SEQ.NEXTVAL into nRecordID from dual;
    INSERT INTO STB$JOBTRAIL (entryid
                            , jobid
                            , TIMESTAMP
                            , textfile
                            , docsize
                            , documenttype
                            , reference
                            , mimetype
                            , filename
                            , blobFlag
                            , download)
        VALUES (nRecordID
              , cnJobid
              , SYSDATE
              , clProperties
              , DBMS_LOB.getLength (clProperties)
              , 'PROPERTIES'
              , STB$JOB.getJobParameter ('REFERENCE', cnJobId)
              , 'text/xml'
              , 'properties.xml'
              , 'C'
              , Stb$util.getSystemParameter('DOWNLOADFLAG'));
    
    xProperties := ISR$XML.clobToXml(clProperties);

    DELETE FROM isr$report$grouping$tab
     WHERE (repid, groupingname) IN
                 (SELECT STB$JOB.getJobParameter ('REPID', cnJobId), sValue
                    FROM table (lXPathes)
                   WHERE TRIM(ISR$XML.valueOf (xProperties, '//custom-property[@name="'||sValue||'"]/text()')) IS NOT NULL);          
    INSERT INTO isr$report$grouping$tab(repid, groupingname, groupingvalue)
    (SELECT STB$JOB.getJobParameter('REPID', cnJobId), sValue, ISR$XML.valueOf(xProperties, '//custom-property[@name="'||sValue||'"]/text()')
       FROM table(lXPathes)
      WHERE TRIM(ISR$XML.valueOf(xProperties, '//custom-property[@name="'||sValue||'"]/text()')) IS NOT NULL);
      
    -- ISRC-1044
    for rGetRepid in (select r.repid, 'GRP$' || sValue groupingname, r.condition
                        from table (lXPathes), stb$report r
                       where r.repid = TO_NUMBER (STB$JOB.getJobParameter ('REPID', cnJobId))
                         and TRIM (ISR$XML.valueOf (xProperties, '//custom-property[@name="' || sValue || '"]/text()')) is not null) loop
      ISR$TRACE.debug('variables','rGetRepid.repid: '||rGetRepid.repid||',  rGetRepid.groupingname: '||rGetRepid.groupingname||',  rGetRepid.condition: '||rGetRepid.condition     ,sCurrentName);
      delete from ISR$REPORT$GROUPING$TABLE
       where repid = rGetRepid.repid
         and groupingname = rGetRepid.groupingname; 
      if rGetRepid.condition != 0 then            
        insert into ISR$REPORT$GROUPING$TABLE (GROUPINGNAME
                                             , GROUPINGID
                                             , GROUPINGVALUE
                                             , GROUPINGDATE
                                             , REPID)
          (select rg.GROUPINGNAME
                , rg.GROUPINGID
                , rg.GROUPINGVALUE
                , rg.GROUPINGDATE
                , rg.REPID
             from isr$report$grouping rg
            where rg.repid = rGetRepid.repid
              and groupingname = rGetRepid.groupingname);
      end if;        
    end loop;      

    COMMIT;     
    isr$trace.debug('get properties finished','properties.xml', sCurrentName, clProperties);
  END IF;  
  
  iSR$Trace.stat('end', 'end', sCurrentName);  
  RETURN oErrorObj;

exception
  when others then 
    sMsg :=  utd$msglib.getmsg ('exUnexpectedError', stb$security.getcurrentlanguage, csP1 => sqlerrm(sqlcode));
    oErrorObj.handleError ( Stb$typedef.cnSeverityCritical, sMsg, sCurrentName, 
                            dbms_utility.format_error_stack || CHR(10) || dbms_utility.format_error_backtrace  );
    SELECT ip, to_char(port) INTO sIp, sPort FROM STB$JOBSESSION WHERE jobid = cnJobId;
    Stb$job.markDBJobAsBroken(cnJobId,sIp, sPort, oErrorObj.sErrorCode, oErrorObj.sErrorMessage,oErrorObj.sSeverityCode);                              
    return oErrorObj;
end createPropertiesFile;
    
end iSR$CustomProperties;