CREATE OR REPLACE PACKAGE BODY ISR$UTD$PACKAGE is

  --sCurrentLal         constant VARCHAR2(100) := STB$UTIL.getCurrentLal;

  CURSOR cGetCrit (nRepID number, csEntity varchar2) IS
    SELECT entity, rtrim(xmlagg(xmlelement(e,display,'_').extract('//text()') order by ordernumber).GetStringVal(),'_') display
    FROM ISR$CRIT
    WHERE repid = nRepID -- STB$OBJECT.getCurrentRepid - CurrentRepID is not always available - ARDIS-435
    and entity=csEntity
    group by entity
    ;

  CURSOR cGetFirstCrit (nRepID number, csEntity varchar2) IS
    SELECT entity, display
    FROM ISR$CRIT
    WHERE repid = nRepID
    and entity=csEntity
    and rownum = 1
    ;

  CURSOR cGetFirstCritInfo (nRepID number, csEntity varchar2) IS
    SELECT entity, info
    FROM ISR$CRIT
    WHERE repid = nRepID
    and entity=csEntity
    and rownum = 1
    ;

  CURSOR cGetCritInfo (nRepID number, csEntity varchar2) IS
    SELECT entity, rtrim(xmlagg(xmlelement(e,info,'_').extract('//text()') order by ordernumber).GetStringVal(),'_') info
    FROM ISR$CRIT
    WHERE repid = nRepID -- STB$OBJECT.getCurrentRepid - CurrentRepID is not always available - ARDIS-435
    and entity=csEntity
    group by entity
    ;

  CURSOR cGetReportParameter (nRepID number)  IS
    SELECT parametername, parametervalue
    FROM stb$reportparameter
    WHERE repid = nRepID; -- STB$OBJECT.getCurrentRepid - CurrentRepID is not always available - ARDIS-435

  CURSOR cGetCritEntities (nRepID number) IS
    SELECT distinct entity
    FROM   ISR$CRIT
    WHERE  repid = nRepID;


--************************************************************************************************************************************
PROCEDURE getCustomIcon (olNodeList IN OUT STB$TREENODELIST, sNodeType IN VARCHAR2 DEFAULT NULL)
IS
  oErrorObj      STB$OERROR$RECORD := STB$OERROR$RECORD();
  sCurrentName   constant varchar2(4000) := $$PLSQL_UNIT||'.getCustomIcon('|| sNodeType ||')';

  CURSOR cCheckExtraEsig(csReference IN VARCHAR2) IS
    SELECT mod(count(*),2) modulo
      FROM stb$esig e
     WHERE csReference = e.REFERENCE
       AND e.action LIKE 'CAN_%LOCK_REPORT'
       AND e.success = 'T';

  rCheckExtraEsig cCheckExtraEsig%rowtype;

  CURSOR cGetJobIcon(csReference IN VARCHAR2) IS
    SELECT DISTINCT  CASE
                        WHEN NVL (jp.parametervalue, 'F') = 'T' THEN 'deactivated'
                        WHEN NVL(us.state, 'F') = 'SCHEDULED' THEN 'scheduled'
                     END icon
      FROM stb$jobparameter jp
         , stb$jobparameter jp1
         , stb$jobqueue jq
         , user_scheduler_jobs us
         , stb$report r
     WHERE r.repid = jp1.parametervalue
       AND jp1.parametername = 'REPID'
       AND jp.parametername IN ('INVISIBLE_FILTER_INACTIVE', 'FILTER_INACTIVE')
       AND jp.jobid = jp1.jobid
       AND r.title = csReference
       AND jq.jobid in (jp.jobid, jp1.jobid)
       AND TRIM(us.job_name) = TRIM(jq.oraclejobid)
     ORDER BY 1 asc nulls last;

  rGetJobIcon cGetJobIcon%rowtype;

  CURSOR cCheckAppendToIcon(csReference IN VARCHAR2) IS
    SELECT groupingvalue, CASE WHEN INSTR (rt.groupingname, ':') = 0 THEN NULL ELSE SUBSTR (rt.groupingname, INSTR (rt.groupingname, ':') + 1) END required_value
      FROM isr$report$grouping$tab rgt, stb$report r, (SELECT parametervalue groupingname, reporttypeid
                                                         FROM stb$reptypeparameter
                                                        WHERE parametername = 'APPEND_TO_ICON') rt
     WHERE rgt.repid = r.repid
       AND r.title = csReference
       AND r.reporttypeid = rt.reporttypeid
       AND rgt.groupingname = CASE WHEN INSTR (rt.groupingname, ':') = 0 THEN rt.groupingname ELSE SUBSTR (rt.groupingname, 0, INSTR (rt.groupingname, ':') - 1) END;

  rCheckAppendToIcon cCheckAppendToIcon%rowtype; 

  cursor csExistsAppendToIcon is
    SELECT 'T'
      FROM isr$report$grouping$table rgt
         , stb$report r
         , (SELECT parametervalue groupingname, reporttypeid
              FROM stb$reptypeparameter
             WHERE parametername = 'APPEND_TO_ICON') rt
         , table(olNodeList) nodes
     WHERE rgt.repid = r.repid
       AND r.title = nodes.sNodeId
       AND r.reporttypeid = rt.reporttypeid
       AND rgt.groupingname = CASE WHEN INSTR (rt.groupingname, ':') = 0 THEN rt.groupingname ELSE SUBSTR (rt.groupingname, 0, INSTR (rt.groupingname, ':') - 1) END;

  sExistsAppendToIcon   VARCHAR2(1) := 'F'; 

  cursor csUseJobIcon is
    SELECT 'T'
      FROM stb$jobparameter jp
         , stb$jobparameter jp1
         , stb$jobqueue jq
         , user_scheduler_jobs us
         , stb$report r
         , table(olNodeList) nodes
     WHERE r.repid = jp1.parametervalue
       AND jp1.parametername = 'REPID'
       AND jp.parametername IN ('INVISIBLE_FILTER_INACTIVE', 'FILTER_INACTIVE')
       AND jp.jobid = jp1.jobid
       AND r.title = nodes.sNodeId
       AND jq.jobid in (jp.jobid, jp1.jobid)
       AND TRIM(us.job_name) = TRIM(jq.oraclejobid)
       AND (NVL(us.state, 'F') = 'SCHEDULED' /* scheduled */ OR NVL (jp.parametervalue, 'F') = 'T' /*deactivated*/);

  sUseJobIcon           VARCHAR2(1) := 'F';

  cursor csExistsExtraEsig is
    select 'T'
      from stb$esig e, table(olNodeList) nodes
     WHERE nodes.sNodeId = e.REFERENCE
       AND e.action LIKE 'CAN_%LOCK_REPORT'
       AND e.success = 'T';

  sExistsExtraEsig      VARCHAR2(1) := 'F';

   -- ISRC-1051, ABBIS-83
  cursor csGetTitleIcon (csTitle in varchar2) is
    select iconid
    from   isr$icon
    join   stb$report
      on   'TITLE_'||stb$report.reportTypeid = isr$icon.iconid
     and   stb$report.title = csTitle;

   rTitleIcon csGetTitleIcon%rowtype;
   sTitleIcon VARCHAR2(500) := 'TITLE_BLANK'; 


BEGIN
  isr$trace.stat('begin', 'begin', sCurrentName);

  /* ISRC-816 check if to use special icons anywhere before using the loop */ 

  open csExistsAppendToIcon;
  fetch csExistsAppendToIcon into sExistsAppendToIcon;
  if csExistsAppendToIcon%notfound then
    sExistsAppendToIcon := 'F';
  end if;
  close csExistsAppendToIcon;

  open csUseJobIcon;
  fetch csUseJobIcon into sUseJobIcon;
  if csUseJobIcon%notfound then
    sUseJobIcon := 'F';
  end if;
  close csUseJobIcon; 

  open csExistsExtraEsig;
  fetch csExistsExtraEsig into sExistsExtraEsig;
  if csExistsExtraEsig%notfound then
    sExistsExtraEsig := 'F';
  end if;
  close csExistsExtraEsig; 

  isr$trace.debug ('sExistsExtraEsig', sExistsExtraEsig, sCurrentName);
  isr$trace.debug ('sUseJobIcon', sUseJobIcon, sCurrentName);
  isr$trace.debug ('sExistsAppendToIcon', sExistsAppendToIcon, sCurrentName);

 /* IF (sNodeType = 'TITLE') THEN
     -- ISRC-1051, ABBIS-83 Icon is wrong since the parent of this procedure is determining it wrongly
     -- all nodes in this list are of same NODETYPE
       open csGetTitleIcon(olNodeList(1).sNodeId);
       fetch csGetTitleIcon into rTitleIcon;
       if csGetTitleIcon%found then
           sTitleIcon := rTitleIcon.iconid;
       end if;
       close csGetTitleIcon; 

    FOR i IN 1..olNodeList.COUNT() LOOP
        olNodeList(i).sIcon := sTitleIcon;
    END LOOP;
  END IF;*/

  IF (   (sNodeType = 'TITLE' AND (sExistsAppendToIcon = 'T' or sUseJobIcon = 'T'))
      OR (sNodeType = 'REPORT' AND sExistsExtraEsig = 'T') 
      OR sNodeType IS NULL) THEN

    FOR i IN 1..olNodeList.COUNT() LOOP

      isr$trace.debug ('sNodeType',  olNodeList(i).sNodeType, sCurrentName);
      isr$trace.debug ('sReference',  olNodeList(i).sNodeId, sCurrentName);

      CASE
        WHEN olNodeList(i).sNodeType = 'REPORT' THEN

          OPEN cCheckExtraEsig(olNodeList(i).sNodeId);
          FETCH cCheckExtraEsig INTO rCheckExtraEsig;
          IF rCheckExtraEsig.modulo = 1 THEN
            olNodeList(i).sIcon := 'CAN_LOCK_REPORT';
          END IF;
          CLOSE cCheckExtraEsig;

        WHEN olNodeList(i).sNodeType = 'TITLE' THEN

          OPEN cGetJobIcon(olNodeList(i).sNodeId);
          FETCH cGetJobIcon INTO rGetJobIcon;
          IF cGetJobIcon%FOUND AND TRIM(rGetJobIcon.icon) IS NOT NULL THEN
            olNodeList(i).sIcon := olNodeList(i).sIcon || '_' || rGetJobIcon.icon;
          END IF;
          CLOSE cGetJobIcon;

          OPEN cCheckAppendToIcon(olNodeList(i).sNodeId);
          FETCH cCheckAppendToIcon INTO rCheckAppendToIcon;
          IF cCheckAppendToIcon%FOUND THEN
            IF rCheckAppendToIcon.required_value is not null AND rCheckAppendToIcon.required_value = rCheckAppendToIcon.groupingvalue 
            OR rCheckAppendToIcon.required_value is null THEN
              olNodeList(i).sIcon := olNodeList(i).sIcon || '_' || rCheckAppendToIcon.groupingvalue;
            END IF;
          END IF;
          CLOSE cCheckAppendToIcon;

        ELSE
          NULL;

      END CASE;

      isr$trace.debug ('olNodeList('||i||').sIcon', olNodeList(i).sIcon, sCurrentName);

    END LOOP;

  END IF;

  isr$trace.stat('end', 'end', sCurrentName);
EXCEPTION
  WHEN OTHERS THEN
      oErrorObj.handleError ( Stb$typedef.cnSeverityCritical, utd$msglib.getmsg ('exUnexpectedError', stb$security.getCurrentlanguage,  csP1 => sqlerrm(sqlcode)), 
        sCurrentName, dbms_utility.format_error_stack ||chr(10)|| dbms_utility.format_error_backtrace );
END getCustomIcon;

--************************************************************************************************************************************
FUNCTION createSRNumber RETURN VARCHAR2
is
  sSRNumber    VARCHAR2(4000);
  sCurrentName constant varchar2(4000) := $$PLSQL_UNIT||'.createSRNumber('||' '||')';
  oErrorObj    STB$OERROR$RECORD := STB$OERROR$RECORD();

  CURSOR cGetNewSRNumber is
    select c.display || '-' || to_char(Stb$object.getCurrentRepId)
      from isr$crit c, isr$report$wizard w, stb$report r
     where r.repid = Stb$object.getCurrentRepId
       and r.reporttypeid = w.reporttypeid
       and w.maskno = 1
       and c.repid = r.repid
       and c.entity = w.entity;

BEGIN
  isr$trace.stat('begin', 'begin', sCurrentName);

  sSRNumber := STB$UTIL.getReptypeParameter(STB$OBJECT.getCurrentReporttypeId, 'SRNUMBER');

  sSRNumber := REPLACE(sSRNumber,'%REPID%',STB$OBJECT.getCurrentRepid);
  sSRNumber := REPLACE(sSRNumber,'%DATE%',to_char(sysdate, STB$UTIL.getSystemParameter('DATEFORMAT')));

-- replace identifieres in "Template" (like %SB$PRODUCT%) only if the entity is referenced in that template, ABBIS-256
-- wahrscheinlich kann man sich die LOOP sparen

  FOR rEntities in cGetCritEntities(STB$OBJECT.getCurrentRepid) LOOP
    if INSTR( sSRNumber, rEntities.entity) > 0 THEN

      FOR rGetCrit IN cGetFirstCrit(STB$OBJECT.getCurrentRepid, rEntities.entity) LOOP
        sSRNumber := REPLACE(sSRNumber, '%FIRST_'||rGetCrit.entity||'%', REPLACE(rGetCrit.display, '''', ''''''));
      END LOOP;

      FOR rGetCrit IN cGetFirstCritInfo(STB$OBJECT.getCurrentRepid, rEntities.entity) LOOP
        sSRNumber := REPLACE(sSRNumber, '%FIRST_'||rGetCrit.entity||'_INFO%', REPLACE(rGetCrit.info, '''', ''''''));
      END LOOP;

      FOR rGetCrit IN cGetCrit(STB$OBJECT.getCurrentRepid, rEntities.entity) LOOP
        sSRNumber := REPLACE(sSRNumber, '%'||rGetCrit.entity||'%', rGetCrit.display);
      END LOOP;

      FOR rGetCrit IN cGetCritInfo(STB$OBJECT.getCurrentRepid, rEntities.entity) LOOP
        sSRNumber := REPLACE(sSRNumber, '%'||rGetCrit.entity||'_INFO%', rGetCrit.info);
      END LOOP;

     END IF;
  END LOOP;

  FOR rGetReportParameter IN cGetReportParameter(STB$OBJECT.getCurrentRepid) LOOP
    sSRNumber := REPLACE(sSRNumber, '%'||rGetReportParameter.parametername||'%', rGetReportParameter.parametervalue);
  END LOOP;

  isr$trace.debug('sSRNumber',sSRNumber,sCurrentName);

  IF Trim(sSRNumber) IS NULL THEN
    OPEN cGetNewSRNumber;
    FETCH cGetNewSRNumber into sSrNumber;
    CLOSE cGetNewSRNumber;
  END IF;

  isr$trace.stat('end', 'end', sCurrentName);
  RETURN NVL(sSRNumber,'NO_SRNUMBER');
EXCEPTION
  WHEN OTHERS THEN
      oErrorObj.handleError ( Stb$typedef.cnSeverityCritical, utd$msglib.getmsg ('exUnexpectedError', stb$security.getCurrentlanguage,  csP1 => sqlerrm(sqlcode)), 
        sCurrentName, dbms_utility.format_error_stack ||chr(10)|| dbms_utility.format_error_backtrace );
    RETURN 'SRNUMBER_ERR';

END createSRNumber;

--************************************************************************************************************************************
FUNCTION createTitle RETURN VARCHAR2
is
  sCurrentName   constant varchar2(4000) := $$PLSQL_UNIT||'.createTitle';
  sTitle         VARCHAR2(4000);
  oErrorObj      STB$OERROR$RECORD := STB$OERROR$RECORD();

  CURSOR cGetNewTitle is
    select c.display || '_' || to_char(sysdate, STB$UTIL.getSystemParameter('DATEFORMAT'))
      from isr$crit c, isr$report$wizard w, stb$report r
     where r.repid = Stb$object.getCurrentRepId
       and r.reporttypeid = w.reporttypeid
       and w.maskno = 1
       and c.repid = r.repid
       and c.entity = w.entity;
BEGIN
  isr$trace.stat('begin', 'begin', sCurrentName);

  sTitle := STB$UTIL.getReptypeParameter(STB$OBJECT.getCurrentReporttypeId, 'TITLE');

  sTitle := REPLACE(sTitle,'%REPID%',STB$OBJECT.getCurrentRepid);
  sTitle := REPLACE(sTitle,'%DATE%',to_char(sysdate, STB$UTIL.getSystemParameter('DATEFORMAT')));

  -- replace identifieres in "Template" (like %SB$PRODUCT%) only if the entity is referenced in that template, ABBIS-256
  FOR rEntities in cGetCritEntities(STB$OBJECT.getCurrentRepid) LOOP
    if INSTR( sTitle, rEntities.entity) > 0 THEN

      FOR rGetCrit IN cGetFirstCrit(STB$OBJECT.getCurrentRepid, rEntities.entity) LOOP
        sTitle := REPLACE(sTitle, '%FIRST_'||rGetCrit.entity||'%', REPLACE(rGetCrit.display, '''', ''''''));
      END LOOP;

      FOR rGetCrit IN cGetFirstCritInfo(STB$OBJECT.getCurrentRepid, rEntities.entity) LOOP
        sTitle := REPLACE(sTitle, '%FIRST_'||rGetCrit.entity||'_INFO%', REPLACE(rGetCrit.info, '''', ''''''));
      END LOOP;

      FOR rGetCrit IN cGetCrit(STB$OBJECT.getCurrentRepid, rEntities.entity) LOOP
        sTitle := REPLACE(sTitle, '%'||rGetCrit.entity||'%', REPLACE(rGetCrit.display, '''', ''''''));
      END LOOP;

      FOR rGetCrit IN cGetCritInfo(STB$OBJECT.getCurrentRepid, rEntities.entity) LOOP
        sTitle := REPLACE(sTitle, '%'||rGetCrit.entity||'_INFO%', REPLACE(rGetCrit.info, '''', ''''''));
      END LOOP;

    END IF;

  END LOOP;

  FOR rGetReportParameter IN cGetReportParameter(STB$OBJECT.getCurrentRepid) LOOP
    sTitle := REPLACE(sTitle, '%'||rGetReportParameter.parametername||'%', rGetReportParameter.parametervalue);
  END LOOP;


  isr$trace.debug('sTitle',sTitle,sCurrentName);

  BEGIN
    execute immediate 'select '||sTitle||' from dual' into sTitle;
  EXCEPTION WHEN OTHERS THEN
    NULL;
  END;

  IF Trim(sTitle) IS NULL THEN
    OPEN cGetNewTitle;
    FETCH cGetNewTitle into sTitle;
    CLOSE cGetNewTitle; 
  END IF;

  isr$trace.stat('end', 'end', sCurrentName);
  RETURN NVL(sTitle,'TITLE_EMPTY'); -- ABBIS-256
EXCEPTION
  WHEN OTHERS THEN
      oErrorObj.handleError ( Stb$typedef.cnSeverityCritical, utd$msglib.getmsg ('exUnexpectedError', stb$security.getCurrentlanguage,  csP1 => sqlerrm(sqlcode)), 
        sCurrentName, dbms_utility.format_error_stack ||chr(10)|| dbms_utility.format_error_backtrace );
    RETURN 'TITLE_ERR';
END createTitle;


--************************************************************************************************************************************
FUNCTION createNameForParameter(cnRepid IN NUMBER, cnDocid IN NUMBER, csNameParameter in varchar2) RETURN VARCHAR2
is
  sCurrentName   constant varchar2(4000) := $$PLSQL_UNIT||'.createNameForParameter('|| csNameParameter ||')';
  sDocumentName  VARCHAR2(4000);
  nReporttypeId  NUMBER;
  sSRNumber      STB$REPORT.SRNUMBER%TYPE;
  sTitle         STB$REPORT.TITLE%TYPE;
  oErrorObj      STB$OERROR$RECORD := STB$OERROR$RECORD();

  CURSOR cGetReporttypeId(nRepid IN NUMBER) IS
    select reporttypeid, srnumber, title
      from stb$report
     where repid = nRepid;

  csFileSigns           CONSTANT VARCHAR2(4000) := '_*\/:?"<>|';
BEGIN
  isr$trace.stat('begin', 'begin', sCurrentName);

  OPEN cGetReporttypeId(cnRepid);
  FETCH cGetReporttypeId INTO nReporttypeId, sSRNumber, sTitle;
  CLOSE cGetReporttypeId;

  sDocumentName := STB$UTIL.getReptypeParameter(nReporttypeId, csNameParameter);

  sDocumentName := REPLACE(sDocumentName,'%REPID%',cnRepid);
  sDocumentName := REPLACE(sDocumentName,'%DOCID%',cnDocid);
  sDocumentName := REPLACE(sDocumentName,'%SRNUMBER%',REPLACE(sSRNumber, '''', ''''''));
  sDocumentName := REPLACE(sDocumentName,'%TITLE%',REPLACE(sTitle, '''', ''''''));
  sDocumentName := REPLACE(sDocumentName,'%DATE%',to_char(sysdate, STB$UTIL.getSystemParameter('DATEFORMAT')));

 -- replace identifieres in "Template" (like %SB$PRODUCT%) only if the entity is referenced in that template, ABBIS-256
  FOR rEntities in cGetCritEntities(cnRepid) LOOP
    if INSTR( sDocumentName, rEntities.entity) > 0 THEN

      FOR rGetCrit IN cGetFirstCrit(cnRepid, rEntities.entity) LOOP
        sDocumentName := REPLACE(sDocumentName, '%FIRST_'||rGetCrit.entity||'%', REPLACE(rGetCrit.display, '''', ''''''));
      END LOOP;

      FOR rGetCrit IN cGetFirstCritInfo(cnRepid, rEntities.entity) LOOP
        sDocumentName := REPLACE(sDocumentName, '%FIRST_'||rGetCrit.entity||'%', REPLACE(rGetCrit.info, '''', ''''''));
      END LOOP;


      FOR rGetCrit IN cGetCrit(cnRepid, rEntities.entity) LOOP
        sDocumentName := REPLACE(sDocumentName, '%'||rGetCrit.entity||'%', REPLACE(rGetCrit.display, '''', ''''''));

      END LOOP;

      FOR rGetCrit IN cGetCritInfo(cnRepid, rEntities.entity) LOOP
        sDocumentName := REPLACE(sDocumentName, '%'||rGetCrit.entity||'_INFO%', REPLACE(rGetCrit.info, '''', ''''''));
      END LOOP;

    END IF;
  END LOOP;

  FOR rGetReportParameter IN cGetReportParameter(cnRepid) LOOP
    sDocumentName := REPLACE(sDocumentName, '%'||rGetReportParameter.parametername||'%', rGetReportParameter.parametervalue);
  END LOOP;
  isr$trace.debug('sDocumentName', sDocumentName, sCurrentName);

  BEGIN
    execute immediate 'select '||sDocumentName||' from dual' into sDocumentName;
  EXCEPTION WHEN OTHERS THEN
    NULL;
  END;

  sDocumentName := REPLACE(Translate(sDocumentName, csFileSigns, '-'), ' ');

  isr$trace.stat('end', 'end', sCurrentName);
  RETURN NVL(sDocumentName,'NO_VALUE'); --ABBIS-256
EXCEPTION
  WHEN OTHERS THEN
      oErrorObj.handleError ( Stb$typedef.cnSeverityCritical, utd$msglib.getmsg ('exUnexpectedError', stb$security.getCurrentlanguage,  csP1 => sqlerrm(sqlcode)), 
        sCurrentName, dbms_utility.format_error_stack ||chr(10)|| dbms_utility.format_error_backtrace );
    RETURN 'VALUE_ERR';
END createNameForParameter;


--************************************************************************************************************************************
FUNCTION createDocumentName(cnRepid IN NUMBER, cnDocid IN NUMBER) RETURN VARCHAR2
is
  sCurrentName   constant varchar2(4000) := $$PLSQL_UNIT||'.createDocumentName('||' '||')';
  sDocumentName  VARCHAR2(4000);

BEGIN
  isr$trace.stat('begin', 'begin', sCurrentName);

  sDocumentName := isr$utd$package.createNameForParameter(cnRepid, cnDocid, 'DOCUMENT_NAME');

  isr$trace.stat('end', 'end', sCurrentName);
  RETURN sDocumentName;

END createDocumentName;

--****************************************************************************************************************************
FUNCTION ckeckRemoveEmptyBookmarks(cnJobId IN NUMBER, sRemoveFlag OUT VARCHAR2) RETURN STB$OERROR$RECORD
IS
  oErrorObj      STB$OERROR$RECORD := STB$OERROR$RECORD();
  sCurrentName   constant varchar2(4000) := $$PLSQL_UNIT||'.ckeckRemoveEmptyBookmarks('||cnJobId||')';

  cursor cGetValueRemove is
    select parametervalue
    from stb$reportparameter
    where repid = STB$JOB.getJobParameter('REPID', cnJobId)
    and parametername = 'REMOVE_EMPTY_BOOKMARKS';

  cursor cCheckStatusReport is
    select status
    from stb$report
    where repid = STB$JOB.getJobParameter('REPID', cnJobId);

  nStatusReport STB$REPORT.STATUS%TYPE;
BEGIN
  isr$trace.stat('begin', 'begin', sCurrentName);

  sRemoveFlag := 'F';

  OPEN cGetValueRemove;
  FETCH cGetValueRemove INTO sRemoveFlag;
  IF cGetValueRemove%NOTFOUND THEN
    IF STB$UTIL.getRepTypeParameter(STB$JOB.getJobParameter('REPORTTYPEID', cnJobId), 'REMOVE_EMPTY_BOOKMARKS') = '0' THEN -- ALWAYS
      sRemoveFlag := 'T';
      isr$trace.debug('sRemoveFlag from stb$reptypeparameter - always',sRemoveFlag,sCurrentName);
    ELSE
      OPEN cCheckStatusReport;
      FETCH cCheckStatusReport INTO nStatusReport;
      isr$trace.debug('nStatusReport',nStatusReport,sCurrentName);
      IF STB$UTIL.getRepTypeParameter(STB$JOB.getJobParameter('REPORTTYPEID', cnJobId), 'REMOVE_EMPTY_BOOKMARKS') = TO_CHAR(nStatusReport) THEN
        sRemoveFlag := 'T';
        isr$trace.debug('sRemoveFlag from stb$reptypeparameter - status '||TO_CHAR(nStatusReport),sRemoveFlag,sCurrentName);
      END IF;
      CLOSE cCheckStatusReport;
    END IF;
  ELSE
    isr$trace.debug('sRemoveFlag from stb$reportparameter',sRemoveFlag,sCurrentName);
  END IF; 
  CLOSE cGetValueRemove;
  isr$trace.debug('sRemoveFlag',sRemoveFlag,sCurrentName);

  isr$trace.stat('end', 'end', sCurrentName);
  RETURN oErrorObj;

EXCEPTION
  WHEN OTHERS THEN
      oErrorObj.handleError ( Stb$typedef.cnSeverityCritical, utd$msglib.getmsg ('exUnexpectedError', stb$security.getCurrentlanguage,  csP1 => sqlerrm(sqlcode)), 
        sCurrentName, dbms_utility.format_error_stack ||chr(10)|| dbms_utility.format_error_backtrace );
    RETURN oErrorObj;
END ckeckRemoveEmptyBookmarks;

--********************************************************************************************************************************
FUNCTION writeAdditionalFinalDocProps(cxXml IN OUT XMLTYPE, cnRepid IN NUMBER) RETURN STB$OERROR$RECORD
IS
  sCurrentName      constant varchar2(4000) := $$PLSQL_UNIT||'.writeAdditionalFinalDocProps('||cnRepid||')';
  oErrorObj         STB$OERROR$RECORD := STB$OERROR$RECORD();

  sSRNumber         STB$REPORT.SRNUMBER%TYPE;
  sTitle            STB$REPORT.TITLE%TYPE;
  sCreatedBy        STB$USER.FULLNAME%TYPE;
  sCreatedOn        VARCHAR2(256);
  sCreatedVersion   STB$REPORT.VERSION%TYPE;
  sCreatedText      VARCHAR2(1000);

  sApprovedBy       STB$ESIG.FULLNAME%TYPE;
  sApprovedOn       VARCHAR2(256);
  sApprovedVersion  STB$REPORT.VERSION%TYPE;
  sApprovedText     VARCHAR2(1000);

  sLockedBy         STB$ESIG.FULLNAME%TYPE;
  sLockedOn         VARCHAR2(256);
  sLockedVersion    STB$REPORT.VERSION%TYPE;
  sLockedText       VARCHAR2(1000);
  sLastAction       STB$ESIG.ACTION%TYPE;

  sFinalBy          STB$ESIG.FULLNAME%TYPE;
  sFinalOn          VARCHAR2(256);
  sFinalVersion     STB$REPORT.VERSION%TYPE;
  sFinalText        VARCHAR2(1000);

  -- draft version
  CURSOR cGetEsigFullname_draft IS
    SELECT u.fullname
         , TO_CHAR (r.createdon, STB$UTIL.getSystemParameter ('DATEFORMAT') || ' HH24:MI:SS') createdon
         , r.srnumber
         , r.title
         , r.version
      FROM STB$REPORT r, STB$USER u, (SELECT MAX (repid) repid, status
                                        FROM stb$report r
                                      START WITH repid = cnRepid
                                      CONNECT BY PRIOR parentrepid = repid
                                      GROUP BY status) last_draft_version
     WHERE r.createdby IN (u.oracleusername, u.ldapusername)
       AND last_draft_version.status = STB$TYPEDEF.cnStatusDraft
       AND last_draft_version.repid = r.repid;

  -- inspection version
  CURSOR cGetEsigFullname_inspection IS
     SELECT e.fullname
         , TO_CHAR (e.timestamp, STB$UTIL.getSystemParameter ('DATEFORMAT') || ' HH24:MI:SS') inspected_on
         , r.version
      FROM STB$REPORT r, STB$ESIG e, (SELECT repid, status
                                        FROM stb$report r
                                      START WITH repid = cnRepid
                                      CONNECT BY PRIOR parentrepid = repid) inspection_version
     WHERE inspection_version.status = STB$TYPEDEF.cnStatusInspection
       AND inspection_version.repid = r.repid
       AND r.srnumber = e.srnumber
       AND r.title = e.title
       AND (to_char (r.parentrepid) = e.reference
         OR (e.nodetype, e.reference) IN (select 'DOCUMENT', to_char(d.docid) from stb$doctrail d where d.nodeid = to_char (r.parentrepid)))
       AND action IN ('CAN_RELEASE_FOR_INSPECTION', 'CAN_CREATE_REPORT_INSPECTION_VERSION')
     ORDER BY entryid DESC;

  -- locked version
  CURSOR cGetEsigFullname_lock IS
    SELECT e.fullname
         , TO_CHAR (e.timestamp, STB$UTIL.getSystemParameter ('DATEFORMAT') || ' HH24:MI:SS') locked_on
         , r.version
         , e.action
      FROM STB$REPORT r, STB$ESIG e, (SELECT repid, status
                                        FROM stb$report r
                                      START WITH repid = cnRepid
                                      CONNECT BY PRIOR parentrepid = repid) inspection_version
     WHERE inspection_version.status = STB$TYPEDEF.cnStatusInspection
       AND inspection_version.repid = r.repid
       AND r.srnumber = e.srnumber
       AND r.title = e.title
       AND (to_char (r.repid) = e.reference
         OR (e.nodetype, e.reference) IN (select 'DOCUMENT', to_char(d.docid) from stb$doctrail d where d.nodeid = to_char (r.repid)))
       AND (e.action LIKE 'CAN_%LOCK_REPORT' or action IN ('CAN_LOCK_REPORT', 'CAN_CREATE_REPORT_LOCK_VERSION'))
       AND e.success = 'T'
     ORDER BY entryid DESC;

  -- final version
  CURSOR cGetEsigFullname_final IS
    SELECT e.fullname
         , TO_CHAR (e.timestamp, STB$UTIL.getSystemParameter ('DATEFORMAT') || ' HH24:MI:SS') finalized_on
         , r.version
      FROM STB$REPORT r, STB$ESIG e, (SELECT repid, status
                                        FROM stb$report r
                                      START WITH repid = cnRepid
                                      CONNECT BY PRIOR parentrepid = repid) final_version
     WHERE final_version.status = STB$TYPEDEF.cnStatusFinal
       AND final_version.repid = r.repid
       AND r.srnumber = e.srnumber
       AND r.title = e.title
       AND (to_char (r.parentrepid) = e.reference
         OR (e.nodetype, e.reference) IN (select 'DOCUMENT', to_char(d.docid) from stb$doctrail d where d.nodeid = to_char (r.parentrepid)))
       AND action IN ('CAN_FINALIZE_REPORT', 'CAN_CREATE_REPORT_FINAL_VERSION')
     ORDER BY entryid DESC;

BEGIN
  isr$trace.stat('begin', 'begin', sCurrentName);

  --write the document properties manually

  sCreatedText := Utd$msglib.GetMsg('STATUS_1',Stb$security.GetCurrentLanguage);
  OPEN cGetEsigFullname_draft;
  FETCH cGetEsigFullname_draft INTO sCreatedBy, sCreatedOn, sSRNumber, sTitle, sCreatedVersion;
  CLOSE cGetEsigFullname_draft;
  IF sCreatedBy IS NULL THEN
    sCreatedBy := '-';
    sCreatedOn := '-';
  END IF;
  isr$trace.debug('sCreatedBy',sCreatedBy  || '  sCreatedOn ' || sCreatedOn , sCurrentName);

  sApprovedText := Utd$msglib.GetMsg('STATUS_2',Stb$security.GetCurrentLanguage);
  OPEN cGetEsigFullname_inspection;
  FETCH cGetEsigFullname_inspection INTO sApprovedBy, sApprovedOn, sApprovedVersion;
  CLOSE cGetEsigFullname_inspection;
  IF sApprovedBy IS NULL THEN
    sApprovedBy := '-';
    sApprovedOn := '-';
  END IF;
  isr$trace.debug('sApprovedBy', sApprovedBy, sCurrentName);
  isr$trace.debug('sApprovedOn', sApprovedOn, sCurrentName);

  sLockedText := Utd$msglib.GetMsg('STATUS_LOCK',Stb$security.GetCurrentLanguage);
  OPEN cGetEsigFullname_lock;
  FETCH cGetEsigFullname_lock INTO sLockedBy, sLockedOn, sLockedVersion, sLastAction;
  CLOSE cGetEsigFullname_lock;
  IF sLockedBy IS NULL OR sLastAction = 'CAN_UNLOCK_REPORT' THEN
    sLockedBy := '-';
    sLockedOn := '-';
  END IF;
  isr$trace.debug('sLockedBy', sLockedBy, sCurrentName);
  isr$trace.debug('sLockedOn', sLockedOn, sCurrentName);

  sFinalText := Utd$msglib.GetMsg('STATUS_3',Stb$security.GetCurrentLanguage);
  OPEN cGetEsigFullname_final;
  FETCH cGetEsigFullname_final INTO sFinalBy, sFinalOn, sFinalVersion;
  CLOSE cGetEsigFullname_final;
  IF sFinalBy IS NULL THEN
    sFinalBy := '-';
    sFinalOn := '-';
  END IF;
  isr$trace.debug('sFinalBy', sFinalBy, sCurrentName);
  isr$trace.debug('sFinalOn', sFinalOn, sCurrentName);

  -- special cases for the esig document
  FOR i IN 1..ISR$XML.valueOf(cxXML, 'count(/config/apps/app[@name="wordmodul"])') LOOP
    isr$trace.debug('i','ISR$UTD$PACKAGE.writeAdditionalDocProps',i,sCurrentName);

    -- writing the doc properties into the xml-dom
    IF ISR$XML.valueOf(cxXML, 'count(/config/apps/app[@name="wordmodul"]['||i ||']/properties)') = 0 THEN
      ISR$XML.CreateNode(cxXML, '/config/apps/app[@name="wordmodul"]['||i ||']','properties', '');
    END IF;

    -- for all files where the word module is set
    isr$trace.debug('wordmodule','ISR$UTD$PACKAGE.writeAdditionalDocProps','wordmodule' , sCurrentName);

    ISR$XML.CreateNode(cxXML, '/config/apps/app[@order="' || i ||'" and @name="wordmodul"]/properties', 'custom-property', sCreatedBy);
    ISR$XML.SetAttribute(cxXML, '/config/apps/app[@order="' || i ||'" and @name="wordmodul"]/properties/custom-property[position()=last()]', 'name', 'COMPILBY');
    ISR$XML.CreateNode(cxXML, '/config/apps/app[@order="' || i ||'" and @name="wordmodul"]/properties', 'custom-property', sCreatedOn);
    ISR$XML.SetAttribute(cxXML, '/config/apps/app[@order="' || i ||'" and @name="wordmodul"]/properties/custom-property[position()=last()]', 'name', 'COMPILEDON');
    ISR$XML.CreateNode(cxXML, '/config/apps/app[@order="' || i ||'" and @name="wordmodul"]/properties', 'custom-property', sCreatedVersion);
    ISR$XML.SetAttribute(cxXML, '/config/apps/app[@order="' || i ||'" and @name="wordmodul"]/properties/custom-property[position()=last()]', 'name', 'COMPILEDVERSION');
    ISR$XML.CreateNode(cxXML, '/config/apps/app[@order="' || i ||'" and @name="wordmodul"]/properties', 'custom-property', sCreatedText);
    ISR$XML.SetAttribute(cxXML, '/config/apps/app[@order="' || i ||'" and @name="wordmodul"]/properties/custom-property[position()=last()]', 'name', 'COMPILEDTEXT');

    ISR$XML.CreateNode(cxXML, '/config/apps/app[@order="' || i ||'" and @name="wordmodul"]/properties', 'custom-property', sSRNumber);
    ISR$XML.SetAttribute(cxXML, '/config/apps/app[@order="' || i ||'" and @name="wordmodul"]/properties/custom-property[position()=last()]', 'name', 'SRNUMBER');

    ISR$XML.CreateNode(cxXML, '/config/apps/app[@order="' || i ||'" and @name="wordmodul"]/properties', 'custom-property', sTitle);
    ISR$XML.SetAttribute(cxXML, '/config/apps/app[@order="' || i ||'" and @name="wordmodul"]/properties/custom-property[position()=last()]', 'name', 'TITLE');

    ISR$XML.CreateNode(cxXML, '/config/apps/app[@order="' || i ||'" and @name="wordmodul"]/properties', 'custom-property', sApprovedBy);
    ISR$XML.SetAttribute(cxXML, '/config/apps/app[@order="' || i ||'" and @name="wordmodul"]/properties/custom-property[position()=last()]', 'name', 'APPROVED1BY');
    ISR$XML.CreateNode(cxXML, '/config/apps/app[@order="' || i ||'" and @name="wordmodul"]/properties', 'custom-property', sApprovedOn);
    ISR$XML.SetAttribute(cxXML, '/config/apps/app[@order="' || i ||'" and @name="wordmodul"]/properties/custom-property[position()=last()]', 'name', 'APPROVED1ON');
    ISR$XML.CreateNode(cxXML, '/config/apps/app[@order="' || i ||'" and @name="wordmodul"]/properties', 'custom-property', sApprovedVersion);
    ISR$XML.SetAttribute(cxXML, '/config/apps/app[@order="' || i ||'" and @name="wordmodul"]/properties/custom-property[position()=last()]', 'name', 'APPROVED1VERSION');
    ISR$XML.CreateNode(cxXML, '/config/apps/app[@order="' || i ||'" and @name="wordmodul"]/properties', 'custom-property', sApprovedText);
    ISR$XML.SetAttribute(cxXML, '/config/apps/app[@order="' || i ||'" and @name="wordmodul"]/properties/custom-property[position()=last()]', 'name', 'APPROVED1TEXT');

    ISR$XML.CreateNode(cxXML, '/config/apps/app[@order="' || i ||'" and @name="wordmodul"]/properties', 'custom-property', sLockedBy);
    ISR$XML.SetAttribute(cxXML, '/config/apps/app[@order="' || i ||'" and @name="wordmodul"]/properties/custom-property[position()=last()]', 'name', 'LOCKEDBY');
    ISR$XML.CreateNode(cxXML, '/config/apps/app[@order="' || i ||'" and @name="wordmodul"]/properties', 'custom-property', sLockedOn);
    ISR$XML.SetAttribute(cxXML, '/config/apps/app[@order="' || i ||'" and @name="wordmodul"]/properties/custom-property[position()=last()]', 'name', 'LOCKEDON');
    ISR$XML.CreateNode(cxXML, '/config/apps/app[@order="' || i ||'" and @name="wordmodul"]/properties', 'custom-property', sLockedVersion);
    ISR$XML.SetAttribute(cxXML, '/config/apps/app[@order="' || i ||'" and @name="wordmodul"]/properties/custom-property[position()=last()]', 'name', 'LOCKEDVERSION');
    ISR$XML.CreateNode(cxXML, '/config/apps/app[@order="' || i ||'" and @name="wordmodul"]/properties', 'custom-property', sLockedText);
    ISR$XML.SetAttribute(cxXML, '/config/apps/app[@order="' || i ||'" and @name="wordmodul"]/properties/custom-property[position()=last()]', 'name', 'LOCKEDTEXT');

    ISR$XML.CreateNode(cxXML, '/config/apps/app[@order="' || i ||'" and @name="wordmodul"]/properties', 'custom-property', sFinalBy);
    ISR$XML.SetAttribute(cxXML, '/config/apps/app[@order="' || i ||'" and @name="wordmodul"]/properties/custom-property[position()=last()]', 'name', 'APPROVED2BY');
    ISR$XML.CreateNode(cxXML, '/config/apps/app[@order="' || i ||'" and @name="wordmodul"]/properties', 'custom-property', sFinalOn);
    ISR$XML.SetAttribute(cxXML, '/config/apps/app[@order="' || i ||'" and @name="wordmodul"]/properties/custom-property[position()=last()]', 'name', 'APPROVED2ON');
    ISR$XML.CreateNode(cxXML, '/config/apps/app[@order="' || i ||'" and @name="wordmodul"]/properties', 'custom-property', sFinalVersion);
    ISR$XML.SetAttribute(cxXML, '/config/apps/app[@order="' || i ||'" and @name="wordmodul"]/properties/custom-property[position()=last()]', 'name', 'APPROVED2VERSION');
    ISR$XML.CreateNode(cxXML, '/config/apps/app[@order="' || i ||'" and @name="wordmodul"]/properties', 'custom-property', sFinalText);
    ISR$XML.SetAttribute(cxXML, '/config/apps/app[@order="' || i ||'" and @name="wordmodul"]/properties/custom-property[position()=last()]', 'name', 'APPROVED2TEXT');

  END LOOP;

  isr$trace.stat('end', 'end', sCurrentName);
  RETURN oErrorObj;

EXCEPTION
  WHEN OTHERS THEN
      oErrorObj.handleError ( Stb$typedef.cnSeverityCritical, utd$msglib.getmsg ('exUnexpectedError', stb$security.getCurrentlanguage,  csP1 => sqlerrm(sqlcode)), 
        sCurrentName, dbms_utility.format_error_stack ||chr(10)|| dbms_utility.format_error_backtrace );
    RETURN oErrorObj;
END writeAdditionalFinalDocProps;

--********************************************************************************************************************************
FUNCTION writeCustomDocProperties(cxXml IN OUT XMLTYPE, cnJobId IN NUMBER) RETURN STB$OERROR$RECORD
IS
  sCurrentName      constant varchar2(4000) := $$PLSQL_UNIT||'.writeCustomDocProperties('||cnJobid||')';
  oErrorObj         STB$OERROR$RECORD := STB$OERROR$RECORD();
  cnRepid           NUMBER;

  sSRNumber         STB$REPORT.SRNUMBER%TYPE;
  sTitle            STB$REPORT.TITLE%TYPE;
  sCreatedBy        STB$USER.FULLNAME%TYPE;
  sCreatedOn        VARCHAR2(256);
  sCreatedVersion   STB$REPORT.VERSION%TYPE;
  sCreatedText      VARCHAR2(1000);

  sApprovedBy       STB$ESIG.FULLNAME%TYPE;
  sApprovedOn       VARCHAR2(256);
  sApprovedVersion  STB$REPORT.VERSION%TYPE;
  sApprovedText     VARCHAR2(1000);

  sLockedBy         STB$ESIG.FULLNAME%TYPE;
  sLockedOn         VARCHAR2(256);
  sLockedVersion    STB$REPORT.VERSION%TYPE;
  sLockedText       VARCHAR2(1000);
  sLastAction       STB$ESIG.ACTION%TYPE;

  sFinalBy          STB$ESIG.FULLNAME%TYPE;
  sFinalOn          VARCHAR2(256);
  sFinalVersion     STB$REPORT.VERSION%TYPE;
  sFinalText        VARCHAR2(1000);
  sCurrentLal       constant VARCHAR2(100) := STB$UTIL.getCurrentLal;


  -- draft version
  CURSOR cGetEsigFullname_draft IS
    SELECT u.fullname
         , TO_CHAR (r.createdon, STB$UTIL.getSystemParameter ('DATEFORMAT') || ' HH24:MI:SS') createdon
         , r.srnumber
         , r.title
         , r.version
      FROM STB$REPORT r, STB$USER u, (SELECT MAX (repid) repid, status
                                        FROM stb$report r
                                      START WITH repid = cnRepid
                                      CONNECT BY PRIOR parentrepid = repid
                                      GROUP BY status) last_draft_version
     WHERE r.createdby IN (u.oracleusername, u.ldapusername)
       AND last_draft_version.status = STB$TYPEDEF.cnStatusDraft
       AND last_draft_version.repid = r.repid;

  -- inspection version
  CURSOR cGetEsigFullname_inspection IS
    SELECT e.fullname
         , TO_CHAR (e.timestamp, STB$UTIL.getSystemParameter ('DATEFORMAT') || ' HH24:MI:SS') inspected_on
         , r.version
      FROM STB$REPORT r, STB$ESIG e, (SELECT repid, status
                                        FROM stb$report r
                                      START WITH repid = cnRepid
                                      CONNECT BY PRIOR parentrepid = repid) inspection_version
     WHERE inspection_version.status = STB$TYPEDEF.cnStatusInspection
       AND inspection_version.repid = r.repid
       AND r.srnumber = e.srnumber
       AND r.title = e.title
       AND (to_char (r.parentrepid) = e.reference
         OR (e.nodetype, e.reference) IN (select 'DOCUMENT', to_char(d.docid) from stb$doctrail d where d.nodeid = to_char (r.parentrepid)))
       AND action IN ('CAN_RELEASE_FOR_INSPECTION', 'CAN_CREATE_REPORT_INSPECTION_VERSION')
     ORDER BY entryid DESC;

  -- locked version
  CURSOR cGetEsigFullname_lock IS
    SELECT e.fullname
         , TO_CHAR (e.timestamp, STB$UTIL.getSystemParameter ('DATEFORMAT') || ' HH24:MI:SS') locked_on
         , r.version
         , e.action
      FROM STB$REPORT r, STB$ESIG e, (SELECT repid, status
                                        FROM stb$report r
                                      START WITH repid = cnRepid
                                      CONNECT BY PRIOR parentrepid = repid) inspection_version
     WHERE inspection_version.status = STB$TYPEDEF.cnStatusInspection
       AND inspection_version.repid = r.repid
       AND r.srnumber = e.srnumber
       AND r.title = e.title
       AND (to_char (r.repid) = e.reference
         OR (e.nodetype, e.reference) IN (select 'DOCUMENT', to_char(d.docid) from stb$doctrail d where d.nodeid = to_char (r.repid)))
       AND (e.action LIKE 'CAN_%LOCK_REPORT' or action IN ('CAN_LOCK_REPORT', 'CAN_CREATE_REPORT_LOCK_VERSION'))
       AND e.success = 'T'
     ORDER BY entryid DESC;

  -- final version
  CURSOR cGetEsigFullname_final IS
    SELECT e.fullname
         , TO_CHAR (e.timestamp, STB$UTIL.getSystemParameter ('DATEFORMAT') || ' HH24:MI:SS') finalized_on
         , r.version
      FROM STB$REPORT r, STB$ESIG e, (SELECT repid, status
                                        FROM stb$report r
                                      START WITH repid = cnRepid
                                      CONNECT BY PRIOR parentrepid = repid) final_version
     WHERE final_version.status = STB$TYPEDEF.cnStatusFinal
       AND final_version.repid = r.repid
       AND r.srnumber = e.srnumber
       AND r.title = e.title
       AND (to_char (r.parentrepid) = e.reference
         OR (e.nodetype, e.reference) IN (select 'DOCUMENT', to_char(d.docid) from stb$doctrail d where d.nodeid = to_char (r.parentrepid)))
       AND action IN ('CAN_FINALIZE_REPORT', 'CAN_CREATE_REPORT_FINAL_VERSION')
     ORDER BY entryid DESC;
BEGIN
  isr$trace.stat('begin', 'begin', sCurrentName);

  cnRepid := TO_NUMBER(STB$JOB.getJobParameter('REPID', cnJobId));

  --write the document properties manually

  sCreatedText := Utd$msglib.GetMsg('STATUS_1',Stb$security.GetCurrentLanguage);
  OPEN cGetEsigFullname_draft;
  FETCH cGetEsigFullname_draft INTO sCreatedBy, sCreatedOn, sSRNumber, sTitle, sCreatedVersion;
  CLOSE cGetEsigFullname_draft;
  IF sCreatedBy IS NULL THEN
    sCreatedBy := '-';
    sCreatedOn := '-';
  END IF;
  isr$trace.debug('sCreatedBy',sCreatedBy  || '  sCreatedOn ' || sCreatedOn , sCurrentName);

  sApprovedText := Utd$msglib.GetMsg('STATUS_2',Stb$security.GetCurrentLanguage);
  OPEN cGetEsigFullname_inspection;
  FETCH cGetEsigFullname_inspection INTO sApprovedBy, sApprovedOn, sApprovedVersion;
  CLOSE cGetEsigFullname_inspection;
  IF sApprovedBy IS NULL THEN
    sApprovedBy := '-';
    sApprovedOn := '-';
  END IF;
  isr$trace.debug('sApprovedBy', sApprovedBy, sCurrentName);
  isr$trace.debug('sApprovedOn', sApprovedOn, sCurrentName);

  sLockedText := Utd$msglib.GetMsg('STATUS_LOCK',Stb$security.GetCurrentLanguage);
  OPEN cGetEsigFullname_lock;
  FETCH cGetEsigFullname_lock INTO sLockedBy, sLockedOn, sLockedVersion, sLastAction;
  CLOSE cGetEsigFullname_lock;
  IF sLockedBy IS NULL OR sLastAction = 'CAN_UNLOCK_REPORT' THEN
    sLockedBy := '-';
    sLockedOn := '-';
  END IF;
  isr$trace.debug('sLockedBy', sLockedBy, sCurrentName);
  isr$trace.debug('sLockedOn', sLockedOn, sCurrentName);

  sFinalText := Utd$msglib.GetMsg('STATUS_3',Stb$security.GetCurrentLanguage);
  OPEN cGetEsigFullname_final;
  FETCH cGetEsigFullname_final INTO sFinalBy, sFinalOn, sFinalVersion;
  CLOSE cGetEsigFullname_final;
  IF sFinalBy IS NULL THEN
    sFinalBy := '-';
    sFinalOn := '-';
  END IF;
  isr$trace.debug('sFinalBy', sFinalBy, sCurrentName);
  isr$trace.debug('sFinalOn', sFinalOn, sCurrentName);

  -- special cases for the esig document
  FOR i IN 1..ISR$XML.valueOf(cxXML, 'count(/config/apps/app[@name="wordmodul"])') LOOP
    isr$trace.debug('i',i,150);

    -- writing the doc properties into the xml-dom
    IF ISR$XML.valueOf(cxXML, 'count(/config/apps/app[@name="wordmodul"]['||i ||']/properties)') = 0 THEN
      ISR$XML.CreateNode(cxXML, '/config/apps/app[@name="wordmodul"]['||i ||']','properties', '');
    END IF;

    -- for all files where the word module is set
    isr$trace.debug('wordmodule','wordmodule' , sCurrentName);

    ISR$XML.CreateNode(cxXML, '/config/apps/app[@order="' || i ||'" and @name="wordmodul"]/properties', 'custom-property', sCreatedBy);
    ISR$XML.SetAttribute(cxXML, '/config/apps/app[@order="' || i ||'" and @name="wordmodul"]/properties/custom-property[position()=last()]', 'name', 'COMPILEDBY');
    ISR$XML.CreateNode(cxXML, '/config/apps/app[@order="' || i ||'" and @name="wordmodul"]/properties', 'custom-property', sCreatedOn);
    ISR$XML.SetAttribute(cxXML, '/config/apps/app[@order="' || i ||'" and @name="wordmodul"]/properties/custom-property[position()=last()]', 'name', 'COMPILEDON');
    ISR$XML.CreateNode(cxXML, '/config/apps/app[@order="' || i ||'" and @name="wordmodul"]/properties', 'custom-property', sCreatedVersion);
    ISR$XML.SetAttribute(cxXML, '/config/apps/app[@order="' || i ||'" and @name="wordmodul"]/properties/custom-property[position()=last()]', 'name', 'COMPILEDVERSION');
    ISR$XML.CreateNode(cxXML, '/config/apps/app[@order="' || i ||'" and @name="wordmodul"]/properties', 'custom-property', sCreatedText);
    ISR$XML.SetAttribute(cxXML, '/config/apps/app[@order="' || i ||'" and @name="wordmodul"]/properties/custom-property[position()=last()]', 'name', 'COMPILEDTEXT');
    ISR$XML.CreateNode(cxXML, '/config/apps/app[@order="' || i ||'" and @name="wordmodul"]/properties', 'custom-property', '');
    ISR$XML.SetAttribute(cxXML, '/config/apps/app[@order="' || i ||'" and @name="wordmodul"]/properties/custom-property[position()=last()]', 'name', 'COMPILEDROLE');


    ISR$XML.CreateNode(cxXML, '/config/apps/app[@order="' || i ||'" and @name="wordmodul"]/properties', 'custom-property', sApprovedBy);
    ISR$XML.SetAttribute(cxXML, '/config/apps/app[@order="' || i ||'" and @name="wordmodul"]/properties/custom-property[position()=last()]', 'name', 'APPROVED1BY');
    ISR$XML.CreateNode(cxXML, '/config/apps/app[@order="' || i ||'" and @name="wordmodul"]/properties', 'custom-property', sApprovedOn);
    ISR$XML.SetAttribute(cxXML, '/config/apps/app[@order="' || i ||'" and @name="wordmodul"]/properties/custom-property[position()=last()]', 'name', 'APPROVED1ON');
    ISR$XML.CreateNode(cxXML, '/config/apps/app[@order="' || i ||'" and @name="wordmodul"]/properties', 'custom-property', sApprovedVersion);
    ISR$XML.SetAttribute(cxXML, '/config/apps/app[@order="' || i ||'" and @name="wordmodul"]/properties/custom-property[position()=last()]', 'name', 'APPROVED1VERSION');
    ISR$XML.CreateNode(cxXML, '/config/apps/app[@order="' || i ||'" and @name="wordmodul"]/properties', 'custom-property', sApprovedText);
    ISR$XML.SetAttribute(cxXML, '/config/apps/app[@order="' || i ||'" and @name="wordmodul"]/properties/custom-property[position()=last()]', 'name', 'APPROVED1TEXT');
    ISR$XML.CreateNode(cxXML, '/config/apps/app[@order="' || i ||'" and @name="wordmodul"]/properties', 'custom-property', '');
    ISR$XML.SetAttribute(cxXML, '/config/apps/app[@order="' || i ||'" and @name="wordmodul"]/properties/custom-property[position()=last()]', 'name', 'APPROVED1ROLE');

    ISR$XML.CreateNode(cxXML, '/config/apps/app[@order="' || i ||'" and @name="wordmodul"]/properties', 'custom-property', sLockedBy);
    ISR$XML.SetAttribute(cxXML, '/config/apps/app[@order="' || i ||'" and @name="wordmodul"]/properties/custom-property[position()=last()]', 'name', 'LOCKEDBY');
    ISR$XML.CreateNode(cxXML, '/config/apps/app[@order="' || i ||'" and @name="wordmodul"]/properties', 'custom-property', sLockedOn);
    ISR$XML.SetAttribute(cxXML, '/config/apps/app[@order="' || i ||'" and @name="wordmodul"]/properties/custom-property[position()=last()]', 'name', 'LOCKEDON');
    ISR$XML.CreateNode(cxXML, '/config/apps/app[@order="' || i ||'" and @name="wordmodul"]/properties', 'custom-property', sLockedVersion);
    ISR$XML.SetAttribute(cxXML, '/config/apps/app[@order="' || i ||'" and @name="wordmodul"]/properties/custom-property[position()=last()]', 'name', 'LOCKEDVERSION');
    ISR$XML.CreateNode(cxXML, '/config/apps/app[@order="' || i ||'" and @name="wordmodul"]/properties', 'custom-property', sLockedText);
    ISR$XML.SetAttribute(cxXML, '/config/apps/app[@order="' || i ||'" and @name="wordmodul"]/properties/custom-property[position()=last()]', 'name', 'LOCKEDTEXT');
    ISR$XML.CreateNode(cxXML, '/config/apps/app[@order="' || i ||'" and @name="wordmodul"]/properties', 'custom-property', '');
    ISR$XML.SetAttribute(cxXML, '/config/apps/app[@order="' || i ||'" and @name="wordmodul"]/properties/custom-property[position()=last()]', 'name', 'LOCKEDROLE');

    ISR$XML.CreateNode(cxXML, '/config/apps/app[@order="' || i ||'" and @name="wordmodul"]/properties', 'custom-property', sFinalBy);
    ISR$XML.SetAttribute(cxXML, '/config/apps/app[@order="' || i ||'" and @name="wordmodul"]/properties/custom-property[position()=last()]', 'name', 'APPROVED2BY');
    ISR$XML.CreateNode(cxXML, '/config/apps/app[@order="' || i ||'" and @name="wordmodul"]/properties', 'custom-property', sFinalOn);
    ISR$XML.SetAttribute(cxXML, '/config/apps/app[@order="' || i ||'" and @name="wordmodul"]/properties/custom-property[position()=last()]', 'name', 'APPROVED2ON');
    ISR$XML.CreateNode(cxXML, '/config/apps/app[@order="' || i ||'" and @name="wordmodul"]/properties', 'custom-property', sFinalVersion);
    ISR$XML.SetAttribute(cxXML, '/config/apps/app[@order="' || i ||'" and @name="wordmodul"]/properties/custom-property[position()=last()]', 'name', 'APPROVED2VERSION');
    ISR$XML.CreateNode(cxXML, '/config/apps/app[@order="' || i ||'" and @name="wordmodul"]/properties', 'custom-property', sFinalText);
    ISR$XML.SetAttribute(cxXML, '/config/apps/app[@order="' || i ||'" and @name="wordmodul"]/properties/custom-property[position()=last()]', 'name', 'APPROVED2TEXT');
    ISR$XML.CreateNode(cxXML, '/config/apps/app[@order="' || i ||'" and @name="wordmodul"]/properties', 'custom-property', '');
    ISR$XML.SetAttribute(cxXML, '/config/apps/app[@order="' || i ||'" and @name="wordmodul"]/properties/custom-property[position()=last()]', 'name', 'APPROVED2ROLE');

  END LOOP;

  -- go to lal
  IF STB$UTIL.checkMethodExists('writeCustomDocProperties') = 'T' THEN
    EXECUTE immediate
      'BEGIN
         :1 := '|| sCurrentLal ||'.writeCustomDocProperties(:2, :3);
       END;'
    Using OUT oErrorObj, IN OUT cxXML, IN cnJobId;
  END IF;

  isr$trace.stat('end', 'end', sCurrentName);
  RETURN oErrorObj;

EXCEPTION
  WHEN OTHERS THEN
      oErrorObj.handleError ( Stb$typedef.cnSeverityCritical, utd$msglib.getmsg ('exUnexpectedError', stb$security.getCurrentlanguage,  csP1 => sqlerrm(sqlcode)), 
        sCurrentName, dbms_utility.format_error_stack ||chr(10)|| dbms_utility.format_error_backtrace );
    RETURN oErrorObj;
END writeCustomDocProperties;

--********************************************************************************************************************************
FUNCTION validate(nMaskNo in NUMBER) RETURN STB$OERROR$RECORD
IS
  sCurrentName              constant varchar2(4000) := $$PLSQL_UNIT||'.validate('||nMaskNo||')';
  oErrorObj                 STB$OERROR$RECORD := STB$OERROR$RECORD();
  nRepid                    NUMBER;
  nNumberOfSelections       NUMBER;
  nNumberOfMasters          NUMBER;
  nNumberMasterOfSelections NUMBER;

  sEntity                   ISR$REPORT$WIZARD.ENTITY%TYPE;
  sMasterEntity             ISR$REPORT$WIZARD.ENTITY%TYPE;

  nMaskType                 ISR$REPORT$WIZARD.MASKTYPE%TYPE;
  sCurrentLal               constant VARCHAR2(100) := STB$UTIL.getCurrentLal;

  CURSOR cGetNumberOfSelections(cnRepID in NUMBER, csEntity in VARCHAR2) is
      select count(*) AS NumberOfSelections
      from   isr$crit
      where  repid = cnRepID
      and    entity = csEntity;

  CURSOR cGetMasterEntity(csEntity in VARCHAR2) is
    SELECT parententityname
      FROM isr$meta$crit$grouping g, isr$meta$relation r
     WHERE g.reporttypeid = STB$OBJECT.GetCurrentReportTypeId
       AND r.childentityname = csEntity
       AND g.relationname = r.relationname;

  CURSOR cGetMaster(cnRepID in NUMBER, csEntity in VARCHAR2) is
      select count(distinct masterkey) AS NumberOfSelections
      from   isr$crit
      where  repid = cnRepID
      and    entity = csEntity;

  CURSOR cGetFormInfo(cnReportTypeID in NUMBER, cnMaskNo in NUMBER) is
      select entity, maskType
      from   ISR$REPORT$WIZARD
      where  reporttypeid = cnReportTypeID
      and    maskno = cnMaskNo;
BEGIN
  isr$trace.stat('begin', 'begin', sCurrentName);

  -- go to lal
  IF STB$UTIL.checkMethodExists('validate') = 'T' THEN
    EXECUTE immediate
      'BEGIN
         :1 := '|| sCurrentLal ||'.validate(:2);
       END;'
    Using OUT oErrorObj, IN nMaskNo;
  END IF;

  IF oErrorObj.sSeverityCode = stb$typedef.cnnoerror THEN

     open cGetFormInfo(STB$OBJECT.GetCurrentReportTypeId, nMaskNo);
    fetch cGetFormInfo into sEntity, nMaskType;
    close cGetFormInfo;

    nRepid := STB$OBJECT.getCurrentRepid;

     open cGetNumberOfSelections(nRepid, sEntity);
    fetch cGetNumberOfSelections into nNumberOfSelections;
    close cGetNumberOfSelections;
    isr$trace.debug('sEntity/nMaskType/nNumberOfSelections', sEntity||'/'||nMaskType||'/'||nNumberOfSelections, sCurrentName);

    if ( nNumberOfSelections = 0 ) then

       isr$trace.debug('check', 'No selection', sCurrentName);

       oErrorObj.sErrorCode      := Utd$msglib.GetMsg('exNoSelection', Stb$security.GetCurrentLanguage);
       oErrorObj.sErrorMessage   := Utd$msglib.GetMsg('exNoSelectionText', Stb$security.GetCurrentLanguage);
       oErrorObj.sSeverityCode   := Stb$typedef.cnWizardValidateStop;

    elsif ( nMaskType = STB$TYPEDEF.cnMasterDetail ) then

        open cGetMaster(nRepid, sEntity);
       fetch cGetMaster into nNumberMasterOfSelections;
       close cGetMaster;

        open cGetMasterEntity(sEntity);
       fetch cGetMasterEntity into sMasterEntity;
       close cGetMasterEntity;

      /*  open cGetNumberOfSelections(nRepid, sMasterEntity);
       fetch cGetNumberOfSelections into nNumberOfMasters;
       close cGetNumberOfSelections;*/
       nNumberOfMasters := stb$Object.GetCurrentNumberOfMasters;

       isr$trace.debug('nNumberOfMasters/nNumberMasterOfSelections',  nNumberOfMasters||'/'||nNumberMasterOfSelections, sCurrentName);
       if ( nNumberOfMasters > nNumberMasterOfSelections ) then
          isr$trace.debug('check', 'Missing values', sCurrentName);
          oErrorObj.sErrorCode      := Utd$msglib.GetMsg('exMissingValues', Stb$security.GetCurrentLanguage);
          oErrorObj.sErrorMessage   := Utd$msglib.GetMsg('exMissingValuesText', Stb$security.GetCurrentLanguage);
          oErrorObj.sSeverityCode   := Stb$typedef.cnWizardValidateWarn;
       end if;

    end if;

  END IF;

  isr$trace.stat('end', 'end', sCurrentName);
  RETURN oErrorObj;

EXCEPTION
  WHEN OTHERS THEN
      oErrorObj.handleError ( Stb$typedef.cnSeverityCritical, utd$msglib.getmsg ('exUnexpectedError', stb$security.getCurrentlanguage,  csP1 => sqlerrm(sqlcode)), 
        sCurrentName, dbms_utility.format_error_stack ||chr(10)|| dbms_utility.format_error_backtrace );
    RETURN oErrorObj;
END validate;

--***********************************************************************************************************************************************
FUNCTION changeDataForInspectionStatus(nNewRepid IN NUMBER) RETURN STB$OERROR$RECORD 
IS
  sCurrentName   constant varchar2(4000) := $$PLSQL_UNIT||'.changeDataForInspectionStatus('||nNewRepid||')';
  oErrorObj      STB$OERROR$RECORD := STB$OERROR$RECORD();
  sCurrentLal    constant VARCHAR2(100) := STB$UTIL.getCurrentLal;

BEGIN
  isr$trace.stat('begin', 'begin', sCurrentName);

  -- go to lal
  IF STB$UTIL.checkMethodExists('changeDataForInspectionStatus') = 'T' THEN
    EXECUTE immediate
      'BEGIN
         :1 := '|| sCurrentLal ||'.changeDataForInspectionStatus(:2);
       END;'
    Using OUT oErrorObj, IN nNewRepid;
  END IF;

  isr$trace.stat('end', 'end', sCurrentName);
  RETURN oErrorObj;

EXCEPTION
  WHEN OTHERS THEN
      oErrorObj.handleError ( Stb$typedef.cnSeverityCritical, utd$msglib.getmsg ('exUnexpectedError', stb$security.getCurrentlanguage,  csP1 => sqlerrm(sqlcode)), 
        sCurrentName, dbms_utility.format_error_stack ||chr(10)|| dbms_utility.format_error_backtrace );
    RETURN oErrorObj;
END changeDataForInspectionStatus;

--*******************************************************************************************************************************
FUNCTION validateInspectionData(sValidateFlag OUT VARCHAR2) RETURN STB$OERROR$RECORD
IS
  oErrorObj      STB$OERROR$RECORD := STB$OERROR$RECORD();
  sCurrentName   constant varchar2(4000) := $$PLSQL_UNIT||'.validateInspectionData('||' '||')';
  sCurrentLal    constant VARCHAR2(100) := STB$UTIL.getCurrentLal;

BEGIN
  isr$trace.stat('begin', 'begin', sCurrentName);

  sValidateFlag := 'T';

  -- go to lal
  IF STB$UTIL.checkMethodExists('validateInspectionData') = 'T' THEN
    EXECUTE immediate
      'BEGIN
         :1 := '|| sCurrentLal ||'.validateInspectionData(:2);
       END;'
    Using OUT oErrorObj, OUT sValidateFlag;
  END IF;

  isr$trace.stat('end', 'end', sCurrentName);
  RETURN oErrorObj;

EXCEPTION
  WHEN OTHERS THEN
      oErrorObj.handleError ( Stb$typedef.cnSeverityCritical, utd$msglib.getmsg ('exUnexpectedError', stb$security.getCurrentlanguage,  csP1 => sqlerrm(sqlcode)), 
        sCurrentName, dbms_utility.format_error_stack ||chr(10)|| dbms_utility.format_error_backtrace );
    RETURN oErrorObj;
END validateInspectionData;

--*******************************************************************************************************************************
function getLov( csParameter    in  varchar2,
                 sFilter        in  varchar2,
                 oSelectionList out ISR$TLRSELECTION$LIST)
  return STB$OERROR$RECORD
is
  oErrorObj         STB$OERROR$RECORD := STB$OERROR$RECORD();
  sCurrentName      constant varchar2(100) := $$PLSQL_UNIT||'.getLov('||csParameter||')';
  exNoLDAPServer    EXCEPTION;
  sCurMdmMethod     stb$systemparameter.parametervalue%type := STB$UTIL.getSystemParameter('MASTER_DATA_MANAGEMENT_METHOD');
  nLanguage         number := Stb$security.GetCurrentLanguage;

  -- oracle users
  CURSOR cGetOracleUser is
    SELECT a.username
      FROM all_users a
    MINUS
    SELECT oracleusername FROM stb$user
    ORDER BY 1;

  -- iSR Users
  CURSOR cGetISRUser is
    SELECT oracleusername username, fullname
      FROM stb$user
     WHERE oracleusername IS NOT NULL
       AND ldapusername IS NULL
    UNION
    SELECT ldapusername username, fullname
      FROM stb$user
     WHERE ldapusername IS NOT NULL
    ORDER BY 1;

  -- languages
  CURSOR cGetLang is
    select langid, description
      from UTD$LANGUAGE;

  -- get the datagroups
  CURSOR cGetDataGroup is
    SELECT groupname, description, usergroupno
      FROM STB$USERGROUP
     WHERE grouptype = 'DATAGROUP';

  -- get only the data groups of the user
  CURSOR cGetDataGroupUser is
    SELECT g.groupname, g.description, g.usergroupno
      FROM STB$USERGROUP g, STB$USERLINK l
     WHERE g.grouptype = 'DATAGROUP'
       AND l.USERNO = STB$SECURITY.GetCurrentUser
       AND l.USERGROUPNO = g.USERGROUPNO;

  CURSOR cGetLovText(sEntity in VARCHAR2, nLangId in NUMBER) is
    SELECT VALUE
         , case when MSGKEY IS NULL then display else utd$msglib.getmsg (MSGKEY, nLangId) end display
         , case when MSGKEY IS NULL then info else utd$msglib.getmsg (MSGKEY, nLangId) end info
         , ordernumber
      FROM isr$parameter$values
     WHERE entity = sEntity
    ORDER BY ordernumber;

  xXml  XMLTYPE;

  CURSOR cGetXmlFilter(xXml IN XMLTYPE, csFilter IN VARCHAR2) is
    SELECT DISTINCT EXTRACTVALUE (
                       COLUMN_VALUE
                     , CASE
                          WHEN csFilter LIKE '@%' THEN 'node()/' || csFilter
                          ELSE csFilter
                       END
                    ) VALUE
      FROM table(XMLSEQUENCE(EXTRACT (
                                (xXml), '//'|| CASE
                                                  WHEN csFilter LIKE '@%' THEN 'node()[' || csFilter || ']'
                                                  ELSE csFilter
                                               END
                             )))
    ORDER BY 1;

  CURSOR cGetLals is
    SELECT DISTINCT parametervalue packagename
      FROM stb$reptypeparameter
     WHERE parametername = 'LALPACKAGE';

  oSelectionList1  ISR$TLRSELECTION$LIST := ISR$TLRSELECTION$LIST();


  CURSOR cGetByClause (sList IN VARCHAR2) IS
    WITH row_count AS
           (SELECT DISTINCT
                   LENGTH (sList) - LENGTH (REPLACE (sList, ';', '')) + 1 rowcnt
                 , sList
              FROM DUAL)
    SELECT display
      FROM (SELECT TRIM(SUBSTR (
                           sList
                         , INSTR (sList, ';', 1, ROWNUM) + 1 
                         , DECODE (INSTR (SUBSTR (sList, INSTR (sList, ';', 1, ROWNUM)+ 1), ';')
                                   - 1,
                                   -1, LENGTH (SUBSTR (sList, INSTR (sList, ';', 1, ROWNUM) + 1)),
                                   INSTR (SUBSTR (sList, INSTR (sList, ';', 1, ROWNUM)+ 1), ';')
                                   - 1)
                        ))
                      display
              FROM row_count, user_tables
             WHERE ROWNUM <= rowcnt
          ORDER BY INSTR (sList, ';', 1, ROWNUM) + 1 )
     WHERE display IS NOT NULL;

    nReportType     NUMBER; 
    CURSOR cGetDocdefinition(nReportType NUMBER) IS
      SELECT VALUE
       , display display
       , info
       , ordernumber
      FROM isr$parameter$values p
     WHERE entity = 'DOCDEFINITION'
       AND NVL(STB$UTIL.getReptypeParameter(stb$object.getCurrentReporttypeId, 'USE_APPENDIX'), 'F') = 'T'
       AND not exists (select 1 from stb$doctrail where nodeid=''||stb$object.GetCurrentRepId||'' and doc_definition=p.value)
  ORDER BY ordernumber;

  sbyhour   VARCHAR2(500) := '0;1;2;3;4;5;6;7;8;9;10;11;12;13;14;15;16;17;18;19;20;21;22;23';
  sbyday    VARCHAR2(500) := 'MON;TUE;WED;THU;FRI;SAT;SUN';
  --sbyweekno VARCHAR2(500) := '1;2;3;4;5;6;7;8;9;10;11;12;13;14;15;16;17;18;19;20;21;22;23;24;25;26;27;28;29;30;31;32;33;34;35;36;37;38;39;40;41;42;43;44;45;46;47;48;49;50;51;52;53';
  sbymonth  VARCHAR2(500) := 'JAN;FEB;MAR;APR;MAY;JUN;JUL;AUG;SEP;OCT;NOV;DEC';
  sTerm     VARCHAR2(500);

  CURSOR cGetIcon IS
    SELECT iconid
      FROM isr$icon
     --WHERE systemicon = 'F'
     WHERE iconsize = 'small'
       AND dbms_lob.getlength(icon) > 0
       AND icon is not null
    ORDER BY systemicon, iconid;

  oAttributes ISR$ATTRIBUTE$LIST := ISR$ATTRIBUTE$LIST ();

  nCount   NUMBER := 0;
  oParamList iSR$ParamList$Rec := iSR$ParamList$Rec();
  sCurrentLal         constant VARCHAR2(100) := STB$UTIL.getCurrentLal;

BEGIN
  isr$trace.stat('begin', 'csParameter: '||csParameter||'  sFilter: '||sFilter, sCurrentName);

  oSelectionList := ISR$TLRSELECTION$LIST();

  CASE

    --***************************************
      -- vs 27.Nov.2017 [ISRC-724]
      when UPPER (csParameter) in ('DEBUG_JOB','LOGLEVELNAME', 'LOGLEVEL') then
        for recLvl in ( select rank() over( order by lvl.loglevel_begin desc ) rn,
                                 lvl.*
                          from   isr$loglevel lvl
                          where  loglevel <> 'FATAL') loop
          oSelectionList.EXTEND;
          oSelectionList (oSelectionList.COUNT) := ISR$OSELECTION$RECORD(recLvl.loglevel, recLvl.loglevel, recLvl.loglevel, null, recLvl.rn );
        end loop;

    --***************************************
      -- vs 06.Jul.2018 [ISRC-1025]
      when csParameter in ('MASTER_DATA_MANAGEMENT_METHOD') then
        oSelectionList.EXTEND;
        oSelectionList(oSelectionList.COUNT) := ISR$OSELECTION$RECORD( sDisplay  => utd$msglib.getmsg ('ON',nLanguage), 
                                                                       sValue    => 'ISR_MDM_DEFAULT', 
                                                                       sInfo     => utd$msglib.getmsg ('ON',nLanguage), 
                                                                       sSelected => case when sCurMdmMethod = 'ISR_MDM_DEFAULT' then 'T' else null end,
                                                                       nOrdernum => oSelectionList.COUNT );
        oSelectionList.EXTEND;
        oSelectionList(oSelectionList.COUNT) := ISR$OSELECTION$RECORD( sDisplay  => utd$msglib.getmsg ('OFF',nLanguage), 
                                                                       sValue    => 'DISABLED', 
                                                                       sInfo     => utd$msglib.getmsg ('OFF',nLanguage), 
                                                                       sSelected => case when sCurMdmMethod = 'DISABLED' then 'T' else null end,
                                                                       nOrdernum => oSelectionList.COUNT );

    --***************************************
    WHEN upper (csParameter) = 'COMPARE_WITH_TEMPLATE' THEN
      oSelectionList := isr$tlrselection$list ();
      FOR rGetMasterTemplates IN (SELECT t.fileid
                                       , t.templatename display
                                       ,    utd$msglib.getmsg ('VERSION', stb$security.getcurrentlanguage)
                                         || t.version
                                         || CASE WHEN t.release = 'T' THEN utd$msglib.getmsg ('RELEASED', stb$security.getcurrentlanguage) END
                                         || utd$msglib.getmsg ('TEMPLATEFILE', stb$security.getcurrentlanguage)
                                         || f.filename
                                            description
                                       , t.version
                                       , t.release
                                       , t.icon
                                    FROM isr$template t, isr$file f
                                   WHERE t.fileid = f.fileid
                                     AND t.fileid != stb$object.getcurrentnodeid
                                     order by 2, 4) LOOP
        oSelectionList.EXTEND;
        oSelectionList (oSelectionList.count()) := ISR$OSELECTION$RECORD(rGetMasterTemplates.display, rGetMasterTemplates.fileid, rGetMasterTemplates.description, null, oSelectionList.COUNT());

        SELECT ISR$ATTRIBUTE$RECORD (sParameter, sValue, sPrepresent)
          BULK COLLECT INTO oSelectionList (oSelectionList.COUNT ()).tloAttributeList
          FROM (SELECT 'ICON' sParameter
                     , rGetMasterTemplates.icon sValue
                     , NULL sPrepresent
                  FROM DUAL);
      END LOOP;

    --***************************************
      WHEN UPPER (csParameter) = 'ICON' THEN
        oAttributes.EXTEND;
        FOR rGetIcon IN cGetIcon LOOP
          oSelectionList.EXTEND;
          oSelectionList (oSelectionList.COUNT) := ISR$OSELECTION$RECORD(rGetIcon.ICONID, rGetIcon.ICONID, rGetIcon.ICONID, null, oSelectionList.COUNT());
          oAttributes (1) := ISR$ATTRIBUTE$RECORD ('ICON', rGetIcon.ICONID);
          oSelectionList (oSelectionList.COUNT).tloAttributeList := oAttributes;
        END LOOP;

    --***************************************
    WHEN Upper (csParameter) like 'JOBBY\_%' escape '\' THEN

        CASE 
          WHEN REPLACE(csParameter, 'JOBBY_') = 'DAILY' THEN sTerm := 'BYHOUR=';
          WHEN REPLACE(csParameter, 'JOBBY_') = 'WEEKLY' THEN sTerm := 'BYDAY=';
       --WHEN REPLACE(csParameter, 'JOBBY_') = 'MONTHLY' THEN sTerm := 'BYWEEKNO=';
          WHEN REPLACE(csParameter, 'JOBBY_') = 'YEARLY' THEN sTerm := 'BYMONTH=';
        END CASE;


        FOR rGetValues IN cGetByClause( CASE WHEN REPLACE(csParameter, 'JOBBY_') = 'DAILY' THEN sbyhour
                                             WHEN REPLACE(csParameter, 'JOBBY_') = 'WEEKLY' THEN sbyday
                                             --WHEN REPLACE(csParameter, 'JOBBY_') = 'MONTHLY' THEN sbyweekno
                                             WHEN REPLACE(csParameter, 'JOBBY_') = 'YEARLY' THEN sbymonth
                                        END) LOOP
          oSelectionList.EXTEND;
          oSelectionList (oSelectionList.COUNT()) := ISR$OSELECTION$RECORD(Utd$msglib.GetMsg (rGetValues.display, Stb$security.GetCurrentLanguage), sTerm||rGetValues.display, Utd$msglib.GetMsg (rGetValues.display, Stb$security.GetCurrentLanguage), null, oSelectionList.COUNT());
        END LOOP;

    --***************************************
    -- Oracle user names (not used by ISR) -------------------------------------
    WHEN UPPER(csParameter) = 'ORACLEUSERNAME' then
      for rGetOracleUser in cGetOracleUser LOOP
        oSelectionList.EXTEND;
        oSelectionList(oSelectionList.COUNT()) :=  ISR$OSELECTION$RECORD(rGetOracleUser.username, rGetOracleUser.username, rGetOracleUser.username, null, oSelectionList.COUNT());
      END LOOP;

    --***************************************
    -- Oracle user names (not used by ISR) -------------------------------------
    WHEN UPPER(csParameter) = 'LDAPUSERNAME' then

      declare

        clReturn CLOB;

        cursor cGetLDAPServer is
          SELECT HOST
               , port
               , isr$server$base.getServerParameter (serverid, 'USERNAME') username
               , isr$server$base.getServerParameter (serverid, 'PASSWORD') password
               , isr$server$base.getServerParameter (serverid, 'LDAPBASE') ldapbase
               , isr$server$base.getServerParameter (serverid, 'LDAPSEARCH') ldapsearch
               , isr$server$base.getServerParameter (serverid, 'SSLWRL') sslWrl
               , isr$server$base.getServerParameter (serverid, 'SSLWALLETPASSWD') sslWalletPasswd
               , isr$server$base.getServerParameter (serverid, 'SSLAUTH') sslAuth
               , isr$server$base.getServerParameter (serverid, 'LDAPFILTER') dirFilter 
            FROM isr$server
           WHERE servertypeid = 5;

        rGetLDAPServer cGetLDAPServer%rowtype;

      begin
       isr$trace.debug('begin', 'LDAPUSERNAME', sCurrentName);

        OPEN cGetLDAPServer;
        FETCH cGetLDAPServer INTO rGetLDAPServer;
        IF cGetLDAPServer%FOUND THEN
          WHILE cGetLDAPServer%FOUND LOOP

            clReturn := ISR$LDAP.getLDAPInformation ( sHost => rGetLDAPServer.HOST
                                                    , sPort => rGetLDAPServer.port
                                                    , sSSLAuth => rGetLDAPServer.sslauth
                                                    , sSSLWrl => rGetLDAPServer.sslWrl
                                                    , sSSLWalletPasswd => rGetLDAPServer.sslWalletPasswd
                                                    , sGlobUserName => rGetLDAPServer.username
                                                    , sGlobPassword => rGetLDAPServer.password
                                                    , sLDAPBase => rGetLDAPServer.ldapbase
                                                    , sAttributes => rGetLDAPServer.ldapsearch
                                                    , sFilter => rGetLDAPServer.dirFilter
                                                    ); 

            isr$trace.debug('clReturn', 's. logclob', sCurrentName, clReturn);
            FOR rGetUsers IN (SELECT username
                                FROM (SELECT EXTRACTVALUE (COLUMN_VALUE, '/ROW/' || rGetLDAPServer.ldapsearch || '/text()') AS username 
                                        FROM TABLE (XMLSEQUENCE (EXTRACT (xmltype (clReturn), '/ROWSET/ROW'))))
                               WHERE username NOT IN (SELECT ldapusername FROM stb$user where ldapusername is not null)
                            ORDER BY 1) LOOP
              --oSelectionList1.EXTEND;
              --oSelectionList1(oSelectionList1.COUNT()) := ISR$OSELECTION$RECORD(rGetUsers.userName, rGetUsers.userName, rGetUsers.userName, null, oSelectionList1.COUNT());
              oSelectionList.EXTEND;
              oSelectionList(oSelectionList.COUNT()) := ISR$OSELECTION$RECORD(rGetUsers.userName, rGetUsers.userName, rGetUsers.userName, null, oSelectionList.COUNT());
            END LOOP;

            FETCH cGetLDAPServer INTO rGetLDAPServer;
          END LOOP;
        ELSE
          RAISE exNoLDAPServer;
        END IF;
        CLOSE cGetLDAPServer;

        /*
        SELECT ISR$OSELECTION$RECORD (sDisplay, sValue, sInfo, sSelected, nOrderNum, sMasterKey, ISR$ATTRIBUTE$LIST ())
          BULK COLLECT
          INTO oSelectionList
          FROM TABLE (oSelectionList1)
        ORDER BY sDisplay;
        */

      end;

    --***************************************
    -- only oracle and ldap users used by isr ----------------------------------
    WHEN Upper (csParameter) = 'ORACLEUSER' THEN
        FOR rGetISRUser IN cGetISRUser LOOP
          oSelectionList.EXTEND;
          oSelectionList (oSelectionList.COUNT()) := ISR$OSELECTION$RECORD(rGetISRUser.username, rGetISRUser.username, rGetISRUser.fullname, null, oSelectionList.COUNT());
        END LOOP;

    --***************************************
    -- Users with email
    WHEN Upper (csParameter) = 'USERNO' THEN
        FOR rGetEmailUsers IN (select userno, fullname 
                                 from stb$user 
                                where email is not null 
                                order by 2) LOOP
          oSelectionList.EXTEND;
          oSelectionList (oSelectionList.COUNT()) := ISR$OSELECTION$RECORD(rGetEmailUsers.fullname, rGetEmailUsers.userno, rGetEmailUsers.fullname, null, oSelectionList.COUNT());
        END LOOP;

    --***************************************
    -- All groups
    WHEN Upper (csParameter) = 'USERGROUPNO' THEN
        FOR rGetGroups IN (select usergroupno, groupname 
                             from stb$usergroup 
                            where active = 'T' 
                            order by 2) LOOP
          oSelectionList.EXTEND;
          oSelectionList (oSelectionList.COUNT()) := ISR$OSELECTION$RECORD(rGetGroups.groupname, rGetGroups.usergroupno, rGetGroups.groupname, null, oSelectionList.COUNT());
        END LOOP;

    --***************************************
    -- All tokens
    WHEN Upper (csParameter) = 'TOKEN' THEN
        FOR rGetTokens IN (select distinct token, Utd$msglib.GetMsg(NVL(description, token), Stb$security.GetCurrentLanguage) description 
                             from stb$contextmenu 
                            where attribute = 'TOKEN' 
                              and NVL(visible, 'T') = 'T' 
                            order by 1) LOOP
          oSelectionList.EXTEND;
          oSelectionList (oSelectionList.COUNT()) := ISR$OSELECTION$RECORD(rGetTokens.token, rGetTokens.token, rGetTokens.description, null, oSelectionList.COUNT());
        END LOOP;

    --***************************************
    -- Languages ---------------------------------------------------------------
    WHEN UPPER(csParameter) = 'LANG' then
      for rGetLang in cGetLang  LOOP
        oSelectionList.EXTEND;
        oSelectionList(oSelectionList.COUNT()) := ISR$OSELECTION$RECORD(rGetLang.description, rGetLang.langid, rGetLang.description, null, oSelectionList.COUNT());
      END LOOP;

    --***************************************
    -- Data groups all ---------------------------------------------------------
    WHEN UPPER(csParameter) = 'DATAGROUP' then
      for rGetDataGroup in cGetDataGroup LOOP
        oSelectionList.EXTEND;
        oSelectionList(oSelectionList.COUNT()) := ISR$OSELECTION$RECORD(rGetDataGroup.groupname, rGetDataGroup.usergroupno, rGetDataGroup.description, null, oSelectionList.COUNT());
      END LOOP;

    --***************************************
    -- Only data groups of the user --------------------------------------------
    WHEN UPPER(csParameter) = 'DATAGROUP_ONLY_USER' then
      for rGetDataGroup in cGetDataGroupUser LOOP
        oSelectionList.EXTEND;
        oSelectionList(oSelectionList.COUNT()) := ISR$OSELECTION$RECORD(rGetDataGroup.groupname, rGetDataGroup.usergroupno, rGetDataGroup.description, null, oSelectionList.COUNT());
      END LOOP;

    --***************************************
    WHEN UPPER(csParameter) = 'DOCDEFINITION' then
      IF stb$object.GetCurrentDocid IS NOT null THEN
        FOR rReportType IN (SELECT reporttypeid FROM stb$report r, stb$doctrail d where r.repid = d.nodeid and d.docid = stb$object.GetCurrentDocid) LOOP
          nReportType := rReportType.reporttypeid;
        END LOOP; 
      ELSE
        nReportType := stb$object.getcurrentreporttypeid;
      END IF;
      FOR rGetDocdefinition in cGetDocdefinition(nReportType) LOOP
        oSelectionList.EXTEND;
        oSelectionList(oSelectionList.COUNT()) := ISR$OSELECTION$RECORD(rGetDocdefinition.display, rGetDocdefinition.VALUE, rGetDocdefinition.info, null, oSelectionList.COUNT());
      END LOOP;

    --***************************************
    ELSE

      BEGIN
        IF STB$OBJECT.getCurrentToken like '%AUDIT%' THEN
          SELECT ISR$XML.clobToXml (audittext)
            INTO xXml
            FROM stb$audittrail
           WHERE audittype = NVL (REPLACE (stb$object.getCurrentToken, 'CAN_DISPLAY_'), stb$object.getCurrentNodeType)
             AND (reference = stb$object.getCurrentNodeId
               OR reference = '-1');
        ELSE
          EXECUTE IMMEDIATE 'select xml from '||SUBSTR(STB$OBJECT.getCurrentToken, 0, 30) INTO xXml;
        END IF;
        isr$trace.debug ('audit_'||stb$object.getCurrentNodeType||'.xml',  'see xXml in logclob', sCurrentName, xXml.getClobVal());
      EXCEPTION WHEN OTHERS THEN
        -- nothing found, doesn't matter
        NULL;
      END;

      IF xXml IS NOT NULL THEN
        FOR rGetXmlFilter IN cGetXmlFilter(xXml, csParameter) LOOP
          oSelectionList.EXTEND;
          oSelectionList(oSelectionList.COUNT()) := ISR$OSELECTION$RECORD(SUBSTR(rGetXmlFilter.value, 0, 500), SUBSTR(rGetXmlFilter.value, 0, 500), SUBSTR(Utd$msglib.GetMsg(rGetXmlFilter.value, Stb$security.GetCurrentLanguage), 0, 2000), null, oSelectionList.COUNT());
        END LOOP;
      END IF;

      IF oSelectionList.COUNT() = 0 THEN
        -- check the parameter values
        isr$trace.debug('check the parameter values', 'check the parameter values', sCurrentName); 
        FOR rGetLovText in cGetLovText(csParameter, STB$OBJECT.getCurrentReportLanguage) LOOP
          oSelectionList.EXTEND;
          oSelectionList(oSelectionList.COUNT()) := ISR$OSELECTION$RECORD(rGetLovText.display, rGetLovText.VALUE, rGetLovText.info, null, oSelectionList.COUNT());
        END LOOP;

        -- go to lal
        IF oSelectionList.COUNT() = 0 THEN
          isr$trace.debug('check the lal', 'check the lal', sCurrentName); 
          IF sCurrentLal IS NULL THEN -- loop over all lals

            FOR rGetLals IN cGetLals LOOP
              IF STB$UTIL.checkMethodExists('getLov', rGetLals.packagename) = 'T' THEN
                EXECUTE immediate
                  'BEGIN
                     :1 := '|| rGetLals.packagename ||'.getLov(:2, :3, :4);
                   END;'
                Using OUT oErrorObj, IN csParameter, IN sFilter, OUT oSelectionList1;
                IF oSelectionList1 IS NOT NULL AND  oSelectionList1.COUNT() != 0 THEN
                  FOR i IN 1..oSelectionList1.COUNT LOOP
                    oSelectionList.EXTEND();
                    oSelectionList(oSelectionList.COUNT()) := oSelectionList1(i);
                  END LOOP;
                END IF; 
              END IF;
            END LOOP;

          ELSE

            IF STB$UTIL.checkMethodExists('getLov') = 'T' THEN
              EXECUTE immediate
                'BEGIN
                   :1 := '|| sCurrentLal ||'.getLov(:2, :3, :4);
                 END;'
              Using OUT oErrorObj, IN csParameter, IN sFilter, OUT oSelectionList;
            END IF;

          END IF;

          -- don't report errors
          oErrorObj := STB$OERROR$RECORD();
        END IF;
      END IF;

      -- get the entity list
      IF oSelectionList IS NULL OR oSelectionList.COUNT() = 0 THEN
        -- check first if arameter is an entity
        select count(1) into nCount 
        from isr$meta$entity
        where entityname = UPPER(csParameter);
        if nCount != 0 then
          -- the entity exists, so get the values
          -- fill Param List with values
          oParamList.AddParam('REPORTTYPEID', STB$OBJECT.getCurrentReporttypeId);
          oParamList.AddParam('REPID', STB$OBJECT.getCurrentRepid);
          -- other params here ?....
          oSelectionList := ISR$REMOTE$COLLECTOR.GetList (UPPER(csParameter), oParamList);
        end if;
      END IF;
  END CASE;

  IF sFilter IS NOT NULL AND oSelectionList IS NOT NULL AND  oSelectionList.COUNT() != 0 THEN
    isr$trace.debug ('sFilter',  sFilter, sCurrentName); 
    FOR i IN 1..oSelectionList.Count() LOOP
      IF oSelectionList(i).sValue = sFilter THEN
        isr$trace.debug('oSelectionList('||i||').sSelected', oSelectionList(i).sValue, sCurrentName); 
        oSelectionList(i).sSelected := 'T';
      END IF;
    END LOOP;
  END IF;

  isr$trace.stat('end', 'oSelectionList, see column LOGCLOB', sCurrentName);
  RETURN oErrorObj;

EXCEPTION
  WHEN exNoLDAPServer THEN
    oErrorObj.sErrorCode      :=Utd$msglib.GetMsg('exNoLDAPServer',Stb$security.GetCurrentLanguage);
    oErrorObj.handleError ( Stb$typedef.cnSeverityMessage, utd$msglib.getmsg ('cnSeverityMessage', stb$security.getCurrentlanguage,  csP1 => sqlerrm(sqlcode)), 
        sCurrentName, dbms_utility.format_error_stack ||chr(10)|| dbms_utility.format_error_backtrace );
    RETURN oErrorObj; 
  WHEN OTHERS then
      oErrorObj.handleError ( Stb$typedef.cnSeverityCritical, utd$msglib.getmsg ('exUnexpectedError', stb$security.getCurrentlanguage,  csP1 => sqlerrm(sqlcode)), 
        sCurrentName, dbms_utility.format_error_stack ||chr(10)|| dbms_utility.format_error_backtrace );
    RETURN oErrorObj;
END getLov;
--==**********************************************************************************************************************************************==--
FUNCTION prepareForXML(csData IN VARCHAR2, csType IN VARCHAR2) RETURN VARCHAR2
IS
  sCurrentName   constant varchar2(4000) := $$PLSQL_UNIT||'.prepareForXML('||' '||')';
  sReturn        VARCHAR2(4000);
  sCurrentLal    constant VARCHAR2(100) := STB$UTIL.getCurrentLal;

BEGIN
  isr$trace.stat('begin', 'begin', sCurrentName);
  sReturn := csData;

  -- go to lal
  IF STB$UTIL.checkMethodExists('prepareForXML') = 'T' THEN
    EXECUTE immediate
      'BEGIN
         :1 := '|| sCurrentLal ||'.prepareForXML(:2, :3);
       END;'
    Using OUT sReturn, IN csData, IN csType;
  END IF;

  isr$trace.stat('end', 'end', sCurrentName);
  RETURN sReturn;
END prepareForXML;

--***************************************************************************************************
FUNCTION preparePostXML(cnJobId IN NUMBER, oParamList IN ISR$ATTRLIST$REC, xXml IN OUT XMLTYPE) RETURN STB$OERROR$RECORD 
IS
  sCurrentName   constant varchar2(4000) := $$PLSQL_UNIT||'.preparePostXML('||cnJobId||')';
  oErrorObj      STB$OERROR$RECORD := STB$OERROR$RECORD();
  sCurrentLal    constant VARCHAR2(100) := STB$UTIL.getCurrentLal;

BEGIN
  isr$trace.stat('begin', 'begin', sCurrentName);

  -- go to lal
  IF STB$UTIL.checkMethodExists('preparePostXML') = 'T' THEN
    EXECUTE immediate
      'BEGIN
         :1 := '|| sCurrentLal ||'.preparePostXML(:2, :3, :4);
       END;'
    Using OUT oErrorObj, IN cnJobId, IN oParamList, IN OUT xXml;
  END IF;

  isr$trace.stat('end', 'end', sCurrentName);
  RETURN oErrorObj;
EXCEPTION
  WHEN OTHERS THEN
      oErrorObj.handleError ( Stb$typedef.cnSeverityCritical, utd$msglib.getmsg ('exUnexpectedError', stb$security.getCurrentlanguage,  csP1 => sqlerrm(sqlcode)), 
        sCurrentName, dbms_utility.format_error_stack ||chr(10)|| dbms_utility.format_error_backtrace );
    RETURN oErrorObj;
END preparePostXML;

--*************************************************************************************************************************************
FUNCTION getDefaultValues ( oSelection IN OUT ISR$TLRSELECTION$LIST,
                            csDefaultToken IN VARCHAR2,
                            csEntity IN VARCHAR2 ) RETURN STB$OERROR$RECORD
IS
  sCurrentName                constant varchar2(100) := $$PLSQL_UNIT||'.getDefaultValues(' || csEntity || ')';
  oErrorObj                   STB$OERROR$RECORD := STB$OERROR$RECORD();
  nLineIndex                  NUMBER;

  sDefault                    VARCHAR2(4000);
  sFound                      VARCHAR2(1) := 'F';

  sValue                      VARCHAR2(4000);
  sFilter                     VARCHAR2(4000);
  sFilterAll                  VARCHAR2(4000);

  sWizardAttribute            VARCHAR2(50);

  nMaskType                   number;
  sParamNameForLogicFilter    VARCHAR2(4000);
  sParamForLogicFilterDefined number;
  sCurrentLal                 constant VARCHAR2(100) := STB$UTIL.getCurrentLal;


--  CURSOR cGetDefaultSelection IS
--    SELECT token_default_selection
--      FROM isr$report$wizard
--     WHERE entity = csEntity
--       AND reporttypeid = STB$OBJECT.getCurrentReporttypeId;
--
  CURSOR cGetDefaultSelection IS
    -- outer join to grid entities to allow logicalfilter also for grid columns
    SELECT token_default_selection
      FROM isr$report$wizard wz,
           isr$meta$grid$entity mge
     WHERE wz.entity = mge.wizardentity(+)
       and wz.reporttypeid = mge.reporttypeid(+)
       and (wz.entity = csEntity or mge.entityname = csEntity) 
       AND wz.reporttypeid = STB$OBJECT.getCurrentReporttypeId;

  cursor cGetWizardMaskType is
    select masktype
      from isr$report$wizard wz,
           isr$meta$grid$entity mge
     where wz.entity = mge.wizardentity(+)
       and wz.reporttypeid = mge.reporttypeid(+)
       and (wz.entity = csEntity or mge.entityname = csEntity)
       and wz.reporttypeid = STB$OBJECT.getCurrentReporttypeId;

  CURSOR cGetGridDefFilter IS
    SELECT VALUE
      FROM (SELECT STB$UTIL.getReptypeParameter (
                      STB$OBJECT.getCurrentReporttypeid
                    , csEntity || '_DEFAULT'
                   )
                      VALUE
                 , 1 ordernumber
              FROM DUAL
            UNION
            SELECT VALUE, 2 ordernumber
              FROM ISR$PARAMETER$VALUES
             WHERE entity = csEntity || '_DEFAULT')
       WHERE TRIM (VALUE) IS NOT NULL
    ORDER BY ordernumber;

  CURSOR cGetGridWizardAttribute IS
    SELECT WIZARDATTRIBUTE
      FROM ISR$META$ATTRIBUTE
     WHERE entityname = csEntity
       AND WIZARDATTRIBUTE is not null
    ORDER BY ORDERNO
    FETCH FIRST 1 ROW ONLY;

  oSelectionRec     ISR$OSELECTION$RECORD;
  oSelection1       ISR$TLRSELECTION$LIST := ISR$TLRSELECTION$LIST();
  oSelection2       ISR$TLRSELECTION$LIST := ISR$TLRSELECTION$LIST();
  oParamList        iSR$ParamList$Rec     := iSR$ParamList$Rec();
BEGIN
  isr$trace.stat('begin','csDefaultToken: '||csDefaultToken,sCurrentName);
  isr$trace.debug('parameter','oSelection',sCurrentName,oSelection);

  OPEN cGetDefaultSelection;
  FETCH cGetDefaultSelection INTO sDefault;
  CLOSE cGetDefaultSelection;
  isr$trace.debug('value', 'sDefault: '||sDefault, sCurrentName);

  IF TRIM(sDefault) IS NOT NULL THEN

    IF Upper(Trim(sDefault)) LIKE 'LOGICALFILTER%' THEN
      isr$trace.debug(csEntity||' sDefault is logicalfilter',  sDefault, sCurrentName);

      open cGetWizardMaskType;
      fetch cGetWizardMaskType INTO nMaskType;
      close cGetWizardMaskType;

      if nMaskType = STB$TYPEDEF.cnMasterDetail then
        isr$trace.debug('code position', 'determine logical filter for master-detail masktype for entity '||csEntity, sCurrentName);

        for i in 1 .. oSelection.count() loop

          /*master detail can define default selection per master entry
            for this a parameter depending on the master key is concatinated*/
          sValue := stb$util.getLeftDelim(oSelection(i).sMasterKey, '#@#');
          isr$trace.debug_all('sValue for ' || csEntity || ' oSelection(i).sMasterKey', sValue, sCurrentName);

          sParamNameForLogicFilter    := csEntity || '_DEFAULT_SELECTION_' || sValue;
          isr$trace.debug_all('parametername for logical filter for entity ' || csEntity, sParamNameForLogicFilter, sCurrentName);

          /*chech if the parameter with the name is defined*/

          /*select count(*)
            into sParamForLogicFilterDefined
            from stb$reptypeparameter
           where parametername = sParamNameForLogicFilter
             and reporttypeid = STB$OBJECT.getCurrentReporttypeId;

          if sParamForLogicFilterDefined > 0 then */

            /*now we have the name for the parameter containing the logical filter*/
          sFilter    := STB$UTIL.getReptypeParameter(STB$OBJECT.getCurrentReporttypeId, sParamNameForLogicFilter);
          isr$trace.debug_all('sFilter for ' || csEntity, sFilter, sCurrentName);
          if sFilter is null then
            isr$trace.warn('configuration warning', 'logical filter is defined in isr$report$wizard, but the is no corresponding filter definition in stb$reptypeparameter for '||sParamNameForLogicFilter, sCurrentName);
          else
            if regexp_like(oSelection(i).sValue, sFilter, 'i') then
              oSelection(i).sSelected    := 'T';
              isr$trace.debug_all(oSelection(i).sValue || ' matches ' || sFilter, oSelection(i).sSelected, sCurrentName);
            end if;
          end if;
        end loop;
      elsif nMaskType = STB$TYPEDEF.cnSpecial then -- COVIB-689 HSP: New logicalfilter that treats grid columns like a master-detail entity with the grid entity as master
        isr$trace.debug('code position', 'determine logical filter for grid masktype for entity ' || csEntity, sCurrentName);

        for i in 1 .. oSelection.count() loop
          /*grid can define default selection per master entry
            for this a parameter depending on the master key is concatinated*/
          sValue := stb$util.getLeftDelim(oSelection(i).sMasterKey, '#@#');
          isr$trace.debug_all('sValue for ' || csEntity || ' oSelection(i).sMasterKey', sValue, sCurrentName);

          sParamNameForLogicFilter := csEntity || '_DEFAULT_SELECTION';
          isr$trace.debug_all('parametername for catch all logical filter for entity ' || csEntity, sParamNameForLogicFilter, sCurrentName);
          sFilterAll := STB$UTIL.getReptypeParameter(STB$OBJECT.getCurrentReporttypeId, sParamNameForLogicFilter);

          sParamNameForLogicFilter := csEntity || '_DEFAULT_SELECTION_' || sValue;
          isr$trace.debug_all('parametername for logical filter for entity ' || csEntity, sParamNameForLogicFilter, sCurrentName);
          sFilter := STB$UTIL.getReptypeParameter(STB$OBJECT.getCurrentReporttypeId, sParamNameForLogicFilter);

          isr$trace.debug_all('sFilter for ' || csEntity, sFilter, sCurrentName);
          if sFilter is null and sFilterAll is null then
            isr$trace.warn('configuration warning', 'logical filter is defined in isr$report$wizard, but the is no corresponding filter definition in stb$reptypeparameter for '||sParamNameForLogicFilter, sCurrentName);
          else
            -- Check the type of the grid gield (for now either LoV or text field)
            open cGetGridWizardAttribute;
            fetch cGetGridWizardAttribute INTO sWizardAttribute;
            close cGetGridWizardAttribute;

            -- check if the entity field is EDITABLE (Text Field)
            if sWizardAttribute = 'EDITABLE' then
              oSelection(i).sDisplay := sFilter;
              oSelection(i).sValue := sFilter;
              oSelection(i).sInfo := sFilter;
            else
              oSelectionRec := null;
              -- fill Param List with values
              oParamList.AddParam('REPORTTYPEID', STB$OBJECT.getCurrentReporttypeId);
              oParamList.AddParam('REPID', STB$OBJECT.getCurrentRepid);
              -- other params here ?....
              -- then call getList
              oSelection1 := ISR$REMOTE$COLLECTOR.getList (csEntity, oParamList);

              SELECT ISR$OSELECTION$RECORD (e.sDisplay
                                          , e.sValue
                                          , e.sInfo
                                          , 'T' --sSelected
                                          , oSelection(i).nOrderNum
                                          , oSelection(i).sMasterkey
                                          , oSelection(i).tloAttributeList)
                INTO oSelectionRec
                FROM table (oSelection1) e
               WHERE e.sValue = nvl(sFilter,sFilterAll)
               FETCH FIRST 1 ROW ONLY;

              if oSelectionRec is not null then
                oSelection(i) := oSelectionRec;
              end if;
            end if;
          end if;
        end loop;
      else
        isr$trace.debug('code position', 'determine logical filter for non master-detail/grid masktype for entity ' || csEntity, sCurrentName);

        /*for other masktypes the is no master key, hence the parametername for logical filter do not contains it*/

        sParamNameForLogicFilter    := csEntity || '_DEFAULT_SELECTION';
        isr$trace.debug('parametername for logical filter for entity ' || csEntity, sParamNameForLogicFilter, sCurrentName);

        /*check if the parameter with the name is defined*/

        select count(*)
          into sParamForLogicFilterDefined
          from stb$reptypeparameter
         where parametername = sParamNameForLogicFilter
           and reporttypeid = STB$OBJECT.getCurrentReporttypeId;

        if sParamForLogicFilterDefined > 0
        then

          /*now we have the name for the parameter containing the logical filter*/
          sFilter    := STB$UTIL.getReptypeParameter(STB$OBJECT.getCurrentReporttypeId, sParamNameForLogicFilter);
          isr$trace.debug(csEntity || ' sFilter', sFilter, sCurrentName);

          for i in 1 .. oSelection.count()
          loop
            if regexp_like(oSelection(i).sValue, sFilter, 'i')
            then
              oSelection(i).sSelected    := 'T';
              isr$trace.debug_all(oSelection(i).sValue || ' matches ' || sFilter, oSelection(i).sSelected, sCurrentName);
            end if;
          end loop;
        else
          isr$trace.warn('configuration warning', 'logical filter is defined in isr$report$wizard, but the is no corresponding filter definition in stb$reptypeparameter for '||sParamNameForLogicFilter, sCurrentName);
        end if;
      end if;

      -- go to lal 
      IF STB$UTIL.checkMethodExists('getDefaultValues') = 'T' THEN
        EXECUTE immediate
          'BEGIN
             :1 := '|| sCurrentLal ||'.getDefaultValues(:2, :3, :4);
           END;'
        Using OUT oErrorObj, IN OUT oSelection, IN csDefaultToken, IN csEntity;
      END IF;

      -- select the element if only one element is in the list
      IF oSelection.Count() = 1 THEN
        oSelection(1).sSelected := 'T';
      END IF;

    ELSE  -- Upper(Trim(sDefault)) is not like 'LOGICALFILTER%'
      -- here is ony string comparision possible
      nLineIndex := oSelection.First;
      isr$trace.debug('sDefault is not LOGICALFILTER%','nLineIndex: '||nvl(to_char(nLineIndex),'NULL'), sCurrentName);

      WHILE oSelection.EXISTS(nLineIndex) LOOP
        isr$trace.debug('oSelection('||nLineIndex||').sValue', oSelection(nLineIndex).sValue, sCurrentName);
        IF(oSelection(nLineIndex).sValue LIKE sDefault) THEN
          oSelection(nLineIndex).sSelected := 'T';
          isr$trace.debug('oSelection('||nLineIndex||').sSelected',oSelection(nLineIndex).sSelected, sCurrentName);
        END IF;
        nLineIndex := oSelection.NEXT(nLineIndex);
      END LOOP;

    END IF;

  ELSE
    isr$trace.debug(csEntity||' sDefault is null',  sDefault, sCurrentName);

    OPEN cGetGridDefFilter;
    FETCH cGetGridDefFilter INTO sFilter;
    IF cGetGridDefFilter%FOUND AND TRIM(sFilter) IS NOT NULL THEN

      isr$trace.debug(csEntity||' sFilter',  sFilter, sCurrentName);

      -- fill Param List with values
      oParamList.AddParam('REPORTTYPEID', STB$OBJECT.getCurrentReporttypeId);
      oParamList.AddParam('REPID', STB$OBJECT.getCurrentRepid);
      -- other params here ?....
      -- then call getList
      oSelection1 := ISR$REMOTE$COLLECTOR.getList (csEntity, oParamList);

      SELECT ISR$OSELECTION$RECORD (sDisplay
                                  , sValue
                                  , sInfo
                                  , sSelected
                                  , nOrderNum
                                  , sMasterKey
                                  , tloAttributeList)
        BULK COLLECT
        INTO oSelection2
        FROM (SELECT e.sDisplay
                   , e.sValue
                   , NULL sInfo
                   , 'T' sSelected
                   , sel.nOrderNum
                   , sel.sMasterkey
                   , sel.tloAttributeList
                FROM table (oSelection1) e
                   , table (oSelection) sel
               WHERE e.sValue = sFilter);

      oSelection := oSelection2;

    END IF;
    CLOSE cGetGridDefFilter;

  END IF;

  isr$trace.debug('parameter','oSelection',sCurrentName,oSelection);
  isr$trace.stat('end', 'end', sCurrentName);
  RETURN oErrorObj;

EXCEPTION
  WHEN OTHERS THEN
      oErrorObj.handleError ( Stb$typedef.cnSeverityCritical, utd$msglib.getmsg ('exUnexpectedError', stb$security.getCurrentlanguage,  csP1 => sqlerrm(sqlcode)), 
        sCurrentName, dbms_utility.format_error_stack ||chr(10)|| dbms_utility.format_error_backtrace );
    RETURN oErrorObj;
END getDefaultValues;

--***************************************************************************************************
FUNCTION getGridMaskAdds(sEntity IN STB$TYPEDEF.tLine, csMasterKey IN varchar2, lAdds OUT iSR$Crit$List) RETURN STB$OERROR$RECORD 
IS
  sCurrentName   constant varchar2(4000) := $$PLSQL_UNIT||'.getGridMaskAdds('||sEntity||')';
  oErrorObj      STB$OERROR$RECORD := STB$OERROR$RECORD();
  sCurrentLal    constant VARCHAR2(100) := STB$UTIL.getCurrentLal;

  CURSOR cAdds IS
    SELECT Value, display, info 
    FROM iSR$Parameter$Values
    WHERE entity = sEntity||'_ADD';
  rToAdd cAdds%ROWTYPE;
BEGIN
  isr$trace.stat('begin','csMasterKey:'||csMasterKey,50, sCurrentName);

  lAdds := iSR$Crit$List();

  -- go to lal
  IF STB$UTIL.checkMethodExists('getGridMaskAdds') = 'T' THEN
    EXECUTE immediate
      'BEGIN
         :1 := '|| sCurrentLal ||'.getGridMaskAdds(:2, :3, :4);
       END;'
    Using OUT oErrorObj, IN sEntity, IN csMasterKey, OUT lAdds;
  END IF;

  OPEN cAdds;
  FETCH cAdds INTO rToAdd;
  WHILE cAdds%FOUND LOOP
    lAdds.EXTEND;
    lAdds(lAdds.Last) := iSR$Crit$Rec(rToAdd.Value, rToAdd.Value||CASE WHEN csMasterKey IS NULL THEN NULL ELSE '#@#'||csMasterKey END, 
                         rToAdd.display, rToAdd.info, NULL);
    FETCH cAdds INTO rToAdd;
  END LOOP;
  CLOSE cAdds;

  isr$trace.debug('status','entries found:'||lAdds.Count,sCurrentName);
  isr$trace.stat('end', 'end', sCurrentName);
  RETURN oErrorObj;
EXCEPTION
  WHEN OTHERS THEN
      oErrorObj.handleError ( Stb$typedef.cnSeverityCritical, utd$msglib.getmsg ('exUnexpectedError', stb$security.getCurrentlanguage,  csP1 => sqlerrm(sqlcode)), 
        sCurrentName, dbms_utility.format_error_stack ||chr(10)|| dbms_utility.format_error_backtrace );
    RETURN oErrorObj;
END GetGridMaskAdds;

--***************************************************************************************************
FUNCTION getEntityDefault(sEntity IN STB$TYPEDEF.tLine, csMasterKey IN varchar2, oRow OUT iSR$Crit$Rec) RETURN STB$OERROR$RECORD IS
  sCurrentName   constant varchar2(4000) := $$PLSQL_UNIT||'.getEntityDefault('||sEntity||')';
  oErrorObj         STB$OERROR$RECORD := STB$OERROR$RECORD();
  CURSOR cDefault IS
    SELECT value, display, info
      FROM ISR$PARAMETER$VALUES
     WHERE entity = sEntity||'_DEFAULT';
  rDefault cDefault%ROWTYPE;
  sCurrentLal         constant VARCHAR2(100) := STB$UTIL.getCurrentLal;


BEGIN
  isr$trace.stat('begin','csMasterKey:'||csMasterKey, sCurrentName);

  -- go to lal
  IF STB$UTIL.checkMethodExists('getEntityDefault') = 'T' THEN
    EXECUTE immediate
      'BEGIN
         :1 := '|| sCurrentLal ||'.getEntityDefault(:2, :3, :4);
       END;'
    Using OUT oErrorObj, IN sEntity, IN csMasterKey, OUT oRow;
  END IF;

  OPEN cDefault;
  FETCH cDefault INTO rDefault;
  IF cDefault%FOUND THEN
    oRow := iSR$Crit$Rec(rDefault.Value, csMasterKey, rDefault.display, rDefault.info, NULL);
  END IF;
  CLOSE cDefault;

  isr$trace.debug('status',oRow.key,sCurrentName, oRow);
  isr$trace.stat('end', 'end', sCurrentName);
  RETURN oErrorObj;
EXCEPTION
  WHEN OTHERS THEN
      oErrorObj.handleError ( Stb$typedef.cnSeverityCritical, utd$msglib.getmsg ('exUnexpectedError', stb$security.getCurrentlanguage,  csP1 => sqlerrm(sqlcode)), 
        sCurrentName, dbms_utility.format_error_stack ||chr(10)|| dbms_utility.format_error_backtrace );
    RETURN oErrorObj;
END GetEntityDefault;

--***************************************************************************************************
FUNCTION getGridMaster(cnMaskNo IN NUMBER, sMasterEntity OUT STB$TYPEDEF.tLine) RETURN STB$OERROR$RECORD
IS
  sCurrentName   constant varchar2(4000) := $$PLSQL_UNIT||'.getGridMaster('||cnMaskNo||')';
  oErrorObj                   STB$OERROR$RECORD := STB$OERROR$RECORD();
  sCurrentLal         constant VARCHAR2(100) := STB$UTIL.getCurrentLal;

BEGIN
  isr$trace.stat('begin', 'begin', sCurrentName);

  -- go to lal
  IF STB$UTIL.checkMethodExists('getGridMaster') = 'T' THEN
    EXECUTE immediate
      'BEGIN
         :1 := '|| sCurrentLal ||'.getGridMaster(:2, :3);
       END;'
    Using OUT oErrorObj, IN cnMaskNo, OUT sMasterEntity;
  END IF;

  isr$trace.stat('end', 'end', sCurrentName);
 RETURN oErrorObj;
EXCEPTION
  WHEN OTHERS THEN
      oErrorObj.handleError ( Stb$typedef.cnSeverityCritical, utd$msglib.getmsg ('exUnexpectedError', stb$security.getCurrentlanguage,  csP1 => sqlerrm(sqlcode)), 
        sCurrentName, dbms_utility.format_error_stack ||chr(10)|| dbms_utility.format_error_backtrace );
    RETURN oErrorObj;
END  getGridMaster;

--***************************************************************************************************
FUNCTION SetWizardAttributes(oSelection in OUT ISR$TLRSELECTION$LIST, csEntity IN VARCHAR2, csMasterKey IN VARCHAR2 DEFAULT NULL) RETURN STB$OERROR$RECORD
IS
  sCurrentName   constant varchar2(4000) := $$PLSQL_UNIT||'.SetWizardAttributes('||csEntity||')';
  oErrorObj      STB$OERROR$RECORD := STB$OERROR$RECORD();
  sCurrentLal    constant VARCHAR2(100) := STB$UTIL.getCurrentLal;

BEGIN
  isr$trace.stat('begin', 'begin', sCurrentName);

  -- go to lal
  IF STB$UTIL.checkMethodExists('SetWizardAttributes') = 'T' THEN
    -- csMasterKey - optional parameter needed in grids for hierarchical lists - see ISRC-764
    EXECUTE immediate
      'BEGIN
         :1 := '|| sCurrentLal ||'.SetWizardAttributes(:2, :3, :4);
       END;'
    Using OUT oErrorObj, IN OUT oSelection, IN csEntity, IN csMasterKey;
  END IF;

  isr$trace.stat('end', 'end', sCurrentName);
 RETURN oErrorObj;
EXCEPTION
  WHEN OTHERS THEN
      oErrorObj.handleError ( Stb$typedef.cnSeverityCritical, utd$msglib.getmsg ('exUnexpectedError', stb$security.getCurrentlanguage,  csP1 => sqlerrm(sqlcode)), 
        sCurrentName, dbms_utility.format_error_stack ||chr(10)|| dbms_utility.format_error_backtrace );
    RETURN oErrorObj;
END SetWizardAttributes;

--***************************************************************************************************
FUNCTION PostGridSelection(csEntity IN VARCHAR2) RETURN STB$OERROR$RECORD
IS
  sCurrentName   constant varchar2(4000) := $$PLSQL_UNIT||'.PostGridSelection('||csEntity||')';
  oErrorObj      STB$OERROR$RECORD := STB$OERROR$RECORD();
  sCurrentLal    constant VARCHAR2(100) := STB$UTIL.getCurrentLal;

BEGIN
  isr$trace.stat('begin', 'begin', sCurrentName);
  isr$trace.debug('begin','begin. Entity:'||csEntity, sCurrentName);

  -- go to lal
  IF STB$UTIL.checkMethodExists('PostGridSelection') = 'T' THEN
    EXECUTE immediate
      'BEGIN
         :1 := '|| sCurrentLal ||'.PostGridSelection(:2);
       END;'
    Using OUT oErrorObj, IN csEntity;
  END IF;

  isr$trace.stat('end', 'end', sCurrentName);
 RETURN oErrorObj;
EXCEPTION
  WHEN OTHERS THEN
      oErrorObj.handleError ( Stb$typedef.cnSeverityCritical, utd$msglib.getmsg ('exUnexpectedError', stb$security.getCurrentlanguage,  csP1 => sqlerrm(sqlcode)), 
        sCurrentName, dbms_utility.format_error_stack ||chr(10)|| dbms_utility.format_error_backtrace );
    RETURN oErrorObj;
END PostGridSelection;

--***************************************************************************************************
Function InitOutputOptions RETURN stb$oerror$record 
IS
  sCurrentName   constant varchar2(4000) := $$PLSQL_UNIT||'.InitOutputOptions';
  oErrorObj      STB$OERROR$RECORD := STB$OERROR$RECORD();
  sCurrentLal    constant VARCHAR2(100) := STB$UTIL.getCurrentLal;

BEGIN
  isr$trace.stat('begin', 'begin', sCurrentName);

  -- go to lal
  IF STB$UTIL.checkMethodExists('InitOutputOptions', sCurrentLal) = 'T' THEN
    EXECUTE immediate
      'BEGIN
         :1 := '|| sCurrentLal ||'.InitOutputOptions;
       END;'
    Using OUT oErrorObj;
  else 
    isr$trace.debug('not found in', sCurrentLal, sCurrentName);
  END IF;

  isr$trace.stat('end', 'end', sCurrentName);
  RETURN oErrorObj;
EXCEPTION
  WHEN OTHERS THEN
      oErrorObj.handleError ( Stb$typedef.cnSeverityCritical, utd$msglib.getmsg ('exUnexpectedError', stb$security.getCurrentlanguage,  csP1 => sqlerrm(sqlcode)), 
        sCurrentName, dbms_utility.format_error_stack ||chr(10)|| dbms_utility.format_error_backtrace );
    RETURN oErrorObj;
END InitOutputOptions;

--***************************************************************************************************
Function ExitOutputOptions RETURN stb$oerror$record 
IS
  sCurrentName   constant varchar2(4000) := $$PLSQL_UNIT||'.ExitOutputOptions';
  oErrorObj               STB$OERROR$RECORD := STB$OERROR$RECORD();
  sCurrentLal         constant VARCHAR2(100) := STB$UTIL.getCurrentLal;

BEGIN
  isr$trace.stat('begin', 'begin', sCurrentName);

  -- go to lal
  IF STB$UTIL.checkMethodExists('ExitOutputOptions', sCurrentLal) = 'T' THEN
    EXECUTE immediate
      'BEGIN
         :1 := '|| sCurrentLal ||'.ExitOutputOptions;
       END;'
    Using OUT oErrorObj;
  else 
    isr$trace.debug('not found in', sCurrentLal, sCurrentName);
  END IF;

  isr$trace.stat('end', 'end', sCurrentName);
  RETURN oErrorObj;
EXCEPTION
  WHEN OTHERS THEN
      oErrorObj.handleError ( Stb$typedef.cnSeverityCritical, utd$msglib.getmsg ('exUnexpectedError', stb$security.getCurrentlanguage,  csP1 => sqlerrm(sqlcode)), 
        sCurrentName, dbms_utility.format_error_stack ||chr(10)|| dbms_utility.format_error_backtrace );
    RETURN oErrorObj;
END ExitOutputOptions;

END ISR$UTD$PACKAGE;