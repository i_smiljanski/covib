CREATE OR REPLACE PACKAGE ISR$XML is
-- ***************************************************************************************************
-- Description/Usage:
-- The package is used for xml creation and preparation
-- The package uses the xml dom functionality of the oracle xml parser
-- With Oracle 10g the xml dom object was changed from the type number to the type raw,
-- the casting of the object can be found in the package
-- It is necessary for performance issues to use the xmltype functionality in future!
-- ***************************************************************************************************

FUNCTION initXml(sRootName in VARCHAR2, sCoding in VARCHAR2 default STB$UTIL.GETSYSTEMPARAMETER('XML_ENCODING')) RETURN XMLTYPE;
-- ***************************************************************************************************
-- Date and Author: 17. May 2011, MCD
-- initializes a new xml with the given rootnode 
-- if a coding is defined this coding is used, otherwise the default coding is used
--
-- Parameter:
-- sRootName    name of the root-node
--
-- Return:
-- NUMBER      the DomID as number
--
-- ***************************************************************************************************

FUNCTION ClobToXML (clXml in CLOB) RETURN XMLTYPE;
-- ***************************************************************************************************
-- Date and Author: 17. May 2011, MCD
-- converts a clob XML file to an xmltype
--
-- Parameter:
-- clXml        the CLOB containing the XML-File
--
-- Return:
-- NUMBER       the DomID of the created DOM
--
-- ***************************************************************************************************

FUNCTION XMLToClob (xXml in XMLTYPE) RETURN CLOB;
-- ***************************************************************************************************
-- Date and Author: 17. May 2011, MCD
-- converts an xmltype to a clob
--
-- Parameter:
-- xXml         the XML
--
-- Return:
-- CLOB         the CLOB, into which the XML-File has been written.
--
-- ***************************************************************************************************

FUNCTION XSLTTransform(cXSLT in CLOB, cXML in CLOB, cResult in OUT CLOB, sParameter IN VARCHAR2 DEFAULT NULL, cnJobid in number ) RETURN VARCHAR2;
-- ***************************************************************************************************
-- Date and Author: 17. May 2011, MCD
-- transforms a XML CLOB using an XSLT CLOB
-- in most cases (systemparameter PARSER) the SAXON parser is used to transform 
--
-- Parameter:
-- cXSLT         XML-Stylesheet as CLOB which will be used for the transformation
-- cXML          XML-Documnet as CLOB which is transformed
-- cResult       Resulting Document as CLOB
-- sParameter    the parameter
-- cnJobid       the job ID
--
-- Return:
-- VARCHAR2      an empy string on success, an errordescription otherwise
--
-- ***************************************************************************************************

FUNCTION valueOf (xXml in XMLTYPE, sXpath in VARCHAR2) RETURN VARCHAR2;
-- ***************************************************************************************************
-- Date and Author: 17. May 2011, MCD
-- retrieves the value of a node or attribute according to the given xpath
--
-- Parameter:
-- xXml         the XML
-- sXpath       XPATH to the Node/Attribute, which value should be retrieved
--
-- Return:
-- VARCHAR2      
--
-- ***************************************************************************************************

FUNCTION ValueAsClob (xXml IN XMLTYPE,
                  sXpath IN VARCHAR2) RETURN clob;

-- ********************************************************************************************************                  

PROCEDURE InsertChildXMLFragment(xXml in out XMLTYPE, sXpath in varchar2, clXML in clob);
-- ***************************************************************************************************
-- Date and Author: 17. May 2011, MCD
-- inserts a fragment into a XML to the node specified in the xpath
-- Renamed from CreateDomFragmentFromString
--
-- Parameter:
-- xXml         the XML to be modified
-- sXpath       XPATH to the Node, under which the fragment should be created
-- clXML        XML string fragment to be appended. Has to have a root element
--
-- ***************************************************************************************************


FUNCTION getValuesProperties (nJobId IN NUMBER, lXpathes in ISR$ATTRIBUTE$LIST, xXml IN XMLTYPE) RETURN CLOB;
-- ***************************************************************************************************
-- Date and Author: 17. May 2011, MCD
-- retrieves values for xpathes given in the list and return them in a clob
--
-- Parameter:
-- nJobid       the job ID
-- lXpathes     xpathes in a list
-- xXml         the XML 
--
-- ***************************************************************************************************

PROCEDURE CreateNode (xXml in out XMLTYPE, sXpath in VARCHAR2, sName in VARCHAR2, sValue in VARCHAR2);
-- ***************************************************************************************************
-- Date and Author: 17. May 2011, MCD
-- creates a new node with the given name and value at the given XPATH
--
-- Parameter:
-- xXml         the XML 
-- sXpath       XPATH to the Node, under which the new node should be created
-- sName        Name of the new node
-- sValue       Value of the new node
--
-- ***************************************************************************************************

PROCEDURE CreateNodeAttr (xXml in out XMLTYPE, sXpath in VARCHAR2, sName in VARCHAR2, sValue in VARCHAR2, sAttrName in VARCHAR2, sAttrValue in VARCHAR2);
-- ***************************************************************************************************
-- Date and Author: 17. May 2011, MCD
-- combined function: creates a new node with one attribute at the given XPATH
--
-- Parameter:
-- xXml         the XML 
-- sXpath       XPATH to the Node, under which the new node should be created
-- sName        Name of the new node
-- sValue       Value of the new node
-- sAttrName    Name of the attribute of the new node
-- sAttrValue   Value of the attribute of the new node
--
-- ***************************************************************************************************

PROCEDURE SetAttribute (xXml in out XMLTYPE, sXpath in VARCHAR2, sName in VARCHAR2, sValue in VARCHAR2);
-- ***************************************************************************************************
-- Date and Author: 17. May 2011, MCD
-- creates a new attribute with the given name and value at the given XPATH 
--
-- Parameter:
-- xXml         the XML 
-- sXpath       XPATH to the Node, under which the new node should be created
-- sName        Name of the new attribute
-- sValue       Value of the new attribute
--
-- ***************************************************************************************************

PROCEDURE UpdateNode (xXml in out XMLTYPE, sXpath in VARCHAR2, sValue in VARCHAR2);
-- ***************************************************************************************************
-- Date and Author: 17. May 2011, MCD
-- updates the node given in XPATH with the new value
--
-- Parameter:
-- xXml         the XML 
-- sXpath       XPATH to the Node, under which the new node should be created
-- sValue       Value of the new node
--
-- ***************************************************************************************************

PROCEDURE DeleteNode (xXml in out XMLTYPE, sXpath in VARCHAR2);
-- ***************************************************************************************************
-- Date and Author: 17. May 2011, MCD
-- removes a node from the xml
--
-- Parameter:
-- xXml         the XML 
-- sXpath       XPATH to the node, which should be deleted
--
-- ***************************************************************************************************

FUNCTION InsertChildXMLFragment(xXml in XMLTYPE, sXpath IN VARCHAR2, xAppendXML IN XMLTYPE) RETURN XMLTYPE;
-- ***************************************************************************************************
-- Date and Author: 16. May 2011, MCD
-- appends an xml fragment on the node given in xpath and return the new xml
-- Renamed from CreateDomFragmentFromString
--
-- Parameter:
-- xXml         the xml
-- sXpath       the xpath on which to append
-- xAppendXML   the xml to append
--
-- Return:
-- XMLTYPE      the new xml
--
-- ***************************************************************************************************

FUNCTION getXmlValue(sValue IN CLOB) RETURN CLOB;
-- ***************************************************************************************************
-- Date and Author: 16. May 2011, MCD
-- checks an value if it contains < > or &
--
-- Parameter:
-- sValue   the value to check
--
-- Return:
-- CLOB     the clean value
--
-- ***************************************************************************************************

FUNCTION getOriginalValue(sValue IN CLOB) RETURN CLOB;
-- ***************************************************************************************************
-- Date and Author: 16. May 2011, MCD
-- return the original value for a checked value
--
-- Parameter:
-- sValue   the value to check
--
-- Return:
-- CLOB     the clean value
--
-- ***************************************************************************************************

function getXml( csSelectStatement varchar2 )
  return clob;
-- ***************************************************************************************************
-- Date and Author: 09.Oct.2018, vs
-- a wrapper for DBMS_XMLGEN.getxml( 'sqlstatement' ) --> generates canonical XML output (as /ROWSET/ROW)
-- The context is so set, that all are returned in the XML clob, even if the values of a column all are null.
-- this function is counterpart to getCanonicalDynSql 
--
-- Parameter:
-- csSelectStatement   The select statement string
--
-- Return:
-- CLOB     returned XML result of the select as CLOB
-- ***************************************************************************************************

function getCanonicalDynSql( cclCanonical clob )
  return clob;
-- ***************************************************************************************************
-- Date and Author: 09.Oct.2018, vs
-- generates code to query canonical XML (/ROWSET/ROW) to be used as subsequent dynamical SQL statement
-- this function is a counterpart of function getXml
--
-- Parameter:
-- cclCanonical   The canonical XML as CLOB
--
-- Return:
-- CLOB           returned XML statement, which can query the input as dynamical SQL
-- ***************************************************************************************************

function generateCanonicalQuery( csTable  varchar2,
                                 csColumn varchar2,
                                 csFilter varchar2 default '1 = 1' )
  return clob;
-- ***************************************************************************************************
-- Date and Author: 10.Oct.2018, vs
-- generates an SQL query for canonical XML (/ROWSET/ROW), which is stored in a CLOB-column of a table
-- this function is similar to getCanonicalDynSql
--
-- Parameter:
-- csTable     table to be queried
-- csColumn    column name in that table
-- csFilter    filter criterium for data selection
-- 
-- Return:
-- CLOB        SQL-code
--
-- sample call  -->   select generateCanonicalQuery( csTable  => 'ISR$LOG',
--                                                   csColumn => 'LOGCLOB',
--                                                   csFilter => 'entryid = 163483953') from dual;
-- ***************************************************************************************************

function deleteTmpDataForSession(cnSessionId in number) return STB$OERROR$RECORD;
--**********************************************************************************************************************
END ISR$XML;