CREATE OR REPLACE PACKAGE ISR$LDAP
as

-- ****************************************************************************************************
-- Description/Usage:
-- the package ISR$LDAP contains the ldap functionality
-- ****************************************************************************************************

FUNCTION getLDAPInformation(sHost IN VARCHAR2
                          , sPort IN VARCHAR2
                          , sSSLAuth IN VARCHAR2 DEFAULT 0
                          , sSSLWrl IN VARCHAR2 DEFAULT null
                          , sSSLWalletPasswd IN VARCHAR2 DEFAULT null
                          , sGlobUserName IN VARCHAR2
                          , sGlobPassword IN VARCHAR2
                          , sLDAPBase IN VARCHAR2 DEFAULT NULL
                          , sAttributes IN VARCHAR2 DEFAULT '*'
                          , sFilter IN VARCHAR2 DEFAULT 'objectclass=*'
                          , sPassword IN VARCHAR2 DEFAULT NULL) 
return CLOB;
-- ****************************************************************************************************
FUNCTION getLDAPStatus(sHost IN VARCHAR2
                     , sPort IN VARCHAR2
                     , sSSLAuth IN VARCHAR2 DEFAULT 0
                     , sSSLWrl IN VARCHAR2 DEFAULT null
                     , sSSLWalletPasswd IN VARCHAR2 DEFAULT null
                     , sGlobUserName IN VARCHAR2
                     , sGlobPassword IN VARCHAR2) 
return CLOB;
END ISR$LDAP;