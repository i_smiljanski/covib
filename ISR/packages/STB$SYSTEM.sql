CREATE OR REPLACE PACKAGE STB$SYSTEM
  IS

  oParameterBackup              STB$MENUENTRY$RECORD;
  oNodeBackup                   STB$TREENODE$RECORD;
  
  type tJobIdRecord is record( nOutputId  isr$action$output.outputid%type,
                               nActionId  isr$action$output.actionid%type,
                               sFunction  isr$report$output$type.actionfunction%type );
  
  -- cursor cGetJobIds is used here and in package isr$server$base                            
  cursor cGetJobIds( cstoken         in isr$action.token%type, 
                     csdocdefinition in ISR$ACTION$OUTPUT.DOC_DEFINITION%type)
    return tJobIdRecord;
    

-- ***************************************************************************************************
-- Description/Usage:
-- The package is dealing with the functionality of the iSR administration.
-- It holds all functionality to get, set or modify the system parameters. 
-- The package handles all command tokens that are set up on the node MAINADMIN.
-- ***************************************************************************************************

FUNCTION removeJob (nJobId IN STB$JOBSESSION.JOBID%TYPE) RETURN STB$OERROR$RECORD;
-- ***************************************************************************************************
-- Date and Autor: 30 Mar 2012, MCD
-- removes a job with the given jobid and oracle jobname
--
-- Parameters:
-- nJobId         the iStudyReporter jobid
-- sOracleJobid   the Oracle job name
--
-- Return
-- STB$OERROR$RECORD
--
-- ***************************************************************************************************

PROCEDURE destroyOldValues;
-- ***************************************************************************************************
-- Date and Autor: 30 Jan 2006  JB
-- destroys the old values of the node object when a new node is set in the Explorer
--
-- ***************************************************************************************************

FUNCTION checkDependenciesOnMask( oParameter IN STB$MENUENTRY$RECORD, olParameter IN OUT STB$PROPERTY$LIST, sModifiedFlag OUT VARCHAR2 ) RETURN STB$OERROR$RECORD;
-- ***************************************************************************************************
-- Date und Autor: 25. Apr 2008  MCD
-- calls the customer package to check if there are data dependencies on a 
-- dialog window 
--
-- Parameter :
-- oParameter         the menu entry
-- olParameter        the selected parameters
-- sModifiedFlag      checks if the property list changes
--
-- Return:
-- STB$OERROR$RECORD  
--
-- ***************************************************************************************************

--FUNCTION checkDependenciesOnMaskForClob( oParameter IN STB$MENUENTRY$RECORD, cParameter IN OUT CLOB, sModifiedFlag OUT VARCHAR2 ) RETURN STB$OERROR$RECORD;
-- ***************************************************************************************************
-- Date und Autor: 23. Feb 2011  IS
-- calls the customer package to check if there are data dependencies on a 
-- dialog window 
--
-- Parameter :
-- oParameter         the menu entry
-- olParameter        the selected parameters
-- sModifiedFlag      checks if the property list changes
--
-- Return:
-- STB$OERROR$RECORD  
--
--For each of the Token (Grid) is written its own type, 
--In the STB$SYSTEM all are commented on,  that relates to these Token (For the duration of the test)
-- ***************************************************************************************************

FUNCTION callFunction(oParameter in STB$MENUENTRY$RECORD, olNodeList OUT STB$TREENODELIST)  RETURN STB$OERROR$RECORD;
-- ***************************************************************************************************
-- Date und Autor: 02 Feb 2006  JB
-- is the interface to process a command token selected in the context menu.
-- The function checks if the command token can be executed and calls the backend functionality
-- to process the token. If further information is required to process the command token,
-- a node object  is returned and displayed as dialog window
--
-- Parameter:
-- oParameter            the command token selected in the context menu
-- olNodeList            a node list describing a pop mask where further information can be  entered asked
--                       to process the command token
--
-- Return:
-- STB$OERROR$RECORD    
--
-- ***************************************************************************************************

FUNCTION putParameters(oParameter in STB$MENUENTRY$RECORD, olParameter in STB$PROPERTY$LIST, clParameter in CLOB DEFAULT NULL) RETURN STB$OERROR$RECORD;
-- ***************************************************************************************************
-- Date und Autor: 02 Feb 2006  JB
-- is the counterpart function of callFunction , using the putParameter a
-- command token is procssed from a called dialog window.The entered information (e.g. esig , filter )
-- whes considered when the command token is processed
--
-- Parameter:
-- oParameter      the command token selected in the context menu
-- olParameter     object list holding all information on the fields of the pop-up mask (Object type has the
--                 attributes sParameter, sPromptDisplay,sValue,sDisplay,sEditable,sRequired,sDisplayed,
--                 hasLov,type,format)
--
-- Return:
-- STB$OERROR$RECORD    
--
-- ***************************************************************************************************

FUNCTION getLov(sEntity in VARCHAR2, sSuchwert in VARCHAR2, oSelectionList OUT ISR$TLRSELECTION$LIST) RETURN STB$OERROR$RECORD;
-- ***************************************************************************************************
-- Date und Autor: 02 Feb 2006  JB
-- returns the a list of values of an data entity
--
-- Parameter:
-- sEntity            the entity to which the list of values belongs to
-- sSuchwert          the search string to filter / limit the list to certain values
-- oSelectionList     the list of values  belong to the passed entity
--
-- Return:
-- STB$OERROR$RECORD    
--
-- ***************************************************************************************************

FUNCTION setCurrentNode(oSelectedNode in STB$TREENODE$RECORD) RETURN STB$OERROR$RECORD;
-- ***************************************************************************************************
-- Date und Autor: 02 Feb 2006  JB
-- sets the information on a tree node in the package STB$OBJECT 
--
-- Parameter:
-- oSelectedNode       the node object describing the selected iStudyReporter Explorer node
--
-- Return:
-- STB$OERROR$RECORD    
--
-- ***************************************************************************************************

FUNCTION setMenuAccess(sParameter in VARCHAR2, sMenuAllowed OUT VARCHAR2)  RETURN STB$OERROR$RECORD;
-- ***************************************************************************************************
-- Date and Autor:  02 Feb 2006  JB
-- checks if a command token is selectable in the context menu. If a command token is
-- selectable depends on the context of the tree node and the granted system privileges 
-- of the authorization groups the user belongs to
--
-- Parameter:
-- sParameter            the command token of the context menu
-- sMenuAllowed          flag to indicate if the context command token is selectable in the context menu
--
-- Return:
-- STB$OERROR$RECORD    
--
-- ***************************************************************************************************

FUNCTION getAuditMask(olNodeList OUT STB$TREENODELIST)  RETURN STB$OERROR$RECORD;
-- ***************************************************************************************************
-- Date und Autor: 02 Feb 2006  JB
-- retrieves a window, holding the audit dialog, in this window the audit reason 
-- of a prompted audit can be entered 
--
-- Parameter:
-- olNodeList      node object holding all fields of the audit mask (Object type has the
--                 attributes sParameter, sPromptDisplay,sValue,sDisplay,sEditable,sRequired,sDisplayed,
--                 hasLov,type,format)
--
-- Return:
-- STB$OERROR$RECORD    
--
-- ***************************************************************************************************

FUNCTION putAuditMask(olParameter in STB$PROPERTY$LIST)  RETURN STB$OERROR$RECORD;
-- ***************************************************************************************************
-- Date und Autor: 02 Feb 2006  JB
-- writes the audit reason that was entered in the audit window into the 
-- table STB$AUDITTRAIL
--
-- Parameter:
-- olParameter      node object holding all fields of the audit mask (Object type has the
--                 attributes sParameter, sPromptDisplay,sValue,sDisplay,sEditable,sRequired,sDisplayed,
--                 hasLov,type,format)
--
-- Return:
-- STB$OERROR$RECORD    
--
-- ***************************************************************************************************

FUNCTION getNodeChilds(oSelectedNode in STB$TREENODE$RECORD, olNodeList OUT STB$TREENODELIST)  RETURN STB$OERROR$RECORD;
-- ***************************************************************************************************
-- Date und Autor: 02 Feb 2006  JB
-- retrievs the subnodes of the selected tree node in the iStudyReporter Explorer
--
-- Parameter:
-- oSelectedNode   the node for which the childs should be retrieved
-- olChildNodes    the list of childnodes
--
-- Return:
-- STB$OERROR$RECORD    
--
-- ***************************************************************************************************

FUNCTION getNodeList(oSelectedNode in STB$TREENODE$RECORD, olNodeList OUT STB$TREENODELIST)  RETURN STB$OERROR$RECORD;
-- ***************************************************************************************************
-- Date und Autor: 02 Feb 2006  JB
-- retrievs all nodes that should be displayed as properties of the selected treenode 
-- in the right split screen of the iStudyReporter Explorer
--
-- Parameter:
-- oSelectedNode  the node for which the childs should be retrieved
-- olNodeList     the list of nodes to be displayed
--
-- Return:
-- STB$OERROR$RECORD    
--
-- ***************************************************************************************************

FUNCTION getContextMenu( nMenu in NUMBER,  olMenuEntryList OUT STB$MENUENTRY$LIST ) RETURN STB$OERROR$RECORD;
-- ***************************************************************************************************
-- Date und Autor: 02 Feb 2006  JB
-- returns the context menu of a selected tree node in the iStudyReporter Explorer ,
-- if a menu entry is passed into the function, the returned information will be the sub menu
-- belonging to the passed menu entry
--
-- Parameter:
-- nMenu              optional identifer of the "parent" menu entry
-- olMenuEntryList     the context menu of the current node or the submenu for the provided menu entry
--
-- Return:
-- STB$OERROR$RECORD    
--
-- ***************************************************************************************************

FUNCTION unLockReports(sWhereClause in VARCHAR2) RETURN STB$OERROR$RECORD;
-- ***************************************************************************************************
-- Date und Autor:  02 Feb 2006  JB
-- unlocks all reports which are effected by the passed where-condition
-- the where condition will be written into a dynamic sql statement
--
-- Parameter:
-- sWhereClause    the where clause to identify which reports will be unlocked
--
-- Return:
-- STB$OERROR$RECORD    
--
-- ***************************************************************************************************

FUNCTION lockReports(sWhereClause in VARCHAR2) RETURN STB$OERROR$RECORD;
-- ***************************************************************************************************
-- Date und Autor: 02 Feb 2006  JB
-- locks all reports which are effected by the passed where-condition
-- the where condition will be written into a dynamic sql statement
--
-- Parameter:
-- sWhereClause    the where clause to identify which reports will be locked
--
-- Return:
-- STB$OERROR$RECORD    
--
-- ***************************************************************************************************

FUNCTION getAuditType RETURN STB$OERROR$RECORD;
-- ***************************************************************************************************

FUNCTION getDefaultTablespace RETURN varchar2;
-- ***************************************************************************************************
-- Date und Autor: 12 Nov 2015  vs
-- fetches the user's default tablespace one time only from USER_USERS view
-- and returns that (first time fetched) value whenever called.
--
-- Parameter:
--   no parameters
--
-- Return:
--   varchar2   
--
-- Sample calls:
--   select stb$system.getDefaultTablespace from dual;
--
-- ***************************************************************************************************

FUNCTION getNologTablespace( csForceRead varchar2  default 'F' )
  RETURN varchar2;
-- ***************************************************************************************************
-- Date und Autor: 12 Nov 2015  vs
-- Fetches the user's nolog-tablespace one time only from stb$systemparameter and returns 
-- that (first time fetched) value whenever called. After a change of the systemparameter 
-- ISR_TABLE_NOLOG_TABLESPACE, this function must be called with the parameter 
-- csForceRead = 'T' instantly.
--
-- Parameter:
--   csForceRead  The value 'T' forces a data retrieval of the systemparameter ISR_TABLE_NOLOG_TABLESPACE (anew).
--
-- Return:
--   varchar2   
--
-- Sample calls:
--   select stb$system.getNologTablespace from dual;
--   select stb$system.getNologTablespace('T') from dual;
--
-- ***************************************************************************************************

function getJobIcon(cnJobId in number, cnProgress in number, csState in varchar2) return varchar2;
-- ***************************************************************************************************
END Stb$system;