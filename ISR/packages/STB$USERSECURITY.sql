CREATE OR REPLACE PACKAGE Stb$usersecurity
  IS

-- ***************************************************************************************************
-- Description/Usage:
-- The package is dealing with the functionality of the user administration (users, data and function groups).
-- It holds all functionality to get, set or modify the attributes parameters.
-- ***************************************************************************************************

FUNCTION checkDependenciesOnMask( oParameter IN STB$MENUENTRY$RECORD, olParameter IN OUT STB$PROPERTY$LIST, sModifiedFlag OUT VARCHAR2 ) RETURN STB$OERROR$RECORD;
-- ***************************************************************************************************
-- Date und Autor: 25. Apr 2008  MCD
-- function calls the customer package to check if there are data dependencies on a
-- dialog window
--
-- Parameter :
-- oParameter         the menu entry
-- olParameter        the selected parameters
-- sModifiedFlag      checks if the property list changes
--
-- Return:
-- STB$OERROR$RECORD
--
-- ***************************************************************************************************

PROCEDURE destroyOldValues;
-- ***************************************************************************************************
-- Date and Author: 30 Jan 2006  JB
-- procedure destroys the old values of the node object when a new node is set in the Explorer
--
-- ***************************************************************************************************

FUNCTION getLov(sEntity in VARCHAR2, sSuchwert in VARCHAR2, oSelectionList OUT ISR$TLRSELECTION$LIST) RETURN STB$OERROR$RECORD;
-- ***************************************************************************************************
-- Date und Autor: 30 Jan 2006  JB
-- function returns the a list of values of an data entity
--
-- Parameter:
-- sEntity            the entity to which the list of values belongs to
-- sSuchwert          the search string to filter / limit the list to certain values
-- oSelectionList     the list of values  belong to the passed entity
--
-- Return:
-- STB$OERROR$RECORD
--
-- ***************************************************************************************************

FUNCTION setCurrentNode(oSelectedNode in STB$TREENODE$RECORD) RETURN STB$OERROR$RECORD;
-- ***************************************************************************************************
-- Date und Autor: 30 Jan 2006  JB
-- function sets the information on a tree node in the package STB$OBJECT
--
-- Parameter:
-- oSelectedNode       the node object describing the selected iStudyReporter Explorer node
--
-- Return:
-- STB$OERROR$RECORD
--
-- ***************************************************************************************************

FUNCTION callFunction(oParameter in STB$MENUENTRY$RECORD, olNodeList OUT STB$TREENODELIST) RETURN STB$OERROR$RECORD;
-- ***************************************************************************************************
-- Date und Autor: 02 Feb 2006  JB
-- function is the interface to process a command token selected in the context menu.
-- The function checks if the command token can be executed and calls the backend functionality
-- to process the token. If further information is required to process the command token,
-- a node object  is returned and displayed as dialog window
--
-- Parameter:
-- oParameter            the command token selected in the context menu
-- olNodeList            a node list describing a pop mask where further information can be  entered asked
--                       to process the command token
--
-- Return:
-- STB$OERROR$RECORD
--
-- ***************************************************************************************************

FUNCTION putParameters(oParameter in STB$MENUENTRY$RECORD, olParameter in STB$PROPERTY$LIST) RETURN STB$OERROR$RECORD;
-- ***************************************************************************************************
-- Date und Autor: 02 Feb 2006  JB
-- function is the counterpart function of callFunction , using the putParameter a
-- command token is procssed from a called dialog window.The entered information (e.g. esig , filter )
-- whes considered when the command token is processed
--
-- Parameter:
-- oParameter      the command token selected in the context menu
-- olParameter     object list holding all information on the fields of the dialog (Object type has the
--                 attributes sParameter, sPromptDisplay,sValue,sDisplay,sEditable,sRequired,sDisplayed,
--                 hasLov,type,format)
--
-- Return:
-- STB$OERROR$RECORD
--
-- ***************************************************************************************************

FUNCTION getNodeChilds(oSelectedNode in STB$TREENODE$RECORD, olChildNodes OUT STB$TREENODELIST) RETURN STB$OERROR$RECORD;
-- ***************************************************************************************************
-- Date und Autor: 02 Feb 2006  JB
-- function retrievs the subnodes of the selected tree node in the iStudyReporter Explorer
--
-- Parameter:
-- oSelectedNode   the node for which the childs should be retrieved
-- olChildNodes    the list of childnodes
--
-- Return:
-- STB$OERROR$RECORD
--
-- ***************************************************************************************************

FUNCTION getNodeList(oSelectedNode in STB$TREENODE$RECORD, olNodeList OUT STB$TREENODELIST) RETURN STB$OERROR$RECORD;
-- ***************************************************************************************************
-- Date und Autor: 02 Feb 2006  JB
-- function retrievs all nodes that should be displayed as properties of the selected treenode
-- in the right split screen of the iStudyReporter Explorer
--
-- Parameter:
-- oSelectedNode  the node for which the childs should be retrieved
-- olNodeList     the list of nodes to be displayed
--
-- Return:
-- STB$OERROR$RECORD
--
-- ***************************************************************************************************

FUNCTION setMenuAccess(sParameter in VARCHAR2, sMenuAllowed OUT VARCHAR2) RETURN STB$OERROR$RECORD;
-- ***************************************************************************************************
-- Date and Author: 02 Feb 2006  JB
-- function checks if a command token is selectable in the context menu. If a command token is
-- selectable depends on the context of the tree node and the granted system privileges
-- of the authorization groups the user belongs to
--
-- Parameter:
-- sParameter            the command token of the context menu
-- sMenuAllowed          flag to indicate if the context command token is selectable in the context menu
--
-- Return:
-- STB$OERROR$RECORD
--
-- ***************************************************************************************************

FUNCTION getAuditMask(olNodeList OUT STB$TREENODELIST) RETURN STB$OERROR$RECORD;
-- ***************************************************************************************************
-- Date und Autor: 02 Feb 2006  JB
-- function retrieves a window, holding the audit dialog, in this window the audit reason
-- of a prompted audit can be entered
--
-- Parameter:
-- olNodeList      node object holding all fields of the audit mask (Object type has the
--                 attributes sParameter, sPromptDisplay,sValue,sDisplay,sEditable,sRequired,sDisplayed,
--                 hasLov,type,format)
--
-- Return:
-- STB$OERROR$RECORD
--
-- ***************************************************************************************************

FUNCTION putAuditMask(olParameter in STB$PROPERTY$LIST) RETURN STB$OERROR$RECORD;
-- ***************************************************************************************************
-- Date und Autor: 02 Feb 2006  JB
-- function writes the audit reason that was entered in the audit window into the
-- table STB$AUDITTRAIL
--
-- Parameter:
-- olParameter     node object holding all fields of the audit mask (Object type has the
--                 attributes sParameter, sPromptDisplay,sValue,sDisplay,sEditable,sRequired,sDisplayed,
--                 hasLov,type,format)
--
-- Return:
-- STB$OERROR$RECORD
--
-- ***************************************************************************************************

FUNCTION getContextMenu( nMenu in NUMBER, olMenuEntryList OUT STB$MENUENTRY$LIST ) RETURN STB$OERROR$RECORD;
-- ***************************************************************************************************
-- Date und Autor: 02 Feb 2006  JB
-- function returns the context menu of a selected tree node in the iStudyReporter Explorer ,
-- if a menu entry is passed into the function, the returned information will be the sub menu
-- belonging to the passed menu entry
--
-- Parameter:
-- nMenu               optional identifer of the "parent" menu entry
-- olMenuEntryList     the context menu of the current node or the submenu for the provided menu entry
--
-- Return:
-- STB$OERROR$RECORD
--
-- ***************************************************************************************************

FUNCTION createSyn(sRoleName in VARCHAR2, sNewUser in VARCHAR2, sISROwner in VARCHAR2)  RETURN VARCHAR2;
-- ***************************************************************************************************
-- Date und Autor: 20 Jun 2006 jb
-- function creates the required synonyms for a new user of iStudyReporter
--
-- Parameter:
-- sRoleName          the role of the iStudyReporter
-- sNewUser           the new user of iStudyReporter
-- sISROwner         the owner of the iStudyReporter
--
-- Return:
-- STB$OERROR$RECORD
--
-- ***************************************************************************************************

FUNCTION checkOutDirect(cnUserNo IN NUMBER)  RETURN VARCHAR2;
-- ***************************************************************************************************
-- Date und Autor: 17. May 2011, MCD
-- checks if the user has activated direct checkout
--
-- Parameter:
-- cnUserNo   the user no
--
-- Return:
-- 'T' if activated else 'F'
--
-- ***************************************************************************************************

FUNCTION getUserParameter (nUserNo IN NUMBER, sParameterName IN VARCHAR2) RETURN VARCHAR2;
-- ***************************************************************************************************
-- Date and Autor: 30 Mar 2012, MCD
-- gets the parameter value of the user parameter
--
-- Parameters:
-- nUserNo        the user ID
-- sParameterName the parameter
--
-- Return
-- user parameter value
--
-- ***************************************************************************************************
END Stb$usersecurity;