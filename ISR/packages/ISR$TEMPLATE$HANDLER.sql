CREATE OR REPLACE PACKAGE ISR$TEMPLATE$HANDLER
as

-- ****************************************************************************************************
-- Description/Usage:
-- the package ISR$TEMPLATE$HANDLER contains the template functionality
-- ****************************************************************************************************

FUNCTION updateTemplate(cnMasterTemplate IN NUMBER, csComment IN VARCHAR2, csIcon IN VARCHAR2, csFilename IN VARCHAR2, blBinaryFile IN BLOB, nNewFileId OUT NUMBER, csForceCopy IN VARCHAR2 DEFAULT 'F') RETURN STB$OERROR$RECORD;
-- ***************************************************************************************************
-- Date and Autor:  04. Mar 2015, MCD
-- function updates a master template 
--
-- Parameter:
-- cnMasterTemplate    the old master template number to copy the values of 
-- csComment           an exchange comment
-- csIcon              an icon
-- csFilename          the new filename
-- blBinaryFile        the new file
-- csForceCopy         forces the copy of the file even if the file is equal
--
-- Return:
-- STB$OERROR$RECORD    
--
-- ***************************************************************************************************

function updateTemplate(cnJobid IN NUMBER, cxXml IN OUT XMLTYPE) return STB$OERROR$RECORD;
-- ***************************************************************************************************
-- Date and Autor:  16 Feb 2006 JB
-- function updates a master template of an output id in the table ISR$FILE
--
-- Parameter:
-- cnJobid     the identifier of the iStudyReporter job
-- cnDomId     the domid of the xml configuration file
--
-- Return:
-- STB$OERROR$RECORD    
--
-- ***************************************************************************************************

function copyTemplateFileToJobTrail(cnJobid IN NUMBER, cxXml IN OUT XMLTYPE) return STB$OERROR$RECORD;
-- ***************************************************************************************************
-- Date and Autor:  16 Feb 2006 JB
-- function copies a master template to the jobtrail
--
-- Parameter:
-- cnJobid     the identifier of the iStudyReporter job
-- cnDomId     the domid of the xml configuration file
--
-- Return:
-- STB$OERROR$RECORD   
--
-- ***************************************************************************************************

FUNCTION copyFile(nOldFileId IN NUMBER, nFileId OUT NUMBER, sNewFileName IN VARCHAR2 DEFAULT null, sComment IN VARCHAR2 DEFAULT NULL, sIcon IN VARCHAR2 DEFAULT NULL, csForceTemplateCreation IN VARCHAR2 DEFAULT 'F') RETURN STB$OERROR$RECORD;
-- ***************************************************************************************************
-- Date and Autor:  16. May 2011, MCD
-- function copies a template file and return the new fileid
--
-- Parameter:
-- nOldFileId   the old template id
-- nFileId      the new template id
-- sNewFileName the new filename
-- sComment     a comment
--
-- Return:
-- STB$OERROR$RECORD
--
-- **************************************************************************************************

function copyTemplate(cnJobid IN NUMBER, cxXml IN OUT XMLTYPE) return STB$OERROR$RECORD;
-- ***************************************************************************************************
-- Date and Autor:  16 Feb 2006 JB
-- function clones an existing output format and uploads a new master template into the database table
-- ISR$FILE
--
-- Parameter:
-- cnJobid     the identifier of the iStudyReporter job
-- cnDomId     the domid of the xml configuration file
--
-- Return:
-- STB$OERROR$RECORD    
--
-- ***************************************************************************************************

FUNCTION compareTemplates(cnFileid1 IN NUMBER, cnFileid2 IN NUMBER, csOnlyDiffs IN VARCHAR2 DEFAULT 'F') RETURN XMLTYPE;

FUNCTION compareTemplates(cnJobid IN NUMBER, cxXml IN OUT XMLTYPE) RETURN STB$OERROR$RECORD;
-- ***************************************************************************************************
-- Date and Autor:  04. Mar 2015, MCD
-- function compares two templates and creates an xml file with the differences
--
-- Parameter:
-- cnJobid     the identifier of the iStudyReporter job
-- cnDomId     the domid of the xml configuration file
--
-- Return:
-- STB$OERROR$RECORD    
--
-- ***************************************************************************************************

PROCEDURE showOnlyDiffs(sOldValue IN OUT VARCHAR2, sNewValue IN OUT VARCHAR2);

FUNCTION getTemplateDescription(cnFileId IN VARCHAR2) RETURN VARCHAR2;


FUNCTION setMenuAccess(sParameter in VARCHAR2, sMenuAllowed OUT VARCHAR2)  RETURN STB$OERROR$RECORD;
-- ***************************************************************************************************
-- Date and Autor:  16 Nov 2016 IS
-- checks if a command token is selectable in the context menu, if a command token can be
-- selected depends on the context of the node and the activiated system privileges of the authorization
-- groups the user belongs to
--
-- Parameter:
-- sParameter            the command token of the context menu
-- sMenuAllowed          flag to indicate if the context command token is selectable in the context menu
--
-- Return:
-- STB$OERROR$RECORD    
--
-- ***************************************************************************************************

FUNCTION callFunction(oParameter in STB$MENUENTRY$RECORD,  olNodeList OUT STB$TREENODELIST)    RETURN STB$OERROR$RECORD;
-- ***************************************************************************************************
-- Date und Autor: 16 Nov 2016 IS
-- is the interface to process a command token selected in the context menu.
-- The function checks if the command token can be executed and calls the backend functionality
-- to process the token. If further information is required to process the command token,
-- a node object  is returned and displayed as dialog window
--
-- Parameters:
-- oParameter            the command token selected in the context menu
-- olNodeList            the dialog window when further information is required 
--
-- Return:
-- STB$OERROR$RECORD    
--
-- ***************************************************************************************************

FUNCTION putParameters(oParameter in STB$MENUENTRY$RECORD, olParameter in STB$PROPERTY$LIST)  RETURN STB$OERROR$RECORD;
-- ***************************************************************************************************
-- Date und Autor: 16 Nov 2016 IS
-- is the counterpart function of callFunction , using the putParameter a
-- command token is procssed from a called dialog window.The entered information (e.g. esig , filter )
-- whes considered when the command token is processed
--
-- Parameters:
-- oParameter      the command token selected in the context menu
-- olParameter     object list holding all information of the attributes of the dialog window
--
-- Return:
-- STB$OERROR$RECORD    
--
-- ***************************************************************************************************
function exportTemplate(cnJobid IN NUMBER, cxXml IN OUT XMLTYPE) return STB$OERROR$RECORD;
-- ***************************************************************************************************
-- Date and Autor:  16 Nov 2016 IS
-- function to export a template including infrastructure and downloads it into the client
--
-- Parameter:
-- cnJobid     the identifier of the iStudyReporter job
-- cxXml       the configuration file
--
-- Return:
-- STB$OERROR$RECORD    
--
-- ***************************************************************************************************

function importTemplate(cnJobid IN NUMBER, cxXml IN OUT XMLTYPE) return STB$OERROR$RECORD;
-- ***************************************************************************************************
-- Date and Autor:  16 Nov 2016 IS
-- function to import a template including infrastructure 
--
-- Parameter:
-- cnJobid     the identifier of the iStudyReporter job
-- cxXml       the configuration file
--
-- Return:
-- STB$OERROR$RECORD    
--
-- ***************************************************************************************************

PROCEDURE setAllowedTockenParameters (oParamListRec in out ISR$PARAMLIST$REC);
procedure setMenuAllowed (sParameter   in  varchar2,sMenuAllowed  in out varchar2 );

END ISR$TEMPLATE$HANDLER;