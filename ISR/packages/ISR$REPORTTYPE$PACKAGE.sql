CREATE OR REPLACE PACKAGE ISR$REPORTTYPE$PACKAGE
  as
-- ***************************************************************************************************
-- Description/Usage:
-- the package ISR$REPORTTYPE$PACKAGE is the interface of all report types of the iStudyReporter.
-- the package is called from STB$GAL the Gui interface and holds all functions that deal with a report
-- type. The providing of the context menu entries as well as the execution of context menu entries.
-- the report wizard is invoked by this package and the data of the  report wizard is transported
-- though the package into Frontend.
-- ***************************************************************************************************
PROCEDURE sessionUpdate;
--******************************************************************************
FUNCTION createreportsum (cnrepid IN NUMBER) RETURN NUMBER;
-- ***************************************************************************************************
-- Date and Autor: 01 Dec 2011 MCD
-- calculates the checksum for a report
--
-- Parameters:
-- cnRepid    the report id
--
-- Return
-- checksum
--
-- ***************************************************************************************************

PROCEDURE destroyOldValues;
-- ***************************************************************************************************
-- Date and Autor: 19 Dez 2005 JB
-- destroys the old values of the node object if a new node is set in the Explorer tree
--
-- ***************************************************************************************************

FUNCTION GetCurrentFormInfo (oFormInfoRecord OUT ISR$FORMINFO$RECORD, oEntityList OUT STB$ENTITY$LIST, cnMaskNo IN NUMBER, csFrontend IN VARCHAR2 DEFAULT 'F') RETURN  STB$OERROR$RECORD;
-- ***************************************************************************************************
-- Date und Autor: 19 Dez 2005 JB
-- retrieves the defintion of a wizard selection scrren and the entities of the selection screen
-- if the screen is of the type "Master-Detail" or "Grid".
--
-- Parameters:
-- lsFormInfo           object that defines the selection screen
-- oEntityList          entity list if there are more than one entity on the screen
--                      when the screen has the type "Grid" or "Master-Detail"
-- cnMaskNo             the number of the selection screen in the report wizard
-- csFrontend           flag to determine if the function is called from the Frontend or in the
--                      background from the report type package
--
-- Return:
-- STB$OERROR$RECORD
--
-- ***************************************************************************************************

FUNCTION GetSList (sEntity in  Stb$typedef.tLine, sMasterKey in VARCHAR2, sNavigateMode in VARCHAR2, oSelectionList OUT ISR$TLRSELECTION$LIST, cnNextNavigationMask IN NUMBER, oFormInfoRecord IN OUT ISR$FORMINFO$RECORD, oEntityList IN OUT STB$ENTITY$LIST, csFrontend IN VARCHAR2 DEFAULT 'F') RETURN STB$OERROR$RECORD;
-- ***************************************************************************************************
-- Date and Autor:   19 Dez 2005 JB
-- All available values of the report wizard selection screen are retrieved into the oSelectionRecord.
-- Previous selected values have the attribute  selected ='T'
--
-- Parameters:
-- sEntity             the current entity in the selection screen
-- sMasterKey          the master key if the selection is a grid select
-- sNavigateMode       flag to indicate if the scrren is called in the navigation mode
-- nextNavigationMask  the number of the selection screen
-- oSelectionList      object list with the attributes "sValue","sMasterKey", "sDisplay", "sInfo",
--                    "sSelected" and "order number"
-- oFormInfoRecord     object that defines the selection screen
-- oEntityList         a list of all entities of the seection scren if more than one exist
-- csFrontend           flag to determine if the function is called from the Frontend or in the
--                      background from the reprt type package
--
-- Return:
-- STB$OERROR$RECORD
--
-- ***************************************************************************************************

FUNCTION PutSList (sEntity   in  Stb$typedef.tLine,oSelection in ISR$TLRSELECTION$LIST) RETURN STB$OERROR$RECORD;
-- ***************************************************************************************************
-- Date and Autor:  19 Dez 2005 JB
-- writes the selected values of a selection screen of the report wizard  into the database
-- table ISR$CRIT. The changes are commited when the report is saved at the end of the wizard.
--
-- Parameters:
-- sEntity          the entity in the selection screen
-- oSelection        object list of the selected values having the attributes
--                  "sMasterKey", "sValue", "sDisplay", "sInfo", "sSelected" and "order number"
--
-- Return:
-- STB$OERROR$RECORD
--
-- ***************************************************************************************************

FUNCTION createAudit(sAuditFlag OUT VARCHAR2,xXml OUT XMLTYPE,nLastFinalRepId in NUMBER)   RETURN STB$OERROR$RECORD;
-- ***************************************************************************************************
-- Date and Autor:  19 Dez 2005 JB
-- calls in a loop the createAudit -function to check if there have been changes
-- on the criteria of a report.
--  if there have been changes made on the selection criteria or output options the
-- sAuditFlag  is set to "T ".
--
-- Parameter:
-- sAuditFlag         flag to indicate if there have been changes on the selection criteria
-- nDomId             the xml dom
-- nLastFinalRepId    the report id of the previous report version
--
-- Return:
-- STB$OERROR$RECORD
--
-- ***************************************************************************************************

FUNCTION initForms(oForm OUT ISR$FORMINFO$LIST)  RETURN STB$OERROR$RECORD;
-- ***************************************************************************************************
-- Date and Autor:  19 Dez 2005 JB
-- gets an object list with all seelction scrrens
-- is required to initialize the packages during the document creation process
--
-- Parameter:
-- oForm           object list holding the definitions of all seelction screens
--
-- Return:
-- STB$OERROR$RECORD
--
-- ***************************************************************************************************

FUNCTION createNewReport(nArt IN NUMBER DEFAULT NULL) RETURN STB$OERROR$RECORD;
-- ***************************************************************************************************
-- Date and Autor:  19 Dez 2005 JB
-- creates a new report, depending on the flag "cnArt" it will be distinguished how the new
-- report is created .It is possible to create a new report from scratch or to clone an existing report
--
-- Parameter:
--
-- cnArt                  0 for modification of the selection criteria or new report
--                        1 for variation
--                        2 for new version from a final report
--
-- Return:
-- STB$OERROR$RECORD
--
-- ***************************************************************************************************

FUNCTION getOutputOption(olParameter OUT STB$PROPERTY$LIST)  RETURN STB$OERROR$RECORD;
-- ***************************************************************************************************
-- Date und Autor: 19 Dez 2005 JB
-- retrieves the output parameter of a report type. The output options are the last selection
-- screen of the report wizard.
--
-- Parameters:
-- olParameter         the object list holding the output options
--
--
-- Return:
-- STB$OERROR$RECORD
--
-- ***************************************************************************************************
FUNCTION getNumberOfForms(csVisibleFlag IN VARCHAR2 DEFAULT null) RETURN NUMBER;
--******************************************************************************
FUNCTION putOutputOption(olParameter in STB$PROPERTY$LIST)  RETURN STB$OERROR$RECORD;
-- ***************************************************************************************************
-- Date und Autor: 19 Dez 2005 JB
-- sets the new output options of the report in the table STB$REPORTPARAMETER
--
-- Parameters:
-- olParameter        the object list holding the user defined output options
--
-- Return:
-- STB$OERROR$RECORD
--
-- ***************************************************************************************************

function setMenuAccessForModifyReport(sparameter   in varchar2, sMenuAllowed in out varchar2 )
  return stb$oerror$record;
--******************************************************************************
FUNCTION setMenuAccess(sParameter in VARCHAR2, sMenuAllowed OUT VARCHAR2)  RETURN STB$OERROR$RECORD;
-- ***************************************************************************************************
-- Date and Autor:  19 Dez 2005 JB
-- checks if a command token is selectable in the context menu, if a command token can be
-- selected depends on the context of the node and the activiated system privileges of the authorization
-- groups the user belongs to
--
-- Parameter:
-- sParameter            the command token of the context menu
-- sMenuAllowed          flag to indicate if the context command token is selectable in the context menu
--
-- Return:
-- STB$OERROR$RECORD
--
-- ***************************************************************************************************

FUNCTION setInspectionStatus RETURN STB$OERROR$RECORD ;
-- ***************************************************************************************************
-- Date and Autor:  02 Feb 2006 JB
-- sets a draft report version into the inspection status, depending on the report type
-- the data will be validated before, a document will be generated automatically .
-- The LIMS data blocks will be updated the last time before the report is finalized
--
-- Return:
-- STB$OERROR$RECORD
--
-- ***************************************************************************************************

FUNCTION deactivateInspectionStatus RETURN STB$OERROR$RECORD ;
-- ***************************************************************************************************
-- Date and Autor:  02 Feb 2006 JB
-- deactivates the  inspection status of the report and reset the report to draft
--
-- Return:
-- STB$OERROR$RECORD
--
-- ***************************************************************************************************

FUNCTION setFinalStatus RETURN STB$OERROR$RECORD ;
-- ***************************************************************************************************
-- Date and Autor:  02 Feb 2006 JB
-- finalizes a report, a document will be generated
-- automatically .The document will be read-only and can`t be altered anymore . The document is equal to
-- the inspection document apart from the document properties
--
-- Return:
-- STB$OERROR$RECORD
--
-- ***************************************************************************************************

FUNCTION getSaveDialog(olSaveList OUT STB$TREENODELIST) RETURN STB$OERROR$RECORD;
-- ***************************************************************************************************
-- Date and Autor:  02 Feb 2006 JB
-- retrieves the attributes of the save-as-dialog
-- The number of attributes in the dialog  and which values can be modidified
-- vary between the report types
--
-- Parameters:
-- olSaveList        object holding the default attributes of the save-as-dialog
--
-- Return:
-- STB$OERROR$RECORD    error object with the fields (sErrorCode, sErrorMessage, sSeverityCode)
--
-- Error behaviour/ particularities:
-- none
-- ***************************************************************************************************

FUNCTION createSrAudit(sSrNumber in VARCHAR2, sReason IN VARCHAR2 DEFAULT NULL) RETURN STB$OERROR$RECORD;
-- ***************************************************************************************************
-- Date and Autor:  02 Feb 2006 JB
-- writes the  project-title report history
--
-- Parameter:
-- sSrNumber  the project title
--
-- Return:
-- STB$OERROR$RECORD
--
-- ***************************************************************************************************

FUNCTION putSaveDialog(olSaveList in STB$PROPERTY$LIST) RETURN STB$OERROR$RECORD;
-- ***************************************************************************************************
-- Date and Autor:   02 Feb 2006 JB
-- writes the save-as-dialog in to the database .
-- The attributes are part of the report definiton in the table STB$REPROT
--
-- Parameters:
-- olSaveList        object holding the defined attributes of the save-as-dialog
--
-- Return:
-- STB$OERROR$RECORD
--
-- ***************************************************************************************************

FUNCTION putSaveDialog1(olSaveList in STB$PROPERTY$LIST, scurrenttoken IN VARCHAR2) RETURN STB$OERROR$RECORD;
-- ***************************************************************************************************
-- Date and Autor:   02 Feb 2006 JB
-- writes the save-as-dialog in to the database .
-- The attributes are part of the report definiton in the table STB$REPROT
--
-- Parameters:
-- olSaveList        object holding the defined attributes of the save-as-dialog
--
-- Return:
-- STB$OERROR$RECORD
--
-- ***************************************************************************************************

FUNCTION putSaveDialog2(olSaveList in STB$PROPERTY$LIST, scurrenttoken IN VARCHAR2) RETURN STB$OERROR$RECORD;
-- ***************************************************************************************************
-- Date and Autor:   02 Feb 2006 JB
-- writes the save-as-dialog in to the database .
-- The attributes are part of the report definiton in the table STB$REPROT
--
-- Parameters:
-- olSaveList        object holding the defined attributes of the save-as-dialog
--
-- Return:
-- STB$OERROR$RECORD
--
-- ***************************************************************************************************

FUNCTION putSaveDialogUnitTC(olSaveList in STB$PROPERTY$LIST, scurrenttoken IN VARCHAR2) RETURN STB$OERROR$RECORD;
-- ***************************************************************************************************
-- Date and Autor:   02 Feb 2006 JB
-- writes the save-as-dialog in to the database .
-- The attributes are part of the report definiton in the table STB$REPROT
--
-- Parameters:
-- olSaveList        object holding the defined attributes of the save-as-dialog
--
-- Return:
-- STB$OERROR$RECORD
--
-- ***************************************************************************************************

FUNCTION GetGridSelection (sEntity in  Stb$typedef.tLine, sNavigateMode in VARCHAR2, oSelection OUT ISR$TLRSELECTION$LIST) RETURN STB$OERROR$RECORD;
-- ***************************************************************************************************
-- Date and Autor:  02 Feb 2006 JB
-- initalizes the selected values in the grid selection screen
--
-- Parameter:
-- sEntity          the current entity of the screen
-- sNavigateMode    flag to indicate if the application is in the navigation mode
-- oSelection       the values of the grid entity
--
-- Return:
-- STB$OERROR$RECORD    error object with the fields (sErrorCode, sErrorMessage, sSeverityCode)
--
-- Error behaviour/ particularities:
-- none
-- ***************************************************************************************************

FUNCTION GetGridDependency(csEntity in VARCHAR2 , oEntityList OUT STB$ENTITY$LIST,csKey in VARCHAR2) RETURN STB$OERROR$RECORD;
-- ***************************************************************************************************
-- Date and Autor:  02 Feb 2006 JB
-- retrieves the depending entities of an entity and deletes if necessary the
-- depdending values if the maser value changes
--
-- Parameters:
-- csEntity           the current entity
-- lsEntity           object list with all  entites of the screen with the flag if
--                    the values have to be deleted when the key value changes
-- csKey              the key value of the entity
--
--
-- Return:
-- STB$OERROR$RECORD
--
-- ***************************************************************************************************

FUNCTION validateInspectionData(sValidateFlag OUT VARCHAR2) RETURN STB$OERROR$RECORD;
-- ***************************************************************************************************
-- Date and Autor:  02 Feb 2006 JB
-- is called to validate the selection criteria that should be set in the inspection state,
-- the function calls the customer package to perform a customer and LIMS specific  validation of the data
--
-- Parameter:
-- sFlag           flag to indicate if the report can be set into the inspection state
--
-- Return:
-- STB$OERROR$RECORD
--
-- ***************************************************************************************************

FUNCTION checkManipulation(sManipulation OUT VARCHAR2)  RETURN STB$OERROR$RECORD;
-- ***************************************************************************************************
-- Date and Autor:  02 Feb 2006 JB
-- checks if there have been manipulations on the report data, the checksum is recalculated
--
-- Parameter:
-- sManipulation        flag to indicate if there have been manipulations onthe data
--
-- Return:
-- STB$OERROR$RECORD
--
-- Error behaviour/ particularities:
-- If there are manipuations on trhe data ,the checksum isn't correct there is no functionality
-- in the context menu of the report selectable
-- ***************************************************************************************************

FUNCTION initList (sEntity in  VARCHAR2, lSList OUT ISR$TLRSELECTION$LIST) RETURN STB$OERROR$RECORD;
-- ***************************************************************************************************
-- Date and Autor:  02 Feb 2006 JB
-- initalizes the master values for a master-detail selection screen
--
-- Parameter :
-- sEntity         the master entity
-- lSList          object list holding the master values
--
-- Return:
-- STB$OERROR$RECORD
--
-- ***************************************************************************************************

FUNCTION showHeadData(olPopertityList OUT STB$PROPERTY$LIST) RETURN STB$OERROR$RECORD;
-- ***************************************************************************************************
-- Date und Autor: 02 Feb 2006 JB
-- displays the master data of a report
--
-- Parameter:
-- olPopertityList  window holding the report master data
--
-- Return:
-- STB$OERROR$RECORD
--
-- ***************************************************************************************************

function getRenderingServerStatus return varchar2;
--******************************************************************************
FUNCTION callFunction(oParameter in STB$MENUENTRY$RECORD,  olNodeList OUT STB$TREENODELIST)    RETURN STB$OERROR$RECORD;
-- ***************************************************************************************************
-- Date und Autor: 02 Feb 2006 JB
-- is the interface to process a command token selected in the context menu.
-- The function checks if the command token can be executed and calls the backend functionality
-- to process the token. If further information is required to process the command token,
-- a node object  is returned and displayed as dialog window
--
-- Parameters:
-- oParameter            the command token selected in the context menu
-- olNodeList            the dialog window when further information is required
--
-- Return:
-- STB$OERROR$RECORD
--
-- ***************************************************************************************************

FUNCTION getLov(sEntity in VARCHAR2, sSuchwert in VARCHAR2, oSelectionList OUT ISR$TLRSELECTION$LIST) RETURN STB$OERROR$RECORD;
-- ***************************************************************************************************
-- Date und Autor: 02 Feb 2006  JB
-- returns the a list of values of an data entity
--
-- Parameters:
-- sEntity            the entity to which the list of values belongs
-- sSuchwert          search string to filter/ limit the list
-- oSelectionList     the list of values  belong to the passed entity
--
-- Return:
-- STB$OERROR$RECORD
--
-- ***************************************************************************************************

FUNCTION GetNodeList(oSelectedNode in STB$TREENODE$RECORD, olNodeList OUT STB$TREENODELIST)  RETURN STB$OERROR$RECORD;
-- ***************************************************************************************************
-- Date und Autor: 02 Feb 2006  JB
-- retrievs all nodes that should be displayed as properties of the selected treenode
-- in the right split screen of the iStudyReporter Explorer
--
-- Parameters:
-- oSelectedNode  the node for which the property nodes  should be retrieved
-- olNodeList     the list of nodes to be displayed  in theright slit screen
--
-- ***************************************************************************************************

FUNCTION GetNodeChilds(oSelectedNode in STB$TREENODE$RECORD, olChildNodes OUT STB$TREENODELIST)  RETURN STB$OERROR$RECORD;
-- ***************************************************************************************************
-- retrievs the subnodes of the selected tree node in the iStudyReporter Explorer
--
-- Parameters:
-- oSelectedNode   the node for which the sub nodes should be retrieved
-- olChildNodes    the list of children nodes
--
-- Return:
-- STB$OERROR$RECORD
--
-- ***************************************************************************************************

FUNCTION SetCurrentNode(oSelectedNode in STB$TREENODE$RECORD )  RETURN STB$OERROR$RECORD;
-- ***************************************************************************************************
-- Date und Autor: 02 Feb 2006  JB
-- sets the information on a tree node in the package STB$OBJECT
--
-- Parameters:
-- oSelectedNode       the node object describing the selected node in the iStudyReporter Explorer
--
-- Return:
-- STB$OERROR$RECORD
--
-- ***************************************************************************************************

FUNCTION putParameters(oParameter in STB$MENUENTRY$RECORD, olParameter in STB$PROPERTY$LIST)  RETURN STB$OERROR$RECORD;
-- ***************************************************************************************************
-- Date und Autor: 19.08.04 jb
-- is the counterpart function of callFunction , using the putParameter a
-- command token is procssed from a called dialog window.The entered information (e.g. esig , filter )
-- whes considered when the command token is processed
--
-- Parameters:
-- oParameter      the command token selected in the context menu
-- olParameter     object list holding all information of the attributes of the dialog window
--
-- Return:
-- STB$OERROR$RECORD
--
-- ***************************************************************************************************

FUNCTION getAuditMask(olNodeList OUT STB$TREENODELIST)  RETURN STB$OERROR$RECORD;
-- ***************************************************************************************************
-- Date und Autor: 02 Feb 2006  JB
-- retrieves a window holding the audit dialog, in this window the audit reason
--of a prompted audit can be entered
--
-- Parameter:
-- olNodeList     audit window
--
-- Return:
-- STB$OERROR$RECORD
--
-- ***************************************************************************************************

FUNCTION putAuditMask(olParameter in STB$PROPERTY$LIST)  RETURN STB$OERROR$RECORD;
-- ***************************************************************************************************
-- Date und Autor: 02 Feb 2006  JB
-- writes the audit reason entered in the audit window into the audittrail
--
-- Parameter:
-- olParameter      the audit winodw
--
-- Return:
-- STB$OERROR$RECORD
--
-- ***************************************************************************************************

FUNCTION checkAuditRequired(sAuditRequired OUT VARCHAR2)  RETURN STB$OERROR$RECORD;
-- ***************************************************************************************************
-- Date und Autor: 02 Feb 2006  JB
-- calls create Audit-function to checks if there have been changes on the report criteria,
-- output options or saving values. Function first verifies if the report type requires an audit
--
-- Parameter:
-- sAudit         flag to indicate if there have been changes
--
-- Return:
-- STB$OERROR$RECORD
--
-- ***************************************************************************************************

FUNCTION getContextMenu( nMenu in NUMBER,  olMenuEntryList OUT STB$MENUENTRY$LIST ) RETURN STB$OERROR$RECORD;
-- ***************************************************************************************************
-- Date und Autor: 02 Feb 2006  JB
-- returns the context menu for the selected tree node in the iStudyReporter Explorer ,
-- if a menu entry is passed into the function, the returned information will be the sub menu
-- belonging to the passed menu entry
--
-- Parameter:
-- nMenu                optional identifer of the "parent" menu entry
-- olMenuEntryList     the context menu of the current node or the submenu for the provided menu entry
--
-- Return:
-- STB$OERROR$RECORD
--
-- ***************************************************************************************************

FUNCTION checkDependenciesOnMask( oParameter IN STB$MENUENTRY$RECORD, olParameter IN OUT STB$PROPERTY$LIST, sModifiedFlag OUT VARCHAR2 ) RETURN STB$OERROR$RECORD;
-- ***************************************************************************************************
-- Date und Autor: 25. Apr 2008  MCD
-- calls the customer package to check if there are data dependencies on a
-- dialog window
--
-- Parameter :
-- oParameter         the menu entry
-- olParameter        the selected parameters
-- sModifiedFlag      checks if the property list changes
--
-- Return:
-- STB$OERROR$RECORD
--
-- ***************************************************************************************************

FUNCTION getlastfinalversion (nlastfinalrepid OUT NUMBER) RETURN stb$oerror$record;
-- ***************************************************************************************************
-- Date und Autor: 05. Mar 2010, MCD
-- moved from stb$util to here
-- gets the repid of the last final report within the current title
--
-- Parameter:
-- nlastfinalrepid    the repid
--
-- ***************************************************************************************************

FUNCTION firstform RETURN stb$oerror$record;
-- ***************************************************************************************************
-- Date und Autor: 05. Mar 2010, MCD
-- opens the wizard
-- published for LabOne
--
-- ***************************************************************************************************

FUNCTION createpassword (cnrepid IN NUMBER, spassword OUT VARCHAR2) RETURN stb$oerror$record;
-- ***************************************************************************************************
-- Date und Autor: 05. Mar 2010, MCD
-- creates a password
-- published for LabOne
--
-- Parameter:
-- cnrepid        the report
-- spassword      the password
--
-- ***************************************************************************************************

FUNCTION updatechecksum (cnrepid IN NUMBER) RETURN stb$oerror$record ;
-- ***************************************************************************************************
-- Date und Autor: 05. Mar 2010, MCD
-- updates a checksum for a report
-- published for LabOne
--
-- Parameter:
-- cnrepid       the report
--
-- ***************************************************************************************************

FUNCTION checktitleexists (stitle IN VARCHAR2) RETURN VARCHAR2;
-- ***************************************************************************************************
-- Date und Autor: 05. Mar 2010, MCD
-- checks if the title exists
-- published for LabOne
--
-- Parameter:
-- stitle       the title
--
-- ***************************************************************************************************

FUNCTION getSelectedLovDisplay(csParameter IN VARCHAR2, csValue IN VARCHAR2, csPackage IN VARCHAR2 DEFAULT 'ISR$REPORTTYPE$PACKAGE') RETURN VARCHAR2;
-- ***************************************************************************************************
-- Date und Autor: 17. May 2011, MCD
-- return the display of the lov for the given lov and value
-- uses a temporary table to save the values for a better performance
--
-- Parameter:
-- csParameter  the lov parameter
-- csValue      the lov value
-- csPackage    the package which handles the lov
--
-- Return:
-- the lov display
--
-- ***************************************************************************************************

FUNCTION createReportFromJob(cnRepid IN NUMBER, csTitle IN VARCHAR2, cnReporttypeid IN NUMBER) RETURN stb$oerror$record;
-- ***************************************************************************************************
-- Date und Autor: 17. May 2011, MCD
-- creates a new report version, used in the job package
-- return errors if the node is locked or the document is checked out
--
-- Parameter:
-- cnRepid          the repid
-- csTitle          the title, necessary if the repid is not available
-- cnReporttypeid   the reporttypeid, necessary if the repid is not available
--
-- Return:
-- stb$oerror$record
--
-- ***************************************************************************************************

function checkDocCheckout( csMenuAllowed char,
                           cnRepId       stb$report.repid%type )
  return char;
-- ***************************************************************************************************
-- Date und Autor: 29. Feb 2016, vs
--   checks, if a document of a report is in checked-out state.
--   if it is checked out, then 'F' is returned
--   if the input parameter csMenuAllowed = 'F' then 'F' is returned
--   otherwise 'T' is returned.
--
-- Parameter:
--   csMenuAllowed   current state of menu-allowed variable
--   cnRepId         Report to be examined, if document is checked out
--
-- Return:
--   new menu-allowed variable
--
-- ***************************************************************************************************


FUNCTION setJobParameter (cstoken IN isr$action.token%TYPE)
  RETURN stb$oerror$record ;

FUNCTION defineMailText(csToken in varchar2,oParamListRec in  isr$paramlist$rec,  sText out varchar2 )
return STB$OERROR$RECORD ;

FUNCTION defineMailSubject(csToken in varchar2,oParamListRec in  isr$paramlist$rec,  sSubject   out  varchar2 )
return STB$OERROR$RECORD;

FUNCTION defineMailAttachment(csToken in varchar2 ,oParamListRec in  isr$paramlist$rec,olFileList in   out  ISR$FILE$LIST  )
return STB$OERROR$RECORD ;
--******************************************************************************

procedure setCurrentParameter(coCurrentParameter stb$property$list);
--******************************************************************************
FUNCTION bookmarkList (olpopertylist IN OUT stb$property$list) RETURN stb$oerror$record;
--******************************************************************************************************
function getCurrentParameter return stb$property$list;
--******************************************************************************
function dropReport( cnRepid in number, csDropReportSelf in varchar2 default 'T') return stb$oerror$record ;
--******************************************************************************************************
FUNCTION switchCurrentGroupRight (nreporttypeid IN NUMBER, ocurrentnode IN stb$treenode$record, sToken IN VARCHAR2) RETURN stb$oerror$record ;

END ISR$REPORTTYPE$PACKAGE;