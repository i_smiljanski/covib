CREATE OR REPLACE PACKAGE Stb$action
is
-- ***************************************************************************************************
-- Description/Usage:
-- The package is a general libary of action steps defined in the table isr$action$job.
-- these functions are steps during the iStudyReporter job processing
-- the action steps can be part of an iStudyReporter document creation or can be any kind of action step
-- to display ,upload or create a file
-- All required action steps to fullfill a job are listed below a n action id and are called with
-- dynamic sql in a loop in the package stb$ral in the function createReport
-- please note: not all action steps of iStudyReporter are defined in this package
-- ****************************************************************************************************

FUNCTION initialiseJob(cnJobid in NUMBER, cxXml IN OUT XMLTYPE) RETURN STB$OERROR$RECORD;
-- ***************************************************************************************************
-- Date and Autor: 05.Jan 2006, MCD
-- initialises the loader and the job
--
-- Parameter:
-- cnJobid   the identifier of the iStudyReporter job
-- cnDomId   the domid of the xml configuration file
--
-- Return:
-- STB$OERROR$RECORD
--
-- ***************************************************************************************************
FUNCTION terminateJob(cnJobid in NUMBER, cxXml IN OUT XMLTYPE) RETURN STB$OERROR$RECORD;
-- ***************************************************************************************************
-- Date and Autor: 05.Jan 2006, MCD
-- terminates the loader module and the job
--
-- Parameter:
-- cnJobid   the identifier of the iStudyReporter job
-- cnDomId   the domid of the xml configuration file
--
-- Return:
-- STB$OERROR$RECORD
--
-- ***************************************************************************************************
FUNCTION callActionFunction (cnJobid in NUMBER, cxXml IN OUT XMLTYPE) RETURN STB$OERROR$RECORD;
-- ***************************************************************************************************
-- Date and Autor: 05.Jan 2007, MCD
-- calls the function which is set in the job parameters
--
-- Parameter:
-- cnJobid   the identifier of the iStudyReporter job
-- cnDomId   the domid of the xml configuration file
--
-- Return:
-- STB$OERROR$RECORD
--
-- ***************************************************************************************************
FUNCTION startCollector (cnJobid in NUMBER, cxXml IN OUT XMLTYPE) RETURN STB$OERROR$RECORD;
-- ***************************************************************************************************
-- Date and Autor: 31 Jan 2006 JB
-- starts the general collector, retrieves the name of the collector from report type parameters
-- using dynamic sql that function is called, there can be a set of collectors for the output id
-- below that function. This function is only the start point
--
-- Parameter:
-- cnJobid   the identifier of the iStudyReporter job
-- cnDomId   the domid of the xml configuration file
--
-- Return:
-- STB$OERROR$RECORD
--
-- ***************************************************************************************************
function copyFinalRawFile( cnJobid in number, cxXml in out xmltype) return STB$OERROR$RECORD;
-- ***************************************************************************************************
-- Date and Autor: 22.Sep 2015 HR
-- copies the raw data file from the parent version
--
-- Parameter:
-- cnJobid   the identifier of the iStudyReporter job
-- cxXml     the xml configuration file
--
-- Return:
-- STB$OERROR$RECORD
--
-- ***************************************************************************************************
function fillTableModel( cnJobid in number, cxXml in out xmltype) return STB$OERROR$RECORD;
-- ***************************************************************************************************
-- Date and Autor: 22.Sep 2015 HR
-- fills the temporary tables for the current report Job with the LIMS data
--
-- Parameter:
-- cnJobid   the identifier of the iStudyReporter job
-- cxXml     the xml configuration file
--
-- Return:
-- STB$OERROR$RECORD
--
-- ***************************************************************************************************
function createRawDatFile( cnJobid in number, cxXml in out xmltype) return STB$OERROR$RECORD;
-- ***************************************************************************************************
-- Date and Autor: 22.Sep 2015 HR
-- generates the raw data file for the current report Job from the LIMS data in the emporary tables
--
-- Parameter:
-- cnJobid   the identifier of the iStudyReporter job
-- cxXml     the xml configuration file
--
-- Return:
-- STB$OERROR$RECORD
--
-- ***************************************************************************************************

FUNCTION createRemoteSnapshot (cnJobid in NUMBER, cxXml IN OUT XMLTYPE) RETURN STB$OERROR$RECORD;

FUNCTION loadFilesToClient(cnJobid in NUMBER, cxXml IN OUT XMLTYPE) RETURN STB$OERROR$RECORD;
-- ***************************************************************************************************
-- Date and Autor: 08.Jan 2007, MCD
-- PL-SQL wrapper for the java-class to upload the files to the client computer from the database
-- table stb$jobtrail. The function uploads all files of the current job.
--
-- Parameter:
-- cnJobid   the identifier of the iStudyReporter job
-- cnDomId   the domid of the xml configuration file
--
-- Return:
-- STB$OERROR$RECORD
--
-- ***************************************************************************************************
FUNCTION copyDocumentIntoDoctrail(cnJobid in NUMBER, cxXml IN OUT XMLTYPE) RETURN STB$OERROR$RECORD;
-- ***************************************************************************************************
-- Date and Autor: 08.Jan 2007, MCD
-- copies the result document(s) created within the current database job from the table stb$jobtrail
-- into the document table stb$doctrail
--
-- Parameter:
-- cnJobid   the identifier of the iStudyReporter job
-- cnDomId   the domid of the xml configuration file
--
-- Return:
-- STB$OERROR$RECORD
--
-- ***************************************************************************************************

function copySnapshotIntoDoctrail( cnJobid in     number, cxXml   in out xmltype) RETURN STB$OERROR$RECORD;
FUNCTION copyXmlIntoDoctrail(cnJobid IN NUMBER, cxXml IN OUT XMLTYPE) RETURN STB$OERROR$RECORD;
-- ***************************************************************************************************
-- Date and Autor: 08.Jan 2007, MCD
-- copies the raw data xml file into the doctrail
--
-- Parameter:
-- cnJobid   the identifier of the iStudyReporter job
-- cnDomId   the domid of the xml configuration file
--
-- Return:
-- STB$OERROR$RECORD
--
-- ***************************************************************************************************
FUNCTION copyClobIntoDoctrail(cnJobid in NUMBER, cxXml IN OUT XMLTYPE) RETURN STB$OERROR$RECORD;
-- ***************************************************************************************************
-- Date and Autor: 08.Jan 2007, MCD
-- copies a clob as result document file from table stb$jobtrail
-- into the table stb$doctrail
--
-- Parameter:
-- cnJobid   the identifier of the iStudyReporter job
-- cnDomId   the domid of the xml configuration file
--
-- Return:
-- STB$OERROR$RECORD
--
-- ***************************************************************************************************
FUNCTION copyCommonIntoDoctrail(cnJobid in NUMBER, cxXml IN OUT XMLTYPE) RETURN STB$OERROR$RECORD;
-- ***************************************************************************************************
-- Date and Autor: 08.Jan 2007, MCD
-- copies a common document from the table stb$jobtrail into the stb$doctrail
--
-- Parameter:
-- cnJobid   the identifier of the iStudyReporter job
-- cnDomId   the domid of the xml configuration file
--
-- Return:
-- STB$OERROR$RECORD
--
-- ***************************************************************************************************
FUNCTION copyCommonIntoJobtrail(cnJobid in NUMBER, cxXml IN OUT XMLTYPE) RETURN STB$OERROR$RECORD;
-- ***************************************************************************************************
-- Date and Autor: 09.Jan 2007, MCD
-- copies a common document from the table stb$doctrail into the stb$jobtrail
--
-- Parameter:
-- cnJobid   the identifier of the iStudyReporter job
-- cnDomId   the domid of the xml configuration file
--
-- Return:
-- STB$OERROR$RECORD
--
-- ***************************************************************************************************
FUNCTION createParametersWordmodule(cnJobid in NUMBER, cxXml IN OUT XMLTYPE) RETURN STB$OERROR$RECORD;
-- ***************************************************************************************************
-- Date and Autor: 08.Jan 2007, MCD
-- creates the parameter string in the "configuration xml" to steer the output application MS WORD
-- depending on the request of the outputid. The defined options for the wordmodul are:
--
-- CAN_DISPLAY_REPORT
-- CAN_CHECKOUT
-- CAN_RUN_REPORT
-- CAN_FINALIZE_REPORT
--
-- Further the document properties of the Word document are written
--
-- Parameter:
-- cnJobid   the identifier of the iStudyReporter job
-- cnDomId   the domid of the xml configuration file
--
-- Return:
-- STB$OERROR$RECORD
--

-- ***************************************************************************************************
FUNCTION createParametersCheckout(cnJobid in NUMBER, cxXml IN OUT XMLTYPE) RETURN STB$OERROR$RECORD;
-- ***************************************************************************************************
-- Date and Autor: 08.Jan 2007, MCD
-- creates the parameter string in the "configuration xml" to steer the output application MS WORD
-- depending on the request of the outputid. The defined options for the word module are:
--
-- CAN_CHECKOUT
-- CAN_CHECKOUT_WITH_COMMON_DOC
--
-- Parameter:
-- cnJobid   the identifier of the iStudyReporter job
-- cnDomId   the domid of the xml configuration file
--
-- Return:
-- STB$OERROR$RECORD
--
-- ***************************************************************************************************
FUNCTION createParametersCommon(cnJobid in NUMBER, cxXml IN OUT XMLTYPE) RETURN STB$OERROR$RECORD;
-- ***************************************************************************************************
-- Date and Autor: 08.Jan 2007, MCD
-- creates the parameter string in the "config.xml" to steer the output application
-- for the common document.
-- The destination application of the common document on the client is found using
-- the file type of the common document.
-- If the file type is not known by the client an "open-with" dialog is displayed.
--
-- Parameter:
-- cnJobid   the identifier of the iStudyReporter job
-- cnDomId   the domid of the xml configuration file
--
-- Return:
-- STB$OERROR$RECORD
--
-- ***************************************************************************************************
FUNCTION createParametersUpload(cnJobid in NUMBER, cxXml IN OUT XMLTYPE) RETURN STB$OERROR$RECORD;
-- ***************************************************************************************************
-- Date and Autor: 08.Jan 2007, MCD
-- writes the upload information in the "config.xml" for the loader
-- when a common document is loaded into iStudyReporter
--
-- Parameter:
-- cnJobid   the identifier of the iStudyReporter job
-- cnDomId   the domid of the xml configuration file
--
-- Return:
-- STB$OERROR$RECORD
--
-- ***************************************************************************************************
FUNCTION createParametersCheckin(cnJobid in NUMBER, cxXml IN OUT XMLTYPE) RETURN STB$OERROR$RECORD;
-- ***************************************************************************************************
-- Date and Autor: 08.Jan 2007, MCD
-- writes the check-in information in the "config.xml" for an iStudyReporter or
-- common document. Depending on passed request standing behind the current output id:
-- CAN_UNDO_CHECKOUT
-- CAN_UNDO_CHECKOUT_COMMON_DOC
-- CAN_CHECKIN_COMMON_DOC
-- CAN_CHECKIN
-- If the document has an system oid, a module is executed first
-- which checks if the system oid of the document to checkin matches the estimated system oid.
--
-- Parameter:
-- cnJobid   the identifier of the iStudyReporter job
-- cnDomId   the domid of the xml configuration file
--
-- Return:
-- STB$OERROR$RECORD
--
-- ***************************************************************************************************
FUNCTION checkout(cnJobid in NUMBER, cxXml IN OUT XMLTYPE) RETURN STB$OERROR$RECORD;
-- ***************************************************************************************************
-- Date and Autor: 08.Jan 2007, MCD
-- writes the check-out information in the stb$doctrail for an iStudyReporter or
-- common document. Depending on passed request standing behind the current output id:
-- CAN_CHECKOUT
-- CAN_CHECKOUT_COMMON_DOC
-- CAN_CHECKOUT_WITH_COMMON_DOC
--
-- Parameter:
-- cnJobid   the identifier of the iStudyReporter job
-- cnDomId   the domid of the xml configuration file
--
-- Return:
-- STB$OERROR$RECORD
--
-- ***************************************************************************************************
FUNCTION createParametersPDF(cnJobid in NUMBER, cxXml IN OUT XMLTYPE) RETURN STB$OERROR$RECORD;
-- ***************************************************************************************************
-- Date and Autor: 31 Jan 2006 JB
-- creates the parameter string in the "config.xml"
-- to steer the output application PDF Module.
--
-- Parameter:
-- cnJobid   the identifier of the iStudyReporter job
-- cnDomId   the domid of the xml configuration file

-- Return:
-- STB$OERROR$RECORD
--
-- ***************************************************************************************************

FUNCTION createParametersGeneral(cnJobid IN NUMBER, cxXml IN OUT XMLTYPE) RETURN STB$OERROR$RECORD;
-- ***************************************************************************************************
-- Date and Autor: 08.Jan 2007, MCD
-- creates the general parameter string in the "config.xml"
--
-- Parameter:
-- cnJobid   the identifier of the iStudyReporter job
-- cnDomId   the domid of the xml configuration file
--
-- Return:
-- STB$OERROR$RECORD
--
-- ***************************************************************************************************

function createParametersArbitraryApp( cnJobid in number, cxXml in out xmltype) RETURN STB$OERROR$RECORD;
-- ***************************************************************************************************
-- Date and Autor: 21.Sep 2017, HR
-- creates the parameter string in the "config.xml" for opening file with arbitrary application
--
-- Parameter:
-- cnJobid   the identifier of the iStudyReporter job
-- cxXml     the xml configuration file
--
-- Return:
-- STB$OERROR$RECORD
--
-- ***************************************************************************************************


FUNCTION loadFilesFromClient(cnJobid in NUMBER, cxXml IN OUT XMLTYPE) RETURN STB$OERROR$RECORD;
-- ***************************************************************************************************
-- Date and Autor: 08.Jan 2007, MCD
-- loads the files from the client into the database.
-- First the "config.xml" is loaded back in the database to check
-- which in this file defined other target files have to retrieved from the client
-- and returned back in the table stb$jobtrail
--
-- Parameter:
-- cnJobid   the identifier of the iStudyReporter job
-- cnDomId   the domid of the xml configuration file
--
-- Return:
-- STB$OERROR$RECORD
--
-- ***************************************************************************************************

FUNCTION loadFilesFromClientAfterError(cnJobid in NUMBER, csFileNames in VARCHAR2) RETURN STB$OERROR$RECORD;
-- ***************************************************************************************************
-- Date and Autor: 08.Jan 2007, MCD
-- loads the files from the client into the database after an error occured
--
-- Parameter:
-- cnJobid   the identifier of the iStudyReporter job
-- csFileNames the fileNames to load
--
-- Return:
-- STB$OERROR$RECORD
--
-- ***************************************************************************************************

FUNCTION loadCommonFromClient(cnJobid in NUMBER, cxXml IN OUT XMLTYPE) RETURN STB$OERROR$RECORD;
-- ***************************************************************************************************
-- Date and Autor: 08.Jan 2007, MCD
-- loads a common document from the client into the database table stb$jobtrail
--
-- Parameter:
-- cnJobid   the identifier of the iStudyReporter job
-- cnDomId   the domid of the xml configuration file
--
-- Return:
-- STB$OERROR$RECORD
--
-- ***************************************************************************************************

FUNCTION loadFileFromClient(cnJobid in NUMBER, cxXml IN OUT XMLTYPE) RETURN STB$OERROR$RECORD;
-- ***************************************************************************************************
-- Date and Autor: 08.Jan 2007, MCD
-- gets a common document during the check in process from the client machine.
-- The document has been checked out and stored in the database before.
--
-- Parameter:
-- cnJobid   the identifier of the iStudyReporter job
-- cnDomId   the domid of the xml configuration file
--
-- Return:
-- STB$OERROR$RECORD
--
-- ***************************************************************************************************

FUNCTION startApp(cnJobid in NUMBER, cxXml IN OUT XMLTYPE) RETURN STB$OERROR$RECORD;
-- ***************************************************************************************************
-- Date and Autor: 08.Jan 2007, MCD
-- starts the client application,
-- the modus (synchron or asynchron) can be found in the config.xml
-- return values of the applications are
-- -1 (any error)
-- -2 (if the user choose [Cancel])
-- -3 (if a system error in the wordmodule occured)
-- -4 (if the user trys to checkin a wrong document)
--
-- Parameter:
-- cnJobid   the identifier of the iStudyReporter job
-- cnDomId   the domid of the xml configuration file
--
-- Return:
-- STB$OERROR$RECORD
--
-- ***************************************************************************************************

FUNCTION copyFilesIntoJobtrail(cnJobid in NUMBER, cxXml IN OUT XMLTYPE) RETURN STB$OERROR$RECORD;
-- ***************************************************************************************************
-- Date and Autor: 08.Jan 2007, MCD
-- copies all files belonging to an output of a versioned report type
-- to the table stb$jobtrail.
-- Files can be any kind templates,xml-files or a parser module.
-- When a previous version of a document already exists in the database
-- this file will be used as template and the so called "MASTERTEMPLATE" remains in the database.
-- The previous version will be retrieved from the table stb$doctrail.
--
-- Parameter:
-- cnJobid   the identifier of the iStudyReporter job
-- cnDomId   the domid of the xml configuration file
--
-- Return:
-- STB$OERROR$RECORD
--
-- ***************************************************************************************************

FUNCTION convertAttachments(cnJobid in NUMBER, cxXml IN OUT XMLTYPE) RETURN STB$OERROR$RECORD;
-- ***************************************************************************************************
-- Date and Autor: 24.Aug 2011, IS
-- converts pdf files to images and saves tham
-- to the table stb$jobtrail.
--
-- Parameter:
-- cnJobid   the identifier of the iStudyReporter job
-- cnDomId   the domid of the xml configuration file
--
-- Return:
-- STB$OERROR$RECORD
--
-- ***************************************************************************************************


FUNCTION writeAttachmentInfo (cnJobid IN NUMBER, cxXml IN OUT XMLTYPE) RETURN STB$OERROR$RECORD;
-- ***************************************************************************************************
-- Date and Autor: 29.Aug 2011, IS
-- appending the additional pdf-files to the config
--
-- Parameter:
-- cnJobid      Job ID
-- cnDomId      DOM ID for the condig file
--
-- Return:
-- STB$OERROR$RECORD

-- ***************************************************************************************************

FUNCTION saveAttachmentsInDocument (cnJobid IN NUMBER, cxXml IN OUT XMLTYPE) RETURN STB$OERROR$RECORD;
-- ***************************************************************************************************
-- Date und Autor: 07. Sep 2011, IS
-- calls the java function addAttachmentsToDoc in stb$java
--
--
-- ***************************************************************************************************

FUNCTION copyFiles2IntoJobtrail(cnJobid in NUMBER, cxXml IN OUT XMLTYPE) RETURN STB$OERROR$RECORD;
-- ***************************************************************************************************
-- Date and Autor: 11.Jan 2007, MCD
-- copies all files belonging to an output of a versioned report type
-- to the table stb$jobtrail.
-- Files can be any kind templates,xml-files or a parser module.
-- When a previous version of a document already exists in the database
-- this file will be used as template and the so called "MASTERTEMPLATE" remains in the database.
-- The previous version will be retrieved from the table stb$doctrail.
--
-- Parameter:
-- cnJobid   the identifier of the iStudyReporter job
-- cnDomId   the domid of the xml configuration file
--
-- Return:
-- STB$OERROR$RECORD
--
-- ***************************************************************************************************

FUNCTION copyFiles3IntoJobtrail(cnJobid in NUMBER, cxXml IN OUT XMLTYPE) RETURN STB$OERROR$RECORD;
-- ***************************************************************************************************
-- Date and Autor: 02.Mar 2011, MCD
-- copies all selected files to the table stb$jobtrail.
-- Files can be any kind templates,xml-files or a parser module.
-- When a previous version of a document already exists in the database
-- this file will be used as template and the so called "MASTERTEMPLATE" remains in the database.
-- The previous version will be retrieved from the table stb$doctrail.
--
-- Parameter:
-- cnJobid   the identifier of the iStudyReporter job
-- cnDomId   the domid of the xml configuration file
--
-- Return:
-- STB$OERROR$RECORD
--
-- ***************************************************************************************************

FUNCTION copyDocumentIntoJobtrail(cnJobid in NUMBER, cxXml IN OUT XMLTYPE) RETURN STB$OERROR$RECORD;
-- ***************************************************************************************************
-- Date and Autor: 08.Jan 2007, MCD
-- copies an existing document from the table stb$doctrail to the table stb$jobtrail.
-- Depending on the action all attachments are copied to the client as well
--
-- Parameter:
-- cnJobid   the identifier of the iStudyReporter job
-- cnDomId   the domid of the xml configuration file
--
-- Return:
-- STB$OERROR$RECORD
--
-- ***************************************************************************************************

FUNCTION copyXmlIntoJobtrail(cnJobid in NUMBER, cxXml IN OUT XMLTYPE) RETURN STB$OERROR$RECORD;
-- ***************************************************************************************************
-- Date and Autor: 08.Jan 2007, MCD
-- copies an existing raw data xml file
-- from the table stb$doctrail to the table stb$jobtrail.
--
-- Parameter:
-- cnJobid   the identifier of the iStudyReporter job
-- cnDomId   the domid of the xml configuration file
--
-- Return:
-- STB$OERROR$RECORD
--
-- ***************************************************************************************************

FUNCTION copyWordmoduleIntoJobtrail(cnJobid in NUMBER, cxXml IN OUT XMLTYPE) RETURN STB$OERROR$RECORD;
-- ***************************************************************************************************
-- Date and Autor: 08.Jan 2007, MCD
-- copies the wordmodul for reports having MS Word as output application to the
-- table stb$jobtrail, function is called during the display process
-- or check-out process of an iStudyReporter MS Word document
--
-- Parameter:
-- cnJobid   the identifier of the iStudyReporter job
-- cnDomId   the domid of the xml configuration file
--
-- Return:
-- STB$OERROR$RECORD
--
-- ***************************************************************************************************
FUNCTION startAppCond(cnJobid in NUMBER, cxXml IN OUT XMLTYPE) RETURN STB$OERROR$RECORD;
-- ***************************************************************************************************
-- Date and Autor: 08.Jan 2007, MCD
-- checks during the document creation if the user parameter
-- to display the created document is set to true.
-- If that is the case the document will be loaded to the client and
-- displayed in the final destination directly after the document creation process.
-- If not the user has to open the document out of iStudyReporter later manually.
--
-- Parameter:
-- cnJobid   the identifier of the iStudyReporter job
-- cnDomId   the domid of the xml configuration file
--
-- Return:
-- STB$OERROR$RECORD
--
-- ***************************************************************************************************

FUNCTION checkAdditionalFinalDocProps(cnJobid IN NUMBER, cxXml IN OUT XMLTYPE) RETURN STB$OERROR$RECORD;
-- ***************************************************************************************************
-- Date and Autor: 08.Feb 2007, MCD
-- writes the document property section in the
-- config file when a report gets finalized
--
-- Parameter:
-- cnJobid   the identifier of the iStudyReporter job
-- cnDomId   the domid of the xml configuration file
--
-- Return:
-- STB$OERROR$RECORD
--
-- ***************************************************************************************************

FUNCTION checkoutundo(cnJobid in NUMBER, cxXml IN OUT XMLTYPE) RETURN STB$OERROR$RECORD;
-- ***************************************************************************************************
-- Date and Autor: 08.Jan 2007, MCD
-- deletes the checked-out file on the client
-- and resets the checked-out flag in the database table stb$doctrail
--
-- Parameter:
-- cnJobid   the identifier of the iStudyReporter job
-- cnDomId   the domid of the xml configuration file
--
-- Return:
-- STB$OERROR$RECORD
--
-- ***************************************************************************************************

FUNCTION checkin(cnJobid in NUMBER, cxXml IN OUT XMLTYPE) RETURN STB$OERROR$RECORD;
-- ***************************************************************************************************
-- Date and Autor: 09.Jan 2007, MCD
-- resets the checked-out flag in the database table stb$doctrail
--
-- Parameter:
-- cnJobid   the identifier of the iStudyReporter job
-- cnDomId   the domid of the xml configuration file
--
-- Return:
-- STB$OERROR$RECORD
--
-- ***************************************************************************************************
FUNCTION deleteCommon(cnJobid in NUMBER, cxXml IN OUT XMLTYPE) RETURN STB$OERROR$RECORD;
-- ***************************************************************************************************
-- Date and Autor: 30 Aug 2011, IS
-- deletes the old file in the table stb$doctrail
--
-- Parameter:
-- cnJobid   the identifier of the iStudyReporter job
-- cnDomId   the domid of the xml configuration file
--
-- Return:
-- STB$OERROR$RECORD
--
-- ***************************************************************************************************

FUNCTION saveConfig(cnJobid in NUMBER, cxXml IN OUT XMLTYPE) RETURN STB$OERROR$RECORD;
-- ***************************************************************************************************
-- Date and Autor: 08.Jan 2007, MCD
-- saves the "config.xml" into the table stb$jobtrail
--
-- Parameter:
-- cnJobid   the identifier of the iStudyReporter job
-- cnDomId   the domid of the xml configuration file
--
-- Return:
-- STB$OERROR$RECORD
--
-- ***************************************************************************************************

FUNCTION getClientIp RETURN VARCHAR2;
-- ***************************************************************************************************
-- Date and Author: 31 Jan 2006 JB
-- returns the IP address of client. Used to determine, to which IP the loader sends the request
--
-- Return:
-- IP Address
--
-- ***************************************************************************************************

FUNCTION getLoaderPort RETURN VARCHAR2;
-- ***************************************************************************************************
-- Date and Autor: 31 Jan 2006 JB
-- returns the port where the loader agent process listens.
-- The oprt is defined in the system parameters
--
-- Return:
-- Port No
--
-- ***************************************************************************************************

FUNCTION isDocServerAvailable(sDocServerIp IN VARCHAR2, sDocServerPort IN VARCHAR2) RETURN VARCHAR2;
-- ***************************************************************************************************
-- Date and Autor: 08.Jan 2007, MCD
-- PL-SQL wrapper for the java-class, checks if the doc server is available
--
-- Parameter:
-- sDocServerIp   the ip address of the doc server
-- sDocServerPort  the port of the doc server
--
-- Return:
-- 'T' if the doc server is available, else 'F'
--

-- ***************************************************************************************************

FUNCTION controlJobs(cnJobid in NUMBER, cxXml IN OUT XMLTYPE) RETURN STB$OERROR$RECORD;
-- ***************************************************************************************************
-- Date and Autor: 10.Feb 2009, MCD
-- controls the jobs if more than one job is running during an action sequence
--
-- Parameter:
-- cnJobid   the identifier of the iStudyReporter job
-- cnDomId   the domid of the xml configuration file
--
-- Return:
-- STB$OERROR$RECORD
--
-- ***************************************************************************************************

FUNCTION createCriteriaReport(cnJobid IN NUMBER, cxXml IN OUT XMLTYPE) RETURN STB$OERROR$RECORD;
-- ***************************************************************************************************
-- Date und Autor: 05. Mar 2010, MCD
-- creates an criteria xml file for reporting
--
--
-- ***************************************************************************************************

FUNCTION createConfigurationFile (cnJobid IN NUMBER, cxXml IN OUT XMLTYPE) RETURN STB$OERROR$RECORD;
-- ***************************************************************************************************
-- Date und Autor: 05. Mar 2010, MCD
-- return the content of the <token> view for reporting
--
--
-- ***************************************************************************************************

FUNCTION createAuditFile (cnJobid IN NUMBER, cxXml IN OUT XMLTYPE) RETURN STB$OERROR$RECORD;
-- ***************************************************************************************************
-- Date und Autor: 05. Mar 2010, MCD
-- creates an audit xml file for reporting
--
--
-- ***************************************************************************************************

FUNCTION archiveReports (cnJobid IN NUMBER, cxXml IN OUT XMLTYPE) RETURN STB$OERROR$RECORD;
-- ***************************************************************************************************
-- Date und Autor: 05. Mar 2010, MCD
-- archives reports
--
--
-- ***************************************************************************************************

FUNCTION extractReports (cnJobid IN NUMBER, cxXml IN OUT XMLTYPE) RETURN STB$OERROR$RECORD;
-- ***************************************************************************************************
-- Date und Autor: 05. Mar 2010, MCD
-- extracts reports
--
--
-- ***************************************************************************************************

FUNCTION callWordmodule (cnJobid IN NUMBER, cxXml IN OUT XMLTYPE) RETURN STB$OERROR$RECORD;
-- ***************************************************************************************************
-- Date und Autor: 05. Mar 2010, MCD
-- calls the java function for the wordmodule in stb$java
--
--
-- ***************************************************************************************************

FUNCTION zipLogTable (cnJobid IN NUMBER, cxXml IN OUT XMLTYPE) RETURN STB$OERROR$RECORD;
-- ***************************************************************************************************
-- Date und Autor: 05. Mar 2010, MCD
-- zips the logclob and logblob column table STB$LOG
--
--
-- ***************************************************************************************************

FUNCTION saveLoaderDirIntoJob (cnJobid IN NUMBER, cxXml IN OUT XMLTYPE) RETURN STB$OERROR$RECORD;
-- ***************************************************************************************************
-- Date und Autor: 26. Aug 2019, IS
-- 1. search entry in STB$JOB for doc_type = 'LOADER_DIR' und jobid = current node
--   if not find:
  --   a. search loader im client for jobid = current node
  --   b. if find, calls 'uploadLoaderDir' from Loader
  --   c. if not find - error
-- 2. writes loader.zip in stb$jobtrail for current job
--
--
-- ***************************************************************************************************
FUNCTION concatenatePDFFiles (cnJobid IN NUMBER, cxXml IN OUT XMLTYPE) RETURN STB$OERROR$RECORD;
-- ***************************************************************************************************
-- Date und Autor: 05. Aug 2012, MCD
-- concatenates more than one PDF file to a single file
--
--
-- ***************************************************************************************************

function getServerLogFiles( cnJobid in     number,   cxXml   in out xmltype)
  return STB$OERROR$RECORD;
--*******************************************************************************************************************

FUNCTION getPath(csFullFileName IN VARCHAR2) RETURN VARCHAR2;

--***********************************************************************************************************************
FUNCTION getFilename(csFullFileName IN VARCHAR2) RETURN VARCHAR2;

--***********************************************************************************************************************
FUNCTION getExtension(csFullFileName IN VARCHAR2) RETURN VARCHAR2;

--***********************************************************************************************************************
function XMLFileExists(cnJobid in number, csDoctype in STB$DOCTRAIL.DOCTYPE%type, nDocIdXML out STB$DOCTRAIL.DOCID%type) return boolean;
-- ***************************************************************************************************

function createParamsForNewDoc( cnJobid in     number,
                                     cxXml   in out xmltype,
                                     csActionType in isr$action.token%type,
                                     coMastertemplateList in isr$varchar$list,
                                     sPassword out STB$REPORT.PASSWORD%type)
  return STB$OERROR$RECORD;
-- ***************************************************************************************************
FUNCTION copyUnprotectDocIntoJobtrail(cnJobid in NUMBER, cxXml IN OUT XMLTYPE) RETURN STB$OERROR$RECORD;
-- ***************************************************************************************************
-- Date and Autor: 9.Jun 2015, IS
-- unprotect the existing  in the table stb$doctrail document and then protect it with option 'ALLOW_ONLY_REVISIONS'  and copy to the table stb$jobtrail.
-- Depending on the action all attachments are copied to the client as well
--
-- Parameter:
-- cnJobid   the identifier of the iStudyReporter job
-- cnDomId   the domid of the xml configuration file
--
-- Return:
-- STB$OERROR$RECORD
--
-- ***************************************************************************************************

FUNCTION saveWordDocument(cnJobid IN NUMBER, nDocId in STB$DOCTRAIL.DOCID%TYPE, sPassword in STB$REPORT.PASSWORD%TYPE) RETURN STB$OERROR$RECORD;
--****************************************************************************************************************************

END Stb$action;