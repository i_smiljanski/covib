CREATE OR REPLACE PACKAGE STB$GAL
as
-- ***************************************************************************************************
-- Description/Usage:
-- The package STB$GAL is the GUI -Interface package. The Frontend calls directly the package functions
-- and procedures.
-- If the Frontens request works on a report type the STB$GAL calls the package ISR$REPORTTYPE$PACKAGE
-- If the request is dealing with the administrartion the request is passed to packages STB$SYSTEM or
-- STB$USERSECURITY
-- ***************************************************************************************************

FUNCTION checkISROwner RETURN VARCHAR2;
-- ***************************************************************************************************
-- Date und Autor: 04 Mar 2009 MCD
-- Return the istudyReporter owner in the system parameter STBOWNER
-- if no owner is found an exception is thrown
--
-- ***************************************************************************************************

FUNCTION checkISRUser(sUsername in varchar2, sPassword in varchar2, sFullUserName OUT VARCHAR2) RETURN STB$OERROR$RECORD;
-- ***************************************************************************************************
-- Date und Autor: 04 Mar 2009 MCD
-- checks if the current user has the permission to log-on to iStudyReporter
--
--
-- Parameters:
-- sFullUserName        full username of the current user
--
-- Return:
-- STB$OERROR$RECORD      
--
-- Error behaviour/ particularities:
-- none
-- ***************************************************************************************************

FUNCTION updateSessionInfo(csCheckOnly IN VARCHAR2 DEFAULT 'T') RETURN STB$OERROR$RECORD;
-- ***************************************************************************************************
-- Date and Autor: 05. Oct 2007, MCD
-- updates the session info in the table ISR$SESSION
--
-- Return:
-- STB$OERROR$RECORD     
--
-- ***************************************************************************************************

PROCEDURE destroyOldValues(reason in NUMBER default null);
-- ***************************************************************************************************
-- Date and Autor: 19 Dez 2005 JB
-- destroys the old values of the node object when a new node is set in the Explorer
-- 
-- Parameters:
-- reason   defines the type of the shutdown if the node is freed due to a log out
--
-- ***************************************************************************************************

PROCEDURE cancelMask;
-- ***************************************************************************************************
-- Date und Autor: 19 Dez 2005 JB
--  rolls back the transactions in the database if a pop window is left with [CANCEL]
--
-- ***************************************************************************************************

PROCEDURE cancelWizzard(oCurrentTreeNode OUT STB$TREENODE$RECORD);
-- ***************************************************************************************************
-- Date und Autor: 19 Dez 2005 JB
--  rolls back the transactions in the database when the report wizard is left with [CANCEL]
--
-- Parameters:
-- oCurrentTreeNode  the Explorer node that called the report wizard
--
-- ***************************************************************************************************

FUNCTION GetCurrentFormInfo(lsFormInfo OUT ISR$FORMINFO$RECORD, lsEntity OUT STB$ENTITY$LIST, nNextNavigationMask IN INTEGER) RETURN STB$OERROR$RECORD;
-- ***************************************************************************************************
-- Date und Autor: 19 Dez 2005 JB
-- retrieves the defintion of a wizard selection scrren and the entities of the selection screen 
-- if the screen is of the type "Master-Detail" or "Grid".
--
-- Parameters:
-- lsFormInfo           object that defines the selection screen
-- lsEntityList         entity list if there are more than one entity on the screen
--                      when the screen has the type "Grid" or "Master-Detail"
-- nNextNavigationMask  the number of the next selection screen in the report wizard
--
-- Return:
-- STB$OERROR$RECORD     
--
-- ***************************************************************************************************

FUNCTION GetSList (sEntity in VARCHAR2, sMasterKey in VARCHAR2, sNavigateMode in VARCHAR2, nextNavigationMask in INTEGER, oSelectionList OUT ISR$TLRSELECTION$LIST, oFormInfoRecord OUT ISR$FORMINFO$RECORD, oEntityList OUT STB$ENTITY$LIST) RETURN STB$OERROR$RECORD;
-- ***************************************************************************************************
-- Date and Autor:   19 Dez 2005 JB
-- All available values of the report wizard selection screen are retrieved into the oSelectionRecord. 
-- Previous selected values have the attribute  selected ='T'
--
-- Parameters:
-- sEntity             the current entity in the selection screen
-- sMasterKey          the master key if the selection is a grid select
-- sNavigateMode       flag to indicate if the scrren is called in the navigation mode
-- nextNavigationMask  the number of the selection screen
-- oSelection          object list with the attributes "sValue","sMasterKey", "sDisplay", "sInfo", 
--                    "sSelected" and "order number"
-- oFormInfoRecord     object that defines the selection screen
-- oEntityList         a list of all entities of the seection scren if more than one exist
--
-- Return:
-- STB$OERROR$RECORD 
--
-- ***************************************************************************************************

FUNCTION PutSList (sEntity in  VARCHAR2, lSList in ISR$TLRSELECTION$LIST) RETURN STB$OERROR$RECORD;
-- ***************************************************************************************************
-- Date and Autor:  19 Dez 2005 JB
-- writes the selected values of a selection screen of the report wizard  into the database
-- table ISR$CRIT. The changes are commited when the report is saved at the end of the wizard.
--
-- Parameters:
-- sEntity          the entity in the selection screen
-- lSList           object list of the selected values having the attributes
--                  "sMasterKey", "sValue", "sDisplay", "sInfo", "sSelected" and "order number"
--
-- Return:
-- STB$OERROR$RECORD 
--
-- ***************************************************************************************************

FUNCTION getOutputOption(olParameter OUT STB$PROPERTY$LIST) RETURN STB$OERROR$RECORD;
-- ***************************************************************************************************
-- Date und Autor: 19 Dez 2005 JB
-- retrieves the output parameter of a report type. The output options are the last selection
-- screen of the report wizard.
--
-- Parameters:
-- olParameter         the object list holding the output options
--
-- Return:
-- STB$OERROR$RECORD 
--
-- ***************************************************************************************************

FUNCTION putOutputOption(olParameter in STB$PROPERTY$LIST) RETURN STB$OERROR$RECORD;
-- ***************************************************************************************************
-- Date und Autor: 19 Dez 2005 JB
-- sets the new output options of the report in the table STB$REPORTPARAMETER
--
-- Parameters:
-- olParameter        the object list holding the user defined output options
--
-- Return:
-- STB$OERROR$RECORD 
--
-- ***************************************************************************************************

FUNCTION setMenuAccess(sParameter in VARCHAR2, sMenuAllowed OUT VARCHAR2)  RETURN STB$OERROR$RECORD;
-- ***************************************************************************************************
-- Date and Autor:  19 Dez 2005 JB
-- checks if a command token is selectable in the context menu. If a command token is
-- selectable depends on the context of the tree node and the granted system privileges 
-- of the authorization groups the user belongs to
--
-- Parameters:
-- sParameter       the command token of the context menu
-- sMenuAllowed     flag to indicate if the context command token is selectable in the context menu
--
-- Return:
-- STB$OERROR$RECORD  
--
-- ***************************************************************************************************

FUNCTION getSaveDialog(olSaveList OUT STB$TREENODELIST) RETURN STB$OERROR$RECORD;
--- ***************************************************************************************************
-- Date and Autor:  02 Feb 2006 JB
-- retrieves the attributes of the save-as-dialog
-- The number of attributes in the dialog  and which values can be modidified 
-- vary between the report types
--
-- Parameters:
-- olSaveList        object holding the default attributes of the save-as-dialog
--
-- Return:
-- STB$OERROR$RECORD 
--
-- ***************************************************************************************************

FUNCTION putSaveDialog(olSaveList in STB$PROPERTY$LIST) RETURN STB$OERROR$RECORD ;
-- ***************************************************************************************************
-- Date and Autor:   02 Feb 2006 JB
-- writes the save-as-dialog in to the database .
-- The attributes are part of the report definiton in the table STB$REPROT
--
-- Parameters:
-- olSaveList        object holding the defined attributes of the save-as-dialog
--
-- Return:
-- STB$OERROR$RECORD  
--
-- ***************************************************************************************************

FUNCTION initList (sEntity in  VARCHAR2, lSList OUT ISR$TLRSELECTION$LIST) RETURN STB$OERROR$RECORD;
-- ***************************************************************************************************
-- Date and Autor:  02 Feb 2006 JB
-- initalizes the master values of a master -detail selection screen
--
-- Parameters:
-- sEntity         the master entity
-- lSList          object record list holding the master values
--
-- Return:
-- STB$OERROR$RECORD   
--
-- ***************************************************************************************************

FUNCTION GetGridSelection (sEntity in  VARCHAR2, sNavigateMode in VARCHAR2, lSList OUT ISR$TLRSELECTION$LIST) RETURN STB$OERROR$RECORD;
-- ***************************************************************************************************
-- Date and Autor:  02 Feb 2006 JB
-- initalizes the selected values in the grid selection scrren
--
-- Parameters:
-- sEntity          the current entity of the mask
-- sNavigateMode    flag to indicate if the application is in the navigation mode
-- lSList           object list with the attributes "sValue","sMasterKey", "sDisplay", 
--                 "sInfo", "sSelected" and "order number"
--
-- Return:
-- STB$OERROR$RECORD    
--
-- ***************************************************************************************************

FUNCTION GetGridDependency(csEntity in VARCHAR2 , lsEntity OUT STB$ENTITY$LIST, csKey in VARCHAR2) RETURN STB$OERROR$RECORD;
-- ***************************************************************************************************
-- Date and Autor:  02 Feb 2006 JB
-- retrieves the depending entities of an entity and deletes if necessary the 
-- depdending values if the maser value changes
--
-- Parameters:
-- csEntity           the current entity
-- lsEntity           object list with all  entites of the screen with the flag if
--                    the values have to be deleted when the key value changes
-- csKey              the key value of the entity 
--
-- Return:
-- STB$OERROR$RECORD
--
-- ***************************************************************************************************

FUNCTION callFunction(oParameter in STB$MENUENTRY$RECORD, olNodeList OUT STB$TREENODELIST) RETURN STB$OERROR$RECORD;
-- ***************************************************************************************************
-- Date und Autor: 02 Feb 2006 JB
-- is the interface to process a command token selected in the context menu.
-- The function checks if the command token can be executed and calls the backend functionality
-- to process the token. If further information is required to process the command token,
-- a node object  is returned and displayed as dialog window
--
-- Parameters:
-- oParameter            the command token selected in the context menu
-- olNodeList            the dialog window when further information is required 
--
-- Return:
-- STB$OERROR$RECORD   
--
-- ***************************************************************************************************

FUNCTION getLov(sEntity in VARCHAR2, sSuchwert in VARCHAR2, oSelectionList OUT ISR$TLRSELECTION$LIST) RETURN STB$OERROR$RECORD;
-- ***************************************************************************************************
-- Date und Autor: 02 Feb 2006  JB
-- returns the a list of values of an data entity
--
-- Parameters:
-- sEntity            the entity to which the list of values belongs
-- sSuchwert          search string to filter/ limit the list 
-- oSelectionList     the list of values  belong to the passed entity
--
-- Return:
-- STB$OERROR$RECORD  
--
-- ***************************************************************************************************

FUNCTION putParameters(olParameter in STB$PROPERTY$LIST, oParameter in STB$MENUENTRY$RECORD, clParameter in CLOB DEFAULT NULL) RETURN STB$OERROR$RECORD;
-- ***************************************************************************************************
-- Date und Autor: 02 Feb 2006  JB
-- is the counterpart function of callFunction , using the putParameter a
-- command token is procssed from a called dialog window.The entered information (e.g. esig , filter )
-- whes considered when the command token is processed
--
-- Parameters:
-- oParameter      the command token selected in the context menu
-- olParameter     object list holding all information of the attributes of the dialog window
-- clParameter     xml holding all information of the attributes of the dialog window
--
-- Return:
-- STB$OERROR$RECORD    
--
-- ***************************************************************************************************

FUNCTION putEsigFailure(olParameter in STB$PROPERTY$LIST)  RETURN STB$OERROR$RECORD;
-- ***************************************************************************************************
-- Date und Autor: 02 Feb 2006  JB
-- is called if an error or failure in the esig window occurs, wrong password, cancel etc.
--
-- Parameters:
-- olParameter     object list with the values of the mask of the electronic signature
--
-- Return:
-- STB$OERROR$RECORD  
--
-- ***************************************************************************************************

FUNCTION GetNodeChilds(oSelectedNode in STB$TREENODE$RECORD, olChildNodes OUT STB$TREENODELIST, csApplicationCall VARCHAR2 DEFAULT 'T', csTransActionNo IN VARCHAR2 DEFAULT 'F') RETURN STB$OERROR$RECORD;
-- ***************************************************************************************************
-- Date und Autor: 02 Feb 2006  JB
-- retrievs the subnodes of the selected tree node in the iStudyReporter Explorer
--
-- Parameters:
-- oSelectedNode   the node for which the sub nodes should be retrieved
-- olChildNodes    the list of children nodes
--
-- Return:
-- STB$OERROR$RECORD   
--
-- ***************************************************************************************************

FUNCTION GetNodeList(oSelectedNode in STB$TREENODE$RECORD, olNodeList OUT STB$TREENODELIST, csApplicationCall VARCHAR2 DEFAULT 'T', csTransActionNo IN VARCHAR2 DEFAULT 'F') RETURN STB$OERROR$RECORD;
-- ***************************************************************************************************
-- Date und Autor: 02 Feb 2006  JB
-- retrievs all nodes that should be displayed as properties of the selected treenode 
-- in the right split screen of the iStudyReporter Explorer
--
-- Parameters:
-- oSelectedNode  the node for which the property nodes  should be retrieved
-- olNodeList     the list of nodes to be displayed  in theright slit screen
--
-- Return:
-- STB$OERROR$RECORD  
--
-- ***************************************************************************************************

FUNCTION SetCurrentNode(oSelectedNode in STB$TREENODE$RECORD, csLeftSide IN VARCHAR2, csApplicationCall VARCHAR2 DEFAULT 'T')  RETURN STB$OERROR$RECORD;
-- ***************************************************************************************************
-- Date und Autor: 02 Feb 2006  JB
-- sets the information on a tree node in the package STB$OBJECT 
--
-- Parameters:
-- oSelectedNode       the node object describing the selected node in the iStudyReporter Explorer
--
-- Return:
-- STB$OERROR$RECORD    
--
-- ***************************************************************************************************

FUNCTION getAuditMask(olNodeList OUT STB$TREENODELIST)  RETURN STB$OERROR$RECORD;
-- ***************************************************************************************************
-- Date und Autor: 02 Feb 2006  JB
-- retrieves a window, holding the audit dialog, in this window the audit reason 
--of a prompted audit can be entered 
--
-- Parameters:
-- olNodeList      the audit window eith all attributes
--
-- Return:
-- STB$OERROR$RECORD  
--
-- ***************************************************************************************************

FUNCTION putAuditMask(olParameter in STB$PROPERTY$LIST)  RETURN STB$OERROR$RECORD;
-- ***************************************************************************************************
-- Date und Autor: 02 Feb 2006  JB
-- writes the audit reason that was entered in the audit window into the 
-- table STB$AUDITTRAIL
--
-- Parameters:
-- olParameter      the audit winodw 
-- Return:
-- STB$OERROR$RECORD   
--
-- ***************************************************************************************************

FUNCTION getContextMenu( nMenu in NUMBER,  olMenuEntryList OUT STB$MENUENTRY$LIST ) RETURN STB$OERROR$RECORD;
--- ***************************************************************************************************
-- Date und Autor: 02 Feb 2006  JB
-- returns the context menu of a selected tree node in the iStudyReporter Explorer ,
-- if a menu entry is passed into the function, the returned information will be the sub menu
-- belonging to the passed menu entry
--
-- Parameters:
-- nMenu              optional identifer of the "parent" menu entry
-- olMenuEntryList    the context menu of the current node or the submenu for the provided menu entry
--
-- Return:
-- STB$OERROR$RECORD    
--
-- ***************************************************************************************************

FUNCTION synchronize (sToken OUT VARCHAR2, olNodeReloadFrom  OUT STB$TREENODELIST, oNewCurrentNode OUT STB$TREENODE$RECORD) RETURN STB$OERROR$RECORD;
-- ***************************************************************************************************
-- Date und Autor: 02 Feb 2006  JB
-- checks if a part of the tree or the property window have to be  synchronized with the backend, 
-- This is necessary if changes of values have occured or new tree nodes have been created
--
-- Parameters:
-- sToken               the token to define if the node has to be set in the right or left screewn of
--                      Explorer
-- oNodeReloadFrom      the start point of the tree reload
-- oNewCurrentNode      the new tree node that will be marked as current node 
--
-- Return:
-- STB$OERROR$RECORD    
--
-- ***************************************************************************************************

FUNCTION getTimeOut(nTimeOutInMinutes OUT NUMBER) RETURN STB$OERROR$RECORD;
-- ***************************************************************************************************
-- Date und Autor: 02 Feb 2006 JB
-- retrieves the timeout value of the current user
--
-- Parameters:
-- sTimeInMinutes         the timeout in minutes
--
-- Return:
-- STB$OERROR$RECORD    
--
-- ***************************************************************************************************

FUNCTION GetCurrentLanguage RETURN NUMBER;
-- ***************************************************************************************************
-- Date und Autor: 02 Feb 2006  JB
-- returns the Frontend language of the current user
--
-- Return:
-- the language id
--
-- ***************************************************************************************************

PROCEDURE setGuiPersistance(clobPersistance in CLOB);
-- ***************************************************************************************************
-- Date und Autor: 02 Feb 2006  JB
-- stores the user settings of the Frontend (size and position of the windows
-- and the selection scrrens)
--
-- Parameters:
-- olGuiObjects    object list with an object id and a byte array holding the properties 
--                 of the frontend
--
-- ***************************************************************************************************

PROCEDURE getGuiPersistance(clobPersistance OUT CLOB);
-- ***************************************************************************************************
-- Date und Autor: 02 Feb 2006  JB
-- gets the user settings of the frontend (size and position of the windows
-- and the selection screens)
--
-- olGuiObjects    object list with an object id and a byte array holding the properties 
--                 of the frontend
--
--******************************************************************************
FUNCTION getJobParameter(csName IN STB$JOBPARAMETER.PARAMETERNAME%TYPE, cnJobid IN STB$JOBPARAMETER.JOBID%TYPE) RETURN STB$JOBPARAMETER.PARAMETERVALUE%TYPE;
-- ***************************************************************************************************
-- Date und Autor: 2 March 2011, IS
-- retrieves the parameter value for the according job parameter name
--
-- Parameter:
-- csName                the parameter name
-- cnJobid         the jobid of the job
--
-- Return:
-- the parameter value or null if the parameter name don't exists
--
-- ***************************************************************************************************

FUNCTION getMenu(sToken in VARCHAR2, olMenuEntryList OUT STB$MENUENTRY$LIST ) RETURN STB$OERROR$RECORD;
-- ***************************************************************************************************
-- Date und Autor: 02 Feb 2006  JB
-- returns the entry record of the context menu for a comnmand token. The token will passed
-- to the backend
--
-- sToken           the passed command token of the contextmenu
-- olMenuEntryList  the menu entry record
--
-- Return:
-- STB$OERROR$RECORD    
--
-- ***************************************************************************************************

FUNCTION setClientPort(nClientPort in INTEGER) RETURN STB$OERROR$RECORD;
-- ***************************************************************************************************
-- Date und Autor: 02 Feb 2006 mcd
-- sets the client port and calls the function STB$SECURITY.setClientPort
--
-- Parameters:
-- nClientPort: the port number to be set
--
-- ***************************************************************************************************

/*
FUNCTION getClientPort RETURN INTEGER;
*/
-- ***************************************************************************************************
-- Date und Autor: 02 Feb 2006 mcd
-- gets the client port and calls the function STB$SECURITY.setClientPort
--
-- Return:
-- the client port
--
-- ***************************************************************************************************

FUNCTION getParameterValue( sParameter in VARCHAR2 ) RETURN VARCHAR2;
-- ***************************************************************************************************
-- Date und Autor: 02 Feb 2006 mcd
-- gets the value of a system parameter
--
-- Return:
-- the parameter value
--
-- ***************************************************************************************************

FUNCTION checkDependenciesOnMask( oParameter IN STB$MENUENTRY$RECORD, olParameter IN OUT STB$PROPERTY$LIST, sModifiedFlag OUT VARCHAR2 ) RETURN STB$OERROR$RECORD;
-- ***************************************************************************************************
-- Date und Autor: 11.Jul 2006 mcd
-- calls the customer package to check if there are data dependencies on a 
-- dialog window 
--
-- Parameters:
-- oParameter       the current command token
-- olParameter      parameter list which describes the dialog window 
-- sModifiedFlag    flag indicates if the attributes in the window have been modified 
--
-- Return:
-- STB$OERROR$RECORD
--
-- ***************************************************************************************************

FUNCTION getIcon( sSize in VARCHAR2, sIconid in VARCHAR2 ) RETURN BLOB;
-- ***************************************************************************************************
-- Date und Autor: 26.Jul 2006 MCD
-- gets an icon as blob from the table ISR$ICON
--
-- Parameters:
-- sSize         the size of the icon
-- sIconId       the icon identifier
--
-- Return:
-- the icon as blob file
--
-- ***************************************************************************************************

FUNCTION checkReload(reload out VARCHAR2) RETURN STB$OERROR$RECORD;
-- ***************************************************************************************************
-- Date und Autor: 19.Jan 2007 mcd
-- checks if the Frontend has to reload the Explorer
--
-- Parameter
-- reload 'T' or 'F'
--
-- return
-- STB$OERROR$RECORD
--
-- ***************************************************************************************************

PROCEDURE setApplicationInfo;
-- ***************************************************************************************************
-- Date und Autor: 2008, jb
-- sets application info as database sesion information
--
-- ***************************************************************************************************

PROCEDURE deleteSessionInfo(nSessionId IN ISR$SESSION.SESSIONID%TYPE, csUserNo in number default null);
-- ***************************************************************************************************
-- Date und Autor: 05. Mar 2010, MCD
-- deletes the session info for a sessionid in the table ISR$SESSION
--
-- Parameter:
-- nSessionId   sessionid
--  
--
-- ***************************************************************************************************

FUNCTION getXML(oParameter IN STB$MENUENTRY$RECORD, clXML OUT CLOB, clParamXml IN CLOB DEFAULT NULL) RETURN STB$OERROR$RECORD;
-- ***************************************************************************************************
-- Date und Autor: 16. May 2011, MCD
-- retrieves an xml file with instructions
--
-- Parameters:
-- oParameter   the parameter
-- clXML        the xml file
-- clParamXml   the xml file with needed parameters, default null
--
-- Return:
-- STB$OERROR$RECORD  
--
-- ***************************************************************************************************

FUNCTION putXML(oParameter IN STB$MENUENTRY$RECORD, clXML IN CLOB) RETURN STB$OERROR$RECORD;
-- ***************************************************************************************************
-- Date und Autor: 16. May 2011, MCD
-- sends an xml file with instructions
--
-- Parameters:
-- oParameter the parameter
-- clXML      the xml file 
--
-- Return:
-- STB$OERROR$RECORD   
--
-- ***************************************************************************************************

FUNCTION getJavaLogLevel RETURN VARCHAR2;

function setLoaderConnection (csPort in varchar2, csUserName in varchar2, csStart in varchar2 default 'T')
   return STB$OERROR$RECORD;

-- ***************************************************************************************************

FUNCTION getNodeDetails(  clXML      out clob)  RETURN STB$OERROR$RECORD;
-- ***************************************************************************************************
-- Date und Autor: 24. Jun 2015, IS
-- retrieves a node details as xml
--
-- Parameters:
-- clXML        the xml file
--
-- Return:
-- STB$OERROR$RECORD  
--
-- ****
FUNCTION getRecordTimestamp( csTableName in varchar2, csTableKey in varchar2, csKeyValue in varchar2, sModifiedOn out varchar2)  RETURN STB$OERROR$RECORD;
-- ***************************************************************************************************
-- Date und Autor: 02. Jun 2019, IS
-- retrieves a value of column 'modifiedon' for row with primary key 'csTableKey' from table 'csTableName'
--  example: oErrorObj:= stb$gal.getRecordTimestamp('isr$frontend$file', 'filename', 'main.xml', sModifiedOn);
--
-- Return:
-- STB$OERROR$RECORD  
-- DATE
--
-- ****

function synchronizeInBackground (sToken OUT VARCHAR2
                                , olTreeReloadList OUT STB$TREENODELIST
                                , oTreeReloadNode OUT STB$TREENODE$RECORD
                                , csNodeType in VARCHAR2
                                , csNodeId in VARCHAR2
                                , csSessionId in VARCHAR2) return STB$OERROR$RECORD;
END Stb$gal; 