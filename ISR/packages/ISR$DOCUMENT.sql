CREATE OR REPLACE package isr$document
as
-- ***************************************************************************************************
-- Description/Usage:
--   This package provides functionality for document handling
-- ***************************************************************************************************

cursor curReportDocument( ccnRepId stb$report.repid%type   default null,
                          ccnDocId stb$doctrail.docid%type default null ) is
-- this cursor provides the whole hierarchy of reports and allocated documents
-- the arguments allow to depict a certain dataset
-- null arguments allow a higher amount of output datasets
-- exact arguments minimize the number of output datasets
  select -- lpad(' ',2*(level-1))||level rep_lvl,    -->  for testing, dont remove
         rownum ranking, connect_by_root repid root_repid,
         case
           when repid = connect_by_root repid then null
           when trim(replace(sys_connect_by_path(docid,' '),docid,'')) is null then null
           else substr(  trim(replace(sys_connect_by_path(docid,' '),docid,'')),       instr(trim(replace(sys_connect_by_path(docid,' '),docid,'')),' ',-1)+1     )
         end predecessing_docid,
         repdoc.* 
  from   (  select rep.repid, rep.parentrepid, rep.status rep_status, rep.version rep_version, rep.title rep_title, srnumber rep_srnumber, password rep_password,
                   doc.docid, doc.filename, doc.doctype, doc.doc_oid, doc.docversion
            from   stb$report rep
            left join stb$doctrail doc
            on     doc.nodetype = 'REPORT'
            and    doc.doctype = 'D'
            and    doc.nodeid = rep.repid ) repdoc
  where   (      ccnRepId is null
             or  ccnRepId = connect_by_root repid )
  and     (      ccnDocId is null
             or  ccnDocId = docid )
  start  with parentrepid is null
  connect by parentrepid = prior repid
  order by ranking;    


function callFunction( oParameter in  STB$MENUENTRY$RECORD, 
                       olNodeList out STB$TREENODELIST      )  return STB$OERROR$RECORD;
-- ***************************************************************************************************
-- Date und Autor: 09 Feb. 2016, vs
-- ***************************************************************************************************

function setMenuAccess( sParameter   in  varchar2, 
                        sMenuAllowed out varchar2 )    return STB$OERROR$RECORD;
-- ***************************************************************************************************
-- Date und Autor: 16 Feb. 2016, vs
-- checks if a command token is selectable in the context menu.
--
-- Parameters:
--   sParameter       the command token of the context menu
--   sMenuAllowed     flag to indicate if the context command token is selectable in the context menu.  Values: 'T' and 'F'
--
-- Return:
--   STB$OERROR$RECORD    
-- ***************************************************************************************************

function putParameters( oParameter  in STB$MENUENTRY$RECORD, 
                        olParameter in STB$PROPERTY$LIST,
                        clParameter in clob default null   )  return STB$OERROR$RECORD;
-- ***************************************************************************************************
-- Date und Autor: 17 Feb. 2016, vs
-- ***************************************************************************************************       

function prepareDocCompare( cnJobId in     number,
                            xXml    in out xmltype )
  return STB$OERROR$RECORD;
-- ***************************************************************************************************
-- Date und Autor: 23 Feb. 2016, vs
-- ***************************************************************************************************   
-- inserts target-document and template-document into table STB$JOBTRAIL
-- and makes config file complete for further processing
--
-- Parameters:
--   cnJobId          identifier of the job
--   xXml             xml file including the job data
--
-- Return:
--   STB$OERROR$RECORD    
-- ***************************************************************************************************

function saveConfig( cnJobid in     number, 
                     xXml    in out xmltype)
  return STB$OERROR$RECORD;
-- ***************************************************************************************************
-- Date und Autor: 31 Mar. 2016, vs
-- ***************************************************************************************************   


end isr$document;