CREATE OR REPLACE package ISR$trace
as
-- ***************************************************************************************************
-- Description:
-- The package contains all functionality that deals with the logging of iStudyReporter.
-- iStudyReporter also logs some activities. These are handled in the package ISR$ACTION$LOG.
-- The former logging concept used an error number with a standard range from 0 to 255.
-- iSRv3.5 introduces a new concept. A definition set of a loglevels consist of the entries 
-- in table ISR$LOGLEVEL. Although the former usage of numbers is supported with iSRv3.5, for
-- code changes or new code, it is strongly recommended to use the procedures FATAL, ERROR, WARN, 
-- STATISTIC, INFO, INFO_ALL, DEBUG and DEBUG_ALL from this package. 
-- For processing and for comparisons, the number of the loglevel is used furthermore.
------------------------------------------------------------------------------------------------------
-- 10.04.2015 [ISRC-187] Table and index moved to new tablespace ISR_NOLOG_CORE, append-hint added to code
--            and trigger-code moved into package body.
------------------------------------------------------------------------------------------------------
--  VERY IMPORTANT:
--  ---------------
--  Some functions in iStudyReporter are called from this package. To prevent a circle-reference of
--  recursive calls, these functions itself may not log. Currently (2016-09-07) the following modules
--  are involved in that rule:  STB$UTIL.isNumeric
--                              STB$UTIL.getRightDelim
--                              STB$JOB.getJobSession
--                              STB$JOB.getJobParameter
--                              STB$JOB.getJobId
--                              STB$SECURITY.getCurrentUser
--                              STB$SECURITY.getCurrentSession
--                              STB$SECURITY.getOracleUserName
--                              STB$SECURITY.getClientIp
--                              STB$OBJECT.getCurrentDocId
--                              STB$OBJECT.getCurrentReportTypeId
--                              STB$OBJECT.getCurrentRepId
--                              STB$OBJECT.getCurrentNodeType
--                              STB$OBJECT.getCurrentNodeId
--   
-- ***************************************************************************************************


-- Public Package Constants
csErrorLoglevelName constant ISR$LOGLEVEL.LOGLEVEL%type     := 'ERROR';
cnErrorLoglevelNo   constant ISR$LOGLEVEL.LOGLEVEL_END%type := -1;

-- ***************************************************************************************************
-- Date and Author: 17 Oct 2014 IS
-- Writes one entry into the table isr$log for the 'DEBUG' loglevel, defined in table isr$loglevel
--
-- Parameters:
-- sActionTaken     First, short term of the log message, describing the action. This entry is written into isr$log.actiontaken. 
-- sFunctionGroup   The name of the module that called the isr$trace package for logging. It consists of <PKG_NAME>.<PRC_OR_FNC_NAME>
-- sLogEntry        Second descriptive part of the log message. This entry is written into isr$log.logentry.
-- clClob           Container for additional CLOB text information, if more than 4000 characters are expected, e.g. used for broken jobs.
-- blBlob           Container for additional binary BLOB information, e.g. used for broken jobs.
-- cxXml            Container for additional XML information.
-- caObjData        Container for additional object or obj collection information. This entry is written into isr$log.logclob as an XML text
--                  The caller should use sys.anydata.convertObject(<objvar>) or sys.anydata.convertCollection(<collectionvar>);
-- ***************************************************************************************************
-- 12.01.2016 (vs): For the output of collection types, the origin datatype is preferred to the common use of xmltype or anydata
-- it is intended, that complex structured types are only logged in debug mode. The usage of calls with the anydata is only intended
-- as an exception during the programming process, but should be finally removed or replaced with the orignal datatype.
-- ***************************************************************************************************
procedure debug ( csActionTaken   in varchar2   default null,
                  csLogEntry      in varchar2,
                  csFunctionGroup in varchar2,
                  cblBlob         in blob       default EMPTY_BLOB()  ); 
-- overload
procedure debug ( csActionTaken   in varchar2   default null,
                  csLogEntry      in varchar2,
                  csFunctionGroup in varchar2,
                  cclClob         in clob ); 
-- overload
procedure debug ( csActionTaken   in varchar2   default null,
                  csLogEntry      in varchar2,
                  csFunctionGroup in varchar2,
                  cxXml           in xmltype );      
-- overload
procedure debug ( csActionTaken   in varchar2   default null,
                  csLogEntry      in varchar2,
                  csFunctionGroup in varchar2,
                  caObjData       in anydata );
-- overload  stb$treenode$record
procedure debug ( csActionTaken    in varchar2   default null,
                  csLogEntry       in varchar2,
                  csFunctionGroup  in varchar2,
                  coTreenodeRecord in STB$TREENODE$RECORD ); 
-- overload  stb$treenodelist
procedure debug ( csActionTaken    in varchar2   default null,
                  csLogEntry       in varchar2,
                  csFunctionGroup  in varchar2,
                  clTreenodeList   in STB$TREENODELIST );   
-- overload  stb$menuentry$list
procedure debug ( csActionTaken    in varchar2   default null,
                  csLogEntry       in varchar2,
                  csFunctionGroup  in varchar2,
                  clMenuEntryList  in STB$MENUENTRY$LIST );   
-- overload  stb$menuentry$record
procedure debug ( csActionTaken     in varchar2   default null,
                  csLogEntry        in varchar2,
                  csFunctionGroup   in varchar2,
                  coMenuEntryRecord in STB$MENUENTRY$RECORD );                     
-- overload  stb$oerror$record
procedure debug ( csActionTaken    in varchar2   default null,
                  csLogEntry       in varchar2,
                  csFunctionGroup  in varchar2,
                  coErrorRecord    in STB$OERROR$RECORD );  
-- overload  stb$property$list
procedure debug ( csActionTaken    in varchar2   default null,
                  csLogEntry       in varchar2,
                  csFunctionGroup  in varchar2,
                  coPropertyList   in STB$PROPERTY$LIST );   
-- overload  isr$forminfo$record                  
procedure debug ( csActionTaken    in varchar2   default null,
                  csLogEntry       in varchar2,
                  csFunctionGroup  in varchar2,
                  coFormInfoRecord in ISR$FORMINFO$RECORD );  
-- overload  isr$forminfo$list
procedure debug ( csActionTaken    in varchar2   default null,
                  csLogEntry       in varchar2,
                  csFunctionGroup  in varchar2,
                  coFormInfoList   in ISR$FORMINFO$LIST );   
-- overload  isr$tlrselection$list
procedure debug ( csActionTaken    in varchar2   default null,
                  csLogEntry       in varchar2,
                  csFunctionGroup  in varchar2,
                  coSelectionList  in ISR$TLRSELECTION$LIST ); 
 -- overload  isr$paramlist$rec                  
procedure debug ( csActionTaken    in varchar2   default null,
                  csLogEntry       in varchar2,
                  csFunctionGroup  in varchar2,
                  coParamListRec   in ISR$PARAMLIST$REC );         
-- overload  isr$tlrselection$list
procedure debug ( csActionTaken    in varchar2   default null,
                  csLogEntry       in varchar2,
                  csFunctionGroup  in varchar2,
                  coEntityList     in STB$ENTITY$LIST ); 
 -- overload  isr$crit$rec                  
procedure debug ( csActionTaken    in varchar2   default null,  -- [ISRC-852]
                  csLogEntry       in varchar2,
                  csFunctionGroup  in varchar2,
                  coCritRec        in ISR$CRIT$REC );     
-- overload  isr$parameter$rec             
procedure debug ( csActionTaken    in varchar2   default null,  -- [ISRC-908]
                  csLogEntry       in varchar2,
                  csFunctionGroup  in varchar2,
                  coParamRec       in ISR$PARAMETER$REC );

-- ***************************************************************************************************
-- Date and Author: 17 Oct 2014 IS
-- Writes one entry into the table isr$log for the 'DEBUG_ALL' loglevel, defined in table isr$loglevel
--
-- Parameters:
-- sActionTaken     First, short term of the log message, describing the action. This entry is written into isr$log.actiontaken. 
-- sFunctionGroup   The name of the module that called the isr$trace package for logging. It consists of <PKG_NAME>.<PRC_OR_FNC_NAME>
-- sLogEntry        Second descriptive part of the log message. This entry is written into isr$log.logentry.
-- clClob           Container for additional CLOB text information, if more than 4000 characters are expected, e.g. used for broken jobs.
-- blBlob           Container for additional binary BLOB information, e.g. used for broken jobs.
-- ***************************************************************************************************
-- ***************************************************************************************************                
procedure debug_all ( csActionTaken   in varchar2   default null,
                      csLogEntry      in varchar2,
                      csFunctionGroup in varchar2,
                      cblBlob         in blob       default EMPTY_BLOB()  ); 
-- overload
procedure debug_all ( csActionTaken   in varchar2   default null,
                      csLogEntry      in varchar2,
                      csFunctionGroup in varchar2,
                      cclClob         in clob ); 
-- overload
procedure debug_all ( csActionTaken   in varchar2   default null,
                      csLogEntry      in varchar2,
                      csFunctionGroup in varchar2,
                      cxXml           in xmltype );      
-- overload
procedure debug_all ( csActionTaken   in varchar2   default null,
                      csLogEntry      in varchar2,
                      csFunctionGroup in varchar2,
                      caObjData       in anydata );                        


-- ***************************************************************************************************
-- Date and Author: 17 Oct 2014 IS
-- Writes one entry into the table isr$log for the 'ERROR' loglevel, defined in table isr$loglevel
-- ERROR is the default loglevel.
--
-- Parameters:
-- sActionTaken     First, short term of the log message, describing the action. This entry is written into isr$log.actiontaken. 
-- sFunctionGroup   The name of the module that called the isr$trace package for logging. It consists of <PKG_NAME>.<PRC_OR_FNC_NAME>
-- sLogEntry        Second descriptive part of the log message. This entry is written into isr$log.logentry.
-- clClob           Container for additional CLOB text information, if more than 4000 characters are expected, e.g. used for broken jobs.
-- blBlob           Container for additional binary BLOB information, e.g. used for broken jobs.
-- ***************************************************************************************************
-- ***************************************************************************************************
procedure error ( csActionTaken   in varchar2   default null,
                  csLogEntry      in varchar2,
                  csFunctionGroup in varchar2,
                  cblBlob         in blob       default EMPTY_BLOB()  ); 
-- overload
procedure error ( csActionTaken   in varchar2   default null,
                  csLogEntry      in varchar2,
                  csFunctionGroup in varchar2,
                  cclClob         in clob ); 
-- overload
procedure error ( csActionTaken   in varchar2   default null,
                  csLogEntry      in varchar2,
                  csFunctionGroup in varchar2,
                  cxXml           in xmltype ); 
-- overload  stb$oerror$record
procedure error ( csActionTaken    in varchar2   default null,
                  csLogEntry       in varchar2,
                  csFunctionGroup  in varchar2,
                  coErrorRecord    in STB$OERROR$RECORD );                      


-- ***************************************************************************************************
-- Date and Author: 17 Oct 2014 IS
-- Writes one entry into the table isr$log for the 'FATAL' loglevel, defined in table isr$loglevel
-- ERROR is the default loglevel.
--
-- Parameters:
-- sActionTaken     First, short term of the log message, describing the action. This entry is written into isr$log.actiontaken. 
-- sFunctionGroup   The name of the module that called the isr$trace package for logging. It consists of <PKG_NAME>.<PRC_OR_FNC_NAME>
-- sLogEntry        Second descriptive part of the log message. This entry is written into isr$log.logentry.
-- clClob           Container for additional CLOB text information, if more than 4000 characters are expected, e.g. used for broken jobs.
-- blBlob           Container for additional binary BLOB information, e.g. used for broken jobs.
-- ***************************************************************************************************
-- ***************************************************************************************************    
procedure fatal ( csActionTaken   in varchar2   default null,
                  csLogEntry      in varchar2,
                  csFunctionGroup in varchar2,
                  cblBlob         in blob       default EMPTY_BLOB()  ); 
-- overload
procedure fatal ( csActionTaken   in varchar2   default null,
                  csLogEntry      in varchar2,
                  csFunctionGroup in varchar2,
                  cclClob         in clob ); 
-- overload
procedure fatal ( csActionTaken   in varchar2   default null,
                  csLogEntry      in varchar2,
                  csFunctionGroup in varchar2,
                  cxXml           in xmltype ); 
                  
                  
-- ***************************************************************************************************
-- Date and Author: 17 Oct 2014 IS
-- Writes one entry into the table isr$log for the 'INFO' loglevel, defined in table isr$loglevel
-- FATAL is invisible for users and administrators. Sometimes, this loglevel is also called 'critical',
-- it is used for servere errors, that need prompt remedy.
--
-- Parameters:
-- sActionTaken     First, short term of the log message, describing the action. This entry is written into isr$log.actiontaken. 
-- sFunctionGroup   The name of the module that called the isr$trace package for logging. It consists of <PKG_NAME>.<PRC_OR_FNC_NAME>
-- sLogEntry        Second descriptive part of the log message. This entry is written into isr$log.logentry.
-- clClob           Container for additional CLOB text information, if more than 4000 characters are expected, e.g. used for broken jobs.
-- blBlob           Container for additional binary BLOB information, e.g. used for broken jobs.
-- ***************************************************************************************************
-- ***************************************************************************************************
procedure info ( csActionTaken   in varchar2 default null,
                 csLogEntry      in varchar2,
                 csFunctionGroup in varchar2,
                 cclClob         in clob default EMPTY_CLOB(),
                 cblBlob         in blob default EMPTY_BLOB() );                 

-- overload
PROCEDURE info ( csActionTaken   in varchar2 default null,
                  csLogEntry      in varchar2,
                  csFunctionGroup in varchar2,
                  cxXml           in xmltype ) ;    


-- ***************************************************************************************************
-- Date and Author: 2015-02-02 vs
-- Writes one entry into the table isr$log for the 'INFO_ALL' loglevel, defined in table isr$loglevel
-- FATAL is invisible for users and administrators. Sometimes, this loglevel is also called 'critical',
-- it is used for servere errors, that need prompt remedy.
--
-- Parameters:
-- sActionTaken     First, short term of the log message, describing the action. This entry is written into isr$log.actiontaken. 
-- sFunctionGroup   The name of the module that called the isr$trace package for logging. It consists of <PKG_NAME>.<PRC_OR_FNC_NAME>
-- sLogEntry        Second descriptive part of the log message. This entry is written into isr$log.logentry.
-- clClob           Container for additional CLOB text information, if more than 4000 characters are expected, e.g. used for broken jobs.
-- blBlob           Container for additional binary BLOB information, e.g. used for broken jobs.
-- ***************************************************************************************************
-- ***************************************************************************************************
procedure info_all ( csActionTaken   in varchar2  default null,
                     csLogEntry      in varchar2,
                     csFunctionGroup in varchar2,
                     cclClob         in clob      default EMPTY_CLOB(),
                     cblBlob         in blob      default EMPTY_BLOB() );                 


-- ***************************************************************************************************
-- Date and Author: 17 Oct 2014 IS
-- Writes one entry into the table isr$log for the 'STATISTIC' loglevel, defined in table isr$loglevel
-- FATAL is invisible for users and administrators. Sometimes, this loglevel is also called 'critical',
-- it is used for servere errors, that need prompt remedy.
--
-- Parameters:
-- sActionTaken     First, short term of the log message, describing the action. This entry is written into isr$log.actiontaken. 
-- sFunctionGroup   The name of the module that called the isr$trace package for logging. It consists of <PKG_NAME>.<PRC_OR_FNC_NAME>
-- sLogEntry        Second descriptive part of the log message. This entry is written into isr$log.logentry.
-- clClob           Container for additional CLOB text information, if more than 4000 characters are expected, e.g. used for broken jobs.
-- blBlob           Container for additional binary BLOB information, e.g. used for broken jobs.
-- ***************************************************************************************************
-- ***************************************************************************************************
procedure stat ( csActionTaken   in varchar2 default null,
                 csLogEntry      in varchar2,
                 csFunctionGroup in varchar2,
                 cclClob         in clob default EMPTY_CLOB(),
                 cblBlob         in blob default EMPTY_BLOB() );                 
procedure stat ( csActionTaken   in varchar2 default null,
                 csFunctionGroup in varchar2,
                 cclClob         in clob default EMPTY_CLOB(),
                 cblBlob         in blob default EMPTY_BLOB() );     
-- overload
procedure stat ( csActionTaken   in varchar2   default null,
                 csLogEntry      in varchar2,
                 csFunctionGroup in varchar2,
                 cxXml           in xmltype );      
-- overload
procedure stat ( csActionTaken   in varchar2   default null,
                 csLogEntry      in varchar2,
                 csFunctionGroup in varchar2,
                 caObjData       in anydata );    
                 
                 
-- ***************************************************************************************************
-- Date and Author: 17 Oct 2014 IS
-- Writes one entry into the table isr$log for the 'WARN' loglevel, defined in table isr$loglevel
-- FATAL is invisible for users and administrators. Sometimes, this loglevel is also called 'critical',
-- it is used for servere errors, that need prompt remedy.
--
-- Parameters:
-- sActionTaken     First, short term of the log message, describing the action. This entry is written into isr$log.actiontaken. 
-- sFunctionGroup   The name of the module that called the isr$trace package for logging. It consists of <PKG_NAME>.<PRC_OR_FNC_NAME>
-- sLogEntry        Second descriptive part of the log message. This entry is written into isr$log.logentry.
-- clClob           Container for additional CLOB text information, if more than 4000 characters are expected, e.g. used for broken jobs.
-- blBlob           Container for additional binary BLOB information, e.g. used for broken jobs.
-- ***************************************************************************************************
-- ***************************************************************************************************
procedure warn ( csActionTaken   in varchar2   default null,
                 csLogEntry      in varchar2,
                 csFunctionGroup in varchar2,
                 cblBlob         in blob       default EMPTY_BLOB()  ); 
-- overload
procedure warn ( csActionTaken   in varchar2   default null,
                 csLogEntry      in varchar2,
                 csFunctionGroup in varchar2,
                 cclClob         in clob ); 
-- overload
procedure warn ( csActionTaken   in varchar2   default null,
                 csLogEntry      in varchar2,
                 csFunctionGroup in varchar2,
                 cxXml           in xmltype );    
-- overload                 
procedure warn ( csActionTaken   in varchar2 default null,
                 csLogEntry      in varchar2,
                 csFunctionGroup in varchar2,
                 coErrorRecord   in STB$OERROR$RECORD );                  
                                    

-- ***************************************************************************************************
function getSystemLoglevel return number;
-- ***************************************************************************************************
-- 25. May 2016 (vs): read system loglevel from table STB$SYSTEMPARAMETER
-- use result_cache, to improve performance during debugging mode
-- ***************************************************************************************************


-- ***************************************************************************************************
function getAdjustedLoglevelNo ( cnLoglevelno number ) return number result_cache;
-- ***************************************************************************************************
-- Date and Author: 2015-02-06 vs
-- This function calculates the adjusted loglevel number that is determined by others functions in this package.
-- The purpose is a determined assignment of a loglevelevel-number, even if another number of the same level is known.
-- Example:  A given loglevel-number 225 is classified as INFO_ALL, because INFO_ALL.loglevel_begin = 201
--           and INFO_ALL.loglevel_end = 249. 
--           For 225, the new loglevel-number will be adjusted to 249, because 249 is equal to isr$trace.getLoglevelStr(225).
-- Return:
-- Returns the adjusted loglevel-number
-- ***************************************************************************************************


-- ***************************************************************************************************
function getUserLoglevelNo return number result_cache;
-- ***************************************************************************************************
-- Date and Author: 2015-02-06 vs
-- Returns the calulated user-loglevel as number.
--
-- Return:
-- The user's LogLevel as number, as defined in ISR$LOGLEVEL.LOGLEVEL
-- ***************************************************************************************************


-- ***************************************************************************************************
function getUserLoglevelStr return varchar2 result_cache;
-- ***************************************************************************************************
-- Date and Author: 14 Oct 2014 IS
-- Returns the calculated user-loglevel as string.
-- The old name of this function was getLogLevelAsString
--
-- Return:
-- The user's LogLevel as character, as defined in ISR$LOGLEVEL.LOGLEVEL
-- ***************************************************************************************************


-- ***************************************************************************************************
function getJavaUserLoglevelStr return varchar2 ;  -- (vs) 20.Sept.2017:  result_cache ist noch zu kl�ren, besprochen mit (is)
-- ***************************************************************************************************
-- Date and Author: 14 Oct 2014 IS
-- Returns the Java-related user-loglevel as String.
-- Therefore ISR$LOGLEVEL.JAVALOGLEVEL contains the assigned value for the named java loglevel.
-- The old name of this function was getLogLevelAsString
--
-- Return:
-- The user's LogLevel as character, as defined in ISR$LOGLEVEL.LOGLEVEL
-- ***************************************************************************************************


-- ***************************************************************************************************
function getLogLevelNo( csLoglevelName in varchar2 default csErrorLoglevelName ) return number result_cache;
-- ***************************************************************************************************
-- Date and Author: 2015-01-29 vs
-- getLogLevelNo returns a valid log-level-number for a given log-level-name
-- the assignment is defined in table ISR$LOGLEVEL. The returned loglevel is related
-- to the package code, not to the user.
--
-- Return:
-- the log level as number
-- ***************************************************************************************************


-- ***************************************************************************************************
FUNCTION getLogLevelStr( cnLoglevelNo IN NUMBER DEFAULT cnErrorLoglevelNo ) RETURN VARCHAR2 result_cache;
-- ***************************************************************************************************
-- Date and Author: 2015-02-04 vs
-- getLogLevelStr returns a valid log-level-name for a given log-level-number
-- the assignment is defined in table ISR$LOGLEVEL. The returned loglevel is related
-- to the package code, not to the user.
--
-- Return:
-- the log level as varchar2
-- ***************************************************************************************************


-- ***************************************************************************************************
procedure setLogForJava( csActionTaken   in varchar2,
                         csFunctionGroup in varchar2,
                         csLogEntry      in varchar2,
                         csLoglevelName  in varchar2,
                         csReference     in varchar2,
                         csCallpackage   in clob default null,
                         cclClob         in clob     default EMPTY_CLOB(),
                         csLogLevel      in varchar2  default null,
                         cblBlob         in blob     default EMPTY_BLOB() );
-- ***************************************************************************************************
-- ***************************************************************************************************
                
         
-- ***************************************************************************************************
function getReference return varchar2;   
-- ***************************************************************************************************
-- ***************************************************************************************************


-- ***************************************************************************************************
function getCallStack return varchar2 ;
-- ***************************************************************************************************
-- ***************************************************************************************************


-- ***************************************************************************************************
function getLogLevel return number;  -- (vs) 20.Sept.2017:  result_cache ist noch zu kl�ren, besprochen mit (is)
-- ***************************************************************************************************
-- ***************************************************************************************************


-- ***************************************************************************************************
procedure setExternalReference (csExternalReference isr$log.reference%type);
-- ***************************************************************************************************
-- ***************************************************************************************************


-- ***************************************************************************************************
procedure setExternalLogLevel (csExternalLogLevel isr$log.logLevelName%type);
-- ***************************************************************************************************
-- ***************************************************************************************************

-- ***************************************************************************************************
procedure setSessionLoglevel;   
-- ***************************************************************************************************
-- Date and Author: 2019-05-10 vs
-- allows an external call to initialize the session loglevel (variable sSessionLogLevel) for ISR$TRACE  [ISRC-111], [ISRC-1108]
-- ***************************************************************************************************

procedure setSessionInfo;
-- ***************************************************************************************************
-- Date and Author: 2020-04-21 mcd
-- is used to set environment variables to avoid queries in "setLog"
-- it is not possible to set environment variables in the main body because logging
-- is often started before the variables are available
-- ***************************************************************************************************

end ISR$trace; 