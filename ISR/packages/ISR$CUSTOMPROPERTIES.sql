CREATE OR REPLACE PACKAGE iSR$CustomProperties
is
-- ***************************************************************************************************
-- Description/Usage:
-- The package holds all functionality for creation of the properties.xml file  
-- ***************************************************************************************************

FUNCTION createPropertiesFile(cnJobId in NUMBER, cxXml IN OUT XMLTYPE) RETURN STB$OERROR$RECORD;
-- ***************************************************************************************************
-- Date und Autor: 7.Dec 2015  HR
-- function creates and saves the properties.xml file
-- functionality copied from the function STB$TRANSFORM.performTransformation
--
--
-- Parameter:
-- nJobId       the jobid of the iStudyReporter job
--
--
-- Return:
-- STB$OERROR$RECORD   
--
-- ***************************************************************************************************

END iSR$CustomProperties;