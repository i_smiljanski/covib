CREATE OR REPLACE PACKAGE Stb$util
IS
-- ***************************************************************************************************
-- Description/Usage:
-- The package holds all functions and procedures used at more then one place within iStudyReporter
-- ***************************************************************************************************

FUNCTION PutSList (sEntity IN  Stb$typedef.tLine, oSelection IN ISR$TLRSELECTION$LIST) RETURN STB$OERROR$RECORD;
-- ***************************************************************************************************
-- Date and Author: 30 Jan 2006 JB
-- writes the selected values of an entity in the report wizard into the database table ISR$CRIT.
-- The old selection criteria of the entity will be deleted before.
-- The function serves all mask types apart from the grid-selections.
-- is called when the mask will be left
--
-- Parameter :
-- sEntity     the entity of the selection mask
-- oSelection  object list with the attributes (sValue, sMasterKey, sDisplay, sInfo, sSelected , ordernumber)
--
-- Return:
-- STB$OERROR$RECORD
--
-- ***************************************************************************************************

FUNCTION PutSListGrid (sEntity IN  Stb$typedef.tLine, sSuchwert  IN Stb$typedef.tLine, oSelection IN ISR$TLRSELECTION$LIST) RETURN STB$OERROR$RECORD;
-- ***************************************************************************************************
-- Date and Author: 30 Jan 2006 JB
-- writes the selected values of an entity in the report wizard into the database table ISR$CRIT.
-- The old selection criteria of the entity will be deleted before.
-- The function serves grid-selections.
-- The master key of the record will be passed as search string into the function.
-- is called whenever a field is switched in the grid.
--
-- Parameter :
-- sEntity     the entity of the selection mask
-- sSuchwert   the mastervalue to which the detail values belong to
-- oSelection  object list with the attributes (sValue, sMasterKey, sDisplay, sInfo, sSelected , ordernumber)
--
-- Return:
-- STB$OERROR$RECORD
--
-- ***************************************************************************************************

FUNCTION checkCriteria (cnRepId IN NUMBER, csEntity IN VARCHAR2, sFound OUT VARCHAR2, oSelection OUT ISR$TLRSELECTION$LIST) RETURN STB$OERROR$RECORD;
-- ***************************************************************************************************
-- Date and Author: 30 Jan 2006 JB
-- retireves the existing values in the table ISR$CRIT for an entity and report id.
-- The selected values will be written in an object list
--
-- Parameter:
-- cnRepId           the report id
-- csEntity          the entity
-- sFound            flag to indicate if values were  found
-- oSelection        object list holding the selection criteria beloning to the entity and the report id
--                   object list with the attributes(sValue,
--                   sMasterKey, sDisplay, sInfo, sSelected)
--
-- Return:
-- STB$OERROR$RECORD
--
-- ***************************************************************************************************

FUNCTION checkPromptedAuditRequired(sType IN VARCHAR2) RETURN VARCHAR2;
-- ***************************************************************************************************
-- Date and Author: 30 Jan 2006 JB
-- checks if a prompted audit is required for made changes
--
-- Parameter:
-- sType                 the type of the audit
--
-- Return:
-- flag if a prompted audit is required
--
-- ***************************************************************************************************

FUNCTION checksum(csCheck IN BLOB, nSum OUT NUMBER) RETURN STB$OERROR$RECORD;
-- ***************************************************************************************************
-- Date and Author: 30 Jan 2006 JB
-- calculates the checksum of a blob, wrapper for the java class
--
-- Parameter:
-- csCheck                   the blob file for which the checksum will be calculated
-- nSum                      the calculated checksum
--
-- Return:
-- STB$OERROR$RECORD
--
-- ***************************************************************************************************

FUNCTION checksum(csCheck IN CLOB, nSum OUT NUMBER) RETURN STB$OERROR$RECORD;
-- ***************************************************************************************************
-- Date and Author: 30 Jan 2006 JB
-- calculates the checksum of a clob, wrapper for the java class
--
-- Parameter:
-- csCheck                   the clob file for which the checksum will be calculated
-- nSum                      the calculated checksum
--
-- Return:
-- STB$OERROR$RECORD
--
-- ***************************************************************************************************

FUNCTION getChecksum(obj IN BLOB) RETURN NUMBER;
--****************************************************************************************************
-- Date and Autor: 30 Jan 2006 JB
-- calls a wrapper for the java class, that  alculates the checksum of a clob
--
-- Parameter:
-- obj          the blob the checksum will be retrieved
--
-- Return:
-- number       the checksum
--
-- ***************************************************************************************************

FUNCTION getChecksum(obj IN CLOB) RETURN NUMBER;
--****************************************************************************************************
-- Date and Autor: 30 Jan 2006 JB
-- calls a wrapper for the java class, that  alculates the checksum of a clob
--
-- Parameter:
-- obj          the clob the checksum will be retrieved
--
-- Return:
-- number       the checksum
--
-- ***************************************************************************************************

FUNCTION getColPropertyList(sSelect IN VARCHAR2,olPropertyList OUT STB$PROPERTY$LIST) RETURN STB$OERROR$RECORD;
-- ***************************************************************************************************
-- Date and Author: 30 Jan 2006 JB
-- turns a select statement in a object property list, the given select statement retrives one row
--
-- Parameter:
-- sSelect           the select statement to be turned into the object list
-- olPropertyList   the object list created from the given select statement
--
-- Return:
-- STB$OERROR$RECORD
--
-- ***************************************************************************************************

FUNCTION getColListMultiRow(sSelect IN VARCHAR2,olNodeList IN OUT STB$TREENODELIST) RETURN STB$OERROR$RECORD;
-- ***************************************************************************************************
-- Date and Author: 30 Jan 2006 JB
-- turns a select statement in a treenode list, is often used for getNodeList
--
-- Parameter:
-- sSelect           the select statement to be turned into the object list
-- olNodeList        the tree node list created from the given select statement
--
-- Return:
-- STB$OERROR$RECORD
--
-- ***************************************************************************************************

FUNCTION getRepTypeParameter( nRepType IN NUMBER, sParametername IN VARCHAR2, sParameterValue OUT VARCHAR2) RETURN STB$OERROR$RECORD;
-- ***************************************************************************************************
-- Date and Author: 30 Jan 2006 JB
-- retrieves the value of a passed reporttype parameter
--
--
-- Parameter:
-- nRepType          the identifier of the reporttype
-- sParameterName    the parameter name
-- sParameterValue   the value of the parameter
--
-- Return:
-- STB$OERROR$RECORD
--
-- ***************************************************************************************************

FUNCTION getSystemParameter( sParametername IN VARCHAR2, sParameterValue OUT VARCHAR2) RETURN STB$OERROR$RECORD;
-- ***************************************************************************************************
-- Date and Author: 30 Jan 2006 JB
-- retrieves the value of a passed system parameter
--
-- Parameter:
-- sParameterName    the system parameter
-- sParameterValue   the value of the system parameter
--
-- Return:
-- STB$OERROR$RECORD
--
-- ***************************************************************************************************

FUNCTION getContextMenu(sNodeType IN VARCHAR2, nMenu IN NUMBER, olMenuEntryList OUT STB$MENUENTRY$LIST ) RETURN STB$OERROR$RECORD;
-- ***************************************************************************************************
-- Date und Autor: 02 Feb 2006  JB
-- returns the contextmenu for the selected tree node in the iStudyReporter Explorer,
-- if a menu entry is passed into the function, the returned information will be the sub menu
-- belonging to the passed menu entry
--
-- Parameter:
-- nMenu              optional identifer of the "parent" menu entry
-- olMenuEntryList     the context menu of the current node or the submenu for the provided menu entry
--
-- Return:
-- STB$OERROR$RECORD
--
-- ***************************************************************************************************

FUNCTION createFilterParameter(olParameter IN STB$PROPERTY$LIST) RETURN STB$OERROR$RECORD;
-- ***************************************************************************************************
-- Date and Author: 02 Feb 2006  jb
-- writes the dialog in the jobparameter table
--
-- Parameter:
-- olParameter     object holding all fileds to display a filter mask
--
-- Return:
-- STB$OERROR$RECORD
--
-- ***************************************************************************************************

FUNCTION getHandler(csToken in varchar2) RETURN VARCHAR2;
-- ***************************************************************************************************
-- Date and Author:  02 Feb 2006  jb
-- gets the name of the package that handles the command token,
-- when the token is not processed by the current database package
--
-- Parameter:
-- csToken          the token to be processsed
--
-- Return:
-- the package processing the token
--
-- ***************************************************************************************************

FUNCTION getCurrentLAL RETURN VARCHAR2;
-- ***************************************************************************************************
-- Date und Autor: 06.Mrz 2007, MCD
-- retrieves the system parameter of the underlaying lims system which identifies the LAL package
-- to describle the interface towards the lims
--
-- Return:
-- value of the used LAL-Package
--
-- ***************************************************************************************************

FUNCTION getCurrentCustomPackage RETURN VARCHAR2;
-- ***************************************************************************************************
-- Date und Autor: 06.Mrz 2007, MCD
-- retrieves the system parameter which identifies the main custom package
-- to describle the interface towards the customer
--
-- Return:
-- value of the used main custom package
--
-- ***************************************************************************************************

FUNCTION getPropertyList(sToken IN VARCHAR2, olParameter OUT stb$property$list) RETURN STB$OERROR$RECORD;
-- ***************************************************************************************************
-- Date and Author: 15. Apr 2008, MCD
-- Creates a property-list from a dialog (TABLE: ISR$DIALOG)    -- vs 08.Mar.2016
--
-- Parameter:
--   sToken         The Token, that is search criteria in table ISR$DIALOG
--   olParameter    The Property-List to be returned as OUT parameter
--
-- Return:
-- STB$OERROR$RECORD
--
-- ***************************************************************************************************

function getProperty( olParameter   in stb$property$list,
                      csParameter    in varchar2,
                      csOutputValue in varchar2 default 'SVALUE' )
  return varchar2;
-- ***************************************************************************************************
-- Date and Author: 19 Jan 2009, MCD
-- returns the value for a parameter of the propertylist
--
-- Parameter:
-- olParameter          the property list
-- sParameter           the parameter
--
-- Return:
-- the value
--
-- *******************************************************************************************************

procedure setProperty(olParameter in out stb$property$list,
                      csParameter   in varchar2,
                      csOutputValue in varchar2 default 'SVALUE',
                      csNewValue in varchar2);
-- ***************************************************************************************************
-- Date and Author: 31 Aug 2021, IS
-- puts the value 'csNewValue' of parameter 'csParameter' to the propertylist 'olParameter' for attribut 'csOutputValue'
-- example: stb$util.setProperty(olParameter, 'APPENDIX', 'SDISPLAYED', 'T');
-- (in the list 'olParameter' the value 'SDISPLAYED' of parameter 'APPENDIX' changed to 'T')
--
-- Parameter:
-- olParameter          the property list
-- csParameter          the parameter (row) which 'csOutputValue' (column) should be changed
-- csOutputValue        attribut of list 'olParameter'
-- csNewValue           new value
--
-- ***************************************************************************************************     

function getReptypeParameter( nreptype       in number,
                              sparametername in varchar2 )
  return varchar2
  result_cache;
-- ***************************************************************************************************
-- Date and Author: 30.Jan 2009, MCD
-- retrieves the value of a passed reporttype parameter and return it as a string
-- overloaded function
--
--
-- Parameter:
-- nRepType          the identifier of the reporttype
-- sParameterName    the parameter name
--
-- Return:
-- the value of the reptype parameter
--
-- ***************************************************************************************************


FUNCTION getSystemParameter (sParametername IN VARCHAR2) RETURN VARCHAR2 result_cache;
-- ***************************************************************************************************
-- Date and Author: 30.Jan 2009, MCD
-- retrieves the value of a passed system parameter and return it as a string
-- overloaded function
--
-- Parameter:
-- sParameterName    the system parameter
--
-- Return:
-- the value of the system parameter
--
-- ***************************************************************************************************

FUNCTION clobToBlob (clob_in IN CLOB) RETURN BLOB;
-- ***************************************************************************************************
-- Date and Author: 02.Feb 2009, MCD
-- turns a clob into a blob, initialization of the lob is in the function
--
-- Parameter:
-- clob_in    the clob
--
-- Return:
-- the blob
--
-- ***************************************************************************************************

FUNCTION blobToClob (blob_in IN BLOB) RETURN CLOB;
-- ***************************************************************************************************
-- Date and Author: 02.Feb 2009, MCD
-- turns a blob into a clob, initialization of the lob is in the function
--
-- Parameter:
-- blob_in    the blob
--
-- Return:
-- the clob
--
-- ***************************************************************************************************

FUNCTION getLeftDelim(csText IN VARCHAR2, csDelim in VARCHAR2 default '_') return varchar2;
-- ***************************************************************************************************
-- Date and Author: 29.Sep 2008 HR
-- gets the left part in the string csText from the beginning until the first occurrence of the
-- delimiter in csDelim. If csDelim is not found in csText then the entire string is returned
--  Examples: If csText='ABCD_EFG' and csDelim='_' then 'ABCD' is returned.
--            If csText='ABCDEFG' and csDelim='_' then 'ABCDEFG' is returned.
--
-- Parameter:
-- csText           the text to be searched for delimiter
-- csDelim          the delimiter which delimits the first part from the rest of the text
--
-- Return:
-- the text found as the left part of the string
--
-- ***************************************************************************************************

FUNCTION getRightDelim(csText IN VARCHAR2, csDelim in VARCHAR2 default '_') return varchar2;
-- ***************************************************************************************************
-- Date and Author: 29.Sep 2008 HR
-- gets the right part in the string csText from the first occurrence of the delimiter
-- in csDelim until the end. If csDelim is not found in csText then NULL is returned
--  Examples: If csText='ABCD_EFG' and csDelim='_' then 'EFG' is returned.
--            If csText='ABCDEFG' and csDelim='_' then NULL is returned.
--
-- Parameter:
-- csText           the text to be searched for delimiter
-- csDelim          the delimiter which delimits the parts of the text
--
-- Return:
-- the text found as the right part of the string
--
-- ***************************************************************************************************

FUNCTION getGroupEntity(csEntity IN isr$report$wizard.entity%TYPE, cnReportTypeId IN isr$report$wizard.ReportTypeId%TYPE) return varchar2;
-- ***************************************************************************************************
-- Date and Author: 23.Sep 2008 HR
-- retrieves the primary (first) groupng entity from ISR$META$CRIT$GROUPING
-- it is used in SQL selection statements
--
-- Parameter :
-- csEntity        the entity (or entity alis) from iSR$Report$Wizard
-- cnReportTypeId  the report type ID
--
-- Return:
-- Grouping entity or null if no grouping is defined for csEntity in the report
--
-- ***************************************************************************************************

function execute_clob(clCode IN CLOB) return VARCHAR2;
-- ***************************************************************************************************
-- Date and Author: 08.Apr 2009 MCD
-- executes a given clob dynamically, only plsql code is possible
--
-- Parameter :
-- clCode        the code to execute
--
-- Return:
-- T if successfully executed else the error code
--
-- ***************************************************************************************************

FUNCTION getReportParameterValue (csName IN VARCHAR2, cnRepid IN NUMBER) RETURN VARCHAR2;
-- ***************************************************************************************************
-- Date and Author: 03.Sep 2009 HR
-- retrieves the parameter value for a report parameter
-- if the report parameter does not exist, then the function tries to retrieve a report type parameter
-- with the same parameter name
--
-- Parameter :
-- csName            the parameter name
-- cnRepid           the report id
--
-- Return:
-- the value for the parameter of the report
--
-- ***************************************************************************************************

procedure setReportParameterValue(csParameterName IN VARCHAR2, csParameterValue IN VARCHAR2, cnRepid IN NUMBER)  ;
-- ***************************************************************************************************
-- Date and Author: 14.Apr 2016 IS
-- sets the parameter value for a report parameter
--
-- Parameter :
-- csParameterName      the parameter name
-- csParameterValue      the parameter value
-- cnRepid           the report id
--
-- ***************************************************************************************************

FUNCTION getReportTypeForTitle (csTitle IN VARCHAR2) RETURN NUMBER;
-- ***************************************************************************************************

function getPassword(cnRepId in number) RETURN VARCHAR2;
-- ***************************************************************************************************
-- Date and Autor: 31 Jan 2006  JB
-- retrieves the crypted password of a report
--
-- Parameter:
-- cnRepId      the current report id
--
-- Return
-- sPassword    the password of the report id
--
-- ***************************************************************************************************

FUNCTION getMaskTitle(csToken IN VARCHAR2) RETURN VARCHAR2;
-- ***************************************************************************************************
-- Date and Autor: 17, May 2011, MCD
-- returns the title of a dialog mask
--
-- Parameter:
-- csToken    the dialog token
--
-- Return
-- the dialog title
--
-- ***************************************************************************************************

FUNCTION getContext RETURN VARCHAR2;
-- ***************************************************************************************************
-- Date and Autor: 17, May 2011, MCD
-- returns the context of a reporttype/srnumber/title node
--
-- Return
-- the context
--
-- ***************************************************************************************************

FUNCTION isNumeric ( cnCheckString  VARCHAR2 DEFAULT NULL )
  RETURN CHAR;
-- ***************************************************************************************************
-- Determins if a string contains a number
--
-- Return:
--   T    --> TRUE, a number
--   F    --> FALSE, no number
--   NULL --> NULL was either teststring or an error occurred
--
-- Sample Calls:
--   SELECT NVL(stb$util.isNumeric('0,32E5'),'(null)') FROM dual;
--   SELECT NVL(stb$util.isNumeric('1122,3344'),'(null)') FROM dual;
--   SELECT NVL(stb$util.isNumeric('987 654'),'(null)') FROM dual;
--   SELECT NVL(stb$util.isNumeric('abcd'),'(null)') FROM dual;
--   SELECT NVL(stb$util.isNumeric('0'),'(null)') FROM dual;
--   SELECT NVL(stb$util.isNumeric(' '),'(null)') FROM dual;
--   SELECT NVL(stb$util.isNumeric(''),'(null)') FROM dual;
--   SELECT NVL(stb$util.isNumeric('$123456'),'(null)') FROM dual;
-- ***************************************************************************************************

FUNCTION strtokenize (stotokenize IN VARCHAR2, sdelimiter IN VARCHAR2, ltokensarray OUT stb$typedef.tlstrings) RETURN stb$oerror$record;
-- ***************************************************************************************************
-- Date and Autor: 06, Aug 2012, MCD
-- tokenize a string into small strings depending on the defined delimiter
--
-- Parameter:
-- stotokenize    the string to tokenize
-- sdelimiter     the delimiter
-- ltokensarray   the single strings in an out-coming list
--
-- Return
-- STB$OERROR$RECORD
--
-- ***************************************************************************************************

FUNCTION checkMethodExists (sMethod IN VARCHAR2, sPackage IN VARCHAR2 default null ) RETURN VARCHAR2 RESULT_CACHE;

FUNCTION checkObjectExists (sMethod IN VARCHAR2, sPackage IN VARCHAR2) RETURN VARCHAR2 RESULT_CACHE;
FUNCTION setClientcontext(v_attribute varchar2,v_value varchar2 default null)  return boolean;
-- ***************************************************************************************************
--The function allows you to create custom attributes and assign values to them for the context of the session "Clientcontext".
--Parameter:
--v_attribute     Attribute name in Clientcontext
--v_value         Attribute value

---- ***************************************************************************************************
FUNCTION getClientcontext(v_attribute varchar2)   RETURN VARCHAR2;
-- ***************************************************************************************************
--The function  retrieves the set attribute(Attribute value); otherwise, it returns NULL
--Parameter:
--v_attribute     Attribute name in Clientcontext

---- ***************************************************************************************************


END Stb$util;