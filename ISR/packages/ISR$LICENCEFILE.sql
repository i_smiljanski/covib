CREATE OR REPLACE PACKAGE ISR$LICENCEFILE AS
-- ***************************************************************************************************
-- Description/Usage:
-- the package holds the licence aggrements ofthe iStudyReporter. The number of concurent licenses that
-- can be used and the lifetime of the licence key. If the number of licences is exceeded it is possible 
-- to connect to iStudyreproter in a secure mode and to terminate running sessions. 
-- The package is delivered as wrapped  PL-SQL code  
-- ***************************************************************************************************

FUNCTION checkSessionAllowed RETURN STB$OERROR$RECORD;
-- ***************************************************************************************************
-- Date and Autor: 09.Oct 2007, MCD
-- function checks if the session is permittd to log on to iStudyReporter due to the licence agggrement
--
-- Return:
-- STB$OERROR$RECORD
--
-- ***************************************************************************************************

FUNCTION getMode RETURN VARCHAR2;
-- ***************************************************************************************************
-- Date and Autor: 09.Oct 2007, MCD
-- function returns the id of the secure mode 
--
-- Return:
-- Secure Mode 
--
-- ***************************************************************************************************

FUNCTION isLicensed(csModule IN VARCHAR2) RETURN VARCHAR2;
-- ***************************************************************************************************
-- Date and Autor: 16.May 2011, MCD
-- function checks if a module is licensed
--
-- Return:
-- 'T' or 'F'
--
-- ***************************************************************************************************

FUNCTION getLicensedModules RETURN VARCHAR2;
-- ***************************************************************************************************
-- Date and Autor: 16.May 2011, MCD
-- function return the list of licensed modules with description
--
-- Return:
-- licensed list
--
-- ***************************************************************************************************

END ISR$LICENCEFILE;