CREATE OR REPLACE PROCEDURE changePassword(sPassword in varchar2, soldPassword in varchar2)
AUTHID CURRENT_USER
IS
  sCurrentName    varchar2(100)      := 'changePassword';
  sSql            VARCHAR2(4000);
  oErrorObj       stb$oerror$record  := STB$OERROR$RECORD();
  sUserMsg        varchar2(4000);  
BEGIN

  sSql := 'alter user '||user||' identified by '||sPassword||' replace '||sOldPassword;
  ISR$TRACE.debug('statement',sSql,sCurrentName,sSql);
  
  EXECUTE IMMEDIATE sSql;

EXCEPTION WHEN OTHERS THEN
  -- STB$TRACE.setLog(SUBSTR(SQLCODE || ' ' || SQLERRM, 0, 200), 'changePassword', SUBSTR(DBMS_UTILITY.format_error_backtrace,0,4000),-1);
  sUserMsg := utd$msglib.getmsg ('exUnexpectedError', stb$security.getCurrentLanguage, sUserMsg, SQLERRM (SQLCODE));
  oErrorObj.handleError (Stb$typedef.cnSeverityCritical,
                         sUserMsg,
                         sCurrentName,
                         DBMS_UTILITY.format_error_stack || stb$typedef.crlf || DBMS_UTILITY.format_error_backtrace);  
  
  
  RAISE;
END changePassword;