CREATE OR REPLACE procedure CREATETMPTABLES_ALL is
  sDummy varchar2(1);
begin
  -- vs [ISRC-746] Usage for development, quick creation of temporary tables
  -- be aware, that a temp-table can only be recreated, if it is not locked in another session (e.g. iStudyReporter)
  sDummy := createtemptables_all;
  
  -- compile invalid schema objects
 DBMS_UTILITY.COMPILE_SCHEMA( schema      =>  sys_context('USERENV','CURRENT_SCHEMA'),
                              compile_all => FALSE );
END CREATETMPTABLES_ALL;