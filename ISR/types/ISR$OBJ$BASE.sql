CREATE OR REPLACE TYPE ISR$OBJ$BASE FORCE AS OBJECT (
-- this object is used as abstract template for all another objects 
-- ***************************************************************************************************
sJavaLogLevel varchar2(32),
nCurrentLanguage number,
constructor function ISR$OBJ$BASE(self in out nocopy ISR$OBJ$BASE) return self as result,
  -- constructor for empty initialisation
static  procedure destroyOldValues,
-- ***************************************************************************************************
-- Date and Autor: 17 Oct 2014 IS
-- destroys the old values of the node object when a new node is set in the Explorer
--
      
-- ***************************************************************************************************
static function getLov( sEntity        in varchar2,
                 sSuchwert      in varchar2, 
                 oSelectionList out ISR$TLRSELECTION$LIST ) return STB$OERROR$RECORD,
-- ***************************************************************************************************
-- Date und Autor: 17 Oct 2014 IS
-- returns the a list of values of an data entity
--
-- Parameter:
-- sEntity            the entity to which the list of values belongs to
-- sSuchwert          the search string to filter / limit the list to certain values
-- oSelectionList     the list of values  belong to the passed entity
--
-- Return:
-- STB$OERROR$RECORD    
--
-- ***************************************************************************************************

static function setMenuAccess( sParameter  in  varchar2, 
                       sMenuAllowed out varchar2 )  return STB$OERROR$RECORD,
-- ***************************************************************************************************
-- Date and Autor:  17 Oct 2014 IS
-- checks if a command token is selectable in the context menu. If a command token is
-- selectable depends on the context of the tree node and the granted system privileges 
-- of the authorization groups the user belongs to
--
-- Parameter:
-- sParameter            the command token of the context menu
-- sMenuAllowed          flag to indicate if the context command token is selectable in the context menu
--
-- Return:
-- STB$OERROR$RECORD    
--
-- ***************************************************************************************************

static function getContextMenu( nMenu           in  number,  
                         olMenuEntryList out STB$MENUENTRY$LIST ) return STB$OERROR$RECORD,
-- ***************************************************************************************************
-- Date und Autor: 17 Oct 2014 IS
-- returns the context menu of a selected tree node in the iStudyReporter Explorer ,
-- if a menu entry is passed into the function, the returned information will be the sub menu
-- belonging to the passed menu entry
--
-- Parameter:
-- nMenu              optional identifer of the "parent" menu entry
-- olMenuEntryList     the context menu of the current node or the submenu for the provided menu entry
--
-- Return:
-- STB$OERROR$RECORD    
--
-- ***************************************************************************************************

static function getXML( oParameter in  STB$MENUENTRY$RECORD, 
                 clXML      out clob, 
                 clParamXml in  clob  default null ) return STB$OERROR$RECORD,
-- ***************************************************************************************************
-- Date und Autor: 16. May 2011, MCD
-- retrieves an xml file with instructions
--
-- Parameters:
-- oParameter   the parameter
-- clXML        the xml file
-- clParamXml   the xml file with needed parameters, default null
--
-- Return:
-- STB$OERROR$RECORD  
--
-- 
static function getNodeDetails(  clXML      out clob)  RETURN STB$OERROR$RECORD,
-- ***************************************************************************************************
-- Date und Autor: 24. Jun 2015, IS
-- retrieves a node details as xml
--
-- Parameters:
-- clXML        the xml file
--
-- Return:
-- STB$OERROR$RECORD  
--
-- ***************************************************************************************************


static function setCurrentNode( oSelectedNode in STB$TREENODE$RECORD ) return STB$OERROR$RECORD,
-- ***************************************************************************************************
-- Date und Autor: 17 Oct 2014 IS
-- sets the information on a tree node in the package STB$OBJECT 
--
-- Parameter:
-- oSelectedNode       the node object describing the selected iStudyReporter Explorer node
--
-- Return:
-- STB$OERROR$RECORD    
--
-- ***************************************************************************************************

static function getAuditMask (olnodelist OUT stb$treenodelist)
  return stb$oerror$record,               
member function getAuditType return STB$OERROR$RECORD,
static procedure setLovListSelection (ssuchwert  in varchar2, oSelectionList in out isr$tlrselection$list),
static function getSelectedLovDisplay(csParameter in varchar2, csValue in varchar2, csPackage in varchar2 default 'ISR$OBJ$BASE')
  return varchar2  
) not final