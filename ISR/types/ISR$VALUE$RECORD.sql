CREATE OR REPLACE TYPE ISR$VALUE$RECORD FORCE AS OBJECT (sParametername  VARCHAR2 (500),
                                 sParametervalue VARCHAR2 (4000),
                                 sNodetype       VARCHAR2 (1024),
                                 sNodeId         VARCHAR2 (1024),
                                 sNodePackage    VARCHAR2 (500),
  constructor function ISR$VALUE$RECORD(self in out nocopy ISR$VALUE$RECORD, 
                                        sParametername  varchar2 default null,
                                        sParametervalue varchar2 default null,
                                        sNodetype       varchar2 default null,
                                        sNodeId         varchar2 default null,                                             
                                        sNodePackage    varchar2 default null
                                        ) return self as result
) 