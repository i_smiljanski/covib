CREATE OR REPLACE TYPE ISR$ATTRIBUTE$RECORD force AS OBJECT(
   sParameter VARCHAR2 (500),
   sValue VARCHAR2 (500),
   sPrepresent VARCHAR2 (1),
   bBlobValue BLOB,
   CONSTRUCTOR FUNCTION ISR$ATTRIBUTE$RECORD (sParameter     VARCHAR2 default null,
                                              sValue         VARCHAR2 default null,
                                              sPrepresent    VARCHAR2 default null)
      RETURN SELF AS RESULT
) 