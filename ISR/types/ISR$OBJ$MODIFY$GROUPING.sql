CREATE OR REPLACE type isr$obj$modify$grouping force under isr$obj$dialog( 
  constructor function ISR$OBJ$MODIFY$GROUPING(self in out nocopy ISR$OBJ$MODIFY$GROUPING) return self as result,
  static function putParameters(oParameter in stb$menuentry$record, olParameter in stb$property$list, clparameter in clob default null) 
  return stb$oerror$record,
-- ***************************************************************************************************
-- Date und Autor: 06 Dec 2016  is
-- processing of grid tables: writes the data from xXml to table 'sTable'
--
-- Parameter:
-- csToken  Token
-- coAddParamList  additional, token-specific parameters ('TABLE', 'SYSTEMAUDIT', ..)
--
-- Return:
--   varchar2   
--
-- Example XML:
 /*
<?xml version="1.0" encoding="UTF-8"?>
<DOCUMENT>
	<UPDATE count="16">
		<ROW>
			<ENTITY>ITERATION</ENTITY>
			<DESCRIPTION>Iteration for Oracle jobs</DESCRIPTION>
			<ROWID>AAATy+AAKAAAAJrAAP</ROWID>
		</ROW>
		..
	</UPDATE>	
    <DELETE count="1">
        <ROW>
            <ENTITY>test</ENTITY>
            <DESCRIPTION>test</DESCRIPTION>
            <ROWID>AAATy+AAKAAAAJsAAA</ROWID>
        </ROW>
    </DELETE>
</DOCUMENT>
*/

-- ***************************************************************************************************
  static function callFunction (oParameter in STB$MENUENTRY$RECORD, olNodeList out STB$TREENODELIST)
  return STB$OERROR$RECORD
) not final