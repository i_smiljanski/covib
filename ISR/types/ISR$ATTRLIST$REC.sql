CREATE OR REPLACE TYPE iSR$AttrList$Rec as object (lAttributes iSR$Attribute$List,
  constructor function iSR$AttrList$Rec(self in out nocopy iSR$AttrList$Rec) return self as result,
  constructor function iSR$AttrList$Rec(self in out nocopy iSR$AttrList$Rec, lAttributes in iSR$Attribute$List) return self as result,
  member function GetAttrRec ( csParameter in varchar2 ) return iSR$Attribute$Record,
  member function GetAttrValue ( csParameter in varchar2 ) return varchar2
) not final 