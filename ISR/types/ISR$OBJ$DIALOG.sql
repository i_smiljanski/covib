CREATE OR REPLACE type isr$obj$dialog force under isr$obj$visible(

static function putParameters(oparameter in stb$menuentry$record, olparameter in stb$property$list, clparameter in clob default null) 
  return stb$oerror$record
-- ***************************************************************************************************
-- Date und Autor: 02 Feb 2006  JB
-- is the counterpart function of callFunction , using the putParameter a
-- command token is procssed from a called dialog window.The entered information (e.g. esig , filter )
-- whes considered when the command token is processed
--
-- Parameter:
-- oParameter      the command token selected in the context menu
-- olParameter     object list holding all information on the fields of the pop-up mask (Object type has the
--                 attributes sParameter, sPromptDisplay,sValue,sDisplay,sEditable,sRequired,sDisplayed,
--                 hasLov,type,format)
--
-- Return:
-- STB$OERROR$RECORD    
--
-- ***************************************************************************************************
)  not final;