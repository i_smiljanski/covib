CREATE OR REPLACE type ISR$SERVER$RECORD force under isr$obj$dialog( 
static function putParameters(oparameter in stb$menuentry$record, olparameter in stb$property$list, clparameter in clob default null) 
  return stb$oerror$record,
-- ***************************************************************************************************
-- Date und Autor: 17 Oct 2014 IS
-- is the counterpart function of callFunction , using the putParameter a
-- command token is procssed from a called dialog window.The entered information (e.g. esig , filter )
-- whes considered when the command token is processed
--
-- Parameter:
-- oParameter      the command token selected in the context menu
-- olParameter     object list holding all information on the fields of the pop-up mask (Object type has the
--                 attributes sParameter, sPromptDisplay,sValue,sDisplay,sEditable,sRequired,sDisplayed,
--                 hasLov,type,format)
--
-- Return:
-- STB$OERROR$RECORD    
--
-- ***************************************************************************************************
static function getCheckForDialog return varchar2,
static procedure setCheckForDialog (csCheckForDialog in varchar2),
static function checkDependOnMaskForTable( oParameter    in     STB$MENUENTRY$RECORD, 
                                  olParameter   in out STB$PROPERTY$LIST, 
                                  sModifiedFlag out    varchar2, 
                                  sTable    in   varchar2  default 'ISR$SERVER' ) return STB$OERROR$RECORD,                                  
-- ***************************************************************************************************
-- Date und Autor: 17 Oct 2014 IS
-- calls the customer package to check if there are data dependencies on a 
-- dialog window 
--
-- Parameter :
-- oParameter         the menu entry
-- olParameter        the selected parameters
-- sModifiedFlag      checks if the property list changes
--
-- Return:
-- STB$OERROR$RECORD  
--
-- ***************************************************************************************************


static function callFunction( oParameter in STB$MENUENTRY$RECORD, 
                       olNodeList out STB$TREENODELIST, sTable    in   varchar2 )  return STB$OERROR$RECORD,                       
static function callFunction( oParameter in STB$MENUENTRY$RECORD, 
                       olNodeList out STB$TREENODELIST )  return STB$OERROR$RECORD,                       
-- ***************************************************************************************************
-- Date und Autor: 17 Oct 2014 IS
-- is the interface to process a command token selected in the context menu.
-- The function checks if the command token can be executed and calls the backend functionality
-- to process the token. If further information is required to process the command token,
-- a node object  is returned and displayed as dialog window
--
-- Parameter:
-- oParameter            the command token selected in the context menu
-- olNodeList            a node list describing a pop mask where further information can be  entered asked
--                       to process the command token
--
-- Return:
-- STB$OERROR$RECORD    
--
-- ***************************************************************************************************

static function getLov( sEntity        in varchar2,
                 sSuchwert      in varchar2, 
                 oSelectionList out ISR$TLRSELECTION$LIST ) return STB$OERROR$RECORD,
-- ***************************************************************************************************
-- Date und Autor: 17 Oct 2014 IS
-- returns the a list of values of an data entity
--
-- Parameter:
-- sEntity            the entity to which the list of values belongs to
-- sSuchwert          the search string to filter / limit the list to certain values
-- oSelectionList     the list of values  belong to the passed entity
--
-- Return:
-- STB$OERROR$RECORD    
--
-- ***************************************************************************************************

static function setCurrentNode( oSelectedNode in STB$TREENODE$RECORD ) return STB$OERROR$RECORD,
-- ***************************************************************************************************
-- Date und Autor: 17 Oct 2014 IS
-- sets the information on a tree node in the package STB$OBJECT 
--
-- Parameter:
-- oSelectedNode       the node object describing the selected iStudyReporter Explorer node
--
-- Return:
-- STB$OERROR$RECORD    
--
-- ***************************************************************************************************

static function setMenuAccess( sParameter  in  varchar2, 
                       sMenuAllowed out varchar2 )  return STB$OERROR$RECORD,
-- ***************************************************************************************************
-- Date and Autor:  17 Oct 2014 IS
-- checks if a command token is selectable in the context menu. If a command token is
-- selectable depends on the context of the tree node and the granted system privileges 
-- of the authorization groups the user belongs to
--
-- Parameter:
-- sParameter            the command token of the context menu
-- sMenuAllowed          flag to indicate if the context command token is selectable in the context menu
--
-- Return:
-- STB$OERROR$RECORD    
--
-- ***************************************************************************************************

static function getNodeDetails(  clXML      out clob)  RETURN STB$OERROR$RECORD,
-- ***************************************************************************************************
-- Date und Autor: 24. Jun 2015, IS
-- retrieves a node details as xml
--
-- Parameters:
-- clXML        the xml file
--
-- Return:
-- STB$OERROR$RECORD  
--
-- ***************************************************************************************************
static function GetNodeChilds ( oSelectedNode in  STB$TREENODE$RECORD,
                         olNodeList    out STB$TREENODELIST ) return STB$OERROR$RECORD ,           
static function GetNodeList ( oSelectedNode in  STB$TREENODE$RECORD, 
                       olNodeList    out STB$TREENODELIST )  return STB$OERROR$RECORD,          
static function getServerStatus(csObserverHost IN VARCHAR2, csObserverPort IN VARCHAR2, csContext IN VARCHAR2, csNamespace IN VARCHAR2,  
                                      csLoglevel  IN VARCHAR2, csLogReference  IN VARCHAR2, nTimeout NUMBER) RETURN CLOB
) not final