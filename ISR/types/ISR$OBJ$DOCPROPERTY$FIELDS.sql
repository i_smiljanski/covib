CREATE OR REPLACE TYPE isr$obj$docproperty$fields under isr$obj$grid (
constructor function ISR$OBJ$DOCPROPERTY$FIELDS(self in out nocopy ISR$OBJ$DOCPROPERTY$FIELDS) return self as result,
static function putParameters(oParameter in stb$menuentry$record, olParameter in stb$property$list, clParameter in clob default null)
  return stb$oerror$record,
static function callFunction (oParameter in STB$MENUENTRY$RECORD, olNodeList out STB$TREENODELIST)
  return STB$OERROR$RECORD
)