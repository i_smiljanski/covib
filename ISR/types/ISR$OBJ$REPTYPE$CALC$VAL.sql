CREATE OR REPLACE type                   ISR$OBJ$REPTYPE$CALC$VAL under isr$obj$grid(
static function putParameters(oParameter in stb$menuentry$record, olParameter in stb$property$list, clParameter in clob default null)
  return stb$oerror$record,
static function callFunction (oParameter in STB$MENUENTRY$RECORD, olNodeList out STB$TREENODELIST)
  return STB$OERROR$RECORD,
static function setMenuAccess(sParameter   in  varchar2, sMenuAllowed out varchar2 )
    return STB$OERROR$RECORD
)
  not final