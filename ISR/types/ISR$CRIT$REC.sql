CREATE OR REPLACE TYPE iSR$Crit$Rec as object (Key         varchar2(500),
                                               MasterKey   varchar2(500),
                                               Display     varchar2(500),                                            
                                               Info        varchar2(2000),                          
                                               OrderNumber number,                                               
-- the object is used to hold info from iSR$Crit 
-- ***************************************************************************************************
-- Modification history:
-- Ver   Date              Autor    System             Project                            Remarks
-- 1.0   10 Jun  2008      HR       iStudyReporter    iStudyReporter@watson               Creation
-- ***************************************************************************************************                                               
  constructor function iSR$Crit$Rec(self in out nocopy iSR$Crit$Rec) return self as result,
  -- constructor for empty initialisation
  constructor function iSR$Crit$Rec(self in out nocopy iSR$Crit$Rec, Key in varchar2, MasterKey in varchar2, Display in varchar2, Info in varchar2, OrderNumber in number) return self as result,
  -- overwrites the default contructor with not null values
  map member function MapCrit return varchar2
  -- map member function used for select distinct
) not final