CREATE OR REPLACE type iSR$Parameter$Rec force as object(
  sParameter varchar2 (500),
  aValue      sys.anydata,
  constructor function iSR$Parameter$Rec(sParameter in varchar2, aValue in  sys.anydata) return self as result,
  -- replaces the default constructor
  constructor function iSR$Parameter$Rec(sParameter in varchar2, nValue in number) return self as result,
  -- replaces the default constructor and accepts a number value
  constructor function iSR$Parameter$Rec(sParameter in varchar2, sValue in varchar2) return self as result,
  -- replaces the default constructor and accepts a string value
  constructor function iSR$Parameter$Rec(sParameter in varchar2, dValue in date) return self as result
  -- replaces the default constructor and accepts a date value
)