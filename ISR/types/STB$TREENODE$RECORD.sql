CREATE OR REPLACE TYPE STB$TREENODE$RECORD FORCE AS OBJECT (SDISPLAY        VARCHAR2(255),
                                    SPACKAGETYPE    VARCHAR2(255),
                                    SNODETYPE       VARCHAR2(255),
                                    SNODEID         VARCHAR2(4000),
                                    SICON           VARCHAR2(500),
                                    SDEFAULTACTION  VARCHAR2(255),
                                    TLOPROPERTYLIST STB$PROPERTY$LIST,
                                    TLOVALUELIST    ISR$VALUE$LIST,
                                    nDisplayType    NUMBER,
                                    clPropertyList  CLOB,
                                    STOOLTIP        VARCHAR2(4000),
  constructor function STB$TREENODE$RECORD(self in out nocopy STB$TREENODE$RECORD, 
                                           sDisplay          VARCHAR2 DEFAULT null,
                                           sPackageType      VARCHAR2 DEFAULT null,
                                           sNodeType         VARCHAR2 DEFAULT null,
                                           sNodeId           VARCHAR2 DEFAULT null,
                                           sIcon             VARCHAR2 DEFAULT null,
                                           sDefaultAction    VARCHAR2 DEFAULT null,
                                           sTooltip          VARCHAR2 DEFAULT null,
                                           tloPropertyList   STB$PROPERTY$LIST DEFAULT STB$PROPERTY$LIST (),
                                           tloValueList      ISR$VALUE$LIST DEFAULT ISR$VALUE$LIST (),
                                           nDisplayType      NUMBER DEFAULT 1/*, STB$TYPEDEF.cnMenuDialog*/
                                           ) return self as result,

  MEMBER PROCEDURE setDirectory (self IN OUT STB$TREENODE$RECORD, sParameterName IN VARCHAR2 DEFAULT NULL, sParameterValue IN VARCHAR2 DEFAULT NULL),
  -- copied from stb$object at 10.Feb 2014 HR
  
  -- vs 04.Mar.2016
  member function getPropertyParameter(self in STB$TREENODE$RECORD, csParameterName in varchar2, csOutputValue varchar2  default 'SVALUE') return varchar2,

  -- vs 14.Mar.2016
  member function getValueParameter(self in STB$TREENODE$RECORD, csParameterName in varchar2, csOutputValue varchar2  default 'SPARAMETERVALUE') return varchar2
  
)