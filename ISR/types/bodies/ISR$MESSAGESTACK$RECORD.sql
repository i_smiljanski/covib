CREATE OR REPLACE TYPE BODY                   "ISR$MESSAGESTACK$RECORD" as
   constructor function ISR$MESSAGESTACK$RECORD(self in out nocopy ISR$MESSAGESTACK$RECORD,
                                                                             sDevstack    varchar2 default null,
                                                                             sImplstack varchar2 default null,
                                                                             sUserstack varchar2 default null ) return self as result is
   begin
    self.sDevstack := sDevstack;
    self.sImplstack := sImplstack;
    self.sUserstack := sUserstack;
    return;
  end ISR$MESSAGESTACK$RECORD;

--*********************************************************************************************************************************    
member procedure writeStack( sUserstack   in varchar, 
                             sImplemstack in varchar2, 
                             sDevstack    in varchar2) 
is
  sCurrentName  constant varchar2(100) := $$PLSQL_UNIT||'.writeStack';  
begin     
  isr$trace.debug_all('begin end '|| sCurrentName ,'sUserstack: '||self.sUserstack||chr(13)||chr(10)||'sImplstack: '||self.sImplstack||chr(13)||chr(10)||'sDevstack: '||self.sDevstack,sCurrentName);
  --self := stb$object.GetMessageStack;  
  self.sUserstack := case when self.sUserstack is null then sUserstack else self.sUserstack || stb$typedef.crlf  ||   sUserstack end;
  self.sImplstack := case when self.sImplstack is null then sUserstack|| ' (in ' ||sImplemstack || ') ' else self.sImplstack || stb$typedef.crlf  ||  sUserstack || ' (in ' ||sImplemstack || ') ' end;
  self.sDevstack := case when self.sDevstack is null then  sUserstack|| ' (in ' ||sImplemstack || ') ' || ' : '  || sDevstack  else self.sDevstack || stb$typedef.crlf  ||   sUserstack|| ' (in ' ||sImplemstack || ') ' || ' : ' || sDevstack end;
  --stb$object.SetMessageStack(self);
end writeStack;
   
end;