CREATE OR REPLACE TYPE BODY ISR$OBJ$BASE as
-- ***************************************************************************************************
constructor function ISR$OBJ$BASE(self in out nocopy ISR$OBJ$BASE) return self as result is
begin
  self.sJavaLogLevel := isr$trace.getJavaUserLoglevelStr;
  self.nCurrentLanguage := stb$security.getCurrentLanguage;
  return;
end ISR$OBJ$BASE;

static  procedure destroyOldValues is
   oCurrentParameter  STB$PROPERTY$LIST;
begin
  STB$OBJECT.setCurrentToken( null );
  oCurrentParameter := STB$PROPERTY$LIST ();
end destroyOldValues;

-- ***************************************************************************************************
static function getLov( sEntity        in varchar2,
                 sSuchwert      in varchar2,
                 oSelectionList out ISR$TLRSELECTION$LIST ) return STB$OERROR$RECORD
is
  sCurrentName constant varchar2(100) := $$PLSQL_UNIT||'.getLov(' || sEntity || ')';
  sMsg         varchar2(4000);
  oErrorObj    STB$OERROR$RECORD := STB$OERROR$RECORD();

begin
  isr$trace.stat('sEntity='||sEntity, 'begin', sCurrentName);
  oSelectionList := ISR$TLRSELECTION$LIST ();

  execute immediate
    'BEGIN
       :1 :='|| stb$util.getCurrentCustomPackage || '.getLov(:2,:3,:4);
     END;'
  using out oErrorObj,  in sentity, in ssuchwert, out oselectionlist;

  if sSuchWert is not null and oselectionlist is not null and oselectionlist.first is not null then
    isr$trace.debug ('sSuchWert',  sSuchWert, sCurrentName);
    for i IN 1..oSelectionList.count() loop
      if oSelectionList(i).sValue = sSuchWert then
        isr$trace.debug ('oSelectionList('||i||').sSelected', oSelectionList(i).sValue, sCurrentName);
        oSelectionList(i).sSelected := 'T';
      end if;
    end loop;
  end if;

  isr$trace.stat ('end', 'oSelectionList.count() = ' || oSelectionList.count(), sCurrentName);
  return oErrorObj;
exception
  when others then
    sMsg :=  utd$msglib.getmsg ('exUnexpectedError', stb$security.getcurrentlanguage, csP1 => sqlerrm(sqlcode));
    oErrorObj.handleError ( Stb$typedef.cnSeverityCritical, sMsg, sCurrentName,
                            substr(dbms_utility.format_error_stack || CHR(10) || dbms_utility.format_error_backtrace, 1,4000)  );
    return oErrorObj;
end getLov;

-- ***************************************************************************************************
static function setMenuAccess( sParameter  in  varchar2,
                       sMenuAllowed out varchar2 )  return STB$OERROR$RECORD
is
  sCurrentName constant varchar2(100) := $$PLSQL_UNIT||'.setMenuAccess(' || sParameter || ')';
  oErrorObj    STB$OERROR$RECORD := STB$OERROR$RECORD();
  sMsg        varchar2(4000);

begin
  isr$trace.info_all('begin', 'sParameter: ' || sParameter || ' '  || sMenuAllowed,  sCurrentName);
  /* nb - 3 Jun 16 - allow use of result_cache */
  -- oErrorObj := Stb$security.checkGroupRight (sParameter, sMenuAllowed);
  sMenuAllowed := Stb$security.checkGroupRight (sParameter);
  isr$trace.info_all ('end',  'sMenuAllowed:' || sMenuAllowed, sCurrentName);
  return oErrorObj;
exception
  when others then
    sMsg :=  utd$msglib.getmsg ('exUnexpectedError', stb$security.getcurrentlanguage, csP1 => sqlerrm(sqlcode));
    oErrorObj.handleError ( Stb$typedef.cnSeverityCritical, sMsg, sCurrentName,
                            substr(dbms_utility.format_error_stack || CHR(10) || dbms_utility.format_error_backtrace, 1,4000)  );
    return oErrorObj;
end setMenuAccess;

-- ***************************************************************************************************
static function getContextMenu( nMenu           in  number,
                                olMenuEntryList out STB$MENUENTRY$LIST ) return STB$OERROR$RECORD
is
  sCurrentName constant varchar2(100) := $$PLSQL_UNIT||'.getContextMenu';
  oErrorObj    STB$OERROR$RECORD      := STB$OERROR$RECORD();
  sMsg         varchar2(4000);
begin
  isr$trace.info ('begin', 'begin', sCurrentName);
  oErrorObj := STB$UTIL.getContextMenu (Stb$object.getCurrentNodeType, nMenu, olMenuEntryList);
  isr$trace.info ('end', 'end', sCurrentName);
  return (oErrorObj);
exception
  when others then
    rollback;
    sMsg :=  utd$msglib.getmsg ('exUnexpectedError', stb$security.getcurrentlanguage, csP1 => sqlerrm(sqlcode));
    oErrorObj.handleError ( Stb$typedef.cnSeverityCritical, sMsg, sCurrentName,
                            substr(dbms_utility.format_error_stack || CHR(10) || dbms_utility.format_error_backtrace, 1,4000)  );
    return oErrorObj;
END getContextMenu;

-- ***************************************************************************************************
static function getXML( oParameter in  STB$MENUENTRY$RECORD,
                        clXML      out clob,
                        clParamXml in  clob  default null )
  return STB$OERROR$RECORD
is
  oErrorObj        STB$OERROR$RECORD := STB$OERROR$RECORD();
begin
  null;
  return oErrorObj;
end getXML;


-- ***************************************************************************************************
static function getNodeDetails(  clXML      out clob) return STB$OERROR$RECORD
is
  sCurrentName constant varchar2(100) := $$PLSQL_UNIT||'.getNodeDetails';
  oErrorObj    stb$oerror$record      := STB$OERROR$RECORD();

begin
  null;
  return (oErrorObj);
end getNodeDetails;

--****************************************************************************************************************************
static function setCurrentNode(oSelectedNode in STB$TREENODE$RECORD) return STB$OERROR$RECORD
is
  sCurrentName constant varchar2(100) := $$PLSQL_UNIT||'.setCurrentNode';
  oErrorObj    STB$OERROR$RECORD := STB$OERROR$RECORD();
  sMsg         varchar2(4000);
begin
  Isr$trace.stat('begin', 'begin', sCurrentName, xmltype(oSelectedNode).getClobVal());
  stb$object.destroyoldvalues;
  Stb$object.setCurrentNode (oSelectedNode);
  isr$trace.stat('end','end',sCurrentName);
  return oErrorObj;
exception
  when others then
    sMsg :=  utd$msglib.getmsg ('exUnexpectedError', stb$security.getcurrentlanguage,  sqlerrm(sqlcode));
    oErrorObj.handleError ( Stb$typedef.cnSeverityCritical, sMsg, sCurrentName,
                            dbms_utility.format_error_stack || CHR(10) || dbms_utility.format_error_backtrace  );
    return oErrorObj;
end setCurrentNode;


--******************************************************************************
static function getAuditMask (olnodelist OUT stb$treenodelist)
  return stb$oerror$record
IS
  sCurrentName                  CONSTANT VARCHAR2(100) := $$PLSQL_UNIT||'.getAuditMask('||null||')';
  oErrorObj                     stb$oerror$record := STB$OERROR$RECORD();
  sUserMsg                      varchar2(4000);
BEGIN
  isr$trace.stat('begin','begin', sCurrentName);
  oErrorObj := stb$audit.getauditmask (olnodelist);
  isr$trace.stat('end','end', sCurrentName);
  RETURN (oErrorObj);
EXCEPTION
  WHEN OTHERS THEN
    ROLLBACK;
    sUserMsg := utd$msglib.getmsg ('exUnexpectedError', stb$security.getCurrentLanguage,  csP1 => sqlerrm(sqlcode));
    oErrorObj.handleError ( Stb$typedef.cnSeverityCritical, sUserMsg, sCurrentName, dbms_utility.format_error_stack||stb$typedef.crlf||dbms_utility.format_error_backtrace );

    RETURN (oErrorObj);
end getAuditMask;

--******************************************************************************
member function getAuditType
  return STB$OERROR$RECORD
is
    oErrorObj                     STB$OERROR$RECORD := STB$OERROR$RECORD();
    exAudit                       exception;
    sCurrentName      constant varchar2(100) := $$PLSQL_UNIT||'.getAuditType';
    sMsg       varchar2(2000);
begin
    isr$trace.stat('begin','begin',sCurrentName);

    if Stb$util.checkPromptedAuditRequired ('PROMPTED_REP_TYPE_AUDIT') = 'T' and STB$OBJECT.getCurrentReporttypeId is not null
    or Stb$util.checkPromptedAuditRequired ('PROMPTED_SYSTEM_AUDIT') = 'T' then
      raise exAudit;
    -- commit will be in the audit window
    else
      -- Silent Audit
      -- write the silent audit and calculate the checksum
      oErrorObj := Stb$audit.silentAudit;
      -- commit the audit
      commit;
    end if;

    isr$trace.stat('end','end',sCurrentName);
    return oErrorObj;
  exception
    when exAudit then
       oErrorObj.handleError ( Stb$typedef.cnAudit, '', sCurrentName,
                                     'Errorcode = '||Utd$msglib.GetMsg ('exAudit', Stb$security.GetCurrentLanguage)||
                                     ', Errormessage = '||'Call the mask for the audit window'||CHR(10)||SQLERRM||
                                     ' '||DBMS_UTILITY.format_error_backtrace);
      isr$trace.stat('end','end',sCurrentName);
      return oErrorObj;
    when others then
       oErrorObj.handleError ( Stb$typedef.cnseverityWarning, '', sCurrentName,
                                     'Errorcode = '||SQLCODE||
                                     ', Errormessage = '||SQLERRM||
                                     ' '||DBMS_UTILITY.format_error_backtrace);
      return oErrorObj;
end getAuditType;

--******************************************************************************
  static procedure setLovListSelection (ssuchwert  in varchar2, oSelectionList in out isr$tlrselection$list)
is
  sCurrentName                  constant varchar2(100) := $$PLSQL_UNIT||'.setLovListSelection('||ssuchwert||')';
  oErrorObj                     stb$oerror$record := STB$OERROR$RECORD();

begin
  isr$trace.stat('begin','ssuchwert =' || ssuchwert, sCurrentName);

  if sSuchWert is not null and oselectionlist.FIRST is not null then
    isr$trace.info ('sSuchWert', sSuchWert, sCurrentName);
    for i in 1..oSelectionList.COUNT() loop
      if oSelectionList(i).sValue = sSuchWert then
        isr$trace.debug ('oSelectionList('||i||').sSelected', oSelectionList(i).sValue, sCurrentName);
        oSelectionList(i).sSelected := 'T';
      end if;
    end loop;
  end if;

  isr$trace.debug('oSelectionList','s. logclob', sCurrentName, oSelectionList);
  isr$trace.stat('end','end', sCurrentName);
exception
    when others then
    oErrorObj.handleError ( Stb$typedef.cnSeverityCritical, SQLERRM, sCurrentName,
                           DBMS_UTILITY.format_error_stack || CHR(10) || DBMS_UTILITY.format_error_backtrace);
end   setLovListSelection;


--*********************************************************************************************************************************
static function getSelectedLovDisplay(csParameter in varchar2, csValue in varchar2, csPackage in varchar2 default 'ISR$OBJ$BASE')
  return varchar2
IS
  sCurrentName                  CONSTANT VARCHAR2(100) := $$PLSQL_UNIT||'.getSelectedLovDisplay('||csParameter||')';
  PRAGMA AUTONOMOUS_TRANSACTION;
  oErrorObj         STB$OERROR$RECORD := STB$OERROR$RECORD(NULL,NULL,Stb$typedef.cnNoError);

  oSelectionList    ISR$TLRSELECTION$LIST := ISR$TLRSELECTION$LIST();
  sDisplay          VARCHAR2(4000);

  CURSOR cGetTempLovValue IS
    SELECT display
      FROM tmp$lov$value
     WHERE parameter = csParameter
       AND VALUE = csValue
       AND package = csPackage;
BEGIN
  isr$trace.stat('begin','csParameter: '||csParameter||',  csValue: '||csValue||',  csPackage: '||csPackage, sCurrentName);

  IF TRIM(csValue) IS NOT NULL THEN

    OPEN cGetTempLovValue;
    FETCH cGetTempLovValue INTO sDisplay;
    CLOSE cGetTempLovValue;

    isr$trace.debug('display from backup', sDisplay,sCurrentName);

    IF sDisplay IS NULL THEN

      EXECUTE IMMEDIATE
      'BEGIN
        :1 := '||csPackage||'.getLov (:2, :3, :4);
       END;' USING OUT oErrorObj, IN csParameter, IN csValue, OUT oSelectionList;
      SELECT DISTINCT MIN(sDisplay)
        INTO sDisplay
        FROM table (oSelectionList)
       WHERE sSelected = 'T';

      INSERT INTO TMP$LOV$VALUE VALUES(csParameter, csValue, csPackage, sDisplay);
      COMMIT;
      isr$trace.debug('display from list', sDisplay,sCurrentName);

    END IF;

  END IF;

  isr$trace.stat('end','end', sCurrentName);
  RETURN sDisplay;
EXCEPTION
  WHEN OTHERS THEN
    oErrorObj.handleError ( Stb$typedef.cnSeverityCritical, SQLERRM, sCurrentName,
                           DBMS_UTILITY.format_error_stack || CHR(10) || DBMS_UTILITY.format_error_backtrace);
    RETURN sDisplay;
END getSelectedLovDisplay;

end;