CREATE OR REPLACE TYPE BODY STB$OERROR$RECORD as

--********************************************************************************************************************************
constructor function STB$OERROR$RECORD(self in out nocopy STB$OERROR$RECORD, 
                                       sErrorCode     in varchar2 default null,
                                       sErrorMessage  in varchar2 default null,
                                       sSeverityCode  in integer default null ,
                                       tloMessageList in ISR$MESSAGE$LIST default null) return self as result is
  oCurrentErrObj STB$OERROR$RECORD;             
  sCurrentProc constant varchar2(100) := $$PLSQL_UNIT||'.constructor';                           
begin
  self.sErrorCode := sErrorCode;
  self.sErrorMessage := sErrorMessage;
  self.sSeverityCode := nvl(sSeverityCode, 0); 
  -- get the error stack from the "backup"
  oCurrentErrObj := iSR$ErrorObj$Accessor.GetCurrentErrorObj;  
  --self.sSeverityCode := nvl(sSeverityCode, nvl(oCurrentErrObj.sSeverityCode, 0));  IS: unfortunately it doesn't work s.[ISRC-695]
  if tloMessageList is null then       
    -- the constructor is called with a null (uninitialized) message list parameter
    -- this occures within the normal flow of the application  
    -- recycle the session message stack from iSR$ErrorObj$Accessor
    self.tloMessageList := oCurrentErrObj.tloMessageList;
  else       
    -- the constructor is called with an explicit message stack
    -- this occures at start points of the application flow in the GAL or Job 
    --   when the error object constructor passes the collection constructor for the message stack to reset it to 0 elements
    --   or when the iSR$ErrorObj$Accessor package body is initialized in the session
    self.tloMessageList := tloMessageList;
  end if;
  iSR$ErrorObj$Accessor.SetCurrentErrorObj(self);
  return;
end STB$OERROR$RECORD;    

--********************************************************************************************************************************
member procedure  setSeverityCode ( csSeveritycode in integer) is
  sCurrentProc     constant varchar2(100) := $$PLSQL_UNIT||'.setSeverityCode';    
  sOldSeverityCode          integer       := self.sSeverityCode;
begin
  self.sSeverityCode := csSeveritycode;
  isr$trace.info_all('severity change', 'severity changed from '||sOldSeverityCode||' to '||self.sSeverityCode, sCurrentProc);
end setSeverityCode;

--********************************************************************************************************************************
member procedure writeLog( sLogLevel      in varchar2,
                           sActionTaken   in varchar2  default null,
                           sLogEntry      in varchar2,
                           sFunctionGroup in varchar2,
                           clClob         in clob      default EMPTY_CLOB()  ) is
begin
  case sLoglevel
    when 'FATAL' then
      isr$trace.fatal(sActionTaken, sLogEntry, sFunctionGroup, clClob);
    when 'INFO' then
      isr$trace.info(sActionTaken, sLogEntry, sFunctionGroup, clClob);
    when 'INFO_ALL' then
      isr$trace.info_all(sActionTaken, sLogEntry, sFunctionGroup, clClob);
    when 'ERROR' then
      isr$trace.error(sActionTaken, sLogEntry, sFunctionGroup, clClob);
    when 'WARN' then
      isr$trace.warn(sActionTaken, sLogEntry, sFunctionGroup, clClob);
    when 'DEBUG' then
      isr$trace.debug(sActionTaken, sLogEntry, sFunctionGroup, clClob);
    else
      -- default for else is 'ERROR'
      isr$trace.error(sActionTaken, sLogEntry, sFunctionGroup, clClob);
  end case;    
end writeLog;

--********************************************************************************************************************************
member function checkWriteActionLog return boolean is
  bWriteActionlog boolean := false;
begin
  -- Write actionlog, for severities fatal or critical     
  if  self.sSeverityCode in (Stb$typedef.cnFatal, Stb$typedef.cnSeverityCritical)  then
    bWriteActionlog := true;
  end if;

  return bWriteActionlog; 
end checkWriteActionLog;

--********************************************************************************************************************************
member procedure  handleError ( csSeveritycode in integer, 
                                csUserInfo     in varchar2, 
                                csImplInfo     in varchar2, 
                                csDevInfo      in varchar2, 
                                csErrorCode    in varchar2 default null ) is

  sCurrentProc constant varchar2(100) := $$PLSQL_UNIT||'.handleError';
  xListAsType           xmltype;
  sClobText             clob;
  sLogLevel             isr$loglevel.loglevel%type;
  
  procedure writeStack(csUserInfo in varchar, csImplInfo in varchar2, csDevInfo in varchar2) is
    -- adds a message element to the message list tloMessageList
    -- parameters:    
    --    csUserInfo      the Information for the User - the message for the user
    --    csImplInfo      the Message place, (procedure name), NOT the message for the implementor. 
    --    csDevInfo       additional info for developer used to build the developer Message along with the other params  
    nLast     pls_integer;    
  begin
    tloMessageList.extend();
    nLast := self.tloMessageList.last();
    -- limit the resulting Dev Info to 4000 characters to avoid later errors 
    self.tloMessageList(nLast) := ISR$MESSAGE$REC(csUserInfo, 
                                             csUserInfo||chr(10)||chr(10)||csImplInfo, 
                                             substr(csUserInfo||chr(10)||chr(10)||csImplInfo||chr(10)||chr(10)||csDevInfo, 1, 4000));
  end writeStack;

begin  
  isr$trace.debug_all('begin','Member procedure handleError started', sCurrentProc);
  isr$trace.debug_all('status csSeveritycode',  'csSeveritycode: ' || csSeveritycode,  sCurrentProc);  
  isr$trace.debug_all('status csUserInfo',  'csUserInfo: ' || csUserInfo,  sCurrentProc);
  isr$trace.debug_all('status csImplInfo',  'csImplInfo: ' || csImplInfo,  sCurrentProc);
  isr$trace.debug_all('status csDevInfo',   'csDevInfo: '  || csDevInfo,   sCurrentProc);
  isr$trace.debug_all('status csErrorCode', 'csErrorCode: '|| csErrorCode, sCurrentProc);  

  -- add the error infos to the message list 
  writeStack(csUserInfo, csImplInfo, csDevInfo);
  isr$trace.debug_all('status tloMessageList.count', 'tloMessageList.count: '|| tloMessageList.count, sCurrentProc);
  
  -- rebuilt the error obj from parameters 
  self.sErrorCode := csErrorCode;
  self.sErrorMessage := csUserInfo;
  self.sSeverityCode := csSeveritycode; 
  -- set accessor to last error    
  iSR$ErrorObj$Accessor.SetCurrentErrorObj(self);

  -- Get loglevel definition
  sLoglevel := GetLoglevelForSeverityCode;
  
  -- write the action log for fatal and critical errors
  if checkWriteActionLog then
    isr$action$log.setActionLog( Utd$msglib.GetMsg('ERROR_PLACE',Stb$security.GetCurrentLanguage)||' '||csImplInfo, csDevInfo );
  end if;
  
  -- Write into ISR$LOG using rules for serverity and loglevel definition
  writeLog(sLoglevel, sLogLevel, self.serrorMessage, sCurrentProc || ' - ' ||tloMessageList(tloMessageList.last()).sDevMsg, ISR$XML.XMLToClob(xmltype(self)));
  
  isr$trace.debug_all('status serrorMessage', 'serrorMessage: '|| self.serrorMessage, sCurrentProc);
  isr$trace.debug_all('end', 'Member procedure handleError finished', sCurrentProc );  
exception 
  when others then
    isr$trace.error('error handling failed', substr(dbms_utility.format_error_stack || CHR(10) || dbms_utility.format_error_backtrace, 1,4000), sCurrentProc, dbms_utility.format_error_stack || CHR(10) || dbms_utility.format_error_backtrace );
  
end handleError;

--********************************************************************************************************************************
member procedure  handleJavaError ( csSeveritycode in integer, 
                                    cxError        in xmltype, 
                                    csOccurredName in varchar2) 
is
  lMessageList  ISR$MESSAGE$LIST := ISR$MESSAGE$LIST();
  sCurrentProc  constant varchar2(100) := $$PLSQL_UNIT||'.handleJavaError(xmltype)';
  xListAsType   xmltype;
  cRef          sys_refcursor;
  clList        clob; 
  sLogLevel     isr$loglevel.loglevel%type; 
begin
  isr$trace.debug_all('begin', 'Member procedure handleJavaError started', sCurrentProc);
  isr$trace.debug_all('status csSeveritycode', 'csSeveritycode:'||csSeveritycode, sCurrentProc);
  
  -- extract the error data from the cxError parameter into a message list variable
 -- COVIB-125 (ORA-64203: Destination buffer too small to hold CLOB data after character set conversion.)
  for rGetMessages in (select sUserMsg, sImplMsg, sDevMsg  
     from xmltable('/MessageExceptionList/MessageException'
                passing cxError
                columns SUSERMSG clob path 'SUSERMSG',
                        SIMPLMSG clob path 'SIMPLMSG',
                        SDEVMSG  clob path 'SDEVMSG')) loop
    lMessageList.EXTEND;
    lMessageList (lMessageList.count()) := 
           ISR$MESSAGE$REC(rGetMessages.sUserMsg,rGetMessages.sImplMsg,rGetMessages.sDevMsg); 
    self.sErrorMessage := self.sErrorMessage || '; ' || rGetMessages.sUserMsg; 
  end loop;               
  -- add the the message list variable to the message list of the error obj 
  -- and repopulate the error object from the passed error infos.        
  self.tloMessageList := self.tloMessageList multiset union lMessageList;
  self.sSeveritycode  := csSeveritycode;
  self.sErrorCode     := sErrorCode;
  
  iSR$ErrorObj$Accessor.SetCurrentErrorObj(self);
  isr$trace.debug_all('status tloMessageList.count', 'tloMessageList.count: '|| self.tloMessageList.count, sCurrentProc);

  -- Get loglevel definition
  sLoglevel := GetLoglevelForSeverityCode;
  
  -- write the action log for critical errors
  if checkWriteActionLog then
    isr$action$log.setActionLog(Utd$msglib.GetMsg('ERROR_PLACE',Stb$security.GetCurrentLanguage)||' '||csOccurredName, 'Java error');
  end if;  
 
  -- Write into ISR$LOG using rules for serverity and loglevel definition
  writeLog(sLoglevel, sLogLevel, self.serrorMessage, sCurrentProc, ISR$XML.XMLToClob(xmltype(self)));
 
  isr$trace.debug_all('status serrorMessage', 'serrorMessage: '|| self.serrorMessage, sCurrentProc);
  isr$trace.debug_all('end', 'Member procedure handleJavaError finished', sCurrentProc );
exception 
  when others then
    isr$trace.error('error handling failed', substr(dbms_utility.format_error_stack || CHR(10) || dbms_utility.format_error_backtrace, 1,4000), sCurrentProc, dbms_utility.format_error_stack || CHR(10) || dbms_utility.format_error_backtrace );
end handleJavaError;

--********************************************************************************************************************************
member procedure  handleJavaError ( csSeveritycode in  integer, 
                                    cclError       in clob, 
                                    csOccurredName in varchar2) 
is
  sCurrentProc  constant varchar2(100) := $$PLSQL_UNIT||'.handleJavaError ( clob )';
  xError                 xmltype;
begin  
  if cclError is not null then
    xError := xmltype(cclError);
  end if;  
  handleJavaError(csSeveritycode, xError, csOccurredName);  
exception 
  when others then
    isr$trace.error('error handling failed', substr(dbms_utility.format_error_stack || CHR(10) || dbms_utility.format_error_backtrace, 1,4000), sCurrentProc, dbms_utility.format_error_stack || CHR(10) || dbms_utility.format_error_backtrace );
end handleJavaError;


--********************************************************************************************************************************
member procedure checkRemoteException ( sCurrentName  in varchar2, 
                                        csSeveritycode in  integer,
                                        sRemoteEx       in clob) 
is
  exRemoteXml    exception;
begin  
  if sRemoteEx is not null then
    isr$trace.debug('error', 'error ', sCurrentName, sRemoteEx);
    handleJavaError (csSeveritycode, sRemoteEx, sCurrentName);    
  end if;
end checkRemoteException;

--********************************************************************************************************************************
member function GetLogLevelForSeverityCode  return varchar2 is
  sLogLevel isr$loglevel.loglevel%type;
begin

  case 
    when self.sSeverityCode = Stb$typedef.cnFatal then
      sLogLevel := 'FATAL';
    when self.sSeverityCode = Stb$typedef.cnNoError then
      sLogLevel := 'INFO';
    when self.sSeverityCode = Stb$typedef.cnSeverityCritical then
      sLogLevel := 'ERROR';
    when self.sSeverityCode = Stb$typedef.cnSeverityWarning then
      sLogLevel := 'WARN';
    when self.sSeverityCode = Stb$typedef.cnSeverityMessage then
      sLogLevel := 'INFO';
    when self.sSeverityCode in (Stb$typedef.cnaudit, Stb$typedef.cnfirstform) then -- hr 3.Jun 2019 [ISRC-1011, ISRC-719] 
      sLogLevel := 'DEBUG';
    else
      sLogLevel :=  'INFO';
  end case;

  return sLogLevel;
end GetLogLevelForSeverityCode;

--********************************************************************************************************************************
member function GetUserMessages return varchar2 is
  sMessages             varchar2(4000);
  sCurrentProc constant varchar2(100) := $$PLSQL_UNIT||'.GetUserMessages';  
begin
  for i in self.TLOMESSAGELIST.first .. self.TLOMESSAGELIST.last loop
    sMessages := substr(sMessages||chr(10)||self.TLOMESSAGELIST(i).sUserMsg,1,4000);
    if length(sMessages) = 4000 then
      exit;
    end if;
  end loop; 
  isr$trace.info_all('sMessages', sMessages, sCurrentProc);
  return sMessages;
end GetUserMessages;

--********************************************************************************************************************************
member function GetImplMessages return varchar2 is
  sMessages             varchar2(4000);
  sCurrentProc constant varchar2(100) := $$PLSQL_UNIT||'.GetImplMessages';
begin
  for i in self.TLOMESSAGELIST.first .. self.TLOMESSAGELIST.last loop
    sMessages := substr(sMessages||chr(10)||self.TLOMESSAGELIST(i).sImplMsg,1,4000);
    if length(sMessages) = 4000 then
      exit;
    end if;
  end loop; 
  isr$trace.info_all('sMessages', sMessages, sCurrentProc);
  return sMessages;
end GetImplMessages;

--********************************************************************************************************************************
member function GetDevMessages return varchar2 is
  sMessages             varchar2(4000);
  sCurrentProc constant varchar2(100) := $$PLSQL_UNIT||'.GetDevMessages';
begin
  for i in self.TLOMESSAGELIST.first .. self.TLOMESSAGELIST.last loop
    sMessages := substr(sMessages||chr(10)||self.TLOMESSAGELIST(i).sDevMsg,1,4000);
    if length(sMessages) = 4000 then
      exit;
    end if;
  end loop; 
  isr$trace.info_all('sMessages', sMessages, sCurrentProc);
  return sMessages;
end GetDevMessages;

--********************************************************************************************************************************
member procedure GetMessages(sUserMsg out varchar2, sImplMsg out varchar2, sDevMsg out varchar2) is 
  sCurrentProc constant varchar2(100) := $$PLSQL_UNIT||'.GetMessages';
begin
  for i in self.TLOMESSAGELIST.first .. self.TLOMESSAGELIST.last loop
    sUserMsg := substr(sUserMsg||chr(10)||self.TLOMESSAGELIST(i).sUserMsg,1,4000);
    sImplMsg := substr(sImplMsg||chr(10)||self.TLOMESSAGELIST(i).sImplMsg,1,4000);
    sDevMsg  := substr(sDevMsg||chr(10)||self.TLOMESSAGELIST(i).sDevMsg,1,4000);
    if length(sUserMsg) = 4000 and length(sImplMsg) = 4000 and length(sDevMsg)  = 4000 then
      exit;
    end if;
  end loop; 
  isr$trace.info_all('sUserMsg', sUserMsg, sCurrentProc);
  isr$trace.info_all('sImplMsg', sImplMsg, sCurrentProc);
  isr$trace.info_all('sDevMsg', sDevMsg, sCurrentProc);
end GetMessages;

--********************************************************************************************************************************
member procedure SetSeverityCodeToWorst is
  sCurrentProc constant varchar2(100) := $$PLSQL_UNIT||'.SetSeverityCodeToWorst';
begin

  if self.sSeverityCode in (Stb$typedef.cnNoError, Stb$typedef.cnSeverityMessage, Stb$typedef.cnSeverityWarning, Stb$typedef.cnSeverityCritical, Stb$typedef.cnFatal) then
    iSR$ErrorObj$Accessor.SetCurrentErrorObj(self);
    self.sSeverityCode := iSR$ErrorObj$Accessor.GetWorstSeverityCode;
  end if;
  isr$trace.debug_all('sSeverityCode', 'self.sSeverityCode: '||self.sSeverityCode, sCurrentProc);
end SetSeverityCodeToWorst;

--********************************************************************************************************************************
member procedure SetMessageListFromAccessor is
  oCurrentErrObj STB$OERROR$RECORD;                                        
begin
  oCurrentErrObj := iSR$ErrorObj$Accessor.GetCurrentErrorObj;  
  self.tloMessageList :=  oCurrentErrObj.tloMessageList;
end SetMessageListFromAccessor;

member function isExceptionCritical return boolean is
begin
  if self.sSeverityCode = stb$typedef.cnSeverityCritical then
    return true;
  end if;
  return false;
end isExceptionCritical;

end;