CREATE OR REPLACE TYPE BODY isr$obj$docproperty$fields as

constructor function ISR$OBJ$DOCPROPERTY$FIELDS(self in out nocopy ISR$OBJ$DOCPROPERTY$FIELDS) return self as result is
begin
  return;
end ISR$OBJ$DOCPROPERTY$FIELDS; 
--**********************************************************************************************************************************
static function putParameters(oParameter in stb$menuentry$record, olParameter in stb$property$list, clParameter in clob default null) 
  return stb$oerror$record is
  sCurrentName        constant varchar2(100) := $$PLSQL_UNIT||'.putParameters('||oParameter.sToken||')';
  oErrorObj           STB$OERROR$RECORD := STB$OERROR$RECORD();
  sMsg              varchar2(4000);
  oParamListRec   ISR$PARAMLIST$REC := ISR$PARAMLIST$REC();
 begin 
  isr$trace.stat('begin','begin',sCurrentName);
  oParamListRec.AddParam('TABLE', 'ISR$DOC$PROP$TEMPLATE');  
  if STB$OBJECT.getCurrentReporttypeid is null then
    oParamListRec.AddParam('AUDITTYPE', 'SYSTEMAUDIT');
    oParamListRec.AddParam('AUDITREFERENCE', TO_CHAR (-1));
  else
    oParamListRec.AddParam('AUDITTYPE', 'REPORTTYPEAUDIT');
    oParamListRec.AddParam('AUDITREFERENCE', TO_CHAR(STB$OBJECT.getCurrentReporttypeid));
  end if;
  oErrorObj := isr$obj$grid().modifyGridTable(oParameter.sToken, clParameter, oParamListRec);
  update stb$reporttype
  set modifiedon = SYSDATE
  , modifiedby =
      STB$SECURITY.getOracleUserName (STB$SECURITY.getCurrentUser)
  where reporttypeid = STB$OBJECT.getCurrentReporttypeId;
  isr$trace.stat('end','end',sCurrentName);
  return oErrorObj;
exception
  when others then
    sMsg :=  utd$msglib.getmsg ('exUnexpectedError', isr$obj$base().nCurrentLanguage, csP1 => SQLERRM(SQLCODE));
    oErrorObj.handleError ( Stb$typedef.cnSeverityCritical, sMsg, sCurrentName,
                            DBMS_UTILITY.format_error_stack || CHR(10) || DBMS_UTILITY.format_error_backtrace );
    return oErrorObj; 
 end putParameters;
 
static function callFunction (oParameter in STB$MENUENTRY$RECORD, olNodeList out STB$TREENODELIST)
  return STB$OERROR$RECORD is
  sCurrentName         constant varchar2(100) := $$PLSQL_UNIT||'.callFunction('||oParameter.sToken||')';
  oErrorObj                     STB$OERROR$RECORD := STB$OERROR$RECORD();
  sMsg              varchar2(4000);
  oParamListRec   ISR$PARAMLIST$REC := ISR$PARAMLIST$REC();
begin
  isr$trace.stat('begin', 'oParameter.sToken: '||oParameter.sToken ,sCurrentName);
  isr$trace.debug('parameter', 'oParameter', sCurrentName, oParameter);
    
  STB$OBJECT.setCurrentToken(oparameter.stoken);
  
  oParamListRec.AddParam('TABLE', 'ISR$DOC$PROP$TEMPLATE');
  oErrorObj := isr$obj$docproperty$fields().buildGridMask(oParameter.sToken, oParamListRec, olNodeList);
  isr$trace.stat('end', 'end', sCurrentName);
  return oErrorObj;
exception
  when others then
    sMsg :=  utd$msglib.getmsg ('exUnexpectedError', isr$obj$base().nCurrentLanguage, csP1 => SQLERRM(SQLCODE));
    oErrorObj.handleError ( Stb$typedef.cnSeverityCritical, sMsg, sCurrentName,
                            DBMS_UTILITY.format_error_stack || CHR(10) || DBMS_UTILITY.format_error_backtrace );
    return oErrorObj; 
end callFunction;


end;