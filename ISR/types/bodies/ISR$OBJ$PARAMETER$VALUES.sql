CREATE OR REPLACE TYPE BODY                   isr$obj$parameter$values as

--**********************************************************************************************************************************
static function putParameters(oParameter in stb$menuentry$record, olParameter in stb$property$list, clParameter in clob default null) 
  return stb$oerror$record is
  sCurrentName        constant varchar2(100) := $$PLSQL_UNIT||'.putParameters('||oParameter.sToken||')';
  oErrorObj           STB$OERROR$RECORD := STB$OERROR$RECORD();
  sMsg              varchar2(4000);
  oParamListRec   ISR$PARAMLIST$REC := ISR$PARAMLIST$REC();
 begin 
  isr$trace.stat('begin','begin',sCurrentName);
  oParamListRec.AddParam('TABLE', 'ISR$PARAMETER$VALUES');
  oParamListRec.AddParam('AUDITTYPE', 'SYSTEMAUDIT');
  oParamListRec.AddParam('AUDITREFERENCE', TO_CHAR (-1));
  oErrorObj := isr$obj$grid().modifyGridTable(oParameter.sToken, clParameter, oParamListRec);
  isr$trace.stat('end','end',sCurrentName);
  return oErrorObj;
exception
  when others then
    sMsg :=  utd$msglib.getmsg ('exUnexpectedError', isr$obj$base().nCurrentLanguage, csP1 => SQLERRM(SQLCODE));
    oErrorObj.handleError ( Stb$typedef.cnSeverityCritical, sMsg, sCurrentName,
                            DBMS_UTILITY.format_error_stack || CHR(10) || DBMS_UTILITY.format_error_backtrace );
    return oErrorObj; 
 end putParameters;
 
static function callFunction (oParameter in STB$MENUENTRY$RECORD, olNodeList out STB$TREENODELIST)
  return STB$OERROR$RECORD is
  sCurrentName         constant varchar2(100) := $$PLSQL_UNIT||'.callFunction('||oParameter.sToken||')';
  oErrorObj                     STB$OERROR$RECORD := STB$OERROR$RECORD();
  sMsg              varchar2(4000);
  oParamListRec   ISR$PARAMLIST$REC := ISR$PARAMLIST$REC();
begin
  isr$trace.stat('begin', 'oParameter.sToken: '||oParameter.sToken ,sCurrentName);
  isr$trace.debug('parameter', 'oParameter', sCurrentName, oParameter);
    
  STB$OBJECT.setCurrentToken(oparameter.stoken);
  
  oParamListRec.AddParam('TABLE', 'ISR$PARAMETER$VALUES');
  oErrorObj := isr$obj$grid().buildGridMask(oParameter.sToken, oParamListRec, olNodeList);
  isr$trace.stat('end', 'end', sCurrentName);
  return oErrorObj;
exception
  when others then
    sMsg :=  utd$msglib.getmsg ('exUnexpectedError', isr$obj$base().nCurrentLanguage, csP1 => SQLERRM(SQLCODE));
    oErrorObj.handleError ( Stb$typedef.cnSeverityCritical, sMsg, sCurrentName,
                            DBMS_UTILITY.format_error_stack || CHR(10) || DBMS_UTILITY.format_error_backtrace );
    return oErrorObj; 
end callFunction;


-- ***************************************************************************************************
static function getLov( sEntity        in varchar2,
                 sSuchwert      in varchar2, 
                 oSelectionList out ISR$TLRSELECTION$LIST ) return STB$OERROR$RECORD 
is
  sCurrentName constant varchar2(100) := $$PLSQL_UNIT||'.getLov(' || sEntity || ')';
  sMsg         varchar2(4000);
  nCounter     number            := 0;
  oErrorObj    STB$OERROR$RECORD := STB$OERROR$RECORD();
  
  cursor cGetEntity is
    select entity
         , UTD$MSGLIB.getMsg (entity, stb$security.GetCurrentLanguage) translation
      from isr$parameter$entities
     where editable = 'T'
    order by 1;

begin
  isr$trace.stat('sEntity='||sEntity, 'begin', sCurrentName);
  oSelectionList := ISR$TLRSELECTION$LIST ();
   
  if UPPER (sEntity) = 'ENTITY' then
    for rGetEntity in cGetEntity loop
      oSelectionList.EXTEND;
      oSelectionList (oSelectionList.COUNT()) := ISR$OSELECTION$RECORD(rGetEntity.entity, rGetEntity.entity, rGetEntity.translation, null, oSelectionList.COUNT());
    end loop;
  end if;
        
  if sSuchWert is not null and oselectionlist is not null and oselectionlist.first is not null then
    isr$trace.debug ('sSuchWert',  sSuchWert, sCurrentName); 
    for i IN 1..oSelectionList.count() loop
      if oSelectionList(i).sValue = sSuchWert then
        isr$trace.debug ('oSelectionList('||i||').sSelected', oSelectionList(i).sValue, sCurrentName);
        oSelectionList(i).sSelected := 'T';
      end if;
    end loop;
  end if;

  isr$trace.stat ('end', 'oSelectionList.count() = ' || oSelectionList.count(), sCurrentName);
  return oErrorObj;
exception
  when others then
    sMsg :=  utd$msglib.getmsg ('exUnexpectedError', stb$security.getcurrentlanguage, csP1 => sqlerrm(sqlcode));
    oErrorObj.handleError ( Stb$typedef.cnSeverityCritical, sMsg, sCurrentName, 
                            substr(dbms_utility.format_error_stack || CHR(10) || dbms_utility.format_error_backtrace, 1,4000)  );  
    return oErrorObj;    
end getLov;

end;