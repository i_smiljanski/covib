CREATE OR REPLACE type body ISR$OBJ$CAN$MODIFY$WIZARD as

--**********************************************************************************************************************************
 constructor function ISR$OBJ$CAN$MODIFY$WIZARD(self in out nocopy ISR$OBJ$CAN$MODIFY$WIZARD) return self as result is
    begin
    return;
  end ISR$OBJ$CAN$MODIFY$WIZARD; 
   

static function putParameters(oParameter in stb$menuentry$record, olParameter in stb$property$list, clParameter in clob default null) 
  return stb$oerror$record is
  sCurrentName        constant varchar2(100) := $$PLSQL_UNIT||'.putParameters('||oParameter.sToken||')';
  oErrorObj           STB$OERROR$RECORD := STB$OERROR$RECORD();
  sMsg              varchar2(4000);
  oParamListRec   ISR$PARAMLIST$REC := ISR$PARAMLIST$REC();
  oWizard ISR$OBJ$CAN$MODIFY$WIZARD:=ISR$OBJ$CAN$MODIFY$WIZARD();
 begin 
  isr$trace.stat('begin','begin',sCurrentName);
  oParamListRec.AddParam('TABLE', 'ISR$REPORT$WIZARD');
  oParamListRec.AddParam('AUDITTYPE', 'REPORTTYPEAUDIT');
  oParamListRec.AddParam('AUDITREFERENCE', TO_CHAR(STB$OBJECT.getCurrentReporttypeid));
  --oErrorObj := isr$obj$grid().modifyGridTable(oParameter.sToken, clParameter, oParamListRec);
  oErrorObj := oWizard.modifyGridTable(oParameter.sToken, clParameter, oParamListRec);
  
  isr$trace.stat('end','end',sCurrentName);
  return oErrorObj;
exception
  when others then
    sMsg :=  utd$msglib.getmsg ('exUnexpectedError', isr$obj$base().nCurrentLanguage, csP1 => SQLERRM(SQLCODE));
    oErrorObj.handleError ( Stb$typedef.cnSeverityCritical, sMsg, sCurrentName,
                            DBMS_UTILITY.format_error_stack || CHR(10) || DBMS_UTILITY.format_error_backtrace );
    return oErrorObj; 
 end putParameters;
 
static function callFunction (oParameter in STB$MENUENTRY$RECORD, olNodeList out STB$TREENODELIST)
  return STB$OERROR$RECORD is
  sCurrentName         constant varchar2(100) := $$PLSQL_UNIT||'.callFunction('||oParameter.sToken||')';
  oErrorObj                     STB$OERROR$RECORD := STB$OERROR$RECORD();
  sMsg              varchar2(4000);
  oParamListRec   ISR$PARAMLIST$REC := ISR$PARAMLIST$REC();
  oWizard ISR$OBJ$CAN$MODIFY$WIZARD:=ISR$OBJ$CAN$MODIFY$WIZARD();
begin
  isr$trace.stat('begin', 'oParameter.sToken: '||oParameter.sToken ,sCurrentName);
  isr$trace.debug('parameter', 'oParameter', sCurrentName, oParameter);
    
  STB$OBJECT.setCurrentToken(oparameter.stoken);
  
 

  ISR$TEMPLATE$HANDLER.setAllowedTockenParameters(oParamListRec);
  oParamListRec.AddParam('TABLE', 'ISR$REPORT$WIZARD');
  --oErrorObj := isr$obj$grid().buildGridMask(oParameter.sToken, oParamListRec, olNodeList);
  oErrorObj := oWizard.buildGridMask(oParameter.sToken, oParamListRec, olNodeList);
  isr$trace.stat('end', 'end', sCurrentName);
  return oErrorObj;
exception
  when others then
    sMsg :=  utd$msglib.getmsg ('exUnexpectedError', isr$obj$base().nCurrentLanguage, csP1 => SQLERRM(SQLCODE));
    oErrorObj.handleError ( Stb$typedef.cnSeverityCritical, sMsg, sCurrentName,
                            DBMS_UTILITY.format_error_stack || CHR(10) || DBMS_UTILITY.format_error_backtrace );
    return oErrorObj; 
end callFunction;


end;