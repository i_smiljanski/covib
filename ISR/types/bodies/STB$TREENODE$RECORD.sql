CREATE OR REPLACE TYPE BODY STB$TREENODE$RECORD AS 

constructor function STB$TREENODE$RECORD(self in out nocopy STB$TREENODE$RECORD, 
                                         sDisplay          VARCHAR2 DEFAULT null,
                                         sPackageType      VARCHAR2 DEFAULT null,
                                         sNodeType         VARCHAR2 DEFAULT null,
                                         sNodeId           VARCHAR2 DEFAULT null,
                                         sIcon             VARCHAR2 DEFAULT null,
                                         sDefaultAction    VARCHAR2 DEFAULT null,
                                         sTooltip          VARCHAR2 DEFAULT null,
                                         tloPropertyList   STB$PROPERTY$LIST DEFAULT STB$PROPERTY$LIST (),
                                         tloValueList      ISR$VALUE$LIST DEFAULT ISR$VALUE$LIST (),
                                         nDisplayType      NUMBER DEFAULT 1 /*STB$TYPEDEF.cnMenuDialog*/
                                       ) return self as result is
begin
  self.sDisplay := sDisplay;
  self.sPackageType := sPackageType;
  self.sNodeType := sNodeType;
  self.sNodeId := sNodeId;
  self.sIcon := sIcon;
  self.sDefaultAction := sDefaultAction;
  self.sTooltip := sTooltip;
  self.tloPropertyList := tloPropertyList;
  self.tloValueList := tloValueList;
  self.nDisplayType := nDisplayType;
  return;
end STB$TREENODE$RECORD; 

--*********************************************************************************************************************************    
member procedure setDirectory (self            in out STB$TREENODE$RECORD, 
                               sParameterName  in     varchar2  default null, 
                               sParameterValue in     varchar2  default null) 
is
  sCurrentName constant varchar2(100) := $$PLSQL_UNIT||'.setDirectory';
begin
  if self.tloValueList is null then
    self.tloValueList := ISR$VALUE$LIST();
  end if;
  self.tloValueList.extend;
  self.tloValueList(self.tloValueList.count()) := ISR$VALUE$RECORD(null, null, self.sNodeType, self.sNodeId, self.sPackageType);
  if sParameterName IS NOT NULL THEN
    self.tloValueList(self.tloValueList.count()).sParametername := sParameterName;
  else
    self.tloValueList(self.tloValueList.COUNT()).sParametername := self.sNodeType;
  end if;
  if sParameterName is not null then
    self.tloValueList(self.tloValueList.count()).sParametervalue := sParameterValue;
  else
    self.tloValueList(self.tloValueList.count()).sParametervalue := self.sNodeId;
  end if;
  self.sNodeId := STB$OBJECT.calculateHash (self.tloValueList)||'#@#'||STB$SECURITY.getCurrentUser;
  isr$trace.debug_all('parameter treenode', 'xmltype(self).getClobVal() see column LOGCLOB', sCurrentName, xmltype(self).getClobVal());
end setDirectory;    

--*********************************************************************************************************************************  
member function getPropertyParameter( self            in STB$TREENODE$RECORD, 
                                      csParameterName in varchar2, 
                                      csOutputValue   in varchar2 default 'SVALUE' ) 
  return varchar2
is
  sCurrentName      constant varchar2(100) := $$PLSQL_UNIT||'.getPropertyParameter(self,'||csParameterName||','||csOutputValue||')';
  olPropertyList    STB$PROPERTY$LIST;
  sReturnString     varchar2(100);
  sParameterName    varchar2(100);
  sOutputValue      varchar2(4000);
begin
  isr$trace.stat('begin','begin',sCurrentName);
  
  olPropertyList := self.tloPropertyList;
  sParameterName := upper(csParameterName);
  sOutputValue   := upper(csOutputValue);
  
  if olPropertyList.exists(1) then
    for i in 1..olPropertyList.count
    loop
      if olPropertyList(i).sParameter = sParameterName then
        case
          when sOutputValue = 'SPROMPTDISPLAY' then
            sReturnString := olPropertyList(i).sPromptDisplay;
          when sOutputValue = 'SVALUE' then
            sReturnString := olPropertyList(i).sValue;
          when sOutputValue = 'SDISPLAY' then
            sReturnString := olPropertyList(i).sDisplay;          
          when sOutputValue = 'SEDITABLE' then
            sReturnString := olPropertyList(i).sEditable;
          when sOutputValue = 'SREQUIRED' then
            sReturnString := olPropertyList(i).sRequired;
          when sOutputValue = 'SDISPLAYED' then
            sReturnString := olPropertyList(i).sDisplayed;          
          when sOutputValue = 'HASLOV' then
            sReturnString := olPropertyList(i).hasLov;
          when sOutputValue = 'TYPE' then
            sReturnString := olPropertyList(i).type;
          when sOutputValue = 'FORMAT' then
            sReturnString := olPropertyList(i).format;          
          when sOutputValue = 'SFOCUS' then
            sReturnString := olPropertyList(i).sFocus;          
          else
            sReturnString := 'ERROR in '||sCurrentName;
        end case;
      end if;
    end loop;
  end if;
  
  isr$trace.stat('end','sReturnString: '||sReturnString,sCurrentName);
  return sReturnString;
exception
  when others then 
    return 'ERROR in '||sCurrentName;
end getPropertyParameter;

--*********************************************************************************************************************************  
member function getValueParameter( self            in STB$TREENODE$RECORD, 
                                   csParameterName in varchar2, 
                                   csOutputValue   in varchar2 default 'SPARAMETERVALUE' ) 
  return varchar2
is
  sCurrentName      constant varchar2(100) := $$PLSQL_UNIT||'.getValueParameter(self,'||csParameterName||','||csOutputValue||')';
  olValueList       ISR$VALUE$LIST;
  sReturnString     varchar2(100);
  sParameterName    varchar2(100);
  sOutputValue      varchar2(4000);
begin
  isr$trace.stat('begin','begin',sCurrentName);
  
  olValueList    := self.tloValueList;
  sParameterName := upper(csParameterName);
  sOutputValue   := upper(csOutputValue);

  if olValueList.exists(1) then  
    for i in 1..olValueList.count
    loop
      if olValueList(i).sParameterName = sParameterName then
        case
          when sOutputValue = 'SPARAMETERVALUE' then
            sReturnString := olValueList(i).sParameterValue;
          when sOutputValue = 'sNodeType' then
            sReturnString := olValueList(i).sNodeType;
          when sOutputValue = 'sNodeId' then
            sReturnString := olValueList(i).sNodeId;          
          when sOutputValue = 'sNodePackage' then
            sReturnString := olValueList(i).sNodePackage;
          else
            sReturnString := 'ERROR in '||sCurrentName;
        end case;
      end if;
    end loop;
  end if;

  isr$trace.stat('end','sReturnString: '||sReturnString,sCurrentName);
  return sReturnString;
exception
  when others then 
    return 'ERROR in '||sCurrentName;
end getValueParameter;
    
end; 