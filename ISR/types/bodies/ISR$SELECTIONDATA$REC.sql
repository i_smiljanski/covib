CREATE OR REPLACE TYPE BODY                   "ISR$SELECTIONDATA$REC" as

--*********************************************************************************************************************************
  constructor function iSR$SelectionData$Rec(self in out nocopy iSR$SelectionData$Rec) return self as result is
  begin
    self.lSelectionList := ISR$TLRSELECTION$LIST();
    return;
  end iSR$SelectionData$Rec;  

--*********************************************************************************************************************************  
constructor function iSR$SelectionData$Rec(self in out nocopy iSR$SelectionData$Rec, lSelectionList ISR$TLRSELECTION$LIST) return self as result is
begin
  self.lSelectionList := lSelectionList;
  return; 
end iSR$SelectionData$Rec;
  
--*********************************************************************************************************************************  
member procedure OrderSelection is  
  sCurrentName    constant varchar2(100) := $$PLSQL_UNIT||'.GetAttrValue';  
  oSelectionOld   iSR$tlrSelection$List := self.lSelectionList;
begin
  isr$trace.info_all('begin','Trigger '||sCurrentName||' started',sCurrentName);

  select iSR$oSelection$Record( sDisplay, sValue, sInfo, sSelected, nOrderNum, sMasterKey, TLOATTRIBUTELIST )
  bulk collect into oSelectionOld
  from table(self.lSelectionList) tmp
  order by tmp.nOrderNum, sValue;    
  
  SELECT ISR$OSELECTION$RECORD( sDisplay, sValue, sInfo, sSelected, nOrderNum, sMasterKey, TLOATTRIBUTELIST )
  bulk collect into self.lSelectionList
  from table(oSelectionOld) tmp
  order by tmp.nOrderNum, sValue;    

  isr$trace.info_all('end','Trigger '||sCurrentName||' finished',sCurrentName);
end OrderSelection;
  
--*********************************************************************************************************************************  
member procedure SetSelectionOrder is
  oSelectionOld   iSR$tlrSelection$List := self.lSelectionList;
begin
  SELECT ISR$OSELECTION$RECORD( sDisplay, sValue, sInfo, sSelected, rownum, sMasterKey, TLOATTRIBUTELIST )
  bulk collect into self.lSelectionList
  from table(oSelectionOld);  
end SetSelectionOrder;
  
end;