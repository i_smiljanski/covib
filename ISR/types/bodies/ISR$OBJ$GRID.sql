CREATE OR REPLACE type body isr$obj$grid as 
-- ***************************************************************************************************   
constructor function ISR$OBJ$GRID(self in out nocopy ISR$OBJ$GRID) return self as result is
begin
  return;
end ISR$OBJ$GRID; 

-- ***************************************************************************************************  
member function modifyGridTable( csToken        in varchar2, 
                                 csModifyXml    in clob,
                                 coAddParamList in  isr$paramlist$rec )
  return STB$OERROR$RECORD 
is                 
    sCurrentName        constant varchar2(100) := $$PLSQL_UNIT||'.modifyGridTable('||csToken||')';
    oErrorObj           STB$OERROR$RECORD := STB$OERROR$RECORD();
    sExecuteStatement  boolean;
    
    sToken            varchar2(500) := csToken;
    sTable            varchar2(30);
    sParamAuditType   varchar2(1000);
    sParamAuditRef    varchar2(4000);
    sOldValueString   varchar2(32767);
    sNewValueString   varchar2(32767);
    sAction           varchar2(500);
    sRowId            varchar2(500);
    sStmt             varchar2(32767);
    --nCount            number;
    sKeys             varchar2(4000);
    sKeyValues        varchar2(4000);
    sAuditKeys        varchar2(4000);
    sKey             ISR$DIALOG.PARAMETERNAME%type;
    sKey1             ISR$DIALOG.PARAMETERNAME%type;
  --  sKey2             ISR$DIALOG.PARAMETERNAME%type;
    sKeyValue1        ISR$DIALOG.PARAMETERVALUE%type;
  --  sKeyValue2        ISR$DIALOG.PARAMETERVALUE%type;
    sParameter        varchar2(500);
    sOldValue         varchar2(32767);
    sNewValue         varchar2(32767);
    sNewValueClob     clob;
    blOldValue        blob;
    sMsg              varchar2(32767);
    sEditable         varchar2(1);
    xXml              xmltype;
    sPackage          varchar2(200);
    sParameterType    isr$dialog.parametertype%type;
    sKeyWhere         varchar2(1000);
    
    cursor cGetParameterInfo(csParameter in varchar2, csValue in varchar2, csPackage in varchar2) is
      select utd$msglib.getmsg (description, stb$security.getcurrentlanguage) parameter
           , case
                when haslov in ('T', 'W') then
                   NVL(isr$obj$base.getSelectedLovDisplay (parametername, csValue, csPackage), csValue)
                else
                   csValue
             end sValue
           , editable
           , parametertype
        from isr$dialog
       where token = sToken
         and parametername = csParameter;
         
  rGetParameterInfo   cGetParameterInfo%rowtype;
    
  cursor cPrimaryKeys(sTableName varchar2) is
    select distinct cols.column_name
      from all_constraints cons, all_cons_columns cols
     where cols.table_name = sTableName
       and cons.constraint_type = 'P'
       and cons.constraint_name = cols.constraint_name
       and cons.owner = cols.owner
     order by 1;
          
begin
  isr$trace.stat('begin', 'begin', sCurrentName);
  isr$trace.debug('parameter','csModifyXml',sCurrentName,csModifyXml);
  isr$trace.debug('parameter','coAddParamList',sCurrentName,coAddParamList);
  
  oErrorObj := coAddParamList.GetVerfiedParamStr('TABLE', sTable); 
  oErrorObj := coAddParamList.GetVerfiedParamStr('AUDITTYPE', sParamAuditType);
  oErrorObj := coAddParamList.GetVerfiedParamStr('AUDITREFERENCE', sParamAuditRef);
  
  isr$trace.debug ('variable','sTable: '||sTable, sCurrentName);
  isr$trace.debug ('variable','sParamAuditType: '||sParamAuditType, sCurrentName);  
  isr$trace.debug ('variable','sParamAuditRef: '||sParamAuditRef, sCurrentName);    
  
  STB$OBJECT.setCurrentToken(cstoken);
  oErrorObj := Stb$audit.openAudit (sParamAuditType, sParamAuditRef);  
  oErrorObj := Stb$audit.AddAuditRecord (sToken);
    
  xXml := ISR$XML.clobToXML(csModifyXml);

  for i in 1..ISR$XML.valueOf(xXml, 'count(//ROW)') loop
    sOldValueString := null;
    sNewValueString := null;
    sKeys := null;
    sKeyValues := null;
    sAuditKeys := null;
    sKeyWhere := null;

    sAction := ISR$XML.valueOf(xXml, '(//ROW)['||i||']/../name()');
    sRowId := ISR$XML.valueOf(xXml, '(//ROW)['||i||']/ROWID/text()');
    if TRIM(sRowId) is null then
      sRowId := ISR$XML.valueOf(xXml, '(//ROW)['||i||']/VROWID/text()');  --for view, because 'ORA-01445: Cannot Select ROWID from a Join View'
    end if;
    if TRIM(sRowId) is null then
      sAction := 'INSERT';
    end if;

    isr$trace.debug ('sAction',  sAction, sCurrentName);
    isr$trace.debug ('sRowId',  sRowId, sCurrentName);

   
    for rGetPks in cPrimaryKeys(sTable) loop
      sKeys := sKeys||rGetPks.column_name||', ';
      sKeyValues := sKeyValues||''''||REPLACE(ISR$XML.valueOf(xXml, '(//ROW)['||i||']/'||rGetPks.column_name||'/text()'), '''', '''''')||''', ';
      sAuditKeys := sAuditKeys||rGetPks.column_name||': '||REPLACE(ISR$XML.valueOf(xXml, '(//ROW)['||i||']/'||rGetPks.column_name||'/text()'), '''', '''''')||CHR(10);
      sKey := rGetPks.column_name;
      sKeyWhere := sKeyWhere||sKey||' = '''||REPLACE(ISR$XML.valueOf(xXml, '(//ROW)['||i||']/'||sKey||'/text()'), '''', '''''')||''' and ';
    
    end loop;
    sKeys := RTRIM(sKeys, ', ');
    sKeyValues := RTRIM(sKeyValues, ', ');
    sAuditKeys := RTRIM(sAuditKeys, CHR(10));
    
    sKeyWhere := REGEXP_REPLACE(sKeyWhere, 'and ', '', instr(sKeyWhere ,'and ',-1));
    
    isr$trace.debug ('sKeys', sKeys, sCurrentName);
    isr$trace.debug ('sKeyValues', sKeyValues, sCurrentName);
    isr$trace.debug ('sAuditKeys', sAuditKeys, sCurrentName);
    isr$trace.debug ('sKeyWhere', sKeyWhere, sCurrentName);

    if sAction = 'INSERT' then
      sStmt := 'INSERT INTO '||sTable||' '||
               '   ('||sKeys||') '||
               'VALUES '||
               '  ('||sKeyValues||')';
      isr$trace.debug ('sStmt', sStmt, sCurrentName);
      execute immediate sStmt;
      sStmt := 'SELECT ROWID FROM '||sTable||' WHERE '|| sKeyWhere;
      isr$trace.debug ('sStmt', sStmt, sCurrentName);
      execute immediate sStmt into sRowid;
    end if;    
    
    sPackage := nvl(STB$OBJECT.getCurrentNodePackage(Stb$object.getCurrentNodeType,csToken),STB$OBJECT.getCurrentNodePackage);

    for j in 1..ISR$XML.valueOf(xXml, 'count((//ROW)['||i||']/*[name()!= "ROWID"])') loop
      sParameter := ISR$XML.valueOf(xXml, '((//ROW)['||i||']/*[name()!= "ROWID"])['||j||']/name()');
      isr$trace.debug ('sParameter', sParameter, sCurrentName);



      sEditable := 'F';
      open cGetParameterInfo(sParameter, sNewValue, sPackage);
      fetch cGetParameterInfo into rGetParameterInfo;
      sEditable := rGetParameterInfo.editable; 
      sParameterType := rGetParameterInfo.parameterType;
      close cGetParameterInfo;

      if sParameterType not in ('FILE') then
      
        sNewValue := ISR$XML.ValueOf(xXml, '((//ROW)['||i||']/*[name()!= "ROWID"])['||j||']/text()');
        isr$trace.debug ('sNewValue, sEditable', sNewValue || ', ' || sEditable, sCurrentName);

        if sEditable = 'T' then
          sOldValue := null;
          sExecuteStatement:=executeStatement(sAction ,sTable , sRowId ,sParameter ,sStmt ,sOldValue ,sNewValue );

          if sAction in ('DELETE') or (sAction in ('UPDATE') and NVL(sOldValue, '#@#') != NVL(sNewValue, '#@#') ) then
            open cGetParameterInfo(sParameter, sOldValue, sPackage);
            fetch cGetParameterInfo into rGetParameterInfo;
            sParameter := rGetParameterInfo.parameter;
            sOldValue := rGetParameterInfo.sValue;
            close cGetParameterInfo;
            sOldValueString := SUBSTR(sOldValueString||sParameter||':'||sOldValue||CHR(10), 0, 4000);
          end if;
          if sAction in ('INSERT') or (sAction in ('UPDATE') and NVL(sOldValue, '#@#') != NVL(sNewValue, '#@#') ) then
            open cGetParameterInfo( sParameter, sNewValue, sPackage);
            fetch cGetParameterInfo into rGetParameterInfo;
            sParameter := rGetParameterInfo.parameter;
            --sNewValue := rGetParameterInfo.sValue;  --ISRC-1334
            close cGetParameterInfo;
            sNewValueString := SUBSTR(sNewValueString||sParameter||':'||sNewValue||CHR(10), 0, 4000);
          end if;
        end if;
        
      else  --type 'FILE'
      
        sNewValueClob := isr$xml.ValueAsClob(xXml, '((//ROW)['||i||']/*[name()!= "ROWID"])['||j||']/text()');
        isr$trace.debug ('sNewValueClob', 's. logclob', sCurrentName, sNewValueClob);

        if sEditable = 'T' then
          blOldValue := null;
          if sAction in ('UPDATE', 'DELETE') then          
            sStmt := 'SELECT '||sParameter||' FROM '||sTable||' WHERE ROWID = '''||sRowId||'''';
            isr$trace.debug ('sStmt', 's. logblob', sCurrentName, sStmt);
            execute immediate sStmt into blOldValue;
          end if;

          if sAction in ('INSERT', 'UPDATE') then 
           sStmt := 'UPDATE '||sTable||' '||
                             'SET '||sParameter||' = :1 '||
                             'WHERE '|| sKeyWhere;
                             --sKey1||' = '''||REPLACE(sKeyValue1, '''', '''''')||'''' ;                             
           ISR$TRACE.debug('SQL','sStmt',sCurrentName,sStmt);
           execute immediate sStmt using stb$java.getStringByteBlob(sNewValueClob); 
                   
           -- sStmtClob := 'UPDATE '||sTable||' SET '||sParameter||' = stb$java.getStringByteBlob('''||sNewValueClob||''') WHERE ROWID = ''' || sRowId || ''' ';
            isr$trace.debug ('sStmt', 's. logclob', sCurrentName, sStmt);
          end if;

        end if;
        
       end if;

    end loop;

    if sAction in ('DELETE') then
      sStmt := 'DELETE FROM '||sTable||' WHERE ROWID = ''' || sRowId || ''' ';
      isr$trace.debug ('sStmt', sStmt, sCurrentName);
      execute immediate sStmt;
    end if;

    open cGetParameterInfo( sKey1, sKeyValue1, sPackage);
    fetch cGetParameterInfo into sKey1, sKeyValue1, sEditable, sParameterType;
    close cGetParameterInfo;

    case
      when sAction = 'INSERT' then
        oErrorObj := Stb$audit.AddAuditEntry (sAuditKeys/*sKeys||': '||sKeyValues*/, 'Insert', RTRIM(sNewValueString, CHR(10)), 1);
      when sAction = 'DELETE' then
        oErrorObj := Stb$audit.AddAuditEntry (sAuditKeys/*sKeys||': '||sKeyValues*/, 'Delete', RTRIM(sOldValueString,CHR(10)), 1);
      when sAction = 'UPDATE' and sOldValueString != sNewValueString then
        oErrorObj := Stb$audit.AddAuditEntry (sAuditKeys/*sKeys||': '||sKeyValues*/, RTRIM(sOldValueString, CHR(10)), RTRIM(sNewValueString, CHR(10)), 1);
      else
        null;
    end case;

  end loop;

  oErrorObj := isr$obj$base().getAuditType;
  
  isr$trace.stat('end', 'end', sCurrentName);

  return oErrorObj;     
exception
  when others then
    if SQLCODE = -2290 and SQLERRM like '%UPPER%' then
      sMsg :=  utd$msglib.getmsg ('exUpperCaseRequired', isr$obj$base().nCurrentLanguage, sKeys);     
      oErrorObj.handleError ( Stb$typedef.cnSeverityWarning, sMsg, sCurrentName,
                            DBMS_UTILITY.format_error_stack || CHR(10) || DBMS_UTILITY.format_error_backtrace, SQLCODE );
    else
      sMsg :=  utd$msglib.getmsg ('exUnexpectedError', isr$obj$base().nCurrentLanguage, csP1 => SQLERRM(SQLCODE));
      oErrorObj.handleError ( Stb$typedef.cnSeverityCritical, sMsg, sCurrentName,
                              DBMS_UTILITY.format_error_stack || CHR(10) || DBMS_UTILITY.format_error_backtrace, SQLCODE );
    end if;
    return oErrorObj;                      
end modifyGridTable;

--**********************************************************************************************************************************
member function buildGridMask(csToken  in varchar2, coAddParamList in  isr$paramlist$rec, olNodeList out STB$TREENODELIST)
  return STB$OERROR$RECORD
is
  oErrorObj         STB$OERROR$RECORD := STB$OERROR$RECORD();
  sCurrentName      constant varchar2(100) := $$PLSQL_UNIT||'.buildGridMask('||csToken||')';
  sMsg              varchar2(4000);
  cParameterOld     clob;
  sHeader           varchar2(32767);
  sXmlelement       varchar2(32767);
  xXml              xmltype;
  xXml1             xmltype;
  xHeader           xmltype;
  sSql              varchar2(4000);
  sInsertAllowed    varchar2(1) := 'F';
  sUpdateAllowed    varchar2(1) := 'F';
  sDeleteAllowed    varchar2(1) := 'F';
  sTable  varchar2(32);
  
  cursor cPrimaryKeys(sTableName varchar2) is
    select distinct cols.column_name
      from all_constraints cons, all_cons_columns cols
     where cols.table_name = sTableName
       and cons.constraint_type = 'P'
       and cons.constraint_name = cols.constraint_name
       and cons.owner = cols.owner
     order by 1;

  type tGetXml is ref cursor;
  cGetXml           tGetXml;

  cursor cDialog(csUpdateAllowed in varchar2) is
    select PARAMETERNAME
         , PARAMETERVALUE
         , utd$msglib.getmsg (DESCRIPTION, Stb$security.GetCurrentLanguage)
              headerValue
         , PARAMETERTYPE
         , DISPLAYED
         , HASLOV
         , case when csUpdateAllowed = 'T' then EDITABLE else 'F' end editable
         , REQUIRED
         , case
             when parametertype = 'DATE' then nvl(FORMAT, STB$UTIL.getSystemParameter('JAVA_DATE_FORMAT')) 
             else format
           end format
      from ISR$DIALOG
     where token = csToken
       and parametertype is not null
    order by DECODE (tab, null, orderno, tab), orderno;

  sPrimaryKey       varchar2(1);

  cursor cGetWhereClause is
    select parametervalue sWhere
      from isr$dialog
     where token = csToken
       and parametername = 'WHERE';

  cursor cGetSortOrder is
    select RTRIM(XMLAGG(XMLELEMENT(e,parametername||case when INSTR(sortorder, ' ') > 0 then SUBSTR(sortorder,INSTR(sortorder, ' ')) end,',').EXTRACT('//text()') order by sortorder).GetStringVal(),',') orderby
      from isr$dialog
     where sortorder is not null
       and token = csToken
    order by sortorder;

  sPackage            varchar2(200);

begin
  isr$trace.stat('begin', 'csToken: '||csToken, sCurrentName);
  isr$trace.debug('parameter','coAddParamList',sCurrentName,coAddParamList);
  
  oErrorObj := coAddParamList.GetVerfiedParamStr('TABLE', sTable);   
  isr$trace.debug ('sTable',  sTable, sCurrentName);
  --sExecuteStatement:=test_em(sTable);
  olNodeList := STB$TREENODELIST();
  olNodeList.EXTEND;
  olNodeList (1) := STB$TREENODE$RECORD(STB$UTIL.getMaskTitle(UPPER (csToken)), 'STB$SYSTEM', 'TRANSPORT', 'MASK', '');
  isr$trace.debug('value','olNodeList', sCurrentName,olNodeList);
  
  cParameterOld := olNodeList (1).clPropertyList;

  sInsertAllowed := STB$SECURITY.checkGroupRight (UPPER(csToken)||'_I', STB$OBJECT.getCurrentReporttypeid);
  sUpdateAllowed := STB$SECURITY.checkGroupRight (UPPER(csToken)||'_U', STB$OBJECT.getCurrentReporttypeid);
  sDeleteAllowed := STB$SECURITY.checkGroupRight (UPPER(csToken)||'_D', STB$OBJECT.getCurrentReporttypeid);
 


  sPackage := nvl(STB$OBJECT.getCurrentNodePackage(Stb$object.getCurrentNodeType,csToken),STB$OBJECT.getCurrentNodePackage);

  isr$trace.debug('getCurrentNodePackage', sPackage, sCurrentName);
  for rDialog in cDialog(sUpdateAllowed) loop

    sPrimaryKey := 'F';
    for rPrimaryKeys in cPrimaryKeys(sTable) loop
      if rDialog.parametername = rPrimaryKeys.column_name then
        sPrimaryKey := 'T';
      end if;
    end loop;
    
    sHeader :=  sHeader ||
                   ', XMLELEMENT("' || rDialog.parametername || '", XMLATTRIBUTES(''' || rDialog.parametertype || ''' as "type"
                    , '''|| rDialog.haslov || ''' as "haslov"
                    , '''|| rDialog.format || ''' as "format"
                    , '|| case
                              when NVL(INSTR(rDialog.parametervalue,':exec:'),0) > 0 then '('||REPLACE(rDialog.parametervalue, ':exec:')||')'
                              else ''''||rDialog.parametervalue||''''
                           end || ' as "defaultvalue"
                    , '''|| rDialog.editable || ''' as "editable"
                    , '''|| rDialog.displayed || ''' as "displayed"
                    , '''|| sPrimaryKey || ''' as "primaryKey"
                    , '''|| rDialog.required || ''' as "required"), '''
                    || rDialog.headerValue || ''')';

   sXmlelement := sXmlelement || ', XMLELEMENT("' || rDialog.parametername || '",' || 
       case 
        when rDialog.haslov in ('T', 'W') then 
           ' isr$obj$base.getSelectedLovDisplay('''||rDialog.parametername||''', '||rDialog.parametername||', ''' || sPackage || ''') ' 
        when rDialog.parametertype = 'FILE' then
           ' stb$java.getBlobByteString(' || rDialog.parametername || ')'
       else rDialog.parametername 
       end||')';

  end loop;


  open cGetXml for 'select XMLELEMENT("HEADSET" ' || sHeader || ') from dual';
  loop
    fetch cGetXml into xHeader;
    exit when cGetXml%notfound;
  end loop;

  sSql := 'select  XMLAGG(XMLELEMENT("ROW"' || sXmlelement || ')) from (select * from ' || sTable ||')';

  for rGetWhereClause in cGetWhereClause loop
    if TRIM(rGetWhereClause.sWhere) is not null then
      sSql := SUBSTR(sSql, 0 , LENGTH(sSql)-1) || ' where '||rGetWhereClause.sWhere ||')';
    end if;
  end loop;

  for rGetSortOrder in cGetSortOrder loop
    if TRIM(rGetSortOrder.orderby) is not null then
      sSql := SUBSTR(sSql, 0 , LENGTH(sSql)-1) || ' order by '||rGetSortOrder.orderby ||')';
    end if;
  end loop;

  isr$trace.debug('statement','statement',scurrentName, sSql);

  open cGetXml for sSql;
  loop
    fetch cGetXml into xXml;
    exit when cGetXml%notfound;
  end loop;

  select XMLELEMENT ("DOCUMENT"
                   , XMLELEMENT ("PROPERTIES"
                               , XMLELEMENT ("TABLE", sTable)
                               , XMLELEMENT ("INSERT_ALLOWED", sInsertAllowed)
                               , XMLELEMENT ("DELETE_ALLOWED", sDeleteAllowed))
                   , xHeader
                   , XMLELEMENT ("ROWSET", xXml))
    into xXml1
    from DUAL;

  olNodeList (1).clPropertyList :=  ISR$XML.XMLToClob(xXml1);

  isr$trace.debug('parameter','olNodeList OUT',sCurrentName,olNodeList);  
  isr$trace.stat('end', 'end', sCurrentName);
  return oErrorObj;
exception
  when others then
    sMsg :=  utd$msglib.getmsg ('exUnexpectedError', isr$obj$base().nCurrentLanguage, csP1 => SQLERRM(SQLCODE));
    oErrorObj.handleError ( Stb$typedef.cnSeverityCritical, sMsg, sCurrentName,
                            DBMS_UTILITY.format_error_stack || CHR(10) || DBMS_UTILITY.format_error_backtrace, SQLCODE );
    return oErrorObj;  
end buildGridMask;

member function executeStatement(sAction varchar2, sTable varchar2, sRowId varchar2, sParameter varchar2, sStmt IN OUT varchar2, 
                sOldValue IN OUT varchar2, sNewValue varchar2)
return boolean is
 sCurrentName        constant varchar2(300) := $$PLSQL_UNIT||'.executeStatement(' || sAction || ', ' || sParameter || ')';
 oErrorObj         STB$OERROR$RECORD := STB$OERROR$RECORD();
 begin
    isr$trace.stat('begin', 'begin', sCurrentName);
    if sAction in ('UPDATE', 'DELETE') then          
      sStmt := 'SELECT '||sParameter||' FROM '||sTable||' WHERE ROWID = '''||sRowId||'''';
      isr$trace.debug ('sStmt', sStmt, sCurrentName);
      execute immediate sStmt into sOldValue;
      isr$trace.debug ('sOldValue', sOldValue, sCurrentName);
    end if;

    if sAction in ('INSERT', 'UPDATE') then          
      if NVL(sOldValue, '#@#') != NVL(sNewValue, '#@#') then
        sStmt := 'UPDATE '||sTable||' SET '||sParameter||' = '''||REPLACE(sNewValue, '''', '''''')||''' WHERE ROWID = ''' || sRowId || ''' ';
        isr$trace.debug ('sStmt', sStmt, sCurrentName);
        execute immediate sStmt;
      end if;
    end if;
    isr$trace.stat('end', 'end', sCurrentName);
    return true;
  exception
  when others then
    oErrorObj.handleError ( Stb$typedef.cnSeverityCritical, 
              utd$msglib.getmsg ('exUnexpectedError', isr$obj$base().nCurrentLanguage, csP1 => SQLERRM(SQLCODE)), 
              sCurrentName, DBMS_UTILITY.format_error_stack || CHR(10) || DBMS_UTILITY.format_error_backtrace, SQLCODE );
    return false; 
 end executeStatement;
--member function test_em(sTable varchar2) return boolean is
--begin
--isr$trace.debug ('test', 'test', 'grid');
--return true;
--end;

end;