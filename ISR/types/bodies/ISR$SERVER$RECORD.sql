CREATE OR REPLACE TYPE BODY ISR$SERVER$RECORD as
-- nCurrentLanguage constant  number  := stb$security.getcurrentlanguage;


--******************************************************************************
static function getCheckForDialog return varchar2 is
begin
  return isr$server$base.sCheckForDialog;
end getCheckForDialog;


-- *****************************************************************************
static procedure setCheckForDialog(csCheckForDialog in varchar2) is
begin
  isr$server$base.sCheckForDialog := csCheckForDialog;
end setCheckForDialog;


-- *****************************************************************************
static  function checkDependOnMaskForTable( oParameter    in     STB$MENUENTRY$RECORD,
                                          olParameter   in out STB$PROPERTY$LIST,
                                          sModifiedFlag out    varchar2,
                                          sTable        in   varchar2  default 'ISR$SERVER' )
  return STB$OERROR$RECORD
is
  sCurrentName    constant varchar2(100) := $$PLSQL_UNIT||'.checkDependOnMaskForTable (' || oParameter.sToken  || ')';
  oErrorObj       STB$OERROR$RECORD := STB$OERROR$RECORD();
  sMsg            varchar2(4000);
  olParameterOld  STB$PROPERTY$LIST := STB$PROPERTY$LIST();
  oSelectionList  ISR$TLRSELECTION$LIST := ISR$TLRSELECTION$LIST();

  sKey1           ISR$DIALOG.PARAMETERNAME%type;
  sKeyValue1      ISR$DIALOG.PARAMETERVALUE%type;
  sStmt   varchar2(2000);
begin
  isr$trace.stat('begin','begin '||oParameter.sToken, sCurrentName);
-- isr$trace.debug('status oParameter', oParameter.toString('oParameter'), sCurrentName);
  isr$trace.debug('begin status', 'olParameter, see column LOGCLOB' ,sCurrentName, sys.anydata.convertCollection(nvl(olParameter, STB$PROPERTY$LIST())));

  olParameterOld := olParameter;

  case
    ------------ modify parameter entities ----------------------
    ------------ delete parameter entities ----------------------
    when    Upper(oParameter.sToken) =  'CAN_MODIFY_SERVER'
         or Upper(oParameter.sToken) = 'CAN_DELETE_SERVER'  then
      sKey1      := 'SERVERID';
      sKeyValue1 := STB$UTIL.getProperty(olParameter, sKey1);
      if sKeyValue1 is not null then
        -- fill the hidden field -> the values are selected only once then
        if olParameter(2).sParameter != sKeyValue1 then
          olParameter(2).sParameter := sKeyValue1;
          for i in 3..olParameter.count() loop
            if olParameter(i).type != 'TABSHEET' then
              begin
                sStmt :=  'SELECT NVL('||case
                                                   when olParameter(i).type LIKE '%PASSWORD%' then 'isr$util.decryptpassword('||olParameter(i).sParameter||')'
                                                   else olParameter(i).sParameter
                                                 end||'
                                             , '''||olParameter(i).sValue||''') '||
                                  'FROM '||sTable||' '||
                                  'WHERE '||sKey1||' = '''||replace(sKeyValue1, '''', '''''')||'''';
                isr$trace.info('sStmt', sStmt, sCurrentName);
                execute immediate sStmt   into olParameter(i).sValue;
                olParameter(i).sDisplay := olParameter(i).sValue;
              exception when others then
                null;
              end;
              if olParameter(i).hasLov = 'T' then
                oErrorObj := getLov (olParameter(i).sParameter, olParameter(i).sValue, oSelectionList);
                if oSelectionList.first is not null then
                  for j in 1..oSelectionList.count() loop
                    if oSelectionList(j).sSelected = 'T' then
                      olParameter(i).sDisplay := oSelectionList(j).sDisplay;
                    end if;
                  end loop;
                end if;
              end if;
            end if;
            isr$trace.info('olParameter', olParameter(i).sParameter|| ' '||olParameter(i).sValue, sCurrentName);
          end loop;
        end if;
      end if;
    else
      -- all other tokens
      null;
  end case;

  -- compare the objects if something changed only then refresh the mask
  select case when count ( * ) = 0 then 'F' else 'T' end
  into   sModifiedFlag
  from ( select xmltype (value (p)).getStringVal ()
         from   table (olParameter) p
         minus
         select xmltype (value (p)).getStringVal ()
         from   table (olParameterOld) p);

   isr$trace.debug('end status', 'olParameter, see column LOGCLOB' ,sCurrentName, sys.anydata.convertCollection(nvl(olParameter, STB$PROPERTY$LIST())));
   isr$trace.stat('end','sModifiedFlag '||sModifiedFlag, sCurrentName);
  return oErrorObj;
exception
  when others then
    sMsg :=  utd$msglib.getmsg ('exUnexpectedError', stb$security.getcurrentlanguage, csP1 => sqlerrm(sqlcode));
    oErrorObj.handleError ( Stb$typedef.cnSeverityCritical, sMsg, sCurrentName,
                            substr(dbms_utility.format_error_stack || CHR(10) || dbms_utility.format_error_backtrace, 1,4000)  );
    return oErrorObj;
end checkDependOnMaskForTable;


-- *****************************************************************************
static function callFunction( oParameter in STB$MENUENTRY$RECORD,
                       olNodeList out STB$TREENODELIST)  return STB$OERROR$RECORD
  is
begin
  return callFunction(oParameter,olNodeList,'ISR$SERVER');
end callFunction;


-- *****************************************************************************
static function callFunction( oParameter in STB$MENUENTRY$RECORD,
                       olNodeList out STB$TREENODELIST, sTable    in   varchar2  )  return STB$OERROR$RECORD
is
  sCurrentName constant varchar2(100) := $$PLSQL_UNIT||'.callFunction (' || oParameter.sToken  || ')';
  exNoServerStartAllowed exception;
  exNoServerStopAllowed  exception;
  exRemoteServerstart    exception;
  exRemoteServerStop   exception;
  sMsg         varchar2(4000);
  sExists      varchar2(1);
  sToken       varchar2(500);
  nServerId    number;
  oCurNode     STB$TREENODE$RECORD := STB$TREENODE$RECORD();
  nSeverity    number              := stb$typedef.cnNoError;
  oErrorObj    STB$OERROR$RECORD   := STB$OERROR$RECORD();
  sMenuToken   varchar2(500)       := UPPER (oParameter.sToken);
  sJavaLogLevel isr$loglevel.javaloglevel%type;
  sRemoteEx              clob;
  csDefaultNullString  constant char(6)                           := '(null)';
  sServerName   isr$serverobserver$v.alias%type;

  cursor cGetServer is
   select s.serverid, s.observerid, s.alias, s.runstatus, s.classname, s.context,
            s.namespace, s.observer_port observerPort,
            s.observer_host observerHost,  s.host,  s.port,
            s.server_path, s.servertypeid, s.targetmodelid,
            s.timeout, s.observer_timeout,
            s.java_startoptions, nvl(serverstatus, -1) serverstatus
    from  isr$serverobserver$v s
    where  s.serverid = STB$OBJECT.getCurrentNodeId;

   rGetServer cGetServer%rowtype;

procedure stopServer is
  begin
      sServerName := rGetServer.alias;
      isr$trace.debug('rGetServer.host, rGetServer.observerPort', rGetServer.observerHost||';  '|| rGetServer.observerPort, sCurrentName);
      sRemoteEx := isr$server$base.stopRemoteServer(rGetServer.observerHost, rGetServer.observerPort,  rGetServer.serverid, isr$trace.getJavaUserLoglevelStr, rGetServer.timeout);
      if sRemoteEx is not null then
        ISR$TRACE.debug('CAN_STOP_SERVER error', 'error ', sCurrentName,sRemoteEx);
        oErrorObj.handleJavaError (Stb$typedef.cnSeverityWarning, xmltype( sRemoteEx), sCurrentName);
        sMsg := sMsg || Utd$msglib.GetMsg ('exCouldNotStopServerText', Stb$security.GetCurrentLanguage, csP1 => 'serverId = ' || rGetServer.serverid);
       raise exRemoteServerStop;
      end if;
      isr$server$base.setServerStatus(rGetServer.serverid, 5);
      ISR$TREE$PACKAGE.sendSyncCall('OBSERVER', rGetServer.observerid);
    exception
      when others then
        oErrorObj.handleError ( Stb$typedef.cnSeverityWarning, sMsg, sCurrentName, dbms_utility.format_error_stack ||Stb$typedef.crlf|| dbms_utility.format_error_backtrace);
  
  end stopServer;

begin
  isr$trace.stat('begin',  'parameter: ' || oParameter.sToken, sCurrentName, xmltype(Stb$object.getCurrentNode).getClobVal());
  isr$trace.debug('status oParameter', oParameter.toString('oParameter'), sCurrentName);
  sJavaLogLevel := isr$trace.getJavaUserLoglevelStr;

  setCheckForDialog('F');

  STB$OBJECT.setCurrentToken(oparameter.stoken);
  -- get the current node
  oCurNode  := Stb$object.getCurrentNode;
  nServerId := STB$OBJECT.getCurrentNodeid;
  
  
  open cGetServer;
  fetch cGetServer into rGetServer;

  case
    -- ================== Dialogs
    when Upper (sMenuToken) = 'CAN_MODIFY_SERVER' then

      if cGetServer%found then
        sToken := 'CAN_MODIFY_'|| rGetServer.servertypeid||'_SERVER';
        isr$trace.debug ('sToken',  sToken, sCurrentName);
        olNodeList := STB$TREENODELIST ();
        olNodeList.extend;
        olNodeList (1) := STB$TREENODE$RECORD(STB$UTIL.getMaskTitle(Upper (oParameter.sToken)),  $$PLSQL_UNIT, 'TRANSPORT', 'MASK', '');
        oErrorObj := STB$UTIL.getPropertyList(sToken, olNodeList (1).tloPropertylist);
        oErrorObj := checkDependOnMaskForTable(oParameter, olNodeList (1).tloPropertylist, sExists, sTable);
      end if;
      
      isr$trace.debug ('Upper (oParameter.sToken)',  upper (oParameter.sToken), sCurrentName);

    -- ==================
    when upper (sMenuToken) = 'CAN_DELETE_SERVER' then

      for rGetChildServer in (select serverid from isr$server where observerid = nserverid) loop
        oErrorObj := isr$server$base.deleteserver(rGetChildServer.serverid, oParameter.sToken);
      end loop;
      oErrorObj :=  isr$server$base.deleteserver(nServerId, oParameter.sToken);
      ISR$TREE$PACKAGE.selectCurrent;

    -- ==================
    when upper (sMenuToken) = 'CAN_START_SERVER' then

      sServerName := rGetServer.alias;
      isr$trace.debug('status', rGetServer.runstatus || ' ' || STB$OBJECT.getCurrentNodeId, sCurrentName);
      if cGetServer%found and NVL(rGetServer.runstatus, 'F') = 'F' then
        isr$trace.debug('rGetServer.host, rGetServer.port', rGetServer.observerHost||';  '|| rGetServer.port, sCurrentName);
        sRemoteEx := iSR$Server$base.startRemoteServer(rGetServer.observerHost, rGetServer.observerPort, rGetServer.port, rGetServer.className,
                                                rGetServer.context, rGetServer.namespace, rGetServer.serverid, rGetServer.server_path, rGetServer.java_startoptions, isr$trace.getJavaUserLoglevelStr,
                                                isr$trace.getReference, rGetServer.observer_timeout);
        if sRemoteEx is not null then
          isr$trace.debug_all('CAN_START_SERVER error', 'error ', sCurrentName, sRemoteEx);
          oErrorObj.handleJavaError( Stb$typedef.cnSeverityWarning,  sRemoteEx, sCurrentName );
          sMsg :=  'observerHost=' || NVL( rGetServer.observerHost, csDefaultNullString)
                   ||' '|| 'observerPort=' || NVL( rGetServer.observerPort, csDefaultNullString)
                   ||' '|| 'port='         || NVL( rGetServer.port, csDefaultNullString)
                   ||' '|| 'className='    || NVL( rGetServer.className, csDefaultNullString)
                   ||' '|| 'context='      || NVL( rGetServer.context, csDefaultNullString)
                   ||' '|| 'namespace='    || NVL( rGetServer.namespace, csDefaultNullString)
                   ||' '|| 'server_path='    ||  rGetServer.server_path
                   ||' '|| 'logLevel='    ||  isr$trace.getJavaUserLoglevelStr
                   ||' '|| 'logReference='    ||  isr$trace.getReference
                   ||' '|| 'timeout='    ||  rGetServer.observer_timeout
                   ||' '|| 'serverid='     || NVL( to_char(rGetServer.serverid), csDefaultNullString);
          raise exRemoteServerstart;
        end if;
        isr$server$base.setServerStatus(rGetServer.serverid, 3);
        --ISR$TREE$PACKAGE.selectCurrent;
        ISR$TREE$PACKAGE.sendSyncCall('OBSERVER', rGetServer.observerid);
       else
        raise exNoServerStartAllowed;
      end if;
      
    -- ==================
    when upper (oParameter.sToken) = 'CAN_STOP_SERVER' THEN
      isr$trace.debug('CAN_STOP_SERVER status', rGetServer.runstatus || ' ' || STB$OBJECT.getCurrentNodeId, sCurrentName);
      if cGetServer%FOUND and NVL(rGetServer.runstatus, 'F') = 'T' then
        stopServer;
      else
        sMsg := sMsg || utd$msglib.getmsg ('exNoServerStopAllowed', stb$security.getcurrentlanguage) || Stb$typedef.cr;
        raise exNoServerStopAllowed;
      end if;
    -- ==================
    when upper (oParameter.sToken) = 'CAN_DISABLED_SERVER' THEN
      if cGetServer%FOUND and NVL(rGetServer.runstatus, 'F') = 'T' then
        stopServer;
      end if;
      isr$server$base.switchServerStatus(STB$OBJECT.getCurrentNodeId, 'T');
      ISR$TREE$PACKAGE.selectCurrent;
    -- ==================
    when upper (oParameter.sToken) = 'CAN_ENABLED_SERVER' THEN
      isr$server$base.switchServerStatus(STB$OBJECT.getCurrentNodeId, 'F');
      ISR$TREE$PACKAGE.selectCurrent;

    -- ==================
    when  Upper (oParameter.sToken) = 'CAN_DEPLOY_JAVA_RUNTIME' then

      oErrorObj := isr$server$base.deployJavaOnServer(rGetServer.observerHost, rGetServer.observerPort, rGetServer.servertypeid, rGetServer.server_path,  'jre', rGetServer.timeout);
      
      if  oErrorObj.sSeverityCode in (Stb$typedef.cnNoError)  then
        isr$server$base.setServerStatus(STB$OBJECT.getCurrentNodeId, 2);
        ISR$TREE$PACKAGE.selectCurrent;
        isr$trace.debug (' serverstatus updated', 'serverid=' || STB$OBJECT.getCurrentNodeId, sCurrentName);
      end if;

    -- ==================
    when  Upper (oParameter.sToken) = 'CAN_DEPLOY_JAVA_APPL' then

      for rGetTarget in (select distinct target from isr$server$deployment where servertypeid = rGetServer.servertypeid and target != 'jre') loop
        oErrorObj := isr$server$base.deployJavaOnServer(rGetServer.observerHost, rGetServer.observerPort, rGetServer.servertypeid, 
                       rGetServer.server_path,  rGetTarget.target, rGetServer.timeout);
      end loop;
      if  oErrorObj.sSeverityCode in (Stb$typedef.cnNoError)  then
        isr$server$base.setServerStatus(STB$OBJECT.getCurrentNodeId, 3);
        ISR$TREE$PACKAGE.selectCurrent;
        /*sMsg := utd$msglib.getmsg ('loadSuccessfullyTxt', stb$security.getcurrentlanguage);
        oErrorObj.handleError ( Stb$typedef.cnSeverityMessage, sMsg, sCurrentName,  'info'  );*/
        isr$trace.debug (' serverstatus updated', 'serverid=' || STB$OBJECT.getCurrentNodeId, sCurrentName);
      end if;

    -- ==================
    else

       sMsg := sMsg || utd$msglib.getmsg ('exNotExistsText', stb$security.getcurrentlanguage);
       setCheckForDialog('T');

  end case;  
  
  close cGetServer;

  if oErrorObj.sSeverityCode < nSeverity then
    oErrorObj.setSeverityCode(nSeverity);
  end if;

  isr$trace.stat ('end',  'end', sCurrentName);

  return oErrorObj;
exception
 when exNoServerStartAllowed then
   oErrorObj.handleError ( Stb$typedef.cnSeverityWarning, sMsg, sCurrentName,  -- sCurrentName + additional parameter info etc. bei bedarf
                            substr(dbms_utility.format_error_stack ||stb$typedef.crlf|| dbms_utility.format_error_backtrace, 1,4000)  );
    return oErrorObj;
 when exNoServerStopAllowed then
   oErrorObj.handleError ( Stb$typedef.cnSeverityWarning, sMsg, sCurrentName,  -- sCurrentName + additional parameter info etc. bei bedarf
                            substr(dbms_utility.format_error_stack ||stb$typedef.crlf|| dbms_utility.format_error_backtrace, 1,4000)  );
    return oErrorObj;
 when exRemoteServerstart then
    sMsg := utd$msglib.getmsg('exRemoteServerstart', stb$security.getcurrentlanguage, sServerName, sMsg );
    oErrorObj.handleError ( Stb$typedef.cnSeverityWarning, sMsg, sCurrentName,  -- sCurrentName + additional parameter info etc. bei bedarf
                            substr(dbms_utility.format_error_stack ||stb$typedef.crlf|| dbms_utility.format_error_backtrace, 1,4000)  );
    return oErrorObj;
  when exRemoteServerStop then
    sMsg := utd$msglib.getmsg('exRemoteServerStop', stb$security.getcurrentlanguage, sServerName, sMsg );
    oErrorObj.handleError ( Stb$typedef.cnSeverityWarning, sMsg, sCurrentName,  -- sCurrentName + additional parameter info etc. bei bedarf
                            substr(dbms_utility.format_error_stack ||stb$typedef.crlf|| dbms_utility.format_error_backtrace, 1,4000)  );
    return oErrorObj;
  when others then
    rollback;
    sMsg :=  utd$msglib.getmsg ('exUnexpectedError', stb$security.getcurrentlanguage, csP1 => 'serverid='||nServerId || 'server name = ' || sserverName, csP2 => sqlerrm(sqlcode));
    oErrorObj.handleError ( Stb$typedef.cnSeverityCritical, sMsg, sCurrentName,
                            substr(dbms_utility.format_error_stack ||stb$typedef.crlf || dbms_utility.format_error_backtrace, 1,4000)  );
    return oErrorObj;
end callFunction;


-- *****************************************************************************
static function putParameters( oParameter  in STB$MENUENTRY$RECORD,
                               olParameter in STB$PROPERTY$LIST,
                               clParameter in clob  default null ) return STB$OERROR$RECORD
is
  exTokenNotExists exception;
  sCurrentName     constant varchar2(100) := $$PLSQL_UNIT||'.putParameters ('||oParameter.sToken||')';
  oErrorObj        STB$OERROR$RECORD      := STB$OERROR$RECORD();
  sMsg             varchar2(4000);
  sKeyValue1       ISR$DIALOG.PARAMETERVALUE%type;
  sRemoteEx              clob;

  cursor cGetServerStatus(nServerId in number)  is
    select serverstatus
    from   isr$server
    where  serverid = nServerId;

  nServerStatus    number;

begin
  isr$trace.stat('begin','begin', sCurrentName);
  isr$trace.debug('parameter','olParameter',sCurrentName,olParameter);

  STB$OBJECT.setCurrentToken(oparameter.stoken);
  sKeyValue1 := STB$UTIL.getProperty (olParameter, 'SERVERID');
  open cGetServerStatus(sKeyValue1);
  fetch cGetServerStatus into nServerStatus;
  close cGetServerStatus;

  case
    -- ================== modify (one key)
    when Upper (oParameter.sToken) = 'CAN_ADD_SERVER'
      or Upper (oParameter.sToken) = 'CAN_MODIFY_SERVER'
      or Upper (oParameter.sToken) = 'CAN_DELETE_SERVER'  then

      isr$trace.debug('value','sKeyValue1: '||sKeyValue1, sCurrentName);
      oErrorObj := isr$server$base.saveServerDialogData(oParameter, olParameter);
      if Upper (oParameter.sToken) = 'CAN_ADD_SERVER' then

        insert into STB$SYSTEMPARAMETER (PARAMETERNAME
                                       , PARAMETERVALUE
                                       , DESCRIPTION
                                       , PARAMETERTYPE
                                       , USERPARAMETER
                                       , EDITABLE
                                       , HASLOV
                                       , VISIBLE
                                       , ORDERNO
                                       , REQUIRED
                                       , XML)
           (SELECT 'PARA_DISPLAYED_SERVER_'
                   || STB$UTIL.getProperty (olParameter, 'SERVERID')
                 , 'T'
                 , utd$msglib.getmsg ('SHOW_USER_PARAMETER'
                                    , stb$security.getcurrentlanguage)
                   || utd$msglib.getmsg ('USE_SERVER_IN_WIZARD_'
                                       , stb$security.getcurrentlanguage)
                   || STB$UTIL.getProperty (olParameter, 'ALIAS')
                 , 'BOOLEAN'
                 , 'T'
                 , 'T'
                 , 'F'
                 , 'T'
                 , 1000
                 , 'T'
                 , 'F'
              from DUAL
             where STB$UTIL.getProperty (olParameter, 'SERVERTYPEID') in (2));

        insert into STB$ADMINRIGHTS (PARAMETERNAME, PARAMETERVALUE, USERGROUPNO)
        select 'PARA_DISPLAYED_SERVER_' || STB$UTIL.getProperty (olParameter, 'SERVERID'), 'T', usergroupno
        from   stb$usergroup
        where  grouptype = 'FUNCTIONGROUP'
        and    STB$UTIL.getProperty (olParameter, 'SERVERTYPEID') in (2);
      end if;

      oErrorObj := STB$SYSTEM.getAuditType;
      isr$trace.debug ('oErrorObj.sSeverityCode; nServerStatus' , oErrorObj.sSeverityCode||';   '||nServerStatus, sCurrentName);
      
      if UPPER (oParameter.sToken) = 'CAN_MODIFY_SERVER'  and oErrorObj.sSeverityCode in (Stb$typedef.cnNoError, stb$typedef.cnAudit) then  -- vs [ISRC-718]

        if nServerStatus is null then
          for rGetServerType in (select servertypename, observer_host, observer_port, timeout
                                    from isr$serverobserver$v
                                   where serverid = sKeyValue1
                                   and observerid is not null) loop
            isr$trace.debug ('servertypename', rGetServerType.servertypename, sCurrentName);
            sRemoteEx := isr$server$base.createServerDirectory( rGetServerType.observer_host,
                                                              rGetServerType.observer_port,
                                                              rGetServerType.servertypename || ' ' || sKeyValue1,
                                                              isr$server$base.csJavaServerFolders,
                                                              ',',
                                                              isr$trace.getUserLoglevelStr, rGetServerType.timeout);

            if sRemoteEx is not null and sRemoteEx != 'T' then
             isr$trace.debug ('error',  'see sRemoteEx in logclob column', sCurrentName,  sRemoteEx);
             oErrorObj.handleJavaError (Stb$typedef.cnSeverityWarning, sRemoteEx, sCurrentName);
            else
              isr$server$base.setServerStatus(sKeyValue1, 0, 'F');
              isr$trace.debug (oParameter.sToken || ': serverstatus updated', 'serverid=' || sKeyValue1 || ' serverstatus = 1', sCurrentName);
              --commit;
            end if;
          end loop;                   
        end if;
        
        for rGetServerType in (select 1
                                  from isr$serverobserver$v
                                 where serverid = sKeyValue1
                                 and servertypeid = 5
                                 and isr$server$ldap.getLDAPRunStatus(serverid) = 'T') loop
          isr$server$base.setServerStatus(sKeyValue1, 1, 'F');                       
        end loop;      

        --isr$trace.debug ('oErrorObj.sSeverityCode' , oErrorObj.sSeverityCode||';   '||nServerStatus, sCurrentName);
      end if;

      -- ADD
      if upper (oParameter.sToken) = 'CAN_ADD_SERVER' then
        ISR$TREE$PACKAGE.selectCurrent;
      -- MODIFY
      elsif upper (oParameter.sToken) = 'CAN_MODIFY_SERVER' then
        if STB$OBJECT.getCurrentNodeType(-2) = 'SERVERS' then
          ISR$TREE$PACKAGE.selectCurrent;
        else
          ISR$TREE$PACKAGE.reloadPrevNodesAndSelectCurren(-1);
        end if;
      -- DELETE
      elsif upper (oParameter.sToken) = 'CAN_DELETE_SERVER' then
        ISR$TREE$PACKAGE.reloadPrevNodesAndSelect(-1);
      end if;

    -- ==================  CAN_DISPLAY_SERVER_LOG_LIST
    when  Upper (oParameter.sToken) = 'CAN_DISPLAY_SERVER_LOG_LIST' then

      oErrorObj := isr$server$base.downloadLogServerFiles(oParameter, olParameter);

    -- ==================  not existing
    else

      raise exTokenNotExists;

  end case;
--  commit;

  isr$trace.debug ('oErrorObj.sSeverityCode', oErrorObj.sSeverityCode, sCurrentName, olParameter);
  isr$trace.stat ('end', 'end', sCurrentName);
  return oErrorObj;
exception
  when exTokenNotExists then
    sMsg :=  utd$msglib.getmsg ('exTokenNotExists', stb$security.getcurrentlanguage, upper(oParameter.sToken));
    oErrorObj.handleError ( Stb$typedef.cnSeverityCritical, sMsg, sCurrentName,
                            substr(dbms_utility.format_error_stack ||stb$typedef.crlf|| dbms_utility.format_error_backtrace, 1,4000)  );
    return oErrorObj;
  when others then
    sMsg :=  utd$msglib.getmsg ('exUnexpectedError', stb$security.getcurrentlanguage, csP1 => sqlerrm(sqlcode));
    oErrorObj.handleError ( Stb$typedef.cnSeverityCritical, sMsg, sCurrentName,
                            substr(dbms_utility.format_error_stack ||stb$typedef.crlf|| dbms_utility.format_error_backtrace, 1,4000)  );
    return oErrorObj;
end putParameters;


-- *****************************************************************************
static function getLov( sEntity        in varchar2,
                 sSuchwert      in varchar2,
                 oSelectionList out ISR$TLRSELECTION$LIST ) return STB$OERROR$RECORD
is
  sCurrentName constant varchar2(100) := $$PLSQL_UNIT||'.getLov(' || sEntity || ')';
  sMsg         varchar2(4000);
  nCounter     number            := 0;
  oErrorObj    STB$OERROR$RECORD := STB$OERROR$RECORD();

  cursor cGetServer is
    select serverid, alias, host||':'||port info
    from   isr$server
    order by 2;

  cursor cGeObserver is
    select serverid, alias, host||':'||port info
    from   isr$server
    where  servertypeid=1
    order by 2;
 /* --plugin   is, 10.10.17 no more used?
  cursor cGetPlugins is
    select p.plugin,
           UTD$MSGLIB.getMsg(p.plugin, stb$security.GetCurrentLanguage)  AS TRANSLATION
    from   isr$plugin p
    where  plugintype='ENTITY'
    order by 1;  */
  --servertype
  cursor cGetServertype is
    select * from isr$validvalue$servertype
    where nodetype in('SERVER_GENERAL',  'OBSERVER');
    --targetmodel
  cursor cGetTargetmodel(cnServerId  in  isr$server.serverid%type) is
    select targetmodel_name, tm.servertypeid, tm.targetmodelid,
           UTD$MSGLIB.getMsg(targetmodel_name, stb$security.GetCurrentLanguage)  AS TRANSLATION
     from   isr$servertype$targetmodel tm, isr$server$servertype$v s
    where s.serverid = cnServerId
       and  s.servertypeid = tm.servertypeid
    order by 1;
begin
  isr$trace.stat('begin','sEntity='||sEntity,sCurrentName);
  oSelectionList := ISR$TLRSELECTION$LIST ();

  case
    --***************************************
    when upper (sEntity) = 'SERVER' then
      for rGetEntity in cGetServer loop
        nCounter := nCounter + 1;
        oSelectionList.extend;
        oSelectionList (nCounter) := ISR$OSELECTION$RECORD(rGetEntity.alias, rGetEntity.serverid, rGetEntity.alias, null, nCounter);
      end loop;
    --***************************************
      -- Observer ---------------------------------------------------------------
    when UPPER(sEntity) = 'OBSERVERID' then
      nCounter :=0;
      for rGetObserver in cGeObserver  loop
        nCounter := nCounter+1;
        oSelectionList.extend;
        oSelectionList (nCounter) := ISR$OSELECTION$RECORD(rGetObserver.alias, rGetObserver.serverid, rGetObserver.info, null, nCounter);
      end loop;
    --***************************************
   /* WHEN Upper (sEntity) = 'PLUGIN' THEN
      FOR rGetPlugins IN cGetPlugins LOOP
        nCounter := nCounter + 1;
        oSelectionList.EXTEND;
        oSelectionList (nCounter) := ISR$OSELECTION$RECORD(rGetPlugins.plugin, rGetPlugins.plugin, rGetPlugins.translation, null, nCounter);
      END LOOP;  */
    --***************************************
    when Upper (sEntity) = 'SERVERTYPEID' then
      for rGetServertype in cGetServertype loop
        nCounter := nCounter + 1;
        oSelectionList.extend;
        oSelectionList (nCounter) := ISR$OSELECTION$RECORD(rGetServertype.servertypename, rGetServertype.servertypeid, rGetServertype.servertypename, null, nCounter);
      end loop;
    --***************************************
     WHEN Upper (sEntity) = 'TARGETMODELID' THEN
      isr$trace.stat('STB$OBJECT.getCurrentNodeId', STB$OBJECT.getCurrentNodeId, sCurrentName);
      FOR rGetTargetmodel IN cGetTargetmodel ( STB$OBJECT.getCurrentNodeId ) LOOP
        nCounter := nCounter + 1;
        oSelectionList.EXTEND;
        oSelectionList (nCounter) := ISR$OSELECTION$RECORD(rGetTargetmodel.targetmodel_name, rGetTargetmodel.targetmodelid, rGetTargetmodel.translation, null, nCounter);
      END LOOP;
    --***************************************
    else
      execute immediate
      'BEGIN
         :1 :='|| stb$util.getcurrentcustompackage || '.getLov(:2,:3,:4);
       END;'
      using out oErrorObj,  in sentity, in ssuchwert, out oselectionlist;
  end case;

  if sSuchWert is not null and oselectionlist is not null and oselectionlist.first is not null then
    isr$trace.debug ('sSuchWert',  sSuchWert, sCurrentName);
    for i IN 1..oSelectionList.count() loop
      if oSelectionList(i).sValue = sSuchWert then
        isr$trace.debug ('oSelectionList('||i||').sSelected', oSelectionList(i).sValue, sCurrentName);
        oSelectionList(i).sSelected := 'T';
      end if;
    end loop;
  end if;

  isr$trace.stat ('end', 'oSelectionList.count() = ' || oSelectionList.count(), sCurrentName);
  return oErrorObj;
exception
  when others then
    sMsg :=  utd$msglib.getmsg ('exUnexpectedError', stb$security.getcurrentlanguage, csP1 => sqlerrm(sqlcode));
    oErrorObj.handleError ( Stb$typedef.cnSeverityCritical, sMsg, sCurrentName,
                            substr(dbms_utility.format_error_stack || CHR(10) || dbms_utility.format_error_backtrace, 1,4000)  );
    return oErrorObj;
end getLov;


-- *****************************************************************************
static function setMenuAccess( sParameter  in  varchar2,
                       sMenuAllowed out varchar2 )  return STB$OERROR$RECORD
is
  sCurrentName constant varchar2(100) := $$PLSQL_UNIT||'.setMenuAccess(' || sParameter || ')';
  oErrorObj    STB$OERROR$RECORD := STB$OERROR$RECORD();
  sMsg        varchar2(4000);

  cursor cGetServer is
    select s.runstatus, observer_runstatus observerStatus,  s.host,  s.port, nvl(s.serverstatus, -1) serverstatus, disabled
    from   isr$serverobserver$v s
    where  s.serverid = STB$OBJECT.getCurrentNodeId;

    rGetServer    cGetServer%rowtype;
begin
  isr$trace.info_all('begin', 'sParameter: ' || sParameter || ' '  || sMenuAllowed,  sCurrentName);
  /* nb - 3 Jun 16 - allow use of result_cache */
  -- oErrorObj := Stb$security.checkGroupRight (sParameter, sMenuAllowed);
  sMenuAllowed := Stb$security.checkGroupRight (sParameter);
  isr$trace.info_all('sMenuAllowed', 'sMenuAllowed: ' || sMenuAllowed,  sCurrentName);

  if sMenuAllowed = 'T' then

    open cGetServer;
    fetch cGetServer into rGetServer;
    close cGetServer;

    if rGetServer.disabled = 'T' then
      sMenuAllowed := case when sParameter = 'CAN_ENABLED_SERVER' then 'T' else 'F' end;
    else
      case
        when sParameter in ('CAN_START_SERVER') then
          if nvl(rGetServer.observerStatus, 'F') = 'F' or  nvl(rGetServer.runstatus, 'F') = 'T' or rGetServer.serverstatus < 3 then
            sMenuAllowed := 'F';
          end if;
        when sParameter in ( 'CAN_STOP_SERVER') then
          if  nvl(rGetServer.runstatus, 'F') = 'F' then
            sMenuAllowed := 'F';
          end if;
        /*when sParameter in ( 'CAN_MODIFY_SERVER') then
          if  nvl(rGetServer.runstatus, 'F') = 'T' then
            sMenuAllowed := 'F';
          end if;*/
        when sParameter in ( 'CAN_DEPLOY_JAVA_RUNTIME') then
          if  nvl(rGetServer.runstatus, 'F') = 'T' or rGetServer.serverstatus < 0 then
            sMenuAllowed := 'F';
          end if;
        when sParameter in ('CAN_DEPLOY_JAVA_APPL') then
          if  nvl(rGetServer.runstatus, 'F') = 'T' or rGetServer.serverstatus < 1 then
            sMenuAllowed := 'F';
          end if;
        when sParameter in ( 'CAN_DISPLAY_SERVER_LOG_LIST') then
         if  rGetServer.serverstatus < 2 and rGetServer.host is null then  -- host is not null: observer, has no status in servers workflow
            sMenuAllowed := 'F';
          end if;
        when  sParameter in ( 'CAN_MAP_SERVER') then
         if  rGetServer.serverstatus < 1 then
            sMenuAllowed := 'F';
          end if;
        when sParameter in ('CAN_DEPLOY_REMOTE') then
          if  nvl(rGetServer.runstatus, 'F') = 'F' then
            sMenuAllowed := 'F';
          end if;
        when sParameter = 'CAN_ENABLED_SERVER' then
          sMenuAllowed := 'F';
        else
          sMenuAllowed :=  'T';
      end case;
    end if;
  end if;

  isr$trace.info_all ('end',  'sMenuAllowed:' || sMenuAllowed, sCurrentName);
  return oErrorObj;
exception
  when others then
    sMsg :=  utd$msglib.getmsg ('exUnexpectedError', stb$security.getcurrentlanguage, csP1 => sqlerrm(sqlcode));
    oErrorObj.handleError ( Stb$typedef.cnSeverityCritical, sMsg, sCurrentName,
                            substr(dbms_utility.format_error_stack || CHR(10) || dbms_utility.format_error_backtrace, 1,4000)  );
    return oErrorObj;
end setMenuAccess;



-- *****************************************************************************
static function getNodeDetails(  clXML      out clob) return STB$OERROR$RECORD
is
  sCurrentName constant varchar2(100) := $$PLSQL_UNIT||'.getNodeDetails';
  oErrorObj    stb$oerror$record      := STB$OERROR$RECORD();
  sUserMsg     varchar2(4000);
  xStatusXml    xmltype;
  cursor cGetServer is
    select s.runstatus, nvl( s. observer_host, s.host) as host, s.port,  s.context, s.namespace
    from   isr$serverobserver$v s
    where  s.serverid = STB$OBJECT.getCurrentNodeId;

  rGetServer    cGetServer%rowtype;
begin
  isr$trace.info('begin', 'static function '||sCurrentName||' started.', sCurrentName);
  open cGetServer;
  fetch cGetServer into rGetServer;
  close cGetServer;
  if  nvl(rGetServer.runstatus, 'F') = 'T' then
    clXML :=  getServerStatus (rGetServer.host, rGetServer.port, rGetServer.context, rGetServer.namespace,  isr$trace.getUserLoglevelStr, isr$trace.getReference, 10);
  else
    select  xmlelement("status", utd$msglib.getmsg ('serverNotRunning', stb$security.getcurrentlanguage))
    into xStatusXml
    from dual;
    clXML := xStatusXml.getClobVal();
  end if;

  isr$trace.info('end', 'static function '||sCurrentName||' finished',sCurrentName);
  return (oErrorObj);
exception
 when others then
  isr$trace.info('sqlcode','sqlcode: '||sqlcode, sCurrentName);
  sUserMsg := utd$msglib.getmsg ('exUnexpectedError', stb$security.getcurrentlanguage, sqlerrm(sqlcode));
  oErrorObj.handleError ( Stb$typedef.cnSeverityCritical, sUserMsg, sCurrentName, substr(dbms_utility.format_error_stack ||stb$typedef.crlf|| dbms_utility.format_error_backtrace, 1,4000) );
  return oErrorObj;
end getNodeDetails;


-- *****************************************************************************
static function GetNodeChilds ( oSelectedNode in  STB$TREENODE$RECORD,
                         olNodeList    out STB$TREENODELIST ) return STB$OERROR$RECORD
is
  sCurrentName       constant varchar2(100) := $$PLSQL_UNIT||'.GetNodeChilds('||oSelectedNode.sNodeType||')';
  oErrorObj    STB$OERROR$RECORD := STB$OERROR$RECORD();
  sMsg         varchar2(4000);

  sStatus      varchar2(1);
  nCount       number;
  nDateDiff    number;
  nPingPeriod  number;

begin
  isr$trace.stat ('begin',  'begin', sCurrentName);
  olNodeList := STB$TREENODELIST ();

  --=========================SERVERS=========================
  if oSelectedNode.sNodetype = 'SERVERS' then

    for rGetNodes in (SELECT serverid
                           , alias
                           , st.package
                           , CASE
                                WHEN s.servertypeid = 1 THEN
                                      st.iconid
                                   || '_'
                                   || CASE
                                         WHEN s.runstatus = 'F' THEN 'ERROR'
                                         WHEN s.runstatus = 'T' THEN 'OK'
                                         ELSE 'UNKNOWN'
                                      END
                                ELSE
                                   st.iconid
                             END
                                iconid
                           , st.nodetype
                           , s.servertypeid
                           , HOST
                           , port
                           , s.timeout
                           , s.modifiedon
                        FROM isr$server s, isr$validvalue$servertype st
                       WHERE observerid IS NULL
                         AND s.servertypeid = st.servertypeid
                      ORDER BY alias) loop
      olNodeList.extend;
      olNodeList( olNodeList.count()) := STB$TREENODE$RECORD( rGetNodes.alias, rGetNodes.package, rGetNodes.nodetype,
                                                              rGetNodes.serverid, rGetNodes.iconid,
                                                              tloValueList => oSelectedNode.tloValueList
                                                              );
      nDateDiff := isr$util.getDateDifferenceInSec(rGetNodes.modifiedon, sysdate);
      nPingPeriod := to_number(stb$util.getSystemParameter('OBSERVER_PING_PERIOD'));
      isr$trace.info ('ping date', 'date difference=' || nDateDiff || '; ping period=' || nPingPeriod, sCurrentName);
      IF rGetNodes.servertypeid = 1 and rGetNodes.host is not null and nDateDiff > 2*nPingPeriod THEN
        sStatus:= isr$server$base.isServerAvailable(rGetNodes.host, rGetNodes.port,  isr$trace.getUserLoglevelStr);
        nCount := isr$remote$service.setServerStatus(rGetNodes.host, rGetNodes.port, sStatus);
        isr$trace.debug ('server status', sStatus ||'; rows updated: ' || nCount, sCurrentName);
      ELSIF rGetNodes.servertypeid = 5 THEN
        sStatus:= isr$server$ldap.getLDAPRunStatus(rGetNodes.serverid);
        nCount := isr$remote$service.setServerStatus(rGetNodes.serverid, sStatus);
        isr$trace.debug ('server status', sStatus ||'; rows updated: ' || nCount, sCurrentName);
      ELSIF rGetNodes.servertypeid = 7 THEN
       -- sStatus:= isr$server$ldap.getLDAPRunStatus(rGetNodes.serverid);
        nCount := isr$remote$service.setServerStatus(rGetNodes.serverid, sStatus);
        isr$trace.debug ('server status', sStatus ||'; rows updated: ' || nCount, sCurrentName);
      END IF;
      oErrorObj := STB$UTIL.getColPropertyList(' SELECT HOST
                                                      , port
                                                      , servertypename
                                                      , runstatus
                                                      , description
                                                   FROM isr$server$servertype$v
                                                  WHERE serverid = '||rGetNodes.serverid
                                              , olNodeList (olNodeList.count()).tloPropertyList);
      STB$OBJECT.setDirectory( olNodeList (olNodeList.count()) );
    end loop;
  end if;

  isr$trace.debug ('olNodeList',  'end', sCurrentName, olNodeList);
  isr$trace.stat ('end', 'end', sCurrentName);
  return (oErrorObj);
exception
  when others then
    rollback;
    sMsg :=  utd$msglib.getmsg ('exUnexpectedError',  stb$security.getcurrentlanguage,  sqlerrm(sqlcode));
    oErrorObj.handleError ( Stb$typedef.cnSeverityCritical, sMsg, sCurrentName,
                            dbms_utility.format_error_stack || CHR(10) || dbms_utility.format_error_backtrace  );
    return oErrorObj;

end GetNodeChilds;


 -- *****************************************************************************
static function GetNodeList ( oSelectedNode in  STB$TREENODE$RECORD,
                              olNodeList    out STB$TREENODELIST )
  return STB$OERROR$RECORD
is
  exUserExc    exception;
  sCurrentName constant varchar2(100) := $$PLSQL_UNIT||'.getNodeList';
  oErrorObj    STB$OERROR$RECORD := STB$OERROR$RECORD();
  sMsg         varchar2(4000);
begin
  isr$trace.stat ('begin',  oSelectedNode.sNodeType, sCurrentName);

  oErrorObj := GetNodeChilds (oSelectedNode, olNodeList);

  if  oErrorObj.sSeverityCode != Stb$typedef.cnNoError then
    raise exUserExc;
  end if;
  isr$trace.stat ('end','end',sCurrentName);
  return (oErrorObj);
exception
  when others then
    rollback;
    sMsg :=  utd$msglib.getmsg ('exUnexpectedError', stb$security.getcurrentlanguage, sqlerrm(sqlcode));
    oErrorObj.handleError ( Stb$typedef.cnSeverityCritical, sMsg, sCurrentName,
                            dbms_utility.format_error_stack || CHR(10) || dbms_utility.format_error_backtrace );
    return oErrorObj;
end GetNodeList;


 -- *****************************************************************************
static function deleteServerDirectory( csObserverHost  in varchar2,  csObserverPort  in varchar2, csDirectoryName in varchar2,  csLoglevel  in varchar2, nTimeout NUMBER) return clob
as language java
name 'com.uptodata.isr.observerclient.ObserverClient.deleteServerDirectory( java.lang.String, java.lang.String, java.lang.String, java.lang.String, int ) return java.sql.Clob';


-- *****************************************************************************
static function setCurrentNode(oSelectedNode in STB$TREENODE$RECORD) return STB$OERROR$RECORD
is
  sCurrentName constant varchar2(100) := $$PLSQL_UNIT||'.setCurrentNode';
  oErrorObj    STB$OERROR$RECORD := STB$OERROR$RECORD();
  sMsg         varchar2(4000);
begin
  Isr$trace.stat('begin', 'begin', sCurrentName, xmltype(oSelectedNode).getClobVal());
  stb$object.destroyoldvalues;
  Stb$object.setCurrentNode (oSelectedNode);
  isr$trace.stat('end','end',sCurrentName);
  return oErrorObj;
exception
  when others then
    sMsg :=  utd$msglib.getmsg ('exUnexpectedError', stb$security.getcurrentlanguage,  sqlerrm(sqlcode));
    oErrorObj.handleError ( Stb$typedef.cnSeverityCritical, sMsg, sCurrentName,
                            dbms_utility.format_error_stack || CHR(10) || dbms_utility.format_error_backtrace  );
    return oErrorObj;
end setCurrentNode;


-- *****************************************************************************
static function getServerStatus(csObserverHost IN VARCHAR2, csObserverPort IN VARCHAR2, csContext IN VARCHAR2, csNamespace IN VARCHAR2,
                                      csLoglevel  IN VARCHAR2, csLogReference  IN VARCHAR2, nTimeout NUMBER) RETURN CLOB
AS LANGUAGE JAVA
NAME 'com.uptodata.isr.observerclient.ObserverClient.getServerStatus( java.lang.String, java.lang.String, java.lang.String, java.lang.String, java.lang.String, java.lang.String, int) return java.sql.Clob';


end;