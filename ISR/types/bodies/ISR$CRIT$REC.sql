CREATE OR REPLACE TYPE BODY                   "ISR$CRIT$REC" as 

  constructor function iSR$Crit$Rec(self in out nocopy iSR$Crit$Rec) return self as result is
  begin
    self.Key := null;
    self.MasterKey := null;
    self.Display := null;
    self.Info := null;    
    self.OrderNumber := null;
    return;
  end iSR$Crit$Rec;

--*********************************************************************************************************************************  
  constructor function iSR$Crit$Rec(self in out nocopy iSR$Crit$Rec, Key in varchar2, MasterKey in varchar2, Display in varchar2, Info in varchar2, OrderNumber in number) return self as result is
  begin
    self.Key := Key;
    self.MasterKey := MasterKey;
    self.Display := Display;
    self.Info := Info;    
    self.OrderNumber := OrderNumber;
    return;
  end iSR$Crit$Rec;
  
--*********************************************************************************************************************************
  map member function MapCrit return varchar2 is
  begin
    return self.Key||'#@#'||self.MasterKey||'#@#'||self.Display||'#@#'||self.Info||'#@#'||to_char(self.OrderNumber);
  end MapCrit;
  
end;