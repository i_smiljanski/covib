CREATE OR REPLACE TYPE BODY                   STB$MENUENTRY$RECORD as
                                      
  constructor function STB$MENUENTRY$RECORD(self in out nocopy STB$MENUENTRY$RECORD, 
                                            sType        varchar2 default null,
                                            sDisplay     varchar2 default null,
                                            sToken       varchar2 default null,
                                            sHandledBy   varchar2 default null,
                                            sIcon        varchar2 default null,                                             
                                            sConfirm     varchar2 default null,
                                            sConfirmText varchar2 default null,
                                            nID          number default null) return self as result is
  begin
    self.sType := sType; 
    self.sDisplay := sDisplay;  
    self.sToken := sToken; 
    self.sHandledby := sHandledby; 
    self.sIcon := sIcon; 
    self.sConfirm := sConfirm; 
    self.sConfirmText := sConfirmText; 
    self.nId := nId; 
    return;
  end STB$MENUENTRY$RECORD;                                         

----------------------------------------------------------------------
member function toString( csVariableName varchar2 ) 
  return varchar2
is
  sReturnString varchar2(4000);
  crlf          constant varchar2(2) := chr(10)||chr(13);    
begin
  sReturnstring := substr( csVariableName||'.sToken:       '||nvl(self.sToken,'(null)')||crlf||
                           csVariableName||'.sType:        '||nvl(self.sType,'(null)')||crlf||
                           csVariableName||'.sDisplay:     '||nvl(self.sDisplay,'(null)')||crlf||
                           csVariableName||'.sHandledBy:   '||nvl(self.sHandledBy,'(null)')||crlf||
                           csVariableName||'.sIcon:        '||nvl(self.sIcon,'(null)')||crlf||                  
                           csVariableName||'.sConfirm:     '||nvl(self.sConfirm,'(null)')||crlf||
                           csVariableName||'.sConfirmText: '||nvl(self.sConfirmText,'(null)')||crlf||
                           csVariableName||'.nID:          '||nvl(to_char(self.nID),'(null)'), 1, 4000);
  return sReturnString;
end;
                                     
end;