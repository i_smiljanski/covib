CREATE OR REPLACE TYPE BODY "ISR$MESSAGE$REC" as
  constructor function ISR$MESSAGE$REC(self in out nocopy ISR$MESSAGE$REC,
                                       sUserMsg varchar2 default null,
                                       sImplMsg varchar2 default null,
                                       sDevMsg  varchar2 default null) return self as result is
  begin   
    -- limit the length to 4000 so the content can be used with sql                                    
    self.sUserMsg := substr(sUserMsg,1,4000);
    self.sImplMsg := substr(sImplMsg,1,4000);
    self.sDevMsg  := substr(sDevMsg,1,30000);
    return;
  end ISR$MESSAGE$REC;  

end;