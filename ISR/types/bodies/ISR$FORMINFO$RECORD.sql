CREATE OR REPLACE TYPE BODY ISR$FORMINFO$RECORD as

  constructor function ISR$FORMINFO$RECORD(self in out nocopy ISR$FORMINFO$RECORD,
                                           nMaskNo            NUMBER DEFAULT NULL,
                                           nMaskType          NUMBER DEFAULT NULL,
                                           sISRPackage        VARCHAR2 DEFAULT '',
                                           sHeadline1         VARCHAR2 DEFAULT '',
                                           sHeadline2         VARCHAR2 DEFAULT '',
                                           sHeadline3         VARCHAR2 DEFAULT '',
                                           sEntity            VARCHAR2 DEFAULT '',
                                           sTitle             VARCHAR2 DEFAULT '',
                                           sDescription       VARCHAR2 DEFAULT '',
                                           sTooltip           VARCHAR2 DEFAULT '',
                                           nTotalFormsNum     NUMBER DEFAULT NULL) return self as result is
  begin
    self.nMaskNo        := nMaskNo;
    self.nMaskType      := nMaskType;
    self.sISRPackage    := sISRPackage;
    self.sHeadline1     := sHeadline1;
    self.sHeadline2     := sHeadline2;
    self.sHeadline3     := sHeadline3;
    self.sEntity        := sEntity;
    self.sTitle         := sTitle;
    self.sDescription   := sDescription;
    self.sTooltip       := sTooltip;
    self.nTotalFormsNum := nTotalFormsNum;
    return;
  end ISR$FORMINFO$RECORD;

end; 