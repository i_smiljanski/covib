CREATE OR REPLACE TYPE BODY ISR$PARAMETER$REC as 
  constructor function iSR$Parameter$Rec(sParameter in varchar2, aValue in sys.anydata) return self as result is
  begin
    self.sParameter := sParameter;
    self.aValue := aValue;
    return;
  end iSR$Parameter$Rec;
  
  constructor function iSR$Parameter$Rec(sParameter in varchar2, nValue in number) return self as result is
  begin
    self.sParameter := sParameter;
    aValue := sys.anydata.convertnumber(nValue);
    return;
  end iSR$Parameter$Rec;
  
  constructor function iSR$Parameter$Rec(sParameter in varchar2, sValue in varchar2) return self as result is
  begin
    self.sParameter := sParameter;
    aValue := sys.anydata.convertvarchar2(sValue);
    return;
  end iSR$Parameter$Rec;
  
  constructor function iSR$Parameter$Rec(sParameter in varchar2, dValue in date) return self as result is
  begin
    self.sParameter := sParameter;
    aValue := sys.anydata.convertdate(dValue);
    return;
  end iSR$Parameter$Rec;
end;