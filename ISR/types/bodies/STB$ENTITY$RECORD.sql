CREATE OR REPLACE TYPE BODY STB$ENTITY$RECORD as

  constructor function STB$ENTITY$RECORD(self in out nocopy STB$ENTITY$RECORD) return self as result is
  begin
    self.sEnt := null;
    self.sPreSelected := null;
    self.sEditable := null;
    return;
  end STB$ENTITY$RECORD;

  constructor function STB$ENTITY$RECORD(self in out nocopy STB$ENTITY$RECORD, sEnt in varchar2, sPreSelected in varchar2, sEditable in varchar2) return self as result is
  begin
    self.sEnt := sEnt;
    self.sPreSelected := sPreSelected;
    self.sEditable := sEditable;
    return;
  end STB$ENTITY$RECORD;

end; 