CREATE OR REPLACE TYPE BODY                   "ISR$ATTRLIST$REC" as
  constructor function iSR$AttrList$Rec(self in out nocopy iSR$AttrList$Rec) return self as result is
  begin
    self.lAttributes := iSR$Attribute$List();
    return;
  end iSR$AttrList$Rec;

--*********************************************************************************************************************************  
constructor function iSR$AttrList$Rec(self in out nocopy iSR$AttrList$Rec, lAttributes in iSR$Attribute$List) return self as result is
begin
  self.lAttributes := lAttributes;
end iSR$AttrList$Rec;

--*********************************************************************************************************************************
member function GetAttrRec( csParameter in varchar2 ) 
  return iSR$Attribute$Record
is 
  sCurrentName  constant varchar2(100) := $$PLSQL_UNIT||'.GetAttrRec';  
  oArribute     iSR$Attribute$Record;
  
  cursor cParameter( sName in varchar2 ) is
    select sParameter, sValue, sPrepresent
    from table(self.lAttributes)
    where sParameter = sName;  
    
  rParameter  cParameter%rowtype;
  
begin 
  open cParameter(csParameter);
  fetch cParameter into rParameter;
  close cParameter;
  oArribute := iSR$Attribute$Record(rParameter.sParameter, rParameter.sValue, rParameter.sPrepresent); 
  return oArribute; 
exception
  when others then 
    isr$trace.error('error in datatype method', 
                     substr(dbms_utility.format_error_stack||chr(13)||chr(10)||dbms_utility.format_error_backtrace, 1,4000), 
                     sCurrentName, dbms_utility.format_error_stack||chr(13)||chr(10)||dbms_utility.format_error_backtrace );  
    raise;
end GetAttrRec;

--*********************************************************************************************************************************
member function GetAttrValue( csParameter in varchar2 ) 
  return varchar2 
is
  sCurrentName  constant varchar2(100) := $$PLSQL_UNIT||'.GetAttrValue';  
  oArribute     iSR$Attribute$Record;
begin
  oArribute := self.GetAttrRec(csParameter);
  return oArribute.sValue;
exception
  when others then 
    isr$trace.error('error in datatype method', 
                     substr(dbms_utility.format_error_stack||chr(13)||chr(10)||dbms_utility.format_error_backtrace, 1,4000), 
                     sCurrentName, dbms_utility.format_error_stack||chr(13)||chr(10)||dbms_utility.format_error_backtrace );    
    raise;    
end GetAttrValue;
  
end;