CREATE OR REPLACE TYPE BODY ISR$VALUE$RECORD AS 

  constructor function ISR$VALUE$RECORD(self in out nocopy ISR$VALUE$RECORD, 
                                        sParametername  varchar2 default null,
                                        sParametervalue varchar2 default null,
                                        sNodetype       varchar2 default null,
                                        sNodeId         varchar2 default null,                                             
                                        sNodePackage    varchar2 default null
                                        ) return self as result is
  begin
    self.sParametername := sParametername;
    self.sParametervalue := sParametervalue;
    self.sNodetype := sNodetype;
    self.sNodeId := sNodeId;
    self.sNodePackage := sNodePackage;
    return;
  end ISR$VALUE$RECORD; 
end; 