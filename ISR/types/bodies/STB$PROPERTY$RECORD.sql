CREATE OR REPLACE TYPE BODY STB$PROPERTY$RECORD AS 

  constructor function STB$PROPERTY$RECORD(self in out nocopy STB$PROPERTY$RECORD, 
                                           sParameter      varchar2 default null,  
                                           sPromptDisplay  varchar2 default null,
                                           sValue          varchar2 default null,
                                           sDisplay        varchar2 default null,
                                           sEditable       varchar2 default null,
                                           sRequired       varchar2 default null,
                                           sDisplayed      varchar2 default null,
                                           hasLov          varchar2 default null,
                                           type            varchar2 default null,
                                           format          varchar2 default null,
                                           sFocus          varchar2 default 'F'
                                         ) return self as result is
  begin
    self.sParameter := sParameter;
    self.sPromptDisplay := sPromptDisplay;
    self.sValue := sValue;
    self.sDisplay := sDisplay;
    self.sEditable := sEditable;
    self.sRequired := sRequired;
    self.sDisplayed := sDisplayed;
    self.hasLov := hasLov;
    self.type := type;
    self.format := format;
    self.sFocus := sFocus;
    return;
  end STB$PROPERTY$RECORD; 
end; 