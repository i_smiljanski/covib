CREATE OR REPLACE type body isr$obj$dialog as

static function putParameters(oparameter in stb$menuentry$record, olparameter in stb$property$list, clparameter in clob default null) 
  return stb$oerror$record is
  sCurrentName        constant varchar2(100) := $$PLSQL_UNIT||'.putParameters('||oParameter.sToken||')';
  oErrorObj           STB$OERROR$RECORD := STB$OERROR$RECORD();
  exNoTokenHandle     exception;
  sMsg              varchar2(4000);
begin
  raise exNoTokenHandle;
exception
  when exNoTokenHandle then
    sMsg := utd$msglib.getmsg ('exNoTokenHandle', stb$security.getcurrentlanguage, oParameter.sToken);
    oErrorObj.handleError ( Stb$typedef.cnSeverityWarning, sMsg, sCurrentName, 'user defined exception: exNoTokenHandle'||stb$typedef.crlf||DBMS_UTILITY.format_error_stack ||stb$typedef.crlf|| DBMS_UTILITY.format_error_backtrace );
    rollback;
  return oErrorObj;
end putParameters;

end;