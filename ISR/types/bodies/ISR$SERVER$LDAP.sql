CREATE OR REPLACE TYPE BODY ISR$SERVER$LDAP as

static function getServerType return number is
begin
  return 5 ;
end getServerType;

static function verifyLDAPConnection( sUsername in varchar2, 
                                      sPassword in varchar2 ) 
  return STB$OERROR$RECORD 
is
  oErrorObj         STB$OERROR$RECORD := STB$OERROR$RECORD();
  
  v_ldap_session    DBMS_LDAP.SESSION;
  v_ldapconn_admin  binary_integer;
  v_ldapconn        binary_integer;
  res_attrs         DBMS_LDAP.STRING_COLLECTION;
  retval            PLS_INTEGER;
  res_message       DBMS_LDAP.MESSAGE;  
  
  exNoLDAPServer    EXCEPTION;
  --exNoUserDN          EXCEPTION;
  exNoResult            EXCEPTION;
  --exNoLDAPSession   EXCEPTION;
  exNoAuthentication EXCEPTION;
  
  sMsg              varchar2(4000);
  sSqlErrMsg        varchar2(4000);
  tLdapbase         isr$serverparameter.parametervalue%TYPE; 
  tLdapCount        integer; 
  nCount            integer := 1;
  sCurrentName CONSTANT VARCHAR2(100) := $$PLSQL_UNIT||'.verifyLDAPConnection';
  bSuccess          VARCHAR2(1) := 'F';
  
  cursor cGetLDAPServer is
    SELECT HOST
         , port
         , isr$server$base.getServerParameter (serverid, 'USERNAME') username
         , isr$server$base.getServerParameter (serverid, 'PASSWORD') password
         , isr$server$base.getServerParameter (serverid, 'LDAPBASE') ldapbase
         , isr$server$base.getServerParameter (serverid, 'LDAPSEARCH') ldapsearch
         , isr$server$base.getServerParameter (serverid, 'SSLWRL') sslWrl
         , isr$server$base.getServerParameter (serverid, 'SSLWALLETPASSWD') sslWalletPasswd
         , isr$server$base.getServerParameter (serverid, 'SSLAUTH') sslAuth
      FROM isr$server
     WHERE servertypeid = 5
     AND (nvl(serverstatus, 0) > 0 or nvl(runstatus, 'F') = 'T');
  
  rGetLDAPServer cGetLDAPServer%rowtype;   

BEGIN
  isr$trace.stat ('begin', 'begin', sCurrentName);
   
  OPEN cGetLDAPServer;
  FETCH cGetLDAPServer INTO rGetLDAPServer;
  
  IF cGetLDAPServer%NOTFOUND THEN
    sMsg :=  'No running LDAP server found' ;
    sSqlErrMsg := sMsg;
    RAISE exNoLDAPServer;
  END IF;
  
  WHILE cGetLDAPServer%FOUND LOOP
    nCount := 1;
    isr$trace.info_all('rGetLDAPServer.host ', rGetLDAPServer.host, sCurrentName); 
    sMsg :=  'Error by trying to connect to the LDAP server';
    sSqlErrMsg :=  'Error by trying to connect to the LDAP server on host  ' || rGetLDAPServer.host || ' and port ' ||rGetLDAPServer.port||'.' ;     
    v_ldap_session := DBMS_LDAP.INIT(
        HOSTNAME => rGetLDAPServer.host
      , PORTNUM  => rGetLDAPServer.port
    );
    
    IF v_ldap_session IS NULL THEN
      RAISE exNoLDAPServer;
    END IF;  
            
    -- SSL
    IF NVL(rGetLDAPServer.sslauth, '0') != '0' THEN  
      sMsg := 'Error by establishing an SSL connection'; 
      sSqlErrMsg := 'Error by establishing an SSL connection;  wallet location = ' || rGetLDAPServer.sslWrl || '; SSL Authentication Mode=' || rGetLDAPServer.sslAuth;    
      v_ldapconn_admin := DBMS_LDAP.OPEN_SSL(
          LD              => v_ldap_session
        , SSLWRL          => rGetLDAPServer.sslWrl                                    --'file:/pfad/zum/Oracle/Wallet/Verzeichnis'
        , SSLWALLETPASSWD => STB$JAVA.decryptPassword(rGetLDAPServer.sslWalletPasswd) --'{Oracle-Wallet-Passwort}'
        , SSLAUTH         => rGetLDAPServer.sslAuth                                   -- if 1 then the two parameters above remain empty else 2 or 3
      );
    END IF;
    
    sMsg := 'Error by trying to authenticate a user ''' || rGetLDAPServer.username || ''''; 
    v_ldapconn_admin := DBMS_LDAP.SIMPLE_BIND_S(
        LD => v_ldap_session
      , DN => rGetLDAPServer.username
      , PASSWD => STB$JAVA.decryptPassword(rGetLDAPServer.password)
    );
      
    sMsg := '';  
    res_attrs(1) := '*';
                
    tLdapCount := regexp_count(rGetLDAPServer.ldapbase, '[^;]+'); -- count of the ldap servers
    isr$trace.info_all('tLdapCount', tLdapCount, sCurrentName);
    while nCount <= tLdapCount loop
      tLdapbase :=regexp_substr(rGetLDAPServer.ldapbase, '[^;]+',1, nCount) ;
      isr$trace.info_all('tLdapbase ', tLdapbase, sCurrentName); 
      begin  
        retval := DBMS_LDAP.SEARCH_S(
            ld         =>  v_ldap_session
          , base       => tLdapbase
          , scope      => DBMS_LDAP.SCOPE_SUBTREE
          , filter     => rGetLDAPServer.ldapsearch||'='||sUsername
          , attrs      => res_attrs
          , attronly   => 0
          , res        => res_message
        );      
        isr$trace.info_all('retval', retval, sCurrentName); 
        if retval = DBMS_LDAP.SUCCESS then
           IF res_message IS NULL THEN
             sSqlErrMsg := sSqlErrMsg || chr(10) || 'The search in the directory '  || 'with filter ''' ||rGetLDAPServer.ldapsearch||'='||sUsername || ''' failed for DN = '''||rGetLDAPServer.ldapbase ||  ''''  ;
             sSqlErrMsg := trim(leading chr(10) from sSqlErrMsg);
             RAISE exNoResult; 
           END IF;
          IF DBMS_LDAP.COUNT_ENTRIES(v_ldap_session, res_message) > 0 THEN          
            --sMsg:='User authentication failed for ''' || DBMS_LDAP.get_dn(v_ldap_session, RAWTOHEX(res_message)) || '''';
            sSqlErrMsg:='User authentication failed for ''' || DBMS_LDAP.get_dn(v_ldap_session, RAWTOHEX(res_message)) || '''';
            v_ldapconn := DBMS_LDAP.SIMPLE_BIND_S(
                LD => v_ldap_session
              , DN => DBMS_LDAP.get_dn(v_ldap_session, RAWTOHEX(res_message))
              , PASSWD => sPassword
            );
          ELSE
           -- sMsg:= sMsg || chr(10) ||'User does not exist.';
            sSqlErrMsg:= sSqlErrMsg || chr(10) ||'User '|| sUsername ||' does not exist.';
          END IF;
         
          IF nvl(v_ldapconn, DBMS_LDAP.OPERATIONS_ERROR) = DBMS_LDAP.SUCCESS THEN
            bSuccess := 'T';
          END IF;     
          isr$trace.info_all('bSuccess' , bSuccess, sCurrentName); 
            
          --sMsg:= sMsg || chr(10) || 'Closing an LDAP session.' ;
          v_ldapconn_admin := DBMS_LDAP.UNBIND_S(LD => v_ldap_session);
            
          STB$SECURITY.setCurrentUsername(sUsername);
          STB$SECURITY.setCurrentPassword(sPassword);
        end if;     
        exit when bSuccess = 'T';   
        exception when others then
          sSqlErrMsg := SQLERRM;          
          if sSqlErrMsg like '%Invalid credentials%' or sSqlErrMsg like '%data 52e,%' then 
            sSqlErrMsg:='The password entered for the user "' || sUsername || '" is wrong'|| chr(10) ||sSqlErrMsg;
          end if;
          
      end;
      nCount := nCount + 1;
    end loop;     
         
    
    FETCH cGetLDAPServer INTO rGetLDAPServer;
  END LOOP;
  CLOSE cGetLDAPServer;
  
   IF bSuccess = 'F' THEN
      sMsg := trim(leading chr(10) from sMsg);
      sMsg := 'LDAP authentication for user "' || sUsername || '" failed' || chr(10) || sMsg;
      RAISE  exNoAuthentication;
    END IF; 
    
  isr$trace.stat('end', 'sUsername: '||sUsername, sCurrentName);
  RETURN oErrorObj;

EXCEPTION
  when exNoAuthentication then
    sMsg :=  nvl(sMsg, utd$msglib.getmsg ('exNoAuthentication', 2, csP1 => sqlerrm(sqlcode)));
    oErrorObj.handleError ( Stb$typedef.cnSeverityCritical, sMsg, sCurrentName, 
                            sSqlErrMsg, SQLCODE);
    return oErrorObj; 
  when others then
  
    sMsg :=  nvl(sMsg, utd$msglib.getmsg ('exUnexpectedError', 2, csP1 => sqlerrm(sqlcode)));
    oErrorObj.handleError ( Stb$typedef.cnSeverityCritical, sMsg, sCurrentName, 
                            dbms_utility.format_error_stack || CHR(10) || dbms_utility.format_error_backtrace, SQLCODE); 
    --isr$trace.error('error', sSqlErrMsg, sCurrentName);                         
    return oErrorObj; 
end verifyLDAPConnection;

 --*************************************************************************************************************************
static function getLDAPServerStatus (nServerid in number) return clob  is
  sCurrentName      constant varchar2(100) := $$PLSQL_UNIT||'.getLDAPServerStatus';
  oErrorObj         STB$OERROR$RECORD := STB$OERROR$RECORD();
  exNoLDAPServer    EXCEPTION;
  sMsg         varchar2(4000);
  clReturn clob;
  
  cursor cGetLDAPServer is
    SELECT HOST
         , port
         , isr$server$base.getServerParameter (serverid, 'USERNAME') username
         , isr$server$base.getServerParameter (serverid, 'PASSWORD') password
         , isr$server$base.getServerParameter (serverid, 'LDAPBASE') ldapbase
         , isr$server$base.getServerParameter (serverid, 'LDAPSEARCH') ldapsearch
         , isr$server$base.getServerParameter (serverid, 'SSLWRL') sslWrl
         , isr$server$base.getServerParameter (serverid, 'SSLWALLETPASSWD') sslWalletPasswd
         , isr$server$base.getServerParameter (serverid, 'SSLAUTH') sslAuth
         , isr$server$base.getServerParameter (serverid, 'LDAPFILTER') dirFilter 
      FROM isr$server
     WHERE serverid = nServerid;
  
  rGetLDAPServer cGetLDAPServer%rowtype;   

begin
 isr$trace.debug('begin', 'LDAPUSERNAME', sCurrentName);  

  OPEN cGetLDAPServer;
  FETCH cGetLDAPServer INTO rGetLDAPServer;
  IF cGetLDAPServer%FOUND THEN
    WHILE cGetLDAPServer%FOUND LOOP
    
      clReturn := ISR$LDAP.getLDAPStatus ( sHost => rGetLDAPServer.HOST
                                              , sPort => rGetLDAPServer.port
                                              , sSSLAuth => rGetLDAPServer.sslauth
                                              , sSSLWrl => rGetLDAPServer.sslWrl
                                              , sSSLWalletPasswd => rGetLDAPServer.sslWalletPasswd
                                              , sGlobUserName => rGetLDAPServer.username
                                              , sGlobPassword => rGetLDAPServer.password
                                              ); 
                        
      isr$trace.debug('clReturn', 's. logclob', sCurrentName, clReturn);             
   
      FETCH cGetLDAPServer INTO rGetLDAPServer;
    END LOOP;
  ELSE
    RAISE exNoLDAPServer;      
  END IF;
  CLOSE cGetLDAPServer;
   isr$trace.stat('end', 'oSelectionList, see column LOGCLOB', sCurrentName);  
  RETURN clReturn;

EXCEPTION
  WHEN exNoLDAPServer THEN
    sMsg :=  nvl(sMsg, utd$msglib.getmsg ('exNoLDAPServerText', 2, csP1 => sqlerrm(sqlcode)));
    oErrorObj.handleError ( Stb$typedef.cnSeverityMessage, sMsg, sCurrentName, 
                            dbms_utility.format_error_stack || CHR(10) || dbms_utility.format_error_backtrace, SQLCODE); 
    RETURN '<ERROR>'||sqlerrm||'</ERROR>'; 
  WHEN OTHERS then
     sMsg :=  nvl(sMsg, utd$msglib.getmsg ('exUnexpectedError', 2, csP1 => sqlerrm(sqlcode)));
    oErrorObj.handleError ( Stb$typedef.cnSeverityCritical, sMsg, sCurrentName, 
                            dbms_utility.format_error_stack || CHR(10) || dbms_utility.format_error_backtrace, SQLCODE);  
    RETURN '<ERROR>'||sqlerrm||'</ERROR>';             
end getLDAPServerStatus; 
  
 --*************************************************************************************************************************
static function getLDAPRunStatus(nServerid in number) return varchar2  is
  sCurrentName      constant varchar2(100) := $$PLSQL_UNIT||'.getLDAPRunStatus';
  oErrorObj         STB$OERROR$RECORD := STB$OERROR$RECORD();
  sMsg         varchar2(4000); 
  sStatus    varchar2(1):='F';
  clReturn clob;
begin
   isr$trace.stat('begin', 'begin', sCurrentName); 
   clReturn :=  getLDAPServerStatus(nServerid);
   if clReturn = 'T' then
     sStatus := 'T';
   end if;
   isr$trace.stat('end', sStatus, sCurrentName);  
   return sStatus;
 exception
  WHEN OTHERS then
     sMsg :=  nvl(sMsg, utd$msglib.getmsg ('exUnexpectedError', 2, csP1 => sqlerrm(sqlcode)));
     oErrorObj.handleError ( Stb$typedef.cnSeverityCritical, sMsg, sCurrentName, 
                            dbms_utility.format_error_stack || CHR(10) || dbms_utility.format_error_backtrace, SQLCODE);  
    return 'F';
end getLDAPRunStatus; 

      
-- ***************************************************************************************************
static function getNodeDetails(  clXML      out clob) return STB$OERROR$RECORD
is
  sCurrentName constant varchar2(100) := $$PLSQL_UNIT||'.getNodeDetails';
  oErrorObj    stb$oerror$record      := STB$OERROR$RECORD();
  sUserMsg     varchar2(4000);
  xStatusXml    xmltype;
  cursor cGetServer is
    select s.serverid, s.runstatus, nvl( s. observer_host, s.host) as host, s.port,  s.context, s.namespace
    from   isr$serverobserver$v s
    where  s.serverid = STB$OBJECT.getCurrentNodeId;
       
  rGetServer    cGetServer%rowtype;
begin
  isr$trace.info('begin', 'static function '||sCurrentName||' started.', sCurrentName);
  open cGetServer;
  fetch cGetServer into rGetServer;
  close cGetServer;
  isr$trace.debug('ldap server status', rGetServer.runstatus, sCurrentName);
  if  nvl(rGetServer.runstatus, 'F') = 'T' then
    select  xmlelement("status", utd$msglib.getmsg ('serverRunning', stb$security.getcurrentlanguage))
    into xStatusXml
    from dual;
    clXML := xStatusXml.getClobVal();
  else
    clXML := getLDAPServerStatus(rGetServer.serverid);
  end if;
  
  isr$trace.info('end', 'static function '||sCurrentName||' finished',sCurrentName);
  return (oErrorObj);
exception
 when others then
  isr$trace.info('sqlcode','sqlcode: '||sqlcode, sCurrentName);
  sUserMsg := utd$msglib.getmsg ('exUnexpectedError', stb$security.getcurrentlanguage, sqlerrm(sqlcode));
  oErrorObj.handleError ( Stb$typedef.cnSeverityCritical, sUserMsg, sCurrentName, substr(dbms_utility.format_error_stack ||stb$typedef.crlf|| dbms_utility.format_error_backtrace, 1,4000) );  
  return oErrorObj;
end getNodeDetails;
    
end;