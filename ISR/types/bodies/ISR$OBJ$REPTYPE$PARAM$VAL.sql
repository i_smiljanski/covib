CREATE OR REPLACE type body                   ISR$OBJ$REPTYPE$PARAM$VAL as
--******************************************************************************
static function putParameters(oParameter in stb$menuentry$record, olParameter in stb$property$list, clParameter in clob default null) 
  return stb$oerror$record is
  sCurrentName        constant varchar2(100) := $$PLSQL_UNIT||'.putParameters('||oParameter.sToken||')';
  oErrorObj           STB$OERROR$RECORD := STB$OERROR$RECORD();
  sMsg                varchar2(4000);
  oParamListRec       ISR$PARAMLIST$REC := ISR$PARAMLIST$REC();
 begin 
  isr$trace.stat('begin','begin',sCurrentName);
  
  oParamListRec.AddParam('TABLE', 'ISR$FILE$PARAMETER$VALUES');
  oParamListRec.AddParam('AUDITTYPE', 'TEMPLATEAUDIT');
  oParamListRec.AddParam('AUDITREFERENCE', STB$OBJECT.getCurrentNode().sDisplay);
  oErrorObj := isr$obj$grid().modifyGridTable(oParameter.sToken, clParameter, oParamListRec);
  update isr$template
     set modifiedon = SYSDATE
       , modifiedby = STB$SECURITY.getOracleUserName (STB$SECURITY.getCurrentUser)
  where fileid = STB$OBJECT.getCurrentNodeId;
  
  isr$trace.stat('end','end',sCurrentName);
  return oErrorObj;
exception
  when others then
    sMsg :=  utd$msglib.getmsg ('exUnexpectedError', isr$obj$base().nCurrentLanguage, csP1 => SQLERRM(SQLCODE));
    oErrorObj.handleError ( Stb$typedef.cnSeverityCritical, sMsg, sCurrentName,
                            DBMS_UTILITY.format_error_stack || CHR(10) || DBMS_UTILITY.format_error_backtrace );
    return oErrorObj; 
 end putParameters;

--****************************************************************************** 
static function callFunction (oParameter in STB$MENUENTRY$RECORD, olNodeList out STB$TREENODELIST)
  return STB$OERROR$RECORD is
  sCurrentName         constant varchar2(100) := $$PLSQL_UNIT||'.callFunction('||oParameter.sToken||')';
  oErrorObj                     STB$OERROR$RECORD := STB$OERROR$RECORD();
  sMsg                          varchar2(4000);
  oParamListRec                 ISR$PARAMLIST$REC := ISR$PARAMLIST$REC();
  nNewFileId                    number;
begin
  isr$trace.stat('begin', 'oParameter.sToken: '||oParameter.sToken ,sCurrentName);
  isr$trace.debug('parameter', 'oParameter', sCurrentName, oParameter);
    
  STB$OBJECT.setCurrentToken(oparameter.stoken);
  
  if STB$UTIL.getProperty(STB$OBJECT.getCurrentNode().tloPropertyList, 'RELEASE') = 'T' then
    oErrorObj := ISR$TEMPLATE$HANDLER.copyFile(STB$OBJECT.getCurrentNodeId, nNewFileId);
  else
    nNewFileId := STB$OBJECT.getCurrentNodeId;
  end if;
  isr$trace.debug ('nNewFileId',  nNewFileId, sCurrentName);
  ISR$TREE$PACKAGE.reloadPrevNodesAndSelect(-1, 'F');
  ISR$TREE$PACKAGE.selectRightNode(ISR$TREE$PACKAGE.buildNode('CURRENTTEMPLATE', nNewFileId, STB$OBJECT.getCurrentNode));  
  
  ISR$TEMPLATE$HANDLER.setAllowedTockenParameters(oParamListRec);
  oParamListRec.AddParam('TABLE', 'ISR$FILE$PARAMETER$VALUES$VIEW');
  oErrorObj := isr$obj$grid().buildGridMask(oParameter.sToken, oParamListRec, olNodeList);
  
  isr$trace.stat('end', 'end', sCurrentName);
  return oErrorObj;
exception
  when others then
    sMsg :=  utd$msglib.getmsg ('exUnexpectedError', isr$obj$base().nCurrentLanguage, csP1 => SQLERRM(SQLCODE));
    oErrorObj.handleError ( Stb$typedef.cnSeverityCritical, sMsg, sCurrentName,
                            DBMS_UTILITY.format_error_stack || CHR(10) || DBMS_UTILITY.format_error_backtrace );
    return oErrorObj; 
end callFunction;

--******************************************************************************
static function setMenuAccess( sParameter   in  varchar2,  -- sParameter is the token
                               sMenuAllowed out varchar2 )
  return STB$OERROR$RECORD
is
  oErrorObj         STB$OERROR$RECORD := STB$OERROR$RECORD();
  sCurrentName      constant varchar2(100) := $$PLSQL_UNIT||'.setMenuAccess_param('||sParameter||')';
  sUserMsg          varchar2(4000); 
begin
  isr$trace.stat('begin', 'sParameter: ' || sParameter, sCurrentName);
  ISR$TEMPLATE$HANDLER.setMenuAllowed(sParameter,sMenuAllowed) ;   
  isr$trace.stat('end', sMenuAllowed  , sCurrentName);
  RETURN oErrorObj;        
exception
  when others then
    sUserMsg := utd$msglib.getmsg( 'exUnexpectedError', stb$security.getCurrentLanguage,  csP1 => sqlerrm(sqlcode) );
    oErrorObj.handleError( Stb$typedef.cnSeverityCritical, sUserMsg, sCurrentName, dbms_utility.format_error_stack||stb$typedef.crlf||dbms_utility.format_error_backtrace );  
    return oErrorObj;
end setMenuAccess;

end; 