CREATE OR REPLACE type body                   isr$obj$modify$grouping as
  -- ***************************************************************************************************
  constructor function isr$obj$modify$grouping (
    self   in out nocopy isr$obj$modify$grouping)
    return self as result is
  begin
    return;
  end isr$obj$modify$grouping;

  static function putparameters (oparameter    in stb$menuentry$record,
                                 olparameter   in stb$property$list,
                                 clparameter   in clob default null)
    return stb$oerror$record is
    scurrentname   constant varchar2 (100)
      := $$plsql_unit || '.putParameters(' || oparameter.stoken || ')' ;
    oerrorobj               stb$oerror$record := stb$oerror$record ();
    coaddparamlist          isr$paramlist$rec := isr$paramlist$rec ();
    stoken                  oparameter.stoken%type;

    stable                  varchar2 (30);
    sparamaudittype         varchar2 (1000);
    sparamauditref          varchar2 (4000);
    soldvaluestring         varchar2 (32767);
    snewvaluestring         varchar2 (32767);
    saction                 varchar2 (500);
    srowid                  varchar2 (500);
    sstmt                   varchar2 (32767);
    ncount                  number;
    skeys                   varchar2 (4000);
    skeyvalues              varchar2 (4000);
    sauditkeys              varchar2 (4000);
    skey1                   isr$dialog.parametername%type;
    skey2                   isr$dialog.parametername%type;
    skeyvalue1              isr$dialog.parametervalue%type;
    skeyvalue2              isr$dialog.parametervalue%type;
    --sParameter        varchar2(500);
    --sOldValue         varchar2(32767);
    --sNewValue         varchar2(32767);
    --sNewValueClob     clob;
    --blOldValue        blob;
    smsg                    varchar2 (32767);
    seditable               varchar2 (1);
    xxml                    xmltype;
    spackage                varchar2 (200);
    sparametertype          isr$dialog.parametertype%type;

    cursor cgetparameterinfo (
      csparameter   in varchar2,
      csvalue       in varchar2,
      cspackage     in varchar2) is
      select utd$msglib.getmsg (description, stb$security.getcurrentlanguage)
               parameter,
             case
               when haslov in ('T', 'W') then
                 nvl (
                   isr$obj$base.getselectedlovdisplay (parametername,
                                                       csvalue,
                                                       cspackage),
                   csvalue)
               else
                 csvalue
             end
               svalue,
             editable,
             parametertype
        from isr$dialog
       where token = stoken and parametername = csparameter;

    --rGetParameterInfo   cGetParameterInfo%rowtype;

    cursor cprimarykeys (
      stablename    varchar2) is
        select distinct cols.column_name
          from all_constraints cons, all_cons_columns cols
         where     cols.table_name = stablename
               and cons.constraint_type = 'P'
               and cons.constraint_name = cols.constraint_name
               and cons.owner = cols.owner
      order by 1;
  begin
    isr$trace.stat ('begin', 'begin', scurrentname);
    coaddparamlist.addparam ('TABLE', 'ISR$GROUPING$TAB');
    coaddparamlist.addparam ('AUDITTYPE', 'REPORTTYPEAUDIT');
    coaddparamlist.addparam ('AUDITREFERENCE', TO_CHAR(STB$OBJECT.getCurrentReporttypeid)); 
    stoken := oparameter.stoken;

    oerrorobj      := coaddparamlist.getVerfiedParamStr ('TABLE', stable);
    oerrorobj      := coaddparamlist.getVerfiedParamStr ('AUDITTYPE', sparamaudittype);
    sparamauditref := coaddparamlist.getParamStr        ('AUDITREFERENCE');   -- vs 13.Dec 2017 [ISRC-908]

    isr$trace.debug ('sTable', stable, scurrentname, clparameter);

    stb$object.setcurrenttoken (stoken);
    oerrorobj := stb$audit.openaudit (sparamaudittype, sparamauditref);
    oerrorobj := stb$audit.addauditrecord (stoken);

    xxml := isr$xml.clobtoxml (clparameter);

    for i in 1 .. isr$xml.valueof (xxml, 'count(//ROW)') loop
      soldvaluestring := null;
      snewvaluestring := null;

      saction :=
        isr$xml.valueof (xxml, '(//ROW)[' || i || ']/../name()');
      srowid :=
        isr$xml.valueof (xxml, '(//ROW)[' || i || ']/ROWID/text()');
      if trim (srowid) is null then
        srowid :=
          isr$xml.valueof (xxml, '(//ROW)[' || i || ']/VROWID/text()'); --for view, because 'ORA-01445: Cannot Select ROWID from a Join View'
      end if;
      if trim (srowid) is null then
        saction := 'INSERT';
      end if;

      isr$trace.debug ('sAction', saction, scurrentname);
      isr$trace.debug ('sRowId', srowid, scurrentname);

      ncount := 0;
      skeys := '';
      skeyvalues := '';
      sauditkeys := '';
      for rgetpks in cprimarykeys (stable) loop
        ncount := ncount + 1;
        skeys := skeys || rgetpks.column_name || ', ';
        skeyvalues :=
             skeyvalues
          || ''''
          || replace (
               isr$xml.valueof (
                 xxml,
                 '(//ROW)[' || i || ']/' || rgetpks.column_name || '/text()'),
               '''',
               '''''')
          || ''', ';
        sauditkeys :=
             sauditkeys
          || rgetpks.column_name
          || ': '
          || replace (
               isr$xml.valueof (
                 xxml,
                 '(//ROW)[' || i || ']/' || rgetpks.column_name || '/text()'),
               '''',
               '''''')
          || chr (10);
        if ncount = 1 then
          skey1 := rgetpks.column_name;
          isr$trace.debug ('sKey1', skey1, scurrentname);
          skeyvalue1 :=
            isr$xml.valueof (xxml,
                             '(//ROW)[' || i || ']/' || skey1 || '/text()');
          isr$trace.debug ('sKeyValue1', skeyvalue1, scurrentname);
        else
          skey2 := rgetpks.column_name;
          isr$trace.debug ('sKey2', skey2, scurrentname);
          skeyvalue2 :=
            isr$xml.valueof (xxml,
                             '(//ROW)[' || i || ']/' || skey2 || '/text()');
          isr$trace.debug ('sKeyValue2', skeyvalue2, scurrentname);
        end if;
      end loop;
      skeys := rtrim (skeys, ', ');
      skeyvalues := rtrim (skeyvalues, ', ');
      sauditkeys := rtrim (sauditkeys, chr (10));

      isr$trace.debug ('sKeys', skeys, scurrentname);
      isr$trace.debug ('sKeyValues', skeyvalues, scurrentname);
      isr$trace.debug ('sAuditKeys', sauditkeys, scurrentname);

      -- if UPPER(oParameter.sToken) = 'CAN_MODIFY_GROUPING' then
      if isr$xml.valueof (xxml, '(//ROW)[' || i || ']/SELECTED/text()') = 'T' then
        saction := 'INSERT';
      else
        saction := 'DELETE';
      end if;
      --sStmt := 'SELECT ROWID FROM '||sTable||' WHERE '||sKey1||' = '''||REPLACE(sKeyValue1, '''', '''''')||''''|| case when TRIM(sKey2) is not null then ' and '||sKey2||' = '''||REPLACE(sKeyValue2, '''', '''''')||'''' end ;
      sstmt :=
           'SELECT ROWID FROM '
        || stable
        || ' WHERE ('
        || skeys
        || ') IN ('
        || skeyvalues
        || ')';
      isr$trace.debug ('sStmt', sstmt, scurrentname);

      begin
        execute immediate sstmt into srowid;

        if saction = 'INSERT' and srowid is not null then
          saction := 'DO_NOTHING';
        end if;
      exception
        when others then
          if saction = 'DELETE' then
            saction := 'DO_NOTHING';
          end if;
      end;

      if saction = 'DELETE' then
        oerrorobj := isr$customer.deleteallsubnodes (skeyvalue1);
      end if;
      --end if;


      if saction = 'INSERT' then
        sstmt :=
             'INSERT INTO '
          || stable
          || ' '
          || '   ('
          || skeys
          || ') '
          || 'VALUES '
          || --'  ('''||REPLACE(sKeyValue1, '''', '''''')||''''|| case when TRIM(sKey2) is not null then ', '''||REPLACE(sKeyValue2, '''', '''''')||'''' end ||')';
             '  ('
          || skeyvalues
          || ')';
        isr$trace.debug ('sStmt', sstmt, scurrentname);

        execute immediate sstmt;

        sstmt :=
             'SELECT ROWID FROM '
          || stable
          || ' WHERE '
          || skey1
          || ' = '''
          || replace (skeyvalue1, '''', '''''')
          || ''''
          || case
               when trim (skey2) is not null then
                    ' and '
                 || skey2
                 || ' = '''
                 || replace (skeyvalue2, '''', '''''')
                 || ''''
             end;
        isr$trace.debug ('sStmt', sstmt, scurrentname);

        execute immediate sstmt into srowid;
      end if;

      spackage :=
        nvl (
          stb$object.getcurrentnodepackage (stb$object.getcurrentnodetype,
                                            stoken),
          stb$object.getcurrentnodepackage);


      if saction in ('DELETE') then
        sstmt :=
          'DELETE FROM ' || stable || ' WHERE ROWID = ''' || srowid || ''' ';
        isr$trace.debug ('sStmt', sstmt, scurrentname);

        execute immediate sstmt;
      end if;

      open cgetparameterinfo (skey1, skeyvalue1, spackage);

      fetch cgetparameterinfo
        into skey1,
             skeyvalue1,
             seditable,
             sparametertype;

      close cgetparameterinfo;

      if skeyvalue2 is not null then
        open cgetparameterinfo (skey2, skeyvalue2, spackage);

        fetch cgetparameterinfo
          into skey2,
               skeyvalue2,
               seditable,
               sparametertype;

        close cgetparameterinfo;
      end if;

      case
        when saction = 'INSERT' then
          oerrorobj :=
            stb$audit.addauditentry (sauditkeys    /*sKeys||': '||sKeyValues*/
                                               ,
                                     'Insert',
                                     rtrim (snewvaluestring, chr (10)),
                                     1);
        when saction = 'DELETE' then
          oerrorobj :=
            stb$audit.addauditentry (sauditkeys    /*sKeys||': '||sKeyValues*/
                                               ,
                                     'Delete',
                                     rtrim (soldvaluestring, chr (10)),
                                     1);
        when saction = 'UPDATE' and soldvaluestring != snewvaluestring then
          oerrorobj :=
            stb$audit.addauditentry (sauditkeys    /*sKeys||': '||sKeyValues*/
                                               ,
                                     rtrim (soldvaluestring, chr (10)),
                                     rtrim (snewvaluestring, chr (10)),
                                     1);
        else
          null;
      end case;
    end loop;

    oerrorobj := isr$obj$base ().getaudittype;

    isr$trace.stat ('end', 'end', scurrentname);

    return oerrorobj;
  exception
    when others then
      smsg :=
        utd$msglib.getmsg ('exUnexpectedError',
                           isr$obj$base ().ncurrentlanguage,
                           csp1   => sqlerrm (sqlcode));
      oerrorobj.handleerror (
        stb$typedef.cnseveritycritical,
        smsg,
        scurrentname,
           dbms_utility.format_error_stack
        || chr (10)
        || dbms_utility.format_error_backtrace,
        sqlcode);
      return oerrorobj;
  end putparameters;

  --**********************************************************************************************************************************
  static function callfunction (oparameter   in     stb$menuentry$record,
                                olnodelist      out stb$treenodelist)
    return stb$oerror$record is
    scurrentname   constant varchar2 (100)
      := $$plsql_unit || '.callFunction(' || oparameter.stoken || ')' ;
    oerrorobj               stb$oerror$record := stb$oerror$record ();
    smsg                    varchar2 (4000);
    coaddparamlist          isr$paramlist$rec := isr$paramlist$rec ();
    cstoken                 oparameter.stoken%type;

    cparameterold           clob;
    sheader                 varchar2 (32767);
    sxmlelement             varchar2 (32767);
    xxml                    xmltype;
    xxml1                   xmltype;
    xheader                 xmltype;
    ssql                    varchar2 (4000);
    sinsertallowed          varchar2 (1) := 'F';
    supdateallowed          varchar2 (1) := 'F';
    sdeleteallowed          varchar2 (1) := 'F';
    stable                  varchar2 (32);
    ncurrentreporttypeid    number;

    cursor cprimarykeys (
      stablename    varchar2) is
        select distinct cols.column_name
          from all_constraints cons, all_cons_columns cols
         where     cols.table_name = stablename
               and cons.constraint_type = 'P'
               and cons.constraint_name = cols.constraint_name
               and cons.owner = cols.owner
      order by 1;

    type tgetxml is ref cursor;

    cgetxml                 tgetxml;

    cursor cdialog (
      csupdateallowed   in varchar2) is
        select parametername,
               parametervalue,
               utd$msglib.getmsg (description, stb$security.getcurrentlanguage)
                 headervalue,
               parametertype,
               displayed,
               haslov,
               case when csupdateallowed = 'T' then editable else 'F' end
                 editable,
               required,
               case
                 when parametertype = 'DATE' then nvl(FORMAT, STB$UTIL.getSystemParameter('JAVA_DATE_FORMAT')) 
                 else format
               end format
          from isr$dialog
         where token = cstoken and parametertype is not null
      order by decode (tab, null, orderno, tab), orderno;

    sprimarykey             varchar2 (1);

    cursor cgetwhereclause is
      select parametervalue swhere
        from isr$dialog
       where token = cstoken and parametername = 'WHERE';

    cursor cgetsortorder is
        select rtrim (
                 xmlagg (xmlelement (
                           e,
                              parametername
                           || case
                                when instr (sortorder, ' ') > 0 then
                                  substr (sortorder, instr (sortorder, ' '))
                              end,
                           ',').extract ('//text()') order by sortorder).getstringval (),
                 ',')
                 orderby
          from isr$dialog
         where sortorder is not null and token = cstoken
      order by sortorder;

    --clAttributes  clob;
    spackage                varchar2 (200);
  begin
    isr$trace.stat ('begin',
                    'oParameter.sToken: ' || oparameter.stoken,
                    scurrentname);
    isr$trace.debug ('parameter',
                     'oParameter',
                     scurrentname,
                     oparameter);

    stb$object.setcurrenttoken (oparameter.stoken);

    coaddparamlist.addparam ('TABLE', 'ISR$GROUPING$VIEW');
    cstoken := oparameter.stoken;

    oerrorobj := coaddparamlist.getverfiedparamstr ('TABLE', stable);
    isr$trace.debug ('sTable', stable, scurrentname);
    olnodelist := stb$treenodelist ();
    olnodelist.extend;
    olnodelist (1) :=
      stb$treenode$record (stb$util.getmasktitle (upper (cstoken)),
                           'STB$SYSTEM',
                           'TRANSPORT',
                           'MASK',
                           '');
    isr$trace.debug ('value',
                     'olNodeList',
                     scurrentname,
                     olnodelist);

    cparameterold := olnodelist (1).clpropertylist;

    sinsertallowed :=
      stb$security.checkgroupright (upper (cstoken) || '_I',
                                    stb$object.getcurrentreporttypeid);
    supdateallowed :=
      stb$security.checkgroupright (upper (cstoken) || '_U',
                                    stb$object.getcurrentreporttypeid);
    sdeleteallowed :=
      stb$security.checkgroupright (upper (cstoken) || '_D',
                                    stb$object.getcurrentreporttypeid);



    spackage :=
      nvl (
        stb$object.getcurrentnodepackage (stb$object.getcurrentnodetype,
                                          cstoken),
        stb$object.getcurrentnodepackage);

    isr$trace.debug ('getCurrentNodePackage', spackage, scurrentname);
    for rdialog in cdialog (supdateallowed) loop
      sprimarykey := 'F';
      for rprimarykeys in cprimarykeys (stable) loop
        if rdialog.parametername = rprimarykeys.column_name then
          sprimarykey := 'T';
        end if;
      end loop;

      if rdialog.parametername = 'GROUPINGNAME' then
        sprimarykey := 'T';
      end if;


      sheader :=
           sheader
        || ', XMLELEMENT("'
        || rdialog.parametername
        || '", XMLATTRIBUTES('''
        || rdialog.parametertype
        || ''' as "type"
                    , '''
        || rdialog.haslov
        || ''' as "haslov"
                    , '''
        || rdialog.format
        || ''' as "format"
                    , '
        || case
             when nvl (instr (rdialog.parametervalue, ':exec:'), 0) > 0 then
               '(' || replace (rdialog.parametervalue, ':exec:') || ')'
             else
               '''' || rdialog.parametervalue || ''''
           end
        || ' as "defaultvalue"
                    , '''
        || rdialog.editable
        || ''' as "editable"
                    , '''
        || rdialog.displayed
        || ''' as "displayed"
                    , '''
        || sprimarykey
        || ''' as "primaryKey"
                    , '''
        || rdialog.required
        || ''' as "required"), '''
        || rdialog.headervalue
        || ''')';


      if rdialog.parametername = 'ROWID' then
        sxmlelement :=
             sxmlelement
          || ', XMLELEMENT("'
          || rdialog.parametername
          || '", GROUPINGNAME)';
      else
        sxmlelement :=
             sxmlelement
          || ', XMLELEMENT("'
          || rdialog.parametername
          || '",'
          || case
               when rdialog.haslov in ('T', 'W') then
                    ' isr$obj$base.getSelectedLovDisplay('''
                 || rdialog.parametername
                 || ''', '
                 || rdialog.parametername
                 || ', '''
                 || spackage
                 || ''') '
               when rdialog.parametertype = 'FILE' then
                    ' stb$java.getBlobByteString('
                 || rdialog.parametername
                 || ')'
               else
                 rdialog.parametername
             end
          || ')';
      end if;
    end loop;


    open cgetxml for
      'select XMLELEMENT("HEADSET" ' || sheader || ') from dual';

    loop
      fetch cgetxml into xheader;

      exit when cgetxml%notfound;
    end loop;

    ssql :=
         'select  XMLAGG(XMLELEMENT("ROW"'
      || sxmlelement
      || ')) from (select * from '
      || stable
      || ')';

    for rgetwhereclause in cgetwhereclause loop
      if trim (rgetwhereclause.swhere) is not null then
        ssql :=
             substr (ssql, 0, length (ssql) - 1)
          || ' where '
          || rgetwhereclause.swhere
          || ')';
      end if;
    end loop;

    for rgetsortorder in cgetsortorder loop
      if trim (rgetsortorder.orderby) is not null then
        ssql :=
             substr (ssql, 0, length (ssql) - 1)
          || ' order by '
          || rgetsortorder.orderby
          || ')';
      end if;
    end loop;

    isr$trace.debug ('statement',
                     'statement',
                     scurrentname,
                     ssql);

    open cgetxml for ssql;

    loop
      fetch cgetxml into xxml;

      exit when cgetxml%notfound;
    end loop;

    select xmlelement (
             "DOCUMENT",
             xmlelement ("PROPERTIES",
                         xmlelement ("TABLE", stable),
                         xmlelement ("INSERT_ALLOWED", sinsertallowed),
                         xmlelement ("DELETE_ALLOWED", sdeleteallowed)),
             xheader,
             xmlelement ("ROWSET", xxml))
      into xxml1
      from dual;

    olnodelist (1).clpropertylist := isr$xml.xmltoclob (xxml1);

    isr$trace.stat ('end', scurrentname, olnodelist (1).clpropertylist);
    return oerrorobj;
  exception
    when others then
      smsg :=
        utd$msglib.getmsg ('exUnexpectedError',
                           isr$obj$base ().ncurrentlanguage,
                           csp1   => sqlerrm (sqlcode));
      oerrorobj.handleerror (
        stb$typedef.cnseveritycritical,
        smsg,
        scurrentname,
           dbms_utility.format_error_stack
        || chr (10)
        || dbms_utility.format_error_backtrace,
        sqlcode);
      return oerrorobj;
  end callfunction;
end;