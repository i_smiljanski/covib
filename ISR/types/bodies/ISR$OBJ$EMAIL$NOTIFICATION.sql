CREATE OR REPLACE TYPE BODY isr$obj$email$notification as

--**********************************************************************************************************************************
static function putParameters(oParameter in stb$menuentry$record, olParameter in stb$property$list, clParameter in clob default null) 
  return stb$oerror$record is
  sCurrentName        constant varchar2(100) := $$PLSQL_UNIT||'.putParameters('||oParameter.sToken||')';
  oErrorObj           STB$OERROR$RECORD := STB$OERROR$RECORD();
  sMsg              varchar2(4000);
  oParamListRec   ISR$PARAMLIST$REC := ISR$PARAMLIST$REC();
 begin 
  isr$trace.stat('begin','begin',sCurrentName);
  oParamListRec.AddParam('TABLE', 'ISR$NOTIFY');
  oParamListRec.AddParam('AUDITTYPE', 'SYSTEMAUDIT');
  oParamListRec.AddParam('AUDITREFERENCE', TO_CHAR (-1));
  oErrorObj := isr$obj$grid().modifyGridTable(oParameter.sToken, clParameter, oParamListRec);
  isr$trace.stat('end','end',sCurrentName);
  return oErrorObj;
exception
  when others then
    sMsg :=  utd$msglib.getmsg ('exUnexpectedError', isr$obj$base().nCurrentLanguage, csP1 => SQLERRM(SQLCODE));
    oErrorObj.handleError ( Stb$typedef.cnSeverityCritical, sMsg, sCurrentName,
                            DBMS_UTILITY.format_error_stack || CHR(10) || DBMS_UTILITY.format_error_backtrace );
    return oErrorObj; 
 end putParameters;
 
static function callFunction (oParameter in STB$MENUENTRY$RECORD, olNodeList out STB$TREENODELIST)
  return STB$OERROR$RECORD is
  sCurrentName         constant varchar2(100) := $$PLSQL_UNIT||'.callFunction('||oParameter.sToken||')';
  oErrorObj                     STB$OERROR$RECORD := STB$OERROR$RECORD();
  sMsg              varchar2(4000);
  oParamListRec   ISR$PARAMLIST$REC := ISR$PARAMLIST$REC();
begin
  isr$trace.stat('begin', 'oParameter.sToken: '||oParameter.sToken ,sCurrentName);
  isr$trace.debug('parameter', 'oParameter', sCurrentName, oParameter);
    
  STB$OBJECT.setCurrentToken(oparameter.stoken);
  
  oParamListRec.AddParam('TABLE', 'ISR$NOTIFY');
  oErrorObj := isr$obj$grid().buildGridMask(oParameter.sToken, oParamListRec, olNodeList);
  isr$trace.stat('end', 'end', sCurrentName);
  return oErrorObj;
exception
  when others then
    sMsg :=  utd$msglib.getmsg ('exUnexpectedError', isr$obj$base().nCurrentLanguage, csP1 => SQLERRM(SQLCODE));
    oErrorObj.handleError ( Stb$typedef.cnSeverityCritical, sMsg, sCurrentName,
                            DBMS_UTILITY.format_error_stack || CHR(10) || DBMS_UTILITY.format_error_backtrace );
    return oErrorObj; 
end callFunction;


-- ***************************************************************************************************
static function getLov( sEntity        in varchar2,
                 sSuchwert      in varchar2, 
                 oSelectionList out ISR$TLRSELECTION$LIST ) return STB$OERROR$RECORD 
is
  sCurrentName constant varchar2(100) := $$PLSQL_UNIT||'.getLov(' || sEntity || ')';
  sMsg         varchar2(4000);
  nCounter     number            := 0;
  oErrorObj    STB$OERROR$RECORD := STB$OERROR$RECORD();

begin
  isr$trace.stat('begin '||sEntity, 'begin', sCurrentName);
  oSelectionList := ISR$TLRSELECTION$LIST ();

   CASE sEntity
        WHEN 'USERNO'THEN
              oSelectionList.extend;
              oSelectionList (oSelectionList.COUNT()) := ISR$OSELECTION$RECORD('Current User', '0', 'Current User', null, oSelectionList.COUNT());

            FOR rGetEmailUsers IN (select userno, fullname 
                                 from stb$user 
                                where email is not null 
                                order by 2) LOOP
              oSelectionList.EXTEND;
              oSelectionList (oSelectionList.COUNT()) := ISR$OSELECTION$RECORD(rGetEmailUsers.fullname, rGetEmailUsers.userno, rGetEmailUsers.fullname, null, oSelectionList.COUNT());
            END LOOP;       
              oSelectionList.extend;
              oSelectionList (oSelectionList.COUNT()) := ISR$OSELECTION$RECORD('-', '-1', '-', null, oSelectionList.COUNT());

              isr$obj$base.setLovListSelection(ssuchwert, oSelectionList);
        WHEN 'USERGROUPNO' THEN
            FOR rGetGroups IN (select usergroupno, groupname 
                             from stb$usergroup 
                            where active = 'T' 
                            order by 2) LOOP
              oSelectionList.EXTEND;
              oSelectionList (oSelectionList.COUNT()) := ISR$OSELECTION$RECORD(rGetGroups.groupname, rGetGroups.usergroupno, rGetGroups.groupname, null, oSelectionList.COUNT());
            END LOOP;       
              oSelectionList.extend;
              oSelectionList (oSelectionList.COUNT()) := ISR$OSELECTION$RECORD('-', '-1', '-', null, oSelectionList.COUNT());
              isr$obj$base.setLovListSelection(ssuchwert, oSelectionList);
        ELSE
            oErrorObj := isr$obj$base.getLov( sEntity,
                 sSuchwert, 
                 oSelectionList );

    END CASE; 
  
  isr$trace.stat ('end', 'oSelectionList.count() = ' || oSelectionList.count(), sCurrentName);
  return oErrorObj;
exception
  when others then
    sMsg :=  utd$msglib.getmsg ('exUnexpectedError', stb$security.getcurrentlanguage, csP1 => sqlerrm(sqlcode));
    oErrorObj.handleError ( Stb$typedef.cnSeverityCritical, sMsg, sCurrentName, 
                            substr(dbms_utility.format_error_stack || CHR(10) || dbms_utility.format_error_backtrace, 1,4000)  );  
    return oErrorObj;    
end getLov;

end;