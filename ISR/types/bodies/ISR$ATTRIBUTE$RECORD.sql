CREATE OR REPLACE TYPE BODY ISR$ATTRIBUTE$RECORD IS

  CONSTRUCTOR FUNCTION ISR$ATTRIBUTE$RECORD(sParameter      varchar2 default null,
                                        sValue          varchar2 default null,
                                        sPrepresent   varchar2 default null
                                    ) RETURN SELF AS RESULT IS
  BEGIN
     SELF.sParameter := sParameter;
     SELF.sValue := sValue;
     SELF.sPrepresent := sPrepresent;
     RETURN;
  END ISR$ATTRIBUTE$RECORD;                                  
END; 