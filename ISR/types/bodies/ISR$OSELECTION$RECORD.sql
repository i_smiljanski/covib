CREATE OR REPLACE TYPE BODY ISR$OSELECTION$RECORD as 
  constructor function ISR$OSELECTION$RECORD(self in out nocopy ISR$OSELECTION$RECORD, 
                                             sDisplay         varchar2 default null,
                                             sValue           varchar2 default null,
                                             sInfo            varchar2 default null,
                                             sSelected        varchar2 default null,
                                             nOrderNum        number   default null,
                                             sMasterKey       varchar2 default null,
                                             TLOATTRIBUTELIST ISR$ATTRIBUTE$LIST default ISR$ATTRIBUTE$LIST())  return self as result is
  begin
    self.sDisplay := sDisplay;
    self.sValue := sValue;
    self.sInfo := sInfo;
    self.sSelected := sSelected;
    self.nOrderNum := nOrderNum;
    self.sMasterKey := sMasterKey;
    self.TLOATTRIBUTELIST := TLOATTRIBUTELIST;
    return;
  end ISR$OSELECTION$RECORD;
                                               
  map member function Map return varchar2 is
    sAttributeHash VARCHAR2(4000);
  begin
    IF self.TLOATTRIBUTELIST IS NOT NULL AND self.TLOATTRIBUTELIST.COUNT()>0 THEN
      FOR i IN 1..self.TLOATTRIBUTELIST.COUNT() LOOP
        sAttributeHash := sAttributeHash||STB$JAVA.md5_string(self.TLOATTRIBUTELIST(i).sParameter||'#@#'||self.TLOATTRIBUTELIST(i).sValue||'#@#'||self.TLOATTRIBUTELIST(i).sPrePresent);
      END LOOP;
    END IF;
    return self.sDisplay||'#@#'||self.sValue||'#@#'||self.sInfo||'#@#'||self.sSelected||'#@#'||to_char(self.nOrderNum)||'#@#'||self.sMasterKey||'#@#'||sAttributeHash;
  end Map;
  
end;