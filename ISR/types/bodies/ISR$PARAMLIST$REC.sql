CREATE OR REPLACE TYPE BODY ISR$PARAMLIST$REC as

  constructor function iSR$ParamList$Rec(lParamList in isr$Parameter$list default isr$Parameter$list()) return self as result is
  begin
    self.lParamList := lParamList;
    return;
  end iSR$ParamList$Rec;

  member procedure AddParam(oParamRec in iSR$Parameter$Rec) is
    oParamRecExistent iSR$Parameter$Rec;
    bFound boolean := false;
  begin
    -- first try to find the parameter. Maybe it is alredy in the list
    if lParamList.count() > 0 then
      for idx in lParamList.first..lParamList.last loop
        if upper(lParamList(idx).sParameter) = upper(oParamRec.sParameter) then
          -- found. Replace it with the new value, then stop the loop
          lParamList(idx) := oParamRec;
          bFound := true;
          exit;
        end if;
      end loop;
    end if;

    if not bFound then
      -- add the new parameter
      lParamList.extend();
      lParamList(lParamList.last):= oParamRec;
    end if;
  end AddParam;

  member procedure AddParam(csParameter in varchar2, caValue in sys.anydata) is
  begin
    AddParam(iSR$Parameter$Rec(csParameter, caValue));
  end AddParam;

  member procedure AddParam(csParameter in varchar2, cnValue in number) is
  begin
    AddParam(iSR$Parameter$Rec(csParameter, cnValue));
  end AddParam;

  member procedure AddParam(csParameter in varchar2, csValue in varchar2) is
  begin
    AddParam(iSR$Parameter$Rec(csParameter, csValue));
  end AddParam;

  member procedure AddParam(csParameter in varchar2, cdValue in date) is
  begin
    AddParam(iSR$Parameter$Rec(csParameter, cdValue));
  end AddParam;

--*********************************************************************************************************************************
member function GetParamRec(csParameter in varchar2) return iSR$Parameter$Rec is
  sCurrentName  constant varchar2(100) := $$PLSQL_UNIT||'.GetParamRec';
  oParamRec iSR$Parameter$Rec;
  cursor cParameter is
    select iSR$Parameter$Rec(sParameter, aValue)
    from table(self.lParamList)
    where upper(sParameter) = upper(csParameter);
begin
  isr$trace.stat('begin','csParameter: '||csParameter,sCurrentName);
  open cParameter;
  fetch cParameter into oParamRec;
  close cParameter;
  return oParamRec;
exception
  when others then
    isr$trace.error('error in datatype method',
                     substr(dbms_utility.format_error_stack||chr(13)||chr(10)||dbms_utility.format_error_backtrace, 1,4000),
                     sCurrentName, dbms_utility.format_error_stack||chr(13)||chr(10)||dbms_utility.format_error_backtrace );
end GetParamRec;

  member function GetParamValue(csParameter in varchar2) return sys.anydata is
    aValue sys.anydata;
    oParamRec iSR$Parameter$Rec;
  begin
    -- do it in two steps to return null for nonexistent parameters, not in one as: return self.GetParamRec(csParameter).aValue;
    oParamRec := self.GetParamRec(csParameter);
    if oParamRec is not null then
      aValue := oParamRec.aValue;
    end if;
    return aValue;
  end GetParamValue;

  member function GetParamDatatype(csParameter in varchar2) return varchar2 is
    sDataType varchar2(4000);
    oParamRec iSR$Parameter$Rec;
  begin
    -- do it in two steps to return null for nonexistent parameters, not in one as: return self.GetParamRec(csParameter).aValue.GetTypeName;
    oParamRec := self.GetParamRec(csParameter);
    if oParamRec is not null then
      sDataType := oParamRec.aValue.GetTypeName;
    end if;
    return sDataType;
  end GetParamDatatype;

  member function GetParamNum(csParameter in varchar2) return Number is
    nValue number;
    nRet pls_integer;
    oParamRec iSR$Parameter$Rec;
  begin
    oParamRec := self.GetParamRec(csParameter);
    if oParamRec is not null then
      nRet := oParamRec.aValue.GetNumber(nValue);
    end if;
    return nValue;
  end GetParamNum;

  member function GetParamStr(csParameter in varchar2)
    return varchar2
  is
    sCurrentName     constant varchar2(100) := $$PLSQL_UNIT||'.GetParamStr('||csParameter||')';
    sValue           varchar2(4000);
    nRet             pls_integer;
    oParamRec        iSR$Parameter$Rec;
  begin
    isr$trace.stat('begin','csParameter: '||csParameter,sCurrentName);
    oParamRec := self.GetParamRec( csParameter );
    if oParamRec is not null then
      nRet := oParamRec.aValue.GetVarchar2(sValue);
    end if;

    isr$trace.debug('variable','oParamRec',sCurrentName,oParamRec);
    isr$trace.debug('variables','nRet: '||nRet||',  sValue: '||sValue,sCurrentName);
    isr$trace.stat('end','sValue: '||sValue,sCurrentName);
    return sValue;
  end GetParamStr;

  member function GetParamDate(csParameter in varchar2) return date is
    dValue date;
    nRet pls_integer;
    oParamRec iSR$Parameter$Rec;
  begin
    oParamRec := self.GetParamRec(csParameter);
    if oParamRec is not null then
      nRet := oParamRec.aValue.GetDate(dValue);
    end if;
    return dValue;
  end GetParamDate;


  member procedure DeleteParam(csParameter in varchar2) is
  begin
    if lParamList.count() > 0 then
      for idx in lParamList.first..lParamList.last loop
        if upper(lParamList(idx).sParameter) = upper(csParameter) then
          lParamList.delete(idx);
          exit;
        end if;
      end loop;
    end if;

  end DeleteParam;

--*********************************************************************************************************************************
member function GetVerfiedParamStr( csParameter in  varchar2,
                                    sValue      out varchar2 )
  return STB$OERROR$RECORD
is
  exNullValue       exception;
  sCurrentName      constant varchar2(100) := $$PLSQL_UNIT||'.GetVerfiedParamStr('||csParameter||')';
  oErrorObj         STB$OERROR$RECORD := STB$OERROR$RECORD();
  sMsg              varchar2(4000);

begin
  isr$trace.stat('begin','csParameter: '||csParameter, sCurrentName);

  sValue := GetParamStr( csParameter );
  if sValue is null then
    raise exNullValue;
  end if;

  isr$trace.stat('end','sValue out: '||sValue, sCurrentName);
  return oErrorObj;
exception
  when exNullValue then
    isr$trace.debug('parameter','sValue out: '||sValue,sCurrentName);
    sMsg :=  utd$msglib.getmsg ('exNullValueText', stb$security.getCurrentLanguage, csP1 => csParameter);
    oErrorObj.handleError ( STB$TYPEDEF.cnSeverityCritical, sMsg, sCurrentName,
                            DBMS_UTILITY.format_error_stack || CHR(10) || DBMS_UTILITY.format_error_backtrace );
    return oErrorObj;
end GetVerfiedParamStr;

end;