CREATE OR REPLACE type ISR$OBJ$CAN$MODIFY$OVERVIEW under isr$obj$grid(
constructor function ISR$OBJ$CAN$MODIFY$OVERVIEW(self in out nocopy ISR$OBJ$CAN$MODIFY$OVERVIEW) return self as result,
static function putParameters(oParameter in stb$menuentry$record, olParameter in stb$property$list, clParameter in clob default null)
  return stb$oerror$record,
static function callFunction (oParameter in STB$MENUENTRY$RECORD, olNodeList out STB$TREENODELIST)
  return STB$OERROR$RECORD
)
  not final