CREATE OR REPLACE TYPE ISR$MESSAGE$REC force AS OBJECT (
  sUserMsg VARCHAR2(4000),  
  sImplMsg VARCHAR2(4000),
  sDevMsg  VARCHAR2(32000),  
  constructor function ISR$MESSAGE$REC(self in out nocopy ISR$MESSAGE$REC,
                                       sUserMsg varchar2 default null,
                                       sImplMsg varchar2 default null,
                                       sDevMsg  varchar2 default null) return self as result )