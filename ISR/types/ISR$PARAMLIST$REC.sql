CREATE OR REPLACE TYPE ISR$PARAMLIST$REC force as object(
  lParamList isr$Parameter$List, 
  constructor function iSR$ParamList$Rec(lParamList in isr$Parameter$list default isr$Parameter$list()) return self as result,
  -- replaces the default constructor
  member procedure AddParam(oParamRec in iSR$Parameter$Rec),
  -- adds a parameter. If the parameter exists it is replaced with the new one
  member procedure AddParam(csParameter in varchar2, caValue in sys.anydata),
  -- adds a anydata parameter. If the parameter exists it is replaced with the new one
  member procedure AddParam(csParameter in varchar2, cnValue in number),
  -- adds a numeric parameter. If the parameter exists it is replaced with the new one
  member procedure AddParam(csParameter in varchar2, csValue in varchar2),
  -- adds a string parameter. If the parameter exists it is replaced with the new one
  member procedure AddParam(csParameter in varchar2, cdValue in date),
  -- adds a date parameter. If the parameter exists it is replaced with the new one
  member function GetParamRec(csParameter in varchar2) return iSR$Parameter$Rec,
  -- finds a parameter in the list and returns it (the rec)
  member function GetParamValue(csParameter in varchar2) return sys.anydata,
  -- finds a parameter in the list and returns it as anydata
  member function GetParamDatatype(csParameter in varchar2) return varchar2,
  -- finds a parameter in the list and returns the datatype name
  member function GetParamNum(csParameter in varchar2) return Number,
  -- finds a numeric parameter in the list and returns its value
  member function GetParamStr(csParameter in varchar2) return varchar2,
  -- finds a string parameter in the list and returns its value
  member function GetParamDate(csParameter in varchar2) return date,
  -- finds a date parameter in the list and returns its value
  member procedure DeleteParam(csParameter in varchar2),
  -- deletes a parameter in the list 
  member function GetVerfiedParamStr(csParameter in varchar2, sValue out varchar2) return STB$OERROR$RECORD 
  -- finds a string parameter in the list and returns its value or throws exception if a value is null
)