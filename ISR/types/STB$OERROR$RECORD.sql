CREATE OR REPLACE TYPE STB$OERROR$RECORD force as object (SERRORCODE    varchar2(2000),
                                                     SERRORMESSAGE varchar2(4000),
                                                     SSEVERITYCODE integer,
                                                     TLOMESSAGELIST  ISR$MESSAGE$LIST,                                  
constructor function STB$OERROR$RECORD(self in out nocopy STB$OERROR$RECORD, 
                                       sErrorCode     in varchar2 default null,
                                       sErrorMessage  in varchar2 default null,
                                       sSeverityCode  in integer default null,
                                       tloMessageList in ISR$MESSAGE$LIST default null) return self as result,
                                       -- replaces the default constructor  
member procedure  setSeverityCode ( csSeveritycode IN integer),
member procedure  handleError ( csSeveritycode IN integer, csUserInfo in varchar2, csImplInfo in varchar2, csDevInfo in varchar2, csErrorCode in varchar2 default null),
-- defined to handle an error in the exception handling of the application code
-- parameters:
--    csSeveritycode  the severity codes from Stb$typedef
--    csUserInfo      the Information for the User - the message for the user. It is used also to fill sErrorMessage
--    csImplInfo      the Message place, (procedure name), NOT the message for the implementor. 
--    csDevInfo       additional info for developer used to build the developer Message along with the other params  
--    csErrorCode     used only for backward compatibility and special needs in the frontend

member procedure  handleJavaError ( csSeveritycode in integer, 
                                    cxError        in xmltype, 
                                    csOccurredName in varchar2),
-- defined to handle an java error while error info is passed in xml format
-- parameters:
--    csSeveritycode  the severity codes from Stb$typedef
--    xError the error in xml format
--    sOccurredName place where the error occured

member procedure  handleJavaError ( csSeveritycode in integer, 
                                    cclError       in clob, 
                                    csOccurredName in varchar2),
-- defined to handle an java error while error info is passed in xml format contained in an clob 
-- overloads and calls the procedure above after the clob is converted in xmltype 
-- parameters:
--    csSeveritycode  the severity codes from Stb$typedef
--    xError the error in xml format
--    sOccurredName place where the error occured
--********************************************************************************************************************************
member procedure checkRemoteException ( sCurrentName  in varchar2, 
                                        csSeveritycode in  integer,
                                        sRemoteEx       in clob),
--********************************************************************************************************************************
member function GetLogLevelForSeverityCode  return varchar2,
-- This function contains the business logic for the mapping of severity-codes from STB$TYPEDEF to a defined loglevel.


member procedure writeLog( sLogLevel      in varchar2,
                           sActionTaken   in varchar2  default null,
                           sLogEntry      in varchar2,
                           sFunctionGroup in varchar2,
                           clClob         in clob      default EMPTY_CLOB()  ),
-- This procedure is the single point in the STB$OERROR$RECORD object, that writes 
-- into the logging table ISR$LOG. Therefore it defines the business rule for output into that table.

member function checkWriteActionLog return boolean, 
-- This procedure is the single point in the STB$OERROR$RECORD object, decides to write into the action-log
-- If the returned value is TRUE or FALSE depends on the severity-level.

member function GetUserMessages return varchar2,
-- retrieves and returns the user messages part of the error stack stored in tloMessageList.sUserMsg
-- the messages are separated by a carriage return chr(10)

member function GetImplMessages return varchar2,
-- retrieves and returns the implementor messages part of the error stack stored in tloMessageList.sImplMsg
-- the messages are separated by a carriage return chr(10)

member function GetDevMessages return varchar2,
-- retrieves and returns the developer messages part of the error stack stored in tloMessageList.sDevMsg
-- the messages are separated by a carriage return chr(10)

member procedure GetMessages(sUserMsg out varchar2, sImplMsg out varchar2, sDevMsg out varchar2),
-- retrieves the user, implementor and developer messages part of the error stack stored in tloMessageList
-- and return them in the respective parameters
-- the messages are separated by a carriage return chr(10)

member procedure SetSeverityCodeToWorst,
-- retrieves the worst severity code from the error object accessor package and sets this in sErrorCode of the object

member procedure SetMessageListFromAccessor,
-- retrieves the message list from the error object accessor package and sets this in tloMessageList of the object
member function isExceptionCritical return boolean
-- checks sSeverityCode for critical
)