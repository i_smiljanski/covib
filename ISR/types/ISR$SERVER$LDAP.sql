CREATE OR REPLACE TYPE ISR$SERVER$LDAP UNDER   ISR$SERVER$RECORD (
--*************************************************************************************************************************
static function getServerType return number
 --*************************************************************************************************************************
, static function verifyLDAPConnection (sUsername in varchar2, sPassword in varchar2) 
  return STB$OERROR$RECORD  
-- ***************************************************************************************************
, static function getNodeDetails(  clXML      out clob) return STB$OERROR$RECORD
--*************************************************************************************************************************
, static function getLDAPServerStatus (nServerid in number) return clob  
--*************************************************************************************************************************
, static function getLDAPRunStatus(nServerid in number) return varchar2                    
)