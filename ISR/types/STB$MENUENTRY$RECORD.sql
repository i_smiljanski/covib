CREATE OR REPLACE TYPE STB$MENUENTRY$RECORD FORCE AS OBJECT (STYPE        VARCHAR2(20),
                                     SDISPLAY     VARCHAR2(255),
                                     STOKEN       VARCHAR2(255),
                                     SHANDLEDBY   VARCHAR2(255),
                                     SICON        VARCHAR2(255),
                                     SCONFIRM     VARCHAR2(1),
                                     SCONFIRMTEXT VARCHAR2(4000),
                                     nID          NUMBER,
  constructor function STB$MENUENTRY$RECORD(self in out nocopy STB$MENUENTRY$RECORD, 
                                            sType        varchar2 default null,
                                            sDisplay     varchar2 default null,
                                            sToken       varchar2 default null,
                                            sHandledBy   varchar2 default null,
                                            sIcon        varchar2 default null,                                             
                                            sConfirm     varchar2 default null,
                                            sConfirmText varchar2 default null,
                                            nID          number default null) return self as result,                                      
member function toString( csVariableName varchar2 ) return varchar2
) 