CREATE OR REPLACE TYPE iSR$SelectionData$Rec AS OBJECT
(  lSelectionList ISR$TLRSELECTION$LIST,
-- the object is used to hold data for one selection list and perform operations on this list
-- ***************************************************************************************************
-- Modification history:
-- Ver   Date              Autor    System             Project                            Remarks
-- 1.0   11 Sep  2008      HR       iStudyReporter    iStudyReporter@watson               Creation
-- ***************************************************************************************************                                                 
  constructor function iSR$SelectionData$Rec(self in out nocopy iSR$SelectionData$Rec) return self as result,
  -- constructor for empty initialisation
  constructor function iSR$SelectionData$Rec(self in out nocopy iSR$SelectionData$Rec, lSelectionList ISR$TLRSELECTION$LIST) return self as result,
  -- overwrites the default contructor with not null values
  member procedure OrderSelection,
  -- reorders the list by the record element nOrderNum
  member procedure SetSelectionOrder
  -- sets the record element nOrderNum corresponding to the order in the collection
  ) not final