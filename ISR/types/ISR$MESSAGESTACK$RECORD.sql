CREATE OR REPLACE TYPE ISR$MESSAGESTACK$RECORD AS OBJECT (
                                  SDEVSTACK VARCHAR2(4000),
                                  SIMPLSTACK VARCHAR2(4000),
                                  SUSERSTACK    VARCHAR2(4000),
                                  constructor function ISR$MESSAGESTACK$RECORD(self in out nocopy ISR$MESSAGESTACK$RECORD,
                                                                             sDevstack    varchar2 default null,
                                                                             sImplstack varchar2 default null,
                                                                             sUserstack varchar2 default null) return self as result,
                                  member procedure writeStack(sUserstack in VARCHAR, sImplemstack in VARCHAR2 default null, sDevstack in VARCHAR2 default null) ) 