CREATE OR REPLACE TYPE ISR$OSELECTION$RECORD FORCE as object (sDisplay         varchar2(4000), -- [ISRC-1071]
                                      sValue           varchar2(4000),
                                      sInfo            varchar2(4000),
                                      sSelected        varchar2(1),
                                      nOrderNum        number,
                                      sMasterKey       varchar2(500) ,
                                      TLOATTRIBUTELIST ISR$ATTRIBUTE$LIST,   
                                      
  constructor function ISR$OSELECTION$RECORD(self in out nocopy ISR$OSELECTION$RECORD, 
                                             sDisplay         varchar2 default null,
                                             sValue           varchar2 default null,
                                             sInfo            varchar2 default null,
                                             sSelected        varchar2 default null,
                                             nOrderNum        number   default null,
                                             sMasterKey       varchar2 default null,
                                             TLOATTRIBUTELIST ISR$ATTRIBUTE$LIST default ISR$ATTRIBUTE$LIST())  return self as result,
  -- overwrites the default contructor   
                                                                                    
  -- map member function used for select distinct
  map member function Map return varchar2
) not final