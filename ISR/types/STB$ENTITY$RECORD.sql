CREATE OR REPLACE TYPE STB$ENTITY$RECORD FORCE as object (sEnt           varchar2(500),
                                  sPreSelected   varchar2(1),
                                  sEditable      varchar2(1),
-- the object is used to hold data for one Entity in a entity list
-- ***************************************************************************************************
-- Modification history:
-- Ver   Date              Autor    System             Project                    Remarks
-- 1.0   24 Okt  2008      HR       iStudyReporter    iStudyReporter@watson       Added constructor functions and sEditable
-- ***************************************************************************************************
  constructor function STB$ENTITY$RECORD(self in out nocopy STB$ENTITY$RECORD) return self as result,
  -- constructor for empty initialisation
  constructor function STB$ENTITY$RECORD(self in out nocopy STB$ENTITY$RECORD,
                                         sEnt in varchar2, sPreSelected in varchar2, sEditable in varchar2) return self as result
  -- constructor overwrites default constructor
) not final 