CREATE OR REPLACE TYPE STB$PROPERTY$RECORD FORCE as OBJECT (sParameter      varchar2(200),
                                    sPromptDisplay  varchar2(200),
                                    sValue          varchar2(4000) ,
                                    sDisplay        varchar2(4000),
                                    sEditable       varchar2(1),
                                    sRequired       varchar2(1),
                                    sDisplayed      varchar2(1),
                                    hasLov          varchar2(1),
                                    type            varchar2(25),
                                    format          varchar2(25),
                                    sFocus          varchar2(1),

  constructor function STB$PROPERTY$RECORD(self in out nocopy STB$PROPERTY$RECORD,
                                           sParameter      varchar2 default null,
                                           sPromptDisplay  varchar2 default null,
                                           sValue          varchar2 default null,
                                           sDisplay        varchar2 default null,
                                           sEditable       varchar2 default null,
                                           sRequired       varchar2 default null,
                                           sDisplayed      varchar2 default null,
                                           hasLov          varchar2 default null,
                                           type            varchar2 default null,
                                           format          varchar2 default null,
                                           sFocus          varchar2 default 'F'
                                            ) return self as result
) 