CREATE OR REPLACE type isr$obj$grid force under isr$obj$dialog(
  constructor function ISR$OBJ$GRID(self in out nocopy ISR$OBJ$GRID) return self as result,
  member function modifyGridTable(csToken  in varchar2,
                        csModifyXml in clob,
                        coAddParamList in isr$paramlist$rec ) return STB$OERROR$RECORD,
-- ***************************************************************************************************
-- Date und Autor: 06 Dec 2016  is
-- processing of grid tables: writes the data from xXml to table 'sTable'
--
-- Parameter:
-- csToken  Token
-- coAddParamList  additional, token-specific parameters ('TABLE', 'SYSTEMAUDIT', ..)
--
-- Return:
--   varchar2
--
-- Example XML:
 /*
<?xml version="1.0" encoding="UTF-8"?>
<DOCUMENT>
	<UPDATE count="16">
		<ROW>
			<ENTITY>ITERATION</ENTITY>
			<DESCRIPTION>Iteration for Oracle jobs</DESCRIPTION>
			<ROWID>AAATy+AAKAAAAJrAAP</ROWID>
		</ROW>
		..
	</UPDATE>
    <DELETE count="1">
        <ROW>
            <ENTITY>test</ENTITY>
            <DESCRIPTION>test</DESCRIPTION>
            <ROWID>AAATy+AAKAAAAJsAAA</ROWID>
        </ROW>
    </DELETE>
</DOCUMENT>
*/

-- ***************************************************************************************************
  member function buildGridMask(csToken  in varchar2, coAddParamList in  isr$paramlist$rec, olNodeList out STB$TREENODELIST)
  return STB$OERROR$RECORD,
  member function executeStatement(sAction varchar2, sTable varchar2, sRowId varchar2, sParameter varchar2, sStmt IN OUT varchar2, 
                sOldValue IN OUT varchar2, sNewValue varchar2) return boolean
   --,
  --member function test_em(sTable varchar2) return boolean
) not final