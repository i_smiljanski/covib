package com.uptodata.isr.db.connection;

import com.uptodata.isr.db.trace.DbLogging;
import com.uptodata.isr.utils.ConvertUtils;
import com.uptodata.isr.utils.exceptions.MessageStackException;
import oracle.jdbc.OracleCallableStatement;
import oracle.jdbc.OracleDriver;

import java.io.IOException;
import java.sql.*;

import static com.uptodata.isr.db.DbConvertUtils.checkError;

/**
 * Created by smiljanskii60 on 12.Feb.2016.
 */
public class ConnectionUtil {

    public static Connection getDefaultConnection() throws SQLException {
        Connection conn = new OracleDriver().defaultConnection();
        return  conn;
    }


    public static void closeStatement(OracleCallableStatement stmt) throws SQLException {
        if (stmt != null ) {
            stmt.close();
        }
    }

    public static OracleCallableStatement prepareCall(String stringStmt) throws SQLException {
        OracleCallableStatement statement = (OracleCallableStatement) getDefaultConnection().prepareCall(stringStmt);
        return statement;
    }

    public static Clob initializeSession(Connection conn, DbLogging log) throws SQLException {
        log.debug("begin", "begin");
        OracleCallableStatement statement = (OracleCallableStatement) conn.prepareCall(
                "begin ? := isr$remote$service.initializeSession; end; ");
        statement.registerOutParameter(1, java.sql.Types.CLOB);
        statement.executeQuery();
        Clob clobError = statement.getClob(1);
        ConnectionUtil.closeStatement(statement);
        log.debug("end", "end");
        return clobError;
    }

    /**
     * connects to the database, initializes iSR session
     * if any errors occurs, MessageStackException is thrown
     * @param log
     * @return connection object
     * @throws SQLException
     * @throws IOException
     * @throws MessageStackException
     */
    public static Connection getInitializeConnection(DbLogging log) throws SQLException, IOException, MessageStackException {
        Connection conn = getDefaultConnection();
        Clob clobError = initializeSession(conn, log);
        checkError(clobError);
        return  conn;
    }

}
