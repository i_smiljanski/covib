package com.uptodata.isr.db.dbservlets;

import com.uptodata.isr.db.trace.DbLogging;
import org.apache.commons.lang.StringUtils;

import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import java.io.UnsupportedEncodingException;
import java.net.URLDecoder;
import java.util.Enumeration;

/**
 * Created by smiljanskii60 on 12.Feb.2016.
 */
public class ServletUtil extends HttpServlet {

    public String getRequestRequiredParameter(HttpServletRequest request, String parameter) throws Exception {
        String paramValue = getRequestParameter(request, parameter);
        if (StringUtils.isEmpty(paramValue)) {
            throw new Exception("request parameter " + parameter + " is null!");
        }
        return paramValue;
    }


    public String getRequestParameter(HttpServletRequest request, String parameter) {
        for (Enumeration en = request.getParameterNames(); en.hasMoreElements(); ) {
            String reqParamName = (String) en.nextElement();
            if (reqParamName.equalsIgnoreCase(parameter)) {
                String paramValue = request.getParameter(reqParamName);
                try {
                    paramValue = URLDecoder.decode(paramValue, "UTF-8");
                } catch (Exception e) {
                    System.out.println("warn: " + e);
                }
                return paramValue;
            }
        }
        return null;
    }


    public boolean checkServletPath(String path, String servletName) {
        boolean checkPath = false;
        if (StringUtils.isNotEmpty(path) && StringUtils.isNotEmpty(servletName)) {
            checkPath = path.toLowerCase().contains(servletName.toLowerCase());
        }
        return checkPath;
    }


    public void initLog(DbLogging log, HttpServletRequest req) {
        String logLevel = getRequestParameter(req, "logLevel");
        if (StringUtils.isEmpty(logLevel)) {
            logLevel = "ERROR";
        }
        log.setLogLevel(logLevel);

        String logReference = getRequestParameter(req, "logReference");
        log.setLogReference(logReference);

    }

}
