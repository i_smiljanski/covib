package com.uptodata.isr.db;

import com.uptodata.isr.db.trace.DbLogging;
import com.uptodata.isr.utils.ConvertUtils;
import com.uptodata.isr.utils.exceptions.MessageStackException;
import oracle.aurora.vm.OracleRuntime;
import oracle.sql.*;
import org.apache.commons.collections4.map.CaseInsensitiveMap;

import java.io.IOException;
import java.io.Writer;
import java.sql.*;
import java.util.Map;

import static com.uptodata.isr.utils.ConvertUtils.getBytes;

/**
 * Created by smiljanskii60 on 16.09.2019.
 */
public class DbConvertUtils {


    public static Clob getClob(Connection connection, byte[] data, DbLogging log) throws SQLException, IOException {

        Clob clob = connection.createClob();
        if (data != null) {
            Writer writer = clob.setCharacterStream(1);
            log.debug("before convertBytesToString", data.length);
            String str = convertBytesToString(data, log);
            log.debug("string length", str.length());
            writer.write(str);
            writer.flush();
            writer.close();
        }
        return clob;
    }


    public static String convertBytesToString(byte[] bytes, DbLogging log) throws IOException {

        try {
            return tryToConvertBytesToString(bytes, log);
        } catch (OutOfMemoryError e) {
            int times = 2;
            log.info("OutOfMemoryError occurred", "Java memory will be increased by a factor of " + times);
            increaseMemory(times);
            return tryToConvertBytesToString(bytes, log);
        }
    }


    private static String tryToConvertBytesToString(byte[] bytes, DbLogging log) throws IOException {
        StringBuilder response = new StringBuilder();

        int basicBufferSize = 1024 * 1024;
        int expectedSize = bytes.length;
        log.debug("getStatistics", getStatistics());

        log.debug("expectedSize; basicBufferSize=", expectedSize + "; " + basicBufferSize);
        int pos = 0;
        while (true) {
            if (basicBufferSize + pos > expectedSize) {
                log.debug("pos; last: ", pos + "; " + (expectedSize - pos));
                response.append(new String(bytes, pos, expectedSize - pos, ConvertUtils.encoding));

                log.debug("after concatenation", response.length());
                return response.toString();
            }
//                log.debug(" pos", pos);
            try {
                String partStr = new String(bytes, pos, basicBufferSize, ConvertUtils.encoding);
                response.append(partStr);
//                log.debug("position after append",pos);
            } catch (OutOfMemoryError e) {
                for (StackTraceElement stackTraceElement : e.getStackTrace()) {
                    log.error("OutOfMemoryError stack", stackTraceElement.getClassName() + "." + stackTraceElement.getMethodName() + "(" + stackTraceElement.getLineNumber() + ")");
                }
                throw new OutOfMemoryError("OutOfMemoryError during append String bei position " + pos);
            }

            pos += basicBufferSize;
        }
    }


    private static String getStatistics() {
        int MB_MULTIPLICATOR = 1024 * 1024;
        final Runtime runtime = Runtime.getRuntime();

        final long totalMemory = runtime.totalMemory();
        final long usedMemory = totalMemory - runtime.freeMemory();

        return usedMemory / MB_MULTIPLICATOR +
                " / " +
                totalMemory / MB_MULTIPLICATOR +
                " MB (used / total), getMaxMemorySize " + OracleRuntime.getMaxMemorySize() / MB_MULTIPLICATOR + " MB " +
                ", running on " +
                System.getProperty("java.version");
    }


    public static Map<String, Object> anyDataArrayToStringMap(Array params) throws Exception {
        Map<String, Object> map = new CaseInsensitiveMap<String, Object>();
        Object[] elements = (Object[]) params.getArray();
        for (Object element : elements) {
            Struct s = (Struct) element;
            Object[] data = s.getAttributes();
            String paramName = (String) data[0];
            ANYDATA obj = ((ANYDATA) data[1]);  //new ANYDATA((OPAQUE) data[1]);
            Object value = null;
            if (!obj.isNull()) {
                Datum dat = obj.accessDatum();
                TypeDescriptor typeDesc = obj.getTypeDescriptor();
                if (typeDesc.getTypeCode() == TypeDescriptor.TYPECODE_VARCHAR2) {
                    value = ((CHAR) dat).stringValue();
                } else if (typeDesc.getTypeCode() == TypeDescriptor.TYPECODE_NUMBER) {
                    value = ((NUMBER) dat).stringValue();
                } else if (typeDesc.getTypeCode() == TypeDescriptor.TYPECODE_DATE) {
                    value = ((DATE) dat).stringValue();
                } else if (typeDesc.getTypeCode() == TypeDescriptor.TYPECODE_BLOB) {
                    Blob blobFile = (Blob) dat;
                    value = getBytes(blobFile);
                } else if (typeDesc.getTypeCode() == TypeDescriptor.TYPECODE_CLOB) {
                    Clob clobFile = (Clob) dat;
                    value = getBytes(clobFile);
                }
            }
            map.put(paramName, value);
        }
        return map;
    }


    public static byte[] convertAnyDataToBytes(ANYDATA anydata) throws Exception {
        TypeDescriptor typeDesc = anydata.getTypeDescriptor();
        Datum embeddedDatum = anydata.accessDatum();
        if (embeddedDatum == null) {
            throw new Exception("data from database are empty");
        }

        byte[] bytes;

        if (typeDesc.getTypeCode() == TypeDescriptor.TYPECODE_BLOB) {
            Blob blobFile = (Blob) embeddedDatum;
            bytes = getBytes(blobFile);
        } else if (typeDesc.getTypeCode() == TypeDescriptor.TYPECODE_CLOB) {
            Clob clobFile = (Clob) embeddedDatum;
            bytes = getBytes(clobFile);
        } else if (typeDesc.getTypeCode() == TypeDescriptor.TYPECODE_VARCHAR2) {
            String stringValue = embeddedDatum.stringValue();
            bytes = stringValue.getBytes(ConvertUtils.encoding);
        } else {
            String errorMsg = "Type Code of ANYDATA object is unknown(type code from ANYDATA object: " +
                    typeDesc.getTypeCode() + ")";
            throw new Exception(errorMsg);
        }
        return bytes;
    }


    private static void increaseMemory(int times) {
        OracleRuntime.setMaxRunspaceSize(times * OracleRuntime.getMaxRunspaceSize());
        OracleRuntime.setSessionGCThreshold(times * OracleRuntime.getSessionGCThreshold());
        OracleRuntime.setNewspaceSize(times * OracleRuntime.getNewspaceSize());
        OracleRuntime.setMaxMemorySize(times * OracleRuntime.getMaxMemorySize());
        OracleRuntime.setJavaStackSize(times * OracleRuntime.getJavaStackSize());
        OracleRuntime.setThreadStackSize(times * OracleRuntime.getThreadStackSize());
    }


    public static void checkError(Clob clobError) throws SQLException, IOException, MessageStackException {
        if (clobError != null && clobError.length() > 0) {
            byte[] errBytes = getBytes(clobError);
            throw new MessageStackException(errBytes);
        }
    }

    public static void main(String[] args) {
//        verbose:gc -XX:+UseG1GC -XX:+UseStringDeduplication
        try {
            byte[] test = ConvertUtils.fileToBytes("tmp0000.htm");

//            FileTransferServlet servlet = new FileTransferServlet();

            String s1 = DbConvertUtils.convertBytesToString(test, new DbLogging());
            String s2 = DbConvertUtils.convertBytesToString(test, new DbLogging());
//            ConvertUtils.convertBytesToString(test);
        } catch (Exception e) {
            e.printStackTrace();
        }

    }
}
