package com.uptodata.isr.db.connection;


import oracle.jdbc.OracleDriver;

import java.sql.Connection;
import java.sql.SQLException;

public class ConnectionFactory {
    private static interface Singleton {
        final ConnectionFactory INSTANCE = new ConnectionFactory();
    }

    private final Connection conn;

    private ConnectionFactory() {
        Connection connection = null;
        try {
            OracleDriver ora = new OracleDriver();
            connection = ora.defaultConnection();

        } catch (SQLException e) {
            System.out.println("e = " + e);

        }
        conn = connection;
    }

    public static Connection getDatabaseConnection() {
        ConnectionFactory instance = Singleton.INSTANCE;
        if (instance != null) {
            Connection connection = instance.conn;
//            System.out.println("CommonUtils (ConnectionFactory.getDatabaseConnection()): connection = " + connection);
            return connection;
        }
        return null;
    }



}