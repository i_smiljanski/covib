package com.uptodata.isr.db.connection;


import java.sql.*;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.List;

/**
 * Sets positional parameters in a {@link java.sql.PreparedStatement}. This class provides convenient
 * way of setting parameters (and setting the pair parameters) without having to
 * remember the parameter position. The pair parameters allow you to write SQL similar to
 * <code><tt>select ... where ((? is null) or (someColumn = ?))</tt></code>.<br/>
 * Another benefit of using this class is that it can return the SQL statement with the positional
 * arguments set. This is useful for debugging purposes.
 *
 * @author janm
 */
public final class PreparedStatementParameterSetter {
    private DateFormat SQL_DATE_FORMAT = new SimpleDateFormat("yyyy-MM-dd");

    private PreparedStatement preparedStatement;
    private int parameterIndex;
    private List<Object> arguments;

    /**
     * Create instance of this class that will add parameters to the {@code preparedStatement}
     *
     * @param preparedStatement The {@link java.sql.PreparedStatement} to set, never {@code null}
     */
    public PreparedStatementParameterSetter(PreparedStatement preparedStatement) {
//        Assert.notNull(preparedStatement, "The 'preparedStatement' argument cannot be null.");
        this.arguments = new ArrayList<Object>();
        this.preparedStatement = preparedStatement;
        this.parameterIndex = 1;
    }


    /**
     * Sets a date; typically used for <code>someColumn = ?</code>, where {@code someColumn}'s
     * data type is {@code datetime}
     *
     * @param date The date value to set, never {@code null}
     * @throws java.sql.SQLException If the parameter cannot be set
     */
    public void setDate(Date date) throws SQLException {
        this.preparedStatement.setDate(this.parameterIndex, new Date(date.getTime()));
        this.parameterIndex++;
        this.arguments.add(date);
    }


    /**
     * Sets an int parameter; typically used for <code>someColumn = ?</code>, where {@code someColumn}'s
     * data type is {@code number}
     *
     * @param i The integer value to set
     * @throws java.sql.SQLException If the parameter cannot be set
     */
    public void setInt(int i) throws SQLException {
        this.preparedStatement.setInt(this.parameterIndex, i);
        this.parameterIndex++;
        this.arguments.add(i);
    }


    public void setString(String s) throws SQLException {
        if (s != null) {
            this.preparedStatement.setString(this.parameterIndex, s);
            this.parameterIndex++;
            this.arguments.add(s);
        }
    }


    public void setObject(Object o) throws SQLException {
        if (o != null) {
            this.preparedStatement.setObject(this.parameterIndex, o);
            this.parameterIndex++;
            this.arguments.add(o);
        }
    }

    public void setLong(long l) throws SQLException {
        this.preparedStatement.setLong(this.parameterIndex, l);
        this.parameterIndex++;
        this.arguments.add(l);
    }


    public void setBlob(Blob b) throws SQLException {
        if (b != null) {
            this.preparedStatement.setBlob(this.parameterIndex, b);
            this.parameterIndex++;
            this.arguments.add(b);
        }
    }


    public void setClob(Clob c) throws SQLException {
        if (c != null) {
            this.preparedStatement.setClob(this.parameterIndex, c);
            this.parameterIndex++;
            this.arguments.add(c);
        }
    }


    /**
     * Prepares a SQL statement with the positional parameters replaced by the values set in the calls to the
     * various {@code set*} methods.
     *
     * @param sqlWithPositionalParameters The text of the SQL statement with positional parameters
     * @return The SQL statement with all positional parameters set to the actual values
     */
    public String prepareSql(String sqlWithPositionalParameters) {
        int parameterIndex = 0;
        StringBuilder sb = new StringBuilder();
        for (int i = 0; i < sqlWithPositionalParameters.length(); i++) {
            char c = sqlWithPositionalParameters.charAt(i);
            if (c != '?') {
                sb.append(c);
            } else {
                sb.append(formatParameter(this.arguments.get(parameterIndex)));
                parameterIndex++;
            }
        }
        return sb.toString();
    }

    private String formatParameter(Object parameter) {
        if (parameter == null) return "null";
        if (parameter instanceof Date) return String.format("'%s'", SQL_DATE_FORMAT.format(parameter));
        if (parameter instanceof String) return String.format("'%s'", parameter);
        return parameter.toString();
    }

    public PreparedStatement getPreparedStatement() {
        return preparedStatement;
    }
}
