package com.uptodata.isr.db;

import com.uptodata.isr.utils.ConvertUtils;
import org.apache.commons.lang.StringUtils;

import java.io.IOException;
import java.io.InputStream;
import java.io.UnsupportedEncodingException;
import java.sql.Clob;
import java.sql.SQLException;

public class HashCalculation {
    public static void main(String args[]) {
        try {
            System.out.println(calculateHash("2e9e5208ca1cd2453db421ded1e12628"));
        } catch (UnsupportedEncodingException e) {
            e.printStackTrace();
        }
    }

    public static String calculateHash(String value) throws UnsupportedEncodingException {
        if (StringUtils.isNotEmpty(value)) {
            byte[] buffer = value.getBytes(ConvertUtils.encoding);
            return ConvertUtils.calculateHash(buffer);
        }
        return null;
    }

    public static String calculateClobHash(Clob clob) throws SQLException, IOException {
        byte[] data = new byte[new Long(clob.length()).intValue()];
        InputStream in = clob.getAsciiStream();
        in.read(data);
        in.close();
        return ConvertUtils.calculateHash(data);
    }

}