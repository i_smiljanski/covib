package com.uptodata.isr.mail;

import com.uptodata.isr.db.trace.DbLogging;

import javax.activation.DataSource;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.sql.Blob;
import java.sql.SQLException;


public class BlobDataSource implements DataSource {

    private Blob blob;
    private String name;

    public BlobDataSource(Blob blob, String name) {

        this.blob = blob;
        this.name = name;
    }

    public String getContentType() {

        return "application/octet-stream";
    }

    public InputStream getInputStream() throws IOException {

        DbLogging log = new DbLogging();
        log.stat("begin", "begin");
        try {
            InputStream is = blob.getBinaryStream();
            log.stat("end", "end");
            return is;
        } catch (SQLException e) {
            log.error("exception", e);
        }
        return null;
    }

    public OutputStream getOutputStream() throws IOException {

        return new ByteArrayOutputStream(0);
    }

    public String getName() {

        return name;
    }
}