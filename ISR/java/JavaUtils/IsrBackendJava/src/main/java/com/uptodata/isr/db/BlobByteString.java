package com.uptodata.isr.db;

import com.uptodata.isr.db.trace.DbLogging;
import com.uptodata.isr.utils.ConvertUtils;
import org.apache.commons.codec.binary.Base64;

import java.io.InputStream;
import java.sql.Blob;
import java.sql.Clob;
import java.sql.Connection;
import java.sql.DriverManager;

public class BlobByteString {

    public static Clob getBlobByteString(Blob blob) {
        DbLogging log = new DbLogging();
        log.stat("begin", "begin");
        try {
            byte[] data = ConvertUtils.getBytes(blob);
            String text = Base64.encodeBase64String(data);
            Connection conn = DriverManager.getConnection("jdbc:default:connection");
            Clob clText = conn.createClob();
//            Clob clText = Clob.createTemporary(DriverManager.getConnection("jdbc:default:connection"), true, CLOB.DURATION_SESSION);
            clText.setString(1, text);
            return clText;
        } catch (Exception e) {
            log.error("exception", e);
        }
        log.stat("end", "end");
        return null;
    }

    public static Blob getStringByteBlob(Clob clText) {
        DbLogging log = new DbLogging();
        log.stat("begin", "begin");

        try {
            InputStream is = clText.getAsciiStream();
            String text = ConvertUtils.inputStreamToString(is);
            byte[] data = Base64.decodeBase64(text);
            return ConvertUtils.getBlob(DriverManager.getConnection("jdbc:default:connection"), data);
        } catch (Exception e) {
            log.error("exception", e);
        }
        log.stat("end", "end");
        return null;
    }

    public static void main(String[] args) throws Exception {

        //System.out.println(getBlobByteString(null));
        //System.out.println(Util.bytesToFile(getStringByteBlob("iVBORw0KGgoAAAANSUhEUgAAABAAAAAQCAYAAAAf8/9hAAAAK3RFWHRDcmVhdGlvbiBUaW1lAEZyIDE1IE5vdiAyMDAyIDEwOjA4OjI5ICswMTAw9aJixgAAAAd0SU1FB9MEGBAlLP5eAuUAAAAJcEhZcwAACvAAAArwAUKsNJgAAAAEZ0FNQQAAsY8L/GEFAAACjUlEQVR42qWTX0iTURjGf9+3uRnppqWLGGT5L0SErroLQzO72kV00VUiGBWKEURJilAiSBfddFFkQoi3dTOILMjKS8MphgSr6dYfN3Xp9m1z375/HbUUsa488MJ5D+d53vd5zntgj0vadfKMAqK0yQ7Zd2h/cbWh69JSSglaGdPPPgbpIvl/gmFOl2TdIy0nmr01nmNElAjR9CK/VIVgLMrs3EJMs2mXuMnr3QQCXGsvf9V9qsUZSX8lmJxhVc2QUnOkcjpKTiWpqvwIKzlNNn2CZHQdJv9tu2TNNdJT3+qcWZ0gqExhWKZgl7EsSYRFIpvmckU7ZRVuB3FpmNsUbRP8oO1iXZM3ooSIrUUwTQlVN1ANE900hYw4b5sC3Knrx18/Sn5poQeVa9sEefhqPJVMxyf4lkiQ1nJkRGQ1je/KEm/OTFPurgI7jK28IGvmCfGSb4ugVLi9kPnJRGiJRvt1JheiJNQ0kWSM0cYpyosF2AZPQj10jA9ALl+gnMe3CCzDlD4th7hX/ZAbtR08P/kOJWPjZUOAo0WVG00+FeDO8X5MXSQZF5ib3m8QLCfFO0smfeFbJPQkFYWVfDwXprygauPSUKiXzg/9aIZIcgKSLgQtG9z2YM30z8eWSFhRLkzWI8kSmmVsaB6a66X9fZ8wVRRdrxorA2MVdPzbBC4GP4ciUd2yEYjP0hJoYMWYF213c2VMgDUBXq++fABWDgvwXByDRzsHaYCzdkP2272yI2voFDpAWRPnxp9YPAJxrzBsVic/cZ4Hmx3sHOW7NJMVQ+J2eshzCp1CQ6YAUgfFPiUiHMeltnJ/E/zvz9QlJizNVUybDzmvCksShuS+CA1+4fxjhojv9QfvWL8Bzesu9yCbl+cAAAAASUVORK5CYII="), "C:\\CONNECTION_CONNECT.png"));

        String text = "";

        ConvertUtils.bytesToFile(Base64.decodeBase64(text), "test.doc");

    }
}
