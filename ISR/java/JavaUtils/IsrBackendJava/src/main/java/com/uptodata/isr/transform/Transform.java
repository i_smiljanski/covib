package com.uptodata.isr.transform;

import com.uptodata.isr.utils.ConvertUtils;
import com.uptodata.isr.utils.xml.dom.XmlHandler;
import oracle.jdbc.OracleCallableStatement;
import oracle.jdbc.OracleResultSet;
import org.w3c.dom.NodeList;

import javax.xml.transform.*;
import javax.xml.transform.stream.StreamResult;
import javax.xml.transform.stream.StreamSource;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.Reader;
import java.io.Writer;
import java.sql.Clob;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;

@Deprecated
public class Transform {

    public static Clob transform(Clob clXML, Clob clStyleSheet, String xml) throws Exception {

        // use default connection
        Connection conn = DriverManager.getConnection("jdbc:default:connection");

        // create a temporary CLOB
        Clob clResult = ConvertUtils.getClob(conn, null);
        transform(clXML.getCharacterStream(), clStyleSheet.getCharacterStream(), xml, clResult.setCharacterStream(1));
        return clResult;
    }

    public static Clob transform(Clob clXML, Clob clStyleSheet) throws Exception {

        return transform(clXML, clStyleSheet, null);
    }

    private static void transform(Reader xmlReader, Reader styleSheetReader, String xml, Writer result) throws Exception {
        Result transResult = new StreamResult(result);

        Source xmlSource = new StreamSource(xmlReader);
        Source xsltSource = new StreamSource(styleSheetReader);

//        TransformerFactory tf = new net.sf.saxon.TransformerFactoryImpl();
        System.setProperty("javax.xml.transform.TransformerFactory", "net.sf.saxon.TransformerFactoryImpl");

        // create the transformer factory & transformer instance
        TransformerFactory tf = TransformerFactory.newInstance();
        tf.setURIResolver(new MyURIResolver());
        Transformer t = tf.newTransformer(xsltSource);

        if (xml != null && xml.length() > 0) {
            XmlHandler handler = new XmlHandler(xml.getBytes(ConvertUtils.encoding));
            for (int i = 1; i <= ((NodeList) (handler.findXpath("/*/*", "NODESET"))).getLength(); i++) {
                String param = handler.findXpath("/*/*[" + i + "]/@name", "STRING").toString();
                String value = handler.findXpath("/*/*[" + i + "]", "STRING").toString();
                t.setParameter(param, value);
            }
        }

        // execute transformation & fill result target object
        t.transform(xmlSource, transResult);
    }

    public static void main(String[] args) {
        try {
            Reader xmlReader = new FileReader(System.getProperty("user.dir") + "\\IsrBackendJava\\ISR$STD$ERRORS.xml");
            Reader styleSheetReader = new FileReader(System.getProperty("user.dir") + "\\IsrBackendJava\\ISR$STD$ERRORS.XSL");
            Writer result = new FileWriter(System.getProperty("user.dir") + "\\IsrBackendJava\\result.xml");

            transform(xmlReader, styleSheetReader, null, result);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}

@Deprecated
class MyURIResolver implements URIResolver {

    MyURIResolver() {
    }

    StreamSource getSubStylesheet(String name) throws SQLException {
        System.out.println("name = " + name);
        String statement = "select stb$transform.getIncludeStylesheet(?) from dual";
        Connection conn = DriverManager.getConnection("jdbc:default:connection");
        OracleCallableStatement cs = (OracleCallableStatement) conn.prepareCall(statement);
        cs.setString(1, name);
        OracleResultSet rs = (OracleResultSet) cs.executeQuery();
        StreamSource retSrc = rs.next() ? new StreamSource(rs.getCLOB(1).characterStreamValue()) : null;
        rs.close();
        cs.close();
        return retSrc;
    }

    public Source resolve(String href, String base) throws TransformerException {
        Source xsltSource = null;
        try {
            xsltSource = getSubStylesheet(href);
            xsltSource.setSystemId(href);
        } catch (SQLException e) {
        }
        return xsltSource;
    }
}
