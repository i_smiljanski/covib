package com.uptodata.isr.dbstored;

import java.io.InputStream;
import java.sql.Blob;
import java.sql.Clob;
import java.util.zip.CRC32;


public class GetChecksum {

    public static long printChecksum(Blob blob) {

        CRC32 checkSum = new CRC32();
        int i;
        try {
            InputStream is = blob.getBinaryStream();
            while ((i = is.read()) != -1) {
                checkSum.update(i);
            }
            return checkSum.getValue();
        } catch (Exception ex) {
            return -1;
        }
    }

    public static long printChecksum(Clob clob) {

        CRC32 checkSum = new CRC32();
        int i;
        try {
            InputStream is = clob.getAsciiStream();
            while ((i = is.read()) != -1) {
                checkSum.update(i);
            }
            return checkSum.getValue();
        } catch (Exception ex) {
            ex.printStackTrace();
            return -1;
        }
    }

    public static void main(String[] args) {

        new GetChecksum();
    }


}