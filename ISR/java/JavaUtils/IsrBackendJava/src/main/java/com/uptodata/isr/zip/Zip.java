package com.uptodata.isr.zip;

import com.uptodata.isr.db.DbConvertUtils;
import com.uptodata.isr.db.trace.DbLogging;
import com.uptodata.isr.utils.ConvertUtils;

import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.sql.*;
import java.util.Map;
import java.util.zip.ZipEntry;
import java.util.zip.ZipInputStream;
import java.util.zip.ZipOutputStream;

public class Zip {

    // ZIP - Handling

    public static byte[] zip(byte[] zipEntry, String zipEntryName) throws IOException {
        DbLogging log = new DbLogging();
        log.stat("begin", zipEntryName);
        ByteArrayOutputStream bos = new ByteArrayOutputStream();

        ZipOutputStream zipfile = new ZipOutputStream(bos);
        ZipEntry zipentry = new ZipEntry(zipEntryName);
        zipfile.putNextEntry(zipentry);
        zipfile.write(zipEntry);
        zipfile.closeEntry();
        zipfile.close();
        byte[] bytes = bos.toByteArray();
        log.stat("end", bytes.length);
        return bytes;
    }

    public static byte[] unzip(byte[] zipFile) throws IOException {
        DbLogging log = new DbLogging();
        log.stat("begin", "begin");
        ByteArrayOutputStream bos = new ByteArrayOutputStream();
        InputStream fileStream = new ByteArrayInputStream(zipFile);
        ZipInputStream zipfile = new ZipInputStream(fileStream);
        ZipEntry ze = zipfile.getNextEntry();
        if (ze != null) {
            for (int c = zipfile.read(); c != -1; c = zipfile.read()) {
                bos.write(c);
            }
            zipfile.closeEntry();
            zipfile.close();
            return bos.toByteArray();
        }
        log.stat("end", "end");
        return zipFile;
    }

    // Wrapper

    public static Blob zip(int jobid) throws Exception {
        DbLogging log = new DbLogging();
        log.stat("begin", "begin");
        Connection connection = DriverManager.getConnection("jdbc:default:connection");

        ByteArrayOutputStream bos = new ByteArrayOutputStream();
        ZipOutputStream zipfile = new ZipOutputStream(bos);

        String sqlStmt = "SELECT filename, textfile, binaryfile " +
                "FROM stb$jobtrail " +
                "WHERE jobid = ? " +
                "AND documenttype IN ('BLOB','CLOB','SCRIPT')";
        log.debug("sqlStmt", sqlStmt);

        CallableStatement cs = connection.prepareCall(sqlStmt);
        cs.setInt(1, jobid);
        ResultSet rs = cs.executeQuery();

        while (rs.next()) {
            log.debug("filename", rs.getString(1));
            ZipEntry zipentry = new ZipEntry(rs.getString(1));
            zipfile.putNextEntry(zipentry);
            Blob bl = rs.getBlob(3);
            byte[] bytes;
            if (bl != null && bl.length() > 0) {
                log.debug("blob", "blob");
                bytes = ConvertUtils.getBytes((Blob) bl);
            } else {
                log.debug("clob", "clob");
                bytes = ConvertUtils.getBytes(rs.getClob(2));
            }
            log.debug("bytes", bytes.length);
            zipfile.write(bytes);
            zipfile.closeEntry();
        }
        rs.close();
        cs.close();

        log.debug("zip file size", bos.size());

        zipfile.close();
        Blob bl = ConvertUtils.getBlob(connection, bos.toByteArray());
        log.stat("end", bl.length());
        return bl;
    }

    public static void unzip(int jobid, Blob blobZipFile) throws Exception {
        DbLogging log = new DbLogging();
        log.stat("begin", "begin");
        Connection connection = DriverManager.getConnection("jdbc:default:connection");

        CallableStatement cs = connection.prepareCall("INSERT INTO STB$JOBTRAIL " +
                "(ENTRYID, JOBID, DOCUMENTTYPE, FILENAME, TIMESTAMP, TEXTFILE, BINARYFILE) " +
                "(SELECT STB$JOBTRAIL$SEQ.NEXTVAL, ?, ?, ?, SYSDATE, ?, ? FROM DUAL)");

        ZipInputStream zipfile = new ZipInputStream(new ByteArrayInputStream(ConvertUtils.getBytes(blobZipFile)));

        ZipEntry entry;
        while ((entry = zipfile.getNextEntry()) != null) {
            ByteArrayOutputStream bos = new ByteArrayOutputStream();
            for (int c = zipfile.read(); c != -1; c = zipfile.read())
                bos.write(c);
            cs.setInt(1, jobid);
            cs.setString(2, (entry.getName().toUpperCase().indexOf("BLOB") > 0) ? "BLOB" : "CLOB");
            cs.setString(3, entry.getName());
            if (entry.getName().toUpperCase().indexOf("BLOB") > 0) {
                cs.setClob(4, ConvertUtils.getClob(connection, null));
                cs.setBlob(5, ConvertUtils.getBlob(connection, bos.toByteArray()));
            } else {
                cs.setClob(4, ConvertUtils.getClob(connection, bos.toByteArray()));
                cs.setBlob(5, ConvertUtils.getBlob(connection, null));
            }
            cs.executeQuery();
        }
        zipfile.close();

        cs.close();

        connection.commit();
        log.stat("end", "end");
    }

    public static Blob zip(Blob blobZipEntry, String zipEntryName) throws Exception {
        DbLogging log = new DbLogging();
        log.stat("begin", "begin");
        Connection connection = DriverManager.getConnection("jdbc:default:connection");
        byte[] bytes = ConvertUtils.getBytes(blobZipEntry);
        byte[] zipFile = zip(bytes, zipEntryName);
        Blob bl = ConvertUtils.getBlob(connection, zipFile);
        log.stat("end", "end");
        return bl;
    }

    public static Blob unzip(Blob blobZipFile) throws Exception {
        DbLogging log = new DbLogging();
        log.stat("begin", "begin");
        Connection connection = DriverManager.getConnection("jdbc:default:connection");

        byte[] zipFileEntry = unzip(ConvertUtils.getBytes(blobZipFile));
        Blob bl = ConvertUtils.getBlob(connection, zipFileEntry);
        log.stat("end", "end");
        return bl;
    }

    public static Blob zip(Array filesToZip) throws Exception {
        DbLogging log = new DbLogging();
        log.stat("begin", "begin");
        Blob bl;
        Connection connection = DriverManager.getConnection("jdbc:default:connection");
        Map<String, Object> filesMap = DbConvertUtils.anyDataArrayToStringMap(filesToZip);

        log.debug("elements", filesMap.size());

        ByteArrayOutputStream bos = new ByteArrayOutputStream();
        ZipOutputStream resultFile = new ZipOutputStream(bos);

        for (String name : filesMap.keySet()) {
            Object value = filesMap.get(name);
            log.debug("name, value", name + ", " + value);
            ZipEntry zipEntry = new ZipEntry(name);
            resultFile.putNextEntry(zipEntry);
            if (value instanceof byte[]) {
                resultFile.write((byte[]) value);
            }
            resultFile.closeEntry();
        }

        resultFile.close();
        bl = ConvertUtils.getBlob(connection, bos.toByteArray());
        log.stat("end", "end");
        return bl;
    }

    // Main

    public static void main(String[] args) throws IOException {

        byte[] bytes = ConvertUtils.fileToBytes(System.getProperty("user.dir") + "\\IsrBackendJava\\input.pdf");
        byte[] myUnZippedFile = unzip(bytes);
        ConvertUtils.bytesToFile(myUnZippedFile, System.getProperty("user.dir") + "\\IsrBackendJava\\result.pdf");

    }
}