package com.uptodata.isr.mail;

import com.uptodata.isr.db.connection.ConnectionUtil;
import com.uptodata.isr.db.trace.DbLogging;
import com.uptodata.isr.utils.ConvertUtils;
import com.uptodata.isr.utils.exceptions.MessageStackException;

import javax.activation.DataHandler;
import javax.activation.FileDataSource;
import javax.mail.*;
import javax.mail.internet.InternetAddress;
import javax.mail.internet.MimeBodyPart;
import javax.mail.internet.MimeMessage;
import javax.mail.internet.MimeMultipart;
import java.io.IOException;
import java.sql.*;
import java.util.ArrayList;
import java.util.List;
import java.util.Properties;

public class SendMail {

    public static Clob sendMail(String SMTP_HOST_NAME, String SMTP_PORT_NAME, String SMTP_AUTH_USER, String SMTP_AUTH_PWD,
                                String to, String cc, String subject, String message, Array arrayList) throws IOException, SQLException {
        DbLogging log = new DbLogging();
        log.stat("begin", "begin");

        try {

            log.debug("ARRAYToArray", SMTP_HOST_NAME + "   " + SMTP_PORT_NAME + "   " + SMTP_AUTH_USER + "   " + SMTP_AUTH_PWD);
            List<FileRec> fileList = null;
            if (arrayList != null) {
                fileList = ARRAYToArray(arrayList);
            }
            sendAttachmentMail(SMTP_HOST_NAME, SMTP_PORT_NAME, SMTP_AUTH_USER, SMTP_AUTH_PWD, to, cc, subject, message, fileList);

        } catch (MessageStackException e) {
            return onFailMethod(e);
        } catch (Exception e) {
            MessageStackException messageStackException = new MessageStackException(e, MessageStackException.SEVERITY_CRITICAL,
                    "error " + e, MessageStackException.getCurrentMethod());
            return onFailMethod(messageStackException);
        }
        log.stat("end", "end");
        return null;
    }


    public static void sendAttachmentMail(String SMTP_HOST_NAME, String SMTP_PORT_NAME, String SMTP_AUTH_USER,
                                          String SMTP_AUTH_PWD, String to, String cc, String subject, String message,
                                          List<FileRec> fileRecs) throws MessagingException {
        DbLogging log = new DbLogging();
        log.stat("begin", "begin");
        Message mail = setMessage(SMTP_HOST_NAME, SMTP_PORT_NAME, SMTP_AUTH_USER, SMTP_AUTH_PWD, to, cc, subject);
        Multipart multipart = new MimeMultipart();

        // text
        if (message != null) {
            BodyPart messageBodyPartText = new MimeBodyPart();
            messageBodyPartText.setText(message);
            multipart.addBodyPart(messageBodyPartText);
        }
        if (fileRecs != null && fileRecs.size() > 0) {
            // attachment(s)
            for (FileRec fileRec : fileRecs) {
                BodyPart messageBodyPartAttachment = new MimeBodyPart();

                messageBodyPartAttachment.setDataHandler(fileRec.dataHandler);
                messageBodyPartAttachment.setFileName(fileRec.fileName);
                messageBodyPartAttachment.setHeader("Content-Type", fileRec.mimeType);
                log.debug("messageBodyPartAttachment", messageBodyPartAttachment.getFileName());
                multipart.addBodyPart(messageBodyPartAttachment);
            }
        }
        log.debug("multipart.getCount()", multipart.getCount());
        mail.setContent(multipart);

        // send mail
        Transport.send(mail);


        log.stat("end", "mail sent to " + to);
    }


    private static Clob onFailMethod(MessageStackException e) throws IOException, SQLException {
        DbLogging log = new DbLogging();
        log.stat("begin", "begin");
        Connection conn = ConnectionUtil.getDefaultConnection();

        byte[] exBytes = e.toXml().xmlToString().getBytes();
        Clob cl = ConvertUtils.getClob(conn, exBytes);
        log.stat("end", "end");
        return cl;
    }


    private static List<FileRec> ARRAYToArray(Array file_List) throws Exception {
        DbLogging log = new DbLogging();
        log.stat("begin", "begin");
        Blob blob;

        Object[] elements = (Object[]) file_List.getArray();
        List<FileRec> fileRecs = new ArrayList<>();

        for (Object element : elements) {
            FileRec rec;
            Struct s = (Struct) element;
            Object[] data = s.getAttributes();
            rec = new FileRec();
            rec.fileName = (String) data[0];
            rec.mimeType = (String) data[1];
            blob = (Blob) data[2];
            log.debug(rec.fileName, blob.length());
            rec.dataHandler = new DataHandler(new BlobDataSource(blob, rec.fileName));
            fileRecs.add(rec);
        }
        log.stat("end", "end");
        return fileRecs;
    }


    public static void check(String SMTP_HOST_NAME, String SMTP_PORT_NAME, String SMTP_AUTH_USER, String SMTP_AUTH_PWD) {
        try {

            //create properties field
            Properties props = new Properties();
            props.put("mail.transport.protocol", "smtp");
            props.put("mail.smtp.host", SMTP_HOST_NAME);
            props.put("mail.smtp.port", SMTP_PORT_NAME);
            props.put("mail.pop3.starttls.enable", "true");
            props.put("mail.smtp.auth", "true");
            final String SMTP_AUTH_USER1 = SMTP_AUTH_USER;
            final String SMTP_AUTH_PWD1 = SMTP_AUTH_PWD;
            class SMTPAuthenticator extends javax.mail.Authenticator {
                @Override
                public PasswordAuthentication getPasswordAuthentication() {
                    String username = SMTP_AUTH_USER1;
                    String password = SMTP_AUTH_PWD1;
                    return new PasswordAuthentication(username, password);
                }
            }
            System.out.println("props = " + props);
            Authenticator auth = new SMTPAuthenticator();
            Session emailSession = Session.getInstance(props, auth);

            //create the POP3 store object and connect with the pop server
            Store store = emailSession.getStore("imaps");

            store.connect(SMTP_HOST_NAME, SMTP_AUTH_USER, SMTP_AUTH_PWD);

            //create the folder object and open it
            Folder emailFolder = store.getFolder("INBOX");
            emailFolder.open(Folder.READ_ONLY);

            // retrieve the messages from the folder in an array and print it
            Message[] messages = emailFolder.getMessages();
            System.out.println("messages.length---" + messages.length);

            for (int i = 0, n = messages.length; i < n; i++) {
                Message message = messages[i];
                System.out.println("---------------------------------");
                System.out.println("Email Number " + (i + 1));
                System.out.println("Subject: " + message.getSubject());
                System.out.println("From: " + message.getFrom()[0]);
                System.out.println("Text: " + message.getContent().toString());

            }

            //close the store and folder objects
            emailFolder.close(false);
            store.close();

        } catch (NoSuchProviderException e) {
            e.printStackTrace();
        } catch (MessagingException e) {
            e.printStackTrace();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private static Message setMessage(String SMTP_HOST_NAME, String SMTP_PORT_NAME, String SMTP_AUTH_USER, String SMTP_AUTH_PWD, String to, String cc, String subject) {
        DbLogging log = new DbLogging();

        String from = SMTP_AUTH_USER;
        final String SMTP_AUTH_USER1 = SMTP_AUTH_USER;
        final String SMTP_AUTH_PWD1 = SMTP_AUTH_PWD;

        log.stat("begin", "SMTP_AUTH_USER " + SMTP_AUTH_USER);

        // set host
        Properties props = new Properties();
        props.put("mail.transport.protocol", "smtp");
        props.put("mail.smtp.host", SMTP_HOST_NAME);
        props.put("mail.smtp.port", SMTP_PORT_NAME);

        Session session;

        if (SMTP_AUTH_PWD == null || SMTP_AUTH_PWD.length() == 0) {
            session = Session.getInstance(props);
        } else {
            props.put("mail.smtp.auth", "true");

            // create session
            class SMTPAuthenticator extends javax.mail.Authenticator {
                @Override
                public PasswordAuthentication getPasswordAuthentication() {
                    String username = SMTP_AUTH_USER1;
                    String password = SMTP_AUTH_PWD1;
                    return new PasswordAuthentication(username, password);
                }
            }
            System.out.println("props = " + props);
            Authenticator auth = new SMTPAuthenticator();
            session = Session.getInstance(props, auth);
        }

        session.setDebug(true); // Verbose!


        // create mail
        Message mail = new MimeMessage(session);
        //  try {
        // set from
        if (from != null) try {
            mail.addFrom(InternetAddress.parse(from));
        } catch (MessagingException e) {
            e.printStackTrace();
        }


        // set to
        if (to != null) try {
            mail.setRecipients(Message.RecipientType.TO, InternetAddress.parse(to));
        } catch (MessagingException e) {
            e.printStackTrace();
        }

        // set cc
        if (cc != null) try {
            mail.setRecipients(Message.RecipientType.CC, InternetAddress.parse(cc));
        } catch (MessagingException e) {
            e.printStackTrace();
        }

        // set subject
        if (subject != null) try {
            mail.setSubject(subject);
        } catch (MessagingException e) {
            e.printStackTrace();
        }
        log.stat("end", "mail sent to " + to);
        return mail;
    }


    public static void main(String[] args) throws MessagingException {


        FileRec fileRec1 = new FileRec();
        FileRec fileRec2 = new FileRec();
        fileRec1.fileName = "1.docx";
        fileRec2.fileName = "11.docx";
        fileRec1.dataHandler = new DataHandler(new FileDataSource(fileRec1.fileName));
        fileRec2.dataHandler = new DataHandler(new FileDataSource(fileRec2.fileName));
        fileRec1.mimeType = "text/plain";
        fileRec2.mimeType = "text/plain";
        List<FileRec> arrayList = new ArrayList<>();
        arrayList.add(fileRec1);
        arrayList.add(fileRec2);

        sendAttachmentMail("stargate.utddomain.utd", "25", "service@uptodata.de", "",
                "inna.smiljanski@uptodata.com", "", "iStudyReporter Mail: 110587 -> " + "110587_15-Oct-2013-6820 1.0", "test mail", null);

//        check("stargate.utddomain.utd", "25", "service@uptodata.de", "");

    }

    static class FileRec {
        String fileName;
        String mimeType;
        DataHandler dataHandler;

        public FileRec(String fileName, String mimeType, DataHandler dataHandler) {
            this.fileName = fileName;
            this.mimeType = mimeType;
            this.dataHandler = dataHandler;
        }

        FileRec() {
            this.fileName = "";
            this.mimeType = "";
            this.dataHandler = null;
        }


    }


}
