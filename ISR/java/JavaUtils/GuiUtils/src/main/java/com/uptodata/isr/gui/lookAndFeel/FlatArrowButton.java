package com.uptodata.isr.gui.lookAndFeel;


import com.uptodata.isr.gui.util.Colors;

import javax.swing.*;
import javax.swing.plaf.basic.BasicArrowButton;
import java.awt.*;


class FlatArrowButton extends BasicArrowButton {

    public FlatArrowButton(int direction, Color background, ButtonColor color) {

        super(direction, background, color, Colors.getUIControlText(), background);

    }

    // don't paint any offset
    public void paintTriangle(Graphics g, int x, int y, int size, int direction,
                              boolean isEnabled) {

        boolean isPressed = getModel().isPressed();
        // int off = isPressed? 0:1;
        int off = isPressed ? 0 : 0;
        g.translate(off, off);
        super.paintTriangle(g, x, y, size, direction, isEnabled);
        g.translate(-off, -off);

    }

    // change background on press+armed
    public Color getBackground() {

        if (LFConst.FULL_FLAT) {

            ButtonModel m = getModel();
            if (m.isArmed() && m.isPressed()) return Colors.getUIControlShadow();

        }

        return super.getBackground();

    }

}