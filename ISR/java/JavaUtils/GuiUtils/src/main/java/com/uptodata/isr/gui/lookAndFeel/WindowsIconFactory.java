package com.uptodata.isr.gui.lookAndFeel;

import javax.swing.*;
import javax.swing.plaf.UIResource;
import java.awt.*;
import java.io.Serializable;


/**
 * From Suns Windows L&F
 *
 * @author PeterBuettner.de
 */
public class WindowsIconFactory implements Serializable {

    private static Icon checkBoxIcon;

    private static Icon radioButtonIcon;

    public static Icon getCheckBoxIcon() {

        if (checkBoxIcon == null) {
            checkBoxIcon = new CheckBoxIcon();
        }

        return checkBoxIcon;

    }

    public static Icon getRadioButtonIcon() {

        if (radioButtonIcon == null) {
            radioButtonIcon = new RadioButtonIcon();
        }

        return radioButtonIcon;

    }

    private static class CheckBoxIcon implements Icon, Serializable {

        final static int csize = 13;

        public void paintIcon(Component c, Graphics g, int x, int y) {

            JCheckBox cb = (JCheckBox) c;
            ButtonModel model = cb.getModel();

            {
                // outer bevel
                if (!cb.isBorderPaintedFlat()) {
                    // Outer top/left
                    g.setColor(UIManager.getColor("CheckBox.shadow"));
                    g.drawLine(x, y, x + 11, y);
                    g.drawLine(x, y + 1, x, y + 11);

                    // Outer bottom/right
                    g.setColor(UIManager.getColor("CheckBox.highlight"));
                    g.drawLine(x + 12, y, x + 12, y + 12);
                    g.drawLine(x, y + 12, x + 11, y + 12);

                    // Inner top.left
                    //g.setColor(UIManager.getColor("CheckBox.darkShadow"));
                    // PEB
                    g.setColor(UIManager.getColor("CheckBox.background"));

                    g.drawLine(x + 1, y + 1, x + 10, y + 1);
                    g.drawLine(x + 1, y + 2, x + 1, y + 10);

                    // Inner bottom/right
                    // g.setColor(UIManager.getColor("CheckBox.light"));
                    // PEB
                    g.setColor(UIManager.getColor("CheckBox.background"));

                    g.drawLine(x + 1, y + 11, x + 11, y + 11);
                    g.drawLine(x + 11, y + 1, x + 11, y + 10);

                    // inside box
                    if ((model.isPressed() && model.isArmed()) || !model.isEnabled()) {

                        g.setColor(UIManager.getColor("CheckBox.background"));

                    } else {

                        g.setColor(UIManager.getColor("CheckBox.interiorBackground"));

                    }

                    g.fillRect(x + 2, y + 2, csize - 4, csize - 4);
                    g.fillRect(x + 1, y + 1, csize - 2, csize - 2); // PEB

                } else {

                    g.setColor(UIManager.getColor("CheckBox.shadow"));
                    g.drawRect(x + 1, y + 1, csize - 3, csize - 3);

                    if ((model.isPressed() && model.isArmed()) || !model.isEnabled()) {

                        g.setColor(UIManager.getColor("CheckBox.background"));

                    } else {

                        g.setColor(UIManager.getColor("CheckBox.interiorBackground"));

                    }

                    g.fillRect(x + 2, y + 2, csize - 4, csize - 4);

                }

                if (model.isEnabled()) {

                    g.setColor(UIManager.getColor("CheckBox.darkShadow"));

                } else {

                    g.setColor(UIManager.getColor("CheckBox.shadow"));

                }

                // paint check
                if (model.isSelected()) {

                    g.drawLine(x + 9, y + 3, x + 9, y + 3);
                    g.drawLine(x + 8, y + 4, x + 9, y + 4);
                    g.drawLine(x + 7, y + 5, x + 9, y + 5);
                    g.drawLine(x + 6, y + 6, x + 8, y + 6);
                    g.drawLine(x + 3, y + 7, x + 7, y + 7);
                    g.drawLine(x + 4, y + 8, x + 6, y + 8);
                    g.drawLine(x + 5, y + 9, x + 5, y + 9);
                    g.drawLine(x + 3, y + 5, x + 3, y + 5);
                    g.drawLine(x + 3, y + 6, x + 4, y + 6);

                }

            }

        }

        public int getIconWidth() {

            return csize;

        }

        public int getIconHeight() {

            return csize;

        }

    }

    private static class RadioButtonIcon implements Icon, UIResource, Serializable {

        public void paintIcon(Component c, Graphics g, int x, int y) {

            AbstractButton b = (AbstractButton) c;
            ButtonModel model = b.getModel();

            {
                // fill interior
                if ((model.isPressed() && model.isArmed()) || !model.isEnabled()) {

                    g.setColor(UIManager.getColor("RadioButton.background"));

                } else {

                    g.setColor(UIManager.getColor("RadioButton.interiorBackground"));

                }

                g.fillRect(x + 2, y + 2, 8, 8);

                // outter left arc
                g.setColor(UIManager.getColor("RadioButton.shadow"));
                g.drawLine(x + 4, y + 0, x + 7, y + 0);
                g.drawLine(x + 2, y + 1, x + 3, y + 1);
                g.drawLine(x + 8, y + 1, x + 9, y + 1);
                g.drawLine(x + 1, y + 2, x + 1, y + 3);
                g.drawLine(x + 0, y + 4, x + 0, y + 7);
                g.drawLine(x + 1, y + 8, x + 1, y + 9);

                // outter right arc
                g.setColor(UIManager.getColor("RadioButton.highlight"));
                g.drawLine(x + 2, y + 10, x + 3, y + 10);
                g.drawLine(x + 4, y + 11, x + 7, y + 11);
                g.drawLine(x + 8, y + 10, x + 9, y + 10);
                g.drawLine(x + 10, y + 9, x + 10, y + 8);
                g.drawLine(x + 11, y + 7, x + 11, y + 4);
                g.drawLine(x + 10, y + 3, x + 10, y + 2);

                // inner left arc
                g.setColor(UIManager.getColor("RadioButton.darkShadow"));
                g.drawLine(x + 4, y + 1, x + 7, y + 1);
                g.drawLine(x + 2, y + 2, x + 3, y + 2);
                g.drawLine(x + 8, y + 2, x + 9, y + 2);
                g.drawLine(x + 2, y + 3, x + 2, y + 3);
                g.drawLine(x + 1, y + 4, x + 1, y + 7);
                g.drawLine(x + 2, y + 8, x + 2, y + 8);

                // inner right arc
                g.setColor(UIManager.getColor("RadioButton.light"));
                g.drawLine(x + 2, y + 9, x + 3, y + 9);
                g.drawLine(x + 4, y + 10, x + 7, y + 10);
                g.drawLine(x + 8, y + 9, x + 9, y + 9);
                g.drawLine(x + 9, y + 8, x + 9, y + 8);
                g.drawLine(x + 10, y + 7, x + 10, y + 4);
                g.drawLine(x + 9, y + 3, x + 9, y + 3);

                // indicate whether selected or not
                if (model.isSelected()) {

                    g.setColor(UIManager.getColor("RadioButton.darkShadow"));
                    g.fillRect(x + 4, y + 5, 4, 2);
                    g.fillRect(x + 5, y + 4, 2, 4);

                }

            }

        }

        public int getIconWidth() {

            return 13;

        }

        public int getIconHeight() {

            return 13;

        }

    } // end class RadioButtonIcon

}

