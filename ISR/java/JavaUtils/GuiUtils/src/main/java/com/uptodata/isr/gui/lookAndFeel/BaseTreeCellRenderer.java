package com.uptodata.isr.gui.lookAndFeel;

import com.uptodata.isr.gui.util.Colors;

import javax.swing.*;
import javax.swing.plaf.ColorUIResource;
import javax.swing.plaf.FontUIResource;
import javax.swing.tree.TreeCellRenderer;
import java.awt.*;
import java.util.Objects;


/**
 * Copy and modified of the javax.swing.DefaultTreeCellRenderer changes: paint,
 * getLabelStart, paint Focus, getLabelStart
 * <p>
 * see there for info & doku (i stripped comments from here)
 *
 * @author PeterBuettner.de
 */
public class BaseTreeCellRenderer extends JLabel implements TreeCellRenderer {

    /**
     * Last tree the renderer was painted in.
     */
    private JTree tree;

    /**
     * Is the value currently selected.
     */
    protected boolean selected;

    /**
     * True if has focus.
     */
    protected boolean hasFocus;

    /**
     * True if draws focus border around icon as well.
     */
    protected boolean drawsFocusBorderAroundIcon;

    // Icons
    /**
     * Icon used to show non-leaf nodes that aren't expanded.
     */
    transient protected Icon closedIcon;

    /**
     * Icon used to show leaf nodes.
     */
    transient protected Icon leafIcon;

    /**
     * Icon used to show non-leaf nodes that are expanded.
     */
    transient protected Icon openIcon;

    // Colors
    /**
     * Color to use for the foreground for selected nodes.
     */
    protected Color textSelectionColor;

    /**
     * Color to use for the foreground for non-selected nodes.
     */
    protected Color textNonSelectionColor;

    /**
     * Color to use for the background when a node is selected.
     */
    protected Color backgroundSelectionColor;

    /**
     * Color to use for the background when the node isn't selected.
     */
    protected Color backgroundNonSelectionColor;

    /**
     * Color to use for the focus indicator when the node has focus.
     */
    protected Color borderSelectionColor;

    /**
     * Returns a new instance of DefaultTreeCellRenderer. Alignment is set to
     * left aligned. Icons and text color are determined from the UIManager.
     */
    public BaseTreeCellRenderer() {

        setHorizontalAlignment(JLabel.LEFT);
        setIconTextGap(6);

        setLeafIcon(UIManager.getIcon("Tree.leafIcon"));
        setClosedIcon(UIManager.getIcon("Tree.closedIcon"));
        setOpenIcon(UIManager.getIcon("Tree.openIcon"));

        setTextSelectionColor(UIManager.getColor("Tree.selectionForeground"));
        setTextNonSelectionColor(UIManager.getColor("Tree.textForeground"));
        setBackgroundSelectionColor(UIManager.getColor("Tree.selectionBackground"));
        setBackgroundNonSelectionColor(UIManager.getColor("Tree.textBackground"));
        setBorderSelectionColor(UIManager.getColor("Tree.selectionBorderColor"));
        drawsFocusBorderAroundIcon = Boolean.TRUE.equals(UIManager
                .get("Tree.drawsFocusBorderAroundIcon"));

    }

    public Icon getDefaultOpenIcon() {

        return UIManager.getIcon("Tree.openIcon");
    }

    public Icon getDefaultClosedIcon() {

        return UIManager.getIcon("Tree.closedIcon");
    }

    public Icon getDefaultLeafIcon() {

        return UIManager.getIcon("Tree.leafIcon");
    }

    public void setOpenIcon(Icon newIcon) {

        openIcon = newIcon;
    }

    public Icon getOpenIcon() {

        return openIcon;
    }

    public void setClosedIcon(Icon newIcon) {

        closedIcon = newIcon;
    }

    public Icon getClosedIcon() {

        return closedIcon;
    }

    public void setLeafIcon(Icon newIcon) {

        leafIcon = newIcon;
    }

    public Icon getLeafIcon() {

        return leafIcon;
    }

    public void setTextSelectionColor(Color newColor) {

        textSelectionColor = newColor;
    }

    public Color getTextSelectionColor() {

        return textSelectionColor;
    }

    public void setTextNonSelectionColor(Color newColor) {

        textNonSelectionColor = newColor;
    }

    public Color getTextNonSelectionColor() {

        return textNonSelectionColor;
    }

    public void setBackgroundSelectionColor(Color newColor) {

        backgroundSelectionColor = newColor;
    }

    public Color getBackgroundSelectionColor() {

        return backgroundSelectionColor;
    }

    public void setBackgroundNonSelectionColor(Color newColor) {

        backgroundNonSelectionColor = newColor;
    }

    public Color getBackgroundNonSelectionColor() {

        return backgroundNonSelectionColor;
    }

    public void setBorderSelectionColor(Color newColor) {

        borderSelectionColor = newColor;
    }

    public Color getBorderSelectionColor() {

        return borderSelectionColor;
    }

    public void setFont(Font font) {

        super.setFont((font instanceof FontUIResource) ? null : font);
    }

    /**
     * Gets the font of this component.
     *
     * @return this component's font; if a font has not been set for this
     * component, the font of its parent is returned
     */
    public Font getFont() {

        Font font = super.getFont();
        if (font == null && tree != null) {
            // Strive to return a non-null value, otherwise the html support
            // will typically pick up the wrong font in certain situations.
            font = tree.getFont();
        }

        return font;

    }

    /**
     * Subclassed to map <code>ColorUIResource</code> s to null. If
     * <code>color</code> is null, or a <code>ColorUIResource</code>, this
     * has the effect of letting the background color of the JTree show through.
     * On the other hand, if <code>color</code> is non-null, and not a
     * <code>ColorUIResource</code>, the background becomes
     * <code>color</code>.
     */
    public void setBackground(Color color) {

        super.setBackground((color instanceof ColorUIResource) ? null : color);

    }

    /**
     * Configures the renderer based on the passed in components. The value is
     * set from messaging the tree with <code>convertValueToText</code>,
     * which ultimately invokes <code>toString</code> on <code>value</code>.
     * The foreground color is set based on the selection and the icon is set
     * based on on leaf and expanded.
     */
    public Component getTreeCellRendererComponent(JTree tree, Object value, boolean sel,
                                                  boolean expanded, boolean leaf, int row,
                                                  boolean hasFocus) {

        String stringValue = tree.convertValueToText(value, sel, expanded, leaf, row,
                hasFocus);

        this.tree = tree;
        this.hasFocus = hasFocus;
        setText(stringValue);

        if (sel) {

            if (isPermFocusAndAppActive(tree))
                setForeground(getTextSelectionColor());
                //			if(tree.isFocusOwner())setForeground(getTextSelectionColor());

            else
                setForeground(Colors.getItemHiliteInactiveText());

        } else

            setForeground(getTextNonSelectionColor());

        // There needs to be a way to specify disabled icons.
        if (!tree.isEnabled()) {

            setEnabled(false);
            if (leaf)
                setDisabledIcon(getLeafIcon());
            else if (expanded)
                setDisabledIcon(getOpenIcon());
            else
                setDisabledIcon(getClosedIcon());

        } else {

            setEnabled(true);
            if (leaf)
                setIcon(getLeafIcon());
            else if (expanded)
                setIcon(getOpenIcon());
            else
                setIcon(getClosedIcon());

        }

        setComponentOrientation(tree.getComponentOrientation());
        selected = sel;
        return this;

    }

    /**
     * Paints the value. The background is filled based on selected.
     */
    public void paint(Graphics g) {

        Color bColor;
        if (selected) {

            if (isPermFocusAndAppActive(tree))
                bColor = getBackgroundSelectionColor();
            else
                bColor = Colors.getItemHiliteInactiveBack();

        } else {

            bColor = getBackgroundNonSelectionColor();
            if (bColor == null) bColor = getBackground();

        }
        // .................................

        int imageOffset = -1;
        if (bColor != null) {

            imageOffset = getLabelStart();
            g.setColor(bColor);
            int x = getComponentOrientation().isLeftToRight() ? imageOffset : 0;
            g.fillRect(x, 0, getWidth() - imageOffset, getHeight());

        }

        if (hasFocus) {

            if (drawsFocusBorderAroundIcon)
                imageOffset = 0;
            else if (imageOffset == -1) imageOffset = getLabelStart();

            int x = getComponentOrientation().isLeftToRight() ? imageOffset : 0;
            paintFocus(g, x, 0, getWidth() - imageOffset, getHeight());

        }

        super.paint(g);

    }// -----------------------------------------------------------------------------

    private int getLabelStart() {

        Icon icon = getIcon();
        if (icon == null || getText() == null) return 0;
        return icon.getIconWidth() + Math.max(0, getIconTextGap() - 1) - 2 /*-2:PEB*/;

    }// -----------------------------------------------------------------------------

    protected void paintFocus(Graphics g, int x, int y, int w, int h) {

        Color bsColor = getBorderSelectionColor();
        if (bsColor == null) return;

        g.setColor(bsColor);
        g.drawRect(x, y, w - 1, h - 1);

    }// -----------------------------------------------------------------------------

    /**
     * Overrides <code>JComponent.getPreferredSize</code> to return slightly
     * wider preferred size value.
     */
    public Dimension getPreferredSize() {

        Dimension d = super.getPreferredSize();
        if (d != null) d = new Dimension(d.width + 3, d.height);
        return d;

    }

    // from here: Overridden for performance reasons. see javax.swing....

    public void validate() {

    }

    public void revalidate() {

    }

    public void repaint(long tm, int x, int y, int width, int height) {

    }

    public void repaint(Rectangle r) {

    }

    protected void firePropertyChange(String propertyName, Object oldValue, Object newValue) {

        if (Objects.equals(propertyName, "text"))
            // Strings get interned...
            super.firePropertyChange(propertyName, oldValue,
                    newValue);
    }

    public void firePropertyChange(String propertyName, byte oldValue, byte newValue) {

    }

    public void firePropertyChange(String propertyName, char oldValue, char newValue) {

    }

    public void firePropertyChange(String propertyName, short oldValue, short newValue) {

    }

    public void firePropertyChange(String propertyName, int oldValue, int newValue) {

    }

    public void firePropertyChange(String propertyName, long oldValue, long newValue) {

    }

    public void firePropertyChange(String propertyName, float oldValue, float newValue) {

    }

    public void firePropertyChange(String propertyName, double oldValue, double newValue) {

    }

    public void firePropertyChange(String propertyName, boolean oldValue, boolean newValue) {

    }
    /**
     * check if the component is the permanent focus owner and its top window is
     * the focused window
     *
     * @param component
     * @return
     */
    public static boolean isPermFocusAndAppActive(Component component) {

        KeyboardFocusManager fm = KeyboardFocusManager.getCurrentKeyboardFocusManager();
        if (component != fm.getPermanentFocusOwner()) return false;

        Window fw = fm.getFocusedWindow();
        return fw == component || SwingUtilities.getWindowAncestor(component) == fw;
    }
}