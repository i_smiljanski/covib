package com.uptodata.isr.gui.swing.plaf.auxUI;

import javax.accessibility.Accessible;
import javax.swing.*;
import javax.swing.plaf.ComponentUI;
import javax.swing.plaf.ScrollPaneUI;
import java.awt.*;


/**
 * In 1.4 Swing Scrollpane-Scrollbars are focusable by default, this Aux UI
 * removes this. <br>
 * 
 * 
 * @author PeterBuettner.de
 *  
 */
public class PEBAuxScrollPaneUI extends ScrollPaneUI {

    public void installUI( JComponent c ) {

        ( (JScrollPane)c ).getVerticalScrollBar().setFocusable( false );
        ( (JScrollPane)c ).getHorizontalScrollBar().setFocusable( false );
    }

    public static ComponentUI createUI( JComponent c ) {

        return new PEBAuxScrollPaneUI();
    }

    /*
     * these are called for each aux UI, but returnvalues are thrown away. see
     * \docs\api\javax\swing\plaf\multi\doc-files\multi_tsc.html
     */

    /* ComponentUI, overwrite for 'performance' [and function:update] */
    public void update( Graphics g, JComponent c ) {

    }// or nothing can be seen

    public String getToolTipText() {

        return null;
    }

    public boolean contains( JComponent c, int x, int y ) {

        return false;
    }

    public void paint( Graphics g, JComponent c ) {

    }

    public int getAccessibleChildrenCount( JComponent c ) {

        return 0;
    }

    public Accessible getAccessibleChild( JComponent c, int i ) {

        return null;
    }

    public Dimension getMaximumSize( JComponent c ) {

        return null;
    }

    public Dimension getMinimumSize( JComponent c ) {

        return null;
    }

    public Dimension getPreferredSize( JComponent c ) {

        return null;
    }

}