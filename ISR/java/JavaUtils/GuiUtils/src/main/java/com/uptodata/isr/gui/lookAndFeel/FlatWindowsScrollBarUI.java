package com.uptodata.isr.gui.lookAndFeel;

import com.sun.java.swing.plaf.windows.WindowsScrollBarUI;
import com.uptodata.isr.gui.util.Colors;

import javax.swing.*;
import javax.swing.plaf.ComponentUI;
import java.awt.*;
import java.awt.event.AdjustmentEvent;
import java.awt.event.AdjustmentListener;
import java.awt.geom.Rectangle2D;


public class FlatWindowsScrollBarUI extends WindowsScrollBarUI {

    /**
     * if we are in a Scrollpane and there is a Table in the view paint markers
     * for the selected elements (out of view) if there is a
     * getClientProperty(PAINT_SELECTION_MARKER) in the table there is a need to
     * repaint the scroller if selection changes, we don't add any listener
     * here, see {@link com.utd.gui.event.RepaintScrollerOnSelectionChange}use
     * a String "v" and/or "H" to have the markers in the horizontal/vertical
     * bar
     */
    public static final String PAINT_SELECTION_MARKER = FlatWindowsScrollBarUI.class
            .getName()
            + ".PAINT_SELECTION_MARKER";

    private static AdjustmentListener adjListener = new AdjustmentListener() {

        public void adjustmentValueChanged(
                AdjustmentEvent e) {

            if (e.getAdjustable() instanceof Component)
                ((Component) e
                        .getAdjustable())
                        .repaint();
        }
    };

    public static ComponentUI createUI(JComponent h) {

        return new FlatWindowsScrollBarUI();

    }

    public void installUI(JComponent c) {

        super.installUI(c);
        ((JScrollBar) c).addAdjustmentListener(adjListener);

    }

    public void uninstallUI(JComponent c) {

        ((JScrollBar) c).removeAdjustmentListener(adjListener);
        super.uninstallUI(c);

    }

    protected JButton createArrowButton(int orientation) {

        ButtonColor bc;
        if (LFConst.FULL_FLAT)
            bc = new ButtonColor(UIManager.getColor("ScrollBar.thumbDarkShadow"), UIManager
                    .getColor("ScrollBar.thumbHighlight"), UIManager
                    .getColor("ScrollBar.thumbShadow"));
        else
            bc = new ButtonColor(UIManager.getColor("ScrollBar.thumbDarkShadow"), UIManager
                    .getColor("ScrollBar.thumbDarkShadow"), UIManager
                    .getColor("ScrollBar.thumbShadow"));

        JButton button = new SBButton(orientation, UIManager.getColor("ScrollBar.thumb"),
                bc);
        bc.setButton(button);

        if (LFConst.FULL_FLAT)
            button.setBorder(new FlatArrowButtonBorder(Colors.getUIControlShadow(), Colors
                    .getUIControlDkShadow()));
        else
            button.setBorder(new FlatButtonBorder());

        return button;

    }

    protected JButton createDecreaseButton(int orientation) {

        return createArrowButton(orientation);
    }

    protected JButton createIncreaseButton(int orientation) {

        return createArrowButton(orientation);
    }

    private class SBButton extends FlatArrowButton {

        public SBButton(int direction, Color background, ButtonColor color) {

            super(direction, background, color);

        }

        public Dimension getPreferredSize() {

            int size = 16;
            if (scrollbar == null) return new Dimension(size, size);
            JScrollBar s = scrollbar;
            size = Math.max(5, s.getOrientation() == JScrollBar.VERTICAL ? s.getWidth() : s
                    .getHeight());
            return new Dimension(size, size);

        }

    }

    protected void paintThumb(Graphics g, JComponent c, Rectangle thumbBounds) {

        if (thumbBounds.isEmpty() || !scrollbar.isEnabled()) return;

        int w = thumbBounds.width;
        int h = thumbBounds.height;
        boolean adj = scrollbar.getValueIsAdjusting();// && false;

        g.translate(thumbBounds.x, thumbBounds.y);

        if (LFConst.FULL_FLAT) {// full flat

            g.setColor(adj ? thumbDarkShadowColor : thumbLightShadowColor);
            g.drawRect(0, 0, w - 1, h - 1);
            g.setColor(adj ? thumbLightShadowColor : thumbColor);
            g.fillRect(1, 1, w - 2, h - 2);

        } else {

            // topleft, windows paints bottom after top
            g.setColor(thumbHighlightColor);
            g.drawLine(0, 0, w - 1, 0);
            g.drawLine(0, 0, 0, h - 1);

            // bottomright
            g.setColor(thumbLightShadowColor);
            g.drawLine(0, h - 1, w - 1, h - 1);
            g.drawLine(0 + w - 1, 0, w - 1, h - 1);

            // fill
            g.setColor(thumbColor);
            g.fillRect(1, 1, w - 2, h - 2);

        }

        g.translate(-thumbBounds.x, -thumbBounds.y);

    }

    /**
     * if we are in a Scrollpane and there is a Table in the view paint markers
     * for the selected elements (out of view) if there is a
     * getClientProperty(PAINT_SELECTION_MARKER) in the table
     * <p>
     * there is an need to repaint on selection change we don't add any listener
     * here, see {@link com.utd.gui.event.RepaintScrollerOnSelectionChange}
     */
    protected void paintTrack(Graphics g1, JComponent c, Rectangle trackBounds) {

        Graphics2D g = (Graphics2D) g1;
        super.paintTrack(g, c, trackBounds);

        if (!(c.getParent() instanceof JScrollPane)) return;

        JScrollPane sp = (JScrollPane) c.getParent();
        Component v = sp.getViewport().getView();
        if (!(v instanceof JTable)) return;

        JTable table = (JTable) v;
        // ... paint or not ................................................
        Object vertHorz = table.getClientProperty(PAINT_SELECTION_MARKER);
        if (vertHorz == null) return;
        int orient = scrollbar.getOrientation();

        if (orient == JScrollBar.VERTICAL
                && vertHorz.toString().toLowerCase().indexOf('v') == -1) return;
        if (orient == JScrollBar.HORIZONTAL
                && vertHorz.toString().toLowerCase().indexOf('h') == -1) return;
        // ...................................................................

        int[] sel = table.getSelectedRows();
        if (sel == null || sel.length == 0) return;
        // since we paint the track we know that there is something invisible
        // maybe later if 'scroller always visible' don't paint?

        int dx = trackBounds.width / 2;
        int dy = (dx * 2) / 3;
        double scale = (trackBounds.height * 1.0) / table.getRowCount();

        // w/o it is sharp but not exactly aligned, with it is toosmooth
        //	RenderingHints saveRenderingHints = g.getRenderingHints();
        //	g.setRenderingHint(RenderingHints.KEY_ANTIALIASING,
        // RenderingHints.VALUE_ANTIALIAS_ON);
        //	g.setRenderingHint(RenderingHints.KEY_STROKE_CONTROL,RenderingHints.VALUE_STROKE_PURE);

        Rectangle2D.Double r = new Rectangle2D.Double(dx / 2, 0, dx, dy);
        g.translate(trackBounds.x, trackBounds.y);
        for (int i = 0; i < sel.length; i++) {

            r.y = sel[i] * scale + 1;

            g.setColor(Colors.getUIControl());
            g.fill(r);
            g.setColor(Colors.getUIControlShadow());
            g.draw(r);

        }

        g.translate(-trackBounds.x, -trackBounds.y);
        //	g.setRenderingHints(saveRenderingHints);
    }// -------------------------------------------------------

}