package com.uptodata.isr.gui.util;

import javax.swing.*;
import java.awt.*;


/**
 * Easy acces UIManager Colors, defines standard names used in
 * UIManager.getColor() as Constants, see also {@link java.awt.SystemColor},
 * but SystemColor doesn't reflect the properties if the Swing Look &amp; Feel
 * <p>
 * TODO remove hardcoded colors
 *
 * @author PeterBuettner.de
 */
public class Colors {

    public static final String DESKTOP = "desktop";
    public static final String ACTIVE_CAPTION = "activeCaption";
    public static final String ACTIVE_CAPTION_TEXT = "activeCaptionText";
    public static final String ACTIVE_CAPTION_BORDER = "activeCaptionBorder";
    public static final String INACTIVE_CAPTION = "inactiveCaption";
    public static final String INACTIVE_CAPTION_TEXT = "inactiveCaptionText";
    public static final String INACTIVE_CAPTION_BORDER = "inactiveCaptionBorder";
    public static final String WINDOW = "window";
    public static final String WINDOW_BORDER = "windowBorder";
    public static final String WINDOW_TEXT = "windowText";
    public static final String MENU = "menu";
    public static final String MENU_TEXT = "menuText";
    public static final String TEXT = "text";
    public static final String TEXT_TEXT = "textText";
    public static final String TEXT_HIGHLIGHT = "textHighlight";
    public static final String TEXT_HIGHLIGHT_TEXT = "textHighlightText";
    public static final String TEXT_INACTIVE_TEXT = "textInactiveText";
    public static final String CONTROL = "control";
    public static final String CONTROL_TEXT = "controlText";
    public static final String CONTROL_HIGHLIGHT = "controlHighlight";
    public static final String CONTROL_LT_HIGHLIGHT = "controlLtHighlight";
    public static final String CONTROL_SHADOW = "controlShadow";
    public static final String CONTROL_DK_SHADOW = "controlDkShadow";
    public static final String SCROLLBAR = "scrollbar";
    public static final String INFO = "info";
    public static final String INFO_TEXT = "infoText";
    public static final String TABLE_BACKGROUND = "Table.background";

    /**
     * Ask UIManager for Color 'desktop'.
     */
    public static Color getUIDesktop() {

        return UIManager.getColor(DESKTOP);

    }

    /**
     * Ask UIManager for Color 'activeCaption'.
     */
    public static Color getUIActiveCaption() {

        return UIManager.getColor(ACTIVE_CAPTION);

    }

    /**
     * Ask UIManager for Color 'activeCaptionText'.
     */
    public static Color getUIActiveCaptionText() {

        return UIManager.getColor(ACTIVE_CAPTION_TEXT);

    }

    /**
     * Ask UIManager for Color 'activeCaptionBorder'.
     */
    public static Color getUIActiveCaptionBorder() {

        return UIManager.getColor(ACTIVE_CAPTION_BORDER);

    }

    /**
     * Ask UIManager for Color 'inactiveCaption'.
     */
    public static Color getUIInactiveCaption() {

        return UIManager.getColor(INACTIVE_CAPTION);

    }

    /**
     * Ask UIManager for Color 'inactiveCaptionText'.
     */
    public static Color getUIInactiveCaptionText() {

        return UIManager.getColor(INACTIVE_CAPTION_TEXT);

    }

    /**
     * Ask UIManager for Color 'inactiveCaptionBorder'.
     */
    public static Color getUIInactiveCaptionBorder() {

        return UIManager.getColor(INACTIVE_CAPTION_BORDER);

    }

    /**
     * Ask UIManager for Color 'window'.
     */
    public static Color getUIWindow() {

        return UIManager.getColor(WINDOW);

    }

    /**
     * Ask UIManager for Color 'windowBorder'.
     */
    public static Color getUIWindowBorder() {

        return UIManager.getColor(WINDOW_BORDER);

    }

    /**
     * Ask UIManager for Color 'windowText'.
     */
    public static Color getUIWindowText() {

        return UIManager.getColor(WINDOW_TEXT);

    }

    /**
     * Ask UIManager for Color 'menu'.
     */
    public static Color getUIMenu() {

        return UIManager.getColor(MENU);

    }

    /**
     * Ask UIManager for Color 'menuText'.
     */
    public static Color getUIMenuText() {

        return UIManager.getColor(MENU_TEXT);

    }

    /**
     * Ask UIManager for Color 'text'.
     */
    public static Color getUIText() {

        return UIManager.getColor(TEXT);

    }

    /**
     * Ask UIManager for Color 'textText'.
     */
    public static Color getUITextText() {

        return UIManager.getColor(TEXT_TEXT);

    }

    /**
     * Ask UIManager for Color 'textHighlight'.
     */
    public static Color getUITextHighlight() {

        return UIManager.getColor(TEXT_HIGHLIGHT);

    }

    /**
     * Ask UIManager for Color 'textHighlightText'.
     */
    public static Color getUITextHighlightText() {

        return UIManager.getColor(TEXT_HIGHLIGHT_TEXT);

    }

    /**
     * Ask UIManager for Color 'textInactiveText'.
     */
    public static Color getUITextInactiveText() {

        return UIManager.getColor(TEXT_INACTIVE_TEXT);

    }

    /**
     * Ask UIManager for Color 'control'.
     */
    public static Color getUIControl() {

        return UIManager.getColor(CONTROL);

    }

    /**
     * Ask UIManager for Color 'controlText'.
     */
    public static Color getUIControlText() {

        return UIManager.getColor(CONTROL_TEXT);

    }

    /**
     * Ask UIManager for Color 'controlHighlight'.
     */
    public static Color getUIControlHighlight() {

        return UIManager.getColor(CONTROL_HIGHLIGHT);

    }

    /**
     * Ask UIManager for Color 'controlLtHighlight'.
     */
    public static Color getUIControlLtHighlight() {

        return UIManager.getColor(CONTROL_LT_HIGHLIGHT);

    }

    /**
     * Ask UIManager for Color 'controlShadow'.
     */
    public static Color getUIControlShadow() {

        return UIManager.getColor(CONTROL_SHADOW);

    }

    /**
     * Ask UIManager for Color 'controlDkShadow'.
     */
    public static Color getUIControlDkShadow() {

        return UIManager.getColor(CONTROL_DK_SHADOW);

    }

    /**
     * Ask UIManager for Color 'scrollbar'.
     */
    public static Color getUIScrollbar() {

        return UIManager.getColor(SCROLLBAR);

    }

    /**
     * Ask UIManager for Color 'info'.
     */
    public static Color getUIInfo() {

        return UIManager.getColor(INFO);

    }

    /**
     * Ask UIManager for Color 'infoText'.
     */
    public static Color getUIInfoText() {

        return UIManager.getColor(INFO_TEXT);

    }

    /**
     * Synthesize a color to paint background of listitems in a control that
     * doesn't have focus.
     */
    public static Color getItemHiliteInactiveBack() {

        // win uses controlHighlight but this is also used for dialog/form
        // background (ugly)
        // so we mostly use the brighter version if
        // the colors 'LtHilite != windows'
        Color c1 = getUIControlLtHighlight();
        Color c2 = getUIWindow(); // list or table background
        if (!c1.equals(c2)) return c1;

        return getColorBetween(getUIControlHighlight(), c2);

    }

    /**
     * Synthesize a color to paint foreground of listitems in a control that
     * doesn't have focus.
     */
    public static Color getItemHiliteInactiveText() {

        return getUIControlText();

    }

    /**
     * Synthesize a color to paint the background of components (JTextField,
     * JTextArea,...) to show invalid data, is similar to 'normal' text back, so
     * you should use normal text color
     */
    public static Color getDataBackInvalid() {

        return new Color(0xffe8e0);

    }

    /**
     * Synthesize a color to paint the background of components (JTextField,
     * JTextArea,...) to show empty data, is similar to 'normal' text back, so
     * you should use normal text color
     */
    public static Color getDataBackIllegalEmpty() {

        return new Color(0xf0f0ff);

    }

    /**
     * Synthesize a text color for warning messages (red) for controls with
     * standard background
     */
    public static Color getUIControlTextWarn() {

        return Color.RED;

    }

    public static Color getUIControlTableBackground() {

        return UIManager.getColor(TABLE_BACKGROUND);

    }

    /**
     * brighter or darker, cutoff to fit into [0,255]
     *
     * @param color, if null returns null
     * @param factor
     * @return
     */
    private static Color getFactoredColor(Color color, float factor) {

        if (color == null) return null;
        return new Color(Math.min((int) (color.getRed() * factor), 255), Math
                .min((int) (color.getGreen() * factor), 255), Math
                .min((int) (color.getBlue() * factor), 255));

    }

    /**
     * like {@link getFactoredColor()}, but you ask for a UIManager-color-name
     *
     * @param uiColor
     * @param factor
     * @return
     * @see getFactoredColor
     */
    private static Color getFactoredColor(String uiColor, float factor) {

        return getFactoredColor(UIManager.getColor(uiColor), factor);

    }

    /**
     * Calculate a color between two others
     *
     * @param col1
     * @param col2
     * @return null if one of the inputs is null
     */
    private static Color getColorBetween(Color col1, Color col2) {

        if ((col1 == null) || (col2 == null)) return null;
        return new Color(Math.min((col1.getRed() + col2.getRed()) / 2, 255), Math
                .min((col1.getGreen() + col2.getGreen()) / 2, 255), Math
                .min((col1.getBlue() + col2.getBlue()) / 2, 255));

    }

    /**
     * like {@link getColorBetween()}, but with UIManager-color-names
     *
     * @param uiColor1
     * @param uiColor2
     * @return
     */
    private static Color getColorBetween(String uiColor1, String uiColor2) {

        return getColorBetween(UIManager.getColor(uiColor1), UIManager.getColor(uiColor2));

    }

}