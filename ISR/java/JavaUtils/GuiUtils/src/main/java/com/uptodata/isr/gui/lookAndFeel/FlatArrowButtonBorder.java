package com.uptodata.isr.gui.lookAndFeel;


import com.uptodata.isr.gui.borders.OnePixelBorder;
import com.uptodata.isr.gui.util.Colors;

import javax.swing.*;
import java.awt.*;


/**
 * For Arrowbuttons of comboboxes and scrollbars
 *
 * @author PeterBuettner.de
 */
public class FlatArrowButtonBorder extends OnePixelBorder {

    protected Color pressed;
    protected Color normal;

    public FlatArrowButtonBorder(Color normal, Color pressed) {

        this.pressed = pressed;
        this.normal = normal;

    }

    public FlatArrowButtonBorder() {

        this(Colors.getUIControlShadow(), Colors.getUIControlDkShadow());

    }

    public void paintBorder(Component c, Graphics g, int x, int y, int width, int height) {

        boolean isPressed = false;

        if (c instanceof AbstractButton) {

            ButtonModel model = ((AbstractButton) c).getModel();
            isPressed = model.isPressed();// && model.isArmed();
        }

        Color old = g.getColor();
        paintButton(g, x, y, width, height, isPressed, normal, pressed);
        g.setColor(old);

    }

    private void paintButton(Graphics g, int x, int y, int w, int h, boolean isPressed,
                             Color shadow, Color dark) {

        g.setColor(!isPressed ? shadow : dark);
        g.drawRect(x, y, w - 1, h - 1);

    }

}