package com.uptodata.isr.gui.util;

import java.awt.*;


/**
 * Tools around the screen (monitor)
 *
 * @author PeterBuettner.de
 */
public class Screen {

    /**
     * Calculates the bounds (size with insets included, insets -> e.g. a
     * taskbar) of the screen where the component resides, if the component is
     * not 'on the screen' etc (see:
     * {@link java.awt.Component.getGraphicsConfiguration()}) the bounds
     * (without examining insets ) of the primary screen is returned
     *
     * @param component
     * @return
     */
    public static Rectangle getScreenBounds(Component component) {

        GraphicsConfiguration gc = component.getGraphicsConfiguration();
        if (gc != null) {
            Insets i = Toolkit.getDefaultToolkit().getScreenInsets(gc);
            Rectangle screen = gc.getBounds();

            screen.width -= (i.left + i.right);
            screen.height -= (i.top + i.bottom);
            screen.x += i.left;
            screen.y += i.top;
            return screen;
        }
        // default:
        return new Rectangle(new Point(), Toolkit.getDefaultToolkit().getScreenSize());

    }

}