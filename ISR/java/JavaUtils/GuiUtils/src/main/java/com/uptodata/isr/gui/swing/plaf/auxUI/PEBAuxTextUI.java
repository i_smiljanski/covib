package com.uptodata.isr.gui.swing.plaf.auxUI;

import javax.accessibility.Accessible;
import javax.swing.*;
import javax.swing.plaf.ComponentUI;
import javax.swing.plaf.TextUI;
import javax.swing.text.BadLocationException;
import javax.swing.text.EditorKit;
import javax.swing.text.JTextComponent;
import javax.swing.text.Position.Bias;
import javax.swing.text.View;
import java.awt.*;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;


/**
 * Auxilliary TextUI that adds popup-menus to JTextComponents
 * 
 * @author PeterBuettner.de
 */

public class PEBAuxTextUI extends TextUI {

    static PEBMouseListener mouseListener = new PEBMouseListener();

    public PEBAuxTextUI() {

    }

    public void installUI( JComponent c ) {

        c.addMouseListener( mouseListener );
    }

    public void uninstallUI( JComponent c ) {

        c.removeMouseListener( mouseListener );
    }

    public static ComponentUI createUI( JComponent c ) {

        return new PEBAuxTextUI();
    }

    // *****************************************************************

    /*
     * these are called for each aux UI, but returnvalues are thrown away. see
     * \docs\api\javax\swing\plaf\multi\doc-files\multi_tsc.html
     */

    /* ComponentUI, overwrite for 'performance' [and function:update] */
    public void update( Graphics g, JComponent c ) {

    }// or nothing can be seen

    public String getToolTipText( JTextComponent t, Point pt ) {

        return null;
    }

    public boolean contains( JComponent c, int x, int y ) {

        return false;
    }

    public void paint( Graphics g, JComponent c ) {

    }

    public int getAccessibleChildrenCount( JComponent c ) {

        return 0;
    }

    public Accessible getAccessibleChild( JComponent c, int i ) {

        return null;
    }

    public Dimension getMaximumSize( JComponent c ) {

        return null;
    }

    public Dimension getMinimumSize( JComponent c ) {

        return null;
    }

    public Dimension getPreferredSize( JComponent c ) {

        return null;
    }

    /* TextUI, overwrite for 'performance' */
    public Rectangle modelToView( JTextComponent t, int pos ) throws BadLocationException {

        return null;
    }

    public Rectangle modelToView( JTextComponent t, int pos, Bias bias )
                                                                        throws BadLocationException {

        return null;
    }

    public int viewToModel( JTextComponent t, Point pt ) {

        return 0;
    }

    public int viewToModel( JTextComponent arg0, Point arg1, Bias[] arg2 ) {

        return 0;
    }

    public int getNextVisualPositionFrom( JTextComponent arg0, int arg1, Bias arg2, int arg3,
                                         Bias[] arg4 ) throws BadLocationException {

        return 0;
    }

    public void damageRange( JTextComponent t, int p0, int p1 ) {

    }

    public void damageRange( JTextComponent t, int p0, int p1, Bias firstBias, Bias secondBias ) {

    }

    public EditorKit getEditorKit( JTextComponent t ) {

        return null;
    }

    public View getRootView( JTextComponent t ) {

        return null;
    }

    // -----------------------------------------------------------------

    static class PEBMouseListener extends MouseAdapter {

        public void mouseReleased( MouseEvent e ) {

            mayPopup( e );
        }

        public void mousePressed( MouseEvent e ) {

            mayPopup( e );
        }

        private void mayPopup( MouseEvent e ) {

            if ( e.isPopupTrigger() ) {
                JTextComponent tc = (JTextComponent)e.getSource();
                if ( !tc.isEnabled() ) return;
                // activate on 'right-click'/popup-button so selection is shown:
                // also the wrapping of the actions is unneccesary,
                // since the DefaultEdi...-Actions use getFocussedComponent()
                // if the sender (popupMenu) is no TextComponent
                tc.requestFocus();
                new PEBTextCopyPopupMenu( tc ).show( tc, e.getX(), e.getY() );

            }

        }

    }

}