package com.uptodata.isr.gui.lookAndFeel;


import com.uptodata.isr.gui.util.Colors;

import javax.swing.*;
import javax.swing.border.AbstractBorder;
import java.awt.*;


/**
 * real flat button border: combine with MarginBorder, caveat: look out for
 * toggleButtonBorders
 *
 * @author PeterBuettner.de
 */
public class FlatButtonBorder extends AbstractBorder {

    protected Color shadow;
    protected Color darkShadow;
    protected Color highlight;
    protected Color lightHighlight;

    public FlatButtonBorder(Color shadow, Color darkShadow, Color highlight,
                            Color lightHighlight) {

        this.shadow = shadow;
        this.darkShadow = darkShadow;
        this.highlight = highlight;
        this.lightHighlight = lightHighlight;

    }

    public FlatButtonBorder() {

        this(Colors.getUIControlShadow(), Colors.getUIControlDkShadow(), Colors
                .getUIControlHighlight(), Colors.getUIControlLtHighlight());

    }

    public void paintBorder(Component c, Graphics g, int x, int y, int width, int height) {

        boolean isPressed = false;
        boolean isDefault = false;

        if (c instanceof AbstractButton) {

            ButtonModel model = ((AbstractButton) c).getModel();
            isPressed = model.isPressed() && model.isArmed();
            isDefault = (c instanceof JButton) && ((JButton) c).isDefaultButton();

        }

        Color old = g.getColor();
        paintButton(g, x, y, width, height, isPressed, isDefault, lightHighlight, shadow,
                darkShadow);
        g.setColor(old);

    }

    private void paintButton(Graphics g, int x, int y, int w, int h, boolean isPressed,
                             boolean isDefault, Color lite, Color shadow, Color dark) {

        if (isDefault) { //has a ''black'' border around

            g.setColor(dark);
            g.drawRect(x, y, w - 1, h - 1);

            if (false) {

                g.drawRect(x + 1, y + 1, w - 3, h - 3);
                x++;
                y++;
                w -= 2;
                h -= 2;

            }

            x++;
            y++;
            w -= 2;
            h -= 2;

        }

        // topleft, windows paints bottom after top
        g.setColor(isPressed ? shadow : lite);
        g.drawLine(x, y, x + w - 1, y);
        g.drawLine(x, y, x, y + h - 1);

        // bottomright
        g.setColor(!isPressed ? shadow : lite);
        g.drawLine(x, y + h - 1, x + w - 1, y + h - 1);
        g.drawLine(x + w - 1, y, x + w - 1, y + h - 1);

    }

    public Insets getBorderInsets(Component c) {

        return getBorderInsets(c, new Insets(0, 0, 0, 0));

    }

    public Insets getBorderInsets(Component c, Insets insets) {

        // leave room for default visual
        insets.top = 2;
        insets.left = insets.bottom = insets.right = 3;
        return insets;

    }

}