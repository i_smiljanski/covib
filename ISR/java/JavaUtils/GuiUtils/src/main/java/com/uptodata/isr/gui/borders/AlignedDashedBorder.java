package com.uptodata.isr.gui.borders;

import javax.swing.*;
import java.awt.*;


/**
 * <p>
 * A bit like {@link DashedBorder}but: <br>
 * This one is special, it paints a dotted line in a CellRenderer, where the
 * dots are aligned relative to the screen, so that multiple cells side-by-side
 * looks pretty, it should work in all type of components, but needed only in
 * JTables :: scrolling 'parent' is usable for JTable or any component contained
 * in a JViewport.
 * </p>
 * <p>
 * A better focus border for windows L&F, since the sun implementation assumes a
 * 'Selected Color' as background, but unselected list/table/tree items may have
 * focus.
 * </p>
 * <p>
 * <b>Example usage: </b> <br>
 * new
 * AlignedDashedBorder(table.getSelectionBackground(),table.getSelectionForeground(),
 * true, false); <br>
 * or, asking the table for selectedBackground and selectionForeground (if no
 * containing JTable UIManager.getColor("Table.selectionBackground") etc is
 * used) <br>
 * new AlignedDashedBorder( true, false);
 */
public class AlignedDashedBorder extends OnePixelBorder {

    /**
     * if true then ignore colors and use the table ones
     */
    private boolean autocolor;

    /**
     * using a nondefault color, see: autocolor
     */
    private Color dashColor;
    /**
     * using a nondefault color, see: autocolor
     */
    private Color backColor;

    /**
     * draw left side
     */
    private boolean left;
    /**
     * draw right side
     */
    private boolean right;

    // shared instances
    private final static AlignedDashedBorder CELL_LEFT = new AlignedDashedBorder(true,
            false);
    private final static AlignedDashedBorder CELL_MIDDLE = new AlignedDashedBorder(false,
            false);
    private final static AlignedDashedBorder CELL_RIGHT = new AlignedDashedBorder(false,
            true);
    private final static AlignedDashedBorder CELL_SINGLE = new AlignedDashedBorder(true, true);

    /**
     * get shared instances of AlignedDashedBorder, see:
     * {@link AlignedDashedBorder(boolean left, boolean right)}
     *
     * @param left
     * @param right
     * @return
     */
    public static AlignedDashedBorder getShared(boolean left, boolean right) {

        if (left && right) return CELL_SINGLE;
        if (left) return CELL_LEFT;
        if (right) return CELL_RIGHT;
        return CELL_MIDDLE;
    }

    /**
     * create new border, see {@link AlignedDashedBorder}, colors of the
     * containing JTable are used ({@link javax.swing.JTable#getSelectionForeground()},
     * {@link javax.swing.JTable#getSelectionBAckground()}), if no containing JTable
     * UIManager.getColor("Table.selectionBackground") etc is used
     *
     * @param left
     * @param right
     */
    public AlignedDashedBorder(boolean left, boolean right) {

        this.left = left;
        this.right = right;
        autocolor = true;
    }

    /**
     * create new border, see {@link AlignedDashedBorder}.
     *
     * @param color1
     * @param color2
     * @param left
     * @param right
     */
    public AlignedDashedBorder(Color color1, Color color2, boolean left, boolean right) {

        this.left = left;
        this.right = right;
        backColor = color1;
        dashColor = color2;
    }

    public void paintBorder(Component c, Graphics g, int x, int y, int width, int height) {

        // Note: this probably (is untested) doesn't work for translated
        // graphics!

        //	 ... get info for sync dots ..............................
        // get ancestor JTable or (if null) the Component contained in a
        // JViewport
        Component ancestor = SwingUtilities.getAncestorOfClass(JTable.class, c);
        if (ancestor == null) {
            ancestor = SwingUtilities.getAncestorOfClass(JViewport.class, c);
            if (ancestor != null) ancestor = ((JViewport) ancestor).getView();
        }

        // pos of the renderer rel to parent
        Point pt = new Point();
        if (ancestor == null)
            SwingUtilities.convertPointToScreen(pt, c); // try harder:
            // toScreen, but is
            // _ugly_ if scrolled
        else
            pt = SwingUtilities.convertPoint(c, pt, ancestor);
        boolean doOffset = odd(pt.x + pt.y);

        // ... colors ..............................
        if (autocolor) {
            if (ancestor instanceof JTable) {
                backColor = ((JTable) ancestor).getSelectionBackground();
                dashColor = ((JTable) ancestor).getSelectionForeground();
            } else {
                backColor = UIManager.getColor("Table.selectionBackground");
                dashColor = UIManager.getColor("Table.selectionForeground");
            }
        }

        // ... do paint ...................................

        Color oldColor = g.getColor();

        // 'background' first
        g.setColor(backColor);
        if (left) g.fillRect(x, y, 1, height);
        if (right) g.fillRect(x + width - 1, y, 1, height);
        // top and bottom lines
        g.fillRect(x, y, width, 1);
        g.fillRect(x, y + height - 1, width, 1);

        // now the dashes
        g.setColor(dashColor);
        if (left) drawDashedVLine(g, x, y, height, doOffset);
        if (right) drawDashedVLine(g, x + width - 1, y, height, doOffset);
        drawDashedHLines(g, x, y, width, height, doOffset); // top and bottom
        // lines

        g.setColor(oldColor);
    }

    private void drawDashedHLines(Graphics g, int x, int y, int width, int height,
                                  boolean doOffset) {

        // draw upper and lower horizontal dashes, in the offset x,y is mangled
        // in!
        for (int i = x + ord(doOffset); i < (x + width); i += 2)
            g.fillRect(i, y, 1, 1);

        doOffset = doOffset ^ even(height);
        for (int i = x + ord(doOffset); i < (x + width); i += 2)
            g.fillRect(i, y + height - 1, 1, 1);
    }

    private void drawDashedVLine(Graphics g, int x, int y, int height, boolean doOffset) {

        // x is not the x of the border! so recalc:
        doOffset = doOffset ^ odd(x + y); // switch in cases
        // draw vertical dashes
        for (int i = y + ord(doOffset); i < (y + height); i += 2)
            g.fillRect(x, i, 1, 1);
    }

    /**
     * true:1 <br>
     * false:0
     */
    private static int ord(boolean b) {

        return b ? 1 : 0;
    }

    private static boolean odd(int i) {

        return (i & 1) != 0;
    }

    private static boolean even(int i) {

        return (i & 1) == 0;
    }

}