package com.uptodata.isr.gui.awt.wind.hwnd;

/**
 * 
 * @author PeterBuettner.de
 *  
 */
public interface IWindowGripCanvas {

    public static final int SWP_NOSIZE         = 0x0001;
    public static final int SWP_NOMOVE         = 0x0002;
    public static final int SWP_NOZORDER       = 0x0004;
    public static final int SWP_NOREDRAW       = 0x0008;
    public static final int SWP_NOACTIVATE     = 0x0010;
    public static final int SWP_FRAMECHANGED   = 0x0020;           /*
                                                                    * The frame
                                                                    * changed:
                                                                    * send
                                                                    * WM_NCCALCSIZE
                                                                    */
    public static final int SWP_SHOWWINDOW     = 0x0040;
    public static final int SWP_HIDEWINDOW     = 0x0080;
    public static final int SWP_NOCOPYBITS     = 0x0100;
    public static final int SWP_NOOWNERZORDER  = 0x0200;           /*
                                                                    * Don't do
                                                                    * owner Z
                                                                    * ordering
                                                                    */
    public static final int SWP_NOSENDCHANGING = 0x0400;           /*
                                                                    * Don't send
                                                                    * WM_WINDOWPOSCHANGING
                                                                    */

    public static final int SWP_DRAWFRAME      = SWP_FRAMECHANGED;
    public static final int SWP_NOREPOSITION   = SWP_NOOWNERZORDER;

    //	 #if(WINVER >= 0x0400)
    public static final int SWP_DEFERERASE     = 0x2000;
    public static final int SWP_ASYNCWINDOWPOS = 0x4000;
    //	 #endif

    public static final int HWND_TOP           = 0;
    public static final int HWND_BOTTOM        = 1;
    public static final int HWND_TOPMOST       = -1;
    public static final int HWND_NOTOPMOST     = -2;

    /**
     * For less flicker while 'size and move' the topmost parent use
     * SWP_NOACTIVATE | SWP_NOZORDER. see Windows API
     * 
     * @param hwnd
     * @param hwndAfter
     * @param x
     * @param y
     * @param width
     * @param height
     * @param flags
     */
    public void setWindowPos( int hwnd, int hwndAfter, int x, int y, int width, int height,
                             int flags );

    /**
     * the first parent/grandparent/g... of this with (Style != WS_CHILD) so it
     * will be Frame/Window/Dialog Only valid if this canvas was painted once,
     * if you did some things like dispose, etc, it maybe invalid until painted
     * another time.
     * 
     * @return
     */
    public int getWindowHandle();

}