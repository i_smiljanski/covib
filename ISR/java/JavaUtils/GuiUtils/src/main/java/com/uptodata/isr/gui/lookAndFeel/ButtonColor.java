package com.uptodata.isr.gui.lookAndFeel;

import javax.swing.*;
import java.awt.*;


/**
 * Color that changes value on state of a button :-)
 */
class ButtonColor extends Color {

    private JButton button;
    private Color pressed;
    private Color disabled;

    public ButtonColor(Color normal, Color pressed, Color disabled) {

        super(normal.getRGB());
        this.pressed = pressed;
        this.disabled = disabled;

    }

    public int getRGB() {

        if (button != null) {

            ButtonModel m = button.getModel();
            if (!m.isEnabled()) return disabled.getRGB();
            if (m.isArmed() && m.isPressed()) return pressed.getRGB();

        }

        return super.getRGB();

    }

    public void setButton(JButton button) {

        this.button = button;

    }

}