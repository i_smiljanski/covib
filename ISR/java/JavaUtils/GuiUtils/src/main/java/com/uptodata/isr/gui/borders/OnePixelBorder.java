package com.uptodata.isr.gui.borders;

import javax.swing.border.Border;
import java.awt.*;


/**
 * Lite Base implementation for a all side 1px wide border
 *
 * @author PeterBuettner.de
 */

public abstract class OnePixelBorder implements Border {

    /**
     * Returns the insets of the border.
     *
     * @param c the component for which this border insets value applies
     */
    public Insets getBorderInsets(Component c) {

        return new Insets(1, 1, 1, 1);
    }

    /**
     * Reinitialize the insets parameter with this Border's current Insets.
     *
     * @param c      the component for which this border insets value applies
     * @param insets the object to be reinitialized
     */
    public Insets getBorderInsets(Insets insets) {

        insets.left = insets.top = insets.right = insets.bottom = 1;
        return insets;
    }

    public boolean isBorderOpaque() {

        return true;
    }

    public abstract void paintBorder(Component c, Graphics g, int x, int y, int width,
                                     int height);
}