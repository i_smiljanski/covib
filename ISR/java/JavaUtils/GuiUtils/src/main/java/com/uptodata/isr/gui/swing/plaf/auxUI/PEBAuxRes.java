package com.uptodata.isr.gui.swing.plaf.auxUI;

import javax.swing.*;
import java.util.Locale;
import java.util.MissingResourceException;
import java.util.ResourceBundle;


/**
 * Strings for the menus
 * 
 * @author PeterBuettner.de
 *  
 */
class PEBAuxRes {

    private static ResourceBundle bundle;

    /**
     * The resourcebundle, read on first access, uses the default locale, if
     * locale changed: reread it
     * 
     * @return
     */
    static ResourceBundle getBundle() {

        if ( bundle != null ) {
            if ( bundle.getLocale().equals( Locale.getDefault() ) ) return bundle;
        }

        if ( bundle == null ) bundle = ResourceBundle.getBundle( PEBAuxRes.class.getName() );
        return bundle;
    }

    private static String getString( ResourceBundle bundle, String key ) {

        try {
            return bundle.getString( key );
        } catch ( MissingResourceException e ) {
           e.printStackTrace();
        }
        return null;
    }

    static void initActionFromKey( String resName, Action action ) {

        initActionFromKey( getBundle(), resName, action );
    }

    private static void initActionFromKey( ResourceBundle bundle, String resName, Action a ) {

        a.putValue( Action.NAME, getString( bundle, resName + ".name" ) );
        String ms = getString( bundle, resName + ".mnemonic" )  + "\0";
        a.putValue( Action.MNEMONIC_KEY, new Integer( ms.charAt( 0 ) ) );
    }

}