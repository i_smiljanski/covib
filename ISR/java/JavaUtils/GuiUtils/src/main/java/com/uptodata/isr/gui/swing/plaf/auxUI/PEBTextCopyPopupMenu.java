package com.uptodata.isr.gui.swing.plaf.auxUI;

import javax.swing.*;
import javax.swing.text.DefaultEditorKit;
import javax.swing.text.JTextComponent;
import java.awt.*;
import java.awt.datatransfer.DataFlavor;
import java.awt.datatransfer.Transferable;
import java.awt.event.ActionEvent;


/**
 * A popupmenu to be used with <code>JTextComponent</code> s to
 * copy/cut/paste/selectAll. Preconditions: the target reacts on the actions
 * used (see below), from DefaultEditorKit <br>
 * Nearly no errorchecking! Just to show the basic principle. <br>
 * I18n is left as an exercise for the reader:-) <br>
 * I18n-Draft: put Actions (with text, mnemonic and icon) into an accessible
 * place (UIManager?) and read this designinfo from there.
 * 
 * @author PeterBuettner.de
 *  
 */
class PEBTextCopyPopupMenu extends JPopupMenu {

    private void addMenuItem( JComponent newSrc, String actionName, String menuText,
                             boolean enabled ) {

        // wrap the original action, to do: naming/enabling/eventSource
        Action wrapper = new ResAction( newSrc, actionName, menuText );
        wrapper.setEnabled( enabled );
        add( new JMenuItem( wrapper ) );
    }

    private static class ResAction extends AbstractAction {

        private final String     actionName;
        private final JComponent newSrc;

        ResAction( final JComponent newSrc, String actionName, String resKey ) {

            this.actionName = actionName;
            this.newSrc = newSrc;
            PEBAuxRes.initActionFromKey( resKey, this );
        }

        public void actionPerformed( ActionEvent e ) {

            ActionMap am = newSrc.getActionMap();
            if ( am == null ) return;
            Action delegate = am.get( actionName );

            if ( delegate == null ) return;
            delegate.actionPerformed( new ActionEvent( newSrc, e.getID(), actionName ) );
        }
    }

    public PEBTextCopyPopupMenu( JTextComponent target ) {

        int start = target.getSelectionStart();
        int end = target.getSelectionEnd();
        boolean emptySel = start == end;
        boolean allSelected = ( start == 0 ) && ( end == target.getDocument().getLength() );

        // maybe later we look for html on a JEditorPane with html content too
        boolean canPaste = false;
        try {
            Transferable t = getTransferable();
            canPaste = t
                .isDataFlavorSupported( new DataFlavor( "text/plain; class=java.lang.String" ) );
        } catch ( ClassNotFoundException e ) {
            e.printStackTrace();
        }

        boolean canEdit = target.isEditable();

        // 'they' don't allow to copy text from such one
        boolean isPasswd = ( target instanceof JPasswordField );

        addMenuItem( target, DefaultEditorKit.cutAction, "cut", !emptySel && canEdit
                                                                && !isPasswd );
        addMenuItem( target, DefaultEditorKit.copyAction, "copy", !emptySel && !isPasswd );
        addMenuItem( target, DefaultEditorKit.pasteAction, "paste", canPaste && canEdit );
        addMenuItem( target, DefaultEditorKit.deleteNextCharAction, "delete", !emptySel
                                                                              & canEdit );
        add( new JSeparator() );
        addMenuItem( target, DefaultEditorKit.selectAllAction, "selectAll", !allSelected );

    }

    /**
     * tries 'Webstart-Clipboard'(Oh no: it doesn't work) Just the
     * System-Clipboard one
     * 
     * @return
     */
    private Transferable getTransferable() {

        try {

            return Toolkit.getDefaultToolkit().getSystemClipboard().getContents( null );

        } catch ( Exception e ) {
            e.printStackTrace();
        } // permisson etc.

        return null;

    }

}