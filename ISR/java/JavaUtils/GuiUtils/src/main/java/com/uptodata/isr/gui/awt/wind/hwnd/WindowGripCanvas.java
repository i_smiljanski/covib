package com.uptodata.isr.gui.awt.wind.hwnd;

import java.awt.*;


public class WindowGripCanvas extends Canvas implements IWindowGripCanvas {

    private int windowHandle;

    static {

        System.loadLibrary( "WindowGripCanvas" );
        initFields();

    }

    public native void paint( Graphics g );

    private static native void initFields();

    public int getWindowHandle() {

        return windowHandle;
    }

    public native void setWindowPos( int hwnd, int hwndAfter, int x, int y, int width,
                                    int height, int flags );

}