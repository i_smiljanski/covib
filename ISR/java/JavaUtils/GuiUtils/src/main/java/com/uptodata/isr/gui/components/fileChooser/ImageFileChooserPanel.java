package com.uptodata.isr.gui.components.fileChooser;

import javax.swing.*;
import java.io.File;

/**
 * @author Inna Smiljanski  created 04.07.13
 */
public class ImageFileChooserPanel extends JPanel {
    private JFileChooser fc;
    private String filterMsg;
    private String title;

    public ImageFileChooserPanel(String title, String filterMsg) {
        this.filterMsg = filterMsg;
        this.title = title;
    }

    public File chooseFile() {
        //Set up the file chooser.
        if (fc == null) {
            fc = new JFileChooser();

            //Add a custom file filter and disable the default
            //(Accept All) file filter.
            fc.addChoosableFileFilter(new ImageFilter());
            fc.setAcceptAllFileFilterUsed(true);

            //Add custom icons for file types.
            fc.setFileView(new ImageFileView());

            //Add the preview pane.
            fc.setAccessory(new ImagePreview(fc));
        }

        //Show it.
        int returnVal = fc.showDialog(this, title);

        File file = null;
        //Process the results.
        if (returnVal == JFileChooser.APPROVE_OPTION) {
            file = fc.getSelectedFile();
        } else {
            System.out.println("Attachment cancelled by user");
        }


        //Reset the file chooser for the next time it's shown.
        fc.setSelectedFile(null);
        return file;
    }
}
