package com.uptodata.isr.gui.util;

import com.jgoodies.forms.util.LayoutStyle;

import java.awt.*;


/**
 * Wrapper (and corrector) for jGoodies FormLayout sizes, to get pixelSizes from
 * there more easy.
 *
 * @author PeterBuettner.de
 */
public class PixelSizes {

    public static int getButtonBarGapY(Component c) {

        return LayoutStyle.getCurrent().getUnrelatedComponentsPadY().getPixelSize(c);

    }

}