package com.uptodata.isr.gui.lookAndFeel;


import com.uptodata.isr.gui.util.Colors;

import javax.swing.*;
import java.awt.*;


/**
 * Lend from com.sun.java.swing.plaf.windows.WindowsTreeUI, but enhanced: uses
 * UI-Colors (not hardcoded), paints area behind the +/-, so full-row-selected
 * trees looking good.
 *
 * @author PeterBuettner.de
 */
class TreeIcon implements Icon {

    private static final int SIZE = 9;
    private static final int HALF_SIZE = 4;
    private boolean expanded;

    public TreeIcon(boolean expanded) {

        this.expanded = expanded;

    }

    public void paintIcon(Component c, Graphics g, int x, int y) {

        g.setColor(Colors.getUIWindow());
        g.fillRect(x, y, SIZE - 1, SIZE - 1);
        g.setColor(Colors.getUIControlShadow());
        g.drawRect(x, y, SIZE - 1, SIZE - 1);
        g.setColor(Colors.getUIControlText());
        g.drawLine(x + 2, y + HALF_SIZE, x + (SIZE - 3), y + HALF_SIZE);
        if (!expanded) g.drawLine(x + HALF_SIZE, y + 2, x + HALF_SIZE, y + (SIZE - 3));

    }

    public int getIconWidth() {

        return SIZE;

    }

    public int getIconHeight() {

        return SIZE;

    }

}