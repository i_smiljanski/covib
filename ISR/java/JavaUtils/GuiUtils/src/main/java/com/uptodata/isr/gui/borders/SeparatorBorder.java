package com.uptodata.isr.gui.borders;

import javax.swing.*;
import javax.swing.border.Border;
import javax.swing.border.EmptyBorder;
import java.awt.*;


/**
 * Like an EtchedBorder with etches only on one (some) sides, uses Colors from
 * JSeparator, maybe changed to look exactly like a separator (on each choosen
 * side), sonce a Look and Feel may paint separators not like an etch, may look
 * <b>ugly </b> if border is on more than one side for now
 *
 * @author Peter
 */
public class SeparatorBorder implements Border {

    private boolean top;
    private boolean left;
    private boolean bottom;
    private boolean right;

    private static int SIZE = 2;

    /**
     * Has a Separator on ...
     *
     * @param top
     * @param left
     * @param bottom
     * @param right
     */
    public SeparatorBorder(boolean top, boolean left, boolean bottom, boolean right) {

        this.top = top;
        this.left = left;
        this.bottom = bottom;
        this.right = right;
    }

    /**
     * Separator just on one side, see SwingConstants (TOP , LEFT , BOTTOM,
     * RIGHT), paints nothing if illegal pos
     *
     * @param pos
     */
    public SeparatorBorder(int pos) {

        top = (pos == SwingConstants.TOP);
        left = (pos == SwingConstants.LEFT);
        bottom = (pos == SwingConstants.BOTTOM);
        right = (pos == SwingConstants.RIGHT);
    }

    public void paintBorder(Component c, Graphics g, int x, int y, int width, int height) {

        /*
         * with separator: sep = new JSeparator(); // only one sep for all
         * borders, 'Flywight' sep.setSize(width); sep.setLocation(0,0);
         * sep.getUI().paint(g,sep);
         *  
         */

        int w = width;
        int h = height;

        g.translate(x, y);

        // 'shadow' e.g. top line of top
        g.setColor(UIManager.getColor("Separator.foreground"));
        //		g.drawRect(0, 0, w - 2, h - 2);
        if (top) g.drawLine(0, 0, w, 0);
        if (left) g.drawLine(0, 0, 0, h);
        if (bottom) g.drawLine(0, h - 2, w, h - 2);
        if (right) g.drawLine(w - 2, 0, w - 2, h);

        // 'hilite' e.g. bottom line of top
        g.setColor(UIManager.getColor("Separator.background"));
        if (top) g.drawLine(0, 1, w, 1);
        if (left) g.drawLine(1, 0, 1, h);
        if (bottom) g.drawLine(0, h - 1, w, h - 1);
        if (right) g.drawLine(w - 1, 0, w - 1, h);

        //		g.drawLine(1, h - 3, 1, 1);
        //		g.drawLine(1, 1, w - 3, 1);
        //
        //		g.drawLine(0, h - 1, w - 1, h - 1);
        //		g.drawLine(w - 1, h - 1, w - 1, 0);

        g.translate(-x, -y);
    }

    public Insets getBorderInsets(Component c) {

        Insets insets = null;

        if (insets == null) insets = new Insets(0, 0, 0, 0);
        insets.left = left ? SIZE : 0;
        insets.top = top ? SIZE : 0;
        insets.right = right ? SIZE : 0;
        insets.bottom = bottom ? SIZE : 0;
        return insets;
    }

    public boolean isBorderOpaque() {

        return true;
    }

    public static void main(String[] args) {

        JFrame f = new JFrame("Testing SeparatorBorder");
        f.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        JPanel p = new JPanel();
        p.setBorder(new EmptyBorder(5, 5, 5, 5));

        JPanel pnl = new JPanel(new GridLayout(0, 2, 10, 10));
        for (int i = 0; i < 4; i++) {
            JLabel l = new JLabel("        X      ");
            l.setBorder(new SeparatorBorder(i == 0, i == 1, i == 2, i == 3));
            pnl.add(l);
        }

        for (int i = 0; i < 4; i++) {
            JLabel l = new JLabel("        X      ");
            l
                    .setBorder(new SeparatorBorder(i % 2 == 0, i % 2 == 0, i % 2 == 1,
                            i % 2 == 1));
            pnl.add(l);
        }

        p.add(pnl);

        f.getContentPane().add(p);

        f.pack();
        f.setVisible(true);
    }
}

