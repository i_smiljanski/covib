package com.uptodata.isr.gui.lookAndFeel;

import com.jidesoft.plaf.LookAndFeelFactory;
import com.uptodata.isr.gui.util.Colors;
import org.apache.commons.lang.StringUtils;

import javax.swing.*;
import javax.swing.border.Border;
import javax.swing.border.EmptyBorder;
import javax.swing.border.LineBorder;
import javax.swing.plaf.InsetsUIResource;
import java.awt.*;

/**
 * Some changes th the (Windows) Look and Feel
 *
 * @author PeterBuettner.de
 */
public class TweakUI {


    /**
     * sets the Look &amp; Feel from a list
     */
    public static void setLookAndFeel() {

        String userLAF = System.getProperty("user.laf");
        if (StringUtils.isEmpty(userLAF)){
            userLAF = System.getProperty("jnlp.user.laf");
        }

        String[] lf = {
                userLAF
                , "OfficeBlue2007"
                , "OfficeBlack2007"
                , "OfficeSilver2007"
                , "Mariner"
                , "DustCoffee"
        };

        for (int i = 0; i < lf.length; i++) {

            try {

                String layoutName = lf[i];
                UIManager.setLookAndFeel("org.pushingpixels.substance.api.skin.Substance" + layoutName + "LookAndFeel");
                LookAndFeelFactory.installJideExtension();
                JDialog.setDefaultLookAndFeelDecorated(true);

//                try {
//                    UIDefaults defaults = UIManager.getDefaults();
//                    SortedMap sortedSystemProperties = new TreeMap(defaults);
//                    Set keySet = sortedSystemProperties.keySet();
//                    Iterator iterator = keySet.iterator();
//                    while (iterator.hasNext()) {
//                        String key = (String) iterator.next();
//                        log.debug(key + " = " + defaults.get(key).toString());
//                    }
//                } catch (Exception e) {
//                    log.error("Can not show uidefaults of " + layoutName + ": " + e.getMessage());
//                }

                String[] classes = "BorderSplitPane,Tree,Table,List".split(",");
                for (int j = 0; j < classes.length; j++) {
                    UIManager.put(classes[j] + ".background", Colors.getUIWindow());
                }
                UIManager.put("Tree.textBackground", Colors.getUIWindow());
                UIManager.put("BorderSplitPane.borderColor", UIManager.getColor("RootPane.background"));
                // use this if your colors are not bright
                //UIManager.put(SubstanceLookAndFeel.COLORIZATION_FACTOR, 1.0);

                break;

            } catch (Exception e) {
                e.printStackTrace();
            }

        }
    }

    public static void setFrameDecoration(JFrame frame) {
        if (UIManager.getLookAndFeel().getSupportsWindowDecorations()) {
            frame.setUndecorated(true);
            frame.getRootPane().setWindowDecorationStyle(JRootPane.FRAME);
        } else {
            frame.setUndecorated(false);
        }
    }

    public static void setDialogDecoration(JDialog dialog) {
        if (UIManager.getLookAndFeel().getSupportsWindowDecorations()) {
            dialog.setUndecorated(true);
            dialog.getRootPane().setWindowDecorationStyle(JRootPane.FRAME);
        } else {
            dialog.setUndecorated(false);
        }
    }

    /**
     * Some changes the (Windows) Look and Feel
     * <p>
     * <p>
     * com.sun.java.swing.plaf.windows.WindowsLookAndFeel
     *
     * @param flat          try to look flat (~lite 3D on buttons etc.)
     * @param dynamicLayout
     */
    public static void tweakUI(boolean flat, boolean dynamicLayout) {

        // TODO test for windows L&L, check for XP Flag
        UIManager.put("com.utd.gui.flatStyle", Boolean.valueOf(flat));

        // ask with UIManager.getBoolean("com.utd.gui.flatStyle"); which will
        // return true if value is set and true
        UIManager.put("Viewport.background", Colors.getUIWindow());
        Border emptyBorder = BorderFactory.createEmptyBorder();
        UIManager.put("SplitPane.border", emptyBorder);
        UIManager.put("SplitPaneDivider.border", emptyBorder);

        if (flat) {

            Border flatLineBorder = new LineBorder(Colors.getUIControlShadow(), 1);
            Border flatFieldBorder = new FlatFieldBorder(Colors.getUIControlShadow());
            Border flatButtonBorder = TweakedBorderFactory.getFlatButtonBorderUIResource();

            UIManager.put("Button.border", flatButtonBorder);
            UIManager.put("TabbedPane.darkShadow", Colors.getUIControl());

            //UIManager.put("Separator.foreground", Colors.getUIControl());
            //UIManager.put("Separator.background",
            // UIManager.get("Separator.foreground"));
            //UIManager.put("Separator.background", Colors.getUIControl());
            UIManager.put("TextField.border", flatFieldBorder);
            UIManager.put("ComboBox.border", flatFieldBorder);
            UIManager.put("FormattedTextField.border", flatFieldBorder);
            UIManager.put("PasswordField.border", flatFieldBorder);

            UIManager.put("ProgressBar.border", flatLineBorder);
            UIManager.put("ScrollPane.border", flatLineBorder);
            UIManager.put("Table.scrollPaneBorder", flatLineBorder);

            UIManager.put("ComboBoxUI", FlatWindowsComboBoxUI.class.getName());
            UIManager.put("ScrollBarUI", FlatWindowsScrollBarUI.class.getName());

            UIManager.put("CheckBox.icon", WindowsIconFactory.getCheckBoxIcon());
            UIManager.put("RadioButton.icon", WindowsIconFactory.getRadioButtonIcon());

            InsetsUIResource textFieldMargin = new InsetsUIResource(1, 2, 1, 2);
            UIManager.put("FormattedTextField.margin", textFieldMargin);
            UIManager.put("PasswordField.margin", textFieldMargin);
            UIManager.put("TextField.margin", textFieldMargin);
            UIManager.put("TextArea.margin", textFieldMargin);

            // ... Menu .............................
            UIManager
                    .put("PopupMenu.border", TweakedBorderFactory.getPopupBorderUIResource());

        }

        // let Tableheader look like Windows ListView
        UIManager.put("TableHeader.cellBorder", TweakedBorderFactory
                .getTableHeaderBorder(flat));
        UIManager.put("TableHeaderUI", FlatWindowsTableHeaderUI.class.getName());

        if (!flat) {
            UIManager.put("Table.scrollPaneBorder", UIManager
                    .get("ScrollPane.border"));
        }

        // someone forgot that line below the menubar
        UIManager.put("MenuBar.border", new EmptyBorder(0, 0, 1, 0));

        // some special tweaks:
        UIManager.put("FormattedTextField.font", UIManager.getFont("TextField.font"));

        // open/close comboboxes with alt+ up/down
        InputMap im = (InputMap) UIManager.get("ComboBox.ancestorInputMap");
        if (im != null) {

            im.put(KeyStroke.getKeyStroke("alt DOWN"), "togglePopup");
            im.put(KeyStroke.getKeyStroke("alt UP"), "togglePopup");

        }

        // stop sizing (F8) splitter with <Escape>
        // NONO, not really ok, since it eats ESCAPE also if a parent wants it
        // im = (InputMap)UIManager.get("SplitPane.ancestorInputMap");
        // if(im!=null){
        // 		im.put(KeyStroke.getKeyStroke("ESCAPE"), "toggleFocus");
        // }
        // ... disabled Text components: ..............................
        String[] classes = "PasswordField,TextField,EditorPane,TextArea,FormattedTextField"
                .split(",");
        Color inactiveBack = Colors.getItemHiliteInactiveBack();
        Color inactiveFore = Colors.getItemHiliteInactiveText();

        for (int i = 0; i < classes.length; i++) {

            UIManager.put(classes[i] + ".inactiveForeground", inactiveFore);
            UIManager.put(classes[i] + ".inactiveBackground", inactiveBack);

        }

        // let user read disabled Comboboxes:
        UIManager.put("ComboBox.disabledForeground", UIManager.get("ComboBox.foreground"));

        // [+] [-] Icons with filled background: needed for TreeTable, no matter
        // for JTree, maybe TreeTable should get its own ones (own
        // UIManager-key)
        UIManager.put("Tree.expandedIcon", new TreeIcon(true));
        UIManager.put("Tree.collapsedIcon", new TreeIcon(false));

        Toolkit.getDefaultToolkit().setDynamicLayout(dynamicLayout);

    }

    /*
     * 
     * 
     * Windows L&F "PasswordField.inactiveForeground", InactiveTextColor,
     * "PasswordField.inactiveBackground", ControlBackgroundColor,
     * "TextField.inactiveBackground", ControlBackgroundColor,
     * "TextField.inactiveForeground", InactiveTextColor,
     * 
     * "EditorPane.inactiveForeground", InactiveTextColor,
     * "TextArea.inactiveForeground", InactiveTextColor,
     * 
     * 
     * Basic L&F "FormattedTextField.inactiveForeground",
     * table.get("textInactiveText"), "FormattedTextField.inactiveBackground",
     * table.get("control"), "PasswordField.inactiveForeground",
     * table.get("textInactiveText"), "PasswordField.inactiveBackground",
     * table.get("control"),
     *  
     */
}
