package com.uptodata.isr.gui.lookAndFeel;

import javax.swing.*;
import javax.swing.border.BevelBorder;
import javax.swing.border.Border;
import javax.swing.border.CompoundBorder;
import javax.swing.plaf.BorderUIResource;
import javax.swing.plaf.basic.BasicBorders;
import javax.swing.plaf.basic.BasicGraphicsUtils;
import java.awt.*;


/**
 * Borders for a tweaked Windows Look
 *
 * @author PeterBuettner.de
 */

public class TweakedBorderFactory {

    public static Border getFlatButtonBorderUIResource() {

        return new BorderUIResource.CompoundBorderUIResource(new FlatButtonBorder(),
                new BasicBorders.MarginBorder());
    }

    /**
     * no UIResource
     */
    public static Border getFlatButtonBorder() {

        return new CompoundBorder(new FlatButtonBorder(), new BasicBorders.MarginBorder());
    }

    /**
     * for popup menus, mostly copied from WindowsLookAndFeel -> BasicBorders
     */
    public static Border getPopupBorderUIResource() {

        UIDefaults t = UIManager.getLookAndFeelDefaults();
        return new BorderUIResource.CompoundBorderUIResource(
                new BevelBorder(
                        BevelBorder.RAISED,
                        t
                                .getColor("InternalFrame.borderLight"),
                        t
                                .getColor("InternalFrame.borderHighlight"),
                        t
                                .getColor("InternalFrame.borderShadow"),
                        t
                                .getColor("PopupMenu.background") // table.getColor("InternalFrame.borderDarkShadow"),
                ), BorderFactory
                .createEmptyBorder(1, 1, 1,
                        1));
        //		Colors.getUIControlHighlight(),
        //		Colors.getUIControlLtHighlight(),
        //		Colors.getUIControlShadow(),

    }

    /**
     * create a more Windows Listview like border for TableHeaderCells
     *
     * @param flat
     * @return
     */
    public static Border getTableHeaderBorder(boolean flat) {

        if (!flat) return new TableHeader3DBorder();
        return new FlatTableHeaderBorder(UIManager.getColor("Table.highlight"), UIManager
                .getColor("Table.shadow"));

    }

    private static class FlatTableHeaderBorder implements Border {

        protected Color topLeft;
        protected Color bottomRight;

        public FlatTableHeaderBorder(Color topLeft, Color bottomRight) {

            this.topLeft = topLeft;
            this.bottomRight = bottomRight;

        }

        public void paintBorder(Component c, Graphics g, int x, int y, int width, int height) {

            Color oldColor = g.getColor();

            g.setColor(topLeft);
            g.drawLine(x, y, x + width - 1, y);
            g.drawLine(x, y, x, y + height - 1);

            g.setColor(bottomRight);
            g.drawLine(x, y + height - 1, x + width - 1, y + height - 1);
            g.drawLine(x + width - 1, y, x + width - 1, y + height - 1);

            g.setColor(oldColor);

        }

        public boolean isBorderOpaque() {

            return false;

        }

        public Insets getBorderInsets(Component c) {

            return new Insets(1, 4, 1, 4);

        }

    }

    /**
     * with this border, the tableheader is more Windows ListView sized
     */
    private static class TableHeader3DBorder extends BasicBorders.ButtonBorder {

        public TableHeader3DBorder() {

            this(UIManager.getColor("Table.shadow"), UIManager
                            .getColor("Table.darkShadow"), UIManager.getColor("Table.light"),
                    UIManager.getColor("Table.highlight"));

        }

        public TableHeader3DBorder(Color shadow, Color darkShadow, Color highlight,
                                   Color lightHighlight) {

            super(shadow, darkShadow, highlight, lightHighlight);

        }

        public void paintBorder(Component c, Graphics g, int x, int y, int width, int height) {

            BasicGraphicsUtils.drawBezel(g, x, y, width, height, false, false, shadow,
                    darkShadow, highlight, lightHighlight);

        }

        public Insets getBorderInsets(Component c, Insets insets) {

            insets.top = 1;
            insets.left = 5;
            insets.bottom = 2;
            insets.right = 5;

            return insets;

        }

    }

}