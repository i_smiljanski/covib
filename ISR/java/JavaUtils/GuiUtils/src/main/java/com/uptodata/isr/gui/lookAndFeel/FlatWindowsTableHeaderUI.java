package com.uptodata.isr.gui.lookAndFeel;

import com.sun.java.swing.plaf.windows.WindowsTableHeaderUI;

import javax.swing.*;
import javax.swing.border.Border;
import javax.swing.plaf.ComponentUI;
import java.awt.*;


/**
 * reason: paint a border (only top/left and down 'line') for the last (non
 * existent) headercolumn, looks like Windows ListView
 *
 * @author PeterBuettner.de
 */
public class FlatWindowsTableHeaderUI extends WindowsTableHeaderUI {

    public static ComponentUI createUI(JComponent h) {

        return new FlatWindowsTableHeaderUI();

    }

    public void installUI(JComponent c) {

        super.installUI(c);

    }

    public void paint(Graphics g, JComponent c) {

        Border border = UIManager.getBorder("TableHeader.cellBorder");
        if (border != null) {

            Insets i = border.getBorderInsets(header); // the border 'width'
            // is interessting
            int start = header.getColumnModel().getTotalColumnWidth();
            border.paintBorder(c, g, start, 0, c.getWidth() - start + i.right + i.left + 5, c
                    .getHeight());

        }
        try {
            super.paint(g, c);
        } catch (Exception e) {
            System.err.println("FlatWindowsTableHeaderUI: paint e = " + e);
        }

    }

}