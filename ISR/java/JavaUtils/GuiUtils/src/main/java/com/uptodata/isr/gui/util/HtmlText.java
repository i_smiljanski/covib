package com.uptodata.isr.gui.util;

/**
 * Tools for html text
 *
 * @author PeterBuettner.de
 */
public class HtmlText {

    /**
     * translates the html problematic chars to their entities '&gt;', '&lt;' ,
     * '&amp;' ,'&quot;' <br>
     * returns empty string on null input
     *
     * @param text
     * @return
     */
    public static String toHtml(String text) {

        if (text == null) return "";
        int len = text.length();
        StringBuffer sb = new StringBuffer(len * (110 / 100));

        for (int i = 0; i < len; i++) {

            switch (text.charAt(i)) {

                case '<':
                    sb.append("&lt;");
                    break;
                case '>':
                    sb.append("&gt;");
                    break;
                case '&':
                    sb.append("&amp;");
                    break;
                case '"':
                    sb.append("&quot;");
                    break;
                default:
                    sb.append("" + text.charAt(i));

            }

        }

        return sb.toString();

    } // -----------------------------------------------------------------------------

    public static String toHtml(Object object) {

        return toHtml(String.valueOf(object));

    }

}