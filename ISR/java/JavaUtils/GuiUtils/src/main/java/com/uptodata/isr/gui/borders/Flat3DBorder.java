package com.uptodata.isr.gui.borders;


import com.uptodata.isr.gui.util.Colors;

import java.awt.*;


/**
 * real flat 3D border, only 1px than thosee 2pix eetched ones
 *
 * @author PeterBuettner.de
 */
public class Flat3DBorder extends OnePixelBorder {

    protected Color shadow;
    protected Color highlight;
    private boolean down;

    public Flat3DBorder(boolean down, Color shadow, Color highlight) {

        this.down = down;
        this.shadow = shadow;
        this.highlight = highlight;
    }

    /**
     * UI colors, down
     */
    public Flat3DBorder() {

        this(true);
    }

    /**
     * UI colors
     *
     * @param down
     */
    public Flat3DBorder(boolean down) {

        this(down, Colors.getUIControlShadow(), Colors.getUIControlLtHighlight());
    }

    public void paintBorder(Component c, Graphics g, int x, int y, int width, int height) {

        Color old = g.getColor();
        paint(g, x, y, width, height);
        g.setColor(old);
    }

    private void paint(Graphics g, int x, int y, int w, int h) {

        // topleft, windows paints bottom after top
        g.setColor(down ? shadow : highlight);
        g.drawLine(x, y, x + w - 1, y);
        g.drawLine(x, y, x, y + h - 1);

        // bottomright
        g.setColor(!down ? shadow : highlight);
        g.drawLine(x, y + h - 1, x + w - 1, y + h - 1);
        g.drawLine(x + w - 1, y, x + w - 1, y + h - 1);
    }

}