package com.uptodata.isr.gui.lookAndFeel;

import com.uptodata.isr.gui.borders.DashedBorder;
import com.uptodata.isr.gui.util.Colors;

import javax.swing.border.Border;
import java.awt.*;


/**
 * Better FocusBorder
 *
 * @author PeterBuettner.de
 */
public class WinTreeCellRenderer extends BaseTreeCellRenderer {

    private Border focusBorder = new DashedBorder(Colors.getUITextHighlight(), Colors
            .getUITextHighlightText());

    public WinTreeCellRenderer() {

    }// -----------------------------------------------------------------------------

    protected void paintFocus(Graphics g, int x, int y, int w, int h) {

        focusBorder.paintBorder(this, g, x, y, w, h);

    }

}