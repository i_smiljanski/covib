package com.uptodata.isr.gui.lookAndFeel;

import com.sun.java.swing.plaf.windows.WindowsComboBoxUI;
import com.uptodata.isr.gui.util.Colors;

import javax.swing.*;
import javax.swing.plaf.ComponentUI;
import javax.swing.plaf.basic.BasicArrowButton;


public class FlatWindowsComboBoxUI extends WindowsComboBoxUI {

    public static ComponentUI createUI(JComponent h) {

        return new FlatWindowsComboBoxUI();

    }

    public void installUI(JComponent c) {

        super.installUI(c);

    }

    protected JButton createArrowButton() {

        ButtonColor bc;

        if (LFConst.FULL_FLAT)
            bc = new ButtonColor(UIManager.getColor("ComboBox.buttonDarkShadow"), UIManager
                    .getColor("ComboBox.buttonHighlight"), UIManager
                    .getColor("ComboBox.buttonShadow"));
        else
            bc = new ButtonColor(UIManager.getColor("ComboBox.buttonDarkShadow"), UIManager
                    .getColor("ComboBox.buttonDarkShadow"), UIManager
                    .getColor("ComboBox.buttonShadow"));

        JButton button = new FlatArrowButton(BasicArrowButton.SOUTH, UIManager
                .getColor("ComboBox.buttonBackground"), bc);
        bc.setButton(button);

        if (LFConst.FULL_FLAT)
            button.setBorder(new FlatArrowButtonBorder(Colors.getUIControlShadow(), Colors
                    .getUIControlDkShadow()));
        else
            button.setBorder(new FlatButtonBorder());

        return button;

    }

}