package com.uptodata.isr.gui.util;

import java.awt.*;
import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Properties;


/**
 * @author PeterBuettner.de
 */
public class PropertyTools {

    public static void setRectangle(Properties p, String prefix, Rectangle r) {

        p.setProperty(prefix + ".x", "" + r.x);
        p.setProperty(prefix + ".y", "" + r.y);
        p.setProperty(prefix + ".width", "" + r.width);
        p.setProperty(prefix + ".height", "" + r.height);

    }

    /**
     * reads a rectangle from properties, if one of x,y,width,height is missing
     * or in wrong format then returns null;
     *
     * @param p
     * @param prefix
     * @return
     */
    public static Rectangle getRectangle(Properties p, String prefix) {

        Rectangle r = new Rectangle();
        Integer i;
        i = getInteger(p, prefix + ".y");
        if (i == null) return null;
        r.y = i;
        i = getInteger(p, prefix + ".width");
        if (i == null) return null;
        r.width = i;
        i = getInteger(p, prefix + ".height");
        if (i == null) return null;
        r.height = i;
        i = getInteger(p, prefix + ".x");
        if (i == null) return null;
        r.x = (int) ((Toolkit.getDefaultToolkit().getScreenSize().getWidth() - r.width) / 2);//i.intValue();
        return r;
    }

    /**
     * gets the Integer , or null if not there or not in number format
     *
     * @param p
     * @param name
     * @return
     */
    public static Integer getInteger(Properties p, String name) {

        String value = p.getProperty(name);
        if (value != null) {
            try {
                return Integer.valueOf(value);
            } catch (Exception e) {
                return null;
            }
        }
        return null;
    }

    /**
     * gets the int, or the default def if not there or not in number format
     *
     * @param p
     * @param name
     * @param def
     * @return
     */
    public static int getInt(Properties p, String name, int def) {

        Integer value = getInteger(p, name);
        if (value == null) return def;
        return value;
    }

    // comment, MCD, 13.Jul 2006
    public static void setArrayList(Properties p, String key, java.util.List list) {

        for (int i = 0; i < list.size(); i++) {
            if (list.get(i) != null) {
                System.out.println("PropertyTools.setArrayList: i = " + i + " " + list.get(i).toString());
                p.setProperty(key + "." + i, list.get(i).toString());
            }
        }
    }

    // comment, MCD, 13.Jul 2006
    public static ArrayList getArrayList(Properties p, String key) {

        ArrayList<String> list = new ArrayList<String>();
        for (int i = 0; i < 10; i++) {
            String value = p.getProperty(key + "." + i);
            if (value != null) {
                list.add(value);
            }
        }
        if (list.size() == 0) return null;
        return list;
    }

    /**
     * stores the properties in a byte array, don't throws any Exceptions
     *
     * @param prop
     * @param initialSize of the used ByteArrayOutputStream
     * @return never null, but maybe empty
     */
    public static byte[] toByteArray(Properties prop, int initialSize) {

        ByteArrayOutputStream baos = new ByteArrayOutputStream(initialSize);
        try {
            prop.store(baos, null);
        } catch (IOException e) {
            e.printStackTrace();
        }
        return baos.toByteArray();
    }

    /**
     * loads the properties from a byte array, don't throws any Exceptions
     *
     * @param b
     * @return never null, but maybe empty
     */
    public static Properties fromByteArray(byte[] b) {

        Properties p = new Properties();
        try {
            p.load(new ByteArrayInputStream(b));
        } catch (IOException e) {
            e.printStackTrace();
        }
        return p;
    }

}

