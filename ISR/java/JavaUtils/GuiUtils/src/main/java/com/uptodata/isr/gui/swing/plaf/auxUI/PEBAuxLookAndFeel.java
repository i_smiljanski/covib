package com.uptodata.isr.gui.swing.plaf.auxUI;

import javax.swing.*;


/**
 * Example Auxilliary Look and Feel that adds popup-menus to JTextComponents,
 * and also removes a bug in Jre/Jdk 1.4 which gives Scrollbars in JScrollPanes
 * focus.
 * 
 * @author PeterBuettner.de
 */
public class PEBAuxLookAndFeel extends LookAndFeel {

    public String getName() {

        return "PeterBuettner.de: example Aux L&F";
    }

    public String getID() {

        return getID();
    }

    public String getDescription() {

        return getID();
    }

    public boolean isNativeLookAndFeel() {

        return false;
    }

    public boolean isSupportedLookAndFeel() {

        return true;
    }

    public UIDefaults getDefaults() {

        UIDefaults table = new UIDefaults() {

            protected void getUIError( String msg ) {

            }// eat error msg
        };
        table.put( "ScrollPaneUI", PEBAuxScrollPaneUI.class.getName() );
        table.put( "TextAreaUI", PEBAuxTextUI.class.getName() );
        table.put( "EditorPaneUI", PEBAuxTextUI.class.getName() );
        table.put( "TextFieldUI", PEBAuxTextUI.class.getName() );
        table.put( "PasswordFieldUI", PEBAuxTextUI.class.getName() );
        table.put( "FormattedTextFieldUI", PEBAuxTextUI.class.getName() );
        return table;

    }
}