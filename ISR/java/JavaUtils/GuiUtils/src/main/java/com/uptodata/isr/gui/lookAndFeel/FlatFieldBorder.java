package com.uptodata.isr.gui.lookAndFeel;

import javax.swing.border.AbstractBorder;
import javax.swing.plaf.UIResource;
import javax.swing.text.JTextComponent;
import java.awt.*;


/**
 * Flat border for <code>JTextComponent</code>s, copied from
 * javax.swing.plaf.basic.BasicBorders, changed to be flat
 *
 * @author PeterBuettner.de
 */
public class FlatFieldBorder extends AbstractBorder implements UIResource {

    protected Color lineColor;

    public FlatFieldBorder(Color lineColor) {

        this.lineColor = lineColor;
    }

    public void paintBorder(Component c, Graphics g, int x, int y, int width, int height) {

        Color oldColor = g.getColor();
        g.setColor(lineColor);
        g.drawRect(x, y, width - 1, height - 1);
        g.setColor(oldColor);

    }

    public Insets getBorderInsets(Component c) {

        return getBorderInsets(c, new Insets(0, 0, 0, 0));

    }

    public Insets getBorderInsets(Component c, Insets insets) {

        Insets margin = (c instanceof JTextComponent)
                ? ((JTextComponent) c).getMargin()
                : null;

        int d = 1;

        if (margin == null) {

            insets.left = insets.top = insets.right = insets.bottom = d;

        } else {

            insets.top = d + margin.top;
            insets.left = d + margin.left;
            insets.bottom = d + margin.bottom;
            insets.right = d + margin.right;

        }

        return insets;

    }

}