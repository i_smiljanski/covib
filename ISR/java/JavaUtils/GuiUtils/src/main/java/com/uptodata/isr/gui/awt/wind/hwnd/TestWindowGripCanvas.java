package com.uptodata.isr.gui.awt.wind.hwnd;

import javax.swing.*;
import java.awt.*;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;


public class TestWindowGripCanvas {

    public static void main( String[] args ) {

        JFrame f = new JFrame( "Title" );
        f.setDefaultCloseOperation( JFrame.EXIT_ON_CLOSE );
        f.setBackground( new Color( 255, 200, 200 ) );
        f.getContentPane().setLayout( null );
        f.setBounds( 30, 30, 300, 200 );

        final WindowGripCanvas can = new WindowGripCanvas();
        can.setBackground( new Color( 200, 200, 255 ) );
        f.getContentPane().add( can );
        can.setBounds( 0, 0, 10, 10 );

        f.setVisible(true);

        f.addMouseListener( new MouseAdapter() {

            public void mousePressed( MouseEvent e ) {

                can.setVisible( false );
                can
                    .setWindowPos(
                                   can.getWindowHandle(),
                                   0,
                                   10,
                                   10,
                                   400,
                                   500,
                                   IWindowGripCanvas.SWP_NOACTIVATE
                                                                                                                                            | IWindowGripCanvas.SWP_NOZORDER );
            }

        } );

    }

}