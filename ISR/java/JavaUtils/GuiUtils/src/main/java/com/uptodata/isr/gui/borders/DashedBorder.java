package com.uptodata.isr.gui.borders;

import javax.swing.plaf.basic.BasicGraphicsUtils;
import java.awt.*;


/**
 * a better focus border for windows L&F, since the sun implementation assumes a
 * 'Selected Color' as background, but unselected list/table/tree items may have
 * focus. Example usage: new
 * DashedBorder(list.getSelectionBackground(),list.getBackground())
 */
public class DashedBorder extends OnePixelBorder {

    private Color dashColor;
    private Color backColor;

    public DashedBorder(Color color1, Color color2) {

        backColor = color1;
        dashColor = color2;
    }

    public void paintBorder(Component c, Graphics g, int x, int y, int width, int height) {

        Color oldColor = g.getColor();

        g.setColor(backColor);
        g.drawRect(x, y, width - 1, height - 1);

        g.setColor(dashColor);
        BasicGraphicsUtils.drawDashedRect(g, x, y, width, height);

        g.setColor(oldColor);
    }
}