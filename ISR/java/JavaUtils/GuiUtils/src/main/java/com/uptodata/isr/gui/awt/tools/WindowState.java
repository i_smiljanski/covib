package com.uptodata.isr.gui.awt.tools;

import java.awt.*;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;
import java.io.Serializable;

/**
 * Helper to make State and Bounds of a Frame/Dialog/Window persistant, usage
 * fragment:
 *
 * <pre>
 *
 *  public YourFrame(){
 *  	super(&quot;My Frame&quot;);
 *  	readStatus();
 *  	addWindowListener(new WindowAdapter() {
 *  		public void windowClosing(WindowEvent e) { saveStatus();}
 *  		});
 *  	options.getMainWindowState().putWindowStateListener(this);
 *  	...
 *  	options.getMainWindowState().restore(this);
 *  	doLayout();
 *  	show();
 *  ...
 *  }
 *
 *
 *  private void saveStatus(){
 *  	options.getMainWindowState().save(this);
 *  	...
 *  	}
 *
 * </pre>
 *
 * Note that extendedState is only for Frame
 *
 * @author PeterBuettner.de
 */
public class WindowState implements Serializable {

    private Rectangle bounds = new Rectangle(10, 10, 600, 400);
    private int state = javax.swing.JFrame.NORMAL;

    public WindowState() {

    }

    public WindowState(Rectangle bounds, int state) {

        this.bounds = new Rectangle(bounds);
        this.state = state;
    }

    /**
     * Reads the state and bounds from the frame, bounds only if not maximized,
     * so the former saved bounds is not
     * overwritten. Tested on Windows2k. Note that Dialog and Window doesn't
     * have an ExtendedState, only Frame!
     *
     * @param window
     */
    public void save(Window window) {

        if (window instanceof Frame) {
            state = ((Frame) window).getExtendedState();
        }
        if ((state & Frame.MAXIMIZED_BOTH) == 0) {
            bounds = window.getBounds();
        }
    }

    /**
     * Sets the state and bounds of the frame.
     *
     * @param window
     */
    public void restore(Window window) {
        if (bounds.getX() < 0 || bounds.getY() < 0) {
            System.out.println("bounds = " + bounds);
            // window.setLocationRelativeTo(null);
            final Dimension d = Toolkit.getDefaultToolkit().getScreenSize();
            window.setSize(800, 500);
            window.setLocation((int) ((d.getWidth() - window.getWidth()) / 2), (int) ((d.getHeight() - window.getHeight()) / 2));
        } else {
            window.setBounds(bounds);
        }
        if (window instanceof Frame) {
            ((Frame) window).setExtendedState(state);
        }
    }

    /**
     * use this to catch state-changes, since after maximize the 'old' position
     * is not reachable anymore, to save the State into any persistance layer
     * use to get it back.
     *
     * @param window
     */
    public void putWindowStateListener(final Window window) {

        window.addWindowStateListener(new WindowAdapter() {

            public void windowStateChanged(WindowEvent e) {

                // save this now, since bounds are maximized if state is
                // maximized
                if (e.getNewState() == Frame.NORMAL
                        && (e.getOldState() & Frame.MAXIMIZED_BOTH) != 0) {
                    bounds = window
                            .getBounds();
                }
            }
        });
    }

    public Rectangle getBounds() {

        return new Rectangle(bounds);
    }

    public void setBounds(Rectangle bounds) {

        this.bounds = new Rectangle(bounds);/* copy */

    }

    public int getState() {

        return state;
    }

    public void setState(int state) {

        this.state = state;
    }

}
