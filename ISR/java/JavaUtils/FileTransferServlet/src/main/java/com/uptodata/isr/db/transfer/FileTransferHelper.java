package com.uptodata.isr.db.transfer;

import oracle.jdbc.OracleCallableStatement;
import oracle.sql.ANYDATA;
import org.apache.commons.collections4.map.CaseInsensitiveMap;
import org.apache.commons.lang.StringUtils;

import javax.servlet.http.HttpServletRequest;
import java.sql.Connection;
import java.sql.DriverManager;
import java.util.ArrayList;
import java.util.Enumeration;
import java.util.HashMap;

/**
 * Created by schroedera85 on 17.12.2015.
 */
public class FileTransferHelper {

    public static ANYDATA getDataForDownload(HttpServletRequest request) throws Exception {
        CaseInsensitiveMap<String, String> requestParameters = getRequestParameters(request);
        String configName = requestParameters.get("configName");
        if (StringUtils.isEmpty(configName)) {
            throw new Exception("Parameter configName is not set!");
        }

        String jobId = requestParameters.get("jobId");
        if (StringUtils.isEmpty(jobId)) {
            throw new Exception("Parameter jobId is not set!");
        }

        HashMap<String, String> tableProperties = getTablePropertiesByConfigName(configName);

        String targetTable = tableProperties.get("targetTable");
        if (StringUtils.isEmpty(targetTable)) {
            throw new Exception("There is no target table name for config " + configName);
        }
        String targetTablePk = tableProperties.get("targetTablePk");
        if (StringUtils.isEmpty(targetTablePk)) {
            throw new Exception("There is no primary keys for config " + configName);
        }

        String delimiter = tableProperties.get("key_delimiter");
        if (delimiter == null) {
            throw new Exception("There is no delimiter for config " + configName);
        }
        ArrayList<String> primaryKeys = getTablePrimaryKeys(targetTablePk, delimiter);

        String dataType = tableProperties.get("dataType");
        StringBuilder statementBuilder = new StringBuilder();
        String fileColumn;
        if (dataType.equalsIgnoreCase("B")) {
            statementBuilder.append("select anydata.convertBlob(?)");
            fileColumn = tableProperties.get("blobColumn");
        } else if (dataType.equalsIgnoreCase("C")) {
            statementBuilder.append("select anydata.convertClob(?)");
            fileColumn = tableProperties.get("clobColumn");
        } else {
            throw new Exception("Can not evaluate type of file column for config " + configName);
        }
        statementBuilder.append(" from ? ").append(" where ");
        for (String primaryKey : primaryKeys) {
            statementBuilder.append(primaryKey).append("=? and ");
        }

        int statementLength = statementBuilder.length();
        statementBuilder.delete(statementLength - 5, statementLength);// remove last 'and'
        String statementString = statementBuilder.toString();
        OracleCallableStatement statement = null;
        Connection conn = null;
        ANYDATA anydt;
        try {
            conn = DriverManager.getConnection("jdbc:default:connection");

            statement = (OracleCallableStatement) conn.prepareCall(statementString);
            statement.setString(1, fileColumn);
            statement.setString(2, targetTable);
            int counter = 3;
            for (String primaryKey : primaryKeys) {
                String keyValue = requestParameters.get(primaryKey);
                if (keyValue == null) {
                    throw new Exception("Value of key " + primaryKey + " is not set! Config name " + configName);
                }
                statement.setString(counter, keyValue);
                counter++;
            }

            statement.executeQuery();
            anydt = (ANYDATA) statement.getObject(1);
        } finally {
            if (statement != null) {
                statement.close();
            }

        }

        return anydt;
    }

    public static HashMap<String, String> getTablePropertiesByConfigName(String configName) throws Exception {
        String statementString = "select targetTable, targetTablePk, blobColumn, clobColumn, key_delimiter,  regexp_count(targetTablePk, key_delimiter,1)  nDelimiters," +
                "    case when blobcolumn is not null then 'B' when clobcolumn is not null then 'C' else 'F' end as dataType" +
                "    from   isr$transfer$file$config " +
                "   where  configname = ?;";
        Connection conn = null;
        OracleCallableStatement statement = null;
        HashMap<String, String> result;
        try {
            conn = DriverManager.getConnection("jdbc:default:connection");
            statement = (OracleCallableStatement) conn.prepareCall(statementString);
            statement.setString(1, configName);
            statement.executeQuery();

            String targetTable = statement.getString("targetTable");
            String targetTablePk = statement.getString("targetTablePk");
            String blobColumn = statement.getString("blobColumn");
            String clobColumn = statement.getString("clobColumn");
            String key_delimiter = statement.getString("key_delimiter");
            String dataType = statement.getString("dataType");

            result = new HashMap<String, String>();
            result.put("targetTable", targetTable);
            result.put("targetTablePk", targetTablePk);
            result.put("blobColumn", blobColumn);
            result.put("clobColumn", clobColumn);
            result.put("key_delimiter", key_delimiter);
            result.put("dataType", dataType);
        } finally {
            if (statement != null) {
                statement.close();
            }
        }

        return result;

    }

    public static ArrayList<String> getTablePrimaryKeys(String keysString, String delimiter) {
        ArrayList<String> result = new ArrayList<String>();
        String[] keys = keysString.split(delimiter);
        for (String key : keys) {
            result.add(key.trim());
        }
        return result;
    }

    public static String getRequestParameter(HttpServletRequest request, String parameter) {
        for (Enumeration en = request.getParameterNames(); en.hasMoreElements(); ) {
            String reqParamName = (String) en.nextElement();
            if (reqParamName.equalsIgnoreCase(parameter)) {
                return request.getParameter(reqParamName);
            }
        }
        return null;
    }

    public static CaseInsensitiveMap<String, String> getRequestParameters(HttpServletRequest request) {
        CaseInsensitiveMap<String, String> result = new CaseInsensitiveMap<String, String>();
        for (Enumeration en = request.getParameterNames(); en.hasMoreElements(); ) {
            String parameterName = (String) en.nextElement();
            String parameterValue = request.getParameter(parameterName);
            result.put(parameterName, parameterValue);
        }
        return result;
    }
}
