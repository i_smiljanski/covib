package com.uptodata.isr.db.transfer.fileupload;

import org.apache.commons.fileupload.disk.DiskFileItem;
import org.apache.commons.io.output.DeferredFileOutputStream;

import java.io.*;

/**
 * Created by smiljanskii60 on 04.05.2015.
 */
public class StreamFileItem extends DiskFileItem {
    private ByteArrayOutputStream outputStream;

    public StreamFileItem(String fieldName, String contentType, boolean isFormField, String fileName, int sizeThreshold, File repository) {
        super(fieldName, contentType, isFormField, fileName, sizeThreshold, repository);
        outputStream = new ByteArrayOutputStream();
    }


    public void write(File file) throws Exception {
        FileOutputStream stream = new FileOutputStream(file);
        try {
            stream.write(get());
        } finally {
            stream.close();
        }
    }

    public byte[] get() {
        return outputStream.toByteArray();
    }

    public InputStream getInputStream() throws IOException {
        return new ByteArrayInputStream(get());
    }

    public OutputStream getOutputStream() throws IOException {
        return  outputStream;
    }
}
