package com.uptodata.isr.db.transfer;

import com.uptodata.isr.db.DbConvertUtils;
import com.uptodata.isr.db.connection.ConnectionUtil;
import com.uptodata.isr.db.dbservlets.ServletUtil;
import com.uptodata.isr.db.trace.DbLogging;
import com.uptodata.isr.db.transfer.fileupload.StreamFileItemFactory;
import com.uptodata.isr.utils.ConvertUtils;
import com.uptodata.isr.utils.exceptions.ExceptionUtils;
import com.uptodata.isr.utils.exceptions.MessageStackException;
import oracle.jdbc.OracleCallableStatement;
import oracle.jdbc.OracleTypes;
import oracle.sql.ANYDATA;
import oracle.sql.Datum;
import oracle.sql.TypeDescriptor;
import org.apache.commons.fileupload.FileItem;
import org.apache.commons.fileupload.FileItemFactory;
import org.apache.commons.fileupload.servlet.ServletFileUpload;
import org.apache.commons.io.output.FileWriterWithEncoding;
import org.apache.commons.lang.StringUtils;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.io.Writer;
import java.sql.Blob;
import java.sql.Clob;
import java.sql.Connection;
import java.sql.SQLException;
import java.util.Arrays;
import java.util.Enumeration;
import java.util.List;

import static com.uptodata.isr.db.DbConvertUtils.checkError;


/**
 * Created by smiljanskii60 on 11.03.2015.
 */
public class FileTransferServlet extends ServletUtil {

    private String errorMsg;
    private int errorCode = HttpServletResponse.SC_INTERNAL_SERVER_ERROR;
    private Connection conn;

    public void doPost(HttpServletRequest req, HttpServletResponse resp)
            throws java.io.IOException {
        DbLogging log = new DbLogging();
        initLog(log, req);
        log.debug("request",  httpServletRequestToString(req));

        String servletPath = req.getRequestURI();
        log.stat("begin", "getCharacterEncoding = " + req.getCharacterEncoding());

        try {
            if (checkServletPath(servletPath, "upload")) {
                String transferKeyId = getRequestRequiredParameter(req, "transferKeyId");
                String configName = getRequestRequiredParameter(req, "configName");

                log.debug("transferKeyId", transferKeyId);
                log.debug("configName", configName);

                FileItemFactory factory = new StreamFileItemFactory();
                ServletFileUpload servletFileUpload = new ServletFileUpload(factory);
                List<FileItem> files;
                try {
                    files = servletFileUpload.parseRequest(req);
                } catch (Exception e) {
                    log.error("error", e);
                    throw e;
                }

                if (StringUtils.isNotEmpty(transferKeyId) || StringUtils.isNotEmpty(configName)) {
                    log.debug("vor upload", "");
                    upload(configName, transferKeyId, files.get(0), log);

                } else {
                    errorMsg = "servlet parameters configName or (and) transferKeyId are unknown!";
                    errorCode = HttpServletResponse.SC_NOT_FOUND;  //todo: ?
                    resp.getWriter().println(errorMsg);
                    throw new Exception(errorMsg);
                }
            } else {
                errorMsg = "servlet path " + servletPath + " is unknown!";
                errorCode = HttpServletResponse.SC_METHOD_NOT_ALLOWED;  //?
                resp.getWriter().println(errorMsg);
                throw new Exception(errorMsg);
            }
            log.stat("end", "end");
            resp.getWriter().close();
        } catch (Exception e) {
            log.error("error", e);
            e.printStackTrace();
            resp.setStatus(errorCode);
            resp.getWriter().println(errorMsg);
        } catch (OutOfMemoryError e) {
            errorMsg = ExceptionUtils.formatVirtualMachineError(e);
            log.error("error", errorMsg);
            resp.getWriter().println(errorMsg);
        } finally {
            log.debug("end","end");
            resp.getWriter().close();
        }
    }


    private String httpServletRequestToString(HttpServletRequest request) {
        StringBuilder sb = new StringBuilder();

        sb.append("Request Method = [").append(request.getMethod()).append("], ");
        sb.append("Request URI Path = [").append(request.getRequestURI()).append("], ");
        sb.append("Request cookies = [").append(Arrays.toString(request.getCookies())).append("] ");
        sb.append("Request remote addr = [").append(request.getRemoteAddr()).append("] ");
        sb.append("Request remote user = [").append(request.getRemoteUser()).append("]\n");

        sb.append("Request parameters: [");
        for (Enumeration en = request.getParameterNames(); en.hasMoreElements(); ) {
            String reqParamName = (String) en.nextElement();
            String paramValue = request.getParameter(reqParamName);
            sb.append(reqParamName).append(" = ").append(paramValue).append(", ");
        }
        sb.append("] \n");

        return sb.toString();
    }


    public void doGet(HttpServletRequest req, HttpServletResponse resp) throws java.io.IOException {
        DbLogging log = new DbLogging();
        initLog(log, req);
        String msg;
        String servletPath = req.getRequestURI();
        log.stat("begin", "servletPath = '" + servletPath);
//        log.debug("request",  httpServletRequestToString(req));
        try {
            if (checkServletPath(servletPath, "download")) {
                String transferKeyId = getRequestRequiredParameter(req, "transferKeyId");
                String configName = getRequestRequiredParameter(req, "configName");
                String zipped = null;
                try {
                    zipped = getRequestRequiredParameter(req, "zipped");
                } catch (Exception e) {
                    log.info("zipped", zipped);
                }
                if (StringUtils.isNotEmpty(transferKeyId) || StringUtils.isNotEmpty(configName)) {
                    download(configName, transferKeyId, ConvertUtils.convertStringToBoolean(zipped), resp, log);
                } else {
                    msg = "servlet parameters configName or (and) transferKeyId are unknown!";
                    resp.getWriter().println(msg);
                    throw new MessageStackException(msg);
                }
            } else if (servletPath.toLowerCase().contains("upload")) {
                doPost(req, resp);
            } else {
                msg = "servlet path " + servletPath + " is unknown!";
                resp.getWriter().println(msg);
                throw new MessageStackException(msg);
            }
            log.stat("end", "File downloaded at client successfully");
        } catch (Exception e) {
            e.printStackTrace();
            log.error("error", "" + e);
//            resp.getWriter().println(e);
        } catch (OutOfMemoryError e) {
            errorMsg = ExceptionUtils.formatVirtualMachineError(e);
            log.error("error", errorMsg);
            resp.getWriter().println(errorMsg);
        } finally {
            log.stat("end", "end");
            resp.getWriter().close();
        }
    }

    /**
     * downloads the file from database to client.
     *
     * @param transferKeyId identification of keys in a responsible file table
     * @param response      http response to write the answer
     */
    private void download(String configName, String transferKeyId, boolean isZipped, HttpServletResponse response,
                          DbLogging log) throws SQLException, IOException, MessageStackException {

        log.stat("begin", "db connected");
        ANYDATA anydt;
        if (conn == null) {
            conn = ConnectionUtil.getInitializeConnection(log);
        }

        OracleCallableStatement statement = (OracleCallableStatement) conn.prepareCall(
                "begin ? := isr$file$transfer$service.getFileFromDb(?, ?, ?); end; ");
        statement.registerOutParameter(1, OracleTypes.OPAQUE, "SYS.ANYDATA");
        statement.setString(2, configName);
        statement.setString(3, transferKeyId);
        statement.registerOutParameter(4, java.sql.Types.CLOB);  //todo
        statement.executeQuery();

        Clob clobError = statement.getClob(4);
        checkError(clobError);

        anydt = (ANYDATA) statement.getObject(1);
        ConnectionUtil.closeStatement(statement);

        if (anydt != null) {
            TypeDescriptor typeDesc = anydt.getTypeDescriptor();
            Datum embeddedDatum = anydt.accessDatum();
            if (embeddedDatum == null) {
                throw new RuntimeException("file from database is empty");
            }
            long fileLength = 0;
            byte[] bytes = null;
            InputStream inputStream = null;

            if (typeDesc.getTypeCode() == TypeDescriptor.TYPECODE_BLOB) {
                Blob blobFile = (Blob) embeddedDatum;
                inputStream = blobFile.getBinaryStream();
                fileLength = blobFile.length();
            } else if (typeDesc.getTypeCode() == TypeDescriptor.TYPECODE_CLOB) {
                Clob clobFile = (Clob) embeddedDatum;
                bytes = ConvertUtils.getBytes(clobFile);
                fileLength = clobFile.length();
            } else {
                log.error("end", "Type Code of ANYDATA object is unknown(type code from ANYDATA object: " +
                        typeDesc.getTypeCode() + "), params  of request configName: " + configName
                        + ", transferKeyId: " + transferKeyId);
            }
            if (bytes != null) {
                writeStringToResponse(bytes, response, log);
            } else if (inputStream != null) {
                writeInputStreamToResponse(inputStream, fileLength, isZipped, response, log);
            }
            log.stat("end", "fileLength = " + fileLength);
        } else {
            log.error("end", "ANYDATA object  is null, params of request configName: " + configName
                    + ", transferKeyId: " + transferKeyId);
        }
    }


    private void writeStringToResponse(byte[] bytes, HttpServletResponse response, DbLogging log) throws IOException {
        Writer writer;

        if (response == null) {  //for test
            log.debug("test", "test");
            writer = new FileWriterWithEncoding("result.xml", ConvertUtils.encoding);
        } else {
            log.debug("fileLength", bytes.length);
            response.setContentLength(bytes.length);
            String mimeType = "application/octet-stream; charset=UTF-8";
            response.setContentType(mimeType);
            // writes the file to the client
            writer = response.getWriter();
            log.debug("enc", response.getCharacterEncoding());
        }
        ConvertUtils.writeBytesToWriter(bytes, writer);
        writer.flush();
        writer.close();

    }


    private void writeInputStreamToResponse(InputStream inputStream, long fileLength, boolean isZipped, HttpServletResponse response, DbLogging log)
            throws IOException {
        log.debug("fileLength", "" + fileLength);
        byte[] buffer = new byte[1024];
        response.setContentLength((int) fileLength);
        String mimeType = "application/octet-stream";
        if (isZipped) {
            mimeType = "application/zip";
        }
        response.setContentType(mimeType);
        log.debug("enc", response.getCharacterEncoding());
        OutputStream outStream = response.getOutputStream();
        int bytesRead;
        int allReadBytes = 0;
        while ((bytesRead = inputStream.read(buffer)) != -1) {
            outStream.write(buffer, 0, bytesRead);
            allReadBytes = allReadBytes + bytesRead;
        }
        if (allReadBytes > 0) {
            log.info("end bytesRead", "read bytes " + allReadBytes);
        } else {
            log.error("end bytesRead", "check the file, the amount of read bytes is 0!");
        }
        inputStream.close();
        outStream.flush();
        outStream.close();
    }


    private void upload(String configName, String transferKeyId, FileItem fileItem, DbLogging log) throws SQLException, IOException, MessageStackException {
//        String currentName = getClass().getName() + ".upload";
        errorMsg = null;
        log.stat("begin", "upload begin");
        byte[] bytes = fileItem.get();
        log.info("file length", bytes.length);

        if (conn == null) {
            conn = ConnectionUtil.getInitializeConnection(log);
        }

        OracleCallableStatement statement = (OracleCallableStatement) conn.prepareCall(
                "begin isr$file$transfer$service.saveFileInDb(?, ?, ?, ?); end; ");
        statement.setString(1, configName);
        statement.setString(2, transferKeyId);
        ANYDATA anyData;
        String dataType = getDataType(configName, transferKeyId, conn, log);
        log.debug("dataType", dataType);
        if ("C".equalsIgnoreCase(dataType)) {
            Clob cl = DbConvertUtils.getClob(conn, bytes, log);
            anyData = ANYDATA.convertDatum((Datum) cl);
        } else if ("B".equalsIgnoreCase(dataType)) {
            Blob bl = ConvertUtils.getBlob(conn, bytes);
            anyData = ANYDATA.convertDatum((Datum) bl);
        } else {
            throw new RuntimeException("Data type is unknown!");
        }
        log.debug("anyData", anyData);
        statement.setObject(3, anyData);
        statement.registerOutParameter(4, java.sql.Types.CLOB);
        statement.executeQuery();

        log.debug("after executeQuery", "after executeQuery");
        Clob clobError = statement.getClob(4);
        checkError(clobError);
        ConnectionUtil.closeStatement(statement);

        log.stat("end", "errorMsg=" + errorMsg);
        if (StringUtils.isNotEmpty(errorMsg)) {  //todo
            throw new RuntimeException(errorMsg);
        }
    }


    private String getDataType(String configName, String transferKeyId, Connection conn, DbLogging log) throws SQLException {
        String dataType;
        try {
            OracleCallableStatement statement = (OracleCallableStatement) conn.prepareCall(
                    "begin ? := isr$file$transfer$service.getDataType(?, ?); end; ");
            statement.registerOutParameter(1, java.sql.Types.VARCHAR);
            statement.setString(2, configName);
            statement.setString(3, transferKeyId);
            statement.executeQuery();
            dataType = statement.getString(1);
            ConnectionUtil.closeStatement(statement);
        } catch (SQLException e) {
            log.error("error", "errorMsg=" + e);
            throw e;
        }
        return dataType;
    }

   /* private String getMaxMemory() {
        Runtime runtime = Runtime.getRuntime();
        int mb = 1024 * 1024;
        return "max memory: " + (runtime.maxMemory() / mb);
    }*/

    public static void main(String[] args) {

        FileTransferServlet servlet = new FileTransferServlet();
        try {
            servlet.writeStringToResponse(ConvertUtils.fileToBytes("rawdata1.xml"), null, new DbLogging());
        } catch (IOException e) {
            e.printStackTrace();
        } catch (OutOfMemoryError e) {
            e.printStackTrace();
        }
    }
}
