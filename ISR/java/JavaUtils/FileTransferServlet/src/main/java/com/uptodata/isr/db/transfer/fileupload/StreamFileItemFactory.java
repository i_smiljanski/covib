package com.uptodata.isr.db.transfer.fileupload;

import org.apache.commons.fileupload.FileItem;
import org.apache.commons.fileupload.disk.DiskFileItemFactory;

/**
 * Created by smiljanskii60 on 04.05.2015.
 */
public class StreamFileItemFactory  extends DiskFileItemFactory {

    public FileItem createItem(String fieldName, String contentType, boolean isFormField, String fileName) {
        return new StreamFileItem(fieldName,contentType, isFormField, fileName, 0, null);
    }
}
