package com.uptodata.isr.server.utils.serverProcess;

import javax.xml.bind.annotation.XmlElement;
import java.util.HashMap;

/**
 * Created by schroedera85 on 07.09.2015.
 */
public class ProcessInfoObject {

    private String inputFileName;
    private String transformerFileName;
    private String resultFileName;
    private String resultId;
    private HashMap<String, String> parameters;

    public ProcessInfoObject() {
    }

    public ProcessInfoObject(String inputFileName, String transformerFileName, String resultFileName, String resultId,
                             HashMap<String, String> parameters) {
        this.inputFileName = inputFileName;
        this.transformerFileName = transformerFileName;
        this.resultFileName = resultFileName;
        this.resultId = resultId;
        this.parameters = parameters;
    }

    public ProcessInfoObject(String resultFileName, String resultId) {
        this.resultFileName = resultFileName;
        this.resultId = resultId;
    }


    public String getInputFileName() {
        return inputFileName;
    }

    @XmlElement
    public void setInputFileName(String inputFileName) {
        this.inputFileName = inputFileName;
    }

    public String getTransformerFileName() {
        return transformerFileName;
    }

    @XmlElement
    public void setTransformerFileName(String transformerFileName) {
        this.transformerFileName = transformerFileName;
    }

    public String getResultFileName() {
        return resultFileName;
    }

    @XmlElement(name = "resultFileName")
    public void setResultFileName(String resultFileName) {
        this.resultFileName = resultFileName;
    }

    @XmlElement
    public String getResultId() {
        return resultId;
    }

    public void setResultId(String resultId) {
        this.resultId = resultId;
    }

    public HashMap<String, String> getParameters() {
        return parameters;
    }

    public void setParameters(HashMap<String, String> parameters) {
        this.parameters = parameters;
    }

    @Override
    public String toString() {
        return "ProcessInfoObject{" +
                "inputFileName='" + inputFileName + '\'' +
                ", transformerFileName='" + transformerFileName + '\'' +
                ", resultFileName='" + resultFileName + '\'' +
                ", resultId='" + resultId + '\'' +
                ", parameters=" + parameters +
                '}';
    }
}
