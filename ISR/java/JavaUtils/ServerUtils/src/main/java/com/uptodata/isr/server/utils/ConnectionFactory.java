package com.uptodata.isr.server.utils;

import com.uptodata.isr.utils.exceptions.MessageStackException;
import org.apache.commons.dbcp.DriverManagerConnectionFactory;
import org.apache.commons.dbcp.PoolableConnectionFactory;
import org.apache.commons.dbcp.PoolingDataSource;
import org.apache.commons.pool.impl.GenericObjectPool;

import java.sql.Connection;

public class ConnectionFactory {


    private static GenericObjectPool connectionPool = null;

    public static Connection getConnection(String driver, String dbURL, String user, String password) throws MessageStackException {
        Connection conn = null;
        try {
            //
            // Load JDBC Driver class.
            //
            Class.forName(driver).newInstance();

            //
            // Creates an instance of GenericObjectPool that holds our
            // pool of connections object.
            //
            connectionPool = new GenericObjectPool();
            connectionPool.setMaxActive(10);

            //
            // Creates a connection factory object which will be use by
            // the pool to create the connection object. We passes the
            // JDBC url info, username and password.
            //
            DriverManagerConnectionFactory cf = new DriverManagerConnectionFactory(dbURL, user, password);

            //
            // Creates a PoolableConnectionFactory that will wraps the
            // connection object created by the ConnectionFactory to add
            // object pooling functionality.
            //
            //  PoolableConnectionFactory pcf = new PoolableConnectionFactory(cf, connectionPool, null, null, false, true);
            new PoolableConnectionFactory(cf, connectionPool, null, null, 3, false, false, Connection.TRANSACTION_READ_COMMITTED);
            conn = new PoolingDataSource(connectionPool).getConnection();
        } catch (Exception e) {
            throw new MessageStackException(e, MessageStackException.SEVERITY_WARNING,
                    "cannot get a database connection for " + dbURL + " user = " + user + " password = " + password, MessageStackException.getCurrentMethod());
        }
        return conn;
    }

    public GenericObjectPool getConnectionPool() {
        return connectionPool;
    }

    public static void main(String[] args) throws Exception {
        ConnectionFactory demo = new ConnectionFactory();
        // getConnection("com.microsoft.sqlserver.jdbc.SQLServerDriver", "jdbc:sqlserver://10.0.100.214:1433;databaseName=isr_remote;", "ISRTEST", "ISRTEST");
        getConnection("oracle.jdbc.driver.OracleDriver", "jdbc:oracle:thin:@loradb92x:1521:e92", "remote35_utd_stab", " cmVtb3RlMzVfdXRkX3N0YWI=");
        demo.printStatus();

    }

    /**
     * Prints connection pool status.
     */
    private void printStatus() {
        System.out.println("Max   : " + getConnectionPool().getMaxActive() + "; " +
                "Active: " + getConnectionPool().getNumActive() + "; " +
                "Idle  : " + getConnectionPool().getNumIdle());
    }
}

