package com.uptodata.isr.server.utils.fileSystem;

import com.uptodata.isr.utils.exceptions.MessageStackException;
import net.lingala.zip4j.core.ZipFile;
import net.lingala.zip4j.exception.ZipException;
import org.apache.commons.io.FileUtils;
import org.apache.commons.io.filefilter.AgeFileFilter;
import org.apache.commons.io.filefilter.WildcardFileFilter;
import org.zeroturnaround.zip.ZipUtil;

import java.io.*;
import java.nio.file.*;
import java.nio.file.attribute.BasicFileAttributes;
import java.util.Collection;
import java.util.Properties;

/**
 * Created by schroedera85 on 10.12.2014.
 */
public class FileSystemWorker {
//    private static Logger log = LogManager.getLogger(FileSystemWorker.class);

    public static File createFolder(String folderPath, String folderName) throws IOException {
        File folder = new File(folderPath, folderName);
        FileUtils.forceMkdir(folder);
        return folder;
    }


    public static File createFolder(String folderName) throws IOException {
        File folder = new File(folderName);
        FileUtils.forceMkdir(folder);
        return folder;
    }

    public static boolean tryToCreateFolder(String folderName, int triesNumber, long triesInterval) throws Exception {
        boolean isOk = false;
        StringBuilder errorMsg = new StringBuilder();
        try {
            createFolder(folderName);
            isOk = true;
        } catch (IOException e) {
            int tryCounter = 0;
            int maxTryCount = 5;

            while (tryCounter < maxTryCount && !isOk) {
                try {
                    try {
                        Thread.currentThread();
                        Thread.sleep(500);
                    } catch (InterruptedException e1) {
                        e1.printStackTrace();// nothing to do
                    }
                    createFolder(folderName);
                    isOk = true;
                } catch (IOException ex) {
                    tryCounter++;
                    errorMsg.append(" ").append(ex);
                }
            }
        }

        if (!isOk) {
            throw new Exception(errorMsg.toString());
        }
        return true;
    }


    public static boolean checkFolder(File file) {
        return file != null && file.exists();
    }

    public static String getFileContent(File file) throws IOException {
        return FileUtils.readFileToString(file, "UTF-8");
    }

    public static void unpackZipToFolder(File zip, File folder) {
        ZipUtil.unpack(zip, folder);
    }

    public static void unpackZipToFolder(InputStream zip, File folder) {
        ZipUtil.unpack(zip, folder);
    }

    public static void packFolderToZip(File zip, File folder) {
        ZipUtil.pack(folder, zip);
    }

    public static void unpackEncryptedZip(String zipPath, String destinationPath, String pwd) throws ZipException {
        ZipFile zipFile = new ZipFile(zipPath);
        zipFile.setPassword(pwd);
        zipFile.extractAll(destinationPath);
    }


    public static Properties loadProperties(String propertiesFileName) throws Exception {
        File configFile = new File(propertiesFileName);
        if (!configFile.exists()) {
            throw new MessageStackException("File \"" + propertiesFileName + "\" in " + configFile.getAbsolutePath() + " not found!");
        }
        Properties properties = new Properties();
        try (InputStream is = new FileInputStream(configFile)) {
            properties.load(is);
        }
        return properties;
    }

    public static void removeFolder(File file) throws IOException {
        removeFolder(file, true);
    }


    public static void removeFolder(File file, boolean removeWithFolder) throws IOException {
        Path rootPath = Paths.get(file.getAbsolutePath());
        Files.walkFileTree(rootPath, new SimpleFileVisitor<Path>() {
            @Override
            public FileVisitResult visitFile(Path file, BasicFileAttributes attrs) throws IOException {
                try {
                    Files.delete(file);
                } catch (Exception e) {
                    System.out.println("e = " + e + " " + file);
                    throw e;
                }
                return FileVisitResult.CONTINUE;
            }

            @Override
            public FileVisitResult postVisitDirectory(Path dir, IOException exc) throws IOException {
                if (removeWithFolder) {
                    try {
                        Files.delete(dir);
                    } catch (Exception e) {
                        System.out.println("e = " + e + " " + dir);
                        throw e;
                    }
                }
                return FileVisitResult.CONTINUE;
            }

        });
    }


    public static boolean tryToRemoveFolder(File folder, int triesNumber, long triesInterval) throws Exception {
        boolean isOk = false;
//        System.out.println("begin remove folder '" + folder + "' " );
        String errorMsg = null;
        if (folder.exists()) {
            try {
                removeFolder(folder);
                isOk = true;
            } catch (IOException e) {
                int tryCounter = 0;
                while (tryCounter < triesNumber && !isOk) {
                    try {
                        try {
                            Thread.sleep(triesInterval);
                        } catch (InterruptedException e1) {
                            e1.printStackTrace();// nothing to do
                        }
                        removeFolder(folder);
                        isOk = true;
                    } catch (IOException ex) {
                        tryCounter++;
                        errorMsg = " " + ex;
                    }
                }
            }
        }
        if (!isOk) {
            System.out.println(folder + " " + errorMsg);
            throw new Exception(errorMsg);
        }
//        System.out.println("end remove folder '" + folder + "'");
        return true;
    }

    public static boolean deleteFile(String filePath) throws IOException {
        Path path = Paths.get(filePath);
        boolean isDeleted = true;
        if (Files.exists(path)) {
            isDeleted = deleteFile(path);
        }
        return isDeleted;
    }


    public static boolean deleteFile(Path path) throws IOException {
        return Files.deleteIfExists(path);
    }

    public static void copyFile(File from, File to) throws IOException {
        FileUtils.copyFile(from, to);
    }

    public static void copyInputStreamToFile(InputStream from, File to) throws IOException {
        FileUtils.copyInputStreamToFile(from, to);
    }

    public static void copyDirectory(File from, File to) throws IOException {
        FileUtils.copyDirectory(from, to);
    }

    public static void copyDirectoryToDirectory(String fromDirectoryName, String toDirectoryName) throws IOException {
        FileUtils.copyDirectoryToDirectory(new File(fromDirectoryName), new File(toDirectoryName));
    }


    public static void copyFileToDirectory(String srcFileName, String toDirectoryName) throws IOException {
        FileUtils.copyFileToDirectory(new File(srcFileName), new File(toDirectoryName));
    }

    /**
     * Determine whether a file is a ZIP File.
     */
    public static boolean isZipFile(File file) throws IOException {
        if (file.isDirectory()) {
            return false;
        }
        if (!file.canRead()) {
            throw new IOException("Cannot read file " + file.getAbsolutePath());
        }
        if (file.length() < 4) {
            return false;
        }
        DataInputStream in = new DataInputStream(new BufferedInputStream(new FileInputStream(file)));
        int test = in.readInt();
        in.close();
        return test == 0x504b0304;
    }

    public static void copyFileToAllDirectories(File parentDirectory, String srcFileName) throws IOException {
        File[] directories = parentDirectory.listFiles(File::isDirectory);
        if (directories != null) {
            for (File directory : directories) {
                FileUtils.copyFileToDirectory(new File(directory + File.separator + srcFileName), directory);
            }
        }
    }


    public static void removeDirectoryRecursive(Path directory) throws IOException {

        Files.walkFileTree(directory, new SimpleFileVisitor<Path>() {
            @Override
            public FileVisitResult visitFile(Path file, BasicFileAttributes attrs) throws IOException {
                Files.delete(file);
                return FileVisitResult.CONTINUE;
            }

            @Override
            public FileVisitResult postVisitDirectory(Path dir, IOException exc) throws IOException {
                try {
                    Files.delete(dir);
                } catch (DirectoryNotEmptyException e) {
                    FileUtils.forceDelete(dir.toFile());
                }
                return FileVisitResult.CONTINUE;
            }
        });
    }

    //getOrCreateFolders(Constants.USER_DIR, "folder1", "folder2", "folder3", ..);
    public static Path getOrCreateFolders(String first, String... more) throws IOException {
        Path folder = Paths.get(first, more);

        if (Files.notExists(folder)) {
            StringBuilder newFirst = new StringBuilder();
            for (String next : more) {
                Path nextFolder = Paths.get(first, newFirst.toString(), next);
                if (Files.notExists(nextFolder)) {
                    folder = Files.createDirectory(nextFolder);
                }
                newFirst.append(File.separator + next);
            }
        }
        return folder;
    }

    /**
     * deletes all files in a directory that is older than the given age
     * @param dir - the directory to search in
     * @param daysToRemainFiles - age of files to delete, in days
     * @param wildcard - the wildcard to search files (e.g. "*test*.txt~*~")
     * @return - null if successfully, or error message
     */
    public static String deleteOldFiles(String dir, int daysToRemainFiles, String wildcard) {
        long cutoff = System.currentTimeMillis() - (daysToRemainFiles * 24 * 60 * 60 * 1000);
        Collection<File> filesToDelete = FileUtils.listFiles(new File(dir),
                new AgeFileFilter(cutoff), new WildcardFileFilter(wildcard));
        StringBuilder errors = new StringBuilder();
        for (File file : filesToDelete) {
            boolean success = FileUtils.deleteQuietly(file);
            if (!success) {
                errors.append("file '").append(file).append("' could not be deleted. ");
            }
        }
        return errors.toString();
    }


}
