package com.uptodata.isr.server.utils.network;

import com.uptodata.isr.utils.exceptions.MessageStackException;
import org.apache.commons.codec.binary.Base64;
import org.apache.commons.io.IOUtils;
import org.apache.commons.validator.routines.UrlValidator;

import javax.xml.ws.Endpoint;
import java.io.IOException;
import java.io.InputStream;
import java.net.InetAddress;
import java.net.ServerSocket;
import java.net.URL;
import java.net.URLConnection;

/**
 * Created by smiljanskii60 on 29.01.2015.
 */
public class UrlUtil {
//    private static Logger log = IsrLogger.getLogger(UrlUtil.class);

    public static String getRequestFromServletAsString(URL url) throws IOException {
//        log.debug(dbServletUrl);
        InputStream is = getRequestFromServletAsIs(url);
        if (is != null) {
            return IOUtils.toString(is);
        }
        return null;
    }

    public static InputStream getRequestFromServletAsIs(URL url) throws IOException {
//        log.debug(dbServletUrl);
//            URL url = new URL(dbServletUrl);
        URLConnection urlConn = url.openConnection();
        InputStream is = urlConn.getInputStream();
        return is;
    }

    public static void authorizeConnectionBasic(URLConnection urlConn, String user, String password) {
        String user_pass = user + ":" + password;
        String encoded = Base64.encodeBase64String(user_pass.getBytes());
        urlConn.setRequestProperty("Authorization", "Basic " + encoded);
    }


    public static String getLocalHostAddress() {
        String localHost;
        try {
            localHost = InetAddress.getLocalHost().getHostAddress();
            System.out.println("localHost = " + localHost);

        } catch (Exception e) {
//            log.error(e);
            localHost = "127.0.0.1";
        }
        return localHost;
    }


    public static String buildUrl(String protocol, String host, String port, String context) throws MessageStackException {
        if (!context.startsWith("/")) {
            context = "/" + context;
        }
        String url = protocol + "://" + host + ":" + port + context;
//        if (!UrlUtil.isUrlValid(url)) {
//            throw new MessageStackException("url \"" + url + "\" is not valid");
//        }
        return url;
    }

    public static String buildDbServletUrl(String protocol, String host, String port) {
        return protocol + "://" + host + ":" + port;
    }

    public static boolean isUrlValid(String url) {
//        String[] schemes = {"http", "https"};
        UrlValidator urlValidator = new UrlValidator();
        return urlValidator.isValid(url);
    }

    public static Endpoint createAndPublishEndpoint(String serverUrl, Object implementer) throws MessageStackException {
//        if (!isUrlValid(serverUrl)) {
//            throw new MessageStackException("url \"" + serverUrl + "\" is not valid");
//        }
        try {
        /*The Endpoint publish static method specify the URL on which the WebService should be available and pass the WebService-class
            to handle the calls received at that URL. JAX-WS RI Endpoint.publish API uses by default a light-weight HTTP server implementation
            that is included in Sun's Java SE. It uses an embedded container i.e. something running inside the same JVM.  */
            Endpoint endpoint = Endpoint.publish(serverUrl, implementer);
//        SOAPBinding binding = (SOAPBinding) endpoint.getBinding();
//        binding.setMTOMEnabled(true); // See more at:
            // http://java.globinch.com/enterprise-java/web-services/jax-ws/jax-ws-attachment-enable-mtom-jax-ws-web-services/#jax-ws-with-mtom-web-service-java-example
            return endpoint;
        } catch (Exception e) {
            throw new MessageStackException(e, MessageStackException.SEVERITY_CRITICAL,
                    "web service '" + implementer + "' can't be published on the url " + serverUrl + ". " + e, MessageStackException.getCurrentMethod());
        }
    }

    public static boolean isPortFree(int portNumber) {
        boolean result;
        try {
            ServerSocket s = new ServerSocket(portNumber);
            s.close();
            result = true;
        } catch (Exception e) {
            result = false;
        }
        return result;
    }


    public static void main(String[] args) {
        String localHost = UrlUtil.getLocalHostAddress();
      /*  String serverUrl = null;
        try {
            serverUrl = UrlUtil.buildUrl("http", localHost, "1155", "/ws/observer/isr/uptodata/com");
        } catch (MessageStackException e) {
            e.printStackTrace();
        }
        System.out.println("serverUrl = " + serverUrl);

//        System.out.println(validator.isValid("http://isrcdevrs.utddomain.utd:5501/ws/remote/isr/uptodata/com"));

        String hostLocation = "isrcdevrs";
        DomainValidator domainValidator = DomainValidator.getInstance(true);
        System.out.println("domainValidator = " + domainValidator.isValid(hostLocation));
        if (!domainValidator.isValid(hostLocation)) {
            InetAddressValidator port = InetAddressValidator.getInstance();
            System.out.println("port.isValid(hostLocation) = " + port.isValid(hostLocation));
        }*/
    }


}
