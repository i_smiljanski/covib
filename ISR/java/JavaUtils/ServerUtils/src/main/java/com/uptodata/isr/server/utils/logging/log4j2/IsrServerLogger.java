package com.uptodata.isr.server.utils.logging.log4j2;

import org.apache.logging.log4j.Level;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.apache.logging.log4j.Marker;
import org.apache.logging.log4j.message.Message;
import org.apache.logging.log4j.message.MessageFactory;
import org.apache.logging.log4j.spi.AbstractLogger;
import org.apache.logging.log4j.spi.ExtendedLoggerWrapper;
import org.apache.logging.log4j.util.Supplier;

/**
 * Extended Logger interface with convenience methods for
 * the STATISTIC custom log level.
 * <p>Compatible with Log4j 2.6 or higher.</p>
 */
public final class IsrServerLogger extends ExtendedLoggerWrapper {
    private static final long serialVersionUID = 1028302645801930L;
    private final ExtendedLoggerWrapper logger;

    private static final String FQCN = IsrServerLogger.class.getName();
    private static final Level STATISTIC = Level.forName("STATISTIC", 250);
    private static final Level ALWAYS = Level.forName("ALWAYS", 150);


    private String logReference;

    private IsrServerLogger(final Logger logger) {
        super((AbstractLogger) logger, logger.getName(), logger.getMessageFactory());
        this.logger = this;
    }

    public String getLogReference() {
        return logReference;
    }

    public void setLogReference(String logReference) {
        this.logReference = logReference;
    }

    /**
     * Returns a custom Logger with the name of the calling class.
     *
     * @return The custom Logger for the calling class.
     */
    public static IsrServerLogger create() {
        final Logger wrapped = LogManager.getLogger();
        return new IsrServerLogger(wrapped);
    }


    /**
     * Returns a custom Logger using the fully qualified name of the Class as
     * the Logger name.
     *
     * @param loggerName The Class whose name should be used as the Logger name.
     *                   If null it will default to the calling class.
     * @return The custom Logger.
     */
    public static IsrServerLogger create(final Class<?> loggerName) {
        final Logger wrapped = LogManager.getLogger(loggerName);
        return new IsrServerLogger(wrapped);
    }

    /**
     * Returns a custom Logger using the fully qualified name of the Class as
     * the Logger name.
     *
     * @param loggerName     The Class whose name should be used as the Logger name.
     *                       If null it will default to the calling class.
     * @param messageFactory The message factory is used only when creating a
     *                       logger, subsequent use does not change the logger but will log
     *                       a warning if mismatched.
     * @return The custom Logger.
     */
    public static IsrServerLogger create(final Class<?> loggerName, final MessageFactory messageFactory) {
        final Logger wrapped = LogManager.getLogger(loggerName, messageFactory);
        return new IsrServerLogger(wrapped);
    }

    /**
     * Returns a custom Logger using the fully qualified class name of the value
     * as the Logger name.
     *
     * @param value The value whose class name should be used as the Logger
     *              name. If null the name of the calling class will be used as
     *              the logger name.
     * @return The custom Logger.
     */
    public static IsrServerLogger create(final Object value) {
        final Logger wrapped = LogManager.getLogger(value);
        return new IsrServerLogger(wrapped);
    }

    /**
     * Returns a custom Logger using the fully qualified class name of the value
     * as the Logger name.
     *
     * @param value          The value whose class name should be used as the Logger
     *                       name. If null the name of the calling class will be used as
     *                       the logger name.
     * @param messageFactory The message factory is used only when creating a
     *                       logger, subsequent use does not change the logger but will log
     *                       a warning if mismatched.
     * @return The custom Logger.
     */
    public static IsrServerLogger create(final Object value, final MessageFactory messageFactory) {
        final Logger wrapped = LogManager.getLogger(value, messageFactory);
        return new IsrServerLogger(wrapped);
    }

    /**
     * Returns a custom Logger with the specified name.
     *
     * @param name The logger name. If null the name of the calling class will
     *             be used.
     * @return The custom Logger.
     */
    public static IsrServerLogger create(final String name) {
        final Logger wrapped = LogManager.getLogger(name);
        return new IsrServerLogger(wrapped);
    }

    /**
     * Returns a custom Logger with the specified name.
     *
     * @param name           The logger name. If null the name of the calling class will
     *                       be used.
     * @param messageFactory The message factory is used only when creating a
     *                       logger, subsequent use does not change the logger but will log
     *                       a warning if mismatched.
     * @return The custom Logger.
     */
    public static IsrServerLogger create(final String name, final MessageFactory messageFactory) {
        final Logger wrapped = LogManager.getLogger(name, messageFactory);
        return new IsrServerLogger(wrapped);
    }

    /**
     * Logs a message with the specific Marker at the {@code STATISTIC} level.
     *
     * @param marker the marker data specific to this log statement
     * @param msg    the message string to be logged
     */
    public void stat(final Marker marker, final Message msg) {
        logger.logIfEnabled(FQCN, STATISTIC, marker, msg, (Throwable) null);
    }

    /**
     * Logs a message with the specific Marker at the {@code STATISTIC} level.
     *
     * @param marker the marker data specific to this log statement
     * @param msg    the message string to be logged
     * @param t      A Throwable or null.
     */
    public void stat(final Marker marker, final Message msg, final Throwable t) {
        logger.logIfEnabled(FQCN, STATISTIC, marker, msg, t);
    }

    /**
     * Logs a message object with the {@code STATISTIC} level.
     *
     * @param marker  the marker data specific to this log statement
     * @param message the message object to log.
     */
    public void stat(final Marker marker, final Object message) {
        logger.logIfEnabled(FQCN, STATISTIC, marker, message, (Throwable) null);
    }

    /**
     * Logs a message CharSequence with the {@code STATISTIC} level.
     *
     * @param marker  the marker data specific to this log statement
     * @param message the message CharSequence to log.
     * @since Log4j-2.6
     */
    public void stat(final Marker marker, final CharSequence message) {
        logger.logIfEnabled(FQCN, STATISTIC, marker, message, (Throwable) null);
    }

    /**
     * Logs a message at the {@code STATISTIC} level including the stack trace of
     * the {@link Throwable} {@code t} passed as parameter.
     *
     * @param marker  the marker data specific to this log statement
     * @param message the message to log.
     * @param t       the exception to log, including its stack trace.
     */
    public void stat(final Marker marker, final Object message, final Throwable t) {
        logger.logIfEnabled(FQCN, STATISTIC, marker, message, t);
    }

    /**
     * Logs a message at the {@code STATISTIC} level including the stack trace of
     * the {@link Throwable} {@code t} passed as parameter.
     *
     * @param marker  the marker data specific to this log statement
     * @param message the CharSequence to log.
     * @param t       the exception to log, including its stack trace.
     * @since Log4j-2.6
     */
    public void stat(final Marker marker, final CharSequence message, final Throwable t) {
        logger.logIfEnabled(FQCN, STATISTIC, marker, message, t);
    }

    /**
     * Logs a message object with the {@code STATISTIC} level.
     *
     * @param marker  the marker data specific to this log statement
     * @param message the message object to log.
     */
    public void stat(final Marker marker, final String message) {
        logger.logIfEnabled(FQCN, STATISTIC, marker, message, (Throwable) null);
    }

    /**
     * Logs a message with parameters at the {@code STATISTIC} level.
     *
     * @param marker  the marker data specific to this log statement
     * @param message the message to log; the format depends on the message factory.
     * @param params  parameters to the message.
     * @see #getMessageFactory()
     */
    public void stat(final Marker marker, final String message, final Object... params) {
        logger.logIfEnabled(FQCN, STATISTIC, marker, message, params);
    }

    /**
     * Logs a message with parameters at the {@code STATISTIC} level.
     *
     * @param marker  the marker data specific to this log statement
     * @param message the message to log; the format depends on the message factory.
     * @param p0      parameter to the message.
     * @see #getMessageFactory()
     * @since Log4j-2.6
     */
    public void stat(final Marker marker, final String message, final Object p0) {
        logger.logIfEnabled(FQCN, STATISTIC, marker, message, p0);
    }

    /**
     * Logs a message with parameters at the {@code STATISTIC} level.
     *
     * @param marker  the marker data specific to this log statement
     * @param message the message to log; the format depends on the message factory.
     * @param p0      parameter to the message.
     * @param p1      parameter to the message.
     * @see #getMessageFactory()
     * @since Log4j-2.6
     */
    public void stat(final Marker marker, final String message, final Object p0, final Object p1) {
        logger.logIfEnabled(FQCN, STATISTIC, marker, message, p0, p1);
    }

    /**
     * Logs a message with parameters at the {@code STATISTIC} level.
     *
     * @param marker  the marker data specific to this log statement
     * @param message the message to log; the format depends on the message factory.
     * @param p0      parameter to the message.
     * @param p1      parameter to the message.
     * @param p2      parameter to the message.
     * @see #getMessageFactory()
     * @since Log4j-2.6
     */
    public void stat(final Marker marker, final String message, final Object p0, final Object p1, final Object p2) {
        logger.logIfEnabled(FQCN, STATISTIC, marker, message, p0, p1, p2);
    }

    /**
     * Logs a message with parameters at the {@code STATISTIC} level.
     *
     * @param marker  the marker data specific to this log statement
     * @param message the message to log; the format depends on the message factory.
     * @param p0      parameter to the message.
     * @param p1      parameter to the message.
     * @param p2      parameter to the message.
     * @param p3      parameter to the message.
     * @see #getMessageFactory()
     * @since Log4j-2.6
     */
    public void stat(final Marker marker, final String message, final Object p0, final Object p1, final Object p2,
                     final Object p3) {
        logger.logIfEnabled(FQCN, STATISTIC, marker, message, p0, p1, p2, p3);
    }

    /**
     * Logs a message with parameters at the {@code STATISTIC} level.
     *
     * @param marker  the marker data specific to this log statement
     * @param message the message to log; the format depends on the message factory.
     * @param p0      parameter to the message.
     * @param p1      parameter to the message.
     * @param p2      parameter to the message.
     * @param p3      parameter to the message.
     * @param p4      parameter to the message.
     * @see #getMessageFactory()
     * @since Log4j-2.6
     */
    public void stat(final Marker marker, final String message, final Object p0, final Object p1, final Object p2,
                     final Object p3, final Object p4) {
        logger.logIfEnabled(FQCN, STATISTIC, marker, message, p0, p1, p2, p3, p4);
    }

    /**
     * Logs a message with parameters at the {@code STATISTIC} level.
     *
     * @param marker  the marker data specific to this log statement
     * @param message the message to log; the format depends on the message factory.
     * @param p0      parameter to the message.
     * @param p1      parameter to the message.
     * @param p2      parameter to the message.
     * @param p3      parameter to the message.
     * @param p4      parameter to the message.
     * @param p5      parameter to the message.
     * @see #getMessageFactory()
     * @since Log4j-2.6
     */
    public void stat(final Marker marker, final String message, final Object p0, final Object p1, final Object p2,
                     final Object p3, final Object p4, final Object p5) {
        logger.logIfEnabled(FQCN, STATISTIC, marker, message, p0, p1, p2, p3, p4, p5);
    }

    /**
     * Logs a message with parameters at the {@code STATISTIC} level.
     *
     * @param marker  the marker data specific to this log statement
     * @param message the message to log; the format depends on the message factory.
     * @param p0      parameter to the message.
     * @param p1      parameter to the message.
     * @param p2      parameter to the message.
     * @param p3      parameter to the message.
     * @param p4      parameter to the message.
     * @param p5      parameter to the message.
     * @param p6      parameter to the message.
     * @see #getMessageFactory()
     * @since Log4j-2.6
     */
    public void stat(final Marker marker, final String message, final Object p0, final Object p1, final Object p2,
                     final Object p3, final Object p4, final Object p5, final Object p6) {
        logger.logIfEnabled(FQCN, STATISTIC, marker, message, p0, p1, p2, p3, p4, p5, p6);
    }

    /**
     * Logs a message with parameters at the {@code STATISTIC} level.
     *
     * @param marker  the marker data specific to this log statement
     * @param message the message to log; the format depends on the message factory.
     * @param p0      parameter to the message.
     * @param p1      parameter to the message.
     * @param p2      parameter to the message.
     * @param p3      parameter to the message.
     * @param p4      parameter to the message.
     * @param p5      parameter to the message.
     * @param p6      parameter to the message.
     * @param p7      parameter to the message.
     * @see #getMessageFactory()
     * @since Log4j-2.6
     */
    public void stat(final Marker marker, final String message, final Object p0, final Object p1, final Object p2,
                     final Object p3, final Object p4, final Object p5, final Object p6,
                     final Object p7) {
        logger.logIfEnabled(FQCN, STATISTIC, marker, message, p0, p1, p2, p3, p4, p5, p6, p7);
    }

    /**
     * Logs a message with parameters at the {@code STATISTIC} level.
     *
     * @param marker  the marker data specific to this log statement
     * @param message the message to log; the format depends on the message factory.
     * @param p0      parameter to the message.
     * @param p1      parameter to the message.
     * @param p2      parameter to the message.
     * @param p3      parameter to the message.
     * @param p4      parameter to the message.
     * @param p5      parameter to the message.
     * @param p6      parameter to the message.
     * @param p7      parameter to the message.
     * @param p8      parameter to the message.
     * @see #getMessageFactory()
     * @since Log4j-2.6
     */
    public void stat(final Marker marker, final String message, final Object p0, final Object p1, final Object p2,
                     final Object p3, final Object p4, final Object p5, final Object p6,
                     final Object p7, final Object p8) {
        logger.logIfEnabled(FQCN, STATISTIC, marker, message, p0, p1, p2, p3, p4, p5, p6, p7, p8);
    }

    /**
     * Logs a message with parameters at the {@code STATISTIC} level.
     *
     * @param marker  the marker data specific to this log statement
     * @param message the message to log; the format depends on the message factory.
     * @param p0      parameter to the message.
     * @param p1      parameter to the message.
     * @param p2      parameter to the message.
     * @param p3      parameter to the message.
     * @param p4      parameter to the message.
     * @param p5      parameter to the message.
     * @param p6      parameter to the message.
     * @param p7      parameter to the message.
     * @param p8      parameter to the message.
     * @param p9      parameter to the message.
     * @see #getMessageFactory()
     * @since Log4j-2.6
     */
    public void stat(final Marker marker, final String message, final Object p0, final Object p1, final Object p2,
                     final Object p3, final Object p4, final Object p5, final Object p6,
                     final Object p7, final Object p8, final Object p9) {
        logger.logIfEnabled(FQCN, STATISTIC, marker, message, p0, p1, p2, p3, p4, p5, p6, p7, p8, p9);
    }

    /**
     * Logs a message at the {@code STATISTIC} level including the stack trace of
     * the {@link Throwable} {@code t} passed as parameter.
     *
     * @param marker  the marker data specific to this log statement
     * @param message the message to log.
     * @param t       the exception to log, including its stack trace.
     */
    public void stat(final Marker marker, final String message, final Throwable t) {
        logger.logIfEnabled(FQCN, STATISTIC, marker, message, t);
    }

    /**
     * Logs the specified Message at the {@code STATISTIC} level.
     *
     * @param msg the message string to be logged
     */
    public void stat(final Message msg) {
        logger.logIfEnabled(FQCN, STATISTIC, null, msg, (Throwable) null);
    }

    /**
     * Logs the specified Message at the {@code STATISTIC} level.
     *
     * @param msg the message string to be logged
     * @param t   A Throwable or null.
     */
    public void stat(final Message msg, final Throwable t) {
        logger.logIfEnabled(FQCN, STATISTIC, null, msg, t);
    }

    /**
     * Logs a message object with the {@code STATISTIC} level.
     *
     * @param message the message object to log.
     */
    public void stat(final Object message) {
        logger.logIfEnabled(FQCN, STATISTIC, null, message, (Throwable) null);
    }

    /**
     * Logs a message at the {@code STATISTIC} level including the stack trace of
     * the {@link Throwable} {@code t} passed as parameter.
     *
     * @param message the message to log.
     * @param t       the exception to log, including its stack trace.
     */
    public void stat(final Object message, final Throwable t) {
        logger.logIfEnabled(FQCN, STATISTIC, null, message, t);
    }

    /**
     * Logs a message CharSequence with the {@code STATISTIC} level.
     *
     * @param message the message CharSequence to log.
     * @since Log4j-2.6
     */
    public void stat(final CharSequence message) {
        logger.logIfEnabled(FQCN, STATISTIC, null, message, (Throwable) null);
    }

    /**
     * Logs a CharSequence at the {@code STATISTIC} level including the stack trace of
     * the {@link Throwable} {@code t} passed as parameter.
     *
     * @param message the CharSequence to log.
     * @param t       the exception to log, including its stack trace.
     * @since Log4j-2.6
     */
    public void stat(final CharSequence message, final Throwable t) {
        logger.logIfEnabled(FQCN, STATISTIC, null, message, t);
    }

    /**
     * Logs a message object with the {@code STATISTIC} level.
     *
     * @param message the message object to log.
     */
    public void stat(final String message) {
        logger.logIfEnabled(FQCN, STATISTIC, null, message, (Throwable) null);
    }

    /**
     * Logs a message with parameters at the {@code STATISTIC} level.
     *
     * @param message the message to log; the format depends on the message factory.
     * @param params  parameters to the message.
     * @see #getMessageFactory()
     */
    public void stat(final String message, final Object... params) {
        logger.logIfEnabled(FQCN, STATISTIC, null, message, params);
    }

    /**
     * Logs a message with parameters at the {@code STATISTIC} level.
     *
     * @param message the message to log; the format depends on the message factory.
     * @param p0      parameter to the message.
     * @see #getMessageFactory()
     * @since Log4j-2.6
     */
    public void stat(final String message, final Object p0) {
        logger.logIfEnabled(FQCN, STATISTIC, null, message, p0);
    }

    /**
     * Logs a message with parameters at the {@code STATISTIC} level.
     *
     * @param message the message to log; the format depends on the message factory.
     * @param p0      parameter to the message.
     * @param p1      parameter to the message.
     * @see #getMessageFactory()
     * @since Log4j-2.6
     */
    public void stat(final String message, final Object p0, final Object p1) {
        logger.logIfEnabled(FQCN, STATISTIC, null, message, p0, p1);
    }

    /**
     * Logs a message with parameters at the {@code STATISTIC} level.
     *
     * @param message the message to log; the format depends on the message factory.
     * @param p0      parameter to the message.
     * @param p1      parameter to the message.
     * @param p2      parameter to the message.
     * @see #getMessageFactory()
     * @since Log4j-2.6
     */
    public void stat(final String message, final Object p0, final Object p1, final Object p2) {
        logger.logIfEnabled(FQCN, STATISTIC, null, message, p0, p1, p2);
    }

    /**
     * Logs a message with parameters at the {@code STATISTIC} level.
     *
     * @param message the message to log; the format depends on the message factory.
     * @param p0      parameter to the message.
     * @param p1      parameter to the message.
     * @param p2      parameter to the message.
     * @param p3      parameter to the message.
     * @see #getMessageFactory()
     * @since Log4j-2.6
     */
    public void stat(final String message, final Object p0, final Object p1, final Object p2,
                     final Object p3) {
        logger.logIfEnabled(FQCN, STATISTIC, null, message, p0, p1, p2, p3);
    }

    /**
     * Logs a message with parameters at the {@code STATISTIC} level.
     *
     * @param message the message to log; the format depends on the message factory.
     * @param p0      parameter to the message.
     * @param p1      parameter to the message.
     * @param p2      parameter to the message.
     * @param p3      parameter to the message.
     * @param p4      parameter to the message.
     * @see #getMessageFactory()
     * @since Log4j-2.6
     */
    public void stat(final String message, final Object p0, final Object p1, final Object p2,
                     final Object p3, final Object p4) {
        logger.logIfEnabled(FQCN, STATISTIC, null, message, p0, p1, p2, p3, p4);
    }

    /**
     * Logs a message with parameters at the {@code STATISTIC} level.
     *
     * @param message the message to log; the format depends on the message factory.
     * @param p0      parameter to the message.
     * @param p1      parameter to the message.
     * @param p2      parameter to the message.
     * @param p3      parameter to the message.
     * @param p4      parameter to the message.
     * @param p5      parameter to the message.
     * @see #getMessageFactory()
     * @since Log4j-2.6
     */
    public void stat(final String message, final Object p0, final Object p1, final Object p2,
                     final Object p3, final Object p4, final Object p5) {
        logger.logIfEnabled(FQCN, STATISTIC, null, message, p0, p1, p2, p3, p4, p5);
    }

    /**
     * Logs a message with parameters at the {@code STATISTIC} level.
     *
     * @param message the message to log; the format depends on the message factory.
     * @param p0      parameter to the message.
     * @param p1      parameter to the message.
     * @param p2      parameter to the message.
     * @param p3      parameter to the message.
     * @param p4      parameter to the message.
     * @param p5      parameter to the message.
     * @param p6      parameter to the message.
     * @see #getMessageFactory()
     * @since Log4j-2.6
     */
    public void stat(final String message, final Object p0, final Object p1, final Object p2,
                     final Object p3, final Object p4, final Object p5, final Object p6) {
        logger.logIfEnabled(FQCN, STATISTIC, null, message, p0, p1, p2, p3, p4, p5, p6);
    }

    /**
     * Logs a message with parameters at the {@code STATISTIC} level.
     *
     * @param message the message to log; the format depends on the message factory.
     * @param p0      parameter to the message.
     * @param p1      parameter to the message.
     * @param p2      parameter to the message.
     * @param p3      parameter to the message.
     * @param p4      parameter to the message.
     * @param p5      parameter to the message.
     * @param p6      parameter to the message.
     * @param p7      parameter to the message.
     * @see #getMessageFactory()
     * @since Log4j-2.6
     */
    public void stat(final String message, final Object p0, final Object p1, final Object p2,
                     final Object p3, final Object p4, final Object p5, final Object p6,
                     final Object p7) {
        logger.logIfEnabled(FQCN, STATISTIC, null, message, p0, p1, p2, p3, p4, p5, p6, p7);
    }

    /**
     * Logs a message with parameters at the {@code STATISTIC} level.
     *
     * @param message the message to log; the format depends on the message factory.
     * @param p0      parameter to the message.
     * @param p1      parameter to the message.
     * @param p2      parameter to the message.
     * @param p3      parameter to the message.
     * @param p4      parameter to the message.
     * @param p5      parameter to the message.
     * @param p6      parameter to the message.
     * @param p7      parameter to the message.
     * @param p8      parameter to the message.
     * @see #getMessageFactory()
     * @since Log4j-2.6
     */
    public void stat(final String message, final Object p0, final Object p1, final Object p2,
                     final Object p3, final Object p4, final Object p5, final Object p6,
                     final Object p7, final Object p8) {
        logger.logIfEnabled(FQCN, STATISTIC, null, message, p0, p1, p2, p3, p4, p5, p6, p7, p8);
    }

    /**
     * Logs a message with parameters at the {@code STATISTIC} level.
     *
     * @param message the message to log; the format depends on the message factory.
     * @param p0      parameter to the message.
     * @param p1      parameter to the message.
     * @param p2      parameter to the message.
     * @param p3      parameter to the message.
     * @param p4      parameter to the message.
     * @param p5      parameter to the message.
     * @param p6      parameter to the message.
     * @param p7      parameter to the message.
     * @param p8      parameter to the message.
     * @param p9      parameter to the message.
     * @see #getMessageFactory()
     * @since Log4j-2.6
     */
    public void stat(final String message, final Object p0, final Object p1, final Object p2,
                     final Object p3, final Object p4, final Object p5, final Object p6,
                     final Object p7, final Object p8, final Object p9) {
        logger.logIfEnabled(FQCN, STATISTIC, null, message, p0, p1, p2, p3, p4, p5, p6, p7, p8, p9);
    }

    /**
     * Logs a message at the {@code STATISTIC} level including the stack trace of
     * the {@link Throwable} {@code t} passed as parameter.
     *
     * @param message the message to log.
     * @param t       the exception to log, including its stack trace.
     */
    public void stat(final String message, final Throwable t) {
        logger.logIfEnabled(FQCN, STATISTIC, null, message, t);
    }

    /**
     * Logs a message which is only to be constructed if the logging level is the {@code STATISTIC}level.
     *
     * @param msgSupplier A function, which when called, produces the desired log message;
     *                    the format depends on the message factory.
     * @since Log4j-2.4
     */
    public void stat(final Supplier<?> msgSupplier) {
        logger.logIfEnabled(FQCN, STATISTIC, null, msgSupplier, (Throwable) null);
    }

    /**
     * Logs a message (only to be constructed if the logging level is the {@code STATISTIC}
     * level) including the stack trace of the {@link Throwable} <code>t</code> passed as parameter.
     *
     * @param msgSupplier A function, which when called, produces the desired log message;
     *                    the format depends on the message factory.
     * @param t           the exception to log, including its stack trace.
     * @since Log4j-2.4
     */
    public void stat(final Supplier<?> msgSupplier, final Throwable t) {
        logger.logIfEnabled(FQCN, STATISTIC, null, msgSupplier, t);
    }

    /**
     * Logs a message which is only to be constructed if the logging level is the
     * {@code STATISTIC} level with the specified Marker.
     *
     * @param marker      the marker data specific to this log statement
     * @param msgSupplier A function, which when called, produces the desired log message;
     *                    the format depends on the message factory.
     * @since Log4j-2.4
     */
    public void stat(final Marker marker, final Supplier<?> msgSupplier) {
        logger.logIfEnabled(FQCN, STATISTIC, marker, msgSupplier, (Throwable) null);
    }

    /**
     * Logs a message with parameters which are only to be constructed if the logging level is the
     * {@code STATISTIC} level.
     *
     * @param marker         the marker data specific to this log statement
     * @param message        the message to log; the format depends on the message factory.
     * @param paramSuppliers An array of functions, which when called, produce the desired log message parameters.
     * @since Log4j-2.4
     */
    public void stat(final Marker marker, final String message, final Supplier<?>... paramSuppliers) {
        logger.logIfEnabled(FQCN, STATISTIC, marker, message, paramSuppliers);
    }

    /**
     * Logs a message (only to be constructed if the logging level is the {@code STATISTIC}
     * level) with the specified Marker and including the stack trace of the {@link Throwable}
     * <code>t</code> passed as parameter.
     *
     * @param marker      the marker data specific to this log statement
     * @param msgSupplier A function, which when called, produces the desired log message;
     *                    the format depends on the message factory.
     * @param t           A Throwable or null.
     * @since Log4j-2.4
     */
    public void stat(final Marker marker, final Supplier<?> msgSupplier, final Throwable t) {
        logger.logIfEnabled(FQCN, STATISTIC, marker, msgSupplier, t);
    }

    /**
     * Logs a message with parameters which are only to be constructed if the logging level is
     * the {@code STATISTIC} level.
     *
     * @param message        the message to log; the format depends on the message factory.
     * @param paramSuppliers An array of functions, which when called, produce the desired log message parameters.
     * @since Log4j-2.4
     */
    public void stat(final String message, final Supplier<?>... paramSuppliers) {
        logger.logIfEnabled(FQCN, STATISTIC, null, message, paramSuppliers);
    }


    /**
     * Logs a message with the specific Marker at the {@code ALWAYS} level.
     *
     * @param marker the marker data specific to this log statement
     * @param msg    the message string to be logged
     */
    public void always(final Marker marker, final Message msg) {
        logger.logIfEnabled(FQCN, ALWAYS, marker, msg, (Throwable) null);
    }

    /**
     * Logs a message with the specific Marker at the {@code ALWAYS} level.
     *
     * @param marker the marker data specific to this log statement
     * @param msg    the message string to be logged
     * @param t      A Throwable or null.
     */
    public void always(final Marker marker, final Message msg, final Throwable t) {
        logger.logIfEnabled(FQCN, ALWAYS, marker, msg, t);
    }

    /**
     * Logs a message object with the {@code ALWAYS} level.
     *
     * @param marker  the marker data specific to this log statement
     * @param message the message object to log.
     */
    public void always(final Marker marker, final Object message) {
        logger.logIfEnabled(FQCN, ALWAYS, marker, message, (Throwable) null);
    }

    /**
     * Logs a message CharSequence with the {@code ALWAYS} level.
     *
     * @param marker  the marker data specific to this log statement
     * @param message the message CharSequence to log.
     * @since Log4j-2.6
     */
    public void always(final Marker marker, final CharSequence message) {
        logger.logIfEnabled(FQCN, ALWAYS, marker, message, (Throwable) null);
    }

    /**
     * Logs a message at the {@code ALWAYS} level including the stack trace of
     * the {@link Throwable} {@code t} passed as parameter.
     *
     * @param marker  the marker data specific to this log statement
     * @param message the message to log.
     * @param t       the exception to log, including its stack trace.
     */
    public void always(final Marker marker, final Object message, final Throwable t) {
        logger.logIfEnabled(FQCN, ALWAYS, marker, message, t);
    }

    /**
     * Logs a message at the {@code ALWAYS} level including the stack trace of
     * the {@link Throwable} {@code t} passed as parameter.
     *
     * @param marker  the marker data specific to this log statement
     * @param message the CharSequence to log.
     * @param t       the exception to log, including its stack trace.
     * @since Log4j-2.6
     */
    public void always(final Marker marker, final CharSequence message, final Throwable t) {
        logger.logIfEnabled(FQCN, ALWAYS, marker, message, t);
    }

    /**
     * Logs a message object with the {@code ALWAYS} level.
     *
     * @param marker  the marker data specific to this log statement
     * @param message the message object to log.
     */
    public void always(final Marker marker, final String message) {
        logger.logIfEnabled(FQCN, ALWAYS, marker, message, (Throwable) null);
    }

    /**
     * Logs a message with parameters at the {@code ALWAYS} level.
     *
     * @param marker  the marker data specific to this log statement
     * @param message the message to log; the format depends on the message factory.
     * @param params  parameters to the message.
     * @see #getMessageFactory()
     */
    public void always(final Marker marker, final String message, final Object... params) {
        logger.logIfEnabled(FQCN, ALWAYS, marker, message, params);
    }

    /**
     * Logs a message with parameters at the {@code ALWAYS} level.
     *
     * @param marker  the marker data specific to this log statement
     * @param message the message to log; the format depends on the message factory.
     * @param p0      parameter to the message.
     * @see #getMessageFactory()
     * @since Log4j-2.6
     */
    public void always(final Marker marker, final String message, final Object p0) {
        logger.logIfEnabled(FQCN, ALWAYS, marker, message, p0);
    }

    /**
     * Logs a message with parameters at the {@code ALWAYS} level.
     *
     * @param marker  the marker data specific to this log statement
     * @param message the message to log; the format depends on the message factory.
     * @param p0      parameter to the message.
     * @param p1      parameter to the message.
     * @see #getMessageFactory()
     * @since Log4j-2.6
     */
    public void always(final Marker marker, final String message, final Object p0, final Object p1) {
        logger.logIfEnabled(FQCN, ALWAYS, marker, message, p0, p1);
    }

    /**
     * Logs a message with parameters at the {@code ALWAYS} level.
     *
     * @param marker  the marker data specific to this log statement
     * @param message the message to log; the format depends on the message factory.
     * @param p0      parameter to the message.
     * @param p1      parameter to the message.
     * @param p2      parameter to the message.
     * @see #getMessageFactory()
     * @since Log4j-2.6
     */
    public void always(final Marker marker, final String message, final Object p0, final Object p1, final Object p2) {
        logger.logIfEnabled(FQCN, ALWAYS, marker, message, p0, p1, p2);
    }

    /**
     * Logs a message with parameters at the {@code ALWAYS} level.
     *
     * @param marker  the marker data specific to this log statement
     * @param message the message to log; the format depends on the message factory.
     * @param p0      parameter to the message.
     * @param p1      parameter to the message.
     * @param p2      parameter to the message.
     * @param p3      parameter to the message.
     * @see #getMessageFactory()
     * @since Log4j-2.6
     */
    public void always(final Marker marker, final String message, final Object p0, final Object p1, final Object p2,
                       final Object p3) {
        logger.logIfEnabled(FQCN, ALWAYS, marker, message, p0, p1, p2, p3);
    }

    /**
     * Logs a message with parameters at the {@code ALWAYS} level.
     *
     * @param marker  the marker data specific to this log statement
     * @param message the message to log; the format depends on the message factory.
     * @param p0      parameter to the message.
     * @param p1      parameter to the message.
     * @param p2      parameter to the message.
     * @param p3      parameter to the message.
     * @param p4      parameter to the message.
     * @see #getMessageFactory()
     * @since Log4j-2.6
     */
    public void always(final Marker marker, final String message, final Object p0, final Object p1, final Object p2,
                       final Object p3, final Object p4) {
        logger.logIfEnabled(FQCN, ALWAYS, marker, message, p0, p1, p2, p3, p4);
    }

    /**
     * Logs a message with parameters at the {@code ALWAYS} level.
     *
     * @param marker  the marker data specific to this log statement
     * @param message the message to log; the format depends on the message factory.
     * @param p0      parameter to the message.
     * @param p1      parameter to the message.
     * @param p2      parameter to the message.
     * @param p3      parameter to the message.
     * @param p4      parameter to the message.
     * @param p5      parameter to the message.
     * @see #getMessageFactory()
     * @since Log4j-2.6
     */
    public void always(final Marker marker, final String message, final Object p0, final Object p1, final Object p2,
                       final Object p3, final Object p4, final Object p5) {
        logger.logIfEnabled(FQCN, ALWAYS, marker, message, p0, p1, p2, p3, p4, p5);
    }

    /**
     * Logs a message with parameters at the {@code ALWAYS} level.
     *
     * @param marker  the marker data specific to this log statement
     * @param message the message to log; the format depends on the message factory.
     * @param p0      parameter to the message.
     * @param p1      parameter to the message.
     * @param p2      parameter to the message.
     * @param p3      parameter to the message.
     * @param p4      parameter to the message.
     * @param p5      parameter to the message.
     * @param p6      parameter to the message.
     * @see #getMessageFactory()
     * @since Log4j-2.6
     */
    public void always(final Marker marker, final String message, final Object p0, final Object p1, final Object p2,
                       final Object p3, final Object p4, final Object p5, final Object p6) {
        logger.logIfEnabled(FQCN, ALWAYS, marker, message, p0, p1, p2, p3, p4, p5, p6);
    }

    /**
     * Logs a message with parameters at the {@code ALWAYS} level.
     *
     * @param marker  the marker data specific to this log statement
     * @param message the message to log; the format depends on the message factory.
     * @param p0      parameter to the message.
     * @param p1      parameter to the message.
     * @param p2      parameter to the message.
     * @param p3      parameter to the message.
     * @param p4      parameter to the message.
     * @param p5      parameter to the message.
     * @param p6      parameter to the message.
     * @param p7      parameter to the message.
     * @see #getMessageFactory()
     * @since Log4j-2.6
     */
    public void always(final Marker marker, final String message, final Object p0, final Object p1, final Object p2,
                       final Object p3, final Object p4, final Object p5, final Object p6,
                       final Object p7) {
        logger.logIfEnabled(FQCN, ALWAYS, marker, message, p0, p1, p2, p3, p4, p5, p6, p7);
    }

    /**
     * Logs a message with parameters at the {@code ALWAYS} level.
     *
     * @param marker  the marker data specific to this log statement
     * @param message the message to log; the format depends on the message factory.
     * @param p0      parameter to the message.
     * @param p1      parameter to the message.
     * @param p2      parameter to the message.
     * @param p3      parameter to the message.
     * @param p4      parameter to the message.
     * @param p5      parameter to the message.
     * @param p6      parameter to the message.
     * @param p7      parameter to the message.
     * @param p8      parameter to the message.
     * @see #getMessageFactory()
     * @since Log4j-2.6
     */
    public void always(final Marker marker, final String message, final Object p0, final Object p1, final Object p2,
                       final Object p3, final Object p4, final Object p5, final Object p6,
                       final Object p7, final Object p8) {
        logger.logIfEnabled(FQCN, ALWAYS, marker, message, p0, p1, p2, p3, p4, p5, p6, p7, p8);
    }

    /**
     * Logs a message with parameters at the {@code ALWAYS} level.
     *
     * @param marker  the marker data specific to this log statement
     * @param message the message to log; the format depends on the message factory.
     * @param p0      parameter to the message.
     * @param p1      parameter to the message.
     * @param p2      parameter to the message.
     * @param p3      parameter to the message.
     * @param p4      parameter to the message.
     * @param p5      parameter to the message.
     * @param p6      parameter to the message.
     * @param p7      parameter to the message.
     * @param p8      parameter to the message.
     * @param p9      parameter to the message.
     * @see #getMessageFactory()
     * @since Log4j-2.6
     */
    public void always(final Marker marker, final String message, final Object p0, final Object p1, final Object p2,
                       final Object p3, final Object p4, final Object p5, final Object p6,
                       final Object p7, final Object p8, final Object p9) {
        logger.logIfEnabled(FQCN, ALWAYS, marker, message, p0, p1, p2, p3, p4, p5, p6, p7, p8, p9);
    }

    /**
     * Logs a message at the {@code ALWAYS} level including the stack trace of
     * the {@link Throwable} {@code t} passed as parameter.
     *
     * @param marker  the marker data specific to this log statement
     * @param message the message to log.
     * @param t       the exception to log, including its stack trace.
     */
    public void always(final Marker marker, final String message, final Throwable t) {
        logger.logIfEnabled(FQCN, ALWAYS, marker, message, t);
    }

    /**
     * Logs the specified Message at the {@code ALWAYS} level.
     *
     * @param msg the message string to be logged
     */
    public void always(final Message msg) {
        logger.logIfEnabled(FQCN, ALWAYS, null, msg, (Throwable) null);
    }

    /**
     * Logs the specified Message at the {@code ALWAYS} level.
     *
     * @param msg the message string to be logged
     * @param t   A Throwable or null.
     */
    public void always(final Message msg, final Throwable t) {
        logger.logIfEnabled(FQCN, ALWAYS, null, msg, t);
    }

    /**
     * Logs a message object with the {@code ALWAYS} level.
     *
     * @param message the message object to log.
     */
    public void always(final Object message) {
        logger.logIfEnabled(FQCN, ALWAYS, null, message, (Throwable) null);
    }

    /**
     * Logs a message at the {@code ALWAYS} level including the stack trace of
     * the {@link Throwable} {@code t} passed as parameter.
     *
     * @param message the message to log.
     * @param t       the exception to log, including its stack trace.
     */
    public void always(final Object message, final Throwable t) {
        logger.logIfEnabled(FQCN, ALWAYS, null, message, t);
    }

    /**
     * Logs a message CharSequence with the {@code ALWAYS} level.
     *
     * @param message the message CharSequence to log.
     * @since Log4j-2.6
     */
    public void always(final CharSequence message) {
        logger.logIfEnabled(FQCN, ALWAYS, null, message, (Throwable) null);
    }

    /**
     * Logs a CharSequence at the {@code ALWAYS} level including the stack trace of
     * the {@link Throwable} {@code t} passed as parameter.
     *
     * @param message the CharSequence to log.
     * @param t       the exception to log, including its stack trace.
     * @since Log4j-2.6
     */
    public void always(final CharSequence message, final Throwable t) {
        logger.logIfEnabled(FQCN, ALWAYS, null, message, t);
    }

    /**
     * Logs a message object with the {@code ALWAYS} level.
     *
     * @param message the message object to log.
     */
    public void always(final String message) {
        logger.logIfEnabled(FQCN, ALWAYS, null, message, (Throwable) null);
    }

    /**
     * Logs a message with parameters at the {@code ALWAYS} level.
     *
     * @param message the message to log; the format depends on the message factory.
     * @param params  parameters to the message.
     * @see #getMessageFactory()
     */
    public void always(final String message, final Object... params) {
        logger.logIfEnabled(FQCN, ALWAYS, null, message, params);
    }

    /**
     * Logs a message with parameters at the {@code ALWAYS} level.
     *
     * @param message the message to log; the format depends on the message factory.
     * @param p0      parameter to the message.
     * @see #getMessageFactory()
     * @since Log4j-2.6
     */
    public void always(final String message, final Object p0) {
        logger.logIfEnabled(FQCN, ALWAYS, null, message, p0);
    }

    /**
     * Logs a message with parameters at the {@code ALWAYS} level.
     *
     * @param message the message to log; the format depends on the message factory.
     * @param p0      parameter to the message.
     * @param p1      parameter to the message.
     * @see #getMessageFactory()
     * @since Log4j-2.6
     */
    public void always(final String message, final Object p0, final Object p1) {
        logger.logIfEnabled(FQCN, ALWAYS, null, message, p0, p1);
    }

    /**
     * Logs a message with parameters at the {@code ALWAYS} level.
     *
     * @param message the message to log; the format depends on the message factory.
     * @param p0      parameter to the message.
     * @param p1      parameter to the message.
     * @param p2      parameter to the message.
     * @see #getMessageFactory()
     * @since Log4j-2.6
     */
    public void always(final String message, final Object p0, final Object p1, final Object p2) {
        logger.logIfEnabled(FQCN, ALWAYS, null, message, p0, p1, p2);
    }

    /**
     * Logs a message with parameters at the {@code ALWAYS} level.
     *
     * @param message the message to log; the format depends on the message factory.
     * @param p0      parameter to the message.
     * @param p1      parameter to the message.
     * @param p2      parameter to the message.
     * @param p3      parameter to the message.
     * @see #getMessageFactory()
     * @since Log4j-2.6
     */
    public void always(final String message, final Object p0, final Object p1, final Object p2,
                       final Object p3) {
        logger.logIfEnabled(FQCN, ALWAYS, null, message, p0, p1, p2, p3);
    }

    /**
     * Logs a message with parameters at the {@code ALWAYS} level.
     *
     * @param message the message to log; the format depends on the message factory.
     * @param p0      parameter to the message.
     * @param p1      parameter to the message.
     * @param p2      parameter to the message.
     * @param p3      parameter to the message.
     * @param p4      parameter to the message.
     * @see #getMessageFactory()
     * @since Log4j-2.6
     */
    public void always(final String message, final Object p0, final Object p1, final Object p2,
                       final Object p3, final Object p4) {
        logger.logIfEnabled(FQCN, ALWAYS, null, message, p0, p1, p2, p3, p4);
    }

    /**
     * Logs a message with parameters at the {@code ALWAYS} level.
     *
     * @param message the message to log; the format depends on the message factory.
     * @param p0      parameter to the message.
     * @param p1      parameter to the message.
     * @param p2      parameter to the message.
     * @param p3      parameter to the message.
     * @param p4      parameter to the message.
     * @param p5      parameter to the message.
     * @see #getMessageFactory()
     * @since Log4j-2.6
     */
    public void always(final String message, final Object p0, final Object p1, final Object p2,
                       final Object p3, final Object p4, final Object p5) {
        logger.logIfEnabled(FQCN, ALWAYS, null, message, p0, p1, p2, p3, p4, p5);
    }

    /**
     * Logs a message with parameters at the {@code ALWAYS} level.
     *
     * @param message the message to log; the format depends on the message factory.
     * @param p0      parameter to the message.
     * @param p1      parameter to the message.
     * @param p2      parameter to the message.
     * @param p3      parameter to the message.
     * @param p4      parameter to the message.
     * @param p5      parameter to the message.
     * @param p6      parameter to the message.
     * @see #getMessageFactory()
     * @since Log4j-2.6
     */
    public void always(final String message, final Object p0, final Object p1, final Object p2,
                       final Object p3, final Object p4, final Object p5, final Object p6) {
        logger.logIfEnabled(FQCN, ALWAYS, null, message, p0, p1, p2, p3, p4, p5, p6);
    }

    /**
     * Logs a message with parameters at the {@code ALWAYS} level.
     *
     * @param message the message to log; the format depends on the message factory.
     * @param p0      parameter to the message.
     * @param p1      parameter to the message.
     * @param p2      parameter to the message.
     * @param p3      parameter to the message.
     * @param p4      parameter to the message.
     * @param p5      parameter to the message.
     * @param p6      parameter to the message.
     * @param p7      parameter to the message.
     * @see #getMessageFactory()
     * @since Log4j-2.6
     */
    public void always(final String message, final Object p0, final Object p1, final Object p2,
                       final Object p3, final Object p4, final Object p5, final Object p6,
                       final Object p7) {
        logger.logIfEnabled(FQCN, ALWAYS, null, message, p0, p1, p2, p3, p4, p5, p6, p7);
    }

    /**
     * Logs a message with parameters at the {@code ALWAYS} level.
     *
     * @param message the message to log; the format depends on the message factory.
     * @param p0      parameter to the message.
     * @param p1      parameter to the message.
     * @param p2      parameter to the message.
     * @param p3      parameter to the message.
     * @param p4      parameter to the message.
     * @param p5      parameter to the message.
     * @param p6      parameter to the message.
     * @param p7      parameter to the message.
     * @param p8      parameter to the message.
     * @see #getMessageFactory()
     * @since Log4j-2.6
     */
    public void always(final String message, final Object p0, final Object p1, final Object p2,
                       final Object p3, final Object p4, final Object p5, final Object p6,
                       final Object p7, final Object p8) {
        logger.logIfEnabled(FQCN, ALWAYS, null, message, p0, p1, p2, p3, p4, p5, p6, p7, p8);
    }

    /**
     * Logs a message with parameters at the {@code ALWAYS} level.
     *
     * @param message the message to log; the format depends on the message factory.
     * @param p0      parameter to the message.
     * @param p1      parameter to the message.
     * @param p2      parameter to the message.
     * @param p3      parameter to the message.
     * @param p4      parameter to the message.
     * @param p5      parameter to the message.
     * @param p6      parameter to the message.
     * @param p7      parameter to the message.
     * @param p8      parameter to the message.
     * @param p9      parameter to the message.
     * @see #getMessageFactory()
     * @since Log4j-2.6
     */
    public void always(final String message, final Object p0, final Object p1, final Object p2,
                       final Object p3, final Object p4, final Object p5, final Object p6,
                       final Object p7, final Object p8, final Object p9) {
        logger.logIfEnabled(FQCN, ALWAYS, null, message, p0, p1, p2, p3, p4, p5, p6, p7, p8, p9);
    }

    /**
     * Logs a message at the {@code ALWAYS} level including the stack trace of
     * the {@link Throwable} {@code t} passed as parameter.
     *
     * @param message the message to log.
     * @param t       the exception to log, including its stack trace.
     */
    public void always(final String message, final Throwable t) {
        logger.logIfEnabled(FQCN, ALWAYS, null, message, t);
    }

    /**
     * Logs a message which is only to be constructed if the logging level is the {@code ALWAYS}level.
     *
     * @param msgSupplier A function, which when called, produces the desired log message;
     *                    the format depends on the message factory.
     * @since Log4j-2.4
     */
    public void always(final Supplier<?> msgSupplier) {
        logger.logIfEnabled(FQCN, ALWAYS, null, msgSupplier, (Throwable) null);
    }

    /**
     * Logs a message (only to be constructed if the logging level is the {@code ALWAYS}
     * level) including the stack trace of the {@link Throwable} <code>t</code> passed as parameter.
     *
     * @param msgSupplier A function, which when called, produces the desired log message;
     *                    the format depends on the message factory.
     * @param t           the exception to log, including its stack trace.
     * @since Log4j-2.4
     */
    public void always(final Supplier<?> msgSupplier, final Throwable t) {
        logger.logIfEnabled(FQCN, ALWAYS, null, msgSupplier, t);
    }

    /**
     * Logs a message which is only to be constructed if the logging level is the
     * {@code ALWAYS} level with the specified Marker.
     *
     * @param marker      the marker data specific to this log statement
     * @param msgSupplier A function, which when called, produces the desired log message;
     *                    the format depends on the message factory.
     * @since Log4j-2.4
     */
    public void always(final Marker marker, final Supplier<?> msgSupplier) {
        logger.logIfEnabled(FQCN, ALWAYS, marker, msgSupplier, (Throwable) null);
    }

    /**
     * Logs a message with parameters which are only to be constructed if the logging level is the
     * {@code ALWAYS} level.
     *
     * @param marker         the marker data specific to this log statement
     * @param message        the message to log; the format depends on the message factory.
     * @param paramSuppliers An array of functions, which when called, produce the desired log message parameters.
     * @since Log4j-2.4
     */
    public void always(final Marker marker, final String message, final Supplier<?>... paramSuppliers) {
        logger.logIfEnabled(FQCN, ALWAYS, marker, message, paramSuppliers);
    }

    /**
     * Logs a message (only to be constructed if the logging level is the {@code ALWAYS}
     * level) with the specified Marker and including the stack trace of the {@link Throwable}
     * <code>t</code> passed as parameter.
     *
     * @param marker      the marker data specific to this log statement
     * @param msgSupplier A function, which when called, produces the desired log message;
     *                    the format depends on the message factory.
     * @param t           A Throwable or null.
     * @since Log4j-2.4
     */
    public void always(final Marker marker, final Supplier<?> msgSupplier, final Throwable t) {
        logger.logIfEnabled(FQCN, ALWAYS, marker, msgSupplier, t);
    }

    /**
     * Logs a message with parameters which are only to be constructed if the logging level is
     * the {@code ALWAYS} level.
     *
     * @param message        the message to log; the format depends on the message factory.
     * @param paramSuppliers An array of functions, which when called, produce the desired log message parameters.
     * @since Log4j-2.4
     */
    public void always(final String message, final Supplier<?>... paramSuppliers) {
        logger.logIfEnabled(FQCN, ALWAYS, null, message, paramSuppliers);
    }



}

