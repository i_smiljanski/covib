package com.uptodata.isr.server.utils.fileSystem;

import javassist.CannotCompileException;
import javassist.ClassPool;
import javassist.CtClass;
import javassist.CtNewMethod;
import org.apache.commons.collections4.map.CaseInsensitiveMap;

import java.io.File;
import java.io.IOException;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.lang.reflect.Parameter;
import java.net.MalformedURLException;
import java.net.URISyntaxException;
import java.net.URL;
import java.net.URLClassLoader;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

/**
 * Created by smiljanskii60 on 27.11.2014.
 */
public class ClassLoaderUtil {
//    private static Logger log = LogManager.getLogger(ConvertUtils.class);


    public static ClassLoader getClassLoaderForJar(String dirPath) throws URISyntaxException, IOException {

        ArrayList<URL> urllist = new ArrayList<>();
        //log.debug(dirPath);
        String[] files = new File(dirPath).list();
        if (files != null) {
            for (String fileName : files) {
                File file = new File(dirPath + File.separator + fileName);
                //log.debug(file.getCanonicalPath());
                if (file.isFile() && file.getName().endsWith(".jar")) {
                    urllist.add(file.toURI().toURL());
                }
            }
        }

        URL[] urls = new URL[urllist.size()];
        urllist.toArray(urls);
//        log.debug(urllist.size());
        return new URLClassLoader(urls, ClassLoaderUtil.class.getClassLoader());
    }


    public static ClassLoader getClassLoaderSingle(String dirPath) throws MalformedURLException {
        URL[] searchPath = new URL[1];
        searchPath[0] = new File(dirPath).toURI().toURL();
        return new URLClassLoader(searchPath);
    }


    public static Class<?> loadClassAtRuntime(String className, ClassLoader classLoader) throws ClassNotFoundException {
        return Class.forName(className, true, classLoader);
    }


   /* public static void generateAndCompileClass(String path, String className, String classText) {
        try {
            File tempDir = new File(path);
            tempDir.mkdir();
            File sourceFile = new File(tempDir + File.separator + className + ".java");
            FileWriter writer = new FileWriter(sourceFile);

            writer.write(classText);
            writer.close();

            JavaCompiler compiler = ToolProvider.getSystemJavaCompiler();
            log.debug(compiler);
            StandardJavaFileManager fileManager =
                    compiler.getStandardFileManager(null, null, null);

            fileManager.setLocation(StandardLocation.CLASS_OUTPUT, Arrays.asList(tempDir));
            // Compile the file
            compiler.getTask(null,
                    fileManager,
                    null,
                    null,
                    null,
                    fileManager.getJavaFileObjectsFromFiles(Arrays.asList(sourceFile)))
                    .call();
            fileManager.close();

            // delete the source file
//            sourceFile.deleteOnExit();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }*/


    public static CtClass createClass(String className, List<String> methods) throws CannotCompileException {
        ClassPool pool = ClassPool.getDefault();
        CtClass ctClass = pool.makeClass(className);

        for (String method : methods) {
            ctClass.addMethod(CtNewMethod.make(method, ctClass));
        }
        return ctClass;
    }


    public static void saveClass(CtClass ctClass, String directoryName) throws CannotCompileException, IOException {
        ctClass.writeFile(directoryName);
    }


    public static void createAndSaveClass(String className, List<String> methods, String directoryName) throws CannotCompileException, IOException {
        CtClass ctClass = createClass(className, methods);
        saveClass(ctClass, directoryName);
    }


    public static Object invoke(Class<?> clazz, String methodName, Class<?>[] params, Object[] paramsObj)
            throws IllegalAccessException, InstantiationException, NoSuchMethodException, InvocationTargetException {
        Object iClass = clazz.newInstance();
        Method thisMethod = clazz.getDeclaredMethod(methodName, params);
        return thisMethod.invoke(iClass, paramsObj);
    }

    public static Object getInstanceByClassName(String className) {
        Object result = null;
        try {
            Class<?> clazz = Class.forName(className);
            return clazz.getConstructor().newInstance();

        } catch (Exception e) {
            e.printStackTrace();  //To change body of catch statement use File | Settings | File Templates.
        }
        return result;
    }


    public static Map<String, Object> getMethodParameters(Method method) {
        Parameter[] parameters = method.getParameters();
        Map<String, Object> parameterMap = new CaseInsensitiveMap<>();

        for (Parameter parameter : parameters) {
            System.out.println("parameter = " + parameter);
            if (!parameter.isNamePresent()) {
                throw new IllegalArgumentException("Parameter names are not present!");
            }
            String parameterName = parameter.getName();
            Object value = null;
            try {
                value = method.invoke(null, parameterName);
            } catch (Exception e) {
                e.printStackTrace();
            }
            parameterMap.put(parameterName, value);
        }
        return parameterMap;
    }

    public static Map<String, Object> getMethodParameters(Class<?> clazz, String methodName) {
        Method[] declaredMethods = clazz.getDeclaredMethods();

        for (Method declaredMethod : declaredMethods) {
            if (declaredMethod.getName().equals(methodName)) {
                return getMethodParameters(declaredMethod);
            }
        }
        return null;
    }

}
