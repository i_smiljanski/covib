package com.uptodata.isr.server.utils.fileSystem;

import java.io.File;

/**
 * Created by smiljanskii60 on 28.05.2015.
 */
public final class Constants {
    public static final String lineSeparator = System.getProperty("line.separator");
    public static final String fileSeparator = File.separator;
    public static final String ASYMMETRIC_ALGO = "RSA";
    public static final String SYMMETRIC_ALGO = "AES";
    public static final String KEY_PACKAGE = "temp";
    public static final String KEY_CLASS = KEY_PACKAGE + ".Common";
    public static final String KEY_LOG_CLASS = KEY_PACKAGE + ".CommonLog";
    public static final String JAR_FILE1 = "util1.jar";
    public static final String JAR_FILE2 = "util2.jar";
    public static String SYS_PROPERTY = "user.dir";
    public static String USER_DIR = System.getProperty(SYS_PROPERTY);
    public static final String LIB_PATH = USER_DIR + File.separator + "lib";
    //    public static final String JAR_FILE_PATH = LIB_PATH + File.separator + Constants.JAR_FILE;
    public static final String RSA_FILE = "dbToken.rsa";
    public static final String RSA_LOG_FILE = "dbTokenLog.rsa";
}
