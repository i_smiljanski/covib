package com.uptodata.isr.server.utils.logging.log4j2;

import org.apache.commons.lang.StringUtils;

import java.util.Properties;

/**
 * Created by smiljanskii60 on 15.06.2015.
 */
public class DbDataForLogging {
    String dbSchema;
    String dbPassword;
    String jdbcUrl;
    Properties properties = new Properties();

    public DbDataForLogging(String dbSchema, String dbPassword, String jdbcUrl) {

        this.dbSchema = dbSchema;
        this.dbPassword = dbPassword;
        this.jdbcUrl = jdbcUrl;
        initProperties();
        validate();
    }

    public DbDataForLogging(Properties properties) {

        this.properties = properties;
        initVariable();
        validate();
    }

    public void initProperties() {
        properties.setProperty("dbSchema", dbSchema);
        properties.setProperty("password", dbPassword);
        properties.setProperty("jdbcUrl", jdbcUrl);
    }

    public void initVariable() {
        this.dbSchema = properties.getProperty("dbSchema");
        this.dbPassword = properties.getProperty("password");
        this.jdbcUrl = properties.getProperty("jdbcUrl");
    }

    public void validate()  {
        StringBuilder validate = new StringBuilder();
        if (StringUtils.isEmpty(dbSchema)) {
            validate.append("dbSchema is null! ");
        }
        if (StringUtils.isEmpty(dbPassword)) {
            validate.append("password is null! ");
        }
        if (StringUtils.isEmpty(jdbcUrl)) {
            validate.append("jdbcUrl is nul! ");
        }
        if (validate.length() > 0) {
            throw new RuntimeException(validate.toString());
        }
    }


    public String getDbSchema() {
        return dbSchema;
    }

    public void setDbSchema(String dbSchema) {
        this.dbSchema = dbSchema;
    }

    public String getDbPassword() {
        return dbPassword;
    }

    public void setDbPassword(String dbPassword) {
        this.dbPassword = dbPassword;
    }

    public String getJdbcUrl() {
        return jdbcUrl;
    }

    public void setJdbcUrl(String jdbcUrl) {
        this.jdbcUrl = jdbcUrl;
    }

    public Properties getProperties() {
        return properties;
    }

    public void setProperties(Properties properties) {
        this.properties = properties;
    }

    public Properties getPropertiesForDriver() {
        Properties properties = new Properties();
        properties.setProperty("user", dbSchema);
        properties.setProperty("password", dbPassword);
        return properties;
    }


}
