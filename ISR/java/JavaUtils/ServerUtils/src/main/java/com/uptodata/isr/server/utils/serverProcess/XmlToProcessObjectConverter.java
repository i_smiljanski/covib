package com.uptodata.isr.server.utils.serverProcess;

import com.uptodata.isr.utils.xml.dom.XmlHandler;
import org.apache.logging.log4j.Logger;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;

import java.util.ArrayList;
import java.util.HashMap;

/**
 * Created by smiljanskii60 on 08.10.2015.
 */
public abstract class XmlToProcessObjectConverter {
    protected String processTag;
    protected String inputTagName;
    protected String transformerTagName;
    protected String resultTagName;

    public ArrayList<ProcessInfoObject> convertAll(String xml, Logger log) throws Exception {
        ArrayList<ProcessInfoObject> result = new ArrayList<>();
        XmlHandler xmlHandler = new XmlHandler(xml);
        NodeList processNodes = xmlHandler.selectNodes(processTag);
        for (int i = 0; i < processNodes.getLength(); i++) {
            Node processNode = processNodes.item(i);
            ProcessInfoObject processInfoObject = convert(xmlHandler, processNode);
            log.debug("processInfoObject = " + processInfoObject);
            result.add(processInfoObject);
        }
        return result;
    }

    public ProcessInfoObject convert(XmlHandler xmlHandler, Node processNode) {
        String inputName = xmlHandler.getTextContentByTagName(processNode, inputTagName);
        String converterName = xmlHandler.getTextContentByTagName(processNode, transformerTagName);
        String resultName = xmlHandler.getTextContentByTagName(processNode, resultTagName);
        HashMap<String, String> parameters = getParameters(xmlHandler, processNode);

        ProcessInfoObject result = new ProcessInfoObject();
        result.setInputFileName(inputName);
        result.setTransformerFileName(converterName);
        result.setResultId(resultName);
        result.setParameters(parameters);
        return result;
    }

    public abstract HashMap<String, String> getParameters(XmlHandler xmlHandler, Node xsltTransformation);
}
