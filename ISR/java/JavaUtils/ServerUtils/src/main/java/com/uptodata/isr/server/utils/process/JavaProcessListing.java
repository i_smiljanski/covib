package com.uptodata.isr.server.utils.process;

/**
 * Created by smiljanskii60 on 26.08.2014.
 */

import com.uptodata.isr.utils.ConvertUtils;
import sun.jvmstat.monitor.MonitoredHost;
import sun.jvmstat.monitor.MonitoredVm;
import sun.jvmstat.monitor.MonitoredVmUtil;
import sun.jvmstat.monitor.VmIdentifier;
import sun.tools.jps.Arguments;

import java.util.Scanner;

public class JavaProcessListing {
//    private static Logger log = LogManager.getLogger(JavaProcessListing.class);

    public static void main(String[] args) throws Exception {
//        Processes.ALL_PROCESSES.listProcesses();

        Arguments arguments = new Arguments(new String[]{"-m", "-mlvV"});

        MonitoredHost monitoredHost = MonitoredHost.getMonitoredHost(arguments.hostId());
        for (Object o : monitoredHost.activeVms()) {
            int jvmProcessId = (Integer) o;
            VmIdentifier vmIdentifier = new VmIdentifier("//" + jvmProcessId + "?mode=r");
            System.out.println("monitoredHostObject = " + o);
            MonitoredVm monitoredVm = monitoredHost.getMonitoredVm(vmIdentifier);

            System.out.println("JVM Process ID: " + jvmProcessId
                    + " Main Class: "
                    + MonitoredVmUtil.mainClass(monitoredVm, true)
                    + " Main Args: "
                    + MonitoredVmUtil.mainArgs(monitoredVm)
                    + " JVM Args: "
                    + MonitoredVmUtil.jvmArgs(monitoredVm)
                    + " JVM Flags: "
                    + MonitoredVmUtil.jvmFlags(monitoredVm)
                    + " JVM Version: "
                    + MonitoredVmUtil.vmVersion(monitoredVm));
            monitoredVm.detach();
        }
    }


    public static enum Processes implements IProcessListingStrategy {
        ALL_PROCESSES;

        private IProcessListingStrategy processListing = selectProcessListingStrategy();

        @Override
        public void listProcesses() throws Exception {
            processListing.listProcesses();
        }

        private IProcessListingStrategy selectProcessListingStrategy() {
            //todo add support for mac ...
            return isWindows() ? new WinProcessListingStrategy() : new LinuxProcessListingStrategy();
        }

        private static boolean isWindows() {
            return System.getProperty("os.name").toLowerCase().contains("win");

        }
    }

    static interface IProcessListingStrategy {
        void listProcesses() throws Exception;
    }

    static abstract class AbstractNativeProcessListingStrategy implements IProcessListingStrategy {
        @Override
        public void listProcesses() throws Exception {
            Process process = makeProcessListingProcessBuilder().start();
            Scanner scanner = new Scanner(process.getInputStream(), ConvertUtils.encoding);
            while (scanner.hasNextLine()) {
                String nextLine = scanner.nextLine();
                System.out.println(nextLine);
            }
            scanner.close();
            process.waitFor();
        }

        protected abstract ProcessBuilder makeProcessListingProcessBuilder();
    }

    static class WinProcessListingStrategy extends AbstractNativeProcessListingStrategy {
        @Override
        protected ProcessBuilder makeProcessListingProcessBuilder() {
//            return new ProcessBuilder("cmd", "/c", "tasklist /fi \"Imagename eq java.exe\"");
            return new ProcessBuilder("jps", "-mlvV");
        }
    }

    static class LinuxProcessListingStrategy extends AbstractNativeProcessListingStrategy {
        @Override
        protected ProcessBuilder makeProcessListingProcessBuilder() {
            return new ProcessBuilder("ps", "-e");
        }
    }
}