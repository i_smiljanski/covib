package com.uptodata.isr.server.utils.logging.log4j2;

import org.apache.logging.log4j.core.Filter;
import org.apache.logging.log4j.core.appender.AbstractAppender;
import org.apache.logging.log4j.core.appender.db.AbstractDatabaseAppender;
import org.apache.logging.log4j.core.appender.db.jdbc.ConnectionSource;
import org.apache.logging.log4j.core.config.Property;
import org.apache.logging.log4j.core.config.plugins.Plugin;
import org.apache.logging.log4j.core.config.plugins.PluginAttribute;
import org.apache.logging.log4j.core.config.plugins.PluginElement;
import org.apache.logging.log4j.core.config.plugins.PluginFactory;
import org.apache.logging.log4j.core.util.Booleans;

/**
 * This Appender writes logging events to a relational database using standard JDBC mechanisms. It takes a list of
 * {@link org.apache.logging.log4j.core.appender.db.jdbc.ColumnConfig}s with which it determines how to save the event data into the appropriate columns in the table.
 * A {@link org.apache.logging.log4j.core.appender.db.jdbc.ConnectionSource} plugin instance instructs the appender (and {@link org.apache.logging.log4j.core.appender.db.jdbc.JdbcDatabaseManager}) how to connect to
 * the database. This appender can be reconfigured at run time.
 *
 * @see org.apache.logging.log4j.core.appender.db.jdbc.ColumnConfig
 * @see org.apache.logging.log4j.core.appender.db.jdbc.ConnectionSource
 */
@Plugin(name = "IsrJDBC", category = "Core", elementType = "appender", printObject = true)
public final class IsrJDBCAppender extends AbstractDatabaseAppender<JdbcDatabaseManager> {
    private final String description;

    private IsrJDBCAppender(final String name, final Filter filter, final boolean ignoreExceptions,
                            final JdbcDatabaseManager manager) {
        super(name, filter, null, ignoreExceptions, Property.EMPTY_ARRAY, manager);
        this.description = this.getName() + "{ manager=" + this.getManager() + " }";
    }

    @Override
    public String toString() {
        return this.description;
    }

    /**
     * Factory method for creating a JDBC appender within the plugin manager.
     *
     * @param name             The name of the appender.
     * @param ignore           If {@code "true"} (default) exceptions encountered when appending events are logged; otherwise
     *                         they are propagated to the caller.
     * @param filter           The filter, if any, to use.
     * @param connectionSource The connections source from which database connections should be retrieved.
     * @param bufferSize       If an integer greater than 0, this causes the appender to buffer log events and flush whenever
     *                         the buffer reaches this size.
     * @param sqlStatement     The sql statement to insert log events into.
     * @return a new JDBC appender.
     */
    @PluginFactory
    public static IsrJDBCAppender createAppender(
            @PluginAttribute("name") final String name,
            @PluginAttribute("ignoreExceptions") final String ignore,
            @PluginElement("Filter") final Filter filter,
            @PluginElement("ConnectionSource") final ConnectionSource connectionSource,
            @PluginAttribute("bufferSize") final String bufferSize,
            @PluginAttribute("sql") final String sqlStatement,
            @PluginElement("StmtParamConfigs") final StmtParameterConfig[] stmtParamConfigs) {
        final int bufferSizeInt = AbstractAppender.parseInt(bufferSize, 0);
        final boolean ignoreExceptions = Booleans.parseBoolean(ignore, true);

        final StringBuilder managerName = new StringBuilder("jdbcManager{ description=").append(name)
                .append(", bufferSize=").append(bufferSizeInt).append(", connectionSource=")
                .append(connectionSource.toString()).append(", sqlStatement=").append(sqlStatement).append(", parameters=[ ");

        int i = 0;
        for (final StmtParameterConfig param : stmtParamConfigs) {
            if (i++ > 0) {
                managerName.append(", ");
            }
            managerName.append(param.toString());
        }

        managerName.append(" ] }");

        final JdbcDatabaseManager manager = JdbcDatabaseManager.getJDBCDatabaseManager(
                managerName.toString(), bufferSizeInt, connectionSource, sqlStatement, stmtParamConfigs);
        if (manager == null) {
            return null;
        }

        return new IsrJDBCAppender(name, filter, ignoreExceptions, manager);
    }
}
