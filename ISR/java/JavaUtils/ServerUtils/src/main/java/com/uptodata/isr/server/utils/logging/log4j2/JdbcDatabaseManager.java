package com.uptodata.isr.server.utils.logging.log4j2;

import org.apache.logging.log4j.core.LogEvent;
import org.apache.logging.log4j.core.appender.AppenderLoggingException;
import org.apache.logging.log4j.core.appender.ManagerFactory;
import org.apache.logging.log4j.core.appender.db.AbstractDatabaseManager;
import org.apache.logging.log4j.core.appender.db.jdbc.ConnectionSource;
import org.apache.logging.log4j.core.layout.PatternLayout;
import org.apache.logging.log4j.core.util.Closer;

import java.io.Serializable;
import java.io.StringReader;
import java.sql.Connection;
import java.sql.DatabaseMetaData;
import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;


/**
 * An {@link AbstractDatabaseManager} implementation for relational databases accessed via JDBC.
 */
public final class JdbcDatabaseManager extends AbstractDatabaseManager {

    private static final JDBCDatabaseManagerFactory INSTANCE = new JDBCDatabaseManagerFactory();

    private final ConnectionSource connectionSource;
    private final String sqlStatement;
    private final List<Parameter> parameters;
    private Connection connection;
    private PreparedStatement statement;
    private boolean isBatchSupported;

    private HashMap<Integer, String> parameterValues = new HashMap<>();  //only for info if error

    private JdbcDatabaseManager(final String name, final int bufferSize, final ConnectionSource connectionSource,
                                final String sqlStatement, final List<Parameter> parameters) {
        super(name, bufferSize);
        this.connectionSource = connectionSource;
        this.sqlStatement = sqlStatement;
        this.parameters = parameters;
    }

    @Override
    protected void startupInternal() throws Exception {
        this.connection = this.connectionSource.getConnection();
        if (this.connection != null) {
            final DatabaseMetaData metaData = this.connection.getMetaData();
            this.isBatchSupported = metaData.supportsBatchUpdates();
            Closer.closeSilently(this.connection);
        }
    }

    @Override
    protected boolean shutdownInternal() {
        if (this.connection != null || this.statement != null) {
          return this.commitAndClose();
        }
        return true;
    }

    @Override
    protected void connectAndStart() {
        try {
            this.connection = this.connectionSource.getConnection();
            if (this.connection != null) {
                this.connection.setAutoCommit(false);
                this.statement = this.connection.prepareStatement(this.sqlStatement);
            }
        } catch (final SQLException e) {
            System.err.println("Cannot write logging event or flush buffer; JDBC manager cannot connect to the database.");
//            throw new AppenderLoggingException(
//                    "Cannot write logging event or flush buffer; JDBC manager cannot connect to the database.", e
//            );
        }
    }

    @Override
    protected void writeInternal(LogEvent logEvent, Serializable serializable) {
        StringReader reader = null;
        try {
            if (!this.isRunning() || this.connection == null || this.connection.isClosed() || this.statement == null
                    || this.statement.isClosed()) {
//                System.err.println("Cannot write logging event; JDBC manager not connected to the database.");

            } else {
                parameterValues = new HashMap<>();
                for (final Parameter param : this.parameters) {
                    String msg = param.layout.toSerializable(logEvent);
//                    System.out.println("msg = [" + msg + "] " + msg.length() + " isClob:" + param.isClob);
                    parameterValues.put(param.index, msg);
                    if (param.isClob) {
                        reader = new StringReader(msg);
                        if (param.isUnicode) {
                            this.statement.setNClob(param.index, reader);
                        } else {
                            this.statement.setClob(param.index, reader);
                        }
                    } else if (param.isNumerical) {
                        this.statement.setInt(param.index, Integer.parseInt(msg));
                    } else {
                        if(msg.length() > 3999){
                            msg = msg.substring(0, 3999);
                        }
                        if (param.isUnicode) {
                            this.statement.setNString(param.index, msg);
                        } else {
                            this.statement.setString(param.index, msg);
                        }
                    }
                }

                if (this.isBatchSupported) {
                    this.statement.addBatch();
                } else if (this.statement.executeUpdate() == 0) {
                    throw new AppenderLoggingException(
                            "No records inserted in database table for log event in JDBC manager.");
                }
            }
        } catch (final SQLException e) {
            throw new AppenderLoggingException("Failed to insert record for log event in JDBC manager: " +
                    e.getMessage(), e);
        } finally {
            Closer.closeSilently(reader);
        }
    }



    @Override
    protected boolean commitAndClose() {
        try {
            if (this.connection != null && !this.connection.isClosed()) {
                if (this.isBatchSupported) {
                    this.statement.executeBatch();
                }
                this.connection.commit();
            }
            return true;
        } catch (final SQLException e) {
            LOGGER.warn("Failed to commit transaction logging event or flushing buffer. " + e
                    + "statement = " + sqlStatement + parameterValues);

//            throw new AppenderLoggingException("Failed to commit transaction logging event or flushing buffer.", e);
            return false;
        } finally {
            try {
//                System.out.println("sqlStatement = " + sqlStatement);
                Closer.close(this.statement);
            } catch (final Exception e) {
                LOGGER.warn("Failed to close SQL statement logging event or flushing buffer.", e);
            } finally {
                this.statement = null;
            }

            try {
                Closer.close(this.connection);
            } catch (final Exception e) {
                LOGGER.warn("Failed to close database connection logging event or flushing buffer.", e);
            } finally {
                this.connection = null;
            }
        }
    }

    /**
     * Creates a JDBC manager for use within the {@link org.apache.logging.log4j.core.appender.db.jdbc.JdbcAppender}, or returns a suitable one if it already exists.
     *
     * @param name             The name of the manager, which should include connection details and hashed passwords where possible.
     * @param bufferSize       The size of the log event buffer.
     * @param connectionSource The source for connections to the database.
     * @return a new or existing JDBC manager as applicable.
     */
    public static JdbcDatabaseManager getJDBCDatabaseManager(final String name, final int bufferSize,
                                                             final ConnectionSource connectionSource,
                                                             final String sqlStatement,
                                                             final StmtParameterConfig[] paramConfigs) {
        return AbstractDatabaseManager.getManager(
                name, new FactoryData(bufferSize, connectionSource, sqlStatement, paramConfigs), getFactory()
        );
    }

    private static JDBCDatabaseManagerFactory getFactory() {
        return INSTANCE;
    }

    /**
     * Encapsulates data that {@link JDBCDatabaseManagerFactory} uses to create managers.
     */
    private static final class FactoryData extends AbstractDatabaseManager.AbstractFactoryData {
        private final ConnectionSource connectionSource;
        private final String sqlStatement;
        private StmtParameterConfig[] paramConfigs;

        protected FactoryData(final int bufferSize, final ConnectionSource connectionSource,
                              final String sqlStatement, final StmtParameterConfig[] paramConfigs) {
            super(bufferSize, null);
            this.connectionSource = connectionSource;
            this.sqlStatement = sqlStatement;
            this.paramConfigs = paramConfigs;
        }
    }

    /**
     * Creates managers.
     */
    private static final class JDBCDatabaseManagerFactory implements ManagerFactory<JdbcDatabaseManager, FactoryData> {
        @Override
        public JdbcDatabaseManager createManager(final String name, final FactoryData data) {

            final List<Parameter> params = new ArrayList<>();
            int i = 0;
            for (final StmtParameterConfig param : data.paramConfigs) {
                params.add(new Parameter(param.getLayout(), param.getIndex(), param.isUnicode(), param.isNumerical(), param.isClob()));
            }
            return new JdbcDatabaseManager(name, data.getBufferSize(), data.connectionSource, data.sqlStatement, params);
        }

    }

    /**
     * Encapsulates information about a database parameter and how to persist data to it.
     */
    private static final class Parameter {
        private final PatternLayout layout;
        private final boolean isUnicode;
        private final boolean isClob;
        private final boolean isNumerical;
        private Integer index;

        private Parameter(final PatternLayout layout, final Integer index, final boolean isUnicode, final boolean isNumerical,
                          final boolean isClob) {
            this.layout = layout;
            this.isUnicode = isUnicode;
            this.isClob = isClob;
            this.isNumerical = isNumerical;
            this.index = index;
        }
    }
}