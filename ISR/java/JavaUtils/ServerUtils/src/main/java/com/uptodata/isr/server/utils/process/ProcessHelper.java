package com.uptodata.isr.server.utils.process;

import com.uptodata.isr.server.utils.fileSystem.FileSystemWorker;
import com.uptodata.isr.utils.ConvertUtils;
import com.uptodata.isr.utils.exceptions.MessageStackException;
import de.flapdoodle.embed.process.runtime.Processes;
import org.apache.commons.io.FileUtils;
import org.apache.commons.io.IOUtils;
import org.apache.commons.lang.StringUtils;
import sun.jvmstat.monitor.*;
import sun.tools.jps.Arguments;

import java.io.*;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Map;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.Future;


/**
 * Created by smiljanskii60 on 23.07.2014.
 */
public class ProcessHelper {
//    private static Logger log = LogManager.getLogger(ProcessHelper.class);

    /**
     * used JVM Process Status Tool (lists instrumented HotSpot Java virtual machines on a target system)
     * in shell: jps -mlvV
     *
     * @param mainClass java main class in searched process
     * @return find process ids
     * @throws Exception
     */
    public static List<Long> findJavaProcessesForClassAndArgs(String mainClass, String[] args) throws Exception {
        List<Long> processIds = new ArrayList<>();
        Arguments arguments = new Arguments(new String[]{"-m", "-mlvV"});

        MonitoredHost monitoredHost = MonitoredHost.getMonitoredHost(arguments.hostId());
        for (Object o : monitoredHost.activeVms()) {
            long jvmProcessId = ((Integer) o).longValue();
            VmIdentifier vmIdentifier = new VmIdentifier("//" + jvmProcessId + "?mode=r");
            String monitoredMainClass = null;
            MonitoredVm monitoredVm = null;
            try {
                monitoredVm = monitoredHost.getMonitoredVm(vmIdentifier);
                monitoredMainClass = MonitoredVmUtil.mainClass(monitoredVm, true);
//                System.out.println("monitoredMainClass = " + monitoredMainClass);
            } catch (Exception e) {
                System.out.println("Info for process number " + o + " is not available. " + e);
            }
            if (monitoredVm != null && monitoredMainClass != null && monitoredMainClass.equals(mainClass)) {
                String mainArgs = null;
                try {
                    mainArgs = MonitoredVmUtil.mainArgs(monitoredVm);
                } catch (MonitorException e) {
                    System.out.println("Info for process number " + o + " is not available. " + e);
                }
                boolean containsArgs = true;
                if (mainArgs != null) {
                    for (String arg : args) {
                        containsArgs = mainArgs.contains(arg);
                        if (!containsArgs) {
                            break;
                        }
                    }
                }
                if (containsArgs) {
                    processIds.add(jvmProcessId);
                }
            }

        }
        return processIds;
    }


    /**
     * @return number of killed processes
     * @throws Exception
     */
    public static int killProcesses(List<Long> processIds) throws Exception {
        int killedProcesses = 0;
        for (Long processId : processIds) {
            try {
                killProcessForId(processId);
                System.out.println("java process " + processId + " is killed");
            } catch (Exception e) {
                throw new MessageStackException("Process " + processId + " can't be killed: " + e);
            }
            killedProcesses++;
        }
        return killedProcesses;
    }


    public static void killProcessForId(long processId) throws IOException, InterruptedException {
        String[] cmd = new String[]{"kill -9 " + processId};
        if (isWindows()) {
            cmd = new String[]{"cmd.exe", "/C", "tskill " + processId};
        }
        Process closeProcess = new ProcessBuilder(cmd).start();
        closeProcess.waitFor();
        closeProcess.destroy();
    }


    private static boolean isWindows() {
        return System.getProperty("os.name").toLowerCase().contains("win");

    }

    public static Process startNewJavaProcess(String jvmHome, String classpath, File userDir, String optionsAsString,
                                              String mainClass, String[] arguments) throws MessageStackException {
        ProcessBuilder processBuilder = createProcessBuilder(jvmHome, classpath, userDir, optionsAsString, mainClass,
                arguments);
//        processBuilder.redirectErrorStream(true);

        File errorProcessFile = FileUtils.getFile(userDir, "error.log");
        processBuilder.redirectError(errorProcessFile);

        File outProcessDir;
        try {
            outProcessDir = FileSystemWorker.createFolder(userDir.getCanonicalPath(), "logs");
//            System.out.println("outProcessDir = " + outProcessDir);
            File outProcessFile = FileUtils.getFile(outProcessDir, "out.log");
//            System.out.println("outProcessFile = " + outProcessFile);
            processBuilder.redirectOutput(outProcessFile);
        } catch (IOException e) {
            e.printStackTrace();
        }

        try {
            Process process = processBuilder.start();
            // log.trace("a new java process with command: " + processBuilder.command() + " is started");
            if (process.isAlive()) {
                System.out.println("a new java process is started");
                return process;
            } else {
                String userMsg = "process isn't alive"
//                        +  ", command: " + processBuilder.command()  todo: hide passwords
                        ;
                throw new MessageStackException(MessageStackException.SEVERITY_CRITICAL, userMsg,
                        "ProcessHelper.startNewJavaProcess", MessageStackException.getCurrentMethod());
            }
        } catch (IOException e) {
            String userMsg = "process '" + processBuilder.command() + "' can't be started";
            // log.warn("process  can't be started, command: " + processBuilder.command());
            throw new MessageStackException(e, MessageStackException.SEVERITY_CRITICAL,
                    userMsg, MessageStackException.getCurrentMethod());
        }
    }


    public static Process startProcessAndRedirectToFile(String jvmHome, String classpath, File userDir, String optionsAsString,
                                                        String mainClass, String[] arguments, File commandsFile, File processLog, File errorLog)
            throws IOException {
        ProcessBuilder processBuilder = createProcessBuilder(jvmHome, classpath, userDir, optionsAsString, mainClass, arguments);
        processBuilder.redirectErrorStream(true);

        if (commandsFile != null) {
            processBuilder.redirectInput(processLog);
        }
        if (processLog != null) {
            processBuilder.redirectOutput(processLog);
        }
        if (errorLog != null) {
            processBuilder.redirectError(errorLog);
        }
        return processBuilder.start();
    }



    /*public static Process stopAndStartProcessWithRedirect(String jvmHome, String classpath, String mainClass, File userDir,
                                                          String optionsAsString, String[] arguments, File commandsFile, File processLog, File errorLog) {
        Process newProcess = null;
        try {
            List<Integer> processIds = findJavaProcessesForClassAndArgs(mainClass, arguments);
            int killedProcesses = ProcessHelper.killProcesses(processIds);
            log.info(killedProcesses + " processes are killed");
            newProcess = ProcessHelper.startProcessAndRedirectToFile(jvmHome, classpath, userDir, optionsAsString,
                    mainClass, arguments, commandsFile, processLog, errorLog);
        } catch (Exception e) {
            log.error(e.getMessage());
        }
        return newProcess;
    }*/


    public static Process stopAndStartProcess(String jvmHome, String classpath, String mainClass, File userDir,
                                              String optionsAsString, String[] argsForStart, String[] argsForStop) throws Exception {
        List<Long> processIds = findJavaProcessesForClassAndArgs(mainClass, argsForStop);
        int killedProcesses = ProcessHelper.killProcesses(processIds);
        System.out.println(killedProcesses + " processes are killed");
        return ProcessHelper.startNewJavaProcess(jvmHome, classpath, userDir, optionsAsString, mainClass, argsForStart);
    }


    private static ProcessBuilder createProcessBuilder(String jvmHome, String classpath, File userDir, String optionsAsString,
                                                       String mainClass, String[] arguments) {
        if (jvmHome == null) {
            jvmHome = System.getProperty("java.home");
        }
        String jvm = jvmHome + File.separator + "bin" + File.separator + "java";
        // log.debug(jvm);
        if (classpath == null) {
            classpath = System.getProperty("java.class.path");
        }
        // String workingDirectory = System.getProperty("user.dir");

        List<String> command = new ArrayList<String>();
        command.add(jvm);
        if (optionsAsString != null) {
            String[] options = optionsAsString.split(" ");
            command.addAll(Arrays.asList(options));
        }
        command.add(mainClass);
        if (arguments != null) {
            command.addAll(Arrays.asList(arguments));
        }

        ProcessBuilder processBuilder = new ProcessBuilder(command);
        Map<String, String> environment = processBuilder.environment();
        System.out.println("classpath=" + classpath);
        environment.put("CLASSPATH", classpath);
        processBuilder.directory(userDir);
        return processBuilder;
    }

    public static Long getProcessId(Process process) {
        return Processes.processId(process);
    }


    public static String readProcessMessages(InputStream is) throws Exception {
        ExecutorService executor = Executors.newCachedThreadPool();
        Future<String> futureCall = executor.submit(() -> {
            StringBuilder msg = new StringBuilder();
            try {
                BufferedReader svnOut = new BufferedReader(new InputStreamReader(is, ConvertUtils.encoding));

                String line;
                while ((line = svnOut.readLine()) != null) {
                    msg.append(line);
//                        log.debug(line);
                }

                svnOut.close();
            } catch (Exception e) {
                msg.append(e);
            }
            return msg.toString();
        });
        String result = futureCall.get();
        return result;
    }


    public static String displayDocument(String path, String document) throws IOException {
        List<String> parameterList = new ArrayList<>();
        String encoding = ConvertUtils.encoding;
        if (System.getProperty("os.name").indexOf("indows") > 0) {
            parameterList.add("cmd.exe");
            parameterList.add("/C");
            parameterList.add("start");
            //"title" because START ["title"] [/Dpath]... -otherwise you can not use path with blank
            parameterList.add("\"" + "title" + "\"");
            encoding = "CP437";
        } else {
            parameterList.add("/bin/bash");
            parameterList.add("-c");
        }
        parameterList.add("\"" + path + document + "\"");
        ProcessBuilder pb = new ProcessBuilder().command(parameterList);
        Process p = pb.start();

        InputStream is = p.getErrorStream();
        String error = IOUtils.toString(is, encoding);

        StringBuilder command = new StringBuilder();
        for (String param : parameterList) {
            command.append(param).append(" ");
        }

        if (StringUtils.isNotEmpty(error)) {
            throw new RuntimeException("Error by command '" + command.toString() + "':  " + error);
        }
        return "command '" + command.toString() + "' executed successfully";
    }

    public static String startCmdProcess(String path, String processName, String[] arguments) throws Exception {
        List<String> parameterList = new ArrayList<>();
        parameterList.add("cmd.exe");
        parameterList.add("/C");
        parameterList.add("start");
        parameterList.add("\"" + "title" + "\"");

        parameterList.add("\"" + path + processName + "\"");

        if (arguments != null) {
            parameterList.addAll(Arrays.asList(arguments));
        }

        ProcessBuilder pb = new ProcessBuilder().command(parameterList);
        Process p = pb.start();

        InputStream is = p.getErrorStream();
        String error = ConvertUtils.inputStreamToString(is);


        StringBuilder command = new StringBuilder();
        for (String param : parameterList) {
            command.append(param).append(" ");
        }

        if (StringUtils.isNotEmpty(error)) {
            throw new Exception("Error by command '" + command + "':  " + error);
        }
        return "command '" + command + "' executed successfully";
    }


    public static void main(String[] args) {
        try {
            String path = "C:\\Users\\Smiljanskii60\\";
            String document = "2020-06-03_iStudyReporter_3.6.ISRPRO";
            ProcessHelper.displayDocument(path, document);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

}
