package com.uptodata.isr.server.utils.process;


import javax.xml.ws.Endpoint;
import java.net.InetAddress;
import java.net.UnknownHostException;

/**
 * Created by smiljanskii60 on 23.07.2014.
 */
public class PublishWebservice {
    //    public static final Logger log = LoggerFactory.getLogger("PublishWebservice");
    static Endpoint endpoint;

    public static void startServer(String host, String port, String context, String wsClassName) {
//        log.debug("start server " + webservice);
        try {
            Object ws = Class.forName(wsClassName).newInstance();
            if (host == null) {
                host = InetAddress.getLocalHost().getHostAddress();
            }
            String sUrl = "http://" + host + ":" + port + "/" + context;
//            log.debug("web service starts on " + sUrl);
            endpoint = Endpoint.publish(sUrl, ws);
//            log.debug("web service started");
        } catch (UnknownHostException e) {
//            log.error(e.getMessage());
        } catch (InstantiationException e) {
//            log.error(e.getMessage());
        } catch (IllegalAccessException e) {
//            log.error(e.getMessage());
        } catch (ClassNotFoundException e) {
//            log.error(e.getMessage());
        }
    }

    public static void main(String[] args) {
        startServer(args[0], args[1], args[2], args[3]);
    }
}
