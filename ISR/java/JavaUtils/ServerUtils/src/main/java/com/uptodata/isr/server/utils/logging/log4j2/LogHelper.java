package com.uptodata.isr.server.utils.logging.log4j2;

import org.apache.commons.lang.StringUtils;
import org.apache.logging.log4j.Level;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.ThreadContext;
import org.apache.logging.log4j.core.LoggerContext;
import org.apache.logging.log4j.core.config.Configuration;

/**
 * Created by smiljanskii60 on 10.02.2015.
 */
public class LogHelper {
    public static final String defaultLevel = "ERROR";

    public static void changeLoggerLevel(String levelName, String loggerName) {
        if (StringUtils.isEmpty(levelName)) {
            levelName = defaultLevel;
        }
        LoggerContext ctx = (LoggerContext) LogManager.getContext(false);
        Configuration conf = ctx.getConfiguration();
        Level logLevel = getLevel(levelName);
        conf.getLoggerConfig(loggerName).setLevel(logLevel);
        ctx.updateLoggers(conf);
    }

    public static IsrServerLogger initLogger(String logLevel, String reference, String loggerName, String additionalInfo) {
        IsrServerLogger log = IsrServerLogger.create(loggerName);
        try {
            LogHelper.changeLoggerLevel(logLevel, loggerName);
        } catch (Exception e) {
            System.err.println("initLogger: log level could not be changed to '" + logLevel + "'. Error:" + e);
        }
        ThreadContext.put("reference", StringUtils.isEmpty(reference) ? "_" : reference);
        ThreadContext.put("logLevel", StringUtils.isEmpty(logLevel) ? defaultLevel : logLevel);
        if (StringUtils.isNotEmpty(additionalInfo)) {
            ThreadContext.put("additionalInfo", additionalInfo);
        }
        return log;
    }

    public static IsrServerLogger initLogger(String logLevel, String reference) {
        return initLogger(logLevel, reference, LogManager.ROOT_LOGGER_NAME, "");
    }


    public static Level getLevel(String levelName) {
        Level logLevel;
        String upperLevelName = levelName.toUpperCase().trim();
        if ("DEBUG_ALL".equals(upperLevelName)) {
            logLevel = Level.TRACE;
        } else {
            logLevel = Level.getLevel(upperLevelName);
        }
        return logLevel;
    }

    public static void setActionTaken(String actionTaken) {
        ThreadContext.put("taken", actionTaken);
    }

    public static void setLogFileName(String logFileName) {
        ThreadContext.put("logFileName", logFileName);
    }

    public static void setAdditionalInfo(String additionalInfo) {
        ThreadContext.put("additionalInfo", additionalInfo);
    }
}
