package com.uptodata.isr.server.utils.security;

import com.uptodata.isr.server.utils.fileSystem.ClassLoaderUtil;
import com.uptodata.isr.server.utils.fileSystem.Constants;
import com.uptodata.isr.server.utils.fileSystem.FileSystemWorker;
import com.uptodata.isr.utils.ConvertUtils;
import javassist.CannotCompileException;
import net.lingala.zip4j.core.ZipFile;
import net.lingala.zip4j.exception.ZipException;
import net.lingala.zip4j.model.ZipParameters;
import net.lingala.zip4j.util.Zip4jConstants;
import org.apache.commons.codec.binary.Base64;

import javax.crypto.Cipher;
import javax.crypto.KeyGenerator;
import javax.crypto.SecretKey;
import java.io.*;
import java.security.*;
import java.security.spec.PKCS8EncodedKeySpec;
import java.security.spec.X509EncodedKeySpec;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Properties;


public class ProtectionUtil {

    /**
     * Generate a RSA pair key (public.key, private.key) and stores
     * it in files.
     *
     * @param privateKeyFileName name of the private key
     * @param publicKeyFileName  name of the public keypublicKeyFileName
     */
    public static void generateKeyPair(String privateKeyFileName, String publicKeyFileName, int rsaKeySize)
            throws NoSuchAlgorithmException, IOException {
        generateKeyPair(new FileOutputStream(privateKeyFileName), new FileOutputStream(publicKeyFileName), rsaKeySize);
    }


    public static byte[] generateKeyPair(OutputStream privateKeyStream, int rsaKeySize)
            throws NoSuchAlgorithmException, IOException {
        try {
            KeyPairGenerator keyPairGen = KeyPairGenerator.getInstance(Constants.ASYMMETRIC_ALGO);
            keyPairGen.initialize(rsaKeySize);
            KeyPair keyPair = keyPairGen.generateKeyPair();
            try (ObjectOutputStream out = new ObjectOutputStream(privateKeyStream)) {
                out.writeObject(keyPair.getPrivate());
            }
            return keyPair.getPublic().getEncoded();
        } finally {
            privateKeyStream.close();
        }
    }

    /**
     * Generiere privaten und oeffentlichen RSA-Schluessel (Streams werden mit close() geschlossen)
     */
    public static void generateKeyPair(OutputStream privateKeyStream, OutputStream publicKeyStream, int rsaKeySize)
            throws NoSuchAlgorithmException, IOException {
        try {
            KeyPairGenerator keyPairGen = KeyPairGenerator.getInstance(Constants.ASYMMETRIC_ALGO);
            keyPairGen.initialize(rsaKeySize);
            KeyPair keyPair = keyPairGen.generateKeyPair();
            ObjectOutputStream out = new ObjectOutputStream(publicKeyStream);
            try {
                out.writeObject(keyPair.getPublic());
            } finally {
                out.close();
            }
            out = new ObjectOutputStream(privateKeyStream);
            try {
                out.writeObject(keyPair.getPrivate());
            } finally {
                out.close();
            }
        } finally {
            privateKeyStream.close();
            publicKeyStream.close();
        }
    }

    /**
     * Encrypt a file with AES using the public RSA key.
     */

    public static void encrypt(InputStream publicKeyStream, InputStream inputStream, OutputStream encryptedStream)
            throws GeneralSecurityException, ClassNotFoundException, IOException {
        try {
            KeyGenerator keyGen = KeyGenerator.getInstance(Constants.SYMMETRIC_ALGO);
            keyGen.init(Math.min(256, Cipher.getMaxAllowedKeyLength(Constants.SYMMETRIC_ALGO)));
            SecretKey symKey = keyGen.generateKey();
            Key publicKey;

            try (ObjectInputStream keyIn = new ObjectInputStream(publicKeyStream)) {
                publicKey = (Key) keyIn.readObject();
            }

            transformCipher(inputStream, encryptedStream, publicKey, symKey);
        } finally {
            publicKeyStream.close();
            inputStream.close();
            encryptedStream.close();
        }
    }


    /**
     * Asymmetric algorithm
     *
     * @param inputStream
     * @param encryptedStream
     * @param rsaKeySize
     * @throws GeneralSecurityException
     * @throws ClassNotFoundException
     * @throws IOException
     */
    public static void encrypt(InputStream inputStream, OutputStream encryptedStream, String keyClassName, String zipName,
                               int rsaKeySize, String libPath) throws IOException, ZipException, CannotCompileException, GeneralSecurityException, ClassNotFoundException {
        File classFile = new File(libPath + File.separator + keyClassName);
        Class clazz = null;
        if (classFile.canRead() && classFile.length() > 0) {
            clazz = ClassLoaderUtil.loadClassAtRuntime(keyClassName,
                    ClassLoaderUtil.getClassLoaderSingle(libPath));
        }
        Key publicKey;
        if (clazz == null) {
            publicKey = generateKeys(rsaKeySize, keyClassName, zipName, libPath);
        } else {
            Class params[] = {};
            Object paramsObj[] = {};
            publicKey = getKey(clazz, "getB", params, paramsObj, true);
        }
        KeyGenerator keyGen = KeyGenerator.getInstance(Constants.SYMMETRIC_ALGO);
        keyGen.init(Math.min(256, Cipher.getMaxAllowedKeyLength(Constants.SYMMETRIC_ALGO)));
        SecretKey symKey = keyGen.generateKey();

        transformCipher(inputStream, encryptedStream, publicKey, symKey);
    }

    private static void transformCipher(InputStream inputStream, OutputStream encryptedStream, Key publicKey, SecretKey symKey) throws IOException, GeneralSecurityException {
        Cipher cipher = Cipher.getInstance(Constants.ASYMMETRIC_ALGO);
        cipher.init(Cipher.WRAP_MODE, publicKey);
        byte[] wrappedKey = cipher.wrap(symKey);

        try (DataOutputStream dataOutputStream = new DataOutputStream(encryptedStream)) {
            dataOutputStream.writeInt(wrappedKey.length);
            dataOutputStream.write(wrappedKey);
            cipher = Cipher.getInstance(Constants.SYMMETRIC_ALGO);
            cipher.init(Cipher.ENCRYPT_MODE, symKey);
            transform(inputStream, dataOutputStream, cipher);
        }
    }

    public static void encrypt(InputStream inputStream, OutputStream encryptedStream, String keyClassName, String zipName,
                               int rsaKeySize) throws ClassNotFoundException, GeneralSecurityException, CannotCompileException, ZipException, IOException {
        encrypt(inputStream, encryptedStream, keyClassName, zipName, rsaKeySize, Constants.LIB_PATH);
    }

    private static PublicKey generateKeys(int rsaKeySize, String keyClassName, String encryptedZipName, String libPath) throws NoSuchAlgorithmException, CannotCompileException, IOException, ZipException {

        KeyPairGenerator keyPairGen = KeyPairGenerator.getInstance(Constants.ASYMMETRIC_ALGO);
        keyPairGen.initialize(rsaKeySize);
        KeyPair keyPair = keyPairGen.generateKeyPair();
        String privateKey = Base64.encodeBase64String(keyPair.getPrivate().getEncoded()).replaceAll(System.getProperty("line.separator"), "");
        String publicKey = Base64.encodeBase64String(keyPair.getPublic().getEncoded()).replaceAll(System.getProperty("line.separator"), "");
        List<String> methods = new ArrayList<>();
        methods.add("    public String getA() {\n" +
                "        return \"" + privateKey + "\";\n" +
                "    }");
        methods.add("    public String getB() {\n" +
                "        return \"" + publicKey + "\";\n" +
                "    }");
        ClassLoaderUtil.createAndSaveClass(keyClassName, methods, libPath);
        File keyPack = new File(libPath + File.separator + Constants.KEY_PACKAGE);
        packFolderToZipWithEncryption(libPath + File.separator + encryptedZipName, keyPack, Constants.SYS_PROPERTY);
        FileSystemWorker.removeFolder(keyPack);
        return keyPair.getPublic();
    }

    /**
     * Decrypt a file with the private key.
     *
     * @param encryptedFile
     * @param outputFile
     */
    public static void decrypt(String encryptedFile, String outputFile, String keyClassName, String zipName)
            throws GeneralSecurityException, ClassNotFoundException, IOException, ZipException {
        InputStream is = null;
        OutputStream os = null;
        try {
            is = new FileInputStream(encryptedFile);
            os = new FileOutputStream(outputFile);
            decrypt(is, os, keyClassName, zipName);
        } finally {
            if (is != null) {
                is.close();
            }
            if (os != null) {
                os.close();
            }
        }

    }

    public static void decrypt(InputStream encryptedStream, OutputStream outputStream, String keyClassName,
                               String encryptedZipName) throws ClassNotFoundException, GeneralSecurityException, ZipException, IOException {
        decrypt(encryptedStream, outputStream, keyClassName, encryptedZipName, Constants.LIB_PATH);
    }


    public static void decrypt(InputStream encryptedStream, OutputStream outputStream, String keyClassName,
                               String encryptedZipName, String libPath)
            throws GeneralSecurityException, ClassNotFoundException, IOException, ZipException {
        try {
            DataInputStream in = new DataInputStream(encryptedStream);
            try {
                int length = in.readInt();
                byte[] wrappedKey = new byte[length];
                in.read(wrappedKey, 0, length);

                FileSystemWorker.unpackEncryptedZip(libPath + File.separator + encryptedZipName,
                        libPath, Constants.SYS_PROPERTY);
                Class clazz = ClassLoaderUtil.loadClassAtRuntime(keyClassName,
                        ClassLoaderUtil.getClassLoaderSingle(libPath));
                Class params[] = {};
                Object paramsObj[] = {};
                Key privateKey = getKey(clazz, "getA", params, paramsObj, false);

                Cipher cipher = Cipher.getInstance(Constants.ASYMMETRIC_ALGO);
                cipher.init(Cipher.UNWRAP_MODE, privateKey);
                Key symKey = cipher.unwrap(wrappedKey, Constants.SYMMETRIC_ALGO, Cipher.SECRET_KEY);

                cipher = Cipher.getInstance(Constants.SYMMETRIC_ALGO);
                cipher.init(Cipher.DECRYPT_MODE, symKey);
                transform(in, outputStream, cipher);
                FileSystemWorker.removeFolder(new File(libPath + File.separator + Constants.KEY_PACKAGE));
            } finally {
                in.close();
            }
        } finally {
            encryptedStream.close();
            outputStream.close();
        }
    }


    private static Key getKey(Class clazz, String methodName, Class[] params, Object paramsObj[], boolean isPublic) {
        Key key = null;
        try {
            Object ret = ClassLoaderUtil.invoke(clazz, methodName, params, paramsObj);
            KeyFactory keyFactory = KeyFactory.getInstance(Constants.ASYMMETRIC_ALGO);


            if (isPublic) {
                X509EncodedKeySpec keySpec = new X509EncodedKeySpec(Base64.decodeBase64((String) ret));
                key = keyFactory.generatePublic(keySpec);
            } else {
                PKCS8EncodedKeySpec keySpec = new PKCS8EncodedKeySpec(Base64.decodeBase64((String) ret));
                key = keyFactory.generatePrivate(keySpec);
            }

//            ObjectInputStream keyIn = new ObjectInputStream(new ByteArrayInputStream(new BASE64Decoder().decodeBuffer((String) ret)));
//            key = (Key) keyIn.readObject();

        } catch (Exception e) {
            e.printStackTrace();
        }
        return key;
    }

    private static void transform(InputStream in, OutputStream out, Cipher cipher)
            throws IOException, GeneralSecurityException {
        int blockSize = cipher.getBlockSize();
        byte[] input = new byte[blockSize];
        byte[] output = new byte[cipher.getOutputSize(blockSize)];
        int len;
        while ((len = in.read(input)) == blockSize) {
            int outLength = cipher.update(input, 0, blockSize, output);
            out.write(output, 0, outLength);
        }
        out.write((len > 0) ? cipher.doFinal(input, 0, len) : cipher.doFinal());
    }


    /**
     * stores encrypted properties in the file "rsaFile" and key file "zipFile"
     * @param properties properties to encrypt and save
     * @param rsaFile   rsa file name
     * @param keyClass  name of class for generated key
     * @param zipFile   name of zip file with class "keyClass"
     * @throws Exception
     */
    public static void saveEncryptedProperties(Properties properties, String rsaFile, String keyClass, String zipFile) throws IOException, ClassNotFoundException, GeneralSecurityException, CannotCompileException, ZipException {
        ByteArrayOutputStream servletStream = new ByteArrayOutputStream();
        properties.store(servletStream, null);
        byte[] servletBytes = servletStream.toByteArray();
        if (servletBytes == null) {
            throw new RuntimeException();
        }
        encrypt(new ByteArrayInputStream(servletBytes), new FileOutputStream(rsaFile), keyClass, zipFile, 1024);
    }


    public static void main(String[] args)  {
        try {
            List<String> methods = new ArrayList<>();
            methods.add("    public String getA() {\n" +
                    "        return \"" + "ddd" + "\";\n" +
                    "    }");
            methods.add("    public String getB() {\n" +
                    "        return \"" + "fff" + "\";\n" +
                    "    }");
            String libPath = Constants.LIB_PATH;
            ClassLoaderUtil.createAndSaveClass(Constants.KEY_CLASS, methods, libPath);
            File keyPack = new File(libPath + File.separator + Constants.KEY_PACKAGE);
            packFolderToZipWithEncryption(libPath + File.separator + Constants.JAR_FILE1,
                    keyPack, Constants.SYS_PROPERTY);
            FileSystemWorker.removeFolder(keyPack);

            FileSystemWorker.unpackEncryptedZip(libPath + File.separator + Constants.JAR_FILE1, libPath, Constants.SYS_PROPERTY);
            Class clazz = ClassLoaderUtil.loadClassAtRuntime(Constants.KEY_CLASS,
                    ClassLoaderUtil.getClassLoaderSingle(libPath));
            System.out.println("clazz = " + Arrays.asList(clazz.getMethods()));
            FileSystemWorker.removeFolder(new File(libPath + File.separator + Constants.KEY_PACKAGE));

        } catch (Exception e) {
            e.printStackTrace();
        }
    }


    public static void packFolderToZipWithEncryption(String zipPath, File folder, String pwd) throws ZipException, IOException {
        ZipParameters parameters = new ZipParameters();
        parameters.setCompressionMethod(Zip4jConstants.COMP_DEFLATE);
        parameters.setCompressionLevel(Zip4jConstants.DEFLATE_LEVEL_NORMAL);
        parameters.setEncryptFiles(true);
        parameters.setEncryptionMethod(Zip4jConstants.ENC_METHOD_AES);
        parameters.setAesKeyStrength(Zip4jConstants.AES_STRENGTH_256);

        parameters.setPassword(pwd);
        boolean isFileDeleted = FileSystemWorker.deleteFile(zipPath);
//        System.out.println(zipPath + " isFileDeleted = " + isFileDeleted);
        if (isFileDeleted) {  //ISRC-1129
            ZipFile zipFile = new ZipFile(zipPath);
            zipFile.addFolder(folder, parameters);
        } else {
            throw new RuntimeException("File '" + zipPath + "' could'not be deleted");
        }
    }

}