package com.uptodata.isr.server.utils.logging.log4j2;

import org.apache.logging.log4j.Logger;
import org.apache.logging.log4j.core.appender.rolling.RollingFileManager;
import org.apache.logging.log4j.core.appender.rolling.RolloverDescription;
import org.apache.logging.log4j.core.appender.rolling.RolloverStrategy;
import org.apache.logging.log4j.core.config.plugins.PluginAttribute;
import org.apache.logging.log4j.core.config.plugins.PluginFactory;
import org.apache.logging.log4j.status.StatusLogger;

import java.io.File;
import java.io.FileFilter;
import java.nio.file.Files;
import java.nio.file.attribute.BasicFileAttributes;
import java.util.Calendar;
import java.util.Date;

/**
 * Created by smiljanskii60 on 16.06.2015.
 */
@org.apache.logging.log4j.core.config.plugins.Plugin(name = "DeleteMaxAgeFilesStrategy", category = "Core", printObject = true)
public class DeleteMaxAgeFilesStrategy implements RolloverStrategy {

    private static final int DEFAULT_MAX_AGE = 14;
    protected static final Logger LOGGER = StatusLogger.getLogger();

    private final int maxAgeIndex;

    public DeleteMaxAgeFilesStrategy(int maxAgeIndex) {
        this.maxAgeIndex = maxAgeIndex;
        LOGGER.debug("maxAgeIndex = " + maxAgeIndex);
        System.out.println("maxAgeIndex = " + maxAgeIndex);
    }


    @Override
    public RolloverDescription rollover(RollingFileManager manager) throws SecurityException {
        System.out.println("rollover: manager = " + manager);
        purgeMaxAgeFiles(maxAgeIndex, manager);
        return null;
    }


    @PluginFactory
    public static DeleteMaxAgeFilesStrategy createStrategy(
            @PluginAttribute("maxAge") final String maxAge) {

        int maxAgeIndex = DEFAULT_MAX_AGE;
        if (maxAge != null) {
            maxAgeIndex = Integer.parseInt(maxAge);
        }
        return new DeleteMaxAgeFilesStrategy(maxAgeIndex);
    }

    /**
     * Purge files older than defined maxAge. If file older than current date - maxAge delete them or else keep it.
     *
     * @param maxAgeIndex maxAge Index
     * @param manager     The RollingFileManager
     */
    private void purgeMaxAgeFiles(final int maxAgeIndex, final RollingFileManager manager) {
        String filename = manager.getFileName();
        System.out.println("filename1 = " + filename);
        File file = new File(filename);
        Calendar cal = Calendar.getInstance();
        cal.add(Calendar.DATE, -maxAgeIndex);
        Date cutoffDate = cal.getTime();
        LOGGER.debug("cutoffDate = " + cutoffDate);

        if (file.getParentFile().exists()) {
            filename = file.getName().replaceAll("\\..*", "");
            LOGGER.debug("filename2 = " + filename);
            File[] files = file.getParentFile().listFiles(
                    new StartsWithFileFilter(filename, false));
            if (files != null) {
                for (File file1 : files) {
                    try {
                        BasicFileAttributes attr = Files.readAttributes(file1.toPath(), BasicFileAttributes.class);
                        LOGGER.debug("attr = " + attr.creationTime());
                        if (new Date(attr.creationTime().toMillis()).before(cutoffDate)) {
                            file1.delete();
                        }
                    } catch (Exception e) {
                        System.err.println("Unable to delete old log files at rollover " + e);
                    }
                }
            }
        }
    }

    static class StartsWithFileFilter implements FileFilter {
        private final String startsWith;
        private final boolean inclDirs;

        public StartsWithFileFilter(String startsWith, boolean includeDirectories) {
            super();
            this.startsWith = startsWith.toUpperCase();
            inclDirs = includeDirectories;
        }

        /*
         * (non-Javadoc)
         *
         * @see java.io.FileFilter#accept(java.io.File)
         */
        public boolean accept(File pathname) {
            if (!inclDirs && pathname.isDirectory()) {
                return false;
            } else
                return pathname.getName().toUpperCase().startsWith(startsWith);
        }
    }
}

