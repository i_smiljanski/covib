package com.uptodata.isr.server.utils.logging.log4j2;

import com.uptodata.isr.server.utils.fileSystem.Constants;
import com.uptodata.isr.server.utils.security.ProtectionUtil;
import net.lingala.zip4j.exception.ZipException;
import org.apache.commons.dbcp.DriverManagerConnectionFactory;
import org.apache.commons.dbcp.PoolableConnectionFactory;
import org.apache.commons.dbcp.PoolingDataSource;
import org.apache.commons.pool.impl.GenericObjectPool;

import javax.sql.DataSource;
import java.io.*;
import java.security.GeneralSecurityException;
import java.sql.Connection;
import java.util.Properties;

public class Log4jConnectionFactory {
    private static interface Singleton {
        final Log4jConnectionFactory INSTANCE = new Log4jConnectionFactory();
    }

    private final DataSource dataSource;

    private Log4jConnectionFactory() {
        Properties connProps = new Properties();
        GenericObjectPool pool = new GenericObjectPool();
        try {
            File token = new File(Constants.RSA_LOG_FILE);
            if (token.canRead() && token.length() > 0) {
                ByteArrayOutputStream bos = new ByteArrayOutputStream();
                try {
                    ProtectionUtil.decrypt(new FileInputStream(token), bos, Constants.KEY_LOG_CLASS, Constants.JAR_FILE2);
                } catch (GeneralSecurityException | ClassNotFoundException | IOException | ZipException e1) {
                    throw new Exception("Log4jConnectionFactory: error by decrypting of token " + Constants.RSA_LOG_FILE + ". " + e1);
                }

                try {
                    connProps.load(new ByteArrayInputStream(bos.toByteArray()));
                } catch (IOException e1) {
                    throw new Exception("Log4jConnectionFactory: error by loading connection properties file.");
                }

                DbDataForLogging dbData = new DbDataForLogging(connProps);

                DriverManagerConnectionFactory connectionFactory = new DriverManagerConnectionFactory(
                        dbData.getJdbcUrl(), dbData.getPropertiesForDriver());

                new PoolableConnectionFactory(
                        connectionFactory, pool, null, "SELECT 1 FROM DUAL", 3, false, false, Connection.TRANSACTION_READ_COMMITTED
                );
            } else {
                throw new Exception("Could not write database logging event; file '" + token + "' could not be read");
//                System.out.println("file " + token + " could not be read");
            }
        } catch (Exception e) {
            System.err.println("Log4jConnectionFactory: e = " + e);
        }
        this.dataSource = new PoolingDataSource(pool);
    }


    public static Connection getDatabaseConnection() {
        try {
            //Connection conn = Singleton.INSTANCE.dataSource.getConnection(); !!Attention!! Call getConnection() is forbidden! It hangs.
//            System.out.println("serverUtils (Log4jConnectionFactory.getDatabaseConnection): connection = " + conn);
            Log4jConnectionFactory inst = Singleton.INSTANCE;
            if (inst == null) {
                throw new Exception("Log4jConnectionFactory: log4j connection instance is null!");
            }
            return inst.dataSource.getConnection();
        } catch (Exception e) {
//            System.err.println("Log4jConnectionFactory: Cannot write logging event in the database: connection is unknown. " + e);
        }
        return null;
    }

}