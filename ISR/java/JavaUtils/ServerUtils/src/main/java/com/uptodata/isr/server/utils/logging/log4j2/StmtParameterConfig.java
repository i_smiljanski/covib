package com.uptodata.isr.server.utils.logging.log4j2;

import org.apache.logging.log4j.Logger;
import org.apache.logging.log4j.core.config.Configuration;
import org.apache.logging.log4j.core.config.plugins.Plugin;
import org.apache.logging.log4j.core.config.plugins.PluginAttribute;
import org.apache.logging.log4j.core.config.plugins.PluginConfiguration;
import org.apache.logging.log4j.core.config.plugins.PluginFactory;
import org.apache.logging.log4j.core.layout.PatternLayout;
import org.apache.logging.log4j.core.util.Booleans;
import org.apache.logging.log4j.status.StatusLogger;
import org.apache.logging.log4j.util.Strings;


/**
 * A configuration element used to configure which event properties are logged to which parameter in the database statement.
 */
@Plugin(name = "StmtParameter", category = "Core", printObject = true)
public class StmtParameterConfig {
    private static final Logger LOGGER = StatusLogger.getLogger();

    private final Integer index;
    private final PatternLayout layout;
    private final String literalValue;
    private final boolean unicode;
    private final boolean numerical;
    private final boolean clob;

    private StmtParameterConfig(final Integer index, final PatternLayout layout, final String literalValue,
                                final boolean unicode, final boolean numerical, final boolean clob) {
        this.index = index;
        this.layout = layout;
        this.literalValue = literalValue;
        this.unicode = unicode;
        this.numerical = numerical;
        this.clob = clob;
    }

    public Integer getIndex() {
        return index;
    }

    public PatternLayout getLayout() {
        return this.layout;
    }

    public String getLiteralValue() {
        return this.literalValue;
    }

    public boolean isUnicode() {
        return this.unicode;
    }

    public boolean isClob() {
        return this.clob;
    }

    public boolean isNumerical() {
        return numerical;
    }

    @Override
    public String toString() {
        return "StmtParameterConfig{" +
                "index=" + index +
                ", layout=" + layout +
                ", literalValue='" + literalValue + '\'' +
                ", unicode=" + unicode +
                ", numerical=" + numerical +
                ", clob=" + clob +
                '}';
    }

    /**
     * Factory method for creating a column config within the plugin manager.
     *
     * @param config       The configuration object
     * @param number       The number of the database parameter .
     * @param pattern      The {@link PatternLayout} pattern to insert in this column. Mutually exclusive with
     *                     {@code literalValue!=null} and {@code eventTimestamp=true}
     * @param literalValue The literal value to insert into the column as-is without any quoting or escaping. Mutually
     *                     exclusive with {@code pattern!=null} and {@code eventTimestamp=true}.
     * @param unicode      If {@code "true"}, indicates that the column is a unicode String.
     * @param clob         If {@code "true"}, indicates that the column is a character LOB (CLOB).
     * @return the created column config.
     */
    @PluginFactory
    public static StmtParameterConfig createParameterConfig(
            @PluginConfiguration final Configuration config,
            @PluginAttribute("index") final String number,
            @PluginAttribute("pattern") final String pattern,
            @PluginAttribute("literal") final String literalValue,
            @PluginAttribute("isUnicode") final String unicode,
            @PluginAttribute("isNumerical") final String numerical,
            @PluginAttribute("isClob") final String clob) {
        if (Strings.isEmpty(number)) {
            System.err.println("The parameter config is not valid because it does not contain a parameter number.");
            return null;
        }

        final Integer index = Integer.parseInt(number);
        final boolean isPattern = Strings.isNotEmpty(pattern);
        final boolean isLiteralValue = Strings.isNotEmpty(literalValue);
        final boolean isUnicode = Booleans.parseBoolean(unicode, true);
        final boolean isNumber = Booleans.parseBoolean(numerical, false);
        final boolean isClob = Boolean.parseBoolean(clob);

        if (isPattern && isLiteralValue) {
            System.err.println("The pattern, literal, and isEventTimestamp attributes are mutually exclusive.");
           return null;
        }


        if (isLiteralValue) {
            return new StmtParameterConfig(index, null, literalValue, false, isNumber, false);
        }
        if (isPattern) {
            final PatternLayout layout =
                    PatternLayout.newBuilder()
                            .withPattern(pattern)
                            .withConfiguration(config)
                            .withAlwaysWriteExceptions(false)
                            .build();
            return new StmtParameterConfig(index, layout, null, isUnicode, isNumber, isClob);
        }

        System.err.println("To configure a column you must specify a pattern or literal or set isEventDate to true.");
        return null;
    }
}
