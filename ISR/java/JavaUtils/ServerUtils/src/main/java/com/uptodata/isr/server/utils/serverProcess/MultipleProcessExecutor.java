package com.uptodata.isr.server.utils.serverProcess;

import com.uptodata.isr.server.utils.logging.log4j2.IsrServerLogger;
import com.uptodata.isr.server.utils.logging.log4j2.LoggerInterface;
import com.uptodata.isr.utils.exceptions.MessageStackException;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.UUID;
import java.util.concurrent.*;

/**
 * Created by smiljanskii60 on 08.10.2015.
 */
public abstract class MultipleProcessExecutor implements LoggerInterface {
    protected String logLevel;
    protected String logReference;
    protected String logAdditional;

    public MultipleProcessExecutor(String logLevel, String logReference, String logAdditional) {
        this.logLevel = logLevel;
        this.logReference = logReference;
        this.logAdditional = logAdditional;
    }

    public HashMap<ProcessInfoObject, Boolean> convertXmlAndExecuteProcess(
            String xml, XmlToProcessObjectConverter xmlConverter, String logLevel, String logReference)
            throws MessageStackException {
        IsrServerLogger log = initLogger(logLevel, logReference);
        ArrayList<ProcessInfoObject> processInfoObjects;
        try {
            processInfoObjects = xmlConverter.convertAll(xml, log);
        } catch (Exception e) {
            String errorMessage = "Can not convertAll xml string to transform info objects! " + e;
            MessageStackException stackException = new MessageStackException(e,
                    MessageStackException.SEVERITY_CRITICAL, errorMessage,
                    MessageStackException.getCurrentMethod());
            throw stackException;
        }
        HashMap<ProcessInfoObject, Boolean> results = executeProcesses(processInfoObjects, log);
        return results;
    }


    public HashMap<ProcessInfoObject, Boolean> executeProcesses(ArrayList<ProcessInfoObject> processInfoObjects,
                                                                IsrServerLogger log)
            throws MessageStackException {

        HashMap<ProcessInfoObject, Boolean> results = new HashMap<>(processInfoObjects.size());
        int cores = Runtime.getRuntime().availableProcessors();
        class NamedThreadFactory implements ThreadFactory {
            public Thread newThread(Runnable r) {
                UUID uuid = UUID.randomUUID();
                return new Thread(r, "ThreadPool for executeProcesses: " + uuid.toString());
            }
        }
//        ExecutorService executorService = Executors.newSingleThreadExecutor(new NamedThreadFactory());
        ExecutorService executorService = Executors.newFixedThreadPool(cores);

        for (ProcessInfoObject processObject : processInfoObjects) {
            Callable<Boolean> thread = null;
            boolean wasCreated = true;
            try {
                thread = createThread(processObject);
            } catch (Exception e) {
                results.put(processObject, Boolean.FALSE);
                wasCreated = false;
                log.error("Can not create thread for process object " + processObject + ". Exception " + e);
            }
            if (wasCreated) {
                Future<Boolean> result = executorService.submit(thread);
                try {
                    results.put(processObject, result.get());
                } catch (Exception e) {
                    results.put(processObject, Boolean.FALSE);
                    log.error("Error by submit of thread for process object " + processObject + ". Exception " + e);
//                    throw new MessageStackException("Error in thread, " + processObject + ", " + e);
                }
            }
        }
        executorService.shutdown();
        try {
            executorService.awaitTermination(java.lang.Long.MAX_VALUE, TimeUnit.NANOSECONDS);
        } catch (InterruptedException e) {
            String errorString = "InterruptedException " + e;
            log.error(errorString);
            throw new MessageStackException(errorString);
        }

        return results;
    }

    protected abstract ProcessCallable createThread(ProcessInfoObject processObject);


    protected static abstract class ProcessCallable implements Callable<Boolean> {
        ProcessInfoObject processObject;

        protected abstract void onExecuteFail(Exception e) throws MessageStackException;

        protected abstract boolean executeProcess() throws MessageStackException;

        public ProcessCallable(ProcessInfoObject processObject) {
            this.processObject = processObject;
        }

        @Override
        public Boolean call() throws MessageStackException {
            boolean result = false;
            try {
                result = executeProcess();
            } catch (Exception e) {
                onExecuteFail(e);
            }
            return result;
        }
    }

    ;

}
