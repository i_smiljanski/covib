package com.uptodata.isr.server.utils.logging.log4j2;

import org.apache.commons.lang.StringUtils;
import org.apache.logging.log4j.LogManager;

import javax.jws.WebMethod;

/**
 * Created by smiljanskii60 on 07.07.2015.
 */
public interface LoggerInterface {
    public static final String defaultLevel = "ERROR";
    public static final String METHOD_BEGIN = "begin";
    public static final String METHOD_END = "end";
    public static final String defaultReference = "java";

    @WebMethod(exclude = true)
    public default IsrServerLogger initLogger(String logLevel, String logReference) {
        IsrServerLogger log = initLoggerForName(logLevel, logReference, LogManager.ROOT_LOGGER_NAME, null);
        return log;
    }

    @WebMethod(exclude = true)
    public default IsrServerLogger initLogger(String logLevel, String logReference, String additionalInfo) {
        IsrServerLogger log = initLoggerForName(logLevel, logReference, LogManager.ROOT_LOGGER_NAME, additionalInfo);
        return log;
    }

    @WebMethod(exclude = true)
    public default IsrServerLogger initLoggerForName(String logLevel, String logReference, String loggerName, String additionalInfo) {
        IsrServerLogger log;
        try {
            if (StringUtils.isEmpty(logLevel)) {
                logLevel = defaultLevel;
            }
            if (StringUtils.isEmpty(logReference)) {
                logReference = defaultReference;
            }
            log = LogHelper.initLogger(logLevel, logReference, loggerName, additionalInfo);
        } catch (Exception e) {
            log = IsrServerLogger.create();  //todo
        }
        return log;
    }
}
