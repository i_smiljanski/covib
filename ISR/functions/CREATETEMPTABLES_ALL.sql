CREATE OR REPLACE FUNCTION CREATETEMPTABLES_ALL return varchar2 
is
  oErrorObj STB$OERROR$RECORD := STB$OERROR$RECORD();
begin
  FOR rGetEntities IN (select entityname from isr$meta$entity ) LOOP
    oErrorObj := iSR$Metadata$Package.DropTempEntityTable(rGetEntities.entityname);
    oErrorObj := iSR$Metadata$Package.CreateEntityTable(rGetEntities.entityname);
  END LOOP;
  return 'T';
END CREATETEMPTABLES_ALL;