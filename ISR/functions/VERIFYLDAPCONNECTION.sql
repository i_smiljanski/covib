CREATE OR REPLACE function verifyLDAPConnection(sHost IN VARCHAR2, sPort IN VARCHAR2, sGlobUserName IN VARCHAR2, sGlobPassword IN VARCHAR2, sLDAPBase IN VARCHAR2, sLDAPSearch IN VARCHAR2, sUserName IN VARCHAR2, sPassword IN VARCHAR2) 
return varchar2 
is

  sReturn           VARCHAR2(4000);
  
  v_ldap_session    DBMS_LDAP.SESSION;
  v_ldapconn_admin  binary_integer;
  v_ldapconn        binary_integer;
  res_attrs         DBMS_LDAP.STRING_COLLECTION;
  retval            PLS_INTEGER;
  res_message       DBMS_LDAP.MESSAGE;
  
  sUserDn           VARCHAR2(4000);  
  
  EX_GOT_NO_USER_DN EXCEPTION;
  
BEGIN
  sReturn := sReturn||'Starting ...'||chr(10)||chr(13);
      
  v_ldap_session := DBMS_LDAP.INIT(
      HOSTNAME => sHost
    , PORTNUM  => sPort
  );
  
  sReturn := sReturn||'Opened session to server'||chr(10)||chr(13);
      
  v_ldapconn_admin := DBMS_LDAP.SIMPLE_BIND_S(
      LD => v_ldap_session
    , DN => sGlobUserName
    , PASSWD => sGlobPassword
  );
  
  sReturn := sReturn||'Connected to global user'||chr(10)||chr(13);
      
  res_attrs(1) := '*';

  retval := DBMS_LDAP.SEARCH_S(
      ld         =>  v_ldap_session
    , base       =>  sLDAPBase
    , scope      =>  DBMS_LDAP.SCOPE_SUBTREE
    , filter     =>  sLDAPSearch||'='||sUsername
    , attrs      =>  res_attrs
    , attronly   =>  0
    , res        =>  res_message
  );
  
  sReturn := sReturn||'Got attributes of connecting user'||chr(10)||chr(13);
      
  IF DBMS_LDAP.COUNT_ENTRIES(v_ldap_session, res_message) > 0 THEN
  
    sUserDn := DBMS_LDAP.get_dn(v_ldap_session, RAWTOHEX(res_message));
  
    sReturn := sReturn||'Long username of connecting user: '|| sUserDn||chr(10)||chr(13);
      
    v_ldapconn := DBMS_LDAP.SIMPLE_BIND_S(
        LD => v_ldap_session
      , DN => sUserDn
      , PASSWD => sPassword
    );
        
  ELSE 
    RAISE EX_GOT_NO_USER_DN;
  END IF;
      
  v_ldapconn_admin := DBMS_LDAP.UNBIND_S(LD => v_ldap_session);
  
  sReturn := sReturn||'Finished!'||chr(10)||chr(13);
  return sReturn;

EXCEPTION
  WHEN EX_GOT_NO_USER_DN THEN
    sReturn := sReturn||'Did not found user'||chr(10)||chr(13);
    return sReturn;
  WHEN OTHERS THEN
    sReturn := sReturn||'Got any error '||sqlcode||' '||sqlerrm||chr(10)||chr(13);
    return sReturn;
END verifyLDAPConnection;