CREATE OR REPLACE function convertXmlIdentifiers( csXmlString varchar2 ) 
  return varchar2
is
  sReturnXmlString varchar2(32000); 
begin
  sReturnXmlString := replace( csXmlString, '&', '&amp');    
  sReturnXmlString := replace( csXmlString, '<', '&lt');    
  sReturnXmlString := replace( csXmlString, '"', '&quot');    
  sReturnXmlString := replace( csXmlString, '''', '&apos');    
  sReturnXmlString := replace( csXmlString, '<', '&lt');      
  return sReturnXmlString;
exception
   when others then 
     return csXmlString;
end convertXmlIdentifiers;