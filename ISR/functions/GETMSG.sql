CREATE OR REPLACE FUNCTION GETMSG ( csKey          varchar2,
                  cnLang         integer,
                  csPlugin       varchar2  default null,
                  csIncludeText1 varchar2  default null,
                  csIncludeText2 varchar2  default null,
                  csIncludeText3 varchar2  default null,
                  csIncludeText4 varchar2  default null,
                  csIncludeText5 varchar2  default null ) return varchar2 is
  pragma AUTONOMOUS_TRANSACTION;
  sMessage utd$msg.msgText%type;
  sUsed    utd$msg.used%type;
  splugin  utd$msg.plugin%type;

  cursor cGetTranslation is
    select msgtext, used, plugin
      from utd$msg
     where msgkey = csKey
       AND lang = cnLang;
begin
  open cGetTranslation;
  fetch cGetTranslation into sMessage, sUsed, sPlugin;
  if cGetTranslation%notfound and cnLang != 999 and trim(csKey) is not null then
    sMessage := case when csKey = UPPER(csKey) then initcap(replace(replace(replace(replace(replace(replace(replace(csKey, 'CAN_'), 'AS$'), 'WT$'), 'SB$'), '$DESC'), '$MASK'), '_', ' ')) else null end;

    merge into utd$msg b
    using (select langid lang, csKey msgkey, sMessage msgtext, 'T' used, nvl(csPlugin, sPlugin) plugin
             from utd$language
            where langid != 999) e
    on (b.lang = e.lang
    and b.msgkey = e.msgkey)
    when not matched then
       insert (b.lang
             , b.msgkey
             , b.msgtext
             , b.used
             , b.plugin)
       values (e.lang
             , e.msgkey
             , e.msgtext
             , e.used
             , e.plugin);
    commit;
  elsif sUsed = 'F' then
    update utd$msg set used = 'T' where msgkey = csKey;
    commit;
  end if;
  close cGetTranslation;

  -- replace tokens for variable text output
  if ( csIncludeText1 is not null or csIncludeText2 is not null or csIncludeText3 is not null or csIncludeText4 is not null or csIncludeText5 is not null ) then
    sMessage := replace(replace(replace(replace(replace(sMessage,'%1%',csIncludeText1),'%2%',csIncludeText2),'%3%',csIncludeText3),'%4%',csIncludeText4),'%5%',csIncludeText5);
  end if;

  return case when sMessage is null then csKey else sMessage end;
end GetMsg;