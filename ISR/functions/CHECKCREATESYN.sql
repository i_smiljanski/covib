CREATE OR REPLACE FUNCTION checkCreateSyn RETURN VARCHAR2 IS
  oErrorObj                     STB$OERROR$RECORD := STB$OERROR$RECORD();
  sReturn                       VARCHAR2 (4000);

  CURSOR cHasPrivilege IS
    SELECT 'T'
    FROM   (SELECT PRIVILEGE
            FROM   role_sys_privs
            UNION
            SELECT PRIVILEGE
            FROM   user_sys_privs)
    WHERE  PRIVILEGE = 'CREATE ANY SYNONYM';
    
  rHasPrivilege cHasPrivilege%rowtype;

  CURSOR cGetUser IS
    SELECT parametervalue
    FROM   isr$installparameter
    WHERE  parametername IN ('ORACLEUSERNAME', 'LDAPORACLEUSER');
BEGIN

  OPEN cHasPrivilege;
  FETCH cHasPrivilege INTO rHasPrivilege;
  IF cHasPrivilege%FOUND THEN
	FOR rGetUser IN cGetUser LOOP
      sReturn := STB$USERSECURITY.Createsyn (STB$UTIL.getSystemParameter ('ISRROLE'), rGetUser.parametervalue, STB$UTIL.getSystemParameter ('STBOWNER'));
	END LOOP;
  END IF;
  CLOSE cHasPrivilege;

  IF sReturn IS NULL THEN
    sReturn := 'T';
  END IF;

  RETURN sReturn;
EXCEPTION WHEN OTHERS THEN
  -- Consider logging the error and then re-raise
  RETURN SQLCODE;
END checkCreateSyn;