CREATE OR REPLACE FUNCTION LINEAGG(lines IN ISR$INSTALL$STRINGLIST)
RETURN CLOB AS

	output CLOB := EMPTY_CLOB();
	temp_clob CLOB := EMPTY_CLOB();

BEGIN

	FOR i IN 1..lines.COUNT LOOP
		temp_clob := lines(i);
		output := output || temp_clob;
		DBMS_LOB.FREETEMPORARY(temp_clob);
	END LOOP;
	
	RETURN output;

END LINEAGG;