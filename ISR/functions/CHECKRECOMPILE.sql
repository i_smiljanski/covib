CREATE OR REPLACE FUNCTION checkRecompile RETURN VARCHAR2 IS
  sReturn            VARCHAR2(4000) := 'T';
  nInvalidObjs       NUMBER := 1;
  sCurrentObject     VARCHAR2(500);
  nStopAfterAttempts NUMBER := 10;
  nAttempts          NUMBER := 1;
BEGIN
  WHILE nInvalidObjs > 0 OR nAttempts < nStopAfterAttempts LOOP
    FOR rGetInvalidObjects IN (SELECT object_name, object_type
                                 FROM user_objects
                                WHERE status = 'INVALID') LOOP
      sCurrentObject := rGetInvalidObjects.object_type || ': ' || rGetInvalidObjects.object_name;
      dbms_output.put_line('[Attempts '||nAttempts||'] '||sCurrentObject||': '||'attempt to compile');
      BEGIN
        IF rGetInvalidObjects.object_type LIKE '%BODY' THEN
          EXECUTE IMMEDIATE 'alter '
                               || REPLACE (rGetInvalidObjects.object_type, 'BODY')
                               || ' "'
                               || rGetInvalidObjects.object_name
                               || '" compile BODY';
        ELSE
          EXECUTE IMMEDIATE 'alter '
                               || rGetInvalidObjects.object_type
                               || ' "'
                               || rGetInvalidObjects.object_name
                               || '" compile';
        END IF;
      EXCEPTION 
        WHEN OTHERS THEN
          dbms_output.put_line('[Attempts '||nAttempts||'] '||sCurrentObject||': '||SQLERRM);
      END;
    END LOOP;

    BEGIN
      nAttempts := nAttempts + 1;      
      SELECT COUNT ( * )
        INTO nInvalidObjs
        FROM user_objects
       WHERE status = 'INVALID';
    EXCEPTION
       WHEN OTHERS THEN
          nInvalidObjs := 0;          
    END;
    IF nInvalidObjs = 0 THEN
      nAttempts := nStopAfterAttempts;
    END IF;
  END LOOP;
  RETURN sReturn;
EXCEPTION
   WHEN OTHERS THEN
      RETURN sReturn;
END checkRecompile;