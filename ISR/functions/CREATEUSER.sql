CREATE OR REPLACE FUNCTION createUser(
                  sFullName IN VARCHAR2
                  , sLDAPUserName VARCHAR2
                  , sDatagroup IN VARCHAR DEFAULT NULL
                  , sEmail IN VARCHAR DEFAULT NULL
                  , sDepartment IN VARCHAR DEFAULT NULL
                  , sLimsusername IN VARCHAR DEFAULT NULL
                  , sApplicationpathes IN VARCHAR DEFAULT NULL) 
                  RETURN VARCHAR2 
  IS
    oErrorObj    Stb$oerror$record := STB$OERROR$RECORD();
    olNodeList   Stb$treenodelist;
    olParameter  STB$PROPERTY$LIST :=STB$PROPERTY$LIST();
    oParameter                   STB$MENUENTRY$RECORD := STB$MENUENTRY$RECORD();
    sCurrentName         constant varchar2(100) := $$PLSQL_UNIT||'.createUser('||sFullName||')';
       
    
  BEGIN
    isr$trace.debug ('begin',  'sFullName '||sFullName||' sLDAPUserName '||sLDAPUserName||' sDatagroup '||sDatagroup||' sEmail '||sEmail||' sDepartment '||sDepartment, sCurrentName);

    oParameter.sToken := 'CAN_ADD_USER';
    oParameter.shandledby := 'STB$USERSECURITY';

    oErrorObj := stb$usersecurity.callFunction (oParameter, olNodeList);
    olParameter := olNodeList (1).tlopropertylist;

    FOR i IN 1..olParameter.COUNT() LOOP
      IF olParameter(i).sParameter = 'FULLNAME' THEN
        olParameter(i).sValue := sFullName;
      ELSIF olParameter(i).sParameter = 'LDAPUSERNAME' THEN
        olParameter(i).sValue := sLdapUserName;
      ELSIF olParameter(i).sParameter = 'DATAGROUP' and TRIM(sDatagroup) IS NOT NULL THEN
        olParameter(i).sValue := sDatagroup;
      ELSIF olParameter(i).sParameter = 'DEPARTMENT' and TRIM(sDepartment) IS NOT NULL THEN
        olParameter(i).sValue := sDepartment;
      ELSIF olParameter(i).sParameter = 'EMAIL' and TRIM(sEmail) IS NOT NULL THEN
        olParameter(i).sValue := sEmail;
      ELSIF olParameter(i).sParameter = 'LIMSUSERNAME' and TRIM(sLimsusername) IS NOT NULL THEN
        olParameter(i).sValue := sLimsusername;
      ELSIF olParameter(i).sParameter = 'APPLICATIONPATHES' and TRIM(sApplicationpathes) IS NOT NULL THEN
        olParameter(i).sValue := sApplicationpathes;
      END IF;    
      olParameter(i).sDisplay := olParameter(i).sValue;    
    END LOOP;    
    
    isr$trace.debug ('propertyList',  'propertyList in clob', sCurrentName, olParameter);
    oErrorObj := stb$usersecurity.putParameters (oParameter, olParameter);
    
    if oErrorObj.isExceptionCritical then
      ROLLBACK;
      return oErrorObj.sErrorCode || ' ' || oErrorObj.sErrorMessage;
    end if;
    commit;
    RETURN 'User successfully created: '||sFullName;
           
           
  end createUser;