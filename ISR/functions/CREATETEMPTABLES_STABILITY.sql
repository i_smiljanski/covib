CREATE OR REPLACE FUNCTION CREATETEMPTABLES_STABILITY return varchar2
is
  oErrorObj STB$OERROR$RECORD := STB$OERROR$RECORD();
begin
  FOR rGetEntities IN (select entityname from isr$meta$entity where plugin = 'STABILITY') LOOP
    oErrorObj := iSR$Metadata$Package.DropTempEntityTable(rGetEntities.entityname);
    oErrorObj := iSR$Metadata$Package.CreateEntityTable(rGetEntities.entityname);
  END LOOP;
  oErrorObj := iSR$Metadata$Package.DropTempEntityTable('SB$LEVEL2');
  oErrorObj := iSR$Metadata$Package.CreateEntityTable('SB$LEVEL2');  
  return 'T';
END CREATETEMPTABLES_STABILITY;