CREATE OR REPLACE FUNCTION checkTableSpaces RETURN VARCHAR2 IS
  sReturn VARCHAR2(4000);
  sToDo   VARCHAR2(4000);
  
  cursor cGetRequiredTablespaces is
    SELECT 'ISR_TABLE' tablespaceName,
           50 requiredSize
    FROM   DUAL
    UNION
    SELECT 'ISR_BLOB' tablespaceName,
           200 requiredSize
    FROM   DUAL
    UNION
    SELECT 'ISR_INDEX' tablespaceName,
           20 requiredSize
    FROM   DUAL;
   
  cursor cGetTablespace(sName in varchar2) is
    SELECT i1.parametervalue tablespaceName, i2.parametervalue autoextend, i3.parametervalue maxsize
    FROM   isr$installparameter i1, isr$installparameter i2, isr$installparameter i3
    WHERE  i1.parametername = sName
    AND    i2.parametername = sName||'_AUTO'
    AND    i3.parametername = sName||'_MAXSIZE';
    
  rGetTablespace cGetTablespace%rowtype;
  
  cursor cGetUserQuota(sTableSpaceName in varchar2) is  
    SELECT   utq.tablespace_name,
             utq.max_bytes,
             SUM (ufs.BYTES) / 1000000 freespace
    FROM     USER_TS_QUOTAS utq,
             USER_FREE_SPACE ufs
    WHERE    utq.tablespace_name = ufs.tablespace_name
    AND      utq.tablespace_name = sTableSpaceName
    GROUP BY utq.tablespace_name,
             utq.max_bytes;
   
  rGetUserQuota cGetUserQuota%rowtype;
  
  cursor cGetRequiredGrants is  
    (SELECT 'CREATE TYPE' privilege FROM DUAL
     UNION
     SELECT 'CREATE VIEW' privilege FROM DUAL
     UNION
     SELECT 'CREATE SYNONYM' privilege FROM DUAL
     UNION
     SELECT 'CREATE ANY SYNONYM' privilege FROM DUAL
     UNION
     SELECT 'CREATE TABLE' privilege FROM DUAL
     UNION
     SELECT 'CREATE SEQUENCE' privilege FROM DUAL
     UNION
     SELECT 'CREATE TRIGGER' privilege FROM DUAL
     UNION
     SELECT 'CREATE PROCEDURE' privilege FROM DUAL
     UNION
     SELECT 'CREATE SESSION' privilege FROM DUAL
     UNION
     SELECT 'ALTER SESSION' privilege FROM DUAL
     UNION
     SELECT 'CREATE JOB' privilege FROM DUAL
     UNION
     SELECT 'MANAGE SCHEDULER' privilege FROM DUAL)
    MINUS
    SELECT privilege
      FROM (SELECT privilege FROM user_sys_privs
            UNION
            SELECT privilege FROM role_sys_privs);
            
  rGetRequiredGrants cGetRequiredGrants%rowtype;  
  
  cursor cGetRequiredJavaGrants is  
    ( select 'SYS:java.security.SecurityPermission' type_name,'insertProvider.SunJSSE' name,'' action, 'ENABLED' enabled from dual union
      select 'SYS:java.security.SecurityPermission','putProviderProperty.SunJSSE','', 'ENABLED' enabled from dual union
      select 'SYS:java.security.SecurityPermission','getProperty.ssl.ServerSocketFactory.provider','', 'ENABLED' enabled from dual union
      select 'SYS:java.security.SecurityPermission','getProperty.cert.provider.x509v1','', 'ENABLED' enabled from dual union
      select 'SYS:java.util.PropertyPermission','java.protocol.handler.pkgs','write', 'ENABLED' enabled from dual union
      select 'SYS:java.util.PropertyPermission','https.proxyHost','write', 'ENABLED' enabled from dual union
      select 'SYS:java.util.PropertyPermission','https.proxyPort','write', 'ENABLED' enabled from dual union
      select 'SYS:java.security.SecurityPermission','getProperty.ssl.SocketFactory.provider','', 'ENABLED' enabled from dual union
      select 'SYS:java.security.SecurityPermission','getProperty.sun.ssl.keymanager.type','', 'ENABLED' enabled from dual union
      select 'SYS:java.security.SecurityPermission','getProperty.sun.ssl.trustmanager.type','', 'ENABLED' enabled from dual union
      select 'SYS:java.util.PropertyPermission', 'javax.net.ssl.trustStore','write', 'ENABLED' enabled from dual union
      select 'SYS:java.util.PropertyPermission', 'javax.net.ssl.trustStorePassword','write', 'ENABLED' enabled from dual union
      select 'SYS:java.util.PropertyPermission', 'javax.net.ssl.keyStore', 'write', 'ENABLED' enabled from dual union
      select 'SYS:java.util.PropertyPermission', 'javax.net.ssl.keyStorePassword','write', 'ENABLED' enabled from dual union
      select 'SYS:java.net.SocketPermission','*','connect,resolve', 'ENABLED' enabled from dual union
      select 'SYS:java.io.FilePermission', '/tmp/loader.log', 'write', 'ENABLED' enabled from dual union
      select 'SYS:java.lang.RuntimePermission', 'setIO', null, 'ENABLED' enabled from dual union
      select 'SYS:java.lang.RuntimePermission', 'getClassLoader', '', 'ENABLED' enabled from dual union
      select 'SYS:java.util.PropertyPermission', 'javax.xml.transform.TransformerFactory', 'write', 'ENABLED' enabled from dual union
      select 'SYS:java.lang.RuntimePermission', 'setSecurityManager', null, 'ENABLED' enabled from dual union
      select 'SYS:java.util.PropertyPermission', 'java.rmi.server.hostname', 'write', 'ENABLED' enabled from dual union
      select 'SYS:java.lang.RuntimePermission', 'writeFileDescriptor', '*', 'ENABLED' enabled from dual union
      select 'SYS:java.lang.RuntimePermission', 'readFileDescriptor', '*', 'ENABLED' enabled from dual
  )
    MINUS
    (SELECT type_schema||':'||type_name, name, action, enabled FROM user_java_policy);
            
  rGetRequiredJavaGrants cGetRequiredJavaGrants%rowtype;                
BEGIN
   
  -- this is for a full iStudyReporter installation

  FOR rGetRequiredTablespaces IN cGetRequiredTablespaces LOOP  
  
    OPEN cGetTablespace(rGetRequiredTablespaces.tablespaceName);
    FETCH cGetTablespace INTO rGetTablespace;
    CLOSE cGetTablespace;
    
    OPEN  cGetUserQuota(rGetTablespace.tablespaceName);
    FETCH cGetUserQuota INTO rGetUserQuota;
    CLOSE cGetUserQuota;
        
    IF rGetUserQuota.max_bytes <> -1 THEN
      sReturn := sReturn || 'User has not quota unlimited on tablespace '  || rGetUserQuota.tablespace_name || '.' || chr(10) ;
    sTodo := sTodo || 'alter user '  || user || ' quota unlimited on ' || rGetUserQuota.tablespace_name || ';' || chr(10) ;
    END IF;
        
    IF rGetUserQuota.freespace < rGetRequiredTablespaces.requiredSize THEN
      IF  rGetTablespace.autoextend = 'F'
      OR (    rGetTablespace.autoextend = 'T'
          AND UPPER(rGetTablespace.maxsize) != 'UNLIMITED'
          AND rGetTablespace.maxsize < rGetRequiredTablespaces.requiredSize) THEN
        sReturn := sReturn || 'There is not enough free space in tablespace  '  || rGetUserQuota.tablespace_name || '.' || chr(10) ;
        sTodo := sTodo || '-- Extend tablespace '  || rGetUserQuota.tablespace_name || '.' || chr(10) ;
      END IF;
    END IF;
  
  END LOOP;
    
  FOR rGetRequiredGrants IN cGetRequiredGrants LOOP
    sReturn := sReturn || 'User hasn''t the privilege '  || rGetRequiredGrants.privilege || '.' || chr(10) ;
    sTodo := sTodo || 'grant '  || rGetRequiredGrants.privilege || ' to ' || user || ';' || chr(10) ;
  END LOOP;
    
  FOR rGetRequiredJavaGrants IN cGetRequiredJavaGrants LOOP
    sReturn := sReturn || 'User hasn''t the java privilege '||rGetRequiredJavaGrants.type_name||','||rGetRequiredJavaGrants.name||','||rGetRequiredJavaGrants.action||' or the privilege is disabled.' || chr(10) ;
    sTodo := sTodo || 'call dbms_java.grant_permission( '''||user||''' , '''||rGetRequiredJavaGrants.type_name||''', '''||rGetRequiredJavaGrants.name||''', '''||rGetRequiredJavaGrants.action||''');' || chr(10) ;
  END LOOP;
  
  IF TRIM(sReturn) is not null THEN
    sReturn := sReturn || 'Please contact your local DBA for the execution of the following steps:' || chr(10) || sTodo;
  ELSE
    sReturn := 'T';
  END IF;

  RETURN sReturn;
EXCEPTION WHEN OTHERS THEN
  -- Consider logging the error and then re-raise
  RETURN SQLCODE;
END checkTableSpaces;