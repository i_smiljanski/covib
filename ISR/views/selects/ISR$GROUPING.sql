CREATE OR REPLACE VIEW ISR$GROUPING (GROUPINGNAME, DESCRIPTION, CANHAVEFORMATMASK, FLAG, ORDERNO) AS SELECT GROUPINGNAME
        , UTD$MSGLIB.getMsg (DESCRIPTION, stb$security.getcurrentlanguage)
        , CANHAVEFORMATMASK
        , FLAG
        , ORDERNO
     FROM (SELECT 'GRP$' || entity groupingname
                , description
                , 'F' CANHAVEFORMATMASK
                , 'C' FLAG
                , ROWNUM ORDERNO
             FROM (SELECT DISTINCT (upper(entity) || '_INFO') entity, (upper(entity) || '_INFO') description
                     from isr$report$wizard
                    where reporttypeid != 999
                      and masktype not in (4, 5)
                   UNION
                   SELECT DISTINCT UPPER (entity) entity, UPPER (entity) description
                     FROM isr$report$wizard
                    WHERE reporttypeid != 999
                      AND masktype NOT IN (4, 5)
                   UNION
                   SELECT DISTINCT UPPER (entityname) entity, UPPER (entityname) description
                     FROM isr$meta$grid$entity
                    WHERE reporttypeid != 999
                      AND NVL (editable, 'T') != 'F'
                   UNION
                   SELECT 'SERVER', 'SERVER' FROM DUAL
                   ORDER BY 2)
           UNION
           SELECT 'GRP$' || parametername groupingname
                , description
                , DECODE (parametertype, 'DATE', 'T', 'F') CANHAVEFORMATMASK
                , 'O' FLAG
                , ROWNUM ORDERNO
             FROM (SELECT DISTINCT UPPER (parametername) parametername, description, parametertype
                     FROM STB$REPTYPEPARAMETER
                    WHERE repparameter = 'T'
                      AND visible = 'T'
                   ORDER BY 2)
           UNION
           SELECT parametername groupingname
                , description
                , 'F' CANHAVEFORMATMASK
                , 'S' FLAG
                , MIN (orderno) ORDERNO
             FROM isr$dialog
            WHERE token LIKE 'SAVE_DIALOG_%'
              AND token != 'SAVE_DIALOG_999'
           GROUP BY parametername, description
           UNION
           SELECT VALUE groupingname
                , display
                , info CANHAVEFORMATMASK
                , SUBSTR (entity, 0, 1) FLAG
                , ordernumber ORDERNO
             FROM isr$parameter$values
            WHERE entity IN ('DOCUMENT_GROUPING', 'REPORT_GROUPING'))
   UNION
   SELECT DISTINCT 'GRP$' || identifier groupingname
                 , identifier
                 , 'F' CANHAVEFORMATMASK
                 , 'X' FLAG
                 , 0 ORDERNO
     FROM isr$output$property
   UNION
   SELECT DISTINCT 'GRP$' || parametername groupingname
                 , parametername
                 , DECODE (parametertype, 'DATE', 'T', 'F') CANHAVEFORMATMASK
                 , 'Y' FLAG
                 , 0 ORDERNO
     FROM isr$doc$prop$template
   --           UNION
   --           SELECT entity groupingname
   --                , description
   --                , 'F' CANHAVEFORMATMASK
   --                , 'P' FLAG
   --                , ROWNUM ORDERNO
   --             FROM (SELECT UPPER (VALUE) entity, UPPER (display) description
   --                     FROM isr$parameter$values
   --                    WHERE entity = 'PLACEHOLDER_NODES'
   --                   ORDER BY ordernumber)
   --           UNION
   --           SELECT entityname groupingname
   --                , description
   --                , 'F' CANHAVEFORMATMASK
   --                , 'E' FLAG
   --                , ROWNUM ORDERNO
   --             FROM (SELECT DISTINCT
   --                          UPPER (entityname) entityname
   --                        , UPPER (entityname) || '$DESC' description
   --                     FROM isr$meta$entity
   --                   ORDER BY 2)
   --           UNION
   --           SELECT DISTINCT EXTRACTVALUE (COLUMN_VALUE, '/name/@attr') sParameter
   --                , UTD$MSGLIB.getMsg (EXTRACTVALUE (COLUMN_VALUE, '/name/@attr')
   --                                   , STB$SECURITY.getCurrentLanguage) sValue
   --                , EXTRACTVALUE (COLUMN_VALUE, '/name/@attr')
   --                , 'X'
   --                , rownum
   --             FROM table(XMLSEQUENCE(EXTRACT (
   --                                       (SELECT XMLQUERY ('for $i in //*[.=text()] return <name attr="{$i/name()}"/>'
   --                                                  PASSING BY VALUE (SELECT XMLTYPE(STB$UTIL.blobToClob(STB$DOCKS.unzip(content)))
   --                                                                      FROM stb$doctrail
   --                                                                     WHERE doctype = 'X') RETURNING CONTENT)
   --                                          FROM DUAL)
   --                                     , '/*'
   --                                    )))
   ORDER BY 4, 5, 1