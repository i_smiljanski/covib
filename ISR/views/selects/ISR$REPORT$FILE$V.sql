CREATE OR REPLACE VIEW ISR$REPORT$FILE$V (JOBID, FILENAME, TEXTFILE) AS select j.jobid, rf.filename, rf.textfile
from stb$jobqueue j, ISR$REPORT$FILE rf
     where repid = STB$JOB.getJobParameter ('REPID', j.jobid)
       -- non versioned reports
       and NVL (TRIM (STB$JOB.getJobParameter ('SRNUMBER', j.jobid)), 'NOT_FOUND') != 'NOT_FOUND'
       -- criteria reports
       and TRIM (STB$JOB.getJobParameter ('TOKEN', j.jobid)) not like 'CAN_CREATE_CRITERIA'
       -- audit reports
       and TRIM (STB$JOB.getJobParameter ('TOKEN', j.jobid)) not like 'CAN_DISPLAY%AUDIT'
       -- audit reports
       and TRIM (STB$JOB.getJobParameter ('TOKEN', j.jobid)) not like 'CAN_OFFSHOOT_DOC'
       -- non versioned reports
       and STB$UTIL.getReptypeParameter (STB$JOB.getJobParameter ('REPORTTYPEID', j.jobid), 'REQUIRES_VERSIONING') != 'F' 