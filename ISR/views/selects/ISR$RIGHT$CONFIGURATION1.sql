CREATE OR REPLACE VIEW ISR$RIGHT$CONFIGURATION1 (ID, TYPE, PARAMETERNAME, DESCRIPTION, ORDERNO, USERGROUPNO, GROUPNAME, TYPE2, RIGHT, GROUPING, FOLDER, FOLDERID, ICON) AS select ID
       , TYPE
       , rc.PARAMETERNAME
       , rc.DESCRIPTION
       , rc.ORDERNO
       , USERGROUPNO
       , GROUPNAME
       , TYPE2
       , RIGHT
       , c.nodetype
       , case
           when c.parentnode is not null then
             (select UTD$MSGLIB.getMsg (NVL (description, token), STB$SECURITY.getCurrentLanguage)
                from stb$contextmenu
               where entryid = c.parentnode)
         end
       , case
           when c.parentnode is not null then
             (select ordernum * 1000
                from stb$contextmenu
               where entryid = c.parentnode)
           else
             0
         end
         + c.ordernum
       , null                                                                                                                                                             --c.icon
    from ISR$RIGHT$CONFIGURATION rc, stb$contextmenu c
   where rc.parametername = c.token(+)
     and NVL (c.visible, 'T') = 'T'
     and 1 = SUBSTR (rc.orderno, 1, 1)
  union
  select ID
       , TYPE
       , rc.PARAMETERNAME
       , rc.DESCRIPTION
       , rc.ORDERNO
       , USERGROUPNO
       , GROUPNAME
       , TYPE2
       , RIGHT
       , null
       , null
       , 0
       , null
    from ISR$RIGHT$CONFIGURATION rc
   where 1 != SUBSTR (orderno, 1, 1) 