CREATE OR REPLACE VIEW CAN_DISPLAY_TEMPLATE_REPORT (XML) AS with reportlink as ( select distinct rot.reporttypeid, ouf.fileid
                     from   isr$report$output$type rot, isr$output$file ouf, isr$template t, stb$reporttype rpt
                     where  rot.outputid = ouf.outputid
                     and    t.fileid = ouf.fileid
                     and    rpt.reporttypeid = rot.reporttypeid
                     and    rpt.activetype = 1 ) -- [ISRC-206]
SELECT
		   (XMLELEMENT("SYSTEMCONFIGURATION"
					 , (SELECT
							   (XMLAGG(XMLELEMENT("TEMPLATES"
												, (SELECT
														  XMLAGG(XMLELEMENT("TEMPLATE"
																		  , XMLELEMENT("TEMPLATENAME", templatename)
																		  , XMLELEMENT("VERSION", version)
																		  , XMLELEMENT("FILENAME", filename)
																		  , XMLELEMENT("RELEASE", release)
																		  , XMLELEMENT("RELEASEDBY", releasedby)
																		  , XMLELEMENT("RELEASEDON", releasedon)
																		  , XMLELEMENT("EXCHANGE_COMMENT", exchange_comment)
																		  , XMLELEMENT("MODIFIEDON", modifiedon)
																		  , XMLELEMENT("MODIFIEDBY", modifiedby)))
												   FROM
														  (SELECT
																  t.templatename
																, t.exchange_comment
																, t.release
																, f.filename
																, CASE
																	  WHEN t.release = 'T'
														   AND			   t.releasedby IS NULL
																	  THEN
																		  'Installation User'
																	  ELSE
																		  t.releasedby
																  END
																	  releasedby
																, CASE
																	  WHEN t.release = 'T'
														   AND			   t.releasedon IS NULL
																	  THEN
																		  'Installation Date'
																	  ELSE
																		  TO_CHAR(t.releasedon
																				, STB$UTIL.getSystemParameter('DATEFORMAT'))
																  END
																	  releasedon
																, t.version
																, TO_CHAR(t.modifiedon
																		, STB$UTIL.getSystemParameter('DATEFORMAT'))
																	  modifiedon
																, t.modifiedby
														   FROM	  isr$file f, 
                                      isr$template t, 
                                      reportlink rl
														   WHERE  t.fileid = f.fileid
                               AND    t.fileid in rl.fileid
														   ORDER BY  t.templatename, t.version) t))))
						FROM
							   DUAL)
					 , (SELECT
							   (XMLAGG(XMLELEMENT("FILEPARAMETERSVALUES"
												, XMLATTRIBUTES( TRIM( templatename||' ['||version||']'||' '|| description ) AS "typename")
												, XMLELEMENT("TEMPLATENAME", XMLATTRIBUTES('F' AS "f"), templatename)
												, (SELECT
														  XMLAGG(XMLELEMENT("PARAMETERVALUE"
																		  , XMLELEMENT("BOOKMARKVALUE", VALUE)
																		  , XMLELEMENT("FORMAT_ENTITY", entity)
																		  , XMLELEMENT("DISPLAY", display)
																		  , XMLELEMENT("INFO", info)
																		  , XMLELEMENT("USE", use)
																		  , XMLELEMENT("ORDERNUMBER", ordernumber)
																		  , XMLELEMENT("VISIBLE", visible)
																		  , XMLELEMENT("CLASS", class)
																		  , XMLELEMENT("STYLE", style)
																		  , XMLELEMENT("MODIFIEDON", modifiedon)
																		  , XMLELEMENT("MODIFIEDBY", modifiedby)))
												   FROM
														  (SELECT
																  entity
																, VALUE
																, display
																, info
																, fpv.visible
																, editable
																, use
																, ordernumber
																, (SELECT
																		  ordernumber
																   FROM
																		  isr$parameter$values pv
																   WHERE
																		  pv.entity = 'FORMAT_ENTITY'
																   AND	  pv.VALUE = fpv.entity)
																	  entityordernumber
																, TO_CHAR(fpv.modifiedon
																		, STB$UTIL.getSystemParameter('DATEFORMAT'))
																	  modifiedon
																, fpv.modifiedby
																, fpv.fileid
																, fpv.class
																, fpv.style
														   FROM
																  isr$file$parameter$values fpv, isr$template t
														   WHERE
																  fpv.fileid = t.fileid
														   ORDER BY
																  entityordernumber, ordernumber) t2
												   WHERE
														  t1.fileid = t2.fileid
												   AND	  t1.VALUE = t2.VALUE))))
						FROM
							   ( SELECT DISTINCT t.templatename, t.version, t.fileid, fpv.VALUE, s.description
								   FROM   isr$template t, 
                          isr$file$parameter$values fpv, 
                          isr$stylesheet s, 
                          isr$output$file iof,
									        isr$output$transform iot, 
                          reportlink rl
								   WHERE  t.fileid = fpv.fileid
                   AND    t.fileid = rl.fileid
								   AND    fpv.VALUE = iot.bookmarkname
								   AND    iot.outputid = iof.outputid
								   AND    iof.fileid = t.fileid
								   AND    iot.stylesheetid = s.stylesheetid
								   ORDER BY 1, 2, 4) t1)
					 , (SELECT
							   (XMLAGG(XMLELEMENT("FILECALCVALUES"
												, XMLATTRIBUTES(TRIM(	templatename
																	 || ' ['
																	 || version
																	 || ']'
																	 || ' '
																	 || description) AS "typename")
												, XMLELEMENT("TEMPLATENAME", XMLATTRIBUTES('F' AS "visible"), templatename)
												, (SELECT
														  XMLAGG(XMLELEMENT("CALCVALUE"
																		  , XMLELEMENT("BOOKMARKVALUE", experiment)
																		  , XMLELEMENT("CALC_BLOCK", VALUE)
																		  , XMLELEMENT("FORMAT_ENTITY", entity)
																		  , XMLELEMENT("DISPLAY", display)
																		  , XMLELEMENT("INFO", info)
																		  , XMLELEMENT("USE", use)
																		  , XMLELEMENT("ORDERNUMBER", ordernumber)
																		  , XMLELEMENT("VISIBLE", visible)
																		  , XMLELEMENT("MODIFIEDON", modifiedon)
																		  , XMLELEMENT("MODIFIEDBY", modifiedby)))
												   FROM
														  (SELECT entity, 
                                      (  SELECT DISTINCT rpv.VALUE
                                         FROM   ISR$FILE$CALC$VALUES rpv
                                         WHERE  rpv.display = fpv.VALUE
                                         AND	  rpv.fileid = fpv.fileid
                                         AND	  rpv.entity = 'WT$EXP$CALC'
                                         AND	  use = 'T') experiment,
                                      VALUE, 
                                      display,
                                      info,
                                      fpv.visible,
                                      editable,
                                      use,
                                      ordernumber,
                                      (  SELECT ordernumber
                                         FROM   isr$parameter$values pv
                                         WHERE  pv.entity = 'FORMAT_ENTITY'
                                         AND	  pv.VALUE = fpv.entity ) entityordernumber,
                                      TO_CHAR(fpv.modifiedon, STB$UTIL.getSystemParameter('DATEFORMAT')) modifiedon,
                                      fpv.modifiedby,
                                      t.fileid
														   FROM   isr$file$calc$values fpv, isr$template t
														   WHERE
																  fpv.fileid = t.fileid
														   ORDER BY
																  entityordernumber, ordernumber) t2
												   WHERE
														  t1.fileid = t2.fileid
												   AND	  t1.VALUE = t2.VALUE))))
						FROM
							   (SELECT DISTINCT t.templatename, t.version, t.fileid, fpv.VALUE, REPLACE(s.description, 'Stylesheet', 'Calculation') description
								  FROM   isr$template         t, 
                         isr$file$calc$values fpv, 
                         isr$stylesheet       s, 
                         isr$output$file      iof,
									       isr$output$transform iot,
                         reportlink           rl
								  WHERE  t.fileid = fpv.fileid
								  AND    fpv.VALUE = iot.bookmarkname
								  AND    iot.outputid = iof.outputid
								  AND    iof.fileid = t.fileid
                  AND    t.fileid = rl.fileid
								  AND    iot.stylesheetid = s.stylesheetid
								  ORDER BY 1, 2, 4) t1)
					 , (SELECT ( XMLAGG(XMLELEMENT("FILESTYLEVALUES",
                              XMLATTRIBUTES(TRIM(	templatename||' ['||version||']'||' '||description) AS "typename"),
                              XMLELEMENT("TEMPLATENAME", XMLATTRIBUTES('F' AS "visible"), templatename),
                              (SELECT XMLAGG(XMLELEMENT("STYLEVALUE", 
                                             XMLELEMENT("CLASS", entity),
                                             XMLELEMENT("STYLENAME", VALUE),
                                             XMLELEMENT("STYLEVALUE", display),
                                             XMLELEMENT("USE", use),
                                             XMLELEMENT("MODIFIEDON", modifiedon),
                                             XMLELEMENT("MODIFIEDBY", modifiedby )))
                               FROM  ( SELECT entity, VALUE, display, fpv.visible, editable, use, ordernumber,
                                              TO_CHAR(fpv.modifiedon, STB$UTIL.getSystemParameter('DATEFORMAT')) modifiedon,
                                              fpv.modifiedby, t.fileid
                                       FROM   isr$file$style$values fpv, isr$template t
                                       WHERE  fpv.fileid = t.fileid
                                       ORDER BY entity, VALUE ) t2
                               WHERE  t1.fileid = t2.fileid ))))
						  FROM   ( SELECT DISTINCT t.templatename, t.version, t.fileid, s.description
                       FROM   isr$template t, 
                              isr$stylesheet s, 
                              isr$output$file iof, 
                              isr$output$transform iot,
                              isr$report$output$type rot,
                              reportlink rl
                      WHERE  iot.outputid = iof.outputid
                      AND    iof.fileid = t.fileid
                      AND    rl.fileid = t.fileid
                      AND    iot.stylesheetid = s.stylesheetid
                      AND    s.structured = 'B'
                      AND    rot.visible = 'T'
                      AND    rot.outputid = iof.outputid
                      ORDER BY 1, 2, 4) t1)))
	FROM
		   DUAL