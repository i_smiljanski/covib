CREATE OR REPLACE VIEW ISR$SERVER$V (SERVERID, MAX_JOBS, OBSERVERID, DESCRIPTION, NAMESPACE, SERVERTYPENAME, SERVER_TYPE_RANK, SERVERTYPE_PLUGIN, SERVERTYPE_PACKAGE, SERVERTYPE_NODETYPE, SERVERTYPE_ICONID, ALIAS, PARAMETERNAME, PARAMETERVALUE, TOKEN, SERVERTYPEID, HOST, PORT, RUNSTATUS, CLASSNAME, CONTEXT, TIMEOUT) AS SELECT s.serverid, s.alias, st.servertypeid, s.host, s.port, s.runstatus, s.classname, s.context, s.timeout, s.max_jobs,  s.observerid, s.description, s.namespace, 
       st.servertypename, st.ordernumber "SERVER_TYPE_RANK", st.plugin "SERVERTYPE_PLUGIN", st.package "SERVERTYPE_PACKAGE", st.nodetype "SERVERTYPE_NODETYPE", st.iconid "SERVERTYPE_ICONID",
       sp.parametername, sp.parametervalue, 'CAN_MODIFY_'|| TO_CHAR(st.servertypeid)  ||'_SERVER' "TOKEN"
FROM   isr$server   s
LEFT OUTER JOIN isr$validvalue$servertype  st
ON     st.servertypeid = s.servertypeid
LEFT OUTER JOIN ( SELECT serverid, parametervalue, parametername
                  FROM isr$serverparameter
                  UNION
                  SELECT s.serverid,  CAST(s.alias AS VARCHAR2(4000)) "PARAMETERVALUE", 'ALIAS' "PARAMETERNAME"
                  FROM   isr$server s
                  UNION
                  SELECT s.serverid, CAST(s.serverid AS VARCHAR2(4000)) "PARAMETERVALUE", 'SERVERID' "PARAMETERNAME"
                  FROM   isr$server s
                  UNION
                  SELECT s.serverid,  CAST(s.servertypeid AS VARCHAR2(4000)) "PARAMETERVALUE", 'SERVERTYPEID' "PARAMETERNAME"
                  FROM   isr$server s
                  UNION
                  SELECT s.serverid, CAST(s.observerid AS VARCHAR2(4000)) "PARAMETERVALUE", 'OBSERVERID' "PARAMETERNAME"
                  FROM  isr$server s
                  UNION
                  SELECT s.serverid,  CAST(s.context AS VARCHAR2(4000)) "PARAMETERVALUE", 'CONTEXT' "PARAMETERNAME"
                  FROM   isr$server s
                  UNION
                  SELECT s.serverid, CAST(s.namespace AS VARCHAR2(4000)) "PARAMETERVALUE", 'NAMESPACE' "PARAMETERNAME"
                  FROM   isr$server s
                  UNION
                  SELECT s.serverid,  CAST(s.classname AS VARCHAR2(4000)) "PARAMETERVALUE", 'CLASSNAME' "PARAMETERNAME"
                  FROM   isr$server s
                )  sp
ON     sp.serverid = s.serverid