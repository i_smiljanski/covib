CREATE OR REPLACE VIEW ISR$SERVERDIALOG$V (SERVERTYPEID, SERVERID, RUNSTATUS, PARAMETERNAME, PARAMETERVALUE) AS select st.servertypeid, s.serverid,  s.runstatus, parametername,  isr$server$base.getServerParameter(s.serverid,parametername)
 from isr$dialog d, isr$validvalue$servertype st, isr$server s
 where token like 'CAN_MODIFY_' || st.servertypeid || '_SERVER'
 and d.parametertype !='TABSHEET'
 and d.parametername not like '%HIDDEN%'
 and s.servertypeid = st.servertypeid
 order by s.servertypeid,  s.serverid