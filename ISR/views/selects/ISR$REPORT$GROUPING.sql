CREATE OR REPLACE VIEW ISR$REPORT$GROUPING (GROUPINGNAME, GROUPINGID, GROUPINGVALUE, GROUPINGDATE, REPID) AS select GROUPINGNAME
       , key GROUPINGID
       , info GROUPINGVALUE
       , null GROUPINGDATE
       , repid
    from ISR$USED$GROUPING$ENTITIES, isr$crit
   where flag = 'C'
     and entity = REPLACE (REPLACE (groupingname, '_INFO'), 'GRP$')
     and TRIM (key) is not null
  union
  select GROUPINGNAME
       , key GROUPINGID
       , case
           when entity = 'SERVER' then
             (select alias || ' (' || serverid || ')'
                from isr$server
               where TO_CHAR (serverid) = key)
           else
             display
         end
           GROUPINGVALUE
       , null GROUPINGDATE
       , repid
    from ISR$USED$GROUPING$ENTITIES, isr$crit
   where flag = 'C'
     and entity = REPLACE (groupingname, 'GRP$')
     and TRIM (key) is not null
  union
  /* Output properties */
  select t.groupingname
       , t1.groupingvalue
       , t1.groupingvalue
       , null
       , repid
    from ISR$USED$GROUPING$ENTITIES t, isr$report$grouping$tab t1
   where flag = 'X'
     and t1.groupingname = REPLACE (t.groupingname, 'GRP$')
     and TRIM (t1.groupingvalue) is not null
  union
  /* Doc properties */
  select groupingname
       , parametervalue
       , parameterdisplay
       , null
       , repid
    from ISR$USED$GROUPING$ENTITIES, (select TO_NUMBER (d.nodeid) repid
                                           , parametername
                                           , parametervalue
                                           , NVL (parameterdisplay, parametervalue) parameterdisplay
                                        from stb$doctrail d, isr$doc$property dp
                                       where d.nodetype = 'REPORT'
                                         and d.docid = dp.docid)
   where flag = 'Y'
     and parametername = REPLACE (groupingname, 'GRP$')
     and TRIM (parametervalue) is not null
  union
  /* Output options */
  select groupingname
       , parametervalue
       , parametervalue
       , case when canhaveformatmask = 'T' then TO_DATE (parametervalue, systemDateFormate) else null end
       , repid
    from ISR$USED$GROUPING$ENTITIES, (select parametername, parametervalue, rp.repid
                                        from stb$reportparameter rp, stb$report r
                                       where condition != 0
                                         and rp.repid = r.repid), (select parametervalue systemDateFormate from stb$systemparameter where parametername = 'DATEFORMAT')
   where flag = 'O'
     and parametername = REPLACE (groupingname, 'GRP$')
     and TRIM (parametervalue) is not null
  union
  /* Report properties */
  select groupingname
       , parametervalue
       , parametervalue
       , case when canhaveformatmask = 'T' then TO_DATE (parametervalue, systemDateFormate) else null end
       , repid
    from ISR$USED$GROUPING$ENTITIES, ISR$GROUPING$OPTIONS$REPORT, (select parametervalue systemDateFormate from stb$systemparameter where parametername = 'DATEFORMAT')
   where flag = 'R'
     and parametername = groupingname
     and TRIM (parametervalue) is not null
  union
  /* Save dialog properties */
  select groupingname
       , parametervalue
       , parametervalue
       , case when canhaveformatmask = 'T' then TO_DATE (parametervalue, systemDateFormate) else null end
       , repid
    from ISR$USED$GROUPING$ENTITIES, ISR$GROUPING$OPTIONS$SAVE, (select parametervalue systemDateFormate from stb$systemparameter where parametername = 'DATEFORMAT')
   where flag = 'S'
     and parametername = groupingname
     and TRIM (parametervalue) is not null 