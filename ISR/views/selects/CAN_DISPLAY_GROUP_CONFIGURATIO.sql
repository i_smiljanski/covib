CREATE OR REPLACE VIEW CAN_DISPLAY_GROUP_CONFIGURATIO (XML) AS select (XMLELEMENT (
            "SYSTEMCONFIGURATION"
          , (select (XMLAGG(XMLELEMENT (
                              evalname (grouptype || '_DESCRIPTION')
                            , xmlattributes (grouptype as "REFERENCE")
                            , (select XMLAGG (XMLELEMENT (evalname (elementname), xmlattributes (attributename as "name"), case
                                                                                                                             when elementname like 'GROUP%' then
                                                                                                                               NVL ( (select 'x'
                                                                                                                                        from stb$userlink
                                                                                                                                       where userno = t1.userno
                                                                                                                                         and usergroupno = t.mynumber), '-')
                                                                                                                             else
                                                                                                                               elementvalue
                                                                                                                           end))
                                 from (select 'GROUP_' || usergroupno elementname
                                            , groupname attributename
                                            , null elementvalue
                                            , 2
                                            , usergroupno mynumber
                                            , grouptype mygrouptype
                                         from stb$usergroup, stb$user
                                       union
                                       select 'USER' elementname
                                            , null attributename
                                            , fullname elementvalue
                                            , 1
                                            , userno mynumber
                                            , null
                                         from stb$user
                                       order by 4
                                              , 2
                                              , 3
                                              , 1) t
                                where ( (t1.userno = t.mynumber
                                     and elementname = 'USER')
                                    or elementname != 'USER')
                                  and ( (t1.grouptype = mygrouptype
                                     and elementname != 'USER')
                                    or elementname = 'USER'))
                            )))
               from (select distinct grouptype, userno, fullname
                       from stb$usergroup, stb$user
                     order by 1, 3) t1)
          , (select (XMLAGG(XMLELEMENT (
                              "RIGHT"
                            , xmlattributes (
                                   UTD$MSGLIB.getMsg (type2, stb$security.getCurrentLanguage)
                                || ' '
                                || UTD$MSGLIB.getMsg (privilegeType, stb$security.getCurrentLanguage)
                                || ' '
                                || case when TYPE != 'SYSTEMRIGHTS' then UTD$MSGLIB.getMsg (TYPE, stb$security.getCurrentLanguage) end as "REFERENCE"
                              )
                            , XMLELEMENT ("REPORTTYPEID", XMLATTRIBUTES ('F' as "visible"), id)
                            , XMLELEMENT ("RIGHTTYPE", XMLATTRIBUTES ('F' as "visible"), type2)
                            , XMLELEMENT ("PRIVILEGETYPE", XMLATTRIBUTES ('F' as "visible"), privilegeType)
                            , (select XMLAGG (XMLELEMENT (evalname (elementname), xmlattributes (attributename as "name"), elementvalue))
                                 from (select 'GROUP_' || usergroupno elementname
                                            , groupname attributename
                                            , right elementvalue
                                            , 2
                                            , type2
                                            , TYPE
                                            , parametername
                                            , UTD$MSGLIB.getMsg(grouping, stb$security.getcurrentlanguage) grouping
                                            , folderid
                                         from isr$right$configuration1
                                        where type2 = 'GROUPRIGHTS'
                                       union
                                       select 'USER_' || usergroupno elementname
                                            , groupname attributename
                                            , right elementvalue
                                            , 3
                                            , type2
                                            , TYPE
                                            , parametername
                                            , UTD$MSGLIB.getMsg(grouping, stb$security.getcurrentlanguage) grouping
                                            , folderid
                                         from isr$right$configuration1
                                        where type2 = 'USERRIGHTS'
                                       union
                                       select 'RIGHT' elementname
                                            , null attributename
                                            ,    case when TRIM (grouping) is not null then UTD$MSGLIB.getMsg(grouping, stb$security.getcurrentlanguage) || ' > ' end
                                              || case when TRIM (folder) is not null then folder || ' > ' end
                                              || UTD$MSGLIB.getMsg(description, stb$security.getcurrentlanguage)
                                                elementvalue
                                            , 1
                                            , type2
                                            , TYPE
                                            , parametername
                                            , UTD$MSGLIB.getMsg(grouping, stb$security.getcurrentlanguage) grouping
                                            , folderid
                                         from isr$right$configuration1
                                       order by 4                                                              -- first the third union select, then the first, at last the second
                                                 , 2                                                                                                        -- the group/user name
                                                    ) t
                                where t.type2 = t1.type2
                                  and t.TYPE = t1.TYPE
                                  and t.parametername = t1.parametername
                                  and (t.GROUPING = t1.GROUPING
                                    or t1.GROUPING is null)
                                  and (t.folderid = t1.folderid
                                    or t1.folderid is null))
                            )))
               from (select distinct id
                                   , type2
                                   , TYPE
                                   , parametername
                                   , orderno
                                   , UTD$MSGLIB.getMsg(grouping, stb$security.getcurrentlanguage) grouping
                                   , folderid
                                   , SUBSTR (orderno, 1, 1)
                                   , case
                                       when TYPE != 'SYSTEMRIGHTS'
                                        and SUBSTR (orderno, 1, 1) = 1 then
                                         'REPORTTYPERIGHT'
                                       when TYPE != 'SYSTEMRIGHTS'
                                        and SUBSTR (orderno, 1, 1) = 2 then
                                         'REPORTTYPEPARAMETER'
                                       when TYPE != 'SYSTEMRIGHTS'
                                        and SUBSTR (orderno, 1, 1) = 3 then
                                         'REPORTPARAMETER'
                                       when TYPE != 'SYSTEMRIGHTS'
                                        and SUBSTR (orderno, 1, 1) = 4 then
                                         'TEMPLATE'
                                       when TYPE != 'SYSTEMRIGHTS'
                                        and SUBSTR (orderno, 1, 1) = 5 then
                                         'WIZARDSCREEN'
                                       when TYPE = 'SYSTEMRIGHTS'
                                        and SUBSTR (orderno, 1, 1) = 1 then
                                         'SYSTEMRIGHT'
                                       when TYPE = 'SYSTEMRIGHTS'
                                        and SUBSTR (orderno, 1, 1) = 2 then
                                         'SYSTEMPARAMETER'
                                     end
                                       privilegeType
                       from isr$right$configuration1
                     order by 3
                            , 2
                            , 8
                            , 6
                            , 7
                            , 5) t1)
          ))
    from DUAL 