CREATE OR REPLACE VIEW ISR$GROUPING$OPTIONS$REPORT (PARAMETERVALUE, PARAMETERNAME, REPID) AS (select createdby parametervalue, nodename parametername, repid
     from stb$report, (select distinct nodename
                         from isr$custom$grouping
                        where nodename = 'GRP$USER')
    where condition != 0
   union
   select (select r.createdby
             from stb$report r
            where r.parentrepid is null
           start with r.repid = r1.repid
           connect by r.repid = prior r.parentrepid)
            parametervalue, nodename parametername, repid
     from stb$report r1, (select distinct nodename
                            from isr$custom$grouping
                           where nodename = 'GRP$CREATOR')
    where condition != 0
   union
   select TO_CHAR (MODIFIEDON, STB$UTIL.getSystemParameter ('DATEFORMAT')) parametervalue, nodename parametername, repid
     from stb$report, (select distinct nodename
                         from isr$custom$grouping
                        where nodename = 'GRP$MODIFIEDON')
    where condition != 0
   union
   select TO_CHAR (d.MODIFIEDON, STB$UTIL.getSystemParameter ('DATEFORMAT')) parametervalue, nodename parametername, repid
     from stb$report r, stb$doctrail d, (select distinct nodename
                                           from isr$custom$grouping
                                          where nodename = 'GRP$DOC$MODIFIEDON')
    where condition != 0
      and d.nodeid = TO_CHAR (r.repid)
      and d.nodetype = 'REPORT'
   union
   select rf.doc_definition parametervalue, nodename parametername, r.repid
     from stb$report r, isr$report$file rf, (select distinct nodename
                                           from isr$custom$grouping
                                          where nodename = 'GRP$DOC_DEFINITION')
    where condition != 0
      and rf.repid = r.repid
      and trim(rf.doc_definition) is not null
   union
   select groupname parametervalue, nodename parametername, repid
     from stb$report r
        , stb$usergroup ug
        , stb$userlink ul
        , (select distinct nodename
             from isr$custom$grouping
            where nodename = 'GRP$DATAGROUP')
    where r.datagroup = ug.usergroupno
      and ul.userno = STB$SECURITY.getCurrentUser
      and ul.usergroupno = ug.usergroupno
      and condition != 0
   union
   select distinct templatename parametervalue, nodename parametername, repid
     from isr$template t, stb$report r, (select distinct nodename
                                           from isr$custom$grouping
                                          where nodename = 'GRP$TEMPLATE')
    where r.template = t.fileid
      and condition != 0
   union
   select templatename || ' [' || t.version || ']' parametervalue, nodename parametername, repid
     from isr$template t, stb$report r, (select distinct nodename
                                           from isr$custom$grouping
                                          where nodename = 'GRP$TEMPLATEVERSION')
    where r.template = t.fileid
      and condition != 0
   union
   select UTD$MSGLIB.getMsg (reporttypename, stb$security.GetCurrentLanguage) parametervalue, nodename parametername, repid
     from stb$report r, stb$reporttype rt, (select distinct nodename
                                              from isr$custom$grouping
                                             where nodename = 'GRP$REPORTTYPE')
    where rt.reporttypeid = r.reporttypeid
      and condition != 0
   union
   select UTD$MSGLIB.getMsg (case
                               when TO_CHAR (status) = '1' then
                                 '1 - Draft'
                               when TO_CHAR (status) = '2' then
                                 case
                                   when (select MOD (COUNT ( * ), 2)
                                           from STB$ESIG
                                          where reference = TO_CHAR (repid)
                                            and action like 'CAN_%LOCK_REPORT'
                                            and success = 'T') = 1 then
                                     '2 - Locked'
                                   else
                                     '2 - Inspection'
                                 end
                               when TO_CHAR (status) = '3'
                                 or TO_CHAR (status) = '999' then
                                 '3 - Final'
                               else
                                 'Not defined'
                             end, stb$security.GetCurrentLanguage)
            parametervalue, nodename parametername, repid
     from stb$report, (select distinct nodename
                         from isr$custom$grouping
                        where nodename = 'GRP$STATUS')
    where condition != 0
   union
   select UTD$MSGLIB.getMsg ('Active', stb$security.GetCurrentLanguage) parametervalue, 'GRP$CONDITION' parametername, repid
     from stb$report                                                                                            -- do not join with customgrouping since condition is always asked
    where condition != 0
   union
   select UTD$MSGLIB.getMsg (MIN (case when r2.version = '0.1' then 'Active' else 'Archived' end), stb$security.GetCurrentLanguage) parametervalue
        , 'GRP$CURRENT_STATE' parametername
        , r.repid
     from stb$report r
        , STB$REPORT r1
        , STB$REPORT r2
        , (select distinct nodename
             from isr$custom$grouping
            where nodename = 'GRP$CURRENT_STATE')
    where TO_CHAR (r1.repid) = r.repid
      and r1.srnumber = r2.srnumber
      and r1.title = r2.title
   group by r.repid
   /*UNION
   SELECT UTD$MSGLIB.getMsg (CASE
                                WHEN TO_CHAR (condition) = '0' THEN
                                   'Deactivated'
                                WHEN TO_CHAR (condition) = '-2' THEN
                                   'Locked'
                                ELSE
                                   (SELECT MIN (CASE WHEN r2.version = '0.1' THEN 'Active' ELSE 'Archived' END)
                                      FROM STB$REPORT r1, STB$REPORT r2
                                     WHERE TO_CHAR (r1.repid) = r.repid
                                       AND r1.srnumber = r2.srnumber
                                       AND r1.title = r2.title)
                             END, stb$security.GetCurrentLanguage)
             parametervalue, 'GRP$CURRENT_STATE' parametername, repid
     FROM stb$report r*/
   union
   select UTD$MSGLIB.getMsg (case
                               when TO_CHAR (status) = '1' then
                                 '1 - Draft'
                               when TO_CHAR (status) = '2' then
                                 case
                                   when (select MOD (COUNT ( * ), 2)
                                           from STB$ESIG
                                          where reference = TO_CHAR (r.repid)
                                            and action like 'CAN_%LOCK_REPORT'
                                            and success = 'T') = 1 then
                                     '2 - Locked'
                                   else
                                     '2 - Inspection'
                                 end
                               when TO_CHAR (status) = '3'
                                 or TO_CHAR (status) = '999' then
                                 '3 - Final'
                               else
                                 'Not defined'
                             end, stb$security.GetCurrentLanguage)
            parametervalue, nodename parametername, r.repid
     from (select MAX (repid) repid, title
             from stb$report
           group by title) r1, stb$report r, (select distinct nodename
                                                from isr$custom$grouping
                                               where nodename = 'GRP$CURRENTSTATUS')
    where r1.repid = r.repid
      and r.condition != 0) 