CREATE OR REPLACE VIEW CAN_DISPLAY_ARCHIVE_LOG (XML) AS SELECT INSERTCHILDXML (
             XMLTYPE ('<ARCHIVELOG></ARCHIVELOG>')
           , '/*[1]'
           , 'LOG'
           , EXTRACT (
                (SELECT XMLAGG(XMLTYPE(   '<LOGFILE>
                                              <LOG>
                                                <JOBID>'|| jobid || '</JOBID>
                                                <LINE>
                                                  <![CDATA['
                                       || REPLACE (logfile, CHR (10), ']]></LINE></LOG><LOG><JOBID>' || jobid || '</JOBID><LINE><![CDATA[') || 
                                                  ']]>
                                                </LINE>
                                              </LOG>
                                           </LOGFILE>'))
                   FROM ISR$ARCHIVED$REPORTS)
              , '/LOGFILE/LOG[LINE/text()!="<![CDATA[]]>"]'
             )
          ) AS xml
     FROM DUAL