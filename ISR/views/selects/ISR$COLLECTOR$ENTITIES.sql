CREATE OR REPLACE VIEW ISR$COLLECTOR$ENTITIES (ORDERCOL, ENTITYNAME, BRANCH, LEAF) AS WITH relations AS ( SELECT r.*
                     FROM   isr$meta$relation         r,
                            isr$meta$relation$report  rr    
                     WHERE  r.relationname = rr.relationname
                     AND    rr.reporttypeid = STB$OBJECT.getCurrentReporttypeid )
SELECT DISTINCT 0             "ORDERCOL",
       rroot.parententityname "ENTITYNAME",
       rroot.parententityname "BRANCH",      -- ROOT_NODE of branch
       0                      "LEAF"
FROM   relations rroot
WHERE  rroot.parententityname NOT IN (SELECT childentityname FROM relations)
UNION  -- Union causes DISTINCT, therefore no distinct within analytical function necessary
SELECT MAX(LEVEL) OVER( PARTITION BY rbranch.childentityname, CONNECT_BY_ROOT rbranch.parententityname, CONNECT_BY_ISLEAF ) "ORDERCOL", 
       rbranch.childentityname                  "ENTITYNAME",
       CONNECT_BY_ROOT rbranch.parententityname "BRANCH",    -- ROOT_NODE of branch
       CONNECT_BY_ISLEAF                        "LEAF"
FROM   relations rbranch
START WITH rbranch.parententityname IN ( SELECT parententityname FROM relations MINUS SELECT childentityname FROM relations )
CONNECT BY NOCYCLE rbranch.parententityname = PRIOR rbranch.childentityname