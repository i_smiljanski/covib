CREATE OR REPLACE VIEW ISR$GROUPING$VIEW (GROUPINGFLAG, GROUPINGNAME, DESCRIPTION, SELECTED) AS SELECT Utd$msglib.GetMsg ("FLAG" || '_GROUPING_FLAG'
, Stb$security.GetCurrentLanguage)
, "GROUPINGNAME"
, "DESCRIPTION"
, NVL ( (SELECT 'T'
FROM isr$grouping$tab t
WHERE t.groupingname = g.groupingname), 'F')
FROM isr$grouping g