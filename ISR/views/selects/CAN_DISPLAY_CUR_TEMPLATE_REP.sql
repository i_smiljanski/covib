CREATE OR REPLACE VIEW CAN_DISPLAY_CUR_TEMPLATE_REP (XML) AS SELECT (XMLELEMENT (
              "SYSTEMCONFIGURATION"
            , (SELECT (XMLAGG(XMLELEMENT ("TEMPLATE"
                                        , XMLELEMENT ("FILEID", XMLATTRIBUTES ('F' AS "visible"), fileid)
                                        , (SELECT XMLAGG (XMLELEMENT ("TEMPLATE"
                                                                    , XMLELEMENT ("TEMPLATENAME", templatename)
                                                                    , XMLELEMENT ("VERSION", version)
                                                                    , XMLELEMENT ("FILENAME", filename)
                                                                    , XMLELEMENT ("RELEASE", release)
                                                                    , XMLELEMENT ("RELEASEDBY", releasedby)
                                                                    , XMLELEMENT ("RELEASEDON", releasedon)
                                                                    , XMLELEMENT ("EXCHANGE_COMMENT", exchange_comment)
                                                                    , XMLELEMENT ("MODIFIEDON", modifiedon)
                                                                    , XMLELEMENT ("MODIFIEDBY", modifiedby)))
                                             FROM (SELECT t.templatename
                                                        , t.exchange_comment
                                                        , t.release
                                                        , t.fileid
                                                        , f.filename
                                                        , CASE
                                                             WHEN t.release = 'T'
                                                              AND t.releasedby IS NULL THEN
                                                                'Installation User'
                                                             ELSE
                                                                t.releasedby
                                                          END
                                                             releasedby
                                                        , CASE
                                                             WHEN t.release = 'T'
                                                              AND t.releasedon IS NULL THEN
                                                                'Installation Date'
                                                             ELSE
                                                                TO_CHAR (t.releasedon, STB$UTIL.getSystemParameter ('DATEFORMAT'))
                                                          END
                                                             releasedon
                                                        , t.version
                                                        , TO_CHAR (t.modifiedon, STB$UTIL.getSystemParameter ('DATEFORMAT')) modifiedon
                                                        , t.modifiedby
                                                     FROM isr$file f, isr$template t
                                                    WHERE t.fileid = f.fileid
                                                   ORDER BY t.templatename, t.version) t2
                                            WHERE t1.fileid = t2.fileid))))
                 FROM (SELECT DISTINCT t.fileid
                         FROM isr$template t
                       ORDER BY 1) t1)
            , (SELECT (XMLAGG (XMLELEMENT ("FILEPARAMETERSVALUES"
                                         , XMLATTRIBUTES (TRIM (description) AS "typename")
                                         , XMLELEMENT ("FILEID", XMLATTRIBUTES ('F' AS "visible"), fileid)
                                         , (SELECT XMLAGG (XMLELEMENT ("PARAMETERVALUE"
                                                                     , XMLELEMENT ("BOOKMARKVALUE", VALUE)
                                                                     , XMLELEMENT ("FORMAT_ENTITY", entity)
                                                                     , XMLELEMENT ("DISPLAY", display)
                                                                     , XMLELEMENT ("INFO", info)
                                                                     , XMLELEMENT ("USE", use)
                                                                     , XMLELEMENT ("ORDERNUMBER", ordernumber)
                                                                     , XMLELEMENT ("CLASS", class)
                                                                     , XMLELEMENT ("STYLE", style)
                                                                     , XMLELEMENT ("VISIBLE", XMLATTRIBUTES ('F' AS "visible"), visible)
                                                                     , XMLELEMENT ("MODIFIEDON", modifiedon)
                                                                     , XMLELEMENT ("MODIFIEDBY", modifiedby)))
                                              FROM (SELECT entity
                                                         , VALUE
                                                         , display
                                                         , info
                                                         , fpv.visible
                                                         , editable
                                                         , use
                                                         , ordernumber
                                                         , (SELECT ordernumber
                                                              FROM isr$parameter$values pv
                                                             WHERE pv.entity = 'FORMAT_ENTITY'
                                                               AND pv.VALUE = fpv.entity)
                                                              entityordernumber
                                                         , TO_CHAR (fpv.modifiedon, STB$UTIL.getSystemParameter ('DATEFORMAT')) modifiedon
                                                         , fpv.modifiedby
                                                         , fpv.fileid
                                                         , fpv.class
                                                         , fpv.style
                                                      FROM isr$file$parameter$values fpv, isr$template t
                                                     WHERE fpv.fileid = t.fileid
                                                    ORDER BY entityordernumber, ordernumber) t2
                                             WHERE t1.fileid = t2.fileid
                                               AND t1.VALUE = t2.VALUE))))
                 FROM (SELECT DISTINCT t.templatename
                                     , t.version
                                     , t.fileid
                                     , fpv.VALUE
                                     , s.description
                         FROM isr$template t
                            , isr$file$parameter$values fpv
                            , isr$stylesheet s
                            , isr$output$file iof
                            , isr$output$transform iot
                        WHERE t.fileid = fpv.fileid
                          AND fpv.VALUE = iot.bookmarkname
                          AND iot.outputid = iof.outputid
                          AND iof.fileid = t.fileid
                          AND iot.stylesheetid = s.stylesheetid
                       ORDER BY 1, 2, 4) t1)
            , (SELECT (XMLAGG (XMLELEMENT ("FILECALCVALUES"
                                         , XMLATTRIBUTES (TRIM (description) AS "typename")
                                         , XMLELEMENT ("FILEID", XMLATTRIBUTES ('F' AS "visible"), fileid)
                                         , (SELECT XMLAGG (XMLELEMENT ("CALCVALUE"
                                                                     , XMLELEMENT ("BOOKMARKVALUE", experiment)
                                                                     , XMLELEMENT ("CALC_BLOCK", VALUE)
                                                                     , XMLELEMENT ("FORMAT_ENTITY", entity)
                                                                     , XMLELEMENT ("DISPLAY", display)
                                                                     , XMLELEMENT ("INFO", info)
                                                                     , XMLELEMENT ("USE", use)
                                                                     , XMLELEMENT ("ORDERNUMBER", ordernumber)
                                                                     , XMLELEMENT ("VISIBLE", XMLATTRIBUTES ('F' AS "visible"), visible)
                                                                     , XMLELEMENT ("MODIFIEDON", modifiedon)
                                                                     , XMLELEMENT ("MODIFIEDBY", modifiedby)))
                                              FROM (SELECT entity
                                                         , (SELECT DISTINCT rpv.VALUE
                                                              FROM ISR$FILE$CALC$VALUES rpv
                                                             WHERE rpv.display = fpv.VALUE
                                                               AND rpv.fileid = fpv.fileid
                                                               AND rpv.entity = 'WT$EXP$CALC'
                                                               AND use = 'T')
                                                              experiment
                                                         , VALUE
                                                         , display
                                                         , info
                                                         , fpv.visible
                                                         , editable
                                                         , use
                                                         , ordernumber
                                                         , (SELECT ordernumber
                                                              FROM isr$parameter$values pv
                                                             WHERE pv.entity = 'FORMAT_ENTITY'
                                                               AND pv.VALUE = fpv.entity)
                                                              entityordernumber
                                                         , TO_CHAR (fpv.modifiedon, STB$UTIL.getSystemParameter ('DATEFORMAT')) modifiedon
                                                         , fpv.modifiedby
                                                         , t.fileid
                                                      FROM isr$file$calc$values fpv, isr$template t
                                                     WHERE fpv.fileid = t.fileid
                                                    ORDER BY entityordernumber, ordernumber) t2
                                             WHERE t1.fileid = t2.fileid
                                               AND t1.VALUE = t2.VALUE))))
                 FROM (SELECT DISTINCT t.templatename
                                     , t.version
                                     , t.fileid
                                     , fpv.VALUE
                                     , REPLACE (s.description, 'Stylesheet', 'Calculation') description
                         FROM isr$template t
                            , isr$file$calc$values fpv
                            , isr$stylesheet s
                            , isr$output$file iof
                            , isr$output$transform iot
                        WHERE t.fileid = fpv.fileid
                          AND fpv.VALUE = iot.bookmarkname
                          AND iot.outputid = iof.outputid
                          AND iof.fileid = t.fileid
                          AND iot.stylesheetid = s.stylesheetid
                       ORDER BY 1, 2, 4) t1)
            , (SELECT (XMLAGG (XMLELEMENT ("FILESTYLEVALUES"
                                         , XMLATTRIBUTES (TRIM (description) AS "typename")
                                         , XMLELEMENT ("FILEID", XMLATTRIBUTES ('F' AS "visible"), fileid)
                                         , (SELECT XMLAGG (XMLELEMENT ("STYLEVALUE"
                                                                     , XMLELEMENT ("CLASS", entity)
                                                                     , XMLELEMENT ("STYLENAME", VALUE)
                                                                     , XMLELEMENT ("STYLEVALUE", display)
                                                                     , XMLELEMENT ("USE", use)
                                                                     , XMLELEMENT ("VISIBLE", XMLATTRIBUTES ('F' AS "visible"), visible)
                                                                     , XMLELEMENT ("MODIFIEDON", modifiedon)
                                                                     , XMLELEMENT ("MODIFIEDBY", modifiedby)))
                                              FROM (SELECT entity
                                                         , VALUE
                                                         , display
                                                         , fpv.visible
                                                         , editable
                                                         , use
                                                         , ordernumber
                                                         , TO_CHAR (fpv.modifiedon, STB$UTIL.getSystemParameter ('DATEFORMAT')) modifiedon
                                                         , fpv.modifiedby
                                                         , t.fileid
                                                      FROM isr$file$style$values fpv, isr$template t
                                                     WHERE fpv.fileid = t.fileid
                                                    ORDER BY entity, VALUE) t2
                                             WHERE t1.fileid = t2.fileid))))
                 FROM (SELECT DISTINCT t.templatename
                                     , t.version
                                     , t.fileid
                                     , s.description
                         FROM isr$template t
                            , isr$stylesheet s
                            , isr$output$file iof
                            , isr$output$transform iot
                            , isr$report$output$type rot
                            , stb$reporttype r
                        WHERE iot.outputid = iof.outputid
                          AND iof.fileid = t.fileid
                          AND iot.stylesheetid = s.stylesheetid
                          AND s.structured = 'B'
                          AND rot.visible = 'T'
                          AND rot.outputid = iof.outputid
                          AND rot.reporttypeid = r.reporttypeid
                          AND r.activetype = 1
                       ORDER BY 1, 2, 4) t1)
           ))
     FROM DUAL 