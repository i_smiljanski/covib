CREATE OR REPLACE VIEW CAN_DISPLAY_TRANSLATION_REPORT (XML) AS SELECT (XMLELEMENT ("SYSTEMCONFIGURATION", (SELECT XMLAGG (XMLELEMENT ("TRANSLATIONENTRY"
                                                                        , XMLELEMENT ("NAME", description)
                                                                        , XMLELEMENT ("LANG", lang)
                                                                        , XMLELEMENT ("ABBREVIATION", abbreviation)
                                                                        , XMLELEMENT ("MSGKEY", msgkey)
                                                                        , XMLELEMENT ("MSGTEXT", msgtext)
                                                                        , XMLELEMENT ("PLUGIN", plugin)))
                                                 FROM (SELECT msgkey
                                                            , msgtext
                                                            , lang
                                                            , m.plugin
                                                            , description
                                                            , abbreviation
                                                         FROM utd$msg m, utd$language l
                                                        WHERE m.lang = l.langid
                                                          AND l.langid != 999
                                                       ORDER BY 3, 1, 2))))
     FROM DUAL