CREATE OR REPLACE VIEW ISR$RIGHT$CONFIGURATION (ID, TYPE, PARAMETERNAME, DESCRIPTION, ORDERNO, USERGROUPNO, GROUPNAME, TYPE2, RIGHT) AS select 0 ID
       , 'SYSTEMRIGHTS' TYPE
       , a.parametername
       , s.description
       , case when s.userparameter = 'T' then '1' else '2' end || LPAD (s.orderno, 6, '0')
       , g.usergroupno
       , g.groupname
       , 'GROUPRIGHTS' TYPE2
       , DECODE (a.parametervalue, 'T', 'x', '-') RIGHT
    from stb$systemparameter s, stb$usergroup g, stb$adminrights a
   where a.usergroupno = g.usergroupno
     and a.parametername = s.parametername
     and g.grouptype = 'FUNCTIONGROUP'
     and s.visible = 'T'
  union
  select 0 ID
       , 'SYSTEMRIGHTS' TYPE
       , a.parametername
       , s.description
       , case when s.userparameter = 'T' then '1' else '2' end || LPAD (s.orderno, 6, '0')
       , U.userno
       , U.fullname
       , 'USERRIGHTS' TYPE2
       , DECODE (a.parametervalue, 'T', 'x', '-') RIGHT
    from stb$systemparameter s
       , stb$user U
       , stb$userlink l
       , stb$usergroup g
       , stb$adminrights a
   where a.usergroupno = l.usergroupno
     and l.userno = U.userno
     and l.usergroupno = g.usergroupno
     and g.grouptype = 'FUNCTIONGROUP'
     and s.visible = 'T'
     and a.parametername = s.parametername
     and (2, a.parametervalue) not in (select COUNT (distinct r.parametervalue), 'F'
                                         from stb$userlink l, stb$adminrights r
                                        where r.usergroupno = l.usergroupno
                                          and l.userno = U.userno
                                          and a.parametername = r.parametername
                                       group by l.userno, r.parametername)
  union
  select r.reporttypeid ID
       , r.reporttypename TYPE
       , gr.parametername
       , rp.description
       , rp.orderno
       , g.usergroupno
       , g.groupname
       , 'GROUPRIGHTS' TYPE2
       , DECODE (gr.parametervalue, 'T', 'x', '-') RIGHT
    from (select rt.reporttypeid
               , parametername
               , rt.description description
               , case when NVL (repparameter, 'F') = 'T' then '3' else case when userparameter = 'T' then '1' else '2' end end || LPAD (orderno, 6, '0') orderno
            from stb$reptypeparameter rt, stb$reporttype r
           where visible = 'T'
             and editable = 'T'
             and r.activetype = 1
             and r.reporttypeid = rt.reporttypeid
          union
          select distinct rot.reporttypeid
                        , 'TEMPLATE' || TO_CHAR (t.fileid)
                        , t.templatename || ' [' || t.version || ']'
                        , '4' || LPAD (t.fileid, 6, '0') orderno
            from isr$template t
               , isr$output$file iof
               , isr$report$output$type rot
               , stb$reporttype r
           where t.visible = 'T'
             and rot.visible = 'T'
             and rot.outputid = iof.outputid
             and iof.fileid = t.fileid
             and rot.reporttypeid is not null
             and r.activetype = 1
             and r.reporttypeid = rot.reporttypeid
          union
          select rw.reporttypeid
               , TO_CHAR (maskno)
               , title description
               , '5' || LPAD (maskno, 6, '0') orderno
            from isr$report$wizard rw, stb$reporttype r
           where visible = 'T'
             and masktype != 5
             and r.activetype = 1
             and r.reporttypeid = rw.reporttypeid) rp
       , stb$reporttype r
       , stb$usergroup g
       , stb$group$reptype$rights gr
   where gr.usergroupno = g.usergroupno
     and gr.parametername = rp.parametername
     and gr.reporttypeid = rp.reporttypeid
     and r.reporttypeid = gr.reporttypeid
     and g.grouptype = 'FUNCTIONGROUP'
  union
  select distinct r.reporttypeid ID
                , r.reporttypename TYPE
                , gr.parametername
                , rp.description
                , rp.orderno
                , U.userno
                , U.fullname
                , 'USERRIGHTS' TYPE2
                , DECODE (gr.parametervalue, 'T', 'x', '-') RIGHT
    from (select rt.reporttypeid
               , parametername
               , rt.description description
               , case when NVL (repparameter, 'F') = 'T' then '3' else case when userparameter = 'T' then '1' else '2' end end || LPAD (orderno, 6, '0') orderno
            from stb$reptypeparameter rt, stb$reporttype r
           where visible = 'T'
             and editable = 'T'
             and r.activetype = 1
             and r.reporttypeid = rt.reporttypeid
          union
          select distinct rot.reporttypeid
                        , 'TEMPLATE' || TO_CHAR (t.fileid)
                        , t.templatename || ' [' || t.version || ']'
                        , '4' || LPAD (t.fileid, 6, '0') orderno
            from isr$template t
               , isr$output$file iof
               , isr$report$output$type rot
               , stb$reporttype r
           where t.visible = 'T'
             and rot.visible = 'T'
             and rot.outputid = iof.outputid
             and iof.fileid = t.fileid
             and rot.reporttypeid is not null
             and r.activetype = 1
             and r.reporttypeid = rot.reporttypeid
          union
          select rw.reporttypeid
               , TO_CHAR (maskno)
               , title description
               , '5' || LPAD (maskno, 6, '0') orderno
            from isr$report$wizard rw, stb$reporttype r
           where visible = 'T'
             and masktype != 5
             and r.activetype = 1
             and r.reporttypeid = rw.reporttypeid) rp
       , stb$reporttype r
       , stb$user U
       , stb$userlink l
       , stb$usergroup g
       , stb$group$reptype$rights gr
   where r.reporttypeid = rp.reporttypeid
     and gr.usergroupno = l.usergroupno
     and l.userno = U.userno
     and l.usergroupno = g.usergroupno
     and g.grouptype = 'FUNCTIONGROUP'
     and gr.parametername = rp.parametername
     and gr.reporttypeid = rp.reporttypeid
     and (2, gr.parametervalue) not in (select COUNT (distinct parametervalue), 'F'
                                          from stb$userlink l, stb$group$reptype$rights r
                                         where r.usergroupno = l.usergroupno
                                           and l.userno = U.userno
                                           and r.parametername = gr.parametername
                                           and r.reporttypeid = gr.reporttypeid
                                        group by l.userno, r.reporttypeid, r.parametername)
  order by 1 nulls first
         , 8
         , 5
         , 4
         , 6 