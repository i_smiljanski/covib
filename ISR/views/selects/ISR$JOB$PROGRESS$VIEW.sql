CREATE OR REPLACE VIEW ISR$JOB$PROGRESS$VIEW (FUNCTIONTOCALL, ACTIONID, ACTIONORDER, DESCRIPTION, FUNCTIONCNT, FUNCTIONCUR, PROGRESS_ESTIMATED, PROGRESS_CALCULATED, TIME_ESTIMATED) AS SELECT aj.functiontocall
, aj.actionid
, aj.actionorder
, NVL (TRIM(aj.description), aj.functiontocall) description
,                                                  -- next to not used
(SELECT MAX (actionorder)
FROM isr$action$job
WHERE actionid = aj.actionid)
functionCnt
, actionorder functionCur
, (SELECT ROUND (  AVG (time)
/ (SELECT case when sum (avg_time) = 0 then -1 else sum (avg_time) end 
FROM (SELECT AVG (time) avg_time, actionid, text
FROM isr$job$progress
GROUP BY actionid, text) t
WHERE t.actionid = j.actionid)
* 100)
FROM isr$job$progress j
WHERE aj.functiontocall = text
AND aj.actionid = actionid
GROUP BY actionid, text)
progress_estimated
, ROUND ( (1
/ (SELECT MAX (actionorder)
FROM isr$action$job
WHERE actionid = aj.actionid))
* 100)
progress_calculated
, (SELECT (AVG (time)
/ (SELECT case when sum (avg_time) = 0 then -1 else sum (avg_time) end 
FROM (SELECT AVG (time) avg_time, actionid, text
FROM isr$job$progress
GROUP BY actionid, text) t
WHERE t.actionid = j.actionid))
* 60
FROM isr$job$progress j
WHERE aj.functiontocall = text
AND aj.actionid = actionid
GROUP BY actionid, text)
time_estimated
FROM ISR$ACTION$JOB aj
ORDER BY aj.actionid, aj.actionorder