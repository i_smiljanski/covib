CREATE OR REPLACE VIEW CAN_DISPLAY_ACTIVITY_LOG (XML) AS SELECT (XMLELEMENT ("ROWSET", (SELECT XMLAGG (XMLELEMENT ("ROW"
                                                           , XMLELEMENT ("ENTRYID", entryid)
                                                           , XMLELEMENT ("ORACLEUSER", ORACLEUSER)
                                                           , XMLELEMENT ("CONTEXT", CONTEXT)
                                                           , XMLELEMENT ("DATE", logdate)
                                                           , XMLELEMENT ("ACTION", action)
                                                           , XMLELEMENT ("REASON", reason)
                                                           , XMLELEMENT ("TIMESTAMPRAW", TIMESTAMPRAW)
                                                           , XMLELEMENT ("REPORT_LOGIN_INFORMATION", REPORT_LOGIN_INFORMATION)
                                                           , XMLELEMENT ("REPORT_MAINTENANCE_INFORMATION", REPORT_MAINTENANCE_INFORMATION)))
                                    FROM (SELECT entryid
                                               , oracleusername ORACLEUSER
                                               , reference CONTEXT
                                               , TO_CHAR (timestamp, STB$DOCKS.getDateFormat ('CAN_DISPLAY_ACTIVITY_LOG')) logdate
                                               , action
                                               , reason
                                               , TO_CHAR ( (timestamp - TO_DATE ('01.01.2000', 'dd.mm.yyyy')) * 24 * 60 * 60) TIMESTAMPRAW
                                               , CASE WHEN action IN ('LOGON', 'SHUTDOWN') THEN 'T' ELSE '' END REPORT_LOGIN_INFORMATION
                                               , CASE WHEN action IN ('MAINTENANCE') THEN 'T' ELSE '' END REPORT_MAINTENANCE_INFORMATION
                                            FROM isr$actionlog
                                          ORDER BY entryid DESC))))
     FROM DUAL