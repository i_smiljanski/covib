CREATE OR REPLACE VIEW ISR$SERVER$DEPLOYMENT$V (FILEID, SERVERTYPEID, TARGET, FILENAME, BINARYFILE, JAVA_VERSION, UNZIP, PLATFORM_ARCHITECTURE, PLATFORM_NAME) AS select d.fileid, servertypeid, target, f.filename,  binaryfile, java_version, unzip, platform_architecture, platform_name
    from isr$server$deployment d, isr$server$file f
    where d.fileid = f.fileid