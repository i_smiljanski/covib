CREATE OR REPLACE VIEW CAN_DISPLAY_INVENTORY (XML) AS SELECT (XMLELEMENT ("ROWSET", (SELECT XMLAGG (XMLELEMENT ("ROW"
                                                           , XMLELEMENT ("OBJECTTYPE", objecttype)
                                                           , XMLELEMENT ("OBJECTDESCRIPTION", objectdescription)
                                                           , XMLELEMENT ("CREATEDON", createdon)
                                                           , XMLELEMENT ("MODIFIEDON", modifiedon)
                                                           , XMLELEMENT ("MODIFIEDBY", modifiedby)))
                                    FROM (SELECT DISTINCT ' JARFILE' objecttype
                                                        , jarfileversion objectversion
                                                        , jarfile objectdescription
                                                        , TO_CHAR (MAX (loadedon), STB$DOCKS.getDateFormat ('CAN_DISPLAY_INVENTORY')) createdon
                                                        , TO_CHAR (MAX (completedon), STB$DOCKS.getDateFormat ('CAN_DISPLAY_INVENTORY')) modifiedon
                                                        , NULL modifiedby
                                                        , MAX (loadedon) sort1
                                                        , MAX (completedon) sort2
                                            FROM isr$installtable
                                          GROUP BY jarfile, jarfileversion
                                          UNION
                                          SELECT REPLACE (objecttype, ' ', '_') objecttype
                                               , objectversion
                                               , objectdescription
                                               , TO_CHAR (createdon, STB$DOCKS.getDateFormat ('CAN_DISPLAY_INVENTORY')) createdon
                                               , TO_CHAR (modifiedon, STB$DOCKS.getDateFormat ('CAN_DISPLAY_INVENTORY')) modifiedon
                                               , modifiedby
                                               , createdon sort1
                                               , modifiedon sort2
                                            FROM isr$inventory
                                          ORDER BY 7 DESC, 8 DESC))))
     FROM DUAL