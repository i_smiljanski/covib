CREATE OR REPLACE VIEW CAN_DISPLAY_ESIG_ALL (XML) AS SELECT (XMLELEMENT ("ROWSET", (SELECT XMLAGG (XMLELEMENT ("ROW"
                                                           , XMLELEMENT ("ENTRYID", ENTRYID)
                                                           , XMLELEMENT ("ORACLEUSER", ORACLEUSER)
                                                           , XMLELEMENT ("FULLNAME", FULLNAME)
                                                           , XMLELEMENT ("REFERENCE", REFERENCE)
                                                           , XMLELEMENT ("DATE", esigdate)
                                                           , XMLELEMENT ("ACTION", ACTION)
                                                           , XMLELEMENT ("REASON", REASON)
                                                           , XMLELEMENT ("REASONDISPLAY", REASONDISPLAY)
                                                           , XMLELEMENT ("SUCCESS", SUCCESS)
                                                           , XMLELEMENT ("TIMESTAMPRAW", TIMESTAMPRAW)
                                                           , XMLELEMENT ("ENTITY", ENTITY)
                                                           , XMLELEMENT ("SRNUMBER", SRNUMBER)
                                                           , XMLELEMENT ("TITLE", TITLE)
                                                           , XMLELEMENT ("IPADDRESS", IPADDRESS)
                                                           , XMLELEMENT ("ISRSESSION", ISRSESSION)
                                                           , XMLELEMENT ("NODETYPE", NODETYPE)
                                                           , XMLELEMENT ("STATUS", STATUS)
                                                           , XMLELEMENT ("VERSION", VERSION)))
                                    FROM (SELECT entryid
                                               , oracleusername ORACLEUSER
                                               , fullname
                                               , reference
                                               , TO_CHAR (timestamp, STB$DOCKS.getDateFormat ('CAN_DISPLAY_ESIG_ALL')) "ESIGDATE"
                                               , UTD$MSGLIB.getMsg (action, STB$SECURITY.getCurrentLanguage) action
                                               , reason
                                               , reasondisplay
                                               , success
                                               , TO_CHAR ( (timestamp - TO_DATE ('01.01.2000', 'dd.mm.yyyy')) * 24 * 60 * 60) TIMESTAMPRAW
                                               , entity
                                               , NVL (srnumber, 'NO_SRNUMBER') srnumber
                                               , NVL (title, 'NO_TITLE') title
                                               , ipaddress
                                               , isrsession
                                               , UTD$MSGLIB.getMsg (nodetype, STB$SECURITY.getCurrentLanguage) nodetype
                                               , UTD$MSGLIB.getMsg (CASE
                                                                       WHEN nodetype = 'REPORT' THEN
                                                                          (SELECT 'STATUS_ESIG_' || status
                                                                             FROM stb$report
                                                                            WHERE TO_CHAR (repid) = reference)
                                                                       WHEN nodetype = 'DOCUMENT' THEN
                                                                          (SELECT 'STATUS_ESIG_' || status
                                                                             FROM stb$report r, stb$doctrail d
                                                                            WHERE TO_CHAR (r.repid) = d.nodeid
                                                                              AND TO_CHAR (d.docid) = reference)
                                                                       ELSE
                                                                          ''
                                                                    END, STB$SECURITY.getCurrentLanguage)
                                                    status
                                               , CASE
                                                    WHEN nodetype = 'REPORT' THEN
                                                       (SELECT version
                                                          FROM stb$report
                                                         WHERE TO_CHAR (repid) = reference)
                                                    WHEN nodetype = 'DOCUMENT' THEN
                                                       (SELECT version
                                                          FROM stb$report r, stb$doctrail d
                                                         WHERE TO_CHAR (r.repid) = d.nodeid
                                                           AND TO_CHAR (d.docid) = reference)
                                                    ELSE
                                                       ''
                                                 END
                                                    version
                                            FROM stb$esig
                                          ORDER BY entryid DESC))))
     FROM DUAL