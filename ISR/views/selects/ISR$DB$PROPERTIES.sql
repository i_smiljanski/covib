CREATE OR REPLACE VIEW ISR$DB$PROPERTIES (SERVER, DBUSER, DBPASSWORD, SERVERALIAS, ORDERNO, SCHEMA) AS SELECT SUBSTR (p1.parametervalue, INSTR (p1.parametervalue, '@') + 1)
, p2.parametervalue
, p3.parametervalue
, alias
, s.serverid
, USER
FROM isr$server s, 
  isr$serverparameter p1,
  isr$serverparameter p2,
  isr$serverparameter p3
WHERE servertypeid = 8
AND LENGTH (SUBSTR (p1.parametervalue, INSTR (p1.parametervalue, '@') + 1)) > 0
AND s.serverid=p1.serverid
AND p1.parametername='DBURL'
AND s.serverid=p2.serverid
AND p2.parametername='USERNAME'
AND s.serverid=p3.serverid
AND p3.parametername='PASSWORD'
ORDER BY alias