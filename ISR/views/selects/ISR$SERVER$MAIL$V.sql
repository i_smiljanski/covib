CREATE OR REPLACE VIEW ISR$SERVER$MAIL$V (HOST, PORT, USERNAME, PASSWORD, RUNSTATUS) AS select isr$server$base.getServerParameter(serverid, 'HOST'),
        isr$server$base.getServerParameter(serverid, 'PORT') , 
        isr$server$base.getServerParameter(serverid, 'USERNAME'), 
        isr$server$base.getServerParameter(serverid, 'PASSWORD') ,
        runstatus
                       from isr$server 
                      where servertypeid = 7