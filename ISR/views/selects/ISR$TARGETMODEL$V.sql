CREATE OR REPLACE VIEW ISR$TARGETMODEL$V (TARGETMODELID, TARGETMODEL_NAME, SERVERTYPEID, PLUGIN, ENTITYNAME, PROCESSING_HANDLER, CREATE_SCRIPT) AS select tm.targetmodelid, tm.targetmodel_name, tm.servertypeid, tm.plugin, me.entityname, me.processing_handler, me.create_script
     from   isr$servertype$targetmodel tm, isr$meta$entity$targetmodel me
    where tm.targetmodelid = me.targetmodelid