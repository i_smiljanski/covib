CREATE OR REPLACE VIEW ISR$GROUPING$OPTIONS$SAVE (PARAMETERVALUE, PARAMETERNAME, REPID) AS (SELECT groupname parametervalue, nodename parametername, repid
      FROM stb$report r
         , stb$usergroup ug
         , stb$userlink ul
         , (SELECT DISTINCT nodename
              FROM isr$custom$grouping
             WHERE nodename = 'DATAGROUP')
     WHERE r.datagroup = ug.usergroupno
       AND ul.userno = STB$SECURITY.getCurrentUser
       AND ul.usergroupno = ug.usergroupno
       AND condition != 0
    UNION
    SELECT srnumber parametervalue, nodename parametername, repid
      FROM stb$report r, (SELECT DISTINCT nodename
                            FROM isr$custom$grouping
                           WHERE nodename = 'SRNUMBER')
     WHERE TRIM (srnumber) IS NOT NULL
       AND condition != 0
    UNION
    SELECT title parametervalue, nodename parametername, repid
      FROM stb$report r, (SELECT DISTINCT nodename
                            FROM isr$custom$grouping
                           WHERE nodename = 'TITLE')
     WHERE TRIM (srnumber) IS NOT NULL
       AND condition != 0
    UNION
    SELECT remarks parametervalue, nodename parametername, repid
      FROM stb$report r, (SELECT DISTINCT nodename
                            FROM isr$custom$grouping
                           WHERE nodename = 'REMARKS')
     WHERE TRIM (remarks) IS NOT NULL
       AND condition != 0)