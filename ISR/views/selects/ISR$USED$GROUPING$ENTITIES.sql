CREATE OR REPLACE VIEW ISR$USED$GROUPING$ENTITIES (GROUPINGNAME, FLAG, CANHAVEFORMATMASK) AS SELECT DISTINCT g.groupingname, flag, canhaveformatmask
     FROM isr$grouping$tab t, isr$grouping g, isr$custom$grouping cg
    WHERE (g.groupingname = nodename
        OR g.groupingname = 'GRP$CONDITION')
      AND t.groupingname = g.groupingname