CREATE OR REPLACE VIEW CAN_DISPLAY_LOG_REPORT (XML) AS select (xmlelement ("ROWSET",
                      (select xmlagg (xmlelement ("ROW",
                                                  xmlelement ("ENTRYID", ENTRYID),
                                                  xmlelement ("ORACLEUSER", ORACLEUSER),
                                                  xmlelement ("CONTEXT", CONTEXT),
                                                  xmlelement ("DATE", logdate),
                                                  xmlelement ("ACTION", ACTION),
                                                  xmlelement ("LOGENTRY", LOGENTRY),
                                                  xmlelement ("ACTIONPLACE", ACTIONPLACE),
                                                  xmlelement ("SESSIONID", SESSIONID),
                                                  xmlelement ("IP", IP),
                                                  xmlelement ("LOGLEVEL", LOGLEVEL),
                                                  xmlelement ("TIMESTAMPRAW", TIMESTAMPRAW)))
                         from (select ENTRYID,
                                      ORACLEUSERNAME                                                               ORACLEUSER,
                                      REFERENCE                                                                    CONTEXT,
                                      to_char (timestamp, STB$DOCKS.getDateFormat)                logdate,
                                      ACTIONTAKEN                                                                  action,
                                      LOGENTRY,
                                      FUNCTIONGROUP                                                                actionplace,
                                      SESSIONID,
                                      IP,
                                      LOGLEVEL,
                                      to_char ( (timestamp - to_date ('01.01.2000', 'dd.mm.yyyy')) * 24 * 60 * 60) TIMESTAMPRAW
                                 from isr$log
                                where entryid >   (select last_number
                                                     from user_sequences
                                                    where sequence_name = 'STB$LOG$SEQ')
                                                - STB$UTIL.getSystemParameter ('MAX_LOG_REPORT_ENTRIES')))))
    from dual