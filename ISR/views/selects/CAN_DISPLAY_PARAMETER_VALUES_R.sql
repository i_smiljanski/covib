CREATE OR REPLACE VIEW CAN_DISPLAY_PARAMETER_VALUES_R (XML) AS SELECT (XMLELEMENT ("SYSTEMCONFIGURATION", (SELECT XMLAGG (XMLELEMENT ("PARAMETER_VALUE"
, XMLELEMENT ("ENTITY", XMLATTRIBUTES ('F' AS "visible"), entity)
, XMLELEMENT ("VALUE", VALUE)
, XMLELEMENT ("DISPLAY", display)
, XMLELEMENT ("INFO", info)
, XMLELEMENT ("ORDERNUMBER", ordernumber)
, XMLELEMENT ("EDITABLE_VALUE", editable_value)
, XMLELEMENT ("EDITABLE_ENTITY", editable_entity)
, XMLELEMENT ("PLUGIN", plugin)
, XMLELEMENT ("DESCRIPTION", description)))
FROM (SELECT pv.entity
, VALUE
, display
, info
, ordernumber
, pv.editable editable_value
, pe.editable editable_entity
, pe.plugin
, description
FROM isr$parameter$values pv, isr$parameter$entities pe
WHERE pv.entity = pe.entity))))
FROM DUAL