CREATE OR REPLACE VIEW CAN_DISPLAY_CONFIGURATION (XML) AS select (XMLELEMENT ("SYSTEMCONFIGURATION"
                    , (select XMLAGG (XMLELEMENT ("REPORTTYPE"
                                                , XMLELEMENT ("REPORTTYPENAME", reporttypename)
                                                , XMLELEMENT ("REPORTTYPEID", reporttypeid)
                                                , XMLELEMENT ("DESCRIPTION", description)
                                                , XMLELEMENT ("MODIFIEDON", modifiedon)
                                                , XMLELEMENT ("MODIFIEDBY", modifiedby)))
                         from (select r.reporttypename
                                    , r.description
                                    , r.reporttypeid
                                    , TO_CHAR (r.modifiedon, STB$DOCKS.getDateFormat ('CAN_DISPLAY_CONFIGURATION')) modifiedon
                                    , r.modifiedby
                                 from STB$REPORTTYPE r
                                where r.activetype = 1
                                  and (r.reporttypeid <> 999
                                    or (r.reporttypeid = 999
                                    and STB$UTIL.getSystemParameter ('SHOW_UNIT_TEST') = 'T'))
                               order by r.reporttypeid))
                    , (select XMLAGG (XMLELEMENT ("PARAMETER"
                                                , XMLELEMENT ("REPORTTYPENAME", reporttypename)
                                                , XMLELEMENT ("REPORTTYPEID", reporttypeid)
                                                , XMLELEMENT ("PARAMETERNAME", parametername)
                                                , XMLELEMENT ("PARAMETERVALUE", parametervalue)
                                                , XMLELEMENT ("DESCRIPTION", description)
                                                , XMLELEMENT ("MODIFIEDON", modifiedon)
                                                , XMLELEMENT ("MODIFIEDBY", modifiedby)
                                                , XMLELEMENT ("EDITABLE", editable)
                                                , XMLELEMENT ("VISIBLE", visible)
                                                , XMLELEMENT ("REQUIRED", required)
                                                , XMLELEMENT ("HASLOV", haslov)
                                                , XMLELEMENT ("ORDERNO", orderno)
                                                , XMLELEMENT ("REPPARAMETER", repparameter)))
                         from (select parametername
                                    , NVL (parametervalue, 'NOT SPECIFIED') parametervalue
                                    , Utd$msglib.GetMsg (description, Stb$security.GetCurrentLanguage) description
                                    , TO_CHAR (modifiedon, STB$DOCKS.getDateFormat ('CAN_DISPLAY_CONFIGURATION')) modifiedon
                                    , modifiedby
                                    , editable
                                    , visible
                                    , required
                                    , haslov
                                    , null repparameter
                                    , orderno
                                    , 0 reporttypeid
                                    , UTD$MSGLIB.getMsg ('NO_REPORTTYPE_NAME', stb$security.GetCurrentLanguage) reporttypename
                                 from stb$systemparameter
                                where userparameter = 'F'
                               union
                               select parametername
                                    , NVL (parametervalue, 'NOT SPECIFIED') parametervalue
                                    , Utd$msglib.GetMsg (description, Stb$security.GetCurrentLanguage) description
                                    , TO_CHAR (modifiedon, STB$DOCKS.getDateFormat ('CAN_DISPLAY_CONFIGURATION')) modifiedon
                                    , modifiedby
                                    , editable
                                    , visible
                                    , required
                                    , haslov
                                    , repparameter
                                    , orderno
                                    , rp.reporttypeid
                                    , reporttypename
                                 from stb$reptypeparameter rp, (select reporttypename, reporttypeid
                                                                  from STB$REPORTTYPE r
                                                                 where r.activetype = 1
                                                                   and (r.reporttypeid <> 999
                                                                     or (r.reporttypeid = 999
                                                                     and STB$UTIL.getSystemParameter ('SHOW_UNIT_TEST') = 'T'))) r
                                where userparameter = 'F'
                                  and rp.reporttypeid = r.reporttypeid
                               order by reporttypeid
                                      , visible
                                      , repparameter
                                      , orderno))
                    , (select XMLAGG (XMLELEMENT ("DOCPROPERTY"
                                                , XMLELEMENT ("REPORTTYPENAME", reporttypename)
                                                , XMLELEMENT ("REPORTTYPEID", reporttypeid)
                                                , XMLELEMENT ("PARAMETERNAME", parametername)
                                                , XMLELEMENT ("PARAMETERDEFAULT", parameterdefault)
                                                , XMLELEMENT ("PARAMETERTYPE", parametertype)
                                                , XMLELEMENT ("EDITABLE", editable)
                                                , XMLELEMENT ("VISIBLE", visible)
                                                , XMLELEMENT ("REQUIRED", required)
                                                , XMLELEMENT ("HASLOV", haslov)
                                                , XMLELEMENT ("ORDERNO", orderno)
                                                , XMLELEMENT ("MODIFIEDON", modifiedon)
                                                , XMLELEMENT ("MODIFIEDBY", modifiedby)))
                         from (select parametername
                                    , parameterdefault
                                    , parametertype
                                    , visible
                                    , editable
                                    , required
                                    , haslov
                                    , TO_CHAR (modifiedon, STB$DOCKS.getDateFormat ('CAN_DISPLAY_CONFIGURATION')) modifiedon
                                    , modifiedby
                                    , orderno
                                    , dpt.reporttypeid
                                    , reporttypename
                                 from isr$doc$prop$template dpt, (select reporttypename, reporttypeid
                                                                    from STB$REPORTTYPE r
                                                                   where r.activetype = 1
                                                                     and (r.reporttypeid <> 999
                                                                       or (r.reporttypeid = 999
                                                                       and STB$UTIL.getSystemParameter ('SHOW_UNIT_TEST') = 'T'))) r
                                where dpt.reporttypeid = r.reporttypeid))
                    , (select XMLAGG (XMLELEMENT ("STYLESHEET"
                                                , XMLELEMENT ("REPORTTYPENAME", reporttypename)
                                                , XMLELEMENT ("REPORTTYPEID", reporttypeid)
                                                , XMLELEMENT ("NAME", filename)
                                                , XMLELEMENT ("DESCRIPTION", description)
                                                , XMLELEMENT ("MODIFIEDON", modifiedon)
                                                , XMLELEMENT ("MODIFIEDBY", modifiedby)
                                                , XMLELEMENT ("EXCHANGE_COMMENT", exchange_comment)
                                                , XMLELEMENT ("STRUCTURED", structured)
                                                , XMLELEMENT ("BOOKMARKNAME", bookmarkname)
                                                , XMLELEMENT ("EXTENSION", extension)))
                         from (select distinct iss.filename
                                             , iss.description
                                             , TO_CHAR (iss.modifiedon, STB$DOCKS.getDateFormat ('CAN_DISPLAY_CONFIGURATION')) modifiedon
                                             , iss.modifiedby
                                             , iss.exchange_comment
                                             , iss.structured
                                             , iss.extension
                                             , rot.bookmarkname
                                             , rot.reporttypeid
                                             , reporttypename
                                 from isr$stylesheet iss, (select reporttypename, reporttypeid
                                                             from STB$REPORTTYPE r
                                                            where r.activetype = 1
                                                              and r.reporttypeid in (select reporttypeid
                                                                                       from isr$report$output$type rot, isr$output$transform ot
                                                                                      where rot.outputid = ot.outputid)
                                                              and (r.reporttypeid <> 999
                                                                or (r.reporttypeid = 999
                                                                and STB$UTIL.getSystemParameter ('SHOW_UNIT_TEST') = 'T'))
                                                           union
                                                           select UTD$MSGLIB.getMsg ('NO_REPORTTYPE_NAME', stb$security.GetCurrentLanguage) reporttypename, 0 from DUAL
                                                           order by reporttypeid) r, (select reporttypeid, stylesheetid, bookmarkname
                                                                                        from isr$report$output$type rot, isr$output$transform ot
                                                                                       where rot.outputid = ot.outputid) rot
                                where rot.stylesheetid = iss.stylesheetid
                                  and (rot.reporttypeid = r.reporttypeid
                                    or rot.reporttypeid is null
                                   and r.reporttypeid = 0)
                               order by 6, 1))
                    , (select XMLAGG (XMLELEMENT ("FILE"
                                                , XMLELEMENT ("REPORTTYPENAME", reporttypename)
                                                , XMLELEMENT ("REPORTTYPEID", reporttypeid)
                                                , XMLELEMENT ("NAME", filename)
                                                , XMLELEMENT ("DESCRIPTION", description)
                                                , XMLELEMENT ("MODIFIEDON", modifiedon)
                                                , XMLELEMENT ("MODIFIEDBY", modifiedby)
                                                , XMLELEMENT ("EXCHANGE_COMMENT", exchange_comment)
                                                , XMLELEMENT ("USAGE", usage)
                                                , XMLELEMENT ("DOC_DEFINITION", DOC_DEFINITION)
                                                , XMLELEMENT ("MIMETYPE", MIMETYPE)))
                         from (select distinct iff.filename
                                             , TRIM (   iff.description
                                                     || ' '
                                                     || (select ' - ' || templatename || ' [' || version || ']'
                                                           from isr$template
                                                          where fileid = iff.fileid))
                                                 description
                                             , TO_CHAR (iff.modifiedon, STB$DOCKS.getDateFormat ('CAN_DISPLAY_CONFIGURATION')) modifiedon
                                             , iff.modifiedby
                                             , iff.exchange_comment
                                             , iff.usage
                                             , iff.fileid
                                             , iff.doc_definition
                                             , iff.mimetype
                                             , rot.reporttypeid
                                             , reporttypename
                                 from isr$file iff, (select reporttypename, reporttypeid
                                                       from STB$REPORTTYPE r
                                                      where r.activetype = 1
                                                        and r.reporttypeid in (select reporttypeid
                                                                                 from isr$report$output$type rot, isr$output$transform ot
                                                                                where rot.outputid = ot.outputid)
                                                        and (r.reporttypeid <> 999
                                                          or (r.reporttypeid = 999
                                                          and STB$UTIL.getSystemParameter ('SHOW_UNIT_TEST') = 'T'))
                                                     union
                                                     select UTD$MSGLIB.getMsg ('NO_REPORTTYPE_NAME', stb$security.GetCurrentLanguage) reporttypename, 0 from DUAL
                                                     order by reporttypeid) r, (select reporttypeid, fileid
                                                                                  from isr$report$output$type rot, isr$output$file iof
                                                                                 where rot.outputid = iof.outputid) rot
                                where rot.fileid = iff.fileid
                                  and (rot.reporttypeid = r.reporttypeid
                                    or rot.reporttypeid is null
                                   and r.reporttypeid = 0)
                               order by 6, 5))))
    from DUAL 