CREATE OR REPLACE TRIGGER ISR_isr$validv$servertype_U
BEFORE UPDATE
ON isr$validvalue$servertype REFERENCING OLD AS old NEW AS new
FOR EACH ROW
BEGIN
  :New.MODIFIEDON := SYSDATE;
  :New.MODIFIEDBY := STB$SECURITY.getOracleUserName(STB$SECURITY.getCurrentUser);
end;