CREATE OR REPLACE TRIGGER "ISR_ISR$TRANSFER$REMOTE$TMP_I" BEFORE INSERT
ON "ISR$TRANSFER$REMOTEDATA$TMP" REFERENCING OLD AS old NEW AS new
FOR EACH ROW
BEGIN
  :New.MODIFIEDON := SYSDATE;
  :New.MODIFIEDBY := STB$SECURITY.getOracleUserName(STB$SECURITY.getCurrentUser);
  if :New.blobdata is not null then
    :New.FILESIZE := dbms_lob.getLength(:New.blobdata);
  elsif :New.clobdata is not null then
    :New.FILESIZE := dbms_lob.getLength(:New.clobdata);
  end if;
end;