CREATE OR REPLACE TRIGGER ISR_STB$USER_U2 
BEFORE UPDATE OF LOGLEVELNAME, LOGLEVEL ON STB$USER REFERENCING OLD AS old NEW AS new
FOR EACH ROW
BEGIN
  
  if :new.loglevelname != :old.loglevelname then    
    :NEW.LOGLEVEL := isr$trace.getLoglevelno(:New.LOGLEVELNAME);    
  elsif :new.loglevel != :old.loglevel then
    :new.loglevelname := isr$trace.getLoglevelStr(:new.loglevel);  
  end if;  
END;