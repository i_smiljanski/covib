CREATE OR REPLACE TRIGGER ISR_STB$SYSTEMPARAMETER_I
BEFORE INSERT
ON STB$SYSTEMPARAMETER REFERENCING OLD AS old NEW AS new
FOR EACH ROW
BEGIN
  :New.MODIFIEDON := SYSDATE;
  :New.MODIFIEDBY := STB$SECURITY.getOracleUserName(STB$SECURITY.getCurrentUser);
  /*insert into stb$adminrights(parametername, usergroupno, plugin, parametervalue)
  (select :new.parametername, usergroupno, :new.plugin, 'T'
     from stb$usergroup
    where grouptype = 'FUNCTIONGROUP');  */
end;