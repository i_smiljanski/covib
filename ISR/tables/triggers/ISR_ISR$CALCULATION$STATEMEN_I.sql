CREATE OR REPLACE TRIGGER ISR_ISR$CALCULATION$STATEMEN_I
BEFORE INSERT
ON ISR$CALCULATION$STATEMENTS REFERENCING OLD AS old NEW AS new
FOR EACH ROW
BEGIN
  :New.MODIFIEDON := SYSDATE;
  :New.MODIFIEDBY := STB$SECURITY.getOracleUserName(STB$SECURITY.getCurrentUser);
end;