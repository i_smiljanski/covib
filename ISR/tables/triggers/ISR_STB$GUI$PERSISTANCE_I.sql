CREATE OR REPLACE TRIGGER ISR_STB$GUI$PERSISTANCE_I
BEFORE INSERT
ON STB$GUI$PERSISTANCE REFERENCING OLD AS old NEW AS new
FOR EACH ROW
BEGIN
  :New.MODIFIEDON := SYSDATE;
  :New.MODIFIEDBY := STB$SECURITY.getOracleUserName(STB$SECURITY.getCurrentUser);
end;