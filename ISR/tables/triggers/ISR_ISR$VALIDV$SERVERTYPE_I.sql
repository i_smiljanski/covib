CREATE OR REPLACE TRIGGER ISR_isr$validv$servertype_I
BEFORE INSERT
ON  isr$validvalue$servertype REFERENCING OLD AS old NEW AS new
FOR EACH ROW
BEGIN
  :New.MODIFIEDON := SYSDATE;
  :New.MODIFIEDBY := STB$SECURITY.getOracleUserName(STB$SECURITY.getCurrentUser);
end;