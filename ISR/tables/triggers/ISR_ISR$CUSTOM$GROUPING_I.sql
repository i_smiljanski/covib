CREATE OR REPLACE TRIGGER ISR_ISR$CUSTOM$GROUPING_I
BEFORE INSERT
ON ISR$CUSTOM$GROUPING REFERENCING OLD AS old NEW AS new
FOR EACH ROW
BEGIN
  :New.MODIFIEDON := SYSDATE;
  :New.MODIFIEDBY := STB$SECURITY.getOracleUserName(STB$SECURITY.getCurrentUser);
end;