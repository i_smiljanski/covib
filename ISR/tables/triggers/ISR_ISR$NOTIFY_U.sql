CREATE OR REPLACE TRIGGER ISR_ISR$NOTIFY_U
  BEFORE UPDATE
  ON ISR$NOTIFY REFERENCING OLD AS old NEW AS new
  for each row
declare

begin

--- update modified flags
:New.ModifiedBy := User;
:New.ModifiedOn := SYSDATE;

end;