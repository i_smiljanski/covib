CREATE OR REPLACE trigger ISR_ISR$TREE$NODES_I
  before insert
  on ISR$TREE$NODES
  referencing old as old new as new
  for each row
begin
  :New.MODIFIEDON := SYSDATE;
  :New.MODIFIEDBY := USER;
end; 