CREATE OR REPLACE TRIGGER ISR_ISR$FILE_U
BEFORE UPDATE
ON ISR$FILE REFERENCING OLD AS old NEW AS new
FOR EACH ROW
BEGIN
  :New.MODIFIEDON := SYSDATE;
  :New.MODIFIEDBY := STB$SECURITY.getOracleUserName(STB$SECURITY.getCurrentUser);
  :New.DOCSIZE := dbms_lob.getlength(:New.textfile);
  if :New.DOCSIZE = 0 then
    dbms_lob.open(:New.binaryfile, DBMS_LOB.LOB_READWRITE);
    :New.DOCSIZE := dbms_lob.getlength(:New.binaryfile);
    dbms_lob.close(:New.binaryfile);
  end if;
end;