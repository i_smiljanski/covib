CREATE OR REPLACE TRIGGER "ISR$SERVER$FILE_I" before insert
   on "ISR$SERVER$FILE"
   referencing old as old new as new
   for each row
declare
   tmpVar              number;
begin
--   select SEQ$ISR$FILE$SERVER.NEXTVAL into tmpVar from DUAL;

--   :NEW.fileid := tmpVar;
   :New.MODIFIEDON := SYSDATE;
   :New.MODIFIEDBY := STB$SECURITY.getOracleUserName (STB$SECURITY.getCurrentUser);
--    if :New.binaryfile is not null  then 
--      :New.FILESIZE := dbms_lob.getlength(:New.binaryfile);
--    end if;
end;