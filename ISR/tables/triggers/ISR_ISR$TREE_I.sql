CREATE OR REPLACE TRIGGER ISR_ISR$TREE_I
  before insert
  ON ISR$TREE   referencing old as old new as new
  for each row
begin
  :New.MODIFIEDON := SYSDATE;
  :New.MODIFIEDBY := USER;
end; 