CREATE OR REPLACE TRIGGER ISR_ISR$REPORT$OUTPUT$TYPE_I
BEFORE INSERT
ON ISR$REPORT$OUTPUT$TYPE REFERENCING OLD AS old NEW AS new
FOR EACH ROW
BEGIN
  :New.MODIFIEDON := SYSDATE;
  :New.MODIFIEDBY := STB$SECURITY.getOracleUserName(STB$SECURITY.getCurrentUser);
  /*if :new.reporttypeid is not null then
    insert into stb$group$reptype$rights(parametername, reporttypeid, usergroupno, plugin, parametervalue)
    (select :new.outputid, :new.reporttypeid, usergroupno, :new.plugin, 'T'
       from stb$usergroup
      where grouptype = 'FUNCTIONGROUP');  
  end if;*/
end;