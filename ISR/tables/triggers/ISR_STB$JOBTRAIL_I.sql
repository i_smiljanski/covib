CREATE OR REPLACE TRIGGER ISR_STB$JOBTRAIL_I
BEFORE INSERT
ON STB$JOBTRAIL REFERENCING OLD AS old NEW AS new
FOR EACH ROW
BEGIN
  if :new.entryid is null then
    :new.entryid := STB$JOBTRAIL$SEQ.NEXTVAL;
  end if;
  
  if :new.timestamp is null then
    :new.timestamp := sysdate;
  end if;
end;