CREATE OR REPLACE trigger ISR_ISR$META$FIXED$RULE_U
before update
on ISR$META$FIXED$RULE referencing old as old new as new
for each row
begin
  :New.MODIFIEDON := sysdate;
  :New.MODIFIEDBY := STB$SECURITY.getOracleUserName(STB$SECURITY.getCurrentUser);
end;