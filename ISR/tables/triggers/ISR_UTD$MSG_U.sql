CREATE OR REPLACE TRIGGER ISR_UTD$MSG_U
BEFORE UPDATE
ON UTD$MSG REFERENCING OLD AS old NEW AS new
FOR EACH ROW
BEGIN
  :New.MODIFIEDON := SYSDATE;
  :New.MODIFIEDBY := STB$SECURITY.getOracleUserName(STB$SECURITY.getCurrentUser);
end;