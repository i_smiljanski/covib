CREATE OR REPLACE trigger ISR_ISR$META$FIXED$RULE_I
before insert
on ISR$META$FIXED$RULE referencing old as old new as new
for each row
begin
  :New.MODIFIEDON := nvl(:New.modifiedon,sysdate);
  :New.MODIFIEDBY := nvl(:New.modifiedby,STB$SECURITY.getOracleUserName(STB$SECURITY.getCurrentUser));
end;