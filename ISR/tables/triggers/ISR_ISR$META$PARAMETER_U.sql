CREATE OR REPLACE TRIGGER ISR_ISR$META$PARAMETER_U
BEFORE UPDATE
ON ISR$META$PARAMETER REFERENCING OLD AS old NEW AS new
FOR EACH ROW
BEGIN
  :New.MODIFIEDON := SYSDATE;
  :New.MODIFIEDBY := STB$SECURITY.getOracleUserName(STB$SECURITY.getCurrentUser);
end;