CREATE OR REPLACE TRIGGER ISR_ISR$TREE_U
  before update
  ON ISR$TREE   referencing old as old new as new
  for each row
begin
  :New.MODIFIEDON := SYSDATE;
  :New.MODIFIEDBY := USER;
end; 