CREATE OR REPLACE TRIGGER ISR_STB$SYSTEMPARAMETER_U
BEFORE UPDATE
ON STB$SYSTEMPARAMETER REFERENCING OLD AS old NEW AS new
FOR EACH ROW
BEGIN
  :New.MODIFIEDON := SYSDATE;
  :New.MODIFIEDBY := STB$SECURITY.getOracleUserName(STB$SECURITY.getCurrentUser);
end;