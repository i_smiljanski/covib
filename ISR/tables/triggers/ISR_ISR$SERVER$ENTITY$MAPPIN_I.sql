CREATE OR REPLACE TRIGGER ISR_ISR$SERVER$ENTITY$MAPPIN_I
BEFORE INSERT
ON ISR$SERVER$ENTITY$MAPPING REFERENCING OLD AS old NEW AS new
FOR EACH ROW
BEGIN
  :New.MODIFIEDON := SYSDATE;
  :New.MODIFIEDBY := STB$SECURITY.getOracleUserName(STB$SECURITY.getCurrentUser);
end;