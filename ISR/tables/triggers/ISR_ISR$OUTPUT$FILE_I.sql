CREATE OR REPLACE TRIGGER ISR_ISR$OUTPUT$FILE_I
BEFORE INSERT
ON ISR$OUTPUT$FILE REFERENCING OLD AS old NEW AS new
FOR EACH ROW
BEGIN
  :New.MODIFIEDON := SYSDATE;
  :New.MODIFIEDBY := STB$SECURITY.getOracleUserName(STB$SECURITY.getCurrentUser);
end;