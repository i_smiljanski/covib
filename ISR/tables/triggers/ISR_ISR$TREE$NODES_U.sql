CREATE OR REPLACE trigger ISR_ISR$TREE$NODES_U
  before update
  on ISR$TREE$NODES
  referencing new as NEW old as OLD
  for each row
begin
  :New.MODIFIEDON := SYSDATE;
  :New.MODIFIEDBY := USER;
end; 