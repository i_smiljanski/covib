CREATE OR REPLACE TRIGGER "ISR_ISR$SERVER$FILE_B_U" BEFORE UPDATE
ON "ISR$SERVER$FILE" REFERENCING OLD AS old NEW AS new
FOR EACH ROW
BEGIN
  :New.MODIFIEDON := SYSDATE;
  :New.MODIFIEDBY := STB$SECURITY.getOracleUserName(STB$SECURITY.getCurrentUser); 
  
  /*dbms_lob.open(:New.textfile, DBMS_LOB.LOB_READWRITE);
  :New.FILESIZE := dbms_lob.getlength(:New.textfile);
  dbms_lob.close(:New.textfile);
  if :New.FILESIZE < 1 then
    dbms_lob.open(:New.binaryfile, DBMS_LOB.LOB_READWRITE);
    :New.FILESIZE := dbms_lob.getlength(:New.binaryfile);
    dbms_lob.close(:New.binaryfile);
  end if; */
end;