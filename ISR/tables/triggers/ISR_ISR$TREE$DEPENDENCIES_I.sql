CREATE OR REPLACE trigger ISR_ISR$TREE$DEPENDENCIES_I
  before insert
  on ISR$TREE$DEPENDENCIES
  referencing old as old new as new
  for each row
begin
  :New.MODIFIEDON := SYSDATE;
  :New.MODIFIEDBY := USER;
end; 