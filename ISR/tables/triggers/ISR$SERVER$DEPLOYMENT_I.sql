CREATE OR REPLACE TRIGGER ISR$SERVER$DEPLOYMENT_I
   before insert
   ON ISR$SERVER$DEPLOYMENT 
   referencing old as old new as new
   for each row
begin
  
   :New.MODIFIEDON := SYSDATE;
   :New.MODIFIEDBY := STB$SECURITY.getOracleUserName (STB$SECURITY.getCurrentUser);
   select filename into :New.filename from isr$server$file where fileid = :New.fileid;
end;