CREATE OR REPLACE TRIGGER ISR_STB$REPTYPEPARAMETER_I
BEFORE INSERT
ON STB$REPTYPEPARAMETER 
REFERENCING NEW AS NEW OLD AS OLD
FOR EACH ROW
BEGIN
  :New.MODIFIEDON := SYSDATE;
  :New.MODIFIEDBY := STB$SECURITY.getOracleUserName(STB$SECURITY.getCurrentUser);
  /*insert into stb$group$reptype$rights(parametername, reporttypeid, usergroupno, plugin, parametervalue)
  (select :new.parametername, :new.reporttypeid, usergroupno, :new.plugin, 'T'
     from stb$usergroup
    where grouptype = 'FUNCTIONGROUP');*/
end;