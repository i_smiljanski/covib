CREATE OR REPLACE trigger ISR_ISR$TREE$DEPENDENCIES_U
  before update
  on ISR$TREE$DEPENDENCIES
  referencing new as NEW old as OLD
  for each row
begin
  :New.MODIFIEDON := SYSDATE;
  :New.MODIFIEDBY := USER;
end; 