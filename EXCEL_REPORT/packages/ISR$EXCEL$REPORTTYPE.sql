CREATE OR REPLACE PACKAGE isr$excel$reporttype AS

FUNCTION getNodeChilds (oSelectedNode IN stb$treenode$record, olchildnodes OUT stb$treenodelist) RETURN STB$OERROR$RECORD;
--******************************************************************************
function getNodeList (oSelectedNode in stb$treenode$record, olNodeList out stb$treenodelist) return stb$oerror$record;  
--******************************************************************************
FUNCTION setCurrentNode (oSelectedNode IN stb$treenode$record) RETURN stb$oerror$record;  
--******************************************************************************
function setMenuAccess( sparameter   in varchar2, sMenuAllowed out varchar2 ) return stb$oerror$record;
--******************************************************************************
FUNCTION getContextMenu( nMenu           in  number, olMenuEntryList out stb$menuentry$list) RETURN stb$oerror$record;
--******************************************************************************
function callFunction( oParameter IN STB$MENUENTRY$RECORD, olNodeList OUT STB$TREENODELIST  ) return stb$oerror$record;
--******************************************************************************
FUNCTION putParameters( oParameter  IN STB$MENUENTRY$RECORD, olParameter IN STB$PROPERTY$LIST ) RETURN stb$oerror$record;
--******************************************************************************
FUNCTION getLov (sEntity IN VARCHAR2, ssuchwert IN VARCHAR2, oSelectionList OUT isr$tlrselection$list)  RETURN stb$oerror$record;
--******************************************************************************
function getDocumentType(doctype in varchar2) return varchar2;
--******************************************************************************
function getDefaultAction(doctype in varchar2) return varchar2;
--******************************************************************************
function getIconName(doctype in varchar2, sJobFlag in varchar2, sdoc_definition in varchar2, sCheckedout in varchar2) return varchar2;

function isWordReportExists(cnRepId in number) return varchar2;

function isWordDocumentExists(cnRepId in number) return varchar2;

END isr$excel$reporttype; 