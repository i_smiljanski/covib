CREATE OR REPLACE PACKAGE isr$excel$action AS

FUNCTION createParametersExcelmodule(cnJobid in NUMBER, cxXml IN OUT XMLTYPE) RETURN STB$OERROR$RECORD;
--***********************************************************************************************************************
FUNCTION createParametersWordmodule(cnJobid in NUMBER, cxXml IN OUT XMLTYPE) RETURN STB$OERROR$RECORD;
--***********************************************************************************************************************
FUNCTION copyReportFiles(cnJobid IN NUMBER, cxXml IN OUT XMLTYPE) RETURN STB$OERROR$RECORD;
--***********************************************************************
function copyWordDocumentIntoDoctrail( cnJobid in     number, cxXml   in out xmltype)   return STB$OERROR$RECORD;
--***********************************************************************************************************************
function loadFilesFromClient( cnJobid in     number, cxXml   in out xmltype)  return STB$OERROR$RECORD;
--***********************************************************************************************************************
function saveConfigForRunReport( cnJobid in     number, cxXml   in out xmltype) return STB$OERROR$RECORD;
--*********************************************************************************
function createParametersCheckout( cnJobid in     number, cxXml   in out xmltype) return STB$OERROR$RECORD;  
--***********************************************************************************************************************
function saveConfigForCheckout( cnJobid in     number, cxXml   in out xmltype) return STB$OERROR$RECORD;
--***********************************************************************************************************************
function copyDocumentsIntoDocForFinal( cnJobid in     number, cxXml   in out xmltype) return STB$OERROR$RECORD;
--**********************************************************************************************************************
function createParametersCheckin( cnJobid in number,  cxXml   in out xmltype )return STB$OERROR$RECORD;
--***********************************************************************************************************************
function createParametersFinal( cnJobid in     number, cxXml   in out xmltype) return STB$OERROR$RECORD;
--***********************************************************************************************************************
function updateDoctrailAfterCheckin( cnJobid in     number, cxXml   in out xmltype) return STB$OERROR$RECORD;
--***********************************************************************************************************************
function copyXMLToJob( cnJobid in number, cxXml   in out xmltype) return STB$OERROR$RECORD;
--****************************************************************************************************************************
function copyDocumentsIntoJobForFinal( cnJobid in     number, cxXml   in out xmltype)  return STB$OERROR$RECORD;
--************************************************************************************************************************************
FUNCTION createDocumentName(cnRepid IN NUMBER, cnDocid IN NUMBER) RETURN VARCHAR2;
--***********************************************************************************************************************
FUNCTION copyTemplateIntoJobtrail(cnJobid in number, cxXml IN OUT xmltype) return STB$OERROR$RECORD;
--==**********************************************************************************************************************************************==--
FUNCTION adjustRawDatFileForWord(cnJobId IN NUMBER, cxXml IN OUT XMLTYPE) RETURN STB$OERROR$RECORD; 
 --***********************************************************************************************************************
function checkWordDocOid( cnJobid in     number,cxXml   in out xmltype) return STB$OERROR$RECORD;
--****************************************************************************************************************************
FUNCTION copyUnprotectDocIntoJobtrail(cnJobid IN NUMBER, cxXml IN OUT XMLTYPE) RETURN STB$OERROR$RECORD;
--****************************************************************************************************************************
function copyStartFilesIntoJobtrail( cnJobid in     number, cxXml   in out xmltype) return STB$OERROR$RECORD;
END isr$excel$action;