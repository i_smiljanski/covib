CREATE OR REPLACE PACKAGE BODY isr$excel$action AS

  sIP                    STB$JOBSESSION.IP%type;
  sPort                  STB$JOBSESSION.PORT%type;
  sContext               varchar2(500) := STB$UTIL.getSystemParameter('LOADER_CONTEXT');
  sNameSpace            varchar2(500) := STB$UTIL.getSystemParameter('LOADER_NAMESPACE');
  sTimeout               varchar2(10) := STB$UTIL.getSystemParameter('TIMEOUT_LOADER_THREAD');

  
  oFileTypeList  constant   isr$varchar$list := isr$varchar$list('GRAPHIC','TRANSFORMATION', 'ATTACHMENTHTML') ;
  csDocPropOid          varchar2(100) :='DOC_OID';


--***********************************************************************************************************************
procedure markDBJobAsBroken(cnJobId in number, oErrorObj in STB$OERROR$RECORD) is

begin
  if sIp is null or sPort is null then
    SELECT ip, to_char(port) INTO sIp, sPort FROM STB$JOBSESSION WHERE jobid = cnJobId;
  end if;
  Stb$job.markDBJobAsBroken(cnJobId, sIp, sPort, oErrorObj.sErrorCode, oErrorObj.sErrorMessage, oErrorObj.sSeverityCode);
end;

--***********************************************************************************************************************************************
FUNCTION insertIntoDoctrail( cnRepId IN NUMBER,
                    csDocType IN VARCHAR2,
                    csFileName IN VARCHAR2,
                    csMimeType IN VARCHAR2,
                    cnUserId IN NUMBER,
                    cnDocId OUT NUMBER,
                    cnReporttypeId IN NUMBER,
                    csDocDefinition IN varchar2) RETURN STB$OERROR$RECORD
IS
  sCurrentName    constant varchar2(4000) := $$PLSQL_UNIT||'.insertIntoDoctrail('||cnRepId||')';
  oError                   STB$OERROR$RECORD := STB$OERROR$RECORD();
  
BEGIN
  isr$trace.stat('begin', 'begin', sCurrentName);
  isr$trace.debug('parameters','cnRepId ' || cnRepId || ' csDocType ' || csDocType  || ' docdefinition ' || csDocDefinition, 50);

  SELECT STB$DOC$SEQ.NEXTVAL INTO cnDocId FROM dual;
  isr$trace.debug('cnDocId', cnDocId, sCurrentName);

  INSERT INTO STB$DOCTRAIL (
      DOCID,
      CHECKEDOUT,
      NODEID,
      NODETYPE,
      DOC_OID,
      FILENAME,
      DOCTYPE,
      MIMETYPE,
      MODIFIEDON,
      MODIFIEDBY,
      DOCVERSION,
      VERSIONDISPLAYED,
      DOC_DEFINITION)
  VALUES ( cnDocId,
           'F',
           cnRepId,
           'REPORT',
           STB$UTIL.getSystemParameter('SYSTEM_OID') || '.' ||cnrepid || '.' || cnDocId,
           csFileName,
           csDocType,
           csMimeType,
           Sysdate,
           Stb$security.getOracleUserName(cnUserId),
           0,
           0,
           csDocDefinition );
  isr$trace.debug('INSERTED', 'INSERTED', sCurrentName);

  stb$docks.copyDocProperties(cnDocId, 0);

  isr$trace.stat('end', 'end', sCurrentName);
  RETURN oError;

EXCEPTION
WHEN OTHERS THEN   
    oError.handleError ( Stb$typedef.cnSeverityCritical, utd$msglib.getmsg ('exUnexpectedError', stb$security.getCurrentLanguage), sCurrentName, 
                           dbms_utility.format_error_stack || CHR(10) || dbms_utility.format_error_backtrace); 
   
  RETURN oError;
END  insertIntoDoctrail;

--************************************************************************************************************************************
FUNCTION createNameForParameter(cnRepid IN NUMBER, cnDocid IN NUMBER, csNameParameter in varchar2) RETURN VARCHAR2
is
  sCurrentName   constant varchar2(4000) := $$PLSQL_UNIT||'.createNameForParameter('|| csNameParameter ||')';
  sDocumentName  VARCHAR2(4000);
  nReporttypeId  NUMBER;
  sSRNumber      STB$REPORT.SRNUMBER%TYPE;
  sTitle         STB$REPORT.TITLE%TYPE;
  oErrorObj      STB$OERROR$RECORD := STB$OERROR$RECORD();

  CURSOR cGetReporttypeId(nRepid IN NUMBER) IS
    select reporttypeid, srnumber, title
      from stb$report
     where repid = nRepid;
     
  CURSOR cGetCrit (nRepID number, csEntity varchar2) IS
    SELECT entity, rtrim(xmlagg(xmlelement(e,display,'_').extract('//text()') order by ordernumber).GetStringVal(),'_') display
    FROM ISR$CRIT
    WHERE repid = nRepID -- STB$OBJECT.getCurrentRepid - CurrentRepID is not always available - ARDIS-435
    and entity=csEntity
    group by entity;

  CURSOR cGetFirstCrit (nRepID number, csEntity varchar2) IS
    SELECT entity, display
    FROM ISR$CRIT
    WHERE repid = nRepID
    and entity=csEntity
    and rownum = 1;

  CURSOR cGetFirstCritInfo (nRepID number, csEntity varchar2) IS
    SELECT entity, info
    FROM ISR$CRIT
    WHERE repid = nRepID
    and entity=csEntity
    and rownum = 1;

  CURSOR cGetCritInfo (nRepID number, csEntity varchar2) IS
    SELECT entity, rtrim(xmlagg(xmlelement(e,info,'_').extract('//text()') order by ordernumber).GetStringVal(),'_') info
    FROM ISR$CRIT
    WHERE repid = nRepID -- STB$OBJECT.getCurrentRepid - CurrentRepID is not always available - ARDIS-435
    and entity=csEntity
    group by entity;
  
  CURSOR cGetReportParameter (nRepID number)  IS
    SELECT parametername, parametervalue
    FROM stb$reportparameter
    WHERE repid = nRepID; -- STB$OBJECT.getCurrentRepid - CurrentRepID is not always available - ARDIS-435

  CURSOR cGetCritEntities (nRepID number) IS
    SELECT distinct entity
    FROM   ISR$CRIT
    WHERE  repid = nRepID;

  csFileSigns           CONSTANT VARCHAR2(4000) := '_*\/:?"<>|';
BEGIN
  isr$trace.stat('begin', 'begin', sCurrentName);

  OPEN cGetReporttypeId(cnRepid);
  FETCH cGetReporttypeId INTO nReporttypeId, sSRNumber, sTitle;
  CLOSE cGetReporttypeId;

  sDocumentName := STB$UTIL.getReptypeParameter(nReporttypeId, csNameParameter);

  sDocumentName := REPLACE(sDocumentName,'%REPID%',cnRepid);
  sDocumentName := REPLACE(sDocumentName,'%DOCID%',cnDocid);
  sDocumentName := REPLACE(sDocumentName,'%SRNUMBER%',REPLACE(sSRNumber, '''', ''''''));
  sDocumentName := REPLACE(sDocumentName,'%TITLE%',REPLACE(sTitle, '''', ''''''));
  sDocumentName := REPLACE(sDocumentName,'%DATE%',to_char(sysdate, STB$UTIL.getSystemParameter('DATEFORMAT')));

 -- replace identifieres in "Template" (like %SB$PRODUCT%) only if the entity is referenced in that template, ABBIS-256
  FOR rEntities in cGetCritEntities(cnRepid) LOOP
    if INSTR( sDocumentName, rEntities.entity) > 0 THEN

      FOR rGetCrit IN cGetFirstCrit(cnRepid, rEntities.entity) LOOP
        sDocumentName := REPLACE(sDocumentName, '%FIRST_'||rGetCrit.entity||'%', REPLACE(rGetCrit.display, '''', ''''''));
      END LOOP;

      FOR rGetCrit IN cGetFirstCritInfo(cnRepid, rEntities.entity) LOOP
        sDocumentName := REPLACE(sDocumentName, '%FIRST_'||rGetCrit.entity||'%', REPLACE(rGetCrit.info, '''', ''''''));
      END LOOP;


      FOR rGetCrit IN cGetCrit(cnRepid, rEntities.entity) LOOP
        sDocumentName := REPLACE(sDocumentName, '%'||rGetCrit.entity||'%', REPLACE(rGetCrit.display, '''', ''''''));

      END LOOP;

      FOR rGetCrit IN cGetCritInfo(cnRepid, rEntities.entity) LOOP
        sDocumentName := REPLACE(sDocumentName, '%'||rGetCrit.entity||'_INFO%', REPLACE(rGetCrit.info, '''', ''''''));
      END LOOP;

    END IF;
  END LOOP;

  FOR rGetReportParameter IN cGetReportParameter(cnRepid) LOOP
    sDocumentName := REPLACE(sDocumentName, '%'||rGetReportParameter.parametername||'%', rGetReportParameter.parametervalue);
  END LOOP;
  isr$trace.debug('sDocumentName', sDocumentName, sCurrentName);

  BEGIN
    execute immediate 'select '||sDocumentName||' from dual' into sDocumentName;
  EXCEPTION WHEN OTHERS THEN
    NULL;
  END;

  sDocumentName := Translate(sDocumentName, csFileSigns, '-');

  isr$trace.stat('end', 'end', sCurrentName);
  RETURN NVL(sDocumentName,'NO_VALUE');
EXCEPTION
  WHEN OTHERS THEN
      oErrorObj.handleError ( Stb$typedef.cnSeverityCritical, utd$msglib.getmsg ('exUnexpectedError', stb$security.getCurrentlanguage,  csP1 => sqlerrm(sqlcode)), 
        sCurrentName, dbms_utility.format_error_stack ||chr(10)|| dbms_utility.format_error_backtrace );
    RETURN 'VALUE_ERR';
END createNameForParameter;

--***********************************************************************************************************************
FUNCTION createParametersExcelmodule(cnJobid in NUMBER, cxXml IN OUT XMLTYPE) RETURN STB$OERROR$RECORD
is
  sCurrentName   constant varchar2(100) := $$PLSQL_UNIT||'.createParametersExcelmodule (JobID='||cnJobId||')';
  sUserMsg                varchar2(4000);
  oErrorObj               STB$OERROR$RECORD :=STB$OERROR$RECORD();
  exNoRepFiles            exception;
  
  sActionType             isr$ACTION.TOKEN%type;
  nWorksheetNr            number := 0;
  nOrderNr                number := 0;
  sIsFileSeparately       varchar2(1) := nvl(STB$UTIL.GetReportParameterValue('SEPARATE_FILE_PER_EXPERIMENT', STB$JOB.getJobParameter('REPID',cnJobid)), 'F');
  sCreateTOC      varchar2(1) := nvl(STB$UTIL.GetReportParameterValue('CREATETOC', STB$JOB.getJobParameter('REPID',cnJobid)), 'F');
 
  csMasterTemplate      varchar2(100) :='MASTERTEMPLATE';
  csTemplate      varchar2(100) :='TEMPLATE';
  sFileName       varchar2(1000);
  
  cursor cgetTargetInfo is
    select doc_definition,
           MIMETYPE,
           blobflag,
           documenttype,
           REFERENCE,
           filename
    from   STB$JOBTRAIL
    where  jobid = cnJobId
    and    documenttype in (csMasterTemplate, csTemplate);
     
  rGetTargetInfo  cgetTargetInfo%rowtype;
    
  cursor cGetAction is
    select Nvl (STB$JOB.getJobParameter ('COMMAND', cnJobId), token) token
      from isr$ACTION
     where actionid = STB$JOB.getJobParameter ('ACTIONID', cnJobId);
  
  cursor cGetProperties is
    select textfile
      from stb$jobtrail
     where jobid = cnJobid
       and documenttype = 'PROPERTIES';
  clProperties            clob;
  
  cursor cGetRepFiles is
    select filename, textfile
      from isr$report$file 
     where repid = STB$JOB.getJobParameter ('REPID', cnJobId) and filename like '%.html'
     order by filename;
  bCursorFound boolean := FALSE;
  
  sTitle VARCHAR2(30);
  sTitleCount VARCHAR(30);

BEGIN
  isr$trace.stat('begin', 'sIsFileSeparately '||sIsFileSeparately, sCurrentName);
  isr$trace.debug('status cxXml', 'cxXml: see column LOGCLOB', sCurrentName, cxXml);
 
  isr$XML.CreateNode(cxXML, '/config/apps', 'app', '');
  isr$XML.SetAttribute(cxXML, '/config/apps/app', 'order', '1');
  isr$XML.SetAttribute(cxXML, '/config/apps/app[@order=1]', 'name', 'excelmodule');
  isr$XML.CreateNode(cxXML, '/config/apps/app[@order=1]', 'executable', 'excelmodul.exe');
  isr$XML.CreateNode(cxXML, '/config/apps/app[@order=1]', 'wait_for_process', 'true');
  isr$XML.CreateNode(cxXML, '/config/apps/app[@order=1]', 'parameter', '[config]');  
  
  open  cGetAction;
  fetch cGetAction into sActionType;
  close cGetAction;
  
  open cGetTargetInfo;
  fetch cGetTargetInfo into rGetTargetInfo;
  
  open cGetProperties;
  fetch cGetProperties into clProperties;
  
  isr$XML.CreateNode(cxXML, '/config/apps/app[@order=1]', 'command', sActionType);

  isr$XML.CreateNode(cxXML, '/config/apps/app[@name="excelmodule"]', 'workbooks', ''); 
  if sIsFileSeparately = 'F' then
    execute immediate 'select '||$$PLSQL_UNIT||'.createDocumentName(STB$JOB.getJobParameter(''REPID'', '||cnJobId||'), STB$JOB.getJobParameter(''DOCID'', '||cnJobId||')) from dual'
    into sFileName;
    isr$XML.CreateNode(cxXML, '/config/apps/app[@name="excelmodule"]/workbooks', 'workbook', '');
    isr$XML.SetAttribute(cxXML, '/config/apps/app[@name="excelmodule"]/workbooks/workbook', 'filename', sFilename || '.xlsx');
  end if;
      
  for rGetFiles in cGetRepFiles loop
  
    if sIsFileSeparately = 'T' then
      nOrderNr := nOrderNr +1;
      nWorksheetNr := 1; 
      isr$XML.CreateNode(cxXML, '/config/apps/app[@name="excelmodule"]/workbooks', 'workbook', '');
      isr$XML.SetAttribute(cxXML, '/config/apps/app[@name="excelmodule"]/workbooks/workbook[position()=last()]', 'filename', replace(rGetFiles.filename, '.html', '.xlsx'));
    else
      nOrderNr := 1;
      nWorksheetNr := nWorksheetNr + 1;
    end if;
    
    isr$XML.CreateNode(cxXML, '/config/apps/app[@name="excelmodule"]/workbooks/workbook[position()=last()]', 'worksheet', rGetFiles.filename);
    
    -- Extracts the title for Excel Sheet
    sTitle := NULL;
    if regexp_like(dbms_lob.substr(rGetFiles.textfile, 800, 1), '<title>.*</title>') then
      -- Extract the title, remove special characters not supported in Excel Tab names, convert excaped charcaters to plan text and shorten to 30 characters
      sTitle := substr(dbms_xmlgen.Convert(translate(regexp_substr(dbms_lob.substr(rGetFiles.textfile, 800, 1),'<title>(.*)</title>', 1, 1, 'i', 1), ':*/\[]?''', '        '), 1),1, 30);
      isr$XML.SetAttribute(cxXML, '/config/apps/app[@name="excelmodule"]/workbooks/workbook[position()=last()]/worksheet[position()=last()]', 'actualname', nvl(sTitle,to_char(nWorksheetNr, '000')));
      -- Check if the same title is already used for anotehr file and add a counter if true
      sTitleCount := isr$XML.ValueOf(cxXML, 'count(/config/apps/app[@name="excelmodule"]/workbooks/workbook[position()=last()]/worksheet[@actualname = ../worksheet[position()=last()]/@actualname])');
      if sTitleCount != '1' then
        sTitle := substr(sTitle, 1, 28 - length(sTitleCount)) || '(' || sTitleCount || ')';
      end if;
    end if;
    
    isr$XML.SetAttribute(cxXML, '/config/apps/app[@name="excelmodule"]/workbooks/workbook[position()=last()]/worksheet[position()=last()]', 'name', nvl(sTitle,to_char(nWorksheetNr, '000')));
    isr$XML.SetAttribute(cxXML, '/config/apps/app[@name="excelmodule"]/workbooks/workbook[position()=last()]/worksheet[position()=last()]', 'description', isr$util.getSplit(rGetFiles.filename, 1, '.'));
    
    isr$XML.SetAttribute(cxXML, '/config/apps/app[@name="excelmodule"]/workbooks/workbook[position()=last()]', 'order', nOrderNr);
    isr$XML.SetAttribute(cxXML, '/config/apps/app[@name="excelmodule"]/workbooks/workbook[position()=last()]', 'targettype', rgetTargetInfo.blobflag);
    isr$XML.SetAttribute(cxXML, '/config/apps/app[@name="excelmodule"]/workbooks/workbook[position()=last()]', 'mimetype', rgetTargetInfo.mimetype);
    isr$XML.SetAttribute(cxXML, '/config/apps/app[@name="excelmodule"]/workbooks/workbook[position()=last()]', 'doc_definition', rgetTargetInfo.doc_definition);
    isr$XML.SetAttribute(cxXML, '/config/apps/app[@name="excelmodule"]/workbooks/workbook[position()=last()]', 'isrdoctype', 'D');
    
    if sCreateTOC = 'F' then
      isr$XML.SetAttribute(cxXML, '/config/apps/app[@name="excelmodule"]/workbooks/workbook[position()=last()]', 'createtoc', 'false');
    else
      isr$XML.SetAttribute(cxXML, '/config/apps/app[@name="excelmodule"]/workbooks/workbook[position()=last()]', 'createtoc', 'true');
    end if;
    
    bCursorFound := TRUE;
  end  loop;
  
  if STB$JOB.getJobParameter('FINALFLAG', cnJobid) is null then       
     if cGetProperties%found then
        isr$XML.InsertChildXMLFragment(cxXML, '/config/apps/app[@name="excelmodule"]/workbooks/workbook[position()=last()]', clProperties);
     end if;
  end if;
    
  if not bCursorFound then
    raise exNoRepFiles;
  end if;

 
  close cGetProperties;
  close cGetTargetInfo;

  -- number OF APPS
  isr$XML.SetAttribute(cxXML, '/config/apps', 'num', isr$XML.valueOf(cxXML, 'count(/config/apps/app)'));

  isr$trace.debug('final status cxXml', 'cxXml: see column LOGCLOB', sCurrentName, cxXml);
  isr$trace.stat('end', 'Function '||sCurrentName||' finished',sCurrentName);
  return oErrorObj;

exception
  when exNoRepFiles then
    sUserMsg := utd$msglib.getmsg ('exNoRepFilesError', stb$security.getCurrentLanguage);   
    oErrorObj.handleError ( Stb$typedef.cnSeverityCritical, sUserMsg, sCurrentName, 
          dbms_utility.format_error_stack ||chr(10)|| dbms_utility.format_error_backtrace );
    markDBJobAsBroken(cnJobId, oErrorObj);
    return oErrorObj;
  when others then
    sUserMsg := utd$msglib.getmsg ('exUnexpectedError', stb$security.getCurrentLanguage,  csP1 => sqlerrm(sqlcode));   
    oErrorObj.handleError ( Stb$typedef.cnSeverityCritical, sUserMsg, sCurrentName, 
          dbms_utility.format_error_stack ||chr(10)|| dbms_utility.format_error_backtrace );
    markDBJobAsBroken(cnJobId, oErrorObj);
    return oErrorObj;
 end createParametersExcelmodule;


--************************************************************************************************************************************
FUNCTION createDocumentName(cnRepid IN NUMBER, cnDocid IN NUMBER) RETURN VARCHAR2
is
  sCurrentName   constant varchar2(4000) := $$PLSQL_UNIT||'.createDocumentName('||' '||')';
  sDocumentName   VARCHAR2(4000);
     
BEGIN
  isr$trace.stat('begin', 'begin', sCurrentName);
  
  IF STB$JOB.GetJobParameter('NODETYPE', STB$JOB.GetJobID) = 'REPORT_WORD' THEN 
    sDocumentName := createNameForParameter(cnRepid, cnDocid, 'WORD_DOCUMENT_NAME');
  ELSE
    sDocumentName := createNameForParameter(cnRepid, cnDocid, 'DOCUMENT_NAME');
  END IF;

  isr$trace.stat('end', sDocumentName, sCurrentName);
  RETURN sDocumentName;

END createDocumentName;  
 
--***********************************************************************************************************************
FUNCTION createParametersWordmodule(cnJobid in NUMBER, cxXml IN OUT XMLTYPE) RETURN STB$OERROR$RECORD
is
  sCurrentName   constant varchar2(100) := $$PLSQL_UNIT||'.createParametersWordmodule (JobID='||cnJobId||')';
  sUserMsg                varchar2(4000);
  oErrorObj               STB$OERROR$RECORD :=STB$OERROR$RECORD();
  exNoRepFiles            exception;
  
  sPassword  STB$REPORT.PASSWORD%type;
  --sActionType             isr$ACTION.TOKEN%type;
  sIsFileSeparately       varchar2(1) := nvl(STB$UTIL.GetReportParameterValue('SEPARATE_FILE_PER_EXPERIMENT', STB$JOB.getJobParameter('REPID',cnJobid)), 'F');
  
  coMastertemplateList  isr$varchar$list;
  sFileName               varchar2(500);
  sFileExtension          varchar2(16);
  
  cursor cGetProtected(cnRepid in STB$REPORT.REPID%type)  is
    select case when STB$UTIL.GetReportParameterValue('W_PROTECTED', cnRepid) = 'T' then null else 'T' end
    from   dual;

  sUnProtected varchar2(1);
  
  cursor cGetAction is
    select Nvl (STB$JOB.getJobParameter ('COMMAND', cnJobId), token) token
    from   isr$ACTION
    where  actionid = STB$JOB.getJobParameter ('ACTIONID', cnJobId);
  

BEGIN
  isr$trace.stat('begin', 'sIsFileSeparately '||sIsFileSeparately, sCurrentName);
  isr$trace.debug('status cxXml', 'cxXml: see column LOGCLOB', sCurrentName, cxXml);
 
  if STB$JOB.getJobParameter ('FILTER_MASTERTEMPLATE_WORD', cnJobId) = 'PREV_WORD_DOC' then
    coMastertemplateList := isr$varchar$list('TEMPLATE');
  else
    coMastertemplateList := isr$varchar$list('MASTERTEMPLATE_WORD');
  end if;
  oErrorObj := stb$action.createParamsForNewDoc(cnJobId, cxXml, 'CAN_RUN_REPORT', coMastertemplateList, sPassword);
  
  --target file
  execute immediate 'select '||$$PLSQL_UNIT||'.createDocumentName(STB$JOB.getJobParameter(''REPID'', '||cnJobId||'), STB$JOB.getJobParameter(''DOCID'', '||cnJobId||')) from dual'
  into sFileName;

  sFileExtension := isr$util.getSplit(isr$XML.valueOf(cxXML, '/config/apps/app[@name="wordmodul"]/target/text()'),2,'.');
  isr$XML.updateNode(cxXML, '/config/apps/app[@name="wordmodul"]/target', sFileName || '.' || sFileExtension);
  
  isr$XML.updateNode(cxXML, '/config/apps/app[@name="wordmodul"]/isrdoctype', 'W');
 
  open cGetProtected(STB$JOB.getJobParameter('REPID',cnJobid));
  fetch cGetProtected into sUnProtected;
  close cGetProtected;
  isr$trace.debug('status sUnProtected', 'sUnProtected: '||sUnProtected, sCurrentName);

  if TRIM(sUnProtected) is not null then
    isr$XML.CreateNode(cxXML, '/config/apps/app[@name="wordmodul"]', 'mode', sUnProtected);
  end if;
  
  if isr$XML.valueOf(cxXML, 'count(/config/apps/app[@name="pdfmodul"]/target)') > 0 then
    isr$XML.updateNode(cxXML, '/config/apps/app[@name="pdfmodul"]/source', sFileName || '.' || sFileExtension);
    isr$XML.updateNode(cxXML, '/config/apps/app[@name="pdfmodul"]/target', sFileName || '.pdf');
  end if;
  
  -- number OF APPS
  isr$XML.SetAttribute(cxXML, '/config/apps', 'num', isr$XML.valueOf(cxXML, 'count(/config/apps/app)'));
  
  isr$trace.debug('final status cxXml', 'cxXml: see column LOGCLOB', sCurrentName, cxXml);
  isr$trace.stat('end', 'Function '||sCurrentName||' finished',sCurrentName);
  return oErrorObj;

exception
  when exNoRepFiles then
    sUserMsg := utd$msglib.getmsg ('exNoRepFilesError', stb$security.getCurrentLanguage);   
    oErrorObj.handleError ( Stb$typedef.cnSeverityCritical, sUserMsg, sCurrentName, 
          dbms_utility.format_error_stack ||chr(10)|| dbms_utility.format_error_backtrace );
    markDBJobAsBroken(cnJobId, oErrorObj);
    return oErrorObj;
  when others then
    sUserMsg := utd$msglib.getmsg ('exUnexpectedError', stb$security.getCurrentLanguage,  csP1 => sqlerrm(sqlcode));   
    oErrorObj.handleError ( Stb$typedef.cnSeverityCritical, sUserMsg, sCurrentName, 
          dbms_utility.format_error_stack ||chr(10)|| dbms_utility.format_error_backtrace );
    markDBJobAsBroken(cnJobId, oErrorObj);
    return oErrorObj;
 end createParametersWordmodule;
  
 --***********************************************************************************************************************
function createParametersFinal( cnJobid in     number,
                                     cxXml   in out xmltype)
  return STB$OERROR$RECORD
is
  sCurrentName   constant varchar2(100) := $$PLSQL_UNIT||'.createParametersFinal (JobID='||cnJobId||')';
  sUserMsg                varchar2(4000);
  sDevMsg                 varchar2(4000);
  sImplMsg                varchar2(4000);
  oErrorObj               STB$OERROR$RECORD :=STB$OERROR$RECORD();
  sPassword               STB$REPORT.PASSWORD%type;
  sRemoveEmpty            varchar2(1);
  sDisableClear           varchar2(1);
  sActionType             isr$ACTION.TOKEN%type;
  nAppId       number;
  sDocOid                 STB$DOCTRAIL.DOC_OID%type;
  
  cursor cGetProtected(cnRepid in STB$REPORT.REPID%type)  is
    select case when STB$UTIL.GetReportParameterValue('PROTECTED', cnRepid) = 'T' then null else 'T' end
    from   dual;

  sUnProtected varchar2(1);
  nFileID      number;

  cursor cGetAction is
    select Nvl (STB$JOB.getJobParameter ('COMMAND', cnJobId), token) token
    from   isr$ACTION
    where  actionid = STB$JOB.getJobParameter ('ACTIONID', cnJobId);

  coMastertemplateList  constant   isr$varchar$list := isr$varchar$list('WORD_TEMPLATE') ;
  
  cursor cGetExcelInfo is
    select doc_definition,
           MIMETYPE,
           blobflag,
           documenttype,
           REFERENCE,
           filename
    from   STB$JOBTRAIL
    where  jobid = cnJobId
    and    documenttype = 'EXCEL_TEMPLATE';
    
  rGetExcelInfo cGetExcelInfo%rowtype;  
  
  cursor cGetDocOid(sReference in STB$JOBTRAIL.reference%type) is
   select doc_oid
      from  STB$DOCTRAIL 
     where docid = sReference;
        

BEGIN
  isr$trace.stat('begin', 'Function '||sCurrentName||' started.', sCurrentName);
  isr$trace.debug('status cxXml', 'cxXml: see column LOGCLOB', sCurrentName, cxXml);

  -- entspricht CAN_RUN_REPORT or OFFSHOOT
  if STB$JOB.getJobParameter('DOCID', cnJobId) is null then

    open  cGetAction;
    fetch cGetAction into sActionType;
    close cGetAction;

    oErrorObj := stb$action.createParamsForNewDoc(cnJobId, cxXml, sActionType, coMastertemplateList, sPassword);

 end if;

  -- MCD, 08.Jan 2006
  -- PROTECTED or UNPROTECTED
  open cGetProtected(STB$JOB.getJobParameter('REPID',cnJobid));
  fetch cGetProtected into sUnProtected;
  close cGetProtected;
  isr$trace.debug('status sUnProtected', 'sUnProtected: '||sUnProtected, sCurrentName);

  if TRIM(sUnProtected) is not null then
    isr$XML.CreateNode(cxXML, '/config/apps/app[@name="wordmodul"]', 'mode', sUnProtected);
  end if;

  execute immediate
      'BEGIN
        :1 := ' || STB$UTIL.getCurrentCustomPackage || '.ckeckRemoveEmptyBookmarks(:2, :3);
      end;'
    using out oErrorObj, in cnJobid, out sRemoveEmpty;

  isr$XML.CreateNode(cxXML, '/config/apps/app[@name="wordmodul"]', 'removeNonReferencedBookmarks', sRemoveEmpty);

  sDisableClear := STB$UTIL.getReptypeParameter(STB$JOB.getJobParameter('REPORTTYPEID',cnJobId), 'DEACTIVATE_CLEAR_STATIC_BOOKMARKS');
  isr$XML.CreateNode(cxXML, '/config/apps/app[@name="wordmodul"]', 'deactivateClearStaticBookmarks', sDisableClear);

  isr$XML.CreateNode(cxXML, '/config/apps/app[@name="wordmodul"]', 'isr-bookmarks', '');
  -- MS 2014.12.17 - Exclude "Create once"-Bookmarks from this list. Otherwise these would be removed during "Remove Empty Bookmarks"
  
  for rGetAllBookmarks in ( select distinct display, fpv.ordernumber
                              from isr$file$parameter$values fpv,
                                   isr$output$transform ot
                             where fpv.fileid  = STB$JOB.getJobParameter ('MASTERTEMPLATE', cnJobId)
                               and fpv.entity  = 'STYLESHEET$BOOKMARK'
                               and fpv.info    = ot.stylesheetid
                               and ot.recreate = 'T'
                             order by fpv.ordernumber ) loop
    isr$XML.CreateNode(cxXML, '/config/apps/app[@name="wordmodul"]/isr-bookmarks', 'bookmark', rGetAllBookmarks.display);
  end loop;
  
  isr$XML.updateNode(cxXML, '/config/apps/app[position()=last()]/isrdoctype', 'W');

  
  -- check doc_oid 
  open cGetExcelInfo;
  fetch cGetExcelInfo into rGetExcelInfo;
  close cGetExcelInfo; 
  
  nAppId := isr$XML.valueOf(cxXML, 'count(/config/apps/app)') + 1;
  isr$trace.debug('nAppId', nAppId, sCurrentName);
  isr$XML.CreateNodeAttr(cxXML,  '/config/apps', 'app', '', 'order', nAppId);
  isr$XML.SetAttribute(cxXML, '/config/apps/app[@order='||nAppId||']', 'name', 'ME');
  isr$XML.CreateNode(cxXML, '/config/apps/app[@order='||nAppId||']', 'command', 'CHECK_EXCEL_OID');
  isr$XML.CreateNode(cxXML, '/config/apps/app[@order='||nAppId||']', 'targettype', rGetExcelInfo.blobflag);
  isr$XML.CreateNode(cxXML, '/config/apps/app[@order='||nAppId||']', 'mimetype', rGetExcelInfo.mimetype);
  isr$XML.CreateNode(cxXML, '/config/apps/app[@order='||nAppId||']', 'doc_definition', rGetExcelInfo.doc_definition);   
  isr$XML.CreateNode(cxXML, '/config/apps/app[@order='||nAppId||']', 'isrdoctype', 'D');
  isr$XML.CreateNode(cxXML, '/config/apps/app[@order='||nAppId||']', 'target', rGetExcelInfo.filename);
  
  open  cGetDocOid(rGetExcelInfo.reference);
  fetch cGetDocOid into sDocOid;
  close cGetDocOid;
  isr$XML.CreateNode(cxXML, '/config/apps/app[@order='||nAppId||']', csDocPropOid, sDocOid);
  
  -- number OF APPS
  isr$XML.SetAttribute(cxXML, '/config/apps', 'num', isr$XML.valueOf(cxXML, 'count(/config/apps/app)'));
  
  isr$trace.debug('final status cxXml', 'cxXml: see column LOGCLOB', sCurrentName, cxXml);
  isr$trace.stat('end', 'Function '||sCurrentName||' finished',sCurrentName);
  return oErrorObj;

exception
  when others then
    sUserMsg := utd$msglib.getmsg ('exUnexpectedError', stb$security.getCurrentLanguage,  csP1 => sqlerrm(sqlcode));
    sImplMsg := sCurrentName || rtrim(': '||sImplMsg,': ');
    sDevMsg  := dbms_utility.format_error_stack ||chr(10)|| dbms_utility.format_error_backtrace;
    oErrorObj.handleError ( Stb$typedef.cnSeverityCritical, sUserMsg, sImplMsg, sDevMsg );
    markDBJobAsBroken(cnJobId, oErrorObj);
    return oErrorObj;
end createParametersFinal;

 
 --***********************************************************************************************************************
function checkWordDocOid( cnJobid in     number,
                                     cxXml   in out xmltype)
  return STB$OERROR$RECORD
 is
  sCurrentName   constant varchar2(100) := $$PLSQL_UNIT||'.checkWordDocOid (JobID='||cnJobId||')';
  oErrorObj               STB$OERROR$RECORD :=STB$OERROR$RECORD();
  exWrongDoc              exception;
  exErrorInComp           exception;
  sMsg                varchar2(4000);
  sDocOid                 STB$DOCTRAIL.DOC_OID%type;
  sMatchDocs varchar2(4000);
    
  cursor cGetFiles is
    select jt.entryid,
           jt.filename,
           jt.reference,
           jt.binaryfile,
           d.doctype,
           d.nodeid
      from STB$JOBTRAIL jt, STB$DOCTRAIL d
     where jt.documenttype = 'WORD_TEMPLATE'
       and jt.jobid = cnJobId
       and d.docid(+) = jt.reference;
  cursor cGetDocOid(csReference in varchar2) is
    select doc_oid
      from stb$doctrail
     where docid = csReference;     
begin
  isr$trace.stat('begin', 'begin', sCurrentName); 
  if sIp is null or sPort is null then
    SELECT ip, to_char(port) INTO sIp, sPort FROM STB$JOBSESSION WHERE jobid = cnJobId;
  end if;
  for rGetFiles in cGetFiles loop
    open  cGetDocOid(rGetFiles.reference);
    fetch cGetDocOid into sDocOid;
    close cGetDocOid;
    isr$trace.debug('status sDocOid', 'sDocOid: '||sDocOid, sCurrentName);      
    sMatchDocs := ISR$LOADER.compareDocumentOID(sip, sport, sContext, sNamespace, 
            sDocOid, rGetFiles.binaryfile, to_number(stimeout));     
    case   
    when sMatchDocs = 'T' or rGetFiles.doctype is null then
      isr$trace.debug('sMatchDocs', sMatchDocs,sCurrentName);                              
    when sMatchDocs = 'F' then
      sMsg :=    'sDocOid: '||sDocOid||'; sMatchDocs: '||sMatchDocs;
      raise exWrongDoc;
    else
      sMsg := 'sDocOid: '||sDocOid;
      raise exErrorInComp;
    end case;
  end loop;
  isr$trace.stat('end', 'end',sCurrentName);
  return oErrorObj;

exception
  when exWrongDoc then
    oErrorObj.handleError ( Stb$typedef.cnSeverityCritical, utd$msglib.getmsg ('exWrongDocText', stb$security.getCurrentLanguage,  
        csP1 => sqlerrm(sqlcode), csP2 => sMsg), 
        sCurrentName, dbms_utility.format_error_stack || CHR(10) || dbms_utility.format_error_backtrace);
    markDBJobAsBroken(cnJobId, oErrorObj);
    return oErrorObj;
  when exErrorInComp then
    oErrorObj.handleError ( Stb$typedef.cnSeverityCritical, utd$msglib.getmsg ('exErrorInCompText', stb$security.getCurrentLanguage, 
        csP1 => sqlerrm(sqlcode), csP2 => sMsg), 
        sCurrentName, dbms_utility.format_error_stack || CHR(10) || dbms_utility.format_error_backtrace);
    markDBJobAsBroken(cnJobId, oErrorObj);
    return oErrorObj;
  when others then
    oErrorObj.handleError ( Stb$typedef.cnSeverityCritical, utd$msglib.getmsg ('exUnexpectedError', stb$security.getCurrentLanguage,  csP1 => sqlerrm(sqlcode)), 
        sCurrentName, dbms_utility.format_error_stack || CHR(10) || dbms_utility.format_error_backtrace);
    markDBJobAsBroken(cnJobId, oErrorObj);
    return oErrorObj;
end checkWordDocOid;  


 --***********************************************************************************************************************
function copyDocumentsIntoDocForFinal( cnJobid in     number,
                                     cxXml   in out xmltype)
  return STB$OERROR$RECORD
 is
  sCurrentName   constant varchar2(100) := $$PLSQL_UNIT||'.copyDocumentsIntoDocForFinal (JobID='||cnJobId||')';
  oErrorObj               STB$OERROR$RECORD :=STB$OERROR$RECORD();
 
  nRepId                  number := STB$JOB.getJobParameter('REPID',cnJobId);
 -- sNodeTyp                varchar2(100) := Nvl(STB$JOB.getJobParameter('NODETYPE',cnJobId),'REPORT');
  sNodeId                 varchar2(100) := Nvl(STB$JOB.getJobParameter('REPID',cnJobId),STB$JOB.getJobParameter('NODEID',cnJobId));
  nDocId  number;
  
  cursor cGetWordFile is
    select jt.entryid,d.docid,
           jt.filename,
           jt.reference,
           jt.binaryfile,
           d.doctype,
           d.nodeid,
           'REPORT_WORD' nodetype
      from STB$JOBTRAIL jt, STB$DOCTRAIL d
     where jt.documenttype = 'W' 
       and jt.jobid = cnJobId
       and d.doctype = 'W'
       and d.nodeid = jt.reference;
       
  cursor cGetExcelFile is     
   select documenttype,
           filename,
           reference,
           entryid,
           mimetype,
           doc_definition,
           'REPORT' nodetype
      from STB$JOBTRAIL jt
      where jt.documenttype = 'D' 
       and jt.jobid = cnJobId;
         
begin
  isr$trace.stat('begin', 'begin', sCurrentName); 
  for rGetFiles in cGetWordFile loop
         
    oErrorObj := Stb$docks.updateDoctrail( rGetFiles.docid,
                                          sNodeId,
                                          rGetFiles.entryid,
                                          rGetFiles.nodetype,
                                          null );
     
  end loop;
  for rGetFiles in cGetExcelFile loop
    oErrorObj := insertIntoDoctrail( nRepId,
                                            rGetFiles.documenttype,
                                            rGetFiles.fileName,
                                            rGetFiles.mimetype,
                                            STB$JOB.getJobParameter('USER',cnJobId),
                                            nDocId,
                                            STB$JOB.getJobParameter('REPORTTYPEID',cnJobid) ,
                                            rGetFiles.doc_definition); 
                                            
    if oErrorObj.isExceptionCritical then
      raise stb$typedef.exCritical;
    end if;                                        
    oErrorObj := Stb$docks.updateDoctrail( nDocId,
                                        nRepId,
                                        rGetFiles.entryid,
                                        rGetFiles.nodetype,
                                        STB$JOB.getJobParameter('COMMENT',cnJobId) );                                        
  end loop;
  isr$trace.stat('end', 'end',sCurrentName);
  return oErrorObj;

exception
 
  when others then
    oErrorObj.handleError ( Stb$typedef.cnSeverityCritical, utd$msglib.getmsg ('exUnexpectedError', stb$security.getCurrentLanguage,  csP1 => sqlerrm(sqlcode)), 
        sCurrentName, dbms_utility.format_error_stack || CHR(10) || dbms_utility.format_error_backtrace);
    markDBJobAsBroken(cnJobId, oErrorObj);
    return oErrorObj;
end copyDocumentsIntoDocForFinal;  

--**********************************************************************************************************************
function createParametersCheckin( cnJobid in number,
                                  cxXml   in out xmltype )
  return STB$OERROR$RECORD
is
  sCurrentName   constant varchar2(100) := $$PLSQL_UNIT||'.createParametersCheckin (JobID='||cnJobId||')';
  oErrorObj               STB$OERROR$RECORD :=STB$OERROR$RECORD();
  nAppId       number;
  sDocOid                 STB$DOCTRAIL.DOC_OID%type;
  cursor cGetDocOid is
    select doc_oid
    from stb$doctrail
    where docid = STB$JOB.getJobParameter('DOCID', cnJobId);

begin
  isr$trace.stat('begin', 'Function '||sCurrentName||' started.', sCurrentName);
 
  -- General parameters
  isr$XML.CreateNode(cxXML, '/config/apps', 'fullpathname', stb$action.getPath(STB$JOB.getJobParameter('TO', cnJobId)));

  nAppId := isr$XML.valueOf(cxXML, 'count(/config/apps/app)') + 1;
  isr$XML.CreateNodeAttr(cxXML,  '/config/apps', 'app', '', 'order', nAppId);
  isr$XML.SetAttribute(cxXML, '/config/apps/app[@order='||nAppId||']', 'name', 'ME');
  isr$XML.CreateNode(cxXML, '/config/apps/app[@order='||nAppId||']', 'command', 'CHECK_EXCEL_OID');
  isr$XML.CreateNode(cxXML, '/config/apps/app[@order='||nAppId||']', 'target', stb$action.getFileName(STB$JOB.getJobParameter('TO', cnJobId)));
  
  open  cGetDocOid;
  fetch cGetDocOid into sDocOid;
  close cGetDocOid;
  isr$XML.CreateNode(cxXML, '/config/apps/app[@order='||nAppId||']', csDocPropOid, sDocOid);

  isr$trace.debug('final status cxXml', 'cxXml: see column LOGCLOB', sCurrentName, cxXml);
  isr$trace.stat('end', 'Function '||sCurrentName||' finished',sCurrentName);
  return oErrorObj;

exception
  when others then
    oErrorObj.handleError ( Stb$typedef.cnSeverityCritical, utd$msglib.getmsg ('exUnexpectedError', stb$security.getCurrentLanguage,  csP1 => sqlerrm(sqlcode)), 
        sCurrentName, dbms_utility.format_error_stack || CHR(10) || dbms_utility.format_error_backtrace);
    markDBJobAsBroken(cnJobId, oErrorObj);
    return oErrorObj;
end createParametersCheckin;

--***********************************************************************************************************************
function createParametersCheckout( cnJobid in     number,
                                   cxXml   in out xmltype)
  return STB$OERROR$RECORD
is
  sCurrentName   constant varchar2(100) := $$PLSQL_UNIT||'.createParametersCheckout (JobID='||cnJobId||')';
  oErrorObj               STB$OERROR$RECORD :=STB$OERROR$RECORD();
  sPasswordTarget        STB$REPORT.PASSWORD%type;
  sPasswordSource        STB$REPORT.PASSWORD%type;
  sActionType            isr$ACTION.TOKEN%type;
  csTarget              varchar2(100) :='TARGET';

  cursor cGetLeadingDoc is
    select jt.filename,
           jt.leadingdoc,
           jt.REFERENCE,
           jt.documenttype,
           jt.mimetype,
           jt.DOC_DEFINITION
    from   STB$JOBTRAIL jt
    where  (   jt.documenttype = 'D'
            or jt.documenttype = csTarget)
    and    jt.jobid = cnJobid
    and    jt.leadingdoc = 'T';

    cursor cGetAction is
    select Nvl (STB$JOB.getJobParameter ('COMMAND', cnJobId), token) token
    from   isr$ACTION
    where  actionid = STB$JOB.getJobParameter ('ACTIONID', cnJobId);

  rGetLeadingDoc cGetLeadingDoc%ROWTYPE;

  nAppId number;

begin
  isr$trace.stat('begin', 'Function '||sCurrentName||' started.', sCurrentName);
  isr$trace.debug('status cxXml', 'cxXml: see column LOGCLOB', sCurrentName, cxXml);

  open  cGetLeadingDoc;
  fetch cGetLeadingDoc into rGetLeadingDoc;
  close cGetLeadingDoc;

  -- get passwords
  oErrorObj := Stb$docks.getPasswordFromDoc(STB$JOB.getJobParameter('OLDDOCID', cnJobId),sPasswordSource);
  isr$trace.debug('status sPasswordSource', 'sPasswordSource: '||sPasswordSource, sCurrentName);
  oErrorObj := Stb$docks.getPasswordFromDoc(STB$JOB.getJobParameter('REFERENCE', cnJobId),sPasswordTarget);
  isr$trace.debug('status sPasswordTarget', 'xxxsPasswordTargetxx: '||sPasswordTarget, sCurrentName);

  -- General parameters
  -- Append doc ending
  if stb$action.getExtension(STB$JOB.getJobParameter('TO', cnJobId)) is null then
    STB$JOB.setJobParameter('TO', STB$JOB.getJobParameter('TO', cnJobId)||'.'||stb$action.getExtension(rGetLeadingDoc.filename), cnJobId);
  end if;
  isr$XML.CreateNode(cxXML, '/config/apps', 'fullpathname', stb$action.getPath(STB$JOB.getJobParameter('TO', cnJobId)));
  isr$XML.CreateNode(cxXML, '/config/apps', 'target', stb$action.getFileName(STB$JOB.getJobParameter('TO', cnJobId)));
  isr$XML.CreateNode(cxXML, '/config/apps', 'extension', stb$action.getExtension(STB$JOB.getJobParameter('TO', cnJobId)));
  isr$XML.CreateNode(cxXML, '/config/apps', 'overwrite', STB$JOB.getJobParameter('OVERWRITE', cnJobId));

  
  -- ========= excelmodule
  -- =======================

  open cGetAction;
  fetch cGetAction into sActionType;
  close cGetAction;

  nAppId := isr$XML.valueOf(cxXML, 'count(/config/apps/app)') + 1;
  isr$XML.CreateNodeAttr(cxXML,  '/config/apps', 'app', '', 'order', nAppId);
  isr$XML.SetAttribute(cxXML, '/config/apps/app[@order='||nAppId||']', 'name', 'excelmodule');
  isr$XML.CreateNode(cxXML, '/config/apps/app[@order='||nAppId||']', 'wait_for_process', 'true');
  isr$XML.CreateNode(cxXML, '/config/apps/app[@order='||nAppId||']', 'executable', 'excelmodul.exe');
  isr$XML.CreateNode(cxXML, '/config/apps/app[@order='||nAppId||']', 'parameter', '[config]');
  isr$XML.CreateNode(cxXML, '/config/apps/app[@order='||nAppId||']', 'command', sActionType);
  isr$XML.CreateNode(cxXML, '/config/apps/app[@order='||nAppId||']', 'target', NVL(ISR$XML.valueOf(cxXML, '/config/apps/target/text()'), rGetLeadingDoc.filename));
  isr$XML.CreateNode(cxXML, '/config/apps/app[@order='||nAppId||']', 'targettype', 'B');
  isr$XML.CreateNode(cxXML, '/config/apps/app[@order='||nAppId||']', 'mimetype', rGetLeadingDoc.mimetype);
  isr$XML.CreateNode(cxXML, '/config/apps/app[@order='||nAppId||']', 'doc_definition', rGetLeadingDoc.doc_definition);   
  isr$XML.CreateNode(cxXML, '/config/apps/app[@order='||nAppId||']', 'isrdoctype', 'D');
  isr$XML.CreateNode(cxXML, '/config/apps/app[@order='||nAppId||']', 'sourcepassword', sPasswordSource);
  isr$XML.CreateNode(cxXML, '/config/apps/app[@order='||nAppId||']', 'targetpassword', sPasswordTarget);
  if TRIM(STB$JOB.getJobParameter('USE_PROTECTION', cnJobId)) is not null then
    isr$XML.CreateNode(cxXML, '/config/apps/app[@order='||nAppId||']', 'use-protection', STB$JOB.getJobParameter('USE_PROTECTION', cnJobId));
  end if;
  isr$XML.CreateNode(cxXML, '/config/apps/app[@order='||nAppId||']', 'workbooks', '');
  isr$XML.CreateNode(cxXML, '/config/apps/app[@order='||nAppId||']/workbooks', 'workbook', '');
  isr$XML.SetAttribute(cxXML, '/config/apps/app[@order='||nAppId||']/workbooks/workbook', 'filename', NVL(ISR$XML.valueOf(cxXML, '/config/apps/target/text()'), rGetLeadingDoc.filename));

  -- ========= SAVE
  -- =======================
  nAppId := isr$XML.valueOf(cxXML, 'count(/config/apps/app)') + 1;
  isr$XML.CreateNodeAttr(cxXML,  '/config/apps', 'app', '', 'order', nAppId);
  isr$XML.SetAttribute(cxXML, '/config/apps/app[@order='||nAppId||']', 'name', 'ME');
  isr$XML.CreateNode(cxXML, '/config/apps/app[@order='||nAppId||']', 'wait_for_process', 'false');
  isr$XML.CreateNode(cxXML, '/config/apps/app[@order='||nAppId||']', 'command', 'SAVE');
  isr$XML.CreateNode(cxXML, '/config/apps/app[@order='||nAppId||']', 'sources', ' ');
  isr$XML.CreateNode(cxXML,'/config/apps/app[@order='||nAppId||']/sources','source','');
  isr$XML.setAttribute(cxXML, '/config/apps/app[@order='||nAppId||']/sources/source[position()=last()]', 'name', rGetLeadingDoc.filename);
  isr$XML.setAttribute(cxXML, '/config/apps/app[@order='||nAppId||']/sources/source[position()=last()]', 'leading', 'T');
  
  -- ========= DISPLAY
  -- =======================
  if NVL(STB$JOB.getJobParameter('DISPLAY_DOC', cnJobId), 'T') = 'T' then
    nAppId := isr$XML.valueOf(cxXML, 'count(/config/apps/app)') + 1;
    isr$XML.CreateNodeAttr(cxXML,  '/config/apps', 'app', '', 'order', nAppId);
    isr$XML.setAttribute(cxXML, '/config/apps/app[@order='||nAppId||']', 'name', 'ME');
    isr$XML.createNode(cxXML, '/config/apps/app[@order='||nAppId||']', 'wait_for_process', 'false');
    isr$XML.createNode(cxXML, '/config/apps/app[@order='||nAppId||']', 'command', 'DISPLAY');
 end if;

  -- number OF APPS
  isr$XML.SetAttribute(cxXML, '/config/apps', 'num', isr$XML.valueOf(cxXML, 'count(/config/apps/app)'));

  isr$trace.debug('final status cxXml', 'cxXml: see column LOGCLOB', sCurrentName, cxXml);
  isr$trace.stat('end', 'Function '||sCurrentName||' finished',sCurrentName);
  return oErrorObj;

exception
  when others then
    oErrorObj.handleError ( Stb$typedef.cnSeverityCritical, utd$msglib.getmsg ('exUnexpectedError', stb$security.getCurrentLanguage,  csP1 => sqlerrm(sqlcode)), 
        sCurrentName, dbms_utility.format_error_stack || CHR(10) || dbms_utility.format_error_backtrace);
    markDBJobAsBroken(cnJobId, oErrorObj);
    return oErrorObj;
end createParametersCheckout;
  
--***********************************************************************************************************************
FUNCTION copyReportFiles(cnJobid in number, cxXml IN OUT xmltype) return STB$OERROR$RECORD is
  sCurrentName constant varchar2(100) := $$PLSQL_UNIT||'.copyReportFiles';
  oErrorObj               STB$OERROR$RECORD := STB$OERROR$RECORD();
  sMsg    varchar2(4000);
  nRepId number := STB$JOB.getJobParameter ('REPID', cnJobId);
begin
  isr$trace.stat('begin', 'begin', sCurrentName);  
  merge into  ISR$REPORT$FILE rf
  using (select jobId
                , filename
                , textfile
                , STB$JOB.getJobParameter ('REPID', cnJobId) repid
                , doc_definition
         from stb$jobtrail
        where jobid = cnJobId
          and filename not like  '%.HTM'
          and documenttype in (select column_value from table(oFileTypeList))) j
  on ( rf.filename = j.filename and rf.repid = j.repid )
  when matched then update set rf.textfile = j.textfile            
                               , rf.jobid = j.jobid 
  when not matched then insert ( rf.entryid
                               , rf.repid
                               , rf.jobid
                               , rf.filename
                               , rf.textfile
                               , rf.doc_definition)
  values( SEQ$ISR$REPORT$FILE.NEXTVAL
         , j.repid
         , j.jobid
         , j.filename
         , j.textfile
         , j.doc_definition); 
  commit;
  ISR$TREE$PACKAGE.sendSyncCall('REPORT_WORD', STB$JOB.getJobParameter ('REPID', cnJobId));
  for rGetReportFiles in (select rf.entryid 
                            from isr$report$file rf, stb$jobtrail j 
                           where rf.repid = STB$JOB.getJobParameter ('REPID', cnJobId) 
                             and j.jobid = cnJobId 
                             and j.filename = rf.filename) loop
    ISR$TREE$PACKAGE.sendSyncCall('HTM_REPORT_FILE', rGetReportFiles.entryid);         
  end loop; 
  isr$trace.stat('end', 'end', sCurrentName);    
  return oErrorObj;
exception
   when others then 
     sMsg := utd$msglib.getmsg ('exUnexpectedError', stb$security.getCurrentLanguage,  csP1 => sqlerrm(sqlcode));   
     oErrorObj.handleError ( Stb$typedef.cnSeverityCritical, sMsg, sCurrentName, 
                           dbms_utility.format_error_stack || CHR(10) || dbms_utility.format_error_backtrace); 
    markDBJobAsBroken(cnJobId, oErrorObj);
    return oErrorObj;                               
end copyReportFiles;


--***********************************************************************************************************************
FUNCTION copyTemplateIntoJobtrail(cnJobid in number, cxXml IN OUT xmltype) return STB$OERROR$RECORD is
  sCurrentName constant varchar2(100) := $$PLSQL_UNIT||'.copyTemplateIntoJobtrail';
  oErrorObj               STB$OERROR$RECORD := STB$OERROR$RECORD();
  sMsg    varchar2(4000);
  nRepId number := STB$JOB.getJobParameter ('REPID', cnJobId);
  blBlob                STB$DOCTRAIL.CONTENT%TYPE;
  
  cursor cGetPrevWordDoc is
   select *
    from stb$doctrail d1
    where docid =
    (select  MAX(d.docId) KEEP (DENSE_RANK last ORDER BY d.docversion)
        from stb$report r, stb$doctrail d
       where     d.nodeid = r.repid
             and doctype = 'W'
      start with r.repid = nRepId
      connect by r.repid = prior r.parentrepid);
   
begin
  isr$trace.stat('begin', 'begin', sCurrentName);  
  if STB$JOB.getJobParameter ('FILTER_MASTERTEMPLATE_WORD', cnJobId) = 'PREV_WORD_DOC' then
    FOR rGetPrevWordDoc IN cGetPrevWordDoc LOOP
      blBlob := STB$JAVA.unzip(rGetPrevWordDoc.content);
      IF DBMS_LOB.GETLENGTH(blBlob) = 0 THEN
        blBlob := null;
      END IF;
      isr$trace.debug('after unzip', DBMS_LOB.GETLENGTH(blBlob), sCurrentName);

      INSERT INTO STB$JOBTRAIL( JOBID, DOCUMENTTYPE, MIMETYPE, DOCSIZE,
                            TIMESTAMP, BINARYFILE, FILENAME, BLOBFLAG,
                            ENTRYID, REFERENCE,LEADINGDOC, DOWNLOAD,DOC_DEFINITION)
                        values( cnJobId, 'TEMPLATE', rGetPrevWordDoc.mimetype, rGetPrevWordDoc.docsize,
                            sysdate, nvl(blBlob,rGetPrevWordDoc.content), rGetPrevWordDoc.filename, 'B',
                            STB$JOBTRAIL$SEQ.NEXTVAL, to_char(rGetPrevWordDoc.DocId), 'F', 'T', rGetPrevWordDoc.doc_definition);        
      
    end loop;        
  end if;
  isr$trace.stat('end', 'end', sCurrentName);    
  return oErrorObj;
exception
   when others then
    sMsg := utd$msglib.getmsg ('exUnexpectedError', stb$security.getCurrentLanguage,  csP1 => sqlerrm(sqlcode));   
    oErrorObj.handleError ( Stb$typedef.cnSeverityCritical, sMsg, sCurrentName, 
                           dbms_utility.format_error_stack || CHR(10) || dbms_utility.format_error_backtrace); 
    markDBJobAsBroken(cnJobId, oErrorObj);
    return oErrorObj;                               
end copyTemplateIntoJobtrail;


--***********************************************************************************************************************
function copyWordDocumentIntoDoctrail( cnJobid in     number,
                                   cxXml   in out xmltype)
  return STB$OERROR$RECORD
is
  sCurrentName   constant varchar2(100) := $$PLSQL_UNIT||'.copyWordDocumentIntoDoctrail (JobID='||cnJobId||')';
  sMsg               varchar2(4000);
  oErrorObj               STB$OERROR$RECORD :=STB$OERROR$RECORD();
  sNodeTyp               varchar2(100) := 'REPORT_WORD';
  nRepId   number;

  cursor cGetDocIds is
  select max(dt.DOCID) docid, jt.ENTRYID
  from   STB$DOCTRAIL dt, STB$JOBTRAIL jt
  where  dt.nodeid = to_char(nRepId)
  and    jt.DOCUMENTTYPE IN('W')
  and    ( jt.doc_definition = dt.doc_definition )
  and    jt.jobid = cnJobId
  group by jt.ENTRYID
  order by entryid;

begin
  isr$trace.stat('begin', 'Function '||sCurrentName||' started.', sCurrentName);
  nRepId := STB$JOB.getJobParameter('REPID',cnJobId);
  isr$trace.debug('status repid', 'STB$JOB.getJobParameter(REPID,cnJobId): '||nRepId, sCurrentName);
  

  for rGetDocIds in cGetDocIds loop

    oErrorObj := Stb$docks.updateDoctrail( rGetDocIds.DOCID,
                                        nRepId,
                                        rGetDocIds.ENTRYID,
                                        sNodeTyp,
                                        STB$JOB.getJobParameter('COMMENT',cnJobId) );

  end loop;

  ISR$TREE$PACKAGE.sendSyncCall('REPORT_WORD', nRepId); 
  isr$trace.stat('end', 'Function '||sCurrentName||' finished',sCurrentName);
  return oErrorObj;

exception
  when others then
    rollback;
    sMsg := utd$msglib.getmsg ('exUnexpectedError', stb$security.getCurrentLanguage,  csP1 => sqlerrm(sqlcode));   
    oErrorObj.handleError ( Stb$typedef.cnSeverityCritical, sMsg, sCurrentName, 
                           dbms_utility.format_error_stack || CHR(10) || dbms_utility.format_error_backtrace); 
    markDBJobAsBroken(cnJobId, oErrorObj);
    return oErrorObj;         
end copyWordDocumentIntoDoctrail;


--***********************************************************************************************************************
function isReportFileExist(nRepid in isr$report$file.repid%type, sFilename in isr$report$file.filename%type) return varchar2 is
  sCurrentName constant varchar2(100) := $$PLSQL_UNIT||'.isReportFileExist';
  oErrorObj               STB$OERROR$RECORD := STB$OERROR$RECORD();
  sMsg    varchar2(4000);
  sIsExist varchar2(1) := 'F';
begin
  isr$trace.stat('begin', 'begin', sCurrentName);  
  select 'T'
  into sIsExist
  from isr$report$file
  where repid = nRepid and filename = sFilename;
  isr$trace.stat('end', 'end', sCurrentName);    
  return sIsExist;
exception
   when others then
    oErrorObj.handleError ( Stb$typedef.cnSeverityCritical, sMsg, sCurrentName, 
                           dbms_utility.format_error_stack || CHR(10) || dbms_utility.format_error_backtrace); 
    return sIsExist;
end isReportFileExist;

--***********************************************************************************************************************
function loadFilesFromClient( cnJobid in     number,
                              cxXml   in out xmltype)
  return STB$OERROR$RECORD
is
  exLoadFileError         exception;
  sCurrentName   constant varchar2(100) := $$PLSQL_UNIT||'.loadFilesFromClient (JobID='||cnJobId||')';
  sUserMsg                varchar2(4000);
  oErrorObj               STB$OERROR$RECORD :=STB$OERROR$RECORD();
  sReturn                 varchar2(4000);
  xXml                    xmltype;
  sTargetType             varchar2(1);
  sFileName               STB$DOCTRAIL.FILENAME%type;
  sMimetype               STB$DOCTRAIL.MIMETYPE%type;
  sIsrDocType             STB$JOBTRAIL.DOCUMENTTYPE%type;
  sDocDefinition          STB$DOCTRAIL.DOC_DEFINITION%type;
  sclDom                  STB$JOBTRAIL.TEXTFILE%type;
  sReference            STB$JOBTRAIL.REFERENCE%type;
  
  csConfig              varchar2(100) :='CONFIG';
  sJavaLogLevel         isr$loglevel.javaloglevel%type := isr$trace.getJavaUserLoglevelStr;
begin
  isr$trace.stat('begin', 'Function '||sCurrentName||' started.', sCurrentName);

  sFileName := 'config.xml';
  SELECT ip, to_char(port) INTO sIp, sPort FROM STB$JOBSESSION WHERE jobid = cnJobId;
  
  sReturn := ISR$LOADER.loadFileFromLoader(sIp, sPort, sContext, sNameSpace, cnJobId, sFileName, 
             'LOADER_BLOB_FILE', csConfig,  sJavaLogLevel, isr$trace.getReference, to_number(stimeout));
  IF sReturn != 'T' THEN
    ISR$TRACE.error('sReturn', sReturn, sCurrentName);
  END IF;
  if sReturn != 'T' then
    raise exLoadFileError;
  end if;

  select textfile into sclDom
  from   STB$JOBTRAIL
  where  jobid = cnjobid
  and    documenttype = csConfig;
  isr$trace.debug('status sclDom', 'DBMS_LOB.GETLENGTH(sclDom): '||DBMS_LOB.GETLENGTH(sclDom), sCurrentName, 'sclDom: '||sclDom);

  xXml := isr$XML.ClobToXml(sclDom);

  for i in 1..ISR$XML.valueOf(xXml, 'count(/config/apps/app/workbooks/workbook)') loop
    isr$trace.debug('i',i, sCurrentName);
  
    sTargetType := isr$XML.valueOf(xXml, '/config/apps/app/workbooks/workbook[@order="' || i || '"]/@targettype');
    sMimetype   := isr$XML.valueOf(xXml, '/config/apps/app/workbooks/workbook[@order="' || i || '"]/@mimetype');
    sIsrDocType := isr$XML.valueOf(xXml, '/config/apps/app/workbooks/workbook[@order="' || i || '"]/@isrdoctype');
    sDocDefinition := isr$XML.valueOf(xXml, '/config/apps/app/workbooks/workbook[@order="' || i || '"]/@doc_definition');
    
    sReference := STB$JOB.getJobParameter('REPID', cnJobId);

    sFileName   := isr$XML.valueOf(xXml, '/config/apps/app/workbooks/workbook[@order="' || i || '"]/@filename');
    isr$trace.debug('status sFileName/sTargetType','sFileName/sTargetType: '||sFileName||'/'||sTargetType, sCurrentName);
    
    sReturn := ISR$LOADER.loadFileFromLoader(sIp, sPort, sContext, sNameSpace, cnJobId, sFileName, 
               case when sTargetType = 'C' then 'LOADER_CLOB_FILE' else 'LOADER_BLOB_FILE' end, sIsrDocType, sJavaLogLevel, isr$trace.getReference, to_number(stimeout));        
    
    IF sReturn != 'T' THEN
      ISR$TRACE.error('sReturn', sReturn, sCurrentName);
      raise exLoadFileError;
    END IF;

    update STB$JOBTRAIL
     set docsize = case when sTargetType = 'C' then DBMS_LOB.GETLENGTH(TEXTFILE)
                        when sTargetType = 'B' then DBMS_LOB.GETLENGTH(BINARYFILE) end,
      reference = sReference,
      mimetype = sMimeType,
      blobflag = sTargetType,
      leadingdoc = 'T',
      download = 'T',
      doc_definition = sDocDefinition
    where jobid = cnJobId
    and documenttype = sIsrDocType
    and filename = sFileName;

  end loop;

  commit;

  isr$trace.stat('end', 'Function '||sCurrentName||' finished',sCurrentName);
  return oErrorObj;

exception
  when exLoadFileError then
    sUserMsg := utd$msglib.getmsg ('exLoadFileError', stb$security.getCurrentLanguage,  csP1 => sqlerrm(sqlcode), csP2 => 'ISR$LOADER.loadFileFromLoader failed', csP3 => ' filename: '||sFileName);
   
    oErrorObj.handleError ( Stb$typedef.cnSeverityCritical, sUserMsg, sCurrentName, 
                           dbms_utility.format_error_stack || CHR(10) || dbms_utility.format_error_backtrace); 
    markDBJobAsBroken(cnJobId, oErrorObj);
    return oErrorObj;
  when others then
    sUserMsg := utd$msglib.getmsg ('exUnexpectedError', stb$security.getCurrentLanguage,  csP1 => sqlerrm(sqlcode));
    
    oErrorObj.handleError ( Stb$typedef.cnSeverityCritical, sUserMsg, sCurrentName, 
                           dbms_utility.format_error_stack || CHR(10) || dbms_utility.format_error_backtrace); 
    markDBJobAsBroken(cnJobId, oErrorObj);
    return oErrorObj;
end loadFilesFromClient;


--***********************************************************************************************************************
function saveConfigForRunReport( cnJobid in     number,
                     cxXml   in out xmltype)
  return STB$OERROR$RECORD
is
  sCurrentName   constant varchar2(100) := $$PLSQL_UNIT||'.saveConfigForRunReport('||cnJobId||')';
  sUserMsg               varchar2(4000);
  oErrorObj               STB$OERROR$RECORD :=STB$OERROR$RECORD();
 
  nDocId                STB$DOCTRAIL.DOCID%type;
  sIsrDocType           STB$DOCTRAIL.DOCTYPE%type;
  sFileName             STB$DOCTRAIL.FILENAME%type;
  sMimetype             STB$DOCTRAIL.MIMETYPE%type;
  sDocDefinition        STB$DOCTRAIL.DOC_DEFINITION%type;
  sDocOid               STB$DOCTRAIL.DOC_OID%type;

  cursor cGetDocOid(cnDocId in STB$DOCTRAIL.DOCID%type) is
    select doc_oid
    from   stb$doctrail
    where  docid = cnDocId;


  cursor cGetApplicationPathes(cnUserId in STB$USER.USERNO%type) is
    select applicationpathes
    from   stb$user
    where  userno = cnUserId;

begin
  isr$trace.stat('begin','begin',sCurrentName);
  isr$trace.debug('value', 'cxXml: see column LOGCLOB', sCurrentName, cxXml);

  -- over the config xml file get the targets and create an entry in the doctrail
  for i in 1..ISR$XML.valueOf(cxXml, 'count(/config/apps/app/workbooks/workbook)') loop
    isr$trace.debug_all('for i in 1..ISR$XML.valueOf()','i: '||i, sCurrentName);
    sMimetype   := isr$XML.valueOf(cxXml, '/config/apps/app/workbooks/workbook[@order="' || i || '"]/@mimetype');
    sIsrDocType := isr$XML.valueOf(cxXml, '/config/apps/app/workbooks/workbook[@order="' || i || '"]/@isrdoctype');
    sDocDefinition := isr$XML.valueOf(cxXml, '/config/apps/app/workbooks/workbook[@order="' || i || '"]/@doc_definition');
    sFileName   := isr$XML.valueOf(cxXml, '/config/apps/app/workbooks/workbook[@order="' || i || '"]/@filename');    

    isr$trace.debug_all('status docid', 'STB$JOB.getJobParameter(DOCID,cnJobid): '||STB$JOB.getJobParameter('DOCID',cnJobid), sCurrentName);
    isr$trace.debug_all('status sMimetype', 'sMimetype: '||sMimetype, sCurrentName);

    oErrorObj := insertIntoDoctrail( STB$JOB.getJobParameter('REPID',cnJobid),
                                            sIsrDocType,
                                            sFileName,
                                            sMimetype,
                                            STB$JOB.getJobParameter('USER',cnJobId),
                                            nDocid,
                                            STB$JOB.getJobParameter('REPORTTYPEID',cnJobid) ,
                                            sDocDefinition); 

    --mark the document to be deleted when job fails
    update stb$doctrail
       set job_flag = 'D'
     where docid = nDocId;
    commit;
    
    -- get oid
    open cGetDocOid(nDocId);
    fetch cGetDocOid into sDocOid;
    close cGetDocOid;
    
   -- isr$XML.CreateNode(cxXML, '/config/apps/app[@name="excelmodule"]/workbooks/workbook['|| i ||']/properties', 'custom-property', sDocOid );
   -- isr$XML.SetAttribute(cxXML, '/config/apps/app[@name="excelmodule"]/workbooks/workbook['|| i ||']/properties/custom-property[position()=last()]', 'name', csDocPropOid);
    
  end loop;

  isr$XML.CreateNode(cxXML, '/config/apps/app[@name="excelmodule"]/workbooks/workbook[position()=last()]/properties', 'custom-property', sDocOid );
  isr$XML.SetAttribute(cxXML, '/config/apps/app[@name="excelmodule"]/workbooks/workbook[position()=last()]/properties/custom-property[position()=last()]', 'name', csDocPropOid);

  isr$trace.debug('cxXml', 's. logclob', sCurrentName, cxXml);
 
  insert into STB$JOBTRAIL (
      JOBID,
      REFERENCE,
      TEXTFILE,
      DOCUMENTTYPE,
      MIMETYPE,
      TIMESTAMP,
      FILENAME,
      BLOBFLAG,
      DOWNLOAD
     ) values (
      cnJobid,
      cnJobid,
      cxXML.getClobVal(),
      'CONFIG',
      'text/xml',
       sysdate ,
      'config.xml',
      'C',
      'T'
     );

  commit;

  isr$trace.stat('end', 'Function '||sCurrentName||' finished',sCurrentName);
  return oErrorObj;

exception
  when others then 
    sUserMsg := utd$msglib.getmsg ('exUnexpectedError', stb$security.getCurrentLanguage,  csP1 => sqlerrm(sqlcode));
    
    oErrorObj.handleError ( Stb$typedef.cnSeverityCritical, sUserMsg, sCurrentName, 
                           dbms_utility.format_error_stack || CHR(10) || dbms_utility.format_error_backtrace); 
    markDBJobAsBroken(cnJobId, oErrorObj);
    return oErrorObj;
end saveConfigForRunReport;


--***********************************************************************************************************************
function saveConfigForFinal( cnJobid in     number,
                     cxXml   in out xmltype)
  return STB$OERROR$RECORD
is
  sCurrentName   constant varchar2(100) := $$PLSQL_UNIT||'.saveConfigForFinal('||cnJobId||')';
  sUserMsg               varchar2(4000);
 
  oErrorObj               STB$OERROR$RECORD :=STB$OERROR$RECORD();
  nRecordID            number;

  nDocId                STB$DOCTRAIL.DOCID%type;
  sDocOid               STB$DOCTRAIL.DOC_OID%type;
  

  cursor cGetPropertiesSetByUser(cnDocOid in STB$DOCTRAIL.DOC_OID%TYPE,
                                 cnDocId in STB$DOCTRAIL.DOCID%type) is
    select p.parametername,
           NVL(parameterdisplay, parametervalue) parametervalue
    from   isr$doc$property P,
           stb$doctrail d,
           isr$doc$prop$template t
    where  P.docid = d.docid
    and    (   d.doc_oid = Nvl(cnDocOid,'DOC_OID_NOT_SET')
            or d.docid = Nvl(cnDocId,0))
    and    t.parametername = p.parametername
    and    t.parametertype != 'PARAMETERS'
    and    t.reporttypeid = STB$JOB.getJobParameter('REPORTTYPEID',cnJobid)
    union
    select v.value, utd$msglib.getMsg(v.msgkey, stb$object.GetCurrentReportLanguage, v.plugin)
    from   isr$doc$property P,
           stb$doctrail d,
           isr$doc$prop$template t,
           isr$parameter$values v,
           utd$msg m
    where  P.docid = d.docid
    and    t.parametername = p.parametername
    and    (   d.doc_oid = Nvl(cnDocOid,'DOC_OID_NOT_SET')
            or d.docid = Nvl(cnDocId,0))
    and    t.parametertype = 'PARAMETERS'
    and    t.reporttypeid = STB$JOB.getJobParameter('REPORTTYPEID',cnJobid)
    and    v.entity = p.parametervalue;

  cursor cGetApplicationPathes(cnUserId in STB$USER.USERNO%type) is
    select applicationpathes
    from   stb$user
    where  userno = cnUserId;

  sApplicationPathes STB$USER.APPLICATIONPATHES%type;
begin
  isr$trace.stat('begin','begin',sCurrentName);
  isr$trace.debug('value', 'cxXml: see column LOGCLOB', sCurrentName, cxXml);

  -- over the config xml file get the targets 
  for i in 1..ISR$XML.valueOf(cxXML,'count(/config/apps/app[@name="wordmodul" and command="CAN_RUN_REPORT"])') loop
    isr$trace.debug_all('for i in 1..ISR$XML.valueOf()','i: '||i, sCurrentName);

    if isr$XML.ValueOf(cxXML, 'count(/config/apps/app[@name="wordmodul" and command="CAN_RUN_REPORT"]['|| i ||']/properties)') = 0 then
      isr$XML.CreateNode(cxXML, '/config/apps/app[@name="wordmodul" and command="CAN_RUN_REPORT"]['|| i ||']','properties', '');
    end if;

    isr$XML.CreateNode(cxXML, '/config/apps/app[@name="wordmodul" and command="CAN_RUN_REPORT"]['|| i ||']/properties', 'custom-property', sDocOid );
    isr$XML.SetAttribute(cxXML, '/config/apps/app[@name="wordmodul" and command="CAN_RUN_REPORT"]['|| i ||']/properties/custom-property[position()=last()]', 'name', csDocPropOid);

    if  isr$XML.ValueOf(cxXML, '/config/apps/app[@name="wordmodul" and command="CAN_RUN_REPORT"]['||i ||']/doc_definition/text()') = 'esig' then
      isr$trace.debug('esig doc - master oid','sDoc0id: '||sDocOid, sCurrentName, cxXML);
      -- get the master oid of all other documents in the config xml. file
      isr$XML.CreateNode(cxXML, '/config/apps/app[@name="wordmodul" and command="CAN_RUN_REPORT"]['|| i ||']/properties', 'custom-property',
                  isr$XML.ValueOf(cxXML, '/config/apps/app[@name="wordmodul" and command="CAN_RUN_REPORT" and doc_definition != "esig"]/properties/custom-property[@name="DOC_OID"]/text()') );
      isr$XML.SetAttribute(cxXML, '/config/apps/app[@name="wordmodul" and command="CAN_RUN_REPORT"]['|| i ||']/properties/custom-property[position()=last()]', 'name', 'MASTER_OID');
      isr$trace.debug('esig doc - after master oid','logclob', sCurrentName, cxXML);
    end if;

  end loop;


  -- write user's properties
  for i in 1..ISR$XML.valueOf(cxXML,'count(/config/apps/app[@name="wordmodul"])') loop

    -- if not final then write all other document properties
    if STB$JOB.getJobParameter('FINALFLAG', cnJobid) is  null  then

      -- check if the doc id isn't null  added JB 17 Nov 2008
      if STB$JOB.getJobParameter('DOCID',cnJobid) is not null then
        isr$trace.debug_all('status docid', 'STB$JOB.getJobParameter(DOCID,cnJobid): '||STB$JOB.getJobParameter('DOCID',cnJobid), sCurrentName);

        for rGetPropertiesSetByUser in cGetPropertiesSetByUser(ISR$XML.valueOf(cxXML,'/config/apps/app[@name="wordmodul"]['||i||']/properties/custom-property[@name="'||csDocPropOid||'"]/text()'),
                                                             STB$JOB.getJobParameter('DOCID',cnJobid)) loop
          if isr$XML.ValueOf(cxXML, 'count(/config/apps/app[@name="wordmodul"]['||i||']/properties)') = 0 then
            isr$XML.CreateNode(cxXML, '/config/apps/app[@name="wordmodul"]['||i||']','properties', '');
         end if;
          isr$XML.CreateNode(cxXML, '/config/apps/app[@name="wordmodul"]['||i||']/properties', 'custom-property', rGetPropertiesSetByUser.parametervalue);
          isr$XML.SetAttribute(cxXML, '/config/apps/app[@name="wordmodul"]['||i||']/properties/custom-property[position()=last()]', 'name', rGetPropertiesSetByUser.parametername);
        end loop;

      elsif nDocid is not null then
         isr$trace.debug_all('status nDocid', 'nDocid: '||nDocid, sCurrentName);
         for rGetPropertiesSetByUser in cGetPropertiesSetByUser(ISR$XML.valueOf(cxXML,'/config/apps/app[@name="wordmodul"]['||i||']/properties/custom-property[@name="'||csDocPropOid||'"]/text()'),
                                                            nDocid ) loop
          if isr$XML.ValueOf(cxXML, 'count(/config/apps/app[@name="wordmodul"]['||i||']/properties)') = 0 then
            isr$XML.CreateNode(cxXML, '/config/apps/app[@name="wordmodul"]['||i||']','properties', '');
         end if;
          if isr$XML.valueOf(cxXML, 'count((/config/apps/app[@name="wordmodul"])['||i||']/properties/custom-property[@name="'||rGetPropertiesSetByUser.parametername||'"])') = 0 then
            isr$XML.CreateNode(cxXML, '/config/apps/app[@name="wordmodul"]['||i||']/properties', 'custom-property', rGetPropertiesSetByUser.parametervalue);
            isr$XML.SetAttribute(cxXML, '/config/apps/app[@name="wordmodul"]['||i||']/properties/custom-property[position()=last()]', 'name', rGetPropertiesSetByUser.parametername);
         end if;
        end loop;

     end if;

      -- call the customer package
      execute immediate
        'BEGIN
          :1 := ' || STB$UTIL.getCurrentCustomPackage || '.writeCustomDocProperties(:2, :3);
         end;'
      using out oErrorObj, in out cxXML, in cnJobid;

   end if;

  end loop;


  open  cGetApplicationPathes(To_Number(STB$JOB.getJobParameter('USER',cnJobid)));
  fetch cGetApplicationPathes into sApplicationPathes;
  close cGetApplicationPathes;

  if Trim(sApplicationPathes) is not null then
    isr$XML.createNode(cxXML, '/config/apps', 'applicationpathes', sApplicationPathes);
 end if;

  if Trim(ISR$XML.valueOf(cxXML, '/config/apps/fullpathname/text()')) is null then
    isr$XML.deleteNode(cxXML, '/config/apps/fullpathname');
 end if;
  if Trim(ISR$XML.valueOf(cxXML, '/config/apps/target/text()')) is null then
    isr$XML.deleteNode(cxXML, '/config/apps/target');
 end if;
  if Trim(ISR$XML.valueOf(cxXML, '/config/apps/extension/text()')) is null then
    isr$XML.deleteNode(cxXML, '/config/apps/extension');
 end if;
  if Trim(ISR$XML.valueOf(cxXML, '/config/apps/overwrite/text()')) is null then
    isr$XML.deleteNode(cxXML, '/config/apps/overwrite');
 end if;

  isr$trace.debug('cxXml', 's. logclob', sCurrentName, cxXml);
  -- create entry in STB$JOBTRAIL
  select STB$JOBTRAIL$SEQ.NEXTVAL into nRecordID from dual;


  insert into STB$JOBTRAIL (
      ENTRYID,
      JOBID,
      REFERENCE,
      TEXTFILE,
      DOCUMENTTYPE,
      MIMETYPE,
      TIMESTAMP,
      FILENAME,
      BLOBFLAG,
      DOWNLOAD
     ) values (
      nRecordID,
      cnJobid,
      cnJobid,
      cxXML.getClobVal(),
      'CONFIG',
      'text/xml',
       sysdate ,
      'config.xml',
      'C',
      'T'
     );

  commit;

  isr$trace.stat('end', 'Function '||sCurrentName||' finished',sCurrentName);
  return oErrorObj;

exception
  when others then
    sUserMsg := utd$msglib.getmsg ('exUnexpectedError', stb$security.getCurrentLanguage,  csP1 => sqlerrm(sqlcode));
    
    oErrorObj.handleError ( Stb$typedef.cnSeverityCritical, sUserMsg, sCurrentName, 
                           dbms_utility.format_error_stack || CHR(10) || dbms_utility.format_error_backtrace); 
    markDBJobAsBroken(cnJobId, oErrorObj);
    return oErrorObj;
end saveConfigForFinal;

--***********************************************************************************************************************
function saveConfigForCheckout( cnJobid in     number,
                     cxXml   in out xmltype)
  return STB$OERROR$RECORD
is
  sCurrentName   constant varchar2(100) := $$PLSQL_UNIT||'.saveConfigForCheckout('||cnJobId||')';
  sUserMsg               varchar2(4000);
  oErrorObj               STB$OERROR$RECORD :=STB$OERROR$RECORD();
 
  nDocId                STB$DOCTRAIL.DOCID%type; 
  csWorkbookPath        varchar2(100) := '/config/apps/app[@name="excelmodule"]/workbooks/workbook';
  
  cursor cGetDocOid(cnDocId in STB$DOCTRAIL.DOCID%type) is
    select doc_oid
    from   stb$doctrail
    where  docid = cnDocId;
  sDocOid               STB$DOCTRAIL.DOC_OID%type;

cursor cGetPropertiesSetByUser(cnDocOid in STB$DOCTRAIL.DOC_OID%TYPE,
                                 cnDocId in STB$DOCTRAIL.DOCID%type) is
    select p.parametername,
           NVL(parameterdisplay, parametervalue) parametervalue
    from   isr$doc$property P,
           stb$doctrail d,
           isr$doc$prop$template t
    where  P.docid = d.docid
    and    (   d.doc_oid = Nvl(cnDocOid,'DOC_OID_NOT_SET')
            or d.docid = Nvl(cnDocId,0))
    and    t.parametername = p.parametername
    and    t.parametertype != 'PARAMETERS'
    and    t.reporttypeid = STB$JOB.getJobParameter('REPORTTYPEID',cnJobid)
    union
    select v.value, utd$msglib.getMsg(v.msgkey, stb$object.GetCurrentReportLanguage, v.plugin)
    from   isr$doc$property P,
           stb$doctrail d,
           isr$doc$prop$template t,
           isr$parameter$values v,
           utd$msg m
    where  P.docid = d.docid
    and    t.parametername = p.parametername
    and    (   d.doc_oid = Nvl(cnDocOid,'DOC_OID_NOT_SET')
            or d.docid = Nvl(cnDocId,0))
    and    t.parametertype = 'PARAMETERS'
    and    t.reporttypeid = STB$JOB.getJobParameter('REPORTTYPEID',cnJobid)
    and    v.entity = p.parametervalue;
    
  cursor cGetApplicationPathes(cnUserId in STB$USER.USERNO%type) is
    select applicationpathes
    from   stb$user
    where  userno = cnUserId;
    


begin
  isr$trace.stat('begin','begin',sCurrentName);
  isr$trace.debug('value', 'cxXml: see column LOGCLOB', sCurrentName, cxXml);

  for i in 1..ISR$XML.valueOf(cxXML,'count(/config/apps/app[@name="excelmodule" and command="CAN_CHECKOUT"])') loop

    nDocId := STB$JOB.getJobParameter('REFERENCE',cnJobid);

    isr$XML.CreateNode(cxXML, '/config/apps/app[@name="excelmodule" and command="CAN_CHECKOUT"]['|| i ||']', 'docid', nDocId);

    -- get oid
    open cGetDocOid(STB$JOB.getJobParameter('REFERENCE',cnJobid));
    fetch cGetDocOid into sDocOid;
    close cGetDocOid;

  end loop;

 -- write user's properties
  for i in 1..ISR$XML.valueOf(cxXML,'count('|| csWorkbookPath|| ')') loop
    
    if isr$XML.ValueOf(cxXML, 'count('|| csWorkbookPath || '['|| i ||']/properties)') = 0 then
      isr$XML.CreateNode(cxXML, csWorkbookPath || '['|| i ||']','properties', '');
    end if;

    isr$XML.CreateNode(cxXML, csWorkbookPath || '['|| i ||']/properties', 'custom-property', sDocOid );
    isr$XML.SetAttribute(cxXML, csWorkbookPath || '['|| i ||']/properties/custom-property[position()=last()]', 'name', csDocPropOid);

    -- if not final then write all other document properties
    if STB$JOB.getJobParameter('FINALFLAG', cnJobid) is  null  then
      -- check if the doc id isn't null 
      if STB$JOB.getJobParameter('DOCID',cnJobid) is not null then
        isr$trace.debug_all('status docid', 'STB$JOB.getJobParameter(DOCID,cnJobid): '||STB$JOB.getJobParameter('DOCID',cnJobid), sCurrentName);

        for rGetPropertiesSetByUser in cGetPropertiesSetByUser(ISR$XML.valueOf(cxXML,csWorkbookPath|| '['||i||']/properties/custom-property[@name="'||csDocPropOid||'"]/text()'),
                                                             STB$JOB.getJobParameter('DOCID',cnJobid)) loop
          if isr$XML.ValueOf(cxXML, 'count('|| csWorkbookPath|| '['||i||']/properties)') = 0 then
            isr$XML.CreateNode(cxXML, csWorkbookPath|| '['||i||']','properties', '');
         end if;
          isr$XML.CreateNode(cxXML, csWorkbookPath|| '['||i||']/properties', 'custom-property', rGetPropertiesSetByUser.parametervalue);
          isr$XML.SetAttribute(cxXML, csWorkbookPath|| '['||i||']/properties/custom-property[position()=last()]', 'name', rGetPropertiesSetByUser.parametername);
        end loop;

      elsif nDocid is not null then
         isr$trace.debug_all('status nDocid', 'nDocid: '||nDocid, sCurrentName);
         for rGetPropertiesSetByUser in cGetPropertiesSetByUser(ISR$XML.valueOf(cxXML,csWorkbookPath|| '['||i||']/properties/custom-property[@name="'||csDocPropOid||'"]/text()'),
                                                            nDocid ) loop
          if isr$XML.ValueOf(cxXML, 'count('|| csWorkbookPath || '['||i||']/properties)') = 0 then
            isr$XML.CreateNode(cxXML, csWorkbookPath|| '['||i||']','properties', '');
         end if;
          if isr$XML.valueOf(cxXML, 'count((count('|| csWorkbookPath|| ')['||i||']/properties/custom-property[@name="'||rGetPropertiesSetByUser.parametername||'"])') = 0 then
            isr$XML.CreateNode(cxXML, csWorkbookPath || '['||i||']/properties', 'custom-property', rGetPropertiesSetByUser.parametervalue);
            isr$XML.SetAttribute(cxXML, csWorkbookPath || '['||i||']/properties/custom-property[position()=last()]', 'name', rGetPropertiesSetByUser.parametername);
         end if;
        end loop;

     end if;

      -- call the customer package
      execute immediate
        'BEGIN
          :1 := ' || STB$UTIL.getCurrentCustomPackage || '.writeCustomDocProperties(:2, :3);
         end;'
      using out oErrorObj, in out cxXML, in cnJobid;

   end if;

  end loop;
  isr$trace.debug('cxXml', 's. logclob', sCurrentName, cxXml);
 
  insert into STB$JOBTRAIL (
      JOBID,
      REFERENCE,
      TEXTFILE,
      DOCUMENTTYPE,
      MIMETYPE,
      TIMESTAMP,
      FILENAME,
      BLOBFLAG,
      DOWNLOAD
     ) values (
      cnJobid,
      cnJobid,
      cxXML.getClobVal(),
      'CONFIG',
      'text/xml',
       sysdate ,
      'config.xml',
      'C',
      'T'
     );

  commit;

  isr$trace.stat('end', 'Function '||sCurrentName||' finished',sCurrentName);
  return oErrorObj;

exception
  when others then 
    sUserMsg := utd$msglib.getmsg ('exUnexpectedError', stb$security.getCurrentLanguage,  csP1 => sqlerrm(sqlcode));
    
    oErrorObj.handleError ( Stb$typedef.cnSeverityCritical, sUserMsg, sCurrentName, 
                           dbms_utility.format_error_stack || CHR(10) || dbms_utility.format_error_backtrace); 
    markDBJobAsBroken(cnJobId, oErrorObj);
    return oErrorObj;
end saveConfigForCheckout;

--***********************************************************************************************************************
function updateDoctrailAfterCheckin( cnJobid in     number,
                                 cxXml   in out xmltype)
  return STB$OERROR$RECORD
is
  sCurrentName   constant varchar2(100) := $$PLSQL_UNIT||'.updateDoctrailAfterCheckin (JobID='||cnJobId||')';
  sUserMsg                varchar2(4000);
  oErrorObj               STB$OERROR$RECORD :=STB$OERROR$RECORD();
  sNodeTyp                varchar2(100) := Nvl(STB$JOB.getJobParameter('NODETYPE',cnJobId),'REPORT');
  sNodeId                 varchar2(100) := Nvl(STB$JOB.getJobParameter('REPID',cnJobId),STB$JOB.getJobParameter('NODEID',cnJobId));

  cursor cGetFiles is
    select jt.entryid,
           jt.filename,
           d.doctype
      from STB$JOBTRAIL jt, STB$DOCTRAIL d
     where jt.documenttype = 'TARGET'
       and jt.jobid = cnJobId
       and d.docid(+) = jt.reference;

begin
  isr$trace.stat('begin', 'Function '||sCurrentName||' started.', sCurrentName);
  isr$trace.debug('status sNodeTyp sNodeId','sNodeTyp: '||sNodeTyp||'  sNodeId: '||sNodeId, sCurrentName);

  for rGetFiles in cGetFiles loop
    isr$trace.debug('status filename', 'rGetFiles.filename: '||rGetFiles.filename, sCurrentName);
    isr$trace.debug('status doctype', 'rGetFiles.doctype: '||rGetFiles.doctype, sCurrentName);
   
    if rGetFiles.doctype = 'D' then           
      oErrorObj := Stb$docks.updateDoctrail( STB$JOB.getJobParameter('DOCID',cnJobId),
                                          sNodeId,
                                          rGetFiles.entryid,
                                          sNodeTyp,
                                          STB$JOB.getJobParameter('COMMENT',cnJobId) );
    end if;
  end loop;

  isr$trace.stat('end', 'Function '||sCurrentName||' finished',sCurrentName);
  return oErrorObj;

exception
   when others then
    rollback;
    sUserMsg := utd$msglib.getmsg ('exUnexpectedError', stb$security.getCurrentLanguage,  csP1 => sqlerrm(sqlcode));
    oErrorObj.handleError ( Stb$typedef.cnSeverityCritical, sUserMsg, sCurrentName, 
                           dbms_utility.format_error_stack || CHR(10) || dbms_utility.format_error_backtrace); 
    markDBJobAsBroken(cnJobId, oErrorObj);
    return oErrorObj;
end updateDoctrailAfterCheckin;

--***********************************************************************************************************************
function copyXMLToJob( cnJobid in number, cxXml   in out xmltype)
  return STB$OERROR$RECORD
is
  exRawDataError          exception; 
  sCurrentName   constant varchar2(100) := $$PLSQL_UNIT||'.copyXMLToJob (JobID='||cnJobId||')';
  sUserMsg                varchar2(4000);
  oErrorObj               STB$OERROR$RECORD :=STB$OERROR$RECORD();
  nDocIdXML               STB$DOCTRAIL.DOCID%type;

 begin
  isr$trace.stat('begin', 'Function '||sCurrentName||' started.', sCurrentName);

  if stb$action.XMLFileExists(cnJobid, 'X', nDocIdXML) then
    oErrorObj := Stb$docks.copyXMLToJob(nDocIdXML,cnJobId, 'ROHDATEN');
  end if;

  if oErrorObj.sSeverityCode <> stb$typedef.cnNoError then
    STB$JOB.setJobParameter('ALERTERROR', 'T', cnJobid);
    raise exRawDataError;
  end if;

  isr$trace.stat('end', 'Function '||sCurrentName||' finished',sCurrentName);
  return oErrorObj;

exception
  when exRawDataError then
    sUserMsg := utd$msglib.getmsg ('exRawDataError', stb$security.getCurrentLanguage,  csP1 => sqlerrm(sqlcode));
    oErrorObj.handleError ( Stb$typedef.cnSeverityCritical, sUserMsg, sCurrentName, 
                           dbms_utility.format_error_stack || CHR(10) || dbms_utility.format_error_backtrace); 
    markDBJobAsBroken(cnJobId, oErrorObj);
    return oErrorObj;
  when others then
    sUserMsg := utd$msglib.getmsg ('exUnexpectedError', stb$security.getCurrentLanguage,  csP1 => sqlerrm(sqlcode));
    oErrorObj.handleError ( Stb$typedef.cnSeverityCritical, sUserMsg, sCurrentName, 
                           dbms_utility.format_error_stack || CHR(10) || dbms_utility.format_error_backtrace); 
    markDBJobAsBroken(cnJobId, oErrorObj);
    return oErrorObj;
end copyXMLToJob;


--****************************************************************************************************************************
function copyDocumentsIntoJobForFinal( cnJobid in     number,
                                   cxXml   in out xmltype)
  return STB$OERROR$RECORD
is
  sCurrentName   constant varchar2(100) := $$PLSQL_UNIT||'.copyDocumentsIntoJobForFinal (JobID='||cnJobId||')';
  sUserMsg               varchar2(4000);
  sDevMsg                varchar2(4000);
  sImplMsg               varchar2(4000);
  oErrorObj               STB$OERROR$RECORD :=STB$OERROR$RECORD();
  
  cursor cGetExcelId is
  SELECT distinct d.docid
  FROM   STB$REPORT r, STB$DOCTRAIL d
    where  r.repid = STB$JOB.getJobParameter('REPID', cnJobid)
     and   r.parentrepid = d.nodeid
     and   doctype in ('D')
  ORDER BY d.DOCVERSION DESC, d.DOCVERSION ASC
  FETCH  first 1 rows only;
  
  cursor cGetWordId is
  SELECT distinct d.docid
  FROM   STB$REPORT r, STB$DOCTRAIL d
    where  r.repid = STB$JOB.getJobParameter('REPID', cnJobid)
     and   r.parentrepid = d.nodeid
     and   doctype in ('W')
  ORDER BY d.DOCVERSION DESC, d.DOCVERSION ASC
  FETCH  first 1 rows only;
     
 
begin
  isr$trace.stat('begin', 'begin', sCurrentName);
    
  for rGetExcelId in cGetExcelId loop
    isr$trace.debug('rGetExcelId.docid', rGetExcelId.docid,sCurrentName);
    oErrorObj := Stb$docks.copyDocToJob(rGetExcelId.docid, cnJobId);
    update STB$JOBTRAIL
           set documenttype = 'EXCEL_TEMPLATE',
               leadingdoc = null
         where jobid = cnJobId
           and documenttype = 'TARGET'
           and leadingdoc = 'T';
    commit;
  end loop;
  
  for rGetWordId in cGetWordId loop
    isr$trace.debug('rGetExcelId.docid', rGetWordId.docid,sCurrentName);
    oErrorObj := Stb$docks.copyDocToJob(rGetWordId.docid, cnJobId);
    update STB$JOBTRAIL
           set documenttype = 'WORD_TEMPLATE',
               leadingdoc = null
         where jobid = cnJobId
           and documenttype = 'TARGET'
           and leadingdoc = 'T';
    commit;
  end loop;

  isr$trace.stat('end', 'end',sCurrentName);
  return oErrorObj;

exception
  when others then
    sUserMsg := utd$msglib.getmsg ('exUnexpectedError', stb$security.getCurrentLanguage,  csP1 => sqlerrm(sqlcode));
    sImplMsg := sCurrentName || rtrim(': '||sImplMsg,': ');
    sDevMsg  := dbms_utility.format_error_stack ||CHR(10)|| dbms_utility.format_error_backtrace;
    oErrorObj.handleError ( Stb$typedef.cnSeverityCritical, sUserMsg, sImplMsg, sDevMsg );
    Stb$job.markDBJobAsBroken(cnJobId, sIp, sPort, oErrorObj.sErrorCode, oErrorObj.sErrorMessage, oErrorObj.sSeverityCode);
    return oErrorObj;
end copyDocumentsIntoJobForFinal;

--==**********************************************************************************************************************************************==--
FUNCTION adjustRawDatFileForWord(cnJobId IN NUMBER, cxXml IN OUT XMLTYPE) RETURN STB$OERROR$RECORD IS
  sCurrentName constant  varchar2(100) := $$PLSQL_UNIT||'.adjustRawDatFileForWord';
  oErrorObj               STB$OERROR$RECORD :=STB$OERROR$RECORD();  
  sOutputId VARCHAR2(50);
  sFileId   VARCHAR2(50);
  xParamData   XMLTYPE;
  xRawData   XMLTYPE;
  clRawData    clob;
  
  CURSOR cGetWordParams IS
    SELECT rtp.parametername, rp.parametervalue, rtp.orderno
      FROM stb$reportparameter rp
         , stb$reptypeparameter rtp
         , stb$report r
          WHERE r.repid = STB$JOB.getJobParameter ('REPID', cnJobId)
            AND rtp.xml = 'T'
            AND rtp.reporttypeid = r.reporttypeid
            AND rtp.repparameter in ('W', 'A')
            AND rp.repid = r.repid
            AND rp.parametername = 'W_'||rtp.parametername
         ORDER BY orderno;
  
begin
  isr$trace.stat('begin','begin', sCurrentName);
  SELECT STB$JOB.getJobParameter ('OUTPUTID', cnJobId), STB$JOB.getJobParameter ('FILTER_MASTERTEMPLATE_WORD', cnJobid)
    INTO sOutputId, sFileId
    FROM DUAL;
  if sFileId = 'PREV_WORD_DOC' then
    SELECT  f.fileid
     INTO sFileId
     FROM isr$file f, isr$output$file iof
    WHERE usage IN ('MASTERTEMPLATE_WORD')
      AND iof.fileid = f.fileid
      AND iof.outputid = sOutputId;
  end if;
  
  oErrorObj := isr$rawdata.adjustRawDatFile(cnJobId, cxXml, sOutputId, sFileId);
  
  SELECT jt.Textfile
   INTO clRawData
   FROM STB$JOBTRAIL jt
  WHERE jt.jobid = cnJobID
    AND jt.documenttype = 'ROHDATEN';
   
  xRawData := ISR$XML.clobToXML(clRawData);
   
  FOR rParam IN cGetWordParams LOOP
    isr$XML.deleteNode(xRawData, '/*/attributes/reportparameter/*[name()="'||lower (rParam.parametername)||'"]');
  END LOOP;
  
  SELECT XMLELEMENT( "reportparameter", XMLAGG (
           XMLELEMENT (evalname (LOWER (parametername)), parametervalue)
        ))
   into xParamData
   FROM (SELECT rtp.parametername, rp.parametervalue, rtp.orderno
           FROM stb$reportparameter rp
              , stb$reptypeparameter rtp
              , stb$report r
          WHERE r.repid = STB$JOB.getJobParameter ('REPID', cnJobId)
            AND rtp.xml = 'T'
            AND rtp.reporttypeid = r.reporttypeid
            AND rtp.repparameter in ('W', 'A')
            AND rp.repid = r.repid
            AND rp.parametername = 'W_'||rtp.parametername
         ORDER BY orderno);
         
  --ISR$XML.deleteNode(xRawData, '/*/attributes/reportparameter');
  isr$trace.Debug('reportparameter.xml','reportparameter.xml', sCurrentName, xParamData);
  ISR$XML.InsertChildXMLFragment(xRawData, '/*/attributes', ISR$XML.XMLToClob(xParamData)); 
   
  UPDATE STB$JOBTRAIL jt
     SET jt.Textfile = ISR$XML.XMLToClob(xRawData)
   WHERE jt.jobid = cnJobID
     AND jt.documenttype = 'ROHDATEN';     
  commit;
     
  isr$trace.stat('end','end', sCurrentName);
  return oErrorObj;
  EXCEPTION
  WHEN OTHERS THEN
    oErrorObj.handleError ( Stb$typedef.cnSeverityCritical,  utd$msglib.getmsg ('ERROR', stb$security.getCurrentLanguage) || SQLCODE, sCurrentName,
       dbms_utility.format_error_stack ||chr(10)||chr(13)|| dbms_utility.format_error_backtrace);
    RETURN oErrorObj;
end adjustRawDatFileForWord;

--****************************************************************************************************************************
FUNCTION copyUnprotectDocIntoJobtrail(cnJobid IN NUMBER, cxXml IN OUT XMLTYPE) RETURN STB$OERROR$RECORD
IS
  oError          STB$OERROR$RECORD := STB$OERROR$RECORD();
  sCurrentName    constant varchar2(100) := $$PLSQL_UNIT||'.copyUnprotectDocIntoJobtrail (JobID='||cnJobId||')';
  sMsg            varchar2(4000);
  nDocId          STB$DOCTRAIL.DOCID%TYPE;
  sPassword       STB$REPORT.PASSWORD%TYPE;

BEGIN
  isr$trace.stat('begin','begin',sCurrentName);

  nDocId := STB$JOB.getJobParameter('DOCID',cnJobId);

  oError := Stb$docks.getPasswordFromDoc(nDocId, sPassword);
  isr$trace.debug ('sPassword',  nDocId|| '; ' ||sPassword, sCurrentName);
  IF sPassword is not null THEN
    oError := stb$action.saveWordDocument(cnJobId, nDocId, sPassword);
  ELSE
   oError := Stb$docks.copyDocToJob(nDocId, cnJobId);
  END IF;
  
  isr$trace.debug('stb$object.getCurrentSrNumber',  stb$object.getCurrentSrNumber, sCurrentName);
  STB$OBJECT.setCurrentRepid(STB$JOB.getJobParameter('REPID',cnJobId));
  STB$OBJECT.setCurrentDocid(STB$JOB.getJobParameter('DOCID',cnJobId));
  oError := isr$reporttype$package.createSrAudit (stb$object.getCurrentSrNumber, STB$JOB.getJobParameter('COMMENT',cnJobId));

  isr$trace.stat('end','end',sCurrentName);
  RETURN oError;

EXCEPTION
WHEN OTHERS THEN
  sMsg := utd$msglib.getmsg ('exUnexpectedError', stb$security.getCurrentLanguage,  sqlerrm(sqlcode));
  oError.handleError ( Stb$typedef.cnSeverityCritical, sMsg, sCurrentName, dbms_utility.format_error_stack ||chr(10)|| dbms_utility.format_error_backtrace );
  Stb$job.markDBJobAsBroken(cnJobId, sIp, sPort, oError.sErrorCode, oError.sErrorMessage, oError.sSeverityCode);
  RETURN oError;
END copyUnprotectDocIntoJobtrail;


--****************************************************************************************************************************
function copyStartFilesIntoJobtrail( cnJobid in     number,
                                     cxXml   in out xmltype)
  return STB$OERROR$RECORD
is
  sCurrentName   constant varchar2(100) := $$PLSQL_UNIT||'.copyStartFilesIntoJobtrail (JobID='||cnJobId||')';
  sUserMsg               varchar2(4000);
  sDevMsg                varchar2(4000);
  sImplMsg               varchar2(4000);
  oErrorObj               STB$OERROR$RECORD :=STB$OERROR$RECORD();

  cursor cGetId is
    select f.fileid, f.BLOBFLAG
      from isr$FILE f, isr$OUTPUT$FILE iof
     where iof.outputid = STB$JOB.getJobParameter ('OUTPUTID', cnJobid)
       and iof.fileid = f.fileid
       and f.USAGE = 'START';


begin
  isr$trace.stat('begin', 'Function '||sCurrentName||' started.', sCurrentName);

 for rGetFiles in cGetId loop
    isr$trace.debug('status file','rGetFiles.fileid: '||rGetFiles.fileid||'  rGetFiles.blobFlag: '||rGetFiles.blobFlag, sCurrentName);
    oErrorObj := Stb$docks.copyFileToJob(rGetFiles.fileid,cnJobId,rGetFiles.blobflag);
 end loop;
 
  isr$trace.stat('end', 'Function '||sCurrentName||' finished',sCurrentName);
  return oErrorObj;

exception
  when others then
    sUserMsg := utd$msglib.getmsg ('exUnexpectedError', stb$security.getCurrentLanguage,  csP1 => sqlerrm(sqlcode));
    sImplMsg := sCurrentName || rtrim(': '||sImplMsg,': ');
    sDevMsg  := dbms_utility.format_error_stack ||chr(10)||chr(13)|| dbms_utility.format_error_backtrace;
    oErrorObj.handleError ( Stb$typedef.cnSeverityCritical, sUserMsg, sImplMsg, sDevMsg );
    Stb$job.markDBJobAsBroken(cnJobId, sIp, sPort, oErrorObj.sErrorCode, oErrorObj.sErrorMessage, oErrorObj.sSeverityCode);
    return oErrorObj;
END copyStartFilesIntoJobtrail;

END isr$excel$action;