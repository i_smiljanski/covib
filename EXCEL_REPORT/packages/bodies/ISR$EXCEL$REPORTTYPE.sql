CREATE OR REPLACE package body isr$excel$reporttype as

--*********local functions**************************************************************************

function isWordReportExists(cnRepId in number) return varchar2 is
  sCurrentName        constant varchar2(200) := $$PLSQL_UNIT||'.isWordReportExists('||cnRepId||')';
  cIsExists   varchar2(1) := 'F';
  cursor cGetWordRepExists is
    select 'T'  from stb$doctrail where doctype = 'W' and nodeid = cnRepId
    union
    select 'T'  from isr$report$file where filename like '%.HTM' and repid = cnRepId;
begin
  isr$trace.stat('begin','begin', sCurrentName);
   open cGetWordRepExists;
   fetch cGetWordRepExists into cIsExists;
   close cGetWordRepExists;  
  isr$trace.stat('end', cIsExists, sCurrentName);
  return cIsExists;
end isWordReportExists;

function isWordDocumentExists(cnRepId in number) return varchar2 is
  sCurrentName        constant varchar2(200) := $$PLSQL_UNIT||'.isWordDocumentExists('||cnRepId||')';
  cIsExists   varchar2(1) := 'F';
  cursor cGetWordDocExists is
    select 'T'  from stb$doctrail where doctype = 'W' and nodeid = cnRepId;
begin
  isr$trace.stat('begin','begin', sCurrentName);
   open cGetWordDocExists;
   fetch cGetWordDocExists into cIsExists;
   close cGetWordDocExists;  
  isr$trace.stat('end', cIsExists, sCurrentName);
  return cIsExists;
end isWordDocumentExists;
--******************************************************************************
PROCEDURE getCustomIcon (olNodeList in out STB$TREENODELIST, sNodeType in varchar2) is
begin
   execute immediate
    'BEGIN
       '|| STB$UTIL.getCurrentCustomPackage  ||'.getCustomIcon(:1, :2);
     END;'
  using in out olNodeList, in sNodeType;
end getCustomIcon;

--******************************************************************************
function getNodeChilds (oSelectedNode in stb$treenode$record, olChildNodes out stb$treenodelist) return STB$OERROR$RECORD
is
  sCurrentName        constant varchar2(200) := $$PLSQL_UNIT||'.getNodeChilds('||oSelectedNode.sNodeType||')';
  oErrorObj           stb$oerror$record := STB$OERROR$RECORD();
  sMsg   varchar2(4000);
  sSql                varchar2 (4000);
  exChildDisplayError exception;
  

begin
  isr$trace.stat('begin','begin', sCurrentName);
  olChildNodes := stb$treenodelist ();
   
  if oSelectedNode.sNodeType = 'REPORT' and isWordReportExists(STB$OBJECT.getCurrentNodeId) = 'T' then
    sSql :=
             'BEGIN 
             select STB$TREENODE$RECORD (SDISPLAY, SPACKAGETYPE, sNodeType, SNODEID, SICON) 
             BULK COLLECT into :1 
             FROM ( select ''Word'' SDISPLAY,
                     '''|| $$PLSQL_UNIT || '''  SPACKAGETYPE,
                     ''REPORT_WORD''                  sNodeType,
                     r.REPID                   SNODEID,
                     ''REPORT_WORD''            SICON
                from stb$report r
               where r.repid = '
           || STB$OBJECT.getCurrentNodeId || '); ' || ' END;';

    begin
      isr$trace.debug ('sSql',  sSql, sCurrentName);
      execute immediate sSql using in out olChildNodes;

      isr$trace.debug ('olChildNodes.COUNT()', olChildNodes.count (), sCurrentName);

      getCustomIcon(olChildNodes, 'REPORT');

      for i in 1 .. olChildNodes.count () loop
        olChildNodes (i).tlovaluelist := STB$OBJECT.getCurrentValueList;
        STB$OBJECT.setDirectory(olChildNodes (i));
      end loop;
      isr$trace.debug('object','olChildNodes',sCurrentName,olChildNodes);
    exception when others then
      isr$trace.error ('sSql '||sqlcode,  sSql, sCurrentName);
      raise exChildDisplayError;
    end;

  else
      oErrorObj := isr$reporttype$package.getNodeChilds(oSelectedNode, olChildNodes);
  end if;
  isr$trace.stat('end','end', sCurrentName);
  return oErrorObj;

exception
  when others then
    sMsg := utd$msglib.getmsg ('exUnexpectedError', stb$security.getCurrentlanguage,  csP1 => sqlerrm(sqlcode));
    oErrorObj.handleError ( Stb$typedef.cnSeverityCritical, sMsg, sCurrentName, dbms_utility.format_error_stack ||chr(10)||chr(13)|| dbms_utility.format_error_backtrace );
    return oErrorObj;
end getNodeChilds;

  
--******************************************************************************
function getDocumentType(doctype in varchar2) return varchar2 is
  sDocType  varchar2(255);
begin
  sDocType := case when doctype = 'C' then 'COMMONDOC' 
              when doctype = 'D' then 'DOCUMENT_EXCEL' 
              when doctype = 'X' then 'RAWDATA' 
              when doctype = 'L' then 'LINK' 
              when doctype = 'S' then 'REMOTESNAPSHOT' 
              when doctype = 'W' then 'DOCUMENT_WORD' 
              else 'DOCUMENT' 
              end; 
  return sDocType;
end getDocumentType;


--******************************************************************************
function getDefaultAction(doctype in varchar2) return varchar2 is
  sDefaultAction  varchar2(255);
begin
  sDefaultAction := case when doctype = 'C' then 'CAN_DISPLAY_COMMON_DOC' 
                    when doctype = 'D' then 'CAN_DISPLAY_REPORT' 
                    when doctype = 'X' then 'CAN_DISPLAY_RAWDATA' 
                    when doctype = 'L' then 'CAN_DISPLAY_LINK' 
                    when doctype = 'S' then 'CAN_DISPLAY_REMOTESNAPSHOT' 
                    else 'CAN_DISPLAY_REPORT' 
                    end; 
  return sDefaultAction;
end getDefaultAction;

--******************************************************************************
function getIconName(doctype in varchar2, sJobFlag in varchar2, sdoc_definition in varchar2, sCheckedOut in varchar2) return varchar2 is
  sIcon  varchar2(255);
  sCurrentName    constant varchar2(200) := $$PLSQL_UNIT||'.getIconName';
begin
  --isr$trace.stat('begin','begin', sCurrentName);
  sIcon :=  
    doctype || '_' 
            ||  case when sdoc_definition is not null and sJobFlag is null and sCheckedOut = 'F' and doctype != 'COMMONDOC' then sdoc_definition
                     when sdoc_definition is not null and sJobFlag is null and sCheckedOut = 'F' and doctype = 'COMMONDOC' then 'document_pdf' 
                end 
            || case when sJobFlag is not null then sJobFlag 
               else case when sCheckedOut = 'T' then sCheckedOut end 
               end;
  --isr$trace.stat('end',sIcon, sCurrentName);
  return sIcon;
end getIconName;

--******************************************************************************
function fillNodeList (csTreeNodeStmt in varchar2, csPropertyListStmt in varchar2, 
              tloValueList in ISR$VALUE$LIST, tloPropertyList in  STB$PROPERTY$LIST, olNodeList in out stb$treenodelist) return stb$oerror$record
is
  sCurrentName    constant varchar2(200) := $$PLSQL_UNIT||'.fillNodeList';
  oErrorObj                     stb$oerror$record := STB$OERROR$RECORD();
  type tNodeListValues is ref cursor;  
  cGetNodeListValues             tNodeListValues;
  sDisplay          varchar2(255);
  sPackageType      VARCHAR2(255);
  sNodeType         VARCHAR2(255);
  sNodeId           VARCHAR2(4000);
  sIcon             VARCHAR2(500);
  sDefaultAction    VARCHAR2(255);
  sTooltip          VARCHAR2(4000);
  nDisplayType      NUMBER ;
  olParameter       stb$property$list;
  cPropertyListStmt   varchar2(4000);
begin
  isr$trace.stat('begin','begin', sCurrentName);
  isr$trace.debug ('csTreeNodeStmt', '-> logclob', sCurrentName, csTreeNodeStmt);
  open cGetNodeListValues for csTreeNodeStmt;

  loop
  
    fetch cGetNodeListValues
    into  sDisplay, sPackageType, sNodeType, sNodeId, sIcon, sDefaultAction, sTooltip, nDisplayType;
    exit when cGetNodeListValues%notfound;
    
    olNodeList.extend;    
    olNodeList (olNodeList.count()) := STB$TREENODE$RECORD(sDisplay, 
                                                           sPackageType, 
                                                           sNodeType, 
                                                           sNodeId, 
                                                           sIcon, 
                                                           sDefaultAction, 
                                                           sTooltip, 
                                                           tloPropertyList, 
                                                           tloValueList, 
                                                           nDisplayType);
    STB$OBJECT.setDirectory(olNodeList (olNodeList.count()));
    
    cPropertyListStmt := replace(csPropertyListStmt, '%nodetype%', olnodelist (olNodeList.count()).snodetype) || sNodeId;
    isr$trace.debug ('cPropertyListStmt', '-> logclob', sCurrentName, cPropertyListStmt);
    oErrorObj := STB$UTIL.GETCOLPROPERTYLIST(cPropertyListStmt, olParameter);
    
    isr$trace.debug ('olParameter.count()', olParameter.count(), sCurrentName);
    olNodeList (olNodeList.count()).tlopropertylist := olParameter;
  end loop;
  isr$trace.stat('end','end', sCurrentName);
  return oErrorObj;
end fillNodeList; 

--******************************************************************************
function getNodeList (oSelectedNode in stb$treenode$record, olNodeList out stb$treenodelist)
  return stb$oerror$record
is
  sCurrentName                  constant varchar2(200) := $$PLSQL_UNIT||'.getNodeList('||oSelectedNode.sNodeType||')';
  oErrorObj                     stb$oerror$record := STB$OERROR$RECORD();
  sMsg   varchar2(4000);
  olParameter                   stb$property$list;
  sRowStmt                 varchar2 (4000);
  sPropertyListStmt             varchar2 (4000);
  nRepid                        STB$REPORT.REPID%type;
  olValueList         ISR$VALUE$LIST;
    
  olNodeList1   stb$treenodelist := stb$treenodelist();
  cursor cGetRepStatus(cnRepid in number) is
    select status
      from stb$report
     where repid = cnRepid;

  sStatus                       STB$REPORT.STATUS%type;
begin
  isr$trace.stat('begin','begin', sCurrentName);
  olNodeList := STB$TREENODELIST ();
  
  olValueList := oSelectedNode.tloValueList;
  FOR i IN 1..olValueList.count() LOOP
    IF olValueList(i).sParameterName = 'REPORT' THEN
      nRepid := olValueList(i).sParameterValue;
    END IF;
  END LOOP;
  
  case
    --=========================REPORT=========================
    when oSelectedNode.sNodeType = 'REPORT' then
      nRepid := to_number (STB$OBJECT.getCurrentNodeId);
      if nRepId is not null then  --ISRC-1245
        isr$trace.debug ('nRepid',  nRepid, sCurrentName);
           
        open cGetRepStatus(nRepid);
        fetch cGetRepStatus into sStatus;
        close cGetRepStatus;
        isr$trace.debug ('sStatus',  sStatus, sCurrentName);
        if sStatus = 999 then
          return oErrorObj;
        end if;
    
        sRowStmt := 'select 
                          filename sDisplay, 
                          ''ISR$EXCEL$REPORTTYPE'' sPackageType, 
                          isr$excel$reporttype.getDocumentType(doctype) sNodeType, 
                          docid sNodeId, 
                          isr$excel$reporttype.getIconName(isr$excel$reporttype.getDocumentType(doctype), job_flag, doc_definition, checkedout) sIcon, 
                          isr$excel$reporttype.getDefaultAction(doctype)  sDefaultAction, 
                          null sTooltip, 
                          null nDisplayType
                          from STB$DOCTRAIL
                          WHERE nodeid = TO_CHAR('|| nRepid || ')
                          AND doctype != ''W''
                          and nodetype = ''REPORT''
                          order by 3, doc_definition, docid, versiondisplayed';

        sPropertyListStmt := 'SELECT 
                                    -- isr$excel$reporttype.getDefaultAction(doctype)  sDefaultAction, 
                                     TO_CHAR (docid) docid,
                                     versiondisplayed,
                                     isr$excel$reporttype.getDocumentType(doctype) doctype,
                                     doc_definition,
                                     checkedout,
                                     modifiedby,
                                     modifiedon
                              FROM   stb$doctrail
                              WHERE  docid = ';
                                                    
        oErrorObj := fillNodeList(sRowStmt, sPropertyListStmt, oSelectedNode.tloValueList, null, olNodeList);
        
        getCustomIcon(olNodeList, 'DOCUMENT');
              
        IF nvl(STB$USERSECURITY.getUserParameter (stb$security.getCurrentUser, 'SHOW_REPORT_FILES'), 'F') = 'T' THEN
                  
          sRowStmt := 'select 
                          filename sDisplay, 
                          ''ISR$REPORTTYPE$PACKAGE'' sPackageType, 
                          UPPER(substr(filename, instr(filename, ''.'', -1)+1))||''_REPORT_FILE'' sNodeType, 
                          entryid sNodeId, 
                          UPPER(substr(filename, instr(filename, ''.'', -1)+1))||''_REPORT_FILE'' sIcon, 
                          ''CAN_DISPLAY_COMMON_DOC''  sDefaultAction, 
                          null sTooltip, 
                          null nDisplayType
                          from isr$report$file 
                          where (filename like ''%.html'' or filename like ''%.csv'') and repid = ' || nRepid || ' order by filename';

          sPropertyListStmt := 'SELECT 
                             --   ''CAN_DISPLAY_COMMON_DOC''  sDefaultAction, 
                                entryid docid,
                                '''' versiondisplayed,
                                ''%nodetype%'' doctype,
                                doc_definition,
                                ''F'' checkedout,
                                modifiedby,
                                modifiedon
                                FROM   isr$report$file
                                WHERE  (filename like ''%.html'' or filename like ''%.csv'') and entryid = ';
                    
          oErrorObj := fillNodeList(sRowStmt, sPropertyListStmt, oSelectedNode.tloValueList, null, olNodeList); 
        END IF;
        
        if  isWordReportExists(nRepid) = 'T' then
          oErrorObj := getNodeChilds (oSelectedNode, olNodeList1);
          oErrorObj := STB$UTIL.GetColPropertyList('SELECT null docid, null versiondisplayed, null doctype, null doc_definition, null checkedout, null modifiedby, null modifiedon FROM dual', olParameter);
          isr$trace.debug ('olParameter.count()', olParameter.count(), sCurrentName);

          olNodeList.extend;
          olNodeList (olNodeList.count()) := olNodeList1(1);
          olNodeList (olNodeList.count()).tlopropertylist := olParameter;      
        end if;         
      end if;

    --=========================REPORT_WORD=========================
   when oSelectedNode.sNodeType = 'REPORT_WORD' then

      isr$trace.debug ('nRepid',  nRepid, sCurrentName);
      
      sRowStmt := 'select 
                filename sDisplay, 
                ''ISR$EXCEL$REPORTTYPE'' sPackageType, 
                isr$excel$reporttype.getDocumentType(doctype) sNodeType, 
                docid sNodeId, 
                isr$excel$reporttype.getIconName(isr$excel$reporttype.getDocumentType(doctype), job_flag, doc_definition, checkedout) sIcon, 
                isr$excel$reporttype.getDefaultAction(doctype)  sDefaultAction, 
                null sTooltip, 
                null nDisplayType
                from STB$DOCTRAIL
                WHERE nodeid = TO_CHAR('|| nRepid || ')
                AND nodetype = ''REPORT_WORD''
                order by 3, doc_definition, docid, versiondisplayed';

      sPropertyListStmt := 'SELECT TO_CHAR (docid) docid,
                 versiondisplayed,
                 isr$excel$reporttype.getDocumentType(doctype) doctype,
                 doc_definition,
                 checkedout,
                 modifiedby,
                 modifiedon
          FROM   stb$doctrail
          WHERE  docid = ';
                                
      oErrorObj := fillNodeList(sRowStmt, sPropertyListStmt, oSelectedNode.tloValueList, null, olNodeList);

      getCustomIcon(olNodeList, 'DOCUMENT');

      IF nvl(STB$USERSECURITY.getUserParameter (stb$security.getCurrentUser, 'SHOW_REPORT_FILES'), 'F') = 'T' THEN

          sRowStmt := 'select 
            filename sDisplay, 
            ''ISR$REPORTTYPE$PACKAGE'' sPackageType, 
            UPPER(substr(filename, instr(filename, ''.'', -1)+1))||''_REPORT_FILE'' sNodeType, 
            entryid sNodeId, 
            UPPER(substr(filename, instr(filename, ''.'', -1)+1))||''_REPORT_FILE'' sIcon, 
            ''CAN_DISPLAY_COMMON_DOC''  sDefaultAction, 
            null sTooltip, 
            null nDisplayType
            from isr$report$file 
            where (filename like ''%.HTM'' or filename like ''%.csv'') and repid = ' || nRepid || ' order by filename';

          sPropertyListStmt := 'SELECT entryid docid,
                  '''' versiondisplayed,
                  ''%nodetype%'' doctype,
                  doc_definition,
                  ''F'' checkedout,
                  modifiedby,
                  modifiedon
                  FROM   isr$report$file
                  WHERE  (filename like ''%.HTM'' or filename like ''%.csv'') and entryid = ';

          oErrorObj := fillNodeList(sRowStmt, sPropertyListStmt, oSelectedNode.tloValueList, null, olNodeList);          

        END IF;

      --=========================NOT DEFINED=========================
    else
      oErrorObj := isr$reporttype$package.getNodeList (oSelectedNode, olNodeList);
  end case;

  isr$trace.stat('end','end', sCurrentName);
  return oErrorObj;
exception
  when others then
    rollback;
    sMsg := utd$msglib.getmsg ('exUnexpectedError', stb$security.getCurrentlanguage,  csP1 => sqlerrm(sqlcode));
    oErrorObj.handleError ( Stb$typedef.cnSeverityCritical, sMsg, sCurrentName, dbms_utility.format_error_stack ||chr(10)||chr(13)|| dbms_utility.format_error_backtrace );
    return oErrorObj;
end getnodelist;

--******************************************************************************
function setCurrentNode (oSelectedNode in stb$treenode$record) return stb$oerror$record is
  sCurrentName                  constant varchar2(200) := $$PLSQL_UNIT||'.setCurrentNode('||oSelectedNode.sNodeType||')';
  oErrorObj                     stb$oerror$record := STB$OERROR$RECORD();

  nReporttypeid   STB$REPORT.REPORTTYPEID%type;
  sSrNumber       STB$REPORT.SRNUMBER%type;
  nTemplate       STB$REPORT.TEMPLATE%type;
  nNodeId         number;
  cursor cGetReportInfo (csTitle varchar2) is
    select reporttypeid, srnumber
      from stb$report
     where title = csTitle;

  cursor cGetReportTemplate (cnRepID number) is
    select template
      from stb$report
     where repid = cnRepID;
begin
  isr$trace.stat('begin','begin', sCurrentName);
  isr$trace.debug('oSelectedNode','s. logclob', sCurrentName, oSelectedNode);
  stb$object.destroyOldValues;
  stb$object.setCurrentNode (oSelectedNode);
  stb$object.setCurrentReptypePackage ($$PLSQL_UNIT);
  case
    --=========================REPORT_WORD=========================
  when oSelectedNode.sNodeType = 'REPORT_WORD' then
    open cGetReportInfo(stb$object.getNodeIdForNodeType('TITLE'));
    fetch cGetReportInfo into nReporttypeid, sSrNumber;
    close cGetReportInfo;
    open cGetReportTemplate( to_number(stb$object.getNodeIdForNodeType('REPORT')));
    fetch cGetReportTemplate into nTemplate;
    close cGetReportTemplate;
    stb$object.setCurrentReporttypeid (nReporttypeid);
    stb$object.setCurrentSrnumber (sSrNumber);
    stb$object.setCurrentTitle (stb$object.getNodeIdForNodeType('TITLE'));
    stb$object.setCurrentRepid (stb$object.getNodeIdForNodeType('REPORT'));
    stb$object.setCurrentTemplate (nTemplate);
    --=========================DOCUMENT_WORD=========================
  when oSelectedNode.sNodeType in ('DOCUMENT_WORD', 'DOCUMENT_EXCEL') then
     BEGIN
        OPEN cGetReportInfo(stb$object.getNodeIdForNodeType('TITLE'));
        FETCH cGetReportInfo INTO nReporttypeid, sSrNumber;
        CLOSE cGetReportInfo;
        OPEN cGetReportTemplate( TO_NUMBER(stb$object.getNodeIdForNodeType('REPORT')));
        FETCH cGetReportTemplate INTO nTemplate;
        CLOSE cGetReportTemplate;
        stb$object.setCurrentreporttypeid (nReporttypeid);
        stb$object.setCurrentsrnumber (sSrNumber);
        stb$object.setCurrenttitle (stb$object.getNodeIdForNodeType('TITLE'));
        stb$object.setCurrentrepid (stb$object.getNodeIdForNodeType('REPORT'));
        stb$object.setCurrenttemplate (nTemplate);
        nNodeId := stb$object.getNodeIdForNodeType(oSelectedNode.sNodeType);
        isr$trace.debug('nNodeId', nNodeId, sCurrentName);
        stb$object.setCurrentDocid (nNodeId);
      EXCEPTION
        WHEN OTHERS THEN
          isr$trace.info('error',sqlerrm, sCurrentName);
          stb$object.setCurrentDocid (nNodeId);
      END;
  else
    oErrorObj := isr$reporttype$package.setCurrentNode(oSelectedNode);
  end case;
  isr$trace.stat('end','end', sCurrentName);
  return oErrorObj;
end setCurrentNode;

--******************************************************************************
function setMenuAccess (sParameter   in varchar2, sMenuAllowed out varchar2) return stb$oerror$record is
   sCurrentName                  constant varchar2(200) := $$PLSQL_UNIT||'.setMenuAccess('||sParameter||')';
   oErrorObj                     stb$oerror$record := STB$OERROR$RECORD();
   sDocDefinition                STB$DOCTRAIL.DOC_DEFINITION%TYPE;
   sLockMe                       varchar2(1);
   sCurrentNodeType              STB$CONTEXTMENU.NodeType%type;

  cursor cCheckExcelExists (cnrepid in stb$report.repid%type) is
    select 'T'
    from   stb$doctrail T
    where  T.nodeid = cnrepid
    and    T.doctype = 'D';

  sExists    varchar2(1);

begin
  isr$trace.stat('begin','begin', sCurrentName);
  sCurrentNodeType := stb$object.getCurrentNodeType;
  isr$trace.debug ('sCurrentNodeType',  sCurrentNodeType, sCurrentName);
  
  case
  when sParameter in ('CAN_CREATE_WORD_DOCUMENT', 'CAN_MODIFY_WORD_OUTPUTOPTIONS') then
      
    -- check if the report is locked by the user himself
    oErrorObj := stb$security.checkLockedByMyself (STB$SECURITY.getCurrentSession, sLockme);
    isr$trace.debug ('sLockMe',  'sLockMe: ' || slockme, sCurrentName);
    
    sDocDefinition := STB$DOCKS.getDocDefType (Stb$object.GetCurrentDocId, Stb$object.getCurrentRepId);
    isr$trace.info('sDocDefinition', sDocDefinition, sCurrentName);
    
     -- check of the current combination
    isr$trace.debug ('before checkcontextrep', 'sMenuAllowed: '||sMenuAllowed, sCurrentName);
    oErrorObj := stb$security.checkContextRep (sparameter, sCurrentNodeType, sDocDefinition, sMenuAllowed, sLockme);
    isr$trace.debug ('after checkcontextrep', 'sMenuAllowed: '||sMenuAllowed, sCurrentName);

    IF sMenuAllowed = 'T' THEN
      -- check the status of the children nodes
      oErrorObj := stb$security.checkChildrenStatus (sparameter, NVL(sCurrentNodeType, 'REPORT'), sDocDefinition, sMenuAllowed);
      isr$trace.debug ('after checkchildrenstatus', 'sMenuAllowed '||sMenuAllowed, sCurrentName);

      IF sMenuAllowed = 'F' THEN
        isr$trace.stat('end', 'sParameter: '||sparameter||'  sMenuAllowed: '||sMenuAllowed  , sCurrentName);
        RETURN oErrorObj;
      END IF;
    ELSE
      isr$trace.stat('end', 'sParameter: '||sparameter||'  sMenuAllowed: '||sMenuAllowed  , sCurrentName);
      RETURN oErrorObj;                                                                                                                                                      -- not the right context
    END IF;
    
  when sParameter in ('CAN_FINALIZE_EXCEL_REPORT') then
   sExists := isWordDocumentExists(stb$object.getCurrentRepid);
   if sExists = 'T' then
     oErrorObj := isr$reporttype$package.setMenuAccess('CAN_FINALIZE_REPORT', sMenuAllowed);
   else
    sMenuAllowed := 'F';
   end if;
   
  else
    oErrorObj := isr$reporttype$package.setMenuAccess(sParameter, sMenuAllowed);
  end case;
  isr$trace.stat('end', 'sParameter: '||sParameter||'  sMenuAllowed: '||sMenuAllowed  , sCurrentName);
  return oErrorObj;
exception
  when others then
    oErrorObj.handleError ( Stb$typedef.cnSeverityCritical,  utd$msglib.getmsg ('ERROR', stb$security.getCurrentLanguage) || sqlcode, sCurrentName,
       dbms_utility.format_error_stack ||chr(10)||chr(13)|| dbms_utility.format_error_backtrace);

    return oErrorObj;
end setMenuAccess;

--******************************************************************************
function getContextMenu( nMenu           in  number,
                         olMenuEntryList out stb$menuentry$list)
  return stb$oerror$record is
  sCurrentName                  constant varchar2(200) := $$PLSQL_UNIT||'.getContextMenu('||nMenu||')';
   oErrorObj                     stb$oerror$record := STB$OERROR$RECORD();
begin
  isr$trace.stat('begin','begin', sCurrentName);
  oErrorObj := isr$reporttype$package.getContextMenu(nMenu, olMenuEntryList);
  isr$trace.stat('end','olMenuEntryList.count: ' || olMenuEntryList.count, sCurrentName);
  return oErrorObj;
end getContextMenu;


--************************************************************************************************************
FUNCTION getOutputOption (olParameter IN OUT STB$PROPERTY$LIST, cnCurrentFormNumber in integer default null, csRepParameterValue in varchar2 default 'T')
  RETURN stb$oerror$record
IS
  sCurrentName                  CONSTANT VARCHAR2(100) := $$PLSQL_UNIT||'.getOutputOption';
  oerrorobj                     stb$oerror$record := STB$OERROR$RECORD();
  oSelectionList                ISR$TLRSELECTION$LIST := ISR$TLRSELECTION$LIST();

  nDocid                        NUMBER;
  nRepid                        NUMBER;
  ncounter                      NUMBER;

  tloPropertylist1              STB$PROPERTY$LIST := STB$PROPERTY$LIST();
  tloPropertylist2              STB$PROPERTY$LIST := STB$PROPERTY$LIST();
  tloPropertylist3              STB$PROPERTY$LIST := STB$PROPERTY$LIST();

  -- Cursor
  CURSOR cGetParameter IS
    SELECT rt.parameterName
         , utd$msglib.getmsg (TRIM (SUBSTR (rt.description, 1, DECODE (INSTR (rt.description, '(') - 1, -1, LENGTH (rt.description), INSTR (rt.description, '(') - 1)))
                            , stb$security.getCurrentLanguage)
              promptDisplay
         , NVL (rpa.parameterValue, rt.parameterValue) parametervalue
         , CASE
              WHEN rt.parametertype = 'STRING'
               AND rt.haslov = 'T'
               AND pv.msgkey IS NULL THEN
                 pv.display
              WHEN rt.parametertype = 'STRING'
               AND rt.haslov = 'T'
               AND pv.msgkey IS NOT NULL THEN
                 utd$msglib.getmsg (pv.msgkey, stb$object.getCurrentReportlanguage, pv.plugin)
              ELSE
                 NVL (rpa.parameterValue, rt.parameterValue)
           END
              parameterDisplay
         , rt.parameterType
         , rt.editable
         , rt.haslov
         , rt.REQUIRED
         , TRIM (SUBSTR (rt.description, DECODE (INSTR (rt.description, '('), 0, LENGTH (rt.description) + 1, INSTR (rt.description, '(')))) sqlstatement
         , CASE WHEN STB$SECURITY.checkGroupRight (rt.parametername, rt.reporttypeid) = 'T' THEN rt.VISIBLE ELSE 'F' END visible
      FROM stb$reportparameter rpa, stb$reptypeparameter rt, isr$parameter$values pv
     WHERE rpa.repid(+) = stb$object.getCurrentRepid
       AND rpa.parametername(+) = case when rt.repparameter in ('W','A') then 'W_' || rt.parameterName else rt.parameterName end
       AND rt.reporttypeid = stb$object.getCurrentReporttypeid
       AND (rt.repparameter in('A', csRepParameterValue)
         OR parametertype IN ('GROUP'))
       AND rpa.parameterName = pv.entity(+)
       AND rpa.parametervalue = pv.VALUE(+)
    ORDER BY rt.orderno, rt.parameterName;
    
    CURSOR cGetHighestDoc(cnRepId in number, csDocType in varchar2) IS
    SELECT Max (docId) docId
      FROM STB$DOCTRAIL
     WHERE To_Char (nodeid) = cnRepId
       AND (doctype = csDocType);

BEGIN
  isr$trace.stat('begin','begin', sCurrentName);
  
  -- get the current Report id
  nrepid := stb$object.getCurrentRepid;
  
  oErrorObj := STB$UTIL.getPropertyList('CAN_RUN_REPORT_SERVER', tloPropertylist1);
  isr$trace.debug('tloPropertylist1', 's. logclob', sCurrentName, tloPropertylist1);

      
  IF isr$reporttype$package.getRenderingServerStatus = 'F' THEN
    FOR i IN 1..tloPropertylist1.COUNT() LOOP
      IF tloPropertylist1(i).sParameter = 'LOCAL' THEN
        tloPropertylist1(i).sValue := 'T';
        tloPropertylist1(i).sDisplay := 'T';
        tloPropertylist1(i).sEditable := 'F';
      END IF;
    END LOOP;
  END IF;
    
  IF STB$USERSECURITY.checkoutDirect(stb$security.getcurrentuser) = 'T'
  AND STB$UTIL.getreptypeparameter (stb$object.getCurrentReporttypeid, 'REQUIRES_VERSIONING') = 'T' THEN
    oErrorObj := STB$UTIL.getPropertyList('CAN_RUN_REPORT_CHECKOUT', tloPropertylist2);
  END IF;


  IF STB$SECURITY.checkGroupRight ('CAN_CONFIGURE_ORACLE_JOBS') = 'T'
       AND STB$OBJECT.getCurrentRepid is null THEN
    FOR i IN 1..tloPropertylist1.COUNT() LOOP
      IF tloPropertylist1(i).sParameter = 'LOCAL' THEN
        tloPropertylist1(i).sValue := 'F';
        tloPropertylist1(i).sDisplay := 'F';
        tloPropertylist1(i).sEditable := 'F';
      END IF;
    END LOOP;
      oErrorObj := STB$UTIL.getPropertyList('CAN_CONFIGURE_ORACLE_JOBS', tloPropertylist3);
  END IF;

                                        
  FOR i IN 1..tloPropertylist3.COUNT() LOOP
    olParameter.EXTEND;
    olParameter(olParameter.COUNT) := tloPropertylist3(i);
  END LOOP;

  FOR i IN 1..tloPropertylist1.COUNT() LOOP
    olParameter.EXTEND;
    olParameter(olParameter.COUNT) := tloPropertylist1(i);
    IF NVL(tloPropertylist1(i).sDisplayed, 'T') = 'T' THEN
      olParameter(olParameter.COUNT).sDisplayed := STB$USERSECURITY.getUserParameter (STB$SECURITY.getCurrentUser, 'CAN_RUN_REPORT_SERVER');
    END IF;
    IF STB$UTIL.getreptypeparameter (stb$object.getCurrentReporttypeid, 'REQUIRES_VERSIONING') = 'F'
      AND tloPropertylist1(i).sParameter = 'PARA_DISPLAYED_PDF_CREATION_ALLOWED' THEN
      olParameter(olParameter.COUNT).sDisplayed := 'F';
    END IF;
  END LOOP;

  FOR i IN 1..tloPropertylist2.COUNT() LOOP
    olParameter.EXTEND;
    olParameter(olParameter.COUNT) := tloPropertylist2(i);
  END LOOP;
    
  olParameter.EXTEND;
  olParameter(olParameter.COUNT) := STB$PROPERTY$RECORD('GROUP_WORD_OPTIONS', null, null, null, 'F', 'F', 'F', 'F', 'GROUP');
  
  nCounter := olParameter.COUNT;

  FOR rGetParameter IN cGetParameter LOOP
    ncounter := ncounter + 1;
    isr$trace.debug_all('ncounter', '' || ncounter, sCurrentName);
    olParameter.EXTEND;
    olParameter (ncounter) := STB$PROPERTY$RECORD(rGetParameter.parametername);
    IF trim(rGetParameter.sqlstatement) IS NOT NULL THEN
      EXECUTE IMMEDIATE rGetParameter.sqlstatement INTO olParameter (ncounter).spromptdisplay;
    END IF;
    olParameter (ncounter).spromptdisplay := rGetParameter.promptdisplay || olParameter (ncounter).spromptdisplay;
    isr$trace.debug_all('value', 'olParameter ('||ncounter||').spromptdisplay: '||olParameter(ncounter).spromptdisplay, sCurrentName );

    olParameter (ncounter).svalue := rGetParameter.parametervalue;
    isr$trace.debug(olParameter(ncounter).sParameter, 'olParameter ('||ncounter||').svalue: '||olParameter(ncounter).svalue, sCurrentName);

    olParameter (ncounter).seditable := rGetParameter.editable;
    olParameter (ncounter).TYPE := rGetParameter.parametertype;
    olParameter (ncounter).haslov := rGetParameter.haslov;
    olParameter (ncounter).srequired := rGetParameter.REQUIRED;
    olParameter (ncounter).sdisplayed := rGetParameter.VISIBLE;

    IF rGetParameter.hasLov = 'T' AND rGetParameter.parametertype != 'FILE' AND trim(olParameter (ncounter).svalue) IS NOT NULL THEN
      BEGIN
        oErrorObj := getLov (rGetParameter.parametername, olParameter (ncounter).svalue, oSelectionList);

        IF oSelectionList.first IS NOT NULL THEN
          FOR j IN 1..oSelectionList.count() LOOP
            IF oSelectionList(j).sSelected = 'T' THEN
              olParameter (olParameter.count()).sDisplay := oSelectionList(j).sDisplay;
            END IF;
          END LOOP;
          IF trim(olParameter(olParameter.count()).sValue) IS NULL
          AND oSelectionList.count() >= 1
          AND olParameter(olParameter.count()).sRequired = 'T' THEN
            olParameter(olParameter.count()).sValue := oSelectionList(1).sValue;
            olParameter(olParameter.count()).sDisplay := oSelectionList(1).sDisplay;
          END IF;
        -- no lov
        ELSE
          olParameter (olParameter.count()).sValue := null;
          olParameter (olParameter.count()).sDisplay := null;
        END IF;
      EXCEPTION WHEN OTHERS THEN
        isr$trace.error ('could not set value from lov',  'parameter '||rGetParameter.parametername||' value '||olParameter(ncounter).sValue, sCurrentName);
      END;
    ELSE
      olParameter (nCounter).sDisplay := rGetParameter.parameterDisplay;
    END IF;

    IF rGetParameter.parameterType = 'DATE' THEN
      olParameter (ncounter).format := STB$UTIL.getSystemParameter('JAVA_DATE_FORMAT');
      IF (instr(olParameter(ncounter).sValue,':exec:') IS NULL OR instr(olParameter(ncounter).sValue,':exec:') = 0)
      AND olParameter (ncounter).sValue IS NOT NULL THEN
        BEGIN
          olParameter (ncounter).sValue := to_char(to_date(olParameter (ncounter).sValue, STB$UTIL.getSystemParameter('DATEFORMAT')), STB$UTIL.getSystemParameter('DATEFORMAT'));
        EXCEPTION WHEN OTHERS THEN
          olParameter (ncounter).sValue := to_char(to_date(olParameter (ncounter).sValue, STB$UTIL.getSystemParameter('DATEFORMAT'), 'nls_date_language=german'), STB$UTIL.getSystemParameter('DATEFORMAT'));
        END;
        olParameter (ncounter).sDisplay := olParameter(ncounter).sValue;
      END IF;
    END IF;
  END LOOP;
  
  isr$trace.debug('olParameter', '-> logclob', sCurrentName, olParameter);

  IF olParameter.count() > 0 THEN
    FOR i IN 1..olParameter.count() LOOP
      IF instr(olParameter(i).sValue,':exec:') IS NOT NULL AND instr(olParameter(i).sValue,':exec:') > 0 THEN
        execute immediate REPLACE(olParameter(i).sValue, ':exec:') INTO olParameter(i).sValue;
        olParameter(i).sDisplay := olParameter(i).sValue;
      END IF;
      IF olParameter(i).TYPE = 'DATE' AND olParameter (i).sValue IS NOT NULL THEN
        BEGIN
          olParameter (i).sValue := to_char(to_date(olParameter (i).sValue, STB$UTIL.getSystemParameter('DATEFORMAT')), STB$UTIL.getSystemParameter('DATEFORMAT'));
        EXCEPTION WHEN OTHERS THEN
          olParameter (i).sValue := to_char(to_date(olParameter (i).sValue, STB$UTIL.getSystemParameter('DATEFORMAT'), 'nls_date_language=german'), STB$UTIL.getSystemParameter('DATEFORMAT'));
        END;
        olParameter(i).sDisplay := olParameter(i).sValue;
      END IF;
      
      -- OUTPUT/PROTECTED in higher versions not editable
        IF olParameter (i).sParameter = 'PROTECTED' THEN
          -- check if an document already exists then the default output id can't be changed
          OPEN cGetHighestDoc(nRepid, 'W');
          FETCH cGetHighestDoc INTO nDocId;
          IF cGetHighestDoc%NOTFOUND THEN
          isr$trace.debug('cGetHighestDoc%notFound ','not found', sCurrentName);
          END IF;
          CLOSE cGetHighestDoc;
          
          isr$trace.debug('nDocid',nDocid, sCurrentName);
          
          IF nvl (ndocid, 0) <> 0 THEN
            olParameter (i).sEditable := 'F';
            isr$trace.debug ('DocExists',  'DocExists: ' || ndocid, sCurrentName);
          END IF;
        END IF;
      
      -- display parameters which are null, invisible and required
      IF olParameter (i).sValue IS NULL
      AND olParameter (i).sDisplay = 'F'
      AND olParameter (i).sRequired = 'T' THEN
        olParameter (i).sDisplay := 'T';
        olParameter (i).sEditable := 'T';
      END IF;

    END LOOP;
  END IF;

  FOR i IN 1..olParameter.count() LOOP
    IF olParameter(i).type IN ('TABSHEET', 'GROUP') THEN
      -- if the last position is tab/group, delete it
      IF i = olParameter.count() THEN
        olParameter(i).type := 'DELETE';
      END IF;
      -- if the next visible is tab/group, delete it: exception if group follows tab
      IF i >= 1  AND i < olParameter.count() THEN
        FOR j IN i+1..olParameter.count() LOOP
          IF olParameter(j).type IN ('TABSHEET', 'GROUP')
          AND (olParameter(i).type = olParameter(j).type
              OR olParameter(i).type = 'GROUP' AND olParameter(j).type = 'TABSHEET') THEN
            olParameter(i).type := 'DELETE';
          END IF;
          IF olParameter(j).sDisplayed = 'T'
          AND (olParameter(j).type NOT IN ('TABSHEET', 'GROUP') OR olParameter(i).type != olParameter(j).type) THEN
            EXIT;
          END IF;
        END LOOP;
      END IF;
    END IF;
  END LOOP;

  isr$trace.stat('end','end', sCurrentName);
  RETURN oerrorobj;
EXCEPTION
  WHEN OTHERS THEN
     oErrorObj.handleError( Stb$typedef.cnSeverityWarning, utd$msglib.getmsg ('exUnexpectedError', stb$security.getCurrentLanguage,  csP1 => sqlerrm(sqlcode)),sCurrentName,
            dbms_utility.format_error_stack ||stb$typedef.crlf|| dbms_utility.format_error_backtrace );
    RETURN oerrorobj;
END getoutputoption;


--******************************************************************************
function callFunction( oParameter in STB$MENUENTRY$RECORD, olNodeList out STB$TREENODELIST  ) return stb$oerror$record
  is
  sCurrentName                  constant varchar2(200) := $$PLSQL_UNIT||'.callFunction('||oParameter.sToken||')';
  oErrorObj                     stb$oerror$record := STB$OERROR$RECORD();
  oParameter1  STB$MENUENTRY$RECORD;
 
begin
  isr$trace.stat('begin','begin', sCurrentName);
  case
   when upper (oparameter.stoken) in ('CAN_CREATE_WORD_DOCUMENT', 'CAN_MODIFY_WORD_OUTPUTOPTIONS') then

    olNodeList := stb$treenodelist ();
    olNodeList.extend;
    olNodeList (1) := STB$TREENODE$RECORD(STB$UTIL.getMaskTitle(upper (oParameter.sToken)),
                                          'ISR$REPORTTYPE$PACKAGE', 'TRANSPORT', 'MASK');

    
    oErrorObj := getOutputOption (olnodelist(1).tloPropertyList, isr$reporttype$package.getNumberOfForms, 'W');    
    
    isr$trace.debug('olNodeList', 's. logclob', sCurrentName, olNodeList);
      
  when upper (oparameter.stoken) = 'CAN_FINALIZE_EXCEL_REPORT' then
    oParameter1 := STB$MENUENTRY$RECORD(oParameter.sType, oParameter.sDisplay, 'CAN_FINALIZE_REPORT', 
                        'ISR$REPORTTYPE$PACKAGE', oParameter.sIcon, oParameter.sConfirm, oParameter.sConfirmText, oParameter.nId);
    oErrorObj := isr$reporttype$package.callFunction(oParameter1, olNodeList);
    
  when upper (oparameter.stoken) in ('CAN_SAVE_WORD_DOCUMENT', 'CAN_SAVE_EXCEL_DOCUMENT') then  
    oErrorObj := stb$system.unlockReports ('and locksession = ' || to_char(stb$security.getCurrentSession)); 
    olNodeList := STB$TREENODELIST ();
    olNodeList.EXTEND;
    olNodeList (1) := STB$TREENODE$RECORD(STB$UTIL.getMaskTitle('CAN_SAVE_DOCUMENT'),
                                          'ISR$REPORTTYPE$PACKAGE', 'TRANSPORT', 'MASK');
    oErrorObj := STB$UTIL.getPropertyList(upper ('CAN_SAVE_DOCUMENT'), olNodeList (1).tloPropertylist);
  else
    oErrorObj := isr$reporttype$package.callFunction(oParameter, olNodeList);
  end case;
  isr$trace.stat('end','end', sCurrentName);
  return oErrorObj;
EXCEPTION
  WHEN OTHERS THEN
    oErrorObj.handleError ( Stb$typedef.cnSeverityCritical,  utd$msglib.getmsg ('ERROR', stb$security.getCurrentLanguage) || SQLCODE, sCurrentName,
       dbms_utility.format_error_stack ||chr(10)||chr(13)|| dbms_utility.format_error_backtrace);
    RETURN oErrorObj; 
end callFunction;


--******************************************************************************
FUNCTION setJobParameterForWord (csToken IN isr$action.token%TYPE)
  RETURN stb$oerror$record
IS
  oErrorObj                     stb$oerror$record := STB$OERROR$RECORD();
  sCurrentName      CONSTANT VARCHAR2(100) := $$PLSQL_UNIT||'.setJobParameterForWord (' || csToken || ')';
  noutputid                     isr$report$output$type.outputid%TYPE;
  nactionid                     isr$action.actionid%TYPE;
  sfunction                     isr$report$output$type.actionfunction%TYPE;
  nRepid                        STB$REPORT.REPID%TYPE;

  olValueList ISR$VALUE$LIST;
  sMsg                  varchar2(4000);

  CURSOR cGetJobIds IS
    SELECT DISTINCT o.outputid, a.actionid, r.repid
      FROM isr$action a
         , isr$report$output$type o
         , isr$action$output ao
         , (SELECT max(repid) repid, reporttypeid, title FROM stb$report GROUP BY reporttypeid, title) r
     WHERE r.title = STB$OBJECT.getCurrentTitle
       AND r.reporttypeid = STB$OBJECT.getCurrentReporttypeid
       AND o.reporttypeid = r.reporttypeid
       AND o.outputid = ao.outputid
       AND a.token = csToken
       AND a.actionid = ao.actionid
      -- AND STB$DOCKS.getOutput(r.repid) = o.outputid
       ;
BEGIN
  isr$trace.stat('begin','begin', sCurrentName);

  -- call of the general job package
  SELECT stb$job$seq.NEXTVAL
  INTO   stb$job.njobid
  FROM   DUAL;
  
  OPEN cGetJobIds;
  FETCH cGetJobIds
  INTO  noutputid, nactionid, nRepid;
  CLOSE cGetJobIds;
  IF STB$OBJECT.getCurrentRepid IS NULL THEN
    STB$OBJECT.setCurrentRepid(nRepid);
  END IF;
  IF STB$UTIL.getReptypeParameter(stb$object.getCurrentReporttypeid, 'LONG_RUNNING_JOB') IS NOT NULL THEN
    stb$job.setJobParameter ('JOBTYPE', 'LONG_RUNNING_JOB_'||stb$object.getCurrentReporttypeid, stb$job.njobid);
  END IF;  

  if nactionid is null then
    sMsg := utd$msglib.getmsg ('exNotFindAction', stb$security.getCurrentLanguage, csToken);
    raise stb$job.exNotFindAction;
  end if;

  stb$job.setJobParameter ('TOKEN', csToken, stb$job.njobid);
  stb$job.setJobParameter ('OUTPUTID', noutputid, stb$job.njobid);
  stb$job.setJobParameter ('MASTERTEMPLATE', STB$OBJECT.getCurrentTemplate, stb$job.njobid);

  stb$job.setJobParameter ('ACTIONID', nactionid, stb$job.njobid);

  isr$trace.debug('ACTIONID', nactionid, sCurrentName);
  stb$job.setJobParameter ('REFERENCE', stb$object.getCurrentNodeId, stb$job.njobid);

  stb$job.setJobParameter ('REPORTTYPEID', stb$object.getCurrentReporttypeid, stb$job.njobid);

  IF trim (sfunction) IS NOT NULL THEN
    stb$job.setJobParameter ('FUNCTION', sfunction, stb$job.njobid);
  END IF;

  stb$job.setJobParameter ('SRNUMBER', stb$object.getcurrentsrnumber, stb$job.njobid);
  stb$job.setJobParameter ('TITLE', stb$object.getcurrenttitle, stb$job.njobid);

  stb$job.setJobParameter ('REPID', stb$object.getCurrentRepid, stb$job.njobid);


  olValueList := STB$OBJECT.getCurrentValueList;
  FOR i IN 1..olValueList.Count() LOOP
    STB$JOB.setJobParameter('olValueList('||To_Char(i)||').sNodeType', olValueList(i).sNodeType, STB$JOB.nJobId);
    STB$JOB.setJobParameter('olValueList('||To_Char(i)||').sNodeId', olValueList(i).sNodeId, STB$JOB.nJobId);
    STB$JOB.setJobParameter('olValueList('||To_Char(i)||').sNodePackage', olValueList(i).sNodePackage, STB$JOB.nJobId);
    STB$JOB.setJobParameter('olValueList('||To_Char(i)||').sParameterName', olValueList(i).sParameterName, STB$JOB.nJobId);
    STB$JOB.setJobParameter('olValueList('||To_Char(i)||').sParameterValue', olValueList(i).sParameterValue, STB$JOB.nJobId);
  END LOOP;

  STB$JOB.setJobParameter('SNODEID', STB$OBJECT.getCurrentNode().snodeid, STB$JOB.nJobId);

  isr$trace.stat('end','end', sCurrentName);
  RETURN oErrorObj;
EXCEPTION
  when stb$job.exNotFindAction then
    oErrorObj.handleError ( Stb$typedef.cnSeverityCritical, sMsg, sCurrentName, dbms_utility.format_error_stack ||stb$typedef.crlf|| dbms_utility.format_error_backtrace );
    return oErrorObj;
  WHEN OTHERS THEN
    oErrorObj.handleError ( Stb$typedef.cnSeverityCritical,  utd$msglib.getmsg ('ERROR', stb$security.getCurrentLanguage) || SQLCODE, sCurrentName, dbms_utility.format_error_stack ||chr(13)|| dbms_utility.format_error_backtrace );
    RETURN oErrorObj;
END setJobParameterForWord;

--*************************************************************************************************************************
FUNCTION putWordOutputOption (olParameter IN stb$property$list)
  RETURN stb$oerror$record
IS
  sCurrentName                  CONSTANT VARCHAR2(100) := $$PLSQL_UNIT||'.putWordOutputOption';
  oerrorobj                     stb$oerror$record := STB$OERROR$RECORD();
  sCurrentFormPackage  Stb$typedef.tLine;
  nCurrentFormNumber   INTEGER := 1;

  CURSOR cgetforminfo IS
    SELECT validatefunction, isrpackage
      FROM isr$report$wizard
     WHERE masktype = STB$TYPEDEF.cnOutputOption
       AND reporttypeid = stb$object.getCurrentReporttypeid;

   rGetFormInfo cgetforminfo%ROWTYPE;
BEGIN
  isr$trace.stat('begin','begin', sCurrentName);

  -- validate the output options
  OPEN cgetforminfo;
  FETCH cgetforminfo INTO rGetFormInfo;
  sCurrentFormPackage := rGetFormInfo.isrpackage; 
  IF cgetforminfo%FOUND AND UPPER(rGetFormInfo.validatefunction) IN ('T', 'VALIDATE') THEN
    -- validate if the selection is okay
    EXECUTE IMMEDIATE
      'BEGIN
         :1 :='||stb$util.getcurrentcustompackage||'.validate(:2);
       END;'
    using OUT oErrorObj, IN nCurrentFormNumber;
  END IF;
  CLOSE cgetforminfo;

  INSERT INTO STB$REPORTPARAMETER(REPID, PARAMETERNAME, PARAMETERVALUE, MODIFIEDON, MODIFIEDBY)
    (SELECT STB$OBJECT.getCurrentRepid, 'W_' ||sParameter, sValue, sysdate, stb$security.getoracleusername (stb$security.getcurrentuser)
       FROM table(olParameter)
      WHERE 'W_' ||sParameter NOT IN (SELECT parametername FROM STB$REPORTPARAMETER WHERE repid = STB$OBJECT.getCurrentRepid)
        AND sDisplayed = 'T');

  FOR i IN 1..olParameter.count() LOOP
    UPDATE stb$reportparameter
       SET parametervalue = olParameter (i).sValue,
           modifiedon = sysdate,
           modifiedby = stb$security.getoracleusername (stb$security.getcurrentuser)
     WHERE parametername = 'W_' || olParameter (i).sParameter
       AND repid = STB$OBJECT.getCurrentRepid;
  END LOOP;

  UPDATE stb$report
     SET modifiedon = sysdate
       , modifiedby = stb$security.getoracleusername (stb$security.getcurrentuser)
   WHERE repid = STB$OBJECT.getCurrentRepid;

  oErrorObj := isr$reporttype$package.updateCheckSum (STB$OBJECT.getCurrentRepid);

   -- Expected end of ouput option handling
   -- cleanup
   EXECUTE IMMEDIATE 'BEGIN
                       :1 := '||sCurrentFormPackage||'.ExitOutputOptions;
                     END;'
   using OUT oErrorObj;

  isr$trace.stat('end','end', sCurrentName);
  RETURN oerrorobj;
EXCEPTION
  WHEN OTHERS THEN
    oErrorObj.handleError ( Stb$typedef.cnSeverityCritical,  utd$msglib.getmsg ('ERROR', stb$security.getCurrentLanguage) || SQLCODE, sCurrentName,
       dbms_utility.format_error_stack ||chr(10)||chr(13)|| dbms_utility.format_error_backtrace);
    RETURN oErrorObj;
END putWordOutputOption;
--******************************************************************************
function putParameters( oParameter  in STB$MENUENTRY$RECORD, olParameter in STB$PROPERTY$LIST ) return stb$oerror$record
  is
  sCurrentName                  constant varchar2(200) := $$PLSQL_UNIT||'.putParameters('||oParameter.sToken||')';
  oErrorObj                     stb$oerror$record := STB$OERROR$RECORD();
  sToken      varchar2(255);
  oParameter1 STB$MENUENTRY$RECORD;
  sAuditFlag  varchar2(1);
begin
  isr$trace.stat('begin','begin', sCurrentName);
  sToken := case when oParameter.sToken = 'CAN_MODIFY_WORD_OUTPUTOPTIONS' then 'CAN_CREATE_WORD_DOCUMENT' else oParameter.sToken end;
  STB$OBJECT.setCurrentToken(sToken);
  case
    when upper (stoken) in ('CAN_CREATE_WORD_DOCUMENT') then
      oErrorObj := putWordOutputOption(olParameter);
      
        --======audit======
      oErrorObj := isr$reporttype$package.createSrAudit (stb$object.getCurrentSrNumber);
      isr$trace.debug('after sraudit','sToken: '||sToken, sCurrentName);
      oErrorObj := isr$reporttype$package.checkAuditRequired(sAuditFlag);
      isr$trace.debug('status sAuditFlag','sAuditFlag: '||sAuditFlag, sCurrentName);
      if sAuditFlag = 'T' then
        oErrorObj := Stb$audit.silentAudit;
      else
        isr$trace.debug('no audit required','sAuditFlag: '||sAuditFlag, sCurrentName);
      end if;
      commit;
      isr$trace.debug('after commit', 'commit', sCurrentName);
      
      oErrorObj := setJobParameterForWord (upper (stoken));
      stb$job.setJobParameter ('NODETYPE', 'REPORT_WORD', stb$job.njobid);
      
      if oErrorObj.isExceptionCritical then
        raise stb$typedef.exCritical;
      end if;
      stb$job.setJobParameter ('REFERENCE', stb$object.getCurrentRepid, stb$job.njobid);
      oErrorObj := Stb$util.createFilterParameter (olParameter);

      IF STB$USERSECURITY.checkoutDirect(stb$security.getcurrentuser) = 'F' THEN
        stb$job.setJobParameter ('CAN_USE_DOC_SERVER', 'T', stb$job.njobid);
      ELSE
        stb$job.setJobParameter ('ACTIONTYPE', 'SAVE', stb$job.njobid);
        stb$job.setJobParameter ('CAN_USE_DOC_SERVER', 'F', stb$job.njobid);
      END IF;

      -- unlock the reports because a scheduled job is started
      oErrorObj := stb$system.unlockReports ('and title = '''||REPLACE(STB$OBJECT.getCurrentTitle, '''', '''''')||''' and reporttypeid = ''' || stb$object.getCurrentReporttypeid || ''' and locksession = ' || to_char(stb$security.getcurrentsession));

      oErrorObj := stb$job.startJob;
     when upper (oparameter.stoken) in ('CAN_FINALIZE_EXCEL_REPORT') then
      oParameter1 := STB$MENUENTRY$RECORD(oParameter.sType, oParameter.sDisplay, 'CAN_FINALIZE_REPORT', 
                        'ISR$REPORTTYPE$PACKAGE', oParameter.sIcon, oParameter.sConfirm, oParameter.sConfirmText, oParameter.nId);
     oErrorObj := isr$reporttype$package.putParameters(oParameter1, olParameter);
    when upper (stoken) in ('CAN_CREATE_PDF') then
      oErrorObj := isr$reporttype$package.setJobParameter (upper (oparameter.stoken));
      stb$job.setJobParameter ('NODETYPE', 'REPORT_WORD', stb$job.njobid);
      stb$job.setJobParameter ('DO_NOT_SWITCH_REPSESSION', 'T', stb$job.njobid);
      stb$job.setJobParameter ('COMMAND', 'CAN_DISPLAY_REPORT', stb$job.njobid);
      stb$job.setJobParameter ('CAN_USE_DOC_SERVER', 'T', stb$job.njobid);
      IF olParameter IS NOT NULL THEN
        FOR i IN 1..olParameter.count() LOOP
          STB$JOB.setJobParameter(olParameter(i).sParameter, olParameter(i).sValue, STB$JOB.nJobid);
        END LOOP;
      END IF;
      oErrorObj := Stb$util.createFilterParameter (olParameter);
      oErrorObj := stb$job.startJob;
    when upper (stoken) in ('CAN_SAVE_WORD_DOCUMENT', 'CAN_SAVE_EXCEL_DOCUMENT') then  
      oErrorObj := stb$system.unlockReports ('and locksession = ' || to_char(stb$security.getCurrentSession));
      oErrorObj := isr$reporttype$package.setJobParameter (upper (oparameter.stoken));
      stb$job.setJobParameter ('ACTIONTYPE', 'SAVE', stb$job.njobid);
      stb$job.setJobParameter ('DOCID', STB$OBJECT.getCurrentNodeId, stb$job.njobid);
      FOR i IN 1 .. olParameter.count () LOOP
        stb$job.setJobParameter (olParameter (i).sparameter, olParameter (i).svalue, stb$job.njobid);
      END LOOP;
      oErrorObj := stb$job.startJob;
    else
     oErrorObj := isr$reporttype$package.putParameters(oParameter, olParameter);
  end case;
  isr$trace.stat('end','end', sCurrentName);
  return oErrorObj;
EXCEPTION
  WHEN OTHERS THEN
    oErrorObj.handleError ( Stb$typedef.cnSeverityCritical,  utd$msglib.getmsg ('ERROR', stb$security.getCurrentLanguage) || SQLCODE, sCurrentName,
       dbms_utility.format_error_stack ||chr(10)||chr(13)|| dbms_utility.format_error_backtrace);
    RETURN oErrorObj; 
end putParameters;

--******************************************************************************
FUNCTION getLov (sEntity IN VARCHAR2, ssuchwert IN VARCHAR2, oSelectionList OUT isr$tlrselection$list)
  RETURN stb$oerror$record
IS
  sCurrentName                  CONSTANT VARCHAR2(200) := $$PLSQL_UNIT||'.getLov('||sEntity||')';
  oErrorObj                     stb$oerror$record := STB$OERROR$RECORD();
  
  bPrevWordAvailable            BOOLEAN := FALSE;
  
  CURSOR cGetMasterTemplates IS
    SELECT DISTINCT
           f.fileid
         , t.templatename display
         ,    utd$msglib.getmsg ('VERSION', stb$security.getCurrentLanguage)
           || t.version
           || CASE WHEN t.release = 'T' THEN utd$msglib.getmsg ('RELEASED', stb$security.getCurrentLanguage) END
           || utd$msglib.getmsg ('TEMPLATEFILE', stb$security.getCurrentLanguage)
           || f.filename
              description
         , t.version
         , t.release
         , t.icon
      FROM isr$file f
         , (SELECT t.*
              FROM (SELECT templatename, NVL (release, 'F') release, MAX (version) version
                      FROM isr$template
                    GROUP BY templatename, release) t1, isr$template t
             WHERE t.templatename = t1.templatename
               AND t.version = t1.version) t
         , isr$output$file iof
         , isr$report$output$type rot
         , (SELECT reporttypeid
                 , STB$SECURITY.checkGroupRight ('SHOW_IN_EXPLORER', reporttypeid) GRANT_SHOW_IN_EXPLORER
                 , STB$SECURITY.checkGroupRight ('CAN_CREATE_NEW_REPORT', reporttypeid) GRANT_CAN_CREATE_NEW_REPORT
                 , STB$SECURITY.checkGroupRight ('SHOW_UNRELEASED_TEMPLATES', reporttypeid) GRANT_SHOW_UNRELEASED_TEMPL
              FROM stb$reporttype
             WHERE activetype = 1) r
     WHERE (rot.reporttypeid = STB$OBJECT.getCurrentReporttypeId
         OR STB$OBJECT.getCurrentReporttypeId IS NULL)
       AND rot.VISIBLE = 'T'
       AND rot.reporttypeid = r.reporttypeid
       AND f.usage IN ('MASTERTEMPLATE_WORD')
       AND iof.fileid = t.fileid
       AND iof.outputid = rot.outputid
       AND t.fileid = f.fileid
       AND GRANT_SHOW_IN_EXPLORER = 'T'
       AND GRANT_CAN_CREATE_NEW_REPORT = 'T'
       AND (t.release = 'F' AND GRANT_SHOW_UNRELEASED_TEMPL = 'T' OR t.release = 'T')
       AND CASE WHEN STB$SECURITY.checkGroupRight ('TEMPLATE' || t.fileid, rot.reporttypeid) = 'T' THEN t.VISIBLE ELSE 'F' END = 'T'
    ORDER BY 2, 4;
      
   cursor cGetPrevWordDoc is
   select distinct 'PREV_WORD_DOC' value, 
          filename description, 
          utd$msglib.getmsg ('PREV_WORD_DOC', stb$security.getCurrentLanguage) display,
          isr$excel$reporttype.getIconName(isr$excel$reporttype.getDocumentType(doctype), doc_definition, null, 'F') icon,
          d.nodeid, d.DOCVERSION
   from STB$REPORT r, STB$DOCTRAIL d
    where  (r.repid = stb$object.getCurrentRepid and r.parentrepid = d.nodeid or d.nodeid=stb$object.getCurrentRepid)
     and   doctype in ('W')
  ORDER BY d.nodeid desc, d.DOCVERSION DESC, d.nodeid asc, d.DOCVERSION ASC
  FETCH  first 1 rows only;

BEGIN
  isr$trace.stat('begin','begin', sCurrentName);
  
  CASE
    --***************************************
    WHEN upper (sEntity) = 'MASTERTEMPLATE_WORD' THEN
      oSelectionList := isr$tlrselection$list ();
      FOR rGetMasterTemplates IN cGetMasterTemplates LOOP
        oSelectionList.EXTEND;
        oSelectionList (oSelectionList.count()) := ISR$OSELECTION$RECORD(rGetMasterTemplates.display, rGetMasterTemplates.fileid, rGetMasterTemplates.description, null, oSelectionList.COUNT());

        SELECT ISR$ATTRIBUTE$RECORD (sParameter, sValue, sPrepresent)
          BULK COLLECT INTO oSelectionList (oSelectionList.COUNT ()).tloAttributeList
          FROM (SELECT 'ICON' sParameter
                     , rGetMasterTemplates.icon sValue
                     , NULL sPrepresent
                  FROM DUAL);
        isr$trace.debug ('oSelectionList('||oSelectionList.count()||').sSelected', '', sCurrentName, oSelectionList);
      END LOOP;
      
      FOR rGetPrevWordDoc IN cGetPrevWordDoc LOOP
        oSelectionList.EXTEND;
        oSelectionList (oSelectionList.count()) := ISR$OSELECTION$RECORD(rGetPrevWordDoc.display, 
                  rGetPrevWordDoc.value, rGetPrevWordDoc.description, null, oSelectionList.COUNT());
        SELECT ISR$ATTRIBUTE$RECORD (sParameter, sValue, sPrepresent)
          BULK COLLECT INTO oSelectionList (oSelectionList.COUNT ()).tloAttributeList
          FROM (SELECT 'ICON' sParameter
                     , rGetPrevWordDoc.icon sValue
                     , NULL sPrepresent
                  FROM DUAL);
        bPrevWordAvailable := TRUE;
      end loop;
    
      IF (sSuchWert IS NOT NULL OR NOT(bPrevWordAvailable)) AND oselectionlist.first IS NOT NULL THEN
        isr$trace.debug ('sSuchWert',  sSuchWert, sCurrentName);
        FOR i IN 1..oSelectionList.count() LOOP
          -- Select previous version if available
          IF (oSelectionList(i).sValue = sSuchWert AND NOT(bPrevWordAvailable)) OR oSelectionList(i).sValue = 'PREV_WORD_DOC' THEN
            isr$trace.debug ('oSelectionList('||i||').sSelected', oSelectionList(i).sValue, sCurrentName);
            oSelectionList(i).sSelected := 'T';
          END IF;
        END LOOP;
      END IF;
    --***************************************
    ELSE
     oErrorObj := isr$reporttype$package.getLov(sEntity, ssuchwert, oSelectionList);
  END CASE;


  isr$trace.stat('end','end', sCurrentName);
  RETURN oErrorObj;
EXCEPTION
  WHEN OTHERS THEN
     oErrorObj.handleError ( Stb$typedef.cnSeverityMessage,  utd$msglib.getmsg ('ERROR', stb$security.getCurrentLanguage) || SQLCODE, sCurrentName,
       dbms_utility.format_error_stack ||chr(10)||chr(13)|| dbms_utility.format_error_backtrace);
    RETURN oErrorObj; 
END getlov;

end isr$excel$reporttype;