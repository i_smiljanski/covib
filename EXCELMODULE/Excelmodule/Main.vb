﻿' <summary>The Main function, and some global utility routines for error handling.</summary>

Imports System
Imports System.Diagnostics
Imports System.AppDomain
Imports System.Reflection
Imports System.IO

Module Main

    ''' <summary>
    ''' Name of the application as known in iSR
    ''' </summary>
    Public sConfigAppName As String = "excelmodule"

    ''' <summary>
    ''' Global Trace object
    ''' </summary>
    Public oTrace As TraceSource = New TraceSource("Excelmodule")

    ''' <summary>
    ''' Entry Point for the program.
    ''' Just handles the trace system and contains a top level try/catch for everyting.
    ''' </summary>
    Function Main() As Integer

        Dim iReturn As Integer = 0
        Dim oMainCon As MainControl = Nothing
        Dim idxLogListener As New Integer()

        'Change the Current path to the app.path. This is essential for use on a rendering server
        Environment.CurrentDirectory = CurrentDomain.BaseDirectory()

        'Delete an existing log
        If File.Exists("Excelmodule.log") Then Kill("Excelmodule.log")

        'Configue Trace
        Dim oSwitch = New SourceSwitch("ExcelmoduleSwitch", "Information")
        oTrace.Switch = oSwitch
        oTrace.Switch.Level = SourceLevels.Verbose
        idxLogListener = oTrace.Listeners.Add(New TimestampedTextTraceListener("Excelmodule.log"))

        'Console Output (not needed if log present)
        'iSR loader does not handle output on stdout, activating this will lead to a deadlock when run in this context.
        'Dim oConsoleListener = New ConsoleTraceListener()
        'oTrace.Listeners.Add(oConsoleListener)

        'And go
        oTrace.TraceEvent(TraceEventType.Information, 1, "Excelmodule " & FileVersionInfo.GetVersionInfo(Assembly.GetExecutingAssembly().Location).FileVersion)
        oTrace.TraceEvent(TraceEventType.Information, 1, "Entering Main: " & Date.Now)

        If System.Diagnostics.Debugger.IsAttached() = False Then
            oTrace.TraceEvent(TraceEventType.Information, 10, "Excelmodule is running native.")

            'Take over Error handling
            Try
                oMainCon = New MainControl
                iReturn = oMainCon.MainSequence()
                oMainCon.CleanUp()
            Catch ex As Exception
                oTrace.TraceEvent(TraceEventType.Critical, 10, "Exception Occured in MainControl.MainSequence: " & ex.ToString)

                ' Write an error file
                File.AppendAllText(sConfigAppName & ".error", "Exception Occured in MainControl.MainSequence: " & ex.ToString)

                ''Check if there is a Word Object still open
                'If Not oMainCon.GetWordBackup Is Nothing Then
                '    oTrace.TraceEvent(TraceEventType.Information, 10, "Closing Word Instance")
                '    oMainCon.GetWordBackup.Quit(True)
                'End If

                oTrace.TraceEvent(TraceEventType.Information, 10, "Returning -1 (Error)" & Date.Now)

                iReturn = -1

            End Try

        Else
            oTrace.TraceEvent(TraceEventType.Information, 10, "Excelmodule is running in IDE.")

            'Let the IDE do the error handling
            oMainCon = New MainControl
            iReturn = oMainCon.MainSequence()
            oMainCon.CleanUp()

        End If

        oMainCon = Nothing

        oTrace.TraceEvent(TraceEventType.Information, 1, "Main Return Code: " & iReturn)
        oTrace.TraceEvent(TraceEventType.Information, 1, "Exiting Main: " & Date.Now)

        oTrace.Flush()
        oTrace.Close()

        oTrace = Nothing

        Return iReturn

    End Function

    ''' <summary>
    ''' Handles Exceptions by writing a message to the trace log.
    ''' Called only at places where exceptions are expected, i.e. in the catch block.
    ''' </summary>
    ''' <param name="ex">The exception.</param>  
    ''' <param name="sSource">Additional information, usually the name of the method.</param>  
    Public Sub HandleException(ByVal ex As Exception, ByVal sSource As String)

        oTrace.TraceEvent(TraceEventType.Warning, 10, "Exception Occured in " & sSource & ": " & ex.ToString)

    End Sub

End Module
