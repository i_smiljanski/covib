﻿' <summary>Container for all options from the config.xml.</summary>

Imports System.Xml.XPath
Imports System.IO

Public Class Options

    Private iOrder As Integer

    ''' <summary>
    ''' Path to the config.xml
    ''' </summary>
    Private sConfigPath As String
    ''' <summary>
    ''' The Path to the config.xml
    ''' </summary>
    Public Property ConfigPath() As String
        Get
            Return sConfigPath
        End Get
        Private Set(ByVal value As String)
            sConfigPath = value
        End Set
    End Property

    ''' <summary>
    ''' Contains the Filename of the Config.xml.
    ''' </summary>
    Private sConfigFile As String
    ''' <summary>
    ''' The Filename of the Config.xml.
    ''' </summary>
    Public Property ConfigFile() As String
        Get
            Return sConfigFile
        End Get
        Private Set(ByVal value As String)
            sConfigFile = value
        End Set
    End Property

    ''' <summary>
    ''' Contains the command.
    ''' </summary>
    Private sCommand As String
    ''' <summary>
    ''' The Filename of the Document/Workbook.
    ''' </summary>
    Public Property Command() As String
        Get
            Return sCommand
        End Get
        Private Set(ByVal value As String)
            sCommand = value
        End Set
    End Property

    ''' <summary>
    ''' Contains The Filename of the Template
    ''' </summary>
    Private sTemplateFilename As String
    ''' <summary>
    ''' The Filename of the Document/Workbook.
    ''' </summary>
    Public Property TemplateFilename() As String
        Get
            Return sTemplateFilename
        End Get
        Private Set(ByVal value As String)
            sTemplateFilename = value
        End Set
    End Property

    ''' <summary>
    ''' Contructor.
    ''' </summary>
    ''' <remarks>
    ''' Sets the default values according to the FRS (EXP25), 
    ''' but these will likely be overriden by the values of the config.xml.
    ''' </remarks>
    Public Sub New()
        oTrace.TraceEvent(TraceEventType.Information, 1, "Entering Options.New")

    End Sub

    ''' <summary>
    ''' In absence of a 'real' destructor.
    ''' </summary>
    Public Sub CleanUp()
        oTrace.TraceEvent(TraceEventType.Information, 1, "Entering Options.CleanUp")

    End Sub

    ''' <summary>
    ''' Parses the command line arguments.
    ''' </summary>
    ''' <remarks>
    ''' If no config.xml is given at the command line, it defaults to "config.xml" (in the app.path).
    ''' </remarks>
    Sub ParseArgs()
        oTrace.TraceEvent(TraceEventType.Information, 1, "Entering Options.ParseArgs")

        If Environment.GetCommandLineArgs.Count = 1 Then
            oTrace.TraceEvent(TraceEventType.Warning, 10, "No Config File parameter given. Trying default config.xml.")
            sConfigFile = Environment.CurrentDirectory & "\config.xml"
            oTrace.TraceEvent(TraceEventType.Warning, 10, "No order parameter given. Trying default 1.")
            iOrder = 1 'Default
        Else
            sConfigFile = Environment.GetCommandLineArgs(1)
        End If

        If My.Application.CommandLineArgs.Count > 1 Then
            iOrder = Convert.ToInt32(Environment.GetCommandLineArgs(2))
        End If

        ConfigPath = Path.GetDirectoryName(sConfigFile)

        If ConfigPath = String.Empty Then
            'No path given with the config. Determine the application path instead
            ConfigPath = My.Application.Info.DirectoryPath
        End If

    End Sub

    ''' <summary>
    ''' Reads the config.xml file.
    ''' </summary>
    ''' <remarks>
    ''' With the exception of fullpathname ("/config/apps"), all terms are read from 
    ''' "/config/apps/app[@name='excelmodule']" from the config.xml.     
    ''' <list type="bullet">
    '''   <item><description>template</description></item>
    '''   <item><description>workbook</description></item>
    ''' </list>
    ''' </remarks>
    ''' <returns>true if the file was opend and could be read, false if something went wrong.</returns>  
    Function ReadConfigFile(oWorkbooksXM As List(Of WorkbookXM)) As Boolean
        oTrace.TraceEvent(TraceEventType.Information, 1, "Entering Options.ReadConfigFile")

        Dim oXpathDoc As XPathDocument
        Dim oXmlNav As XPathNavigator
        Dim oResult As Object
        Dim oResultWS As Object
        Dim sValue As String

        Dim oWorkbookXM As WorkbookXM
        Dim oWorksheetXM As WorksheetXM
        Dim iWorkbookCount As Integer = 0

        Dim sXpathBase As String = "/config/apps/app[@name='" & Main.sConfigAppName & "' and @order='" & iOrder & "' ]/"

        Try
            oXpathDoc = New XPathDocument(sConfigFile)
            oXmlNav = oXpathDoc.CreateNavigator()
        Catch ex As XPathException
            HandleException(ex, "ReadConfigFile, XMLException")
            Return False
        Catch ex As Exception
            HandleException(ex, "ReadConfigFile")
            Return False
        End Try

        'Read Command
        oResult = oXmlNav.SelectSingleNode(sXpathBase & "command")
        If Not oResult Is Nothing Then
            sCommand = oResult.ToString
            oTrace.TraceEvent(TraceEventType.Information, 11, "Command = " & sCommand)
        End If

        'Read TemplateFilename
        oResult = oXmlNav.SelectSingleNode(sXpathBase & "template")
        If Not oResult Is Nothing Then
            sTemplateFilename = CheckPath(oResult.ToString)
            oTrace.TraceEvent(TraceEventType.Information, 11, "TemplateFilename = " & sTemplateFilename)
        End If

        ' Laden aller Workbooks.
        oResult = oXmlNav.Select(sXpathBase & "workbooks/workbook")
        If Not oResult Is Nothing Then

            For Each oXmlNav2 In oResult

                iWorkbookCount = iWorkbookCount + 1

                oWorkbookXM = New WorkbookXM

                oWorkbookXM.Filename = CheckPath(oXmlNav2.GetAttribute("filename", ""))
                oWorkbookXM.Protect = CheckBoolean(oXmlNav2.GetAttribute("protected", ""), False)
                oWorkbookXM.Password = oXmlNav2.GetAttribute("password", "")
                oWorkbookXM.CreateToC = CheckBoolean(oXmlNav2.GetAttribute("createtoc", ""), True)

                oResultWS = oXmlNav2.Select(sXpathBase & "workbooks/workbook[" & iWorkbookCount & "]/worksheet")
                If Not oResultWS Is Nothing Then

                    For Each oXmlNav3 In oResultWS

                        oWorksheetXM = New WorksheetXM

                        oWorksheetXM.Name = oXmlNav3.GetAttribute("name", "")
                        oWorksheetXM.Description = oXmlNav3.GetAttribute("description", "")
                        oWorksheetXM.Protect = CheckBoolean(oXmlNav2.GetAttribute("protected", ""), False)
                        oWorksheetXM.Password = oXmlNav3.GetAttribute("password", "")
                        oWorksheetXM.Filename = CheckPath(oXmlNav3.ToString)

                        oWorkbookXM.AddWorksheet(oWorksheetXM)

                    Next oXmlNav3

                End If

                'Read the custom properties
                oTrace.TraceEvent(TraceEventType.Information, 1, "Reading Doc Properties from config.")
                oResult = oXmlNav2.Select(sXpathBase & "workbooks/workbook[" & iWorkbookCount & "]/properties/custom-property")
                If Not oResult Is Nothing Then
                    For Each oXmlNav3 In oResult

                        If oXmlNav3.ToString Is Nothing Or oXmlNav3.ToString = String.Empty Then
                            sValue = " "
                        Else
                            sValue = Replace(oXmlNav3.ToString, "\r\n", vbCrLf)
                        End If

                        'Include replacing of vbCrLf Tokens
                        oWorkbookXM.oProperties.AddProperty(oXmlNav3.GetAttribute("name", ""), sValue)

                    Next oXmlNav3
                End If

                'Add the workbook instance to the list of workbooks
                oWorkbooksXM.Add(oWorkbookXM)

            Next oXmlNav2

        End If


        Return True

    End Function

    ''' <summary>
    ''' Checks if there is an "\" in the path, else the Path to the Config will be added
    ''' </summary>
    ''' <param name="sFile">The filename with or without path.</param>  
    ''' <returns>File with path</returns>  
    Function CheckPath(sFile) As String

        If Not InStr(sFile.ToString, "\") > 0 Then
            CheckPath = ConfigPath & "\" & sFile.ToString
        Else
            CheckPath = sFile.ToString
        End If

    End Function

    Function CheckBoolean(sValue As String, bDefault As Boolean) As Boolean

        If sValue = "true" Then
            Return True
        Else
            If sValue = String.Empty Then
                Return bDefault
            Else
                Return False
            End If
        End If

    End Function

End Class
