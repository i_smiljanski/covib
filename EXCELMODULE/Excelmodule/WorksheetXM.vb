﻿Public Class WorksheetXM

    Private sName As String
    Private sDescription As String
    Private sProtect As Boolean
    Private sPassword As String
    Private sFilename As String

    ''' <summary>
    ''' Name of the Worksheet This is used for the tab, thus may not exceed 30 chars.
    ''' </summary>
    Public Property Name As String
        Get
            Return sName
        End Get
        Set(value As String)
            sName = Left(value, 30)
        End Set
    End Property

    ''' <summary>
    ''' Description of the worksheet. Used in Table of Contents.
    ''' </summary>
    Public Property Description As String
        Get
            Return sDescription
        End Get
        Set(value As String)
            sDescription = value
        End Set
    End Property

    ''' <summary>
    ''' Protected status for the worksheet.
    ''' </summary>
    Public Property Protect As Boolean
        Get
            Return sProtect
        End Get
        Set(value As Boolean)
            sProtect = value
        End Set
    End Property

    ''' <summary>
    ''' Password of the worksheet, if protected
    ''' </summary>
    Public Property Password As String
        Get
            Return sPassword
        End Get
        Set(value As String)
            sPassword = value
        End Set
    End Property

    ''' <summary>
    ''' Filename of the source file, which will be imported to an worksheet
    ''' </summary>
    Public Property Filename As String
        Get
            Return sFilename
        End Get
        Set(value As String)
            sFilename = value
        End Set
    End Property

End Class
