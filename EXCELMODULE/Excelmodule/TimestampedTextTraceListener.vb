﻿Public Class TimestampedTextTraceListener : Inherits System.Diagnostics.TextWriterTraceListener

    ''' <summary>
    ''' Stopwatch for Logging
    ''' </summary>
    Private oStopWatch As New Stopwatch()

    Sub New(sFilename As String)

        MyBase.New(sFilename)

        oStopWatch.Start()

    End Sub

    Protected Overrides Sub Finalize()

        oStopWatch.Stop()
        oStopWatch = Nothing

        MyBase.Finalize()

    End Sub

    Public Overloads Overrides Sub Write(ByVal Message As String)

        MyBase.Write(oStopWatch.Elapsed.ToString & " " & Message.PadRight(35))

    End Sub


    Public Overloads Overrides Sub WriteLine(ByVal Message As String)

        MyBase.WriteLine(Message)

    End Sub

End Class
