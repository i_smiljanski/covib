﻿' <summary>The central sequence handler.</summary>

Imports System.Xml.XPath
Imports System.Xml

Public Class MainControl

    Dim oOptions As Options = Nothing
    Dim oHandlerExcel As HandlerExcel = Nothing

    Dim oWorkbooksXM As List(Of WorkbookXM) = Nothing

    ''' <summary>
    ''' Constructor.
    ''' </summary>
    Public Sub New()
        oTrace.TraceEvent(TraceEventType.Information, 1, "Entering MainControl.New")

        'Initalize some general objects
        oOptions = New Options
        oWorkbooksXM = New List(Of WorkbookXM)

    End Sub

    ''' <summary>
    ''' In absence of a 'real' destructor.
    ''' </summary>
    Public Sub CleanUp()
        oTrace.TraceEvent(TraceEventType.Information, 1, "Entering MainControl.CleanUp")

        'Cleanup
        oOptions = Nothing
        oHandlerExcel = Nothing

        'The excel task does not go away if the GC is not called!
        GC.Collect()
        GC.WaitForPendingFinalizers()

    End Sub

    ''' <summary>
    ''' The Mainloop kind thing. Though it is no loop at all.
    ''' </summary>
    Public Function MainSequence() As Integer
        oTrace.TraceEvent(TraceEventType.Information, 1, "Entering MainControl.MainSequence")

        Dim iReturncode As Integer = 0

        'Read the Command line
        oOptions.ParseArgs()

        'Read the Config file
        If oOptions.ReadConfigFile(oWorkbooksXM) = False Then Return -1

        Select Case oOptions.Command

            Case "CAN_RUN_REPORT"

                For Each oWorkbookXM In oWorkbooksXM

                    oHandlerExcel = New HandlerExcel
                    If oOptions.TemplateFilename = String.Empty Then
                        oHandlerExcel.CreateNewWorkbook()
                    Else
                        oHandlerExcel.CreateWorkbookFromTemplate(oOptions.TemplateFilename)
                    End If

                    For Each oWorksheetXM In oWorkbookXM.oWorksheets
                        oHandlerExcel.ImportWorksheet(oWorksheetXM)
                    Next

                    'Create table of Contents
                    If oWorkbookXM.CreateToC = True Then
                        oHandlerExcel.CreateTableOfContents(oWorkbookXM)
                    Else
                        oHandlerExcel.RemoveWorksheet("XM_FIRST")
                    End If

                    oHandlerExcel.WriteCustomProperties(oWorkbookXM.oProperties)
                    oHandlerExcel.SaveWorkbook(oWorkbookXM.Filename)
                    oHandlerExcel.CloseWorkingWorkbook()
                    oHandlerExcel = Nothing

                Next

            Case "CAN_CHECKOUT", "CAN_REFRESH_DOC_PROPERTIES"

                For Each oWorkbookXM In oWorkbooksXM

                    oHandlerExcel = New HandlerExcel

                    If oHandlerExcel.OpenWorkbook(oWorkbookXM.Filename) = True Then
                        oHandlerExcel.WriteCustomProperties(oWorkbookXM.oProperties)
                        oHandlerExcel.SaveWorkbook(oWorkbookXM.Filename)
                        oHandlerExcel.CloseWorkingWorkbook()
                    Else
                        oTrace.TraceEvent(TraceEventType.Critical, 12, "Cannot open " & oWorkbookXM.Filename)
                    End If

                    oHandlerExcel = Nothing

                Next

            Case "SAVE"

                oHandlerExcel = New HandlerExcel
                If oHandlerExcel.OpenWorkbook(oOptions.TemplateFilename) = True Then
                    oHandlerExcel.SaveOneFilePerWorksheet(oOptions.ConfigPath)
                    oHandlerExcel.CloseWorkingWorkbook()
                Else
                    oTrace.TraceEvent(TraceEventType.Critical, 12, "Cannot open " & oOptions.TemplateFilename)
                End If
                oHandlerExcel = Nothing

            Case Else
                iReturncode = -1

        End Select

        Return iReturncode

    End Function

    ''' <summary>
    ''' Replaces all invalid characters by "_"
    ''' </summary>
    ''' <param name="sFilename">filename to be checked.</param>  
    Private Function ReplaceInvalidFileNameChars(sFilename As String) As String

        For Each invalidChar In IO.Path.GetInvalidFileNameChars
            sFilename = sFilename.Replace(invalidChar, "_")
        Next
        Return sFilename

    End Function

End Class