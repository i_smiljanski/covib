﻿' <summary>Handels everything Word.</summary>

'Imports System.Xml.XPath
Imports Microsoft.Office.Interop.Excel
Imports Microsoft.Office.Core
Imports System.Text
'Imports System.Security.Cryptography
'Imports System.Text.RegularExpressions


Public Class HandlerExcel

    Private oExcel As Application = Nothing
    Private sWorkbookNameSource As String

    Private oWorkbookSource As Workbook

    ''' <summary>
    ''' Contructor.
    ''' Initializes the Excel Object.
    ''' </summary>
    Public Sub New()
        oTrace.TraceEvent(TraceEventType.Information, 1, "Entering HandlerExcel.New")

        'Initialize the Excel Object
        oExcel = New Application
        oExcel.Visible = False
        oExcel.DisplayAlerts = False

    End Sub


    ''' <summary>
    ''' In absence of a 'real' destructor.
    ''' </summary>
    Public Sub CleanUp()
        oTrace.TraceEvent(TraceEventType.Information, 1, "Entering HandlerExcel.CleanUp")

        'If the object is already empty, there is nothing to do here
        If oExcel Is Nothing Then Exit Sub

        oExcel.DisplayAlerts = False

        'Now leave Excel.
        oExcel.Quit()

        'Free the Excel object
        oExcel = Nothing

    End Sub

    ''' <summary>
    ''' In Case the Excelmodule exits due to an error, Main has to have the possibility to close the Excel instance gracefully.
    ''' </summary>
    ''' <returns>the Excel Object</returns>  
    Public Function GetExcelBackup() As Application

        Return oExcel

    End Function

    ''' <summary>
    ''' Open the associated workbook and sets it as the working workbook
    ''' </summary>
    ''' <param name="sWorkbookFile">The workbook filename (including path).</param>  
    ''' <returns>true if the file was opend, false if something went wrong.</returns>  
    Public Function OpenWorkbook(ByVal sWorkbookFile As String) As Boolean
        oTrace.TraceEvent(TraceEventType.Information, 1, "Entering HandlerExcel.OpenWorkbook")
        oTrace.Flush()

        oTrace.TraceEvent(TraceEventType.Information, 1, "Opening " & sWorkbookFile)

        Try
            oTrace.TraceEvent(TraceEventType.Information, 1, "Document Size: " & My.Computer.FileSystem.GetFileInfo(sWorkbookFile).Length)
            oWorkbookSource = oExcel.Workbooks.Open(sWorkbookFile)
            sWorkbookNameSource = sWorkbookFile
        Catch ex As Exception
            HandleException(ex, "OpenWorkbook")
            Return False
        End Try

        Return True

    End Function

    ''' <summary>
    ''' Closes a Workbook.
    ''' </summary>
    Sub CloseWorkingWorkbook()
        oTrace.TraceEvent(TraceEventType.Information, 1, "Entering HandlerExcel.CloseWorkbook")

        Try
            oWorkbookSource.Close()
        Catch ex As Exception
            HandleException(ex, "CloseWorkbook")
        End Try

    End Sub

    ''' <summary>
    ''' Saves the workbook as a PDF document
    ''' </summary>
    ''' <param name="sFilename">The pdf filename (including path).</param>  
    ''' <returns>True if the workbook was exported successfully, false if something went wrong</returns>  
    Public Function SaveAsPDF(ByVal sFilename As String) As Boolean
        oTrace.TraceEvent(TraceEventType.Information, 1, "Entering HandlerExcel.SaveAsPDF")

        Try
            'Save Workbook as PDF
            oWorkbookSource.ExportAsFixedFormat(Filename:=sFilename, Type:=XlFixedFormatType.xlTypePDF, IncludeDocProperties:=True)
            oTrace.TraceEvent(TraceEventType.Information, 1, "Saved PDF File: " & sFilename)

            'Excel adds a .pdf, if no extension is given. So check, if the file exists.
            If Not IO.File.Exists(sFilename) Then

                Try
                    My.Computer.FileSystem.RenameFile(sFilename & ".pdf", IO.Path.GetFileName(sFilename))
                Catch ex As Exception
                    oTrace.TraceEvent(TraceEventType.Critical, 12, "Rename failed. iSR will probably fail to locate the resulting pdf.")
                End Try

            End If

        Catch ex As Exception
            HandleException(ex, "SaveAsPDF")
            Return False
        End Try

        Return True

    End Function

    ''' <summary>
    ''' Creates a new Working worksheet, and renames the default Worksheet to "XM_FIRST"
    ''' </summary>
    Public Sub CreateNewWorkbook()
        oTrace.TraceEvent(TraceEventType.Information, 1, "Entering HandlerExcel.CreateNewWorkbook")

        oWorkbookSource = oExcel.Workbooks.Add()

        'Mark the first default worksheet 
        oWorkbookSource.Sheets(1).name = "XM_FIRST"

    End Sub

    Public Sub CreateWorkbookFromTemplate(sTemplateFilename As String)
        oTrace.TraceEvent(TraceEventType.Information, 1, "Entering HandlerExcel.CreateWorkbookFromTemplate")

        Dim oWorksheet As Worksheet
        Dim sFirstWSexists As Boolean = False

        oWorkbookSource = oExcel.Workbooks.Open(sTemplateFilename)

        'Check if there is a Worksheet calles XM_FIRST
        For Each oWorksheet In oWorkbookSource.Worksheets
            If oWorksheet.Name = "XM_FIRST" Then sFirstWSexists = True
            Exit For
        Next

        If sFirstWSexists = False Then
            oWorksheet = oWorkbookSource.Sheets.Add()
            'This is the last worksheet. rename it.
            oWorksheet.Name = "XM_FIRST"
        End If

    End Sub

    ''' <summary>
    ''' Saves the working Worksheet
    ''' </summary>
    ''' <param name="sFilename">Filename for the working Worksheet (incl. path)</param>  
    Public Sub SaveWorkbook(sFilename As String)
        oTrace.TraceEvent(TraceEventType.Information, 1, "Entering HandlerExcel.SaveWorkbook")

        oWorkbookSource.Sheets(1).Activate()

        oWorkbookSource.SaveAs(Filename:=sFilename)

    End Sub

    ''' <summary>
    ''' Imports a (html) file to a temporary Worksheet and transfers it to the working Worksheet
    ''' </summary>
    ''' <param name="oWorksheetXM">Metadata for the Worksheet, which has to be imported.</param>  
    Public Sub ImportWorksheet(oWorksheetXM As WorksheetXM)
        oTrace.TraceEvent(TraceEventType.Information, 1, "Entering HandlerExcel.ImportWorksheet")

        Dim oImportWorkbook As Workbook

        'Open the HTML File
        oImportWorkbook = oExcel.Workbooks.Open(oWorksheetXM.Filename)

        'Rename the tab 
        oImportWorkbook.Sheets(1).name = oWorksheetXM.Name

        'Add the description as custom property
        oImportWorkbook.Sheets(1).CustomProperties.Add(Name:="Description", Value:=oWorksheetXM.Description)

        'Move it at the end of the Worksheets
        oImportWorkbook.Sheets.Move(After:=oWorkbookSource.Sheets(oWorkbookSource.Sheets.Count))

    End Sub

    ''' <summary>
    ''' Removes the given named Worksheet
    ''' </summary>
    ''' <param name="sWorksheetName">Name of the worksheet to be removed</param>  
    Public Sub RemoveWorksheet(sWorksheetName As String)
        oTrace.TraceEvent(TraceEventType.Information, 1, "Entering HandlerExcel.sWorksheetName")

        'An Excel Workbook needs at least one worksheet
        If oWorkbookSource.Sheets.Count > 1 Then

            For i = 1 To oWorkbookSource.Sheets.Count

                If oWorkbookSource.Sheets(i).Name = sWorksheetName Then
                    oWorkbookSource.Sheets(i).delete()
                    Exit For
                End If

            Next
        Else
            oTrace.TraceEvent(TraceEventType.Warning, 12, "Cannot remove " & sWorksheetName & ", as it is the only worksheet in the workbook.")
        End If

    End Sub

    ''' <summary>
    ''' Creates the Table of Contents on the first sheet of the Workbook
    ''' </summary>
    ''' <param name="oWorkbookXM">WorkbookXM Object, which contains the metadata for the workbook</param>  
    Public Sub CreateTableOfContents(oWorkbookXM As WorkbookXM)
        oTrace.TraceEvent(TraceEventType.Information, 1, "Entering HandlerExcel.CreateTableOfContents")

        Dim oToCWS As Worksheet
        Dim oWorksheetXM As WorksheetXM
        Dim iLine As Integer
        Dim oWorksheet As Worksheet

        'Use the first Excel Sheet as ToC, as this is created by default anyway
        oToCWS = oWorkbookSource.Sheets(1)

        'Names for the toc. Yep, this should be done differently
        oToCWS.Name = My.Resources.ToC
        oToCWS.Cells(1, 1) = My.Resources.TableOfContents

        iLine = 3

        For Each oWorksheetXM In oWorkbookXM.oWorksheets

            oToCWS.Cells(iLine, 1) = "'" & oWorksheetXM.Name
            oToCWS.Cells(iLine, 2) = oWorksheetXM.Description

            'Create a hypelink to the target Worksheet
            oToCWS.Hyperlinks.Add(Anchor:=oToCWS.Cells(iLine, 1), Address:="", SubAddress:="'" & oWorksheetXM.Name & "'!A1", TextToDisplay:=oWorksheetXM.Name)

            'Insert two rows on the target Worksheet
            oWorksheet = oWorkbookSource.Sheets(oWorksheetXM.Name)
            oWorksheet.Rows(1).Resize(2).insert()

            'Create a hyperlink to the ToC
            oWorksheet.Hyperlinks.Add(Anchor:=oWorksheet.Cells(1, 1), Address:="", SubAddress:="'" & oToCWS.Name & "'!A1", TextToDisplay:=My.Resources.Back)

            iLine = iLine + 1

        Next

        'Propersize everything
        oToCWS.UsedRange.EntireColumn.AutoFit()

    End Sub

    ''' <summary>
    ''' writes all custom Properties to the word document
    ''' </summary>
    ''' <param name="oProperties">The Properties Object.</param>  
    Public Sub WriteCustomProperties(ByVal oProperties As Properties)
        oTrace.TraceEvent(TraceEventType.Information, 1, "Entering HandlerExcel.WriteCustomProperties")

        Dim sPropertyName As String
        Dim sPropertyValue As String
        Dim oCustomProperty As Object = Nothing

        Dim oCustomProperties As Object

        'Get the Custom Document Properties collection.
        oCustomProperties = oWorkbookSource.CustomDocumentProperties

        For Each sPropertyName In oProperties.GetAllKeys
            sPropertyValue = oProperties.GetProperty(sPropertyName)

            'Check if the doc property already exists
            Try
                oCustomProperty = oCustomProperties.item(sPropertyName)
            Catch ex As Exception
                oCustomProperty = Nothing
            End Try

            If oCustomProperty Is Nothing Then
                oCustomProperties.Add(sPropertyName, False, MsoDocProperties.msoPropertyTypeString, sPropertyValue)
            Else
                oCustomProperty.Value = sPropertyValue
            End If

            oCustomProperty = Nothing

        Next

    End Sub

    ''' <summary>
    ''' writes all worksheets of a workbook into single xlsx files
    ''' </summary>
    ''' <param name="sSavePath">The path, where the files are to be saved.</param>  
    Sub SaveOneFilePerWorksheet(sSavePath As String)
        oTrace.TraceEvent(TraceEventType.Information, 1, "Entering HandlerExcel.SaveOneFilePerWorksheet")

        Dim oWorksheet As Worksheet
        Dim oWorksheet1st As Worksheet
        Dim oNewWorkbook As Workbook

        Dim oCustomProperty As CustomProperty = Nothing
        Dim oProperty As CustomProperty
        Dim sFilename As String = String.Empty

        For Each oWorksheet In oWorkbookSource.Worksheets

            'Read the property to construct the filename
            sFilename = String.Empty

            Try
                For Each oProperty In oWorksheet.CustomProperties
                    If oProperty.Name = "Description" Then sFilename = oProperty.Value.ToString
                Next
            Catch ex As Exception
                oProperty = Nothing
                sFilename = String.Empty
            End Try

            If sFilename = String.Empty Then
                oTrace.TraceEvent(TraceEventType.Critical, 12, "Cannot create filename for " & oWorksheet.Name & ". Saving worksheet aborted.")
            Else

                oNewWorkbook = oExcel.Workbooks.Add()

                'Mark the first default worksheet 
                oNewWorkbook.Sheets(1).name = "XM_FIRST"

                'Copy the worksheet
                oWorksheet.Copy(After:=oNewWorkbook.Sheets(oNewWorkbook.Sheets.Count))

                'Delete the default worksheet
                For Each oWorksheet1st In oNewWorkbook.Worksheets
                    If oWorksheet1st.Name = "XM_FIRST" Then oWorksheet1st.Delete()
                    Exit For
                Next

                'Save the new workbook
                oTrace.TraceEvent(TraceEventType.Information, 1, "Saving " & sSavePath & "\" & sFilename & ".xlsx")
                oNewWorkbook.SaveAs(Filename:=sSavePath & "\" & sFilename & ".xlsx")

                oNewWorkbook.Close()

            End If

        Next

    End Sub

End Class