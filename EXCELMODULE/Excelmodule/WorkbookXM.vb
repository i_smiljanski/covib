﻿Public Class WorkbookXM

    Private sProtect As Boolean
    Private sPassword As String
    Private sFilename As String
    Private sCreateToC As Boolean

    Public oWorksheets As List(Of WorksheetXM) = Nothing
    Public oProperties As Properties = Nothing

    ''' <summary>
    ''' Contructor.
    ''' </summary>
    Public Sub New()
        oTrace.TraceEvent(TraceEventType.Information, 1, "Entering WorkbookXM.New")

        'Initialize the Hashtable
        oWorksheets = New List(Of WorksheetXM)
        oProperties = New Properties

    End Sub

    ''' <summary>
    ''' In absence of a 'real' destructor.
    ''' </summary>
    Public Sub CleanUp()
        oTrace.TraceEvent(TraceEventType.Information, 1, "Entering WorkbookXM.CleanUp")

        oWorksheets = Nothing
        oProperties = Nothing

    End Sub

    ''' <summary>
    ''' Protection Status for the workbook
    ''' </summary>
    Public Property Protect As Boolean
        Get
            Return sProtect
        End Get
        Set(value As Boolean)
            sProtect = value
        End Set
    End Property

    ''' <summary>
    ''' Password for the workbook, if protected
    ''' </summary>
    Public Property Password As String
        Get
            Return sPassword
        End Get
        Set(value As String)
            sPassword = value
        End Set
    End Property

    ''' <summary>
    ''' Filename of the workbook
    ''' </summary>
    Public Property Filename As String
        Get
            Return sFilename
        End Get
        Set(value As String)
            sFilename = value
        End Set
    End Property

    ''' <summary>
    ''' Create Table of Contents
    ''' </summary>
    Public Property CreateToC As String
        Get
            Return sCreateToC
        End Get
        Set(value As String)
            sCreateToC = value
        End Set
    End Property

    ''' <summary>
    ''' Add a Worksheet to the List of Worksheets
    ''' </summary>
    Public Sub AddWorksheet(oWorksheet As WorksheetXM)

        oWorksheets.Add(oWorksheet)

    End Sub

    ''' <summary>
    ''' Returns the specified Worksheet
    ''' </summary>
    ''' <returns>the worksheet specified by the index number</returns>  
    Public Function GetWorksheet(iWorksheetIndex As Integer)

        Return oWorksheets(iWorksheetIndex)

    End Function

    ''' <summary>
    ''' Returns the the number of worksheets saved in oWorksheet
    ''' </summary>
    ''' <returns>the worksheet count</returns>  
    Public Function CountWorksheets() As Integer

        Return oWorksheets.Count

    End Function

End Class
