﻿Imports System.Resources

Imports System
Imports System.Reflection
Imports System.Runtime.InteropServices

' General Information about an assembly is controlled through the following 
' set of attributes. Change these attribute values to modify the information
' associated with an assembly.

' Review the values of the assembly attributes

<Assembly: AssemblyTitle("Excelmodule")> 
<Assembly: AssemblyDescription("")> 
<Assembly: AssemblyCompany("up to data GmbH")> 
<Assembly: AssemblyProduct("Excelmodule")> 
<Assembly: AssemblyCopyright("Copyright ©  2019")> 
<Assembly: AssemblyTrademark("")> 

<Assembly: ComVisible(False)> 

'The following GUID is for the ID of the typelib if this project is exposed to COM
<Assembly: Guid("35253ab4-bb63-4306-9f6f-8d985af036fc")> 

' Version information for an assembly consists of the following four values:
'
'      Major Version
'      Minor Version 
'      Build Number
'      Revision
'
' You can specify all the values or you can default the Build and Revision Numbers 
' by using the '*' as shown below:
' <Assembly: AssemblyVersion("1.0.*")> 

<Assembly: AssemblyVersion("1.0.0.0")> 
<Assembly: AssemblyFileVersion("0.0.0.7")> 

<Assembly: NeutralResourcesLanguageAttribute("en-GB")> 