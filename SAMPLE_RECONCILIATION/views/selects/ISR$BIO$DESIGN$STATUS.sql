CREATE OR REPLACE VIEW ISR$BIO$DESIGN$STATUS (STUDYCODE, STATUS, HAS_DRAFT_DATA) AS (select s.studycode
      , status 
      , case when status='DRAFT' then 'T' else 'F' end HAS_DRAFT_DATA
   from (select studycode, status
         from isr$covib$designsample$status) s
      , (SELECT key studycode FROM isr$crit
          WHERE repid = stb$object.getcurrentrepid
            and Entity = 'BIO$STUDY') std
   where s.studycode = std.studycode)