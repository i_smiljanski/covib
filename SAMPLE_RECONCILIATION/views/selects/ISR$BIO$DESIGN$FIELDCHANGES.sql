CREATE OR REPLACE VIEW ISR$BIO$DESIGN$FIELDCHANGES (USERSAMPLEID, FIELDID, FIELDNAME, WATSON_ID, WATSON_VALUE, ISR_ID, ISR_VALUE) AS SELECT
    null as USERSAMPLEID
  , null as FIELDID
  , null as FIELDNAME
  , null as WATSON_ID
  , null as WATSON_VALUE
  , null as ISR_ID
  , null as ISR_VALUE
FROM dual
WHERE 0=1