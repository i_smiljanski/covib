CREATE OR REPLACE FORCE VIEW ISR$BIO$DESIGNSAMPLE$MODIFIED
(STUDYID, DESIGNSAMPLEID, DESIGNSUBJECTTREATMENTKEY, DESIGNSUBJECTID, SUBJECTGROUPID, 
 TREATMENTEVENTID, SAMPLETYPESPLITKEY, SAMPLETYPEKEY, SPLITNUMBER, SAMPLINGTIMEID, 
 STARTDAY, STARTHOUR, STARTMINUTE, STARTSECOND, ENDDAY, 
 ENDHOUR, ENDMINUTE, ENDSECOND, RELATIVEDAY, RELATIVEHOUR, 
 RELATIVEMINUTE, USERSAMPLEID, SAMPLESTATUS, SAMPLEVOLUME, SAMPLINGDATETIME, 
 VIALSAMPLEID, SAMPLEENTRYMODE, SAMPLEENTRYDATETIME, YEAR, MONTH, 
 WEEK, VISIT, TIMETEXT, RECORDTYPE, TREATMENTSITEID, 
 STATUS, REVISIONNUMBER, USERID, RECORDTIMESTAMP, USERSAMPLEIDKEY, 
 CONDITION2, RECEIPTDATE, RECEIPTUSERID, DISCREPANCY, ISOTOPE1KEY, 
 ISOTOPE2KEY, INFECTIOUSAGENTKEY, IDENTIFICATIONSTATUSKEY, SAMPLETEXTVARIABLE1, SAMPLETEXTVARIABLE2, 
 SAMPLETEXTVARIABLE3, OTHERCONDITIONKEY, TUBECONDITIONKEY, FROZENCONDITIONKEY, RADIOLABELED, 
 INFECTIOUS, INACTIVATIONTEMP, INACTIVATIONDATEIN, INACTIVATIONDATEOUT, COMMENTMEMO, 
 ISFORMULATION, EXTFTCOUNT)
AS
select ds.STUDYID                                                       as STUDYID
     , ds.DESIGNSAMPLEID                                                as DESIGNSAMPLEID
     , nvl(dst.DESIGNSUBJECTTREATMENTKEY, ds.DESIGNSUBJECTTREATMENTKEY) as DESIGNSUBJECTTREATMENTKEY
     , nvl(dst.DESIGNSUBJECTID, ds.DESIGNSUBJECTID)                     as DESIGNSUBJECTID
     , ds.SUBJECTGROUPID                                                as SUBJECTGROUPID
     , case when dst.DESIGNSUBJECTTREATMENTKEY is not null
            then dst.TREATMENTEVENTID  else ds.TREATMENTEVENTID end     as TREATMENTEVENTID
     , ds.SAMPLETYPESPLITKEY                                            as SAMPLETYPESPLITKEY
     , nvl(dst.SAMPLETYPEKEY, ds.SAMPLETYPEKEY)                         as SAMPLETYPEKEY
     , nvl(dst.SPLITNUMBER,ds.SPLITNUMBER)                              as SPLITNUMBER
     , ds.SAMPLINGTIMEID                                                as SAMPLINGTIMEID
     , nvl(dst.STARTDAY, ds.STARTDAY)                                   as STARTDAY
     , nvl(dst.STARTHOUR, ds.STARTHOUR)                                 as STARTHOUR
     , nvl(dst.STARTMINUTE, ds.STARTMINUTE)                             as STARTMINUTE
     , ds.STARTSECOND                                                   as STARTSECOND
     , nvl(dst.ENDDAY, ds.ENDDAY)                                       as ENDDAY
     , nvl(dst.ENDHOUR, ds.ENDHOUR)                                     as ENDHOUR
     , nvl(dst.ENDMINUTE, ds.ENDMINUTE)                                 as ENDMINUTE
     , ds.ENDSECOND                                                     as ENDSECOND
     , ds.RELATIVEDAY                                                   as RELATIVEDAY
     , ds.RELATIVEHOUR                                                  as RELATIVEHOUR
     , ds.RELATIVEMINUTE                                                as RELATIVEMINUTE
     , nvl(dst.USERSAMPLEID,ds.USERSAMPLEID)                            as USERSAMPLEID
     , nvl(dst.SAMPLESTATUS, ds.SAMPLESTATUS)                           as SAMPLESTATUS
     , ds.SAMPLEVOLUME                                                  as SAMPLEVOLUME
     , ds.SAMPLINGDATETIME                                              as SAMPLINGDATETIME
     , ds.VIALSAMPLEID                                                  as VIALSAMPLEID
     , ds.SAMPLEENTRYMODE                                               as SAMPLEENTRYMODE
     , ds.SAMPLEENTRYDATETIME                                           as SAMPLEENTRYDATETIME     
     , case when dst.DESIGNSUBJECTTREATMENTKEY is not null
            then dst.YEAR else ds.YEAR end                              as YEAR
     , case when dst.DESIGNSUBJECTTREATMENTKEY is not null
            then dst.MONTH else ds.MONTH end                            as MONTH
     , case when dst.DESIGNSUBJECTTREATMENTKEY is not null
            then dst.WEEK else ds.WEEK end                              as WEEK
     , case when dst.DESIGNSUBJECTTREATMENTKEY is not null 
            then dst.VISIT else null end                                as VISIT
     , nvl(dst.TIMETEXT, ds.TIMETEXT)                                   as TIMETEXT
     , ds.RECORDTYPE                                                    as RECORDTYPE
     , ds.TREATMENTSITEID                                               as TREATMENTSITEID
     , ds.STATUS                                                        as STATUS
     , ds.REVISIONNUMBER                                                as REVISIONNUMBER
     , ds.USERID                                                        as USERID
     , ds.RECORDTIMESTAMP                                               as RECORDTIMESTAMP
     , ds.USERSAMPLEIDKEY                                               as USERSAMPLEIDKEY
     , ds.CONDITION2                                                    as CONDITION2
     , ds.RECEIPTDATE                                                   as RECEIPTDATE
     , ds.RECEIPTUSERID                                                 as RECEIPTUSERID
     , ds.DISCREPANCY                                                   as DISCREPANCY
     , ds.ISOTOPE1KEY                                                   as ISOTOPE1KEY
     , ds.ISOTOPE2KEY                                                   as ISOTOPE2KEY
     , ds.INFECTIOUSAGENTKEY                                            as INFECTIOUSAGENTKEY
     , ds.IDENTIFICATIONSTATUSKEY                                       as IDENTIFICATIONSTATUSKEY
     , nvl(dst.SAMPLETEXTVARIABLE1, ds.SAMPLETEXTVARIABLE1)             as SAMPLETEXTVARIABLE1
     , nvl(dst.SAMPLETEXTVARIABLE2, ds.SAMPLETEXTVARIABLE2)             as SAMPLETEXTVARIABLE2
     , nvl(dst.SAMPLETEXTVARIABLE3, ds.SAMPLETEXTVARIABLE3)             as SAMPLETEXTVARIABLE3
     , ds.OTHERCONDITIONKEY                                             as OTHERCONDITIONKEY
     , ds.TUBECONDITIONKEY                                              as TUBECONDITIONKEY
     , ds.FROZENCONDITIONKEY                                            as FROZENCONDITIONKEY
     , ds.RADIOLABELED                                                  as RADIOLABELED
     , ds.INFECTIOUS                                                    as INFECTIOUS
     , ds.INACTIVATIONTEMP                                              as INACTIVATIONTEMP
     , ds.INACTIVATIONDATEIN                                            as INACTIVATIONDATEIN
     , ds.INACTIVATIONDATEOUT                                           as INACTIVATIONDATEOUT
     , ds.COMMENTMEMO                                                   as COMMENTMEMO
     , ds.ISFORMULATION                                                 as ISFORMULATION
     , ds.EXTFTCOUNT                                                    as EXTFTCOUNT
from &&LIMS_USER&.designsample ds,
    (select dc.studycode, dc.designsampleid, dc.designsubjecttreatmentkey,
            dsti.treatmenteventid, dsti.month, dsti.week, dsti.visittext as visit, dsti.year, dc.designsubjectid,
            dc.SAMPLETEXTVARIABLE1, dc.SAMPLETEXTVARIABLE2, dc.SAMPLETEXTVARIABLE3, 
            dc.TIMETEXT, dc.SAMPLETYPEKEY, dc.SPLITNUMBER, 
            dc.STARTDAY, dc.STARTHOUR, dc.STARTMINUTE, dc.ENDDAY, dc.ENDHOUR, dc.ENDMINUTE,
            dc.USERSAMPLEID, dc.SAMPLESTATUS            
       from &&LIMS_USER&.designsubjecttreatment dsti,
            ISR$COVIB$DESIGNSAMPLE$CHANGES dc
      where dsti.designsubjecttreatmentkey = dc.designsubjecttreatmentkey 
        and dc.designsubjecttreatmentkey is not null
        and dsti.studyid = dc.studycode
      union all
     select dc.studycode, dc.designsampleid, dc.designsubjecttreatmentkey,
            null as treatmenteventid, null as month, null as week, null as visit, null as year, dc.DESIGNSUBJECTID,
            dc.SAMPLETEXTVARIABLE1, dc.SAMPLETEXTVARIABLE2, dc.SAMPLETEXTVARIABLE3, 
            dc.TIMETEXT, dc.SAMPLETYPEKEY, dc.SPLITNUMBER, 
            dc.STARTDAY, dc.STARTHOUR, dc.STARTMINUTE, dc.ENDDAY, dc.ENDHOUR, dc.ENDMINUTE,
            dc.USERSAMPLEID, dc.SAMPLESTATUS
       from ISR$COVIB$DESIGNSAMPLE$CHANGES dc
     where  dc.designsubjecttreatmentkey is null) dst
where ds.studyid = dst.studycode(+)
  and ds.designsampleid = dst.designsampleid(+)