begin
  for rGetTable in (select count(*) num from user_objects where object_name='ISR$COVIB$DESIGNSAMPLE$CHANGES' and object_type = 'TABLE') loop
    if rGetTable.num < 1 then
      execute immediate 'CREATE TABLE ISR$COVIB$DESIGNSAMPLE$CHANGES
      (
        CODE                       VARCHAR2(100 BYTE),
        STUDYCODE                  INTEGER            NOT NULL,
        DESIGNSUBJECTID            INTEGER,
        DESIGNSUBJECTTREATMENTKEY  INTEGER,
        TREATMENTKEY               INTEGER,
        USERSAMPLEID               VARCHAR2(255 BYTE) NOT NULL,
        ENDDAY                     INTEGER,
        ENDHOUR                    NUMBER,
        ENDMINUTE                  INTEGER,
        STARTDAY                   INTEGER,
        STARTHOUR                  NUMBER,
        STARTMINUTE                INTEGER,
        SAMPLESTATUS               INTEGER,
        SAMPLETEXTVARIABLE1        VARCHAR2(100 BYTE),
        SAMPLETEXTVARIABLE2        VARCHAR2(100 BYTE),
        SAMPLETEXTVARIABLE3        VARCHAR2(100 BYTE),
        SPLITNUMBER                INTEGER,
        TIMETEXT                   VARCHAR2(50 BYTE),
        PLUGIN                     VARCHAR2(500 BYTE),
        DESIGNSAMPLEID             INTEGER            NOT NULL,
        SAMPLETYPEKEY              INTEGER
      )';
    else
      for rGetCol1 in (select count(*) num from user_tab_columns where column_name ='SAMPLETYPEKEY' and table_name = 'ISR$COVIB$DESIGNSAMPLE$CHANGES') loop
        if rGetCol1.num < 1 then
          execute immediate 'alter table ISR$COVIB$DESIGNSAMPLE$CHANGES add (SAMPLETYPEKEY INTEGER)';
        end if;
      end loop;
     
    end if;
  end loop;
end;