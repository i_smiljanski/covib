CREATE OR REPLACE VIEW ISR$BIO$DESIGNSAMPLE$CHANGES
(CODE, DESIGNSAMPLEID, STUDYCODE, USERSAMPLEID, DESIGNSUBJECTID_ORG, 
 DESIGNSUBJECTID, DESIGNSUBJECTTAG_ORG, STUDYSUBJECTTAG_ORG, GENDERID_ORG, DESIGNSUBJECTTREATMENTKEY_ORG, 
 DESIGNSUBJECTTREATMENTKEY, SUBJECTGROUPNAME_ORG, VISIT_ORG, VISIT, PERIOD_ORG, 
 PERIOD, WEEK_ORG, WEEK, STUDYDAY_ORG, STUDYDAY, 
 TREATMENTKEY_ORG, TREATMENTKEY, TREATMENTDESC_ORG, TREATMENTID_ORG, ENDDAY_ORG, 
 ENDDAY, ENDHOUR_ORG, ENDHOUR, ENDMINUTE_ORG, ENDMINUTE, 
 STARTDAY_ORG, STARTDAY, STARTHOUR_ORG, STARTHOUR, STARTMINUTE_ORG, 
 STARTMINUTE, SAMPLESTATUS_ORG, SAMPLESTATUS, SAMPLESTATUSNAME_ORG, SAMPLETEXTVARIABLE1_ORG, 
 SAMPLETEXTVARIABLE1, SAMPLETEXTVARIABLE2_ORG, SAMPLETEXTVARIABLE2, SAMPLETEXTVARIABLE3_ORG, SAMPLETEXTVARIABLE3, 
 SPLITNUMBER_ORG, SPLITNUMBER, TIMETEXT_ORG, TIMETEXT, SAMPLETYPEKEY_ORG, 
 SAMPLETYPEKEY, SAMPLETYPEID_ORG)
AS 
select dc.code                       as CODE
     , dc.DESIGNSAMPLEID             as DESIGNSAMPLEID
     , dc.STUDYCODE                  as STUDYCODE 
     , ds.USERSAMPLEID               as USERSAMPLEID    
     , ds.DESIGNSUBJECTID            as DESIGNSUBJECTID_ORG
     , dc.DESIGNSUBJECTID            as DESIGNSUBJECTID
     , ds.DESIGNSUBJECTTAG           as DESIGNSUBJECTTAG_ORG -- for simple help
     , ds.STUDYSUBJECTTAG            as STUDYSUBJECTTAG_ORG -- for simple help
     , ds.GENDERID                   as GENDERID_ORG -- for simple help
     , ds.DESIGNSUBJECTTREATMENTKEY  as DESIGNSUBJECTTREATMENTKEY_ORG
     , dc.DESIGNSUBJECTTREATMENTKEY  as DESIGNSUBJECTTREATMENTKEY
     , ds.SUBJECTGROUPNAME           as SUBJECTGROUPNAME_ORG -- for simple help
     , ds.VISIT                      as VISIT_ORG -- for simple help
     , dst.VISITTEXT                 as VISIT
     , ds.PERIOD                     as PERIOD_ORG -- for simple help
     , dst.PERIOD                    as PERIOD
     , ds.WEEK                       as WEEK_ORG -- for simple help
     , dst.WEEK                      as WEEK
     , ds.STUDYDAY                   as STUDYDAY_ORG -- for simple help
     , dst.STUDYDAY                  as STUDYDAY     
     , ds.TREATMENTKEY               as TREATMENTKEY_ORG
     , dc.TREATMENTKEY               as TREATMENTKEY
     , ds.TREATMENTDESC              as TREATMENTDESC_ORG -- for simple help
     , ds.TREATMENTID                as TREATMENTID_ORG -- for simple help
     , ds.ENDDAY                     as ENDDAY_ORG
     , dc.ENDDAY                     as ENDDAY
     , ds.ENDHOUR                    as ENDHOUR_ORG
     , dc.ENDHOUR                    as ENDHOUR
     , ds.ENDMINUTE                  as ENDMINUTE_ORG
     , dc.ENDMINUTE                  as ENDMINUTE
     , ds.STARTDAY                   as STARTDAY_ORG
     , dc.STARTDAY                   as STARTDAY
     , ds.STARTHOUR                  as STARTHOUR_ORG
     , dc.STARTHOUR                  as STARTHOUR
     , ds.STARTMINUTE                as STARTMINUTE_ORG
     , dc.STARTMINUTE                as STARTMINUTE     
     , ds.SAMPLESTATUS               as SAMPLESTATUS_ORG
     , dc.SAMPLESTATUS               as SAMPLESTATUS
     , ds.SAMPLESTATUSNAME           as SAMPLESTATUSNAME_ORG  -- for simple help
     , ds.SAMPLETEXTVARIABLE1        as SAMPLETEXTVARIABLE1_ORG                                   
     , dc.SAMPLETEXTVARIABLE1        as SAMPLETEXTVARIABLE1
     , ds.SAMPLETEXTVARIABLE2        as SAMPLETEXTVARIABLE2_ORG
     , dc.SAMPLETEXTVARIABLE2        as SAMPLETEXTVARIABLE2
     , ds.SAMPLETEXTVARIABLE3        as SAMPLETEXTVARIABLE3_ORG
     , dc.SAMPLETEXTVARIABLE3        as SAMPLETEXTVARIABLE3
     , ds.SPLITNUMBER                as SPLITNUMBER_ORG
     , dc.SPLITNUMBER                as SPLITNUMBER
     , ds.TIMETEXT                   as TIMETEXT_ORG
     , dc.TIMETEXT                   as TIMETEXT
     , ds.SAMPLETYPEKEY              as SAMPLETYPEKEY_ORG
     , dc.SAMPLETYPEKEY              as SAMPLETYPEKEY
     , ds.SAMPLETYPEID               as SAMPLETYPEID_ORG 
from ISR$BIO$DESIGNSAMPLE ds,
     ISR$COVIB$DESIGNSAMPLE$CHANGES dc,
     &&LIMS_USER&.designsubjecttreatment dst
where ds.code = dc.code
  and dst.designsubjecttreatmentkey(+) = dc.designsubjecttreatmentkey 
  and dst.studyid(+) = dc.studycode