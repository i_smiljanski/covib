CREATE OR REPLACE FORCE VIEW ISR$BIO$DESIGNSAMPLE
(CODE, DESIGNSAMPLEID, STUDYCODE, USERSAMPLEID, ENDDAY, 
 ENDHOUR, ENDMINUTE, STARTDAY, STARTHOUR, STARTMINUTE, 
 SAMPLESTATUS, SAMPLESTATUSNAME, SAMPLETEXTVARIABLE1, SAMPLETEXTVARIABLE2, SAMPLETEXTVARIABLE3, SPLITNUMBER, 
 TIMETEXT, DESIGNSUBJECTID, GENDERID, PLACEBOFLAG, DESIGNSUBJECTTAG, 
 STUDYSUBJECTTAG, SUBJECTNUMBERVARIABLE1, SUBJECTNUMBERVARIABLE2, SUBJECTNUMBERVARIABLE3, SUBJECTNUMBERVARIABLE4, 
 SUBJECTNUMBERVARIABLE5, SUBJECTNUMBERVARIABLE6, SUBJECTNUMBERVARIABLE7, SUBJECTNUMBERVARIABLE8, SUBJECTTEXTVARIABLE1, 
 SUBJECTTEXTVARIABLE2, SUBJECTTEXTVARIABLE3, SUBJECTTEXTVARIABLE4, SUBJECTTEXTVARIABLE5, SUBJECTTEXTVARIABLE6, 
 SUBJECTTEXTVARIABLE7, SUBJECTTEXTVARIABLE8, DESIGNSUBJECTTREATMENTKEY, MONTH, PERIOD, STUDYDAY, 
 VISIT, WEEK, SUBJECTGROUPNAME, TREATMENTKEY, DOSEAMOUNT, 
 DOSEUNITSDESCRIPTION, PLACEBOTREATMENT, TREATMENTDESC, TREATMENTID, SAMPLETYPEKEY,
 SAMPLETYPEID,SAMPLETYPEABBREVIATION)
BEQUEATH DEFINER
AS 
select  ds.studyid || '-' || ds.DESIGNSAMPLEID as CODE
      , ds.DESIGNSAMPLEID as DESIGNSAMPLEID
      , ds.studyid                             as STUDYCODE
      , ds.usersampleid                        as USERSAMPLEID        -- Custom ID
      , ds.endday                              as ENDDAY              -- Day Nominal
      , ds.endhour                             as ENDHOUR             -- Hour Nominal
      , ds.endminute                           as ENDMINUTE           -- Minute Nominal
      , ds.startday                            as STARTDAY 
      , ds.starthour                           as STARTHOUR
      , ds.startminute                         as STARTMINUTE
      , ds.samplestatus                        as SAMPLESTATUS        -- Sample Condition
      , (select name from isr$bio$samplestatus st where st.code = ds.samplestatus  )  as SAMPLESTATUSNAME
      , ds.sampletextvariable1                 as SAMPLETEXTVARIABLE1 -- Sample User Text 1
      , ds.sampletextvariable2                 as SAMPLETEXTVARIABLE2 -- Sample User Text 1
      , ds.sampletextvariable3                 as SAMPLETEXTVARIABLE3 -- Sample User Text 1
      , ds.splitnumber                         as SPLITNUMBER         -- Split
      , ds.timetext                            as TIMETEXT            -- Time text
      , dsb.designsubjectid                    as DESIGNSUBJECTID
      , dsb.genderid                           as GENDERID
      , dsb.placeboflag                        as PLACEBOFLAG
      , dsb.designsubjecttag                   as DESIGNSUBJECTTAG
      , dsb.studysubjecttag                    as STUDYSUBJECTTAG
      , dsb.subjectnumbervariable1             as SUBJECTNUMBERVARIABLE1
      , dsb.subjectnumbervariable2             as SUBJECTNUMBERVARIABLE2
      , dsb.subjectnumbervariable3             as SUBJECTNUMBERVARIABLE3
      , dsb.subjectnumbervariable4             as SUBJECTNUMBERVARIABLE4
      , dsb.subjectnumbervariable5             as SUBJECTNUMBERVARIABLE5
      , dsb.subjectnumbervariable6             as SUBJECTNUMBERVARIABLE6
      , dsb.subjectnumbervariable7             as SUBJECTNUMBERVARIABLE7
      , dsb.subjectnumbervariable8             as SUBJECTNUMBERVARIABLE8
      , dsb.subjecttextvariable1               as SUBJECTTEXTVARIABLE1
      , dsb.subjecttextvariable2               as SUBJECTTEXTVARIABLE2
      , dsb.subjecttextvariable3               as SUBJECTTEXTVARIABLE3
      , dsb.subjecttextvariable4               as SUBJECTTEXTVARIABLE4
      , dsb.subjecttextvariable5               as SUBJECTTEXTVARIABLE5
      , dsb.subjecttextvariable6               as SUBJECTTEXTVARIABLE6
      , dsb.subjecttextvariable7               as SUBJECTTEXTVARIABLE7
      , dsb.subjecttextvariable8               as SUBJECTTEXTVARIABLE8
      , dst.designsubjecttreatmentkey          as DESIGNSUBJECTTREATMENTKEY
      , dst.month                              as MONTH
      , dst.period                             as PERIOD
      , dst.studyday                           as STUDYDAY
      , dst.visittext                          as VISIT
      , dst.week                               as WEEK
      , (select subjectgroupname from &&LIMS_USER&.designsubjectgroup dsg 
           where dst.studyid = dsg.studyid and dst.subjectgroupid = dsg.subjectgroupid) as SUBJECTGROUPNAME 
      , dt.treatmentkey                        as TREATMENTKEY
      , dt.doseamount                          as DOSEAMOUNT
      , (select doseunitsdescription from &&LIMS_USER&.doseunits du 
           where dt.doseunitsid = du.doseunitsid ) as DOSEUNITSDESCRIPTION
      , dt.placeboflag                         as PLACEBOFLAG
      , dt.treatmentdesc                       as TREATMENTDESC
      , dt.treatmentid                         as TREATMENTID
      , ds.sampletypekey                       as SAMPLETYPEKEY
      , st.sampletypeid                        as SAMPLETYPEID
      , st.sampletypeabbreviation              as SAMPLETYPEABBREVIATION
from &&LIMS_USER&.designsample ds, 
     &&LIMS_USER&.designsubject dsb, 
     &&LIMS_USER&.designsubjecttreatment dst,
     &&LIMS_USER&.designtreatment dt,
     &&LIMS_USER&.configsampletypes st         
where ds.studyid = dsb.studyid
  and ds.designsubjectid = dsb.designsubjectid
  and ds.studyid = dst.studyid
  and ds.designsubjecttreatmentkey = dst.designsubjecttreatmentkey
  and dst.studyid = dt.studyid
  and dst.treatmentkey = dt.treatmentkey
  and ds.sampletypekey  = st.sampletypekey