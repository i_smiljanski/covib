CREATE OR REPLACE PACKAGE ISR$CONTEXTMENU$SAMPLE$RECONCIL
as
-- ***************************************************************************************************
-- Description/Usage:
-- The package contains all functions for creating a context menu for tokens with handleby = 'ISR$CONTEXTMENU$SAMPLE$RECONCIL'
--
-- ***************************************************************************************************

PROCEDURE destroyOldValues;
-- ***************************************************************************************************
-- Date and Author: 30 Jan 2006  JB
-- destroys the old values of the node object when a new node is set in the Explorer
--
-- ***************************************************************************************************
    
FUNCTION setMenuAccess(sParameter in VARCHAR2, sMenuAllowed OUT VARCHAR2)  RETURN STB$OERROR$RECORD;
-- ***************************************************************************************************
-- Date and Autor:  15.Feb 2006, MCD
-- checks if a command token is selectable in the context menu. If a command token can be
-- selected depends on the context of the node and the activiated system privileges of the authorization
-- groups the user belongs to
--
-- Parameter:
-- sParameter            the command token of the context menu
-- sMenuAllowed          flag to indicate if the context command token is selectable in the context menu
--
-- Return:
-- STB$OERROR$RECORD    
--
-- ***************************************************************************************************

FUNCTION callFunction(oParameter in STB$MENUENTRY$RECORD,  olNodeList OUT STB$TREENODELIST)    RETURN STB$OERROR$RECORD;
-- ***************************************************************************************************
-- Date und Autor: 02 Feb 2006 JB
-- is the interface to process a command token selected in the context menu.
-- The function checks if the command token can be executed and calls the backend functionality
-- to process the token. If further information is required to process the command token a node object
-- is returned to display a dialog window
--
-- Parameter:
-- oParameter            the command token selected in the context menu
-- olNodeList            a node list describing a dialog winodw when further user information is required 
--
-- Return:
-- STB$OERROR$RECORD    
--
-- ***************************************************************************************************

FUNCTION getLov(sEntity in VARCHAR2, sSuchwert in VARCHAR2, oSelectionList OUT ISR$TLRSELECTION$LIST) RETURN STB$OERROR$RECORD;
-- ***************************************************************************************************
-- Date und Autor: 02 Feb 2006  JB
-- returns the list of values of an entity
--
-- Parameter:
-- sEntity            the entity to which the list of values belongs 
-- sSuchwert          the search string to filter / limit the values in the list
-- oSelectionList     the list of values  
--
-- Return:
-- STB$OERROR$RECORD    
--
-- ***************************************************************************************************


FUNCTION putParameters(oParameter in STB$MENUENTRY$RECORD, olParameter in STB$PROPERTY$LIST)  RETURN STB$OERROR$RECORD;
-- ***************************************************************************************************
-- Date und Autor: 02 Feb 2006  JB
-- is the counterpart function of "callFunction", by using "putParameter" a
-- command token is procssed from a called dialog window, 
-- the entered information (e.g. esig , filter ) will be considered when processing the command token
--
-- Parameter:
-- oParameter      the command token selected in the context menu
-- olParameter     the dialog window with all attributes
--
-- Return:
-- STB$OERROR$RECORD   
--
-- ***************************************************************************************************

FUNCTION checkDependenciesOnMask( oParameter IN STB$MENUENTRY$RECORD, olParameter IN OUT STB$PROPERTY$LIST, sModifiedFlag OUT VARCHAR2 ) RETURN STB$OERROR$RECORD;
-- ***************************************************************************************************
-- Date and Autor:  16.Jun 2006, MCD
-- checks the dependencies on the window and acts accordingly
-- e.g.: if one field in the window is selected the next field is not editable
--       or if one field in the window has a certain value the next field is not displayed 
--
-- Parameter :
-- oParameter         the  menu entry that was called 
-- olParameter        the attributes of the dialog window
-- sModifiedFlag      flag to indicate if an attribute  was modiifed
--
-- Return:
-- STB$OERROR$RECORD  
--
-- ***************************************************************************************************

end ISR$CONTEXTMENU$SAMPLE$RECONCIL; 