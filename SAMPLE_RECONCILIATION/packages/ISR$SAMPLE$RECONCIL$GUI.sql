CREATE OR REPLACE PACKAGE ISR$SAMPLE$RECONCIL$GUI
as

TYPE outCursor IS REF CURSOR;

procedure getStudyList(csLdapUser in varchar2, sStudyStatus in VARCHAR2 default null, outList out outCursor);

function getStudyName(sStudyCode in VARCHAR2) return varchar2;

procedure getFullDesignSampleList(sStudyCode in varchar2, outList out outCursor);

procedure getSampleStatusList(outList out outCursor);

procedure getSampleTypesList(outList out outCursor);

procedure getDesignSampleChangesList(sStudyCode in varchar2, outList out outCursor);

procedure saveDesignSampleChanges(ccode in varchar2, cstudycode in integer, 
           cdesignsubjectid in integer default null, cdesignsubjecttreatmentkey  integer default null, 
           ctreatmentkey in integer default null, cusersampleid in varchar2 default null, cendday in integer default null, 
           cendhour in number default null, cendminute in integer default null, 
           cstartday in integer default null, cstarthour in number default null, 
           cstartminute in integer default null, csamplestatus in integer default null, 
           csampletextvariable1 in varchar2 default null, csampletextvariable2 in varchar2 default null, 
           csampletextvariable3 in varchar2 default null, 
           csplitnumber in integer default null, ctimetext in  varchar2 default null,
           cDesignSampleId in number, cSampleTypeKey in number);

procedure resetDesignSampleChanges(csCode in varchar2);

procedure publishDesignSampleChanges(csCode in varchar2, errorList out outCursor);

function insertRowDataXmlToRemoteTable ( csConfXml IN CLOB, csDataXml IN CLOB, csServerId  IN VARCHAR2, csLoglevel  IN VARCHAR2,  csLogReference  IN VARCHAR2, nTimeout in NUMBER ) RETURN CLOB;

procedure writeAudit(ccode in varchar2, cstudycode in integer, csStatus in varchar2, cAction in varchar2,
           cdesignsubjectid in integer default null, 
           cdesignsubjecttreatmentkey  integer default null, 
           ctreatmentkey in integer default null, 
           cusersampleid in varchar2 default null, 
           cendday in integer default null, cendhour in number default null, 
           cendminute in integer default null, cstartday in integer default null, 
           cstarthour in number default null, cstartminute in integer default null, 
           csamplestatus in integer default null, 
           csampletextvariable1 in varchar2 default null, 
           csampletextvariable2 in varchar2 default null, 
           csampletextvariable3 in varchar2 default null, 
           csplitnumber in integer default null, 
           ctimetext in  varchar2 default null,
           cDesignSampleId in number default null,
           cSampleTypeKey in number default null);
-- ***************************************************************************************************
-- Date and Autor:  23.Nov 2021, MCD
-- writes the audit 
--
-- Parameter :
-- ccode         the code or FULL if a study code is submitted 
-- cstudycode    the study code
-- csStatus      the status DRAFT/PUBLISHED/APPROVED
-- cAction       the action save/reset/publish/approve
--
-- Return:
-- -
--
-- ***************************************************************************************************

end ISR$SAMPLE$RECONCIL$GUI;