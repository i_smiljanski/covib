CREATE OR REPLACE PACKAGE BODY ISR$CONTEXTMENU$SAMPLE$RECONCIL IS


  --****************************************************************************************************
  PROCEDURE destroyOldValues IS
  BEGIN
    STB$OBJECT.setCurrentToken('');
  END destroyOldValues;



  --**************************************************************************************************************************
  function setMenuAccess( sParameter   in  varchar2,
                          sMenuAllowed out varchar2 )
    return STB$OERROR$RECORD
  is
    oErrorObj                     STB$OERROR$RECORD := STB$OERROR$RECORD();
    sCurrentName                  constant varchar2(100) := $$PLSQL_UNIT||'.setMenuAccess('||sParameter||')';
  BEGIN
    isr$trace.stat('begin', 'sParameter: '||sParameter, sCurrentName);

    sMenuAllowed := Stb$security.checkGroupRight (sParameter);

    isr$trace.stat('end', 'sMenuAllowed: '||sMenuAllowed, sCurrentName);
    return oErrorObj;
  exception
    WHEN OTHERS THEN
      oErrorObj.handleError ( Stb$typedef.cnSeverityCritical, utd$msglib.getmsg ('exUnexpectedError', stb$security.getCurrentlanguage,  csP1 => sqlerrm(sqlcode)),
        sCurrentName, dbms_utility.format_error_stack ||chr(10)|| dbms_utility.format_error_backtrace );
      RETURN oErrorObj;
  END setMenuAccess;



  --**********************************************************************************************************************************
  FUNCTION callFunction (oParameter IN STB$MENUENTRY$RECORD, olNodeList OUT STB$TREENODELIST)
    RETURN STB$OERROR$RECORD IS
    oErrorObj                     STB$OERROR$RECORD := STB$OERROR$RECORD();
    sCurrentName  constant varchar2(100) := $$PLSQL_UNIT||'.callFunction ('||oParameter.sToken||')';
    exNotExists                   EXCEPTION;
    olValueList             isr$value$list := isr$value$list();
    sToken            varchar2(4000);
    
    cursor cIsOracleUser(sLdapUser in varchar2) is
      select 'T'
        from stb$user
       where UPPER (oracleusername) = UPPER (sLdapUser);
    sIsOracleuser varchar2(1) := 'F';
  
  BEGIN
    isr$trace.stat ('begin',  'parameter: ' || oParameter.sToken, sCurrentName);
    sToken := oParameter.sToken;
    case
    when upper (stoken) = 'CAN_CREATE_SAMPLE_RECONCILIATIONS'
        or  upper (stoken) = 'CAN_PUBLISH_SAMPLE_RECONCILIATIONS' then

      STB$OBJECT.setCurrentToken(stoken);
                 
      olValueList.extend;
      olValueList(olValueList.count()) := ISR$VALUE$RECORD('process', 'SamRec.exe');
      
      for rGetUserLdapName in (select nvl(ldapusername, oracleusername) ldapusername from stb$user where userno = stb$security.getCurrentUser) loop
        olValueList.extend;
        olValueList(olValueList.count()) := ISR$VALUE$RECORD('LdapUserName', rGetUserLdapName.ldapusername);
        
        isr$trace.debug('rGetUserLdapName.ldapusername', rGetUserLdapName.ldapusername, sCurrentName);     
        open cIsOracleUser(rGetUserLdapName.ldapusername);
        fetch cIsOracleUser into sIsOracleuser;
        close cIsOracleUser;
        isr$trace.debug('sIsOracleuser', sIsOracleuser, sCurrentName);       
        -- initalize of the user
        if sIsOracleuser = 'F' then
          Stb$security.initUser(sIsOracleuser);
        end if; 
       
      end loop;
      
      olValueList.extend;
      olValueList(olValueList.count()) := ISR$VALUE$RECORD('StudyStatus', case when stoken like '%PUBLISH%' then 'DRAFT' else '' end);
          
      olNodeList := STB$TREENODELIST ();
      olNodeList.extend;
      olNodeList(1) := STB$TREENODE$RECORD( STB$UTIL.getMaskTitle(sToken ), 
                                            null, 'TRANSPORT', 'PROCESS', null, null, null, null, olValueList, 
                                            STB$TYPEDEF.cnCallProcess );
    when upper (stoken) = 'CAN_APPROVE_SAMPLE_RECONCILIATIONS' then
      olNodeList := STB$TREENODELIST ();
      olNodeList.EXTEND;
      olNodeList (1) := STB$TREENODE$RECORD(STB$UTIL.getMaskTitle(Upper (sToken)),
                                            $$PLSQL_UNIT, 'TRANSPORT', 'MASK');
      oErrorObj := STB$UTIL.getPropertyList(upper (sToken), olNodeList (1).tloPropertylist);
      isr$trace.debug('olNodeList(1).tloPropertylist', 's. logclob', sCurrentName, olNodeList(1).tloPropertylist);
    else
      oErrorObj := stb$system.callFunction(oParameter,olNodeList);
    end case;
    
    isr$trace.debug('olNodeList','->',sCurrentName,olNodeList);
    isr$trace.stat ('end', 'end', sCurrentName);
    RETURN oErrorObj;
  EXCEPTION
    WHEN exNotExists THEN
      oErrorObj.handleError ( Stb$typedef.cnSeverityCritical, utd$msglib.getmsg ('exNotExistsText', stb$security.getCurrentlanguage,  csP1 => sqlerrm(sqlcode)),
        sCurrentName, dbms_utility.format_error_stack ||chr(10)|| dbms_utility.format_error_backtrace );
      RETURN oErrorObj;
    WHEN OTHERS THEN
      ROLLBACK;
      oErrorObj.handleError ( Stb$typedef.cnSeverityCritical, utd$msglib.getmsg ('exUnexpectedError', stb$security.getCurrentlanguage,  csP1 => sqlerrm(sqlcode)),
        sCurrentName, dbms_utility.format_error_stack ||chr(10)|| dbms_utility.format_error_backtrace );
      RETURN oErrorObj;
  END callFunction;

  --**********************************************************************************************************************************
  FUNCTION getLov (sEntity IN VARCHAR2, sSuchwert IN VARCHAR2, oSelectionList OUT ISR$TLRSELECTION$LIST)
    RETURN STB$OERROR$RECORD IS
    sCurrentName   constant varchar2(4000) := $$PLSQL_UNIT||'.getLov('||sEntity||')';
    oErrorObj                     STB$OERROR$RECORD := STB$OERROR$RECORD();
    
    cStudyList ISR$SAMPLE$RECONCIL$GUI.outCursor;
    sName   tmp$bio$study.Name%type;
    sCode   tmp$bio$study.Code%type;
    sTitle  tmp$bio$study.Title%type;
   
    CURSOR cGetGrouping IS
      SELECT groupingname, description, groupingflag
        FROM isr$grouping$view
       WHERE selected = 'T'
      ORDER BY 3, 2;
      
    CURSOR cGetXmlFilter(xXml IN XMLTYPE, csFilter IN VARCHAR2) is
    SELECT DISTINCT EXTRACTVALUE (
                       COLUMN_VALUE
                     , CASE
                          WHEN csFilter LIKE '@%' THEN 'node()/' || csFilter
                          ELSE csFilter
                       END
                    ) VALUE
      FROM table(XMLSEQUENCE(EXTRACT (
                                (xXml), '//'|| CASE
                                                  WHEN csFilter LIKE '@%' THEN 'node()[' || csFilter || ']'
                                                  ELSE csFilter
                                               END
                             )))
    ORDER BY 1;       
    
  BEGIN
    isr$trace.stat('begin', 'begin', sCurrentName);
    oSelectionList := ISR$TLRSELECTION$LIST ();
    CASE
    --***************************************
    WHEN upper (sEntity) = 'STUDYLIST' THEN
      --delete from tmp$bio$study;
      
      ISR$SAMPLE$RECONCIL$GUI.getStudyList(
         nvl(STB$SECURITY.getCurrentUsername(), STB$SECURITY.getOracleUserName(STB$SECURITY.getCurrentUser())), 
         'PUBLISHED', cStudyList);
      
      loop fetch cStudyList into  sName, sCode, sTitle;
      exit when cStudyList%NOTFOUND;     
        oSelectionList.EXTEND;
        oSelectionList (oSelectionList.count()) := ISR$OSELECTION$RECORD(sCode, sName, sTitle, null, oSelectionList.COUNT());
      end loop;
      close cStudyList;
      
     
      WHEN UPPER (sEntity) = 'GROUPING' THEN
        FOR rGetGrouping IN cGetGrouping LOOP
          oSelectionList.EXTEND;
          oSelectionList (oSelectionList.COUNT()) := ISR$OSELECTION$RECORD(rGetGrouping.description, rGetGrouping.groupingname, rGetGrouping.groupingflag, null, oSelectionList.COUNT());
        END LOOP;
      
      WHEN upper (sEntity) = 'TYPE' THEN
        for rGetAuditList in (SELECT ISR$XML.clobToXml (audittext) xXml
            FROM stb$audittrail WHERE audittype = 'SAMRECAUDIT' and reference != '-1') loop           
          FOR rGetXmlFilter IN cGetXmlFilter(rGetAuditList.xXml, sEntity) LOOP
            oSelectionList.EXTEND;
            oSelectionList(oSelectionList.COUNT()) := ISR$OSELECTION$RECORD(SUBSTR(rGetXmlFilter.value, 0, 500), SUBSTR(rGetXmlFilter.value, 0, 500), SUBSTR(Utd$msglib.GetMsg(rGetXmlFilter.value, Stb$security.GetCurrentLanguage), 0, 2000), null, oSelectionList.COUNT());
          END LOOP;
        end loop;
      --***************************************
      ELSE
        EXECUTE IMMEDIATE
        'DECLARE
           olocError STB$OERROR$RECORD;
         BEGIN
           olocError :='|| stb$util.getCurrentCustompackage || '.getLov(:1,:2,:3);
           :4 :=  olocError;
         END;'
        USING IN sentity, IN ssuchwert, OUT oselectionlist, OUT oErrorObj;
    END CASE;

    IF sSuchWert IS NOT NULL AND oselectionlist.First IS NOT NULL THEN
      isr$trace.debug ('sSuchWert',  sSuchWert, sCurrentName);
      FOR i IN 1..oSelectionList.Count() LOOP
        IF oSelectionList(i).sValue = sSuchWert THEN
          isr$trace.debug ('oSelectionList('||i||').sSelected', oSelectionList(i).sValue, sCurrentName);
          oSelectionList(i).sSelected := 'T';
        END IF;
      END LOOP;
    END IF;

    isr$trace.debug ('status', 'oSelectionList.COUNT(): '||oSelectionList.COUNT(), sCurrentName);
    isr$trace.stat('end', 'end', sCurrentName);
    RETURN oErrorObj;
  EXCEPTION
    WHEN OTHERS THEN
      oErrorObj.handleError ( Stb$typedef.cnSeverityMessage, utd$msglib.getmsg ('exUnexpectedError', stb$security.getCurrentlanguage,  csP1 => sqlerrm(sqlcode)),
        sCurrentName, dbms_utility.format_error_stack ||chr(10)|| dbms_utility.format_error_backtrace );
      RETURN oErrorObj;
  END getLov;

  --**********************************************************************************************************************
  FUNCTION putParameters (oParameter IN STB$MENUENTRY$RECORD, olParameter IN STB$PROPERTY$LIST)
    RETURN STB$OERROR$RECORD IS
    oErrorObj                     STB$OERROR$RECORD := STB$OERROR$RECORD();
    sCurrentName  constant varchar2(100) := $$PLSQL_UNIT||'.putParameters ('||oParameter.sToken||')';   
    sStudyCode  tmp$bio$study.Code%type;
    nOutputId                     ISR$REPORT$OUTPUT$TYPE.OUTPUTID%type;
    nActionId                     ISR$ACTION.ACTIONID%type;
    sFunction                     ISR$REPORT$OUTPUT$TYPE.ACTIONFUNCTION%type;
    sMsg              varchar2(4000);
    
    cursor cGetJobIds( cstoken in isr$action.token%type) is
     select ao.outputid, ao.actionid, o.actionfunction myfunction
     from   isr$action A, isr$report$output$type o, isr$action$output ao
     where  (UPPER (o.token) = UPPER (cstoken) or UPPER (A.token) = UPPER (cstoken))
     and o.reporttypeid is null
     and o.outputid = ao.outputid
     and A.actionid = ao.actionid
     and ao.doc_definition = 'NOT_DETERMINED';

  BEGIN
    isr$trace.stat('begin', 'begin', sCurrentName);
    isr$trace.debug('olParameter', ' -> logclob', sCurrentName, olParameter);

    STB$OBJECT.setCurrentToken(oparameter.stoken);
    case 
    when upper (oparameter.stoken) = 'CAN_APPROVE_SAMPLE_RECONCILIATIONS' then
    
      /*FOR i IN 1..olParameter.COUNT LOOP
        IF olParameter(i).sParameter = 'STUDYLIST' THEN
          sStudyCode := olParameter(i).sValue;
          exit;
        END IF;
      END LOOP;   */
      sStudyCode := stb$util.getProperty(olParameter, 'STUDYLIST');
      
      oErrorObj := stb$audit.openAudit ('SAMRECAUDIT', ISR$SAMPLE$RECONCIL$GUI.getStudyName(sStudyCode));
      ISR$SAMPLE$RECONCIL$GUI.writeAudit('FULL', sStudyCode, 'APPROVED (reason ''' || stb$util.getProperty(olParameter, 'CAN_APPROVE_SAMPLE_RECONCILIATIONS') || ''')', 'approve');
      
      update ISR$COVIB$DESIGNSAMPLE$STATUS set status = 'APPROVED' where studycode = sStudyCode;      
      oErrorObj := stb$audit.silentAudit;    
      commit;
      
    when upper (oParameter.sToken) = 'CAN_DISPLAY_SAMRECAUDIT' then  

      open cGetJobIds ( oParameter.sToken);
      fetch cGetJobIds
      into  nOutputId, nActionId, sFunction;
      close cGetJobIds;

      if nActionId is null then
        sMsg := utd$msglib.getmsg ('exNotFindAction', stb$security.getCurrentLanguage, oParameter.sToken);
        raise stb$job.exNotFindAction;
      end if;
     
      select STB$JOB$SEQ.NEXTVAL
      into   STB$JOB.nJobid
      from   DUAL;

      oErrorObj := Stb$util.createFilterParameter (olParameter);

      STB$JOB.setJobParameter ('TOKEN', upper (oParameter.sToken), STB$JOB.nJobid);
      STB$JOB.setJobParameter ('OUTPUTID', nOutputId, STB$JOB.nJobid);
      STB$JOB.setJobParameter ('ACTIONID', nActionId, STB$JOB.nJobid);
      STB$JOB.setJobParameter ('REFERENCE', STB$UTIL.getProperty(olParameter, 'TYPE'), STB$JOB.nJobid);
      STB$JOB.setJobParameter ('FUNCTION', sFunction, STB$JOB.nJobid);
      
      --STB$JOB.setJobParameter ('FILTER_TYPE', '', STB$JOB.nJobid);
      oErrorObj := STB$JOB.startJob;
      
    else
      null;
    end case;

    isr$trace.stat('end', 'end', sCurrentName);
    RETURN oErrorObj;
  EXCEPTION
    when stb$job.exNotFindAction then
      oErrorObj.handleError ( Stb$typedef.cnSeverityCritical, sMsg, sCurrentName, 
          dbms_utility.format_error_stack ||chr(10)|| dbms_utility.format_error_backtrace );
      RETURN oErrorObj;
    WHEN OTHERS THEN
      ROLLBACK;
      IF SQLCODE = '-1' THEN
        oErrorObj.handleError ( Stb$typedef.cnSeverityMessage, utd$msglib.getmsg ('exNodeExistsText', stb$security.getCurrentlanguage,  csP1 => sqlerrm(sqlcode)),
        sCurrentName, dbms_utility.format_error_stack ||chr(10)|| dbms_utility.format_error_backtrace );
        RETURN oErrorObj;
      ELSE
        oErrorObj.handleError ( Stb$typedef.cnSeverityWarning, utd$msglib.getmsg ('exUnexpectedError', stb$security.getCurrentlanguage,  csP1 => sqlerrm(sqlcode)),
        sCurrentName, dbms_utility.format_error_stack ||chr(10)|| dbms_utility.format_error_backtrace );
        RETURN oErrorObj;
      END IF;
  END putParameters;

--**********************************************************************************************************************
FUNCTION checkDependenciesOnMask( oParameter IN STB$MENUENTRY$RECORD,
                                  olParameter IN OUT STB$PROPERTY$LIST,
                                  sModifiedFlag OUT VARCHAR2 )   RETURN STB$OERROR$RECORD
is
  sCurrentName   constant varchar2(4000) := $$PLSQL_UNIT||'.checkDependenciesOnMask('||' '||')';
  oErrorObj           STB$OERROR$RECORD := STB$OERROR$RECORD();
  olParameterOld      STB$PROPERTY$LIST := STB$PROPERTY$LIST();


BEGIN
  isr$trace.stat('begin', 'oParameter.sToken: '||oParameter.sToken, sCurrentName);

  olParameterOld := olParameter;


  -- compare the objects if something changed only then refresh the mask
  SELECT CASE WHEN COUNT ( * ) = 0 THEN 'F' ELSE 'T' END
    INTO sModifiedFlag
    FROM (SELECT XMLTYPE (VALUE (p)).getStringVal ()
            FROM table (olParameter) p
          MINUS
          SELECT XMLTYPE (VALUE (p)).getStringVal ()
            FROM table (olParameterOld) p);

  isr$trace.stat('end', 'sModifiedFlag: '||sModifiedFlag, sCurrentName);
  RETURN(oErrorObj);

EXCEPTION
  WHEN OTHERS then
    oErrorObj.handleError ( Stb$typedef.cnSeverityWarning, utd$msglib.getmsg ('exUnexpectedError', stb$security.getCurrentlanguage,  csP1 => sqlerrm(sqlcode)),
        sCurrentName, dbms_utility.format_error_stack ||chr(10)|| dbms_utility.format_error_backtrace );
    RETURN(oErrorObj);
END checkDependenciesOnMask;


END ISR$CONTEXTMENU$SAMPLE$RECONCIL;