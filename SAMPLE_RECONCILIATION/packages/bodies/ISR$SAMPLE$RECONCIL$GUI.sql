CREATE OR REPLACE PACKAGE BODY ISR$SAMPLE$RECONCIL$GUI
as

nLdapUserNo   stb$user.userno%type;
sStudyName    varchar2(4000);
nAuditModified number := 0;
cCodeForAudit tmp$bio$designsample.code%type;

--******************************************************************************
procedure initUser is
oErrorObj                     STB$OERROR$RECORD := STB$OERROR$RECORD();
    sCurrentName  constant varchar2(100) := $$PLSQL_UNIT||'.initUser';
    
  
  BEGIN
    isr$trace.stat ('begin',  ' ' ||nLdapUserNo, sCurrentName);    
        
    -- initalize of the user
    Stb$security.initJobUser(nLdapUserNo);
    isr$trace.stat ('end',  'end', sCurrentName);
 end initUser;
    
--******************************************************************************
function getStudyName(sStudyCode in VARCHAR2) return varchar2 as
  sCurrentName    constant varchar2(100) := $$PLSQL_UNIT||'.getStudyName (' ||sStudyCode || ')';
  oErrorObj                STB$OERROR$RECORD := STB$OERROR$RECORD();
  sMsg                     VARCHAR2(4000);
  
  cursor cGetStudyName is
    select name from tmp$bio$study
    where code = sStudyCode;
begin
  isr$trace.stat('begin', sStudyName, sCurrentName);
  
  if sStudyName is null then 
    open cGetStudyName;
    fetch cGetStudyName into sStudyName; 
    close cGetStudyName;
  end if;
    
  isr$trace.stat('end', sStudyName, sCurrentName);
  return sStudyName;
  
exception
  when others then
    sMsg :=  utd$msglib.getmsg ('exUnexpectedError', stb$security.getcurrentlanguage, csP1 => sqlerrm(sqlcode));
    oErrorObj. handleError (Stb$typedef.cnSeverityCritical, sMsg, sCurrentName, 'SQLCODE = '||SQLCODE||', SQLERRM = '||SQLERRM||' '||dbms_utility.format_error_backtrace);  
    /*open outList for SELECT xt.usermsg, xt.implmsg, xt.devmsg  FROM 
       XMLTABLE('//TLOMESSAGELIST'
         PASSING xmltype(oErrorObj) 
         COLUMNS 
           usermsg     VARCHAR2(4)  PATH 'susermsg',
           implmsg     VARCHAR2(10) PATH 'simplmsg',
           devmsg       VARCHAR2(9)  PATH 'sdevmsg'
         ) xt;*/
    return '';
end getStudyName;

--******************************************************************************
procedure getStudyList(csLdapUser in varchar2, sStudyStatus in VARCHAR2, outList out outCursor) as
  sCurrentName    constant varchar2(100) := $$PLSQL_UNIT||'.getStudyList (' ||csLdapUser || ', ' || sStudyStatus ||')';
  oErrorObj                STB$OERROR$RECORD := STB$OERROR$RECORD();
  sMsg                     VARCHAR2(4000);
  oParamList               ISR$PARAMLIST$REC:=ISR$PARAMLIST$REC();
  sLimsUser                VARCHAR2(100);
  lConditionsList          ISR$VARCHAR$VARRAY := ISR$VARCHAR$VARRAY();
  exNoLimsUser   exception;
  --sTrimStudyStatus         isr$covib$designsample$status.status%type := trim(sStudyStatus);
  
  cursor cLimsUser is
    select limsusername, userno 
      from stb$User
     where nvl(ldapusername, oracleusername) = csLdapUser;
       
begin
  isr$trace.stat('begin', 'begin', sCurrentName);
    
  open cLimsUser;
  fetch cLimsUser into sLimsUser, nLdapUserNo;
  close cLimsUser;
  if sLimsUser is null then
    raise exNoLimsUser;
  end if;
  
  initUser;
  
  delete from tmp$bio$usersec;
  delete from tmp$bio$study;
  
  ISR$Entity$Conditions.AddEntityCondition('BIO$USERSEC','LOGINNAME', sLimsUser);
  oErrorObj := ISR$REMOTE$COLLECTOR.fillTmpTableForEntity ('BIO$USERSEC', oParamList, 'F');
  select studycode 
    bulk collect into lConditionsList
    from TMP$BIO$USERSEC;
  ISR$Entity$Conditions.AddEntityCondition('BIO$STUDY','CODE', lConditionsList);
  oErrorObj := ISR$REMOTE$COLLECTOR.fillTmpTableForEntity ('BIO$STUDY', oParamList, 'F');
  
  if oErrorObj.isExceptionCritical then
    raise stb$typedef.exCritical;
  end if; 
  if sStudyStatus is not null then
    open outList for 'select distinct code, name, title from tmp$bio$study s, isr$covib$designsample$status sta 
                      where s.code = sta.studycode and upper(sta.status) = ''' || upper(sStudyStatus) ||'''';
  else
    open outList for 'select code, name, title from tmp$bio$study';
  end if;
  
  -- clear all conditions for entities 
  ISR$Entity$Conditions.RemoveEntityConditions;
  
  isr$trace.stat('end', 'end', sCurrentName);
exception
  when exNoLimsUser then
    sMsg :=  utd$msglib.getmsg ('exNoLimsUserMsg', stb$security.getCurrentLanguage, csLdapUser);
    isr$trace.debug('sMsg', sMsg, sCurrentName);
    oErrorObj. handleError (Stb$typedef.cnSeverityCritical, sMsg, sCurrentName, 'SQLCODE = '||SQLCODE||', SQLERRM = '||SQLERRM||' '||dbms_utility.format_error_backtrace);  
    open outList for 'select ''' || sMsg || ' ' || SQLERRM ||' '||dbms_utility.format_error_backtrace|| ''' as err from dual';
  when others then
    sMsg :=  utd$msglib.getmsg ('exUnexpectedError', stb$security.getCurrentLanguage, csP1 => sqlerrm(sqlcode));
    
    oErrorObj. handleError (Stb$typedef.cnSeverityCritical, sMsg, sCurrentName, 'SQLCODE = '||SQLCODE||', SQLERRM = '||SQLERRM||' '||dbms_utility.format_error_backtrace);  
    /*open outList for SELECT xt.usermsg, xt.implmsg, xt.devmsg  FROM 
       XMLTABLE('//TLOMESSAGELIST'
         PASSING xmltype(oErrorObj) 
         COLUMNS 
           usermsg     VARCHAR2(4)  PATH 'susermsg',
           implmsg     VARCHAR2(10) PATH 'simplmsg',
           devmsg       VARCHAR2(9)  PATH 'sdevmsg'
         ) xt;*/
    open outList for 'select ''' || sMsg || ' ' || SQLERRM ||' '||dbms_utility.format_error_backtrace|| ''' as err from dual';
end getStudyList;

--******************************************************************************
procedure getFullDesignSampleList(sStudyCode in varchar2, outList out outCursor) as
  sCurrentName    constant varchar2(100) := $$PLSQL_UNIT||'.getFullDesignSampleList';
  oErrorObj                STB$OERROR$RECORD := STB$OERROR$RECORD();
  sMsg                     VARCHAR2(4000);
  oParamList               ISR$PARAMLIST$REC:=ISR$PARAMLIST$REC();
begin
  isr$trace.stat('begin', 'begin', sCurrentName);
  ISR$Entity$Conditions.AddEntityCondition('BIO$DESIGNSAMPLE','STUDYCODE', sStudyCode);
  oErrorObj := ISR$REMOTE$COLLECTOR.fillTmpTableForEntity ('BIO$DESIGNSAMPLE', oParamList, 'F');
  if oErrorObj.isExceptionCritical then
      raise stb$typedef.exCritical;
  end if; 
  open outList for 'select * from TMP$BIO$DESIGNSAMPLE';  
  -- clear all conditions for entities 
  ISR$Entity$Conditions.RemoveEntityConditions('BIO$DESIGNSAMPLE');
  isr$trace.stat('end', 'end', sCurrentName);
exception
  when others then
    sMsg :=  utd$msglib.getmsg ('exUnexpectedError', stb$security.getcurrentlanguage, csP1 => sqlerrm(sqlcode));
    oErrorObj. handleError (Stb$typedef.cnSeverityCritical, sMsg, sCurrentName, 'SQLCODE = '||SQLCODE||', SQLERRM = '||SQLERRM||' '||dbms_utility.format_error_backtrace);  
   
    open outList for 'select ''' || sMsg || ' '|| SQLERRM ||' '||dbms_utility.format_error_backtrace|| ''' as err from dual'; 
end getFullDesignSampleList;

--******************************************************************************
procedure getSampleStatusList(outList out outCursor)as
  sCurrentName    constant varchar2(100) := $$PLSQL_UNIT||'.getSampleStatusList';
  oErrorObj                STB$OERROR$RECORD := STB$OERROR$RECORD();
  sMsg                     VARCHAR2(4000);
  oParamList               ISR$PARAMLIST$REC:=ISR$PARAMLIST$REC();
begin
  isr$trace.stat('begin', 'begin', sCurrentName);
  oErrorObj := ISR$REMOTE$COLLECTOR.fillTmpTableForEntity ('BIO$SAMPLESTATUS', oParamList, 'F');
  if oErrorObj.isExceptionCritical then
      raise stb$typedef.exCritical;
  end if; 
  open outList for 'select to_number(code) as SampleStatus, name as SampleStatusName from TMP$BIO$SAMPLESTATUS';
  isr$trace.stat('end', 'end', sCurrentName);
exception
  when others then
    sMsg :=  utd$msglib.getmsg ('exUnexpectedError', stb$security.getcurrentlanguage, csP1 => sqlerrm(sqlcode));
    oErrorObj. handleError (Stb$typedef.cnSeverityCritical, sMsg, sCurrentName, 'SQLCODE = '||SQLCODE||', SQLERRM = '||SQLERRM||' '||dbms_utility.format_error_backtrace);  
   
    open outList for 'select ''' || sMsg || ' '|| SQLERRM ||' '||dbms_utility.format_error_backtrace|| ''' as err from dual'; 
end getSampleStatusList;


--******************************************************************************
procedure getSampleTypesList(outList out outCursor)as
  sCurrentName    constant varchar2(100) := $$PLSQL_UNIT||'.getSampleTypesList';
  oErrorObj                STB$OERROR$RECORD := STB$OERROR$RECORD();
  sMsg                     VARCHAR2(4000);
  oParamList               ISR$PARAMLIST$REC:=ISR$PARAMLIST$REC();
begin
  isr$trace.stat('begin', 'begin', sCurrentName);
  oErrorObj := ISR$REMOTE$COLLECTOR.fillTmpTableForEntity ('BIO$SAMPLETYPES', oParamList, 'F');
  if oErrorObj.isExceptionCritical then
      raise stb$typedef.exCritical;
  end if; 
  open outList for 'select to_number(code) as code, name, info from TMP$BIO$SAMPLETYPES';
  isr$trace.stat('end', 'end', sCurrentName);
exception
  when others then
    sMsg :=  utd$msglib.getmsg ('exUnexpectedError', stb$security.getcurrentlanguage, csP1 => sqlerrm(sqlcode));
    oErrorObj. handleError (Stb$typedef.cnSeverityCritical, sMsg, sCurrentName, 'SQLCODE = '||SQLCODE||', SQLERRM = '||SQLERRM||' '||dbms_utility.format_error_backtrace);  
   
    open outList for 'select ''' || sMsg || ' '|| SQLERRM ||' '||dbms_utility.format_error_backtrace|| ''' as err from dual'; 
end getSampleTypesList;

--******************************************************************************
procedure getDesignSampleChangesList(sStudyCode in varchar2, outList out outCursor) as
  sCurrentName    constant varchar2(100) := $$PLSQL_UNIT||'.getDesignSampleChangesList';
  oErrorObj                STB$OERROR$RECORD := STB$OERROR$RECORD();
  sMsg                     VARCHAR2(4000);
begin
  isr$trace.stat('begin', 'begin', sCurrentName);  
  open outList for 'select * from ISR$COVIB$DESIGNSAMPLE$CHANGES where studyCode = ' || sStudyCode;
  isr$trace.stat('end', 'end', sCurrentName);
exception
  when others then
    sMsg :=  utd$msglib.getmsg ('exUnexpectedError', stb$security.getcurrentlanguage, csP1 => sqlerrm(sqlcode));
    oErrorObj. handleError (Stb$typedef.cnSeverityCritical, sMsg, sCurrentName, 'SQLCODE = '||SQLCODE||', SQLERRM = '||SQLERRM||' '||dbms_utility.format_error_backtrace);  
     
end getDesignSampleChangesList;


--******************************************************************************
function getSampleStatus(cSampleStatus  in number) return varchar2 as
  sCurrentName    constant varchar2(300) := $$PLSQL_UNIT||'.getSampleStatus';
  sampleStatusName   tmp$bio$samplestatus.name%type;
  
begin
  isr$trace.stat('begin', 'cSampleStatus= ' || cSampleStatus, sCurrentName);
  if cSampleStatus is not null then
    select name into sampleStatusName from TMP$BIO$SAMPLESTATUS where code = cSampleStatus;
  end if;
  isr$trace.stat('end', sampleStatusName, sCurrentName);
  return sampleStatusName;
end getSampleStatus;

--******************************************************************************
function getDesignSubjectTreatment(cnStudyCode in number, cnDesignsubjectTreatmentKey in number) return varchar2 as
 sCurrentName    constant varchar2(300) := $$PLSQL_UNIT||'.getDesignSubjectTreatment(' || cnStudyCode ||', ' || cnDesignsubjectTreatmentKey ||')';
 designSubject   varchar2(4000);
  
begin
  isr$trace.stat('begin', 'begin', sCurrentName);
  if cnStudyCode is not null and cnDesignsubjectTreatmentKey is not null then
      
    select 'Group ' || SUBJECTGROUPNAME || ', Treatment ID ' || TREATMENTID || ', Treatment Description ' || TREATMENTDESC ||
        ', Dose ' || DOSEAMOUNT || ', Dose Units ' || DOSEUNITSDESCRIPTION || ', Placebo-Treatment ' || PLACEBOTREATMENT ||
        ', Period ' || PERIOD || ', Visit ' || VISIT || ', Month ' || MONTH || ', Week ' || WEEK || ', Study Day ' || STUDYDAY
     into designSubject
     from tmp$bio$designsample 
     where studycode=cnStudyCode 
     and DesignsubjectTreatmentKey=cnDesignsubjectTreatmentKey 
     and rownum = 1;
  end if;
  isr$trace.stat('end', designSubject, sCurrentName);
  return designSubject;
end getDesignSubjectTreatment;

--******************************************************************************
function getDesignSubjectTag(cnStudyCode in number, cndesignsubjectid in number) return varchar2 as
 sCurrentName constant varchar2(300) := $$PLSQL_UNIT||'.getDesignSubjectTag(' || cnStudyCode ||', ' || cndesignsubjectid ||')';
 oErrorObj                STB$OERROR$RECORD := STB$OERROR$RECORD();
 oParamList             ISR$PARAMLIST$REC:=ISR$PARAMLIST$REC();
 designSubjectTag       varchar2(4000);
 nEntriesCount          number;

begin
  isr$trace.stat('begin', 'begin', sCurrentName);
  if cnStudyCode is not null and cndesignsubjectid is not null then
   /* select count(*) into nEntriesCount from TMP$BIO$DESIGNSAMPLE
    where studycode=cnStudyCode and designsubjectid=cndesignsubjectid ;
    isr$trace.debug('nEntriesCount', ' ' || nEntriesCount, sCurrentName);
    if nEntriesCount < 1 then
      ISR$Entity$Conditions.AddEntityCondition('BIO$DESIGNSAMPLE','STUDYCODE', cnStudyCode);
      oErrorObj := ISR$REMOTE$COLLECTOR.fillTmpTableForEntity ('BIO$DESIGNSAMPLE', oParamList, 'F');
      if oErrorObj.isExceptionCritical then
          raise stb$typedef.exCritical;
      end if; 
    end if;  */  
    select designsubjecttag--, STUDYSUBJECTTAG, GENDERID                          
     into designSubjectTag
     from tmp$bio$designsample 
     where studycode=cnStudyCode 
     and designsubjectid=cndesignsubjectid      
     and rownum = 1;
    isr$trace.stat('end', designSubjectTag, sCurrentName);
    return designSubjectTag;
  end if;
  isr$trace.stat('end', null, sCurrentName);
  return null;
end getDesignSubjectTag;

--******************************************************************************
function getSampleType(cSampleType  in number) return varchar2 as
  sCurrentName    constant varchar2(300) := $$PLSQL_UNIT||'.getSampleType';
  oErrorObj                STB$OERROR$RECORD := STB$OERROR$RECORD();
  oParamList               ISR$PARAMLIST$REC:=ISR$PARAMLIST$REC();
  sampleTypeName   tmp$bio$sampletypes.name%type;
  nSampleStatusCount   number;
  
begin
  isr$trace.stat('begin', 'cSampleType= ' || cSampleType, sCurrentName);
  if cSampleType is not null then
   /* select count(*) into nSampleStatusCount from TMP$BIO$SAMPLETYPES;
    isr$trace.debug('nSampleStatusCount', 'nSampleStatusCount= ' || cSampleType, sCurrentName);
    if nSampleStatusCount < 1 then
      oErrorObj := ISR$REMOTE$COLLECTOR.fillTmpTableForEntity ('BIO$SAMPLETYPES', oParamList, 'F');
      if oErrorObj.isExceptionCritical then
          raise stb$typedef.exCritical;
      end if; 
    end if;*/
    select name into sampleTypeName from TMP$BIO$SAMPLETYPES where code = cSampleType;
    isr$trace.stat('end', sampleTypeName, sCurrentName);
    return sampleTypeName;
  end if;
  isr$trace.stat('end', sampleTypeName, sCurrentName);
  return null;
end getSampleType;
  --******************************************************************************
  procedure addAudit(cOldCode in VARCHAR2, cParameter in VARCHAR2, cOldText in VARCHAR2, cText in VARCHAR2, 
                    cOldTranslated in varchar default null, cNewTranslated in varchar default null) is
    sCurrentName    constant varchar2(300) := $$PLSQL_UNIT||'.addAudit';
    oErrorObj                STB$OERROR$RECORD := STB$OERROR$RECORD();
    sModificationType VARCHAR2(50) := 'updated';
    sCode             VARCHAR2(500);
    sOldText          VARCHAR2(4000);
    sStmt             VARCHAR2(4000);
    sWriteAudit       VARCHAR2(1) := 'T';
    sOldTranslated    varchar2(4000);
    nStudyCode        number;
    nDesignSubjecttreatmentKey  number;
    sUserSampleId  tmp$bio$designsample.userSampleId%type;
  begin
    isr$trace.debug('cOldCode '||cOldCode||' cParameter '||cParameter, 'cOldText '||cOldText||' cText '||cText, sCurrentName);
     
    if (trim(cOldText) is not null or trim(cText) is not null) and (cOldText != cText or trim(cOldText) is null or trim(cText) is null)  then
      sWriteAudit := 'T'; 
      sCode := case when cCodeForAudit = 'FULL' then cOldCode else cCodeForAudit end;
      sOldText := cOldText;
      sModificationType := case when trim(cOldText) is null then 'inserted' when trim(cText) is null then 'deleted' else 'updated' end;
      if sModificationType = 'inserted' then
       -- begin
          sStmt := 'select '||cParameter||', studycode, designsubjecttreatmentkey from tmp$bio$designsample where code = '''||sCode||''' and rownum = 1';
          isr$trace.debug('cOldText', cOldText || ': ' || sStmt, sCurrentName);
          execute immediate sStmt into sOldText, nStudyCode, nDesignSubjecttreatmentKey;
          if trim(sOldText) is not null and sOldText = cText then
            sWriteAudit := 'F';
          elsif sOldText is not null then
            case
              when lower(cParameter) = 'treatmentkey' then
                sOldTranslated := getDesignSubjectTreatment(nStudyCode, nDesignSubjecttreatmentKey);
              when lower(cParameter) = 'designsubjectid' then
                sOldTranslated := getDesignSubjectTag(nStudyCode, sOldText);
              when lower(cParameter) = 'samplestatus' then
                sOldTranslated := getSampleStatus(sOldText);
              when lower(cParameter) = 'sampletypekey' then
                sOldTranslated := getSampleType(sOldText);
              else
                null;
            end case;
          end if;
          isr$trace.debug('sOldText', sOldText, sCurrentName);
       /* exception when others then
          isr$trace.debug('error', SQLERRM, sCurrentName);
          sOldText := cOldText;
        end;*/
      end if;
      if sWriteAudit = 'T' then
        nAuditModified := nAuditModified + 1;
        oErrorObj := stb$audit.addAuditEntry (cParameter, sOldText, cText, nAuditModified);        
        oErrorObj := stb$audit.AddAuditAdditionalElement ('NAME_TRANSLATED', utd$msglib.getmsg (cParameter, stb$security.getcurrentlanguage));
        if cOldTranslated is not null then
          sOldTranslated := cOldTranslated;
        end if;
          oErrorObj := stb$audit.AddAuditAdditionalElement ('OLDVALUE_TRANSLATED', sOldTranslated);
        if cNewTranslated is not null then
          oErrorObj := stb$audit.AddAuditAdditionalElement ('NEWVALUE_TRANSLATED', cNewTranslated);
        end if;
        sStmt := 'select userSampleId from tmp$bio$designsample where code = '''||sCode||''' and rownum = 1';
        execute immediate sStmt into sUserSampleId;
        isr$trace.debug('sUserSampleId', sUserSampleId || ' : ' || sStmt, sCurrentName);
        oErrorObj := stb$audit.AddAuditAdditionalElement ('ACTION', sModificationType);
        oErrorObj := stb$audit.AddAuditAdditionalElement ('SAMPLEREF', sCode);
        oErrorObj := stb$audit.AddAuditAdditionalElement ('CUSTOMID', sUserSampleId);
      end if;
    end if;  
  end addAudit; 


--******************************************************************************
procedure writeAudit(ccode in varchar2, cstudycode in integer, csStatus in varchar2, cAction in varchar2,
           cdesignsubjectid in integer default null, 
           cdesignsubjecttreatmentkey  integer default null, 
           ctreatmentkey in integer default null, 
           cusersampleid in varchar2 default null, 
           cendday in integer default null, cendhour in number default null, 
           cendminute in integer default null, cstartday in integer default null, 
           cstarthour in number default null, cstartminute in integer default null, 
           csamplestatus in integer default null, 
           csampletextvariable1 in varchar2 default null, 
           csampletextvariable2 in varchar2 default null, 
           csampletextvariable3 in varchar2 default null, 
           csplitnumber in integer default null, 
           ctimetext in  varchar2 default null,
           cDesignSampleId in number default null,
           cSampleTypeKey in number default null) is
           
  sCurrentName    constant varchar2(300) := $$PLSQL_UNIT||'.writeAudit (' || ccode || ', ' || cstudycode || ', ' || csStatus || ', ' || cAction || ')';         

  oErrorObj                STB$OERROR$RECORD := STB$OERROR$RECORD();
  
  cursor cGetOldValues is
    select *
      from ISR$COVIB$DESIGNSAMPLE$CHANGES
     where (code = cCode and cCode != 'FULL')
        or (studycode = cstudycode and cCode = 'FULL');

  rGetOldValues cGetOldValues%rowtype;

  cursor cGetOldStatus is
    select *
      from ISR$COVIB$DESIGNSAMPLE$STATUS
     where studycode = cstudycode;
  
  rGetOldStatus cGetOldStatus%rowtype;

begin

  isr$trace.stat('begin', 'begin', sCurrentName);
  cCodeForAudit := cCode;
  nAuditModified := 0;
  
  oErrorObj := stb$audit.addAuditRecord (getStudyName(cstudycode),  cAction );
    
  isr$trace.debug('cGetOldStatus', 'cstudycode '||cstudycode, sCurrentName);
  open cGetOldStatus;
  fetch cGetOldStatus into rGetOldStatus;
    isr$trace.debug('rGetOldStatus.status '||rGetOldStatus.status, 'csStatus '||csStatus, sCurrentName);
    if (rGetOldStatus.status != csStatus or trim(rGetOldStatus.status) is null or trim(csStatus) is null) then
      oErrorObj := stb$audit.addAuditEntry ('status', rGetOldStatus.status, csStatus, 1);
    end if;
  close cGetOldStatus;
    
  if csStatus = 'DRAFT' then
  
    isr$trace.debug('cGetOldValues', 'cCode '||cCode||' cstudycode '||cstudycode, sCurrentName);  
    open cGetOldValues;
    fetch cGetOldValues into rGetOldValues;
    if cGetOldValues%NOTFOUND then    -- insert
      isr$trace.debug('no old value found', 'ccode '||ccode, sCurrentName);
      addAudit(null, 'usersampleid', null, cusersampleid);
      addAudit(null, 'designSampleId', null, cDesignSampleId);
      addAudit(null, 'studycode', null, cstudycode);
      --addAudit(null, 'designsubjectid', null, cdesignsubjectid, null);
      addAudit(null, 'designsubjectid', null, cdesignsubjectid, null
             , getDesignSubjectTag(cstudycode, cdesignsubjectid));
      addAudit(null, 'designsubjecttreatmentkey', null, cdesignsubjecttreatmentkey);
      addAudit(null, 'treatmentkey', null, ctreatmentkey, null
             , getDesignSubjectTreatment(cstudycode, cdesignsubjecttreatmentkey));
      addAudit(null, 'endday', null, cendday);
      addAudit(null, 'endhour', null, cendhour);
      addAudit(null, 'endminute', null, cendminute);
      addAudit(null, 'startday', null, cstartday);
      addAudit(null, 'starthour', null, cstarthour);
      addAudit(null, 'startminute', null, cstartminute);
      addAudit(null, 'sampleStatus', null,csamplestatus, null, getSampleStatus(csamplestatus));
      addAudit(null, 'sampletextvariable1', null, csampletextvariable1);
      addAudit(null, 'sampletextvariable2', null, csampletextvariable2);
      addAudit(null, 'sampletextvariable3', null, csampletextvariable3);
      addAudit(null, 'splitnumber', null, csplitnumber);
      addAudit(null, 'timetext', null, ctimetext);      
      addAudit(null, 'sampletypekey', null, cSampleTypeKey, null, getSampleType(csampleTypeKey));      
    else               -- update, delete
      loop
        EXIT WHEN cGetOldValues%NOTFOUND; -- avoid duplicate audit for last row
        isr$trace.debug('rGetOldValues.code '||rGetOldValues.code, 'ccode '||ccode, sCurrentName);
        addAudit(rGetOldValues.code, 'usersampleid', rGetOldValues.userSampleId, cusersampleid);
        addAudit(rGetOldValues.code, 'designSampleId', rGetOldValues.designSampleId, cDesignSampleId);
        addAudit(rGetOldValues.code, 'studycode', rGetOldValues.studycode, cstudycode);
        --addAudit(rGetOldValues.code, 'designsubjectid', rGetOldValues.designsubjectid, cdesignsubjectid);
        addAudit(rGetOldValues.code, 'designsubjectid', rGetOldValues.designsubjectid, cdesignsubjectid
           , getDesignSubjectTag(cstudycode, rGetOldValues.designsubjectid)
           , getDesignSubjectTag(cstudycode, cdesignsubjectid)); --noch holen
        addAudit(rGetOldValues.code, 'designsubjecttreatmentkey', rGetOldValues.designsubjecttreatmentkey, cdesignsubjecttreatmentkey);
        addAudit(rGetOldValues.code, 'treatmentkey', rGetOldValues.treatmentkey, ctreatmentkey
           , getDesignSubjectTreatment(cstudycode, rGetOldValues.designsubjecttreatmentkey)
           , getDesignSubjectTreatment(cstudycode, cdesignsubjecttreatmentkey));
        addAudit(rGetOldValues.code, 'endday', rGetOldValues.endday, cendday);
        addAudit(rGetOldValues.code, 'endhour', rGetOldValues.endhour, cendhour);
        addAudit(rGetOldValues.code, 'endminute', rGetOldValues.endminute, cendminute);
        addAudit(rGetOldValues.code, 'startday', rGetOldValues.startday, cstartday);
        addAudit(rGetOldValues.code, 'starthour', rGetOldValues.starthour, cstarthour);
        addAudit(rGetOldValues.code, 'startminute', rGetOldValues.startminute, cstartminute);
        addAudit(rGetOldValues.code, 'sampleStatus', rGetOldValues.samplestatus,csamplestatus, getSampleStatus(rGetOldValues.samplestatus), getSampleStatus(csamplestatus));
        addAudit(rGetOldValues.code, 'sampletextvariable1', rGetOldValues.sampletextvariable1, csampletextvariable1);
        addAudit(rGetOldValues.code, 'sampletextvariable2', rGetOldValues.sampletextvariable2, csampletextvariable2);
        addAudit(rGetOldValues.code, 'sampletextvariable3', rGetOldValues.sampletextvariable3, csampletextvariable3);
        addAudit(rGetOldValues.code, 'splitnumber', rGetOldValues.splitnumber, csplitnumber);
        addAudit(rGetOldValues.code, 'timetext', rGetOldValues.timetext, ctimetext);      
        addAudit(rGetOldValues.code, 'sampletypekey', rGetOldValues.sampleTypeKey, cSampleTypeKey, getSampleType(rGetOldValues.sampleTypeKey), getSampleType(csampleTypeKey));      
        
        fetch cGetOldValues into rGetOldValues;
      end loop;   
    end if;
    close cGetOldValues;
  
  end if;
  
  isr$trace.stat('end', 'end', sCurrentName);

end writeAudit;
 
--******************************************************************************
procedure saveDesignSampleChanges(ccode in varchar2, cstudycode in integer, 
           cdesignsubjectid in integer default null, cdesignsubjecttreatmentkey  integer default null, 
           ctreatmentkey in integer default null, cusersampleid in varchar2 default null, cendday in integer default null, 
           cendhour in number default null, cendminute in integer default null, 
           cstartday in integer default null, cstarthour in number default null, 
           cstartminute in integer default null, csamplestatus in integer default null, 
           csampletextvariable1 in varchar2 default null, csampletextvariable2 in varchar2 default null, 
           csampletextvariable3 in varchar2 default null, 
           csplitnumber in integer default null, ctimetext in  varchar2 default null,
           cDesignSampleId in number, cSampleTypeKey in number
) as
  sCurrentName    constant varchar2(300) := $$PLSQL_UNIT||'.saveDesignSampleChanges (' || ccode || ', ' || cdesignsubjectid || ')';
  oErrorObj                STB$OERROR$RECORD := STB$OERROR$RECORD();
  exAuditException         exception;
 
begin
  isr$trace.stat('begin', 'begin', sCurrentName);
  initUser;

  oErrorObj := stb$audit.openAudit ('SAMRECAUDIT', getStudyName(cstudycode));
  
  writeAudit( ccode, cstudycode, 'DRAFT', 'save changes', cdesignsubjectid, cdesignsubjecttreatmentkey ,
              ctreatmentkey, cusersampleid, cendday, cendhour, cendminute, 
              cstartday, cstarthour, cstartminute, csamplestatus, 
              csampletextvariable1, csampletextvariable2,
              csampletextvariable3, csplitnumber, ctimetext, cDesignSampleId, csampleTypeKey);
              
  isr$trace.debug('merge into ISR$COVIB$DESIGNSAMPLE$CHANGES', 'cCode '||cCode||' cstudycode '||cstudycode, sCurrentName);            
      
  merge into ISR$COVIB$DESIGNSAMPLE$CHANGES dest
  using (select ccode as code, cDesignSampleId as designSampleId, cstudycode as studycode, cdesignsubjectid as designsubjectid, 
    cdesignsubjecttreatmentkey as designsubjecttreatmentkey, ctreatmentkey as treatmentkey, 
    cusersampleid as usersampleid, cendday as endday, cendhour as endhour, cendminute as endminute, 
    cstartday as startday, cstarthour as starthour, cstartminute as startminute, csamplestatus as samplestatus, 
    csampletextvariable1 as sampletextvariable1, csampletextvariable2 as sampletextvariable2, 
    csampletextvariable3 as sampletextvariable3, csplitnumber as splitnumber, ctimetext as timetext, csampleTypeKey as sampleTypeKey
    from dual) src
  on ( dest.code = src.code)
  when matched then update set 
    dest.designSampleId = src.designSampleId,
    dest.designsubjectid = src.designsubjectid,
    dest.designsubjecttreatmentkey = src.designsubjecttreatmentkey, 
    dest.treatmentkey = src.treatmentkey, 
    dest.usersampleid = src.usersampleid, 
    dest.endday = src.endday, 
    dest.endhour = src.endhour, 
    dest.endminute = src.endminute, 
    dest.startday = src.startday, 
    dest.starthour = src.starthour, 
    dest.startminute = src.startminute, 
    dest.samplestatus = src.samplestatus, 
    dest.sampletextvariable1 = src.sampletextvariable1, 
    dest.sampletextvariable2 = src.sampletextvariable2, 
    dest.sampletextvariable3 = src.sampletextvariable3, 
    dest.splitnumber = src.splitnumber, 
    dest.timetext = src.timetext, 
    dest.sampleTypeKey = src.sampleTypeKey
  when not matched then 
  insert (code, designSampleId, studycode, designsubjectid, designsubjecttreatmentkey, treatmentkey, 
    usersampleid, endday, endhour, endminute, startday, 
    starthour, startminute, samplestatus, sampletextvariable1, sampletextvariable2, 
    sampletextvariable3, splitnumber, timetext, sampleTypeKey)
  values (src.code, src.designSampleId, src.studycode, src.designsubjectid, src.designsubjecttreatmentkey, src.treatmentkey, 
    src.usersampleid, src.endday, src.endhour, src.endminute, src.startday, 
    src.starthour, src.startminute, src.samplestatus, src.sampletextvariable1, src.sampletextvariable2, 
    src.sampletextvariable3, src.splitnumber, src.timetext, src.sampleTypeKey);
    
  isr$trace.debug('merge into ISR$COVIB$DESIGNSAMPLE$STATUS', 'cCode '||cCode||' cstudycode '||cstudycode, sCurrentName);
    
  merge into ISR$COVIB$DESIGNSAMPLE$STATUS dest
  using (select cstudycode as studycode, 'DRAFT' as status from dual) src
  on ( dest.studycode = src.studycode)
  when matched then update set 
    dest.status = src.status
    when not matched then 
  insert (studycode, status)
  values (src.studycode, src.status);
  
  isr$trace.debug('close audit', 'cCode '||cCode||' cstudycode '||cstudycode, sCurrentName);
  
  oErrorObj := stb$audit.silentAudit;  
  if oErrorObj.isExceptionCritical then
    raise exAuditException;
  end if;
  
  commit;
  isr$trace.stat('end', 'end', sCurrentName);
/*exception
  when others then
    sMsg :=  utd$msglib.getmsg ('exUnexpectedError', stb$security.getcurrentlanguage, csP1 => sqlerrm(sqlcode));
    oErrorObj. handleError (Stb$typedef.cnSeverityCritical, sMsg, sCurrentName, 'SQLCODE = '||SQLCODE||', SQLERRM = '||SQLERRM||' '||dbms_utility.format_error_backtrace);  
  */  
end saveDesignSampleChanges;

--******************************************************************************
procedure resetDesignSampleChanges(csCode in varchar2) as
  sCurrentName    constant varchar2(300) := $$PLSQL_UNIT||'.resetDesignSampleChanges (' || csCode || ')';
  oErrorObj                STB$OERROR$RECORD := STB$OERROR$RECORD();
begin
  isr$trace.stat('begin', 'begin', sCurrentName);
  initUser;

  oErrorObj := stb$audit.openAudit ('SAMRECAUDIT', getStudyName(cscode));
 
  writeAudit('FULL', csCode, 'DRAFT', 'reset changes');

  update ISR$COVIB$DESIGNSAMPLE$STATUS set status='DRAFT' where studycode = csCode;
  delete from ISR$COVIB$DESIGNSAMPLE$CHANGES where studycode = csCode;

  oErrorObj := stb$audit.silentAudit;  

  commit;
  isr$trace.stat('end', 'end', sCurrentName);
end resetDesignSampleChanges;

--******************************************************************************
procedure publishDesignSampleChanges(csCode in varchar2, errorList out outCursor) as
  sCurrentName    constant varchar2(300) := $$PLSQL_UNIT||'.publishDesignSampleChanges (' || csCode || ')';
  oErrorObj       STB$OERROR$RECORD := STB$OERROR$RECORD();
  sMsg            VARCHAR2(4000);
  exNoServerAvailable  exception;
  clData   clob;
  xInputXml  xmltype;
  sRemoteEx  clob;
  xServerPart  xmltype;
  xDbParamXml  xmltype;
  nServerId   number;
  ntimeout  number;
begin
  isr$trace.stat('begin', 'begin', sCurrentName);
  initUser;
  clData := dbms_xmlgen.getxml('select * from ISR$COVIB$DESIGNSAMPLE$CHANGES where studycode = ' || csCode );
  isr$trace.debug('clData',  '->', sCurrentName, clData);
  
  select xmlagg( xmlelement (
                 "SERVER"
               , xmlelement ("SERVERID", s.serverid)
               , xmlelement ("SERVERNAME", '"' || s.alias || '"')
               , xmlelement ("SERVERTYPE", servertypeid)
               , xmlelement ("HOST", observer_host )
               , xmlelement ("PORT", port)
               , xmlelement ("WEBSERVICE", classname)
               , xmlelement ("CONTEXT", context)
               , xmlelement ("NAMESPACE", namespace)
               , xmlelement ("CONFIGNAME", 'REMOTE_DATA')
               , xmlelement("DATEPATTERN", STB$UTIL.getSystemParameter ('REMOTECOLLECTOR_DATEFORMAT'))
               , xmlelement ("TIMEOUT", timeout)
               , xmlagg ( xmlelement(EVALNAME(parametername),
                        parametervalue ))))
     into xServerPart
    from isr$serverobserver$v s, isr$serverparameter sp, isr$server$entity$mapping sem
   where  s.serverid = sem.serverid
     and sem.entityname = 'BIO$DESIGNSAMPLE'
     and s.serverid = sp.serverid
     and sp.parametervalue is not null
     and s.runstatus = 'T'
     group by s.serverid, s.alias, servertypeid, observer_host, port, classname, context, namespace, timeout;
     
  if xServerPart is null then
    raise exNoServerAvailable;
  end if;
  
  select s.serverid, timeout
  into  nServerId, nTimeout
   from isr$serverobserver$v s,isr$server$entity$mapping sem
   where  s.serverid = sem.serverid
   and sem.entityname = 'BIO$DESIGNSAMPLE';
  
  select xmlelement("DBPARAMETER", xmlattributes('STUDYCODE' as "columnname"), 
                                  xmlelement("STUDYCODE", csCode) )
  into xDbParamXml 
  from dual;   
  isr$trace.debug('xDbParamXml',  '->', sCurrentName, xDbParamXml); 
  
  select xmlroot(xmlelement("ISR_REMOTE_XML",
                 xmlattributes('ISR$COVIB$DESIGNSAMPLE$CHANGES' as "tablename", nTimeout as "timeout"),
                 xmlconcat(xServerPart, xDbParamXml)),
       version '1.0" encoding="'||STB$UTIL.getSystemParameter('XML_ENCODING'))
  into xInputXML
  from dual;
     
  isr$trace.debug('xInputXML',  '->', sCurrentName, xInputXML); 
  isr$trace.debug('logLevel',isr$trace.getJavaUserLoglevelStr, sCurrentName);
  
  sRemoteEx := insertRowDataXmlToRemoteTable ( xInputXML.getClobVal, clData, nServerId, isr$trace.getJavaUserLoglevelStr, isr$trace.getReference, nTimeout);  
  oErrorObj.checkRemoteException(sCurrentName, Stb$typedef.cnSeverityCritical, sRemoteEx);
  
  isr$trace.debug('oErrorObj', oErrorObj.sSeverityCode, sCurrentName, oErrorObj);
  if oErrorObj.isExceptionCritical then
    raise stb$typedef.exCritical;
  end if;
  
  oErrorObj := stb$audit.openAudit ('SAMRECAUDIT', getStudyName(csCode));
  
  writeAudit('FULL', csCode, 'PUBLISHED', 'change status');

  update ISR$COVIB$DESIGNSAMPLE$STATUS set status = 'PUBLISHED' where studycode = csCode;

  oErrorObj := stb$audit.silentAudit;  
  
  commit;
  isr$trace.stat('end', 'end', sCurrentName);
exception
  when others then
    sMsg :=  utd$msglib.getmsg ('exUnexpectedError', stb$security.getcurrentlanguage, csP1 => sqlerrm(sqlcode));
    oErrorObj. handleError (Stb$typedef.cnSeverityCritical, sMsg, sCurrentName, 'SQLCODE = '||SQLCODE||', SQLERRM = '||SQLERRM||' '||dbms_utility.format_error_backtrace);  
   
    open errorList for 'select ''' || SQLERRM ||' '||dbms_utility.format_error_backtrace|| ''' as err from dual';
end publishDesignSampleChanges;

--******************************************************************************
function insertRowDataXmlToRemoteTable ( csConfXml IN CLOB, csDataXml IN CLOB, csServerId  IN VARCHAR2, csLoglevel  IN VARCHAR2,  csLogReference  IN VARCHAR2, nTimeout in NUMBER ) RETURN CLOB
AS LANGUAGE JAVA
NAME 'com.uptodata.isr.db.wsclient.RemoteDBClient.insertRowDataXmlToRemoteTable( java.sql.Clob, java.sql.Clob, java.lang.String, java.lang.String, java.lang.String, int) return java.sql.Clob';


end ISR$SAMPLE$RECONCIL$GUI;