package com.uptodata.isr.remote.ws;


import com.uptodata.isr.observers.base.dbAccess.DbData;
import com.uptodata.isr.observers.base.secutity.CryptDbPropertiesHandler;
import com.uptodata.isr.server.utils.logging.log4j2.IsrServerLogger;
import com.uptodata.isr.server.utils.logging.log4j2.LogHelper;
import com.uptodata.isr.server.utils.network.UrlUtil;
import com.uptodata.isr.utils.ConvertUtils;

import java.util.Properties;

/**
 * Created by smiljanskii60 on 15.07.2014.
 */
public class RemoteCollectorServicePublisher {


    public static void main(String[] args) {
        IsrServerLogger log = null;
        System.out.println("Constants.USER_DIR = " + com.uptodata.isr.server.utils.fileSystem.Constants.USER_DIR);
        try {
            Properties props = ConvertUtils.convertArrayToProperties(args);

            String logLevel = props.getProperty("logLevel");
            String wsUrl = props.getProperty("wsUrl");
            DbData dbData = new DbData(props);

            CryptDbPropertiesHandler.saveDbPropertiesForLog(args);

            log = LogHelper.initLogger(logLevel, "remote collector");
            log.info("properties for start server==" + ConvertUtils.propertiesWithPasswordToString(props));

            JDBCCollectorService remoteCollector = new JDBCCollectorService(dbData, logLevel);
            remoteCollector.init();

            UrlUtil.createAndPublishEndpoint(wsUrl, remoteCollector);

            log.info("Service 'RemoteCollectorService' is started on ==" + wsUrl);
        } catch (Exception e) {
            if (log != null) {
                log.error("error==" + e);
            }
            e.printStackTrace();
        }
    }


}
