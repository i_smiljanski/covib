package com.uptodata.isr.remote.sax;

import org.apache.commons.lang.StringUtils;
import org.xml.sax.Attributes;
import org.xml.sax.SAXException;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * Created by smiljanskii60 on 16.01.2017.
 */
public class JDBCInputXmlHandler extends RemoteInputXmlHandler {

    public String dbDriver;
    public String dbUrl;

    public String orderBy;
    public List<Map<String, String>> dbParamsColumns = new ArrayList<>();
    public Map<String, List<String>> dbParams = new HashMap<>();

    private List<String> paramValues = new ArrayList<>();
    private Map<String, String> dbParamColumn;
    private String keyType;
    private String dbParamColumnName;

    @Override
    public void startElement(String uri, String localName, String qName, Attributes atts) throws SAXException {
        super.startElement(uri, localName, qName, atts);
        if (qName.equals("DBPARAMETER")) {
            dbParamColumnName = atts.getValue("columnname");
            keyType = atts.getValue("key");
            paramValues = new ArrayList<>();
            dbParamColumn = new HashMap<>();
        }
    }


    @Override
    public void endElement(String uri, String localName, String qName) throws SAXException {
        super.endElement(uri, localName, qName);
        if (qName.equals(dbParamColumnName)) {
            row = new HashMap<>();
            row.put("PARAMETER", dbParamColumnName);
            row.put("VALUE", currentValue);
            rows.add(row);
            if(StringUtils.isNotEmpty(currentValue)) {  //ISRC-1339
                paramValues.add(currentValue);
            }
            dbParamColumn.put("COLUMN", dbParamColumnName);
            dbParamColumn.put("KEY", keyType);
        } else if (qName.equals("DBPARAMETER") && !qName.equals(currentElement)) {  //exclude empty elements
            if(paramValues.size() > 0) {
                dbParamsColumns.add(dbParamColumn);
                dbParams.put(dbParamColumnName, paramValues);
            }
        } else if (qName.equals("ORDERBY")) {
            orderBy = currentValue;
        }
        if (isCurrentServer) {
            switch (qName) {
                case "DBDRIVER":
                    dbDriver = currentValue;
                    break;
                case "DBURL":
                    dbUrl = currentValue;
                    break;
            }
        }
    }

    @Override
    public String toString() {
        return "JDBCInputXmlHandler{" +
                "dbDriver='" + dbDriver + '\'' +
                ", dbUrl='" + dbUrl + '\'' +
                ", orderBy='" + orderBy + '\'' + "\n" +
                ", dbParamsColumns=" + dbParamsColumns  + "\n" +
                ", dbParams=" + dbParams  +
                '}' + ";\n" + super.toString();
    }
}