package com.uptodata.isr.remote.ws;

import com.uptodata.isr.remote.sax.JDBCInputXmlHandler;
import com.uptodata.isr.remote.sax.RowsetRowXmlParser;
import com.uptodata.isr.server.utils.fileSystem.Constants;
import com.uptodata.isr.server.utils.logging.log4j2.IsrServerLogger;
import com.uptodata.isr.server.utils.logging.log4j2.LogHelper;
import com.uptodata.isr.utils.ConvertUtils;
import com.uptodata.isr.utils.exceptions.MessageStackException;
import com.uptodata.isr.utils.security.SymmetricProtection;
import com.uptodata.isr.utils.xml.dom.XmlHandler;
import org.apache.commons.codec.binary.Base64;
import org.apache.commons.lang.StringUtils;
import org.w3c.dom.CDATASection;
import org.w3c.dom.Document;
import org.w3c.dom.Element;

import java.io.ByteArrayInputStream;
import java.sql.*;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

/**
 * Created by smiljanskii60 on 24.03.2015.
 */
public class RemoteDataAssistant {
    private IsrServerLogger log;
    private String serverName;
    private boolean isOracleDriver;
    static String KEY_TABLE = "TMP$ISR$KEYS";
    static boolean TABLE_TEMP = true;
    private static final String METHOD_BEGIN = JDBCCollectorService.METHOD_BEGIN;
    private static final String METHOD_END = JDBCCollectorService.METHOD_END;


    public RemoteDataAssistant(IsrServerLogger log, String serverName) {
        this.log = log;
        this.serverName = serverName;
    }

    Document getXmlFromRemoteData(Document document, String server, RowsetRowXmlParser parser)
            throws MessageStackException, SQLException {
        long start = System.currentTimeMillis();
        JDBCInputXmlHandler xmlHandler = (JDBCInputXmlHandler) parser.getHandler();
        Connection connection = getConnection(xmlHandler);
        connection.setAutoCommit(false);

        if (isOracleDriver)
            try {
                writeKeys(server, connection, parser);
            } catch (Exception e) {
                log.error("error during write keys == " + e);
            }

        String sql = getSql(xmlHandler);
        getXmlForSelect(document, server, sql, connection);
        connection.close();
        long millis = System.currentTimeMillis() - start;
        log.debug("table ==" + xmlHandler.tableName + ": required time in ms: " + millis);
        return document;
    }

    private Connection getConnection(JDBCInputXmlHandler xmlHandler) throws MessageStackException {
        String dbDriver = xmlHandler.dbDriver;
        String dbURL = xmlHandler.dbUrl;
        String user = xmlHandler.username;
        String password = xmlHandler.password;
        return getConnection(dbDriver, dbURL, user, password);
    }

    private Connection getConnection(String dbDriver, String dbURL, String user, String password) throws MessageStackException {
        isOracleDriver = dbDriver.contains("OracleDriver");
        try {
            DriverManager.registerDriver((Driver) Class.forName(dbDriver).newInstance());
        } catch (Exception e) {
            throw new MessageStackException(e, MessageStackException.SEVERITY_CRITICAL,
                    "driver class '" + dbDriver + "' could not be found. Server: " + serverName + "; ",
                    MessageStackException.getCurrentMethod());
        }
        Connection connection;
        try {
            log.debug("dbUrl == " + dbURL + "; user = " + user);
            connection = DriverManager.getConnection(dbURL, user, SymmetricProtection.decrypt(password));
        } catch (Exception e) {
            throw new MessageStackException(e, MessageStackException.SEVERITY_CRITICAL, "problem by getting of remote connection. Server: " + serverName + "; ",
                    MessageStackException.getCurrentMethod());
        }
        return connection;
    }


    private void getXmlForSelect(Document document, String server, String sql, Connection connection) throws SQLException {
        log.stat(METHOD_BEGIN);

        CallableStatement cs = connection.prepareCall(sql);
        cs.setEscapeProcessing(false);  //temporary solution because error "Non supported SQL92" [ARDIS-705]
        ResultSet rs = cs.executeQuery();

        ResultSetMetaData rsmd = rs.getMetaData();

        int counter = 0;
        XmlHandler xmlHandler = new XmlHandler();

        while (rs.next()) {
            counter++;
            Element rowElement = document.createElement("ROW");

            Element orderbyEl = document.createElement("ENTITYORDERBY");
            orderbyEl.setTextContent(Integer.toString(counter));
            rowElement.appendChild(orderbyEl);

            Element serverEl = document.createElement("SERVERID");
            serverEl.setTextContent(server);
            rowElement.appendChild(serverEl);

            document.getFirstChild().appendChild(rowElement);
            for (int i = 1; i < rsmd.getColumnCount() + 1; i++) {
                int columnType = rsmd.getColumnType(i);
                if (columnType == Types.CLOB) {
                    String value = rs.getString(i);
                    if (value != null && value.length() > 0) {
                        Element colElement;
                        try {
                            colElement = document.createElement(rsmd.getColumnName(i));
                        } catch (Exception e) {
                            colElement = document.createElement("FUNCTION");
                        }
                        CDATASection cdata = document.createCDATASection(value.trim());
                        colElement.appendChild(cdata);
                        rowElement.appendChild(colElement);
                    }
                } else if (columnType == Types.BLOB) {
                    byte[] data = null;
                    try {
                        data = ConvertUtils.getBytes(rs.getBlob(i));
                    } catch (Exception e) {
                        log.info("no data");
                    }
                    if (data != null && data.length > 0) {
                        Element colElement = document.createElement(rsmd.getColumnName(i));
                        CDATASection cdata = document.createCDATASection(Base64.encodeBase64String(data));
                        colElement.appendChild(cdata);
                        rowElement.appendChild(colElement);
                    }
                } else {
                    String value = rs.getString(i);
                    if (value != null && value.length() > 0) {
                        value = xmlHandler.stripNonValidXMLCharacters(value);
                        Element colElement = document.createElement(rsmd.getColumnName(i));
                        colElement.setTextContent(value);
                        rowElement.appendChild(colElement);
                    }
                }
            }
        }
        rs.close();
        cs.close();

        log.stat(METHOD_END + " ==" + " rows inserted: " + counter);

    }


    private void writeKeys(String server, Connection connection, RowsetRowXmlParser remoteXmlParser) throws Exception {
        log.stat(METHOD_BEGIN);
        checkKeyTable(server, connection);
        int insertedRows = remoteXmlParser.insertXml(KEY_TABLE, connection);
        log.stat(METHOD_END + "==keys inserted: " + insertedRows);
    }


    private void checkKeyTable(String server, Connection connection) throws SQLException {
        Statement stmt = connection.createStatement();
        String query = "select count(*) from user_tables where table_name='";
        ResultSet rs = stmt.executeQuery(query + KEY_TABLE + "'");
        rs.next();
        int resultCount = rs.getInt(1);
        rs.close();
        stmt.close();
        log.debug("resultCount==" + resultCount);
        if (resultCount == 0) {
            stmt = connection.createStatement();
            String sql = TABLE_TEMP ? "CREATE GLOBAL TEMPORARY TABLE " + KEY_TABLE +
                    "(SESSIONID  VARCHAR2(50)," +
                    "  PARAMETER  VARCHAR2(100)," +
                    "  VALUE      VARCHAR2(2000)" +
                    ") ON COMMIT PRESERVE ROWS " +
                    "NOCACHE"
                    :
                    "CREATE TABLE " + KEY_TABLE +
                            " ( SESSIONID  VARCHAR2(50)," +
                            "  PARAMETER  VARCHAR2(100)," +
                            "  VALUE      VARCHAR2(2000)" +
                            ")";
            log.info("sql == " + sql);
            stmt.executeUpdate(sql);
            stmt.close();
            log.info("table== " + KEY_TABLE + " created");
        }
    }


    private String getSql(JDBCInputXmlHandler handler) {
        log.stat(METHOD_BEGIN);
//        log.debug(handler);
        StringBuilder sql = new StringBuilder("SELECT ");

        StringBuilder columns = new StringBuilder();
        String alias = "a";
        List<String> tempTableColumns = new ArrayList<>();
        tempTableColumns.add("SESSIONID");
        tempTableColumns.add("PARAMETER");
        tempTableColumns.add("VALUE");
        String colAlias = "";

        List<Map<String, String>> columnList = handler.columns;
        for (Map<String, String> column : columnList) {
            String columnName = column.get("columnname");
            columns.append(", ");
            if (tempTableColumns.contains(columnName)) {
                colAlias = alias + ".";
            }
            columns.append(colAlias).append(columnName);
        }
        String table = handler.tableName;
        sql.append(columns.toString().replaceFirst(",", ""));

        List<Map<String, String>> dbORParamsColumns = new ArrayList<>();
        List<Map<String, String>> dbANDParamsColumns = new ArrayList<>();
        for (Map<String, String> dbParamMap : handler.dbParamsColumns) {
            if ("OR".equalsIgnoreCase(dbParamMap.get("KEY"))) {
                dbORParamsColumns.add(dbParamMap);
            } else {
                dbANDParamsColumns.add(dbParamMap);
            }
        }
        log.debug(dbORParamsColumns);
        log.debug(dbANDParamsColumns);

        StringBuilder from = new StringBuilder(" FROM ");
        from.append(table).append(" ").append(alias);

        StringBuilder conditions = getANDConditions(dbANDParamsColumns, handler.maxInParameters, handler.dbParams, alias, from);
        StringBuilder orConditions = getORConditions(dbORParamsColumns, handler.dbParams);

        if (conditions.length() > 0 && orConditions.length() > 0) {
            conditions.append(" AND (").append(orConditions).append(")");
        } else if (orConditions.length() > 0) {
            conditions.append(" (").append(orConditions).append(")");
        }

        sql.append(from);

        if (conditions.length() > 0) {
            sql.append(" WHERE ").append(conditions);
        }
        String orderBy = handler.orderBy;
        if (StringUtils.isNotBlank(orderBy)) {
            log.debug("orderBy==" + orderBy);
            sql.append(" order by ").append(orderBy);
        }
        String sqlStr = sql.toString();
        log.stat(METHOD_END + "==" + sqlStr);
        return sqlStr;
    }

    private StringBuilder getORConditions(List<Map<String, String>> paramColumns, Map<String, List<String>> dbParams) {
        StringBuilder conditions = new StringBuilder();
        String[] parameters = new String[paramColumns.size()];
        int i = 1;
        for (Map<String, String> dbParamMap : paramColumns) {
            parameters[i - 1] = dbParamMap.get("COLUMN");
            i++;
        }
        for (i = 0; i < paramColumns.size(); i++) {
            if (i > 0) {
                conditions.append(" OR ");
            }

            List<String> paramValues = dbParams.get(parameters[i]);
            conditions.append(parameters[i]).append(" in (");
            for (String value : paramValues) {
                conditions.append("'").append(value.replace("'", "''")).append("'").append(",");
            }
            conditions.deleteCharAt(conditions.lastIndexOf(",")).append(")");
        }
        return conditions;
    }


    private StringBuilder getANDConditions(List<Map<String, String>> paramColumns, String givenMaxInParam,
                                           Map<String, List<String>> dbParams, String alias, StringBuilder from) {
        StringBuilder conditions = new StringBuilder();
        int maxInParameters = 20;
        try {
            maxInParameters = Integer.parseInt(givenMaxInParam);
        } catch (NumberFormatException ignored) {
        }
        log.debug("maxInParameters==" + maxInParameters);

        String[] parameters = new String[paramColumns.size()];
        String[] key_type = new String[paramColumns.size()];

        int i = 1;

        for (Map<String, String> dbParamMap : paramColumns) {
            List<String> paramValues = dbParams.get(dbParamMap.get("COLUMN"));
            boolean isJoin = isOracleDriver && paramValues.size() > maxInParameters;
            if (isJoin) {
                from.append(", ").append(KEY_TABLE).append(" a").append(i);
            }
            parameters[i - 1] = dbParamMap.get("COLUMN");
            key_type[i - 1] = dbParamMap.get("KEY");
            i++;
        }

        for (i = 0; i < paramColumns.size(); i++) {
            if (i > 0) {
                conditions.append(" AND ");
            }
            List<String> paramValues = dbParams.get(parameters[i]);
            log.debug(key_type[i]);
            if ("like".equalsIgnoreCase(key_type[i])) {
                conditions.append(parameters[i]).append(" LIKE '").append(paramValues.get(0)).append("'");
            } else {
                boolean isJoin = isOracleDriver && paramValues.size() > maxInParameters;
//                log.debug(paramValues.size() +"; "+ isJoin);
                if (isJoin) {
                    conditions.append("a").append(i + 1).append(".parameter").append(" = '").append(parameters[i]).append("'");
                    conditions.append(" AND a").append(i + 1).append(".value").append(" = ").append(alias).append(".").append(parameters[i]);
                } else if (paramValues.size() > 0) {
                    String delChar1 = "{";
                    String delChar2 = "}";
                    conditions.append(parameters[i]).append(" in (");
                    for (String value : paramValues) {
                        if (isOracleDriver) {
                            if (value.endsWith(delChar2)) {
                                delChar1 = "@";
                                delChar2 = "@";
                            }
                            conditions.append("q'").append(delChar1).append(value).append(delChar2).append("'").append(",");
                        } else {
                            conditions.append("'").append(value.replace("'", "''")).append("'").append(",");
                        }
                    }
                    conditions.deleteCharAt(conditions.lastIndexOf(",")).append(")");
                } else {
                    conditions.delete(conditions.lastIndexOf("AND"), conditions.length());
                }
            }
        }
        return conditions;
    }


    //todo: sax parser instead of xmlHandler
    void sendScriptsToRemote(String[] scripts, XmlHandler xmlHandler) throws MessageStackException {
        long start = System.currentTimeMillis();
        StringBuilder errMsg = new StringBuilder();
        String dbDriver = (String) xmlHandler.findXpath("//DBDRIVER", "STRING");
        String dbURL = (String) xmlHandler.findXpath("//DBURL", "STRING");
        String user = (String) xmlHandler.findXpath("//USERNAME", "STRING");
        String password = (String) xmlHandler.findXpath("//PASSWORD", "STRING");
        String timeout = (String) xmlHandler.findXpath("//TIMEOUT", "STRING");
        Connection connection;
        try {
            connection = getConnection(dbDriver, dbURL, user, password);
//            connection.setAutoCommit(false);

            String dateFormat = (String) xmlHandler.findXpath("//DATEFORMAT", "STRING");
            String limsUser = (String) xmlHandler.findXpath("//LIMSUSER", "STRING");

            Statement stmt = connection.createStatement();
            stmt.setQueryTimeout(Integer.parseInt(timeout));  //todo

            for (String script : scripts) {
                if (StringUtils.isNotEmpty(script)) {
                    String[] underScripts = script.split("&&;&");
                    for (String underScript : underScripts) {
                        if (StringUtils.isNotEmpty(underScript)) {
                            String sql = underScript.replace("&&FORMAT_DATE&", dateFormat).replace("&&LIMS_USER&", limsUser);
                            log.debug("sql==" + sql);
                            try {
                                stmt.execute(sql);
                                log.debug("executed");
                            } catch (SQLException e) {
                                log.warn("error by execute");
                                errMsg.append("error by execute sql: ").append(sql).append(" ").append(e).append(Constants.lineSeparator);
                            }
                        }
                    }
                }
            }
            stmt.close();

            connection.close();
            if (StringUtils.isNotEmpty(errMsg.toString())) {
                throw new Exception(errMsg.toString());
            }
        } catch (Exception e) {
            throw new MessageStackException(e, MessageStackException.SEVERITY_CRITICAL, "send script to remote failed. Server: " + serverName + "; ",
                    MessageStackException.getCurrentMethod());
        }

        long millis = System.currentTimeMillis() - start;
        log.debug("required time in ms== " + millis);
    }


    void insertRowDataXmlToTable(RowsetRowXmlParser parser, byte[] dataXml, String tableName)
            throws Exception {
        long start = System.currentTimeMillis();
        JDBCInputXmlHandler xmlHandler = (JDBCInputXmlHandler) parser.getHandler();

        Connection connection = getConnection(xmlHandler);
        connection.setAutoCommit(false);
        PreparedStatement ps;

        Map<String, List<String>> dbParams = xmlHandler.dbParams;
        StringBuilder deleteStmt = new StringBuilder();
        StringBuilder whereStmt = new StringBuilder("where ");

        for (String key : dbParams.keySet()) {
            whereStmt.append(key).append(" in ('");
            for (String value : dbParams.get(key)) {
                whereStmt.append(value).append("', '");
            }
            whereStmt.delete(whereStmt.lastIndexOf(", '"), whereStmt.length()).append(")");
            whereStmt.append(" AND ");
        }
        deleteStmt.append("DELETE FROM ").append(tableName).append(" ").
                append(whereStmt.delete(whereStmt.lastIndexOf(" AND "), whereStmt.length()));
        log.debug("deleteStmt==" + deleteStmt);
        ps = connection.prepareStatement(deleteStmt.toString());
        int rowDeleted = ps.executeUpdate();
        log.debug("rowDeleted==" + rowDeleted);
        ps.close();

        if (dataXml != null && dataXml.length > 0) {
            ResultSet rs;
            String dataTypeStmtString = "select column_name, data_type, data_length from user_tab_columns where table_name= ?";
            log.debug("dataTypeStmtString==" + dataTypeStmtString + " (" + tableName + ")");


            StringBuilder columns = new StringBuilder("COLUMNS\n");
            StringBuilder columnValues = new StringBuilder("(");
            ps = connection.prepareStatement(dataTypeStmtString);
            ps.setString(1, tableName);
            rs = ps.executeQuery();

            while (rs.next()) {
                String columnName = rs.getString(1);
                String dataType = rs.getString(2);
                String dataLength = rs.getString(3);

                columns.append(columnName).append("  ").append(dataType);
                if ("VARCHAR2".equalsIgnoreCase(dataType)) {
                    columns.append("(").append(dataLength).append(")");
                }
                columns.append("  path '").append(columnName).append("',\n");

                columnValues.append(columnName).append(",");
            }
            rs.close();
            ps.close();

            StringBuilder insertStmt = new StringBuilder();
            insertStmt.append("INSERT INTO ").append(tableName).append("\n").
                    append(columnValues.deleteCharAt(columnValues.lastIndexOf(","))).append(") ").
                    append("SELECT xt.*\n").
                    append("FROM XMLTABLE('/ROWSET/ROW'\n").
                    append("    PASSING xmltype(?) ");

            insertStmt.append(columns.deleteCharAt(columns.lastIndexOf(",")));
            insertStmt.append(") xt");
            log.debug("insertStmt==" + insertStmt);

            ps = connection.prepareStatement(insertStmt.toString());
            ps.setAsciiStream(1, new ByteArrayInputStream(dataXml));
            int insertedRows = ps.executeUpdate();
            ps.close();
            log.stat("rows inserted== " + insertedRows);
        }
        connection.commit();
        connection.close();
        long millis = System.currentTimeMillis() - start;
        log.debug("table ==" + xmlHandler.tableName + ": required time in ms: " + millis);
    }


    public static void main(String[] args) {
        TABLE_TEMP = false;
        KEY_TABLE = "ISR$KEYS$TEST";
        IsrServerLogger log = LogHelper.initLogger("DEBUG", "test RemoteXml", "RemoteHandler", null);
        RemoteDataAssistant handler = new RemoteDataAssistant(log, "server test");

    }

}
