package com.uptodata.isr.remote.ws;

import com.uptodata.isr.observer.transfer.DbFileSupplier;
import com.uptodata.isr.observer.transfer.FileSupplier;
import com.uptodata.isr.observers.base.dbAccess.DbData;
import com.uptodata.isr.observers.childServer.ObserverChildServerImpl;
import com.uptodata.isr.remote.sax.JDBCInputXmlHandler;
import com.uptodata.isr.remote.sax.RowsetRowXmlParser;
import com.uptodata.isr.server.utils.fileSystem.FileSystemWorker;
import com.uptodata.isr.server.utils.logging.log4j2.LogHelper;
import com.uptodata.isr.utils.ConvertUtils;
import com.uptodata.isr.utils.exceptions.MessageStackException;
import com.uptodata.isr.utils.xml.dom.XmlHandler;
import org.apache.commons.codec.binary.Base64;
import org.apache.commons.collections4.map.CaseInsensitiveMap;
import org.apache.commons.io.FileUtils;
import org.apache.logging.log4j.Level;
import org.w3c.dom.Document;
import org.xml.sax.SAXException;

import javax.jws.WebMethod;
import javax.jws.WebService;
import javax.jws.soap.SOAPBinding;
import java.io.File;
import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.net.URISyntaxException;
import java.nio.charset.Charset;
import java.sql.Connection;
import java.sql.Driver;
import java.sql.DriverManager;
import java.sql.SQLException;
import java.util.Properties;

/**
 * Created by smiljanskii60 on 15.07.2014.
 */
@WebService(name = "JDBCCollectorInterface", targetNamespace = "http://ws.remote.isr.uptodata.com/")
@SOAPBinding(style = SOAPBinding.Style.DOCUMENT, use = SOAPBinding.Use.LITERAL)
public class JDBCCollectorService extends ObserverChildServerImpl {
    DbData dbData;
    private String logLevel;
    private String workFolderName = "workFolder";

    public JDBCCollectorService() {
        statusXmlMethod = 0;
        statusXml = initStatusXml();
    }

    public JDBCCollectorService(DbData dbData, String logLevel) {
        this.logLevel = logLevel;
        this.dbData = dbData;
        statusXmlMethod = 0;
    }

    public void init() throws MessageStackException {
        log = initLogger(logLevel, "remote collector service");
        boolean isWorkFolderCreated = initWorkFolder();
        if (!isWorkFolderCreated) {
            log.error("error==XsltTransformServer can not create work folder with name " + workFolderName);
        }
        statusXml = initStatusXml();  //test case for test exception: comment this row
    }


    /*
   * initiates the working directory if it doesn't exists
   * if wd exists, deletes the wd with the data
   * */
    private boolean initWorkFolder() throws MessageStackException {
        File workFolder = new File(workFolderName);
        if (workFolder.exists()) {
            try {
                boolean isOk = FileSystemWorker.tryToRemoveFolder(workFolder, 5, 500);
            } catch (Exception e) {
                String workFolderPath = workFolder.getAbsolutePath();
                String errorMsg = "Can not remove old work directory " + workFolderPath + System.lineSeparator() + " " + e;
                log.error("error==" + errorMsg);
                throw new MessageStackException(e, MessageStackException.SEVERITY_CRITICAL, errorMsg,
                        MessageStackException.getCurrentMethod());
            }
        }
        try {
            boolean isOk = FileSystemWorker.tryToCreateFolder(workFolderName, 5, 500);
        } catch (Exception e) {
            String workFolderPath = workFolder.getAbsolutePath();
            String errorMsg = "Can not create  work directory " + workFolderPath + " " + e;
            log.error("error==" + errorMsg);
            throw new MessageStackException(e, MessageStackException.SEVERITY_CRITICAL, errorMsg,
                    MessageStackException.getCurrentMethod());
        }

        return FileSystemWorker.checkFolder(workFolder);
    }


    public boolean pingRemoteDB(String dbDriver, String dbUrl, String user, String password) {
        try {
            DriverManager.registerDriver((Driver) (Class.forName(dbDriver).newInstance()));
            Connection connection = DriverManager.getConnection(dbUrl, user, password);
            connection.close();
            log.info("pingDB==true");
            return true;
        } catch (Exception e) {
            log.info("pingDB==false");
        }
        return false;
    }


    public String sendRemoteXml(String server, String serverName, byte[] xml, String logLevel, String logReference, String entity) throws MessageStackException {
        String currentName = "sendRemoteXml";
        String retStr = "";
        log = initLogger(logLevel, logReference, entity);
        log.stat(METHOD_BEGIN);


        try {
            JDBCInputXmlHandler saxXmlHandler = new JDBCInputXmlHandler();
            RowsetRowXmlParser parser = new RowsetRowXmlParser(saxXmlHandler, null) {
                @Override
                public void log(String actionTaken, Object logEntry, String logLevel) {
                    log.log(LogHelper.getLevel(logLevel), actionTaken + " == " + logEntry);
                }
            };

            if (log.getLevel().isLessSpecificThan(Level.DEBUG)) {
                CaseInsensitiveMap<String, String> args = new CaseInsensitiveMap<>();
                args.put("server", server);
                try {
                    args.put("xml", new String(xml, ConvertUtils.encoding));
                } catch (UnsupportedEncodingException e) {
                    log.info("exception==" + e);
                }
                log.debug(server);
                createMethodNode(currentName, args);
            }

            saxXmlHandler.setServerId(server);

            parser.parseXml(xml);
//            log.debug("xmlHandler==" + saxXmlHandler);
            Document document = XmlHandler.initialiseDocument("ROWSET");

            File repFolder = new File(workFolderName, saxXmlHandler.repId);
            log.debug("repFolder==" + repFolder);
            DbFileSupplier fileWorker = new DbFileSupplier(repFolder, dbData);
            log.debug(dbData);

            RemoteDataAssistant remoteAssistant = new RemoteDataAssistant(log, serverName);
            document = remoteAssistant.getXmlFromRemoteData(document, server, parser);

            byte[] bytes = ConvertUtils.convertDocumentToByte(document);
            File resultFile = FileUtils.getFile(repFolder.getAbsolutePath(), saxXmlHandler.entity + ".xml");
            FileUtils.writeByteArrayToFile(resultFile, bytes);

//            retStr = Base64.encodeBase64String(bytes);
            boolean isUploaded = fileWorker.uploadFile(resultFile, saxXmlHandler.transferKeyId, saxXmlHandler.configName, logLevel, logReference);
            log.info(resultFile.getName() + " : " + isUploaded);
            if (log.getLevel().intLevel() < Level.DEBUG.intLevel()) {
                log.debug("clean workFolder: " + fileWorker.getWorkFolder());
                clearFileWorker(fileWorker);
            }
        } catch (URISyntaxException | SAXException | MessageStackException | SQLException | IOException e) {
            String userStack = "error on the server '" + serverName + "' by get remote xml";
            log.error(userStack + "==" + e);
            onMethodFailed(e, MessageStackException.SEVERITY_CRITICAL, userStack, MessageStackException.getCurrentMethod(), statusXmlMethod);
        }
        log.stat(METHOD_END);
        return retStr;
    }


    public String deployRemote(String[] scripts, byte[] inputXml, String serverName, String logLevel, String logReference) throws MessageStackException {
        String currentName = "deployRemote";
        String retStr = "F";
        log = initLogger(logLevel, logReference);
        log.stat(METHOD_BEGIN);
        CaseInsensitiveMap<String, String> args = new CaseInsensitiveMap<>();
        args.put("logLevel", logLevel);
        args.put("logReference", logReference);
        args.put("inputXml", new String(inputXml, Charset.forName(ConvertUtils.encoding)));

        if (log.getLevel().isLessSpecificThan(Level.DEBUG)) {
            createMethodNode(currentName, args);
        }

        RemoteDataAssistant remoteAssistant = new RemoteDataAssistant(log, serverName);

        try {
            remoteAssistant.sendScriptsToRemote(scripts, new XmlHandler(inputXml));
            onMethodSuccess("success", statusXmlMethod);
            retStr = "T";
        } catch (Exception e) {
            String userStack = "error on the server " + serverName + " " + e.getMessage();
            log.error(userStack + "==" + e);
            onMethodFailed(e, MessageStackException.SEVERITY_CRITICAL, userStack,
                    MessageStackException.getCurrentMethod(), statusXmlMethod);
        }
        log.stat(METHOD_END);
        return retStr;
    }

    private void clearFileWorker(FileSupplier fileWorker) {
        if (fileWorker != null) {
            try {
                fileWorker.clearFileSystem();
            } catch (MessageStackException e) {
                log.error(e);
            }
        }
    }

    @WebMethod
    public String insertRowDataXmlToTable(String server, String serverName, byte[] confXml, byte[] dataXml,
                                   String logLevel, String logReference, String tableName) throws MessageStackException {
        String currentName = "insertXmlToTable";
        String retStr = "";
        log = initLogger(logLevel, logReference);
        log.stat(METHOD_BEGIN);

        try {
            JDBCInputXmlHandler saxXmlHandler = new JDBCInputXmlHandler();
            RowsetRowXmlParser parser = parseXml(saxXmlHandler, server, confXml, currentName);
            RemoteDataAssistant remoteAssistant = new RemoteDataAssistant(log, serverName);
            remoteAssistant.insertRowDataXmlToTable(parser, dataXml, tableName);

        } catch (Exception e) {
            String userStack = "error on the server '" + serverName + "': ";
            log.error(userStack + "==" + e);
            onMethodFailed(e, MessageStackException.SEVERITY_CRITICAL, userStack, MessageStackException.getCurrentMethod(),
                    statusXmlMethod);
        }
        log.stat(METHOD_END);
        return retStr;
    }

    RowsetRowXmlParser parseXml(JDBCInputXmlHandler saxXmlHandler, String server, byte[] confXml,String currentName)
            throws MessageStackException, IOException, SAXException {

        RowsetRowXmlParser parser = new RowsetRowXmlParser(saxXmlHandler, null) {
            @Override
            public void log(String actionTaken, Object logEntry, String logLevel) {
                log.log(LogHelper.getLevel(logLevel), actionTaken + " == " + logEntry);
            }
        };

        if (log.getLevel().isLessSpecificThan(Level.DEBUG)) {
            CaseInsensitiveMap<String, String> args = new CaseInsensitiveMap<>();
            args.put("server", server);
            try {
                args.put("xml", new String(confXml, ConvertUtils.encoding));
            } catch (UnsupportedEncodingException e) {
                log.info("exception==" + e);
            }
            log.debug(server);
            createMethodNode(currentName, args);
        }

        saxXmlHandler.setServerId(server);

        parser.parseXml(confXml);
        return parser;
    }

    public static void main(String[] args) {
        RemoteDataAssistant.TABLE_TEMP = false;
        RemoteDataAssistant.KEY_TABLE = "ISR$KEYS$TEST";
        try {
            byte[] inputXml = ConvertUtils.fileToBytes(System.getProperty("user.dir") + "\\test\\input.xml");
            byte[] dataXml = ConvertUtils.fileToBytes(System.getProperty("user.dir") + "\\test\\rowData.xml");

            DbData dbData = new DbData("isrowner_covib", "covib-d-d19", "8082",
                    "isrws_covib", "isrws_covib");
            JDBCCollectorService service = new JDBCCollectorService(dbData, "DEBUG");
            log = service.initLogger("DEBUG", "test");
            service.statusXml = service.initStatusXml();
            String ret = service.insertRowDataXmlToTable("201", "test", inputXml, dataXml, "DEBUG",
                    "test sendRemoteXml", "ISR$COVIB$DESIGNSAMPLE$CHANGES");
            log.debug(ret);

//            String[] scripts = new String[1];
//            scripts[0] = "";
//            String ret = service.deployRemote(scripts, inputXml, "test sendRemoteXml", "DEBUG", null);
//            log.debug(ret);


        } catch (Exception e) {
            e.printStackTrace();
            log.error(e);
        }
    }
}
