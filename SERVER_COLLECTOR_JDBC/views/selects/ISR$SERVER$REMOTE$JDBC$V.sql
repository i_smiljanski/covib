CREATE OR REPLACE VIEW ISR$SERVER$REMOTE$JDBC$V (SERVERID, ALIAS, HOST, PORT, RUNSTATUS, CLASSNAME, CONTEXT, TIMEOUT, MAX_JOBS, OBSERVERID, DESCRIPTION, NAMESPACE, TARGETMODELID, SERVERTYPEID, PLUGIN, DBURL, DBDRIVER, USERNAME, PASSWORD) AS select  s.serverid, s.alias,  s.host, s.port, s.runstatus, s.classname, s.context, s.timeout, s.max_jobs,  s.observerid, s.description, s.namespace, s.targetmodelid, s.servertypeid,
        isr$server$base.getServerParameter(serverid, 'PLUGIN') , 
        isr$server$base.getServerParameter(serverid, 'DBURL'), 
        isr$server$base.getServerParameter(serverid, 'DBDRIVER') ,
        isr$server$base.getServerParameter(serverid, 'USERNAME'), 
        isr$server$base.getServerParameter(serverid, 'PASSWORD')
   from isr$server s
  where servertypeid = 2