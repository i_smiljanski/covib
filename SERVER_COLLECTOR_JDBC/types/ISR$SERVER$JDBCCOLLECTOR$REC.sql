CREATE OR REPLACE TYPE ISR$SERVER$JDBCCOLLECTOR$REC UNDER   ISR$SERVER$COLLECTOR$REC (
constructor function ISR$SERVER$JDBCCOLLECTOR$REC(self in out nocopy ISR$SERVER$JDBCCOLLECTOR$REC) return self as result,
static function callFunction( oParameter in STB$MENUENTRY$RECORD, 
                       olNodeList out STB$TREENODELIST )  return STB$OERROR$RECORD,                       
-- ***************************************************************************************************
static function putParameters( oParameter  in STB$MENUENTRY$RECORD, 
                        olParameter in STB$PROPERTY$LIST, 
                        clParameter in clob  default null ) return STB$OERROR$RECORD,
-- ***************************************************************************************************
--  former callRoutine 
static function fillTmpTableForEntity( csEntity    in  varchar2, 
                      coParamList in  isr$ParamList$Rec, 
                      csWizard    in  varchar2, 
                      csMasterKey in  varchar2 default null,
                      nTimeout in number,
                      nServertypeId in number ) return STB$OERROR$RECORD
--******************************************************************************
, static function deployRemote (rtabScripts in ISR$CLOB$LIST,  csXml IN CLOB, csLoglevel  IN VARCHAR2,  csLogReference  IN VARCHAR2, nTimeout in number  ) RETURN CLOB
-- ***************************************************************************************************
, overriding member function getInputXmlDataPart (csDataEntity    in  varchar2, 
                             csCritEntity    in  varchar2,
                             csWizard    in  varchar2 default 'F',
                             csMasterKey in  varchar2 default null, 
                             coParamList in  iSR$ParamList$Rec) 
  return xmltype 
-- ***************************************************************************************************  
, overriding member function getInputXmlConditionPart (csEntity in varchar2) return xmltype   
                     
)