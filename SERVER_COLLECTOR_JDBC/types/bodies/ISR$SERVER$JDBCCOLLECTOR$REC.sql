CREATE OR REPLACE TYPE BODY ISR$SERVER$JDBCCOLLECTOR$REC as
 
constructor function ISR$SERVER$JDBCCOLLECTOR$REC(self in out nocopy ISR$SERVER$JDBCCOLLECTOR$REC) return self as result is
begin
    return;
end ISR$SERVER$JDBCCOLLECTOR$REC;
 -- ***************************************************************************************************
static function callfunction( oParameter in  STB$MENUENTRY$RECORD, 
                              olNodeList out STB$TREENODELIST )  return STB$OERROR$RECORD  
is
  exNoServerStartAllowed exception;
  --exNotExists            exception;  
  exRemoteServerstart    exception;
  exRemoteServerStop     exception;
  exRemote               exception;
  sCurrentName           varchar2(100) := $$PLSQL_UNIT||'.callFunction ('||oParameter.sToken||')';  
  oErrorObj              STB$OERROR$RECORD := STB$OERROR$RECORD();
  sMsg                   varchar2(4000);
  sMenuToken             varchar2(500) := Upper (oParameter.sToken);
  sRemoteEx              clob;
  sRemoteReturn          varchar2(1) := 'F';
  sInputxml              clob;
  sCheckForDialog        varchar2(1);
  sModifiedFlag          char(1) := 'F';
  
  nServerId  number;  

  cursor cGetServer is
   select s.serverid, s.alias, s.runstatus, s.classname, s.context, s.namespace, s.observer_port observerPort,
            s.observer_host observerHost,  s.host,  s.port,  s.server_path, s.servertypeid, s.targetmodelid, s.timeout
    from  isr$serverobserver$v s
    where  s.serverid = nServerId;
    
  rGetServer cGetServer%rowtype;   
     
  rTabScripts  ISR$CLOB$LIST;
   
  nCountServerInCrit  number;
  exCritServerExist  exception;

begin
  isr$trace.stat('begin','parameter: ' || sMenuToken,  sCurrentName);
  isr$trace.debug('value', 'oParameter',sCurrentName, oParameter);
  isr$trace.debug('status','current node',sCurrentName, Stb$object.getCurrentNode);
  
  sCheckForDialog := 'F';
  stb$object.setCurrentToken( oParameter.stoken );
  olNodeList := STB$TREENODELIST();
  
  nServerId := STB$OBJECT.getCurrentNodeid;
      
  case 
       
    when  Upper (oParameter.sToken) = 'CAN_DEPLOY_REMOTE' then    
      open cGetServer;
      fetch cGetServer into rGetServer;
      
      select ( xmlelement (
                 "SERVER"
               , xmlelement ("SERVERTYPE", rGetServer.servertypeid)
               , xmlelement ("HOST", rGetServer.observerhost )
               , xmlelement ("PORT", rGetServer.port)
               , xmlelement ("CONTEXT", rGetServer.context)
               , xmlelement ("SERVERNAME", '"' || rGetServer.alias || '"')
            --   , xmlelement ("LIMSUSER",STB$UTIL.getSystemParameter('LIMS_USER'))
               , xmlelement ("DATEFORMAT", stb$typedef.csInternalDateFormat)
               , xmlelement ("TIMEOUT", 10)  --todo
               , xmlelement ("NAMESPACE", rGetServer.namespace)
               ,  xmlagg ( xmlelement(EVALNAME(parametername), parametervalue)))
               ).getclobval()
      into sInputxml
      from isr$serverparameter
      where serverid = rGetServer.serverid
       and parametervalue is not null;
    
     isr$trace.debug ('sInputxml',  'sInputxml in logclob',  sCurrentName,  sInputxml);  
    
      select  create_script  bulk collect 
      into rTabScripts
      from  isr$meta$entity$targetmodel 
      where  targetmodelid = rGetServer.targetmodelid
      and create_script is not null
      order by ordernumber;
      
     isr$trace.debug ('rTabScripts',  'rTabScripts in logclob',  sCurrentName,  sys.anydata.convertCollection( rTabScripts));  
        sRemoteEx := deployRemote(rTabScripts, sInputxml, isr$trace.getJavaUserLoglevelStr, isr$trace.getReference, rGetServer.timeout);
      if sRemoteEx is not null then
        isr$trace.debug ('error',  'see sRemoteEx in logclob column',  sCurrentName,  sRemoteEx);  
        oErrorObj.handleJavaError (Stb$typedef.cnSeverityWarning, sRemoteEx, sCurrentName);  
        raise exRemote;
      end if;
      close cGetServer;
      if  oErrorObj.sSeverityCode in (Stb$typedef.cnNoError) and sRemoteReturn = 'T'  then 
        isr$server$base.setServerStatus(nServerId, 4);    
        isr$trace.debug (' serverstatus updated', 'serverid=' || nServerId, sCurrentName);
      end if;
                               
    else
       oErrorObj :=  isr$server$collector$rec.callfunction( oParameter,  olNodeList, 'ISR$SERVER$REMOTE$JDBC$V');
  end case;
  if nvl(sCheckForDialog, 'F') = 'T' then
    oErrorObj.sErrorCode := Utd$msglib.GetMsg('exNotExists',Stb$security.GetCurrentLanguage) ;
  end if;
  isr$trace.debug('value','olNodeList',sCurrentName,olNodeList);
  isr$trace.stat ('end', 'end', sCurrentName);
  return oErrorObj;
exception
  when exCritServerExist then
    oErrorObj.handleError ( Stb$typedef.cnSeverityWarning, sMsg, sCurrentName,  
                            dbms_utility.format_error_stack ||stb$typedef.crlf|| dbms_utility.format_error_backtrace  );    
    return oErrorObj;   
  when exRemoteServerstart then
    sMsg := utd$msglib.getmsg('exRemoteServerstart', stb$security.getcurrentlanguage, sMsg );
    oErrorObj.handleError ( Stb$typedef.cnSeverityWarning, sMsg, sCurrentName,  -- sCurrentName + additional parameter info etc. bei bedarf
                            dbms_utility.format_error_stack ||stb$typedef.crlf|| dbms_utility.format_error_backtrace  );    
    return oErrorObj;        
  when exRemoteServerStop then
    sMsg := utd$msglib.getmsg('exRemoteServerStop', stb$security.getcurrentlanguage, sMsg );
    oErrorObj.handleError ( Stb$typedef.cnSeverityWarning, sMsg, sCurrentName,  -- sCurrentName + additional parameter info etc. bei bedarf
                            dbms_utility.format_error_stack ||stb$typedef.crlf|| dbms_utility.format_error_backtrace  );    
    return oErrorObj;              
  when others then             -- exServerJdbcCollector 
    sMsg :=  utd$msglib.getmsg ('exUnexpectedError', stb$security.getcurrentlanguage, csP1 => sqlerrm(sqlcode));
    oErrorObj.handleError ( Stb$typedef.cnSeverityCritical, sMsg, sCurrentName, 
                            dbms_utility.format_error_stack ||stb$typedef.crlf|| dbms_utility.format_error_backtrace  );  
    return oErrorObj;    
end callfunction;

-- ***************************************************************************************************
static function putParameters( oParameter  in STB$MENUENTRY$RECORD, 
                        olParameter in STB$PROPERTY$LIST, 
                        clParameter in clob  default null ) return STB$OERROR$RECORD is
  oErrorObj             STB$OERROR$RECORD := STB$OERROR$RECORD();
begin
  oErrorObj :=  isr$server$collector$rec.putParameters( oParameter,  olParameter, 'ISR$SERVER$REMOTE$JDBC$V', clParameter);
  return oErrorObj;
end putParameters;

-- ***************************************************************************************************
static function fillTmpTableForEntity( csEntity    in  varchar2, 
                      coParamList in  isr$ParamList$Rec, 
                      csWizard    in  varchar2, 
                      csMasterKey in  varchar2 default null,
                      nTimeout in number,
                      nServertypeId in number ) return STB$OERROR$RECORD
is
  sCurrentName    constant varchar2(100)     := $$PLSQL_UNIT||'.fillTmpTableForEntity';
  sMsg            varchar2(4000);
  oErrorObj       STB$OERROR$RECORD := STB$OERROR$RECORD();
  oJdbcCollector  ISR$SERVER$JDBCCOLLECTOR$REC := ISR$SERVER$JDBCCOLLECTOR$REC();
  
begin 
  isr$trace.stat('begin','csEntity: '||csEntity, sCurrentName);
  oErrorObj := oJdbcCollector.fillTmpTableForEntityMember(csEntity, coParamList, csWizard, csMasterKey, nTimeout, nServertypeId);  
  isr$trace.stat('end','csEntity: '||csEntity, sCurrentName);
  return oErrorObj; 

exception 
  when others then
    sMsg :=  utd$msglib.getmsg ('exUnexpectedError', stb$security.getcurrentlanguage, csP1 => sqlerrm(sqlcode));
    oErrorObj.handleError ( Stb$typedef.cnSeverityCritical, sMsg, sCurrentName, 
                            dbms_utility.format_error_stack ||stb$typedef.crlf|| dbms_utility.format_error_backtrace  );  
    return oErrorObj;      
end fillTmpTableForEntity;

--******************************************************************************
static function deployRemote (rtabScripts in ISR$CLOB$LIST,  csXml IN CLOB, csLoglevel  IN VARCHAR2,  csLogReference  IN VARCHAR2, nTimeout in number  ) RETURN CLOB
AS LANGUAGE JAVA
NAME 'com.uptodata.isr.db.wsclient.RemoteDBClient.deployRemote( oracle.sql.ARRAY, java.sql.Clob, java.lang.String,  java.lang.String, int) return java.sql.Clob';

 -- *************************************************************************************************** 
overriding member function getInputXmlDataPart (csDataEntity    in  varchar2, 
                             csCritEntity    in  varchar2,
                             csWizard    in  varchar2 default 'F',
                             csMasterKey in  varchar2 default null, 
                             coParamList in  iSR$ParamList$Rec) 
  return xmltype 
is
  sCurrentName constant varchar2(100) := $$PLSQL_UNIT||'.getInputXmlDataPart';
  oErrorObj             STB$OERROR$RECORD := STB$OERROR$RECORD();
  sMsg                  varchar2(4000);
          
  sStatement            varchar2(32767);
  xInputXML             xmltype;
    
 -- sTargetEntity varchar2(500) := csEntity; --the target entity can be different from sEntity for wizard entities 
  sEntityType   varchar2(1); -- T isrmanaged, F non isrmanaged
  nTimeout      number;
  sOrderexpr    isr$meta$entity.orderexpr%type;
  
  nRepID            constant integer := coParamList.GetParamNum('REPID');
  nRepTypeID        constant integer := coParamList.GetParamNum('REPORTTYPEID');  
  
  sTempTabSql      varchar2(32767);
   
  
  cursor cGetColumns is
    select distinct case when description is not null then description||' ' end || attrname attrname, datatype, sapattribute
      from isr$meta$attribute a
     where entityName in (csCritEntity) 
       and istemp = 'F'
    order by 1;    
    
  cursor cGetRelColsChild is
    select distinct r.childattrname, r.relationtype
      from isr$meta$relation r, isr$meta$relation$report rr
     where r.relationname = rr.relationname
       AND r.childentityname in (csDataEntity, csCritEntity)
       AND rr.reporttypeid = nRepTypeID
       AND (csWizard = 'F' or (csWizard = 'T' and r.relationtype != 'ALIAS'))
       AND r.relationtype not in ('SK');
  
  cursor cGetRelColsParent is
    select distinct r.parentattrname, r.relationtype
      from isr$meta$relation r, isr$meta$relation$report rr
     where r.relationname = rr.relationname
       and r.parententityname in (csDataEntity, csCritEntity)
       and rr.reporttypeid = nRepTypeID
       and (csWizard = 'F' or (csWizard = 'T' and r.relationtype != 'ALIAS'))
       and r.relationtype not in ('SK');       

  cursor cGetRelations(csColName in varchar2, csChild in varchar2 default 'T') is
    select r.parententityname, r.parentattrname, r.childentityname, r.childattrname
      from isr$meta$relation r, isr$meta$relation$report rr
     where r.relationname = rr.relationname
       and (    (r.childentityname in (csDataEntity, csCritEntity) and r.childattrname = csColName and csChild = 'T')
             or (r.parententityname in (csDataEntity, csCritEntity) and r.parentattrname = csColName and csChild = 'F'))
       and rr.reporttypeid = nRepTypeID
       and (csWizard = 'F' or (csWizard = 'T' and r.relationtype != 'ALIAS'))
       and r.relationtype not in ('SK')
     order by 4;
       
  type tGetTempValues is ref cursor;
  cGetTempValues      tGetTempValues;
  sTempValue          varchar(24000);
  
  cursor cGetDisplayValue is
    select display
      from ISR$META$CRIT
     where entity = csDataEntity;
    
  sDisplayValue ISR$META$CRIT.DISPLAY%type;
  sSearchValue  varchar2(4000);   
  xDbParameter  xmltype;
  xDBRelParam   xmltype;  
     
 oCollector   isr$server$collector$rec := isr$server$collector$rec();

begin
  isr$trace.stat(csDataEntity||' begin','csDataEntity: '||csDataEntity, sCurrentName);
  -- do no longer order the data when it is selected [OPPIS-256]
  if nvl(STB$UTIL.getSystemParameter('REMOTE_ENTITY_ORDER'), 'F') = 'T' then
    sOrderexpr := oCollector.getInputXmlOrderExpr(csCritEntity);
  end if;
  
    <<GetColumns>>    
    begin
      select xmlconcat(
               xmlelement("COLUMNS", 
                 xmlagg(xmlelement("COLUMN", xmlattributes(datatype as "columntype", attrname as "columnname"), attrname) order by attrname)),
               -- do no longer order the data when it is selected [OPPIS-256] because multiple servers would use same entityorderby
               case when sOrderexpr is not null then xmlelement("ORDERBY", sOrderexpr) end)
      into xInputXML                   
      from (select distinct case when description is not null then description||' ' end || attrname attrname, datatype, sapattribute
              from isr$meta$attribute a
             where entityName IN (csCritEntity) 
               and istemp = 'F');               
    exception
      when no_data_found then
        -- ?? was machen wir da? Entity ist nicht richtig konfiguriert!!
        -- eigentlich handle und dann raise?
        isr$trace.error('error: no columns!', sCurrentName, 'no columns for entity '||csCritEntity);
    end GetColumns;   
    
    isr$trace.debug(csDataEntity||' xInputXML', 'after retrieving of colums', sCurrentName, xInputXML);   
              
    -- Condition through parent criteria/temporary tables
    <<RelationColumns>>
    for rGetRelCols in cGetRelColsChild loop       
      xDbParameter    := null;
      
      <<RealationsToParents>>
      for rGetRelations in cGetRelations(rGetRelCols.childattrname) loop
        isr$trace.debug(csDataEntity||' rGetRelations.parententityname',  rGetRelations.parententityname, sCurrentName);
        isr$trace.debug(csDataEntity||' rGetRelations.parentattrname',  rGetRelations.parentattrname, sCurrentName);
        isr$trace.debug(csDataEntity||' rGetRelations.childentityname',  rGetRelations.childentityname, sCurrentName);
        isr$trace.debug(csDataEntity||' rGetRelations.childattrname',  rGetRelations.childattrname, sCurrentName);
        
        -- temporary tables
        sTempTabSql :=           
          'SELECT  XMLAGG( XMLELEMENT("'||rGetRelations.childattrname ||'", XMLCDATA(datacolumn)))'||
            'FROM (SELECT TO_CHAR('||rGetRelations.parentattrname||') datacolumn FROM TMP$'||rGetRelations.parententityname ||' where trim(TO_CHAR('||rGetRelations.parentattrname||')) is not null)';      
          isr$trace.debug(csDataEntity||' sTempTabSql', sTempTabSql, sCurrentName);       
        begin     
          execute immediate sTempTabSql into xDBRelParam;          
          isr$trace.debug(csDataEntity||' xDBRelParam', ' after execte immediate - see clobval ->', sCurrentName, xDBRelParam);      
          
          if xDBRelParam is null then        
            oCollector.writeCriteriaCondition( rGetRelations.parententityname, rGetRelations.parentattrname, rGetRelations.childattrname, nRepID, xDBRelParam);
            isr$trace.debug(csDataEntity||' xDBRelParam', 'after criteria - see clobval ->', sCurrentName, xDBRelParam);
          end if;    
 
        exception 
          when others then
            -- wieso kommt hier eine exception?             
            isr$trace.debug(csDataEntity||' what exception?', 'see clobval ->', sCurrentName, 
                            dbms_utility.format_error_stack ||stb$typedef.crlf|| dbms_utility.format_error_backtrace);
            -- criteria
            oCollector.writeCriteriaCondition( rGetRelations.parententityname, rGetRelations.parentattrname, rGetRelations.childattrname, nRepID, xDBRelParam);   
            isr$trace.debug(csDataEntity||' xDBRelParam', 'after criteria in exception - see clobval ->', sCurrentName, xDBRelParam);  
        end abc;
        
        select xmlconcat(xDbParameter, xDBRelParam) 
        into xDbParameter
        from dual;        
        isr$trace.debug(csDataEntity||' xDbParameter', 'see clobval ->', sCurrentName, xDbParameter);
        
        -- GRID reference for masterkey 
        select xmlconcat(xDbParameter,
                 (select xmlelement(evalname(childattrname),xmlcdata(csMasterKey))          
                  from  ( select childattrname
                          from isr$meta$relation r
                             , isr$meta$relation$report rr
                             , isr$meta$grid$entity ge1
                             , isr$meta$grid$entity ge2
                         where r.relationname = rr.relationname
                           and rr.reporttypeid = STB$OBJECT.getCurrentReporttypeId
                           and ge1.entityname = csDataEntity
                           and ge2.columnpos = 1
                           and ge1.entityname = r.childentityname
                           and ge2.entityname = r.parententityname
                           and ge1.wizardentity = ge2.wizardentity
                           and ge1.reporttypeid = ge2.reporttypeid
                           and ge2.reporttypeid = rr.reporttypeid)
                   where csWizard = 'T'))                   
          into xDbParameter    
          from dual;  
            
        isr$trace.debug(csDataEntity||' xDbParameter', 'with grid regerence? see clobval ->', sCurrentName, xDbParameter);            
                                               
      end loop RealationsToParents;
            
      if xDbParameter is null and rGetRelCols.relationtype = 'FK' and csWizard = 'F' then
        select xmlelement(evalname(rGetRelCols.childattrname))
        into xDbParameter
        from dual;
        isr$trace.debug(csDataEntity||' xDbParameter', 'see clobval ->', sCurrentName, xDbParameter);
      end if; 
      
      select xmlconcat(xInputXML, xmlelement("DBPARAMETER", xmlattributes(rGetRelCols.childattrname as "columnname", rGetRelCols.relationtype as "key"), xDbParameter))
      into xInputXML 
      from dual;
      isr$trace.debug(csDataEntity||' xInputXML',  'within loop', sCurrentName, xInputXML);
      
      --sValues := sValues||'</DBPARAMETER>';
    END LOOP RelationColumns;
    
    -- das ist nicht mehr nÃƒÂ¶tig oder was verkehrt gemacht
    --    select xmlconcat(xInputXML, xDbParameter) 
    --    into xInputXML
    --    from dual;
    isr$trace.debug(csDataEntity||' xInputXML',  'finished RelationColumns start inverse looking', sCurrentName, xInputXML);
        
    -- for inverse looking           
    <<InvRealationsToParents>>       
    for rGetRelCols in cGetRelColsParent loop      
      xDbParameter := null;
      xDBRelParam  := null;
      
      for rGetRelations in cGetRelations( rGetRelCols.parentattrname, 'F') loop
        isr$trace.debug(csDataEntity||' inverse rGetRelations.parententityname',  rGetRelations.parententityname, sCurrentName);
        isr$trace.debug(csDataEntity||' inverse rGetRelations.parentattrname',  rGetRelations.parentattrname, sCurrentName);
        isr$trace.debug(csDataEntity||' inverse rGetRelations.childentityname',  rGetRelations.childentityname, sCurrentName);
        isr$trace.debug(csDataEntity||' inverse rGetRelations.childattrname',  rGetRelations.childattrname, sCurrentName);
        
        -- temporary tables        
        
        sTempTabSql :=           
          'SELECT  XMLAGG( XMLELEMENT("'||rGetRelations.parentattrname ||'", XMLCDATA(datacolumn)))'||
            'FROM (SELECT distinct TO_CHAR('||rGetRelations.childattrname||') datacolumn FROM TMP$'||rGetRelations.childentityname  ||' where trim(TO_CHAR('||rGetRelations.parentattrname||')) is not null)';   
        isr$trace.debug(csDataEntity||' sTempTabSql', sTempTabSql, sCurrentName);         
        begin     
          execute immediate sTempTabSql into xDBRelParam;           
          --sRelationValues := xDbParam.getClobVal(); 
          isr$trace.debug(csDataEntity||' xDBRelParam', 'see clobval', sCurrentName, xDBRelParam);          
             
        exception when others then
          -- schauen ob das vorkommt?
          isr$trace.warn(csDataEntity||' inverse execute others',  'others error', sCurrentName);
         ---!!!!
          null;     
        end;
        
        select xmlconcat(xDbParameter, xDBRelParam) 
        into xDbParameter
        from dual;  
        
      end loop;  
      
      isr$trace.debug(csDataEntity||' sRelationValues.xml inverse',  'see xml in logclob -->', sCurrentName, xDbParameter);          
      if xDbParameter is null and csWizard = 'F' then
        -- if the entity is the first entity in list
        xDBRelParam := null;
        for rGetBranches in (select distinct 'T' from isr$collector$entities where branch = csDataEntity) loop  --isrc-691
          oCollector.writeCriteriaCondition( csDataEntity, rGetRelCols.parentattrname, rGetRelCols.parentattrname, nRepid, xDBRelParam);
        end loop;
      end if;    
      
      select xmlconcat(xInputXML,                        
                       xmlelement("DBPARAMETER", xmlattributes(rGetRelCols.parentattrname as "columnname"), 
                                  xmlconcat(xDbParameter, xDBRelParam) ))
      into xInputXML 
      from dual;   
      isr$trace.debug(csDataEntity||' xInputXML',  'withininverse looking', sCurrentName, xInputXML);   
      
    end loop InvRealationsToParents;
    isr$trace.debug(csDataEntity||' xInputXML',  'finished inverse looking', sCurrentName, xInputXML);
    
    -- criteria for entity itself
    if csWizard = 'F' and csDataEntity = csCritEntity then  --DSIS-233 and ISRC-1159
      xDBRelParam := null;
      xDbParameter := null;
      for rGetKeyAttr in (select attrname from isr$meta$attribute where entityname = csDataEntity and iskey = 'T') loop
        oCollector.writeCriteriaCondition(csDataEntity, rGetKeyAttr.attrname, rGetKeyAttr.attrname, nRepid, xDBRelParam);
        if xDBRelParam is not null then
          select xmlconcat(xInputXML,                        
                         xmlelement("DBPARAMETER", xmlattributes(rGetKeyAttr.attrname as "columnname"), 
                                    xmlconcat(xDbParameter, xDBRelParam) ))
          into xInputXML 
          from dual;   
        end if;
        isr$trace.debug(csDataEntity||' xInputXML',  'entity criteria', sCurrentName, xInputXML);   
      end loop;    
    end if;    
  isr$trace.stat(csDataEntity||' end','csEntity: '||csDataEntity, sCurrentName);
  return xInputXML;   
exception 
  when others then
    sMsg :=  utd$msglib.getmsg ('exUnexpectedError', stb$security.getcurrentlanguage, csP1 => sqlerrm(sqlcode));
    oErrorObj.handleError ( Stb$typedef.cnSeverityCritical, sMsg, sCurrentName, 
                            dbms_utility.format_error_stack ||stb$typedef.crlf|| dbms_utility.format_error_backtrace  );  
    return null; 
end getInputXmlDataPart;

overriding member function getInputXmlConditionPart (csEntity    in  varchar2) 
  return xmltype 
is
  sCurrentName constant varchar2(100) := $$PLSQL_UNIT||'.getInputXmlConditionPart';
  oErrorObj             STB$OERROR$RECORD := STB$OERROR$RECORD();
  sMsg                  varchar2(4000);
  
  xDbParameter          xmltype;
  xDBcondParam          xmltype;
   
begin
  isr$trace.stat('begin','csEntity: '||csEntity, sCurrentName);
  for cCoditions in (select attrname, conditionlist, relationtype
                     from tmp$entity$condition
                     where entityname = csEntity) loop
                     
    xDBcondParam := null;   
      select xmlagg(xmlelement(evalname(cCoditions.attrname),condval))
        into xDBcondParam
      from (select column_value as condval FROM table(cCoditions.conditionlist)) condlist;
    
    isr$trace.debug(csEntity||' xDBcondParam',  'additional conditions for column '||cCoditions.attrname, sCurrentName, xDBcondParam);  
    if xDBcondParam is not null then
      select xmlconcat(xDbParameter,                        
                       xmlelement("DBPARAMETER", xmlattributes(cCoditions.attrname as "columnname", cCoditions.relationtype as "key"), xDBcondParam))
          into xDbParameter 
          from dual;
    end if;                
    isr$trace.debug(csEntity||' xDbParameter',  'additional conditions for column '||cCoditions.attrname, sCurrentName, xDbParameter);          
  end loop;
  
  isr$trace.stat('end','csEntity: '||csEntity, sCurrentName);
  return xDbParameter; 
exception 
  when others then
    sMsg :=  utd$msglib.getmsg ('exUnexpectedError', stb$security.getcurrentlanguage, csP1 => sqlerrm(sqlcode));
    oErrorObj.handleError ( Stb$typedef.cnSeverityCritical, sMsg, sCurrentName, 
                            dbms_utility.format_error_stack ||stb$typedef.crlf|| dbms_utility.format_error_backtrace  );  
    return null;
end getInputXmlConditionPart;

end;