  SELECT XMLELEMENT("experiment", XMLATTRIBUTES('&&Experiment&' AS "name" ,'matrixfactor' AS "type"),
  (
  select
  Xmlagg(XMLConcat(
    Xmlagg(XMLELEMENT("calculation", xmlconcat(targ,val,res)) order by acceptedrun, analyteorder, species, matrix, runid, nominalconc, ref_group, own_group)
   ) order by acceptedrun, analyteorder, species, matrix, runid, nominalconc, ref_group, own_group)
   from (
   select acceptedrun, analyteid, analyteorder, species, matrix, ref_group, own_group, runid, nominalconc,
      XMLELEMENT("target",
        XMLELEMENT("acceptedrun", acceptedrun),
        XMLELEMENT("analyteid", analyteid),
        XMLELEMENT("species", species),
        XMLELEMENT("matrix", matrix),
        XMLELEMENT("nominalconc", &&ConcFormatFunc&(&&ConcRepresentationFunc&(nominalconc, &&ConcFigures&), &&ConcFigures&)),
        XMLELEMENT("group", own_group),
        XMLELEMENT("refgroup", ref_group),
        XMLELEMENT("blocktype", blocktype),
        XMLELEMENT("internalstdname", internalstdname),
        XMLELEMENT("runid", runid)
        ) targ,
      XMLELEMENT("values", Xmlagg(
        XMLELEMENT("sample", XMLATTRIBUTES(sampleid AS "sampleid", sampleresultrawid AS "sampleresultrawid" ),
          XMLELEMENT("levelname", regexp_replace(name,'^.*[ _\-](\w+)$','\1',1,1,'i')),
          XMLELEMENT("name", name),
          XMLELEMENT("source", source),
          XMLELEMENT("analytearea", &&PeakFormatFunc&(&&PeakRepresentationFunc&(analytearea, &&PeakFigures&), &&PeakFigures&)),
          XMLELEMENT("mf-a", &&MFFormatFunc&(&&MFRepresentationFunc&(mf, &&MFFigures&), &&MFFigures&)),
          XMLELEMENT("internalstandardarea", &&PeakFormatFunc&(&&PeakRepresentationFunc&(internalstandardarea, &&PeakFigures&), &&PeakFigures&)),
          XMLELEMENT("mf-is", &&MFFormatFunc&(&&MFRepresentationFunc&(mf_is, &&MFFigures&), &&MFFigures&)),
          XMLELEMENT("norm-mf", &&MFFormatFunc&(&&MFRepresentationFunc&(norm_mf, &&MFNFigures&), &&MFNFigures&)),
          XMLELEMENT("replicatenumber", replicatenumber),
          XMLELEMENT("runsamplesequencenumber", runsamplesequencenumber),
          XMLELEMENT("deactivated", isdeactivated),
          case
            when isr_deactivation = 'T' then
              XMLELEMENT("deactivationreason", deactivationreason)
            else null
          end)
          order by runsamplesequencenumber, replicatenumber)
          ) val,
      XMLELEMENT("result",
        XMLELEMENT("n-a", Count(useanalytearea)),
        XMLELEMENT("m-a", Count(analytearea)),
        XMLELEMENT("npercm-a", case when Count(analytearea) = 0 then null else FormatRounded(round(Count(useanalytearea)/Count(analytearea)*100, &&DecPlPercBias&), &&DecPlPercBias&) end ),
        XMLELEMENT("mean-a", &&PeakFormatFunc&(&&PeakRepresentationFunc&(max(avg_analytearea), &&PeakFigures&), &&PeakFigures&)),
        XMLELEMENT("mean-mf-a", &&MFFormatFunc&(&&MFRepresentationFunc&(max(avg_mf), &&MFFigures&), &&MFFigures&)),
        XMLELEMENT("n-is", Count(useinternalstandardarea)),
        XMLELEMENT("m-is", Count(internalstandardarea)),
        XMLELEMENT("npercm-is", case when Count(internalstandardarea) = 0 then null else FormatRounded(round(Count(useinternalstandardarea)/Count(internalstandardarea)*100, &&DecPlPercBias&), &&DecPlPercBias&) end ),
        XMLELEMENT("mean-is", &&PeakFormatFunc&(&&PeakRepresentationFunc&(max(avg_internalstandardarea), &&PeakFigures&), &&PeakFigures&)),
        XMLELEMENT("mean-mf-is", &&MFFormatFunc&(&&MFRepresentationFunc&(max(avg_mf_is), &&MFFigures&), &&MFFigures&)),
        XMLELEMENT("mean-norm-mf",&&MFFormatFunc&(&&MFRepresentationFunc&(avg(case when '&&FinalAccuracy&' = 'T' then usenorm_mf else round(usenorm_mf, &&MFNFigures&) end), &&MFNFigures&), &&MFNFigures&)),
        XMLELEMENT("sd-norm-mf",&&MFFormatFunc&(&&MFRepresentationFunc&(stddev(case when '&&FinalAccuracy&' = 'T' then usenorm_mf else round(usenorm_mf, &&MFNFigures&) end), &&MFNFigures&), &&MFNFigures&)),
        XMLELEMENT("cv-norm-mf",FormatRounded(round(case when '&&FinalAccuracy&' = 'T' then Stddev(usenorm_mf)/Avg(usenorm_mf)*100
                                                         else round(Stddev(round(usenorm_mf, &&MFNFigures&)), &&MFNFigures&)/
                                                              round(Avg(round(usenorm_mf, &&MFNFigures&)), &&MFNFigures&)*100
                                                    end, &&DecPlPercCV&), &&DecPlPercCV&))
        ) res
   from
   (
   with analyterun as (
   SELECT knownID, sampleID, sampleresultrawID, name, source, name_group, acceptedrun,
     runid, analyteid, analyteorder, species, matrix, replicatenumber, analytearea, useanalytearea, internalstandardarea, useinternalstandardarea, internalstdname,
     case when '&&FinalAccuracy&' = 'T' then avg(useanalytearea) over (partition by acceptedrun, analyteid, analyteorder, species, matrix, name_group, runid, internalstdname, nominalconc)
          else &&PeakRepresentationFunc&(avg(useanalytearea) over (partition by acceptedrun, analyteid, analyteorder, species, matrix, name_group, runid, internalstdname, nominalconc), &&PeakFigures&)
     end avg_analytearea,
     case when '&&FinalAccuracy&' = 'T' then avg(useinternalstandardarea) over (partition by acceptedrun, analyteid, analyteorder, species, matrix, name_group, runid, internalstdname, nominalconc)
          else &&PeakRepresentationFunc&(avg(useinternalstandardarea) over (partition by acceptedrun, analyteid, analyteorder, species, matrix, name_group, runid, internalstdname, nominalconc), &&PeakFigures&)
     end avg_internalstandardarea,
     ISDEACTIVATED, isr_deactivation, deactivationreason, runsamplesequencenumber, nominalconc
     FROM
     (
      SELECT K.ID knownID, s.ID sampleID, srw.ID sampleresultrawID, K.NAME, k.source,
        case when regexp_like(k.name,'PURE','i') then 'PURE'
             --when regexp_like(k.name,'PURE','i') then 'MF_Ref_H'
             when regexp_like(k.name,'POST','i') then 'POST'
             --when regexp_like(k.name,'POST','i') then 'MF_Test_H'
             else k.name
        end name_group,
        case when ra.RunAnalyteRegressionStatus = &&RejectedRunAnalyte& then 'F' else 'T' end acceptedrun,
        case
          when d.code is not null then 'T'
          else s.ISDEACTIVATED
        end ISDEACTIVATED,
        case
          when d.code is not null then 'T'
          else 'F'
        end isr_deactivation,
        d.reason deactivationreason, srw.internalstdname,
        s.runid, s.runsamplesequencenumber, k.concentrationconv nominalconc,
        ra.analyteid, sa.analyteorder, a.species, a.sampletypeid matrix, r.runtypedescription, s.replicatenumber, k.flagpercent,
        case when '&&FinalAccuracy&' = 'T' then srw.analytearea
             else &&PeakRepresentationFunc&(srw.analytearea, &&PeakFigures&)
        end analytearea,
        case when ISDEACTIVATED = 'T' or d.code is not null then null
             when '&&FinalAccuracy&' = 'T' then srw.analytearea
             else &&PeakRepresentationFunc&(srw.analytearea, &&PeakFigures&)
        end useanalytearea,
        case when '&&FinalAccuracy&' = 'T' or d.code is not null then srw.internalstandardarea
             else &&PeakRepresentationFunc&(srw.internalstandardarea, &&PeakFigures&)
        end internalstandardarea,
        case when ISDEACTIVATED = 'T' or d.code is not null then null
             when '&&FinalAccuracy&' = 'T' then srw.internalstandardarea
             else &&PeakRepresentationFunc&(srw.internalstandardarea, &&PeakFigures&)
        end useinternalstandardarea
      FROM &&TempTabPrefix&bio$run$ana$known K,
           &&TempTabPrefix&bio$run r,
           &&TempTabPrefix&bio$run$analytes ra,
           &&TempTabPrefix&bio$study$analytes sa,
           &&TempTabPrefix&bio$assay a,
           &&TempTabPrefix&bio$run$samples s,
           &&TempTabPrefix&bio$run$sample$result$raw srw,
           &&TempTabPrefix&bio$covib$runana$smp$deact d,
           isr$crit kwz,
           table(&&LALPackage&.GetRunStates) rs
      WHERE K.ID = s.runknownID
        AND R.ID = s.runid AND R.ID = srw.runid
        --AND r.studyassayid =s.studyassayid AND s.studyassayid = srw.studyassayid
        AND r.RuntypeDescription in (&&RunTypes&)
        AND s.ID = srw.runsampleID
        AND s.sampletype = 'known'
        and k.knowntype = s.runsamplekind
        and kwz.masterkey = '&&ExpMasterkey&'
        AND ra.id = srw.runanalyteid
        AND a.id = r.assayid
        AND sa.id = ra.analyteid
        AND s.studycode || '-' || r.runid || '-' || s.runsamplesequencenumber || '-' || sa.analyteorder = d.code (+)
        AND ra.runid = s.runid
        AND ra.studyID = '&&StudyID&'
        AND K.studyid = '&&StudyID&' AND s.studyid = '&&StudyID&'
        AND srw.studyID = '&&StudyID&' AND r.studyid = '&&StudyID&'
        and kwz.key = K.NAME
        and kwz.entity = '&&KnownEntity&'
        and repid = &&RepID&
        AND rs.column_value = r.runstatusnum &&AddCondition&
     ))
     select
       testmf.acceptedrun, testmf.analyteid, testmf.analyteorder, testmf.species, testmf.matrix, testmf.runid, testmf.name, testmf.internalstdname, testmf.source,
       testmf.name_group OWN_GROUP, ref_avg.name_group REF_GROUP, 'testmf'  blocktype,
       testmf.avg_analytearea, testmf.avg_internalstandardarea, testmf.runsamplesequencenumber,
       case when ref_avg.avg_analytearea = 0 then null
            when '&&FinalAccuracy&' = 'T' then testmf.analytearea/ref_avg.avg_analytearea
            else &&MFRepresentationFunc&(&&PeakRepresentationFunc&(testmf.analytearea, &&PeakFigures&)/&&PeakRepresentationFunc&(ref_avg.avg_analytearea, &&PeakFigures&), &&MFFigures&)
       end mf,
       case when ref_avg.avg_analytearea = 0 then null
            when '&&FinalAccuracy&' = 'T' then testmf.useanalytearea/ref_avg.avg_analytearea
            else &&MFRepresentationFunc&(&&PeakRepresentationFunc&(testmf.useanalytearea, &&PeakFigures&)/&&PeakRepresentationFunc&(ref_avg.avg_analytearea, &&PeakFigures&), &&MFFigures&)
       end usemf,
       case when ref_avg.avg_analytearea = 0 then null
            when '&&FinalAccuracy&' = 'T' then testmf.avg_analytearea/ref_avg.avg_analytearea
            else &&MFRepresentationFunc&(&&PeakRepresentationFunc&(testmf.avg_analytearea, &&PeakFigures&)/&&PeakRepresentationFunc&(ref_avg.avg_analytearea, &&PeakFigures&), &&MFFigures&)
       end avg_mf,
       testmf.analytearea, testmf.useanalytearea,
       case when ref_avg.avg_internalstandardarea = 0 then null
            when '&&FinalAccuracy&' = 'T' then testmf.internalstandardarea/ref_avg.avg_internalstandardarea
            else &&MFRepresentationFunc&(&&PeakRepresentationFunc&(testmf.internalstandardarea, &&PeakFigures&)/&&PeakRepresentationFunc&(ref_avg.avg_internalstandardarea, &&PeakFigures&), &&MFFigures&)
       end mf_is,
       case when ref_avg.avg_internalstandardarea = 0 then null
            when '&&FinalAccuracy&' = 'T' then testmf.useinternalstandardarea/ref_avg.avg_internalstandardarea
            else &&MFRepresentationFunc&(&&PeakRepresentationFunc&(testmf.useinternalstandardarea, &&PeakFigures&)/&&PeakRepresentationFunc&(ref_avg.avg_internalstandardarea, &&PeakFigures&), &&MFFigures&)
       end usemf_is,
       case when ref_avg.avg_internalstandardarea = 0 then null
            when '&&FinalAccuracy&' = 'T' then testmf.avg_internalstandardarea/ref_avg.avg_internalstandardarea
            else &&MFRepresentationFunc&(&&PeakRepresentationFunc&(testmf.avg_internalstandardarea, &&PeakFigures&)/&&PeakRepresentationFunc&(ref_avg.avg_internalstandardarea, &&PeakFigures&), &&MFFigures&)
       end avg_mf_is,
       (case when ref_avg.avg_analytearea = 0 then null
            when '&&FinalAccuracy&' = 'T' then testmf.analytearea/ref_avg.avg_analytearea
            else &&MFRepresentationFunc&(&&PeakRepresentationFunc&(testmf.analytearea, &&PeakFigures&)/&&PeakRepresentationFunc&(ref_avg.avg_analytearea, &&PeakFigures&), &&MFFigures&)
       end) /
       (case when ref_avg.avg_internalstandardarea = 0 or testmf.internalstandardarea = 0 then null
            when '&&FinalAccuracy&' = 'T' then testmf.internalstandardarea/ref_avg.avg_internalstandardarea
            else &&MFRepresentationFunc&(&&PeakRepresentationFunc&(testmf.internalstandardarea, &&PeakFigures&)/&&PeakRepresentationFunc&(ref_avg.avg_internalstandardarea, &&PeakFigures&), &&MFFigures&)
       end) norm_mf,
       case when testmf.isdeactivated = 'T' then null
            else (case when ref_avg.avg_analytearea = 0 then null
                       when '&&FinalAccuracy&' = 'T' then testmf.analytearea/ref_avg.avg_analytearea
                       else &&MFRepresentationFunc&(&&PeakRepresentationFunc&(testmf.analytearea, &&PeakFigures&)/&&PeakRepresentationFunc&(ref_avg.avg_analytearea, &&PeakFigures&), &&MFFigures&)
                  end) /
                 (case when ref_avg.avg_internalstandardarea = 0 or testmf.internalstandardarea = 0 then null
                       when '&&FinalAccuracy&' = 'T' then testmf.internalstandardarea/ref_avg.avg_internalstandardarea
                       else &&MFRepresentationFunc&(&&PeakRepresentationFunc&(testmf.internalstandardarea, &&PeakFigures&)/&&PeakRepresentationFunc&(ref_avg.avg_internalstandardarea, &&PeakFigures&), &&MFFigures&)
                  end)
       end usenorm_mf,
       testmf.internalstandardarea, testmf.useinternalstandardarea, testmf.replicatenumber,
       testmf.isdeactivated, testmf.isr_deactivation, testmf.deactivationreason, testmf.sampleid, testmf.sampleresultrawID, ref_avg.nominalconc
     from (select acceptedrun, min(avg_analytearea) avg_analytearea, min(avg_internalstandardarea) avg_internalstandardarea,
                  analyteid, analyteorder, species, matrix, runid, name_group, nominalconc
           from analyterun
           group by acceptedrun, analyteid, analyteorder, species, matrix, runid, name_group, nominalconc) ref_avg,
          analyterun testmf
     where (ref_avg.analyteid = testmf.analyteid)
       and ref_avg.acceptedrun = testmf.acceptedrun
       and ref_avg.runid = testmf.runid
       and ref_avg.name_group = 'PURE' and testmf.name_group = 'POST'
       and ref_avg.nominalconc  = testmf.nominalconc
     union all
     select
       ref.acceptedrun, ref.analyteid, ref.analyteorder, ref.species, ref.matrix, ref.runid, ref.name, ref.internalstdname, ref.source,
       ref.name_group OWN_GROUP, null REF_GROUP, 'reference'  blocktype,
       ref.avg_analytearea, ref.avg_internalstandardarea, ref.runsamplesequencenumber,
       null mf, null usemf, null avg_mf, ref.analytearea, ref.useanalytearea,
       null mf_is, null usemf_is,  null avg_mf_is, null norm_mf, null usenorm_mf,
       ref.internalstandardarea, ref.useinternalstandardarea, ref.replicatenumber,
       ref.isdeactivated, ref.isr_deactivation, ref.deactivationreason, ref.sampleid, ref.sampleresultrawID, ref.nominalconc
     from analyterun ref
     where ref.name_group = 'PURE'
     )
     group by acceptedrun, analyteid, analyteorder, species, matrix, nominalconc, REF_GROUP, own_GROUP, blocktype, internalstdname, runid)
     group by acceptedrun, analyteid, analyteorder, species, matrix, runid, nominalconc, ref_group, own_group)
    ) FROM dual
