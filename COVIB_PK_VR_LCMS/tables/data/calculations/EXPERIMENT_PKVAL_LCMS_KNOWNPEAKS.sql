  SELECT XMLELEMENT("experiment", XMLATTRIBUTES('&&Experiment&' AS "name" ,'knownpeaks' AS "type"),
           Xmlagg (XMLConcat(
             Xmlagg (XMLELEMENT("calculation", xmlconcat(targ,val,res)) order by acceptedrun, internalstdname, species, matrix, runno),
             XMLELEMENT("statistic", XMLATTRIBUTES(acceptedrun AS "acceptedrun", internalstdname AS "internalstdname", species as "species", matrix as "matrix"),
               XMLELEMENT("mean-cv-internalstandardarea", FormatRounded(Avg(cvinternalstandardarea), &&DecPlPercCV&))
             )) order by acceptedrun, internalstdname, species, matrix))
  FROM
   (SELECT internalstdname, species, matrix, acceptedrun, runid, runno,
      round(case when '&&FinalAccuracy&' = 'T' then case when Avg(useinternalstandardarea) = 0 then null else Stddev(useinternalstandardarea)/Avg(useinternalstandardarea)*100 end
                                                  else case when &&PeakRepresentationFunc&(Avg(useinternalstandardarea), &&PeakFigures&) = 0 then null
                                                            else &&PeakRepresentationFunc&(Stddev(useinternalstandardarea), &&PeakSDFigures&)
                                                                /&&PeakRepresentationFunc&(Avg(useinternalstandardarea), &&PeakFigures&)*100
                                                       end
                                             end, &&DecPlPercCV&) cvinternalstandardarea,
      case when '&&FinalAccuracy&' = 'T' then Avg(useinternalstandardarea)
           else round(Avg(useinternalstandardarea), &&PeakFigures&)
      end meaninternalstandardarea,
      XMLELEMENT("target",
        XMLELEMENT("acceptedrun", acceptedrun),
        XMLELEMENT("internalstdname", internalstdname),
        XMLELEMENT("species", species),
        XMLELEMENT("matrix", matrix),
        XMLELEMENT("runid", runid)
        ) targ,
      XMLELEMENT("values", Xmlagg(
        XMLELEMENT("sample",
          XMLELEMENT("name", name),
          XMLELEMENT("orderby", NameOrderBy),
          XMLELEMENT("replicatenumber", replicatenumber),
          XMLELEMENT("internalstandardarea", &&PeakFormatFunc&(&&PeakRepresentationFunc&(internalstandardarea, &&PeakFigures&), &&PeakFigures&)),
          XMLELEMENT("runsamplesequencenumber", runsamplesequencenumber),
          XMLELEMENT("entityorderby", entityorderby),
          XMLELEMENT("deactivated", isdeactivated),
          XMLELEMENT("deactivationreason", deactivationreason)
        ) order by /*name,*/ NameOrderBy, entityorderby, runsamplesequencenumber, replicatenumber)
      ) val,
      XMLELEMENT("result",
        XMLELEMENT("n-internalstandardarea", Count(case when useinternalstandardarea is not null then 1 else null end)),
        XMLELEMENT("m-internalstandardarea", Count(internalstandardarea)),
        XMLELEMENT("npercm-internalstandardarea", FormatRounded(round(Count(case when useinternalstandardarea is not null then 1 else null end)/Count(internalstandardarea)*100, &&DecPlPercBias&), &&DecPlPercBias&)),
        XMLELEMENT("mean-internalstandardarea", &&PeakFormatFunc&(&&PeakRepresentationFunc&(Avg(useinternalstandardarea), &&PeakFigures&), &&PeakFigures&)),
        XMLELEMENT("sd-internalstandardarea", &&PeakFormatFunc&(&&PeakRepresentationFunc&(Stddev(useinternalstandardarea), &&PeakSDFigures&), &&PeakSDFigures&)),
        XMLELEMENT("cv-internalstandardarea", FormatRounded(round(case when '&&FinalAccuracy&' = 'T' then case when Avg(useinternalstandardarea) = 0 then null else Stddev(useinternalstandardarea)/Avg(useinternalstandardarea)*100 end
                                                  else case when &&PeakRepresentationFunc&(Avg(useinternalstandardarea), &&PeakFigures&) = 0 then null
                                                            else &&PeakRepresentationFunc&(Stddev(useinternalstandardarea), &&PeakSDFigures&)
                                                                /&&PeakRepresentationFunc&(Avg(useinternalstandardarea), &&PeakFigures&)*100
                                                       end
                                             end, &&DecPlPercCV&), &&DecPlPercCV&))
        ) res
    FROM
    (
    SELECT Name, source, acceptedrun, runid, runno, species, matrix, internalstdname, replicatenumber,
     case
       when name like 'CAL%' then 1
       when name like 'LLOQ%' then 2
       when name like 'LQC%' then 3
       when name like 'LMQC%' then 4
       when name like 'MQC%' then 5
       when name like 'HQC%' then 6
       when name like 'DQC%' then 7
       else 8
     end NameOrderBy,
     internalstandardarea, min(ISDEACTIVATED) ISDEACTIVATED, max(deactivationreason) deactivationreason, runsamplesequencenumber, runsampleordernumber, savedordernumber, min(entityorderby) entityorderby, stabilitytype,
     case when min(ISDEACTIVATED) = 'T' then null else internalstandardarea end useinternalstandardarea
     FROM
     (SELECT K.ID knownID, s.ID sampleID, srw.ID sampleresultrawID, s.name, k.source,
        case when ra.RunAnalyteRegressionStatus = &&RejectedRunAnalyte& then 'F' else 'T' end acceptedrun,
        case
          when d.code is not null then 'T'
          else s.ISDEACTIVATED
        end ISDEACTIVATED,
        nvl(d.reason,ds.reason) deactivationreason, s.runid, r.runid runno, s.runsamplesequencenumber, s.runsampleordernumber, s.savedordernumber, s.entityorderby, k.concentrationconv nominalconc,
        ra.analyteid, sa.analyteorder, a.species, a.sampletypeid matrix, srw.internalstdname, r.runtypedescription, s.replicatenumber, k.flagpercent, k.stabilitytype,
        case when '&&FinalAccuracy&' = 'T' then srw.internalstandardarea
             else &&PeakRepresentationFunc&(srw.internalstandardarea, &&PeakFigures&)
        end internalstandardarea
      FROM &&TempTabPrefix&bio$run$ana$known K,
           &&TempTabPrefix&bio$run r,
           &&TempTabPrefix&bio$run$analytes ra,
           &&TempTabPrefix&bio$study$analytes sa,
           &&TempTabPrefix&bio$assay a,
           &&TempTabPrefix&bio$run$samples s,
           &&TempTabPrefix&bio$run$sample$result$raw srw,
           &&TempTabPrefix&bio$covib$runana$smp$deact d,
           &&TempTabPrefix&BIO$DEACTIVATED$SAMPLES ds,
           isr$crit kwz,
           table(&&LALPackage&.GetRunStates) rs
      WHERE K.ID = s.runknownID
        AND R.ID = s.runid AND R.ID = srw.runid
        --AND r.studyassayid =s.studyassayid AND s.studyassayid = srw.studyassayid
        AND r.RuntypeDescription in (&&RunTypes&)
        AND s.ID = srw.runsampleID
        --AND s.sampletype = 'known'
        and k.knowntype = s.runsamplekind
        AND ra.id = srw.runanalyteid
        AND sa.id = ra.analyteid
        AND a.id = r.assayid
        --AND ra.assayanalyteid = aa.ID
        AND s.studycode || '-' || r.runid || '-' || s.runsamplesequencenumber || '-' || sa.analyteorder = d.code (+)
        AND s.id = ds.runsampleid (+)
        AND ra.runid = s.runid
        AND ra.studyID = '&&StudyID&'
        AND K.studyid = '&&StudyID&' AND s.studyid = '&&StudyID&'
        AND srw.studyID = '&&StudyID&' AND r.studyid = '&&StudyID&'
        and kwz.key = K.NAMEPREFIX
        and kwz.entity = '&&KnownEntity&'
        and repid = &&RepID&
        AND rs.column_value = r.runstatusnum
        &&AddCondition&
     ) GROUP BY Name, source, acceptedrun, runid, runno, species, matrix, internalstdname, replicatenumber, internalstandardarea, runsamplesequencenumber, runsampleordernumber, savedordernumber, stabilitytype)
     GROUP BY species, matrix, internalstdname, acceptedrun, runid, runno)
    GROUP BY species, matrix, internalstdname, acceptedrun