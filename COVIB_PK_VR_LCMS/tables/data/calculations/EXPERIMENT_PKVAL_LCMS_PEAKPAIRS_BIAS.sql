  SELECT XMLELEMENT("experiment", XMLATTRIBUTES('&&Experiment&' AS "name" ,'&&PeakPairsBiCalcType&' AS "type"),
  (
  select Xmlagg(XMLConcat(
  Xmlagg(calc order by acceptedrun, analyteid, analyteorder, species, matrix, nominalconc),
    XMLELEMENT("statistic", XMLATTRIBUTES(acceptedrun AS "acceptedrun",
                                          analyteid AS "analyteid",
                                          species AS "species",
                                          matrix AS "matrix",
                                          runid AS "runid" ),
       XMLELEMENT("mean-recovery-analytearea", case when sum(n) != 0 then FormatRounded(round(sum(mean_recovery_analytearea*n)/sum(n),&&DecPlPercBias&),&&DecPlPercBias&) else null end),
       XMLELEMENT("mean-recovery-internalstandardarea", case when sum(n) != 0 then FormatRounded(round(sum(mean_recovery_isarea*n)/sum(n),&&DecPlPercBias&),&&DecPlPercBias&) else null end),
       XMLELEMENT("mean-recovery-range-analytearea", FormatRounded(round((max(mean_recovery_analytearea) - min(mean_recovery_analytearea)),&&DecPlPercBias&),&&DecPlPercBias&)),
       XMLELEMENT("mean-recovery-range-internalstandardarea", FormatRounded(round((max(mean_recovery_isarea) - min(mean_recovery_isarea)),&&DecPlPercBias&),&&DecPlPercBias&)),
       XMLELEMENT("flag-mean-recovery-range-analytearea", case when round((max(mean_recovery_analytearea) - min(mean_recovery_analytearea)),&&DecPlPercBias&) > 30 then '30' else '' end),
       XMLELEMENT("flag-mean-recovery-range-internalstandardarea", case when round((max(mean_recovery_isarea) - min(mean_recovery_isarea)),&&DecPlPercBias&) > 30 then '30' else '' end),
       XMLELEMENT("min-cv", FormatRounded(round(
                              case when min(cv_recovery_analytearea) < min(cv_recovery_isarea) then
                                min(cv_recovery_analytearea)
                              else
                                min(cv_recovery_isarea)
                              end
                            , &&DecPlPercCV&), &&DecPlPercCV&)),
       XMLELEMENT("max-cv", FormatRounded(round(
                              case when max(cv_recovery_analytearea) > max(cv_recovery_isarea) then
                                max(cv_recovery_analytearea)
                              else
                                max(cv_recovery_isarea)
                              end
                            , &&DecPlPercCV&), &&DecPlPercCV&))
       )) order by acceptedrun, analyteid, analyteorder, species, matrix)
  from (
    with analyterun as (
    SELECT acceptedrun, analyteid, analyteorder, species, matrix, runid, temperature, name, nominalconc,
           preparationdate, cycles, hours, longtermtime, longtermunits,
           sampleid, sampleresultrawid, ratio, analytearea, internalstandardarea,
           replicatenumber, runsamplesequencenumber, ISDEACTIVATED, isr_deactivation, deactivationreason,
           case when '&&FinalAccuracy&' = 'T' then Avg(case when ISDEACTIVATED = 'T' then null else analytearea end)
                                  over (partition by analyteid, analyteorder, species, matrix, NAME, runid, temperature, hours, longtermtime, longtermunits)
                else &&PeakRepresentationFunc&(Avg(case when ISDEACTIVATED = 'T' then null else analytearea end)
                           over (partition by analyteid, analyteorder, species, matrix, NAME, runid, temperature, hours, longtermtime, longtermunits), &&PeakFigures&)
           end as mean_analytearea,
           case when '&&FinalAccuracy&' = 'T' then Stddev(case when ISDEACTIVATED = 'T' then null else analytearea end)
                                  over (partition by analyteid, analyteorder, species, matrix, NAME, runid, temperature, hours, longtermtime, longtermunits)
                else &&PeakRepresentationFunc&(Stddev(case when ISDEACTIVATED = 'T' then null else analytearea end)
                           over (partition by analyteid, analyteorder, species, matrix, NAME, runid, temperature, hours, longtermtime, longtermunits), &&PeakSDFigures&)
           end as sd_analytearea,
           Count(case when ISDEACTIVATED = 'T' then null else analytearea end)
             over (partition by analyteid, analyteorder, species, matrix, NAME, runid, temperature, hours, longtermtime, longtermunits) as n_analytearea,
           case when '&&FinalAccuracy&' = 'T' then Avg(case when ISDEACTIVATED = 'T' then null else internalstandardarea end)
                                  over (partition by analyteid, analyteorder, species, matrix, NAME, runid, temperature, hours, longtermtime, longtermunits)
                else round(Avg(case when ISDEACTIVATED = 'T' then null else internalstandardarea end)
                           over (partition by analyteid, analyteorder, species, matrix, NAME, runid, temperature, hours, longtermtime, longtermunits), &&PeakFigures&)
           end as mean_internalstandardarea,
           case when '&&FinalAccuracy&' = 'T' then Stddev(case when ISDEACTIVATED = 'T' then null else internalstandardarea end)
                                  over (partition by analyteid, analyteorder, species, matrix, NAME, runid, temperature, hours, longtermtime, longtermunits)
                else &&PeakRepresentationFunc&(Stddev(case when ISDEACTIVATED = 'T' then null else internalstandardarea end)
                           over (partition by analyteid, analyteorder, species, matrix, NAME, runid, temperature, hours, longtermtime, longtermunits), &&PeakSDFigures&)
           end as sd_internalstandardarea,
           Count(case when ISDEACTIVATED = 'T' then null else internalstandardarea end)
             over (partition by analyteid, analyteorder, species, matrix, NAME, runid, temperature, hours, longtermtime, longtermunits) as n_internalstandardarea,
           case when ISDEACTIVATED = 'T' then null else ratio end useratio,
           case when ISDEACTIVATED = 'T' then null else analytearea end useanalytearea,
           case when ISDEACTIVATED = 'T' then null else internalstandardarea end useinternalstandardarea
     FROM
     (SELECT K.ID knownID, s.ID sampleID, srw.ID sampleresultrawID, K.NAME,
        case
          when d.code is not null then 'T'
          else s.ISDEACTIVATED
        end ISDEACTIVATED,
        case
          when d.code is not null then 'T'
          else 'F'
        end isr_deactivation,
        d.reason deactivationreason,
        case when ra.RunAnalyteRegressionStatus = &&RejectedRunAnalyte& then 'F' else 'T' end acceptedrun,
        s.runid, s.runsamplesequencenumber, k.concentrationconv nominalconc,
        ra.analyteid, sa.analyteorder, a.species, a.sampletypeid matrix, r.runtypedescription, s.replicatenumber, k.flagpercent,
        k.stabilitytype, k.preparationdate,
        &&StabilityInfo&--k.cycles, k.hours, k.longtermtime, k.longtermunits, k.temperature,
        case when '&&FinalAccuracy&' = 'T' then srw.ratio/case when regexp_like(K.name,'Test') then &&PeakCorrectionFactor& else 1 end
             else &&PeakRatioRepresentationFunc&(srw.ratio/case when regexp_like(K.name,'Test') then &&PeakCorrectionFactor& else 1 end, &&PeakRatioFigures&)
        end ratio,
        case when '&&FinalAccuracy&' = 'T' then srw.analytearea/case when regexp_like(K.name,'Test') then &&PeakCorrectionFactor& else 1 end
             else &&PeakRepresentationFunc&(srw.analytearea/case when regexp_like(K.name,'Test') then &&PeakCorrectionFactor& else 1 end, &&PeakFigures&)
        end analytearea,
        case when '&&FinalAccuracy&' = 'T' then srw.internalstandardarea/case when regexp_like(K.name,'Test') then &&PeakCorrectionFactor& else 1 end
             else round(srw.internalstandardarea/case when regexp_like(K.name,'Test') then &&PeakCorrectionFactor& else 1 end, &&PeakFigures&)
        end internalstandardarea
      FROM &&TempTabPrefix&bio$run$ana$known K,
           &&TempTabPrefix&bio$run r,
           &&TempTabPrefix&bio$run$analytes ra,
           &&TempTabPrefix&bio$study$analytes sa,
           &&TempTabPrefix&bio$assay a,
           &&TempTabPrefix&bio$run$samples s,
           &&TempTabPrefix&bio$run$sample$result$raw srw,
           &&TempTabPrefix&bio$covib$runana$smp$deact d,
           isr$crit kwz,
           table(&&LALPackage&.GetRunStates) rs
      WHERE K.ID = s.runknownID
        AND R.ID = s.runid AND R.ID = srw.runid
        --AND r.studyassayid = s.studyassayid AND s.studyassayid = srw.studyassayid
        AND r.RuntypeDescription in (&&RunTypes&)
        AND s.ID = srw.runsampleID
        AND s.sampletype = 'known'
        and k.knowntype = s.runsamplekind
        and kwz.masterkey = '&&ExpMasterkey&'
        AND ra.id = srw.runanalyteid
        AND sa.id = ra.analyteid
        AND a.id = r.assayid
        AND s.studycode || '-' || r.runid || '-' || s.runsamplesequencenumber || '-' || sa.analyteorder = d.code (+)
        AND ra.runid = s.runid
        AND ra.studyID = '&&StudyID&'
        AND K.studyid = '&&StudyID&' AND s.studyid = '&&StudyID&'
        AND srw.studyID = '&&StudyID&' AND r.studyid = '&&StudyID&'
        and kwz.key = K.NAME
        and kwz.entity = '&&KnownEntity&'
        and repid = &&RepID&
        AND rs.column_value = r.runstatusnum
        &&AddCondition&
     ))
     select ref.acceptedrun, ref.analyteid, ref.analyteorder, ref.species, ref.matrix, ref.runid, ref.nominalconc,
      XMLELEMENT("calculation",
      XMLELEMENT("target",
        XMLELEMENT("acceptedrun", ref.acceptedrun),
        XMLELEMENT("analyteid", ref.analyteid),
        XMLELEMENT("species", ref.species),
        XMLELEMENT("matrix", ref.matrix),
        XMLELEMENT("name", ref.NAME),
        XMLELEMENT("test-name", test.NAME),
        XMLELEMENT("runid", ref.runid),
        XMLELEMENT("temperature", ref.temperature),
        XMLELEMENT("hours", ref.hours),
        XMLELEMENT("longtermtime", ref.longtermtime),
        XMLELEMENT("longtermunits", test.longtermunits)
        ),
      XMLELEMENT("values", Xmlagg(
        XMLELEMENT("sample", XMLATTRIBUTES(ref.sampleid AS "ref-sampleid", ref.sampleresultrawid AS "ref-sampleresultrawid", test.sampleid AS "test-sampleid", test.sampleresultrawid AS "test-sampleresultrawid" ),
          XMLELEMENT("replicatenumber", ref.replicatenumber),
          XMLELEMENT("ref-ratio", &&PeakRatioFormatFunc&(&&PeakRatioRepresentationFunc&(ref.ratio, &&PeakRatioFigures&), &&PeakRatioFigures&)),
          XMLELEMENT("ref-analytearea", &&PeakFormatFunc&(&&PeakRepresentationFunc&(ref.analytearea, &&PeakFigures&), &&PeakFigures&)),
          XMLELEMENT("test-ratio", &&PeakRatioFormatFunc&(&&PeakRatioRepresentationFunc&(test.ratio, &&PeakRatioFigures&), &&PeakRatioFigures&)),
          XMLELEMENT("test-analytearea", &&PeakFormatFunc&(&&PeakRepresentationFunc&(test.analytearea, &&PeakFigures&), &&PeakFigures&)),
          XMLELEMENT("bias-analytearea", FormatRounded(round(case when ref.analytearea = 0 then null
                                                    when '&&FinalAccuracy&' = 'T' then test.analytearea/ref.analytearea*100
                                                    else &&PeakRepresentationFunc&(test.analytearea, &&PeakFigures&)/&&PeakRepresentationFunc&(ref.analytearea, &&PeakFigures&)*100
                                               end, &&DecPlPercBias&), &&DecPlPercBias&)),
          XMLELEMENT("bias-ratio", FormatRounded(round(case when ref.ratio = 0 then null
                                              when '&&FinalAccuracy&' = 'T' then test.ratio/ref.ratio*100
                                              else &&PeakRatioRepresentationFunc&(test.ratio, &&PeakRatioFigures&)/&&PeakRatioRepresentationFunc&(ref.ratio, &&PeakRatioFigures&)*100
                                         end, &&DecPlPercBias&), &&DecPlPercBias&)),
          XMLELEMENT("recovery-analytearea", FormatRounded(round(test.analytearea/ref.mean_analytearea*100,&&DecPlPercBias&),&&DecPlPercBias&)),
          XMLELEMENT("recovery-internalstandardarea", FormatRounded(round(test.internalstandardarea/ref.mean_internalstandardarea*100,&&DecPlPercBias&),&&DecPlPercBias&),
          XMLELEMENT("deactivated", test.isdeactivated),
          case
            when test.isr_deactivation = 'T' then
              XMLELEMENT("deactivationreason", test.deactivationreason)
            else null
          end)
          ) order by ref.replicatenumber)),
      XMLELEMENT("result",
        XMLELEMENT("n", Count(ref.useanalytearea)),
        XMLELEMENT("mean-analytearea", &&PeakFormatFunc&(&&PeakRepresentationFunc&(min(ref.mean_analytearea), &&PeakFigures&), &&PeakFigures&)),
        XMLELEMENT("sd-analytearea", &&PeakFormatFunc&(&&PeakRepresentationFunc&(min(ref.sd_analytearea), &&PeakSDFigures&), &&PeakSDFigures&)),
        XMLELEMENT("cv-analytearea", FormatRounded(round(min(ref.sd_analytearea)/min(ref.mean_analytearea)*100, &&DecPlPercCV&), &&DecPlPercCV&)),
        XMLELEMENT("n-analytearea", min(ref.n_analytearea)),
        XMLELEMENT("mean-test-analytearea", &&PeakFormatFunc&(&&PeakRepresentationFunc&(min(test.mean_analytearea), &&PeakFigures&), &&PeakFigures&)),
        XMLELEMENT("sd-test-analytearea", &&PeakFormatFunc&(&&PeakRepresentationFunc&(min(test.sd_analytearea), &&PeakSDFigures&), &&PeakSDFigures&)),
        XMLELEMENT("cv-test-analytearea", FormatRounded(round(min(test.sd_analytearea)/min(test.mean_analytearea)*100, &&DecPlPercCV&), &&DecPlPercCV&)),
        XMLELEMENT("n-test-analytearea", min(test.n_analytearea)),
        XMLELEMENT("mean-recovery-analytearea", FormatRounded(round(Min(case when '&&FinalAccuracy&' = 'T' then test.mean_analytearea/ref.mean_analytearea*100
                                                                             else round(test.mean_analytearea/ref.mean_analytearea*100,&&DecPlPercBias&)
                                                                        end),&&DecPlPercBias&),&&DecPlPercBias&)),
        /*XMLELEMENT("mean-recovery-analytearea", FormatRounded(round(Avg(case when '&&FinalAccuracy&' = 'T' then test.analytearea/ref.mean_analytearea*100
                                                                             else round(test.analytearea/ref.mean_analytearea*100,&&DecPlPercBias&)
                                                                        end),&&DecPlPercBias&),&&DecPlPercBias&)),*/
        XMLELEMENT("sd-recovery-analytearea", FormatRounded(round(Stddev(case when '&&FinalAccuracy&' = 'T' then test.analytearea/ref.mean_analytearea*100
                                                                     else round(test.analytearea/ref.mean_analytearea*100,&&PeakSDFigures&)
                                                                end),&&PeakSDFigures&),&&PeakSDFigures&)),
        XMLELEMENT("cv-recovery-analytearea", FormatRounded(round(case when '&&FinalAccuracy&' = 'T' then case when Avg(test.analytearea/ref.mean_analytearea*100) = 0 then null
                                                                                              else Stddev(test.analytearea/ref.mean_analytearea*100)/
                                                                                                   Avg(test.analytearea/ref.mean_analytearea*100)*100
                                                                                         end
                                                                       else case when round(Avg(round(test.analytearea/ref.mean_analytearea*100,&&PeakFigures&)),&&PeakFigures&) = 0 then null
                                                                                 else round(Stddev(round(test.analytearea/ref.mean_analytearea*100,0)),&&PeakSDFigures&)
                                                                                     /round(Avg(round(test.analytearea/ref.mean_analytearea*100,&&PeakFigures&)),&&PeakFigures&)*100
                                                                            end
                                                                  end, &&DecPlPercCV&), &&DecPlPercCV&)),
        XMLELEMENT("mean-internalstandardarea", &&PeakFormatFunc&(&&PeakRepresentationFunc&(min(ref.mean_internalstandardarea), &&PeakFigures&), &&PeakFigures&)),
        XMLELEMENT("sd-internalstandardarea", &&PeakFormatFunc&(&&PeakRepresentationFunc&(min(ref.sd_internalstandardarea), &&PeakSDFigures&), &&PeakSDFigures&)),
        XMLELEMENT("cv-internalstandardarea", FormatRounded(round(min(ref.sd_internalstandardarea)/nullif(min(ref.mean_internalstandardarea),0)*100, &&DecPlPercCV&), &&DecPlPercCV&)),
        XMLELEMENT("n-internalstandardarea", min(ref.n_internalstandardarea)),
        XMLELEMENT("mean-test-internalstandardarea", &&PeakFormatFunc&(&&PeakRepresentationFunc&(min(test.mean_internalstandardarea), &&PeakFigures&), &&PeakFigures&)),
        XMLELEMENT("sd-test-internalstandardarea", &&PeakFormatFunc&(&&PeakRepresentationFunc&(min(test.sd_internalstandardarea), &&PeakSDFigures&), &&PeakSDFigures&)),
        XMLELEMENT("cv-test-internalstandardarea", FormatRounded(round(min(test.sd_internalstandardarea)/nullif(min(test.mean_internalstandardarea),0)*100, &&DecPlPercCV&), &&DecPlPercCV&)),
        XMLELEMENT("n-test-internalstandardarea", min(test.n_internalstandardarea)),
        XMLELEMENT("mean-recovery-internalstandardarea", FormatRounded(round(Min(case when '&&FinalAccuracy&' = 'T' then test.mean_internalstandardarea/ref.mean_internalstandardarea*100
                                                                             else round(test.mean_internalstandardarea/ref.mean_internalstandardarea*100, &&DecPlPercBias&)
                                                                        end),&&DecPlPercBias&),&&DecPlPercBias&)),
        /*XMLELEMENT("mean-recovery-internalstandardarea", FormatRounded(round(Avg(case when '&&FinalAccuracy&' = 'T' then test.internalstandardarea/ref.mean_internalstandardarea*100
                                                                             else round(test.internalstandardarea/ref.mean_internalstandardarea*100, &&DecPlPercBias&)
                                                                        end),&&DecPlPercBias&),&&DecPlPercBias&)),*/
        XMLELEMENT("sd-recovery-internalstandardarea", FormatRounded(round(Stddev(case when '&&FinalAccuracy&' = 'T' then test.internalstandardarea/ref.mean_internalstandardarea*100
                                                                     else round(test.internalstandardarea/ref.mean_internalstandardarea*100,&&PeakFigures&)
                                                                end),&&PeakSDFigures&),&&PeakSDFigures&)),
        XMLELEMENT("cv-recovery-internalstandardarea", FormatRounded(round(case when '&&FinalAccuracy&' = 'T' then case when Avg(test.internalstandardarea/ref.mean_internalstandardarea*100) = 0 then null
                                                                                              else Stddev(test.internalstandardarea/ref.mean_internalstandardarea*100)/
                                                                                                   Avg(test.internalstandardarea/ref.mean_internalstandardarea*100)*100
                                                                                         end
                                                                       else case when round(Avg(round(test.internalstandardarea/ref.mean_internalstandardarea*100,&&PeakFigures&)),&&PeakFigures&) = 0 then null
                                                                                 else round(Stddev(round(test.internalstandardarea/ref.mean_internalstandardarea*100,&&PeakFigures&)),&&PeakSDFigures&)
                                                                                     /round(Avg(round(test.internalstandardarea/ref.mean_internalstandardarea*100,&&PeakFigures&)),&&PeakFigures&)*100
                                                                            end
                                                                  end, &&DecPlPercCV&), &&DecPlPercCV&))
        ))   calc,
        Min(case when '&&FinalAccuracy&' = 'T' then test.mean_analytearea/ref.mean_analytearea*100
                       else round(test.mean_analytearea/ref.mean_analytearea*100,&&DecPlPercBias&)
                  end) as mean_recovery_analytearea,
        Min(case when '&&FinalAccuracy&' = 'T' then test.mean_internalstandardarea/ref.mean_internalstandardarea*100
                       else round(test.mean_internalstandardarea/ref.mean_internalstandardarea*100,&&PeakFigures&)
                  end) as mean_recovery_isarea,
        /*Avg(case when '&&FinalAccuracy&' = 'T' then test.analytearea/ref.mean_analytearea*100
                       else round(test.analytearea/ref.mean_analytearea*100,&&DecPlPercBias&)
                  end)  as mean_recovery_analytearea,
        Avg(case when '&&FinalAccuracy&' = 'T' then test.internalstandardarea/ref.mean_internalstandardarea*100
                       else round(test.internalstandardarea/ref.mean_internalstandardarea*100,&&PeakFigures&)
                  end)  as mean_recovery_isarea,*/
        case when '&&FinalAccuracy&' = 'T' then case when Avg(test.analytearea/ref.mean_analytearea*100) = 0 then null
                                                   else Stddev(test.analytearea/ref.mean_analytearea*100)/
                                                        Avg(test.analytearea/ref.mean_analytearea*100)*100
                                              end
             else case when round(Avg(round(test.analytearea/ref.mean_analytearea*100,&&PeakFigures&)),&&PeakFigures&) = 0 then null
                       else round(Stddev(round(test.analytearea/ref.mean_analytearea*100,&&PeakFigures&)),&&PeakSDFigures&)
                           /round(Avg(round(test.analytearea/ref.mean_analytearea*100,&&PeakFigures&)),&&PeakFigures&)*100
                  end
        end cv_recovery_analytearea,
        case when '&&FinalAccuracy&' = 'T' then case when Avg(test.internalstandardarea/ref.mean_internalstandardarea*100) = 0 then null
                                                   else Stddev(test.internalstandardarea/ref.mean_internalstandardarea*100)/
                                                        Avg(test.internalstandardarea/ref.mean_internalstandardarea*100)*100
                                end
             else case when round(Avg(round(test.internalstandardarea/ref.mean_internalstandardarea*100,&&PeakFigures&)),&&PeakFigures&) = 0 then null
                       else round(Stddev(round(test.internalstandardarea/ref.mean_internalstandardarea*100,&&PeakFigures&)),&&PeakSDFigures&)
                           /round(Avg(round(test.internalstandardarea/ref.mean_internalstandardarea*100,&&PeakFigures&)),&&PeakFigures&)*100
                  end
        end cv_recovery_isarea,
        Count(ref.useanalytearea) n
     from analyterun ref, analyterun test
     where ref.acceptedrun = test.acceptedrun
       and ref.analyteid = test.analyteid
       and ref.species = test.species
       and ref.matrix = test.matrix
       and ref.runid = test.runid
       and ref.replicatenumber = test.replicatenumber
       &&JoinCondition&
    GROUP BY ref.acceptedrun, ref.analyteid, ref.analyteorder, ref.species, ref.matrix, ref.NAME, test.name, ref.runid, ref.temperature, ref.hours, ref.longtermtime, ref.longtermunits, test.longtermunits, ref.nominalconc
  ) GROUP BY acceptedrun, analyteid, analyteorder, species, matrix, runid)
  ) FROM dual