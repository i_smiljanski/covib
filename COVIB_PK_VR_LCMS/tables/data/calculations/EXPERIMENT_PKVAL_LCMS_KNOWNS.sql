  SELECT XMLELEMENT("experiment", XMLATTRIBUTES('&&Experiment&' AS "name" ,'&&KnownsCalcType&' AS "type"),
           Xmlagg (XMLConcat(
             Xmlagg (XMLELEMENT("calculation", xmlconcat(targ,val,res)) order by &&OrderByAgg& &&DilColumnGroupBy& &&RunIDColumn&),
             XMLELEMENT("statistic", XMLATTRIBUTES(acceptedrun AS "acceptedrun", analyteid AS "analyteid", analyteorder as "analyteorder", species as "species", matrix as "matrix", temperature as "temperature", hours as "hours", longtermtime as "longtermtime", longtermunits as "longtermunits", cycles as "cycles", concentrationunits as "unit") &&RunIDInTarget&,
               XMLELEMENT("flagpercent", min(flagpercent)),
               XMLELEMENT("min-bias", FormatRounded(min(bias), &&DecPlPercBias&)),
               XMLELEMENT("max-bias", FormatRounded(max(bias), &&DecPlPercBias&)),
               XMLELEMENT("mean-bias", FormatRounded(avg(bias), &&DecPlPercBias&)),
               XMLELEMENT("min-nominalconc", &&ConcFormatFunc&(&&ConcRepresentationFunc&(min(min_nominalconc), &&ConcFigures&), &&ConcFigures&)),
               XMLELEMENT("mean-conc", &&ConcFormatFunc&(&&ConcRepresentationFunc&(min(mean_conc), &&ConcFigures&), &&ConcFigures&)),
               XMLELEMENT("sd-conc", &&ConcFormatFunc&(&&ConcRepresentationFunc&(min(sd_conc), &&ConcSDFigures&), &&ConcSDFigures&)),
               XMLELEMENT("cv-conc", FormatRounded(round(&&ConcRepresentationFunc&(min(sd_conc), &&ConcSDFigures&)/nullif(&&ConcRepresentationFunc&(min(mean_conc), &&ConcFigures&),0)*100, &&DecPlPercCV&), &&DecPlPercCV&)),
               XMLELEMENT("biasonmean-conc", FormatRounded(round((&&ConcRepresentationFunc&(min(mean_conc), &&ConcFigures&)-min(min_nominalconc))/nullif(min(min_nominalconc),0)*100, &&DecPlPercBias&), &&DecPlPercBias&)),
               XMLELEMENT("n-conc", FmtNum(min(n_conc))),
               XMLELEMENT("numacc", sum(cntunflagged)), &&InterLotCvStatistic&
               XMLELEMENT("numtotal", sum(cnt)),
               XMLELEMENT("percacc", case when count(unflagged_bias) != 0 then FmtNum(round(sum(unflagged_bias)/count(unflagged_bias)*100,0)) else null end),
               XMLELEMENT("hours", hours), XMLELEMENT("longtermunits", longtermunits), XMLELEMENT("longtermtime", longtermtime), XMLELEMENT("temperature", temperature),
               XMLELEMENT("lloq",
                 XMLELEMENT("name", min(lloqname)),
                 XMLELEMENT("nominalconc", &&ConcFormatFunc&(&&ConcRepresentationFunc&(min(lloq_nominalconc), &&ConcFigures&), &&ConcFigures&))
                 ),
               XMLELEMENT("uloq",
                 XMLELEMENT("name", min(uloqname)),
                 XMLELEMENT("nominalconc", &&ConcFormatFunc&(&&ConcRepresentationFunc&(min(uloq_nominalconc), &&ConcFigures&), &&ConcFigures&))
                 )
             )) order by &&OrderBy&))
  FROM
   (SELECT analyteorder, nominalconc, min(knownorder) knownorder, species, matrix, temperature, longtermtime, longtermunits, hours, cycles, concentrationunits,
      longtermtime * case when longtermunits='D' then 24 when longtermunits='W' then (7*24) when longtermunits='M' then (30*24) when longtermunits='Y' then (360*24) else 1 end longtermorder,
      Count(case when anchor = 'T' then null else useconc end) as cnt, count(case when anchor = 'T' then null else unflagged end) as cntunflagged,
      case when LENGTH(TRIM(TRANSLATE(name, ' +-.0123456789', ' '))) is null then to_number(name)
           else null end namenum,
      &&ConcRepresentationFunc&(Avg(case when anchor = 'T' then null
                                         when '&&FinalAccuracy&' = 'T' then useconc else useconc_sigrnd
                                    end), &&ConcFigures&) as mean,
      &&ConcRepresentationFunc&(Stddev(case when anchor = 'T' then null
                                       when '&&FinalAccuracy&' = 'T' then useconc else useconc_sigrnd
                                       end), &&ConcSDFigures&) as sd,
      round(case when min(anchor) = 'T' then null
                 when '&&FinalAccuracy&' = 'T' then Stddev(useconc)/nullif(Avg(useconc),0)*100
                 else &&ConcRepresentationFunc&(Stddev(useconc_sigrnd), &&ConcSDFigures&)/nullif(&&ConcRepresentationFunc&(Avg(useconc_sigrnd), &&ConcFigures&),0)*100
            end , &&DecPlPercCV&) as cv,
      round(Avg(case when anchor = 'T' then null when '&&FinalAccuracy&' = 'T' then usebias else usebias_sigrnd end), &&DecPlPercBias&) as bias,
      case when nominalconc = 0 then null
           else round(case when '&&FinalAccuracy&' = 'T' then (Avg(case when anchor = 'T' then null else useconc end)-nominalconc)/nullif(nominalconc,0)*100
                           else (&&ConcRepresentationFunc&(Avg(case when anchor = 'T' then null else useconc_sigrnd end), &&ConcFigures&)-nominalconc_sigrnd)/nullif(nominalconc_sigrnd,0)*100
                      end, &&DecPlPercBias&)
           end as biasonmean,
      case when min(anchor) = 'T' then null --anchor
           when abs(Avg(case when '&&FinalAccuracy&' = 'T' then usebias else usebias_sigrnd end)) > min(flagpercent) then 0
           else 1
      end as unflagged_bias,
      min(flagpercent) flagpercent, analyteid, name, acceptedrun&&DilColumnGroupBy& &&RunIDColumn&,&&AddGroupings&
      case when '&&FinalAccuracy&' = 'T' then min(lloq_nominalconc) else min(lloq_nominalconc_sigrnd) end lloq_nominalconc,
      case when '&&FinalAccuracy&' = 'T' then min(uloq_nominalconc) else min(uloq_nominalconc_sigrnd) end uloq_nominalconc,
      Min(nominalconc_sigrnd) over (partition by acceptedrun, analyteid, analyteorder, species, matrix, concentrationunits &&RunIDColumn&) as min_nominalconc,
      Avg(case when '&&FinalAccuracy&' = 'T' then Avg(useconc)
                     else &&ConcRepresentationFunc&(Avg(useconc_sigrnd), &&ConcFigures&)
      end) over (partition by acceptedrun, analyteid, analyteorder, species, matrix, concentrationunits &&RunIDColumn&) as mean_conc,
      Stddev(case when '&&FinalAccuracy&' = 'T' then Avg(useconc)
                     else &&ConcRepresentationFunc&(Avg(useconc_sigrnd), &&ConcFigures&)
      end) over (partition by acceptedrun, analyteid, analyteorder, species, matrix, concentrationunits &&RunIDColumn&) as sd_conc,
      Count(case when '&&FinalAccuracy&' = 'T' then Avg(useconc)
                     else &&ConcRepresentationFunc&(Avg(useconc_sigrnd), &&ConcFigures&)
      end) over (partition by acceptedrun, analyteid, analyteorder, species, matrix, concentrationunits &&RunIDColumn&) as n_conc,
      min(lloqconv) lloqconv, min(uloqconv) uloqconv,
      min(lloqname) lloqname, min(uloqname) uloqname, min(anchor) anchor,
      XMLELEMENT("target",
        XMLELEMENT("acceptedrun", acceptedrun), XMLELEMENT("analyteid", analyteid), XMLELEMENT("species", species), XMLELEMENT("matrix", matrix),
        XMLELEMENT("name", NAME), XMLELEMENT("nominalconc", &&ConcFormatFunc&(nominalconc_sigrnd, &&ConcFigures&)),
        XMLELEMENT("hours", hours), XMLELEMENT("longtermunits", longtermunits), XMLELEMENT("longtermtime", longtermtime), XMLELEMENT("temperature", temperature),
        XMLELEMENT("cycles", cycles), /*XMLELEMENT("source", source), */XMLELEMENT("conc", FmtNum(nominalconc)),
        XMLELEMENT("unit", concentrationunits)&&AddGroupingsTarget&
        &&DilTraget& &&LevelTarget& &&RunIDInTarget&) as targ,
      XMLELEMENT("values", Xmlagg(
        XMLELEMENT("sample", XMLATTRIBUTES(sampleid AS "sampleid", sampleresultid AS "sampleresultid" ), &&RunIDInValues&
          XMLELEMENT("runsamplesequencenumber", runsamplesequencenumber),
          XMLELEMENT("conctext", conctext),
          XMLELEMENT("concentration", case when conctext = 'LLOQ' or (nominalconc_sigrnd < lloqconv and conc is null and analytearea is not null) then '< LLOQ' when conctext = 'ULOQ' or (nominalconc_sigrnd > uloqconv and conc is null and analytearea is not null) then '> ULOQ' else &&ConcFormatFunc&(conc_sigrnd,&&ConcFigures&) end ),
          --XMLELEMENT("concentration", &&ConcFormatFunc&(conc_sigrnd, &&ConcFigures&)),
          XMLELEMENT("concval", &&ConcFormatFunc&(conc_sigrnd,&&ConcFigures&)),
          XMLELEMENT("nominalconc", &&ConcFormatFunc&(nominalconc_sigrnd, &&ConcFigures&)),
          XMLELEMENT("subset", subset),
          XMLELEMENT("bias", FormatRounded(bias_sigrnd, &&DecPlPercBias&)),
          XMLELEMENT("base_mean", &&ConcFormatFunc&(base_mean,&&ConcFigures&)),
          XMLELEMENT("difference", FormatRounded(round(case when base_mean = 0 or cycles = '1' then null
                                                    when '&&FinalAccuracy&' = 'T' then (conc-base_mean)/nullif(base_mean,0)*100
                                                    else (&&ConcRepresentationFunc&(conc_sigrnd, &&ConcFigures&)-base_mean)/nullif(base_mean,0)*100
                                               end, &&DecPlPercBias&), &&DecPlPercBias&)),
          XMLELEMENT("accuracy", FormatRounded(accuracy_sigrnd,&&DecPlPercBias&)),
          XMLELEMENT("resultcommenttext", resultcommenttext),
          XMLELEMENT("analytearea", &&ConcFormatFunc&(&&ConcRepresentationFunc&(analytearea,&&ConcFigures&),&&ConcFigures&)),
          XMLELEMENT("flag", flagged),
          XMLELEMENT("cvflag", cvflagged),
          XMLELEMENT("flagpercent", flagpercent),
          XMLELEMENT("cvflagpercent", cvflagpercent),
          XMLELEMENT("lloq", lloqconv),
          XMLELEMENT("uloq", uloqconv),
          XMLELEMENT("deactivated", isdeactivated),
          case
            when isr_deactivation = 'T' then
              XMLELEMENT("deactivationreason", deactivationreason)
            else null
          end,
          XMLELEMENT("run-conc", &&ConcFormatFunc&(&&ConcRepresentationFunc&(run_useconc_sigrnd,&&ConcFigures&),&&ConcFigures&))
          &&DilValue&)
          order by runnr, runsamplesequencenumber
          )) as val,
      XMLELEMENT("result",
        XMLELEMENT("n", Count(useconc)),
        XMLELEMENT("mean", &&ConcFormatFunc&(&&ConcRepresentationFunc&(Avg(
                           case when '&&FinalAccuracy&' = 'T' then useconc else useconc_sigrnd
                           end), &&ConcFigures&), &&ConcFigures&)),
        XMLELEMENT("sd", case when Count(useconc) <= 2 then null else &&ConcFormatFunc&(&&ConcRepresentationFunc&(Stddev(
                        case when '&&FinalAccuracy&' = 'T' then useconc
                        else useconc_sigrnd
                        end), &&ConcSDFigures&), &&ConcSDFigures&) end),
        XMLELEMENT("cv", case when Count(useconc) <= 2 then null when '&&FinalAccuracy&' = 'T' then FormatRounded(round(Stddev(useconc)/nullif(Avg(useconc),0)*100, &&DecPlPercCV&), &&DecPlPercCV&)
                         else FormatRounded(round(&&ConcRepresentationFunc&(Stddev(useconc_sigrnd), &&ConcFigures&)/nullif(&&ConcRepresentationFunc&(Avg(useconc_sigrnd), &&ConcFigures&),0)*100, &&DecPlPercCV&), &&DecPlPercCV&)
                         end), &&InterLotCvResult&
        XMLELEMENT("bias", FormatRounded(round(Avg(case when '&&FinalAccuracy&' = 'T' then usebias else usebias_sigrnd end), &&DecPlPercBias&), &&DecPlPercBias&)),
        XMLELEMENT("biasonmean", FormatRounded(round(case when nominalconc = 0 then null
                                                    when '&&FinalAccuracy&' = 'T' then (Avg(useconc)-nominalconc)/nullif(nominalconc,0)*100
                                                    else (&&ConcRepresentationFunc&(Avg(useconc_sigrnd), &&ConcFigures&)-nominalconc_sigrnd)/nullif(nominalconc_sigrnd,0)*100
                                               end, &&DecPlPercBias&), &&DecPlPercBias&)),
        XMLELEMENT("difference", FormatRounded(round(case when min(base_mean) = 0 or cycles = '1' then null
                                                    when '&&FinalAccuracy&' = 'T' then (Avg(useconc)-min(base_mean))/nullif(min(base_mean),0)*100
                                                    else (&&ConcRepresentationFunc&(Avg(useconc_sigrnd), &&ConcFigures&)-min(base_mean))/nullif(min(base_mean),0)*100
                                               end, &&DecPlPercBias&), &&DecPlPercBias&)),
        XMLELEMENT("accuracy", FormatRounded(round(case when nominalconc = 0 then null
                                                    when '&&FinalAccuracy&' = 'T' then ((Avg(useconc)-nominalconc)/nullif(nominalconc,0)*100)+100
                                                    else ((&&ConcRepresentationFunc&(Avg(useconc_sigrnd), &&ConcFigures&)-nominalconc_sigrnd)/nullif(nominalconc_sigrnd,0)*100)+100
                                               end, &&DecPlPercBias&), &&DecPlPercBias&)),
        XMLELEMENT("totalerror", FormatRounded(round(case when nominalconc = 0 or Count(useconc) <= 2 then null
                                                          when '&&FinalAccuracy&' = 'T' then Abs((Avg(useconc)-nominalconc)/nullif(nominalconc,0)*100) +
                                                                                             FormatRounded(round(Stddev(useconc)/nullif(Avg(useconc),0)*100, &&DecPlPercCV&), &&DecPlPercCV&)
                                                          else Abs((&&ConcRepresentationFunc&(Avg(useconc_sigrnd), &&ConcFigures&)-nominalconc_sigrnd)/nullif(nominalconc_sigrnd,0)*100) +
                                                               FormatRounded(round(&&ConcRepresentationFunc&(Stddev(useconc_sigrnd), &&ConcFigures&)/nullif(&&ConcRepresentationFunc&(Avg(useconc_sigrnd), &&ConcFigures&),0)*100, &&DecPlPercCV&), &&DecPlPercCV&)
                                               end, &&DecPlPercTE&), &&DecPlPercTE&)),
        XMLELEMENT("wo-n", Count(wo_useconc)),
        XMLELEMENT("wo-mean", &&ConcFormatFunc&(&&ConcRepresentationFunc&(Avg(
                             case when '&&FinalAccuracy&' = 'T' then wo_useconc else wo_useconc_sigrnd
                             end), &&ConcFigures&), &&ConcFigures&)),
        XMLELEMENT("wo-sd", case when Count(wo_useconc) <= 2 then null else &&ConcFormatFunc&(&&ConcRepresentationFunc&(Stddev(
                           case when '&&FinalAccuracy&' = 'T' then wo_useconc
                                else wo_useconc_sigrnd
                           end), &&ConcSDFigures&), &&ConcSDFigures&) end),
        XMLELEMENT("wo-cv", case when Count(wo_useconc) <= 2 then null when '&&FinalAccuracy&' = 'T' then FormatRounded(round(Stddev(wo_useconc)/nullif(Avg(wo_useconc),0)*100, &&DecPlPercCV&), &&DecPlPercCV&)
                              else FormatRounded(round(&&ConcRepresentationFunc&(Stddev(wo_useconc_sigrnd), &&ConcFigures&)/nullif(&&ConcRepresentationFunc&(Avg(wo_useconc_sigrnd), &&ConcFigures&),0)*100, &&DecPlPercCV&), &&DecPlPercCV&)
                         end),
        XMLELEMENT("wo-biasonmean", FormatRounded(round(case when nominalconc = 0 then null
                                                    when '&&FinalAccuracy&' = 'T' then (Avg(wo_useconc)-nominalconc)/nullif(nominalconc,0)*100
                                                    else (&&ConcRepresentationFunc&(Avg(wo_useconc_sigrnd), &&ConcFigures&)-nominalconc_sigrnd)/nullif(nominalconc_sigrnd,0)*100
                                               end, &&DecPlPercBias&), &&DecPlPercBias&)),
        XMLELEMENT("wo-totalerror", FormatRounded(round(case when nominalconc = 0 or Count(wo_useconc) <= 2 then null
                                                          when '&&FinalAccuracy&' = 'T' then Abs((Avg(wo_useconc)-nominalconc)/nullif(nominalconc,0)*100) +
                                                                                             FormatRounded(round(Stddev(wo_useconc)/nullif(Avg(wo_useconc),0)*100, &&DecPlPercCV&), &&DecPlPercCV&)
                                                          else Abs((&&ConcRepresentationFunc&(Avg(wo_useconc_sigrnd), &&ConcFigures&)-nominalconc_sigrnd)/nullif(nominalconc_sigrnd,0)*100) +
                                                               FormatRounded(round(&&ConcRepresentationFunc&(Stddev(wo_useconc_sigrnd), &&ConcFigures&)/nullif(&&ConcRepresentationFunc&(Avg(wo_useconc_sigrnd), &&ConcFigures&),0)*100, &&DecPlPercCV&), &&DecPlPercCV&)
                                               end, &&DecPlPercTE&), &&DecPlPercTE&)),
        XMLELEMENT("base_mean", &&ConcFormatFunc&(&&ConcRepresentationFunc&(min(base_mean), &&ConcFigures&), &&ConcFigures&)),
        XMLELEMENT("meanofreps", &&ConcFormatFunc&(&&ConcRepresentationFunc&(Avg(useconc), &&ConcFigures&), &&ConcFigures&)),
        XMLELEMENT("biasofreps", FormatRounded(round(case when nominalconc = 0 then null
                                                    else (Avg(useconc)-nominalconc)/nullif(nominalconc,0)*100
                                               end, &&DecPlPercBias&), &&DecPlPercBias&)),
        XMLELEMENT("sdofreps", &&ConcFormatFunc&(&&ConcRepresentationFunc&(Stddev(useconc), &&ConcSDFigures&), &&ConcSDFigures&)),
        XMLELEMENT("cvofreps", FormatRounded(round(Stddev(useconc)/nullif(Avg(useconc),0)*100, &&DecPlPercCV&), &&DecPlPercCV&)),
        XMLELEMENT("flagged",
          case when abs(round(case when nominalconc = 0 then null
               when '&&FinalAccuracy&' = 'T' then (Avg(useconc)-nominalconc)/nullif(nominalconc,0)*100
               else (&&ConcRepresentationFunc&(Avg(useconc_sigrnd), &&ConcFigures&)-nominalconc_sigrnd)/nullif(nominalconc_sigrnd,0)*100
          end, &&DecPlPercBias&)) > min(flagpercent) then 'T' else 'F' end),
        XMLELEMENT("cvflagged",
          case when abs(case when '&&FinalAccuracy&' = 'T' then Stddev(useconc)/NULLIF(Avg(useconc),0)*100
               else round(&&ConcRepresentationFunc&(Stddev(useconc_sigrnd), &&ConcFigures&)/NULLIF(&&ConcRepresentationFunc&(Avg(useconc_sigrnd), &&ConcFigures&),0)*100, &&DecPlPercCV&)
          end) > min(cvflagpercent) then 'T' else 'F' end),
        XMLELEMENT("flagpercent", min(flagpercent)),
        XMLELEMENT("cvflagpercent", min(cvflagpercent)),
        XMLELEMENT("analytearea-n", Count(useanalytearea)),
        XMLELEMENT("analytearea-mean", &&ConcFormatFunc&(&&ConcRepresentationFunc&(Avg(
                           case when '&&FinalAccuracy&' = 'T' then useanalytearea else useanalytearea_sigrnd
                           end), &&ConcFigures&), &&ConcFigures&)),
        XMLELEMENT("analytearea-sd", &&ConcFormatFunc&(&&ConcRepresentationFunc&(Stddev(
                        case when '&&FinalAccuracy&' = 'T' then useanalytearea else useanalytearea_sigrnd
                        end), &&ConcSDFigures&), &&ConcSDFigures&)),
        XMLELEMENT("analytearea-cv", case when '&&FinalAccuracy&' = 'T' then FormatRounded(round(Stddev(useanalytearea)/nullif(Avg(useanalytearea),0)*100, &&DecPlPercCV&), &&DecPlPercCV&)
                         else FormatRounded(round(&&ConcRepresentationFunc&(Stddev(useanalytearea_sigrnd), &&ConcFigures&)/nullif(&&ConcRepresentationFunc&(Avg(useanalytearea_sigrnd), &&ConcFigures&),0)*100, &&DecPlPercCV&), &&DecPlPercCV&)
                         end),
        case when min(knowntype) = 'STANDARD' then XMLCONCAT(XMLELEMENT("anchor",min(anchor)),XMLELEMENT("at_lloq",min(at_lloq)),XMLELEMENT("at_uloq",min(at_uloq))) else null end
        ) as res
    FROM
    (SELECT knownID,
     sampleID, sampleresultID, name, subset &&LevelCol&, &&InterLotCvCalc&
     nominalconc, nominalconc_sigrnd, analyteid &&DilColumn&,
     lloqconv, uloqconv, knowntype, resultcommenttext, &&AddGroupings&
     min(case when round(nominalconc,15) = round(lloqconv,15) then name else null end) over (partition by acceptedrun, analyteid, analyteorder, species, matrix) as lloqname,
     min(case when round(nominalconc,15) = round(uloqconv,15) then name else null end) over (partition by acceptedrun, analyteid, analyteorder, species, matrix) as uloqname,
     hours, longtermtime, longtermunits, temperature, cycles, /*source, */concentrationunits,
     case when knowntype = 'STANDARD' and (nominalconc_sigrnd<&&ConcRepresentationFunc&(lloqconv, &&ConcFigures&) or nominalconc_sigrnd>&&ConcRepresentationFunc&(uloqconv, &&ConcFigures&)) then 'T' else 'F' end anchor,
     case when knowntype = 'STANDARD' and (nominalconc_sigrnd=&&ConcRepresentationFunc&(lloqconv, &&ConcFigures&)) then 'T' else 'F' end at_lloq,
     case when knowntype = 'STANDARD' and (nominalconc_sigrnd=&&ConcRepresentationFunc&(uloqconv, &&ConcFigures&)) then 'T' else 'F' end at_uloq,
     /* Added, potentially wrong: */
     MIN(nominalconc_sigrnd) over (partition by acceptedrun, analyteid, analyteorder, species, matrix) lloq_nominalconc_sigrnd,
     MAX(nominalconc_sigrnd) over (partition by acceptedrun, analyteid, analyteorder, species, matrix) uloq_nominalconc_sigrnd,
     MIN(nominalconc) over (partition by acceptedrun, analyteid, analyteorder, species, matrix) lloq_nominalconc,
     MAX(nominalconc) over (partition by acceptedrun, analyteid, analyteorder, species, matrix) uloq_nominalconc,
     analyteorder, knownorder, species, matrix, conc, conctext, conc_sigrnd, analytearea, analytearea_sigrnd,
     bias, bias_sigrnd, flagpercent, cvflagpercent, accuracy_sigrnd, ISDEACTIVATED, isr_deactivation, deactivationreason,
     acceptedrun, runid, runnr, runsamplesequencenumber,
     Avg(case when ISDEACTIVATED = 'T' then null else conc_sigrnd end) over (partition by acceptedrun, analyteid, analyteorder, species, matrix, runid, concentrationunits) run_useconc_sigrnd,
     case when ISDEACTIVATED = 'T' then null else conc_sigrnd end useconc_sigrnd, case when ISDEACTIVATED = 'T' then null else conc end useconc,
     case when ISDEACTIVATED = 'T' then null else bias_sigrnd end usebias_sigrnd, case when ISDEACTIVATED = 'T' then null else bias end usebias,
     case when isr_deactivation = 'D' then null else conc_sigrnd end wo_useconc_sigrnd, case when isr_deactivation = 'D' then null else conc end wo_useconc,
     case when ISDEACTIVATED = 'T' then null else analytearea_sigrnd end useanalytearea_sigrnd, case when ISDEACTIVATED = 'T' then null else analytearea end useanalytearea,
     case when abs(case when '&&FinalAccuracy&' = 'T' then bias else bias_sigrnd end) > flagpercent then '&&FlaggedSymbol&' else null end flagged,
     case when '&&CVFlagOnResultComment&' is not null and regexp_like(resultcommenttext,'&&CVFlagOnResultComment&', 'i') then '&&FlaggedSymbolCV&' else null end cvflagged,
     case when ISDEACTIVATED <> 'T' and abs(case when '&&FinalAccuracy&' = 'T' then bias else bias_sigrnd end) <= flagpercent then 1 else null end unflagged,
     Min(
       case when ISDEACTIVATED = 'T' or cycles != '1' then null
            else case when '&&FinalAccuracy&' = 'T' then conc else conc_sigrnd end end
      )
      over (partition by analyteid, analyteorder, species, matrix, name, nominalconc, nominalconc_sigrnd, temperature, hours, longtermtime, longtermunits, concentrationunits, acceptedrun &&DilColumnGroupBy&, runid&&LevelGroupBy&) base_conc,
     Avg(
       case when ISDEACTIVATED = 'T' or cycles != '1' then null
            else case when '&&FinalAccuracy&' = 'T' then conc else conc_sigrnd end end
      )
      over (partition by analyteid, analyteorder, species, matrix, name, nominalconc, nominalconc_sigrnd, temperature, hours, longtermtime, longtermunits, concentrationunits, acceptedrun &&DilColumnGroupBy&, runid&&LevelGroupBy&) base_mean,
     Count(conc)
      over (partition by analyteid, analyteorder, species, matrix, name, nominalconc, nominalconc_sigrnd, temperature, hours, longtermtime, longtermunits, cycles, concentrationunits, acceptedrun &&DilColumnGroupBy&, runid&&LevelGroupBy&) nByRun
     FROM
     (SELECT K.ID knownID,
        s.ID sampleID, sr.ID sampleresultID,
        &&KnownNameTargetField& name, &&AddGroupingsSelect& &&LevelSelect&
        k.namesuffix subset,
        case when ra.RunAnalyteRegressionStatus = &&RejectedRunAnalyte& then 'F' else 'T' end acceptedrun,
        case
          when d.code is not null then 'T'
          else s.ISDEACTIVATED
        end ISDEACTIVATED,
        case
          when d.code is not null then 'T'
          when s.ISDEACTIVATED = 'T' then 'D'
          else 'F'
        end isr_deactivation,
        d.reason deactivationreason, &&DilColumnQualif&
        s.runsamplesequencenumber, ra.analyteid, sa.analyteorder, a.species, a.sampletypeid matrix, kwz.ordernumber knownorder,
        sr.nominalconcentrationconv nominalconc, &&ConcRepresentationFunc&(sr.nominalconcentrationconv, &&ConcFigures&) nominalconc_sigrnd,
        sr.resultcommenttext, ra.lloqconv, ra.uloqconv, K.knowntype,-- k.source,
        case when sr.nominalconcentrationconv = ra.lloqconv then &&FlagPercentBiasLLOQ&
             when sr.nominalconcentrationconv = ra.uloqconv then &&FlagPercentBiasULOQ&
             when sr.nominalconcentrationconv not between ra.lloqconv and ra.uloqconv and '&&ExcludeAnchorsFromFlags&' = 'T' then null
             else &&FlagPercentBias&
        end flagpercent,
        case when sr.nominalconcentrationconv = ra.lloqconv then &&FlagPercentCvLLOQ&
             when sr.nominalconcentrationconv = ra.uloqconv then &&FlagPercentCvULOQ&
             when sr.nominalconcentrationconv not between ra.lloqconv and ra.uloqconv and '&&ExcludeAnchorsFromFlags&' = 'T' then null
             else &&FlagPercentCV&
        end cvflagpercent,
        s.runid, r.runid runnr,
        &&ConcRepresentationFunc&(sr.concentrationconv, &&ConcFigures&) conc_sigrnd,
        sr.concentrationconv conc, &&StabilityInfo&
        srw.analytearea, srw.analytearea_sigrnd analytearea_sigrnd,
        case when '&&ExcludeLLOQULOQFromStats&'='F' then null -- disable the LLOQ/LLOQ-feature, treat as numbers
             when sr.concentrationconv/nullif(s.dilutionfactor,0) < ra.lloqconv then 'LLOQ'
             when sr.concentrationconv/nullif(s.dilutionfactor,0) > ra.uloqconv then 'ULOQ'
             else null
        end conctext,
        ra.concentrationunits,
        case when sr.nominalconcentrationconv = 0 then null
             when (sr.concentrationconv/nullif(s.dilutionfactor,0) < ra.lloqconv and '&&ExcludeLLOQULOQFromStats&'='T') then null
             when (sr.concentrationconv/nullif(s.dilutionfactor,0) > ra.uloqconv and '&&ExcludeLLOQULOQFromStats&'='T') then null
             else round((&&ConcRepresentationFunc&(sr.concentrationconv, &&ConcFigures&)-&&ConcRepresentationFunc&(sr.nominalconcentrationconv, &&ConcFigures&))*100/
                         nullif(&&ConcRepresentationFunc&(sr.nominalconcentrationconv, &&ConcFigures&),0), &&DecPlPercBias&)
        end bias_sigrnd,
        case when sr.nominalconcentrationconv = 0 then null
             when (sr.concentrationconv/nullif(s.dilutionfactor,0) < ra.lloqconv and '&&ExcludeLLOQULOQFromStats&'='T') then null
             when (sr.concentrationconv/nullif(s.dilutionfactor,0) > ra.uloqconv and '&&ExcludeLLOQULOQFromStats&'='T') then null
             else round(&&ConcRepresentationFunc&(sr.concentrationconv,&&ConcFigures&)*100/
                        nullif(&&ConcRepresentationFunc&(sr.nominalconcentrationconv,&&ConcFigures&),0),&&DecPlPercBias&)
        end accuracy_sigrnd,
        case when sr.nominalconcentrationconv = 0 then null
             when (sr.concentrationconv/nullif(s.dilutionfactor,0) < ra.lloqconv and '&&ExcludeLLOQULOQFromStats&'='T') then null
             when (sr.concentrationconv/nullif(s.dilutionfactor,0) > ra.uloqconv and '&&ExcludeLLOQULOQFromStats&'='T') then null
             else (sr.concentrationconv-sr.nominalconcentrationconv)*100/
                   nullif(sr.nominalconcentrationconv,0)
        end bias
      FROM &&TempTabPrefix&bio$run$ana$known K,
           &&TempTabPrefix&bio$run r,
           &&TempTabPrefix&bio$run$analytes ra,
           &&TempTabPrefix&bio$study$analytes sa,
           &&TempTabPrefix&bio$assay a,
           &&TempTabPrefix&bio$run$samples s,
           &&TempTabPrefix&bio$run$sample$results sr,
           --&&TempTabPrefix&bio$run$sample$result$raw srw,
           (select runknownid, Avg(analytearea) analytearea, &&ConcRepresentationFunc&(Avg(analytearea), &&ConcFigures&) analytearea_sigrnd from &&TempTabPrefix&bio$run$sample$result$raw, &&TempTabPrefix&bio$run$samples s where runsampleid = s.id group by runknownid) srw,
           &&TempTabPrefix&bio$covib$runana$smp$deact d,
           isr$crit kwz,
           table(&&LALPackage&.GetRunStates) rs
      WHERE K.ID = s.runknownID
        AND R.ID = s.runid AND R.ID = sr.runid
        AND r.RuntypeDescription in (&&RunTypes&)
        AND s.ID = sr.runsampleID
        AND K.knowntype = '&&KnownType&'
        AND s.sampletype = 'known'
        AND s.runsamplekind = '&&KnownType&'
        AND K.knowntype = s.runsamplekind
        AND ra.id = sr.runanalyteid
        AND a.id = r.assayid
        AND sa.id = ra.analyteid
        AND ra.runid = s.runid
        AND K.studyid = '&&StudyID&' AND s.studyid = '&&StudyID&'
        AND sr.studyID = '&&StudyID&' AND r.studyid = '&&StudyID&'
        AND ra.studyID = '&&StudyID&'
        AND s.studycode || '-' || r.runid || '-' || s.runsamplesequencenumber || '-' || sa.analyteorder = d.code (+)
        --AND s.ID = srw.runSampleID (+)
        AND s.runknownID = srw.runknownID (+)
        AND rs.column_value = r.runstatusnum
        AND kwz.key = &&KnownNameJoinField&
        AND kwz.entity = '&&KnownEntity&'
        AND kwz.masterkey = '&&ExpMasterkey&'
        AND repid = &&RepID&
        &&DilCond& &&AddCondition&
     ))
    &&GroupingConditions&
    GROUP BY analyteid, analyteorder, species, matrix, name, nominalconc, nominalconc_sigrnd, temperature, hours, longtermtime, longtermunits, cycles, /*source, */concentrationunits, acceptedrun &&AddGroupingsGroupBy& &&DilColumnGroupBy& &&RunIDColumn& &&LevelGroupBy&)
    GROUP BY analyteid, analyteorder, species, matrix, name, namenum, knownorder, nominalconc, temperature, hours, longtermorder, longtermtime, longtermunits, cycles, concentrationunits, acceptedrun &&RunIDColumn&