CREATE OR REPLACE FUNCTION FORMATSIG (cnVal IN number, cnFig IN integer) RETURN  varchar2 is
begin
  return iSR$Format.FormatSig(cnVal, cnFig);
end FormatSig;