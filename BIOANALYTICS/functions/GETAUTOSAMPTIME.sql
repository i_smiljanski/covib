CREATE OR REPLACE FUNCTION GETAUTOSAMPTIME (sRunCode in VARCHAR2, nStudycode IN NUMBER) return number
is
  nReturn NUMBER;
begin
  execute immediate 'select '||STB$UTIL.getCurrentLal||'.getAutoSampTime(:1, :2) from dual' 
  into nReturn
  using in sRunCode, in nStudycode;
  return nReturn;   
end getAutoSampTime;