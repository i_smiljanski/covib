CREATE OR REPLACE FUNCTION FMTPARAMVAL ( csValue in number, csParamNAME in varchar2, cnFig IN integer) return  varchar2
is
  sReturn VARCHAR2(4000);
begin
  execute immediate 'select '||STB$UTIL.getCurrentLal||'.FmtParamVal(:1, :2, :3) from dual' 
  into sReturn
  using in csValue, in csParamNAME, in cnFig;
  return sReturn;   
end FmtParamVal;