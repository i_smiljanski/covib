CREATE OR REPLACE FUNCTION FORMATROUNDED (cnVal IN number, nDec in number) RETURN  varchar2 is
begin
  return iSR$Format.FormatRounded(cnVal, nDec);
end FormatRounded;