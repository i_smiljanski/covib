CREATE OR REPLACE FUNCTION FMTNUM ( Value number) return  varchar2 is
begin
  return iSR$Format.FmtNum(Value); 
end FmtNum;