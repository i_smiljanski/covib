CREATE OR REPLACE FUNCTION SIGFIGURE ( Value number, nFigures integer) return number is
begin
   return iSR$Format.sigFigure(Value, nFigures); 
end sigFigure;