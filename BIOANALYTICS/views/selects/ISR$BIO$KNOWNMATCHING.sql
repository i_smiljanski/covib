CREATE OR REPLACE VIEW ISR$BIO$KNOWNMATCHING (CODE, KNOWN, DISPLAY1, DISPLAY2, ORDERNUMBER) AS SELECT known_crit.key
, known_crit.key
, known_crit.display
, known_crit.display
, MIN (known_crit.ordernumber)
FROM (SELECT *
FROM isr$crit
WHERE repid = stb$object.getcurrentrepid
AND entity in ('BIO$KNOWN1','BIO$KNOWN')
AND info = 'KNOWN') known_crit
GROUP BY known_crit.key, known_crit.display
ORDER BY 5