CREATE OR REPLACE VIEW ISR$BIO$EXPERIMENT (ENTITY, CODE, NAME, KNOWNTYPE, ORDERNUMBER, SELECT_KNOWNS) AS SELECT 'BIO$EXPERIMENT'
, REPLACE (parametername, 'EXPERIMENT_')
, utd$msglib.getMsg (description, stb$security.getCurrentLanguage)
, parametervalue
, orderno
, visible
FROM STB$REPTYPEPARAMETER pv
WHERE parametername LIKE 'EXPERIMENT_%'
AND visible = 'T'
AND reporttypeid = STB$OBJECT.getCurrentReporttypeID