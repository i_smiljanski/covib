CREATE OR REPLACE VIEW ISR$BIO$GRAPHIC (ENTITY, CODE, NAME, INFO) AS select 'BIO$GRAPHIC' ENTITY,
PV.Value            CODE,
PV.DISPLAY          Name,
PV.INFO             Info
FROM ISR$PARAMETER$VALUES pv
WHERE pv.entity='BIO$GRAPHIC'
ORDER BY PV.ORDERNUMBER