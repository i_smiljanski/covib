CREATE OR REPLACE VIEW ISR$BIO$INCLUDED$RUN (CODE) AS (select s.studycode || '-' || r.runid Code
  from (SELECT key studycode FROM isr$crit
         WHERE repid = stb$object.getcurrentrepid
           and Entity = 'BIO$STUDY') s,
       (SELECT parametervalue runid from STB$REPORTPARAMETER
         WHERE parametername = 'INCLUDED_RUN'
           and repid = STB$OBJECT.GetCurrentRepId) r
)