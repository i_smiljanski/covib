CREATE OR REPLACE VIEW ISR$BIO$ANA$KNOWN (CODE, STUDYCODE, ANALYTECODE, NAME, NAMEPREFIX, NAMESUFFIX, DESCRIPTION, KNOWNTYPE, MINCONCENTRATION, STABILITYTYPE, DILUTIONFACTOR) AS select distinct 
  knowncode code, 
  studycode, 
  analytecode, 
  name, 
  nameprefix, 
  namesuffix,
  description,
  knowntype,
  minconcentration, 
  stabilitytype, 
  dilutionfactor
from TMP$BIO$RUN$ANA$KNOWN