CREATE OR REPLACE VIEW ISR$BIO$PROTECTIONFLAG (ENTITY, CODE, NAME, ORDERNUMBER) AS select 'BIO$PROTECTIONFLAG' ENTITY,
         ot.bookmarkname      code,
         m.msgtext            name,
         sh.stylesheetid      ordernumber
    from isr$output$transform ot,
         isr$stylesheet       sh,
         stb$reptypeparameter rtp,
         utd$msg              m
   where     ot.stylesheetid = sh.stylesheetid
         and ot.outputid = (select f.outputid
                              from isr$output$file f, isr$action$output ao
                             where ao.outputid = f.outputid
                               and f.fileid = stb$object.GetCurrentTemplate
                               and ao.actionid = 2
                               and rownum < 2)
         and rtp.reporttypeid = stb$object.GetCurrentReportTypeId
         and rtp.parametername = 'EXPERIMENT_' || ot.bookmarkname(+)
         and rtp.description = m.msgkey(+)
         and sh.structured = 'S'
         and m.lang (+) = stb$security.getcurrentlanguage