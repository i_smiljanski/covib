CREATE OR REPLACE TYPE ISR$PSEUDOSD as object
(
  lVals sys.OdciNumberList,
  static function ODCIAggregateInitialize(sctx IN OUT ISR$PseudoSD) 
    return number,
  member function ODCIAggregateIterate(self IN OUT ISR$PseudoSD, 
    value IN number) return number,
  member function ODCIAggregateTerminate(self IN ISR$PseudoSD, 
    returnValue OUT number, flags IN number) return number,
  member function ODCIAggregateMerge(self IN OUT ISR$PseudoSD, 
    ctx2 IN ISR$PseudoSD) return number
)