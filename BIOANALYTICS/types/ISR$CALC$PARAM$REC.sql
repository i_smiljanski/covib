CREATE OR REPLACE TYPE ISR$CALC$PARAM$REC as object (sParameter varchar2(2000),
                                   sSetting   varchar2(2000),
                                   sValue     varchar2(4000),
                                   sIsEval    varchar2(1)
                                   )
not final