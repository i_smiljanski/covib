CREATE OR REPLACE TYPE ISR$ANARUNANA$REC as object
(RegStatus varchar2(50),
 nm number,
 vec number,
 reason varchar2(100))