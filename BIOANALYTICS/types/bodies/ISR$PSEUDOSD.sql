CREATE OR REPLACE TYPE BODY ISR$PSEUDOSD is 
static function ODCIAggregateInitialize(sctx IN OUT ISR$PseudoSD) 
return number is 
begin
  sctx := ISR$PseudoSD(sys.OdciNumberList());
  return ODCIConst.Success;
end;

member function ODCIAggregateIterate(self IN OUT ISR$PseudoSD, value IN number) return number is
begin
  lVals.extend;
  lVals(lVals.last) := value;
  return ODCIConst.Success;
end;

member function ODCIAggregateTerminate(self IN ISR$PseudoSD, 
    returnValue OUT number, flags IN number) return number is
  nRep      number := 0;
  n         number := 0;
  nHarm     number := 0;
  nIndharm  number := 0;
  nIharm    number := 0;
  nIndDif   number := 0;
begin
  n := self.lVals.count;
  if n > 1 then    
    for nVal in 1 .. n loop
      if self.lVals(nVal) = 0 then 
        returnValue := null;
        return ODCIConst.Success;
      else
        nRep := nRep + 1/self.lVals(nVal);
      end if;
    end loop;
    dbms_output.put_line('n:'||n||' nRep:'||nRep);

    for nVal in 1 .. n loop
      dbms_output.put_line('alles:'||(nRep - 1/self.lVals(nVal)));
      nIndharm := (N - 1) / (nRep - 1/self.lVals(nVal));
      nIharm := nIharm + nIndharm;
    end loop;  
    nHarm := nIharm / N;

    for nVal in 1 .. n loop
      nIndharm := (N - 1) / (nRep - 1 / self.lVals(nVal));
      nIndDif := nIndDif + power((nIndharm - nHarm), 2);
    end loop;
    returnValue := Sqrt((N - 1) * nIndDif);     
  else
    returnValue := null;
  end if;
  return ODCIConst.Success;
end;

member function ODCIAggregateMerge(self IN OUT ISR$PseudoSD, ctx2 IN ISR$PseudoSD) return number is  
begin
  select column_value BULK COLLECT INTO self.lVals FROM (
   (select column_value from table(self.lVals))
   union all
   (select column_value from table(ctx2.lVals)));
  return ODCIConst.Success;
end;
end;