CREATE OR REPLACE PACKAGE BODY ISR$REPORTCALCULATIONLIB
AS

--******************************************************************************
FUNCTION GetAdditionalCondition(csCalcStatmentAlias in varchar2, csSubCondition in varchar2 default null) return varchar2 is
  sCurrentName    constant varchar2(100)     := $$PLSQL_UNIT||'.GetAdditionalCondition';
  sCondExpression varchar2(20000);
  nRepTypeID      number;
  cursor cGetOrder is
    select display, info, ordernumber from (
      select fileid, display, info, ordernumber,
        max(fileid) over (partition by reporttypeid, display) maxfile
        FROM ISR$FILE$CALC$VALUES
       WHERE ((Entity = 'EXP$CALC$COND' and csSubCondition is null) or Entity = 'EXP$CALC$COND$'||csSubCondition)
         AND VALUE = csCalcStatmentAlias
         AND reporttypeid = nRepTypeID
         AND fileid in (0, ISR$REPORTCALCULATION.cnFileId)
         AND Use = 'T')
    where fileid = maxfile
    ORDER BY OrderNumber;
  rGetOrder cGetOrder%rowtype;
BEGIN
  isr$trace.stat('begin', 'begin '||csCalcStatmentAlias||', '||csSubCondition, sCurrentName);
  nRepTypeID := STB$OBJECT.getCurrentReporttypeId;
  open cGetOrder;
  fetch cGetOrder into rGetOrder;
  while cGetOrder%found loop
    sCondExpression := sCondExpression||chr(10)||'      AND ('||rGetOrder.Info||')';
    fetch cGetOrder into rGetOrder;
  end loop;
  close cGetOrder;
  isr$trace.stat('end','end '||sCondExpression, sCurrentName);
  return sCondExpression;
END GetAdditionalCondition;

--******************************************************************************
FUNCTION GetCalcValuesOrder(csCalcStatmentAlias in varchar2, csValues in varchar2) return varchar2 is
  sCurrentName    constant varchar2(100)     := $$PLSQL_UNIT||'.GetCalcValuesOrder';
  sOrderExpression varchar2(500);
  nRepTypeID       number;
  cursor cGetOrder(sEntity in varchar2) is
    select display, info, ordernumber from (
      select fileid, display, info, ordernumber,
        max(fileid) over (partition by reporttypeid, display) maxfile
        FROM ISR$FILE$CALC$VALUES
       WHERE Entity = sEntity
         AND VALUE = csCalcStatmentAlias
         AND reporttypeid = nRepTypeID
         AND fileid in (0, ISR$REPORTCALCULATION.cnFileId)
         AND Use = 'T')
    where fileid = maxfile
    ORDER BY OrderNumber;
  rGetOrder cGetOrder%rowtype;
BEGIN
  isr$trace.stat('begin', 'begin '||csCalcStatmentAlias, sCurrentName);
  nRepTypeID := STB$OBJECT.getCurrentReporttypeId;
  if csValues = 'T' then
    open cGetOrder('EXP$CALC$ORDER$VAL');
  else
    open cGetOrder('EXP$CALC$ORDER$TARG');
  end if;
  fetch cGetOrder into rGetOrder;
  while cGetOrder%found loop
    if sOrderExpression is null then
      sOrderExpression := rGetOrder.Display||' '||rGetOrder.Info;
    else
      sOrderExpression := sOrderExpression||', '||rGetOrder.Display||' '||rGetOrder.Info;
    end if;
    fetch cGetOrder into rGetOrder;
  end loop;
  close cGetOrder;
  if sOrderExpression is not null then
    sOrderExpression := 'ORDER BY '||sOrderExpression;
  end if;
  isr$trace.stat('end','end '||sOrderExpression, sCurrentName);
  return sOrderExpression;
END GetCalcValuesOrder;

--******************************************************************************
FUNCTION FindUsedParams(csText in varchar2) RETURN sys.ODCIVarchar2List is
  sCurrentName    constant varchar2(100)     := $$PLSQL_UNIT||'.FindUsedParams';
  lVarList sys.ODCIVarchar2List := sys.ODCIVarchar2List();
  lDistVarList sys.ODCIVarchar2List := sys.ODCIVarchar2List();
  sTextRest varchar2(32000) := csText;
  sFoundVar varchar2(500);
  nFoundPos integer;
BEGIN
  isr$trace.stat('begin', 'begin', sCurrentName);
  sFoundVar := trim(both '&' from REGEXP_SUBSTR(sTextRest,'&&[^&]+&'));
  nFoundPos := REGEXP_INSTR(sTextRest,'&&[^&]+&');
  while nFoundPos > 0 loop
    lVarList.extend;
    lVarList(lVarList.last) := sFoundVar;
    sTextRest := substr(sTextRest,nFoundPos+3+length(nFoundPos));
    sFoundVar := trim(both '&' from REGEXP_SUBSTR(sTextRest,'&&[^&]+&'));
    nFoundPos := REGEXP_INSTR(sTextRest,'&&[^&]+&');
  end loop;
  select distinct column_value
  bulk collect into lDistVarList
  from table(lVarList);

  isr$trace.stat('end','end', sCurrentName);
  RETURN lDistVarList;
END FindUsedParams;

--******************************************************************************
Function GetRepTypeExperimentEntity( cnReportTypeID in integer ) return varchar2 is
  sCurrentName    constant varchar2(100)     := $$PLSQL_UNIT||'.GetRepTypeExperimentEntity';
  cursor cGetEntity is
    select value from (
      select fileid, value,
        max(fileid) over (partition by reporttypeid, value) maxfile
        FROM ISR$FILE$CALC$VALUES
       WHERE reporttypeid = cnReportTypeID
         AND fileid in (0, ISR$REPORTCALCULATION.cnFileId)
         AND Entity = 'EXP$LIST$ENTITY'
         AND Use = 'T')
    where fileid = maxfile;
  rGetEntity cGetEntity%rowtype;
begin
  isr$trace.stat('begin', 'begin', sCurrentName);
  open cGetEntity;
  fetch cGetEntity into rGetEntity;
  close cGetEntity;
  isr$trace.stat('end','end', sCurrentName);
  return rGetEntity.value;
end GetRepTypeExperimentEntity;

--******************************************************************************
Function GetExpCalcStatements( cnReportTypeID in integer, csExperiment in varchar2 ) return tlExpStatementsList is
  sCurrentName    constant varchar2(100)     := $$PLSQL_UNIT||'.GetExpCalcStatements';
  cursor cGetExpCalcStatm is
    select statementname, statement, CalcStatmentAlias from (
      select fileid, cs.statementname, cs.statement, rpv.display CalcStatmentAlias, rpv.ordernumber,--display, info, rpv.ordernumber,
        max(fileid) over (partition by reporttypeid, cs.statementname, rpv.display) maxfile
        FROM ISR$FILE$CALC$VALUES rpv, ISR$CALCULATION$STATEMENTS cs
       WHERE rpv.info = cs.statementname
         AND reporttypeid = cnReportTypeID
         AND fileid in (0, ISR$REPORTCALCULATION.cnFileId)
         AND Entity = 'EXP$CALC'
         AND VALUE = csExperiment
         AND use = 'T')
    where fileid = maxfile
    ORDER BY ordernumber;
  rGetExpCalcStatm cGetExpCalcStatm%rowtype;
  lExpStatements tlExpStatementsList;
  nIx binary_integer := 0;
begin
  isr$trace.stat('begin', 'begin', sCurrentName);
  open cGetExpCalcStatm;
  fetch cGetExpCalcStatm into rGetExpCalcStatm;
  while cGetExpCalcStatm%found loop
    nIx := nIx + 1;
    lExpStatements(nIx) := rGetExpCalcStatm;
    fetch cGetExpCalcStatm into rGetExpCalcStatm;
  end loop;
  close cGetExpCalcStatm;
  isr$trace.stat('end','end', sCurrentName);
  return lExpStatements;
end GetExpCalcStatements;

--******************************************************************************
FUNCTION GetReportRunTypes(nRepID IN number)  RETURN varchar2 IS
  sCurrentName    constant varchar2(100)     := $$PLSQL_UNIT||'.GetReportRunTypes';
  oErrorObj STB$OERROR$RECORD := STB$OERROR$RECORD();

  /*CURSOR cGetParameter(csName IN varchar2) IS
    SELECT parametervalue
    FROM stb$reportparameter
    WHERE repid= nRepID
      AND PARAMETERNAME = csName;*/
  sParamValue stb$reportparameter.ParameterValue%TYPE;
  sReportRunTypes STB$REPTYPEPARAMETER.ParameterValue%TYPE;
  sRunTypes varchar2(1000);
BEGIN
  isr$trace.stat('begin', 'begin', sCurrentName);
  oErrorObj := STB$UTIL.getRepTypeParameter(STB$OBJECT.getCurrentReporttypeId, 'RUNTYPE', sReportRunTypes);
  --sReportRunTypes := trim(REPLACE(sReportRunTypes,' ',NULL));
  /*OPEN   cGetParameter('ADDITIONAL_RUNTYPE');
  FETCH  cGetParameter INTO sParamValue;
  CLOSE  cGetParameter;*/
  --sParamValue := trim(REPLACE(sParamValue,' ',NULL));
  sParamValue:= STB$UTIL.GetReportParameterValue('ADDITIONAL_RUNTYPE', nRepID);
  IF sReportRunTypes IS NULL THEN
    sRunTypes := sParamValue;
  ELSIF sParamValue IS NULL THEN
    sRunTypes := sReportRunTypes;
  ELSE
    sRunTypes := sReportRunTypes||','||sParamValue;
  END IF;
  sRunTypes := ''''||REPLACE(sRunTypes,',',''',''')||'''';
  isr$trace.stat('end','end sRunTypes:'||sRunTypes, sCurrentName);
  RETURN sRunTypes;
END GetReportRunTypes;

--******************************************************************************
FUNCTION BuildCalibrationParamPiece(csParamName in varchar2,  csFormatFunc in varchar2, csReprFunc in varchar2, cnRepresentationParam IN number) RETURN varchar2 IS
  sCurrentName    constant varchar2(100)     := $$PLSQL_UNIT||'.BuildCalibrationParamPiece';
  cursor cGetStatmPiece is
    select cs.statementname, cs.statement
    from ISR$CALCULATION$STATEMENTS cs
    where statementname = 'EXPERIMENT_REGPARAMVAL_PIECE';
  rGetStatmPiece cGetStatmPiece%rowtype;
  sPiece varchar2(32000);
begin
  isr$trace.stat('begin', 'begin', sCurrentName);
  open cGetStatmPiece;
  fetch cGetStatmPiece into rGetStatmPiece;
  close cGetStatmPiece;
  isr$trace.debug('fetched piece', rGetStatmPiece.statement, sCurrentName);
  sPiece := replace(rGetStatmPiece.statement,'&&ParamName&', csParamName);
  sPiece := replace(sPiece,'&&OwnRepresentationParam&', cnRepresentationParam);
  sPiece := replace(sPiece,'&&OwnFormatFunc&', csFormatFunc);
  sPiece := replace(sPiece,'&&OwnRepresentationFunc&', csReprFunc);
  isr$trace.stat('end','end replaced piece='||sPiece, sCurrentName);
  return sPiece;
end BuildCalibrationParamPiece;

--******************************************************************************
FUNCTION BuildCalibrationParamPiece(csParamName in varchar2,  csFormatFunc in varchar2, csReprFunc in varchar2, cnRepresentationParam IN number, csPieceStatement IN varchar2) RETURN varchar2 IS
  sCurrentName    constant varchar2(100)     := $$PLSQL_UNIT||'.BuildCalibrationParamPiece';
  cursor cGetStatmPiece is
    select cs.statementname, cs.statement
    from ISR$CALCULATION$STATEMENTS cs
    where statementname = csPieceStatement;
  rGetStatmPiece cGetStatmPiece%rowtype;
  sPiece varchar2(32000);
begin
  isr$trace.stat('begin', 'begin', sCurrentName);
  open cGetStatmPiece;
  fetch cGetStatmPiece into rGetStatmPiece;
  close cGetStatmPiece;
  isr$trace.debug('fetched piece', rGetStatmPiece.statement, sCurrentName);
  sPiece := replace(rGetStatmPiece.statement,'&&ParamName&', csParamName);
  sPiece := replace(sPiece,'&&OwnRepresentationParam&', cnRepresentationParam);
  sPiece := replace(sPiece,'&&OwnFormatFunc&', csFormatFunc);
  sPiece := replace(sPiece,'&&OwnRepresentationFunc&', csReprFunc);
  isr$trace.stat('end','end replaced piece='||sPiece, sCurrentName);
  return sPiece;
end BuildCalibrationParamPiece;

--******************************************************************************
FUNCTION BuildCalibrationParamPiece(csStudyID IN varchar2, csParamName in varchar2, csRunTypes in varchar2,
                                    cnDecPlOwn IN number, cnDecPlSD IN number, cnPrecPercCV in number)
                                    RETURN varchar2 IS
BEGIN
  return '(SELECT Xmlagg (XMLELEMENT("calculation",
      XMLELEMENT("target",
        XMLELEMENT("assayanalyteid", assayanalyteid),
        XMLELEMENT("regparam", '''||csParamName||''')
        ),
      XMLELEMENT("values", Xmlagg(
        XMLELEMENT("run",  XMLATTRIBUTES(runid AS "runid" ),
          XMLELEMENT("'||csParamName||'", FormatRounded(round('||csParamName||','||cnDecPlOwn||'),'||cnDecPlOwn||')))
        )),
      XMLELEMENT("result",
        XMLELEMENT("n", Count('||csParamName||')),
        XMLELEMENT("mean", FormatRounded(round(avg('||csParamName||'),'||cnDecPlOwn||'),'||cnDecPlOwn||')),
        XMLELEMENT("sd", FormatRounded(round(Stddev('||csParamName||'),'||cnDecPlSD||'),'||cnDecPlSD||')),
        XMLELEMENT("cv", case when round(Avg('||csParamName||'),'||cnDecPlOwn||') = 0 then null else FormatRounded(round(round(Stddev('||csParamName||'),'||cnDecPlSD||')/round(Avg('||csParamName||'),'||cnDecPlOwn||')*100,'||cnPrecPercCV||'),'||cnPrecPercCV||') end )
      )))
    FROM
    (SELECT ra.runid, ra.assayanalyteid, '||csParamName||'
     FROM tmp$bio$RUN$ANALYTES ra,
          tmp$bio$run r,
          table('||STB$UTIL.getCurrentLal||'.GetRunStates) rs
     WHERE ra.runid=r.ID
       AND r.studyid = '''||csStudyID||''' AND r.studyid = '''||csStudyID||'''
       AND r.RuntypeDescription in  ('||csRunTypes||')
       AND rs.column_value=r.runstatusnum )
    GROUP BY assayanalyteid, '''||csParamName||''')';
 END BuildCalibrationParamPiece;

--******************************************************************************
FUNCTION BuildStatementCalibrationParam(csStudyID IN varchar2, cnDecPlRegParam IN number, cnRsquPrec in number, cnDecPlSD IN number, cnPrecPercCV in number, nRepID IN number)
                                        RETURN varchar2 IS
  sCurrentName    constant varchar2(100)     := $$PLSQL_UNIT||'.BuildStatementCalibrationParam';
  sStatement varchar2(32000);
  sRunTypes varchar2(1000);
  sStmnPiece varchar2(32000);
BEGIN
  isr$trace.stat('begin', 'begin', sCurrentName);
  sRunTypes := GetReportRunTypes(nRepID);
  sStmnPiece := BuildCalibrationParamPiece(csStudyID, 'regparamval1', sRunTypes, cnDecPlRegParam, cnDecPlSD, cnPrecPercCV);
  sStatement :=
  'SELECT XMLELEMENT("regparamvalues",
  '||sStmnPiece||',
  ';
  sStmnPiece := BuildCalibrationParamPiece(csStudyID, 'regparamval2', sRunTypes, cnDecPlRegParam, cnDecPlSD, cnPrecPercCV);
  sStatement := sStatement||sStmnPiece||',
  ';
  sStmnPiece := BuildCalibrationParamPiece(csStudyID, 'regparamval3', sRunTypes, cnDecPlRegParam, cnDecPlSD, cnPrecPercCV);
  sStatement := sStatement||sStmnPiece||',
  ';
  sStmnPiece := BuildCalibrationParamPiece(csStudyID, 'regparamval4', sRunTypes, cnDecPlRegParam, cnDecPlSD, cnPrecPercCV);
  sStatement := sStatement||sStmnPiece||',
  ';
  sStmnPiece := BuildCalibrationParamPiece(csStudyID, 'regparamval5', sRunTypes, cnDecPlRegParam, cnDecPlSD, cnPrecPercCV);
  sStatement := sStatement||sStmnPiece||',
  ';
  sStmnPiece := BuildCalibrationParamPiece(csStudyID, 'rsquared', sRunTypes, cnRsquPrec, cnDecPlSD, cnPrecPercCV);
  sStatement := sStatement||sStmnPiece||'
    ) FROM dual'   ;
  isr$trace.stat('end','end', sCurrentName);
  return sStatement;
end BuildStatementCalibrationParam;

end ISR$REPORTCALCULATIONLIB;