CREATE OR REPLACE PACKAGE BODY ISR$REPORTCALCULATION is

  csTempTabPrefix CONSTANT varchar2(50) := STB$UTIL.GetSystemParameter('TEMP_TAB_PREFIX');
  csTablePrefix   CONSTANT VARCHAR2(10) := 'ISR$'; -- system variable
  cnCurrentJobId           NUMBER;

--******************************************************************************
function CreateCalculationNode(csCalcStatement in varchar2, csStudyID in varchar2, csExperiment IN varchar2, cxXml in out XMLTYPE) return  STB$oError$Record is
  sCurrentName    constant varchar2(100)     := $$PLSQL_UNIT||'.CreateCalculationNode';
  sMsg            varchar2(4000);
  oErrorObj       STB$OERROR$RECORD := STB$OERROR$RECORD();

  nSeverity       number;

  TYPE tDynCursor IS ref CURSOR;
  cDynCursor tDynCursor;

  xXML            XMLTYPE;

begin
  isr$trace.stat('begin', 'begin', sCurrentName);  

  OPEN cDynCursor FOR csCalcStatement;  
  FETCH cDynCursor INTO xXML;    
  CLOSE cDynCursor;     
  isr$trace.debug('XML for '||csExperiment , substr(csCalcStatement,1,100),sCurrentName, xXML);
  -- add the  fragment to calculations for the current study
  cxXml := ISR$XML.InsertChildXMLFragment(cxXml,'/study/bioanalytic/studies/study[@id='''||csStudyID||''']/calculations', xXML);

  isr$trace.stat('end','end', sCurrentName);
  return oErrorObj;
exception 
  when others then
    isr$trace.warn('error statement in experiment '||csExperiment , 'see error statement in column LOGCLOB', sCurrentName, csCalcStatement);
    if nvl(STB$JOB.getJobParameter('ALERTERROR',cnCurrentJobId), 'F') = 'F' THEN
      STB$JOB.logError('error in statement '||csExperiment, SQLERRM, Stb$typedef.cnSeverityCritical);
      nSeverity := Stb$typedef.cnNoError;
    else
      nSeverity := Stb$typedef.cnSeverityCritical;
    end if;

    sMsg :=  utd$msglib.getmsg ('exUnexpectedError', stb$security.getcurrentlanguage, csP1 => sqlerrm(sqlcode));
    oErrorObj.handleError ( nSeverity, sMsg, sCurrentName, 
                            dbms_utility.format_error_stack ||stb$typedef.crlf|| dbms_utility.format_error_backtrace  );  
    return oErrorObj;       
end CreateCalculationNode;  

--******************************************************************************
function ReplaceCalcParamValues( csText in varchar2, lExpVars in sys.ODCIVarchar2List, lCalc$Param$list in iSR$Calc$Param$list) return varchar2 is
  sCurrentName    constant varchar2(100)     := $$PLSQL_UNIT||'.ReplaceCalcParamValues';

  sRetText varchar2(32000) := csText;
  cursor cVal(csPar in varchar2) is
    select sValue
    from table(lCalc$Param$list)
    where sParameter = csPar
      and sIsEval = 'T';
  sVal varchar2(32000);
begin
  isr$trace.stat('begin - '||lExpVars.count||' variables', substrb(csText,1,4000), sCurrentName);  

  if lExpVars.count > 0 then
      for i in 1 .. lExpVars.count loop
        open cVal(lExpVars(i));
        fetch cVal into sVal;
        if cVal%found then
          sRetText := replace(sRetText, '&&'||lExpVars(i)||'&', sVal);
          isr$trace.debug('sRetText for i='||i||' varname='||lExpVars(i)||' sVal='||sVal, substrb(sRetText,1,4000), sCurrentName,
                           case when length(sRetText)>4000 then sRetText else null end);
        end if; 
        close cVal; 
      end loop;
  end if;
  isr$trace.debug('sRetText is...', substrb(sRetText,1,4000) ,sCurrentName, case when length(sRetText)>4000 then sRetText else null end);
  isr$trace.stat('end','end', sCurrentName);
  return sRetText;
end ReplaceCalcParamValues;

--******************************************************************************
procedure  FillParamListValues(lCalc$Param$list in out iSR$Calc$Param$list) is
  sCurrentName    constant varchar2(100)     := $$PLSQL_UNIT||'.FillParamListValues';
  sMsg            varchar2(4000);

  nCnt integer;
  nRep integer := 0;
  lExpVars sys.ODCIVarchar2List := sys.ODCIVarchar2List();
  sSetExp varchar2(4000);
  sVal varchar2(4000);
begin
  isr$trace.stat('begin', 'begin', sCurrentName); 

  while nRep < 5 loop
    nRep := nRep + 1;
    select count(1) into nCnt
    from table(lCalc$Param$list)
    where sIsEval = 'F';
    isr$trace.debug('nCnt/nRep', 'nRep:'||nRep||', nCnt:'||nCnt, sCurrentName);
    exit when nCnt = 0;
    for i in 1 .. lCalc$Param$list.count loop
      if lCalc$Param$list(i).sIsEval = 'F' then
        isr$trace.debug('lCalc$Param$list('||i||').sSetting', lCalc$Param$list(i).sSetting, sCurrentName);
        isr$trace.debug('lCalc$Param$list('||i||').sParameter', lCalc$Param$list(i).sParameter, sCurrentName);
        lExpVars := ISR$REPORTCALCULATIONLib.FindUsedParams(lCalc$Param$list(i).sSetting);
        if lExpVars.count > 0 then
          -- setting containes vars. Try to replace them
          sSetExp := ReplaceCalcParamValues(lCalc$Param$list(i).sSetting, lExpVars, lCalc$Param$list);
        else
          sSetExp := lCalc$Param$list(i).sSetting;
        end if;
        if REGEXP_INSTR(sSetExp,'&&[^&]+&') = 0 then         
          -- try to evaluate
          begin
            isr$trace.debug('evaluation statement', 'select ('||sSetExp||') from dual', sCurrentName);

            execute immediate 'select ('||sSetExp||') from dual' into sVal;
            lCalc$Param$list(i).sValue := sVal;
            lCalc$Param$list(i).sIsEval := 'T'; 
          exception          
            when others then 
            isr$trace.debug('not evaluated', SQLERRM, sCurrentName); 
            lCalc$Param$list(i).sValue := sSetExp;
            lCalc$Param$list(i).sIsEval := 'T'; 
          end;
          isr$trace.debug('sValue', lCalc$Param$list(i).sValue, sCurrentName);
        else          
          -- not all vars used are known yet
          -- do nothing!
          null;           
        end if;       
        isr$trace.debug('sParameter/sValue/sIsEval', lCalc$Param$list(i).sParameter||'/'||lCalc$Param$list(i).sValue||'/'||lCalc$Param$list(i).sIsEval, sCurrentName);   
      end if;
    end loop;
  end loop;
  isr$trace.stat('end','end', sCurrentName);

end FillParamListValues;

--******************************************************************************
procedure FillCalcParamList (sContext in varchar2, lParamList in out iSR$Calc$Param$list) is
-- fills param list with found vars
  sCurrentName    constant varchar2(100)     := $$PLSQL_UNIT||'.FillCalcParamList';
  sMsg            varchar2(4000);

  nI binary_integer;
  sReplEntList varchar2(500);
  type trExpCalcVars is record(
    parameter ISR$FILE$CALC$VALUES.display%type,
    value     ISR$FILE$CALC$VALUES.info%type);
  type tDynCur is ref cursor;
  cExpCalcVars tDynCur;  
  sDynSel varchar2(4000);
  rExpCalcVars trExpCalcVars;
  cursor cReplaceEntities is
    select value entity
    from ISR$PARAMETER$VALUES
    where entity = 'EXP$CALC$REPL$ENT'
    order by ordernumber;
  sEntity ISR$PARAMETER$VALUES.value%type;
begin
  isr$trace.stat('begin', 'begin', sCurrentName);
  open cReplaceEntities;
  fetch cReplaceEntities into sEntity;
  while cReplaceEntities%found loop
    if sReplEntList is null then
      sReplEntList := ''''||sEntity||'''';
    else
      sReplEntList := sReplEntList||','''||sEntity||'''';
    end if;  
    fetch cReplaceEntities into sEntity;
  end loop;
  close cReplaceEntities;
  sDynSel :=
    'select parameter, value from (
      select reporttypeid, fileid, display parameter, info value, ordernumber,
        max(fileid) over (partition by reporttypeid, display) maxfile
      from ISR$FILE$CALC$VALUES
      where reporttypeid = '||STB$OBJECT.GetCurrentReportTypeId||'
        and fileid in (0, '||ISR$REPORTCALCULATION.cnFileId||')
        and entity in ('||sReplEntList||')
        and value = '''||sContext||'''
        and Use = ''T'' )
    where fileid = maxfile     
    order by ordernumber';
  isr$trace.debug('sDynSel', sDynSel, sCurrentName);    
  open cExpCalcVars for sDynSel;
  fetch cExpCalcVars into rExpCalcVars;
  while cExpCalcVars%found loop
    --look for the variable in the list
    nI := lParamList.first;
    while nI is not null  loop
      if lParamList(nI).sParameter = rExpCalcVars.parameter then
        -- variable already exists. Replace it if setting is not null
        if trim(rExpCalcVars.value) is not null then
          isr$trace.debug('replace parameter '||rExpCalcVars.parameter, rExpCalcVars.value, sCurrentName);
          lParamList(nI) := iSR$Calc$Param$Rec(rExpCalcVars.parameter,rExpCalcVars.value, null,'F');
        end if;
        exit;  -- leave the loop if parameter was found
      end if;
      nI := lParamList.next(nI);
    end loop;
    if nI is null then
      -- variable does not exist yet. add it
      lParamList.Extend;
      nI := lParamList.last;
      lParamList(nI) := iSR$Calc$Param$Rec(rExpCalcVars.parameter,rExpCalcVars.value, null,'F');
    end if;
    fetch cExpCalcVars into rExpCalcVars;  
  end loop;
  close cExpCalcVars;
  isr$trace.stat('end','end', sCurrentName);
end FillCalcParamList;

--******************************************************************************
function AddStudyCalculations(nJobId IN number, cxXml in out xmltype, oParamList in iSR$AttrList$Rec) return STB$oError$Record is
  sCurrentName    constant varchar2(100)     := $$PLSQL_UNIT||'.AddStudyCalculations';
  sMsg            varchar2(4000);

  oErrorObj           STB$OERROR$RECORD := STB$OERROR$RECORD();
    -- cursor for studies
  type tDynCur is ref cursor;
  cStudies tDynCur;  
  cRepExperiments tDynCur;

  sDynSel varchar2(4000);
  sStudyID VARCHAR2(4000);

  sExpEntity ISR$PARAMETER$VALUES.ENTITY%type;
  sExperiment ISR$PARAMETER$VALUES.VALUE%type;

  nRepID number := STB$OBJECT.GetCurrentRepId;
  nRepTypeID number := STB$OBJECT.GetCurrentReportTypeId;

  sCalculationStatement varchar2(32000);

  lExpStatements ISR$REPORTCALCULATIONLib.tlExpStatementsList;
  lExpEmpty ISR$REPORTCALCULATIONLib.tlExpStatementsList; -- do not assign any value to this. Used for initialisation only

  xXML           XMLTYPE;
  lExpVars sys.ODCIVarchar2List := sys.ODCIVarchar2List();
  lRepParamList iSR$Calc$Param$list := iSR$Calc$Param$list();
  lStudyParamList iSR$Calc$Param$list := iSR$Calc$Param$list();
  lExpParamList iSR$Calc$Param$list := iSR$Calc$Param$list();

begin
  isr$trace.stat('begin', 'begin', sCurrentName);

  cnFileId := TO_NUMBER(STB$JOB.getJobParameter('MASTERTEMPLATE', nJobId));    
  cnCurrentJobId := nJobId;

  sExpEntity := csTablePrefix||ISR$REPORTCALCULATIONLib.GetRepTypeExperimentEntity(nRepTypeID);

  -- first fill only parameter on report level an evaluate them
  FillCalcParamList('REPORT', lRepParamList);

  -- generate and insert calculation fragments for studies
  sDynSel := 'SELECT ID FROM '||csTempTabPrefix||'BIO$STUDY'; -- TODO: needs to be defined using a parameter instead, if possible
  OPEN cStudies for sDynSel;
  FETCH cStudies INTO sStudyID;  
  WHILE cStudies%FOUND LOOP

    -- create "calculations"-node for this study
    ISR$XML.CreateNode  (cxXml, '/study/bioanalytic/studies/study[@id='''||sStudyID||''']', 'calculations', NULL);

    -- add study parameters 
    lStudyParamList := lRepParamList;
    -- add from local variables
    lStudyParamList.Extend;    
    lStudyParamList(lStudyParamList.last) := iSR$Calc$Param$Rec('StudyID', sStudyID, sStudyID, 'T');
    FillCalcParamList('STUDY', lStudyParamList);
    isr$trace.debug('after list for Study '||sStudyID, nRepID||' - '||nRepTypeID, sCurrentName);

    sDynSel := 
    'SELECT KEY experiment 
    FROM isr$crit c, '||sExpEntity||' e 
    WHERE c.entity = e.entity 
      AND c.KEY = e.CODE 
      AND repid = :cnRepid 
    ORDER BY e.ordernumber';
    isr$trace.debug('sExpEntity/sStudyID', sExpEntity||'/'||sStudyID, sCurrentName);
    open cRepExperiments for sDynSel using in nRepid;
    FETCH cRepExperiments INTO sExperiment;
    WHILE cRepExperiments%FOUND LOOP
      lExpStatements := lExpEmpty;
      lExpStatements := ISR$REPORTCALCULATIONLib.GetExpCalcStatements(nRepTypeID, sExperiment);
      if lExpStatements.count > 0 then
        for i in 1 .. lExpStatements.count loop 
          isr$trace.info('statement for '||sExperiment, lExpStatements(i).statementname, sCurrentName, lExpStatements(i).statement);
          sCalculationStatement := lExpStatements(i).statement;   
          lExpVars := ISR$REPORTCALCULATIONLib.FindUsedParams(lExpStatements(i).statement);            

          -- add experiment and calculation parameters
          lExpParamList := lStudyParamList;
          lExpParamList.Extend;
          lExpParamList(lExpParamList.last) := iSR$Calc$Param$Rec('Experiment', sExperiment, sExperiment,'T');  
          lExpParamList.Extend;
          lExpParamList(lExpParamList.last) := iSR$Calc$Param$Rec('CalcStatmentAlias',lExpStatements(i).CalcStatmentAlias,lExpStatements(i).CalcStatmentAlias,'T');  
          -- add statement parameters
          FillCalcParamList(lExpStatements(i).StatementName, lExpParamList); 
          -- add experiment parameters
          FillCalcParamList(lExpStatements(i).CalcStatmentAlias, lExpParamList);
          FillParamListValues(lExpParamList);

          -- debug
          isr$trace.debug('lExpParamList.Count', lExpParamList.count, sCurrentName);
          for i in 1..lExpParamList.count loop
            isr$trace.debug('lExpParamList('||i||').sParameter/sValue/sIsEval', 
                              lExpParamList(i).sParameter||'/'||lExpParamList(i).sValue||'/'||lExpParamList(i).sIsEval, sCurrentName);
          end loop;

          sCalculationStatement := ReplaceCalcParamValues( sCalculationStatement, lExpVars, lExpParamList);

          -- if variables return strings with vars, this have to be replaced too
          lExpVars := ISR$REPORTCALCULATIONLib.FindUsedParams(sCalculationStatement);
          if lExpVars.count > 0 then
            -- statement still containes vars. Try to replace them
             sCalculationStatement := ReplaceCalcParamValues( sCalculationStatement, lExpVars, lExpParamList);
          end if;

          isr$trace.info('statement for '||lExpStatements(i).CalcStatmentAlias||' after replace', lExpStatements(i).statementname, sCurrentName, sCalculationStatement);

          oErrorObj := CreateCalculationNode(sCalculationStatement, sStudyID, sExperiment, cxXml);
          IF oErrorObj.sSeverityCode!=Stb$typedef.cnNoError THEN
            Stb$job.setJobProgress(nJobID, Stb$typedef.cnReportAbort, 'Aborted with error ('||oErrorObj.sErrorCode||')', 1 );
            RETURN oErrorObj;
          END IF;

        end loop;
      else 
        isr$trace.debug('no statement for '||sExperiment, sStudyID, sCurrentName);
      end if;
      FETCH cRepExperiments INTO sExperiment;
    END LOOP;
    close cRepExperiments;


    FETCH cStudies INTO sStudyID;    
  END LOOP;
  CLOSE cStudies;

  isr$trace.stat('end','end', sCurrentName);
  return oErrorObj;

exception 
  when others then        
    sMsg :=  utd$msglib.getmsg ('exUnexpectedError', stb$security.getcurrentlanguage, csP1 => sqlerrm(sqlcode));
    oErrorObj.handleError ( Stb$typedef.cnSeverityCritical, sMsg, sCurrentName, 
                            dbms_utility.format_error_stack ||stb$typedef.crlf|| dbms_utility.format_error_backtrace  );  
    return oErrorObj;          
end AddStudyCalculations;

end ISR$REPORTCALCULATION;