CREATE OR REPLACE PACKAGE ISR$REPORTCALCULATIONLIB
AS
-- ***************************************************************************************************
-- Description/Usage:
-- the package is the inerface between the underlaying Watson LIMS of Abbott Laboratories 
-- to the backend of iStudyReporter  
-- ***************************************************************************************************
-- Modification history:
-- Ver   Date          Autor    System            Project                         Remarks
-- ***************************************************************************************************
-- 1.0   13. Nov 2008  HR       iStudyReporter    iStudyReproter@watson           Creation
-- ***************************************************************************************************
-- functions and procedures
-- ****************************************************************************************************
type tCalcualtionStatement is record(
  StatementName     ISR$CALCULATION$STATEMENTS.STATEMENTNAME%type,
  Statement         ISR$CALCULATION$STATEMENTS.STATEMENT%type,
  CalcStatmentAlias ISR$FILE$CALC$VALUES.Info%type);

type tlExpStatementsList is table of tCalcualtionStatement index by binary_integer;
-- holds the calculations list for one experiment in one report

FUNCTION FindUsedParams(csText in varchar2) RETURN sys.ODCIVarchar2List;
-- ***************************************************************************************************
-- Date and Autor: 17.Aug 09 HR
-- Finds the contained variables marked with &&Variable& in csText and returs them in the collection 
--  of type iSR$Text$List. eache variable found is conained once in the collection, 
--  regardless of its frequency
--
-- Parameter
-- csText       the text to be searched for variables
--
-- Return
--   collection of type iSR$Text$List, containing the variables found
--
-- Error behaviour/ particularities:
-- none
-- ***************************************************************************************************

FUNCTION GetCalcValuesOrder(csCalcStatmentAlias in varchar2, csValues in varchar2) return varchar2;
--25. Aug 2009

FUNCTION GetAdditionalCondition(csCalcStatmentAlias in varchar2, csSubCondition in varchar2 default null) return varchar2;
-- ***************************************************************************************************
-- Date and Autor: 24.Oct 2019
-- Biulds the additional condition for a calculation in an experiment
--  searches the entries in ISR$FILE$CALC$VALUES with Entity = 'EXP$CALC$COND' or "'EXP$CALC$COND$' + csSubCondition" if not null
--  and the passed statement alias in value
--  The entries found are put together with LF and an AND between them
--
-- Parameter
-- csCalcStatmentAlias     the statement alias
--
-- Return
--   the build additional condition
--
-- Error behaviour/ particularities:
-- none
-- ***************************************************************************************************

FUNCTION BuildCalibrationParamPiece(csParamName in varchar2, csFormatFunc in varchar2, csReprFunc in varchar2, cnRepresentationParam IN number) RETURN varchar2;
-- ***************************************************************************************************
-- Date and Autor: 25. Aug 2009
-- builds the calibration parameter statement piece.
--  Searches in ISR$CALCULATION$STATEMENTS for the statement named EXPERIMENT_REGPARAMVAL_PIECE
--  Replaces the variables '&&ParamName&','&&OwnRepresentationParam&', '&&OwnFormatFunc&' 
--  and '&&OwnRepresentationFunc&' with the values in the parameters (in this order) and 
--  return the result of the replacement
--
-- Parameter
--  csParamName             the parameter name
--  csFormatFunc            the format function (FormatSig or FormatRounded)
--  csReprFunc              the representation function (SigFigures os round)     
--  cnRepresentationParam   the representation parameter (num of figures or dec places)
--
-- Return
--   the biuld calibration parameter statement piece.
--
-- Error behaviour/ particularities:
-- none
-- ***************************************************************************************************

FUNCTION BuildCalibrationParamPiece(csParamName in varchar2, csFormatFunc in varchar2, csReprFunc in varchar2, cnRepresentationParam IN number, csPieceStatement IN varchar2) RETURN varchar2;
-- ***************************************************************************************************
-- Date and Autor: 05. Sep 2019
-- builds the calibration parameter statement piece.
--  Searches in ISR$CALCULATION$STATEMENTS for the statement named in csPieceStatement
--  Replaces the variables '&&ParamName&','&&OwnRepresentationParam&', '&&OwnFormatFunc&' 
--  and '&&OwnRepresentationFunc&' with the values in the parameters (in this order) and 
--  return the result of the replacement
--
-- Parameter
--  csParamName             the parameter name
--  csFormatFunc            the format function (FormatSig or FormatRounded)
--  csReprFunc              the representation function (SigFigures os round)     
--  cnRepresentationParam   the representation parameter (num of figures or dec places)
--  csPieceStatement        the calc statement to be used for the pieces
--
-- Return
--   the build calibration parameter statement piece.
--
-- Error behaviour/ particularities:
-- none
-- ***************************************************************************************************

Function GetRepTypeExperimentEntity( cnReportTypeID in integer) return varchar2;
-- ***************************************************************************************************
-- Date and Autor: 13.Aug 2009 HR
-- Retrieves the experiments entity name from ISR$FILE$CALC$VALUES 
--  configured here with Entity = 'EXP$LIST$ENTITY'
--
-- Parameter
-- cnReportTypeID       the report type ID for which the entity name is needed
--
-- Return
--   the entity name
--
-- Error behaviour/ particularities:
-- none
-- ***************************************************************************************************

Function GetExpCalcStatements( cnReportTypeID in integer, csExperiment in varchar2 ) return tlExpStatementsList;
-- ***************************************************************************************************
-- Date and Autor: 13.Aug 2009 HR
-- Retrieves the statements of a report experiment
--
-- Parameter
-- cnReportTypeID       the report type ID for which the statements are needed
-- csExperiment         the experiment 
--
-- Return
--   the experiments list
--
-- Error behaviour/ particularities:
-- none
-- ***************************************************************************************************


FUNCTION GetReportRunTypes(nRepID IN number)  RETURN varchar2;
-- ***************************************************************************************************
-- Date and Autor:  18.Dec 2008 HR
-- Retrieves the supported run types for the report in nRepID
--  The primary used run types are configured in the report type parameter RUNTYPE
--  In addition, a report parameter ADDITIONAL_RUNTYPE could exist for some report types 
--  This found run types are put together in a string usable in a IN list in the where clause
--  Example: the run types list could be 'VALIDATION'   or   'VALIDATION','PSAE'
--
-- Parameter
-- nRepID          the report ID for which the run types are needed
--
-- Return
--   the run types list
--
-- Error behaviour/ particularities:
-- none
-- ***************************************************************************************************

FUNCTION BuildStatementCalibrationParam(csStudyID IN varchar2, cnDecPlRegParam IN number, cnRsquPrec in number, cnDecPlSD IN number, cnPrecPercCV in number, nRepID IN number) RETURN varchar2;
-- ***************************************************************************************************
-- Date and Autor:  18.Dec 2008 HR
-- Builds the select statement for the regression parameter columns calculations.
--  Calculation is used in different report types
--
-- Parameter
-- csStudyID       the study ID in the temp tables 
-- cnDecPlRegParam number of decimal places to be used to format the regparam values
-- cnRsquPrec      number of decimal places to be used to format the rsquared values
-- cnDecPlSD       number of decimal places to be used to format the SD values
-- cnPrecPercCV    number of decimal places to be used to format the CV values
-- nRepID          the report ID
--
-- Return
--   the select statement 
--
-- Error behaviour/ particularities:
-- none
-- ***************************************************************************************************



end ISR$REPORTCALCULATIONLIB;