CREATE OR REPLACE PACKAGE ISR$REPORTCALCULATION
AS
-- ***************************************************************************************************
-- Description/Usage:

-- ***************************************************************************************************
-- Modification history:
-- Ver   Date          Autor    System            Project                         Remarks
-- ***************************************************************************************************
-- 1.0   15.Sep 2009  HR       iStudyReporter    iStudyReproter@watson           Creation
-- ***************************************************************************************************
-- functions and procedures
-- ****************************************************************************************************

  cnFileId NUMBER; -- used to store the template id

function AddStudyCalculations(nJobId IN number, cxXml in out XMLTYPE, oParamList in iSR$AttrList$Rec) return STB$oError$Record;
-- ***************************************************************************************************
-- Date and Autor:  13.Nov 2008 HR
-- adds the calculations node under the study node and the calculations specific to the tox to this node
--
-- Parameter
-- nJobId        the job ID of the current XML file creation run
-- cxXml         the XML file
-- oParamList    the list of parameter values of the current report
--
-- Return
-- STB$OERROR$RECORD    error object with the fields (sErrorCode, sErrorMessage, sSeverityCode) 
--
-- Error behaviour/ particularities:
-- none
-- ***************************************************************************************************

end ISR$REPORTCALCULATION;