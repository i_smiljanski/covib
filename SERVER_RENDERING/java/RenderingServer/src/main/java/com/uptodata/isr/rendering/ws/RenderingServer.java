package com.uptodata.isr.rendering.ws;

import com.uptodata.isr.loader.webservice.LoaderWebserviceImpl;
import com.uptodata.isr.observers.base.dbAccess.DbData;
import com.uptodata.isr.observers.childServer.ObserverChildServerInterface;
import com.uptodata.isr.server.utils.logging.log4j2.IsrServerLogger;
import com.uptodata.isr.utils.exceptions.MessageStackException;
import com.uptodata.isr.utils.xml.dom.XmlHandler;
import org.apache.commons.collections4.map.CaseInsensitiveMap;

import javax.jws.WebMethod;
import javax.jws.WebService;
import javax.jws.soap.SOAPBinding;

/**
 * Created by schroedera85 on 22.09.2015.
 */
@WebService(name = "RenderingServer", targetNamespace = "http://ws.rendering.isr.uptodata.com/")
@SOAPBinding(style = SOAPBinding.Style.RPC)
public class RenderingServer extends LoaderWebserviceImpl implements ObserverChildServerInterface {

    private String logLevel;
//    DbData dbData;
    static public IsrServerLogger log;
    protected XmlHandler statusXml;
    protected int statusXmlMethod;



    @WebMethod(exclude = true)
    public void createMethodNode(String methodName, CaseInsensitiveMap<String, String> params) throws MessageStackException {
        statusXmlMethod++;
        createMethodNodeWithNum(methodName, params, statusXmlMethod);
    }

    @WebMethod(exclude = true)
    public void setStatusXml(XmlHandler statusXml) {
        this.statusXml = statusXml;
    }

    @WebMethod(exclude = true)
    public XmlHandler getStatusXml() {
        return statusXml;
    }


    @WebMethod
    public boolean pingMe() {
        return true;
    }

    RenderingServer(DbData dbData, String logLevel) {
        this.dbData = dbData;
        this.logLevel = logLevel;
        statusXmlMethod = 0;
    }

    public boolean ping() {
        return true;
    }


}
