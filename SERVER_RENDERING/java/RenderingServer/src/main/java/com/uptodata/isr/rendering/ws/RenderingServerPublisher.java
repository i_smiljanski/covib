package com.uptodata.isr.rendering.ws;

import com.uptodata.isr.observers.base.dbAccess.DbData;
import com.uptodata.isr.observers.base.secutity.CryptDbPropertiesHandler;
import com.uptodata.isr.server.utils.logging.log4j2.IsrServerLogger;
import com.uptodata.isr.server.utils.logging.log4j2.LogHelper;
import com.uptodata.isr.server.utils.network.UrlUtil;
import com.uptodata.isr.utils.ConvertUtils;

import javax.xml.ws.Endpoint;
import java.io.File;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.Properties;

/**
 * Created by schroedera85 on 20.11.2015.
 */
public class RenderingServerPublisher {


    public static void main(String[] args) {
        IsrServerLogger log = null;
        try {
            Path currentRelativePath = Paths.get("");
            String currentDirectoryPath = currentRelativePath.toAbsolutePath().toString();
            Properties props = ConvertUtils.convertArrayToProperties(args);

            File logsFolder = new File(currentDirectoryPath, "logs");
            String logsFolderPath = logsFolder.getAbsolutePath();
            System.setProperty("java.io.tmpdir", logsFolderPath); // das ist für logger aus frontend.
            String logLevel = props.getProperty("logLevel");
            String wsUrl = props.getProperty("wsUrl");

            DbData dbData = new DbData(props);

            //logging in DB
            CryptDbPropertiesHandler.saveDbPropertiesForLog(args);

            log = LogHelper.initLogger(logLevel, "rendering server");
            log.info("RenderingServerPublisher has params==: " + ConvertUtils.propertiesWithPasswordToString(props));
            log.debug("Rendering server directory== " + currentDirectoryPath);
            log.debug("java.io.tmpdir== " + System.getProperty("java.io.tmpdir"));
            RenderingServer renderingServer = new RenderingServer(dbData, logLevel);
            renderingServer.setStandalone(true);
            renderingServer.initStatusXml();
            renderingServer.createEnvironment(currentDirectoryPath, "workFolder");

            log.debug("try to start loader web service");
            Endpoint endpoint = UrlUtil.createAndPublishEndpoint(wsUrl, renderingServer);
            if (endpoint != null) {
                log.debug("loader web service started");
            } else {
                log.debug("loader web service couldn't started");
            }


        } catch (Exception e) {
            if (log != null) {
                log.error("error==" + e);
            }

            e.printStackTrace();
        }
    }
}
