package com.uptodata.isr.transform;

import com.uptodata.isr.observer.transfer.DbFileSupplier;
import com.uptodata.isr.observer.transfer.FileSupplier;
import com.uptodata.isr.server.utils.logging.log4j2.IsrServerLogger;
import com.uptodata.isr.server.utils.logging.log4j2.LogHelper;
import com.uptodata.isr.server.utils.logging.log4j2.LoggerInterface;
import com.uptodata.isr.server.utils.serverProcess.ProcessInfoObject;
import com.uptodata.isr.utils.ConvertUtils;
import com.uptodata.isr.utils.exceptions.MessageStackException;
import org.apache.commons.lang.StringUtils;

import javax.xml.transform.*;
import javax.xml.transform.stream.StreamResult;
import javax.xml.transform.stream.StreamSource;
import java.io.*;
import java.util.HashMap;
import java.util.Map;

/**
 * Created by schroedera85 on 30.09.2015.
 */
public class XsltTransformer implements LoggerInterface {
    private FileSupplier fileWorker;
    private static String transformError;
    /*
    * jobId, logLevel, logReference are for the logging
    * processInfoObject has the information about the object that should be transformed(String inputFileName; String transformerFileName;
    *                                                                                   String resultFileName; String resultId;)
    *
    * */

    public boolean transformProcessObject(final TransformerFactory transformerFactory,
                                          final ProcessInfoObject processInfoObject,
                                          String logLevel, String logReference) throws MessageStackException {
        String xmlFileName = processInfoObject.getInputFileName();
        String xsltFileName = processInfoObject.getTransformerFileName();
        String resultId = processInfoObject.getResultId();
        HashMap<String, String> parameters = processInfoObject.getParameters();
        Reader xmlReader;
        IsrServerLogger log = initLogger(logLevel, logReference);
        log.stat("begin==" + xmlFileName);
        try {
            xmlReader = getXmlReader(xmlFileName, logLevel, logReference);
        } catch (MessageStackException e) {
            String errorMessage = "Can not create xml reader for file " + xmlFileName;
            log.error(errorMessage);
            throw new MessageStackException(e, MessageStackException.SEVERITY_CRITICAL, errorMessage,
                    MessageStackException.getCurrentMethod());

        }
        if (xmlReader == null) {
            String errorMessage = "Can not create xml reader for file " + xmlFileName;
            log.error(errorMessage);
            throw new MessageStackException(new RuntimeException("xml reader is null"),
                    MessageStackException.SEVERITY_CRITICAL, errorMessage, MessageStackException.getCurrentMethod());
        }
        log.debug("XmlReader for file ' " + xmlFileName + "' created successfully");
        Reader xsltReader;
        try {
            xsltReader = getXsltReader(xsltFileName, logLevel, logReference);
        } catch (MessageStackException e) {
            String errorMessage = "Can not create xslt reader for file " + xsltFileName;
            log.error(errorMessage);
            throw new MessageStackException(e, MessageStackException.SEVERITY_CRITICAL, errorMessage,
                    MessageStackException.getCurrentMethod());
        }
        if (xsltReader == null) {
            String errorMessage = "Can not create xslt reader for file " + xsltFileName;
            log.error(errorMessage);
            throw new MessageStackException(new RuntimeException("xslt reader is null"),
                    MessageStackException.SEVERITY_CRITICAL, errorMessage, MessageStackException.getCurrentMethod());
        }
        log.debug("XsltReader for file ' " + xsltFileName + "'  created successfully");

        File resultFile = fileWorker.getResultFile(resultId);
        StreamResult result = new StreamResult(resultFile);
        try {
            transform(transformerFactory, xmlReader, xsltReader, parameters, result, log);
            return true;
        } catch (Exception e) {
            String errorMessage = "Can not make xslt transformation for file " + xmlFileName + " with stylesheet "
                    + xsltFileName + ", " + (StringUtils.isNotEmpty(transformError) ? transformError : e);
            log.error(errorMessage);
            createErrorFile(resultFile, e, log);
            throw new MessageStackException(e, MessageStackException.SEVERITY_CRITICAL, errorMessage,
                    MessageStackException.getCurrentMethod());
        } finally {
            try {
                xmlReader.close();
                log.debug("xmlReader for " + xmlFileName + " closed");
            } catch (IOException e1) {
                log.error("Can not close xml reader for " + xmlFileName);
            }

            try {
                xsltReader.close();
                log.debug("xsltReader for " + xsltFileName + "closed");
            } catch (IOException e1) {
                log.error("Can not close xslt reader for " + xsltFileName);
            }
            log.stat("end==" + xmlFileName);
        }
    }

    /*
    * creates local log file with errors
    *
    * */

    private void createErrorFile(File resultFile, Exception e, IsrServerLogger log) {
        log.stat("begin");
        FileWriter fileWriter;
        try {
            fileWriter = new FileWriter(resultFile);
            PrintWriter printWriter = new PrintWriter(fileWriter, true);
            printWriter.println("<html><body>");
            log.debug("transformError: " + transformError);
            if (StringUtils.isNotEmpty(transformError)) {
                printWriter.println(transformError);
            } else {
                e.printStackTrace(printWriter);
            }
            printWriter.println("</body></html>");
            printWriter.close();
            fileWriter.close();
            log.stat("end");
        } catch (IOException e1) {
            log.error("Can not write error file for " + resultFile.getAbsolutePath());
        }
    }

    private Reader getXmlReader(String xmlFileName, String logLevel, String logReference) throws MessageStackException {
        Reader result = getReader(DbFileSupplier.FileType.INPUT, xmlFileName, logLevel, logReference);
        return result;
    }

    private Reader getXsltReader(String xsltFileName, String logLevel, String logReference) throws MessageStackException {
        Reader result = getReader(DbFileSupplier.FileType.TRANSFORMER, xsltFileName, logLevel, logReference);
        return result;
    }

    private Reader getReader(DbFileSupplier.FileType fileType, String fileName,
                             String logLevel, String logReference) throws MessageStackException {
        IsrServerLogger log = initLogger(logLevel, logReference);
        log.stat("begin==" + fileName);
        Reader isReader;
        File file = fileWorker.getFileFromDb(fileType, fileName, logLevel, logReference);
        log.debug(file);
        try {
            InputStream inputStream = new FileInputStream(file);
            isReader = new InputStreamReader(inputStream, ConvertUtils.encoding);
            log.debug("fileReader ready for " + file.getAbsolutePath());
        } catch (FileNotFoundException e) {
            String errorMessage = "File " + file.getAbsolutePath() + " not found! " + e;
            log.error(errorMessage);
            throw new MessageStackException(e, MessageStackException.SEVERITY_CRITICAL, errorMessage, MessageStackException.getCurrentMethod());
        } catch (Exception e) {
            String errorMessage = "Unknown exception " + e;
            log.error(errorMessage);
            throw new MessageStackException(e, MessageStackException.SEVERITY_CRITICAL, errorMessage, MessageStackException.getCurrentMethod());

        } finally {
            log.stat("end==" + fileName);
        }
        return isReader;
    }

    public void transform(TransformerFactory transformerFactory, Reader xmlReader, Reader xsltReader,
                          HashMap<String, String> transformParameters, Result transformResult, IsrServerLogger log) throws Exception {
        log.stat("begin");
        Source xmlSource = new StreamSource(xmlReader);
        Source xsltSource = new StreamSource(xsltReader);

        ErrorListener listener = new ErrorListener() {
            public void error(TransformerException exception) throws
                    TransformerException {
                transformError = exception.getMessageAndLocation();
                log.error(transformError);
                throw exception;
            }

            public void fatalError(TransformerException exception)
                    throws TransformerException {
                transformError = exception.getMessageAndLocation();
                log.error(transformError);
                throw exception;
            }

            public void warning(TransformerException exception)
                    throws TransformerException {
                transformError = exception.getMessageAndLocation();
                log.warn("error: " + exception);
            }
        };
        transformerFactory.setErrorListener(listener);

        Transformer transformer = transformerFactory.newTransformer(xsltSource);
        transformer.setOutputProperty(OutputKeys.ENCODING, ConvertUtils.encoding);
        transformer.setErrorListener(listener);

        for (Map.Entry<String, String> parameter : transformParameters.entrySet()) {
            String parameterName = parameter.getKey();
            String parameterValue = parameter.getValue();
            transformer.setParameter(parameterName, parameterValue);
        }
        // execute transformation & fill result target object
        transformer.transform(xmlSource, transformResult);

        log.stat("end");
    }


    public void setFileWorker(FileSupplier fileWorker) {
        this.fileWorker = fileWorker;
    }

    public static void main(String[] args) {
        XsltTransformer xsltTransformer = new XsltTransformer();
        String userDir = System.getProperty("user.dir");
        File xmlFile = new File(userDir + "\\test\\rohdaten.xml");
        File xsltFile = new File(userDir + "\\test\\transformers\\ISR$WT$TOX$SUM$QC.xsl");
        File resultFile = new File(userDir + "\\test\\result.xml");
        StreamResult result = new StreamResult(resultFile);
        System.setProperty("javax.xml.transform.TransformerFactory", "net.sf.saxon.TransformerFactoryImpl");
        TransformerFactory transformerFactory = TransformerFactory.newInstance();
        transformerFactory.setURIResolver(new URIResolver() {
            @Override
            public Source resolve(String href, String base) throws TransformerException {
                try {
                    return new StreamSource(new FileInputStream(userDir + "\\test\\transformers\\" + href));
                } catch (FileNotFoundException e) {
                    e.printStackTrace();
                    return null;
                }
            }
        });
        IsrServerLogger log = null;
        try {
            Reader xmlReader = new FileReader(xmlFile);
            Reader xsltReader = new FileReader(xsltFile);
            log = LogHelper.initLogger("DEBUG", "");

            xsltTransformer.transform(transformerFactory, xmlReader, xsltReader, new HashMap<>(), result, log);

        } catch (Exception e) {
            XsltTransformer transformer = new XsltTransformer();
            transformer.createErrorFile(resultFile, e, log);
        }

    }
}
