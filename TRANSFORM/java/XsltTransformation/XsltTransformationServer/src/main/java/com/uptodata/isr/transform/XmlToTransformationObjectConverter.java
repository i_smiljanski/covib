package com.uptodata.isr.transform;

import com.uptodata.isr.server.utils.serverProcess.XmlToProcessObjectConverter;
import com.uptodata.isr.utils.xml.dom.XmlHandler;
import org.w3c.dom.CharacterData;
import org.w3c.dom.*;
import org.xml.sax.InputSource;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import java.io.StringReader;
import java.util.HashMap;
import java.util.List;

/**
 * Created by schroedera85 on 24.09.2015.
 */
public class XmlToTransformationObjectConverter extends XmlToProcessObjectConverter {

    public XmlToTransformationObjectConverter() {
        processTag = "//xsltTransformation";
        inputTagName = "xml";
        transformerTagName = "xslt";
        resultTagName = "result";
    }

/*
    schema von einer xslt transformation:
<jobId>
    <processNode>
        <xml> xmlFile.xml </xml>
        <xslt> xsltFile.xslt </xslt>
        <result> 1234 </result>
        <transformationParameters>
            <parameter>
                <name> parameterName_1 </name>
                <value> parameterValue_1 </value>
            </parameter>
            <parameter>
                <name> parameterName_2 </name>
                <value> parameterValue_2 </value>
            </parameter>
            .....
        </transformationParameters>
    </processNode>



<transformationParameters>
<![CDATA[<PARAMETERS>
 <PARAMETER name="1">S_APPROVAL</PARAMETER>
.....
<PARAMETER name="2">S_APPROVAL</PARAMETER>
</PARAMETERS>]]>
</transformationParameters>
     */

    @Override
    public HashMap<String, String> getParameters(XmlHandler xmlHandler, Node xsltTransformation) {
        HashMap<String, String> result = new HashMap<String, String>();
        String parametersTagName = "transformationParameters";
//        String parameterTagName = "parameter"; // todo die namen sind sehr änlich, sehr fehleranfällig!
        List<Node> transformationParameters = xmlHandler.selectNodes(xsltTransformation, parametersTagName); // tag <parameters>

        if (transformationParameters.isEmpty()) {
            return result;
        }

        if (transformationParameters.size() > 1) {
            throw new RuntimeException("In XsltTransformation are found more as one tag '" + parametersTagName
                    + "', it is exactly one tag '" + parametersTagName + "' allowed! " +
                    System.lineSeparator() + xmlHandler.nodeToString(xsltTransformation));
        }

        Node allParametersNode = transformationParameters.get(0);// tag <parameters>
        String parametersAsString = getCharacterDataFromElement((Element) allParametersNode);

        DocumentBuilder e;
        try {
            e = this.createDocumentBuilder();
            if (parametersAsString != null && !parametersAsString.isEmpty()) {
                Document doc = e.parse(new InputSource(new StringReader(parametersAsString)));
                NodeList allParametersList = doc.getElementsByTagName("PARAMETER");
                for (int i = 0; i < allParametersList.getLength(); i++) {
                    Node parameterNode = allParametersList.item(i);
                    String parameterName = parameterNode.getAttributes().getNamedItem("name").getNodeValue();
                    String parameterValue = parameterNode.getTextContent();
                    result.put(parameterName, parameterValue);
                }
            }
        } catch (Exception e1) {
            e1.printStackTrace();
        }


//        List<Node> parameters = xmlHandler.selectNodes(xsltTransformation, parameterTagName);// das sind die einzelne tags <parameter>
//        String parameterNameTag = "name";
//        String parameterValueTag = "value";
//        for (Node parameter : parameters) {
//            String parameterName = xmlHandler.getTextContentByTagName(parameter, parameterNameTag);
//            String parameterValue = xmlHandler.getTextContentByTagName(parameter, parameterValueTag);
//            String oldParameterValue = result.put(parameterName, parameterValue);
//            if (oldParameterValue != null) {
//                throw new RuntimeException("There are multiple parameters with the same name " + parameterName +
//                        System.lineSeparator() + xmlHandler.nodeToString(allParametersNode));
//            }
//        }
        return result;
    }

    public String getCharacterDataFromElement(Element e) {
        NodeList list = e.getChildNodes();
        String data;

        for (int index = 0; index < list.getLength(); index++) {
            if (list.item(index) instanceof CharacterData) {
                CharacterData child = (CharacterData) list.item(index);
                data = child.getData();

                if (data != null && data.trim().length() > 0)
                    return child.getData();
            }
        }
        return "";
    }

    private DocumentBuilder createDocumentBuilder() throws ParserConfigurationException {
        DocumentBuilderFactory docBuilderFactory = DocumentBuilderFactory.newInstance();
        docBuilderFactory.setNamespaceAware(true);
        return docBuilderFactory.newDocumentBuilder();
    }
}
