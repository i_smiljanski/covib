package com.uptodata.isr.transform;

import com.uptodata.isr.observers.base.dbAccess.DbData;
import com.uptodata.isr.observers.base.secutity.CryptDbPropertiesHandler;
import com.uptodata.isr.server.utils.logging.log4j2.IsrServerLogger;
import com.uptodata.isr.server.utils.logging.log4j2.LogHelper;
import com.uptodata.isr.server.utils.network.UrlUtil;
import com.uptodata.isr.transform.ws.XsltTransformServerImpl;
import com.uptodata.isr.transform.ws.XsltTransformationServerInterface;
import com.uptodata.isr.utils.ConvertUtils;

import java.util.Properties;

/**
 * Created by schroedera85 on 25.09.2015.
 */
public class XsltTransformationServerPublisher {

    public static void main(String[] args) {
        IsrServerLogger log = null;
        try {
            Properties props = ConvertUtils.convertArrayToProperties(args);// args from db, are set by creating the server

            String logLevel = props.getProperty("logLevel");
            String wsUrl = props.getProperty("wsUrl");

            DbData dbData = new DbData(props);
            CryptDbPropertiesHandler.saveDbPropertiesForLog(args);

            log = LogHelper.initLogger(logLevel, "xslt transformation server");
            log.info("properties for start server==" + ConvertUtils.propertiesWithPasswordToString(props));

            XsltTransformationServerInterface transformServer = new XsltTransformServerImpl(dbData, logLevel);
            transformServer.init();

            UrlUtil.createAndPublishEndpoint(wsUrl, transformServer);

            log.info("Service 'XsltTransformationServer' is started on ==" + wsUrl);

            //logging in DB
        } catch (Exception e) {
            if (log != null) {
                log.error(e);
            }
            e.printStackTrace();
        }
    }
}
