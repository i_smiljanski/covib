package com.uptodata.isr.transform.ws;

import com.uptodata.isr.observer.transfer.DbFileSupplier;
import com.uptodata.isr.observer.transfer.FileSupplier;
import com.uptodata.isr.observers.base.dbAccess.DbData;
import com.uptodata.isr.server.utils.fileSystem.FileSystemWorker;
import com.uptodata.isr.server.utils.serverProcess.ProcessInfoObject;
import com.uptodata.isr.transform.XmlToTransformationObjectConverter;
import com.uptodata.isr.utils.exceptions.MessageStackException;
import com.uptodata.isr.utils.xml.dom.XmlHandler;
import org.apache.commons.collections4.map.CaseInsensitiveMap;
import org.apache.logging.log4j.Level;
import org.w3c.dom.Node;

import javax.jws.WebService;
import java.io.*;
import java.net.URISyntaxException;
import java.nio.charset.StandardCharsets;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.time.ZonedDateTime;
import java.time.format.DateTimeFormatter;
import java.util.Base64;
import java.util.HashMap;
import java.util.stream.Stream;

/**
 * Created by schroedera85 on 22.09.2015.
 */
@WebService(endpointInterface = "com.uptodata.isr.transform.ws.XsltTransformationServerInterface")
public class XsltTransformServerImpl extends XsltTransformationServerInterface {

    private String workFolderName = "workFolder";
    private HashMap<String, MultipleXsltTransformationsExecutor> jobs;
    private String logLevel;
    DbData dbData;


    public XsltTransformServerImpl() {
        initErrorStream();
    }

    /*
     *
     * initiates the format of the log entries
     * writes errors and outputs into log files
      */
    private static void initErrorStream() {
        String pattern = "dd-MM-yy_HH_mm";
        DateTimeFormatter dateFormatter = DateTimeFormatter.ofPattern(pattern);
        ZonedDateTime nowData = ZonedDateTime.now();
        String fileName = nowData.format(dateFormatter);
        File errorFile = new File("XsltTransformServerErrors_" + fileName + ".log");
        try {
            System.setErr(new PrintStream(errorFile));
            System.setOut(new PrintStream(errorFile));
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        }
    }

    public XsltTransformServerImpl(DbData dbData, String logLevel) {
        this.dbData = dbData;
        this.logLevel = logLevel;
        statusXmlMethod = 0;
        initErrorStream();
    }

    /**
     * Checks wether the working directory was created
     * initiates logger
     */


    @Override
    public void init() throws MessageStackException {
        log = initLogger(logLevel, "xslt transformation service");
        boolean isWorkFolderCreated = initWorkFolder();
        if (!isWorkFolderCreated) {
            log.error("error==XsltTransformServer can not create work folder with name " + workFolderName);
        }
        jobs = new HashMap<>();
        statusXml = initStatusXml();  //test case for test exception: comment this row
    }

    /*
    * initiates the working directory if it doesn't exists
    * if wd exists, deletes the wd with the data
    * */
    private boolean initWorkFolder() throws MessageStackException {
        File workFolder = new File(workFolderName);
        if (workFolder.exists()) {
            try {
                boolean isOk = FileSystemWorker.tryToRemoveFolder(workFolder, 5, 500);
            } catch (Exception e) {
                String workFolderPath = workFolder.getAbsolutePath();
                String errorMsg = "Can not remove old work directory " + workFolderPath + System.lineSeparator() + " " + e;
                log.error("error==" + errorMsg);
                throw new MessageStackException(e, MessageStackException.SEVERITY_CRITICAL, errorMsg,
                        MessageStackException.getCurrentMethod());
            }
        }
        try {
            boolean isOk = FileSystemWorker.tryToCreateFolder(workFolderName, 5, 500);
        } catch (Exception e) {
            String workFolderPath = workFolder.getAbsolutePath();
            String errorMsg = "Can not create  work directory " + workFolderPath + " " + e;
            log.error("error==" + errorMsg);
            throw new MessageStackException(e, MessageStackException.SEVERITY_CRITICAL, errorMsg,
                    MessageStackException.getCurrentMethod());
        }

        return FileSystemWorker.checkFolder(workFolder);
    }

    /*
    * transforms @xmlAsBase64 with the rules from xmlConfigName using xsltSConfigName
    * writes the as resultConfigName
    *
    * */

    @Override
    public String transform(String xmlAsBase64,
                            String xmlConfigName,
                            String xsltSConfigName,
                            String resultConfigName,
                            String logLevel,
                            String logReference,
                            String logAdditional) throws MessageStackException {
        String currentName = "transformProcessObject";
        log = initLogger(logLevel, logReference, logAdditional);
        log.stat("begin ");
        // for details of the node 'Transformation server'
        if (log.getLevel().isLessSpecificThan(Level.DEBUG)) {
            CaseInsensitiveMap<String, String> args = new CaseInsensitiveMap<>();
            args.put("xmlConfigName", xmlConfigName);
            args.put("xsltSConfigName", xsltSConfigName);
            args.put("resultConfigName", resultConfigName);
            args.put("logLevel", logLevel);
            args.put("logReference", logReference);
            createMethodNode(currentName, args);
        }

        String xml;
//        DbFileSupplier fileWorker = null;
        byte[] base64Bytes = xmlAsBase64.getBytes(StandardCharsets.UTF_8);
        byte[] xmlStringBytes = Base64.getDecoder().decode(base64Bytes);
        xml = new String(xmlStringBytes, StandardCharsets.UTF_8);// converted string(from base24)

        String serverName = "";
        try {
            String jobId = getNodeContent(xml, "transformations/jobId");
            serverName = getNodeContent(xml, "transformations/serverName");

            // there are several transformations possible during the job.
            // So that job folders are differ, is the variable transformNumber:
            String transformNumber = getNodeContent(xml, "transformations/transformNumber");
            log.debug("transformNumber==" + transformNumber);
            String keyDelimiter = getNodeContent(xml, "transformations/keyDelimiter");
            File jobFolder = new File(workFolderName, jobId + "_" + transformNumber);
            log.debug("jobFolder==" + jobFolder);
            DbFileSupplier fileWorker = new DbFileSupplier(jobFolder, dbData);

            MultipleXsltTransformationsExecutor xsltTransformer = new MultipleXsltTransformationsExecutor(
                    jobId, keyDelimiter, logLevel, logReference, logAdditional);
            fileWorker.init(xmlConfigName, xsltSConfigName, resultConfigName);
            log.debug("after init fileWorker");
            xsltTransformer.setFileWorker(fileWorker);
            jobs.put(jobId, xsltTransformer); // adds current jobId and DbFileSupplier to the map

            HashMap<ProcessInfoObject, Boolean> allTransformResults = xsltTransformer.convertXmlAndExecuteProcess(xml,
                    new XmlToTransformationObjectConverter(), logLevel, logReference);
            log.debug("after convertXmlAndExecuteProcess== " + allTransformResults);
            final String[] answer = {""};

            Stream<Path> files = Files.list(Paths.get(jobFolder.getAbsolutePath(), fileWorker.resultFolderName));
            files.forEach(fileName -> {
                log.debug(fileName);
                File resultFile = new File(String.valueOf(fileName));
                boolean isUploaded = false;
                try {
                    log.debug("try to upload xslt transformProcessObject result for ==" + resultFile.getName());
                    isUploaded = fileWorker.uploadFile(resultFile, resultFile.getName(), resultConfigName, logLevel, logReference);// upload file to the db

                } catch (Exception e) {
                    log.error("Could not upload result file  for xslt transformation ==" + resultFile.getName() + " " + e);
                }
                String transformationResult = resultFile.getName() + " : " + isUploaded;
                answer[0] = answer[0] + " " + transformationResult;
                log.debug("transformationResult==" + transformationResult);
            });
            files.close();

            jobs.remove(jobId); // todo muss man speichern in map?
// OFF(0), FATAL(100), ERROR(200), WARN(300), INFO(400), DEBUG(500), TRACE(600)
//            log.debug("logLevel int: " + log.getLevel().intLevel() + " debug int:" + Level.DEBUG.intLevel() + " " + log.getLevel().isMoreSpecificThan(Level.DEBUG));
            if (log.getLevel().intLevel() < Level.DEBUG.intLevel()) {
                log.debug("clean workFolder: " + fileWorker.getWorkFolder());
                clearFileWorker(fileWorker);
            }
            log.stat("end");
            return answer[0]; // resultid : true/false
        } catch (Exception e) {
            String userStack = "error on the server '" + serverName + "' by get remote xml \n" + e;
            onMethodFailed(e, MessageStackException.SEVERITY_CRITICAL, userStack, MessageStackException.getCurrentMethod(), statusXmlMethod);
            return null;
        }
    }


    //todo die selbe metho ist in xsltTransformer, das ist falsch
    private String createErrorMessage(String errorMessage) {
        return errorMessage;
    }

    private String getNodeContent(String xml, String expression) throws MessageStackException {
        XmlHandler xmlHandler = new XmlHandler(xml);
        Node jobIdNode = xmlHandler.getNode(expression);
        if (jobIdNode == null) {
            String errorMessage = "the xml doesn't have the tag '" + expression + "' !";
            log.error(errorMessage);
            throw new MessageStackException(errorMessage);
        }
        String jobId = jobIdNode.getTextContent();
        return jobId;
    }

    private void clearFileWorker(FileSupplier fileWorker) {
        if (fileWorker != null) {
            try {
                fileWorker.clearFileSystem();
            } catch (MessageStackException e) {
                log.error(e);
            }
        }
    }


}
