package com.uptodata.isr.transform.ws;

import com.uptodata.isr.observers.childServer.ObserverChildServerImpl;
import com.uptodata.isr.observers.childServer.ObserverChildServerInterface;
import com.uptodata.isr.utils.exceptions.MessageStackException;

import javax.jws.WebMethod;
import javax.jws.WebService;
import javax.jws.soap.SOAPBinding;

/**
 * Created by schroedera85 on 22.09.2015.
 */


@WebService(name = "XsltTransformationServerInterface", targetNamespace = "http://ws.transform.isr.uptodata.com/")
@SOAPBinding(style = SOAPBinding.Style.DOCUMENT, use = SOAPBinding.Use.LITERAL)
public abstract class XsltTransformationServerInterface extends ObserverChildServerImpl {

    @WebMethod
    public abstract String transform(String xml,
                                     String xmlConfigName,
                                     String xsltSConfigName,
                                     String resultConfigName,
                                     String logLevel,
                                     String logReference,
                                     String logAdditional) throws MessageStackException;

    public abstract void init() throws MessageStackException;
}
