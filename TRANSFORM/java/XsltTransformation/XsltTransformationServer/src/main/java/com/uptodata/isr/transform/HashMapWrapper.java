package com.uptodata.isr.transform;

import java.util.HashMap;

/**
 * Created by schroedera85 on 02.10.2015.
 */
public class HashMapWrapper {

    public HashMap<String, Boolean> map;//muss public sein, sonnst bekommt man leeres antwort in soup request

    public HashMapWrapper(HashMap<String, Boolean> map) {
        this.map = map;
    }

    public HashMap<String, Boolean> getMap() {
        return map;
    }
}
