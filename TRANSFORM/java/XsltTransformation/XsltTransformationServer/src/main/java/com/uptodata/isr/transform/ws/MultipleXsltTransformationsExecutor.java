package com.uptodata.isr.transform.ws;

import com.uptodata.isr.observer.transfer.DbFileSupplier;
import com.uptodata.isr.observer.transfer.FileSupplier;
import com.uptodata.isr.server.utils.logging.log4j2.IsrServerLogger;
import com.uptodata.isr.server.utils.serverProcess.MultipleProcessExecutor;
import com.uptodata.isr.server.utils.serverProcess.ProcessInfoObject;
import com.uptodata.isr.transform.XsltTransformer;
import com.uptodata.isr.utils.exceptions.MessageStackException;

import javax.xml.transform.Source;
import javax.xml.transform.TransformerException;
import javax.xml.transform.TransformerFactory;
import javax.xml.transform.URIResolver;
import javax.xml.transform.stream.StreamSource;
import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.io.Reader;
import java.util.ArrayList;

/**
 * Created by schroedera85 on 24.09.2015.
 */
public class MultipleXsltTransformationsExecutor extends MultipleProcessExecutor {
    private FileSupplier fileWorker;
    private String jobId;
    String keyDelimiter;
    IsrServerLogger log;

    /*
    * initiates MultipleProcessExecutor logLevel and logReference for loging
    * lobid
    *keyDelimiter
    * initiates loging
    * */
    public MultipleXsltTransformationsExecutor(String jobId, String keyDelimiter, String logLevel, String logReference,
                                               String logAdditional) {
        super(logLevel, logReference, logAdditional);
        this.jobId = jobId;
        this.keyDelimiter = keyDelimiter;
        log = initLogger(logLevel, logReference, logAdditional);
        log.setLogReference(logReference);
    }

    /**
     * creates Thread that should execute the thread
     *
     * @param processInfoObject
     * @return
     */
    @Override
    protected ProcessCallable createThread(ProcessInfoObject processInfoObject) {
        return new ProcessCallable(processInfoObject) {
            @Override
            protected void onExecuteFail(Exception e) {
                String errorMsg = "Error by xslt transformation " + processInfoObject + System.lineSeparator() + e.getMessage();
                log.error("error==" + errorMsg);
            }

            @Override
            protected boolean executeProcess() throws MessageStackException {
                XsltTransformer xsltTransformer = new XsltTransformer();
                xsltTransformer.setFileWorker(fileWorker);
                TransformerFactory transformerFactory = createTransformerFactory();
                log = initLogger(logLevel, logReference, logAdditional);
                log.debug("begin");
                boolean result = false;
                try {
                    result = xsltTransformer.transformProcessObject(transformerFactory, processInfoObject, logLevel, logReference);
                } catch (MessageStackException e) {
                    throw e;
                } finally {
                    MyURIResolver uriResolver = (MyURIResolver) transformerFactory.getURIResolver();
                    uriResolver.closeAllSubStyleReaders();
                }
                log.debug("end");
                return result;
            }
        };
    }

    // is used in case of inner included xslt
    class MyURIResolver implements URIResolver {
        private ArrayList<Reader> subStyleReaders = new ArrayList<>();

        StreamSource getSubStylesheet(String name) {
            Reader reader = null;
            try {
                File subStylesheetFile = fileWorker.getFileFromDb(DbFileSupplier.FileType.TRANSFORMER,
                        jobId + keyDelimiter + name, logLevel, logReference);
                reader = new FileReader(subStylesheetFile);
                StreamSource result = new StreamSource(reader);
                result.setSystemId(name);
                subStyleReaders.add(reader);
                return result;
            } catch (Exception e) {
                System.err.println("Exception in  " + getClass().getCanonicalName() +
                        " in method " + Thread.currentThread().getStackTrace()[2].getMethodName() + ", " + e);
                if (reader != null) {
                    try {
                        reader.close();
                    } catch (IOException e1) {
                        e1.printStackTrace();// is in the logbecauase error stream is redirected to the log, see initErrorStream
                    }
                }
            }
            return null;
        }

        //base the name of the actual file, href included xslt
        public Source resolve(String href, String base) throws TransformerException {
            Source xsltSource = null;
            IsrServerLogger log = initLogger(logLevel, logReference, logAdditional);
            xsltSource = getSubStylesheet(href);
            log.debug("end resolve for ==" + href);
            return xsltSource;
        }

        public void closeAllSubStyleReaders() {
            for (Reader reader : subStyleReaders) {
                try {
                    reader.close();
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
        }
    }

    public TransformerFactory createTransformerFactory() {
        System.setProperty("javax.xml.transform.TransformerFactory", "net.sf.saxon.TransformerFactoryImpl");
        TransformerFactory transformerFactory = TransformerFactory.newInstance();
        transformerFactory.setURIResolver(new MyURIResolver());
        return transformerFactory;
    }

    private String createErrorMessage(String errorMessage) {
        return null;
    }

    private String createSuccessMessage() {
        return null;
    }

    public FileSupplier getFileWorker() {
        return fileWorker;
    }

    public void setFileWorker(DbFileSupplier fileWorker) {
        this.fileWorker = fileWorker;
    }
}
