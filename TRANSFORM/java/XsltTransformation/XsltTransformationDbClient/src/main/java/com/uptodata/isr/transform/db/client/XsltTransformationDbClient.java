package com.uptodata.isr.transform.db.client;

import com.uptodata.isr.db.connection.ConnectionUtil;
import com.uptodata.isr.db.trace.DbLogging;
import com.uptodata.isr.utils.ConvertUtils;
import com.uptodata.isr.utils.exceptions.MessageStackException;
import com.uptodata.isr.utils.ws.SOAPUtil;
import com.uptodata.isr.utils.xml.dom.XmlHandler;
import org.apache.commons.codec.binary.Base64;
import org.apache.commons.lang.StringUtils;

import java.sql.Clob;
import java.sql.Connection;


/**
 * Created by schroedera85 on 30.09.2015.
 */
public class XsltTransformationDbClient {
    private static DbLogging log;

    //    final static String context = "ws/remote/isr/uptodata/com";
    final static String targetNamespace = "http://ws.transform.isr.uptodata.com/";
    static XmlHandler inputXmlHandler;
    private static String methodName = "transform";

    public static Clob transform(Clob clobXml, String logLevel, String logReference, int timeout) throws Exception {
        initLogger();
        log.stat("begin", "timeout = " + timeout);
        try {
            byte[] inputXml = ConvertUtils.getBytes(clobXml);
            String result = sendRequestToServer(inputXml, methodName, logLevel, logReference, timeout);
            log.stat("end", result);
            return null;
        } catch (MessageStackException e) {
            Connection conn = ConnectionUtil.getDefaultConnection();
            if (conn == null) {
                throw new MessageStackException(e, MessageStackException.SEVERITY_CRITICAL, "database connection is impossible",
                        MessageStackException.getCurrentMethod());
            }
            byte[] exBytes = e.toXml().xmlToString().getBytes();
            Clob cl = ConvertUtils.getClob(conn, exBytes);
            log.stat("end", "error");
            return cl;
        }
    }


    private static void initLogger() throws MessageStackException {
        log = new DbLogging();
    }

    private static String sendRequestToServer(byte[] inputXml, String methodName, String logLevel,
                                              String logReference, int timeout) throws MessageStackException {
        initLogger();
        log.stat("begin", methodName);
        inputXmlHandler = new XmlHandler(inputXml);
        String host = inputXmlHandler.findXpath("//HOST", "STRING").toString();
        String port = inputXmlHandler.findXpath("//PORT", "STRING").toString();
        String context = inputXmlHandler.findXpath("//CONTEXT", "STRING").toString();
        String xmlConfig = inputXmlHandler.findXpath("//servletConfiguration/xmlConfig", "STRING").toString();
        String xsltConfig = inputXmlHandler.findXpath("//servletConfiguration/xsltConfig", "STRING").toString();
        String result = inputXmlHandler.findXpath("//servletConfiguration/result", "STRING").toString();
        String jobId = inputXmlHandler.findXpath("//jobId", "STRING").toString();
        String transformNumber = inputXmlHandler.findXpath("//transformNumber", "STRING").toString();

        log.info("Webservice url", "http://" + host + ":" + port + context);
        String[] args = new String[7];
        args[0] = Base64.encodeBase64String(inputXml);
        args[1] = xmlConfig;
        args[2] = xsltConfig;
        args[3] = result;
        args[4] = logLevel;
        args[5] = logReference;
        args[6] = jobId + (StringUtils.isNotEmpty(transformNumber) ? "_" + transformNumber : "");

        String url = "http://" + host + ":" + port + context;
        log.debug("send request to  ", url + "; method: " + methodName);
        String sent = SOAPUtil.callMethod(url, targetNamespace, args, methodName, timeout);
        log.stat("end", "fileTransferId=" + sent);
        return sent;
    }


    public static void main(String[] args) {
        try {

            byte[] inputXml = ConvertUtils.fileToBytes(System.getProperty("user.dir") + "\\input1.xml");
            String res = sendRequestToServer(inputXml, methodName, "DEBUG", "test", 30);

            log.debug("result", res);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}
