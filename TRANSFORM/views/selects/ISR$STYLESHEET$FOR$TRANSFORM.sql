CREATE OR REPLACE VIEW ISR$STYLESHEET$FOR$TRANSFORM (JOBID, STYLESHEETID, FILENAME, BOOKMARKNAME, EXTENSION, STYLESHEET, STRUCTURED, DOC_DEFINITION) AS select j.jobid
       , s.stylesheetid
       , s.filename
       , case when structured in ('X', 'S', 'A') then ot.bookmarkname else '' end
       , s.extension
       , case
           when structured = 'I' then stylesheet
           when structured = 'B' then isr$transform.generateBStylesheet (
                                        case when STB$JOB.GetJobParameter('NODETYPE', STB$JOB.GetJobID) = 'REPORT_WORD' then STB$UTIL.GetRepTypeParameter(STB$OBJECT.GetCurrentReportTypeId, 'MASTERTEMPLATE_WORD')
                                             else STB$JOB.getJobParameter ('MASTERTEMPLATE', j.jobid)
                                        end
                                      )
           when structured in ('X', 'S', 'A') then isr$transform.replaceVariables (stylesheet, 
                                                     case when STB$JOB.GetJobParameter('NODETYPE', STB$JOB.GetJobID) = 'REPORT_WORD' then STB$UTIL.GetRepTypeParameter(STB$OBJECT.GetCurrentReportTypeId, 'MASTERTEMPLATE_WORD')
                                                          else STB$JOB.getJobParameter ('MASTERTEMPLATE', j.jobid)
                                                     end
                                                   )
         end stylesheet
       , structured
       , DOC_DEFINITION
    from isr$stylesheet s, isr$output$transform ot, stb$jobsession j
   -- here is cross join because jobid is needed. The view is always called with parameter jobid
   where ot.stylesheetid = s.STYLESHEETID
     and ot.outputid = STB$JOB.getJobParameter ('OUTPUTID', j.jobid)
     and (ot.keyname is null
       or ot.keyname in (select key
                           from isr$crit
                          where repid = STB$JOB.getJobParameter ('REPID', j.jobid)
                            and entity = STB$UTIL.getReptypeParameter (STB$JOB.getJobParameter ('REPORTTYPEID', j.jobid), 'STYLESHEETENTITY')))
     --is,13.Apr 2016 ardis-533
     and (ot.recreate = 'T'
       or (ot.recreate = 'F'
       and (case when ot.bookmarkname = 'NOT_DEFINED' then null else ot.bookmarkname end || '.' || s.extension not in
                (select filename
                   from isr$report$file
                  where repid in (select repid
                                    from stb$report
                                  start with repid = STB$JOB.getJobParameter ('REPID', j.jobid)
                                  connect by prior parentrepid = repid)))
        or STB$JOB.getJobParameter ('TOKEN', j.jobid) = 'CAN_OFFSHOOT_DOC')
       or STB$UTIL.getReptypeParameter (STB$JOB.getJobParameter ('REPORTTYPEID', j.jobid), 'REQUIRES_VERSIONING') = 'F')