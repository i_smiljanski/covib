CREATE OR REPLACE VIEW ISR$TRANSFORM$V (JOBID, XSLT_NAME, BOOKMARKNAME, RESULTNAME, STYLESHEETID, PARAMETERS, EXECUTIONORDER, BASESTYLESHEET, ACTION, DOC_DEFINITION) AS select jobid
       , s.filename
       , ot.bookmarkname
       , NVL (case when ot.bookmarkname = 'NOT_DEFINED' then null else bookmarkname end, SUBSTR (filename, 0, INSTR (filename, '.') - 1)) || '.' || extension resultname
       , s.stylesheetid
       , ot.parameters
       , ot.executionorder
       , ot.basestylesheet
       , ot.action
       , doc_definition
    from ISR$STYLESHEET s, ISR$OUTPUT$TRANSFORM ot, stb$jobsession j
   where ot.outputid = STB$JOB.getJobParameter ('OUTPUTID', j.jobid)
     and ot.stylesheetid = s.STYLESHEETID
     and structured in ('X', 'S', 'A')
     and (ot.keyname is null
       or ot.keyname in (select key
                           from isr$crit
                          where repid = STB$JOB.getJobParameter ('REPID', j.jobid)
                            and entity = STB$UTIL.getReptypeParameter (STB$JOB.getJobParameter ('REPORTTYPEID', j.jobid), 'STYLESHEETENTITY')))
     and (ot.recreate = 'T'
       or (ot.recreate = 'F'
       and (ot.bookmarkname || '.' || s.extension not in (select filename
                                                            from isr$report$file
                                                           where repid in (select repid
                                                                             from stb$report
                                                                           start with repid = STB$JOB.getJobParameter ('REPID', j.jobid)
                                                                           connect by prior parentrepid = repid)))
        or STB$JOB.getJobParameter ('TOKEN', j.jobid) = 'CAN_OFFSHOOT_DOC')
       or STB$UTIL.getReptypeParameter (STB$JOB.getJobParameter ('REPORTTYPEID', j.jobid), 'REQUIRES_VERSIONING') = 'F') 