CREATE OR REPLACE PACKAGE isr$transform
is


  cnCurrentJobId          NUMBER;

-- ***************************************************************************************************
-- Description/Usage:
-- The package holds the functinality to transform a raw data file into a raw report
-- ***************************************************************************************************

function callJavaTransformForJob( cnJobid          in number,
                                  csRawXmlDocumenttype  in varchar2,
                                  cnExecutionorder in number,
                                  csDocumentType   in varchar2 default 'TRANSFORMATION',
                                  csTransformConfigName in varchar2 default 'TRANSFORM_STYLESHEET',
                                  csResultFileName in varchar2 default null) return stb$oerror$record;
-- ***************************************************************************************************
-- Date and Autor: 12 Jan 2016 is
-- calls the transformation on the server for all stylesheets with given executionorder.
-- example: oErrorObj := isr$transform.callJavaTransformForJob(cnJobid, 'IMAGES_LIST_' || rGetPdfs1.value, null, 
--                                'ATTACHMENTHTML', 'TRANSFORM_STYLESHEET', rGetPdfs1.value || '.HTM');
--
-- Parameter:
-- cnJobid               the identifier of the iStudyReporter job
-- csRawXmlDocumenttype  documenttype of input xml (or raw data xml) for get it from 'STB$JOBTRAIL' 
-- cnExecutionorder      the stylesheet executionorder in isr$output$transform; if null, it is calculated
-- csDocumentType        documenttype of result document for save to 'STB$JOBTRAIL'
-- csTransformConfigName configuration name of 'transformers' (stylesheets)  from table 'ISR$FILE$TRANSFER$CONFIG'
-- csResultFileName      file name of result document for save to 'STB$JOBTRAIL'; if null, it is resultname from isr$transform$v
--
-- Return:
-- STB$OERROR$RECORD
--
-- ***************************************************************************************************


function callJavaTransform(  xConfigXml in xmltype, timeout in number) return stb$oerror$record;
-- ***************************************************************************************************
-- Date and Autor: 28 Apr 2016 is
-- calls the transformation on the server for all stylesheets with given executionorder.
--
-- Parameter:
-- xConfigXml     cofiguration file with all informations for for transformation
-- timeout server timeout
--
-- Return:
-- STB$OERROR$RECORD
--
-- ***************************************************************************************************



FUNCTION performTransformation (cnJobid in NUMBER, cxXml IN OUT XMLTYPE) RETURN STB$OERROR$RECORD;
-- ***************************************************************************************************
-- Date and Autor: 03 Feb 2006 JB
-- Starts the transformation of the raw data file.
-- There can be a set of transformations all working on the raw data file.
-- The result can be any kind of type jpg, xml or html file.
--
-- Parameter:
-- cnJobid     the identifier of the iStudyReporter job
-- cnDomId     the domid of the configuration file
--
-- Return:
-- STB$OERROR$RECORD
--
-- ***************************************************************************************************

function getIncludeStylesheet(csFileName IN VARCHAR2) return CLOB;
-- *********************************************************************************************************************
-- Date and Autor: 03. Nov 2009 MCD
-- gets the stylesheet for xsl:include
--
-- Parameter:
-- csFileName    the filename of the stylesheet which should be included
--
-- Return:
-- the include stylesheet
--
-- *********************************************************************************************************************

function getIncludeStylesheet(csFileName IN VARCHAR2, cnJobId IN NUMBER) return CLOB;
-- *********************************************************************************************************************
-- Date and Autor: 02. Sep 2011 IS
-- gets the stylesheet for xsl:include
--
-- Parameter:
-- csFileName    the filename of the stylesheet which should be included
-- cnJobId
--
-- Return:
-- the include stylesheet
--
-- *********************************************************************************************************************

function generateBStylesheet(sFileId in varchar2) return clob;

-- *********************************************************************************************************************
function replaceVariables(clXSLT IN  CLOB, csFileId IN NUMBER) return clob;

-- *********************************************************************************************************************
FUNCTION transform( csInputXml IN CLOB, csLoglevel  IN VARCHAR2,  csLogReference  IN VARCHAR2, nTimeout in number )  RETURN CLOB;
-- ***************************************************************************************************
END isr$transform;