CREATE OR REPLACE PACKAGE BODY isr$transform
is

  cnTransformNumber       NUMBER := 0;

--******************************************************************************
function replaceVariables(clXSLT IN  CLOB, csFileId IN NUMBER) return clob is
  sRetXslt   clob := clXSLT;
  sCurrentName   constant varchar2(4000) := $$PLSQL_UNIT||'.replaceVariables('||' '||')';
begin
  isr$trace.stat('begin', 'begin', sCurrentName);
  for rGetReplaceVariables in ( SELECT entity
                                     , display
                                     , info
                                     , use
                                     , ordernumber
                                     , VALUE
                                  FROM isr$file$parameter$values rpv
                                 WHERE rpv.fileid = csFileId
                                   AND rpv.entity = 'STYLESHEET$REPLACE$VARIABLES'
                                   AND rpv.use = 'T'
                                ORDER BY rpv.ordernumber) loop
    sRetXslt := replace(clXSLT, rGetReplaceVariables.display, rGetReplaceVariables.info);
  end loop;
  isr$trace.stat('end', 'end', sCurrentName);
  return sRetXslt;

end replaceVariables;

--******************************************************************************-******************************************************************************
function getIncludeStylesheet(csFileName IN VARCHAR2) return CLOB
is
  clInclude  CLOB;

  sCurrentName constant varchar2(100) := $$PLSQL_UNIT||'.getIncludeStylesheet';
  -- look in stylesheet table
  CURSOR cGetStylesheet is
    SELECT s.stylesheet
      FROM ISR$STYLESHEET s, ISR$OUTPUT$TRANSFORM ot
     WHERE ot.outputid = STB$JOB.getJobParameter ('OUTPUTID', cnCurrentJobId)
       AND ot.stylesheetid = s.STYLESHEETID
       AND UPPER(s.filename) = UPPER(csFileName)
       AND s.structured = 'I';

  -- look in jobtrail table
  CURSOR cGetFile is
    SELECT textfile
      FROM STB$JOBTRAIL jt
     WHERE UPPER(jt.filename) = UPPER(csFileName)
       AND jt.jobid = cnCurrentJobId;
begin
 isr$trace.stat('begin', 'begin', sCurrentName);
 isr$trace.debug('cnCurrentJobId',cnCurrentJobId,sCurrentName);

  OPEN cGetStylesheet;
  FETCH cGetStylesheet INTO clInclude;
  IF cGetStylesheet%NOTFOUND THEN
    OPEN cGetFile;
    FETCH cGetFile INTO clInclude;
    CLOSE cGetFile;
  END IF;
  CLOSE cGetStylesheet;

  IF NVL(DBMS_LOB.getLength(clInclude), 0) = 0 THEN
    RAISE NO_DATA_FOUND;
  END IF;

  isr$trace.debug(csFileName,DBMS_LOB.getLength(clInclude),sCurrentName,clInclude);
  isr$trace.stat('end', 'end', sCurrentName);
  RETURN clInclude;
end getIncludeStylesheet;

-- *****************************************************************************
function getIncludeStylesheet(csFileName IN VARCHAR2, cnJobId IN NUMBER) return CLOB
IS
  sCurrentName   constant varchar2(4000) := $$PLSQL_UNIT||'.getIncludeStylesheet('||' '||')';
BEGIN
  isr$trace.info('begin', 'begin', sCurrentName);
  cnCurrentJobId := cnJobId;
  isr$trace.info('end', 'end', sCurrentName);
  RETURN getIncludeStylesheet(csFileName);
END getIncludeStylesheet;

--******************************************************************************
function callJavaTransformForJob( cnJobid          in number,
                                  csRawXmlDocumenttype  in varchar2,
                                  cnExecutionorder in number,
                                  csDocumentType   in varchar2 default 'TRANSFORMATION',
                                  csTransformConfigName in varchar2 default 'TRANSFORM_STYLESHEET',
                                  csResultFileName in varchar2 default null)
  return stb$oerror$record
 is
  sCurrentName constant varchar2(100) := $$PLSQL_UNIT||'.callJavaTransformForJob('||cnJobid||')';

  oErrorObj               STB$OERROR$RECORD := STB$OERROR$RECORD();
  sMsg    varchar2(4000);
  xConfigXml             xmltype;

  sStylesheetDelimiter  isr$transfer$file$config.configname%type;
  sRawdatatDelimiter  isr$transfer$file$config.configname%type;
  sResultDelimiter  isr$transfer$file$config.configname%type;

  cursor cGetTransformServer is
  select observer_host, port, context, timeout, alias
  from isr$serverobserver$v
  where servertypeid = 12 and nvl(runstatus, 'F') = 'T';

  rGetTransformServer cGetTransformServer%rowtype;

  cursor cStylesheetCnt is
    select count(*) cnt
      from isr$transform$v tr
     where jobid = cnJobid
       and NVL (executionorder, -1) = NVL (cnExecutionorder, -1);

  rStylesheetCnt cStylesheetCnt%rowtype;

  cursor cGetConfigXml is
    select XMLELEMENT (
             "transformations"
           , XMLELEMENT ("jobId", jobid)
           , XMLELEMENT ("serverName", rGetTransformServer.alias)
           , XMLELEMENT ("transformNumber", cnTransformNumber)
           , XMLELEMENT ("keyDelimiter", sStylesheetDelimiter)
           , XMLELEMENT ("HOST", rGetTransformServer.observer_host)
           , XMLELEMENT ("PORT", rGetTransformServer.port)
           , XMLELEMENT ("CONTEXT", rGetTransformServer.context)
           , XMLELEMENT ("servletConfiguration"
                       , XMLELEMENT ("xmlConfig", 'TRANSFORM_RAW_XML')
                       , XMLELEMENT ("xsltConfig", csTransformConfigName)
                       , XMLELEMENT ("result", 'TRANSFORM_RESULT'))
           , XMLAGG (XMLELEMENT ("xsltTransformation"
                               , XMLELEMENT ("xml", jobid || sRawdatatDelimiter || csRawXmlDocumenttype)
                               , XMLELEMENT ("xslt", jobid || sStylesheetDelimiter || xslt_name || sStylesheetDelimiter || bookmarkname)
                               , XMLELEMENT ("result", jobid || sResultDelimiter || nvl(csResultFileName, resultname) || sResultDelimiter || csDocumentType || sResultDelimiter || tr.stylesheetid || sResultDelimiter || tr.doc_definition)
                               , XMLELEMENT ("transformationParameters", XMLCDATA (parameters))))
           )
             configXml
      from isr$transform$v tr
     where jobid = cnJobid
       and NVL (executionorder, -1) = NVL (cnExecutionorder, -1)
       -- ISRC-550: existing files in ISR$REPORT$FILE won't be transformed
     /*  and tr.resultname not in (select filename
                                   from ISR$REPORT$FILE$V
                                  where jobid = cnJobid)*/
    group by jobid;

    exTransformServerNotFound  exception;
    exStylesheetNotFound  exception;

begin
  isr$trace.stat('begin','csRawXmlDocumenttype:'||csRawXmlDocumenttype||',  cnExecutionorder: '||cnExecutionorder||',  csDocumentType: '||csDocumentType,sCurrentName);

  cnTransformNumber := cnTransformNumber + 1;

  sStylesheetDelimiter  :=  isr$file$transfer$service.getKeyDelimiter(csTransformConfigName);
  sRawdatatDelimiter  :=  isr$file$transfer$service.getKeyDelimiter('TRANSFORM_RAW_XML');
  sResultDelimiter  :=  isr$file$transfer$service.getKeyDelimiter('TRANSFORM_RESULT');

  open cStylesheetCnt;
  fetch cStylesheetCnt into rStylesheetCnt;
  if cStylesheetCnt%notfound or rStylesheetCnt.cnt = 0 then
    close cStylesheetCnt;
    sMsg :=  Utd$msglib.GetMsg('exStylesheetNotFoundText',Stb$security.GetCurrentLanguage, 'executionorder=' || cnExecutionorder);
    raise exStylesheetNotFound;
  end if;

  open cGetTransformServer;
  fetch cGetTransformServer into rGetTransformServer;
  if cGetTransformServer%notfound then
    close cGetTransformServer;
    sMsg :=  Utd$msglib.GetMsg('exTransformServerNotFoundText',Stb$security.GetCurrentLanguage);
    raise exTransformServerNotFound;
  end if;

  --xConfigXml
  open cGetConfigXml;
  fetch cGetConfigXml into xConfigXml;
  if cGetConfigXml%found then
    isr$trace.debug('xConfigXml','s. logclob', sCurrentName, xConfigXml);

    oErrorObj := callJavaTransform( xConfigXml, rGetTransformServer.timeout);

    update stb$jobtrail
    set docsize = dbms_lob.getlength (textfile), download = 'T', blobflag='C'
    where documenttype = csDocumentType
    and jobid = cnJobid
    and blobflag is null
    and download is null;

  end if;
  close cGetConfigXml;
  close cGetTransformServer;

  isr$trace.stat('end','end', sCurrentName);
  return oErrorObj;
exception
   when exStylesheetNotFound then  --ISRC-1054
    oErrorObj.handleError ( Stb$typedef.cnSeverityWarning, sMsg, sCurrentName,
                           dbms_utility.format_error_stack || CHR(10) || dbms_utility.format_error_backtrace);
    return oErrorObj;
   when others then
    oErrorObj.handleError ( Stb$typedef.cnSeverityCritical, sMsg, sCurrentName,
                           dbms_utility.format_error_stack || CHR(10) || dbms_utility.format_error_backtrace);
    return oErrorObj;
end callJavaTransformForJob;


--******************************************************************************
function callJavaTransform( xConfigXml in xmltype, timeout in number) return stb$oerror$record
 is
  sCurrentName constant varchar2(100) := $$PLSQL_UNIT||'.callJavaTransform';
  sRemoteEx              clob;
  oErrorObj               STB$OERROR$RECORD := STB$OERROR$RECORD();
  sMsg    varchar2(4000);

begin
  isr$trace.stat('begin','java log level: ' || isr$trace.getJavaUserLoglevelStr,  sCurrentName);

  sRemoteEx := isr$transform.transform( xConfigXml.getclobval(), isr$trace.getJavaUserLoglevelStr, isr$trace.getReference, timeout);

  if sRemoteEx is not null then
   isr$trace.error('transform error', 'error ', sCurrentName, sRemoteEx);
   oErrorObj.handleJavaError( Stb$typedef.cnSeverityCritical,  sRemoteEx, sCurrentName );
  end if;

  isr$trace.stat('end','end', sCurrentName);
  return oErrorObj;
exception
   when others then
    oErrorObj.handleError ( Stb$typedef.cnSeverityCritical, sMsg, sCurrentName,
                           dbms_utility.format_error_stack || CHR(10) || dbms_utility.format_error_backtrace);
    return oErrorObj;
end callJavaTransform;
--******************************************************************************

FUNCTION performTransformation (cnJobid in NUMBER, cxXml IN OUT XMLTYPE) RETURN STB$OERROR$RECORD
is
  sCurrentName constant varchar2(100) := $$PLSQL_UNIT||'.performTransformation';
  sMsg    varchar2(4000);
  oErrorObj               STB$OERROR$RECORD := STB$OERROR$RECORD();

  -- information for error case
  sIp                     STB$JOBSESSION.IP%TYPE;
  sPort                   STB$JOBSESSION.PORT%TYPE;

BEGIN
  isr$trace.stat('begin','cnJobID: ' || cnJobID, sCurrentName);

  cnCurrentJobId := cnJobID;

  insert into STB$JOBTRAIL (jobid
                          , textfile
                          , docsize
                          , documenttype
                          , doc_definition
                          , reference
                          , mimetype
                          , filename
                          , blobFlag
                          , download)
    select cnJobid
         , stylesheet
         , DBMS_LOB.getLength (stylesheet)
         , 'STYLESHEET'
         , doc_definition
         , STB$JOB.getJobParameter ('REFERENCE', cnJobId)
         , 'text/css'
         , filename
         , 'C'
         , Stb$util.getSystemParameter ('DOWNLOADFLAG')
      from isr$stylesheet$for$transform
     where jobid = cnJobid
       and structured = 'B';

  -- ISRC-550: existing files in ISR$REPORT$FILE have to be in jobtrail
 /* insert into STB$JOBTRAIL (jobid
                          , textfile
                          , docsize
                          , documenttype
                          , reference
                          , mimetype
                          , filename
                          , blobFlag
                          , download)
    select cnJobid
         , textfile
         , DBMS_LOB.getLength (textfile)
         , 'TRANSFORMATION'
         , STB$JOB.getJobParameter ('REFERENCE', cnJobId)
         , 'text/css'
         , filename
         , 'C'
         , 'T'
      from ISR$REPORT$FILE$V
     where jobid = cnJobid;*/

  oErrorObj := isr$transform.callJavaTransformForJob(cnJobid, 'ROHDATEN', null); -- only stylesheets without executionorder. They can be transformed parallel. The stylesheets with executionorder have to be processed  extra, e.g. graphics in separate action.

  if oErrorObj.sSeverityCode = Stb$typedef.cnSeverityCritical then
    SELECT ip, To_Char(port) INTO sIp, sPort FROM STB$JOBSESSION WHERE jobid = cnJobId;
    Stb$job.markDBJobAsBroken(cnJobId,sIp, sPort,oErrorObj.sErrorCode, oErrorObj.sErrorMessage, oErrorObj.sSeverityCode);
  end if;

  isr$trace.stat('end','end', sCurrentName);
  RETURN oErrorObj;

EXCEPTION
   when others then
    oErrorObj.handleError ( Stb$typedef.cnSeverityCritical, sMsg, sCurrentName,
                           dbms_utility.format_error_stack || CHR(10) || dbms_utility.format_error_backtrace);
    SELECT ip, To_Char(port) INTO sIp, sPort FROM STB$JOBSESSION WHERE jobid = cnJobId;
    Stb$job.markDBJobAsBroken(cnJobId,sIp, sPort,oErrorObj.sErrorCode, oErrorObj.sErrorMessage, oErrorObj.sSeverityCode);
    RETURN oErrorObj;
END performTransformation;

function generateBStylesheet(sFileId in varchar2) return clob
is
  sCurrentName   constant varchar2(4000) := $$PLSQL_UNIT||'.generateBStylesheet('||sFileId||')';
  clCss  clob;
begin
  isr$trace.stat('begin', 'begin', sCurrentName);
  clCss := '<xsl:stylesheet version="2.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">'||chr(10)||
           '<!-- FileID: '||sFileID||' -->'||chr(10)||
           '  <xsl:template name="css">'||chr(10)||
           '    <style type="text/css">'||chr(10);
  FOR rGetStyles IN ( SELECT DISTINCT entity
                        FROM isr$file$style$values
                       WHERE fileid = sFileId
                         AND use = 'T'
                       ORDER BY entity) LOOP
    clCss :=  clCss||'      '||rGetStyles.entity||' {'||chr(10);
    FOR rGetStyleDesign IN (SELECT *
                              FROM isr$file$style$values
                             WHERE fileid = sFileId
                               AND entity = rGetStyles.entity
                               AND use = 'T'
                             ORDER BY display) LOOP
      clCss :=  clCss||'        '||rGetStyleDesign.value||': '||rGetStyleDesign.display||';'||chr(10);
    END LOOP;
    clCss :=  clCss||'      }'||chr(10);
  END LOOP;
  clCss :=  clCss||'    </style>'||chr(10)||
                   '  </xsl:template>'||chr(10)||
                    '</xsl:stylesheet>';
 isr$trace.stat('end', 'end', sCurrentName);
 return clCss;
end generateBStylesheet;


FUNCTION transform( csInputXml IN CLOB, csLoglevel  IN VARCHAR2,  csLogReference  IN VARCHAR2, nTimeout in number )  RETURN CLOB
AS LANGUAGE JAVA
NAME 'com.uptodata.isr.transform.db.client.XsltTransformationDbClient.transform( java.sql.Clob, java.lang.String,  java.lang.String, int) return java.sql.Clob';

END isr$transform;