CREATE OR REPLACE PACKAGE BODY             isr$pdfimport
is
  exErrorOccurred  EXCEPTION;
  cnTransformNumber       NUMBER := 0;

function callJavaPdfImport 
  return stb$oerror$record
is
  oErrorObj        STB$OERROR$RECORD := STB$OERROR$RECORD();
  sUserMsg         VARCHAR2(4000):= 'pdfimport ';  
  sCurrentName     constant VARCHAR2(100) := $$PLSQL_UNIT||'.callJavaPdfImport';
  xConfigXml       clob;
  sRemoteEx        clob;
  
  cursor cGetConfigXml is select textfile from stb$jobtrail where jobid=5000 and filename='pdfconfig.xml';
    
begin
  isr$trace.stat('begin', 'begin', sCurrentName);
    open cGetConfigXml;
    fetch cGetConfigXml into xConfigXml;
    if cGetConfigXml%NOTFOUND THEN
    sUserMsg:= sCurrentName||'is not valid file/file could not be read';
     RAISE exErrorOccurred;
    end if;
    isr$trace.info('nUserNo', sUserMsg, sCurrentName);
  --xConfigXml:= (select binaryfile from stb$jobtrail where jobid=5000 and filename='pdfConfig.xml');
 
     sRemoteEx := isr$pdfimport.convertPdfToImage( xConfigXml, 'DEBUG','test', 300);
    
      isr$trace.stat('begin','callJavaToPdfs', sCurrentName,sRemoteEx);
      close cGetConfigXml;
      isr$trace.info('isr$pdfimport', 'sRemoteEx',sCurrentName);
      EXCEPTION WHEN OTHERS THEN
    
  isr$trace.stat('end', 'end', sCurrentName);
  return oErrorObj;
end callJavaPdfImport;
  
    FUNCTION convertPdfToImage( csInputXml IN CLOB, csLogLevel IN VARCHAR2, csLogReference  IN VARCHAR2, nTimeout in number )  RETURN CLOB
AS LANGUAGE JAVA
NAME 'com.uptodata.isr.pdfimport.PdfImportDbClient.convertPdfToImage( oracle.sql.CLOB, java.lang.String, java.lang.String, int) return oracle.sql.CLOB';
  -- public static String sendRequestToServer(byte[] inputXml, String methodName, String logLevel,
     --                                         String logReference, int timeou
  end isr$pdfimport;