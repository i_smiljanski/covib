CREATE OR REPLACE PACKAGE isr$pdfimport
is


  cnCurrentJobId          NUMBER;

-- ***************************************************************************************************
-- Description/Usage:
-- The package holds the functinality to converting pdf to image
-- ***************************************************************************************************

function callJavaPdfImport return stb$oerror$record;
FUNCTION convertPdfToImage( csInputXml IN CLOB, csLogLevel IN VARCHAR2, csLogReference  IN VARCHAR2, nTimeout in number )  RETURN CLOB;

end isr$pdfimport;