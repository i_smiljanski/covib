CREATE OR REPLACE package body ISR$MDM
is
  exMetaNoKey               exception;
  exCouDoubletVariants      exception;
  exPkColumnList            exception;
  exCreateTable             exception;
  exAttributesMissing       exception;
  exCheckSum                exception;
  exNoTable                 exception;
  exNoTokenHandle           exception;
  exVariantProcessing       exception;
  exClearDoublets           exception;
  exCreateAudit             exception;
  exUnknownNodetype         exception;
  exUnexpectedError         exception;
  exNotExists               exception;
  exLoopOverflow            exception;
  nCurrentLanguage          constant number       := stb$security.getCurrentLanguage;
  crlf                      constant varchar2(2)  := chr(10)||chr(13);
  csTmpTabPrefix            constant varchar2(50) := 'TMP$';
  csMdmTabPrefix            constant varchar2(50) := 'MDM$';
  csUser                    varchar2(50)          := nvl(stb$security.getCurrentUserName,user)||'('||stb$security.getCurrentUser||')';
  sCallback                 varchar2(32000)       := null;

  cursor curGetMdmEntity( csFilter varchar2, csOrderPriority varchar2 default null) is   -- set filter to 'ENABLED' or 'DISABLED' or 'BOTH'
    select ent.entityname, nvl(ent.mdm,'F') mdm
    from   isr$meta$entity ent
    join   ( select distinct entityname from isr$server$entity$mapping )  mapp
    on     ent.entityname = mapp.entityname
    where  csFilter = 'BOTH'
    or     csFilter = 'ENABLED' and ent.mdm = 'T'
    or     csFilter = 'DISABLED' and nvl(ent.mdm,'F') = 'F'
    order by decode(ent.entityname, csOrderPriority, 1, 2), 1;

  cursor curNlsSessionParameters is
    select parameter, value
    from   v$nls_parameters
    where  parameter in( 'NLS_DATE_FORMAT', 'NLS_TIMESTAMP_FORMAT', 'NLS_LANGUAGE', 'NLS_CALENDAR', 'NLS_DATE_LANGUAGE', 'NLS_TIME_FORMAT' );

  -- current state, used for restore
  type typNlsSessionParameters is table of curNlsSessionParameters%ROWTYPE;
  olNlsSessionParameters typNlsSessionParameters  := typNlsSessionParameters();

  -- within MDM, a primary key is mandatory for an entity [ISRC-109]
  cursor cPrimaryKeys(sTableName VARCHAR2) IS
    select distinct cols.column_name
    from   all_constraints cons, all_cons_columns cols
    where  cols.table_name = sTableName
    and    cons.constraint_type = 'P'
    and    cons.constraint_name = cols.constraint_name
    and    cons.owner = cols.owner
    order by 1;


--*****************************************************************************************************************************
function reCalcChecksum ( csTableName in varchar2 default null )
  return STB$OERROR$RECORD
is
  oErrorObj      STB$OERROR$RECORD := STB$OERROR$RECORD();
  sCurrentName   constant varchar2(100) := $$PLSQL_UNIT||'.reCalcChecksum('||csTableName||')';
  sEntity        varchar2(100);
  sSql           varchar2(32000);
begin
  isr$trace.stat('begin', 'begin', sCurrentName);

  if csTableName is null then
    -- processing for all MDM-Tables
    for recTables in ( select table_name from user_tables where table_name like 'MDM$%' )
    loop
      sEntity := substr(recTables.table_name,5);
      sSql :=    'update '||recTables.table_name||chr(10)
              ||    'set    checksum = to_number( stb$java.calculateHash(to_clob(xmlforest('||ISR$MDM.getEntityAttributeList(sEntity)||'))), lpad(''x'',60,''x''))';   -- [ISRC-1125]
      isr$trace.debug('statement','sSql',sCurrentName,sSql);
      execute immediate sSql;
    end loop;
  else
    -- processing for one specific MDM-Table
    sEntity := substr(csTableName,5);
    sSql :=    'update '||csTableName||chr(10)
            ||    'set    checksum = to_number( stb$java.calculateHash(to_clob(xmlforest('||ISR$MDM.getEntityAttributeList(sEntity)||'))), lpad(''x'',50,''x''))';    -- [ISRC-1125]
    isr$trace.debug('statement','sSql',sCurrentName,sSql);
    execute immediate sSql;
  end if;

  isr$trace.stat('end', 'end', sCurrentName);
  return oErrorObj;
exception
  when others then
    oErrorObj.handleError( STB$TYPEDEF.cnSeverityCritical, utd$msglib.getmsg ('exUnexpectedError', nCurrentLanguage, sqlerrm(sqlcode)), sCurrentName,
                           substr(dbms_utility.format_error_stack || CHR(10) || dbms_utility.format_error_backtrace, 1,4000));
    return oErrorObj;
end reCalcChecksum;


--*********************************************************************************************************************************
function generateCanonicalQuery( cxCanonical  xmltype,
                                 csColumnList varchar2 )
  return varchar2
is
  sCurrentName  constant varchar2(100) := $$PLSQL_UNIT||'.generateCanonicalQuery()';
  oErrorObj      Stb$oerror$record := STB$OERROR$RECORD();
  sSql          varchar2(32000);
  sSqlSnippet#0 varchar2(32000);
  sSqlSnippet#1 varchar2(32000);
  sSqlSnippet#2 varchar2(32000);
  sSqlSnippet#3 varchar2(32000);
  sSqlSnippet#4 varchar2(32000);
  sReturnSql    varchar2(32000);
  sColumn       varchar2(30);
  sColumnList   varchar2(4000) := csColumnList||',';
  nCounter      number         := 0;
begin

  sSqlSnippet#0 :=     'with base_data as (  select :1 data_col     --  :1  -->  USING cxCanonical'||chr(10)
                    || '                     from   dual )';

  sSqlSnippet#1 := 'select xmldata.'|| replace(csColumnList, ',', ','||chr(10)||'       xmldata.');

  sSqlSnippet#2 := 'from base_data a,';

  sSqlSnippet#3 :=     '     xmltable( ''for $i in ROWSET/ROW return$i'''||chr(10)
                    || '     passing  data_col';


  -- process sSqlSnippet#4
  sSqlSnippet#4 := '      columns';
  while instr(sColumnList,',') > 0
  loop
    nCounter      := nCounter + 1;
    sColumn       := isr$util.getSplit(sColumnList,1);
    sColumnList   := substr(sColumnList,instr(sColumnList,',')+1);
    sSqlSnippet#4 := sSqlSnippet#4||chr(10)||'        '||rpad(sColumn,31)||'varchar2(4000) path ''./'||sColumn||'/text()'',';
    if nCounter >= 500 then
      raise exLoopOverflow;
    end if;
  end loop;

  sSqlSnippet#4 := substr(sSqlSnippet#4,1,length(sSqlSnippet#4)-1)||chr(10)||'        ) xmldata';

  /*** use for debugging *** /
  dbms_output.put_line('-------- sSqlSnippet#0 --------');
  dbms_output.put_line(sSqlSnippet#0);
  dbms_output.put_line('-------- sSqlSnippet#1 --------');
  dbms_output.put_line(sSqlSnippet#1);
  dbms_output.put_line('-------- sSqlSnippet#2 --------');
  dbms_output.put_line(sSqlSnippet#2);
  dbms_output.put_line('-------- sSqlSnippet#3 --------');
  dbms_output.put_line(sSqlSnippet#3);
  dbms_output.put_line('-------- sSqlSnippet#4 --------');
  dbms_output.put_line(sSqlSnippet#4);
  /***/

  sReturnSql :=   sSqlSnippet#0||chr(10)||sSqlSnippet#1||chr(10)||sSqlSnippet#2||chr(10)||sSqlSnippet#3||chr(10)||sSqlSnippet#4||chr(10);
  return sReturnSql;

exception
  when exLoopOverflow then
    oErrorObj.handleError( STB$TYPEDEF.cnSeverityCritical, 'exLoopOverflow', sCurrentName,
                           substr(dbms_utility.format_error_stack || CHR(10) || dbms_utility.format_error_backtrace, 1,4000));
    return 'exLoopOverflow';
  when others then
    return sqlerrm(sqlcode)||' '||chr(10)||chr(10)||dbms_utility.format_error_backtrace;

end generateCanonicalQuery;

--*****************************************************************************************************************************
function setCallback ( csFunction in varchar2 )
  return STB$OERROR$RECORD
is
  oErrorObj      Stb$oerror$record := STB$OERROR$RECORD();
  sCurrentName   constant varchar2(100) := $$PLSQL_UNIT||'.setCallback('||csFunction||')';
begin
  isr$trace.stat('begin', 'begin', sCurrentName);
  sCallback := csFunction;
  isr$trace.stat('end', 'end', sCurrentName);
  return oErrorObj;
exception
  when others then
    oErrorObj.handleError( STB$TYPEDEF.cnSeverityCritical, utd$msglib.getmsg ('exUnexpectedError', nCurrentLanguage, sqlerrm(sqlcode)), sCurrentName,
                           substr(dbms_utility.format_error_stack || CHR(10) || dbms_utility.format_error_backtrace, 1,4000));
    return oErrorObj;
end setCallback;


--*****************************************************************************************************************************
function resetCallback
  return STB$OERROR$RECORD
is
  oErrorObj      Stb$oerror$record := STB$OERROR$RECORD();
  sCurrentName   constant varchar2(100) := $$PLSQL_UNIT||'.resetCallback';
begin
  isr$trace.stat('begin', 'begin', sCurrentName);
  sCallback := null;
  return oErrorObj;
exception
  when others then
    oErrorObj.handleError( STB$TYPEDEF.cnSeverityCritical, utd$msglib.getmsg ('exUnexpectedError', nCurrentLanguage, sqlerrm(sqlcode)), sCurrentName,
                           substr(dbms_utility.format_error_stack || CHR(10) || dbms_utility.format_error_backtrace, 1,4000));
    return oErrorObj;
end resetCallback;


--****************************************************************************************************
function entityExists( csEntityName varchar2 )
  return boolean
is
  nDummy  number;
begin
  select 1
  into   nDummy
  from   isr$meta$entity
  where  entityname = csEntityName;

  return true;
exception
  when others then
    return false;
end entityExists;


--****************************************************************************************************
procedure createMdmUserRecord( csEntityName   varchar2,
                               csMdmTableName varchar2,
                               csPkValue      varchar2,
                               csPkList       varchar2,
                               csChecksum     varchar2  )
is
  sCurrentName     constant varchar2(4000) := $$PLSQL_UNIT||'.createMdmUserRecord('||csMdmTableName||','||csPkValue||')';
  sSql             varchar2(4000);
  oErrorObj        STB$OERROR$RECORD       := STB$OERROR$RECORD();
  sAttributeList   varchar2(4000);
begin
  isr$trace.stat('begin','csEntityName: '||csEntityName||',  csMdmTableName: '||csMdmTableName||',  csPkValue: '||csPkValue||',  csPkList: '||csPkList||',  csChecksum: '||csChecksum, sCurrentName);

  sAttributeList := getEntityAttributeList(csEntityName);
  if sAttributeList is null then
    raise exAttributesMissing;
  end if;

  sSql :=    ' insert into '||csMdmTableName||'('||sAttributeList||',checksum,modifiedon,modifiedby,mdm_resolution,mdm_user_record,mdm_group_accepted)'||chr(10)
          || ' select '||sAttributeList||','||-csCheckSum||',sysdate,'''||csUser||''',''F'',''T'',mdm_group_accepted from '||csMdmTableName
          || ' where  checksum = '||csChecksum;

  isr$trace.debug('statement','sSql',sCurrentName,sSql);
  execute immediate sSql;

  isr$trace.stat('end','end',sCurrentName);
exception
  when others then
    oErrorObj.handleError( STB$TYPEDEF.cnSeverityCritical, utd$msglib.getmsg ('exUnexpectedError', nCurrentLanguage, sqlerrm(sqlcode)), sCurrentName,
                           substr(dbms_utility.format_error_stack || CHR(10) || dbms_utility.format_error_backtrace, 1,4000));
end createMdmUserRecord;


--****************************************************************************************************
procedure deleteMdmUserRecord( csMdmTableName varchar2,
                               csChecksum     varchar2  )
is
  sCurrentName     constant varchar2(4000) := $$PLSQL_UNIT||'.deleteMdmUserRecord('||csMdmTableName||','||csChecksum||')';
  sSql             varchar2(4000);
  oErrorObj        STB$OERROR$RECORD       := STB$OERROR$RECORD();
  sAttributeList   varchar2(4000);
begin
  isr$trace.stat('begin','csChecksum: '||csChecksum,sCurrentName);

  sSql :=    ' delete from '||csMdmTableName||chr(10)
          || ' where  checksum = '||csChecksum||chr(10)
          || ' and    mdm_user_record = ''T''';

  isr$trace.debug('statement','sSql',sCurrentName,sSql);
  execute immediate sSql;

  isr$trace.stat('end','end',sCurrentName);
exception
  when others then
    oErrorObj.handleError( STB$TYPEDEF.cnSeverityCritical, utd$msglib.getmsg ('exUnexpectedError', nCurrentLanguage, sqlerrm(sqlcode)), sCurrentName,
                           substr(dbms_utility.format_error_stack || CHR(10) || dbms_utility.format_error_backtrace, 1,4000));
end deleteMdmUserRecord;


--****************************************************************************************************
procedure setMdmFlag( csMdmTableName   varchar2,
                      csPkValue        varchar2,
                      csPkList         varchar2,
                      csChecksum       varchar2,
                      csFlagColumnName varchar2,
                      csNewEntry       varchar2 default null,
                      cnServerId       number   default null )
is
  sCurrentName     constant varchar2(4000) := $$PLSQL_UNIT||'.setMdmFlag('||csMdmTableName||','||csPkValue||')';
  sMdmMethod       constant varchar2(50) := nvl(trim(upper(STB$UTIL.getSystemParameter('MASTER_DATA_MANAGEMENT_METHOD'))),'DISABLED');  -- 'ISR_MDM_DEFAULT','DISABLED'
  sSql             varchar2(4000);
  oErrorObj        STB$OERROR$RECORD       := STB$OERROR$RECORD();
begin
  isr$trace.stat('begin','csChecksum: '||csChecksum,sCurrentName);
  case
    when csFlagColumnName is null then
      -- no action
      isr$trace.warn('WARNING','wrong parameter found',sCurrentName);
    when csFlagColumnName = 'MDM_RESOLUTION' then
      -- reset old resolution record
      sSql :=    ' update '||csMdmTableName||chr(10)
              || ' set    MDM_RESOLUTION = ''F'','||chr(10)
              || '        modifiedon = sysdate,'||chr(10)
              || '        modifiedby = '''||csUser||''''||chr(10)
              || ' where  MDM_RESOLUTION = ''T'''||chr(10)
              || ' and    '||isr$util.getSplit(csPkList,1)||' = '''||csPkValue||''''||chr(10);

      if sMdmMethod = 'ISR_PK_CHECKSUM' then
        sSql := sSql||' and    checksum = '''||csChecksum||'''';
      end if;

      isr$trace.debug('statement','sSql',sCurrentName,sSql);
      execute immediate sSql;

      -- set new resolution record
      sSql :=    ' update '||csMdmTableName||chr(10)
              || ' set    MDM_RESOLUTION = ''T'','||chr(10)
              || '        modifiedon = sysdate,'||chr(10)
              || '        modifiedby = '''||csUser||''''||chr(10)
              || ' where  '||isr$util.getSplit(csPkList,1)||' = '''||csPkValue||''''||chr(10)
              || ' and    serverid = '||cnServerId||chr(10)       -- vs [ISRC-1153]
              || ' and    checksum = '''||csChecksum||'''';

      isr$trace.debug('statement','sSql',sCurrentName,sSql);
      execute immediate sSql;

    else
      -- regular processing (fork)
      sSql :=    ' update '||csMdmTableName||chr(10)
              || ' set    '||csFlagColumnName||' = '''||csNewEntry||''','||chr(10)
              || '        modifiedon = sysdate,'||chr(10)
              || '        modifiedby = '''||csUser||''''||chr(10)
              || ' where  '||isr$util.getSplit(csPkList,1)||' = '''||csPkValue||''''||chr(10);

      if sMdmMethod = 'ISR_PK_CHECKSUM' then
        sSql := sSql||' and    checksum = '''||csChecksum||'''';
      end if;

      isr$trace.debug('statement','sSql',sCurrentName,sSql);
      execute immediate sSql;

  end case;
  isr$trace.stat('end','end',sCurrentName);
exception
  when others then
    oErrorObj.handleError( STB$TYPEDEF.cnSeverityCritical, utd$msglib.getmsg ('exUnexpectedError', nCurrentLanguage, sqlerrm(sqlcode)), sCurrentName,
                           substr(dbms_utility.format_error_stack || CHR(10) || dbms_utility.format_error_backtrace, 1,4000));
end setMdmFlag;


--****************************************************************************************************
procedure updateMdmUserRecord( csEntityName     varchar2,
                               csMdmTableName   varchar2,
                               csPkValue        varchar2,
                               csPkList         varchar2,
                               olParameter      STB$PROPERTY$LIST )
is
  sCurrentName     constant varchar2(4000) := $$PLSQL_UNIT||'.updateMdmUserRecord('||csEntityname||','||csPkValue||')';
  sSql             varchar2(4000);
  oErrorObj        STB$OERROR$RECORD       := STB$OERROR$RECORD();
  sAttributeList   varchar2(4000);
  sIncludeSettings varchar2(32000);

begin
  isr$trace.stat('begin','begin',sCurrentName);

  sAttributeList := getEntityAttributeList(csEntityName);
  if sAttributeList is null then
    raise exAttributesMissing;
  end if;

  isr$trace.stat('begin','begin',sCurrentName);

  for i in 1..olParameter.count()
  loop
    case when    olParameter(i).sParameter in ('MDM_RESOLUTION','MDM_USER_RECORD','MDM_GROUP_ACCEPTED','SERVER','MODIFIEDON')   -- these columns are not to be changed
              or instr( csPkList, ','||olParameter(i).sParameter||',' ) > 0 then                                                -- primary key columns may not be changed as well
        -- nothing to do
        null;
      else
        -- all other columns are updated
        sIncludeSettings := sIncludeSettings||'        ,'||olParameter(i).sParameter||' = '''||olParameter(i).sValue||''''||chr(10);
    end case;
  end loop;

  sSql :=    ' update '||csMdmTableName||chr(10)
          || ' set    modifiedon  = sysdate,'||chr(10)
          || '        modifiedby  = '''||csUser||''''||chr(10)
          || sIncludeSettings
          || ' where  '||isr$util.getSplit(csPkList,1)||' = '''||csPkValue||''''||chr(10)
          || ' and    mdm_user_record = ''T''';

  isr$trace.debug('statement', 'sSql', sCurrentName, sSql);
  execute immediate sSql;

  sSql := ' update '||csMdmTableName||chr(10)||
          ' set    checksum = to_number( stb$java.calculateHash( to_clob(xmlforest('||sAttributeList||'))), lpad(''x'',60,''x''))'||chr(10)||    -- [ISRC-1125]
          ' where  mdm_user_record = ''T'''||chr(10)||
          ' and  '||isr$util.getSplit(csPkList,1)||' = '''||csPkValue||''''||chr(10);
  isr$trace.debug('statement', 'sSql', sCurrentName, sSql);
  execute immediate sSql;

  isr$trace.stat('end','end',sCurrentName);
exception
  when others then
    oErrorObj.handleError( STB$TYPEDEF.cnSeverityCritical, utd$msglib.getmsg ('exUnexpectedError', nCurrentLanguage, sqlerrm(sqlcode)), sCurrentName,
                           substr(dbms_utility.format_error_stack || CHR(10) || dbms_utility.format_error_backtrace, 1,4000));
end updateMdmUserRecord;


--****************************************************************************************************
function userRecordExistsInGroup( csMdmTableName varchar2,
                                  csPkValue      varchar2,
                                  csPkList       varchar2,
                                  csChecksum     varchar2 )
  return varchar2
is
  sCurrentName     constant varchar2(4000) := $$PLSQL_UNIT||'.userRecordExistsInGroup('||csMdmTableName||','||csPkValue||')';
  sMdmMethod       constant varchar2(50) := nvl(trim(upper(STB$UTIL.getSystemParameter('MASTER_DATA_MANAGEMENT_METHOD'))),'DISABLED');  -- 'ISR_MDM_DEFAULT','DISABLED'
  sReturn          varchar2(1);
  sSql             varchar2(4000);
  oErrorObj        STB$OERROR$RECORD := STB$OERROR$RECORD();
begin
  sSql :=    ' select distinct ''T'''||chr(10)
          || ' from   ' ||csMdmTableName||chr(10)
          || ' where  '||isr$util.getSplit(csPkList,1)||' = '''||csPkValue||''''||chr(10)
          || ' and    mdm_user_record = ''T'''||chr(10);

  if sMdmMethod = 'ISR_PK_CHECKSUM' then
    sSql := sSql||' and    checksum = '''||csChecksum||'''';
  end if;

  isr$trace.debug('statement','sSql',sCurrentName,sSql);

  execute immediate sSql
  into    sReturn;

  return nvl(sReturn,'F');
exception
  when no_data_found then
    return 'F';
  when others then
    oErrorObj.handleError( STB$TYPEDEF.cnSeverityCritical, utd$msglib.getmsg ('exUnexpectedError', nCurrentLanguage, sqlerrm(sqlcode)), sCurrentName,
                           substr(dbms_utility.format_error_stack || CHR(10) || dbms_utility.format_error_backtrace, 1,4000));
    return 'F';

end userRecordExistsInGroup;


--*********************************************************************************************************************************
function countMdmEntities( csFilter varchar2 )
  return number
is
  sCurrentName        constant varchar2(4000) := $$PLSQL_UNIT||'.countMdmEntities('||csFilter||')';
  oErrorObj             STB$OERROR$RECORD     := STB$OERROR$RECORD();
  nReturnCounted      number := 0;
begin
  for recEntity in curGetMdmEntity( csFilter )
  loop
    nReturnCounted := nReturnCounted + 1;
  end loop;
  return nReturnCounted;
exception
  when others then
    oErrorObj.handleError( STB$TYPEDEF.cnSeverityCritical, utd$msglib.getmsg ('exUnexpectedError', nCurrentLanguage, sqlerrm(sqlcode)), sCurrentName,
                           substr(dbms_utility.format_error_stack || CHR(10) || dbms_utility.format_error_backtrace, 1,4000));
    return 0;
end;


--*********************************************************************************************************************************
function checkMdmTable( sMdmTableName varchar2 )
  return varchar
is
  nDummy number;
begin
  execute immediate 'select distinct 1 from '||sMdmTableName||' where rownum = 1'
  into nDummy;
  return 'OK';
exception
  when NO_DATA_FOUND
    then return  'NO_DATA_FOUND';
  when others then
    return 'NO_TABLE';
end checkMdmTable;


--*********************************************************************************************************************************
function getMdmMasterServerid( csEntity in isr$meta$entity.entityname%type )
  return number
is
  -- this function retrieves the serverid of the MDM-Master server from table ISR$SERVER$ENTITY$MAPPING
  sCurrentName          constant varchar2(4000) := $$PLSQL_UNIT||'.getMdmMasterServerid('||csEntity||')';
  oErrorObj             STB$OERROR$RECORD       := STB$OERROR$RECORD();
  nMdmMasterServerid    number;
begin
  isr$trace.stat('begin','begin',sCurrentName);

  select serverid
  into   nMdmMasterServerid
  from   ( select entityname, serverid, mdmmaster,
                  rank() over( order by decode(mdmmaster,'T',1,'F',3,2), serverid ) rank
           from   isr$server$entity$mapping
           where  entityname = 'SB$PRODUCT' )
  where rank = 1;

  isr$trace.stat('end', 'nMdmMasterServerid: '||nMdmMasterServerid, sCurrentName);
  return nMdmMasterServerid;
exception
  when others then
    oErrorObj.handleError( STB$TYPEDEF.cnSeverityCritical, utd$msglib.getmsg ('exUnexpectedError', nCurrentLanguage, sqlerrm(sqlcode)), sCurrentName,
                           substr(dbms_utility.format_error_stack || CHR(10) || dbms_utility.format_error_backtrace, 1,4000));
end getMdmMasterServerid;


--*********************************************************************************************************************************
function getMdmRulePklist( csEntity in isr$meta$entity.entityname%type,
                           csPkList varchar2 )
  return varchar2
is
  -- this function applies the primary key rules for the respective MDM method
  sCurrentName          constant varchar2(4000) := $$PLSQL_UNIT||'.getMdmRulePklist('||csEntity||')';
  sMdmMethod            constant varchar2(50) := nvl(trim(upper(STB$UTIL.getSystemParameter('MASTER_DATA_MANAGEMENT_METHOD'))),'DISABLED');  -- 'ISR_MDM_DEFAULT','DISABLED'
  oErrorObj             STB$OERROR$RECORD       := STB$OERROR$RECORD();
  sReturnRuledPkList    varchar2(4000);
begin
  isr$trace.stat('begin', 'csPkList: '||csPkList, sCurrentName);
  case
    when sMdmMethod = 'ISR_PK_CHECKSUM' then   -- vs 05.Aug.2018  [BISOS-47], [ISRC-971], [ISRSB-5]
      -- 'ISR_PK_CHECKSUM' uses the CHECKSUM and the incoming primary key list
      sReturnRuledPkList := csPkList||',CHECKSUM';
    else
      -- 'ISR_MDM_DEFAULT' uses the incoming primary key list
      sReturnRuledPkList := csPkList;
  end case;

  isr$trace.stat('end', 'sReturnRuledPkList: '||sReturnRuledPkList, sCurrentName);
  return sReturnRuledPkList;
exception
  when others then
    oErrorObj.handleError( STB$TYPEDEF.cnSeverityCritical, utd$msglib.getmsg ('exUnexpectedError', nCurrentLanguage, sqlerrm(sqlcode)), sCurrentName,
                           substr(dbms_utility.format_error_stack || CHR(10) || dbms_utility.format_error_backtrace, 1,4000));
end getMdmRulePklist;


--*********************************************************************************************************************************
function getXmlNlsParameters
  return xmltype
is
  returnXml xmltype;
begin
  select xmlelement("rowset", xmlagg( xmlelement("row", xmlforest(parameter "parameter",
                                                                value     "value" ) ) )  )
  into   returnXml
  from   v$nls_parameters;
  return returnXml;
end getXmlNlsParameters;


--*********************************************************************************************************************************
function getNlsFormats
  return STB$OERROR$RECORD
is
  oErrorObj        STB$OERROR$RECORD := STB$OERROR$RECORD();
  sCurrentName     constant varchar2(4000) := $$PLSQL_UNIT||'.getNlsFormats';
  xXmlParameters   xmltype;
begin
  isr$trace.stat('begin','begin',sCurrentName,getXmlNlsParameters());
  -- rescue sessionparameters
  for recNls in curNlsSessionParameters
  loop
    olNlsSessionParameters.extend;
    olNlsSessionParameters(olNlsSessionParameters.last) := recNls;
  end loop;

  isr$trace.stat('end','end', sCurrentName,getXmlNlsParameters());
  return   oErrorObj;
exception
  when others then
    oErrorObj.handleError( STB$TYPEDEF.cnSeverityWarning, utd$msglib.getmsg ('exUnexpectedError', nCurrentLanguage, sqlerrm(sqlcode)), sCurrentName,
                           substr(dbms_utility.format_error_stack || CHR(10) || dbms_utility.format_error_backtrace, 1,4000));
    return oErrorObj;
end getNlsFormats;


--*********************************************************************************************************************************
function uncacceptedMdmGroupExists( csTableName varchar2 )
  return boolean
is
  nCounted    number;
  bReturn     boolean := false;
begin
    execute immediate 'select distinct 1 from '||csTableName||' where mdm_group_accepted = ''F'' or mdm_group_accepted is null'
    into nCounted;

    if nCounted is null then
      bReturn := false;
    else
      bReturn := true;
    end if;

  return bReturn;
exception
  when others then
    return false;
end uncacceptedMdmGroupExists;


--*********************************************************************************************************************************
function tableExists( csTableName varchar2 )
  return boolean
is
  bReturn boolean := false;
begin
  for i in ( select 1 from user_tables where table_name = upper(csTableName) )
  loop
    bReturn := true;
  end loop;
  return bReturn;
exception
  when others then
    return false;
end tableExists;


--*********************************************************************************************************************************
function recordExists( csTableName varchar2 )
  return boolean
is
  nCounted    number;
  bReturn     boolean := false;
begin
    execute immediate    'select distinct 1 from '||csTableName
    into nCounted;

    if nCounted is null then
      bReturn := false;
    else
      bReturn := true;
    end if;

  return bReturn;
exception
  when others then
    return false;
end recordExists;


--*********************************************************************************************************************************
function couDoubletVariants( csTmpTableName in varchar2,
                             csPkList       in varchar2 )
  return number
is
  sCurrentName     constant varchar2(4000) := $$PLSQL_UNIT||'.couDoubletVariants('||csTmpTableName||')';
  oErrorObj        STB$OERROR$RECORD       := STB$OERROR$RECORD();
  nReturnCounted   number;
  sSql             varchar2(4000);
begin
  isr$trace.stat('begin','csPkList: '||csPkList,sCurrentName);

  sSql :=              ' select nvl(sum(cou),0)'         -- don't remove sum !
           ||chr(10)|| ' from   ( select count(1) cou'
           ||chr(10)|| '          from   '||csTmpTableName
           ||chr(10)|| '          group by '||csPkList
           ||chr(10)|| '          having count(1) > 1)';

  isr$trace.debug('statement','sSql',sCurrentName,sSql);
  execute immediate sSql into nReturnCounted;

  isr$trace.stat('end', 'nReturnCounted: '||nReturnCounted, sCurrentName);
  return nReturnCounted;
exception
  when others then
    oErrorObj.handleError( STB$TYPEDEF.cnSeverityCritical, utd$msglib.getmsg ('exUnexpectedError', nCurrentLanguage, sqlerrm(sqlcode)), sCurrentName,
                           substr(dbms_utility.format_error_stack || CHR(10) || dbms_utility.format_error_backtrace, 1,4000));
    return -1;
end couDoubletVariants;


--*********************************************************************************************************************************
function checkEntityAttributes( csEntity in isr$meta$entity.entityname%type )
  return boolean
is
  sCurrentName  constant varchar2(4000) := $$PLSQL_UNIT||'.checkEntityAttributes('||csEntity||')';
  oErrorObj     STB$OERROR$RECORD       := STB$OERROR$RECORD();
  sReturn       boolean;
  sReturnString varchar2(5);
  nCountOrderno number;
begin
  isr$trace.stat('begin', 'csEntity: '||csEntity, sCurrentName);
  select count(1) cou
  into   nCountOrderno
  from   ( select entityname, orderno, count(1) cou
           from   isr$meta$attribute
           where  entityname = csEntity
           group by entityname, orderno
           having count(1) > 1 );

  if nCountOrderno > 0 then
    sReturn := false;
    sReturnString := 'FALSE';
  else
    sReturn := true;
    sReturnString := 'TRUE';
  end if;

  isr$trace.stat('end', 'sReturn: '||sReturnString, sCurrentName);
  return sReturn;
exception
  when others then
    oErrorObj.handleError( STB$TYPEDEF.cnSeverityCritical, utd$msglib.getmsg ('exUnexpectedError', nCurrentLanguage, sqlerrm(sqlcode)), sCurrentName,
                           substr(dbms_utility.format_error_stack || CHR(10) || dbms_utility.format_error_backtrace, 1,4000));
    return false;
end checkEntityAttributes;


--*********************************************************************************************************************************
function getEntityPkList( csEntity in isr$meta$entity.entityname%type )
  return varchar2
is
  sCurrentName   constant varchar2(4000) := $$PLSQL_UNIT||'.getEntityPkList('||csEntity||')';
  oErrorObj      STB$OERROR$RECORD       := STB$OERROR$RECORD();
  sReturnPkList  varchar2(4000);
begin
  isr$trace.stat('begin', 'csEntity: '||csEntity, sCurrentName);

  select listagg(attrname,',') within group(order by orderno)
  into   sReturnPkList
  from   isr$meta$attribute
  where  entityname = csEntity
  and    (iskey = 'T');

  isr$trace.stat('end', 'sReturnPkList '||sReturnPkList, sCurrentName);
  return sReturnPkList;
exception
  when others then
    oErrorObj.handleError( STB$TYPEDEF.cnSeverityCritical, utd$msglib.getmsg ('exUnexpectedError', nCurrentLanguage, sqlerrm(sqlcode)), sCurrentName,
                           substr(dbms_utility.format_error_stack || CHR(10) || dbms_utility.format_error_backtrace, 1,4000));
    return sReturnPkList;
end getEntityPkList;


--*********************************************************************************************************************************
function getColumnList( csTableName in varchar2 )
  return varchar2
is
  sCurrentName   constant varchar2(4000) := $$PLSQL_UNIT||'.getColumnList('||csTableName||')';
  oErrorObj      STB$OERROR$RECORD       := STB$OERROR$RECORD();
  sReturnColList varchar2(4000);
  sSql           varchar2(4000);
begin
  isr$trace.stat('begin', 'csTableName: '||csTableName, sCurrentName);

  sSql :=     ' select listagg(column_name,'','') within group(order by column_id)'
           || ' from   user_tab_columns'
           || ' where  table_name ='''||csTableName||'''';
  isr$trace.debug('statement', 'sSql', sCurrentName,sSql);
  execute immediate sSql into sReturnColList ;

  isr$trace.stat('end', 'sReturnColList: '||sReturnColList, sCurrentName);
  return sReturnColList;
exception
  when others then
    oErrorObj.handleError( STB$TYPEDEF.cnSeverityCritical, utd$msglib.getmsg ('exUnexpectedError', nCurrentLanguage, sqlerrm(sqlcode)), sCurrentName,
                           substr(dbms_utility.format_error_stack || CHR(10) || dbms_utility.format_error_backtrace, 1,4000));
    return sReturnColList;
end getColumnList;


--*********************************************************************************************************************************
function getEntityAttributeList( csEntity in isr$meta$entity.entityname%type )
  return varchar2
is
  sCurrentName         constant varchar2(4000) := $$PLSQL_UNIT||'.getEntityAttributeList('||csEntity||')';
  oErrorObj            STB$OERROR$RECORD       := STB$OERROR$RECORD();
  sReturnAttributeList varchar2(4000);
begin
  isr$trace.stat('begin', 'csEntity: '||csEntity, sCurrentName);

  select listagg(attrname,',') within group(order by orderno)
  into   sReturnAttributeList
  from   isr$meta$attribute
  where  entityname = csEntity;

  isr$trace.stat('end', 'sReturnAttributeList: '||sReturnAttributeList, sCurrentName);
  return sReturnAttributeList;
exception
  when others then
    oErrorObj.handleError( STB$TYPEDEF.cnSeverityCritical, utd$msglib.getmsg ('exUnexpectedError', nCurrentLanguage, sqlerrm(sqlcode)), sCurrentName,
                           substr(dbms_utility.format_error_stack || CHR(10) || dbms_utility.format_error_backtrace, 1,4000));
    return sReturnAttributeList;
end getEntityAttributeList;


--*********************************************************************************************************************************
function isMdmEntity( csEntity in isr$meta$entity.entityname%type )
  return boolean
is
  sCurrentName    constant varchar2(4000) := $$PLSQL_UNIT||'.isMdmEntity('||csEntity||')';
  oErrorObj       STB$OERROR$RECORD       := STB$OERROR$RECORD();
  sReturn         boolean;
  sReturnString   varchar2(5);
  nCountedEntries number;
begin
  isr$trace.stat('begin', 'csEntity: '||csEntity, sCurrentName);

  select count(1)
  into   nCountedEntries
  from   isr$meta$entity
  where  entityname = csEntity
  and    mdm = 'T';

  if nCountedEntries = 0 then
    sReturn := false;
    sReturnString := 'FALSE';
  else
    sReturn := true;
    sReturnString := 'TRUE';
  end if;

  isr$trace.stat('end', 'sReturn: '||sReturnString, sCurrentName);
  return sReturn;
exception
  when others then
    oErrorObj.handleError( STB$TYPEDEF.cnSeverityCritical, utd$msglib.getmsg ('exUnexpectedError', nCurrentLanguage, sqlerrm(sqlcode)), sCurrentName,
                           substr(dbms_utility.format_error_stack || CHR(10) || dbms_utility.format_error_backtrace, 1,4000));
end isMdmEntity;


--*********************************************************************************************************************************
function checkChecksum( csTmpTableName in varchar2 )
  return number
is
  sCurrentName     constant varchar2(4000) := $$PLSQL_UNIT||'.checkChecksum('||csTmpTableName||')';
  oErrorObj        STB$OERROR$RECORD       := STB$OERROR$RECORD();
  nReturnCounted   number;
begin
  isr$trace.stat('begin', 'csTmpTableName: '||csTmpTableName, sCurrentName);
  execute immediate 'select count(1) from '||csTmpTableName||' where nvl(checksum,0) = 0'
  into nReturnCounted;
  isr$trace.stat('end', 'nReturnCounted: '||nReturnCounted, sCurrentName);
  return nReturnCounted;
exception
  when others then
    oErrorObj.handleError( STB$TYPEDEF.cnSeverityCritical, utd$msglib.getmsg ('exUnexpectedError', nCurrentLanguage, sqlerrm(sqlcode)), sCurrentName,
                           substr(dbms_utility.format_error_stack || CHR(10) || dbms_utility.format_error_backtrace, 1,4000));
    return -1;
end checkChecksum;


--*********************************************************************************************************************************
function checkServerDoublets( csTmpTableName in varchar2,
                              csPkList       in varchar2 )  -- inclusive SERVERID and CHECKSUM
  return number
is
  sCurrentName     constant varchar2(4000) := $$PLSQL_UNIT||'.checkServerDoublets';
  oErrorObj        STB$OERROR$RECORD       := STB$OERROR$RECORD();
  nReturnCounted   number;
  sSql             varchar2(4000);
begin
  isr$trace.stat('begin', 'csTmpTableName: '||csTmpTableName,sCurrentName);
  isr$trace.debug('parameter','csPkList: '||csPkList,sCurrentName);

  sSql :=     ' select count(1) COU'
           || ' from   ( select '||csPkList||', count(1) over( partition by '||csPkList||') COU'
           || '          from '|| csTmpTableName||')'
           || ' where  cou > 1';
  isr$trace.debug('statement', 'sSql', sCurrentName, sSql);
  execute immediate sSql into nReturnCounted;

  isr$trace.stat('end', 'nReturnCounted: '||nReturnCounted, sCurrentName);
  return nReturnCounted;
exception
  when others then
    oErrorObj.handleError( STB$TYPEDEF.cnSeverityCritical, utd$msglib.getmsg ('exUnexpectedError', nCurrentLanguage, sqlerrm(sqlcode)), sCurrentName,
                           substr(dbms_utility.format_error_stack || CHR(10) || dbms_utility.format_error_backtrace, 1,4000));
    return -1;
end checkServerDoublets;


--*********************************************************************************************************************************
function clearDoubletsFromTempTable( csTmpTableName in varchar2,
                                     csPkList       in varchar2 )
  return number
is
  sCurrentName     constant varchar2(4000) := $$PLSQL_UNIT||'.clearDoubletsFromTempTable';
  oErrorObj        STB$OERROR$RECORD       := STB$OERROR$RECORD();
  nReturnCounted   number;
  sSql             varchar2(4000);
begin
  isr$trace.stat('begin', 'csTmpTableName: '||csTmpTableName||crlf||'csPkList: '||csPkList, sCurrentName);
  sSql :=     ' delete from '||csTmpTableName
           || ' where  ('||csPkList||',checksum,serverid ) '
           || '         not in ( select '||csPkList||',checksum,min(serverid)'
           || '                  from   '||csTmpTableName
           || '                  group by '||csPkList||',checksum)';
  isr$trace.debug('statement', 'sSql', sCurrentName, sSql);
  execute immediate sSql;
  nReturnCounted := sql%rowcount;

  isr$trace.stat('end', 'nReturnCounted: '||nReturnCounted, sCurrentName);
  return nReturnCounted;
exception
  when others then
    oErrorObj.handleError( STB$TYPEDEF.cnSeverityCritical, utd$msglib.getmsg ('exUnexpectedError', nCurrentLanguage, sqlerrm(sqlcode)), sCurrentName,
                           substr(dbms_utility.format_error_stack || CHR(10) || dbms_utility.format_error_backtrace, 1,4000));
    return -1;
end clearDoubletsFromTempTable;


--*********************************************************************************************************************************
function makeComparePkList( csPkList varchar2 )
  return varchar2
is
  sReturnPkList      varchar2(32000) := '(';
  nCountListElements number;
  sListElement       varchar2(100);
begin
  nCountListElements := regexp_count(csPkList,',') + 1;

  for i in 1..nCountListElements
  loop
    sListElement := ISR$UTIL.getSplit( csPkList, i, ',');
    sReturnPkList := sReturnPkList || ' a.'||sListElement||' = b.'||sListElement||' and';
  end loop;

  sReturnPkList := rtrim(sReturnPkList,'and') || ')';
  return sReturnPkList;
end makeComparePkList;


--*********************************************************************************************************************************
function changeMdmTableAutonomous( csEntityName varchar2,
                                   csSqlInsert  varchar2,
                                   csSqlUpdate  varchar2,
                                   csColumnList varchar2,
                                   cxTmpTable   xmltype )
  return number
is
  -- Data shall be written into MDM-tables within autonomous transactions [ABBIS-169], [ISRC-1074]
  pragma autonomous_transaction;

  oErrorObj             STB$OERROR$RECORD       := STB$OERROR$RECORD();
  sCurrentName          constant varchar2(4000) := $$PLSQL_UNIT||'.changeMdmTableAutonomous('||csEntityName||')';
  sQueryTempTable       varchar2(32000);
  sSqlInsert            varchar2(32000);
  nChanges              number;
begin
  isr$trace.stat('begin','AUTONOMOUS_TRANSACTION',sCurrentName);
  isr$trace.debug('parameter','csSqlInsert',sCurrentName,csSqlInsert);
  isr$trace.debug('parameter','csSqlUpdate',sCurrentName,csSqlUpdate);
  isr$trace.debug('parameter','cxTmpTable',sCurrentName,cxTmpTable);

  sQueryTempTable := generateCanonicalQuery(cxTmpTable, csColumnList);
  isr$trace.debug('variable','sQueryTempTable',sCurrentName,sQueryTempTable);

  sSqlInsert := replace( csSqlInsert, '###QueryTempTable', sQueryTempTable );
  isr$trace.debug('SQL','sSqlInsert',sCurrentName,sSqlInsert);

  execute immediate sSqlInsert
  using   cxTmpTable;
  nChanges := sql%rowcount;

  execute immediate csSqlUpdate;
  nChanges := nChanges + sql%rowcount;

  commit;
  isr$trace.stat('end','nChanges: '||nChanges,sCurrentName);
  return nChanges;

exception
  when others then
    oErrorObj.handleError( STB$TYPEDEF.cnSeverityCritical, utd$msglib.getmsg ('exUnexpectedError', nCurrentLanguage, sqlerrm(sqlcode)), sCurrentName,
                           substr(dbms_utility.format_error_stack || CHR(10) || dbms_utility.format_error_backtrace, 1,4000));
    return -1;
end changeMdmTableAutonomous;



--*********************************************************************************************************************************
function processDoubletVariants( csTmpTableName  in varchar2,
                                 csMdmTableName  in varchar2,
                                 csPkList        in varchar2,
                                 csRuledPkList   in varchar2,
                                 csAttributeList in varchar2,   -- all attributes, defined in entity definition  (inclusive keys)
                                 csColumnList    in varchar2 )  -- all columns in TMP- or MDM-Table
  return number
is

  sCurrentName          constant varchar2(4000) := $$PLSQL_UNIT||'.processDoubletVariants('||substr(csMdmTableName,5)||')';
  sMdmMethod            varchar2(50) := nvl(trim(upper(STB$UTIL.getSystemParameter('MASTER_DATA_MANAGEMENT_METHOD'))),'DISABLED');  -- 'ISR_MDM_DEFAULT','DISABLED'
  oErrorObj             STB$OERROR$RECORD       := STB$OERROR$RECORD();
  nReturnCounted        number := 0;  -- counted doublet variant datasets, which are not defined in MDM-Table
  nCouInsertVariants    number := 0;
  nCouUpdateResolutions number := 0;
  nCountedInserts       number := 0;
  nCountedDeletions     number := 0;
  nUnconfirmedProcessed number := 0;
  sSql                  varchar2(32000);
  sSqlInsert            varchar2(32000);
  sSqlUpdate            varchar2(32000);
  sEntityName           isr$server$entity$mapping.entityName%type;
  clTmpTable            clob;        -- contains data of temporary table in ROWSET/ROW format
  nMdmTableChanged      number;

begin
  isr$trace.stat('begin', 'csTmpTableName: '||csTmpTableName||',  csMdmTableName: '||csMdmTableName||',  csPkList: '||csPkList||',  csRuledPkList: '||csRuledPkList,sCurrentName);
  isr$trace.debug('parameter','csAttributeList',sCurrentName,csAttributeList);
  isr$trace.debug('parameter','csColumnList',sCurrentName,csColumnList);

  -- insert lacking data into MDM-table
  -- that is all doublet canditates, which have a checksum, which is unknown in MDM-Table
  -- do all changes in autonomous transaction
  -- first determine all parameters ...

  -- parameter #1: entity
  sEntityName := substr(csMdmTableName,5);

  -- parameter #2: insert statement
  sSqlInsert :=              ' insert into '||csMdmTableName||'('||csColumnList||',mdm_resolution, mdm_user_record, mdm_group_accepted, modifiedon, modifiedby)'
                ||chr(10)|| ' select '||csColumnList||','
                ||chr(10)|| '        ''F''               as MDM_RESOLUTION,'
                ||chr(10)|| '        ''F''               as MDM_USER_RECORD,'
                ||chr(10)|| '        ''F''               as MDM_GROUP_ACCEPTED,'
                ||chr(10)|| '        sysdate             as MODIFIEDON,'
                ||chr(10)|| '        '''||sMdmMethod||''' as MODIFIEDBY'
                ||chr(10)|| ' from   ( ###QueryTempTable )'                            --  ||chr(10)|| ' from   '||csTmpTableName
/* das muss vorher passieren, da in der autonomen Transaktion keine Zugriff auf die TMP$ tabellen m�glich ist
                ||chr(10)|| ' where  ('||csPkList||') in( -- Only process datasets with existing variants in TMP$-table'
                ||chr(10)|| '                            select '||csPkList
                ||chr(10)|| '                            from   ( select '||csPkList -- || ', checksum' wenn man die checksum mitnimmt, findet man nur vollkommen identische Datens�tze (die k�nnen nicht existieren!!)
                ||chr(10)|| '                                     from '||csTmpTableName||')'
                ||chr(10)|| '                                     group by '||csPkList
                ||chr(10)|| '                                     having count(1) > 1 )'
*/
                ||chr(10)|| ' where    ('||csPkList||',checksum) not in (select '||csPkList||',checksum from '||csMdmTableName||')';

  -- parameter #2: update statement for resolution record
  sSqlUpdate :=               ' update '||csMdmTableName
                  ||chr(10)|| ' set    mdm_resolution     = ''T'','
                  ||chr(10)|| '        mdm_user_record    = ''F'','
                  ||chr(10)|| '        mdm_group_accepted = ''F'','
                  ||chr(10)|| '        modifiedon         = sysdate,'
                  ||chr(10)|| '        modifiedby         = '''||sMdmMethod||''''    -- vs 06.Apr.2018  [BISOS-46], [ISRC-971], [ISRSB-5]
                  ||chr(10)|| ' where  rowid in ( select rid'
                  ||chr(10)|| '                   from   ( select a.rowid rid, '||csRuledPkList||', a.mdm_resolution, sem.mdmMaster,'
                  ||chr(10)|| '                                   rank() over( partition by '||csRuledPkList||' order by decode(a.mdm_resolution,''T'',1,2), decode(sem.mdmMaster,''T'',1,''F'',3,2),rownum) ranking'
                  ||chr(10)|| '                            from   '||csMdmTableName||' a'
                                                           ----
                  ||chr(10)|| '                            left join   isr$server$entity$mapping sem'
                  ||chr(10)|| '                            on     a.serverid = sem.serverid'
                  ||chr(10)|| '                            and    sem.entityname = '''||sEntityName||''' )'
                  ||chr(10)|| '                            where ranking = 1 )'
                  ||chr(10)|| ' and    nvl(mdm_resolution,''F'') <> ''T''';

  -- data from temp table
  execute immediate  'select to_clob(dbms_xmlgen.getxml(''select * from '||csTmpTableName
  -- nur die dubletten �bergeben da sonst die Entscheidung �ber eine Dublette nicht mehr getroffen werden kann
                 ||chr(10)|| ' where  ('||csPkList||') in( -- Only process datasets with existing variants in TMP$-table'
                ||chr(10)|| '                            select '||csPkList
                ||chr(10)|| '                            from   ( select '||csPkList -- || ', checksum' wenn man die checksum mitnimmt, findet man nur vollkommen identische Datens�tze (die k�nnen nicht existieren!!)
                ||chr(10)|| '                                     from '||csTmpTableName||')'
                ||chr(10)|| '                                     group by '||csPkList
                ||chr(10)|| '                                     having count(1) > 1 )'
                ||          ''')) from dual'
  into    clTmpTable;

  nMdmTableChanged := changeMdmTableAutonomous( csEntityName => sEntityName,
                                                csSqlInsert  => sSqlInsert,
                                                csSqlUpdate  => sSqlUpdate,
                                                csColumnList => csColumnList,
                                                cxTmpTable   => xmltype(clTmpTable) );

  if nMdmTableChanged >= 1 then
    isr$trace.debug('code position', 'Changes found in '||csMdmTableName||': '||nMdmTableChanged, sCurrentName);
    -- add audit entry
    begin
      isr$trace.debug('code position', 'before Audit actions',sCurrentName);

      oErrorObj := Stb$audit.openAudit ('SYSTEMAUDIT', csMdmTableName);                          -- SYSTEMAUDIT for MDM$-table
      if oErrorObj.sSeverityCode not in ( stb$typedef.cnNoError, stb$typedef.cnAudit ) then      -- vs 05.Apr.2018  [BISOS-46], [ISRC-971], [ISRSB-4]
        raise exCreateAudit;
      end if;

      oErrorObj := Stb$audit.AddAuditRecord ( 'MDM_RESOLUTION_ENTRY_AUDIT' );                    -- TOKEN
      if oErrorObj.sSeverityCode not in ( stb$typedef.cnNoError, stb$typedef.cnAudit ) then      -- vs 05.Apr.2018  [BISOS-46], [ISRC-971], [ISRSB-4]
        raise exCreateAudit;
      end if;

      oErrorObj := Stb$audit.AddAuditEntry(csMdmTableName,'(null)','NewVariants: '||nCouInsertVariants||'  NewResolutions: '||nCouUpdateResolutions, nCouInsertVariants + nCouUpdateResolutions);    -- AUDIT-Entry
      if oErrorObj.sSeverityCode not in ( stb$typedef.cnNoError, stb$typedef.cnAudit ) then      -- vs 05.Apr.2018  [BISOS-46], [ISRC-971], [ISRSB-4]
        raise exCreateAudit;
      end if;
      -- silent audit and close audit
      oErrorObj := Stb$audit.silentAudit;
      if oErrorObj.sSeverityCode not in ( stb$typedef.cnNoError, stb$typedef.cnAudit ) then      -- vs 05.Apr.2018  [BISOS-46], [ISRC-971], [ISRSB-4]
        raise exCreateAudit;
      end if;
    exception
      when others then
        raise exCreateAudit;
    end;
  end if;

  -- insert resolution datasets from MDM table into TMP table, if necessary
  isr$trace.debug('code position','Insert resolution datasets for existing variants, which do not exist in '||csTmpTableName,sCurrentName);
  sSql :=             ' insert into '||csTmpTableName||'('||csColumnList||')'
          ||chr(10)|| ' select '||csColumnList
          ||chr(10)|| ' from   '||csMdmTableName
          ||chr(10)|| ' where  mdm_resolution = ''T'''
          ||chr(10)|| ' and    ('||csPkList||',checksum) not in ( select '||csPkList||',checksum'   -- check if exact resolution-set already exists in TMP-table
          ||chr(10)|| '                                           from   '||csTmpTableName||' )'
          ||chr(10)|| ' and    ('||csPkList||') in ( select '||csPkList                             -- check, if doublets exist for this primary key
          ||chr(10)|| '                              from   '||csTmpTableName
          ||chr(10)|| '                              group  by '||csPkList
          ||chr(10)|| '                              having count(1) > 1 )';
  isr$trace.debug('statement', 'sSql', sCurrentName, sSql);
  execute immediate sSql;
  nCountedInserts  := sql%rowcount;
  isr$trace.debug('code position', 'New resolution datasets in '||csTmpTableName||': '||nCountedInserts, sCurrentName);

  -- delete all variants from temporary table which are not defined as resolution dataset
  isr$trace.debug('code position','Cleanup doublet variants in '||csTmpTableName,sCurrentName);
  sSql :=             ' delete from  '||csTmpTableName
          ||chr(10)|| ' where  ('||csPkList||') in (select '||csPkList                                -- Existing variants in TMP$-table
          ||chr(10)|| '                              from   ( select '||csPkList||',checksum'
          ||chr(10)|| '                                       from  '||csTmpTableName||')'
          ||chr(10)|| '                                       group by '||csPkList
          ||chr(10)|| '                                       having count(1) > 1 )'
          ||chr(10)|| '  and   ('||csPkList||') in ( select '||csPkList                               -- Delete only, if a Resolution set exists   <-- additional safety here, this part could be omited
          ||chr(10)|| '                              from   '||csMdmTableName
          ||chr(10)|| '                              where  mdm_resolution =''T'' )'
          ||chr(10)|| '  and  ('||csPkList||',checksum) not in  ( select '||csPkList||', checksum'    -- delete all variants, that have no RESOLUTION counterpart
          ||chr(10)|| '                                           from   '||csMdmTableName
          ||chr(10)|| '                                           where  mdm_resolution = ''T'' )';
  isr$trace.debug('statement', 'sSql', sCurrentName, sSql);
  execute immediate sSql;
  nCountedDeletions  := sql%rowcount;
  isr$trace.debug('code position', 'Cleaned doublet variants in '||csTmpTableName||': '||nCountedDeletions, sCurrentName);

  -- finally clean doublets from different servers (they have same checksum)
  cleanupServerDoublets( csTmpTableName => csTmpTableName,
                         csScope        => 'SERVER_ALL',
                         csPkList       => csRuledPkList );

  -- check, if unconfirmed mdm_group has been processed: MDM_GROUP_ACCEPTED = F   [ABBIS-53]
    sSql :=             ' select count(1)'
            ||chr(10)|| ' from   ( select '||csPkList
            ||chr(10)|| '          from   '||csMdmTableName
            ||chr(10)|| '          where  mdm_group_accepted = ''F'''
            ||chr(10)|| '          intersect'
            ||chr(10)|| '          select '||csPkList
            ||chr(10)|| '          from   '||csTmpTableName||' )';
  isr$trace.debug('statement', 'sSql', sCurrentName, sSql);
  execute immediate sSql into nUnconfirmedProcessed;

  isr$trace.debug('variable', 'nUnconfirmedProcessed: '||nUnconfirmedProcessed, sCurrentName);
  case
    when nUnconfirmedProcessed > 0 and sCallback is not null then
      -- processing of callback routine
      isr$trace.debug('code position', 'calling callback '||sCallback, sCurrentName);
      execute immediate 'DECLARE
                           olocError STB$OERROR$RECORD;
                         BEGIN
                           olocError:='|| sCallBack || ';
                           :1 := olocError;
                         END;'
      using out oErrorObj;
    when nUnconfirmedProcessed > 0 then
      isr$trace.debug('variable', 'sCallback IS NULL', sCurrentName);
    else
      null; -- nothing to do
  end case;

  isr$trace.stat('end', 'rReturnCounted: '||nReturnCounted, sCurrentName);

  return nReturnCounted;
exception
  when exCreateAudit then
    isr$trace.debug('code position', 'processDoubletVariants EXCEPTION exCreateAudit',sCurrentName);
    raise exCreateAudit;
  when others then
    oErrorObj.handleError( STB$TYPEDEF.cnSeverityCritical, utd$msglib.getmsg ('exUnexpectedError', nCurrentLanguage, sqlerrm(sqlcode)), sCurrentName,
                           substr(dbms_utility.format_error_stack || CHR(10) || dbms_utility.format_error_backtrace, 1,4000));
    return -1;
end processDoubletVariants;


--*********************************************************************************************************************************
procedure cleanupServerDoublets( csTmpTableName varchar2,
                                 csScope        varchar2,
                                 csPkList       varchar2 )  -- SERVER_SPECIFIC or SERVER_ALL
is
  sCurrentName        varchar2(100)  := $$PLSQL_UNIT||'.cleanupServerDoublets('||csTmpTableName||')';
  sSql                varchar2(32000);
  nMdmMasterServerid  isr$server$entity$mapping.serverid%type;
  sEntityName         varchar2(30);
  nCountDeleted       number;
begin
  isr$trace.stat('begin','csScope: '||csScope, sCurrentName);
  -- this procedure cleans doublets from the temporary table of the entity, depending on the scope, which is either SERVER_SPECIFIC or SERVER_ALL
  -- currently there is no definiton for preferences, for servers as well as for critorder or entityorder
  -- for a realisation of preferences, this procedure is the right position in the code

  -- Fetch ID from MDM master server
  sEntityName := substr(csTmpTableName,5);
  nMdmMasterServerid := getMdmMasterServerid( sEntityName );

  sSql :=             ' delete from '||csTmpTableName
          ||chr(10)|| ' where  rowid in ( select rid'
          ||chr(10)|| '                   from   ( select rowid rid, '
          ||chr(10)|| '                                   rownum rnum, '
          ||chr(10)|| '                                   checksum, serverid, count(1) over( partition by checksum, serverid) cou, '
          ||chr(10)|| '                                   rank() over( partition by '||csPkList ||',checksum, serverid order by rownum) ranking_specific, '
          ||chr(10)|| '                                   rank() over( partition by '||csPkList ||',checksum order by decode(serverid,'||nMdmMasterServerid||',1,serverid)) ranking_all '
          ||chr(10)|| '                             from   '||csTmpTableName||' ) '
          ||chr(10)|| '                   where  ( '''||csScope||''' = ''SERVER_SPECIFIC'' and ranking_specific > 1 )'
          ||chr(10)|| '                   or     ( '''||csScope||''' = ''SERVER_ALL''      and ranking_all > 1)     )';
  isr$trace.debug('statement', 'sSql', sCurrentName, sSql);
  execute immediate sSql;
  nCountDeleted := sql%rowcount;

  isr$trace.stat('end','datasets deleted: '||nCountDeleted, sCurrentName);
exception
  when others then
    isr$trace.warn('WARNING','cannot remove doublets from same server',sCurrentName,'sSql: '||sSql);
    isr$trace.warn('WARNING',utd$msglib.getmsg('exUnexpectedError', nCurrentLanguage, sqlerrm(sqlcode)),sCurrentName,dbms_utility.format_error_stack||CHR(10)||dbms_utility.format_error_backtrace);
end cleanupServerDoublets;


--*********************************************************************************************************************************
function processMdm( csEntity in isr$meta$entity.entityname%type )
  return STB$OERROR$RECORD
is
  oErrorObj                 STB$OERROR$RECORD := STB$OERROR$RECORD();  -- [ISRC-722]
  -- keep next line, for testing
  --oErrorObj               STB$OERROR$RECORD := STB$OERROR$RECORD(tloMessageList => ISR$MESSAGE$LIST()); --initialize the message list too
  sCurrentName              constant varchar2(4000) := $$PLSQL_UNIT||'.processMdm('||csEntity||')';
  sMdmMethod                varchar2(50) := nvl(trim(upper(STB$UTIL.getSystemParameter('MASTER_DATA_MANAGEMENT_METHOD'))),'DISABLED');  -- 'ISR_MDM_DEFAULT','DISABLED'
  sTmpTable                 constant varchar2(30)   := csTmpTabPrefix||csEntity;
  sMdmTable                 constant varchar2(30)   := csMdmTabPrefix||csEntity;
  sAttributeList            varchar2(4000);   -- list of all attributes of the entity, inclusive key attributes
  sPkList                   varchar2(4000);   -- list of primary key columns as defined in ISR$META$ENTITY
  sRuledPkList              varchar2(4000);   -- list of primary key columns from ISR$META$ENITY, including PK colums by MDM rule
  sColumnList               varchar2(4000);   -- list of all columns in temporary table
  sSql                      varchar2(4000);
  clTableClob               clob;
  nCouDoubletVariants       number;
  nCouNewVariantsProcessed  number;
  nCouDeletedDoublets       number;
begin
  isr$trace.stat('begin', 'sMdmMethod: '||sMdmMethod, sCurrentName);   -- vs 05.Aug.2018  [BISOS-47], [ISRC-971], [ISRSB-5]

  execute immediate  'select to_clob( dbms_xmlgen.getxml(''select * from '||sTmpTable||''')) from dual' into clTableClob;
  ISR$TRACE.debug('status','initial status of '||sTmpTable,sCurrentName,clTableClob);

  -- Check prerequisites for MDM-processing
  case
    when sMdmMethod not in ('ISR_MDM_DEFAULT','ISR_PK_CHECKSUM','DISABLED')  then  -- vs 05.Aug.2018  [BISOS-47], [ISRC-971], [ISRSB-5]
      -- wrong definition of sMdmMethod
      isr$trace.warn('WARNING','wrong definition for systemparameter MASTER_DATA_MANAGEMENT_METHOD: '||sMdmMethod ,sCurrentName);
    when not isMdmEntity(csEntity)  then
      -- MDM processing for this entity is not defined
      isr$trace.info('INFO','There is no defined MDM-processing for this entity',sCurrentName);
    else
      -- Prerequisites ok, start MDM processing for entity

      -- Check temporary table
      isr$trace.debug('code position', 'check temporary table',sCurrentName);
      if not tableExists( sTmpTable ) then
        -- TMP-Table does not exist
        raise exNoTable;
      end if;

      -- get primary key columns
      sPkList := getEntityPkList(csEntity);
      -- redefine list of primary keys, due to MDM rule
      sRuledPkList := getMdmRulePklist( csEntity, sPkList );  -- vs 05.Aug.2018  [BISOS-47], [ISRC-971], [ISRSB-5]

      if sRuledPkList is null then
        raise exMetaNoKey;  -- primary key column(s) could not be determined
      end if;

      -- get column list from temp table
      isr$trace.debug('code position', 'get all columns from temporary table',sCurrentName);
      sColumnList := getColumnList(sTmpTable);
      if sColumnList is null then
        raise exPkColumnList;
      end if;

      -- Read and set relevant current NLS session parameters and set specific values for this session --> important for checksum processing
      oErrorObj := isr$util.initializeSession;   -- [ISRC-1108]

      -- Checksum Processing
      -- get attribute list first
      isr$trace.debug('code position', 'checksum processing',sCurrentName);
      sAttributeList := getEntityAttributeList(csEntity);
      if sAttributeList is null then
        raise exAttributesMissing;
      end if;
      -- build and execute dyn sql update statement for checksum
      -- insert / update the column CHECKSUM, based on all colums of the entity (which are defined in variable sAttributeList)
      sSql := 'update '||sTmpTable||' set checksum = to_number( stb$java.calculateHash( to_clob(xmlforest('||sAttributeList||'))), lpad(''x'',60,''x''))';   -- [ISRC-1125]
      isr$trace.debug('statement', 'sSql', sCurrentName, sSql);
      execute immediate sSql;

      if checkChecksum( sTmpTable ) <> 0 then
        -- Raise exCheckSum, if checksum errors are found
        raise exCheckSum;
      end if;

      isr$trace.debug('code position', 'check doublets from same server',sCurrentName);
      if checkServerDoublets( csTmpTableName => sTmpTable,
                              csPkList       => sPkList||',SERVERID,CHECKSUM') <> 0 then
        -- doublets delivered from the same server found
        -- fetch data of doublets from same server into the variable clTableClob in XML format
        execute immediate  'select to_clob( dbms_xmlgen.getxml(''select * from '||sTmpTable||''')) from dual' into clTableClob;
        -- [ISRC-495],[ARDIS-494] no raise
        isr$trace.warn('WARNING',utd$msglib.getMsg('exDoubletsFromSameServer',1,sTmpTable),sCurrentName,clTableClob);
        -- cleanup doublets from same server
        cleanupServerDoublets( csTmpTableName => sTmpTable,
                               csScope        => 'SERVER_SPECIFIC',
                               csPkList       => sRuledPkList );
        -- Add eMail here: ISRC-392 = Future Release
      end if;

      isr$trace.debug('code position', 'Handle doublet variants',sCurrentName);
      -- Handle doublet variants
      nCouDoubletVariants := couDoubletVariants( csTmpTableName => sTmpTable,
                                                 csPkList       => sRuledPkList );
      if nCouDoubletVariants = -1 then
        -- this is a processing error in function couDoubletVariants
        raise exCouDoubletVariants;
      elsif nCouDoubletVariants > 1 then
        if not tableExists( sMdmTable ) then
          -- Create MDM$-Table
          isr$trace.debug('code position','Create MDM$-Table',sCurrentName);
          oErrorObj := ISR$METADATA$PACKAGE.CreateEntityTable( csEntity, 'MDM' );
          if oErrorObj.sSeverityCode <> stb$typedef.cnNoError then
            raise exCreateTable;
          end if;
        end if;

        -- Standard processing for doublet variants: Remember new variants due to caller info
        -- Write results into MDM-Table and define and do some housekeeping for the MDM-rule
        nCouNewVariantsProcessed := processDoubletVariants( csTmpTableName  => sTmpTable,         -- name of TMP-Table
                                                            csMdmTableName  => sMdmTable,         -- name of MDM-Table
                                                            csPkList        => sPkList,           -- Primary-Key-list by ISR$META$ATTRIBUTE
                                                            csRuledPkList   => sRuledPkList,      -- Primary-Key-list by ISR$META$ATTRIBUTE and MDM rule               -- vs 05.Aug.2018  [BISOS-47], [ISRC-971], [ISRSB-5]
                                                            csAttributeList => sAttributeList,    -- all attributes, defined in entity definition  (inclusive keys)
                                                            csColumnList    => sColumnList );     -- all columns in TMP- or MDM-Table
        if nCouNewVariantsProcessed > 1 then
          isr$trace.debug('status', 'New variants have been processed. Counted: '||nCouNewVariantsProcessed,sCurrentName);
        elsif nCouNewVariantsProcessed = -1 then
          raise exVariantProcessing;
        else
          isr$trace.debug('status','No new doublet variants processed',sCurrentName);
        end if;
      else
        isr$trace.debug('status','No doublet variants found',sCurrentName);
      end if;

      -- final check
      isr$trace.debug('code position', 'Check final result',sCurrentName);
      nCouDoubletVariants := couDoubletVariants( csTmpTableName => sTmpTable,
                                                 csPkList       => sRuledPkList );
      if nCouDoubletVariants > 0 then
        -- exMdmFinalCheck may not impede overall processing [ISRC-495],[ARDIS-494]
        isr$trace.warn('WARNING',utd$msglib.getMsg('exMdmFinalCheck',1,sTmpTable),sCurrentName);
        -- Probably add eMail here: ISRC-392 = Future Release
      end if;

  end case;

  execute immediate  'select to_clob( dbms_xmlgen.getxml(''select * from '||sTmpTable||''')) from dual' into clTableClob;
  ISR$TRACE.debug('status','final status of '||sTmpTable,sCurrentName,clTableClob);

  isr$trace.stat('end','end',sCurrentName);
  return oErrorObj;
exception
  when exMetaNoKey then
    oErrorObj.handleError( STB$TYPEDEF.cnSeverityCritical, utd$msglib.getmsg('exMetaNoKey', nCurrentLanguage, csEntity), sCurrentName,
                           substr(dbms_utility.format_error_stack || CHR(10) || dbms_utility.format_error_backtrace, 1,4000));
    return oErrorObj;
  when exCouDoubletVariants then
    oErrorObj.handleError( STB$TYPEDEF.cnSeverityCritical, utd$msglib.getmsg ('exCouDoubletVariants', nCurrentLanguage), sCurrentName,
                           substr(dbms_utility.format_error_stack || CHR(10) || dbms_utility.format_error_backtrace, 1,4000));
    return oErrorObj;
  when exPkColumnList then
    oErrorObj.handleError( STB$TYPEDEF.cnSeverityCritical, utd$msglib.getmsg ('exPkColumnList', nCurrentLanguage, sTmpTable), sCurrentName,
                           substr(dbms_utility.format_error_stack || CHR(10) || dbms_utility.format_error_backtrace, 1,4000));
    return oErrorObj;
  when exCreateTable then
    oErrorObj.handleError( STB$TYPEDEF.cnSeverityCritical, utd$msglib.getmsg ('exCreateTable', nCurrentLanguage, sMdmTable), sCurrentName,
                           substr(dbms_utility.format_error_stack || CHR(10) || dbms_utility.format_error_backtrace, 1,4000));
    return oErrorObj;
  when exAttributesMissing then
    oErrorObj.handleError( STB$TYPEDEF.cnSeverityCritical, utd$msglib.getmsg ('exAttributesMissing', nCurrentLanguage, csEntity), sCurrentName,
                           substr(dbms_utility.format_error_stack || CHR(10) || dbms_utility.format_error_backtrace, 1,4000));
    return oErrorObj;
  when exNoTable then
    oErrorObj.handleError( STB$TYPEDEF.cnSeverityCritical, utd$msglib.getmsg ('exNoTable', nCurrentLanguage, null,sTmpTable), sCurrentName,
                           substr(dbms_utility.format_error_stack || CHR(10) || dbms_utility.format_error_backtrace, 1,4000));
    return oErrorObj;
  when exCheckSum then
    oErrorObj.handleError( STB$TYPEDEF.cnSeverityCritical, utd$msglib.getmsg ('exCheckSumErrorText', nCurrentLanguage, sqlerrm(sqlcode)), sCurrentName,
                           substr(dbms_utility.format_error_stack || CHR(10) || dbms_utility.format_error_backtrace, 1,4000));
    return oErrorObj;
  when exClearDoublets then
    oErrorObj.handleError( STB$TYPEDEF.cnSeverityCritical, utd$msglib.getmsg ('exClearDoublets', nCurrentLanguage, sTmpTable), sCurrentName,
                           substr(dbms_utility.format_error_stack || CHR(10) || dbms_utility.format_error_backtrace, 1,4000));
    return oErrorObj;
  when exVariantProcessing then
    oErrorObj.handleError( STB$TYPEDEF.cnSeverityCritical, utd$msglib.getmsg ('exVariantProcessing', nCurrentLanguage, sTmpTable), sCurrentName,
                           substr(dbms_utility.format_error_stack || CHR(10) || dbms_utility.format_error_backtrace, 1,4000));
    return oErrorObj;
  when exCreateAudit then
    isr$trace.debug('code position', 'processMdm EXCEPTION exCreateAudt',sCurrentName);
    oErrorObj.handleError( STB$TYPEDEF.cnSeverityCritical, utd$msglib.getmsg ('exCreateAudit', nCurrentLanguage), sCurrentName,
                           substr(dbms_utility.format_error_stack || CHR(10) || dbms_utility.format_error_backtrace, 1,4000));
    return oErrorObj;
  when others then
    oErrorObj.handleError( STB$TYPEDEF.cnSeverityCritical, utd$msglib.getmsg ('exUnexpectedError', nCurrentLanguage, sqlerrm(sqlcode)), sCurrentName,
                           substr(dbms_utility.format_error_stack || CHR(10) || dbms_utility.format_error_backtrace, 1,4000));
    return oErrorObj;
end processMdm;


--*********************************************************************************************************************************
function processFixedRules( csEntity in isr$meta$entity.entityname%type )
  return STB$OERROR$RECORD
is
  sCurrentName              constant varchar2(4000) := $$PLSQL_UNIT||'.processFixedRules('||csEntity||')';
  csTmpTableName            constant varchar2(30)   := csTmpTabPrefix||csEntity;
  oErrorObj                 STB$OERROR$RECORD       := STB$OERROR$RECORD();
  sSql                      varchar2(4000);
  nCouProcessedRules        number := 0;
begin
  isr$trace.stat('begin','csEntity: '||csEntity, sCurrentName);
  -- loop fixed rules for entity
  for recFixedRule in ( select rul.entityname, rul.attrname, rul.serverid, rul.fixed_rule
                        from   isr$meta$fixed$rule rul
                        join   isr$meta$entity     ent
                        on     rul.entityname = ent.entityname
                        where  ent.entityname = csEntity
                        and    ent.mdm = 'T' )
  loop
    sSql :=             ' update '||csTmpTableName
            ||chr(10)|| ' set    '||recFixedRule.attrname||' = '||recFixedRule.fixed_rule
            ||chr(10)|| ' where  serverid = '||recFixedRule.serverid;
    isr$trace.debug('statement', 'sSql', sCurrentName, sSql);
    execute immediate sSql;
    nCouProcessedRules := nCouProcessedRules + sql%rowcount;
  end loop;

  isr$trace.stat('end', 'Processed fixed rules: '||nCouProcessedRules, sCurrentName);
  return oErrorObj;
exception
  when others then
    oErrorObj.handleError( STB$TYPEDEF.cnSeverityCritical, utd$msglib.getmsg ('exFixedRule', nCurrentLanguage, sqlerrm(sqlcode)), sCurrentName||crlf||'sSql: '||sSql,
                           substr(dbms_utility.format_error_stack || CHR(10) || dbms_utility.format_error_backtrace, 1,4000));
    return oErrorObj;
end processFixedRules;


--****************************************************************************************************************************
function setCurrentNode(oSelectedNode in STB$TREENODE$RECORD) return STB$OERROR$RECORD
is
  sCurrentName   constant varchar2(4000) := $$PLSQL_UNIT||'.setCurrentNode';
  oErrorObj      STB$OERROR$RECORD       := STB$OERROR$RECORD();
  sMsg           varchar2(4000);
begin
  isr$trace.stat('begin', 'begin', sCurrentName, xmltype(oSelectedNode).getClobVal());
  stb$object.destroyoldvalues;
  stb$object.setCurrentNode (oSelectedNode);
  isr$trace.stat('end', 'end', sCurrentName);
  return oErrorObj;
exception
  when others then
    sMsg :=  utd$msglib.getmsg ('exUnexpectedError', nCurrentLanguage, csP1 => sqlerrm(sqlcode));
    oErrorObj.handleError ( Stb$typedef.cnSeverityCritical, sMsg, sCurrentName,
                            substr(dbms_utility.format_error_stack || CHR(10) || dbms_utility.format_error_backtrace, 1,4000)  );
    return oErrorObj;
end setCurrentNode;


--*************************************************************************************************************************
function setMenuAccess ( sParameter   in  varchar2,
                         sMenuAllowed out varchar2 )
  return STB$OERROR$RECORD
is
    sCurrentName   constant varchar2(4000) := $$PLSQL_UNIT||'.setMenuAccess('||sParameter||')';
    oErrorObj      STB$OERROR$RECORD       := STB$OERROR$RECORD();
    sMsg           varchar2(4000);
    oCurNode       STB$TREENODE$RECORD := STB$OBJECT.getCurrentNode;
    sPkValue       varchar2(4000);
    sMdmTableName  varchar2(30);
    sEntityName    varchar2(30);
    sPkList        varchar2(4000);
    sPkListRuled   varchar2(4000);
    sChecksum      varchar2(100);
    sUserExists    varchar2(1);
    sMdmMethod     varchar2(50) := nvl(trim(upper(STB$UTIL.getSystemParameter('MASTER_DATA_MANAGEMENT_METHOD'))),'DISABLED');  -- 'ISR_MDM_DEFAULT','DISABLED'
begin
  isr$trace.stat ('begin','begin',sCurrentName );
  isr$trace.debug('treenode','oCurNode',sCurrentName,oCurNode);

  case
    when sMdmMethod = 'DISABLED' then
      -- MDM is not active
      sMenuAllowed := 'F';
    -----------------------------------------------------------------------------  CAN_ENABLE_MDMENTITY  ------
    when sParameter = 'CAN_ENABLE_MDMENTITY' and countMdmEntities('DISABLED') > 0 then
        oErrorObj := Stb$security.checkGroupRight(sParameter, sMenuAllowed, STB$OBJECT.getCurrentReporttypeId);
    when sParameter = 'CAN_ENABLE_MDMENTITY' then
        sMenuAllowed := 'F';
    -----------------------------------------------------------------------------  CAN_CREATE_MDM_USER_RECORD  ------
    when sParameter = 'CAN_CREATE_MDM_USER_RECORD' and oCurnode.getPropertyParameter('MDM_USER_RECORD') = 'T'  THEN
      sMenuAllowed := 'F';
    when sParameter = 'CAN_CREATE_MDM_USER_RECORD' then
      sEntityName   := substr(oCurnode.getValueParameter('MDMENTITY'),4);  -- name of the Entity
      sMdmTableName := 'MDM$'||sEntityName;                                -- name of the MDM-Table
      sPkValue      := oCurnode.sDisplay;                                  -- Value of the Primary-Key field
      sPkList       := getEntityPkList( sEntityName );                     -- Primary-Key column list
      sChecksum     := oCurnode.getValueParameter('MDMRECORD');            -- checksum of the selected record
      isr$trace.debug('values','sMdmTableName, sEntityName, sPkValue, sPkList, sPkListRuled, sChecksum',sCurrentName,  'sMdmTableName: '||sMdmTableName||chr(10)
                                                                                                                     ||'sEntityName:   '||sEntityName||chr(10)
                                                                                                                     ||'sPkValue:      '||sPkValue||chr(10)
                                                                                                                     ||'sPkList:       '||sPkList||chr(10)
                                                                                                                     ||'sPkListRuled:  '||sPkListRuled||chr(10)
                                                                                                                     ||'sChecksum:     '||sChecksum );
      sUserExists := userRecordExistsInGroup( sMdmTableName, sPkValue, sPkList, sChecksum );
      isr$trace.debug('variable','sUserExists: '||sUserExists,sCurrentName);

      if sUserExists = 'T' then
        sMenuAllowed := 'F';
      else
        oErrorObj := Stb$security.checkGroupRight(sParameter, sMenuAllowed, STB$OBJECT.getCurrentReporttypeId);
      end if;
    -----------------------------------------------------------------------------  CAN_EDIT_MDM_USER_RECORD  ------
    -----------------------------------------------------------------------------  CAN_DELETE_MDM_USER_RECORD  ------
    when sParameter in ('CAN_EDIT_MDM_USER_RECORD','CAN_DELETE_MDM_USER_RECORD') THEN
      if oCurnode.getPropertyParameter('MDM_USER_RECORD') = 'T' then
        oErrorObj := Stb$security.checkGroupRight(sParameter, sMenuAllowed, STB$OBJECT.getCurrentReporttypeId);
      else
        sMenuAllowed := 'F';
      end if;
    -----------------------------------------------------------------------------  CAN_SET_MDM_RESOLUTION_RECORD  ------
    when sParameter = 'CAN_SET_MDM_RESOLUTION_RECORD' THEN
      if oCurnode.getPropertyParameter('MDM_RESOLUTION') = 'T' then
        sMenuAllowed := 'F';
      else
        oErrorObj := Stb$security.checkGroupRight(sParameter, sMenuAllowed, STB$OBJECT.getCurrentReporttypeId);
      end if;
    -----------------------------------------------------------------------------  CAN_ACCEPT_MDM_GROUP  ------
    when sParameter = 'CAN_ACCEPT_MDM_GROUP' THEN
      if oCurnode.getPropertyParameter('MDM_GROUP_ACCEPTED') = 'T' then
        sMenuAllowed := 'F';
      else
        oErrorObj := Stb$security.checkGroupRight(sParameter, sMenuAllowed, STB$OBJECT.getCurrentReporttypeId);
      end if;
    -----------------------------------------------------------------------------  CAN_REVERT_MDM_GROUP_ACCEPTANCE  ------
    when sParameter = 'CAN_REVERT_MDM_GROUP_ACCEPTANCE' THEN
      if oCurnode.getPropertyParameter('MDM_GROUP_ACCEPTED') = 'T' then
        oErrorObj := Stb$security.checkGroupRight(sParameter, sMenuAllowed, STB$OBJECT.getCurrentReporttypeId);
      else
        sMenuAllowed := 'F';
      end if;
    -----------------------------------------------------------------------------  CAN_REVERT_MDM_GROUP_ACCEPTANCE  ------
    when sParameter = 'CAN_CHANGE_MDMMASTER' THEN
      null;
    -----------------------------------------------------------------------------  else  ------
    else
      oErrorObj := Stb$security.checkGroupRight(sParameter, sMenuAllowed, STB$OBJECT.getCurrentReporttypeId);
  end case;

  isr$trace.stat ('end',  'sMenuAllowed:' || sMenuAllowed, sCurrentName);
  return oErrorObj;
exception
  when others then
    sMsg :=  utd$msglib.getmsg ('exUnexpectedError', nCurrentLanguage, csP1 => sqlerrm(sqlcode));
    oErrorObj.handleError ( Stb$typedef.cnSeverityCritical, sMsg, sCurrentName,
                            substr(dbms_utility.format_error_stack || CHR(10) || dbms_utility.format_error_backtrace, 1,4000)  );
    return oErrorObj;
END setMenuAccess;


--*****************************************************************************************************
function getNodeChilds ( oSelectedNode in  STB$TREENODE$RECORD,
                         olNodeList    out STB$TREENODELIST    )
  return STB$OERROR$RECORD
is
  sCurrentName     constant varchar2(4000) := $$PLSQL_UNIT||'.getNodeChilds('||oSelectedNode.sNodeType||')';
  oErrorObj        STB$OERROR$RECORD       := STB$OERROR$RECORD();
  sMsg             varchar2(4000);
  sMdmTable        varchar2(30);
  sCurIcon         isr$icon.iconid%type;
  sGroupAcceptance varchar2(100);
  sMdmMethod       varchar2(50) := nvl(trim(upper(STB$UTIL.getSystemParameter('MASTER_DATA_MANAGEMENT_METHOD'))),'DISABLED');  -- 'ISR_MDM_DEFAULT','DISABLED'

begin
  isr$trace.stat ('begin','begin', sCurrentName);
  isr$trace.debug('parameter','olNodeList',sCurrentName,olNodeList);
  olNodeList := STB$TREENODELIST ();

  -- NodeTypes
  case
    when sMdmMethod = 'DISABLED' then
      -- nothing to do
      isr$trace.debug('variable','sMdmMethod: '||sMdmMethod,sCurrentName);
    when oSelectedNode.sNodetype = 'MDM' then
      -- MDM top nodetype: MDM
      for recMdmNode in curGetMdmEntity( csFilter => 'ENABLED' )
      loop
        sMdmTable := 'MDM$'||recMdmNode.entityname;

        -- check group acceptance status
        sGroupAcceptance := checkGroupAcceptance( sMdmTable );

        -- check icon
        case
          when sGroupAcceptance = 'NO_TABLE' then
            sCurIcon := 'MDM_NODATA';
          when sGroupAcceptance = 'NO_DATA' then
            sCurIcon := 'MDM_NODATA';
          when sGroupAcceptance = 'ALL_GROUPS_ACCEPTED' then
            sCurIcon := 'MDM_DATA_RESOLVED';
          when sGroupAcceptance = 'UNACCEPTED_GROUP_EXISTS' then
            sCurIcon := 'MDM_DATA_UNRESOLVED';
          else -- sGroupAcceptance = 'UNKNOWN'
            -- this case should not occur
            sCurIcon := oSelectedNode.sIcon;
        end case;

        -- create list element
        olNodeList.extend;                                                                  -- new list element
        olNodeList(olNodeList.count()) := STB$TREENODE$RECORD ();                           -- initialize new list element
        olNodeList(olNodeList.count()).sDisplay        := recMdmNode.entityname;            -- assign values to new list element ...
        olNodeList(olNodeList.count()).sPackageType    := oSelectedNode.sPackageType;
        olNodeList(olNodeList.count()).sNodeType       := 'MDMENTITY';
        olNodeList(olNodeList.count()).sNodeId         := 'mdm'||recMdmNode.entityname;
        olNodeList(olNodeList.count()).sIcon           := sCurIcon;
        olNodeList(olNodeList.count()).sDefaultAction  := oSelectedNode.sDefaultAction;
        olNodeList(olNodeList.count()).sTooltip        := '';
        olNodeList(olNodeList.count()).tloPropertyList := oSelectedNode.tloPropertyList;
        olNodeList(olNodeList.count()).tloValueList    := oSelectedNode.tloValueList;
        olNodeList(olNodeList.count()).nDisplayType    := oSelectedNode.nDisplayType;
        STB$OBJECT.setDirectory( olNodeList(olNodeList.count()) );
      end loop;
    when oSelectedNode.sNodetype = 'MDMENTITY' then
      -- MDM sublevel nodetype: MDM --> MDMENTITY
      null;
    else
      -- Handle unknown Nodetype
      raise exUnknownNodetype;
  end case;

  isr$trace.debug('parameter','olNodeList OUT',sCurrentName,olNodeList);
  isr$trace.stat ('end','end', sCurrentName );
  return (oErrorObj);
exception
  when exUnknownNodetype then
    oErrorObj.handleError ( Stb$typedef.cnSeverityWarning, 'Unexpected nodetype: oSelectedNode.sNodetype <> MDM', sCurrentName,
                            substr(dbms_utility.format_error_stack || CHR(10) || dbms_utility.format_error_backtrace, 1,4000)  );
    return oErrorObj;
  when others then
    rollback;
    sMsg :=  utd$msglib.getmsg('exUnexpectedError', nCurrentLanguage, csP1 => sqlerrm(sqlcode));
    oErrorObj.handleError ( Stb$typedef.cnSeverityCritical, sMsg, sCurrentName,
                            substr(dbms_utility.format_error_stack || CHR(10) || dbms_utility.format_error_backtrace, 1,4000)  );
    return oErrorObj;

end getNodeChilds;


-- ***************************************************************************************************
function getNodeList( oSelectedNode in  STB$TREENODE$RECORD,
                      olNodeList    out STB$TREENODELIST )
  return STB$OERROR$RECORD
is
  sCurrentName       constant varchar2(4000) := $$PLSQL_UNIT||'.getNodeList('||oSelectedNode.sNodeType||')';
  sMdmMethod         varchar2(50) := nvl(trim(upper(STB$UTIL.getSystemParameter('MASTER_DATA_MANAGEMENT_METHOD'))),'DISABLED');  -- 'ISR_MDM_DEFAULT','DISABLED'
  sEntity            varchar2(30)            := oSelectedNode.sDisplay;
  sMdmTableName      varchar2(30)            := 'MDM$'||sEntity;
  oErrorObj          STB$OERROR$RECORD       := STB$OERROR$RECORD();
  nCouLoop           number                  := 0;
  sMsg               varchar2(4000);
  sMdmUnconfirmedOnly varchar2(1);
  tloValueList       ISR$VALUE$LIST;
  olParameter        STB$TREENODELIST;
  sColumnList        varchar2(4000);
  sSql               varchar2(32000);

  cursor curMdmColumns is
    select *
    from   user_tab_columns
    where  table_name = sMdmTableName
    order by column_id;

  recMdmColumns curMdmColumns%rowtype;

begin
  isr$trace.stat('begin', 'oSelectedNode.sNodeType: '||oSelectedNode.sNodeType, sCurrentName );
  isr$trace.debug('parameter','oSelectedNode',sCurrentName,oSelectedNode);
  olNodeList  := STB$TREENODELIST();


  if sMdmMethod = 'DISABLED' then
    -- nothing to do
    null;

  elsif oSelectedNode.sNodeType = 'MDM' then

    olNodeList.EXTEND;
    olNodeList(1) := STB$TREENODE$RECORD( sPackageType => 'ISR$MDM',
                                          sNodeType    => 'MDMENTITY',
                                          tloValueList => oSelectedNode.tloValueList );
    sSql :=  q'^select *
                from   ( with base as ( select sem.entityname,
                                               nvl(ent.mdm,'F') mdm,
                                               sem.mdmmaster,
                                               srv.alias servername,
                                               srt.servertypename,
                                               tm.targetmodel_name targetmodel,
                                               nvl(max(mdmmaster) over( partition by sem.entityname ),'F')  MDMMASTER_EXISTS
                                        from   isr$server$entity$mapping sem
                                        left join isr$servertype$targetmodel tm
                                        on     tm.targetmodelid = sem.targetmodelid
                                        left join isr$server srv
                                        on     srv.serverid = sem.serverid
                                        left join ISR$VALIDVALUE$SERVERTYPE srt
                                               on srt.servertypeid = srv.servertypeid
                                        left join isr$meta$entity ent
                                               on ent.entityname = sem.entityname )
                        select distinct
                               entityname  NODEID,
                               'MDM'       NODETYPE,
                               entityname  NODENAME,
                               mdm,
                               'ISR$MDM'   PACKAGENAME,
                               mdmmaster_exists  MDMMASTER,
                               case when mdmmaster = 'T' then servername
                                    else null
                               end  SERVERNAME,
                               case when mdmmaster = 'T' then servertypename
                                    else null
                               end  SERVERTYPENAME,
                               case when mdmmaster = 'T' then targetmodel
                                    else null
                               end  targetmodel
                        from   base  )
                where   not( mdmmaster = 'T' and servername = 'NO_MDM_MASTER' )
                order by 1^';


    isr$trace.debug ('sSql','sNodeType = MDM ... STB$UTIL.getColListMultiRow',sCurrentName,sSql);
    oErrorObj := STB$UTIL.getColListMultiRow (sSql, olNodeList);

  else -- oSelectedNode.sNodeType = 'MDMENTITY'
    case checkMdmTable( sMdmTableName )
      when 'NO_TABLE' then
        isr$trace.debug('status','MDM-Table '||sMdmTableName||' does not exist.',sCurrentName);
      when 'NO_DATA_FOUND' then
        isr$trace.debug('status','MDM-Table '||sMdmTableName||' does exist, but does not contain any data.',sCurrentName);
      else --  'OK' --> main processing
        sMdmUnconfirmedOnly := nvl( STB$UTIL.getSystemParameter('MDM_DISPLAY_UNCONFIRMED_ONLY'), 'F' );
        isr$trace.debug('variable','sMdmUnconfirmedOnly: '||sMdmUnconfirmedOnly,sCurrentName);

        olNodeList.EXTEND;
        olNodeList(1) := STB$TREENODE$RECORD( sPackageType => 'ISR$MDM',
                                              sNodeType    => 'MDMRECORD',
                                              tloValueList => oSelectedNode.tloValueList );
        -- get column_list
        select listagg(attrname,',') within group( order by orderno )
        into   sColumnList
        from   isr$meta$attribute
        where  entityname = sEntity
        and    nvl(istemp,'F') = 'F';
        sColumnList := lower('mdm_resolution,mdm_user_record,mdm_group_accepted,'||sColumnList);

        sSql := ' select '''||sEntity||'@@''||checksum||''@@''||aaa.serverid      nodeid,
                         ''MDMRECORD'' nodetype,
                         '||isr$util.getSplit(isr$mdm.getEntityPkList(sEntity),1)||' nodename,
                         case when  mdm_user_record = ''T'' and  mdm_resolution = ''T'' and mdm_group_accepted = ''T''  then ''MDM_RECORD_USER_RESOLUTION_ACCEPTED''
                              when  mdm_user_record = ''T'' and  mdm_resolution = ''T''                                 then ''MDM_RECORD_USER_RESOLUTION''
                              when  mdm_user_record = ''T''                             and mdm_group_accepted = ''T''  then ''MDM_RECORD_USER_ACCEPTED''
                              when  mdm_user_record = ''T''                                                             then ''MDM_RECORD_USER''
                              when                               mdm_resolution = ''T'' and mdm_group_accepted = ''T''  then ''MDM_RECORD_SYSTEM_RESOLUTION_ACCEPTED''
                              when                               mdm_resolution = ''T''                                 then ''MDM_RECORD_SYSTEM_RESOLUTION''
                              when                                                          mdm_group_accepted = ''T''  then ''MDM_RECORD_SYSTEM_ACCEPTED''
                                                                                                                        else ''MDM_RECORD_SYSTEM''
                         end  icon,'
                         ||sColumnList||',
                         ''ISR$MDM''   PACKAGENAME,'||
                         ' ( select alias||''( ''||serverid||'' )''from isr$server where serverid=aaa.serverid ) server,' ||
                         ' aaa.modifiedon modifiedon'||
                         ' from '||sMdmTableName||' aaa '||
                         ' where  nvl(STB$UTIL.getSystemParameter(''MDM_DISPLAY_UNCONFIRMED_ONLY''),''F'') = ''F'''||
                         ' or    STB$UTIL.getSystemParameter(''MDM_DISPLAY_UNCONFIRMED_ONLY'') = ''T'' and  mdm_group_accepted = ''F'''||
                         ' order by 3, decode(mdm_user_record,''T'',1,2)';

        isr$trace.debug ('sSql','else ... STB$UTIL.getColListMultiRow',sCurrentName,sSql);
        oErrorObj := STB$UTIL.getColListMultiRow (sSql, olNodeList);

    end case;
  end if;

  if  oErrorObj.sSeverityCode != Stb$typedef.cnNoError then
    raise exUnexpectedError;
  end if;

  isr$trace.debug('parameter','olNodeList OUT',sCurrentName,olNodeList);
  isr$trace.stat ('end','end',sCurrentName );
  return oErrorObj;
exception
  when others then
    rollback;
    sMsg :=  utd$msglib.getmsg ('exUnexpectedError', nCurrentLanguage, csP1 => sqlerrm(sqlcode));
    oErrorObj.handleError ( Stb$typedef.cnSeverityCritical, sMsg, sCurrentName,
                            substr(dbms_utility.format_error_stack || CHR(10) || dbms_utility.format_error_backtrace, 1,4000)  );
    return oErrorObj;
end getNodeList;


-- ***************************************************************************************************
function getContextMenu ( nMenu           in  number,
                          olMenuEntryList out STB$MENUENTRY$LIST )
  return STB$OERROR$RECORD
is
  sCurrentName    constant varchar2(4000) := $$PLSQL_UNIT||'.getContextMenu';
  oErrorObj       STB$OERROR$RECORD       := STB$OERROR$RECORD();
  sMsg            varchar2(4000);
begin
  isr$trace.stat('begin', 'nMenu: '||nMenu||'  Stb$object.getCurrentNodeType: '||Stb$object.getCurrentNodeType, sCurrentName);

  oErrorObj := Stb$util.getContextMenu( Stb$object.getCurrentNodeType, nMenu, olMenuEntryList );

  isr$trace.debug('parameter','olMenuEntryList',sCurrentName,olMenuentryList);
  isr$trace.stat('end','end',sCurrentName );
  return (oErrorObj);
exception
  when others then
    rollback;
    sMsg :=  utd$msglib.getmsg ('exUnexpectedError', nCurrentLanguage, csP1 => sqlerrm(sqlcode));
    oErrorObj.handleError ( Stb$typedef.cnSeverityCritical, sMsg, sCurrentName,
                            substr(dbms_utility.format_error_stack || CHR(10) || dbms_utility.format_error_backtrace, 1,4000)  );
    return oErrorObj;
end getContextMenu;


-- ***************************************************************************************************
function callFunction( oParameter in  STB$MENUENTRY$RECORD,
                       olNodeList out STB$TREENODELIST)
  return STB$OERROR$RECORD
is
    sCurrentName         constant varchar2(4000)      := $$PLSQL_UNIT||'.callFunction('||oParameter.sToken||')';
    oErrorObj                     STB$OERROR$RECORD   := STB$OERROR$RECORD();
    oCurNode                      STB$TREENODE$RECORD := STB$TREENODE$RECORD();
    sList                         varchar2(4000);
    sMsg                          varchar2(4000);
    olParameter                   STB$PROPERTY$LIST;
    tloPropertylist1              STB$PROPERTY$LIST   := STB$PROPERTY$LIST();
    oParamListRec                 ISR$PARAMLIST$REC   := ISR$PARAMLIST$REC();
    sAuditExists                  varchar2 (1) := 'F';
    sEsigExists                   varchar2 (1) := 'F';
    sExists                       varchar2(1);
    sToken                        varchar2(500);
    sNodeId                       varchar2(4000);
    cmd                           varchar2(4000);
    nCounted                      number;
    sMdmTableName                 varchar2(30);
    sPkValue                      varchar2(4000);
    sPkList                       varchar2(4000);
    sChecksum                     varchar2(100);
    sEntityName                   varchar2(30);
    sGroupAcceptanceStatus        varchar2(30);
    nServerId                     number;
begin
  isr$trace.stat('begin','begin',sCurrentName);
  isr$trace.debug('parameter','oParameter',sCurrentName,oParameter);

  STB$OBJECT.setCurrentToken(oparameter.stoken);
  oCurNode := Stb$object.getCurrentNode;
  isr$trace.debug('value','oCurNode', sCurrentName,oCurNode);

  olNodeList := STB$TREENODELIST ();


  case
    -----------------------------------------------------------------------------  CAN_DISABLE_MDMENTITY  ------
    when oParameter.sToken = 'CAN_DISABLE_MDMENTITY' THEN
      update isr$meta$entity
      set    mdm = 'F'
      where  entityname = oCurNode.sDisplay;

      nCounted :=  sql%rowcount;

      if nCounted > 0 then
        isr$trace.debug('status','MDM disabled for entity ' ||oCurNode.sDisplay,sCurrentName);
        commit;
        ISR$TREE$PACKAGE.selectCurrent;
      else
        isr$trace.warn('WARNING','Disabling of MDM for entity '||oCurNode.sDisplay||' failed',sCurrentName);
      end if;
    -----------------------------------------------------------------------------  CAN_ENABLE_MDMENTITY, CAN_CHANGE_MDMMASTER ------
    when oParameter.sToken in ('CAN_ENABLE_MDMENTITY','CAN_CHANGE_MDMMASTER') THEN
      -- create dialog element for olNodeList
      olNodeList := STB$TREENODELIST ();
      olNodeList.extend;
      olNodeList(1) := STB$TREENODE$RECORD(STB$UTIL.getMaskTitle(oParameter.sToken), null, 'TRANSPORT', 'MASK');
      -- load dialog
      oErrorObj := STB$UTIL.getPropertyList(oParameter.sToken, olNodeList(1).tloPropertylist);
    -----------------------------------------------------------------------------  CAN_CREATE_MDM_USER_RECORD  ------
    when oParameter.sToken = 'CAN_CREATE_MDM_USER_RECORD' THEN
      sEntityName   := substr(oCurnode.getValueParameter('MDMENTITY'),4);                  -- name of the Entity
      sMdmTableName := 'MDM$'||sEntityName;  -- name of the MDM-Table
      sPkValue      := oCurnode.sDisplay;                                                  -- Value of the Primary-Key field
      sPkList       := getEntityPkList( sEntityName );                                     -- Primary-Key column list
      sChecksum     := isr$util.getSplit(oCurnode.getValueParameter('MDMRECORD'),2,'@@');  -- checksum of the selected record , -- [ISRC-1118], [ABBIS-219] 30.Jan.2019 vs: getSplit added
      isr$trace.debug('values','sMdmTableName, sPkValue, sPkList, sChecksum',sCurrentName,  'sEntityName:   '||sEntityName||chr(10)
                                                                                          ||'sMdmTableName: '||sMdmTableName||chr(10)
                                                                                          ||'sPkValue:      '||sPkValue||chr(10)
                                                                                          ||'sPkList:       '||sPkList||chr(10)
                                                                                          ||'sChecksum:     '||sChecksum );
      createMdmUserRecord( sEntityName, sMdmTableName, sPkValue, sPkList, sChecksum);
      commit;
      ISR$TREE$PACKAGE.selectCurrent;
    -----------------------------------------------------------------------------  CAN_EDIT_MDM_USER_RECORD  ------
    when oParameter.sToken = 'CAN_EDIT_MDM_USER_RECORD' THEN
      sEntityName   := substr(oCurnode.getValueParameter('MDMENTITY'),4);          -- name of the Entity
      sPkList       := getEntityPkList( sEntityName );                             -- Primary-Key column list
      isr$trace.debug('values','sMdmTableName, sPkValue, sPkList, sChecksum',sCurrentName,  'sEntityName:   '||sEntityName||chr(10)
                                                                                          ||'sPkList:       '||sPkList );
      -- create olNodeList-element for a dynamical dialog
      olNodeList := STB$TREENODELIST ();
      olNodeList.extend;
      olNodeList(1) := STB$TREENODE$RECORD(STB$UTIL.getMaskTitle(oParameter.sToken), null, 'TRANSPORT', 'MASK');
      -- prepare dynamical dialog,                                    olNodeList is OUT parameter
      oErrorObj := editUserRecordDialog( oParameter.sToken, oCurNode, olNodeList, sPkList);
    -----------------------------------------------------------------------------  CAN_DELETE_MDM_USER_RECORD  ------
    when oParameter.sToken = 'CAN_DELETE_MDM_USER_RECORD' THEN
      sEntityName   := substr(oCurnode.getValueParameter('MDMENTITY'),4);                  -- name of the Entity
      sMdmTableName := 'MDM$'||sEntityName;  -- name of the MDM-Table
      sPkValue      := oCurnode.sDisplay;                                                  -- Value of the Primary-Key field
      sPkList       := getEntityPkList( sEntityName );                                     -- Primary-Key column list
      sChecksum     := isr$util.getSplit(oCurnode.getValueParameter('MDMRECORD'),2,'@@');  -- checksum of the selected record

      isr$trace.debug('values','sMdmTableName, sChecksum',sCurrentName,  'sMdmTableName: '||sMdmTableName||chr(10)
                                                                                          ||'sChecksum:     '||sChecksum );
      deleteMdmUserRecord( sMdmTableName, sChecksum);
      commit;
      ISR$TREE$PACKAGE.selectCurrent;
    -----------------------------------------------------------------------------  CAN_SET_MDM_RESOLUTION_RECORD  ------
    when oParameter.sToken = 'CAN_SET_MDM_RESOLUTION_RECORD' THEN
      sEntityName   := substr(oCurnode.getValueParameter('MDMENTITY'),4);          -- name of the Entity
      sMdmTableName := 'MDM$'||sEntityName;  -- name of the MDM-Table
      sPkValue      := oCurnode.sDisplay;                                          -- Value of the Primary-Key field
      sPkList       := getEntityPkList( sEntityName );                             -- Primary-Key column list
      sChecksum     := isr$util.getSplit(oCurnode.getValueParameter('MDMRECORD'),2,'@@');  -- checksum of the selected record
      nServerId     := isr$util.getSplit(oCurnode.getValueParameter('MDMRECORD'),3,'@@');  -- checksum of the selected record
      isr$trace.debug('values','sMdmTableName, sPkValue, sPkList, sChecksum',sCurrentName,  'sEntityName:   '||sEntityName||chr(10)
                                                                                          ||'sMdmTableName: '||sMdmTableName||chr(10)
                                                                                          ||'sPkValue:      '||sPkValue||chr(10)
                                                                                          ||'sPkList:       '||sPkList||chr(10)
                                                                                          ||'sChecksum:     '||sChecksum
                                                                                          ||'nServerId:     '||nServerId);
      setMdmFlag( sMdmTableName, sPkValue, sPkList, sChecksum, 'MDM_RESOLUTION',  null, nServerId );    -- vs [ISRC-1153]
      commit;
      ISR$TREE$PACKAGE.selectCurrent;
    -----------------------------------------------------------------------------  CAN_ACCEPT_MDM_GROUP  ------
    -----------------------------------------------------------------------------  CAN_REVERT_MDM_GROUP_ACCEPTANCE  ------
    when oParameter.sToken in ('CAN_ACCEPT_MDM_GROUP','CAN_REVERT_MDM_GROUP_ACCEPTANCE') THEN
      sEntityName   := substr(oCurnode.getValueParameter('MDMENTITY'),4);          -- name of the Entity
      sMdmTableName := 'MDM$'||sEntityName;  -- name of the MDM-Table
      sPkValue      := oCurnode.sDisplay;                                          -- Value of the Primary-Key field
      sPkList       := getEntityPkList( sEntityName );                             -- Primary-Key column list
      sChecksum     := isr$util.getSplit(oCurnode.getValueParameter('MDMRECORD'),2,'@@');   -- checksum of the selected record
      isr$trace.debug('values','sMdmTableName, sPkValue, sPkList, sChecksum',sCurrentName,  'sEntityName:   '||sEntityName||chr(10)
                                                                                          ||'sMdmTableName: '||sMdmTableName||chr(10)
                                                                                          ||'sPkValue:      '||sPkValue||chr(10)
                                                                                          ||'sPkList:       '||sPkList||chr(10)
                                                                                          ||'sChecksum:     '||sChecksum );
      setMdmFlag( sMdmTableName, sPkValue, sPkList, sChecksum,
                                                              case
                                                                 when oParameter.sToken = 'CAN_ACCEPT_MDM_GROUP' then 'MDM_GROUP_ACCEPTED'
                                                                 when oParameter.sToken = 'CAN_REVERT_MDM_GROUP_ACCEPTANCE' then 'MDM_GROUP_ACCEPTED'
                                                                 else null
                                                              end,
                                                              case
                                                                 when oParameter.sToken = 'CAN_ACCEPT_MDM_GROUP' then 'T'
                                                                 when oParameter.sToken = 'CAN_REVERT_MDM_GROUP_ACCEPTANCE' then 'F'
                                                                 else 'F'
                                                              end );
      commit;
      ISR$TREE$PACKAGE.selectCurrent;
    -----------------------------------------------------------------------------  CAN_CHANGE_ALL_MDMMASTER  ------
    when oParameter.sToken in ('CAN_CHANGE_ALL_MDMMASTER') THEN
      -- create dialog element for olNodeList
      olNodeList := STB$TREENODELIST ();
      olNodeList.extend;
      olNodeList(1) := STB$TREENODE$RECORD(STB$UTIL.getMaskTitle(oParameter.sToken), null, 'TRANSPORT', 'MASK');
      -- load dialog
      oErrorObj := STB$UTIL.getPropertyList(oParameter.sToken, olNodeList(1).tloPropertylist);
    -----------------------------------------------------------------------------  else  ------
    else
      raise exNoTokenHandle;
  end case;

  isr$trace.debug('parameter','olNodeList',sCurrentName,olNodeList);
  isr$trace.stat('end','end',sCurrentName);
  return oErrorObj;
exception
  when exNoTokenHandle then
    sMsg := utd$msglib.getmsg('exNoTokenHandle', stb$security.getcurrentlanguage, oParameter.sToken);
    oErrorObj.handleError( Stb$typedef.cnSeverityWarning, sMsg, sCurrentName, 'user defined exception: exNoTokenHandle'||stb$typedef.crlf||DBMS_UTILITY.format_error_stack ||stb$typedef.crlf|| DBMS_UTILITY.format_error_backtrace );
    rollback;
    return oErrorObj;
  when others then
    oErrorObj.handleError ( Stb$typedef.cnSeverityCritical,
                            utd$msglib.getmsg ('exUnexpectedError', nCurrentLanguage,  csP1 => sqlerrm(sqlcode)),
                            sCurrentName,
                            substr(dbms_utility.format_error_stack ||crlf|| dbms_utility.format_error_backtrace, 1,4000) );
    rollback;
    return oErrorObj;
end callFunction;


--**********************************************************************************************************************
function putParameters( oParameter  in STB$MENUENTRY$RECORD,
                        olParameter in STB$PROPERTY$LIST,
                        clParameter in clob default null)
  return STB$OERROR$RECORD
IS
  sCurrentName        constant varchar2(4000) := $$PLSQL_UNIT||'.putParameters('||oParameter.sToken||')';
  oErrorObj           STB$OERROR$RECORD       := STB$OERROR$RECORD();
  oCurNode            STB$TREENODE$RECORD     := STB$OBJECT.getCurrentNode;
  olNodeList          STB$TREENODELIST        := STB$TREENODELIST ();
  sUserMsg            varchar2(4000);
  nCounted            number;
  sMdmTableName       varchar2(30);
  sPkValue            varchar2(4000);
  sPkList             varchar2(4000);
  sSql                varchar2(32000);
  sEntityName         varchar2(30);

begin
  isr$trace.stat('begin','begin',sCurrentName);
  isr$trace.debug('parameter','oParameter',sCurrentName,oParameter);
  isr$trace.debug('parameter','olParameter',sCurrentName,olParameter);
  isr$trace.debug('parameter','clParameter',sCurrentName,clParameter);
  isr$trace.debug('value','oCurNode',sCurrentName,oCurNode);

  STB$OBJECT.setCurrentToken(oparameter.stoken);

  case
    -----------------------------------------------------------------------------  CAN_DISABLE_MDMENTITY  ------
    when oParameter.sToken = 'CAN_DISABLE_MDMENTITY' THEN
      -- refresh screen
      oErrorObj := getNodeChilds( oCurNode, olNodeList );   -- left side
     -----------------------------------------------------------------------------  CAN_CHANGE_MDMMASTER (for one entity)  ------
     -----------------------------------------------------------------------------  CAN_CHANGE_ALL_MDMMASTER (for all entities)  ------
    when oParameter.sToken in ('CAN_CHANGE_MDMMASTER','CAN_CHANGE_ALL_MDMMASTER') THEN
      -- disable old server setting, if applicable
      update isr$server$entity$mapping
      set    mdmmaster = null
      where  mdmmaster = 'T'
      and    (     serverid <> isr$mdm.getServerId(olParameter(1).sValue)
               or   isr$mdm.getServerId(olParameter(1).sValue) is null    )
      and    (     entityname = oCurnode.sDisplay  and  oParameter.sToken = 'CAN_CHANGE_MDMMASTER'
               or   oParameter.sToken = 'CAN_CHANGE_ALL_MDMMASTER'    );

      nCounted := sql%rowcount;

      -- enable new server setting if applicable
      update isr$server$entity$mapping
      set    mdmmaster = 'T'
      where  nvl(mdmmaster,'*') <> 'T'
      and    serverid   = isr$mdm.getServerId(olParameter(1).sValue)
      and    (     entityname = oCurnode.sDisplay  and  oParameter.sToken = 'CAN_CHANGE_MDMMASTER'
               or   oParameter.sToken = 'CAN_CHANGE_ALL_MDMMASTER'    );

      nCounted :=  nCounted + sql%rowcount;

      update isr$server$entity$mapping
      set    mdmmaster = 'F'
      where  mdmmaster is null;

      nCounted :=  nCounted + sql%rowcount;

      commit;
      oErrorObj := getNodeChilds( oCurNode, olNodeList );
      isr$trace.debug('status','Counted updates: '||nCounted,sCurrentName);

    -----------------------------------------------------------------------------  CAN_ENABLE_MDMENTITY  ------
    when oParameter.sToken = 'CAN_ENABLE_MDMENTITY' THEN

      update isr$meta$entity
      set    mdm = 'T'
      where  entityname = olParameter(1).sValue;

      nCounted :=  sql%rowcount;

      if nCounted > 0 then
        isr$trace.debug('status','MDM for entity '||olParameter(1).sValue||' enabled',sCurrentName);
        -- refresh left and right side of the iSR explorer tree
        commit;
        ISR$TREE$PACKAGE.selectCurrent;
      else
        isr$trace.warn('WARNING','Enabling of MDM for entity '||olParameter(1).sValue||' failed',sCurrentName);
      end if;
    -----------------------------------------------------------------------------  CAN_ACCEPT_MDM_GROUP  ------
    when oParameter.sToken = 'CAN_ACCEPT_MDM_GROUP' THEN
      null;
    -----------------------------------------------------------------------------  CAN_REVERT_MDM_GROUP_ACCEPTANCE  ------
    when oParameter.sToken = 'CAN_REVERT_MDM_GROUP_ACCEPTANCE' THEN
      null;
     -----------------------------------------------------------------------------  CAN_REVERT_MDM_GROUP_ACCEPTANCE  ------
    when oParameter.sToken = 'CAN_SET_MDM_RESOLUTION_RECORD' THEN
      null;
     -----------------------------------------------------------------------------  CAN_REVERT_MDM_GROUP_ACCEPTANCE  ------
    when oParameter.sToken = 'CAN_CREATE_MDM_RESOLUTION_RECORD' THEN
      null;
    -----------------------------------------------------------------------------  CAN_REVERT_MDM_GROUP_ACCEPTANCE  ------
    when oParameter.sToken = 'CAN_EDIT_MDM_USER_RECORD' THEN
      sEntityName   := substr(oCurnode.getValueParameter('MDMENTITY'),4);          -- name of the Entity
      sMdmTableName := 'MDM$'||sEntityName;  -- name of the MDM-Table
      sPkValue      := oCurnode.sDisplay;                                          -- Value of the Primary-Key field
      sPkList       := getEntityPkList( sEntityName );                             -- Primary-Key column list
      isr$trace.debug('values','sEntityname, sMdmTableName, sPkValue, sPkList',sCurrentName,  'sEntityName:   '||sEntityName||chr(10)
                                                                                             ||'sMdmTableName: '||sMdmTableName||chr(10)
                                                                                             ||'sPkValue:      '||sPkValue||chr(10)
                                                                                             ||'sPkList:       '||sPkList );
      updateMdmUserRecord( sEntityName, sMdmTableName, sPkValue, sPkList, olParameter );
      commit;
      ISR$TREE$PACKAGE.selectCurrent;

    -----------------------------------------------------------------------------  CAN_REVERT_MDM_GROUP_ACCEPTANCE  ------
    when oParameter.sToken = 'CAN_DELETE_MDM_USER_RECORD' THEN
      null;
    -----------------------------------------------------------------------------  else  ------
    else
      raise exNotExists;
  end case;

  isr$trace.stat('end','end',sCurrentName);
  return oErrorObj;

exception
  when exNotExists then
    sUserMsg := utd$msglib.getmsg ('exNotExistsText', stb$security.getcurrentlanguage);
    oErrorObj.handleError (stb$typedef.cnSeverityWarning, sUserMsg, sCurrentName, dbms_utility.format_error_stack ||stb$typedef.crlf || dbms_utility.format_error_backtrace  );
    return oErrorObj;
  when others then
    sUserMsg :=  utd$msglib.getmsg ('exUnexpectedError', stb$security.getcurrentlanguage, csP1 => sqlerrm(sqlcode));
    oErrorObj.handleError ( Stb$typedef.cnSeverityCritical, sUserMsg, sCurrentName,
                            substr(dbms_utility.format_error_stack || CHR(10) || dbms_utility.format_error_backtrace, 1,4000)  );
    rollback;
    return oErrorObj;
end putParameters;


-- ***************************************************************************************************
function getLov( csEntity        in  varchar2,
                 csSuchwert      in  varchar2,
                 oSelectionList  out ISR$TLRSELECTION$LIST )
  return STB$OERROR$RECORD
is
  sCurrentName constant varchar2(4000) := $$PLSQL_UNIT||'.getLov('||csEntity||')';
  sUserMsg     varchar2(4000);
  oErrorObj    STB$OERROR$RECORD       := STB$OERROR$RECORD();
  sSuchwert    varchar2(1000)          := csSuchwert;
  oCurNode     STB$TREENODE$RECORD     := STB$OBJECT.getCurrentNode;
  sNodeId      varchar2(1000);

  cursor curMdmMaster( ccsEntity varchar2 ) is
    select servername --,entityname, mdm, mdmmaster
    from   (  select ent.entityname, --ent.mdm,
                     srv.alias servername, -- sem.mdmmaster,
                     case
                       when max(mdmmaster) over( partition by ent.entityname ) is not null   and  sem.mdmmaster is not null then 1
                       when max(mdmmaster) over( partition by ent.entityname ) is not null  then 2
                       else 4
                     end rnk
              from   isr$meta$entity ent
              join   isr$server$entity$mapping sem
              on     ent.entityname = sem.entityname
              join   isr$server srv
              on     srv.serverid = sem.serverid
              where ent.entityname = ccsEntity
              union
              select ccsEntity entityname, utd$msglib.getmsg('NOMDMMASTER$DESC',nCurrentLanguage) servername, 3 rnk from dual  )
    order by rnk, servername;

  recMdmMaster curMdmMaster%rowtype;

  cursor curGetMdmMaster is
    with serverEntityMap as ( select distinct serverid, null alias, null info, mdmmaster,
                                     rank() over( partition by serverid  order by decode(mdmmaster,'T',1,2)) rnk
                              from   isr$server$entity$mapping
                              --union
                              --select 0, 'MultipleMdmMaster','MultipleMdmMasterInfo', 'F', 1 from dual
                              union
                              select -1, 'DISABLE_ALL_MDM_MASTER','DISABLE_ALL_MDM_MASTER$INFO','F',1 from dual
                            ),
                              ------
         countMdmMaster  as ( select nvl(  ( select distinct count(distinct serverid) over( partition by 1)
                                             from   isr$server$entity$mapping
                                             where  mdmmaster = 'T' ), 0  ) cou_master
                              from dual )
    select sem.serverid, nvl(sem.alias,srv.alias) alias, nvl(sem.info,sty.servertypename) info, sem.mdmmaster, mdm.cou_master,
           case when mdm.cou_master is null and sem.serverid = -1 then 1  -- disable mdm settings
                when mdm.cou_master > 1 and sem.serverid = 0 then 1       -- multiple mdm masters
                when mdm.cou_master = 1 and sem.mdmmaster = 'T' then 1    -- current mdm master
                else 2
           end orderno
    from   serverEntityMap sem
    join   countMdmMaster  mdm
    on     sem.rnk = 1
    and    not( cou_master <= 1 and serverid = 0 )
    left join ISR$SERVER   srv
    on     srv.serverid = sem.serverid
    left join ISR$VALIDVALUE$SERVERTYPE  sty
    on     sty.servertypeid in ( 2,10 )           --  2 Remote Collector,  10 Local Collector
    and    sty.servertypeid = srv.servertypeid
    order by orderno,
             decode(serverid,0,1,-1,2,3),
             nvl(sem.alias,srv.alias);

begin
  isr$trace.stat('begin','csSuchwert: '||csSuchwert,sCurrentName);
  isr$trace.debug('value','oCurNode',sCurrentName,oCurNode);
  oSelectionList := ISR$TLRSELECTION$LIST ();

  --------------------------------------------- node example -----
  -- TOPNODE    -4711
  -- MAINADMIN  -30
  -- MDM        -101               (left side)
  -- MDM        SB$PRODUCT         (right side)
  ----------------------------------------------------------------
  sNodeId := oCurNode.getValueParameter( 'MDM' );
  isr$trace.debug('value','sNodeId: '||sNodeId,sCurrentName);

  case
    ------------------------------------------------  ENTITYNAME right side -----
    when csEntity = 'ENTITYNAME' and entityExists(sNodeId) then
      for recEntity in curGetMdmEntity( csFilter => 'DISABLED', csOrderPriority => sNodeId )
      loop
        oSelectionList.extend;
        oSelectionList(oSelectionList.count()) := ISR$OSELECTION$RECORD( sDisplay => recEntity.entityname,
                                                                         sValue   => recEntity.entityname,
                                                                         sInfo    => recEntity.entityname,
                                                                         nOrderNum => oSelectionList.COUNT() );
      end loop;


    ------------------------------------------------  ENTITYNAME left side-----
    when csEntity = 'ENTITYNAME' then
      for recEntity in curGetMdmEntity( csFilter => 'DISABLED' )
      loop
        oSelectionList.extend;
        oSelectionList(oSelectionList.count()) := ISR$OSELECTION$RECORD( sDisplay => recEntity.entityname,
                                                                         sValue   => recEntity.entityname,
                                                                         sInfo    => recEntity.entityname,
                                                                         nOrderNum => oSelectionList.COUNT() );
      end loop;


    ------------------------------------------------  MDMMASTER for one entity -----
    when csEntity = 'MDMMASTER' then

      if sSuchwert is null then
        sSuchwert := oCurNode.sDisplay;
      end if;

      for recMdmMaster in curMdmMaster( sSuchwert )
      loop
        oSelectionList.extend;
        oSelectionList(oSelectionList.count()) := ISR$OSELECTION$RECORD( sDisplay  => recMdmMaster.servername,
                                                                         sValue    => recMdmMaster.servername,
                                                                         sInfo     => recMdmMaster.servername,
                                                                         sSelected => case when oSelectionList.count() = 1 then 'T' else 'F' end,
                                                                         nOrderNum => oSelectionList.COUNT() );
      end loop;
    ------------------------------------------------  MDMMASTER for all Entities -----
    when csEntity = 'MDM_MASTER' then
      for recGetMdmMaster in curGetMdmMaster  loop
        oSelectionList.extend;
        oSelectionList(oSelectionList.count()) := ISR$OSELECTION$RECORD( sDisplay  => recGetMdmMaster.alias,
                                                                         sValue    => recGetMdmMaster.alias,
                                                                         sInfo     => recGetMdmMaster.alias,
                                                                         sSelected => case when oSelectionList.count() = 1 then 'T' else 'F' end,
                                                                         nOrderNum => oSelectionList.COUNT() );
      end loop;
    ------------------------------------------------  else -----
    else
      raise exNotExists;
  end case;

  isr$trace.debug('parameter','oSelectionList',sCurrentName,oSelectionList);
  isr$trace.stat('end','end', sCurrentName);
  return oErrorObj;
exception
  when exNotExists then
    sUserMsg := utd$msglib.getmsg ('exNotExistsText', stb$security.getcurrentlanguage);
    oErrorObj.handleError (stb$typedef.cnSeverityWarning, sUserMsg, sCurrentName, dbms_utility.format_error_stack ||stb$typedef.crlf || dbms_utility.format_error_backtrace  );
    return oErrorObj;
  when others then
    sUserMsg :=  utd$msglib.getmsg ('exUnexpectedError', stb$security.getcurrentlanguage, csP1 => sqlerrm(sqlcode));
    oErrorObj.handleError ( Stb$typedef.cnSeverityCritical, sUserMsg, sCurrentName,
                            substr(dbms_utility.format_error_stack || CHR(10) || dbms_utility.format_error_backtrace, 1,4000)  );
    return oErrorObj;
end getLov;


-- ***************************************************************************************************
function getServerId( cnServerName varchar2 )
  return number
is
  sCurrentName     constant varchar2(4000) := $$PLSQL_UNIT||'.getServerId('||cnServerName||')';
  oErrorObj        STB$OERROR$RECORD       := STB$OERROR$RECORD();
  sUserMsg         varchar2(4000);
  nReturnServerId  number;
begin
  select serverid
  into   nReturnServerId
  from   isr$server
  where  alias = cnServerName;

  return nReturnServerId;
exception
  when no_data_found then
    return -1.234E-12;
  when others then
    sUserMsg :=  utd$msglib.getmsg ('exUnexpectedError', stb$security.getcurrentlanguage, csP1 => sqlerrm(sqlcode));
    oErrorObj.handleError ( Stb$typedef.cnSeverityCritical, sUserMsg, sCurrentName,
                            substr(dbms_utility.format_error_stack || CHR(10) || dbms_utility.format_error_backtrace, 1,4000)  );
    return null;
end getServerId;


--****************************************************************************************************
function checkGroupAcceptance( csMdmTable varchar2 )
  return varchar2
is
  sCurrentName     constant varchar2(4000) := $$PLSQL_UNIT||'.checkGroupAcceptance('||csMdmTable||')';
  oErrorObj        STB$OERROR$RECORD       := STB$OERROR$RECORD();
  sUserMsg         varchar2(4000);
  sReturnStatus    varchar2(100);
  nCounted         number;
begin
  isr$trace.stat('begin','begin',sCurrentName);

  case
    when not tableExists( csMdmTable ) then
      sReturnStatus := 'NO_TABLE';
    when not recordExists( csMdmTable ) then
      sReturnStatus := 'NO_DATA';
    when uncacceptedMdmGroupExists( csMdmTable ) then
      sReturnStatus := 'UNACCEPTED_GROUP_EXISTS';
    else
      sReturnStatus := 'ALL_GROUPS_ACCEPTED';
  end case;

  return sReturnStatus;
  isr$trace.stat('end','sReturnStatus: '||sReturnStatus,sCurrentName);
exception
  when others then
    sUserMsg :=  utd$msglib.getmsg ('exUnexpectedError', stb$security.getcurrentlanguage, csP1 => sqlerrm(sqlcode));
    oErrorObj.handleError ( Stb$typedef.cnSeverityWarning, sUserMsg, sCurrentName,
                            substr(dbms_utility.format_error_stack || CHR(10) || dbms_utility.format_error_backtrace, 1,4000)  );
    return 'UNKNOWN';
end checkGroupAcceptance;


--****************************************************************************************************
function editUserRecordDialog( csToken    in     varchar2,
                               oNode      in     STB$TREENODE$RECORD,
                               olNodeList in out STB$TREENODELIST,
                               csPkList   in     varchar2 )
  return STB$OERROR$RECORD
is
  sCurrentName     constant varchar2(4000) := $$PLSQL_UNIT||'.editUserRecordDialog('||csToken||')';
  oErrorObj        STB$OERROR$RECORD       := STB$OERROR$RECORD();
  sUserMsg         varchar2(4000);
  sPkList          varchar2(4000)          := upper(','||csPkList||',');
begin
  isr$trace.stat('begin','begin',sCurrentName);
  isr$trace.debug('parameter','oNode',sCurrentName,oNode);
  isr$trace.debug('parameter','olNodeList IN OUT',sCurrentName,olNodeList);

  olNodeList(1).tloPropertyList := oNode.tloPropertyList;

  -- change individual data before dialog --> allows editing
  for i in 1..olNodeList(1).tloPropertyList.count()
  loop
    case when    olNodeList(1).tloPropertyList(i).sParameter in ('MDM_RESOLUTION','MDM_USER_RECORD','MDM_GROUP_ACCEPTED','SERVER')   -- these columns are not to be changed
              or instr( sPkList, ','||olNodeList(1).tloPropertyList(i).sParameter||',' ) > 0 then                                                -- primary key columns may not be changed as well
        -- nothing to do
        null;
      else
        -- all other columns are editable
        olNodeList(1).tloPropertyList(i).sEditable := 'T';
    end case;
  end loop;

  isr$trace.debug('parameter','olNodeList IN OUT',sCurrentName,olNodeList);
  isr$trace.stat('end','end',sCurrentName);
  return oErrorObj;
exception
  when others then
    sUserMsg :=  utd$msglib.getmsg ('exUnexpectedError', stb$security.getcurrentlanguage, csP1 => sqlerrm(sqlcode));
    oErrorObj.handleError ( Stb$typedef.cnSeverityWarning, sUserMsg, sCurrentName,
                            substr(dbms_utility.format_error_stack || CHR(10) || dbms_utility.format_error_backtrace, 1,4000)  );
end editUserRecordDialog;


end ISR$MDM;