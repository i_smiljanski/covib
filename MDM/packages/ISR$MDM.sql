CREATE OR REPLACE package ISR$MDM
as
-- ***************************************************************************************************
-- Description/Usage:
-- This package contains functionality for the Master Data Management (MDM)
-- The main task of MDM is the filtering of doublets. Another task is a server related data custumization of metadata, which is also called fixed_rule.
-- 
-- Transaction control
--   Transaction control is out of scope of this package. Thus transaction control must be handled from the calling module.
--
-- Definitions:
--   Temporary Table
--     Temporary tables belong to an entity and are generally realized as global temporary tables (GTT)
--     Temporary tables have the name pattern TMP$<entity_name_in_uppercase>.
--   MDM Table
--     MDM tables belong to an entity and are only created, if MDM rules and settings apply.
--     MDM tables have the name pattern MDM$<entity_name_in_uppercase> and are created from program code in this package only.
--     The decision, which entry finally remains in the temporary table, is made by means of the MDM table of an entity. For that
--     purpose, a resolution dataset is built for each existing primary key attribute.
--   Doublet
--     Within an entity, we speak about doublets, if there are more than one dataset with the same primary key.
--   Doublet-Variant
--     Doublet variants exist if there are doublets - which have idenical primary key column(s) - but any other attribute of the entity has some 
--     different contents. The default column with the name CHECKSUM is used for the identification of variants.
--
-- Important Issues for coding:
--   [ISRC-109],[ISRC-198]
--   [ARDIS-494]
-- 
-- Prerequisites:
--   It is expeced, that ISR$METADATA$PACKAGE.chkEntityTables ensures a clean structure of entities and respectively related temporary tables.
--   nevertheless, some structural checks are embedded in this package.
--   The definition of the default columns, like checksum apply to the temporary tables and to the mdm tables 
--   and is realized in ISR$METADATA$PACKAGE.GetBaseEntity. Any change of these scructures must be considered in this package as well.
--
-- Audit:
--   An audit record is written into the audit trail, if one or more resolution sets are created in the MDM-Table.
--
-- Configuration Parameters:
--   System-Parameter: 'MASTER_DATA_MANAGEMENT_METHOD'
--                      Values: 'ISR_MDM_DEFAULT', 
--                              'ISR_PK_CHECKSUM'
--                              'DISABLED'
--                               null or not set ==> 'DISABLED'
--   Table/Column ISR$META$ENTITY.MDM: 'T' = TRUE  --> MDM rules apply to MDM processing
--                                     'F' = FALSE --> no MDM processing
-- 
-- ***************************************************************************************************

--****************************************************************************************************
function getXmlNlsParameters
  return xmltype;
-- ***************************************************************************************************
-- Date und Autor: 25 Apr 2016, vs
-- fetch data from v$nls_parameters and return the canonical xml description
--
-- Parameters:
--             no parameters
--
-- Return:
--   xmltype
-- ***************************************************************************************************    

function tableExists( csTableName varchar2 )
  return boolean;
-- ***************************************************************************************************
-- Date und Autor: 30 May 2015, vs
-- checks, if a table exists and returns a boolean
--
-- Parameters:
-- csTableName   table to be checked
--
-- Return:
--   boolean
-- ***************************************************************************************************  

function recordExists( csTableName varchar2 )
  return boolean;
-- ***************************************************************************************************
-- Date und Autor: 27 Jul 2018, vs
-- checks, if records exist in a table
--
-- Parameters:
-- csTableName   table to be checked
--
-- Return:
--   boolean
-- ***************************************************************************************************

function uncacceptedMdmGroupExists( csTableName varchar2 )
  return boolean;
-- ***************************************************************************************************
-- Date und Autor: 27 Jul 2018, vs
-- checks, if there are unaccepted groups in an MDM table
--
-- Parameters:
-- csTableName   table to be checked
--
-- Return:
--   boolean
-- ***************************************************************************************************

function checkEntityAttributes( csEntity in isr$meta$entity.entityname%type ) 
  return boolean;
-- ***************************************************************************************************
-- Date und Autor: 30 May 2015, vs
-- checks the structure of an entity in table isr$meta$attribute
--
-- Parameters:
-- csEntity   the name of the entity
--
-- Return:
--   boolean
-- ***************************************************************************************************

function getEntityAttributeList( csEntity in isr$meta$entity.entityname%type ) 
  return varchar2;
-- ***************************************************************************************************
-- Date und Autor: 30 May 2015, vs
-- receives the attribute-list of an entiy due to the definition in isr$meta$attribute
--
-- Parameters:
-- csEntity   the name of the entity
--
-- Return:
--   the comma-separated attribute list
-- ***************************************************************************************************

function getEntityPkList( csEntity in isr$meta$entity.entityname%type ) 
  return varchar2;
-- ***************************************************************************************************
-- Date und Autor: 30 May 2015, vs
-- receives the attribute-list of primary key columns or entiy due to the definition in isr$meta$attribute
--
-- Parameters:
-- csEntity   the name of the entity
--
-- Return:
--   the comma-separated pk-attribute list
-- ***************************************************************************************************

function getColumnList( csTableName in varchar2 ) 
  return varchar2;
-- ***************************************************************************************************
-- Date und Autor: 30 May 2015, vs
-- receives the column-list of a table from user_tab_columns
--
-- Parameters:
-- csTableName   the name of the table
--
-- Return:
--   the comma-separated column list
-- ***************************************************************************************************

function isMdmEntity( csEntity in isr$meta$entity.entityname%type ) 
  return boolean;
-- ***************************************************************************************************
-- Date und Autor: 30 May 2015, vs
-- checks isr$meta$entity.mdm
--
-- Parameters:
-- csEntity   the name of the entity
--
-- Return:
--   boolean    TRUE  for isr$meta$entity.mdm = 'T'
--              FALSE for isr$meta$entity.mdm = 'F'
-- ***************************************************************************************************

function checkChecksum( csTmpTableName in varchar2 ) 
  return number;
-- ***************************************************************************************************
-- Date und Autor: 30 May 2015, vs
-- checks the result of the column checksum
--
-- Parameters:
-- csTmpTableName   the name of the temporary table
--
-- Return:
--   number of found errors   ( 0 = no error )
-- ***************************************************************************************************

function checkServerDoublets( csTmpTableName in varchar2,
                              csPkList       in varchar2 ) 
  return number;
-- ***************************************************************************************************
-- Date und Autor: 30 May 2015, vs
-- check and count doublets from same server
--
-- Parameters:
-- csTmpTableName   the name of the temporary table
-- csPkList         the comma-separated pk-list
--
-- Return:
--   number of found dublets from same server      ( 0 = no error )
-- ***************************************************************************************************

function couDoubletVariants( csTmpTableName in varchar2,
                             csPkList       in varchar2 ) 
  return number;
-- ***************************************************************************************************
-- Date und Autor: 30 May 2015, vs
-- check and count doublets variants
--
-- Parameters:
-- csTmpTableName   the name of the temporary table
-- csPkList         the comma-separated pk-list
--
-- Return:
--   number of found dublet variants
-- ***************************************************************************************************

function clearDoubletsFromTempTable( csTmpTableName in varchar2,
                                     csPkList       in varchar2 ) 
  return number;
-- ***************************************************************************************************
-- Date und Autor: 30 May 2015, vs
-- deletes unncessary doublet variants from a temporary table
--
-- Parameters:
-- csTmpTableName   the name of the temporary table
-- csPkList         the comma-separated pk-list
--
-- Return:
--   number of deleted dublet variants in temporary table
-- ***************************************************************************************************

function processDoubletVariants( csTmpTableName  in varchar2,
                                 csMdmTableName  in varchar2,
                                 csPkList        in varchar2,  
                                 csRuledPkList   in varchar2,
                                 csAttributeList in varchar2,
                                 csColumnList    in varchar2 ) 
  return number;
-- ***************************************************************************************************
-- Date und Autor: 31 May 2015, vs
-- Does all the processing steps for doublet variants in MDM$-table and in TMP$-table
--
-- Parameters:
-- csTmpTableName   the name of the temporary table
-- csMdmTableName   the name of the MDM$-table
-- csPkList         comma-separated list, that defines the primary key
-- csAttributeList  comma-separated list of all attributes (including PK columns), defined by isr$meta$entity
-- csColumnList     comma-separated list of all columns defined by user_tab_columns
--
-- Return:
--   number of processed doublets
-- ***************************************************************************************************

function processMdm( csEntity in isr$meta$entity.entityname%type ) 
  return STB$OERROR$RECORD;
-- ***************************************************************************************************
-- Date and Autor:  05. Jun 2015 vs
-- The function processMdm( csEntity ) is the main processing functionality in this package.
-- It controls the whole processing flow.
--
-- Parameter:  
--   csEntity       The name of the entity, defined in isr$meta$entity.entityname, comparison is case sensitive
--
-- Return:
--   STB$OERROR$RECORD  
--
-- List of defined return values and message(s):
--   STB$TYPEDEF.cnNoError
--      All entities and column definitions, including key columns are successfully checked
--   STB$TYPEDEF.cnSeverityCritical
--      exMetaNokey                 No key criteria in metadata defined for entity.
--      exCouDoubletVariants:       The number of doublet-variants from different servers could not be determined.
--      exPkColumnList:             Cannot determine a primary key column list from temporary table.
--      exCreateTable:              MDM$-Table could not be created.
--      exAttributesMissing         Cannot determine attributes for entity.
--      exNoTable                   An expected table does not exist. 
--      exCheckSum                  Wrong checksum
--      exDoubletsFromSameServer    Doublets found in temporary table, which belong to the same server.
--      exVariantProcessing         In the temporary table, existing doublet-variants couldn't be properly processed. 
--      exClearDoublets             In the temporary table, existing doublets couldn't be properly deleted.
--      exMdmFinalCheck             Errors found in temporary table during the final check of the Master Data Management process.
--      exCreateAudit               An error occurred during the creation of an audit.
--
--  Supported MASTER_DATA_MANAGEMENT_METHODs:
--      ISR_MDM_DEFAULT             Expects exactly one primary key column in the table ISR$META$ATTRIBUTE (column ISKEY='T')
--                                  and uses this key for the identification of doublets only.
--      ISR_PK_CHECKSUM             Expects exactly one primary key column in the table ISR$META$ATTRIBUTE (column ISKEY='T').
--                                  This key is combined with the checksum for the identification of doublets.
--      DISABLED                    This entry is used, if there is no defined MDM method.
--      
-- Process Control:
--   RAISE if any ERROR occurs within code ..
--   IF (     ISR$META$ENTITY.MDM(entity) = TRUE   AND  (  SYSTEMPARAMETER('MASTER_DATA_MANAGEMENT_METHOD')= 'ISR_MDM_DEFAULT'
--        or  ISR$META$ENTITY.MDM(entity) = TRUE   AND  (  SYSTEMPARAMETER('MASTER_DATA_MANAGEMENT_METHOD')= 'ISR_PK_CHECKSUM'   ) THEN
--        Prerequisites ok, start MDM processing for entity       
--        check temporary table.          FUNCTION tableExists(tmpTable),            
--        get primary key columns.        FUNCTION getEntityPkList(entity)           
--        get column list from temp table FUNCTION getColumnList(tmpTable)           
--        get attribute list              FUNCTION getEntityAttributeList(entity)    
--        build checksums in tmptable     UPDATE-statement                           
--        check doublets from same server FUNCTION checkServerDoublets(tmpTable)     
--        handle doublet variants from different servers FUNCTION couDoubletVariants 
--        IF doublet variants exist
--           create mdm-table if it does not exist  ISR$METADATA$PACKAGE.CreateEntityTable 
--           Process doublet variants      FUNCTION processDoubletVariants             
--              Insert new variants into MDM$-table                                             
--              Insert lacking SYSTEM_RESOLUTION sets tion MDM$-table  
--              IF any new lacking SYSTEM_RESOLUTION inserted then
--                 write audit                                                        
--              END IF
--           Insert RESOLUTION datasets from MDM$-table into tmpTable, if such a resolution set (with same checksum) does not exist
--           cleanup doublet variants from tmpTable
--        END IF
--        Handle doublets with same checksum from different servers, one dataset remains in TMP-table, others are deleted. 
--          FUNCTION clearDoubletsFromTempTable            
--        check final result FUNCTION couDoubletVariants    
--  END IF;
--
-- ***************************************************************************************************  

procedure cleanupServerDoublets( csTmpTableName varchar2,
                                 csScope        varchar2,
                                 csPkList       varchar2 );
-- ***************************************************************************************************
-- Date und Autor: 14.Mar.2019, vs
-- Parameter csPkList added          
-- ***************************************************************************************************

function processFixedRules( csEntity in isr$meta$entity.entityname%type ) 
  return STB$OERROR$RECORD;
-- ***************************************************************************************************
-- Date und Autor: 18 Jun 2015, vs
-- processes fixed rules for an entity
--
-- Parameters:
--   csEntity       The name of the entity, defined in isr$meta$entity.entityname, comparison is case sensitive
--
-- Return:
--   number of processed fixed rules
-- ***************************************************************************************************

function setCurrentNode( oSelectedNode in STB$TREENODE$RECORD ) return STB$OERROR$RECORD;
-- ***************************************************************************************************
-- Date und Autor: 02 Sept 2015  vs
-- sets tree node information in the package STB$OBJECT.
-- 
-- Nodetypes of the iSR treenode:
--   MDM               Main nodetype of the MDM
--   MDMENTITY         Entity node type (sublevel of MDM)
--
-- Parameter:
-- oSelectedNode       the node object describing the selected iStudyReporter Explorer node
--
-- Return:
-- STB$OERROR$RECORD    
-- ***************************************************************************************************

function setMenuAccess( sParameter   in varchar2, 
                        sMenuAllowed out varchar2)  return STB$OERROR$RECORD;
-- ***************************************************************************************************
-- Date and Autor:  10 Sept 2015 vs
-- checks if a command token is selectable in the context menu. If a command token is
-- selectable, depends on the context of the tree node and the granted system privileges 
-- of the authorization groups the user belongs to
--
-- Parameter:
-- sParameter            the command token of the context menu
-- sMenuAllowed          flag to indicate if the context command token is selectable in the context menu
--
-- Return:
-- STB$OERROR$RECORD    
--
-- ***************************************************************************************************

function GetNodeChilds ( oSelectedNode in  STB$TREENODE$RECORD,
                         olNodeList    out STB$TREENODELIST    ) return STB$OERROR$RECORD;
-- ***************************************************************************************************

function getNodeList( oSelectedNode in  STB$TREENODE$RECORD, 
                      olNodeList    out STB$TREENODELIST )  return STB$OERROR$RECORD;  
-- ***************************************************************************************************

function getContextMenu( nMenu          in  number,  
                        olMenuEntryList out STB$MENUENTRY$LIST ) return STB$OERROR$RECORD;
-- ***************************************************************************************************

function callFunction( oParameter in  STB$MENUENTRY$RECORD, 
                       olNodeList out STB$TREENODELIST)
  return STB$OERROR$RECORD;
--****************************************************************************************************

function putParameters( oParameter  in STB$MENUENTRY$RECORD, 
                        olParameter in STB$PROPERTY$LIST,
                        clParameter in clob default null)
  return STB$OERROR$RECORD;
--****************************************************************************************************  

function getLov( csEntity        in  varchar2,  
                 csSuchwert      in  varchar2,
                 oSelectionList  out ISR$TLRSELECTION$LIST ) 
  return STB$OERROR$RECORD;
--****************************************************************************************************    

function getServerId( cnServerName varchar2 ) 
  return number;
--****************************************************************************************************    

function checkGroupAcceptance( csMdmTable varchar2 ) 
  return varchar2;
--****************************************************************************************************    
-- returns group acceptance status of an MDM-Table
-- Return-Codes:  NO_TABLE
--                NO_DATA
--                ALL_GROUPS_ACCEPTED
--                UNACCEPTED_GROUP_EXISTS
--                UNKNOWN
--****************************************************************************************************    

function userRecordExistsInGroup( csMdmTableName varchar2,
                                  csPkValue      varchar2, 
                                  csPkList       varchar2,
                                  csChecksum     varchar2 )
  return varchar2;
--****************************************************************************************************    
-- returns information, if there exists a user-recod in the group.
-- Return-Codes:  T - user record exists
--                F - user record does not exist
--****************************************************************************************************   


function editUserRecordDialog( csToken    in     varchar2, 
                               oNode      in     STB$TREENODE$RECORD, 
                               olNodeList in out STB$TREENODELIST,
                               csPkList   in     varchar2 )
  return STB$OERROR$RECORD;  
--****************************************************************************************************    
-- create an object for a dynamical record to edit MDM user records
-- Date und Autor: 31.Jul.2018 vs
--****************************************************************************************************     

function setCallback ( csFunction in varchar2 )
  return STB$OERROR$RECORD; 
-- ***************************************************************************************************
-- Date and Autor:  27.Aug.2018 vs
-- defines a callback-routine for processed MDM recods of an unaccepted group.
--
-- Parameter:
-- csFunction            the function to be called
--
-- Return:
-- STB$OERROR$RECORD    
-- ***************************************************************************************************
  
function resetCallback
  return STB$OERROR$RECORD; 
-- ***************************************************************************************************
-- Date and Autor:  27.Aug.2018 vs
-- resets the callback-routine for processed MDM recods of an unaccepted group
--
-- Parameter:
--    no parameters
--
-- Return:
-- STB$OERROR$RECORD    
-- ***************************************************************************************************

function reCalcChecksum ( csTableName in varchar2 default null )
  return STB$OERROR$RECORD;
-- ***************************************************************************************************
-- Date and Autor:  14.Mar.2019 vs   [ISRC-1125]
-- Re-Calculate Checksums with stb$java.calculateHash algorithm
-- In either one MDM-Table or in all MDM-Tables
--
-- Sample Call:
--    declare
--      oErrorObj      STB$OERROR$RECORD := STB$OERROR$RECORD();
--    begin
--      --oErrorObj := isr$mdm.reCalcChecksum;
--      oErrorObj := isr$mdm.reCalcChecksum('MDM$SB$PRODUCT');
--    end;
--    /
--
--
-- Parameter:
--    csTableName   Name of the MDM-Table
--                  or NULL --> Processes all existing MDM-Tables
--
-- Transaction control:
--    No Commit    
--
-- Return:
-- STB$OERROR$RECORD    
-- ***************************************************************************************************


end ISR$MDM;