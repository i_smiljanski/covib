CREATE OR REPLACE PACKAGE isr$docfragment AS
-- ***************************************************************************************************
-- Description/Usage:
-- The package olds all functionality for document fragments 
-- ***************************************************************************************************

 
function createParametersForFragment( cnJobid in     number, cxXml   in out xmltype) return STB$OERROR$RECORD;
-- ***************************************************************************************************
-- Date und Autor: 30 Aug 2016  IS
-- inserts fragment blocks to CONFIG.XML  and saves fragment file in ISR$JOBTRAIL 
--
-- Parameters:
-- cnJobid   the identifier of the iStudyReporter job
-- cxXml     the xml configuration file
--
-- Return:
-- STB$OERROR$RECORD    
--
-- ***************************************************************************************************

END isr$docfragment;