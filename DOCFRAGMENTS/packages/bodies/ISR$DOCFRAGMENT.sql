CREATE OR REPLACE package body isr$docfragment as

--***********************************************************************************************************************
function createParametersForFragment( cnJobid in     number,
                                     cxXml   in out xmltype)
  return STB$OERROR$RECORD
is
  sCurrentName   constant varchar2(100) := $$PLSQL_UNIT||'.createParametersForFragment (JobID='||cnJobId||')';
  sUserMsg                varchar2(4000);
  oErrorObj               STB$OERROR$RECORD :=STB$OERROR$RECORD();

 cursor cGetFragments is
 /*select df.filename, df.binaryfile, db.bookmarkname, db.protected
  from stb$reportparameter rp,
       isr$docfragment$type dt,
       isr$docfragments df,
       isr$docfragment$type$bookmark db
 where  rp.repid = STB$JOB.getJobParameter ('REPID', cnJobid)
       and rp.parametername = dt.docfragmenttypeid
       and rp.parametervalue = df.docfragmentid
       and dt.docfragmenttypeid = db.docfragmenttypeid;*/
  select df.filename, df.binaryfile, rp.parametername bookmarkname, 'T' protected
  from stb$reportparameter rp,
       isr$docfragment$type dt,
       isr$docfragments df
 where  rp.repid = STB$JOB.getJobParameter ('REPID', cnJobid)
       and rp.parametername = dt.docfragmenttypeid
       and rp.parametervalue = df.docfragmentid;


begin
  isr$trace.stat('begin', 'begin', sCurrentName);

  for rGetFragments in cGetFragments loop
    --isr$XML.CreateNode(cxXML, '/config/apps/app[@name="wordmodul"]/blocks', 'block', rGetFragments.filename);
    isr$XML.CreateNodeAttr(cxXml, '/config/apps/app[@name="wordmodul"]/blocks', 'block', rGetFragments.filename, 'name', rGetFragments.bookmarkname);
    isr$XML.SetAttribute(cxXml, '/config/apps/app[@name="wordmodul"]/blocks/block[@name="'||rGetFragments.bookmarkname||'"]', 'protected', rGetFragments.protected);

    isr$XML.CreateNode(cxXML, '/config/apps/app[@name="wordmodul"]/isr-bookmarks', 'bookmark', rGetFragments.bookmarkname);

    insert into STB$JOBTRAIL (JOBID
                            , DOCUMENTTYPE
                            , MIMETYPE
                            , DOCSIZE
                            , TIMESTAMP
                            , BINARYFILE
                            , FILENAME
                            , BLOBFLAG
                            , ENTRYID
                            , REFERENCE
                            , LEADINGDOC
                            , DOWNLOAD)
    values (
              cnJobId
            , 'FRAGMENT'
            , null
            , DBMS_LOB.getlength (rGetFragments.binaryfile)
            ,  SYSDATE
            , rGetFragments.binaryfile
            , rGetFragments.filename
            , 'B'
            , STB$JOBTRAIL$SEQ.NEXTVAL
            , STB$JOB.getJobParameter ('REPID', cnJobid)
            , 'F'
            , 'T'
           );
  end loop;
  isr$trace.stat('end', 'end', sCurrentName);
  return oErrorObj;
exception
  when others then
    sUserMsg := utd$msglib.getmsg ('exUnexpectedError', stb$security.getcurrentlanguage,  SQLERRM(SQLCODE));
    oErrorObj.handleError ( Stb$typedef.cnSeverityCritical, sUserMsg, sCurrentName, DBMS_UTILITY.format_error_stack ||CHR(10)|| DBMS_UTILITY.format_error_backtrace );
   -- Stb$job.markDBJobAsBroken(cnJobId, sIp, sPort, oErrorObj.sErrorCode, oErrorObj.sErrorMessage, oErrorObj.sSeverityCode);
    return oErrorObj;
end createParametersForFragment;

end isr$docfragment;