CREATE OR REPLACE TRIGGER ISR_isr$docfr$type$bookmark_u
BEFORE UPDATE
ON isr$docfragment$type$bookmark REFERENCING OLD AS old NEW AS new
FOR EACH ROW
BEGIN
  :New.MODIFIEDON := SYSDATE;
  :New.MODIFIEDBY := STB$SECURITY.getOracleUserName(STB$SECURITY.getCurrentUser);
  
  :new.docfragmentbookmarkid := upper(regexp_replace(:new.docfragmentbookmarkid, '[[:space:]]*',''));
end;