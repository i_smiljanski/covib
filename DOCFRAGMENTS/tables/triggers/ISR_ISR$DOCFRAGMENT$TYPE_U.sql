CREATE OR REPLACE TRIGGER ISR_isr$docfragment$type_u
BEFORE UPDATE
ON isr$docfragment$type REFERENCING OLD AS old NEW AS new
FOR EACH ROW
BEGIN
  :New.MODIFIEDON := SYSDATE;
  :New.MODIFIEDBY := STB$SECURITY.getOracleUserName(STB$SECURITY.getCurrentUser);
  :new.docfragmenttypeid := regexp_replace(:new.docfragmenttypeid, '[[:space:]]*','');
end;