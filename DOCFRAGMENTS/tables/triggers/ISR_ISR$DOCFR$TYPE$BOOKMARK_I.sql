CREATE OR REPLACE TRIGGER ISR_isr$docfr$type$bookmark_I
BEFORE INSERT
ON isr$docfragment$type$bookmark REFERENCING OLD AS old NEW AS new
FOR EACH ROW
DECLARE
  nColumnSize   number;
BEGIN
  :New.MODIFIEDON := SYSDATE;
  :New.MODIFIEDBY := STB$SECURITY.getOracleUserName(STB$SECURITY.getCurrentUser);
  
  select  data_length 
  into nColumnSize
  from user_tab_columns 
  where table_name = 'ISR$DOCFRAGMENT$TYPE$BOOKMARK'
  and column_name ='DOCFRAGMENTBOOKMARKID';
  
  if :new.docfragmentbookmarkid is null then
    :new.docfragmentbookmarkid := upper(substr(regexp_replace(:new.bookmarkname, '[[:space:]]*',''), 0, nColumnSize));
  else
    :new.docfragmentbookmarkid := upper(regexp_replace(:new.docfragmentbookmarkid, '[[:space:]]*',''));
  end if;
end;