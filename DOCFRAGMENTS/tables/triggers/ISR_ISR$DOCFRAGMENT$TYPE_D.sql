CREATE OR REPLACE TRIGGER ISR_isr$docfragment$type_d
BEFORE DELETE
ON isr$docfragment$type 
FOR EACH ROW
DECLARE
  exReptypeAssigned exception;
  nCount number;
BEGIN
  select count(*) into nCount from stb$reptypeparameter where parametername = :old.docfragmenttypeid;
  if nCount > 0 then
    raise exReptypeAssigned;
  end if;
exception
  when exReptypeAssigned then
   RAISE_APPLICATION_ERROR (-20001, utd$msglib.getmsg ('exReptypeAssignedText', isr$obj$base().nCurrentLanguage, :old.docfragmenttypeid)); 
end;