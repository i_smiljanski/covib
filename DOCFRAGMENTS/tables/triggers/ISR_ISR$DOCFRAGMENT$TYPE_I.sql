CREATE OR REPLACE TRIGGER ISR_isr$docfragment$type_I
BEFORE INSERT
ON isr$docfragment$type REFERENCING OLD AS old NEW AS new
FOR EACH ROW
DECLARE
  nColumnSize   number;
BEGIN
  :New.MODIFIEDON := SYSDATE;
  :New.MODIFIEDBY := STB$SECURITY.getOracleUserName(STB$SECURITY.getCurrentUser);
  
  select  data_length 
  into nColumnSize
  from user_tab_columns 
  where table_name = 'ISR$DOCFRAGMENT$TYPE'
  and column_name ='DOCFRAGMENTTYPEID';
  
  if :new.docfragmenttypeid is null then
    :new.docfragmenttypeid := substr(regexp_replace(:new.docfragmenttype, '[[:space:]]*',''), 0, nColumnSize);
  else
    :new.docfragmenttypeid := regexp_replace(:new.docfragmenttypeid, '[[:space:]]*','');
  end if;
end;