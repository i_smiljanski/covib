CREATE OR REPLACE TRIGGER isr_isr$docfragments_u
BEFORE UPDATE
ON isr$docfragments REFERENCING OLD AS old NEW AS new
FOR EACH ROW
DECLARE
  nColumnSize   number;
BEGIN
  :New.MODIFIEDON := SYSDATE;
  :New.MODIFIEDBY := STB$SECURITY.getOracleUserName(STB$SECURITY.getCurrentUser);
  :new.docfragmentid := regexp_replace(:new.docfragmentid, '[[:space:]]*','');
  
end;