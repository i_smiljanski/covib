CREATE OR REPLACE TRIGGER isr_isr$docfragments_i
BEFORE INSERT
ON isr$docfragments REFERENCING OLD AS old NEW AS new
FOR EACH ROW
DECLARE
  nColumnSize   number;
BEGIN
  :New.MODIFIEDON := SYSDATE;
  :New.MODIFIEDBY := STB$SECURITY.getOracleUserName(STB$SECURITY.getCurrentUser);
    
  select  data_length 
  into nColumnSize
  from user_tab_columns 
  where table_name = 'ISR$DOCFRAGMENTS'
  and column_name ='DOCFRAGMENTID';
  
  if :new.docfragmentid is null then
    :new.docfragmentid := substr(regexp_replace(:new.name, '[[:space:]]*',''), 0, nColumnSize);
 -- else
  --  :new.docfragmentid := upper(regexp_replace(:new.docfragmentid, '[[:space:]]*',''));
  end if;
end;