CREATE OR REPLACE type body ISR$OBJ$DOCFRAGMENT$REPTYPES as


--**********************************************************************************************************************************
static function putParameters(oParameter in stb$menuentry$record, olParameter in stb$property$list, clParameter in clob default null) 
  return stb$oerror$record is
  sCurrentName        constant varchar2(100) := $$PLSQL_UNIT||'.putParameters('||oParameter.sToken||')';
  oErrorObj           STB$OERROR$RECORD := STB$OERROR$RECORD();
  sMsg              varchar2(4000);
  oParamListRec   ISR$PARAMLIST$REC := ISR$PARAMLIST$REC();
  xXml      xmltype;
  sAction           varchar2(500);
  sParametername    stb$reptypeparameter.parametername%type;
  sReporttypeid     stb$reptypeparameter.reporttypeid%type;
 begin 
  isr$trace.stat('begin','begin',sCurrentName);
  oParamListRec.AddParam('TABLE', 'STB$REPTYPEPARAMETER');
  oParamListRec.AddParam('AUDITTYPE', 'SYSTEMAUDIT');
  oParamListRec.AddParam('AUDITREFERENCE', TO_CHAR (-1));
  oErrorObj := isr$obj$grid().modifyGridTable(oParameter.sToken, clParameter, oParamListRec);
  
  xXml := ISR$XML.clobToXML(clParameter);
  for i in 1..ISR$XML.valueOf(xXml, 'count(//ROW)') loop
    sAction := ISR$XML.valueOf(xXml, '(//ROW)['||i||']/../name()');
    sParametername := ISR$XML.valueOf(xXml, '(//ROW)['||i||']/PARAMETERNAME/text()');
    sReporttypeid := ISR$XML.valueOf(xXml, '(//ROW)['||i||']/REPORTTYPEID/text()');
    isr$trace.debug('sAction', sAction || '; sParametername= ' || sParametername || '; sReporttypeid= ' || sReporttypeid,sCurrentName);
    if sAction <> 'DELETE' then
    
      update stb$reptypeparameter 
      set handledby = nvl(handledby, 'ISR$OBJ$DOCFRAGMENT$TYPES'), 
          userparameter = 'F',
          repparameter = 'T',
          description = sParametername||'$DESC',
          parametertype = 'STRING', 
          haslov = 'T'
      where parametername = sParametername 
      and reporttypeid = sReporttypeid;
      isr$trace.debug(sReporttypeid, sql%rowcount || ' rows udated', sCurrentName);
      
      merge into stb$group$reptype$rights r
        using (select sReporttypeid reporttypeid, 1 usergroupno, sParametername parametername, 'T' parametervalue from dual) d
      on (r.reporttypeid = d.reporttypeid and r.parametername = d.parametername and r.usergroupno = d.usergroupno)
      when not matched then insert(REPORTTYPEID, USERGROUPNO, PARAMETERNAME, PARAMETERVALUE)
      values (d.reporttypeid, d.usergroupno, d.parametername, d.parametervalue);
      isr$trace.debug(sReporttypeid, sql%rowcount || ' rows inserted for group 1', sCurrentName);
      
      merge into stb$group$reptype$rights r
        using (select sReporttypeid reporttypeid, 2 usergroupno,  sParametername parametername, 'T' parametervalue from dual) d
      on (r.reporttypeid = d.reporttypeid and r.parametername = d.parametername and r.usergroupno = d.usergroupno)
      when not matched then insert(REPORTTYPEID, USERGROUPNO, PARAMETERNAME, PARAMETERVALUE)
      values (d.reporttypeid, d.usergroupno, d.parametername, d.parametervalue);
      isr$trace.debug(sReporttypeid, sql%rowcount || ' rows inserted for group 2', sCurrentName);
      
      merge into stb$group$reptype$rights r
        using (select sReporttypeid reporttypeid, 3 usergroupno, sParametername parametername, 'T' parametervalue from dual) d
      on (r.reporttypeid = d.reporttypeid and r.parametername = d.parametername and r.usergroupno = d.usergroupno)
      when not matched then insert(REPORTTYPEID, USERGROUPNO, PARAMETERNAME, PARAMETERVALUE)
      values (d.reporttypeid, d.usergroupno, d.parametername, d.parametervalue);
      isr$trace.debug(sReporttypeid, sql%rowcount || ' rows inserted for group 3', sCurrentName);
      
      merge into stb$group$reptype$rights r
        using (select sReporttypeid reporttypeid, 7 usergroupno, sParametername parametername, 'T' parametervalue from dual) d
      on (r.reporttypeid = d.reporttypeid and r.parametername = d.parametername and r.usergroupno = d.usergroupno)
      when not matched then insert(REPORTTYPEID, USERGROUPNO, PARAMETERNAME, PARAMETERVALUE)
      values (d.reporttypeid, d.usergroupno, d.parametername, d.parametervalue);
      isr$trace.debug(sReporttypeid, sql%rowcount || ' rows inserted for group 7', sCurrentName);
    else
      delete from stb$group$reptype$rights 
      where parametername =  sParametername 
      and reporttypeid = sReporttypeid;
      isr$trace.debug(sReporttypeid, sql%rowcount || ' rows deleted', sCurrentName);
    end if; 
  end loop;
  commit;
  isr$trace.stat('end','end',sCurrentName);
  return oErrorObj;
exception
  when others then
    sMsg :=  utd$msglib.getmsg ('exUnexpectedError', isr$obj$base().nCurrentLanguage, csP1 => SQLERRM(SQLCODE));
    oErrorObj.handleError ( Stb$typedef.cnSeverityCritical, sMsg, sCurrentName,
                            DBMS_UTILITY.format_error_stack || CHR(10) || DBMS_UTILITY.format_error_backtrace );
    return oErrorObj; 
 end putParameters;
 
static function callFunction (oParameter in STB$MENUENTRY$RECORD, olNodeList out STB$TREENODELIST)
  return STB$OERROR$RECORD is
  sCurrentName         constant varchar2(100) := $$PLSQL_UNIT||'.callFunction('||oParameter.sToken||')';
  oErrorObj                     STB$OERROR$RECORD := STB$OERROR$RECORD();
  sMsg              varchar2(4000);
  oParamListRec   ISR$PARAMLIST$REC := ISR$PARAMLIST$REC();
begin
  isr$trace.stat('begin', 'oParameter.sToken: '||oParameter.sToken ,sCurrentName);
  isr$trace.debug('parameter', 'oParameter', sCurrentName, oParameter);
    
  STB$OBJECT.setCurrentToken(oparameter.stoken);
  
  oParamListRec.AddParam('TABLE', 'ISR$DOCFRAGMENT$REPTYPE$V');
  oErrorObj := isr$obj$grid().buildGridMask(oParameter.sToken, oParamListRec, olNodeList);
  isr$trace.stat('end', 'end', sCurrentName);
  return oErrorObj;
exception
  when others then
    sMsg :=  utd$msglib.getmsg ('exUnexpectedError', isr$obj$base().nCurrentLanguage, csP1 => SQLERRM(SQLCODE));
    oErrorObj.handleError ( Stb$typedef.cnSeverityCritical, sMsg, sCurrentName,
                            DBMS_UTILITY.format_error_stack || CHR(10) || DBMS_UTILITY.format_error_backtrace );
    return oErrorObj; 
end callFunction;


--***********************************************************************************************************************
static function getLov (sEntity  in varchar2, ssuchwert  in varchar2, oSelectionList out isr$tlrselection$list)
   return stb$oerror$record
is
  sCurrentName                  constant varchar2(100) := $$PLSQL_UNIT||'.getLov('||sEntity||')'; 
  oErrorObj                     stb$oerror$record := STB$OERROR$RECORD();
  
  cursor cGetDocFragmentType is
  select docfragmenttypeid
       , docfragmenttype
       , description
  from isr$docfragment$type
  order by 1;
  
  cursor cGetReporttypes is
      select reporttypeid
           , UTD$MSGLIB.getMsg (reporttypename, stb$security.GetCurrentLanguage)
                reporttypename
           , UTD$MSGLIB.getMsg (description, stb$security.GetCurrentLanguage)
                description
        from stb$reporttype
       where activetype = 1
         and (reporttypeid <> 999
           or (reporttypeid = 999 and STB$UTIL.getSystemParameter ('SHOW_UNIT_TEST') = 'T'))
     
      order by 1;
begin
  isr$trace.stat('begin','sEntity = ' || sEntity || '; ssuchwert =' || ssuchwert, sCurrentName); 
  oSelectionList := isr$tlrselection$list (); 
  case
    when UPPER (sEntity) = 'PARAMETERNAME' then
      for rGetDocFragmentType in cGetDocFragmentType loop
        isr$trace.debug('docfragmenttypeid', rGetDocFragmentType.docfragmenttypeid, sCurrentName);
        oSelectionList.EXTEND;
        oSelectionList (oSelectionList.COUNT()) := ISR$OSELECTION$RECORD(rGetDocFragmentType.docfragmenttypeid, 
            rGetDocFragmentType.docfragmenttypeid, rGetDocFragmentType.description, null, oSelectionList.COUNT());
      end loop; 
    when UPPER (sEntity) = 'REPORTTYPEID' then
      for rGetReporttypes in cGetReporttypes loop
          isr$trace.debug('reporttypeid', rGetReporttypes.reporttypeid, sCurrentName);
          oSelectionList.EXTEND;
          oSelectionList (oSelectionList.COUNT()) := ISR$OSELECTION$RECORD(rGetReporttypes.reporttypeid, rGetReporttypes.reporttypeid, rGetReporttypes.description, null, oSelectionList.COUNT());
        end loop;
  end case;  
  
  isr$obj$base.setLovListSelection(ssuchwert, oSelectionList);
  
  isr$trace.stat('end','end', sCurrentName);
  return oErrorObj;
exception
    when others then
    oErrorObj.handleError ( Stb$typedef.cnSeverityCritical, SQLERRM, sCurrentName,
                           DBMS_UTILITY.format_error_stack || CHR(10) || DBMS_UTILITY.format_error_backtrace);
    return oErrorObj;
end   getLov;

end;