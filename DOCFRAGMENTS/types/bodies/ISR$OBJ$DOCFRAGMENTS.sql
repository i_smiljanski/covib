CREATE OR REPLACE type body ISR$OBJ$DOCFRAGMENTS as


--**********************************************************************************************************************************
static function putParameters(oParameter in stb$menuentry$record, olParameter in stb$property$list, clParameter in clob default null) 
  return stb$oerror$record is
  sCurrentName        constant varchar2(100) := $$PLSQL_UNIT||'.putParameters('||oParameter.sToken||')';
  oErrorObj           STB$OERROR$RECORD := STB$OERROR$RECORD();
  sMsg              varchar2(4000);
  oParamListRec   ISR$PARAMLIST$REC := ISR$PARAMLIST$REC();
  errCheckConstraint  exception;
 begin 
  isr$trace.stat('begin','begin',sCurrentName);
  oParamListRec.AddParam('TABLE', 'ISR$DOCFRAGMENTS');
  oParamListRec.AddParam('AUDITTYPE', 'SYSTEMAUDIT');
  oParamListRec.AddParam('AUDITREFERENCE', TO_CHAR (-1));
  oErrorObj := isr$obj$grid().modifyGridTable(oParameter.sToken, clParameter, oParamListRec);
    
  isr$trace.stat('end','end',sCurrentName);
  return oErrorObj;
  exception
  
  when others then
    sMsg :=  utd$msglib.getmsg ('exUnexpectedError', isr$obj$base().nCurrentLanguage, csP1 => SQLERRM(SQLCODE));
    oErrorObj.handleError ( Stb$typedef.cnSeverityCritical, sMsg, sCurrentName,
                            DBMS_UTILITY.format_error_stack || CHR(10) || DBMS_UTILITY.format_error_backtrace, SQLCODE );
    return oErrorObj; 
 end putParameters;
 
static function callFunction (oParameter in STB$MENUENTRY$RECORD, olNodeList out STB$TREENODELIST)
  return STB$OERROR$RECORD is
  sCurrentName         constant varchar2(100) := $$PLSQL_UNIT||'.callFunction('||oParameter.sToken||')';
  oErrorObj                     STB$OERROR$RECORD := STB$OERROR$RECORD();
  sMsg              varchar2(4000);
  oParamListRec   ISR$PARAMLIST$REC := ISR$PARAMLIST$REC();
begin
  isr$trace.stat('begin', 'oParameter.sToken: '||oParameter.sToken ,sCurrentName);
  isr$trace.debug('parameter', 'oParameter', sCurrentName, oParameter);
    
  STB$OBJECT.setCurrentToken(oparameter.stoken);
  
  oParamListRec.AddParam('TABLE', 'ISR$DOCFRAGMENTS');
  oErrorObj := isr$obj$grid().buildGridMask(oParameter.sToken, oParamListRec, olNodeList);
  isr$trace.stat('end', 'end', sCurrentName);
  return oErrorObj;
exception
  when others then
    sMsg :=  utd$msglib.getmsg ('exUnexpectedError', isr$obj$base().nCurrentLanguage, csP1 => SQLERRM(SQLCODE));
    oErrorObj.handleError ( Stb$typedef.cnSeverityCritical, sMsg, sCurrentName,
                            DBMS_UTILITY.format_error_stack || CHR(10) || DBMS_UTILITY.format_error_backtrace );
    return oErrorObj; 
end callFunction;


--***********************************************************************************************************************
static function getLov (sEntity  in varchar2, ssuchwert  in varchar2, oSelectionList out isr$tlrselection$list)
   return stb$oerror$record
is
  sCurrentName                  constant varchar2(100) := $$PLSQL_UNIT||'.getLov('||sEntity||')'; 
  oErrorObj                     stb$oerror$record := STB$OERROR$RECORD();
  
  cursor cGetDocFragmentType is
  select docfragmenttypeid
       , docfragmenttype
       , description
  from isr$docfragment$type
  order by 1;
  
  cursor cGetFragments(sFragmentType in varchar2) is
  select docfragmentid, name, description
  from isr$docfragments
  where docfragmenttypeid = sFragmentType;     
begin
  isr$trace.stat('begin','sEntity = ' || sEntity || '; ssuchwert =' || ssuchwert, sCurrentName); 
  oSelectionList := isr$tlrselection$list (); 
  case
    when UPPER (sEntity) = 'DOCFRAGMENTTYPEID' then
      for rGetDocFragmentType in cGetDocFragmentType loop
        isr$trace.debug('docfragmenttypeid', rGetDocFragmentType.docfragmenttypeid, sCurrentName);
        oSelectionList.EXTEND;
        oSelectionList (oSelectionList.COUNT()) := ISR$OSELECTION$RECORD(rGetDocFragmentType.docfragmenttype, 
            rGetDocFragmentType.docfragmenttypeid, rGetDocFragmentType.description, null, oSelectionList.COUNT());
      end loop; 
    else     
      for rGetFragments in cGetFragments(sEntity) loop
        oSelectionList.EXTEND;
        oSelectionList (oSelectionList.COUNT()) := ISR$OSELECTION$RECORD(rGetFragments.name, rGetFragments.docfragmentid, rGetFragments.description, null, oSelectionList.COUNT());
      end loop;
  end case;  
  
  isr$obj$base.setLovListSelection(ssuchwert, oSelectionList);
  
  isr$trace.stat('end','end', sCurrentName);
  return oErrorObj;
exception
    when others then
    oErrorObj.handleError ( Stb$typedef.cnSeverityCritical, SQLERRM, sCurrentName,
                           DBMS_UTILITY.format_error_stack || CHR(10) || DBMS_UTILITY.format_error_backtrace);
    return oErrorObj;
end   getLov;

end;