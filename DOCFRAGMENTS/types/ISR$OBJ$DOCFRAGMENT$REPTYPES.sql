CREATE OR REPLACE type ISR$OBJ$DOCFRAGMENT$REPTYPES under isr$obj$grid(
static function putParameters(oParameter in stb$menuentry$record, olParameter in stb$property$list, clParameter in clob default null)
  return stb$oerror$record,
  static function callFunction (oParameter in STB$MENUENTRY$RECORD, olNodeList out STB$TREENODELIST)
  return STB$OERROR$RECORD,
  static function getLov (sEntity  in varchar2, ssuchwert  in varchar2, oSelectionList out isr$tlrselection$list)
   return stb$oerror$record
)
  not final