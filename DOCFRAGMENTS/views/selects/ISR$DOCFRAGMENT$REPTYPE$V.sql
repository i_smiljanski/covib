CREATE OR REPLACE VIEW ISR$DOCFRAGMENT$REPTYPE$V (VROWID, REPORTTYPEID, HANDLEDBY, PARAMETERNAME, USERPARAMETER, REPPARAMETER) AS select rt.rowid vrowid,
         reporttypeid,
         rt.handledby,
         rt.parametername,
         rt.userparameter,
         rt.repparameter
    from isr$docfragment$type fr, stb$reptypeparameter rt
   where upper (fr.docfragmenttypeid) = upper (rt.parametername)