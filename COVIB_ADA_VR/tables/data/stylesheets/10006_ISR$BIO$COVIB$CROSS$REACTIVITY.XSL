<?xml version="1.0" encoding="UTF-8"?>
<!--
ISR$BIO$COVIB$CROSS$REACTIVITY.XSL
==========================================================================

-->
<xsl:stylesheet version="3.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform" xmlns:xs="http://www.w3.org/2001/XMLSchema" xmlns:fn="http://www.w3.org/2005/xpath-functions" xmlns:utd="http://www.uptodata.com">
	<xsl:strip-space elements="*"/>
	<xsl:output method="html" html-version="5.0" encoding="UTF-8" indent="yes" media-type="text/html"/>
	<!--
==========================================================================
Parameters from ISR$OUTPUT$TRANSFORM. (Defaults for running it standalone).
==========================================================================-->
	<xsl:param name="experiment">S_CROSS_REACTIVITY</xsl:param>
	<xsl:param name="hash">XREA</xsl:param>
	<!--
==========================================================================
Includes
==========================================================================-->
	<xsl:include href="ISR$CSS.XSL"/>
	<xsl:include href="ISR$UTIL.XSL"/>
	<xsl:include href="ISR$CAPTION.XSL"/>
	<xsl:include href="ISR$WT$TABLE.XSL"/>
	<xsl:include href="ISR$WT$PAGE.XSL"/>
	<!--
==========================================================================
Variables
==========================================================================-->
	<!-- shortcuts -->
	<xsl:variable name="project" select="/study/bioanalytic/project"/>
	<xsl:variable name="study" select="/study/bioanalytic/studies/study"/>
	<xsl:variable name="runs" select="/study/bioanalytic/runs"/>
	<xsl:variable name="criteria" select="/study/protocol/criteria"/>
	<!-- attributes -->
	<xsl:variable name="attributes" select="/study/attributes/*"/>
	<xsl:variable name="not_determined" select="$attributes/not_determined_identifier"/>
	<xsl:variable name="not_performed" select="$attributes/not_performed_identifier"/>
	<xsl:variable name="not_detected" select="$attributes/not_detected_identifier"/>
	<xsl:variable name="min_values" select="xs:integer($attributes/min_conc_values)"/>
	<xsl:variable name="cap" select="$attributes/*[name() = concat( 'max_cols_', lower-case($experiment) ) ]" as="xs:integer"/>
	<xsl:variable name="runtypes" select="$attributes/default_run_type,$attributes/additional_run_type"/>
	<xsl:variable name="runstates" select="$attributes/runstates/state"/>
	<xsl:variable name="maxlines" select="$attributes/*[name() = concat( 'max_lines_', lower-case($experiment) ) ]"/>
	<xsl:variable name="response_unit" select="$attributes/ada_response_unit"/>
	<xsl:variable name="not_calculated" select="$not_detected"/>
	<xsl:variable name="mean-or-median-nc" select="$attributes/mean_or_median_nc"/>
	<xsl:variable name="show-cv" select="$attributes/show_cv"/>
	<xsl:variable name="fixed_screen_cutpoint" select="$attributes/fixed_screen_cutpoint"/>
	<xsl:variable name="fixed_titer_cutpoint" select="$attributes/fixed_titer_cutpoint"/>
	<xsl:variable name="screen_cp_nfactor" select="$attributes/screen_cp_nfactor"/>
	<xsl:variable name="titer_cp_nfactor" select="$attributes/titer_cp_nfactor"/>
	<!-- Formats -->
	<xsl:variable name="table-caption" select="utd:formats($experiment,'caption')"/>
	<xsl:variable name="table-type" select="utd:formats($experiment,('var'))[upper-case(@name)='TABLE_TYPE']"/>
	<xsl:variable name="cycle-caption" select="utd:formats($experiment,('header','legend'))"/>
	<xsl:variable name="deactivated" select="utd:formats($experiment,'deactivated')"/>
	<xsl:variable name="hide_cols" select="for $s in utd:formats($experiment,'var')[@type='OPTCOL'] return if ($attributes/*[name() = $s] != 'T') then $s/@key else ()"/>
	<xsl:variable name="columns" select="utd:formats($experiment,'cols')[not(@name=$hide_cols)]"/>
	<xsl:variable name="column_behind" select="utd:formats($experiment,('cols','behind'))[not(@name=$hide_cols)]"/>
	<xsl:variable name="column_width" select="utd:formats($experiment,('cols','width'))"/>
	<xsl:variable name="column_group" select="utd:formats($experiment,('cols','group'))"/>
	<xsl:variable name="legend" select="utd:formats($experiment,('header','legend'))"/>
	<xsl:variable name="hide_stats" select="for $s in utd:formats($experiment,'var')[@type='OPTSTAT'] return if ($attributes/*[name() = $s] != 'T') then $s/@key else ()"/>
	<xsl:variable name="stats_individual" select="utd:formats($experiment,'calc')[@type='INDIVIDUAL'][not(@name=$hide_stats)]"/>
	<xsl:variable name="stats_intra" select="utd:formats($experiment,'calc')[@type='INTRA'][not(@name=$hide_stats)]"/>
	<xsl:variable name="stats_overall" select="utd:formats($experiment,'calc')[@type='OVERALL'][not(@name=$hide_stats)]"/>
	<xsl:variable name="crit_entity" select="utd:formats($experiment,'crit')"/>
	<xsl:variable name="main-ref" select="utd:formats($experiment,('var'))[upper-case(@name)='MAINREF']"/>
	<xsl:variable name="caption-type" select="utd:formats($experiment,('var'))[upper-case(@name)='CAPTION_TYPE']"/>
	<xsl:variable name="sampletypes" select="utd:formats($experiment,'var')[@type='SAMPLETYPE']"/>
	<xsl:variable name="excel-filename" select="utd:formats($experiment,'var')[@name='EXCEL_FILENAME']"/>
	<xsl:variable name="section-title" select="utd:formats($experiment,'var')[@name='SECTION_TITLE']"/>
	<xsl:variable name="note-fixed_screen_cp" select="utd:formats($experiment,'footer')[@name='FIXED_SCREEN_CP']"/>
	<xsl:variable name="note-fixed_titer_cp" select="utd:formats($experiment,'footer')[@name='FIXED_TITER_CP']"/>
	<xsl:variable name="note-screen_cp_nfactor" select="utd:formats($experiment,'footer')[@name='SCREEN_CP_NFACTOR']"/>
	<xsl:variable name="note-titer_cp_nfactor" select="utd:formats($experiment,'footer')[@name='TITER_CP_NFACTOR']"/>
	<!-- other -->
	<xsl:variable name="calculations" select="$study/calculations/experiment[@name=$experiment][@type='xreactivity_response']"/>
	<xsl:variable name="selected" select="$criteria[@entity=$crit_entity]/value[masterkey=$experiment]"/>
	<xsl:variable name="sig" as="node()*">
		<tag>temperature</tag>
		<tag>hours</tag>
		<tag>longtermtime</tag>
		<tag>longtermunits</tag>
	</xsl:variable>
	<!--
==========================================================================
Functions
========================================================================== -->
	<xsl:function name="utd:signature" as="xs:string">
		<xsl:param name="target"/>
		<xsl:value-of select="string-join(for $i in $sig return string($target/(*,@*)[name()=$i]),'_')"/>
	</xsl:function>
	<!--
==========================================================================
Main template
========================================================================== -->
	<xsl:template match="/">
		<xsl:variable name="isr-sections">
			<isr-sections emptytable="{true()}" experiment="{$experiment}">
				<xsl:attribute name="title"><xsl:text>ISR$BIO$COVIB$CROSS$REACTIVITY.XSL - </xsl:text><xsl:value-of select="$experiment"/></xsl:attribute>
				<xsl:if test="$table-type">
					<xsl:attribute name="type" select="$table-type"/>
				</xsl:if>
				<xsl:if test="exists($calculations/calculation/values/sample)">
					<xsl:for-each select="distinct-values($calculations/calculation/target/acceptedrun)">
						<xsl:variable name="acceptedrun" select="."/>
						<xsl:variable name="section-params" as="node()*">
							<study>
								<xsl:value-of select="$study/name"/>
							</study>
							<acceptedrun>
								<xsl:value-of select="$acceptedrun"/>
							</acceptedrun>
						</xsl:variable>
						<isr-section>
							<xsl:attribute name="experiment"><xsl:value-of select="$experiment"/></xsl:attribute>
							<xsl:if test="$section-title">
								<xsl:attribute name="title" select="utd:params($section-title,$section-params)"/>
							</xsl:if>
							<xsl:if test="$excel-filename">
								<xsl:attribute name="filename" select="utd:params($excel-filename,$section-params)"/>
							</xsl:if>
							<xsl:if test="$table-type">
								<xsl:attribute name="type" select="$table-type"/>
							</xsl:if>
							<xsl:for-each select="distinct-values($calculations/calculation/target[acceptedrun = $acceptedrun]/analyteid)">
								<xsl:variable name="analyteid" select="."/>
								<xsl:for-each select="distinct-values($calculations/calculation/target[acceptedrun = $acceptedrun][analyteid = $analyteid]/species)">
									<xsl:variable name="species" select="current()"/>
									<xsl:for-each select="distinct-values($calculations/calculation/target[acceptedrun = $acceptedrun][analyteid = $analyteid][species = $species]/matrix)">
										<xsl:variable name="matrix" select="current()"/>
										<xsl:for-each select="distinct-values($calculations/calculation/target[acceptedrun = $acceptedrun][analyteid = $analyteid][species = $species][matrix = $matrix]/conc-unit)">
											<xsl:variable name="conc-unit" select="current()"/>
											<xsl:variable name="analyte" select="$study/analytes/analyte[@id=$analyteid]"/>
											<xsl:variable name="calcs" select="$calculations/calculation[target/acceptedrun = $acceptedrun][target/analyteid = $analyteid][target/species = $species][target/matrix = $matrix][target/conc-unit = $conc-unit]"/>
											<xsl:variable name="hashstring" select="concat($acceptedrun, '_', $analyte/@code, '_', $species, '_', $matrix, '_', $conc-unit)"/>
											<xsl:variable name="tables" select="ceiling(count($calcs) div $cap)"/>
											<xsl:variable name="colcnt" select="count($calcs)"/>
											<xsl:variable name="cols" select="if ($cap gt $colcnt) then $colcnt else $cap"/>
											<xsl:for-each select="1 to xs:integer($tables)">
												<xsl:variable name="number" select="."/>
												<xsl:call-template name="createTable">
													<xsl:with-param name="hash" select="concat( $hash, '_', utd:hashcalc( $hashstring, 18 ) )"/>
													<xsl:with-param name="number" select="$number"/>
													<xsl:with-param name="acceptedrun" select="$acceptedrun"/>
													<xsl:with-param name="analyte" select="$analyte"/>
													<xsl:with-param name="species" select="$species"/>
													<xsl:with-param name="matrix" select="$matrix"/>
													<xsl:with-param name="conc-unit" select="$conc-unit"/>
													<xsl:with-param name="calcs" select="$calcs[position() gt (($number*$cap)-$cap) and position() le ($number*$cap)]"/>
												</xsl:call-template>
											</xsl:for-each>
										</xsl:for-each>
									</xsl:for-each>
								</xsl:for-each>
							</xsl:for-each>
						</isr-section>
					</xsl:for-each>
				</xsl:if>
			</isr-sections>
		</xsl:variable>
		<!--xsl:copy-of select="$isr-section"/-->
		<xsl:apply-templates select="$isr-sections/isr-sections"/>
	</xsl:template>
	<!-- 
==========================================================================
createTable
========================================================================== -->
	<xsl:template name="createTable">
		<xsl:param name="hash"/>
		<xsl:param name="number"/>
		<xsl:param name="acceptedrun"/>
		<xsl:param name="analyte"/>
		<xsl:param name="species"/>
		<xsl:param name="matrix"/>
		<xsl:param name="conc-unit"/>
		<xsl:param name="calcs"/>
		<xsl:variable name="concentrationunit">
			<xsl:choose>
				<xsl:when test="$attributes/concentration_unit = 'UNIT_NO_CHANGE' ">
					<xsl:value-of select="$analyte/concentrationunits"/>
				</xsl:when>
				<xsl:when test="$attributes/concentration_unit">
					<xsl:value-of select="$attributes/concentration_unit"/>
				</xsl:when>
				<xsl:otherwise>
					<xsl:value-of select="$analyte/concentrationunits"/>
				</xsl:otherwise>
			</xsl:choose>
		</xsl:variable>
		<!-- PARAMETERS -->
		<xsl:variable name="isr-parameters">
			<isr-parameters>
				<study>
					<xsl:value-of select="$study/name"/>
				</study>
				<acceptedrun>
					<xsl:value-of select="$acceptedrun"/>
				</acceptedrun>
				<analyte-name>
					<xsl:value-of select="$analyte/name"/>
				</analyte-name>
				<analyte>
					<xsl:value-of select="$analyte/name"/>
				</analyte>
				<unit>
					<xsl:value-of select="$conc-unit"/>
				</unit>
				<species>
					<xsl:value-of select="$species"/>
				</species>
				<matrix>
					<xsl:value-of select="$matrix"/>
				</matrix>
				<response-unit>
					<xsl:value-of select="$response_unit"/>
				</response-unit>
				<conc-unit>
					<xsl:value-of select="$conc-unit"/>
				</conc-unit>
				<mean-or-median-nc>
					<xsl:choose>
						<xsl:when test="$mean-or-median-nc = 'mean'">Mean</xsl:when>
						<xsl:when test="$mean-or-median-nc = 'meanofmeans'">Mean of Means</xsl:when>
						<xsl:otherwise>Median</xsl:otherwise>
					</xsl:choose>
				</mean-or-median-nc>
				<s1-source>
					<xsl:value-of select="$calcs[1]/values/sample[1]/s1-source"/>
				</s1-source>
				<s2-source>
					<xsl:value-of select="$calcs[1]/values/sample[1]/s2-source"/>
				</s2-source>
				<fixed_screen_cp>
					<xsl:value-of select="$fixed_screen_cutpoint"/>
				</fixed_screen_cp>
				<fixed_titer_cp>
					<xsl:value-of select="$fixed_titer_cutpoint"/>
				</fixed_titer_cp>
				<screen_cp_nfactor>
					<xsl:value-of select="$screen_cp_nfactor"/>
				</screen_cp_nfactor>
				<titer_cp_nfactor>
					<xsl:value-of select="$titer_cp_nfactor"/>
				</titer_cp_nfactor>
			</isr-parameters>
		</xsl:variable>
		<!-- CAPTION -->
		<xsl:variable name="isr-caption">
			<isr-caption>
				<xsl:attribute name="type">listed</xsl:attribute>
				<xsl:if test="$caption-type">
					<xsl:attribute name="type" select="$caption-type"/>
				</xsl:if>
				<xsl:attribute name="hash" select="$hash"/>
				<xsl:attribute name="ref" select="$hash"/>
				<xsl:if test="$main-ref">
					<xsl:attribute name="mainref" select="$main-ref"/>
				</xsl:if>
				<xsl:attribute name="number" select="$number"/>
				<title>
					<xsl:value-of select="$table-caption"/>
				</title>
			</isr-caption>
		</xsl:variable>
		<!-- TABLE -->
		<xsl:variable name="isr-table">
			<isr-table type="standard">
				<xsl:if test="not(empty($maxlines))">
					<xsl:attribute name="maxlines" select="$maxlines"/>
				</xsl:if>
				<headers>
					<xsl:variable name="legend1" select="$legend[@name='LEGEND1']"/>
					<xsl:variable name="rowspan" select="1"/>
					<xsl:if test="$legend1">
						<header>
							<head class="borderAll" colspan="{count($columns)}"/>
							<head class="borderAll" colspan="{(count($calcs)*count($stats_individual))}">
								<xsl:call-template name="utd:excelAttributes">
									<xsl:with-param name="input" select="$legend1"/>
								</xsl:call-template>
								<xsl:value-of select="$legend1"/>
							</head>
							<head class="borderAll" colspan="{count($column_behind)}"/>
						</header>
					</xsl:if>
					<header calcwidth="true">
						<xsl:for-each select="$columns">
							<xsl:variable name="col" select="."/>
							<head class="borderTopBottom" rowspan="{$rowspan}">
								<xsl:attribute name="class" select="'border'||(if (last()=1) then 'All' else ('TopBottom'||(if (position()=1) then 'Left' else if (position()=last()) then 'Right' else '')))"/>
								<xsl:if test="current()/@name=('NOMINALCONC', 'S1-MEAN')">
									<xsl:attribute name="class">borderTopBottomLeft</xsl:attribute>
								</xsl:if>
								<xsl:if test="@name=('NOMINALCONC', 'S1-MEAN') or not(starts-with(@name, 'S1') or @name=('NOMINALCONC', 'SUBJECT'))">
									<xsl:attribute name="class">borderTopBottomLeft</xsl:attribute>
								</xsl:if>
								<xsl:if test="$column_width[@name=$col/@name]">
									<xsl:attribute name="width" select="$column_width[@name=$col/@name]"/>
								</xsl:if>
								<xsl:call-template name="utd:excelAttributes">
									<xsl:with-param name="input" select="current()"/>
									<xsl:with-param name="header" select="true()"/>
								</xsl:call-template>
								<xsl:if test="@name='SCREENCP'">
									<xsl:choose>
										<xsl:when test="$fixed_screen_cutpoint!='' and $note-fixed_screen_cp!=''">
											<xsl:attribute name="footnoteun" select="$note-fixed_screen_cp"/>
										</xsl:when>
										<xsl:when test="$screen_cp_nfactor!='' and $note-screen_cp_nfactor!=''">
											<xsl:attribute name="footnoteun" select="$note-screen_cp_nfactor"/>
										</xsl:when>
									</xsl:choose>
								</xsl:if>
								<xsl:if test="@name='TITERCP'">
									<xsl:choose>
										<xsl:when test="$fixed_titer_cutpoint!='' and $note-fixed_titer_cp!=''">
											<xsl:attribute name="footnoteun" select="$note-fixed_titer_cp"/>
										</xsl:when>
										<xsl:when test="$titer_cp_nfactor!='' and $note-titer_cp_nfactor!=''">
											<xsl:attribute name="footnoteun" select="$note-titer_cp_nfactor"/>
										</xsl:when>
									</xsl:choose>
								</xsl:if>
								<xsl:value-of select="$col"/>
							</head>
						</xsl:for-each>
						<xsl:for-each select="$calcs">
							<xsl:variable name="target" select="target"/>
							<xsl:variable name="params" as="node()*">
								<xsl:for-each select="$target/*">
									<xsl:element name="{name()}">
										<xsl:value-of select="."/>
									</xsl:element>
								</xsl:for-each>
								<xsl:copy-of select="$isr-parameters/isr-parameters/*"/>
							</xsl:variable>
							<xsl:for-each select="$stats_individual">
								<head class="borderTopBottom">
									<xsl:attribute name="class" select="'border'||(if (last()=1) then 'All' else ('TopBottom'||(if (position()=1) then 'Left' else if (position()=last()) then 'Right' else '')))"/>
									<xsl:call-template name="utd:excelAttributes">
										<xsl:with-param name="input" select="current()"/>
										<xsl:with-param name="header" select="true()"/>
									</xsl:call-template>
									<xsl:value-of select="utd:params(.,$params)"/>
								</head>
							</xsl:for-each>
						</xsl:for-each>
						<xsl:for-each select="$column_behind">
							<xsl:variable name="col" select="."/>
							<head class="borderTopBottom" rowspan="{$rowspan}">
								<xsl:if test="$column_width[@name=$col/@name]">
									<xsl:attribute name="width" select="$column_width[@name=$col/@name]"/>
								</xsl:if>
								<xsl:call-template name="utd:excelAttributes">
									<xsl:with-param name="input" select="current()"/>
									<xsl:with-param name="header" select="true()"/>
								</xsl:call-template>
								<xsl:value-of select="$col"/>
							</head>
						</xsl:for-each>
					</header>
				</headers>
				<rows>
					<xsl:for-each select="$runs/run[@id = $calcs/values/sample/runid]">
						<xsl:sort select="runid" data-type="number"/>
						<xsl:variable name="run" select="."/>
						<xsl:variable name="rpos" select="position()"/>
						<xsl:variable name="rlast" select="last()"/>
						<xsl:for-each select="distinct-values($calcs/values/sample[runid=$run/@id]/nominalconc)">
							<xsl:sort select="." data-type="number"/>
							<xsl:variable name="npos" select="position()"/>
							<xsl:variable name="nlast" select="last()"/>
							<xsl:variable name="nominalconc" select="."/>
							<xsl:for-each select="distinct-values($calcs/values/sample[runid=$run/@id][nominalconc=$nominalconc]/subject)">
								<xsl:sort select="." data-type="number"/>
								<xsl:variable name="subject" select="."/>
								<xsl:variable name="xpos" select="position()"/>
								<xsl:variable name="xlast" select="last()"/>
								<row bracket="{$run/@id}">
									<xsl:variable name="params" as="node()*">
										<xsl:call-template name="parameters">
											<xsl:with-param name="run" select="$run"/>
											<xsl:with-param name="pos" select="$rpos"/>
											<xsl:with-param name="sample" select="($calcs/values/sample[runid=$run/@id][nominalconc=$nominalconc][subject=$subject])[1]"/>
										</xsl:call-template>
									</xsl:variable>
									<xsl:for-each select="$columns">
										<cell>
											<xsl:for-each select="$params[@name=current()/@name]/@*[not( name() = ('name') )]">
												<xsl:attribute name="{ current()/name() }" select="current()"/>
											</xsl:for-each>
											<xsl:if test="$column_width[@name=current()/@name]">
												<xsl:attribute name="width" select="$column_width[@name=current()/@name]"/>
											</xsl:if>
											<xsl:if test="$params[@name=current()/@name]/@group and not($params[@name=current()/@name]/@class)">
												<xsl:attribute name="class">borderBottomLeft</xsl:attribute>
											</xsl:if>
											<xsl:if test="not($params[@name=current()/@name]/@group)">
												<xsl:attribute name="class">borderNone</xsl:attribute>
											</xsl:if>
											<xsl:if test="$xpos=$xlast and not($params[@name=current()/@name]/@group)">
												<xsl:attribute name="class">borderBottom</xsl:attribute>
											</xsl:if>
											<xsl:if test="current()/@name=('NOMINALCONC', 'S1-MEAN')">
												<xsl:attribute name="class" select="'border'||(if (last()=1) then 'All' else ((if ($xpos=$xlast) then 'Bottom' else '')||'Left'))"/>
											</xsl:if>
											<xsl:call-template name="utd:excelAttributes">
												<xsl:with-param name="input" select="current()"/>
											</xsl:call-template>
											<xsl:value-of select="$params[@name=current()/@name]"/>
										</cell>
									</xsl:for-each>
									<xsl:for-each select="$calcs">
										<xsl:variable name="sample" select="values/sample[runid=$run/@id][nominalconc=$nominalconc][subject=$subject]"/>
										<xsl:for-each select="$stats_individual">
											<!--xsl:variable name="stat" select="utd:formats($experiment,'var')[@type=$key and @key=current()/@key]"/-->
											<cell>
												<!--xsl:if test="$xpos = $xlast">
													<xsl:attribute name="class">borderBottom</xsl:attribute>
												</xsl:if-->
												<xsl:if test="$npos = $nlast and $xpos = $xlast">
													<xsl:attribute name="class">borderBottom</xsl:attribute>
												</xsl:if>
												<xsl:attribute name="class" select="'border'||(if (last()=1) then 'All' else ((if ($xpos=$xlast) then 'Bottom' else '')||(if (position()=1) then 'Left' else if (position()=last()) then 'Right' else '')))"/>
												<xsl:choose>
													<xsl:when test="@key = ('S1-MEAN') ">
														<xsl:if test="$sample/sample1/sample/deactivated = 'T' ">
															<xsl:attribute name="deactivated" select="true()"/>
															<xsl:attribute name="reason" select="$sample/sample1/sample[deactivated='T']/(samplename||(if (reason != '') then ': '||reason else ''))" separator=", "/>
														</xsl:if>
														<xsl:if test="$sample/s1-cvflag = 'T'">
															<xsl:attribute name="footnote" select="'CV above acceptance criteria of '||$attributes/precision_acceptance_criteria||'%'"/>
														</xsl:if>
													</xsl:when>
													<!--xsl:when test="@key = ('S1-CV') and $sample/s1-cvflag = 'T'">
														<xsl:attribute name="footnote" select="'CV above acceptance criteria of '||$attributes/precision_acceptance_criteria||'%'"/>
													</xsl:when-->
													<xsl:when test="@key = ('S2-MEAN') ">
														<xsl:if test="$sample/sample2/sample/deactivated = 'T' ">
															<xsl:attribute name="deactivated" select="true()"/>
															<xsl:attribute name="reason" select="$sample/sample2/sample[deactivated='T']/(samplename||(if (reason != '') then ': '||reason else ''))" separator=", "/>
														</xsl:if>
														<xsl:if test="$sample/s2-cvflag = 'T'">
															<xsl:attribute name="footnote" select="'CV above acceptance criteria of '||$attributes/precision_acceptance_criteria||'%'"/>
														</xsl:if>
													</xsl:when>
													<!--xsl:when test="@key = ('S2-CV') and $sample/s2-cvflag = 'T'">
														<xsl:attribute name="footnote" select="'CV above acceptance criteria of '||$attributes/precision_acceptance_criteria||'%'"/>
													</xsl:when-->
												</xsl:choose>
												<xsl:call-template name="utd:excelAttributes">
													<xsl:with-param name="input" select="current()"/>
												</xsl:call-template>
												<xsl:value-of select="$sample/*[name()=lower-case(current()/@key)]"/>
											</cell>
										</xsl:for-each>
									</xsl:for-each>
									<xsl:for-each select="$column_behind">
										<xsl:variable name="name" select="."/>
										<cell>
											<xsl:if test="not(empty($column_group[@name=current()/@name]))">
												<xsl:attribute name="group" select="$column_group[@name=current()/@name]"/>
											</xsl:if>
											<xsl:if test="$column_width[@name=current()/@name]">
												<xsl:attribute name="width" select="$column_width[@name=current()/@name]"/>
											</xsl:if>
											<!--xsl:if test="$xpos = $xlast">
												<xsl:attribute name="class">borderBottom</xsl:attribute>
											</xsl:if-->
											<xsl:if test="$npos = $nlast and $xpos = $xlast">
												<xsl:attribute name="class">borderBottom</xsl:attribute>
											</xsl:if>
											<xsl:call-template name="utd:excelAttributes">
												<xsl:with-param name="input" select="current()"/>
											</xsl:call-template>
											<xsl:value-of select="$params[@name=$name]"/>
										</cell>
									</xsl:for-each>
								</row>
							</xsl:for-each>
						</xsl:for-each>
					</xsl:for-each>
				</rows>
			</isr-table>
		</xsl:variable>
		<!-- NOTES -->
		<xsl:variable name="isr-notes">
			<isr-notes>
				<xsl:for-each select="utd:formats($experiment,'footer')[starts-with(@name, 'LINE')]">
					<note>
						<xsl:value-of select="."/>
					</note>
				</xsl:for-each>
			</isr-notes>
		</xsl:variable>
		<!-- PAGE -->
		<xsl:variable name="isr-page">
			<isr-page>
				<xsl:attribute name="createCap" select="true()"/>
				<xsl:attribute name="number" select="$number"/>
				<xsl:attribute name="hash" select="$hash"/>
				<xsl:attribute name="viewDeactivated" select="if ($deactivated[@name='VIEW'] = 'F') then false() else true() "/>
				<xsl:copy-of select="$isr-parameters"/>
				<xsl:copy-of select="$isr-caption"/>
				<xsl:copy-of select="$isr-table"/>
				<xsl:copy-of select="$isr-notes"/>
			</isr-page>
		</xsl:variable>
		<xsl:copy-of select="$isr-page"/>
	</xsl:template>
	<!--
==========================================================================
calcrows
========================================================================== -->
	<xsl:template name="calcrows">
		<xsl:param name="calcs"/>
		<xsl:param name="analyte"/>
		<xsl:param name="species"/>
		<xsl:param name="matrix"/>
		<xsl:param name="runid"/>
		<xsl:variable name="paramtitles" select="$stats_intra/text()"/>
		<xsl:variable name="paranames" select="$stats_intra/lower-case(@key)"/>
		<xsl:for-each select="1 to count($paramtitles)">
			<xsl:variable name="xpos" select="position()"/>
			<xsl:variable name="xlast" select="last()"/>
			<row>
				<cell class="sum">
					<xsl:copy-of select="utd:borderclass( $xpos, $xlast, false(), true() )"/>
					<xsl:value-of select="$paramtitles[$xpos]"/>
				</cell>
				<xsl:if test="(count($columns)-1) gt 0">
					<xsl:for-each select="for $i in 1 to ( count($columns) -1 ) return $i">
						<cell>
							<xsl:copy-of select="utd:borderclass( $xpos, $xlast, false(), true() )"/>
							<xsl:text>&#160;</xsl:text>
						</cell>
					</xsl:for-each>
				</xsl:if>
				<xsl:for-each select="$calcs">
					<xsl:variable name="calc" select="."/>
					<xsl:variable name="analyteid" select="analyteid"/>
					<xsl:variable name="calculation" select="$calc[target/runid=$runid]"/>
					<!--xsl:variable name="calculation" select="$calculationsetintra[target/assayanalyteid=$analyteid][target/name=$qcname][target/knownconc=$knownconc][target/runid=$runid][target/cycles=$cycle]"/-->
					<xsl:variable name="individualparams" as="node()*">
						<param name="bias">
							<xsl:value-of select="$calculation/result/bias"/>
						</param>
					</xsl:variable>
					<xsl:for-each select="$stats_individual/@key">
						<xsl:variable name="name" select="lower-case( substring-before(current(), '#') )"/>
						<xsl:variable name="column" select="lower-case( substring-after(current(), '#') )"/>
						<xsl:choose>
							<xsl:when test="$name='concentration'">
								<cell>
									<xsl:copy-of select="utd:borderclass( $xpos, $xlast, false(), true() )"/>
									<xsl:value-of select="$calculation/result/*[name()=$paranames[$xpos]]"/>
								</cell>
							</xsl:when>
							<xsl:otherwise>
								<cell>
									<xsl:copy-of select="utd:borderclass( $xpos, $xlast, false(), true() )"/>
									<xsl:if test="$paranames[$xpos]=$column">
										<xsl:value-of select="$individualparams[@name=$name]"/>
									</xsl:if>
								</cell>
							</xsl:otherwise>
						</xsl:choose>
					</xsl:for-each>
				</xsl:for-each>
				<xsl:if test="(count($column_behind)) gt 0">
					<cell colspan="{(count($column_behind))}">
						<xsl:copy-of select="utd:borderclass( $xpos, $xlast, false(), true() )"/>
						<xsl:text>&#160;</xsl:text>
					</cell>
				</xsl:if>
			</row>
		</xsl:for-each>
	</xsl:template>
	<!--
==========================================================================
parameters
==========================================================================-->
	<xsl:template name="parameters">
		<xsl:param name="run"/>
		<xsl:param name="pos"/>
		<xsl:param name="sample"/>
		<xsl:variable name="all-run" select="$runs/allruns/run[runid=$run/runid]"/>
		<param name="RUNID" class="borderBottomLeft" group="true_{$run/runid}">
			<xsl:value-of select="$run/runid"/>
		</param>
		<param name="STARTDATE" class="borderBottomLeft" group="true_{$run/runid}">
			<xsl:value-of select="$run/runstartdate"/>
		</param>
		<param name="RUNTYPE" class="borderBottomLeft" group="true_{$run/runid}">
			<xsl:value-of select="$run/runtypedescription"/>
		</param>
		<param name="ANALYST" class="borderBottomLeft" group="true_{$run/runid}">
			<xsl:value-of select="$all-run/analyst" separator=", "/>
		</param>
		<param name="INTRARUNPOS" class="borderBottomLeft" group="true_{$run/runid}">
			<xsl:value-of select="$pos" separator=", "/>
		</param>
		<param name="ITERATOR" class="borderBottomRight" group="true_{$run/runid}">
			<xsl:value-of select="$run/runid" separator=", "/>
		</param>
		<!--param name="NCMEANMEDIAN" class="borderBottom" group="true_{$run/runid}">
			<xsl:value-of select="$sample/nc-meanmedian"/>
		</param-->
		<param name="SCREENCP" class="borderBottomLeft" group="true_{$run/runid}">
			<xsl:value-of select="$sample/screen-cp"/>
		</param>
		<param name="CONFIRMCP" class="borderBottomLeft" group="true_{$run/runid}">
			<xsl:value-of select="$sample/confirm-cp"/>
		</param>
		<param name="TITERCP" class="borderBottomLeft" group="true_{$run/runid}">
			<xsl:value-of select="$sample/borderBottomLeft"/>
		</param>
		<xsl:for-each select="$sample/*[name()!='runid']">
			<param name="{upper-case(name())}">
				<xsl:choose>
					<xsl:when test="name() = ('nc-meanmedian') ">
						<xsl:attribute name="group" select="'true_'||$run/runid"/>
						<xsl:if test="$sample/sample1/sample/deactivated = 'T' ">
							<xsl:attribute name="deactivated" select="true()"/>
							<xsl:attribute name="reason" select="$sample/nc/sample[deactivated='T']/(samplename||(if (reason != '') then ': '||reason else ''))" separator=", "/>
						</xsl:if>
						<xsl:if test="$sample/nc-cvflag = 'T'">
							<xsl:attribute name="footnote" select="'CV above acceptance criteria of '||$attributes/precision_acceptance_criteria||'%'"/>
						</xsl:if>
					</xsl:when>
					<!--xsl:when test="name() = ('nc-cv') and $sample/nc-cvflag = 'T'">
						<xsl:attribute name="footnote" select="'CV above acceptance criteria of '||$attributes/precision_acceptance_criteria||'%'"/>
					</xsl:when-->
					<xsl:when test="name() = ('s1-mean') ">
						<xsl:if test="$sample/sample1/sample/deactivated = 'T' ">
							<xsl:attribute name="deactivated" select="true()"/>
							<xsl:attribute name="reason" select="$sample/sample1/sample[deactivated='T']/(samplename||(if (reason != '') then ': '||reason else ''))" separator=", "/>
						</xsl:if>
						<xsl:if test="$sample/s1-cvflag = 'T'">
							<xsl:attribute name="footnote" select="'CV above acceptance criteria of '||$attributes/precision_acceptance_criteria||'%'"/>
						</xsl:if>
					</xsl:when>
					<!--xsl:when test="name() = ('s1-cv') and $sample/s1-cvflag = 'T'">
						<xsl:attribute name="footnote" select="'CV above acceptance criteria of '||$attributes/precision_acceptance_criteria||'%'"/>
					</xsl:when-->
					<xsl:when test="name() = ('s2-mean') ">
						<xsl:attribute name="class">borderBottomLeft</xsl:attribute>
						<xsl:if test="$sample/sample2/sample/deactivated = 'T' ">
							<xsl:attribute name="deactivated" select="true()"/>
							<xsl:attribute name="reason" select="$sample/sample2/sample[deactivated='T']/(samplename||(if (reason != '') then ': '||reason else ''))" separator=", "/>
						</xsl:if>
						<xsl:if test="$sample/s2-cvflag = 'T'">
							<xsl:attribute name="footnote" select="'CV above acceptance criteria of '||$attributes/precision_acceptance_criteria||'%'"/>
						</xsl:if>
					</xsl:when>
					<!--xsl:when test="name() = ('s2-cv') and $sample/s2-cvflag = 'T'">
						<xsl:attribute name="footnote" select="'CV above acceptance criteria of '||$attributes/precision_acceptance_criteria||'%'"/>
					</xsl:when-->
				</xsl:choose>
				<xsl:value-of select="."/>
			</param>
		</xsl:for-each>
	</xsl:template>
</xsl:stylesheet>
