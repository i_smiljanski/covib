CREATE OR REPLACE PACKAGE ISR$COVIB$BIOANALYTICS$WT$LAL
AS
-- ***************************************************************************************************
-- Description/Usage:
-- the package is the inerface between the underlaying Watson LIMS of Abbott Laboratories 
-- to the backend of iStudyReporter  
-- ***************************************************************************************************
-- Modification history:
-- Ver   Date          Autor    System            Project                         Remarks
-- ***************************************************************************************************
-- 1.0   28.May 2008  JB       iStudyReporter    iStudyReproter@watson           Creation
-- ***************************************************************************************************
-- functions and procedures
-- ****************************************************************************************************

FUNCTION getRunStates RETURN iSR$Text$List PIPELINED;
-- ***************************************************************************************************
-- Date and Autor:  02.Dec 2008  HR
-- Pipelined function returning valid run types
-- The valid runs states have to be congured in one entry in ISR$PARAMETER$VALUES with entity = 'WT$RUN$STATES'
-- For more than one valid state, the values should be entered comma separated without blancs
--
-- Parameters:
--  none
--
-- Return:
--  pipelined run state values
--
-- ***************************************************************************************************

--FUNCTION getAutoSampTime (sRunCode in VARCHAR2, nStudycode IN NUMBER) return number;
-- ***************************************************************************************************
-- Date and Autor:  03.Apr 2009, JB
-- function calculates the autosampler time as difference 
-- by subtracting the minimum
-- injection date/time for ASQCs from the maximum injection date/time for ARQCs.
-- hours will be rounded down to the nearest integer and presented below the table.
-- Function is called from the calucution package
--
-- Parameter :
--  sRunCode                     the run id 
--  nStudycode                  the study id
--
-- Return:
--  time in hours                   
--
-- ***************************************************************************************************

function fmtParamVal(nVal in number, csParamNAME in varchar2, sRoundNotSigFig in Varchar2, nFmtParam IN integer) return varchar2;
-- ***************************************************************************************************
-- Date und Autor: 2.Mrz 2009  HR
-- function formats a parameter number depending on its parameter name
--  If the name is Tmax and then the value is rounded and formatted to the number  
--    of decimal places sprecified in DECIMAL_PLACES_TMAX if DECIMAL_PLACES_TMAX is specified. 
--  If the name is Tmax and DECIMAL_PLACES_TMAX is is not specified, or the parameter name
--  is other than Tmax,the number is formatted using FormatSig and sigFigure 
--  respective FormatRounded and rounddepending on the parameter sRoundNotSigFig
--
-- Parameter:
-- nVal            the number which should be formatted
-- csParamNAME     the parameter name
-- sRoundNotSigFig if 'F' then fromat using significant figures. If 'T' then fromat using decimal places
-- nFmtParam       the number of significant figures or decimal places for parameters other than Tmax
--
-- Return:
-- the formatted number as text
--
-- ***************************************************************************************************

Function FormatRowValue(csEntityName in varchar2, csFormat in varchar2, dataRow in Rowid) return varchar2;
-- ***************************************************************************************************
-- Date und Autor: 02.Okt 2009  HR
--   builds a formatted string with data from the row in dataRow of the table in csEntityName
--   the data columns have to be numeric. The format in csFormat containes the colums names of the data
--  Example: [endday//studyday//d ][endhour/2]:[endminute/2]:[endsecond/2]
--  the text in [] containes max 5 parameters seperated with a /
--   - source data column
--   - number of digits (before a possible decimal separator
--   - compare data column - if data is the same as source data column, then the data should not be included in the formatted string
--   .....
--
-- Parameter:
-- csEntityName    
-- csFormat  
-- dataRow
--
-- Return:
-- the value 
--
-- ***************************************************************************************************

FUNCTION getStudyConf(csStudyid IN varchar2, csFieldName IN varchar2) RETURN varchar2;
-- ***************************************************************************************************
-- Date und Autor: 15.Sep 2009  HR
-- retrieves the fieldvalue from WT$STUDY$CONFIG for the study in csStudyid and FIELDNAME in csFieldName
--
-- Parameter:
-- csStudyid    the stud ID
-- csFieldName  the field name
--
-- Return:
-- the value from fieldvalue
--
-- ***************************************************************************************************

FUNCTION StudyUsesGender(cnStudy IN number) RETURN varchar2;
-- ***************************************************************************************************
-- Date and Autor:  23.Jul 2008 HR
--  checks if a study uses genders in the study design. It counts the distinct genderid in designsubject
--
-- Parameters:
-- cnStudy          the study id
--
-- Return:
-- 'T' if genders are found and 'F' if no genders are defined
--
-- ***************************************************************************************************

--FUNCTION getLov(csParameter IN VARCHAR2, sFilter IN VARCHAR2, oSelectionList OUT ISR$TLRSELECTION$LIST) RETURN STB$OERROR$RECORD;
-- ***************************************************************************************************
-- Date and Autor:  28.May 2008 jb
-- function gets the list of values for a entity
--
-- Parameters:
-- csParameter       the entity name for which the list of values is retrieved
-- sFilter           string that can be used to filter  the list of values
-- oSelectionList    the list of values
--
-- Return:
-- STB$OERROR$RECORD    
--
-- ***************************************************************************************************

FUNCTION getGridMaster(cnMaskNo IN NUMBER, sMasterEntity OUT STB$TYPEDEF.tLine) RETURN STB$OERROR$RECORD;
-- ***************************************************************************************************
-- Date and Autor:  24. Okt 2008 HR
-- the function is called from package iSR$Grid$Mask and determines the master entity if more than one
-- entity  can be used as master entity. 
-- This decision is made depending on the selection in a previous step in the wizard   
--
-- Parameters:
-- cnMaskNo        the screen number in the reprot wizard
-- sMasterEntity   the grid master entity that should be used
--
-- Return:
-- STB$OERROR$RECORD   
--
-- ***************************************************************************************************

--FUNCTION GetGridMaskAdds(sEntity IN STB$TYPEDEF.tLine, csMasterKey IN varchar2, lAdds OUT iSR$Crit$List) RETURN STB$OERROR$RECORD;
-- ***************************************************************************************************
-- Date and Autor:  03. Nov 2008 HR
-- Delivers a collection of type iSR$Crit$List containing the summary entries tthat need to be added at 
-- the end of the grid master of the graphics.  
-- For the entities 'WT$SUBJGRPDAY' and 'WT$SUBJECT' the entries are selected from ISR$WT$STUDY$TREATMENT
-- with the condition doseamount != 0
-- For other entities entries to be added can be confured in iSR$Parameter$Values by adding the suffix '_ADD' 
-- to the entity name
--
-- Parameters:
--  sEntity       the grid master entity for which additional entries can be added
--  csMasterKey   the master key to be used in the returned list elements
--  lAdds         colection of type iSR$Crit$List to be filled
--
-- Return:
--  STB$OERROR$RECORD    
--
-- ***************************************************************************************************

FUNCTION prepareForXML(csData IN VARCHAR2, csType IN VARCHAR2) RETURN VARCHAR2;
-- ***************************************************************************************************
-- Date and Autor:  06.Apr 2009 MCD
-- prepares the data value for the xml, this can be e.g. formatting of date and number values, 
-- rounding of number values, removing of disallowed signs ... 
--
-- Parameter :
-- csData   the value which should be prepared
-- csType   the data type of the value
--
-- Return:
-- VARCHAR2 the prepared value
--
-- ***************************************************************************************************

--FUNCTION preparePreXML(cnJobId IN NUMBER, oParamList IN ISR$ATTRLIST$REC) RETURN STB$OERROR$RECORD;
-- ***************************************************************************************************
-- Date and Autor:  06.Apr 2009 MCD
-- prepares the temp tables before the xml creation starts, this can be e.g doing calculations 
-- on the temp tables, filling empty columns in the temp tables or formatting values in 
-- the temp tables 
--
-- Parameter :
-- cnJobId    the current jobid
-- oParamList the current parameter list
--
-- Return:
-- STB$OERROR$RECORD
--
-- ***************************************************************************************************

FUNCTION preparePostXML(cnJobId IN NUMBER, oParamList IN ISR$ATTRLIST$REC, cxXML IN OUT XMLTYPE) RETURN STB$OERROR$RECORD;
-- ***************************************************************************************************
-- Date and Autor:  06.Apr 2009 MCD
-- prepares the generated xml, this can be e.g adding or removing nodes in the dom
--
-- Parameter :
-- cnJobId    the current jobid
-- oParamList the current parameter list
-- nDomId     the xml in dom format
--
-- Return:
-- STB$OERROR$RECORD
--
-- ***************************************************************************************************

FUNCTION getDefaultValues (oSelection in OUT ISR$TLRSELECTION$LIST,csDefaultToken in VARCHAR2,csEntity in VARCHAR2) RETURN STB$OERROR$RECORD;
-- ***************************************************************************************************
-- Date and Autor:  07.Jul 2006 jb
-- retrieves the default values of a selection screen. it is possible to define string filters that are
-- used in a "like" expression or to define "logical" filters that require a function or regular expression
-- to resolve the values
-- of the data
--
-- Parameter :
-- oSelection         list of values belonging to a wizard entity
-- csDefaultToken    the default token of the wizard selection screen (STRING or Logical)
-- csEntity          the entity of the wizard screen
--
-- Return:
-- STB$OERROR$RECORD    
--
-- ***************************************************************************************************

FUNCTION GetEntityDefault(sEntity IN STB$TYPEDEF.tLine, csMasterKey IN varchar2, oRow OUT iSR$Crit$Rec) RETURN STB$OERROR$RECORD;
-- ***************************************************************************************************
-- Date and Autor:  24. Okt 2008 HR
-- The function is called from package iSR$Grid$Mask to get the default entry in a grid cell 
--  The values for the entry are selected from iSR$Parameter$Values using the condition
--  that entity is the value from sEntity suffixed by '_DEFAULT';
--
-- Parameter :
--  sEntity      the column entity of the grid
--  csMasterKey  the master key to be used in the returned object
--  oRow         a object of type iSR$Crit$Rec containing the data for the default entry in a grid cell
--
-- Return:
--  STB$OERROR$RECORD    
--
-- ***************************************************************************************************

Function GetFetalDamMatrixKey(csStudyID in varchar2, nColumn in integer, csTempTabPrefix in varchar2) return number;
-- ***************************************************************************************************
-- Date and Autor:  12. Okt 2010 HR
-- The function is called by the query for the calculation PKFETALDAM
--  It retrieves the matrixkey of the first (dam) or second (fetal) column
--
-- Parameter :
--  csStudyID      the study ID
--  nColumn        the column 
--
-- Return: the Matrixkey of the first or second column
--
-- ***************************************************************************************************

Function IsFetalDamStudy(csStudyID in varchar2) return varchar2;
-- ***************************************************************************************************
-- Date and Autor:  10. Dec 2010 HR
-- The function is called by the query for the calculations pkvalues and pkparameters
--  It checks if the study is a fetal/dam study. therefore it checks 
--    if the matrix table contains a matrix with the fetal suffix in its name 
--
-- Parameter :
--  csStudyID      the study ID
--
-- Return: 'T' if the study is a fetal/dam study and 'F' otherwise
--
-- ***************************************************************************************************

FUNCTION getLov(sEntity in VARCHAR2, sSuchwert in VARCHAR2, oSelectionList OUT ISR$TLRSELECTION$LIST) RETURN STB$OERROR$RECORD;
-- ***************************************************************************************************
-- Date und Autor: 13 Mar 2018  HSP
-- returns the a list of values of an data entity
--
-- Parameters:
-- sEntity            the entity to which the list of values belongs
-- sSuchwert          search string to filter/ limit the list 
-- oSelectionList     the list of values  belong to the passed entity
--
-- Return:
-- STB$OERROR$RECORD    
--
-- ***************************************************************************************************

END ISR$COVIB$BIOANALYTICS$WT$LAL;