CREATE OR REPLACE PACKAGE ISR$COVIB$PACKAGE
is
-- ***************************************************************************************************
-- Description/Usage:
-- Package for COVIB specific procedures.´
-- ****************************************************************************************************

FUNCTION copyFilesIntoJobtrail(cnJobid in NUMBER, cxXml IN OUT XMLTYPE) RETURN STB$OERROR$RECORD;
-- ***************************************************************************************************
-- Date and Autor: 13.Mar 2018, HSP
-- copies all selected files to the table stb$jobtrail.
-- Files can be any kind templates,xml-files or a parser module.
--
-- Parameter:
-- cnJobid   the identifier of the iStudyReporter job
-- cxXml     the xml configuration file
--
-- Return:
-- STB$OERROR$RECORD  
--
-- ***************************************************************************************************

FUNCTION setFileFilter(cnJobid IN NUMBER, cxXml IN OUT XMLTYPE) RETURN STB$OERROR$RECORD;
-- ***************************************************************************************************
-- Date and Author: 10 Jun 2014, HSP
-- sets filter job parameters according to selected experiment 
--
-- Parameter:
-- cnJobid   the identifier of the iStudyReporter job
-- cnDomId   the domid of the xml configuration file
--
-- Return:
-- STB$OERROR$RECORD  
--
-- ***************************************************************************************************

FUNCTION loadFilesToClient(cnJobid in NUMBER, cxXml IN OUT XMLTYPE) RETURN STB$OERROR$RECORD;
-- ***************************************************************************************************
-- Date and Autor: 08.Jan 2007, MCD
-- PL-SQL wrapper for the java-class to upload the files to the client computer from the database
-- table stb$jobtrail. The function uploads all files of the current job.
--
-- Parameter:
-- cnJobid   the identifier of the iStudyReporter job
-- cnDomId   the domid of the xml configuration file
--
-- Return:
-- STB$OERROR$RECORD  
--
-- ***************************************************************************************************

FUNCTION startApp(cnJobid in NUMBER, cxXml IN OUT XMLTYPE) RETURN STB$OERROR$RECORD;
-- ***************************************************************************************************
-- Date and Autor: 28.Jun 2018, HSP
-- starts the client application, 
-- the modus (synchron or asynchron) can be found in the config.xml
-- return values of the applications are 
-- -1 (any error)
-- -2 (if the user choose [Cancel]) 
-- -3 (if a system error in the wordmodule occured)
-- -4 (if the user trys to checkin a wrong document) 
--
-- Parameter:
-- cnJobid   the identifier of the iStudyReporter job
-- cnDomId   the domid of the xml configuration file
--
-- Return:
-- STB$OERROR$RECORD  
--
-- ***************************************************************************************************

FUNCTION createParametersExcel(cnJobid IN NUMBER, cxXml IN OUT XMLTYPE) RETURN STB$OERROR$RECORD;
-- ***************************************************************************************************
-- Date and Autor: 15.Mar 2018, HSP
-- creates the parameter string in the "config.xml" for the MUIB Excel Reports
--
-- Parameter:
-- cnJobid   the identifier of the iStudyReporter job
-- cnDomId   the domid of the xml configuration file
--
-- Return:
-- STB$OERROR$RECORD  
--
-- ***************************************************************************************************

function split (csList varchar2, csDelimiter varchar2 := ',') return SYS.ODCIVARCHAR2LIST deterministic;
-- ***************************************************************************************************
-- Date and Autor: 18, Jun 2014, HSP
-- splitting a string according to the specified delimiter and return a collection
--
-- Parameter:
-- csList          the string to split
-- csDelimiter     the delimiter
--
-- Return
-- SYS.ODCIVARCHAR2LIST (deterministic)
--
-- ***************************************************************************************************

function splitNumbers (csList varchar2, csDelimiter varchar2 := ',', csRange varchar2 := '-') return SYS.ODCINUMBERLIST pipelined;
-- ***************************************************************************************************
-- Date and Autor: 26, Aug 2021, HSP
-- splitting a string according to the specified delimiter and range and return all values as number in a pipelined table
--
-- Parameter:
-- csList          the string to split
-- csDelimiter     the delimiter
-- csRange         the range marker
--
-- Return
-- SYS.ODCIVARCHAR2LIST (pipelined)
--
-- Example:
-- '1,2,4-7,9' returns (1, 2, 4, 5, 6, 7, 9) through a pipe
-- "runid in (select column_value from splitNumbers('1,2,4-7,9')" will match only the numbers (1, 2, 4, 5, 6, 7, 9)
--
-- ***************************************************************************************************

function putParameters( oParameter  in STB$MENUENTRY$RECORD, 
                        olParameter in STB$PROPERTY$LIST  )
  return stb$oerror$record;
-- ***************************************************************************************************
-- Date and Autor: 27.Jun.2018, vs
-- ***************************************************************************************************

function setMenuAccess ( sParameter   in  varchar2, 
                         sMenuAllowed out varchar2 ) 
  return stb$oerror$record;
-- ***************************************************************************************************
-- Date and Autor: 27.Jun.2018, vs
-- ***************************************************************************************************

function callFunction( oParameter IN  STB$MENUENTRY$RECORD, 
                       olNodeList OUT STB$TREENODELIST )
  return stb$oerror$record;
-- ***************************************************************************************************
-- Date and Autor: 28.Jun.2018, vs
-- ***************************************************************************************************  
FUNCTION getLov (sEntity IN VARCHAR2, ssuchwert IN VARCHAR2, oSelectionList OUT isr$tlrselection$list)  RETURN stb$oerror$record;
--******************************************************************************
FUNCTION sendEmail(cnJobid in NUMBER, cxXml IN OUT XMLTYPE) RETURN STB$OERROR$RECORD;


FUNCTION foreCast( cnX in number,
                  cnKY in SYS.ODCINUMBERLIST,
                  cnKX in SYS.ODCINUMBERLIST)
  return number;
-- ***************************************************************************************************
-- Date and Autor: 29, Oct 2019, HSP
-- replicates the Excel forecast function
--
-- Parameter:
-- cnX    Cutpoint
-- cnKY   List of numbers for the y-axis
-- cnKX   List of numbers for the x-axis
--
-- Return:
-- NUMBER
--
-- ***************************************************************************************************

FUNCTION getSSTLimits (csStudyID IN VARCHAR2, csKnownEntity IN VARCHAR2, csSSTMasterKey IN VARCHAR2, cnRepID IN NUMBER, cnAcceptedRunAnalyte IN NUMBER, cnResponseFigures IN NUMBER, cnSSTSDNumber IN NUMBER) RETURN ISR$COVIB$SST$LIMIT$LIST DETERMINISTIC;
-- ***************************************************************************************************
-- Date and Autor: 16, Jan 2020, HSP
-- Calculates the System Suitability Limits and returns them in a collection
--
-- Parameter:
-- csStudyID              StudyID for the current Calc
-- csKnownEntity          KnownEntity that was set for the Calc
-- csSSTMasterKey         SSTMasterKey that was set for the Calc (or report default)
-- cnRepID                RepID for the current Job
-- cnAcceptedRunAnalyte   AcceptedRunaAnalyte number
-- cnResponseFigures      Significant figures to be used for the Mean of the response
-- cnSSTSDNumber          Number to that SD should be multiplied by
--
-- Return
-- ISR$COVIB$SST$LIMIT$LIST (deterministic)
--
-- ***************************************************************************************************

FUNCTION BuildDTAFields(csCalcStatmentAlias in VARCHAR2, csDTAType in VARCHAR2) return VARCHAR2;
-- ***************************************************************************************************
-- Date and Autor: 7, Feb 2020, HSP
-- Fields to be selected for the DTA tables using ISR$FILE$CALC$VALUES.
--
-- Parameter:
-- csCalcStatmentAlias    Alias for the Calc
-- csDTAType              DTAType to determine for which DTAID the fields should be pulled and which columns
--
-- Return
-- VARCHAR2
--
-- ***************************************************************************************************
function createAuditFile( cnJobid in     number, cxXml   in out xmltype) return STB$OERROR$RECORD;
-- ***************************************************************************************************

end ISR$COVIB$PACKAGE;