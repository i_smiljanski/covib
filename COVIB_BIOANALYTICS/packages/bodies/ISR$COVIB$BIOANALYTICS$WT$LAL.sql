CREATE OR REPLACE PACKAGE BODY                ISR$COVIB$BIOANALYTICS$WT$LAL IS

  xXML    XMLTYPE;
  sReturn VARCHAR2(4000);

  sBackupEntity       VARCHAR2(30);
  sBackupID           VARCHAR2(100);
  oSelection1Backup   ISR$TLRSELECTION$LIST;
  oSelection2Backup   ISR$TLRSELECTION$LIST;
  oSelection3Backup   ISR$TLRSELECTION$LIST;


  --*********************************************************************************************
  FUNCTION strTokenize (stotokenize IN VARCHAR2, sdelimiter IN VARCHAR2, ltokensarray OUT stb$typedef.tlstrings)
    RETURN stb$oerror$record IS
    oerrorobj                     stb$oerror$record := STB$OERROR$RECORD();
    sCurrentName       constant varchar2(100) := $$PLSQL_UNIT||'.strTokenize(' || stotokenize || ')'; 
    nanzahl                       INTEGER := 1;
    nposition                     INTEGER := 1;
    nalteposition                 INTEGER := 0;
  BEGIN
    isr$trace.stat ('begin', 'sDelimiter: ' || sdelimiter, sCurrentName);
    nposition := instr (stotokenize, sdelimiter, 1, nanzahl);

    WHILE nposition > 0 LOOP
      ltokensarray (nanzahl) := substr (stotokenize, nalteposition + 1, nposition - nalteposition - 1);
      nalteposition := nposition;
      nanzahl := nanzahl + 1;
      nposition := instr (stotokenize, sdelimiter, 1, nanzahl);
    END LOOP;

    ltokensarray (nanzahl) := substr (stotokenize, nalteposition + 1);
    isr$trace.stat ('end', 'number of tokens: ' || nanzahl, sCurrentName);
    RETURN (oerrorobj);
  EXCEPTION
    WHEN OTHERS THEN
     oErrorObj.handleError( Stb$typedef.cnSeverityCritical, utd$msglib.getmsg ('exUnexpectedError', stb$security.getCurrentLanguage,  csP1 => sqlerrm(sqlcode)),sCurrentName,
            dbms_utility.format_error_stack ||stb$typedef.crlf|| dbms_utility.format_error_backtrace ); 
      RETURN (oerrorobj);
  END strtokenize;

--==**********************************************************************************************************************************************==--
FUNCTION GetStudyConf(csStudyid IN varchar2, csFieldName IN varchar2) RETURN varchar2 IS
  TYPE      tDynCursor IS ref CURSOR;
  cStudConf tDynCursor;
  sConf     TMP$BIO$STUDY$CONFIG.fieldvalue%TYPE; 
BEGIN

  OPEN cStudConf FOR 'SELECT fieldvalue
                        FROM '||STB$UTIL.GetSystemParameter('TEMP_TAB_PREFIX')||'BIO$STUDY$CONFIG
                       WHERE Studyid = :csStudyid
                         AND FieldName = :csFieldName' USING IN csStudyid, IN csFieldName;
  FETCH cStudConf INTO sConf;
  CLOSE cStudConf;

  RETURN sConf;

END GetStudyConf;

--***************************************************************************************************
FUNCTION GetRunStates RETURN ISR$Text$List PIPELINED IS
  sRunStates    VARCHAR2(200);
  lTokensArray  STB$TYPEDEF.tlStrings;
  oErrorObj     STB$OERROR$RECORD := STB$OERROR$RECORD();

  -- Cursor for Run States
  CURSOR cGetRunStates IS     
   SELECT display
   FROM   ISR$PARAMETER$VALUES
   WHERE  entity = 'BIO$RUN$STATES';
BEGIN

  OPEN   cGetRunStates;
  FETCH  cGetRunStates INTO sRunStates;
  CLOSE  cGetRunStates;
  oErrorObj := StrTokenize(sRunStates, ',', lTokensArray);  

  FOR i IN 1..lTokensArray.count() LOOP
    pipe ROW (lTokensArray(i));
  END LOOP;

  RETURN;

END GetRunStates;

--==**********************************************************************************************************************************************==--
FUNCTION FmtParamVal(nVal in number, csParamNAME in varchar2, sRoundNotSigFig in Varchar2, nFmtParam IN integer) return varchar2 is
  nTmaxDecPl  integer;
begin

  if csParamNAME = 'Tmax' then
    nTmaxDecPl := STB$UTIL.GetReportParameterValue('DECIMAL_PLACES_TMAX',STB$OBJECT.getCurrentRepid);         
  end if;

  -- the other parameters or Tmax not configured
  RETURN 
    CASE 
      WHEN csParamNAME = 'Tmax' AND trim(nTmaxDecPl) IS NOT NULL THEN
        FormatRounded(round(nVal,nTmaxDecPl),nTmaxDecPl)
      WHEN sRoundNotSigFig = 'F' THEN 
        FormatSig(sigFigure(nVal,nFmtParam),nFmtParam)
      ELSE
        FormatRounded(round(nVal,nFmtParam),nFmtParam)
    END;

end FmtParamVal;

--***************************************************************************************************  
Function FormatRowValue(csEntityName in varchar2, csFormat in varchar2, dataRow in Rowid) return varchar2 is
  oErrorObj         STB$OERROR$RECORD := STB$OERROR$RECORD();
  sCurrentName       constant varchar2(100) := $$PLSQL_UNIT||'.FormatRowValue(' || csEntityName || ')'; 
  sCode             varchar2(500);
  sFormattedText    varchar2(2000);
  sReplaceText      varchar2(500);
  nReplaceVal       number;
  nCompVal          number; 

  lVarList          ISR$Text$List := ISR$Text$List();
  lDistVarList      ISR$Text$List := ISR$Text$List();
  sTextRest         varchar2(32000) := csFormat;
  sFoundVar         varchar2(500);
  nFoundPos         integer; 
  lVarPieces        stb$typedef.tlstrings;
  lEmptyPieces      stb$typedef.tlstrings;
  sPieceColumn      Varchar2(100);
  sPieceCompColumn  Varchar2(100);
  sPieceDigits      Varchar2(100);
  sPieceBefore      varchar2(100);
  sPieceAfter       varchar2(100);
  sSearchPattern    varchar2(50) := '\[[A-Za-z01-9/ :\.]+\]';
  sCondSearchPattern varchar2(50) := '({.+?})';
  sCondFoundVar     varchar2(500);
  sCondColList      varchar2(500);
  sCondFormatRest   varchar2(500);
  lCondCols         stb$typedef.tlstrings;
  sCondPiece        varchar2(500);
  sCondCode         varchar2(500);
  lCondSettings     stb$typedef.tlstrings;
  sCondExpr         varchar2(500);
  sCondPieceExpr    varchar2(500);
  nCondCount        pls_integer;

  sTempTabPrefix    varchar2(50) := STB$UTIL.GetSystemParameter('TEMP_TAB_PREFIX');
begin
  isr$trace.stat('begin',null,sCurrentName);

  execute immediate 'select code from '||sTempTabPrefix||csEntityName||' where rowid=:1' into sCode using in dataRow;
  if sCode is not null then         
    -- first evaluate conditions
    sCondFoundVar := trim(trailing '}' from trim(leading '{' from REGEXP_SUBSTR(sTextRest,sCondSearchPattern)));
    while sCondFoundVar is not null loop    
      isr$trace.debug('sCondFoundVar',sCondFoundVar, sCurrentName);
      sCondExpr := null;
      oErrorObj := strtokenize(sCondFoundVar,';',lCondSettings);
      sCondFormatRest := lCondSettings(lCondSettings.last);
      if lCondSettings.count = 2 then
        lCondSettings(lCondSettings.last) := 'is not null';
      else
        lCondSettings.delete(lCondSettings.last);
      end if;
      -- now only condition pairs exist
       -- sCondColList := stb$util.getLeftDelim(sCondFoundVar,';');
       -- sCondFormatRest := stb$util.getRightDelim(sCondFoundVar,';');
      nCondCount := lCondSettings.count;
      while nCondCount > 0 loop
        isr$trace.debug('sCondExpr',sCondExpr, sCurrentName);      
        sCondColList := lCondSettings(lCondSettings.first);
        sCondPieceExpr := lCondSettings(lCondSettings.next(lCondSettings.first));
        oErrorObj := strtokenize(sCondColList,',',lCondCols);
        sCondPiece := null;
        if lCondCols.count > 0 then
          for i in 1..lCondCols.count loop
            if sCondPiece is not null then
              sCondPiece := sCondPiece||' OR ';          
            end if;
            sCondPiece := sCondPiece||lCondCols(i)||' '||sCondPieceExpr;
          end loop;  
        end if;   
        lCondSettings.delete(lCondSettings.first);
        lCondSettings.delete(lCondSettings.first);        
        nCondCount := lCondSettings.count;
        if sCondPiece is not null then
          if sCondExpr is not null then
            sCondExpr := sCondExpr||' AND ';
          end if;
          sCondExpr := sCondExpr||' ('||sCondPiece||')';
        end if;        
      end loop;

      if sCondExpr is not null then
        isr$trace.debug('sCondExpr',sCondExpr, sCurrentName);
        begin
          execute immediate 'select code from '||sTempTabPrefix||csEntityName||' where rowid=:1 and ('||sCondExpr||')'
          into sCondCode using in dataRow;
        exception
          when NO_DATA_FOUND then 
            sCondCode := null;
        end;
        isr$trace.debug('sTextRest before repl',sTextRest, sCurrentName);
        if sCondCode is not null then 
          sTextRest := regexp_replace(sTextRest,sCondSearchPattern,sCondFormatRest,1,1);
        else
          sTextRest := regexp_replace(sTextRest,sCondSearchPattern,null,1,1);
        end if;
        isr$trace.debug('sTextRest after repl',sTextRest, sCurrentName);
        isr$trace.debug('sCondSearchPattern',sCondSearchPattern, sCurrentName);
        isr$trace.debug('sCondFormatRest',sCondFormatRest, sCurrentName);
      else
        -- this should not occur: no condition columns but { and } found!
        isr$trace.debug('sTextRest before replx',sTextRest, sCurrentName);
        sTextRest := regexp_replace(sTextRest,sCondSearchPattern,sCondFoundVar,1,1);
        isr$trace.debug('sTextRest after replx',sTextRest, sCurrentName);
      end if;
      sCondFoundVar := trim(trailing '}' from trim(leading '{' from REGEXP_SUBSTR(sTextRest,sCondSearchPattern)));
    end loop;
    isr$trace.debug('sTextRest after conditions',sTextRest, sCurrentName);

    -- then replace values
    sFormattedText := sTextRest;    
    sFoundVar := trim(trailing ']' from trim(leading '[' from REGEXP_SUBSTR(sTextRest,sSearchPattern)));
    nFoundPos := REGEXP_INSTR(sTextRest,'\[[A-Za-z01-9/ ]+\]');
    while nFoundPos > 0 loop
      lVarList.extend;
      lVarList(lVarList.last) := sFoundVar;
      sTextRest := substr(sTextRest,nFoundPos+2+length(sFoundVar));
      sFoundVar := trim(trailing ']' from trim(leading '[' from REGEXP_SUBSTR(sTextRest,sSearchPattern)));
      nFoundPos := REGEXP_INSTR(sTextRest,sSearchPattern);
    end loop;
    select distinct column_value 
    bulk collect into lDistVarList
    from table(lVarList);  

    if lDistVarList.count > 0 then        
      for i in 1..lDistVarList.count loop
        lVarPieces := lEmptyPieces;
        sReplaceText := null;
        oErrorObj := strtokenize(lDistVarList(i),'/',lVarPieces);
        if lVarPieces.count > 0 then
          sPieceColumn := lVarPieces(1);  
          sPieceDigits := null;
          sPieceCompColumn := null;            
          sPieceBefore := null;
          sPieceAfter := null;
          if lVarPieces.exists(2) then
            sPieceDigits := lVarPieces(2);
            if lVarPieces.exists(3) then
              sPieceCompColumn := lVarPieces(3);
              if lVarPieces.exists(4) then
                sPieceBefore := lVarPieces(4);
                if lVarPieces.exists(5) then
                  sPieceAfter := lVarPieces(5);                  
                end if;                
              end if;              
            end if;
          end if;            
         -- execute immediate 'select :1.'||sPieceColumn||' from dual' into nReplaceVal using in rSampleRow ;
          execute immediate 'select nvl('||sPieceColumn||',0) from '||sTempTabPrefix||csEntityName||' where rowid=:1' into nReplaceVal using in dataRow;
          if sPieceCompColumn is not null then
            execute immediate 'select nvl('||sPieceCompColumn||',0) from '||sTempTabPrefix||csEntityName||' where rowid=:1' into nCompVal using in dataRow;
            isr$trace.debug('sPieceCompColumn',sPieceCompColumn, sCurrentName);
            isr$trace.debug('nCompVal',nCompVal, sCurrentName);
            if nCompVal = nReplaceVal then 
              nReplaceVal := null;                
            end if;
          end if;  
          if nReplaceVal is not null then
            nReplaceVal := abs(nReplaceVal);
            nReplaceVal := SigFigure(nReplaceVal, 16);
            if sPieceDigits is null then
              sReplaceText := ISR$Format.FmtNum(nReplaceVal);
            else 
              sReplaceText := regexp_replace(to_char(nReplaceVal,'FM'||substr(lpad('0',sPieceDigits,'0'),1,sPieceDigits)||'D9999999999999999'), '\'||trim(to_char(1/10,'D'))||'$');
              if substr(sReplaceText,1,1) = '#' then
                sReplaceText := ISR$Format.FmtNum(nReplaceVal);
              end if;

              isr$trace.debug('sReplaceText',sReplaceText, sCurrentName);

            end if;
            sReplaceText := sPieceBefore||sReplaceText||sPieceAfter;
          end if;
          sFormattedText := replace(sFormattedText, '['||lDistVarList(i)||']', sReplaceText);
          isr$trace.debug('sFormattedText',sFormattedText, sCurrentName);
        end if;
      end loop;
    end if;
  end if;
  isr$trace.stat('end','sFormattedText:'||sFormattedText,sCurrentName);
  return sFormattedText;
end FormatRowValue;
--***************************************************************************************************  
FUNCTION getConvFactor (sSampleConcUnit IN VARCHAR2, sRepConcUnit IN VARCHAR2) return number
IS
--  oErrorObj STB$OERROR$RECORD := STB$OERROR$RECORD();
--  clXml     CLOB;
BEGIN
--
--  SELECT XMLELEMENT ("PARAMETERS"
--                   , XMLELEMENT ("PARAMETER1", sSampleConcUnit)
--                   , XMLELEMENT ("PARAMETER2", sRepConcUnit))
--    INTO xXml
--    FROM DUAL;    
--  
--  clXml := ISR$REMOTE$COLLECTOR.getRemoteValues('CONVERSIONFACTOR', xXml.getClobVal());    
--  
--  if clxml is null then
--    raise no_data_found;
--  end if;
--  
--  xXml := XMLTYPE(clXml);
--  
--  SELECT EXTRACTVALUE(xXml, '/PARAMETERS/PARAMETER/PARAMETER1')
--    INTO sReturn
--    FROM dual;         
--
--  return TO_NUMBER(sReturn);
return 1;
--  
--EXCEPTION
--  WHEN no_data_found THEN
--    RETURN 1; -- no conversion
--  WHEN OTHERS THEN
--    oErrorObj.sErrorCode :=Utd$msglib.GetMsg('ERROR',Stb$security.GetCurrentLanguage) || SQLCODE || ' ' || SQLERRM;
--    oErrorObj.sErrorMessage :=Utd$msglib.GetMsg('ERROR_PLACE',Stb$security.GetCurrentLanguage) ||' ISR$COVIB$BIOANALYTICS$WT$LAL.getConvFactor';
--    oErrorObj.sSeverityCode := Stb$typedef.cnSeverityCritical;
--    isr$trace.debug(oErrorObj);
--    raise;  
END getConvFactor;  

--***************************************************************************************************  
--FUNCTION getAutoSampTime (sRunCode in VARCHAR2, nStudycode IN NUMBER) return number
--IS
--  oErrorObj STB$OERROR$RECORD := STB$OBJECT.initialiseOerrorRec;
--BEGIN
--
--  SELECT XMLELEMENT ("PARAMETERS"
--                   , XMLELEMENT ("PARAMETER1", sRunCode)
--                   , XMLELEMENT ("PARAMETER2", nStudycode))
--    INTO xXml
--    FROM DUAL;
--    
--  xXml := XMLTYPE(ISR$REMOTE$COLLECTOR.getRemoteValues('AUTOSAMPTIME', xXml.getClobVal()));
--  
--  SELECT EXTRACTVALUE(xXml, '/PARAMETERS/PARAMETER/PARAMETER1')
--    INTO sReturn
--    FROM dual; 
--
--  return sReturn;
--  
--EXCEPTION
--  WHEN no_data_found THEN
--    RETURN null; -- no samptime
--  WHEN OTHERS THEN
--    oErrorObj.sErrorCode :=Utd$msglib.GetMsg('ERROR',Stb$security.GetCurrentLanguage) || SQLCODE || ' ' || SQLERRM;
--    oErrorObj.sErrorMessage :=Utd$msglib.GetMsg('ERROR_PLACE',Stb$security.GetCurrentLanguage) ||' ISR$COVIB$BIOANALYTICS$WT$LAL.getAutoSampTime';
--    oErrorObj.sSeverityCode := Stb$typedef.cnSeverityCritical;
--    isr$trace.debug(oErrorObj);
--    raise;    
--END getAutoSampTime;

--***************************************************************************************************                      
FUNCTION StudyUsesGender(cnStudy IN number) RETURN varchar2 
IS
BEGIN

  select usesGender into sReturn
  from tmp$BIO$study
  where code = cnStudy;

--  SELECT XMLELEMENT ("PARAMETERS"
--                   , XMLELEMENT ("PARAMETER1", cnStudy))
--    INTO xXml
--    FROM DUAL;
--    
--  xXml := XMLTYPE(ISR$REMOTE$COLLECTOR.getRemoteValues('STUDYUSESGENDER', xXml.getClobVal()));
--  
--  SELECT EXTRACTVALUE(xXml, '/PARAMETERS/PARAMETER/PARAMETER1')
--    INTO sReturn
--    FROM dual; 

  RETURN 
    CASE 
      WHEN sReturn IS NOT NULL THEN 
        'T' 
      ELSE 
        'F' 
    END;

END StudyUsesGender;

--*******************************************************************************************************************************
--FUNCTION getLov(csParameter IN VARCHAR2,
--                sFilter IN VARCHAR2,
--                oSelectionList OUT ISR$TLRSELECTION$LIST)  RETURN STB$OERROR$RECORD
--IS
--  oErrorObj                   STB$OERROR$RECORD := STB$OBJECT.initialiseOerrorRec;
--  exNotExists                 EXCEPTION;
--  nCounter                    NUMBER := 0;
--  sExists                     VARCHAR2(1);
--  
--  sOrgConcUnit                VARCHAR2(500);
--  nAnalytesInStudy            INTEGER;
--                
--  -- get grouping list
--  CURSOR cGetGrouping is 
--    SELECT *
--      FROM ISR$PARAMETER$VALUES
--     WHERE entity = 'WT$TOX$GROUP_BY'
--    ORDER BY ordernumber;         
--BEGIN
--  isr$trace.debug('begin', 'ISR$COVIB$BIOANALYTICS$WT$LAL.getLov', 'csParameter:'||csParameter||', sFilter:'||sFilter, 50);
--
--  oSelectionList :=  ISR$TLRSELECTION$LIST();
--
--  CASE
--        
    -- list of all limsusers
--    WHEN upper (csParameter) = 'WATSONLIMSUSERNAME' THEN
--
--      xXml := XMLTYPE(ISR$REMOTE$COLLECTOR.getRemoteValues('WATSONLIMSUSERNAME', null));
--      
--      SELECT ISR$OSELECTION$RECORD (sValue
--                                  , sDisplay
--                                  , sInfo
--                                  , sSelected
--                                  , nOrderNum
--                                  , sMasterKey
--                                  , tloAttributeList)
--        BULK COLLECT
--        INTO oSelectionList
--        FROM (SELECT EXTRACTVALUE (COLUMN_VALUE, '/PARAMETER/PARAMETER1') sValue
--                   , EXTRACTVALUE (COLUMN_VALUE, '/PARAMETER/PARAMETER2') sDisplay
--                   , EXTRACTVALUE (COLUMN_VALUE, '/PARAMETER/PARAMETER3') sInfo
--                   , NULL sSelected
--                   , ROWNUM nOrderNum
--                   , NULL sMasterKey
--                   , ISR$ATTRIBUTE$LIST () tloAttributeList
--                FROM TABLE (XMLSEQUENCE (EXTRACT (xXml, '/PARAMETERS/PARAMETER'))));

--    -- list of all templates
--    WHEN upper (csParameter) = 'PK_TEMPLATE' THEN
--
--      xXml := XMLTYPE(ISR$REMOTE$COLLECTOR.getRemoteValues('PK_TEMPLATE', null));
--      
--      SELECT ISR$OSELECTION$RECORD (sValue
--                                  , sDisplay
--                                  , sInfo
--                                  , sSelected
--                                  , nOrderNum
--                                  , sMasterKey
--                                  , tloAttributeList)
--        BULK COLLECT
--        INTO oSelectionList
--        FROM (SELECT EXTRACTVALUE (COLUMN_VALUE, '/PARAMETER/PARAMETER1') sValue
--                   , EXTRACTVALUE (COLUMN_VALUE, '/PARAMETER/PARAMETER2') sDisplay
--                   , EXTRACTVALUE (COLUMN_VALUE, '/PARAMETER/PARAMETER3') sInfo
--                   , NULL sSelected
--                   , ROWNUM nOrderNum
--                   , NULL sMasterKey
--                   , ISR$ATTRIBUTE$LIST () tloAttributeList
--                FROM TABLE (XMLSEQUENCE (EXTRACT (xXml, '/PARAMETERS/PARAMETER'))));    
--                
--      oSelectionList.EXTEND;
--      oSelectionList(oSelectionList.COUNT()) :=  STB$OBJECT.initialiseOselectionRec;
--      oSelectionList(oSelectionList.COUNT()).sValue := STB$Util.GetSystemParameter('PK_TEMPLATE_NO_PK');
--      oSelectionList(oSelectionList.COUNT()).sDisplay := Utd$msglib.GetMsg(STB$Util.GetSystemParameter('PK_TEMPLATE_NO_PK'),Stb$security.GetCurrentLanguage); 
--      oSelectionList(oSelectionList.COUNT()).sInfo := Utd$msglib.GetMsg(STB$Util.GetSystemParameter('PK_TEMPLATE_NO_PK'),Stb$security.GetCurrentLanguage);
--      oSelectionList(oSelectionList.COUNT()).nOrderNum := oSelectionList.COUNT();                             

    -- list of all possible concentrations
--    WHEN upper (csParameter) in ('CONCENTRATION_UNIT','CONCENTRATION_UNIT_UNKNOWNS') THEN
--    
--      BEGIN
--        SELECT XMLELEMENT ("PARAMETERS"
--                         , XMLELEMENT ("PARAMETER1", (select key from isr$crit where entity = 'WT$STUDY' and repid = STB$OBJECT.getCurrentRepid)))
--          INTO xXml
--          FROM DUAL;
--          
--        xXml := XMLTYPE(ISR$REMOTE$COLLECTOR.getRemoteValues('ORGCONCUNIT', xXml.getClobVal()));
--        
--        SELECT EXTRACTVALUE(xXml, '/PARAMETERS/PARAMETER/PARAMETER1')
--             , TO_NUMBER(EXTRACTVALUE(xXml, '/PARAMETERS/PARAMETER/PARAMETER2'))
--          INTO sOrgConcUnit, nAnalytesInStudy
--          FROM dual;
--         
--        isr$trace.debug('sOrgConcUnit, nAnalytesInStudy', 'ISR$COVIB$BIOANALYTICS$WT$LAL.getLov', 'sOrgConcUnit:'||sOrgConcUnit||', nAnalytesInStudy:'||nAnalytesInStudy, 55);           
--          
--        SELECT XMLELEMENT ("PARAMETERS"
--                         , XMLELEMENT ("PARAMETER1", sOrgConcUnit))
--          INTO xXml
--          FROM DUAL;
--
--        xXml := XMLTYPE(ISR$REMOTE$COLLECTOR.getRemoteValues('ORGCONCENTRATION', xXml.getClobVal()));
--      
--        SELECT ISR$OSELECTION$RECORD (sValue
--                                    , sDisplay
--                                    , sInfo
--                                    , sSelected
--                                    , nOrderNum
--                                    , sMasterKey
--                                    , tloAttributeList)
--          BULK COLLECT
--          INTO oSelectionList
--          FROM (SELECT EXTRACTVALUE (COLUMN_VALUE, '/PARAMETER/PARAMETER1') sValue
--                     , EXTRACTVALUE (COLUMN_VALUE, '/PARAMETER/PARAMETER2') sDisplay
--                     , EXTRACTVALUE (COLUMN_VALUE, '/PARAMETER/PARAMETER3') sInfo
--                     , NULL sSelected
--                     , ROWNUM nOrderNum
--                     , NULL sMasterKey
--                     , ISR$ATTRIBUTE$LIST () tloAttributeList
--                  FROM TABLE (XMLSEQUENCE (EXTRACT (xXml, '/PARAMETERS/PARAMETER')))); 
--      EXCEPTION WHEN OTHERS THEN
--        NULL;
--        isr$trace.debug('ORGCONCENTRATION','ISR$COVIB$BIOANALYTICS$WT$LAL.getLov',substr(SQLCODE || ' ' || SQLERRM,1,4000),55); 
--      END;
--      isr$trace.debug('ORGCONCENTRATION','ISR$COVIB$BIOANALYTICS$WT$LAL.getLov','cnt '||oSelectionList.COUNT(),55);
--
--      -- add 'unit of analyte' only if study has more than one analyte or if no unit is found
--      IF nAnalytesInStudy > 1 OR oSelectionList.COUNT() = 0 THEN
--        oSelectionList.EXTEND;
--        oSelectionList(oSelectionList.COUNT()) :=  STB$OBJECT.initialiseOselectionRec;
--        oSelectionList(oSelectionList.COUNT()).sValue := STB$UTIL.GetSystemParameter('UNIT_NO_CHANGE');
--        oSelectionList(oSelectionList.COUNT()).sDisplay := Utd$msglib.GetMsg(STB$UTIL.GetSystemParameter('UNIT_NO_CHANGE'),Stb$security.GetCurrentLanguage); 
--        oSelectionList(oSelectionList.COUNT()).sInfo := Utd$msglib.GetMsg(STB$UTIL.GetSystemParameter('UNIT_NO_CHANGE'),Stb$security.GetCurrentLanguage);
--        oSelectionList(oSelectionList.COUNT()).nOrderNum := oSelectionList.COUNT();        
--      END IF;

    -- list of all additional runtypes
--    WHEN upper (csParameter) = 'ADDITIONAL_RUNTYPE' THEN
--
--      SELECT XMLELEMENT ("PARAMETERS"
--                       , XMLELEMENT ("PARAMETER1", (select key from isr$crit where entity = 'WT$STUDY' and repid = STB$OBJECT.getCurrentRepid))
--                       , XMLELEMENT ("PARAMETER2", stb$util.getReportParameterValue('RUNTYPE', STB$OBJECT.getCurrentRepid)))
--        INTO xXml
--        FROM DUAL;         
--
--      xXml := XMLTYPE(ISR$REMOTE$COLLECTOR.getRemoteValues('RUNTYPES', xXml.getClobVal()));
--      
--      SELECT ISR$OSELECTION$RECORD (sValue
--                                  , sDisplay
--                                  , sInfo
--                                  , sSelected
--                                  , nOrderNum
--                                  , sMasterKey
--                                  , tloAttributeList)
--        BULK COLLECT
--        INTO oSelectionList
--        FROM (SELECT EXTRACTVALUE (COLUMN_VALUE, '/PARAMETER/PARAMETER1') sValue
--                   , EXTRACTVALUE (COLUMN_VALUE, '/PARAMETER/PARAMETER2') sDisplay
--                   , EXTRACTVALUE (COLUMN_VALUE, '/PARAMETER/PARAMETER3') sInfo
--                   , NULL sSelected
--                   , ROWNUM nOrderNum
--                   , NULL sMasterKey
--                   , ISR$ATTRIBUTE$LIST () tloAttributeList
--                FROM TABLE (XMLSEQUENCE (EXTRACT (xXml, '/PARAMETERS/PARAMETER'))));    
--
--     
    -- list of grouping
--    WHEN upper (csParameter) = 'MATRIX_GROUP_BY' THEN
--    
--      SELECT XMLELEMENT ("PARAMETERS"
--                       , XMLELEMENT ("PARAMETER1", (select key from isr$crit where entity = 'WT$STUDY' and repid = STB$OBJECT.getCurrentRepid)))
--        INTO xXml
--        FROM DUAL;
--        
--      xXml := XMLTYPE(ISR$REMOTE$COLLECTOR.getRemoteValues('CHECKPERIODEXISTS', xXml.getClobVal()));
--      
--      SELECT EXTRACTVALUE(xXml, '/PARAMETERS/PARAMETER/PARAMETER1')
--        INTO sExists
--        FROM dual; 
--    
--      if sExists = 'T' then    
--        nCounter := 0;
--        FOR rGetGrouping IN cGetGrouping LOOP
--         nCounter := nCounter+1;
--         oSelectionList.EXTEND;
--         oSelectionList(nCounter) :=  STB$OBJECT.initialiseOselectionRec;
--         oSelectionList(nCounter).sValue := rGetGrouping.value;
--         oSelectionList(nCounter).sDisplay := Utd$msglib.GetMsg(rGetGrouping.display,Stb$security.GetCurrentLanguage);
--         oSelectionList(nCounter).sInfo := rGetGrouping.info;
--         oSelectionList(nCounter).nOrderNum := nCounter;
--         isr$trace.debug('grouping','ISR$COVIB$BIOANALYTICS$WT$LAL.getLov','rGetGrouping.value:'||rGetGrouping.value||' rGetGrouping.display:'||rGetGrouping.display,170);
--        END LOOP; 
--      else
--        nCounter := nCounter+1;
--        oSelectionList.EXTEND;
--        oSelectionList(nCounter) :=  STB$OBJECT.initialiseOselectionRec;
--        oSelectionList(nCounter).sValue := 'TREATMENT';
--        oSelectionList(nCounter).sDisplay := Utd$msglib.GetMsg('Treatment',Stb$security.GetCurrentLanguage);
--        oSelectionList(nCounter).sInfo := Utd$msglib.GetMsg('Treatment',Stb$security.GetCurrentLanguage);
--        oSelectionList(nCounter).nOrderNum := nCounter;
--        isr$trace.debug('grouping','ISR$COVIB$BIOANALYTICS$WT$LAL.getLov','Treatment',50);     
--      end if;
--      
--    ELSE
--      RAISE exNotExists;    
--  END CASE;
--
--  isr$trace.debug('end','ISR$COVIB$BIOANALYTICS$WT$LAL.getLov','cnt '||oSelectionList.COUNT(),50);
--
--  RETURN oErrorObj;
--
--EXCEPTION
--  WHEN exNotExists THEN
--    oErrorObj.sErrorCode      := 'No Lov';
--    oErrorObj.sErrorMessage   := 'No Lov defined for : ' || csParameter || ' within version ' || STB$UTIL.getSystemParameter('ISR_VERSION');
--    oErrorObj.sSeverityCode   := Stb$typedef.cnSeverityMessage;
--    isr$trace.debug(oErrorObj);
--    RETURN oErrorObj;
--  WHEN OTHERS THEN
--    oErrorObj.sErrorCode :=Utd$msglib.GetMsg('ERROR',Stb$security.GetCurrentLanguage) || SQLCODE || ' ' || SQLERRM;
--    oErrorObj.sErrorMessage :=Utd$msglib.GetMsg('ERROR_PLACE',Stb$security.GetCurrentLanguage) ||' ISR$COVIB$BIOANALYTICS$WT$LAL.getLov';
--    oErrorObj.sSeverityCode := Stb$typedef.cnSeverityCritical;
--    isr$trace.debug(oErrorObj);
--    RETURN oErrorObj;
--END getLov;

--***************************************************************************************************                      
FUNCTION getGridMaster(cnMaskNo IN NUMBER, sMasterEntity OUT STB$TYPEDEF.tLine) RETURN STB$OERROR$RECORD
IS
  oErrorObj                   STB$OERROR$RECORD := STB$OERROR$RECORD();
  sCurrentName       constant varchar2(100) := $$PLSQL_UNIT||'.getGridMaster('||cnMaskNo||')';  

  CURSOR cGetGraphicMaster IS
    SELECT KEY
      FROM ISR$Crit
     WHERE RepId = STB$OBJECT.getCurrentRepId
       AND entity like 'BIO$GRAPHIC%';

BEGIN
  isr$trace.stat('begin', 'begin', sCurrentName);

  OPEN cGetGraphicMaster;
  FETCH cGetGraphicMaster INTO sMasterEntity;
  CLOSE cGetGraphicMaster;

  isr$trace.stat('end',sMasterEntity,sCurrentName);
 RETURN oErrorObj;
EXCEPTION
  WHEN OTHERS THEN
    oErrorObj.handleError( Stb$typedef.cnSeverityCritical, utd$msglib.getmsg ('exUnexpectedError', stb$security.getCurrentLanguage,  csP1 => sqlerrm(sqlcode)),sCurrentName,
            dbms_utility.format_error_stack ||stb$typedef.crlf|| dbms_utility.format_error_backtrace ); 
    RETURN oErrorObj;
END getGridMaster;

--***************************************************************************************************
--FUNCTION GetGridMaskAdds(sEntity IN STB$TYPEDEF.tLine, csMasterKey IN varchar2, lAdds OUT ISR$Crit$List) RETURN STB$OERROR$RECORD IS
--  oErrorObj         STB$OERROR$RECORD := STB$OERROR$RECORD();
--BEGIN
--  isr$trace.debug('begin','ISR$COVIB$BIOANALYTICS$WT$LAL.GetGridMaskAdds','sEntity:'||sEntity||' csMasterKey:'||csMasterKey,155);
--    
--  lAdds := ISR$Crit$List();
--  
--  IF sEntity in  ('WT$SUBJGRPDAY','WT$SUBJECT') THEN
--
--    SELECT XMLELEMENT ("PARAMETERS"
--                     , XMLELEMENT ("PARAMETER1", (select key from isr$crit where entity = 'WT$STUDY' and repid = STB$OBJECT.getCurrentRepid)))
--      INTO xXml
--      FROM DUAL;
--          
--    xXml := XMLTYPE(ISR$REMOTE$COLLECTOR.getRemoteValues('STUDYTREATMENT', xXml.getClobVal()));
--              
--    FOR rGetGroupAdd IN ( SELECT EXTRACTVALUE (COLUMN_VALUE, '/PARAMETER/PARAMETER1') value
--                               , EXTRACTVALUE (COLUMN_VALUE, '/PARAMETER/PARAMETER2') display
--                               , EXTRACTVALUE (COLUMN_VALUE, '/PARAMETER/PARAMETER2') info
--                            FROM TABLE (XMLSEQUENCE (EXTRACT (xXml, '/PARAMETERS/PARAMETER')))) LOOP
--      lAdds.EXTEND;
--      lAdds(lAdds.last) := ISR$Crit$Rec(rGetGroupAdd.value
--                                      , rGetGroupAdd.value||CASE WHEN csMasterKey IS NULL THEN NULL ELSE '#@#'||csMasterKey END
--                                      , rGetGroupAdd.display
--                                      , rGetGroupAdd.info
--                                      , NULL);  
--    END LOOP;
--  END IF;
--  
--  isr$trace.debug('end','ISR$COVIB$BIOANALYTICS$WT$LAL.GetGridMaskAdds','entries found:'||lAdds.count,155);
--  RETURN oErrorObj;
--EXCEPTION
--  WHEN OTHERS THEN
--    oErrorObj.sErrorCode :=Utd$msglib.GetMsg('ERROR',Stb$security.GetCurrentLanguage) || SQLCODE || ' ' || SQLERRM;
--    oErrorObj.sErrorMessage :=Utd$msglib.GetMsg('ERROR_PLACE',Stb$security.GetCurrentLanguage) ||' ISR$COVIB$BIOANALYTICS$WT$LAL. GetGridMaskAdds';
--    oErrorObj.sSeverityCode := Stb$typedef.cnSeverityCritical;
--    isr$trace.debug(oErrorObj);
--    RETURN oErrorObj;
--END GetGridMaskAdds;

--==**********************************************************************************************************************************************==--
FUNCTION prepareForXML(csData IN VARCHAR2, csType IN VARCHAR2) RETURN VARCHAR2
IS
  sReturn                 VARCHAR2(4000);
  sCurrentName       constant varchar2(200) := $$PLSQL_UNIT||'.prepareForXML('||csData||')';  
  sConcRoundNotSigFig     VARCHAR2(1);
  sConcFigures            VARCHAR2(500);  

  sRegparamRoundNotSigFig VARCHAR2(1);
  sRegparamFigures        VARCHAR2(500);

  sRsquRoundNotSigFig     VARCHAR2(1);
  sRsquaredFigures        VARCHAR2(500);
  
  sDiffDecimals           VARCHAR2(500);

BEGIN
  isr$trace.stat('begin', csData, sCurrentName);

  sReturn := csData;  

  IF csType = 'NUMBER' THEN
    sConcRoundNotSigFig := STB$UTIL.GetReportParameterValue('CONC_ROUND_NOT_SIG_FIG',STB$OBJECT.getCurrentRepid);
    sConcFigures :=  STB$UTIL.GetReportParameterValue('CONC_FIGURES', STB$OBJECT.getCurrentRepid);
    sDiffDecimals :=  STB$UTIL.GetReportParameterValue('DECIMAL_PLACES_DIFFERENCE', STB$OBJECT.getCurrentRepid);
    IF trim(sConcFigures) IS NOT NULL THEN
      IF nvl(STB$UTIL.GetRightDelim(csData,'.'),csData) IN ( 'KNOWNCONCENTRATION', 'KNOWNCONCENTRATIONCONV', 
                                                            'CONCENTRATION', 'CONCENTRATIONCONV', 
                                                            'NOMINALCONCENTRATION', 'NOMINALCONCENTRATIONCONV',
                                                            'REASSAYCONCENTRATION', 'REASSAYCONCENTRATIONCONV',
                                                            'SAMPLECONCENTRATION', 'SAMPLECONCENTRATIONCONV',
                                                            'LLOQ', 'LLOQCONV', 'ULOQ', 'ULOQCONV', 'NMCONV', 'VECCONV', 
                                                            'FIRSTLLOQ', 'FIRSTLLOQCONV', 'FIRSTULOQ', 'FIRSTULOQCONV',
                                                            'LLOQUNKCONV', 'ULOQUNKCONV', 'FIRSTLLOQUNKCONV', 'FIRSTULOQUNKCONV',
                                                            'VECUNKCONV', 'NMUNKCONV')
      OR csData in ('BIO$REASSAY$HISTORY.CALIBRATIONRANGECONV', 'BIO$SAMPLE$RESULTS.CALIBRATIONRANGECONV',
                    'BIO$REASSAY$HISTORY.NMCONVSAMPLE', 'BIO$REASSAY$HISTORY.VECCONVSAMPLE',
                    'BIO$REASSAY$HISTORY.NMCONVREASSAY', 'BIO$REASSAY$HISTORY.VECCONVREASSAY') THEN
        if sConcRoundNotSigFig = 'F' then
          sReturn := 'FormatSig(sigFigure('||csData||','||sConcFigures||'),'||sConcFigures||')';
        else
          sReturn := 'FormatRounded(ROUND('||csData||','||sConcFigures||'),'||sConcFigures||')';
        end if;
      ELSIF csData in ('BIO$REASSAY$HISTORY.DIFFERENCEPERCENT') THEN
        sReturn := 'FormatRounded(ROUND('||csData||','||sDiffDecimals||'),'||sDiffDecimals||')';
      ELSIF csData in ('BIO$REASSAY$HISTORY.DIFFERENCEPERCENTTOPREV') THEN
        sReturn := 'FormatRounded(ROUND('||csData||','||sDiffDecimals||'),'||sDiffDecimals||')';
      ELSIF (csData = 'PARAMATERVALUE' OR csData LIKE '%.PARAMETERVALUE') THEN
        sReturn := STB$UTIL.getCurrentLal||'.FmtParamVal('||csData||', NAME, '''||sConcRoundNotSigFig||''', '||sConcFigures||')';
      END IF;    
    END IF;  

    IF (csData LIKE 'REGPARAMVAL%' OR csData LIKE '%.REGPARAMVAL%') THEN
      sRegparamRoundNotSigFig := STB$UTIL.GetReportParameterValue('REGPARAM_ROUND_NOT_SIG_FIG',STB$OBJECT.getCurrentRepid);
      sRegparamFigures := STB$UTIL.GetReportParameterValue('REGPARAM_FIGURES',STB$OBJECT.getCurrentRepid);
      if sRegparamFigures is not null then
        if sRegparamRoundNotSigFig = 'T' then          
          sReturn := 'FormatRounded(ROUND('||csData||','||sRegparamFigures||'),'||sRegparamFigures||')';
        else
          sReturn :=  'FormatSig(sigFigure('||csData||','||sRegparamFigures||'),'||sRegparamFigures||')';          
        end if;    
      end if;         
    end if;    

    IF (csData = 'RSQUARED' OR csData LIKE '%.RSQUARED')
      OR (csData = 'CONFIDENCE95' OR csData LIKE '%.CONFIDENCE95') THEN
      sRsquRoundNotSigFig := STB$UTIL.GetReportParameterValue('RSQUARED_ROUND_NOT_SIG_FIG',STB$OBJECT.getCurrentRepid);
      sRsquaredFigures := STB$UTIL.GetReportParameterValue('RSQUARED_FIGURES',STB$OBJECT.getCurrentRepid);
      if sRsquaredFigures is not null then
        if sRsquRoundNotSigFig = 'T' then
          sReturn := 'FormatRounded(ROUND('||csData||','||sRsquaredFigures||'),'||sRsquaredFigures||')';
        else
          sReturn := 'FormatSig(sigFigure('||csData||','||sRsquaredFigures||'),'||sRsquaredFigures||')';
        END IF;
      end if;
    END IF;
    
  ELSIF csType = 'DATE' THEN
    sReturn := 'to_char('||csData||','''||NVL(STB$UTIL.getRepTypeParameter(STB$OBJECT.getCurrentReporttypeId, 'DATEFORMAT'),STB$UTIL.GetSystemParameter('DATEFORMAT'))||''')';
  END IF;

  IF nvl(STB$UTIL.GetRightDelim(csData,'.'),csData) IN ('ENDHOUR','DOSEAMOUNT','BTTIME') THEN    
    sReturn := 'FmtNum('||csData||')';
  END IF;    

  isr$trace.stat('end', sReturn, sCurrentName);
  RETURN sReturn;
END prepareForXML;

--***************************************************************************************************
FUNCTION preparePostXML(cnJobId IN NUMBER, oParamList IN ISR$ATTRLIST$REC, cxXML IN OUT XMLTYPE) RETURN STB$OERROR$RECORD IS
  oErrorObj           STB$OERROR$RECORD := STB$OERROR$RECORD();
  sCurrentName       constant varchar2(100) := $$PLSQL_UNIT||'.preparePostXML('||cnJobId||')';  
  nSignificantFigures NUMBER;
BEGIN
  isr$trace.stat('begin','begin',sCurrentName);

  IF STB$UTIL.getRepTypeParameter(STB$OBJECT.getCurrentReporttypeId, 'REP_TYPE_CALCULATION_PACKAGE') IS NOT NULL THEN

    nSignificantFigures := STB$UTIL.getReportParameterValue('SIGNIFICANT_FIGURES', STB$OBJECT.getCurrentRepid);

    EXECUTE immediate
      'BEGIN
         :1 :='|| STB$UTIL.getRepTypeParameter(STB$OBJECT.getCurrentReporttypeId, 'REP_TYPE_CALCULATION_PACKAGE') ||'.AddStudyCalculations(:2, :3, :4);
       END;'
    using OUT oErrorObj, IN cnJobId, IN OUT cxXML, IN oParamList;

  END IF;

  isr$trace.stat('end','end',sCurrentName);
  RETURN oErrorObj;
EXCEPTION
  WHEN OTHERS THEN
    oErrorObj.handleError( Stb$typedef.cnSeverityCritical, utd$msglib.getmsg ('exUnexpectedError', stb$security.getCurrentLanguage,  csP1 => sqlerrm(sqlcode)),sCurrentName,
            dbms_utility.format_error_stack ||stb$typedef.crlf|| dbms_utility.format_error_backtrace ); 
    RETURN oErrorObj;
END preparePostXML;

--***************************************************************************************************
FUNCTION getDefaultValues (oSelection in OUT ISR$TLRSELECTION$LIST
                          ,csDefaultToken in VARCHAR2
                          ,csEntity in VARCHAR2) RETURN STB$OERROR$RECORD IS
  oErrorObj         STB$OERROR$RECORD := STB$OERROR$RECORD();
  sCurrentName       constant varchar2(200) := $$PLSQL_UNIT||'.getDefaultValues('||csDefaultToken||')'; 
  oSelection1       ISR$TLRSELECTION$LIST := ISR$TLRSELECTION$LIST();
  oSelection2       ISR$TLRSELECTION$LIST := ISR$TLRSELECTION$LIST();
  oParamList        iSR$ParamList$Rec := iSR$ParamList$Rec(); --parameter list
  -- create sparsed collection
  type toSNonRef is table of ISR$OSELECTION$RECORD index by binary_integer;
  oSelectionNonRef  toSNonRef;
  bNonRef           boolean := false;
  nIxNRef           binary_integer;
  lWBNRs            sys.ODCINumberList := sys.ODCINumberList();
  sDefaultParam     varchar2(500);
  
  sStudyCode        varchar2(10);   

BEGIN
  isr$trace.stat('begin','csEntity:'||csEntity||' csDefaultToken:'||csDefaultToken,sCurrentName);
  -- fill Param List with values
  oParamList.AddParam('REPORTTYPEID', STB$OBJECT.getCurrentReporttypeId);
  oParamList.AddParam('REPID', STB$OBJECT.getCurrentRepid);  

  CASE
    WHEN csEntity = 'BIO$KNOWN1_INVISIBLE' THEN

      FOR i IN 1..oSelection.COUNT() LOOP
        IF oSelection(i).sInfo = 'STANDARD' THEN
          oSelection(i).sSelected := 'T';
        ELSE
          oSelection(i).sSelected := 'F';
        END IF;
        isr$trace.debug(csEntity,oSelection(i).sInfo||' '||oSelection(i).sSelected,sCurrentName);
      END LOOP;

    /*WHEN csEntity = 'BIO$KNOWN1' THEN

      FOR i IN 1..oSelection.COUNT() LOOP
        oSelection(i).sSelected := 'F';
        isr$trace.debug(csEntity,'ISR$BIOANALYTICS$BIO$LAL.getDefaultValues',oSelection(i).sInfo||' '||oSelection(i).sSelected,155);
        IF oSelection(i).tloAttributeList.COUNT() > 0 THEN
          FOR j IN 1 .. oSelection(i).tloAttributeList.COUNT() LOOP
            isr$trace.debug(oSelection(i).tloAttributeList(j).sPrePresent,'ISR$BIOANALYTICS$BIO$LAL.getDefaultValues',oSelection(i).tloAttributeList(j).sParameter||' '||oSelection(i).tloAttributeList(j).sValue||' '||STB$UTIL.getLeftDelim(oSelection(i).sMasterKey, '#@#'),155);
            IF oSelection(i).tloAttributeList(j).sPrePresent = 'V'
            AND oSelection(i).tloAttributeList(j).sParameter = 'Dil.factor' THEN
              IF NVL(oSelection(i).tloAttributeList(j).sValue, 1) > 1
              AND STB$UTIL.getLeftDelim(oSelection(i).sMasterKey, '#@#') like '%DIL%' THEN
                oSelection(i).sSelected := 'T';
              ELSIF NVL(oSelection(i).tloAttributeList(j).sValue, 1) <= 1
              AND STB$UTIL.getLeftDelim(oSelection(i).sMasterKey, '#@#') not like '%DIL%' THEN
                oSelection(i).sSelected := 'T';
              END IF;
            END IF;
          END LOOP;
        END IF;
      END LOOP;*/

    WHEN csEntity = 'BIO$KNOWN' THEN
      FOR i IN 1..oSelection.COUNT() LOOP
        isr$trace.debug(csEntity,oSelection(i).sDisplay||' '||oSelection(i).sValue||' '||oSelection(i).sInfo||' '||oSelection(i).sSelected||' '||oSelection(i).nOrderNum||' '||oSelection(i).sMasterKey,sCurrentName);
        --
        if oSelection(i).sMasterKey = 'S_WHOLEBLOOD_NON_REFR' then 
          if oSelection(i).sSelected = 'T' then            
            lWBNRs.extend;
            lWBNRs(lWBNRs.count):= i;
          end if;
          bNonRef := true;            
          oSelectionNonRef(i) := oSelection(i);
        else
          if bNonRef then
            -- past non refr. Now do it!
            for k in 1..lWBNRs.count loop
              nIxNRef := oSelectionNonRef.first;
              while nIxNRef is not null loop
                if nIxNRef != k then
                  isr$trace.debug(csEntity,'k:'||k||' nIxNRef:'||nIxNRef||' oSelection(nIxNRef).sDisplay:'||oSelection(nIxNRef).sDisplay||' oSelection(lWBNRs(k)).sDisplay:'||oSelection(lWBNRs(k)).sDisplay,sCurrentName);
                  if replace(oSelection(nIxNRef).sDisplay, 'WB', 'WBNR') = oSelection(lWBNRs(k)).sDisplay then
                    -- found one! Make it selected
                    oSelection(nIxNRef).sSelected := 'T';
                  end if;
                end if;

                nIxNRef := oSelectionNonRef.next(nIxNRef);
              end loop;
            end loop;
            bNonRef := false;
          end if;  


        end if;
      END LOOP;

    WHEN csEntity IN ('BIO$KNOWNMATCHING') THEN

      oSelection1 := ISR$REMOTE$COLLECTOR.getList ('BIO$KNOWNMATCHING', oParamList);
      SELECT ISR$OSELECTION$RECORD (sDisplay
                                  , sValue
                                  , sInfo
                                  , sSelected
                                  , nOrderNum
                                  , sMasterKey
                                  , tloAttributeList)
        BULK COLLECT
        INTO oSelection2
        FROM (SELECT e.sDisplay
                   , e.sValue
                   , NULL sInfo
                   , 'T' sSelected
                   , sel.nOrderNum
                   , sel.sMasterkey
                   , sel.tloAttributeList
                FROM table (oSelection1) e
                   , table (oSelection) sel
               WHERE sel.sMasterKey = e.sMasterKey);

      oSelection := oSelection2;   

    when csEntity IN ('BIO$COVIB$RUN$DESCRIPTION','BIO$COVIB$RUN$COMMENTS') and STB$OBJECT.getCurrentReporttypeId in ( 10700, 10710, 10720, 10730 )  THEN
      select key into sStudyCode from isr$crit 
      where entity = 'BIO$STUDY' and repid = STB$OBJECT.getCurrentRepid;
      
      IF sBackupEntity IS NULL 
      OR sBackupEntity NOT IN ('BIO$COVIB$RUN$DESCRIPTION','BIO$COVIB$RUN$COMMENTS')
      OR nvl(sBackupID,'-1') != sStudyCode THEN 
        sBackupEntity := csEntity;
        sBackupID := sStudyCode;
          
        oSelection1Backup := ISR$REMOTE$COLLECTOR.getList ('BIO$GRID$COVIB$RUN', oParamList);
      
        isr$trace.debug('sStudyCode and oSelection1Backup',sStudyCode,sCurrentName, oSelection1Backup);
         
      END IF; 
      oSelection1 := oSelection1Backup;      
      
      BEGIN   
        if csEntity in ('BIO$COVIB$RUN$DESCRIPTION') then       
      
          SELECT ISR$OSELECTION$RECORD (sDisplay
                                      , sDisplay
                                      , sDisplay
                                      , sSelected
                                      , nOrderNum
                                      , sMasterKey
                                      , tloAttributeList)
            BULK COLLECT
            INTO oSelection2
            FROM (SELECT e.sDisplay
                       , e.sValue
                       , e.sInfo
                       , 'T' sSelected
                       , sel.nOrderNum
                       , sel.sMasterkey
                       , sel.tloAttributeList
                    FROM table (oSelection1) e
                       , table (oSelection) sel
                   WHERE sel.sMasterKey = e.sMasterKey);
                   
        else
          -- for BIO$COVIB$RUN$COMMENTS
          SELECT ISR$OSELECTION$RECORD (sInfo
                                      , sInfo
                                      , sInfo
                                      , sSelected
                                      , nOrderNum
                                      , sMasterKey
                                      , tloAttributeList)
            BULK COLLECT
            INTO oSelection2
            FROM (SELECT e.sDisplay
                       , e.sValue
                       , case when e.sInfo = e.sValue then null else e.sInfo end sInfo
                       , 'T' sSelected
                       , sel.nOrderNum
                       , sel.sMasterkey
                       , sel.tloAttributeList
                    FROM table (oSelection1) e
                       , table (oSelection) sel
                   WHERE sel.sMasterKey = e.sMasterKey);
         isr$trace.debug('oSelection1','see -->',sCurrentName, oSelection1);
                  
        end if;
      end;
      
      oSelection := oSelection2;   
      
    when csEntity = 'BIO$COVIB$PARAM$ACCEPTANCE' then
    
      sDefaultParam := STB$UTIL.getReptypeParameter(STB$OBJECT.getCurrentReporttypeid, csEntity || '_DEFAULT');
      
      if sDefaultParam is not null then
      
        oSelection1 := ISR$REMOTE$COLLECTOR.getList (csEntity, oParamList);
        
        SELECT ISR$OSELECTION$RECORD (sDisplay
                                    , sValue
                                    , sInfo
                                    , sSelected
                                    , nOrderNum
                                    , sMasterKey
                                    , tloAttributeList)
          BULK COLLECT
          INTO oSelection2
          FROM (SELECT e.sDisplay
                     , e.sValue
                     , NULL sInfo
                     , 'T' sSelected
                     , sel.nOrderNum
                     , sel.sMasterkey
                     , sel.tloAttributeList
                  FROM table (oSelection1) e
                     , table (oSelection) sel
                 WHERE e.sValue = sDefaultParam);
                           
        oSelection := oSelection2;   
      end if;
    
    when csEntity = 'BIO$GRID$STUDY$ANALYUNITS' then
      
      SELECT ISR$OSELECTION$RECORD (crit.sDisplay
                                  , crit.sValue
                                  , crit.sInfo
                                  , crit.sSelected
                                  , crit.nOrderNum
                                  , crit.sMasterKey
                                  , sel.tloAttributeList)
            BULK COLLECT
            INTO oSelection2    
       from (select info        sDisplay
                  , info        sValue
                  , 1           sInfo
                  , 'T'         sSelected
                  , ordernumber nOrderNum
                  , key         sMasterkey
             from isr$crit 
             where entity = 'BIO$GRID$STUDY$ANALYTES' 
             and repid = STB$OBJECT.getCurrentRepid) crit,
             table (oSelection) sel
             where  sel.sMasterKey = crit.sMasterKey;
             
       oSelection := oSelection2;  
    ELSE null;

  END CASE;

  isr$trace.stat('end','end',sCurrentName);
  RETURN oErrorObj;
EXCEPTION
  WHEN OTHERS THEN
    oErrorObj.handleError( Stb$typedef.cnSeverityCritical, utd$msglib.getmsg ('exUnexpectedError', stb$security.getCurrentLanguage,  csP1 => sqlerrm(sqlcode)),sCurrentName,
            dbms_utility.format_error_stack ||stb$typedef.crlf|| dbms_utility.format_error_backtrace ); 
    RETURN oErrorObj;
END getDefaultValues;

--***************************************************************************************************                      
FUNCTION getEntityDefault(sEntity IN STB$TYPEDEF.tLine, csMasterKey IN varchar2, oRow OUT iSR$Crit$Rec) RETURN STB$OERROR$RECORD IS
  oErrorObj         STB$OERROR$RECORD := STB$OERROR$RECORD();
  oSelection        ISR$TLRSELECTION$LIST;
  oParamList        iSR$ParamList$Rec := iSR$ParamList$Rec(); --parameter list
  sCurrentName constant varchar2(100) := $$PLSQL_UNIT||'.getEntityDefault('||sEntity||')';  
  sStudyCode        varchar2(10);      

BEGIN
  isr$trace.stat('begin','sEntity:'||sEntity||' csMasterKey:'||csMasterKey,sCurrentName);
    -- fill Param List with values
  oParamList.AddParam('REPORTTYPEID', STB$OBJECT.getCurrentReporttypeId);
  oParamList.AddParam('REPID', STB$OBJECT.getCurrentRepid);  
  
  CASE
    WHEN sEntity IN ('BIO$KNOWNMATCHING') THEN

      oSelection := ISR$REMOTE$COLLECTOR.getList (sEntity, oParamList);

      SELECT DISTINCT ISR$CRIT$REC (e.sValue
                                  , csMasterKey
                                  , e.sDisplay
                                  , e.sInfo
                                  , NULL)
        INTO oRow
        FROM table (oSelection) e
       WHERE e.sValue = csMasterkey;
      
      -- this is now done in getDefaultValues
--    when sEntity IN ('BIO$COVIB$RUN$DESCRIPTION','BIO$COVIB$RUN$COMMENTS') and STB$OBJECT.getCurrentReporttypeId=10700 and 2=1 THEN
--    
--        select key into sStudyCode from isr$crit 
--        where entity = 'BIO$STUDY' and repid = STB$OBJECT.getCurrentRepid;
--        
--        IF sBackupEntity IS NULL 
--        OR sBackupEntity NOT IN ('BIO$COVIB$RUN$DESCRIPTION','BIO$COVIB$RUN$COMMENTS')
--        OR nvl(sBackupID,'-1') != sStudyCode THEN 
--          sBackupEntity := sEntity;
--          sBackupID := sStudyCode;
--          oSelection1Backup := ISR$REMOTE$COLLECTOR.getList ('BIO$GRID$COVIB$RUN', oParamList);
--                   
--          isr$trace.debug('sStudyCode and oSelection1Backup',sStudyCode,sCurrentName, oSelection1Backup);
--        END IF; 
--        
--        BEGIN   
--          if sEntity in ('BIO$COVIB$RUN$DESCRIPTION') then
--            SELECT DISTINCT ISR$CRIT$REC (e.sDisplay                                        
--                                        , csMasterKey
--                                        , e.sDisplay
--                                        , e.sDisplay
--                                        , NULL)
--              INTO oRow
--              FROM table (oSelection1Backup) e
--             WHERE to_char(sStudyCode)||'-'||e.sValue = csMasterKey
--             --order by e.sDisplay
--             ;          
--          else                                                                      -- hier nur positive digits berücksichtigen          
--            SELECT ISR$CRIT$REC (listagg(all e.sInfo,' / ') within group (order by e.nOrdernum)                                    
--                               , csMasterKey
--                               , listagg(all e.sInfo,' / ') within group (order by e.nOrdernum)
--                               , listagg(all e.sInfo,' / ') within group (order by e.nOrdernum)
--                               , NULL) 
--              INTO oRow
--              FROM table (oSelection1Backup) e
--             WHERE to_char(sStudyCode)||'-'||e.sValue = csMasterKey
--             --order by e.sinfo
--             ;
--          end if;
--             --AND case when sEntity in ('SB$PRECISIONSPEC', 'SB$PRECISION') then e.sDisplay else e.sInfo end != csMasterKey; 
--            isr$trace.debug('oRow.sDisplay',oRow.Display,sCurrentName);  
--        EXCEPTION WHEN OTHERS THEN
--          --Stb$trace.setLog('exc_oRow_xml','ISR$STABILITY$SM$LAL.GetEntityDefault',sqlerrm,250,ISR$XML.XMLToClob(XMLTYPE(oRow)));
--          isr$trace.error('error',dbms_utility.format_error_stack ||stb$typedef.crlf|| dbms_utility.format_error_backtrace ,sCurrentName);
--          null;       
--        END;              
    ELSE
      NULL;
  END CASE;

  isr$trace.stat('end', oRow.key,sCurrentName);
  RETURN oErrorObj;
EXCEPTION
  WHEN OTHERS THEN
    oErrorObj.handleError( Stb$typedef.cnSeverityCritical, utd$msglib.getmsg ('exUnexpectedError', stb$security.getCurrentLanguage,  csP1 => sqlerrm(sqlcode)),sCurrentName,
            dbms_utility.format_error_stack ||stb$typedef.crlf|| dbms_utility.format_error_backtrace ); 
    RETURN oErrorObj;
END GetEntityDefault;

--***************************************************************************************************
Function GetFetalDamMatrixKey(csStudyID in varchar2, nColumn in integer, csTempTabPrefix in varchar2) return number is 
  TYPE      tDynCursor IS ref CURSOR;
  cMatrix   tDynCursor;
  sSuffixMatrix varchar2(50);
  nSuffixKey number;
  sPrefixMatrix varchar2(50);
  nPrefixKey number;
  --sTempTabPrefix    varchar2(50) := STB$UTIL.GetSystemParameter('TEMP_TAB_PREFIX');
  oErrorObj         STB$OERROR$RECORD := STB$OERROR$RECORD();
  sCurrentName       constant varchar2(100) := $$PLSQL_UNIT||'.GetFetalDamMatrixKey()'; 
  sStatement varchar2(32000);
begin      
  sStatement := 
  'select distinct bm.matrixkey, bm.matrix        
    from '||csTempTabPrefix||'BIO$SAMPLE s,
         '||csTempTabPrefix||'BIO$SAMPLE$RESULTS r,
         '||csTempTabPrefix||'BIO$BIOMATRIX BM,
         '||csTempTabPrefix||'BIO$STUDYSUBJ$GRP$TREATM SGT,
         '||csTempTabPrefix||'BIO$STUDY$SUBJECT Su
    where s.id=r.sampleID 
      and s.subjectgrouptreatmentid = bm.subjectgrouptreatmentid
      and sgt.id=s.subjectgrouptreatmentid
      and su.id=s.subjectid
      and s.sampletypesplitkey = bm.sampletypesplitkey      
      and sgt.studyid = '''||csStudyID||'''
      AND S.STUDYID = '''||csStudyID||''' AND R.STUDYID = '''||csStudyID||''' 
      AND BM.STUDYID = '''||csStudyID||''' AND su.STUDYID = '''||csStudyID||'''
      AND ((REGEXP_LIKE(bm.matrix, :csLikestr, ''i'') and :sRecComp = ''T'') OR (bm.matrix like :csLikestr and :sRecComp = ''F''))' ;
  isr$trace.debug('sStatement',sStatement,sCurrentName);    
  open cMatrix for sStatement using '_fetal','T','_fetal','T';
  fetch cMatrix into nSuffixKey, sSuffixMatrix;
  if cMatrix%found then
    close cMatrix;
    sPrefixMatrix := substr(sSuffixMatrix,1,instr(sSuffixMatrix,'_')-1);
    open cMatrix for sStatement using sPrefixMatrix,'F',sPrefixMatrix,'F';
    fetch cMatrix into nPrefixKey, sPrefixMatrix;
  end if;
  close cMatrix; 
  if nColumn = 2 then       
    return nPrefixKey;  -- dam
  else    
    return nSuffixKey;  -- fetal
  end if;  
EXCEPTION
  WHEN OTHERS THEN
    oErrorObj.handleError( Stb$typedef.cnSeverityCritical, utd$msglib.getmsg ('exUnexpectedError', stb$security.getCurrentLanguage,  csP1 => sqlerrm(sqlcode)),sCurrentName,
            dbms_utility.format_error_stack ||stb$typedef.crlf|| dbms_utility.format_error_backtrace );  
end GetFetalDamMatrixKey;

--***************************************************************************************************
Function IsFetalDamStudy(csStudyID in varchar2) return varchar2 is 
  cursor cMatrix(csLikestr in Varchar2) is
    select matrixkey, matrix
    from tmp$BIO$BIOMATRIX
    where REGEXP_LIKE(matrix, csLikestr, 'i');
  sSuffixMatrix varchar2(50);
  nSuffixKey number;
begin
  open cMatrix('_fetal');
  fetch cMatrix into nSuffixKey, sSuffixMatrix;
  close cMatrix;

  if sSuffixMatrix is not null then 
    return 'T';
  else 
    return 'F';
  end if;
end IsFetalDamStudy;

--***************************************************************************************************
FUNCTION getLov (sEntity IN VARCHAR2, ssuchwert IN VARCHAR2, oSelectionList OUT isr$tlrselection$list)
  RETURN stb$oerror$record   
IS
  sCurrentName                  CONSTANT VARCHAR2(100) := $$PLSQL_UNIT||'.getLov('||null||')';
  oErrorObj                     stb$oerror$record := STB$OERROR$RECORD();
  shandledby                    stb$reptypeparameter.handledby%TYPE;

  CURSOR cgetExperiment IS
    SELECT name display,
           code value
      FROM isr$bio$experiment
     ORDER BY ordernumber;  

  CURSOR cgethandledby (csEntity IN VARCHAR2) IS
    SELECT handledby
      FROM stb$reptypeparameter
     WHERE parametername = csEntity
       AND reporttypeid = stb$object.getcurrentreporttypeid;
BEGIN
  isr$trace.stat('begin','begin', sCurrentName);

  OPEN cgethandledby (sEntity);
  FETCH cgethandledby
  INTO  shandledby;
  CLOSE cgethandledby;
  isr$trace.debug ('sHandledBy', shandledby || ' ' || sEntity || ' ' || stb$object.getcurrentreporttypeid, sCurrentName);
  oSelectionList := isr$tlrselection$list ();
  
  CASE
    --***************************************
    WHEN upper (sEntity) = 'EXPERIMENT' THEN
      
      FOR rgetExperiment IN cgetExperiment LOOP
        oSelectionList.EXTEND;
        oSelectionList (oSelectionList.count()) := ISR$OSELECTION$RECORD(
                                                      rgetExperiment.display,
                                                      rgetExperiment.value,
                                                      null,
                                                      null,
                                                      oSelectionList.count()
                                                    );
      END LOOP;
   
    ELSE
      NULL;
    --***************************************  
  END CASE;

  IF sSuchWert IS NOT NULL AND oselectionlist.first IS NOT NULL THEN
    isr$trace.debug ('sSuchWert',  sSuchWert, sCurrentName); 
    FOR i IN 1..oSelectionList.count() LOOP
      IF oSelectionList(i).sValue = sSuchWert THEN
        isr$trace.debug ('oSelectionList('||i||').sSelected', oSelectionList(i).sValue, sCurrentName);
        oSelectionList(i).sSelected := 'T';
      END IF;
    END LOOP;
  END IF;

  isr$trace.stat('end','end', sCurrentName);  
  RETURN oErrorObj;
EXCEPTION
  WHEN OTHERS THEN
    oErrorObj.handleError( Stb$typedef.cnSeverityCritical, utd$msglib.getmsg ('exUnexpectedError', stb$security.getCurrentLanguage,  csP1 => sqlerrm(sqlcode)),sCurrentName,
            dbms_utility.format_error_stack ||stb$typedef.crlf|| dbms_utility.format_error_backtrace ); 
    RETURN oErrorObj;
END getlov;


END ISR$COVIB$BIOANALYTICS$WT$LAL;