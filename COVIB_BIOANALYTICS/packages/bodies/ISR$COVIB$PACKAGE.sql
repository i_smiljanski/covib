CREATE OR REPLACE PACKAGE BODY ISR$COVIB$PACKAGE
is

  EX_NOT_EXISTS         EXCEPTION;  
  sIP                   STB$JOBSESSION.IP%type;
  sPort                 STB$JOBSESSION.PORT%type;
  sTimeout              varchar2(10);

  --constants
  csOffshoot            varchar2(100) :='CAN_OFFSHOOT_DOC';
  csTarget              varchar2(100) :='TARGET';
  csConfig              varchar2(100) :='CONFIG';
  csMasterTemplate      varchar2(100) :='MASTERTEMPLATE';
  csTemplate            varchar2(100) :='TEMPLATE';
  csRohReport           varchar2(100) :='ROHREPORT';
  csRohDaten            varchar2(100) :='ROHDATEN';
  csRemoteSnapshot      varchar2(100) :='REMOTESNAPSHOT';
  csDocPropOid          varchar2(100) :='DOC_OID';
  csMimeType            varchar2(100) :='application/octet-stream';
  csISRTempPath         varchar2(100) :='[ISR.TEMP.PATH]';

  sContext              varchar2(500) := STB$UTIL.getSystemParameter('LOADER_CONTEXT');
  sNameSpace            varchar2(500) := STB$UTIL.getSystemParameter('LOADER_NAMESPACE');
  nCurrentLanguage      number        := stb$security.getcurrentlanguage;
  crlf                  constant varchar2(2) := chr(10)||chr(13);
  sJavaLogLevel      isr$loglevel.javaloglevel%type;

--***********************************************************************************************************************************
function copyFilesIntoJobtrail( cnJobid in     number,
                                 cxXml   in out xmltype)
  return STB$OERROR$RECORD
is
  sCurrentName   constant varchar2(100) := $$PLSQL_UNIT||'.copyFilesIntoJobtrail (JobID='||cnJobId||')';
  sUserMsg               varchar2(4000);
  sDevMsg                varchar2(4000);
  sImplMsg               varchar2(4000);
  oErrorObj               STB$OERROR$RECORD :=STB$OERROR$RECORD();
  nCounter               number;

  cursor cGetIdAll is
    select rf.filename
         , rf.textfile
         , rf.binaryfile
         , STB$UTIL.getReportParameterValue (
             'GRAPHIC_HEIGHT'
            , STB$JOB.getJobParameter ('REPID', cnJobid)
           )
              height
         , case when parametervalue != 'T' then 'F' else 'T' end display
      from isr$REPORT$FILE rf, stb$jobparameter jp
     where repid = STB$JOB.getJobParameter ('REPID', cnJobid)
       and jp.jobid = cnjobid
       and (parametername = 'FILTER_' || REPLACE(REPLACE(filename, '@'), '$')
        and parametervalue = 'T'
         or rf.binaryfile is not null
        and parametername = 'REPID')
    order by SUBSTR (rf.filename, INSTR (rf.filename, '.') + 1), rf.filename;

  CURSOR cGetTabFiles IS
    SELECT rf.filename
         , rf.textfile
         , rf.binaryfile
         , STB$UTIL.getReportParameterValue (
             'GRAPHIC_HEIGHT'
            , STB$JOB.getJobParameter ('REPID', cnJobid)
           )
              height
         , STB$UTIL.getReportParameterValue ( 'TABNAME_'||rf.filename, STB$JOB.getJobParameter ('REPID', cnJobid) ) display
      FROM stb$jobtrail rf, stb$jobparameter jp
     WHERE jp.jobid = cnJobid
       AND rf.jobid = jp.jobid
       AND parametername = 'FILTER_' || REPLACE(REPLACE(filename, '@'), '$')
       AND parametervalue = 'T'
    ORDER BY to_number( STB$UTIL.getReportParameterValue ( 'TABORDER_'||rf.filename, STB$JOB.getJobParameter ('REPID', cnJobid) ) ), SUBSTR (rf.filename, INSTR (rf.filename, '.') + 1), rf.filename;
  clXSLT      clob;
  clTextFile  clob;
  clXML       clob;

  clCSS       CLOB;

  sServer     VARCHAR2(500);

  sExcelHeader  stb$reportparameter.parametervalue%TYPE := STB$UTIL.getReportParameterValue ( 'EXCEL_HEADER', STB$JOB.getJobParameter ('REPID', cnJobid) );
  sExcelFooter  stb$reportparameter.parametervalue%TYPE := STB$UTIL.getReportParameterValue ( 'EXCEL_FOOTER', STB$JOB.getJobParameter ('REPID', cnJobid) );

  sUseTabs  stb$reportparameter.parametervalue%TYPE := STB$UTIL.getReportParameterValue ( 'USE_EXCEL_TABS', STB$JOB.getJobParameter ('REPID', cnJobid) );

  sTitle  stb$jobparameter.parametervalue%TYPE := STB$JOB.getJobParameter ('TITLE', cnJobid);

  xHtmlFile   XMLTYPE;

  sLobFlag    VARCHAR2(1);
  sReturn   varchar2(1);

begin
  isr$trace.stat('begin', 'Function '||sCurrentName||' started.', sCurrentName);

  SELECT listagg(server.alias, ', ') WITHIN GROUP (order by crit.ordernumber) server
  INTO sServer
  FROM ISR$CRIT crit, ISR$SERVER server
  WHERE crit.repid = STB$JOB.getJobParameter ('REPID', cnJobid)
  AND crit.key = server.serverid
  AND crit.entity = 'SERVER';

  IF sExcelHeader is not null THEN
        sExcelHeader := REPLACE(sExcelHeader,'%REPID%', STB$JOB.getJobParameter ('REPID', cnJobid));
        sExcelHeader := REPLACE(sExcelHeader,'%SRNUMBER%', STB$JOB.getJobParameter ('SRNUMBER', cnJobid));
        sExcelHeader := REPLACE(sExcelHeader,'%TITLE%', replace(sTitle,'&','&&'));  
        sExcelHeader := REPLACE(sExcelHeader,'%DATE%',to_char(sysdate, STB$UTIL.getSystemParameter('DATEFORMAT')));
        sExcelHeader := REPLACE(sExcelHeader,'%ISR_SYSTEM%',replace(STB$UTIL.getSystemParameter('ISR_SYSTEM'),'&','&&'));
        sExcelHeader := REPLACE(sExcelHeader,'%ISR_VERSION%',replace(STB$UTIL.getSystemParameter('ISR_VERSION'),'&','&&'));
        sExcelHeader := REPLACE(sExcelHeader,'%CUSTOMER%',replace(STB$UTIL.getSystemParameter('CUSTOMER'),'&','&&'));
        sExcelHeader := REPLACE(sExcelHeader,'%SERVER%',replace(sServer,'&','&&'));
  END IF;

  IF sExcelFooter is not null THEN
        sExcelFooter := REPLACE(sExcelFooter,'%REPID%', STB$JOB.getJobParameter ('REPID', cnJobid));
        sExcelFooter := REPLACE(sExcelFooter,'%SRNUMBER%', STB$JOB.getJobParameter ('SRNUMBER', cnJobid));
        sExcelFooter := REPLACE(sExcelFooter,'%TITLE%', replace(sTitle,'&','&&'));  
        sExcelFooter := REPLACE(sExcelFooter,'%DATE%',to_char(sysdate, STB$UTIL.getSystemParameter('DATEFORMAT')));
        sExcelFooter := REPLACE(sExcelFooter,'%ISR_SYSTEM%',replace(STB$UTIL.getSystemParameter('ISR_SYSTEM'),'&','&&'));
        sExcelFooter := REPLACE(sExcelFooter,'%ISR_VERSION%',replace(STB$UTIL.getSystemParameter('ISR_VERSION'),'&','&&'));
        sExcelFooter := REPLACE(sExcelFooter,'%CUSTOMER%',replace(STB$UTIL.getSystemParameter('CUSTOMER'),'&','&&'));
        sExcelFooter := REPLACE(sExcelFooter,'%SERVER%',replace(sServer,'&','&&'));
  END IF;

  IF sUseTabs is not null and sUseTabs = 'T' THEN
    clXSLT :=  '<?xml version="1.0" encoding="UTF-8"?>
                  <xsl:stylesheet version="3.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform" xmlns:xs="http://www.w3.org/2001/XMLSchema" xmlns:fn="http://www.w3.org/2005/xpath-functions">
                    <xsl:output method="html" version="4.01" encoding="ISO-8859-15" indent="yes" media-type="text/html"/>
                    <xsl:template match="/">
                      <xsl:text disable-output-escaping="yes"><![CDATA[<html xmlns:v="urn:schemas-microsoft-com:vml"
                        xmlns:o="urn:schemas-microsoft-com:office:office"
                        xmlns:x="urn:schemas-microsoft-com:office:excel"
                        xmlns:dt="uuid:C2F41010-65B3-11d1-A29F-00AA00C14882"
                        xmlns="http://www.w3.org/TR/REC-html40">

                        <head>
                        <meta name="Excel Workbook Frameset">
                        <meta http-equiv=Content-Type content="text/html; charset=iso-8859-15">
                        <meta name=ProgId content=Excel.Sheet>
                        <meta name=Generator content="Microsoft Excel 12">
                        <title>'||sTitle||'</title>
                        <style>
                          @page {
                            mso-header-data:"'||sExcelHeader||'";
                            mso-footer-data:"'||sExcelFooter||'";
                          } 
                        </style>

                        <!--[if gte mso 9]><xml>
                         <x:ExcelWorkbook>
                          <x:ExcelWorksheets>';
    FOR rGetTabFiles IN cGetTabFiles LOOP
      isr$trace.debug('status filename', 'rGetTabFiles.filename: '||rGetTabFiles.filename, sCurrentName);
      isr$trace.debug('status display', 'rGetTabFiles.display: '||rGetTabFiles.display, sCurrentName);
      clXSLT := clXSLT ||'
                           <x:ExcelWorksheet>
                            <x:Name>'||rGetTabFiles.display||'</x:Name>
                            <x:WorksheetSource HRef="'||rGetTabFiles.filename||'"/>
                           </x:ExcelWorksheet>';
    END LOOP;
    clXSLT := clXSLT ||' 
                          </x:ExcelWorksheets>
                          <x:ActiveSheet>0</x:ActiveSheet>
                          <x:ProtectStructure>False</x:ProtectStructure>
                          <x:ProtectWindows>False</x:ProtectWindows>
                         </x:ExcelWorkbook>
                        </xml><![endif]-->
                        </head>
                      <body></body>
                    </html>]]></xsl:text>


                   </xsl:template>
                 </xsl:stylesheet>
                  ';




  ELSE
      clXSLT :=  '<?xml version="1.0" encoding="UTF-8"?>
                  <xsl:stylesheet version="2.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform" xmlns:xs="http://www.w3.org/2001/XMLSchema" xmlns:fn="http://www.w3.org/2005/xpath-functions">
                    <xsl:output method="html" version="4.01" encoding="ISO-8859-15" indent="yes" media-type="text/html"/>
                    <xsl:template match="/">
                      <html>
                        <head>
                          <title>'||sTitle||'</title>
                          <style>
                            <![CDATA[@page {
                              mso-header-data:"'||sExcelHeader||'";
                              mso-footer-data:"'||sExcelFooter||'";
                            } ]]>
                          </style>
                        </head>
                        <body>
                          <table>
                  ';

      --nCounter := 0;
  for rGetFiles  in cGetIdAll loop
    isr$trace.debug('status filename', 'rGetFiles.filename: '||rGetFiles.filename, sCurrentName);
    isr$trace.debug('status display', 'rGetFiles.display: '||rGetFiles.display, sCurrentName);

    sLobFlag := case
                   when NVL (DBMS_LOB.getlength (rGetFiles.textfile), 0) = 0 then
                      'B'
                   else
                      'C'
                end;
    isr$trace.debug('status sLobFlag', 'sLobFlag: '||sLobFlag, sCurrentName);

    if rGetFiles.display = 'T' then

      clXSLT :=  clXSLT ||
                 '<tr><td'||case when rGetFiles.height is not null then ' height="'||rGetFiles.height||'"' end ||'>' ||
                 case
                   when sLobFlag = 'B' then '<img src="REPORTFILE$'||rGetFiles.filename||'" title="'||rGetFiles.filename||'" alt="'||rGetFiles.filename||'"/>' 
                   else '<xsl:value-of select="document(''REPORTFILE$'||rGetFiles.filename||''')/dummy" disable-output-escaping="yes"/>' 
                 end ||
                 '</td></tr>';

   end if;

    xHtmlFile := isr$XML.ClobToXML('<?xml version="handled by isr$XML" encoding="handled by isr$XML"?><dummy><![CDATA['||rGetFiles.textfile||']]></dummy>');

    insert into STB$JOBTRAIL (JOBID
                            , DOCUMENTTYPE
                            , MIMETYPE
                            , DOCSIZE
                            , TIMESTAMP
                            , BINARYFILE
                            , TEXTFILE
                            , FILENAME
                            , BLOBFLAG
                            , ENTRYID
                            , REFERENCE
                            , LEADINGDOC
                            , DOWNLOAD)
    values (
              cnJobId
            , 'REPORTFILE'
            , null
            , case
                 when sLobFlag = 'B' then
                    DBMS_LOB.getlength (rGetFiles.binaryfile)
                 else
                    DBMS_LOB.getlength (rGetFiles.textfile)
              END
            ,  sysdate
            , rGetFiles.binaryfile
            , isr$XML.XMLToClob(xHtmlFile)
            , 'REPORTFILE$'||rGetFiles.filename
            , sLobFlag
            , STB$JOBTRAIL$SEQ.NEXTVAL
            , STB$JOB.getJobParameter ('REPID', cnJobid)
            , 'F'
            , 'T'
           );

    if STB$UTIL.getSystemParameter('JAVA_INSIDE_DB') = 'F' and NVL (isr$process.checkProcessServer, 'F') = 'T' and sLobFlag != 'B' then
      isr$PROCESS.saveBlob(STB$UTIL.clobToBlob(ISR$XML.XMLToClob(xHtmlFile)), isr$PROCESS.getUserHome, isr$PROCESS.sSessionPath||'REPORTFILE$'||rGetFiles.filename);
   end if;
  end loop;

  clXSLT := clXSLT ||
            '         </table>
                    </body>
                  </html>
                </xsl:template>
              </xsl:stylesheet>
              ';
  END IF;    
  clCSS := '
            @page {
              mso-header-data:"'||sExcelHeader||'";
              mso-footer-data:"'||sExcelFooter||'";
            }
           ';
  INSERT INTO STB$JOBTRAIL (
                  JOBID
                , DOCUMENTTYPE
                , MIMETYPE
                , DOCSIZE
                , TIMESTAMP
                , TEXTFILE
                , FILENAME
                , BLOBFLAG
                , ENTRYID
                , REFERENCE
                , LEADINGDOC
                , DOWNLOAD
              )
        VALUES (
                  cnJobId
                , 'REPORTFILE'
                , 'text/css'
                , DBMS_LOB.getlength (clCSS)
                , SYSDATE
                , clCSS
                , 'stylesheet.css'
                , 'C'
                , STB$JOBTRAIL$SEQ.NEXTVAL
                , STB$JOB.getJobParameter ('REPID', cnJobid)
                , null
                , 'T'
               );              
  isr$trace.debug_all('status clXSLT', 'clXSLT: '||clXSLT, sCurrentName);

  oErrorObj := STB$DOCKS.saveXMLReport( isr$XML.clobToXml('<dummy/>'), cnJobid, 'REPORTFILES.XML');
  select textfile
    into clXML
    from stb$jobtrail
   where documenttype = csRohDaten
     and jobid = cnJobId
     and filename = 'REPORTFILES.XML';

  sReturn := isr$xml.XSLTTransform(clXSLT, clXML, clTextFile, null, cnJobId);
  if sReturn != 'T' then
    Stb$job.markDBJobAsBroken(cnJobId, sIp, sPort, oErrorObj.sErrorCode, oErrorObj.sErrorMessage, oErrorObj.sSeverityCode);
    return oErrorObj;
 end if;

  insert into STB$JOBTRAIL (JOBID
                          , DOCUMENTTYPE
                          , MIMETYPE
                          , DOCSIZE
                          , TIMESTAMP
                          , TEXTFILE
                          , FILENAME
                          , BLOBFLAG
                          , ENTRYID
                          , REFERENCE
                          , LEADINGDOC
                          , DOWNLOAD)
  values (
            cnJobId
          , csTarget
          , null
          , DBMS_LOB.getlength (clTextFile)
          ,  sysdate
          , clTextFile
          , 'REPORTFILES.HTM'
          , 'C'
          , STB$JOBTRAIL$SEQ.NEXTVAL
          , STB$JOB.getJobParameter ('REPID', cnJobid)
          , 'F'
          , 'T'
         );


  isr$trace.stat('end', 'Function '||sCurrentName||' finished',sCurrentName);
  return oErrorObj;

exception
  when others then
    sUserMsg := utd$msglib.getmsg ('exUnexpectedError', nCurrentLanguage,  csP1 => sqlerrm(sqlcode));
    sImplMsg := sCurrentName || rtrim(': '||sImplMsg,': ');
    sDevMsg  := substr(dbms_utility.format_error_stack ||crlf|| dbms_utility.format_error_backtrace, 1,4000);
    oErrorObj.handleError ( Stb$typedef.cnSeverityCritical, sUserMsg, sImplMsg, sDevMsg );
    Stb$job.markDBJobAsBroken(cnJobId, sIp, sPort, oErrorObj.sErrorCode, oErrorObj.sErrorMessage, oErrorObj.sSeverityCode);
    return oErrorObj;
end copyFilesIntoJobtrail;

--***********************************************************************************************************************************
function setFileFilter(cnJobid IN NUMBER, cxXml IN OUT XMLTYPE) RETURN STB$OERROR$RECORD
is
  sCurrentName   constant varchar2(100) := $$PLSQL_UNIT||'.setFileFilter (JobID='||cnJobId||')';
  sUserMsg               varchar2(4000);
  sDevMsg                varchar2(4000);
  sImplMsg               varchar2(4000);
  oErrorObj               STB$OERROR$RECORD :=STB$OERROR$RECORD();
  sExperiment             stb$reportparameter.parametervalue%TYPE := STB$UTIL.getReportParameterValue ( 'EXPERIMENT', STB$JOB.getJobParameter ('REPID', cnJobid) );

  CURSOR cGetFiles IS
    SELECT ot.bookmarkname||'.'||st.extension filename
      FROM ISR$STYLESHEET st, ISR$OUTPUT$TRANSFORM ot, ISR$FILE$PARAMETER$VALUES fpv, ISR$CRIT c
     WHERE st.stylesheetid = fpv.info
       AND fpv.entity = 'STYLESHEET$BOOKMARK'
       --AND fpv.value = sExperiment
       AND st.stylesheetid = ot.stylesheetid
       AND fpv.reporttypeid = STB$JOB.getJobParameter ('REPORTTYPEID', cnJobid)
       AND ot.outputid = STB$JOB.getJobParameter ('OUTPUTID', cnJobid)
       AND fpv.fileid = STB$JOB.getJobParameter ('MASTERTEMPLATE', cnJobid)
       AND (c.repid = STB$JOB.getJobParameter ('REPID', cnJobid)
       AND c.entity = 'BIO$EXPERIMENT'
       AND fpv.display = c.key)
    ORDER BY st.extension, ot.bookmarkname;

BEGIN
  isr$trace.stat('begin', 'Function '||sCurrentName||' started.', sCurrentName);

  FOR rGetFiles IN cGetFiles LOOP
    isr$trace.debug('status filename', 'rGetFiles.filename: '||rGetFiles.filename, sCurrentName);

    STB$JOB.setJobParameter('FILTER_'||rGetFiles.filename, 'T', cnJobid);
  END LOOP;

  isr$trace.stat('end', 'Function '||sCurrentName||' finished',sCurrentName);
  return oErrorObj;

exception
when others then
    sUserMsg := utd$msglib.getmsg ('exUnexpectedError', nCurrentLanguage,  csP1 => sqlerrm(sqlcode));
    sImplMsg := sCurrentName || rtrim(': '||sImplMsg,': ');
    sDevMsg  := substr(dbms_utility.format_error_stack ||crlf|| dbms_utility.format_error_backtrace, 1,4000);
    oErrorObj.handleError ( Stb$typedef.cnSeverityCritical, sUserMsg, sImplMsg, sDevMsg );
    Stb$job.markDBJobAsBroken(cnJobId, sIp, sPort, oErrorObj.sErrorCode, oErrorObj.sErrorMessage, oErrorObj.sSeverityCode);
    return oErrorObj;
end setFileFilter;

--*******************************************************************************************************************
function loadFilesToClient( cnJobid in     number,
                            cxXml   in out xmltype)
  return STB$OERROR$RECORD
is
  exUploadFileError exception;
  sCurrentName      constant varchar2(100) := $$PLSQL_UNIT||'.loadFilesToClient (JobID='||cnJobId||')';
  sUserMsg          varchar2(4000);
  sDevMsg           varchar2(4000);
  sImplMsg          varchar2(4000);
  oErrorObj         STB$OERROR$RECORD :=STB$OERROR$RECORD();
  sclXML            clob;
  nJavaRet          number;
  sReturn           varchar2(4000);
  nMengeBytes       number;
  nMengeDocs        number;
  nRecordID         STB$JOBTRAIL.ENTRYID%type;
  cBlob             STB$JOBTRAIL.BINARYFILE%type;
  sRemoteEx     clob;
  sConfigName    isr$transfer$file$config.configname%type;

  cursor cGetFiles(nJobId in number) is
    select blobflag
         , binaryfile
         , filename
         , documenttype
      from STB$JOBTRAIL
     where jobid = nJobId
       and download = 'T'
       and (NVL(dbms_lob.getlength(binaryfile), 0) > 0
            or NVL(dbms_lob.getlength(textfile), 0) > 0);

  rFiles cGetFiles%ROWTYPE;

  cursor cGetLog is
    select isr$XML.xmlToClob (
              xmlelement (
                 "ENTRIES"
               , (select xmlagg(xmlelement (
                                   "ENTRY"
                                 , xmlattributes (
                                      TO_CHAR (timestamp, STB$UTIL.getSystemParameter('DATEFORMAT')) as "DATE"
                                   )
                                 , xmlelement ("ACTIONTAKEN", ACTIONTAKEN)
                                 , xmlelement ("FUNCTIONGROUP", FUNCTIONGROUP)
                                 , xmlelement ("LOGENTRY", LOGENTRY)
                                 , xmlelement ("USERNAME", ORACLEUSERNAME)
                                 , xmlelement ("LOGLEVEL", LOGLEVEL)
                                 , xmlelement ("REFERENCE", REFERENCE)
                                ))
                    from isr$log)
              )
           )
      from DUAL;

begin
  isr$trace.stat('begin', 'Function '||sCurrentName||' started.', sCurrentName);
  sJavaLogLevel := isr$trace.getJavaUserLoglevelStr;

  if Stb$util.getSystemParameter('DOWNLOADFLAG') = 'T' then

    update STB$JOBTRAIL
       set DOWNLOAD = 'T'
     where jobid = cnJobId
       /*AND (DBMS_LOB.getlength (rFiles.binaryfile) > 0
         or DBMS_LOB.getlength (rFiles.textfile) > 0)*/;

    insert into STB$JOBTRAIL (JOBID
                            , DOCUMENTTYPE
                            , MIMETYPE
                            , DOCSIZE
                            , TIMESTAMP
                            , TEXTFILE
                            , FILENAME
                            , BLOBFLAG
                           -- , ENTRYID
                            , REFERENCE
                            , LEADINGDOC
                            , DOWNLOAD)
       (select cnJobId
             , 'STYLESHEET'
             , EXTENSION
             , DOCSIZE
             ,  sysdate
             , STYLESHEET
             , FILENAME
             , 'C'
         --    , STB$JOBTRAIL$SEQ.NEXTVAL
             , TO_CHAR (s.stylesheetid)
             , 'F'
             , 'T'
          from isr$stylesheet s,
            (select distinct s.stylesheetid
          from isr$STYLESHEET s, isr$OUTPUT$TRANSFORM ot
         where ot.outputid = STB$JOB.getJobParameter ('OUTPUTID', cnJobId)
           and ot.stylesheetid = s.STYLESHEETID
           and stylesheet is not null
             and DBMS_LOB.getLength (stylesheet) > 0) so
         where so.stylesheetid =s.stylesheetid) ;

    commit;
 end if;

  /*SELECT SUM (docsize), count (1)
    into nMengeBytes, nMengeDocs
    from STB$JOBTRAIL
   where jobid = cnJobId
     and blobflag is not null
     and ( (blobflag = 'B'
        and NVL(dbms_lob.getlength(binaryfile), 0) > 0)
       or (blobflag = 'C'
       and NVL(dbms_lob.getlength(textfile), 0) > 0))
     and download = 'T';

  nJavaRet := Stb$java.initUpload(sIp, sPort, To_Char(cnJobId), To_Char(nMengeDocs), To_Char(nMengeBytes));
  if nJavaRet != 0 then
    isr$trace.debug('nJavaRet',nJavaRet, scurrentname);
    raise exJavaErr;
 end if;*/

  for rGetClobFiles in (select *
                          from stb$jobtrail
                         where blobflag = 'C'
                           and NVL(dbms_lob.getlength(textfile), 0) > 0
                           and jobid = cnJobid) loop
    cBlob := STB$UTIL.clobToBlob (rGetClobFiles.textfile);
    update STB$JOBTRAIL
       set binaryfile = cBlob,
           blobflag ='B',
           textfile = Empty_Clob()
     where entryid = rGetClobFiles.entryid;
    commit;
    DBMS_LOB.FREETEMPORARY(cBlob);
  end loop;

  sIP := STB$JOB.getJobParameter('CLIENT_IP', cnJobid);
  sPort := STB$JOB.getJobParameter('CLIENT_PORT', cnJobid);

  open cGetFiles(cnJobId);
  loop
   fetch cGetFiles into rFiles;
    exit when cGetFiles%NOTFOUND;
    isr$trace.debug('status filename', 'rFiles.filename: '||rFiles.filename, sCurrentName);
    if dbms_lob.getlength(rFiles.binaryfile) > 0 then
      isr$trace.debug('action load file', 'load '||rFiles.filename, sCurrentName);
      sConfigName := case when rFiles.blobflag = 'B' then 'LOADER_BLOB_FILE' when rFiles.blobflag = 'C' then 'LOADER_CLOB_FILE' end;
        sImplMsg :=    'sIP: '||sIP ||crlf
                    || 'sPort: '||sPort||crlf
                    || 'sContext: '||sContext||crlf
                    || 'sNamespace: '||sNamespace||crlf
                    || 'cnJobid: '||cnJobid||crlf
                    || 'filename: '||rFiles.filename||crlf
                    || 'sConfigName: '||sConfigName;
      sRemoteEx := ISR$LOADER.loadFileToLoader(sIp, sPort, sContext, sNamespace, cnJobid, rFiles.filename, sConfigName, rFiles.documenttype, sJavaLogLevel, isr$trace.getReference, to_number(stimeout));

     if sRemoteEx is not null then
      isr$trace.warn(' error', 'error ', sCurrentName, sRemoteEx);
      sUserMsg := utd$msglib.getmsg('exUploadFileErrorText', nCurrentLanguage, rFiles.filename,  sIp || ' ' || sPort );
      oErrorObj.handleJavaError( Stb$typedef.cnSeverityCritical,  sRemoteEx, sCurrentName );
      raise exUploadFileError;
    end if;
   end if;
  end loop;
  close cGetFiles;

  isr$trace.stat('end', 'Function '||sCurrentName||' finished',sCurrentName);
  return oErrorObj;

exception
  when exUploadFileError then
    /*sUserMsg := utd$msglib.getmsg ('exUploadFileErrorText', nCurrentLanguage);
    sImplMsg := sCurrentName ||crlf|| sImplMsg;
    sDevMsg  := substr(dbms_utility.format_error_stack ||crlf|| dbms_utility.format_error_backtrace, 1,4000);*/
    oErrorObj.handleError ( Stb$typedef.cnSeverityCritical, sUserMsg, sImplMsg, sDevMsg );
    oErrorObj.handleError ( Stb$typedef.cnSeverityCritical, sUserMsg, sCurrentName,
                            substr(dbms_utility.format_error_stack || CHR(10) || dbms_utility.format_error_backtrace, 1,4000)  );
    Stb$job.markDBJobAsBroken(cnJobId, sIp, sPort, oErrorObj.sErrorCode, oErrorObj.sErrorMessage, oErrorObj.sSeverityCode);
    return oErrorObj;
  when others then
    sUserMsg := utd$msglib.getmsg ('exUnexpectedError', nCurrentLanguage,  csP1 => sqlerrm(sqlcode));
    sImplMsg := sCurrentName || rtrim(': '||sImplMsg,': ');
    sDevMsg  := substr(dbms_utility.format_error_stack ||crlf|| dbms_utility.format_error_backtrace, 1,4000);
    oErrorObj.handleError ( Stb$typedef.cnSeverityCritical, sUserMsg, sImplMsg, sDevMsg );
    Stb$job.markDBJobAsBroken(cnJobId, sIp, sPort, oErrorObj.sErrorCode, oErrorObj.sErrorMessage, oErrorObj.sSeverityCode);
    return oErrorObj;
end loadFilesToClient;

--***********************************************************************************************************************
function startApp( cnJobid in     number,
                   cxXml   in out xmltype)
  return STB$OERROR$RECORD
is
  sCurrentName   constant varchar2(100) := $$PLSQL_UNIT||'.startApp('||cnJobId||')';
  sUserMsg               varchar2(4000);
  sDevMsg                varchar2(4000);
  sImplMsg               varchar2(4000);
  oErrorObj               STB$OERROR$RECORD :=STB$OERROR$RECORD();
  nJavaRet                   number;
  exApplicationError         exception;
  exFileExistsError          exception;
  exUserCanceledJobError     exception;
  exPDFCreationCanceledError exception;

  xXml                    xmltype;

  sJobSequence            STB$JOBTRAIL.ENTRYID%type;
  sclDom                  STB$JOBTRAIL.TEXTFILE%type;

  bWait                  varchar2(50);
  sDocOid                 STB$DOCTRAIL.DOC_OID%type;

  /*cursor cGetEntryIdConfig is
    select jt.entryid
    from STB$JOBTRAIL jt
   where jt.jobid = cnJobId
     and jt.documenttype = csConfig;*/

  cursor cGetUserData(cnUserNo in STB$USER.USERNO%type) is
    select applicationpathes
    from   STB$USER
    where  userno = cnUserNo;

  rGetUserData cGetUserData%ROWTYPE;

  nModified              number := 0;
  sApplicationPathes     STB$USER.APPLICATIONPATHES%type;

  cursor cGetRunningRSApplication is
    select count ( * ), progress
      from stb$jobparameter jp, stb$jobsession js1, stb$jobsession js2, stb$jobqueue jq
     where jp.parametername = 'EXECUTE_APP'
       and jp.parametervalue = 'T'
       and jp.jobid = js1.jobid
       and js1.ip = js2.ip
       and js1.port = js2.port
       and js2.jobid = cnJobid
       and js2.jobid = jq.jobid
     group by progress;

  nCntJobs      number := 0;
  nProgress     number := 0;
  sOldProgress varchar2(4000);
  sReturn      varchar2(4000);
begin
  isr$trace.stat('begin', 'begin', sCurrentName);
  isr$trace.debug('value','cxXml IN OUT',sCurrentName,cxXml);

  select STB$JOB.getJobParameter('CLIENT_IP', cnJobid), STB$JOB.getJobParameter('CLIENT_PORT', cnJobid), nvl(so.timeout, STB$UTIL.getSystemParameter('TIMEOUT_LOADER_THREAD'))
  into sIp, sPort, sTimeout
  from STB$JOBSESSION js, isr$serverobserver$v so
  where js.ip = so.observer_host (+)
  and JS.PORT = so.port (+)
  and jobid = cnJobId;


  if TRIM(STB$JOB.getJobParameter('MAX_RS_JOBS', cnJobId)) is not null then
    begin
      select statustext into sOldProgress from STB$JOBQUEUE where jobid = cnJobId;
    exception when others then
      sOldProgress := null;
    end;
    open cGetRunningRSApplication;
   fetch cGetRunningRSApplication into nCntJobs, nProgress;
    if cGetRunningRSApplication%NOTFOUND then
      nCntJobs := 0;
   end if;
    close cGetRunningRSApplication;
    while nCntJobs >= STB$JOB.getJobParameter('MAX_RS_JOBS', cnJobId) loop
      open cGetRunningRSApplication;
     fetch cGetRunningRSApplication into nCntJobs, nProgress;
      if cGetRunningRSApplication%NOTFOUND then
        nCntJobs := 0;
     end if;
      close cGetRunningRSApplication;
      STB$JOB.setProgress(nProgress, Utd$msglib.GetMsg ('WAITING_FOR_RENDERING_SERVER_APPLICATIONS', Stb$security.GetCurrentLanguage), cnJobId);
    end loop;
    STB$JOB.setJobParameter('EXECUTE_APP', 'T', cnJobId);
    if TRIM(sOldProgress) is not null then
      STB$JOB.setProgress(nProgress, sOldProgress, cnJobId);
   end if;
 end if;

  isr$trace.debug('values','sIp: '||sIp||',  sPort: '||sPort||',  sContext: '||sContext||',  sNamespace: '||sNamespace||',  cnJobid: '||cnJobid, sCurrentName);
  isr$trace.debug('values','Stb$util.getSystemParameter(''TIMEOUT_LOADER_THREAD''): '||Stb$util.getSystemParameter('TIMEOUT_LOADER_THREAD'), sCurrentName);
  sReturn := ISR$LOADER.startApp(sIp, sPort, sContext, sNamespace, cnJobid, Stb$util.getSystemParameter('TIMEOUT_LOADER_THREAD'));
  IF sReturn != 'T' THEN
    ISR$TRACE.error('sReturn', sReturn, sCurrentName);
    sUserMsg := sReturn;
    raise exApplicationError;
  END IF;

  if TRIM(STB$JOB.getJobParameter('MAX_RS_JOBS', cnJobId)) is not null then
    STB$JOB.setJobParameter('EXECUTE_APP', 'F', cnJobId);
  end if;

  if sReturn != 'T' then
    sUserMsg := 'sIp=' || sIp || '; sPort=' || sPort || '; sContext=' ||sContext || '; sNamespace=' ||sNamespace || '; cnJobid=' ||cnJobid;
    raise exApplicationError;
  end if;

  -- 3. get the configFile (here config.xml)
  /*open  cGetEntryIdConfig;
  fetch cGetEntryIdConfig into sJobSequence;
  close cGetEntryIdConfig;
  isr$trace.debug('value','sJobSequence: '||sJobSequence,sCurrentName);*/

  sReturn := ISR$LOADER.loadFileFromLoader(sIp, sPort, sContext, sNameSpace, cnJobId, 'config.xml', 'LOADER_CLOB_FILE', csConfig, sJavaLogLevel, isr$trace.getReference, to_number(stimeout));
  IF sReturn != 'T' THEN
    ISR$TRACE.error('sReturn', sReturn, sCurrentName);
  END IF;
  if sReturn != 'T' then
    isr$trace.warn('warning config file load', 'Could not load config file from loader to write the applicationpathes'||crlf||'sReturn: '||sReturn, sCurrentName);
  else

    select textfile into sclDom
    from   STB$JOBTRAIL
    where  jobid = cnjobid
    and    documenttype = csConfig;

    xXml := isr$XML.ClobToXml(sclDom);
    sApplicationPathes := isr$XML.valueOf(xXml, '/config/apps/applicationpathes[last()]/text()');
    isr$trace.debug('status sApplicationPathes', 'sApplicationPathes: '||sApplicationPathes, sCurrentName);

    open  cGetUserData(STB$JOB.getJobParameter('USER', cnJobid));
    fetch cGetUserData into rGetUserData;
    close cGetUserData;

    nModified := 0;
    oErrorObj := Stb$audit.Openaudit ('USERAUDIT', STB$JOB.getJobParameter('USER', cnJobid));

    update stb$user
    set applicationpathes = sApplicationPathes
    where sApplicationPathes is not null and (applicationpathes is null or sApplicationPathes != applicationpathes)
    and userno = STB$JOB.getJobParameter('USER', cnJobid);

    if sApplicationPathes is not null
    and (rGetUserData.applicationpathes is null or sApplicationPathes != rGetUserData.applicationpathes) then
      nModified := nModified + 1;
      isr$trace.debug('status modified', 'nModified: '||nModified, sCurrentName);
      if nModified = 1 then
        oErrorObj := Stb$audit.Addauditrecord ('CHANGE');
      end if;
      oErrorObj := Stb$audit.Addauditentry ('APPLICATIONPATHES', rGetUserData.applicationpathes, sApplicationPathes, nModified);
    end if;

    oErrorObj := Stb$audit.Silentaudit;
    commit;

  end if;

  isr$trace.debug('value','cxXml IN OUT',sCurrentName,cxXml);
  isr$trace.stat('end','end',sCurrentName);
  return STB$OERROR$RECORD();

exception
  when exApplicationError then
    sUserMsg := utd$msglib.getmsg ('exApplicationErrorText', nCurrentLanguage,  csP1 => sqlerrm(sqlcode)) ||crlf|| sUserMsg;
    sImplMsg := sCurrentName || rtrim(': '||sImplMsg,': ');
    sDevMsg  := substr(dbms_utility.format_error_stack ||crlf|| dbms_utility.format_error_backtrace, 1,4000);
    oErrorObj.handleError ( Stb$typedef.cnSeverityCritical, sUserMsg, sImplMsg, sDevMsg );
    Stb$job.markDBJobAsBroken(cnJobId, sIp, sPort, oErrorObj.sErrorCode, oErrorObj.sErrorMessage, oErrorObj.sSeverityCode);
    return oErrorObj;
  when others then
    sUserMsg := utd$msglib.getmsg ('exUnexpectedError', nCurrentLanguage,  csP1 => sqlerrm(sqlcode));
    sImplMsg := sCurrentName || rtrim(': '||sImplMsg,': ');
    sDevMsg  := substr(dbms_utility.format_error_stack ||crlf|| dbms_utility.format_error_backtrace, 1,4000);
    oErrorObj.handleError ( Stb$typedef.cnSeverityCritical, sUserMsg, sImplMsg, sDevMsg );
    Stb$job.markDBJobAsBroken(cnJobId, sIp, sPort, oErrorObj.sErrorCode, oErrorObj.sErrorMessage, oErrorObj.sSeverityCode);
    return oErrorObj;
end startApp;

--***********************************************************************************************************************
function createParametersExcel( cnJobid in     number,
                                  cxXml   in out xmltype)
  return STB$OERROR$RECORD
is
  sCurrentName   constant varchar2(100) := $$PLSQL_UNIT||'.createParametersExcel (JobID='||cnJobId||')';
  sUserMsg               varchar2(4000);
  sDevMsg                varchar2(4000);
  sImplMsg               varchar2(4000);
  oErrorObj               STB$OERROR$RECORD :=STB$OERROR$RECORD();
  nCount                number;
  sNumberApps           varchar2(4000);
  sApp                   STB$SYSTEMPARAMETER.PARAMETERVALUE%type;
  csViewer              varchar2(200) :='viewer.exe';
  csExcel               varchar2(200) :='excel.exe';

  -- Get the application
  cursor cGetApp is
    select jt.filename, jt.documenttype
      from STB$JOBTRAIL jt
     where jt.jobid = cnJobId
       and jt.documenttype = 'APPL'
    order by entryid;

  cursor cGetFiles is
    select jt.filename, jt.documenttype
      from STB$JOBTRAIL jt
     where jt.jobid = cnJobId
       and (jt.documenttype = csRohReport
         or jt.documenttype = csTemplate
         or jt.documenttype = csTarget
         or jt.documenttype = 'TRANSFORMATION')
    order by entryid;

  cursor cGetNumberofApp is
    select TO_CHAR (COUNT (jt.filename)) app
      from STB$JOBTRAIL jt
     where jt.jobid = cnJobId
       and jt.documenttype = 'APPL';

begin
  isr$trace.stat('begin', 'Function '||sCurrentName||' started.', sCurrentName);
  isr$trace.debug('status cxXml', 'cxXml: see column LOGCLOB', sCurrentName, cxXml);

  -- number of applications comes out of the number of templates
  open  cGetNumberofApp ;
  fetch cGetNumberofApp  into sNumberApps;
  close cGetNumberofApp ;

  nCount :=0;
  -- use the general application set in the system parameters
  if sNumberApps = 0 then

    sApp := NVL(NVL(STB$JOB.getJobParameter('INVISIBLE_FILTER_AUDITAPPLICATION', cnJobId), STB$JOB.getJobParameter('FILTER_AUDITAPPLICATION', cnJobId)), STB$UTIL.getSystemParameter('AUDITAPPLICATION'));

    if sAPP = 'PDF' then

      for rGetFiles in cGetFiles loop
        --IF rGetFiles.documenttype in (csRohReport, csTarget) then
          STB$JOB.setJobParameter('TO', rGetFiles.FileName, cnJobId);
        --END IF;
      end loop;

      oErrorObj := copyFilesIntoJobtrail(cnJobid, cxXML);
      oErrorObj := stb$action.createParametersPDF(cnJobid, cxXML);

      for rGetFiles in cGetFiles loop
        --IF rGetFiles.documenttype in (csRohReport, csTarget) then
          STB$JOB.setJobParameter('TO', stb$action.getFileName(rGetFiles.FileName)||'.pdf', cnJobId);
        --END IF;
      end loop;

      oErrorObj := stb$action.createParametersCommon(cnJobid, cxXML);

    else

      nCount := isr$XML.valueOf(cxXML, 'count(/config/apps/app)') + 1;

      isr$XML.CreateNodeAttr(cxXML,  '/config/apps', 'app', '', 'order', nCount);
      isr$XML.SetAttribute(cxXML, '/config/apps/app[@order='||nCount||']', 'name', 'APPL');
      isr$XML.CreateNode(cxXML, '/config/apps/app[@order='||nCount||']', 'wait_for_process', 'false');
      isr$XML.CreateNode(cxXML, '/config/apps/app[@order='||nCount||']', 'executable', sApp);
      isr$XML.CreateNode(cxXML, '/config/apps/app[@order='||nCount||']', 'command', 'START');

      /*for rGetFiles in cGetFiles loop
        if upper(sAPP) = upper(csExcel) or (UPPER(sAPP) != upper(csExcel) and rGetFiles.documenttype in (csRohReport, 'TRANSFORMATION', csTarget)) then
          isr$XML.CreateNode(cxXML, '/config/apps/app[@order='||nCount||']', 'parameter', csISRTempPath || rGetFiles.FileName);
          isr$XML.SetAttribute(cxXML, '/config/apps/app[@order='||nCount||']/parameter[position()=last()]', 'order', isr$XML.valueOf(cxXML, 'count(/config/apps/app[@order='||nCount||']/parameter)'));
       end if;
      end loop;*/
      isr$XML.CreateNode(cxXML, '/config/apps/app[@order='||nCount||']', 'parameter', csISRTempPath || 'REPORTFILES.HTM');

   end if;

  else

    -- application is set in the file table
    for rGetApp in cGetApp loop

      nCount := isr$XML.valueOf(cxXML, 'count(/config/apps/app)') + 1;

      isr$XML.CreateNodeAttr(cxXML,  '/config/apps', 'app', '', 'order', nCount);
      isr$XML.SetAttribute(cxXML, '/config/apps/app[@order='||nCount||']', 'name', 'APPL');
      if nCount < To_Number(sNumberApps) then
        isr$XML.CreateNode(cxXML, '/config/apps/app[@order='||nCount||']', 'wait_for_process', 'true');
      else
        isr$XML.CreateNode(cxXML, '/config/apps/app[@order='||nCount||']', 'wait_for_process', 'false');
     end if;
      isr$XML.CreateNode(cxXML, '/config/apps/app[@order='||nCount||']', 'executable',rGetApp.filename);
      isr$XML.CreateNode(cxXML, '/config/apps/app[@order='||nCount||']', 'command', 'START');

      -- Emailjob
      if STB$JOB.getJobParameter('EMAILADDRESS', cnJobId) is not null then
        isr$XML.CreateNode(cxXML, '/config/apps/app[@order='||nCount||']', 'parameter', '-to');
        isr$XML.SetAttribute(cxXML, '/config/apps/app[@order='||nCount||']/parameter[position()=last()]', 'order', isr$XML.valueOf(cxXML, 'count(/config/apps/app[@order='||nCount||']/parameter)'));
        isr$XML.CreateNode(cxXML, '/config/apps/app[@order='||nCount||']', 'parameter', STB$JOB.getJobParameter('EMAILADDRESS', cnJobId));
        isr$XML.SetAttribute(cxXML, '/config/apps/app[@order='||nCount||']/parameter[position()=last()]', 'order', isr$XML.valueOf(cxXML, 'count(/config/apps/app[@order='||nCount||']/parameter)'));
        isr$XML.CreateNode(cxXML, '/config/apps/app[@order='||nCount||']', 'parameter', '-files');
        isr$XML.SetAttribute(cxXML, '/config/apps/app[@order='||nCount||']/parameter[position()=last()]', 'order', isr$XML.valueOf(cxXML, 'count(/config/apps/app[@order='||nCount||']/parameter)'));
     end if;

      -- get the files
      for rGetFiles in cGetFiles loop
        if rGetApp.filename <> csViewer then
          isr$XML.CreateNode(cxXML, '/config/apps/app[@order='||nCount||']', 'parameter', csISRTempPath || rGetFiles.FileName);
        else
          if rGetFiles.documenttype != csRohReport then
            isr$XML.CreateNode  (cxXML, '/config/apps/app[@order='||nCount||']', 'parameter', '"dataUrl=file:/'||csISRTempPath || rGetFiles.FileName|| '"' );
         end if;
       end if;
        isr$XML.SetAttribute(cxXML, '/config/apps/app[@order='||nCount||']/parameter[position()=last()]', 'order', isr$XML.valueOf(cxXML, 'count(/config/apps/app[@order='||nCount||']/parameter)'));
      end loop;

    end loop;
 end if;

  -- Emailjob
  if STB$JOB.getJobParameter('EMAILADDRESS', cnJobId) is not null then
    for i in 1..ISR$XML.valueOf(cxXML, 'count(/config/apps/app[@name="ME"])') loop
      isr$XML.DeleteNode(cxXML, '/config/apps/app[@name="ME"]');
    end loop;
    if STB$JOB.getJobParameter('NODETYPE', cnJobId) != 'DOCUMENT'
    or upper(ISR$XML.valueOf(cxXML, '/config/apps/app[@name="wordmodul"]/target')) NOT LIKE '%.DOC' then
      for i in 1..ISR$XML.valueOf(cxXML, 'count(/config/apps/app[@name="wordmodul"])') loop
        isr$XML.DeleteNode(cxXML, '/config/apps/app[@name="wordmodul"]');
      end loop;
   end if;
 end if;

  -- number OF APPS
  isr$XML.SetAttribute(cxXML, '/config/apps', 'num', isr$XML.valueOf(cxXML, 'count(/config/apps/app)'));

  isr$trace.debug('final status cxXml', 'cxXml: see column LOGCLOB', sCurrentName, cxXml);
  isr$trace.stat('end', 'Function '||sCurrentName||' finished',sCurrentName);
  return oErrorObj;

exception
  when others then
    sUserMsg := utd$msglib.getmsg ('exUnexpectedError', nCurrentLanguage,  csP1 => sqlerrm(sqlcode));
    sImplMsg := sCurrentName || rtrim(': '||sImplMsg,': ');
    sDevMsg  := substr(dbms_utility.format_error_stack ||crlf|| dbms_utility.format_error_backtrace, 1,4000);
    oErrorObj.handleError ( Stb$typedef.cnSeverityCritical, sUserMsg, sImplMsg, sDevMsg );
    Stb$job.markDBJobAsBroken(cnJobId, sIp, sPort, oErrorObj.sErrorCode, oErrorObj.sErrorMessage, oErrorObj.sSeverityCode);
    return oErrorObj;
end createParametersExcel;

--***********************************************************************************************************************

function split (csList varchar2, csDelimiter varchar2 := ',') return SYS.ODCIVARCHAR2LIST deterministic
is
    lResult       SYS.ODCIVARCHAR2LIST := SYS.ODCIVARCHAR2LIST();
    nStart        number(5) := 1;
    nEnd          number(5);
    cnLen constant number(5) := length( csList );
    cnLd  constant number(5) := length( csDelimiter );
begin
  if cnLen > 0 then
    nEnd := instr(csList, csDelimiter, nStart);
    while nEnd > 0 loop
      lResult.extend;
      lResult(lResult.COUNT) := SUBSTR(csList, nStart, nEnd - nStart);
      nStart := nEnd + cnLd;
      nEnd := instr(csList, csDelimiter, nStart);
    end loop;
    if nStart <= cnLen +1 then
      lResult.extend;
      lResult(lResult.count) := substr(csList, nStart, cnLen - nStart + 1);
    end if;
  end if;
  return lResult;
end split;

--******************************************************************************
function splitNumbers (csList varchar2, csDelimiter varchar2 := ',', csRange varchar2 := '-') return SYS.ODCINUMBERLIST pipelined
IS
    sList         varchar2(2000) := csList || csDelimiter; -- Add delimiter at the end to loop through all values, including the last one
    nStart        number(10) := 1;
    nEnd          number(10);
    cnLen         constant number(5) := length( sList );
    cnLd          constant number(5) := length( csDelimiter );
    lNums         SYS.ODCINUMBERLIST := SYS.ODCINUMBERLIST();
    
    function processValue (csValue varchar2) return SYS.ODCINUMBERLIST deterministic
    is
      sSplit        varchar2(200);
      nRangeStart   number(10);
      nRangeEnd     number(10);
      cnRangeLd     constant number(5) := length( csRange );
      lNums         SYS.ODCINUMBERLIST := SYS.ODCINUMBERLIST();
    begin
      if (validate_conversion(csValue as number) = 1) then
        if (to_number(csValue) is not null) then
          lNums.extend;
          lNums(lNums.count) := to_number(csValue);
        end if;
      elsif instr(csValue, csRange) != 0 then
        -- Range identificator found
        sSplit := substr(csValue, 1, instr(csValue, csRange) - cnRangeLd);
        nRangeStart := case when (validate_conversion(sSplit as number) = 1) then to_number(sSplit) else null end;
        sSplit := substr(csValue, instr(csValue, csRange) + cnRangeLd);
        nRangeEnd := case when (validate_conversion(sSplit as number) = 1) then to_number(sSplit) else null end;
        if (nRangeStart is not null and nRangeEnd is not null) then
          for nNum in nRangeStart .. nRangeEnd loop
            lNums.extend;
            lNums(lNums.count) := nNum;
          end loop;
        end if;
      end if;
      return lNums;
    end;
begin
  if cnLen > 0 then
    nEnd := instr(sList, csDelimiter, nStart);
    while nEnd > 0 loop
      lNums := processValue( SUBSTR(sList, nStart, nEnd - nStart) );
      for i in 1 .. lNums.count loop
        pipe row(lNums(i));
      end loop;
      nStart := nEnd + cnLd;
      nEnd := instr(sList, csDelimiter, nStart);
    end loop;
  end if;
  return;
end splitNumbers;


--******************************************************************************
function putParameters( oParameter  in STB$MENUENTRY$RECORD, 
                        olParameter in STB$PROPERTY$LIST  )
  return stb$oerror$record 
is
  sCurrentName                  constant varchar2(100) := $$PLSQL_UNIT||'.putParameters('||oParameter.sToken ||')';
  sHandleBy                     stb$reptypeparameter.handledby%TYPE;
  oErrorObj                     STB$OERROR$RECORD := STB$OERROR$RECORD();

begin
  ISR$TRACE.stat('begin','begin',sCurrentName);
  ISR$TRACE.debug('parameter','oParameter',sCurrentName,oParameter);  
  ISR$TRACE.debug('parameter','olParameter',sCurrentName,olParameter);    

  STB$OBJECT.setCurrentToken(oparameter.stoken);

  -- Processing takes place in  the current package
  case
  ---------------------- CAN_SEND_EMAIL_TO ------------------------
    when oparameter.stoken = 'CAN_SEND_EMAIL_TO' then                                   -- [MUIB-260] vs 27.Jun.2018
      oErrorObj := ISR$REPORTTYPE$PACKAGE.setJobParameter(oParameter.stoken);
      STB$JOB.setJobParameter('DO_NOT_SWITCH_REPSESSION', 'T', STB$JOB.njobid);

      if olParameter is not null then    
        for i in 1..olParameter.count()
        loop
          STB$JOB.setJobParameter(olParameter(i).sParameter, olParameter(i).sValue, STB$JOB.nJobid);
        end loop;
      end if;  

      oErrorObj := STB$UTIL.createFilterParameter(olParameter);
      oErrorObj := STB$JOB.startJob;
    --------------------------------------------------------------------------
  else
      raise EX_NOT_EXISTS;
  end case;

  ISR$TRACE.debug('end','end',sCurrentName);
  return oErrorObj;
exception
  when EX_NOT_EXISTS then
    oErrorObj.sErrorCode := utd$msglib.getmsg ('exNotExists', stb$security.getcurrentlanguage);
    oErrorObj.handleError( Stb$typedef.cnSeverityMessage, utd$msglib.getmsg ('exNotExistsText', stb$security.getCurrentLanguage,  csP1 => sqlerrm(sqlcode)),sCurrentName,
            dbms_utility.format_error_stack ||stb$typedef.crlf|| dbms_utility.format_error_backtrace ); 
    return oErrorObj;
  when others then
    rollback;
    oErrorObj.handleError ( Stb$typedef.cnSeverityWarning, utd$msglib.getmsg ('exUnexpectedError', nCurrentLanguage, csP1 => sqlerrm(sqlcode)), sCurrentName, 
                            substr(dbms_utility.format_error_stack || CHR(10) || dbms_utility.format_error_backtrace, 1,4000)  );  
    return oErrorObj;
end putparameters;


--*************************************************************************************************************************
function setMenuAccess( sParameter   in  varchar2, 
                        sMenuAllowed out varchar2 ) 
  return stb$oerror$record
is
    sCurrentName   constant varchar2(100) := $$PLSQL_UNIT||'.setMenuAccess('||sParameter||')';
    oErrorObj      STB$OERROR$RECORD      := STB$OERROR$RECORD();
    nReportStatus  number;
    sMsg           varchar2(4000);    
begin
  isr$trace.stat ('begin','begin', sCurrentName );

  nReportStatus := STB$OBJECT.getCurrentRepStatus;
  isr$trace.debug ('variable','nReportStatus: '||nReportStatus, sCurrentName );

  case
  ---------------------- CAN_SEND_EMAIL_TO ------------------------
    when sParameter = 'CAN_SEND_EMAIL_TO' then                   -- [MUIB-260] vs 27.Jun.2018
      if nReportStatus = STB$TYPEDEF.cnStatusFinal then
        sMenuAllowed := 'T';
      else
        sMenuAllowed := 'F';
      end if;
    --------------------------------------------------------------------------
  else
      raise EX_NOT_EXISTS;
  end case;

  isr$trace.stat ('end',  'sMenuAllowed:' || sMenuAllowed, sCurrentName);
  return oErrorObj;
exception
  when EX_NOT_EXISTS then
    oErrorObj.sErrorCode := utd$msglib.getmsg ('exNotExists', stb$security.getcurrentlanguage);
     oErrorObj.handleError( Stb$typedef.cnSeverityMessage, utd$msglib.getmsg ('exNotExistsText', stb$security.getCurrentLanguage,  csP1 => sqlerrm(sqlcode)),sCurrentName,
            dbms_utility.format_error_stack ||stb$typedef.crlf|| dbms_utility.format_error_backtrace ); 
    return oErrorObj;
  when others then
    sMsg :=  utd$msglib.getmsg ('exUnexpectedError', nCurrentLanguage, csP1 => sqlerrm(sqlcode));
    oErrorObj.handleError ( Stb$typedef.cnSeverityCritical, sMsg, sCurrentName, 
                            substr(dbms_utility.format_error_stack || CHR(10) || dbms_utility.format_error_backtrace, 1,4000)  );  
    return oErrorObj;    
END setMenuAccess;


--*************************************************************************************************************************
function callFunction( oParameter IN  STB$MENUENTRY$RECORD, 
                       olNodeList OUT STB$TREENODELIST )
  return stb$oerror$record
is
    sCurrentName       constant varchar2(100) := $$PLSQL_UNIT||'.callFunction('||oParameter.sToken ||')';
    nLanguage          utd$msg.lang%type      := STB$SECURITY.getCurrentLanguage;    
    oErrorObj          STB$OERROR$RECORD      := STB$OERROR$RECORD();
    nReporttypeId      number                 := STB$OBJECT.getCurrentReportTypeId;    
    sMsg               varchar2(4000);    
    oCurNode           STB$TREENODE$RECORD;    
    sReportTypeName    stb$reporttype.reportTypeName%type;

    cursor curGetReportTypeName is
      select UTD$MSGLIB.getMsg( sReportTypeName, nLanguage)
      from   stb$reporttype
      where  reporttypeid = nReporttypeId;

begin
  isr$trace.stat( 'begin','begin', sCurrentName );
  isr$trace.debug('parameter','olNodeList',sCurrentName,olNodeList);    

  oCurNode := STB$OBJECT.getCurrentNode;
  STB$OBJECT.setCurrentToken(oParameter.stoken);

  case
    when oParameter.stoken = 'CAN_SEND_EMAIL_TO' then

      olNodeList := STB$TREENODELIST ();
      olNodeList.extend;
      olNodeList(1) := STB$TREENODE$RECORD( STB$UTIL.getMaskTitle(oParameter.sToken ), 
                                            null, 'TRANSPORT', 'MASK', null, null, null, null, null,
                                            STB$TYPEDEF.cnMenuDialog );
      oErrorObj := STB$UTIL.getPropertyList(oParameter.sToken, olNodeList(1).tloPropertylist);
  else
    null;
  end case;  

  isr$trace.stat('end','end',sCurrentName);
  return oErrorObj;
exception
  when others then
    sMsg :=  utd$msglib.getmsg ('exUnexpectedError', nCurrentLanguage, csP1 => sqlerrm(sqlcode));
    oErrorObj.handleError ( Stb$typedef.cnSeverityCritical, sMsg, sCurrentName, 
                            substr(dbms_utility.format_error_stack || CHR(10) || dbms_utility.format_error_backtrace, 1,4000)  );  
    return oErrorObj;    
END callFunction;


--******************************************************************************
FUNCTION getLov (sEntity IN VARCHAR2, ssuchwert IN VARCHAR2, oSelectionList OUT isr$tlrselection$list)
  RETURN stb$oerror$record
IS
  sCurrentName                  CONSTANT VARCHAR2(200) := $$PLSQL_UNIT||'.getLov('||sEntity||')';
  oErrorObj                     stb$oerror$record := STB$OERROR$RECORD();

  cursor cGetDTA is
  select dtaid, description from isr$covib$dta;
   
   
BEGIN
  isr$trace.stat('begin','begin', sCurrentName);
  oSelectionList := isr$tlrselection$list ();
  
  CASE
    --***************************************
    WHEN upper (sEntity) in ('DTA_FOR_WORD', 'DTA_FOR_EXCEL') or sEntity like 'BIO$DTA$SELECTION_DEFAULT_SELECTION%' THEN
     oSelectionList.EXTEND;
     oSelectionList (oSelectionList.count()) := ISR$OSELECTION$RECORD(null, null, null, null, oSelectionList.COUNT());
     FOR rGetDTA IN cGetDTA LOOP
        oSelectionList.EXTEND;
        oSelectionList (oSelectionList.count()) := ISR$OSELECTION$RECORD(rGetDTA.dtaid, rGetDTA.dtaid, rGetDTA.description, null, oSelectionList.COUNT());
                  
       /* SELECT ISR$ATTRIBUTE$RECORD (sParameter, sValue, sPrepresent)
          BULK COLLECT INTO oSelectionList (oSelectionList.COUNT ()).tloAttributeList
          FROM (SELECT 'ICON' sParameter
                     , rGetDTA.icon sValue
                     , NULL sPrepresent
                  FROM DUAL);*/
      end loop;
    
      IF sSuchWert IS NOT NULL AND oselectionlist.first IS NOT NULL THEN
        isr$trace.debug ('sSuchWert',  sSuchWert, sCurrentName);
        FOR i IN 1..oSelectionList.count() LOOP
          IF oSelectionList(i).sValue = sSuchWert THEN
            isr$trace.debug ('oSelectionList('||i||').sSelected', oSelectionList(i).sValue, sCurrentName);
            oSelectionList(i).sSelected := 'T';
          END IF;
        END LOOP;
      END IF;
    --***************************************
    ELSE
     oErrorObj := isr$reporttype$package.getLov(sEntity, ssuchwert, oSelectionList);
  END CASE;


  isr$trace.stat('end','end', sCurrentName);
  RETURN oErrorObj;
EXCEPTION
  WHEN OTHERS THEN
     oErrorObj.handleError ( Stb$typedef.cnSeverityMessage,  utd$msglib.getmsg ('ERROR', stb$security.getCurrentLanguage) || SQLCODE, sCurrentName,
       dbms_utility.format_error_stack ||chr(10)||chr(13)|| dbms_utility.format_error_backtrace);
    RETURN oErrorObj; 
END getlov;

--*********************************************************************************************************************************
FUNCTION sendMail( csRecipient in varchar2, 
                   csJobid     in varchar2) RETURN STB$OERROR$RECORD IS
  oErrorObj               STB$OERROR$RECORD       := STB$OERROR$RECORD();
  sCurrentName            constant varchar2(4000) := $$PLSQL_UNIT||'.sendMail('||'csJobid'||')';
  sMsg                    varchar2(4000);

  CURSOR cGetText IS
    SELECT STB$JOB.GETJOBPARAMETER ('TOKEN_DESC', csJobid)
           || (SELECT CASE
                         WHEN TRIM (srnumber) IS NOT NULL THEN ': ' || srnumber
                      END
                      || CASE
                            WHEN TRIM (title) IS NOT NULL THEN ' -> ' || title
                         END
                      || CASE
                            WHEN TRIM (srnumber) IS NOT NULL THEN ' ' || version
                         END
                 FROM stb$report
                WHERE repid = STB$JOB.getjobparameter ('REPID', csJobid))
         , STB$JOB.GETJOBPARAMETER ('MAIL_DESC', csJobid)
      FROM DUAL;

  sSubject VARCHAR(4000);                      
  sText    VARCHAR(4000);
BEGIN
  isr$trace.stat('begin', 'csRecipient: '||csRecipient, sCurrentName);

  FOR rGetServer IN (select host, port, username, password 
                       from isr$server$mail$v   )
  LOOP
    isr$trace.debug('status','send eMail now',sCurrentName, 'server:port --> '||rGetServer.host||':'||rGetServer.port || ' - To: '||csRecipient);
    isr$trace.debug('status','send eMail now',sCurrentName, 'User --> '||rGetServer.username || ' / '|| rGetServer.password);


    BEGIN
      OPEN cGetText;
      FETCH cGetText INTO sSubject, sText;
      CLOSE cGetText;
      oErrorObj := STB$OERROR$RECORD();
      STB$JAVA.sendMail( rGetServer.host
                       , rGetServer.port
                       , rGetServer.username
                       , case when rGetServer.password is not null then STB$JAVA.decryptPassword(rGetServer.password) else null end
                       , csRecipient
                       , ' '
                       , Utd$msglib.GetMsg('MAIL_SUBJECT',Stb$security.GetCurrentLanguage) || ' ' ||sSubject
                       , Utd$msglib.GetMsg('MAIL_TEXT',Stb$security.GetCurrentLanguage) || ' ' ||NVL(sText, sSubject)
                       , csJobid);
      EXIT;
    EXCEPTION WHEN OTHERS THEN
      sMsg :=  utd$msglib.getmsg ('exUnexpectedError', STB$SECURITY.getCurrentLanguage, csP1 => sqlerrm(sqlcode));
      oErrorObj.handleError ( Stb$typedef.cnSeverityCritical, sMsg, sCurrentName, 
                              substr(dbms_utility.format_error_stack || CHR(10) || dbms_utility.format_error_backtrace, 1,4000)  );  
      -- Mailserver not available or another error
      isr$trace.warn('ERROR','email transmission failed',sCurrentName,oErrorObj);
    END;           
  END LOOP;

  isr$trace.stat('end', 'end', sCurrentName);
  RETURN oErrorObj;

EXCEPTION WHEN OTHERS THEN
     oErrorObj.handleError( Stb$typedef.cnSeverityCritical, utd$msglib.getmsg ('exUnexpectedError', stb$security.getCurrentLanguage,  csP1 => sqlerrm(sqlcode)),sCurrentName,
            dbms_utility.format_error_stack ||stb$typedef.crlf|| dbms_utility.format_error_backtrace ); 
  RETURN oErrorObj;

END sendMail;


--***********************************************************************************************************************************
function sendEmail( cnJobid in     number, 
                    cxXml   in out xmltype) 
  return STB$OERROR$RECORD
is
  oErrorObj            STB$OERROR$RECORD := STB$OERROR$RECORD();   -- [MUIB-260]
  sCurrentName         constant varchar2(100) := $$PLSQL_UNIT||'.sendEmail('||cnJobid||')';  
  sRecipients          varchar2(1000);
  sPassword            STB$REPORT.password%type;
  sOldTokenDesc        varchar2(1000);
  sMsg                 varchar2(4000);
begin
  isr$trace.stat('begin','begin',sCurrentName);
  isr$trace.debug('parameter','IN OUT cxXml',sCurrentName,cxXml);  

  sRecipients := STB$UTIL.getReportParameterValue ( 'FINALIZE_EMAIL_RECIPIENT', STB$JOB.getJobParameter ('REPID', cnJobid) ); --STB$UTIL.getSystemParameter('SEND_FINAL_DOC_TO');
  oErrorObj := Stb$docks.getPasswordFromDoc(STB$JOB.getJobParameter('REFERENCE', cnJobid), sPassword);
  sPassword := substr(stb$java.md5_string(sPassword), 1, 8);
  isr$trace.debug('variable','sRecipients: '||sRecipients,sCurrentName);

  sOldTokenDesc := STB$JOB.getJobParameter('TOKEN_DESC',  cnJobid);   

  STB$JOB.setJobParameter('TOKEN_DESC', Utd$msglib.GetMsg('FINAL_DOC_MAIL_SUBJECT',Stb$security.GetCurrentLanguage) , cnJobid);
  STB$JOB.setJobParameter('MAIL_DESC', CHR(13) || CHR(13) || Utd$msglib.GetMsg('FINAL_DOC_MAIL_TEXT',Stb$security.GetCurrentLanguage) || ' ' ||sPassword, cnJobid);

  oErrorObj := sendMail(sRecipients, cnJobId);
  isr$trace.debug('variable','oErrorObj',sCurrentName,oErrorObj);

  if oErrorObj.sSeverityCode = Stb$typedef.cnSeverityCritical then
     isr$trace.debug('Errorcode not null','oErrorObj',sCurrentName,oErrorObj);
     Stb$job.markDBJobAsBroken(cnJobId, sIp, sPort, oErrorObj.sErrorCode, oErrorObj.sErrorMessage, oErrorObj.sSeverityCode);
  end if;

  STB$JOB.setJobParameter('TOKEN_DESC', sOldTokenDesc, cnJobid);

  isr$trace.debug('parameter','IN OUT cxXml',sCurrentName,cxXml);  
  isr$trace.stat('end','end',sCurrentName);     
  return oErrorObj;
exception
  when others then
    sMsg :=  'EMail transfer failed!'|| chr(13) || utd$msglib.getmsg ('exUnexpectedError', nCurrentLanguage, csP1 => sqlerrm(sqlcode));

    oErrorObj.handleError ( Stb$typedef.cnSeverityCritical, sMsg, sCurrentName, 
                            substr(dbms_utility.format_error_stack || CHR(10) || dbms_utility.format_error_backtrace, 1,4000)  );  
    return oErrorObj;   
    Stb$job.markDBJobAsBroken(cnJobId, sIp, sPort, oErrorObj.sErrorCode, oErrorObj.sErrorMessage, oErrorObj.sSeverityCode);
    return oErrorObj;
end sendEmail;

--***********************************************************************************************************************************

function foreCast( cnX    in number, 
                   cnKY   in SYS.ODCINUMBERLIST,
                   cnKX   in SYS.ODCINUMBERLIST)
  return number
is
  oErrorObj     STB$OERROR$RECORD := STB$OERROR$RECORD(); 
  sCurrentName  constant varchar2(100) := $$PLSQL_UNIT||'.foreCast';
  nNr           number(38,6) := 0;      
  nDr           number(38,6) := 0;
  nCountX       number(38) := 0;
  nAverageX     number(38,4) := 0;
  nAverageY     number(38,4) := 0;
  nA            number(38,4) := 0;
  nB            number(38,4) := 0;
begin
  isr$trace.stat('begin','begin',sCurrentName);
  
  select Count(column_value), Avg(column_value)
    into nCountX, nAverageX
    from table(cnKX);

  select Avg(column_value)
    into nAverageY
    from table(cnKY);

  for nIndex in 1..nCountX loop
    nNr := nNr + ( (cnKX(nIndex) - nAverageX) * (cnKY(nIndex) - nAverageY) );
    nDr := nDr + ( (cnKX(nIndex) - nAverageX) * (cnKX(nIndex) - nAverageX) );
  end loop;

  if nDr is null or nDr = 0 then
    return null;
  end if;

  nB := nNr / nDr;
  nA := nAverageY - nB * nAverageX;
  isr$trace.stat('end','end',sCurrentName);

  return nA + nB * cnX;
end foreCast;

--***********************************************************************************************************************

FUNCTION getSSTLimits (csStudyID IN VARCHAR2, csKnownEntity IN VARCHAR2, csSSTMasterKey IN VARCHAR2, cnRepID IN NUMBER, cnAcceptedRunAnalyte IN NUMBER, cnResponseFigures IN NUMBER, cnSSTSDNumber IN NUMBER) RETURN ISR$COVIB$SST$LIMIT$LIST DETERMINISTIC
AS
  -- Calc Placeholders: StudyID, KnownEntity, SSTMasterkey, RepID, AcceptedRunAnalyte, ResponseFigures, SSTSDNumber
  oSSTLimits ISR$COVIB$SST$LIMIT$LIST := ISR$COVIB$SST$LIMIT$LIST();

  CURSOR cSSTLimits IS
    SELECT
      analyteid, name,
      sigFigure(Avg(s1_mean_useresponse), cnResponseFigures) + (cnSSTSDNumber * sigFigure(Stddev(s1_mean_useresponse), cnResponseFigures)) mean_plus_xsd,
      sigFigure(Avg(s1_mean_useresponse), cnResponseFigures) - (cnSSTSDNumber * sigFigure(Stddev(s1_mean_useresponse), cnResponseFigures)) mean_minus_xsd
    FROM (
        SELECT
          studyid, analyteid, rid, runid, nominalconc,
          name, s1_mean_useresponse, s2_mean_useresponse
        FROM (
          SELECT
            sample1.studyid, sample1.analyteid, sample1.rid, sample1.runid, sample1.nominalconc,
            sample1.name, sample1.mean_useresponse s1_mean_useresponse, sample2.mean_useresponse s2_mean_useresponse
          FROM
            (
              SELECT
                studyid, analyteid, rid, runid, nominalconc, name,
                Avg(case when(isdeactivated='F') then response else null end) mean_useresponse
              FROM (
                SELECT
                  r.studyid, s.id sampleid, sr.id sampleresultid, s.designsampleid, s.samplename,
                  case
                    when regexp_like(s.samplename, '^([A-Z]+).*$', 'i') then regexp_replace(s.samplename, '^([A-Z]+).*$', '\1', 1, 1, 'i')
                    else null
                  end name,
                  r.id rid, r.runid, sa.id analyteid, sr.analytearea response, k.concentration nominalconc,
                  case
                    when d.code is not null then 'T'
                    else 'F'
                  end isdeactivated
                FROM 
                  TMP$bio$run r,
                  TMP$bio$run$analytes ra,
                  TMP$bio$study$analytes sa,
                  TMP$bio$assay a,
                  TMP$bio$run$worklist s,
                  TMP$bio$run$worklist$result$rw sr,
                  TMP$bio$run$ana$known k,
                  TMP$bio$covib$run$sample$deact d,
                  isr$crit kwz
                WHERE r.ID = s.runID
                AND s.ID = sr.worklistID
                AND a.ID = r.assayID
                AND r.studyID = csStudyID AND a.studyID = csStudyID
                AND sr.runAnalyteCode = ra.code
                AND r.ID = ra.runID
                AND ra.analyteID = sa.ID
                AND s.code = d.code (+)
                AND k.runAnalyteCode = ra.code AND k.name = s.sampleName
                AND kwz.key = s.samplename AND kwz.entity = csKnownEntity AND kwz.masterkey = csSSTMasterKey AND kwz.repid = cnRepID
                AND (regexp_like(upper(s.samplename), '[ _\-]ND$'))
                AND (ra.RunAnalyteRegressionStatus = cnAcceptedRunAnalyte)
              )
              GROUP BY studyid, analyteid, rid, runid, nominalconc, name
            ) sample1, (
              SELECT
                studyid, analyteid, rid, runid, nominalconc, name,
                Avg(case when(isdeactivated='F') then response else null end) mean_useresponse
              FROM (
                SELECT
                  r.studyid, s.id sampleid, sr.id sampleresultid,
                  s.designsampleid, s.samplename,
                  case
                    when regexp_like(s.samplename, '^([A-Z]+).*$', 'i') then regexp_replace(s.samplename, '^([A-Z]+).*$', '\1', 1, 1, 'i')
                    else null
                  end name,
                  r.id rid, r.runid, sa.id analyteid, sr.analytearea response,
                  k.concentration nominalconc,
                  case
                    when d.code is not null then 'T'
                    else 'F'
                  end isdeactivated
                FROM 
                  TMP$bio$run r,
                  TMP$bio$run$analytes ra,
                  TMP$bio$study$analytes sa,
                  TMP$bio$assay a,
                  TMP$bio$run$worklist s,
                  TMP$bio$run$worklist$result$rw sr,
                  TMP$bio$run$ana$known k,
                  TMP$bio$covib$run$sample$deact d,
                  isr$crit kwz
                WHERE r.ID = s.runID
                AND s.ID = sr.worklistID
                AND a.ID = r.assayID
                AND r.studyID = csStudyID AND a.studyID = csStudyID
                AND sr.runAnalyteCode = ra.code
                AND r.ID = ra.runID
                AND ra.analyteID = sa.ID
                AND s.code = d.code (+)
                AND k.runAnalyteCode = ra.code AND k.name = s.sampleName
                AND kwz.key = s.samplename AND kwz.entity = csKnownEntity AND kwz.masterkey = csSSTMasterKey AND kwz.repid = cnRepID
                AND (regexp_like(upper(s.samplename), '[ _\-]D$'))
                AND (ra.RunAnalyteRegressionStatus = cnAcceptedRunAnalyte)
              )
              GROUP BY studyid, analyteid, rid, runid, nominalconc, name
            ) sample2
          WHERE sample1.studyID = sample2.studyID
          AND sample1.rID = sample2.rID
          AND sample1.runid = sample2.runid
          AND sample1.analyteID = sample2.analyteID
          AND sample1.nominalconc = sample2.nominalconc
          AND sample1.name = sample2.name
          AND (to_char(sample1.runid) not in ( select nvl(trim(column_value),' ') from table(split(stb$util.GetReportParameterValue('EXCLUDED_RUNS',cnRepID))) ))
        )
     )
     GROUP BY studyid, analyteid, name;

BEGIN
  FOR rSSTLimits IN cSSTLimits LOOP
    oSSTLimits.EXTEND;
    oSSTLimits(oSSTLimits.COUNT) := ISR$COVIB$SST$LIMIT$REC(rSSTLimits.analyteid, rSSTLimits.name, rSSTLimits.mean_plus_xsd, rSSTLimits.mean_minus_xsd);
  END LOOP;

  RETURN oSSTLimits;
END;

--***********************************************************************************************************************

FUNCTION BuildDTAFields(csCalcStatmentAlias in varchar2, csDTAType in varchar2) return varchar2 is
  sCurrentName    constant varchar2(100) := $$PLSQL_UNIT||'.BuildDTAFields';
  sFields varchar2(32000);
  sField varchar2(4000);
  sFormat varchar2(4000);
  nRepTypeID      constant number := STB$OBJECT.getCurrentReporttypeId;
  cursor cGetFields is
    select cdf.Name, cdf.Type, cdf.Length, cdf.Mask, cdf.OrderBy, cdf.Field, field.Info FieldValue, type.Info FieldFormat, entry.Info Entry
      from ISR$COVIB$DTA$FIELDS cdf
      join TMP$BIO$DTA$COLUMN dc
        on dc.TargetID = csDTAType
      join ISR$FILE$CALC$VALUES entry
        on entry.Entity = 'EXP$CALC$VALUE'
       and entry.Value = csCalcStatmentAlias
       and entry.reporttypeid = nRepTypeID
       and entry.fileid in (0, ISR$REPORTCALCULATION.cnFileId)
       and entry.Use = 'T'
       and entry.Display = 'ENTRY'
      left outer join ISR$FILE$CALC$VALUES field
        on field.Entity = 'EXP$CALC$FIELD'
       and field.Value = csCalcStatmentAlias
       and field.reporttypeid = nRepTypeID
       and field.fileid in (0, ISR$REPORTCALCULATION.cnFileId)
       and field.Use = 'T'
       and field.Display = cdf.Field
      left outer join ISR$FILE$CALC$VALUES type
        on type.Entity = 'EXP$CALC$FIELD$TYPE'
       and type.Value = csCalcStatmentAlias
       and type.reporttypeid = nRepTypeID
       and type.fileid in (0, ISR$REPORTCALCULATION.cnFileId)
       and type.Use = 'T'
       and type.Display = case when cdf.Field is null then 'DUMMY' else cdf.Type end
     where cdf.Field||'@'||cdf.OrderBy = dc.ColumnCode
       and cdf.DTAID = dc.DTAID
       and (field.Display = cdf.Field or cdf.Field is null)
       and type.Info is not null
     order by to_number(cdf.OrderBy), cdf.Name;
  rGetField cGetFields%rowtype;
BEGIN
  isr$trace.stat('begin', 'begin '||csCalcStatmentAlias||', '||csDTAType, sCurrentName);
  open cGetFields;
  fetch cGetFields into rGetField;
  while cGetFields%found loop
    sFormat := rGetField.FieldFormat;
    sFormat := replace(sFormat,'&&FieldValue&', nvl(rGetField.FieldValue, 'NULL'));
    sFormat := replace(sFormat,'&&LENGTH&', rGetField.Length);
    sFormat := replace(sFormat,'&&MASK&', rGetField.Mask);
    execute immediate 'select ('||sFormat||') from dual' into sFormat;
    sField := rGetField.Entry;
    sField := replace(sField,'&&FieldLabel&', rGetField.Name);
    sField := replace(sField,'&&FieldFormat&', sFormat);
    sField := replace(sField,'&&FieldName&', rGetField.Field||'@'||rGetField.OrderBy);
    sFields := sFields||chr(10)||'      , '||sField||'';
    fetch cGetFields into rGetField;
  end loop;
  close cGetFields;
  --isr$trace.debug('debug','sFields-length: '||length(sFields), sCurrentName);
  --isr$trace.debug('debug', 'see CLOB', sCurrentName, sFields);
  isr$trace.stat('end','end '||csCalcStatmentAlias||', '||csDTAType, sCurrentName);
  return sFields;
exception          
  when others then 
    isr$trace.debug('not evaluated: ('||sFormat||')', SQLERRM, sCurrentName); 
END BuildDTAFields;


--*******************************************************************************************************************
function createAuditFile( cnJobid in     number,
                          cxXml   in out xmltype)
  return STB$OERROR$RECORD
is
  sCurrentName   constant varchar2(100) := $$PLSQL_UNIT||'.createAuditFile (JobID='||cnJobId||')';
  oErrorObj      STB$OERROR$RECORD :=STB$OERROR$RECORD();
  sAuditType     varchar2(500);
  xXml           xmltype;
  xXml1          xmltype;
  cResult        clob;
  sReturn        varchar2(4000);

begin
  isr$trace.stat('begin', 'Function '||sCurrentName||' started.', sCurrentName);

  sIP := STB$JOB.getJobParameter('CLIENT_IP', cnJobid);
  sPort := STB$JOB.getJobParameter('CLIENT_PORT', cnJobid);

  sAuditType := NVL(REPLACE(STB$JOB.getJobParameter('TOKEN', cnJobid), 'CAN_DISPLAY_'),STB$JOB.getJobParameter('NODETYPE', cnJobid));
  xXml := stb$audit.getAudit( sAuditType, STB$JOB.getJobParameter('REFERENCE', cnJobid) ); 
  isr$trace.debug('xXml', 'after getAudit', sCurrentName, xXml);
  isr$XML.setAttribute(xXml, '/*[1]', 'display', STB$JOB.getJobParameter('REFERENCE_DISPLAY', cnJobid));


  select xmlelement ("TRANSLATION"
                   , (select XMLAGG (XMLELEMENT ("TRANSLATIONENTRY", xmlattributes (parametername as "KEY"), parametervalue))
                        from (select distinct parametername, UTD$MSGLIB.getMsg (description, STB$SECURITY.getCurrentLanguage) parametervalue from STB$SYSTEMPARAMETER
                              union
                              select distinct parametername, UTD$MSGLIB.getMsg (description, STB$SECURITY.getCurrentLanguage) parametervalue from STB$REPTYPEPARAMETER
                              union
                              select distinct parametername, UTD$MSGLIB.getMsg (description, STB$SECURITY.getCurrentLanguage) parametervalue from isr$DIALOG)))
    into xXml1
    from DUAL;

  isr$XML.InsertChildXMLFragment(xXml, '/*[1]', isr$XML.XMLToClob(xXml1));

  for rGetAuditStylesheet in ( select stylesheet
                                 from isr$stylesheet
                                where structured = 'R') loop
    sReturn := isr$xml.XSLTTransform(rGetAuditStylesheet.stylesheet, xXml.getClobVal(), cResult, null, cnJobid);
  end loop;
  
  if cResult is not null then
    oErrorObj := STB$DOCKS.saveXmlReport(XMLTYPE(cResult), cnJobid, sAuditType||'_audit.xml');
    if oErrorObj.sSeverityCode != stb$typedef.cnNoError then
      Stb$job.markDBJobAsBroken(cnJobId, sIp, sPort, oErrorObj.sErrorCode, oErrorObj.sErrorMessage, oErrorObj.sSeverityCode);
    end if;
  end if;

  isr$trace.stat('end', 'Function '||sCurrentName||' finished', sCurrentName);
  return oErrorObj;

exception
  when others then
    oErrorObj.handleError ( Stb$typedef.cnSeverityCritical, utd$msglib.getmsg ('exUnexpectedError', stb$security.getCurrentlanguage,  csP1 => sqlerrm(sqlcode)), 
         sCurrentName, dbms_utility.format_error_stack ||chr(10)|| dbms_utility.format_error_backtrace );
    Stb$job.markDBJobAsBroken(cnJobId, sIp, sPort, oErrorObj.sErrorCode, oErrorObj.sErrorMessage, oErrorObj.sSeverityCode);
    return oErrorObj;
end createAuditFile;

end ISR$COVIB$PACKAGE;