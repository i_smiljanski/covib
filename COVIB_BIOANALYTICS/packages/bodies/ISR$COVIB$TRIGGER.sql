CREATE OR REPLACE PACKAGE BODY ISR$COVIB$TRIGGER AS

--==**********************************************************************************************************************************************==--
function FillReportConditions(cnReportTypeID in number, cnRepID in integer) return STB$OERROR$RECORD
is
  oErrorObj                STB$OERROR$RECORD := STB$OERROR$RECORD();
  sMsg                     VARCHAR2(2000);
  sCurrentName    CONSTANT VARCHAR2(100) := $$PLSQL_UNIT||'.FillReportConditions('||cnRepID||')';

  lConditionsList          isr$varchar$varray := isr$varchar$varray(); 
  sConditionText           varchar2(4000);
begin
  isr$trace.stat('begin', 'begin', sCurrentName);
   
  sConditionText := stb$util.GetReportParameterValue('RUNANALYTEREGRESSIONSTATES', cnRepid);

  isr$trace.debug('sConditionText for RUNANALYTEREGRESSIONSTATUS', sConditionText, sCurrentName);
  if sConditionText is not null then
    ISR$Entity$Conditions.AddEntityCondition('BIO$RUN$ANALYTES','RUNANALYTEREGRESSIONSTATUS', sConditionText);
    --ISR$Entity$Conditions.AddEntityCondition('BIO$RUN','RUNANALYTEREGRESSIONSTATUS', lConditionsList);  
  else
    select value 
    bulk collect into lConditionsList
    from ISR$PARAMETER$VALUES
    where entity = 'BIO$RUNANALYTEREGRESSIONSTATUS';
    if lConditionsList.count > 0 then
      ISR$Entity$Conditions.AddEntityCondition('BIO$RUN$ANALYTES','RUNANALYTEREGRESSIONSTATUS', lConditionsList);
    end if;
  end if;  

  lConditionsList := isr$varchar$varray();
  select value 
  bulk collect into lConditionsList
  from ISR$PARAMETER$VALUES
  where entity = 'BIO$STUDY$CONFIG';
  isr$trace.debug('lConditionsList.count', lConditionsList.count, sCurrentName);
  if lConditionsList.count > 0 then
    ISR$Entity$Conditions.AddEntityCondition('BIO$STUDY$CONFIG','FIELDNAME', lConditionsList);    
  end if;

  isr$trace.stat('end', 'end', sCurrentName);
  return oErrorObj;
eND FillReportConditions;

function  ProcessPre_BIO$USERSEC(coParamList IN iSR$ParamList$Rec) return STB$oError$Record is

  sCurrentName    constant varchar2(100) := $$PLSQL_UNIT||'.ProcessPost_BIO$USERSEC';
  oErrorObj                STB$OERROR$RECORD := STB$OERROR$RECORD();
  sMsg                     VARCHAR2(2000);
  
  sLimsUser                varchar2(100);
  cursor cUser is 
    select limsusername 
    from STB$USER 
    where userno = STB$SECURITY.getCurrentUser;
  
begin
  isr$trace.stat('begin', 'begin', sCurrentName);
  
  open cUser;
  fetch cUser into sLimsUser;
  close cUser;
  -- make sure that no sec entries are found if limsusername is not set for the current user
  sLimsUser := nvl(sLimsUser,'!! no user !!');
  isr$trace.debug('limsuser', sLimsUser, sCurrentName);
      
  ISR$Entity$Conditions.AddEntityCondition('BIO$USERSEC','LOGINNAME', sLimsUser);              
  
  isr$trace.stat('end', 'end', sCurrentName);  
  return oErrorObj;
EXCEPTION
  WHEN OTHERS THEN
    sMsg :=  utd$msglib.getmsg ('exUnexpectedError', stb$security.getcurrentlanguage, csP1 => sqlerrm(sqlcode));
    oErrorObj. handleError (Stb$typedef.cnSeverityCritical, sMsg, sCurrentName, 
                            dbms_utility.format_error_stack || CHR(10) || dbms_utility.format_error_backtrace);   
    RETURN oErrorObj;    
end ProcessPre_BIO$USERSEC;    

function  ProcessPre_BIO$STUDY(coParamList IN iSR$ParamList$Rec) return STB$oError$Record is

  sCurrentName    constant varchar2(100) := $$PLSQL_UNIT||'.ProcessPost_BIO$STUDY';
  oErrorObj                STB$OERROR$RECORD := STB$OERROR$RECORD();
  sMsg                     VARCHAR2(2000);
  sInWizard       constant varchar2(50)  := coParamList.GetParamStr('INWIZARD');
  
  lConditionsList          isr$varchar$varray := isr$varchar$varray();   
  cursor cKeys is
    select RIGHTSKEY
    from ISR$REMOTE$SECURITY$KEYS 
    where USERNO = STB$SECURITY.getCurrentUser
      and SESSIONID = STB$SECURITY.getCurrentSession;
begin
  isr$trace.stat('begin', 'begin', sCurrentName);
  if sInWizard = 'T' then 
    -- only in wizard and only when lims security is active
    if STB$SECURITY.checkGroupRight('CAN_OVERRULE_REMOTE_SECURITY_CHECK') = 'F' and STB$UTIL.getSystemparameter('USE_REMOTE_SECURITY_PRIVS') = 'T' then
      
      open cKeys;
      fetch cKeys bulk collect into lConditionsList;
      close cKeys;
      isr$trace.debug('lConditionsList.count', lConditionsList.count, sCurrentName);
      if lConditionsList.count > 0 then
        ISR$Entity$Conditions.AddEntityCondition('BIO$STUDY','CODE', lConditionsList);
      else
        -- make sure that no studies are retrieved if user has no access to any study
        ISR$Entity$Conditions.AddEntityCondition('BIO$STUDY','CODE', -99999);  
      end if;
          
    end if;
  
  end if;
  
  isr$trace.stat('end', 'end', sCurrentName);  
  return oErrorObj;
EXCEPTION
  WHEN OTHERS THEN
    sMsg :=  utd$msglib.getmsg ('exUnexpectedError', stb$security.getcurrentlanguage, csP1 => sqlerrm(sqlcode));
    oErrorObj. handleError (Stb$typedef.cnSeverityCritical, sMsg, sCurrentName, 
                            dbms_utility.format_error_stack || CHR(10) || dbms_utility.format_error_backtrace);   
    RETURN oErrorObj;    
end ProcessPre_BIO$STUDY;
  

function ProcessPost_BIO$RUN$ANALYTES(coParamList IN iSR$ParamList$Rec) return STB$oError$Record is

  sCurrentName    constant varchar2(100) := $$PLSQL_UNIT||'.ProcessPost_BIO$RUN$ANALYTES';
  oErrorObj                STB$OERROR$RECORD := STB$OERROR$RECORD();
  sMsg                     VARCHAR2(2000);
  nRepID                   STB$REPORT.REPID%TYPE;

begin
  isr$trace.stat('begin', 'begin', sCurrentName);
  
  nRepID := STB$OBJECT.getCurrentRepid;
  isr$trace.debug('nRepID', nRepID, sCurrentName);

  delete from tmp$bio$run
  where code in
   (select code from tmp$bio$run
    minus 
    select distinct runcode from tmp$bio$run$analytes);
  isr$trace.debug('run rows deleted?', 'deleted count: '||sql%rowcount, sCurrentName);
  for rParameterVals in (SELECT c1.MASTERKEY RunAnalyteCode, c1.key RunAnalyteStatusID
                           FROM ISR$CRIT c1
                          WHERE c1.entity = 'BIO$COVIB$RUN$ACCEPTANCE'
                            AND c1.repid = nRepID) loop
    update TMP$BIO$RUN$ANALYTES
    set RUNANALYTEREGRESSIONSTATUS = rParameterVals.RunAnalyteStatusID
    where code = rParameterVals.RunAnalyteCode;
  end loop;

  isr$trace.stat('end', 'end', sCurrentName);  
  return oErrorObj;
EXCEPTION
  WHEN OTHERS THEN
    sMsg :=  utd$msglib.getmsg ('exUnexpectedError', stb$security.getcurrentlanguage, csP1 => sqlerrm(sqlcode));
    oErrorObj. handleError (Stb$typedef.cnSeverityCritical, sMsg, sCurrentName, 'SQLCODE = '||SQLCODE||', SQLERRM = '||SQLERRM||' '||dbms_utility.format_error_backtrace);   
    RETURN oErrorObj;    
end ProcessPost_BIO$RUN$ANALYTES;

function ProcessPost_BIO$RUN$ANA$KNOWN(coParamList IN iSR$ParamList$Rec) return STB$oError$Record is

  sCurrentName    constant varchar2(100) := $$PLSQL_UNIT||'.ProcessPost_BIO$RUN$ANA$KNOWN';
  sEntity         constant varchar2(50)  := coParamList.GetParamStr('ENTITY');
  oErrorObj                STB$OERROR$RECORD := STB$OERROR$RECORD();
  sMsg                     VARCHAR2(2000);

begin
  isr$trace.stat('begin', 'begin sEntity: '||sEntity, sCurrentName);

  isr$trace.stat('end', 'end', sCurrentName);  
  return oErrorObj;
EXCEPTION
  WHEN OTHERS THEN
    sMsg :=  utd$msglib.getmsg ('exUnexpectedError', stb$security.getcurrentlanguage, csP1 => sqlerrm(sqlcode));
    oErrorObj. handleError (Stb$typedef.cnSeverityCritical, sMsg, sCurrentName, 'SQLCODE = '||SQLCODE||', SQLERRM = '||SQLERRM||' '||dbms_utility.format_error_backtrace);   
    RETURN oErrorObj;    
end ProcessPost_BIO$RUN$ANA$KNOWN;

function ProcessPost_BIO$DES$FIELDCHNG(coParamList IN iSR$ParamList$Rec) return STB$oError$Record is

  sCurrentName    constant varchar2(100) := $$PLSQL_UNIT||'.ProcessPost_BIO$DES$FIELDCHNG';
  sEntity         constant varchar2(50)  := coParamList.GetParamStr('ENTITY');
  oErrorObj                STB$OERROR$RECORD := STB$OERROR$RECORD();
  sMsg                     VARCHAR2(2000);
  oParamList               iSR$ParamList$Rec :=  iSR$ParamList$Rec();
  lConditionsList          isr$varchar$varray := isr$varchar$varray(); 
  sStatus                  varchar2(50);
  nCountChanges            integer;
  sIsrValue                varchar2(200);
  sIsrValue2               varchar2(200);
  sIsrValue3               varchar2(200);
  sIsrValue4               varchar2(200);
  sIsrValue5               varchar2(200);
  sIsrValue6               varchar2(200);
  nCounter                 integer := 0;
  nEntries                 number;
  cursor cStatus is
    select status from tmp$BIO$DESIGN$STATUS
    where studycode in (select key 
                        from isr$crit 
                        where entity = 'BIO$STUDY' and repid = STB$OBJECT.getCurrentRepid);
--BIO$DESIGN$FIELDCHANGES
begin
  isr$trace.stat('begin', 'begin sEntity: '||sEntity, sCurrentName);
  
  delete from tmp$BIO$DESIGN$STATUS;
  oParamList.AddParam('REPORTTYPEID', STB$OBJECT.getCurrentReporttypeId);
  oParamList.AddParam('REPID', STB$OBJECT.getCurrentRepid);
  oParamList.AddParam('ENTITY','BIO$DESIGN$STATUS');
  
  select key 
    bulk collect into lConditionsList 
  from isr$crit 
  where entity = 'BIO$STUDY' and repid = STB$OBJECT.getCurrentRepid;
  ISR$Entity$Conditions.AddEntityCondition('BIO$DESIGN$STATUS','STUDYCODE', lConditionsList);    
  oErrorObj := ISR$REMOTE$COLLECTOR.fillTmpTableForEntity ('BIO$DESIGN$STATUS', oParamList, 'F');
  
  open cStatus;
  fetch cStatus into sStatus;
  -- continue only if an entry is found
  if cStatus%found then
    -- ...
    if sStatus = 'DRAFT' then
      -- published data may exist
      select count(1) into nCountChanges
      from tmp$BIO$DESIGNSAMPLE$CHANGES;
      if nCountChanges > 0 then
        update tmp$BIO$DESIGN$STATUS
        set has_draft_data = 'T';
      end if;
    else  
      update tmp$BIO$DESIGN$STATUS
      set has_draft_data = 'F';
    end if;
  end if;
  close cStatus;
  
  for rChangesRow in (select studycode, designsampleid, usersampleid, 
                             designsubjectid_org, designsubjectid, designsubjecttag_org, 
                             STUDYSUBJECTTAG_ORG, GENDERID_ORG,                         
                             designsubjecttreatmentkey_org, designsubjecttreatmentkey, subjectgroupname_org,
                             VISIT_ORG, VISIT, PERIOD_ORG, PERIOD, WEEK_ORG, WEEK, STUDYDAY_ORG, STUDYDAY,                    
                             treatmentkey_org, treatmentkey, treatmentdesc_org, TREATMENTID_ORG,                           
                             endday_org, endday, endhour_org, endhour, 
                             endminute_org, endminute, startday_org, startday,                              
                             starthour_org, starthour, startminute_org, startminute,                              
                             samplestatus_org, samplestatus, samplestatusname_org,
                             sampletextvariable1_org, sampletextvariable1, 
                             sampletextvariable2_org, sampletextvariable2, 
                             sampletextvariable3_org, sampletextvariable3, 
                             splitnumber_org, splitnumber, timetext_org, timetext,
                             sampletypekey_org, sampletypekey, sampletypeid_org
                      from tmp$BIO$DESIGNSAMPLE$CHANGES) loop
    if rChangesRow.designsubjectid is not null then
      nCounter := nCounter + 1;
      sIsrValue := null;
      -- use for loop to avoid instead of excepoption handling
      for rIsrVal in (select designsubjecttag, STUDYSUBJECTTAG, GENDERID
                          from tmp$bio$designsample 
                          where designsubjectid = rChangesRow.designsubjectid) loop
        sIsrValue := rIsrVal.designsubjecttag;
        sIsrValue2 := rIsrVal.STUDYSUBJECTTAG;
        sIsrValue3 := rIsrVal.GENDERID;
        exit;                 
      end loop;
      INSERT INTO TMP$BIO$DESIGN$FIELDCHANGES (USERSAMPLEID, FIELDID, FIELDNAME, WATSON_ID, WATSON_VALUE, 
                                             ISR_ID, ISR_VALUE, SERVERID, 
                                             CRITORDERBY, ENTITYORDERBY) 
      VALUES (rChangesRow.usersampleid, 'DESIGNSUBJECTID', 'DESIGNSUBJECTTAG', rChangesRow.designsubjectid_org, rChangesRow.designsubjecttag_org,
              rChangesRow.designsubjectid, sIsrValue, null, null, nCounter);
      if nvl(rChangesRow.STUDYSUBJECTTAG_org,'##notexthere##') <> nvl(sIsrValue2,'##notexthere##') then
        -- soething has been changed
        nCounter := nCounter + 1;
        INSERT INTO TMP$BIO$DESIGN$FIELDCHANGES (USERSAMPLEID, FIELDID, FIELDNAME, WATSON_ID, WATSON_VALUE, 
                                               ISR_ID, ISR_VALUE, SERVERID, 
                                               CRITORDERBY, ENTITYORDERBY) 
        VALUES (rChangesRow.usersampleid, 'DESIGNSUBJECTID', 'STUDYSUBJECTTAG', rChangesRow.designsubjectid_org, rChangesRow.STUDYSUBJECTTAG_org,
                rChangesRow.designsubjectid, sIsrValue2, null, null, nCounter);
      end if;
      if nvl(to_char(rChangesRow.GENDERID_org),'##notexthere##') <> nvl(sIsrValue3,'##notexthere##') then
        -- soething has been changed
        nCounter := nCounter + 1;
        INSERT INTO TMP$BIO$DESIGN$FIELDCHANGES (USERSAMPLEID, FIELDID, FIELDNAME, WATSON_ID, WATSON_VALUE, 
                                               ISR_ID, ISR_VALUE, SERVERID, 
                                               CRITORDERBY, ENTITYORDERBY) 
        VALUES (rChangesRow.usersampleid, 'DESIGNSUBJECTID', 'GENDERID', rChangesRow.designsubjectid_org, rChangesRow.GENDERID_org,
                rChangesRow.designsubjectid, sIsrValue3, null, null, nCounter);
      end if;
    end if;
    if rChangesRow.designsubjecttreatmentkey is not null then
      nCounter := nCounter + 1;
      sIsrValue := null;
      -- use for loop to avoid excepoption handling
      for rIsrVal in (select treatmentdesc, SUBJECTGROUPNAME, VISIT, PERIOD, WEEK, STUDYDAY  
                          from tmp$bio$designsample 
                          where designsubjecttreatmentkey = rChangesRow.designsubjecttreatmentkey) loop
        sIsrValue :=  rIsrVal.treatmentdesc;
        sIsrValue2 :=  rIsrVal.SUBJECTGROUPNAME;
        sIsrValue3 :=  rIsrVal.VISIT;
        sIsrValue4 :=  rIsrVal.PERIOD;
        sIsrValue5 :=  rIsrVal.WEEK;
        sIsrValue6 :=  rIsrVal.STUDYDAY;
        exit;                 
      end loop;
      INSERT INTO TMP$BIO$DESIGN$FIELDCHANGES (USERSAMPLEID, FIELDID, FIELDNAME, WATSON_ID, WATSON_VALUE, 
                                             ISR_ID, ISR_VALUE, SERVERID, 
                                             CRITORDERBY, ENTITYORDERBY) 
      VALUES (rChangesRow.usersampleid, 'DESIGNSUBJECTTREATMENTKEY', 'TREATMENTDESC', rChangesRow.designsubjecttreatmentkey_org, rChangesRow.treatmentdesc_org,
              rChangesRow.designsubjecttreatmentkey, sIsrValue, null, null, nCounter);
      if nvl(rChangesRow.SUBJECTGROUPNAME_org,'##notexthere##') <> nvl(sIsrValue2,'##notexthere##') then
        -- soething has been changed
        nCounter := nCounter + 1;
        INSERT INTO TMP$BIO$DESIGN$FIELDCHANGES (USERSAMPLEID, FIELDID, FIELDNAME, WATSON_ID, WATSON_VALUE, 
                                               ISR_ID, ISR_VALUE, SERVERID, 
                                               CRITORDERBY, ENTITYORDERBY) 
        VALUES (rChangesRow.usersampleid, 'DESIGNSUBJECTTREATMENTKEY', 'SUBJECTGROUPNAME', rChangesRow.designsubjecttreatmentkey_org, rChangesRow.SUBJECTGROUPNAME_org,
                rChangesRow.designsubjecttreatmentkey, sIsrValue2, null, null, nCounter);
      end if;
      if nvl(to_char(rChangesRow.VISIT_org),'##notexthere##') <> nvl(sIsrValue3,'##notexthere##') then
        -- soething has been changed
        nCounter := nCounter + 1;
        INSERT INTO TMP$BIO$DESIGN$FIELDCHANGES (USERSAMPLEID, FIELDID, FIELDNAME, WATSON_ID, WATSON_VALUE, 
                                               ISR_ID, ISR_VALUE, SERVERID, 
                                               CRITORDERBY, ENTITYORDERBY) 
        VALUES (rChangesRow.usersampleid, 'DESIGNSUBJECTTREATMENTKEY', 'VISIT', rChangesRow.designsubjecttreatmentkey_org, rChangesRow.VISIT_org,
                rChangesRow.designsubjecttreatmentkey, sIsrValue3, null, null, nCounter);
      end if;
      if nvl(to_char(rChangesRow.PERIOD_org),'##notexthere##') <> nvl(sIsrValue4,'##notexthere##') then
        -- soething has been changed
        nCounter := nCounter + 1;
        INSERT INTO TMP$BIO$DESIGN$FIELDCHANGES (USERSAMPLEID, FIELDID, FIELDNAME, WATSON_ID, WATSON_VALUE, 
                                               ISR_ID, ISR_VALUE, SERVERID, 
                                               CRITORDERBY, ENTITYORDERBY) 
        VALUES (rChangesRow.usersampleid, 'DESIGNSUBJECTTREATMENTKEY', 'PERIOD', rChangesRow.designsubjecttreatmentkey_org, rChangesRow.PERIOD_org,
                rChangesRow.designsubjecttreatmentkey, sIsrValue4, null, null, nCounter);
      end if;      
      if nvl(to_char(rChangesRow.WEEK_org),'##notexthere##') <> nvl(sIsrValue5,'##notexthere##') then
        -- soething has been changed
        nCounter := nCounter + 1;
        INSERT INTO TMP$BIO$DESIGN$FIELDCHANGES (USERSAMPLEID, FIELDID, FIELDNAME, WATSON_ID, WATSON_VALUE, 
                                               ISR_ID, ISR_VALUE, SERVERID, 
                                               CRITORDERBY, ENTITYORDERBY) 
        VALUES (rChangesRow.usersampleid, 'DESIGNSUBJECTTREATMENTKEY', 'WEEK', rChangesRow.designsubjecttreatmentkey_org, rChangesRow.WEEK_org,
                rChangesRow.designsubjecttreatmentkey, sIsrValue5, null, null, nCounter);
      end if;
      if nvl(to_char(rChangesRow.STUDYDAY_org),'##notexthere##') <> nvl(sIsrValue6,'##notexthere##') then
        -- soething has been changed
        nCounter := nCounter + 1;
        INSERT INTO TMP$BIO$DESIGN$FIELDCHANGES (USERSAMPLEID, FIELDID, FIELDNAME, WATSON_ID, WATSON_VALUE, 
                                               ISR_ID, ISR_VALUE, SERVERID, 
                                               CRITORDERBY, ENTITYORDERBY) 
        VALUES (rChangesRow.usersampleid, 'DESIGNSUBJECTTREATMENTKEY', 'STUDYDAY', rChangesRow.designsubjecttreatmentkey_org, rChangesRow.STUDYDAY_org,
                rChangesRow.designsubjecttreatmentkey, sIsrValue6, null, null, nCounter);
      end if;
    end if;
    
    if rChangesRow.treatmentkey is not null then
      nCounter := nCounter + 1;
      sIsrValue := null;
      -- use for loop to avoid excepoption handling
      for rIsrVal in (select treatmentid
                          from tmp$bio$designsample 
                          where treatmentkey = rChangesRow.treatmentkey) loop
        sIsrValue :=  rIsrVal.treatmentid;
        exit;                 
      end loop;
      INSERT INTO TMP$BIO$DESIGN$FIELDCHANGES (USERSAMPLEID, FIELDID, FIELDNAME, WATSON_ID, WATSON_VALUE, 
                                             ISR_ID, ISR_VALUE, SERVERID, 
                                             CRITORDERBY, ENTITYORDERBY) 
      VALUES (rChangesRow.usersampleid, 'TREATMENTKEY', 'TREATMENTID', rChangesRow.treatmentkey_org, rChangesRow.treatmentid_org,
              rChangesRow.treatmentkey, sIsrValue, null, null, nCounter);
    end if; 
    
       
    if rChangesRow.endday is not null then
      nCounter := nCounter + 1;      
      INSERT INTO TMP$BIO$DESIGN$FIELDCHANGES (USERSAMPLEID, FIELDID, FIELDNAME, WATSON_ID, WATSON_VALUE, 
                                             ISR_ID, ISR_VALUE, SERVERID, 
                                             CRITORDERBY, ENTITYORDERBY) 
      VALUES (rChangesRow.usersampleid, null, 'ENDDAY', null, rChangesRow.endday_org,
              null, rChangesRow.endday, null, null, nCounter);
    end if;
    if rChangesRow.endhour is not null then
      nCounter := nCounter + 1;      
      INSERT INTO TMP$BIO$DESIGN$FIELDCHANGES (USERSAMPLEID, FIELDID, FIELDNAME, WATSON_ID, WATSON_VALUE, 
                                             ISR_ID, ISR_VALUE, SERVERID, 
                                             CRITORDERBY, ENTITYORDERBY) 
      VALUES (rChangesRow.usersampleid, null, 'ENDHOUR', null, rChangesRow.endhour_org,
              null, rChangesRow.endhour, null, null, nCounter);
    end if;
    if rChangesRow.endminute is not null then
      nCounter := nCounter + 1;      
      INSERT INTO TMP$BIO$DESIGN$FIELDCHANGES (USERSAMPLEID, FIELDID, FIELDNAME, WATSON_ID, WATSON_VALUE, 
                                             ISR_ID, ISR_VALUE, SERVERID, 
                                             CRITORDERBY, ENTITYORDERBY) 
      VALUES (rChangesRow.usersampleid, null, 'ENDMINUTE', null, rChangesRow.endminute_org,
              null, rChangesRow.endminute, null, null, nCounter);
    end if;
    if rChangesRow.startday is not null then
      nCounter := nCounter + 1;      
      INSERT INTO TMP$BIO$DESIGN$FIELDCHANGES (USERSAMPLEID, FIELDID, FIELDNAME, WATSON_ID, WATSON_VALUE, 
                                             ISR_ID, ISR_VALUE, SERVERID, 
                                             CRITORDERBY, ENTITYORDERBY) 
      VALUES (rChangesRow.usersampleid, null, 'STARTDAY', null, rChangesRow.startday_org,
              null, rChangesRow.startday, null, null, nCounter);
    end if;    
    if rChangesRow.starthour is not null then
      nCounter := nCounter + 1;      
      INSERT INTO TMP$BIO$DESIGN$FIELDCHANGES (USERSAMPLEID, FIELDID, FIELDNAME, WATSON_ID, WATSON_VALUE, 
                                             ISR_ID, ISR_VALUE, SERVERID, 
                                             CRITORDERBY, ENTITYORDERBY) 
      VALUES (rChangesRow.usersampleid, null, 'STARTHOUR', null, rChangesRow.starthour_org,
              null, rChangesRow.starthour, null, null, nCounter);
    end if;
    if rChangesRow.startminute is not null then
      nCounter := nCounter + 1;      
      INSERT INTO TMP$BIO$DESIGN$FIELDCHANGES (USERSAMPLEID, FIELDID, FIELDNAME, WATSON_ID, WATSON_VALUE, 
                                             ISR_ID, ISR_VALUE, SERVERID, 
                                             CRITORDERBY, ENTITYORDERBY) 
      VALUES (rChangesRow.usersampleid, null, 'STARTMINUTE', null, rChangesRow.startminute_org,
              null, rChangesRow.startminute, null, null, nCounter);
    end if;
    if rChangesRow.samplestatus is not null then
      nCounter := nCounter + 1;
      select count(1) into nEntries from tmp$bio$samplestatus;
      if nEntries = 0 then
        oParamList.AddParam('ENTITY','BIO$SAMPLESTATUS'); --replace setting
        oErrorObj := ISR$REMOTE$COLLECTOR.fillTmpTableForEntity ('BIO$SAMPLESTATUS', oParamList, 'F');
      end if; 
      -- use for loop to avoid excepoption handling
      for rIsrVal in (select name 
                        from tmp$bio$samplestatus 
                        where code = rChangesRow.samplestatus) loop
        sIsrValue :=  rIsrVal.name;
        exit;                 
      end loop;
      if nEntries = 0 then
        -- if the table was empty then leave it empty
        delete from tmp$bio$samplestatus;
      end if;       
      INSERT INTO TMP$BIO$DESIGN$FIELDCHANGES (USERSAMPLEID, FIELDID, FIELDNAME, WATSON_ID, WATSON_VALUE, 
                                             ISR_ID, ISR_VALUE, SERVERID, 
                                             CRITORDERBY, ENTITYORDERBY) 
      VALUES (rChangesRow.usersampleid, 'SAMPLESTATUS', 'SAMPLESTATUSNAME', rChangesRow.samplestatus_org, rChangesRow.samplestatusname_org,
              rChangesRow.samplestatus, sIsrValue, null, null, nCounter);
    end if;
    if rChangesRow.sampletextvariable1 is not null then
      nCounter := nCounter + 1;      
      INSERT INTO TMP$BIO$DESIGN$FIELDCHANGES (USERSAMPLEID, FIELDID, FIELDNAME, WATSON_ID, WATSON_VALUE, 
                                             ISR_ID, ISR_VALUE, SERVERID, 
                                             CRITORDERBY, ENTITYORDERBY) 
      VALUES (rChangesRow.usersampleid, null, 'SAMPLETEXTVARIABLE1', null, rChangesRow.sampletextvariable1_org,
              null, rChangesRow.sampletextvariable1, null, null, nCounter);
    end if;
    if rChangesRow.sampletextvariable2 is not null then
      nCounter := nCounter + 1;      
      INSERT INTO TMP$BIO$DESIGN$FIELDCHANGES (USERSAMPLEID, FIELDID, FIELDNAME, WATSON_ID, WATSON_VALUE, 
                                             ISR_ID, ISR_VALUE, SERVERID, 
                                             CRITORDERBY, ENTITYORDERBY) 
      VALUES (rChangesRow.usersampleid, null, 'SAMPLETEXTVARIABLE2', null, rChangesRow.sampletextvariable2_org,
              null, rChangesRow.sampletextvariable2, null, null, nCounter);
    end if;
    if rChangesRow.sampletextvariable3 is not null then
      nCounter := nCounter + 1;      
      INSERT INTO TMP$BIO$DESIGN$FIELDCHANGES (USERSAMPLEID, FIELDID, FIELDNAME, WATSON_ID, WATSON_VALUE, 
                                             ISR_ID, ISR_VALUE, SERVERID, 
                                             CRITORDERBY, ENTITYORDERBY) 
      VALUES (rChangesRow.usersampleid, null, 'SAMPLETEXTVARIABLE3', null, rChangesRow.sampletextvariable3_org,
              null, rChangesRow.sampletextvariable3, null, null, nCounter);
    end if;    
    if rChangesRow.splitnumber is not null then
      nCounter := nCounter + 1;      
      INSERT INTO TMP$BIO$DESIGN$FIELDCHANGES (USERSAMPLEID, FIELDID, FIELDNAME, WATSON_ID, WATSON_VALUE, 
                                             ISR_ID, ISR_VALUE, SERVERID, 
                                             CRITORDERBY, ENTITYORDERBY) 
      VALUES (rChangesRow.usersampleid, null, 'SPLITNUMBER', null, rChangesRow.splitnumber_org,
              null, rChangesRow.splitnumber, null, null, nCounter);
    end if;
    if rChangesRow.timetext is not null then
      nCounter := nCounter + 1;      
      INSERT INTO TMP$BIO$DESIGN$FIELDCHANGES (USERSAMPLEID, FIELDID, FIELDNAME, WATSON_ID, WATSON_VALUE, 
                                             ISR_ID, ISR_VALUE, SERVERID, 
                                             CRITORDERBY, ENTITYORDERBY) 
      VALUES (rChangesRow.usersampleid, null, 'TIMETEXT', null, rChangesRow.timetext_org,
              null, rChangesRow.timetext, null, null, nCounter);
    end if;
    if rChangesRow.sampletypekey is not null then
      nCounter := nCounter + 1;
      sIsrValue := null;
      -- use for loop to avoid excepoption handling
      for rIsrVal in (select sampletypeid 
                          from tmp$bio$designsample 
                          where sampletypekey = rChangesRow.sampletypekey) loop
        sIsrValue :=  rIsrVal.sampletypeid;
        exit;                 
      end loop;
      INSERT INTO TMP$BIO$DESIGN$FIELDCHANGES (USERSAMPLEID, FIELDID, FIELDNAME, WATSON_ID, WATSON_VALUE, 
                                             ISR_ID, ISR_VALUE, SERVERID, 
                                             CRITORDERBY, ENTITYORDERBY) 
      VALUES (rChangesRow.usersampleid, 'SAMPLETYPEKEY', 'SAMPLETYPEID', rChangesRow.sampletypekey_org, rChangesRow.sampletypeid_org,
              rChangesRow.sampletypekey, sIsrValue, null, null, nCounter);
    end if; 
  end loop;

  isr$trace.stat('end', 'end', sCurrentName);  
  return oErrorObj;
EXCEPTION
  WHEN OTHERS THEN
    sMsg :=  utd$msglib.getmsg ('exUnexpectedError', stb$security.getcurrentlanguage, csP1 => sqlerrm(sqlcode));
    oErrorObj. handleError (Stb$typedef.cnSeverityCritical, sMsg, sCurrentName, 'SQLCODE = '||SQLCODE||', SQLERRM = '||SQLERRM||' '||dbms_utility.format_error_backtrace);   
    RETURN oErrorObj;    
end ProcessPost_BIO$DES$FIELDCHNG;




function ProcessPost_BIO$SAMPLESTATUS(coParamList IN iSR$ParamList$Rec) return STB$oError$Record is

  sCurrentName    constant varchar2(100) := $$PLSQL_UNIT||'.ProcessPost_BIO$RUN$ANA$KNOWN';
  oErrorObj                STB$OERROR$RECORD := STB$OERROR$RECORD();
  sMsg                     VARCHAR2(2000);

begin
  isr$trace.stat('begin', 'begin', sCurrentName);

  for rParameterVals in (select value, display, info
                         from ISR$PARAMETER$VALUES
                         where entity = 'BIO$SAMPLESTATUS') loop
    update TMP$BIO$SAMPLESTATUS
    set name = rParameterVals.display,
        info = rParameterVals.info
    where code = rParameterVals.value;
  end loop;

  isr$trace.stat('end', 'end', sCurrentName);  
  return oErrorObj;
EXCEPTION
  WHEN OTHERS THEN
    sMsg :=  utd$msglib.getmsg ('exUnexpectedError', stb$security.getcurrentlanguage, csP1 => sqlerrm(sqlcode));
    oErrorObj. handleError (Stb$typedef.cnSeverityCritical, sMsg, sCurrentName, 'SQLCODE = '||SQLCODE||', SQLERRM = '||SQLERRM||' '||dbms_utility.format_error_backtrace);   
    RETURN oErrorObj;    
end ProcessPost_BIO$SAMPLESTATUS;


--***************************************************************************************************  
function ProcessPost_BIO$REASSAY$HIST(coParamList IN iSR$ParamList$Rec) return STB$oError$Record is

  sCurrentName    constant varchar2(100) := $$PLSQL_UNIT||'.ProcessPost_WT$REASSAY$HISTORY';
  oErrorObj                STB$OERROR$RECORD := STB$OERROR$RECORD();
  sMsg                     VARCHAR2(2000);

  nCount number;
  nCounter number;

begin
  isr$trace.stat('begin', 'begin', sCurrentName);

  /**/
  if STB$OBJECT.getCurrentReporttypeId in (10700, 10730) then
    for rHistoryGroup in (select STUDYCODE, SUBJECTCODE, 
                                 -- for COVIB-112
                                 --BIOMATRIXSAMPLINGCODE, 
                                 SPECIMENKEY,
                                 TREATMENTCODE, ANALYTECODE, SAMPLINGTIME, DESIGNSUBJECTTAG,  
                                 --ASSAYDATETIME, RUNSAMPLESEQUENCENUMBER, RUNID                                
                                 min(nvl(assaydatetime,to_date('01.01.1900','DD.MM.YYYY'))) min_assaydatetime
                            from tmp$BIO$REASSAY$HISTORY
                            group by STUDYCODE, SUBJECTCODE, 
                                     -- for COVIB-112
                                     --BIOMATRIXSAMPLINGCODE, 
                                     SPECIMENKEY,
                                     TREATMENTCODE, ANALYTECODE, SAMPLINGTIME, DESIGNSUBJECTTAG) loop
      select count(1) into nCount
        from tmp$BIO$REASSAY$HISTORY
       where ASSAYDATETIME = rHistoryGroup.min_assaydatetime -- true only for real assaydatetime, not for nulls alias 1900
         and STUDYCODE = rHistoryGroup.STUDYCODE
         and SUBJECTCODE = rHistoryGroup.SUBJECTCODE
         -- for COVIB-112
         --and BIOMATRIXSAMPLINGCODE = rHistoryGroup.BIOMATRIXSAMPLINGCODE
         and SPECIMENKEY = rHistoryGroup.SPECIMENKEY
         and TREATMENTCODE = rHistoryGroup.TREATMENTCODE
         and ANALYTECODE = rHistoryGroup.ANALYTECODE
         and SAMPLINGTIME = rHistoryGroup.SAMPLINGTIME
         and DESIGNSUBJECTTAG = rHistoryGroup.DESIGNSUBJECTTAG;
      isr$trace.debug('nCount min_assaydatetime', nCount, sCurrentName);
      nCounter := 0;
      if nCount > 1 then
        -- if more than one rows have (real!) assaydatetime = min_assaydatetime then consider the Originalvalue from LIMS
        for rHistoryEntry in (select code, samplestatus, ASSAYDATETIME, RUNID, RUNSAMPLESEQUENCENUMBER 
                                from tmp$BIO$REASSAY$HISTORY
                               where STUDYCODE = rHistoryGroup.STUDYCODE
                                 and SUBJECTCODE = rHistoryGroup.SUBJECTCODE
                                 -- for COVIB-112
                                 --and BIOMATRIXSAMPLINGCODE = rHistoryGroup.BIOMATRIXSAMPLINGCODE
                                 and SPECIMENKEY = rHistoryGroup.SPECIMENKEY
                                 and TREATMENTCODE = rHistoryGroup.TREATMENTCODE
                                 and ANALYTECODE = rHistoryGroup.ANALYTECODE
                                 and SAMPLINGTIME = rHistoryGroup.SAMPLINGTIME
                                 and DESIGNSUBJECTTAG = rHistoryGroup.DESIGNSUBJECTTAG
                            order by ASSAYDATETIME, case when Originalvalue = 'Y' then 1 else 2 end) loop
          nCounter := nCounter + 1;
          isr$trace.debug('assaydate special: nCounter / code / samplestatus', nCounter||' / '||rHistoryEntry.code||' / '||rHistoryEntry.samplestatus, sCurrentName);
          if rHistoryEntry.samplestatus = 2 then
            if nCounter = 1 then
              update tmp$BIO$REASSAY$HISTORY 
              set Originalvalue = 'Y'
              where code = rHistoryEntry.code;
              isr$trace.debug('set Orig to Y for code',rHistoryEntry.code, sCurrentName); 
            else
              update tmp$BIO$REASSAY$HISTORY 
              set Originalvalue = 'N'
              where code = rHistoryEntry.code;
              isr$trace.debug('set Orig to N for code',rHistoryEntry.code, sCurrentName);
            end if;
          end if;
        end loop; 
  
      else 
        -- normal case. assaydatetime could be null. consider also run and seq
        for rHistoryEntry in (select code, samplestatus, ASSAYDATETIME, RUNID, RUNSAMPLESEQUENCENUMBER 
                                from tmp$BIO$REASSAY$HISTORY
                              where STUDYCODE = rHistoryGroup.STUDYCODE
                                 and SUBJECTCODE = rHistoryGroup.SUBJECTCODE
                                 -- for COVIB-112
                                 --and BIOMATRIXSAMPLINGCODE = rHistoryGroup.BIOMATRIXSAMPLINGCODE
                                 and SPECIMENKEY = rHistoryGroup.SPECIMENKEY
                                 and TREATMENTCODE = rHistoryGroup.TREATMENTCODE
                                 and ANALYTECODE = rHistoryGroup.ANALYTECODE
                                 and SAMPLINGTIME = rHistoryGroup.SAMPLINGTIME
                                 and DESIGNSUBJECTTAG = rHistoryGroup.DESIGNSUBJECTTAG
                            order by nvl(assaydatetime,to_date('01.01.1900','DD.MM.YYYY')), RUNID, RUNSAMPLESEQUENCENUMBER) loop
          nCounter := nCounter + 1;
          isr$trace.debug('normal: nCounter / code / samplestatus', nCounter||' / '||rHistoryEntry.code||' / '||rHistoryEntry.samplestatus, sCurrentName);
          if rHistoryEntry.samplestatus = 2 then
            if nCounter = 1 then
              update tmp$BIO$REASSAY$HISTORY 
              set Originalvalue = 'Y'
              where code = rHistoryEntry.code;
              isr$trace.debug('set Orig to Y for code',rHistoryEntry.code, sCurrentName);
            else
              update tmp$BIO$REASSAY$HISTORY 
              set Originalvalue = 'N'
              where code = rHistoryEntry.code;
              isr$trace.debug('set Orig to N for code',rHistoryEntry.code, sCurrentName);
            end if;
          end if;
        end loop; 
  
      end if;  
    end loop;
  end if;
  /**/    

  --  -- update nonexistent original values
  --  update tmp$WT$REASSAY$HISTORY h set ORIGINALVALUE = 'U' where code =
  --  (select min(code) KEEP (dense_rank first ORDER BY studycode, analytecode, specimenkey, runid, runsampleordernumber) 
  --  from tmp$WT$REASSAY$HISTORY h1
  --  where h1.studycode=h.studycode and h1.specimenkey= h.specimenkey and h1.analytecode= h.analytecode
  --  group by specimenkey, studycode, analytecode
  --  having max(ORIGINALVALUE) = 'N');
  
  isr$trace.stat('end', 'end', sCurrentName);  
  return oErrorObj;
EXCEPTION
  WHEN OTHERS THEN
    sMsg :=  utd$msglib.getmsg ('exUnexpectedError', stb$security.getcurrentlanguage, csP1 => sqlerrm(sqlcode));
    oErrorObj. handleError (Stb$typedef.cnSeverityCritical, sMsg, sCurrentName, 'SQLCODE = '||SQLCODE||', SQLERRM = '||SQLERRM||' '||dbms_utility.format_error_backtrace);   
    RETURN oErrorObj;    
end ProcessPost_BIO$REASSAY$HIST;

--***************************************************************************************************  
function PostModelProcessing return STB$oError$Record is

  sCurrentName    constant varchar2(100) := $$PLSQL_UNIT||'.PostModelProcessing';
  oErrorObj                STB$OERROR$RECORD := STB$OERROR$RECORD();
  sMsg                     VARCHAR2(2000);
  oParamList               iSR$ParamList$Rec;

  nRepID                   STB$REPORT.REPID%TYPE;

  sRepConcUnit             varchar2(4000);
  sRepFlagPerc             varchar2(4000);-- for bias
  sRepFlagPercLLOQ         varchar2(4000);-- for bias
  nConcConvFactor          number;
  sSampleConcUnit          varchar2(4000);
  sRepConcUnkUnit          varchar2(4000); -- unknowns unit

  sFirstSampleDate         varchar2(4000);
  dtFirstSampleDate        date;  

  sSamplingTimeFormat      varchar2(4000);
  sSamplingTimeStartFormat varchar2(4000);
  sSamplingTimeEndFormat   varchar2(4000);

  nSamplingTimeHourCap number;

  CURSOR cGetSampleConc IS 
    SELECT DISTINCT CONCENTRATIONUNITS 
    FROM TMP$BIO$STUDY$ANALYTES;    

  /* --- new --- */
  CURSOR cGetRunSampleConc IS 
    SELECT DISTINCT CONCENTRATIONUNITS 
    FROM TMP$BIO$RUN$ANALYTES;  

  /* --- old ---*/
  --CURSOR cGetRunSampleConc IS 
  --  SELECT DISTINCT CONCENTRATIONUNITS 
  --  FROM TMP$WT$ASSAY$ANALYTE;  

  CURSOR cGetReassayHistConc IS 
    SELECT DISTINCT CONCENTRATIONUNITS 
    FROM TMP$BIO$REASSAY$HISTORY;

  nSampleHour number;
  nSampleMinute number;  

  --sKnownNameDelimiter    varchar2(50) := STB$UTIL.GetSystemParameter('KNOWN_NAME_DELIMITER');
  sSamplingTimeConvTo    varchar2(1);
  sSamplingTimeConvMinToHour    varchar2(1);

  procedure ConvertHourToMinutes(nHour in out number, nMinute in out number) is
    nWorkHour number := nHour;
    nWorkMinute number := nvl(nMinute,0);
  begin
    nWorkMinute := nWorkMinute + nWorkHour*60;
    nWorkHour := trunc(nWorkMinute/60);
    nWorkMinute := mod(nWorkMinute,60);
    nHour := nWorkHour;
    if not (nWorkMinute = 0 and nMinute is null) then
      nMinute := nWorkMinute;
    end if;
  end ConvertHourToMinutes;

  procedure ConvertMinutesToHours(nHour in out number, nMinute in out number) is
    nWorkHour number := nHour;
  begin
    nWorkHour := nMinute/60;    
    if nWorkHour is not null then
      nHour := nvl(nHour,0)+nWorkHour;
    end if;  
    nMinute := null;   
  end ConvertMinutesToHours;

  FUNCTION getConvFactor (sSampleConcUnit IN VARCHAR2, sRepConcUnit IN VARCHAR2) return number
  IS

    cursor cConvFactor is
      SELECT CONVERSIONFACTOR 
        FROM TMP$BIO$CONCENTRATIONS
       WHERE ORIGINALCONCENTRATIONUNIT = sSampleConcUnit
         AND RESULTCONCENTRATIONUNIT = sRepConcUnit;
    nConcFactor TMP$BIO$CONCENTRATIONS.CONVERSIONFACTOR%type;
  BEGIN
    -- table TMP$BIO$CONCENTRATIONS has to be filled prior to first call of function  
    open cConvFactor;
    fetch cConvFactor into nConcFactor;
    if cConvFactor%notfound then
      nConcFactor := 1;
    end if;
    close cConvFactor;
    return nConcFactor;    
  END getConvFactor; 

begin
  isr$trace.stat('begin', 'begin', sCurrentName);

    /*
  delete from tmp$wt$run$analytes$deac where RUNANALYTEREGRESSIONSTATUS in (select value from isr$parameter$values where entity = 'BIO$RUNANALYTEREGRESSIONSTATUS');
  delete from tmp$wt$run where code in (select runcode from tmp$wt$run$analytes$deac);
  */
  nRepID := STB$OBJECT.getCurrentRepid;
  isr$trace.debug('nRepID', nRepID, sCurrentName);

  UPDATE tmp$bio$pkparameter
     SET REPORTEDUNIT =
            (SELECT display
               FROM isr$parameter$values
              WHERE entity = 'BIO$UNIT'
                AND VALUE = REPORTEDUNIT)
       , TRANSLATEDREPORTNAME =
            (SELECT display
               FROM isr$parameter$values
              WHERE entity = 'BIO$PKNAME'
                AND VALUE = TRANSLATEDREPORTNAME);

  -- update nameprefix and -suffix from name
  /*update TMP$BIO$ASSAY$ANA$KNOWN
  set nameprefix = STB$UTIL.getLeftDelim(name, sKnownNameDelimiter),
      namesuffix = STB$UTIL.getRightDelim(name, sKnownNameDelimiter); */

  /* --- new version, will be needed after targetmodel has been migrated to 3.5 --- */
  /* matching of KNOWN QCs*/
  FOR rGetNames IN ( SELECT code, name, description, nameprefix, namesuffix
                       FROM TMP$BIO$ANA$KNOWN
                      WHERE knowntype = 'QC') LOOP  
    FOR rGetMatching IN ( SELECT *
                            FROM isr$crit
                           WHERE repid = STB$OBJECT.getCurrentRepid
                             AND entity = 'BIO$KNOWNMATCHING'
                             --AND masterkey = rGetNames.name
                             AND masterkey = rGetNames.nameprefix
                             AND masterkey != key ) LOOP
      update TMP$BIO$RUN$ANA$KNOWN
         set nameprefix = rGetMatching.key
             /*name = rGetMatching.key,
             description = rGetMatching.key,
             nameprefix = STB$UTIL.getLeftDelim(rGetMatching.key, sKnownNameDelimiter),
             namesuffix = STB$UTIL.getRightDelim(rGetMatching.key, sKnownNameDelimiter)*/
       where knowncode = rGetNames.code;
      update TMP$BIO$ANA$KNOWN
         set nameprefix = rGetMatching.key
       where code = rGetNames.code;
    END LOOP;
  END LOOP;

  /* --- old version, copied of MUIB 3.2 LAL --- */
  --FOR rGetNames IN ( SELECT id, name, description, nameprefix, namesuffix
  --                     FROM TMP$BIO$ASSAY$ANA$KNOWN
  --                    WHERE knowntype = 'QC') LOOP  
  --  FOR rGetMatching IN ( SELECT *
  --                          FROM isr$crit
  --                         WHERE repid = STB$OBJECT.getCurrentRepid
  --                           AND entity = 'BIO$KNOWNMATCHING'
  --                           --AND masterkey = rGetNames.name
  --                           AND masterkey = rGetNames.nameprefix
  --                           AND masterkey != key ) LOOP
  --    update TMP$BIO$ASSAY$ANA$KNOWN
  --       set nameprefix = rGetMatching.key
  --           /*name = rGetMatching.key,
  --           description = rGetMatching.key,
  --           nameprefix = STB$UTIL.getLeftDelim(rGetMatching.key, sKnownNameDelimiter),
  --           namesuffix = STB$UTIL.getRightDelim(rGetMatching.key, sKnownNameDelimiter)*/
  --     where id = rGetNames.id;
  --  END LOOP;
  --END LOOP;


  nSamplingTimeHourCap := stb$util.GetReportParameterValue('SAMPLINGTIME_HOUR_CONVERT_CAP',STB$OBJECT.getCurrentRepid);
  if nSamplingTimeHourCap is not null then
    -- convert in bio$sample
    -- convert for enhour and endminute
    for rSampleRow in ( select rowid, endhour, endminute
                        from tmp$bio$sample  
                        where abs(endhour) < nSamplingTimeHourCap and endhour != 0) loop      
      nSampleHour := rSampleRow.endhour;
      nSampleMinute := rSampleRow.endminute;      
      ConvertHourToMinutes(nSampleHour,nSampleMinute);
      update tmp$bio$sample set endhour = nSampleHour, endminute = nSampleMinute
      where rowid = rSampleRow.rowid;      
    end loop;    
    -- convert for starthour and startminute
    for rSampleRow in ( select rowid, starthour, startminute
                        from tmp$bio$sample  
                        where abs(starthour) < nSamplingTimeHourCap and starthour != 0) loop      
      nSampleHour := rSampleRow.starthour;
      nSampleMinute := rSampleRow.startminute;
      ConvertHourToMinutes(nSampleHour,nSampleMinute);
      update tmp$bio$sample set starthour = nSampleHour, startminute = nSampleMinute
      where rowid = rSampleRow.rowid;     
    end loop;   
    -- convert for relativehour and relativeminute
    for rSampleRow in ( select rowid, relativehour, relativeminute
                        from tmp$bio$sample  
                        where relativehour < nSamplingTimeHourCap and relativehour != 0) loop      
      nSampleHour := rSampleRow.relativehour;
      nSampleMinute := rSampleRow.relativeminute;
      ConvertHourToMinutes(nSampleHour,nSampleMinute);
      update tmp$bio$sample set relativehour = nSampleHour, relativeminute = nSampleMinute
      where rowid = rSampleRow.rowid;     
    end loop; 
    -- convert in bio$reassay$history
    -- convert for enhour and endminute
    for rSampleRow in ( select rowid, endhour, endminute
                        from tmp$bio$reassay$history  
                        where abs(endhour) < nSamplingTimeHourCap and endhour != 0) loop      
      nSampleHour := rSampleRow.endhour;
      nSampleMinute := rSampleRow.endminute;
      ConvertHourToMinutes(nSampleHour,nSampleMinute);
      update tmp$bio$reassay$history set endhour = nSampleHour, endminute = nSampleMinute
      where rowid = rSampleRow.rowid;      
    end loop;      
    -- convert for starthour and startminute
    for rSampleRow in ( select rowid, starthour, startminute
                        from tmp$bio$reassay$history  
                        where abs(starthour) < nSamplingTimeHourCap and starthour != 0) loop      
      nSampleHour := rSampleRow.starthour;
      nSampleMinute := rSampleRow.startminute;
      ConvertHourToMinutes(nSampleHour,nSampleMinute);
      update tmp$bio$reassay$history set starthour = nSampleHour, startminute = nSampleMinute
      where rowid = rSampleRow.rowid;     
    end loop;  
  end if;

  sSamplingTimeConvMinToHour := stb$util.GetReportParameterValue('SAMPLINGTIME_CONVERT_MIN_TO_HOUR',STB$OBJECT.getCurrentRepid);
  if sSamplingTimeConvMinToHour = 'T' then
    -- convert in bio$sample
    -- convert for enhour and endminute
    for rSampleRow in ( select rowid, endhour, endminute
                        from tmp$bio$sample  
                        where endminute is not null) loop      
      nSampleHour := rSampleRow.endhour;
      nSampleMinute := rSampleRow.endminute;      
      ConvertMinutesToHours(nSampleHour,nSampleMinute);
      update tmp$bio$sample set endhour = nSampleHour, endminute = nSampleMinute
      where rowid = rSampleRow.rowid;      
    end loop;       
    -- convert for starthour and startminute
    for rSampleRow in ( select rowid, starthour, startminute
                        from tmp$bio$sample  
                        where startminute is not null) loop      
      nSampleHour := rSampleRow.starthour;
      nSampleMinute := rSampleRow.startminute;
      ConvertMinutesToHours(nSampleHour,nSampleMinute);
      update tmp$bio$sample set starthour = nSampleHour, startminute = nSampleMinute
      where rowid = rSampleRow.rowid;     
    end loop;     
    -- convert for relativehour and relativeminute
    for rSampleRow in ( select rowid, relativehour, relativeminute
                        from tmp$bio$sample  
                        where relativeminute is not null) loop      
      nSampleHour := rSampleRow.relativehour;
      nSampleMinute := rSampleRow.relativeminute;
      ConvertMinutesToHours(nSampleHour,nSampleMinute);
      update TMP$BIO$SAMPLE set relativehour = nSampleHour, relativeminute = nSampleMinute
      where rowid = rSampleRow.rowid;     
    end loop; 
    -- convert in bio$reassay$history
    -- convert for enhour and endminute
    for rSampleRow in ( select rowid, endhour, endminute
                        from tmp$bio$reassay$history  
                        where endminute is not null) loop      
      nSampleHour := rSampleRow.endhour;
      nSampleMinute := rSampleRow.endminute;
      ConvertMinutesToHours(nSampleHour,nSampleMinute);
      update tmp$bio$reassay$history set endhour = nSampleHour, endminute = nSampleMinute
      where rowid = rSampleRow.rowid;      
    end loop;       
    -- convert for starthour and startminute
    for rSampleRow in ( select rowid, starthour, startminute
                        from tmp$bio$reassay$history  
                        where startminute is not null) loop      
      nSampleHour := rSampleRow.starthour;
      nSampleMinute := rSampleRow.startminute;
      ConvertMinutesToHours(nSampleHour,nSampleMinute);
      update tmp$bio$reassay$history set starthour = nSampleHour, startminute = nSampleMinute
      where rowid = rSampleRow.rowid;     
    end loop;                      
  end if;

  <<FormatSamplingTime>>
  declare
    sStatement varchar2(32767);
  begin

    sSamplingTimeFormat := STB$UTIL.getreptypeparameter(STB$OBJECT.getCurrentReporttypeId,'SAMPLINGTIME_FORMAT_EXPR');
    sSamplingTimeStartFormat := STB$UTIL.getreptypeparameter(STB$OBJECT.getCurrentReporttypeId,'SAMPLINGTIME_START_FORMAT_EXPR');
    sSamplingTimeEndFormat := STB$UTIL.getreptypeparameter(STB$OBJECT.getCurrentReporttypeId,'SAMPLINGTIME_FORMAT_EXPR');

    if sSamplingTimeFormat is not null or sSamplingTimeEndFormat is not null or sSamplingTimeEndFormat is not null then

      if sSamplingTimeFormat is not null then
        sStatement := ', FormattedSamplingTime = '||sSamplingTimeFormat;
      end if;

      if sSamplingTimeStartFormat is not null then
        sStatement := sStatement||', FormattedSamplingTimeStart = '||sSamplingTimeStartFormat;
      end if;

      if sSamplingTimeEndFormat is not null then
        sStatement := sStatement||', FormattedSamplingTimeEnd = '||sSamplingTimeEndFormat;
      end if;
      sStatement := 'update TMP$BIO$SAMPLE set '||ltrim(sStatement,',');

      isr$trace.debug('sStatement for bio$sample', sStatement, sCurrentName);
      execute immediate sStatement;  

      if sSamplingTimeFormat is not null then
        sStatement := 'update TMP$BIO$REASSAY$HISTORY set FormattedSamplingTime = '||sSamplingTimeFormat;
        isr$trace.debug('sStatement for bio$reassay$history', sStatement, sCurrentName);
        execute immediate sStatement;

        sStatement := 'update TMP$BIO$DEACTIVATED$SAMPLES set FormattedSamplingTime = '||sSamplingTimeFormat;
        isr$trace.debug('sStatement for TMP$BIO$DEACTIVATED$SAMPLES', sStatement, sCurrentName);
        execute immediate sStatement; 

      end if;

    end if;

  end FormatSamplingTime;

  /*oErrorObj := STB$UTIL.getreptypeparameter(STB$OBJECT.getCurrentReporttypeId,'SAMPLINGTIME_FORMAT',sSamplingTimeFormat);
  oErrorObj := STB$UTIL.getreptypeparameter(STB$OBJECT.getCurrentReporttypeId,'SAMPLINGTIME_START_FORMAT',sSamplingTimeStartFormat);
  oErrorObj := STB$UTIL.getreptypeparameter(STB$OBJECT.getCurrentReporttypeId,'SAMPLINGTIME_END_FORMAT',sSamplingTimeEndFormat);  

  -- update in loop because direct update crashes with mutating table
  for rSampleRow in ( select rowid, 
                             ISR$WT$BIBC$PostSVType$Entity.FormatRowValue('BIO$SAMPLE', sSamplingTimeFormat, rowid) as ForamttedSampling,
                             ISR$WT$BIBC$PostSVType$Entity.FormatRowValue('BIO$SAMPLE', sSamplingTimeStartFormat, rowid) as ForamttedSamplingStart,
                             ISR$WT$BIBC$PostSVType$Entity.FormatRowValue('BIO$SAMPLE', sSamplingTimeEndFormat, rowid) as ForamttedSamplingEnd   
                      from TMP$BIO$SAMPLE ) loop
    update TMP$BIO$SAMPLE 
    set FormattedSamplingTime = rSampleRow.ForamttedSampling,
        FormattedSamplingTimeStart = rSampleRow.ForamttedSamplingStart,
        FormattedSamplingTimeEnd = rSampleRow.ForamttedSamplingEnd
    where rowid = rSampleRow.rowid;
  end loop;

  for cSampleRow in ( select rowid, ISR$WT$MUIB$PostSVType$Entity.FormatRowValue('BIO$REASSAY$HISTORY', sSamplingTimeFormat, rowid) as ForamttedSampling 
                      from TMP$BIO$REASSAY$HISTORY ) loop
    update TMP$BIO$REASSAY$HISTORY
    set FormattedSamplingTime = cSampleRow.ForamttedSampling
    where rowid = cSampleRow.rowid;
  end loop;

  for cSampleRow in ( select rowid, ISR$BIO$MUIB$PostSVType$Entity.FormatRowValue('BIO$DEACTIVATED$SAMPLES', sSamplingTimeFormat, rowid) as ForamttedSampling 
                      from TMP$BIO$DEACTIVATED$SAMPLES ) loop
    update TMP$BIO$DEACTIVATED$SAMPLES
    set FormattedSamplingTime = cSampleRow.ForamttedSampling
    where rowid = cSampleRow.rowid;
  end loop;
  */

  sSamplingTimeConvTo := stb$util.GetReportParameterValue('SAMPLINGTIME_CONVERT_TO',STB$OBJECT.getCurrentRepid);
  -- 86400*nvl(d.endday,0)+3600*nvl(d.endhour,0)+60*nvl(d.endminute,0)+nvl(d.endsecond,0) endtotaltime,
  if sSamplingTimeConvTo in ('h','H') then
    update tmp$bio$sample 
      set ENDSAMPLINGTIME = nvl(endhour,0) + nvl(endminute,0)/60+nvl(endsecond,0)/3600;
  elsif sSamplingTimeConvTo in ('m','M') then
    update tmp$bio$sample 
      set ENDSAMPLINGTIME = nvl(endhour,0)*60 + nvl(endminute,0)+nvl(endsecond,0)/60;
  elsif sSamplingTimeConvTo in ('s','S') then
    update tmp$bio$sample 
      set ENDSAMPLINGTIME = nvl(endhour,0)*3600 + nvl(endminute,0)*60+nvl(endsecond,0);
  end if;   

  -- calculate concentrations for tox report
  sRepConcUnit := STB$UTIL.getReportParameterValue('CONCENTRATION_UNIT', nRepID);
  isr$trace.debug('sRepConcUnit', sRepConcUnit, sCurrentName);
  IF sRepConcUnit = STB$UTIL.GetSystemParameter('UNIT_NO_CHANGE') THEN
    sRepConcUnit := null; -- conversion factor should be 1. See below
  END IF;  

  sRepFlagPerc := STB$UTIL.getReportParameterValue('FLAG_PERCENT', nRepID); --for bias  
  isr$trace.debug('sRepFlagPerc', sRepFlagPerc, sCurrentName);

  UPDATE TMP$BIO$RUN$ANA$KNOWN set FLAGPERCENT = sRepFlagPerc 
  WHERE originalknowntype = 'STABILITY' AND FLAGPERCENT IS NULL;

  sRepFlagPercLLOQ := STB$UTIL.getReportParameterValue('FLAG_PERCENT_LLOQ', nRepID); --for LLOQ bias  
  isr$trace.debug('sRepFlagPercLLOQ', sRepFlagPercLLOQ, sCurrentName);

  UPDATE TMP$BIO$RUN$ANA$KNOWN set FLAGPERCENT = sRepFlagPerc 
  WHERE originalknowntype = 'STABILITY' AND FLAGPERCENT IS NULL;  

  /* --- new version --- */
   UPDATE TMP$BIO$RUN$ANA$KNOWN k
   set FLAGPERCENT = case when exists(
                                 select KEY
                                   from isr$crit c
                                  where entity like 'BIO$KNOWN%'
                                    and masterkey in (select value from ISR$PARAMETER$VALUES where entity = 'BIO$EXPERIMENT_LLOQ')
                                    and repid = STB$OBJECT.getCurrentRepid and k.nameprefix = c.key) 
                          then sRepFlagPercLLOQ
                          else sRepFlagPerc 
                     end 
   WHERE originalknowntype = 'STABILITY' AND FLAGPERCENT IS NULL;  

  /* --- old version --- */
  /*UPDATE TMP$wt$assay$ana$known k
  set FLAGPERCENT = case when exists(select KEY 
                                     from isr$crit c 
                                     where entity like 'BIO$KNOWN%' 
                                       and masterkey in (select value from ISR$PARAMETER$VALUES where entity = 'BIO$EXPERIMENT_LLOQ') 
                                       and repid = STB$OBJECT.getCurrentRepid and k.nameprefix = c.key) 
                         then sRepFlagPercLLOQ
                         else sRepFlagPerc 
                    end 
  WHERE originalknowntype = 'STABILITY' AND FLAGPERCENT IS NULL;*/

  /* --- new version --- */
    IF sRepConcUnit IS NULL THEN 
      -- conversion factor = 1
      --UPDATE TMP$BIO$ASSAY$ANALYTE set lloqconv = lloq, uloqconv = uloq, firstlloqconv = firstlloq, firstuloqconv = firstuloq;
      UPDATE TMP$BIO$sample$results set concentrationconv = concentration, calibrationrangeconv = calibrationrange;
      UPDATE TMP$BIO$RUN$SAMPLE$RESULTS 
         SET concentrationconv = concentration, 
             --knownconcentrationconv = knownconcentration,
             nominalconcentrationconv = nominalconcentration;
      UPDATE TMP$BIO$RUN$ANA$KNOWN
         set concentrationconv = concentration, minconcentrationconv = minconcentration;     
      UPDATE TMP$BIO$RUN$ANALYTES 
         set vecconv = vec, nmconv = nm,
             lloqconv = lloq, uloqconv = uloq;  
      UPDATE TMP$BIO$REASSAY$HISTORY
        SET reassayconcentrationconv = reassayconcentration,
            sampleconcentrationconv = sampleconcentration,
            calibrationrangeconv = calibrationrange,
            nmconvsample =nm,
            vecconvsample =vec,
            nmconvreassay =nm,
            vecconvreassay =vec;         
    ELSE 
      -- make sure that conversion factors table is filled   
      oParamList.AddParam('REPORTTYPEID', STB$OBJECT.getCurrentReporttypeId);
      oParamList.AddParam('REPID', STB$OBJECT.getCurrentRepid);
      oParamList.AddParam('ENTITY','BIO$CONCENTRATIONS'); 
      oErrorObj := ISR$REMOTE$COLLECTOR.fillTmpTableForEntity ('BIO$CONCENTRATIONS', oParamList, 'F');
     /* 
      --  lloq and uloq
      -- use cursor and valiables defined for run sample results      
      OPEN cGetRunSampleConc;
      FETCH cGetRunSampleConc INTO sSampleConcUnit;
      WHILE cGetRunSampleConc%FOUND LOOP      
        isr$trace.debug('assay analyte conc unit', sSampleConcUnit, sCurrentName);
        nConcConvFactor := getConvFactor(sSampleConcUnit,sRepConcUnit);      
        isr$trace.debug('nConcConvFactor', nConcConvFactor, sCurrentName);
        IF nConcConvFactor IS NULL THEN 
          nConcConvFactor := 1;
        END IF;
        UPDATE TMP$wt$ASSAY$ANALYTE 
          set lloqconv = lloq * nConcConvFactor, uloqconv = uloq * nConcConvFactor,
              firstlloqconv = firstlloq * nConcConvFactor, firstuloqconv = firstuloq * nConcConvFactor
        WHERE concentrationunits = sSampleConcUnit;      
        FETCH cGetRunSampleConc INTO sSampleConcUnit;
      END LOOP;
      CLOSE cGetRunSampleConc;    */
      isr$trace.debug('start SampleConc', null, sCurrentName);
      OPEN cGetSampleConc;
      FETCH cGetSampleConc INTO sSampleConcUnit;
      WHILE cGetSampleConc%FOUND LOOP      
        isr$trace.debug('sSampleConcUnit', sSampleConcUnit, sCurrentName);
        nConcConvFactor := NULL;
        nConcConvFactor := getConvFactor(sSampleConcUnit,sRepConcUnit);      
        isr$trace.debug('nConcConvFactor', nConcConvFactor, sCurrentName);
        IF nConcConvFactor IS NULL THEN 
          nConcConvFactor := 1;
        END IF;
        UPDATE TMP$BIO$SAMPLE$RESULTS 
        set concentrationconv = concentration * nConcConvFactor,
            calibrationrangeconv = calibrationrange * nConcConvFactor
        WHERE analytecode IN (SELECT code FROM TMP$BIO$STUDY$ANALYTES WHERE CONCENTRATIONUNITS = sSampleConcUnit);
        FETCH cGetSampleConc INTO sSampleConcUnit;
      END LOOP;
      CLOSE cGetSampleConc;    
      isr$trace.debug('finished SampleConc start RunSampleConc', null, sCurrentName);
      OPEN cGetRunSampleConc;
      FETCH cGetRunSampleConc INTO sSampleConcUnit;
      WHILE cGetRunSampleConc%FOUND LOOP      
        isr$trace.debug('sSampleConcUnit', sRepFlagPerc, sSampleConcUnit);
        nConcConvFactor := NULL;
        nConcConvFactor := getConvFactor(sSampleConcUnit,sRepConcUnit);      
        isr$trace.debug('nConcConvFactor', sRepFlagPerc, nConcConvFactor);
        IF nConcConvFactor IS NULL THEN 
          nConcConvFactor := 1;
        END IF;
        UPDATE TMP$BIO$RUN$SAMPLE$RESULTS 
        SET concentrationconv = concentration * nConcConvFactor,/*
            knownconcentrationconv = knownconcentration * nConcConvFactor,*/
            nominalconcentrationconv = nominalconcentration * nConcConvFactor
        WHERE runanalytecode IN (SELECT code FROM TMP$BIO$RUN$ANALYTES WHERE CONCENTRATIONUNITS = sSampleConcUnit);
        UPDATE TMP$BIO$run$ana$known
        set concentrationconv = concentration * nConcConvFactor,  
            minconcentrationconv = minconcentration * nConcConvFactor   
        WHERE runanalytecode IN (SELECT code FROM TMP$BIO$RUN$ANALYTES WHERE CONCENTRATIONUNITS = sSampleConcUnit);    
        UPDATE TMP$BIO$RUN$ANALYTES 
        SET vecconv = vec * nConcConvFactor,
            nmconv = nm * nConcConvFactor,
            lloqconv = lloq * nConcConvFactor, 
            uloqconv = uloq * nConcConvFactor
        WHERE concentrationunits = sSampleConcUnit; 

        FETCH cGetRunSampleConc INTO sSampleConcUnit;
      END LOOP;
      CLOSE cGetRunSampleConc;  

      isr$trace.debug('finished RunSampleConc start ReassayHistory', null, sCurrentName); 
      OPEN cGetReassayHistConc;
      FETCH cGetReassayHistConc INTO sSampleConcUnit;
      WHILE cGetReassayHistConc%FOUND LOOP      
        isr$trace.debug('sSampleConcUnit', sSampleConcUnit, sCurrentName);
        nConcConvFactor := NULL;
        nConcConvFactor := getConvFactor(sSampleConcUnit,sRepConcUnit);      
        isr$trace.debug('nConcConvFactor', nConcConvFactor, sCurrentName);
        IF nConcConvFactor IS NULL THEN 
          nConcConvFactor := 1;
        END IF;

        UPDATE TMP$BIO$REASSAY$HISTORY
        SET reassayconcentrationconv = reassayconcentration * nConcConvFactor,
            sampleconcentrationconv = sampleconcentration * nConcConvFactor,
            calibrationrangeconv = calibrationrange * nConcConvFactor,
            vecconvsample =vec * nConcConvFactor,
            nmconvsample =nm * nConcConvFactor,        
            vecconvreassay =vec * nConcConvFactor,
            nmconvreassay =nm * nConcConvFactor                 
        WHERE CONCENTRATIONUNITS = sSampleConcUnit;        
        FETCH cGetReassayHistConc INTO sSampleConcUnit;
      END LOOP;
      CLOSE cGetReassayHistConc;     

      -- empty the conversion table because no longer needed
      delete from TMP$BIO$CONCENTRATIONS;

    END IF;

  /* --- old version --- */

  --  IF sRepConcUnit IS NULL THEN 
  --    -- conversion factor = 1
  --    UPDATE TMP$wt$ASSAY$ANALYTE
  --       set lloqconv = lloq, uloqconv = uloq, firstlloqconv = firstlloq, firstuloqconv = firstuloq,
  --           lloqunkconv = lloq, uloqunkconv = uloq, firstlloqunkconv = firstlloq, firstuloqunkconv = firstuloq;
  --    UPDATE TMP$wt$sample$results set concentrationconv = concentration, calibrationrangeconv = calibrationrange;
  --    UPDATE TMP$wt$run$sample$results 
  --       SET concentrationconv = concentration, 
  --           knownconcentrationconv = knownconcentration,
  --           nominalconcentrationconv = nominalconcentration;
  --    UPDATE TMP$wt$assay$ana$known set concentrationconv = concentration;     
  --    UPDATE tmp$wt$RUN$ANALYTES 
  --      set  vecconv = vec, nmconv = nm, vecunkconv = vec, nmunkconv = nm;  
  --    UPDATE TMP$wt$reassay$history
  --      SET reassayconcentrationconv = reassayconcentration,
  --          sampleconcentrationconv = sampleconcentration,
  --          calibrationrangeconv = calibrationrange,          
  --          nmconv = nm,
  --          vecconv =vec;         
  --  ELSE 
  --    --  lloq and uloq
  --    -- use cursor and valiables defined for run sample results  
  --    OPEN cGetRunSampleConc;
  --    FETCH cGetRunSampleConc INTO sSampleConcUnit;
  --    WHILE cGetRunSampleConc%FOUND LOOP
  --      isr$trace.debug('assay analyte conc unit', 'ISR$BIOANALYTICS$WT$LAL.createReport', sSampleConcUnit, 50);
  --      nConcConvFactor := getConvFactor(sSampleConcUnit,sRepConcUnit);
  --      isr$trace.debug('nConcConvFactor', 'ISR$BIOANALYTICS$WT$LAL.createReport', nConcConvFactor, 50);
  --      UPDATE TMP$wt$ASSAY$ANALYTE 
  --        set lloqconv = lloq * nConcConvFactor, uloqconv = uloq * nConcConvFactor,
  --            firstlloqconv = firstlloq * nConcConvFactor, firstuloqconv = firstuloq * nConcConvFactor,
  --            lloqunkconv = lloq * nConcConvFactor, uloqunkconv = uloq * nConcConvFactor,
  --            firstlloqunkconv = firstlloq * nConcConvFactor, firstuloqunkconv = firstuloq * nConcConvFactor
  --      WHERE concentrationunits = sSampleConcUnit;      
  --      FETCH cGetRunSampleConc INTO sSampleConcUnit;
  --    END LOOP;
  --    CLOSE cGetRunSampleConc;    
  --    isr$trace.debug('finished lloq/uloq start SampleConc', 'ISR$BIOANALYTICS$WT$LAL.createReport', NULL, 50);
  --    OPEN cGetSampleConc;
  --    FETCH cGetSampleConc INTO sSampleConcUnit;
  --    WHILE cGetSampleConc%FOUND LOOP
  --      isr$trace.debug('sSampleConcUnit', 'ISR$BIOANALYTICS$WT$LAL.createReport', sSampleConcUnit, 50);
  --      nConcConvFactor := NULL;
  --      nConcConvFactor := getConvFactor(sSampleConcUnit,sRepConcUnit);
  --      isr$trace.debug('nConcConvFactor', 'ISR$BIOANALYTICS$WT$LAL.createReport', nConcConvFactor, 50);
  --      IF nConcConvFactor IS NULL THEN 
  --        nConcConvFactor := 1;
  --      END IF;
  --      UPDATE TMP$wt$sample$results 
  --      set concentrationconv = concentration * nConcConvFactor,
  --          calibrationrangeconv = calibrationrange * nConcConvFactor
  --      WHERE analytecode IN (SELECT code FROM TMP$WT$STUDY$ANALYTES WHERE CONCENTRATIONUNITS = sSampleConcUnit);
  --      FETCH cGetSampleConc INTO sSampleConcUnit;
  --    END LOOP;
  --    CLOSE cGetSampleConc;    
  --    isr$trace.debug('finished SampleConc start RunSampleConc', 'ISR$BIOANALYTICS$WT$LAL.createReport', NULL, 50);
  --    OPEN cGetRunSampleConc;
  --    FETCH cGetRunSampleConc INTO sSampleConcUnit;
  --    WHILE cGetRunSampleConc%FOUND LOOP
  --      isr$trace.debug('sSampleConcUnit', 'ISR$BIOANALYTICS$WT$LAL.createReport', sSampleConcUnit, 50);
  --      nConcConvFactor := NULL;
  --      nConcConvFactor := getConvFactor(sSampleConcUnit,sRepConcUnit);
  --      isr$trace.debug('nConcConvFactor', 'ISR$BIOANALYTICS$WT$LAL.createReport', nConcConvFactor, 50);
  --      IF nConcConvFactor IS NULL THEN 
  --        nConcConvFactor := 1;
  --      END IF;
  --      UPDATE TMP$wt$run$sample$results 
  --      SET concentrationconv = concentration * nConcConvFactor,
  --          knownconcentrationconv = knownconcentration * nConcConvFactor,
  --          nominalconcentrationconv = nominalconcentration * nConcConvFactor
  --      WHERE assayanalytecode IN (SELECT code FROM TMP$WT$ASSAY$ANALYTE WHERE CONCENTRATIONUNITS = sSampleConcUnit);
  --      UPDATE TMP$wt$assay$ana$known
  --      set concentrationconv = concentration * nConcConvFactor      
  --      WHERE assayanalytecode IN (SELECT code FROM TMP$WT$ASSAY$ANALYTE WHERE CONCENTRATIONUNITS = sSampleConcUnit);    
  --      UPDATE tmp$wt$RUN$ANALYTES 
  --      SET vecconv = vec * nConcConvFactor,
  --          nmconv = nm * nConcConvFactor,
  --          vecunkconv = vec * nConcConvFactor,
  --          nmunkconv = nm * nConcConvFactor
  --      WHERE assayanalytecode IN (SELECT code FROM tmp$WT$ASSAY$ANALYTE WHERE CONCENTRATIONUNITS = sSampleConcUnit); 
  --
  --      FETCH cGetRunSampleConc INTO sSampleConcUnit;
  --    END LOOP;
  --    CLOSE cGetRunSampleConc;  
  --
  --    isr$trace.debug('finished RunSampleConc start ReassayHistory', 'ISR$BIOANALYTICS$WT$LAL.createReport', NULL, 50);    
  --    OPEN cGetReassayHistConc;
  --    FETCH cGetReassayHistConc INTO sSampleConcUnit;
  --    WHILE cGetReassayHistConc%FOUND LOOP
  --      isr$trace.debug('sSampleConcUnit', 'ISR$BIOANALYTICS$WT$LAL.createReport', sSampleConcUnit, 50);
  --      nConcConvFactor := NULL;
  --      nConcConvFactor := getConvFactor(sSampleConcUnit,sRepConcUnit);
  --      isr$trace.debug('nConcConvFactor', 'ISR$BIOANALYTICS$WT$LAL.createReport', nConcConvFactor, 50);
  --      IF nConcConvFactor IS NULL THEN 
  --        nConcConvFactor := 1;
  --      END IF;
  --
  --      UPDATE TMP$wt$reassay$history
  --      SET reassayconcentrationconv = reassayconcentration * nConcConvFactor,
  --          sampleconcentrationconv = sampleconcentration * nConcConvFactor,
  --          calibrationrangeconv = calibrationrange * nConcConvFactor,
  --          vecconv =vec * nConcConvFactor,
  --          nmconv =nm * nConcConvFactor                 
  --      WHERE CONCENTRATIONUNITS = sSampleConcUnit;        
  --      FETCH cGetReassayHistConc INTO sSampleConcUnit;
  --    END LOOP;
  --    CLOSE cGetReassayHistConc;     
  --  END IF;

  /* --- additional old code copied from MUIB 3.2, need to check if it's still needed --- */

  -- check if parameter for unknowns unit exist and convert unknowns in new values
  --  sRepConcUnkUnit := STB$UTIL.getReportParameterValue('CONCENTRATION_UNIT_UNKNOWNS', nRepID);
  --  isr$trace.debug('sRepConcUnkUnit', 'ISR$BIOANALYTICS$WT$LAL.createReport', sRepConcUnit, 50);
  --  IF sRepConcUnkUnit = STB$UTIL.GetSystemParameter('UNIT_NO_CHANGE') THEN
  --    sRepConcUnkUnit := null; -- conversion factor should be 1. See below
  --  END IF;  
  --
  --  if sRepConcUnkUnit is not null then
  --    -- second unit for unknowns
  --
  --    /*OPEN cGetRunSampleConc;
  --    FETCH cGetRunSampleConc INTO sSampleConcUnit;
  --    WHILE cGetRunSampleConc%FOUND LOOP
  --      isr$trace.debug('assay analyte conc unit', 'ISR$BIOANALYTICS$WT$LAL.createReport', sSampleConcUnit, 50);
  --      nConcConvFactor := getConvFactor(sSampleConcUnit,sRepConcUnkUnit);
  --      isr$trace.debug('nConcConvFactor', 'ISR$BIOANALYTICS$WT$LAL.createReport', nConcConvFactor, 50);
  --      UPDATE TMP$wt$ASSAY$ANALYTE 
  --        set lloqunkconv = lloq * nConcConvFactor, uloqunkconv = uloq * nConcConvFactor,
  --            firstlloqunkconv = firstlloq * nConcConvFactor, firstuloqunkconv = firstuloq * nConcConvFactor
  --      WHERE concentrationunits = sSampleConcUnit;      
  --      FETCH cGetRunSampleConc INTO sSampleConcUnit;
  --    END LOOP;
  --    CLOSE cGetRunSampleConc;
  --    isr$trace.debug('finished lloq/uloq start SampleConc', 'ISR$BIOANALYTICS$WT$LAL.createReport', NULL, 50);*/
  --    OPEN cGetSampleConc;
  --    FETCH cGetSampleConc INTO sSampleConcUnit;
  --    WHILE cGetSampleConc%FOUND LOOP
  --      isr$trace.debug('sSampleConcUnit', 'ISR$BIOANALYTICS$WT$LAL.createReport', sSampleConcUnit, 50);
  --      nConcConvFactor := NULL;
  --      nConcConvFactor := getConvFactor(sSampleConcUnit,sRepConcUnkUnit);
  --      isr$trace.debug('nConcConvFactor', 'ISR$BIOANALYTICS$WT$LAL.createReport', nConcConvFactor, 50);
  --      IF nConcConvFactor IS NULL THEN 
  --        nConcConvFactor := 1;
  --      END IF;
  --      UPDATE TMP$BIO$sample$results 
  --      set concentrationconv = concentration * nConcConvFactor,
  --          calibrationrangeconv = calibrationrange * nConcConvFactor
  --      WHERE analytecode IN (SELECT code FROM TMP$BIO$STUDY$ANALYTES WHERE CONCENTRATIONUNITS = sSampleConcUnit);
  --      FETCH cGetSampleConc INTO sSampleConcUnit;
  --    END LOOP;
  --    CLOSE cGetSampleConc;    
  --    isr$trace.debug('finished SampleConc start RunSampleConc', 'ISR$BIOANALYTICS$WT$LAL.createReport', NULL, 50);
  --    OPEN cGetRunSampleConc;
  --    FETCH cGetRunSampleConc INTO sSampleConcUnit;
  --    WHILE cGetRunSampleConc%FOUND LOOP
  --      isr$trace.debug('sSampleConcUnit', 'ISR$BIOANALYTICS$WT$LAL.createReport', sSampleConcUnit, 50);
  --      nConcConvFactor := NULL;
  --      nConcConvFactor := getConvFactor(sSampleConcUnit,sRepConcUnkUnit);
  --      isr$trace.debug('nConcConvFactor', 'ISR$BIOANALYTICS$WT$LAL.createReport', nConcConvFactor, 50);
  --      IF nConcConvFactor IS NULL THEN 
  --        nConcConvFactor := 1;
  --      END IF;
  --      UPDATE TMP$BIO$run$sample$results 
  --      SET concentrationconv = concentration * nConcConvFactor,
  --          knownconcentrationconv = knownconcentration * nConcConvFactor,
  --          nominalconcentrationconv = nominalconcentration * nConcConvFactor
  --      WHERE assayanalytecode IN (SELECT code FROM TMP$BIO$ASSAY$ANALYTE WHERE CONCENTRATIONUNITS = sSampleConcUnit)
  --        AND RUNSAMPLECODE IN (select code from TMP$BIO$RUN$SAMPLES where sampletype='unknown');
  --      -- knowns sind keine unknowns
  --      --UPDATE TMP$wt$assay$ana$known
  --      --set concentrationconv = concentration * nConcConvFactor      
  --      --WHERE assayanalytecode IN (SELECT code FROM TMP$WT$ASSAY$ANALYTE WHERE CONCENTRATIONUNITS = sSampleConcUnit);    
  --      UPDATE tmp$BIO$RUN$ANALYTES 
  --      SET vecunkconv = vec * nConcConvFactor,
  --          nmunkconv = nm * nConcConvFactor
  --      WHERE assayanalytecode IN (SELECT code FROM tmp$BIO$ASSAY$ANALYTE WHERE CONCENTRATIONUNITS = sSampleConcUnit); 
  --
  --      FETCH cGetRunSampleConc INTO sSampleConcUnit;
  --    END LOOP;
  --    CLOSE cGetRunSampleConc;  
  --    isr$trace.debug('finished RunSampleConc start ReassayHistory', 'ISR$BIOANALYTICS$WT$LAL.createReport', NULL, 50);    
  --    OPEN cGetReassayHistConc;
  --    FETCH cGetReassayHistConc INTO sSampleConcUnit;
  --    WHILE cGetReassayHistConc%FOUND LOOP
  --      isr$trace.debug('sSampleConcUnit', 'ISR$BIOANALYTICS$WT$LAL.createReport', sSampleConcUnit, 50);
  --      nConcConvFactor := NULL;
  --      nConcConvFactor := getConvFactor(sSampleConcUnit,sRepConcUnkUnit);
  --      isr$trace.debug('nConcConvFactor', 'ISR$BIOANALYTICS$WT$LAL.createReport', nConcConvFactor, 50);
  --      IF nConcConvFactor IS NULL THEN 
  --        nConcConvFactor := 1;
  --      END IF;      
  --      UPDATE TMP$BIO$reassay$history
  --      SET reassayconcentrationconv = reassayconcentration * nConcConvFactor,
  --          sampleconcentrationconv = sampleconcentration * nConcConvFactor,
  --          calibrationrangeconv = calibrationrange * nConcConvFactor,
  --          vecconv =vec * nConcConvFactor,
  --          nmconv =nm * nConcConvFactor                 
  --      WHERE CONCENTRATIONUNITS = sSampleConcUnit;        
  --      FETCH cGetReassayHistConc INTO sSampleConcUnit;
  --    END LOOP;
  --    CLOSE cGetReassayHistConc;  
  --  end if;

  -- update results with aliquotfactor
  UPDATE TMP$BIO$RUN$SAMPLE$RESULTS rsr
  set concentrationconv = concentrationconv / (SELECT nvl(aliquotfactor,1) FROM TMP$BIO$RUN$SAMPLES rs WHERE code = rsr.runsamplecode);

  UPDATE TMP$BIO$sample$results 
  set concentrationconv = concentrationconv / aliquotfactor,
      calibrationrangeconv = calibrationrangeconv / aliquotfactor;

  /* --- new version --- */

    UPDATE TMP$BIO$REASSAY$HISTORY 
    set reassayconcentrationconv = reassayconcentrationconv / nvl(reassay_aliquotfactor,1),
        sampleconcentrationconv = sampleconcentrationconv / nvl(sample_aliquotfactor,1),
        --calibrationrangeconv = calibrationrangeconv / nvl(reassay_aliquotfactor,1),
        calibrationrangeconv = calibrationrangeconv / nvl(sample_aliquotfactor,1),
        nmconvsample =nmconvsample / nvl(sample_aliquotfactor,1),
        vecconvsample = vecconvsample / nvl(sample_aliquotfactor,1), 
        nmconvreassay =nmconvreassay / nvl(reassay_aliquotfactor,1),
        vecconvreassay = vecconvreassay / nvl(reassay_aliquotfactor,1);


  /* --- old version --- */

  --  UPDATE TMP$BIO$reassay$history 
  --  set reassayconcentrationconv = reassayconcentrationconv / nvl(reassay_aliquotfactor,1),
  --      sampleconcentrationconv = sampleconcentrationconv / nvl(sample_aliquotfactor,1),
  --      calibrationrangeconv = calibrationrangeconv / nvl(sample_aliquotfactor,1),
  --      nmconv =nmconv / nvl(reassay_aliquotfactor,1),
  --      vecconv = vecconv / nvl(reassay_aliquotfactor,1);

  sFirstSampleDate := STB$UTIL.getReportParameterValue('FIRST_SAMPLE_DATE', nRepid);
  begin
    dtFirstSampleDate := to_date(sFirstSampleDate,STB$UTIL.getSystemParameter('DATEFORMAT'));
  exception
    when others then dtFirstSampleDate := null;
  end;

  if dtFirstSampleDate is not null then
    update TMP$BIO$STUDY$CALC set assayenddays = trunc(assayenddate)-dtFirstSampleDate;
  end if;

  -- update nonexistent original values
  update TMP$BIO$REASSAY$HISTORY h set ORIGINALVALUE = 'U' where code =
  (select min(code) KEEP (dense_rank first ORDER BY studycode, specimenkey, runid, runsampleordernumber) 
  from TMP$BIO$REASSAY$HISTORY h1
  where h1.studycode=h.studycode and h1.specimenkey= h.specimenkey
  group by specimenkey, studycode
  having max(ORIGINALVALUE) = 'N');  
  
  <<CalcDifference>>
  declare 
    nRepID              STB$REPORT.REPID%TYPE := STB$OBJECT.getCurrentRepid;
    sConcRoundNotSigFig varchar2(10) := stb$util.GetReportParameterValue('CONC_ROUND_NOT_SIG_FIG',nRepID);
    sFinalAccuracy      varchar2(10) := stb$util.GetReportParameterValue('FINAL_ACCURACY',nRepID);
    nConcFigures        integer      := stb$util.GetReportParameterValue('CONC_FIGURES',nRepID);
    nReassayDiffType    number       := stb$util.GetReportParameterValue('REASSAY_DIFF_TYPE',nRepID);
    nDifference         number;
    nPrevConc           number;
    sPrevConcStatus     varchar2(4000);
    nPrevDifference     number;
  begin
  
    for rOrigEntry in (select code, STUDYCODE, SUBJECTCODE, SPECIMENKEY,reassayconcentrationconv, reassayconcentration,
                              TREATMENTCODE, ANALYTECODE, SAMPLINGTIME, DESIGNSUBJECTTAG
                        from tmp$BIO$REASSAY$HISTORY
                        where originalvalue in ('Y','U')
                          /*and reassayconcentrationconv is not null
                          and reassayconcentrationconv <> 0*/) loop
      isr$trace.debug('Orig code / reassayconcentrationconv',rOrigEntry.code||' / '||rOrigEntry.reassayconcentrationconv, sCurrentName);
      nPrevConc := null;
      sPrevConcStatus := null;
      nPrevDifference := null;
      for rReassayEntry in (select CODE, STUDYCODE, SUBJECTCODE, SPECIMENKEY,reassayconcentrationconv, reassayconcentration, reassay_concentrationstatus,
                                   TREATMENTCODE, ANALYTECODE, SAMPLINGTIME, DESIGNSUBJECTTAG  
                              from tmp$BIO$REASSAY$HISTORY
                             where originalvalue = 'N' 
                               and reassayconcentrationconv is not null
                               and STUDYCODE = rOrigEntry.STUDYCODE
                               and SUBJECTCODE = rOrigEntry.SUBJECTCODE
                               and SPECIMENKEY = rOrigEntry.SPECIMENKEY
                               and TREATMENTCODE = rOrigEntry.TREATMENTCODE
                               and ANALYTECODE = rOrigEntry.ANALYTECODE
                               and SAMPLINGTIME = rOrigEntry.SAMPLINGTIME
                               and DESIGNSUBJECTTAG = rOrigEntry.DESIGNSUBJECTTAG) loop
        nDifference := null;
        isr$trace.debug('Entry code / reassayconcentrationconv',rReassayEntry.code||' / '||rReassayEntry.reassayconcentrationconv, sCurrentName);
        isr$trace.debug('Difference Type',nReassayDiffType, sCurrentName);
        if rOrigEntry.reassayconcentrationconv is not null and rOrigEntry.reassayconcentrationconv <> 0 then
          if sFinalAccuracy = 'T' then
            nDifference := (rReassayEntry.reassayconcentrationconv - rOrigEntry.reassayconcentrationconv) /rOrigEntry.reassayconcentrationconv * 100;
          else
            if sConcRoundNotSigFig = 'T' then
              if nReassayDiffType = 2 then
                nDifference := (round(rReassayEntry.reassayconcentrationconv,nConcFigures) - round(rOrigEntry.reassayconcentrationconv,nConcFigures)) 
                              / round(rReassayEntry.reassayconcentrationconv,nConcFigures) * 100;
              elsif nReassayDiffType = 3 then
                nDifference := (round(rReassayEntry.reassayconcentrationconv,nConcFigures) - round(rOrigEntry.reassayconcentrationconv,nConcFigures)) 
                              / round( (round(rOrigEntry.reassayconcentrationconv,nConcFigures) + round(rReassayEntry.reassayconcentrationconv,nConcFigures)) / 2 ,nConcFigures) * 100;
              else
                nDifference := (round(rReassayEntry.reassayconcentrationconv,nConcFigures) - round(rOrigEntry.reassayconcentrationconv,nConcFigures)) 
                              / round(rOrigEntry.reassayconcentrationconv,nConcFigures) * 100;
              end if;
            else
              if nReassayDiffType = 2 then
                nDifference := (sigFigure(rReassayEntry.reassayconcentrationconv,nConcFigures) - sigFigure(rOrigEntry.reassayconcentrationconv,nConcFigures)) 
                              / sigFigure(rReassayEntry.reassayconcentrationconv,nConcFigures) * 100;
              elsif nReassayDiffType = 3 then
                nDifference := (sigFigure(rReassayEntry.reassayconcentrationconv,nConcFigures) - sigFigure(rOrigEntry.reassayconcentrationconv,nConcFigures)) 
                              / sigFigure( (sigFigure(rOrigEntry.reassayconcentrationconv,nConcFigures) + sigFigure(rReassayEntry.reassayconcentrationconv,nConcFigures)) / 2 ,nConcFigures) * 100;
              else
                nDifference := (sigFigure(rReassayEntry.reassayconcentrationconv,nConcFigures) - sigFigure(rOrigEntry.reassayconcentrationconv,nConcFigures)) 
                              / sigFigure(rOrigEntry.reassayconcentrationconv,nConcFigures) * 100;
              end if;
            end if;
          end if;
        end if;
        isr$trace.debug('Difference',nDifference, sCurrentName);

        if nPrevConc is not null and sPrevConcStatus is null and rReassayEntry.reassay_concentrationstatus is null then
          if sFinalAccuracy = 'T' then
            nPrevDifference := (rReassayEntry.reassayconcentrationconv - nPrevConc)
                              / ( (nPrevConc + rReassayEntry.reassayconcentrationconv) / 2) * 100;
          else
            if sConcRoundNotSigFig = 'T' then
              nPrevDifference := (round(rReassayEntry.reassayconcentrationconv,nConcFigures) - round(nPrevConc,nConcFigures)) 
                                / round( (round(nPrevConc,nConcFigures) + round(rReassayEntry.reassayconcentrationconv,nConcFigures)) / 2 ,nConcFigures) * 100;
            
            else
              nPrevDifference := (sigFigure(rReassayEntry.reassayconcentrationconv,nConcFigures) - sigFigure(nPrevConc,nConcFigures)) 
                                / sigFigure( (sigFigure(nPrevConc,nConcFigures) + sigFigure(rReassayEntry.reassayconcentrationconv,nConcFigures)) / 2 ,nConcFigures) * 100;
            end if;
          end if;
        end if;
        isr$trace.debug('Difference to previous reassay', nPrevDifference, sCurrentName);
         
        update tmp$BIO$REASSAY$HISTORY
        set DIFFERENCEPERCENT = nDifference, DIFFERENCEPERCENTTOPREV = nPrevDifference
        where code = rReassayEntry.code;
        isr$trace.debug('Difference updated',sql%rowcount, sCurrentName);
        
        nPrevConc := rReassayEntry.reassayconcentrationconv;
        sPrevConcStatus := rReassayEntry.reassay_concentrationstatus;
        nPrevDifference := null;
        isr$trace.debug('Saved conc to compare for against next reassay, nPrevConc: ', nPrevConc, sCurrentName);
        isr$trace.debug('Saved conc status to compare for against next reassay, sPrevConcStatus: ', sPrevConcStatus, sCurrentName);
      end loop;                            
    end loop;
  
  end CalcDifference;

  -- format numeric stability tab information in BIO$run$samples due to MUIB-162
  update TMP$BIO$RUN$SAMPLES
  set hours = SigFigure(hours,16),
      cycles  = SigFigure(cycles,16),
      temperature = SigFigure(temperature,16),
      longtermtime = SigFigure(longtermtime,16);

  -- clear all conditions for entities 
  ISR$Entity$Conditions.RemoveEntityConditions;

  isr$trace.stat('end', 'end', sCurrentName);  
  return oErrorObj;
EXCEPTION
  WHEN OTHERS THEN
    sMsg :=  utd$msglib.getmsg ('exUnexpectedError', stb$security.getcurrentlanguage, csP1 => sqlerrm(sqlcode));
    oErrorObj. handleError (Stb$typedef.cnSeverityCritical, sMsg, sCurrentName, 'SQLCODE = '||SQLCODE||', SQLERRM = '||SQLERRM||' '||dbms_utility.format_error_backtrace);   
    RETURN oErrorObj;    
end PostModelProcessing;


function ProcessPost_BIO$ALLRUN$ANALYTES(coParamList IN iSR$ParamList$Rec) return STB$oError$Record is

  sCurrentName    constant varchar2(100) := $$PLSQL_UNIT||'.ProcessPost_BIO$ALLRUN$ANALYTES';
  oErrorObj                STB$OERROR$RECORD := STB$OERROR$RECORD();
  sMsg                     VARCHAR2(2000);
  nRepID                   STB$REPORT.REPID%TYPE;

begin
  isr$trace.stat('begin', 'begin', sCurrentName);
  
  nRepID := STB$OBJECT.getCurrentRepid;
  isr$trace.debug('nRepID', nRepID, sCurrentName);

  for rParameterVals in (SELECT c1.MASTERKEY RunAnalyteCode, c1.display RunAnalyteStatus, c2.key RunAnalyteValParamStatusID, c2.display RunAnalyteValParamStatus, c3.key RunAnalyteComments
                           FROM ISR$CRIT c1, ISR$CRIT c2, ISR$CRIT c3
                          WHERE c1.entity = 'BIO$COVIB$RUN$ACCEPTANCE'
                            AND c2.entity = 'BIO$COVIB$PARAM$ACCEPTANCE'
                            AND c3.entity = 'BIO$COVIB$RUN$COMMENTS'
                            AND c1.masterkey = c2.masterkey
                            AND c2.masterkey = c3.masterkey
                            AND c1.repid = nRepID
                            AND c1.repid = c2.repid
                            AND c2.repid = c3.repid) loop
    update TMP$BIO$ALLRUN$ANALYTES
    set REGSTATUS = rParameterVals.RunAnalyteStatus,
        VALPARAMSTATUSID = rParameterVals.RunAnalyteValParamStatusID,
        VALPARAMSTATUS = rParameterVals.RunAnalyteValParamStatus,
        COMMENTS = rParameterVals.RunAnalyteComments
    where code = rParameterVals.RunAnalyteCode;
  end loop;

  isr$trace.stat('end', 'end', sCurrentName);  
  return oErrorObj;
EXCEPTION
  WHEN OTHERS THEN
    sMsg :=  utd$msglib.getmsg ('exUnexpectedError', stb$security.getcurrentlanguage, csP1 => sqlerrm(sqlcode));
    oErrorObj. handleError (Stb$typedef.cnSeverityCritical, sMsg, sCurrentName, 'SQLCODE = '||SQLCODE||', SQLERRM = '||SQLERRM||' '||dbms_utility.format_error_backtrace);   
    RETURN oErrorObj;    
end ProcessPost_BIO$ALLRUN$ANALYTES;

function ProcessPost_BIO$COVIB$RUN$SAMPLE$DEACT(coParamList IN iSR$ParamList$Rec) return STB$oError$Record is

  sCurrentName    constant varchar2(100) := $$PLSQL_UNIT||'.ProcessPost_BIO$COVIB$RUN$SAMPLE$DEACT';
  oErrorObj                STB$OERROR$RECORD := STB$OERROR$RECORD();
  sMsg                     VARCHAR2(2000);
  nRepID                   STB$REPORT.REPID%TYPE;
  
  sInWizard       constant varchar2(50)  := coParamList.GetParamStr('INWIZARD');

begin
  isr$trace.stat('begin', 'begin', sCurrentName);
  
  nRepID := STB$OBJECT.getCurrentRepid;
  isr$trace.debug('nRepID', nRepID, sCurrentName);
  
  if sInWizard = 'F' then
    for rParameterVals in (SELECT masterkey WorklistCode, display DeactivationReason
                             FROM ISR$CRIT
                            WHERE entity = 'BIO$COVIB$DEACT$REASON'
                              AND repid = nRepID) loop
      update TMP$BIO$COVIB$RUN$SAMPLE$DEACT
         set REASON = rParameterVals.DeactivationReason
       where code = rParameterVals.WorklistCode;
    end loop;
    
    delete from TMP$BIO$COVIB$RUN$SAMPLE$DEACT
     where CODE not in (SELECT key
                             FROM ISR$CRIT
                            WHERE entity = 'BIO$COVIB$RUN$SAMPLE$DEACT'
                              AND repid = nRepID);
  end if;

  isr$trace.stat('end', 'end', sCurrentName);  
  return oErrorObj;
EXCEPTION
  WHEN OTHERS THEN
    sMsg :=  utd$msglib.getmsg ('exUnexpectedError', stb$security.getcurrentlanguage, csP1 => sqlerrm(sqlcode));
    oErrorObj. handleError (Stb$typedef.cnSeverityCritical, sMsg, sCurrentName, 'SQLCODE = '||SQLCODE||', SQLERRM = '||SQLERRM||' '||dbms_utility.format_error_backtrace);   
    RETURN oErrorObj;    
end ProcessPost_BIO$COVIB$RUN$SAMPLE$DEACT;

function ProcessPost_BIO$COVIB$RUNANA$SMP$DEACT(coParamList IN iSR$ParamList$Rec) return STB$oError$Record is

  sCurrentName    constant varchar2(100) := $$PLSQL_UNIT||'.ProcessPost_BIO$COVIB$RUNANA$SMP$DEACT';
  oErrorObj                STB$OERROR$RECORD := STB$OERROR$RECORD();
  sMsg                     VARCHAR2(2000);
  nRepID                   STB$REPORT.REPID%TYPE;
  
  sInWizard       constant varchar2(50)  := coParamList.GetParamStr('INWIZARD');

begin
  isr$trace.stat('begin', 'begin', sCurrentName);
  
  nRepID := STB$OBJECT.getCurrentRepid;
  isr$trace.debug('nRepID', nRepID, sCurrentName);
  
  if sInWizard = 'F' then
    for rParameterVals in (SELECT masterkey Code, display DeactivationReason
                             FROM ISR$CRIT
                            WHERE entity = 'BIO$COVIB$DEACT$REASON'
                              AND repid = nRepID) loop
      update TMP$BIO$COVIB$RUNANA$SMP$DEACT
         set REASON = rParameterVals.DeactivationReason
       where code = rParameterVals.Code;
    end loop;
    
    delete from TMP$BIO$COVIB$RUNANA$SMP$DEACT
     where CODE not in (SELECT key
                             FROM ISR$CRIT
                            WHERE entity = 'BIO$COVIB$RUNANA$SMP$DEACT'
                              AND repid = nRepID);
  end if;

  isr$trace.stat('end', 'end', sCurrentName);  
  return oErrorObj;
EXCEPTION
  WHEN OTHERS THEN
    sMsg :=  utd$msglib.getmsg ('exUnexpectedError', stb$security.getcurrentlanguage, csP1 => sqlerrm(sqlcode));
    oErrorObj. handleError (Stb$typedef.cnSeverityCritical, sMsg, sCurrentName, 'SQLCODE = '||SQLCODE||', SQLERRM = '||SQLERRM||' '||dbms_utility.format_error_backtrace);   
    RETURN oErrorObj;    
end ProcessPost_BIO$COVIB$RUNANA$SMP$DEACT;

function ProcessPost_BIO$RUN$WORKLIST(coParamList IN iSR$ParamList$Rec) return STB$oError$Record is

  sCurrentName    constant varchar2(100) := $$PLSQL_UNIT||'.ProcessPost_BIO$RUN$WORKLIST';
  sEntity         constant varchar2(50)  := coParamList.GetParamStr('ENTITY');
  oErrorObj                STB$OERROR$RECORD := STB$OERROR$RECORD();
  sMsg                     VARCHAR2(2000);
  nLTMaxDays               number;

begin
  isr$trace.stat('begin', 'begin sEntity: '||sEntity, sCurrentName);
  
  nLTMaxDays := stb$util.GetReportParameterValue('LT_MAX_DAYS', STB$OBJECT.getCurrentRepid);

  update tmp$BIO$RUN$WORKLIST
     set LTDays = trunc(assaydatetime) - trunc(nvl(samplingdatetime, receiptdate)),
         LTStable = case when nLTMaxDays is null then null
                         when trunc(assaydatetime) - trunc(nvl(samplingdatetime, receiptdate)) <= nLTMaxDays then 'Y'
                         when assaydatetime - nvl(samplingdatetime, receiptdate) is null then 'Y'
                         else 'N'
                    end
  where runsamplekind = 'UNKNOWN';  

  isr$trace.stat('end', 'end '||sql%rowcount ||' rows updated' , sCurrentName);  
  return oErrorObj;
EXCEPTION
  WHEN OTHERS THEN
    sMsg :=  utd$msglib.getmsg ('exUnexpectedError', stb$security.getcurrentlanguage, csP1 => sqlerrm(sqlcode));
    oErrorObj. handleError (Stb$typedef.cnSeverityCritical, sMsg, sCurrentName, 'SQLCODE = '||SQLCODE||', SQLERRM = '||SQLERRM||' '||dbms_utility.format_error_backtrace);   
    RETURN oErrorObj;    
end ProcessPost_BIO$RUN$WORKLIST;

function ProcessPost_BIO$RUN$WORKLIST$RESULT$RW(coParamList IN iSR$ParamList$Rec) return STB$oError$Record is

  sCurrentName    constant varchar2(100) := $$PLSQL_UNIT||'.ProcessPost_BIO$RUN$WORKLIST$RESULT$RW';
  sEntity         constant varchar2(50)  := coParamList.GetParamStr('ENTITY');
  oErrorObj                STB$OERROR$RECORD := STB$OERROR$RECORD();
  sMsg                     VARCHAR2(2000);
  sTiterSigFig             VARCHAR2(1);
  nTiterPlaces             NUMBER;
  nTiterMRD                NUMBER;

begin
  isr$trace.stat('begin', 'begin sEntity: '||sEntity, sCurrentName);
  
  sTiterSigFig := case when stb$util.GetReportParameterValue('ROUND_OR_SIGFIG_TITER', STB$OBJECT.getCurrentRepid) = 'sigfig' then 'T' else 'F' end;
  nTiterPlaces := stb$util.GetReportParameterValue('PLACES_TITER', STB$OBJECT.getCurrentRepid);
  nTiterMRD := stb$util.GetReportParameterValue('TITER_MRD', STB$OBJECT.getCurrentRepid);

  isr$trace.debug('sTiterSigFig', sTiterSigFig, sCurrentName);
  isr$trace.debug('nTiterPlaces', nTiterPlaces, sCurrentName);
  isr$trace.debug('nTiterMRD', nTiterMRD, sCurrentName);
  
  if nTiterMRD is not null then
    update tmp$BIO$RUN$WORKLIST$RESULT$RW
       set AnalyteArea = AnalyteArea * nTiterMRD,
           ResultText = case
                          when regexp_like(ResultText,'^\d+\.?\d{0,}$','i') then
                            case
                              when sTiterSigFig = 'T' then FormatSig(sigFigure(to_char(to_number(ResultText) * nTiterMRD), nTiterPlaces), nTiterPlaces)
                              else FormatRounded(round(to_char(to_number(ResultText) * nTiterMRD), nTiterPlaces), nTiterPlaces)
                            end
                          when regexp_like(ResultText,'^(.*\([<>]?)(\d+\.?\d{0,})(\).*)$','i') then
                            REGEXP_REPLACE(ResultText, '^(.*\([<>]?)(\d+\.?\d{0,})(\).*)$', '\1') ||
                            case
                              when sTiterSigFig = 'T' then FormatSig(sigFigure(to_char(to_number(REGEXP_REPLACE(ResultText, '^(.*\([<>]?)(\d+\.?\d{0,})(\).*)$', '\2')) * nTiterMRD), nTiterPlaces), nTiterPlaces)
                              else FormatRounded(round(to_char(to_number(REGEXP_REPLACE(ResultText, '^(.*\([<>]?)(\d+\.?\d{0,})(\).*)$', '\2')) * nTiterMRD), nTiterPlaces), nTiterPlaces)
                            end
                            || REGEXP_REPLACE(ResultText, '^(.*\([<>]?)(\d+\.?\d{0,})(\).*)$', '\3')
                          else ResultText
                      end,
          Result = case
                          when regexp_like(Result,'^\d+\.?\d{0,}$','i') then
                            case
                              when sTiterSigFig = 'T' then FormatSig(sigFigure(to_char(to_number(Result) * nTiterMRD), nTiterPlaces), nTiterPlaces)
                              else FormatRounded(round(to_char(to_number(Result) * nTiterMRD), nTiterPlaces), nTiterPlaces)
                            end
                          when regexp_like(Result,'^(.*\([<>]?)(\d+\.?\d{0,})(\).*)$','i') then
                            REGEXP_REPLACE(Result, '^(.*\([<>]?)(\d+\.?\d{0,})(\).*)$', '\1') ||
                            case
                              when sTiterSigFig = 'T' then FormatSig(sigFigure(to_char(to_number(REGEXP_REPLACE(Result, '^(.*\([<>]?)(\d+\.?\d{0,})(\).*)$', '\2')) * nTiterMRD), nTiterPlaces), nTiterPlaces)
                              else FormatRounded(round(to_char(to_number(REGEXP_REPLACE(Result, '^(.*\([<>]?)(\d+\.?\d{0,})(\).*)$', '\2')) * nTiterMRD), nTiterPlaces), nTiterPlaces)
                            end
                            || REGEXP_REPLACE(Result, '^(.*\([<>]?)(\d+\.?\d{0,})(\).*)$', '\3')
                          else Result
                      end
    where WorklistCode in (
      select Code
        from tmp$BIO$RUN$WORKLIST    
       where SampleSubType = 'Titer'
    );
  end if;

  isr$trace.stat('end', 'end '||sql%rowcount ||' rows updated' , sCurrentName);  
  return oErrorObj;
EXCEPTION
  WHEN OTHERS THEN
    sMsg :=  utd$msglib.getmsg ('exUnexpectedError', stb$security.getcurrentlanguage, csP1 => sqlerrm(sqlcode));
    oErrorObj. handleError (Stb$typedef.cnSeverityCritical, sMsg, sCurrentName, 'SQLCODE = '||SQLCODE||', SQLERRM = '||SQLERRM||' '||dbms_utility.format_error_backtrace);   
    RETURN oErrorObj;    
end ProcessPost_BIO$RUN$WORKLIST$RESULT$RW;

function ProcessPost_BIO$BTInfo(coParamList IN iSR$ParamList$Rec) return STB$oError$Record is

  sCurrentName    constant varchar2(100) := $$PLSQL_UNIT||'.ProcessPost_BIO$BTINFO';
  sEntity         constant varchar2(50)  := coParamList.GetParamStr('ENTITY');
  oErrorObj                STB$OERROR$RECORD := STB$OERROR$RECORD();
  sMsg                     VARCHAR2(2000);
  nBTMaxHours              number;

begin
  isr$trace.stat('begin', 'begin sEntity: '||sEntity, sCurrentName);
  
  nBTMaxHours := stb$util.GetReportParameterValue('BT_MAX_HOURS', STB$OBJECT.getCurrentRepid);
 
  if nBTMaxHours is not null then 
    update tmp$BIO$BTINFO
      set BTStable = case when  BTTime <= nBTMaxHours then 'Y' 
                          when BTTime is null then 'Y'
                          else 'N'
                     end; 
  end if;
  
  isr$trace.stat('end', 'end '||sql%rowcount ||' rows updated', sCurrentName);  
  return oErrorObj;
EXCEPTION
  WHEN OTHERS THEN
    sMsg :=  utd$msglib.getmsg ('exUnexpectedError', stb$security.getcurrentlanguage, csP1 => sqlerrm(sqlcode));
    oErrorObj. handleError (Stb$typedef.cnSeverityCritical, sMsg, sCurrentName, 'SQLCODE = '||SQLCODE||', SQLERRM = '||SQLERRM||' '||dbms_utility.format_error_backtrace);   
    RETURN oErrorObj;    
end ProcessPost_BIO$BTInfo;

function ProcessPost_BIO$FTInfo(coParamList IN iSR$ParamList$Rec) return STB$oError$Record is

  sCurrentName    constant varchar2(100) := $$PLSQL_UNIT||'.ProcessPost_BIO$FTINFO';
  sEntity         constant varchar2(50)  := coParamList.GetParamStr('ENTITY');
  oErrorObj                STB$OERROR$RECORD := STB$OERROR$RECORD();
  sMsg                     VARCHAR2(2000);
  nFTMaxCycles             number;

begin
  isr$trace.stat('begin', 'begin sEntity: '||sEntity, sCurrentName);
  
  nFTMaxCycles := stb$util.GetReportParameterValue('FT_MAX_CYCLES', STB$OBJECT.getCurrentRepid);
  
  if nFTMaxCycles is not null then
    update tmp$BIO$FTINFO
      set FTStable = case when FREEZETHAWCOUNT  <= nFTMaxCycles then 'Y' 
                          when FREEZETHAWCOUNT is null then 'Y'
                          else 'N'
                     end; 
  end if;
  
  isr$trace.stat('end', 'end '||sql%rowcount ||' rows updated', sCurrentName);  
  return oErrorObj;
EXCEPTION
  WHEN OTHERS THEN
    sMsg :=  utd$msglib.getmsg ('exUnexpectedError', stb$security.getcurrentlanguage, csP1 => sqlerrm(sqlcode));
    oErrorObj. handleError (Stb$typedef.cnSeverityCritical, sMsg, sCurrentName, 'SQLCODE = '||SQLCODE||', SQLERRM = '||SQLERRM||' '||dbms_utility.format_error_backtrace);   
    RETURN oErrorObj;    
end ProcessPost_BIO$FTInfo;


function ProcessPost_GridUnits(coParamList IN iSR$ParamList$Rec) return STB$oError$Record is

  sCurrentName    constant varchar2(100) := $$PLSQL_UNIT||'.ProcessPost_GridUnits';
  sEntity         constant varchar2(50)  := coParamList.GetParamStr('ENTITY');
  sMasterkey      constant varchar2(50)  := coParamList.GetParamStr('MASTERKEY');
  oErrorObj                STB$OERROR$RECORD := STB$OERROR$RECORD();
  sMsg                     VARCHAR2(2000);
  nFTMaxCycles             number;
  nRepID                   number := coParamList.GetParamNum('REPID');

begin
  isr$trace.stat('begin', 'begin sEntity: '||sEntity, sCurrentName);
  
  isr$trace.debug('coParamList for sEntity', 'see lob', sCurrentName, coParamList);
  
  delete from TMP$BIO$GRID$STUDY$ANALYUNITS
  where originalconcentrationunit not in
  (select info   
     from isr$crit 
   where entity = 'BIO$GRID$STUDY$ANALYTES' 
     and repid = nRepID
     and key = sMasterkey);
     
  isr$trace.debug('rows deleted', sql%rowcount, sCurrentName);
  
  isr$trace.stat('end', 'end', sCurrentName);  
  return oErrorObj;
EXCEPTION
  WHEN OTHERS THEN
    sMsg :=  utd$msglib.getmsg ('exUnexpectedError', stb$security.getcurrentlanguage, csP1 => sqlerrm(sqlcode));
    oErrorObj. handleError (Stb$typedef.cnSeverityCritical, sMsg, sCurrentName, 'SQLCODE = '||SQLCODE||', SQLERRM = '||SQLERRM||' '||dbms_utility.format_error_backtrace);   
    RETURN oErrorObj;    
end ProcessPost_GridUnits;

function ProcessPost_RunAnaStabConc(coParamList IN iSR$ParamList$Rec) return STB$oError$Record is

  sCurrentName    constant varchar2(100) := $$PLSQL_UNIT||'.ProcessPost_RunAnaStabConc';
  sEntity         constant varchar2(50)  := coParamList.GetParamStr('ENTITY');
  --sMasterkey      constant varchar2(50)  := coParamList.GetParamStr('MASTERKEY');
  oErrorObj                STB$OERROR$RECORD := STB$OERROR$RECORD();
  sMsg                     VARCHAR2(2000);
  nRepID                   number := coParamList.GetParamNum('REPID');

begin
  isr$trace.stat('begin', 'begin sEntity: '||sEntity, sCurrentName);
  
  delete from tmp$BIO$RUN$ANA$STABILITY$CONC
  where nameprefix not in
    (select distinct KEY
       from isr$crit c
      where entity = 'BIO$KNOWN'
        and masterkey in ('S_REFRIGERATED','S_SOLUTION','S_FREEZETHAW','S_BENCHTOP','S_LONGTERM')
        and repid = nRepID);
        
  isr$trace.debug('rows deleted for other knowns', sql%rowcount, sCurrentName);  
  
  delete tmp$BIO$RUN$ANA$STABILITY$CONC
   where rowid not in (select min(rowid)
                       from tmp$BIO$RUN$ANA$STABILITY$CONC
                       group by runcode, analytecode, concentration);
   
  isr$trace.debug('duplicate rows deleted', sql%rowcount, sCurrentName);
   
  update tmp$BIO$RUN$ANA$STABILITY$CONC
  set runanaconctext = 'Run: '||runno||', Analyte: '||analytename||', Nominalconc: '||concentration;
  
  isr$trace.debug('rows updated', sql%rowcount, sCurrentName);
                      
  isr$trace.stat('end', 'end', sCurrentName);  
  return oErrorObj;
EXCEPTION
  WHEN OTHERS THEN
    sMsg :=  utd$msglib.getmsg ('exUnexpectedError', stb$security.getcurrentlanguage, csP1 => sqlerrm(sqlcode));
    oErrorObj. handleError (Stb$typedef.cnSeverityCritical, sMsg, sCurrentName, 'SQLCODE = '||SQLCODE||', SQLERRM = '||SQLERRM||' '||dbms_utility.format_error_backtrace);   
    RETURN oErrorObj;    
end ProcessPost_RunAnaStabConc;


end ISR$COVIB$Trigger;