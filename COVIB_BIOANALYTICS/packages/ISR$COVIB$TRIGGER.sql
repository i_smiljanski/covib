CREATE OR REPLACE PACKAGE ISR$COVIB$TRIGGER
as
-- ***************************************************************************************************
-- Description/Usage:
-- Contains pre and post Processing functionality for COVIB
-- ***************************************************************************************************


function FillReportConditions(cnReportTypeID in number, cnRepID in integer) return STB$OERROR$RECORD;
-- ***************************************************************************************************
-- Date und Autor: 12.Mar 2018  HR
-- Makes initializations for entities 
--
-- Parameter:
-- cnReportTypeID       the report type
-- cnRepID              the report id
--
--
-- Return:
-- STB$OERROR$RECORD   
--
-- ***************************************************************************************************


function PostModelProcessing return STB$oError$Record;
-- ***************************************************************************************************
-- Date und Autor: 8.Nov 2016 HR
-- makes the Post processing for the reports
--
-- Return:
-- STB$OERROR$RECORD   
--
-- ***************************************************************************************************

function  ProcessPre_BIO$USERSEC(coParamList IN iSR$ParamList$Rec) return STB$oError$Record;
-- ***************************************************************************************************
-- Date und Autor: 18.Nov 2019 HR
-- makes the Pre processing for the entity BIO$USERSEC to limit the entries for the limsusernamne
--
-- Parameter:
-- coParamList   the parameter list
--
-- Return:
-- STB$OERROR$RECORD  
--
-- ***************************************************************************************************

function  ProcessPre_BIO$STUDY(coParamList IN iSR$ParamList$Rec) return STB$oError$Record;
-- ***************************************************************************************************
-- Date und Autor: 18.Nov 2019 HR
-- makes the Pre processing for the entity BIO$STUDY to limit the entries for the RIGHTSKEY entries in 
--   ISR$REMOTE$SECURITY$KEYS
--
-- Parameter:
-- coParamList   the parameter list
--
-- Return:
-- STB$OERROR$RECORD  
--
-- ***************************************************************************************************

function ProcessPost_BIO$RUN$ANALYTES(coParamList IN iSR$ParamList$Rec) return STB$oError$Record;
-- ***************************************************************************************************
-- Date und Autor: 24.Oct 2018 HSP
-- makes the Post processing for the entity BIO$RUN$ANALYTES to adjust BIO$RUN
-- changes the RUNREGRESSIONSTATUS to the one specified in the Wizard (if it was specified) 
--
-- Parameter:
-- coParamList   the parameter list
--
-- Return:
-- STB$OERROR$RECORD  
--
-- ***************************************************************************************************

function ProcessPost_BIO$DES$FIELDCHNG(coParamList IN iSR$ParamList$Rec) return STB$oError$Record;
-- ***************************************************************************************************
-- Date und Autor: 1.Dec 2021 HR
-- makes the Post processing for the entity BIO$SAMPLESTATUS 
--
-- Parameter:
-- coParamList   the parameter list
--
-- Return:
-- STB$OERROR$RECORD  
--
-- ***************************************************************************************************

function ProcessPost_BIO$SAMPLESTATUS(coParamList IN iSR$ParamList$Rec) return STB$oError$Record;
-- ***************************************************************************************************
-- Date und Autor: 14.Mar 2018 HR
-- makes the Post processing for the entity BIO$SAMPLESTATUS 
--
-- Parameter:
-- coParamList   the parameter list
--
-- Return:
-- STB$OERROR$RECORD  
--
-- ***************************************************************************************************


function ProcessPost_BIO$REASSAY$HIST(coParamList IN iSR$ParamList$Rec) return STB$oError$Record;
-- ***************************************************************************************************
-- Date und Autor: 16.Apr 2018 HR
-- makes the Post processing for the entity BIO$REASSAY$HISTORY
--
-- Parameter:
-- coParamList   the parameter list
--
-- Return:
-- STB$OERROR$RECORD  
--
-- ***************************************************************************************************


function ProcessPost_BIO$RUN$ANA$KNOWN(coParamList IN iSR$ParamList$Rec) return STB$oError$Record;
-- ***************************************************************************************************
-- Date und Autor: 14.Mar 2018 HR
-- makes the Post processing for the entity BIO$RUN$ANA$KNOWN 
--
-- Parameter:
-- coParamList   the parameter list
--
-- Return:
-- STB$OERROR$RECORD  
--
-- ***************************************************************************************************


function ProcessPost_BIO$ALLRUN$ANALYTES(coParamList IN iSR$ParamList$Rec) return STB$oError$Record;
-- ***************************************************************************************************
-- Date und Autor: 24.Oct 2019 HSP
-- changes the REGSTATUS to the one specified in the Wizard (if it was specified) and sets the VALPARAM(/ID) and COMMENTS if specified in the Wizard
--
-- Parameter:
-- coParamList   the parameter list
--
-- Return:
-- STB$OERROR$RECORD  
--
-- ***************************************************************************************************


function ProcessPost_BIO$COVIB$RUN$SAMPLE$DEACT(coParamList IN iSR$ParamList$Rec) return STB$oError$Record;
-- ***************************************************************************************************
-- Date und Autor: 19.Nov 2019 HSP
-- changes the REASON to the one specified in the Wizard (if it was specified)
--
-- Parameter:
-- coParamList   the parameter list
--
-- Return:
-- STB$OERROR$RECORD  
--
-- ***************************************************************************************************


function ProcessPost_BIO$COVIB$RUNANA$SMP$DEACT(coParamList IN iSR$ParamList$Rec) return STB$oError$Record;
-- ***************************************************************************************************
-- Date und Autor: 13.Sep 2021 HSP
-- changes the REASON to the one specified in the Wizard (if it was specified)
--
-- Parameter:
-- coParamList   the parameter list
--
-- Return:
-- STB$OERROR$RECORD  
--
-- ***************************************************************************************************


function ProcessPost_BIO$RUN$WORKLIST(coParamList IN iSR$ParamList$Rec) return STB$oError$Record;
-- ***************************************************************************************************
-- Date und Autor: 11.May 2020 HR
-- makes the Post processing for the entity BIO$RUN$WORKLIST 
--
-- Parameter:
-- coParamList   the parameter list
--
-- Return:
-- STB$OERROR$RECORD  
--
-- ***************************************************************************************************

function ProcessPost_BIO$RUN$WORKLIST$RESULT$RW(coParamList IN iSR$ParamList$Rec) return STB$oError$Record;
-- ***************************************************************************************************
-- Date und Autor: 12.Aug 2021 HSP
-- makes the Post processing for the entity BIO$RUN$WORKLIST$RESULT$RW 
--
-- Parameter:
-- coParamList   the parameter list
--
-- Return:
-- STB$OERROR$RECORD  
--
-- ***************************************************************************************************

function ProcessPost_BIO$BTInfo(coParamList IN iSR$ParamList$Rec) return STB$oError$Record;
-- ***************************************************************************************************
-- Date und Autor: 11.May 2020 HR
-- makes the Post processing for the entity BIO$RUN$BTInfo 
--
-- Parameter:
-- coParamList   the parameter list
--
-- Return:
-- STB$OERROR$RECORD  
--
-- ***************************************************************************************************

function ProcessPost_BIO$FTInfo(coParamList IN iSR$ParamList$Rec) return STB$oError$Record;
-- ***************************************************************************************************
-- Date und Autor: 11.May 2020 HR
-- makes the Post processing for the entity BIO$RUN$LTInfo 
--
-- Parameter:
-- coParamList   the parameter list
--
-- Return:
-- STB$OERROR$RECORD  
--
-- ***************************************************************************************************

function ProcessPost_GridUnits(coParamList IN iSR$ParamList$Rec) return STB$oError$Record;

function ProcessPost_RunAnaStabConc(coParamList IN iSR$ParamList$Rec) return STB$oError$Record;
-- ***************************************************************************************************
-- Date und Autor: 7.Aug 2020 HR
-- makes the Post processing for the entity BIO$RUN$ANA$STABILITY$CONC
--
-- Parameter:
-- coParamList   the parameter list
--
-- Return:
-- STB$OERROR$RECORD  
--
-- ***************************************************************************************************

END ISR$COVIB$Trigger;