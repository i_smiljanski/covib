CREATE OR REPLACE type ISR$COVIB$SST$LIMIT$REC as object (
  analyteid varchar2(4000),
  name varchar2(4000),
  mean_plus_xsd number,
  mean_minus_xsd number
);