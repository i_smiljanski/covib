CREATE OR REPLACE FORCE VIEW "ISR$BIO$STUDYSUBJ$GRP$DAY" ("ENTITY", "CODE", "STUDYCODE", "SUBJECTGROUPCODE", "SUBJECTGROUPTREATMENTCODE", "GENDER", "DESIGNSUBJECTTAG", "VISIT", "YEAR", "PHASE", "MONTH", "WEEK", "PERIOD", "STUDYDAY", "TIME_GENDER", "TREATMENT_GENDER", "TREATMENTNAME", "TREATMENTTITLE", "GENDER_TITLE") AS 
  select 'BIO$STUDYSUBJ$GRP$DAY' as ENTITY
       , s.code || '-' || t.studyday as CODE
       , g.studycode as STUDYCODE
       , s.subjectgroupcode as SUBJECTGROUPCODE
       , t.code as SUBJECTGROUPTREATMENTCODE
       , s.gender as GENDER
       , s.designsubjecttag as DESIGNSUBJECTTAG
       , t.visit as VISIT
       , t.year as YEAR
       , t.phase as PHASE
       , t.month as MONTH
       , t.week as WEEK
       , t.period as PERIOD
       , T.studyday as STUDYDAY
       , t.timeident || '_' || s.gender as TIME_GENDER
       , tm.name || '_' || s.gender as TREATMENT_GENDER -- MUIB specific
       , tm.name as TRESTMENTNAME -- MUIB specific
       , tm.title as TREATMENTITLE
       , case when s.gender is not null then s.gender || ' ' else null end || tm.title as GENDER_TITLE -- MUIB specific
    from ISR$BIO$STUDY$SUBJECTGROUP g
       , ISR$BIO$STUDYSUBJ$GRP$TREATM t
       , ISR$BIO$STUDY$SUBJECT s
       , ISR$BIO$STUDY$TREATMENT tm
   where g.studycode = t.studycode
     and s.subjectgroupcode = g.code
     and s.studycode = g.studycode
     and g.code = t.subjectgroupcode
     and (tm.doseamount is not null
       or tm.doseamount <> 0)
     and tm.Studycode = g.studycode
     and t.treatmentcode = tm.code
     and tm.doseamount != 0
  order by period
, year, month
, week
, studyday
, time_gender