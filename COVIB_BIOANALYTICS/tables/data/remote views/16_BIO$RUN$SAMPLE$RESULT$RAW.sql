CREATE OR REPLACE FORCE VIEW "ISR$BIO$RUN$SAMPLE$RESULT$RAW" ("ENTITY", "CODE", "SAMPLERESULTCODE", "RUNSAMPLECODE", "RUNCODE", "STUDYCODE", "RUNANALYTECODE", "SAMPLENAME", "ANALYTEHEIGHT", "ANALYTEHEIGHTSTATUS", "ANALYTEAREA", "ANALYTEAREASTATUS", "ANALYTEPEAKRETENTIONTIME", "INTERNALSTANDARDHEIGHT", "INTERNALSTANDARDHEIGHTSTATUS", "INTERNALSTANDARDAREA", "INTERNALSTANDARDAREASTATUS", "INTERNALSTANDARDRETENTIONTIME", "INTERNALSTDNAME", "RATIO", "RATIOSTATUS", "STATUS", "COMMENTTEXT", "PLATE") AS 
  select 'BIO$RUN$SAMPLE$RESULT$RAW' as ENTITY
       , A.studyid || '-' || A.runid || '-' || A.runsamplesequencenumber || '-' || AA.ANALYTEID || A.plate as CODE
       , A.studyid || '-' || A.runid || '-' || A.runsamplesequencenumber || '-' || AA.ANALYTEID as SAMPLERESULTCODE
       , A.studyid || '-' || A.runid || '-' || A.runsamplesequencenumber || '-' || AA.ANALYTEID as RUNSAMPLECODE
       , A.studyid || '-' || A.runid as RUNCODE
       , A.studyid as STUDYCODE
       , A.studyid || '-' || A.runid || '-' || AA.ANALYTEID as RUNANALYTECODE
       , A.samplename as SAMPLENAME
       , A.analyteheight as ANALYTEHEIGHT
       , A.analyteheightstatus as ANALYTEHEIGHTSTATUS
       , A.analytearea as ANALYTEAREA
       , A.analyteareastatus as ANALYTEAREASTATUS
       , A.analytepeakretentiontime as ANALYTEPEAKRETENTIONTIME
       , A.internalstandardheight as INTERNALSTANDARDHEIGHT
       , A.internalstandardheightstatus as INTERNALSTANDARDHEIGHTSTATUS
       , A.internalstandardarea as INTERNALSTANDARDAREA
       , A.internalstandardareastatus as INTERNALSTANDARDAREASTATUS
       , A.internalstandardretentiontime as INTERNALSTANDARDRETENTIONTIME
       , A.internalstdname as INTERNALSTDNAME
       , A.ratio as ratio
       , A.ratiostatus as RATIOSTATUS
       , A.status as STATUS
       , A.commenttext as COMMENTTEXT
       , A.plate as PLATE
    from &&LIMS_USER&.anarunrawanalytepeak A
       , &&LIMS_USER&.assayanalytes aa
       , &&LIMS_USER&.assay ass
   where aa.studyid = A.studyid
     and ass.runid = A.RUNID
     and ass.assayid = Aa.assayid
     and AA.ANALYTEINDEX = A.ANALYTEINDEX
     and ass.studyid = a.studyid