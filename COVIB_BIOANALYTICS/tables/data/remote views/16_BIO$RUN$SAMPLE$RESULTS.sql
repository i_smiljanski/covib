CREATE OR REPLACE FORCE VIEW "ISR$BIO$RUN$SAMPLE$RESULTS" ("ENTITY", "CODE", "RUNSAMPLECODE", "RUNCODE", "STUDYCODE", "RUNANALYTECODE", "CONCENTRATION", "CORRECTEDCONCENTRATION", "CONCENTRATIONSTATUS", "ELIMINATEDFLAG", "RESULTCOMMENTTEXT", "RESULT", "RESULTTEXT", "NOMINALCONCENTRATION", "ISCHOICE") AS 
  select 'BIO$RUN$SAMPLE$RESULTS' as ENTITY
       , a.studyid || '-' || a.runid || '-' || A.runsamplesequencenumber || '-' || aa.analyteid as CODE
       , a.studyid || '-' || a.runid || '-' || A.runsamplesequencenumber || '-' || aa.analyteid as RUNSAMPLECODE
       , a.studyid || '-' || a.runid as RUNCODE
       , a.studyid as STUDYCODE
       , a.studyid || '-' || a.runid || '-' || aa.analyteid as RUNANALYTECODE
       , A.concentration as CONCENTRATION
       , A.correctedconcentration as CORRECTEDCONCENTRATION
       , A.concentrationstatus as CONCENTRATIONSTATUS
       , A.eliminatedflag as ELIMINATEDFLAG
       , A.resultcommenttext as RESULTCOMMENTTEXT
       , A.RESULT as RESULT
       , A.resulttext as RESULTTEXT
       , (SELECT round(ak.concentration, 12) 
            FROM &&LIMS_USER&.ASSAYANALYTEKNOWN ak
           WHERE ak.assayid = aa.assayid
             AND ak.studyid = ars.studyid
             AND ak.analyteindex = aa.analyteindex
             AND ak.levelnumber = ars.assaylevel
             AND ak.knowntype = ars.runsamplekind) as NOMINALCONCENTRATION
       , (select case when count(1) > 0 then 'Y' else 'N' end
            from &&LIMS_USER&.SAMPRESCONFLICTCHOICES sc
           where sc.studyid = ars.studyid
             and sc.analyteid = aa.analyteid
             and sc.designsampleid = ars.designsampleid
             and sc.runid = ars.runid
             and sc.runsamplesequencenumber = ars.runsamplesequencenumber)
           as ISCHOICE
    from &&LIMS_USER&.ANARUNANALYTERESULTS A
       , &&LIMS_USER&.ASSAYANALYTES aa
       , &&LIMS_USER&.ASSAY ass --kann darauf nicht verzichten
       , &&LIMS_USER&.ANALYTICALRUNSAMPLE ars
   where aa.studyid = a.studyid
     and ass.runid = a.runid
     and ars.runid = a.runid
     and ass.assayid = Aa.assayid
     and aa.analyteindex = a.analyteindex
     and ars.studyid = a.studyid --sa.studyid = ars.studyid
     and ars.runsamplesequencenumber = a.runsamplesequencenumber