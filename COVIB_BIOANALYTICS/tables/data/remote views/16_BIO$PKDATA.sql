CREATE OR REPLACE FORCE VIEW "ISR$BIO$PKDATA" ("ENTITY", "CODE", "TEMPLATENAME", "PARAMETER") AS 
  select 'BIO$PKDATA' as ENTITY,
          templatename || '-' || element_no as CODE,
          templatename,
          parameter as PARAMETER
     from (with row_count
                as (select length (templatedata) - length (replace (templatedata, chr (9), '')) + 1
                            as rowcnt,
                           templatedata,
                           templatename
                      from (select c.templatetype,
                                   c.templatename,
                                   replace (trim (c.templatedata),
                                            '    ',
                                            chr (9))
                                      as templatedata,
                                   c.templatedefault
                              from &&LIMS_USER&.CONFIGTEMPLATE c
                             where templatetype = 'PK Templates'))
           select rtrim (templatedata, chr (9)) as templatedata,
                  templatename,
                  case when next_pos = 0 then
                        substr (templatedata, start_pos, length (templatedata) - start_pos + 1)
                       else
                        substr (templatedata, start_pos, (next_pos - start_pos))
                  end as parameter,
                  element_no
             from (select row_count.templatedata,
                          row_count.templatename,
                          nt.column_value as element_no,
                          instr (
                               row_count.templatedata,
                               chr (9),
                               decode (nt.column_value, 1, 0, 1),
                               decode (nt.column_value,
                                       1, 1,
                                       nt.column_value - 1))
                            + 1
                             as start_pos,
                          instr (
                             row_count.templatedata,
                             chr (9),
                             1,
                             decode (nt.column_value, 1, 1, nt.column_value))
                             as next_pos
                     from row_count,
                          table (
                             cast (
                                multiset (
                                       select rownum
                                         from dual
                                   connect by rownum <= row_count.rowcnt) as sys.odcinumberlist)) nt))
    where parameter not like '%Deactivated'