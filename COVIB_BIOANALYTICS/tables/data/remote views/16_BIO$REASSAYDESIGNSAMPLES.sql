CREATE OR REPLACE FORCE VIEW "ISR$BIO$REASSAYDESIGNSAMPLES" ("ENTITY", "CODE", "STUDYCODE", "FROMSAMPLEID", "TOSAMPLEID", "SPECIMENKEY") AS 
  select 'BIO$REASSAYDESIGNSAMPLES' as ENTITY
       ,    DS1.StudyId
         || '-'
         || DS1.DesignSampleId
         || '-'
         || DS2.DesignSampleId
         || '-'
         || to_char(DS1.STUDYID)
         || 'q'
         || to_char(DS1.DESIGNSUBJECTTREATMENTKEY)
         || 'q'
         || to_char(DS1.SAMPLETYPEKEY)
         || 'q'
         || to_char(nvl(DS1.STARTDAY, 0))
         || 'q'
         || to_char(nvl(DS1.STARTHOUR, 0))
         || 'q'
         || to_char(nvl(DS1.STARTMINUTE, 0))
         || 'q'
         || to_char(nvl(DS1.STARTSECOND, 0))
         || 'q'
         || to_char(nvl(DS1.ENDDAY, 0))
         || 'q'
         || to_char(nvl(DS1.ENDHOUR, 0))
         || 'q'
         || to_char(nvl(DS1.ENDMINUTE, 0))
         || 'q'
         || to_char(nvl(DS1.ENDSECOND, 0))
           as CODE
       , DS1.StudyId as STUDYCODE
       , DS1.DesignSampleId as FROMSAMPLEID
       , DS2.DesignSampleId as TOSAMPLEID
       ,    to_char(DS1.STUDYID)
         || 'q'
         || to_char(DS1.DESIGNSUBJECTTREATMENTKEY)
         || 'q'
         || to_char(DS1.SAMPLETYPEKEY)
         || 'q'
         || to_char(nvl(DS1.STARTDAY, 0))
         || 'q'
         || to_char(nvl(DS1.STARTHOUR, 0))
         || 'q'
         || to_char(nvl(DS1.STARTMINUTE, 0))
         || 'q'
         || to_char(nvl(DS1.STARTSECOND, 0))
         || 'q'
         || to_char(nvl(DS1.ENDDAY, 0))
         || 'q'
         || to_char(nvl(DS1.ENDHOUR, 0))
         || 'q'
         || to_char(nvl(DS1.ENDMINUTE, 0))
         || 'q'
         || to_char(nvl(DS1.ENDSECOND, 0))
           as SPECIMENKEY
    from ISR$BIO$DESIGNSAMPLE$MODIFIED DS1, ISR$BIO$DESIGNSAMPLE$MODIFIED DS2
   where DS1.STUDYID = DS2.STUDYID
     and DS1.DESIGNSUBJECTTREATMENTKEY = DS2.DESIGNSUBJECTTREATMENTKEY
     and DS1.SAMPLETYPEKEY = DS2.SAMPLETYPEKEY
     and nvl(DS1.STARTDAY, -987.123) = nvl(DS2.STARTDAY, -987.123)
     and nvl(DS1.STARTHOUR, -987.123) = nvl(DS2.STARTHOUR, -987.123)
     and nvl(DS1.STARTMINUTE, -987.123) = nvl(DS2.STARTMINUTE, -987.123)
     and nvl(DS1.STARTSECOND, -987.123) = nvl(DS2.STARTSECOND, -987.123)
     and nvl(DS1.ENDDAY, -987.123) = nvl(DS2.ENDDAY, -987.123)
     and nvl(DS1.ENDHOUR, -987.123) = nvl(DS2.ENDHOUR, -987.123)
     and nvl(DS1.ENDMINUTE, -987.123) = nvl(DS2.ENDMINUTE, -987.123)
     and nvl(DS1.ENDSECOND, -987.123) = nvl(DS2.ENDSECOND, -987.123)