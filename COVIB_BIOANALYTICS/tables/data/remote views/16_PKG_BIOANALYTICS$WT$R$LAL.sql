CREATE OR REPLACE PACKAGE ISR$BIOANALYTICS$WT$R$LAL
AS
-- ***************************************************************************************************
-- Description/Usage:
-- the package is the inerface between the underlaying Watson LIMS of Abbott Laboratories 
-- to the backend of iStudyReporter  
-- ***************************************************************************************************
-- Modification history:
-- Ver   Date          Autor    System            Project                         Remarks
-- ***************************************************************************************************
-- 1.0   28.May 2008  JB       iStudyReporter    iStudyReproter@watson           Creation
--
---- ***************************************************************************************************
---- global declarations (constants, types and variables)
---- ****************************************************************************************************
  type tStabRec is record(cycles number,
                          temperature number,
                          hours number,
                          longtermtime number,
                          longtermunits varchar2(5));

  type tlStabList is table of tStabRec;

  type tlAnaRunAnaRec is record(RegStatus varchar2(50),
                                nm number,
                                vec number,
                                reason varchar2(100));

  type tlAnaRunAnaList is table of tlAnaRunAnaRec;   


-- ***************************************************************************************************
-- functions and procedures
-- ****************************************************************************************************

Function GetAnaRunAnaData(cnAnalyteIndex in number, cnStudyID in number, cnRunID in number) return tlAnaRunAnaList pipelined;
-- ***************************************************************************************************
-- Date and Autor:  04.Jun 2009, HR
-- function retrieves values from ANALYTICALRUNANALYTES  and CONFIGANALYTERUNSTATUS 
-- for the Analyte Index, StudyID and RunID from the parameters
-- and pipes them in a record of type iSR$AnaRunAna$Rec  
--
-- Parameter :
--  cnAnalyteIndex             the analyte index
--  cnStudyID                  the studyid
--  cnRunID                    the runid
--
-- Return:
-- iSR$AnaRunAna$List          list of type iSR$AnaRunAna$List but always one record per function call       
--
-- ***************************************************************************************************

FUNCTION getFirstReceiveDate (nStudycode IN NUMBER) return date;
-- ***************************************************************************************************
-- Date and Autor:  03.Apr 2009, JB
-- function retrieves the first receiving date of a sample for a study
--
-- Parameter :
--  nStudycode                  the study id
--
-- Return:
-- the first receiving date                   
--
-- ***************************************************************************************************


FUNCTION getLastReceiveDate (nStudycode IN NUMBER) return date;
-- ***************************************************************************************************
-- Date and Autor:  03.Apr 2009, JB
-- ffunction retrieves the last receiving date of a sample for a study
--
-- Parameter :
--  nStudycode                  the study id
--
-- Return:
-- the last receiving date                 
--
-- ***************************************************************************************************

FUNCTION GetAssayStartDate (cnStudy IN number) RETURN date;
-- ***************************************************************************************************
-- Date and Autor:  10.Feb 2009 HR
-- retrieves the assay start date of a study
--
-- Parameters:
-- cnStudy          the study id
--
-- Return:
-- the assay start date
--
-- ***************************************************************************************************

FUNCTION GetAssayEndDate (cnStudy IN number) RETURN date;
-- ***************************************************************************************************
-- Date and Autor:  10.Feb 2009 HR
-- retrieves the assay end date of a study
--
-- Parameters:
-- cnStudy          the study id
--
-- Return:
--  the assay end date
--
-- ***************************************************************************************************

FUNCTION GetAnalyticalStartDate (cnStudy IN number) RETURN date;
-- ***************************************************************************************************
-- Date and Autor:  23.Jul 2008 HR
-- retrieves the analytical start date of a study
--
-- Parameters:
-- cnStudy          the study id
--
-- Return:
-- the analytical start date 
--
-- ***************************************************************************************************

FUNCTION GetAnalyticalEndDate (cnStudy IN number) RETURN date;
-- ***************************************************************************************************
-- Date and Autor:  23.Jul 2008 HR
-- retrieves the analytical end date of a study
--
-- Parameter :
-- cnStudy          the study id
--
-- Return:
-- the analytical end date
--
-- ***************************************************************************************************

FUNCTION StudyUsesGender(cnStudy IN number) RETURN varchar2;
-- ***************************************************************************************************
-- Date and Autor:  23.Jul 2008 HR
--  checks if a study uses genders in the study design. It counts the distinct genderid in designsubject
--
-- Parameters:
-- cnStudy          the study id
--
-- Return:
-- 'T' if genders are found and 'F' if no genders are defined
--
-- ***************************************************************************************************

FUNCTION GetSampleDeactivated(cnStudyId IN number, cnRunid in number, 
                              cnRunSampleSequenceNumber in number, cnAnalyteIndex in number) RETURN varchar2;
-- ***************************************************************************************************
-- Date and Autor:  16.Mrz 2009, HR
-- checks if a sample is deavtivated (listed in the table anarunpeakdecision)
--
-- Parameters:
--  cnStudyId                  the studyid
--  cnRunId                    the runid 
--  cnRunSampleSequenceNumber  the RunSampleSequenceNumber
--  cnAnalyteIndex             the AnalyteIndex
--
-- Return:
-- 'T' if deactivated (found in anarunpeakdecision) and 'F' otherwise
--
-- ***************************************************************************************************

-- ***************************************************************************************************
FUNCTION  SampleInRunOrHasResults
   (cnDesignSampleId IN &&LIMS_USER&.designsample.DESIGNSAMPLEID%type, /* ID design sample*/  
    cnStudyId IN &&LIMS_USER&.designsample.STUDYID%type) /* Study Id */  
 RETURN varchar2;--roh!

FUNCTION Buildsamplename(anStudyID IN &&LIMS_USER&.ANALYTICALRUNSAMPLE.STUDYID%type 
                        ,anSampleStudyID IN &&LIMS_USER&.ANALYTICALRUNSAMPLE.STUDYID%type 
                        ,anRunID IN &&LIMS_USER&.ANALYTICALRUNSAMPLE.RUNID%type 
                        ,anDesignSampleID IN &&LIMS_USER&.designsample.DESIGNSAMPLEID%type 
                        ,anAssayLevel IN &&LIMS_USER&.ANALYTICALRUNSAMPLE.ASSAYLEVEL%type 
                        ,anReplicateNum IN &&LIMS_USER&.ANALYTICALRUNSAMPLE.REPLICATENUMBER%type 
                        ,avRunSampleKind IN &&LIMS_USER&.ANALYTICALRUNSAMPLE.RUNSAMPLEKIND%type 
                        ,anInclStudyName IN NUMBER) 
RETURN varchar2; 


FUNCTION BuildSamplingTime(cnStartday in number, cnStarthour in number, cnStartminute in number, cnStartsecond in number, cnEndday in number, cnEndhour in number, cnEndminute in number, cnEndsecond in number) return varchar2 deterministic;
-- ***************************************************************************************************
-- Date and Autor:  13.Aug 2012 HR
-- Builds the Samplingtime for WT$SAMPLE
--
-- Parameter :
-- cnStartday        the Startday 
-- cnStarthour       the Starthour
-- cnStartminute     the Startminute
-- cnStartsecond     the Startsecond
-- cnEndday          the Endday
-- cnEndhour         the Endhour 
-- cnEndminute       the Endminute
-- cnEndsecond       the Endsecond
--
-- Return:
--  the Samplingtime
--
-- ***************************************************************************************************


END ISR$BIOANALYTICS$WT$R$LAL;