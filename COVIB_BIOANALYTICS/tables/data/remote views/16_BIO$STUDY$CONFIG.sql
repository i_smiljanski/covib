CREATE OR REPLACE FORCE VIEW "ISR$BIO$STUDY$CONFIG" ("ENTITY", "CODE", "STUDYCODE", "FIELDNAME", "FIELDVALUE") AS 
  select 'BIO$STUDY$CONFIG' as ENTITY
       , studyid || '-' || fieldname as CODE
       , studyid as STUDYCODE
       , fieldname as FIELDNAME
       , nvl(to_char(valueinteger), nvl(valuetext, to_char(valuedouble))) as FIELDVALUE
    from &&LIMS_USER&.configgeneral cg
          -- because of MUIB-179. The fieldname will be filtered by the backend
          --, ISR$PARAMETER$VALUES pv
   where --cg.fieldname = pv.VALUE and pv.entity = 'BIO$STUDY$CONFIG' -- MUIB specific
     --and 
     projectid != -999
     and studyid != -999
  order by projectid, studyid