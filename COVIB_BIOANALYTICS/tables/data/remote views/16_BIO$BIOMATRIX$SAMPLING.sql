CREATE OR REPLACE FORCE VIEW "ISR$BIO$BIOMATRIX$SAMPLING" ("ENTITY", "CODE", "STUDYCODE", "BIOMATRIXCODE", "SAMPLINGTIMEID", "SAMPLETYPESPLITKEY", "STARTDAY", "STARTHOUR", "STARTMINUTE", "STARTSECOND", "ENDDAY", "ENDHOUR", "ENDMINUTE", "ENDSECOND", "SAMPLINGTIME", "SAMPLINGTIMEDESC", "STARTTOTALTIME", "ENDTOTALTIME", "STARTSORTTIME", "SORTTIME", "VISIT", "YEAR", "PHASE", "MONTH", "WEEK", "PERIOD", "STUDYDAY", "TIMEIDENT", "TIMEDESC", "TIMETEXT", "STATUS", "POINTINDEX") AS 
  select 'BIO$BIOMATRIX$SAMPLING' as ENTITY
       , d.studyid || '-' || d.samplingtimeid || '-' ||d.subjectgroupid as CODE
       , d.studyid as STUDYCODE
       , d.studyid || '-' || d.sampletypesplitkey || '-' ||d.subjectgroupid as BIOMATRIXCODE
       , d.samplingtimeid as SAMPLINGTIMEID
       , d.sampletypesplitkey as SAMPLETYPESPLITKEY
       , d.startday as STARTDAY
       , d.starthour as STARTHOUR
       , d.startminute as STARTMINUTE
       , d.startsecond as STARTSECOND
       , d.endday as ENDDAY
       , --round(nvl(d.endhour,round (d.endminute/60,2)),10)            endhour,
         round(d.endhour, 10) as ENDHOUR
       , d.endminute as ENDMINUTE
       , d.endsecond as ENDSECOND
       , ISR$BIOANALYTICS$WT$R$LAL.BuildSamplingTime (d.startday,
                                                         d.starthour,
                                                         d.startminute,
                                                         d.startsecond,
                                                         d.endday,
                                                         d.endhour,
                                                         d.endminute,
                                                         d.endsecond)
           as SAMPLINGTIME -- MUIB specific
       , trim(   case when d.startday is null then null else 'Start day ' || d.startday end
              || case when d.starthour is null then null else ' Start hour ' || d.starthour end
              || case when d.startminute is null then null else ' Start minute ' || d.startminute end
              || case when d.startsecond is null then null else ' Start second ' || d.startsecond end
              || case when d.endday is null then null else ' End day ' || d.endday end
              || case
                   when d.endhour is null
                   then
                     null
                   else
                        ' End hour '
                     || case
                          when abs(d.endhour) < 1
                           and abs(d.endhour) > 0
                          then
                            case when d.endhour < 0 then '-' else null end || '0' || trim(to_char(abs(d.endhour), 'TM'))
                          else
                            trim(to_char(d.endhour, 'TM'))
                        end
                 end
              || case when d.endminute is null then null else ' End minute ' || d.endminute end
              || case when d.endsecond is null then null else ' End second ' || d.endsecond end)
           as SAMPLINGTIMEDESC
       , 86400 * nvl(d.startday, 0) + 3600 * nvl(d.starthour, 0) + 60 * nvl(d.startminute, 0) + nvl(d.startsecond, 0)
           as STARTTOTALTIME
       , 86400 * nvl(d.endday, 0) + 3600 * nvl(d.endhour, 0) + 60 * nvl(d.endminute, 0) + nvl(d.endsecond, 0) as ENDTOTALTIME
       , 3600 * nvl(d.starthour, 0) + 60 * nvl(d.startminute, 0) + nvl(d.startsecond, 0) as STARTSORTTIME
       , 3600 * nvl(d.endhour, 0) + 60 * nvl(d.endminute, 0) + nvl(d.endsecond, 0) as SORTTIME
       , de.visittext as VISIT
       , d.YEAR as YEAR
       , de.phase as PHASE
       , d.MONTH as MONTH
       , d.week as WEEK
       , de.period as PERIOD
       , de.studyday as STUDYDAY
       , nvl (case when regexp_like(de.visittext, '^([0-9]*)$') then lpad(de.visittext, 4, '0') else de.visittext end, 'X') -- MUIB specific
         || '_'
         || nvl(regexp_replace(to_char(de.YEAR, 'FM0000D99'), '\' || trim(to_char(1 / 10, 'D')) || '$'), 'X')
         || '_'
         || nvl(regexp_replace(to_char(de.MONTH, 'FM0000D99'), '\' || trim(to_char(1 / 10, 'D')) || '$'), 'X')
         || '_'
         || nvl(regexp_replace(to_char(de.week, 'FM0000D99'), '\' || trim(to_char(1 / 10, 'D')) || '$'), 'X')
         || '_'
         || nvl(regexp_replace(to_char(de.studyday, 'FM0000D99'), '\' || trim(to_char(1 / 10, 'D')) || '$'), 'X')
           as TIMEIDENT
       , trim(   decode(de.visittext, null, null, ' Visit ' || de.visittext)
              || decode(de.YEAR, null, null, ' Year ' || de.YEAR)
              || decode(de.MONTH, null, null, ' Month ' || de.MONTH)
              || decode(de.week, null, null, ' Week ' || de.week)
              || decode(de.studyday, null, null, ' Day ' || de.studyday))
           as TIMEDESC
       , d.timetext as TIMETEXT
       , d.status as STATUS
       , row_number()
         over(partition by d.studyid, d.sampletypesplitkey
              order by d.studyid, d.sampletypesplitkey, 1440 * nvl(d.endday, 0) + 60 * nvl(d.endhour, 0) + nvl(d.endminute, 0))
           as POINTINDEX
    from &&LIMS_USER&.designeventsampletime d, &&LIMS_USER&.designevent de
   where d.studyid = de.STUDYID
     and d.subjectgroupid = de.subjectgroupid
     and D.TREATMENTEVENTID = DE.TREATMENTEVENTID
  order by d.studyid, d.sampletypesplitkey, 1440 * nvl(d.endday, 0) + 60 * nvl(d.endhour, 0) + nvl(d.endminute, 0)