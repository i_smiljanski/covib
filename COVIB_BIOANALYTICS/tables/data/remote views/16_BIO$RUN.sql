CREATE OR REPLACE FORCE VIEW "ISR$BIO$RUN" ("ENTITY", "CODE", "STUDYCODE", "ASSAYCODE", "NAME", "RUNID", "RUNTYPEDESCRIPTION", "RUNSTATUSNUM", "RUNSTATUS", "ACTIVESTATUS", "REORDERWARNING", "RUNSTARTDATE", "EXTRACTIONDATE", "ANALYST", "FILEPREFIX", "SAMPLEFILEPREFIX", "CELLTEMPLATE", "INSERTMETHOD", "USERID", "RECORDTIMESTAMP", "RUNRESULTSIMPORTFILENAME", "INSTGROUPNAME", "OBSERVATIONS") AS 
  select 'BIO$RUN' as ENTITY
       , A.studyid || '-' || A.runid as code
       , A.studyid as STUDYCODE
       , A.studyid || '-' || A.runid || '-' || A.assayid as ASSAYCODE
       --, for SK on BIO$ASSAY only
       , A.rundescription as NAME
       , A.runid as RUNID
       , CR.RUNTYPEDESCRIPTION as RUNTYPEDESCRIPTION
       , A.runstatus as RUNSTATUSNUM
       , cs.anaregstatusdesc as RUNSTATUS
       , A.activestatus as ACTIVESTATUS
       , A.reorderwarning as REORDERWARNING
       , to_char(A.runstartdate, '&&FORMAT_DATE&') as RUNSTARTDATE
       , to_char(A.extractiondate, '&&FORMAT_DATE&') as EXTRACTIONDATE
       , A.analyst as ANALYST
       , A.fileprefix as FILEPREFIX
       , A.samplefileprefix as SAMPLEFILEPREFIX
       , A.celltemplate as CELLTEMPLATE
       , A.insertmethod as INSERTMETHOD
       , A.userid as USERID
       , to_char(A.recordtimestamp, '&&FORMAT_DATE&') as RECORDTIMESTAMP
       , A.runresultsimportfilename as RUNRESULTSIMPORTFILENAME
       , A.instgroupname as INSTGROUPNAME
       , A.observations as OBSERVATIONS
    from &&LIMS_USER&.ANALYTICALRUN A
       , &&LIMS_USER&.CONFIGRUNTYPES cr
       , &&LIMS_USER&.CONFIGANALYTERUNSTATUS cs
       --because of MUIB-179.
       /*, (select distinct studyid, runid
            from &&LIMS_USER&.ANALYTICALRUNANALYTES
           where RUNANALYTEREGRESSIONSTATUS in (select value
                                                  from ISR$PARAMETER$VALUES
                                                 where entity = 'BIO$RUNANALYTEREGRESSIONSTATUS')) rai*/
   where A.RUNTYPEID = CR.RUNTYPEID
     and A.runstatus = cs.runanalyteregressionstatus
     --and a.studyid = rai.studyid
     --and a.runid = rai.runid