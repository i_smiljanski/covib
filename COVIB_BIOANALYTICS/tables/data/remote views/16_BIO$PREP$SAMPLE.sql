CREATE OR REPLACE FORCE VIEW "ISR$BIO$PREP$SAMPLE" ("ENTITY", "CODE", "STUDYCODE", "DESIGNSUBJECTTREATMENTKEY", "USERSAMPLEID") AS 
  select 'BIO$PREP$SAMPLE' as ENTITY
       , ds.studyid || '-' || ds.designsampleid as CODE
       , ds.studyid as STUDYCODE
       , ds.designsubjecttreatmentkey as DESIGNSUBJECTTREATMENTKEY
       , ds.usersampleid as USERSAMPLEID
    from ISR$BIO$DESIGNSAMPLE$MODIFIED ds
   where usersampleid is not null