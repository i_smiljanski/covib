CREATE OR REPLACE FORCE VIEW ISR$BIO$SAMPLETYPES
(ENTITY, CODE, NAME, INFO)
BEQUEATH DEFINER
AS 
select 'BIO$SAMPLETYPES'           as ENTITY
       , cst.sampletypekey          as CODE
       , cst.sampletypeid           as NAME  
       , cst.sampletypeabbreviation as INFO
from &&LIMS_USER&.CONFIGSAMPLETYPES cst