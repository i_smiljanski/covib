CREATE OR REPLACE FORCE VIEW "ISR$BIO$WATSONLIMSUSER" ("ENTITY", "CODE", "LIMSUSER", "FULLNAME", "FIRSTNAME", "MIDDLEINITIAL", "LASTNAME", "USERID", "LOGINNAME", "ACTIVEACCOUNT") AS 
  SELECT 'WT$WATSONLIMSUSER'      ENTITY,
s.LOGINNAME                     CODE,
s.LOGINNAME                     LIMSUSER,
s.FIRSTNAME || ' ' || LASTNAME  FULLNAME,
s.FIRSTNAME ,
s.MIDDLEINITIAL,
s.LASTNAME , 
s.userid, 
s.LOGINNAME ,
s.ACTIVEACCOUNT
from &&LIMS_USER&.secuseraccounts s
--CHANGE BY HSP, 20131023, temporary
where s.ACTIVEACCOUNT = 'Y'