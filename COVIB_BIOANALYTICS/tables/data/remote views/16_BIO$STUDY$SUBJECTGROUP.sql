CREATE OR REPLACE FORCE VIEW "ISR$BIO$STUDY$SUBJECTGROUP" ("ENTITY", "CODE", "STUDYCODE", "NAME", "SEQUENCE", "MASK", "STATUS", "SUBJECTCOUNT", "TREATMENTSITEID", "SITEZIP", "SITECOUNTRY", "SITESTATE", "SITECITY", "SHIPPINGADDRESS", "TREATMENTSITEDESC", "SITEADDR1", "SITEADDR2", "SITEADDR3", "SITEADDR4") AS 
  select 'BIO$STUDY$SUBJECTGROUP' as ENTITY
       , designsubjectgroup.studyid || '-' || designsubjectgroup.subjectgroupid as CODE
       , designsubjectgroup.studyid as STUDYCODE
       , designsubjectgroup.subjectgroupname as NAME
       , designsubjectgroup.sequence as SEQUENCE
       , designsubjectgroup.mask as MASK
       , designsubjectgroup.status as STATUS
       , designsubjectgroup.subjectcount as SUBJECTCOUNT
       , designsubjectgroup.treatmentsiteid as TREATMENTSITEID
       , configtreatmentsite.sitezip as SITEZIP
       , configtreatmentsite.sitecountry as SITECOUNTRY
       , configtreatmentsite.sitestate as SITESTATE
       , configtreatmentsite.sitecity as SITECITY
       , configtreatmentsite.shippingaddress as SHIPPINGADDRESS
       , configtreatmentsite.treatmentsitedesc as TREATMENTSITEDESC
       , configtreatmentsite.siteaddr1 as SITEADDR1
       , configtreatmentsite.siteaddr2 as SITEADDR2
       , configtreatmentsite.siteaddr3 as SITEADDR3
       , configtreatmentsite.siteaddr4 as SITEADDR4
    from &&LIMS_USER&.DESIGNSUBJECTGROUP
       , &&LIMS_USER&.CONFIGTREATMENTSITE
   where designsubjectgroup.TREATMENTSITEID = configtreatmentsite.treatmentsiteid(+)
  order by Code