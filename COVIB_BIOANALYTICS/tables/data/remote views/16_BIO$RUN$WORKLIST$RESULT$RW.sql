CREATE OR REPLACE FORCE VIEW "ISR$BIO$RUN$WORKLIST$RESULT$RW" ("ENTITY", "CODE", "WORKLISTCODE", "RUNCODE", "STUDYCODE", "RUNANALYTECODE", "ANALYTENAME", "SAMPLENAME", "ANALYTEHEIGHT", "ANALYTEHEIGHTSTATUS", "ANALYTEAREA", "ANALYTEAREASTATUS", "ANALYTEPEAKRETENTIONTIME", "INTERNALSTANDARDHEIGHT", "INTERNALSTANDARDHEIGHTSTATUS", "INTERNALSTANDARDAREA", "INTERNALSTANDARDAREASTATUS", "INTERNALSTANDARDRETENTIONTIME", "INTERNALSTDNAME", "RATIO", "RATIOSTATUS", "STATUS", "COMMENTTEXT", "PLATE", "IMPORTFILENAME", "CONCENTRATION", "CORRECTEDCONCENTRATION", "CONCENTRATIONSTATUS", "CONCENTRATIONUNITS", "ELIMINATEDFLAG", "RESULTCOMMENTTEXT", "RESULT", "RESULTTEXT", "REASSAYTAG", "IMPORTEDDATASOURCE") AS 
  select 'BIO$RUN$WORKLIST$RESULT$RAW' as ENTITY
       , A.studyid || '-' || A.runid || '-' || A.runsamplesequencenumber || '-' || AA.ANALYTEID || A.plate as CODE
       , a.StudyID || '-' || a.RunID || '-' || a.RunSampleSequenceNumber as WORKLISTCODE
       , A.studyid || '-' || A.runid as RUNCODE
       , A.studyid as STUDYCODE
       , --A.studyid||'-'||AA.ASSAYID                                              runassaycode,
         --A.studyid||'-'||sa.assayid                                              studyassaycode,
         A.studyid || '-' || A.runid || '-' || AA.ANALYTEID as RUNANALYTECODE
       , ga.analytedescription as ANALYTENAME
       , --A.analyteindex                                                          analyteindex,
         A.samplename as SAMPLENAME
       , A.analyteheight as ANALYTEHEIGHT
       , A.analyteheightstatus as ANALYTEHEIGHTSTATUS
       , A.analytearea as ANALYTEAREA
       , A.analyteareastatus as ANALYTEAREASTATUS
       , A.analytepeakretentiontime as ANALYTEPEAKRETENTIONTIME
       , A.internalstandardheight as INTERNALSTANDARDHEIGHT
       , A.internalstandardheightstatus as INTERNALSTANDARDHEIGHTSTATUS
       , A.internalstandardarea as INTERNALSTANDARDAREA
       , A.internalstandardareastatus as INTERNALSTANDARDAREASTATUS
       , A.internalstandardretentiontime as INTERNALSTANDARDRETENTIONTIME
       , A.internalstdname as INTERNALSTDNAME
       , A.ratio as RATIO
       , A.ratiostatus as RATIOSTATUS
       , A.status as STATUS
       , A.commenttext as COMMENTTEXT
       , A.plate as PLATE
       , A.importfilename as IMPORTFILENAME
       , ARAR.CONCENTRATION as CONCENTRATION
 --nvl(S.CONCENTRATION, ARAR.CONCENTRATION)
       , ARAR.CORRECTEDCONCENTRATION as CORRECTEDCONCENTRATION
       , ARAR.ConcentrationStatus as CONCENTRATIONSTATUS
 --nvl(S.ConcentrationStatus, ARAR.ConcentrationStatus)
       , nvl(CU.ConcentrationUnits, CU2.ConcentrationUnits) as CONCENTRATIONUNITS
       , ARAR.ELIMINATEDFLAG as ELIMINATEDFLAG 
       , ARAR.RESULTCOMMENTTEXT as RESULTCOMMENTTEXT
 --nvl(S.RESULTCOMMENTTEXT, ARAR.RESULTCOMMENTTEXT)
       , ARAR.RESULT as RESULT
 --nvl(S.RESULT, ARAR.RESULT)
       , ARAR.RESULTTEXT as RESULTTEXT
 --nvl(S.RESULTTEXT, ARAR.RESULTTEXT)
       , case
          when ars.DesignSampleid is null then null
          else &&LIMS_USER&.SF_GETREASSAYTAGANDSTATUS(ars.DesignSampleid, a.StudyID, aa.AnalyteID)
        end as REASSAYTAG
       , S.IMPORTEDDATASOURCE as IMPORTEDDATASOURCE
    from &&LIMS_USER&.anarunrawanalytepeak A, 
         &&LIMS_USER&.assayanalytes aa, 
         &&LIMS_USER&.assay ass,
         &&LIMS_USER&.globalanalytes ga,
         &&LIMS_USER&.anarunanalyteresults ARAR,
         &&LIMS_USER&.AnalyticalRunSample ars,
         &&LIMS_USER&.ConcentrationUnits CU,
         &&LIMS_USER&.ConcentrationUnits CU2,
         &&LIMS_USER&.SAMPLERESULTS s
   --ISR$G$STUDY$ASSAY sa
   where aa.studyid = A.studyid
     and ass.runid = A.RUNID
     and ass.assayid = Aa.assayid
     and AA.ANALYTEINDEX = A.ANALYTEINDEX
     --AND ass.studyid = sa.studyid
     --AND ass.assaydescription = sa.assaydescription;
     and ass.studyid = a.studyid
     and aa.analyteid = ga.globalanalyteid     
     AND ARAR.AnalyteIndex(+) = A.AnalyteIndex
     AND ARAR.RunSampleSequenceNumber(+) = A.RunSampleSequenceNumber
     AND ARAR.RunID(+) = A.RunID
     AND ARAR.StudyId(+) = A.StudyId
     AND ars.StudyID(+) = a.StudyID
     AND ars.RunID(+) = a.RunID
     AND ars.RunSampleSequenceNumber(+) = a.RunSampleSequenceNumber
     AND S.ConcUnitsId = CU.ConcUnitsId(+) 
     AND AA.ConcUnitsId = CU2.ConcUnitsId
     AND A.studyid = s.studyid(+)
     AND AA.analyteid = s.analyteid(+)
     AND ARS.designsampleid = s.designsampleid(+)
order by A.RUNSAMPLESEQUENCENUMBER
