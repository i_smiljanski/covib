CREATE OR REPLACE FORCE VIEW "ISR$BIO$INTER$STANDARD" ("ENTITY", "CODE", "ASSAYCODE", "STUDYCODE", "INTERNALSTD", "INTERNALSTDSOURCE", "INTERNALSTDLOTNUM") AS 
  select distinct 'BIO$INTER$STANDARD' as ENTITY
                , A.studyid || '-' || A.assayid || '-' || AIS.INTERNALSTD as CODE
                , A.STUDYID || '-' || A.runid || '-' || A.assayid as ASSAYCODE
                , A.STUDYID as STUDYCODE
                , AIS.INTERNALSTD as INTERNALSTD
                , AIS.INTERNALSTDSOURCE as INTERNALSTDSOURCE
                , AIS.INTERNALSTDLOTNUM as INTERNALSTDLOTNUM
    from &&LIMS_USER&.assay A
       , &&LIMS_USER&.assayinternalstandard ais
       --, ISR$G$STUDY$ASSAY sa -- test HSP, might not be needed anymore
   where A.assayid = ais.assayid
     and A.studyid = ais.studyid
     and a.runid > -1 -- test HSP
     --and a.studyid = sa.studyid
     --and a.assayid = sa.assayid
     --and a.assaydescription = sa.assaydescription
  order by AIS.INTERNALSTD