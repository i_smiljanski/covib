CREATE OR REPLACE FORCE VIEW "ISR$BIO$SHIPMENT" ("ENTITY", "CODE", "STUDYCODE", "SHIPMENTID", "SHIPMENTNAME", "TRANSFERDESC", "TREATMENTSITEID", "TREATMENTSITEDESC", "DATESHIPPED", "DATERECEIVED", "DATERECEIVEDRAW", "HAZMAT", "CARRIER", "AIRBILL", "INVESTIGATOR", "HANDLEDBY", "SHIPMENTINOROUT", "CONTACTNAME", "BOXNAME", "BOXCONDITION", "DRYICEWEIGHT", "SHIPBOXID", "SHIPBOXSTATUS", "DATESHIPPEDNUM", "DATERECEIVEDNUM") AS 
  select 'BIO$SHIPMENT' as ENTITY
       , sh.studyid || '-' || s.ShipmentId as CODE
       , sh.studyid as STUDYCODE
       , s.ShipmentId as SHIPMENTID
       , s.ShipmentName as SHIPMENTNAME
       , ck.TransferDesc as TRANSFERDESC
       , s.TreatmentSiteId as TREATMENTSITEID
       , cs.TreatmentSiteDesc as TREATMENTSITEDESC
       , to_char(s.DateShipped, '&&FORMAT_DATE&') as DATESHIPPED
       , to_char(s.DateReceived, '&&FORMAT_DATE&') as DATERECEIVED
       , s.DateReceived as DATERECEIVEDRAW -- MUIB specific
       , s.HazMat as HAZMAT
       , s.Carrier as CARRIER
       , s.AirBill as AIRBILL
       , s.Investigator as INVESTIGATOR
       , s.HandledBy as HANDLEDBY
       , s.ShipmentInOrOut as SHIPMENTINOROUT
       , c.contactname as CONTACTNAME
       , sb.boxname as BOXNAME
       , SB.BOXCONDITION as BOXCONDITION
       , SB.DRYICEWEIGHT as DRYICEWEIGHT
       , SB.SHIPBOXID as SHIPBOXID
       , SB.SHIPBOXSTATUS as SHIPBOXSTATUS
       , to_char(s.DateShipped, 'YYYYMMDDHH24MI') as DATESHIPPEDNUM
       , to_char(s.DateReceived, 'YYYYMMDDHH24MI') as DATERECEIVEDNUM
    from &&LIMS_USER&.shipment s
       , &&LIMS_USER&.shipmentbox sb
       , &&LIMS_USER&.configtransferkind ck
       , &&LIMS_USER&.configtreatmentsite cs
       , &&LIMS_USER&.contacts c
       , (select distinct sx.shipmentid, cx.studyid
            from &&LIMS_USER&.shipment sx
	       , &&LIMS_USER&.shipmentbox sbx
	       , &&LIMS_USER&.locationcontainer lc
	       , &&LIMS_USER&.containersample cx
           where sx.shipmentid = sbx.shipmentid
             and sbx.shipboxid = lc.shipboxid
             and lc.containerid = cx.containerid
             and sx.DATERECEIVED = lc.addeddate
             and lc.addeddate = cx.addeddate
             and sx.SHIPMENTINOROUT = 'I') sh
   where s.TreatmentSiteId = cs.TreatmentSiteId
     and S.SHIPMENTID = sb.shipmentid
     and ck.TransferKind = s.ShipmentType
     and s.contactid = c.contactid(+)
     and s.ShipmentInOrOut in ('I')
     and s.shipmentid = sh.shipmentid