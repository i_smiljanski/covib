CREATE OR REPLACE FORCE VIEW ISR$BIO$USERSEC
(ENTITY, CODE, LOGINNAME, STUDYCODE)
BEQUEATH DEFINER
AS 
select distinct 'BIO$USERSEC' as ENTITY
       , ua.loginname || '-' || r.studyid as CODE
       , ua.loginname                     as LOGINNAME
       , r.studyid                        as STUDYCODE
from &&LIMS_USER&.SECUSERSTUDYROLES r,
     &&LIMS_USER&.SECUSERACCOUNTS ua
where r.userid=ua.userid