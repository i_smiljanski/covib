CREATE OR REPLACE FORCE VIEW "ISR$BIO$DEACTIVATED$SAMPLES" ("ENTITY", "CODE", "STUDYCODE", "RUNCODE", "ALLRUNCODE", "RUNSAMPLECODE", "ANALYTECODE", "RUNNO", "RUNSAMPLESEQUENCENUMBER", "RUNSAMPLEORDERNUMBER", "OLDSAMPLENAME", "SAMPLENAME", "ANALYTEDESCRIPTION", "DESIGNSAMPLEID", "RECORDTIMESTAMP", "REASON", "FIRSTNAME", "MIDDLEINITIAL", "LASTNAME", "ANALYTE", "SAMPLINGTIMEID", "STARTDAY", "STARTHOUR", "STARTMINUTE", "STARTSECOND", "ENDDAY", "ENDHOUR", "ENDMINUTE", "ENDSECOND", "SAMPLINGTIME", "SAMPLINGTIMEDESC", "VISIT", "YEAR", "PHASE", "MONTH", "WEEK", "PERIOD", "STUDYDAY", "TIMEIDENT", "TIMEDESC", "DESIGNSUBJECTTAG", "RUNANALYTEREGRESSIONSTATUS", "RUNTYPEDESCRIPTION") AS 
  select 'BIO$DEACTIVATED$SAMPLES' as ENTITY
       , Ars.studyid || '-' || Ars.runid || '-' || Ars.runsamplesequencenumber || '-' || aa.analyteid as CODE
       , ars.studyid as STUDYCODE
       , ars.studyid || '-' || ars.runid as RUNCODE
       , ars.studyid || '-' || ars.runid as ALLRUNCODE
       , ars.studyid || '-' || ars.runid || '-' || ars.runsamplesequencenumber || '-' || aa.analyteid as RUNSAMPLECODE
       , -- SK on WT$RUN$SAMPLES
        ars.studyid || '-' || aa.analyteid as ANALYTECODE
       , --SK on WT$STUDY$ANALYTE
        ars.runid as RUNNO
       , ars.runsamplesequencenumber as RUNSAMPLESEQUENCENUMBER
       , ars.runsampleordernumber as RUNSAMPLEORDERNUMBER
       , ARS.exportsamplename as OLDSAMPLENAME
       , ISR$BIOANALYTICS$WT$R$LAL.Buildsamplename(ars.STUDYID
                                                 , ars.SampleStudyID
                                                 , ars.RunID
                                                 , ars.DesignSampleID
                                                 , ars.AssayLevel
                                                 , ars.ReplicateNumber
                                                 , ars.RunSampleKind
                                                 , null)
           as SAMPLENAME
       , ga.analytedescription as ANALYTEDESCRIPTION
       , ars.designsampleid as DESIGNSAMPLEID
       , arp.recordtimestamp as RECORDTIMESTAMP
       , arp.decisionreason as REASON
       , su.firstname as FIRSTNAME
       , su.middleinitial as MIDDLEINITIAL
       , su.lastname as LASTNAME
       , aa.analyteid as ANALYTE
       , dde.samplingtimeid as SAMPLINGTIMEID
       , dde.startday as STARTDAY
       , dde.starthour as STARTHOUR
       , dde.startminute as STARTMINUTE
       , dde.startsecond as STARTSECOND
       , dde.endday as ENDDAY
       , round(dde.endhour, 10) as ENDHOUR
       , dde.endminute as ENDMINUTE
       , dde.endsecond as ENDSECOND
       , ISR$BIOANALYTICS$WT$R$LAL.BuildSamplingTime (dde.startday,
                                                       dde.starthour,
                                                       dde.startminute,
                                                       dde.startsecond,
                                                       dde.endday,
                                                       dde.endhour,
                                                       dde.endminute,
                                                       dde.endsecond)
          as SAMPLINGTIME -- MUIB specific
       , trim(   case when dde.startday is null then null else 'Start day ' || dde.startday end
              || case when dde.starthour is null then null else ' Start hour ' || dde.starthour end
              || case when dde.startminute is null then null else ' Start minute ' || dde.startminute end
              || case when dde.startsecond is null then null else ' Start second ' || dde.startsecond end
              || case when dde.endday is null then null else ' End day ' || dde.endday end
              || case
                   when dde.endhour is null
                   then
                     null
                   else
                        ' End hour '
                     || case
                          when abs(dde.endhour) < 1
                           and abs(dde.endhour) > 0
                          then
                            case when dde.endhour < 0 then '-' else null end || '0' || trim(to_char(abs(dde.endhour), 'TM'))
                          else
                            trim(to_char(dde.endhour, 'TM'))
                        end
                 end
              || case when dde.endminute is null then null else ' End minute ' || dde.endminute end
              || case when dde.endsecond is null then null else ' End second ' || dde.endsecond end)
           as SAMPLINGTIMEDESC
       , dde.visittext as VISIT
       , dde.YEAR as YEAR
       , dde.phase as PHASE
       , dde.MONTH as MONTH
       , dde.week as WEEK
       , dde.period as PERIOD
       , dde.studyday as STUDYDAY
       , nvl (case when regexp_like(dde.visittext, '^([0-9]*)$') then lpad(dde.visittext, 4, '0') else dde.visittext end, 'X') -- MUIB specific
         || '_'
         || nvl(regexp_replace(to_char(dde.YEAR, 'FM0000D99'), '\' || trim(to_char(1 / 10, 'D')) || '$'), 'X')
         || '_'
         || nvl(regexp_replace(to_char(dde.MONTH, 'FM0000D99'), '\' || trim(to_char(1 / 10, 'D')) || '$'), 'X')
         || '_'
         || nvl(regexp_replace(to_char(dde.week, 'FM0000D99'), '\' || trim(to_char(1 / 10, 'D')) || '$'), 'X')
         || '_'
         || nvl(regexp_replace(to_char(dde.studyday, 'FM0000D99'), '\' || trim(to_char(1 / 10, 'D')) || '$'), 'X')
           as TIMEIDENT
       , trim(   decode(dde.visittext, null, null, ' Visit ' || dde.visittext)
              || decode(dde.YEAR, null, null, ' Year ' || dde.YEAR)
              || decode(dde.MONTH, null, null, ' Month ' || dde.MONTH)
              || decode(dde.week, null, null, ' Week ' || dde.week)
              || decode(dde.studyday, null, null, ' Day ' || dde.studyday))
           as TIMEDESC
       , dde.designsubjecttag as DESIGNSUBJECTTAG
       , (
            select runanalyteregressionstatus
              from &&LIMS_USER&.analyticalrunanalytes
             where     studyid = Ars.studyid
               and runid = ars.runid
               and analyteindex = aa.analyteindex
         ) as RUNANALYTEREGRESSIONSTATUS -- MUIB specific
       , (
           select CR.RUNTYPEDESCRIPTION
                   from &&LIMS_USER&.configruntypes cr
                 where AR.RUNTYPEID = CR.RUNTYPEID
          ) as RUNTYPEDESCRIPTION -- MUIB specific
    from &&LIMS_USER&.analyticalrun ar
       , &&LIMS_USER&.analyticalrunsample ars
       , &&LIMS_USER&.anarunpeakdecision arp
       , &&LIMS_USER&.assayanalytes aa
       , &&LIMS_USER&.secuseraccounts su
       , &&LIMS_USER&.globalanalytes ga
       , &&LIMS_USER&.assay Ass
       , (select d.studyid
               , d.DESIGNSAMPLEID
               , d.samplingtimeid
               , d.startday
               , d.starthour
               , d.startminute
               , d.startsecond
               , d.endday
               , round(d.endhour, 10) endhour
               , d.endminute
               , d.endsecond
               , de.visittext
               , d.YEAR
               , de.phase
               , d.MONTH
               , d.week
               , de.period
               , de.studyday
               , ds.designsubjecttag
            from ISR$BIO$DESIGNSAMPLE$MODIFIED d, &&LIMS_USER&.designsubject ds, &&LIMS_USER&.designevent de
           where d.studyid = de.STUDYID
             and D.TREATMENTEVENTID = DE.TREATMENTEVENTID
             and d.SUBJECTGROUPID = de.SUBJECTGROUPID
             and D.DESIGNSUBJECTID = DS.DESIGNSUBJECTID
             and d.studyid = ds.studyid) dde
   where arp.runsamplesequencenumber = ars.runsamplesequencenumber
     and arp.runid = ars.runid
     and arp.studyid = ars.studyid
     and ar.runid = arp.runid
     and ar.studyid = arp.studyid
     and aa.analyteindex = arp.analyteindex
     and ar.assayid = aa.assayid
     and aa.analyteid = ga.globalanalyteid
     and arp.userid = su.userid
     and dde.studyid(+) = ars.studyid
     and Dde.DESIGNSAMPLEID(+) = ars.DESIGNSAMPLEID
     and aa.assayId = Ass.assayid
     and Ass.runid = ar.runid
     and Ass.studyid = ar.studyid