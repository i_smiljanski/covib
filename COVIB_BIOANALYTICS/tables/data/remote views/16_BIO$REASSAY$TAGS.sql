CREATE OR REPLACE FORCE VIEW "ISR$BIO$REASSAY$TAGS" ("ENTITY", "CODE", "STUDYCODE", "ANALYTECODE", "NAME", "ANALYTEDESCRIPTION", "DESIGNSAMPLEID", "REASSAYREASON", "TAGSTATUS") AS 
  select 'BIO$REASSAY$TAGS' as ENTITY
       , rt.studyid || '-' || rt.designsampleid || '-' || rt.analyteid as CODE
       , rt.studyid as STUDYCODE
       , -- fk on WT$Study
        rt.studyid || '-' || rt.analyteid as ANALYTECODE
       , -- sk auf WT$Study$Analytes
        &&LIMS_USER&.Sf_Buildsamplename(null
                         , rt.studyid
                         , null
                         , rt.designsampleid
                         , null
                         , null
                         , 'UNKNOWN'
                         , 1)
           as NAME
       , ga.analytedescription as ANALYTEDESCRIPTION
       , rt.designsampleid as DESIGNSAMPLEID
       , rt.reassayreason as REASSAYREASON
       , rt.tagstatus as TAGSTATUS
    from &&LIMS_USER&.REASSAYTAG RT
       , &&LIMS_USER&.GLOBALANALYTES ga
   where rt.analyteid = ga.globalanalyteid
     and rt.tagstatus in ('New', 'Rej', 'Run')