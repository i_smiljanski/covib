CREATE OR REPLACE FORCE VIEW "ISR$BIO$BIOMATRIX" ("ENTITY", "CODE", "STUDYCODE", "SUBJECTGROUPCODE", "SUBJECTGROUPTREATMENTCODE", "MATRIX", "MATRIXKEY", "SAMPLETYPESPLITKEY", "SPLITNUMBER", "STATUS") AS 
  select 'BIO$BIOMATRIX' as ENTITY
       , d.studyid || '-' || d.sampletypesplitkey|| '-' ||d.subjectgroupid as CODE
       , d.studyid as STUDYCODE
       , d.studyid || '-' || d.subjectgroupid as SUBJECTGROUPCODE
       , d.studyid || '-' || D.TREATMENTEVENTID as SUBJECTGROUPTREATMENTCODE
       , CS.SAMPLETYPEID as MATRIX
       , D.SAMPLETYPEKEY as MATRIXKEY
       , d.sampletypesplitkey as SAMPLETYPESPLITKEY
       , d.splitnumber as SPLITNUMBER
       , d.status as STATUS
    from &&LIMS_USER&.designeventsampletype d, &&LIMS_USER&.configsampletypes cs
   where D.SAMPLETYPEKEY = CS.SAMPLETYPEKEY
     and d.revisionnumber = (
                             select max(dm.revisionnumber)
                               from &&LIMS_USER&.designeventsampletype dm
                              where dm.studyid = d.studyid
                                and dm.subjectgroupid = d.subjectgroupid
                                and dm.treatmenteventid = d.treatmenteventid
                                and dm.splitnumber = d.splitnumber
                                and dm.status = d.status
                                and dm.SAMPLETYPEKEY = d.SAMPLETYPEKEY
         )