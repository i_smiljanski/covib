CREATE OR REPLACE FORCE VIEW "ISR$BIO$PKPARAMETER" ("ENTITY", "CODE", "NAME", "STUDYCODE", "PKCODE", "REPORTNAME", "PARAMETERVALUE", "PARAMETERUNITS", "PARAMETERVALUETEXT", "REPORTEDUNIT", "TRANSLATEDREPORTNAME") AS 
  select distinct 'BIO$PKPARAMETER' as ENTITY
                , P.studyid || '-' || P.pkkeyvalue || '-' || P.attributename as CODE
                , P.attributename as NAME
                , P.studyid as STUDYCODE
                , P.studyid || '-' || P.pkkeyvalue || '-' || D.treatmentkey as PKCODE
                , P.attributename as REPORTNAME
                , P.parametervalue as PARAMETERVALUE
                , P.parameterunits as PARAMETERUNITS
                , P.parametervaluetext as PARAMETERVALUETEXT
                , P.parameterunits as REPORTEDUNIT --NVL (ip.display, P.parameterunits) reportedunit
                , P.attributename as TRANSLATEDREPORTNAME --NVL (ip2.display, P.attributename) translatedReportname
    from &&LIMS_USER&.pkparameters P
       , &&LIMS_USER&.pkkey pk
       , &&LIMS_USER&.designevent d
   where Pk.studyday = d.studyday
     and Pk.treatmentkey = d.treatmentkey
     and Pk.studyid = d.studyid
     and pk.studyid = p.studyid
     and p.pkkeyvalue = pk.pkkeyvalue