CREATE OR REPLACE FORCE VIEW "ISR$BIO$CONCENTRATIONS" ("ENTITY", "CODE", "ORIGINALCONCENTRATIONUNIT", "RESULTCONCENTRATIONUNIT", "CONVERSIONFACTOR") AS 
  SELECT DISTINCT 
'BIO$CONCENTRATIONS'                            entity,
cc.originalconcunits||'-'||cc.resultconcunits  code,
cc.originalconcunits, 
cc.resultconcunits,
cc.conversionfactor
FROM &&LIMS_USER&.concconversion cc
WHERE cc.molecularweightneeded='X'