CREATE OR REPLACE FORCE VIEW ISR$BIO$RUN$WORKLIST ("CODE", "STUDYCODE", "RUNCODE", "ASSAYCODE", "DESIGNSAMPLECODE", "RUNSAMPLESEQUENCENUMBER", "RUNSAMPLEORDERNUMBER", "SAMPLESUBTYPE", "RUNSAMPLEKIND", "ASSAYLEVEL", "REPLICATENUMBER", "SAMPLESTUDYCODE", "STABILITYTYPEID", "DESIGNSAMPLEOID", "EXPORTSAMPLENAME", "TREATMENTID", "KNOWNMATRIX", "KNOWNMATRIXABBREVIATION", "ALIQUOTFACTOR", "ASSAYDATETIME", "SAMPLEACCEPTEDTIMESTAMP", "SAMPLECOMMENT", "CHARGEABLE", "USERID", "EXPORTFILENAME", "IMPORTFILENAME", "SAMPLEMETHOD", "DILUTION", "SPIKEDANALYTE", "SPIKEDANALYTEUNITS", "CONCENTRATION", "SOURCE", "CONCUNITS", "VOLUME", "TITERPOSITION", "RACKNUMBER", "PREPDATE", "SAVEDORDERNUMBER", "ANALYSISTYPE", "BATCHNAME", "WATSONID", "HOURS", "LONGTERMTIME", "LONGTERMUNITS", "STABILITYTYPE", "CYCLES", "TEMPERATURE", "DECISIONREASON", "USERNAME", "CURRENTLOCATION", "CURRENTCONTAINER", "SAMPLENAME", "TITERMIN", "TITERMAX", "STATUS", "SAMPLESTUDYNAME", "CUSTOMID", "MATRIXSPLIT", "SUBJECT", "ALIAS", "TIMETEXT", "STARTTIME", "RELATIVETIME", "TIME", "SAMPLESTATUS", "SAMPLEVOLUME", "SAMPLINGDATETIME", "VIALSAMPLEID", "SAMPLEENTRYMODE", "SAMPLEENTRYDATETIME", "VISIT", "YEAR", "PHASE", "MONTH", "WEEK", "PERIOD", "STUDYDAY", "COMMENTMEMO", "SAMPLINGTIMEID", "STARTDAY", "STARTHOUR", "STARTMINUTE", "STARTSECOND", "ENDDAY", "ENDHOUR", "ENDMINUTE", "ENDSECOND", "RELATIVEDAY", "RELATIVEHOUR", "RELATIVEMINUTE", "RECORDTYPE", "TREATMENTSITEDESC", "RECORDTIMESTAMP", "REVISIONNUMBER", "FLAGPERCENT", "RECEIPTDATE", "RECEIPTUSERNAME", "DISCREPANCY", "ISOTOPE1TEXT", "ISOTOPE2TEXT", "INFECTIOUSAGENTTEXT", "IDENTIFICATIONSTATUSTEXT", "SAMPLETEXTVARIABLE1", "SAMPLETEXTVARIABLE2", "SAMPLETEXTVARIABLE3", "OTHERCONDITIONTEXT", "TUBECONDITIONTEXT", "FROZENCONDITIONTEXT", "RADIOLABELED", "INFECTIOUS", "INACTIVATIONTEMP", "INACTIVATIONDATEIN", "INACTIVATIONDATEOUT", "ISFORMULATION", "ANALYSISSTATUS", "DOSEDATETIME", "BARCODEID") AS
  select distinct 
       ars.StudyID || '-' || ars.RunID || '-' || ars.RunSampleSequenceNumber as Code,
       ars.StudyID as StudyCode,
       ars.StudyID || '-' || ars.RunID as RunCode,
       ars.StudyID || '-' || ars.RunID || '-' || r.AssayID as AssayCode,
       case when ars.DesignSampleID is not null then
         ars.StudyID || '-' || ars.DesignSampleID
       else
         null
       end as DesignSampleCode,
       ars.RunSampleSequenceNumber,
       ars.RunSampleOrderNumber,
       ars.SampleSubType,
       ars.RunSampleKind,
       ars.AssayLevel,
       ars.ReplicateNumber,  -- Rep.
       ars.SampleStudyID as SampleStudyCode,
       ars.StabilityTypeID,
       ars.DesignSampleID as DesignSampleOID,
       ars.ExportSampleName,
       dst.TreatmentId,
       case when ars.AssayLevel is null and ars.RunSampleKind != 'UNKNOWN' then
         null
       else
           case when ars.RunSampleKind != 'UNKNOWN' then
             cst1.SampleTypeId
           else
             cst2.SampleTypeId
           end
       end as KnownMatrix,
       case when ars.AssayLevel is null and ars.RunSampleKind != 'UNKNOWN' then
         null
       else
           case when ars.RunSampleKind != 'UNKNOWN' then
             cst1.SAMPLETYPEABBREVIATION
           else
             cst2.SAMPLETYPEABBREVIATION
           end
       end as KnownMatrixAbbreviation,
       ars.AliquotFactor,
       to_char(r.RunStartDate, '&&FORMAT_DATE&') AssayDateTime, -- to_char(ars.AssayDateTime, '&&FORMAT_DATE&') AssayDateTime,
       to_char(ars.SampleAcceptedTimestamp, '&&FORMAT_DATE&') SampleAcceptedTimestamp,
       ars.SampleComment,
       ars.Chargeable,
       ars.UserID,
       ars.ExportFilename,
       ars.ImportFilename,
       ars.Method as SampleMethod,
       round(1/ars.AliquotFactor,2) as Dilution,  -- Dilution
       ars.SpikedAnalyte,
       ars.SpikedAnalyteUnits as SpikedAnalyteUnits,
       nvl(ar.Concentration, ars.Concentration) as CONCENTRATION,
       ar.SOURCE as SOURCE,
       ar.ConcUnits as CONCUNITS,
       nvl(ars.InjectionVolume, round(a.StandardVolume * ars.AliquotFactor)) as Volume,  -- Volume
       ars.TiterPosition,  -- Plate/Cell Postion
       ars.RackNumber,  -- Rack Number
       to_char(ars.PrepDate, '&&FORMAT_DATE&') PrepDate,
       ars.SavedOrderNumber,  -- Saved Position
       ars.AnalysisType,
       bd.BatchName,
       case when ars.RunSampleKind = 'UNKNOWN' then
         &&LIMS_USER&.SF_BuildBarcode(ars.StudyId, ars.DesignSampleId)
       else
         null
       end as WatsonID,
       ars.Hours,
       ars.LongTermTime,
       ars.LongTermUnits,
       ars.StabilityType,
       ars.Cycles,
       ars.Temperature,
       null DecisionReason,--arp.DecisionReason,
       null Username,--&&LIMS_USER&.Sf_GetUsername(arp.userid) Username,
       udf.currentlocation,
       udf.currentcontainer,
       -- udf.analysis,
       case
         when ars.RunSampleKind = 'UNKNOWN' then
           SUBSTR(&&LIMS_USER&.Sf_BuildSampleName(ars.StudyID, ars.SampleStudyID, ars.RunID, ars.DesignSampleID, ars.AssayLevel, ars.ReplicateNumber, ars.RunSampleKind, 0), 1, 60)
         when ar.ID is not null then
           ar.ID
         else
           ars.RunSampleKind
       end as SampleName, -- ID
       ar.ttrmin TiterMin,
       ar.ttrmax TiterMax,
       ds.status,
       s.STUDYNAME SampleStudyName,
       ds.USERSAMPLEID CustomID,
       ds.SPLITNUMBER MatrixSplit,
       dsbj.DesignSubjectTag Subject,
       dsbj.StudySubjectTag Alias,
       ds.TIMETEXT,
       &&LIMS_USER&.Sf_GetNominalTimeString(ds.startday, ds.starthour, ds.startminute, ds.TIMETEXT, NULL) as StartTime,
       &&LIMS_USER&.Sf_GetNominalTimeString(ds.relativeday, ds.relativehour, ds.relativeminute, ds.TIMETEXT, NULL) as RelativeTime,
       &&LIMS_USER&.Sf_GetNominalTimeString(ds.endday, ds.endhour, ds.endminute, ds.TIMETEXT, NULL) as Time,
       css.samplestatustext as SAMPLESTATUS,
       ds.samplevolume as SAMPLEVOLUME,
       to_char(ds.samplingdatetime, '&&FORMAT_DATE&') as SAMPLINGDATETIME,
       ds.vialsampleid as VIALSAMPLEID,
       ds.sampleentrymode as SAMPLEENTRYMODE,
       to_char(ds.sampleentrydatetime, '&&FORMAT_DATE&') as SAMPLEENTRYDATETIME,
       de.visittext as VISIT,
       de.year as YEAR,
       de.phase as PHASE,
       de.MONTH as MONTH,
       de.week as WEEK,
       de.period as PERIOD,
       de.studyday as STUDYDAY,
       ds.commentmemo as COMMENTMEMO,
       ds.samplingtimeid as SAMPLINGTIMEID,
       ds.startday as STARTDAY,
       ds.starthour as STARTHOUR,
       ds.startminute as STARTMINUTE,
       ds.startsecond as STARTSECOND,
       ds.endday as ENDDAY,
       round(ds.endhour, 10) as ENDHOUR,
       ds.endminute as ENDMINUTE,
       ds.endsecond as ENDSECOND,
       ds.relativeday as RELATIVEDAY,
       ds.relativehour as RELATIVEHOUR,
       ds.relativeminute as RELATIVEMINUTE,
       ds.recordtype as RECORDTYPE,
       CS.TREATMENTSITEDESC as TREATMENTSITEDESC,
       to_char(ds.recordtimestamp, '&&FORMAT_DATE&') as RECORDTIMESTAMP,
       ds.revisionnumber as REVISIONNUMBER,
       nvl(ar.flagpercent
           , case when ar.knowntype = 'QC' then a.RELIMITQC when ar.knowntype = 'STANDARD' then a.RELIMITSTD else null end)
           as FLAGPERCENT,
       to_char(ds.receiptdate, '&&FORMAT_DATE&') as RECEIPTDATE,
       sua.lastname||case when sua.firstname is not null or sua.middleinitial is not null then ', ' end||sua.firstname||case when sua.middleinitial is not null then ' '||sua.middleinitial end as RECEIPTUSERNAME,
       ds.discrepancy as DISCREPANCY,
       ci1.ISOTOPETEXT as ISOTOPE1TEXT,
       ci2.ISOTOPETEXT as ISOTOPE2TEXT,
       ca.INFECTIOUSAGENTTEXT as INFECTIOUSAGENTTEXT,
       cis.IDENTIFICATIONSTATUSTEXT as IDENTIFICATIONSTATUSTEXT,
       ds.sampletextvariable1 as SAMPLETEXTVARIABLE1,
       ds.sampletextvariable2 as SAMPLETEXTVARIABLE2,
       ds.sampletextvariable3 as SAMPLETEXTVARIABLE3,
       coc.otherconditiontext as OTHERCONDITIONTEXT,
       ctc.tubeconditiontext as TUBECONDITIONTEXT,
       cfc.frozenconditiontext as FROZENCONDITIONTEXT,
       ds.radiolabeled as RADIOLABELED,
       ds.infectious as INFECTIOUS,
       ds.inactivationtemp as INACTIVATIONTEMP,
       to_char(ds.inactivationdatein, '&&FORMAT_DATE&') as INACTIVATIONDATEIN,
       to_char(ds.inactivationdateout, '&&FORMAT_DATE&') as INACTIVATIONDATEOUT,
       ds.isformulation as ISFORMULATION,
       ISR$BIOANALYTICS$WT$R$LAL.SampleInRunOrHasResults(ds.designsampleid, ds.studyid) as ANALYSISSTATUS,
       to_char(dst.dosedatetime, '&&FORMAT_DATE&') as DOSEDATETIME,
       dsb.BarcodeID as BARCODEID
from &&LIMS_USER&.AnalyticalRunSample ars
join &&LIMS_USER&.AnalyticalRun r
  on ars.StudyID = r.StudyID and ars.RunID = r.RunID
join &&LIMS_USER&.Assay a
  on a.AssayID = r.AssayID
join &&LIMS_USER&.ConfigSampleTypes cst1
  on a.SampleTypeKey = cst1.SampleTypeKey
left outer join &&LIMS_USER&.AssayReps ar
  on r.AssayID = ar.AssayID 
  and ars.RunSampleKind = ar.KnownType
  and ars.AssayLevel = ar.LevelNumber
left outer join &&LIMS_USER&.BatchDefinition bd
  on ar.BatchID  = bd.batchid
--left outer join &&LIMS_USER&.anarunpeakdecision arp 
--  on arp.runsamplesequencenumber = ars.runsamplesequencenumber 
--  and arp.runid = ars.runid 
--  and arp.studyid = ars.studyid
left outer join ISR$BIO$DESIGNSAMPLE$MODIFIED ds
  on ars.SampleStudyID = ds.StudyID
  and ars.DesignSampleID = ds.DesignSampleID
left outer join &&LIMS_USER&.designevent de
  on ds.studyid = de.studyid
  and ds.treatmenteventid = de.treatmenteventid
left outer join &&LIMS_USER&.DesignSubjectTreatment dst
  on ars.SampleStudyID = dst.StudyID
  and ds.DesignSubjectTreatmentKey = dst.DesignSubjectTreatmentKey
left outer join &&LIMS_USER&.DesignTreatment dt
  on ds.studyid = dt.studyid
  and dst.TreatmentKey = dt.TreatmentKey
left outer join &&LIMS_USER&.ConfigSampleTypes cst2
  on ds.SampleTypeKey = cst2.SampleTypeKey
left outer join &&LIMS_USER&.DesignSubject dsbj
  on ars.SampleStudyID = dsbj.StudyID
  and ds.DesignSubjectId = dsbj.DesignSubjectId
left outer join &&LIMS_USER&.study s
  on ars.SampleStudyID = s.StudyID
left outer join &&LIMS_USER&.ConfigIsotope ci1
  on ds.Isotope1Key = ci1.IsotopeKey
left outer join &&LIMS_USER&.ConfigIsotope ci2
  on ds.Isotope2Key = ci2.IsotopeKey
left outer join &&LIMS_USER&.ConfigInfectiousAgent ca
  on ds.InfectiousAgentKey = ca.InfectiousAgentKey
left outer join &&LIMS_USER&.ConfigIdentificationStatus cis
  on ds.IdentificationStatusKey = cis.IdentificationStatusKey
left outer join &&LIMS_USER&.ConfigOtherCondition coc
  on ds.OtherConditionKey = coc.OtherConditionKey
left outer join &&LIMS_USER&.ConfigTubeCondition ctc
  on ds.TubeConditionKey = ctc.TubeConditionKey
left outer join &&LIMS_USER&.ConfigFrozenCondition cfc
  on ds.FrozenConditionKey = cfc.FrozenConditionKey
left outer join &&LIMS_USER&.ConfigFormulation cf
  on dt.FormulationID = cf.FormulationID
left outer join &&LIMS_USER&.ConfigFastedFed cff
  on dt.fastedFedID = cff.fastedFedID
left outer join &&LIMS_USER&.ConfigRouteAdmin cra
  on dt.RouteAdminId = cra.RouteAdminId
left outer join &&LIMS_USER&.ConfigRegimen cr
  on dt.RegimenId = cr.RegimenId
left outer join &&LIMS_USER&.ConfigSampleStatus css
  on ds.SampleStatus = css.SampleStatusCode
left outer join &&LIMS_USER&.DesignSampleBarcode dsb
  on ars.StudyID = dsb.StudyID
  and ars.designsampleid = dsb.designsampleid
left outer join &&LIMS_USER&.ConfigTreatmentSite cs
  on ds.treatmentsiteid = cs.treatmentsiteid
left outer join &&LIMS_USER&.SecUserAccounts sua
  on ds.receiptuserid = sua.userid
left outer join (
    SELECT rds.studyid, 
           rds.timemin                                AS timeMin,
           rds.username                               AS receiptuser,
           DS.DesignSampleId,
           SUBSTR (rds.samplelocation, 1, 530)        AS currentlocation,
           SUBSTR (rds.samplecontainerbarcode, 1, 20) AS currentcontainer,
           -- SUBSTR (rds.sampleanalysis, 1, 60)         AS analysis,
           SUBSTR (rds.samplename, 1, 60)             AS SampleName
            FROM ISR$BIO$DESIGNSAMPLE$MODIFIED DS, &&LIMS_USER&.RPT_DESIGNSAMPLE RDS
           WHERE     RDS.STUDYID = DS.STUDYID
                 AND RDS.DESIGNSAMPLEID = DS.DESIGNSAMPLEID
                 AND (DS.Status = 1 OR DS.Status = 2)) UDF
  on UDF.StudyID = dst.StudyID
 and UDF.DesignSampleID = ars.DesignSampleID  
--where ars.StudyId = 789 and ars.RunId = 3
order by RunSampleOrderNumber
