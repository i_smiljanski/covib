CREATE MATERIALIZED VIEW ISR$BIO$BTINFO
BUILD DEFERRED REFRESH FAST ON DEMAND
AS
SELECT
    EXTERNAL_REFERENCE CODE,
    substr (EXTERNAL_REFERENCE, instr( EXTERNAL_REFERENCE,'STUDYID')+7, instr( EXTERNAL_REFERENCE,'DESIGNSAMPLEID')-8 ) STUDYCODE,
    substr ( EXTERNAL_REFERENCE, instr( EXTERNAL_REFERENCE,'STUDYID')+7, instr( EXTERNAL_REFERENCE,'DESIGNSAMPLEID')-8 ) || '-' ||substr ( EXTERNAL_REFERENCE, instr( EXTERNAL_REFERENCE,'DESIGNSAMPLEID')+14 ) SAMPLECODE,
    to_char(MAX(enddate), '&&FORMAT_DATE&') as LASTDATE,
    round(SUM(thawdurationhrs), 2) AS BTTIME
FROM( 
select 
  sa.external_reference,
  lead(aut.timestamp, 1) over (partition BY AE.Audit_table_numeric_key order by AE.Audit_table_numeric_key, aut.timestamp) endDate,
  DECODE(lou.u_is_frozen,'T',0,1) * ( (lead(aut.timestamp, 1) over (partition BY AE.Audit_table_numeric_key order by AE.Audit_table_numeric_key, aut.timestamp)) -aut.timestamp) * 24 AS ThawDurationHrs
FROM
  &&LIMS_USER&.audit_transaction AuT
LEFT JOIN &&LIMS_USER&.audit_event AE
ON AuT.Audit_Transaction_ID = AE.Audit_Transaction_id
LEFT JOIN &&LIMS_USER&.audit_data AD
ON AD.AUDIT_EVENT_ID = ae.audit_event_id
LEFT JOIN &&LIMS_USER&.aliquot Al
ON AE.Audit_table_numeric_key = al.aliquot_id
LEFT JOIN &&LIMS_USER&.sample Sa
ON  al.sample_id = sa.sample_id
LEFT JOIN &&LIMS_USER&.study St
ON sa.study_id = st.study_id
LEFT JOIN &&LIMS_USER&.LOCATION loc
ON ad.new_value = loc.location_id
LEFT JOIN &&LIMS_USER&.location_user lou
ON loc.location_id = lou.location_id
WHERE AD.AUDIT_FIELD       = 'Location Id'
AND ae.AUDIT_TABLE        = 'Aliquot'
AND al.aliquot_template_id = 21
order by ae.audit_table_numeric_key, aut.timestamp ) t
    group by external_reference