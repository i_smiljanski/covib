CREATE OR REPLACE FORCE VIEW "ISR$BIO$REASSAY$HISTORY" ("ENTITY", "STUDYCODE", "CODE", "SUBJECTCODE", "BIOMATRIXSAMPLINGCODE", "TREATMENTCODE", "ANALYTECODE", "RUNSAMPLECODE", "SPECIES", "MATRIX", "DESIGNSUBJECTTAG", "SPECIMENKEY", "DESIGNSAMPLEID", "SAMPLESUBTYPE", "REASSAYCONCENTRATION", "RESULTCOMMENTTEXT", "REASSAY_CONCENTRATIONSTATUS", "REASSAY_DILUTION", "REASSAY_ALIQUOTFACTOR", "ASSAYDATETIME", "ORIGINALVALUE", "CONCENTRATIONUNITS", "CONCUNITSID", "SAMPLE_DILUTION", "SAMPLE_ALIQUOTFACTOR", "SAMPLECONCENTRATION", "ANALYTE", "SAMPLE_CONCENTRATIONSTATUS", "REASON_FOR_REASSAY", "REASON_FOR_REPORTED_CONC", "REASON_CONCENTRATIONSTATUS", "NM", "VEC", "NP", "RUNID", "RUNSAMPLEORDERNUMBER", "RUNSAMPLESEQUENCENUMBER", "DECIMALPLACES", "SPLITNUMBER", "DESIGNTREATMENTID", "TREATMENTDESC", "VISIT", "YEAR", "PHASE", "MONTH", "WEEK", "PERIOD", "STUDYDAY", "TIMEIDENT", "TIMEDESC", "STARTDAY", "STARTHOUR", "STARTMINUTE", "STARTSECOND", "ENDDAY", "ENDHOUR", "ENDMINUTE", "ENDSECOND", "SAMPLINGTIME", "SAMPLINGTIMEDESC", "STARTTOTALTIME", "ENDTOTALTIME", "TIMETEXT", "USERSAMPLEID", "CALIBRATIONRANGEFLAG", "CALIBRATIONRANGE", "ACCEPTANCETIMESTAMP", "REPORTED", "SAMPLESTATUS", "RECORDTIMESTAMP", "SAMPLECOMMENT", "RESULT", "RESULTTEXT", "EXPORTSAMPLENAME") AS 
  select distinct
         'BIO$REASSAY$HISTORY' as ENTITY
       , arr.studyid as STUDYCODE
       ,    arr.studyid
         || '-'
         || ds.designsubjectid
         || '-'
         || ds.samplingtimeid
         || '-'
         || dt.treatmentkey
         || '-'
         || ar.runid
         || '-'
         || arr.runsamplesequencenumber
         || '-'
         || aa.ANALYTEID
           as CODE
       , --  Einträge eindeutig machen analog zu WT$RUN$SAMPLE$RESULTS
        arr.studyid || '-' || ds.designsubjectid as SUBJECTCODE
       , -- fk on ISR$BIO$STUDY$SUBJECT
        arr.studyid || '-' || ds.samplingtimeid || '-' ||ds.subjectgroupid as BIOMATRIXSAMPLINGCODE
       , -- fk on WT$BIOMATRIX$SAMPLING (time)
        arr.studyid || '-' || dt.treatmentkey as TREATMENTCODE
       , -- fk on ISR$BIO$STUDY$TREATMENT
        arr.studyid || '-' || aa.analyteid as ANALYTECODE
       , arr.studyid || '-' || arr.runid || '-' || arr.runsamplesequencenumber || '-' || aa.analyteid as RUNSAMPLECODE
        -- SK on WT$RUN$SAMPLES
       , cs.species as SPECIES
       , cst.sampletypeid as MATRIX
       , dsu.designsubjecttag as DESIGNSUBJECTTAG
       , rds.specimenkey as SPECIMENKEY
       , src.designsampleid as DESIGNSAMPLEID
       , ars.SampleSubType as SAMPLESUBTYPE
       , arr.concentration as REASSAYCONCENTRATION
       , arr.resultcommenttext as RESULTCOMMENTTEXT
       , arr.concentrationstatus as REASSAY_CONCENTRATIONSTATUS
       , case when ars.aliquotfactor = 0 then 1 else round(1 / nvl(ars.aliquotfactor, 1), 8) end as REASSAY_DILUTION
       , ars.aliquotfactor as REASSAY_ALIQUOTFACTOR
       , to_char(ars.assaydatetime, '&&FORMAT_DATE&') as ASSAYDATETIME
       , src.originalvalue as ORIGINALVALUE
       , cu.concentrationunits as CONCENTRATIONUNITS
       , cu.concunitsid as CONCUNITSID
       , case when sr.aliquotfactor = 0 then 1 else round(1 / nvl(sr.aliquotfactor, 1), 8) end as SAMPLE_DILUTION
       , sr.aliquotfactor as SAMPLE_ALIQUOTFACTOR
       , sr.concentration as SAMPLECONCENTRATION
       , src.analyteid as ANALYTE
       , sr.concentrationstatus as SAMPLE_CONCENTRATIONSTATUS
       , srcd.reassayreason as REASON_FOR_REASSAY
       , srcd.reassayconcreason as REASON_FOR_REPORTED_CONC
       , srcd.concentrationstatus as REASON_CONCENTRATIONSTATUS
       , ara.nm as NM
       , ara.vec as VEC
       , ara.np as NP
       , ars.runid as RUNID
       , ars.runsampleordernumber as RUNSAMPLEORDERNUMBER
       , ars.runsamplesequencenumber as RUNSAMPLESEQUENCENUMBER
       , aa.decimalplaces as DECIMALPLACES
       , ds.splitnumber as SPLITNUMBER
       , dt.treatmentid as DESIGNTREATMENTID
       , dt.treatmentdesc as TREATMENTDESC
       , dst.visittext as VISIT
       , dst.YEAR as YEAR
       , dst.phase as PHASE
       , dst.MONTH as MONTH
       , dst.week as WEEK
       , dst.period as PERIOD
       , dst.studyday as STUDYDAY
       , nvl (case when regexp_like(dst.visittext, '^([0-9]*)$') then lpad(dst.visittext, 4, '0') else dst.visittext end, 'X')
         || '_'
         || nvl(regexp_replace(to_char(dst.YEAR, 'FM0000D99'), '\' || trim(to_char(1 / 10, 'D')) || '$'), 'X')
         || '_'
         || nvl(regexp_replace(to_char(dst.MONTH, 'FM0000D99'), '\' || trim(to_char(1 / 10, 'D')) || '$'), 'X')
         || '_'
         || nvl(regexp_replace(to_char(dst.week, 'FM0000D99'), '\' || trim(to_char(1 / 10, 'D')) || '$'), 'X')
         || '_'
         || nvl(regexp_replace(to_char(dst.studyday, 'FM0000D99'), '\' || trim(to_char(1 / 10, 'D')) || '$'), 'X')
           as TIMEIDENT
       , trim(   decode(dst.visittext, null, null, ' Visit ' || dst.visittext)
              || decode(dst.YEAR, null, null, ' Year ' || dst.YEAR)
              || decode(dst.MONTH, null, null, ' Month ' || dst.MONTH)
              || decode(dst.week, null, null, ' Week ' || dst.week)
              || decode(dst.studyday, null, null, ' Day ' || dst.studyday))
           as TIMEDESC
       , ds.startday as STARTDAY
       , ds.starthour as STARTHOUR
       , ds.startminute as STARTMINUTE
       , ds.startsecond as STARTSECOND
       , ds.endday as ENDDAY
       , round(ds.endhour, 10) as ENDHOUR
       , ds.endminute as ENDMINUTE
       , ds.endsecond as ENDSECOND
       , ISR$BIOANALYTICS$WT$R$LAL.BuildSamplingTime (ds.startday,
                                                       ds.starthour,
                                                       ds.startminute,
                                                       ds.startsecond,
                                                       ds.endday,
                                                       ds.endhour,
                                                       ds.endminute,
                                                       ds.endsecond)
           as SAMPLINGTIME -- MUIB specific
       , trim(   case when ds.startday is null then null else 'Start day ' || ds.startday end
              || case when ds.starthour is null then null else ' Start hour ' || ds.starthour end
              || case when ds.startminute is null then null else ' Start minute ' || ds.startminute end
              || case when ds.startsecond is null then null else ' Start second ' || ds.startsecond end
              || case when ds.endday is null then null else ' End day ' || ds.endday end
              || case
                   when ds.endhour is null
                   then
                     null
                   else
                        ' End hour '
                     || case
                          when abs(ds.endhour) < 1
                           and abs(ds.endhour) > 0
                          then
                            case when ds.endhour < 0 then '-' else null end || '0' || trim(to_char(abs(ds.endhour), 'TM'))
                          else
                            trim(to_char(ds.endhour, 'TM'))
                        end
                 end
              || case when ds.endminute is null then null else ' End minute ' || ds.endminute end
              || case when ds.endsecond is null then null else ' End second ' || ds.endsecond end)
           as SAMPLINGTIMEDESC
       , 86400 * nvl(ds.startday, 0) + 3600 * nvl(ds.starthour, 0) + 60 * nvl(ds.startminute, 0) + nvl(ds.startsecond, 0)
           as STARTTOTALTIME
       , 86400 * nvl(ds.endday, 0) + 3600 * nvl(ds.endhour, 0) + 60 * nvl(ds.endminute, 0) + nvl(ds.endsecond, 0) as ENDTOTALTIME
       , ds.timetext as TIMETEXT
       , ds.usersampleid as USERSAMPLEID
       , sr.calibrationrangeflag as CALIBRATIONRANGEFLAG
       , sr.calibrationrange as CALIBRATIONRANGE
       , to_char(srcd.RECORDTIMESTAMP, '&&FORMAT_DATE&') as ACCEPTANCETIMESTAMP
       , case
           when /*(srcd.runsamplesequencenumber = ars.runsamplesequencenumber
             and srcd.runid = ars.runid)
             or srcd.runid is null*/
             SR.ReassaysPresentFlag = 'Y'
           then
             'Y'
           else
             'N'
         end
           as REPORTED
       , sr.samplestatus as SAMPLESTATUS
       , to_char(src.RECORDTIMESTAMP, '&&FORMAT_DATE&') as RECORDTIMESTAMP
       , ars.samplecomment as SAMPLECOMMENT
       , arr.RESULT as RESULT
       , arr.RESULTTEXT as RESULTTEXT
       , ars.ExportSampleName as EXPORTSAMPLENAME
    from &&LIMS_USER&.SAMPLERESULTSCONFLICT src
       , ISR$BIO$REASSAYDESIGNSAMPLES rds
       , &&LIMS_USER&.ANALYTICALRUNSAMPLE ars
       , &&LIMS_USER&.ANARUNANALYTERESULTS arr
       , &&LIMS_USER&.ASSAYANALYTES aa
       , (select sccd.*
            from &&LIMS_USER&.SAMPRESCONFLICTDEC sccd
               , (select STUDYID, ANALYTEID, DESIGNSAMPLEID, max(RECORDTIMESTAMP) as RECORDTIMESTAMP
                    from &&LIMS_USER&.SAMPRESCONFLICTDEC
                  group by STUDYID, ANALYTEID, DESIGNSAMPLEID) max_sccd
           where sccd.STUDYID = max_sccd.STUDYID
             and sccd.ANALYTEID = max_sccd.ANALYTEID
             and sccd.DESIGNSAMPLEID = max_sccd.DESIGNSAMPLEID
             and sccd.RECORDTIMESTAMP = max_sccd.RECORDTIMESTAMP) srcd
       , ISR$BIO$DESIGNSAMPLE$MODIFIED ds
       , &&LIMS_USER&.DESIGNSUBJECT dsu
       , &&LIMS_USER&.DESIGNSUBJECTTREATMENT dst
       , &&LIMS_USER&.DESIGNTREATMENT dt
       , &&LIMS_USER&.CONCENTRATIONUNITS cu
       , &&LIMS_USER&.CONFIGSAMPLETYPES cst
       , &&LIMS_USER&.ANALYTICALRUN ar
       , &&LIMS_USER&.ANALYTICALRUNANALYTES ara
       , &&LIMS_USER&.SAMPLERESULTS sr
       , &&LIMS_USER&.ASSAY a
       , &&LIMS_USER&.CONFIGSPECIES cs
   where ds.studyid = RDS.STUDYCODE
     and ds.designsampleid = RDS.TOSAMPLEID
     and src.studyid = ars.studyid
     and src.runid = ars.runid
     and src.runsamplesequencenumber = ars.runsamplesequencenumber
     and src.studyid = ar.studyid
     and src.runid = ar.runid
     and ar.assayid = aa.assayid
     and src.studyid = arr.studyid
     and src.runid = arr.runid
     and ars.runsamplesequencenumber = arr.runsamplesequencenumber
     and src.analyteid = aa.analyteid
     and aa.analyteindex = ara.analyteindex
     and src.studyid = srcd.studyid(+)
     and src.designsampleid = srcd.designsampleid(+)
     and src.analyteid = srcd.analyteid(+)
     and src.studyid = ds.studyid
     and src.designsampleid = ds.designsampleid
     and dsu.designsubjectid = dst.designsubjectid
     and dst.treatmentkey = dt.treatmentkey
     and ds.designsubjectid = dsu.designsubjectid
     and ds.designsubjecttreatmentkey = dst.designsubjecttreatmentkey
     and ds.studyid = dsu.studyid
     and dsu.studyid = dst.studyid
     and dst.studyid = dt.studyid
     and aa.concunitsid = cu.concunitsid
     and cst.sampletypekey = ds.sampletypekey
     and ara.studyid = arr.studyid
     and ara.runid = arr.runid
     and ara.studyid = ar.studyid
     and ara.runid = ar.runid
     and ara.analyteindex = arr.analyteindex
     and sr.studyid(+) = srcd.studyid
     and sr.designsampleid(+) = srcd.designsampleid
     and sr.analyteid(+) = srcd.analyteid
     and aa.assayid = a.assayid
     and a.speciesid = cs.speciesid
