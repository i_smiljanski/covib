CREATE OR REPLACE FORCE VIEW "ISR$BIO$STUDY$SUBJECT" ("ENTITY", "CODE", "STUDYCODE", "SUBJECTGROUPCODE", "DESIGNSUBJECTTAG", "STUDYSUBJECTTAG", "AGE", "WEIGHT", "HEIGHT", "PLACEBOFLAG", "STATUS", "GENDER", "TREATMENTSITEDESC", "SUBJECTGROUPNAME", "RACE", "SUBJECTTEXTVARIABLE1", "SUBJECTTEXTVARIABLE2", "SUBJECTTEXTVARIABLE3", "SUBJECTTEXTVARIABLE4", "SUBJECTTEXTVARIABLE5", "SUBJECTTEXTVARIABLE6", "SUBJECTTEXTVARIABLE7", "SUBJECTTEXTVARIABLE8", "SUBJECTNUMBERVARIABLE1", "SUBJECTNUMBERVARIABLE2", "SUBJECTNUMBERVARIABLE3", "SUBJECTNUMBERVARIABLE4", "SUBJECTNUMBERVARIABLE5", "SUBJECTNUMBERVARIABLE6", "SUBJECTNUMBERVARIABLE7", "SUBJECTNUMBERVARIABLE8") AS 
  select 'BIO$STUDY$SUBJECT' as ENTITY
       , designsubject.studyid || '-' || designsubject.designsubjectid as CODE
       , designsubject.studyid as STUDYCODE
       , designsubject.studyid || '-' || designsubject.subjectgroupid as SUBJECTGROUPCODE
       , designsubject.designsubjecttag as DESIGNSUBJECTTAG
       , designsubject.studysubjecttag as STUDYSUBJECTTAG
       , designsubject.age as AGE
       , designsubject.weight as WEIGHT
       , designsubject.height as HEIGHT
       , designsubject.placeboflag as PLACEBOFLAG
       , designsubject.status as STATUS
       , configgender.gender as GENDER
       , configtreatmentsite.treatmentsitedesc as TREATMENTSITEDESC
       , designsubjectgroup.subjectgroupname as SUBJECTGROUPNAME
       , configrace.race as RACE
       , designsubject.subjecttextvariable1 as SUBJECTTEXTVARIABLE1
       , designsubject.subjecttextvariable2 as SUBJECTTEXTVARIABLE2
       , designsubject.subjecttextvariable3 as SUBJECTTEXTVARIABLE3
       , designsubject.subjecttextvariable4 as SUBJECTTEXTVARIABLE4
       , designsubject.subjecttextvariable5 as SUBJECTTEXTVARIABLE5
       , designsubject.subjecttextvariable6 as SUBJECTTEXTVARIABLE6
       , designsubject.subjecttextvariable7 as SUBJECTTEXTVARIABLE7
       , designsubject.subjecttextvariable8 as SUBJECTTEXTVARIABLE8
       , designsubject.subjectnumbervariable1 as SUBJECTNUMBERVARIABLE1
       , designsubject.subjectnumbervariable2 as SUBJECTNUMBERVARIABLE2
       , designsubject.subjectnumbervariable3 as SUBJECTNUMBERVARIABLE3
       , designsubject.subjectnumbervariable4 as SUBJECTNUMBERVARIABLE4
       , designsubject.subjectnumbervariable5 as SUBJECTNUMBERVARIABLE5
       , designsubject.subjectnumbervariable6 as SUBJECTNUMBERVARIABLE6
       , designsubject.subjectnumbervariable7 as SUBJECTNUMBERVARIABLE7
       , designsubject.subjectnumbervariable8 as SUBJECTNUMBERVARIABLE8
    from &&LIMS_USER&.DESIGNSUBJECT
       , &&LIMS_USER&.CONFIGACTIVESTATUS
       , &&LIMS_USER&.CONFIGGENDER
       , &&LIMS_USER&.CONFIGTREATMENTSITE
       , &&LIMS_USER&.DESIGNSUBJECTGROUP
       , &&LIMS_USER&.CONFIGRACE
   where designsubject.genderid = configgender.genderid(+)
     and designsubject.treatmentsiteid = configtreatmentsite.treatmentsiteid(+)
     and designsubject.status = configactivestatus.activestatuscode(+)
     and designsubject.raceid = configrace.raceid(+)
     and designsubject.studyid = designsubjectgroup.studyid(+)
     and designsubject.subjectgroupid = designsubjectgroup.subjectgroupid(+)
     and designsubject.designsubjectid > 0
  order by designsubject.subjectgroupid, designsubject.designsubjecttag