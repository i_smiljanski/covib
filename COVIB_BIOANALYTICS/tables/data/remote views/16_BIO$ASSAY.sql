CREATE OR REPLACE FORCE VIEW "ISR$BIO$ASSAY" ("ENTITY", "CODE", "STUDYCODE", "MASTERASSAY", "NAME", "SPECIES", "SAMPLETYPEID", "ASSAYTYPE", "DEACTIVATEIND", "REVISIONDATE", "DESCRIPTION", "ASSAYCOMMENTS", "TEMPLATENAME", "VOLUME", "RELIMITSTDLLOQ", "RELIMITSTD", "RELIMITQC", "CVLIMITSTDLLOQ", "CVLIMITSTD", "CVLIMITQC", "CUTPOINTFACTOR", "CUTPOINTOFFSET", "CUTPOINTCONTROL", "CUTPOINTLIMIT", "CUTPOINTMETHOD", "CUTPOINTMIN",  "SIGNALTYPE", "ISMASTER", "RUNNO", "MEANORMEDIAN") AS 
  select distinct
                  'BIO$ASSAY' as ENTITY
                , ma.studyid || '-' || A.runid || '-' || A.assayid as CODE
                , ma.studyid as STUDYCODE
                , a.masterassayid as MASTERASSAY
                , A.assaydescription as NAME
                , cs.species as SPECIES
                , cst.sampletypeid as SAMPLETYPEID
                , cat.ASSAYTYPE as ASSAYTYPE
                , A.deactivateind as DEACTIVATEIND
                , to_char(A.revisiondate, '&&FORMAT_DATE&') as REVISIONDATE
                , A.FULLASSAYDESCRIPTION as DESCRIPTION
                , A.ASSAYCOMMENTS as ASSAYCOMMENTS
                , rt.templatename as TEMPLATE
                , a.standardvolume as VOLUME
                , a.RELIMITSTDLLOQ as RELIMITSTDLLOQ
                , a.RELIMITSTD as RELIMITSTD
                , a.RELIMITQC as RELIMITQC
                , a.CVLIMITSTDLLOQ as CVLIMITSTDLLOQ
                , a.CVLIMITSTD as CVLIMITSTD
                , a.CVLIMITQC as CVLIMITQC
                , a.CUTPOINTFACTOR as CUTPOINTFACTOR
                , a.CUTPOINTOFFSET as CUTPOINTOFFSET
                , a.CUTPOINTCONTROL as CUTPOINTCONTROL
                , a.CUTPOINTLIMIT as CUTPOINTLIMIT
                , a.CUTPOINTMETHOD as CUTPOINTMETHOD
                , a.CUTPOINTMIN as CUTPOINTMIN
                , case
                    when a.HEIGHTORAREA = 0 then 'Area'
                    when a.HEIGHTORAREA = 1 then 'Height'
                    else null
                  end as SIGNALTYPE
                , case
                    when masterassayid = 0 then 'T'
                    else 'F'
                  end as ISMASTER
                , a.runid as RUNNO
                , case
                    when a.MEANORMEDIAN = 0 then 'Mean'
                    when a.MEANORMEDIAN = 1 then 'Median'
                    when a.MEANORMEDIAN = 2 then 'Mean of Means'
                    else null
                  end as SIGNALTYPE
    from &&LIMS_USER&.configsampletypes cst
       , &&LIMS_USER&.configassaytypes cat
       , &&LIMS_USER&.assay A
       , &&LIMS_USER&.configspecies cs
       , &&LIMS_USER&.RunAcceptanceTemplate rt
       , (select distinct studyid, masterassayid assay
            from &&LIMS_USER&.assay
           where masterassayid != 0) ma
   where A.speciesid = cs.speciesid
     and cst.sampletypekey = A.sampletypekey
     and CAT.ASSAYTYPEID (+) = A.ASSAYTYPEID
     and a.RUNACCEPTANCETEMPLATE = rt.templateid(+)
     and ((a.masterassayid = ma.assay
       and a.studyid = ma.studyid)
       or --this is when a.masterassayid != 0 and a.studyid !=0 and
         (a.masterassayid = 0
      and a.studyid = 0
      and runid = -1
      and a.assayid = ma.assay))
