CREATE OR REPLACE FORCE VIEW "ISR$BIO$SHIPMENT$SAMPLEINFO" ("SAMPLCOUNT", "RECEIVED", "ANALYZED", "SHIPMENTCODE", "SHIPMENTNAME", "DATERECEIVED", "STUDYCODE") AS 
  SELECT COUNT (*) AS samplcount,
         SUM (DECODE (DESIGNSAMPLE.SampleStatus, -2, 0, 1)) AS received,
         SUM (
            DECODE (&&LIMS_USER&.SF_SAMPLEINRUNORHASRESULTS (DESIGNSAMPLE.DESIGNSAMPLEID, DESIGNSAMPLE.STUDYID),
                    'Has Accepted Result(s)', 1,
                    'In Analytical Run(s) and has Accepted Result(s)', 1,
                    0))
            AS analyzed,
         shipment.SHIPMENTID,
         MAX (shipment.SHIPMENTNAME) AS shipmentname,
         to_char(MAX (shipment.DATERECEIVED), '&&FORMAT_DATE&') AS DATERECEIVED,
         DESIGNSAMPLE.Studyid
    FROM ISR$BIO$DESIGNSAMPLE$MODIFIED DESIGNSAMPLE,
         &&LIMS_USER&.CONTAINERSAMPLE,
         &&LIMS_USER&.Container,
         &&LIMS_USER&.LOCATIONCONTAINER,
         &&LIMS_USER&.SHIPMENTBOX,
         &&LIMS_USER&.SHIPMENT
   WHERE     DESIGNSAMPLE.Status <> -2 /* not "Not Received"*/
         AND CONTAINER.containerid = LOCATIONCONTAINER.containerid
         AND LOCATIONCONTAINER.shipboxid = SHIPMENTBOX.shipboxid
         AND SHIPMENTBOX.shipmentid = SHIPMENT.shipmentid
         AND CONTAINERSAMPLE.containerid = CONTAINER.containerid
         AND CONTAINERSAMPLE.StudyId = DESIGNSAMPLE.StudyId
         AND CONTAINERSAMPLE.DesignSampleId = DESIGNSAMPLE.DesignSampleId
         AND SHIPMENT.SHIPMENTINOROUT = 'I'
         AND LOCATIONCONTAINER.addeddate = CONTAINERSAMPLE.addeddate
         AND shipment.DATERECEIVED = locationcontainer.addeddate
GROUP BY DESIGNSAMPLE.Studyid, shipment.SHIPMENTID
ORDER BY shipmentname, DATERECEIVED