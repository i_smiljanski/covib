CREATE OR REPLACE FORCE VIEW "ISR$BIO$STUDYSUBJ$GRP$TIME" ("ENTITY", "CODE", "STUDYCODE", "SUBJECTGROUPCODE", "SUBJECTGROUPTREATMENTCODE", "GENDER", "DESIGNSUBJECTTAG", "TREATMENTNAME", "VISIT", "YEAR", "PHASE", "MONTH", "WEEK", "PERIOD", "STUDYDAY", "TIMEIDENT", "TIMEDESC") AS 
  SELECT 'BIO$STUDYSUBJ$GRP$TIME' entity,
          s.code || '-' || t.studyday code,
          g.studycode studycode,
          s.subjectgroupcode subjectgroupcode,
          t.code SUBJECTGROUPTREATMENTCODE,
          s.gender GENDER,
          S.DESIGNSUBJECTTAG DESIGNSUBJECTTAG,
          tm.name treatmentname,
          t.visit visit,
          t.YEAR YEAR,
          t.phase phase,
          t.MONTH MONTH,
          t.week week,
          t.period period,
          T.STUDYDAY STUDYDAY,
             NVL (
                REGEXP_REPLACE (TO_CHAR (t.period, 'FM0000D99'),
                                TRIM (TO_CHAR (1 / 10, 'D')) || '$'),
                'X')
          || '-'
          || t.TIMEIDENT,
             DECODE (t.period, NULL, NULL, 'Period ' || t.period)
          || ' '
          || t.timedesc
     FROM ISR$BIO$STUDY$SUBJECTGROUP G,
          ISR$BIO$STUDYSUBJ$GRP$TREATM T,
          ISR$BIO$STUDY$SUBJECT s,
          ISR$BIO$STUDY$TREATMENT tm
    WHERE     G.studycode = T.studycode
          AND S.SUBJECTGROUPCODE = G.CODE
          AND S.STUDYCODE = G.STUDYCODE
          AND G.CODE = T.SUBJECTGROUPCODE
          --AND (TM.DOSEAMOUNT IS NOT NULL OR TM.DOSEAMOUNT <> 0)
          AND TM.Studycode = G.studycode
          AND T.TREATMENTCODE = TM.CODE
--AND TM.doseamount != 0