CREATE OR REPLACE FORCE VIEW "ISR$BIO$STUDY$ANALYTES" ("ENTITY", "CODE", "STUDYCODE", "NAME", "ANALYTEORDER", "TITLE", "DESCRIPTION", "CONCENTRATIONUNITS", "INTERNALSTANDARD", "LOTNUMBER") AS 
  select distinct
         'BIO$STUDY$ANALYTES' as ENTITY
       , aa.studyid || '-' || aa.analyteid as CODE
       , aa.studyid as STUDYCODE
       , ga.analytedescription as NAME
       , aa.analyteid as ANALYTEORDER
       , ga.drugname as TITLE
       , ga.commentmemo as DESCRIPTION
       , min(cu.concentrationunits) keep (dense_rank first order by aa.assayid) over (partition by aa.studyid, aa.analyteid)
           as CONCENTRATIONUNITS
       , min(aa.internalstandard) keep (dense_rank first order by aa.assayid) over (partition by aa.studyid, aa.analyteid)
           as INTERNALSTANDARD
       , min(aa.lotnumber) keep (dense_rank first order by aa.assayid) over (partition by aa.studyid, aa.analyteid) as LOTNUMBER
       /*, (SELECT COUNT (1)
             FROM (SELECT DISTINCT studyid, designsampleid, runid
                     FROM analyticalrunsample
                    WHERE runsamplekind = 'UNKNOWN') r
            WHERE     studyid = aa.studyid
                  --and runsamplekind ='UNKNOWN'
                  AND runid IN
                         (SELECT DISTINCT ara.runid
                            FROM analyticalrunanalytes ara,
                                 analyticalrun ar,
                                 configruntypes cr
                           WHERE     runanalyteregressionstatus = 4
                                 AND ar.studyid = ara.studyid
                                 AND ar.runid = ara.runid
                                 AND ar.RUNTYPEID = CR.RUNTYPEID
                                 AND cr.RUNTYPEDESCRIPTION !=
                                        'MANDATORY REPEATS'
                                 AND ara.studyid = aa.studyid
                                 AND analyteindex = aa.analyteindex)
          ) as sampleofrejected*/ -- MUIB specific, need to check how we will implement this for 3.5
    from &&LIMS_USER&.assayanalytes aa
       , &&LIMS_USER&.globalanalytes ga
       , &&LIMS_USER&.concentrationunits cu
   where aa.ANALYTEID = ga.GLOBALANALYTEID
     and aa.concunitsid = cu.CONCUNITSID
 (+)
  order by aa.studyid, analyteid