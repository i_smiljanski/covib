CREATE OR REPLACE VIEW ISR$BIO$RUN$ANA$STABILITY$CONC
(ENTITY, CODE, STUDYCODE, RUNCODE, ANALYTECODE, 
 NAMEPREFIX, RUNNO, ANALYTENAME, CONCENTRATION, RUNANACONCTEXT)
AS 
select 'BIO$RUN$ANA$STABILITY$CONC' entity
     , r.code||'-'||sa.analyteorder||'-'||isr$format.fmtnum(rak.concentration) code
     , r.studycode                  studycode
     , r.code                       runcode
     , sa.code                      analytecode
     , rak.nameprefix               nameprefix
     , r.runid                      runno
     , sa.name                      analytename
     , rak.concentration            concentration
     , null                         runanaconctext
from isr$bio$run r,
     isr$bio$Study$analytes sa,
     isr$bio$Run$Ana$known rak
where r.studycode=sa.studycode
  and r.studycode=rak.studycode
  and r.code=rak.runcode
  and sa.code=rak.analytecode
  and rak.knowntype='KNOWN'