CREATE OR REPLACE FORCE VIEW "ISR$BIO$SAMPLE" ("ENTITY", "CODE", "STUDYCODE", "SUBJECTCODE", "SUBJECTGROUPCODE", "SUBJECTGROUPTREATMENTCODE", "TREATMENTCODE", "DESIGNSAMPLEID", "DESIGNSUBJECTTREATMENTKEY", "DESIGNSUBJECTTAG", "STUDYSUBJECTTAG", "SUBJECTGROUPNAME", "SAMPLETYPESPLITKEY", "SAMPLETYPEKEY", "SPLITNUMBER", "STATUS", "COMMENTMEMO", "SAMPLINGTIMEID", "STARTDAY", "STARTHOUR", "STARTMINUTE", "STARTSECOND", "ENDDAY", "ENDHOUR", "ENDMINUTE", "ENDSECOND", "RELATIVEDAY", "RELATIVEHOUR", "RELATIVEMINUTE", "SAMPLINGTIME", "SAMPLINGTIMEDESC", "RELATIVESAMPLINGTIME", "RELATIVESAMPLINGTIMEDESC", "STARTTOTALTIME", "ENDTOTALTIME", "RELATIVETOTALTIME", "USERSAMPLEID", "SAMPLESTATUS", "SAMPLEVOLUME", "SAMPLINGDATETIME", "VIALSAMPLEID", "SAMPLEENTRYMODE", "SAMPLEENTRYDATETIME", "VISIT", "YEAR", "PHASE", "MONTH", "WEEK", "PERIOD", "STUDYDAY", "TIMEIDENT", "TIMEDESC", "TIMETEXT", "RECORDTYPE", "TREATMENTSITEDESC", "REVISIONNUMBER", "RECORDTIMESTAMP", "USERSAMPLEIDKEY", "CONDITION2", "RECEIPTDATE", "RECEIPTDATERAW", "DATERECEIVEDNUM", "RECEIPTUSERID", "DISCREPANCY", "ISOTOPE1KEY", "ISOTOPE2KEY", "INFECTIOUSAGENTKEY", "IDENTIFICATIONSTATUSKEY", "SAMPLETEXTVARIABLE1", "SAMPLETEXTVARIABLE2", "SAMPLETEXTVARIABLE3", "OTHERCONDITIONKEY", "OTHERCONDITIONTEXT", "TUBECONDITIONKEY", "FROZENCONDITIONKEY", "RADIOLABELED", "INFECTIOUS", "INACTIVATIONTEMP", "INACTIVATIONDATEIN", "INACTIVATIONDATEOUT", "ISFORMULATION", "ANALYSISSTATUS", "WATSONID", "MATRIX", "MATRIXABBREVIATION", "IDENTIFICATIONSTATUSTEXT", "TUBECONDITIONTEXT", "FROZENCONDITIONTEXT") AS 
  select distinct
         'BIO$SAMPLE' as ENTITY
       , d.studyid || '-' || d.designsampleid as CODE
       , d.studyid as STUDYCODE
       , d.studyid || '-' || d.designsubjectid as SUBJECTCODE
       , d.studyid || '-' || d.subjectgroupid as SUBJECTGROUPCODE
       , d.studyid || '-' || D.TREATMENTEVENTID as SUBJECTGROUPTREATMENTCODE
       , d.studyid || '-' || DE.treatmentkey as TREATMENTCODE
       , d.designsampleid as DESIGNSAMPLEID
       , d.designsubjecttreatmentkey as DESIGNSUBJECTTREATMENTKEY
       , ds.designsubjecttag as DESIGNSUBJECTTAG
       , ds.studysubjecttag as STUDYSUBJECTTAG
       , dg.subjectgroupname as SUBJECTGROUPNAME
       , d.sampletypesplitkey as SAMPLETYPESPLITKEY
       , d.sampletypekey as SAMPLETYPEKEY
       , d.splitnumber as SPLITNUMBER
       , d.status as STATUS
       , d.commentmemo as COMMENTMEMO
       , d.samplingtimeid as SAMPLINGTIMEID
       , d.startday as STARTDAY
       , d.starthour as STARTHOUR
       , d.startminute as STARTMINUTE
       , d.startsecond as STARTSECOND
       , d.endday as ENDDAY
       , round(d.endhour, 10) as ENDHOUR
       , d.endminute as ENDMINUTE
       , d.endsecond as ENDSECOND
       , d.relativeday as RELATIVEDAY
       , d.relativehour as RELATIVEHOUR
       , d.relativeminute as RELATIVEMINUTE
       /*,    nvl(regexp_replace(to_char(d.startday, 'FM0000D99'), '\' || trim(to_char(1 / 10, 'D')) || '$'), 'X')
         || '_'
         || nvl(regexp_replace(to_char(d.starthour, 'FM0000D99'), '\' || trim(to_char(1 / 10, 'D')) || '$'), 'X')
         || '_'
         || nvl(regexp_replace(to_char(d.startminute, 'FM0000D99'), '\' || trim(to_char(1 / 10, 'D')) || '$'), 'X')
         || '_'
         || nvl(regexp_replace(to_char(d.startsecond, 'FM0000D99'), '\' || trim(to_char(1 / 10, 'D')) || '$'), 'X')
         || '_'
         || nvl(regexp_replace(to_char(d.endday, 'FM0000D99'), '\' || trim(to_char(1 / 10, 'D')) || '$'), 'X')
         || '_'
         || nvl(regexp_replace(to_char(d.endhour, 'FM0000D99'), '\' || trim(to_char(1 / 10, 'D')) || '$'), 'X')
         || '_'
         || nvl(regexp_replace(to_char(d.endminute, 'FM0000D99'), '\' || trim(to_char(1 / 10, 'D')) || '$'), 'X')
         || '_'
         || nvl(regexp_replace(to_char(d.endsecond, 'FM0000D99'), '\' || trim(to_char(1 / 10, 'D')) || '$'), 'X')
           as SAMPLINGTIME*/
       , ISR$BIOANALYTICS$WT$R$LAL.BuildSamplingTime (d.startday,
                                                      d.starthour,
                                                      d.startminute,
                                                      d.startsecond,
                                                      d.endday,
                                                      d.endhour,
                                                      d.endminute,
                                                      d.endsecond)
           as SAMPLINGTIME -- MUIB specific
       , trim(   case when d.startday is null then null else 'Start day ' || d.startday end
              || case when d.starthour is null then null else ' Start hour ' || d.starthour end
              || case when d.startminute is null then null else ' Start minute ' || d.startminute end
              || case when d.startsecond is null then null else ' Start second ' || d.startsecond end
              || case when d.endday is null then null else ' End day ' || d.endday end
              || case
                   when d.endhour is null
                   then
                     null
                   else
                        ' End hour '
                     || case
                          when abs(d.endhour) < 1
                           and abs(d.endhour) > 0
                          then
                            case when d.endhour < 0 then '-' else null end || '0' || trim(to_char(abs(d.endhour), 'TM'))
                          else
                            trim(to_char(d.endhour, 'TM'))
                        end
                 end
              || case when d.endminute is null then null else ' End minute ' || d.endminute end
              || case when d.endsecond is null then null else ' End second ' || d.endsecond end)
           as SAMPLINGTIMEDESC
       ,    nvl(to_char(d.relativeday), 'X')
         || '_'
         || nvl(to_char(d.relativehour), 'X')
         || '_'
         || nvl(to_char(d.relativeminute), 'X')
           as RELATIVESAMPLINGTIME
       , trim(   case when d.relativeday is null then null else ' Relative day ' || d.relativeday end
              || case when d.relativehour is null then null else ' Relative hour ' || d.relativehour end
              || case when d.relativeminute is null then null else ' Relative minute ' || d.relativeminute end)
           as RELATIVESAMPLINGTIMEDESC
       , 86400 * nvl(d.startday, 0) + 3600 * nvl(d.starthour, 0) + 60 * nvl(d.startminute, 0) + nvl(d.startsecond, 0)
           as STARTTOTALTIME
       , 86400 * nvl(d.endday, 0) + 3600 * nvl(d.endhour, 0) + 60 * nvl(d.endminute, 0) + nvl(d.endsecond, 0) as ENDTOTALTIME
       , 86400 * nvl(d.relativeday, 0) + 3600 * nvl(d.relativehour, 0) + 60 * nvl(d.relativeminute, 0) as RELATIVETOTALTIME
       , d.usersampleid as USERSAMPLEID
       , d.samplestatus as SAMPLESTATUS
       , d.samplevolume as SAMPLEVOLUME
       , to_char(d.samplingdatetime, '&&FORMAT_DATE&') as SAMPLINGDATETIME
       , d.vialsampleid as VIALSAMPLEID
       , d.sampleentrymode as SAMPLEENTRYMODE
       , to_char(d.sampleentrydatetime, '&&FORMAT_DATE&') as SAMPLEENTRYDATETIME
       , de.visittext as VISIT
       , de.year as YEAR
       , de.phase as PHASE
       , de.MONTH as MONTH
       , de.week as WEEK
       , de.period as PERIOD
       , de.studyday as STUDYDAY
       ,    nvl( case when regexp_like (de.visittext, '^([0-9]*)$') then lpad(de.visittext, 4, '0') else de.visittext end, 'X') -- MUIB specific
         || '_'
         || nvl(regexp_replace(to_char(de.YEAR, 'FM0000D99'), '\' || trim(to_char(1 / 10, 'D')) || '$'), 'X')
         || '_'
         || nvl(regexp_replace(to_char(de.MONTH, 'FM0000D99'), '\' || trim(to_char(1 / 10, 'D')) || '$'), 'X')
         || '_'
         || nvl(regexp_replace(to_char(de.week, 'FM0000D99'), '\' || trim(to_char(1 / 10, 'D')) || '$'), 'X')
         || '_'
         || nvl(regexp_replace(to_char(de.studyday, 'FM0000D99'), '\' || trim(to_char(1 / 10, 'D')) || '$'), 'X')
           as TIMEIDENT
       , trim(   decode(de.visittext, null, null, ' Visit ' || de.visittext)
              || decode(de.year, null, null, ' Year ' || de.YEAR)
              || decode(de.month, null, null, ' Month ' || de.MONTH)
              || decode(de.week, null, null, ' Week ' || de.week)
              || decode(de.studyday, null, null, ' Day ' || de.studyday))
           as TIMEDESC
       , d.timetext as TIMETEXT
       , d.recordtype as RECORDTYPE
       , CS.TREATMENTSITEDESC as TREATMENTSITEDESC
       , d.revisionnumber as REVISIONNUMBER
       , to_char(d.recordtimestamp, '&&FORMAT_DATE&') as RECORDTIMESTAMP
       , d.usersampleidkey as USERSAMPLEIDKEY
       , d.condition2 as CONDITION2
       , to_char(d.receiptdate, '&&FORMAT_DATE&') as RECEIPTDATE
       , d.receiptdate as RECEIPTDATERAW
       , to_char(d.receiptdate, 'YYYYMMDDHH24MI') as DATERECEIVEDNUM
       , d.receiptuserid as RECEIPTUSERID
       , d.discrepancy as DISCREPANCY
       , d.isotope1key as ISOTOPE1KEY
       , d.isotope2key as ISOTOPE2KEY
       , d.infectiousagentkey as INFECTIOUSAGENTKEY
       , d.identificationstatuskey as IDENTIFICATIONSTATUSKEY
       , d.sampletextvariable1 as SAMPLETEXTVARIABLE1
       , d.sampletextvariable2 as SAMPLETEXTVARIABLE2
       , d.sampletextvariable3 as SAMPLETEXTVARIABLE3
       , d.otherconditionkey as OTHERCONDITIONKEY
       , coc.otherconditiontext as OTHERCONDITIONTEXT
       , d.tubeconditionkey as TUBECONDITIONKEY
       , d.frozenconditionkey as FROZENCONDITIONKEY
       , d.radiolabeled as RADIOLABELED
       , d.infectious as INFECTIOUS
       , d.inactivationtemp as INACTIVATIONTEMP
       , to_char(d.inactivationdatein, '&&FORMAT_DATE&') as INACTIVATIONDATEIN
       , to_char(d.inactivationdateout, '&&FORMAT_DATE&') as INACTIVATIONDATEOUT
       , d.isformulation as ISFORMULATION
       , ISR$BIOANALYTICS$WT$R$LAL.SampleInRunOrHasResults(d.designsampleid, d.studyid) as ANALYSISSTATUS
       , &&LIMS_USER&.SF_BuildBarcode(d.StudyId, d.DesignSampleId) AS WatsonID
       , cst.SampleTypeId as MATRIX
       , cst.SAMPLETYPEABBREVIATION as MATRIXABBREVIATION
       , cis.IDENTIFICATIONSTATUSTEXT as IDENTIFICATIONSTATUSTEXT
       , ctc.tubeconditiontext as TUBECONDITIONTEXT
       , cfc.frozenconditiontext as FROZENCONDITIONTEXT
    from ISR$BIO$DESIGNSAMPLE$MODIFIED d
       , &&LIMS_USER&.designsubjectgroup dg
       , &&LIMS_USER&.designsubject ds
       , &&LIMS_USER&.designevent de
       , &&LIMS_USER&.configtreatmentsite cs
       , &&LIMS_USER&.CONFIGOTHERCONDITION coc
       , &&LIMS_USER&.ConfigSampleTypes cst
       , &&LIMS_USER&.ConfigIdentificationStatus cis
       , &&LIMS_USER&.ConfigTubeCondition ctc
       , &&LIMS_USER&.ConfigFrozenCondition cfc
   where d.subjectgroupid = dg.subjectgroupid
     and d.studyid = dg.studyid
     and d.designsubjectid = ds.designsubjectid
     and d.studyid = ds.studyid
     and d.studyid = de.studyid
     and d.treatmenteventid = de.treatmenteventid
     and ds.status = 2
     and d.treatmentsiteid = cs.treatmentsiteid(+)
     and d.otherconditionkey = coc.otherconditionkey(+)
     and d.SampleTypeKey = cst.SampleTypeKey(+)
     and d.IdentificationStatusKey = cis.IdentificationStatusKey(+)
     and d.TubeConditionKey = ctc.TubeConditionKey(+)
     and d.FrozenConditionKey = cfc.FrozenConditionKey(+)
