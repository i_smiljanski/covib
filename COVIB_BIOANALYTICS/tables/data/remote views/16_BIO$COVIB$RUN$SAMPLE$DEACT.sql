CREATE OR REPLACE FORCE VIEW "ISR$BIO$COVIB$RUN$SAMPLE$DEACT" ("ENTITY", "CODE", "SAMPLENAME", "SAMPLEDESC", "RUNID", "RUNSAMPLESEQUENCENUMBER", "STUDYCODE") AS 
  SELECT 'BIO$COVIB$RUN$SAMPLE' as ENTITY
     , s.code
     , s.sampleName
     , s.sampleName || ', Run: ' || r.runID || ', Seq.No.: ' || s.runSampleSequenceNumber || ', Replicate No.:' || s.replicateNumber SAMPLEDESC
     , r.runID                                               
     , s.runSampleSequenceNumber
     --, sa.analyteOrder
     --, sa.name
     , s.studyCode
  FROM ISR$bio$run$worklist s,
       ISR$bio$run r--,
       --ISR$bio$run$analytes ra,
       --ISR$bio$study$analytes sa,
       --ISR$bio$run$worklist$result$rw sr
 WHERE r.Code = s.runCode
   --AND sr.runAnalyteCode = ra.code
   --AND r.Code= ra.runCode
   --AND ra.analyteCode = sa.Code
   AND s.studyCode = r.studyCode
   --AND s.studyCode = sr.studyCode
   --AND s.studyCode = sa.studyCode
   --AND s.studyCode = ra.studyCode
 ORDER BY s.studyCode, r.runID, s.runSampleSequenceNumber