CREATE OR REPLACE FORCE VIEW "ISR$BIO$RUN$ANALYTES" ("ENTITY", "CODE", "STUDYCODE", "RUNCODE", "ANALYTECODE", "REGPARAMVAL1", "REGPARAMVAL2", "REGPARAMVAL3", "REGPARAMVAL4", "REGPARAMVAL5", "RSQUARED", "CONFIDENCE95", "NM", "VEC", "WEIGHTINGFACTOR", "RUNANALYTEREGRESSIONSTATUS", "REGRESSIONTEXT", "REGRESSIONID", "LLOQ", "ULOQ", "CONCENTRATIONUNITS","SIGNIFICANTPLACES","RUNNO") AS 
  select 'BIO$RUN$ANALYTES' as ENTITY
       , ara.studyid || '-' || ara.runid || '-' || aa.analyteid as CODE
       , ara.studyid as STUDYCODE
       , ara.studyid || '-' || ara.runid as RUNCODE
       , ara.studyid || '-' || aa.analyteid as ANALYTECODE
       , ar1.parametervalue as REGPARAMVAL1
       , ar2.parametervalue as REGPARAMVAL2
       , ar3.parametervalue as REGPARAMVAL3
       , ar4.parametervalue as REGPARAMVAL4
       , ar5.parametervalue as REGPARAMVAL5
       , ara.rsquared as RSQUARED
       , ar1.confidence95 as CONFIDENCE95
       , ara.nm as NM
       , ara.vec as VEC
       , aa.weightingfactor as WEIGHTINGFACTOR
       , ara.RUNANALYTEREGRESSIONSTATUS as RUNANALYTEREGRESSIONSTATUS
       , CR.REGRESSIONTEXT as REGRESSIONTEXT
       , aa.regressionidentifier as REGRESSIONID
       , aa.nm as LLOQ
       , aa.vec as ULOQ
       , cu.concentrationunits as CONCENTRATIONUNITS
       , aa.decimalplaces as SIGNIFICANTPLACES
       , ara.runid as RUNNO
    from &&LIMS_USER&.anarunregparameters ar1
       , &&LIMS_USER&.anarunregparameters ar2
       , &&LIMS_USER&.anarunregparameters ar3
       , &&LIMS_USER&.anarunregparameters ar4
       , &&LIMS_USER&.anarunregparameters ar5
       , &&LIMS_USER&.analyticalrunanalytes ara
       , &&LIMS_USER&.configregressiontypes cr
       , &&LIMS_USER&.concentrationunits cu
       , &&LIMS_USER&.assayanalytes aa
       , &&LIMS_USER&.analyticalrun ar
   where ar1.runid(+) = ara.runid
     and ar1.ANALYTEINDEX(+) = ara.ANALYTEINDEX
     and ar1.STUDYID(+) = ara.STUDYID
     and ar1.regressionparameterid(+) = 1
     and ar2.runid(+) = ara.runid
     and ar2.ANALYTEINDEX(+) = ara.ANALYTEINDEX
     and AR2.STUDYID(+) = ara.studyid
     and ar2.regressionparameterid(+) = 2
     and ar3.runid(+) = ara.runid
     and ar3.ANALYTEINDEX(+) = ara.ANALYTEINDEX
     and AR3.STUDYID(+) = ara.studyid
     and ar3.regressionparameterid(+) = 3
     and ar4.runid(+) = ara.runid
     and ar4.ANALYTEINDEX(+) = ara.ANALYTEINDEX
     and AR4.STUDYID(+) = ara.studyid
     and ar4.regressionparameterid(+) = 4
     and ar5.runid(+) = ara.runid
     and ar5.ANALYTEINDEX(+) = ara.ANALYTEINDEX
     and AR5.STUDYID(+) = ara.studyid
     and ar5.regressionparameterid(+) = 5
     and aa.ANALYTEINDEX = ara.ANALYTEINDEX
     and aa.assayId = ar.assayid
     and aa.regressionidentifier = nvl(ara.regressionidentifier,aa.regressionidentifier)
     and ar.runid = ara.runid
     and ar.studyid = ara.studyid
     and CR.REGRESSIONID(+) = ara.regressionidentifier
     and aa.concunitsid = cu.concunitsid(+)
     -- because of MUIB-179. The status will be filtered in the backend
     /*and ara.RUNANALYTEREGRESSIONSTATUS in (select value
                                              from ISR$PARAMETER$VALUES
                                             where entity = 'BIO$RUNANALYTEREGRESSIONSTATUS')*/
  order by ar1.studyid, ar1.runid, aa.analyteid