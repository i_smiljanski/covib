  CREATE OR REPLACE FORCE VIEW "ISR$BIO$COVIB$CONDITIONS" ("ENTITY", "CODE", "STUDYCODE", "DISPLAY", "ORDERNUMBER", "CONDITION") AS 
  SELECT 'BIO$COVIB$CONDITIONS' entity
     , sa.code||'-'||p.condition code
     , sa.StudyCode
     , 'Analyte: '||sa.name||', Condition: '||p.condition display
     , 0 ordernumber
     , p.condition
  FROM
      (
        SELECT distinct
               k.analytecode,
               to_number(regexp_replace(k.nameprefix,'^.*[ _\-]WBS(\d+)[ _\-]?.*$', '\1', 1, 1, 'i')) condition
          FROM ISR$bio$run$ana$known k
         WHERE regexp_like(k.nameprefix,'^.*[ _\-]WBS(\d+)[ _\-]?.*$','i')
           AND k.knowntype = 'KNOWN'
      ) p,
      ISR$BIO$STUDY$ANALYTES sa
  WHERE sa.code = p.analytecode
  ORDER BY sa.name, p.condition