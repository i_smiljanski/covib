CREATE OR REPLACE FORCE VIEW "ISR$BIO$STUDY$CONTACTS" ("ENTITY", "CODE", "STUDYCODE", "NAME", "TITLE", "DESCRIPTION", "TYPE", "SITE", "SITESTATE", "SITECITY", "SITEZIP", "SITEADDRESS1", "SITEADDRESS2", "SITEADDRESS3", "SITEADDRESS4") AS 
  select 'BIO$STUDY$CONTACTS' as ENTITY
       , contactstudy.studyid || '-' || contacts.contactid as CODE
       , contactstudy.studyid as STUDYCODE
       , contacts.contactname as NAME
       , contacts.title as TITLE
       , contacts.commentmemo as DESCRIPTION
       , configcontacttypes.contacttypetext as TYPE
       , configtreatmentsite.treatmentsitedesc as SITE
       , configtreatmentsite.sitestate as SITESTATE
       , configtreatmentsite.sitecity as SITECITY
       , configtreatmentsite.sitezip as SITEZIP
       , configtreatmentsite.siteaddr1 as SITEADDRESS1
       , configtreatmentsite.siteaddr2 as SITEADDRESS2
       , configtreatmentsite.siteaddr3 as SITEADDRESS3
       , configtreatmentsite.siteaddr4 as SITEADDRESS4
    from &&LIMS_USER&.CONTACTSTUDY
       , &&LIMS_USER&.CONTACTS
       , &&LIMS_USER&.CONFIGCONTACTTYPES
       , &&LIMS_USER&.CONFIGTREATMENTSITE
   where contacts.contacttypeid = configcontacttypes.contacttypeid
     and contacts.treatmentsiteid = configtreatmentsite.treatmentsiteid(+)
     and contactstudy.contactid = contacts.contactid
  order by upper(configcontacttypes.contacttypetext), upper(contacts.contactname)