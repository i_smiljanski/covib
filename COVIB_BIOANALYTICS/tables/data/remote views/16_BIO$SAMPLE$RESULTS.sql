CREATE OR REPLACE FORCE VIEW "ISR$BIO$SAMPLE$RESULTS" ("ENTITY", "CODE", "ANALYTECODE", "SAMPLECODE", "STUDYCODE", "DESIGNSAMPLEID", "ANALYTEDESCRIPTION", "SAMPLESTATUS", "CONCENTRATION", "CONCUNITSID", "CONCENTRATIONSTATUS", "CALIBRATIONRANGEFLAG", "CALIBRATIONRANGE", "ALIQUOTFACTOR", "PRECISION", "REASSAYSPRESENTFLAG", "ACCEPTANCETIMESTAMP", "USERNAME", "IMPORTEDSAMPLE", "RESULTCOMMENTTEXT", "IMPORTEDDATASOURCE", "RUNID", "RUNSAMPLESEQUENCENUMBER", "ISCHOICE") AS 
  select distinct 'BIO$SAMPLE$RESULTS' as ENTITY
                , s.studyid || '-' || s.designsampleid || '-' || s.analyteid as CODE
                , s.studyid || '-' || s.analyteid as ANALYTECODE
                , s.studyid || '-' || s.designsampleid as SAMPLECODE
                , s.studyid as STUDYCODE
                , s.designsampleid as DESIGNSAMPLEID
                , GA.ANALYTEDESCRIPTION as ANALYTEDESCRIPTION
                , s.samplestatus as SAMPLESTATUS
                , round(decode(s.samplestatus, 1, null, s.concentration), 10) as CONCENTRATION
                , s.concunitsid as CONCUNITSID
                , decode(s.samplestatus, 1, null, s.concentrationstatus) as CONCENTRATIONSTATUS
                , decode(s.samplestatus, 1, null, s.calibrationrangeflag) as CALIBRATIONRANGEFLAG
                , s.calibrationrange as CALIBRATIONRANGE
                , s.aliquotfactor as ALIQUOTFACTOR
                , s.PRECISION as PRECISION
                , s.reassayspresentflag as REASSAYSPRESENTFLAG
                , to_char(s.acceptancetimestamp, '&&FORMAT_DATE&') as ACCEPTANCETIMESTAMP
                , sa.firstname || ' ' || SA.LASTNAME as USERNAME
                , s.importedsample as IMPORTEDSAMPLE
                , s.resultcommenttext as RESULTCOMMENTTEXT
                , s.importeddatasource as IMPORTEDDATASOURCE
                , s.runid as RUNID
                , s.runsamplesequencenumber as RUNSAMPLESEQUENCENUMBER
                , case when cnt > 0 then 'Y' else 'N' end as ISCHOICE
    from &&LIMS_USER&.SAMPLERESULTS s
       , &&LIMS_USER&.GLOBALANALYTES ga
       , &&LIMS_USER&.SECUSERACCOUNTS sa
       , (select count(1) cnt
               , sc.studyid
               , sc.analyteid
               , sc.designsampleid
               , sc.runid
               , sc.runsamplesequencenumber
            from &&LIMS_USER&.SAMPRESCONFLICTCHOICES sc
          group by sc.studyid
                 , sc.analyteid
                 , sc.designsampleid
                 , sc.runid
                 , sc.runsamplesequencenumber) sc
   where s.userid = sa.userid
     and S.ANALYTEID = GA.GLOBALANALYTEID
     and sc.studyid(+) = s.studyid
     and sc.analyteid(+) = s.analyteid
     and sc.designsampleid(+) = s.designsampleid
     and sc.runid(+) = s.runid
     and sc.runsamplesequencenumber(+) = s.runsamplesequencenumber
  order by s.designsampleid
         , s.studyid
         , s.studyid || '-' || s.analyteid
         , s.runid
         , s.runsamplesequencenumber

