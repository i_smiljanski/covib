CREATE OR REPLACE FORCE VIEW "ISR$BIO$STUDYSUBJ$GRP$TREATM" ("ENTITY", "CODE", "STUDYCODE", "SUBJECTGROUPCODE", "TREATMENTCODE", "VISIT", "YEAR", "PHASE", "MONTH", "WEEK", "PERIOD", "STUDYDAY", "TIMEIDENT", "TIMEDESC", "EVENTPOSITION", "STATUS") AS 
  select 'BIO$STUDYSUBJ$GRP$TREATM' as ENTITY
       , studyid || '-' || treatmenteventid as CODE
       , studyid as STUDYCODE
       , studyid || '-' || subjectgroupid as SUBJECTGROUPCODE
       , studyid || '-' || treatmentkey as TREATMENTCODE
       , d.visittext as VISIT
       , d.YEAR as YEAR
       , d.phase as PHASE
       , d.month as MONTH
       , d.week as WEEK
       , d.period as PERIOD
       , d.studyday as STUDYDAY
       , nvl( case when regexp_like (d.visittext, '^([0-9]*)$') then lpad(d.visittext, 4, '0') else d.visittext end, 'X') -- MUIB specific
         || '_'
         || nvl(regexp_replace(to_char(d.year, 'FM0000D99'), trim(to_char(1 / 10, 'D')) || '$'), 'X')
         || '_'
         || nvl(regexp_replace(to_char(d.month, 'FM0000D99'), trim(to_char(1 / 10, 'D')) || '$'), 'X')
         || '_'
         || nvl(regexp_replace(to_char(d.week, 'FM0000D99'), trim(to_char(1 / 10, 'D')) || '$'), 'X')
         || '_'
         || nvl(regexp_replace(to_char(d.studyday, 'FM0000D99'), trim(to_char(1 / 10, 'D')) || '$'), 'X')
           as TIMEIDENT
       , trim(   decode(d.visittext, null, null, ' Visit ' || d.visittext)
              || decode(d.year, null, null, ' Year ' || d.year)
              || decode(d.month, null, null, ' Month ' || d.month)
              || decode(d.week, null, null, ' Week ' || d.week)
              || decode(d.studyday, null, null, ' Day ' || d.studyday))
           as TIMEDESC
       , eventposition as EVENTPOSITION
       , status as STATUS
    from &&LIMS_USER&.DESIGNEVENT d
  order by studycode, subjectgroupcode, timeident
