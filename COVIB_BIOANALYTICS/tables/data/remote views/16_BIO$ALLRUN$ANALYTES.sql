CREATE OR REPLACE FORCE VIEW "ISR$BIO$ALLRUN$ANALYTES" ("ENTITY", "CODE", "RUNANALYTECODE", "STUDYCODE", "ANALYTECODE", "NAME", "RUNID", "ANALYTEDESC", "RUNTYPE", "STARTDATE", "EXTRACTIONDATE", "NOTEBOOK", "PAGENUMBER", "ASSAYDESC", "ANALYTEORDER", "REGSTATUS", "NM", "VEC", "REASON", "FIRSTNAME", "MIDDLENAME", "LASTNAME", "RUNSTATUSNUM", "RUNSTATUS", "OBSERVATIONS", "INSTRUMENTS", "FIRSTINJECT", "LASTINJECT", "ANARUNIMPORTFILENAME") AS 
  select 'BIO$ALLRUN$ANALYTES' as ENTITY
       , ar.studyid || '-' || ar.runid || '-' || asa.analyteid as CODE
       , ar.studyid || '-' || ar.runid || '-' || asa.analyteid as RUNANALYTECODE
       , ar.studyid as STUDYCODE
       , ar.studyid || '-' || asa.analyteid as ANALYTECODE
       , ar.rundescription as NAME
       , ar.runid as RUNID
       , ga.analytedescription as ANALYTEDESC
       , crt.runtypedescription as RUNTYPE
       , to_char(ar.runstartdate, '&&FORMAT_DATE&') as STARTDATE
       , to_char(ar.extractiondate, '&&FORMAT_DATE&') as EXTRACTIONDATE
       , ar.notebook as NOTEBOOK
       , ar.pagenumber as PAGENUMBER
       , ass.assaydescription as ASSAYDESC
       , asa.analyteindex as ANALYTEORDER
       , nvl(nvl(cs2.anaregstatusdesc, cs.anaregstatusdesc), 'Not available') as REGSTATUS
       , ara.nm as NM
       , ara.vec as VEC
       , ara.acceptrejectreason as REASON
       , sa.firstname as FIRSTNAME
       , sa.middleinitial as MIDDLENAME
       , sa.lastname as LASTNAME
       , ar.runstatus as RUNSTATUSNUM
       , cs.anaregstatusdesc as RUNSTATUS
       , ar.observations as OBSERVATIONS
       , (
           select listagg(instcompname,', ') within group (order by recordtimestamp)
             from &&LIMS_USER&.ANALYTICALRUNINSTRUMENTS
            where INSTTYPENAME <> 'Sample Prep' 
              and studyid = ar.studyid 
              and runid = ar.runid
            group by studyid,runid
         ) as INSTRUMENTS --MUIB specific
       , (
           select TO_CHAR(min(a.assaydatetime), 'DD-MON-YYYY HH24:MI') assaydatetime
             from &&LIMS_USER&.ANALYTICALRUNSAMPLE a
            where upper(a.runsamplekind) like '%DOUBLE BLANK%'
              and a.studyid = ar.studyid
              and a.runid = ar.runid 
         ) as FIRSTINJECT -- MUIB specific
       , (
           select distinct to_char(assaydatetime, 'DD-MON-YYYY HH24:MI') assaydatetime
             from &&LIMS_USER&.ANALYTICALRUNSAMPLE 
            where studyid =   ar.studyid 
              and runid = ar.runID
              and assaydatetime >= (
                select max(assaydatetime) 
                  from &&LIMS_USER&.ANALYTICALRUNSAMPLE
                 where studyid = ar.studyid
                   and runid = ar.runID 
              )
         ) as LASTINJECT -- MIB specific
       , ara.AnaRunImportFileName as ANARUNIMPORTFILENAME
    from &&LIMS_USER&.analyticalrun ar
         join &&LIMS_USER&.assay ass on ar.assayid = ass.assayid
         join &&LIMS_USER&.assayanalytes asa on ar.assayid = asa.assayid
         join &&LIMS_USER&.configruntypes crt on ar.runtypeid = crt.runtypeid
         join &&LIMS_USER&.globalanalytes ga on asa.analyteid = ga.globalanalyteid
         join &&LIMS_USER&.secuseraccounts sa on ar.userid = sa.userid
         left join &&LIMS_USER&.analyticalRunAnalytes ara
           on ara.studyid = ar.studyid
          and ara.runid = ar.runid
          and ara.analyteindex = asa.analyteindex
         left join &&LIMS_USER&.configanalyterunstatus cs on ar.runstatus = cs.runanalyteregressionstatus
         left join &&LIMS_USER&.configanalyterunstatus cs2 on ara.runanalyteregressionstatus = cs2.runanalyteregressionstatus
  order by ar.studyid, ASA.AnalyteIndex, AR.RunId