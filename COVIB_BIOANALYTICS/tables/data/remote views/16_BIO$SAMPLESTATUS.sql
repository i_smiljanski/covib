CREATE OR REPLACE FORCE VIEW "ISR$BIO$SAMPLESTATUS" ("ENTITY", "CODE", "NAME", "INFO", "ORDERNUMBER") AS 
select 'BIO$SAMPLESTATUS' as ENTITY
       , css.samplestatuscode as CODE
       , css.samplestatustext as NAME  
       , null as INFO
       , css.counterindex as ORDERNUMBER
    from &&LIMS_USER&.CONFIGSAMPLESTATUS css   
  order by css.counterindex