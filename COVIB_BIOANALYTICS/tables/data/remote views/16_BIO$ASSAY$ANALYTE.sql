CREATE OR REPLACE FORCE VIEW "ISR$BIO$ASSAY$ANALYTE" ("ENTITY", "CODE", "ASSAYCODE", "NAME", "STUDYCODE", "ANALYTECODE", "ASSAYIDENTIFIER", "DESCRIPTION", "ANALYTEINDEX", "INTERNALSTANDARDCODE", "CONCENTRATIONUNITS", "WEIGHTINGFACTOR", "CONTROL", "SIGNIFICANTPLACES", "REGRESSIONTEXT", "REGRESSIONID", "LLOQ", "ULOQ", "TRANSITION") AS 
  select distinct
                'BIO$ASSAY$ANALYTE' as ENTITY
                , aA.studyid || '-' || aA.assayid || '-' || aA.analyteid as CODE
                , aA.studyid || '-' || a.runid || '-' || a.assayid as ASSAYCODE
                , aA.analyteid as NAME
                , aA.studyid as STUDYCODE
                , aA.studyid || '-' || aA.analyteid as ANALYTECODE
                , aa.assayid as ASSAYIDENTIFIER
                , ga.analytedescription as DESCRIPTION
                , aa.analyteindex analyteindex
                , aA.studyid || '-' || aa.assayid || '-' || aa.internalstandard as INTERNALSTANDARDCODE
                , cu.concentrationunits as CONCENTRATIONUNITS
                , aa.weightingfactor as WEIGHTINGFACTOR
                , aa.control as CONTROL
                , aa.decimalplaces as SIGNIFICANTPLACES
                , crt.regressiontext as REGRESSIONTEXT
                , aa.regressionidentifier as REGRESSIONID
                , aa.nm as LLOQ
                , aa.vec as ULOQ
                , aa.transition as TRANSITION
    from &&LIMS_USER&.assayanalytes aa
       , &&LIMS_USER&.concentrationunits cu
       , &&LIMS_USER&.configregressiontypes crt
       , &&LIMS_USER&.globalanalytes ga
       , &&LIMS_USER&.assay A
   where a.assayid = aa.assayid
     and aa.analyteid = ga.globalanalyteid
     and aa.concunitsid = cu.concunitsid(+)
     and aa.regressionidentifier = crt.regressionid(+)