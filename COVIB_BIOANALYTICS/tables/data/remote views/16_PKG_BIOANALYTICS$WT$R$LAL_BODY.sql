CREATE OR REPLACE PACKAGE BODY ISR$BIOANALYTICS$WT$R$LAL IS


--***************************************************************************************************  
FUNCTION GetAssayStartDate (cnStudy IN number) RETURN date IS

   CURSOR cGetDate IS
    SELECT min(EXTRACTIONDATE)
    FROM &&LIMS_USER&.ANALYTICALRUN
    WHERE studyid = cnStudy;

  dStart date;
BEGIN
  OPEN cGetDate;
  FETCH cGetDate INTO dStart;
  CLOSE cGetDate;
  RETURN dStart;

END GetAssayStartDate;

--***************************************************************************************************
FUNCTION GetAssayEndDate (cnStudy IN number) RETURN date IS

   CURSOR cGetDate IS
    SELECT max(runstartdate)
    FROM &&LIMS_USER&.ANALYTICALRUN
    WHERE studyid = cnStudy;
  dStart date;
BEGIN
  OPEN cGetDate;
  FETCH cGetDate INTO dStart;
  CLOSE cGetDate;
  RETURN dStart;

END GetAssayEndDate;

--***************************************************************************************************                      
FUNCTION GetAnalyticalStartDate (cnStudy IN number) RETURN date IS

   CURSOR cGetDate IS
    SELECT min(ReceiptdateRaw)
    FROM ISR$BIO$SAMPLE
    WHERE studycode = cnStudy;
  dStart date;
BEGIN
  OPEN cGetDate;
  FETCH cGetDate INTO dStart;
  CLOSE cGetDate;
  RETURN dStart;

END GetAnalyticalStartDate;

--***************************************************************************************************                      
FUNCTION GetAnalyticalEndDate (cnStudy IN number) RETURN date IS
  CURSOR cGetDate IS
    SELECT max(runanalyteregtimestamp) 
    FROM &&LIMS_USER&.analyticalrunanalytes
    WHERE studyid = cnStudy;
  dStart date;
BEGIN
  OPEN cGetDate;
  FETCH cGetDate INTO dStart;
  CLOSE cGetDate;
  RETURN dStart;
END GetAnalyticalEndDate;

--***************************************************************************************************                      
FUNCTION StudyUsesGender(cnStudy IN number) RETURN varchar2 IS
  CURSOR cGender IS     
    SELECT DISTINCT ds.genderid 
    FROM &&LIMS_USER&.designsubject ds,
         &&LIMS_USER&.configgender cg
    WHERE studyid = cnStudy
      and cg.genderid=ds.genderid
    ORDER BY genderid desc NULLS last;  
  nGenderid &&LIMS_USER&.designsubject.genderid%TYPE;
BEGIN
  OPEN cGender;
  FETCH cGender INTO nGenderid;
  CLOSE cGender;
  IF nGenderid IS NOT NULL THEN RETURN 'T';
  ELSE RETURN 'F';
  END IF;
END StudyUsesGender;

--***************************************************************************************************  
FUNCTION GetSampleDeactivated(cnStudyId IN number, cnRunid in number, 
                              cnRunSampleSequenceNumber in number, cnAnalyteIndex in number) RETURN varchar2 is
  cursor cGetDeactivated is
    select apd.studyid, apd.runid 
      from &&LIMS_USER&.anarunpeakdecision apd
     where apd.studyid = cnStudyId
       and apd.runid = cnRunid
       and apd.runsamplesequencenumber = cnRunSampleSequenceNumber
       and apd.analyteindex = cnAnalyteIndex;
  rGetDeactivated cGetDeactivated%rowtype;
begin
  open cGetDeactivated;
  fetch cGetDeactivated into rGetDeactivated;
  if cGetDeactivated%found then
    close cGetDeactivated;
    return 'T';
  else
    close cGetDeactivated;
    return 'F';
  end if;
end GetSampleDeactivated;                          

--***************************************************************************************************  
FUNCTION getFirstReceiveDate (nStudycode IN NUMBER) return date 
is
 cursor cGetTime is
    select min (DATERECEIVEDRAW) DATERECEIVED 
      from ISR$BIO$SHIPMENT
     where studycode=nStudyCode;

  dReturn DATE;
begin
  open cGetTime;
  fetch cGetTime into dReturn;
  close cGetTime;
  return dReturn;

end getFirstReceiveDate;

--***************************************************************************************************  
FUNCTION getLastReceiveDate (nStudycode IN NUMBER) return date
is
 cursor cGetTime is
    select max(DATERECEIVEDRAW) DATERECEIVED
      from ISR$BIO$SHIPMENT
     where studycode=nStudyCode;

  dReturn DATE;
begin
  open cGetTime;
  fetch cGetTime into dReturn;
  close cGetTime;
  return dReturn;

end getLastReceiveDate;

--***************************************************************************************************
Function GetAnaRunAnaData(cnAnalyteIndex in number, cnStudyID in number, cnRunID in number) return tlAnaRunAnaList pipelined is
  cursor cData is
    select --ARA.NM  NM, ARA.VEC  VEC, ARA.AcceptRejectReason REASON, CA.AnaRegStatusDesc  REGSTATUS
           CA.AnaRegStatusDesc REGSTATUS, ARA.NM  NM, ARA.VEC VEC, ARA.AcceptRejectReason REASON
    from 
      &&LIMS_USER&.ANALYTICALRUNANALYTES  ARA,
      &&LIMS_USER&.CONFIGANALYTERUNSTATUS CA
    where ARA.RunAnalyteRegressionStatus = CA.RunAnalyteRegressionStatus(+)
      AND ARA.StudyId = cnStudyId
      AND ARA.RunID = cnRunID
      AND ARA.AnalyteIndex = cnAnalyteIndex;

  --rData cData%rowtype;   

  rAnaRunAna tlAnaRunAnaRec;

begin
  open cData;
  fetch cData into rAnaRunAna;--rData;
  close cData;
  pipe row (rAnaRunAna);
  RETURN;
end GetAnaRunAnaData;

--***************************************************************************************************
FUNCTION  SampleInRunOrHasResults
   (cnDesignSampleId IN &&LIMS_USER&.designsample.DESIGNSAMPLEID%type, /* ID design sample*/  
    cnStudyId IN &&LIMS_USER&.designsample.STUDYID%type) /* Study Id */  
 RETURN varchar2 IS   
  sAnalysis varchar2(60);
  nInSampleResults NUMBER;  
  nInAnalyticalRun NUMBER;  
  nInAcceptedSampleResults NUMBER;  
  CURSOR cAnaRun IS  
    SELECT COUNT(*)  
    FROM  &&LIMS_USER&.ANALYTICALRUNSAMPLE ar  
    WHERE ar.DESIGNSAMPLEID = cnDesignSampleId 
      AND ar.SAMPLESTUDYID = cnStudyId 
      -- for Watson release 7.4
      --AND (ar.ANALYSISTYPE is null or NOT ar.ANALYSISTYPE='ISR')
      ;  
  CURSOR cSampResult IS  
    SELECT COUNT(*)  
    FROM  &&LIMS_USER&.SAMPLERESULTS sr  
    WHERE sr.DESIGNSAMPLEID = cnDesignSampleId AND  
      sr.STUDYID = cnStudyId;  
  CURSOR cAcceptedResult IS  
    SELECT COUNT(*)  
    FROM  &&LIMS_USER&.SAMPLERESULTS sr  
    WHERE sr.DESIGNSAMPLEID = cnDesignSampleId AND  
      sr.STUDYID = cnStudyId and 
      sr.samplestatus=2;  


BEGIN  
  nInSampleResults := 0;  
  nInAnalyticalRun := 0;  
  nInAcceptedSampleResults := 0;  
  sAnalysis := 'None';  

  IF NOT cAnaRun%ISOPEN THEN  
    OPEN cAnaRun;  
  END IF;  
  FETCH cAnaRun INTO nInAnalyticalRun;  
  CLOSE cAnaRun;  

  IF NOT cSampResult%ISOPEN THEN  
    OPEN cSampResult;  
  END IF;  
  FETCH cSampResult INTO nInSampleResults;  
  CLOSE cSampResult;  

  /* Check to see how many of the sample results found are accepted */    
  IF NOT cAcceptedResult%ISOPEN  THEN  
       OPEN cAcceptedResult;  
  END IF;  
  FETCH cAcceptedResult INTO nInAcceptedSampleResults;  
  CLOSE cAcceptedResult;  

  IF nInSampleResults > 0 AND nInAnalyticalRun = 0  THEN   
     sAnalysis := 'Has Accepted Result(s)';  
  ELSIF nInAnalyticalRun > 0 AND nInSampleResults = 0 THEN   
    sAnalysis := 'In Analytical Run(s)';  
  ELSIF nInAnalyticalRun > 0 AND nInSampleResults > 0  THEN  
    IF nInAcceptedSampleResults > 0 THEN  
      sAnalysis := 'In Analytical Run(s) and has Accepted Result(s)'; 
    ELSE 
    /* if no accepted results then leave status at Reassays pending */ 
      sAnalysis := 'Reassay Pending'; 
    END IF;  
  ELSE  
    sAnalysis := 'None';  
  END IF;  

  RETURN sAnalysis;  
END SampleInRunOrHasResults;

--***************************************************************************************************
FUNCTION Buildsamplename(anStudyID IN &&LIMS_USER&.ANALYTICALRUNSAMPLE.STUDYID%type 
                        ,anSampleStudyID IN &&LIMS_USER&.ANALYTICALRUNSAMPLE.STUDYID%type 
                        ,anRunID IN &&LIMS_USER&.ANALYTICALRUNSAMPLE.RUNID%type 
                        ,anDesignSampleID IN &&LIMS_USER&.designsample.DESIGNSAMPLEID%type 
                        ,anAssayLevel IN &&LIMS_USER&.ANALYTICALRUNSAMPLE.ASSAYLEVEL%type 
                        ,anReplicateNum IN &&LIMS_USER&.ANALYTICALRUNSAMPLE.REPLICATENUMBER%type 
                        ,avRunSampleKind IN &&LIMS_USER&.ANALYTICALRUNSAMPLE.RUNSAMPLEKIND%type 
                        ,anInclStudyName IN NUMBER) 
RETURN varchar2 IS 

lvSampleName        varchar2(355);--fieldlengths.LENGTH355%Type; 
lvStudyName         &&LIMS_USER&.STUDY.studyname%TYPE; 
lvID                &&LIMS_USER&.ASSAYREPS.ID%TYPE; 
lnReplicateNumber   NUMBER(9); 
lvDesSubjTag        &&LIMS_USER&.DESIGNSUBJECT.DesignSubjectTag%TYPE; 
lvUserSampleID      &&LIMS_USER&.DESIGNSAMPLE.USERSAMPLEID%TYPE; 
lnSplitNumber       &&LIMS_USER&.DESIGNSAMPLE.splitnumber%TYPE; 
lnEndDay            &&LIMS_USER&.DESIGNSAMPLE.endday%TYPE; 
lnEndHour           &&LIMS_USER&.DESIGNSAMPLE.endhour%TYPE; 
lnEndMinute         &&LIMS_USER&.DESIGNSAMPLE.endminute%TYPE; 
lnPeriod            &&LIMS_USER&.DESIGNSUBJECTTREATMENT.Period%TYPE; 
lvTreatID           &&LIMS_USER&.DESIGNSUBJECTTREATMENT.Treatmentid%TYPE; 
lvSampTypeID        &&LIMS_USER&.CONFIGSAMPLETYPES.SAMPLETYPEID%TYPE; 
lvTimeText          &&LIMS_USER&.DESIGNSAMPLE.TIMETEXT%TYPE; 
lsISFORMULATION     &&LIMS_USER&.DESIGNSAMPLE.ISFORMULATION%TYPE; 
lsWatsonBarcode      varchar2(20);--FIELDLENGTHS.LENGTH20%TYPE; 
lsTREATMENTDESC     &&LIMS_USER&.DESIGNTREATMENT.TREATMENTDESC%TYPE; 
lsFORMULATIONLOT    &&LIMS_USER&.DESIGNTREATMENT.FORMULATIONLOT%TYPE; 
ldPREPDATE          &&LIMS_USER&.DESIGNTREATMENT.PREPDATE%TYPE; 
sBarCodeStudyId     &&LIMS_USER&.study.BARCODESTUDYID%type;

FUNCTION GetCustomUserSampleIdKey(asBarcodeStudyId IN &&LIMS_USER&.study.BARCODESTUDYID%type) 
  RETURN varchar2 /*fieldlengths.LENGTH50%type*/ IS 


lsReturn varchar2(50);--fieldlengths.LENGTH50%type; 
BEGIN 
  lsReturn := TO_CHAR(LENGTH(asBarcodeStudyId)) || asBarcodeStudyId; 
  IF (MOD(LENGTH(lsReturn) + 7, 2) = 1) THEN 
    lsReturn := '0' || lsReturn; 
  END IF; 
  RETURN lsReturn; 
END GetCustomUserSampleIdKey;

function WatsonDateFormat(  adDate IN DATE) 
  RETURN varchar2 /*fieldlengths.LENGTH50%type*/ IS 

lvFormatedDate  varchar2(50);--fieldlengths.LENGTH50%type; 

BEGIN 

  /*  Converts date to std Watson date  */ 

  lvFormatedDate := to_char(adDate, 'DD-Mon-YYYY'); 

  return lvFormatedDate; 

END WatsonDateFormat;

FUNCTION FmtNum(nVal IN number) RETURN VARCHAR2 IS  
  nAbs number := abs(nVal);
  sText varchar2(100);
  sMinus varchar2(1);
begin
  if nAbs < 1 and nAbs > 0 then
    IF nVal < 0 THEN 
      sMinus := '-';
    END IF;
    sText := sMinus||'0'||trim(to_char(nAbs, 'TM'));     
  else
    sText := trim(to_char(nVal, 'TM')); 
  end if;
  return sText;
end FmtNum;

FUNCTION Getnominaltimestring (anEndDay IN &&LIMS_USER&.designsample.ENDDAY%type 
                             , anEndHour IN &&LIMS_USER&.designsample.ENDDAY%type 
                             , anEndMinute IN &&LIMS_USER&.designsample.ENDMINUTE%type 
                           , asTimeText IN &&LIMS_USER&.designsample.TIMETEXT%type 
                          , anStudyid IN &&LIMS_USER&.designsample.STUDYID%type) 
RETURN varchar /*fieldlengths.LENGTH100%type*/ IS 
/* 
 Purpose: This function accepts day, hour, and minutes as variants. It assumes that it is called 
           with variant data taken directly from data records or from strings ( probably  taken 
           from grid cell). In either case, as null data values or an empty string is assumed 
           to correspond to time elements that were not entered to represent the sampling time. 

       Regarding Nominal Time Text, if there is a value then 
       if giNominalTimeStyle = 0 then show the text value only 
       if giNominalTimeStyle = 1 then show the numeric value only 
       if giNominalTimeStyle = 2 then show the text and numeric value. 

*/ 

lvTimeVal varchar2(100);--fieldlengths.LENGTH100%type; 
lngiNominalTimeStyle NUMBER; 
lvTimetext varchar2(50);--fieldlengths.LENGTH50%type; 

BEGIN 
   IF anStudyid IS NOT NULL THEN 
  /* Get configuration value for study */ 
    DECLARE 
      CURSOR TimeStyle_cur 
      IS 
      SELECT  
      VALUEINTEGER 
      FROM &&LIMS_USER&.CONFIGGENERAL 
      WHERE CONFIGGENERAL.studyid = anStudyid 
      AND CONFIGGENERAL.FIELDNAME = 'NominalTimeStyle'; 
    BEGIN 
      IF NOT TimeStyle_cur%ISOPEN 
      THEN 
        OPEN TimeStyle_cur; 
      END IF; 
      FETCH TimeStyle_cur INTO lngiNominalTimeStyle; 
      CLOSE TimeStyle_cur; 
      EXCEPTION 
        WHEN NO_DATA_FOUND THEN 
           /* Default to 0 if there is no configuration record */ 
          lngiNominalTimeStyle := 0; 
        WHEN OTHERS THEN 
           /* Default to 0 if there is no configuration record */ 
          lngiNominalTimeStyle := 0; 
    END; 
    /* Default to 0 if tlngiNominalTimeStyle IS NULL */ 
    IF lngiNominalTimeStyle IS NULL 
    THEN 
      lngiNominalTimeStyle := 0; 
    END IF; 

  ELSE 
     /* If there is no studyid show both for audit purposes */ 
    lngiNominalTimeStyle := 2; 
  END IF; 

  IF asTimeText IS NOT NULL THEN 
      lvTimeVal :=  asTimeText ; 
     lvTimetext :=  asTimeText ; 
  ELSE 
      lvTimeVal :=  NULL ; 
     lvTimetext :=  NULL ; 

  END IF; 
/* If the value for nominal time text is null or the time style is not to show text only then.... 
 */    
  IF   asTimeText IS  NULL OR lngiNominalTimeStyle <> 0 THEN   
     lvTimeVal := ' '; 
     IF anEndDay IS NOT NULL THEN 
         lvTimeVal := lvTimeVal ||'Day ' || anEndDay || ' '; 
     END IF; 

     IF anEndHour IS NOT NULL THEN 
         lvTimeVal := lvTimeVal || FmtNum(anEndHour) || 'h '; 
     END IF; 

     IF anEndMinute IS NOT NULL THEN 
         lvTimeVal := lvTimeVal || anEndMinute || 'm'; 
     END IF; 
  END IF; 
  /* Showing numeric value only or text value only */ 
  IF lngiNominalTimeStyle = 0  OR lngiNominalTimeStyle = 1 THEN 
         RETURN NVL(lvTimeVal,' '); 
  /* Showing numeric and text time */ 
  ELSIF lngiNominalTimeStyle = 2  THEN 
      IF lvTimetext IS NULL THEN 
         RETURN NVL(lvTimeVal,' '); 
      ELSE 
         lvTimeVal := lvTimetext ||  ' / ' || NVL(lvTimeVal, ' '); 
       RETURN NVL(lvTimeVal, ' ');  
      END IF; 
  END IF; 

END Getnominaltimestring;


BEGIN  

IF avRunSampleKind = 'UNKNOWN' THEN 

/* Get StudyName */ 
   IF anInclStudyName IS NOT NULL THEN 
       SELECT studyname INTO lvStudyName FROM &&LIMS_USER&.STUDY WHERE studyid = anSampleStudyID; 
      lvSampleName := lvStudyNAme || ' '; 
   END IF; 


/* Need UserSampleID, endday, 
    endhour, endminute, 
    sampletypeID, splitnumber, 
    designsubjecttag, period, treatmentid*/ 

    SELECT dsub.DesignSubjectTag, ds.usersampleid, ds.endday, ds.endhour, ds.endminute, ds.splitnumber, ds.TIMETEXT, dst.Period, dst.Treatmentid, c.SampletypeID, ds.ISFORMULATION 
        INTO lvDesSubjTag, lvUserSampleID, lnEndDay, lnEndHour, lnEndMinute, lnSplitNumber, lvTimeText, lnPeriod, lvTreatID, lvSampTypeID, lsISFORMULATION 
        FROM ISR$BIO$DESIGNSAMPLE$MODIFIED ds, &&LIMS_USER&.DESIGNSUBJECTTREATMENT dst, &&LIMS_USER&.DESIGNSUBJECT dsub, &&LIMS_USER&.CONFIGSAMPLETYPES c 
        WHERE   ds.studyid = anSampleStudyID 
        AND     ds.designsampleid = anDesignSampleID 
        AND     dst.studyid = ds.studyid 
        AND     dst.designsubjecttreatmentkey = ds.designsubjecttreatmentkey 
        AND     dsub.studyid = ds.studyid 
        AND     dsub.designsubjectid = ds.designsubjectid 
        AND     c.sampletypekey = ds.sampletypekey; 

  IF lsISFORMULATION = 'Y' THEN 
  /* Formulation sample name */ 

    SELECT DESIGNTREATMENT.TREATMENTDESC, DESIGNTREATMENT.FORMULATIONLOT, DESIGNTREATMENT.PREPDATE  
    INTO lsTREATMENTDESC,lsFORMULATIONLOT,ldPREPDATE  
    FROM &&LIMS_USER&.DESIGNTREATMENT, ISR$BIO$DESIGNSAMPLE$MODIFIED DESIGNSAMPLE, &&LIMS_USER&.DESIGNSUBJECTTREATMENT  
    WHERE DESIGNSAMPLE.STUDYID = DESIGNSUBJECTTREATMENT.STUDYID and 
    DESIGNSAMPLE.designsubjecttreatmentkey = DESIGNSUBJECTTREATMENT.designsubjecttreatmentkey and 
    DESIGNSAMPLE.STUDYID = DESIGNTREATMENT.STUDYID and 
    DESIGNSUBJECTTREATMENT.TREATMENTKEY = DESIGNTREATMENT.TREATMENTKEY and 
    DESIGNSUBJECTTREATMENT.studyid = anSampleStudyID AND 
      DESIGNSAMPLE.designsampleid = anDesignSampleID; 

         select sBarCodeStudyId 
          into sBarCodeStudyId
         from &&LIMS_USER&.Study where StudyId = anSampleStudyID;

         lsWatsonBarcode := GetCustomUserSampleIdKey(sBarCodeStudyId) || SUBSTR(CONCAT('000000', anDesignSampleID), -7);         

    lvSampleName := lsTREATMENTDESC; 

      IF lsFORMULATIONLOT IS NOT NULL THEN 
      lvSampleName := lvSampleName ||' ' || lsFORMULATIONLOT; 
    END IF; 
      IF ldPREPDATE IS NOT NULL THEN 
      lvSampleName := lvSampleName ||' ' || WatsonDateFormat(ldPREPDATE); 
    END IF; 


    lvSampleName := lvSampleName ||' ' || lsWatsonBarcode; 

  ELSE 
      IF lvUserSampleID IS NOT NULL THEN 
          lvSampleName := lvSampleName || lvUserSampleID || ' '; 
      END IF; 
  /* Study Sample Name */ 
      lvSampleName := lvSampleName || lvDesSubjTag; 

      IF lvTreatID = '?' THEN 
          IF lnPeriod IS NOT NULL THEN 
              lvSampleName := lvSampleName || ' Per. ' || lnPeriod; 
          END IF; 
      ELSE   /* Use treatment id  */ 
          lvSampleName := lvSampleName || ' ' || lvTreatID; 
          IF lnPeriod IS NOT NULL THEN 
              lvSampleName := lvSampleName || ' P' || lnPeriod; 
          END IF; 
      END IF; 

      /* build matrix-split */ 
      lvSampleName := lvSampleName || ' ' || lvSampTypeID || '-' || lnSplitNumber; 

      /* build TimeString */ 
      lvSampleName := RTRIM(LTRIM(lvSampleName || ' ' || Getnominaltimestring(lnEndDay, lnEndHour, lnEndMinute, lvTimeText, NULL)));  

  END IF; 

ELSIF avRunSampleKind = 'QC' OR 
      avRunSampleKind = 'STANDARD' OR 
      avRunSampleKind = 'STABILITY'  THEN 


    /* Get StudyName */ 
    IF anInclStudyName IS NOT NULL THEN 
        SELECT studyname INTO lvStudyName FROM &&LIMS_USER&.STUDY WHERE studyid = anStudyID; 
      lvSampleName := lvStudyNAme || ' '; 
    END IF; 


      /* Get ID  */ 
      SELECT a.ID INTO lvID 
      FROM  &&LIMS_USER&.ANALYTICALRUN ar, 
            &&LIMS_USER&.ASSAYREPS a 
      WHERE ar.studyid = anStudyID AND 
            ar.runid = anRunID AND 
            a.studyid = ar.studyid AND 
            a.assayid = ar.assayid AND 
            a.knowntype = avRunSamplekind AND 
            a.levelNumber = anAssaylevel; 

      lvSampleName := LTRIM(RTRIM(lvStudyName || ' ' || lvID || ' '|| anReplicateNum)); 

ELSE /* User Defined */ 

       /* Get StudyName */ 
    IF anInclStudyName IS NOT NULL THEN 
        SELECT studyname INTO lvStudyName FROM &&LIMS_USER&.STUDY WHERE studyid = anStudyID; 
      lvSampleName := lvStudyNAme || ' '; 
    END IF; 


      lvSampleName := LTRIM(RTRIM(lvStudyName || ' ' || avRunSampleKind)); 
END IF; 


RETURN lvSampleName; 

EXCEPTION 
   WHEN NO_DATA_FOUND THEN 
           null;  
RETURN lvSampleName; 

END Buildsamplename;

FUNCTION BuildSamplingTime(cnStartday in number, cnStarthour in number, cnStartminute in number, cnStartsecond in number, cnEndday in number, cnEndhour in number, cnEndminute in number, cnEndsecond in number) return varchar2 deterministic
is 
  nHours number;
  nMinutes number;
  procedure ConvertHourToMinutes(nHour in out number, nMinute in out number) is
    nWorkHour number := nvl(nHour,0);
    nWorkMinute number := nvl(nMinute,0);
  begin
    nWorkMinute := nWorkMinute + nWorkHour*60;
    nWorkHour := trunc(nWorkMinute/60);
    nWorkMinute := mod(nWorkMinute,60);
    nHour := nWorkHour;
    nMinute := nWorkMinute;    
  end ConvertHourToMinutes;
begin
null;
  nHours := cnEndhour;
  nMinutes := cnEndminute;
  ConvertHourToMinutes(nHours,nMinutes);
  return nvl(regexp_replace(to_char(cnStartday,'FM0000D99'),'\'||trim(to_char(1/10,'D'))||'$'),'X')||'_'||
         nvl(regexp_replace(to_char(cnStarthour,'FM0000D99'),'\'||trim(to_char(1/10,'D'))||'$'),'X')||'_'||
         nvl(regexp_replace(to_char(cnStartminute,'FM0000D99'),'\'||trim(to_char(1/10,'D'))||'$'),'X')||'_'||
         nvl(regexp_replace(to_char(cnStartsecond,'FM0000D99'),'\'||trim(to_char(1/10,'D'))||'$'),'X')||'_'||
         nvl(regexp_replace(to_char(cnEndday,'FM0000D99'),'\'||trim(to_char(1/10,'D'))||'$'),'X')||'_'||
         nvl(regexp_replace(to_char(nHours,'FM0000D99'),'\'||trim(to_char(1/10,'D'))||'$'),'X')||'_'||
         nvl(regexp_replace(to_char(nMinutes,'FM0000D99'),'\'||trim(to_char(1/10,'D'))||'$'),'X')||'_'||
         nvl(regexp_replace(to_char(cnEndsecond,'FM0000D99'),'\'||trim(to_char(1/10,'D'))||'$'),'X');
end BuildSamplingTime;

END ISR$BIOANALYTICS$WT$R$LAL;