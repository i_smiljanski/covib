CREATE OR REPLACE FORCE VIEW "ISR$BIO$COVIB$RUN$ANALYTES" ("ENTITY", "CODE", "RUNANALYTECODE", "STUDYCODE", "ANALYTECODE", "DESCRIPTION", "NAME", "RUNID", "ASSAYDESC", "ANALYTEDESC", "ANALYTEORDER") AS 
  select 'BIO$COVIB$RUN$ANALYTES' as ENTITY
       , Code
       , RunAnalyteCode
       , StudyCode
       , AnalyteCode
       , 'Run '||RunID||', '||Name||', '||AnalyteDesc as DESCRIPTION
       , Name
       , RunID
       , AssayDesc
       , AnalyteDesc
       , AnalyteOrder
    from ISR$BIO$ALLRUN$ANALYTES
order by StudyCode, AnalyteOrder, RunID
