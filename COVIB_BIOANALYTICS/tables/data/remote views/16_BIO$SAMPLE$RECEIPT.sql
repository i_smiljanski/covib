CREATE OR REPLACE FORCE VIEW "ISR$BIO$SAMPLE$RECEIPT" ("ENTITY", "CODE", "STUDYCODE", "SAMPLESTATUSCODE", "SAMPLESTATUS", "SPLITNUMBER", "MATRIX", "SAMPLECOUNT", "RECEIPTDATE") AS 
  select 'BIO$SAMPLE$RECEIPT' as ENTITY
       , d.studyid || '-' || to_char(d.receiptdate, 'YYYYMMDD') || '-' || d.splitnumber as CODE
       , d.studyid as STUDYCODE
       , d.samplestatus as SAMPLESTATUSCODE
       , d.samplestatus as SAMPLESTATUS
       , d.splitnumber as SPLITNUMBER
       , cst.sampletypeid as MATRIX
       , count(d.designsampleid) as SAMPLECOUNT
       , to_char(d.receiptdate, '&&FORMAT_DATE&') as RECEIPTDATE
    from ISR$BIO$DESIGNSAMPLE$MODIFIED d
       , &&LIMS_USER&.CONFIGSAMPLETYPES cst
   where d.status = 2
     and cst.sampletypekey = d.sampletypekey
  group by d.studyid, receiptdate, splitnumber, d.samplestatus, d.sampletypekey, cst.sampletypeid