CREATE OR REPLACE FORCE VIEW "ISR$BIO$RUN$ANA$KNOWN" ("ENTITY", "CODE", "RUNCODE", "ANALYTECODE", "RUNANALYTECODE", "KNOWNCODE", "NAME", "NAMEPREFIX", "NAMESUFFIX", "DESCRIPTION", "STUDYCODE", "KNOWNTYPE", "ORIGINALKNOWNTYPE", "NUMBEROFREPLICATES", "CONCENTRATION", "MINCONCENTRATION", "BATCHID", "BATCHNAME", "SOURCE", "STABILITYTYPE", "DILUTIONFACTOR", "FLAGPERCENT", "PREPARATIONDATE", "LEVELNUMBER", "CYCLES", "TEMPERATURE", "HOURS", "LONGTERMTIME", "LONGTERMUNITS") AS 
  with separator
       as (select '%' delimiter from dual) --value delimiter
             --from isr$parameter$values
            --where entity = 'BIO$KNOWNSEPARATOR')
  select distinct
         'BIO$RUN$ANA$KNOWN' as ENTITY
       , a.studyid || '-' || ass.runid || '-' || A.knowntype || '-' || replace(A.ID, chr(10), null) || '-' || aA.analyteid
           as CODE
       , A.studyid || '-' || ass.runid as RUNCODE
       , A.studyid || '-' || aA.analyteid as ANALYTECODE
       , A.studyid || '-' || ass.runid || '-' || aA.analyteid as RUNANALYTECODE
       , a.studyid || '-' || A.knowntype || '-' || replace(A.ID, chr(10), null) || '-' || aA.analyteid as KNOWNCODE
       , replace(A.ID, chr(10), null) as NAME
       , nvl(trim(substr(replace(A.ID, chr(10), null)
                       , 1
                       , instr(replace(A.ID, chr(10), null), (select delimiter from separator)) - 1))
           , replace(A.ID, chr(10), null))
           as NAMEPREFIX
       , case
           when instr(a.id, (select delimiter from separator)) < 1
           then
             null
           else
             trim(substr(replace(A.ID, chr(10), null)
                       , instr(replace(A.ID, chr(10), null), (select delimiter from separator)) + 1))
         end
           as NAMESUFFIX
       , replace(A.ID, chr(10), null) as DESCRIPTION
       , A.studyid as STUDYCODE
       , case
             when A.knowntype IN ('QC', 'STABILITY') THEN 'KNOWN'
             else A.knowntype
         end as KNOWNTYPE
       , a.knowntype as ORIGINALKNOWNTYPE
       , a.numberofreplicates as NUMBEROFREPLICATES
       , round(ak.concentration, 14) as CONCENTRATION
       , min(round(ak.concentration, 14)) over (partition by a.studyid, a.knowntype, a.id) as MINCONCENTRATION
       , a.batchid as BATCHID
       , b.batchname as BATCHNAME
       , a.SOURCE as SOURCE
       , a.stabilitytype as STABILITYTYPE
       , case when a.dilutionfactor is null then 1 when a.dilutionfactor = 0 then 0 else round(1 / A.dilutionfactor, 2) end as DILUTIONFACTOR
       , nvl(a.flagpercent
           , case when A.knowntype = 'QC' then ass.RELIMITQC when A.knowntype = 'STANDARD' then ass.RELIMITSTD else null end)
           as FLAGPERCENT
       , --  nvl(a.flagpercent, (select case when Ak.knowntype='QC' then RELIMITQC -- get flagpercent from run=-1
         --                                  when Ak.knowntype='STANDARD' then RELIMITSTD
         --                                  else null end
         --                      from assay
         --                      where studyid=ass.studyid and assaydescription=ass.assaydescription and runid=-1)) flagpercent,
         to_char(a.preparationdate, '&&FORMAT_DATE&') as PREPARATIONDATE
       , a.levelnumber as LEVELNUMBER
       , a.cycles as CYCLES
       , a.temperature as TEMPERATURE
       , a.hours HOURS
       , --a.longtermtime                                                                                    longtermtime,
         case
           when longtermunits = 'M' then round(longtermtime / 30)
           when longtermunits = 'W' then round(longtermtime / 7)
           else longtermtime
         end
           as LONGTERMTIME
       , a.longtermunits as LONGTERMUNITS
    from &&LIMS_USER&.assayreps a
       , &&LIMS_USER&.assay ass
       , &&LIMS_USER&.assayanalytes aa
       , &&LIMS_USER&.batchdefinition b
       , &&LIMS_USER&.assayanalyteknown ak
   where a.batchid = b.batchid(+)
     and a.assayid = ak.assayid(+)
     and a.studyid = ak.studyid(+)
     and a.levelnumber = ak.levelnumber(+)
     and AA.ANALYTEINDEX = Ak.ANALYTEINDEX(+)
     and aa.studyid = a.studyid
     and aa.assayid = a.assayid
     and Ak.knowntype(+) = a.knowntype
     and ak.assayid(+) = aa.assayid
     and ak.studyid(+) = aa.studyid
     and ass.assayid = aa.assayid
     and ass.studyid = aa.studyid
     and ass.runid > -1
