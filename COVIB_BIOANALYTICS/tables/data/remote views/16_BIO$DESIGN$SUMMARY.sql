CREATE OR REPLACE FORCE VIEW "ISR$BIO$DESIGN$SUMMARY" ("ENTITY", "CODE", "STUDYCODE", "SAMPLCOUNT", "RECEIVED", "ANALYZED", "SAMPLESTATUS", "SAMPLESTATUSTEXT") AS 
  select 'BIO$DESIGN$SUMMARY' as ENTITY
       , D.studyid || '-' || CSS.SampleStatusCode as CODE
       , D.studyid as STUDYCODE
       , count(1) as SAMPLCOUNT
       , sum(decode(D.SampleStatus, -2, 0, 1)) as RECEIVED
       , sum(decode(ISR$BIOANALYTICS$WT$R$LAL.SampleInRunOrHasResults(D.DESIGNSAMPLEID, D.STUDYID)
                  , 'Has Accepted Result(s)', 1
                  , 'In Analytical Run(s) and has Accepted Result(s)', 1
                  , 0))
           as ANALYZED
       , CSS.SampleStatusCode as SAMPLESTATUS
       , max(CSS.SampleStatusText) as SAMPLESTATUSTEXT
    from ISR$BIO$DESIGNSAMPLE$MODIFIED D, &&LIMS_USER&.ConfigSampleStatus CSS
   where D.Status = 2
     and D.SampleStatus = CSS.SampleStatusCode
  group by D.studyid, CSS.SampleStatusCode