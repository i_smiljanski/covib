CREATE OR REPLACE FORCE VIEW "ISR$BIO$COVIB$RUNANA$SMP$DEACT" ("ENTITY", "CODE", "SAMPLENAME", "SAMPLEDESC", "RUNID", "RUNSAMPLESEQUENCENUMBER", "ANALYTEORDER", "ANALYTENAME", "STUDYCODE") AS 
  SELECT 'BIO$COVIB$RUNANA$SMP$DEACT' as ENTITY
     , s.code || '-' || sa.analyteorder
     , s.sampleName
     , s.sampleName ||', '|| sa.name || ', Run: ' || r.runID || ', Seq.No.: ' || s.runSampleSequenceNumber || ', Replicate No.:' || s.replicateNumber SAMPLEDESC
     , r.runID
     , s.runSampleSequenceNumber
     , sa.analyteOrder
     , sa.name analyteName
     , s.studyCode
  FROM ISR$bio$run$worklist s,
       ISR$bio$run r,
       ISR$bio$run$analytes ra,
       ISR$bio$study$analytes sa
 WHERE r.Code = s.runCode
   AND r.Code = ra.runCode
   AND ra.analyteCode = sa.Code
   AND s.studyCode = r.studyCode
   AND s.studyCode = sa.studyCode
   AND s.studyCode = ra.studyCode
   AND ra.code in (
    SELECT sr.runAnalyteCode
      FROM ISR$bio$run$worklist$result$rw sr
     WHERE s.studyCode = sr.studyCode
   )
 ORDER BY s.studyCode, sa.name, r.runID, s.runSampleSequenceNumber
