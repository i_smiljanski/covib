CREATE OR REPLACE FORCE VIEW "ISR$BIO$CUSTOM$STUDY$FIELDS" ("ENTITY", "CODE", "STUDYCODE", "NAME", "VALUE") AS 
  select 'BIO$CUSTOM$STUDY$FIELDS' as ENTITY
       , studycustomdata.studyid || '-' || trim(studycustomdata.studycustomdatadesc) as CODE
       , studycustomdata.studyid as STUDYCODE
       , trim(studycustomdata.studycustomdatadesc) as NAME
       , studycustomdata.studycustomdatavalue as VALUE
    from &&LIMS_USER&.studycustomdata