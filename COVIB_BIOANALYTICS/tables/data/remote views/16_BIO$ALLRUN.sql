CREATE OR REPLACE FORCE VIEW "ISR$BIO$ALLRUN" ("ENTITY", "CODE", "STUDYCODE", "ASSAYCODE", "NAME", "RUNID", "RUNTYPEDESCRIPTION", "RUNSTATUSNUM", "RUNSTATUS", "ACTIVESTATUS", "REORDERWARNING", "RUNSTARTDATE", "EXTRACTIONDATE", "ANALYST", "FILEPREFIX", "SAMPLEFILEPREFIX", "CELLTEMPLATE", "INSERTMETHOD", "USERID", "RECORDTIMESTAMP", "RUNRESULTSIMPORTFILENAME", "INSTGROUPNAME", "OBSERVATIONS", "INSTRUMENTNAME", "INSTRUMENTINTERFACENAME") AS 
  select 'BIO$ALLRUN' as ENTITY
       , AR.studyid || '-' || AR.runid as CODE
       , AR.studyid as STUDYCODE
       , AR.studyid || '-' || AR.runid || '-' || AR.assayid as ASSAYCODE
       , --for SK on BIO$ASSAY only
        AR.rundescription as NAME
       , AR.runid as RUNID
       , CRT.RUNTYPEDESCRIPTION as RUNTYPEDESCRIPTION
       , AR.runstatus as RUNSTATUSNUM
       , CARS.anaregstatusdesc as RUNSTATUS
       , AR.activestatus as ACTIVESTATUS
       , AR.reorderwarning as REORDERWARNING
       , to_char(AR.runstartdate, '&&FORMAT_DATE&') as RUNSTARTDATE
       , to_char(AR.extractiondate, '&&FORMAT_DATE&') as EXTRACTIONDATE
       , AR.analyst as ANALYST
       , AR.fileprefix as FILEPREFIX
       , AR.samplefileprefix as SAMPLEFILEPREFIX
       , AR.celltemplate as CELLTEMPLATE
       , AR.insertmethod as INSERTMETHOD
       , AR.userid as USERID
       , to_char(AR.recordtimestamp, '&&FORMAT_DATE&') as RECORDTIMESTAMP
       , AR.runresultsimportfilename as RUNRESULTSIMPORTFILENAME
       , AR.instgroupname as INSTGROUPNAME
       , AR.observations as OBSERVATIONS
       , CII.InstrumentName as InstrumentName
       , CII_1.InstrumentName as InstrumentInterfaceName
    from &&LIMS_USER&.AnalyticalRun AR,
         &&LIMS_USER&.ConfigRunTypes CRT,
         &&LIMS_USER&.ConfigAnalyteRunStatus CARS,
         &&LIMS_USER&.Assay A,
         &&LIMS_USER&.ConfigInstInterface CII,
         &&LIMS_USER&.ConfigInstInterface CII_1
   where AR.RUNTYPEID                = CRT.RUNTYPEID
     and AR.runstatus                = CARS.runanalyteregressionstatus
     and AR.AssayId                   = A.assayId
     and A.instrumentType            = CII.InstrumentInterfaceId(+)
     and A.instrumentInterfaceFormat = CII_1.InstrumentInterfaceId(+)