CREATE OR REPLACE FORCE VIEW "ISR$BIO$PK" ("ENTITY", "CODE", "STUDYCODE", "ANALYTECODE", "SUBJECTCODE", "TREATMENTCODE", "PKKEYVALUE", "PKKEYINDEX", "ANALYTE", "VISIT", "YEAR", "PHASE", "MONTH", "WEEK", "PERIOD", "STUDYDAY", "TIMEIDENT", "TIMEDESC", "SAMPLETYPEKEY", "SAMPLETYPEID", "SAMPLETYPEABBREVIATION", "SAMPLEKIND", "GROUPDESCRIPTION", "TIMEUNITS", "CONCENTRATIONUNITS", "TIMESTYLE", "PKENGINE", "PKENGINEVERSION") AS 
  select distinct
         'BIO$PK' as ENTITY
       , P.studyid || '-' || P.pkkeyvalue || '-' || D.treatmentkey as CODE
       , P.studyid as STUDYCODE
       , P.studyid || '-' || P.analyte as ANALYTECODE
       , --P.studyid  || '-' ||D.TREATMENTEVENTID            subjectgrouptreatmentcode,
         P.studyid || '-' || P.subjectid as SUBJECTCODE
       , P.studyid || '-' || D.treatmentkey as TREATMENTCODE
       , P.pkkeyvalue as PKKEYVALUE
       , P.pkkeyindex as PKKEYINDEX
       , P.analyte as ANALYTE
       , --D.TREATMENTEVENTID                                treatmenteventid,
         d.visittext as visit
       , d.YEAR as YEAR
       , d.phase as PHASE
       , d.MONTH as MONTH
       , d.week as WEEK
       , d.period as PERIOD
       , d.studyday as STUDYDAY
       , nvl (case when regexp_like(d.visittext, '^([0-9]*)$') then lpad(d.visittext, 4, '0') else d.visittext end, 'X') -- MUIB specific
         || '_'
         || nvl(regexp_replace(to_char(d.YEAR, 'FM0000D99'), trim(to_char(1 / 10, 'D')) || '$'), 'X')
         || '_'
         || nvl(regexp_replace(to_char(d.MONTH, 'FM0000D99'), trim(to_char(1 / 10, 'D')) || '$'), 'X')
         || '_'
         || nvl(regexp_replace(to_char(d.week, 'FM0000D99'), trim(to_char(1 / 10, 'D')) || '$'), 'X')
         || '_'
         || nvl(regexp_replace(to_char(d.studyday, 'FM0000D99'), trim(to_char(1 / 10, 'D')) || '$'), 'X')
           as TIMEIDENT
       , trim(   decode(d.visittext, null, null, ' Visit ' || d.visittext)
              || decode(d.YEAR, null, null, ' Year ' || d.YEAR)
              || decode(d.MONTH, null, null, ' Month ' || d.MONTH)
              || decode(d.week, null, null, ' Week ' || d.week)
              || decode(d.studyday, null, null, ' Day ' || d.studyday))
           as TIMEDESC
       , CS.SAMPLETYPEKEY as SAMPLETYPEKEY
       , CS.SAMPLETYPEID as SAMPLETYPEID
       , CS.SAMPLETYPEABBREVIATION as SAMPLETYPEABBREVIATION
       , CS.SAMPLEKIND as SAMPLEKIND
       , P.groupdescription as GROUPDESCRIPTION
       , P.timeunits as TIMEUNITS
       , CU.CONCENTRATIONUNITS as CONCENTRATIONUNITS
       , P.timestyle as TIMESTYLE
       , P.pkengine as PKENGINE
       , P.pkengineversion as PKENGINEVERSION
    from &&LIMS_USER&.pkkey P
       , &&LIMS_USER&.designevent d
       , &&LIMS_USER&.configsampletypes cs
       , &&LIMS_USER&.concentrationunits cu
   where nvl(P.studyday, -1) = nvl(d.studyday, -1)
     --and nvl(p.period,-1)=nvl(d.period,-1)
     and nvl(p.week, -1) = nvl(d.week, -1)
     and nvl(p.year, -1) = nvl(d.year, -1)
     --and nvl(p.phase,-1)=nvl(d.phase,-1)
     and nvl(p.month, -1) = nvl(d.month, -1)
     and (nvl(p.visittext, 'x') = nvl(d.visittext, 'x') or p.visittext is null) -- MUIB specific
     and P.treatmentkey = d.treatmentkey
     and P.studyid = d.studyid
     and P.SAMPLETYPEID = CS.SAMPLETYPEKEY
     and P.CONCUNITSID = CU.CONCUNITSID