CREATE OR REPLACE FORCE VIEW "ISR$BIO$STUDY$TREATMENT" ("ENTITY", "CODE", "STUDYCODE", "ANALYTECODE", "NAME", "DESCRIPTION", "TITLE", "DOSEAMOUNT", "DOSEUNITSDESCRIPTION", "ROUTEADMINDESCRIPTION", "FORMULATIONDESC", "REGIMENDESCRIPTION", "FASTEDFEDTEXT", "STATUS", "REPLICATES", "VEHICLEDESC", "PLACEBOFLAG", "ISFORMULATION", "TREATMENTDESC", "DRUGID", "PRODUCTNUMBER", "COMMENTMEMO", "FORMULATIONLOT", "PREPDATE", "PREPARATIONDATE", "TEXTVARIABLE1", "TEXTVARIABLE2", "TEXTVARIABLE3", "TEXTVARIABLE4", "TEXTVARIABLE5", "TEXTVARIABLE6", "TEXTVARIABLE7", "TEXTVARIABLE8", "NUMBERVARIABLE1", "NUMBERVARIABLE2", "NUMBERVARIABLE3", "NUMBERVARIABLE4", "NUMBERVARIABLE5", "NUMBERVARIABLE6", "NUMBERVARIABLE7", "NUMBERVARIABLE8", "ANALYTENAME", "ANALYTELOT", "CONCENTRATION", "CONCENTRATIONUNITS", "ANALYTEFLAGPERCENT", "CVFLAGPERCENT", "TREATMENTKEY") AS 
  select 'BIO$STUDY$TREATMENT' as ENTITY
       , dt.studyid || '-' || dt.treatmentkey as CODE
       , dt.studyid as STUDYCODE
       , dt.studyid || '-' || dtc.analyteid as ANALYTECODE
       , dt.treatmentid as NAME
       , dt.treatmentkey as DESCRIPTION
       ,    case
              when round(dt.doseamount, 15) < 1
               and round(dt.doseamount, 15) != 0
              then
                '0'
              else
                null
            end
         || round(dt.doseamount, 15)
         || ' '
         || DOSEUNITSDESCRIPTION
         || ' '
         || cfa.routeadmindescription
         || ' '
         || cfl.formulationdesc
         || ' '
         || cr.regimendescription
           as TITLE
       , round(dt.doseamount, 15) as DOSEAMOUNT
       , du.doseunitsdescription as DOSEUNITSDESCRIPTION
       , cfa.routeadmindescription as ROUTEADMINDESCRIPTION
       , cfl.formulationdesc as FORMULATIONDESC
       , cr.regimendescription as REGIMENDESCRIPTION
       , cff.fastedfedtext as FASTEDFEDTEXT
       , dt.status as STATUS
       , dt.replicates as REPLICATES
       , cv.VEHICLEDESC as VEHICLEDESC
       , dt.placeboflag as PLACEBOFLAG
       , dt.isformulation as ISFORMULATION
       , dt.treatmentdesc as TREATMENTDESC
       , dt.drugid as DRUGID
       , dt.productnumber as PRODUCTNUMBER
       , dt.commentmemo as COMMENTMEMO
       , dt.formulationlot as FORMULATIONLOT
       , to_char(dt.prepdate, '&&FORMAT_DATE&') as PREPDATE
       , to_char(iSR$Format.ToTime(textvariable1, dt.prepdate, 0), '&&FORMAT_DATE&') as PREPARATIONDATE
       , dt.textvariable1 as TEXTVARIABLE1
       , dt.textvariable2 as TEXTVARIABLE2
       , dt.textvariable3 as TEXTVARIABLE3
       , dt.textvariable4 as TEXTVARIABLE4
       , dt.textvariable5 as TEXTVARIABLE5
       , dt.textvariable6 as TEXTVARIABLE6
       , dt.textvariable7 as TEXTVARIABLE7
       , dt.textvariable8 as TEXTVARIABLE8
       , dt.numbervariable1 as NUMBERVARIABLE1
       , dt.numbervariable2 as NUMBERVARIABLE2
       , dt.numbervariable3 as NUMBERVARIABLE3
       , dt.numbervariable4 as NUMBERVARIABLE4
       , dt.numbervariable5 as NUMBERVARIABLE5
       , dt.numbervariable6 as NUMBERVARIABLE6
       , dt.numbervariable7 as NUMBERVARIABLE7
       , dt.numbervariable8 as NUMBERVARIABLE8
       , ga.analytedescription as ANALYTENAME
       , dtc.analytelot as ANALYTELOT
       , dtc.concentration as CONCENTRATION
       , cu.concentrationunits as CONCENTRATIONUNITS
       , dtc.analyteflagpercent as ANALYTEFLAGPERCENT
       , dtc.cvflagpercent as CVFLAGPERCENT
       , dt.treatmentkey as TREATMENTKEY
    from &&LIMS_USER&.DESIGNTREATMENT dt
       , &&LIMS_USER&.DESIGNTREATMENTCOMPOUND dtc
       , &&LIMS_USER&.GLOBALANALYTES ga
       , &&LIMS_USER&.CONCENTRATIONUNITS cu
       , &&LIMS_USER&.DOSEUNITS du
       , &&LIMS_USER&.CONFIGROUTEADMIN cfa
       , &&LIMS_USER&.CONFIGFORMULATION cfl
       , &&LIMS_USER&.CONFIGREGIMEN cr
       , &&LIMS_USER&.CONFIGFASTEDFED cff
       , &&LIMS_USER&.CONFIGVEHICLE cv
   where dt.doseunitsid = du.doseunitsid(+)
     and dt.routeadminid = cfa.routeadminid(+)
     and dt.formulationid = cfl.formulationid(+)
     and dt.regimenid = cr.regimenid(+)
     and dt.fastedfedid = cff.fastedfedid(+)
     and dt.vehicleid = cv.vehicleid(+)
     and dtc.studyid(+) = dt.studyid
     and dtc.treatmentkey(+) = dt.treatmentkey
     and dtc.concentrationunitid = cu.concunitsid(+)
     and ga.globalanalyteid(+) = dtc.analyteid