CREATE OR REPLACE FORCE VIEW "ISR$BIO$STUDY$CALC" ("ENTITY", "CODE", "NAME", "ASSAYSTARTDATE", "ASSAYENDDATE", "ANALYTICALSTARTDATE", "ANALYTICALENDDATE", "FIRSTRECEIVEDDATE", "LASTRECEIVEDDATE") AS 
  select 'BIO$STUDY$CALC' as ENTITY
       , study.studyid as CODE
       , study.studyname as NAME
       , to_char(ISR$BIOANALYTICS$WT$R$LAL.GetAssayStartDate(study.studyid), '&&FORMAT_DATE&') as ASSAYSTARTDATE
       , to_char(ISR$BIOANALYTICS$WT$R$LAL.GetAssayEndDate(study.studyid), '&&FORMAT_DATE&') as ASSAYENDDATE
       , to_char(ISR$BIOANALYTICS$WT$R$LAL.GetAnalyticalStartDate(study.studyid), '&&FORMAT_DATE&') as ANALYTICALSTARTDATE
       , to_char(ISR$BIOANALYTICS$WT$R$LAL.GetAnalyticalEndDate(study.studyid), '&&FORMAT_DATE&') as ANALYTICALENDDATE
       , to_char(ISR$BIOANALYTICS$WT$R$LAL.getFirstReceiveDate(study.studyid), '&&FORMAT_DATE&') as FIRSTRECEIVEDDATE
       , to_char(ISR$BIOANALYTICS$WT$R$LAL.getLastReceiveDate(study.studyid), '&&FORMAT_DATE&') as LASTRECEIVEDDATE
    from &&LIMS_USER&.study
  order by upper(study.studyid)