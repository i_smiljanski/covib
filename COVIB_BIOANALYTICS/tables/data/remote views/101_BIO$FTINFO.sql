CREATE MATERIALIZED VIEW ISR$BIO$FTINFO
BUILD DEFERRED REFRESH FAST ON DEMAND
AS
SELECT
    samp.EXTERNAL_REFERENCE CODE,
    substr ( samp.EXTERNAL_REFERENCE, instr( samp.EXTERNAL_REFERENCE,'STUDYID')+7, instr( samp.EXTERNAL_REFERENCE,'DESIGNSAMPLEID')-8 ) STUDYCODE,
    substr ( samp.EXTERNAL_REFERENCE, instr( samp.EXTERNAL_REFERENCE,'STUDYID')+7, instr( samp.EXTERNAL_REFERENCE,'DESIGNSAMPLEID')-8 ) || '-' ||substr ( samp.EXTERNAL_REFERENCE, instr( samp.EXTERNAL_REFERENCE,'DESIGNSAMPLEID')+14 ) SAMPLECODE,    
    to_char(alq.received_on, '&&FORMAT_DATE&') as RECEIVEDON,
    alqu.u_freeze_thaw_count AS FREEZETHAWCOUNT,
    sdg.name AS SDGNAME,
    to_char(sdg.received_on, '&&FORMAT_DATE&') as SDGRECEIVEDON
FROM
    &&LIMS_USER&.sample samp
LEFT JOIN &&LIMS_USER&.sample_user sampu
    ON samp.sample_id = sampu.sample_id
LEFT JOIN &&LIMS_USER&.aliquot alq
    ON alq.sample_id = samp.sample_id
LEFT JOIN &&LIMS_USER&.aliquot_user alqu
    ON alq.aliquot_id = alqu.aliquot_id
LEFT OUTER JOIN &&LIMS_USER&.sdg sdg
    ON alqu.U_SDG_ASSOCIATION = sdg.name
LEFT JOIN &&LIMS_USER&.sdg_user sdgu
    ON sdg.sdg_id = sdgu.sdg_id
WHERE
    samp.EXTERNAL_REFERENCE is not null
and alq.ALIQUOT_TEMPLATE_ID = 21