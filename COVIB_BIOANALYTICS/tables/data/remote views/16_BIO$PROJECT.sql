CREATE OR REPLACE FORCE VIEW "ISR$BIO$PROJECT" ("ENTITY", "CODE", "NAME", "PROJECTNAME", "DESCRIPTION", "TITLE") AS 
  select 'BIO$PROJECT' as ENTITY
       , PROJECT.projectid as CODE
       , PROJECT.projectidtext as NAME
       , PROJECT.projectname as PROJECTNAME
       , PROJECT.projectidtext || ' ' || PROJECT.projectname as DESCRIPTION
       , PROJECT.projecttitle as TITLE
    from &&LIMS_USER&.PROJECT
  order by PROJECT.projectidtext