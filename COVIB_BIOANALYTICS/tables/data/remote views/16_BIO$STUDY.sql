CREATE OR REPLACE FORCE VIEW "ISR$BIO$STUDY" ("ENTITY", "CODE", "PROJECTCODE", "NAME", "TITLE", "DESCRIPTION", "TYPE", "SPECIES", "NOTEBOOK", "PAGEID", "STUDYDIRECTOR", "DEPUTYSTUDYDIRECTOR", "ANALYTICALCOORDINATOR", "PKDIRECTOR", "DEPARTMENT", "ISGLP", "BARCODESTUDYID", "USESGENDER", "HASUNKNOWNRUNS", "HASVALIDATIONRUNS") AS 
  select 'BIO$STUDY' as ENTITY
       , study.studyid as CODE
       , study.projectid as PROJECTCODE
       , study.studyname as NAME
       , study.studytitle as TITLE
       , study.studydescription as DESCRIPTION
       , configstudytypes.studytype as TYPE
       , configspecies.species as SPECIES
       , study.notebook as NOTEBOOK
       , study.pagenumber as PAGEID
       , study.studydirector as STUDYDIRECTOR
       , study.deputystudydirector as DEPUTYSTUDYDIRECTOR
       , study.analyticalcoordinator as ANALYTICALCOORDINATOR
       , study.pkdirector as PKDIRECTOR
       , study.department as DEPARTMENT
       , study.glp as ISGLP
       , study.BarcodeStudyId as BARCODESTUDYID
       , ISR$BIOANALYTICS$WT$R$LAL.StudyUsesGender(study.studyid) as USESGENDER
       , studyruntypes.unknownruns as HASUNKNOWNRUNS
       , studyruntypes.validationruns as HASVALIDATIONRUNS
    from &&LIMS_USER&.STUDY
       , &&LIMS_USER&.CONFIGSPECIES
       , &&LIMS_USER&.CONFIGSTUDYTYPES
       , (select studyid
               , case when sum(unknownruns) > 0 then 'T' else 'F' end UNKNOWNRUNS
               , case when sum(validationruns) > 0 then 'T' else 'F' end VALIDATIONRUNS
            from (select a.studyid
                       , case when CR.runtypedescription in ('PSAE', 'MANDATORY REPEATS', 'UNKNOWNS') then 1 else 0 end
                           UNKNOWNRUNS
                       , case when CR.runtypedescription = 'VALIDATION' then 1 else 0 end VALIDATIONRUNS
                    from &&LIMS_USER&.ANALYTICALRUN A
                       , &&LIMS_USER&.CONFIGRUNTYPES cr
                       -- because of MUIB-179. More studies than expected may be shown in the wizard
                       /*, (select distinct studyid, runid
                            from &&LIMS_USER&.ANALYTICALRUNANALYTES
                           where runanalyteregressionstatus in (select value
                                                                  from ISR$PARAMETER$VALUES
                                                                 where entity = 'BIO$RUNANALYTEREGRESSIONSTATUS')) rai*/
                   where A.RUNTYPEID = CR.RUNTYPEID
                     -- removed because of MUIB-179.
                     /*and a.studyid = rai.studyid
                     and a.runid = rai.runid*/)
          group by studyid) studyruntypes
   where study.speciesid = configspecies.speciesid(+)
     and study.studytypeid = configstudytypes.studytypeid(+)
     and study.studyid = studyruntypes.studyid
  order by upper(study.studyname)