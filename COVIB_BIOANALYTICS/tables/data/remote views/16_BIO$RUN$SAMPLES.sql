CREATE OR REPLACE FORCE VIEW "ISR$BIO$RUN$SAMPLES" ("ENTITY", "CODE", "STUDYCODE", "SAMPLECODE", "RUNCODE", "RUNKNOWNCODE", "SAMPLETYPE", "RUNSAMPLESEQUENCENUMBER", "RUNSAMPLEORDERNUMBER", "SAVEDORDERNUMBER", "OLDNAME", "NAME", "ASSAYLEVEL", "SAMPLESTUDYID", "RUNSAMPLEKIND", "REPLICATENUMBER", "STABILITYTYPEID", "SAMPLENAME", "INJECTIONVOLUME", "ALIQUOTFACTOR", "DILUTIONFACTOR", "ASSAYDATETIME", "SAMPLEACCEPTEDTIMESTAMP", "TITERPOSITION", "SAMPLECOMMENT", "CHARGEABLE", "PREPDATE", "METHOD", "CYCLES", "TEMPERATURE", "HOURS", "LONGTERMTIME", "LONGTERMUNITS", "STABILITYTYPE", "SAMPLESUBTYPE", "SPIKEDANALYTE", "SPIKEDANALYTEUNITS", "CONCENTRATION", "RACKNUMBER", "KNOWNNAME", "ISDEACTIVATED", "ANALYSISTYPE", "RUNANALYTECODE", "ANALYTEORDER") AS 
  select 'BIO$RUN$SAMPLES' as ENTITY
       , A.studyid || '-' || A.runid || '-' || A.runsamplesequencenumber || '-' || aa.analyteid as CODE
       , A.studyid as STUDYCODE
       , A.studyid || '-' || A.designsampleid as SAMPLECODE
       , A.studyid || '-' || A.runid as RUNCODE
       ,    A.studyid || '-' || A.runid || '-' || A.runsamplekind || '-'
         || (select replace(ID, chr(10), null)
               from &&LIMS_USER&.assayreps
              where assayid = aa.AssayId
                and levelnumber = a.AssayLevel
                and knowntype = A.runsamplekind)
         || '-' || aa.analyteid as RUNKNOWNCODE
       , decode(designsampleid, null, 'known', 'unknown') as SAMPLETYPE
       , A.runsamplesequencenumber as RUNSAMPLESEQUENCENUMBER
       , A.runsampleordernumber as RUNSAMPLEORDERNUMBER
       , A.savedordernumber as SAVEDORDERNUMBER
       , A.exportsamplename as OLDNAME
       , ISR$BIOANALYTICS$WT$R$LAL.Buildsamplename(a.STUDYID
                                                 , a.SampleStudyID
                                                 , a.RunID
                                                 , a.DesignSampleID
                                                 , a.AssayLevel
                                                 , a.ReplicateNumber
                                                 , a.RunSampleKind
                                                 , null)
           as NAME
       , A.assaylevel as ASSAYLEVEL
       , A.samplestudyid as SAMPLESTUDYID
       , CASE
             WHEN A.runsamplekind IN ('QC', 'STABILITY') THEN 'KNOWN'
             ELSE A.runsamplekind
          END
           as RUNSAMPLEKIND -- MUIB specific
       , A.replicatenumber as REPLICATENUMBER
       , A.stabilitytypeid as STABILITYTYPEID
       , A.samplename as SAMPLENAME
       , A.injectionvolume as INJECTIONVOLUME
       , A.aliquotfactor as ALIQUOTFACTOR
       , case when A.aliquotfactor = 0 then 1 else round(1 / nvl(A.aliquotfactor, 1), 8) end as DILUTIONFACTOR
       , to_char(A.assaydatetime, '&&FORMAT_DATE&') as ASSAYDATETIME
       , to_char(A.sampleacceptedtimestamp, '&&FORMAT_DATE&') as SAMPLEACCEPTEDTIMESTAMP
       , A.titerposition as TITERPOSITION
       , A.samplecomment as SAMPLECOMMENT
       , A.chargeable as CHARGEABLE
       , to_char(A.prepdate, '&&FORMAT_DATE&') as PREPDATE
       , A.METHOD as METHOD
       , a.cycles as CYCLES
       , A.temperature as TEMPERATURE
       , A.hours as HOURS
       , case
           when longtermunits = 'M' then round(longtermtime / 30)
           when longtermunits = 'W' then round(longtermtime / 7)
           else longtermtime
         end
           as LONGTERMTIME -- new for MUIB, need to check with HR
       , A.longtermunits as LONGTERMUNITS -- cycles, temperature, hours, longtermtime, longtermunits áre pulled via a method in the R-LAL from assayreps in MUIB, didn't copy that over yet, instead calculations could be used to achieve this
       , A.stabilitytype as STABILITYTYPE
       , A.samplesubtype as SAMPLESUBTYPE
       , A.spikedanalyte as SPIKEDANALYTE
       , A.spikedanalyteunits as SPIKEDANALYTEUNITS
       , A.concentration as CONCENTRATION
       , A.racknumber as RACKNUMBER
       , (select replace(ID, chr(10), null)
            from &&LIMS_USER&.assayreps
           where assayid = aa.AssayId
             and levelnumber = a.AssayLevel
             and knowntype = A.runsamplekind)
           as KNOWNNAME
       , ISR$BIOANALYTICS$WT$R$LAL.GetSampleDeactivated(A.studyid, A.runid, A.runsamplesequencenumber, aa.analyteindex)
           as ISDEACTIVATED
       --, null -- for Watson release 7.3
       , a.analysistype as ANALYSISTYPE -- for Watson release 7.4
       , aa.studyid || '-' || ara.runid || '-' || aa.analyteid as RUNANALYTECODE
       , aa.analyteindex as ANALYTEORDER
    from &&LIMS_USER&.analyticalrunsample A
       , &&LIMS_USER&.assay ass
       , &&LIMS_USER&.assayanalytes aa
       , &&LIMS_USER&.analyticalrunanalytes ara
   where A.runid = ass.runid
     and ass.assayid = aa.assayid
     and A.studyid = aa.studyid
     and A.studyid = ara.studyid
     and A.runid = ara.runid
     and aa.analyteindex = ara.analyteindex