<?xml version="1.0" encoding="UTF-8"?>
<!--
ISR$BIO$COVIB$RAW$DATA.XSL
==========================================================================

Description:
Creates the Raw Data Table for Covance Reports
========================================================================== -->
<xsl:stylesheet version="3.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform" xmlns:xs="http://www.w3.org/2001/XMLSchema" xmlns:fn="http://www.w3.org/2005/xpath-functions" xmlns:utd="http://www.uptodata.com">
	<xsl:strip-space elements="*"/>
	<xsl:output method="html" html-version="5.0" encoding="UTF-8" indent="yes" media-type="text/html"/>
	<!--
==========================================================================
Includes
==========================================================================-->
	<xsl:include href="ISR$CSS.XSL"/>
	<xsl:include href="ISR$UTIL.XSL"/>
	<xsl:include href="ISR$CAPTION.XSL"/>
	<xsl:include href="ISR$WT$TABLE.XSL"/>
	<xsl:include href="ISR$WT$PAGE.XSL"/>
	<!--
==========================================================================
Variables
==========================================================================-->
	<xsl:variable name="experiment">S_SOURCERAWDATA</xsl:variable>
	<xsl:variable name="hash">SRCR</xsl:variable>
	<!--=====================================================================-->
	<xsl:variable name="attributes" select="/study/attributes/*"/>
	<xsl:variable name="not_determined" select="$attributes/not_performed_identifier"/>
	<xsl:variable name="not_performed" select="$attributes/not_performed_identifier"/>
	<xsl:variable name="not_detected" select="$attributes/not_detected_identifier"/>
	<xsl:variable name="show_instrumentid_runsummary" select="$attributes/show_instrumentid_runsummary"/>
	<xsl:variable name="runs" select="/study/bioanalytic/runs"/>
	<xsl:variable name="study" select="/study/bioanalytic/studies/study"/>
	<xsl:variable name="assays" select="/study/bioanalytic/studies/study/assays/assay"/>
	<xsl:variable name="run-info" select="$study/RunInfo/run"/>
	<xsl:variable name="criteria" select="/study/protocol/criteria"/>
	<xsl:variable name="format" select="/study/formats/experiment[@name=$experiment]"/>
	<xsl:variable name="table-caption" select="utd:formats($experiment,'caption')"/>
	<xsl:variable name="table-type" select="utd:formats($experiment,('var'))[upper-case(@name)='TABLE_TYPE']"/>
	<xsl:variable name="column_entity" select="$attributes/exp_col_entity"/>
	<xsl:variable name="selected-columns" select="$criteria[@entity=$column_entity]/value[masterkey=$experiment]"/>
	<xsl:variable name="hide_cols" select="( for $s in utd:formats($experiment,'var')[@type='OPTCOL'] return if ($attributes/*[name() = $s] != 'T') then $s/@key else () ), if ($show_instrumentid_runsummary and $show_instrumentid_runsummary='F') then 'INSTRUMENTGROUP' else ()"/>
	<xsl:variable name="columns" as="node()*">
		<xsl:choose>
			<xsl:when test="$criteria[@entity='BIO$EXPERIMENT$COLS']">
				<xsl:for-each select="$selected-columns/key">
					<xsl:copy-of select="(utd:formats($experiment,'cols'))[@name=current()][not(@name=$hide_cols)]"/>
				</xsl:for-each>
			</xsl:when>
			<xsl:otherwise>
				<xsl:copy-of select="(utd:formats($experiment,'cols'))[not(@name=$hide_cols)]"/>
			</xsl:otherwise>
		</xsl:choose>
	</xsl:variable>
	<xsl:variable name="column_styles" select="utd:formats($experiment,('cols','style'))"/>
	<xsl:variable name="column_width" select="utd:formats($experiment,('cols','width'))"/>
	<xsl:variable name="caption-type" select="utd:formats($experiment,('var'))[upper-case(@name)='CAPTION_TYPE']"/>
	<xsl:variable name="main-ref" select="utd:formats($experiment,('var'))[upper-case(@name)='MAINREF']"/>
	<xsl:variable name="sort-by">runid</xsl:variable>
	<xsl:variable name="sort-type">number</xsl:variable>
	<xsl:variable name="sort-order">ascending</xsl:variable>
	<xsl:variable name="maxlines" select="$attributes/*[name() = concat( 'max_lines_', lower-case($experiment) ) ]"/>
	<xsl:variable name="maxcols" select="$attributes/*[name() = concat( 'max_cols_', lower-case($experiment) ) ]"/>
	<xsl:variable name="excel-filename" select="utd:formats($experiment,'var')[@name='EXCEL_FILENAME']"/>
	<xsl:variable name="section-title" select="utd:formats($experiment,'var')[@name='SECTION_TITLE']"/>
	<!--
==========================================================================
Functions
========================================================================== -->
	<xsl:function name="utd:excelAttributes" as="node()*">
		<xsl:param name="name"/>
		<xsl:variable name="style" select="$column_styles[@name=$name]"/>
		<xsl:if test="$style">
			<xsl:variable name="excel-type" select="utd:styleValue('excel-type', $style)"/>
			<xsl:if test="$excel-type">
				<xsl:attribute name="excel-type" select="$excel-type"/>
			</xsl:if>
			<xsl:variable name="excel-format" select="utd:styleValue('excel-format', $style)"/>
			<xsl:if test="$excel-format">
				<xsl:attribute name="excel-format" select="$excel-format"/>
			</xsl:if>
		</xsl:if>
	</xsl:function>
	<xsl:function name="utd:nvl">
		<xsl:param name="value"/>
		<xsl:param name="alternative"/>
		<xsl:choose>
			<xsl:when test="$value != ''">
				<xsl:value-of select="$value"/>
			</xsl:when>
			<xsl:otherwise>
				<xsl:value-of select="$alternative"/>
			</xsl:otherwise>
		</xsl:choose>
	</xsl:function>
	<!--
==========================================================================
Main template
========================================================================== -->
	<xsl:template match="/">
		<xsl:variable name="isr-sections">
			<isr-sections emptytable="{false()}" experiment="{$experiment}">
				<xsl:attribute name="title">ISR$BIO$COVIB$RAW$DATA.XSL - S_SOURCERAWDATA</xsl:attribute>
				<xsl:if test="$table-type">
					<xsl:attribute name="type" select="$table-type"/>
				</xsl:if>
				<isr-section>
					<xsl:attribute name="experiment" select="$experiment"/>
					<xsl:variable name="section-params" as="node()*">
						<study>
							<xsl:value-of select="$study/name"/>
						</study>
					</xsl:variable>
					<xsl:if test="$section-title">
						<xsl:attribute name="title" select="utd:params($section-title,$section-params)"/>
					</xsl:if>
					<xsl:if test="$excel-filename">
						<xsl:attribute name="filename" select="utd:params($excel-filename,$section-params)"/>
					</xsl:if>
					<xsl:if test="$table-type">
						<xsl:attribute name="type" select="$table-type"/>
					</xsl:if>
					<!--xsl:for-each select="distinct-values($runs/raw-data/run/analyteid)">
					<xsl:sort select="." data-type="number"/>
					<xsl:variable name="analyteid" select="."/>
					<xsl:variable name="hashstring" select="concat( $study/@code, '_', current() )"/-->
					<xsl:variable name="hashstring" select="$study/@code"/>
					<xsl:variable name="cap" select="if ($maxcols='' or empty($maxcols)) then 100 else $maxcols - 1 "/>
					<xsl:variable name="tables" select="ceiling(count($columns[@name != 'RUNID']) div $cap)"/>
					<xsl:for-each select="1 to xs:integer($tables)">
						<xsl:variable name="number" select="current()"/>
						<xsl:call-template name="createTable">
							<xsl:with-param name="hash" select="concat( $hash, '_', utd:hashcalc( $hashstring, 18 ) )"/>
							<!--xsl:with-param name="runs" select="$runs/raw-data/run[analyteid=$analyteid]"/-->
							<!--xsl:with-param name="analyteid" select="$analyteid"/-->
							<xsl:with-param name="number" select="position()"/>
							<xsl:with-param name="columns_sel" select="$columns[@name = 'RUNID'] | $columns[@name != 'RUNID'][position() gt (($number*$cap)-$cap) and position() le ($number*$cap)]"/>
						</xsl:call-template>
					</xsl:for-each>
					<!--/xsl:for-each-->
				</isr-section>
			</isr-sections>
		</xsl:variable>
		<!--xsl:copy-of select="$isr-section"/-->
		<xsl:apply-templates select="$isr-sections/isr-sections"/>
	</xsl:template>
	<!--
==========================================================================
createTable
========================================================================== -->
	<xsl:template name="createTable">
		<xsl:param name="hash"/>
		<!--xsl:param name="runs"/>
		<xsl:param name="analyteid"/-->
		<xsl:param name="number"/>
		<xsl:param name="columns_sel"/>
		<!-- PARAMETERS -->
		<xsl:variable name="isr-parameters">
			<isr-parameters>
				<!--analyte-name>
					<xsl:value-of select="$runs[1]/analytedesc"/>
				</analyte-name-->
				<species>
					<xsl:value-of select="$study/assays/assay[1]/species"/>
				</species>
				<matrix>
					<xsl:value-of select="$study/assays/assay[1]/sampletypeid"/>
				</matrix>
			</isr-parameters>
		</xsl:variable>
		<!-- CAPTION -->
		<xsl:variable name="isr-caption">
			<isr-caption>
				<xsl:attribute name="type">listed</xsl:attribute>
				<xsl:if test="$caption-type">
					<xsl:attribute name="type"><xsl:value-of select="$caption-type"/></xsl:attribute>
				</xsl:if>
				<xsl:attribute name="hash" select="$hash"/>
				<xsl:attribute name="ref" select="$hash"/>
				<xsl:if test="$main-ref">
					<xsl:attribute name="mainref"><xsl:value-of select="$main-ref"/></xsl:attribute>
				</xsl:if>
				<xsl:attribute name="number" select="$number"/>
				<title>
					<xsl:value-of select="$table-caption"/>
				</title>
				<parameters/>
			</isr-caption>
		</xsl:variable>
		<!-- TABLE -->
		<xsl:variable name="isr-table">
			<isr-table type="standard">
				<xsl:if test="not(empty($maxlines))">
					<xsl:attribute name="maxlines" select="$maxlines"/>
				</xsl:if>
				<headers>
					<header calcwidth="true">
						<xsl:for-each select="$columns_sel">
							<head class="borderTopBottom">
								<xsl:if test="$column_width[@name=current()/@name]">
									<xsl:attribute name="width" select="$column_width[@name=current()/@name]"/>
								</xsl:if>
								<xsl:value-of select="."/>
							</head>
						</xsl:for-each>
					</header>
				</headers>
				<rows>
					<xsl:for-each select="distinct-values($runs/raw-data/run/runid)">
						<xsl:sort select="($runs/raw-data/run[runid=current()])[1]/*[name() = $sort-by]" data-type="{$sort-type}" order="{$sort-order}"/>
						<xsl:variable name="xpos" select="position()"/>
						<xsl:variable name="xlast" select="last()"/>
						<xsl:variable name="run" select="$runs/raw-data/run[runid=current()]"/>
						<xsl:variable name="params" as="node()*">
							<xsl:call-template name="parameters">
								<xsl:with-param name="run" select="$run"/>
								<!--xsl:with-param name="analyteid" select="$analyteid"/-->
							</xsl:call-template>
						</xsl:variable>
						<row>
							<xsl:for-each select="$columns_sel">
								<cell>
									<xsl:for-each select="$params[@name=current()/@name]/@*[not( name() = ('name') )]">
										<xsl:attribute name="{ current()/name() }" select="current()"/>
									</xsl:for-each>
									<xsl:if test="$xpos = $xlast">
										<xsl:attribute name="class" select=" 'borderBottom' "/>
									</xsl:if>
									<xsl:copy-of select="utd:excelAttributes(current()/@name)"/>
									<xsl:variable name="value" select="$params[@name=current()/@name]"/>
									<xsl:choose>
										<xsl:when test="$value = '' or empty($value)">
											<xsl:value-of select=" '-' "/>
										</xsl:when>
										<xsl:otherwise>
											<xsl:value-of select="$value"/>
										</xsl:otherwise>
									</xsl:choose>
								</cell>
							</xsl:for-each>
						</row>
					</xsl:for-each>
				</rows>
			</isr-table>
		</xsl:variable>
		<!-- PAGE -->
		<xsl:variable name="isr-page">
			<isr-page>
				<xsl:attribute name="createCap" select="true()"/>
				<xsl:attribute name="number" select="$number"/>
				<xsl:attribute name="hash" select="$hash"/>
				<xsl:copy-of select="$isr-parameters"/>
				<xsl:copy-of select="$isr-caption"/>
				<xsl:copy-of select="$isr-table"/>
			</isr-page>
		</xsl:variable>
		<xsl:copy-of select="$isr-page"/>
	</xsl:template>
	<!--
==========================================================================
parameters
==========================================================================-->
	<xsl:template name="parameters">
		<xsl:param name="run"/>
		<!--xsl:param name="analyteid"/-->
		<xsl:variable name="study-run" select="$runs/run[runid=$run[1]/runid]"/>
		<xsl:variable name="all-run" select="$runs/allruns/run[runid=$run[1]/runid]"/>
		<!--for grouping: <param name="xxx" group="true" class="borderBottom"> -->
		<param name="RUNID" group="true">
			<xsl:value-of select="$run[1]/runid"/>
		</param>
		<param name="NAME">
			<xsl:value-of select="$run[1]/name"/>
		</param>
		<param name="ASSAY_DESCRIPTION">
			<xsl:value-of select="$run[1]/assaydesc"/>
		</param>
		<param name="RUNTYPE">
			<xsl:value-of select="$run[1]/runtype"/>
		</param>
		<param name="EXTRACTIONDATE">
			<xsl:choose>
				<xsl:when test="$run[1]/extractiondate and $run[1]/extractiondate != '' ">
					<xsl:value-of select="$run[1]/extractiondate"/>
				</xsl:when>
				<xsl:otherwise>
					<xsl:text>N/A</xsl:text>
				</xsl:otherwise>
			</xsl:choose>
		</param>
		<param name="FIRSTINJECT">
			<xsl:choose>
				<xsl:when test="$run[1]/firstinject and $run[1]/firstinject != '' ">
					<xsl:value-of select="$run[1]/firstinject"/>
				</xsl:when>
				<xsl:otherwise>
					<xsl:text>N/A</xsl:text>
				</xsl:otherwise>
			</xsl:choose>
		</param>
		<param name="LASTINJECT">
			<xsl:choose>
				<xsl:when test="$run[1]/lastinject and $run[1]/lastinject != '' ">
					<xsl:value-of select="$run[1]/lastinject"/>
				</xsl:when>
				<xsl:otherwise>
					<xsl:text>N/A</xsl:text>
				</xsl:otherwise>
			</xsl:choose>
		</param>
		<param name="STARTDATE">
			<xsl:value-of select="$run[1]/startdate"/>
		</param>
		<param name="NOTEBOOK">
			<xsl:value-of select="$run[1]/notebook"/>
		</param>
		<param name="PAGENUMBER">
			<xsl:value-of select="$run[1]/pagenumber"/>
		</param>
		<param name="ANALYTE">
			<xsl:value-of select="$run/analytedesc" separator=" / "/>
		</param>
		<param name="REGSTATUS">
			<xsl:value-of select="$run/regstatus" separator=" / "/>
		</param>
		<param name="REASON">
			<xsl:value-of select="$run/(if(reason!='') then reason else $not_detected)" separator=" / "/>
		</param>
		<param name="LLOQ">
			<xsl:value-of select="$study-run/knowns/known/analytes/analyte[analyteid=$run/analyteid]/lloqconv"/>
		</param>
		<param name="ULOQ">
			<xsl:value-of select="$study-run/knowns/known/analytes/analyte[analyteid=$run/analyteid]/uloqconv"/>
		</param>
		<param name="REGRESSIONTYPE">
			<xsl:value-of select="$study-run/analytes/analyte[analyteid=$run/analyteid]/(if (regressiontext!='') then regressiontext else $not_detected)" separator=" / "/>
		</param>
		<param name="WEIGHTING">
			<xsl:value-of select="$study-run/analytes/analyte[analyteid=$run/analyteid]/weightingfactor" separator=", "/>
		</param>
		<param name="INSTRUMENT">
			<xsl:value-of select="$all-run/instruments" separator=", "/>
		</param>
		<param name="OBSERVATIONS">
			<xsl:value-of select="$all-run/observations" separator=", "/>
		</param>
		<param name="ANALYST">
			<xsl:value-of select="$all-run/analyst" separator=", "/>
		</param>
		<param name="ACTIVESTATUS">
			<xsl:value-of select="$all-run/activestatus" separator=", "/>
		</param>
		<param name="CELLTEMPLATE">
			<xsl:value-of select="$all-run/celltemplate" separator=", "/>
		</param>
		<param name="INSERTMETHOD">
			<xsl:value-of select="$all-run/insertmethod" separator=", "/>
		</param>
		<param name="RUNRESULTSIMPORTFILENAME">
			<xsl:value-of select="$all-run/runresultsimportfilename" separator=", "/>
		</param>
		<param name="INSTRUMENTGROUP">
			<xsl:value-of select="$all-run/instgroupname" separator=", "/>
		</param>
		<param name="FULL_ASSAY_DESCRIPTION">
			<xsl:value-of select="$assays[@id=$all-run/assayid]/description" separator=", "/>
		</param>
		<param name="ASSAY_COMMENTS">
			<xsl:value-of select="$assays[@id=$all-run/assayid]/assaycomments" separator=", "/>
		</param>
		<param name="ANARUNIMPORTFILENAMEFULL">
			<xsl:value-of select="$run[1]/anarunimportfilename" separator=" / "/>
		</param>
		<param name="ANARUNIMPORTFILENAME">
			<xsl:value-of select="$run[1]/tokenize(tokenize(anarunimportfilename,'\\')[last()],'\.')[1]" separator=" / "/>
		</param>
		<param name="MATRIX">
			<xsl:value-of select="$assays[@id=$all-run/assayid]/sampletypeid" separator=", "/>
		</param>
		<param name="VALPARAMSTATUS">
			<xsl:value-of select="$run/valparamstatus" separator=", "/>
		</param>
		<param name="COMMENTS">
			<xsl:value-of select="$run/comments" separator=", "/>
		</param>
		<param name="SUBJECTS">
			<xsl:value-of select="$run-info[@runno=$all-run/runid]/subjects" separator=", "/>
		</param>
		<param name="ASSAY_TYPE">
			<xsl:value-of select="distinct-values($run-info[@runno=$all-run/runid]/samplesubtype[.!=''])" separator=", "/>
		</param>
	</xsl:template>
</xsl:stylesheet>
