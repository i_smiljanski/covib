  SELECT XMLELEMENT("experiment", XMLATTRIBUTES('&&Experiment&' AS "name" ,'knownsanova' AS "type"),
  (SELECT Xmlagg (XMLELEMENT("calculation", 
      XMLELEMENT("target", 
        XMLELEMENT("assayanalyteid", assayanalyteid), 
        XMLELEMENT("name", name)),      
      XMLELEMENT("result",       
        XMLELEMENT("n", ncount),
        XMLELEMENT("mean", &&ConcFormatFunc&(&&ConcRepresentationFunc&(mean,&&ConcFigures&),&&ConcFigures&)),      
        XMLELEMENT("sd", &&ConcFormatFunc&(&&ConcRepresentationFunc&(overall_sd,&&ConcSDFigures&),&&ConcSDFigures&)),     
        XMLELEMENT("intra_batch_cv", FormatRounded(round(intra_batch_cv,&&DecPlPercCV&),&&DecPlPercCV&)),  
        XMLELEMENT("overall_cv", FormatRounded(round(overall_cv,&&DecPlPercCV&),&&DecPlPercCV&)),          
        XMLELEMENT("mean_bias_RE", FormatRounded(round(mean_bias_RE,&&DecPlPercBias&),&&DecPlPercBias&)),
        XMLELEMENT("totalerror", 
                   case when '&&FinalAccuracy&'='T' 
                   then FormatRounded(round(abs(mean_bias_RE)+abs(overall_cv),greatest(&&DecPlPercBias&,&&DecPlPercCV&)),greatest(&&DecPlPercBias&,&&DecPlPercCV&))
                   else FormatRounded(abs(round(mean_bias_RE,&&DecPlPercBias&))+abs(round(overall_cv,&&DecPlPercCV&)),greatest(&&DecPlPercBias&,&&DecPlPercCV&))
                   end ),
        XMLELEMENT("MSb", FormatRounded(Round(MSb,3),3)),
        XMLELEMENT("MSw", FormatRounded(Round(MSw,3),3)),
        XMLELEMENT("MSt", FormatRounded(Round(MSt,3),3))
        )
    ))
    FROM
    (select ans.assayanalyteid, ans.knownconc, ans.name, ans.ncount, ans.mean,
    sqrt((case when MSb > MSw 
       then (case when MSb > MSw then power(sqrt((irs.pcount-1)/(ans.ncount-irs.incount)*(MSb-MSw)),2) else 0 end)
       else 0 end) + power((case when MSb > MSw then sqrt(MSw) else sqrt(MSt) end), 2 )) overall_sd,
    (case when MSb > MSw then sqrt(MSw) else sqrt(MSt) end)/ans.knownconc*100 intra_batch_cv,
    (sqrt((case when MSb > MSw 
       then (case when MSb > MSw then power(sqrt((irs.pcount-1)/(ans.ncount-irs.incount)*(MSb-MSw)),2) else 0 end)
       else 0 end) + power((case when MSb > MSw then sqrt(MSw) else sqrt(MSt) end), 2 ))/ans.knownconc*100) overall_cv,
    (ans.mean-ans.knownconc)/ans.knownconc*100 mean_bias_RE, MSb, MSw, MSt
    from
    (select assayanalyteid, knownconc, name, count(conc) ncount,
    STATS_ONE_WAY_ANOVA( runid, conc, 'MEAN_SQUARES_WITHIN') MSw,
    STATS_ONE_WAY_ANOVA( runid, conc, 'MEAN_SQUARES_BETWEEN') MSb,
    Variance(conc) MSt,
    case when '&&FinalAccuracy&'='T' then AVG(conc) 
         else &&ConcRepresentationFunc&(AVG(conc),&&ConcFigures&) 
    end mean
    from
    (SELECT K.ID knownID, s.ID sampleID, sr.ID sampleresultID, K.NAMEPREFIX name, s.runid, 
        case when '&&FinalAccuracy&'='T' then sr.knownconcentrationconv  
             else &&ConcRepresentationFunc&(sr.knownconcentrationconv,&&ConcFigures&)
        end knownconc, 
        sr.assayanalyteid, r.runtypedescription, s.replicatenumber, k.flagpercent,
        case when '&&FinalAccuracy&'='T' then sr.concentrationconv
             else &&ConcRepresentationFunc&(sr.concentrationconv,&&ConcFigures&) 
        end conc
      FROM &&TempTabPrefix&wt$assay$ana$known K,
           &&TempTabPrefix&wt$run r,
           &&TempTabPrefix&wt$run$analytes ra,
           &&TempTabPrefix&wt$run$samples s,
           &&TempTabPrefix&wt$run$sample$results sr,
           isr$crit kwz,
           table(&&LALPackage&.GetRunStates) rs
      WHERE K.ID=s.knownID
        AND R.ID = s.runid AND R.ID = sr.runid
        AND r.studyassayid =s.studyassayid AND s.studyassayid = sr.studyassayid      
        AND r.RuntypeDescription in (&&RunTypes&)
        AND s.ID=sr.runsampleID        
        AND s.sampletype='known'
        and k.knowntype = s.runsamplekind
        and s.isdeactivated = 'F'
        and kwz.masterkey = '&&ExpMasterkey&'
        AND ra.assayanalyteid = sr.assayanalyteid
        AND ra.runid = s.runid  
        AND ra.studyID = '&&StudyID&'      
        AND K.studyid = '&&StudyID&' AND s.studyid = '&&StudyID&' 
        AND sr.studyID = '&&StudyID&' AND r.studyid = '&&StudyID&'
        and kwz.key=K.NAMEPREFIX
        and kwz.entity = '&&KnownEntity&'
        AND rs.column_value=r.runstatusnum
        AND sr.knownconcentrationconv != 0
        and repid=&&RepID& &&AddCondition&) 
      group by assayanalyteid, knownconc, name) ans,
    (select name, assayanalyteid, 
     sum(rcount) n, sum(rpw) qs, count(distinct decode(rcount,0,null,runid)) pcount,
     sum(rpw) / sum(rcount) incount
     from
     (
     select name, runid, assayanalyteid, 
     count(conc) rcount, power(count(conc),2) rpw  
     from
     (SELECT K.ID knownID, K.NAMEPREFIX name, s.runid,         
        case when '&&FinalAccuracy&'='T' then sr.knownconcentrationconv 
             else &&ConcRepresentationFunc&(sr.knownconcentrationconv,&&ConcFigures&) 
        end knownconc,         
        sr.assayanalyteid, s.replicatenumber, k.flagpercent,
        case when '&&FinalAccuracy&'='T' then sr.concentrationconv
             else &&ConcRepresentationFunc&(sr.concentrationconv,&&ConcFigures&) 
        end conc                   
      FROM &&TempTabPrefix&wt$assay$ana$known K,
           &&TempTabPrefix&wt$run r,
           &&TempTabPrefix&wt$run$analytes ra,
           &&TempTabPrefix&wt$run$samples s,
           &&TempTabPrefix&wt$run$sample$results sr,
           isr$crit kwz,
           table(&&LALPackage&.GetRunStates) rs
      WHERE K.ID=s.knownID
        AND R.ID = s.runid AND R.ID = sr.runid
        AND r.studyassayid =s.studyassayid AND s.studyassayid = sr.studyassayid      
        AND r.RuntypeDescription in (&&RunTypes&)
        AND s.ID=sr.runsampleID        
        AND s.sampletype='known'
        and k.knowntype = s.runsamplekind
        and s.isdeactivated = 'F'
        and kwz.masterkey = '&&ExpMasterkey&'
        AND ra.assayanalyteid = sr.assayanalyteid
        AND ra.runid = s.runid  
        AND ra.studyID = '&&StudyID&'        
        AND K.studyid = '&&StudyID&' AND s.studyid = '&&StudyID&' 
        AND sr.studyID = '&&StudyID&' AND r.studyid = '&&StudyID&'
        and kwz.key=K.NAMEPREFIX
        and kwz.entity = '&&KnownEntity&'
        AND rs.column_value=r.runstatusnum
        AND sr.knownconcentrationconv != 0
        and repid=&&RepID&)
      group by name, runid, assayanalyteid
      )
      group by name, assayanalyteid
      having  sum(rcount) != 0) irs
      where ans.name=irs.name and ans.assayanalyteid = irs.assayanalyteid
      )
     )    
    ) FROM dual