 (SELECT Xmlagg (XMLELEMENT("calculation", 
      XMLELEMENT("target",         
        XMLELEMENT("acceptedrun",acceptedrun),
        XMLELEMENT("analyteid",analyteid),
        XMLELEMENT("species", species),
        XMLELEMENT("matrix", matrix),
        XMLELEMENT("regparam", '&&ParamName&')
        ),
      XMLELEMENT("values", Xmlagg(
        XMLELEMENT("run",  XMLATTRIBUTES(runid AS "runid" ),
          XMLELEMENT("&&ParamName&", &&OwnFormatFunc&(&&OwnRepresentationFunc&(&&ParamName&,&&OwnRepresentationParam&),&&OwnRepresentationParam&))
        ) order by runnr)),
      XMLELEMENT("result",       
        XMLELEMENT("n", Count(&&ParamName&)),
        XMLELEMENT("mean", &&OwnFormatFunc&(&&OwnRepresentationFunc&(avg(case when '&&FinalAccuracy&'='T' then &&ParamName&
                                                                              else &&OwnRepresentationFunc&(&&ParamName&,&&OwnRepresentationParam&)
                                                                         end),&&OwnRepresentationParam&),&&OwnRepresentationParam&)),
        XMLELEMENT("sd", &&OwnFormatFunc&(&&OwnRepresentationFunc&(Stddev(case when '&&FinalAccuracy&'='T' then &&ParamName&
                                                         else &&OwnRepresentationFunc&(&&ParamName&,&&OwnRepresentationParam&)                                             
                                                    end),&&OwnRepresentationParam&),&&OwnRepresentationParam&)),
        XMLELEMENT("cv", case when &&OwnRepresentationFunc&(Avg(&&ParamName&),&&OwnRepresentationParam&) = 0 then null 
                              else FormatRounded(round(case when '&&FinalAccuracy&'='T' then Stddev(&&ParamName&)/Avg(&&ParamName&)*100
                                                            else &&OwnRepresentationFunc&(Stddev(&&OwnRepresentationFunc&(&&ParamName&,&&OwnRepresentationParam&)),&&OwnRepresentationParam&)/
                                                                 &&OwnRepresentationFunc&(Avg(&&OwnRepresentationFunc&(&&ParamName&,&&OwnRepresentationParam&)),&&OwnRepresentationParam&)*100 
                                                       end,&&DecPlRegparamPercCV&),&&DecPlRegparamPercCV&) 
                         end )                        
      ))) 
    FROM
    (SELECT distinct r.id runid, r.runid runnr, ra.analyteid, a.species, a.sampletypeid matrix, &&ParamName&, case when ra.RunAnalyteRegressionStatus = &&RejectedRunAnalyte& then 'F' else 'T' end acceptedrun
     FROM &&TempTabPrefix&bio$study$analytes sa,
          &&TempTabPrefix&BIO$RUN$ANALYTES ra,
          &&TempTabPrefix&bio$run r,
          &&TempTabPrefix&bio$assay a,            --neu
          table(&&LALPackage&.GetRunStates) rs
     WHERE ra.runid = r.ID
       AND r.studyid = '&&StudyID&' AND ra.studyid = '&&StudyID&' AND sa.studyid = '&&StudyID&'
       AND r.RuntypeDescription in  (&&RunTypes&)
       AND a.id = r.assayid
       AND ra.analyteid = sa.id
       AND sa.concentrationunits != 'Titer Units'
       AND ( rs.column_value = '&&AllRunStates&' or rs.column_value=r.runstatusnum) &&AddCondition&)  
    GROUP BY acceptedrun, analyteid, species, matrix, '&&ParamName&') 