  SELECT XMLELEMENT("experiment", XMLATTRIBUTES('&&Experiment&' AS "name" ,'eclrun' AS "type"),
      Xmlagg ( 
        Xmlagg (XMLELEMENT("calculation", xmlconcat(targ,val,res)) order by analytename, runid, rid, posgroup, source, concentrationunit) order by studyid
      ))
  FROM (
  SELECT studyid, analytename, rid, runid, posgroup, source, concentrationunit,
    XMLELEMENT("target",
      XMLELEMENT("runid", rid),
      XMLELEMENT("runno", runid),
      XMLELEMENT("analytename", analytename),
      XMLELEMENT("group", posgroup),
      XMLELEMENT("source", case
                             when LENGTH(TRIM(TRANSLATE(source, ' +-.0123456789', ' '))) is null then
                               FormatSig(sigFigure(source, &&ConcFigures&), &&ConcFigures&)
                             else
                               to_char(source)
                           end),
      XMLELEMENT("concentrationunit", concentrationunit)
    ) as targ,
    XMLELEMENT("values", 
      Xmlagg(
        XMLELEMENT("sample",
          XMLATTRIBUTES(sampleid AS "sampleid", sampleresultid AS "sampleresultid" ),
          XMLELEMENT("runsamplesequencenumber", runsamplesequencenumber),
          XMLELEMENT("samplesubtype", samplesubtype),
          XMLELEMENT("ecl", ecl),
          XMLELEMENT("ecl_sig", FormatSig(sigFigure(ecl_sig, &&ECLFigures&), &&ECLFigures&)),
          XMLELEMENT("ecl_drug", ecl_drug),
          XMLELEMENT("ecl_drug_sig", FormatSig(sigFigure(ecl_drug_sig, &&ECLFigures&), &&ECLFigures&)),
          XMLELEMENT("status", &&concentrationstatus&)
        ) order by runsamplesequencenumber
      )
    ) as val,
    XMLELEMENT("result",
      XMLELEMENT("n", Count(ecl)),
      XMLELEMENT("ncmedian", FormatSig(sigFigure(ncmedian, &&ECLFigures&), &&ECLFigures&)),
      XMLELEMENT("cutpoint", FormatSig(sigFigure(cutpoint, &&ECLFigures&), &&ECLFigures&)),
      XMLELEMENT("mean", FormatSig(sigFigure(Avg(ecl), &&ECLFigures&), &&ECLFigures&)),
      XMLELEMENT("mean_drug", FormatSig(sigFigure(Avg(ecl_drug), &&ECLFigures&), &&ECLFigures&)),
      XMLELEMENT("sd", FormatSig(sigFigure(Stddev(ecl), &&ConcSDFigures&), &&ConcSDFigures&)),
      XMLELEMENT("cv", FormatRounded(round(sigFigure(Stddev(ecl), &&ConcSDFigures&)/sigFigure(Avg(ecl), &&ECLFigures&)*100, &&DecPlPercCV&), &&DecPlPercCV&)),
      XMLELEMENT("ratio", FormatRounded(round(sigFigure(Avg(ecl), &&ECLFigures&)/sigFigure(cutpoint, &&ECLFigures&), &&DecPlNCdivCP&), &&DecPlNCdivCP&)),
      XMLELEMENT("bias", FormatRounded(round(
                          (Avg(ecl)-Avg(ecl_drug))/Avg(ecl)*100--(sigFigure(Avg(ecl), &&ECLFigures&)-sigFigure(Avg(ecl_drug), &&ECLFigures&))/sigFigure(Avg(ecl), &&ECLFigures&)*100
                          , &&DecPlPercBias&), &&DecPlPercBias&)),
      XMLELEMENT("flag",
                case
                  when (round(sigFigure(Stddev(ecl), &&ConcSDFigures&)/sigFigure(Avg(ecl), &&ECLFigures&)*100, &&DecPlPercCV&)) > 20 then 'T'
                  else 'F'
                end
      )
    ) as res
  FROM (
    SELECT
      nc.studyid, nc.rid, nc.runid, nc. analytename,
      -- NC:
      nc.cutpoint, nc.cutpoint_sig, nc.medianecl ncmedian, nc.medianecl_sig ncmedian_sig,
      -- Screens:
      unspiked.posgroup, unspiked.sampleid, unspiked.sampleresultid, unspiked.samplesubtype, unspiked.runsamplesequencenumber,
      unspiked.ecl ecl, sigFigure(unspiked.ecl, &&ECLFigures&) ecl_sig, unspiked.concentrationstatus, unspiked.source, unspiked.concentrationunit,
      spiked.ecl ecl_drug, sigFigure(spiked.ecl, &&ECLFigures&) ecl_drug_sig
    FROM (
      SELECT
        r.studyid,
        r.id rid,
        r.runid,
        sa.id analyteid,
        sa.analyteorder,
        &&AnalyteName& analytename,
        sigFigure(median(sr.analytearea), &&ECLFigures&) medianecl,
        median(sigFigure(sr.analytearea, &&ECLFigures&)) medianecl_sig,
        sigFigure(median(sr.analytearea), &&ECLFigures&) * min(a.cutpointfactor) cutpoint,
        median(sigFigure(sr.analytearea, &&ECLFigures&)) * min(a.cutpointfactor) cutpoint_sig
      FROM 
        &&TempTabPrefix&bio$run r,
        &&TempTabPrefix&bio$run$analytes ra,
        &&TempTabPrefix&bio$study$analytes sa,
        &&TempTabPrefix&bio$assay a,
        &&TempTabPrefix&bio$run$worklist s,
        &&TempTabPrefix&bio$run$worklist$result$rw sr,
        table(&&LALPackage&.GetRunStates) rs
      WHERE r.ID = s.runID
      AND s.ID = sr.worklistID
      AND a.ID = r.assayID
      --AND s.treatmentid = 'ADA'
      --AND s.studyid = '&&StudyID&' AND sr.studyID = '&&StudyID&'
      AND r.studyid = '&&StudyID&'
      AND a.studyID = '&&StudyID&'
      AND rs.column_value = r.runstatusnum
      AND sr.runAnalyteCode = ra.code
      AND r.ID = ra.runID
      AND ra.analyteID = sa.ID
      AND s.runsamplekind = '&&NCKnownType&'
      &&AddConditionNC&
      GROUP BY r.studyid, sa.analyteorder, sa.id, sa.name, r.id, r.runid
    ) nc , (
      SELECT
        r.studyid,
        s.id sampleid,
        sr.id sampleresultid,
        r.id rid,
        r.runid,
        sa.id analyteid,
        s.samplesubtype,
        s.runsamplesequencenumber,
        sr.analytearea ecl,
        s.status,
        sr.commenttext,
        sr.concentrationstatus,
        s.source,
        s.concentrationunit,
        s.samplesubtype posgroup,
        s.replicatenumber
      FROM 
        &&TempTabPrefix&bio$run r,
        &&TempTabPrefix&bio$run$analytes ra,
        &&TempTabPrefix&bio$study$analytes sa,
        &&TempTabPrefix&bio$assay a,
        &&TempTabPrefix&bio$run$worklist s,
        &&TempTabPrefix&bio$run$worklist$result$rw sr,
        table(&&LALPackage&.GetRunStates) rs
      WHERE r.ID = s.runID
      AND s.ID = sr.worklistID
      AND a.ID = r.assayID
      --AND s.treatmentid = 'ADA'
      --AND s.studyid = '&&StudyID&' AND sr.studyID = '&&StudyID&'
      AND r.studyid = '&&StudyID&'
      AND a.studyID = '&&StudyID&'
      AND rs.column_value = r.runstatusnum
      AND sr.runAnalyteCode = ra.code
      AND r.ID = ra.runID
      AND ra.analyteID = sa.ID
      &&AddConditionUnspiked&
    ) unspiked , (
      SELECT
        r.studyid,
        s.id sampleid,
        sr.id sampleresultid,
        r.id rid,
        r.runid,
        sa.id analyteid,
        s.samplesubtype,
        s.runsamplesequencenumber,
        sr.analytearea ecl,
        s.status,
        sr.commenttext,
        sr.concentrationstatus,
        s.source,
        s.concentrationunit,
        s.samplesubtype posgroup,
        s.replicatenumber
      FROM 
        &&TempTabPrefix&bio$run r,
        &&TempTabPrefix&bio$run$analytes ra,
        &&TempTabPrefix&bio$study$analytes sa,
        &&TempTabPrefix&bio$assay a,
        &&TempTabPrefix&bio$run$worklist s,
        &&TempTabPrefix&bio$run$worklist$result$rw sr,
        table(&&LALPackage&.GetRunStates) rs
      WHERE r.ID = s.runID
      AND s.ID = sr.worklistID
      AND a.ID = r.assayID
      --AND s.treatmentid = 'ADA'
      --AND s.studyid = '&&StudyID&' AND sr.studyID = '&&StudyID&'
      AND r.studyid = '&&StudyID&'
      AND a.studyID = '&&StudyID&'
      AND rs.column_value = r.runstatusnum
      AND sr.runAnalyteCode = ra.code
      AND r.ID = ra.runID
      AND ra.analyteID = sa.ID
      &&AddConditionSpiked&
    ) spiked
    WHERE unspiked.studyID = nc.studyID
    AND unspiked.rID = nc.rID
    AND unspiked.runid = nc.runid
    AND unspiked.analyteID = nc.analyteID
    AND unspiked.studyID = spiked.studyID (+)
    AND unspiked.rID = spiked.rID (+)
    AND unspiked.runid = spiked.runid (+)
    AND unspiked.analyteID = spiked.analyteID (+)
    AND unspiked.posgroup = spiked.posgroup (+)
    AND unspiked.replicatenumber = spiked.replicatenumber (+)
    &&AddCondition&
  )
  GROUP BY studyid, analytename, runid, rid, cutpoint, cutpoint_sig, ncmedian, ncmedian_sig, posgroup, source, concentrationunit)
GROUP BY studyid
