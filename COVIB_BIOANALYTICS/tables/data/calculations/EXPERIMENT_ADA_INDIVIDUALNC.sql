SELECT XMLELEMENT("experiment", XMLATTRIBUTES('&&Experiment&' AS "name" ,'individualnc' AS "type"),
      Xmlagg ( XMLConcat(
        Xmlagg (XMLELEMENT("calculation", xmlconcat(targ,val,res)) order by studyid, acceptedrun, analyteid, species, matrix)
        --XMLELEMENT("statistic", XMLATTRIBUTES(analyteid AS "analyteid"),
               
             --)) order by studyid, analyteid, species, matrix, hours, longtermunits, longtermtime, temperature
      )))
  FROM (
  SELECT
    studyid, analyteid, species, matrix, /*rid, runid,*/ acceptedrun,
    --sum(s1_n)+sum(s2_n) n, sum(s1_m)+sum(s2_m) m,
    XMLELEMENT("target",
      XMLELEMENT("studyid", studyid),
      XMLELEMENT("analyteid", analyteid),
      XMLELEMENT("species", species),
      XMLELEMENT("matrix", matrix),
      XMLELEMENT("acceptedrun", acceptedrun)
    ) as targ,
    XMLELEMENT("values",
      Xmlagg(
        XMLELEMENT("sample",
          XMLELEMENT("runid", rid),
          XMLELEMENT("runno", runid),
          XMLELEMENT("replicate", replicate),
          XMLELEMENT("replicatenumber", replicatenumber),
          XMLELEMENT("runsamplesequencenumber", runsamplesequencenumber),
          XMLELEMENT("screen-cp", FormatRounded(round(screen_cp, &&DecPlCutPoint&), &&DecPlCutPoint&)),
          XMLELEMENT("confirm-cp", FormatRounded(round(confirm_cp, &&DecPlConfirmCutPoint&), &&DecPlConfirmCutPoint&)),
          --XMLELEMENT("titer-cp", FormatRounded(round(titer_cp, &&DecPlCutPoint&), &&DecPlCutPoint&)),
          nc_xml,
          XMLELEMENT("nc1-response", FormatRounded(round(nc_mean_response, &&DecPlResponse&), &&DecPlResponse&)),
          XMLELEMENT("nc1-sd", &&FormatFuncResponseSD&(&&RepresentationFuncResponseSD&(nc_sd, &&PlacesResponseSD&), &&PlacesResponseSD&)),
          XMLELEMENT("nc1-cv", FormatRounded(round(nc_sd/nullif(nc_mean_useresponse,0) * 100, &&DecPlPercCV&), &&DecPlPercCV&)),
          XMLELEMENT("nc1-cvflag", case when nc_sd/nullif(nc_mean_useresponse,0) * 100 > &&PrecisionAcceptanceCriteria& then 'T' else 'F' end),
          XMLELEMENT("nc1-n", nc_n),
          XMLELEMENT("nc1-m", nc_m),
          XMLELEMENT("nc2-response", FormatRounded(round(nc2_mean_response, &&DecPlResponse&), &&DecPlResponse&)),
          XMLELEMENT("nc2-sd", &&FormatFuncResponseSD&(&&RepresentationFuncResponseSD&(nc2_sd, &&PlacesResponseSD&), &&PlacesResponseSD&)),
          XMLELEMENT("nc2-cv", FormatRounded(round(nc2_sd/nullif(nc2_mean_useresponse,0) * 100, &&DecPlPercCV&), &&DecPlPercCV&)),
          XMLELEMENT("nc2-cvflag", case when nc2_sd/nullif(nc2_mean_useresponse,0) * 100 > &&PrecisionAcceptanceCriteria& then 'T' else 'F' end),
          XMLELEMENT("nc2-n", nc2_n),
          XMLELEMENT("nc2-m", nc2_m),
          XMLELEMENT("difference", FormatRounded(round(difference, &&DecPlInhibition&), &&DecPlInhibition&)),
          XMLELEMENT("inhibition", FormatRounded(round(inhibition, &&DecPlInhibition&), &&DecPlInhibition&)),
          XMLELEMENT("run-nc1-medianormofmeans", FormatRounded(round(medianormofmeans, &&DecPlResponse&), &&DecPlResponse&)),
          XMLELEMENT("run-nc1-mean", FormatRounded(round(run_nc_mean_useresponse, &&DecPlResponse&), &&DecPlResponse&)),
          XMLELEMENT("run-nc1-sd", &&FormatFuncResponseSD&(&&RepresentationFuncResponseSD&(run_nc_sd_useresponse, &&PlacesResponseSD&), &&PlacesResponseSD&)),
          XMLELEMENT("run-nc1-cv", FormatRounded(round(run_nc_sd_useresponse/nullif(run_nc_mean_useresponse,0) * 100, &&DecPlPercCV&), &&DecPlPercCV&)),
          XMLELEMENT("run-nc1-cvflag", case when run_nc_sd_useresponse/nullif(run_nc_mean_useresponse,0) * 100 > &&PrecisionAcceptanceCriteria& then 'T' else 'F' end),
          XMLELEMENT("run-nc1-n", nc_n),
          XMLELEMENT("run-nc1-m", nc_m),
          XMLELEMENT("run-nc2-mean", FormatRounded(round(run_nc2_mean_useresponse, &&DecPlResponse&), &&DecPlResponse&)),
          XMLELEMENT("run-nc2-sd", &&FormatFuncResponseSD&(&&RepresentationFuncResponseSD&(run_nc2_sd_useresponse, &&PlacesResponseSD&), &&PlacesResponseSD&)),
          XMLELEMENT("run-nc2-cv", FormatRounded(round(run_nc2_sd_useresponse/nullif(run_nc2_mean_useresponse,0) * 100, &&DecPlPercCV&), &&DecPlPercCV&)),
          XMLELEMENT("run-nc2-cvflag", case when run_nc2_sd_useresponse/nullif(run_nc2_mean_useresponse,0) * 100 > &&PrecisionAcceptanceCriteria& then 'T' else 'F' end),
          XMLELEMENT("run-nc2-n", nc2_n),
          XMLELEMENT("run-nc2-m", nc2_m),
          XMLELEMENT("run-difference-mean", FormatRounded(round(run_mean_difference, &&DecPlInhibition&), &&DecPlInhibition&)),
          XMLELEMENT("run-difference-sd", FormatRounded(round(run_sd_difference, &&DecPlInhibition&), &&DecPlInhibition&)),
          XMLELEMENT("run-difference-cv", FormatRounded(round(run_sd_difference/nullif(run_mean_difference,0) * 100, &&DecPlInhibition&), &&DecPlInhibition&)),
          XMLELEMENT("run-inhibition-mean", FormatRounded(round(run_mean_inhibition, &&DecPlInhibition&), &&DecPlInhibition&)),
          XMLELEMENT("run-inhibition-sd", FormatRounded(round(run_sd_inhibition, &&DecPlInhibition&), &&DecPlInhibition&)),
          XMLELEMENT("run-inhibition-cv", FormatRounded(round(run_sd_inhibition/nullif(run_mean_inhibition,0) * 100, &&DecPlInhibition&), &&DecPlInhibition&))
        ) order by runid, runsamplesequencenumber, replicate, replicatenumber
      )
    ) as val,
    XMLELEMENT("result",
      XMLELEMENT("nc1-mean", FormatRounded(round(Avg(nc_mean_useresponse), &&DecPlResponse&), &&DecPlResponse&)),
      XMLELEMENT("nc1-sd", &&FormatFuncResponseSD&(&&RepresentationFuncResponseSD&(Stddev(nc_mean_useresponse), &&PlacesResponseSD&), &&PlacesResponseSD&)),
      XMLELEMENT("nc1-cv", FormatRounded(round(Stddev(nc_mean_useresponse)/nullif(Avg(nc_mean_useresponse),0) * 100, &&DecPlPercCV&), &&DecPlPercCV&)),
      XMLELEMENT("nc1-n", Sum(nc_n)),
      XMLELEMENT("nc1-m", Sum(nc_m)),
      XMLELEMENT("nc1-mean-plus-xsd", FormatRounded(round(Avg(nc_mean_useresponse) + (&&SSTSDNumber& * Stddev(nc_mean_useresponse)), &&DecPlMeanPlusMinusXSD&), &&DecPlMeanPlusMinusXSD&)),
      XMLELEMENT("nc1-mean-minus-xsd", FormatRounded(round(Avg(nc_mean_useresponse) - (&&SSTSDNumber& * Stddev(nc_mean_useresponse)), &&DecPlMeanPlusMinusXSD&), &&DecPlMeanPlusMinusXSD&)),
      XMLELEMENT("nc2-mean", FormatRounded(round(Avg(nc2_mean_useresponse), &&DecPlResponse&), &&DecPlResponse&)),
      XMLELEMENT("nc2-sd", &&FormatFuncResponseSD&(&&RepresentationFuncResponseSD&(Stddev(nc2_mean_useresponse), &&PlacesResponseSD&), &&PlacesResponseSD&)),
      XMLELEMENT("nc2-cv", FormatRounded(round(Stddev(nc2_mean_useresponse)/nullif(Avg(nc2_mean_useresponse),0) * 100, &&DecPlPercCV&), &&DecPlPercCV&)),
      XMLELEMENT("nc2-n", Sum(nc2_n)),
      XMLELEMENT("nc2-m", Sum(nc2_m)),
      XMLELEMENT("nc2-mean-plus-xsd", FormatRounded(round(Avg(nc2_mean_useresponse) + (&&SSTSDNumber& * Stddev(nc2_mean_useresponse)), &&DecPlMeanPlusMinusXSD&), &&DecPlMeanPlusMinusXSD&)),
      XMLELEMENT("nc2-mean-minus-xsd", FormatRounded(round(Avg(nc2_mean_useresponse) - (&&SSTSDNumber& * Stddev(nc2_mean_useresponse)), &&DecPlMeanPlusMinusXSD&), &&DecPlMeanPlusMinusXSD&)),
      XMLELEMENT("difference-mean", FormatRounded(round(Avg(difference), &&DecPlInhibition&), &&DecPlInhibition&)),
      XMLELEMENT("difference-sd", FormatRounded(round(Stddev(difference), &&DecPlInhibition&), &&DecPlInhibition&)),
      XMLELEMENT("difference-cv", FormatRounded(round(Stddev(difference)/nullif(Avg(difference),0) * 100, &&DecPlInhibition&), &&DecPlInhibition&)),
      XMLELEMENT("inhibition-mean", FormatRounded(round(Avg(inhibition), &&DecPlInhibition&), &&DecPlInhibition&)),
      XMLELEMENT("inhibition-sd", FormatRounded(round(Stddev(inhibition), &&DecPlInhibition&), &&DecPlInhibition&)),
      XMLELEMENT("inhibition-cv", FormatRounded(round(Stddev(inhibition)/nullif(Avg(inhibition),0) * 100, &&DecPlInhibition&), &&DecPlInhibition&))
    ) as res
  FROM (
    SELECT
      NC.studyid, NC.analyteid, NC.rid, NC.runid, NC.acceptedrun, NC.replicate, NC.replicatenumber, NC.runsamplesequencenumber, NC.species, NC.matrix,
      case
        when regexp_like('&&FixedScreenCutpoint&', '^\-?(\d+)?\.?\d+$') then to_number('&&FixedScreenCutpoint&')
        when '&&ScreenCPNFactor&' is not null then
          case
            when stb$util.GetReportParameterValue('MEAN_OR_MEDIAN_NC',&&RepID&) = 'meanofmeans' then Avg(case when NC.replicatenumber=1 then NC.nc_meanreplicateresponse else null end) over (partition by NC.studyid, NC.analyteid, NC.species, NC.matrix, NC.rid, NC.runid, NC.acceptedrun)
            else meanmedian_useresponse
          end * to_number('&&ScreenCPNFactor&')
        else
          case
            when stb$util.GetReportParameterValue('MEAN_OR_MEDIAN_NC',&&RepID&) = 'meanofmeans' then Avg(case when NC.replicatenumber=1 then NC.nc_meanreplicateresponse else null end) over (partition by NC.studyid, NC.analyteid, NC.species, NC.matrix, NC.rid, NC.runid, NC.acceptedrun)
            else meanmedian_useresponse
          end
      end screen_cp,
      case
        when stb$util.GetReportParameterValue('MEAN_OR_MEDIAN_NC',&&RepID&) = 'meanofmeans' then Avg(case when NC.replicatenumber=1 then NC.nc_meanreplicateresponse else null end) over (partition by NC.studyid, NC.analyteid, NC.species, NC.matrix, NC.rid, NC.runid, NC.acceptedrun)
        else meanmedian_useresponse
      end medianormofmeans,
      NC.confirm_cp,
      NC.nc_mean_response, NC.nc_mean_useresponse,
      NC.nc_sd, NC.nc_n, NC.nc_m,
      NC.nc2_mean_response, NC.nc2_mean_useresponse,
      NC.nc2_sd, NC.nc2_n, NC.nc2_m,
      NC.xml nc_xml,
      (NC.nc_mean_useresponse - NC.nc2_mean_useresponse) / nullif(NC.nc_mean_useresponse,0) * 100 difference,
      Abs((NC.nc_mean_useresponse - NC.nc2_mean_useresponse) / nullif(NC.nc_mean_useresponse,0) * 100) inhibition,
      Avg(NC.nc_mean_useresponse) over (partition by NC.studyid, NC.analyteid, NC.species, NC.matrix, NC.rid, NC.runid, NC.acceptedrun) run_nc_mean_useresponse,
      Stddev(NC.nc_mean_useresponse) over (partition by NC.studyid, NC.analyteid, NC.species, NC.matrix, NC.rid, NC.runid, NC.acceptedrun) run_nc_sd_useresponse,
      Avg(NC.nc2_mean_useresponse) over (partition by NC.studyid, NC.analyteid, NC.species, NC.matrix, NC.rid, NC.runid, NC.acceptedrun) run_nc2_mean_useresponse,
      Stddev(NC.nc2_mean_useresponse) over (partition by NC.studyid, NC.analyteid, NC.species, NC.matrix, NC.rid, NC.runid, NC.acceptedrun) run_nc2_sd_useresponse,
      Avg((NC.nc_mean_useresponse - NC.nc2_mean_useresponse) / nullif(NC.nc_mean_useresponse,0) * 100) over (partition by NC.studyid, NC.analyteid, NC.species, NC.matrix, NC.rid, NC.runid, NC.acceptedrun) run_mean_difference,
      Stddev((NC.nc_mean_useresponse - NC.nc2_mean_useresponse) / nullif(NC.nc_mean_useresponse,0) * 100) over (partition by NC.studyid, NC.analyteid, NC.species, NC.matrix, NC.rid, NC.runid, NC.acceptedrun) run_sd_difference,
      Avg(Abs((NC.nc_mean_useresponse - NC.nc2_mean_useresponse) / nullif(NC.nc_mean_useresponse,0) * 100)) over (partition by NC.studyid, NC.analyteid, NC.species, NC.matrix, NC.rid, NC.runid, NC.acceptedrun) run_mean_inhibition,
      Stddev(Abs((NC.nc_mean_useresponse - NC.nc2_mean_useresponse) / nullif(NC.nc_mean_useresponse,0) * 100)) over (partition by NC.studyid, NC.analyteid, NC.species, NC.matrix, NC.rid, NC.runid, NC.acceptedrun) run_sd_inhibition
    FROM
      (
        SELECT
          nc.studyid, nc.analyteid, nc.rid, nc.runid, nc.species, nc.matrix, nc.replicate, nc.replicatenumber, nc.acceptedrun,
 nc.runsamplesequencenumber,
          Avg(nc.response) nc_mean_response, Min(nc.meanreplicateresponse) nc_meanreplicateresponse,
          Avg(case when(nc.isdeactivated='F') then nc.response else null end) nc_mean_useresponse,
          Stddev(case when(nc.isdeactivated='F') then nc.response else null end) nc_sd,
          count(case when(nc.isdeactivated='F') then 1 else null end) nc_n,
          count(1) nc_m,
          Avg(nc2.response) nc2_mean_response,
          Avg(case when(nc2.isdeactivated='F') then nc2.response else null end) nc2_mean_useresponse,
          Stddev(case when(nc2.isdeactivated='F') then nc2.response else null end) nc2_sd,
          count(case when(nc2.isdeactivated='F') then 1 else null end) nc2_n,
          count(1) nc2_m,
          min(nc.meanmedianresponse) meanmedian_response,
          min(nc.meanmedianuseresponse) meanmedian_useresponse,
          case when regexp_like(ccp.key, '^\-?(\d+)?\.?\d+$') then to_number(ccp.key) else null end confirm_cp,
          XMLConcat(
            XMLELEMENT("s1",
              Xmlagg(
                XMLELEMENT("sample",
                  XMLATTRIBUTES(nc.sampleid AS "sampleid", nc.sampleresultid AS "sampleresultid" ),
                  XMLELEMENT("runsamplesequencenumber", nc.runsamplesequencenumber),
                  XMLELEMENT("samplename", nc.samplename),
                  XMLELEMENT("samplesubtype", nc.samplesubtype),
                  XMLELEMENT("response", FormatRounded(round(nc.response, &&DecPlResponse&), &&DecPlResponse&)),
                  XMLELEMENT("status", nc.resulttext),
                  XMLELEMENT("source", nc.source),
                  XMLELEMENT("deactivated", nc.isdeactivated),
                  XMLELEMENT("reason", nc.reason)
                ) order by nc.runsamplesequencenumber
              )
            ),
            XMLELEMENT("s2",
              Xmlagg(
                XMLELEMENT("sample",
                  XMLATTRIBUTES(nc2.sampleid AS "sampleid", nc2.sampleresultid AS "sampleresultid" ),
                  XMLELEMENT("runsamplesequencenumber", nc2.runsamplesequencenumber),
                  XMLELEMENT("samplename", nc2.samplename),
                  XMLELEMENT("samplesubtype", nc2.samplesubtype),
                  XMLELEMENT("response", FormatRounded(round(nc2.response, &&DecPlResponse&), &&DecPlResponse&)),
                  XMLELEMENT("difference", FormatRounded(round(
                    (nc.response - nc2.response) / nullif(nc.response,0) * 100
                  , &&DecPlInhibition&), &&DecPlInhibition&)),
                  XMLELEMENT("inhibition", FormatRounded(round(Abs(
                    (nc.response - nc2.response) / nullif(nc.response,0) * 100
                  ), &&DecPlInhibition&), &&DecPlInhibition&)),
                  XMLELEMENT("status", nc2.resulttext),
                  XMLELEMENT("source", nc2.source),
                  XMLELEMENT("deactivated", nc2.isdeactivated),
                  XMLELEMENT("reason", nc2.reason)
                ) order by nc2.runsamplesequencenumber
              )
            )
          ) xml
        FROM (
          SELECT
            r.studyid, s.id sampleid, sr.id sampleresultid, s.replicatenumber,
            s.designsampleid, s.samplename, regexp_replace(s.samplename,'^(\D+\d+).*$', '\1', 1, 1, 'i') name,
            case
              when regexp_like(s.samplename, '^\D+(\d+).*$', 'i') then to_number(regexp_replace(s.samplename, '^\D+(\d+).*$', '\1', 1, 1, 'i'))
              else null
            end replicate,
            r.id rid, r.runid, sa.id analyteid, s.samplesubtype,
            case when ra.RunAnalyteRegressionStatus = &&AcceptedRunAnalyte& then 'T' else 'F' end acceptedrun,
            s.runsamplesequencenumber, sr.analytearea response, case when(d.code is not null) then sr.analytearea else null end useresponse,
            Avg(case when(d.code is null) then sr.analytearea else null end) over (partition by r.studyid, sa.id, r.id, case when regexp_like(s.samplename, '^\D+(\d+).*$', 'i') then to_number(regexp_replace(s.samplename, '^\D+(\d+).*$', '\1', 1, 1, 'i')) else null end) meanreplicateresponse,
            &&NCMeanOrMedian&(case when(d.code is null) then sr.analytearea else null end) over (partition by r.studyid, sa.id, r.id) meanmedianresponse,
            &&NCMeanOrMedian&(case when(d.code is null) then sr.analytearea else null end) over (partition by r.studyid, sa.id, r.id) meanmedianuseresponse,
            s.status, sr.commenttext, sr.concentration conc, d.reason,
            sr.resulttext, s.source, s.concentration nominalconc,
            sr.concentrationunits concentrationunit, a.species, a.sampletypeid matrix,
            s.hours, s.longtermunits, s.longtermtime, s.temperature,
            case
              when d.code is not null then 'T'
              else 'F'
            end isdeactivated
          FROM 
            &&TempTabPrefix&bio$run r,
            &&TempTabPrefix&bio$run$analytes ra,
            &&TempTabPrefix&bio$study$analytes sa,
            &&TempTabPrefix&bio$assay a,
            &&TempTabPrefix&bio$run$worklist s,
            &&TempTabPrefix&bio$run$worklist$result$rw sr,
            &&TempTabPrefix&bio$covib$run$sample$deact d,
            isr$crit kwz--,
            --table(&&LALPackage&.GetRunStates) rs
          WHERE r.ID = s.runID AND s.ID = sr.worklistID AND a.ID = r.assayID AND r.studyID = 'S1' AND a.studyID = 'S1'
          --AND rs.column_value = r.runstatusnum
          AND sr.runAnalyteCode = ra.code AND r.ID = ra.runID
          AND sr.runanalyteCode = ra.Code AND ra.analyteID = sa.ID
          AND s.code = d.code (+)
          AND kwz.key = s.samplename AND kwz.entity = '&&KnownEntity&' AND kwz.masterkey = '&&ExpMasterkey&' AND kwz.repid = &&RepID&
          --and s.runcode || '-' || s.runsamplesequencenumber || '-' || sa.analyteorder in (select code from &&TempTabPrefix&bio$deactivated$samples)
          &&AddConditionNC&
        ) NC,
        (
          SELECT
            r.studyid, s.id sampleid, sr.id sampleresultid, s.replicatenumber,
            s.designsampleid, s.samplename, regexp_replace(s.samplename,'^(\D+\d+).*$', '\1', 1, 1, 'i') name,
            r.id rid, r.runid, sa.id analyteid, s.samplesubtype,
            s.runsamplesequencenumber, sr.analytearea response, case when(d.code is not null) then sr.analytearea else null end useresponse,
            s.status, sr.commenttext, sr.concentration conc, d.reason,
            sr.resulttext, s.source, s.concentration nominalconc,
            sr.concentrationunits concentrationunit, a.species, a.sampletypeid matrix,
            s.hours, s.longtermunits, s.longtermtime, s.temperature,
            case
              when d.code is not null then 'T'
              else 'F'
            end isdeactivated
          FROM 
            &&TempTabPrefix&bio$run r,
            &&TempTabPrefix&bio$run$analytes ra,
            &&TempTabPrefix&bio$study$analytes sa,
            &&TempTabPrefix&bio$assay a,
            &&TempTabPrefix&bio$run$worklist s,
            &&TempTabPrefix&bio$run$worklist$result$rw sr,
            &&TempTabPrefix&bio$covib$run$sample$deact d,
            isr$crit kwz--,
            --table(&&LALPackage&.GetRunStates) rs
          WHERE r.ID = s.runID AND s.ID = sr.worklistID AND a.ID = r.assayID AND r.studyID = 'S1' AND a.studyID = 'S1'
          --AND rs.column_value = r.runstatusnum
          AND sr.runAnalyteCode = ra.code AND r.ID = ra.runID
          AND sr.runanalyteCode = ra.Code AND ra.analyteID = sa.ID
          AND s.code = d.code (+)
          AND kwz.key = s.samplename AND kwz.entity = '&&KnownEntity&' AND kwz.masterkey = '&&ExpMasterkey&' AND kwz.repid = &&RepID&
          --and s.runcode || '-' || s.runsamplesequencenumber || '-' || sa.analyteorder in (select code from &&TempTabPrefix&bio$deactivated$samples)
          &&AddConditionNC2&
        ) NC2, isr$crit ccp
        WHERE ccp.entity = 'BIO$COVIB$CONFIRM$CUTPOINT' AND ccp.masterkey = 'NORM' AND ccp.repid = &&RepID&
        AND NC.studyID = NC2.studyID(+) AND NC.rID = NC2.rID(+) AND NC.runid = NC2.runid(+)
        AND NC.analyteID = NC2.analyteID(+) AND NC.species = NC2.species(+) AND NC.matrix = NC2.matrix(+)
        AND NC.name = NC2.name(+) AND NC.replicatenumber = NC2.replicatenumber(+)
        GROUP BY NC.studyid, NC.analyteid, NC.species, NC.matrix, NC.rid, NC.runid, NC.replicate, NC.replicatenumber, NC.acceptedrun, NC.runsamplesequencenumber, ccp.key) NC
 )
 GROUP BY studyid, analyteid, species, matrix, /*rid, runid,*/ acceptedrun)
GROUP BY studyid--, analyteid, species, matrix, acceptedrun
