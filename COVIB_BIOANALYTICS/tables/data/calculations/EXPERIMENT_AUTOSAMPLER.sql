  SELECT XMLELEMENT("experiment", XMLATTRIBUTES('&&Experiment&' AS "name" ,'autosampler' AS "type"),  
  (
  select XMLAgg(XMLConcat(Xmlagg (XMLELEMENT("calculation", xmlconcat(targ,val,res))),
    XMLELEMENT("statistic", XMLATTRIBUTES(analyteid AS "analyteid", species AS "species", matrix AS "matrix"),
      XMLELEMENT("flagpercent", min(flagpercent)),
      XMLELEMENT("min-bias", FormatRounded(round(min(minbias_as),&&DecPlPercBias&),&&DecPlPercBias&)),
      XMLELEMENT("max-bias", FormatRounded(round(max(maxbias_as),&&DecPlPercBias&),&&DecPlPercBias&)),
      XMLELEMENT("numacc", sum(cntunflagged)),
      XMLELEMENT("numtotal", sum(cnt)),
      XMLELEMENT("percacc", case when sum(cnt) != 0 then FmtNum(round(sum(cntunflagged)/sum(cnt)*100,&&DecPlPercStat&)) else null end)
    )) order by analyteid, species, matrix)
  from (  
  SELECT 
      analyteid, species, matrix,
      Count(useconc) cnt,
      count(unflagged) cntunflagged,
      FormatRounded(round(Avg(usebias),1),1) bias,
      min(minbias_as) minbias_as,
      min(maxbias_as) maxbias_as,
      min(flagpercent) flagpercent,
      XMLELEMENT("target", 
        XMLELEMENT("analyteid", analyteid),
        XMLELEMENT("species", species),
        XMLELEMENT("matrix", matrix),
        XMLELEMENT("runid", runid)
        ) targ,
      XMLELEMENT("values", Xmlagg(
        XMLELEMENT("sample",  XMLATTRIBUTES(sampleresultid_init AS "sampleresultid-init", sampleresultid AS "sampleresultid"), 
          XMLELEMENT("runid-init", runid_init),                     
          XMLELEMENT("sampleid-init", sampleID_init),
          XMLELEMENT("wtrunid-init", wtrunid_init),          
          XMLELEMENT("conc-init", &&ConcFormatFunc&(&&ConcRepresentationFunc&(conc_init,&&ConcFigures&),&&ConcFigures&)),     
          XMLELEMENT("name-init", name_init),   
          XMLELEMENT("rep-init", rep_init),
          XMLELEMENT("autosampletime", autosampletime),          
          XMLELEMENT("sampleid", sampleID),          
          XMLELEMENT("wtrunid", wtrunid),  
          XMLELEMENT("conc", &&ConcFormatFunc&(&&ConcRepresentationFunc&(conc,&&ConcFigures&),&&ConcFigures&)),
          XMLELEMENT("bias", FormatRounded(round(bias,&&DecPlPercBias&),&&DecPlPercBias&)),
          XMLELEMENT("name", NAME),
          XMLELEMENT("rep", rep),
          XMLELEMENT("runsamplesequencenumber", runsamplesequencenumber),
          XMLELEMENT("critorder", critorder),
          XMLELEMENT("flag", flagged),
          XMLELEMENT("flagpercent", flagpercent),
          XMLELEMENT("deactivated", isdeactivated),
          XMLELEMENT("deactivated-init", isdeactivated_init)          
          ) order by nominalconc, runsamplesequencenumber)) val,      
      XMLELEMENT("result",              
        XMLELEMENT("n", Count(useconc)),        
        XMLELEMENT("mean", &&ConcFormatFunc&(&&ConcRepresentationFunc&(Avg(useconc),&&ConcFigures&),&&ConcFigures&)),
        XMLELEMENT("sd", &&ConcFormatFunc&(&&ConcRepresentationFunc&(Stddev(useconc),&&ConcSDFigures&),&&ConcSDFigures&)),
        XMLELEMENT("cv", FormatRounded(round(case when '&&FinalAccuracy&'='T' then Stddev(useconc)/Avg(useconc)*100
                                                  else &&ConcRepresentationFunc&(Stddev(useconc),&&ConcFigures&)
                                                      /&&ConcRepresentationFunc&(Avg(useconc),&&ConcFigures&)*100
                                             end,
                         &&DecPlPercCV&),&&DecPlPercCV&)),        
        XMLELEMENT("mean-init", &&ConcFormatFunc&(&&ConcRepresentationFunc&(Avg(useconc_init),&&ConcFigures&),&&ConcFigures&)),
        XMLELEMENT("sd-init", &&ConcFormatFunc&(&&ConcRepresentationFunc&(Stddev(useconc_init),&&ConcFigures&),&&ConcFigures&)),
        XMLELEMENT("cv-init", FormatRounded(round(case when '&&FinalAccuracy&'='T' then Stddev(useconc_init)/Avg(useconc_init)*100
                                                       else &&ConcRepresentationFunc&(Stddev(useconc_init),&&ConcFigures&)
                                                           /&&ConcRepresentationFunc&(Avg(useconc_init),&&ConcFigures&)*100
                                                  end,
                              &&DecPlPercCV&),&&DecPlPercCV&)),
        XMLELEMENT("bias", FormatRounded(round(Avg(usebias),&&DecPlPercBias&),&&DecPlPercBias&)),
        XMLELEMENT("biasonmean", FormatRounded(round(case when Avg(useconc_init)= 0 then null
                                                    when '&&FinalAccuracy&'='T' then (Avg(useconc)-Avg(useconc_init))/Avg(useconc_init)*100
                                                    else (&&ConcRepresentationFunc&(Avg(useconc),&&ConcFigures&)-&&ConcRepresentationFunc&(Avg(useconc_init),&&ConcFigures&))/&&ConcRepresentationFunc&(Avg(useconc_init),&&ConcFigures&)*100
                                               end,&&DecPlPercBias&),&&DecPlPercBias&))      
       ) res
    FROM
    (
    SELECT wtrunid, wtrunid_init, 
        knownID, sampleID, sampleresultID, 
         autosampletime,
        name, nominalconc, runid, 
        analyteid, analyteorder, species, matrix, rep,
        runsamplesequencenumber, flagpercent, 
        case when abs(bias) > flagpercent then '&&FlaggedSymbol&' else null end flagged,
        case when ISDEACTIVATED <> 'T' and abs(bias) <= flagpercent then 1 else null end unflagged,
        isdeactivated, isdeactivated_init,
        sampleID_init, sampleresultID_init, 
        runid_init, name_init,
        rep_init, critorder,
        conc, conc_init, bias,
        case when ISDEACTIVATED='T' then null else conc end useconc,
        case when ISDEACTIVATED='T' or ISDEACTIVATED_init='T' then null else bias end usebias,
        case when ISDEACTIVATED_init='T' then null else conc end useconc_init,      
        min(case when ISDEACTIVATED='T' or ISDEACTIVATED_init='T' then null 
                else bias end) over (partition by analyteid, analyteorder, species, matrix) minbias_as,
        max(case when ISDEACTIVATED='T' or ISDEACTIVATED_init='T' then null 
                else bias end) over (partition by analyteid, analyteorder, species, matrix) maxbias_as                
     FROM  
     (    
     SELECT r.runid wtrunid, rl.runid wtrunid_init, GETAUTOSAMPTIME(s.runcode,s.studycode) autosampletime,
        K.ID knownID, s.ID sampleID, sr.ID sampleresultID, 
        S.NAME, &&ConcRepresentationFunc&(K.concentration,&&ConcFigures&) nominalconc, s.runid runid, 
        ra.analyteid, sa.analyteorder, a.species, a.matrix, s.replicatenumber rep,
        s.runsamplesequencenumber, k.flagpercent, 
        s.isdeactivated, sl.isdeactivated isdeactivated_init,
        sl.ID sampleID_init, srl.ID sampleresultID_init, 
        sl.runid runid_init, sl.name name_init,
        sl.replicatenumber rep_init, kwz.ordernumber critorder,
        case when '&&FinalAccuracy&'='T' then sr.concentrationconv
             else &&ConcRepresentationFunc&(sr.concentrationconv,&&ConcFigures&)
        end conc, 
        case when '&&FinalAccuracy&'='T' then srl.concentrationconv
             else &&ConcRepresentationFunc&(srl.concentrationconv,&&ConcFigures&)
        end conc_init,
        case when srl.concentrationconv = 0 then null
             else case when '&&FinalAccuracy&'='T' then (sr.concentrationconv-srl.concentrationconv)*100/srl.concentrationconv
                       else round((&&ConcRepresentationFunc&(sr.concentrationconv,&&ConcFigures&)-
                                   &&ConcRepresentationFunc&(srl.concentrationconv,&&ConcFigures&))*100
                                  /&&ConcRepresentationFunc&(srl.concentrationconv,&&ConcFigures&),&&DecPlPercBias&) 
                  end
        end bias
      FROM &&TempTabPrefix&bio$run$ana$known K,
           &&TempTabPrefix&bio$run r,
           &&TempTabPrefix&bio$run$analytes ra,
           &&TempTabPrefix&bio$study$analytes sa,
           &&TempTabPrefix&bio$assay a,
           &&TempTabPrefix&bio$run$samples s,
           &&TempTabPrefix&bio$run$sample$results sr,
           table(&&LALPackage&.GetRunStates) rs,
           isr$crit kwz,
           &&TempTabPrefix&bio$assay$ana$known Kl,
           &&TempTabPrefix&bio$run rl,
           &&TempTabPrefix&bio$run$analytes ral,
           &&TempTabPrefix&bio$study$analytes sal,
           &&TempTabPrefix&bio$assay al,
           &&TempTabPrefix&bio$run$samples sl,
           &&TempTabPrefix&bio$run$sample$results srl,
           table(&&LALPackage&.GetRunStates) rsl,
           isr$crit kwzl           
      WHERE K.ID=s.runknownID
        AND R.ID = s.runid AND R.ID = sr.runid
        --AND r.studyassayid =s.studyassayid AND s.studyassayid = sr.studyassayid      
        AND r.RuntypeDescription in (&&RunTypes&)
        AND s.ID=sr.runsampleID        
        AND s.sampletype='known'
        AND k.knowntype = s.runsamplekind
        AND kwz.masterkey = '&&ExpMasterkey&'
        AND regexp_like(r.name, 'Autosampler', 'i') &&AutosamplerCondition&   
        AND ra.id = sr.runanalyteid
        AND a.id = r.assayid
        AND sa.id = ra.analyteid
        AND ra.runid = s.runid  
        AND K.studyid = '&&StudyID&' AND s.studyid = '&&StudyID&' 
        AND sr.studyID = '&&StudyID&' AND r.studyid = '&&StudyID&'
        AND ra.studyID = '&&StudyID&'
        AND kwz.key=K.NAMEPREFIX
        AND kwz.entity = '&&KnownEntity&'
        AND kwz.repid=&&RepID&   
        AND Kl.ID=sl.knownID
        AND Rl.ID = sl.runid AND Rl.ID = srl.runid
        --AND rl.studyassayid =sl.studyassayid AND sl.studyassayid = srl.studyassayid      
        AND rl.RuntypeDescription in (&&RunTypes&)
        AND sl.ID=srl.runsampleID        
        AND sl.sampletype='known'
        AND k.knowntype = sl.runsamplekind
        AND kwzl.masterkey = '&&ExpMasterkey&'
        AND ral.id = srl.runanalyteid
        AND al.id = rl.assayid
        AND sal.id = ral.analyteid
        AND ral.runid = sl.runid  
        AND Kl.studyid = '&&StudyID&' AND sl.studyid = '&&StudyID&' 
        AND srl.studyID = '&&StudyID&' AND rl.studyid = '&&StudyID&'
        AND ral.studyID = '&&StudyID&'
        AND kwzl.key=Kl.NAME
        AND kwzl.entity = '&&KnownEntity&'
        AND kwzl.repid=&&RepID&
        AND a.id = al.id
        AND sa.id = sal.id
        AND sr.runanalyteid = srl.runanalyteid
        AND s.replicatenumber = sl.replicatenumber
        AND r.studyassayid = rl.studyassayid 
        AND rs.column_value = r.runstatusnum
        AND rsl.column_value = rl.runstatusnum
        &&AddCondition& 
     ))
     GROUP BY analyteid, species, matrix, runid)
     GROUP BY analyteid, species, matrix
     ))
     from dual