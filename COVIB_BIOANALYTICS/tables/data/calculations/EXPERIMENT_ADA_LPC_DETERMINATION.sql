SELECT XMLELEMENT("experiment", XMLATTRIBUTES('&&Experiment&' AS "name" ,'responsesample' AS "type"), --EXPERIMENT_ADA_LPC_DETERMINATION
        Xmlagg (XMLConcat(Xmlagg (XMLELEMENT("calculation", xmlconcat(targ,val,res)) order by studyid, acceptedrun, analyteid, species, matrix, conc_unit, nominalconc desc, dilution),
          XMLELEMENT("statistic",
            XMLATTRIBUTES(analyteid AS "analyteid", species AS "species", matrix AS "matrix", acceptedrun as "acceptedrun"),
            XMLELEMENT("screen-acc-mean", &&FormatFuncConc&(&&RepresentationFuncConc&(min(mean_fc), &&PlacesConc&), &&PlacesConc&)),
            XMLELEMENT("screen-acc-sd", &&FormatFuncConcSD&(&&RepresentationFuncConcSD&(min(sd_fc), &&PlacesConcSD&), &&PlacesConcSD&)),
            XMLELEMENT("screen-acc-cv", FormatRounded(round(
              case
                when min(mean_fc) = 0 then null
                else min(sd_fc) / min(mean_fc) * 100
              end
            , &&DecPlPercCV&), &&DecPlPercCV&)),
            XMLELEMENT("titer-acc-mean", &&FormatFuncConc&(&&RepresentationFuncConc&(min(mean_titer_fc), &&PlacesConc&), &&PlacesConc&)),
            XMLELEMENT("titer-acc-sd", &&FormatFuncConcSD&(&&RepresentationFuncConcSD&(min(sd_titer_fc), &&PlacesConcSD&), &&PlacesConcSD&)),
            XMLELEMENT("titer-acc-cv", FormatRounded(round(
              case
                when min(mean_titer_fc) = 0 then null
                else min(sd_titer_fc) / min(mean_titer_fc) * 100
              end, &&DecPlPercCV&), &&DecPlPercCV&)),
            XMLELEMENT("confirm-acc-mean", &&FormatFuncConc&(&&RepresentationFuncConc&(min(mean_confirm_fc), &&PlacesConc&), &&PlacesConc&)),
            XMLELEMENT("confirm-acc-sd", &&FormatFuncConcSD&(&&RepresentationFuncConcSD&(min(sd_confirm_fc), &&PlacesConcSD&), &&PlacesConcSD&)),
            XMLELEMENT("confirm-acc-cv", FormatRounded(round(
              case
                when min(mean_confirm_fc) = 0 then null
                else min(sd_confirm_fc) / min(mean_confirm_fc) * 100
              end
            , &&DecPlPercCV&), &&DecPlPercCV&)),
            XMLELEMENT("screen_n", screen_cnt), XMLELEMENT("screen_df", screen_tv.df), XMLELEMENT("screen_t001", FormatRounded(round(screen_tv.t001, 3), 3)), XMLELEMENT("screen_t005", FormatRounded(round(screen_tv.t005, 3), 3)),
            XMLELEMENT("titer_n", titer_cnt), XMLELEMENT("titer_df", titer_tv.df), XMLELEMENT("titer_t001", FormatRounded(round(titer_tv.t001, 3), 3)), XMLELEMENT("titer_t005", FormatRounded(round(titer_tv.t005, 3), 3)),
            XMLELEMENT("confirm_n", confirm_cnt), XMLELEMENT("confirm_df", confirm_tv.df), XMLELEMENT("confirm_t001", FormatRounded(round(confirm_tv.t001, 3), 3)), XMLELEMENT("confirm_t005", FormatRounded(round(confirm_tv.t005, 3), 3)),
            XMLELEMENT("screen-acc-lpcconc", &&FormatFuncConc&(&&RepresentationFuncConc&( min(mean_fc) + ( round(screen_tv.t001, 3) * min(sd_fc) ), &&PlacesConc&), &&PlacesConc&)),
            XMLELEMENT("screen-acc-assaysens", &&FormatFuncConc&(&&RepresentationFuncConc&( min(mean_fc) + ( round(screen_tv.t005, 3) * min(sd_fc) ), &&PlacesConc&), &&PlacesConc&)),
            XMLELEMENT("screen-acd-median", &&FormatFuncMedianTargetTiter&(&&RepresentationFuncMedianTargetTiter&(min(median_acd), &&PlacesMedianTargetTiter&), &&PlacesMedianTargetTiter&)),
            XMLELEMENT("confirm-acc-lpcconc", &&FormatFuncConc&(&&RepresentationFuncConc&( min(mean_confirm_fc) + ( round(confirm_tv.t001, 3) * min(sd_confirm_fc) ), &&PlacesConc&), &&PlacesConc&)),
            XMLELEMENT("confirm-acc-assaysens", &&FormatFuncConc&(&&RepresentationFuncConc&( min(mean_confirm_fc) + ( round(confirm_tv.t005, 3) * min(sd_confirm_fc) ), &&PlacesConc&), &&PlacesConc&)),
            XMLELEMENT("titer-acc-lpcconc", &&FormatFuncConc&(&&RepresentationFuncConc&( min(mean_titer_fc) + ( round(titer_tv.t001, 3) * min(sd_titer_fc) ), &&PlacesConc&), &&PlacesConc&)),
            XMLELEMENT("titer-acc-assaysens", &&FormatFuncConc&(&&RepresentationFuncConc&( min(mean_titer_fc) + ( round(titer_tv.t005, 3) * min(sd_titer_fc) ), &&PlacesConc&), &&PlacesConc&)),
            XMLELEMENT("titer-acd-median", &&FormatFuncMedianTargetTiter&(&&RepresentationFuncMedianTargetTiter&(min(median_titer_acd), &&PlacesMedianTargetTiter&), &&PlacesMedianTargetTiter&))
          )
        ) order by studyid, acceptedrun, analyteid, species, matrix)
       )
  FROM (
  SELECT
    studyid, analyteid, species, matrix, acceptedrun, conc_unit, nominalconc, dilution,
    count(case when screen_fc is not null then '1' else null end) screen_cnt,
    count(case when titer_fc is not null then '1' else null end) titer_cnt,
    count(case when confirm_fc is not null then '1' else null end) confirm_cnt,
    Avg(screen_fc) mean_fc, Stddev(screen_fc) sd_fc,
    Median(above_cp_dil) median_acd,
    Avg(titer_fc) mean_titer_fc, Stddev(titer_fc) sd_titer_fc,
    Median(ge_titer_cp_dil) median_titer_acd,
    Avg(confirm_fc) mean_confirm_fc, Stddev(confirm_fc) sd_confirm_fc,
    XMLELEMENT("target",
      XMLELEMENT("studyid", studyid), XMLELEMENT("analyteid", analyteid), XMLELEMENT("species", species), XMLELEMENT("matrix", matrix), XMLELEMENT("nominalconc", &&FormatFuncConc&(&&RepresentationFuncConc&(nominalconc, &&PlacesConc&), &&PlacesConc&)), XMLELEMENT("acceptedrun", acceptedrun), XMLELEMENT("dilution", dilution), XMLELEMENT("conc-unit", conc_unit)
    ) as targ,
    XMLELEMENT("values",
      Xmlagg(
        XMLELEMENT("sample",
          XMLELEMENT("runid", rid), XMLELEMENT("runno", runid), XMLELEMENT("subject", subject), XMLELEMENT("sample1", s1_xml), XMLELEMENT("sample2", s2_xml), XMLELEMENT("nc", nc_xml),
          XMLELEMENT("s1-mean", FormatRounded(round(s1_mean_useresponse, &&DecPlResponse&), &&DecPlResponse&)), XMLELEMENT("s2-mean", FormatRounded(round(s2_mean_useresponse, &&DecPlResponse&), &&DecPlResponse&)), XMLELEMENT("nc-meanmedian", FormatRounded(round(nc_meanmedian_useresponse, &&DecPlResponse&), &&DecPlResponse&)),
          XMLELEMENT("screen-cp", FormatRounded(round(screen_cp, &&DecPlCutPoint&), &&DecPlCutPoint&)), XMLELEMENT("confirm-cp", FormatRounded(round(confirm_cp, &&DecPlConfirmCutPoint&), &&DecPlConfirmCutPoint&)), XMLELEMENT("titer-cp", FormatRounded(round(titer_cp, &&DecPlCutPoint&), &&DecPlCutPoint&)),
          XMLELEMENT("below-cp", below_cp), XMLELEMENT("above-cp", above_cp), XMLELEMENT("above-cp-dil", above_cp_dil), XMLELEMENT("above-titer-cp-dil", ge_titer_cp_dil),
          XMLELEMENT("at-cutpoint-conc", &&FormatFuncConc&(&&RepresentationFuncConc&(screen_fc, &&PlacesConc&), &&PlacesConc&) ),
          XMLELEMENT("at-titer-cutpoint-dil", &&FormatFuncConc&(&&RepresentationFuncConc&(titer_fc, &&PlacesConc&), &&PlacesConc&) ),
          XMLELEMENT("at-confirm-cutpoint-conc", &&FormatFuncConc&(&&RepresentationFuncConc&(confirm_fc, &&PlacesConc&), &&PlacesConc&) ),
          XMLELEMENT("n", s1_n+s2_n), XMLELEMENT("s1-n", s1_n), XMLELEMENT("s2-n", s2_n), XMLELEMENT("nc-n", nc_n),
          XMLELEMENT("m", s1_m+s2_m), XMLELEMENT("s1-m", s1_m), XMLELEMENT("s2-m", s2_m), XMLELEMENT("nc-m", nc_m),    
          XMLELEMENT("s1-cv", FormatRounded(round(s1_cv, &&DecPlPercCV&), &&DecPlPercCV&)),
          XMLELEMENT("s2-cv", FormatRounded(round(s2_cv, &&DecPlPercCV&), &&DecPlPercCV&)),
          XMLELEMENT("nc-cv", FormatRounded(round(nc_cv, &&DecPlPercCV&), &&DecPlPercCV&)),
          XMLELEMENT("s1-cvflag", case when s1_cv > &&PrecisionAcceptanceCriteria& then 'T' else 'F' end),
          XMLELEMENT("s2-cvflag", case when s2_cv > &&PrecisionAcceptanceCriteria& then 'T' else 'F' end),
          XMLELEMENT("nc-cvflag", case when nc_cv > &&PrecisionAcceptanceCriteria& then 'T' else 'F' end),
          XMLELEMENT("difference", FormatRounded(round(
            case
              when s1_mean_useresponse = 0 then null
              else (s1_mean_useresponse-s2_mean_useresponse)/s1_mean_useresponse*100
            end
          , &&DecPlInhibition&), &&DecPlInhibition&)),
          XMLELEMENT("inhibition", FormatRounded(round(abs(
            case
              when s1_mean_useresponse = 0 then null
              else (s1_mean_useresponse-s2_mean_useresponse)/s1_mean_useresponse*100
            end
          ), &&DecPlInhibition&), &&DecPlInhibition&))
        ) order by runid, subject
      )
    ) as val,
    XMLELEMENT("result", 
      XMLELEMENT("s1-mean", FormatRounded(round(Avg(s1_mean_useresponse), &&DecPlResponse&), &&DecPlResponse&)),
      XMLELEMENT("s2-mean", FormatRounded(round(Avg(s2_mean_useresponse), &&DecPlResponse&), &&DecPlResponse&)),
      XMLELEMENT("difference", FormatRounded(round(case
        when Avg(s2_mean_useresponse) = 0 then null
        else (Avg(s1_mean_useresponse) - Avg(s2_mean_useresponse)) / Avg(s1_mean_useresponse) * 100
      end, &&DecPlInhibition&), &&DecPlInhibition&)),
      XMLELEMENT("inhibition", FormatRounded(round(abs(case
        when Avg(s1_mean_useresponse) = 0 then null
        else (Avg(s1_mean_useresponse) - Avg(s2_mean_useresponse)) / Avg(s1_mean_useresponse) * 100
      end), &&DecPlInhibition&), &&DecPlInhibition&))
    ) as res
  FROM (
    SELECT
      studyid, analyteid, rid, runid, acceptedrun, subject, nominalconc, dilution, species, matrix,
      screen_cp, titer_cp, confirm_cp,
      below_cp, above_cp, conc_unit, below_titer_cp, above_titer_cp, ge_titer_cp, below_confirm_cp, above_confirm_cp,
      s1_mean_response, s1_mean_useresponse, s1_n, s1_m, s1_sd, s1_xml,
      case
        when s1_mean_useresponse = 0 then null
        else s1_sd / s1_mean_useresponse * 100
      end s1_cv,
      s2_mean_response, s2_mean_useresponse, s2_n, s2_m, s2_sd, s2_xml,
      case
        when s2_mean_useresponse = 0 then null
        else s2_sd / s2_mean_useresponse * 100
      end s2_cv,
      nc_meanmedian_response, nc_meanmedian_useresponse, nc_n, nc_m, nc_sd, nc_xml,
      case
        when nc_meanmedian_useresponse = 0 then null
        else nc_sd / nc_meanmedian_useresponse * 100
      end nc_cv,
      below_cp_dil, above_cp_dil, below_cp_conc, above_cp_conc, ge_titer_cp_dil, below_titer_cp_dil, above_titer_cp_dil, below_confirm_cp_conc, above_confirm_cp_conc,
      ISR$COVIB$PACKAGE.foreCast(screen_cp, SYS.ODCINUMBERLIST(below_cp_conc, above_cp_conc), SYS.ODCINUMBERLIST(below_cp,above_cp)) screen_fc,
      ISR$COVIB$PACKAGE.foreCast(titer_cp, SYS.ODCINUMBERLIST(below_titer_cp_dil, above_titer_cp_dil), SYS.ODCINUMBERLIST(below_titer_cp,above_titer_cp)) titer_fc,
      ISR$COVIB$PACKAGE.foreCast(confirm_cp, SYS.ODCINUMBERLIST(below_confirm_cp_conc, above_confirm_cp_conc), SYS.ODCINUMBERLIST(below_confirm_cp,above_confirm_cp)) confirm_fc
    FROM (
    SELECT
      studyid, analyteid, rid, runid, acceptedrun, subject, nominalconc, dilution, species, matrix, screen_cp, titer_cp, confirm_cp, below_cp, above_cp, conc_unit, below_titer_cp, above_titer_cp, ge_titer_cp, below_confirm_cp, above_confirm_cp,
      s1_mean_response, s1_mean_useresponse, s1_n, s1_m, s1_sd, s1_xml,
      s2_mean_response, s2_mean_useresponse, s2_n, s2_m, s2_sd, s2_xml,
      nc_meanmedian_response, nc_meanmedian_useresponse, nc_n, nc_m, nc_sd, nc_xml,
      max(case when s1_mean_useresponse = below_cp then dilution else null end) over (partition by studyid, analyteid, species, matrix, subject, rid, runid, acceptedrun) below_cp_dil,
      max(case when s1_mean_useresponse = above_cp then dilution else null end) over (partition by studyid, analyteid, species, matrix, subject, rid, runid, acceptedrun) above_cp_dil,
      max(case when s1_mean_useresponse = below_cp then nominalconc else null end) over (partition by studyid, analyteid, species, matrix, subject, rid, runid, acceptedrun) below_cp_conc,
      max(case when s1_mean_useresponse = above_cp then nominalconc else null end) over (partition by studyid, analyteid, species, matrix, subject, rid, runid, acceptedrun) above_cp_conc,
      max(case when s1_mean_useresponse = ge_titer_cp then dilution else null end) over (partition by studyid, analyteid, species, matrix, subject, rid, runid, acceptedrun) ge_titer_cp_dil,
      max(case when s1_mean_useresponse = below_titer_cp then dilution else null end) over (partition by studyid, analyteid, species, matrix, subject, rid, runid, acceptedrun) below_titer_cp_dil,
      max(case when s1_mean_useresponse = above_titer_cp then dilution else null end) over (partition by studyid, analyteid, species, matrix, subject, rid, runid, acceptedrun) above_titer_cp_dil,
      max(case when
        case
          when s1_mean_useresponse = 0 then null
          else (s1_mean_useresponse-s2_mean_useresponse)/s1_mean_useresponse*100
        end = below_confirm_cp then nominalconc
      else null end) over (partition by studyid, analyteid, species, matrix, subject, rid, runid, acceptedrun) below_confirm_cp_conc,
      max(case when
        case
          when s1_mean_useresponse = 0 then null
          else (s1_mean_useresponse-s2_mean_useresponse)/s1_mean_useresponse*100
        end = above_confirm_cp then nominalconc
      else null end) over (partition by studyid, analyteid, species, matrix, subject, rid, runid, acceptedrun) above_confirm_cp_conc
    FROM (
      SELECT
        sample1.studyid, sample1.analyteid, sample1.rid, sample1.runid, sample1.acceptedrun, sample1.subject, sample1.nominalconc, sample1.dilution, sample1.conc_unit, sample1.species, sample1.matrix, nc.screen_cp, nc.titer_cp, nc.confirm_cp,
        max(case when sample1.mean_useresponse < nc.screen_cp then sample1.mean_useresponse else null end) over (partition by sample1.studyid, sample1.analyteid, sample1.species, sample1.matrix, sample1.subject, sample1.rid, sample1.runid, sample1.acceptedrun) below_cp,
        min(case when sample1.mean_useresponse >= nc.screen_cp then sample1.mean_useresponse else null end) over (partition by sample1.studyid, sample1.analyteid, sample1.species, sample1.matrix, sample1.subject, sample1.rid, sample1.runid, sample1.acceptedrun) above_cp,
        max(case when sample1.mean_useresponse < nc.titer_cp then sample1.mean_useresponse else null end) over (partition by sample1.studyid, sample1.analyteid, sample1.species, sample1.matrix, sample1.subject, sample1.rid, sample1.runid, sample1.acceptedrun) below_titer_cp,
        min(case when sample1.mean_useresponse >= nc.titer_cp then sample1.mean_useresponse else null end) over (partition by sample1.studyid, sample1.analyteid, sample1.species, sample1.matrix, sample1.subject, sample1.rid, sample1.runid, sample1.acceptedrun) above_titer_cp,
        min(case when sample1.mean_useresponse >= nc.titer_cp then sample1.mean_useresponse else null end) over (partition by sample1.studyid, sample1.analyteid, sample1.species, sample1.matrix, sample1.subject, sample1.rid, sample1.runid, sample1.acceptedrun) ge_titer_cp,
        max(case when
          case
            when sample1.mean_useresponse = 0 then null
            else (sample1.mean_useresponse-sample2.mean_useresponse)/sample1.mean_useresponse*100
          end <= nc.confirm_cp then case when sample1.mean_useresponse = 0 then null else (sample1.mean_useresponse-sample2.mean_useresponse)/sample1.mean_useresponse*100 end
        else null end) over (partition by sample1.studyid, sample1.analyteid, sample1.species, sample1.matrix, sample1.subject, sample1.rid, sample1.runid) below_confirm_cp,
        min(case when
          case
            when sample1.mean_useresponse = 0 then null
            else (sample1.mean_useresponse-sample2.mean_useresponse)/sample1.mean_useresponse*100
          end > nc.confirm_cp then case when sample1.mean_useresponse = 0 then null else (sample1.mean_useresponse-sample2.mean_useresponse)/sample1.mean_useresponse*100 end
        else null end) over (partition by sample1.studyid, sample1.analyteid, sample1.species, sample1.matrix, sample1.subject, sample1.rid, sample1.runid) above_confirm_cp,
        sample1.mean_response s1_mean_response, sample1.mean_useresponse s1_mean_useresponse, sample1.n s1_n, sample1.m s1_m, sample1.sd s1_sd, sample1.xml s1_xml,
        sample2.mean_response s2_mean_response, sample2.mean_useresponse s2_mean_useresponse, sample2.n s2_n, sample2.m s2_m, sample2.sd s2_sd, sample2.xml s2_xml,
        NC.meanmedian_response nc_meanmedian_response, NC.meanmedian_useresponse nc_meanmedian_useresponse, NC.n nc_n, NC.m nc_m, NC.sd nc_sd, NC.xml nc_xml
      FROM
        (
          SELECT
            studyid, analyteid, rid, runid, acceptedrun, subject, nominalconc, dilution, species, matrix, concentrationunit conc_unit,
            Avg(response) mean_response, Avg(case when(isdeactivated='F') then response else null end) mean_useresponse,
            Stddev(case when(isdeactivated='F') then response else null end) sd,
            count(case when(isdeactivated='F') then 1 else null end) n, count(1) m,
            Xmlagg(
              XMLELEMENT("sample",
                XMLATTRIBUTES(sampleid AS "sampleid", sampleresultid AS "sampleresultid" ),
                XMLELEMENT("runsamplesequencenumber", runsamplesequencenumber),
                XMLELEMENT("samplename", samplename), XMLELEMENT("samplesubtype", samplesubtype),
                XMLELEMENT("response", FormatRounded(round(response, &&DecPlResponse&), &&DecPlResponse&)),
                XMLELEMENT("status", resulttext), XMLELEMENT("deactivated", isdeactivated), XMLELEMENT("reason", reason)
              ) order by runsamplesequencenumber
            ) xml
          FROM (
            SELECT
              r.studyid, s.id sampleid, sr.id sampleresultid, s.designsampleid, s.samplename, to_number(regexp_replace(s.samplename, '^\D+(\d+).*$', '\1', 1, 1, 'i')) subject,
              r.id rid, r.runid, case when ra.RunAnalyteRegressionStatus = &&AcceptedRunAnalyte& then 'T' else 'F' end acceptedrun, sa.id analyteid, s.samplesubtype, s.runsamplesequencenumber, sr.analytearea response, s.status, sr.commenttext, sr.concentration conc,
              sr.resulttext, s.source, d.reason, k.concentration nominalconc, s.dilution, sr.concentrationunits concentrationunit, a.species, a.sampletypeid matrix,
              case
                when d.code is not null then 'T'
                else 'F'
              end isdeactivated
            FROM 
              &&TempTabPrefix&bio$run r, &&TempTabPrefix&bio$run$analytes ra, &&TempTabPrefix&bio$study$analytes sa,
              &&TempTabPrefix&bio$assay a, &&TempTabPrefix&bio$run$worklist s, &&TempTabPrefix&bio$run$worklist$result$rw sr,
              &&TempTabPrefix&bio$run$ana$known k, &&TempTabPrefix&bio$covib$run$sample$deact d, isr$crit kwz
            WHERE r.ID = s.runID AND s.ID = sr.worklistID AND a.ID = r.assayID
            AND r.studyID = '&&StudyID&' AND a.studyID = '&&StudyID&'
            AND sr.runAnalyteCode = ra.code AND r.ID = ra.runID AND ra.analyteID = sa.ID
            AND s.code = d.code (+) AND k.runAnalyteCode = ra.code AND k.name = s.sampleName
            AND kwz.key = s.samplename AND kwz.entity = '&&KnownEntity&' AND kwz.masterkey = '&&ExpMasterkey&' AND kwz.repid = &&RepID&
            &&AddConditionS1&
          )
          GROUP BY studyid, analyteid, species, matrix, rid, runid, acceptedrun, subject, nominalconc, dilution, concentrationunit
        ) sample1, (
          SELECT
            studyid, analyteid, rid, runid, subject, nominalconc, dilution, species, matrix,
            Avg(response) mean_response, Avg(case when(isdeactivated='F') then response else null end) mean_useresponse,
            Stddev(case when(isdeactivated='F') then response else null end) sd,
            count(case when(isdeactivated='F') then 1 else null end) n, count(1) m,
            Xmlagg(
              XMLELEMENT("sample",
                XMLATTRIBUTES(sampleid AS "sampleid", sampleresultid AS "sampleresultid" ),
                XMLELEMENT("runsamplesequencenumber", runsamplesequencenumber),
                XMLELEMENT("samplename", samplename), XMLELEMENT("samplesubtype", samplesubtype),
                XMLELEMENT("response", FormatRounded(round(response, &&DecPlResponse&), &&DecPlResponse&)),
                XMLELEMENT("status", resulttext), XMLELEMENT("deactivated", isdeactivated), XMLELEMENT("reason", reason)
              ) order by runsamplesequencenumber
            ) xml
          FROM (
            SELECT
              r.studyid, s.id sampleid, sr.id sampleresultid, s.designsampleid, s.samplename, to_number(regexp_replace(s.samplename, '^\D+(\d+).*$', '\1', 1, 1, 'i')) subject,
              r.id rid, r.runid, sa.id analyteid, s.samplesubtype, s.runsamplesequencenumber, sr.analytearea response, s.status, sr.commenttext, sr.concentration conc,
              sr.resulttext, s.source, d.reason, k.concentration nominalconc, s.dilution, sr.concentrationunits concentrationunit, a.species, a.sampletypeid matrix,
              case when d.code is not null then 'T' else 'F' end isdeactivated
            FROM 
              &&TempTabPrefix&bio$run r, &&TempTabPrefix&bio$run$analytes ra, &&TempTabPrefix&bio$study$analytes sa,
              &&TempTabPrefix&bio$assay a, &&TempTabPrefix&bio$run$worklist s, &&TempTabPrefix&bio$run$worklist$result$rw sr,
              &&TempTabPrefix&bio$run$ana$known k, &&TempTabPrefix&bio$covib$run$sample$deact d, isr$crit kwz
            WHERE r.ID = s.runID AND s.ID = sr.worklistID AND a.ID = r.assayID
            AND r.studyID = '&&StudyID&' AND a.studyID = '&&StudyID&'
            AND sr.runAnalyteCode = ra.code AND r.ID = ra.runID AND ra.analyteID = sa.ID
            AND s.code = d.code (+) AND k.runAnalyteCode = ra.code AND k.name = s.sampleName
            AND kwz.key = s.samplename AND kwz.entity = '&&KnownEntity&' AND kwz.masterkey = '&&ExpMasterkey&' AND kwz.repid = &&RepID&
            &&AddConditionS2&
          )
          GROUP BY studyid, analyteid, species, matrix, rid, runid, subject, nominalconc, dilution
        ) sample2, 
        (
          SELECT
            studyid, analyteid, rid, runid, species, matrix,
            &&NCMeanOrMedian&(
              case when stb$util.GetReportParameterValue('MEAN_OR_MEDIAN_NC',&&RepID&) = 'meanofmeans' then case when replicatenumber = 1 then meanresponse else null end
              else response end
            ) meanmedian_response, &&NCMeanOrMedian&(
              case when stb$util.GetReportParameterValue('MEAN_OR_MEDIAN_NC',&&RepID&) = 'meanofmeans' then case when replicatenumber = 1 then meanuseresponse else null end
              else case when(isdeactivated='F') then response else null end end
            ) meanmedian_useresponse,
            case when regexp_like('&&FixedScreenCutpoint&', '^\-?(\d+)?\.?\d+$') then to_number('&&FixedScreenCutpoint&') when '&&ScreenCPNFactor&' is not null then &&NCMeanOrMedian&(
                case when stb$util.GetReportParameterValue('MEAN_OR_MEDIAN_NC',&&RepID&) = 'meanofmeans' then case when replicatenumber = 1 then meanuseresponse else null end
                else case when(isdeactivated='F') then response else null end end
              )*to_number('&&ScreenCPNFactor&')
            else &&NCMeanOrMedian&(
                case when stb$util.GetReportParameterValue('MEAN_OR_MEDIAN_NC',&&RepID&) = 'meanofmeans' then case when replicatenumber = 1 then meanuseresponse else null end
                else case when(isdeactivated='F') then response else null end end
            ) end screen_cp,
            case when '&&FixedTiterCutpoint&' is not null then to_number('&&FixedTiterCutpoint&') when '&&TiterCPNFactor&' is not null then &&NCMeanOrMedian&(
              case when stb$util.GetReportParameterValue('MEAN_OR_MEDIAN_NC',&&RepID&) = 'meanofmeans' then case when replicatenumber = 1 then meanuseresponse else null end
              else case when(isdeactivated='F') then response else null end end
            )*to_number('&&TiterCPNFactor&')
            else null end titer_cp,
            case when regexp_like(ccp.key, '^\-?(\d+)?\.?\d+$') then to_number(ccp.key) else null end confirm_cp,
            Stddev(
              case when stb$util.GetReportParameterValue('MEAN_OR_MEDIAN_NC',&&RepID&) = 'meanofmeans' then case when replicatenumber = 1 then meanuseresponse else null end
              else case when(isdeactivated='F') then response else null end end
            ) sd,
            count(
              case when stb$util.GetReportParameterValue('MEAN_OR_MEDIAN_NC',&&RepID&) = 'meanofmeans' then case when replicatenumber = 1 then 1 else null end
              else case when(isdeactivated='F') then 1 else null end end
            ) n, count(1) m,
            Xmlagg(
              XMLELEMENT("sample",
                XMLATTRIBUTES(sampleid AS "sampleid", sampleresultid AS "sampleresultid" ), XMLELEMENT("runsamplesequencenumber", runsamplesequencenumber), XMLELEMENT("samplename", samplename), XMLELEMENT("samplesubtype", samplesubtype),
                XMLELEMENT("response", FormatRounded(round(response, &&DecPlResponse&), &&DecPlResponse&)), XMLELEMENT("status", resulttext), XMLELEMENT("deactivated", isdeactivated), XMLELEMENT("reason", reason)
              ) order by runsamplesequencenumber
            ) xml
          FROM (
            SELECT
              r.studyid, s.id sampleid, sr.id sampleresultid, s.designsampleid, s.samplename, s.replicatenumber, d.reason, r.id rid, r.runid, sa.id analyteid, s.samplesubtype, s.runsamplesequencenumber, sr.analytearea response,
              s.status, sr.commenttext, sr.concentration conc, sr.resulttext, s.source, s.concentration nominalconc, sr.concentrationunits concentrationunit, a.species, a.sampletypeid matrix,
              Avg(case when(d.code is null) then sr.analytearea else null end) over (partition by r.studyid, sa.id, r.id, s.samplename) meanresponse, Avg(case when(d.code is null) then sr.analytearea else null end) over (partition by r.studyid, sa.id, r.id, s.samplename) meanuseresponse,
              case when d.code is not null then 'T' else 'F' end isdeactivated
            FROM 
              &&TempTabPrefix&bio$run r, &&TempTabPrefix&bio$run$analytes ra, &&TempTabPrefix&bio$study$analytes sa,
              &&TempTabPrefix&bio$assay a, &&TempTabPrefix&bio$run$worklist s, &&TempTabPrefix&bio$run$worklist$result$rw sr,
              &&TempTabPrefix&bio$covib$run$sample$deact d, isr$crit kwz
            WHERE r.ID = s.runID AND s.ID = sr.worklistID
            AND a.ID = r.assayID AND r.studyID = '&&StudyID&' AND a.studyID = '&&StudyID&'
            AND sr.runAnalyteCode = ra.code AND r.ID = ra.runID AND ra.analyteID = sa.ID
            AND s.code = d.code (+)
            AND kwz.key = s.samplename AND kwz.entity = '&&KnownEntity&' AND kwz.masterkey = '&&ExpMasterkey&' AND kwz.repid = &&RepID&
            &&AddConditionNC&
          ), isr$crit ccp
          WHERE ccp.entity = 'BIO$COVIB$CONFIRM$CUTPOINT' AND ccp.masterkey = 'NORM' AND ccp.repid = &&RepID&
          GROUP BY studyid, analyteid, species, matrix, rid, runid, ccp.key) NC
      WHERE sample1.studyID = sample2.studyID(+) AND sample1.rID = sample2.rID(+) AND sample1.runid = sample2.runid(+)
      AND sample1.analyteID = sample2.analyteID(+) AND sample1.species = sample2.species(+) AND sample1.matrix = sample2.matrix(+) AND sample1.subject = sample2.subject(+) AND sample1.nominalconc = sample2.nominalconc(+)
      AND sample1.studyID = NC.studyID(+) AND sample1.rID = NC.rID(+) AND sample1.runid = NC.runid(+) AND sample1.analyteID = NC.analyteID(+) AND sample1.species = NC.species(+) AND sample1.matrix = NC.matrix(+)
      &&AddCondition&
    )
 ))
 GROUP BY studyid, analyteid, species, matrix, nominalconc, acceptedrun, dilution, conc_unit),
 isr$tvalues screen_tv,isr$tvalues titer_tv,isr$tvalues confirm_tv
 WHERE screen_cnt = screen_tv.n(+) AND titer_cnt = titer_tv.n(+) AND confirm_cnt = confirm_tv.n(+)
GROUP BY studyid, analyteid, species, matrix, acceptedrun, screen_cnt, titer_cnt, confirm_cnt, screen_tv.n, screen_tv.df, screen_tv.t001, screen_tv.t005, titer_tv.n, titer_tv.df, titer_tv.t001, titer_tv.t005, confirm_tv.n, confirm_tv.df, confirm_tv.t001, confirm_tv.t005
