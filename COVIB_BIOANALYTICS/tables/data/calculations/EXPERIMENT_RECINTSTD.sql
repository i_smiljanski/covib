  SELECT  XMLELEMENT("experiment", XMLATTRIBUTES('&&Experiment&' AS "name" ,'recintstd' AS "type"),
   (
   select Xmlagg(XMLConcat(
     Xmlagg(XMLELEMENT("calculation", xmlconcat(targ,val,res))), 
     XMLELEMENT("statistic", XMLATTRIBUTES(internalstdname AS "internalstdname" ),
       XMLELEMENT("min-mean_recovery", min(mean_recovery)),
       XMLELEMENT("max-mean_recovery", max(mean_recovery)),
       XMLELEMENT("numtotal", sum(cnt))
       )))
  from ( 
  SELECT  
      internalstdname,
      Count(useistarea) cnt,
      FormatRounded(round(min(mean_extracted)/min(mean_unextracted)*100,&&DecPlPercBias&),&&DecPlPercBias&) mean_recovery,
      XMLELEMENT("target", 
        XMLELEMENT("runid", runid), 
        XMLELEMENT("internalstdname", internalstdname)
        ) targ,
      XMLELEMENT("values", Xmlagg(
        XMLELEMENT("sample",  XMLATTRIBUTES(sampleid AS "sampleid", sampleresultrawid AS "sampleresultrawid" ),
          XMLELEMENT("knownid", knownid), 
          XMLELEMENT("knownconc", &&ConcFormatFunc&(knownconc,&&ConcFigures&)),          
          XMLELEMENT("name", name),          
          XMLELEMENT("internalstandardarea", &&PeakFormatFunc&(&&PeakRepresentationFunc&(istarea,&&PeakFigures&),&&PeakFigures&)),
          XMLELEMENT("runsamplesequencenumber", runsamplesequencenumber),
          XMLELEMENT("replicatenumber", replicatenumber),
          XMLELEMENT("stabilitytype", stabilitytype),
          XMLELEMENT("mean", &&PeakFormatFunc&(&&PeakRepresentationFunc&(mean,&&PeakFigures&),&&PeakFigures&)),
          XMLELEMENT("deactivated", isdeactivated)                           
        )
        order by stabilitytype, name, runsamplesequencenumber)) val,
      XMLELEMENT("result",       
        XMLELEMENT("n", Count(useistarea)),
        XMLELEMENT("mean_extracted", &&PeakFormatFunc&(&&PeakRepresentationFunc&(min(mean_extracted),&&PeakFigures&),&&PeakFigures&)),
        XMLELEMENT("mean_unextracted", &&PeakFormatFunc&(&&PeakRepresentationFunc&(min(mean_unextracted),&&PeakFigures&),&&PeakFigures&)),
        XMLELEMENT("mean_recovery", FormatRounded(round(min(mean_extracted)/min(mean_unextracted)*100,&&DecPlPercBias&),&&DecPlPercBias&))  
        ) res
    --))
    FROM
    (SELECT distinct knownID, sampleID, sampleresultrawID, Name, knownconc, stabilitytype, runsamplesequencenumber,
     istarea, runid, internalstdname, replicatenumber, mean, ISDEACTIVATED, useistarea, 
     case when stabilitytype='Extracted' then mean else null end as mean_extracted,
     case when stabilitytype='Unextracted' then mean else null end as mean_unextracted       
     FROM   
    (SELECT knownID, sampleID, sampleresultrawID, Name, knownconc,   stabilitytype,  runsamplesequencenumber,
     istarea, runid, internalstdname, replicatenumber, ISDEACTIVATED, useistarea,
     case when '&&FinalAccuracy&'='T' then avg(useistarea) over (partition by runid, internalstdname, stabilitytype)
          else &&PeakRepresentationFunc&(avg(useistarea) over (partition by runid, internalstdname, stabilitytype),&&PeakFigures&) 
     end mean 
     FROM
     (SELECT K.ID knownID, s.ID sampleID, srw.ID sampleresultrawID,  s.runsamplesequencenumber,
        K.NAME, &&ConcRepresentationFunc&(K.concentration,&&ConcFigures&) knownconc, s.runid, s.ISDEACTIVATED,
        k.stabilitytype stabtype, case when regexp_like(K.NAME, '^&&Set1&') then 'Extracted' else 'Unextracted' end stabilitytype, 
        srw.internalstdname, s.replicatenumber, 
        k.assayanalyteid, min(k.assayanalyteid) over (partition by srw.internalstdname,s.runid) minassayanalyteid,
        case when '&&FinalAccuracy&'='T' then srw.internalstandardarea
             else &&PeakRepresentationFunc&(srw.internalstandardarea,&&PeakFigures&) 
        end istarea,
        case when s.ISDEACTIVATED='T' then null 
             else case when '&&FinalAccuracy&'='T' then srw.internalstandardarea
                       else &&PeakRepresentationFunc&(srw.internalstandardarea,&&PeakFigures&) 
                  end
        end useistarea
      FROM &&TempTabPrefix&wt$assay$ana$known K,    
           &&TempTabPrefix&wt$run r,  
           &&TempTabPrefix&wt$run$analytes ra,      
           &&TempTabPrefix&wt$run$samples s,
           &&TempTabPrefix&wt$run$sample$result$raw srw,
           isr$crit kwz,
           table(&&LALPackage&.GetRunStates) rs
      WHERE K.ID=s.knownID   
        AND R.ID = s.runid AND R.ID = srw.runid       
        AND r.studyassayid =s.studyassayid AND s.studyassayid = srw.studyassayid 
        AND K.ASSAYCODE = S.STUDYASSAYCODE   
        AND r.RuntypeDescription in (&&RunTypes&)                    
        AND s.ID=srw.runsampleID
        AND s.sampletype='known'
        AND K.knowntype = s.runsamplekind
        and kwz.masterkey = '&&ExpMasterkey&'
        AND ra.assayanalyteid = srw.assayanalyteid
        AND ra.runid = s.runid  
        AND ra.studyID = '&&StudyID&'
        AND K.studyid='&&StudyID&' AND s.studyid='&&StudyID&' 
        AND srw.studyID='&&StudyID&' AND r.studyid = '&&StudyID&'
        and kwz.key=K.NAMEPREFIX
        and kwz.entity = '&&KnownEntity&'
        and repid=&&RepID&
        AND rs.column_value=r.runstatusnum &&AddCondition&)
     WHERE assayanalyteid = minassayanalyteid)
    )
    GROUP BY runid, internalstdname)
    GROUP BY internalstdname)
    ) FROM dual