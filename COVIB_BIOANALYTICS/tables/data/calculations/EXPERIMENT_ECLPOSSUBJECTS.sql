  SELECT XMLELEMENT("experiment", XMLATTRIBUTES('&&Experiment&' AS "name" ,'eclsample' AS "type"),
      Xmlagg (XMLConcat(
          Xmlagg (XMLELEMENT("calculation", xmlconcat(targ,val,res)) order by studyid, analytename, dosegroupno, dosegroup, studyday, doseamount, doseunit), 
          XMLELEMENT("statistic",
            XMLATTRIBUTES(analytename AS "analytename"),
            XMLELEMENT("dosed_m", max(dosed)),
            XMLELEMENT("dosed_n", max(positive_dosed)),
            XMLELEMENT("difference", FormatRounded(round(
                          max(positive_dosed)/max(dosed)*100
                          , &&DecPlPercBias&), &&DecPlPercBias&))
          )
        ) order by studyid, analytename
      ))
  FROM (
  SELECT studyid, analytename, dosegroupno, dosegroup, studyday, doseamount, doseunit, max(all_dosed) dosed, max(all_positive_dosed) positive_dosed,
    XMLELEMENT("target",
      XMLELEMENT("dosegroup", dosegroup),      
      XMLELEMENT("analytename", analytename),
      XMLELEMENT("studyday", studyday),
      XMLELEMENT("timetext", timetext),
      XMLELEMENT("doseamount", FmtNum(doseamount)),
      XMLELEMENT("doseunit", doseunit)
    ) as targ,
    XMLELEMENT("values", 
      Xmlagg(
        XMLELEMENT("sample",
          XMLATTRIBUTES(sampleid AS "sampleid", sampleresultid AS "sampleresultid" ),
          XMLELEMENT("designsampleid", designSampleID),
          XMLELEMENT("subjectid", subjectid),
          XMLELEMENT("designsubjecttag", designsubjecttag),
          XMLELEMENT("time", time),
          XMLELEMENT("result", result),
          XMLELEMENT("runid", rid),
          XMLELEMENT("runno", runid)
        ) order by sampleid
      )
    ) as val,
    XMLELEMENT("result",
      XMLELEMENT("m", count(subjectid)),
      XMLELEMENT("n", count(positive)),
      XMLELEMENT("difference", FormatRounded(round(
                          count(positive)/count(subjectid)*100
                          , &&DecPlPercBias&), &&DecPlPercBias&))
    ) as res
  FROM (
      SELECT
        sam.studyid,
        sa.analyteorder,
        sa.id analyteid,
        &&AnalyteName& analytename,
        t.name dosegroup,
        case
          when REGEXP_LIKE(t.name, '^[+-]?\d?\.?\d+$','i') then
            to_number(t.name)
          else
            null
        end dosegroupno,
        t.doseamount,
        t.doseunitsdescription doseunit,
        sam.subjectid,
        s.id sampleid,
        sr.id sampleresultid,
        s.designsampleid,
        s.samplesubtype,
        sam.designsubjecttag,
        sr.commenttext,
        sr.concentrationstatus result,
        s.time,
        s.timetext,
        sam.studyday,
        r.id rid,
        r.runid,
        case
          when sr.concentrationstatus like 'Positive%' then
            sr.concentrationstatus
          else null
        end positive,
        count(distinct case
          when t.doseamount != 0 then
            sam.designsubjecttag
          else null
        end) over (partition by sa.name ) all_dosed,
        count(distinct case
          when sr.concentrationstatus like 'Positive%' and t.doseamount != 0 then
            sam.designsubjecttag
          else null
        end) over (partition by sa.name ) all_positive_dosed
      FROM 
        &&TempTabPrefix&bio$run r,
        &&TempTabPrefix&bio$run$analytes ra,
        &&TempTabPrefix&bio$study$analytes sa,
        &&TempTabPrefix&bio$assay a,
        &&TempTabPrefix&bio$run$worklist s,
        &&TempTabPrefix&bio$run$worklist$result$rw sr,
        &&TempTabPrefix&bio$study$treatment t,
        &&TempTabPrefix&bio$sample sam,
        table(&&LALPackage&.GetRunStates) rs
      WHERE r.ID = s.runID
      AND s.ID = sr.worklistID
      AND a.ID = r.assayID
      AND s.designSampleID = sam.ID
      AND r.studyid = '&&StudyID&'
      AND a.studyID = '&&StudyID&'
      AND sam.studyID = '&&StudyID&'
      AND rs.column_value = r.runstatusnum
      AND sr.runAnalyteCode = ra.code
      AND r.ID = ra.runID
      AND ra.analyteID = sa.ID
      AND sam.treatmentcode = t.code
      AND sr.concentrationstatus is not null
      AND s.runcode || '-' || s.runsamplesequencenumber || '-' || sa.analyteorder not in (select code from &&TempTabPrefix&bio$deactivated$samples)
      AND (
        r.code || '-' || s.runsamplesequencenumber || '-' || sa.analyteorder not in (select runsamplecode from &&TempTabPrefix&bio$reassay$history) 
        or r.code || '-' || s.runsamplesequencenumber || '-' || sa.analyteorder in (select runsamplecode from &&TempTabPrefix&bio$reassay$history where reported = 'Y')
      )
      AND s.samplesubtype like 'Screen%'
    &&AddCondition&
  )
    GROUP BY studyid, analytename, dosegroupno, dosegroup, studyday, timetext, doseamount, doseunit)
GROUP BY studyid, analytename

