SELECT XMLELEMENT("experiment", XMLATTRIBUTES('&&Experiment&' AS "name" ,'cutpoint' AS "type"),
      Xmlagg ( XMLConcat(
        Xmlagg (XMLELEMENT("calculation", xmlconcat(targ,val)) order by studyid, acceptedrun, analyteid, species, matrix, poporder, population)
        --XMLELEMENT("statistic", XMLATTRIBUTES(analyteid AS "analyteid"),
               
             --)) order by studyid, analyteid, species, matrix, hours, longtermunits, longtermtime, temperature
      )))
  FROM (
  SELECT
    studyid, analyteid, species, matrix, rid, runid, acceptedrun, population, poporder,
    --sum(s1_n)+sum(s2_n) n, sum(s1_m)+sum(s2_m) m,
    XMLELEMENT("target",
      XMLELEMENT("studyid", studyid),
      XMLELEMENT("analyteid", analyteid),
      XMLELEMENT("species", species),
      XMLELEMENT("matrix", matrix),
      XMLELEMENT("population", population),
      XMLELEMENT("poplabel", min(poplabel)),
      XMLELEMENT("runid", rid),
      XMLELEMENT("runno", runid),
      XMLELEMENT("acceptedrun", acceptedrun)
    ) as targ,
    /*Xmlagg(
            XMLELEMENT("sample",
              XMLATTRIBUTES(sampleid AS "sampleid", sampleresultid AS "sampleresultid" ),
              XMLELEMENT("runsamplesequencenumber", runsamplesequencenumber),
              XMLELEMENT("samplesubtype", samplesubtype),
              XMLELEMENT("response", response),
              XMLELEMENT("response", FormatSig(response, 3)),
              XMLELEMENT("status", resulttext),
              XMLELEMENT("deactivated", isdeactivated)
            ) order by runsamplesequencenumber
          ) xml*/
    XMLELEMENT("values",
      Xmlagg(
        XMLELEMENT("sample",
          XMLELEMENT("subject", subject),
          XMLELEMENT("sample1", s1_xml),
          XMLELEMENT("sample2", s2_xml),
          XMLELEMENT("nc", nc_xml),
          XMLELEMENT("s1-mean", FormatRounded(round(s1_mean_useresponse, &&DecPlResponse&), &&DecPlResponse&)),
          XMLELEMENT("s2-mean", FormatRounded(round(s2_mean_useresponse, &&DecPlResponse&), &&DecPlResponse&)),
          XMLELEMENT("nc-mean", FormatRounded(round(nc_mean_useresponse, &&DecPlResponse&), &&DecPlResponse&)),
          XMLELEMENT("n", s1_n+s2_n),
          XMLELEMENT("s1-n", s1_n),
          XMLELEMENT("s2-n", s2_n),
          XMLELEMENT("m", s1_m+s2_m),
          XMLELEMENT("s1-m", s1_m),
          XMLELEMENT("s2-m", s2_m),
          XMLELEMENT("nc-n", nc_n),
          XMLELEMENT("nc-m", nc_m),
          XMLELEMENT("s1-cv", FormatRounded(round(s1_cv, &&DecPlPercCV&), &&DecPlPercCV&)),
          XMLELEMENT("s2-cv", FormatRounded(round(s2_cv, &&DecPlPercCV&), &&DecPlPercCV&)),
          XMLELEMENT("nc-cv", FormatRounded(round(nc_cv, &&DecPlPercCV&), &&DecPlPercCV&)),
          XMLELEMENT("s1-cvflag", 
            case
              when s1_cv > &&PrecisionAcceptanceCriteria& then 'T' else 'F'
            end
          ),
          XMLELEMENT("s2-cvflag",
            case
              when s2_cv > &&PrecisionAcceptanceCriteria& then 'T' else 'F'
            end
          ),
          XMLELEMENT("s1-ratio", FormatRounded(round(
            s1_mean_useresponse/nullif(nc_mean_useresponse,0)--nullif(nc_meanmedian_useresponse,0)
          , &&DecPlRatio&), &&DecPlRatio&)),
          XMLELEMENT("s2-ratio", FormatRounded(round(
            s2_mean_useresponse/nullif(nc_mean_useresponse,0)--nullif(nc_meanmedian_useresponse,0)
          , &&DecPlRatio&), &&DecPlRatio&)),
          XMLELEMENT("difference", FormatRounded(round(
            (s1_mean_useresponse - s2_mean_useresponse) / nullif(s1_mean_useresponse,0) * 100
          , &&DecPlInhibition&), &&DecPlInhibition&)),
          XMLELEMENT("inhibition", FormatRounded(round(Abs(
            (s1_mean_useresponse - s2_mean_useresponse) / nullif(s1_mean_useresponse,0) * 100
          ), &&DecPlInhibition&), &&DecPlInhibition&))
        ) order by subject, runid
      )
    ) as val
  FROM (
    SELECT
      sample1.studyid, sample1.analyteid, sample1.rid, sample1.runid, sample1.acceptedrun, sample1.subject,
      sample1.species, sample1.matrix, pop.ordernumber poporder, pop.display poplabel, sample1.population,
      sample1.mean_response s1_mean_response, sample1.mean_useresponse s1_mean_useresponse,
      sample1.n s1_n, sample1.m s1_m, sample1.sd s1_sd,
      case
        when sample1.mean_useresponse = 0 then null
        else sample1.sd / sample1.mean_useresponse * 100
      end s1_cv, sample1.xml s1_xml,
      sample2.mean_response s2_mean_response, sample2.mean_useresponse s2_mean_useresponse,
      sample2.n s2_n, sample2.m s2_m, sample2.sd s2_sd,
      case
        when sample2.mean_useresponse = 0 then null
        else sample2.sd / sample2.mean_useresponse * 100
      end s2_cv, sample2.xml s2_xml,
      NC.mean_response nc_mean_response, NC.mean_useresponse nc_mean_useresponse,
      NC.n nc_n, NC.m nc_m, NC.sd nc_sd,
      case
        when NC.mean_useresponse = 0 then null
        else NC.sd / NC.mean_useresponse * 100
      end nc_cv, NC.xml nc_xml
    FROM
      (
        SELECT
          studyid, analyteid, rid, runid, acceptedrun, subject, population, species, matrix,
          Avg(response) mean_response,
          Avg(case when(isdeactivated='F') then response else null end) mean_useresponse,
          Stddev(case when(isdeactivated='F') then response else null end) sd,
          count(case when(isdeactivated='F') then 1 else null end) n,
          count(1) m,
          Xmlagg(
            XMLELEMENT("sample",
              XMLATTRIBUTES(sampleid AS "sampleid", sampleresultid AS "sampleresultid" ),
              XMLELEMENT("runsamplesequencenumber", runsamplesequencenumber),
              XMLELEMENT("samplename", samplename),
              XMLELEMENT("samplesubtype", samplesubtype),
              XMLELEMENT("response", FormatRounded(round(response, &&DecPlResponse&), &&DecPlResponse&)),
              XMLELEMENT("status", resulttext),
              XMLELEMENT("source", source),
              XMLELEMENT("deactivated", isdeactivated),
              XMLELEMENT("reason", reason)
            ) order by runsamplesequencenumber
          ) xml
        FROM (
          SELECT
            r.studyid, s.id sampleid, sr.id sampleresultid,
            s.designsampleid, s.samplename, to_number(regexp_replace(s.samplename, '^\D+(\d+).*$', '\1', 1, 1, 'i')) subject,
            case
              when regexp_like(s.samplename,'^\D+\d+[ _\-]([A-Z0-9]+)[ _\-]N?D$','i') then regexp_replace(s.samplename,'^\D+\d+[ _\-]([A-Z0-9]+)[ _\-]N?D$', '\1', 1, 1, 'i')
              else 'NORM'
            end population,
            r.id rid, r.runid, sa.id analyteid, s.samplesubtype,
            case when ra.RunAnalyteRegressionStatus = &&AcceptedRunAnalyte& then 'T' else 'F' end acceptedrun,
            s.runsamplesequencenumber, sr.analytearea response,
            s.status, sr.commenttext, sr.concentration conc, d.reason,
            sr.resulttext, s.source, s.concentration nominalconc,
            sr.concentrationunits concentrationunit, a.species, a.sampletypeid matrix,
            s.hours, s.longtermunits, s.longtermtime, s.temperature,
            case
              when d.code is not null then 'T'
              else 'F'
            end isdeactivated
          FROM 
            &&TempTabPrefix&bio$run r,
            &&TempTabPrefix&bio$run$analytes ra,
            &&TempTabPrefix&bio$study$analytes sa,
            &&TempTabPrefix&bio$assay a,
            &&TempTabPrefix&bio$run$worklist s,
            &&TempTabPrefix&bio$run$worklist$result$rw sr,
            &&TempTabPrefix&bio$covib$run$sample$deact d,
            isr$crit kwz--,
            --table(&&LALPackage&.GetRunStates) rs
          WHERE r.ID = s.runID
          AND s.ID = sr.worklistID
          AND a.ID = r.assayID
          AND r.studyID = 'S1' AND a.studyID = 'S1'
          --AND rs.column_value = r.runstatusnum
          AND sr.runAnalyteCode = ra.code
          AND r.ID = ra.runID
          AND ra.analyteID = sa.ID
          AND s.code = d.code (+)
          AND kwz.key = s.samplename AND kwz.entity = '&&KnownEntity&' AND kwz.masterkey = '&&ExpMasterkey&' AND kwz.repid = &&RepID&
          --and s.runcode || '-' || s.runsamplesequencenumber || '-' || sa.analyteorder in (select code from &&TempTabPrefix&bio$deactivated$samples)
          &&AddConditionS1&
        )
        GROUP BY studyid, analyteid, species, matrix, rid, runid, acceptedrun, subject, population
      ) sample1, (
        SELECT
          studyid, analyteid, rid, runid, subject,
          species, matrix,
          Avg(response) mean_response,
          Avg(case when(isdeactivated='F') then response else null end) mean_useresponse,
          Stddev(case when(isdeactivated='F') then response else null end) sd,
          count(case when(isdeactivated='F') then 1 else null end) n,
          count(1) m,
          Xmlagg(
            XMLELEMENT("sample",
              XMLATTRIBUTES(sampleid AS "sampleid", sampleresultid AS "sampleresultid" ),
              XMLELEMENT("runsamplesequencenumber", runsamplesequencenumber),
              XMLELEMENT("samplename", samplename),
              XMLELEMENT("samplesubtype", samplesubtype),
              XMLELEMENT("response", FormatRounded(round(response, &&DecPlResponse&), &&DecPlResponse&)),
              XMLELEMENT("status", resulttext),
              XMLELEMENT("source", source),
              XMLELEMENT("deactivated", isdeactivated),
              XMLELEMENT("reason", reason)
            ) order by runsamplesequencenumber
          ) xml
        FROM (
          SELECT
            r.studyid, s.id sampleid, sr.id sampleresultid,
            s.designsampleid, s.samplename, to_number(regexp_replace(s.samplename, '^\D+(\d+).*$', '\1', 1, 1, 'i')) subject,
            r.id rid, r.runid, sa.id analyteid, s.samplesubtype,
            s.runsamplesequencenumber, sr.analytearea response,
            s.status, sr.commenttext, sr.concentration conc, d.reason,
            sr.resulttext, s.source, s.concentration nominalconc,
            sr.concentrationunits concentrationunit, a.species, a.sampletypeid matrix,
            s.hours, s.longtermunits, s.longtermtime, s.temperature,
            case
              when d.code is not null then 'T'
              else 'F'
            end isdeactivated
          FROM 
            &&TempTabPrefix&bio$run r,
            &&TempTabPrefix&bio$run$analytes ra,
            &&TempTabPrefix&bio$study$analytes sa,
            &&TempTabPrefix&bio$assay a,
            &&TempTabPrefix&bio$run$worklist s,
            &&TempTabPrefix&bio$run$worklist$result$rw sr,
            &&TempTabPrefix&bio$covib$run$sample$deact d,
            isr$crit kwz--,
            --table(&&LALPackage&.GetRunStates) rs
          WHERE r.ID = s.runID
          AND s.ID = sr.worklistID
          AND a.ID = r.assayID
          AND r.studyID = 'S1' AND a.studyID = 'S1'
          --AND rs.column_value = r.runstatusnum
          AND sr.runAnalyteCode = ra.code
          AND r.ID = ra.runID
          AND sr.runanalyteCode = ra.Code
          AND ra.analyteID = sa.ID
          AND s.code = d.code (+)
          AND kwz.key = s.samplename AND kwz.entity = '&&KnownEntity&' AND kwz.masterkey = '&&ExpMasterkey&' AND kwz.repid = &&RepID&
          --and s.runcode || '-' || s.runsamplesequencenumber || '-' || sa.analyteorder in (select code from &&TempTabPrefix&bio$deactivated$samples)
          &&AddConditionS2&
        )
        GROUP BY studyid, analyteid, species, matrix, rid, runid, subject
      ) sample2, 
      (
        SELECT
          nc.studyid, nc.analyteid, nc.rid, nc.runid, nc.species, nc.matrix,
          Avg(nc.response) mean_response,
          Avg(case when(nc.isdeactivated='F') then nc.response else null end) mean_useresponse,
          Stddev(case when(nc.isdeactivated='F') then nc.response else null end) sd,
          count(case when(nc.isdeactivated='F') then 1 else null end) n,
          count(1) m,
          Xmlagg(
            XMLELEMENT("sample",
              XMLATTRIBUTES(nc.sampleid AS "sampleid", nc.sampleresultid AS "sampleresultid" ),
              XMLELEMENT("runsamplesequencenumber", nc.runsamplesequencenumber),
              XMLELEMENT("samplename", nc.samplename),
              XMLELEMENT("replicatenumber", nc.replicatenumber),
              XMLELEMENT("samplesubtype", nc.samplesubtype),
              XMLELEMENT("s1-response", FormatRounded(round(nc.response, &&DecPlResponse&), &&DecPlResponse&)),
              XMLELEMENT("has-s2", case when nc2.sampleid is not null then 'T' else 'F' end),
              XMLELEMENT("s2-samplename", nc2.samplename),
              XMLELEMENT("s2-response", FormatRounded(round(nc2.response, &&DecPlResponse&), &&DecPlResponse&)),
              XMLELEMENT("difference", FormatRounded(round(
                (nc.useresponse - nc2.useresponse) / nullif(nc.useresponse,0) * 100
              , &&DecPlInhibition&), &&DecPlInhibition&)),
              XMLELEMENT("inhibition", FormatRounded(round(Abs(
                (nc.useresponse - nc2.useresponse) / nullif(nc.useresponse,0) * 100
              ), &&DecPlInhibition&), &&DecPlInhibition&)),
              XMLELEMENT("status", nc.resulttext),
              XMLELEMENT("source", nc.source),
              XMLELEMENT("s1-deactivated", nc.isdeactivated),
              XMLELEMENT("s2-deactivated", nc2.isdeactivated),
              XMLELEMENT("s1-reason", nc.reason),
              XMLELEMENT("s2-reason", nc2.reason)
            ) order by nc.runsamplesequencenumber
          ) xml
        FROM (
          SELECT
            r.studyid, s.id sampleid, sr.id sampleresultid, s.replicatenumber,
            s.designsampleid, s.samplename, regexp_replace(s.samplename,'^(\D+\d+).*$', '\1', 1, 1, 'i') name,
            r.id rid, r.runid, sa.id analyteid, s.samplesubtype,
            s.runsamplesequencenumber, sr.analytearea response,
            case when d.code is not null then null else sr.analytearea end useresponse,
            s.status, sr.commenttext, sr.concentration conc, d.reason,
            sr.resulttext, s.source, s.concentration nominalconc,
            sr.concentrationunits concentrationunit, a.species, a.sampletypeid matrix,
            s.hours, s.longtermunits, s.longtermtime, s.temperature,
            case when d.code is not null then 'T' else 'F' end isdeactivated
          FROM 
            &&TempTabPrefix&bio$run r,
            &&TempTabPrefix&bio$run$analytes ra,
            &&TempTabPrefix&bio$study$analytes sa,
            &&TempTabPrefix&bio$assay a,
            &&TempTabPrefix&bio$run$worklist s,
            &&TempTabPrefix&bio$run$worklist$result$rw sr,
            &&TempTabPrefix&bio$covib$run$sample$deact d,
            isr$crit kwz--,
            --table(&&LALPackage&.GetRunStates) rs
          WHERE r.ID = s.runID AND s.ID = sr.worklistID AND a.ID = r.assayID AND r.studyID = 'S1' AND a.studyID = 'S1'
          --AND rs.column_value = r.runstatusnum
          AND sr.runAnalyteCode = ra.code AND r.ID = ra.runID
          AND sr.runanalyteCode = ra.Code AND ra.analyteID = sa.ID
          AND s.code = d.code (+)
          AND kwz.key = s.samplename AND kwz.entity = '&&KnownEntity&' AND kwz.masterkey = '&&ExpMasterkey&' AND kwz.repid = &&RepID&
          --and s.runcode || '-' || s.runsamplesequencenumber || '-' || sa.analyteorder in (select code from &&TempTabPrefix&bio$deactivated$samples)
          &&AddConditionNC&
        ) nc,
        (
          SELECT
            r.studyid, s.id sampleid, sr.id sampleresultid, s.replicatenumber,
            s.designsampleid, s.samplename, regexp_replace(s.samplename,'^(\D+\d+).*$', '\1', 1, 1, 'i') name,
            r.id rid, r.runid, sa.id analyteid, s.samplesubtype,
            s.runsamplesequencenumber, sr.analytearea response,
            case when d.code is not null then null else sr.analytearea end useresponse,
            s.status, sr.commenttext, sr.concentration conc, d.reason,
            sr.resulttext, s.source, s.concentration nominalconc,
            sr.concentrationunits concentrationunit, a.species, a.sampletypeid matrix,
            s.hours, s.longtermunits, s.longtermtime, s.temperature,
            case when d.code is not null then 'T' else 'F' end isdeactivated
          FROM 
            &&TempTabPrefix&bio$run r,
            &&TempTabPrefix&bio$run$analytes ra,
            &&TempTabPrefix&bio$study$analytes sa,
            &&TempTabPrefix&bio$assay a,
            &&TempTabPrefix&bio$run$worklist s,
            &&TempTabPrefix&bio$run$worklist$result$rw sr,
            &&TempTabPrefix&bio$covib$run$sample$deact d,
            isr$crit kwz--,
            --table(&&LALPackage&.GetRunStates) rs
          WHERE r.ID = s.runID AND s.ID = sr.worklistID AND a.ID = r.assayID AND r.studyID = 'S1' AND a.studyID = 'S1'
          --AND rs.column_value = r.runstatusnum
          AND sr.runAnalyteCode = ra.code AND r.ID = ra.runID
          AND sr.runanalyteCode = ra.Code AND ra.analyteID = sa.ID
          AND s.code = d.code (+)
          AND kwz.key = s.samplename AND kwz.entity = '&&KnownEntity&' AND kwz.masterkey = '&&ExpMasterkey&' AND kwz.repid = &&RepID&
          --and s.runcode || '-' || s.runsamplesequencenumber || '-' || sa.analyteorder in (select code from &&TempTabPrefix&bio$deactivated$samples)
          &&AddConditionNC2&
        ) nc2
        WHERE nc.studyID = nc2.studyID(+) AND nc.rID = nc2.rID(+) AND nc.runid = nc2.runid(+)
        AND nc.analyteID = nc2.analyteID(+) AND nc.species = nc2.species(+) AND nc.matrix = nc2.matrix(+)
        AND nc.name = nc2.name(+) AND nc.replicatenumber = nc2.replicatenumber(+)
        GROUP BY nc.studyid, nc.analyteid, nc.species, nc.matrix, nc.rid, nc.runid) NC,
        isr$crit pop
    WHERE sample1.studyID = sample2.studyID(+) AND sample1.rID = sample2.rID(+) AND sample1.runid = sample2.runid(+)
    AND sample1.analyteID = sample2.analyteID(+) AND sample1.species = sample2.species(+) AND sample1.matrix = sample2.matrix(+) AND sample1.subject = sample2.subject(+)
    -- AND (nvl(sample.reported,'Y') = 'Y')
    AND sample1.studyID = NC.studyID(+) AND sample1.rID = NC.rID(+) AND sample1.runid = NC.runid(+)
    AND sample1.analyteID = NC.analyteID(+) AND sample1.species = NC.species(+) AND sample1.matrix = NC.matrix(+)
    AND pop.entity = 'BIO$COVIB$POPULATIONS' AND pop.masterkey = sample1.population AND pop.repid = &&RepID&
    &&AddCondition&
 )
 GROUP BY studyid, analyteid, species, matrix, rid, runid, acceptedrun, population, poporder)
GROUP BY studyid--, analyteid, species, matrix, acceptedrun
