SELECT XMLELEMENT("experiment", XMLATTRIBUTES('&&Experiment&' AS "name" ,'responsesample' AS "type"), --EXPERIMENT_ADA_SYSTEM_SUITABILITY
      Xmlagg ( XMLConcat(
        Xmlagg (XMLELEMENT("calculation", xmlconcat(targ,val,res)) order by studyid, acceptedrun, analyteid, species, matrix, nominalconc, name, runid),
        XMLELEMENT("statistic", XMLATTRIBUTES(analyteid AS "analyteid", species AS "species", matrix AS "matrix", &&FormatFuncConc&(&&RepresentationFuncConc&(nominalconc, &&PlacesConc&), &&PlacesConc&) AS "nominalconc", name AS "conc-name", acceptedrun AS "acceptedrun"),
          XMLELEMENT("s1-mean", FormatRounded(round(min(s1_overall_mean), &&DecPlResponse&), &&DecPlResponse&)),
          XMLELEMENT("s1-sd", &&FormatFuncResponseSD&(&&RepresentationFuncResponseSD&(min(s1_overall_sd), &&PlacesResponseSD&), &&PlacesResponseSD&)),
          XMLELEMENT("s1-cv", FormatRounded(round(min(s1_overall_sd) / nullif(min(s1_overall_mean),0) *100, &&DecPlPercCV&), &&DecPlPercCV&)),
          XMLELEMENT("s1-ratio-mean", FormatRounded(round(min(s1_overall_ratio_mean), &&DecPlRatio&), &&DecPlRatio&)),
          XMLELEMENT("s1-ratio-sd", FormatRounded(round(min(s1_overall_ratio_sd), &&DecPlRatio&), &&DecPlRatio&)),
          XMLELEMENT("s1-ratio-cv", FormatRounded(round(min(s1_overall_ratio_sd) / nullif(min(s1_overall_ratio_mean),0) *100, &&DecPlRatio&), &&DecPlRatio&)),
          XMLELEMENT("s2-mean", FormatRounded(round(min(s2_overall_mean), &&DecPlResponse&), &&DecPlResponse&)),
          XMLELEMENT("s2-sd", &&FormatFuncResponseSD&(round(min(s2_overall_sd), &&PlacesResponseSD&), &&PlacesResponseSD&)),
          XMLELEMENT("s2-cv", FormatRounded(round(min(s2_overall_sd) / nullif(min(s2_overall_mean),0) * 100, &&DecPlPercCV&), &&DecPlPercCV&)),
          XMLELEMENT("s2-ratio-mean", FormatRounded(round(min(s2_overall_ratio_mean), &&DecPlRatio&), &&DecPlRatio&)),
          XMLELEMENT("s2-ratio-sd", FormatRounded(round(min(s2_overall_ratio_sd), &&DecPlRatio&), &&DecPlRatio&)),
          XMLELEMENT("s2-ratio-cv", FormatRounded(round(min(s2_overall_ratio_sd) / nullif(min(s2_overall_ratio_mean),0) *100, &&DecPlRatio&), &&DecPlRatio&)),
          XMLELEMENT("s1-mean-plus-xsd", FormatRounded(round(min(s1_overall_mean) + (&&SSTSDNumber& * min(s1_overall_sd)), &&DecPlMeanPlusMinusXSD&), &&DecPlMeanPlusMinusXSD&)),
          XMLELEMENT("s1-mean-minus-xsd", FormatRounded(round(min(s1_overall_mean) - (&&SSTSDNumber& * min(s1_overall_sd)), &&DecPlMeanPlusMinusXSD&), &&DecPlMeanPlusMinusXSD&)),
          XMLELEMENT("s1-ratio-mean-plus-xsd", FormatRounded(round(min(s1_overall_ratio_mean) + (&&SSTSDNumber& * min(s1_overall_ratio_sd)), &&DecPlRatio&), &&DecPlRatio&)),
          XMLELEMENT("s1-ratio-mean-minus-xsd", FormatRounded(round(min(s1_overall_ratio_mean) - (&&SSTSDNumber& * min(s1_overall_ratio_sd)), &&DecPlRatio&), &&DecPlRatio&)),
          XMLELEMENT("nc-meanmedian", FormatRounded(round(min(nc_overall_meanmedian), &&DecPlResponse&), &&DecPlResponse&)),
          XMLELEMENT("nc-sd", &&FormatFuncResponseSD&(&&RepresentationFuncResponseSD&(min(nc_overall_sd), &&PlacesResponseSD&), &&PlacesResponseSD&)),
          XMLELEMENT("nc-cv", FormatRounded(round(min(nc_overall_sd) / nullif(min(nc_overall_meanmedian),0) *100, &&DecPlPercCV&), &&DecPlPercCV&)),
          XMLELEMENT("nc-meanmedian-plus-xsd", FormatRounded(round(min(nc_overall_meanmedian) + (&&SSTSDNumber& * min(nc_overall_sd)), &&DecPlMeanPlusMinusXSD&), &&DecPlMeanPlusMinusXSD&)),
          XMLELEMENT("nc-meanmedian-minus-xsd", FormatRounded(round(min(nc_overall_meanmedian) - (&&SSTSDNumber& * min(nc_overall_sd)), &&DecPlMeanPlusMinusXSD&), &&DecPlMeanPlusMinusXSD&)),
          XMLELEMENT("nc-n", FmtNum(min(nc_overall_count))),
          XMLELEMENT("difference-mean", FormatRounded(round(min(difference_overall_mean), &&DecPlInhibition&), &&DecPlInhibition&)),
          XMLELEMENT("difference-sd", FormatRounded(round(min(difference_overall_sd), &&DecPlInhibition&), &&DecPlInhibition&)),
          XMLELEMENT("difference-cv", FormatRounded(round(min(difference_overall_sd) / nullif(min(difference_overall_mean),0) *100, &&DecPlInhibition&), &&DecPlInhibition&)),
          XMLELEMENT("inhibition-mean", FormatRounded(round(min(inhibition_overall_mean), &&DecPlInhibition&), &&DecPlInhibition&)),
          XMLELEMENT("inhibition-sd", FormatRounded(round(min(inhibition_overall_sd), &&DecPlInhibition&), &&DecPlInhibition&)),
          XMLELEMENT("inhibition-cv", FormatRounded(round(min(inhibition_overall_sd) / nullif(min(inhibition_overall_mean),0) *100, &&DecPlInhibition&), &&DecPlInhibition&))
        )) order by studyid, acceptedrun, analyteid, species, matrix, nominalconc, name
      ))
  FROM (
  SELECT
    studyid, analyteid, species, matrix, nominalconc, name, runid, acceptedrun,
    min(nc_overall_meanmedian) nc_overall_meanmedian, min(nc_overall_sd) nc_overall_sd, min(nc_overall_n) nc_overall_count,
    min(s1_overall_mean) s1_overall_mean, min(s1_overall_sd) s1_overall_sd, min(s1_overall_ratio_mean) s1_overall_ratio_mean, min(s1_overall_ratio_sd) s1_overall_ratio_sd,
    min(s2_overall_mean) s2_overall_mean, min(s2_overall_sd) s2_overall_sd, min(s2_overall_ratio_mean) s2_overall_ratio_mean, min(s2_overall_ratio_sd) s2_overall_ratio_sd,
    min(difference_overall_mean) difference_overall_mean, min(difference_overall_sd) difference_overall_sd, min(inhibition_overall_mean) inhibition_overall_mean, min(inhibition_overall_sd) inhibition_overall_sd,
    XMLELEMENT("target",
      XMLELEMENT("studyid", studyid), XMLELEMENT("analyteid", analyteid), XMLELEMENT("species", species), XMLELEMENT("matrix", matrix),
      XMLELEMENT("nominalconc", &&FormatFuncConc&(&&RepresentationFuncConc&(nominalconc, &&PlacesConc&), &&PlacesConc&)),
      XMLELEMENT("conc-name", name), XMLELEMENT("runid", rid), XMLELEMENT("runno", runid), XMLELEMENT("acceptedrun", acceptedrun)
    ) as targ,
    XMLELEMENT("values",
      Xmlagg(
        XMLELEMENT("sample",
          XMLELEMENT("subject", subject),
          XMLELEMENT("sample1", s1_xml), XMLELEMENT("sample2", s2_xml), XMLELEMENT("nc", nc_xml),
          XMLELEMENT("s1-mean", FormatRounded(round(s1_mean_useresponse, &&DecPlResponse&), &&DecPlResponse&)), XMLELEMENT("s1-cv", FormatRounded(round(s1_cv, &&DecPlPercCV&), &&DecPlPercCV&)), XMLELEMENT("s1-ratio", FormatRounded(round(s1_ratio, &&DecPlRatio&), &&DecPlRatio&)), XMLELEMENT("s1-n", s1_n), XMLELEMENT("s1-m", s1_m),
          XMLELEMENT("s2-mean", FormatRounded(round(s2_mean_useresponse, &&DecPlResponse&), &&DecPlResponse&)), XMLELEMENT("s2-cv", FormatRounded(round(s2_cv, &&DecPlPercCV&), &&DecPlPercCV&)), XMLELEMENT("s2-ratio", FormatRounded(round(s2_ratio, &&DecPlRatio&), &&DecPlRatio&)), XMLELEMENT("s2-n", s2_n), XMLELEMENT("s2-m", s2_m),
          XMLELEMENT("nc-meanmedian", FormatRounded(round(nc_meanmedian_useresponse, &&DecPlResponse&), &&DecPlResponse&)), XMLELEMENT("nc-cv", FormatRounded(round(nc_cv, &&DecPlPercCV&), &&DecPlPercCV&)), XMLELEMENT("nc-n", nc_n), XMLELEMENT("nc-m", nc_m),
          XMLELEMENT("screen-cp", FormatRounded(round(screen_cp, &&DecPlCutPoint&), &&DecPlCutPoint&)), XMLELEMENT("confirm-cp", FormatRounded(round(confirm_cp, &&DecPlConfirmCutPoint&), &&DecPlConfirmCutPoint&)),
          XMLELEMENT("titer-cp", case when '&&FixedTiterCutpoint&' is not null then FormatRounded(round(to_number('&&FixedTiterCutpoint&'), &&DecPlCutPoint&), &&DecPlCutPoint&)
            when '&&TiterCPNFactor&' is not null then FormatRounded(round(nc_meanmedian_useresponse*to_number('&&TiterCPNFactor&'), &&DecPlCutPoint&), &&DecPlCutPoint&)
            else null end),
          XMLELEMENT("s1-screen-result", case when s1_mean_useresponse is null then null
            when s1_mean_useresponse >= screen_cp then 'Positive'
            else 'Negative' end),
          XMLELEMENT("s2-screen-result", case when s2_mean_useresponse is null then null
            when s2_mean_useresponse >= screen_cp then 'Positive'
            else 'Negative' end),
          XMLELEMENT("s1-confirm-result",
            case when s2_mean_useresponse is null or confirm_cp is null then null
              else 'Confirmed ' || case when difference >= confirm_cp then 'Positive' else 'Negative' end
            end),
          XMLELEMENT("s2-confirm-result",
            case when s2_mean_useresponse is null or confirm_cp is null then null
              else 'Confirmed ' || case when difference>= confirm_cp then 'Positive' else 'Negative' end
            end),
          XMLELEMENT("n", s1_n+s2_n), XMLELEMENT("m", s1_m+s2_m),
          XMLELEMENT("s1-cvflag", case when s1_cv > &&PrecisionAcceptanceCriteria& then 'T' else 'F' end),
          XMLELEMENT("s2-cvflag", case when s2_cv > &&PrecisionAcceptanceCriteria& then 'T' else 'F' end),
          XMLELEMENT("nc-cvflag", case when nc_cv > &&PrecisionAcceptanceCriteria& then 'T' else 'F' end),
          XMLELEMENT("difference", FormatRounded(round(difference, &&DecPlInhibition&), &&DecPlInhibition&)),
          XMLELEMENT("inhibition", FormatRounded(round(inhibition, &&DecPlInhibition&), &&DecPlInhibition&))
        ) order by subject, runid
      )
    ) as val,
    XMLELEMENT("result", 
      XMLELEMENT("nc-meanmedian", FormatRounded(round(Avg(nc_meanmedian_useresponse), &&DecPlResponse&), &&DecPlResponse&)),
      XMLELEMENT("nc-sd", &&FormatFuncResponseSD&(&&RepresentationFuncResponseSD&(Stddev(nc_meanmedian_useresponse), &&PlacesResponseSD&), &&PlacesResponseSD&)),
      XMLELEMENT("nc-cv", FormatRounded(round(Stddev(nc_meanmedian_useresponse) / nullif(Avg(nc_meanmedian_useresponse),0) * 100, &&DecPlPercCV&), &&DecPlPercCV&)),
      XMLELEMENT("nc-meanmedian-plus-xsd", FormatRounded(round(Avg(nc_meanmedian_useresponse) + (&&SSTSDNumber& * Stddev(nc_meanmedian_useresponse)), &&DecPlMeanPlusMinusXSD&), &&DecPlMeanPlusMinusXSD&)),
      XMLELEMENT("nc-meanmedian-minus-xsd", FormatRounded(round(Avg(nc_meanmedian_useresponse) - (&&SSTSDNumber& * Stddev(nc_meanmedian_useresponse)), &&DecPlMeanPlusMinusXSD&), &&DecPlMeanPlusMinusXSD&)),
      XMLELEMENT("s1-mean", FormatRounded(round(Avg(s1_mean_useresponse), &&DecPlResponse&), &&DecPlResponse&)),
      XMLELEMENT("s1-sd", &&FormatFuncResponseSD&(&&RepresentationFuncResponseSD&(Stddev(s1_mean_useresponse), &&PlacesResponseSD&), &&PlacesResponseSD&)),
      XMLELEMENT("s1-cv", FormatRounded(round(Stddev(s1_mean_useresponse) / nullif(Avg(s1_mean_useresponse),0) * 100, &&DecPlPercCV&), &&DecPlPercCV&)),
      XMLELEMENT("s1-ratio-mean", FormatRounded(round(Avg(s1_ratio), &&DecPlRatio&), &&DecPlRatio&)),
      XMLELEMENT("s1-ratio-sd", FormatRounded(round(Stddev(s1_ratio), &&DecPlRatio&), &&DecPlRatio&)),
      XMLELEMENT("s1-ratio-cv", FormatRounded(round(Stddev(s1_ratio) / nullif(Avg(s1_ratio),0) * 100, &&DecPlRatio&), &&DecPlRatio&)),
      XMLELEMENT("s2-mean", FormatRounded(round(Avg(s2_mean_useresponse), &&DecPlResponse&), &&DecPlResponse&)),
      XMLELEMENT("s2-sd", &&FormatFuncResponseSD&(&&RepresentationFuncResponseSD&(Stddev(s2_mean_useresponse), &&PlacesResponseSD&), &&PlacesResponseSD&)),
      XMLELEMENT("s2-cv", FormatRounded(round(Stddev(s2_mean_useresponse) / nullif(Avg(s2_mean_useresponse),0) * 100, &&DecPlPercCV&), &&DecPlPercCV&)),
      XMLELEMENT("s2-ratio-mean", FormatRounded(round(Avg(s2_ratio), &&DecPlRatio&), &&DecPlRatio&)),
      XMLELEMENT("s2-ratio-sd", FormatRounded(round(Stddev(s2_ratio), &&DecPlRatio&), &&DecPlRatio&)),
      XMLELEMENT("s2-ratio-cv", FormatRounded(round(Stddev(s2_ratio) / nullif(Avg(s2_ratio),0) * 100, &&DecPlRatio&), &&DecPlRatio&)),
      XMLELEMENT("s1-mean-plus-xsd", FormatRounded(round(Avg(s1_mean_useresponse) + (&&SSTSDNumber& * Stddev(s1_mean_useresponse)), &&DecPlMeanPlusMinusXSD&), &&DecPlMeanPlusMinusXSD&)),
      XMLELEMENT("s1-mean-minus-xsd", FormatRounded(round(Avg(s1_mean_useresponse) - (&&SSTSDNumber& * Stddev(s1_mean_useresponse)), &&DecPlMeanPlusMinusXSD&), &&DecPlMeanPlusMinusXSD&)),
      XMLELEMENT("s1-ratio-mean-plus-xsd", FormatRounded(round(Avg(s1_ratio) + (&&SSTSDNumber& * Stddev(s1_ratio)), &&DecPlRatio&), &&DecPlRatio&)),
      XMLELEMENT("s1-ratio-mean-minus-xsd", FormatRounded(round(Avg(s1_ratio) - (&&SSTSDNumber& * Stddev(s1_ratio)), &&DecPlRatio&), &&DecPlRatio&)),
      XMLELEMENT("difference-mean", FormatRounded(round(Avg(difference), &&DecPlInhibition&), &&DecPlInhibition&)),
      XMLELEMENT("difference-sd", FormatRounded(round(Stddev(difference), &&DecPlInhibition&), &&DecPlInhibition&)),
      XMLELEMENT("difference-cv", FormatRounded(round(Stddev(difference) / nullif(Avg(difference),0) * 100, &&DecPlInhibition&), &&DecPlInhibition&)),
      XMLELEMENT("inhibition-mean", FormatRounded(round(Avg(inhibition), &&DecPlInhibition&), &&DecPlInhibition&)),
      XMLELEMENT("inhibition-sd", FormatRounded(round(Stddev(inhibition), &&DecPlInhibition&), &&DecPlInhibition&)),
      XMLELEMENT("inhibition-cv", FormatRounded(round(Stddev(inhibition) / nullif(Avg(inhibition),0) * 100, &&DecPlInhibition&), &&DecPlInhibition&))
    ) as res
  FROM (
    SELECT
      studyid, analyteid, rid, runid, acceptedrun, subject, nominalconc, dilution, species, matrix, screen_cp, confirm_cp, name,
      s1_mean_response, s1_mean_useresponse, s1_n, s1_m, s1_sd, s1_sd / nullif(s1_mean_useresponse,0) * 100 s1_cv, s1_mean_useresponse / nullif(nc_meanmedian_useresponse,0) s1_ratio, s1_xml,
      s2_mean_response, s2_mean_useresponse, s2_n, s2_m, s2_sd, s2_sd / nullif(s2_mean_useresponse,0) * 100 s2_cv, s2_mean_useresponse / nullif(nc_meanmedian_useresponse,0) s2_ratio, s2_xml,
      nc_mean_response, nc_mean_useresponse, nc_meanmedian_response, nc_meanmedian_useresponse, nc_n, nc_m, nc_sd, nc_sd / nullif(nc_mean_useresponse,0) * 100 nc_cv, nc_xml,
      overall_nc_mean nc_overall_meanmedian, overall_nc_sd nc_overall_sd, overall_nc_n nc_overall_n,
      (s1_mean_useresponse - s2_mean_useresponse) / nullif(s1_mean_useresponse,0) * 100 difference,
      abs((s1_mean_useresponse - s2_mean_useresponse) / nullif(s1_mean_useresponse,0) * 100) inhibition,
      Avg(s1_mean_useresponse) over (partition by studyid, analyteid, species, matrix, nominalconc, name, acceptedrun) s1_overall_mean,
      Stddev(s1_mean_useresponse) over (partition by studyid, analyteid, species, matrix, nominalconc, name, acceptedrun) s1_overall_sd,
      Avg(s1_mean_useresponse / nullif(nc_meanmedian_useresponse,0)) over (partition by studyid, analyteid, species, matrix, nominalconc, name, acceptedrun) s1_overall_ratio_mean,
      Stddev(s1_mean_useresponse / nullif(nc_meanmedian_useresponse,0)) over (partition by studyid, analyteid, species, matrix, nominalconc, name, acceptedrun) s1_overall_ratio_sd,
      Avg(s2_mean_useresponse) over (partition by studyid, analyteid, species, matrix, nominalconc, name, acceptedrun) s2_overall_mean,
      Stddev(s2_mean_useresponse) over (partition by studyid, analyteid, species, matrix, nominalconc, name, acceptedrun) s2_overall_sd,
      Avg(s2_mean_useresponse / nullif(nc_meanmedian_useresponse,0)) over (partition by studyid, analyteid, species, matrix, nominalconc, name, acceptedrun) s2_overall_ratio_mean,
      Stddev(s2_mean_useresponse / nullif(nc_meanmedian_useresponse,0)) over (partition by studyid, analyteid, species, matrix, nominalconc, name, acceptedrun) s2_overall_ratio_sd,
      Avg((s1_mean_useresponse - s2_mean_useresponse) / nullif(s1_mean_useresponse,0) * 100) over (partition by studyid, analyteid, species, matrix, nominalconc, name, acceptedrun) difference_overall_mean,
      Stddev((s1_mean_useresponse - s2_mean_useresponse) / nullif(s1_mean_useresponse,0) * 100) over (partition by studyid, analyteid, species, matrix, nominalconc, name, acceptedrun) difference_overall_sd,
      Avg(abs((s1_mean_useresponse - s2_mean_useresponse) / nullif(s1_mean_useresponse,0) * 100)) over (partition by studyid, analyteid, species, matrix, nominalconc, name, acceptedrun) inhibition_overall_mean,
      Stddev(abs((s1_mean_useresponse - s2_mean_useresponse) / nullif(s1_mean_useresponse,0) * 100)) over (partition by studyid, analyteid, species, matrix, nominalconc, name, acceptedrun) inhibition_overall_sd
    FROM (
      SELECT
        sample1.studyid, sample1.analyteid, sample1.rid, sample1.runid, sample1.acceptedrun, sample1.subject, sample1.nominalconc, sample1.dilution, sample1.species, sample1.matrix,
        nc.screen_cp, sample1.name, case when regexp_like(ccp.key, '^\-?(\d+)?\.?\d+$') then to_number(ccp.key) else null end confirm_cp,      
        sample1.mean_response s1_mean_response, sample1.mean_useresponse s1_mean_useresponse, sample1.n s1_n, sample1.m s1_m, sample1.sd s1_sd, sample1.xml s1_xml,
        sample2.mean_response s2_mean_response, sample2.mean_useresponse s2_mean_useresponse, sample2.n s2_n, sample2.m s2_m, sample2.sd s2_sd, sample2.xml s2_xml,
        NC.mean_response nc_mean_response, NC.mean_useresponse nc_mean_useresponse, NC.meanmedian_response nc_meanmedian_response, NC.meanmedian_useresponse nc_meanmedian_useresponse,
        NC.n nc_n, NC.m nc_m, NC.sd nc_sd, NC.overall_nc_mean, NC.overall_nc_sd, NC.overall_nc_n, NC.xml nc_xml
      FROM
        (
          SELECT
            studyid, analyteid, rid, runid, acceptedrun, subject, nominalconc, dilution, species, matrix,
            Avg(response) mean_response, name, Avg(case when(isdeactivated='F') then response else null end) mean_useresponse, Stddev(case when(isdeactivated='F') then response else null end) sd,
            count(case when(isdeactivated='F') then 1 else null end) n, count(1) m,
            sum(case when (max(isdeactivated)='F') then 1 else null end) over (partition by studyid, analyteid, species, matrix, rid, runid, acceptedrun, name, nominalconc) run_n,
            Xmlagg(
              XMLELEMENT("sample",
                XMLATTRIBUTES(sampleid AS "sampleid", sampleresultid AS "sampleresultid" ),
                XMLELEMENT("runsamplesequencenumber", runsamplesequencenumber),
                XMLELEMENT("samplename", samplename), XMLELEMENT("samplesubtype", samplesubtype),
                XMLELEMENT("response", FormatRounded(round(response, &&DecPlResponse&), &&DecPlResponse&)),
                XMLELEMENT("status", resulttext), XMLELEMENT("deactivated", isdeactivated), XMLELEMENT("reason", reason)
              ) order by runsamplesequencenumber
            ) xml
          FROM (
            SELECT
              r.studyid, s.id sampleid, sr.id sampleresultid, s.designsampleid, s.samplename,
              case when regexp_like(s.samplename, '&&SubjectRegex&', 'i') then to_number(regexp_replace(s.samplename, '&&SubjectRegex&', '\1', 1, 1, 'i')) else null end subject,
              case when regexp_like(s.samplename, '^([A-Z]+).*$', 'i') then regexp_replace(s.samplename, '^([A-Z]+).*$', '\1', 1, 1, 'i') else null end name,
              r.id rid, r.runid, sa.id analyteid, s.samplesubtype,
              s.runsamplesequencenumber, sr.analytearea response,
              s.status, sr.commenttext, sr.concentration conc,
              sr.resulttext, s.source, d.reason, k.concentration nominalconc, s.dilution,
              sr.concentrationunits concentrationunit, a.species, a.sampletypeid matrix,
              case when ra.RunAnalyteRegressionStatus = &&AcceptedRunAnalyte& then 'T' else 'F' end acceptedrun,
              case when d.code is not null then 'T' else 'F' end isdeactivated
            FROM 
              &&TempTabPrefix&bio$run r, &&TempTabPrefix&bio$run$analytes ra,
              &&TempTabPrefix&bio$study$analytes sa, &&TempTabPrefix&bio$assay a,
              &&TempTabPrefix&bio$run$worklist s, &&TempTabPrefix&bio$run$worklist$result$rw sr,
              &&TempTabPrefix&bio$run$ana$known k, &&TempTabPrefix&bio$covib$run$sample$deact d,
              isr$crit kwz
            WHERE r.ID = s.runID AND s.ID = sr.worklistID
            AND a.ID = r.assayID AND r.studyID = '&&StudyID&' AND a.studyID = '&&StudyID&'
            AND sr.runAnalyteCode = ra.code AND r.ID = ra.runID
            AND ra.analyteID = sa.ID AND s.code = d.code (+)
            AND k.runAnalyteCode = ra.code AND k.name = s.sampleName
            AND kwz.key = s.samplename AND kwz.entity = '&&KnownEntity&' AND kwz.masterkey = '&&ExpMasterkey&' AND kwz.repid = &&RepID&
            &&AddConditionS1&
          )
          GROUP BY studyid, analyteid, species, matrix, rid, runid, acceptedrun, subject, nominalconc, dilution, name
        ) sample1, (
          SELECT
            studyid, analyteid, rid, runid, subject, nominalconc, dilution,
            species, matrix,
            Avg(response) mean_response,
            Avg(case when(isdeactivated='F') then response else null end) mean_useresponse,
            Stddev(case when(isdeactivated='F') then response else null end) sd,
            count(case when(isdeactivated='F') then 1 else null end) n,
            count(1) m,
            Xmlagg(
              XMLELEMENT("sample",
                XMLATTRIBUTES(sampleid AS "sampleid", sampleresultid AS "sampleresultid" ),
                XMLELEMENT("runsamplesequencenumber", runsamplesequencenumber),
                XMLELEMENT("samplename", samplename), XMLELEMENT("samplesubtype", samplesubtype),
                XMLELEMENT("response", FormatRounded(round(response, &&DecPlResponse&), &&DecPlResponse&)),
                XMLELEMENT("status", resulttext), XMLELEMENT("deactivated", isdeactivated), XMLELEMENT("reason", reason)
              ) order by runsamplesequencenumber
            ) xml
          FROM (
            SELECT
              r.studyid, s.id sampleid, sr.id sampleresultid, s.designsampleid, s.samplename,
              case when regexp_like(s.samplename, '&&SubjectRegex&', 'i') then to_number(regexp_replace(s.samplename, '&&SubjectRegex&', '\1', 1, 1, 'i')) else null end subject,
              r.id rid, r.runid, sa.id analyteid, s.samplesubtype,
              s.runsamplesequencenumber, sr.analytearea response,
              s.status, sr.commenttext, sr.concentration conc,
              sr.resulttext, s.source, d.reason, k.concentration nominalconc, s.dilution,
              sr.concentrationunits concentrationunit, a.species, a.sampletypeid matrix,
              case when d.code is not null then 'T' else 'F' end isdeactivated
            FROM 
              &&TempTabPrefix&bio$run r, &&TempTabPrefix&bio$run$analytes ra,
              &&TempTabPrefix&bio$study$analytes sa, &&TempTabPrefix&bio$assay a,
              &&TempTabPrefix&bio$run$worklist s, &&TempTabPrefix&bio$run$worklist$result$rw sr,
              &&TempTabPrefix&bio$run$ana$known k, &&TempTabPrefix&bio$covib$run$sample$deact d,
              isr$crit kwz
            WHERE r.ID = s.runID AND s.ID = sr.worklistID
            AND a.ID = r.assayID AND r.studyID = '&&StudyID&' AND a.studyID = '&&StudyID&'
            AND sr.runAnalyteCode = ra.code AND r.ID = ra.runID
            AND ra.analyteID = sa.ID AND s.code = d.code (+)
            AND k.runAnalyteCode = ra.code AND k.name = s.sampleName
            AND kwz.key = s.samplename AND kwz.entity = '&&KnownEntity&' AND kwz.masterkey = '&&ExpMasterkey&' AND kwz.repid = &&RepID&
            &&AddConditionS2&
          )
          GROUP BY studyid, analyteid, species, matrix, rid, runid, subject, nominalconc, dilution
        ) sample2, 
        (
          SELECT
            studyid, analyteid, rid, runid, species, matrix,
            Avg(response) mean_response,
            Avg(case when(isdeactivated='F') then response else null end) mean_useresponse,
            &&NCMeanOrMedian&(
              case when mean_or_median = 'meanofmeans' then case when replicatenumber = 1 then meanuseresponse else null end
              else response end
            ) meanmedian_response,
            &&NCMeanOrMedian&(
              case when mean_or_median = 'meanofmeans' then case when replicatenumber = 1 then meanuseresponse else null end
              else case when(isdeactivated='F') then response else null end end
            ) meanmedian_useresponse,
            min(overall_nc_mean) overall_nc_mean, min(overall_nc_sd) overall_nc_sd, min(overall_nc_n) overall_nc_n,
            case
              when '&&FixedScreenCutpoint&' is not null then to_number('&&FixedScreenCutpoint&')
              when '&&ScreenCPNFactor&' is not null then &&NCMeanOrMedian&(
                case when mean_or_median = 'meanofmeans' then case when replicatenumber = 1 then meanuseresponse else null end
                else case when(isdeactivated='F') then response else null end end
              )*to_number('&&ScreenCPNFactor&')
              else &&NCMeanOrMedian&(
                case when mean_or_median = 'meanofmeans' then case when replicatenumber = 1 then meanuseresponse else null end
                else case when(isdeactivated='F') then response else null end end
              )
            end screen_cp,
            Stddev(
              case when mean_or_median = 'meanofmeans' then case when replicatenumber = 1 then meanuseresponse else null end
              else case when(isdeactivated='F') then response else null end end
            ) sd,
            count(
              case when mean_or_median = 'meanofmeans' then case when replicatenumber = 1 then meanuseresponse else null end
              else case when(isdeactivated='F') then response else null end end
            ) n,
            count(
              case when mean_or_median = 'meanofmeans' then case when replicatenumber = 1 then meanuseresponse else null end
              else response end
            ) m,
            Xmlagg(
              XMLELEMENT("sample",
                XMLATTRIBUTES(sampleid AS "sampleid", sampleresultid AS "sampleresultid" ),
                XMLELEMENT("runsamplesequencenumber", runsamplesequencenumber),
                XMLELEMENT("samplename", samplename), XMLELEMENT("samplesubtype", samplesubtype),
                XMLELEMENT("response", FormatRounded(round(response, &&DecPlResponse&), &&DecPlResponse&)),
                XMLELEMENT("status", resulttext), XMLELEMENT("deactivated", isdeactivated), XMLELEMENT("reason", reason)
              ) order by runsamplesequencenumber
            ) xml
          FROM (
            SELECT
              r.studyid, s.id sampleid, sr.id sampleresultid, s.designsampleid, s.samplename, d.reason, s.replicatenumber,
              r.id rid, r.runid, sa.id analyteid, s.samplesubtype, s.runsamplesequencenumber, sr.analytearea response,
              s.status, sr.commenttext, sr.concentration conc, sr.resulttext, s.source, s.concentration nominalconc,
              sr.concentrationunits concentrationunit, a.species, a.sampletypeid matrix, stb$util.GetReportParameterValue('MEAN_OR_MEDIAN_NC',&&RepID&) mean_or_median,
              Avg(case when d.code is null then sr.analytearea else null end) over (partition by r.studyid, sa.id, a.species, a.sampletypeid, case when ra.RunAnalyteRegressionStatus = &&AcceptedRunAnalyte& then 'T' else 'F' end) overall_nc_mean,
              Stddev(case when d.code is null then sr.analytearea else null end) over (partition by r.studyid, sa.id, a.species, a.sampletypeid, case when ra.RunAnalyteRegressionStatus = &&AcceptedRunAnalyte& then 'T' else 'F' end) overall_nc_sd,
              count(case when d.code is null then sr.analytearea else null end) over (partition by r.studyid, sa.id, a.species, a.sampletypeid, case when ra.RunAnalyteRegressionStatus = &&AcceptedRunAnalyte& then 'T' else 'F' end) overall_nc_n,
              Avg(case when(d.code is null) then sr.analytearea else null end) over (partition by r.studyid, sa.id, r.id, s.samplename) meanresponse,
              Avg(case when(d.code is null) then sr.analytearea else null end) over (partition by r.studyid, sa.id, r.id, s.samplename) meanuseresponse,
              case when d.code is not null then 'T' else 'F' end isdeactivated
            FROM 
              &&TempTabPrefix&bio$run r, &&TempTabPrefix&bio$run$analytes ra,
              &&TempTabPrefix&bio$study$analytes sa, &&TempTabPrefix&bio$assay a,
              &&TempTabPrefix&bio$run$worklist s, &&TempTabPrefix&bio$run$worklist$result$rw sr,
              &&TempTabPrefix&bio$covib$run$sample$deact d, isr$crit kwz
            WHERE r.ID = s.runID AND s.ID = sr.worklistID
            AND a.ID = r.assayID AND r.studyID = '&&StudyID&' AND a.studyID = '&&StudyID&'
            AND sr.runAnalyteCode = ra.code AND r.ID = ra.runID
            AND ra.analyteID = sa.ID AND s.code = d.code (+)
            AND kwz.key = s.samplename AND kwz.entity = '&&KnownEntity&' AND kwz.masterkey = '&&ExpMasterkey&' AND kwz.repid = &&RepID&
            &&AddConditionNC&
          )
          GROUP BY studyid, analyteid, species, matrix, rid, runid) NC,
          isr$crit ccp
      WHERE sample1.studyID = sample2.studyID(+)
      AND sample1.rID = sample2.rID(+) AND sample1.runid = sample2.runid(+)
      AND sample1.analyteID = sample2.analyteID(+) AND sample1.species = sample2.species(+)
      AND sample1.matrix = sample2.matrix(+)
      AND sample1.subject = sample2.subject(+) AND sample1.nominalconc = sample2.nominalconc(+)
      AND sample1.studyID = NC.studyID(+) AND sample1.rID = NC.rID(+)
      AND sample1.runid = NC.runid(+) AND sample1.analyteID = NC.analyteID(+)
      AND sample1.species = NC.species(+) AND sample1.matrix = NC.matrix(+)
      AND ccp.entity = 'BIO$COVIB$CONFIRM$CUTPOINT' AND ccp.masterkey = 'NORM' AND ccp.repid = &&RepID&
      &&AddCondition& &&GroupConditions&
    )
 )
 GROUP BY studyid, analyteid, species, matrix, nominalconc, name, rid, runid, acceptedrun)
GROUP BY studyid, analyteid, species, matrix, nominalconc, name, acceptedrun
