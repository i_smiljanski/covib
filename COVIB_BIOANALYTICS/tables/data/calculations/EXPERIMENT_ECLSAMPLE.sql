  SELECT XMLELEMENT("experiment", XMLATTRIBUTES('&&Experiment&' AS "name" ,'eclsample' AS "type"),
      Xmlagg ( XMLConcat(
        Xmlagg (XMLELEMENT("calculation", xmlconcat(targ,val,res)) order by analytename, dosegroupno, dosegroup, designsubjecttag, subjectid, designSampleID, studyday, samplingtime, time, doseamount, doseunit, runid, rid, posgroup, source, concentrationunit),
        XMLELEMENT("statistic", XMLATTRIBUTES(analytename AS "analytename"),
               XMLELEMENT("n", max(n)),
               XMLELEMENT("m", max(m)),
               XMLELEMENT("posm", max(posm)),
               XMLELEMENT("cutpointfactor", FmtNum(max(cutpointfactor)))
             )) order by studyid, analytename
      ))
  FROM (
  SELECT studyid, analytename, dosegroupno, dosegroup, designsubjecttag, subjectid, designSampleID, samplingtime, time, studyday, rid, runid, posgroup, source, concentrationunit, doseamount, doseunit, n, m, posm, cutpointfactor,
    XMLELEMENT("target",
      XMLELEMENT("dosegroup", dosegroup),
      XMLELEMENT("subjectid", subjectid),
      XMLELEMENT("designsubjecttag", designsubjecttag),
      XMLELEMENT("designsampleid", designSampleID),
      XMLELEMENT("runid", rid),
      XMLELEMENT("runno", runid),
      XMLELEMENT("analytename", analytename),
      XMLELEMENT("doseamount", FmtNum(doseamount)),
      XMLELEMENT("doseunit", doseunit),
      XMLELEMENT("group", posgroup),
      XMLELEMENT("samplingtime", samplingtime),
      XMLELEMENT("timetext", timetext),
      XMLELEMENT("time", time),
      XMLELEMENT("studyday", studyday),
      XMLELEMENT("concentrationunit", concentrationunit)
    ) as targ,
    XMLELEMENT("values", 
      Xmlagg(
        XMLELEMENT("sample",
          XMLATTRIBUTES(sampleid AS "sampleid", sampleresultid AS "sampleresultid" ),
          XMLELEMENT("runsamplesequencenumber", runsamplesequencenumber),
          XMLELEMENT("samplesubtype", samplesubtype),
          XMLELEMENT("ecl", ecl),
          XMLELEMENT("ecl_sig", FormatSig(ecl_sig, &&ECLFigures&)),
          XMLELEMENT("status", concentrationstatus),
          XMLELEMENT("time", time),
          XMLELEMENT("deactivated", isdeactivated)
        ) order by runsamplesequencenumber
      )
    ) as val,
    XMLELEMENT("result",
      XMLELEMENT("m", Count(ecl)),
      XMLELEMENT("n", Count(useecl)),
      XMLELEMENT("result", result),
      XMLELEMENT("ncmedian",
        case
          when '&&ECLFullPrecision&' = 'T' then
            FmtNum(ncmedian)
          when '&&FinalAccuracy&' = 'T' then
            FormatSig(sigFigure(ncmedian, &&ECLFigures&), &&ECLFigures&)
          else
            FormatSig(sigFigure(ncmedian_sig, &&ECLFigures&), &&ECLFigures&)
        end),
      XMLELEMENT("cutpoint",
        case
          when '&&ECLFullPrecision&' = 'T' then
            FmtNum(cutpoint)
          when '&&FinalAccuracy&' = 'T' then
            FormatSig(sigFigure(cutpoint, &&ECLFigures&), &&ECLFigures&)
          else
            FormatSig(sigFigure(cutpoint_sig, &&ECLFigures&), &&ECLFigures&)
        end),
      XMLELEMENT("mean",
        case
          when '&&ECLFullPrecision&' = 'T' then
            FmtNum(Avg(useecl))
          when '&&FinalAccuracy&' = 'T' then
            FormatSig(sigFigure(Avg(useecl), &&ECLFigures&), &&ECLFigures&)
          else
            FormatSig(sigFigure(Avg(useecl_sig), &&ECLFigures&), &&ECLFigures&)
        end),
      XMLELEMENT("sd", FormatSig(sigFigure(case when '&&ECLFullPrecision&' = 'T' or '&&FinalAccuracy&' = 'T' then Stddev(useecl) else Stddev(useecl_sig) end, &&ConcSDFigures&), &&ConcSDFigures&)),
      XMLELEMENT("cv", FormatRounded(round(
                        case
                          when '&&ECLFullPrecision&' = 'T' or '&&FinalAccuracy&' = 'T' then 
                            sigFigure(Stddev(useecl), &&ConcSDFigures&)/Avg(useecl)*100
                          else
                            sigFigure(Stddev(useecl_sig), &&ConcSDFigures&)/sigFigure(Avg(useecl_sig), &&ECLFigures&)*100
                        end, &&DecPlPercCV&), &&DecPlPercCV&)),
      XMLELEMENT("ratio", FormatRounded(round(
                            case
                              when '&&ECLFullPrecision&' = 'T' or '&&FinalAccuracy&' = 'T' then 
                                Avg(useecl)/cutpoint
                              else
                                sigFigure(Avg(useecl_sig), &&ECLFigures&)/cutpoint_sig
                            end, &&DecPlNCdivCP&), &&DecPlNCdivCP&)),
      XMLELEMENT("signal-to-noise",  FormatRounded(round(
                                      case
                                        when '&&ECLFullPrecision&' = 'T' or '&&FinalAccuracy&' = 'T' then 
                                          Avg(useecl)/ncmedian
                                        else
                                          sigFigure(Avg(useecl_sig), &&ECLFigures&)/sigFigure(ncmedian_sig, &&ECLFigures&)
                                      end, &&DecPlSN&), &&DecPlSN&)),
      XMLELEMENT("flag",
                case
                  when (round(sigFigure(Stddev(useecl_sig), &&ConcSDFigures&)/sigFigure(Avg(useecl_sig), &&ECLFigures&)*100, &&DecPlPercCV&)) > 20 then 'T'
                  else 'F'
                end
      )
    ) as res
  FROM (
    SELECT
      nc.studyid, nc.rid, nc.runid, nc.analytename,
      -- NC:
      nc.cutpoint, nc.cutpoint_sig, nc.medianecl ncmedian, nc.medianecl_sig ncmedian_sig,
      -- Screens:
      designsubjecttag, subjectid, designSampleID, samplingtime, time, timetext, studyday, dosegroupno, dosegroup, doseamount, doseunit,
      posgroup, sampleid, sampleresultid, samplesubtype, runsamplesequencenumber,
      ecl, sigFigure(ecl, &&ECLFigures&) ecl_sig,
      case when (isdeactivated='F') then ecl else null end useecl, case when (isdeactivated='F') then sigFigure(ecl, &&ECLFigures&) else null end useecl_sig,
      concentrationstatus, result, source, concentrationunit,
      sigFigure(Avg(sigFigure(case when(isdeactivated='F') then ecl else null end, &&ECLFigures&)) over (partition by nc.studyid, nc.analytename, dosegroup, subjectid, designSampleID), &&ECLFigures&) mean_ecl_sig,
      isdeactivated,
      count(ecl) over (partition by nc.studyid, nc.analytename) m,
      count(case when (isdeactivated='F') then ecl else null end) over (partition by nc.studyid, nc.analytename) n,
      count(case when lower(result) like 'positive%' then ecl else null end) over (partition by nc.studyid, nc.analytename) posm,
      cutpointfactor
    FROM (
      SELECT
        r.studyid,
        r.id rid,
        r.runid,
        sa.id analyteid,
        &&AnalyteName& analytename,
        sa.analyteorder,
        median(sr.analytearea) medianecl,
        median(sigFigure(sr.analytearea, &&ECLFigures&)) medianecl_sig,
        median(sr.analytearea) * min(a.cutpointfactor) cutpoint,
        median(sigFigure(sr.analytearea, &&ECLFigures&)) * min(a.cutpointfactor) cutpoint_sig,
        min(a.cutpointfactor) over (partition by sa.id) cutpointfactor
      FROM 
        &&TempTabPrefix&bio$run r,
        &&TempTabPrefix&bio$run$analytes ra,
        &&TempTabPrefix&bio$study$analytes sa,
        &&TempTabPrefix&bio$assay a,
        &&TempTabPrefix&bio$run$worklist s,
        &&TempTabPrefix&bio$run$worklist$result$rw sr,
        table(&&LALPackage&.GetRunStates) rs
      WHERE r.ID = s.runID
      AND s.ID = sr.worklistID
      AND a.ID = r.assayID
      --AND s.treatmentid = 'ADA'
      --AND s.studyid = '&&StudyID&' AND sr.studyID = '&&StudyID&'
      AND r.studyid = '&&StudyID&'
      AND a.studyID = '&&StudyID&'
      AND rs.column_value = r.runstatusnum
      AND sr.runAnalyteCode = ra.code
      AND r.ID = ra.runID
      AND ra.analyteID = sa.ID
      AND s.runsamplekind = '&&NCKnownType&'
      &&AddConditionNC&
      GROUP BY r.studyid, sa.name, sa.analyteorder, sa.id, r.id, r.runid, a.cutpointfactor
    ) nc , (
      SELECT
        r.studyid,
        s.id sampleid,
        sr.id sampleresultid,
        sam.subjectid,
        sam.designsubjecttag,
        s.designsampleid,
        r.id rid,
        r.runid,
        sa.id analyteid,
        s.samplesubtype,
        s.runsamplesequencenumber,
        sr.analytearea ecl,
        s.status,
        sr.commenttext,
        sr.concentrationstatus,
        max(sr.concentrationstatus) over (partition by s.designsampleid, r.id) result,
        s.source,
        s.concentrationunit,
        s.time,
        s.timetext,
        s.studyday,
        t.name dosegroup,
        max(rh.reported) over (partition by s.designsampleid, r.id) reported,
        case
            /*when REGEXP_LIKE(sg.name, '^[+-]?\d?\.?\d+$','i') then
                to_number(sg.name)
*/
            when REGEXP_LIKE(t.name, '^[+-]?\d?\.?\d+$','i') then
                to_number(t.name)
            when REGEXP_LIKE(t.description, '^[+-]?\d?\.?\d+$','i') then
                to_number(t.description)
            else
              null
        end dosegroupno,
        t.doseamount,
        t.doseunitsdescription doseunit,
        sam.samplingtime,
        s.samplesubtype posgroup,
        case
          when s.runcode || '-' || s.runsamplesequencenumber || '-' || sa.analyteorder in (select code from &&TempTabPrefix&bio$deactivated$samples) then 'T'
          else 'F'
        end isdeactivated
      FROM 
        &&TempTabPrefix&bio$run r,
        &&TempTabPrefix&bio$run$analytes ra,
        &&TempTabPrefix&bio$study$analytes sa,
        &&TempTabPrefix&bio$assay a,
        &&TempTabPrefix&bio$run$worklist s,
        &&TempTabPrefix&bio$run$worklist$result$rw sr,
        &&TempTabPrefix&bio$sample sam,
        &&TempTabPrefix&bio$study$treatment t,
        &&TempTabPrefix&bio$reassay$history rh,
        table(&&LALPackage&.GetRunStates) rs
      WHERE r.ID = s.runID
      AND s.ID = sr.worklistID
      AND a.ID = r.assayID
      AND s.designSampleID = sam.ID
      AND r.studyid = '&&StudyID&'
      AND a.studyID = '&&StudyID&'
      AND rs.column_value = r.runstatusnum
      AND sr.runAnalyteCode = ra.code
      AND r.ID = ra.runID
      AND ra.analyteID = sa.ID
      AND sam.treatmentcode = t.code
      --s.runcode || '-' || s.runsamplesequencenumber || '-' || sa.analyteorder in (select code from &&TempTabPrefix&bio$deactivated$samples)
      AND r.code || '-' || s.runsamplesequencenumber || '-' || sa.analyteorder = rh.runsamplecode (+)
      &&AddConditionSample&
    ) sample
    WHERE sample.studyID = nc.studyID
    AND sample.rID = nc.rID
    AND sample.runid = nc.runid
    AND sample.analyteID = nc.analyteID
    &&AddCondition&
  )
  &&OverallCondition&
  GROUP BY studyid, analytename, dosegroupno, dosegroup, designsubjecttag, subjectid, designSampleID, samplingtime, time, timetext, studyday, doseamount, doseunit, runid, rid, cutpoint, cutpoint_sig, ncmedian, ncmedian_sig, posgroup, source, concentrationunit, result, n, m, posm, cutpointfactor)
GROUP BY studyid, analytename
