 (SELECT Xmlagg (XMLELEMENT("calculation", 
      XMLELEMENT("target",         
        XMLELEMENT("analyteid",analyteid),
        XMLELEMENT("species", species),
        XMLELEMENT("matrix", matrix),
        XMLELEMENT("regparam", '&&ParamName&')
        ),
      XMLELEMENT("values", Xmlagg(
        XMLELEMENT("run",  XMLATTRIBUTES(runid AS "runid" ),
          XMLELEMENT("&&ParamName&", &&OwnFormatFunc&(&&OwnRepresentationFunc&(&&ParamName&,&&OwnRepresentationParam&),&&OwnRepresentationParam&))
        ) order by runnr)),
      XMLELEMENT("result",       
        XMLELEMENT("n", Count(&&ParamName&)),
        XMLELEMENT("mean", &&OwnFormatFunc&(&&OwnRepresentationFunc&(avg(case when '&&FinalAccuracy&'='T' then &&ParamName&
                                                                              else &&OwnRepresentationFunc&(&&ParamName&,&&OwnRepresentationParam&)
                                                                         end),&&OwnRepresentationParam&),&&OwnRepresentationParam&)),
        XMLELEMENT("sd", FormatRounded(round(Stddev(case when '&&FinalAccuracy&'='T' then &&ParamName&
                                                         else &&OwnRepresentationFunc&(&&ParamName&,&&OwnRepresentationParam&)                                             
                                                    end),&&DecPlRegparamSD&),&&DecPlRegparamSD&)),
        XMLELEMENT("cv", case when &&OwnRepresentationFunc&(Avg(&&ParamName&),&&OwnRepresentationParam&) = 0 then null 
                              else FormatRounded(round(case when '&&FinalAccuracy&'='T' then Stddev(&&ParamName&)/Avg(&&ParamName&)*100
                                                            else round(Stddev(&&OwnRepresentationFunc&(&&ParamName&,&&OwnRepresentationParam&)),&&DecPlRegparamSD&)/
                                                                 &&OwnRepresentationFunc&(Avg(&&OwnRepresentationFunc&(&&ParamName&,&&OwnRepresentationParam&)),&&OwnRepresentationParam&)*100 
                                                       end,&&DecPlRegparamPercCV&),&&DecPlRegparamPercCV&) 
                         end )                        
      ))) 
    FROM
    (SELECT distinct r.id runid, r.runid runnr, ra.analyteid,
     &&SpeciesMatrix&
     &&ParamName&
     FROM &&TempTabPrefix&BIO$RUN$ANALYTES ra,
          &&TempTabPrefix&bio$run r,
          &&TempTabPrefix&bio$assay a,            --neu
          table(&&LALPackage&.GetRunStates) rs
     WHERE ra.runid=r.ID
       AND r.studyid = '&&StudyID&' AND r.studyid = '&&StudyID&'
       AND r.RuntypeDescription in  (&&RunTypes&)
       and a.id = r.assayid
       AND ( rs.column_value = '&&AllRunStates&' or rs.column_value=r.runstatusnum) &&AddCondition&)  
    GROUP BY analyteid, species, matrix, '&&ParamName&') 