  SELECT XMLELEMENT("experiment", XMLATTRIBUTES('&&Experiment&' AS "name" ,'anomalous' AS "type"),
   (
   select Xmlagg(XMLConcat(
     Xmlagg(XMLELEMENT("calculation", xmlconcat(targ,val,res)) order by analyteorder, species, matrix),
     XMLELEMENT("statistic", XMLATTRIBUTES(analyteid AS "analyteid", species AS "species", matrix AS "matrix", runid AS "runid", internalstdname AS "internalstdname" ),
       XMLELEMENT("numtotal", sum(cnt)),
       --XMLELEMENT("numok", sum(cntok)),
       --XMLELEMENT("numokana", sum(ana_cntok)),
       --XMLELEMENT("percok", case when sum(cnt) != 0 then FmtNum(round(sum(cntok)/sum(cnt)*100, &&DecPlPercStat&)) else null end),
       --XMLELEMENT("percokana",case when sum(anacnt) != 0 then FmtNum(round(sum(ana_cntok)/sum(anacnt)*100, &&DecPlPercStat&)) else null end),
       XMLELEMENT("flagpercentpeakis", min(flagpercentpeakis)),
       XMLELEMENT("flagpercentpeakanalyte", min(flagpercentpeak))
       )) order by analyteorder, species, matrix)
  from (
  SELECT
      analyteid, analyteorder, species, matrix, runid, internalstdname,
      --Count(case when useanalytearea is not null then 1 else null end) anacnt,
      Count(case when useinternalstandardarea is not null then 1 else null end) cnt,
      --Count(case when percent < flagpercentpeak then 1 else null end) ana_cntok,
      --Count(case when is_percent < flagpercentpeakis then 1 else null end) cntok,
      min(flagpercentpeakis) flagpercentpeakis, min(flagpercentpeak) flagpercentpeak,
      count(internalstandardarea) cnttotal,
      XMLELEMENT("target",
        XMLELEMENT("analyteid", analyteid),
        XMLELEMENT("species", species),
        XMLELEMENT("matrix", matrix),
        XMLELEMENT("runid", runid),
        XMLELEMENT("internalstdname", internalstdname)
        ) targ,
      XMLELEMENT("values", Xmlagg(
        XMLELEMENT("sample",  XMLATTRIBUTES(sampleid AS "sampleid", sampleresultrawid AS "sampleresultrawid" ),
          --XMLELEMENT("knownid", knownid),
          XMLELEMENT("knownname", knownname),
          XMLELEMENT("name", name),
          XMLELEMENT("exportsamplename", exportsamplename),
          XMLELEMENT("lotnumber", lotnumber),
          XMLELEMENT("runsamplesequencenumber", runsamplesequencenumber),
          --XMLELEMENT("peak", &&PeakFormatFunc&(analytearea, &&PeakFigures&)),
          XMLELEMENT("ispeak", &&PeakFormatFunc&(internalstandardarea, &&PeakFigures&)),
          XMLELEMENT("known_min_ispeak", &&PeakFormatFunc&(k_minisarea, &&PeakFigures&)),
          XMLELEMENT("known_max_ispeak", &&PeakFormatFunc&(k_maxisarea, &&PeakFigures&)),
          XMLELEMENT("known_min_low_ispeak", &&PeakFormatFunc&(k_low_minisarea, &&PeakFigures&)),
          XMLELEMENT("known_max_high_ispeak", &&PeakFormatFunc&(k_high_maxisarea, &&PeakFigures&)),
          --XMLELEMENT("percent", FormatRounded(percent, &&DecPlPercBias&)),
          --XMLELEMENT("is-percent", FormatRounded(is_percent, &&DecPlPercBias&)),
          XMLELEMENT("anomalous", isanomalous),
          XMLELEMENT("deactivated", isdeactivated)
        )
        order by runsamplesequencenumber)) val,
      XMLELEMENT("result",
        XMLELEMENT("n", Count(useinternalstandardarea)),
        --XMLELEMENT("cntok", Count(case when percent < flagpercentpeak then 1 else null end )),
        XMLELEMENT("flagpercentpeakanalyte", min(flagpercentpeak)),
        --XMLELEMENT("isn", Count(useinternalstandardarea)),
        --XMLELEMENT("iscntok", Count(case when is_percent < flagpercentpeakis then 1 else null end )),
        XMLELEMENT("flagpercentpeakis", min(flagpercentpeakis))
        --XMLELEMENT("std-name", min(k_name)),
        --XMLELEMENT("std-peak", &&PeakFormatFunc&(min(std_analytearea), &&PeakFigures&)),
        --XMLELEMENT("std-is-peak", &&PeakFormatFunc&(min(k_internalstandardarea), &&PeakFigures&))
      ) res
    FROM
    (
    SELECT --knownID,
           sampleID,  sampleresultrawID, runsamplesequencenumber, &&FlagPercentPeak& FlagPercentPeak, &&FlagPercentPeakIS& FlagPercentPeakIS,
           knownname, lotnumber, NAME, exportsamplename, runid, analyteid, analyteorder, species, matrix,
           INTERNALSTANDARDAREA, useinternalstandardarea, internalstdname, ISDEACTIVATED,
           --round(useinternalstandardarea/std_internalstandardarea*100, &&DecPlPercBias&) is_percent,
           k_minisarea,
           k_maxisarea,
           k_low_minisarea,
           k_high_maxisarea,
           case
             when &&PeakRepresentationFunc&(internalstandardarea, &&PeakFigures&) < &&PeakRepresentationFunc&(k_low_minisarea, &&PeakFigures&)
               or &&PeakRepresentationFunc&(internalstandardarea, &&PeakFigures&) > &&PeakRepresentationFunc&(k_high_maxisarea, &&PeakFigures&)
             then 'T'
             else 'F'
           end isanomalous
    FROM
    (select --knownID,
            sampleID,  sampleresultrawID, runsamplesequencenumber, loc.INTERNALSTANDARDAREA, loc.useinternalstandardarea, internalstdname,
            knownname, /*Stb$util.getRightDelim(loc.name,'_')*/ lotnumber, loc.NAME, loc.exportsamplename,
            loc.runid, loc.analyteid, loc.analyteorder, loc.species, loc.matrix, ISDEACTIVATED,
            --knowns.min_insarea k_minisarea,
            --knowns.max_insarea k_maxisarea,
            min( knowns.minisarea ) over (partition by knowns.analyteid, knowns.analyteorder, knowns.species, knowns.matrix) as k_minisarea,
            max( knowns.maxisarea ) over (partition by knowns.analyteid, knowns.analyteorder, knowns.species, knowns.matrix) as k_maxisarea,
            min( knowns.minisarea ) over (partition by knowns.analyteid, knowns.analyteorder, knowns.species, knowns.matrix) * 0.50 as k_low_minisarea,
            max( knowns.maxisarea ) over (partition by knowns.analyteid, knowns.analyteorder, knowns.species, knowns.matrix) * 1.50 as k_high_maxisarea--,
            --knowns.name k_name
     FROM
     (SELECT --K.ID knownID,
        s.ID sampleID, srw.ID sampleresultrawID, s.runsamplesequencenumber, srw.internalstdname,
        s.NAME, s.oldname exportsamplename, s.knownname knownname, '' lotnumber, s.runid, ra.analyteid, sa.analyteorder, a.species, a.sampletypeid matrix, srw.ratio, s.ISDEACTIVATED,
        &&PeakRepresentationFunc&(srw.internalstandardarea, &&PeakFigures&) internalstandardarea,
        case when s.ISDEACTIVATED = 'T' then null
             else case when '&&FinalAccuracy&' = 'T' then srw.internalstandardarea
                       else &&PeakRepresentationFunc&(srw.internalstandardarea, &&PeakFigures&)
                  end
        end useinternalstandardarea
      FROM &&TempTabPrefix&bio$run r,
           &&TempTabPrefix&bio$run$analytes ra,
           &&TempTabPrefix&bio$study$analytes sa,
           &&TempTabPrefix&bio$assay a,
           &&TempTabPrefix&bio$run$samples s,
           &&TempTabPrefix&bio$run$sample$result$raw srw
      WHERE R.ID = s.runid AND R.ID = srw.runid
        --AND r.studyassayid =s.studyassayid AND s.studyassayid = srw.studyassayid
        AND r.RuntypeDescription in (&&RunTypes&)
        AND s.ID = srw.runsampleID
        AND s.sampletype = 'unknown'
        AND ra.id = srw.runanalyteid
        AND sa.id = ra.analyteid
        AND a.id = r.assayid
        AND ra.runid = s.runid
        AND ra.studyID = '&&StudyID&'
        AND s.studyid = '&&StudyID&'
        AND srw.studyID = '&&StudyID&' AND r.studyid = '&&StudyID&'
        ) loc,
     (
     SELECT s.RUNID, ra.analyteid, sa.analyteorder, a.species, a.sampletypeid matrix,
             round(min(
               case when s.ISDEACTIVATED = 'T' then null
                    else case when '&&FinalAccuracy&' = 'T' then srw.INTERNALSTANDARDAREA
                              else &&PeakRepresentationFunc&(srw.INTERNALSTANDARDAREA, &&PeakFigures&)
                         end
               end), &&PeakFigures&) minisarea ,
             round(max(
               case when s.ISDEACTIVATED = 'T' then null
                    else case when '&&FinalAccuracy&' = 'T' then srw.INTERNALSTANDARDAREA
                              else &&PeakRepresentationFunc&(srw.INTERNALSTANDARDAREA, &&PeakFigures&)
                         end
               end), &&PeakFigures&) maxisarea
      FROM &&TempTabPrefix&bio$run r,
           &&TempTabPrefix&bio$run$analytes ra,
           &&TempTabPrefix&bio$study$analytes sa,
           &&TempTabPrefix&bio$assay a,
           &&TempTabPrefix&bio$run$samples s,
           &&TempTabPrefix&bio$run$sample$results sr,
           &&TempTabPrefix&bio$run$sample$result$raw srw
      WHERE
        srw.RUNSAMPLECODE = s.code
        and srw.RUNSAMPLECODE = s.code
        and srw.sampleresultcode = sr.code
        AND r.ID = s.runid AND r.ID = srw.runid
        --AND r.studyassayid =s.studyassayid AND s.studyassayid = srw.studyassayid
        AND r.RuntypeDescription in (&&RunTypes&)
        AND s.sampletype = 'known'
        AND ra.id = srw.runanalyteid
        AND sa.id = ra.analyteid
        AND a.id = r.assayid
        AND ra.runid = s.runid
        AND ra.studyID = '&&StudyID&'
        AND s.studyid = '&&StudyID&'
        AND srw.studyID = '&&StudyID&' AND r.studyid = '&&StudyID&'
        and s.ISDEACTIVATED = 'F'
        and regexp_like(s.knownname,'&&IncludedKnowns&')
        and (s.dilutionfactor is null or s.dilutionfactor = 1)
        and sr.nominalconcentrationconv != 0
        and (
          (
            s.runsamplekind = 'STANDARD'
            and round(case when '&&FinalAccuracy&' = 'T' then (sr.concentration-sr.nominalconcentration)*100 / sr.nominalconcentration
                      else (&&ConcRepresentationFunc&(sr.concentration, &&ConcFigures&)-&&ConcRepresentationFunc&(sr.nominalconcentration, &&ConcFigures&))*100/
                         &&ConcRepresentationFunc&(sr.nominalconcentration, &&ConcFigures&)
                      end, &&DecPlPercBias&) < case when round(ra.lloq,10) = round(sr.nominalconcentration,10) then 20 else 15 end
          ) or (
            s.runsamplekind = 'KNOWN'
            and round(case when '&&FinalAccuracy&' = 'T' then (sr.concentration-sr.nominalconcentration)*100 / sr.nominalconcentration
                      else (&&ConcRepresentationFunc&(sr.concentration, &&ConcFigures&)-&&ConcRepresentationFunc&(sr.nominalconcentration, &&ConcFigures&))*100/
                         &&ConcRepresentationFunc&(sr.nominalconcentration, &&ConcFigures&)
                      end, &&DecPlPercBias&) < 15
          )
        )
      group by r.runid, s.RUNID, ra.analyteid, sa.analyteorder, a.species, a.sampletypeid
      ) knowns
      where knowns.RUNID = loc.runid
        AND knowns.analyteid = loc.analyteid
        AND knowns.species = loc.species
        AND knowns.matrix = loc.matrix
    )
    --WHERE internalstandardarea > k_low_minisarea and internalstandardarea < k_high_maxisarea
    )
    GROUP BY runid, analyteid, analyteorder, species, matrix, internalstdname)
    GROUP BY analyteid, analyteorder, species, matrix, runid, internalstdname)
    ) FROM dual