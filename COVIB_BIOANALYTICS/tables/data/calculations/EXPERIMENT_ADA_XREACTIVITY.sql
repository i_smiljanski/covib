SELECT XMLELEMENT("experiment", XMLATTRIBUTES('&&Experiment&' AS "name" ,'xreactivity_response' AS "type"),
      Xmlagg ( XMLConcat(
        Xmlagg (XMLELEMENT("calculation", xmlconcat(targ,val,res)) order by studyid, acceptedrun, analyteid, species, matrix, concentrationunit, concentrationunit, typeorder, suborder, s2_source)
        --XMLELEMENT("statistic", XMLATTRIBUTES(analyteid AS "analyteid"),
               
             --)) order by studyid, analyteid, species, matrix
      )))
  FROM (
  SELECT
    studyid, analyteid, species, matrix, acceptedrun, concentrationunit, s2_source,
    case
      when s2_type like 'EXREL%' then 1
      when s2_type like 'IRREL%' then 2
      else null
    end typeorder,
    case
      when regexp_like(s2_type, '^.*(\d+)$', 'i') then to_number(regexp_replace(s2_type, '^.*(\d+)$', '\1', 1, 1, 'i'))
      else 0
    end suborder,
    --sum(s1_n)+sum(s2_n) n, sum(s1_m)+sum(s2_m) m,
    XMLELEMENT("target",
      XMLELEMENT("studyid", studyid),
      XMLELEMENT("analyteid", analyteid),
      XMLELEMENT("species", species),
      XMLELEMENT("matrix", matrix),
      XMLELEMENT("acceptedrun", acceptedrun),
      XMLELEMENT("conc-unit", concentrationunit),
      XMLELEMENT("reagent", s2_source),
      XMLELEMENT("type", s2_type)
    ) as targ,
    XMLELEMENT("values",
      Xmlagg(
        XMLELEMENT("sample",
          XMLELEMENT("dilution", dilution),
          XMLELEMENT("runid", rid),
          XMLELEMENT("runno", runid),
          XMLELEMENT("nominalconc", &&FormatFuncConc&(&&RepresentationFuncConc&(nominalconc, &&PlacesConc&), &&PlacesConc&)),
          XMLELEMENT("subject", subject),
          XMLELEMENT("sample1", s1_xml),
          XMLELEMENT("sample2", s2_xml),
          XMLELEMENT("nc", nc_xml),
          XMLELEMENT("s1-source", s1_source),
          XMLELEMENT("s2-source", s2_source),
          XMLELEMENT("s1-mean", FormatRounded(round(s1_mean_useresponse, &&DecPlResponse&), &&DecPlResponse&)),
          XMLELEMENT("s2-mean", FormatRounded(round(s2_mean_useresponse, &&DecPlResponse&), &&DecPlResponse&)),
          XMLELEMENT("nc-meanmedian", FormatRounded(round(nc_meanmedian_useresponse, &&DecPlResponse&), &&DecPlResponse&)),
          XMLELEMENT("screen-cp", FormatRounded(round(screen_cp, &&DecPlCutPoint&), &&DecPlCutPoint&)),
          XMLELEMENT("confirm-cp", FormatRounded(round(confirm_cp, &&DecPlConfirmCutPoint&), &&DecPlConfirmCutPoint&)),
          XMLELEMENT("titer-cp",
            case
              when '&&FixedTiterCutpoint&' is not null then 
                FormatRounded(round(to_number('&&FixedTiterCutpoint&'), &&DecPlCutPoint&), &&DecPlCutPoint&)
              when '&&TiterCPNFactor&' is not null then
                FormatRounded(round(nc_meanmedian_useresponse*to_number('&&TiterCPNFactor&'), &&DecPlCutPoint&), &&DecPlCutPoint&)
              else null
            end
          ),
          XMLELEMENT("n", s1_n+s2_n),
          XMLELEMENT("s1-n", s1_n),
          XMLELEMENT("s2-n", s2_n),
          XMLELEMENT("m", s1_m+s2_m),
          XMLELEMENT("s1-m", s1_m),
          XMLELEMENT("s2-m", s2_m),
          XMLELEMENT("nc-n", nc_n),
          XMLELEMENT("nc-m", nc_m),
          XMLELEMENT("s1-cv", FormatRounded(round(s1_cv, &&DecPlPercCV&), &&DecPlPercCV&)),
          XMLELEMENT("s2-cv", FormatRounded(round(s2_cv, &&DecPlPercCV&), &&DecPlPercCV&)),
          XMLELEMENT("nc-cv", FormatRounded(round(nc_cv, &&DecPlPercCV&), &&DecPlPercCV&)),
          XMLELEMENT("s1-cvflag", 
            case
              when s1_cv > &&PrecisionAcceptanceCriteria& then 'T'
              else 'F'
            end
          ),
          XMLELEMENT("s2-cvflag",
            case
              when s2_cv > &&PrecisionAcceptanceCriteria& then 'T'
              else 'F'
            end
          ),
          XMLELEMENT("nc-cvflag",
            case
              when nc_cv > &&PrecisionAcceptanceCriteria& then 'T'
              else 'F'
            end
          ),
          XMLELEMENT("s1-ratio", FormatRounded(round(
            case
              when nc_meanmedian_useresponse = 0 then null
              else s1_mean_useresponse / nc_meanmedian_useresponse--*100
            end
          , &&DecPlRatio&), &&DecPlRatio&)),
          XMLELEMENT("difference", FormatRounded(round(
            case
              when s1_mean_useresponse = 0 then null
              else (s1_mean_useresponse - s2_mean_useresponse) / s1_mean_useresponse * 100
            end
          , &&DecPlInhibition&), &&DecPlInhibition&)),
          XMLELEMENT("inhibition", FormatRounded(round(abs(
            case
              when s1_mean_useresponse = 0 then null
              else (s1_mean_useresponse - s2_mean_useresponse) / s1_mean_useresponse * 100
          end
          ), &&DecPlInhibition&), &&DecPlInhibition&)),
          XMLELEMENT("s1-screen",
            case
              when s1_mean_useresponse is null then null
              when s1_mean_useresponse >= screen_cp then 'Positive'
              else 'Negative'
            end
          ),
          XMLELEMENT("confirm",
            case
              when s2_mean_useresponse is null or confirm_cp is null then null
              else
                'Confirmed ' ||
                case
                  when --round(
                    case
                      when s1_mean_useresponse = 0 then null
                      else (s1_mean_useresponse - s2_mean_useresponse) / s1_mean_useresponse * 100
                    end
                  /*, &&DecPlInhibition&)*/ >= confirm_cp then 'Positive'
                  else 'Negative'
                end
            end
          )
          --XMLELEMENT("status", resulttext),
          --XMLELEMENT("deactivated", isdeactivated)
        ) order by runid, nominalconc, subject
      )
    ) as val,
    XMLELEMENT("result", 
      XMLELEMENT("s1-mean", FormatRounded(round(Avg(s1_mean_useresponse), &&DecPlResponse&), &&DecPlResponse&)),
      XMLELEMENT("s1-sd", &&FormatFuncResponseSD&(&&RepresentationFuncResponseSD&(Stddev(s1_mean_useresponse), &&PlacesResponseSD&), &&PlacesResponseSD&)),
      XMLELEMENT("s1-cv", FormatRounded(round(
        case
          when Avg(s1_mean_useresponse) = 0 then null
          else Stddev(s1_mean_useresponse) / Avg(s1_mean_useresponse) * 100
        end
      , &&DecPlPercCV&), &&DecPlPercCV&)),
      XMLELEMENT("s2-mean", FormatRounded(round(Avg(s2_mean_useresponse), &&DecPlResponse&), &&DecPlResponse&)),
      XMLELEMENT("s2-sd", &&FormatFuncResponseSD&(&&RepresentationFuncResponseSD&(Stddev(s2_mean_useresponse), &&PlacesResponseSD&), &&PlacesResponseSD&)),
      XMLELEMENT("s2-cv", FormatRounded(round(
        case
          when Avg(s2_mean_useresponse) = 0 then null
          else Stddev(s2_mean_useresponse) / Avg(s2_mean_useresponse) * 100
        end
      , &&DecPlPercCV&), &&DecPlPercCV&))
    ) as res
  FROM (
    SELECT
      studyid, analyteid, rid, runid, acceptedrun, subject, nominalconc, dilution, concentrationunit,
      species, matrix, screen_cp, confirm_cp,
      s1_source, s1_mean_response, s1_mean_useresponse,
      s1_n, s1_m, s1_sd,
      case
        when s1_mean_useresponse = 0 then null
        else s1_sd / s1_mean_useresponse * 100
      end s1_cv, s1_xml,
      s2_mean_response, s2_mean_useresponse, s2_source, s2_type,
      s2_n, s2_m, s2_sd,
      case
        when s2_mean_useresponse = 0 then null
        else s2_sd / s2_mean_useresponse * 100
      end s2_cv, s2_xml,
      nc_meanmedian_response, nc_meanmedian_useresponse,
      nc_n, nc_m, nc_sd,
      case
        when nc_meanmedian_useresponse = 0 then null
        else nc_sd / nc_meanmedian_useresponse * 100
      end nc_cv, nc_xml
    FROM (
      SELECT
        sample1.studyid, sample1.analyteid, sample1.rid, sample1.runid, sample1.acceptedrun, sample1.subject, sample1.nominalconc, sample1.dilution, sample1.concentrationunit,
        sample1.species, sample1.matrix,
        nc.screen_cp,
        case
          when regexp_like(ccp.key, '^\-?(\d+)?\.?\d+$') then to_number(ccp.key)
          else null
        end confirm_cp,
        sample1.source s1_source, sample1.mean_response s1_mean_response, sample1.mean_useresponse s1_mean_useresponse,
        sample1.n s1_n, sample1.m s1_m, sample1.sd s1_sd, sample1.xml s1_xml,
        sample2.source s2_source, sample2.type s2_type, sample2.mean_response s2_mean_response, sample2.mean_useresponse s2_mean_useresponse,
        sample2.n s2_n, sample2.m s2_m, sample2.sd s2_sd, sample2.xml s2_xml,
        NC.meanmedian_response nc_meanmedian_response, NC.meanmedian_useresponse nc_meanmedian_useresponse,
        NC.n nc_n, NC.m nc_m, NC.sd nc_sd, NC.xml nc_xml
      FROM
        (
          SELECT
            studyid, analyteid, rid, runid, acceptedrun, subject, nominalconc, concentrationunit, dilution, source,
            species, matrix,
            Avg(response) mean_response,
            Avg(case when(isdeactivated='F') then response else null end) mean_useresponse,
            Stddev(case when(isdeactivated='F') then response else null end) sd,
            count(case when(isdeactivated='F') then 1 else null end) n,
            count(1) m,
            Xmlagg(
              XMLELEMENT("sample",
                XMLATTRIBUTES(sampleid AS "sampleid", sampleresultid AS "sampleresultid" ),
                XMLELEMENT("runsamplesequencenumber", runsamplesequencenumber),
                XMLELEMENT("samplename", samplename),
                XMLELEMENT("samplesubtype", samplesubtype),
                XMLELEMENT("response", FormatRounded(round(response, &&DecPlResponse&), &&DecPlResponse&)),
                XMLELEMENT("status", resulttext),
                XMLELEMENT("deactivated", isdeactivated),
                XMLELEMENT("reason", reason)
              ) order by runsamplesequencenumber
            ) xml
          FROM (
            SELECT
              r.studyid, s.id sampleid, sr.id sampleresultid,
              s.designsampleid, s.samplename,
              case
                when regexp_like(s.samplename, '^\D+(\d+).*$', 'i') then to_number(regexp_replace(s.samplename, '^\D+(\d+).*$', '\1', 1, 1, 'i'))
                else null
              end subject,
              r.id rid, r.runid, sa.id analyteid, s.samplesubtype,
              case when ra.RunAnalyteRegressionStatus = &&AcceptedRunAnalyte& then 'T' else 'F' end acceptedrun,
              s.runsamplesequencenumber, sr.analytearea response,
              s.status, sr.commenttext, sr.concentration conc,
              sr.resulttext, s.source, d.reason,
              k.concentration nominalconc, s.dilution,
              sr.concentrationunits concentrationunit, a.species, a.sampletypeid matrix,
              case
                when d.code is not null then 'T'
                else 'F'
              end isdeactivated
            FROM 
              &&TempTabPrefix&bio$run r,
              &&TempTabPrefix&bio$run$analytes ra,
              &&TempTabPrefix&bio$study$analytes sa,
              &&TempTabPrefix&bio$assay a,
              &&TempTabPrefix&bio$run$worklist s,
              &&TempTabPrefix&bio$run$worklist$result$rw sr,
              &&TempTabPrefix&bio$run$ana$known k,
              &&TempTabPrefix&bio$covib$run$sample$deact d,
              isr$crit kwz--,
              --table(&&LALPackage&.GetRunStates) rs
            WHERE r.ID = s.runID
            AND s.ID = sr.worklistID
            AND a.ID = r.assayID
            AND r.studyID = '&&StudyID&' AND a.studyID = '&&StudyID&'
            --AND rs.column_value = r.runstatusnum
            AND sr.runAnalyteCode = ra.code
            AND r.ID = ra.runID
            AND ra.analyteID = sa.ID
            AND s.code = d.code (+)
            AND k.runAnalyteCode = ra.code AND k.name = s.sampleName
            AND kwz.key = s.samplename AND kwz.entity = '&&KnownEntity&' AND kwz.masterkey = '&&ExpMasterkey&' AND kwz.repid = &&RepID&
            --and s.runcode || '-' || s.runsamplesequencenumber || '-' || sa.analyteorder in (select code from &&TempTabPrefix&bio$deactivated$samples)
            &&AddConditionS1&
          )
          GROUP BY studyid, analyteid, species, matrix, rid, runid, acceptedrun, subject, nominalconc, concentrationunit, dilution, source
        ) sample1
        LEFT OUTER JOIN (
          SELECT
            studyid, analyteid, rid, runid, subject, nominalconc, concentrationunit, dilution, source, type,
            species, matrix,
            Avg(response) mean_response,
            Avg(case when(isdeactivated='F') then response else null end) mean_useresponse,
            Stddev(case when(isdeactivated='F') then response else null end) sd,
            count(case when(isdeactivated='F') then 1 else null end) n,
            count(1) m,
            Xmlagg(
              XMLELEMENT("sample",
                XMLATTRIBUTES(sampleid AS "sampleid", sampleresultid AS "sampleresultid" ),
                XMLELEMENT("runsamplesequencenumber", runsamplesequencenumber),
                XMLELEMENT("samplename", samplename),
                XMLELEMENT("samplesubtype", samplesubtype),
                XMLELEMENT("response", FormatRounded(round(response, &&DecPlResponse&), &&DecPlResponse&)),
                XMLELEMENT("status", resulttext),
                XMLELEMENT("deactivated", isdeactivated),
                XMLELEMENT("reason", reason)
              ) order by runsamplesequencenumber
            ) xml
          FROM (
            SELECT
              r.studyid, s.id sampleid, sr.id sampleresultid,
              s.designsampleid, s.samplename,
              case
                when regexp_like(s.samplename, '^\D+(\d+).*$', 'i') then to_number(regexp_replace(s.samplename, '^\D+(\d+).*$', '\1', 1, 1, 'i'))
                else null
              end subject,
              regexp_replace(s.samplename, '^.*((IRREL|EXREL)[ _-]?\d?)$', '\1', 1, 1, 'i') type,
              r.id rid, r.runid, sa.id analyteid, s.samplesubtype,
              s.runsamplesequencenumber, sr.analytearea response,
              s.status, sr.commenttext, sr.concentration conc,
              sr.resulttext, s.source, d.reason,
              k.concentration nominalconc, s.dilution,
              sr.concentrationunits concentrationunit, a.species, a.sampletypeid matrix,
              case
                when d.code is not null then 'T'
                else 'F'
              end isdeactivated
            FROM 
              &&TempTabPrefix&bio$run r,
              &&TempTabPrefix&bio$run$analytes ra,
              &&TempTabPrefix&bio$study$analytes sa,
              &&TempTabPrefix&bio$assay a,
              &&TempTabPrefix&bio$run$worklist s,
              &&TempTabPrefix&bio$run$worklist$result$rw sr,
              &&TempTabPrefix&bio$run$ana$known k,
              &&TempTabPrefix&bio$covib$run$sample$deact d,
              isr$crit kwz--,
              --table(&&LALPackage&.GetRunStates) rs
            WHERE r.ID = s.runID
            AND s.ID = sr.worklistID
            AND a.ID = r.assayID
            AND r.studyID = '&&StudyID&' AND a.studyID = '&&StudyID&'
            --AND rs.column_value = r.runstatusnum
            AND sr.runAnalyteCode = ra.code
            AND r.ID = ra.runID
            AND sr.runanalyteCode = ra.Code
            AND ra.analyteID = sa.ID
            AND s.code = d.code (+)
            AND k.runAnalyteCode = ra.code AND k.name = s.sampleName
            AND kwz.key = s.samplename AND kwz.entity = '&&KnownEntity&' AND kwz.masterkey = '&&ExpMasterkey&' AND kwz.repid = &&RepID&
            --and s.runcode || '-' || s.runsamplesequencenumber || '-' || sa.analyteorder in (select code from &&TempTabPrefix&bio$deactivated$samples)
            &&AddConditionS2&
          )
          GROUP BY studyid, analyteid, species, matrix, rid, runid, subject, nominalconc, concentrationunit, dilution, source, type
        ) sample2
        ON sample1.studyID = sample2.studyID
        AND sample1.rID = sample2.rID
        AND sample1.runid = sample2.runid
        AND sample1.analyteID = sample2.analyteID
        AND sample1.species = sample2.species
        AND sample1.matrix = sample2.matrix
        AND sample1.subject = sample2.subject
        AND nvl(sample1.nominalconc,-1) = nvl(sample2.nominalconc,-1)
        AND sample1.concentrationunit = sample2.concentrationunit
        LEFT OUTER JOIN (
          SELECT
            studyid, analyteid, rid, runid, species, matrix,
            &&NCMeanOrMedian&(
              case when stb$util.GetReportParameterValue('MEAN_OR_MEDIAN_NC',&&RepID&) = 'meanofmeans' then
                case when replicatenumber = 1 then meanresponse else null end
              else
                response
              end
            ) meanmedian_response,
            &&NCMeanOrMedian&(
              case when stb$util.GetReportParameterValue('MEAN_OR_MEDIAN_NC',&&RepID&) = 'meanofmeans' then
                case when replicatenumber = 1 then meanuseresponse else null end
              else
                case when(isdeactivated='F') then response else null end
              end
            ) meanmedian_useresponse,
            case
              when '&&FixedScreenCutpoint&' is not null then
                to_number('&&FixedScreenCutpoint&')
              when '&&ScreenCPNFactor&' is not null then
                &&NCMeanOrMedian&(
                  case when stb$util.GetReportParameterValue('MEAN_OR_MEDIAN_NC',&&RepID&) = 'meanofmeans' then
                    case when replicatenumber = 1 then meanuseresponse else null end
                  else
                    case when(isdeactivated='F') then response else null end
                  end
                )*to_number('&&ScreenCPNFactor&')
              else
                &&NCMeanOrMedian&(
                  case when stb$util.GetReportParameterValue('MEAN_OR_MEDIAN_NC',&&RepID&) = 'meanofmeans' then
                    case when replicatenumber = 1 then meanuseresponse else null end
                  else
                    case when(isdeactivated='F') then response else null end
                  end
                )
            end screen_cp,
            Stddev(
              case when stb$util.GetReportParameterValue('MEAN_OR_MEDIAN_NC',&&RepID&) = 'meanofmeans' then
                case when replicatenumber = 1 then meanuseresponse else null end
              else
                case when(isdeactivated='F') then response else null end
              end
            ) sd,
            count(
              case when stb$util.GetReportParameterValue('MEAN_OR_MEDIAN_NC',&&RepID&) = 'meanofmeans' then
                case when replicatenumber = 1 then meanuseresponse else null end
              else
                case when(isdeactivated='F') then response else null end
              end
            ) n,
            count(
              case when stb$util.GetReportParameterValue('MEAN_OR_MEDIAN_NC',&&RepID&) = 'meanofmeans' then case when replicatenumber = 1 then meanresponse else null end
              else response end
            ) m,
            Xmlagg(
              XMLELEMENT("sample",
                XMLATTRIBUTES(sampleid AS "sampleid", sampleresultid AS "sampleresultid" ),
                XMLELEMENT("runsamplesequencenumber", runsamplesequencenumber),
                XMLELEMENT("samplename", samplename),
                XMLELEMENT("samplesubtype", samplesubtype),
                XMLELEMENT("response", FormatRounded(round(response, &&DecPlResponse&), &&DecPlResponse&)),
                XMLELEMENT("status", resulttext),
                XMLELEMENT("deactivated", isdeactivated),
                XMLELEMENT("reason", reason)
              ) order by runsamplesequencenumber
            ) xml
          FROM (
            SELECT
              r.studyid, s.id sampleid, sr.id sampleresultid,
              s.designsampleid, s.samplename, d.reason, s.replicatenumber,
              r.id rid, r.runid, sa.id analyteid, s.samplesubtype,
              s.runsamplesequencenumber, sr.analytearea response,
              s.status, sr.commenttext, sr.concentration conc,
              sr.resulttext, s.source, s.concentration nominalconc,
              sr.concentrationunits concentrationunit, a.species, a.sampletypeid matrix,
              Avg(case when(d.code is null) then sr.analytearea else null end) over (partition by r.studyid, sa.id, r.id, s.samplename) meanresponse,
              Avg(case when(d.code is null) then sr.analytearea else null end) over (partition by r.studyid, sa.id, r.id, s.samplename) meanuseresponse,
              case
                when d.code is not null then 'T'
                else 'F'
              end isdeactivated
            FROM 
              &&TempTabPrefix&bio$run r,
              &&TempTabPrefix&bio$run$analytes ra,
              &&TempTabPrefix&bio$study$analytes sa,
              &&TempTabPrefix&bio$assay a,
              &&TempTabPrefix&bio$run$worklist s,
              &&TempTabPrefix&bio$run$worklist$result$rw sr,
              &&TempTabPrefix&bio$covib$run$sample$deact d,
              isr$crit kwz--,
              --table(ISR$COVIB$BIOANALYTICS$WT$LAL.GetRunStates) rs
            WHERE r.ID = s.runID
            AND s.ID = sr.worklistID
            AND a.ID = r.assayID
            AND r.studyID = '&&StudyID&' AND a.studyID = '&&StudyID&'
            --AND rs.column_value = r.runstatusnum
            AND sr.runAnalyteCode = ra.code
            AND r.ID = ra.runID
            AND sr.runanalyteCode = ra.Code
            AND ra.analyteID = sa.ID
            AND s.code = d.code (+)
            AND kwz.key = s.samplename AND kwz.entity = '&&KnownEntity&' AND kwz.masterkey = '&&ExpMasterkey&' AND kwz.repid = &&RepID&
            --and s.runcode || '-' || s.runsamplesequencenumber || '-' || sa.analyteorder in (select code from &&TempTabPrefix&bio$deactivated$samples)
            &&AddConditionNC&
          )
          GROUP BY studyid, analyteid, species, matrix, rid, runid) NC
      ON sample1.studyID = NC.studyID
      AND sample1.rID = NC.rID
      AND sample1.runid = NC.runid
      AND sample1.analyteID = NC.analyteID
      AND sample1.species = NC.species
      AND sample1.matrix = NC.matrix
      LEFT OUTER JOIN isr$crit ccp
      ON ccp.entity = 'BIO$COVIB$CONFIRM$CUTPOINT' AND ccp.masterkey = 'NORM' AND ccp.repid = &&RepID&
      WHERE sample2.studyID is not null
      &&AddCondition&
    )
 )
 GROUP BY studyid, analyteid, species, matrix, acceptedrun, concentrationunit, s2_source, s2_type)
GROUP BY studyid--, analyteid, species, matrix
