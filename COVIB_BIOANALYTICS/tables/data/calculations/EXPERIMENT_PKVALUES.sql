  SELECT XMLELEMENT("experiment", XMLATTRIBUTES('&&Experiment&' AS "name" ,'pkvalues' AS "type"),
    (SELECT xmlagg ( XMLELEMENT("calculation",
      XMLELEMENT("target",
        XMLELEMENT("treatmentid", treatmentid),
        XMLELEMENT("matrixkey", matrixkey),
        XMLELEMENT("period", period),
        XMLELEMENT("timeident", timeident),
        XMLELEMENT("samplingtime", samplingtime),
        XMLELEMENT("relativesamplingtime", relativesamplingtime),
        XMLELEMENT("analyteid",ana_id),
        XMLELEMENT("unit", concentrationunits),
        XMLELEMENT("gender", gender)
        ),
      XMLELEMENT("values", xmlagg(
        XMLELEMENT("sample", XMLATTRIBUTES(sampleid AS "sampleid", result_id AS "resultid" ),
          XMLELEMENT("designsubjecttag", designsubjecttag),
          XMLELEMENT("concentration", &&ConcFormatFunc&(&&ConcRepresentationFunc&(concentration, &&ConcFigures&), &&ConcFigures&)),
          XMLELEMENT("treatmentid", orig_treatmentid),
          XMLELEMENT("period", orig_period),
          XMLELEMENT("subjectid",subjectid),
          XMLELEMENT("calibrationrangeflag",calibrationrangeflag),
          XMLELEMENT("calibrationrangeconv", &&ConcFormatFunc&(&&ConcRepresentationFunc&(calibrationrangeconv, &&ConcFigures&), &&ConcFigures&)),
          XMLELEMENT("subjecttextvariable1",subjecttextvariable1),
          XMLELEMENT("excluded",excluded)
          ) order by designsubjecttag)),
      XMLELEMENT("result",
        XMLELEMENT("n", count(useconcentration)),
        XMLELEMENT("mean", &&ConcFormatFunc&(&&ConcRepresentationFunc&(avg(case when '&&FinalAccuracy&' = 'T' then useconcentration
                                                                                else &&ConcRepresentationFunc&(useconcentration, &&ConcFigures&)
                                                                           end), &&ConcFigures&), &&ConcFigures&)),
        XMLELEMENT("sd", &&ConcFormatFunc&(&&ConcRepresentationFunc&(stddev(case when '&&FinalAccuracy&' = 'T' then useconcentration
                                                                                 else &&ConcRepresentationFunc&(useconcentration, &&ConcFigures&)
                                                                            end), &&ConcSDFigures&), &&ConcSDFigures&)),
        XMLELEMENT("sem", &&ConcFormatFunc&(&&ConcRepresentationFunc&(case when '&&FinalAccuracy&' = 'T' then stddev(useconcentration)/sqrt(count(useconcentration))
                                                                           else stddev(&&ConcRepresentationFunc&(useconcentration, &&ConcFigures&))/
                                                                                sqrt(count(useconcentration))
                                                                      end, &&ConcFigures&), &&ConcFigures&))
    )) ORDER BY ana_id, timeident, treatmentid, matrixkey, period, concentrationunits, genderorder, samplingtime)
    FROM
    (
    select r.ID result_id, s.id sampleid, su.id subjectid, ra.concentrationunits,
          r.CALIBRATIONRANGEFLAG, r.CALIBRATIONRANGECONV,
          case when stb$util.GetReportParameterValue('MATRIX_GROUP_BY', &&RepID&) = 'PERIOD'
               then 'X' else sgt.treatmentid end as treatmentid,
          case when stb$util.GetReportParameterValue('MATRIX_GROUP_BY', &&RepID&) = 'TREATMENT'
               then 'X' else sgt.period end as period,
          sgt.treatmentid orig_treatmentid,
          sgt.period orig_period,
          bm.matrixkey, s.designsubjecttag,
          case when &&LALPackage&.IsFetalDamStudy('&&StudyID&') = 'T' then su.SUBJECTTEXTVARIABLE1
               else null
          end SUBJECTTEXTVARIABLE1,
          /*case when STB$UTIL.GetReportParameterValue('IS_DOGCV_STUDY', &&RepID&) = 'T' then 'X_X_X_X_X_X_X_X'
               else s.samplingtime
          end samplingtime,  */
          s.samplingtime,
          case when STB$UTIL.GetReportParameterValue('IS_CV_STUDY', &&RepID&) = 'T' then 'X_X_X_X_X'
               else s.timeident
          end timeident,
          s.relativesamplingtime,
          case when s.designsubjecttag = crit.subject and s.timeident = crit.timeident then 'T' else null end excluded,
          r.ANALYTEID ana_id,
          &&GenderGrouping& AS gender,
          case when r.calibrationrangeflag in ('NM','VEC') then 0
               when r.calibrationrangeflag is null and r.concentrationstatus is not null then null
               else r.concentrationconv
          end concentration,
          case when s.designsubjecttag = crit.subject and s.timeident = crit.timeident then null
               when r.calibrationrangeflag in ('NM','VEC') then 0
               when r.calibrationrangeflag is null and r.concentrationstatus is not null then null
               when su.SUBJECTTEXTVARIABLE1 is not null and &&LALPackage&.IsFetalDamStudy('&&StudyID&') = 'T' then null
               else r.concentrationconv
          end useconcentration,
          case when lower(gender) = 'male' then 1
               when lower(gender) = 'female' then 2
               else 3
          end genderorder
    from &&TempTabPrefix&BIO$SAMPLE s,
         &&TempTabPrefix&BIO$SAMPLE$RESULTS r,
         &&TempTabPrefix&BIO$BIOMATRIX BM,
         &&TempTabPrefix&BIO$STUDYSUBJ$GRP$TREATM SGT,
         &&TempTabPrefix&BIO$STUDY$SUBJECT Su,
         &&TempTabPrefix&BIO$RUN$ANALYTES ra,
         &&TempTabPrefix&BIO$RUN run,
         (select key subject,
             NVL(trim(substr(masterkey,1,instr(masterkey,'-')-1)),masterkey) period,
             case when instr(masterkey,'-') < 1 then null else trim(substr(masterkey,instr(masterkey,'-')+1)) end timeident
           from isr$crit kwz
           where kwz.entity = 'BIO$TIME$SUBJECT'
             and repid = &&RepID&) crit
    where s.id = r.sampleID
      and s.subjectgrouptreatmentid = bm.subjectgrouptreatmentid
      and sgt.id = s.subjectgrouptreatmentid
      and su.id = s.subjectid
      and s.sampletypesplitkey = bm.sampletypesplitkey
      AND ra.analyteid = r.analyteid
      AND (
        (r.runid is not null and r.runid = run.runid)
        or run.runid in (
          select rh.runid
          from &&TempTabPrefix&BIO$REASSAY$HISTORY rh
          where rh.subjectid = su.id
          and rh.samplingtime = s.samplingtime
          and rh.reported = 'Y'
        )
      )
      AND ra.runid = run.id
      AND ra.studyID = '&&StudyID&'
      and sgt.studyid = '&&StudyID&'
      AND S.STUDYID = '&&StudyID&' AND R.STUDYID = '&&StudyID&'
      AND BM.STUDYID = '&&StudyID&' AND su.STUDYID = '&&StudyID&' &&AddCondition&
      and s.designsubjecttag = crit.subject(+) and s.timeident = crit.timeident(+) and nvl(regexp_replace(to_char(s.period,'FM0000D99'),trim(to_char(1/10,'D'))||'$'),'X') = crit.period(+)
    &&GenderGroupBy&
    )
    GROUP BY timeident, treatmentid, matrixkey, period, samplingtime, relativesamplingtime, ana_id, concentrationunits, gender, genderorder)
    ) FROM dual