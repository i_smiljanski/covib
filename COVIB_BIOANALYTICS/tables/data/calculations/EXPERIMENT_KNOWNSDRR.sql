  SELECT XMLELEMENT("experiment", XMLATTRIBUTES('&&Experiment&' AS "name" ,'&&KnownsCalcType&' AS "type"),
           Xmlagg (XMLConcat(
             Xmlagg (XMLELEMENT("calculation", xmlconcat(targ,val,res)) order by temperature, hours, longtermtime, longtermunits, concentrationunits, nominalconc &&LevelSort&, name &&DilColumnGroupBy& &&RunIDColumn&),
             XMLELEMENT("statistic", XMLATTRIBUTES(analyteid AS "analyteid", analyteorder as "analyteorder", species as "species", matrix as "matrix", concentrationunits as "unit") &&RunIDInTarget&,
               XMLELEMENT("flagpercent", min(flagpercent)),
               XMLELEMENT("min-bias", FormatRounded(min(bias), &&DecPlPercBias&)),
               XMLELEMENT("max-bias", FormatRounded(max(bias), &&DecPlPercBias&)),
               XMLELEMENT("mean-bias", FormatRounded(avg(bias), &&DecPlPercBias&)),
               XMLELEMENT("min-bias-wo-lloq", FormatRounded(min(bias_wo_lloq), &&DecPlPercBias&)),
               XMLELEMENT("max-bias-wo-lloq", FormatRounded(max(bias_wo_lloq), &&DecPlPercBias&)),
               XMLELEMENT("mean-bias-wo-lloq", FormatRounded(round(avg(bias_wo_lloq), &&DecPlPercBias&), &&DecPlPercBias&)),
               XMLELEMENT("min-bias-wo-uloq", FormatRounded(min(bias_wo_uloq), &&DecPlPercBias&)),
               XMLELEMENT("max-bias-wo-uloq", FormatRounded(max(bias_wo_uloq), &&DecPlPercBias&)),
               XMLELEMENT("mean-bias-wo-uloq", FormatRounded(round(avg(bias_wo_uloq), &&DecPlPercBias&), &&DecPlPercBias&)),
               XMLELEMENT("min-biasonmean", FormatRounded(min(biasonmean), &&DecPlPercBias&)),
               XMLELEMENT("max-biasonmean", FormatRounded(max(biasonmean), &&DecPlPercBias&)),
               XMLELEMENT("mean-biasonmean", FormatRounded(avg(biasonmean), &&DecPlPercBias&)),
               XMLELEMENT("min-biasonmean-wo-lloq", FormatRounded(min(biasonmean_wo_lloq), &&DecPlPercBias&)),
               XMLELEMENT("max-biasonmean-wo-lloq", FormatRounded(max(biasonmean_wo_lloq), &&DecPlPercBias&)),
               XMLELEMENT("min-biasonmean-w-lloq-intra", FormatRounded(min(min_biasonmean_w_lloq_intra), &&DecPlPercBias&)),
               XMLELEMENT("max-biasonmean-w-lloq-intra", FormatRounded(max(max_biasonmean_w_lloq_intra), &&DecPlPercBias&)),
               XMLELEMENT("min-biasonmean-wo-lloq-intra", FormatRounded(min(min_biasonmean_wo_lloq_intra), &&DecPlPercBias&)),
               XMLELEMENT("max-biasonmean-wo-lloq-intra", FormatRounded(max(max_biasonmean_wo_lloq_intra), &&DecPlPercBias&)),
               XMLELEMENT("mean-biasonmean-wo-lloq", FormatRounded(round(avg(biasonmean_wo_lloq), &&DecPlPercBias&), &&DecPlPercBias&)),
               XMLELEMENT("min-biasonmean-wo-uloq", FormatRounded(min(biasonmean_wo_uloq), &&DecPlPercBias&)),
               XMLELEMENT("max-biasonmean-wo-uloq", FormatRounded(max(biasonmean_wo_uloq), &&DecPlPercBias&)),
               XMLELEMENT("mean-biasonmean-wo-uloq", FormatRounded(round(avg(biasonmean_wo_uloq), &&DecPlPercBias&), &&DecPlPercBias&)),
               XMLELEMENT("min-cv", FormatRounded(min(cv), &&DecPlPercCV&)),
               XMLELEMENT("mean-cv", FormatRounded(avg(cv), &&DecPlPercCV&)),
               XMLELEMENT("max-cv", FormatRounded(max(cv), &&DecPlPercCV&)),
               XMLELEMENT("min-cv-wo-lloq", FormatRounded(round(min(cv_wo_lloq), &&DecPlPercCV&), &&DecPlPercCV&)),
               XMLELEMENT("mean-cv-wo-lloq", FormatRounded(round( case when '&&FinalAccuracy&' = 'T' then avg(cv_wo_lloq) else avg(round(cv_wo_lloq,1)) end, &&DecPlPercCV&), &&DecPlPercCV&)),
               XMLELEMENT("max-cv-wo-lloq", FormatRounded(round(max(cv_wo_lloq), &&DecPlPercCV&), &&DecPlPercCV&)),
               XMLELEMENT("min-cv-w-lloq-intra", FormatRounded(round(min(min_cv_w_lloq_intra), &&DecPlPercCV&), &&DecPlPercCV&)),
               XMLELEMENT("max-cv-w-lloq-intra", FormatRounded(round(max(max_cv_w_lloq_intra), &&DecPlPercCV&), &&DecPlPercCV&)),
               XMLELEMENT("min-cv-wo-lloq-intra", FormatRounded(round(min(min_cv_wo_lloq_intra), &&DecPlPercCV&), &&DecPlPercCV&)),
               XMLELEMENT("max-cv-wo-lloq-intra", FormatRounded(round(max(max_cv_wo_lloq_intra), &&DecPlPercCV&), &&DecPlPercCV&)),
               XMLELEMENT("numacc", sum(cntunflagged)), &&InterLotCvStatistic&
               XMLELEMENT("numtotal", sum(cnt)),
               XMLELEMENT("percacc", case when count(unflagged_bias) != 0 then FmtNum(round(sum(unflagged_bias)/count(unflagged_bias)*100,0)) else null end),
               XMLELEMENT("min-isarea-ok", &&PeakRepresentationFunc&(min(min_isarea_ok), &&PeakFigures&)),
               XMLELEMENT("max-isarea-ok", &&PeakRepresentationFunc&(max(max_isarea_ok), &&PeakFigures&)),
               XMLELEMENT("min-isarea-ok-w-lloq", &&PeakRepresentationFunc&(min(min_isarea_ok_w_lloq), &&PeakFigures&)),
               XMLELEMENT("max-isarea-ok-w-lloq", &&PeakRepresentationFunc&(max(max_isarea_ok_w_lloq), &&PeakFigures&)),
               XMLELEMENT("min-isarea-ok-wo-lloq", &&PeakRepresentationFunc&(min(min_isarea_ok_wo_lloq), &&PeakFigures&)),
               XMLELEMENT("max-isarea-ok-wo-lloq", &&PeakRepresentationFunc&(max(max_isarea_ok_wo_lloq), &&PeakFigures&)),
               XMLELEMENT("hours", hours),
               XMLELEMENT("longtermunits", longtermunits),
               XMLELEMENT("longtermtime", longtermtime),
               XMLELEMENT("temperature", temperature),
               XMLELEMENT("lloq",
                 XMLELEMENT("name", min(lloqname)),
                 XMLELEMENT("nominalconc", &&ConcFormatFunc&(&&ConcRepresentationFunc&(min(lloq_nominalconc), &&ConcFigures&), &&ConcFigures&))
                 ),
               XMLELEMENT("uloq",
                 XMLELEMENT("name", min(uloqname)),
                 XMLELEMENT("nominalconc", &&ConcFormatFunc&(&&ConcRepresentationFunc&(min(uloq_nominalconc), &&ConcFigures&), &&ConcFigures&))
                 )
             )) order by analyteorder, temperature, hours, longtermtime, longtermunits, concentrationunits))
  FROM
   (SELECT analyteorder, species, matrix,
      temperature, longtermtime, longtermunits, hours,
 nominalconc,  concentrationunits,
      Count(case when anchor = 'T' then null else useconc end) as cnt,
      count(case when anchor = 'T' then null else unflagged end) as cntunflagged,
      &&ConcRepresentationFunc&(Avg(case when anchor = 'T' then null
                                    when '&&FinalAccuracy&' = 'T' then useconc else useconc_sigrnd
                                    end), &&ConcFigures&) as mean,
      &&ConcRepresentationFunc&(Stddev(case when anchor = 'T' then null
                                       when '&&FinalAccuracy&' = 'T' then useconc else useconc_sigrnd
                                       end), &&ConcSDFigures&) as sd,
      round(case when min(anchor) = 'T' then null
                 when '&&FinalAccuracy&' = 'T' then Stddev(useconc)/Avg(useconc)*100
                 else &&ConcRepresentationFunc&(Stddev(useconc_sigrnd), &&ConcSDFigures&)/&&ConcRepresentationFunc&(Avg(useconc_sigrnd), &&ConcFigures&)*100
            end , &&DecPlPercCV&) as cv,
      case when name = min(lloqname) then null
           when min(anchor) = 'T' then null
           when '&&FinalAccuracy&' = 'T' then Stddev(useconc)/Avg(useconc)*100
           else &&ConcRepresentationFunc&(Stddev(useconc_sigrnd), &&ConcSDFigures&)/&&ConcRepresentationFunc&(Avg(useconc_sigrnd), &&ConcFigures&)*100
      end as cv_wo_lloq,
      min(case when name != min(lloqname) then null
           when min(anchor) = 'T' then null
           when '&&FinalAccuracy&' = 'T' then Stddev(useconc)/Avg(useconc)*100
           else &&ConcRepresentationFunc&(Stddev(useconc_sigrnd), &&ConcSDFigures&)/&&ConcRepresentationFunc&(Avg(useconc_sigrnd), &&ConcFigures&)*100
      end) over (partition by analyteid, analyteorder, species, matrix) as min_cv_w_lloq_intra,
      max(case when name != min(lloqname) then null
           when min(anchor) = 'T' then null
           when '&&FinalAccuracy&' = 'T' then Stddev(useconc)/Avg(useconc)*100
           else &&ConcRepresentationFunc&(Stddev(useconc_sigrnd), &&ConcSDFigures&)/&&ConcRepresentationFunc&(Avg(useconc_sigrnd), &&ConcFigures&)*100
      end) over (partition by analyteid, analyteorder, species, matrix) as max_cv_w_lloq_intra,
      min(case when name = min(lloqname) then null
           when min(anchor) = 'T' then null
           when '&&FinalAccuracy&' = 'T' then Stddev(useconc)/Avg(useconc)*100
           else &&ConcRepresentationFunc&(Stddev(useconc_sigrnd), &&ConcSDFigures&)/&&ConcRepresentationFunc&(Avg(useconc_sigrnd), &&ConcFigures&)*100
      end) over (partition by analyteid, analyteorder, species, matrix) as min_cv_wo_lloq_intra,
      max(case when name = min(lloqname) then null
           when min(anchor) = 'T' then null
           when '&&FinalAccuracy&' = 'T' then Stddev(useconc)/Avg(useconc)*100
           else &&ConcRepresentationFunc&(Stddev(useconc_sigrnd), &&ConcSDFigures&)/&&ConcRepresentationFunc&(Avg(useconc_sigrnd), &&ConcFigures&)*100
      end) over (partition by analyteid, analyteorder, species, matrix) as max_cv_wo_lloq_intra,
      round(Avg(case when anchor = 'T' then null when '&&FinalAccuracy&' = 'T' then usebias else usebias_sigrnd end), &&DecPlPercBias&) as bias,
      case when nominalconc = 0 then null
           else round(case when '&&FinalAccuracy&' = 'T' then (Avg(case when anchor = 'T' then null else useconc end)-nominalconc)/nominalconc*100
                           else (&&ConcRepresentationFunc&(Avg(case when anchor = 'T' then null else useconc_sigrnd end), &&ConcFigures&)-nominalconc_sigrnd)/nominalconc_sigrnd*100
                      end, &&DecPlPercBias&)
      end as biasonmean,
      case when min(anchor) = 'T' then null --anchor
           when abs(Avg(case when '&&FinalAccuracy&' = 'T' then usebias else usebias_sigrnd end)) > min(flagpercent) then 0
           else 1
      end as unflagged_bias,
      min(flagpercent) flagpercent,
      analyteid, name&&DilColumnGroupBy& &&RunIDColumn&,
      case when round(min(nominalconc),15) <= round(min(lloqconv),15) then null
      else case when '&&FinalAccuracy&' = 'T' then Avg(usebias)
           else round(Avg(usebias_sigrnd), &&DecPlPercBias&) end
      end as bias_wo_lloq,
      case when name = min(lloqname) then null
           when nominalconc = 0 then null
           else case when '&&FinalAccuracy&' = 'T' then (Avg(case when anchor = 'T' then null else useconc end)-nominalconc)/nominalconc*100
                     else round((&&ConcRepresentationFunc&(Avg(case when anchor = 'T' then null else useconc_sigrnd end), &&ConcFigures&)-nominalconc_sigrnd)/nominalconc_sigrnd*100, &&DecPlPercBias&)
                end
      end as biasonmean_wo_lloq,
      case when round(min(nominalconc),15) >= round(min(uloqconv),15) then null
      else case when '&&FinalAccuracy&' = 'T' then Avg(usebias)
           else round(Avg(usebias_sigrnd), &&DecPlPercBias&) end
      end as bias_wo_uloq,
      case when name = min(uloqname) then null
           when nominalconc = 0 then null
           else case when '&&FinalAccuracy&' = 'T' then (Avg(case when anchor = 'T' then null else useconc end)-nominalconc)/nominalconc*100
                     else round((&&ConcRepresentationFunc&(Avg(case when anchor = 'T' then null else useconc_sigrnd end), &&ConcFigures&)-nominalconc_sigrnd)/nominalconc_sigrnd*100, &&DecPlPercBias&)
                end
      end as biasonmean_wo_uloq,
      case when '&&FinalAccuracy&' = 'T' then min(lloq_nominalconc) else min(lloq_nominalconc_sigrnd) end lloq_nominalconc,
      case when '&&FinalAccuracy&' = 'T' then min(uloq_nominalconc) else min(uloq_nominalconc_sigrnd) end uloq_nominalconc,
      min(case when name != min(lloqname) then null
           when nominalconc = 0 then null
           else case when '&&FinalAccuracy&' = 'T' then (Avg(case when anchor = 'T' then null else useconc end)-nominalconc)/nominalconc*100
                     else round((&&ConcRepresentationFunc&(Avg(case when anchor = 'T' then null else useconc_sigrnd end), &&ConcFigures&)-nominalconc_sigrnd)/nominalconc_sigrnd*100, &&DecPlPercBias&)
                end
      end) over (partition by analyteid, analyteorder, species, matrix, concentrationunits) as min_biasonmean_w_lloq_intra,
      max(case when name != min(lloqname) then null
           when nominalconc = 0 then null
           else case when '&&FinalAccuracy&' = 'T' then (Avg(case when anchor = 'T' then null else useconc end)-nominalconc)/nominalconc*100
                     else round((&&ConcRepresentationFunc&(Avg(case when anchor = 'T' then null else useconc_sigrnd end), &&ConcFigures&)-nominalconc_sigrnd)/nominalconc_sigrnd*100, &&DecPlPercBias&)
                end
      end) over (partition by analyteid, analyteorder, species, matrix, concentrationunits) as max_biasonmean_w_lloq_intra,
      min(case when name = min(lloqname) then null
           when nominalconc = 0 then null
           else case when '&&FinalAccuracy&' = 'T' then (Avg(case when anchor = 'T' then null else useconc end)-nominalconc)/nominalconc*100
                     else round((&&ConcRepresentationFunc&(Avg(case when anchor = 'T' then null else useconc_sigrnd end), &&ConcFigures&)-nominalconc_sigrnd)/nominalconc_sigrnd*100, &&DecPlPercBias&)
                end
      end) over (partition by analyteid, analyteorder, species, matrix, concentrationunits) as min_biasonmean_wo_lloq_intra,
      max(case when name = min(lloqname) then null
           when nominalconc = 0 then null
           else case when '&&FinalAccuracy&' = 'T' then (Avg(case when anchor = 'T' then null else useconc end)-nominalconc)/nominalconc*100
                     else round((&&ConcRepresentationFunc&(Avg(case when anchor = 'T' then null else useconc_sigrnd end), &&ConcFigures&)-nominalconc_sigrnd)/nominalconc_sigrnd*100, &&DecPlPercBias&)
                end
      end) over (partition by analyteid, analyteorder, species, matrix, concentrationunits) as max_biasonmean_wo_lloq_intra,
      min( case when nominalconc = 0 then null
           else
             case when round(case when '&&FinalAccuracy&' = 'T' then (Avg(case when anchor = 'T' then null else useconc end)-nominalconc)/nominalconc*100
                           else (&&ConcRepresentationFunc&(Avg(case when anchor = 'T' then null else useconc_sigrnd end), &&ConcFigures&)-nominalconc_sigrnd)/nominalconc_sigrnd*100
                      end, &&DecPlPercBias&) > 15 then null
                  else &&PeakRepresentationFunc&(min(INTERNALSTANDARDAREA), &&PeakFigures&)
      end end ) over (partition by analyteid, analyteorder, species, matrix, concentrationunits) as min_isarea_ok,
      max( case when nominalconc = 0 then null
           else
             case when round(case when '&&FinalAccuracy&' = 'T' then (Avg(case when anchor = 'T' then null else useconc end)-nominalconc)/nominalconc*100
                           else (&&ConcRepresentationFunc&(Avg(case when anchor = 'T' then null else useconc_sigrnd end), &&ConcFigures&)-nominalconc_sigrnd)/nominalconc_sigrnd*100
                      end, &&DecPlPercBias&) > 15 then null
                  else &&PeakRepresentationFunc&(max(INTERNALSTANDARDAREA), &&PeakFigures&)
      end end ) over (partition by analyteid, analyteorder, species, matrix, concentrationunits) as max_isarea_ok,
      min( case when nominalconc = 0 or name != min(lloqname) then null
           else
             case when round(case when '&&FinalAccuracy&' = 'T' then (Avg(case when anchor = 'T' then null else useconc end)-nominalconc)/nominalconc*100
                           else (&&ConcRepresentationFunc&(Avg(case when anchor = 'T' then null else useconc_sigrnd end), &&ConcFigures&)-nominalconc_sigrnd)/nominalconc_sigrnd*100
                      end, &&DecPlPercBias&) > 20 then null
                  else &&PeakRepresentationFunc&(min(INTERNALSTANDARDAREA), &&PeakFigures&)
      end end ) over (partition by analyteid, analyteorder, species, matrix, concentrationunits) as min_isarea_ok_w_lloq,
      max( case when nominalconc = 0 or name != min(lloqname) then null
           else
             case when round(case when '&&FinalAccuracy&' = 'T' then (Avg(case when anchor = 'T' then null else useconc end)-nominalconc)/nominalconc*100
                           else (&&ConcRepresentationFunc&(Avg(case when anchor = 'T' then null else useconc_sigrnd end), &&ConcFigures&)-nominalconc_sigrnd)/nominalconc_sigrnd*100
                      end, &&DecPlPercBias&) > 20 then null
                  else &&PeakRepresentationFunc&(max(INTERNALSTANDARDAREA), &&PeakFigures&)
      end end ) over (partition by analyteid, analyteorder, species, matrix, concentrationunits) as max_isarea_ok_w_lloq,
      min( case when nominalconc = 0 or name = min(lloqname) then null
           else
             case when round(case when '&&FinalAccuracy&' = 'T' then (Avg(case when anchor = 'T' then null else useconc end)-nominalconc)/nominalconc*100
                           else (&&ConcRepresentationFunc&(Avg(case when anchor = 'T' then null else useconc_sigrnd end), &&ConcFigures&)-nominalconc_sigrnd)/nominalconc_sigrnd*100
                      end, &&DecPlPercBias&) > 15 then null
                  else &&PeakRepresentationFunc&(min(INTERNALSTANDARDAREA), &&PeakFigures&)
      end end ) over (partition by analyteid, analyteorder, species, matrix, concentrationunits) as min_isarea_ok_wo_lloq,
      max( case when nominalconc = 0 or name = min(lloqname) then null
           else
             case when round(case when '&&FinalAccuracy&' = 'T' then (Avg(case when anchor = 'T' then null else useconc end)-nominalconc)/nominalconc*100
                           else (&&ConcRepresentationFunc&(Avg(case when anchor = 'T' then null else useconc_sigrnd end), &&ConcFigures&)-nominalconc_sigrnd)/nominalconc_sigrnd*100
                      end, &&DecPlPercBias&) > 15 then null
                  else &&PeakRepresentationFunc&(max(INTERNALSTANDARDAREA), &&PeakFigures&)
      end end ) over (partition by analyteid, analyteorder, species, matrix, concentrationunits) as max_isarea_ok_wo_lloq,
      min(lloqconv) lloqconv, min(uloqconv) uloqconv,
      min(lloqname) lloqname, min(uloqname) uloqname, min(anchor) anchor,
      XMLELEMENT("target",
        XMLELEMENT("analyteid", analyteid),
        XMLELEMENT("species", species),
        XMLELEMENT("matrix", matrix),
        XMLELEMENT("name", NAME),
        XMLELEMENT("nominalconc", &&ConcFormatFunc&(nominalconc_sigrnd, &&ConcFigures&)),
        XMLELEMENT("hours", hours),
        XMLELEMENT("longtermunits", longtermunits),
        XMLELEMENT("longtermtime", longtermtime),
        XMLELEMENT("temperature", temperature), 
        XMLELEMENT("unit", concentrationunits) &&DilTraget& &&RunIDInTarget& &&LevelTarget&) as targ,
      XMLELEMENT("values", Xmlagg(
        XMLELEMENT("sample", XMLATTRIBUTES(sampleid AS "sampleid", sampleresultid AS "sampleresultid" ), &&RunIDInValues&
          XMLELEMENT("runsamplesequencenumber", runsamplesequencenumber),
          XMLELEMENT("concentration", &&ConcFormatFunc&(conc_sigrnd, &&ConcFigures&)),
          XMLELEMENT("nominalconc", &&ConcFormatFunc&(nominalconc_sigrnd, &&ConcFigures&)),
          XMLELEMENT("subset", subset),
          XMLELEMENT("bias", FormatRounded(bias_sigrnd, &&DecPlPercBias&)),
          XMLELEMENT("resultcommenttext", resultcommenttext),
          XMLELEMENT("flag", flagged),
          XMLELEMENT("internalstandardarea", &&PeakRepresentationFunc&(INTERNALSTANDARDAREA, &&PeakFigures&)),
          XMLELEMENT("deactivated", isdeactivated)&&DilValue&)
          order by runnr, runsamplesequencenumber
          )) as val,
      XMLELEMENT("result",
        XMLELEMENT("n", Count(useconc)),
        XMLELEMENT("mean", &&ConcFormatFunc&(&&ConcRepresentationFunc&(Avg(
                           case when '&&FinalAccuracy&' = 'T' then useconc else useconc_sigrnd
                           end), &&ConcFigures&), &&ConcFigures&)),
        XMLELEMENT("sd", &&ConcFormatFunc&(&&ConcRepresentationFunc&(Stddev(
                        case when '&&FinalAccuracy&' = 'T' then useconc
                        else useconc_sigrnd
                        end), &&ConcSDFigures&), &&ConcSDFigures&)),
        XMLELEMENT("cv", case when '&&FinalAccuracy&' = 'T' then FormatRounded(round(Stddev(useconc)/Avg(useconc)*100, &&DecPlPercCV&), &&DecPlPercCV&)
                         else FormatRounded(round(&&ConcRepresentationFunc&(Stddev(useconc_sigrnd), &&ConcFigures&)/&&ConcRepresentationFunc&(Avg(useconc_sigrnd), &&ConcFigures&)*100, &&DecPlPercCV&), &&DecPlPercCV&)
                         end), &&InterLotCvResult&
        XMLELEMENT("bias", FormatRounded(round(Avg(case when '&&FinalAccuracy&' = 'T' then usebias else usebias_sigrnd end), &&DecPlPercBias&), &&DecPlPercBias&)),
        XMLELEMENT("biasonmean", FormatRounded(round(case when nominalconc = 0 then null
                                                    when '&&FinalAccuracy&' = 'T' then (Avg(useconc)-nominalconc)/nominalconc*100
                                                    else (&&ConcRepresentationFunc&(Avg(useconc_sigrnd), &&ConcFigures&)-nominalconc_sigrnd)/nominalconc_sigrnd*100
                                               end, &&DecPlPercBias&), &&DecPlPercBias&)),
        XMLELEMENT("flagpercent", min(flagpercent)),
        --XMLELEMENT("days", min(days)),
        case when min(knowntype) = 'STANDARD' then XMLELEMENT("anchor",min(anchor)) else null end
        ) as res
    FROM
    (SELECT --knownID,
     sampleID, sampleresultID, name, subset &&LevelCol&, &&InterLotCvCalc&
     nominalconc, nominalconc_sigrnd, analyteid &&DilColumn&,
     lloqconv, uloqconv, knowntype, resultcommenttext,
     min(case when round(nominalconc,15) = round(lloqconv,15) then name else null end) over (partition by analyteid, analyteorder, species, matrix) as lloqname,
     min(case when round(nominalconc,15) = round(uloqconv,15) then name else null end) over (partition by analyteid, analyteorder, species, matrix) as uloqname,
     hours, longtermtime, longtermunits, temperature, concentrationunits,
     case when knowntype = 'STANDARD' and (nominalconc_sigrnd<&&ConcRepresentationFunc&(lloqconv, &&ConcFigures&) or nominalconc_sigrnd>&&ConcRepresentationFunc&(uloqconv, &&ConcFigures&)) then 'T' else 'F' end anchor,
     MIN(nominalconc_sigrnd) over (partition by analyteid, analyteorder, species, matrix) lloq_nominalconc_sigrnd,
     MAX(nominalconc_sigrnd) over (partition by analyteid, analyteorder, species, matrix) uloq_nominalconc_sigrnd,
     MIN(nominalconc) over (partition by analyteid, analyteorder, species, matrix) lloq_nominalconc,
     MAX(nominalconc) over (partition by analyteid, analyteorder, species, matrix) uloq_nominalconc,
     analyteorder, species, matrix,
     conc, conc_sigrnd, bias, bias_sigrnd,
     flagpercent,
     ISDEACTIVATED, runid, runnr, runsamplesequencenumber,
     case when ISDEACTIVATED = 'T' then null else conc_sigrnd end useconc_sigrnd,
     case when ISDEACTIVATED = 'T' then null else bias_sigrnd end usebias_sigrnd,
     case when ISDEACTIVATED = 'T' then null else conc end useconc,
     case when ISDEACTIVATED = 'T' then null else bias end usebias,
     case when abs(case when '&&FinalAccuracy&' = 'T' then bias else bias_sigrnd end) > flagpercent then '&&FlaggedSymbol&' else null end flagged,
     case when ISDEACTIVATED <> 'T' and abs(case when '&&FinalAccuracy&' = 'T' then bias else bias_sigrnd end) <= flagpercent then 1 else null end unflagged,
     INTERNALSTANDARDAREA
     FROM
     (SELECT --K.ID knownID,
        s.ID sampleID, sr.ID sampleresultID,
        S.KNOWNNAME name,
        '' subset, s.ISDEACTIVATED, &&DilColumnQualif&
        s.runsamplesequencenumber,
        ra.analyteid, sa.analyteorder, a.species, a.sampletypeid matrix,
        &&ConcRepresentationFunc&(sr.nominalconcentrationconv, &&ConcFigures&) nominalconc_sigrnd,
        sr.nominalconcentrationconv nominalconc, sr.resultcommenttext,
        ra.lloqconv, ra.uloqconv, s.runsamplekind knowntype,
        case when s.runsamplekind = 'STANDARD' then
          case when round(ra.lloq,10) = round(sr.nominalconcentration,10) then 20
          else 15 end
        else 15 end flagpercent,
        s.runid, r.runid runnr,
        &&ConcRepresentationFunc&(sr.concentrationconv, &&ConcFigures&) conc_sigrnd,
        sr.concentrationconv conc, &&StabilityInfo&
        ra.concentrationunits,
        case when sr.nominalconcentrationconv = 0 then null
             else round((&&ConcRepresentationFunc&(sr.concentrationconv, &&ConcFigures&)-&&ConcRepresentationFunc&(sr.nominalconcentrationconv, &&ConcFigures&))*100/
                         &&ConcRepresentationFunc&(sr.nominalconcentrationconv, &&ConcFigures&), &&DecPlPercBias&)
        end bias_sigrnd,
        case when sr.nominalconcentrationconv = 0 then null
             else (sr.concentrationconv-sr.nominalconcentrationconv)*100/
                   sr.nominalconcentrationconv
        end bias,
        srw.INTERNALSTANDARDAREA
      FROM --&&TempTabPrefix&bio$run$ana$known K,
           &&TempTabPrefix&bio$run r,
           &&TempTabPrefix&bio$run$analytes ra,
           &&TempTabPrefix&bio$study$analytes sa,
           &&TempTabPrefix&bio$assay a,
           &&TempTabPrefix&bio$run$samples s,
           &&TempTabPrefix&bio$run$sample$results sr,
           &&TempTabPrefix&bio$RUN$SAMPLE$RESULT$RAW srw
           --isr$crit kwz,
           --table(&&LALPackage&.GetRunStates) rs
      WHERE -- K.ID = s.runknownID AND
        R.ID = s.runid AND R.ID = sr.runid
        AND r.RuntypeDescription in (&&RunTypes&)
        AND s.ID = sr.runsampleID
        --AND K.knowntype = '&&KnownType&'
        AND s.sampletype = 'known'
        AND s.runsamplekind = '&&KnownType&'
        --AND K.knowntype = s.runsamplekind
        AND ra.id = sr.runanalyteid
        AND a.id = r.assayid
        AND sa.id = ra.analyteid
        AND ra.runid = s.runid
  AND s.studyid = '&&StudyID&'
        AND sr.studyID = '&&StudyID&' AND r.studyid = '&&StudyID&'
        AND ra.studyID = '&&StudyID&'
        --AND rs.column_value = r.runstatusnum
        and srw.RUNSAMPLECODE = s.code
        AND r.ID = srw.runid
        --AND s.studyassayid = srw.studyassayid
        AND ra.id = srw.runanalyteid
        AND srw.studyID = '&&StudyID&'
        AND regexp_like(S.KNOWNNAME,'&&IncludedKnowns&')
        --and kwz.key = &&KnownNameJoinField&
        --and kwz.entity = '&&KnownEntity&'
        --and kwz.masterkey = '&&ExpMasterkey&'
        --and repid = &&RepID&
        &&DilCond& &&AddCondition&
     ))
    GROUP BY analyteid, analyteorder, species, matrix, name, nominalconc, nominalconc_sigrnd, temperature, hours, longtermtime, longtermunits, concentrationunits &&DilColumnGroupBy& &&RunIDColumn&&&LevelGroupBy&)
    GROUP BY analyteid, analyteorder, species, matrix, temperature, hours, longtermtime, longtermunits, concentrationunits &&RunIDColumn&
