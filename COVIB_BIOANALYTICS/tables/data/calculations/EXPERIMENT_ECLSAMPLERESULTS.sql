  SELECT XMLELEMENT("experiment", XMLATTRIBUTES('&&Experiment&' AS "name" ,'eclsampleresults' AS "type"),
      Xmlagg ( 
        Xmlagg (XMLELEMENT("calculation", xmlconcat(targ,val,res)) order by studyid, analytename, diseaseorder, species, matrix, dosegroupno, dosegroup, sequence, period, designsubjecttag, studyday, samplingtime, period, doseamount, doseunit/*, runid, rid*/) order by studyid
      ))
  FROM (
  SELECT studyid, analytename, NVL(diseasestate, ' ') diseaseorder, species, matrix, dosegroupno, dosegroup, sequence, subjectid, designsubjecttag, /*designSampleID,*/ samplingtime, time, studyday, /*rid, runid,*/ period, doseamount, doseunit,
    XMLELEMENT("target",
      XMLELEMENT("diseasestate", diseasestate),
      XMLELEMENT("dosegroup", dosegroup),
      XMLELEMENT("dosegroupno", dosegroupno),
      XMLELEMENT("sequence", sequence),
      XMLELEMENT("subjectid", subjectid),
      --XMLELEMENT("designsampleid", designSampleID),
      XMLELEMENT("designsubjecttag", designsubjecttag),
      --XMLELEMENT("runid", rid),
      --XMLELEMENT("runno", runid),
      XMLELEMENT("analytename", analytename),
      XMLELEMENT("species", species),
      XMLELEMENT("matrix", matrix),
      XMLELEMENT("samplingtime", samplingtime),
      XMLELEMENT("timetext", timetext),
      XMLELEMENT("time", time),
      XMLELEMENT("period", period),
      XMLELEMENT("doseamount", FmtNum(doseamount)),
      XMLELEMENT("doseunit", doseunit)
    ) as targ,
    XMLELEMENT("values", 
      Xmlagg(
        case when scr_rid is not null then
        XMLELEMENT("screen",
          --XMLATTRIBUTES(sampleid AS "sampleid", sampleresultid AS "sampleresultid" ),
          XMLELEMENT("designsampleid", designSampleID),
          XMLELEMENT("runid", scr_rid),
          XMLELEMENT("status", scr_result)
        ) 
        else null
        end order by scr_result
      ), Xmlagg(
        case when co_rid is not null then
        XMLELEMENT("confirm",
          --XMLATTRIBUTES(sampleid AS "sampleid", sampleresultid AS "sampleresultid" ),
          XMLELEMENT("designsampleid", designSampleID),
          XMLELEMENT("runid", co_rid),
          XMLELEMENT("status", co_result)
        ) 
        else null
        end order by co_result
      ), Xmlagg(
        case when ti_rid is not null then
        XMLELEMENT("titer",
          --XMLATTRIBUTES(sampleid AS "sampleid", sampleresultid AS "sampleresultid" ),
          XMLELEMENT("designsampleid", designSampleID),
          XMLELEMENT("runid", ti_rid),
          XMLELEMENT("status", ti_result)
        ) 
        else null
        end order by ti_result
      )
    ) as val,
    XMLELEMENT("result",
      XMLELEMENT("screen", max(scr_result) KEEP (DENSE_RANK FIRST ORDER BY scr_result)),
      XMLELEMENT("confirm", max(co_result) KEEP (DENSE_RANK FIRST ORDER BY co_result)),
      XMLELEMENT("titer", max(ti_result) KEEP (DENSE_RANK FIRST ORDER BY ti_result))
    ) as res
  FROM (
    SELECT
      sample.studyid, analytename, sample.id designsampleid, sample.subjectid, sample.designsubjecttag, sample.samplingtime, sample.time, sample.timetext, sample.studyday,
      sample.period, sample.doseGroup, sample.sequence, sample.doseamount, sample.doseunit, --sample.rid, sample.runid,
      --screen.result scr_result, confirm.result co_result,-- titer.result ti_result,
      REGEXP_REPLACE(screen.result, '^(positive|negative)\ ([A-Za-z]*).*$', '\1 \2', 1, 0, 'i') scr_result,
      REGEXP_REPLACE(confirm.result, '^(positive|negative)\ ([A-Za-z]*).*$', '\1 \2', 1, 0, 'i') co_result,
      REGEXP_REPLACE(titer.result, '^(positive|negative)\ ([A-Za-z]*).*$', '\1 \2', 1, 0, 'i') ti_result,
      screen.rid scr_rid, confirm.rid co_rid, titer.rid ti_rid,
      sample.dosegroupno, sample.diseasestate, sample.species, sample.matrix
    FROM (
      SELECT
        sam.studyid,
        sa.analyteorder,
        sa.id analyteid,
        &&AnalyteName& analytename,
        t.name dosegroup,
        case
            when REGEXP_LIKE(sg.name, '^[+-]?\d?\.?\d+$','i') then
                to_number(sg.name)
            when REGEXP_LIKE(t.name, '^[+-]?\d?\.?\d+$','i') then
                to_number(t.name)
            when REGEXP_LIKE(t.description, '^[+-]?\d?\.?\d+$','i') then
                to_number(t.description)
            else
              null
        end dosegroupno,
        sg.sequence sequence,
        t.doseamount,
        t.doseunitsdescription doseunit,
        r.id rid,
        r.runid,
        sam.id,
        sam.subjectid,
        sam.designsubjecttag,
        sam.samplingtime,
        s.time,
        s.timetext,
        s.studyday,
        sam.period,
        REGEXP_REPLACE(a.name,'.*[\ \_]?(screening|screen|scrn|confirmatory|conf|titer|titr)[\ \_]?(.*)$','\2',1,0,'i') diseasestate,
        a.species,
        a.sampletypeid matrix,
        max(rh.reported) reported-- over (partition by sam.id, s.runID) reported
      FROM 
        &&TempTabPrefix&bio$run r,
        &&TempTabPrefix&bio$run$analytes ra,
        &&TempTabPrefix&bio$study$analytes sa,
        &&TempTabPrefix&bio$assay a,
        &&TempTabPrefix&bio$sample sam,
        &&TempTabPrefix&bio$run$worklist s,
        &&TempTabPrefix&bio$run$worklist$result$rw sr,
        &&TempTabPrefix&bio$study$treatment t,
        &&TempTabPrefix&bio$study$subjectgroup sg,
        &&TempTabPrefix&bio$reassay$history rh,
        table(&&LALPackage&.GetRunStates) rs
      WHERE sam.studyid = '&&StudyID&'
      AND r.studyid = '&&StudyID&'
      AND a.studyID = '&&StudyID&'
      AND s.designSampleID = sam.ID
      AND r.ID = s.runID
      AND s.ID = sr.worklistID
      AND a.ID = r.assayID
      AND rs.column_value = r.runstatusnum
      AND sr.runAnalyteCode = ra.code
      AND ra.analyteID = sa.ID
      AND sam.treatmentcode = t.code
      AND sam.subjectgroupcode = sg.code
      AND s.runcode || '-' || s.runsamplesequencenumber || '-' || sa.analyteorder not in (select code from &&TempTabPrefix&bio$deactivated$samples)
      AND r.code || '-' || s.runsamplesequencenumber || '-' || sa.analyteorder = rh.runsamplecode (+)
      AND (s.samplesubtype in ('Screen', 'Titer') or s.samplesubtype like 'ImmunoDepletion%')
      &&AddConditionSample&
    GROUP BY sam.studyid, sa.analyteorder, a.name, a.species, a.sampletypeid, t.name, sg.name, t.description, sg.sequence, sa.id, sa.name, sam.id, sam.subjectid, r.runid, r.id, sam.designsubjecttag, sam.samplingtime, s.time, s.timetext, s.studyday, sam.period, t.doseamount, t.doseunitsdescription
    ) sample , (
      SELECT
        r.studyid,
        sam.subjectid,
        s.designsampleid,
        sa.id analyteid,
        r.id rid,
        s.samplesubtype,
        sr.commenttext,
        sr.concentrationstatus result,
        s.time,
        sam.samplingtime
      FROM 
        &&TempTabPrefix&bio$run r,
        &&TempTabPrefix&bio$run$analytes ra,
        &&TempTabPrefix&bio$study$analytes sa,
        &&TempTabPrefix&bio$assay a,
        &&TempTabPrefix&bio$run$worklist s,
        &&TempTabPrefix&bio$run$worklist$result$rw sr,
        &&TempTabPrefix&bio$sample sam,
        table(&&LALPackage&.GetRunStates) rs
      WHERE r.ID = s.runID
      AND s.ID = sr.worklistID
      AND a.ID = r.assayID
      AND s.designSampleID = sam.ID
      AND r.studyid = '&&StudyID&'
      AND a.studyID = '&&StudyID&'
      AND rs.column_value = r.runstatusnum
      AND sr.runAnalyteCode = ra.code
      AND r.ID = ra.runID
      AND ra.analyteID = sa.ID
      AND sr.concentrationstatus is not null
      AND s.samplesubtype like 'Screen%'
      &&AddConditionScreen&
    ) screen , (
      SELECT
        r.studyid,
        sam.subjectid,
        s.designsampleid,
        sa.id analyteid,
        r.id rid,
        s.samplesubtype,
        sr.commenttext,
        sr.concentrationstatus result,
        s.time,
        sam.samplingtime
      FROM 
        &&TempTabPrefix&bio$run r,
        &&TempTabPrefix&bio$run$analytes ra,
        &&TempTabPrefix&bio$study$analytes sa,
        &&TempTabPrefix&bio$assay a,
        &&TempTabPrefix&bio$run$worklist s,
        &&TempTabPrefix&bio$run$worklist$result$rw sr,
        &&TempTabPrefix&bio$sample sam,
        table(&&LALPackage&.GetRunStates) rs
      WHERE r.ID = s.runID
      AND s.ID = sr.worklistID
      AND a.ID = r.assayID
      AND s.designSampleID = sam.ID
      AND r.studyid = '&&StudyID&'
      AND a.studyID = '&&StudyID&'
      AND rs.column_value = r.runstatusnum
      AND sr.runAnalyteCode = ra.code
      AND r.ID = ra.runID
      AND ra.analyteID = sa.ID
      AND sr.concentrationstatus is not null
      AND s.samplesubtype like 'ImmunoDepletion%'
      &&AddConditionConfirm&
    ) confirm, (
      SELECT
        r.studyid,
        sam.subjectid,
        s.designsampleid,
        sa.id analyteid,
        s.samplesubtype,
        r.id rid,
        sr.commenttext,
        sr.concentrationstatus result,
        s.time,
        sam.samplingtime
      FROM 
        &&TempTabPrefix&bio$run r,
        &&TempTabPrefix&bio$run$analytes ra,
        &&TempTabPrefix&bio$study$analytes sa,
        &&TempTabPrefix&bio$assay a,
        &&TempTabPrefix&bio$run$worklist s,
        &&TempTabPrefix&bio$run$worklist$result$rw sr,
        &&TempTabPrefix&bio$sample sam,
        table(&&LALPackage&.GetRunStates) rs
      WHERE r.ID = s.runID
      AND s.ID = sr.worklistID
      AND a.ID = r.assayID
      AND s.designSampleID = sam.ID
      AND r.studyid = '&&StudyID&'
      AND a.studyID = '&&StudyID&'
      AND rs.column_value = r.runstatusnum
      AND sr.runAnalyteCode = ra.code
      AND r.ID = ra.runID
      AND ra.analyteID = sa.ID
      AND sr.concentrationstatus is not null
      AND s.samplesubtype like 'Titer%'
      &&AddConditionConfirm&
    ) Titer
    WHERE sample.studyID = screen.studyID (+)
    AND sample.analyteID = screen.analyteID (+)
    AND sample.id = screen.designSampleID (+)
    AND sample.rid = screen.rid (+)
    AND sample.studyID = confirm.studyID (+)
    AND sample.analyteID = confirm.analyteID (+)
    AND sample.id = confirm.designSampleID (+)
    AND sample.rid = confirm.rid (+)
    AND sample.studyID = titer.studyID (+)
    AND sample.analyteID = titer.analyteID (+)
    AND sample.id = titer.designSampleID (+)
    AND sample.rid = titer.rid (+)
    AND nvl(sample.reported,'Y') = 'Y'
    &&AddCondition&
  )
    GROUP BY studyid, analytename, diseasestate, species, matrix, dosegroupno, dosegroup, sequence, subjectid, designsubjecttag, /*designSampleID,*/ samplingtime, time, timetext, studyday, period, /*runid, rid,*/ doseamount, doseunit)
GROUP BY studyid

