  SELECT XMLELEMENT("experiment", XMLATTRIBUTES('&&Experiment&' AS "name" ,'isrvalues' AS "type"),
    (SELECT xmlagg (XMLELEMENT("calculation",
      XMLELEMENT("target",
        --/*XMLELEMENT("studyassayid", STUDYASSAYID), */XMLELEMENT("analyteid", ASSAYANALYTEID), XMLELEMENT("matrix", matrix)),
        XMLELEMENT("analyteid", analyteid),
        XMLELEMENT("species", species),
        XMLELEMENT("matrix", matrix),
        XMLELEMENT("unit", concentrationunits)),
 -- Matrix is selected for ISR specifically in Watson
      XMLELEMENT("values", xmlagg(
        XMLELEMENT("sample", XMLATTRIBUTES(samplecode AS "samplecode", sampleid AS "sampleid" ),
          XMLELEMENT("runsampleid", runsampleid),
          XMLELEMENT("designsampleid", designSampleID),
          XMLELEMENT("name", NAME),
          XMLELEMENT("runid", runid),
          XMLELEMENT("concentration", conc),      -- is formatted in select below
          XMLELEMENT("nm", nm),
          XMLELEMENT("vec", vec),
          XMLELEMENT("runidm", mrunid),
          XMLELEMENT("concentrationm", concm),    -- is formatted in select below
          XMLELEMENT("nmm", nmm),
          XMLELEMENT("vecm", vecm),
          XMLELEMENT("bias", bias),               -- is formatted in select below
          XMLELEMENT("designsubjecttag", designsubjecttag),
          XMLELEMENT("treatment", treatment),
          XMLELEMENT("timeident", timeident),
          XMLELEMENT("samplingtime", samplingtime),
          XMLELEMENT("dilutionfactor", dilutionfactor),
          XMLELEMENT("dilutionfactorm", dilutionfactorm),
          XMLELEMENT("runsamplesequencenumber", runsamplesequencenumber),
          XMLELEMENT("validbias", validbias),
          XMLELEMENT("biasflag", flagged)
        )
      ORDER BY DesignSubjectTag, treatment, samplingtime, timeident, mrunid, runsamplesequencenumber)),
      XMLELEMENT("result",
        XMLELEMENT("n", Sum(unflaggedvalid)),
        XMLELEMENT("m", Sum(validbias)),
        XMLELEMENT("pass", Sum(unflaggedvalid)),
        XMLELEMENT("fail", Count(flagged)),
        XMLELEMENT("npercm", FormatRounded(round(Sum(unflaggedvalid)/Sum(validbias)*100, &&DecPlPercDiff&), &&DecPlPercDiff&)),
        XMLELEMENT("bias-pass", case when round(Sum(unflaggedvalid)/Sum(validbias)*100, &&DecPlPercDiff&) < 67 then 'F' else 'T' end)
        )
    ))
    FROM
    (
    select /*STUDYASSAYID, ASSAYANALYTEID, */nm, vec, nmm, vecm,
      midrun, mrunid, runid, samplingtime,
      DesignSubjectTag, timeident, runsamplesequencenumber,
      designsampleid, matrix, treatment,
      analyteid, species, analyteorder, concentrationunits,
      NAME, runsampleid, samplecode, sampleid, flagpercent,
      conc, concentrationstatus, concm, concentrationstatusm,
      bias, validbias, --abs(bias),
      dilutionfactor, dilutionfactorm,
      case when validbias = 0 then null when abs(bias) > flagpercent then '&&FlaggedSymbol&' else null end flagged,
      case when validbias = 0 then null when abs(bias) <= flagpercent then 1 else null end unflaggedvalid,
      count(1) over (partition by mresultid) cnt, ischoice
    from
    (SELECT distinct /*RM.STUDYASSAYID , SRM.ASSAYANALYTEID, */
        &&ConcFormatFunc&(&&ConcRepresentationFunc&(ra.nm*s.dilutionfactor*conversionfactor, &&ConcFigures&), &&ConcFigures&) nm,
        &&ConcFormatFunc&(&&ConcRepresentationFunc&(ra.vec*s.dilutionfactor*conversionfactor, &&ConcFigures&), &&ConcFigures&) vec,
        &&ConcFormatFunc&(&&ConcRepresentationFunc&(ram.nm*sm.dilutionfactor*conversionfactor, &&ConcFigures&), &&ConcFigures&) nmm,
        &&ConcFormatFunc&(&&ConcRepresentationFunc&(ram.vec*sm.dilutionfactor*conversionfactor, &&ConcFigures&), &&ConcFigures&) vecm,
        rm.ID midrun, rm.runid mrunid, r.runid, sam.samplingtime,
        ra.analyteid, sa.analyteorder, a.species, ra.concentrationunits origconcunits, cu.concentrationunits,
        sam.DesignSubjectTag, sam.timeident,sm.runsamplesequencenumber,
        s.dilutionfactor, sm.dilutionfactor dilutionfactorm,
        --RDS.FROMSAMPLEID designSampleID,
        sam.designsampleid, sam.matrix, tr.title treatment, srm.id mresultid, sr.ischoice,
        sm.NAME, srm.runsampleid, s.samplecode, s.sampleid, &&FlagPercentISR& flagpercent,
        CASE WHEN sr.eliminatedflag = 'X' THEN sr.concentrationstatus
             WHEN sr.concentrationstatus= 'NM' THEN '&&LabelBelowLLOQ&&&ConcLimitParenthBefore&'||case when '&&ShowLimitValue&' = 'T' then FormatSig(sigFigure(ra.nm*s.dilutionfactor*conversionfactor, &&ConcFigures&), &&ConcFigures&) else null end||'&&ConcLimitParenthAfter&'--'BQL'
             WHEN sr.concentrationstatus= 'VEC' THEN '&&LabelAboveULOQ&&&ConcLimitParenthBefore&'||case when '&&ShowLimitValue&' = 'T' then FormatSig(sigFigure(ra.vec*s.dilutionfactor*conversionfactor, &&ConcFigures&), &&ConcFigures&) else null end||'&&ConcLimitParenthAfter&'--'AQL'
             WHEN sr.concentrationstatus not in('VEC','NM') and trim(sr.concentrationstatus) is not null THEN null
             ELSE &&ConcFormatFunc&(&&ConcRepresentationFunc&(sr.concentrationconv*conversionfactor, &&ConcFigures&), &&ConcFigures&)
        END conc,
        sr.concentrationstatus concentrationstatus,
        CASE WHEN srm.eliminatedflag = 'X' THEN srm.concentrationstatus
             WHEN srm.concentrationstatus in('NM','BQL') THEN '&&LabelBelowLLOQ&&&ConcLimitParenthBefore&'||case when '&&ShowLimitValue&' = 'T' then FormatSig(sigFigure(ram.nm*sm.dilutionfactor*conversionfactor, &&ConcFigures&), &&ConcFigures&) else null end||'&&ConcLimitParenthAfter&'--'BQL'
             WHEN srm.concentrationstatus in('VEC','AQL') THEN '&&LabelAboveULOQ&&&ConcLimitParenthBefore&'||case when '&&ShowLimitValue&' = 'T' then FormatSig(sigFigure(ram.vec*sm.dilutionfactor*conversionfactor, &&ConcFigures&), &&ConcFigures&) else null end||'&&ConcLimitParenthAfter&'--'AQL'
             WHEN srm.concentrationstatus not in('VEC','AQL','NM','BQL') and trim(srm.concentrationstatus) is not null THEN null
             ELSE &&ConcFormatFunc&(&&ConcRepresentationFunc&(srm.concentrationconv*conversionfactor, &&ConcFigures&), &&ConcFigures&)
        END concm,
        srm.concentrationstatus concentrationstatusm,
        CASE WHEN trim(sr.concentrationstatus) is not null OR trim(srm.concentrationstatus) is not null OR sr.concentrationconv is null THEN '*'
             ELSE FormatRounded(round(case when '&&FinalAccuracy&' = 'T' then (srm.concentrationconv*conversionfactor-sr.concentrationconv*conversionfactor)*100
                                                                          /((sr.concentrationconv*conversionfactor+srm.concentrationconv*conversionfactor)/2)
                                           else (&&ConcRepresentationFunc&(srm.concentrationconv*conversionfactor, &&ConcFigures&)
                                                -&&ConcRepresentationFunc&(sr.concentrationconv*conversionfactor, &&ConcFigures&))*100
                                               /((&&ConcRepresentationFunc&(sr.concentrationconv*conversionfactor, &&ConcFigures&)
                                                 +&&ConcRepresentationFunc&(srm.concentrationconv*conversionfactor, &&ConcFigures&))/2)
                                      end, &&DecPlPercDiff&), &&DecPlPercDiff&)
        END bias,
        CASE WHEN trim(sr.concentrationstatus) is not null OR trim(srm.concentrationstatus) is not null OR sr.concentrationconv is null THEN 0 else 1 end validbias
    FROM &&TempTabPrefix&bio$run r,
         &&TempTabPrefix&bio$run$analytes ra,
         &&TempTabPrefix&bio$run$samples s,
         --&&TempTabPrefix&bio$sample$results wsr,
         &&TempTabPrefix&bio$run$sample$results sr,
         &&TempTabPrefix&bio$sample$results samr,
         &&TempTabPrefix&bio$sample sam,
         --&&TempTabPrefix&bio$BIOMATRIX BM,
         &&TempTabPrefix&bio$STUDY$TREATMENT tr,
         table(&&LALPackage&.GetRunStates) rs,
         &&TempTabPrefix&bio$run rm,
         &&TempTabPrefix&bio$run$analytes ram,
         &&TempTabPrefix&bio$run$samples sm,
         --&&TempTabPrefix&bio$assay$analyte aa,
           &&TempTabPrefix&bio$study$analytes sa,  --neu
           &&TempTabPrefix&bio$assay a,            --neu
         &&TempTabPrefix&bio$run$sample$results srm,
         &&TempTabPrefix&bio$sample samm,
         &&TempTabPrefix&bio$REASSAYDESIGNSAMPLES rds,
         (select masterkey analytecode, key concentrationunits, info conversionfactor
             from isr$crit 
           where entity = 'BIO$GRID$STUDY$ANALYUNITS' 
             and repid = &&RepID&) cu,
         table(&&LALPackage&.GetRunStates) rsm
    WHERE R.ID = s.runid
      --AND ra.assayanalytecode = aa.code    ---!!!
      AND ra.id = sr.runanalyteid    ---nnnneu
      AND a.id = r.assayid              -- neu
      AND sa.id = ra.analyteid ---- ids  neu
      AND ram.id = srm.runanalyteid    ---nnnneu
      --and a.id = rm.assayid              -- neu
      and sa.id = ram.analyteid ---- ids  neu
      and sa.code = cu.analytecode
      AND ra.runid = R.id
      --AND r.runtypedescription != 'MANDATORY REPEATS'
      AND s.ID = sr.runsampleid
      AND lower(s.sampletype) = 'unknown'
      --AND ra.assayanalytecode = SR.assayanalytecode  ---!!!
      --and sam.subjectgrouptreatmentid = bm.subjectgrouptreatmentid
      --and sam.sampletypesplitkey = bm.sampletypesplitkey
      AND s.studyid = '&&StudyID&'
      AND r.studyid = '&&StudyID&'
      --AND BM.STUDYID = '&&StudyID&'
      --AND r.studyassayid =s.studyassayid    ----!!
      AND RM.ID = sm.runid
      --AND ram.assayanalytecode = SRM.assayanalytecode  ---!!!
      AND ram.runid = RM.id
     -- AND rm.runtypedescription = 'MANDATORY REPEATS'
      AND sm.ID = srm.runsampleid
      AND lower(sm.sampletype) = 'unknown'
      AND sm.studyid = '&&StudyID&'
      AND srm.studyid = '&&StudyID&'
      AND rm.studyid = '&&StudyID&'
      --AND rm.studyassayid =sm.studyassayid AND sm.studyassayid = srm.studyassayid AND s.studyassayid = sr.studyassayid  ----!!
      AND sam.studyid = '&&StudyID&'
      AND sm.sampleid = samm.ID
      and RDS.STUDYCODE || '-' || RDS.TOSAMPLEID = sam.code
      and rds.STUDYCODE || '-' || RDS.FROMSAMPLEID = samm.code
      and RDS.STUDYCODE || '-' || RDS.TOSAMPLEID = s.samplecode
      and rds.STUDYCODE || '-' || RDS.FROMSAMPLEID = sm.samplecode
      --and sr.designsampleid = rds.TOSAMPLEID
      --and rds.STUDYCODE = s.studycode
      --and rds.STUDYCODE = sm.studycode
      AND rs.column_value = r.runstatusnum
      AND rsm.column_value = rm.runstatusnum
      --AND sm.samplecode = s.samplecode
      --AND sr.ANALYTECODE = aa.analytecode
      --AND SRM.assayanalytecode = aa.code  ---!!!
      --AND SR.assayanalytecode = aa.code   ---!!!
      AND sr.studyid = a.studyid
        --AND ((r.runid = sr.runid and sr.REASSAYSPRESENTFLAG = 'N')
         --or (sr.runid is null and sr.REASSAYSPRESENTFLAG = 'Y'))
      AND s.isdeactivated = 'F'
      AND sm.isdeactivated = 'F'
      and sm.analysistype = 'ISR'
      AND samr.samplecode = sam.code
      AND samr.studycode || '-' || samr.runid = SR.RUNCODE
      and sam.treatmentcode = tr.code
      AND sm.samplestudyid = sm.studycode
      AND s.samplestudyid = s.studycode 
      AND sa.concentrationunits != 'Titer Units'
    and (s.id, s.studycode) not in 
        (select rh.runsampleid, rh.studycode from &&TempTabPrefix&bio$REASSAY$HISTORY rh
          where     rh.runsampleid = s.ID
                and rh.STUDYCODE = s.studycode
                and rh.REASON_FOR_REASSAY like '%Inadvertent Repeat%') &&AddCondition&
      )
      ) where (cnt = 1 or ischoice = 'Y')
      GROUP BY analyteid, analyteorder, species, matrix, concentrationunits
      )
    ) FROM dual