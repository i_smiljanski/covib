  SELECT XMLELEMENT("experiment", XMLATTRIBUTES('&&Experiment&' AS "name" ,'&&BTCalcType&' AS "type"),
  (  
  select 
  Xmlagg(XMLConcat(  
    Xmlagg(XMLELEMENT("calculation", xmlconcat(targ,val,res)) order by analyteid, analyteorder, species, matrix, stabilitytype, temperature&&TargAddOrder&),
    XMLELEMENT("statistic", XMLATTRIBUTES(analyteid AS "analyteid", species AS "species", matrix AS "matrix", stabilitytype AS "stabilitytype" ),
      XMLELEMENT("flagpercent", min(flagpercent)),
      XMLELEMENT("min-bias", min(bias)),
      XMLELEMENT("max-bias", max(bias)),
      XMLELEMENT("min-biasonmean", min(biasonmean)),
      XMLELEMENT("max-biasonmean", max(biasonmean)),
      XMLELEMENT("min-cv", min(cv)),
      XMLELEMENT("max-cv", max(cv)),
      XMLELEMENT("numacc", sum(cntunflagged)),
      XMLELEMENT("numtotal", sum(cnt)),
      XMLELEMENT("percacc", case when sum(cnt) != 0 then FmtNum(round(sum(cntunflagged)/sum(cnt)*100,&&DecPlPercStat&)) else null end)
    )))
    from (
    SELECT analyteid, analyteorder, species, matrix, stabilitytype, temperature,
      Count(useconc) cnt,
      count(unflagged) cntunflagged,
      min(flagpercent) flagpercent,
      FormatRounded(round(Avg(usebias),&&DecPlPercBias&),&&DecPlPercBias&) bias,
      FormatRounded(round(case when nominalconc = 0 then null
                               when '&&FinalAccuracy&'='T' then (Avg(useconc)-nominalconc)/nominalconc*100
                               else (&&ConcRepresentationFunc&(Avg(useconc),&&ConcFigures&)-nominalconc_sigrnd)/nominalconc_sigrnd*100
                          end,&&DecPlPercBias&),&&DecPlPercBias&) biasonmean,
      FormatRounded(round(case when '&&FinalAccuracy&'='T' then Stddev(useconc)/Avg(useconc)*100
                               else &&ConcRepresentationFunc&(Stddev(useconc),&&ConcFigures&)
                                   /&&ConcRepresentationFunc&(Avg(useconc),&&ConcFigures&)*100
                          end,&&DecPlPercCV&),&&DecPlPercCV&) cv&&DilColumn&,
      XMLELEMENT("target", 
        XMLELEMENT("knownname", knownname),
        XMLELEMENT("analyteid", analyteid),
        XMLELEMENT("species", species),
        XMLELEMENT("matrix", matrix),
        XMLELEMENT("stabilitytype", stabilitytype)&&DilTarget&,
        XMLELEMENT("temperature", temperature),
        XMLELEMENT("percent", percent),
        XMLELEMENT("unit", concentrationunits)        
        ) targ,
      XMLELEMENT("values", Xmlagg(
        XMLELEMENT("sample",  XMLATTRIBUTES(sampleid AS "sampleid", sampleresultid AS "sampleresultid" ),
          XMLELEMENT("knownid", knownid), 
          XMLELEMENT("nominalconc", &&ConcFormatFunc&(nominalconc_sigrnd,&&ConcFigures&)),
          XMLELEMENT("runid", runid),
          XMLELEMENT("cycles", cycles),
          XMLELEMENT("hours", hours),
          XMLELEMENT("longtermtime", longtermtime),
          XMLELEMENT("longtermunits", longtermunits),
          XMLELEMENT("name", name),
          XMLELEMENT("suffix", suffix),
          XMLELEMENT("concentration", &&ConcFormatFunc&(&&ConcRepresentationFunc&(conc,&&ConcFigures&),&&ConcFigures&)),
          XMLELEMENT("bias", FormatRounded(bias,&&DecPlPercBias&)),
          XMLELEMENT("resultcommenttext", resultcommenttext),
          XMLELEMENT("meanconc", &&ConcFormatFunc&(meanconc,&&ConcFigures&)),
          XMLELEMENT("meansd", &&ConcFormatFunc&(meansd,&&ConcFigures&)),
          XMLELEMENT("meanbias", FormatRounded(meanbias,&&DecPlPercBias&)),
          XMLELEMENT("meancv", FormatRounded(meancv,&&DecPlPercCV&)),  
          XMLELEMENT("concentrationstatus", concentrationstatus),
          XMLELEMENT("flag", flagged),
          XMLELEMENT("deactivated", isdeactivated)
        ) order by runno, cycles, hours, longtermtime, longtermunits, suffixleft, suffix, runsamplesequencenumber)) val,
      XMLELEMENT("result",       
        XMLELEMENT("n", Count(useconc)),
        XMLELEMENT("mean", &&ConcFormatFunc&(&&ConcRepresentationFunc&(Avg(useconc),&&ConcFigures&),&&ConcFigures&)),
        XMLELEMENT("sd", &&ConcFormatFunc&(&&ConcRepresentationFunc&(Stddev(useconc),&&ConcSDFigures&),&&ConcSDFigures&)),
        XMLELEMENT("cv", FormatRounded(round(case when '&&FinalAccuracy&'='T' then Stddev(useconc)/Avg(useconc)*100
                                                  else &&ConcRepresentationFunc&(Stddev(useconc),&&ConcFigures&)
                                                      /&&ConcRepresentationFunc&(Avg(useconc),&&ConcFigures&)*100
                                             end,&&DecPlPercCV&),&&DecPlPercCV&)),
        XMLELEMENT("bias", FormatRounded(round(Avg(usebias),&&DecPlPercBias&),&&DecPlPercBias&)),
        XMLELEMENT("biasonmean", FormatRounded(round(case when nominalconc = 0 then null
                                                    when '&&FinalAccuracy&'='T' then (Avg(useconc)-nominalconc)/nominalconc*100
                                                    else (&&ConcRepresentationFunc&(Avg(useconc),&&ConcFigures&)-nominalconc_sigrnd)/nominalconc_sigrnd*100
                                               end,&&DecPlPercBias&),&&DecPlPercBias&)),
        XMLELEMENT("flagpercent", min(flagpercent))
        ) res
    FROM
    (select knownID, sampleID, sampleresultID, ISDEACTIVATED,
     name, knownname, suffix, suffixleft, percent, resultcommenttext, 
     nominalconc, nominalconc_sigrnd, analyteid, concentrationunits, analyteorder, species, matrix, stabilitytype, runid, runno, flagpercent,
     conc, useconc, dilutionfactor, runsamplesequencenumber, concentrationstatus,
     cycles, hours, temperature, longtermtime, longtermunits,
     bias, usebias,--meanconc, meansd, meanbias, meancv,
     case when abs(bias) > flagpercent then '&&FlaggedSymbol&' else null end flagged,
     case when ISDEACTIVATED <> 'T' and abs(bias) <= flagpercent then 1 else null end unflagged,
     &&ConcRepresentationFunc&(avg(useconc) over (partition by cycles, hours, temperature, longtermtime, longtermunits, runid, name, knownname, analyteid, analyteorder, species, matrix, stabilitytype),&&ConcFigures&) meanconc,
     &&ConcRepresentationFunc&(stddev(useconc) over (partition by cycles, hours, temperature, longtermtime, longtermunits, runid, name, knownname, analyteid, analyteorder, species, matrix, stabilitytype),&&ConcFigures&) meansd,
     round(avg(usebias) over (partition by cycles, hours, temperature, longtermtime, longtermunits, runid, name, knownname, analyteid, analyteorder, species, matrix, stabilitytype),&&DecPlPercBias&) meanbias,
     round(case when '&&FinalAccuracy&'='T' then stddev(useconc) over (partition by cycles, hours, temperature, longtermtime, longtermunits, runid, name, knownname, analyteid, analyteorder, species, matrix, stabilitytype)
                      /avg(useconc) over (partition by cycles, hours, temperature, longtermtime, longtermunits, runid, name, knownname, analyteid, analyteorder, species, matrix, stabilitytype)*100 
                else &&ConcRepresentationFunc&(stddev(useconc) over (partition by cycles, hours, temperature, longtermtime, longtermunits, runid, name, knownname, analyteid, analyteorder, species, matrix, stabilitytype),&&ConcFigures&)
                    /&&ConcRepresentationFunc&(avg(useconc) over (partition by cycles, hours, temperature, longtermtime, longtermunits, runid, name, knownname, analyteid, analyteorder, species, matrix, stabilitytype),&&ConcFigures&)
                    *100
           end, &&DecPlPercCV&) meancv

     FROM
    (SELECT K.ID knownID, s.ID sampleID, sr.ID sampleresultID, s.ISDEACTIVATED, k.cycles, k.hours, k.temperature, k.longtermtime, k.longtermunits,
        K.NAME, k.namesuffix suffix, sr.resultcommenttext, 
        case when REGEXP_LIKE ( substr(k.namesuffix, 1, instr(k.namesuffix,':')-1), '^[-+]?[0-9]+[.]?[0-9]*([eE][-+]?[0-9]+)?$') then to_number(substr(k.namesuffix, 1, instr(k.namesuffix,':')-1),'99999999.9999999') else null end suffixleft,
        case when instr(k.nameprefix,'P',-1,1)>instr(k.nameprefix,'.',-1,1) and instr(k.nameprefix,'.') > 0 then substr(k.nameprefix,instr(k.nameprefix,'.',-1,1)+1,instr(k.nameprefix,'P',-1,1)-instr(k.nameprefix,'.',-1,1)-1) else null end percent,
        case when instr(k.nameprefix,'P',-1,1)>instr(k.nameprefix,'.',-1,1) and instr(k.nameprefix,'.') > 0 then substr(k.nameprefix,1,instr(k.nameprefix,'.',-1,1)-1) else k.nameprefix end knownname,
        sr.nominalconcentrationconv nominalconc,
        &&ConcRepresentationFunc&(sr.nominalconcentrationconv,&&ConcFigures&) nominalconc_sigrnd,
        ra.analyteid, ra.concentrationunits, sa.analyteorder, a.species, a.sampletypeid as matrix, s.sampletype, k.stabilitytype, s.runid, r.runid runno, k.flagpercent,
        1/aliquotfactor dilutionfactor, s.runsamplesequencenumber, sr.concentrationstatus,
        case when '&&FinalAccuracy&'='T' then sr.concentrationconv
             else &&ConcRepresentationFunc&(sr.concentrationconv,&&ConcFigures&)
        end conc, 
        case when ISDEACTIVATED = 'T' then null
             else case when '&&FinalAccuracy&'='T' then sr.concentrationconv
                  else &&ConcRepresentationFunc&(sr.concentrationconv,&&ConcFigures&)
             end
        end useconc, 
        case when sr.nominalconcentrationconv = 0 then null
             else  round((&&ConcRepresentationFunc&(sr.concentrationconv,&&ConcFigures&)-
                          &&ConcRepresentationFunc&(sr.nominalconcentrationconv,&&ConcFigures&))*100
                         /&&ConcRepresentationFunc&(sr.nominalconcentrationconv,&&ConcFigures&),&&DecPlPercBias&)
        end bias,
        case when ISDEACTIVATED = 'T' then null
             when sr.nominalconcentrationconv = 0 then null
             else case when '&&FinalAccuracy&'='T' then (sr.concentrationconv-sr.nominalconcentrationconv)*100/sr.nominalconcentrationconv
                       else round((&&ConcRepresentationFunc&(sr.concentrationconv,&&ConcFigures&)-
                                   &&ConcRepresentationFunc&(sr.nominalconcentrationconv,&&ConcFigures&))*100
                                  /&&ConcRepresentationFunc&(sr.nominalconcentrationconv,&&ConcFigures&),&&DecPlPercBias&) 
             end
        end usebias   
      FROM &&TempTabPrefix&bio$run$ana$known K,
           &&TempTabPrefix&bio$run r,
           &&TempTabPrefix&bio$run$analytes ra,
           &&TempTabPrefix&bio$study$analytes sa,
           &&TempTabPrefix&bio$assay a,
           &&TempTabPrefix&bio$run$samples s,
           &&TempTabPrefix&bio$run$sample$results sr,
           isr$crit kwz,
           table(&&LALPackage&.GetRunStates) rs
      WHERE K.ID = s.runknownID
        AND R.ID = s.runid AND R.ID = sr.runid
        --AND r.studyassayid =s.studyassayid AND s.studyassayid = sr.studyassayid
        AND r.RuntypeDescription in (&&RunTypes&)
        AND s.ID = sr.runsampleID
        AND s.sampletype='known'
        and k.knowntype = s.runsamplekind
        and kwz.masterkey = '&&ExpMasterkey&'
        AND ra.id = sr.runanalyteid
        AND sa.id = ra.analyteid
        AND a.id = r.assayid
        AND ra.runid = s.runid
        AND ra.studyID = '&&StudyID&'
        AND K.studyid = '&&StudyID&' AND s.studyid = '&&StudyID&'
        AND sr.studyID = '&&StudyID&' AND r.studyid = '&&StudyID&'
        and kwz.key = K.NAMEPREFIX
        and kwz.entity = '&&KnownEntity&'
        and repid = &&RepID&
        AND rs.column_value = r.runstatusnum
        &&AddCondition&
     ))
    GROUP BY knownname, analyteid, analyteorder, species, matrix, stabilitytype&&DilColumnGroupBy&,percent, temperature, nominalconc, nominalconc_sigrnd, concentrationunits)
    GROUP BY analyteid, analyteorder, species, matrix, stabilitytype)
    ) FROM dual
