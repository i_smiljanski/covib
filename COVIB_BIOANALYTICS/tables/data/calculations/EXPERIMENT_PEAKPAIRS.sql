  SELECT XMLELEMENT("experiment", XMLATTRIBUTES('&&Experiment&' AS "name" ,'&&PeakPairsCalcType&' AS "type"),
  (
  select
  Xmlagg(XMLConcat(
    XMLELEMENT("statistic",
      XMLATTRIBUTES(analyteid AS "analyteid",
                    species AS "species",
                    matrix AS "matrix",
                    internalstdname AS "internalstdname",
                    runid AS "runid" ),
        XMLELEMENT("ref_temperature", ref_temperature),
        XMLELEMENT("test_temperature", test_temperature),
        XMLELEMENT("ref_preparationdate", ref_preparationdate),
        XMLELEMENT("test_preparationdate", test_preparationdate),
        XMLELEMENT("test_hours", iSR$Format.FmtNum(hours)),
        XMLELEMENT("ref_name", ref_name),
        XMLELEMENT("test_name", test_name),
        XMLELEMENT("ref-nominalconc", ref_nominalconc),
        XMLELEMENT("bias-perc-ratio", FormatRounded(round(100+bias_ratio, &&DecPlPercBias&), &&DecPlPercBias&)),
        XMLELEMENT("bias-perc_analytearea", FormatRounded(round(100+bias_analytearea, &&DecPlPercBias&), &&DecPlPercBias&)),
        XMLELEMENT("bias-perc-internalstandardarea", FormatRounded(round(100+bias_internalstandardarea, &&DecPlPercBias&), &&DecPlPercBias&)),
        XMLELEMENT("bias-ratio", FormatRounded(round(bias_ratio, &&DecPlPercBias&), &&DecPlPercBias&)),
        XMLELEMENT("bias-inv-ratio", FormatRounded(round(bias_inv_ratio, &&DecPlPercBias&), &&DecPlPercBias&)),
        XMLELEMENT("bias-analytearea", FormatRounded(round(bias_analytearea, &&DecPlPercBias&), &&DecPlPercBias&)),
        XMLELEMENT("bias-internalstandardarea", FormatRounded(round(bias_internalstandardarea, &&DecPlPercBias&), &&DecPlPercBias&)),
        XMLELEMENT("max-abs-bias-ratio", FormatRounded(round(max_abs_bias_ratio, &&DecPlPercBias&), &&DecPlPercBias&)),
        XMLELEMENT("max-abs-bias-inv-ratio", FormatRounded(round(max_abs_bias_inv_ratio, &&DecPlPercBias&), &&DecPlPercBias&)),
        XMLELEMENT("max-cv-ratio", FormatRounded(round(max_cv_ratio, &&DecPlPercCV&), &&DecPlPercCV&))
    ),refcalc,testcalc
  )order by analyteid, analyteorder, species, matrix, internalstdname, runid, hours, ref_nominalconc)
  from (
    with analyterun as (
    SELECT analyteid, analyteorder, species, matrix, internalstdname, runid, temperature, preparationdate, name, hours,
      nominalconc,
      case when '&&FinalAccuracy&' = 'T' then Avg(useratio)
           else &&PeakRatioRepresentationFunc&(Avg(useratio), &&PeakRatioFigures&)
      end meanratio,
      case when '&&FinalAccuracy&' = 'T' then Avg(useanalytearea)
           else &&PeakRepresentationFunc&(Avg(useanalytearea), &&PeakFigures&)
      end meananalytearea,
      case when '&&FinalAccuracy&' = 'T' then Avg(useinternalstandardarea)
           else round(Avg(useinternalstandardarea), &&PeakFigures&)
      end meaninternalstandardarea,
      case when &&PeakRatioRepresentationFunc&(Avg(useratio), &&PeakRatioFigures&) = 0 then null
           when '&&FinalAccuracy&' = 'T' then Stddev(useratio)/Avg(useratio)*100
           else &&PeakRatioRepresentationFunc&(Stddev(useratio), &&PeakRatioSDFigures&)
               /&&PeakRatioRepresentationFunc&(Avg(useratio), &&PeakRatioFigures&)*100
      end max_cv_ratio,
      XMLELEMENT("target",
        XMLELEMENT("analyteid", analyteid),
        XMLELEMENT("species", species),
        XMLELEMENT("matrix", matrix),
        XMLELEMENT("internalstdname", internalstdname),
        XMLELEMENT("name", name),
        XMLELEMENT("preparationdate", preparationdate),
        XMLELEMENT("runid", runid),
        XMLELEMENT("temperature", temperature),
        XMLELEMENT("hours", iSR$Format.FmtNum(hours)),
        XMLELEMENT("longtermtime", longtermtime),
        XMLELEMENT("longtermunits", longtermunits)
        ) targ,
      XMLELEMENT("values", Xmlagg(
        XMLELEMENT("sample", XMLATTRIBUTES(sampleid AS "sampleid", sampleresultrawid AS "sampleresultrawid" ),
          XMLELEMENT("replicatenumber", replicatenumber),
          XMLELEMENT("ratio", &&PeakRatioFormatFunc&(&&PeakRatioRepresentationFunc&(ratio, &&PeakRatioFigures&), &&PeakRatioFigures&)),
          XMLELEMENT("analytearea", &&PeakFormatFunc&(&&PeakRepresentationFunc&(analytearea, &&PeakFigures&), &&PeakFigures&)),
          XMLELEMENT("internalstandardarea", &&PeakFormatFunc&(&&PeakRepresentationFunc&(internalstandardarea, &&PeakFigures&), &&PeakFigures&)),
          XMLELEMENT("unc-ratio", &&PeakRatioFormatFunc&(&&PeakRatioRepresentationFunc&(unc_ratio, &&PeakRatioFigures&), &&PeakRatioFigures&)),
          XMLELEMENT("unc-analytearea", &&PeakFormatFunc&(&&PeakRepresentationFunc&(unc_analytearea, &&PeakFigures&), &&PeakFigures&)),
          XMLELEMENT("unc-internalstandardarea", &&PeakFormatFunc&(&&PeakRepresentationFunc&(unc_internalstandardarea, &&PeakFigures&), &&PeakFigures&)),
          XMLELEMENT("runsamplesequencenumber", runsamplesequencenumber),
          XMLELEMENT("deactivated", isdeactivated))
          order by runsamplesequencenumber, replicatenumber)
          ) val,
      XMLELEMENT("result",
        XMLELEMENT("n-ratio", Count(case when useratio is not null then 1 else null end)),
        XMLELEMENT("m-ratio", Count(ratio)),
        XMLELEMENT("npercm-ratio", FormatRounded(case when Count(ratio) = 0 then null else round(Count(case when useratio is not null then 1 else null end)/Count(ratio)*100, &&DecPlPercBias&) end, &&DecPlPercBias&)),
        XMLELEMENT("mean-ratio", &&PeakRatioFormatFunc&(&&PeakRatioRepresentationFunc&(Avg(useratio), &&PeakRatioFigures&), &&PeakRatioFigures&)),
        XMLELEMENT("sd-ratio", &&PeakRatioFormatFunc&(&&PeakRatioRepresentationFunc&(Stddev(useratio), &&PeakRatioSDFigures&), &&PeakRatioSDFigures&)),
        XMLELEMENT("cv-ratio", FormatRounded(round(case when &&PeakRatioRepresentationFunc&(Avg(useratio), &&PeakRatioFigures&) = 0 then null
                                                        when '&&FinalAccuracy&' = 'T' then Stddev(useratio)/Avg(useratio)*100
                                                        else &&PeakRatioRepresentationFunc&(Stddev(useratio), &&PeakRatioSDFigures&)
                                                            /&&PeakRatioRepresentationFunc&(Avg(useratio), &&PeakRatioFigures&)*100
                                                   end, &&DecPlPercCV&), &&DecPlPercCV&)),
        XMLELEMENT("n-analytearea", Count(case when useanalytearea is not null then 1 else null end)),
        XMLELEMENT("m-analytearea", Count(analytearea)),
        XMLELEMENT("npercm-analytearea", FormatRounded(round(Count(case when useanalytearea is not null then 1 else null end)/Count(analytearea)*100, &&DecPlPercBias&), &&DecPlPercBias&)),
        XMLELEMENT("mean-analytearea", &&PeakFormatFunc&(&&PeakRepresentationFunc&(Avg(useanalytearea), &&PeakFigures&), &&PeakFigures&)),
        XMLELEMENT("sd-analytearea", &&PeakFormatFunc&(&&PeakRepresentationFunc&(Stddev(useanalytearea), &&PeakSDFigures&), &&PeakSDFigures&)),
        XMLELEMENT("cv-analytearea", FormatRounded(round(case when '&&FinalAccuracy&' = 'T' then case when Avg(useanalytearea) = 0 then null else Stddev(useanalytearea)/Avg(useanalytearea)*100 end
                                                  else case when &&PeakRepresentationFunc&(Avg(useanalytearea), &&PeakFigures&) = 0 then null
                                                            else &&PeakRepresentationFunc&(Stddev(useanalytearea), &&PeakSDFigures&)
                                                                /&&PeakRepresentationFunc&(Avg(useanalytearea), &&PeakFigures&)*100
                                                       end
                                             end, &&DecPlPercCV&), &&DecPlPercCV&)),
        XMLELEMENT("n-internalstandardarea", Count(case when useinternalstandardarea is not null then 1 else null end)),
        XMLELEMENT("m-internalstandardarea", Count(internalstandardarea)),
        XMLELEMENT("npercm-internalstandardarea", FormatRounded(round(Count(case when useinternalstandardarea is not null then 1 else null end)/Count(internalstandardarea)*100, &&DecPlPercBias&), &&DecPlPercBias&)),
        XMLELEMENT("mean-internalstandardarea", &&PeakFormatFunc&(&&PeakRepresentationFunc&(Avg(useinternalstandardarea), &&PeakFigures&), &&PeakFigures&)),
        XMLELEMENT("sd-internalstandardarea", &&PeakFormatFunc&(&&PeakRepresentationFunc&(Stddev(useinternalstandardarea), &&PeakSDFigures&), &&PeakSDFigures&)),
        XMLELEMENT("cv-internalstandardarea", FormatRounded(round(case when '&&FinalAccuracy&' = 'T' then case when Avg(useinternalstandardarea) = 0 then null else Stddev(useinternalstandardarea)/Avg(useinternalstandardarea)*100 end
                                                  else case when &&PeakRepresentationFunc&(Avg(useinternalstandardarea), &&PeakFigures&) = 0 then null
                                                            else &&PeakRepresentationFunc&(Stddev(useinternalstandardarea), &&PeakSDFigures&)
                                                                /&&PeakRepresentationFunc&(Avg(useinternalstandardarea), &&PeakFigures&)*100
                                                       end
                                             end, &&DecPlPercCV&), &&DecPlPercCV&))
        ) res
    FROM
    (
    SELECT knownID, sampleID, sampleresultrawID, Name, nominalconc, runid, analyteid, analyteorder, species, matrix, internalstdname, replicatenumber,
     ratio, analytearea, internalstandardarea, ISDEACTIVATED, runsamplesequencenumber, temperature,
     stabilitytype, preparationdate, cycles, hours, longtermtime, longtermunits,
     unc_ratio, unc_analytearea, unc_internalstandardarea,
     case when ISDEACTIVATED = 'T' then null else ratio end useratio,
     case when ISDEACTIVATED = 'T' then null else analytearea end useanalytearea,
     case when ISDEACTIVATED = 'T' then null else internalstandardarea end useinternalstandardarea
     FROM
     (SELECT K.ID knownID, s.ID sampleID, srw.ID sampleresultrawID, K.NAME, ISDEACTIVATED,
        s.runid, s.runsamplesequencenumber, k.concentrationconv nominalconc,
        ra.analyteid, sa.analyteorder, a.species, a.sampletypeid matrix, srw.internalstdname, r.runtypedescription, s.replicatenumber, k.flagpercent,
        k.stabilitytype, k.preparationdate, k.cycles, k.hours, k.longtermtime, k.longtermunits, k.temperature,
        case when '&&FinalAccuracy&' = 'T' then srw.ratio/case when regexp_like(K.name,'Test') then &&PeakCorrectionFactor& else 1 end
             else &&PeakRatioRepresentationFunc&(srw.ratio/case when regexp_like(K.name,'Test') then &&PeakCorrectionFactor& else 1 end, &&PeakRatioFigures&)
        end ratio,
        case when '&&FinalAccuracy&' = 'T' then srw.analytearea/case when regexp_like(K.name,'Test') then &&PeakCorrectionFactor& else 1 end
             else &&PeakRepresentationFunc&(srw.analytearea/case when regexp_like(K.name,'Test') then &&PeakCorrectionFactor& else 1 end, &&PeakFigures&)
        end analytearea,
        case when '&&FinalAccuracy&' = 'T' then srw.internalstandardarea/case when regexp_like(K.name,'Test') then &&PeakCorrectionFactor& else 1 end
             else &&PeakRepresentationFunc&(srw.internalstandardarea/case when regexp_like(K.name,'Test') then &&PeakCorrectionFactor& else 1 end, &&PeakFigures&)
        end internalstandardarea,
        &&PeakRatioRepresentationFunc&(srw.ratio, &&PeakRatioFigures&) unc_ratio, &&PeakRepresentationFunc&(srw.analytearea, &&PeakFigures&) unc_analytearea, &&PeakRepresentationFunc&(srw.internalstandardarea, &&PeakFigures&) unc_internalstandardarea
      FROM &&TempTabPrefix&bio$run$ana$known K,
           &&TempTabPrefix&bio$run r,
           &&TempTabPrefix&bio$run$analytes ra,
           &&TempTabPrefix&bio$study$analytes sa,
           &&TempTabPrefix&bio$assay a,
           &&TempTabPrefix&bio$run$samples s,
           &&TempTabPrefix&bio$run$sample$result$raw srw,
           isr$crit kwz,
           table(&&LALPackage&.GetRunStates) rs
      WHERE K.ID = s.runknownID
        AND R.ID = s.runid AND R.ID = srw.runid
        --AND r.studyassayid =s.studyassayid AND s.studyassayid = srw.studyassayid
        AND r.RuntypeDescription in (&&RunTypes&)
        AND s.ID = srw.runsampleID
        AND s.sampletype = 'known'
        and k.knowntype = s.runsamplekind
        and kwz.masterkey = '&&ExpMasterkey&'
        AND ra.id = srw.runanalyteid
        AND sa.id = ra.analyteid
        AND a.id = r.assayid
        --AND ra.assayanalyteid = aa.ID
        AND ra.runid = s.runid
        AND ra.studyID = '&&StudyID&'
        AND K.studyid = '&&StudyID&' AND s.studyid = '&&StudyID&'
        AND srw.studyID = '&&StudyID&' AND r.studyid = '&&StudyID&'
        and kwz.key = K.NAME
        and kwz.entity = '&&KnownEntity&'
        and repid = &&RepID&
        AND rs.column_value = r.runstatusnum
        &&AddCondition&
     ))
     GROUP BY analyteid, analyteorder, species, matrix, internalstdname, name, runid, temperature, stabilitytype, preparationdate, cycles, hours, longtermtime, longtermunits, nominalconc
     )
     select
       case when ref.meanratio = 0 then null else (ref.meanratio - test.meanratio)/ref.meanratio*100 end bias_ratio,
       max(abs(
         case
          when ref.meanratio = 0 then
            null
          else
            (ref.meanratio - test.meanratio)/ref.meanratio*100
       end)) over (partition by test.analyteid, test.analyteorder, test.species, test.matrix, test.runid) max_abs_bias_ratio,
       case when ref.meanratio = 0 then null else (test.meanratio - ref.meanratio)/ref.meanratio*100 end bias_inv_ratio,
       max(abs(
         case
          when ref.meanratio = 0 then
            null
          else
            (ref.meanratio - test.meanratio)/ref.meanratio*100
       end)) over (partition by test.analyteid, test.analyteorder, test.species, test.matrix, test.runid) max_abs_bias_inv_ratio,
       case when ref.meananalytearea = 0 then null else (ref.meananalytearea - test.meananalytearea )/ref.meananalytearea*100 end bias_analytearea,
       case when ref.meaninternalstandardarea = 0 then null else (ref.meaninternalstandardarea - test.meaninternalstandardarea)/ref.meaninternalstandardarea*100 end bias_internalstandardarea,
       ref.analyteid, ref.analyteorder, ref.species, ref.matrix, ref.internalstdname, ref.runid, ref.temperature ref_temperature, test.temperature test_temperature,
       ref.name ref_name, test.name test_name, --ref.targ,ref.val,ref.res,
       ref.preparationdate ref_preparationdate, test.preparationdate test_preparationdate,
       test.hours, ref.nominalconc ref_nominalconc, greatest(ref.max_cv_ratio,test.max_cv_ratio) max_cv_ratio,
       XMLELEMENT("calculation", xmlconcat(ref.targ,ref.val,ref.res)) refcalc,
       XMLELEMENT("calculation", xmlconcat(test.targ,test.val,test.res)) testcalc
     from analyterun ref,analyterun test
     where ref.analyteid = test.analyteid
       and ref.species = test.species
       and ref.matrix = test.matrix
       and ref.internalstdname = test.internalstdname
       and ref.runid = test.runid
       &&JoinCondition&
  ))
  ) FROM dual