SELECT XMLELEMENT("experiment", XMLATTRIBUTES('&&Experiment&' AS "name" ,'response' AS "type"),
      --Xmlagg ( XMLConcat(
        Xmlagg (XMLELEMENT("calculation", xmlconcat(targ,val/*,res*/)) order by studyid, analyteid, species, matrix, hours, longtermunits, longtermtime, temperature, nominalconc, name)
        --XMLELEMENT("statistic", XMLATTRIBUTES(analyteid AS "analyteid"),
               
             --)) order by studyid, analyteid, species, matrix, hours, longtermunits, longtermtime, temperature
      )--)
  FROM (
  SELECT
    studyid, analyteid, species, matrix, hours, longtermunits, longtermtime, temperature, nominalconc, name,
    --sum(s1_n)+sum(s2_n) n, sum(s1_m)+sum(s2_m) m,
    XMLELEMENT("target",
      XMLELEMENT("studyid", studyid),
      XMLELEMENT("analyteid", analyteid),
      XMLELEMENT("species", species),
      XMLELEMENT("matrix", matrix),
      XMLELEMENT("hours", hours),
      XMLELEMENT("longtermunits", longtermunits),
      XMLELEMENT("longtermtime", longtermtime),
      XMLELEMENT("temperature", temperature),
      XMLELEMENT("nominalconc", nominalconc),
      XMLELEMENT("name", name)
    ) as targ,
    XMLELEMENT("values",
      Xmlagg(
        XMLELEMENT("subject",
          XMLATTRIBUTES(rid AS "runid", runid AS "runno", subject AS "subject"),
          XMLELEMENT("subject", subject),
          XMLELEMENT("sample1", s1_xml),
          XMLELEMENT("nc", nc_xml),
          XMLELEMENT("s1-mean", FormatSig(sigFigure(s1_mean_useresponse_sig, &&ResponseFigures&), &&ResponseFigures&)),
          XMLELEMENT("screen-cp", case
                                    when '&&FixedScreenCutpoint&' is not null then
                                      FormatSig(sigFigure(to_number('&&FixedScreenCutpoint&'), &&ResponseFigures&), &&ResponseFigures&)
                                    when '&&ScreenCPNFactor&' is not null then
                                      FormatSig(sigFigure(nc_meanmedian_useresponse_sig*to_number('&&ScreenCPNFactor&'), &&ResponseFigures&), &&ResponseFigures&)
                                    else
                                      FormatSig(sigFigure(nc_meanmedian_useresponse_sig, &&ResponseFigures&), &&ResponseFigures&)
                                  end),
          XMLELEMENT("n", s1_n),
          XMLELEMENT("m", s1_m),
          XMLELEMENT("s1-cv", FormatRounded(s1_cv_sig, &&DecPlPercCV&)),
          XMLELEMENT("nc-cv", FormatRounded(nc_cv_sig, 1)),
          XMLELEMENT("ratio", FormatRounded(round(case when nc_meanmedian_useresponse_sig = 0 then null
                                                        when '&&FinalAccuracy&' = 'T' then s1_mean_useresponse/nc_meanmedian_useresponse_sig--*100
                                                        else sigFigure(s1_mean_useresponse_sig, &&ResponseFigures&)/sigFigure(nc_meanmedian_useresponse_sig, &&ResponseFigures&)--*100
                                                   end, &&DecPlPercBias&), &&DecPlPercBias&))
        ) order by runid, subject
      )
    ) as val/*,
    XMLELEMENT("result", 
      XMLELEMENT("mean", FormatSig(sigFigure(Avg(s1_mean_useresponse_sig), &&ResponseFigures&), &&ResponseFigures&)),
      XMLELEMENT("nc-meanmedian", FormatSig(sigFigure(min(nc_meanmedian_useresponse_sig), &&ResponseFigures&), &&ResponseFigures&)),
      XMLELEMENT("n", count(s1_mean_useresponse)),
      XMLELEMENT("m", count(s1_mean_response)),
      XMLELEMENT("nc-n", min(nc_n)),
      XMLELEMENT("nc-m", min(nc_m))       
    ) as res*/
  FROM (
    SELECT
      sample1.studyid, sample1.analyteid, sample1.rid, sample1.runid, sample1.subject, sample1.name, sample1.nominalconc,
      sample1.species, sample1.matrix, sample1.hours, sample1.longtermunits, sample1.longtermtime, sample1.temperature,
      sample1.mean_response s1_mean_response, sample1.mean_response_sig s1_mean_response_sig,
      sample1.mean_useresponse s1_mean_useresponse, sample1.mean_useresponse_sig s1_mean_useresponse_sig,
      sample1.n s1_n, sample1.m s1_m, sample1.cv s1_cv, sample1.cv_sig s1_cv_sig, sample1.xml s1_xml,
      NC.meanmedian_response nc_meanmedian_response, NC.meanmedian_response_sig nc_meanmedian_response_sig,
      NC.meanmedian_useresponse nc_meanmedian_useresponse, NC.meanmedian_useresponse_sig nc_meanmedian_useresponse_sig,
      NC.n nc_n, NC.m nc_m, NC.cv nc_cv, NC.cv_sig nc_cv_sig, NC.xml nc_xml
    FROM
      (
        SELECT
          studyid, analyteid, rid, runid, subject, nominalconc, name,
          species, matrix, hours, longtermunits, longtermtime, temperature,
          Avg(response) mean_response,
          sigFigure(Avg(sigFigure(response, &&ResponseFigures&)), &&ResponseFigures&) mean_response_sig,
          Avg(case when(isdeactivated='F') then response else null end) mean_useresponse,
          sigFigure(Avg(sigFigure(case when(isdeactivated='F') then response else null end, &&ResponseFigures&)), &&ResponseFigures&) mean_useresponse_sig,
          Stddev(case when(isdeactivated='F') then response else null end)/Avg(case when(isdeactivated='F') then response else null end)*100 cv,
          round(
            sigFigure(Stddev(sigFigure(case when(isdeactivated='F') then response else null end, &&ResponseFigures&)), &&ResponseFigures&)/sigFigure(Avg(sigFigure(case when(isdeactivated='F') then response else null end, &&ResponseFigures&)), &&ResponseFigures&)*100
            , &&DecPlPercCV&) cv_sig,
          count(case when(isdeactivated='F') then 1 else null end) n,
          count(1) m,
          Xmlagg(
            XMLELEMENT("sample",
              XMLATTRIBUTES(sampleid AS "sampleid", sampleresultid AS "sampleresultid" ),
              XMLELEMENT("runsamplesequencenumber", runsamplesequencenumber),
              XMLELEMENT("samplesubtype", samplesubtype),
              XMLELEMENT("response", response),
              XMLELEMENT("response_sig", FormatSig(response, &&ResponseFigures&)),
              XMLELEMENT("status", concentrationstatus),
              XMLELEMENT("deactivated", isdeactivated)
            ) order by runsamplesequencenumber
          ) xml
        FROM (
          SELECT
            r.studyid, s.id sampleid, sr.id sampleresultid,
            s.designsampleid, s.samplename, to_number(regexp_replace(s.samplename, '^.*-(\d+)$', '\1', 1, 1, 'i')) subject,
            regexp_replace(s.samplename, '^(\w+-\d+).*$', '\1', 1, 1, 'i') name,
            r.id rid, r.runid, sa.id analyteid, s.samplesubtype,
            s.runsamplesequencenumber, sr.analytearea response,
            s.status, sr.commenttext, sr.concentration conc,
            sr.concentrationstatus, s.source, k.concentration nominalconc,
            sr.concentrationunits concentrationunit, a.species, a.sampletypeid matrix,
            --s.hours, s.longtermunits, s.longtermtime, s.temperature,
            &&StabilityInfo&
            case
              when s.runcode || '-' || s.runsamplesequencenumber || '-' || sa.analyteorder in (select code from &&TempTabPrefix&bio$deactivated$samples) then 'T'
              else 'F'
            end isdeactivated
          FROM 
            &&TempTabPrefix&bio$run r,
            &&TempTabPrefix&bio$run$analytes ra,
            &&TempTabPrefix&bio$study$analytes sa,
            &&TempTabPrefix&bio$assay a,
            &&TempTabPrefix&bio$run$worklist s,
            &&TempTabPrefix&bio$run$worklist$result$rw sr,
            &&TempTabPrefix&bio$run$ana$known k,
            isr$crit kwz--,
            --table(&&LALPackage&.GetRunStates) rs
          WHERE r.ID = s.runID
          AND s.ID = sr.worklistID
          AND a.ID = r.assayID
          AND r.studyID = 'S1' AND a.studyID = 'S1'
          --AND rs.column_value = r.runstatusnum
          AND sr.runAnalyteCode = ra.code
          AND r.ID = ra.runID
          AND ra.analyteID = sa.ID
          AND k.runAnalyteCode = ra.code AND k.name = s.sampleName
          AND kwz.key = s.samplename AND kwz.entity = '&&KnownEntity&' AND kwz.masterkey = '&&ExpMasterkey&' AND kwz.repid = &&RepID&
          --and s.runcode || '-' || s.runsamplesequencenumber || '-' || sa.analyteorder in (select code from &&TempTabPrefix&bio$deactivated$samples)
          &&AddConditionS1&
        )
        GROUP BY studyid, analyteid, species, matrix, hours, longtermunits, longtermtime, temperature, rid, runid, name, subject, nominalconc
      ) sample1, 
      (
        SELECT
          studyid, analyteid, rid, runid, species, matrix,
          &&NCMeanOrMedian&(
            case when stb$util.GetReportParameterValue('MEAN_OR_MEDIAN_NC',&&RepID&) = 'meanofmeans' then case when replicatenumber = 1 then meanresponse else null end
            else response end
          ) meanmedian_response,
          sigFigure(&&NCMeanOrMedian&(sigFigure(
            case when stb$util.GetReportParameterValue('MEAN_OR_MEDIAN_NC',&&RepID&) = 'meanofmeans' then case when replicatenumber = 1 then meanresponse else null end
            else response end
          , &&ResponseFigures&)), &&ResponseFigures&) meanmedian_response_sig,
          &&NCMeanOrMedian&(
            case when stb$util.GetReportParameterValue('MEAN_OR_MEDIAN_NC',&&RepID&) = 'meanofmeans' then
              case when replicatenumber = 1 then meanuseresponse else null end
            else
              case when(isdeactivated='F') then response else null end
            end
          ) meanmedian_useresponse,
          sigFigure(&&NCMeanOrMedian&(sigFigure(
            case when stb$util.GetReportParameterValue('MEAN_OR_MEDIAN_NC',&&RepID&) = 'meanofmeans' then
              case when replicatenumber = 1 then meanuseresponse else null end
            else
              case when(isdeactivated='F') then response else null end
            end
          , &&ResponseFigures&)), &&ResponseFigures&) meanmedian_useresponse_sig,
          Stddev(
            case when stb$util.GetReportParameterValue('MEAN_OR_MEDIAN_NC',&&RepID&) = 'meanofmeans' then
              case when replicatenumber = 1 then meanuseresponse else null end
            else
              case when(isdeactivated='F') then response else null end
            end
          )
           / &&NCMeanOrMedian&(
               case when stb$util.GetReportParameterValue('MEAN_OR_MEDIAN_NC',&&RepID&) = 'meanofmeans' then
                 case when replicatenumber = 1 then meanuseresponse else null end
               else
                 case when(isdeactivated='F') then response else null end
               end
             )
           * 100
           cv,
          round(
            sigFigure(Stddev(sigFigure(
              case when stb$util.GetReportParameterValue('MEAN_OR_MEDIAN_NC',&&RepID&) = 'meanofmeans' then
                case when replicatenumber = 1 then meanuseresponse else null end
              else
                case when(isdeactivated='F') then response else null end
              end
            , &&ResponseFigures&)), &&ResponseFigures&)
             / sigFigure(&&NCMeanOrMedian&(sigFigure(
                 case when stb$util.GetReportParameterValue('MEAN_OR_MEDIAN_NC',&&RepID&) = 'meanofmeans' then
                   case when replicatenumber = 1 then meanuseresponse else null end
                 else
                   case when(isdeactivated='F') then response else null end
                 end
               , &&ResponseFigures&)), &&ResponseFigures&)
            * 100
            , &&DecPlPercCV&) cv_sig,
          count(
            case when stb$util.GetReportParameterValue('MEAN_OR_MEDIAN_NC',&&RepID&) = 'meanofmeans' then
              case when replicatenumber = 1 then meanuseresponse else null end
            else
              case when(isdeactivated='F') then response else null end
            end
          ) n,
          count(
            case when stb$util.GetReportParameterValue('MEAN_OR_MEDIAN_NC',&&RepID&) = 'meanofmeans' then case when replicatenumber = 1 then meanresponse else null end
            else response end
          ) m,
          Xmlagg(
            XMLELEMENT("sample",
              XMLATTRIBUTES(sampleid AS "sampleid", sampleresultid AS "sampleresultid" ),
              XMLELEMENT("runsamplesequencenumber", runsamplesequencenumber),
              XMLELEMENT("samplesubtype", samplesubtype),
              XMLELEMENT("response", response),
              XMLELEMENT("response_sig", FormatSig(response, &&ResponseFigures&)),
              XMLELEMENT("status", concentrationstatus),
              XMLELEMENT("deactivated", isdeactivated)
            ) order by runsamplesequencenumber
          ) xml
        FROM (
          SELECT
            r.studyid, s.id sampleid, sr.id sampleresultid,
            s.designsampleid, s.samplename, s.replicatenumber,
            r.id rid, r.runid, sa.id analyteid, s.samplesubtype,
            s.runsamplesequencenumber, sr.analytearea response,
            s.status, sr.commenttext, sr.concentration conc,
            sr.concentrationstatus, s.source, s.concentration nominalconc,
            sr.concentrationunits concentrationunit, a.species, a.sampletypeid matrix,
            s.hours, s.longtermunits, s.longtermtime, s.temperature,
            Avg(case when(not(s.runcode || '-' || s.runsamplesequencenumber || '-' || sa.analyteorder in (select code from &&TempTabPrefix&bio$deactivated$samples))) then sr.analytearea else null end) over (partition by r.studyid, sa.id, r.id, s.samplename) meanresponse,
            Avg(case when(not(s.runcode || '-' || s.runsamplesequencenumber || '-' || sa.analyteorder in (select code from &&TempTabPrefix&bio$deactivated$samples))) then sr.analytearea else null end) over (partition by r.studyid, sa.id, r.id, s.samplename) meanuseresponse,
            case
              when s.runcode || '-' || s.runsamplesequencenumber || '-' || sa.analyteorder in (select code from &&TempTabPrefix&bio$deactivated$samples) then 'T'
              else 'F'
            end isdeactivated
          FROM 
            &&TempTabPrefix&bio$run r,
            &&TempTabPrefix&bio$run$analytes ra,
            &&TempTabPrefix&bio$study$analytes sa,
            &&TempTabPrefix&bio$assay a,
            &&TempTabPrefix&bio$run$worklist s,
            &&TempTabPrefix&bio$run$worklist$result$rw sr,
            isr$crit kwz--,
            --table(ISR$COVIB$BIOANALYTICS$WT$LAL.GetRunStates) rs
          WHERE r.ID = s.runID
          AND s.ID = sr.worklistID
          AND a.ID = r.assayID
          AND r.studyID = 'S1' AND a.studyID = 'S1'
          --AND rs.column_value = r.runstatusnum
          AND sr.runAnalyteCode = ra.code
          AND r.ID = ra.runID
          AND sr.runanalyteCode = ra.Code
          AND ra.analyteID = sa.ID
          AND kwz.key = s.samplename AND kwz.entity = '&&KnownEntity&' AND kwz.masterkey = '&&ExpMasterkey&' AND kwz.repid = &&RepID&
          --and s.runcode || '-' || s.runsamplesequencenumber || '-' || sa.analyteorder in (select code from &&TempTabPrefix&bio$deactivated$samples)
          &&AddConditionNC&
        )
        GROUP BY studyid, analyteid, species, matrix, rid, runid) NC
    WHERE sample1.studyID = NC.studyID(+)
    AND sample1.rID = NC.rID(+)
    AND sample1.runid = NC.runid(+)
    AND sample1.analyteID = NC.analyteID(+)
    AND sample1.species = NC.species(+)
    AND sample1.matrix = NC.matrix(+)
    &&AddCondition&
 )
 GROUP BY studyid, analyteid, species, matrix, hours, longtermunits, longtermtime, temperature, rid, runid, nominalconc, name)
GROUP BY studyid, analyteid, species, matrix, hours, longtermunits, longtermtime, temperature
