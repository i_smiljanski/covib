  SELECT XMLELEMENT("experiment", XMLATTRIBUTES('&&Experiment&' AS "name" ,'allqcconc' AS "type"),
  (
  select
  Xmlagg(XMLConcat(
    Xmlagg(XMLELEMENT("calculation", xmlconcat(targ,val,res)) order by analyteorder, species, matrix, concentrationunits, nominalconc),
    XMLELEMENT("statistic", XMLATTRIBUTES(analyteid AS "analyteid", species AS "species", matrix AS "matrix", concentrationunits as "unit"),
      XMLELEMENT("flagpercent", min(flagpercent)),
      XMLELEMENT("min-bias", FormatRounded(min(bias), &&DecPlPercBias&)),
      XMLELEMENT("max-bias", FormatRounded(max(bias), &&DecPlPercBias&)),
      XMLELEMENT("min-biasonmean", FormatRounded(min(biasonmean), &&DecPlPercBias&)),
      XMLELEMENT("max-biasonmean", FormatRounded(max(biasonmean), &&DecPlPercBias&)),
      XMLELEMENT("meancv", FormatRounded(round(case when '&&FinalAccuracy&' = 'T' then avg(cv)
                                                    else avg(round(cv, &&DecPlPercCV&))
                                               end, &&DecPlPercCV&), &&DecPlPercCV&)),
      XMLELEMENT("min-cv", FormatRounded(round(min(cv), &&DecPlPercCV&), &&DecPlPercCV&)),
      XMLELEMENT("max-cv", FormatRounded(round(max(cv), &&DecPlPercCV&), &&DecPlPercCV&)),
      XMLELEMENT("min-isarea-ok", &&PeakRepresentationFunc&(min(min_isarea_ok), &&PeakFigures&)),
      XMLELEMENT("max-isarea-ok", &&PeakRepresentationFunc&(max(max_isarea_ok), &&PeakFigures&)),
      XMLELEMENT("numacc", sum(cntunflagged)),
      XMLELEMENT("numtotal", sum(cnt))
    )) order by analyteorder, species, matrix, concentrationunits)
    from (
    SELECT analyteid, analyteorder, species, matrix, concentrationunits,
      Count(useconc) cnt,
      count(unflagged) cntunflagged,
      min(flagpercent) flagpercent,
      round(Avg(usebias), &&DecPlPercBias&) bias,
      nominalconc,
      case when '&&FinalAccuracy&' = 'T' then Stddev(useconc)/Avg(useconc)*100
           else &&ConcRepresentationFunc&(Stddev(useconc), &&ConcSDFigures&)
               /&&ConcRepresentationFunc&(Avg(useconc), &&ConcFigures&)*100
      end cv,
      round(case when nominalconc = 0 then null
                      when '&&FinalAccuracy&' = 'T' then (Avg(useconc)-nominalconc)/nominalconc*100
                      else (&&ConcRepresentationFunc&(Avg(useconc), &&ConcFigures&)-nominalconc_sigrnd)/nominalconc_sigrnd*100
             end, &&DecPlPercBias&) biasonmean,
      min( case when nominalconc = 0 then null
           else
             case when round(case when '&&FinalAccuracy&' = 'T' then (Avg(useconc)-nominalconc)/nominalconc*100
                      else (&&ConcRepresentationFunc&(Avg(useconc), &&ConcFigures&)-nominalconc_sigrnd)/nominalconc_sigrnd*100
                      end, &&DecPlPercBias&) > 15 then null
                  else &&PeakRepresentationFunc&(min(INTERNALSTANDARDAREA), &&PeakFigures&)
      end end ) over (partition by analyteid, analyteorder, species, matrix) as min_isarea_ok,
      max( case when nominalconc = 0 then null
           else
             case when round(case when '&&FinalAccuracy&' = 'T' then (Avg(useconc)-nominalconc)/nominalconc*100
                      else (&&ConcRepresentationFunc&(Avg(useconc), &&ConcFigures&)-nominalconc_sigrnd)/nominalconc_sigrnd*100
                      end, &&DecPlPercBias&) > 15 then null
                  else &&PeakRepresentationFunc&(max(INTERNALSTANDARDAREA), &&PeakFigures&)
      end end ) over (partition by analyteid, analyteorder, species, matrix) as max_isarea_ok,
      XMLELEMENT("target",
        XMLELEMENT("analyteid", analyteid),
        XMLELEMENT("species", species),
        XMLELEMENT("matrix", matrix),
        XMLELEMENT("nominalconc", &&ConcFormatFunc&(&&ConcRepresentationFunc&(nominalconc_sigrnd, &&ConcFigures&), &&ConcFigures&)),
	XMLELEMENT("unit", concentrationunits)
        ) targ,
      XMLELEMENT("values", Xmlagg(
        XMLELEMENT("sample", XMLATTRIBUTES(sampleid AS "sampleid", sampleresultid AS "sampleresultid" ),
          XMLELEMENT("runid", runid),
          XMLELEMENT("concentration", &&ConcFormatFunc&(&&ConcRepresentationFunc&(conc, &&ConcFigures&), &&ConcFigures&)),
          XMLELEMENT("nominalconc", &&ConcFormatFunc&(&&ConcRepresentationFunc&(nominalconc_sigrnd, &&ConcFigures&), &&ConcFigures&)),
          XMLELEMENT("name", NAME),
          XMLELEMENT("runstartdate", to_char(runstartdate,STB$UTIL.GetSystemParameter('DATEFORMAT'))),
          XMLELEMENT("bias", FormatRounded(round(bias, &&DecPlPercBias&), &&DecPlPercBias&)),
          XMLELEMENT("flag", flagged),
          XMLELEMENT("runsamplesequencenumber", runsamplesequencenumber),
          XMLELEMENT("internalstandardarea", &&PeakRepresentationFunc&(INTERNALSTANDARDAREA, &&PeakFigures&)),
          XMLELEMENT("deactivated", isdeactivated)) order by runstartdate, runsamplesequencenumber)
          ) val,
      XMLELEMENT("result",
        XMLELEMENT("n", Count(useconc)),
        XMLELEMENT("mean", &&ConcFormatFunc&(&&ConcRepresentationFunc&(Avg(useconc), &&ConcFigures&), &&ConcFigures&)),
        XMLELEMENT("sd", &&ConcFormatFunc&(&&ConcRepresentationFunc&(Stddev(useconc), &&ConcSDFigures&), &&ConcSDFigures&)),
        XMLELEMENT("cv", FormatRounded(round(case when '&&FinalAccuracy&' = 'T' then Stddev(useconc)/Avg(useconc)*100
                                                  else &&ConcRepresentationFunc&(Stddev(useconc), &&ConcFigures&)
                                                      /&&ConcRepresentationFunc&(Avg(useconc), &&ConcFigures&)*100
                                             end, &&DecPlPercCV&), &&DecPlPercCV&)),
        XMLELEMENT("bias", FormatRounded(round(Avg(usebias), &&DecPlPercBias&), &&DecPlPercBias&)),
        XMLELEMENT("biasonmean", FormatRounded(round(case when nominalconc = 0 then null
                                                    when '&&FinalAccuracy&' = 'T' then (Avg(useconc)-nominalconc)/nominalconc*100
                                                    else (&&ConcRepresentationFunc&(Avg(useconc), &&ConcFigures&)-nominalconc_sigrnd)/nominalconc_sigrnd*100
                                               end, &&DecPlPercBias&), &&DecPlPercBias&)),
        XMLELEMENT("flagpercent", min(flagpercent))
        ) res
    FROM
    (SELECT -- knownID,
     sampleID, sampleresultID, Name, nominalconc, nominalconc_sigrnd, runid, analyteid, analyteorder, species, matrix, concentrationunits, replicatenumber,
     conc, bias, flagpercent, runstartdate,
     ISDEACTIVATED, runsamplesequencenumber, temperature,
     case when ISDEACTIVATED = 'T' then null else conc end useconc,
     case when ISDEACTIVATED = 'T' then null else bias end usebias,
     case when abs(bias) > flagpercent then '&&FlaggedSymbol&' else null end flagged,
     case when ISDEACTIVATED <> 'T' and abs(bias) <= flagpercent then 1 else null end unflagged,
     INTERNALSTANDARDAREA
     FROM
     (SELECT -- K.ID knownID,
        s.ID sampleID, sr.ID sampleresultID, S.KNOWNNAME NAME,--K.NAME,
        ISDEACTIVATED, s.temperature,
        sr.nominalconcentrationconv nominalconc,
        &&ConcRepresentationFunc&(sr.nominalconcentrationconv, &&ConcFigures&) nominalconc_sigrnd,
        s.runid, s.runsamplesequencenumber, r.runstartdate,
        ra.analyteid, sa.analyteorder, a.species, a.sampletypeid matrix, r.runtypedescription, s.replicatenumber,
  15 flagpercent,--k.flagpercent,
        ra.concentrationunits,
        case when '&&FinalAccuracy&' = 'T' then sr.concentrationconv
             else &&ConcRepresentationFunc&(sr.concentrationconv, &&ConcFigures&)
        end conc,
        case when sr.nominalconcentrationconv = 0 then null
             when '&&FinalAccuracy&' = 'T' then (sr.concentrationconv-sr.nominalconcentrationconv)*100/sr.nominalconcentrationconv
             else round((&&ConcRepresentationFunc&(sr.concentrationconv, &&ConcFigures&)-
                         &&ConcRepresentationFunc&(sr.nominalconcentrationconv, &&ConcFigures&))*100
                        /&&ConcRepresentationFunc&(sr.nominalconcentrationconv, &&ConcFigures&), &&DecPlPercBias&)
        end bias,
        srw.INTERNALSTANDARDAREA
      FROM --&&TempTabPrefix&bio$run$ana$known K,
           &&TempTabPrefix&bio$run r,
           &&TempTabPrefix&bio$run$analytes ra,
           &&TempTabPrefix&bio$study$analytes sa,
           &&TempTabPrefix&bio$assay a,
           &&TempTabPrefix&bio$run$samples s,
           &&TempTabPrefix&bio$run$sample$results sr,
           &&TempTabPrefix&bio$RUN$SAMPLE$RESULT$RAW srw
           --isr$crit kwz,
           --table(&&LALPackage&.GetRunStates) rs
      WHERE --K.ID = s.runknownID
        --AND
        R.ID = s.runid AND R.ID = sr.runid
        --AND r.studyassayid =s.studyassayid AND s.studyassayid = sr.studyassayid
        AND r.RuntypeDescription in (&&RunTypes&)
        AND s.ID = sr.runsampleID
        AND s.sampletype = 'known'
        --and k.knowntype = s.runsamplekind
        AND ra.id = sr.runanalyteid
        AND sa.id = ra.analyteid
        AND a.id = r.assayid
        AND ra.runid = s.runid
        AND ra.studyID = '&&StudyID&'
        --AND K.studyid = '&&StudyID&'
        AND s.studyid = '&&StudyID&'
        AND sr.studyID = '&&StudyID&' AND r.studyid = '&&StudyID&'
        and srw.RUNSAMPLECODE = s.code
        AND r.ID = srw.runid
        --AND s.studyassayid = srw.studyassayid
        AND ra.id = srw.runanalyteid
        AND srw.studyID = '&&StudyID&'
        AND regexp_like(S.KNOWNNAME,'&&IncludedKnowns&')
        --and kwz.key = &&KnownNameJoinField&
        --and kwz.entity = '&&KnownEntity&'
        --and kwz.masterkey = '&&ExpMasterkey&'
        --and repid = &&RepID&
        --and k.originalknowntype = 'QC'
        --AND rs.column_value = r.runstatusnum
        &&AddCondition&
     ))
     GROUP BY analyteid, analyteorder, species, matrix, concentrationunits, nominalconc, nominalconc_sigrnd)
     GROUP BY analyteid, analyteorder, species, matrix, concentrationunits
     )
    ) FROM dual