SELECT XMLELEMENT("experiment", XMLATTRIBUTES('&&Experiment&' AS "name" ,'screen_response' AS "type"),
      Xmlagg ( XMLConcat(
        Xmlagg (XMLELEMENT("calculation", xmlconcat(targ,val,res)) order by analyteid, dosegroupno, dosegroup, designsubjecttag, subjectid, samplingtime, doseamount, doseunit, customid),
        XMLELEMENT("statistic", XMLATTRIBUTES(analyteid AS "analyteid"),
               XMLELEMENT("n", count(analyteid))
             )) order by studyid, analyteid
      ))
FROM (
  SELECT studyid, analyteid, dosegroupno, dosegroup, designsubjecttag, subjectid, samplingtime, doseamount, doseunit, customid,
    XMLELEMENT("target",
      XMLELEMENT("dosegroup", dosegroup),
      XMLELEMENT("subjectid", subjectid),
      XMLELEMENT("designsubjecttag", designsubjecttag),      
      XMLELEMENT("analyteid", analyteid),
      XMLELEMENT("doseamount", FmtNum(doseamount)),
      XMLELEMENT("doseunit", doseunit),
      XMLELEMENT("samplingtime", samplingtime),
      XMLELEMENT("customid", customid)
    ) as targ,
    XMLELEMENT("values", 
      Xmlagg(
        XMLELEMENT("sample",
          XMLATTRIBUTES(sampleid AS "sampleid", sampleresultid AS "sampleresultid" ),
          XMLELEMENT("runid", rid),
          XMLELEMENT("runno", runid),
          XMLELEMENT("designsampleid", designSampleID),
          XMLELEMENT("runsamplesequencenumber", runsamplesequencenumber),
          XMLELEMENT("samplesubtype", samplesubtype),
          /*XMLELEMENT("ncmeanmedian",
          case
            when '&&ResponseFullPrecision&' = 'T' then
              FmtNum(nc_meanmedian_response)
            when '&&FinalAccuracy&' = 'T' then
              &&RepresentationFuncResponse&(nc_meanmedian_response, &&PlacesResponse&), &&PlacesResponse&)
            else
              &&RepresentationFuncResponse&(nc_meanmedian_response_sig, &&PlacesResponse&), &&PlacesResponse&)
          end),
          XMLELEMENT("cutpoint",
          case
            when '&&ResponseFullPrecision&' = 'T' then
              FmtNum(cutpoint)
            when '&&FinalAccuracy&' = 'T' then
              &&RepresentationFuncResponse&(cutpoint, &&PlacesResponse&), &&PlacesResponse&)
            else
              &&RepresentationFuncResponse&(cutpoint_sig, &&PlacesResponse&), &&PlacesResponse&)
          end
          ),*/
          XMLELEMENT("response", &&FormatFuncResponse&(&&RepresentationFuncResponse&(response, &&PlacesResponse&), &&PlacesResponse&)),
          XMLELEMENT("response_sig", &&FormatFuncResponse&(&&RepresentationFuncResponse&(response_sig, &&PlacesResponse&), &&PlacesResponse&)),
          /*XMLELEMENT("ratio", FormatRounded(round(
                            case
                              when '&&ResponseFullPrecision&' = 'T' or '&&FinalAccuracy&' = 'T' then 
                                Avg(useresponse)/cutpoint
                              else
                                sigFigure(Avg(useresponse_sig), &&PlacesResponse&)/cutpoint_sig
                            end, &&DecPlNCdivCP&), &&DecPlNCdivCP&)),*/
          XMLELEMENT("status", resulttext),
          XMLELEMENT("comment", resultcomment),
          XMLELEMENT("time", time),
          XMLELEMENT("dilution", dilution),
          XMLELEMENT("deactivated", isdeactivated)
        ) order by runid, runsamplesequencenumber
      )
    ) as val,
    XMLELEMENT("result",
      XMLELEMENT("m", Count(response)),
      XMLELEMENT("n", Count(useresponse)),
      XMLELEMENT("result", max(result)),
      XMLELEMENT("ncmeanmedian",
        case
          when '&&ResponseFullPrecision&' = 'T' then
            FmtNum(nc_meanmedian_response)
          when '&&FinalAccuracy&' = 'T' then
            &&RepresentationFuncResponse&(nc_meanmedian_response, &&PlacesResponse&), &&PlacesResponse&)
          else
            &&RepresentationFuncResponse&(nc_meanmedian_response_sig, &&PlacesResponse&), &&PlacesResponse&)
        end),
      XMLELEMENT("cutpoint",
        case
          when '&&ResponseFullPrecision&' = 'T' then
            FmtNum(cutpoint)
          when '&&FinalAccuracy&' = 'T' then
            &&RepresentationFuncResponse&(cutpoint, &&PlacesResponse&), &&PlacesResponse&)
          else
            &&RepresentationFuncResponse&(cutpoint_sig, &&PlacesResponse&), &&PlacesResponse&)
        end),
      XMLELEMENT("mean",
        case
          when '&&ResponseFullPrecision&' = 'T' then
            FmtNum(Avg(useresponse))
          when '&&FinalAccuracy&' = 'T' then
            &&RepresentationFuncResponse&(Avg(useresponse), &&PlacesResponse&), &&PlacesResponse&)
          else
            &&RepresentationFuncResponse&(Avg(useresponse_sig), &&PlacesResponse&), &&PlacesResponse&)
        end),
      XMLELEMENT("sd", &&FormatFuncResponseSD&(&&RepresentationFuncResponseSD&(case when '&&ResponseFullPrecision&' = 'T' or '&&FinalAccuracy&' = 'T' then Stddev(useresponse) else Stddev(useresponse_sig) end, &&PlacesResponseSD&), &&PlacesResponseSD&)),
      XMLELEMENT("cv", &&FormatFuncPercentage&(&&RepresentationFuncPercentage&(
                        case
                          when &&RepresentationFuncResponse&(Avg(useresponse), &&PlacesResponse&) = 0 then null
                          when '&&ResponseFullPrecision&' = 'T' or '&&FinalAccuracy&' = 'T' then 
                            &&RepresentationFuncResponseSD&(Stddev(useresponse), &&PlacesResponseSD&)/&&RepresentationFuncResponse&(Avg(useresponse), &&PlacesResponse&)*100
                          else
                            &&RepresentationFuncResponseSD&(Stddev(useresponse_sig), &&PlacesResponseSD&)/&&RepresentationFuncResponse&(Avg(useresponse_sig), &&PlacesResponse&)*100
                        end, &&PlacesPercentage&), &&PlacesPercentage&)),
      XMLELEMENT("ratio", &&FormatFuncRatio&(&&RepresentationFuncRatio&(
                            case
                              when '&&ResponseFullPrecision&' = 'T' or '&&FinalAccuracy&' = 'T' then 
                                Avg(useresponse)/cutpoint
                              else
                                &&RepresentationFuncResponse&(vg(useresponse_sig), &&PlacesResponse&)/cutpoint_sig
                            end, &&PlacesRatio&), &&PlacesRatio&)),
      XMLELEMENT("signal-to-noise",  &&FormatFuncRatio&(&&RepresentationFuncRatio&(
                                      case
                                        when nc_meanmedian_response = 0 then null
                                        when '&&ResponseFullPrecision&' = 'T' or '&&FinalAccuracy&' = 'T' then 
                                          Avg(useresponse)/nc_meanmedian_response
                                        else
                                          &&RepresentationFuncResponse&(Avg(useresponse_sig), &&PlacesResponse&)/&&RepresentationFuncResponse&(nc_meanmedian_response_sig, &&PlacesResponse&)
                                      end, &&PlacesRatio&), &&PlacesRatio&)),
      XMLELEMENT("flag",
                case
                  when (&&RepresentationFuncPercentage&(&&RepresentationFuncResponseSD&(Stddev(useresponse), &&PlacesResponseSD&)/&&RepresentationFuncResponse&(Avg(useresponse), &&PlacesResponse&)*100, &&PlacesPercentage&)) > 20 then 'T'
                  else 'F'
                end
      )
    ) as res
  FROM (
    SELECT
      nc.studyid, nc.rid, nc.runid, nc.analyteid, nc.analyteorder,
      -- NC:
      nc.cutpointlimit, nc.meanormedian, nc.meanmedian_response nc_meanmedian_response, nc.meanmedian_response_sig nc_meanmedian_response_sig,
      nc.concunit nc_concunit, nc_cv, nc_n, nc_m,-- nc_xml,
      nc.meanmedian_response * nc.cutpointfactor cutpoint,
      nc.meanmedian_response_sig * nc.cutpointfactor cutpoint_sig,
      -- Screens:
      designsubjecttag, subjectid, designSampleID, samplingtime, time, timetext, studyday, dosegroupno, dosegroup, doseamount, doseunit, customid,
      posgroup, sampleid, sampleresultid, samplesubtype, runsamplesequencenumber,
      response, &&RepresentationFuncResponse&(response, &&PlacesResponse&) response_sig,
      case when (isdeactivated='F') then response else null end useresponse, case when (isdeactivated='F') then &&RepresentationFuncResponse&(response, &&PlacesResponse&) else null end useresponse_sig,
      resulttext, result, source, concentrationunit, resultcomment, dilution,
      &&RepresentationFuncResponse&(Avg(&&RepresentationFuncResponse&(case when(isdeactivated='F') then response else null end, &&PlacesResponse&)) over (partition by nc.studyid, nc.analyteid, dosegroup, subjectid, designSampleID), &&PlacesResponse&) mean_response_sig,
      isdeactivated,
      count(response) over (partition by nc.studyid, nc.analyteid) m,
      count(case when (isdeactivated='F') then response else null end) over (partition by nc.studyid, nc.analyteid) n,
      count(case when lower(result) like 'positive%' then response else null end) over (partition by nc.studyid, nc.analyteid) posm,
      cutpointfactor
    FROM (
      SELECT 
          studyid,
          rid,
          runid,
          analyteid,
          analyteorder,
          concentrationunit concunit,
          cutpointlimit,
          cutpointfactor,
          meanormedian,
          case 
            when meanormedian = 'Mean' then &&RepresentationFuncResponse&(Avg(useresponse), &&PlacesResponse&)
            when meanormedian = 'Median' then &&RepresentationFuncResponse&(Median(useresponse), &&PlacesResponse&)
            else &&RepresentationFuncResponse&(Avg(useresponse), &&PlacesResponse&)
          end meanmedian_response,
          case 
            when meanormedian = 'Mean' then &&RepresentationFuncResponse&(Avg(&&RepresentationFuncResponse&(useresponse, &&PlacesResponse&)), &&PlacesResponse&)
            when meanormedian = 'Median' then &&RepresentationFuncResponse&(Median(&&RepresentationFuncResponse&(useresponse, &&PlacesResponse&)), &&PlacesResponse&)
            else &&RepresentationFuncResponse&(Avg(&&RepresentationFuncResponse&(useresponse, &&PlacesResponse&)), &&PlacesResponse&)
          end meanmedian_response_sig,
          &&RepresentationFuncResponseSD&(Stddev(useresponse), &&PlacesResponseSD&)/&&RepresentationFuncResponse&(Avg(useresponse), &&PlacesResponse&)*100 nc_cv,
          count(useresponse) nc_n,
          count(response) nc_m/*,
          Xmlagg(
              XMLELEMENT("nc",
                XMLATTRIBUTES(sampleid AS "sampleid", sampleresultid AS "sampleresultid" ),
                XMLELEMENT("name", name),
                XMLELEMENT("samplesubtype", samplesubtype),
                XMLELEMENT("response", FormatSig(&&RepresentationFuncResponse&(response, &&PlacesResponse&), &&PlacesResponse&)),
                XMLELEMENT("response_sig", FormatSig(&&RepresentationFuncResponse&(response, &&PlacesResponse&), &&PlacesResponse&)),
                XMLELEMENT("status", resulttext),
                XMLELEMENT("deactivated", isdeactivated)
              ) order by runsamplesequencenumber
            ) nc_xml*/
          FROM(
            SELECT
              r.studyid,
              r.id rid,
              r.runid,
              sa.id analyteid,
              sa.analyteorder,
              s.id sampleid,
              sr.id sampleresultid,
              s.samplename name,
              s.samplesubtype,
              s.runsamplesequencenumber,
              sr.concentrationunits concentrationunit,
              sr.resulttext,
              sr.analytearea response,
              a.meanormedian,
              a.cutpointfactor,
              a.cutpointlimit,
              case when(ds.code is null) then sr.analytearea else null end useresponse,
              case
                when ds.code is not null then 'T'
                else 'F'
              end isdeactivated
            FROM 
              &&TempTabPrefix&bio$run r,
              &&TempTabPrefix&bio$run$analytes ra,
              &&TempTabPrefix&bio$study$analytes sa,
              &&TempTabPrefix&bio$assay a,
              &&TempTabPrefix&bio$run$worklist s,
              &&TempTabPrefix&bio$run$worklist$result$rw sr,
              &&TempTabPrefix&bio$deactivated$samples ds,
              table(&&LALPackage&.GetRunStates) rs
            WHERE r.ID = s.runID
            AND s.ID = sr.worklistID
            AND a.ID = r.assayID
            --AND s.treatmentid = 'ADA'
            --AND s.studyid = '&&StudyID&' AND sr.studyID = '&&StudyID&'
            AND r.studyid = '&&StudyID&'
            AND a.studyID = '&&StudyID&'
            AND rs.column_value = r.runstatusnum
            AND sr.runAnalyteCode = ra.code
            AND r.ID = ra.runID
            AND ra.analyteID = sa.ID
            AND s.runcode || '-' || s.runsamplesequencenumber || '-' || sa.analyteorder = ds.code (+)
            AND (s.runsamplekind like '&&NCKnownType&')
            &&AddConditionNC&
          )
        GROUP BY studyID, rid, runid, analyteid, analyteorder, cutpointlimit, cutpointfactor, meanormedian, concentrationunit
    ) nc, (
          SELECT
            r.studyid,
            s.id sampleid,
            sr.id sampleresultid,
            sam.subjectid,
            sam.designsubjecttag,
            s.designsampleid,
            r.id rid,
            r.runid,
            sa.id analyteid,
            s.samplesubtype,
            s.runsamplesequencenumber,
            sr.analytearea response,
            s.status,
            sr.commenttext,
            sr.resulttext,
            sr.resultcommenttext resultcomment,
            max(sr.resulttext) over (partition by s.designsampleid, r.id) result,
            s.source,
            sr.concentrationunits concentrationunit,
            s.time,
            s.timetext,
            s.studyday,
            t.name dosegroup,
            s.dilution,
            max(rh.reported) over (partition by s.designsampleid, r.id) reported,
            s.customid,
            case
                /*when REGEXP_LIKE(sg.name, '^[+-]?\d?\.?\d+$','i') then
                    to_number(sg.name)
    */
                when REGEXP_LIKE(t.name, '^[+-]?\d?\.?\d+$','i') then
                    to_number(t.name)
                when REGEXP_LIKE(t.description, '^[+-]?\d?\.?\d+$','i') then
                    to_number(t.description)
                else
                  null
            end dosegroupno,
            t.doseamount,
            t.doseunitsdescription doseunit,
            sam.samplingtime,
            s.samplesubtype posgroup,
            case
              when s.runcode || '-' || s.runsamplesequencenumber || '-' || sa.analyteorder in (select code from &&TempTabPrefix&bio$deactivated$samples) then 'T'
              else 'F'
            end isdeactivated
          FROM 
            &&TempTabPrefix&bio$run r,
            &&TempTabPrefix&bio$run$analytes ra,
            &&TempTabPrefix&bio$study$analytes sa,
            &&TempTabPrefix&bio$assay a,
            &&TempTabPrefix&bio$run$worklist s,
            &&TempTabPrefix&bio$run$worklist$result$rw sr,
            &&TempTabPrefix&bio$sample sam,
            &&TempTabPrefix&bio$study$treatment t,
            &&TempTabPrefix&bio$reassay$history rh,
            table(&&LALPackage&.GetRunStates) rs
          WHERE r.ID = s.runID
          AND s.ID = sr.worklistID
          AND a.ID = r.assayID
          AND s.designSampleID = sam.ID
          AND r.studyid = '&&StudyID&'
          AND a.studyID = '&&StudyID&'
          AND rs.column_value = r.runstatusnum
          AND sr.runAnalyteCode = ra.code
          AND r.ID = ra.runID
          AND ra.analyteID = sa.ID
          AND sam.treatmentcode = t.code
          AND s.runsamplekind = 'UNKNOWN'
          AND s.samplesubtype = 'Screen'
          --s.runcode || '-' || s.runsamplesequencenumber || '-' || sa.analyteorder in (select code from &&TempTabPrefix&bio$deactivated$samples)
          AND r.code || '-' || s.runsamplesequencenumber || '-' || sa.analyteorder = rh.runsamplecode (+)
          --&&AddConditionS1&
    ) sample
    WHERE sample.studyID = nc.studyID
    AND sample.rID = nc.rID
    AND sample.runid = nc.runid
    AND sample.analyteID = nc.analyteID
    &&AddCondition&
  ) 
  --&&OverallCondition&
  GROUP BY studyid, analyteid, dosegroupno, dosegroup, designsubjecttag, subjectid, samplingtime, doseamount, doseunit, customid, nc_meanmedian_response, nc_meanmedian_response_sig, cutpoint, cutpoint_sig)
GROUP BY studyid, analyteid
