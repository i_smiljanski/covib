  SELECT XMLELEMENT("experiment", XMLATTRIBUTES('&&Experiment&' AS "name" ,'ecl' AS "type"),
            Xmlagg (
             Xmlagg (XMLELEMENT("calculation", xmlconcat(targ,val,res)) order by runid, rid)
         order by studyid))
  FROM
   (SELECT studyid, runid, rid,
      XMLELEMENT("target",
        XMLELEMENT("runid", rid),
        XMLELEMENT("runno", runid)
      ) as targ,
      XMLELEMENT("values", Xmlagg(
        XMLELEMENT("sample", XMLATTRIBUTES(sampleid AS "sampleid", sampleresultid AS "sampleresultid" ),
          XMLELEMENT("runsamplesequencenumber", runsamplesequencenumber),
          XMLELEMENT("samplesubtype", samplesubtype),
          XMLELEMENT("ecl", FormatRounded(ecl, &&ConcFigures&)),
          XMLELEMENT("cutpointfactor", FormatRounded(cutpointfactor, &&ConcFigures&)),
          XMLELEMENT("status", status),
          XMLELEMENT("commenttext", commenttext),
          XMLELEMENT("concentrationstatus", concentrationstatus)
        )
          order by runid, runsamplesequencenumber
      )) as val,
      XMLELEMENT("result",
        XMLELEMENT("n", Count(ecl)),
        XMLELEMENT("mean", FormatRounded(Avg(ecl), &&ConcFigures&)),
        XMLELEMENT("sd", &&ConcFormatFunc&(&&ConcRepresentationFunc&(Stddev(ecl), &&ConcSDFigures&), &&ConcSDFigures&)),
        XMLELEMENT("cv", FormatRounded(round(Stddev(ecl)/Avg(ecl)*100, &&DecPlPercCV&), &&DecPlPercCV&)),
        XMLELEMENT("cvflag", 
          case
            when (round(Stddev(ecl)/Avg(ecl)*100, &&DecPlPercCV&)) > 20 then 'T'
            else 'F'
          end
        ),
        XMLELEMENT("upperlimit", 
          case
            when (Avg(ecl) * cutpointfactor) > cutpointlimit then 'T'
            else 'F'
          end
        ),
        XMLELEMENT("cutpointonmean", FormatRounded(round(Avg(ecl) * cutpointfactor, &&ConcFigures&), &&ConcFigures&)),
        XMLELEMENT("cutpointlimit", FormatRounded(cutpointlimit, &&ConcFigures&))
      ) as res
    FROM
    (SELECT studyid, sampleid, sampleresultid, rid, runid, samplesubtype, runsamplesequencenumber, ecl, cutpointlimit, cutpointfactor, status, commenttext, concentrationstatus
     FROM
     (SELECT
        r.studyid,
     	s.id sampleid,
     	sr.id sampleresultid,
     	r.id rid,
     	r.runid,
     	s.samplesubtype,
     	s.runsamplesequencenumber,
     	sr.analytearea ecl,
     	a.cutpointlimit,
     	a.cutpointfactor,
     	s.status,
     	sr.commenttext,
     	sr.concentrationstatus
      FROM 
           &&TempTabPrefix&bio$allrun r,
           &&TempTabPrefix&bio$assay a,
           &&TempTabPrefix&bio$run$worklist s,
           &&TempTabPrefix&bio$run$worklist$result$rw sr,
           table(&&LALPackage&.GetRunStates) rs
      WHERE r.ID = s.runID
        AND s.ID = sr.worklistID
        AND a.ID = r.assayID
        --AND s.treatmentid = 'ADA'
        --AND s.studyid = '&&StudyID&' AND sr.studyID = '&&StudyID&'
        AND r.studyid = '&&StudyID&'
        AND a.studyID = '&&StudyID&'
        AND rs.column_value = r.runstatusnum
        &&AddCondition&
     ))
    GROUP BY studyid, runid, rid, cutpointfactor, cutpointlimit)
    GROUP BY studyid, runid, rid
