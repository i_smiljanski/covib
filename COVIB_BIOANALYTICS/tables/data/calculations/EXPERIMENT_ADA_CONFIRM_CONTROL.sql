SELECT
  XMLELEMENT("experiment", XMLATTRIBUTES('&&Experiment&' AS "name" ,'confirm_control' AS "type"),
    Xmlagg(XMLConcat( Xmlagg (XMLELEMENT("calculation", xmlconcat(targ,val,res)) order by acceptedrun, analyteid, levelorder, name, sample_concunit, meanormedian), XMLELEMENT("statistic", XMLATTRIBUTES(studyid AS "studyid"), XMLELEMENT("n", count(studyid))) ) order by studyid)
  )
FROM (
  SELECT studyid, acceptedrun, analyteid, name, sample_concunit, meanormedian,
    to_number(case when name = 'LPC' then 1 when name = 'MPC' then 2 when name = 'HPC' then 3 else 4 end) levelorder,
    XMLELEMENT("target", XMLELEMENT("acceptedrun", acceptedrun), XMLELEMENT("analyteid", analyteid), XMLELEMENT("name", name), XMLELEMENT("sample-concunit", sample_concunit), XMLELEMENT("meanormedian", meanormedian)) as targ,
    XMLELEMENT("values", Xmlagg( XMLELEMENT("sample",
      XMLELEMENT("runid", rid), XMLELEMENT("runno", runid), XMLELEMENT("subject", subject),
      XMLELEMENT("nc-mean-median", &&FormatFuncResponse&(&&RepresentationFuncResponse&(nc_meanmedian_response, &&PlacesResponse&), &&PlacesResponse&)),
      XMLELEMENT("cutpoint", &&FormatFuncResponse&(&&RepresentationFuncResponse&(cutpoint, &&PlacesResponse&), &&PlacesResponse&)), XMLELEMENT("cutpoint-limit", cutpointlimit),
      XMLELEMENT("nc-cv", &&FormatFuncPercentage&(&&RepresentationFuncPercentage&(nc_cv, &&PlacesPercentage&), &&PlacesPercentage&)),
      XMLELEMENT("nc-flagcv", case when nc_cv > cvlimit then 'T' else 'F' end),
      XMLELEMENT("s1_response", &&FormatFuncResponse&(&&RepresentationFuncResponse&(s1_mean_response, &&PlacesResponse&), &&PlacesResponse&)),
      XMLELEMENT("s2_response", &&FormatFuncResponse&(&&RepresentationFuncResponse&(s2_mean_response, &&PlacesResponse&), &&PlacesResponse&)),
      XMLELEMENT("sample1", s1_xml), XMLELEMENT("sample2", s2_xml), XMLELEMENT("s1_comment", s1_resultcomment), XMLELEMENT("s2_comment", s2_resultcomment),
      XMLELEMENT("s1_status", s1_result), XMLELEMENT("s2_status", s2_result), XMLELEMENT("s1_dilution", s1_dilution), XMLELEMENT("s2_dilution", s2_dilution),
      XMLELEMENT("s1_mean", &&FormatFuncResponse&(&&RepresentationFuncResponse&(s1_mean_useresponse, &&PlacesResponse&), &&PlacesResponse&)),
      case when s1_mean_response < llre then XMLELEMENT("s1_flagresponse", XMLATTRIBUTES('LLRe' as "type"), &&FormatFuncResponse&(&&RepresentationFuncResponse&(llre, &&PlacesResponse&), &&PlacesResponse&))
      when s1_mean_response > ulre then XMLELEMENT("s1_flagresponse", XMLATTRIBUTES('ULRe' as "type"), &&FormatFuncResponse&(&&RepresentationFuncResponse&(ulre, &&PlacesResponse&), &&PlacesResponse&)) else null end,
      XMLELEMENT("s2_mean", &&FormatFuncResponse&(&&RepresentationFuncResponse&(s2_mean_useresponse, &&PlacesResponse&), &&PlacesResponse&)),
      /*case when s2_mean_response < llre then XMLELEMENT("s2_flagresponse", XMLATTRIBUTES('LLRe' as "type"), &&FormatFuncResponse&(&&RepresentationFuncResponse&(llre, &&PlacesResponse&), &&PlacesResponse&))
      when s2_mean_response > ulre then XMLELEMENT("s2_flagresponse", XMLATTRIBUTES('ULRe' as "type"), &&FormatFuncResponse&(&&RepresentationFuncResponse&(ulre, &&PlacesResponse&), &&PlacesResponse&)) else null end,
*/
      XMLELEMENT("s1_cv", &&FormatFuncPercentage&(&&RepresentationFuncPercentage&(s1_cv, &&PlacesPercentage&), &&PlacesPercentage&)), XMLELEMENT("s2_cv", &&FormatFuncPercentage&(&&RepresentationFuncPercentage&(s2_cv, &&PlacesPercentage&), &&PlacesPercentage&)),
      XMLELEMENT("s1_ratio", &&FormatFuncRatio&(&&RepresentationFuncRatio&(s1_mean_useresponse/nullif(nc_meanmedian_response,0), &&PlacesRatio&), &&PlacesRatio&)),
      case when s1_mean_useresponse/nullif(nc_meanmedian_response,0) < llra then XMLELEMENT("s1_flagratio", XMLATTRIBUTES('LLRa' as "type"), &&FormatFuncResponse&(&&RepresentationFuncResponse&(llra, &&PlacesResponse&), &&PlacesResponse&))
      when s1_mean_useresponse/nullif(nc_meanmedian_response,0) > ulra then XMLELEMENT("s1_flagratio", XMLATTRIBUTES('ULRa' as "type"), &&FormatFuncResponse&(&&RepresentationFuncResponse&(ulra, &&PlacesResponse&), &&PlacesResponse&)) else null end,
      XMLELEMENT("s2_ratio", &&FormatFuncRatio&(&&RepresentationFuncRatio&(s2_mean_useresponse/nullif(nc_meanmedian_response,0), &&PlacesRatio&), &&PlacesRatio&)),
      /*case when s2_mean_useresponse/nullif(nc_meanmedian_response,0) < llra then XMLELEMENT("s2_flagratio", XMLATTRIBUTES('LLRa' as "type"), &&FormatFuncResponse&(&&RepresentationFuncResponse&(llra, &&PlacesResponse&), &&PlacesResponse&))
      when s2_mean_useresponse/nullif(nc_meanmedian_response,0) > ulra then XMLELEMENT("s2_flagratio", XMLATTRIBUTES('ULRa' as "type"), &&FormatFuncResponse&(&&RepresentationFuncResponse&(ulra, &&PlacesResponse&), &&PlacesResponse&)) else null end,
*/
      XMLELEMENT("difference", &&FormatFuncPercentage&(&&RepresentationFuncPercentage&((s1_mean_useresponse-s2_mean_useresponse)/nullif(s1_mean_useresponse,0)*100, &&PlacesPercentage&), &&PlacesPercentage&)),
      XMLELEMENT("inhibition", &&FormatFuncPercentage&(&&RepresentationFuncPercentage&((s1_mean_useresponse-s2_mean_useresponse)/nullif(s1_mean_useresponse,0)*100, &&PlacesPercentage&), &&PlacesPercentage&)),
      XMLELEMENT("s1_cv_flag", case when s1_cv > cvlimit  then 'T' else 'F' end), XMLELEMENT("s2_cv_flag", case when s2_cv > cvlimit  then 'T' else 'F' end), XMLELEMENT("cvlimit", cvlimit), XMLELEMENT("s2_dilution", s2_dilution),
      XMLELEMENT("run-s1-mean-response", &&FormatFuncResponse&(&&RepresentationFuncResponse&(run_s1_mean_useresponse, &&PlacesResponse&), &&PlacesResponse&)),
      case when run_s1_mean_useresponse < llre then XMLELEMENT("run_s1_flagresponse", XMLATTRIBUTES('LLRe' as "type"), &&FormatFuncResponse&(&&RepresentationFuncResponse&(llre, &&PlacesResponse&), &&PlacesResponse&))
      when run_s1_mean_useresponse > ulre then XMLELEMENT("run_s1_flagresponse", XMLATTRIBUTES('ULRe' as "type"), &&FormatFuncResponse&(&&RepresentationFuncResponse&(ulre, &&PlacesResponse&), &&PlacesResponse&)) else null end,
      XMLELEMENT("run-s1-sd-response", &&FormatFuncResponseSD&(&&RepresentationFuncResponseSD&(run_s1_sd, &&PlacesResponseSD&), &&PlacesResponseSD&)),
      XMLELEMENT("run-s1-cv-response", &&FormatFuncPercentage&(&&RepresentationFuncPercentage&(run_s1_cv, &&PlacesPercentage&), &&PlacesPercentage&)),
      XMLELEMENT("run-s2-mean-response", &&FormatFuncResponse&(&&RepresentationFuncResponse&(run_s2_mean_useresponse, &&PlacesResponse&), &&PlacesResponse&)),
      /*case when run_s2_mean_useresponse < llre then XMLELEMENT("run_s2_flagresponse", XMLATTRIBUTES('LLRe' as "type"), &&FormatFuncResponse&(&&RepresentationFuncResponse&(llre, &&PlacesResponse&), &&PlacesResponse&))
      when run_s2_mean_useresponse > ulre then XMLELEMENT("run_s2_flagresponse", XMLATTRIBUTES('ULRe' as "type"), &&FormatFuncResponse&(&&RepresentationFuncResponse&(ulre, &&PlacesResponse&), &&PlacesResponse&)) else null end,
*/
      XMLELEMENT("run-s2-sd-response", &&FormatFuncResponseSD&(&&RepresentationFuncResponseSD&(run_s2_sd, &&PlacesResponseSD&), &&PlacesResponseSD&)),
      XMLELEMENT("run-s2-cv-response", &&FormatFuncPercentage&(&&RepresentationFuncPercentage&(run_s2_cv, &&PlacesPercentage&), &&PlacesPercentage&)),
      XMLELEMENT("run-s1-mean-ratio", &&FormatFuncRatio&(&&RepresentationFuncRatio&(run_s1_mean_ratio, &&PlacesRatio&), &&PlacesRatio&)),
      case when run_s1_mean_ratio < llra then XMLELEMENT("run_s1_flagratio", XMLATTRIBUTES('LLRa' as "type"), &&FormatFuncResponse&(&&RepresentationFuncResponse&(llra, &&PlacesResponse&), &&PlacesResponse&))
      when run_s1_mean_ratio > ulra then XMLELEMENT("run_s1_flagratio", XMLATTRIBUTES('ULRa' as "type"), &&FormatFuncResponse&(&&RepresentationFuncResponse&(ulra, &&PlacesResponse&), &&PlacesResponse&)) else null end,
      XMLELEMENT("run-s1-sd-ratio", &&FormatFuncRatio&(&&RepresentationFuncRatio&(run_s1_sd_ratio, &&PlacesRatio&), &&PlacesRatio&)),
      XMLELEMENT("run-s1-cv-ratio", &&FormatFuncRatio&(&&RepresentationFuncRatio&(run_s1_cv_ratio, &&PlacesRatio&), &&PlacesRatio&)),
      XMLELEMENT("run-s2-mean-ratio", &&FormatFuncRatio&(&&RepresentationFuncRatio&(run_s2_mean_ratio, &&PlacesRatio&), &&PlacesRatio&)),
      /*case when run_s2_mean_ratio < llra then XMLELEMENT("run_s2_flagratio", XMLATTRIBUTES('LLRa' as "type"), &&FormatFuncResponse&(&&RepresentationFuncResponse&(llra, &&PlacesResponse&), &&PlacesResponse&))
      when run_s2_mean_ratio > ulra then XMLELEMENT("run_s2_flagratio", XMLATTRIBUTES('ULRa' as "type"), &&FormatFuncResponse&(&&RepresentationFuncResponse&(ulra, &&PlacesResponse&), &&PlacesResponse&)) else null end,
*/
      XMLELEMENT("run-s2-sd-ratio", &&FormatFuncRatio&(&&RepresentationFuncRatio&(run_s2_sd_ratio, &&PlacesRatio&), &&PlacesRatio&)),
      XMLELEMENT("run-s2-cv-ratio", &&FormatFuncRatio&(&&RepresentationFuncRatio&(run_s2_cv_ratio, &&PlacesRatio&), &&PlacesRatio&)),
      XMLELEMENT("run--mean-inhibition", &&FormatFuncPercentage&(&&RepresentationFuncPercentage&(run_inhibition_mean, &&PlacesPercentage&), &&PlacesPercentage&)),
      XMLELEMENT("run--sd-inhibition", &&FormatFuncResponseSD&(&&RepresentationFuncResponseSD&(run_inhibition_sd, &&PlacesResponseSD&), &&PlacesResponseSD&)),
      XMLELEMENT("run--cv-inhibition", &&FormatFuncPercentage&(&&RepresentationFuncPercentage&(run_inhibition_cv, &&PlacesPercentage&), &&PlacesPercentage&))
  ) order by runid, subject)) as val,
  XMLELEMENT("result",
    XMLELEMENT("s1_n_response", Count(case when s1_mean_useresponse is not null then '1' else null end)), XMLELEMENT("s1_mean_response", &&FormatFuncResponse&(&&RepresentationFuncResponse&(Avg(s1_mean_useresponse), &&PlacesResponse&), &&PlacesResponse&)),
    case when Avg(s1_mean_useresponse) < llre then XMLELEMENT("s1_flagresponse", XMLATTRIBUTES('LLRe' as "type"), &&FormatFuncResponse&(&&RepresentationFuncResponse&(llre, &&PlacesResponse&), &&PlacesResponse&))
    when Avg(s1_mean_useresponse) > ulre then XMLELEMENT("s1_flagresponse", XMLATTRIBUTES('ULRe' as "type"), &&FormatFuncResponse&(&&RepresentationFuncResponse&(ulre, &&PlacesResponse&), &&PlacesResponse&)) else null end,
    XMLELEMENT("s1_sd_response", &&FormatFuncResponseSD&(&&RepresentationFuncResponseSD&(Stddev(s1_mean_useresponse), &&PlacesResponseSD&), &&PlacesResponseSD&)),
    XMLELEMENT("s1_cv_response", &&FormatFuncPercentage&(&&RepresentationFuncPercentage&(Stddev(s1_mean_useresponse)/nullif(Avg(s1_mean_useresponse), 0)*100, &&PlacesPercentage&), &&PlacesPercentage&)),
    XMLELEMENT("s2_n_response", Count(case when s2_mean_useresponse is not null then '1' else null end)), XMLELEMENT("s2_mean_response", &&FormatFuncResponse&(&&RepresentationFuncResponse&(Avg(s2_mean_useresponse), &&PlacesResponse&), &&PlacesResponse&)),
    /*case when Avg(s2_mean_useresponse) < llre then XMLELEMENT("s2_flagresponse", XMLATTRIBUTES('LLRe' as "type"), &&FormatFuncResponse&(&&RepresentationFuncResponse&(llre, &&PlacesResponse&), &&PlacesResponse&))
    when Avg(s2_mean_useresponse) > ulre then XMLELEMENT("s2_flagresponse", XMLATTRIBUTES('ULRe' as "type"), &&FormatFuncResponse&(&&RepresentationFuncResponse&(ulre, &&PlacesResponse&), &&PlacesResponse&)) else null end,
*/
    XMLELEMENT("s2_sd_response", &&FormatFuncResponseSD&(&&RepresentationFuncResponseSD&(Stddev(s2_mean_useresponse), &&PlacesResponseSD&), &&PlacesResponseSD&)),
    XMLELEMENT("s2_cv_response", &&FormatFuncPercentage&(&&RepresentationFuncPercentage&(Stddev(s2_mean_useresponse)/nullif(Avg(s2_mean_useresponse), 0)*100, &&PlacesPercentage&), &&PlacesPercentage&)),
    XMLELEMENT("s1_n_ratio", Count(case when s1_mean_useresponse/nullif(nc_meanmedian_response,0) is not null then '1' else null end)), XMLELEMENT("s1_mean_ratio", &&FormatFuncRatio&(&&RepresentationFuncRatio&(Avg(s1_mean_useresponse/nullif(nc_meanmedian_response,0)), &&PlacesRatio&), &&PlacesRatio&)),
    case when Avg(s1_mean_useresponse/nullif(nc_meanmedian_response,0)) < llra then XMLELEMENT("s1_flagratio", XMLATTRIBUTES('LLRa' as "type"), &&FormatFuncResponse&(&&RepresentationFuncResponse&(llra, &&PlacesResponse&), &&PlacesResponse&))
    when Avg(s1_mean_useresponse/nullif(nc_meanmedian_response,0)) > ulra then XMLELEMENT("s1_flagratio", XMLATTRIBUTES('ULRa' as "type"), &&FormatFuncResponse&(&&RepresentationFuncResponse&(ulra, &&PlacesResponse&), &&PlacesResponse&)) else null end,
    XMLELEMENT("s1_sd_ratio", &&FormatFuncRatio&(&&RepresentationFuncRatio&(Stddev(s1_mean_useresponse/nullif(nc_meanmedian_response,0)), &&PlacesRatio&), &&PlacesRatio&)),
    XMLELEMENT("s1_cv_ratio", &&FormatFuncRatio&(&&RepresentationFuncRatio&(Stddev(s1_mean_useresponse/nullif(nc_meanmedian_response,0)) / nullif(Avg(s1_mean_useresponse/nullif(nc_meanmedian_response,0)),0)*100, &&PlacesRatio&), &&PlacesRatio&)),
    XMLELEMENT("s2_n_ratio", Count(case when s2_mean_useresponse/nullif(nc_meanmedian_response,0) is not null then '1' else null end)), XMLELEMENT("s2_mean_ratio", &&FormatFuncRatio&(&&RepresentationFuncRatio&(Avg(s2_mean_useresponse/nullif(nc_meanmedian_response,0)), &&PlacesRatio&), &&PlacesRatio&)),
    /*case when Avg(s2_mean_useresponse/nullif(nc_meanmedian_response,0)) < llra then XMLELEMENT("s2_flagratio", XMLATTRIBUTES('LLRa' as "type"), &&FormatFuncResponse&(&&RepresentationFuncResponse&(llra, &&PlacesResponse&), &&PlacesResponse&))
    when Avg(s2_mean_useresponse/nullif(nc_meanmedian_response,0)) > ulra then XMLELEMENT("s2_flagratio", XMLATTRIBUTES('ULRa' as "type"), &&FormatFuncResponse&(&&RepresentationFuncResponse&(ulra, &&PlacesResponse&), &&PlacesResponse&)) else null end,
*/
    XMLELEMENT("s2_sd_ratio", &&FormatFuncRatio&(&&RepresentationFuncRatio&(Stddev(s2_mean_useresponse/nullif(nc_meanmedian_response,0)), &&PlacesRatio&), &&PlacesRatio&)),
    XMLELEMENT("s2_cv_ratio", &&FormatFuncRatio&(&&RepresentationFuncRatio&(Stddev(s2_mean_useresponse/nullif(nc_meanmedian_response,0)) / nullif(Avg(s2_mean_useresponse/nullif(nc_meanmedian_response,0)),0)*100, &&PlacesRatio&), &&PlacesRatio&)),
    XMLELEMENT("_n_inhibition", Count(case when (s1_mean_useresponse-s2_mean_useresponse)/nullif(s1_mean_useresponse,0)*100 is not null then '1' else null end)),
    XMLELEMENT("_mean_inhibition", &&FormatFuncPercentage&(&&RepresentationFuncPercentage&(Avg((s1_mean_useresponse-s2_mean_useresponse)/nullif(s1_mean_useresponse,0)*100), &&PlacesPercentage&), &&PlacesPercentage&)),
    XMLELEMENT("_sd_inhibition", &&FormatFuncResponseSD&(&&RepresentationFuncResponseSD&(Stddev((s1_mean_useresponse-s2_mean_useresponse)/nullif(s1_mean_useresponse,0)*100), &&PlacesResponseSD&), &&PlacesResponseSD&)),
    XMLELEMENT("_cv_inhibition", &&FormatFuncPercentage&(&&RepresentationFuncPercentage&(Stddev((s1_mean_useresponse-s2_mean_useresponse)/nullif(s1_mean_useresponse,0)*100) / nullif(Avg((s1_mean_useresponse-s2_mean_useresponse)/nullif(s1_mean_useresponse,0)*100),0)*100, &&PlacesPercentage&), &&PlacesPercentage&))
  ) as res
  FROM (
    SELECT nc.studyid, nc.rid, nc.runid, nc.acceptedrun, nc.analyteid, nc.analyteorder, sample1.name, sample1.subject, sample1.concentrationunit sample_concunit, sample1.cvlimit,
      /*NC:*/ nc.cutpointlimit, nc.meanormedian, nc.meanmedian_response nc_meanmedian_response, nc.concunit nc_concunit, nc_sd, nc_n, nc_m, nc.cutpoint, nc_sd/nullif(nc.meanmedian_response,0)*100 nc_cv,
      /*--*/ sample1.llre, sample1.ulre, sample1.llra, sample1.ulra,
      /*Sample1:*/ sample1.dilution s1_dilution, sample1.mean_response s1_mean_response, sample1.mean_useresponse s1_mean_useresponse, sample1.sd s1_sd, sample1.resultcomment s1_resultcomment, sample1.m s1_m, sample1.n s1_n, sample1.xml s1_xml, sample1.sd/nullif(sample1.mean_useresponse,0)*100 s1_cv,
      case when sample1.runsamplekind like 'TITERC.NG%' then case when (sample1.mean_useresponse-sample2.mean_useresponse)/nullif(sample1.mean_useresponse,0)*100 >= sample1.titermax then 'Positive Immunodepletion' else 'Negative Immunodepletion' end else sample1.result end s1_result,
      /*Sample2:*/ sample2.dilution s2_dilution, sample2.mean_response s2_mean_response, sample2.mean_useresponse s2_mean_useresponse, sample2.sd s2_sd, sample2.resultcomment s2_resultcomment, sample2.m s2_m, sample2.n s2_n, sample2.xml s2_xml, sample2.sd/nullif(sample2.mean_useresponse,0)*100 s2_cv, sample2.result s2_result,
      -- IntraRunStats
      Avg(nc.meanmedian_response) over (partition by nc.analyteid, sample1.name, sample1.concentrationunit, nc.meanormedian, nc.rid) run_nc_mean_useresponse, Stddev(nc.meanmedian_response) over (partition by nc.analyteid, sample1.name, sample1.concentrationunit, nc.meanormedian, nc.rid) run_nc_sd,
      ( Stddev(nc.meanmedian_response) over (partition by nc.analyteid, sample1.name, sample1.concentrationunit, nc.meanormedian, nc.rid) / nullif(Avg(nc.meanmedian_response) over (partition by nc.analyteid, sample1.name, sample1.concentrationunit, nc.meanormedian, nc.rid),0) * 100 ) run_nc_cv,
      Avg(nc.cutpoint) over (partition by nc.analyteid, sample1.name, sample1.concentrationunit, nc.meanormedian, nc.rid) run_cp_mean_useresponse, Stddev(nc.cutpoint) over (partition by nc.analyteid, sample1.name, sample1.concentrationunit, nc.meanormedian, nc.rid) run_cp_sd,
      ( Stddev(nc.cutpoint) over (partition by nc.analyteid, sample1.name, sample1.concentrationunit, nc.meanormedian, nc.rid) / nullif(Avg(nc.cutpoint) over (partition by nc.analyteid, sample1.name, sample1.concentrationunit, nc.meanormedian, nc.rid),0) * 100 ) run_cp_cv,
      Avg(sample1.mean_useresponse) over (partition by nc.analyteid, sample1.name, sample1.concentrationunit, nc.meanormedian, nc.rid) run_s1_mean_useresponse, Stddev(sample1.mean_useresponse) over (partition by nc.analyteid, sample1.name, sample1.concentrationunit, nc.meanormedian, nc.rid) run_s1_sd,
      ( Stddev(sample1.mean_useresponse) over (partition by nc.analyteid, sample1.name, sample1.concentrationunit, nc.meanormedian, nc.rid) / nullif(Avg(sample1.mean_useresponse) over (partition by nc.analyteid, sample1.name, sample1.concentrationunit, nc.meanormedian, nc.rid),0) * 100 ) run_s1_cv,
      Avg(sample2.mean_useresponse) over (partition by nc.analyteid, sample1.name, sample1.concentrationunit, nc.meanormedian, nc.rid) run_s2_mean_useresponse, Stddev(sample2.mean_useresponse) over (partition by nc.analyteid, sample1.name, sample1.concentrationunit, nc.meanormedian, nc.rid) run_s2_sd,
      ( Stddev(sample2.mean_useresponse) over (partition by nc.analyteid, sample1.name, sample1.concentrationunit, nc.meanormedian, nc.rid) / nullif(Avg(sample2.mean_useresponse) over (partition by nc.analyteid, sample1.name, sample1.concentrationunit, nc.meanormedian, nc.rid),0) * 100 ) run_s2_cv,
      Avg(sample1.mean_useresponse / nullif(nc.meanmedian_response,0)) over (partition by nc.analyteid, sample1.name, sample1.concentrationunit, nc.meanormedian, nc.rid) run_s1_mean_ratio, Stddev(sample1.mean_useresponse / nullif(nc.meanmedian_response,0)) over (partition by nc.analyteid, sample1.name, sample1.concentrationunit, nc.meanormedian, nc.rid) run_s1_sd_ratio,
      ( Stddev(sample1.mean_useresponse / nullif(nc.meanmedian_response,0)) over (partition by nc.analyteid, sample1.name, sample1.concentrationunit, nc.meanormedian, nc.rid) / nullif(Avg(sample1.mean_useresponse / nullif(nc.meanmedian_response,0)) over (partition by nc.analyteid, sample1.name, sample1.concentrationunit, nc.meanormedian, nc.rid),0) * 100 ) run_s1_cv_ratio,
      Avg(sample2.mean_useresponse / nullif(nc.meanmedian_response,0)) over (partition by nc.analyteid, sample1.name, sample1.concentrationunit, nc.meanormedian, nc.rid) run_s2_mean_ratio, Stddev(sample2.mean_useresponse / nullif(nc.meanmedian_response,0)) over (partition by nc.analyteid, sample1.name, sample1.concentrationunit, nc.meanormedian, nc.rid) run_s2_sd_ratio,
      ( Stddev(sample2.mean_useresponse / nullif(nc.meanmedian_response,0)) over (partition by nc.analyteid, sample1.name, sample1.concentrationunit, nc.meanormedian, nc.rid) / nullif(Avg(sample2.mean_useresponse / nullif(nc.meanmedian_response,0)) over (partition by nc.analyteid, sample1.name, sample1.concentrationunit, nc.meanormedian, nc.rid),0) * 100 ) run_s2_cv_ratio,
      Avg((sample1.mean_useresponse-sample2.mean_useresponse)/nullif(sample1.mean_useresponse,0)*100) over (partition by nc.analyteid, sample1.name, sample1.concentrationunit, nc.meanormedian, nc.rid) run_inhibition_mean, Stddev((sample1.mean_useresponse-sample2.mean_useresponse)/nullif(sample1.mean_useresponse,0)*100) over (partition by nc.analyteid, sample1.name, sample1.concentrationunit, nc.meanormedian, nc.rid) run_inhibition_sd,
      ( Stddev((sample1.mean_useresponse-sample2.mean_useresponse)/nullif(sample1.mean_useresponse,0)*100) over (partition by nc.analyteid, sample1.name, sample1.concentrationunit, nc.meanormedian, nc.rid) / nullif(Avg((sample1.mean_useresponse-sample2.mean_useresponse)/nullif(sample1.mean_useresponse,0)*100) over (partition by nc.analyteid, sample1.name, sample1.concentrationunit, nc.meanormedian, nc.rid),0) * 100 ) run_inhibition_cv
    FROM (
      SELECT studyid, rid, runid, acceptedrun, analyteid, analyteorder, concentrationunit concunit, cutpointlimit, meanormedian, count(case when useresponse is not null then 1 end) nc_n, count(case when response is not null then 1 end) nc_m,
          case when meanormedian = 'Mean' then Avg(useresponse) when meanormedian = 'Median' then Median(useresponse) when meanormedian = 'Mean of Means' then Avg(case when replicatenumber = 1 then meanuseresponse else null end) else Avg(useresponse) end meanmedian_response,
          case when min(cutpointmethod) = 3 then min(cutpointoffset) * Stddev(useresponse) else 0
          end + case when min(cutpointmethod) = 1 then min(cutpointoffset) else 0
          end + case when min(cutpointmethod) = 2 then min(cutpointoffset) when meanormedian = 'Mean' then Avg(useresponse) when meanormedian = 'Median' then Median(useresponse) when meanormedian = 'Mean of Means' then Avg(case when replicatenumber = 1 then meanuseresponse else null end) else Avg(useresponse) * min(cutpointfactor)
          end * case when min(cutpointmethod) = 0 then min(cutpointfactor) else 1 end cutpoint,
          case when meanormedian = 'Mean of Means' then Stddev(case when replicatenumber = 1 then meanuseresponse else null end) else Stddev(useresponse) end nc_sd
          FROM(
            SELECT r.studyid, r.id rid, r.runid, sa.id analyteid, sa.analyteorder, s.id sampleid, sr.id sampleresultid, s.samplename name, s.samplesubtype, s.runsamplesequencenumber, s.concunits concentrationunit, sr.resulttext, sr.analytearea response, a.meanormedian, a.cutpointlimit,
              case when ra.RunAnalyteRegressionStatus = &&RejectedRunAnalyte& then 'F' else 'T' end acceptedrun, s.replicatenumber,
              case when '&&ScreenCTRLnFactor&' is not null then to_number('&&ScreenCTRLnFactor&') else a.cutpointfactor end cutpointfactor, a.cutpointoffset,
              case when '&&ScreenCTRLnFactor&' is not null then 0 else a.cutpointmethod end cutpointmethod, case when(ds.code is null) then sr.analytearea else null end useresponse,
              Avg(case when(ds.code is null) then sr.analytearea else null end) over (partition by r.studyid, sa.id, r.id, s.samplename) meanuseresponse, case when ds.code is not null then 'T' else 'F' end isdeactivated
            FROM &&TempTabPrefix&bio$run r, &&TempTabPrefix&bio$run$analytes ra, &&TempTabPrefix&bio$study$analytes sa, &&TempTabPrefix&bio$assay a, &&TempTabPrefix&bio$run$worklist s, &&TempTabPrefix&bio$run$worklist$result$rw sr, &&TempTabPrefix&bio$deactivated$samples ds, table(&&LALPackage&.GetRunStates) rs
            WHERE r.ID = s.runID AND s.ID = sr.worklistID AND a.ID = r.assayID AND r.studyid = '&&StudyID&' AND a.studyID = '&&StudyID&' AND rs.column_value = r.runstatusnum AND sr.runAnalyteCode = ra.code AND r.ID = ra.runID AND ra.analyteID = sa.ID AND s.runcode || '-' || s.runsamplesequencenumber || '-' || sa.analyteorder = ds.code (+) AND (s.runsamplekind like '&&NCKnownType&')
            &&AddConditionNC&
          )
        GROUP BY studyID, rid, runid, acceptedrun, analyteid, analyteorder, cutpointlimit, cutpointfactor, meanormedian, concentrationunit
    ) nc, (
          SELECT studyid, analyteid, rid, runid, name, concentrationunit, subject, max(dilution) dilution, Avg(response) mean_response, max(result) result, max(resultcomment) resultcomment, min(flagpercent) cvlimit,
            min(runsamplekind) runsamplekind, max(titermax) titermax, max(llre) llre, min(ulre) ulre, max(llra) llra, min(ulra) ulra, Avg(case when(isdeactivated='F') then response else null end) mean_useresponse,
            Stddev(case when(isdeactivated='F') then response else null end) sd, count(case when(isdeactivated='F') then 1 else null end) n, count(1) m,
            Xmlagg(XMLELEMENT("sample",
              XMLATTRIBUTES(sampleid AS "sampleid", sampleresultid AS "sampleresultid" ), XMLELEMENT("runsamplesequencenumber", runsamplesequencenumber), XMLELEMENT("samplename", samplename), XMLELEMENT("samplesubtype", samplesubtype),
              XMLELEMENT("response", &&FormatFuncResponse&(&&RepresentationFuncResponse&(response, &&PlacesResponse&), &&PlacesResponse&)), XMLELEMENT("confirm-cp", FormatRounded(round(titermax,1),1)), XMLELEMENT("status", resulttext), XMLELEMENT("deactivated", isdeactivated)
            ) order by runsamplesequencenumber) xml
          FROM (
            SELECT r.studyid, s.id sampleid, s.samplename, sr.id sampleresultid, r.id rid, r.runid, sa.id analyteid, s.samplesubtype, s.runsamplesequencenumber, sr.analytearea response, s.status, sr.commenttext, sr.resulttext,
              sr.resultcommenttext resultcomment, max(sr.resulttext) over (partition by s.designsampleid, s.samplesubtype, r.id) result, s.runsamplekind, s.source, s.concunits concentrationunit, s.dilution, max(rh.reported) over (partition by s.designsampleid, r.id) reported, s.flagpercent,
              llre.key llre, ulre.key ulre, llra.key llra, ulra.key ulra, regexp_replace(s.samplename, '^(.*PC).*$', '\1', 1, 1, 'i') name,
              case when s.runsamplekind like 'TITERC.NG%' then max(titermax) over (partition by r.studyid, sa.id, r.id, r.runid) else titermax end titermax,
              case when regexp_like(s.samplename, '^\D+(\d+).*$', 'i') then to_number(regexp_replace(s.samplename, '^\D+(\d+).*$', '\1', 1, 1, 'i')) else 1 end subject,
              case when s.runcode || '-' || s.runsamplesequencenumber || '-' || sa.analyteorder in (select code from &&TempTabPrefix&bio$deactivated$samples) then 'T' else 'F' end isdeactivated
            FROM &&TempTabPrefix&bio$run r, &&TempTabPrefix&bio$run$analytes ra, &&TempTabPrefix&bio$study$analytes sa, &&TempTabPrefix&bio$assay a,    &&TempTabPrefix&bio$run$worklist s, &&TempTabPrefix&bio$run$worklist$result$rw sr, &&TempTabPrefix&bio$reassay$history rh, table(&&LALPackage&.GetRunStates) rs,
              ISR$CRIT llre, ISR$CRIT ulre, ISR$CRIT llra, ISR$CRIT ulra
            WHERE r.ID = s.runID AND s.ID = sr.worklistID AND a.ID = r.assayID AND r.studyid = '&&StudyID&' AND a.studyID = '&&StudyID&' AND rs.column_value = r.runstatusnum AND sr.runAnalyteCode = ra.code
            AND r.ID = ra.runID AND ra.analyteID = sa.ID AND r.code || '-' || s.runsamplesequencenumber || '-' || sa.analyteorder = rh.runsamplecode (+)
            AND llre.repid(+) = &&RepID& AND llre.entity(+) = 'BIO$COVIB$LO$RESPONSE$'||regexp_replace(s.samplename, '^(.*PC).*$', '\1', 1, 1, 'i') AND llre.masterkey(+) =  sa.code AND ulre.repid(+) = &&RepID& AND ulre.entity(+) = 'BIO$COVIB$HI$RESPONSE$'||regexp_replace(s.samplename, '^(.*PC).*$', '\1', 1, 1, 'i') AND ulre.masterkey(+) =  sa.code
            AND llra.repid(+) = &&RepID& AND llra.entity(+) = 'BIO$COVIB$LO$RATIO$'||regexp_replace(s.samplename, '^(.*PC).*$', '\1', 1, 1, 'i') AND llra.masterkey(+) =  sa.code AND ulra.repid(+) = &&RepID& AND ulra.entity(+) = 'BIO$COVIB$HI$RATIO$'||regexp_replace(s.samplename, '^(.*PC).*$', '\1', 1, 1, 'i') AND ulra.masterkey(+) =  sa.code
            &&AddConditionS1&
          )
          GROUP BY studyid, analyteid, rid, runid, name, concentrationunit, subject
    ) sample1,
    (
          SELECT studyid, analyteid, rid, runid, name, concentrationunit, subject, max(dilution) dilution, Avg(response) mean_response, Avg(case when(isdeactivated='F') then response else null end) mean_useresponse,
            Stddev(case when(isdeactivated='F') then response else null end) sd, count(case when(isdeactivated='F') then 1 else null end) n, count(1) m, max(result) result, max(resultcomment) resultcomment, min(flagpercent) cvlimit,
            Xmlagg(XMLELEMENT("sample",
              XMLATTRIBUTES(sampleid AS "sampleid", sampleresultid AS "sampleresultid" ), XMLELEMENT("runsamplesequencenumber", runsamplesequencenumber), XMLELEMENT("samplename", samplename), XMLELEMENT("samplesubtype", samplesubtype),
              XMLELEMENT("response", &&FormatFuncResponse&(&&RepresentationFuncResponse&(response, &&PlacesResponse&), &&PlacesResponse&)), XMLELEMENT("status", resulttext), XMLELEMENT("deactivated", isdeactivated)
            ) order by runsamplesequencenumber) xml
          FROM (
            SELECT r.studyid, s.id sampleid, s.samplename, sr.id sampleresultid, r.id rid, r.runid, sa.id analyteid, s.samplesubtype, s.runsamplesequencenumber,sr.analytearea response, s.flagpercent,
              s.status, sr.commenttext, sr.resulttext, sr.resultcommenttext resultcomment, max(sr.resulttext) over (partition by s.designsampleid, s.samplesubtype, r.id) result, s.source, s.concunits concentrationunit, s.dilution, max(rh.reported) over (partition by s.designsampleid, r.id) reported,
              regexp_replace(s.samplename, '^(.*PC).*$', '\1', 1, 1, 'i') name, case when regexp_like(s.samplename, '^\D+(\d+).*$', 'i') then to_number(regexp_replace(s.samplename, '^\D+(\d+).*$', '\1', 1, 1, 'i')) else 1 end subject,
              case when s.runcode || '-' || s.runsamplesequencenumber || '-' || sa.analyteorder in (select code from &&TempTabPrefix&bio$deactivated$samples) then 'T' else 'F' end isdeactivated
            FROM &&TempTabPrefix&bio$run r, &&TempTabPrefix&bio$run$analytes ra, &&TempTabPrefix&bio$study$analytes sa, &&TempTabPrefix&bio$assay a, &&TempTabPrefix&bio$run$worklist s, &&TempTabPrefix&bio$run$worklist$result$rw sr, &&TempTabPrefix&bio$reassay$history rh, table(&&LALPackage&.GetRunStates) rs
            WHERE r.ID = s.runID AND s.ID = sr.worklistID AND a.ID = r.assayID AND r.studyid = '&&StudyID&' AND a.studyID = '&&StudyID&' AND rs.column_value = r.runstatusnum AND sr.runAnalyteCode = ra.code
            AND r.ID = ra.runID AND ra.analyteID = sa.ID AND r.code || '-' || s.runsamplesequencenumber || '-' || sa.analyteorder = rh.runsamplecode (+)
            &&AddConditionS2&
          )
          GROUP BY studyid, analyteid, rid, runid, name, concentrationunit, subject
    ) sample2
    WHERE sample1.studyID = nc.studyID AND sample1.rID = nc.rID AND sample1.runid = nc.runid AND sample1.analyteID = nc.analyteID AND sample1.studyID = sample2.studyID AND sample1.rID = sample2.rID AND sample1.runid = sample2.runid
    AND sample1.analyteID = sample2.analyteID AND sample1.name = sample2.name AND sample1.concentrationunit = sample2.concentrationunit AND sample1.subject = sample2.subject
    &&AddCondition&
  ) 
  GROUP BY studyid, acceptedrun, analyteid, name, sample_concunit, meanormedian, llre, ulre, llra, ulra)
GROUP BY studyid
