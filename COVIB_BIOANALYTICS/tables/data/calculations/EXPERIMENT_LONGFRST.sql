  SELECT XMLELEMENT("experiment", XMLATTRIBUTES('&&Experiment&' AS "name" ,'longfrst' AS "type"),
  (
  select
  Xmlagg(XMLConcat(
    Xmlagg(XMLELEMENT("calculation", xmlconcat(targ,val,res)) order by analyteorder, species, matrix, runid, knownorder, nominalconc, temperature, concentrationunits),
    XMLELEMENT("statistic", XMLATTRIBUTES(analyteid AS "analyteid", species AS "species", matrix AS "matrix", runid AS "runid", temperature as "temperature", concentrationunits as "unit"),
      XMLELEMENT("days",min(days)),
      XMLELEMENT("flagpercent", min(flagpercent)),
      XMLELEMENT("min-bias", FormatRounded(min(bias), &&DecPlPercBias&)),
      XMLELEMENT("max-bias", FormatRounded(max(bias), &&DecPlPercBias&)),
      XMLELEMENT("min-biasonmean", FormatRounded(min(biasonmean), &&DecPlPercBias&)),
      XMLELEMENT("max-biasonmean", FormatRounded(max(biasonmean), &&DecPlPercBias&)),
      XMLELEMENT("min-cv", FormatRounded(min(cv), &&DecPlPercCV&)),
      XMLELEMENT("mean-cv", FormatRounded(avg(cv), &&DecPlPercCV&)),
      XMLELEMENT("max-cv", FormatRounded(max(cv), &&DecPlPercCV&)),
      XMLELEMENT("numacc", sum(cntunflagged)),
      XMLELEMENT("numtotal", sum(cnt)),
      XMLELEMENT("percacc", case when sum(cnt) != 0 then FmtNum(round(sum(cntunflagged)/sum(cnt)*100, &&DecPlPercStat&)) else null end),
      XMLELEMENT("longtermtime", min(longtermtime)),
      XMLELEMENT("longtermunits", min(longtermunits))
    )))
    from (
    SELECT analyteid, analyteorder, species, matrix, runid, temperature, nominalconc, knownorder, concentrationunits,
      min(days) days,
      min(longtermtime) longtermtime,
      min(longtermunits) longtermunits,
      Count(useconc) cnt,
      count(unflagged) cntunflagged,
      min(flagpercent) flagpercent,
      round(Avg(usebias), &&DecPlPercBias&) bias,
      round(case when nominalconc = 0 then null
                      when '&&FinalAccuracy&' = 'T' then (Avg(useconc)-nominalconc)/nominalconc*100
                      else (&&ConcRepresentationFunc&(Avg(useconc), &&ConcFigures&)-nominalconc_sigrnd)/nominalconc_sigrnd*100
             end, &&DecPlPercBias&) biasonmean,
      round(case when '&&FinalAccuracy&' = 'T' then Stddev(useconc)/Avg(useconc)*100
                 else &&ConcRepresentationFunc&(Stddev(useconc), &&ConcSDFigures&)/&&ConcRepresentationFunc&(Avg(useconc), &&ConcFigures&)*100
            end , &&DecPlPercCV&) as cv,
      XMLELEMENT("target",
        XMLELEMENT("analyteid", analyteid),
        XMLELEMENT("species", species),
        XMLELEMENT("matrix", matrix),
        XMLELEMENT("name", name),
        XMLELEMENT("nominalconc", &&ConcFormatFunc&(nominalconc_sigrnd, &&ConcFigures&)),
        XMLELEMENT("runid", runid),
        XMLELEMENT("temperature", temperature),
        XMLELEMENT("unit", concentrationunits)
        ) targ,
      XMLELEMENT("values", Xmlagg(
        XMLELEMENT("sample", XMLATTRIBUTES(sampleid AS "sampleid", sampleresultid AS "sampleresultid" ),
          XMLELEMENT("concentration", &&ConcFormatFunc&(&&ConcRepresentationFunc&(conc, &&ConcFigures&), &&ConcFigures&)),
          XMLELEMENT("bias", FormatRounded(round(bias, &&DecPlPercBias&), &&DecPlPercBias&)),
          XMLELEMENT("flag", flagged),
          XMLELEMENT("runsamplesequencenumber", runsamplesequencenumber),
          XMLELEMENT("deactivated", isdeactivated)) order by runsamplesequencenumber)
          ) val,
      XMLELEMENT("result",
        XMLELEMENT("n", Count(useconc)),
        XMLELEMENT("mean", &&ConcFormatFunc&(&&ConcRepresentationFunc&(Avg(useconc), &&ConcFigures&), &&ConcFigures&)),
        XMLELEMENT("sd", &&ConcFormatFunc&(&&ConcRepresentationFunc&(Stddev(useconc), &&ConcSDFigures&), &&ConcSDFigures&)),
        XMLELEMENT("cv", FormatRounded(round(case when '&&FinalAccuracy&' = 'T' then Stddev(useconc)/Avg(useconc)*100
                                                  else &&ConcRepresentationFunc&(Stddev(useconc), &&ConcFigures&)
                                                      /&&ConcRepresentationFunc&(Avg(useconc), &&ConcFigures&)*100
                                             end, &&DecPlPercCV&), &&DecPlPercCV&)),
        XMLELEMENT("bias", FormatRounded(round(Avg(usebias), &&DecPlPercBias&), &&DecPlPercBias&)),
        XMLELEMENT("biasonmean", FormatRounded(round(case when nominalconc = 0 then null
                                                    when '&&FinalAccuracy&' = 'T' then (Avg(useconc)-nominalconc)/nominalconc*100
                                                    else (&&ConcRepresentationFunc&(Avg(useconc), &&ConcFigures&)-nominalconc_sigrnd)/nominalconc_sigrnd*100
                                               end, &&DecPlPercBias&), &&DecPlPercBias&)),
        XMLELEMENT("flagpercent", min(flagpercent)),
        XMLELEMENT("days", days),
        XMLELEMENT("longtermtime", longtermtime),
        XMLELEMENT("longtermunits", longtermunits)
        ) res
    FROM
    (SELECT knownID, sampleID, sampleresultID, Name, nominalconc, nominalconc_sigrnd, runid, analyteid, analyteorder, species, matrix, concentrationunits, replicatenumber,
     conc, bias, flagpercent, days, ISDEACTIVATED, runsamplesequencenumber, temperature, longtermtime, longtermunits, knownorder, 
     case when ISDEACTIVATED = 'T' then null else conc end useconc,
     case when ISDEACTIVATED = 'T' then null else bias end usebias,
     case when abs(bias) > flagpercent then '&&FlaggedSymbol&' else null end flagged,
     case when ISDEACTIVATED <> 'T' and abs(bias) <= flagpercent then 1 else null end unflagged
     FROM
     (SELECT K.ID knownID, s.ID sampleID, sr.ID sampleresultID, K.NAME, ISDEACTIVATED, k.temperature,
        sr.nominalconcentrationconv nominalconc,
        &&ConcRepresentationFunc&(sr.nominalconcentrationconv, &&ConcFigures&) nominalconc_sigrnd,
        s.runid, s.runsamplesequencenumber,
	kwz.ordernumber knownorder,
        ra.analyteid, sa.analyteorder, a.species, a.sampletypeid matrix, ra.concentrationunits, r.runtypedescription, s.replicatenumber, k.flagpercent,
        r.extractiondate-k.preparationdate days, k.longtermtime longtermtime, k.longtermunits longtermunits,
        case when '&&FinalAccuracy&' = 'T' then sr.concentrationconv
             else &&ConcRepresentationFunc&(sr.concentrationconv, &&ConcFigures&)
        end conc,
        case when sr.nominalconcentrationconv = 0 then null
             when '&&FinalAccuracy&' = 'T' then (sr.concentrationconv-sr.nominalconcentrationconv)*100/sr.nominalconcentrationconv
             else round((&&ConcRepresentationFunc&(sr.concentrationconv, &&ConcFigures&)-
                         &&ConcRepresentationFunc&(sr.nominalconcentrationconv, &&ConcFigures&))*100
                        /&&ConcRepresentationFunc&(sr.nominalconcentrationconv, &&ConcFigures&), &&DecPlPercBias&)
        end bias
      FROM &&TempTabPrefix&bio$run$ana$known K,
           &&TempTabPrefix&bio$run r,
           &&TempTabPrefix&bio$run$analytes ra,
           &&TempTabPrefix&bio$study$analytes sa,
           &&TempTabPrefix&bio$assay a,
           &&TempTabPrefix&bio$run$samples s,
           &&TempTabPrefix&bio$run$sample$results sr,
           isr$crit kwz,
           table(&&LALPackage&.GetRunStates) rs
      WHERE K.ID = s.runknownID
        AND R.ID = s.runid AND R.ID = sr.runid
        --AND r.studyassayid =s.studyassayid AND s.studyassayid = sr.studyassayid
        AND r.RuntypeDescription in (&&RunTypes&)
        AND s.ID = sr.runsampleID
        AND s.sampletype = 'known'
        and k.knowntype = s.runsamplekind
        and kwz.masterkey = '&&ExpMasterkey&'
        AND ra.id = sr.runanalyteid
        AND sa.id = ra.analyteid
        AND a.id = r.assayid
        AND ra.runid = s.runid
        AND ra.studyID = '&&StudyID&'
        AND K.studyid = '&&StudyID&' AND s.studyid = '&&StudyID&'
        AND sr.studyID = '&&StudyID&' AND r.studyid = '&&StudyID&'
        and kwz.key = K.NAMEPREFIX
        and kwz.entity = '&&KnownEntity&'
        and repid = &&RepID&
        AND rs.column_value = r.runstatusnum
        &&AddCondition&
     ))
     GROUP BY analyteid, analyteorder, species, matrix, runid, knownorder, nominalconc, nominalconc_sigrnd, name, temperature, days, longtermtime, longtermunits, concentrationunits)
     GROUP BY analyteid, analyteorder, species, matrix, runid, temperature, concentrationunits
     )
    ) FROM dual