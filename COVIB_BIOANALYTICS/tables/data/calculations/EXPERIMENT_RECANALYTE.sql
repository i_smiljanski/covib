  SELECT  XMLELEMENT("experiment", XMLATTRIBUTES('&&Experiment&' AS "name" ,'recanalyte' AS "type"),
   (
   select Xmlagg(XMLConcat(
     Xmlagg(XMLELEMENT("calculation", xmlconcat(targ,val,res))), 
     XMLELEMENT("statistic", XMLATTRIBUTES(assayanalyteid AS "assayanalyteid" ),
       XMLELEMENT("min-mean_recovery", min(mean_recovery)),
       XMLELEMENT("max-mean_recovery", max(mean_recovery)),
       XMLELEMENT("numtotal", sum(cnt))
       )) order by assayanalyteid)
  from ( 
  SELECT  
      assayanalyteid,
      Count(useanalytearea) cnt,
      FormatRounded(round(avg(userecovery),&&DecPlPercBias&),&&DecPlPercBias&) mean_recovery,
      XMLELEMENT("target", 
        XMLELEMENT("runid", runid), 
        XMLELEMENT("internalstdname", internalstdname),
        XMLELEMENT("assayanalyteid", assayanalyteid),
        XMLELEMENT("knownconc", &&ConcFormatFunc&(knownconc,&&ConcFigures&))
        ) targ,
      XMLELEMENT("values", Xmlagg(
        XMLELEMENT("sample",  XMLATTRIBUTES(sampleid AS "sampleid", sampleresultrawid AS "sampleresultrawid" ),
          XMLELEMENT("knownid", knownid), 
          XMLELEMENT("stabilitytype", stabilitytype),         
          XMLELEMENT("name", name),          
          XMLELEMENT("replicatenumber", replicatenumber),
          XMLELEMENT("analytearea", &&PeakFormatFunc&(&&PeakRepresentationFunc&(analytearea,&&PeakFigures&),&&PeakFigures&)),     
          XMLELEMENT("ratio", FormatRounded(round(ratio,&&DecPlPercBias&),&&DecPlPercBias&)),          
          XMLELEMENT("meanarea", &&PeakFormatFunc&(&&PeakRepresentationFunc&(meanarea,&&PeakFigures&),&&PeakFigures&)),
          XMLELEMENT("meanratio", FormatRounded(round(meanratio,&&DecPlPercBias&),&&DecPlPercBias&)),
          XMLELEMENT("extracted_ratio", FormatRounded(round(u_extracted_ratio,&&DecPlPercBias&),&&DecPlPercBias&)),
          XMLELEMENT("recovery", FormatRounded(recovery,&&DecPlPercBias&)),
          XMLELEMENT("deactivated", isdeactivated)                            
        )
        order by stabilitytype, replicatenumber)) val,
      XMLELEMENT("result",       
        XMLELEMENT("n", Count(useanalytearea)),
        XMLELEMENT("meanarea", &&PeakFormatFunc&(&&PeakRepresentationFunc&(min(meanarea),&&PeakFigures&),&&PeakFigures&)),
        XMLELEMENT("meanratio", FormatRounded(round(min(meanratio),&&DecPlPercBias&),&&DecPlPercBias&)),
        XMLELEMENT("mean_recovery", FormatRounded(round(avg(userecovery),&&DecPlPercBias&),&&DecPlPercBias&))  
        ) res
    --))
    FROM
    (SELECT knownID, sampleID, sampleresultrawID, Name, knownconc, stabilitytype,  replicatenumber, ISDEACTIVATED,
     ratio, runid, assayanalyteid, internalstdname, analytearea, meanarea, meanratio, extracted_ratio, u_extracted_ratio, useratio, useanalytearea,
     case when stabilitytype='Extracted' then round(analytearea/meanarea*100,&&DecPlPercBias&) else null end as recovery,
     case when stabilitytype='Extracted' and ISDEACTIVATED='F' then 
             case when '&&FinalAccuracy&'='T' then analytearea/meanarea*100
                  else round(analytearea/meanarea*100,&&DecPlPercBias&) 
             end
          else null 
     end as userecovery            
     FROM  
    (SELECT knownID, sampleID, sampleresultrawID, Name, knownconc, stabilitytype,  replicatenumber, ISDEACTIVATED,
     ratio, runid, assayanalyteid, internalstdname, analytearea, meanratio, extracted_ratio, useratio, useanalytearea,
     case when stabilitytype='Unextracted' then min(extracted_ratio) over (partition by runid, assayanalyteid, internalstdname, knownconc, replicatenumber) else null end as u_extracted_ratio,
     case when stabilitytype='Extracted' then min(meanarea) over (partition by runid, assayanalyteid, internalstdname, knownconc, replicatenumber) else null end as meanarea      
     FROM   
    (SELECT knownID, sampleID, sampleresultrawID, Name, knownconc, stabilitytype,  replicatenumber, ISDEACTIVATED,
     ratio, runid, assayanalyteid, internalstdname, analytearea, useratio, useanalytearea,
     case when stabilitytype='Unextracted' then 
            case when '&&FinalAccuracy&'='T' then avg(useanalytearea) over (partition by runid, assayanalyteid, internalstdname, stabilitytype, knownID)
                 else round(avg(useanalytearea) over (partition by runid, assayanalyteid, internalstdname, stabilitytype, knownID),&&PeakFigures&) 
            end
          else null 
     end as meanarea,     
     case when stabilitytype='Unextracted' then 
            case when '&&FinalAccuracy&'='T' then avg(useratio) over (partition by runid, assayanalyteid, internalstdname, stabilitytype, knownID)
                 else round(avg(useratio) over (partition by runid, assayanalyteid, internalstdname, stabilitytype, knownID),&&DecPlPercBias&) 
            end
          else null 
     end as meanratio,
     case when stabilitytype='Extracted' then ratio else null end as extracted_ratio    
     FROM
     (SELECT K.ID knownID, s.ID sampleID, srw.ID sampleresultrawID, s.replicatenumber, s.ISDEACTIVATED,
        K.NAME, &&ConcRepresentationFunc&(K.concentration, &&ConcFigures&) knownconc, s.runid, k.assayanalyteid,
        k.stabilitytype stabtype, k.stabilitytype stabtype, case when regexp_like(K.NAME, '^&&Set1&') then 'Extracted' else 'Unextracted' end stabilitytype,
        srw.internalstdname,
        case when '&&FinalAccuracy&'='T' then srw.ratio
             else round(srw.ratio,&&DecPlPercBias&) 
        end ratio,
        case when '&&FinalAccuracy&'='T' then srw.analytearea
             else round(srw.analytearea,&&PeakFigures&) 
        end analytearea,        
        case when s.ISDEACTIVATED='T' then null 
             else case when '&&FinalAccuracy&'='T' then srw.ratio
                       else round(srw.ratio,&&DecPlPercBias&) 
                  end
        end useratio,
        case when s.ISDEACTIVATED='T' then null 
             else case when '&&FinalAccuracy&'='T' then srw.analytearea
                       else round(srw.analytearea,&&PeakFigures&)
                  end 
        end useanalytearea
      FROM &&TempTabPrefix&wt$assay$ana$known K,
           &&TempTabPrefix&wt$run r,
           &&TempTabPrefix&wt$run$analytes ra,
           &&TempTabPrefix&wt$run$samples s,
           &&TempTabPrefix&wt$run$sample$result$raw srw,
           isr$crit kwz,
           table(&&LALPackage&.GetRunStates) rs
      WHERE K.ID=s.knownID       
        AND R.ID = s.runid AND R.ID = srw.runid
        AND r.studyassayid =s.studyassayid AND s.studyassayid = srw.studyassayid      
        AND r.RuntypeDescription in (&&RunTypes&)                     
        AND s.ID=srw.runsampleID
        AND s.sampletype='known'
        AND K.knowntype = s.runsamplekind
        and kwz.masterkey = '&&ExpMasterkey&'
        AND ra.assayanalyteid = srw.assayanalyteid
        AND ra.runid = s.runid  
        AND ra.studyID = '&&StudyID&'
        AND K.studyid='&&StudyID&' AND s.studyid='&&StudyID&'
        AND srw.studyID='&&StudyID&' AND r.studyid = '&&StudyID&'
        and kwz.key=K.NAMEPREFIX
        and kwz.entity = '&&KnownEntity&'
        and repid=&&RepID&
        AND rs.column_value=r.runstatusnum &&AddCondition&)
     ) ) )
    GROUP BY runid, internalstdname, assayanalyteid, knownconc)
    group by assayanalyteid)
    ) FROM dual