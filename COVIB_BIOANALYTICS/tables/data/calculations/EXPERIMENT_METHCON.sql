  SELECT XMLELEMENT("experiment", XMLATTRIBUTES('&&Experiment&' AS "name" ,'method_con' AS "type"),
  (  
  select 
  Xmlagg(XMLConcat(  
    Xmlagg(XMLELEMENT("calculation", xmlconcat(targ,val,res)) order by knownid),
    XMLELEMENT("statistic", XMLATTRIBUTES(assayanalyteid AS "assayanalyteid" ),
      XMLELEMENT("flagpercent", min(flagpercent)),
      XMLELEMENT("min-bias", min(bias)),
      XMLELEMENT("max-bias", max(bias)),
      XMLELEMENT("min-cv", min(cv)),
      XMLELEMENT("max-cv", max(cv)),
      XMLELEMENT("numacc", sum(cntunflagged)),
      XMLELEMENT("numtotal", sum(cnt)),
      XMLELEMENT("percacc", FmtNum(round(sum(cntunflagged)/sum(cnt)*100,&&DecPlPercStat&)))      
    )))
    from ( 
    SELECT assayanalyteid, knownid,
      Count(useconc) cnt,
      count(unflagged) cntunflagged,      
      min(flagpercent) flagpercent,
      FormatRounded(round(Avg(usebias),&&DecPlPercBias&),&&DecPlPercBias&) bias,
      FormatRounded(round(case when knownconc = 0 then null
                               when '&&FinalAccuracy&'='T' then (Avg(useconc)-knownconc)/knownconc*100
                               else (&&ConcRepresentationFunc&(Avg(useconc),&&ConcFigures&)-knownconc_sigrnd)/knownconc_sigrnd*100
                          end,&&DecPlPercBias&),&&DecPlPercBias&) biasonmean,
      FormatRounded(round(case when '&&FinalAccuracy&'='T' then Stddev(useconc)/Avg(useconc)*100
                               else &&ConcRepresentationFunc&(Stddev(useconc),&&ConcFigures&)
                                   /&&ConcRepresentationFunc&(Avg(useconc),&&ConcFigures&)*100
                          end,&&DecPlPercCV&),&&DecPlPercCV&) cv,
      XMLELEMENT("target", 
        XMLELEMENT("assayanalyteid", assayanalyteid), 
        XMLELEMENT("knownid", knownid)
        ) targ,
      XMLELEMENT("values", Xmlagg(
        XMLELEMENT("sample", XMLATTRIBUTES(sampleresultid AS "sampleresultid" ),  
          XMLELEMENT("concentration", &&ConcFormatFunc&(conc,&&ConcFigures&)),   
          XMLELEMENT("knownconc", &&ConcFormatFunc&(knownconc_sigrnd,&&ConcFigures&)),
          XMLELEMENT("nominalconc", &&ConcFormatFunc&(nominalconc,&&ConcFigures&)),                    
          XMLELEMENT("bias", FormatRounded(bias,&&DecPlPercBias&)),
          XMLELEMENT("runid", runid),
          XMLELEMENT("replicatenumber", replicatenumber),           
          XMLELEMENT("analyst", analyst),       
          XMLELEMENT("deactivated", isdeactivated),
          XMLELEMENT("flagged", flagged)
        ) order by firstruns, runid, analyst,replicatenumber)) val,
      XMLELEMENT("result",       
        XMLELEMENT("n", Count(useconc)),
        XMLELEMENT("mean", &&ConcFormatFunc&(&&ConcRepresentationFunc&(Avg(useconc),&&ConcFigures&),&&ConcFigures&)),
        XMLELEMENT("sd", &&ConcFormatFunc&(&&ConcRepresentationFunc&(Stddev(useconc),&&ConcSDFigures&),&&ConcSDFigures&)),
        XMLELEMENT("cv", FormatRounded(round(case when '&&FinalAccuracy&'='T' then Stddev(useconc)/Avg(useconc)*100
                                                  else &&ConcRepresentationFunc&(Stddev(useconc),&&ConcFigures&)
                                                      /&&ConcRepresentationFunc&(Avg(useconc),&&ConcFigures&)*100
                                             end,&&DecPlPercCV&),&&DecPlPercCV&)),                                                      
        XMLELEMENT("bias", FormatRounded(round(Avg(usebias),&&DecPlPercBias&),&&DecPlPercBias&)),
        XMLELEMENT("biasonmean", FormatRounded(round(case when knownconc = 0 then null
                                                    when '&&FinalAccuracy&'='T' then (Avg(useconc)-knownconc)/knownconc*100
                                                    else (&&ConcRepresentationFunc&(Avg(useconc),&&ConcFigures&)-knownconc_sigrnd)/knownconc_sigrnd*100
                                               end,&&DecPlPercBias&),&&DecPlPercBias&)),        
        XMLELEMENT("flagpercent", min(flagpercent))
        ) res
    --) order by knownid)
    FROM
    (SELECT knownID, sampleID, sampleresultID, isdeactivated,
     Name, conc, useconc, knownconc_sigrnd, knownconc, nominalconc, runid, assayanalyteid, 
     bias, usebias, flagpercent, replicatenumber, firstruns, analyst,
     case when abs(bias) > flagpercent then '&&FlaggedSymbol&' else null end flagged,
     case when ISDEACTIVATED <> 'T' and abs(bias) <= flagpercent then 1 else null end unflagged
     FROM
     (SELECT K.ID knownID, s.ID sampleID, sr.ID sampleresultID, s.isdeactivated, K.NAME,  s.runid, 
        &&ConcRepresentationFunc&(sr.knownconcentrationconv,&&ConcFigures&) knownconc_sigrnd, sr.knownconcentrationconv knownconc,
        &&ConcRepresentationFunc&(sr.nominalconcentrationconv,&&ConcFigures&) nominalconc,        
        k.nameprefix knownname, 1/aliquotfactor dilutionfactor,
        sr.assayanalyteid, r.runtypedescription, k.flagpercent,        
        &&ConcRepresentationFunc&(sr.concentrationconv,&&ConcFigures&) conc,
        case when ISDEACTIVATED = 'T' then null
             else case when '&&FinalAccuracy&'='T' then sr.concentrationconv
                  else &&ConcRepresentationFunc&(sr.concentrationconv,&&ConcFigures&) 
             end
        end useconc, 
        case when sr.knownconcentrationconv = 0 then null
             else round((&&ConcRepresentationFunc&(sr.concentrationconv,&&ConcFigures&)-
                         &&ConcRepresentationFunc&(sr.knownconcentrationconv,&&ConcFigures&))*100
                        /&&ConcRepresentationFunc&(sr.knownconcentrationconv,&&ConcFigures&),&&DecPlPercBias&) 
        end bias,
        case when ISDEACTIVATED = 'T' then null
             when sr.knownconcentrationconv = 0 then null
             else case when '&&FinalAccuracy&'='T' then (sr.concentrationconv-sr.knownconcentrationconv)*100/sr.knownconcentrationconv
                       else round((&&ConcRepresentationFunc&(sr.concentrationconv,&&ConcFigures&)-
                                   &&ConcRepresentationFunc&(sr.knownconcentrationconv,&&ConcFigures&))*100
                                  /&&ConcRepresentationFunc&(sr.knownconcentrationconv,&&ConcFigures&),&&DecPlPercBias&) 
             end
        end usebias,              
        s.replicatenumber,
        dense_rank() over (partition by sr.assayanalyteid, knownID, analyst order by runstartDATE,r.runid) firstruns,
        dense_rank() over (order by analyst) analyst
      FROM &&TempTabPrefix&wt$assay$ana$known K,
           &&TempTabPrefix&wt$run r,
           &&TempTabPrefix&wt$run$analytes ra,
           &&TempTabPrefix&wt$run$samples s,
           &&TempTabPrefix&wt$run$sample$results sr,
           isr$crit kwz,
           table(&&LALPackage&.GetRunStates) rs
      WHERE K.ID=s.knownID
        AND R.ID = s.runid AND R.ID = sr.runid
        AND r.studyassayid =s.studyassayid AND s.studyassayid = sr.studyassayid      
        AND r.RuntypeDescription in (&&RunTypes&)
        AND s.ID=sr.runsampleID        
        AND s.sampletype='known'
        and k.knowntype = s.runsamplekind        
        and kwz.masterkey = '&&ExpMasterkey&'
        AND ra.assayanalyteid = sr.assayanalyteid
        AND ra.runid = s.runid  
        AND ra.studyID = '&&StudyID&'      
        AND K.studyid = '&&StudyID&' AND s.studyid = '&&StudyID&' 
        AND sr.studyID = '&&StudyID&' AND r.studyid = '&&StudyID&'
        and kwz.key=K.NAMEPREFIX
        and kwz.entity = '&&KnownEntity&'
        and repid=&&RepID&
        AND rs.column_value=r.runstatusnum &&AddCondition&
     )
     where firstruns < 4
     )
     GROUP BY assayanalyteid, knownid, knownconc, knownconc_sigrnd)
     GROUP BY assayanalyteid)
    ) FROM dual