SELECT XMLELEMENT("experiment", XMLATTRIBUTES('&&Experiment&' AS "name" ,'responsesample' AS "type"), --EXPERIMENT_ADA_DRUGTOLERANCE
      Xmlagg ( XMLConcat(
        Xmlagg (XMLELEMENT("calculation", xmlconcat(targ,val,res)) order by studyid, acceptedrun, analyteid, species, matrix, drug, nominalconc, duplicate, conc_unit, runid),
        XMLELEMENT("statistic",
          XMLATTRIBUTES(
            analyteid AS "analyteid", species AS "species", matrix AS "matrix",
            rid AS "runid", runid AS "runno", acceptedrun as "acceptedrun", drug AS "drug", conc_unit AS "conc-unit"
          ),
          XMLELEMENT("mean-fc", &&FormatFuncConc&(&&RepresentationFuncConc&(Avg(mean_fc), &&PlacesConc&), &&PlacesConc&))
        )
      ) order by studyid, acceptedrun, analyteid, species, matrix))
  FROM (
  SELECT
    studyid, analyteid, species, matrix, rid, runid, acceptedrun, duplicate, nominalconc,  conc_unit, drug,
    Avg(&&RepresentationFuncConc&(ISR$COVIB$PACKAGE.foreCast(screen_cp, SYS.ODCINUMBERLIST(below_cp_conc, above_cp_conc), SYS.ODCINUMBERLIST(below_cp,above_cp)), &&PlacesConc&)) mean_fc,
    --sum(s1_n)+sum(s2_n) n, sum(s1_m)+sum(s2_m) m,
    XMLELEMENT("target",
      XMLELEMENT("studyid", studyid),
      XMLELEMENT("analyteid", analyteid),
      XMLELEMENT("species", species),
      XMLELEMENT("matrix", matrix),
      XMLELEMENT("runid", rid),
      XMLELEMENT("runno", runid),
      XMLELEMENT("acceptedrun", acceptedrun),
      XMLELEMENT("duplicate", duplicate),
      XMLELEMENT("drug", drug),
      XMLELEMENT("ab-conc", &&FormatFuncConc&(&&RepresentationFuncConc&(nominalconc, &&PlacesConc&), &&PlacesConc&)),
      XMLELEMENT("conc-unit", conc_unit)
    ) as targ,
    XMLELEMENT("values",
      Xmlagg(
        XMLELEMENT("sample",
          XMLELEMENT("subject", subject),
          XMLELEMENT("drug-conc", &&FormatFuncConc&(&&RepresentationFuncConc&(to_number(drug_conc), &&PlacesConc&), &&PlacesConc&)),
          XMLELEMENT("dilution", dilution),
          XMLELEMENT("sample1", s1_xml),
          XMLELEMENT("sample2", s2_xml),
          XMLELEMENT("nc", nc_xml),
          XMLELEMENT("s1-mean", FormatRounded(round(s1_mean_useresponse, &&DecPlResponse&), &&DecPlResponse&)),
          XMLELEMENT("s2-mean", FormatRounded(round(s2_mean_useresponse, &&DecPlResponse&), &&DecPlResponse&)),
          XMLELEMENT("screen-cp", FormatRounded(round(screen_cp, &&DecPlCutPoint&), &&DecPlCutPoint&)),
          XMLELEMENT("confirm-cp", FormatRounded(round(confirm_cp, &&DecPlConfirmCutPoint&), &&DecPlConfirmCutPoint&)),
          XMLELEMENT("titer-cp",
            case
              when '&&TiterCPNFactor&' is not null then FormatRounded(round(titer_cp, &&DecPlCutPoint&), &&DecPlCutPoint&)
              else null
            end
          ),
          XMLELEMENT("n", s1_n),
          XMLELEMENT("m", s1_m),
          XMLELEMENT("s1-cv", FormatRounded(round(s1_cv, &&DecPlPercCV&), &&DecPlPercCV&)),
          XMLELEMENT("s1-cvflag", 
            case
              when s1_cv > &&PrecisionAcceptanceCriteria& then 'T'
              else 'F'
            end
          ),
          XMLELEMENT("s1-ratio", FormatRounded(round(
            case
              when nc_meanmedian_useresponse = 0 then null
              else s1_mean_useresponse/nc_meanmedian_useresponse
            end
          , &&DecPlRatio&), &&DecPlRatio&)),
          XMLELEMENT("s2-cv", FormatRounded(round(s2_cv, &&DecPlPercCV&), &&DecPlPercCV&)),
          XMLELEMENT("s2-cvflag", 
            case
              when s2_cv > &&PrecisionAcceptanceCriteria& then 'T'
              else 'F'
            end
          ),
          XMLELEMENT("s2-ratio", FormatRounded(round(
            case
              when nc_meanmedian_useresponse = 0 then null
              else s2_mean_useresponse/nc_meanmedian_useresponse
            end
          , &&DecPlRatio&), &&DecPlRatio&)),
          XMLELEMENT("s1-screen",-- "screen",
            case
              when s1_mean_useresponse is null then null 
              when s1_mean_useresponse >= screen_cp then 'POS'--'Positive'
              else 'NEG'--Negative'
            end
          ),
          XMLELEMENT("confirm",
            case
              when s2_mean_useresponse is null or confirm_cp is null then null
              else
                case
                  when (s1_mean_useresponse - s2_mean_useresponse) / NULLIF(s1_mean_useresponse,0) * 100 >= confirm_cp then 'POS'-- 'Positive'
                  else 'NEG'-- 'Negative'
                end
            end
          ),
          XMLELEMENT("difference", FormatRounded(round(
            case
              when s1_mean_useresponse = 0 then null
              else (s1_mean_useresponse-s2_mean_useresponse)/s1_mean_useresponse*100
            end
          , &&DecPlInhibition&), &&DecPlInhibition&)),
          XMLELEMENT("inhibition", FormatRounded(round(abs(
            case
              when s1_mean_useresponse = 0 then null
              else (s1_mean_useresponse-s2_mean_useresponse)/s1_mean_useresponse*100
            end
          ), &&DecPlInhibition&), &&DecPlInhibition&)),
          XMLELEMENT("below-cp-conc", below_cp_conc),
          XMLELEMENT("above-cp-conc", above_cp_conc),
          XMLELEMENT("below-cp", below_cp),
          XMLELEMENT("above-cp", above_cp),
          XMLELEMENT("above_cp_conc", &&FormatFuncConc&(&&RepresentationFuncConc&(ISR$COVIB$PACKAGE.foreCast(screen_cp, SYS.ODCINUMBERLIST(below_cp_conc, above_cp_conc), SYS.ODCINUMBERLIST(below_cp,above_cp)), &&PlacesConc&), &&PlacesConc&)),
          XMLELEMENT("at-confirm-cutpoint-conc", &&FormatFuncConc&(&&RepresentationFuncConc&(ISR$COVIB$PACKAGE.foreCast(confirm_cp, SYS.ODCINUMBERLIST(below_confirm_cp_conc, above_confirm_cp_conc), SYS.ODCINUMBERLIST(below_confirm_cp,above_confirm_cp)), &&PlacesConc&), &&PlacesConc&))
          --XMLELEMENT("above_cp_conc", above_cp_conc)
          --XMLELEMENT("status", resulttext),
          --XMLELEMENT("deactivated", isdeactivated)
        ) order by nominalconc, subject, dilution, to_number(drug_conc)
      )
    ) as val,
    XMLELEMENT("result", 
      XMLELEMENT("mean", FormatRounded(round(Avg(s1_mean_useresponse), &&DecPlResponse&), &&DecPlResponse&)),
      XMLELEMENT("run-mean", FormatRounded(round(min(s1_avg_mean_useresponse), &&DecPlResponse&), &&DecPlResponse&)),
      XMLELEMENT("nc-meanmedian", FormatRounded(round(min(nc_meanmedian_useresponse), &&DecPlResponse&), &&DecPlResponse&)),
      XMLELEMENT("nc-cv", FormatRounded(round(min(nc_cv), &&DecPlPercCV&), &&DecPlPercCV&)),
      XMLELEMENT("mean-fc", &&FormatFuncConc&(&&RepresentationFuncConc&(Avg(&&RepresentationFuncConc&(ISR$COVIB$PACKAGE.foreCast(screen_cp, SYS.ODCINUMBERLIST(below_cp_conc, above_cp_conc), SYS.ODCINUMBERLIST(below_cp,above_cp)), &&PlacesConc&)), &&PlacesConc&), &&PlacesConc&) ),
      --XMLELEMENT("above_cp_conc", max(above_cp_conc)),
      XMLELEMENT("n", count(s1_mean_useresponse)),
      XMLELEMENT("m", count(s1_mean_response)),
      XMLELEMENT("nc-n", min(nc_n)),
      XMLELEMENT("nc-m", min(nc_m))
    ) as res
  FROM (
    SELECT
      studyid, analyteid, rid, runid, acceptedrun, subject, duplicate, nominalconc, dilution,
      species, matrix, source, drug_conc, conc_unit, drug, confirm_cp,
      s1_mean_response, s1_mean_useresponse,
      s1_n, s1_m, s1_sd,
      case
        when s1_mean_useresponse = 0 then null
        else s1_sd/s1_mean_useresponse*100
      end s1_cv,
      s1_xml, s1_avg_mean_useresponse,
      s2_mean_response, s2_mean_useresponse,
      s2_n, s2_m, s2_sd,
      case
        when s2_mean_useresponse = 0 then null
        else s2_sd/s2_mean_useresponse*100
      end s2_cv,
      s2_xml, s2_avg_mean_useresponse,
      nc_meanmedian_response, nc_meanmedian_useresponse,
      nc_n, nc_m, nc_sd,
      case
        when nc_meanmedian_useresponse = 0 then null
        else nc_sd/nc_meanmedian_useresponse*100
      end nc_cv,
      nc_xml,
      screen_cp, titer_cp, below_cp, above_cp,
      max(
        case
          when s1_mean_response = below_cp then &&RepresentationFuncConc&(to_number(drug_conc), &&PlacesConc&)
          else null
        end
      ) over (partition by studyid, analyteid, species, matrix, rid, runid, acceptedrun, nominalconc, duplicate, drug, conc_unit) below_cp_conc,
      max(
        case
          when s1_mean_response = above_cp then &&RepresentationFuncConc&(to_number(drug_conc), &&PlacesConc&)
          else null
        end
      ) over (partition by studyid, analyteid, species, matrix, rid, runid, acceptedrun, nominalconc, duplicate, drug, conc_unit) above_cp_conc,
      below_confirm_cp, above_confirm_cp,
      max(case when
        case
          when s1_mean_useresponse = 0 then null
          else (s1_mean_useresponse-s2_mean_useresponse)/s1_mean_useresponse*100
        end = below_confirm_cp then drug_conc
      else null end) over (partition by studyid, analyteid, species, matrix, rid, runid, acceptedrun, nominalconc, duplicate, drug, conc_unit) below_confirm_cp_conc,
      max(case when
        case
          when s1_mean_useresponse = 0 then null
          else (s1_mean_useresponse-s2_mean_useresponse)/s1_mean_useresponse*100
        end = above_confirm_cp then drug_conc
      else null end) over (partition by studyid, analyteid, species, matrix, rid, runid, acceptedrun, nominalconc, duplicate, drug, conc_unit) above_confirm_cp_conc
    FROM
      (
      SELECT
        sample1.studyid, sample1.analyteid, sample1.rid, sample1.runid, sample1.acceptedrun, sample1.subject, sample1.duplicate, sample1.nominalconc, sample1.dilution,
        sample1.species, sample1.matrix, sample1.source, sample1.drug_conc, sample1.conc_unit, sample1.drug,
        case
          when regexp_like(ccp.key, '^\-?(\d+)?\.?\d+$') then to_number(ccp.key)
          else null
        end confirm_cp,
        sample1.mean_response s1_mean_response, sample1.mean_useresponse s1_mean_useresponse,
        sample1.n s1_n, sample1.m s1_m, sample1.sd s1_sd, sample1.xml s1_xml,
        Avg(sample1.mean_useresponse) over (partition by sample1.analyteid, sample1.species, sample1.matrix, sample1.rid) s1_avg_mean_useresponse,
        sample2.mean_response s2_mean_response, sample2.mean_useresponse s2_mean_useresponse,
        sample2.n s2_n, sample2.m s2_m, sample2.sd s2_sd, sample2.xml s2_xml,
        Avg(sample2.mean_useresponse) over (partition by sample2.analyteid, sample2.species, sample2.matrix, sample2.rid) s2_avg_mean_useresponse,
        NC.meanmedian_response nc_meanmedian_response, NC.meanmedian_useresponse nc_meanmedian_useresponse,
        NC.n nc_n, NC.m nc_m, NC.sd nc_sd, NC.xml nc_xml,
        NC.screen_cp, NC.titer_cp,
        max(case when sample1.mean_response < NC.screen_cp then sample1.mean_response else null end)
          over (partition by sample1.studyid, sample1.analyteid, sample1.species, sample1.matrix, sample1.rid, sample1.runid, sample1.nominalconc, sample1.duplicate, sample1.drug, sample1.conc_unit) below_cp,
        min(case when sample1.mean_response >= NC.screen_cp then sample1.mean_response else null end)
          over (partition by sample1.studyid, sample1.analyteid, sample1.species, sample1.matrix, sample1.rid, sample1.runid, sample1.nominalconc, sample1.duplicate, sample1.drug, sample1.conc_unit) above_cp,
        max(case when
          case
            when sample1.mean_useresponse = 0 then null
            else (sample1.mean_useresponse-sample2.mean_useresponse)/sample1.mean_useresponse*100
          end <= case when regexp_like(ccp.key, '^\-?(\d+)?\.?\d+$') then to_number(ccp.key) else null end then case when sample1.mean_useresponse = 0 then null else (sample1.mean_useresponse-sample2.mean_useresponse)/sample1.mean_useresponse*100 end
        else null end) over (partition by sample1.studyid, sample1.analyteid, sample1.species, sample1.matrix, sample1.rid, sample1.runid, sample1.nominalconc, sample1.duplicate, sample1.drug, sample1.conc_unit) below_confirm_cp,
        min(case when
          case
            when sample1.mean_useresponse = 0 then null
            else (sample1.mean_useresponse-sample2.mean_useresponse)/sample1.mean_useresponse*100
          end > case when regexp_like(ccp.key, '^\-?(\d+)?\.?\d+$') then to_number(ccp.key) else null end then case when sample1.mean_useresponse = 0 then null else (sample1.mean_useresponse-sample2.mean_useresponse)/sample1.mean_useresponse*100 end
        else null end) over (partition by sample1.studyid, sample1.analyteid, sample1.species, sample1.matrix, sample1.rid, sample1.runid, sample1.nominalconc, sample1.duplicate, sample1.drug, sample1.conc_unit) above_confirm_cp
      FROM
        (
          SELECT
            studyid, analyteid, rid, runid, acceptedrun, subject, nominalconc, dilution, duplicate,
            species, matrix, source, drug_conc, conc_unit, drug,
            Avg(response) mean_response,
            Avg(case when(isdeactivated='F') then response else null end) mean_useresponse,
            Stddev(case when(isdeactivated='F') then response else null end) sd,
            count(case when(isdeactivated='F') then 1 else null end) n,
            count(1) m,
            Xmlagg(
              XMLELEMENT("sample",
                XMLATTRIBUTES(sampleid AS "sampleid", sampleresultid AS "sampleresultid" ),
                XMLELEMENT("runsamplesequencenumber", runsamplesequencenumber),
                XMLELEMENT("samplename", samplename),
                XMLELEMENT("samplesubtype", samplesubtype),
                XMLELEMENT("response", FormatRounded(round(response, &&DecPlResponse&), &&DecPlResponse&)),
                XMLELEMENT("status", concentrationstatus),
                XMLELEMENT("deactivated", isdeactivated),
                XMLELEMENT("reason", reason)
              ) order by runsamplesequencenumber
            ) xml
          FROM (
            SELECT
              r.studyid, s.id sampleid, sr.id sampleresultid,
              s.designsampleid, s.samplename, to_number(regexp_replace(s.samplename, '^.*(\d+)\-\d+([ _\-]N?D)?$', '\1', 1, 1, 'i')) subject,
              to_number(regexp_replace(s.samplename, '^.*-(\d+)([ _\-]N?D)?$', '\1', 1, 1, 'i')) duplicate,
              r.id rid, r.runid, sa.id analyteid, s.samplesubtype,
              case when ra.RunAnalyteRegressionStatus = &&AcceptedRunAnalyte& then 'T' else 'F' end acceptedrun,
              s.runsamplesequencenumber, sr.analytearea response,
              s.status, sr.commenttext, sr.concentration conc,
              sr.concentrationstatus, s.source, d.reason,
              k.concentration nominalconc, s.dilution,
              sr.concentrationunits concentrationunit, a.species, a.sampletypeid matrix,
              /*case
                when regexp_like(s.source,'^([^ ]*) ([^ ]*) ([^ ]*)$') then*/ regexp_replace(s.source,'^([^ ]*) ([^ ]*) ([^ ]*)$', '\1', 1, 1, 'i')
                /*else null
              end*/ drug,
              /*case
                when regexp_like(s.source,'^([^ ]*) ([^ ]*) ([^ ]*)$') then*/ regexp_replace(s.source,'^([^ ]*) ([^ ]*) ([^ ]*)$', '\3', 1, 1, 'i')
                /*else null
              end*/ conc_unit,
              /*case
                when regexp_like(s.source,'^([^ ]*) ([^ ]*) ([^ ]*)$') then*/ regexp_replace(s.source,'^([^ ]*) ([^ ]*) ([^ ]*)$', '\2', 1, 1, 'i')
                /*else '0.00'
              end*/ drug_conc,
              case
                when d.code is not null then 'T'
                else 'F'
              end isdeactivated
            FROM 
              &&TempTabPrefix&bio$run r,
              &&TempTabPrefix&bio$run$analytes ra,
              &&TempTabPrefix&bio$study$analytes sa,
              &&TempTabPrefix&bio$assay a,
              &&TempTabPrefix&bio$run$worklist s,
              &&TempTabPrefix&bio$run$worklist$result$rw sr,
              &&TempTabPrefix&bio$run$ana$known k,
              &&TempTabPrefix&bio$covib$run$sample$deact d,
              isr$crit kwz--,
              --table(&&LALPackage&.GetRunStates) rs
            WHERE r.ID = s.runID AND s.ID = sr.worklistID AND a.ID = r.assayID
            AND r.studyID = '&&StudyID&' AND a.studyID = '&&StudyID&'
            --AND rs.column_value = r.runstatusnum
            AND sr.runAnalyteCode = ra.code AND r.ID = ra.runID
            AND ra.analyteID = sa.ID AND s.code = d.code (+)
            AND k.runAnalyteCode = ra.code AND k.name = s.sampleName
            AND kwz.key = s.samplename AND kwz.entity = '&&KnownEntity&' AND kwz.masterkey = '&&ExpMasterkey&' AND kwz.repid = &&RepID&
            --and s.runcode || '-' || s.runsamplesequencenumber || '-' || sa.analyteorder in (select code from &&TempTabPrefix&bio$deactivated$samples)
            &&AddConditionS1&
          ) 
          GROUP BY studyid, analyteid, species, matrix, rid, runid, acceptedrun, duplicate, subject, nominalconc, dilution, drug_conc, conc_unit, drug, source
        ) sample1,
        (
          SELECT
            studyid, analyteid, rid, runid, acceptedrun, subject, nominalconc, dilution, duplicate,
            species, matrix, source, drug_conc, conc_unit, drug,
            Avg(response) mean_response,
            Avg(case when(isdeactivated='F') then response else null end) mean_useresponse,
            Stddev(case when(isdeactivated='F') then response else null end) sd,
            count(case when(isdeactivated='F') then 1 else null end) n,
            count(1) m,
            Xmlagg(
              XMLELEMENT("sample",
                XMLATTRIBUTES(sampleid AS "sampleid", sampleresultid AS "sampleresultid" ),
                XMLELEMENT("runsamplesequencenumber", runsamplesequencenumber),
                XMLELEMENT("samplename", samplename),
                XMLELEMENT("samplesubtype", samplesubtype),
                XMLELEMENT("response", FormatRounded(round(response, &&DecPlResponse&), &&DecPlResponse&)),
                XMLELEMENT("status", concentrationstatus),
                XMLELEMENT("deactivated", isdeactivated),
                XMLELEMENT("reason", reason)
              ) order by runsamplesequencenumber
            ) xml
          FROM (
            SELECT
              r.studyid, s.id sampleid, sr.id sampleresultid,
              s.designsampleid, s.samplename, to_number(regexp_replace(s.samplename, '^.*(\d+)\-\d+([ _\-]N?D)?$', '\1', 1, 1, 'i')) subject,
              to_number(regexp_replace(s.samplename, '^.*-(\d+)([ _\-]N?D)?$', '\1', 1, 1, 'i')) duplicate,
              r.id rid, r.runid, sa.id analyteid, s.samplesubtype,
              case when ra.RunAnalyteRegressionStatus = &&AcceptedRunAnalyte& then 'T' else 'F' end acceptedrun,
              s.runsamplesequencenumber, sr.analytearea response,
              s.status, sr.commenttext, sr.concentration conc,
              sr.concentrationstatus, s.source, d.reason,
              k.concentration nominalconc, s.dilution,
              sr.concentrationunits concentrationunit, a.species, a.sampletypeid matrix,
              /*case
                when regexp_like(s.source,'^([^ ]*) ([^ ]*) ([^ ]*)$') then*/ regexp_replace(s.source,'^([^ ]*) ([^ ]*) ([^ ]*)$', '\1', 1, 1, 'i')
                /*else null
              end*/ drug,
              /*case
                when regexp_like(s.source,'^([^ ]*) ([^ ]*) ([^ ]*)$') then*/ regexp_replace(s.source,'^([^ ]*) ([^ ]*) ([^ ]*)$', '\3', 1, 1, 'i')
                /*else null
              end*/ conc_unit,
              /*case
                when regexp_like(s.source,'^([^ ]*) ([^ ]*) ([^ ]*)$') then*/ regexp_replace(s.source,'^([^ ]*) ([^ ]*) ([^ ]*)$', '\2', 1, 1, 'i')
                /*else '0.00'
              end*/ drug_conc,
              case
                when d.code is not null then 'T'
                else 'F'
              end isdeactivated
            FROM 
              &&TempTabPrefix&bio$run r,
              &&TempTabPrefix&bio$run$analytes ra,
              &&TempTabPrefix&bio$study$analytes sa,
              &&TempTabPrefix&bio$assay a,
              &&TempTabPrefix&bio$run$worklist s,
              &&TempTabPrefix&bio$run$worklist$result$rw sr,
              &&TempTabPrefix&bio$run$ana$known k,
              &&TempTabPrefix&bio$covib$run$sample$deact d,
              isr$crit kwz--,
              --table(&&LALPackage&.GetRunStates) rs
            WHERE r.ID = s.runID AND s.ID = sr.worklistID AND a.ID = r.assayID
            AND r.studyID = '&&StudyID&' AND a.studyID = '&&StudyID&'
            --AND rs.column_value = r.runstatusnum
            AND sr.runAnalyteCode = ra.code AND r.ID = ra.runID
            AND ra.analyteID = sa.ID AND s.code = d.code (+)
            AND k.runAnalyteCode = ra.code AND k.name = s.sampleName
            AND kwz.key = s.samplename AND kwz.entity = '&&KnownEntity&' AND kwz.masterkey = '&&ExpMasterkey&' AND kwz.repid = &&RepID&
            --and s.runcode || '-' || s.runsamplesequencenumber || '-' || sa.analyteorder in (select code from &&TempTabPrefix&bio$deactivated$samples)
            &&AddConditionS2&
          ) 
          GROUP BY studyid, analyteid, species, matrix, rid, runid, acceptedrun, duplicate, subject, nominalconc, dilution, drug_conc, conc_unit, drug, source
        ) sample2,
        (
          SELECT
            studyid, analyteid, rid, runid, species, matrix,
            &&NCMeanOrMedian&(
              case when stb$util.GetReportParameterValue('MEAN_OR_MEDIAN_NC',&&RepID&) = 'meanofmeans' then
                case when replicatenumber = 1 then meanresponse else null end
              else
                response
              end
            ) meanmedian_response,
            &&NCMeanOrMedian&(
              case when stb$util.GetReportParameterValue('MEAN_OR_MEDIAN_NC',&&RepID&) = 'meanofmeans' then
                case when replicatenumber = 1 then meanuseresponse else null end
              else
                case when(isdeactivated='F') then response else null end
              end
            ) meanmedian_useresponse,
            Stddev(case when(isdeactivated='F') then response else null end) sd,
            case
              when '&&FixedScreenCutpoint&' is not null then to_number('&&FixedScreenCutpoint&')
              when '&&ScreenCPNFactor&' is not null then
                &&NCMeanOrMedian&(
                  case when stb$util.GetReportParameterValue('MEAN_OR_MEDIAN_NC',&&RepID&) = 'meanofmeans' then
                    case when replicatenumber = 1 then meanuseresponse else null end
                  else
                    case when(isdeactivated='F') then response else null end
                  end
                )*to_number('&&ScreenCPNFactor&')
              else
                &&NCMeanOrMedian&(
                  case when stb$util.GetReportParameterValue('MEAN_OR_MEDIAN_NC',&&RepID&) = 'meanofmeans' then
                    case when replicatenumber = 1 then meanuseresponse else null end
                  else
                    case when(isdeactivated='F') then response else null end
                  end
                )
            end screen_cp,
            case
              when '&&FixedTiterCutpoint&' is not null then to_number('&&FixedTiterCutpoint&')
              when '&&TiterCPNFactor&' is not null then &&NCMeanOrMedian&(
                case when stb$util.GetReportParameterValue('MEAN_OR_MEDIAN_NC',&&RepID&) = 'meanofmeans' then
                  case when replicatenumber = 1 then meanuseresponse else null end
                else
                  case when(isdeactivated='F') then response else null end
                end
              )*to_number('&&TiterCPNFactor&')
              else null
            end titer_cp,
            count(case when(isdeactivated='F') then 1 else null end) n,
            count(1) m,
            Xmlagg(
              XMLELEMENT("sample",
                XMLATTRIBUTES(sampleid AS "sampleid", sampleresultid AS "sampleresultid" ),
                XMLELEMENT("runsamplesequencenumber", runsamplesequencenumber),
                XMLELEMENT("samplename", samplename),
                XMLELEMENT("samplesubtype", samplesubtype),
                XMLELEMENT("response", FormatRounded(round(response, &&DecPlResponse&), &&DecPlResponse&)),
                XMLELEMENT("status", concentrationstatus),
                XMLELEMENT("deactivated", isdeactivated),
                XMLELEMENT("reason", reason)
              ) order by runsamplesequencenumber
            ) xml
          FROM (
            SELECT
              r.studyid, s.id sampleid, sr.id sampleresultid,
              s.designsampleid, s.samplename, d.reason, s.replicatenumber,
              r.id rid, r.runid, sa.id analyteid, s.samplesubtype,
              s.runsamplesequencenumber, sr.analytearea response,
              s.status, sr.commenttext, sr.concentration conc,
              sr.concentrationstatus, s.source, s.concentration nominalconc,
              sr.concentrationunits concentrationunit, a.species, a.sampletypeid matrix,
              s.hours, s.longtermunits, s.longtermtime, s.temperature,
              Avg(case when(d.code is null) then sr.analytearea else null end) over (partition by r.studyid, sa.id, r.id, s.samplename) meanresponse,
              Avg(case when(d.code is null) then sr.analytearea else null end) over (partition by r.studyid, sa.id, r.id, s.samplename) meanuseresponse,
              case
                when d.code is not null then 'T'
                else 'F'
              end isdeactivated
            FROM 
              &&TempTabPrefix&bio$run r,
              &&TempTabPrefix&bio$run$analytes ra,
              &&TempTabPrefix&bio$study$analytes sa,
              &&TempTabPrefix&bio$assay a,
              &&TempTabPrefix&bio$run$worklist s,
              &&TempTabPrefix&bio$run$worklist$result$rw sr,
              &&TempTabPrefix&bio$covib$run$sample$deact d,
              isr$crit kwz--,
              --table(ISR$COVIB$BIOANALYTICS$WT$LAL.GetRunStates) rs
            WHERE r.ID = s.runID AND s.ID = sr.worklistID AND a.ID = r.assayID
            AND r.studyID = '&&StudyID&' AND a.studyID = '&&StudyID&'
            --AND rs.column_value = r.runstatusnum
            AND sr.runAnalyteCode = ra.code AND r.ID = ra.runID
            AND sr.runanalyteCode = ra.Code AND ra.analyteID = sa.ID
            AND s.code = d.code (+)
            AND kwz.key = s.samplename AND kwz.entity = '&&KnownEntity&' AND kwz.masterkey = '&&ExpMasterkey&' AND kwz.repid = &&RepID&
            --and s.runcode || '-' || s.runsamplesequencenumber || '-' || sa.analyteorder in (select code from &&TempTabPrefix&bio$deactivated$samples)
            &&AddConditionNC&
          )
          GROUP BY studyid, analyteid, species, matrix, rid, runid) NC,
          isr$crit ccp
      WHERE sample1.studyID = sample2.studyID(+) AND sample1.rID = sample2.rID(+) AND sample1.runid = sample2.runid(+)
      AND sample1.analyteID = sample2.analyteID(+) AND sample1.species = sample2.species(+) AND sample1.matrix = sample2.matrix(+) AND sample1.subject = sample2.subject(+) AND sample1.nominalconc = sample2.nominalconc(+)
      AND sample1.drug_conc = sample2.drug_conc(+) AND sample1.drug = sample2.drug(+) AND sample1.conc_unit = sample2.conc_unit(+)
      AND sample1.studyID = NC.studyID(+) AND sample1.rID = NC.rID(+) AND sample1.runid = NC.runid(+) AND sample1.analyteID = NC.analyteID(+) AND sample1.species = NC.species(+) AND sample1.matrix = NC.matrix(+)
      AND ccp.entity = 'BIO$COVIB$CONFIRM$CUTPOINT' AND ccp.masterkey = 'NORM' AND ccp.repid = &&RepID&
      &&AddCondition&
   )
 )
 GROUP BY studyid, analyteid, species, matrix, rid, runid, acceptedrun, nominalconc, duplicate, drug, conc_unit, screen_cp, titer_cp)
GROUP BY studyid, analyteid, species, matrix, rid, runid, acceptedrun, drug, conc_unit
