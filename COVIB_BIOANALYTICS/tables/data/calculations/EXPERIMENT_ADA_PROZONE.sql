SELECT XMLELEMENT("experiment", XMLATTRIBUTES('&&Experiment&' AS "name" ,'prozone_response' AS "type"),
      Xmlagg ( XMLConcat(
        Xmlagg (XMLELEMENT("calculation", xmlconcat(targ,val/*,res*/)) order by studyid, acceptedrun, analyteid, species, matrix, hours, longtermunits, longtermtime, temperature, nominalconc desc, dilution, name)
        --XMLELEMENT("statistic", XMLATTRIBUTES(analyteid AS "analyteid"),
               
             --)) order by studyid, analyteid, species, matrix, hours, longtermunits, longtermtime, temperature
      )))
  FROM (
  SELECT
    studyid, analyteid, species, matrix, acceptedrun, hours, longtermunits, longtermtime, temperature, dilution, nominalconc, name,
    --sum(s1_n)+sum(s2_n) n, sum(s1_m)+sum(s2_m) m,
    XMLELEMENT("target",
      XMLELEMENT("studyid", studyid),
      XMLELEMENT("analyteid", analyteid),
      XMLELEMENT("species", species),
      XMLELEMENT("matrix", matrix),
      XMLELEMENT("acceptedrun", acceptedrun),
      XMLELEMENT("hours", hours),
      XMLELEMENT("longtermunits", longtermunits),
      XMLELEMENT("longtermtime", longtermtime),
      XMLELEMENT("temperature", temperature),
      XMLELEMENT("nominalconc", &&FormatFuncConc&(&&RepresentationFuncConc&(nominalconc, &&PlacesConc&), &&PlacesConc&)),
      XMLELEMENT("dilution", dilution),
      XMLELEMENT("name", name)
    ) as targ,
    XMLELEMENT("values",
      Xmlagg(
        XMLELEMENT("subject",
          XMLATTRIBUTES(rid AS "runid", runid AS "runno", subject AS "subject"),
          XMLELEMENT("subject", subject),
          XMLELEMENT("nominalconc", &&FormatFuncConc&(&&RepresentationFuncConc&(nominalconc, &&PlacesConc&), &&PlacesConc&)),
          XMLELEMENT("dilution", dilution),
          XMLELEMENT("sample1", s1_xml),
          XMLELEMENT("sample2", s2_xml),
          XMLELEMENT("nc", nc_xml),
          XMLELEMENT("conc-unit", conc_unit),
          XMLELEMENT("nc-meanmedian", FormatRounded(round(nc_meanmedian_useresponse, &&DecPlResponse&), &&DecPlResponse&)),
          XMLELEMENT("s1-mean", FormatRounded(round(s1_mean_useresponse, &&DecPlResponse&), &&DecPlResponse&)),
          XMLELEMENT("s2-mean", FormatRounded(round(s2_mean_useresponse, &&DecPlResponse&), &&DecPlResponse&)),
          XMLELEMENT("screen-cp", FormatRounded(round(screen_cp, &&DecPlCutPoint&), &&DecPlCutPoint&)),
          XMLELEMENT("confirm-cp", FormatRounded(round(confirm_cp, &&DecPlConfirmCutPoint&), &&DecPlConfirmCutPoint&)),
          XMLELEMENT("n", s1_n),
          XMLELEMENT("m", s1_m),
          XMLELEMENT("s1-cv", FormatRounded(round(s1_cv, &&DecPlPercCV&), &&DecPlPercCV&)),
          XMLELEMENT("s2-cv", FormatRounded(round(s2_cv, &&DecPlPercCV&), &&DecPlPercCV&)),
          XMLELEMENT("nc-cv", FormatRounded(round(nc_cv, &&DecPlPercCV&), &&DecPlPercCV&)),
          XMLELEMENT("s1-cvflag", 
            case
              when s1_cv > &&PrecisionAcceptanceCriteria& then 'T'
              else 'F'
            end
          ),
          XMLELEMENT("s2-cvflag", 
            case
              when s2_cv > &&PrecisionAcceptanceCriteria& then 'T'
              else 'F'
            end
          ),
          XMLELEMENT("s1-ratio", FormatRounded(round(
            case
              when nc_meanmedian_useresponse = 0 then null
              else s1_mean_useresponse/nc_meanmedian_useresponse--*100
            end
          , &&DecPlRatio&), &&DecPlRatio&)),
          XMLELEMENT("s2-ratio", FormatRounded(round(
            case
              when nc_meanmedian_useresponse = 0 then null
              else s2_mean_useresponse/nc_meanmedian_useresponse--*100
            end
          , &&DecPlRatio&), &&DecPlRatio&)),
          XMLELEMENT("s1-screen",-- "screen",
            case
              when s1_mean_useresponse is null then null 
              when s1_mean_useresponse >= screen_cp then 'POS'--'Positive'
              else 'NEG'--Negative'
            end
          ),
          XMLELEMENT("confirm",
            case
              when s2_mean_useresponse is null or confirm_cp is null then null
              else
                case
                  when (s1_mean_useresponse - s2_mean_useresponse) / NULLIF(s1_mean_useresponse,0) * 100 >= confirm_cp then 'POS'-- 'Positive'
                  else 'NEG'-- 'Negative'
                end
            end
          ),
          XMLELEMENT("difference", FormatRounded(round(
            case
              when s1_mean_useresponse = 0 then null
              else (s1_mean_useresponse-s2_mean_useresponse)/s1_mean_useresponse*100
            end
          , &&DecPlInhibition&), &&DecPlInhibition&)),
          XMLELEMENT("inhibition", FormatRounded(round(abs(
            case
              when s1_mean_useresponse = 0 then null
              else (s1_mean_useresponse-s2_mean_useresponse)/s1_mean_useresponse*100
            end
          ), &&DecPlInhibition&), &&DecPlInhibition&))
        ) order by runid, subject
      )
    ) as val
  FROM (
    SELECT
      sample1.studyid, sample1.analyteid, sample1.rid, sample1.runid, sample1.acceptedrun, sample1.subject, sample1.name, sample1.nominalconc, sample1.dilution, sample1.conc_unit,
      sample1.species, sample1.matrix, sample1.hours, sample1.longtermunits, sample1.longtermtime, sample1.temperature,
      case
        when regexp_like(ccp.key, '^\-?(\d+)?\.?\d+$') then to_number(ccp.key)
        else null
      end confirm_cp,
      sample1.mean_response s1_mean_response, sample1.mean_useresponse s1_mean_useresponse,
      sample1.n s1_n, sample1.m s1_m, sample1.sd s1_sd,
      case
        when sample1.mean_useresponse = 0 then null
        else sample1.sd/sample1.mean_useresponse*100
      end s1_cv, sample1.xml s1_xml,
      sample2.mean_response s2_mean_response, sample2.mean_useresponse s2_mean_useresponse,
      sample2.n s2_n, sample2.m s2_m, sample2.sd s2_sd,
      case
        when sample2.mean_useresponse = 0 then null
        else sample2.sd/sample2.mean_useresponse*100
      end s2_cv, sample2.xml s2_xml,
      case
        when '&&FixedScreenCutpoint&' is not null then
          to_number('&&FixedScreenCutpoint&')
        when '&&ScreenCPNFactor&' is not null then
          NC.meanmedian_useresponse*to_number('&&ScreenCPNFactor&')
        else
          NC.meanmedian_useresponse
      end screen_cp,
      NC.meanmedian_response nc_meanmedian_response, NC.meanmedian_useresponse nc_meanmedian_useresponse,
      NC.n nc_n, NC.m nc_m, NC.sd nc_sd,
      case
        when NC.meanmedian_useresponse = 0 then null
        else NC.sd/NC.meanmedian_useresponse*100
      end nc_cv, NC.xml nc_xml
    FROM
      (
        SELECT
          studyid, analyteid, rid, runid, acceptedrun, subject, nominalconc, dilution, name, concentrationunit conc_unit,
          species, matrix, hours, longtermunits, longtermtime, temperature,
          Avg(response) mean_response,
          Avg(case when(isdeactivated='F') then response else null end) mean_useresponse,
          Stddev(case when(isdeactivated='F') then response else null end) sd,
          count(case when(isdeactivated='F') then 1 else null end) n,
          count(1) m,
          Xmlagg(
            XMLELEMENT("sample",
              XMLATTRIBUTES(sampleid AS "sampleid", sampleresultid AS "sampleresultid" ),
              XMLELEMENT("runsamplesequencenumber", runsamplesequencenumber),
              XMLELEMENT("samplename", samplename),
              XMLELEMENT("samplesubtype", samplesubtype),
              XMLELEMENT("response", FormatRounded(round(response, &&DecPlResponse&), &&DecPlResponse&)),
              XMLELEMENT("status", resulttext),
              XMLELEMENT("deactivated", isdeactivated),
              XMLELEMENT("reason", reason)
            ) order by runsamplesequencenumber
          ) xml
        FROM (
          SELECT
            r.studyid, s.id sampleid, sr.id sampleresultid, d.reason,
            s.designsampleid, s.samplename, to_number(regexp_replace(s.samplename, '^.*[ _\-](\d+)([ _\-]N?D)?$', '\1', 1, 1, 'i')) subject,
            regexp_replace(s.samplename, '^(\w+\d+)[ _\-].*$', '\1', 1, 1, 'i') name,
            r.id rid, r.runid, sa.id analyteid, s.samplesubtype,
            case when ra.RunAnalyteRegressionStatus = &&AcceptedRunAnalyte& then 'T' else 'F' end acceptedrun,
            s.runsamplesequencenumber, sr.analytearea response,
            s.status, sr.commenttext, sr.concentration conc,
            sr.resulttext, s.source,
            k.concentration nominalconc, s.dilution,
            sr.concentrationunits concentrationunit, a.species, a.sampletypeid matrix,
            --s.hours, s.longtermunits, s.longtermtime, s.temperature,
            &&StabilityInfo&
            case
              when d.code is not null then 'T'
              else 'F'
            end isdeactivated
          FROM 
            &&TempTabPrefix&bio$run r,
            &&TempTabPrefix&bio$run$analytes ra,
            &&TempTabPrefix&bio$study$analytes sa,
            &&TempTabPrefix&bio$assay a,
            &&TempTabPrefix&bio$run$worklist s,
            &&TempTabPrefix&bio$run$worklist$result$rw sr,
            &&TempTabPrefix&bio$run$ana$known k,
            &&TempTabPrefix&bio$covib$run$sample$deact d,
            isr$crit kwz--,
            --table(&&LALPackage&.GetRunStates) rs
          WHERE r.ID = s.runID
          AND s.ID = sr.worklistID
          AND a.ID = r.assayID
          AND r.studyID = 'S1' AND a.studyID = 'S1'
          --AND rs.column_value = r.runstatusnum
          AND sr.runAnalyteCode = ra.code
          AND r.ID = ra.runID
          AND ra.analyteID = sa.ID
          AND s.code = d.code (+)
          AND k.runAnalyteCode = ra.code AND k.name = s.sampleName
          AND kwz.key = s.samplename AND kwz.entity = '&&KnownEntity&' AND kwz.masterkey = '&&ExpMasterkey&' AND kwz.repid = &&RepID&
          --and s.runcode || '-' || s.runsamplesequencenumber || '-' || sa.analyteorder in (select code from &&TempTabPrefix&bio$deactivated$samples)
          &&AddConditionS1&
        )
        GROUP BY studyid, analyteid, species, matrix, hours, longtermunits, longtermtime, temperature, rid, runid, acceptedrun, name, subject, nominalconc, dilution, concentrationunit
      ) sample1,
      (
        SELECT
          studyid, analyteid, rid, runid, acceptedrun, subject, nominalconc, dilution, name, concentrationunit conc_unit,
          species, matrix, hours, longtermunits, longtermtime, temperature,
          Avg(response) mean_response,
          Avg(case when(isdeactivated='F') then response else null end) mean_useresponse,
          Stddev(case when(isdeactivated='F') then response else null end) sd,
          count(case when(isdeactivated='F') then 1 else null end) n,
          count(1) m,
          Xmlagg(
            XMLELEMENT("sample",
              XMLATTRIBUTES(sampleid AS "sampleid", sampleresultid AS "sampleresultid" ),
              XMLELEMENT("runsamplesequencenumber", runsamplesequencenumber),
              XMLELEMENT("samplename", samplename),
              XMLELEMENT("samplesubtype", samplesubtype),
              XMLELEMENT("response", FormatRounded(round(response, &&DecPlResponse&), &&DecPlResponse&)),
              XMLELEMENT("status", resulttext),
              XMLELEMENT("deactivated", isdeactivated),
              XMLELEMENT("reason", reason)
            ) order by runsamplesequencenumber
          ) xml
        FROM (
          SELECT
            r.studyid, s.id sampleid, sr.id sampleresultid, d.reason,
            s.designsampleid, s.samplename, to_number(regexp_replace(s.samplename, '^.*[ _\-](\d+)([ _\-]N?D)?$', '\1', 1, 1, 'i')) subject,
            regexp_replace(s.samplename, '^(\w+\d+)[ _\-].*$', '\1', 1, 1, 'i') name,
            r.id rid, r.runid, sa.id analyteid, s.samplesubtype,
            case when ra.RunAnalyteRegressionStatus = &&AcceptedRunAnalyte& then 'T' else 'F' end acceptedrun,
            s.runsamplesequencenumber, sr.analytearea response,
            s.status, sr.commenttext, sr.concentration conc,
            sr.resulttext, s.source,
            k.concentration nominalconc, s.dilution,
            sr.concentrationunits concentrationunit, a.species, a.sampletypeid matrix,
            --s.hours, s.longtermunits, s.longtermtime, s.temperature,
            &&StabilityInfo&
            case
              when d.code is not null then 'T'
              else 'F'
            end isdeactivated
          FROM 
            &&TempTabPrefix&bio$run r,
            &&TempTabPrefix&bio$run$analytes ra,
            &&TempTabPrefix&bio$study$analytes sa,
            &&TempTabPrefix&bio$assay a,
            &&TempTabPrefix&bio$run$worklist s,
            &&TempTabPrefix&bio$run$worklist$result$rw sr,
            &&TempTabPrefix&bio$run$ana$known k,
            &&TempTabPrefix&bio$covib$run$sample$deact d,
            isr$crit kwz--,
            --table(&&LALPackage&.GetRunStates) rs
          WHERE r.ID = s.runID
          AND s.ID = sr.worklistID
          AND a.ID = r.assayID
          AND r.studyID = 'S1' AND a.studyID = 'S1'
          --AND rs.column_value = r.runstatusnum
          AND sr.runAnalyteCode = ra.code
          AND r.ID = ra.runID
          AND ra.analyteID = sa.ID
          AND s.code = d.code (+)
          AND k.runAnalyteCode = ra.code AND k.name = s.sampleName
          AND kwz.key = s.samplename AND kwz.entity = '&&KnownEntity&' AND kwz.masterkey = '&&ExpMasterkey&' AND kwz.repid = &&RepID&
          --and s.runcode || '-' || s.runsamplesequencenumber || '-' || sa.analyteorder in (select code from &&TempTabPrefix&bio$deactivated$samples)
          &&AddConditionS2&
        )
        GROUP BY studyid, analyteid, species, matrix, hours, longtermunits, longtermtime, temperature, rid, runid, acceptedrun, name, subject, nominalconc, dilution, concentrationunit
      ) sample2,
      (
        SELECT
          studyid, analyteid, rid, runid, species, matrix,
          &&NCMeanOrMedian&(
            case when stb$util.GetReportParameterValue('MEAN_OR_MEDIAN_NC',&&RepID&) = 'meanofmeans' then case when replicatenumber = 1 then meanresponse else null end
            else response end
          ) meanmedian_response,
          &&NCMeanOrMedian&(
            case when stb$util.GetReportParameterValue('MEAN_OR_MEDIAN_NC',&&RepID&) = 'meanofmeans' then
              case when replicatenumber = 1 then meanuseresponse else null end
            else
              case when(isdeactivated='F') then response else null end
            end
          ) meanmedian_useresponse,
          Stddev(
            case when stb$util.GetReportParameterValue('MEAN_OR_MEDIAN_NC',&&RepID&) = 'meanofmeans' then
              case when replicatenumber = 1 then meanuseresponse else null end
            else
              case when(isdeactivated='F') then response else null end
            end
          ) sd,
          count(
            case when stb$util.GetReportParameterValue('MEAN_OR_MEDIAN_NC',&&RepID&) = 'meanofmeans' then
              case when replicatenumber = 1 then meanuseresponse else null end
            else
              case when(isdeactivated='F') then response else null end
            end
          ) n,
          count(
            case when stb$util.GetReportParameterValue('MEAN_OR_MEDIAN_NC',&&RepID&) = 'meanofmeans' then case when replicatenumber = 1 then meanresponse else null end
            else response end
          ) m,
          Xmlagg(
            XMLELEMENT("sample",
              XMLATTRIBUTES(sampleid AS "sampleid", sampleresultid AS "sampleresultid" ),
              XMLELEMENT("runsamplesequencenumber", runsamplesequencenumber),
              XMLELEMENT("samplename", samplename),
              XMLELEMENT("samplesubtype", samplesubtype),
              XMLELEMENT("response", FormatRounded(round(response, &&DecPlResponse&), &&DecPlResponse&)),
              XMLELEMENT("status", resulttext),
              XMLELEMENT("deactivated", isdeactivated),
              XMLELEMENT("reason", reason)
            ) order by runsamplesequencenumber
          ) xml
        FROM (
          SELECT
            r.studyid, s.id sampleid, sr.id sampleresultid,
            s.designsampleid, s.samplename, d.reason, s.replicatenumber,
            r.id rid, r.runid, sa.id analyteid, s.samplesubtype,
            s.runsamplesequencenumber, sr.analytearea response,
            s.status, sr.commenttext, sr.concentration conc,
            sr.resulttext, s.source, s.concentration nominalconc,
            sr.concentrationunits concentrationunit, a.species, a.sampletypeid matrix,
            s.hours, s.longtermunits, s.longtermtime, s.temperature,
            Avg(case when(d.code is null) then sr.analytearea else null end) over (partition by r.studyid, sa.id, r.id, s.samplename) meanresponse,
            Avg(case when(d.code is null) then sr.analytearea else null end) over (partition by r.studyid, sa.id, r.id, s.samplename) meanuseresponse,
            case
              when d.code is not null then 'T'
              else 'F'
            end isdeactivated
          FROM 
            &&TempTabPrefix&bio$run r,
            &&TempTabPrefix&bio$run$analytes ra,
            &&TempTabPrefix&bio$study$analytes sa,
            &&TempTabPrefix&bio$assay a,
            &&TempTabPrefix&bio$run$worklist s,
            &&TempTabPrefix&bio$run$worklist$result$rw sr,
            &&TempTabPrefix&bio$covib$run$sample$deact d,
            isr$crit kwz--,
            --table(ISR$COVIB$BIOANALYTICS$WT$LAL.GetRunStates) rs
          WHERE r.ID = s.runID
          AND s.ID = sr.worklistID
          AND a.ID = r.assayID
          AND r.studyID = 'S1' AND a.studyID = 'S1'
          --AND rs.column_value = r.runstatusnum
          AND sr.runAnalyteCode = ra.code
          AND r.ID = ra.runID
          AND sr.runanalyteCode = ra.Code
          AND ra.analyteID = sa.ID
          AND s.code = d.code (+)
          AND kwz.key = s.samplename AND kwz.entity = '&&KnownEntity&' AND kwz.masterkey = '&&ExpMasterkey&' AND kwz.repid = &&RepID&
          --and s.runcode || '-' || s.runsamplesequencenumber || '-' || sa.analyteorder in (select code from &&TempTabPrefix&bio$deactivated$samples)
          &&AddConditionNC&
        )
        GROUP BY studyid, analyteid, species, matrix, rid, runid) NC,
        isr$crit ccp
    WHERE sample1.studyID = sample2.studyID(+) AND sample1.rID = sample2.rID(+) AND sample1.runid = sample2.runid(+)
    AND sample1.analyteID = sample2.analyteID(+) AND sample1.species = sample2.species(+) AND sample1.matrix = sample2.matrix(+) AND sample1.subject = sample2.subject(+) AND sample1.nominalconc = sample2.nominalconc(+) AND sample1.dilution = sample2.dilution(+)
    AND sample1.studyID = NC.studyID(+) AND sample1.rID = NC.rID(+) AND sample1.runid = NC.runid(+) AND sample1.analyteID = NC.analyteID(+) AND sample1.species = NC.species(+) AND sample1.matrix = NC.matrix(+)
    AND ccp.entity = 'BIO$COVIB$CONFIRM$CUTPOINT' AND ccp.masterkey = 'NORM' AND ccp.repid = &&RepID&
    &&AddCondition&
 )
 GROUP BY studyid, analyteid, species, matrix, hours, longtermunits, longtermtime, temperature, rid, runid, acceptedrun, nominalconc, dilution, name)
GROUP BY studyid--, analyteid, species, matrix, hours, longtermunits, longtermtime, temperature, acceptedrun
