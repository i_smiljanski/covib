SELECT XMLELEMENT("experiment", XMLATTRIBUTES('&&Experiment&' AS "name" ,'dtaresults_confirm_&&type&' AS "type"),
      Xmlagg ( XMLConcat(
        Xmlagg (XMLELEMENT("calculation", xmlconcat(targ,val,res)) order by acceptedrun, analyteorder, analyteid, groupno, groupname, dosegroupno, dosegroup, designsubjecttag, subjectid, samplingtime, customid),
        XMLELEMENT("statistic", XMLATTRIBUTES(studyid AS "studyid"),
               XMLELEMENT("n", count(studyid))
             )) order by studyid
      ))
FROM (
  SELECT studyid, analyteid, analyteorder, acceptedrun, matrix, subjectgroupid, treatmentid, groupname, dosegroup, groupno, dosegroupno, designsubjecttag, subjectid, designsampleid, samplingtime, customid,
    XMLELEMENT("target",
      XMLELEMENT("acceptedrun", acceptedrun),
      XMLELEMENT("analyteid", analyteid),
      XMLELEMENT("designsampleid", designsampleid),
      XMLELEMENT("customid", customid),
      XMLELEMENT("meanormedian", min(meanormedian))
    ) as targ,
    XMLELEMENT("values", 
      Xmlagg(
        XMLELEMENT("sample",
          --XMLATTRIBUTES(sampleid AS "sampleid"/*, sampleresultid AS "sampleresultid" */),
          XMLELEMENT("runid", rid),
          XMLELEMENT("runno", runid)
        ) order by runid
      )
    ) as val,
    XMLELEMENT("result",
      XMLATTRIBUTES(designsampleid AS "designsampleid")
      &&DTAFields&
    ) as res
  FROM (
    SELECT
      nc.studyid, nc.rid, nc.runid, nc.acceptedrun, nc.rundate, nc.analyteid, nc.assayname, REGEXP_REPLACE(nc.analytename, '^anti[ _-]', '', 1, 1, 'i') analytename, nc.analyteorder, nc.matrix,
      sample1.subjectgroupid, sample1.treatmentid, sample1.groupname, sample1.dosegroup, sample1.groupno, sample1.dosegroupno,
      sample1.designsubjecttag, sample1.subjectid, sample1.designsampleid, sample1.samplingtime, sample1.doseamount, sample1.doseunit, sample1.customid,
      sample1.cvlimit,
      -- NC:
      nc.cutpointlimit, nc.meanormedian, nc.meanmedian_response nc_meanmedian_response,
      nc.concunit nc_concunit, nc_sd, nc_cv, nc_n, nc_m,-- nc_xml,
      nc.cutpoint,
      -- Sample1:
      sample1.dilution s1_dilution, sample1.mean_response s1_mean_response,
      sample1.mean_useresponse s1_mean_useresponse, sample1.sd s1_sd, sample1.cv s1_cv,
      sample1.result s1_result, sample1.resultcomment s1_resultcomment,
      sample1.m s1_m, sample1.n s1_n, sample1.xml s1_xml,
      -- Sample2:
      sample2.dilution s2_dilution, sample2.mean_response s2_mean_response,
      sample2.mean_useresponse s2_mean_useresponse, sample2.sd s2_sd, sample2.cv s2_cv,
      sample2.result s2_result, sample2.resultcomment s2_resultcomment,
      sample2.m s2_m, sample2.n s2_n, sample2.xml s2_xml
    FROM (
      SELECT 
          studyid,
          rid,
          runid,
          acceptedrun,
          rundate,
          analyteid,
          analytename,
          analyteorder,
          assayname,
          matrix,
          concentrationunit concunit,
          cutpointlimit,
          meanormedian,
          case
            when meanormedian = 'Mean' then Avg(useresponse)
            when meanormedian = 'Median' then Median(useresponse)
            when meanormedian = 'Mean of Means' then Avg(case when replicatenumber = 1 then meanuseresponse else null end)
            else Avg(useresponse)
          end meanmedian_response,
          case
            when min(cutpointmethod) = 3 then min(cutpointoffset) * Stddev(useresponse) else 0
          end + case
            when min(cutpointmethod) = 1 then min(cutpointoffset) else 0
          end + case
            when min(cutpointmethod) = 2 then min(cutpointoffset)
            when meanormedian = 'Mean' then Avg(useresponse)
            when meanormedian = 'Median' then Median(useresponse)
            when meanormedian = 'Mean of Means' then Avg(case when replicatenumber = 1 then meanuseresponse else null end)
            else Avg(useresponse) * min(cutpointfactor)
          end * case
            when min(cutpointmethod) = 0 then min(cutpointfactor) else 1
          end cutpoint,
          case 
            when meanormedian = 'Mean of Means' then Stddev(case when replicatenumber = 1 then meanuseresponse else null end)
            else Stddev(useresponse)
          end nc_sd,
          case 
            when meanormedian = 'Mean' then Stddev(useresponse)/nullif(Avg(useresponse),0)*100
            when meanormedian = 'Median' then Stddev(useresponse)/nullif(Median(useresponse),0)*100
            when meanormedian = 'Mean of Means' then Stddev(case when replicatenumber = 1 then meanuseresponse else null end)/nullif(Avg(case when replicatenumber = 1 then meanuseresponse else null end),0)*100
            else Stddev(useresponse)/nullif(Avg(useresponse),0)*100
          end nc_cv,
          count(useresponse) nc_n,
          count(response) nc_m
          FROM(
            SELECT
              r.studyid,
              r.id rid,
              r.runid,
              case when ra.RunAnalyteRegressionStatus = &&RejectedRunAnalyte& then 'F' else 'T' end acceptedrun,
              r.runstartdate rundate,
              sa.id analyteid,
              sa.name analytename,
              sa.analyteorder,
              a.name assayname,
              a.sampletypeid matrix,
              s.id sampleid,
              sr.id sampleresultid,
              s.samplename name,
              s.samplesubtype,
              s.runsamplesequencenumber,
              sr.concentrationunits concentrationunit,
              sr.resulttext,
              sr.analytearea response,
              a.meanormedian,
              s.replicatenumber,
              a.cutpointfactor,
              a.cutpointoffset,
              a.cutpointmethod,
              a.cutpointlimit,
              case when(ds.code is null) then sr.analytearea else null end useresponse,
              Avg(case when(ds.code is null) then sr.analytearea else null end) over (partition by r.studyid, sa.id, r.id, s.samplename) meanuseresponse,
              case
                when ds.code is not null then 'T'
                else 'F'
              end isdeactivated
            FROM 
              &&TempTabPrefix&bio$run r,
              &&TempTabPrefix&bio$run$analytes ra,
              &&TempTabPrefix&bio$study$analytes sa,
              &&TempTabPrefix&bio$assay a,
              &&TempTabPrefix&bio$run$worklist s,
              &&TempTabPrefix&bio$run$worklist$result$rw sr,
              &&TempTabPrefix&bio$deactivated$samples ds,
              table(&&LALPackage&.GetRunStates) rs
            WHERE r.ID = s.runID
            AND s.ID = sr.worklistID
            AND a.ID = r.assayID
            --AND s.treatmentid = 'ADA'
            --AND s.studyid = '&&StudyID&' AND sr.studyID = '&&StudyID&'
            AND r.studyid = '&&StudyID&'
            AND a.studyID = '&&StudyID&'
            AND rs.column_value = r.runstatusnum
            AND sr.runAnalyteCode = ra.code
            AND r.ID = ra.runID
            AND ra.analyteID = sa.ID
            AND s.runcode || '-' || s.runsamplesequencenumber || '-' || sa.analyteorder = ds.code (+)
            AND (s.runsamplekind like '&&NCKnownType&')
            &&AddConditionNC&
          )
        GROUP BY studyID, rid, runid, acceptedrun, rundate, analyteid, analytename, analyteorder, assayname, matrix, cutpointlimit, cutpointfactor, meanormedian, concentrationunit
    ) nc, (
          SELECT
            studyid, analyteid, analytename, rid, runid, subjectgroupid, treatmentid, groupname, dosegroup, groupno, dosegroupno, designsubjecttag, subjectid, designsampleid, samplingtime, customid,
            doseamount, doseunit,
            max(dilution) dilution,
            Avg(response) mean_response,
            Avg(case when(isdeactivated='F') then response else null end) mean_useresponse,
            Stddev(case when(isdeactivated='F') then response else null end) sd,
            Stddev(case when(isdeactivated='F') then response else null end)/nullif(Avg(case when(isdeactivated='F') then response else null end), 0)*100 cv,
            count(case when(isdeactivated='F') then 1 else null end) n,
            count(1) m,
            max(result) result,
            max(resultcomment) resultcomment,
            max(flagpercent)  cvlimit,
            Xmlagg(
              XMLELEMENT("sample",
                XMLATTRIBUTES(sampleid AS "sampleid", sampleresultid AS "sampleresultid" ),
                XMLELEMENT("runsamplesequencenumber", runsamplesequencenumber),
                XMLELEMENT("samplename", samplename),
                XMLELEMENT("samplesubtype", samplesubtype),
                XMLELEMENT("response", &&FormatFuncResponse&(&&RepresentationFuncResponse&(response, &&PlacesResponse&), &&PlacesResponse&)),
                XMLELEMENT("status", resulttext),
                XMLELEMENT("deactivated", isdeactivated)--,
                --XMLELEMENT("reason", reason)
              ) order by runsamplesequencenumber
            ) xml
          FROM (
            SELECT
              r.studyid,
              s.id sampleid,
              s.samplename,
              sr.id sampleresultid,
              sam.subjectid,
              sam.designsubjecttag,
              s.designsampleid,
              r.id rid,
              r.runid,
              sa.id analyteid,
              sa.name analytename,
              s.samplesubtype,
              s.runsamplesequencenumber,
              sr.analytearea response,
              s.status,
              sr.commenttext,
              sr.resulttext,
              sr.resultcommenttext resultcomment,
              max(case when regexp_like(sr.resultcommenttext,'^Final[-_\ ]Result','i') then 'T' else 'F' end) over (partition by s.studyid, sa.id, sam.subjectgroupid, s.treatmentid, sg.name, t.name, sam.subjectid, s.designsampleid, sam.samplingtime, t.doseamount, t.doseunitsdescription, s.customid) availablefinal,
              case when regexp_like(sr.resultcommenttext,'^Final[-_\ ]Result','i') then 'T' else 'F' end isfinal,
              max(sr.resulttext) over (partition by s.designsampleid, r.id) result,
              s.source,
              sr.concentrationunits concentrationunit,
              s.time,
              s.timetext,
              s.studyday,
              sam.subjectgroupid,
              sam.treatmentid,
              sg.name groupname,
              t.name dosegroup,
              s.dilution,
              max(rh.reported) over (partition by s.designsampleid, r.id) reported,
              s.customid,
              case
                  when REGEXP_LIKE(sg.name, '^[+-]?\d?\.?\d+$','i') then
                      to_number(sg.name)
                  else
                    null
              end groupno,
              case
                  when REGEXP_LIKE(t.name, '^[+-]?\d?\.?\d+$','i') then
                      to_number(t.name)
                  when REGEXP_LIKE(t.description, '^[+-]?\d?\.?\d+$','i') then
                      to_number(t.description)
                  else
                    null
              end dosegroupno,
              t.doseamount,
              t.doseunitsdescription doseunit,
              sam.samplingtime,
              s.samplesubtype posgroup,
              case
                when s.runcode || '-' || s.runsamplesequencenumber || '-' || sa.analyteorder in (select code from &&TempTabPrefix&bio$deactivated$samples) then 'T'
                else 'F'
              end isdeactivated,
              s.flagpercent
            FROM 
              &&TempTabPrefix&bio$run r,
              &&TempTabPrefix&bio$run$analytes ra,
              &&TempTabPrefix&bio$study$analytes sa,
              &&TempTabPrefix&bio$assay a,
              &&TempTabPrefix&bio$run$worklist s,
              &&TempTabPrefix&bio$run$worklist$result$rw sr,
              &&TempTabPrefix&bio$sample sam,
              &&TempTabPrefix&bio$study$subjectgroup sg,
              &&TempTabPrefix&bio$study$treatment t,
              &&TempTabPrefix&bio$reassay$history rh,
              table(&&LALPackage&.GetRunStates) rs
            WHERE r.ID = s.runID
            AND s.ID = sr.worklistID
            AND a.ID = r.assayID
            AND s.designSampleID = sam.ID
            AND r.studyid = '&&StudyID&'
            AND a.studyID = '&&StudyID&'
            AND rs.column_value = r.runstatusnum
            AND sr.runAnalyteCode = ra.code
            AND r.ID = ra.runID
            AND ra.analyteID = sa.ID
            AND sam.subjectgroupcode = sg.code
            AND sam.treatmentcode = t.code
            --s.runcode || '-' || s.runsamplesequencenumber || '-' || sa.analyteorder in (select code from &&TempTabPrefix&bio$deactivated$samples)
            AND r.code || '-' || s.runsamplesequencenumber || '-' || sa.analyteorder = rh.runsamplecode (+)
            &&AddConditionS1&
          )
          WHERE ('&&DTAShowFinal&'!='T' or availablefinal='F' or isfinal='T')
          GROUP BY studyid, analyteid, analytename, rid, runid, subjectgroupid, treatmentid, groupname, dosegroup, groupno, dosegroupno, designsubjecttag, subjectid, designsampleid, samplingtime, doseamount, doseunit, customid
    ) sample1,
    (
          SELECT
            studyid, analyteid, analytename, rid, runid, subjectgroupid, treatmentid, groupname, dosegroup, groupno, dosegroupno, designsubjecttag, subjectid, designsampleid, samplingtime, customid,
            doseamount, doseunit,
            max(dilution) dilution,
            Avg(response) mean_response,
            Avg(case when(isdeactivated='F') then response else null end) mean_useresponse,
            Stddev(case when(isdeactivated='F') then response else null end) sd,
            Stddev(case when(isdeactivated='F') then response else null end)/nullif(Avg(case when(isdeactivated='F') then response else null end), 0)*100 cv,
            count(case when(isdeactivated='F') then 1 else null end) n,
            count(1) m,
            max(result) result,
            max(resultcomment) resultcomment,
            max(flagpercent)  cvlimit,
            Xmlagg(
              XMLELEMENT("sample",
                XMLATTRIBUTES(sampleid AS "sampleid", sampleresultid AS "sampleresultid" ),
                XMLELEMENT("runsamplesequencenumber", runsamplesequencenumber),
                XMLELEMENT("samplename", samplename),
                XMLELEMENT("samplesubtype", samplesubtype),
                XMLELEMENT("response", &&FormatFuncResponse&(&&RepresentationFuncResponse&(response, &&PlacesResponse&), &&PlacesResponse&)),
                XMLELEMENT("status", resulttext),
                XMLELEMENT("deactivated", isdeactivated)--,
                --XMLELEMENT("reason", reason)
              ) order by runsamplesequencenumber
            ) xml
          FROM (
            SELECT
              r.studyid,
              s.id sampleid,
              s.samplename,
              sr.id sampleresultid,
              sam.subjectid,
              sam.designsubjecttag,
              s.designsampleid,
              r.id rid,
              r.runid,
              sa.id analyteid,
              sa.name analytename,
              s.samplesubtype,
              s.runsamplesequencenumber,
              sr.analytearea response,
              s.status,
              sr.commenttext,
              sr.resulttext,
              sr.resultcommenttext resultcomment,
              max(case when regexp_like(sr.resultcommenttext,'^Final[-_\ ]Result','i') then 'T' else 'F' end) over (partition by s.studyid, sa.id, sam.subjectgroupid, s.treatmentid, sg.name, t.name, sam.subjectid, s.designsampleid, sam.samplingtime, t.doseamount, t.doseunitsdescription, s.customid) availablefinal,
              case when regexp_like(sr.resultcommenttext,'^Final[-_\ ]Result','i') then 'T' else 'F' end isfinal,
              max(sr.resulttext) over (partition by s.designsampleid, r.id) result,
              s.source,
              sr.concentrationunits concentrationunit,
              s.time,
              s.timetext,
              s.studyday,
              sam.subjectgroupid,
              sam.treatmentid,
              sg.name groupname,
              t.name dosegroup,
              s.dilution,
              max(rh.reported) over (partition by s.designsampleid, r.id) reported,
              s.customid,
              case
                  when REGEXP_LIKE(sg.name, '^[+-]?\d?\.?\d+$','i') then
                      to_number(sg.name)
                  else
                    null
              end groupno,
              case
                  when REGEXP_LIKE(t.name, '^[+-]?\d?\.?\d+$','i') then
                      to_number(t.name)
                  when REGEXP_LIKE(t.description, '^[+-]?\d?\.?\d+$','i') then
                      to_number(t.description)
                  else
                    null
              end dosegroupno,
              t.doseamount,
              t.doseunitsdescription doseunit,
              sam.samplingtime,
              s.samplesubtype posgroup,
              case
                when s.runcode || '-' || s.runsamplesequencenumber || '-' || sa.analyteorder in (select code from &&TempTabPrefix&bio$deactivated$samples) then 'T'
                else 'F'
              end isdeactivated,
              s.flagpercent
            FROM 
              &&TempTabPrefix&bio$run r,
              &&TempTabPrefix&bio$run$analytes ra,
              &&TempTabPrefix&bio$study$analytes sa,
              &&TempTabPrefix&bio$assay a,
              &&TempTabPrefix&bio$run$worklist s,
              &&TempTabPrefix&bio$run$worklist$result$rw sr,
              &&TempTabPrefix&bio$sample sam,
              &&TempTabPrefix&bio$study$subjectgroup sg,
              &&TempTabPrefix&bio$study$treatment t,
              &&TempTabPrefix&bio$reassay$history rh,
              table(&&LALPackage&.GetRunStates) rs
            WHERE r.ID = s.runID
            AND s.ID = sr.worklistID
            AND a.ID = r.assayID
            AND s.designSampleID = sam.ID
            AND r.studyid = '&&StudyID&'
            AND a.studyID = '&&StudyID&'
            AND rs.column_value = r.runstatusnum
            AND sr.runAnalyteCode = ra.code
            AND r.ID = ra.runID
            AND ra.analyteID = sa.ID
            AND sam.subjectgroupcode = sg.code
            AND sam.treatmentcode = t.code
            --s.runcode || '-' || s.runsamplesequencenumber || '-' || sa.analyteorder in (select code from &&TempTabPrefix&bio$deactivated$samples)
            AND r.code || '-' || s.runsamplesequencenumber || '-' || sa.analyteorder = rh.runsamplecode (+)
            &&AddConditionS2&
          )
          WHERE ('&&DTAShowFinal&'!='T' or availablefinal='F' or isfinal='T')
          GROUP BY studyid, analyteid, analytename, rid, runid, subjectgroupid, treatmentid, groupname, dosegroup, groupno, dosegroupno, designsubjecttag, subjectid, designsampleid, samplingtime, doseamount, doseunit, customid
    ) sample2
    WHERE sample1.studyID = nc.studyID
    AND sample1.rID = nc.rID
    AND sample1.runid = nc.runid
    AND sample1.analyteID = nc.analyteID
    AND sample1.studyID = sample2.studyID
    AND sample1.rID = sample2.rID
    AND sample1.runid = sample2.runid
    AND sample1.analyteID = sample2.analyteID
    AND sample1.designSampleID = sample2.designSampleID
    &&AddCondition&
  ) 
  
  GROUP BY studyid, analyteid, analyteorder, matrix, subjectgroupid, treatmentid, groupname, dosegroup, groupno, dosegroupno, designsubjecttag, subjectid, designsampleid, samplingtime, customid, runid, rid, acceptedrun, nc_meanmedian_response, cutpoint)
GROUP BY studyid--, analytename