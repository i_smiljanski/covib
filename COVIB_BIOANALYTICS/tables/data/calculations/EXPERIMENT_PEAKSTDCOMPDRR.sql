  SELECT XMLELEMENT("experiment", XMLATTRIBUTES('&&Experiment&' AS "name" ,'peakstdcomp' AS "type"),
   (
   select Xmlagg(XMLConcat(
     Xmlagg(XMLELEMENT("calculation", xmlconcat(targ,val,res))),
     XMLELEMENT("statistic", XMLATTRIBUTES(analyteid AS "analyteid", species AS "species", matrix AS "matrix", runid AS "runid", internalstdname AS "internalstdname" ),
       XMLELEMENT("numtotal", sum(cnt)),
       XMLELEMENT("numok", sum(cntok)),
       XMLELEMENT("numokana", sum(ana_cntok)),
       XMLELEMENT("percok", case when sum(cnt) != 0 then FmtNum(round(sum(cntok)/sum(cnt)*100, &&DecPlPercStat&)) else null end),
       XMLELEMENT("percokana",case when sum(anacnt) != 0 then FmtNum(round(sum(ana_cntok)/sum(anacnt)*100, &&DecPlPercStat&)) else null end),
       XMLELEMENT("flagpercentpeakis", min(flagpercentpeakis)),
       XMLELEMENT("flagpercentpeakanalyte", min(flagpercentpeak))
       )) order by analyteid, analyteorder, species, matrix)
  from (
  SELECT
      analyteid, analyteorder, species, matrix, runid, internalstdname,
      Count(case when useanalytearea is not null then 1 else null end) anacnt,
      Count(case when useinternalstandardarea is not null then 1 else null end) cnt,
      Count(case when percent < flagpercentpeak then 1 else null end) ana_cntok,
      Count(case when is_percent < flagpercentpeakis then 1 else null end) cntok,
      min(flagpercentpeakis) flagpercentpeakis, min(flagpercentpeak) flagpercentpeak,
      count(internalstandardarea) cnttotal,
      XMLELEMENT("target",
        XMLELEMENT("analyteid", analyteid),
        XMLELEMENT("species", species),
        XMLELEMENT("matrix", matrix),
        XMLELEMENT("runid", runid),
        XMLELEMENT("internalstdname", internalstdname)
        ) targ,
      XMLELEMENT("values", Xmlagg(
        XMLELEMENT("sample",  XMLATTRIBUTES(sampleid AS "sampleid", sampleresultrawid AS "sampleresultrawid" ),
          --XMLELEMENT("knownid", knownid),
          XMLELEMENT("knownname", knownname),
          XMLELEMENT("name", name),
          XMLELEMENT("exportsamplename", exportsamplename),
          XMLELEMENT("lotnumber", lotnumber),
          XMLELEMENT("runsamplesequencenumber", runsamplesequencenumber),
          XMLELEMENT("peak", &&PeakFormatFunc&(analytearea, &&PeakFigures&)),
          XMLELEMENT("ispeak", &&PeakFormatFunc&(internalstandardarea, &&PeakFigures&)),
          XMLELEMENT("percent", FormatRounded(percent, &&DecPlPercBias&)),
          XMLELEMENT("is-percent", FormatRounded(is_percent, &&DecPlPercBias&)),
          XMLELEMENT("deactivated", isdeactivated)
        )
        order by runsamplesequencenumber)) val,
      XMLELEMENT("result",
        XMLELEMENT("n", Count(useanalytearea)),
        XMLELEMENT("cntok", Count(case when percent < flagpercentpeak then 1 else null end )),
        XMLELEMENT("flagpercentpeakanalyte", min(flagpercentpeak)),
        XMLELEMENT("isn", Count(useinternalstandardarea)),
        XMLELEMENT("iscntok", Count(case when is_percent < flagpercentpeakis then 1 else null end )),
        XMLELEMENT("flagpercentpeakis", min(flagpercentpeakis)),
        XMLELEMENT("std-name", min(std_name)),
        XMLELEMENT("std-peak", &&PeakFormatFunc&(min(std_analytearea), &&PeakFigures&)),
        XMLELEMENT("std-is-peak", &&PeakFormatFunc&(min(std_internalstandardarea), &&PeakFigures&))
      ) res
    FROM
    (
    SELECT --knownID,
           sampleID,  sampleresultrawID, runsamplesequencenumber, &&FlagPercentPeak& FlagPercentPeak, &&FlagPercentPeakIS& FlagPercentPeakIS,
           knownname, lotnumber, name, exportsamplename, runid, analyteid, analyteorder, species, matrix,
           internalstandardarea, useinternalstandardarea, internalstdname, isdeactivated,
           round(useinternalstandardarea/std_internalstandardarea*100, &&DecPlPercBias&) is_percent,
           round(useanalytearea/std_analytearea*100, &&DecPlPercBias&) percent,
           std_internalstandardarea, std_name,
           analytearea, useanalytearea, std_analytearea
    FROM
    (select --knownID,
            sampleID,  sampleresultrawID, runsamplesequencenumber, loc.internalstandardarea, loc.useinternalstandardarea, internalstdname,
            knownname, lotnumber, loc.name, loc.exportsamplename,
            loc.runid, loc.analyteid, loc.analyteorder, loc.species, loc.matrix, isdeactivated,
            avg_internalstandardarea std_internalstandardarea, std.name std_name,
            loc.analytearea, loc.useanalytearea, std.avg_analytearea std_analytearea
     FROM
     (SELECT --K.id knownID,
        s.id sampleID, srw.id sampleresultrawID, s.runsamplesequencenumber, srw.internalstdname,
        s.name, s.oldname exportsamplename, s.knownname knownname, '' lotnumber, s.runid,
        ra.analyteid, sa.analyteorder, a.species, a.sampletypeid matrix, srw.ratio, s.isdeactivated,
        &&PeakRepresentationFunc&(srw.internalstandardarea, &&PeakFigures&) internalstandardarea, -- is not used to calc other values
        case when s.isdeactivated = 'T' then null
             when s.name like '&&HideISLike&' then null
             else case when '&&FinalAccuracy&' = 'T' then srw.internalstandardarea
                       else &&PeakRepresentationFunc&(srw.internalstandardarea, &&PeakFigures&)
                  end
        end useinternalstandardarea,
        &&PeakRepresentationFunc&(srw.analytearea, &&PeakFigures&) analytearea, -- is not used to calc other values
        case when s.isdeactivated = 'T' then null
             when s.name like '&&HideAnalyteLike&' then null
             else case when '&&FinalAccuracy&' = 'T' then srw.analytearea
                       else &&PeakRepresentationFunc&(srw.analytearea, &&PeakFigures&)
                  end
        end useanalytearea
      FROM --&&TempTabPrefix&bio$run$ana$known K,
           &&TempTabPrefix&bio$run r,
           &&TempTabPrefix&bio$run$analytes ra,
           &&TempTabPrefix&bio$study$analytes sa,
           &&TempTabPrefix&bio$assay a,
           &&TempTabPrefix&bio$run$samples s,
           &&TempTabPrefix&bio$run$sample$result$raw srw
           --isr$crit kwz,
           --table(&&LALPackage&.GetRunStates) rs
      WHERE --K.ID = s.knownID
        --AND
        R.ID = s.runid AND R.ID = srw.runid
        --AND r.studyassayid =s.studyassayid AND s.studyassayid = srw.studyassayid
        AND r.runtypedescription in (&&RunTypes&)
        AND s.ID = srw.runsampleID
        AND s.sampletype = 'known'
        AND regexp_like(s.runsamplekind, '&&RunSampleKind&')
        --AND K.knowntype = s.runsamplekind
        --AND kwz.masterkey = '&&ExpMasterkey&'
        AND ra.id = srw.runanalyteid
        AND sa.id = ra.analyteid
        AND a.id = r.assayid
        AND ra.runid = s.runid
        AND ra.studyID = '&&StudyID&'
        --AND K.studyid = '&&StudyID&'
        AND s.studyid = '&&StudyID&'
        AND srw.studyID = '&&StudyID&' AND r.studyid = '&&StudyID&'
        --AND kwz.key = K.nameprefix
        --AND kwz.entity = '&&KnownEntity&'
        --AND repid = &&RepID&
        --AND rs.column_value = r.runstatusnum
        &&AddCondition&
        ) loc,
     (
     SELECT s.runid, ra.analyteid, sa.analyteorder, a.species, a.sampletypeid matrix, s.knownname name,
             round(avg(
               case when s.isdeactivated = 'T' then null
                    else case when '&&FinalAccuracy&' = 'T' then srw.internalstandardarea
                              else &&PeakRepresentationFunc&(srw.internalstandardarea, &&PeakFigures&)
                         end
               end), &&PeakFigures&) avg_INTERNALSTANDARDAREA ,
             round(avg(
               case when s.isdeactivated = 'T' then null
                    else case when '&&FinalAccuracy&' = 'T' then srw.analytearea
                              else &&PeakRepresentationFunc&(srw.analytearea, &&PeakFigures&)
                         end
               end), &&PeakFigures&) avg_analytearea
      FROM --&&TempTabPrefix&bio$run$ana$known K,
           &&TempTabPrefix&bio$run r,
           &&TempTabPrefix&bio$run$analytes ra,
           &&TempTabPrefix&bio$study$analytes sa,
           &&TempTabPrefix&bio$assay a,
           &&TempTabPrefix&bio$run$samples s,
           &&TempTabPrefix&bio$run$sample$results sr,
           &&TempTabPrefix&bio$run$sample$result$raw srw
           --table(&&LALPackage&.GetRunStates) rs
      WHERE
        --K.id = s.knownid
        --AND K.knowntype = s.runsamplekind
        --AND
         srw.runsamplecode = s.code
        AND s.runsamplekind = 'STANDARD'
        AND srw.runsamplecode = s.code
        AND srw.sampleresultcode = sr.code
        AND r.id = s.runid AND r.id = srw.runid
        --AND r.studyassayid = s.studyassayid AND s.studyassayid = srw.studyassayid
        AND r.runtypedescription in (&&RunTypes&)
        AND s.sampletype = 'known'
        AND ra.id = srw.runanalyteid
        AND sa.id = ra.analyteid
        AND a.id = r.assayid
        AND ra.runid = s.runid
        AND ra.studyID = '&&StudyID&'
        AND s.studyid = '&&StudyID&' --AND k.studyid = '&&StudyID&'
        AND srw.studyID = '&&StudyID&' AND r.studyid = '&&StudyID&'
        --AND aa.id = ra.assayanalyteid
        --AND rs.column_value = r.runstatusnum
        AND s.isdeactivated = 'F'
        &&LLOQIdentify&
      group by r.runid, s.RUNID, ra.analyteid, sa.analyteorder, a.species, a.sampletypeid, s.knownname
      ) std
      where std.runid = loc.runid
        AND std.analyteid = loc.analyteid
        AND std.species = loc.species
        AND std.matrix = loc.matrix
    ))
    GROUP BY runid, analyteid, analyteorder, species, matrix, internalstdname)
    GROUP BY analyteid, analyteorder, species, matrix, runid, internalstdname)
    ) FROM dual