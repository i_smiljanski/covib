  SELECT XMLELEMENT("experiment", XMLATTRIBUTES('&&Experiment&' AS "name" ,'pksparse' AS "type"),
    (SELECT xmlagg (XMLELEMENT("calculation", 
      XMLELEMENT("target",         
        XMLELEMENT("treatmentid", treatmentid),   
        XMLELEMENT("matrixkey", matrixkey),   
        XMLELEMENT("period", period),              
        XMLELEMENT("timeident", timeident), 
        XMLELEMENT("analyteid",ana_id),
        XMLELEMENT("gender", gender)),
      XMLELEMENT("values", xmlagg(
        XMLELEMENT("parameter", XMLATTRIBUTES(pkp_id AS "pkparameterid" ), 
          XMLELEMENT("parameter", NAME), 
          XMLELEMENT("parametervalue",case when name='Tmax' then
                              case when &&DecPlTmax& is not null then FormatRounded(round(parametervalue,&&DecPlTmax&),&&DecPlTmax&)
                              else FmtNum(parametervalue) end 
                              else &&ConcFormatFunc&(&&ConcRepresentationFunc&(parametervalue,&&ConcFigures&),&&ConcFigures&) end)                            
        ))),
      XMLELEMENT("result",       
        XMLELEMENT("n", count(name))
       ))) 
    FROM 
     (SELECT P.ID pkp_id, PK.ANALYTEID ana_id, P.NAME, 
         case when stb$util.GetReportParameterValue('MATRIX_GROUP_BY',&&RepID&) = 'PERIOD' 
               then 'X' else sgt.treatmentid end as treatmentid, 
         case when stb$util.GetReportParameterValue('MATRIX_GROUP_BY',&&RepID&) = 'TREATMENT' 
               then 'X' else sgt.period end as period,     
        sgt.timeident, 
        PK.SAMPLETYPEKEY matrixkey,   
        pk.GROUPDESCRIPTION as gender,
        sgt.studyday, P.parametervalue
      FROM &&TempTabPrefix&BIO$PKPARAMETER P,
           &&TempTabPrefix&BIO$PK PK,
           &&TempTabPrefix&BIO$STUDYSUBJ$GRP$TREATM SGT
       WHERE 
        sgt.studyday = PK.studyday
        AND P.PKID = PK.ID
        and sgt.treatmentid=pk.treatmentid
        and upper(pk.GROUPDESCRIPTION) like 'MEAN%' 
        AND PK.STUDYID = '&&StudyID&' AND P.STUDYID = '&&StudyID&' AND SGT.STUDYID = '&&StudyID&'    
      )
      GROUP BY treatmentid, period, timeident, matrixkey, ana_id, gender)    
      ) FROM dual