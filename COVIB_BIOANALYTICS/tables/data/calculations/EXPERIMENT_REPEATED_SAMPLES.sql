  SELECT XMLELEMENT("experiment", XMLATTRIBUTES('S_REASSAY' AS "name" ,'repeatedsamples' AS "type"),
    (SELECT xmlagg (XMLELEMENT("calculation",
      XMLELEMENT("target",
        XMLELEMENT("analyteid", analyteid)),
      XMLELEMENT("values", xmlagg(
        XMLELEMENT("reason",
          XMLELEMENT("reason", reason),
          XMLELEMENT("amount", amount),
          XMLELEMENT("bias", FormatRounded(round(bias, &&DecPlPercBias&), &&DecPlPercBias&)))
        ORDER BY reptype, reason
        )),
      XMLELEMENT("result",
        XMLELEMENT("n", count(reason)),
        XMLELEMENT("total-amount", min(total_amount)),
        XMLELEMENT("analyzed", min(analyzed)),
        XMLELEMENT("bias", FormatRounded(min(total_amount)*100/min(analyzed), &&DecPlPercBias&))
       )
    ) order by analyteid)
    FROM
    (
    select analyteid, analytecode, reptype, reason, amount,
      ds.analyzed, amount*100/ds.analyzed bias,
      sum(amount) over (partition by analyteid) total_amount
    from (
  }    SELECT  a.id analyteid, a.code analytecode, 1 reptype,
        '[RejectedRun]' reason, sampleofrejected amount
      FROM &&TempTabPrefix&bio$study$analytes a
      where a.studyid = '&&StudyID&'
      union all
      select a.id analyteid, a.code analytecode, 2 reptype,
        reason, count(*) amount
      from &&TempTabPrefix&bio$study$analytes a,
           &&TempTabPrefix&bio$deactivated$samples ds
      where a.code = ds.studycode||'-'||ds.analyte
        and a.studyid = '&&StudyID&' and ds.studyid = '&&StudyID&'
        and ds.runanalyteregressionstatus != 4
        and ds.designsampleid is not null
        and ds.runtypedescription != 'MANDATORY REPEATS'
        and ds.reason not in (select display from ISR$PARAMETER$VALUES where Entity = 'BIO$EXPERIMENT_REPEATED_SAMPLES_EXCLUDE')
      group by a.id , a.code, reason
      union all
      select h.analyteid, h.analytecode, 3 reptype,
        '[ReassayHistory]' reason, count(*) amount
      from &&TempTabPrefix&bio$reassay$history h
      where h.studyid = '&&StudyID&'
        and h.originalvalue = 'Y'
      group by h.analyteid, h.analytecode
      ) re,
      (select sum(analyzed) analyzed from &&TempTabPrefix&bio$design$summary
    where studyid = '&&StudyID&') ds
    group by analyteid, analytecode, reptype, reason, amount, analyzed
    )
    GROUP BY analyteid, analytecode)
    ) FROM dual