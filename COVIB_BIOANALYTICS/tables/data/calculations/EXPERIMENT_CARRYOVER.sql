  SELECT XMLELEMENT("experiment", XMLATTRIBUTES('&&Experiment&' AS "name" ,'carryover' AS "type"),
  (SELECT Xmlagg (XMLELEMENT("calculation",
      XMLELEMENT("target",
        XMLELEMENT("analyteid", analyteid),
        XMLELEMENT("species", species),
        XMLELEMENT("matrix", matrix),
        XMLELEMENT("internalstdname", internalstdname),
        XMLELEMENT("runid", runid)),
      XMLELEMENT("values", Xmlagg(
        XMLELEMENT("sample", XMLATTRIBUTES(blank_sampleid AS "blank_sampleid", std_sampleid AS "std_sampleid" ),
          XMLELEMENT("lloq_name", lloq_name),
          XMLELEMENT("blankname", blankname),
          XMLELEMENT("blank-analytearea", &&PeakFormatFunc&(blank_analytearea, &&PeakFigures&)),
          XMLELEMENT("blank-internalstandardarea", &&PeakFormatFunc&(blank_internalstandardarea, &&PeakFigures&)),
          XMLELEMENT("lloq_analytearea", &&PeakFormatFunc&(lloq_analytearea, &&PeakFigures&)),
          XMLELEMENT("lloq_internalstandardarea", &&PeakFormatFunc&(lloq_internalstandardarea, &&PeakFigures&)),
          XMLELEMENT("ratio_analytearea", FormatRounded(ratio_analytearea, &&DecPlPercBias&)),
          XMLELEMENT("ratio_internalstandardarea", FormatRounded(ratio_internalstandardarea, &&DecPlPercBias&))
        ))),
      XMLELEMENT("result",
        XMLELEMENT("n", Count(blank_analytearea))
        )
    ) order by analyteid, analyteorder, species, matrix)
    FROM
    (
    select loc.runid, loc.sampleid blank_sampleid, loc.analyteid, loc.analyteorder, loc.species, loc.matrix, loc.internalstdname, loc.name blankname, loc.runsamplesequencenumber,
           loc.internalstandardarea blank_internalstandardarea, loc.analytearea blank_analytearea,
           std.sampleid std_sampleid, std.name lloq_name, std.internalstandardarea lloq_internalstandardarea, std.analytearea lloq_analytearea,
           round(loc.internalstandardarea/std.internalstandardarea*100,1) ratio_internalstandardarea,
           round(loc.analytearea/std.analytearea*100, 1) ratio_analytearea
    from(
      SELECT s.id sampleid, s.code, s.RUNID, ra.analyteid, sa.analyteorder, a.species, a.sampletypeid matrix, srw.internalstdname, s.name, s.runsamplesequencenumber,
        case when '&&FinalAccuracy&' = 'T' then srw.INTERNALSTANDARDAREA
             else round(srw.INTERNALSTANDARDAREA,0)
        end internalstandardarea,
        case when '&&FinalAccuracy&' = 'T' then srw.analytearea
             else round(srw.analytearea,0)
        end analytearea
      FROM &&TempTabPrefix&bio$run r,
           &&TempTabPrefix&bio$run$analytes ra,
           &&TempTabPrefix&bio$run$samples s,
           &&TempTabPrefix&bio$run$sample$results sr,
           &&TempTabPrefix&bio$run$sample$result$raw srw,
           &&TempTabPrefix&bio$study$analytes sa,
           &&TempTabPrefix&bio$assay a,
           (-- find the blanks preceded by a ULQC within the run. get the lasz of them - not specified but assumed
            select max(b.code) keep (dense_rank last order by b.runsamplesequencenumber) samplecode,
                  max(b.runsamplesequencenumber) runsamplesequencenumber, ul.runsamplesequencenumber ulseqnum, /*b.studyassayid,*/ b.runid, b.runanalyteid, b.analyteorder
            from &&TempTabPrefix&bio$run$samples b,
                (select min(runsamplesequencenumber) runsamplesequencenumber, /*max(code) keep (dense_rank first order by runsamplesequencenumber) code,*/ runid, b.runanalyteid, b.analyteorder
                 /*studyassayid,*/
                 from &&TempTabPrefix&bio$run$samples u
                 where KNOWNNAME LIKE '%ULQC%' AND sampletype = 'known'
                 group by /*studyassayid,*/ runid, runanalyteid, analyteindex) ul
            where --b.studyassayid = ul.studyassayid
              /*and*/ b.runid = ul.runid
              and b.runanalyteid = ul.runanalyteid
              and b.analyteorder = ul.analyteorder
              and b.runsamplekind = 'Blank'
              and b.runsamplesequencenumber > ul.runsamplesequencenumber
            group by /*b.studyassayid,*/ b.runid, b.runanalyteid, b.analyteorder, ul.runsamplesequencenumber) blank,
           table(&&LALPackage&.GetRunStates) rs
      WHERE srw.RUNSAMPLECODE = s.code
        AND s.RUNSAMPLEKIND = 'Blank'
        and sr.RUNSAMPLECODE = s.code
        AND r.ID = sr.runid AND r.ID = srw.runid
        --AND r.studyassayid =s.studyassayid AND s.studyassayid = sr.studyassayid AND s.studyassayid = srw.studyassayid
        AND r.RuntypeDescription in (&&RunTypes&)
        AND s.sampletype = 'known'
        AND ra.id = srw.runanalyteid
        AND sa.id = ra.analyteid
        AND a.id = r.assayid
        AND ra.runid = s.runid
        AND ra.studyID = '&&StudyID&'
        AND s.studyid = '&&StudyID&' AND sr.studyid = '&&StudyID&'
        AND srw.studyID = '&&StudyID&' AND r.studyid = '&&StudyID&'
        AND rs.column_value = r.runstatusnum
        and blank.samplecode = s.code
       )loc,
       (SELECT s.id sampleid, s.RUNID, ra.analyteid, sa.analyteorder, a.species, a.sampletypeid matrix, s.name,
             round(avg(
               case when s.ISDEACTIVATED = 'T' then null
                    else case when '&&FinalAccuracy&' = 'T' then srw.INTERNALSTANDARDAREA
                              else round(srw.INTERNALSTANDARDAREA,0)
                         end
               end),0) INTERNALSTANDARDAREA,
             round(avg(
               case when s.ISDEACTIVATED = 'T' then null
                    else case when '&&FinalAccuracy&' = 'T' then srw.analytearea
                              else round(srw.analytearea,0)
                         end
               end),0) analytearea
      FROM &&TempTabPrefix&bio$run r,
           &&TempTabPrefix&bio$run$analytes ra,
           &&TempTabPrefix&bio$run$samples s,
           &&TempTabPrefix&bio$run$sample$results sr,
           &&TempTabPrefix&bio$run$sample$result$raw srw,
           &&TempTabPrefix&bio$study$analytes sa,
           &&TempTabPrefix&bio$assay a,
           table(&&LALPackage&.GetRunStates) rs
      WHERE srw.RUNSAMPLECODE = s.code
        AND s.RUNSAMPLEKIND = 'STANDARD'
        and sr.RUNSAMPLECODE = s.code
        and srw.sampleresultcode = sr.code
        AND r.ID = sr.runid AND r.ID = srw.runid
        --AND r.studyassayid =s.studyassayid AND s.studyassayid = sr.studyassayid AND s.studyassayid = srw.studyassayid
        AND r.RuntypeDescription in (&&RunTypes&)
        AND s.sampletype = 'known'
        AND ra.id = srw.runanalyteid
        AND sa.id = ra.analyteid
        AND a.id = r.assayid
        AND ra.runid = s.runid
        AND ra.studyID = '&&StudyID&'
        AND s.studyid = '&&StudyID&' AND sr.studyid = '&&StudyID&'
        AND srw.studyID = '&&StudyID&' AND r.studyid = '&&StudyID&'
        --and aa.id = ra.assayanalyteid
        AND rs.column_value = r.runstatusnum
        and round(ra.lloq, 10) = round(sr.nominalconcentration, 10)
        &&AddCondition&
      group by s.id, r.runid, s.RUNID, ra.analyteid, sa.analyteorder, a.species, a.sampletypeid, s.name, analytearea
      )std
      where std.runid = loc.runid
        AND std.analyteid = loc.analyteid
        AND std.analyteorder = loc.analyteorder
        AND std.species = loc.species
        AND std.matrix = loc.matrix
         )
     GROUP BY analyteid, analyteorder, species, matrix, internalstdname, runid)
    ) FROM dual