  SELECT XMLELEMENT("experiment", XMLATTRIBUTES('&&Experiment&' AS "name" ,'selectivityis' AS "type"),
   (
   select Xmlagg(XMLConcat(
     Xmlagg(XMLELEMENT("calculation", xmlconcat(targ,val,res))),
     XMLELEMENT("statistic", XMLATTRIBUTES(analyteid AS "analyteid", species AS "species", matrix AS "matrix", runid AS "runid", internalstdname AS "internalstdname" ),
       XMLELEMENT("numok", sum(cntok)),
       XMLELEMENT("numtotal", sum(cnt)),
       XMLELEMENT("percok", case when sum(cnt) != 0 then FmtNum(round(sum(cntok)/sum(cnt)*100,0)) else null end)
       )) order by analyteid, analyteorder, species, matrix)
  from (
  SELECT
      analyteid, analyteorder, species, matrix, runid, internalstdname,
      Count(case when grouptype = 'WITHOUT IS' then case when useinternalstandardarea is not null then 1 else null end else null end) cnt,
      Count(case when grouptype = 'WITHOUT IS' then case when percent < flagpercentpeak then 1 else null end else null end) cntok,
      count(internalstandardarea) cnttotal,
      XMLELEMENT("target",
        XMLELEMENT("analyteid", analyteid),
        XMLELEMENT("species", species),
        XMLELEMENT("matrix", matrix),
        XMLELEMENT("runid", runid),
        XMLELEMENT("internalstdname", internalstdname),
        XMLELEMENT("grouptype", grouptype)
        ) targ,
      XMLELEMENT("values", Xmlagg(
        XMLELEMENT("sample",  XMLATTRIBUTES(sampleid AS "sampleid", sampleresultrawid AS "sampleresultrawid" ),
          XMLELEMENT("knownid", knownid),
          XMLELEMENT("name", case when grouptype = 'WITHOUT IS' then name else is_name end ),
          XMLELEMENT("exportsamplename", case when grouptype = 'WITHOUT IS' then exportsamplename else is_exportsamplename end ),
          XMLELEMENT("lotnumber", lotnumber),
          XMLELEMENT("ispeak", &&PeakFormatFunc&(case when grouptype = 'WITHOUT IS' then useinternalstandardarea else is_internalstandardarea end, &&PeakFigures&)),
          XMLELEMENT("runsamplesequencenumber", runsamplesequencenumber),
          XMLELEMENT("mean-lloq", &&PeakFormatFunc&(mean_lloq, &&PeakFigures&)),
          XMLELEMENT("percent", FormatRounded(percent, &&DecPlPercBias&)),
          XMLELEMENT("deactivated", isdeactivated)
        )
        order by runsamplesequencenumber)) val,
      XMLELEMENT("result",
        XMLELEMENT("n", Count(useinternalstandardarea)),
        XMLELEMENT("cntok", Count(case when grouptype = 'WITHOUT IS' then case when percent < flagpercentpeak then 1 else null end else null end)),
        XMLELEMENT("flagpercentpeak", min(flagpercentpeak))
      ) res
    FROM
    (
    SELECT knownID, sampleID,  sampleresultrawID, runsamplesequencenumber, &&FlagPercentPeak& FlagPercentPeak,
           lotnumber, NAME, exportsamplename, runid, analyteid, analyteorder, species, matrix, grouptype, internalstandardarea, useinternalstandardarea, internalstdname, isdeactivated, mean_lloq,
           case when grouptype = 'WITHOUT IS' then round(useinternalstandardarea/(min(is_internalstandardarea) over (partition by runid, analyteid, analyteorder, species, matrix))*100,1)
                else null end percent,
           is_internalstandardarea, is_name, is_exportsamplename
    FROM
    (select knownID, sampleID,  sampleresultrawID, runsamplesequencenumber, loc.internalstandardarea, loc.useinternalstandardarea, internalstdname,
            /*Stb$util.getRightDelim(loc.name,'_')*/ lotnumber, loc.NAME, loc.exportsamplename,
            loc.runid, loc.analyteid, loc.analyteorder, loc.species, loc.matrix, grouptype, isdeactivated,
            case when grouptype = 'WITH IS' then
                   case when 'F' = 'T' then avg(avg_internalstandardarea) over (partition by loc.runid, loc.analyteid, loc.analyteorder, loc.species, loc.matrix, grouptype)
                        else &&PeakRepresentationFunc&(avg(avg_internalstandardarea) over (partition by loc.runid, loc.analyteid, loc.analyteorder, loc.species, loc.matrix, grouptype), &&PeakFigures&)
                   end
                 else null
            end mean_lloq,
            case when grouptype = 'WITH IS' then avg_internalstandardarea else null end is_internalstandardarea,
            case when grouptype = 'WITH IS' then std.name else null end is_name,
            case when grouptype = 'WITH IS' then std.exportsamplename else null end is_exportsamplename
     FROM
     (SELECT K.id knownid, s.id sampleid, srw.id sampleresultrawid, s.runsamplesequencenumber, srw.internalstdname,
        s.NAME, s.oldname exportsamplename, k.namesuffix lotnumber, s.runid, ra.analyteid, sa.analyteorder, a.species, a.sampletypeid matrix, srw.ratio, s.isdeactivated,
        case when k.name like '&&LLOQLike&' then 'WITH IS' --SEQC.LL%
             else 'WITHOUT IS' end grouptype,
        &&PeakRepresentationFunc&(srw.internalstandardarea, &&PeakFigures&) internalstandardarea, -- is not used to calc other values
        case when s.ISDEACTIVATED = 'T' then null
             else case when '&&FinalAccuracy&' = 'T' then srw.internalstandardarea
                       else &&PeakRepresentationFunc&(srw.internalstandardarea, &&PeakFigures&)
                  end
        end useinternalstandardarea
      FROM &&TempTabPrefix&bio$run$ana$known K,
           &&TempTabPrefix&bio$run r,
           &&TempTabPrefix&bio$run$analytes ra,
           &&TempTabPrefix&bio$study$analytes sa,
           &&TempTabPrefix&bio$assay a,
           &&TempTabPrefix&bio$run$samples s,
           &&TempTabPrefix&bio$run$sample$result$raw srw,
           isr$crit kwz,
           table(&&LALPackage&.GetRunStates) rs
      WHERE K.id = s.runknownid
        AND R.id = s.runid AND R.id = srw.runid
        --AND r.studyassayid =s.studyassayid AND s.studyassayid = srw.studyassayid
        AND r.RuntypeDescription in (&&RunTypes&)
        AND s.id = srw.runsampleID
        AND s.sampletype = 'known'
        AND K.knowntype = s.runsamplekind
        and kwz.masterkey = '&&ExpMasterkey&'
        AND ra.id = srw.runanalyteid
        AND sa.id = ra.analyteid
        AND a.id = r.assayid
        AND ra.runid = s.runid
        AND ra.studyID = '&&StudyID&'
        AND K.studyid = '&&StudyID&' AND s.studyid = '&&StudyID&'
        AND srw.studyID = '&&StudyID&' AND r.studyid = '&&StudyID&'
        and kwz.key = K.nameprefix
        and kwz.entity = '&&KnownEntity&'
        and repid = &&RepID&
        AND rs.column_value = r.runstatusnum
        &&AddCondition&
        ) loc,
     (SELECT s.runid, ra.analyteid, sa.analyteorder, a.species, a.sampletypeid matrix, s.name, s.oldname exportsamplename,
             round(avg(
               case when s.isdeactivated = 'T' then null
                    else case when 'F' = 'T' then srw.internalstandardarea
                              else &&PeakRepresentationFunc&(srw.internalstandardarea, &&PeakFigures&)
                         end
               end), &&PeakFigures&) avg_INTERNALSTANDARDAREA
      FROM &&TempTabPrefix&bio$run r,
           &&TempTabPrefix&bio$run$analytes ra,
           &&TempTabPrefix&bio$study$analytes sa,
           &&TempTabPrefix&bio$assay a,
           &&TempTabPrefix&bio$run$samples s,
           &&TempTabPrefix&bio$run$sample$results sr,
           &&TempTabPrefix&bio$run$sample$result$raw srw,
           table(&&LALPackage&.GetRunStates) rs
      WHERE srw.runsamplecode = s.code
        AND s.runsamplekind = 'STANDARD'
        and sr.runsamplecode = s.code
        and srw.sampleresultcode = sr.code
        AND r.id = sr.runid AND r.ID = srw.runid
        --AND r.studyassayid =s.studyassayid AND s.studyassayid = sr.studyassayid AND s.studyassayid = srw.studyassayid
        AND r.runtypedescription in (&&RunTypes&)
        AND s.sampletype = 'known'
        AND ra.id = srw.runanalyteid
        AND sa.id = ra.analyteid
        AND a.id = r.assayid
        AND ra.runid = s.runid
        AND ra.studyid = '&&StudyID&'
        AND s.studyid = '&&StudyID&' AND sr.studyid = '&&StudyID&'
        AND srw.studyid = '&&StudyID&' AND r.studyid = '&&StudyID&'
        --and aa.id = ra.assayanalyteid
        AND rs.column_value = r.runstatusnum
        and s.isdeactivated = 'F'
        and round(ra.lloq, 10) = round(sr.nominalconcentration, 10)
      group by r.runid, s.runid, ra.analyteid, sa.analyteorder, a.species, a.sampletypeid, s.name, s.oldname) std
      where std.runid = loc.runid
        AND std.analyteid = loc.analyteid
        AND std.species = loc.species
        AND std.matrix = loc.matrix
    ))
    GROUP BY runid, analyteid, analyteorder, species, matrix, grouptype, internalstdname)
    GROUP BY analyteid, analyteorder, species, matrix, runid, internalstdname)
    ) FROM dual