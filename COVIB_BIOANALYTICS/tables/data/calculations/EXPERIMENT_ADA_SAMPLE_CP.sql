SELECT XMLELEMENT("experiment", XMLATTRIBUTES('&&Experiment&' AS "name" ,'cpresponse' AS "type"),
        Xmlagg ( 
          Xmlagg (XMLELEMENT("calculation", xmlconcat(targ,val,res)) order by /*analyteorder, */acceptedrun, meanormedian, concentrationunit) order by studyid
       ))
FROM (
  SELECT studyid, /*analyteid, analyteorder, */acceptedrun, concentrationunit, meanormedian,
    XMLELEMENT("target",
      XMLELEMENT("meanormedian", meanormedian),
      XMLELEMENT("acceptedrun", acceptedrun),
      /*XMLELEMENT("group", posgroup),
      XMLELEMENT("source", case
                             when LENGTH(TRIM(TRANSLATE(source, ' +-.0123456789', ' '))) is null then
                               &&FormatFuncResponse&(&&RepresentationFuncResponse&(source, &&ConcFigures&), &&ConcFigures&)
                             else
                               to_char(source)
                           end),*/
      XMLELEMENT("concentrationunit", concentrationunit)
    ) as targ,
    XMLELEMENT("values", 
      Xmlagg(
        XMLELEMENT("run",
          XMLATTRIBUTES(rid AS "runid", runid AS "runno" ),
          XMLELEMENT("samples", xml),
          XMLELEMENT("analyteid", analyteid),
          XMLELEMENT("nc-mean-median", &&FormatFuncResponse&(&&RepresentationFuncResponse&(nc_meanmedian_response, &&PlacesResponse&), &&PlacesResponse&)),
          XMLELEMENT("cutpoint", &&FormatFuncResponse&(&&RepresentationFuncResponse&(cutpoint, &&PlacesResponse&), &&PlacesResponse&)),
          XMLELEMENT("cutpoint-limit", cutpointlimit),
          XMLELEMENT("cv", &&FormatFuncPercentage&(&&RepresentationFuncPercentage&(cv, &&PlacesPercentage&), &&PlacesPercentage&)),
          XMLELEMENT("flagcv", case when cv > cvlimit then 'T' else 'F' end),
          XMLELEMENT("n", n),
          XMLELEMENT("m", m),
          XMLELEMENT("cvlimit", cvlimit),
          XMLELEMENT("run-cpresponse-mean", &&FormatFuncResponse&(&&RepresentationFuncResponse&(run_cp_mean_useresponse, &&PlacesResponse&), &&PlacesResponse&)),
          XMLELEMENT("run-cpresponse-sd", &&FormatFuncResponseSD&(&&RepresentationFuncResponseSD&(run_cp_sd, &&PlacesResponseSD&), &&PlacesResponseSD&)),
          XMLELEMENT("run-cpresponse-cv", &&FormatFuncPercentage&(&&RepresentationFuncPercentage&(run_cp_sd/nullif(run_cp_mean_useresponse, 0)*100, &&PlacesPercentage&), &&PlacesPercentage&)),
          XMLELEMENT("run-ncresponse-mean", &&FormatFuncResponse&(&&RepresentationFuncResponse&(run_nc_mean_useresponse, &&PlacesResponse&), &&PlacesResponse&)),
          XMLELEMENT("run-ncresponse-sd", &&FormatFuncResponseSD&(&&RepresentationFuncResponseSD&(run_nc_sd, &&PlacesResponseSD&), &&PlacesResponseSD&)),
          XMLELEMENT("run-ncresponse-cv", &&FormatFuncPercentage&(&&RepresentationFuncPercentage&(run_nc_sd/nullif(run_nc_mean_useresponse, 0)*100, &&PlacesPercentage&), &&PlacesPercentage&))
        ) order by runid
      )
    ) as val,
    XMLELEMENT("result",
      XMLELEMENT("n", Sum(n)),
      XMLELEMENT("m", Sum(m)),
      XMLELEMENT("nc-n", Min(study_response_n)),
      XMLELEMENT("nc-mean", &&FormatFuncResponse&(&&RepresentationFuncResponse&(Min(study_response_mean), &&PlacesResponse&), &&PlacesResponse&)),
      XMLELEMENT("nc-sd", &&FormatFuncResponseSD&(&&RepresentationFuncResponseSD&(Min(study_response_sd), &&PlacesResponseSD&), &&PlacesResponseSD&)),
      XMLELEMENT("nc-cv", &&FormatFuncPercentage&(&&RepresentationFuncPercentage&(Min(study_response_sd)/nullif(Min(study_response_mean), 0)*100, &&PlacesPercentage&), &&PlacesPercentage&)),
      XMLELEMENT("cp-n", Min(study_cp_n)),
      XMLELEMENT("cp-mean", &&FormatFuncResponse&(&&RepresentationFuncResponse&(Min(study_cp_mean_useresponse), &&PlacesResponse&), &&PlacesResponse&)),
      XMLELEMENT("cp-sd", &&FormatFuncResponseSD&(&&RepresentationFuncResponseSD&(Min(study_cp_sd), &&PlacesResponseSD&), &&PlacesResponseSD&)),
      XMLELEMENT("cp-cv", &&FormatFuncPercentage&(&&RepresentationFuncPercentage&(Min(study_cp_sd)/nullif(Min(study_cp_mean_useresponse),0)*100, &&PlacesPercentage&), &&PlacesPercentage&))
    ) as res
  FROM (
    SELECT
      studyid, rid, runid, analyteid, analyteorder, acceptedrun, concentrationunit, cutpointlimit, cvlimit, meanormedian,
      nc_meanmedian_response, cutpoint, cv, n, m, xml,
      Avg(cutpoint) over (partition by studyid, concentrationunit, rid, runid, meanormedian, acceptedrun) run_cp_mean_useresponse,
      Stddev(cutpoint) over (partition by studyid, concentrationunit, rid, runid, meanormedian, acceptedrun) run_cp_sd,
      Count(cutpoint) over (partition by studyid, concentrationunit, meanormedian, acceptedrun) study_cp_n,
      Avg(cutpoint) over (partition by studyid, concentrationunit, meanormedian, acceptedrun) study_cp_mean_useresponse,
      Stddev(cutpoint) over (partition by studyid, concentrationunit, meanormedian, acceptedrun) study_cp_sd,
      Avg(nc_meanmedian_response) over (partition by studyid, concentrationunit, rid, runid, meanormedian, acceptedrun) run_nc_mean_useresponse,
      Stddev(nc_meanmedian_response) over (partition by studyid, concentrationunit, rid, runid, meanormedian, acceptedrun) run_nc_sd,
      study_response_mean, study_response_sd, study_response_n
    FROM (
    SELECT
      studyid,
      rid,
      runid,
      analyteid,
      analyteorder,
      acceptedrun,
      concentrationunit,
      cutpointlimit,
      flagpercent cvlimit,
      meanormedian,
      min(study_response_mean) study_response_mean,
      min(study_response_sd) study_response_sd,
      min(study_response_n) study_response_n,
      case 
        when meanormedian = 'Mean' then Avg(useresponse)
        when meanormedian = 'Median' then Median(useresponse)
        when meanormedian = 'Mean of Means' then Avg(case when replicatenumber = 1 then meanuseresponse else null end)
        else Avg(useresponse)
      end nc_meanmedian_response,
      case
        when min(cutpointmethod) = -1 then min(cutpointfactor)
        else
          case
            when min(cutpointmethod) = 3 then min(cutpointoffset) * Stddev(useresponse) else 0
          end + case
            when min(cutpointmethod) = 1 then min(cutpointoffset) else 0
          end + case
            when min(cutpointmethod) = 2 then min(cutpointoffset)
            when meanormedian = 'Mean' then Avg(useresponse)
            when meanormedian = 'Median' then Median(useresponse)
            when meanormedian = 'Mean of Means' then Avg(case when replicatenumber = 1 then meanuseresponse else null end)
            else Avg(useresponse) * min(cutpointfactor)
          end * case
            when min(cutpointmethod) = 0 then min(cutpointfactor) else 1
          end
      end cutpoint,
      case 
        when meanormedian = 'Mean' then Stddev(useresponse)/nullif(Avg(useresponse),0)*100
        when meanormedian = 'Median' then Stddev(useresponse)/nullif(Median(useresponse),0)*100
        when meanormedian = 'Mean of Means' then Stddev(case when replicatenumber = 1 then meanuseresponse else null end)/nullif(Avg(case when replicatenumber = 1 then meanuseresponse else null end),0)*100
        else Avg(case when replicatenumber = 1 then meanuseresponse else null end)
      end cv,
      /*case 
        when meanormedian = 'Mean of Means' then count(case when(isdeactivated='F' and replicatenumber = 1) then 1 else null end)
        else count(case when(isdeactivated='F') then 1 else null end)
      end n,*/
      count(case when(isdeactivated='F') then 1 else null end) n,
      count(1) m,
      Xmlagg(
          XMLELEMENT("sample",
            XMLATTRIBUTES(sampleid AS "sampleid", sampleresultid AS "sampleresultid" ),
            XMLELEMENT("name", name),
            XMLELEMENT("samplesubtype", samplesubtype),
            XMLELEMENT("replicatenumber", replicatenumber),
            XMLELEMENT("response", &&FormatFuncResponse&(&&RepresentationFuncResponse&(response, &&PlacesResponse&), &&PlacesResponse&)),
            XMLELEMENT("meanuseresponse", &&FormatFuncResponse&(&&RepresentationFuncResponse&(meanuseresponse, &&PlacesResponse&), &&PlacesResponse&)),
            XMLELEMENT("status", &&resulttext&),
            XMLELEMENT("deactivated", isdeactivated),
            XMLELEMENT("reason", reason)
          ) order by runsamplesequencenumber
        ) xml
      FROM (
        SELECT
          r.studyid,
          r.id rid,
          r.runid,
          sa.id analyteid,
          sa.analyteorder,
          s.id sampleid,
          sr.id sampleresultid,
          s.samplename name,
          s.samplesubtype,
          s.runsamplesequencenumber,
          s.replicatenumber,
          sr.concentrationunits concentrationunit,
          sr.resulttext,
          sr.analytearea response,
          Avg(case when(ds.code is null) then sr.analytearea else null end) over (partition by r.studyid, sa.id, r.id, s.samplename) meanuseresponse,
          Avg(case when(ds.code is null) then sr.analytearea else null end) over (partition by r.studyid, a.meanormedian, sr.concentrationunits, ra.RunAnalyteRegressionStatus) study_response_mean,
          Stddev(case when(ds.code is null) then sr.analytearea else null end) over (partition by r.studyid, a.meanormedian, sr.concentrationunits, ra.RunAnalyteRegressionStatus) study_response_sd,
          Count(case when(ds.code is null) then sr.analytearea else null end) over (partition by r.studyid, a.meanormedian, sr.concentrationunits, ra.RunAnalyteRegressionStatus) study_response_n,
          a.meanormedian,
          case
            when '&&ScreenCTRLCutPoint&' is not null then to_number('&&ScreenCTRLCutPoint&')
            when '&&ScreenCTRLnFactor&' is not null then to_number('&&ScreenCTRLnFactor&')
            else a.cutpointfactor
          end cutpointfactor,
          a.cutpointoffset,
          case
            when '&&ScreenCTRLCutPoint&' is not null then -1
            when '&&ScreenCTRLnFactor&' is not null then 0
            else a.cutpointmethod
          end cutpointmethod,
          a.cutpointlimit,
          case when(ds.code is null) then sr.analytearea else null end useresponse,
          case
            when ds.code is not null then 'T'
            else 'F'
          end isdeactivated,
          ds.reason,
          case when ra.RunAnalyteRegressionStatus = &&RejectedRunAnalyte& then 'F' else 'T' end acceptedrun,
          s.flagpercent
        FROM 
          &&TempTabPrefix&bio$run r,
          &&TempTabPrefix&bio$run$analytes ra,
          &&TempTabPrefix&bio$study$analytes sa,
          &&TempTabPrefix&bio$assay a,
          &&TempTabPrefix&bio$run$worklist s,
          &&TempTabPrefix&bio$run$worklist$result$rw sr,
          &&TempTabPrefix&bio$deactivated$samples ds,
          table(&&LALPackage&.GetRunStates) rs
        WHERE r.ID = s.runID
        AND s.ID = sr.worklistID
        AND a.ID = r.assayID
        --AND s.treatmentid = 'ADA'
        --AND s.studyid = '&&StudyID&' AND sr.studyID = '&&StudyID&'
        AND r.studyid = '&&StudyID&'
        AND a.studyID = '&&StudyID&'
        AND rs.column_value = r.runstatusnum
        AND sr.runAnalyteCode = ra.code
        AND r.ID = ra.runID
        AND ra.analyteID = sa.ID
        AND s.runcode || '-' || s.runsamplesequencenumber || '-' || sa.analyteorder = ds.code (+)
        AND s.runsamplekind = '&&NCKnownType&'
        &&AddCondition&
      )
    GROUP BY studyid, analyteid, analyteorder, rid, runid, acceptedrun, concentrationunit, meanormedian, cutpointlimit, meanormedian, flagpercent
  ))
  GROUP BY studyid, acceptedrun, /*analyteid, analyteorder, */meanormedian, concentrationunit)
GROUP BY studyid
