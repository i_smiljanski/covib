SELECT XMLELEMENT("experiment", XMLATTRIBUTES('&&Experiment&' AS "name" ,'responsesample' AS "type"), --EXPERIMENT_ADA_RESPONSE_PAIR
      --Xmlagg ( XMLConcat(
        Xmlagg (XMLELEMENT("calculation", xmlconcat(targ,val,res)) order by studyid, type, acceptedrun, analyteid, species, matrix, concentrationunit, levelorder, nominalconc, name, stabilityorder, hours, longtermunits, longtermtime, temperature, dilution, concentrationunit, nominalconc, stability)
        --XMLELEMENT("statistic", XMLATTRIBUTES(analyteid AS "analyteid"),
               
             --)) order by studyid, analyteid, species, matrix, hours, longtermunits, longtermtime, temperature
      )--)
  FROM (
  SELECT
    studyid, type, analyteid, species, matrix, hours, longtermunits, longtermtime, temperature, acceptedrun, name, nominalconc, concentrationunit, dilution, stability, stabilityorder,
    to_number(case
      when name = 'NC' then 0
      when name = 'LPC' then 1
      when name = 'MPC' then 2
      when name = 'HPC' then 3
      else null
    end) levelorder,
    --sum(s1_n)+sum(s2_n) n, sum(s1_m)+sum(s2_m) m,
    XMLELEMENT("target",
      XMLELEMENT("studyid", studyid),
      XMLELEMENT("type", type),
      XMLELEMENT("analyteid", analyteid),
      XMLELEMENT("species", species),
      XMLELEMENT("matrix", matrix),
      XMLELEMENT("dilution", dilution),
      XMLELEMENT("runid", rid),
      XMLELEMENT("runno", runid),
      XMLELEMENT("acceptedrun", acceptedrun),
      XMLELEMENT("hours", hours),
      XMLELEMENT("longtermunits", longtermunits),
      XMLELEMENT("longtermtime", longtermtime),
      XMLELEMENT("temperature", temperature),
      XMLELEMENT("stability", stability),
      XMLELEMENT("conc-name", name),
      XMLELEMENT("nominalconc", &&FormatFuncConc&(&&RepresentationFuncConc&(nominalconc, &&PlacesConc&), &&PlacesConc&)),
      XMLELEMENT("conc-unit", concentrationunit)
    ) as targ,
    XMLELEMENT("values",
      Xmlagg(
        XMLELEMENT("sample",
          XMLELEMENT("subject", subject),
          XMLELEMENT("sample1", s1_xml),
          XMLELEMENT("sample2", s2_xml),
          XMLELEMENT("nc", nc_xml),
          XMLELEMENT("base-mean", FormatRounded(round(base_mean_useresponse, &&DecPlResponse&), &&DecPlResponse&)),
          XMLELEMENT("s1-mean", FormatRounded(round(s1_mean_useresponse, &&DecPlResponse&), &&DecPlResponse&)),
          XMLELEMENT("s2-mean", FormatRounded(round(s2_mean_useresponse, &&DecPlResponse&), &&DecPlResponse&)),
          XMLELEMENT("nc-meanmedian", FormatRounded(round(nc_meanmedian_useresponse, &&DecPlResponse&), &&DecPlResponse&)),
          XMLELEMENT("screen-cp", FormatRounded(round(screen_cp, &&DecPlCutPoint&), &&DecPlCutPoint&)),
          XMLELEMENT("confirm-cp", FormatRounded(round(confirm_cp, &&DecPlConfirmCutPoint&), &&DecPlConfirmCutPoint&)),
          XMLELEMENT("titer-cp",
            case
              when '&&FixedTiterCutpoint&' is not null then 
                FormatRounded(round(to_number('&&FixedTiterCutpoint&'), &&DecPlCutPoint&), &&DecPlCutPoint&)
              when '&&TiterCPNFactor&' is not null then
                FormatRounded(round(nc_meanmedian_useresponse*to_number('&&TiterCPNFactor&'), &&DecPlCutPoint&), &&DecPlCutPoint&)
              else null
            end
          ),
          XMLELEMENT("n", s1_n+s2_n),
          XMLELEMENT("s1-n", s1_n),
          XMLELEMENT("s2-n", s2_n),
          XMLELEMENT("m", s1_m+s2_m),
          XMLELEMENT("s1-m", s1_m),
          XMLELEMENT("s2-m", s2_m),
          XMLELEMENT("nc-n", nc_n),
          XMLELEMENT("nc-m", nc_m),
          XMLELEMENT("s1-cv", FormatRounded(round(s1_cv, &&DecPlPercCV&), &&DecPlPercCV&)),
          XMLELEMENT("s2-cv", FormatRounded(round(s2_cv, &&DecPlPercCV&), &&DecPlPercCV&)),
          XMLELEMENT("nc-cv", FormatRounded(round(nc_cv, &&DecPlPercCV&), &&DecPlPercCV&)),
          XMLELEMENT("s1-cvflag", 
            case
              when s1_cv > &&PrecisionAcceptanceCriteria& then 'T'
              else 'F'
            end
          ),
          XMLELEMENT("s2-cvflag",
            case
              when s2_cv > &&PrecisionAcceptanceCriteria& then 'T'
              else 'F'
            end
          ),
          XMLELEMENT("s1-ratio", FormatRounded(round(s1_ratio, &&DecPlRatio&), &&DecPlRatio&)),
          XMLELEMENT("difference", FormatRounded(round(
            (s1_mean_useresponse-s2_mean_useresponse) / nullif(s1_mean_useresponse,0)*100
          , &&DecPlInhibition&), &&DecPlInhibition&)),
          XMLELEMENT("inhibition", FormatRounded(round(abs(
            (s1_mean_useresponse-s2_mean_useresponse) / nullif(s1_mean_useresponse,0)*100
          ), &&DecPlInhibition&), &&DecPlInhibition&)),
          XMLELEMENT("s2-confirm-result",
            case
              when s2_mean_useresponse is null or confirm_cp is null then null
              else
                'Confirmed ' ||
                case
                  when (s1_mean_useresponse-s2_mean_useresponse)/nullif(s1_mean_useresponse,0)*100 >= confirm_cp then 'Positive'
                  else 'Negative'
                end
            end
          ),
          XMLELEMENT("s1-sst-above", case when mean_plus_xsd is null then 'F' when s1_mean_useresponse > mean_plus_xsd then 'T' else 'F' end),
          XMLELEMENT("s1-sst-below", case when mean_minus_xsd is null then 'F' when s1_mean_useresponse < mean_minus_xsd then 'T' else 'F' end),
          XMLELEMENT("ratio-sst-above", case when ratio_plus_xsd is null then 'F' when s1_ratio > ratio_plus_xsd then 'T' else 'F' end),
          XMLELEMENT("ratio-sst-below", case when ratio_minus_xsd is null then 'F' when s1_ratio < ratio_minus_xsd then 'T' else 'F' end)
          --XMLELEMENT("status", resulttext),
          --XMLELEMENT("deactivated", isdeactivated)
        ) order by subject, runid
      )
    ) as val,
    XMLELEMENT("result", 
      XMLELEMENT("s1-mean", FormatRounded(round(Avg(s1_mean_useresponse), &&DecPlResponse&), &&DecPlResponse&)),
      XMLELEMENT("s1-sd", &&FormatFuncResponseSD&(&&RepresentationFuncResponseSD&(Stddev(s1_mean_useresponse), &&PlacesResponseSD&), &&PlacesResponseSD&)),
      XMLELEMENT("s1-cv", FormatRounded(round(Stddev(s1_mean_useresponse) / nullif(Avg(s1_mean_useresponse),0) * 100, &&DecPlPercCV&), &&DecPlPercCV&)),
      XMLELEMENT("s1-ratio-mean", FormatRounded(round(Avg(s1_ratio), &&DecPlRatio&), &&DecPlRatio&)),
      XMLELEMENT("s1-ratio-sd", FormatRounded(round(Stddev(s1_ratio), &&DecPlRatio&), &&DecPlRatio&)),
      XMLELEMENT("s1-ratio-cv", FormatRounded(round(Stddev(s1_ratio) / nullif(Avg(s1_ratio),0) * 100, &&DecPlRatio&), &&DecPlRatio&)),
      XMLELEMENT("s1-sst-above", case when mean_plus_xsd is null then 'F' when Avg(s1_mean_useresponse) > mean_plus_xsd then 'T' else 'F' end),
      XMLELEMENT("s1-sst-below", case when mean_minus_xsd is null then 'F' when Avg(s1_mean_useresponse) < mean_minus_xsd then 'T' else 'F' end),
      XMLELEMENT("ratio-sst-above", case when ratio_plus_xsd is null then 'F' when Avg(s1_ratio) > ratio_plus_xsd then 'T' else 'F' end),
      XMLELEMENT("ratio-sst-below", case when ratio_minus_xsd is null then 'F' when Avg(s1_ratio) < ratio_minus_xsd then 'T' else 'F' end),
      case when mean_plus_xsd is not null then
          XMLELEMENT("mean-plus-xsd", mean_plus_xsd)
      else null end,
      case when mean_minus_xsd is not null then
          XMLELEMENT("mean-minus-xsd", mean_minus_xsd)
      else null end,
      case when ratio_plus_xsd is not null then
          XMLELEMENT("ratio-plus-xsd", ratio_plus_xsd)
      else null end,
      case when ratio_minus_xsd is not null then
          XMLELEMENT("ratio-minus-xsd", ratio_minus_xsd)
      else null end,
      XMLELEMENT("s2-mean", FormatRounded(round(Avg(s2_mean_useresponse), &&DecPlResponse&), &&DecPlResponse&)),
      XMLELEMENT("s2-sd", &&FormatFuncResponseSD&(&&RepresentationFuncResponseSD&(Stddev(s2_mean_useresponse), &&PlacesResponseSD&), &&PlacesResponseSD&)),
      XMLELEMENT("s2-cv", FormatRounded(round(Stddev(s2_mean_useresponse) / nullif(Avg(s2_mean_useresponse),0) * 100, &&DecPlPercCV&), &&DecPlPercCV&)),
      XMLELEMENT("s2-ratio-mean", FormatRounded(round(Avg(s2_ratio), &&DecPlRatio&), &&DecPlRatio&)),
      XMLELEMENT("s2-ratio-sd", FormatRounded(round(Stddev(s2_ratio), &&DecPlRatio&), &&DecPlRatio&)),
      XMLELEMENT("s2-ratio-cv", FormatRounded(round(Stddev(s2_ratio) / nullif(Avg(s2_ratio),0) * 100, &&DecPlRatio&), &&DecPlRatio&)),
      XMLELEMENT("base-mean", FormatRounded(round(Avg(base_mean_useresponse), &&DecPlResponse&), &&DecPlResponse&)),
      XMLELEMENT("diff-base", 
        case
          when max(s1_is_base) != 'T' then
            FormatRounded(round(
              (Avg(s1_mean_useresponse) - Avg(base_mean_useresponse)) / nullif(Avg(base_mean_useresponse),0)*100
            , &&DecPlPercDiff&), &&DecPlPercDiff&)
          else null
        end),
      XMLELEMENT("ratio-diff-base",
        case
          when max(s1_is_base) != 'T' then
            FormatRounded(round(
              (Avg(s1_ratio) - Avg(base_ratio)) / nullif(Avg(base_ratio),0)*100
            , &&DecPlPercDiff&), &&DecPlPercDiff&)
          else null
        end)
    ) as res
  FROM (
    SELECT
      studyid, analyteid, rid, runid, acceptedrun, subject, nominalconc, dilution, concentrationunit,
      species, matrix, hours, longtermunits, longtermtime, temperature, name, type,
      stability, stabilityorder, screen_cp, confirm_cp,
      s1_mean_response, s1_mean_useresponse,
      s1_n, s1_m, s1_sd,
      s1_sd/nullif(s1_mean_useresponse,0)*100 s1_cv,
      s1_mean_useresponse / nullif(nc_meanmedian_useresponse,0) s1_ratio,
      s1_xml,
      s2_mean_response, s2_mean_useresponse,
      s2_n, s2_m, s2_sd,
      s2_sd/nullif(s2_mean_useresponse,0)*100 s2_cv,
      s2_mean_useresponse / nullif(nc_meanmedian_useresponse,0) s2_ratio,
      s2_xml,
      nc_meanmedian_response, nc_meanmedian_useresponse,
      nc_n, nc_m, nc_sd,
      nc_sd/nullif(nc_meanmedian_useresponse,0)*100 nc_cv, nc_xml,
      s1_is_base, base_mean_useresponse,
      base_mean_useresponse / nullif(nc_meanmedian_useresponse,0) base_ratio,
      mean_plus_xsd, mean_minus_xsd,
      ratio_plus_xsd, ratio_minus_xsd
    FROM (
      SELECT
        sample1.studyid, sample1.analyteid, sample1.rid, sample1.runid, sample1.acceptedrun, sample1.subject, sample1.nominalconc, sample1.dilution, sample1.concentrationunit,
        sample1.species, sample1.matrix, sample1.hours, sample1.longtermunits, sample1.longtermtime, sample1.temperature, sample1.name, sample1.type,
        sample1.stability, sample1.stabilityorder, nc.screen_cp,
        case
          when regexp_like(ccp.key, '^\-?(\d+)?\.?\d+$') then to_number(ccp.key)
          else null
        end confirm_cp,
        sample1.mean_response s1_mean_response, sample1.mean_useresponse s1_mean_useresponse,
        sample1.n s1_n, sample1.m s1_m, sample1.sd s1_sd, sample1.xml s1_xml,
        sample2.mean_response s2_mean_response, sample2.mean_useresponse s2_mean_useresponse,
        sample2.n s2_n, sample2.m s2_m, sample2.sd s2_sd, sample2.xml s2_xml,
        NC.meanmedian_response nc_meanmedian_response, NC.meanmedian_useresponse nc_meanmedian_useresponse,
        NC.n nc_n, NC.m nc_m, NC.sd nc_sd, NC.xml nc_xml,
        sample1.is_base s1_is_base,
        max(case when sample1.is_base = 'T' then sample1.mean_useresponse else null end) over (partition by sample1.studyid, sample1.analyteid, sample1.species, sample1.matrix, sample1.rid, sample1.runid, sample1.subject, sample1.nominalconc) base_mean_useresponse,
        case
          when sample1.name = 'LPC' then
            case when '&&sst-lpc-mean-plus-xsd&' is not null then to_number('&&sst-lpc-mean-plus-xsd&') else null end
          when sample1.name = 'HPC' then
            case when '&&sst-hpc-mean-plus-xsd&' is not null then to_number('&&sst-hpc-mean-plus-xsd&') else null end
          else null
        end mean_plus_xsd,
        case
          when sample1.name = 'LPC' then
            case when '&&sst-lpc-mean-minus-xsd&' is not null then to_number('&&sst-lpc-mean-minus-xsd&') else null end
          when sample1.name = 'HPC' then
            case when '&&sst-hpc-mean-minus-xsd&' is not null then to_number('&&sst-hpc-mean-minus-xsd&') else null end
          else null
        end mean_minus_xsd,
        case
          when sample1.name = 'LPC' then
            case when '&&sst-lpc-ratio-plus-xsd&' is not null then to_number('&&sst-lpc-ratio-plus-xsd&') else null end
          when sample1.name = 'HPC' then
            case when '&&sst-hpc-ratio-plus-xsd&' is not null then to_number('&&sst-hpc-ratio-plus-xsd&') else null end
          else null
        end ratio_plus_xsd,
        case
          when sample1.name = 'LPC' then
            case when '&&sst-lpc-ratio-minus-xsd&' is not null then to_number('&&sst-lpc-ratio-minus-xsd&') else null end
          when sample1.name = 'HPC' then
            case when '&&sst-hpc-ratio-minus-xsd&' is not null then to_number('&&sst-hpc-ratio-minus-xsd&') else null end
          else null
        end ratio_minus_xsd
      FROM
        (
          SELECT
            studyid, analyteid, rid, runid, acceptedrun, subject, nominalconc, concentrationunit, dilution, name, type, stability, 
            case
              when lower(stability) like 'base' or &&baseCond& then 0
              else  1
            end stabilityorder,
            species, matrix, hours, longtermunits, longtermtime, temperature,
            case
              when &&baseCond& then 'T'
              else 'F'
            end is_base,
            Avg(response) mean_response,
            Avg(case when(isdeactivated='F') then response else null end) mean_useresponse,
            Stddev(case when(isdeactivated='F') then response else null end) sd,
            count(case when(isdeactivated='F') then 1 else null end) n,
            count(1) m,
            Xmlagg(
              XMLELEMENT("sample",
                XMLATTRIBUTES(sampleid AS "sampleid", sampleresultid AS "sampleresultid" ),
                XMLELEMENT("runsamplesequencenumber", runsamplesequencenumber),
                XMLELEMENT("samplename", samplename),
                XMLELEMENT("samplesubtype", samplesubtype),
                XMLELEMENT("response", FormatRounded(round(response, &&DecPlResponse&), &&DecPlResponse&)),
                XMLELEMENT("status", resulttext),
                XMLELEMENT("deactivated", isdeactivated),
                XMLELEMENT("reason", reason)
              ) order by runsamplesequencenumber
            ) xml
          FROM (
            SELECT
              r.studyid, s.id sampleid, sr.id sampleresultid,
              s.designsampleid, s.samplename,
              case
                when regexp_like(s.samplename, '^[A-Z]+[ _-]([A-Z]+).*$', 'i') then regexp_replace(s.samplename, '^[A-Z]+[ _-]([A-Z]+).*$', '\1', 1, 1, 'i')
                else null
              end type,
              case
                when regexp_like(s.samplename, '^([A-Z]+).*$', 'i') then regexp_replace(s.samplename, '^([A-Z]+).*$', '\1', 1, 1, 'i')
                else null
              end name,
              case
                when regexp_like(s.samplename, '^.*-(\d+)$', 'i') then to_number(regexp_replace(s.samplename, '^.*-(\d+)$', '\1', 1, 1, 'i'))
                when regexp_like(s.samplename, '^\D+(\d+).*$', 'i') then to_number(regexp_replace(s.samplename, '^\D+(\d+).*$', '\1', 1, 1, 'i'))
                else null
              end subject,
              case
                when lower(s.samplename) like '%base%' then 'T'
                else 'F'
              end base,
              REGEXP_SUBSTR(s.samplename, '(SS[ _-]?)([a-z0-9]+)[ _-]', 1, 1,'i', 2) stability,
              r.id rid, r.runid, sa.id analyteid, s.samplesubtype,
              case when ra.RunAnalyteRegressionStatus = &&AcceptedRunAnalyte& then 'T' else 'F' end acceptedrun,
              s.runsamplesequencenumber, sr.analytearea response,
              s.status, sr.commenttext, sr.concentration conc,
              sr.resulttext, s.source, d.reason,
              k.concentration nominalconc, s.dilution,
              sr.concentrationunits concentrationunit, a.species, a.sampletypeid matrix,
              --s.hours, s.longtermunits, s.longtermtime, s.temperature,
              &&StabilityInfo&
              case
                when d.code is not null then 'T'
                else 'F'
              end isdeactivated
            FROM 
              &&TempTabPrefix&bio$run r,
              &&TempTabPrefix&bio$run$analytes ra,
              &&TempTabPrefix&bio$study$analytes sa,
              &&TempTabPrefix&bio$assay a,
              &&TempTabPrefix&bio$run$worklist s,
              &&TempTabPrefix&bio$run$worklist$result$rw sr,
              &&TempTabPrefix&bio$run$ana$known k,
              &&TempTabPrefix&bio$covib$run$sample$deact d,
              isr$crit kwz--,
              --table(&&LALPackage&.GetRunStates) rs
            WHERE r.ID = s.runID
            AND s.ID = sr.worklistID
            AND a.ID = r.assayID
            AND r.studyID = '&&StudyID&' AND a.studyID = '&&StudyID&'
            --AND rs.column_value = r.runstatusnum
            AND sr.runAnalyteCode = ra.code
            AND r.ID = ra.runID
            AND ra.analyteID = sa.ID
            AND s.code = d.code (+)
            AND k.runAnalyteCode = ra.code AND k.name = s.sampleName
            AND kwz.key = s.samplename AND kwz.entity = '&&KnownEntity&' AND kwz.masterkey = '&&ExpMasterkey&' AND kwz.repid = &&RepID&
            --and s.runcode || '-' || s.runsamplesequencenumber || '-' || sa.analyteorder in (select code from &&TempTabPrefix&bio$deactivated$samples)
            &&AddConditionS1&
          )
          GROUP BY studyid, analyteid, species, matrix, hours, longtermunits, longtermtime, temperature, rid, runid, acceptedrun, subject, nominalconc, concentrationunit, dilution, name, type, base, stability
        ) sample1
        LEFT OUTER JOIN (
          SELECT
            studyid, analyteid, rid, runid, subject, nominalconc, concentrationunit, dilution, name, type, stability,
            species, matrix, hours, longtermunits, longtermtime, temperature,
            Avg(response) mean_response,
            Avg(case when(isdeactivated='F') then response else null end) mean_useresponse,
            Stddev(case when(isdeactivated='F') then response else null end) sd,
            count(case when(isdeactivated='F') then 1 else null end) n,
            count(1) m,
            Xmlagg(
              XMLELEMENT("sample",
                XMLATTRIBUTES(sampleid AS "sampleid", sampleresultid AS "sampleresultid" ),
                XMLELEMENT("runsamplesequencenumber", runsamplesequencenumber),
                XMLELEMENT("samplename", samplename),
                XMLELEMENT("samplesubtype", samplesubtype),
                XMLELEMENT("response", FormatRounded(round(response, &&DecPlResponse&), &&DecPlResponse&)),
                XMLELEMENT("status", resulttext),
                XMLELEMENT("deactivated", isdeactivated),
                XMLELEMENT("reason", reason)
              ) order by runsamplesequencenumber
            ) xml
          FROM (
            SELECT
              r.studyid, s.id sampleid, sr.id sampleresultid,
              s.designsampleid, s.samplename,
              case
                when regexp_like(s.samplename, '^[A-Z]+[ _-]([A-Z]+).*$', 'i') then regexp_replace(s.samplename, '^[A-Z]+[ _-]([A-Z]+).*$', '\1', 1, 1, 'i')
                else null
              end type,
              case
                when regexp_like(s.samplename, '^([A-Z]+).*$', 'i') then regexp_replace(s.samplename, '^([A-Z]+).*$', '\1', 1, 1, 'i')
                else null
              end name,
              case
                when regexp_like(s.samplename, '^.*-(\d+)$', 'i') then to_number(regexp_replace(s.samplename, '^.*-(\d+)$', '\1', 1, 1, 'i'))
                when regexp_like(s.samplename, '^\D+(\d+).*$', 'i') then to_number(regexp_replace(s.samplename, '^\D+(\d+).*$', '\1', 1, 1, 'i'))
                else null
              end subject,
              REGEXP_SUBSTR(s.samplename, '(SS[ _-]?)([a-z0-9]+)[ _-]', 1, 1,'i', 2) stability,
              r.id rid, r.runid, sa.id analyteid, s.samplesubtype,
              s.runsamplesequencenumber, sr.analytearea response,
              s.status, sr.commenttext, sr.concentration conc,
              sr.resulttext, s.source, d.reason,
              k.concentration nominalconc, s.dilution,
              sr.concentrationunits concentrationunit, a.species, a.sampletypeid matrix,
              --s.hours, s.longtermunits, s.longtermtime, s.temperature,
              &&StabilityInfo&
              case
                when d.code is not null then 'T'
                else 'F'
              end isdeactivated
            FROM 
              &&TempTabPrefix&bio$run r,
              &&TempTabPrefix&bio$run$analytes ra,
              &&TempTabPrefix&bio$study$analytes sa,
              &&TempTabPrefix&bio$assay a,
              &&TempTabPrefix&bio$run$worklist s,
              &&TempTabPrefix&bio$run$worklist$result$rw sr,
              &&TempTabPrefix&bio$run$ana$known k,
              &&TempTabPrefix&bio$covib$run$sample$deact d,
              isr$crit kwz--,
              --table(&&LALPackage&.GetRunStates) rs
            WHERE r.ID = s.runID
            AND s.ID = sr.worklistID
            AND a.ID = r.assayID
            AND r.studyID = '&&StudyID&' AND a.studyID = '&&StudyID&'
            --AND rs.column_value = r.runstatusnum
            AND sr.runAnalyteCode = ra.code
            AND r.ID = ra.runID
            AND sr.runanalyteCode = ra.Code
            AND ra.analyteID = sa.ID
            AND s.code = d.code (+)
            AND k.runAnalyteCode = ra.code AND k.name = s.sampleName
            AND kwz.key = s.samplename AND kwz.entity = '&&KnownEntity&' AND kwz.masterkey = '&&ExpMasterkey&' AND kwz.repid = &&RepID&
            --and s.runcode || '-' || s.runsamplesequencenumber || '-' || sa.analyteorder in (select code from &&TempTabPrefix&bio$deactivated$samples)
            &&AddConditionS2&
          )
          GROUP BY studyid, analyteid, species, matrix, hours, longtermunits, longtermtime, temperature, rid, runid, subject, nominalconc, concentrationunit, dilution, name, type, stability
        ) sample2
        ON sample1.studyID = sample2.studyID
        AND sample1.rID = sample2.rID
        AND sample1.runid = sample2.runid
        AND sample1.analyteID = sample2.analyteID
        AND sample1.species = sample2.species
        AND sample1.matrix = sample2.matrix
        AND (sample1.hours = sample2.hours OR (sample1.hours is null AND sample2.hours is null))
        AND (sample1.longtermunits = sample2.longtermunits OR (sample1.longtermunits is null AND sample2.longtermunits is null))
        AND (sample1.longtermtime = sample2.longtermtime OR (sample1.longtermtime is null AND sample2.longtermtime is null))
        AND (sample1.temperature = sample2.temperature OR (sample1.temperature is null AND sample2.temperature is null))
        AND sample1.subject = sample2.subject
        AND nvl(sample1.nominalconc,-1) = nvl(sample2.nominalconc,-1)
        AND sample1.concentrationunit = sample2.concentrationunit
        AND sample1.name = sample2.name
        AND sample1.type = sample2.type
        AND sample1.stability = sample2.stability
        LEFT OUTER JOIN (
          SELECT
            studyid, analyteid, rid, runid, species, matrix,
            &&NCMeanOrMedian&(
              case when stb$util.GetReportParameterValue('MEAN_OR_MEDIAN_NC',&&RepID&) = 'meanofmeans' then
                case when replicatenumber = 1 then meanresponse else null end
              else
                response
              end
            ) meanmedian_response,
            &&NCMeanOrMedian&(
              case when stb$util.GetReportParameterValue('MEAN_OR_MEDIAN_NC',&&RepID&) = 'meanofmeans' then
                case when replicatenumber = 1 then meanuseresponse else null end
              else
                case when(isdeactivated='F') then response else null end
              end
            ) meanmedian_useresponse,
            case
              when '&&FixedScreenCutpoint&' is not null then
                to_number('&&FixedScreenCutpoint&')
              when '&&ScreenCPNFactor&' is not null then
                &&NCMeanOrMedian&(
                  case when stb$util.GetReportParameterValue('MEAN_OR_MEDIAN_NC',&&RepID&) = 'meanofmeans' then
                    case when replicatenumber = 1 then meanuseresponse else null end
                  else
                    case when(isdeactivated='F') then response else null end
                  end
                )*to_number('&&ScreenCPNFactor&')
              else
                &&NCMeanOrMedian&(
                  case when stb$util.GetReportParameterValue('MEAN_OR_MEDIAN_NC',&&RepID&) = 'meanofmeans' then
                    case when replicatenumber = 1 then meanuseresponse else null end
                  else
                    case when(isdeactivated='F') then response else null end
                  end
                )
            end screen_cp,
            Stddev(
              case when stb$util.GetReportParameterValue('MEAN_OR_MEDIAN_NC',&&RepID&) = 'meanofmeans' then
                case when replicatenumber = 1 then meanuseresponse else null end
              else
                case when(isdeactivated='F') then response else null end
              end
            ) sd,
            count(
              case when stb$util.GetReportParameterValue('MEAN_OR_MEDIAN_NC',&&RepID&) = 'meanofmeans' then
                case when replicatenumber = 1 then meanuseresponse else null end
              else
                case when(isdeactivated='F') then response else null end
              end
            ) n,
            count(
              case when stb$util.GetReportParameterValue('MEAN_OR_MEDIAN_NC',&&RepID&) = 'meanofmeans' then case when replicatenumber = 1 then meanresponse else null end
              else response end
            ) m,
            Xmlagg(
              XMLELEMENT("sample",
                XMLATTRIBUTES(sampleid AS "sampleid", sampleresultid AS "sampleresultid" ),
                XMLELEMENT("runsamplesequencenumber", runsamplesequencenumber),
                XMLELEMENT("samplename", samplename),
                XMLELEMENT("samplesubtype", samplesubtype),
                XMLELEMENT("response", FormatRounded(round(response, &&DecPlResponse&), &&DecPlResponse&)),
                XMLELEMENT("status", resulttext),
                XMLELEMENT("deactivated", isdeactivated),
                XMLELEMENT("reason", reason)
              ) order by runsamplesequencenumber
            ) xml
          FROM (
            SELECT
              r.studyid, s.id sampleid, sr.id sampleresultid,
              s.designsampleid, s.samplename, d.reason, s.replicatenumber,
              r.id rid, r.runid, sa.id analyteid, s.samplesubtype,
              s.runsamplesequencenumber, sr.analytearea response,
              s.status, sr.commenttext, sr.concentration conc,
              sr.resulttext, s.source, s.concentration nominalconc,
              sr.concentrationunits concentrationunit, a.species, a.sampletypeid matrix,
              s.hours, s.longtermunits, s.longtermtime, s.temperature,
              Avg(case when(d.code is null) then sr.analytearea else null end) over (partition by r.studyid, sa.id, r.id, s.samplename) meanresponse,
              Avg(case when(d.code is null) then sr.analytearea else null end) over (partition by r.studyid, sa.id, r.id, s.samplename) meanuseresponse,
              case
                when d.code is not null then 'T'
                else 'F'
              end isdeactivated
            FROM 
              &&TempTabPrefix&bio$run r,
              &&TempTabPrefix&bio$run$analytes ra,
              &&TempTabPrefix&bio$study$analytes sa,
              &&TempTabPrefix&bio$assay a,
              &&TempTabPrefix&bio$run$worklist s,
              &&TempTabPrefix&bio$run$worklist$result$rw sr,
              &&TempTabPrefix&bio$covib$run$sample$deact d,
              isr$crit kwz--,
              --table(ISR$COVIB$BIOANALYTICS$WT$LAL.GetRunStates) rs
            WHERE r.ID = s.runID
            AND s.ID = sr.worklistID
            AND a.ID = r.assayID
            AND r.studyID = '&&StudyID&' AND a.studyID = '&&StudyID&'
            --AND rs.column_value = r.runstatusnum
            AND sr.runAnalyteCode = ra.code
            AND r.ID = ra.runID
            AND sr.runanalyteCode = ra.Code
            AND ra.analyteID = sa.ID
            AND s.code = d.code (+)
            AND kwz.key = s.samplename AND kwz.entity = '&&KnownEntity&' AND kwz.masterkey = '&&ExpMasterkey&' AND kwz.repid = &&RepID&
            --and s.runcode || '-' || s.runsamplesequencenumber || '-' || sa.analyteorder in (select code from &&TempTabPrefix&bio$deactivated$samples)
            &&AddConditionNC&
          )
          GROUP BY studyid, analyteid, species, matrix, rid, runid) NC
      ON sample1.studyID = NC.studyID
      AND sample1.rID = NC.rID
      AND sample1.runid = NC.runid
      AND sample1.analyteID = NC.analyteID
      AND sample1.species = NC.species
      AND sample1.matrix = NC.matrix
      LEFT OUTER JOIN isr$crit ccp
      ON ccp.entity = 'BIO$COVIB$CONFIRM$CUTPOINT' AND ccp.masterkey = 'NORM' AND ccp.repid = &&RepID&
      WHERE 1=1
      &&AddCondition&
    )
 )
 GROUP BY studyid, analyteid, species, matrix, hours, longtermunits, longtermtime, temperature, rid, runid, acceptedrun, nominalconc, concentrationunit, dilution, name, type, stability, stabilityorder, mean_plus_xsd, mean_minus_xsd, ratio_plus_xsd, ratio_minus_xsd)
GROUP BY studyid--, analyteid, species, matrix, hours, longtermunits, longtermtime, temperature
