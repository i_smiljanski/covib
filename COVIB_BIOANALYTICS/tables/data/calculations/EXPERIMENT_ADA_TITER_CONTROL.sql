SELECT XMLELEMENT("experiment", XMLATTRIBUTES('&&Experiment&' AS "name" ,'titer_control' AS "type"),
        Xmlagg ( 
          Xmlagg (XMLELEMENT("calculation", xmlconcat(targ,val,res)) order by acceptedrun, analyteorder, meanormedian, runid, levelorder, name) order by studyid
       ))
FROM (
  SELECT studyid, analyteid, analyteorder, rid, runid, acceptedrun, meanormedian, name, levelorder,
    XMLELEMENT("target",
      XMLELEMENT("analyteid", analyteid),
      XMLELEMENT("meanormedian", meanormedian),
      XMLELEMENT("runid", rid),
      XMLELEMENT("runno", runid),
      XMLELEMENT("acceptedrun", acceptedrun),
      XMLELEMENT("name", name)
    ) as targ,
    XMLELEMENT("values", 
      Xmlagg(
        XMLELEMENT("dilution",
          XMLATTRIBUTES(s1_dilution AS "dilution"),
          XMLELEMENT("dilution", s1_dilution),
          XMLELEMENT("min", case when minPosResponse = s1_mean_response then 'T' else 'F' end),
          XMLELEMENT("min-dilution", case when minPosResponse = s1_mean_response and LENGTH(TRIM(TRANSLATE(runresult, ' +-.0123456789', ' '))) is null then '1/'||FormatRounded(round(s1_dilution, 2), 2) else 'N.C.' end),
          XMLELEMENT("titer-value", case when minPosResponse = s1_mean_response and LENGTH(TRIM(TRANSLATE(runresult, ' +-.0123456789', ' '))) is null then &&FormatFuncTiter&(&&RepresentationFuncTiter&(runresult, &&PlacesTiter&), &&PlacesTiter&) else 'N.C.' end),
          XMLELEMENT("nc-mean-median", &&FormatFuncResponse&(&&RepresentationFuncResponse&(nc_meanmedian_response, &&PlacesResponse&), &&PlacesResponse&)),
          XMLELEMENT("cutpoint", &&FormatFuncResponse&(&&RepresentationFuncResponse&(cutpoint, &&PlacesResponse&), &&PlacesResponse&)),
          XMLELEMENT("cutpoint-limit", cutpointlimit),
          XMLELEMENT("nc-cv", &&FormatFuncPercentage&(&&RepresentationFuncPercentage&(nc_cv, &&PlacesPercentage&), &&PlacesPercentage&)),
          XMLELEMENT("nc-flagcv", case when &&RepresentationFuncPercentage&(nc_cv, &&PlacesPercentage&) > cvlimit then 'T' else 'F' end),
          XMLELEMENT("samples", s1_xml),
          XMLELEMENT("mean", &&FormatFuncResponse&(&&RepresentationFuncResponse&(s1_mean_response, &&PlacesResponse&), &&PlacesResponse&)),
          case
            when s1_mean_response < llre then XMLELEMENT("flagresponse", XMLATTRIBUTES('LLRe' as "type"), &&FormatFuncResponse&(&&RepresentationFuncResponse&(llre, &&PlacesResponse&), &&PlacesResponse&))
            when s1_mean_response > ulre then XMLELEMENT("flagresponse", XMLATTRIBUTES('ULRe' as "type"), &&FormatFuncResponse&(&&RepresentationFuncResponse&(ulre, &&PlacesResponse&), &&PlacesResponse&))
          else null end,
          XMLELEMENT("cv", &&FormatFuncPercentage&(&&RepresentationFuncPercentage&(s1_cv, &&PlacesPercentage&), &&PlacesPercentage&)),
          XMLELEMENT("flagcv", case when &&RepresentationFuncPercentage&(s1_cv, &&PlacesPercentage&) > cvlimit then 'T' else 'F' end),
          XMLELEMENT("result", s1_max_status),
          XMLELEMENT("nc-n", nc_n),
          XMLELEMENT("nc-m", nc_m),
          XMLELEMENT("n", s1_n),
          XMLELEMENT("m", s1_m),
          XMLELEMENT("cvlimit", cvlimit)
        ) order by s1_dilution
      )
    ) as val,
    XMLELEMENT("result",
      XMLELEMENT("nc-mean-response", &&FormatFuncResponse&(&&RepresentationFuncResponse&(Avg(nc_meanmedian_response), &&PlacesResponse&), &&PlacesResponse&)),
      XMLELEMENT("max-response", &&FormatFuncResponse&(&&RepresentationFuncResponse&(max(s1_mean_response), &&PlacesResponse&), &&PlacesResponse&)),
      XMLELEMENT("cv", &&FormatFuncPercentage&(&&RepresentationFuncPercentage&(Stddev(s1_mean_response)/nullif(Avg(s1_mean_response), 0)*100, &&PlacesPercentage&), &&PlacesPercentage&)),
      XMLELEMENT("max-status", max(s1_max_status)),
      XMLELEMENT("run-ncresponse-mean", &&FormatFuncResponse&(&&RepresentationFuncResponse&(Avg(nc_meanmedian_response), &&PlacesResponse&), &&PlacesResponse&)),
      XMLELEMENT("run-ncresponse-sd", &&FormatFuncResponseSD&(&&RepresentationFuncResponseSD&(Stddev(nc_meanmedian_response), &&PlacesResponseSD&), &&PlacesResponseSD&)),
      XMLELEMENT("run-ncresponse-cv", &&FormatFuncPercentage&(&&RepresentationFuncPercentage&(Stddev(nc_meanmedian_response)/nullif(Avg(nc_meanmedian_response), 0)*100, &&PlacesPercentage&), &&PlacesPercentage&)),
      XMLELEMENT("run-response-mean", &&FormatFuncResponse&(&&RepresentationFuncResponse&(Avg(s1_mean_response), &&PlacesResponse&), &&PlacesResponse&)),
          case
            when Avg(s1_mean_response) < llre then XMLELEMENT("flagresponse", XMLATTRIBUTES('LLRe' as "type"), &&FormatFuncResponse&(&&RepresentationFuncResponse&(llre, &&PlacesResponse&), &&PlacesResponse&))
            when Avg(s1_mean_response) > ulre then XMLELEMENT("flagresponse", XMLATTRIBUTES('ULRe' as "type"), &&FormatFuncResponse&(&&RepresentationFuncResponse&(ulre, &&PlacesResponse&), &&PlacesResponse&))
          else null end,
      XMLELEMENT("run-response-sd", &&FormatFuncResponseSD&(&&RepresentationFuncResponseSD&(Stddev(s1_mean_response), &&PlacesResponseSD&), &&PlacesResponseSD&)),
      XMLELEMENT("run-response-cv", &&FormatFuncPercentage&(&&RepresentationFuncPercentage&(Stddev(s1_mean_response)/nullif(Avg(s1_mean_response), 0)*100, &&PlacesPercentage&), &&PlacesPercentage&))
    ) as res
  FROM (
    SELECT
      studyid, rid, runid, acceptedrun, analyteid, analyteorder, levelorder,
      nc_concunit, cutpointlimit, meanormedian, nc_meanmedian_response, nc_cv, nc_n, nc_m,
      cutpoint, llre, ulre, llra, ulra, name, s1_max_status,
      min(case
        when s1_mean_useresponse > cutpoint then s1_mean_useresponse
        else null
      end) over (partition by studyid, analyteid, analyteorder, meanormedian, rid, runid, levelorder, name) minPosResponse,
      max(s1_max_status) over (partition by studyid, analyteid, analyteorder, meanormedian, rid, runid, levelorder, name) runresult,
      s1_dilution, cvlimit, s1_mean_response, s1_mean_useresponse, s1_cv, s1_n, s1_m, s1_xml
    FROM
      (SELECT
        nc.studyid, nc.rid, nc.runid, nc.acceptedrun, nc.analyteid, nc.analyteorder,
        case
          when sample1.name = 'LPC' then 1
          when sample1.name = 'MPC' then 2
          when sample1.name = 'HPC' then 3
          else 4
        end levelorder,
        nc.concentrationunit nc_concunit, nc.cutpointlimit, nc.meanormedian,
        nc.meanmedian_response nc_meanmedian_response,
        nc_cv, nc_n, nc_m,-- nc_xml,
        nc.cutpoint,
        sample1.name, sample1.concentrationunit s1_concunit,
        max(sample1.resulttext) s1_max_status,
        min(case
          when sample1.response > nc.cutpoint then sample1.response
          else null
        end) minPosResponse,
        sample1.dilution s1_dilution, sample1.flagpercent cvlimit,
        Avg(sample1.response) s1_mean_response, Avg(sample1.useresponse) s1_mean_useresponse,
        Stddev(sample1.useresponse)/nullif(Avg(sample1.useresponse), 0)*100 s1_cv,
        count(sample1.useresponse) s1_n, count(sample1.response) s1_m,
        max(sample1.llre) llre, min(sample1.ulre) ulre, max(sample1.llra) llra, min(sample1.ulra) ulra,
        Xmlagg(
            XMLELEMENT("sample",
              XMLATTRIBUTES(sample1.sampleid AS "sampleid", sample1.sampleresultid AS "sampleresultid" ),
              XMLELEMENT("name", sample1.name), XMLELEMENT("dilution", sample1.dilution),
              XMLELEMENT("samplesubtype", sample1.samplesubtype), XMLELEMENT("runsamplekind", sample1.runsamplekind),
              XMLELEMENT("response", &&FormatFuncResponse&(&&RepresentationFuncResponse&(sample1.response, &&PlacesResponse&), &&PlacesResponse&)),
              XMLELEMENT("status", sample1.resulttext),
              XMLELEMENT("deactivated", sample1.isdeactivated)
            ) order by sample1.runsamplesequencenumber
          ) s1_xml
        FROM (
          SELECT 
            studyid,
            rid,
            runid,
            acceptedrun,
            analyteid,
            analyteorder,
            concentrationunit,
            cutpointlimit,
            meanormedian,
            case 
              when meanormedian = 'Mean' then Avg(useresponse)
              when meanormedian = 'Median' then Median(useresponse)
              when meanormedian = 'Mean of Means' then Avg(case when replicatenumber = 1 then meanuseresponse else null end)
              else Avg(useresponse)
            end meanmedian_response,
            case
              when min(cutpointmethod) = 3 then min(cutpointoffset) * Stddev(useresponse) else 0
            end + case
              when min(cutpointmethod) = 1 then min(cutpointoffset) else 0
            end + case
              when min(cutpointmethod) = 2 then min(cutpointoffset)
              when meanormedian = 'Mean' then Avg(useresponse)
              when meanormedian = 'Median' then Median(useresponse)
              when meanormedian = 'Mean of Means' then Avg(case when replicatenumber = 1 then meanuseresponse else null end)
              else Avg(useresponse) * min(cutpointfactor)
            end * case
              when min(cutpointmethod) = 0 then min(cutpointfactor) else 1
            end cutpoint,
            case 
              when meanormedian = 'Mean' then Stddev(useresponse)/nullif(Avg(useresponse),0)*100
              when meanormedian = 'Median' then Stddev(useresponse)/nullif(Median(useresponse),0)*100
              when meanormedian = 'Mean of Means' then Stddev(case when replicatenumber = 1 then meanuseresponse else null end)/nullif(Avg(case when replicatenumber = 1 then meanuseresponse else null end),0)*100
              else Stddev(useresponse)/nullif(Avg(useresponse),0)*100
            end nc_cv,
            count(useresponse) nc_n,
            count(response) nc_m
            FROM(
              SELECT
                r.studyid, r.id rid, r.runid,
                case when ra.RunAnalyteRegressionStatus = &&RejectedRunAnalyte& then 'F' else 'T' end acceptedrun,
                sa.id analyteid, sa.analyteorder,
                s.id sampleid, s.samplename name, s.samplesubtype, s.runsamplesequencenumber, s.replicatenumber,
                sr.id sampleresultid, sr.concentrationunits concentrationunit, sr.resulttext, sr.analytearea response,
                a.meanormedian,
                /*case
                  when '&&ScreenCTRLnFactor&' is not null then
                    to_number('&&ScreenCTRLnFactor&')
                  else
                    a.cutpointfactor
                end*/
                a.cutpointfactor cutpointfactor, a.cutpointoffset,
                /*case
                  when '&&ScreenCTRLnFactor&' is not null then 0
                  else a.cutpointmethod
                end*/
                a.cutpointmethod cutpointmethod, a.cutpointlimit,
                case when(ds.code is null) then sr.analytearea else null end useresponse,
                Avg(case when(ds.code is null) then sr.analytearea else null end) over (partition by r.studyid, sa.id, r.id, s.samplename) meanuseresponse,
                case when ds.code is not null then 'T' else 'F' end isdeactivated
              FROM 
                &&TempTabPrefix&bio$run r,
                &&TempTabPrefix&bio$run$analytes ra,
                &&TempTabPrefix&bio$study$analytes sa,
                &&TempTabPrefix&bio$assay a,
                &&TempTabPrefix&bio$run$worklist s,
                &&TempTabPrefix&bio$run$worklist$result$rw sr,
                &&TempTabPrefix&bio$deactivated$samples ds,
                table(&&LALPackage&.GetRunStates) rs
              WHERE r.ID = s.runID AND s.ID = sr.worklistID AND a.ID = r.assayID
              --AND s.treatmentid = 'ADA'
              --AND s.studyid = '&&StudyID&' AND sr.studyID = '&&StudyID&'
              AND r.studyid = '&&StudyID&' AND a.studyID = '&&StudyID&'
              AND rs.column_value = r.runstatusnum AND sr.runAnalyteCode = ra.code
              AND r.ID = ra.runID AND ra.analyteID = sa.ID
              AND s.runcode || '-' || s.runsamplesequencenumber || '-' || sa.analyteorder = ds.code (+)
              AND s.runsamplekind like '&&NCKnownType&'
              &&AddConditionNC&
            )
          GROUP BY studyID, rid, runid, acceptedrun, analyteid, analyteorder, concentrationunit, cutpointlimit, cutpointfactor, meanormedian
        ) nc, (
          SELECT
            r.studyid, r.id rid, r.runid, sa.id analyteid, sa.analyteorder, s.id sampleid, sr.id sampleresultid,
            --s.samplename name,
            s.samplesubtype, s.runsamplekind, s.runsamplesequencenumber, s.dilution, s.spikedanalyte,
            sr.concentrationunits concentrationunit, sr.resulttext, sr.analytearea response,
            regexp_replace(s.spikedanalyte, '^(.*PC).*$', '\1', 1, 1, 'i') name,
            llre.key llre, ulre.key ulre, llra.key llra, ulra.key ulra,
            case when(ds.code is null) then sr.analytearea else null end useresponse,
            case
              when ds.code is not null then 'T'
              else 'F'
            end isdeactivated,
            s.flagpercent
          FROM 
            &&TempTabPrefix&bio$run r,
            &&TempTabPrefix&bio$run$analytes ra,
            &&TempTabPrefix&bio$study$analytes sa,
            &&TempTabPrefix&bio$assay a,
            &&TempTabPrefix&bio$run$worklist s,
            &&TempTabPrefix&bio$run$worklist$result$rw sr,
            &&TempTabPrefix&bio$deactivated$samples ds,
            table(&&LALPackage&.GetRunStates) rs,
            ISR$CRIT llre, ISR$CRIT ulre, ISR$CRIT llra, ISR$CRIT ulra
          WHERE r.ID = s.runID
          AND s.ID = sr.worklistID
          AND a.ID = r.assayID
          --AND s.treatmentid = 'ADA'
          --AND s.studyid = '&&StudyID&' AND sr.studyID = '&&StudyID&'
          AND r.studyid = '&&StudyID&'
          AND a.studyID = '&&StudyID&'
          AND rs.column_value = r.runstatusnum
          AND sr.runAnalyteCode = ra.code
          AND r.ID = ra.runID
          AND ra.analyteID = sa.ID
          AND s.runcode || '-' || s.runsamplesequencenumber || '-' || sa.analyteorder = ds.code (+)
          AND s.runsamplekind like '&&S1KnownType&'
          AND llre.repid(+) = &&RepID& AND llre.entity(+) = 'BIO$COVIB$LO$RESPONSE$'||regexp_replace(s.spikedanalyte, '^(.*PC).*$', '\1', 1, 1, 'i') AND llre.masterkey(+) =  sa.code
          AND ulre.repid(+) = &&RepID& AND ulre.entity(+) = 'BIO$COVIB$HI$RESPONSE$'||regexp_replace(s.spikedanalyte, '^(.*PC).*$', '\1', 1, 1, 'i') AND ulre.masterkey(+) =  sa.code
          AND llra.repid(+) = &&RepID& AND llra.entity(+) = 'BIO$COVIB$LO$RATIO$'||regexp_replace(s.spikedanalyte, '^(.*PC).*$', '\1', 1, 1, 'i') AND llra.masterkey(+) =  sa.code
          AND ulra.repid(+) = &&RepID& AND ulra.entity(+) = 'BIO$COVIB$HI$RATIO$'||regexp_replace(s.spikedanalyte, '^(.*PC).*$', '\1', 1, 1, 'i') AND ulra.masterkey(+) =  sa.code
          &&AddConditionS1&
        ) sample1
      WHERE nc.rid = sample1.rid
        AND nc.analyteid = sample1.analyteid
        AND nc.studyid = sample1.studyid
        &&AddCondition&
      GROUP BY nc.studyid, nc.analyteid, nc.analyteorder, nc.rid, nc.runid, nc.acceptedrun, sample1.name, nc.concentrationunit, sample1.concentrationunit, nc.meanormedian, nc.cutpointlimit, sample1.dilution, nc.meanmedian_response, nc.cutpoint, nc_cv, nc_n, nc_m, sample1.flagpercent)
  )
  GROUP BY studyid, analyteid, analyteorder, meanormedian, rid, runid, acceptedrun, levelorder, name, llre, ulre, llra, ulra)
GROUP BY studyid
