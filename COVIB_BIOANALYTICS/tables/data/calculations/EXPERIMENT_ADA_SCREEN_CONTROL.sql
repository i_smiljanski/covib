SELECT XMLELEMENT("experiment", XMLATTRIBUTES('&&Experiment&' AS "name" ,'screen_control' AS "type"),
        Xmlagg ( 
          Xmlagg (XMLELEMENT("calculation", xmlconcat(targ,val,res)) order by acceptedrun, analyteorder, meanormedian, levelorder, nc_concunit) order by studyid
       ))
FROM (
  SELECT studyid, analyteid, acceptedrun, analyteorder, name, levelorder, nc_concunit, meanormedian,
    XMLELEMENT("target",
      XMLELEMENT("acceptedrun", acceptedrun),
      XMLELEMENT("analyteid", analyteid),
      XMLELEMENT("meanormedian", meanormedian),
      XMLELEMENT("name", name),
      XMLELEMENT("level", levelorder),
      XMLELEMENT("nc_concnunit", nc_concunit)
    ) as targ,
    XMLELEMENT("values", 
      Xmlagg(
        XMLELEMENT("run",
          XMLATTRIBUTES(rid AS "runid", runid AS "runno"),
          --XMLELEMENT("nc", nc_xml),
          XMLELEMENT("subject", subject),
          XMLELEMENT("s1_concnunit", s1_concunit),
          XMLELEMENT("nc-mean-median", &&FormatFuncResponse&(&&RepresentationFuncResponse&(nc_meanmedian_response, &&PlacesResponse&), &&PlacesResponse&)),
          XMLELEMENT("cutpoint", &&FormatFuncResponse&(&&RepresentationFuncResponse&(cutpoint, &&PlacesResponse&), &&PlacesResponse&)),
          XMLELEMENT("cutpoint-limit", cutpointlimit),
          XMLELEMENT("sample-cutpoint", &&FormatFuncResponse&(&&RepresentationFuncResponse&(watson_cutpoint, &&PlacesResponse&), &&PlacesResponse&)),
          XMLELEMENT("nc-cv", &&FormatFuncPercentage&(&&RepresentationFuncPercentage&(nc_cv, &&PlacesPercentage&), &&PlacesPercentage&)),
          XMLELEMENT("nc-flagcv", case when nc_cv > cvlimit then 'T' else 'F' end),
          XMLELEMENT("samples", s1_xml),
          XMLELEMENT("mean", &&FormatFuncResponse&(&&RepresentationFuncResponse&(s1_mean_response, &&PlacesResponse&), &&PlacesResponse&)),
          case
            when s1_mean_response < llre then XMLELEMENT("flagresponse", XMLATTRIBUTES('LLRe' as "type"), &&FormatFuncResponse&(&&RepresentationFuncResponse&(llre, &&PlacesResponse&), &&PlacesResponse&))
            when s1_mean_response > ulre then XMLELEMENT("flagresponse", XMLATTRIBUTES('ULRe' as "type"), &&FormatFuncResponse&(&&RepresentationFuncResponse&(ulre, &&PlacesResponse&), &&PlacesResponse&))
          else null end,
          XMLELEMENT("cv", &&FormatFuncPercentage&(&&RepresentationFuncPercentage&(s1_cv, &&PlacesPercentage&), &&PlacesPercentage&)),
          XMLELEMENT("flagcv", case when s1_cv > cvlimit then 'T' else 'F' end),
          XMLELEMENT("ratio", &&FormatFuncRatio&(&&RepresentationFuncRatio&(ratio, &&PlacesRatio&), &&PlacesRatio&)),
          case
            when ratio < llra then XMLELEMENT("flagratio", XMLATTRIBUTES('LLRa' as "type"), &&FormatFuncResponse&(&&RepresentationFuncResponse&(llra, &&PlacesResponse&), &&PlacesResponse&))
            when ratio > ulra then XMLELEMENT("flagratio", XMLATTRIBUTES('ULRa' as "type"), &&FormatFuncResponse&(&&RepresentationFuncResponse&(ulra, &&PlacesResponse&), &&PlacesResponse&))
          else null end,
          XMLELEMENT("result",
            case
              when s1_mean_response is not null and cutpoint is not null then
                case when s1_mean_response >= cutpoint then 'Positive Screen' else 'Negative Screen' end
              else
                null
            end
          ),
          XMLELEMENT("nc-n", nc_n),
          XMLELEMENT("nc-m", nc_m),
          XMLELEMENT("n", s1_n),
          XMLELEMENT("m", s1_m),
          XMLELEMENT("cvlimit", cvlimit),
          XMLELEMENT("run-response-mean", &&FormatFuncResponse&(&&RepresentationFuncResponse&(run_s1_mean_useresponse, &&PlacesResponse&), &&PlacesResponse&)),
          case
            when run_s1_mean_useresponse < llre then XMLELEMENT("run-flagresponse", XMLATTRIBUTES('LLRe' as "type"), &&FormatFuncResponse&(&&RepresentationFuncResponse&(llre, &&PlacesResponse&), &&PlacesResponse&))
            when run_s1_mean_useresponse > ulre then XMLELEMENT("run-flagresponse", XMLATTRIBUTES('ULRe' as "type"), &&FormatFuncResponse&(&&RepresentationFuncResponse&(ulre, &&PlacesResponse&), &&PlacesResponse&))
          else null end,
          XMLELEMENT("run-response-sd", &&FormatFuncResponseSD&(&&RepresentationFuncResponseSD&(run_s1_sd, &&PlacesResponseSD&), &&PlacesResponseSD&)),
          XMLELEMENT("run-response-cv", &&FormatFuncPercentage&(&&RepresentationFuncPercentage&(run_s1_sd/nullif(run_s1_mean_useresponse, 0)*100, &&PlacesPercentage&), &&PlacesPercentage&)),
          XMLELEMENT("run-ratio-mean", &&FormatFuncRatio&(&&RepresentationFuncRatio&(run_ratio_mean, &&PlacesRatio&), &&PlacesRatio&)),
          case
            when run_ratio_mean < llra then XMLELEMENT("run-flagratio", XMLATTRIBUTES('LLRa' as "type"), &&FormatFuncResponse&(&&RepresentationFuncResponse&(llra, &&PlacesResponse&), &&PlacesResponse&))
            when run_ratio_mean > ulra then XMLELEMENT("run-flagratio", XMLATTRIBUTES('ULRa' as "type"), &&FormatFuncResponse&(&&RepresentationFuncResponse&(ulra, &&PlacesResponse&), &&PlacesResponse&))
          else null end,
          XMLELEMENT("run-ratio-sd", &&FormatFuncRatio&(&&RepresentationFuncRatio&(run_ratio_sd, &&PlacesRatio&), &&PlacesRatio&)),
          XMLELEMENT("run-ratio-cv", &&FormatFuncRatio&(&&RepresentationFuncRatio&(run_ratio_sd/nullif(run_ratio_mean, 0)*100, &&PlacesRatio&), &&PlacesRatio&))
        ) order by runid
      )
    ) as val,
    XMLELEMENT("result",
      XMLELEMENT("cp-response-n", count(cutpoint_once)),
      XMLELEMENT("cp-response-mean", &&FormatFuncResponse&(&&RepresentationFuncResponse&(Avg(cutpoint_once), &&PlacesResponse&), &&PlacesResponse&)),
      XMLELEMENT("cp-response-sd", &&FormatFuncResponseSD&(&&RepresentationFuncResponseSD&(Stddev(cutpoint_once), &&PlacesResponseSD&), &&PlacesResponseSD&)),
      XMLELEMENT("cp-response-cv", &&FormatFuncPercentage&(&&RepresentationFuncPercentage&(Stddev(cutpoint_once) / nullif(Avg(cutpoint_once), 0)*100, &&PlacesPercentage&), &&PlacesPercentage&)),
      XMLELEMENT("s1-response-n", count(s1_mean_response)),
      XMLELEMENT("s1-response-mean", FormatSig(&&RepresentationFuncResponse&(Avg(s1_mean_response), &&PlacesResponse&), &&PlacesResponse&)),
      case
        when Avg(s1_mean_response) < llre then XMLELEMENT("flagresponse", XMLATTRIBUTES('LLRe' as "type"), &&FormatFuncResponse&(&&RepresentationFuncResponse&(llre, &&PlacesResponse&), &&PlacesResponse&))
        when Avg(s1_mean_response) > ulre then XMLELEMENT("flagresponse", XMLATTRIBUTES('ULRe' as "type"), &&FormatFuncResponse&(&&RepresentationFuncResponse&(ulre, &&PlacesResponse&), &&PlacesResponse&))
      else null end,
      XMLELEMENT("s1-response-sd", &&FormatFuncResponseSD&(&&RepresentationFuncResponseSD&(Stddev(s1_mean_response), &&PlacesResponseSD&), &&PlacesResponseSD&)),
      XMLELEMENT("s1-response-cv", &&FormatFuncPercentage&(&&RepresentationFuncPercentage&(Stddev(s1_mean_response) / nullif(Avg(s1_mean_response), 0)*100, &&PlacesPercentage&), &&PlacesPercentage&)),
      XMLELEMENT("s1-ratio-n", count(ratio)),
      XMLELEMENT("s1-ratio-mean", &&FormatFuncResponse&(&&RepresentationFuncResponse&(Avg(ratio), &&PlacesRatio&), &&PlacesRatio&)),
      case
        when Avg(ratio) < llra then XMLELEMENT("flagratio", XMLATTRIBUTES('LLRa' as "type"), &&FormatFuncResponse&(&&RepresentationFuncResponse&(llra, &&PlacesResponse&), &&PlacesResponse&))
        when Avg(ratio) > ulra then XMLELEMENT("flagratio", XMLATTRIBUTES('ULRa' as "type"), &&FormatFuncResponse&(&&RepresentationFuncResponse&(ulra, &&PlacesResponse&), &&PlacesResponse&))
      else null end,
      XMLELEMENT("s1-ratio-sd", &&FormatFuncRatio&(&&RepresentationFuncRatio&(Stddev(ratio), &&PlacesRatio&), &&PlacesRatio&)),
      XMLELEMENT("s1-ratio-cv", &&FormatFuncRatio&(&&RepresentationFuncRatio&(Stddev(ratio) / nullif(Avg(ratio), 0)*100, &&PlacesRatio&), &&PlacesRatio&))
    ) as res
  FROM (
    SELECT studyid, rid, runid, acceptedrun, analyteid, analyteorder, name, subject, levelorder,
      nc_concunit, cutpointlimit, meanormedian, nc_meanmedian_response,
      nc_cv, nc_n, nc_m,-- nc_xml,
      cutpoint,
      case
        when subject = first_subject then cutpoint
        else null
      end cutpoint_once,
      watson_cutpoint, cvlimit,
      s1_concunit, s1_max_status, s1_dilution, s1_mean_response, s1_mean_useresponse, s1_cv, ratio, s1_n, s1_m, s1_xml,
      llre, ulre, llra, ulra,
      Avg(s1_mean_useresponse) over (partition by studyid, analyteid, name, rid, runid, meanormedian) run_s1_mean_useresponse,
      Stddev(s1_mean_useresponse) over (partition by studyid, analyteid, name, rid, runid, meanormedian) run_s1_sd,
      Avg(ratio) over (partition by studyid, analyteid, name, rid, runid, meanormedian) run_ratio_mean,
      Stddev(ratio) over (partition by studyid, analyteid, name, rid, runid, meanormedian) run_ratio_sd
    FROM (
    SELECT
      nc.studyid, nc.rid, nc.runid, nc.analyteid, nc.analyteorder, nc.acceptedrun,
      sample1.name, sample1.subject, min(sample1.first_subject) first_subject,
      case
        when sample1.name = 'LPC' then 1
        when sample1.name = 'MPC' then 2
        when sample1.name = 'HPC' then 3
        else 4
      end levelorder,
      nc.concentrationunit nc_concunit, nc.cutpointlimit, nc.meanormedian,
      nc.meanmedian_response nc_meanmedian_response, nc.cutpoint,
      nc.watson_cutpoint, nc_cv, nc_n, nc_m,-- nc_xml,
      sample1.flagpercent cvlimit,
      sample1.concentrationunit s1_concunit,
      max(sample1.resulttext) s1_max_status,
      sample1.dilution s1_dilution, Avg(sample1.response) s1_mean_response,
      Avg(sample1.useresponse) s1_mean_useresponse,
      Stddev(sample1.useresponse)/nullif(Avg(sample1.useresponse), 0)*100 s1_cv,
      Avg(sample1.useresponse) / nullif(nc.meanmedian_response, 0) ratio,
      count(sample1.useresponse) s1_n, count(sample1.response) s1_m,
      max(llre) llre, min(ulre) ulre, max(llra) llra, min(ulra) ulra,
      Xmlagg(
          XMLELEMENT("sample",
            XMLATTRIBUTES(sample1.sampleid AS "sampleid", sample1.sampleresultid AS "sampleresultid" ),
            XMLELEMENT("name", sample1.name),
            XMLELEMENT("dilution", sample1.dilution),
            XMLELEMENT("samplesubtype", sample1.samplesubtype),
            XMLELEMENT("runsamplekind", sample1.runsamplekind),
            XMLELEMENT("response", &&FormatFuncResponse&(&&RepresentationFuncResponse&(sample1.response, &&PlacesResponse&), &&PlacesResponse&)),
            XMLELEMENT("status", sample1.resulttext),
            XMLELEMENT("deactivated", sample1.isdeactivated)
          ) order by sample1.runsamplesequencenumber
        ) s1_xml
      FROM (
        SELECT 
          studyid, rid, runid, analyteid, analyteorder, acceptedrun, concentrationunit, cutpointlimit, cutpointfactor, meanormedian,
          case 
            when meanormedian = 'Mean' then Avg(useresponse)
            when meanormedian = 'Median' then Median(useresponse)
            when meanormedian = 'Mean of Means' then Avg(case when replicatenumber = 1 then meanuseresponse else null end)
            else Avg(useresponse)
          end meanmedian_response,
          case
            when min(cutpointmethod) = 3 then min(cutpointoffset) * Stddev(useresponse) else 0
          end + case
            when min(cutpointmethod) = 1 then min(cutpointoffset) else 0
          end + case
            when min(cutpointmethod) = 2 then min(cutpointoffset)
            when meanormedian = 'Mean' then Avg(useresponse)
            when meanormedian = 'Median' then Median(useresponse)
            when meanormedian = 'Mean of Means' then Avg(case when replicatenumber = 1 then meanuseresponse else null end)
            else Avg(useresponse) * min(cutpointfactor)
          end * case
            when min(cutpointmethod) = 0 then min(cutpointfactor) else 1
          end cutpoint,
          case
            when /*is_titerrun*/'T' = 'F' then null
            else
              case
                when min(cutpointmethod) = 3 then min(cutpointoffset) * Stddev(useresponse) else 0
              end + case
                when min(cutpointmethod) = 1 then min(cutpointoffset) else 0
              end + case
                when min(cutpointmethod) = 2 then min(cutpointoffset)
                when meanormedian = 'Mean' then Avg(useresponse)
                when meanormedian = 'Median' then Median(useresponse)
                when meanormedian = 'Mean of Means' then Avg(case when replicatenumber = 1 then meanuseresponse else null end)
                else Avg(useresponse) * min(watson_cpfactor)
              end * case
                when min(cutpointmethod) = 0 then min(watson_cpfactor) else 1
              end
          end watson_cutpoint,
          --Stddev(useresponse)/nullif(Avg(useresponse), 0)*100 nc_cv,
          case 
            when meanormedian = 'Mean' then Stddev(useresponse)/nullif(Avg(useresponse),0)*100
            when meanormedian = 'Median' then Stddev(useresponse)/nullif(Median(useresponse),0)*100
            when meanormedian = 'Mean of Means' then Stddev(case when replicatenumber = 1 then meanuseresponse else null end)/nullif(Avg(case when replicatenumber = 1 then meanuseresponse else null end),0)*100
            else Stddev(useresponse)/nullif(Avg(useresponse),0)*100
          end nc_cv,
          count(useresponse) nc_n,
          count(response) nc_m/*,
          Xmlagg(
              XMLELEMENT("nc",
                XMLATTRIBUTES(sampleid AS "sampleid", sampleresultid AS "sampleresultid" ),
                XMLELEMENT("name", name),
                XMLELEMENT("samplesubtype", samplesubtype),
                XMLELEMENT("response", &&FormatFuncResponse&(&&RepresentationFuncResponse&(response, &&PlacesResponse&), &&PlacesResponse&)),
                XMLELEMENT("status", resulttext),
                XMLELEMENT("deactivated", isdeactivated)
              ) order by runsamplesequencenumber
            ) nc_xml*/
          FROM(
            SELECT
              r.studyid, r.id rid, r.runid, sa.id analyteid, sa.analyteorder,
              case when ra.RunAnalyteRegressionStatus = &&RejectedRunAnalyte& then 'F' else 'T' end acceptedrun,
              s.id sampleid, sr.id sampleresultid, s.samplename name,
              s.samplesubtype, s.runsamplesequencenumber, sr.concentrationunits concentrationunit,
              sr.resulttext, sr.analytearea response,
              case
                when r.ID in (select runID from &&TempTabPrefix&bio$run$worklist where samplesubtype = 'Titer') then 'T'
                else 'F'
              end is_titerrun,
              a.meanormedian, s.replicatenumber,
              case
                when '&&ScreenCTRLnFactor&' is not null then to_number('&&ScreenCTRLnFactor&')
                else a.cutpointfactor
              end cutpointfactor,
              a.cutpointfactor watson_cpfactor, a.cutpointoffset,
              case
                when '&&ScreenCTRLnFactor&' is not null then 0
                else a.cutpointmethod
              end cutpointmethod,
              a.cutpointlimit,
              case when(ds.code is null) then sr.analytearea else null end useresponse,
              Avg(case when(ds.code is null) then sr.analytearea else null end) over (partition by r.studyid, sa.id, r.id, s.samplename) meanuseresponse,
              case
                when ds.code is not null then 'T'
                else 'F'
              end isdeactivated
            FROM 
              &&TempTabPrefix&bio$run r,
              &&TempTabPrefix&bio$run$analytes ra,
              &&TempTabPrefix&bio$study$analytes sa,
              &&TempTabPrefix&bio$assay a,
              &&TempTabPrefix&bio$run$worklist s,
              &&TempTabPrefix&bio$run$worklist$result$rw sr,
              &&TempTabPrefix&bio$deactivated$samples ds,
              table(&&LALPackage&.GetRunStates) rs
            WHERE r.ID = s.runID
            AND s.ID = sr.worklistID AND a.ID = r.assayID
            --AND s.treatmentid = 'ADA'
            --AND s.studyid = '&&StudyID&' AND sr.studyID = '&&StudyID&'
            AND r.studyid = '&&StudyID&' AND a.studyID = '&&StudyID&'
            AND rs.column_value = r.runstatusnum AND sr.runAnalyteCode = ra.code
            AND r.ID = ra.runID AND ra.analyteID = sa.ID
            AND s.runcode || '-' || s.runsamplesequencenumber || '-' || sa.analyteorder = ds.code (+)
            AND (s.runsamplekind like '&&NCKnownType&')
            &&AddConditionNC&
          )
        GROUP BY studyID, rid, runid, acceptedrun, is_titerrun, analyteid, analyteorder, concentrationunit, cutpointlimit, cutpointfactor, watson_cpfactor, meanormedian
      ) nc, (
        SELECT
          r.studyid,
          r.id rid,
          r.runid,
          sa.id analyteid,
          sa.analyteorder,
          s.id sampleid,
          sr.id sampleresultid,
          regexp_replace(s.samplename, '^(.*PC).*$', '\1', 1, 1, 'i') name,
          case
            when regexp_like(s.samplename, '^\D+(\d+).*$', 'i') then
              to_number(regexp_replace(s.samplename, '^\D+(\d+).*$', '\1', 1, 1, 'i'))
            else 1
          end subject,
          min(case
            when regexp_like(s.samplename, '^\D+(\d+).*$', 'i') then
              to_number(regexp_replace(s.samplename, '^\D+(\d+).*$', '\1', 1, 1, 'i'))
            else 1
          end) over (partition by r.studyid, sa.id, regexp_replace(s.samplename, '^([LMH]PC).*$', '\1', 1, 1, 'i')) first_subject,
          s.samplename,
          s.samplesubtype,
          s.runsamplekind,
          s.runsamplesequencenumber,
          sr.concentrationunits concentrationunit,
          sr.resulttext,
          sr.analytearea response,
          s.dilution,
          s.spikedanalyte,
          llre.key llre, ulre.key ulre, llra.key llra, ulra.key ulra,
          case when(ds.code is null) then sr.analytearea else null end useresponse,
          case
            when ds.code is not null then 'T'
            else 'F'
          end isdeactivated,
          s.flagpercent
        FROM 
          &&TempTabPrefix&bio$run r,
          &&TempTabPrefix&bio$run$analytes ra,
          &&TempTabPrefix&bio$study$analytes sa,
          &&TempTabPrefix&bio$assay a,
          &&TempTabPrefix&bio$run$worklist s,
          &&TempTabPrefix&bio$run$worklist$result$rw sr,
          &&TempTabPrefix&bio$deactivated$samples ds,
          table(&&LALPackage&.GetRunStates) rs,
          ISR$CRIT llre, ISR$CRIT ulre, ISR$CRIT llra, ISR$CRIT ulra
        WHERE r.ID = s.runID
        AND s.ID = sr.worklistID
        AND a.ID = r.assayID
        --AND s.treatmentid = 'ADA'
        --AND s.studyid = '&&StudyID&' AND sr.studyID = '&&StudyID&'
        AND r.studyid = '&&StudyID&'
        AND a.studyID = '&&StudyID&'
        AND rs.column_value = r.runstatusnum
        AND sr.runAnalyteCode = ra.code
        AND r.ID = ra.runID
        AND ra.analyteID = sa.ID
        AND s.runcode || '-' || s.runsamplesequencenumber || '-' || sa.analyteorder = ds.code (+)
        AND (&&S1KnownType&)
        AND llre.repid(+) = &&RepID& AND llre.entity(+) = 'BIO$COVIB$LO$RESPONSE$'||regexp_replace(s.samplename, '^(.*PC).*$', '\1', 1, 1, 'i') AND llre.masterkey(+) =  sa.code
        AND ulre.repid(+) = &&RepID& AND ulre.entity(+) = 'BIO$COVIB$HI$RESPONSE$'||regexp_replace(s.samplename, '^(.*PC).*$', '\1', 1, 1, 'i') AND ulre.masterkey(+) =  sa.code
        AND llra.repid(+) = &&RepID& AND llra.entity(+) = 'BIO$COVIB$LO$RATIO$'||regexp_replace(s.samplename, '^(.*PC).*$', '\1', 1, 1, 'i') AND llra.masterkey(+) =  sa.code
        AND ulra.repid(+) = &&RepID& AND ulra.entity(+) = 'BIO$COVIB$HI$RATIO$'||regexp_replace(s.samplename, '^(.*PC).*$', '\1', 1, 1, 'i') AND ulra.masterkey(+) =  sa.code
        &&AddConditionS1&
      ) sample1
    WHERE nc.rid = sample1.rid
      AND nc.analyteid = sample1.analyteid
      AND nc.studyid = sample1.studyid
      &&AddCondition&
    GROUP BY nc.studyid, nc.analyteid, nc.analyteorder, nc.rid, nc.runid, nc.acceptedrun, sample1.name, nc.concentrationunit, sample1.concentrationunit, nc.meanormedian, nc.cutpointlimit, sample1.subject, sample1.dilution, nc.meanmedian_response, nc.cutpoint, nc.watson_cutpoint, nc_cv, nc_n, nc_m, sample1.flagpercent
  ))
  GROUP BY studyid, analyteid, analyteorder, acceptedrun, meanormedian, name, levelorder, nc_concunit, llre, ulre, llra, ulra
)
GROUP BY studyid
