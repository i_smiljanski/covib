SELECT XMLELEMENT("experiment", XMLATTRIBUTES('&&Experiment&' AS "name" ,'concresult' AS "type"),
      Xmlagg ( XMLConcat(
        Xmlagg (XMLELEMENT("calculation", xmlconcat(targ,val,res)) order by acceptedrun, analytename, matrix, groupno, groupname, dosegroupno, dosegroup, designsubjecttag, subjectid, samplingtime, customid, designsampleid),
        XMLELEMENT("statistic", XMLATTRIBUTES(analytename AS "analytename", matrix AS "matrix", acceptedrun AS "acceptedrun"),
               XMLELEMENT("n", count(analytename))
             )) order by studyid, acceptedrun, analytename
      ))
FROM (
  SELECT studyid, acceptedrun, analytename, matrix, subjectgroupid, treatmentid, groupname, dosegroup, groupno, dosegroupno, designsubjecttag, subjectid, designsampleid, samplingtime, customid,
    XMLELEMENT("target",
      XMLELEMENT("acceptedrun", acceptedrun),
      XMLELEMENT("subjectgroupid", subjectgroupid),
      XMLELEMENT("treatmentid", treatmentid),
      XMLELEMENT("subjectid", subjectid),
      XMLELEMENT("designsubjecttag", designsubjecttag),      
      XMLELEMENT("analytename", analytename),
      XMLELEMENT("matrix", matrix),
      XMLELEMENT("designsampleid", designSampleID),
      XMLELEMENT("samplingtime", samplingtime),
      XMLELEMENT("customid", customid)
    ) as targ,
    XMLELEMENT("values", 
      Xmlagg(
        XMLELEMENT("sample",
          XMLATTRIBUTES(sampleid AS "sampleid", sampleresultid AS "sampleresultid" ),
          XMLELEMENT("runid", rid),
          XMLELEMENT("runno", runid),
          XMLELEMENT("analyteid", analyteid),
          XMLELEMENT("runsamplesequencenumber", runsamplesequencenumber),
          XMLELEMENT("samplesubtype", samplesubtype),
          XMLELEMENT("concentration", &&FormatFuncConc&(&&RepresentationFuncConc&(conc, &&PlacesConc&), &&PlacesConc&)),
          XMLELEMENT("concentrationunit", concentrationunit),
          XMLELEMENT("status", concentrationstatus),
          XMLELEMENT("comment", resultcomment),
          XMLELEMENT("time", time),
          XMLELEMENT("dilution", dilution),
          XMLELEMENT("deactivated", isdeactivated)
        ) order by runid, runsamplesequencenumber
      )
    ) as val,
    XMLELEMENT("result",
      XMLELEMENT("m", Count(conc)),
      XMLELEMENT("n", Count(useconc)),
      XMLELEMENT("result", max(result)),
      XMLELEMENT("comment", max(resultcomment)),
      XMLELEMENT("dilution", max(dilution)),
      XMLELEMENT("mean",
        case
          when '&&ResponseFullPrecision&' = 'T' then
            FmtNum(Avg(useconc))
          when '&&FinalAccuracy&' = 'T' then
            &&FormatFuncConc&(&&RepresentationFuncConc&(Avg(useconc), &&PlacesConc&), &&PlacesConc&)
          else
            &&FormatFuncConc&(&&RepresentationFuncConc&(Avg(useconc_sig), &&PlacesConc&), &&PlacesConc&)
        end),
      XMLELEMENT("sd", &&FormatFuncResponseSD&(&&RepresentationFuncResponseSD&(case when '&&ResponseFullPrecision&' = 'T' or '&&FinalAccuracy&' = 'T' then Stddev(useconc) else Stddev(useconc_sig) end, &&PlacesResponseSD&), &&PlacesResponseSD&)),
      XMLELEMENT("cv", &&FormatFuncPercentage&(&&RepresentationFuncPercentage&(
                        case
                          when Avg(useconc) = 0 then null
                          when '&&ResponseFullPrecision&' = 'T' or '&&FinalAccuracy&' = 'T' then 
                            &&RepresentationFuncResponseSD&(Stddev(useconc), &&PlacesResponseSD&)/nullif(Avg(useconc),0)*100
                          when &&RepresentationFuncConc&(Avg(useconc_sig), &&PlacesConc&) = 0 then null 
                          else
                            &&RepresentationFuncResponseSD&(Stddev(useconc_sig), &&PlacesResponseSD&)/nullif(&&RepresentationFuncConc&(Avg(useconc_sig), &&PlacesConc&),0)*100
                        end, &&PlacesPercentage&), &&PlacesPercentage&)),
      XMLELEMENT("flag",
                case
                  when &&RepresentationFuncConc&(Avg(useconc_sig), &&PlacesConc&) = 0 then 'F'
                  when (&&RepresentationFuncPercentage&(&&RepresentationFuncResponseSD&(Stddev(useconc_sig), &&PlacesResponseSD&)/&&RepresentationFuncConc&(Avg(useconc_sig), &&PlacesConc&)*100, &&PlacesPercentage&)) > 20 then 'T'
                  else 'F'
                end
      )
    ) as res
  FROM (
    SELECT
      studyid, rid, runid, acceptedrun, analyteid, REGEXP_REPLACE(analytename, '^anti[ _-]', '', 1, 1, 'i') analytename, analyteorder, matrix,
      -- Screens:
      designsubjecttag, subjectid, designSampleID, samplingtime, time, timetext, studyday, subjectgroupid, treatmentid, groupname, dosegroup, groupno, dosegroupno, doseamount, doseunit, customid,
      posgroup, sampleid, sampleresultid, samplesubtype, runsamplesequencenumber,
      concentration conc, &&RepresentationFuncConc&(concentration, &&PlacesConc&) conc_sig,
      case when (isdeactivated='F') then concentration else null end useconc, case when (isdeactivated='F') then &&RepresentationFuncConc&(concentration, &&PlacesConc&) else null end useconc_sig,
      concentrationstatus, result, source, concentrationunit, resultcomment, dilution,
      &&RepresentationFuncConc&(Avg(&&RepresentationFuncConc&(case when(isdeactivated='F') then &&PlacesConc& else null end, &&PlacesConc&)) over (partition by studyid, analyteID, dosegroup, subjectid, designSampleID), &&PlacesConc&) mean_conc_sig,
      isdeactivated,
      count(concentration) over (partition by studyid, analyteID) m,
      count(case when (isdeactivated='F') then concentration else null end) over (partition by studyid, analyteID) n
    FROM (
          SELECT
            r.studyid,
            s.id sampleid,
            sr.id sampleresultid,
            sam.subjectid,
            sam.designsubjecttag,
            s.designsampleid,
            r.id rid,
            r.runid,
            case when ra.RunAnalyteRegressionStatus = &&RejectedRunAnalyte& then 'F' else 'T' end acceptedrun,
            sa.id analyteid,
            sa.name analytename,
            sa.analyteorder analyteorder,
            a.sampletypeid matrix,
            s.samplesubtype,
            s.runsamplesequencenumber,
            sr.concentration,
            s.status,
            sr.commenttext,
            sr.concentrationstatus,
            sr.resultcommenttext resultcomment,
            max(sr.concentrationstatus) over (partition by s.designsampleid, r.id) result,
            s.source,
            sr.concentrationunits concentrationunit,
            s.time,
            s.timetext,
            s.studyday,
            a.meanormedian,
            sam.subjectgroupid,
            sam.treatmentid,
            sg.name groupname,
            t.name dosegroup,
            s.dilution,
            max(rh.reported) over (partition by s.designsampleid, r.id) reported,
            s.customid,
            case
                when REGEXP_LIKE(sg.name, '^[+-]?\d?\.?\d+$','i') then
                    to_number(sg.name)
                else
                  null
            end groupno,
            case
                /*when REGEXP_LIKE(sg.name, '^[+-]?\d?\.?\d+$','i') then
                    to_number(sg.name)
                */
                when REGEXP_LIKE(t.name, '^[+-]?\d?\.?\d+$','i') then
                    to_number(t.name)
                when REGEXP_LIKE(t.description, '^[+-]?\d?\.?\d+$','i') then
                    to_number(t.description)
                else
                  null
            end dosegroupno,
            t.doseamount,
            t.doseunitsdescription doseunit,
            sam.samplingtime,
            s.samplesubtype posgroup,
            case
              when s.runcode || '-' || s.runsamplesequencenumber || '-' || sa.analyteorder in (select code from &&TempTabPrefix&bio$deactivated$samples) then 'T'
              else 'F'
            end isdeactivated
          FROM 
            &&TempTabPrefix&bio$run r,
            &&TempTabPrefix&bio$run$analytes ra,
            &&TempTabPrefix&bio$study$analytes sa,
            &&TempTabPrefix&bio$assay a,
            &&TempTabPrefix&bio$run$worklist s,
            &&TempTabPrefix&bio$run$worklist$result$rw sr,
            &&TempTabPrefix&bio$sample sam,
            &&TempTabPrefix&bio$study$subjectgroup sg,
            &&TempTabPrefix&bio$study$treatment t,
            &&TempTabPrefix&bio$reassay$history rh,
            table(&&LALPackage&.GetRunStates) rs
          WHERE r.ID = s.runID
          AND s.ID = sr.worklistID
          AND a.ID = r.assayID
          AND s.designSampleID = sam.ID
          AND r.studyid = '&&StudyID&'
          AND a.studyID = '&&StudyID&'
          AND rs.column_value = r.runstatusnum
          AND sr.runAnalyteCode = ra.code
          AND r.ID = ra.runID
          AND ra.analyteID = sa.ID
          AND sam.subjectgroupcode = sg.code
          AND sam.treatmentcode = t.code
          --s.runcode || '-' || s.runsamplesequencenumber || '-' || sa.analyteorder in (select code from &&TempTabPrefix&bio$deactivated$samples)
          AND r.code || '-' || s.runsamplesequencenumber || '-' || sa.analyteorder = rh.runsamplecode (+)
          &&AddCondition&
    ) sample
  ) 
  GROUP BY studyid, acceptedrun, analytename, matrix, subjectgroupid, treatmentid, groupname, dosegroup, groupno, dosegroupno, designsubjecttag, subjectid, designsampleid, samplingtime, customid)
GROUP BY studyid, acceptedrun, analytename, matrix