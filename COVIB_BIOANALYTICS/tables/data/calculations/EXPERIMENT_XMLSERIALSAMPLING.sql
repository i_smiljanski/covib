  SELECT XMLELEMENT("experiment", XMLATTRIBUTES('&&Experiment&' AS "name" ,'xmlserialsampling' AS "type"),
      Xmlagg ( 
        Xmlagg (XMLELEMENT("calculation", xmlconcat(targ,val)) order by studyid, designsubjecttagno, designsubjecttag, gender, dosegroupno, dosegroup, biomatrix, studyday, endhour)
         order by studyid
      ))
  FROM (
  SELECT studyid, designsubjecttagno, designsubjecttag, gender, dosegroupno, dosegroup, biomatrix, studyday, endhour,
    XMLELEMENT("target",
      XMLATTRIBUTES(subjectid AS "subjectid"),
      XMLELEMENT("designsubjecttag", designsubjecttag),
      XMLELEMENT("gender", gender),
      XMLELEMENT("dosegroup", dosegroup),
      XMLELEMENT("biomatrix", biomatrix),
      XMLELEMENT("studyday", studyday),
      XMLELEMENT("endhour", endhour)
    ) as targ,
    XMLELEMENT("values", 
      Xmlagg(
        XMLELEMENT("analyte",
          XMLATTRIBUTES(analyteid AS "analyteid"),
          XMLELEMENT("concentrationunit", concentrationunit),
          XMLELEMENT("analytename", analytename),
          XMLELEMENT("concentration", &&ConcFormatFunc&(conc_sigrnd, &&ConcFigures&)),
          XMLELEMENT("calibrationrange", &&ConcFormatFunc&(calibrationrangeconv, &&ConcFigures&)),
          XMLELEMENT("concentrationstatus", concentrationstatus)
        ) order by concentrationunit, analytename
      )
    ) as val
  FROM (
    SELECT
      r.studyid,
      s.id sampleid,
      sr.id sampleresultid,
      sam.subjectid,
      sam.designsubjecttag,
      s.designsampleid,
      sam.endday studyday,
      sam.endhour,
      bm.matrix biomatrix,
      sub.gender,
      r.id rid,
      r.runid,
      r.runstartdate,
      sa.id analyteid,
      sa.name analytename,
      s.samplesubtype,
      s.runsamplesequencenumber,
      s.samplename,
      s.exportsamplename,
      s.dilution,
      &&ConcRepresentationFunc&(s.concentration, &&ConcFigures&) nominalconc_sigrnd,
      s.concentration nominalconc,
      sr.analytearea analytearea,
      sr.analyteareastatus analyteareastatus,
      sr.analyteheight analyteheight,
      sr.analyteheightstatus analyteheightstatus,
      sr.analytepeakretentiontime analyteretentiontime,
      sr.internalstandardarea internalstandardarea,
      sr.internalstandardareastatus internalstandareastatus,
      sr.internalstandardheight internalstandheight,
      sr.internalstandardheightstatus internalstandheightstatus,
      sr.internalstandardretentiontime internalstandardretentiontime,
      s.status,
      sr.commenttext,
      &&ConcRepresentationFunc&(sr.concentration, &&ConcFigures&) conc_sigrnd,
      sr.concentration conc,
      nvl(s.concentrationunit, sa.concentrationunits) concentrationunit,
      sr.concentrationstatus,
      &&ConcRepresentationFunc&(samr.calibrationrangeconv, &&ConcFigures&) calibrationrangeconv,
      case when s.concentration = 0 then null
           else round((&&ConcRepresentationFunc&(sr.concentration, &&ConcFigures&)-&&ConcRepresentationFunc&(s.concentration, &&ConcFigures&))*100/
                       &&ConcRepresentationFunc&(s.concentration, &&ConcFigures&), &&DecPlPercBias&)
      end bias_sigrnd,
      case when s.concentration = 0 then null
           else (sr.concentration-s.concentration)*100/
                 s.concentration
      end bias,
      max(sr.concentrationstatus) over (partition by s.designsampleid, r.id) result,
      s.source,
      s.time,
      s.timetext,
      t.name dosegroup,
      max(rh.reported) over (partition by s.designsampleid, r.id) reported,
      case
          when LENGTH(TRIM(TRANSLATE(sam.designsubjecttag, ' +-.0123456789', ' '))) is null and TRIM(sam.designsubjecttag) != '-' then
              to_number(sam.designsubjecttag)
          else
            null
      end designsubjecttagno,
      case
          /*when REGEXP_LIKE(sg.name, '^[+-]?\d?\.?\d+$','i') then
              to_number(sg.name)*/
          when REGEXP_LIKE(t.name, '^[+-]?\d?\.?\d+$','i') and TRIM(t.name) != '-' then
              to_number(t.name)
          when REGEXP_LIKE(t.description, '^[+-]?\d?\.?\d+$','i') and TRIM(t.description) != '-' then
              to_number(t.description)
          else
            null
      end dosegroupno,
      t.doseamount,
      t.doseunitsdescription doseunit,
      sam.samplingtime,
      s.samplesubtype posgroup,
      case
        when s.runcode || '-' || s.runsamplesequencenumber || '-' || sa.analyteorder in (select code from &&TempTabPrefix&bio$deactivated$samples) then 'T'
        else 'F'
      end isdeactivated
    FROM 
      &&TempTabPrefix&bio$run r,
      &&TempTabPrefix&bio$run$analytes ra,
      &&TempTabPrefix&bio$study$analytes sa,
      &&TempTabPrefix&bio$assay a,
      &&TempTabPrefix&bio$run$worklist s,
      &&TempTabPrefix&bio$run$worklist$result$rw sr,
      &&TempTabPrefix&bio$sample sam,
      &&TempTabPrefix&bio$sample$results samr,
      &&TempTabPrefix&bio$study$subject sub,
      &&TempTabPrefix&bio$study$treatment t,
      &&TempTabPrefix&bio$reassay$history rh,
      &&TempTabPrefix&BIO$BIOMATRIX BM,
      table(&&LALPackage&.GetRunStates) rs
    WHERE r.ID = s.runID
    AND s.ID = sr.worklistID
    AND a.ID = r.assayID
    AND s.designSampleID = sam.ID
    AND sam.subjectID = sub.ID
    AND sam.subjectgrouptreatmentid = bm.subjectgrouptreatmentid
    AND sam.sampletypesplitkey = bm.sampletypesplitkey
    AND r.studyid = '&&StudyID&'
    AND a.studyID = '&&StudyID&'
    AND BM.STUDYID = '&&StudyID&'
    AND sam.id = samr.sampleid
    AND sa.ID = samr.analyteID
    AND r.runID = samr.runID
    AND rs.column_value = r.runstatusnum
    AND sr.runAnalyteCode = ra.code
    AND r.ID = ra.runID
    AND ra.analyteID = sa.ID
    AND sam.treatmentcode = t.code
    --s.runcode || '-' || s.runsamplesequencenumber || '-' || sa.analyteorder in (select code from &&TempTabPrefix&bio$deactivated$samples)
    AND r.code || '-' || s.runsamplesequencenumber || '-' || sa.analyteorder = rh.runsamplecode (+)
    &&AddCondition&
  )
  GROUP BY studyid, subjectid, designsubjecttagno, designsubjecttag, gender, dosegroupno, dosegroup, biomatrix, studyday, endhour)
GROUP BY studyid 
