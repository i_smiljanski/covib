  SELECT XMLELEMENT("experiment", XMLATTRIBUTES('&&Experiment&' AS "name" ,'qc_dil' AS "type"),
    Xmlagg (XMLConcat(     
     Xmlagg (calcs order by knownname),             
     XMLELEMENT("statistic", XMLATTRIBUTES(assayanalyteid AS "assayanalyteid"),
       XMLELEMENT("flagpercent", min(flagpercent)),
       XMLELEMENT("min-bias", FormatRounded(round(min(bias_kn),&&DecPlPercBias&),&&DecPlPercBias&)),
       XMLELEMENT("max-bias", FormatRounded(round(max(bias_kn),&&DecPlPercBias&),&&DecPlPercBias&)),
       XMLELEMENT("mean-bias", FormatRounded(avg(bias_kn),&&DecPlPercBias&)),
       XMLELEMENT("min-bias-wo-lloq", FormatRounded(round(min(bias_wo_lloq),&&DecPlPercBias&),&&DecPlPercBias&)),
       XMLELEMENT("max-bias-wo-lloq", FormatRounded(round(max(bias_wo_lloq),&&DecPlPercBias&),&&DecPlPercBias&)),
       XMLELEMENT("mean-bias-wo-lloq", FormatRounded(round(avg(bias_wo_lloq),&&DecPlPercBias&),&&DecPlPercBias&)),               
       XMLELEMENT("min-bias-wo-uloq", FormatRounded(round(min(bias_wo_uloq),&&DecPlPercBias&),&&DecPlPercBias&)),
       XMLELEMENT("max-bias-wo-uloq", FormatRounded(round(max(bias_wo_uloq),&&DecPlPercBias&),&&DecPlPercBias&)),               
       XMLELEMENT("mean-bias-wo-uloq", FormatRounded(round(avg(bias_wo_uloq),&&DecPlPercBias&),&&DecPlPercBias&)),               
       XMLELEMENT("min-biasonmean", FormatRounded(round(min(biasonmean_kn),&&DecPlPercBias&),&&DecPlPercBias&)),
       XMLELEMENT("max-biasonmean", FormatRounded(round(max(biasonmean_kn),&&DecPlPercBias&),&&DecPlPercBias&)),
       XMLELEMENT("mean-biasonmean", FormatRounded(avg(biasonmean_kn),&&DecPlPercBias&)),
       XMLELEMENT("min-biasonmean-wo-lloq", FormatRounded(round(min(biasonmean_wo_lloq),&&DecPlPercBias&),&&DecPlPercBias&)),
       XMLELEMENT("max-biasonmean-wo-lloq", FormatRounded(round(max(biasonmean_wo_lloq),&&DecPlPercBias&),&&DecPlPercBias&)),
       XMLELEMENT("mean-biasonmean-wo-lloq", FormatRounded(round(avg(biasonmean_wo_lloq),&&DecPlPercBias&),&&DecPlPercBias&)),               
       XMLELEMENT("min-biasonmean-wo-uloq", FormatRounded(round(min(biasonmean_wo_uloq),&&DecPlPercBias&),&&DecPlPercBias&)),
       XMLELEMENT("max-biasonmean-wo-uloq", FormatRounded(round(max(biasonmean_wo_uloq),&&DecPlPercBias&),&&DecPlPercBias&)),               
       XMLELEMENT("mean-biasonmean-wo-uloq", FormatRounded(round(avg(biasonmean_wo_uloq),&&DecPlPercBias&),&&DecPlPercBias&)),                         
       XMLELEMENT("min-cv", FormatRounded(round(min(cv),&&DecPlPercCV&),&&DecPlPercCV&)),
       XMLELEMENT("max-cv", FormatRounded(round(max(cv),&&DecPlPercCV&),&&DecPlPercCV&)),
       XMLELEMENT("mean-cv", FormatRounded(round(avg(cv),&&DecPlPercCV&),&&DecPlPercCV&)),
       XMLELEMENT("numacc", sum(cntunflagged)),
       XMLELEMENT("numtotal", sum(cnt)),
       XMLELEMENT("percacc", case when sum(cnt) != 0 then FmtNum(round(sum(cntunflagged)/sum(cnt)*100,&&DecPlPercStat&)) else null end),    
       XMLELEMENT("lloq",
         XMLELEMENT("name", min(lloqname)),
         XMLELEMENT("knownconc", &&ConcFormatFunc&(min(lloq_knownconc),&&ConcFigures&))
         ),
       XMLELEMENT("uloq",
         XMLELEMENT("name", min(uloqname)),
         XMLELEMENT("knownconc", &&ConcFormatFunc&(min(uloq_knownconc),&&ConcFigures&))
         )
       ),
     Xmlagg (overalls)
     )))
  FROM   
   (SELECT assayanalyteid, 
      knownname,
      min(cnt) cnt,
      min(cntunflagged) cntunflagged,
      min(mean) mean,
      min(sd) sd,
      min(cv) cv,
      min(flagpercent_kn) flagpercent,
      min(bias_wo_lloq) bias_wo_lloq,
      min(bias_wo_uloq) bias_wo_uloq,
      min(bias_kn) bias_kn,
      min(biasonmean_wo_lloq) biasonmean_wo_lloq,
      min(biasonmean_wo_uloq) biasonmean_wo_uloq,
      min(biasonmean_kn) biasonmean_kn,      
      min(lloqname) lloqname, 
      min(uloqname) uloqname,
      min(lloq_knownconc) lloq_knownconc,
      min(uloq_knownconc) uloq_knownconc,
      XMLELEMENT("overall", XMLATTRIBUTES(assayanalyteid AS "assayanalyteid", knownname AS "knownname"),
        XMLConcat( 
          XMLELEMENT("n", min(cnt)),
          XMLELEMENT("cntunflagged", min(cntunflagged)),
          XMLELEMENT("mean", &&ConcFormatFunc&(&&ConcRepresentationFunc&(min(mean),&&ConcFigures&),&&ConcFigures&)),
          XMLELEMENT("sd", &&ConcFormatFunc&(&&ConcRepresentationFunc&(min(sd),&&ConcFigures&),&&ConcFigures&)),
          XMLELEMENT("cv", FormatRounded(round(min(cv),&&DecPlPercCV&),&&DecPlPercCV&)),
          XMLELEMENT("bias", FormatRounded(round(min(bias_kn),&&DecPlPercBias&),&&DecPlPercBias&)),
          XMLELEMENT("biasonmean", FormatRounded(round(min(biasonmean_kn),&&DecPlPercBias&),&&DecPlPercBias&)),
          XMLELEMENT("flagpercent", min(flagpercent_kn))          
        )) overalls,
        Xmlagg (XMLELEMENT("calculation", xmlconcat(targ,val,res)) order by assayanalyteid, knownname, runid, dilutionfactor) calcs
    from
     (select knownname, min(lloq_knownconc_sigrnd) mil,max(lloq_knownconc_sigrnd) mal,
        assayanalyteid,     
        min(cnt) cnt,
        min(cntunflagged) cntunflagged,
        min(mean) mean,
        min(sd) sd,
        min(cv) cv,
        min(flagpercent_kn) flagpercent_kn,
        min(bias_wo_lloq) bias_wo_lloq,
        min(bias_wo_uloq) bias_wo_uloq,
        min(bias_kn) bias_kn,
        min(biasonmean_wo_lloq) biasonmean_wo_lloq,
        min(biasonmean_wo_uloq) biasonmean_wo_uloq,        
        min(biasonmean_kn) biasonmean_kn,
        min(lloqname) lloqname, 
        min(uloqname) uloqname,
        min(lloq_knownconc) lloq_knownconc,
        min(uloq_knownconc) uloq_knownconc,
        dilutionfactor, runid,
        XMLELEMENT("target", 
          XMLELEMENT("assayanalyteid", assayanalyteid), XMLELEMENT("name", knownname) , XMLELEMENT("dilutionfactor", dilutionfactor) ,
            XMLELEMENT("runid", runid)) as targ,
        XMLELEMENT("values", Xmlagg(
          XMLELEMENT("sample", XMLATTRIBUTES(sampleid AS "sampleid", sampleresultid AS "sampleresultid" ),
            XMLELEMENT("seqnum", runsamplesequencenumber),          
            XMLELEMENT("concentration", &&ConcFormatFunc&(conc_sigrnd,&&ConcFigures&)),
            XMLELEMENT("knownconc", &&ConcFormatFunc&(knownconc_sigrnd,&&ConcFigures&)),
            XMLELEMENT("nominalconc", &&ConcFormatFunc&(nominalconc_sigrnd,&&ConcFigures&)),
            XMLELEMENT("subset", subset),        
            XMLELEMENT("bias", FormatRounded(bias_sigrnd,&&DecPlPercBias&)),
            XMLELEMENT("concentrationstatus", concentrationstatus),
            XMLELEMENT("resultcommenttext", resultcommenttext),
            XMLELEMENT("flag", flagged),
            XMLELEMENT("deactivated", isdeactivated))          
            order by runnr, runsamplesequencenumber
            )) as val,
        XMLELEMENT("result",       
          XMLELEMENT("n", Count(useconc)),
          XMLELEMENT("mean", &&ConcFormatFunc&(&&ConcRepresentationFunc&(Avg(
                             case when '&&FinalAccuracy&'='T' then useconc else useconc_sigrnd 
                             end),&&ConcFigures&),&&ConcFigures&)),
          XMLELEMENT("sd",&&ConcFormatFunc&(&&ConcRepresentationFunc&(Stddev(
                          case when '&&FinalAccuracy&'='T' then useconc 
                          else useconc_sigrnd 
                          end),&&ConcSDFigures&),&&ConcSDFigures&)),
          XMLELEMENT("cv", case when '&&FinalAccuracy&'='T' then FormatRounded(round(Stddev(useconc)/Avg(useconc)*100,&&DecPlPercCV&),&&DecPlPercCV&) 
                           else FormatRounded(round(&&ConcRepresentationFunc&(Stddev(useconc_sigrnd),&&ConcFigures&)/&&ConcRepresentationFunc&(Avg(useconc_sigrnd),&&ConcFigures&)*100,&&DecPlPercCV&),&&DecPlPercCV&)
                           end),
          XMLELEMENT("bias", FormatRounded(round(Avg(case when '&&FinalAccuracy&'='T' then usebias else usebias_sigrnd end),&&DecPlPercBias&),&&DecPlPercBias&)),
          XMLELEMENT("biasonmean", FormatRounded(round(case when knownconc = 0 then null
                                                            when '&&FinalAccuracy&'='T' then (Avg(useconc)-knownconc)/knownconc*100
                                                            else (&&ConcRepresentationFunc&(Avg(useconc_sigrnd),&&ConcFigures&)-knownconc_sigrnd)/knownconc_sigrnd*100
                                                       end,&&DecPlPercBias&),&&DecPlPercBias&)),
          XMLELEMENT("totalerror", 
                      case when '&&FinalAccuracy&'='T' 
                           then FormatRounded(round(abs(Stddev(useconc)/Avg(useconc)*100) + 
                                              abs(Avg(usebias)),
                                              greatest(&&DecPlPercCV&,&&DecPlPercBias&)),greatest(&&DecPlPercCV&,&&DecPlPercBias&))
                           else FormatRounded(abs(round(&&ConcRepresentationFunc&(Stddev(useconc_sigrnd),&&ConcFigures&)/
                                                        &&ConcRepresentationFunc&(Avg(useconc_sigrnd),&&ConcFigures&)*100,&&DecPlPercCV&))+
                                              abs(round(Avg(usebias_sigrnd),&&DecPlPercBias&)),
                                              greatest(&&DecPlPercCV&,&&DecPlPercBias&))                          
                      end),
          XMLELEMENT("flagpercent", min(flagpercent))
          ) as res
    FROM
     (SELECT knownID, sampleID, sampleresultID, NAME, knownname, subset, knownconc_sigrnd_m, knownconc_m,
      Count(useconc) over (partition by assayanalyteid, knownname) as cnt,
      count(unflagged) over (partition by assayanalyteid, knownname) as cntunflagged,
      &&ConcRepresentationFunc&(Avg(case when '&&FinalAccuracy&'='T' then useconc else useconc_sigrnd end) over (partition by assayanalyteid, knownname),&&ConcFigures&) as mean, 
      &&ConcRepresentationFunc&(Stddev(case when '&&FinalAccuracy&'='T' then useconc else useconc_sigrnd end) over (partition by assayanalyteid, knownname),&&ConcSDFigures&) as sd,
      case when '&&FinalAccuracy&'='T' then Stddev(useconc) over (partition by assayanalyteid, knownname)/Avg(useconc) over (partition by assayanalyteid, knownname)*100
           else round(&&ConcRepresentationFunc&(Stddev(useconc_sigrnd) over (partition by assayanalyteid, knownname),&&ConcFigures&)/
                      &&ConcRepresentationFunc&(Avg(useconc_sigrnd) over (partition by assayanalyteid, knownname),&&ConcFigures&)*100,&&DecPlPercCV&)
      end as cv,           
      --Avg(case when '&&FinalAccuracy&'='T' then usebias else round(usebias_sigrnd,&&DecPlPercBias&) end) over (partition by assayanalyteid, knownname) as bias, 
      min(flagpercent) over (partition by assayanalyteid) flagpercent_kn,            
      case when knownconc = 0 then null
           else round(case when '&&FinalAccuracy&'='T' then (Avg(useconc) over (partition by assayanalyteid)-knownconc)/knownconc*100
                           else (&&ConcRepresentationFunc&(Avg(useconc_sigrnd) over (partition by assayanalyteid),&&ConcFigures&)-knownconc_sigrnd)/knownconc_sigrnd*100
                      end,&&DecPlPercBias&) 
      end as biasonmean,
      case when name = min(lloqname) over (partition by assayanalyteid) then null 
          else case when '&&FinalAccuracy&'='T' then Avg(usebias) over (partition by assayanalyteid)     
               else round(Avg(usebias_sigrnd) over (partition by assayanalyteid),&&DecPlPercBias&) end 
      end as bias_wo_lloq,
      case when name = min(lloqname) over (partition by assayanalyteid) then null 
           when knownconc = 0 then null
           else case when '&&FinalAccuracy&'='T' then (Avg(useconc) over (partition by assayanalyteid)-knownconc)/knownconc*100
                     else round((&&ConcRepresentationFunc&(Avg(useconc_sigrnd) over (partition by assayanalyteid),&&ConcFigures&)-knownconc_sigrnd)/knownconc_sigrnd*100,&&DecPlPercBias&) 
                end 
      end as biasonmean_wo_lloq,
      case when name = min(uloqname) over (partition by assayanalyteid) then null 
      else case when '&&FinalAccuracy&'='T' then Avg(usebias) over (partition by assayanalyteid)
          else round(Avg(usebias_sigrnd) over (partition by assayanalyteid),&&DecPlPercBias&) end  
      end as bias_wo_uloq,
      case when name = min(uloqname) over (partition by assayanalyteid) then null 
           when knownconc = 0 then null
           else case when '&&FinalAccuracy&'='T' then (Avg(useconc) over (partition by assayanalyteid)-knownconc)/knownconc*100
                     else round((&&ConcRepresentationFunc&(Avg(useconc_sigrnd) over (partition by assayanalyteid),&&ConcFigures&)-knownconc_sigrnd)/knownconc_sigrnd*100,&&DecPlPercBias&) 
                end
      end as biasonmean_wo_uloq,
      case when '&&FinalAccuracy&'='T' then Avg(usebias) over (partition by assayanalyteid, knownname)
      else round(Avg(usebias_sigrnd) over (partition by assayanalyteid, knownname),&&DecPlPercBias&) end as bias_kn,     
      case when knownconc = 0 then null
           else round(case when 'F'='T' then (Avg(useconc) over (partition by assayanalyteid, knownname)- knownconc)/knownconc*100
                           else (sigFigure(Avg(useconc_sigrnd) over (partition by assayanalyteid, knownname),3)-knownconc_sigrnd)/knownconc_sigrnd*100
                      end,&&DecPlPercBias&) 
      end as biasonmean_kn,       
      case when '&&FinalAccuracy&'='T' then min(lloq_knownconc) over (partition by assayanalyteid, knownname) 
          else min(lloq_knownconc_sigrnd) over (partition by assayanalyteid, knownname) 
      end lloq_knownconc, 
      case when '&&FinalAccuracy&'='T' then min(uloq_knownconc) over (partition by assayanalyteid, knownname) 
           else min(uloq_knownconc_sigrnd) over (partition by assayanalyteid, knownname) 
      end uloq_knownconc,     
      assayanalyteid, dilutionfactor, resultcommenttext,
      lloq_knownconc_sigrnd, uloq_knownconc_sigrnd, --lloq_knownconc, --uloq_knownconc,
      lloqname, uloqname,           
      conc, conc_sigrnd, bias, bias_sigrnd, flagpercent, ISDEACTIVATED, runid, runnr, runsamplesequencenumber,
      useconc_sigrnd, usebias_sigrnd, useconc, usebias, flagged, unflagged,
      knownconc, knownconc_sigrnd, nominalconc, nominalconc_sigrnd, concentrationstatus
      FROM
       (SELECT knownID, sampleID, sampleresultID, NAME, knownname, subset, knownconc_sigrnd_m, knownconc_m,              
        assayanalyteid, dilutionfactor, resultcommenttext,
        MIN(knownconc_sigrnd) over (partition by assayanalyteid) lloq_knownconc_sigrnd,
        MAX(knownconc_sigrnd) over (partition by assayanalyteid) uloq_knownconc_sigrnd,  
        MIN(knownconc) over (partition by assayanalyteid) lloq_knownconc,
        MAX(knownconc) over (partition by assayanalyteid) uloq_knownconc,  
        MIN(knownname) KEEP (DENSE_RANK FIRST ORDER BY knownconc) over (partition by assayanalyteid) as lloqname, 
        MIN(knownname) KEEP (DENSE_RANK LAST ORDER BY knownconc) over (partition by assayanalyteid) as uloqname,             
        conc, conc_sigrnd, bias, bias_sigrnd, flagpercent, ISDEACTIVATED, runid, runnr, runsamplesequencenumber,
        case when ISDEACTIVATED='T' then null else conc_sigrnd end useconc_sigrnd,
        case when ISDEACTIVATED='T' then null else bias_sigrnd end usebias_sigrnd,
        case when ISDEACTIVATED='T' then null else conc end useconc,
        case when ISDEACTIVATED='T' then null else bias end usebias,
        case when abs(bias) > flagpercent then '&&FlaggedSymbol&' else null end flagged,
        case when ISDEACTIVATED <> 'T' and abs(bias) <= flagpercent then 1 else null end unflagged
        ,knownconc, knownconc_sigrnd, nominalconc, nominalconc_sigrnd, concentrationstatus
        FROM
         (SELECT K.ID knownID, s.ID sampleID, sr.ID sampleresultID, K.NAME, k.nameprefix knownname, k.namesuffix subset, 
          s.ISDEACTIVATED, s.dilutionfactor,
          max(&&ConcRepresentationFunc&(sr.knownconcentrationconv,&&ConcFigures&)) over (partition by sr.assayanalyteid, k.nameprefix, s.runid) knownconc_sigrnd_m,
          max(sr.knownconcentrationconv) over (partition by sr.assayanalyteid, k.nameprefix, s.runid) knownconc_m,
          &&ConcRepresentationFunc&(sr.knownconcentrationconv,&&ConcFigures&) knownconc_sigrnd,
          sr.knownconcentrationconv knownconc, s.runsamplesequencenumber,
          &&ConcRepresentationFunc&(sr.nominalconcentrationconv,&&ConcFigures&) nominalconc_sigrnd,
          sr.nominalconcentrationconv nominalconc, sr.resultcommenttext,  
          k.flagpercent, s.runid, r.runid runnr, sr.assayanalyteid,   
          &&ConcRepresentationFunc&(sr.concentrationconv,&&ConcFigures&) conc_sigrnd,
          sr.concentrationconv conc, sr.concentrationstatus,
          case when sr.knownconcentrationconv = 0 then null
               else round((&&ConcRepresentationFunc&(sr.concentrationconv,&&ConcFigures&)-&&ConcRepresentationFunc&(sr.knownconcentrationconv,&&ConcFigures&))*100/
                           &&ConcRepresentationFunc&(sr.knownconcentrationconv,&&ConcFigures&),&&DecPlPercBias&)
          end bias_sigrnd,
          case when sr.knownconcentrationconv = 0 then null
               else (sr.concentrationconv-sr.knownconcentrationconv)*100/
                     sr.knownconcentrationconv
          end bias
          FROM &&TempTabPrefix&wt$assay$ana$known K,
               &&TempTabPrefix&wt$run r,
               &&TempTabPrefix&wt$run$analytes ra,
               &&TempTabPrefix&wt$run$samples s,
               &&TempTabPrefix&wt$run$sample$results sr, 
               isr$crit kwz,
               table(&&LALPackage&.GetRunStates) rs
          WHERE K.ID=s.knownID
            AND R.ID = s.runid AND R.ID = sr.runid
            AND r.studyassayid =s.studyassayid AND s.studyassayid = sr.studyassayid      
            AND r.RuntypeDescription in (&&RunTypes&)
            AND s.ID=sr.runsampleID
            AND K.knowntype = '&&KnownType&'
            AND s.sampletype='known'
            AND s.runsamplekind = '&&KnownType&'
            AND K.knowntype = s.runsamplekind
            AND ra.assayanalyteid = sr.assayanalyteid
            AND ra.runid = s.runid
            AND K.studyid='&&StudyID&' AND s.studyid='&&StudyID&' 
            AND sr.studyID='&&StudyID&' AND r.studyid = '&&StudyID&'
            AND ra.studyID='&&StudyID&'
            AND rs.column_value=r.runstatusnum 
            and kwz.key=K.NAMEPREFIX
            and kwz.entity = '&&KnownEntity&'

            and kwz.masterkey = '&&ExpMasterkey&'
            and repid=&&RepID&     
            &&AddCondition&
       )))     
    GROUP BY assayanalyteid, knownname, dilutionfactor , runid, knownconc, knownconc_sigrnd)
  GROUP BY assayanalyteid, knownname)
  GROUP BY assayanalyteid