  SELECT XMLELEMENT("experiment", XMLATTRIBUTES('&&Experiment&' AS "name" ,'high_dose' AS "type"),
  (SELECT Xmlagg (XMLELEMENT("calculation", 
      XMLELEMENT("target", 
        XMLELEMENT("assayanalyteid", assayanalyteid), 
        XMLELEMENT("name", knownname), 
        XMLELEMENT("runid", runid),
        XMLELEMENT("dilutionfactor", dilutionfactor)),
      XMLELEMENT("values", Xmlagg(
        XMLELEMENT("sample", XMLATTRIBUTES(sampleresultid AS "sampleresultid" ),  
          XMLELEMENT("subset", subset), 
          XMLELEMENT("concentration", &&ConcFormatFunc&(conc,&&ConcFigures&)),   
          XMLELEMENT("originalconcentration", &&ConcFormatFunc&(origconc,&&ConcFigures&)),
          XMLELEMENT("knownconc", &&ConcFormatFunc&(&&ConcRepresentationFunc&(knownconc,&&ConcFigures&),&&ConcFigures&)),
          XMLELEMENT("nominalconc", &&ConcFormatFunc&(&&ConcRepresentationFunc&(nominalconc,&&ConcFigures&),&&ConcFigures&)),                   
          XMLELEMENT("bias", FormatRounded(bias,&&DecPlPercBias&)),
          XMLELEMENT("odval", &&PeakFormatFunc&(analytearea,&&PeakFigures&)),          
          XMLELEMENT("concentrationstatus", concentrationstatus),  
          XMLELEMENT("resultcommenttext", resultcommenttext),   
          XMLELEMENT("flag", flagged),     
          XMLELEMENT("deactivated", isdeactivated)
        ))),
      XMLELEMENT("result",       
        XMLELEMENT("n", Count(useconc)),
        XMLELEMENT("mean", &&ConcFormatFunc&(&&ConcRepresentationFunc&(Avg(useconc),&&ConcFigures&),&&ConcFigures&)),
        XMLELEMENT("meanorig", &&ConcFormatFunc&(&&ConcRepresentationFunc&(Avg(useorigconc),&&ConcFigures&),&&ConcFigures&)),
        XMLELEMENT("cv", FormatRounded(round(case when '&&FinalAccuracy&'='T' then Stddev(useconc)/Avg(useconc)*100
                                                  else &&ConcRepresentationFunc&(Stddev(useconc),&&ConcFigures&)
                                                      /&&ConcRepresentationFunc&(Avg(useconc),&&ConcFigures&)*100
                                             end,&&DecPlPercCV&),&&DecPlPercCV&)),                                                   
        XMLELEMENT("bias", FormatRounded(round(Avg(usebias),&&DecPlPercBias&),&&DecPlPercBias&)),
        XMLELEMENT("biasonmean", FormatRounded(round(case when knownconc = 0 then null
                                                    when '&&FinalAccuracy&'='T' then (Avg(useconc)-knownconc)/knownconc*100
                                                    else (&&ConcRepresentationFunc&(Avg(useconc),&&ConcFigures&)-knownconc)/knownconc*100
                                               end,&&DecPlPercBias&),&&DecPlPercBias&)),        
        XMLELEMENT("meanod", &&PeakFormatFunc&(&&PeakRepresentationFunc&(Avg(useanalytearea),&&PeakFigures&),&&PeakFigures&)),  
        XMLELEMENT("cvod", FormatRounded(round(case when '&&FinalAccuracy&'='T' then Stddev(useanalytearea)/Avg(useanalytearea)*100
                                                  else &&PeakRepresentationFunc&(Stddev(useanalytearea),&&PeakFigures&)
                                                      /&&PeakRepresentationFunc&(Avg(useanalytearea),&&PeakFigures&)*100
                                             end,&&DecPlPercCV&),&&DecPlPercCV&)),        
        XMLELEMENT("target", &&ConcFormatFunc&(min(target),&&ConcFigures&)),      
        XMLELEMENT("flagpercent", min(flagpercent))
        )
    ) order by runid, knownname, dilutionfactor)
    FROM
    (SELECT knownID, sampleID, sampleresultID, isdeactivated, concentrationstatus, resultcommenttext,
     Name, knownname, subset, conc, useconc, origconc, useorigconc, knownconc, nominalconc, runid, assayanalyteid, 
     bias, usebias, flagpercent, dilutionfactor, analytearea, useanalytearea,
     &&ConcRepresentationFunc&(knownconc * aliquotfactor,&&ConcFigures&) target,
     case when abs(bias) > flagpercent then '&&FlaggedSymbol&' else null end flagged
     FROM
     (SELECT K.ID knownID, s.ID sampleID, sr.ID sampleresultID, s.isdeactivated, 
        K.NAME,  s.runid, sr.concentrationstatus, sr.resultcommenttext,
        case when '&&FinalAccuracy&'='T' then max(sr.knownconcentrationconv) 
                                              over (partition by sr.assayanalyteid, k.nameprefix, s.runid) 
             else max(&&ConcRepresentationFunc&(sr.knownconcentrationconv,&&ConcFigures&)) 
                  over (partition by sr.assayanalyteid, k.nameprefix, s.runid) 
        end knownconc,
        &&ConcRepresentationFunc&(sr.nominalconcentrationconv,&&ConcFigures&) nominalconc,
        k.nameprefix knownname, k.namesuffix subset, s.dilutionfactor, aliquotfactor,
        sr.assayanalyteid, r.runtypedescription, k.flagpercent,        
        &&ConcRepresentationFunc&(sr.concentrationconv,&&ConcFigures&) conc,
        case when ISDEACTIVATED = 'T' then null
             else case when '&&FinalAccuracy&'='T' then sr.concentrationconv
                  else &&ConcRepresentationFunc&(sr.concentrationconv,&&ConcFigures&) 
             end
        end useconc,
        &&ConcRepresentationFunc&(sr.concentrationconv*aliquotfactor,&&ConcFigures&) origconc,
        case when ISDEACTIVATED = 'T' then null
             else case when '&&FinalAccuracy&'='T' then sr.concentrationconv*aliquotfactor
                  else &&ConcRepresentationFunc&(sr.concentrationconv*aliquotfactor,&&ConcFigures&) 
             end
        end useorigconc,
        &&PeakRepresentationFunc&(srw.analytearea,&&PeakFigures&) analytearea,          
        case when ISDEACTIVATED = 'T' then null
             else case when '&&FinalAccuracy&'='T' then srw.analytearea
                       else &&PeakRepresentationFunc&(srw.analytearea,&&PeakFigures&) 
                  end 
        end useanalytearea,        
        case when sr.knownconcentrationconv = 0 then null
             else round((&&ConcRepresentationFunc&(sr.concentrationconv,&&ConcFigures&)-
                     max(&&ConcRepresentationFunc&(sr.knownconcentrationconv,&&ConcFigures&)) 
                     over (partition by sr.assayanalyteid, k.nameprefix, s.runid))*100
                    /max(&&ConcRepresentationFunc&(sr.knownconcentrationconv,&&ConcFigures&)) 
                     over (partition by sr.assayanalyteid, k.nameprefix, s.runid),&&DecPlPercBias&)                  
        end bias,        
        case when ISDEACTIVATED = 'T' then null
             when sr.knownconcentrationconv = 0 then null
             else case when '&&FinalAccuracy&'='T' then (sr.concentrationconv-
                                                     max(sr.knownconcentrationconv) 
                                                     over (partition by sr.assayanalyteid, k.nameprefix, s.runid))*100
                                                    /max(sr.knownconcentrationconv) 
                                                     over (partition by sr.assayanalyteid, k.nameprefix, s.runid) 
                       else round((&&ConcRepresentationFunc&(sr.concentrationconv,&&ConcFigures&)-
                               max(&&ConcRepresentationFunc&(sr.knownconcentrationconv,&&ConcFigures&)) 
                               over (partition by sr.assayanalyteid, k.nameprefix, s.runid))*100
                              /max(&&ConcRepresentationFunc&(sr.knownconcentrationconv,&&ConcFigures&)) 
                               over (partition by sr.assayanalyteid, k.nameprefix, s.runid),&&DecPlPercBias&) 
                  end
        end usebias
      FROM &&TempTabPrefix&wt$assay$ana$known K,
           &&TempTabPrefix&wt$run r,
           &&TempTabPrefix&wt$run$analytes ra,
           &&TempTabPrefix&wt$run$samples s,
           &&TempTabPrefix&wt$run$sample$results sr,
           &&TempTabPrefix&wt$run$sample$result$raw srw,
           isr$crit kwz,
           table(&&LALPackage&.GetRunStates) rs
      WHERE K.ID=s.knownID
        AND R.ID = s.runid AND R.ID = sr.runid AND R.ID = srw.runid
        AND r.studyassayid =s.studyassayid AND s.studyassayid = sr.studyassayid AND s.studyassayid = srw.studyassayid      
        AND r.RuntypeDescription in (&&RunTypes&)
        AND s.ID=sr.runsampleID AND s.ID=srw.runsampleID       
        AND srw.sampleresultid=sr.id
        AND s.sampletype='known'
        and k.knowntype = s.runsamplekind
        and kwz.masterkey = '&&ExpMasterkey&'
        AND ra.assayanalyteid = sr.assayanalyteid
        AND ra.runid = s.runid  
        AND ra.studyID = '&&StudyID&'        
        AND K.studyid = '&&StudyID&' AND s.studyid = '&&StudyID&' 
        AND sr.studyID = '&&StudyID&' AND srw.studyID = '&&StudyID&' AND r.studyid = '&&StudyID&'
        and kwz.key=K.NAMEPREFIX
        and kwz.entity = '&&KnownEntity&'
        and repid=&&RepID&
        AND rs.column_value=r.runstatusnum &&AddCondition&
     )
     )
     GROUP BY assayanalyteid, knownname, runid, dilutionfactor, knownconc)
    ) FROM dual