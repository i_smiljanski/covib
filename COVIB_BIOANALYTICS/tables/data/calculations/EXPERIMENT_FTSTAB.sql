  SELECT XMLELEMENT("experiment", XMLATTRIBUTES('&&Experiment&' AS "name" ,'&&FTCalcType&' AS "type"),
   (
   select Xmlagg(XMLConcat(
     Xmlagg(XMLELEMENT("calculation", xmlconcat(targ,val,res)) order by analyteorder, species, matrix &&RunIDColumn&, cycles, knownorder, hours, temperature, longtermtime, longtermunits, concentrationunits),
--, nominalconc
     XMLELEMENT("statistic", XMLATTRIBUTES(analyteid AS "analyteid", species AS "species", matrix AS "matrix", cycles AS "cycles", hours AS "hours", temperature AS "temperature", longtermtime AS "longtermtime", longtermunits AS "longtermunits", concentrationunits as "unit"),
       XMLELEMENT("flagpercent", min(flagpercent)),
       XMLELEMENT("min-bias", FormatRounded(min(bias), &&DecPlPercBias&)),
       XMLELEMENT("max-bias", FormatRounded(max(bias), &&DecPlPercBias&)),
       XMLELEMENT("min-biasonmean", FormatRounded(min(biasonmean), &&DecPlPercBias&)),
       XMLELEMENT("max-biasonmean", FormatRounded(max(biasonmean), &&DecPlPercBias&)),
       XMLELEMENT("min-difference", FormatRounded(min(diff), &&DecPlPercBias&)),
       XMLELEMENT("max-difference", FormatRounded(max(diff), &&DecPlPercBias&)),
       XMLELEMENT("min-cv", FormatRounded(min(cv), &&DecPlPercCV&)),
       XMLELEMENT("max-cv", FormatRounded(max(cv), &&DecPlPercCV&)),
       XMLELEMENT("numacc", sum(cntunflagged)),
       XMLELEMENT("numtotal", sum(cnt)),
       XMLELEMENT("percacc", case when sum(cnt) != 0 then FmtNum(round(sum(cntunflagged)/sum(cnt)*100, &&DecPlPercStat&)) else null end)
       )) order by analyteid, analyteorder, species, matrix, cycles, longtime, hours, concentrationunits)
  from (
  SELECT
      analyteid, analyteorder, species, matrix, concentrationunits, cycles, hours, temperature, longtermtime, longtermunits, knownorder--, nominalconc
      &&RunIDColumn&,
      (case when longtermunits = 'W' then 7 when longtermunits = 'M' then 30 when longtermunits = 'D' then 1 else 0 end) * longtermtime longtime,
      Count(useconc) cnt,
      count(unflagged) cntunflagged,
      min(flagpercent) flagpercent,
      round(Avg(usedifference), &&DecPlPercBias&) diff,
      round(Avg(usebias), &&DecPlPercBias&) bias,
      round(case when nominalconc = 0 then null
                      when '&&FinalAccuracy&' = 'T' then (Avg(useconc)-nominalconc)/nominalconc*100
                      else (&&ConcRepresentationFunc&(Avg(useconc), &&ConcFigures&)-nominalconc_sigrnd)/nominalconc_sigrnd*100
             end, &&DecPlPercBias&) biasonmean,
      round(case when '&&FinalAccuracy&' = 'T' then Stddev(useconc)/Avg(useconc)*100
                 else &&ConcRepresentationFunc&(Stddev(useconc), &&ConcFigures&)
                      /&&ConcRepresentationFunc&(Avg(useconc), &&ConcFigures&)*100
            end, &&DecPlPercCV&) cv,
      XMLELEMENT("target",
        XMLELEMENT("analyteid", analyteid),
        XMLELEMENT("species", species),
        XMLELEMENT("matrix", matrix),
        XMLELEMENT("name", NAME),
        XMLELEMENT("nominalconc", &&ConcFormatFunc&(nominalconc_sigrnd, &&ConcFigures&)),
        XMLELEMENT("cycles", cycles),
        XMLELEMENT("temperature", temperature),
        XMLELEMENT("hours", iSR$Format.FmtNum(hours)),
        XMLELEMENT("longtermtime", longtermtime),
        XMLELEMENT("longtermunits", longtermunits),
        XMLELEMENT("unit", concentrationunits) &&RunIDInTarget&
        ) targ,
      XMLELEMENT("values", Xmlagg(
        XMLELEMENT("sample",  XMLATTRIBUTES(sampleid AS "sampleid", sampleresultID AS "sampleresultID" ),
          XMLELEMENT("knownid", knownid),
          XMLELEMENT("runsamplesequencenumber", runsamplesequencenumber),
          XMLELEMENT("conc", &&ConcFormatFunc&(&&ConcRepresentationFunc&(conc, &&ConcFigures&), &&ConcFigures&)),
          XMLELEMENT("nominalconc", &&ConcFormatFunc&(nominalconc, &&ConcFigures&)),
          XMLELEMENT("replicatenumber", replicatenumber),
          XMLELEMENT("hours", hours),
          XMLELEMENT("bias", FormatRounded(round(bias, &&DecPlPercBias&), &&DecPlPercBias&)),
          XMLELEMENT("control-mean-conc", &&ConcFormatFunc&(&&ConcRepresentationFunc&(ccMean, &&ConcFigures&), &&ConcFigures&)),
          XMLELEMENT("difference", FormatRounded(round(difference, &&DecPlPercBias&), &&DecPlPercBias&)),
          XMLELEMENT("flag", flagged),
          XMLELEMENT("deactivated", isdeactivated)
        )
        order by replicatenumber)) val,
      XMLELEMENT("result",
        XMLELEMENT("n", Count(useconc)),
        XMLELEMENT("mean", &&ConcFormatFunc&(&&ConcRepresentationFunc&(Avg(useconc), &&ConcFigures&), &&ConcFigures&)),
        XMLELEMENT("sd", &&ConcFormatFunc&(&&ConcRepresentationFunc&(Stddev(useconc), &&ConcSDFigures&), &&ConcSDFigures&)),
        XMLELEMENT("cv", FormatRounded(round(case when '&&FinalAccuracy&' = 'T' then Stddev(useconc)/Avg(useconc)*100
                                                  else &&ConcRepresentationFunc&(Stddev(useconc), &&ConcFigures&)
                                                      /&&ConcRepresentationFunc&(Avg(useconc), &&ConcFigures&)*100
                                             end, &&DecPlPercCV&), &&DecPlPercCV&)),
        XMLELEMENT("bias", FormatRounded(round(Avg(usebias), &&DecPlPercBias&), &&DecPlPercBias&)),
        XMLELEMENT("difference", FormatRounded(round(Avg(usedifference), &&DecPlPercBias&), &&DecPlPercBias&)),
        XMLELEMENT("biasonmean", FormatRounded(round(case when nominalconc = 0 then null
                                                    when '&&FinalAccuracy&' = 'T' then (Avg(useconc)-nominalconc)/nominalconc*100
                                                    else (&&ConcRepresentationFunc&(Avg(useconc), &&ConcFigures&)-nominalconc_sigrnd)/nominalconc_sigrnd*100
                                               end, &&DecPlPercBias&), &&DecPlPercBias&)),
        XMLELEMENT("flagpercent", min(flagpercent))
        ) res
    FROM
    (SELECT knownID, sampleID, sampleresultID, Name, nominalconc, nominalconc_sigrnd, runid,
            analyteid, analyteorder, species, matrix, concentrationunits, replicatenumber, cycles, runsamplesequencenumber, hours, temperature, longtermtime, longtermunits, knownorder,
            case when cycles = 1 then 'Control samples' else 'Evaluation samples' end grouptype,
            case when cycles = 1 then null else ccMean end ccMean,
            case when cycles = 1 then null
                 else round((&&ConcRepresentationFunc&(conc, &&ConcFigures&)-
                             &&ConcRepresentationFunc&(ccMean, &&ConcFigures&))*100
                            /&&ConcRepresentationFunc&(ccMean, &&ConcFigures&), &&DecPlPercBias&)
            end difference,
            case when ISDEACTIVATED = 'T' then null
                 else case when cycles = 1 then null
                           else case when '&&FinalAccuracy&' = 'T' then (conc-ccMean)*100/ccMean
                                     else round((&&ConcRepresentationFunc&(conc, &&ConcFigures&)-
                                                 &&ConcRepresentationFunc&(ccMean, &&ConcFigures&))*100
                                                /&&ConcRepresentationFunc&(ccMean, &&ConcFigures&), &&DecPlPercBias&)
                                end
                      end
            end usedifference,
            conc, flagpercent, bias,
            case when abs(bias) > flagpercent then '&&FlaggedSymbol&' else null end flagged,
            case when ISDEACTIVATED <> 'T' and abs(bias) <= flagpercent then 1 else null end unflagged,
            ISDEACTIVATED,
            case when ISDEACTIVATED = 'T' then null else conc end useconc,
            case when ISDEACTIVATED = 'T' then null else bias end usebias
     FROM
     (SELECT K.ID knownID, s.ID sampleID, sr.ID sampleresultID, K.NAME,
        sr.nominalconcentrationconv nominalconc,
        &&ConcRepresentationFunc&(sr.nominalconcentrationconv, &&ConcFigures&) nominalconc_sigrnd, s.runid,
        ra.analyteid, sa.analyteorder, a.species, a.sampletypeid matrix, ra.concentrationunits, r.runtypedescription, s.replicatenumber, k.flagpercent, k.cycles, k.temperature, k.hours, k.longtermtime, k.longtermunits, s.ISDEACTIVATED,
        s.runsamplesequencenumber, kwz.ordernumber knownorder,
        case when '&&FinalAccuracy&' = 'T' then sr.concentrationconv
             else &&ConcRepresentationFunc&(sr.concentrationconv, &&ConcFigures&)
        end conc,
        case when '&&FinalAccuracy&' = 'T' then avg(case when s.ISDEACTIVATED = 'T' then null else sr.concentrationconv end)
                                              KEEP (DENSE_RANK FIRST ORDER BY k.cycles)
                                              over (partition by ra.analyteid, sa.analyteorder, a.species, a.sampletypeid, sr.nominalconcentrationconv, s.runid)
             else &&ConcRepresentationFunc&(avg(case when s.ISDEACTIVATED = 'T' then null
                                                     else &&ConcRepresentationFunc&(sr.concentrationconv, &&ConcFigures&)
                                                end)
                                            KEEP (DENSE_RANK FIRST ORDER BY k.cycles)
                                            over (partition by ra.analyteid, sa.analyteorder, a.species, a.sampletypeid, sr.nominalconcentrationconv, s.runid), &&ConcFigures&)
        end ccMean,
        case when '&&FinalAccuracy&' = 'T' then (sr.concentrationconv-sr.nominalconcentrationconv)*100/sr.nominalconcentrationconv
             else round((&&ConcRepresentationFunc&(sr.concentrationconv, &&ConcFigures&)-
                         &&ConcRepresentationFunc&(sr.nominalconcentrationconv, &&ConcFigures&))*100
                        /&&ConcRepresentationFunc&(sr.nominalconcentrationconv, &&ConcFigures&), &&DecPlPercBias&)
        end bias
      FROM &&TempTabPrefix&bio$run$ana$known K,
           &&TempTabPrefix&bio$run r,
           &&TempTabPrefix&bio$run$analytes ra,
           &&TempTabPrefix&bio$study$analytes sa,
           &&TempTabPrefix&bio$assay a,
           &&TempTabPrefix&bio$run$samples s,
           &&TempTabPrefix&bio$run$sample$results sr,
           isr$crit kwz,
           table(&&LALPackage&.GetRunStates) rs
      WHERE K.ID = s.runknownID
        AND R.ID = s.runid AND R.ID = sr.runid
        --AND r.studyassayid = s.studyassayid AND s.studyassayid = sr.studyassayid
        AND r.RuntypeDescription in (&&RunTypes&)
        AND s.ID = sr.runsampleID
        AND s.sampletype = 'known'
        and k.knowntype = s.runsamplekind
        and kwz.masterkey = '&&ExpMasterkey&'
        AND ra.id = sr.runanalyteid
        AND a.id = r.assayid
        AND sa.id = ra.analyteid
        AND ra.runid = s.runid
        AND ra.studyID = '&&StudyID&'
        AND K.studyid = '&&StudyID&' AND s.studyid = '&&StudyID&'
        AND sr.studyID = '&&StudyID&' AND r.studyid = '&&StudyID&'
        and kwz.key = K.NAMEPREFIX
        and kwz.entity = '&&KnownEntity&'
        and repid = &&RepID&
        AND rs.column_value = r.runstatusnum
        &&AddCondition&
    ))
    GROUP BY analyteid, analyteorder, species, matrix, concentrationunits &&RunIDColumn&, knownorder, nominalconc, nominalconc_sigrnd, name, cycles, hours, temperature, longtermtime, longtermunits)
    GROUP BY analyteid, analyteorder, species, matrix, concentrationunits, cycles, hours, temperature, longtermtime, longtermunits, longtime
    )
  ) FROM dual