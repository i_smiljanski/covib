  SELECT XMLELEMENT("experiment", XMLATTRIBUTES('&&Experiment&' AS "name" ,'mandrep' AS "type"),
    (SELECT xmlagg (XMLELEMENT("calculation",
      XMLELEMENT("target",
        XMLELEMENT("analyteid", analyteid),
        XMLELEMENT("species", species),
        XMLELEMENT("matrix", matrix),
        XMLELEMENT("runid", midrun)),
      XMLELEMENT("values", xmlagg(
        XMLELEMENT("sample", XMLATTRIBUTES(samplecode AS "samplecode", sampleid AS "sampleid" ),
          XMLELEMENT("runsampleid", runsampleid),
          XMLELEMENT("designsampleid", designSampleID),
          XMLELEMENT("name", NAME),
          XMLELEMENT("runid", runid),
          XMLELEMENT("runidseq", lpad(runid,5,'0')),
          XMLELEMENT("runsamplesequencenumber", runsamplesequencenumber),
          XMLELEMENT("runsamplesequencenumberseq", lpad(runsamplesequencenumber,5,'0')),
          XMLELEMENT("specimenkey", specimenkey),
          XMLELEMENT("concentration", conc),      -- is formatted in select below
          XMLELEMENT("nm", nm),
          XMLELEMENT("vec", vec),
          XMLELEMENT("runidm", mrunid),
          XMLELEMENT("runidmseq", lpad(mrunid,5,'0')),
          XMLELEMENT("concentrationm", concm),    -- is formatted in select below
          XMLELEMENT("nmm", nmm),
          XMLELEMENT("vecm", vecm),
          XMLELEMENT("bias", bias),               -- is formatted in select below
          XMLELEMENT("designsubjecttag", designsubjecttag),
          XMLELEMENT("samplingtime", samplingtime)
        )
      ORDER BY DesignSubjectTag, timeident, samplingtime
      )),
      XMLELEMENT("result",
        XMLELEMENT("n", count(conc)),
        XMLELEMENT("min-bias", FormatRounded(min(numbias), &&DecPlPercBias&)),
        XMLELEMENT("max-bias", FormatRounded(max(numbias), &&DecPlPercBias&))
       )
    ))
    FROM
    (SELECT  distinct ram.analyteid, sam.analyteorder, am.species, am.sampletypeid matrix, rds.specimenkey,
        &&ConcFormatFunc&(&&ConcRepresentationFunc&(ra.nmunkconv*s.dilutionfactor, &&ConcFigures&), &&ConcFigures&) nm,
        &&ConcFormatFunc&(&&ConcRepresentationFunc&(ra.vecunkconv*s.dilutionfactor, &&ConcFigures&), &&ConcFigures&) vec,
        &&ConcFormatFunc&(&&ConcRepresentationFunc&(ram.nmunkconv*sm.dilutionfactor, &&ConcFigures&), &&ConcFigures&) nmm,
        &&ConcFormatFunc&(&&ConcRepresentationFunc&(ram.vecunkconv*sm.dilutionfactor, &&ConcFigures&), &&ConcFigures&) vecm,
        rm.ID midrun, rm.runid mrunid, r.runid, sam.samplingtime, sam.DesignSubjectTag, sam.timeident,
        RDS.FROMSAMPLEID designSampleID,  -- formerly wsr.designsampleid
        sm.NAME, srm.runsampleid, s.samplecode, s.sampleid, wsr.ischoice, sm.runsamplesequencenumber,
        CASE WHEN wsr.calibrationrangeflag= 'NM' THEN 'BQL'
             WHEN wsr.calibrationrangeflag= 'VEC' THEN 'AQL'
             WHEN wsr.calibrationrangeflag not in('VEC','NM') and trim(wsr.calibrationrangeflag) is not null THEN null
             ELSE &&ConcFormatFunc&(&&ConcRepresentationFunc&(wsr.concentrationconv, &&ConcFigures&), &&ConcFigures&)
        END conc,
        wsr.calibrationrangeflag,
        CASE WHEN srm.concentrationstatus in('NM','BQL') THEN 'BQL'
             WHEN srm.concentrationstatus in('VEC','AQL') THEN 'AQL'
             WHEN srm.concentrationstatus not in('VEC','AQL','NM','BQL') and trim(srm.concentrationstatus) is not null THEN null
             ELSE &&ConcFormatFunc&(&&ConcRepresentationFunc&(srm.concentrationconv, &&ConcFigures&), &&ConcFigures&)
        END concm,
        srm.concentrationstatus,
        CASE WHEN trim(wsr.concentrationstatus) is not null OR trim(srm.concentrationstatus) is not null OR wsr.concentrationconv is null THEN '*'
             ELSE FormatRounded(round(case when '&&FinalAccuracy&' = 'T' then (srm.concentrationconv-wsr.concentrationconv)*100
                                                                          /((wsr.concentrationconv+srm.concentrationconv)/2)
                                           else (&&ConcRepresentationFunc&(srm.concentrationconv, &&ConcFigures&)
                                                -&&ConcRepresentationFunc&(wsr.concentrationconv, &&ConcFigures&))*100
                                              /((&&ConcRepresentationFunc&(wsr.concentrationconv, &&ConcFigures&)
                                                +&&ConcRepresentationFunc&(srm.concentrationconv, &&ConcFigures&))/2)
                                      end, &&DecPlPercBias&), &&DecPlPercBias&)
        END bias,
        CASE WHEN trim(wsr.concentrationstatus) is not null OR trim(srm.concentrationstatus) is not null OR wsr.concentrationconv is null THEN null
             ELSE round(case when '&&FinalAccuracy&' = 'T' then (srm.concentrationconv-wsr.concentrationconv)*100
                                                            /((wsr.concentrationconv+srm.concentrationconv)/2)
                             else (&&ConcRepresentationFunc&(srm.concentrationconv, &&ConcFigures&)
                                  -&&ConcRepresentationFunc&(wsr.concentrationconv, &&ConcFigures&))*100
                                /((&&ConcRepresentationFunc&(wsr.concentrationconv, &&ConcFigures&)
                                  +&&ConcRepresentationFunc&(srm.concentrationconv, &&ConcFigures&))/2)
                        end, &&DecPlPercBias&)
        END numbias
    FROM &&TempTabPrefix&bio$run r,
         &&TempTabPrefix&bio$run$analytes ra,
         &&TempTabPrefix&bio$study$analytes sa,
         &&TempTabPrefix&bio$assay a,
         &&TempTabPrefix&bio$run$samples s,
         &&TempTabPrefix&bio$sample$results wsr,
         &&TempTabPrefix&bio$sample sam,
         table(&&LALPackage&.GetRunStates) rs,
         &&TempTabPrefix&bio$run rm,
         &&TempTabPrefix&bio$run$analytes ram,
         &&TempTabPrefix&bio$study$analytes sam,
         &&TempTabPrefix&bio$assay am,
         &&TempTabPrefix&bio$run$samples sm,
         &&TempTabPrefix&bio$run$sample$results srm,
         &&TempTabPrefix&bio$REASSAYDESIGNSAMPLES rds,
         table(&&LALPackage&.GetRunStates) rsm
    WHERE R.ID = s.runid
      --AND ra.assayanalytecode = aa.code
      AND s.runanalyteid = sm.runanalyteid
      AND s.analyteorder = sm.analyteorder
      AND ra.id = wsr.runanalyteid
      AND sa.id = ra.analyteid
      AND a.id = r.assayid
      AND ra.runid = R.id
      AND r.runtypedescription != 'MANDATORY REPEATS'
      AND s.sampletype = 'unknown'
      AND s.studyid = '&&StudyID&'
      AND r.studyid = '&&StudyID&'
      --AND r.studyassayid =s.studyassayid
      AND RM.ID = sm.runid
      AND ram.id = srm.runanalyteid
      AND sam.id = ram.analyteid
      AND am.id = rm.assayid
      AND ram.runid = RM.id
      AND rm.runtypedescription = 'MANDATORY REPEATS'
      AND sm.ID = srm.runsampleid
      AND sm.sampletype = 'unknown'
      AND sm.studyid = '&&StudyID&'
      AND srm.studyid = '&&StudyID&'
      AND rm.studyid = '&&StudyID&'
      --AND rm.studyassayid =sm.studyassayid AND sm.studyassayid = srm.studyassayid
      AND sam.studyid = '&&StudyID&'
      AND sm.sampleid = sam.ID
      AND rds.studycode || '-' || rds.tosampleid = s.samplecode
      AND rds.studycode || '-' || rds.fromsampleid = sm.samplecode
      AND wsr.designsampleid = rds.tosampleid
      AND rds.studycode = s.studycode
      AND rds.studycode = sm.studycode
      AND rs.column_value = r.runstatusnum
      AND rsm.column_value = rm.runstatusnum
      AND wsr.samplecode = s.samplecode
      --  AND ((r.runid = wsr.runid AND wsr.REASSAYSPRESENTFLAG = 'N')
      --   or (wsr.runid is null AND wsr.REASSAYSPRESENTFLAG = 'Y'))
      AND (r.runid = wsr.runid OR wsr.runid is null)
      AND s.isdeactivated = 'F'
      AND sm.isdeactivated = 'F'
      AND (wsr.ischoice = 'N' or (wsr.ischoice = 'Y' AND r.runid = wsr.runid))
      ) &&AddCondition&
      GROUP BY analyteid, analyteorder, species, matrix, midrun)
    ) FROM dual