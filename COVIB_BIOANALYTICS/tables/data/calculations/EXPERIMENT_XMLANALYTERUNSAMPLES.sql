  SELECT XMLELEMENT("experiment", XMLATTRIBUTES('&&Experiment&' AS "name" ,'xmlanalyterunsamples' AS "type"),
      Xmlagg ( 
        Xmlagg (XMLELEMENT("calculation", xmlconcat(targ,val)) order by studyid, runid, rid)
         order by runid
      ))
  FROM (
  SELECT studyid, rid, runid,
    XMLELEMENT("target",
      XMLELEMENT("runid", rid),
      XMLELEMENT("runno", runid)
    ) as targ,
    XMLELEMENT("values", 
      Xmlagg(
        XMLELEMENT("sample",
          XMLATTRIBUTES(sampleid AS "sampleid", sampleresultid AS "sampleresultid" ),
          XMLELEMENT("analyteid", analyteid),
          XMLELEMENT("analytename", analytename),
          XMLELEMENT("runsamplesequencenumber", runsamplesequencenumber),
          XMLELEMENT("samplename", samplename),
          XMLELEMENT("exportsamplename", exportsamplename),
          XMLELEMENT("analytearea", analytearea),
          XMLELEMENT("analyteretentiontime", analyteretentiontime),
          XMLELEMENT("internalstandardarea", internalstandardarea),
          XMLELEMENT("internalstandardretentiontime", internalstandardretentiontime),
          /*XMLELEMENT("InstrumentResponse",
            case when internalstandardarea = 0 then null
              else analytearea / internalstandardarea
            end),*/
          XMLELEMENT("ratio", ratio),
          XMLELEMENT("runstartdate", runstartdate),
          XMLELEMENT("assaydatetime", to_char(assaydatetime, 'DD-MON-YYYY HH24:MI:SS')),
          XMLELEMENT("importfilename", importfilename),
          XMLELEMENT("dilution", dilution),
          XMLELEMENT("concentration", &&ConcFormatFunc&(conc_sigrnd, &&ConcFigures&)),
          XMLELEMENT("nominalconc", &&ConcFormatFunc&(nominalconc_sigrnd, &&ConcFigures&)),
          XMLELEMENT("bias", 
            case when nominalconc_sigrnd = 0 then null
              else round((&&ConcRepresentationFunc&(conc_sigrnd, &&ConcFigures&)-&&ConcRepresentationFunc&(nominalconc_sigrnd, &&ConcFigures&))*100/
                         &&ConcRepresentationFunc&(nominalconc_sigrnd, &&ConcFigures&), &&DecPlPercBias&)
            end)
        ) order by runsamplesequencenumber
      )
    ) as val
  FROM (
    SELECT
      r.studyid,
      s.id sampleid,
      sr.id sampleresultid,
      --sam.subjectid,
      --sam.designsubjecttag,
      s.designsampleid,
      r.id rid,
      r.runid,
      r.runstartdate,
      sa.id analyteid,
      sa.name analytename,
      s.samplesubtype,
      s.runsamplesequencenumber,
      s.samplename,
      s.exportsamplename,
      s.dilution,
      &&ConcRepresentationFunc&(s.concentration, &&ConcFigures&) nominalconc_sigrnd,
      s.concentration nominalconc,
      sr.analytearea analytearea,
      sr.analyteareastatus analyteareastatus,
      sr.analyteheight analyteheight,
      sr.analyteheightstatus analyteheightstatus,
      sr.analytepeakretentiontime analyteretentiontime,
      sr.internalstandardarea internalstandardarea,
      sr.internalstandardareastatus internalstandareastatus,
      sr.internalstandardheight internalstandheight,
      sr.internalstandardheightstatus internalstandheightstatus,
      sr.internalstandardretentiontime internalstandardretentiontime,
      sr.ratio,
      sr.importfilename,
      s.status,
      sr.commenttext,
      &&ConcRepresentationFunc&(sr.concentration, &&ConcFigures&) conc_sigrnd,
      sr.concentration conc,
      s.concentrationunit,
      sr.concentrationstatus,
      case when s.concentration = 0 then null
           else round((&&ConcRepresentationFunc&(sr.concentration, &&ConcFigures&)-&&ConcRepresentationFunc&(s.concentration, &&ConcFigures&))*100/
                       &&ConcRepresentationFunc&(s.concentration, &&ConcFigures&), &&DecPlPercBias&)
      end bias_sigrnd,
      case when s.concentration = 0 then null
           else (sr.concentration-s.concentration)*100/
                 s.concentration
      end bias,
      max(sr.concentrationstatus) over (partition by s.designsampleid, r.id) result,
      s.source,
      s.time,
      s.timetext,
      s.studyday,
      to_date(s.assaydatetime, 'dd.Mon.yyyy hh24:mi:ss') assaydatetime,
      --t.name dosegroup,
      max(rh.reported) over (partition by s.designsampleid, r.id) reported,
      /*case
          when REGEXP_LIKE(t.name, '^[+-]?\d?\.?\d+$','i') then
              to_number(t.name)
          when REGEXP_LIKE(t.description, '^[+-]?\d?\.?\d+$','i') then
              to_number(t.description)
          else
            null
      end dosegroupno,
      t.doseamount,
      t.doseunitsdescription doseunit,
      sam.samplingtime,*/
      s.samplesubtype posgroup,
      case
        when s.runcode || '-' || s.runsamplesequencenumber || '-' || sa.analyteorder in (select code from &&TempTabPrefix&bio$deactivated$samples) then 'T'
        else 'F'
      end isdeactivated
    FROM 
      &&TempTabPrefix&bio$run r,
      &&TempTabPrefix&bio$run$analytes ra,
      &&TempTabPrefix&bio$study$analytes sa,
      &&TempTabPrefix&bio$assay a,
      &&TempTabPrefix&bio$run$worklist s,
      &&TempTabPrefix&bio$run$worklist$result$rw sr,
      --&&TempTabPrefix&bio$sample sam,
      --&&TempTabPrefix&bio$study$treatment t,
      &&TempTabPrefix&bio$reassay$history rh,
      table(&&LALPackage&.GetRunStates) rs
    WHERE r.ID = s.runID
    AND s.ID = sr.worklistID
    AND a.ID = r.assayID
    --AND s.designSampleID = sam.ID
    AND r.studyid = '&&StudyID&'
    AND a.studyID = '&&StudyID&'
    AND rs.column_value = r.runstatusnum
    AND sr.runAnalyteCode = ra.code
    AND r.ID = ra.runID
    AND ra.analyteID = sa.ID
    --AND sam.treatmentcode = t.code
    --s.runcode || '-' || s.runsamplesequencenumber || '-' || sa.analyteorder in (select code from &&TempTabPrefix&bio$deactivated$samples)
    AND r.code || '-' || s.runsamplesequencenumber || '-' || sa.analyteorder = rh.runsamplecode (+)
    &&AddCondition&
  )
  GROUP BY studyid, runid, rid)
GROUP BY studyid, runid, rid
