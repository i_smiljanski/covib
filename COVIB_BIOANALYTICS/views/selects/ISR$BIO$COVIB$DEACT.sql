CREATE OR REPLACE VIEW ISR$BIO$COVIB$DEACT (CODE, KNOWN, DISPLAY1, DISPLAY2, ORDERNUMBER) AS SELECT sample_crit.key
, sample_crit.key
, sample_crit.info
, sample_crit.display
, MIN (sample_crit.ordernumber)
FROM (
  SELECT *
    FROM isr$crit
   WHERE repid = stb$object.getcurrentrepid
     AND entity in ('BIO$COVIB$RUN$SAMPLE$DEACT', 'BIO$COVIB$RUNANA$SMP$DEACT')
  ) sample_crit
GROUP BY sample_crit.key, sample_crit.display, sample_crit.info
ORDER BY 5