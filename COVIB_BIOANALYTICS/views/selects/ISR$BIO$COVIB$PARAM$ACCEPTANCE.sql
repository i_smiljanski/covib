CREATE OR REPLACE VIEW ISR$BIO$COVIB$PARAM$ACCEPTANCE (ENTITY, CODE, NAME, INFO, ORDERNUMBER) AS SELECT
    'BIO$COVIB$PARAM$ACCEPTANCE' ENTITY,
    PV.Value          CODE,
    PV.DISPLAY        NAME,
    PV.INFO           INFO,
    pv.ORDERNUMBER    ORDERNUMBER
  FROM ISR$PARAMETER$VALUES pv
  WHERE pv.entity='BIO$COVIB$PARAM$ACCEPTANCE'
  ORDER BY PV.ORDERNUMBER