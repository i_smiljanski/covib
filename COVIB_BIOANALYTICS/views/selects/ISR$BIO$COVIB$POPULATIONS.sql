CREATE OR REPLACE VIEW ISR$BIO$COVIB$POPULATIONS (ENTITY, CODE, DISPLAY, ORDERNUMBER) AS SELECT 'BIO$COVIB$POPULATIONS' entity
     , p.population code
     , nvl(pv.display, p.population) display
     , nvl(pv.ordernumber, 10000) ordernumber
  FROM (
        SELECT 'NORM' population FROM DUAL
        UNION ALL
        SELECT distinct
               case
                 when masterkey = 'S_SELECTIVITY' then regexp_replace(key,'^.*SEL[ _\-]([A-Z0-9]+)[ _\-]N?D$', '\1', 1, 1, 'i')
                 else regexp_replace(key,'^\D+\d+[ _\-]([A-Z0-9]+)[ _\-]N?D$', '\1', 1, 1, 'i')
               end population
          FROM ISR$CRIT
         WHERE entity = 'BIO$KNOWN'
           AND masterkey in ('S_SELECTIVITY', 'S_CUTPOINT')
           AND (
            ( masterkey = 'S_SELECTIVITY' AND regexp_like(key,'^.*SEL[ _\-]([A-Z0-9]+)[ _\-]N?D$') )
            OR ( masterkey = 'S_CUTPOINT' AND regexp_like(key,'^\D+\d+[ _\-]([A-Z0-9]+)[ _\-]N?D$','i') ) )
           AND repid = stb$object.getcurrentrepid
       ) p
  LEFT OUTER JOIN ISR$PARAMETER$VALUES pv
  ON pv.entity = 'BIO$COVIB$POPULATION' and pv.value = p.population
  ORDER BY 4, 2