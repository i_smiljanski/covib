﻿' <summary>Handels everything Excel.</summary>

Imports Microsoft.Office.Interop.Excel

Public Class HandlerExcel

    Private oExcel As Application = Nothing
    Private sWorkbookName As String

    ''' <summary>
    ''' Contructor.
    ''' Initializes the Excel Object.
    ''' </summary>
    Public Sub New()
        oTrace.TraceEvent(TraceEventType.Information, 1, "Entering HandlerExcel.New")

        'Initialize the Excel Object
        oExcel = New Application
        oExcel.Visible = False

    End Sub

    ''' <summary>
    ''' In absence of a 'real' destructor.
    ''' </summary>
    Public Sub CleanUp()
        oTrace.TraceEvent(TraceEventType.Information, 1, "Entering HandlerExcel.CleanUp")

        'If the object is already empty, there is nothing to do here
        If oExcel Is Nothing Then Exit Sub

        'Now leave word.
        oExcel.Quit()

        'Free the word object
        oExcel = Nothing

    End Sub

    ''' <summary>
    ''' In Case the PdfModulX exits due to an error, Main has to have the possibility to close the word instance gracefully.
    ''' </summary>
    ''' <returns>the Word Object</returns>  
    Public Function GetExcelBackup() As Application

        Return oExcel

    End Function

    ''' <summary>
    ''' Open the associated workbook.
    ''' </summary>
    ''' <param name="sWorkbookFile">The workbook filename (including path).</param>  
    ''' <returns>true if the file was opend, false if something went wrong.</returns>  
    Public Function OpenWorkbook(ByVal sWorkbookFile As String) As Boolean
        oTrace.TraceEvent(TraceEventType.Information, 1, "Entering HandlerExcel.OpenWorkbook")

        Try
            oExcel.Workbooks.Open(sWorkbookFile)
        Catch ex As Exception
            HandleException(ex, "OpenWorkbook")
            Return False
        End Try

        sWorkbookName = sWorkbookFile

        Return True

    End Function

    ''' <summary>
    ''' Prints the workbook to the active printer.
    ''' </summary>
    ''' <param name="sFilename">The documents filename (including path).</param>  
    ''' <returns>true if the file was 'printed', false if something went wrong.</returns>  
    Public Function PrintDocument(ByVal sFilename) As Boolean
        oTrace.TraceEvent(TraceEventType.Information, 1, "Entering HandlerExcel.PrintDocument")

        Try
            oExcel.Documents(sWorkbookName).PrintOut(OutputFileName:=sFilename)
        Catch ex As Exception
            HandleException(ex, "PrintWorkbook")
            Return False
        End Try

        Return True

    End Function

    ''' <summary>
    ''' Waits until all print jobs are finished.
    ''' </summary>
    Public Sub WaitForPrinter()
        oTrace.TraceEvent(TraceEventType.Information, 1, "Entering HandlerExcel.WaitForPrinter")

        Dim iWaitCount As Integer = 0
        Dim iWaitInterval As Integer = 250

        While oExcel.BackgroundPrintingStatus > 0
            System.Threading.Thread.Sleep(iWaitInterval)
            iWaitCount = iWaitCount + 1

            'Logentry every minute
            If (iWaitCount * iWaitInterval) Mod 60000 = 0 Then
                oTrace.TraceEvent(TraceEventType.Information, 111, "Waiting - Number of Print Jobs: " & oExcel.BackgroundPrintingStatus)
            End If

            'Abort after 5 minutes
            If (iWaitCount * iWaitInterval) Mod 60000 * 5 = 0 Then
                oTrace.TraceEvent(TraceEventType.Information, 111, "Abort Waiting - Number of Print Jobs: " & oExcel.BackgroundPrintingStatus)
                Exit While
            End If

        End While

    End Sub

    ''' <summary>
    ''' Retrieves all custom Properties from the excel workbook and writes them to the Properties-Object.
    ''' </summary>
    ''' <param name="oProperties">The Properties Object.</param>  
    Public Sub RetrieveDocumentProperties(ByVal oProperties As Properties)
        oTrace.TraceEvent(TraceEventType.Information, 1, "Entering HandlerExcel.RetrieveDocumentProperties")

        Dim oProperty As Object

        'Fetch all Properties to the local Object
        For Each oProperty In oExcel.Workbooks(sWorkbookName).CustomDocumentProperties
            oProperties.AddProperty(oProperty.Name, oProperty.Value)
        Next oProperty

    End Sub

    ''' <summary>
    ''' Saves the document as a PDF document
    ''' </summary>
    ''' <param name="sFilename">The pdf filename (including path).</param>  
    ''' <returns>True if the document was saved, false if something went wrong</returns>  
    Public Function SaveAsPDF(ByVal sFilename As String) As Boolean
        oTrace.TraceEvent(TraceEventType.Information, 1, "Entering HandlerWord.SaveAsPDF")

        Try
            oExcel.Documents(sWorkbookName).ExportAsFixedFormat(OutputFileName:=sFilename, Type:=XlFixedFormatType.xlTypePDF)
        Catch ex As Exception
            HandleException(ex, "SaveAsPDF")
            Return False
        End Try

        Return True

    End Function

End Class
