﻿' <summary>Handels everything PDF.</summary>

Imports System.Text.RegularExpressions
Imports System.IO

Public Class HandlerPdf

    Dim oPdfDistiller As ACRODISTXLib.PdfDistiller

    ''' <summary>
    ''' Contructor.
    ''' </summary>
    Public Sub New()
        oTrace.TraceEvent(TraceEventType.Information, 1, "Entering HandlerPDF.New")

        'Create an Instance for PdfDistiller
        oPdfDistiller = New ACRODISTXLib.PdfDistiller

    End Sub

    ''' <summary>
    ''' In absence of a 'real' destructor.
    ''' </summary>
    Public Sub CleanUp()
        oTrace.TraceEvent(TraceEventType.Information, 1, "Entering HandlerPDF.CleanUp")

        oPdfDistiller = Nothing

    End Sub

    ''' <summary>
    ''' Converts a postscript file to PDF
    ''' </summary>
    ''' <param name="sInFilename">The input filename (including path) of the postscript file.</param>  
    ''' <param name="sOutFilename">The output filename (including path) of the resulting pdf.</param>  
    ''' <returns>true if the file was opend, false if something went wrong</returns>  
    Public Function ConvertPSToPDF(ByVal sInFilename As String, ByVal sOutFilename As String) As Boolean
        oTrace.TraceEvent(TraceEventType.Information, 1, "Entering HandlerPDF.ConvertPSToPDF")
        oTrace.Flush()

        'Generate this to a slightly modified file, or else it may clash with the Wordmodul.log due to an iSR Quirk
        Dim sTempFilename = Path.GetDirectoryName(sOutFilename) & "\Distiller_" & Path.GetFileName(sOutFilename)

        Try
            oPdfDistiller.FileToPDF(sInFilename, sTempFilename, "")
        Catch ex As Exception
            HandleException(ex, "ConvertToPDF")
            Return False
        End Try

        'Check if the file exits
        If Not File.Exists(sTempFilename) Then
            oTrace.TraceEvent(TraceEventType.Critical, 1, "No PDF generated. Check Distiller Log!")
            Return False
        End If

        'Now rename the file to the proper name
        If File.Exists(sOutFilename) Then File.Delete(sOutFilename)
        Rename(sTempFilename, sOutFilename)

        Return True

    End Function

    ''' <summary>
    ''' Converts a postscript file to PDF using an alternate Method
    ''' </summary>
    ''' <param name="sInFilename">The input filename (including path) of the postscript file.</param>  
    ''' <param name="sOutFilename">The output filename (including path) of the resulting pdf.</param>  
    ''' <returns>true if the file was opend, false if something went wrong</returns>  
    Public Function ConvertPSToPDFAlternate(ByVal sInFilename As String, ByVal sOutFilename As String) As Boolean
        oTrace.TraceEvent(TraceEventType.Information, 1, "Entering HandlerPDF.ConvertPSToPDF")
        oTrace.Flush()

        Dim oProcess As Process = New Process

        'Generate this to a slightly modified file, or else it may clash with the Wordmodul.log due to an iSR Quirk
        Dim sTempPdfFilename = sInFilename.Replace(".ps", ".pdf")

        oProcess.StartInfo.FileName = sInFilename
        oProcess.StartInfo.CreateNoWindow = True

        Try
            oProcess.Start()
        Catch ex As Exception
            HandleException(ex, "ConvertToPDF")
            Return False
        End Try

        'Check if the file exits
        If WaitForPDF(sTempPdfFilename) = False Then Return False

        Try
            oProcess.Kill()
        Catch ex As InvalidOperationException
            oTrace.TraceEvent(TraceEventType.Information, 1, "Acrodist process already gone. (InvalidOperationException)")
        Catch ex As System.ComponentModel.Win32Exception
            oTrace.TraceEvent(TraceEventType.Information, 1, "Acrodist can not be killed. (Win32Exception)")
        Catch ex As Exception
            HandleException(ex, "ConvertToPDF")
            Return False
        End Try

        'Now rename the file to the proper name
        If File.Exists(sOutFilename) Then File.Delete(sOutFilename)
        Try
            Rename(sTempPdfFilename, sOutFilename)
        Catch ex As System.ArgumentException
            'If the Rename went wrong, wait a moment and try again (possible race condition)
            oTrace.TraceEvent(TraceEventType.Information, 1, "Rename failed. Trying again...")
            System.Threading.Thread.Sleep(500)
            Rename(sTempPdfFilename, sOutFilename)
        End Try

        Return True

    End Function


    ''' <summary>
    ''' Writes the DOCINFO (aka Doc Properties) to the PS (EXP25)
    ''' </summary>
    ''' <param name="oStreamWriter">Streamwriter with the PS open</param>  
    ''' <param name="oProperties">Instance of the Properties</param>  
    Public Sub WriteDocinfoToPDF(ByVal oStreamWriter As StreamWriter, ByVal oProperties As Properties)
        oTrace.TraceEvent(TraceEventType.Information, 1, "Entering HandlerPDF.WriteDocinfoToPDF")

        Dim sKey As String
        Dim sValue As String

        oStreamWriter.WriteLine("")

        For Each sKey In oProperties.GetAllKeys
            sKey = Replace(sKey, " ", "_")
            sValue = oProperties.GetProperty(sKey).Replace("\", "\\").Replace("(", "\(").Replace(")", "\)")
            oStreamWriter.WriteLine("[ /" & sKey & " (" & sValue & ") /DOCINFO pdfmark")
        Next

    End Sub

    ''' <summary>
    ''' Writes the DOCVIEW (PageMode, StartPage, View, PageLayout) to the PS (EXP25)
    ''' </summary>
    ''' <param name="oStreamWriter">Streamwriter with the PS open</param>  
    ''' <param name="sPageMode">Pdf Pagemode</param>  
    ''' <param name="sFit">Pdf Fit (aka Zoomfactor)</param>  
    ''' <param name="iStartPage">Pdf Starting page</param>  
    ''' <param name="sPageLayout">Pdf Page Layout</param>  
    Public Sub WriteDocviewToPdf(ByVal oStreamWriter As StreamWriter, ByVal sPageMode As String, ByVal sFit As String, ByVal iStartPage As Integer, ByVal sPageLayout As String)
        oTrace.TraceEvent(TraceEventType.Information, 1, "Entering HandlerPDF.WriteDocviewToPdf")

        oStreamWriter.WriteLine("")

        oStreamWriter.WriteLine("[ ")
        If sPageMode <> "Default" Then oStreamWriter.WriteLine("/PageMode /" & sPageMode)
        oStreamWriter.WriteLine("/Page " & iStartPage)
        If sFit <> "Default" Then oStreamWriter.WriteLine("/View [/" & sFit & "]")
        oStreamWriter.WriteLine("/DOCVIEW pdfmark")

        oStreamWriter.WriteLine("")

        If sPageLayout <> "Default" Then oStreamWriter.WriteLine("[ {Catalog} << /PageLayout /" & sPageLayout & " >> /PUT pdfmark")

    End Sub

    ''' <summary>
    ''' Writes OUT (aka Bookmarks) to the PS (EXP25)
    ''' </summary>
    ''' <param name="oStreamWriter">Streamwriter with the PS open</param>  
    ''' <param name="oProperties">Instance of the Properties</param>  
    ''' <param name="sOut">String containing the Bookmark template (Properties are denoted bei "&amp;&amp;property&amp;")</param>  
    Public Sub WriteOutToPdf(ByVal oStreamWriter As StreamWriter, ByVal oProperties As Properties, ByVal sOut As String)
        oTrace.TraceEvent(TraceEventType.Information, 1, "Entering HandlerPDF.WriteOutToPdf")

        Dim sResult As String = sOut
        Dim oRegEx As New Regex("&&([\-_a-zA-Z0-9]*)&")

        For Each oMatch As Match In oRegEx.Matches(sOut)
            sResult = sResult.Replace(oMatch.Value, oProperties.GetProperty(oMatch.Value.Replace("&", "")))
        Next oMatch

        oStreamWriter.WriteLine("")
        oStreamWriter.WriteLine("[ /Page 1 /Title (" & sResult & ") /OUT pdfmark")

    End Sub

    ''' <summary>
    ''' Writes a custom PDFMARK string to the PS
    ''' </summary>
    ''' <param name="oStreamWriter">Streamwriter with the PS open</param>  
    ''' <param name="sCustomPdfMark">Custom PDFMARK string</param>  
    Public Sub WriteCustomPdfmarkToPdf(ByVal oStreamWriter As StreamWriter, ByVal sCustomPdfMark As String)
        oTrace.TraceEvent(TraceEventType.Information, 1, "Entering HandlerPDF.WriteCustomPdfmarkToPdf")

        oStreamWriter.WriteLine("")
        oStreamWriter.WriteLine(sCustomPdfMark)

    End Sub

    ''' <summary>
    ''' Waits until the PDF exists. Used by the alternate method.
    ''' </summary>
    Public Function WaitForPDF(ByVal sPdfFilename As String) As Boolean
        oTrace.TraceEvent(TraceEventType.Information, 1, "Entering HandlerPDF.WaitForPDF")
        oTrace.Flush()

        Dim bStatus = True

        Dim iWaitCount As Integer = 0
        Dim iWaitInterval As Integer = 250

        While Not File.Exists(sPdfFilename)

            System.Threading.Thread.Sleep(iWaitInterval)
            iWaitCount = iWaitCount + 1

            'Logentry every minute
            If (iWaitCount * iWaitInterval) Mod 60000 = 0 Then
                oTrace.TraceEvent(TraceEventType.Information, 111, "Waiting...")
            End If

            'Abort after 5 minutes
            If (iWaitCount * iWaitInterval) Mod 60000 * 5 = 0 Then
                oTrace.TraceEvent(TraceEventType.Information, 111, "Abort Waiting for PDF. ")
                bStatus = False
                Exit While
            End If

        End While

        'Wait once more to ensure the pdf if written completely
        System.Threading.Thread.Sleep(iWaitInterval)

        Return bStatus

    End Function

    ''' <summary>
    ''' Scans all processes for Acrotray.exe and kills them!
    ''' </summary>
    Public Sub KillAllAcrotrays()
        oTrace.TraceEvent(TraceEventType.Information, 1, "Entering HandlerPDF.KillAllAcrotrays")
        oTrace.Flush()

        Dim oProcesses As Process() = Process.GetProcessesByName("acrotray")

        Try
            For Each oProc In oProcesses
                oProc.Kill()
                oTrace.TraceEvent(TraceEventType.Information, 1, "One instance of acrotray.exe killed.")
            Next
        Catch ex As Exception
            oTrace.TraceEvent(TraceEventType.Information, 1, "One instance of acrotray.exe could not be killed.")
        End Try

    End Sub

End Class
