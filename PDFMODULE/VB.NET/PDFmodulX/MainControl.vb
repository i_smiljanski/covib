﻿' <summary>The central sequence handler.</summary>

Public Class MainControl

    Dim oOptions As Options = Nothing
    Dim oHandlerWord As HandlerWord = Nothing
    Dim oHandlerExcel As HandlerExcel = Nothing
    Dim oProperties As Properties = Nothing
    Dim oHandlerPdf As HandlerPdf = Nothing
    Dim oHandlerPdfClassic As HandlerPdfClassic = Nothing
    Dim oHandlerPrinter As HandlerPrinter = Nothing

    ''' <summary>
    ''' Constructor.
    ''' </summary>
    Public Sub New()
        oTrace.TraceEvent(TraceEventType.Information, 1, "Entering MainControl.New")

        'Initalize some general objects
        oOptions = New Options
        oProperties = New Properties

    End Sub

    ''' <summary>
    ''' In absence of a 'real' destructor.
    ''' </summary>
    Public Sub CleanUp()
        oTrace.TraceEvent(TraceEventType.Information, 1, "Entering MainControl.CleanUp")

        'Cleanup
        oOptions.CleanUp()
        oProperties.CleanUp()

        oOptions = Nothing
        oHandlerWord = Nothing
        oHandlerExcel = Nothing
        oProperties = Nothing
        oHandlerPdf = Nothing
        oHandlerPdfClassic = Nothing
        oHandlerPrinter = Nothing

    End Sub

    ''' <summary>
    ''' In Case the PdfModulX exits due to an error, Main has to have the possibility to close the word instance gracefully.
    ''' </summary>
    ''' <returns>the Word Object</returns>  
    Public Function GetWordBackup() As Microsoft.Office.Interop.Word.Application

        If Not oHandlerWord Is Nothing Then
            Return oHandlerWord.GetWordBackup
        Else
            Return Nothing
        End If

    End Function

    ''' <summary>
    ''' In Case the PdfModulX exits due to an error, Main has to have the possibility to close the word instance gracefully.
    ''' </summary>
    ''' <returns>the Word Object</returns>  
    Public Function GetExcelBackup() As Microsoft.Office.Interop.Word.Application

        If Not oHandlerExcel Is Nothing Then
            Return oHandlerExcel.GetExcelBackup
        Else
            Return Nothing
        End If

    End Function

    ''' <summary>
    ''' The Mainloop kind thing. Though it is no loop at all.
    ''' </summary>
    Public Function MainSequence() As Integer
        oTrace.TraceEvent(TraceEventType.Information, 1, "Entering MainControl.MainSequence")

        Dim iReturncode As Integer

        'Read the Command line
        oOptions.ParseArgs()

        'Read the Config file
        If oOptions.ReadConfigFile = False Then Return -1

        If oOptions.ApplicationType = Options.AppType.None Then
            oTrace.TraceEvent(TraceEventType.Error, 1, "Application Type could not be determined (no doc or xls).")
            Return -1
        End If

        'Decide if this is only SaveAsPDF or Acrobat
        If oOptions.SaveAsPDF = True Then
            'Call the SaveAsPDF Module
            iReturncode = SaveAsPDF()
        ElseIf oOptions.SaveAsPDF = False And oOptions.ClassicPdfGeneration = True Then
            'Call the Classic PDF Generation
            iReturncode = ClassicPdfGeneration()
        Else
            'Call the PrintAndConvert Module
            iReturncode = PrintAndConvert()
        End If

        Return iReturncode

    End Function


    ''' <summary>
    ''' Prints the document in Postscript format, injects pdf properties and converts the postscript to pdf.
    ''' </summary>
    Public Function PrintAndConvert() As Integer
        oTrace.TraceEvent(TraceEventType.Information, 1, "Entering MainControl.PrintAndConvert")

        Dim oWriter As System.IO.StreamWriter

        'Randomly generated temp filename
        Dim stempFilename As String = System.IO.Path.ChangeExtension(System.IO.Path.GetRandomFileName(), "ps")

        'Initalize the needed handlers
        oHandlerPdf = New HandlerPdf
        oHandlerPrinter = New HandlerPrinter

        'Change the Printer to the PDF Driver (if necessary)
        If oHandlerPrinter.ChangePrinterToPDF = False Then Return -1

        If oOptions.PdfRemoveAcrotray Then oHandlerPdf.KillAllAcrotrays()

        If oOptions.ApplicationType = Options.AppType.Word Then

            'Initialize the word handler
            oHandlerWord = New HandlerWord

            'Open the Word Document
            If oHandlerWord.OpenDoc(oOptions.DocFilename) = False Then Return -1

            'Retrieve all Properties from the document
            oHandlerWord.RetrieveDocumentProperties(oProperties)

            'Print the document
            If oHandlerWord.PrintDocument(oOptions.PdfTargetPath & stempFilename) = False Then Return -1

            'Now wait the printing job a little time to finish
            oHandlerWord.WaitForPrinter()

            'Close Word and stuff
            oHandlerWord.CleanUp()

        ElseIf oOptions.ApplicationType = Options.AppType.Excel Then

            'Initialize the Excel Handler
            oHandlerExcel = New HandlerExcel

            'Open the Excel Document
            If oHandlerExcel.OpenWorkbook(oOptions.DocFilename) = False Then Return -1

            'Retrieve all Properties from the document
            oHandlerExcel.RetrieveDocumentProperties(oProperties)

            'Print the document
            If oHandlerExcel.PrintDocument(oOptions.PdfTargetPath & stempFilename) = False Then Return -1

            'Now wait the printing job a little time to finish
            oHandlerExcel.WaitForPrinter()

            'Close Excel and stuff
            oHandlerExcel.CleanUp()

        ElseIf oOptions.ApplicationType = Options.AppType.HTML Then

            'Handle HTML in Word

            'Initialize the word handler
            oHandlerWord = New HandlerWord

            'Open the Word Document
            If oHandlerWord.OpenDoc(oOptions.DocFilename) = False Then Return -1

            'Retrieve all Properties from the document
            oHandlerWord.RetrieveDocumentProperties(oProperties)

            'Print the document
            If oHandlerWord.PrintDocument(oOptions.PdfTargetPath & stempFilename) = False Then Return -1

            'Now wait the printing job a little time to finish
            oHandlerWord.WaitForPrinter()

            'Close Word and stuff
            oHandlerWord.CleanUp()

        End If

        'Change the printer back to the original one (if necessary)
        oHandlerPrinter.ChangePrinterToOriginal()

        'Open the PS file
        oWriter = New System.IO.StreamWriter(oOptions.PdfTargetPath & stempFilename, True)

        'Modify the PS
        oHandlerPdf.WriteDocinfoToPDF(oWriter, oProperties)
        oHandlerPdf.WriteDocviewToPdf(oWriter, oOptions.PdfOptionPageMode, oOptions.PdfOptionFit, oOptions.PdfOptionStartPage, oOptions.PdfOptionPageLayout)
        oHandlerPdf.WriteOutToPdf(oWriter, oProperties, oOptions.PdfOptionOut)
        oHandlerPdf.WriteCustomPdfmarkToPdf(oWriter, oOptions.PdfCustomPdfMark)

        'close the PS file
        oWriter.Close()

        'Now convert the resulting PS file to a PDF
        If oOptions.PdfAlternateCreation = True Then
            If oHandlerPdf.ConvertPSToPDFAlternate(oOptions.PdfTargetPath & stempFilename, oOptions.PdfTarget) = False Then Return -1
        Else
            If oHandlerPdf.ConvertPSToPDF(oOptions.PdfTargetPath & stempFilename, oOptions.PdfTarget) = False Then Return -1
        End If

        'Cleanup
        oHandlerPdf.CleanUp()
        oHandlerPrinter.CleanUp()

        Return 0

    End Function

    ''' <summary>
    ''' Saves the Document as PDF by using the export functionality of Word/Excel
    ''' </summary>
    Public Function SaveAsPDF() As Integer
        oTrace.TraceEvent(TraceEventType.Information, 1, "Entering MainControl.SaveAsPDF")

        If oOptions.ApplicationType = Options.AppType.Word Then

            'Initialize the word handler
            oHandlerWord = New HandlerWord

            'Open the Word Document
            If oHandlerWord.OpenDoc(oOptions.DocFilename) = False Then Return -1

            'Save the document
            oHandlerWord.SaveAsPDF(oOptions.PdfTarget)

            'Close Word and stuff
            oHandlerWord.CleanUp()

        ElseIf oOptions.ApplicationType = Options.AppType.Excel Then

            'Initialize the Excel Handler
            oHandlerExcel = New HandlerExcel

            'Open the Excel Document
            If oHandlerExcel.OpenWorkbook(oOptions.DocFilename) = False Then Return -1

            'Save the document
            oHandlerExcel.SaveAsPDF(oOptions.PdfTarget)

            'Close Excel and stuff
            oHandlerExcel.CleanUp()

        ElseIf oOptions.ApplicationType = Options.AppType.HTML Then

            'Handle HTML in Word

            'Initialize the word handler
            oHandlerWord = New HandlerWord

            'Open the Word Document
            If oHandlerWord.OpenDoc(oOptions.DocFilename) = False Then Return -1

            'Set to landscape
            oHandlerWord.SetOrientation(Microsoft.Office.Interop.Word.WdOrientation.wdOrientLandscape)

            'Save the document
            oHandlerWord.SaveAsPDF(oOptions.PdfTarget)

            'Close Word and stuff
            oHandlerWord.CleanUp()

        End If

        Return 0

    End Function

    ''' <summary>
    ''' Creates the PDF in the classic way.
    ''' </summary>
    Public Function ClassicPdfGeneration()
        oTrace.TraceEvent(TraceEventType.Information, 1, "Entering MainControl.ClassicPdfGeneration")

        Dim iReturnvalue As Integer = 0

        'Randomly generated temp filename
        Dim stempFilename As String = oOptions.PdfTargetPath & System.IO.Path.ChangeExtension(System.IO.Path.GetRandomFileName(), System.IO.Path.GetExtension(oOptions.DocFilename))

        'Initalize the Handler for classic PDF generation
        oHandlerPdfClassic = New HandlerPdfClassic

        If oOptions.ApplicationType = Options.AppType.Word Then

            'Initialize the word handler
            oHandlerWord = New HandlerWord

            'Open the Word Document
            If oHandlerWord.OpenDoc(oOptions.DocFilename) = False Then Return -1

            'Unprotect the document
            If oHandlerWord.UnprotectDocument(oOptions.DocPassword) = False Then Return -1

            'Save the document
            oHandlerWord.SaveDocument(stempFilename)

            'Close Word and stuff
            oHandlerWord.CleanUp()

            'Make sure the file is available
            WaitForFile(stempFilename)

            'Convert the Doc to PDF
            iReturnvalue = oHandlerPdfClassic.ConvertToPdfClassic(stempFilename, oOptions.PdfTarget, False, oOptions.DocPassword)

            'Remove the temporary (unprotected!) file
            DeleteFile(stempFilename)

        ElseIf oOptions.ApplicationType = Options.AppType.Excel Then

            'Nothing much to do here, as we do not have protected Excel documents.

            'Convert the Doc to PDF
            oHandlerPdfClassic.ConvertToPdfClassic(stempFilename, oOptions.PdfTarget, False, oOptions.DocPassword)

        ElseIf oOptions.ApplicationType = Options.AppType.HTML Then

            'Convert the HTML to PDF
            oHandlerPdfClassic.ConvertToPdfClassic(stempFilename, oOptions.PdfTarget, False, oOptions.DocPassword)

        End If

        'Cleanup
        oHandlerPdfClassic.CleanUp()

        Return iReturnvalue

    End Function

    ''' <summary>
    ''' Deletes a file on the filesystem
    ''' </summary>
    ''' <param name="sFilename">The filename (including path).</param>  
    Private Sub DeleteFile(sFilename As String)
        oTrace.TraceEvent(TraceEventType.Information, 1, "Entering MainControl.DeleteFile")

        Try
            My.Computer.FileSystem.DeleteFile(sFilename)
        Catch ex As System.IO.IOException
            'If the Rename went wrong, wait a moment and try again (possible race condition)
            oTrace.TraceEvent(TraceEventType.Information, 1, "Delete failed. Trying again in a moment...")
            System.Threading.Thread.Sleep(500)

            Try
                My.Computer.FileSystem.DeleteFile(sFilename)
            Catch ex2 As Exception
                oTrace.TraceEvent(TraceEventType.Information, 1, "Deleteing " & sFilename & " failed.")
                HandleException(ex2, "DeleteFile")
            End Try

            oTrace.TraceEvent(TraceEventType.Information, 1, "Deleteing " & sFilename & " successful.")

        End Try

    End Sub

    ''' <summary>
    ''' Waits until the file is available
    ''' </summary>
    Public Sub WaitForFile(ByVal sFilename As String)
        oTrace.TraceEvent(TraceEventType.Information, 1, "Entering MainControl.WaitForFile")
        oTrace.Flush()

        Dim oStream As IO.FileStream = Nothing
        Dim iWaitCount As Integer = 0
        Dim iWaitInterval As Integer = 5

        While oStream Is Nothing
            System.Threading.Thread.Sleep(iWaitInterval)
            iWaitCount = iWaitCount + 1

            'Abort after 1 minute
            If (iWaitCount * iWaitInterval) Mod 60000 = 0 Then
                oTrace.TraceEvent(TraceEventType.Information, 111, "Abort Waiting")
                Exit While
            End If

            Try
                oStream = IO.File.Open(sFilename, IO.FileMode.Open, IO.FileAccess.Read)
            Catch ex As Exception
                If TypeOf ex Is IO.IOException Then
                    'nothing for now
                End If
            End Try

        End While

        oStream.Close()

        oTrace.TraceEvent(TraceEventType.Information, 1, "Entering MainControl.WaitForFile: " & sFilename & " - Waitcount = " & iWaitCount)
        oTrace.Flush()

    End Sub

End Class
