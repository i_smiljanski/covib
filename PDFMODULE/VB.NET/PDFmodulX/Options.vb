﻿' <summary>Container for all options from the config.xml.</summary>

Imports System.Xml.XPath

' Prerequisite: Printer Options, Reiter "Adobe PDF Settings", "Rely on Systems fonts" abwählen!

' Things which have to be set in the Distiller Options:
' 1.    PDF version: 1.4 (aka 9.	Make compatible with: Acrobat 5.0 and later)
' 10.	Embedding: Set to embed all fonts and characters (including font subsets)

Public Class Options

    ''' <summary>
    ''' Type of the Application
    ''' </summary>
    Enum AppType
        None = 0
        Word = 1
        Excel = 2
        HTML = 3
    End Enum

    ''' <summary>
    ''' Contains the Filename of the Config.xml.
    ''' </summary>
    Private sConfigFile As String
    ''' <summary>
    ''' The Filename of the Config.xml.
    ''' </summary>
    Public Property ConfigFile() As String
        Get
            Return sConfigFile
        End Get
        Private Set(ByVal value As String)
            sConfigFile = value
        End Set
    End Property

    ''' <summary>
    ''' Contains the type of the application (Word/Excel)
    ''' </summary>
    Private eApplicationType As AppType
    ''' <summary>
    ''' the type of the application (Word/Excel)
    ''' </summary>
    Public Property ApplicationType() As AppType
        Get
            Return eApplicationType
        End Get
        Set(ByVal value As AppType)
            eApplicationType = value
        End Set
    End Property

    ''' <summary>
    ''' Use SaveAsPDF instead of Acrobat (Word/Excel)
    ''' </summary>
    Private bSaveAsPDF As Boolean
    ''' <summary>
    ''' True if SaveAsPDF is to be used, false otherwise (Word/Excel)
    ''' </summary>
    Public Property SaveAsPDF() As Boolean
        Get
            Return bSaveAsPDF
        End Get
        Set(ByVal value As Boolean)
            bSaveAsPDF = value
        End Set
    End Property

    ''' <summary>
    ''' Use Classic PDF Generation (Word/Excel)
    ''' </summary>
    Private bClassicPdfGeneration As Boolean
    ''' <summary>
    ''' True if the PDF should be generated the classic way (PDFmodul replacement), false otherwise (Word/Excel)
    ''' </summary>
    Public Property ClassicPdfGeneration() As Boolean
        Get
            Return bClassicPdfGeneration
        End Get
        Set(ByVal value As Boolean)
            bClassicPdfGeneration = value
        End Set
    End Property

    ''' <summary>
    ''' Contains The Filename of the Document/Workbook.
    ''' </summary>
    Private sDocFilename As String
    ''' <summary>
    ''' The Filename of the Document/Workbook.
    ''' </summary>
    Public Property DocFilename() As String
        Get
            Return sDocFilename
        End Get
        Private Set(ByVal value As String)
            sDocFilename = value
        End Set
    End Property

    ''' <summary>
    ''' Contains the Password of the document.
    ''' </summary>
    Private sDocPassword As String
    ''' <summary>
    ''' The Password of the document.
    ''' </summary>
    Public Property DocPassword() As String
        Get
            Return sDocPassword
        End Get
        Private Set(ByVal value As String)
            sDocPassword = value
        End Set
    End Property

    ''' <summary>
    ''' Contains the Filename of the final PDF.
    ''' </summary>
    Private sPdfTarget As String
    ''' <summary>
    ''' The Filename of the final PDF.
    ''' </summary>
    Public Property PdfTarget() As String
        Get
            Return sPdfTarget
        End Get
        Private Set(ByVal value As String)
            sPdfTarget = value
        End Set
    End Property

    ''' <summary>
    ''' Contains the Path to the final pdf.
    ''' </summary>
    Private sPdfTargetPath As String
    ''' <summary>
    ''' Path to the final pdf. Usually the same as the working directory.
    ''' </summary>
    Public Property PdfTargetPath() As String
        Get
            Return sPdfTargetPath
        End Get
        Private Set(ByVal value As String)
            sPdfTargetPath = value
        End Set
    End Property

    ''' <summary>
    ''' Contains the PDF Page Mode
    ''' </summary>
    Private sPdfOptionPageMode As String
    ''' <summary>
    ''' PDF Page Mode
    ''' </summary>
    ''' <remarks>
    ''' Possible Values:
    ''' <list type="table">
    ''' <listheader><term>Option</term><description>Description</description></listheader>
    ''' <item><term>UseNone</term><description>Open the document, displaying neither bookmarks nor thumbnail images.</description></item>
    ''' <item><term>UseOutlines</term><description>Open the document and display bookmarks.</description></item>
    ''' <item><term>UseThumbs</term><description>Open the document and display thumbnail images.</description></item>
    ''' <item><term>FullScreen</term><description>Open the document in full screen mode.</description></item>
    ''' </list>
    '''</remarks>
    Public Property PdfOptionPageMode() As String
        Get
            Return sPdfOptionPageMode
        End Get
        Set(ByVal value As String)
            sPdfOptionPageMode = value
        End Set
    End Property


    ''' <summary>
    ''' Contains the PDF Fit.
    ''' </summary>
    Private sPdfOptionFit As String
    ''' <summary>
    ''' PDF Fit (aka zoom mode).
    ''' </summary>
    ''' <remarks>
    ''' Possible Values:
    ''' <list type="table">
    ''' <listheader><term>Option</term><description>Description</description></listheader>
    ''' <item><term>Fit</term><description>Fit page to window size.</description></item>
    ''' <item><term>FitB</term><description>Fit visible page contents to page width.</description></item>
    ''' <item><term>FitH top</term><description>Fit width of the page to window size. 'top' specifies the desired distance
    '''       from the page origin to the upper edge of the window. If 'top' has a value
    '''       of -32768, Acrobat calculates the distance from the page origin to the upper
    '''       edge of the window automatically.</description></item>
    ''' <item><term>FitBH top</term><description>Fit visible page contents to window size. 'top' specifies the desired distance
    '''       from the page origin to the upper edge of the window.</description></item>
    ''' <item><term>FitR x1 y1 x2 y2</term><description>Fit the rectangle specified by the four numbers to the window.</description></item>
    ''' <item><term>FitV left</term><description>Fit page height to window size. 'left' specifies the desired distance from
    '''       the page origin to the left edge of the window.</description></item>
    ''' <item><term>FitBV left</term><description>Fit height of visible page contents to window size. 'left' specifies the desired
    '''       distance from the page origin to the left edge of the window.</description></item>
    ''' <item><term>XYZ left top zoom</term><description>'left' and 'top' specify the desired distance from the page origin to the
    '''       upper left corner of the window. 'zoom' specifies the zoom factor (1
    '''       means 100%). A value of 'null' for one of the three numbers instructs
    '''       Acrobat to retain the old value.</description></item>
    ''' </list>
    ''' </remarks>
    Public Property PdfOptionFit() As String
        Get
            Return sPdfOptionFit
        End Get
        Set(ByVal value As String)
            sPdfOptionFit = value
        End Set
    End Property

    ''' <summary>
    ''' Contains the PDF Page Layout
    ''' </summary>
    Private sPdfOptionPageLayout As String
    ''' <summary>
    ''' PDF Page Layout
    ''' </summary>
    ''' <remarks>
    ''' Possible Values:
    ''' <list type="table">
    ''' <listheader><term>Option</term><description>Description</description></listheader>
    ''' <item><term>SinglePage</term><description>Display one page at a time.</description></item>
    ''' <item><term>OneColumn</term><description>Display the pages in one column.</description></item>
    ''' <item><term>TwoColumnLeft</term><description>Display the pages in two columns, with odd-numbered pages on the left.</description></item>
    ''' <item><term>TwoColumnRight</term><description>Display the pages in two columns, with odd-numbered pages on the right.</description></item>
    ''' <item><term>TwoPageLeft</term><description>(PDF 1.5) Display the pages two at a time, with odd-numbered pages on the left.</description></item>
    ''' <item><term>TwoPageRight</term><description>(PDF 1.5) Display the pages two at a time, with odd-numbered pages on the right.</description></item>
    ''' </list>
    ''' Default value: SinglePage.
    ''' </remarks>
    Public Property PdfOptionPageLayout() As String
        Get
            Return sPdfOptionPageLayout
        End Get
        Set(ByVal value As String)
            sPdfOptionPageLayout = value
        End Set
    End Property

    ''' <summary>
    ''' Contains the PDF Start Page
    ''' </summary>
    Private iPdfOptionStartPage As Integer
    ''' <summary>
    ''' PDF Start Page
    ''' </summary>
    Public Property PdfOptionStartPage() As Integer
        Get
            Return iPdfOptionStartPage
        End Get
        Set(ByVal value As Integer)
            iPdfOptionStartPage = value
        End Set
    End Property

    ''' <summary>
    ''' Contains PDF Out
    ''' </summary>
    Private sPdfOptionOut As String
    ''' <summary>
    ''' PDF Out (aka Bookmark)
    ''' </summary>
    Public Property PdfOptionOut() As String
        Get
            Return sPdfOptionOut
        End Get
        Set(ByVal value As String)
            sPdfOptionOut = value
        End Set
    End Property

    ''' <summary>
    ''' Contains the Custom PDFMARK string.
    ''' </summary>
    Private sPdfCustomPdfMark As String
    ''' <summary>
    ''' Custom PDFMARK string.
    ''' </summary>
    Public Property PdfCustomPdfMark() As String
        Get
            Return sPdfCustomPdfMark
        End Get
        Set(ByVal value As String)
            sPdfCustomPdfMark = value
        End Set
    End Property

    ''' <summary>
    ''' Contains the alternate Pdf Creation Flag.
    ''' </summary>
    Private bPdfAlternateCreation As Boolean
    ''' <summary>
    ''' If set, the alternate Pdf Creation mode is used.
    ''' </summary>
    Public Property PdfAlternateCreation() As String
        Get
            Return bPdfAlternateCreation
        End Get
        Set(ByVal value As String)
            bPdfAlternateCreation = value
        End Set
    End Property

    ''' <summary>
    ''' Contains the Remove Acrotray Flag.
    ''' </summary>
    Private bPdfRemoveAcrotray As Boolean
    ''' <summary>
    ''' If set, all acrotrays found will be removed
    ''' </summary>
    Public Property PdfRemoveAcrotray() As String
        Get
            Return bPdfRemoveAcrotray
        End Get
        Set(ByVal value As String)
            bPdfRemoveAcrotray = value
        End Set
    End Property


    ''' <summary>
    ''' Contructor.
    ''' </summary>
    ''' <remarks>
    ''' Sets the default values according to the FRS (EXP25), 
    ''' but these will likely be overriden by the values of the config.xml.
    ''' </remarks>
    Public Sub New()
        oTrace.TraceEvent(TraceEventType.Information, 1, "Entering Options.New")

        'Set Defaults
        sPdfOptionPageMode = "UseOutlines"
        sPdfOptionFit = "Default"
        iPdfOptionStartPage = 1
        sPdfOptionPageLayout = "SinglePage"
        sPdfOptionOut = "Test: &&PRODUCT_BATCH_NUMBER& - &&STUDY_NUMBER&."
        sPdfCustomPdfMark = String.Empty
        bPdfAlternateCreation = False
        bPdfRemoveAcrotray = False

        bSaveAsPDF = False
        ClassicPdfGeneration = True

    End Sub

    ''' <summary>
    ''' In absence of a 'real' destructor.
    ''' </summary>
    Public Sub CleanUp()
        oTrace.TraceEvent(TraceEventType.Information, 1, "Entering Options.CleanUp")

    End Sub

    ''' <summary>
    ''' Parses the command line arguments.
    ''' </summary>
    ''' <remarks>
    ''' If no config.xml is given at the command line, it defaults to "config.xml" (in the app.path).
    ''' </remarks>
    Sub ParseArgs()
        oTrace.TraceEvent(TraceEventType.Information, 1, "Entering Options.ParseArgs")

        If Environment.GetCommandLineArgs.Count = 1 Then
            oTrace.TraceEvent(TraceEventType.Error, 10, "No Config File parameter given. Trying default config.xml.")
            sConfigFile = "config.xml"
        Else
            sConfigFile = Environment.GetCommandLineArgs(1)
        End If

    End Sub

    ''' <summary>
    ''' Reads the config.xml file.
    ''' </summary>
    ''' <remarks>
    ''' With the exception of fullpathname ("/config/apps"), all terms are read from 
    ''' "/config/apps/app[@name='pdfmodul']" from the config.xml.     
    ''' <list type="bullet">
    '''   <item><description>target</description></item>
    '''   <item><description>source</description></item>
    '''   <item><description>sourcepassword</description></item>
    '''   <item><description>pdfpagemode</description></item>
    '''   <item><description>pdffit</description></item>
    '''   <item><description>pdfstartpage</description></item>
    '''   <item><description>pdfpagelayout</description></item>
    '''   <item><description>pdfout</description></item>
    '''   <item><description>pdfcustompdfmark</description></item>
    '''   <item><description>SaveAsPDF</description></item>
    '''   <item><description>ClassicPDFGeneration</description></item>
    ''' </list>
    ''' It also tries to determine the type of the application by analysing the Document Name (.doc? .xls?)
    ''' </remarks>
    ''' <returns>true if the file was opend and could be read, false if something went wrong.</returns>  
    Function ReadConfigFile() As Boolean
        oTrace.TraceEvent(TraceEventType.Information, 1, "Entering Options.ReadConfigFile")

        Dim oXpathDoc As XPathDocument
        Dim oXmlNav As XPathNavigator
        Dim oResult As Object

        Try
            oXpathDoc = New XPathDocument(sConfigFile)
            oXmlNav = oXpathDoc.CreateNavigator()
        Catch ex As XPathException
            HandleException(ex, "ReadConfigFile, XMLException")
            Return False
        Catch ex As Exception
            HandleException(ex, "ReadConfigFile")
            Return False
        End Try

        'Read Fullpath
        oResult = oXmlNav.SelectSingleNode("/config/apps/fullpathname")
        If Not oResult Is Nothing Then
            If oResult.ToString.EndsWith("\") Then
                sPdfTargetPath = oResult.ToString
            Else
                sPdfTargetPath = oResult.ToString & "\"
            End If

            oTrace.TraceEvent(TraceEventType.Information, 11, "sPdfTargetPath = " & sPdfTargetPath)
        End If

        'Read PdfTarget
        oResult = oXmlNav.SelectSingleNode("/config/apps/app[@name='pdfmodul']/target")
        If Not oResult Is Nothing Then
            If oResult.ToString = String.Empty Then
                sPdfTarget = Environment.CurrentDirectory & "\" & oResult.ToString
            Else
                sPdfTarget = sPdfTargetPath & oResult.ToString
            End If
            oTrace.TraceEvent(TraceEventType.Information, 11, "sPdfTarget     = " & sPdfTarget)
        End If

        'Read DocFilename
        oResult = oXmlNav.SelectSingleNode("/config/apps/app[@name='pdfmodul']/source")
        If Not oResult Is Nothing Then
            sDocFilename = sPdfTargetPath & oResult.ToString
            oTrace.TraceEvent(TraceEventType.Information, 11, "sDocFilename   = " & sDocFilename)
        End If

        'Read DocPassword
        oResult = oXmlNav.SelectSingleNode("/config/apps/app[@name='pdfmodul']/sourcepassword")
        If Not oResult Is Nothing Then
            sDocPassword = oResult.ToString
            oTrace.TraceEvent(TraceEventType.Information, 11, "sDocPassword   = " & sDocPassword)
        End If

        'Read Pdf Page Mode
        oResult = oXmlNav.SelectSingleNode("/config/apps/app[@name='pdfmodul']/pdfpagemode")
        If Not oResult Is Nothing Then
            sPdfOptionPageMode = oResult.ToString
            oTrace.TraceEvent(TraceEventType.Information, 11, "sPdfOptionPageMode   = " & sPdfOptionPageMode)
        End If

        'Read Pdf Fit
        oResult = oXmlNav.SelectSingleNode("/config/apps/app[@name='pdfmodul']/pdffit")
        If Not oResult Is Nothing Then
            sPdfOptionFit = oResult.ToString
            oTrace.TraceEvent(TraceEventType.Information, 11, "sPdfOptionFit   = " & sPdfOptionFit)
        End If

        'Read Pdf Start Page
        oResult = oXmlNav.SelectSingleNode("/config/apps/app[@name='pdfmodul']/pdfstartpage")
        If Not oResult Is Nothing Then
            iPdfOptionStartPage = Integer.Parse(oResult.ToString)
            oTrace.TraceEvent(TraceEventType.Information, 11, "iPdfOptionStartPage   = " & iPdfOptionStartPage)
        End If

        'Read Pdf Page Layout
        oResult = oXmlNav.SelectSingleNode("/config/apps/app[@name='pdfmodul']/pdfpagelayout")
        If Not oResult Is Nothing Then
            sPdfOptionPageLayout = oResult.ToString
            oTrace.TraceEvent(TraceEventType.Information, 11, "sPdfOptionPageLayout   = " & sPdfOptionPageLayout)
        End If

        'Read Pdf Out (aka Bookmark)
        oResult = oXmlNav.SelectSingleNode("/config/apps/app[@name='pdfmodul']/pdfout")
        If Not oResult Is Nothing Then
            sPdfOptionOut = oResult.ToString
            oTrace.TraceEvent(TraceEventType.Information, 11, "sPdfOptionOut   = " & sPdfOptionOut)
        End If

        'Custom Pdf Mark
        oResult = oXmlNav.SelectSingleNode("/config/apps/app[@name='pdfmodul']/pdfcustompdfmark")
        If Not oResult Is Nothing Then
            sPdfCustomPdfMark = oResult.ToString
            oTrace.TraceEvent(TraceEventType.Information, 11, "sPdfCustomPdfMark   = " & sPdfCustomPdfMark)
        End If

        'PdfAlternateCreation
        oResult = oXmlNav.SelectSingleNode("/config/apps/app[@name='pdfmodul']/pdfalternatecreation")
        If Not oResult Is Nothing Then
            If oResult.ToString.ToUpper = "T" Then bPdfAlternateCreation = True
            If oResult.ToString.ToUpper = "F" Then bPdfAlternateCreation = False
            oTrace.TraceEvent(TraceEventType.Information, 11, "sAlternatePdfCreation   = " & bPdfAlternateCreation)
        End If

        'PdfRemoveAcrotray
        oResult = oXmlNav.SelectSingleNode("/config/apps/app[@name='pdfmodul']/pdfremoveacrotray")
        If Not oResult Is Nothing Then
            If oResult.ToString.ToUpper = "T" Then bPdfRemoveAcrotray = True
            If oResult.ToString.ToUpper = "F" Then bPdfRemoveAcrotray = False
            oTrace.TraceEvent(TraceEventType.Information, 11, "sPdfRemoveAcrotray   = " & bPdfRemoveAcrotray)
        End If

        'SaveAsPDF
        oResult = oXmlNav.SelectSingleNode("/config/apps/app[@name='pdfmodul']/SaveAsPDF")
        If Not oResult Is Nothing Then
            If oResult.ToString.ToUpper = "T" Then bSaveAsPDF = True
            If oResult.ToString.ToUpper = "F" Then bSaveAsPDF = False
            oTrace.TraceEvent(TraceEventType.Information, 11, "bSaveAsPDF   = " & bSaveAsPDF)
        End If

        'ClassicPDFGeneration
        oResult = oXmlNav.SelectSingleNode("/config/apps/app[@name='pdfmodul']/ClassicPDFGeneration")
        If Not oResult Is Nothing Then
            If oResult.ToString.ToUpper = "T" Then bClassicPdfGeneration = True
            If oResult.ToString.ToUpper = "F" Then bClassicPdfGeneration = False
            oTrace.TraceEvent(TraceEventType.Information, 11, "bClassicPDFGeneration   = " & bClassicPdfGeneration)
        End If

        'Determine the type of the application
        eApplicationType = DetermineAppType(sDocFilename)

        Return True

    End Function

    ''' <summary>
    ''' Determines the Apptype according to the extension of the document filename
    ''' </summary>
    ''' <param name="sFilename">The filename of the document</param>  
    ''' <returns>The PDFMAKERAPILib.SupportedApps value associated to the extension of the document</returns>  
    Private Function DetermineAppType(sFilename As String) As AppType
        oTrace.TraceEvent(TraceEventType.Information, 1, "Entering Options.GetAppType")

        Dim sAppType As AppType

        Select Case System.IO.Path.GetExtension(sFilename).ToUpper
            Case ".DOC", ".DOCX"
                sAppType = AppType.Word
            Case ".XLS", ".XLSX"
                sAppType = AppType.Excel
            Case ".HTML", ".HTM"
                sAppType = AppType.HTML
            Case Else
                sAppType = AppType.None
        End Select

        Return sAppType

    End Function

End Class
