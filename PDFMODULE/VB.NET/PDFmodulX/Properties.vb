﻿' <summary>Container for the properties read from the word document.</summary>

Public Class Properties

    Private colProperties As Hashtable
    ''' <summary>
    ''' The collection containing the properties.
    ''' </summary>
    Public Property Properties() As Hashtable
        Get
            Return colProperties
        End Get
        Private Set(ByVal value As Hashtable)
            colProperties = value
        End Set
    End Property

    ''' <summary>
    ''' Contructor.
    ''' </summary>
    Public Sub New()
        oTrace.TraceEvent(TraceEventType.Information, 1, "Entering Properties.New")

        'Initialize the Hashtable
        colProperties = New Hashtable

    End Sub

    ''' <summary>
    ''' In absence of a 'real' destructor.
    ''' </summary>
    Public Sub CleanUp()
        oTrace.TraceEvent(TraceEventType.Information, 1, "Entering Properties.CleanUp")

        colProperties = Nothing

    End Sub

    ''' <summary>
    ''' Adds a property pair (Key+Value) to the property collection.
    ''' </summary>
    ''' <param name="sKey">The property name.</param>  
    ''' <param name="sValue">The property value.</param>  
    Public Sub AddProperty(ByVal sKey As String, ByVal sValue As String)
        oTrace.TraceEvent(TraceEventType.Information, 1, "Entering Properties.AddProperty:")
        oTrace.TraceEvent(TraceEventType.Information, 10, "Key to be added: " & sKey)

        colProperties.Add(sKey, sValue)

    End Sub

    ''' <summary>
    ''' Retrives a property value for the given property name (aka key).
    ''' </summary>
    ''' <param name="sKey">The property name.</param>  
    ''' <returns>The property value, if existing, or empty string if nothing was found.</returns>  
    Public Function GetProperty(ByVal sKey As String) As String
        oTrace.TraceEvent(TraceEventType.Information, 1, "Entering Properties.GetProperty:")
        oTrace.TraceEvent(TraceEventType.Information, 10, "Key to be retrieved: " & sKey)

        If colProperties.Contains(sKey) Then
            Return colProperties.Item(sKey).ToString
        Else
            oTrace.TraceEvent(TraceEventType.Error, 10, "Property not found: " & sKey)
            Return String.Empty
        End If

    End Function

    ''' <summary>
    ''' Returns the number of properties inside the collection.
    ''' </summary>
    ''' <returns>the number of properties inside the collection.</returns>  
    Public Function NumberOfProperties() As Integer
        oTrace.TraceEvent(TraceEventType.Information, 1, "Entering Properties.NumberOfProperties")
        oTrace.TraceEvent(TraceEventType.Information, 10, "Number of Properties: " & colProperties.Count)

        Return colProperties.Count

    End Function

    ''' <summary>
    ''' Returns a collection of all keys in the collection.
    ''' </summary>
    ''' <returns>a collection of all keys in the collection.</returns>  
    Public Function GetAllKeys() As ICollection
        oTrace.TraceEvent(TraceEventType.Information, 1, "Entering Properties.GetAllKeys:")

        Return colProperties.Keys

    End Function

End Class
