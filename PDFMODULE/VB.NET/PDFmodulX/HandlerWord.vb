﻿' <summary>Handels everything Word.</summary>

Imports Microsoft.Office.Interop.Word
Imports System.Text
Imports System.Security.Cryptography


Public Class HandlerWord

    Private oWord As Application = Nothing
    Private sDocumentName As String

    ''' <summary>
    ''' Contructor.
    ''' Initializes the Word Object.
    ''' </summary>
    Public Sub New()
        oTrace.TraceEvent(TraceEventType.Information, 1, "Entering HandlerWord.New")

        'Initialize the Word Object
        oWord = New Application
        oWord.Visible = False

    End Sub

    ''' <summary>
    ''' In absence of a 'real' destructor.
    ''' </summary>
    Public Sub CleanUp()
        oTrace.TraceEvent(TraceEventType.Information, 1, "Entering HandlerWord.CleanUp")

        'If the object is already empty, there is nothing to do here
        If oWord Is Nothing Then Exit Sub

        'Now leave word.
        oWord.Quit(False)

        'Free the word object
        oWord = Nothing

    End Sub

    ''' <summary>
    ''' In Case the PdfModulX exits due to an error, Main has to have the possibility to close the word instance gracefully.
    ''' </summary>
    ''' <returns>the Word Object</returns>  
    Public Function GetWordBackup() As Application

        Return oWord

    End Function

    ''' <summary>
    ''' Open the associated document.
    ''' </summary>
    ''' <param name="sDocFile">The documents filename (including path).</param>  
    ''' <returns>true if the file was opend, false if something went wrong.</returns>  
    Public Function OpenDoc(ByVal sDocFile As String) As Boolean
        oTrace.TraceEvent(TraceEventType.Information, 1, "Entering HandlerWord.OpenDoc")
        oTrace.Flush()

        Try
            oTrace.TraceEvent(TraceEventType.Information, 1, "Document Size: " & My.Computer.FileSystem.GetFileInfo(sDocFile).Length)
            oWord.Documents.Open(sDocFile)
        Catch ex As Exception
            HandleException(ex, "OpenDoc")
            Return False
        End Try

        sDocumentName = sDocFile

        Return True

    End Function

    ''' <summary>
    ''' Prints the document to the active printer.
    ''' </summary>
    ''' <param name="sFilename">The documents filename (including path).</param>  
    ''' <returns>true if the file was 'printed', false if something went wrong.</returns>  
    Public Function PrintDocument(ByVal sFilename) As Boolean
        oTrace.TraceEvent(TraceEventType.Information, 1, "Entering HandlerWord.PrintDocument")
        oTrace.Flush()

        Try
            oWord.Documents(sDocumentName).PrintOut(OutputFileName:=sFilename)
        Catch ex As Exception
            HandleException(ex, "PrintDocument")
            Return False
        End Try

        Return True

    End Function

    ''' <summary>
    ''' Waits until all print jobs are finished
    ''' </summary>
    Public Sub WaitForPrinter()
        oTrace.TraceEvent(TraceEventType.Information, 1, "Entering HandlerWord.WaitForPrinter")
        oTrace.Flush()

        Dim iWaitCount As Integer = 0
        Dim iWaitInterval As Integer = 250

        While oWord.BackgroundPrintingStatus > 0
            System.Threading.Thread.Sleep(iWaitInterval)
            iWaitCount = iWaitCount + 1

            'Logentry every minute
            If (iWaitCount * iWaitInterval) Mod 60000 = 0 Then
                oTrace.TraceEvent(TraceEventType.Information, 111, "Waiting - Number of Print Jobs: " & oWord.BackgroundPrintingStatus)
            End If

            'Abort after 5 minutes
            If (iWaitCount * iWaitInterval) Mod 60000 * 5 = 0 Then
                oTrace.TraceEvent(TraceEventType.Information, 111, "Abort Waiting - Number of Print Jobs: " & oWord.BackgroundPrintingStatus)
                Exit While
            End If

        End While

    End Sub

    ''' <summary>
    ''' Retrieves all custom Properties from the word documents and writes them to the Properties-Object.
    ''' </summary>
    ''' <param name="oProperties">The Properties Object.</param>  
    Public Sub RetrieveDocumentProperties(ByVal oProperties As Properties)
        oTrace.TraceEvent(TraceEventType.Information, 1, "Entering HandlerWord.RetrieveDocumentProperties")

        Dim oProperty As Object

        'Fetch all Properties to the local Object
        For Each oProperty In oWord.Documents(sDocumentName).CustomDocumentProperties
            oProperties.AddProperty(oProperty.Name, oProperty.Value)
        Next oProperty

    End Sub

    ''' <summary>
    ''' Saves the document as a PDF document
    ''' </summary>
    ''' <param name="sFilename">The pdf filename (including path).</param>  
    ''' <returns>True if the document was exported successfully, false if something went wrong</returns>  
    Public Function SaveAsPDF(ByVal sFilename As String) As Boolean
        oTrace.TraceEvent(TraceEventType.Information, 1, "Entering HandlerWord.SaveAsPDF")

        Try
            oWord.Documents(sDocumentName).ExportAsFixedFormat(OutputFileName:=sFilename, ExportFormat:=WdExportFormat.wdExportFormatPDF)

            'Word adds a .pdf, if no extension is given. So check, if the file exists.
            If Not IO.File.Exists(sFilename) Then

                Try
                    My.Computer.FileSystem.RenameFile(sFilename & ".pdf", IO.Path.GetFileName(sFilename))
                Catch ex As Exception
                    oTrace.TraceEvent(TraceEventType.Critical, 12, "Rename failed. iSR will probably fail to locate the resulting pdf.")
                End Try

            End If

        Catch ex As Exception
            HandleException(ex, "SaveAsPDF")
            Return False
        End Try

        Return True

    End Function

    ''' <summary>
    ''' Removes the protection of the document using the given password
    ''' </summary>
    ''' <param name="sPassword">The password for the document.</param>  
    ''' <returns>True if the document was unprotected successfully, false if something went wrong</returns>  
    Public Function UnprotectDocument(ByVal sPassword As String) As Boolean
        oTrace.TraceEvent(TraceEventType.Information, 1, "Entering HandlerWord.UnprotectDocument")

        Try
            If oWord.Documents(sDocumentName).ProtectionType <> WdProtectionType.wdNoProtection Then
                oWord.Documents(sDocumentName).Unprotect(GenerateMD5Hash(sPassword).Substring(0, 8))
            End If
        Catch ex As Exception
            HandleException(ex, "SaveDocument")
            Return False
        End Try

        Return True

    End Function

    ''' <summary>
    ''' Saves the document
    ''' </summary>
    ''' <param name="sFilename">The pdf filename (including path).</param>  
    ''' <returns>True if the document was saved, false if something went wrong</returns>  
    Public Function SaveDocument(ByVal sFilename As String) As Boolean
        oTrace.TraceEvent(TraceEventType.Information, 1, "Entering HandlerWord.SaveDocument")

        Try
            oWord.Documents(sDocumentName).SaveAs(FileName:=sFilename)
        Catch ex As Exception
            HandleException(ex, "SaveDocument")
            Return False
        End Try

        Return True

    End Function

    ''' <summary>
    ''' Generates a MD5 Hash, which can be used for password "decryption"
    ''' </summary>
    ''' <param name="sText">The 'raw' password</param>  
    ''' <returns>The MD5 hash for the given string</returns>  
    Private Function GenerateMD5Hash(ByVal sText As String) As String
        oTrace.TraceEvent(TraceEventType.Information, 1, "Entering HandlerWord.GenerateMD5Hash")

        'Instantiate an MD5 Provider object
        Dim Md5 As New MD5CryptoServiceProvider()

        Dim aHashResult As Byte()
        Dim sTemp As String = ""
        Dim sConvResult As String = ""

        'Compute the hash value from the source
        aHashResult = Md5.ComputeHash(Encoding.ASCII.GetBytes(sText))

        For i As Integer = 0 To aHashResult.Length - 1
            sTemp = Hex(aHashResult(i)).ToLower
            If sTemp.Length = 1 Then sTemp = "0" & sTemp
            sConvResult += sTemp
        Next

        Return sConvResult

    End Function

    ''' <summary>
    ''' Sets the orientation
    ''' </summary>
    ''' <param name="iOrientation">the target orientation.</param>  
    Public Sub SetOrientation(ByVal iOrientation As WdOrientation)
        oTrace.TraceEvent(TraceEventType.Information, 1, "Entering HandlerWord.SaveDocument")

        Try
            oWord.ActiveDocument.PageSetup.Orientation = iOrientation
        Catch ex As Exception
            HandleException(ex, "SetOrientation")
        End Try

    End Sub

End Class
