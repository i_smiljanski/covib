﻿' <summary>Handels everything PDF - the classic way.</summary>

Imports System.Text.RegularExpressions
Imports System.IO
Imports PDFMAKERAPILib

Public Class HandlerPdfClassic

    Dim oPDFMakerApp As PDFMakerApp

    ''' <summary>
    ''' Contructor.
    ''' </summary>
    Public Sub New()
        oTrace.TraceEvent(TraceEventType.Information, 1, "Entering HandlerPdfClassic.New")

        'Create an Instance for PdfDistiller
        oPDFMakerApp = New PDFMakerApp

    End Sub

    ''' <summary>
    ''' In absence of a 'real' destructor.
    ''' </summary>
    Public Sub CleanUp()
        oTrace.TraceEvent(TraceEventType.Information, 1, "Entering HandlerPdfClassic.CleanUp")

        oPDFMakerApp = Nothing

    End Sub

    ''' <summary>
    ''' Creates the PDF module the classic way via the PDFMAKERAPILib
    ''' </summary>
    ''' <param name="sTempFilename">The input filename (including path) of the document</param>  
    ''' <param name="sPDF">The output filename (including path) of the resulting pdf.</param>  
    ''' <param name="sUseProtection">True if the document has to be protected.</param>  
    ''' <param name="sPassword">Password for the protection.</param>  
    ''' <returns>0 if all went well, any other number if anything went wrong. This is actually the return value of the PDFMakerApp.CreatePDF function.</returns>  
    Public Function ConvertToPdfClassic(sTempFilename As String, sPDF As String, sUseProtection As Boolean, sPassword As String) As Integer
        oTrace.TraceEvent(TraceEventType.Information, 1, "Entering HandlerPdfClassic.ConvertToPdfClassic")

        Dim logSercuritySettings As String
        Dim ConvSettings As ConversionSettings
        Dim SecuritySettings As securitySettings
        Dim SettingsBitfield As Long
        Dim SettingsFile As String = ""
        Dim sReturnvalue As Integer

        ConvSettings = New ConversionSettings

        'Defaultsettings holen
        oTrace.TraceEvent(TraceEventType.Information, 1, "Getting default settings.")
        ConvSettings.GetAppConversionDefaults(GetAppType(sTempFilename), SettingsBitfield, SettingsFile)

        'Defaultsettings festlegen
        oTrace.TraceEvent(TraceEventType.Information, 1, "Set default settings.")
        ConvSettings.SetConversionParameters(SettingsBitfield)

        'Sicherheitsrichtlinien festlegen
        oTrace.TraceEvent(TraceEventType.Information, 1, "Set SecuritySettings.")
        SecuritySettings = ConvSettings.GetSecuritySettings
        If sUseProtection = True Then
            SecuritySettings.AllowedChanges = tagAllowedChangesEnum.kAllowChangesCommentingFormfillsSign
            SecuritySettings.PrintingModeAllowed = tagPrintingModesAllowedEnum.kPrintingAllowedHighRes
            SecuritySettings.PermsPasswd = sPassword
            SecuritySettings.PermsPasswdNeeded = True
            ConvSettings.SetSecuritySettings(SecuritySettings)
        End If

        'log the actual security settings
        logSercuritySettings = SecuritySettings.AllowedChanges
        logSercuritySettings = logSercuritySettings & "|" & SecuritySettings.AllowedPermissionsBits
        logSercuritySettings = logSercuritySettings & "|" & SecuritySettings.AttachmentPasswd
        logSercuritySettings = logSercuritySettings & "|" & SecuritySettings.AttachmentPasswdNeeded
        logSercuritySettings = logSercuritySettings & "|" & SecuritySettings.EnableCopyingContent
        logSercuritySettings = logSercuritySettings & "|" & SecuritySettings.EnablePlaintextMetadata
        logSercuritySettings = logSercuritySettings & "|" & SecuritySettings.EnableTextAccessibility
        logSercuritySettings = logSercuritySettings & "|" & SecuritySettings.EncryptAttachmentsOnly
        logSercuritySettings = logSercuritySettings & "|" & SecuritySettings.KeyLen
        logSercuritySettings = logSercuritySettings & "|" & SecuritySettings.OpenDocPasswd
        logSercuritySettings = logSercuritySettings & "|" & SecuritySettings.OpenDocPasswdNeeded
        logSercuritySettings = logSercuritySettings & "|" & SecuritySettings.PermsPasswd
        logSercuritySettings = logSercuritySettings & "|" & SecuritySettings.PermsPasswdNeeded
        logSercuritySettings = logSercuritySettings & "|" & SecuritySettings.PrintingModeAllowed


        oTrace.TraceEvent(TraceEventType.Information, 1, "PDF filename: " & sPDF)
        oTrace.TraceEvent(TraceEventType.Information, 1, "PDF Security Settings:" & logSercuritySettings)
        oTrace.Flush()

        If sPDF <> "" Then
            sReturnvalue = oPDFMakerApp.CreatePDF(sTempFilename, sPDF, ConvSettings)


        Else
            sReturnvalue = -1
        End If

        oTrace.TraceEvent(TraceEventType.Information, 1, "oPDFMakerApp.CreatePDF Returnvalue = " & sReturnvalue)

        Return sReturnvalue

    End Function

    ''' <summary>
    ''' Determines the PDFMAKERAPILib.SupportedApps according to the extension of the document filename
    ''' </summary>
    ''' <param name="sFilename">The filename of the document</param>  
    ''' <returns>The PDFMAKERAPILib.SupportedApps value associated to the extension of the document</returns>  
    Private Function GetAppType(sFilename As String) As SupportedApps
        oTrace.TraceEvent(TraceEventType.Information, 1, "Entering HandlerPdfClassic.GetAppType")

        Dim sAppType As SupportedApps

        Select Case System.IO.Path.GetExtension(sFilename).ToUpper
            Case ".DOC", ".DOCX"
                sAppType = SupportedApps.kAppWord
            Case ".XLS", ".XLSX"
                sAppType = SupportedApps.kAppExcel
            Case Else
                sAppType = SupportedApps.kAppUnknown
        End Select

        Return sAppType

    End Function


End Class
