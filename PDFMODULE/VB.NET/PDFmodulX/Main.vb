﻿' <summary>The Main function, and some global utility routines for error handling.</summary>

Imports System
Imports System.Diagnostics
Imports System.AppDomain

Module Main

    ''' <summary>
    ''' Constant for the Name of the Adobe PDF Printer driver. Make this configurable at the next opportuniy
    ''' </summary>
    Public Const PDFPRINTER As String = "Adobe PDF"

    ''' <summary>
    ''' Global Trace object
    ''' </summary>
    Public oTrace As TraceSource = New TraceSource("PdfModul")

    ''' <summary>
    ''' Entry Point for the program.
    ''' Just handles the trace system and contains a top level try/catch for everyting.
    ''' </summary>
    Function Main() As Integer

        Dim iReturn As Integer = 0
        Dim oMainCon As MainControl = Nothing
        Dim idxLogListener As New Integer()

        'Change the Current path to the app.path. This is essential for use on a rendering server
        Environment.CurrentDirectory = CurrentDomain.BaseDirectory()

        'Configue Trace
        Dim oSwitch = New SourceSwitch("PdfModulSwitch", "Information")
        oTrace.Switch = oSwitch
        oTrace.Switch.Level = SourceLevels.Verbose
        idxLogListener = oTrace.Listeners.Add(New TextWriterTraceListener("PDFmodulX.log"))

        'Turn this on when exact timestamps are needed. The log will become messy!
        'oTrace.Listeners(idxLogListener).TraceOutputOptions = TraceOptions.DateTime

        'Console Output (not needed if log present)
        'Dim oConsoleListener = New ConsoleTraceListener()
        'oTrace.Listeners.Add(oConsoleListener)

        'And go
        oTrace.TraceEvent(TraceEventType.Information, 1, "Entering Main")

        If System.Diagnostics.Debugger.IsAttached() = False Then
            oTrace.TraceEvent(TraceEventType.Information, 10, "PdfModulX is running native.")

            'Take over Error handling
            Try
                oMainCon = New MainControl
                iReturn = oMainCon.MainSequence()
                oMainCon.CleanUp()
            Catch ex As Exception
                oTrace.TraceEvent(TraceEventType.Critical, 10, "Exception Occured in MainControl.MainSequence: " & ex.ToString)

                'Check if there is a Word Object still open
                If Not oMainCon.GetWordBackup Is Nothing Then
                    oTrace.TraceEvent(TraceEventType.Information, 10, "Closing Word Instance")
                    oMainCon.GetWordBackup.Quit(True)
                End If

                If Not oMainCon.GetExcelBackup Is Nothing Then
                    oTrace.TraceEvent(TraceEventType.Information, 10, "Closing Excel Instance")
                    oMainCon.GetWordBackup.Quit()
                End If

                iReturn = -1

            End Try

        Else
            oTrace.TraceEvent(TraceEventType.Information, 10, "PdfModulX is running in IDE.")

            'Let the IDE do the error handling
            oMainCon = New MainControl
            iReturn = oMainCon.MainSequence()
            oMainCon.CleanUp()

        End If

        oMainCon = Nothing

        oTrace.TraceEvent(TraceEventType.Information, 1, "Main Return Code: " & iReturn)
        oTrace.TraceEvent(TraceEventType.Information, 1, "Exiting Main")

        oTrace.Flush()
        oTrace.Close()

        oTrace = Nothing

        Return iReturn

    End Function

    ''' <summary>
    ''' Handles Exceptions by writing a message to the trace log.
    ''' Called only at places where exceptions are expected, i.e. in the catch block.
    ''' </summary>
    ''' <param name="ex">The exception.</param>  
    ''' <param name="sSource">Additional information, usually the name of the method.</param>  
    Public Sub HandleException(ByVal ex As Exception, ByVal sSource As String)

        oTrace.TraceEvent(TraceEventType.Warning, 10, "Exception Occured in " & sSource & ": " & ex.ToString)

    End Sub

End Module
