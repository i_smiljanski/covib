﻿' <summary>Handels everything regarding printer settings.</summary>

Imports System.Drawing
Imports System.Drawing.Printing

Public Class HandlerPrinter

    Private sDefaultPrinterName As String
    Private oPrinterSettings As PrinterSettings

    ''' <summary>
    ''' Contructor.
    ''' Initializes the PrinterSetting and remembers the Original Printer Name
    ''' </summary>
    Public Sub New()
        oTrace.TraceEvent(TraceEventType.Information, 1, "Entering Printer.New")

        'Initialize the Printing Object
        oPrinterSettings = New PrinterSettings

        SetDefaultPrinterName()

    End Sub

    ''' <summary>
    ''' In absence of a 'real' destructor.
    ''' </summary>
    Public Sub CleanUp()
        oTrace.TraceEvent(TraceEventType.Information, 1, "Entering Printer.CleanUp")

        oPrinterSettings = Nothing

    End Sub

    ''' <summary>
    ''' Contructor.
    ''' Initializes the PrinterSetting and remembers the Original Printer Name
    ''' </summary>
    ''' <returns>true if the printer was successfully changed, false if something went wrong</returns>  
    Public Function ChangePrinterToPDF() As Boolean
        oTrace.TraceEvent(TraceEventType.Information, 1, "Entering Printer.ChangePrinterToPDF")

        Dim Printers As New Printing.PrintDocument()
        Dim Printername = Printers.PrinterSettings.PrinterName

        'If the printer is already set to pdf, no need to do anything here
        If oPrinterSettings.PrinterName = PDFPRINTER Then Return True

        ' Find the PDF Printer
        For Each Printername In PrinterSettings.InstalledPrinters
            If Printername = PDFPRINTER Then
                Exit For
            End If
        Next

        'If the PDFprinter was not found, exit
        If Printername <> PDFPRINTER Then Return False

        'Change the printer
        Try
            Shell(String.Format("rundll32 printui.dll,PrintUIEntry /y /n ""{0}""", PDFPRINTER))
        Catch ex As Exception
            HandleException(ex, "ChangePrinterToPDF")
            Return False
        End Try

        Return True

    End Function

    ''' <summary>
    ''' Changes the printer back to the original one
    ''' </summary>
    Public Sub ChangePrinterToOriginal()
        oTrace.TraceEvent(TraceEventType.Information, 1, "Entering Printer.ChangePrinterToOriginal")

        'If the printer was already set to pdf, no need to do anything here
        If sDefaultPrinterName = PDFPRINTER Then Exit Sub

        'Change the printer back
        Shell(String.Format("rundll32 printui.dll,PrintUIEntry /y /n ""{0}""", sDefaultPrinterName))

    End Sub

    ''' <summary>
    ''' Remebers the value of the original default printer.
    ''' </summary>
    Private Sub SetDefaultPrinterName()
        oTrace.TraceEvent(TraceEventType.Information, 1, "Entering Printer.SetDefaultPrinterName")

        Try
            sDefaultPrinterName = oPrinterSettings.PrinterName
        Catch ex As System.Exception
            HandleException(ex, "SetDefaultPrinterName")
            sDefaultPrinterName = ""
        End Try

    End Sub

End Class
