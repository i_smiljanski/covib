Attribute VB_Name = "modConvertToPDF"
Option Explicit

Declare Function GetOpenFileName Lib "comdlg32.dll" Alias "GetOpenFileNameA" (pOpenfilename As OPENFILENAME) As Long

Type OPENFILENAME
    lStructSize As Long
    hwndOwner As Long
    hInstance As Long
    lpstrFilter As String
    lpstrCustomFilter As String
    nMaxCustFilter As Long
    nFilterIndex As Long
    lpstrFile As String
    nMaxFile As Long
    lpstrFileTitle As String
    nMaxFileTitle As Long
    lpstrInitialDir As String
    lpstrTitle As String
    Flags As Long
    nFileOffset As Integer
    nFileExtension As Integer
    lpstrDefExt As String
    lCustData As Long
    lpfnHook As Long
    lpTemplateName As Long
End Type

Public Const OFN_FILEMUSTEXIST = &H1000
Public Const OFN_READONLY = &H1
Public Const OFN_HIDEREADONLY = &H4

Sub InvokeConvertToPDF5(oApp As Object, sApplication$, sDocFileName$, sPasswd$, sTempFilename$)

    'Verweis auf AdobePDFMaker anlegen!
    
    Dim bResult As Boolean
    Dim value As Variant
    Dim aDoc As Word.Document
    Dim aWorkbook As Excel.Workbook
    
    'Puffervariablen; hier werden die Einstellungen gesichert, die f�r die pdf generierung
    'umgebogen werden. Diese Werte werden nach vollbrachter Arbeit wieder zur�ckgesetzt.
    
    Dim AutoSaveFile As Boolean
    Dim ViewPDFFile As Boolean
    Dim OwnerPasswdReq As Boolean
    Dim UserPasswdReq As Boolean
    Dim SecurityPassword As String
    Dim AllowPrint As Boolean
    Dim AllowEdit As Boolean
    Dim AllowNotes As Boolean
    Dim AllowFillAndSign As Boolean
    Dim AllowAccessibility As Boolean
    Dim AllowHighQualPrint As Boolean
    Dim AllowDocAssembly As Boolean
    Dim AllowExtracting As Boolean
    
    
    'Aktuelle Einstellungen retten
        
    bResult = GetValue(HKEY_CURRENT_USER, "Software\Adobe\Acrobat\PDFMaker\5.0\Security", "SecurityPassword", value)
    If Asc(value) <> 0 Then
        SecurityPassword = value
    Else
        SecurityPassword = ""
    End If
    
    bResult = GetValue(HKEY_CURRENT_USER, "Software\Adobe\Acrobat\PDFMaker\5.0\OutputOptions", "AutoSaveFile", value)
    AutoSaveFile = ConvertRegBinaryToBool(value)
    bResult = GetValue(HKEY_CURRENT_USER, "Software\Adobe\Acrobat\PDFMaker\5.0\FileOptions", "ViewPDFFile", value)
    OwnerPasswdReq = ConvertRegBinaryToBool(value)
    bResult = GetValue(HKEY_CURRENT_USER, "Software\Adobe\Acrobat\PDFMaker\5.0\Security", "OwnerPasswdReq", value)
    OwnerPasswdReq = ConvertRegBinaryToBool(value)
    bResult = GetValue(HKEY_CURRENT_USER, "Software\Adobe\Acrobat\PDFMaker\5.0\Security", "UserPasswdReq", value)
    UserPasswdReq = ConvertRegBinaryToBool(value)
    bResult = GetValue(HKEY_CURRENT_USER, "Software\Adobe\Acrobat\PDFMaker\5.0\Security", "AllowPrint", value)
    AllowPrint = ConvertRegBinaryToBool(value)
    bResult = GetValue(HKEY_CURRENT_USER, "Software\Adobe\Acrobat\PDFMaker\5.0\Security", "AllowEdit", value)
    AllowEdit = ConvertRegBinaryToBool(value)
    bResult = GetValue(HKEY_CURRENT_USER, "Software\Adobe\Acrobat\PDFMaker\5.0\Security", "AllowNotes", value)
    AllowNotes = ConvertRegBinaryToBool(value)
    bResult = GetValue(HKEY_CURRENT_USER, "Software\Adobe\Acrobat\PDFMaker\5.0\Security", "AllowFillAndSign", value)
    AllowFillAndSign = ConvertRegBinaryToBool(value)
    bResult = GetValue(HKEY_CURRENT_USER, "Software\Adobe\Acrobat\PDFMaker\5.0\Security", "AllowAccessibility", value)
    AllowAccessibility = ConvertRegBinaryToBool(value)
    bResult = GetValue(HKEY_CURRENT_USER, "Software\Adobe\Acrobat\PDFMaker\5.0\Security", "AllowHighQualPrint", value)
    AllowHighQualPrint = ConvertRegBinaryToBool(value)
    bResult = GetValue(HKEY_CURRENT_USER, "Software\Adobe\Acrobat\PDFMaker\5.0\Security", "AllowDocAssembly", value)
    AllowDocAssembly = ConvertRegBinaryToBool(value)
    bResult = GetValue(HKEY_CURRENT_USER, "Software\Adobe\Acrobat\PDFMaker\5.0\Security", "AllowExtracting", value)
    AllowExtracting = ConvertRegBinaryToBool(value)
    
    'Die ben�tigten Einstellungen in der Registry hinterlegen
    bResult = SetValue(HKEY_CURRENT_USER, "Software\Adobe\Acrobat\PDFMaker\5.0\OutputOptions", "AutoSaveFile", True)
    bResult = SetValue(HKEY_CURRENT_USER, "Software\Adobe\Acrobat\PDFMaker\5.0\FileOptions", "ViewPDFFile", False)
    bResult = SetValue(HKEY_CURRENT_USER, "Software\Adobe\Acrobat\PDFMaker\5.0\Security", "OwnerPasswdReq", True)
    bResult = SetValue(HKEY_CURRENT_USER, "Software\Adobe\Acrobat\PDFMaker\5.0\Security", "UserPasswdReq", False)
    bResult = SetValue(HKEY_CURRENT_USER, "Software\Adobe\Acrobat\PDFMaker\5.0\Security", "AllowPrint", True)
    bResult = SetValue(HKEY_CURRENT_USER, "Software\Adobe\Acrobat\PDFMaker\5.0\Security", "AllowEdit", False)
    bResult = SetValue(HKEY_CURRENT_USER, "Software\Adobe\Acrobat\PDFMaker\5.0\Security", "AllowNotes", True)
    bResult = SetValue(HKEY_CURRENT_USER, "Software\Adobe\Acrobat\PDFMaker\5.0\Security", "AllowFillAndSign", True)
    bResult = SetValue(HKEY_CURRENT_USER, "Software\Adobe\Acrobat\PDFMaker\5.0\Security", "AllowAccessibility", False)
    bResult = SetValue(HKEY_CURRENT_USER, "Software\Adobe\Acrobat\PDFMaker\5.0\Security", "AllowHighQualPrint", True)
    bResult = SetValue(HKEY_CURRENT_USER, "Software\Adobe\Acrobat\PDFMaker\5.0\Security", "AllowDocAssembly", True)
    bResult = SetValue(HKEY_CURRENT_USER, "Software\Adobe\Acrobat\PDFMaker\5.0\Security", "AllowExtracting", False)
    bResult = SetValue(HKEY_CURRENT_USER, "Software\Adobe\Acrobat\PDFMaker\5.0\Security", "SecurityPassword", sPasswd)
    
    oApp.Visible = False

    'Zu konvertierendes Dokument �ffnen
    
    If sApplication = WORD_APP Then
      oApp.application.Documents.Open FileName:=sDocFileName
      Set aDoc = oApp.application.ActiveDocument
      
       'Dokumentschutz aufheben
      If aDoc.ProtectionType <> wdNoProtection Then
          aDoc.Unprotect (sPasswd)
      End If
    
      'Dokument speichern, da sonst PDFmaker mit ner bl�den meldung aufpoppt.
      aDoc.SaveAs sTempFilename
       
      'Dokument in PDF konvertieren
      oApp.application.Run "ConvertToPDF"
    
      'Dokument schliessen
      aDoc.Close
      
    Else
      oApp.application.Workbooks.Open FileName:=sDocFileName
      Set aWorkbook = oApp.application.ActiveWorkbook
      
       'Dokumentschutz aufheben
      If aWorkbook.HasPassword Then
          aWorkbook.Unprotect (sPasswd)
      End If
    
      'Dokument speichern
      aWorkbook.SaveAs sTempFilename
      
      'Dokument in PDF konvertieren
      oApp.application.Run "ConvertToPDF"
    
      'Dokument schliessen
      aWorkbook.Close
      
    End If

    'Einstellungen wiederherstellen
    bResult = SetValue(HKEY_CURRENT_USER, "Software\Adobe\Acrobat\PDFMaker\5.0\OutputOptions", "AutoSaveFile", AutoSaveFile)
    bResult = SetValue(HKEY_CURRENT_USER, "Software\Adobe\Acrobat\PDFMaker\5.0\FileOptions", "ViewPDFFile", ViewPDFFile)
    bResult = SetValue(HKEY_CURRENT_USER, "Software\Adobe\Acrobat\PDFMaker\5.0\Security", "OwnerPasswdReq", OwnerPasswdReq)
    bResult = SetValue(HKEY_CURRENT_USER, "Software\Adobe\Acrobat\PDFMaker\5.0\Security", "UserPasswdReq", UserPasswdReq)
    bResult = SetValue(HKEY_CURRENT_USER, "Software\Adobe\Acrobat\PDFMaker\5.0\Security", "SecurityPassword", SecurityPassword)
    bResult = SetValue(HKEY_CURRENT_USER, "Software\Adobe\Acrobat\PDFMaker\5.0\Security", "AllowPrint", AllowPrint)
    bResult = SetValue(HKEY_CURRENT_USER, "Software\Adobe\Acrobat\PDFMaker\5.0\Security", "AllowEdit", AllowEdit)
    bResult = SetValue(HKEY_CURRENT_USER, "Software\Adobe\Acrobat\PDFMaker\5.0\Security", "AllowNotes", AllowNotes)
    bResult = SetValue(HKEY_CURRENT_USER, "Software\Adobe\Acrobat\PDFMaker\5.0\Security", "AllowFillAndSign", AllowFillAndSign)
    bResult = SetValue(HKEY_CURRENT_USER, "Software\Adobe\Acrobat\PDFMaker\5.0\Security", "AllowAccessibility", AllowAccessibility)
    bResult = SetValue(HKEY_CURRENT_USER, "Software\Adobe\Acrobat\PDFMaker\5.0\Security", "AllowHighQualPrint", AllowHighQualPrint)
    bResult = SetValue(HKEY_CURRENT_USER, "Software\Adobe\Acrobat\PDFMaker\5.0\Security", "AllowDocAssembly", AllowDocAssembly)
    bResult = SetValue(HKEY_CURRENT_USER, "Software\Adobe\Acrobat\PDFMaker\5.0\Security", "AllowExtracting", AllowExtracting)

End Sub

Function InvokeConvertToPDF8(sApplication$, sDocFileName$, sPasswd$, sTempFilename$, sPDF$, sUseProtection$) As Integer
    
    Dim bResult              As Boolean
    Dim value                As Variant
    Dim oApp                 As Object
    Dim aDoc                 As Document
    Dim aWorkbook            As Excel.Workbook
    Dim logSercuritySettings As String
    
    Dim PDFMakerApp          As PDFMakerApp
    Dim ConversionSettings   As ConversionSettings
    Dim SecuritySettings     As SecuritySettings
    Dim SettingsBitfield     As Long
    Dim SettingsFile         As String
    
    Set ConversionSettings = New ConversionSettings
    Set PDFMakerApp = New PDFMakerApp


'    'Get and Check filename
'    sPDF = OpenDialog("Save PDF...", "*.pdf", "pdf")
'    If Dir$(sPDF, vbHidden) <> "" Then
'        If MsgBox("File exists. Overwrite?", vbYesNo, "File exists!") = vbNo Then
'            InvokeConvertToPDF8 = kPDFMCancelled
'            Exit Function
'        End If
'    End If

    'Word action **************************************************************************

    'Word �bernehmen bzw starten
    Set oApp = StartApplication(sApplication)

    'Zu konvertierendes Dokument �ffnen
    If sApplication = WORD_APP Then
        oApp.application.Documents.Open FileName:=sDocFileName
        Set aDoc = oApp.application.ActiveDocument
        
         'Dokumentschutz aufheben
        If aDoc.ProtectionType <> wdNoProtection Then
            aDoc.Unprotect (sPasswd)
        End If
    
        'Dokument speichern
        aDoc.SaveAs sTempFilename
    
        'Dokument schliessen
        aDoc.Close
        
        'Defaultsettings holen
        ConversionSettings.GetAppConversionDefaults kAppWord, SettingsBitfield, SettingsFile
        
    Else
        oApp.application.Workbooks.Open FileName:=sDocFileName
        Set aWorkbook = oApp.application.ActiveWorkbook
        
         'Dokumentschutz aufheben
        If aWorkbook.HasPassword Then
            aWorkbook.Unprotect (sPasswd)
        End If
    
        'Dokument speichern
        aWorkbook.SaveAs sTempFilename
    
        'Dokument schliessen
        aWorkbook.Close
        
        'Defaultsettings holen
        ConversionSettings.GetAppConversionDefaults kAppExcel, SettingsBitfield, SettingsFile
        
    End If
    
   
    QuitApplication oApp

    'Dokument in PDF konvertieren *********************************************************
 
    'Defaultsettings festlegen
    ConversionSettings.SetConversionParameters SettingsBitfield
    
    'Sicherheitsrichtlinien festlegen
    Set SecuritySettings = ConversionSettings.GetSecuritySettings
    If sUseProtection = "true" Then
        SecuritySettings.AllowedChanges = kAllowChangesCommentingFormfillsSign
        SecuritySettings.PrintingModeAllowed = kPrintingAllowedHighRes
        SecuritySettings.PermsPasswd = sPasswd
        SecuritySettings.PermsPasswdNeeded = True
        ConversionSettings.SetSecuritySettings SecuritySettings
    End If

    'log the actual security settings
    logSercuritySettings = SecuritySettings.AllowedChanges
    logSercuritySettings = logSercuritySettings & "|" & SecuritySettings.AllowedPermissionsBits
    logSercuritySettings = logSercuritySettings & "|" & SecuritySettings.AttachmentPasswd
    logSercuritySettings = logSercuritySettings & "|" & SecuritySettings.AttachmentPasswdNeeded
    logSercuritySettings = logSercuritySettings & "|" & SecuritySettings.EnableCopyingContent
    logSercuritySettings = logSercuritySettings & "|" & SecuritySettings.EnablePlaintextMetadata
    logSercuritySettings = logSercuritySettings & "|" & SecuritySettings.EnableTextAccessibility
    logSercuritySettings = logSercuritySettings & "|" & SecuritySettings.EncryptAttachmentsOnly
    logSercuritySettings = logSercuritySettings & "|" & SecuritySettings.KeyLen
    logSercuritySettings = logSercuritySettings & "|" & SecuritySettings.OpenDocPasswd
    logSercuritySettings = logSercuritySettings & "|" & SecuritySettings.OpenDocPasswdNeeded
    logSercuritySettings = logSercuritySettings & "|" & SecuritySettings.PermsPasswd
    logSercuritySettings = logSercuritySettings & "|" & SecuritySettings.PermsPasswdNeeded
    logSercuritySettings = logSercuritySettings & "|" & SecuritySettings.PrintingModeAllowed


    If DEBUG_TRACE Then
        MsgBox "The PDF filename to be created is: " & sPDF, , "DEBUG PDF Modul"
        MsgBox "PDF Security Settings are:" & vbCrLf & logSercuritySettings, , "DEBUG PDF Modul"
    End If
        

    If sPDF <> "" Then
        InvokeConvertToPDF8 = PDFMakerApp.CreatePDF(sTempFilename, sPDF, ConversionSettings)
    Else
        InvokeConvertToPDF8 = kPDFMCancelled
    End If
    
    'Dokument in PDF konvertieren Ende ****************************************************
    
    If DEBUG_TRACE Then
        MsgBox "PDFMaker returns :" & InvokeConvertToPDF8, , "DEBUG PDF Modul"
    End If
        
    '...tempor�res Dokument l�schen
    Kill sTempFilename

End Function

Function ConvertRegBinaryToBool(value As Variant) As Boolean

    If Asc(value) = 1 Then
        ConvertRegBinaryToBool = True
    Else
        ConvertRegBinaryToBool = False
    End If

End Function

Public Function OpenDialog(Optional Titel, Optional Filter, Optional DefExtension, Optional AktDir) As String
    
    Dim strDateiname As String
    Dim strDlgTitel As String
    Dim strFilter As String
    Dim strDefExtension As String
    Dim strAktDir As String
    Dim strNull As String
    Dim OpenDlg As OPENFILENAME
    
    strNull = Chr$(0)
    strDateiname = String$(512, 0)
    
    If IsMissing(Titel) Then
        strDlgTitel = "Datei �ffnen" & strNull
    Else
        strDlgTitel = Titel & strNull
    End If
    
    If IsMissing(Filter) Then
        strFilter = "Alle Dateien" & strNull & "*.*" & strNull & strNull
    Else
        strFilter = Filter & strNull
    End If
    
    If IsMissing(DefExtension) Then
        strDefExtension = strNull
    Else
        strDefExtension = DefExtension & strNull
    End If
    
    If IsMissing(AktDir) Then
        strAktDir = CurDir$ & strNull
    Else
        strAktDir = AktDir & strNull
    End If
    
    With OpenDlg
        .lStructSize = Len(OpenDlg)
'        .hwndOwner = Screen.ActiveForm.hwnd
        .lpstrFilter = strFilter
        .nFilterIndex = 1
        .lpstrFile = strDateiname
        .nMaxFile = Len(strDateiname)
        .lpstrInitialDir = strAktDir
        .lpstrTitle = strDlgTitel
'        .Flags = OFN_FILEMUSTEXIST Or OFN_READONLY
        .lpstrDefExt = strDefExtension
        If GetOpenFileName(OpenDlg) <> 0 Then
            OpenDialog = Left$(.lpstrFile, InStr(.lpstrFile, strNull) - 1)
        Else
            OpenDialog = ""
        End If
    End With

End Function

