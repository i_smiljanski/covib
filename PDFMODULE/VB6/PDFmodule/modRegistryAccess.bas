Attribute VB_Name = "modRegistryAccess"
Option Explicit

'''''''''''''''''''''''''''''''''''
' Constants
'''''''''''''''''''''''''''''''''''
Public Const HKEY_CURRENT_USER = &H80000001
Public Const HKEY_LOCAL_MACHINE = &H80000002

Private Const KEY_QUERY_VALUE = &H1
Private Const KEY_SET_VALUE = &H2
Private Const KEY_CREATE_SUB_KEY = &H4
Private Const KEY_ENUMERATE_SUB_KEYS = &H8
Private Const KEY_NOTIFY = &H10
Private Const KEY_CREATE_LINK = &H20
Private Const KEY_READ = KEY_QUERY_VALUE Or KEY_ENUMERATE_SUB_KEYS Or KEY_NOTIFY
Private Const KEY_WRITE = KEY_SET_VALUE Or KEY_CREATE_SUB_KEY
Private Const KEY_EXECUTE = KEY_READ
Private Const KEY_ALL_ACCESS = KEY_QUERY_VALUE Or KEY_SET_VALUE Or KEY_CREATE_SUB_KEY Or KEY_ENUMERATE_SUB_KEYS Or KEY_NOTIFY Or KEY_CREATE_LINK
Private Const ERROR_SUCCESS = 0&

Private Const REG_NONE = 0      ' No value type
Private Const REG_SZ = 1        ' Unicode nul terminated string
Private Const REG_EXPAND_SZ = 2 ' Unicode nul terminated string (with environment variable references)
Private Const REG_BINARY = 3    ' Free form binary
Private Const REG_DWORD = 4     ' 32-bit number
Private Const REG_DWORD_LITTLE_ENDIAN = 4 ' 32-bit number (same as REG_DWORD)
Private Const REG_DWORD_BIG_ENDIAN = 5    ' 32-bit number
Private Const REG_LINK = 6                ' Symbolic Link (unicode)
Private Const REG_MULTI_SZ = 7            ' Multiple Unicode strings

Private Const REG_OPTION_NON_VOLATILE = &H0
Private Const REG_CREATED_NEW_KEY = &H1

Private Const FORMAT_MESSAGE_ALLOCATE_BUFFER As Long = &H100
Private Const FORMAT_MESSAGE_ARGUMENT_ARRAY  As Long = &H2000
Private Const FORMAT_MESSAGE_FROM_HMODULE  As Long = &H800
Private Const FORMAT_MESSAGE_FROM_STRING  As Long = &H400
Private Const FORMAT_MESSAGE_FROM_SYSTEM  As Long = &H1000
Private Const FORMAT_MESSAGE_MAX_WIDTH_MASK  As Long = &HFF
Private Const FORMAT_MESSAGE_IGNORE_INSERTS  As Long = &H200
Private Const FORMAT_MESSAGE_TEXT_LEN  As Long = &HA0 ' from VC++ ERRORS.H file

'''''''''''''''''''''''''''''''''''
' Windows API Declare
'''''''''''''''''''''''''''''''''''
Private Declare Function RegOpenKeyEx Lib "advapi32.dll" Alias "RegOpenKeyExA" (ByVal hKey As Long, ByVal lpSubKey As String, ByVal ulOptions As Long, ByVal samDesired As Long, phkResult As Long) As Long
Private Declare Function RegCloseKey Lib "advapi32.dll" (ByVal hKey As Long) As Long
Private Declare Function RegQueryValueEx Lib "advapi32.dll" Alias "RegQueryValueExA" (ByVal hKey As Long, ByVal lpValueName As String, ByVal lpReserved As Long, lpType As Long, lpData As Any, lpcbData As Any) As Long
Private Declare Function RegCreateKeyEx Lib "advapi32.dll" Alias "RegCreateKeyExA" (ByVal hKey As Long, ByVal lpSubKey As String, ByVal Reserved As Long, ByVal lpClass As String, ByVal dwOptions As Long, ByVal samDesired As Long, ByVal lpSecurityAttributes As Any, phkResult As Long, lpdwDisposition As Long) As Long
Private Declare Function RegFlushKey Lib "advapi32.dll" (ByVal hKey As Long) As Long
Private Declare Function RegSetValueEx_String Lib "advapi32.dll" Alias "RegSetValueExA" (ByVal hKey As Long, ByVal lpValueName As String, ByVal Reserved As Long, ByVal dwType As Long, ByVal lpData As String, ByVal cbData As Long) As Long
Private Declare Function RegSetValueEx_DWord Lib "advapi32.dll" Alias "RegSetValueExA" (ByVal hKey As Long, ByVal lpValueName As String, ByVal Reserved As Long, ByVal dwType As Long, lpData As Long, ByVal cbData As Long) As Long
Private Declare Function RegDeleteKey Lib "advapi32.dll" Alias "RegDeleteKeyA" (ByVal hKey As Long, ByVal lpSubKey As String) As Long
Private Declare Function RegDeleteValue Lib "advapi32.dll" Alias "RegDeleteValueA" (ByVal hKey As Long, ByVal lpValueName As String) As Long
Private Declare Function FormatMessage Lib "kernel32" Alias "FormatMessageA" (ByVal dwFlags As Long, ByVal lpSource As Any, ByVal dwMessageId As Long, ByVal dwLanguageId As Long, ByVal lpBuffer As String, ByVal nSize As Long, ByRef Arguments As Long) As Long


' Pr�ft auf das Vorhandensein eines Schl�ssels.
' Diese Funktion sollten Sie aufrufen bevor Sie einen neuen Eintrag hinzuf�gen

Public Function ExistKey(root&, schl�ssel$) As Boolean
    On Error GoTo ErrorHandler

    ' Root ist entweder HKEY_CURRENT_USER oder HKEY_LOCAL_MACHINE
    Dim lResult&, keyhandle&

    lResult = RegOpenKeyEx(root, schl�ssel, 0, KEY_READ, keyhandle)
    If lResult = ERROR_SUCCESS Then RegCloseKey keyhandle
    ExistKey = (lResult = ERROR_SUCCESS)
    
    If Not ExistKey And DEBUG_TRACE Then
        Dim hk As String
        If root = HKEY_LOCAL_MACHINE Then
            hk = "HKEY_LOCAL_MACHINE\"
        Else
            hk = "HKEY_CURRENT_USER\"
        End If
    
        MsgBox "DEBUG (ExistKey) for " & hk & schl�ssel & ": " & GetSystemErrorMessageText(lResult), , "DEBUG PDF Modul"
    End If
    
    Exit Function
ErrorHandler:

    MsgBoxCheck "ExistKey: Searching for Registry Key " & schl�ssel & " not successfully."

    ExistKey = False
    
    
End Function

' Liefert den Wert eines Eintrags, der durch Root, Schl�ssel und Feld spezifiziert wird
Public Function GetValue(root&, key$, field$, value As Variant) As Boolean
    Dim lResult&, keyhandle&, dwType&
    Dim zw&, buffersize&, buffer$
    
    On Error GoTo ErrorHandler

    lResult = RegOpenKeyEx(root, key, 0, KEY_READ, keyhandle)
    GetValue = (lResult = ERROR_SUCCESS)
    If lResult <> ERROR_SUCCESS Then Exit Function ' Schl�ssel existiert nicht
    lResult = RegQueryValueEx(keyhandle, field, 0&, dwType, ByVal 0&, buffersize)
    GetValue = (lResult = ERROR_SUCCESS)
    If lResult <> ERROR_SUCCESS Then Exit Function ' Feld existiert nicht
    Select Case dwType
        Case REG_SZ       ' nullterminierter String
            buffer = Space$(buffersize + 1)
            lResult = RegQueryValueEx(keyhandle, field, 0&, dwType, ByVal buffer, buffersize)
            GetValue = (lResult = ERROR_SUCCESS)
            If lResult <> ERROR_SUCCESS Then Exit Function ' Fehler beim auslesen des Feldes
            value = buffer
        
        Case REG_DWORD     ' 32-Bit Number   !!!! Word
            buffersize = 4      ' = 32 Bit
            lResult = RegQueryValueEx(keyhandle, field, 0&, dwType, zw, buffersize)
            GetValue = (lResult = ERROR_SUCCESS)
            If lResult <> ERROR_SUCCESS Then Exit Function ' Fehler beim auslesen des Feldes
            value = zw
        
        Case REG_BINARY
            buffer = String(buffersize + 1, 0)
            lResult = RegQueryValueEx(keyhandle, field, 0&, dwType, ByVal buffer, buffersize)
            GetValue = (lResult = ERROR_SUCCESS)
            If lResult <> ERROR_SUCCESS Then Exit Function ' Fehler beim auslesen des Feldes
            value = Left$(buffer, buffersize)
            
        ' Hier k�nnten auch die weiteren Datentypen behandelt werden, soweit dies sinnvoll ist
    End Select
    If lResult = ERROR_SUCCESS Then RegCloseKey keyhandle
    GetValue = True
    
    If Not GetValue And DEBUG_TRACE Then
        MsgBox "DEBUG(GetValue): " & GetSystemErrorMessageText(lResult), , "DEBUG PDF Modul"
    End If
    
    Exit Function
ErrorHandler:

    MsgBoxCheck "GetValue: Getting Registry Value " & field & " not successfully."

    GetValue = False
    
End Function

Public Function CreateKey(root&, newkey$, Class$) As Boolean
    Dim lResult&, keyhandle&
    Dim Action&
    
    On Error GoTo ErrorHandler

    lResult = RegCreateKeyEx(root, newkey, 0, Class, REG_OPTION_NON_VOLATILE, KEY_ALL_ACCESS, 0&, keyhandle, Action)
    If lResult = ERROR_SUCCESS Then
        If RegFlushKey(keyhandle) = ERROR_SUCCESS Then RegCloseKey keyhandle
    Else
      CreateKey = False
       If DEBUG_TRACE Then
        MsgBox "DEBUG(CreateKey): " & GetSystemErrorMessageText(lResult), , "DEBUG PDF Modul"
    End If
      
      Exit Function
    End If
    CreateKey = (Action = REG_CREATED_NEW_KEY)
    
    Exit Function
ErrorHandler:

    MsgBoxCheck "CreateKey: Creating Registry Value " & newkey$ & " not successfully."

    CreateKey = False
    
End Function

Public Function SetValue(root&, key$, field$, value As Variant) As Boolean
    Dim lResult&, keyhandle&
    Dim s$, l&
    
    On Error GoTo ErrorHandler

    lResult = RegOpenKeyEx(root, key, 0, KEY_ALL_ACCESS, keyhandle)
    If lResult <> ERROR_SUCCESS Then
        SetValue = False
        Exit Function
    End If
    Select Case VarType(value)
        Case vbInteger, vbLong
            l = CLng(value)
            lResult = RegSetValueEx_DWord(keyhandle, field, 0, REG_DWORD, l, 4)
        Case vbString
            s = CStr(value)
            lResult = RegSetValueEx_String(keyhandle, field, 0, REG_SZ, s, Len(s) + 1)    ' +1 f�r die Null am Ende
        Case vbBoolean
            If value = True Then
                lResult = RegSetValueEx_String(keyhandle, field, 0, REG_BINARY, Chr$(1), 1)
            Else
                lResult = RegSetValueEx_String(keyhandle, field, 0, REG_BINARY, Chr$(0), 1)
            End If
            s = CStr(value)

        ' Hier k�nnen noch weitere Datentypen umgewandelt bzw. gespeichert werden
    End Select
    RegCloseKey keyhandle
    SetValue = (lResult = ERROR_SUCCESS)
    
    If Not SetValue And DEBUG_TRACE Then
        MsgBox "DEBUG(SetValue): " & GetSystemErrorMessageText(lResult), , "DEBUG PDF Modul"
    End If
    
    Exit Function
ErrorHandler:

    MsgBoxCheck "SetValue: Setting Registry Value " & key & "/" & field & " not successfully."

    SetValue = False
    
End Function

Public Function DeleteKey(root&, key$) As Boolean
    Dim lResult&
    
    On Error GoTo ErrorHandler
    
    lResult = RegDeleteKey(root, key)
    DeleteKey = (lResult = ERROR_SUCCESS)
    
    If Not DeleteKey And DEBUG_TRACE Then
        MsgBox "DEBUG(DeleteKey): " & GetSystemErrorMessageText(lResult), , "DEBUG PDF Modul"
    End If
    
    Exit Function
ErrorHandler:

    MsgBoxCheck "DeleteKey: Deleting Registry Key " & key & " not successfully."

    DeleteKey = False
End Function

Public Function DeleteValue(root&, key$, field$) As Boolean
    Dim lResult&, keyhandle&
        
    On Error GoTo ErrorHandler

    lResult = RegOpenKeyEx(root, key, 0, KEY_ALL_ACCESS, keyhandle)
    If lResult <> ERROR_SUCCESS Then
        DeleteValue = False
        Exit Function
    End If
    lResult = RegDeleteValue(keyhandle, field)
    DeleteValue = (lResult = ERROR_SUCCESS)
    RegCloseKey keyhandle
    
    If Not DeleteValue And DEBUG_TRACE Then
        MsgBox "DEBUG(DeleteValue): " & GetSystemErrorMessageText(lResult), , "DEBUG PDF Modul"
    End If
    
    Exit Function
ErrorHandler:

    MsgBoxCheck "DeleteValue: Deleting Registry Value " & key & "/" & field & " not successfully."

    DeleteValue = False
    
End Function


''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
' GetSystemErrorMessageText
'
' This function gets the system error message text that corresponds
' to the error code parameter ErrorNumber. This value is the value returned
' by Err.LastDLLError or by GetLastError, or occasionally as the returned
' result of a Windows API function.
'
' These are NOT the error numbers returned by Err.Number (for these
' errors, use Err.Description to get the description of the error).
'
' In general, you should use Err.LastDllError rather than GetLastError
' because under some circumstances the value of GetLastError will be
' reset to 0 before the value is returned to VBA. Err.LastDllError will
' always reliably return the last error number raised in an API function.
'
' The function returns vbNullString is an error occurred or if there is
' no error text for the specified error number.
'''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
Public Function GetSystemErrorMessageText(ErrorNumber As Long) As String
    
    
    Dim ErrorText As String
    Dim TextLen As Long
    Dim FormatMessageResult As Long
    Dim LangID As Long
    
    ''''''''''''''''''''''''''''''''
    ' Initialize the variables
    ''''''''''''''''''''''''''''''''
    LangID = 0&   ' Default language
    ErrorText = String$(FORMAT_MESSAGE_TEXT_LEN, vbNullChar)
    TextLen = FORMAT_MESSAGE_TEXT_LEN
    
    ''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
    ' Call FormatMessage to get the text of the error message text
    ' associated with ErrorNumber.
    ''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
    FormatMessageResult = FormatMessage( _
                            dwFlags:=FORMAT_MESSAGE_FROM_SYSTEM Or _
                                     FORMAT_MESSAGE_IGNORE_INSERTS, _
                            lpSource:=0&, _
                            dwMessageId:=ErrorNumber, _
                            dwLanguageId:=LangID, _
                            lpBuffer:=ErrorText, _
                            nSize:=TextLen, _
                            Arguments:=0&)
    
    If FormatMessageResult = 0& Then
        ''''''''''''''''''''''''''''''''''''''''''''''''''
        ' An error occured. Display the error number, but
        ' don't call GetSystemErrorMessageText to get the
        ' text, which would likely cause the error again,
        ' getting us into a loop.
        ''''''''''''''''''''''''''''''''''''''''''''''''''
        MsgBoxCheck "GetSystemErrorMessageText: " & _
                    "An error occurred with the FormatMessage" & _
                    " API function call." & vbCrLf & _
                    "Error: " & CStr(Err.LastDllError) & _
                    " Hex(" & Hex(Err.LastDllError) & ")."
        GetSystemErrorMessageText = "An internal system error occurred with the" & vbCrLf & _
            "FormatMessage API function: " & CStr(Err.LastDllError) & ". No futher information" & vbCrLf & _
            "is available."
        Exit Function
    End If
    ''''''''''''''''''''''''''''''''''''''''''''''''''''''
    ' If FormatMessageResult is not zero, it is the number
    ' of characters placed in the ErrorText variable.
    ' Take the left FormatMessageResult characters and
    ' return that text.
    ''''''''''''''''''''''''''''''''''''''''''''''''''''''
    ErrorText = Left$(ErrorText, FormatMessageResult)
    '''''''''''''''''''''''''''''''''''''''''''''
    ' Get rid of the trailing vbCrLf, if present.
    '''''''''''''''''''''''''''''''''''''''''''''
    If Len(ErrorText) >= 2 Then
        If Right$(ErrorText, 2) = vbCrLf Then
            ErrorText = Left$(ErrorText, Len(ErrorText) - 2)
        End If
    End If
    
    ''''''''''''''''''''''''''''''''
    ' Return the error text as the
    ' result.
    ''''''''''''''''''''''''''''''''
    GetSystemErrorMessageText = ErrorText

End Function


