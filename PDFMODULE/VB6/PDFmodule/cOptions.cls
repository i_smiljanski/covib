VERSION 1.0 CLASS
BEGIN
  MultiUse = -1  'True
  Persistable = 0  'NotPersistable
  DataBindingBehavior = 0  'vbNone
  DataSourceBehavior  = 0  'vbNone
  MTSTransactionMode  = 0  'NotAnMTSObject
END
Attribute VB_Name = "cOptions"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = True
Attribute VB_PredeclaredId = False
Attribute VB_Exposed = False
Attribute VB_Ext_KEY = "SavedWithClassBuilder6" ,"Yes"
Attribute VB_Ext_KEY = "Top_Level" ,"Yes"
'''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
' Klasse            : cOptions
' Beschreibung      : Parser die Kommandozeile; Stellt die Optionen zur Verf�gung
' Autor             : ms
' Datum             : 19.03.2004
' �nderungshistorie :
' Autor  Datum       Grund            Kommentar
' -----  ----------  ---------------  ---------------------------------------
'
'''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
Option Explicit

Private mvarsDocument           As String
Private mvarsTempDoc            As String
Private mvarsPassword           As String
Private mvarsPDFtarget          As String
Private mvarsPDFtargetPath      As String
Private mvarsUseProtection      As String
Private mvarsApplication        As String

Private oConfigDOM              As Object
Private sConfigPath             As String

Private iMSXMLver               As Integer

Public Property Get sPassword() As String
    sPassword = mvarsPassword
End Property

Public Property Get sTempDoc() As String
    sTempDoc = mvarsTempDoc
End Property

Public Property Get sDocument() As String
    sDocument = mvarsDocument
End Property

Public Property Get sPDFtarget() As String
    sPDFtarget = mvarsPDFtarget
End Property

Public Property Get sPDFtargetPath() As String
    sPDFtargetPath = mvarsPDFtargetPath
End Property

Public Property Get sUseProtection() As String
    sUseProtection = mvarsUseProtection
End Property

Public Property Get sApplication() As String
    If UCase(Right(sDocument, 3)) = "DOC" Then
        sApplication = WORD_APP
    Else
        sApplication = EXCEL_APP
    End If
End Property


'''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
' Funktion            : ParseArgs
' Beschreibung        : Ruft den Parser auf und verteilt die Befehlszeilenargumente
'                       in die entsprechenden Properties.
'                       Startet die Interpretation der XML-Config.
' Parameter           : sArgs (String) - die Befehlszeilenargumente
' wird aufgerufen von : modMain.Main
' R�ckgabewert        : true - Befehlszeile konnte korrekt interpretiert werden
'                       false - Beim Interpretieren der Befehlszeile ist eine Unstimmigkeit aufgetreten
' �nderungshistorie   :
' Autor  Datum       Grund            Kommentar
' -----  ----------  ---------------  ---------------------------------------
'
'''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
Public Function ParseArgs(sArgs As String) As Boolean
    
    Dim aArgs() As String
    Dim iNumArgs    As Integer
    Dim dirAttr As VbFileAttribute
   
    If sArgs = "" Then
        'Offenbar keine Parameter angegeben.
        ParseArgs = False
        Exit Function
    End If
   
    'Options in ein Feld transferieren
    aArgs = SplitArgs(sArgs)
       
    'ConfigDOM in einer eigenen Subroutine initialisieren -> zum Abfangen bei Nichtexistenz
    SetConfigDOM
        
    oConfigDOM.setProperty "SelectionLanguage", "XPath"
     
    iNumArgs = UBound(aArgs) - LBound(aArgs)
            
    sConfigPath = ExtractPath(aArgs(0))
            
    'sArgs enth�lt den Pfad+Configfile
    If oConfigDOM.Load(aArgs(0)) = False Then
        MsgBoxCheck "ParseArgs: Failed to load xml config file."
        ParseArgs = False
        Exit Function
    End If
     
    If ReadArgs() = False Then
        MsgBoxCheck "ParseArgs: Failed to load xml config data from file."
        ParseArgs = False
        Exit Function
    End If
    
    If Not DirExists(mvarsPDFtargetPath) Then
        MsgBoxCheck "ParseArgs: Target Directory '" & mvarsPDFtargetPath & "' does not exist."
        ParseArgs = False
        Exit Function
    End If
       
    dirAttr = FileSystem.GetAttr(mvarsPDFtargetPath)
    If DEBUG_TRACE Then
        MsgBox "Attributes of path '" & mvarsPDFtargetPath & "' are " & dirAttr, , "DEBUG PDF Modul"
    End If
     
    ParseArgs = True
   
End Function

'''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
' Funktion            : SplitArgs
' Beschreibung        : Verteilt die Befehlszeilenargumente in ein Array.
'                       Filenamen in QuotMarks werden korrekt ber�cksichtigt.
' Parameter           : sArgs (String) - die Befehlszeilenargumente
' wird aufgerufen von : .ParseArgs
' R�ckgabewert        : String() - enth�lt die aufgesplittete Befehlszeile
' �nderungshistorie   :
' Autor  Datum       Grund            Kommentar
' -----  ----------  ---------------  ---------------------------------------
'
'''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
Private Function SplitArgs(sArgs As String) As String()

    Dim aTempArgs()     As String
    Dim aArgs()         As String
    Dim iTempNumArgs    As Integer
    Dim iNumArgs        As Integer
    Dim bIsFilename     As Boolean
    Dim I               As Integer
            
    bIsFilename = False

    'Argumentstring in ein Array einzelner Worte aufsplitten
    aTempArgs = Split(sArgs, " ", -1)
    
    'Anzahl der Elemente im Array bestimmen (funktioniert nur bei eindimensionalen Arrays!)
    iTempNumArgs = UBound(aTempArgs) - LBound(aTempArgs)

    If iTempNumArgs = -1 Then Exit Function

    ReDim aArgs(iTempNumArgs)

    'Um Filenamen, die in Quotmarks stehen, verarbeiten zu k�nnen, das gesplittete Feld in einem
    'neuen Feld entsprechend wieder zusammenbauen.
    For I = 0 To iTempNumArgs
        If Trim(aTempArgs(I)) = "" Then
            'Leerzeichen -> ignore
        ElseIf Right(aTempArgs(I), 1) = Chr(34) Then
            bIsFilename = False
            aArgs(iNumArgs) = aArgs(iNumArgs) & aTempArgs(I)
            iNumArgs = iNumArgs + 1
        ElseIf Left(aTempArgs(I), 1) = Chr(34) Or bIsFilename = True Then
            bIsFilename = True
            aArgs(iNumArgs) = aArgs(iNumArgs) & aTempArgs(I) & " "
        ElseIf bIsFilename = False Then
            aArgs(iNumArgs) = aTempArgs(I)
            iNumArgs = iNumArgs + 1
        End If
    Next I
    
    ' Gr��e des Datenfeldes neu festlegen
    ReDim Preserve aArgs(iNumArgs - 1)

    SplitArgs = aArgs

End Function

Private Sub SetConfigDOM()

    On Error Resume Next
    
    Set oConfigDOM = CreateObject("MSXML2.DOMDocument.4.0")
    If oConfigDOM Is Nothing Then
        Set oConfigDOM = CreateObject("MSXML2.DOMDocument.3.0")
        If oConfigDOM Is Nothing Then
            Set oConfigDOM = CreateObject("MSXML2.DOMDocument.2.6")
            If oConfigDOM Is Nothing Then
                Set oConfigDOM = CreateObject("MSXML.DOMDocument")
                If oConfigDOM Is Nothing Then
                    ExitProc (-1)
                Else
                    ExitProc (-2)
                End If
            Else
                iMSXMLver = 26
            End If
        Else
            iMSXMLver = 30
        End If
    Else
        iMSXMLver = 40
    End If

End Sub

Private Function CheckPath(sFile) As String

    Dim sTemp As String

    If Not InStr(sFile, "\") > 0 Then
        CheckPath = sConfigPath & "\" & sFile
    Else
        CheckPath = sFile
    End If

End Function

Private Function ReadArgs() As Boolean

    Dim oRoot       As Object
    Dim oChilds     As Object
    Dim oChild      As Object
    Dim oNode       As Object
    Dim oProps      As Object
    Dim sCommand    As String
            
    Dim sFullpathTarget As String
        
    ReadArgs = True
    
    Set oRoot = oConfigDOM.documentElement

    Set oChilds = oRoot.ChildNodes
    Set oChild = oChilds.Item(0) 'Item z�hlt ab 0

    'Wenn oChild nothing ist, ist was faul
    If oChild Is Nothing Then
        ReadArgs = False
        Exit Function
    End If

    Set oChild = oChild.SelectSingleNode("app[@name='pdfmodul']")

    If Not oChild.SelectSingleNode("../fullpathname") Is Nothing Then
        sFullpathTarget = oChild.SelectSingleNode("../fullpathname").Text
        mvarsPDFtargetPath = sFullpathTarget
    End If

    If Not oChild.SelectSingleNode("../target") Is Nothing Then
        If Right(sFullpathTarget, 1) = "\" Then
            sFullpathTarget = sFullpathTarget & oChild.SelectSingleNode("../target").Text
        Else
            If sFullpathTarget = "" Then
                sFullpathTarget = App.Path & "\" & oChild.SelectSingleNode("../target").Text
            Else
                sFullpathTarget = sFullpathTarget & "\" & oChild.SelectSingleNode("../target").Text
            End If
        End If
    End If

    mvarsPDFtarget = sFullpathTarget

    If Not oChild.SelectSingleNode("source") Is Nothing Then
        mvarsDocument = sConfigPath & "\" & oChild.SelectSingleNode("source").Text
    End If
    If Not oChild.SelectSingleNode("tempfile") Is Nothing Then
        mvarsTempDoc = sConfigPath & "\" & oChild.SelectSingleNode("tempfile").Text
    End If
    If Not oChild.SelectSingleNode("sourcepassword") Is Nothing Then
        mvarsPassword = oChild.SelectSingleNode("sourcepassword").Text
    End If

    If Not oChild.SelectSingleNode("use-protection") Is Nothing Then
        If Left(oChild.SelectSingleNode("use-protection").Text, 1) = "T" Then
            mvarsUseProtection = "true"
        Else
            mvarsUseProtection = "false"
        End If
    Else
        mvarsUseProtection = "false"
    End If

    'MD5 "Verschl�sselung"
    mvarsPassword = Left(goMD5.MD5(mvarsPassword), 8)
    
End Function

' Return True if a directory exists
' (the directory name can also include a trailing backslash)
Private Function DirExists(DirName As String) As Boolean
    On Error GoTo ErrorHandler
    ' test the directory attribute
    DirExists = GetAttr(DirName) And vbDirectory
    
ErrorHandler:
    ' if an error occurs, this function returns False
End Function

