Attribute VB_Name = "modMain"
'''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
' Modul             : modMain
' Beschreibung      : Hier startet das Programm.
' Autor             : ms
' Datum             : 19.03.2004 ff.
' �nderungshistorie :
' Autor  Datum       Grund            Kommentar
' -----  ----------  ---------------  ---------------------------------------
'
'''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
Option Explicit

Global bDEBUG As Boolean
Global goMD5 As cMD5

'Force DEBUG-Mode
Public Const DEBUG_TRACE As Boolean = False

Public Const EXCEL_APP As String = "Excel.Application"
Public Const WORD_APP As String = "Word.Application"

Declare Sub ExitProcess Lib "kernel32" (ByVal Errorlevel As Long)
    
'''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
' Sub-Prozedur        : Main
' Beschreibung        : Hier startet das Programm. Befehlszeile wird ausgelesen
'                       Word gestartet, Doc tempor�r gespeichert, In PDF gewandelt,
'                       tempor�res Doc gel�scht und Programm beendet.
' Parameter           : -
' wird aufgerufen von : Windows ;-)
' �nderungshistorie   :
' Autor  Datum       Grund            Kommentar
' -----  ----------  ---------------  ---------------------------------------
'
'''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
Sub Main()

    Dim oOptions        As cOptions
    Dim oApp            As Object
    Dim sCommandline    As String
    Dim lReturn         As Long
    Dim iVersion        As Integer

    ' Feststellen, ob wir in der IDE oder Standalone laufen (-> bDEBUG wird gesetzt)
    IsIDE

    Set oOptions = New cOptions
    Set goMD5 = New cMD5

    sCommandline = Command()
   
    'Befehlszeilenoptionen auswerten
    If oOptions.ParseArgs(sCommandline) = False Then
        'Falsche Anzahl Options
        MsgBoxCheck "Main: Error in Commandline. "
        End
    End If
       
    iVersion = CheckPDFmakerVersion
    
    If DEBUG_TRACE Then
        MsgBox "The PDF Maker Version is " & iVersion & ".", , "DEBUG PDF Modul"
    End If

    Select Case iVersion
        Case 5
            
            'Office Applikation �bernehmen bzw starten
            Set oApp = StartApplication(oOptions.sApplication)
            
            With oOptions
                InvokeConvertToPDF5 oApp, .sApplication, .sDocument, .sPassword, .sTempDoc
            End With
        
            'Pr�fen ob App noch vorhanden ist
            Set oApp = StartApplication(oOptions.sApplication)
            
            'Dokument schliessen und...
            If oOptions.sApplication = WORD_APP Then
                oApp.application.ActiveDocument.Close
            Else
                oApp.application.ActiveWorkbook.Close
            End If
         
            '...tempor�res Dokument l�schen
            Kill oOptions.sTempDoc
        
            QuitApplication oApp
            
            lReturn = 0
        
        Case 7, 8, 9, 10, 11, 12, 13, 14, 15
            With oOptions
                lReturn = InvokeConvertToPDF8(.sApplication, .sDocument, .sPassword, .sTempDoc, .sPDFtarget, .sUseProtection)
            End With
            
            Select Case lReturn
                Case kPDFMSuccess
                    ' All is well
                Case kPDFMCancelled
                    MsgBoxCheck "Main: Cancelled."
                'Case kPDFMSuccessPartial
                '    MsgBox "ISR-1705PDF: Partial Success, please check document."
                Case Else
                    MsgBoxCheck "Main: Error while creating PDF."
            End Select
            
        Case Else
           MsgBoxCheck "Main: No supported Adobe Acrobat (PDFmaker) found. "
           lReturn = -1
    End Select

    'aufr�umen
    Set oApp = Nothing
    Set goMD5 = Nothing

    'Programmende
    ExitProc lReturn

End Sub

'''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
' Sub-Prozedur        : QuitApplication
' Beschreibung        : Beendet Office Applikation
' Parameter           : Object, das Applikations-Objekt
' wird aufgerufen von : Main
' �nderungshistorie   :
' Autor  Datum       Grund            Kommentar
' -----  ----------  ---------------  ---------------------------------------
'
'''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
Sub QuitApplication(objApplication As Object)

    objApplication.application.Quit

End Sub

'''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
' Funktion            : StartApplication
' Beschreibung        : Startet Word oder Excel, bzw. holt sich eine bestehende Instanz
'                       (wichtig, da w�hrend der PDF-Generierung u.U. die Verbindung
'                       verloren gehen kann)
' Parameter           : -
' wird aufgerufen von : Main
' R�ckgabewert        : Object, das Application-Objekt
' �nderungshistorie   :
' Autor  Datum       Grund            Kommentar
' -----  ----------  ---------------  ---------------------------------------
'
'''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
Function StartApplication(ByVal application As String) As Object
    
    On Error Resume Next 'Fehler �berspringen
 
    Dim objApplication As Object
    Dim intCtr As Integer
  
    Set objApplication = GetObject(, application) 'Verweis erstellen
 
    If (Err.Number = 429) Then    'Sonst neue Instanz laden
        Set objApplication = CreateObject(application)
    ElseIf (Err.Number <> 0) Then
        MsgBox "Microsoft " & application & " konnte nicht gefunden werden.", vbExclamation, "Komponente nicht gefunden"
    End If
   
    Set StartApplication = objApplication
   
End Function

'''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
' Funktion            : ExtractPath
' Beschreibung        : Liefert den Pfad-Anteil eines Pfad/Filenamen Paares
'                       Falls es keinen Pfad-Anteil gibt, wird stattdessen der
'                       App.path geliefert
' Parameter           : Pfad\Filename.ext
' wird aufgerufen von : ~
' R�ckgabewert        : Pfad
' �nderungshistorie   :
' Autor  Datum       Grund            Kommentar
' -----  ----------  ---------------  ---------------------------------------
'
'''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
Public Function ExtractPath(ByVal sPath As String) As String

    Dim sTemp As String

    ' StrReverse is only working in VB6
    sTemp = StrReverse(sPath)
    sTemp = Right(sTemp, Len(sTemp) - InStr(sTemp, "\"))
    sTemp = StrReverse(sTemp)
    
    If sPath = sTemp Then
        sTemp = App.Path
    End If
    
    ExtractPath = sTemp
    
End Function

'''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
' Sub-Prozedur        : ExitProc
' Beschreibung        : Verl�sst das Programm mittels ExitProcess.
'                       In erster linie zu Debugging-Zwecken eingef�hrt, da ein ExitProcess
'                       gleich noch VB mitreisst. =)
' Parameter           : iNum - der R�ckgabewert, mit dem das Programm verlassen werden soll.
' wird aufgerufen von : �berall
' �nderungshistorie   :
' Autor  Datum       Grund            Kommentar
' -----  ----------  ---------------  ---------------------------------------
'
'''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
Public Sub ExitProc(ByVal lNum As Long)

    If Not bDEBUG Then
        ExitProcess lNum
    Else
        End
    End If

End Sub

'''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
' Funktion            : IsIDE
' Beschreibung        : Pr�ft mit einem einfachen Trick, ob das Programm in der
'                       Entwicklungsumgebung ausgef�hrt wurde oder nicht.
' Parameter           : -
' wird aufgerufen von : ~
' R�ckgabewert        : true  - wenn aufgerufen aus IDE
'                       false - wenn standalone ausgef�hrt
' �nderungshistorie   :
' Autor  Datum       Grund            Kommentar
' -----  ----------  ---------------  ---------------------------------------
'
'''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
Public Function IsIDE() As Boolean
    
    On Error GoTo ErrHandler
    'because debug statements are ignored when
    'the app is compiled, the next statment will
    'never be executed in the EXE.
    Debug.Print 1 / 0
    bDEBUG = False
    Exit Function
ErrHandler:
    'If we get an error then we are
    'running in IDE / Debug mode
    bDEBUG = True

End Function

'''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
' Funktion            : CheckPDFmakerVersion
' Beschreibung        : Pr�ft die Version des PDFmakers anhand der Registry
'                       Gibt die h�chste gefundene unterst�tzte Version zur�ck
' Parameter           : -
' wird aufgerufen von : ~
' R�ckgabewert        : Haupt Version des PDF Makers
' �nderungshistorie   :
' Autor  Datum       Grund            Kommentar
' -----  ----------  ---------------  ---------------------------------------
'
'''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
Public Function CheckPDFmakerVersion() As Integer
    On Error GoTo ErrHandler
    
    CheckPDFmakerVersion = 0

    ' Check with Backslash

    If ExistKey(HKEY_CURRENT_USER, "Software\Adobe\Acrobat\PDFMaker\5.0") Then
        CheckPDFmakerVersion = 5
        Exit Function
    End If
    If ExistKey(HKEY_CURRENT_USER, "Software\Adobe\Acrobat\PDFMaker\7.0") Then
        CheckPDFmakerVersion = 7
        Exit Function
    End If
    If ExistKey(HKEY_CURRENT_USER, "Software\Adobe\Acrobat\PDFMaker\8.0") Then
        CheckPDFmakerVersion = 8
        Exit Function
    End If
    If ExistKey(HKEY_CURRENT_USER, "Software\Adobe\Acrobat\PDFMaker\9.0") Then
        CheckPDFmakerVersion = 9
        Exit Function
    End If
    If ExistKey(HKEY_CURRENT_USER, "Software\Adobe\Acrobat\PDFMaker\10.0") Then
        CheckPDFmakerVersion = 10
        Exit Function
    End If
    If ExistKey(HKEY_CURRENT_USER, "Software\Adobe\Acrobat\PDFMaker\11.0") Then
        CheckPDFmakerVersion = 11
        Exit Function
    End If
    If ExistKey(HKEY_CURRENT_USER, "Software\Adobe\Acrobat\PDFMaker\12.0") Then
        CheckPDFmakerVersion = 12
        Exit Function
    End If
    If ExistKey(HKEY_CURRENT_USER, "Software\Adobe\Acrobat\PDFMaker\13.0") Then
        CheckPDFmakerVersion = 13
        Exit Function
    End If
    If ExistKey(HKEY_CURRENT_USER, "Software\Adobe\Acrobat\PDFMaker\14.0") Then
        CheckPDFmakerVersion = 14
        Exit Function
    End If
    If ExistKey(HKEY_CURRENT_USER, "Software\Adobe\Acrobat\PDFMaker\15.0") Then
        CheckPDFmakerVersion = 15
        Exit Function
    End If
        
    ' Check without Backslash
        
    If ExistKey(HKEY_CURRENT_USER, "Software\Adobe\Acrobat PDFMaker\5.0") Then
        CheckPDFmakerVersion = 5
        Exit Function
    End If
    If ExistKey(HKEY_CURRENT_USER, "Software\Adobe\Acrobat PDFMaker\7.0") Then
        CheckPDFmakerVersion = 7
        Exit Function
    End If
    If ExistKey(HKEY_CURRENT_USER, "Software\Adobe\Acrobat PDFMaker\8.0") Then
        CheckPDFmakerVersion = 8
        Exit Function
    End If
    If ExistKey(HKEY_CURRENT_USER, "Software\Adobe\Acrobat PDFMaker\9.0") Then
        CheckPDFmakerVersion = 9
        Exit Function
    End If
    If ExistKey(HKEY_CURRENT_USER, "Software\Adobe\Acrobat PDFMaker\10.0") Then
        CheckPDFmakerVersion = 10
        Exit Function
    End If
    If ExistKey(HKEY_CURRENT_USER, "Software\Adobe\Acrobat PDFMaker\11.0") Then
        CheckPDFmakerVersion = 11
        Exit Function
    End If
    If ExistKey(HKEY_CURRENT_USER, "Software\Adobe\Acrobat PDFMaker\12.0") Then
        CheckPDFmakerVersion = 12
        Exit Function
    End If
    If ExistKey(HKEY_CURRENT_USER, "Software\Adobe\Acrobat PDFMaker\13.0") Then
        CheckPDFmakerVersion = 13
        Exit Function
    End If
    If ExistKey(HKEY_CURRENT_USER, "Software\Adobe\Acrobat PDFMaker\14.0") Then
        CheckPDFmakerVersion = 14
        Exit Function
    End If
    If ExistKey(HKEY_CURRENT_USER, "Software\Adobe\Acrobat PDFMaker\15.0") Then
        CheckPDFmakerVersion = 15
        Exit Function
    End If
        
    If CheckPDFmakerVersion = 0 Then
        'In Citrix Environments we have to check the local machine too because the user profiles may not have set the HCU key
        
        'Check with backslash
        
        If ExistKey(HKEY_LOCAL_MACHINE, "Software\Adobe\Acrobat\PDFMaker\7.0") Then
            CheckPDFmakerVersion = 7
            Exit Function
        End If
        If ExistKey(HKEY_LOCAL_MACHINE, "Software\Adobe\Acrobat\PDFMaker\8.0") Then
        CheckPDFmakerVersion = 8
            Exit Function
        End If
        If ExistKey(HKEY_LOCAL_MACHINE, "Software\Adobe\Acrobat\PDFMaker\9.0") Then
            CheckPDFmakerVersion = 9
            Exit Function
        End If
        If ExistKey(HKEY_LOCAL_MACHINE, "Software\Adobe\Acrobat\PDFMaker\10.0") Then
            CheckPDFmakerVersion = 10
            Exit Function
        End If
        If ExistKey(HKEY_LOCAL_MACHINE, "Software\Adobe\Acrobat\PDFMaker\11.0") Then
            CheckPDFmakerVersion = 11
            Exit Function
        End If
        If ExistKey(HKEY_LOCAL_MACHINE, "Software\Adobe\Acrobat\PDFMaker\12.0") Then
            CheckPDFmakerVersion = 12
            Exit Function
        End If
        If ExistKey(HKEY_LOCAL_MACHINE, "Software\Adobe\Acrobat\PDFMaker\13.0") Then
            CheckPDFmakerVersion = 13
            Exit Function
        End If
        If ExistKey(HKEY_LOCAL_MACHINE, "Software\Adobe\Acrobat\PDFMaker\14.0") Then
            CheckPDFmakerVersion = 14
            Exit Function
        End If
        If ExistKey(HKEY_LOCAL_MACHINE, "Software\Adobe\Acrobat\PDFMaker\15.0") Then
            CheckPDFmakerVersion = 15
            Exit Function
        End If
        
        ' Check without backslash
        
        If ExistKey(HKEY_LOCAL_MACHINE, "Software\Adobe\Acrobat PDFMaker\7.0") Then
            CheckPDFmakerVersion = 7
            Exit Function
        End If
        If ExistKey(HKEY_LOCAL_MACHINE, "Software\Adobe\Acrobat PDFMaker\8.0") Then
        CheckPDFmakerVersion = 8
            Exit Function
        End If
        If ExistKey(HKEY_LOCAL_MACHINE, "Software\Adobe\Acrobat PDFMaker\9.0") Then
            CheckPDFmakerVersion = 9
            Exit Function
        End If
        If ExistKey(HKEY_LOCAL_MACHINE, "Software\Adobe\Acrobat PDFMaker\10.0") Then
            CheckPDFmakerVersion = 10
            Exit Function
        End If
        If ExistKey(HKEY_LOCAL_MACHINE, "Software\Adobe\Acrobat PDFMaker\11.0") Then
            CheckPDFmakerVersion = 11
            Exit Function
        End If
        If ExistKey(HKEY_LOCAL_MACHINE, "Software\Adobe\Acrobat PDFMaker\12.0") Then
            CheckPDFmakerVersion = 12
            Exit Function
        End If
        If ExistKey(HKEY_LOCAL_MACHINE, "Software\Adobe\Acrobat PDFMaker\13.0") Then
            CheckPDFmakerVersion = 13
            Exit Function
        End If
        If ExistKey(HKEY_LOCAL_MACHINE, "Software\Adobe\Acrobat PDFMaker\14.0") Then
            CheckPDFmakerVersion = 14
            Exit Function
        End If
        If ExistKey(HKEY_LOCAL_MACHINE, "Software\Adobe\Acrobat PDFMaker\15.0") Then
            CheckPDFmakerVersion = 15
            Exit Function
        End If
        
    End If
    
    Exit Function
ErrHandler:
    
    MsgBoxCheck "CheckPDFmakerVersion: Registry Key is not accessable."
    CheckPDFmakerVersion = -1

End Function

Sub MsgBoxCheck(sPrompt As String)
    
    'Offnet im Debug eine Messagebox, life wird ein Fehler erzeugt (f�r Logfile & Co)

    If bDEBUG = True Then
        MsgBox (sPrompt)
    Else
        ErrPtnr.OnError "modMain - MsgBoxHandler", sPrompt
    End If

End Sub
