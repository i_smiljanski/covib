CREATE OR REPLACE PACKAGE BODY ISR$RIBBON
IS

  clXmlBackup   CLOB;
  sContext      VARCHAR2(500) := STB$UTIL.getSystemParameter('LOADER_CONTEXT');
  sNameSpace    VARCHAR2(500) := STB$UTIL.getSystemParameter('LOADER_NAMESPACE');
  sTimeOut      VARCHAR2(500) := Stb$util.getSystemParameter('loader.socketTimeout');
  olTokenCache  STB$MENUENTRY$LIST := STB$MENUENTRY$LIST();

--******************************************************************************
PROCEDURE updateRibbon(sHost in VARCHAR2, sPort in VARCHAR2)
IS
  sResult      varchar2(4000);
  sCurrentName constant varchar2(100) := $$PLSQL_UNIT||'.updateRibbon('||sHost||','||sPort||')';
BEGIN
  isr$trace.debug('begin', 'begin', sCurrentName);
  IF STB$UTIL.checkMethodExists('updateRibbon', 'ISR$LOADER') = 'T' THEN
    EXECUTE immediate 'SELECT ISR$LOADER.updateRibbon(:1, :2, :3, :4, :5) FROM DUAL'
    INTO sResult
    USING sHost, sPort, sContext, sNameSpace, sTimeOut;
  END IF;

  ISR$TRACE.debug('sResult', sResult, sCurrentName);
  isr$trace.debug('end', 'end', sCurrentName);
END;

--*****************************************************************************************************
PROCEDURE triggerRibbonUpdate
IS
  oErrorObj      STB$OERROR$RECORD := STB$OERROR$RECORD();
  sCurrentName   constant varchar2(100) := $$PLSQL_UNIT||'.triggerRibbonUpdate';
  sUserMsg       varchar2(4000);
BEGIN
  isr$trace.stat('begin', 'begin', sCurrentName);
  --- (NB 19.1.2016): Gute Absicht, aber unnoetig (zumindest bis wir Wissen wie das alles angenommen wird).
  
  /*
   * triggerRibbonUpdate is called from
   * STB$USERSECURITY         CAN_ALTER_MEMBERSHIP -> missing if privileges are revoked/granted
   * STB$SECURITY             switchRepSession -> wichtig
   * STB$SYSTEM               lockReports / unlockReports
   */

  FOR rGetFrontendSessions IN ( SELECT DISTINCT s.ip, to_char(s.port) port, s.sessionid
                                  FROM isr$session s
                                 WHERE NVL (s.markedforshutdown, 'F') = 'F'
                                   AND NVL (s.inactive, 'F') = 'F'
                                   and NVL (s.wizardInUse, 'F') = 'F'
                                   AND s.ip IS NOT NULL
                                   AND s.port IS NOT NULL
                                   AND s.sessionid != STB$SECURITY.getCurrentSession
                                   AND s.sessionid in (select ts.sessionid from isr$tree$sessions ts where nodetype in ('REPORT','DOCUMENT') and selected = 'T') ) LOOP
    ISR$TRACE.info('frontends', rGetFrontendSessions.ip||':'||rGetFrontendSessions.port||'/'||rGetFrontendSessions.sessionid, 'ISR$RIBBON.triggerRibbonUpdate');
    ISR$RIBBON.updateRibbon(rGetFrontendSessions.ip, rGetFrontendSessions.port);
  END LOOP;

  isr$trace.stat('end', 'end', sCurrentName);
EXCEPTION
  WHEN OTHERS THEN
    sUserMsg := UTD$MSGLIB.getMsg('exUnexpectedError', STB$SECURITY.getCurrentLanguage,  csP1 => sqlerrm(sqlcode));
    oErrorObj.handleError ( STB$TYPEDEF.cnSeverityCritical, sUserMsg, sCurrentName,
                            dbms_utility.format_error_stack||STB$TYPEDEF.crlf||dbms_utility.format_error_backtrace );
END;

--*****************************************************************************************************
FUNCTION isParameterAllowed(sToken IN VARCHAR2, sNodeType in VARCHAR2, sPackage in VARCHAR2 default null) RETURN VARCHAR2
IS
  oErrorObj     STB$OERROR$RECORD := STB$OERROR$RECORD();
  sCurrentName  constant varchar2(1000) := $$PLSQL_UNIT||'.isParameterAllowed (' || sToken  || ', ' || sNodeType  || case when sPackage is not null then ', ' || sPackage end || ')';
  sMenuAllowed  VARCHAR2(1) := 'F';
  sPackage1     VARCHAR2(32);
  sToken1       VARCHAR2(32);
  BEGIN
  ISR$TRACE.debug('begin', 'sToken '||sToken, sCurrentName);
  sPackage1 := sPackage;
  if trim(sPackage1) is null then
    begin
      sToken1 := sToken;
      select t.sHandledby
      into sPackage1
      from table(olTokenCache) t
      where t.sToken = sToken1
      --and t.sDisplay = sNodetype
      --and t.sHandledby is not null
      ;
      ISR$TRACE.debug('from cache', 'sPackage1 '||sPackage1, sCurrentName, olTokenCache);
    exception when others then
      sPackage1 := STB$OBJECT.getCurrentNodePackage(sNodeType,sToken);
      --if trim(sPackage1) is not null then
        olTokenCache.extend();
        olTokenCache(olTokenCache.count()) := STB$MENUENTRY$RECORD(null, sNodetype, sToken, sPackage1, null, null, null, 0);
        ISR$TRACE.debug('from table', 'sPackage1 '||sPackage1, sCurrentName);
      --end if;
    end;
    ISR$TRACE.debug('sPackage1', 'sPackage1: '||sPackage1||',  sNodeType:'||sNodeType, sCurrentName);
  end if;
  oErrorObj := STB$GAL.setMenuAccess(sToken||case when trim(sPackage1) is not null then '#@#'||sPackage1 end, sMenuAllowed);
  ISR$TRACE.debug('end', 'sToken: '||sToken||',  sMenuAllowed:'||sMenuAllowed, sCurrentName);
  RETURN sMenuAllowed;
END;

--*****************************************************************************************************
function getXML( oParameter  in STB$MENUENTRY$RECORD,
                 clXML       out clob,
                 clParamXml  in clob default null)
  return STB$OERROR$RECORD
is
  oErrorObj         STB$OERROR$RECORD := STB$OERROR$RECORD();
  sCurrentName      constant varchar2(100) := $$PLSQL_UNIT||'.getXML('||oParameter.sToken||')';
  xXML              XMLTYPE;
  nLanguage         INTEGER;
  sCurrentNodeType  VARCHAR2(4000);
  sUserMsg          varchar2(4000);

BEGIN
  isr$trace.stat('begin','oParameter.sToken: '||oParameter.sToken, sCurrentName, 'clParamXml IN: '||clParamXml );
  isr$trace.debug('value','oParameter IN', sCurrentname, oParameter);

  nLanguage := Stb$security.GetCurrentLanguage;
  sCurrentNodeType := STB$OBJECT.getCurrentNodeType;

  CASE

    WHEN oParameter.sToken = 'CREATE' THEN

      SELECT (XMLELEMENT ("RIBBON", (SELECT XMLAGG (XMLELEMENT ("RIBBONBUTTON"
                                                              , XMLELEMENT ("BUTTONID", BUTTONID)
                                                              , XMLELEMENT ("TABCONTEXT", TABCONTEXT)
                                                              , XMLELEMENT ("TABVISIBLE", TABVISIBLE)
                                                              , XMLELEMENT ("TOKEN", TOKEN)
                                                              , XMLELEMENT ("ACTIVE", ACTIVE)
                                                              , XMLELEMENT ("VISIBLE", VISIBLE)
                                                              , XMLELEMENT ("TOOLTIP_HEADER", TOOLTIP_HEADER)
                                                              , XMLELEMENT ("TOOLTIP", TOOLTIP)
                                                              , XMLELEMENT ("RIBBONTAB", RIBBONTAB)
                                                              , XMLELEMENT ("BUTTONGROUP", BUTTONGROUP)
                                                              , XMLELEMENT ("BUTTON", BUTTON)
                                                              , XMLELEMENT ("LARGE", LARGE)
                                                              , XMLELEMENT ("BUTTONTYPE", BUTTONTYPE)
                                                              , XMLELEMENT ("ICON_ID", ICON_ID)
                                                              , XMLELEMENT ("ICON", ICON)
                                                              ))
                                       FROM (SELECT /*DISTINCT*/ rb.buttonid
                                                  , Utd$msglib.GetMsg (NVL (rt.description, ribbontab), nLanguage) ribbontab
                                                  , Utd$msglib.GetMsg (rt.context, nLanguage) tabcontext
                                                  , CASE
                                                       WHEN rt.context IN (sCurrentNodeType) THEN 'T'
                                                       WHEN rt.context IS NULL THEN NULL
                                                       ELSE 'F'
                                                    END
                                                       tabvisible
                                                  , Utd$msglib.GetMsg (NVL (rbg.description, buttongroup), nLanguage) buttongroup
                                                  , Utd$msglib.GetMsg (NVL (rb.description, rb.token), nLanguage) button
                                                  , NVL (rbc.token, rb.token) token
                                                  , CASE
                                                       WHEN NVL (rbc.active, rb.active) = 'T'
                                                        AND (rt.context IN (sCurrentNodeType)
                                                          OR rt.context IS NULL) THEN
                                                          CASE
                                                             WHEN NVL (rbc.token, rb.token) LIKE 'FRONTEND.%'
                                                               OR (NVL (rbc.token, rb.token) IS NOT NULL AND ISR$RIBBON.isParameterAllowed (NVL (rbc.token, rb.token), sCurrentNodetype) = 'T') THEN
                                                                CASE
                                                                   WHEN side IS NULL
                                                                     OR oParameter.sToken = 'CREATE'
                                                                     OR REPLACE(oParameter.sToken, 'REFRESH_') = 'RIGHT' AND side = 'R'
                                                                     OR REPLACE(oParameter.sToken, 'REFRESH_') = 'LEFT' AND side = 'L' THEN
                                                                      'T'
                                                                   ELSE
                                                                      'F'
                                                                END
                                                             ELSE
                                                                'F'
                                                          END
                                                       ELSE
                                                          'F'
                                                    END
                                                       active
                                                  , NVL (rbc.visible, rb.visible) visible
                                                  , large
                                                  , icon_id
                                                  , buttontype
                                                  , Utd$msglib.GetMsg (NVL(tooltip_header, NVL (NVL (rbc.token, rb.token), 'NOT_DEFINED')), nLanguage) tooltip_header
                                                  , Utd$msglib.GetMsg (NVL(tooltip, NVL (NVL (rbc.token, rb.token), 'NOT_DEFINED') || '$TOOLTIP'), nLanguage) tooltip
                                                  , NVL (stb$java.getBlobByteString (i.icon), icon_id) icon
                                                  , rt.ordernum
                                                  , rbg.ordernum
                                                  , rb.ordernum
                                               FROM ISR$RIBBON$BUTTON rb
                                                  , ISR$RIBBON$BUTTONGROUP rbg
                                                  , ISR$RIBBON$TAB rt
                                                  , ISR$RIBBON$BUTTON$CONTEXT rbc
                                                  , ISR$ICON i
                                              WHERE rt.ribbontabid = rbg.ribbontabid
                                                AND rbg.buttongroupid = rb.buttongroupid
                                                AND RB.VISIBLE = 'T'
                                                AND (STB$SECURITY.checkGroupRight (ribbontabauthtoken, stb$object.getcurrentreporttypeid) = 'T'
                                                  OR ribbontabauthtoken IS NULL)
                                                AND (STB$SECURITY.checkGroupRight (buttongroupauthtoken, stb$object.getcurrentreporttypeid) = 'T'
                                                  OR buttongroupauthtoken IS NULL)
/* ISRC-446 - es ist nicht klar warum hier ein outer-join gemacht wird , Buttons, Icons und Context gehoeren immer zusammen!!
                                                AND rbc.buttonid(+) = rb.buttonid
                                                AND rbc.context(+) = sCurrentNodeType
                                                AND i.iconid(+) = rb.icon_id
*/
                                                AND rbc.buttonid (+) = rb.buttonid
                                                AND rbc.context (+) = sCurrentNodeType
                                                AND i.iconid = rb.icon_id
                                             ORDER BY rt.ordernum
                                                    , 2
                                                    , rbg.ordernum
                                                    , 5
                                                    , rb.ordernum
                                                    , 6))))
        INTO xXml
        FROM DUAL;

    ELSE

      WITH contextlist AS (SELECT sNodeType FROM table(STB$OBJECT.getCurrentValueList) union select sCurrentNodeType FROM DUAL)
      SELECT (XMLELEMENT ("RIBBON", (SELECT XMLAGG (XMLELEMENT ("RIBBONBUTTON"
                                                              , XMLELEMENT ("BUTTONID", BUTTONID)
                                                              , XMLELEMENT ("TABCONTEXT", TABCONTEXT)
                                                              , XMLELEMENT ("TABVISIBLE", TABVISIBLE)
                                                              , XMLELEMENT ("TOKEN", TOKEN)
                                                              , XMLELEMENT ("ACTIVE", ACTIVE)
                                                              , XMLELEMENT ("VISIBLE", VISIBLE)
                                                              , XMLELEMENT ("TOOLTIP_HEADER", TOOLTIP_HEADER)
                                                              , XMLELEMENT ("TOOLTIP", TOOLTIP)))
                                       FROM (SELECT DISTINCT rb.buttonid
                                                  , Utd$msglib.GetMsg (rt.context, nLanguage) tabcontext
                                                  , CASE
                                                       WHEN rt.context IN (SELECT * FROM contextlist) THEN 'T'
                                                       WHEN rt.context IS NULL THEN NULL
                                                       ELSE 'F'
                                                    END
                                                       tabvisible
                                                  , NVL (rbc.token, rb.token) token
                                                  , CASE
                                                       WHEN NVL (rbc.active, rb.active) = 'T'
                                                        AND (rt.context IN (SELECT * FROM contextlist)
                                                          OR rt.context IS NULL) THEN
                                                          CASE
                                                             WHEN NVL (rbc.token, rb.token) LIKE 'FRONTEND.%'
                                                               OR (NVL (rbc.token, rb.token) IS NOT NULL AND ISR$RIBBON.isParameterAllowed (NVL (rbc.token, rb.token), sCurrentNodeType) = 'T') THEN
                                                                CASE
                                                                   WHEN side IS NULL
                                                                     OR oParameter.sToken = 'CREATE'
                                                                     OR REPLACE(oParameter.sToken, 'REFRESH_') = 'RIGHT' AND side = 'R'
                                                                     OR REPLACE(oParameter.sToken, 'REFRESH_') = 'LEFT' AND side = 'L' THEN
                                                                      'T'
                                                                   ELSE
                                                                      'F'
                                                                END
                                                             ELSE
                                                                'F'
                                                          END
                                                       ELSE
                                                          'F'
                                                    END
                                                       active
                                                  , NVL (rbc.visible, rb.visible) visible
                                                  , Utd$msglib.GetMsg (NVL(tooltip_header, NVL (NVL (rbc.token, rb.token), 'NOT_DEFINED')), nLanguage) tooltip_header   -- vs:07.Sept2016 nvl( nvl (nvl()))  --> see coalesce
                                                  , Utd$msglib.GetMsg (NVL(tooltip, NVL (NVL (rbc.token, rb.token), 'NOT_DEFINED') || '$TOOLTIP'), nLanguage) tooltip
                                                  , rt.ordernum
                                                  , rbg.ordernum
                                                  , rb.ordernum
                                               FROM ISR$RIBBON$BUTTON rb
                                                  , ISR$RIBBON$BUTTONGROUP rbg
                                                  , ISR$RIBBON$TAB rt
                                                  , ISR$RIBBON$BUTTON$CONTEXT rbc
                                              WHERE rt.ribbontabid = rbg.ribbontabid
                                                AND rbg.buttongroupid = rb.buttongroupid
                                                AND RB.VISIBLE = 'T'
                                                AND (STB$SECURITY.checkGroupRight (ribbontabauthtoken, stb$object.getcurrentreporttypeid) = 'T'
                                                  OR ribbontabauthtoken IS NULL)
                                                AND (STB$SECURITY.checkGroupRight (buttongroupauthtoken, stb$object.getcurrentreporttypeid) = 'T'
                                                  OR buttongroupauthtoken IS NULL)
/* ISRC-446
                                                AND rbc.buttonid(+) = rb.buttonid
                                                AND rbc.context(+) = sCurrentNodeType
*/
                                                AND rbc.buttonid (+) = rb.buttonid
                                                AND rbc.context (+) = sCurrentNodeType
                                             ORDER BY rt.ordernum
                                                    , 2
                                                    , rbg.ordernum
                                                    , 5
                                                    , rb.ordernum
                                                    , 6))))
        INTO xXML
        FROM DUAL;

  END CASE;

  isr$trace.debug('status','ready with xmltype',sCurrentName);

  clXml := ISR$XML.XmlToClob(xXML);

  isr$trace.debug('value','length(clXML): '||dbms_lob.getlength(clXML),sCurrentName,clXML);
  isr$trace.stat('end','end', sCurrentName);
  RETURN (oErrorObj);
EXCEPTION
 WHEN OTHERS THEN
   ROLLBACK;
    sUserMsg := UTD$MSGLIB.getMsg('exUnexpectedError', stb$security.getCurrentLanguage,  csP1 => sqlerrm(sqlcode));
    oErrorObj.handleError ( STB$TYPEDEF.cnSeverityCritical, sUserMsg, sCurrentName,
                            dbms_utility.format_error_stack||STB$TYPEDEF.crlf||dbms_utility.format_error_backtrace );
   RETURN (oErrorObj);
END getXML;

--*****************************************************************************************************
FUNCTION putXML(oParameter IN STB$MENUENTRY$RECORD, clXML IN CLOB) RETURN STB$OERROR$RECORD
IS
  oErrorObj        STB$OERROR$RECORD := STB$OERROR$RECORD();
  sUserMsg         varchar2(4000);
  sCurrentName     constant varchar2(4000) := $$PLSQL_UNIT||'.putXML('||oParameter.sToken||')';
BEGIN
  ISR$TRACE.stat('begin','begin','ISR$RIBBON.putXML');

  ISR$TRACE.debug('value','length(xlXML): '||dbms_lob.getlength(clXML),'ISR$RIBBON.putXML',clXML);


  ISR$TRACE.stat('end','end','ISR$RIBBON.putXML');
  RETURN (oErrorObj);
EXCEPTION
  WHEN OTHERS THEN
    ROLLBACK;
      sUserMsg := UTD$MSGLIB.getMsg('exUnexpectedError', stb$security.getCurrentLanguage,  csP1 => sqlerrm(sqlcode));
      oErrorObj.handleError ( STB$TYPEDEF.cnSeverityCritical, sUserMsg, sCurrentName,
                              dbms_utility.format_error_stack||STB$TYPEDEF.crlf||dbms_utility.format_error_backtrace );
   RETURN (oErrorObj);
END putXML;

END ISR$RIBBON;