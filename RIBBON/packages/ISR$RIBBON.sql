CREATE OR REPLACE PACKAGE ISR$RIBBON
as
-- ***************************************************************************************************
-- Description/Usage:
-- The package STB$GAL is the GUI -Interface package. The Frontend calls directly the package functions
-- and procedures.
-- If the Frontens request works on a report type the STB$GAL calls the package ISR$REPORTTYPE$PACKAGE
-- If the request is dealing with the administrartion the request is passed to packages STB$SYSTEM or
-- STB$USERSECURITY
-- ***************************************************************************************************


PROCEDURE updateRibbon(sHost in VARCHAR2, sPort in VARCHAR2);
-- ***************************************************************************************************
-- Date und Autor: 16. Aug 2014, MCD
-- updates the ribbon
--
-- Parameters:
-- sHost  the host
-- sPort  the port 
--
--
-- ***************************************************************************************************

PROCEDURE triggerRibbonUpdate;
-- ***************************************************************************************************
-- Date und Autor: 16. Aug 2014, MCD
-- triggers a ribbon update
--
--
-- ***************************************************************************************************

FUNCTION isParameterAllowed(sToken IN VARCHAR2, sNodeType in VARCHAR2, sPackage in VARCHAR2 default null) RETURN VARCHAR2;
-- ***************************************************************************************************
-- Date und Autor: 16. Aug 2014, MCD
-- checks if a ribbon menu entry is allowed
-- allowed 'T' = colored button icon = selectable/active
-- allowed 'F' = gray button icon    = inactive
--
-- Parameters:
-- sToken   the token
--
-- Return:
-- 'T' if allowed otherwise 'F'  
--
-- ***************************************************************************************************

FUNCTION getXML(oParameter IN STB$MENUENTRY$RECORD, clXML OUT CLOB, clParamXml IN CLOB DEFAULT NULL) RETURN STB$OERROR$RECORD;
-- ***************************************************************************************************
-- Date und Autor: 16. Aug 2014, MCD
-- retrieves an xml file with instructions
--
-- Parameters:
-- oParameter   the parameter
-- clXML        the xml file
-- clParamXml   the xml file with needed parameters, default null
--
-- Return:
-- STB$OERROR$RECORD  
--
-- ***************************************************************************************************

FUNCTION putXML(oParameter IN STB$MENUENTRY$RECORD, clXML IN CLOB) RETURN STB$OERROR$RECORD;
-- ***************************************************************************************************
-- Date und Autor: 16. Aug 2014, MCD
-- sends an xml file with instructions
--
-- Parameters:
-- oParameter the parameter
-- clXML      the xml file 
--
-- Return:
-- STB$OERROR$RECORD   
--
-- ***************************************************************************************************

END ISR$RIBBON; 