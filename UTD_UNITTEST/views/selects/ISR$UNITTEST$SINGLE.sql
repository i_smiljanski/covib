CREATE OR REPLACE VIEW ISR$UNITTEST$SINGLE (ENTITY, CODE, UNITTEST_SINGLECODE, NAME, DISPLAY, INFO, ORDERNUMBER, STYLE, ICON, COLOR, COL1, COL2, COL3, COL4) AS SELECT p.entity
, p.VALUE
, p.VALUE
, p.VALUE
, p.display
, p.info
, p.ordernumber
, DECODE (MOD (p.ordernumber, 4),
1, 'B',
DECODE (MOD (p.ordernumber, 4), 2, 'I', NULL))
, DECODE (MOD (p.ordernumber, 4), 0, 'flower_yellow', NULL)
, DECODE (MOD (p.ordernumber, 4), 3, '99CCFF', 'FFFFFF')
, DECODE (MOD (p.ordernumber, 4), 0, 'flower_yellow', '-')
, 'F'
, DECODE (MOD (p.ordernumber, 4),
1, 'BOLD',
DECODE (MOD (p.ordernumber, 4), 2, 'ITALIC', '-'))
, DECODE (MOD (p.ordernumber, 4), 3, '99CCFF', '-')
FROM ISR$PARAMETER$VALUES p
WHERE p.entity = 'SB$UNITTEST$SINGLE'
ORDER BY p.ordernumber