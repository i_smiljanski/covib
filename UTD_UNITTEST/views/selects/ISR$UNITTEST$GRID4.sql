CREATE OR REPLACE VIEW ISR$UNITTEST$GRID4 (ENTITY, CODE, NAME, DISPLAY, INFO, ORDERNUMBER) AS SELECT p.entity
, p.VALUE
, p.VALUE
, p.display
, p.info
, p.ordernumber
FROM ISR$PARAMETER$VALUES p
WHERE p.entity = 'SB$UNITTEST$GRID4'
ORDER BY p.ordernumber