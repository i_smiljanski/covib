<?xml version="1.0" encoding="UTF-8"?>
<!--
ISR$BIO$COVIB$ADA$TITER$CTRL$SUM.XSL
==========================================================================

-->
<xsl:stylesheet version="3.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform" xmlns:xs="http://www.w3.org/2001/XMLSchema" xmlns:fn="http://www.w3.org/2005/xpath-functions" xmlns:utd="http://www.uptodata.com">
	<xsl:strip-space elements="*"/>
	<xsl:output method="html" version="4.01" encoding="UTF-8" indent="yes" media-type="text/html"/>
	<!--
==========================================================================
Includes
==========================================================================-->
	<xsl:include href="ISR$CSS.XSL"/>
	<xsl:include href="ISR$UTIL.XSL"/>
	<xsl:include href="ISR$CAPTION.XSL"/>
	<xsl:include href="ISR$WT$TABLE.XSL"/>
	<xsl:include href="ISR$WT$PAGE.XSL"/>
	<!--
==========================================================================
Parameters
==========================================================================-->
	<xsl:param name="experiment" as="xs:string">S_TITER_CTRL_SUM</xsl:param>
	<xsl:param name="hash" as="xs:string">TISM</xsl:param>
	<!--
==========================================================================
Variables
==========================================================================-->
	<xsl:variable name="attributes" select="/study/attributes/*"/>
	<xsl:variable name="not_determined" select="$attributes/not_performed_identifier"/>
	<xsl:variable name="not_performed" select="$attributes/not_performed_identifier"/>
	<xsl:variable name="not_detected" select="$attributes/not_detected_identifier"/>
	<xsl:variable name="runs" select="/study/bioanalytic/runs"/>
	<xsl:variable name="study" select="/study/bioanalytic/studies/study"/>
	<xsl:variable name="response_unit" select="$attributes/ada_response_unit"/>
	<xsl:variable name="format" select="/study/formats/experiment[@name=$experiment]"/>
	<xsl:variable name="criteria" select="/study/protocol/criteria"/>
	<xsl:variable name="table-caption" select="utd:formats($experiment,'caption')"/>
	<xsl:variable name="table-type" select="utd:formats($experiment,('var'))[upper-case(@name)='TABLE_TYPE']"/>
	<xsl:variable name="caption-prefix" select="utd:formats($experiment,('var'))[upper-case(@name)='CAPTION_PREFIX']"/>
	<xsl:variable name="caption-sequence" select="utd:formats($experiment,('var'))[upper-case(@name)='CAPTION_SEQUENCE']"/>
	<xsl:variable name="caption-type" select="utd:formats($experiment,('var'))[upper-case(@name)='CAPTION_TYPE']"/>
	<xsl:variable name="column_entity" select="$attributes/exp_col_entity"/>
	<xsl:variable name="selected-columns" select="$criteria[@entity=$column_entity]/value[masterkey=$experiment]"/>
	<xsl:variable name="hide_cols" select="for $s in utd:formats($experiment,'var')[@type='OPTCOL'] return if ($attributes/*[name() = $s] != 'T') then $s/@key else ()"/>
	<xsl:variable name="columns" as="node()*">
		<xsl:for-each select="$selected-columns/key">
			<xsl:copy-of select="(utd:formats($experiment,'cols'))[@name=current()][not(@name=$hide_cols)]"/>
		</xsl:for-each>
	</xsl:variable>
	<xsl:variable name="column_width" select="utd:formats($experiment,('cols','width'))"/>
	<xsl:variable name="column_group" select="utd:formats($experiment,('cols','group'))"/>
	<xsl:variable name="column_styles" select="utd:formats($experiment,('cols','style'))"/>
	<xsl:variable name="hide_stats" select="for $s in utd:formats($experiment,'var')[@type='OPTSTAT'] return if ($attributes/*[name() = $s] != 'T') then $s/@key else ()"/>
	<xsl:variable name="stats_individual" select="utd:formats($experiment,'calc')[@type='INDIVIDUAL'][not(@name=$hide_stats)]"/>
	<xsl:variable name="stats_overall" select="utd:formats($experiment,'calc')[@type='OVERALL'][not(@name=$hide_stats)]"/>
	<xsl:variable name="stat_styles" select="utd:formats($experiment,('calc', 'style'))"/>
	<xsl:variable name="maxlines" select="$attributes/*[name() = concat( 'max_lines_', lower-case($experiment) ) ]"/>
	<xsl:variable name="maxcols" select="$attributes/*[name() = concat( 'max_cols_', lower-case($experiment) ) ]"/>
	<xsl:variable name="excel-filename" select="utd:formats($experiment,'var')[@name='EXCEL_FILENAME']"/>
	<xsl:variable name="section-title" select="utd:formats($experiment,'var')[@name='SECTION_TITLE']"/>
	<!-- other -->
	<xsl:variable name="calculations" select="$study/calculations/experiment[@name=$experiment and @type='titer_control']"/>
	<!--
==========================================================================
Main template
========================================================================== -->
	<xsl:template match="/">
		<xsl:variable name="isr-sections">
			<isr-sections emptytable="{false()}" experiment="{$experiment}">
				<xsl:attribute name="title"><xsl:text>ISR$BIO$COVIB$ADA$TITER$CTRL.XSL - </xsl:text><xsl:value-of select="$experiment"/></xsl:attribute>
				<xsl:if test="$table-type">
					<xsl:attribute name="type" select="$table-type"/>
				</xsl:if>
				<xsl:if test="exists($calculations/calculation/values/dilution)">
					<xsl:for-each select="distinct-values($calculations/calculation/target/acceptedrun)">
						<xsl:variable name="acceptedrun" select="."/>
						<xsl:variable name="section-params" as="node()*">
							<study>
								<xsl:value-of select="$study/name"/>
							</study>
							<acceptedrun>
								<xsl:value-of select="$acceptedrun"/>
							</acceptedrun>
						</xsl:variable>
						<isr-section>
							<xsl:attribute name="experiment" select="$experiment"/>
							<xsl:if test="$section-title">
								<xsl:attribute name="title" select="utd:params($section-title,$section-params)"/>
							</xsl:if>
							<xsl:if test="$excel-filename">
								<xsl:attribute name="filename" select="utd:params($excel-filename,$section-params)"/>
							</xsl:if>
							<xsl:if test="$table-type">
								<xsl:attribute name="type" select="$table-type"/>
							</xsl:if>
							<xsl:for-each select="distinct-values($calculations/calculation/target[acceptedrun = $acceptedrun]/analyteid)">
								<xsl:variable name="analyteid" select="."/>
								<xsl:for-each select="distinct-values($calculations/calculation/target[acceptedrun = $acceptedrun][analyteid = $analyteid]/meanormedian)">
									<xsl:variable name="meanormedian" select="current()"/>
									<!--xsl:for-each select="distinct-values($calculations/calculation/target[analyteid = $analyteid][meanormedian = $meanormedian]/concentrationunit)"-->
									<!--xsl:variable name="unit" select="current()"/-->
									<!--xsl:variable name="analyte" select="$study/analytes/analyte[@id=$analyteid]"/-->
									<xsl:variable name="analyte" select="$study/analytes/analyte[@id=$analyteid]"/>
									<xsl:variable name="hashstring" select="concat( $acceptedrun, '_', $study/@code, '_', $analyte/@code, '_', $meanormedian )"/>
									<xsl:variable name="calcs" select="$calculations/calculation[target/acceptedrun = $acceptedrun][target/analyteid = $analyteid][target/meanormedian = $meanormedian]"/>
									<!--xsl:variable name="cap" select="if ($maxcols='' or empty($maxcols)) then 100 else $maxcols - 1 "/>
									<xsl:variable name="tables" select="ceiling(count($columns[@name != 'RUNID']) div $cap)"/-->
									<!--xsl:for-each select="1 to xs:integer($tables)"-->
									<xsl:variable name="number" select="1"/>
									<xsl:call-template name="createTable">
										<xsl:with-param name="hash" select="concat( $hash, '_', utd:hashcalc( $hashstring, 18 ) )"/>
										<xsl:with-param name="number" select="$number"/>
										<xsl:with-param name="acceptedrun" select="$acceptedrun"/>
										<xsl:with-param name="analyte" select="$analyte"/>
										<xsl:with-param name="meanormedian" select="$meanormedian"/>
										<xsl:with-param name="calcs" select="$calcs"/>
									</xsl:call-template>
									<!--/xsl:for-each-->
								</xsl:for-each>
								<!--/xsl:for-each-->
							</xsl:for-each>
						</isr-section>
					</xsl:for-each>
				</xsl:if>
			</isr-sections>
		</xsl:variable>
		<!--xsl:copy-of select="$isr-sections"/-->
		<xsl:apply-templates select="$isr-sections/isr-sections"/>
	</xsl:template>
	<!--
==========================================================================
createTable
========================================================================== -->
	<xsl:template name="createTable">
		<xsl:param name="hash"/>
		<xsl:param name="number"/>
		<xsl:param name="acceptedrun"/>
		<xsl:param name="analyte"/>
		<xsl:param name="meanormedian"/>
		<xsl:param name="calcs"/>
		<xsl:variable name="concentrationunit">
			<xsl:choose>
				<xsl:when test="$attributes/concentration_unit = 'UNIT_NO_CHANGE' ">
					<xsl:value-of select="$analyte/concentrationunits"/>
				</xsl:when>
				<xsl:when test="$attributes/concentration_unit">
					<xsl:value-of select="$attributes/concentration_unit"/>
				</xsl:when>
				<xsl:otherwise>
					<xsl:value-of select="$analyte/concentrationunits"/>
				</xsl:otherwise>
			</xsl:choose>
		</xsl:variable>
		<!-- PARAMETERS -->
		<xsl:variable name="isr-parameters">
			<isr-parameters>
				<analyte-name>
					<xsl:value-of select="$analyte/name"/>
				</analyte-name>
				<conc-unit>
					<xsl:value-of select="$concentrationunit"/>
				</conc-unit>
				<response-unit>
					<xsl:value-of select="$response_unit"/>
				</response-unit>
				<mean-or-median-nc>
					<xsl:value-of select="$meanormedian"/>
				</mean-or-median-nc>
				<acceptedrun>
					<xsl:value-of select="$acceptedrun"/>
				</acceptedrun>
			</isr-parameters>
		</xsl:variable>
		<!-- CAPTION -->
		<xsl:variable name="isr-caption">
			<isr-caption>
				<xsl:attribute name="type">listed</xsl:attribute>
				<xsl:if test="$caption-type">
					<xsl:attribute name="type"><xsl:value-of select="$caption-type"/></xsl:attribute>
				</xsl:if>
				<xsl:attribute name="hash" select="$hash"/>
				<xsl:attribute name="ref" select="$hash"/>
				<xsl:attribute name="number" select="$number"/>
				<xsl:if test="$caption-prefix">
					<xsl:attribute name="prefix" select="$caption-prefix"/>
				</xsl:if>
				<xsl:if test="$caption-sequence">
					<xsl:attribute name="seq" select="$caption-sequence"/>
				</xsl:if>
				<title>
					<xsl:value-of select="$table-caption"/>
				</title>
				<parameters/>
			</isr-caption>
		</xsl:variable>
		<!-- TABLE -->
		<xsl:variable name="isr-table">
			<isr-table type="standard">
				<xsl:if test="not(empty($maxlines))">
					<xsl:attribute name="maxlines" select="$maxlines"/>
				</xsl:if>
				<xsl:variable name="first_run" select="distinct-values($calcs/target/runid)[1]"/>
				<headers>
					<header calcwidth="true">
						<xsl:for-each select="$columns">
							<head class="borderTopBottom">
								<xsl:attribute name="class" select="'border'||(if (last()=1) then 'All' else ('TopBottom'||(if (position()=1) then 'Left' else '')))"/>
								<xsl:if test="$column_width[@name=current()/@name]">
									<xsl:attribute name="width" select="$column_width[@name=current()/@name]"/>
								</xsl:if>
								<xsl:call-template name="attributes">
									<xsl:with-param name="input" select="current()"/>
									<xsl:with-param name="header" select="true()"/>
								</xsl:call-template>
								<xsl:value-of select="."/>
							</head>
						</xsl:for-each>
						<xsl:for-each select="$calcs[target/runid=$first_run]">
							<xsl:variable name="calc" select="."/>
							<xsl:variable name="params" as="node()*">
								<xsl:for-each select="$calc/target/*">
									<xsl:element name="{name()}">
										<xsl:value-of select="current()"/>
									</xsl:element>
								</xsl:for-each>
								<xsl:copy-of select="$isr-parameters/isr-parameters/*"/>
							</xsl:variable>
							<xsl:for-each select="$stats_individual">
								<head class="borderTopBottom">
									<xsl:attribute name="class" select="'border'||(if (last()=1) then 'All' else ('TopBottom'||(if (position()=1) then 'Left' else if (position()=last()) then 'Right' else '')))"/>
									<xsl:call-template name="attributes">
										<xsl:with-param name="input" select="current()"/>
										<xsl:with-param name="header" select="true()"/>
									</xsl:call-template>
									<xsl:value-of select="utd:params(., $params)"/>
								</head>
							</xsl:for-each>
						</xsl:for-each>
					</header>
				</headers>
				<rows>
					<xsl:for-each select="distinct-values($calcs/target/runid)">
						<!--xsl:sort select="*[name() = $sort-by]" data-type="{$sort-type}" order="{$sort-order}"/-->
						<xsl:variable name="rpos" select="position()"/>
						<xsl:variable name="rlast" select="last()"/>
						<xsl:variable name="run" select="$runs/run[@id=current()]"/>
						<xsl:variable name="calcs" select="$calcs[target/runid=$run/@id]"/>
						<!--xsl:variable name="max-dils" select="max($calcs/count(values/dilution))"/>
						<xsl:for-each select="1 to $max-dils">
							<xsl:variable name="dilpos" select="current()"/>
							<xsl:variable name="xpos" select="position()"/>
							<xsl:variable name="xlast" select="last()"/-->
						<xsl:variable name="params" as="node()*">
							<xsl:call-template name="parameters">
								<xsl:with-param name="run" select="$run"/>
								<xsl:with-param name="calcs" select="$calcs"/>
							</xsl:call-template>
						</xsl:variable>
						<xsl:variable name="varparams" as="node()*">
							<xsl:for-each select="$params">
								<xsl:element name="{lower-case(@name)}">
									<xsl:value-of select="."/>
								</xsl:element>
							</xsl:for-each>
						</xsl:variable>
						<row bracket="{$run/@id}">
							<xsl:for-each select="$columns">
								<cell>
									<xsl:for-each select="$params[@name=current()/@name]/@*[not( name() = ('name') )]">
										<xsl:attribute name="{ current()/name() }" select="current()"/>
									</xsl:for-each>
									<xsl:if test="exists($column_group[@name=current()/@name])">
										<xsl:attribute name="group" select="utd:params($column_group[@name=current()/@name], $varparams)"/>
									</xsl:if>
									<!--xsl:if test="$rpos = $rlast"-->
									<xsl:attribute name="class" select=" 'borderBottom' "/>
									<xsl:attribute name="class" select="'border'||(if (last()=1) then 'All' else ((if ($rpos=$rlast) then 'Bottom' else '')||(if (position()=1) then 'Left' else '')))"/>
									<!--/xsl:if-->
									<xsl:call-template name="attributes">
										<xsl:with-param name="input" select="current()"/>
									</xsl:call-template>
									<xsl:variable name="value" select="$params[@name=current()/@name]"/>
									<xsl:choose>
										<xsl:when test="$value = '' or empty($value)">
											<xsl:value-of select=" '-' "/>
										</xsl:when>
										<xsl:otherwise>
											<xsl:value-of select="$value"/>
										</xsl:otherwise>
									</xsl:choose>
								</cell>
							</xsl:for-each>
							<xsl:for-each select="$calcs">
								<xsl:variable name="calc" select="current()"/>
								<xsl:for-each select="$stats_individual">
									<cell class="borderBottom">
										<xsl:attribute name="class" select="'border'||(if (last()=1) then 'All' else ((if ($rpos=$rlast) then 'Bottom' else '')||(if (position()=1) then 'Left' else if (position()=last()) then 'Right' else '')))"/>
										<xsl:call-template name="attributes">
											<xsl:with-param name="input" select="current()"/>
										</xsl:call-template>
										<xsl:if test="current()/@key = 'MEAN' and $run/flagresponse">
											<xsl:attribute name="footnote">
												<xsl:choose>
													<xsl:when test="$run/flagresponse/@type='LLRe'">Below lower</xsl:when>
													<xsl:when test="$run/flagresponse/@type='ULRe'">Above upper</xsl:when>
												</xsl:choose>
												<xsl:text> limit of </xsl:text>
												<xsl:value-of select="$calc/target/name"/>
												<xsl:text> response (</xsl:text>
												<xsl:value-of select="$run/flagresponse"/>
												<xsl:text>)</xsl:text>
											</xsl:attribute>
										</xsl:if>
										<xsl:value-of select="$calc/result/*[upper-case(name())=current()/@key]"/>
									</cell>
								</xsl:for-each>
							</xsl:for-each>
						</row>
						<!--/xsl:for-each-->
					</xsl:for-each>
					<xsl:call-template name="calcrows">
						<xsl:with-param name="calcs" select="$calcs"/>
						<xsl:with-param name="analyte" select="$analyte"/>
						<xsl:with-param name="isr-parameters" select="$isr-parameters"/>
					</xsl:call-template>
				</rows>
			</isr-table>
		</xsl:variable>
		<!-- NOTES -->
		<xsl:variable name="isr-notes">
			<isr-notes>
				<xsl:for-each select="utd:formats($experiment,'footer')[starts-with(@name, 'LINE')]">
					<note>
						<xsl:value-of select="."/>
					</note>
				</xsl:for-each>
			</isr-notes>
		</xsl:variable>
		<!-- PAGE -->
		<xsl:variable name="isr-page">
			<isr-page>
				<xsl:attribute name="createCap" select="true()"/>
				<xsl:attribute name="number" select="$number"/>
				<xsl:attribute name="hash" select="$hash"/>
				<xsl:copy-of select="$isr-parameters"/>
				<xsl:copy-of select="$isr-caption"/>
				<xsl:copy-of select="$isr-table"/>
				<xsl:copy-of select="$isr-notes"/>
			</isr-page>
		</xsl:variable>
		<xsl:copy-of select="$isr-page"/>
	</xsl:template>
	<!-- 
==========================================================================
calcrows
========================================================================== -->
	<xsl:template name="calcrows">
		<xsl:param name="calcs"/>
		<xsl:param name="analyte"/>
		<xsl:param name="isr-parameters"/>
		<!--xsl:param name="nominal_concs"/>
		<xsl:param name="nominal_number"/-->
		<!--xsl:variable name="calculations" select="if ($type = 'intra') then $calculation_intra else if ($type ='anova') then $calculation_anova else $calculation_overall"/-->
		<xsl:variable name="stats" select="$stats_overall"/>
		<xsl:variable name="paramtitles" select="$stats/text()"/>
		<xsl:variable name="paranames" select="$stats/lower-case(@key)"/>
		<xsl:for-each select="$stats">
			<xsl:variable name="stat" select="current()"/>
			<xsl:variable name="xpos" select="position()"/>
			<xsl:variable name="xlast" select="last()"/>
			<row bracket="calcs">
				<xsl:for-each select="$columns">
					<cell>
						<xsl:if test="$column_width[@name=current()/@name]">
							<xsl:attribute name="width" select="$column_width[@name=current()/@name]"/>
						</xsl:if>
						<xsl:copy-of select="utd:borderclass( $xpos, $xlast, true(), true() )"/>
						<xsl:attribute name="class" select="'border'||(if (last()=1) then 'All' else ((if ($xpos=$xlast) then 'Bottom' else '')||(if (position()=1) then 'Left' else '')))"/>
						<xsl:call-template name="attributes">
							<xsl:with-param name="input" select="$stat"/>
						</xsl:call-template>
						<xsl:choose>
							<xsl:when test="position() = 1">
								<xsl:call-template name="attributes">
									<xsl:with-param name="input" select="$stat"/>
									<xsl:with-param name="header" select="true()"/>
								</xsl:call-template>
								<!--xsl:attribute name="class">sum</xsl:attribute>
								<xsl:copy-of select="utd:borderclass( $xpos, $xlast, false(), true() )"/-->
								<xsl:value-of select="utd:params($stat,$isr-parameters/isr-parameters/*)"/>
							</xsl:when>
							<!--xsl:when test="(@name = 'RUN_NC-MEAN-MEDIAN_SIG' or @name = 'RUN_CUTPOINT_SIG') and $stat/@key = 'N'">
								<xsl:value-of select="$calcs/result/n"/>
							</xsl:when-->
							<xsl:when test="@name = 'RUN_NC-MEAN-MEDIAN_SIG'">
								<xsl:value-of select="$calcs/result/*[upper-case(name()) = 'NC-'||$stat/@key]"/>
							</xsl:when>
							<xsl:when test="@name = 'RUN_CUTPOINT_SIG'">
								<xsl:value-of select="$calcs/result/*[upper-case(name()) = 'CP-'||$stat/@key]"/>
							</xsl:when>
							<xsl:otherwise/>
						</xsl:choose>
					</cell>
				</xsl:for-each>
			</row>
		</xsl:for-each>
	</xsl:template>
	<!--
==========================================================================
attributes
==========================================================================-->
	<xsl:template name="attributes">
		<xsl:param name="input" required="yes"/>
		<xsl:param name="header" as="xs:boolean" select="false()" required="no"/>
		<xsl:variable name="styles" select="if (starts-with($input/@entity, 'COL')) then $column_styles else $stat_styles"/>
		<xsl:variable name="style" select="$styles[@name=$input/@name]"/>
		<xsl:if test="$style">
			<xsl:variable name="excel-type" select="utd:styleValue(if ($header) then 'excel-header-type' else 'excel-type', $style)"/>
			<xsl:if test="$excel-type">
				<xsl:attribute name="excel-type" select="$excel-type"/>
			</xsl:if>
			<xsl:variable name="excel-format" select="utd:styleValue(if ($header) then 'excel-header-format' else 'excel-format', $style)"/>
			<xsl:if test="$excel-format">
				<xsl:attribute name="excel-format" select="$excel-format"/>
			</xsl:if>
		</xsl:if>
	</xsl:template>
	<!--
==========================================================================
parameters
==========================================================================-->
	<xsl:template name="parameters">
		<xsl:param name="run"/>
		<xsl:param name="calcs"/>
		<!--for grouping: <param name="xxx" group="true" class="borderBottom"> -->
		<param name="RUNID" group="true_{$run/runid}">
			<xsl:value-of select="$run/runid"/>
		</param>
		<param name="NAME" group="true_{$run/runid}">
			<xsl:value-of select="$run/name"/>
		</param>
		<param name="ASSAY_DESCRIPTION" group="true_{$run/runid}">
			<xsl:value-of select="$run/assaydesc"/>
		</param>
		<param name="RUNTYPE" group="true_{$run/runid}">
			<xsl:value-of select="$run/runtype"/>
		</param>
		<param name="EXTRACTIONDATE" group="true_{$run/runid}">
			<xsl:choose>
				<xsl:when test="$run/extractiondate and $run/extractiondate != '' ">
					<xsl:value-of select="$run/extractiondate"/>
				</xsl:when>
				<xsl:otherwise>
					<xsl:text>N/A</xsl:text>
				</xsl:otherwise>
			</xsl:choose>
		</param>
		<param name="FIRSTINJECT" group="true_{$run/runid}">
			<xsl:choose>
				<xsl:when test="$run/firstinject and $run/firstinject != '' ">
					<xsl:value-of select="$run/firstinject"/>
				</xsl:when>
				<xsl:otherwise>
					<xsl:text>N/A</xsl:text>
				</xsl:otherwise>
			</xsl:choose>
		</param>
		<param name="LASTINJECT" group="true_{$run/runid}">
			<xsl:choose>
				<xsl:when test="$run/lastinject and $run/lastinject != '' ">
					<xsl:value-of select="$run/lastinject"/>
				</xsl:when>
				<xsl:otherwise>
					<xsl:text>N/A</xsl:text>
				</xsl:otherwise>
			</xsl:choose>
		</param>
		<param name="STARTDATE" group="true_{$run/runid}">
			<xsl:value-of select="$run/runstartdate"/>
		</param>
		<param name="NOTEBOOK" group="true_{$run/runid}">
			<xsl:value-of select="$run/notebook"/>
		</param>
		<param name="PAGENUMBER" group="true_{$run/runid}">
			<xsl:value-of select="$run/pagenumber"/>
		</param>
		<param name="REGSTATUS" group="true_{$run/runid}">
			<xsl:value-of select="$run/regstatus"/>
		</param>
		<param name="REASON" group="true_{$run/runid}">
			<xsl:value-of select="$run/reason"/>
		</param>
		<param name="INSTRUMENTS" group="true_{$run/runid}">
			<xsl:value-of select="$run/instruments" separator=", "/>
		</param>
		<param name="NCMEANMEDIAN" group="true_{$run/runid}">
			<xsl:value-of select="($calcs/values/dilution)[1]/nc-mean-median" separator=", "/>
		</param>
		<param name="CUTPOINT" group="true_{$run/runid}">
			<xsl:value-of select="($calcs/values/dilution)[1]/cutpoint" separator=", "/>
		</param>
		<param name="NCCV" group="true_{$run/runid}">
			<xsl:if test="($calcs/values/dilution)[1]/nc-flagcv = 'T'">
				<xsl:attribute name="footnote" select="'CV above acceptance criteria of '||($calcs/values/dilution)[1]/cvlimit||'%'"/>
			</xsl:if>
			<xsl:value-of select="($calcs/values/dilution)[1]/nc-cv" separator=", "/>
		</param>
	</xsl:template>
</xsl:stylesheet>
