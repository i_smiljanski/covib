<?xml version="1.0" encoding="UTF-8"?>
<!--
ISR$BIO$COVIB$ADA$DTA.XSL
==========================================================================

-->
<xsl:stylesheet version="3.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform" xmlns:xs="http://www.w3.org/2001/XMLSchema" xmlns:fn="http://www.w3.org/2005/xpath-functions" xmlns:utd="http://www.uptodata.com">
	<xsl:strip-space elements="*"/>
	<xsl:output method="html" html-version="5.0" encoding="UTF-8" indent="yes" media-type="text/html"/>
	<!--
==========================================================================
Includes
==========================================================================-->
	<xsl:include href="ISR$CSS.XSL"/>
	<xsl:include href="ISR$UTIL.XSL"/>
	<xsl:include href="ISR$CAPTION.XSL"/>
	<xsl:include href="ISR$WT$TABLE.XSL"/>
	<xsl:include href="ISR$WT$PAGE.XSL"/>
	<!--
==========================================================================
Parameters
==========================================================================-->
	<xsl:param name="experiment" as="xs:string">S_DTA</xsl:param>
	<xsl:param name="hash" as="xs:string">DTAR</xsl:param>
	<xsl:param name="reporttype" as="xs:string">excel</xsl:param>
	<!--
==========================================================================
Variables
==========================================================================-->
	<xsl:variable name="attributes" select="/study/attributes/*"/>
	<xsl:variable name="not_determined" select="$attributes/not_performed_identifier"/>
	<xsl:variable name="not_performed" select="$attributes/not_performed_identifier"/>
	<xsl:variable name="not_detected" select="$attributes/not_detected_identifier"/>
	<xsl:variable name="runs" select="/study/bioanalytic/runs"/>
	<xsl:variable name="study" select="/study/bioanalytic/studies/study"/>
	<xsl:variable name="criteria" select="/study/protocol/criteria"/>
	<xsl:variable name="studydesign" select="/study/bioanalytic/studies/study/design"/>
	<xsl:variable name="samples" select="/study/bioanalytic/samples/sample"/>
	<xsl:variable name="response_unit" select="$attributes/ada_response_unit"/>
	<xsl:variable name="format" select="/study/formats/experiment[@name=$experiment]"/>
	<xsl:variable name="table-caption" select="utd:formats($experiment,'caption')"/>
	<xsl:variable name="table-type" select="utd:formats($experiment,('var'))[upper-case(@name)='TABLE_TYPE']"/>
	<xsl:variable name="caption-prefix" select="utd:formats($experiment,('var'))[upper-case(@name)='CAPTION_PREFIX']"/>
	<xsl:variable name="caption-sequence" select="utd:formats($experiment,('var'))[upper-case(@name)='CAPTION_SEQUENCE']"/>
	<xsl:variable name="caption-type" select="utd:formats($experiment,('var'))[upper-case(@name)='CAPTION_TYPE']"/>
	<xsl:variable name="hide_cols" select="for $s in utd:formats($experiment,'var')[@type='OPTCOL'] return if ($attributes/*[name() = $s] != 'T') then $s/@key else ()"/>
	<xsl:variable name="dta_type" select="'DTA_FOR_'||upper-case($reporttype)"/>
	<xsl:variable name="dta_id" select="$criteria[@entity='BIO$DTA$SELECTION']/value[masterkey=$dta_type]/key"/>
	<xsl:variable name="seperate-analytematrix" select="$attributes/seperate_dta_by_analytematrix='T'"/>
	<!--xsl:variable name="columns_screen" select="utd:formats($experiment,'cols')[@type='SCREEN'][not(@name=$hide_cols)]"/>
	<xsl:variable name="columns_confirm" select="utd:formats($experiment,'cols')[@type='CONFIRM'][not(@name=$hide_cols)]"/>
	<xsl:variable name="columns_titer" select="utd:formats($experiment,'cols')[@type='TITER'][not(@name=$hide_cols)]"/>
	<xsl:variable name="columns_conc" select="utd:formats($experiment,'cols')[@type='CONC'][not(@name=$hide_cols)]"/-->
	<xsl:variable name="column_width" select="utd:formats($experiment,('cols','width'))"/>
	<xsl:variable name="column_group" select="utd:formats($experiment,('cols','group'))"/>
	<xsl:variable name="hide_stats" select="for $s in utd:formats($experiment,'var')[@type='OPTSTAT'] return if ($attributes/*[name() = $s] != 'T') then $s/@key else ()"/>
	<xsl:variable name="stats_individual" select="utd:formats($experiment,'calc')[@type='INDIVIDUAL'][not(@name=$hide_stats)]"/>
	<xsl:variable name="stats_overall" select="utd:formats($experiment,'calc')[@type='OVERALL'][not(@name=$hide_stats)]"/>
	<xsl:variable name="maxlines" select="$attributes/*[name() = concat( 'max_lines_', lower-case($experiment) ) ]"/>
	<xsl:variable name="maxcols" select="$attributes/*[name() = concat( 'max_cols_', lower-case($experiment) ) ]"/>
	<xsl:variable name="excel-filename" select="utd:formats($experiment,'var')[@name='EXCEL_FILENAME']"/>
	<xsl:variable name="section-title" select="utd:formats($experiment,'var')[@name='SECTION_TITLE']"/>
	<!-- other -->
	<xsl:variable name="calculations" select="$study/calculations/experiment[@name=$experiment and @type='dtaresults_'||$reporttype]"/>
	<xsl:variable name="columns" select="$attributes/dtafields/field[@targetid=$dta_type][not(field=$hide_cols)][@columncode=($calculations/calculation/values/sample/field/name)]"/>
	<xsl:variable name="show_caption" select="$attributes/dta_show_caption"/>
	<!--xsl:variable name="calculations_screen" select="$study/calculations/experiment[@name=$experiment and @type='screenresult']"/>
	<xsl:variable name="calculations_confirm" select="$study/calculations/experiment[@name=$experiment and @type='confirmresult']"/>
	<xsl:variable name="calculations_titer" select="$study/calculations/experiment[@name=$experiment and @type='titerresult']"/>
	<xsl:variable name="calculations_conc" select="$study/calculations/experiment[@name=$experiment and @type='concresult']"/>
	<xsl:variable name="calculations" select="$study/calculations/experiment[@name=$experiment and @type=('screenresult', 'confirmresult', 'titerresult', 'concresult')]"/-->
	<!--
==========================================================================
Main template
========================================================================== -->
	<xsl:template match="/">
		<xsl:variable name="isr-sections">
			<isr-sections emptytable="{false()}" experiment="{$experiment}">
				<xsl:attribute name="title"><xsl:text>ISR$BIO$COVIB$ADA$DTA.XSL - </xsl:text><xsl:value-of select="$experiment"/></xsl:attribute>
				<xsl:if test="$table-type">
					<xsl:attribute name="type" select="$table-type"/>
				</xsl:if>
				<xsl:if test="exists($calculations/calculation/values/sample/field) and $dta_id!=''">
					<xsl:variable name="analytes" as="node()*">
						<xsl:choose>
							<xsl:when test="$seperate-analytematrix">
								<xsl:for-each select="$calculations/calculation/target/analyteid">
									<xsl:copy-of select="."/>
								</xsl:for-each>
							</xsl:when>
							<xsl:otherwise>
								<analyteid/>
							</xsl:otherwise>
						</xsl:choose>
					</xsl:variable>
					<xsl:for-each select="distinct-values($analytes)">
						<xsl:variable name="analyteid" select="."/>
						<xsl:variable name="analyte" select="$study/analytes/analyte[@id=$analyteid]"/>
						<xsl:variable name="matrices" as="node()*">
							<xsl:choose>
								<xsl:when test="$seperate-analytematrix">
									<xsl:for-each select="$calculations/calculation/target[analyteid = $analyteid]/knownmatrix">
										<xsl:copy-of select="."/>
									</xsl:for-each>
								</xsl:when>
								<xsl:otherwise>
									<knownmatrix/>
								</xsl:otherwise>
							</xsl:choose>
						</xsl:variable>
						<xsl:for-each select="distinct-values($matrices)">
							<xsl:variable name="knownmatrix" select="current()"/>
							<xsl:variable name="section-params" as="node()*">
								<study>
									<xsl:value-of select="$study/name"/>
								</study>
								<xsl:if test="$analyte">
									<analyte-name>
										<xsl:value-of select="$analyte/name"/>
									</analyte-name>
								</xsl:if>
								<xsl:if test="$knownmatrix">
									<knownmatrix>
										<xsl:value-of select="$knownmatrix"/>
									</knownmatrix>
								</xsl:if>
							</xsl:variable>
							<isr-section>
								<xsl:attribute name="experiment" select="$experiment"/>
								<xsl:if test="$section-title">
								<xsl:attribute name="title" select="utd:params($section-title,$section-params)"/>
								</xsl:if>
								<xsl:if test="$excel-filename">
									<xsl:attribute name="filename" select="utd:params($excel-filename,$section-params)"/>
								</xsl:if>
								<xsl:if test="$table-type">
									<xsl:attribute name="type" select="$table-type"/>
								</xsl:if>
								<xsl:variable name="hashstring" select="concat( $study/@code, '_', $analyteid, '_', $knownmatrix )"/>
								<xsl:variable name="number" select="current()"/>
								<xsl:call-template name="createTable">
									<xsl:with-param name="hash" select="concat( $hash, '_', utd:hashcalc( $hashstring, 18 ) )"/>
									<xsl:with-param name="number" select="1"/>
									<xsl:with-param name="analyte" select="$analyte"/>
									<xsl:with-param name="knownmatrix" select="$knownmatrix"/>
								</xsl:call-template>
							</isr-section>
						</xsl:for-each>
					</xsl:for-each>
				</xsl:if>
			</isr-sections>
		</xsl:variable>
		<!--xsl:copy-of select="$isr-sections"/-->
		<xsl:apply-templates select="$isr-sections/isr-sections"/>
	</xsl:template>
	<!--
==========================================================================
createTable
========================================================================== -->
	<xsl:template name="createTable">
		<xsl:param name="hash"/>
		<xsl:param name="number"/>
		<xsl:param name="analyte"/>
		<xsl:param name="knownmatrix"/>
		<xsl:variable name="isr-parameters">
			<isr-parameters>
				<study>
					<xsl:value-of select="$study/name"/>
				</study>
				<analyte-name>
					<xsl:for-each select="distinct-values($analyte/name)">
						<xsl:choose>
							<xsl:when test="position() = last() and position() != 1"> and </xsl:when>
							<xsl:when test="position() != 1">, </xsl:when>
							<xsl:otherwise></xsl:otherwise>
						</xsl:choose>
						<xsl:value-of select="."/>
					</xsl:for-each>
				</analyte-name>
				<xsl:if test="$knownmatrix">
					<knownmatrix>
						<xsl:value-of select="$knownmatrix"/>
					</knownmatrix>
				</xsl:if>
				<!--conc-unit>
					<xsl:value-of select="$concentrationunit"/>
				</conc-unit-->
				<response-unit>
					<xsl:value-of select="$response_unit"/>
				</response-unit>
			</isr-parameters>
		</xsl:variable>
		<!-- CAPTION -->
		<xsl:variable name="isr-caption">
			<!--xsl:if test="$caption-type != 'none'"-->
			<isr-caption>
				<xsl:attribute name="type">listed</xsl:attribute>
				<xsl:if test="$caption-type">
					<xsl:attribute name="type" select="$caption-type"/>
				</xsl:if>
				<xsl:if test="$caption-type = 'none' and $show_caption = 'T'">
					<xsl:attribute name="type">noref</xsl:attribute>
				</xsl:if>
				<xsl:attribute name="hash" select="$hash"/>
				<xsl:attribute name="ref" select="$hash"/>
				<xsl:attribute name="number" select="$number"/>
				<xsl:if test="$caption-prefix">
					<xsl:attribute name="prefix" select="$caption-prefix"/>
				</xsl:if>
				<xsl:if test="$caption-sequence">
					<xsl:attribute name="seq" select="$caption-sequence"/>
				</xsl:if>
				<title>
					<xsl:value-of select="$table-caption"/>
				</title>
				<parameters/>
			</isr-caption>
			<!--/xsl:if-->
		</xsl:variable>
		<!-- TABLE -->
		<xsl:variable name="isr-table">
			<isr-table type="standard">
				<xsl:if test="not(empty($maxlines))">
					<xsl:attribute name="maxlines" select="$maxlines"/>
				</xsl:if>
				<headers>
					<header calcwidth="true">
						<xsl:for-each select="$columns">
							<head class="borderTopBottom">
								<xsl:if test="width and width!=''">
									<xsl:attribute name="width" select="width"/>
								</xsl:if>
								<xsl:attribute name="excel-type">text</xsl:attribute>
								<xsl:value-of select="name"/>
							</head>
						</xsl:for-each>
					</header>
				</headers>
				<rows>
					<xsl:for-each select="$calculations/calculation[not(exists($analyte)) or target/analyteid=$analyte/@id][not(empty($knownmatrix)) or target/knownmatrix=$knownmatrix]">
						<xsl:variable name="spos" select="position()"/>
						<xsl:variable name="slast" select="last()"/>
						<xsl:variable name="sample" select="values/sample"/>
						<row bracket="{$sample/designsampleid}">
							<xsl:for-each select="$columns">
								<xsl:variable name="field" select="$sample/field[name=current()/@columncode]"/>
								<xsl:variable name="name" select="tokenize($field/name,'@')[1]"/>
								<cell>
									<xsl:if test="exists($column_group[@name=current()/name])">
										<xsl:attribute name="group" select="$column_group[@name=current()/name]"/>
									</xsl:if>
									<xsl:attribute name="class" select=" 'borderBottom' "/>
									<xsl:call-template name="utd:excelAttributes">
										<xsl:with-param name="input" select="current()"/>
									</xsl:call-template>
									<xsl:choose>
										<xsl:when test="$columns[@name=current()/name]/@type = 'NUMBER'">
											<xsl:attribute name="excel-type">number</xsl:attribute>
										</xsl:when>
										<xsl:when test="$columns[@name=current()/name]/@type = 'DATE'">
											<xsl:attribute name="excel-type">text</xsl:attribute>
										</xsl:when>
										<xsl:otherwise>
											<xsl:attribute name="excel-type">text</xsl:attribute>
										</xsl:otherwise>
									</xsl:choose>
									<xsl:choose>
										<xsl:when test="$name = ('IR Result Text', 'IR Result Number') and $sample/titerflag='T' and $attributes/titer_result_below1_footnote != ''">
											<xsl:attribute name="footnote"><xsl:value-of select="$attributes/titer_result_below1_footnote"/></xsl:attribute>
											<xsl:value-of select="$field/value"/>
										</xsl:when>
										<xsl:when test="$name = ('Concentration') and $sample/concflag!=''">
											<xsl:value-of select="$sample/concflag"/>
										</xsl:when>
										<xsl:otherwise>
											<xsl:value-of select="$field/value"/>
										</xsl:otherwise>
									</xsl:choose>
								</cell>
							</xsl:for-each>
						</row>
					</xsl:for-each>
				</rows>
			</isr-table>
		</xsl:variable>
		<!-- NOTES -->
		<xsl:variable name="isr-notes">
			<isr-notes>
				<xsl:for-each select="utd:formats($experiment,'footer')[starts-with(@name, 'LINE')]">
					<note>
						<xsl:value-of select="."/>
					</note>
				</xsl:for-each>
			</isr-notes>
		</xsl:variable>
		<!-- PAGE -->
		<xsl:variable name="isr-page">
			<isr-page>
				<xsl:attribute name="createCap" select="true()"/>
				<xsl:attribute name="number" select="$number"/>
				<xsl:attribute name="hash" select="$hash"/>
				<xsl:copy-of select="$isr-parameters"/>
				<xsl:copy-of select="$isr-caption"/>
				<xsl:copy-of select="$isr-table"/>
				<xsl:copy-of select="$isr-notes"/>
			</isr-page>
		</xsl:variable>
		<xsl:copy-of select="$isr-page"/>
	</xsl:template>
	<!--
==========================================================================
parameters
==========================================================================-->
	<xsl:template name="parameters">
		<xsl:param name="sample"/>
		<param name="DOSEGROUP">
			<xsl:value-of select="$sample/dosegroup"/>
		</param>
		<param name="SUBJECT">
			<xsl:value-of select="$sample/subject"/>
		</param>
		<param name="TREATMENT">
			<xsl:value-of select="$sample/treatment"/>
		</param>
		<param name="YEAR">
			<xsl:value-of select="$sample/year"/>
		</param>
		<param name="MONTH">
			<xsl:value-of select="$sample/month"/>
		</param>
		<param name="WEEK">
			<xsl:value-of select="$sample/week"/>
		</param>
		<param name="STUDYDAY">
			<xsl:value-of select="$sample/studyday"/>
		</param>
		<param name="PERIOD">
			<xsl:value-of select="$sample/period"/>
		</param>
		<param name="VISIT">
			<xsl:value-of select="$sample/visit"/>
		</param>
		<param name="ENDDAY">
			<xsl:value-of select="$sample/endday"/>
		</param>
		<param name="ENDHOUR">
			<xsl:value-of select="$sample/endhour"/>
		</param>
		<param name="ENDMINUTE">
			<xsl:value-of select="$sample/endminute"/>
		</param>
		<param name="TIMETEXT">
			<xsl:value-of select="$sample/timetext"/>
		</param>
		<param name="SPLITNUMBER">
			<xsl:value-of select="$sample/splitnumber"/>
		</param>
		<param name="PERIOD">
			<xsl:value-of select="$sample/period"/>
		</param>
		<param name="VISIT">
			<xsl:value-of select="$sample/visit"/>
		</param>
		<param name="SAMPLINGDATETIME">
			<xsl:value-of select="$sample/samplingdatetime"/>
		</param>
		<param name="CONCENTRATION">
			<xsl:value-of select="$sample/concentration"/>
		</param>
		<param name="CONCENTRATIONSTATUS">
			<xsl:value-of select="$sample/concentrationstatus"/>
		</param>
		<param name="CONCENTRATIONUNIT">
			<xsl:value-of select="$sample/concentrationunit"/>
		</param>
		<param name="RESULTCOMMENT">
			<xsl:value-of select="$sample/resultcomment"/>
		</param>
		<param name="RUNID">
			<xsl:value-of select="$sample/runno"/>
		</param>
		<param name="ANALYTE">
			<xsl:value-of select="$sample/analyte"/>
		</param>
		<param name="DILUTION">
			<xsl:value-of select="$sample/dilution"/>
		</param>
		<param name="SAMPLERECEIPT">
			<xsl:value-of select="$sample/samplereceipt"/>
		</param>
		<param name="ASSAYDATETIME">
			<xsl:value-of select="$sample/assaydatetime"/>
		</param>
		<param name="ASSAY">
			<xsl:value-of select="$sample/assay"/>
		</param>
	</xsl:template>
</xsl:stylesheet>
