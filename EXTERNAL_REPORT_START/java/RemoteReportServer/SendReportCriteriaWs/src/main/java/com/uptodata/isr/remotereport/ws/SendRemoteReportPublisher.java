package com.uptodata.isr.remotereport.ws;

import com.uptodata.isr.observers.base.dbAccess.DbData;
import com.uptodata.isr.observers.base.secutity.CryptDbPropertiesHandler;
import com.uptodata.isr.server.utils.logging.log4j2.LogHelper;
import com.uptodata.isr.server.utils.network.UrlUtil;
import com.uptodata.isr.utils.ConvertUtils;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.util.Properties;

/**
 * Created by smiljanskii60 on 22.07.2015.
 */
public class SendRemoteReportPublisher {

    static Logger log = LogManager.getLogger("SendRemoteReportPublisher");

    /*
    "wsUrl=http://10.0.6.188:5099/ws/remote/isr/uptodata/com"
"logLevel=DEBUG"
"dbSchema=ISROWNER35_ISRC"
"dbHost = isrc-i2-d-d"
"dbWsPort = 8082"
"dbWsUser = isrwebservices_isrc"
"dbWsPassword =isrwebservices_isrc"
"jdbcUrl=jdbc:oracle:thin:@isrc-i2-d-d:1521:ora11204"
"password=isrowner35_isrc"
     */
    public static void main(String[] args) {
        try {
            Properties props = ConvertUtils.convertArrayToProperties(args);

            String logLevel = props.getProperty("logLevel");
            String wsUrl = props.getProperty("wsUrl");

            DbData dbData = new DbData(props);
            CryptDbPropertiesHandler.saveDbPropertiesForLog(args);

            log = LogHelper.initLogger(logLevel, "java SendRemoteReportPublisher");
            log.info(ConvertUtils.propertiesWithPasswordToString(props));
            SendRemoteReportService inter = new SendRemoteReportService(dbData, logLevel, null);

            UrlUtil.createAndPublishEndpoint(wsUrl, inter);
            log.info("Service 'SendRemoteReportPublisher' is started on '" + wsUrl + "'");

        } catch (Exception e) {
            if (log != null) {
                log.error(e);
            }
            e.printStackTrace();
        }
    }
}
