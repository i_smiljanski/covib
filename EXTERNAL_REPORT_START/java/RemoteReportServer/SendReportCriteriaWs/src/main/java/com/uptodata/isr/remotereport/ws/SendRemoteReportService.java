package com.uptodata.isr.remotereport.ws;

import com.uptodata.isr.observer.transfer.FileTransferInterface;
import com.uptodata.isr.observers.base.dbAccess.DbData;
import com.uptodata.isr.observers.base.secutity.CryptDbPropertiesHandler;
import com.uptodata.isr.observers.childServer.ObserverChildServerImpl;
import com.uptodata.isr.server.utils.logging.log4j2.IsrServerLogger;
import com.uptodata.isr.server.utils.logging.log4j2.LogHelper;
import com.uptodata.isr.server.utils.network.UrlUtil;
import com.uptodata.isr.utils.ConvertUtils;
import com.uptodata.isr.utils.exceptions.MessageStackException;
import com.uptodata.isr.utils.xml.dom.XmlHandler;
import org.apache.commons.codec.binary.Base64;
import org.apache.commons.collections4.map.CaseInsensitiveMap;
import org.apache.commons.io.IOUtils;
import org.apache.commons.lang.StringUtils;
import org.apache.logging.log4j.Logger;

import javax.jws.WebMethod;
import javax.jws.WebService;
import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.net.MalformedURLException;
import java.net.URISyntaxException;
import java.net.URL;
import java.nio.charset.Charset;
import java.nio.charset.StandardCharsets;
import java.util.Properties;
import java.util.concurrent.*;

/**
 * Created by smiljanskii60 on 22.07.2015.
 */
@WebService(name = "SendRemoteReportInterface", targetNamespace = "http://ws.remotereport.isr.uptodata.com/", endpointInterface = "com.uptodata.isr.remotereport.ws.SendRemoteReportService")
public class SendRemoteReportService extends ObserverChildServerImpl implements FileTransferInterface {
    DbData dbData;

    public static final String dbServlet = "remotereport";

    public SendRemoteReportService() {
    }

    public SendRemoteReportService(DbData dbData, String logLevel, String LogReference) {
        this.dbData = dbData;
        init(logLevel, LogReference);
    }

    private void init(String logLevel, String LogReference) {
        log = initLogger(logLevel, LogReference);
        statusXmlMethod = 0;
        statusXml = initStatusXml();
    }


    public String sendReportCriteria(String xmlAsBase64, String logLevel, String logReference) throws MessageStackException {
        String result = "F";
        try {
            String user = getUserName(xmlAsBase64);
            String loaderDataAsBase64 = checkUserAndGetLoaderData(user);
//            log.debug("loaderDataAsBase64=" + loaderDataAsBase64);
            String errorAsBase64 = getError(loaderDataAsBase64);
            log.debug("error: " + errorAsBase64);
            if (StringUtils.isEmpty(errorAsBase64)) {
                byte[] xmlBytes = Base64.decodeBase64(loaderDataAsBase64);
                if (xmlBytes != null) {
                    log.debug(new String(xmlBytes));
                }
                XmlHandler loaderDataHandler = new XmlHandler(xmlBytes);
                String port = loaderDataHandler.nodeRead("//PORT");
                String host = loaderDataHandler.nodeRead("//HOST");
                ExecutorService executor = Executors.newCachedThreadPool();
                Future<String> futureCall = executor.submit(new Callable<String>() {
                    @Override
                    public String call() throws Exception {
                        try {
                            String result = sendReportCriteriaToServlet(xmlAsBase64, host, port, logLevel, logReference);
                            return result;
                        } catch (MessageStackException e) {
                            log.error(e);
                            throw e;
                        }
                    }
                });
                result = futureCall.get();
            }
        } catch (ExecutionException | InterruptedException e) {
            String userStack = "error on the server " + e;
            onMethodFailed(e, MessageStackException.SEVERITY_CRITICAL, userStack,
                    MessageStackException.getCurrentMethod(), statusXmlMethod);
        }
        return result;
    }

    private String getUserName(String xmlAsBase64) {
        byte[] xmlBytes = Base64.decodeBase64(xmlAsBase64);
        XmlHandler xmlHandler = new XmlHandler(xmlBytes);
        String user = xmlHandler.nodeRead("//USER");
        return user;
    }

    private String getError(String loaderDataAsBase64) {
        String error = null;
        byte[] loaderDataBytes = Base64.decodeBase64(loaderDataAsBase64);
        if (loaderDataBytes != null) {
            error = new String(loaderDataBytes, Charset.forName(ConvertUtils.encoding));
        }
        try {
            XmlHandler loaderDataHandler = new XmlHandler(loaderDataBytes);
            String errorAsBase64 = loaderDataHandler.nodeRead("//ERROR");
            if (errorAsBase64 != null) {
                byte[] errorAsBytes = Base64.decodeBase64(errorAsBase64);
                if (errorAsBytes != null) {
                    error = new String(errorAsBytes, Charset.forName("UTF-8"));
                }
            }
        } catch (Exception e) {
            log.error(e);
            error = error + " " + e;
        }
        return error;
    }

    private String sendReportCriteriaToServlet(String xmlAsBase64, String host, String port,
                                               String logLevel, String logReference) throws MessageStackException {
        IsrServerLogger logger = initLogger(logLevel, logReference);
        logger.stat(METHOD_BEGIN);
        try {
            byte[] base64Bytes = xmlAsBase64.getBytes(StandardCharsets.UTF_8);
            byte[] xmlBytes = Base64.decodeBase64(base64Bytes);

            CaseInsensitiveMap<String, String> args = new CaseInsensitiveMap<>();
//            args.put("criteria xml", new String(xmlBytes, "utf-8"));
            args.put("host", host);
            args.put("port", port);
            createMethodNode("sendReportCriteria", args);

            String resulrt = sendCriteriaToDb(xmlBytes, "processReport", args, logLevel, logReference);

            logger.stat(METHOD_END);
            return resulrt;
        } catch (Exception e) {
            String userStack = "error on the server " + e;
            onMethodFailed(e, MessageStackException.SEVERITY_CRITICAL, userStack,
                    MessageStackException.getCurrentMethod(), statusXmlMethod);
            return "false";
        }
    }


    private String checkUserAndGetLoaderData(String user) throws MessageStackException  {
        String methodName = "checkUser";
        log.debug("method name: " + methodName + ", user " + user);
        try {
            CaseInsensitiveMap<String, String> params = new CaseInsensitiveMap<>();
            params.put("user", user);
            URL urlStr = dbData.buildDbServletUrl(dbServlet, methodName, params);
            log.debug(urlStr);
//            dbData.doAuthenticate();
            InputStream is = UrlUtil.getRequestFromServletAsIs(urlStr);
            String result = null;
            if (is != null) {
                log.debug("before toString");
                result = IOUtils.toString(is, "UTF-8");
                log.debug(result);
            }
            return result;
        } catch (IOException e) {
            String errorMsg = "Error by  check user. Error " + e;
            log.debug(errorMsg);
            throw new MessageStackException(e, MessageStackException.SEVERITY_CRITICAL, errorMsg,
                    MessageStackException.getCurrentMethod());
        }
    }

    @WebMethod(exclude = true)
    public String sendCriteriaToDb(byte[] resultFile, String function, CaseInsensitiveMap<String, String> params,
                                    String logLevel, String logReference) throws Exception {
        IsrServerLogger log = initLogger(logLevel, logReference);
        log.debug("getUrlForUploadFile: function = " + function);
        String fileName = "criteria.xml";
        InputStream inputStream = null;
        try {
            URL urlStr = dbData.buildDbServletUrl(dbServlet, function, params);
            log.debug(urlStr);
//            dbData.doAuthenticate();
            inputStream = new ByteArrayInputStream(resultFile);
            String result = uploadFileWithResult(fileName, inputStream, urlStr,  logLevel, logReference);
            return result;
        } catch (Exception e) {
            String errorMsg = "Can not upload file '" + fileName + "' " + e;
//            log.debug(errorMsg);
            throw e;
//            throw new MessageStackException(e, MessageStackException.SEVERITY_CRITICAL, errorMsg,
//                    MessageStackException.getCurrentMethod());
        } finally {
            if (inputStream != null) {
                try {
                    inputStream.close();
                } catch (IOException e) {
                    throw new MessageStackException(e, MessageStackException.SEVERITY_CRITICAL, "stream could't be closed",
                            MessageStackException.getCurrentMethod());
                }
            }
        }
    }

    public static void main(String[] args) {
        try {
            byte[] bytesXml = ConvertUtils.fileToBytes(System.getProperty("user.dir") + "\\test.xml");
            Properties props = ConvertUtils.convertArrayToProperties(args);
            String logLevel = "DEBUG";
            DbData dbData = new DbData(props);
            CryptDbPropertiesHandler.saveDbPropertiesForLog(args);

            Logger log = LogHelper.initLogger(logLevel, "test");
            log.info(ConvertUtils.propertiesWithPasswordToString(props));
            SendRemoteReportService reportService = new SendRemoteReportService(dbData, logLevel, null);

            reportService.sendReportCriteria(Base64.encodeBase64String(bytesXml), logLevel, "test");
        } catch (Exception e) {
            e.printStackTrace();
        }

    }
}
