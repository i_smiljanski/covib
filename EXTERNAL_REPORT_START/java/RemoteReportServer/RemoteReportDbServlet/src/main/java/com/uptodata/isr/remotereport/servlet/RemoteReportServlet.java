package com.uptodata.isr.remotereport.servlet;

import com.uptodata.isr.db.connection.ConnectionUtil;
import com.uptodata.isr.db.dbservlets.ServletUtil;
import com.uptodata.isr.db.trace.DbLogging;
import com.uptodata.isr.db.transfer.fileupload.StreamFileItemFactory;
import com.uptodata.isr.utils.ConvertUtils;
import com.uptodata.isr.utils.xml.dom.XmlHandler;
import oracle.jdbc.OracleCallableStatement;
import oracle.jdbc.OracleTypes;
import org.apache.commons.codec.binary.Base64;
import org.apache.commons.fileupload.FileItem;
import org.apache.commons.fileupload.FileItemFactory;
import org.apache.commons.fileupload.servlet.ServletFileUpload;
import org.apache.commons.lang.StringUtils;
import org.w3c.dom.Element;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.sql.Clob;
import java.sql.Connection;
import java.util.List;

/**
 * Created by smiljanskii60 on 20.07.2015.
 */
public class RemoteReportServlet extends ServletUtil {
    String errorMsg;
    int errorCode = HttpServletResponse.SC_INTERNAL_SERVER_ERROR;

    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        DbLogging log = new DbLogging();
        String servletPath = req.getRequestURI();
        log.stat("begin", "servletPath = '" + servletPath + "'");
        try {
            if (checkServletPath(servletPath, "processReport")) {
                String host = getRequestParameter(req, "host");
                String port = getRequestParameter(req, "port");
                if (!StringUtils.isNumeric(port)) {
                    throw new Exception("port " + port + " is not numeric!");
                }
                String ret = sendReportCriteria(req, host, Integer.parseInt(port));
                resp.getWriter().println(ret);

            } else if (checkServletPath(servletPath, "checkUser")) {
                String user = getRequestParameter(req, "user");
                String loaderData = checkUser(user);
                resp.getWriter().println(loaderData);
            } else {
                errorMsg = "servlet path " + servletPath + " is unknown!";
                errorCode = HttpServletResponse.SC_METHOD_NOT_ALLOWED;  //?
                resp.getWriter().println(errorMsg);
                throw new Exception(errorMsg);
            }
            log.stat("end", "end");
        } catch (Exception e) {
            log.error("error", e);
//            e.printStackTrace();
            resp.setStatus(errorCode);
            resp.getWriter().println(errorMsg + e);
        } finally {
            resp.getWriter().close();
        }

    }

    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        doPost(req, resp);
    }

    private String sendReportCriteria(HttpServletRequest req, String host, int port) throws Exception {
        String ret = null;
        DbLogging log = new DbLogging();
        FileItemFactory factory = new StreamFileItemFactory();
        try {
            List<FileItem> files = new ServletFileUpload(factory).parseRequest(req);
            FileItem fileItem = files.get(0);
            byte[] bytes = fileItem.get();
            log.info("file length=", "" + bytes.length);
            Connection conn = ConnectionUtil.getDefaultConnection();

                /*PreparedStatement ps = conn.prepareStatement("select USERENV ('SESSIONID') from dual");
                ResultSet rs = ps.executeQuery();
                rs.next();
                sessionId = rs.getString(1);

                log.debug("sessionid", sessionId);
                rs.close();
                ps.close();*/

            OracleCallableStatement statement = (OracleCallableStatement) conn.prepareCall(
                    "begin ISR$ISS$PACKAGE.CreateInterfaceReport(?, ?, ?, ?); end; ");
            statement.setClob(1, ConvertUtils.getClob(conn, bytes));
            statement.setString(2, host);
            statement.setInt(3, port);
            statement.registerOutParameter(4, java.sql.Types.CLOB);
            statement.executeQuery();

            Clob retMsg = statement.getCLOB(4);
            if (retMsg != null) {
                byte[] errBytes = ConvertUtils.getBytes(retMsg);
                ret = new String(errBytes, ConvertUtils.encoding);
                log.error("error", ret);
//            throw new Exception(error);
            }

        } catch (Exception e) {
            log.error("error", e);
            throw e;
        }
        return ret;
    }


    private String checkUser(String user) throws Exception {
        String loaderData = null;
        DbLogging log = new DbLogging();
        log.stat("begin", "user=" + user);
        OracleCallableStatement statement = null;
        Connection conn = ConnectionUtil.getDefaultConnection();
        log.debug("conn", conn);
        statement = (OracleCallableStatement) conn.prepareCall(
                "begin ISR$ISS$PACKAGE.checkUser(?, ?, ?, ?); end; ");
        statement.setString(1, user);
        statement.registerOutParameter(2, OracleTypes.VARCHAR);
        statement.registerOutParameter(3, OracleTypes.NUMBER);
        statement.registerOutParameter(4, java.sql.Types.CLOB);
        statement.executeQuery();

        String host = statement.getString(2);
        int port = statement.getInt(3);
        Clob errorCl = statement.getCLOB(4);
        String error = null;
        if (errorCl != null) {
            log.debug("errorCl", errorCl);
            byte[] errorXml = ConvertUtils.getBytes(errorCl);
            error = Base64.encodeBase64String(errorXml);
        }
        log.debug("before createLoaderDataXml", host + " " + port);
        String loaderXml = createLoaderDataXml(host, String.valueOf(port), error);
        loaderData = Base64.encodeBase64String(loaderXml.getBytes());
        log.debug("loaderData", loaderData);

        log.stat("end", loaderXml);
        return loaderData;
    }


    private String createLoaderDataXml(String host, String port, String error) {

        String loaderData;
        XmlHandler xmlHandler = new XmlHandler();
        Element root = xmlHandler.nodeRoot("LOADER");
        xmlHandler.createElement("HOST", host, null, root);
        xmlHandler.createElement("PORT", port, null, root);
        xmlHandler.createElement("ERROR", error, null, root);
        loaderData = xmlHandler.xmlToString();
        return loaderData;

    }


    public static void main(String[] args) {
        RemoteReportServlet servlet = new RemoteReportServlet();
        String answ = servlet.createLoaderDataXml(null, "1", "");
        System.out.println("answ = " + answ);
    }
}
