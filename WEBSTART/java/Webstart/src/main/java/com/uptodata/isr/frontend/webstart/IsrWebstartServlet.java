package com.uptodata.isr.frontend.webstart;

import com.uptodata.isr.utils.ConvertUtils;
import com.uptodata.isr.utils.db.ConnectionUtil;
import oracle.jdbc.OracleCallableStatement;
import oracle.jdbc.OracleDriver;
import oracle.jdbc.OracleTypes;
import oracle.sql.ANYDATA;
import oracle.sql.Datum;
import oracle.sql.TypeDescriptor;
import org.apache.commons.io.IOUtils;
import org.apache.commons.io.output.FileWriterWithEncoding;
import org.apache.commons.lang.StringUtils;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.io.Writer;
import java.net.FileNameMap;
import java.net.URLConnection;
import java.sql.Blob;
import java.sql.Clob;
import java.sql.Connection;
import java.sql.Types;

public class IsrWebstartServlet extends HttpServlet {
    Connection conn;

    public IsrWebstartServlet(Connection conn) {
        this.conn = conn;
    }

    @Override
    public void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        String baseUrl = request.getHeader("host");
        String servletUri = request.getRequestURI();

        try {
            String fileName = getRequestParameter(request, "file", true);
            String fileVersion = getRequestParameter(request, "version", false);
            System.out.println("fileName = " + fileName + " fileVersion = " + fileVersion);

            String isTest = getRequestParameter(request, "test", false);
            if ("true".equalsIgnoreCase(isTest)) {
                downloadFile(fileName, response, baseUrl, servletUri);
            } else {
                downloadFromDb(fileName, fileVersion, response, baseUrl, servletUri);
            }
        } catch (Exception e) {
            String error = "error:" + e;
            System.out.println(error);
            response.getWriter().print(error);
        }
    }


    private String getRequestParameter(HttpServletRequest request, String paramName, boolean required) throws Exception {
       /* for (Enumeration en = request.getParameterNames(); en.hasMoreElements(); ) {
            String reqParamName = (String) en.nextElement();
            if (reqParamName.equalsIgnoreCase(parameter)) {
                return request.getParameter(reqParamName);
            }
        }*/
        String paramValue = request.getParameter(paramName);
        if (required && StringUtils.isEmpty(paramValue)) {
            throw new Exception("request parameter " + paramName + " is null!");
        }
        return paramValue;
    }

    private void downloadFile(String fileName, HttpServletResponse response, String baseUrl, String servletUri) throws Exception {
        String mimeType = null;
        boolean isTextFile = true;
        if (fileName.endsWith("jar")) {
            mimeType = "application/java-archive";
            isTextFile = false;
        } else if (fileName.endsWith("jnlp")) {
            mimeType = "application/x-java-jnlp-file";
        }
        if (fileName.endsWith("gif")) {
            isTextFile = false;
        }
        mimeType = getMimeTypeIfNull(mimeType, fileName);
        InputStream inStream = getClass().getClassLoader().getResourceAsStream(fileName);
        if (isTextFile) {
            writeBytesToResponse(IOUtils.toByteArray(inStream), fileName, mimeType, baseUrl, servletUri, response);
        } else {
            writeInputStreamToResponse(inStream, fileName, inStream.available(), mimeType, response);
        }
    }


    private void downloadFromDb(String fileName, String fileVersion, HttpServletResponse response, String baseUrl, String servletUri) throws Exception {
        String mimeType = null;
        if (conn == null) {
            conn = new OracleDriver().defaultConnection();
        }
        ANYDATA anydata = null;
        try {
            OracleCallableStatement statement = (OracleCallableStatement) conn.prepareCall(
                    "begin ? := isr$webstart$service.getFileFromDb(?, ?, ?, ?); end; ");
            statement.registerOutParameter(1, OracleTypes.OPAQUE, "SYS.ANYDATA");
            statement.setString(2, fileName);
            statement.setString(3, fileVersion);
            statement.registerOutParameter(4, Types.VARCHAR);
            statement.registerOutParameter(5, Types.VARCHAR);
            statement.executeQuery();

            mimeType = statement.getString(4);
            String error = statement.getString(5);
            if (StringUtils.isNotEmpty(error)) {
                throw new Exception(error);
            }

            anydata = (ANYDATA) statement.getObject(1);
            ConnectionUtil.closeStatement(statement);
        } catch (Exception e) {
            e.printStackTrace();
        }
        if (anydata != null) {
            TypeDescriptor typeDesc = anydata.getTypeDescriptor();
            Datum embeddedDatum = anydata.accessDatum();
            if (embeddedDatum == null) {
                throw new Exception("file '" + fileName + "' from database is empty");
            }
            byte[] bytes = null;
            InputStream inputStream = null;
            int fileLength = 0;

            if (typeDesc.getTypeCode() == TypeDescriptor.TYPECODE_BLOB) {
                Blob blobFile = (Blob) embeddedDatum;
                inputStream = blobFile.getBinaryStream();
                fileLength = (int) blobFile.length();
            } else if (typeDesc.getTypeCode() == TypeDescriptor.TYPECODE_CLOB) {
                Clob clobFile = (Clob) embeddedDatum;
                bytes = ConvertUtils.getBytes(clobFile);
            } else {
                throw new Exception("Type Code of ANYDATA object (" + typeDesc.getTypeCode() + ") is unknown; fileName = '"
                        + fileName + "', fileVersion = '" + fileVersion + "'");
            }
            mimeType = getMimeTypeIfNull(mimeType, fileName);
            if (bytes != null) {
                writeBytesToResponse(bytes, fileName, mimeType, baseUrl, servletUri, response);
            } else if (inputStream != null) {
                writeInputStreamToResponse(inputStream, fileName, fileLength, mimeType, response);
            }

        } else {
            throw new Exception("ANYDATA object  is null; fileName = '" + fileName + "', fileVersion = '" + fileVersion + "'");

        }
    }


    private void writeBytesToResponse(byte[] bytes, String fileName, String mimeType, String baseUrl, String servletUri, HttpServletResponse response)
            throws IOException {
        System.out.println("IsrWebstartServlet.writeBytesToResponse");
        Writer writer;
        String str;
        str = ConvertUtils.convertByteToString(bytes);
        String replaced = str.replace("$$codebase", baseUrl).replace("$$servlet_uri", servletUri);
        if (response == null) {  //for test with main{}
            writer = new FileWriterWithEncoding("test_" + fileName, ConvertUtils.encoding);
        } else {
            response.setContentLength(replaced.length());
            response.setContentType(mimeType);
            // writes the file to the client
            writer = response.getWriter();
        }
        ConvertUtils.writeStringToWriter(replaced, writer);
        writer.flush();
        writer.close();
        System.out.println("writeBytesToResponse end");
    }


    private void writeInputStreamToResponse(InputStream inputStream, String fileName, int fileLength, String mimeType, HttpServletResponse response)
            throws Exception {
        System.out.println("IsrWebstartServlet.writeInputStreamToResponse: " + fileName + "; " + fileLength);
        byte[] buffer = new byte[1024];
        OutputStream outStream;

        response.setContentLength(fileLength);
        response.setContentType(mimeType);
        response.setHeader("Content-disposition", "attachment;filename=" + fileName);
        outStream = response.getOutputStream();
        int bytesRead;
        int allReadBytes = 0;
        while ((bytesRead = inputStream.read(buffer)) != -1) {
            outStream.write(buffer, 0, bytesRead);
            allReadBytes = allReadBytes + bytesRead;
        }
        if (allReadBytes > 0) {
            System.out.println("allReadBytes = " + allReadBytes);
        } else {
            throw new Exception("check the file, the amount of read bytes is 0!");
        }
        inputStream.close();
        outStream.flush();
        outStream.close();
        System.out.println("writeInputStreamToResponse end");
    }

    private String getMimeTypeIfNull(String mimeType, String fileName) {
        if (StringUtils.isEmpty(mimeType)) {
            FileNameMap fileNameMap = URLConnection.getFileNameMap();
            mimeType = fileNameMap.getContentTypeFor(fileName);
            if (StringUtils.isEmpty(mimeType)) {
                mimeType = "application/octet-stream; charset=UTF-8";
            }
        }
        System.out.println("mimeType = " + mimeType);
        return mimeType;
    }

    public static void main(String[] args) {
        FileNameMap fileNameMap = URLConnection.getFileNameMap();
        String mimeType = fileNameMap.getContentTypeFor("alert.gif");
        System.out.println("mimeType = " + mimeType);
       /* try {
            Connection conn = DriverManager.getConnection(
                    "jdbc:oracle:thin:@isrc-d-d12:1521:ora12201", "isrowner_isrs", "isrowner_isrs");

            IsrWebstartServlet servlet = new IsrWebstartServlet();
            servlet.download("index.html", "", null, "isrc-d-d12:8082", "/isr/webstart/isrowner_isrs", conn);
        } catch (Exception e) {
            e.printStackTrace();
        }*/

    }
}
