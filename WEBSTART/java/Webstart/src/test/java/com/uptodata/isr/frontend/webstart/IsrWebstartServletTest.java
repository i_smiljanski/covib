package com.uptodata.isr.frontend.webstart;

import org.junit.Before;
import org.junit.Test;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;

import javax.servlet.ServletOutputStream;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.PrintWriter;
import java.io.StringWriter;
import java.sql.Connection;
import java.sql.DriverManager;

import static org.junit.Assert.assertTrue;

public class IsrWebstartServletTest extends Mockito {

    @Mock
    private HttpServletRequest request;
    @Mock
    private HttpServletResponse response;
    private ByteArrayOutputStream outputStream;

    @Before
    public void setUp() {
        MockitoAnnotations.initMocks(this);
    }

    @Test
    public void testServlet1() throws Exception {
        String host = "localhost:8080";
        String requestUri = "/testIsrWebstart/sr/webstart";
        IsrWebstartServlet servlet = new IsrWebstartServlet(null);

        when(request.getParameter("file")).thenReturn("isr.jnlp");
        when(request.getParameter("test")).thenReturn("true");
        when(request.getHeader("host")).thenReturn(host);
        when(request.getRequestURI()).thenReturn(requestUri);

        StringWriter stringWriter = new StringWriter();
        PrintWriter writer = new PrintWriter(stringWriter);
        when(response.getWriter()).thenReturn(writer);

        outputStream = new ByteArrayOutputStream();
        when(response.getOutputStream()).thenReturn(new ServletOutputStream() {
            @Override
            public void write(int b) throws IOException {
                outputStream.write(b);
            }
        });

        servlet.doGet(request, response);
        writer.flush(); // it may not have been flushed yet...
//        System.out.println("stringWriter = " + stringWriter);
        assertTrue(stringWriter.toString().contains("jnlp codebase=\"http://" + host));
        assertTrue(stringWriter.toString().contains("servlet_uri=\"" + requestUri));
    }


    @Test
    public void testServlet2() throws Exception {
        String host = "isrc-d-d12:8082";
        String requestUri = "/isr/webstart/isrowner_isrs";
        Connection conn = DriverManager.getConnection(
                "jdbc:oracle:thin:@isrc-d-d12:1521:ora12201", "isrowner_isrs", "isrowner_isrs");
        IsrWebstartServlet servlet = new IsrWebstartServlet(conn);

        when(request.getParameter("file")).thenReturn("isr.jnlp");
        when(request.getParameter("test")).thenReturn("false");
        when(request.getHeader("host")).thenReturn(host);
        when(request.getRequestURI()).thenReturn(requestUri);

        StringWriter stringWriter = new StringWriter();
        PrintWriter writer = new PrintWriter(stringWriter);
        when(response.getWriter()).thenReturn(writer);

        outputStream = new ByteArrayOutputStream();
        when(response.getOutputStream()).thenReturn(new ServletOutputStream() {
            @Override
            public void write(int b) throws IOException {
                outputStream.write(b);
            }
        });

        servlet.doGet(request, response);
        writer.flush();
        System.out.println("stringWriter = " + stringWriter);
        assertTrue(stringWriter.toString().contains("jnlp codebase=\"http://" + host));
        assertTrue(stringWriter.toString().contains("servlet_uri=\"" + requestUri));
    }


}
