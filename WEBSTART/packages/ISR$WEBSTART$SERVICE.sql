CREATE OR REPLACE package isr$webstart$service is
-- ***************************************************************************************************
-- Description/Usage:
-- The package handles all tasks related to the Webstart functionality
-- ***************************************************************************************************

procedure home;
-- ***************************************************************************************************
-- Date und Autor: 11 Oct 2018  HSP
-- Redirects to "files/index.html", needs the document-path setting of DBMS_EPG to be set to "files"
--
-- ***************************************************************************************************

--*********************************************************************************************************************************
procedure servlet;
-- ***************************************************************************************************
-- Date und Autor: 11 Oct 2018  HSP
-- Handles all file requests from browser and sends the requested file.
-- It doesn't take any parameter directly, instead OWA_UTIL is used to figure out what was requested and with parameters (for now only "version" is expected, but optional)
--
-- ***************************************************************************************************

end isr$webstart$service;