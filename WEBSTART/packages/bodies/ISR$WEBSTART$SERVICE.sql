CREATE OR REPLACE package body ISR$WEBSTART$SERVICE
is

--*********************************************************************************************************************************
  function clobToBlob ( cIn IN CLOB ) return BLOB
  is
    sCurrentName   CONSTANT VARCHAR2(100) := $$PLSQL_UNIT || '.clobToBlob';
    
    bOut BLOB;
    iDstOff   PLS_INTEGER := 1;
    iSrcOff   PLS_INTEGER := 1;
    iCtx      PLS_INTEGER := 0;
    iWarning  PLS_INTEGER;
  begin
    isr$trace.stat('begin', 'begin', sCurrentName);
    DBMS_LOB.createTemporary(bOut, true);
    DBMS_LOB.convertToBlob(bOut, cIn, LENGTH(cIn), iDstOff, iSrcOff, 0, iCtx, iWarning);
    isr$trace.stat('end', 'end', sCurrentName);
    return bOut;
  end clobToBlob;
  
--*********************************************************************************************************************************
  function getVarNum (csName IN VARCHAR2) return NUMBER
  is
  begin
    return TO_NUMBER(REGEXP_SUBSTR( '&'||OWA_UTIL.GET_CGI_ENV('QUERY_STRING')||'&', '&'||csName||'=(\d+)&', subexpression => 1));
  end getVarNum;

--*********************************************************************************************************************************
  function getVarString (csName IN VARCHAR2, csPattern IN VARCHAR2) return VARCHAR2
  is
  begin
    return REGEXP_SUBSTR( '&'||OWA_UTIL.GET_CGI_ENV('QUERY_STRING')||'&', '&'||csName||'=('||csPattern||')&', subexpression => 1);
  end getVarString;
  

--*********************************************************************************************************************************
  procedure replaceParams ( cFile IN OUT CLOB )
  is
    sCurrentName   CONSTANT VARCHAR2(100) := $$PLSQL_UNIT || '.replaceParams';
  begin
    isr$trace.stat('begin', 'begin', sCurrentName);
    -- Replace http_host and request_protocol seperately, to allow seperate configuration
    cFile := REPLACE(cFile,'&&http_host&',OWA_UTIL.GET_CGI_ENV('HTTP_HOST'));
    cFile := REPLACE(cFile,'&&request_protocol&',lower(OWA_UTIL.GET_CGI_ENV('REQUEST_PROTOCOL')));
    -- Provide environment variables as substitution parameters
    for i in 1..OWA.NUM_CGI_VARS loop
      cFile := REPLACE(cFile,'&&'||lower(owa.cgi_var_name(i))|| '&',owa.cgi_var_val(i));
    end loop;

    cFile := REPLACE(cFile,'&&request-uri&',OWA_UTIL.GET_CGI_ENV('SCRIPT_NAME'));
    -- Sytem parameter substitutions
    cFile := REPLACE(cFile,'&&isr-system&',STB$UTIL.getSystemParameter('ISR_SYSTEM'));
    isr$trace.stat('end', 'end', sCurrentName);
  end replaceParams;

--*********************************************************************************************************************************
  procedure httpError ( csErrorCode IN NUMBER, csErrorMessage IN VARCHAR2 )
  is
    sCurrentName CONSTANT VARCHAR2(100) := $$PLSQL_UNIT || '.httpError';
    sMimeType    ISR$FRONTEND$FILE.MIMETYPE%type;
    sFileVersion ISR$FRONTEND$FILE.FILE_VERSION%type;
    bFile        BLOB;
    cFile        CLOB;
    
    cursor cGetFile
    is
      select mimetype,
             textfile
       from isr$frontend$file
      where filename = 'error.html'; 
  begin
    isr$trace.stat('begin', 'begin', sCurrentName);
    open cGetFile;
    fetch cGetFile into sMimeType, cFile;

    if cGetFile%NOTFOUND then
      -- No error.html found, show a minimalistic error page instead
      close cGetFile;
      -- MimeType in the HTTP header
      OWA_UTIL.MIME_HEADER('text/html', FALSE);
      -- Set the header response status to the error-code with the error-messahe and close the header
      OWA_UTIL.STATUS_LINE(csErrorCode, csErrorMessage);
      HTP.P(csErrorCode||' - '||csErrorMessage);
      isr$trace.error('Webstart Error', '"error.html" is missing from ISR$FRONTEND$FILE.', sCurrentName);
      return;
    end if;
    
    cFile := REPLACE(cFile,'&&error-code&', csErrorCode);
    cFile := REPLACE(cFile,'&&error-message&', csErrorMessage);

    bFile := clobToBlob(cFile);
    
    -- MimeType in the HTTP header
    OWA_UTIL.MIME_HEADER(sMimeType, FALSE);
    -- Set the header response status to the error-code with the error-messahe and close the header
    OWA_UTIL.STATUS_LINE(csErrorCode, csErrorMessage);
    WPG_DOCLOAD.DOWNLOAD_FILE(bFile);
    isr$trace.stat('end', 'end', sCurrentName);
  end httpError;

--*********************************************************************************************************************************
  procedure home
  is
    sCurrentName   CONSTANT VARCHAR2(100) := $$PLSQL_UNIT || '.home';
  begin
    isr$trace.stat('begin', 'begin', sCurrentName);
    -- redirect to the index file
    OWA_UTIL.REDIRECT_URL('files/index.html');
    isr$trace.stat('end', 'end', sCurrentName);
  end home;

--*********************************************************************************************************************************

  procedure servlet
  is
    sCurrentName CONSTANT VARCHAR2(100) := $$PLSQL_UNIT || '.servlet';
    sFileName    ISR$FRONTEND$FILE.FILENAME%type;
    sMimeType    ISR$FRONTEND$FILE.MIMETYPE%type;
    sFileVersion ISR$FRONTEND$FILE.FILE_VERSION%type;
    bFile        BLOB;
    cFile        CLOB;

    cursor cGetFile ( csFileName IN ISR$FRONTEND$FILE.FILENAME%type )
    is
      select mimetype,
             textfile,
             binaryfile
       from isr$frontend$file
      where '/' || filename = csFileName
        and NVL(file_version,'-1') =  NVL(sFileVersion,'-1');
  begin
    isr$trace.stat('begin', 'begin', sCurrentName);
    -- Check query for "version" parameter
    sFileVersion := getVarNum('version');
    isr$trace.debug('QueryValue(version)', 'version: '||NVL(sFileVersion,'NULL'), sCurrentName);
    
    sFileName := OWA_UTIL.GET_CGI_ENV('PATH_INFO');
    
    -- Legacy path style support
    if sFileName = '/' then
      sFileName := '/'|| getVarString('file', '(\w|\d|\.)+');
    end if;

    open cGetFile(sFileName);
    fetch cGetFile into sMimeType, cFile, bFile;

    if cGetFile%NOTFOUND or sFilename = '/error.html' then
      close cGetFile;
      isr$trace.warn('Webstart Warning', 'FileName: '||sFileName||', FileVersion'||sFileVersion, sCurrentName);
      httpError(404,'File not found');
      return;
    end if;
    close cGetFile;
    
    if cFile is not NULL then
      replaceParams(cFile);
      bFile := clobToBlob(cFile);
    end if; 
    
    -- MimeType in the HTTP header
    OWA_UTIL.MIME_HEADER(sMimeType, FALSE);
    if sMimeType like 'image/%' then
      OWA_CACHE.SET_EXPIRES(1440, 'USER');
      -- Set header to tell client to cache the file up to 1 Day
      HTP.P('Cache-Control: public, max-age=86400');
    end if;
    OWA_UTIL.HTTP_HEADER_CLOSE;
    WPG_DOCLOAD.DOWNLOAD_FILE(bFile);
    isr$trace.stat('end', 'end', sCurrentName);
  exception
    when others then
      isr$trace.error('Webstart Error', 'FileName: '||sFileName||', FileVersion'||sFileVersion, sCurrentName);
      isr$trace.error('Webstart Error', 'Error: '||SQLERRM, sCurrentName);
      httpError(500,'An unknown issue occured');
  end servlet;
end isr$webstart$service;