CREATE OR REPLACE TYPE ISR$SERVER$COLLECTOR$REC force UNDER ISR$SERVER$RECORD (
constructor function ISR$SERVER$COLLECTOR$REC(self in out nocopy ISR$SERVER$COLLECTOR$REC) return self as result,
static function getCurrentEntities ( sTable    in   varchar2,  olpopertylist in out stb$property$list) return stb$oerror$record ,
-- ***************************************************************************************************
static function checkDependenciesOnMask( oParameter    in     STB$MENUENTRY$RECORD,
                                  olParameter   in out STB$PROPERTY$LIST,
                                  sModifiedFlag out    varchar2,
                                  sTable    in   varchar2  default 'ISR$SERVER$REMOTE$JDBC$V') return STB$OERROR$RECORD,
-- ***************************************************************************************************
static function callFunction( oParameter in STB$MENUENTRY$RECORD,
                       olNodeList out STB$TREENODELIST, sTable    in   varchar2 )  return STB$OERROR$RECORD,
-- ***************************************************************************************************
static function putParameters( oParameter  in STB$MENUENTRY$RECORD,
                        olParameter in STB$PROPERTY$LIST,
                        sTable    in   varchar2,
                        clParameter in clob  default null ) return STB$OERROR$RECORD,
-- ***************************************************************************************************
member procedure writeCriteriaCondition(csParentEntity in varchar2, csParentAttr in varchar2, csChildAttr in varchar2, nRepId in integer, xDBRelParam in out xmltype),
-- ***************************************************************************************************
member function putRowForRemoteEntity(csTransferKeyId in varchar2,
                                                csEntity      in  varchar2,
                                                cnRepID       in  number ) return STB$OERROR$RECORD,
-- ***************************************************************************************************
member function fillTmpTableForEntityMember( csEntity    in  varchar2,
                      coParamList in  isr$ParamList$Rec,
                      csWizard    in  varchar2,
                      csMasterKey in  varchar2 default null,
                      nTimeout in number,
                      nServertypeId in number ) return STB$OERROR$RECORD,
-- ***************************************************************************************************
member function getInputXml( csEntity    in  varchar2,
                      csWizard    in  varchar2 default 'F',
                      csMasterKey in  varchar2 default null,
                      nServertypeId in number,
                      sTransferId in varchar2,
                      coParamList in  iSR$ParamList$Rec,
                      clXml       out clob )  return STB$OERROR$RECORD ,
-- ***************************************************************************************************
member function getInputXmlServerPart( csEntity      in varchar2,
                                  cnSeverTypeID in number,
                                  cnRepID       in number,
                                  csTransferId  in varchar2,
                                  xPart         out xmltype ) return STB$OERROR$RECORD
-- ***************************************************************************************************
-- Date und Autor: 28 Feb 2015  HR
-- Create XML information for remote site (server etc)
--
-- Parameter:
-- csEntity      the entity
-- cnSeverTypeID the server type
-- cnRepID       the report ID
--
-- Return:
-- STB$OERROR$RECORD
--
-- ***************************************************************************************************
, member function getInputXmlDataPart (csDataEntity    in  varchar2,
                             csCritEntity    in  varchar2,
                             csWizard    in  varchar2 default 'F',
                             csMasterKey in  varchar2 default null,
                             coParamList in  iSR$ParamList$Rec)
  return xmltype
-- ***************************************************************************************************
, member function getInputXmlConditionPart (csEntity    in  varchar2)
  return xmltype
-- ***************************************************************************************************
-- Date und Autor: 8 Nov 2017  HR
-- Create XML information for additional conditions
--
-- Parameter:
-- csEntity      the entity
--
-- Return: xmltype
--
-- ***************************************************************************************************
, member function getInputXmlOrderExpr(csEntity    in  varchar2) return varchar2
--**********************************************************************************************************************
, member function saveTmpRemoteData(csFileTransferId in varchar2) return STB$OERROR$RECORD
--**********************************************************************************************************************
, member function deleteTmpData(csFileTransferId in varchar2) return STB$OERROR$RECORD
--**********************************************************************************************************************
, static function deleteTmpDataForSession(cnSessionId in number) return STB$OERROR$RECORD
--******************************************************************************
, static function fillTmpTable ( csXml IN CLOB, csEntity  IN VARCHAR2, csLoglevel  IN VARCHAR2,  csLogReference  IN VARCHAR2, nTimeout in NUMBER ) RETURN CLOB
--**********************************************************************************************************************
, static function insertXmlIntoTable ( csEntity  IN VARCHAR2, csXml IN CLOB, csDatepattern  IN VARCHAR2) RETURN CLOB
) not final