CREATE OR REPLACE TYPE BODY ISR$SERVER$COLLECTOR$REC as

constructor function ISR$SERVER$COLLECTOR$REC(self in out nocopy ISR$SERVER$COLLECTOR$REC) return self as result is
begin
  return;
end;
--******************************************************************************
static function getCurrentEntities( sTable    in   varchar2, olpopertylist in out stb$property$list )
  return stb$oerror$record
is
  sCurrentName constant varchar2(100) := $$PLSQL_UNIT||'.getCurrentEntities';
  oErrorObj    stb$oerror$record      := STB$OERROR$RECORD();
  sMsg         varchar2(4000);

  TYPE tGetEntities IS REF CURSOR;
  cGetEntities      tGetEntities;
  sEntitiesStmt  varchar2(4000);

  sEntityName  isr$meta$entity$targetmodel.entityname%type;
  sIsselected  varchar2(1);

begin
  isr$trace.stat ('begin',  'begin', sCurrentName);
  sEntitiesStmt := 'select me.entityname, NVL ( ( select ''T''
                                  from   isr$server$entity$mapping
                                  where  entityname = me.entityname
                                  and    serverid = s.serverid), ''F'') isselected
    from   isr$meta$entity$targetmodel me, ' || sTable || ' s
    where    s.serverid = ' || STB$OBJECT.getCurrentNodeId || '
    and    s.targetmodelid = me.targetmodelid
    order by me.entityname';
    isr$trace.debug ('sEntitiesStmt',  sEntitiesStmt, sCurrentName);
  open cGetEntities for sEntitiesStmt;
  if cGetEntities %FOUND then
    olpopertylist.extend;
    olpopertylist (olpopertylist.count) := STB$PROPERTY$RECORD();
    olpopertylist (olpopertylist.count).sparameter := 'TAB_ENTITIES';
    olpopertylist (olpopertylist.count).spromptdisplay := Utd$msglib.GetMsg ('TAB_ENTITIES',Stb$security.GetCurrentLanguage);
    olpopertylist (olpopertylist.count).sdisplayed := 'T';
    olpopertylist (olpopertylist.count).type := 'TABSHEET';
  end if;
  loop
   fetch cGetEntities into sEntityName, sIsselected;
   exit when cGetEntities%NOTFOUND;
  --for rGetEntities in cGetEntities loop

    olpopertylist.extend;
    olpopertylist (olpopertylist.count) := STB$PROPERTY$RECORD();
    olpopertylist (olpopertylist.count).sparameter := sEntityName;
    olpopertylist (olpopertylist.count).spromptdisplay := sEntityName;
    olpopertylist (olpopertylist.count).svalue := sIsselected;
    olpopertylist (olpopertylist.count).sdisplay := sIsselected;
    olpopertylist (olpopertylist.count).seditable := 'T';
    olpopertylist (olpopertylist.count).srequired := 'T';
    olpopertylist (olpopertylist.count).sdisplayed := 'T';
    olpopertylist (olpopertylist.count).haslov := 'F';
    olpopertylist (olpopertylist.count).type := 'BOOLEAN';
  end loop;
  close cGetEntities;

  isr$trace.stat ('end', 'end', sCurrentName);
  return oErrorObj;
exception
  when others then
    sMsg :=  utd$msglib.getmsg ('exUnexpectedError', stb$security.getcurrentlanguage, csP1 => sqlerrm(sqlcode));
    oErrorObj.handleError ( Stb$typedef.cnSeverityCritical, sMsg, sCurrentName,
                            dbms_utility.format_error_stack ||stb$typedef.crlf|| dbms_utility.format_error_backtrace  );
    return oErrorObj;
end getCurrentEntities;

--******************************************************************************
static function checkDependenciesOnMask( oParameter    in     STB$MENUENTRY$RECORD,
                                  olParameter   in out STB$PROPERTY$LIST,
                                  sModifiedFlag out    varchar2,
                                  sTable    in   varchar2 default 'ISR$SERVER$REMOTE$JDBC$V' ) return STB$OERROR$RECORD is
begin
  return isr$server$record.checkDependOnMaskForTable(oParameter, olParameter, sModifiedFlag, sTable);
end checkDependenciesOnMask;

-- ***************************************************************************************************
static function callfunction( oParameter in  STB$MENUENTRY$RECORD,
                              olNodeList out STB$TREENODELIST, sTable    in   varchar2 )  return STB$OERROR$RECORD
is

  sCurrentName           varchar2(100) := $$PLSQL_UNIT||'.callFunction ('||oParameter.sToken||')';
  oErrorObj              STB$OERROR$RECORD := STB$OERROR$RECORD();
  sMsg                   varchar2(4000);
  sMenuToken             varchar2(500) := Upper (oParameter.sToken);

  sCheckForDialog        varchar2(1);
  sModifiedFlag          char(1) := 'F';

  nServerId  number;

  cursor cGetServer is
   select s.serverid, s.alias, s.runstatus, s.classname, s.context, s.namespace, s.observer_port observerPort,
            s.observer_host observerHost,  s.host,  s.port,  s.server_path, s.servertypeid, s.targetmodelid, s.timeout
    from  isr$serverobserver$v s
    where  s.serverid = nServerId;

  nCountServerInCrit  number;
  exCritServerExist  exception;

begin
  isr$trace.stat('begin','parameter: ' || sMenuToken,  sCurrentName);
  isr$trace.debug('value', 'oParameter',sCurrentName, oParameter);
  isr$trace.debug('status','current node',sCurrentName, Stb$object.getCurrentNode);

  sCheckForDialog := 'F';
  stb$object.setCurrentToken( oParameter.stoken );
  olNodeList := STB$TREENODELIST();

  nServerId := STB$OBJECT.getCurrentNodeid;

  case

    when  Upper (oParameter.sToken) = 'CAN_MAP_SERVER' then
      olNodeList.extend;
      olNodeList (1) := STB$TREENODE$RECORD(STB$UTIL.getMaskTitle(Upper (oParameter.sToken)),  $$PLSQL_UNIT, 'TRANSPORT', 'MASK', '');
      oErrorObj := getCurrentEntities(sTable, olNodeList (1).tloPropertylist);

    when upper (sMenuToken) = 'CAN_DELETE_SERVER' then
      select count(*) into nCountServerInCrit from isr$crit where entity = 'SERVER' and key = nServerId;
      if nCountServerInCrit > 0 then
        sMsg := utd$msglib.getmsg('exCritServerExistText', stb$security.getcurrentlanguage, nServerId );
        ISR$TREE$PACKAGE.selectCurrent;
        raise exCritServerExist;
      end if;
      oErrorObj :=  isr$server$record.callFunction( oParameter,  olNodeList, sTable);

    when  Upper (oParameter.sToken) in ('CAN_DISPLAY_SERVER_LOG_LIST') then

      olNodeList.extend;
      olNodeList (1) := STB$TREENODE$RECORD(STB$UTIL.getMaskTitle(Upper (oParameter.sToken)), $$PLSQL_UNIT, 'TRANSPORT', 'MASK', '');
      oErrorObj := STB$UTIL.getPropertyList(oParameter.sToken, olNodeList (1).tloPropertylist);
      oErrorObj := checkDependenciesOnMask(oParameter, olNodeList (1).tloPropertylist, sModifiedFlag, sTable);

    else
       oErrorObj :=  isr$server$record.callfunction( oParameter,  olNodeList, sTable);
  end case;
  if nvl(sCheckForDialog, 'F') = 'T' then
    oErrorObj.sErrorCode := Utd$msglib.GetMsg('exNotExists',Stb$security.GetCurrentLanguage) ;
  end if;
  isr$trace.debug('value','olNodeList',sCurrentName,olNodeList);
  isr$trace.stat ('end', 'end', sCurrentName);
  return oErrorObj;
exception
  when exCritServerExist then
    oErrorObj.handleError ( Stb$typedef.cnSeverityWarning, sMsg, sCurrentName,
                            dbms_utility.format_error_stack ||stb$typedef.crlf|| dbms_utility.format_error_backtrace  );
    return oErrorObj;
  when others then
    sMsg :=  utd$msglib.getmsg ('exUnexpectedError', stb$security.getcurrentlanguage, csP1 => sqlerrm(sqlcode));
    oErrorObj.handleError ( Stb$typedef.cnSeverityCritical, sMsg, sCurrentName,
                            dbms_utility.format_error_stack ||stb$typedef.crlf|| dbms_utility.format_error_backtrace  );
    return oErrorObj;
end callfunction;

-- ***************************************************************************************************
static function putParameters( oParameter  in STB$MENUENTRY$RECORD,
                        olParameter in STB$PROPERTY$LIST,
                        sTable    in   varchar2,
                        clParameter in clob default null ) return STB$OERROR$RECORD
is
  exTokenNotExists exception;
  sCurrentName     constant varchar2(100) := $$PLSQL_UNIT||'.putParameters ('||oParameter.sToken||')';
  oErrorObj        STB$OERROR$RECORD      := STB$OERROR$RECORD();
  sMsg             varchar2(4000);

  type tGetTargetModel is REF CURSOR;
  cGetTargetModel      tGetTargetModel;
  sTargetModelStmt  varchar2(4000);

  sOldTargetModelId     isr$meta$entity$targetmodel.targetmodelid%type;
  sNewTargetModelId     isr$meta$entity$targetmodel.targetmodelid%type;

  sInsertStmt  varchar2(4000);


begin
  isr$trace.stat(oParameter.sToken || ' begin', 'begin', sCurrentName);
  isr$trace.debug('olParameter', 's. logclob', sCurrentName, olParameter);
  STB$OBJECT.setCurrentToken(oparameter.stoken);
  case
    -- ================== modify (one key)
    when   Upper (oParameter.sToken) = 'CAN_MODIFY_SERVER' then
       oErrorObj :=  isr$server$record.putParameters( oParameter,  olParameter, clParameter);

       --for user dialog properties 'Use server..'
        merge into isr$dialog d
        using (select utd$msglib.getmsg ('USE_SERVER_', stb$security.getcurrentlanguage)  || STB$UTIL.getProperty(olParameter, 'ALIAS') description,
              'CAN_MODIFY_USER_PARAMETER' TOKEN,
              'SERVER_'||STB$UTIL.getProperty(olParameter, 'SERVERID') PARAMETERNAME
              from dual) S
        on (d.token = s.token and d.parametername = s.parametername)
        when matched then update
          set DESCRIPTION = utd$msglib.getmsg ('USE_SERVER_', stb$security.getcurrentlanguage)
             || STB$UTIL.getProperty (olParameter, 'ALIAS')
        when not matched then insert (PARAMETERNAME
                        , PARAMETERVALUE
                        , DESCRIPTION
                        , PARAMETERTYPE
                        , HASLOV
                        , ORDERNO
                        , EDITABLE
                        , REQUIRED
                        , TOKEN
                        , DISPLAYED)
        values ( 'SERVER_' || STB$UTIL.getProperty (olParameter, 'SERVERID')
           , ':exec:SELECT parametervalue
                      FROM (SELECT parametervalue
                              FROM stb$userparameter
                             WHERE userno = CASE WHEN UPPER (STB$OBJECT.getCurrentToken) = ''CAN_MODIFY_OWN_USER_DATA'' THEN STB$SECURITY.getCurrentUser ELSE TO_NUMBER(STB$OBJECT.getCurrentNodeid) END
                               AND parametername = ''SERVER_'||STB$UTIL.getProperty (olParameter, 'SERVERID')||'''
                            UNION
                            SELECT ''T'' FROM DUAL)
                     WHERE ROWNUM = 1'
           , utd$msglib.getmsg ('USE_SERVER_', stb$security.getcurrentlanguage)
             || STB$UTIL.getProperty (olParameter, 'ALIAS')
           , 'BOOLEAN'
           , 'F'
           --, 100  ---- 6.Jun 2019 hr [ISRC-921]
           , STB$UTIL.getProperty (olParameter, 'SERVERID')
           , 'T'
           , 'T'
           , 'CAN_MODIFY_USER_PARAMETER'
           , 'T');
        isr$trace.debug('rows affected by merge', sql%rowcount, sCurrentName);

        --for create report dialog  -- 6.Jun 2019 hr modified insert-part for [ISRC-982]
        merge into isr$dialog d
        using (select utd$msglib.getmsg ('USE_SERVER_IN_WIZARD_', stb$security.getcurrentlanguage) || STB$UTIL.getProperty(olParameter, 'ALIAS') description, 'CAN_CREATE_NEW_REPORT' TOKEN,  'SERVER_'||STB$UTIL.getProperty(olParameter, 'SERVERID') PARAMETERNAME from dual) S
        on (d.token = s.token and d.parametername = s.parametername)
        when matched then update
          set DESCRIPTION = utd$msglib.getmsg ('USE_SERVER_IN_WIZARD_', stb$security.getcurrentlanguage) || STB$UTIL.getProperty (olParameter, 'ALIAS')
        when not matched then insert (PARAMETERNAME
                        , PARAMETERVALUE
                        , DESCRIPTION
                        , PARAMETERTYPE
                        , HASLOV
                        , ORDERNO
                        , EDITABLE
                        , REQUIRED
                        , TOKEN
                        , DISPLAYED)
        values ( 'SERVER_' || STB$UTIL.getProperty (olParameter, 'SERVERID')
                 , ':exec:SELECT NVL ( (SELECT distinct ''T''
                        FROM isr$crit, (select runstatus from isr$server where serverid = '''|| STB$UTIL.getProperty (olParameter, 'SERVERID')|| ''' and runstatus = ''T'')
                       WHERE entity = ''SERVER''
                         AND repid = STB$OBJECT.getCurrentNodeId
                         AND STB$OBJECT.getCurrentNodeType = ''REPORT''
                         AND key = '''|| STB$UTIL.getProperty (olParameter, 'SERVERID') || ''')
                  , (SELECT parametervalue
                       FROM stb$userparameter, (select runstatus from isr$server where serverid = '''|| STB$UTIL.getProperty (olParameter, 'SERVERID')|| ''' and runstatus = ''T'')
                      WHERE userno = STB$SECURITY.getCurrentUser
                      --  AND STB$OBJECT.getCurrentNodeType != ''REPORT''
                        AND parametername = ''SERVER_'|| STB$UTIL.getProperty (olParameter, 'SERVERID')|| ''')) from dual'
                 , utd$msglib.getmsg ('USE_SERVER_IN_WIZARD_', stb$security.getcurrentlanguage) || STB$UTIL.getProperty (olParameter, 'ALIAS')
                 , 'BOOLEAN'
                 , 'F'
                 --, 100  ---- 6.Jun 2019 hr [ISRC-921]
                 , STB$UTIL.getProperty (olParameter, 'SERVERID')
                 , 'T'
                 , 'T'
                 , 'CAN_CREATE_NEW_REPORT'
                 , 'T');
          isr$trace.debug('rows affected by merge', sql%rowcount, sCurrentName);

          merge into  STB$USERPARAMETER d
           using (select utd$msglib.getmsg ('SHOW_USER_PARAMETER', stb$security.getcurrentlanguage) || utd$msglib.getmsg ('USE_SERVER_IN_WIZARD_', stb$security.getcurrentlanguage) || STB$UTIL.getProperty(olParameter, 'ALIAS') description,
               'SERVER_'||STB$UTIL.getProperty(olParameter, 'SERVERID') PARAMETERNAME, userno from stb$user) S
          on ( d.parametername = s.parametername)
          when not matched then insert (USERNO, PARAMETERNAME, PARAMETERVALUE)
           values( s.userno, 'SERVER_'||STB$UTIL.getProperty(olParameter, 'SERVERID'), 'T');
         isr$trace.debug('rows affected by merge', sql%rowcount, sCurrentName);

         sTargetModelStmt := 'select targetmodelid
                              from   ' || sTable || '
                              where  serverid = ' || stb$object.getcurrentnodeid;
         open cGetTargetModel for sTargetModelStmt;
         fetch cGetTargetModel into sOldTargetModelId;
         close cGetTargetModel;
         sNewTargetModelId :=  STB$UTIL.getProperty (olParameter, 'TARGETMODELID');
         isr$trace.debug('sNewTargetModelId', sNewTargetModelId, sCurrentName);
         isr$trace.debug('sOldTargetModelId', sOldTargetModelId, sCurrentName);

         if sOldTargetModelId  != sNewTargetModelId then    -- old != new
            delete from ISR$SERVER$ENTITY$MAPPING where  serverid = STB$OBJECT.getCurrentNodeId;
            for i in 1..olParameter.count() loop
              insert into ISR$SERVER$ENTITY$MAPPING( entityname, serverid, targetmodelid )
                     values (olParameter(i).sParameter,  stb$object.getcurrentnodeid, sNewTargetModelId );
            end loop;
          end if;
       -- commit;  --ISRC-1143, is
    when Upper (oParameter.sToken) = 'CAN_MAP_SERVER' then
      for i in 1..olParameter.count() loop
        delete from ISR$SERVER$ENTITY$MAPPING  where  serverid = STB$OBJECT.getCurrentNodeId and entityname = olParameter(i).sParameter;
        if SQL%ROWCOUNT > 0 then
         isr$trace.debug ('i= ' || i || ', delete SQL%ROWCOUNT for ' ||olParameter(i).sParameter,  SQL%ROWCOUNT,  sCurrentName);
        end if;

        if olParameter(i).sValue = 'T' then
          sInsertStmt := 'insert into ISR$SERVER$ENTITY$MAPPING( entityname, serverid, targetmodelid )
                   select ''' || olParameter(i).sParameter || ''', serverid, targetmodelid  from ' || sTable
                   || '  where  serverid = ' ||stb$object.getcurrentnodeid;
          isr$trace.debug ('sInsertStmt' , sInsertStmt,  sCurrentName);
          execute immediate sInsertStmt;
          isr$trace.debug ('i= ' || i, 'insert ' || SQL%ROWCOUNT || ' for ' ||olParameter(i).sParameter,  sCurrentName);
        end if;
      end loop;
     -- commit;

    else
        oErrorObj :=  isr$server$record.putParameters( oParameter,  olParameter, clParameter);
  end case;

--ISRC-1143
  if oErrorObj.sSeverityCode != Stb$typedef.cnAudit then
    commit;
  end if;

  isr$trace.stat (oParameter.sToken || ' end', 'end', sCurrentName);
  return oErrorObj;
exception
  when exTokenNotExists then
    sMsg :=  utd$msglib.getmsg ('exTokenNotExists', stb$security.getcurrentlanguage, upper(oParameter.sToken));
    oErrorObj.handleError ( Stb$typedef.cnSeverityCritical, sMsg, sCurrentName,
                            dbms_utility.format_error_stack ||stb$typedef.crlf|| dbms_utility.format_error_backtrace );
    return oErrorObj;
  when others then
    sMsg :=  utd$msglib.getmsg ('exUnexpectedError', stb$security.getcurrentlanguage, csP1 => sqlerrm(sqlcode));
    oErrorObj.handleError ( Stb$typedef.cnSeverityCritical, sMsg, sCurrentName,
                            dbms_utility.format_error_stack ||stb$typedef.crlf|| dbms_utility.format_error_backtrace  );
    return oErrorObj;
end putParameters;

-- ***************************************************************************************************
member procedure writeCriteriaCondition(csParentEntity in varchar2, csParentAttr in varchar2, csChildAttr in varchar2, nRepId in integer, xDBRelParam in out xmltype) is

  sCurrentName      constant varchar2(200) := $$PLSQL_UNIT||'.getInputXml#writeCriteriaCondition(' || csParentEntity || ')';
  oErrorObj              STB$OERROR$RECORD := STB$OERROR$RECORD();
  sMsg                   varchar2(4000);
  begin
    isr$trace.info('begin', 'begin', sCurrentName);
    isr$trace.debug('csParentEntity', csParentEntity, sCurrentName);
    isr$trace.debug('csParentAttr', csParentAttr, sCurrentName);
    isr$trace.debug('csChildAttr', csChildAttr, sCurrentName);

    select xmlconcat(xDBRelParam, (select xmlagg(xmlelement(evalname(csChildAttr),xmlcdata(key)))
    from (select distinct case (select  case when key = csParentAttr then 'KEY'
                        when display = csParentAttr then 'DISPLAY'
                        when info = csParentAttr then 'INFO'
                        else 'KEY'
                   end attrcol
              from isr$meta$crit
             where critentity = csParentEntity)
        when 'KEY' then c.key
        when 'DISPLAY' then c.display
        when 'INFO' then c.info
     end key
    from isr$crit c
    where c.repid = nRepID
    and c.entity = csParentEntity)))
    into xDBRelParam
    from dual;

    isr$trace.debug('xDBRelParam', 'see clobval =>', sCurrentName, xDBRelParam);
    isr$trace.info('end', 'end', sCurrentName);

exception
  when others then
    sMsg :=  utd$msglib.getmsg ('exUnexpectedError', stb$security.getcurrentlanguage, csP1 => sqlerrm(sqlcode));
    oErrorObj.handleError ( Stb$typedef.cnSeverityCritical, sMsg, sCurrentName,
                            dbms_utility.format_error_stack ||stb$typedef.crlf|| dbms_utility.format_error_backtrace  );
  end writeCriteriaCondition;

-- ***************************************************************************************************
member function fillTmpTableForEntityMember( csEntity    in  varchar2,
                      coParamList in  isr$ParamList$Rec,
                      csWizard    in  varchar2,
                      csMasterKey in  varchar2 default null,
                      nTimeout in number,
                      nServertypeId in number ) return STB$OERROR$RECORD
is
  sCurrentName    constant varchar2(100)     := $$PLSQL_UNIT||'.fillTmpTableForEntityMember';
  sMsg            varchar2(4000);
  exRemote        exception;
  oErrorObj       STB$OERROR$RECORD := STB$OERROR$RECORD();
  clInputXML      clob;
  sRemoteEx       clob;
  sTransferId       isr$transfer$remotedata$tmp.transferid%type;
  nRepID            constant integer := coParamList.GetParamNum('REPID');

  cursor cGetServerId is
  select s.serverid from isr$serverobserver$v s, isr$server$entity$mapping sem
   where s.serverid = sem.serverid
     and (sem.entityname = csEntity
          or (csWizard = 'T' and sem.entityname = (select entity from isr$meta$crit where critentity = csEntity)))     
     and s.servertypeid = nServertypeId
     and s.runstatus = 'T'
     and ( to_char(s.serverid) in (select key from isr$crit where entity = 'SERVER' and repid = nRepID)
          or nRepID is null)
     group by s.serverid, s.alias, servertypeid, observer_host, port, classname, context, namespace, timeout;

  procedure debugEntities(csEntity in varchar2) is
    sCurrentName    constant varchar2(100)     := $$PLSQL_UNIT||'.debugEntities';
    entityAttributesList CLOB;
    collectedDataXmlStatement CLOB;
    collectedDataXml XMLTYPE;
    cursor entityAttributes(csEntityParam in VARCHAR2) is
      select attrname
      from isr$meta$attribute attr
      where attr.entityname = csEntityParam
      order by orderno;
  begin
    for entityAttribute in entityAttributes(csEntity)
      loop
        entityAttributesList    := entityAttributesList || entityAttribute.attrname || ', ';
      end loop;

    collectedDataXmlStatement      :=
         q'{select xmlelement("ROWSET", xmlattributes('}'
      || csEntity
      || q'{' as "ENTITY"), xmlagg(xmlelement("ROW", xmlforest(}'
      || entityAttributesList
      || q'{null as "NULL")))) from TMP$}'
      || csEntity;

    execute immediate collectedDataXmlStatement into collectedDataXml;

    isr$trace.debug_all('log collected data for ' || csEntity || ' after fillTmpTable'
                      , 'collected data in clob column'
                      , sCurrentName
                      , collectedDataXml);
  exception
    when no_data_found
    then
      isr$trace.warn('log collected data for ' || csEntity || ' after fillTmpTable'
                      , 'failed to create statement for xml since no attributes were found for ' || csEntity
                      , sCurrentName);
    when others
    then
      isr$trace.warn('log collected data for ' || csEntity || ' after fillTmpTable'
                      , 'failed to log collected data for ' || csEntity || ': ' || sqlerrm
                      , sCurrentName);
  end debugEntities;

begin
  isr$trace.stat('begin','csEntity: '||csEntity, sCurrentName);

  select dbms_random.string('A', 8) into sTransferId from dual;
  for rGetServerId in cGetServerId loop
    oErrorObj := putRowForRemoteEntity(sTransferId || '_' || rGetServerId.serverid, csEntity, nRepID);
  end loop;

  oErrorObj := getInputXml(csEntity, csWizard, csMasterKey, nServertypeId, sTransferId, coParamList, clInputXML);

  sRemoteEx := isr$server$collector$rec.fillTmpTable ( clInputXML, csEntity, isr$trace.getJavaUserLoglevelStr, isr$trace.getReference, nTimeout) ;
  oErrorObj.checkRemoteException(sCurrentName, Stb$typedef.cnSeverityCritical, sRemoteEx);
  isr$trace.debug('oErrorObj', oErrorObj.sSeverityCode, sCurrentName, oErrorObj);
  if oErrorObj.isExceptionCritical then
    oErrorObj.handleJavaError (Stb$typedef.cnSeverityCritical, sRemoteEx, sCurrentName); 
    raise stb$typedef.exCritical;
  end if;

  for rGetServerId in cGetServerId loop
    oErrorObj := saveTmpRemoteData(sTransferId || '_' || rGetServerId.serverid);
    if oErrorObj.isExceptionCritical then
      isr$trace.error('oErrorObj', oErrorObj.sSeverityCode, sCurrentName, oErrorObj);
      raise stb$typedef.exCritical;
    end if;
  end loop;

  if isr$trace.getLoglevelStr(isr$trace.getloglevel) = 'DEBUG_ALL' THEN
    debugEntities(csEntity);
  end if;

  isr$trace.stat('end','csEntity: '||csEntity, sCurrentName);
  return oErrorObj;

exception
  when stb$typedef.exCritical then
    oErrorObj.sErrorMessage := oErrorObj.GetDevMessages;
    return oErrorObj;
  when others then
    sMsg :=  utd$msglib.getmsg ('exUnexpectedError', stb$security.getcurrentlanguage, csP1 => sqlerrm(sqlcode));
    oErrorObj.handleError ( Stb$typedef.cnSeverityCritical, sMsg, sCurrentName,
                            dbms_utility.format_error_stack ||stb$typedef.crlf|| dbms_utility.format_error_backtrace  );
    return oErrorObj;
end fillTmpTableForEntityMember;

 -- ***************************************************************************************************
member function getInputXmlDataPart (csDataEntity    in  varchar2,
                             csCritEntity    in  varchar2,
                             csWizard    in  varchar2 default 'F',
                             csMasterKey in  varchar2 default null,
                             coParamList in  iSR$ParamList$Rec)
  return xmltype
is
  sCurrentName constant varchar2(100) := $$PLSQL_UNIT||'.getInputXmlDataPart';
  oErrorObj             STB$OERROR$RECORD := STB$OERROR$RECORD();
begin
  isr$trace.stat(csDataEntity||' begin','csDataEntity: '||csDataEntity, sCurrentName);
  isr$trace.stat(csDataEntity||' end','csDataEntity: '||csDataEntity, sCurrentName);
  return null;
end getInputXmlDataPart;

 -- ***************************************************************************************************
member function getInputXmlConditionPart (csEntity    in  varchar2)
  return xmltype
is
  sCurrentName constant varchar2(100) := $$PLSQL_UNIT||'.getInputXmlConditionPart';
  oErrorObj             STB$OERROR$RECORD := STB$OERROR$RECORD();
begin
  isr$trace.stat('begin','csEntity: '||csEntity, sCurrentName);
  isr$trace.stat('end','csEntity: '||csEntity, sCurrentName);
  return null;
end getInputXmlConditionPart;

 -- ***************************************************************************************************
member function getInputXml( csEntity    in  varchar2,
                      csWizard    in  varchar2 default 'F',
                      csMasterKey in  varchar2 default null,
                      nServertypeId in number,
                      sTransferId in varchar2,
                      coParamList in  iSR$ParamList$Rec,
                      clXml       out clob )
  return STB$OERROR$RECORD
is

  -- the static function uses the select into clause many times without exception handling
  -- because aggregate static functions (like xmlagg) and xmlconcat result in exactly one row!

  exNoServerAvailable   exception;
  sCurrentName constant varchar2(200) := $$PLSQL_UNIT||'.getInputXml(' || csEntity || ')';
  oErrorObj             STB$OERROR$RECORD := STB$OERROR$RECORD();
  sMsg                  varchar2(4000);
  xXML                  xmltype;
  xInputXML             xmltype;
  xCondXML              xmltype;
  sTableName            varchar2(30);
  sBapiName             varchar2(30);

  -- gets the real entity (needed for crit entities)
  cursor cGetEntity is
    select NVL ( (select entity
                    from isr$meta$crit
                   where critentity = csEntity), entityname)
         , isrmanaged
         , timeout
         , sapentity
      from isr$meta$entity e
     where e.entityname = csEntity;

  sEntity       varchar2(500) := csEntity; --the real remote entity
  sTargetEntity varchar2(500) := csEntity; --the target entity can be different from sEntity for wizard entities
  sEntityType   varchar2(1); -- T isrmanaged, F non isrmanaged
  nTimeout      number;
  sOrderexpr    isr$meta$entity.orderexpr%type;

  exIsrmanagedNotSupported  exception;

  sDataEntity varchar2(50);  --wird vor aufruf gesetzt

  nRepID            constant integer := coParamList.GetParamNum('REPID');

begin
  isr$trace.stat(csEntity||' begin','csEntity: '||csEntity, sCurrentName);
  isr$trace.debug(csEntity||' csWizard',  csWizard, sCurrentName);
  isr$trace.debug(csEntity||' csMasterKey',  csMasterKey, sCurrentName);

  --replace predifined values sEntity with value from isr$meta$crit
  open cGetEntity;
  fetch cGetEntity into sEntity, sEntityType, nTimeout, sBapiName;
  isr$trace.debug(csEntity||' sEntity',  sEntity, sCurrentName);
  isr$trace.debug(csEntity||' sEntityType (isrmanaged)', sEntityType, sCurrentName);
  isr$trace.debug(csEntity||' nTimeout',  nTimeout, sCurrentName);
  close cGetEntity;
  sDataEntity := sEntity;

  oErrorObj := getInputXmlServerPart(sEntity, nServertypeId, nRepID, sTransferId, xXml);

  isr$trace.debug(sEntity||'_ServerPartOfInput.xml', 'see logclob -->', sCurrentName, xXml);

  -- serverabfrage (alle/mind. einer) steht noch aus!
  if xXml is null then
    raise exNoServerAvailable;
  end if;


  if sEntityType = 'F' then  -- T isrmanaged, F non isrmanaged
    -- support only for F non isrmanaged
    xInputXML := getInputXmlDataPart (sDataEntity, csEntity, csWizard, csMasterKey, coParamList);
    xCondXML  := getInputXmlConditionPart(csEntity); -- call it for the wizard entity
  else
    -- sEntityType != 'F'
    raise exIsrmanagedNotSupported;

  end if;

  sTableName :=  'ISR$' ||sEntity;

  select xmlroot(xmlelement("ISR_REMOTE_XML",
                 xmlattributes(sTableName as "tablename",
                               nTimeout as "timeout",
                               sTargetEntity as "entity",
                               sBapiName as "bapifunction",
                               STB$UTIL.getSystemParameter('TEMP_TAB_PREFIX') || sTargetEntity as "isrTableName"),
                 xmlconcat(xXml, xInputXML, xCondXML)),
       version '1.0" encoding="'||STB$UTIL.getSystemParameter('XML_ENCODING'))
  into xInputXML
  from dual;

  isr$trace.debug(sEntity||' xInputXML', 'before end', sCurrentName, xInputXML);

  clXML := xInputXML.getClobVal;

  isr$trace.debug(sEntity||'_clXML.xml', dbms_lob.getlength(clXML), sCurrentName, clXML);

  isr$trace.stat('end','sEntity: '||sEntity, sCurrentName);
  return oErrorObj;

exception
  when exNoServerAvailable then
    sMsg := Utd$msglib.GetMsg ('exNoServerAvailable', stb$security.getcurrentlanguage, sEntity);
    oErrorObj.handleError ( Stb$typedef.cnSeverityWarning, sMsg, sCurrentName,
                            dbms_utility.format_error_stack ||stb$typedef.crlf|| dbms_utility.format_error_backtrace  );
    return oErrorObj;
  when exIsrmanagedNotSupported then
    sMsg := Utd$msglib.GetMsg ('exIsrmanagedNotSupported', stb$security.getcurrentlanguage, csEntity);
    oErrorObj.handleError ( Stb$typedef.cnSeverityWarning, sMsg, sCurrentName,
                            dbms_utility.format_error_stack ||stb$typedef.crlf|| dbms_utility.format_error_backtrace  );
    return oErrorObj;
  when others then
    sMsg :=  utd$msglib.getmsg ('exUnexpectedError', stb$security.getcurrentlanguage, sqlerrm(sqlcode));
    oErrorObj.handleError ( Stb$typedef.cnSeverityCritical, sMsg, sCurrentName,
                            dbms_utility.format_error_stack ||stb$typedef.crlf|| dbms_utility.format_error_backtrace  );
    return oErrorObj;
end getInputXml;


member function putRowForRemoteEntity(csTransferKeyId in varchar2,
                                                csEntity      in  varchar2,
                                                cnRepID       in  number ) return STB$OERROR$RECORD is
  pragma autonomous_transaction;
  sCurrentName constant varchar2(100) := $$PLSQL_UNIT||'.putRowForRemoteEntity';
  oErrorObj             STB$OERROR$RECORD := STB$OERROR$RECORD();
  sMsg                  varchar2(4000);
begin
  isr$trace.stat('begin', 'begin', sCurrentName);
  insert into isr$transfer$remotedata$tmp (transferid, username, repid, entityname, sessionid)
  values (csTransferKeyId, stb$security.GetCurrentUser, cnRepID, csEntity, STB$SECURITY.getCurrentSession);
  commit;
  isr$trace.stat('end', 'end', sCurrentName);
  return oErrorObj;
exception
  when others then
    sMsg :=  utd$msglib.getmsg ('exInsertFailed', stb$security.getcurrentlanguage, sqlerrm(sqlcode),  'table = "isr$remote$data$temp", entityname ="' || csEntity || '"');
    oErrorObj.handleError ( Stb$typedef.cnSeverityCritical, sMsg, sCurrentName,
                            substr(dbms_utility.format_error_stack || CHR(10) || dbms_utility.format_error_backtrace, 1,4000)  );
    return oErrorObj;
end putRowForRemoteEntity;
--****************************************************************************************************
member function getInputXmlServerPart( csEntity      in  varchar2,
                                  cnSeverTypeID in  number,
                                  cnRepID       in  number,
                                  csTransferId  in  varchar2,
                                  xPart         out xmltype ) return STB$OERROR$RECORD
is
  sCurrentName constant varchar2(100) := $$PLSQL_UNIT||'.getInputXmlServerPart';
  oErrorObj             STB$OERROR$RECORD := STB$OERROR$RECORD();
  sMsg                  varchar2(4000);
begin
  isr$trace.stat('begin', 'cnSeverTypeID=' || cnSeverTypeID || ' cnRepID=' ||cnRepID, sCurrentName);
  -- Create XML information for remote site
  select xmlagg( xmlelement (
                 "SERVER"
               , xmlelement ("SERVERID", s.serverid)
               , xmlelement ("SERVERNAME", '"' || s.alias || '"')
               , xmlelement ("SERVERTYPE", servertypeid)
               , xmlelement ("HOST", observer_host )
               , xmlelement ("PORT", port)
               , xmlelement ("WEBSERVICE", classname)
               , xmlelement ("CONTEXT", context)
               , xmlelement ("NAMESPACE", namespace)
               , xmlelement ("TRANSFERKEYID", csTransferId || '_' || s.serverid)
               , xmlelement ("REPID", nvl(cnRepID, round(dbms_random.value(10000,100000))))
               , xmlelement ("CONFIGNAME", 'REMOTE_DATA')
               , xmlelement ("MAXINPARAMETERS", STB$UTIL.getSystemParameter('MAX_REMOTE_IN_PARAMETERS'))
               , xmlelement("DATEPATTERN", STB$UTIL.getSystemParameter ('REMOTECOLLECTOR_DATEFORMAT'))
               , xmlelement ("TIMEOUT", timeout)
               , xmlagg ( xmlelement(EVALNAME(parametername),
                        --case when parametername='PASSWORD' then isr$util.decryptpassword (parametervalue) else parametervalue end
                        parametervalue ))))
    into xPart
    from isr$serverobserver$v s, isr$server$entity$mapping sem, isr$serverparameter sp
   where s.serverid = sem.serverid
     and sem.entityname = csEntity
     and s.serverid = sp.serverid
     and sp.parametervalue is not null
     and s.servertypeid = cnSeverTypeID
     and s.runstatus = 'T'
     and ( to_char(s.serverid) in (select key from isr$crit where entity = 'SERVER' and repid = cnRepID)
          or cnRepID is null)
     group by s.serverid, s.alias, servertypeid, observer_host, port, classname, context, namespace, timeout;

  isr$trace.stat('end','end', sCurrentName);
  return oErrorObj;
exception
  when others then
    sMsg :=  utd$msglib.getmsg ('exUnexpectedError', stb$security.getcurrentlanguage, sqlerrm(sqlcode));
    oErrorObj.handleError ( Stb$typedef.cnSeverityCritical, sMsg, sCurrentName,
                            substr(dbms_utility.format_error_stack || CHR(10) || dbms_utility.format_error_backtrace, 1,4000)  );
    return oErrorObj;
END getInputXmlServerPart;


--******************************************************************************
member function getInputXmlOrderExpr(csEntity    in  varchar2) return varchar2 is

  sCurrentName constant varchar2(100) := $$PLSQL_UNIT||'.getInputXmlOrderExpr(' || csEntity || ')';
  sOrderexpr    isr$meta$entity.orderexpr%type;
  CURSOR cGetOrderColumns  IS
    WITH row_count AS
         (SELECT LENGTH (orderexpr) - LENGTH (REPLACE (orderexpr, ',', '')) + 1 rowcnt
               , orderexpr
            FROM (SELECT RTRIM (   RTRIM (TRIM (orderexpr), ',')
                                || ','
                                || (SELECT display
                                      FROM isr$meta$crit
                                     WHERE critentity = entityname), ',')
                            orderexpr
                    FROM isr$meta$entity
                   WHERE entityname = csEntity))
  SELECT CASE
         WHEN datatype = 'NUMBER' THEN
            'TO_NUMBER('||orderexpr||')'
         WHEN datatype = 'DATE' THEN
            'TO_DATE('||orderexpr||', ''' || stb$typedef.csInternalDateFormat || ''')'
         ELSE
            orderexpr
       END orderSQL
  FROM (SELECT orderexpr
          FROM (SELECT TRIM(SUBSTR (
                               orderexpr
                             , INSTR (orderexpr, ',', 1, ROWNUM) + 1
                             , DECODE (INSTR (SUBSTR (orderexpr, INSTR (orderexpr, ',', 1, ROWNUM) + 1), ',')
                                       - 1
                                     , -1
                                     , LENGTH (SUBSTR (orderexpr, INSTR (orderexpr, ',', 1, ROWNUM) + 1))
                                     , INSTR (SUBSTR (orderexpr, INSTR (orderexpr, ',', 1, ROWNUM) + 1), ',')
                                       - 1)
                            ))
                          orderexpr
                     , INSTR (orderexpr, ',', 1, ROWNUM)
                       + 1 ordercol
                  FROM row_count, stb$usergroup--user_tables
                 WHERE ROWNUM <= rowcnt)
        GROUP BY orderexpr
        ORDER BY MIN(ordercol))
       , isr$meta$attribute
  WHERE attrname = orderexpr
   AND entityname = csEntity;
begin
  isr$trace.stat('begin','begin', sCurrentName);
  for rGetOrderColumns IN cGetOrderColumns loop
      sOrderexpr := sOrderexpr||rGetOrderColumns.orderSQL||',';
  end loop;
    sOrderexpr := trim(rtrim(sOrderexpr,','));
  isr$trace.stat('end', sOrderexpr, sCurrentName);
  return sOrderexpr;
end getInputXmlOrderExpr;

--**********************************************************************************************************************
member function deleteTmpData(csFileTransferId in varchar2) return STB$OERROR$RECORD
  is pragma autonomous_transaction;
  oErrorObj       STB$OERROR$RECORD  := STB$OERROR$RECORD ();
  sCurrentName    constant varchar2 (100) := $$PLSQL_UNIT || '.deleteTmpData';
  sUserMsg        varchar2(4000);
begin
  isr$trace.stat('begin', 'begin', sCurrentName);
  delete from isr$transfer$remotedata$tmp where transferid = csFileTransferId;
  commit;
  isr$trace.stat('end', 'end', sCurrentName);
  return oErrorObj;
exception
  when others then
    sUserMsg := utd$msglib.getmsg ('exDeleteTmpDataTxt', nCurrentLanguage,  csP1 =>sqlerrm(sqlcode));
    oErrorObj.handleError ( Stb$typedef.cnSeverityCritical, sUserMsg, sCurrentName,
                          substr(dbms_utility.format_error_stack || CHR(10) || dbms_utility.format_error_backtrace, 1,4000) );
    return oErrorObj;
end deleteTmpData;

--**********************************************************************************************************************
static function deleteTmpDataForSession(cnSessionId in number) return STB$OERROR$RECORD
  is pragma autonomous_transaction;
  oErrorObj       STB$OERROR$RECORD  := STB$OERROR$RECORD ();
  sCurrentName    constant varchar2 (100) := $$PLSQL_UNIT || '.deleteTmpDataForSession';
  sUserMsg        varchar2(4000);
begin
  isr$trace.stat('begin', 'begin', sCurrentName);
  delete from isr$transfer$remotedata$tmp where sessionid = cnSessionId;
  commit;
  isr$trace.stat('end', 'end', sCurrentName);
  return oErrorObj;
exception
  when others then    
    oErrorObj.handleError ( Stb$typedef.cnSeverityCritical, 
                      utd$msglib.getmsg ('exDeleteTmpDataTxt', stb$security.getcurrentlanguage, csP1 => sqlerrm(sqlcode)), sCurrentName,
                      dbms_utility.format_error_stack || CHR(10) || dbms_utility.format_error_backtrace);
    return oErrorObj;
end deleteTmpDataForSession;

--**********************************************************************************************************************
member function saveTmpRemoteData(csFileTransferId in varchar2) return STB$OERROR$RECORD
  is
  sCurrentName    constant varchar2 (100) := $$PLSQL_UNIT || '.saveTmpRemoteData';
  oErrorObj       STB$OERROR$RECORD  := STB$OERROR$RECORD ();
  sUserMsg        varchar2(4000);
  exNoRemoteData    exception;
  exDeleteTmpData   exception;
  sRemoteEx       clob;
  cursor cGetRemoteData is
  select entityname, clobdata
  from isr$transfer$remotedata$tmp
  where transferid = csFileTransferId;

  rGetRemoteData cGetRemoteData%rowtype;


begin
  isr$trace.stat('begin', 'csFileTransferId: '||csFileTransferId, sCurrentName);

  open cGetRemoteData;
  fetch cGetRemoteData into rGetRemoteData;

  if cGetRemoteData%notfound then
    raise exNoRemoteData;
  end if;

  if rGetRemoteData.clobdata is null then
    isr$trace.warn(rGetRemoteData.entityname || ' remote xml has no rows!', '', sCurrentName);
  else
    isr$trace.debug(rGetRemoteData.entityname || ' xml after upload','xml in logblob ->', sCurrentName, rGetRemoteData.clobdata);

    sUserMsg := 'csFileTransferId= ' || csFileTransferId || '; entity = ' || rGetRemoteData.entityname;
    -- insert rows
    sRemoteEx := isr$server$collector$rec.insertXmlIntoTable(rGetRemoteData.entityname, rGetRemoteData.clobdata, STB$UTIL.getSystemParameter ('REMOTECOLLECTOR_DATEFORMAT'));
    oErrorObj.checkRemoteException(sCurrentName, Stb$typedef.cnSeverityCritical, sRemoteEx);
  end if;

  close  cGetRemoteData;

  oErrorObj := deleteTmpData(csFileTransferId);
  if oErrorObj.sSeverityCode != STB$TYPEDEF.cnNoError then
    raise exDeleteTmpData;
  end if;

  isr$trace.stat('end', 'end', sCurrentName);
  return oErrorObj;
exception
   when exDeleteTmpData then
    return oErrorObj;
   when exNoRemoteData then
    sUserMsg := utd$msglib.getmsg ('exNoRemoteData', nCurrentLanguage,  csP1 => csFileTransferId);
    oErrorObj.handleError ( Stb$typedef.cnSeverityCritical, sUserMsg, sCurrentName, dbms_utility.format_error_stack ||CHR(10)|| dbms_utility.format_error_backtrace);
    return oErrorObj;
  when others then
    sUserMsg := utd$msglib.getmsg ('exSaveDataFailed', nCurrentLanguage,  csP1 =>sUserMsg,  csP2=>sqlerrm(sqlcode));
    oErrorObj.handleError ( Stb$typedef.cnSeverityCritical, sUserMsg, sCurrentName,
                          dbms_utility.format_error_stack || CHR(10) || dbms_utility.format_error_backtrace);
    return oErrorObj;
end saveTmpRemoteData;

--******************************************************************************
static function fillTmpTable ( csXml IN CLOB, csEntity  IN VARCHAR2, csLoglevel  IN VARCHAR2,  csLogReference  IN VARCHAR2, nTimeout in NUMBER ) RETURN CLOB
AS LANGUAGE JAVA
NAME 'com.uptodata.isr.db.wsclient.RemoteDBClient.fillTmpTable(  java.sql.Clob, java.lang.String, java.lang.String, java.lang.String, int) return java.sql.Clob';

--******************************************************************************
static function insertXmlIntoTable ( csEntity  IN VARCHAR2, csXml IN CLOB, csDatepattern  IN VARCHAR2) RETURN CLOB
AS LANGUAGE JAVA
NAME 'com.uptodata.isr.db.wsclient.RemoteDBClient.insertXmlIntoTable(java.lang.String, java.sql.Clob, java.lang.String) return java.sql.Clob';

end;