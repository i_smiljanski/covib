package com.uptodata.isr.remote.base;

/**
 * Created by smiljanskii60 on 17.04.2015.
 */
public class ColumnData {
    String columnName;
    Object value;
    String dataType;

    public ColumnData() {
    }

    public ColumnData(String columnName, Object value, String dataType) {
        this.columnName = columnName;
        this.value = value;
        this.dataType = dataType;
    }

    public String getColumnName() {
        return columnName;
    }

    public void setColumnName(String columnName) {
        this.columnName = columnName;
    }

    public Object getValue() {
        return value;
    }

    public void setValue(Object value) {
        this.value = value;
    }

    public String getDataType() {
        return dataType;
    }

    public void setDataType(String dataType) {
        this.dataType = dataType;
    }
}
