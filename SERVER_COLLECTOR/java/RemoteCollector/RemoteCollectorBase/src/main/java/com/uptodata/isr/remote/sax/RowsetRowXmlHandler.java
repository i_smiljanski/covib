package com.uptodata.isr.remote.sax;

import com.uptodata.isr.utils.xml.sax.SaxRowXmlHandler;
import org.xml.sax.Attributes;
import org.xml.sax.SAXException;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * Created by smiljanskii60 on 26.10.2015.
 * <ROWSET>
 * <ROW>
 * ...
 * </ROW>
 * </ROWSET>
 */
public class RowsetRowXmlHandler extends SaxRowXmlHandler {

    private StringBuilder content = new StringBuilder();

    public List<Map<String, String>> getRows() {
        return rows;
    }

    @Override
    public void startElement(String uri, String localName, String qName, Attributes atts)
            throws SAXException {
        if (qName.equals("ROW")) {
            row = new HashMap<String, String>();
//            System.out.println("ROW " + atts.getValue("num"));
        }
        content.setLength(0);
//        System.out.println("qName " + qName);
    }

    @Override
    public void endElement(String uri, String localName, String qName) throws SAXException {
        endElement = qName;
        if (!qName.equals("ROWSET")) {
            if (!qName.equals("ROW") && row != null) {
                row.put(endElement, currentValue);
//            System.out.println("value for " + endElement + ": " + currentValue);
            } else if (row != null && row.size() > 0) {
                rows.add(row);
            }
        }
        content.setLength(0);
    }

    @Override
    public void characters(char[] ch, int start, int length) throws SAXException {
        content.append(ch, start, length);
        currentValue = content.toString();
        currentValue = currentValue.replaceFirst("^\n*", "").replaceFirst("^ *", "");
//        System.out.println("currentValue = " + currentValue + "|");
    }


}
