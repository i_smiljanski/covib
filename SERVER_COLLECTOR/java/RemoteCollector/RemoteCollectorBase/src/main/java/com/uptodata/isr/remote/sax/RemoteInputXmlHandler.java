package com.uptodata.isr.remote.sax;

import com.uptodata.isr.utils.xml.sax.SaxRowXmlHandler;
import org.apache.commons.lang.StringUtils;
import org.xml.sax.Attributes;
import org.xml.sax.SAXException;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * Created by smiljanskii60 on 26.10.2015.
 * handler for remote input xml
 * Example:
 * <ISR_REMOTE_XML tablename="ISR$SB$LEVEL1" entity="SB$LEVEL1" isrTableName="TMP$SB$LEVEL1">
 * <SERVER>
 * <SERVERID>36</SERVERID>
 * <SERVERTYPE>2</SERVERTYPE>
 * <HOST>...</HOST>
 * <PORT>1095</PORT>
 * <WEBSERVICE>com.uptodata.isr.remote.ws.RemoteCollectorServicePublisher</WEBSERVICE>
 * <CONTEXT>/ws/remote/isr/uptodata/com</CONTEXT>
 * <NAMESPACE>http://ws.remote.isr.uptodata.com/</NAMESPACE>
 * <DBDRIVER>oracle.jdbc.driver.OracleDriver</DBDRIVER>
 * <USERNAME>...</USERNAME>
 * <PASSWORD>...</PASSWORD>
 * <LIMSUSER>...</LIMSUSER>
 * <DBURL>...</DBURL>
 * </SERVER>
 * <COLUMNS>
 * <COLUMN columntype="VARCHAR2" columnname="CODE">CODE</COLUMN>
 * <COLUMN columntype="VARCHAR2" columnname="DISPLAY">DISPLAY</COLUMN>
 * </COLUMNS>
 * <ORDERBY>DISPLAY</ORDERBY>
 * <DBPARAMETER columnname="SPECIFICATIONCODE" key="FK">
 * <SPECIFICATIONCODE><![CDATA[...]]></SPECIFICATIONCODE>
 * </DBPARAMETER>
 * <DBPARAMETER columnname="VERSIONCODE" key="FK">
 * <VERSIONCODE><![CDATA[...]]></VERSIONCODE>
 * </DBPARAMETER>
 * <DBPARAMETER columnname="CODE"/>
 * </ISR_REMOTE_XML>
 */
public class RemoteInputXmlHandler extends SaxRowXmlHandler {

    private Map<String, String> column;
    private String columnType;
    private String columnName;
    private String serverId;

    public String username;
    public String password;

    public String currentElement;
    public boolean isCurrentServer;
    public String port;
    public String host;
    public String webservice;
    public String context;
    public String tableName;
    public String entity;
    public String serverType;
    public String maxInParameters;
    public String transferKeyId;
    public String repId;
    public String configName;
    public List<Map<String, String>> columns;

    public String getServerId() {
        return serverId;
    }

    public void setServerId(String serverId) {
        this.serverId = serverId;
    }


    @Override
    public void startElement(String uri, String localName, String qName, Attributes atts)
            throws SAXException {
        currentElement = qName;
        if (qName.equals("ISR_REMOTE_XML")) {
            tableName = atts.getValue("tablename");
            entity = atts.getValue("entity");
        } else if (qName.equals("COLUMN")) {
            columnType = atts.getValue("columntype");
            columnName = atts.getValue("columnname");
        } else if (qName.equals("COLUMNS")) {
            columns = new ArrayList<Map<String, String>>();
        }
    }


    @Override
    public void endElement(String uri, String localName, String qName) throws SAXException {
        if (qName.equals("SERVERID")) {
            isCurrentServer = StringUtils.isNotEmpty(currentValue) && currentValue.equals(serverId);
        } else if (qName.equals("COLUMN")) {
            column = new HashMap<String, String>();
            column.put("columntype", columnType);
            if ("XML".equals(columnType)) {
                column.put("columnname", currentValue);
            } else {
                column.put("columnname", columnName);
            }
            columns.add(column);
        }

        if (isCurrentServer) {
            if (qName.equals("HOST")) {
                host = currentValue;
            } else if (qName.equals("PORT")) {
                port = currentValue;
            } else if (qName.equals("WEBSERVICE")) {
                webservice = currentValue;
            } else if (qName.equals("CONTEXT")) {
                context = currentValue;
            } else if (qName.equals("SERVERTYPE")) {
                serverType = currentValue;
            } else if (qName.equals("MAXINPARAMETERS")) {
                maxInParameters = currentValue;
            } else if (qName.equals("USERNAME")) {
                username = currentValue;
            } else if (qName.equals("PASSWORD")) {
                password = currentValue;
            } else if (qName.equals("TRANSFERKEYID")) {
                transferKeyId = currentValue;
            } else if (qName.equals("REPID")) {
                repId = currentValue;
            } else if (qName.equals("CONFIGNAME")) {
                configName = currentValue;
            }
        }
    }


    @Override
    public void characters(char[] ch, int start, int length) throws SAXException {
        currentValue = new String(ch, start, length);
        //ISRC-1339
        currentValue = currentValue.replaceFirst("^\n*", "").replaceFirst("^ *", "");
    }

    @Override
    public String toString() {
        return "SaxRemoteXmlHandler{" +
                " serverId='" + serverId + '\'' +
                " serverType='" + serverType + '\'' +
                " enity='" + entity + '\'' +
                " maxInParameters='" + maxInParameters + '\'' +
                ", username='" + username + '\'' +
                ", password='" + password + '\'' + "\n" +
                ", transferKeyId='" + transferKeyId + '\'' + "\n" +
                ", repId='" + repId + '\'' + "\n" +
                ", configName='" + configName + '\'' + "\n" +
                ", columns=" + columns + "\n" +
                ", rows=" + rows +
                '}' + "; " + super.toString();
    }

    @Override
    public List<Map<String, String>> getRows() {
        return rows;
    }
}
