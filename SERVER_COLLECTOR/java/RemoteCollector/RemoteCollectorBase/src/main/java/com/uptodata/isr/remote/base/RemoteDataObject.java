package com.uptodata.isr.remote.base;

import java.util.List;
import java.util.Map;

/**
 * Created by smiljanskii60 on 16.04.2015.
 */
public class RemoteDataObject {
    String rowSet;
    //List<Map<String, ColumnData>> rows;
    List<Map<String, Map<String, Object>>> rows;

    public RemoteDataObject() {
    }

    public String getRowSet() {
        return rowSet;
    }

    public void setRowSet(String rowSet) {
        this.rowSet = rowSet;
    }

   /* public List<Map<String, ColumnData>> getRows() {
        return rows;
    }

    public void setRows(List<Map<String, ColumnData>> rows) {
        this.rows = rows;
    }*/

    public List<Map<String, Map<String, Object>>> getRows() {
        return rows;
    }

    public void setRows(List<Map<String, Map<String, Object>>> rows) {
        this.rows = rows;
    }

    @Override
    public String toString() {
        return "RemoteDataObject{" +
                "rowSet='" + rowSet + '\'' +
                ", rows=" + rows +
                '}';
    }
}
