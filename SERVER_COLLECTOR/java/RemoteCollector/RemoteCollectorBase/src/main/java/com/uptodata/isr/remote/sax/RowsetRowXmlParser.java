package com.uptodata.isr.remote.sax;


import com.uptodata.isr.utils.ConvertUtils;
import com.uptodata.isr.utils.xml.sax.SaxRowXmlHandler;
import com.uptodata.isr.utils.xml.sax.insertxml.InsertXmlParser;

import java.io.FileInputStream;
import java.sql.Connection;
import java.sql.Driver;
import java.sql.DriverManager;
import java.sql.SQLException;

/**
 * Created by smiljanskii60 on 28.10.2015.
 */
public abstract class RowsetRowXmlParser extends InsertXmlParser {


    public RowsetRowXmlParser(SaxRowXmlHandler handler, String tableName, String datePattern) {
        super(handler, tableName, datePattern);
    }

    public RowsetRowXmlParser(SaxRowXmlHandler handler, String tableName) {
        super(handler, tableName);
    }

    public int insertXml(String tableName, Connection connection) throws Exception {
        setTableName(tableName);
        int insertedRows = insertXmlIntoTable(connection);
        return insertedRows;
    }

}
