package com.uptodata.isr.db.wsclient;

import com.uptodata.isr.db.connection.ConnectionUtil;
import com.uptodata.isr.db.trace.DbLogging;
import com.uptodata.isr.remote.sax.RowsetRowXmlHandler;
import com.uptodata.isr.remote.sax.RowsetRowXmlParser;
import com.uptodata.isr.utils.ConvertUtils;
import com.uptodata.isr.utils.exceptions.MessageException;
import com.uptodata.isr.utils.exceptions.MessageStackException;
import com.uptodata.isr.utils.ws.SOAPUtil;
import com.uptodata.isr.utils.xml.dom.XmlHandler;
import oracle.jdbc.OracleDriver;
import org.apache.commons.codec.binary.Base64;
import org.apache.commons.lang.StringEscapeUtils;
import org.apache.commons.lang.StringUtils;
import org.w3c.dom.NodeList;

import java.sql.*;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by smiljanskii60 on 26.06.2014.
 */
public class RemoteDBClient {
    protected static DbLogging log;

    protected static String ENTITY;

    public static String METHOD_BEGIN = DbLogging.METHOD_BEGIN;
    public static String METHOD_END = DbLogging.METHOD_END;

    // final static String targetNamespace = "http://ws.remote.isr.uptodata.com/";
    static XmlHandler inputXmlHandler;


    protected static void initLogger() throws MessageStackException {
        try {
            log = new DbLogging();
        } catch (Exception e) {
            throw new MessageStackException("Logger initialization missing " + e);
        }
    }

    public static Clob fillTmpTable(Clob clobXml, String entity, String logLevel, String logReference, int timeout)
            throws MessageStackException {
        initLogger();
        ENTITY = entity;
        log.stat(METHOD_BEGIN, METHOD_BEGIN, ENTITY);
        try {
            byte[] inputXml = ConvertUtils.getBytes(clobXml);
            log.info("inputXml length", inputXml.length, ENTITY);
            RemoteDBClient dbClient = new RemoteDBClient();
            String response = dbClient.fillTmpTable(inputXml, ConnectionUtil.getDefaultConnection(), logLevel, logReference, timeout);

            if (StringUtils.isNotEmpty(response)) {
                return ConvertUtils.getClob(ConnectionUtil.getDefaultConnection(), response.getBytes());
            }
            log.stat(METHOD_END, response, ENTITY);
            return null;
        } catch (Exception e) {
            log.error("error", e, ENTITY);
            if (e instanceof MessageStackException) {
                throw (MessageStackException) e;
            } else {
                throw new MessageStackException(e, MessageStackException.SEVERITY_CRITICAL, e.getMessage(),
                        MessageStackException.getCurrentMethod());
            }
        }
    }

    public static Clob insertXmlIntoTable(String entity, Clob clobXml, String datePattern) throws Exception {
        initLogger();
        ENTITY = entity;
        try {
            byte[] inputXml = ConvertUtils.getBytes(clobXml);
//            String datePattern = inputXmlHandler.nodeRead("//DATEPATTERN");
            log.debug("datePattern", datePattern, ENTITY);
            String tmpTableName = inputXmlHandler.attributeRead("/ISR_REMOTE_XML", "isrTableName");
            RemoteDBClient dbClient = new RemoteDBClient();
            int rows = dbClient.insertXmlIntoTable(ConnectionUtil.getDefaultConnection(), inputXml, tmpTableName, datePattern);
            log.info("for entity '" + entity + "' rows inserted", rows);
            return null;
        } catch (MessageStackException e) {
            log.error("error", e, ENTITY);
            Connection conn = ConnectionUtil.getDefaultConnection();
            if (conn == null) {
                throw new MessageStackException(e, MessageStackException.SEVERITY_CRITICAL, "database connection is impossible",
                        MessageStackException.getCurrentMethod());
            }
            byte[] exBytes = e.toXml().xmlToString().getBytes();
            Clob cl = ConvertUtils.getClob(conn, exBytes);
            log.stat("end", "error");
            return cl;
        }
    }


    protected String fillTmpTable(byte[] inputXml, Connection conn, String logLevel, String logReference, int timeout)
            throws MessageStackException {
        initLogger();
        log.stat(METHOD_BEGIN, METHOD_BEGIN, ENTITY);
        try {
            String unescapedXml = StringEscapeUtils.unescapeXml(ConvertUtils.convertBytesToString(inputXml));
//            DriverManager.registerDriver((Driver) Class.forName("oracle.jdbc.driver.OracleDriver").newInstance());
            getRemoteDataForAllServers(unescapedXml.getBytes(), conn, logLevel, logReference, timeout);
            log.stat(METHOD_END, METHOD_END, ENTITY);
            return null;
        } catch (Exception e) {
            MessageStackException exList = new MessageStackException(e, MessageStackException.SEVERITY_CRITICAL,
                    "error by filling of temporary table. " + e.getMessage(),
                    MessageStackException.getCurrentMethod());
            return exList.toXml().xmlToString();
        }
    }


    protected void getRemoteDataForAllServers(byte[] inputXml, Connection conn, String logLevel,
                                              String logReference, int timeout) throws MessageStackException {
        initLogger();
        log.stat(METHOD_BEGIN, METHOD_BEGIN, ENTITY);
        List<MessageStackException> exceptions = new ArrayList<MessageStackException>();
        inputXmlHandler = new XmlHandler(inputXml);
        try {
            inputXmlHandler.nodeRoot("ROWSET");

            NodeList serverNodes = inputXmlHandler.selectNodes("//SERVER");
            for (int i = 1; i <= serverNodes.getLength(); i++) {
                String serverId = inputXmlHandler.nodeRead("//SERVER[" + i + "]/SERVERID");
                String serverTimeoutStr = inputXmlHandler.nodeRead("//SERVER[" + i + "]/TIMEOUT");
                int serverTimeout = timeout;
                if (StringUtils.isNotEmpty(serverTimeoutStr)) {
                    try {
                        serverTimeout = Integer.valueOf(serverTimeoutStr);
                    } catch (NumberFormatException e) {
                        log.info("error by convert '" + serverTimeoutStr + "' to integer.", "default timeout '" + timeout + "' used", ENTITY);
                    }
                }
                log.debug("serverId", serverId, ENTITY);
                try {
                    String result = sendRemoteRequestToServer(inputXml, "sendRemoteXml", logLevel, logReference, serverTimeout, serverId);
                    /*if (remoteXml != null) {
                        byte[] remoteXmlBytes = Base64.decodeBase64(remoteXml);

                        String datePattern = inputXmlHandler.nodeRead("//DATEPATTERN");
                        log.debug("datePattern", datePattern, ENTITY);
                        String tmpTableName = inputXmlHandler.attributeRead("/ISR_REMOTE_XML", "isrTableName");
                        insertXmlIntoTable(conn, remoteXmlBytes, tmpTableName, datePattern);
                    }*/
                } catch (MessageStackException e) {
//                    "server " + serverId + " has error: ";
                    exceptions.add(e);
                }
            }
            if (exceptions.size() == serverNodes.getLength()) {
                List<MessageException> newExc = new ArrayList<MessageException>();
                for (MessageStackException ex : exceptions) {
                    newExc.addAll(ex.getExceptionList());
                }
                throw new MessageStackException(newExc);
            }
            log.stat(METHOD_END, METHOD_END, ENTITY);
        } catch (Exception e) {
            MessageStackException mse = new MessageStackException(e, MessageStackException.SEVERITY_CRITICAL,
                    "remote data could not be stored in the temporary table: ",
                    MessageStackException.getCurrentMethod());
            log.error("MessageStackException", mse.print(), ENTITY);
            throw mse;
        }
    }


    protected int insertXmlIntoTable(Connection conn, byte[] remoteXml, String tableName, String datePattern) throws Exception {

        RowsetRowXmlHandler handler = new RowsetRowXmlHandler();
        RowsetRowXmlParser parser = new RowsetRowXmlParser(handler, tableName, datePattern) {
            @Override
            public void log(String actionTaken, Object logEntry, String logLevel) {
                log.log(actionTaken, logEntry, logLevel, 1);
            }
        };

        log.stat(METHOD_BEGIN, "tableName = " + tableName, ENTITY);
        int rows = 0;
        try {
            log.debug("handler", parser.getHandler(), ENTITY);
            parser.parseXml(remoteXml);
            log.stat("conn closed?", conn.isClosed(), ENTITY);

            rows = parser.insertXmlIntoTable(conn);
        } catch (Exception e) {
            log.error("error", e, ENTITY);
            throw e;
        } finally {
            log.stat(METHOD_END, "rows = " + rows, ENTITY);
        }
        return rows;
    }


    private static String sendRemoteRequestToServer(byte[] inputXml, String methodName, String logLevel,
                                                    String logReference, int timeout, String serverId) throws MessageStackException {
        initLogger();
        String host = inputXmlHandler.findXpath("//SERVER[SERVERID='" + serverId + "']/HOST", "STRING").toString();
        String port = inputXmlHandler.findXpath("//SERVER[SERVERID='" + serverId + "']/PORT", "STRING").toString();
        String context = inputXmlHandler.findXpath("//SERVER[SERVERID='" + serverId + "']/CONTEXT", "STRING").toString();
        String serverName = inputXmlHandler.findXpath("//SERVER[SERVERID='" + serverId + "']/SERVERNAME", "STRING").toString();
        String targetNamespace = inputXmlHandler.findXpath("//SERVER[SERVERID='" + serverId + "']/NAMESPACE", "STRING").toString();
        log.stat(METHOD_BEGIN, "serverId = " + serverId + " serverName= " + serverName, ENTITY);

        String[] args = new String[6];
        args[0] = serverId;
        args[1] = serverName;
        args[2] = Base64.encodeBase64String(inputXml);
        args[3] = logLevel;
        args[4] = logReference;
        args[5] = ENTITY;

        String url = "http://" + host + ":" + port + context;
        String result = SOAPUtil.callMethod(url, targetNamespace, args, methodName, timeout);
        log.stat(METHOD_END, url + "; serverName= " + serverName, ENTITY);

        return result;
    }


    private static String sendScriptsToServer(byte[] inputXml, String methodName, String logLevel,
                                              String logReference, int timeout, String[] scripts) throws MessageStackException {
        initLogger();
        String host = inputXmlHandler.findXpath("//HOST", "STRING").toString();
        String port = inputXmlHandler.findXpath("//PORT", "STRING").toString();
        String context = inputXmlHandler.findXpath("//CONTEXT", "STRING").toString();
        String serverName = inputXmlHandler.findXpath("//SERVERNAME", "STRING").toString();
        String targetNamespace = inputXmlHandler.findXpath("//NAMESPACE", "STRING").toString();

        log.stat(METHOD_BEGIN, "http://" + host + ":" + port + context, ENTITY);

        Object[] args = new Object[5];
        args[0] = scripts;
        args[1] = Base64.encodeBase64String(inputXml);
        args[2] = serverName;
        args[3] = logLevel;
        args[4] = logReference;

        String url = "http://" + host + ":" + port + context;
        log.debug("send request to  url", url + "; method: " + methodName + " targetNamespace:" + targetNamespace, ENTITY);
        String sent = SOAPUtil.callMethod(url, targetNamespace, args, methodName, timeout);
        log.stat(METHOD_END, sent, ENTITY);
        return sent;
    }


    public static Clob deployRemote(Array scripts, Clob clobXml, String logLevel, String logReference, int timeout)
            throws MessageStackException, SQLException {
        initLogger();
        log.stat(METHOD_BEGIN, METHOD_BEGIN);
        String returnXml;
        String errorMsg = "deploy remote failed";
        Connection conn = null;
        try {

            conn = new OracleDriver().defaultConnection();
            byte[] inputXml = ConvertUtils.getBytes(clobXml);
            log.debug("inputXml", new String(inputXml));
            inputXmlHandler = new XmlHandler(inputXml);

            Object[] elements = (Object[]) scripts.getArray();
            String[] scriptsArray = new String[elements.length];

            for (int i = 0; i < elements.length; i++) {
                scriptsArray[i] = new String(ConvertUtils.getBytes((Clob) elements[i]), ConvertUtils.encoding);
                log.debug("scriptsArray[" + i + "]", scriptsArray[i]);
            }
            String response = sendScriptsToServer(inputXml, "deployRemote", logLevel, logReference, timeout, scriptsArray);

            log.stat("end ", response);
            return null;
        } catch (Exception e) {
            if (conn == null) {
                throw new MessageStackException(MessageStackException.SEVERITY_CRITICAL, "database connection is impossible",
                        MessageStackException.getCurrentMethod(), null);
            }
            try {
                if (e instanceof MessageStackException) {
                    returnXml = ((MessageStackException) e).toXml().xmlToString();
                } else {
                    returnXml = e.getMessage();
                }
                return ConvertUtils.getClob(conn, returnXml.getBytes());
            } catch (Exception e1) {
                throw new MessageStackException(e1, MessageStackException.SEVERITY_WARNING, errorMsg,
                        MessageStackException.getCurrentMethod());
            }
        }
    }

    public static Clob insertRowDataXmlToRemoteTable(Clob confXmlCl, Clob dataXmlCl, String serverId, String logLevel,
                                              String logReference,  int timeout)
            throws MessageStackException {
        initLogger();
        log.stat(METHOD_BEGIN, METHOD_BEGIN);
        try {
            byte[] confXml = ConvertUtils.getBytes(confXmlCl);
            log.info("confXml length", confXml.length);

            byte[] dataXml = null;
            if(dataXmlCl != null) {
                dataXml = ConvertUtils.getBytes(dataXmlCl);
                log.info("dataXml length", dataXml.length);
            }
            RemoteDBClient dbClient = new RemoteDBClient();

            String response = dbClient.sendXmlTableDataToServer(confXml, dataXml,
                    logLevel, logReference, timeout, serverId);

            if (StringUtils.isNotEmpty(response)) {
                return ConvertUtils.getClob(ConnectionUtil.getDefaultConnection(), response.getBytes());
            }
            log.stat(METHOD_END, response);
            return null;
        } catch (Exception e) {
            log.error("error", e);
            if (e instanceof MessageStackException) {
                throw (MessageStackException) e;
            } else {
                throw new MessageStackException(e, MessageStackException.SEVERITY_CRITICAL, e.getMessage(),
                        MessageStackException.getCurrentMethod());
            }
        }
    }


    private String sendXmlTableDataToServer(byte[] confXml, byte[] dataXml, String logLevel,
                                                    String logReference, int timeout, String serverId) throws MessageStackException {
        initLogger();
        log.stat(METHOD_BEGIN, METHOD_BEGIN);
        inputXmlHandler = new XmlHandler(confXml);
        String host = inputXmlHandler.findXpath("//SERVER[SERVERID='" + serverId + "']/HOST", "STRING").toString();
        String port = inputXmlHandler.findXpath("//SERVER[SERVERID='" + serverId + "']/PORT", "STRING").toString();
        String context = inputXmlHandler.findXpath("//SERVER[SERVERID='" + serverId + "']/CONTEXT", "STRING").toString();
        String serverName = inputXmlHandler.findXpath("//SERVER[SERVERID='" + serverId + "']/SERVERNAME", "STRING").toString();
        String targetNamespace = inputXmlHandler.findXpath("//SERVER[SERVERID='" + serverId + "']/NAMESPACE", "STRING").toString();
        String tableName = inputXmlHandler.findXpath("/ISR_REMOTE_XML/@tablename", "STRING").toString();
        log.stat(METHOD_BEGIN, "tableName = " + tableName + " serverName= " + serverName);

        String[] args = new String[7];
        args[0] = serverId;
        args[1] = serverName;
        args[2] = Base64.encodeBase64String(confXml);
        args[3] = dataXml != null ? Base64.encodeBase64String(dataXml) : null;
        args[4] = logLevel;
        args[5] = logReference;
        args[6] = tableName;

        String url = "http://" + host + ":" + port + context;
        String result = SOAPUtil.callMethod(url, targetNamespace, args, "insertRowDataXmlToTable", timeout);
        log.stat(METHOD_END, url + "; serverName= " + serverName);

        return result;
    }

    public static void main(String[] args) {
        try {
            initLogger();
            Connection conn = null;
            byte[] inputXml = ConvertUtils.fileToBytes(System.getProperty("user.dir") + "\\input1.xml");
            try {
//                conn = DriverManager.getConnection(
//                        "jdbc:oracle:thin:@isrc-d-d:1521:ora11204", "isrowner_isrc_dev", "isrowner_isrc_dev"
//                );
                conn = DriverManager.getConnection(
                        "jdbc:oracle:thin:@muib-d-d:1521:ora11204", "isrowner35_muib", "isrowner35_muib"
                );
            } catch (SQLException e) {
                e.printStackTrace();
            }

            RemoteDBClient dbClient = new RemoteDBClient();
            ENTITY = "BIO$SAMPLE";
            String res = dbClient.fillTmpTable(inputXml, conn, "DEBUG", "test", 300);
            log.debug("result", res);

//            byte[] remoteXml = ConvertUtils.fileToBytes(System.getProperty("user.dir") + "\\SB$VERSION.xml");
//            inputXmlHandler = new XmlHandler(inputXml);
//            dbClient.insertXmlIntoTable(conn, remoteXml, "DBG$SB$VERSION", null);

//            String[] scriptsArray = new String[2];
//            scriptsArray[0] = "test1";
//            scriptsArray[1] = "test2";
//            sendScriptsToServer(inputXml, "deployRemote", "DEBUG", "TEST", 100, scriptsArray);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}
