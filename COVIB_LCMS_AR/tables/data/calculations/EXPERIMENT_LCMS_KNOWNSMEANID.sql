  SELECT XMLELEMENT("experiment", XMLATTRIBUTES('&&Experiment&' AS "name" ,'&&KnownsCalcType&' AS "type"),
           Xmlagg (XMLConcat(
             Xmlagg (XMLELEMENT("calculation", xmlconcat(targ,val,res)) order by  temperature, hours, longtermtime, longtermunits, concentrationunits, namenum, knownorder, name, acceptedrun &&DilColumnGroupBy& &&RunIDColumn&),
             XMLELEMENT("statistic", XMLATTRIBUTES(acceptedrun AS "acceptedrun", analyteid AS "analyteid", analyteorder as "analyteorder", species as "species", matrix as "matrix", concentrationunits as "unit") &&RunIDInTarget&,
               XMLELEMENT("flagpercent", min(flagpercent)),
               XMLELEMENT("min-bias", FormatRounded(min(bias), &&DecPlPercBias&)),
               XMLELEMENT("max-bias", FormatRounded(max(bias), &&DecPlPercBias&)),
               XMLELEMENT("mean-bias", FormatRounded(avg(bias), &&DecPlPercBias&)),
               XMLELEMENT("min-biasonmean", FormatRounded(min(biasonmean), &&DecPlPercBias&)),
               XMLELEMENT("max-biasonmean", FormatRounded(max(biasonmean), &&DecPlPercBias&)),
               XMLELEMENT("mean-biasonmean", FormatRounded(avg(biasonmean), &&DecPlPercBias&)),
               XMLELEMENT("min-cv", FormatRounded(min(cv), &&DecPlPercCV&)),
               XMLELEMENT("mean-cv", FormatRounded(avg(cv), &&DecPlPercCV&)),
               XMLELEMENT("max-cv", FormatRounded(max(cv), &&DecPlPercCV&)),
               XMLELEMENT("numacc", sum(cntunflagged)), &&InterLotCvStatistic&
               XMLELEMENT("numtotal", sum(cnt)),
               XMLELEMENT("percacc", case when count(unflagged_bias) != 0 then FmtNum(round(sum(unflagged_bias)/count(unflagged_bias)*100,0)) else null end),
               XMLELEMENT("hours", hours),
               XMLELEMENT("longtermunits", longtermunits),
               XMLELEMENT("longtermtime", longtermtime),
               XMLELEMENT("temperature", temperature)
             )) order by analyteorder, temperature, hours, longtermtime, longtermunits, concentrationunits))
  FROM
   (SELECT analyteorder, min(knownorder) knownorder, species, matrix, temperature, longtermtime, longtermunits, hours, concentrationunits,
      Count(case when anchor = 'T' then null else useconc end) as cnt,
      count(case when anchor = 'T' then null else unflagged end) as cntunflagged,
      case
        when LENGTH(TRIM(TRANSLATE(name, ' +-.0123456789', ' '))) is null then to_number(name)
        else null
      end namenum,
      &&ConcRepresentationFunc&(Avg(case when anchor = 'T' then null
                                    when '&&FinalAccuracy&' = 'T' then useconc else useconc_sigrnd
                                    end), &&ConcFigures&) as mean,
      &&ConcRepresentationFunc&(Stddev(case when anchor = 'T' then null
                                       when '&&FinalAccuracy&' = 'T' then useconc else useconc_sigrnd
                                       end), &&ConcSDFigures&) as sd,
      round(case when min(anchor) = 'T' then null
                 when '&&FinalAccuracy&' = 'T' then Stddev(useconc)/Avg(useconc)*100
                 else &&ConcRepresentationFunc&(Stddev(useconc_sigrnd), &&ConcSDFigures&)/&&ConcRepresentationFunc&(Avg(useconc_sigrnd), &&ConcFigures&)*100
            end , &&DecPlPercCV&) as cv,
      round(Avg(case when anchor = 'T' then null when '&&FinalAccuracy&' = 'T' then usebias else usebias_sigrnd end), &&DecPlPercBias&) as bias,
      case when nominalconc = 0 then null
           else round(case when '&&FinalAccuracy&' = 'T' then (Avg(case when anchor = 'T' then null else useconc end)-nominalconc)/nominalconc*100
                           else (&&ConcRepresentationFunc&(Avg(case when anchor = 'T' then null else useconc_sigrnd end), &&ConcFigures&)-nominalconc_sigrnd)/nominalconc_sigrnd*100
                      end, &&DecPlPercBias&)
      end as biasonmean,
      case when min(anchor) = 'T' then null --anchor
           when abs(Avg(case when '&&FinalAccuracy&' = 'T' then usebias else usebias_sigrnd end)) > min(flagpercent) then 0
           else 1
      end as unflagged_bias,
      min(flagpercent) flagpercent,
      analyteid, name, acceptedrun &&DilColumnGroupBy& &&RunIDColumn&,
      min(anchor) anchor,
      XMLELEMENT("target",
        XMLELEMENT("acceptedrun", acceptedrun),
        XMLELEMENT("analyteid", analyteid),
        XMLELEMENT("species", species),
        XMLELEMENT("matrix", matrix),
        XMLELEMENT("name", NAME),
        XMLELEMENT("nominalconc", &&ConcFormatFunc&(nominalconc_sigrnd, &&ConcFigures&)),
        XMLELEMENT("hours", hours),
        XMLELEMENT("longtermunits", longtermunits),
        XMLELEMENT("longtermtime", longtermtime),
        XMLELEMENT("temperature", temperature), 
        XMLELEMENT("unit", concentrationunits) &&DilTraget& &&RunIDInTarget&) as targ,
      XMLELEMENT("values", Xmlagg(
        XMLELEMENT("sample",
          /*XMLATTRIBUTES(sampleid AS "sampleid", sampleresultid AS "sampleresultid" ),*/ &&RunIDInValues&
          --XMLELEMENT("runsamplesequencenumber", runsamplesequencenumber),
          XMLELEMENT("concentration", &&ConcFormatFunc&(conc_sigrnd, &&ConcFigures&)),
          XMLELEMENT("concval", &&ConcFormatFunc&(conc_sigrnd,&&ConcFigures&)),
          XMLELEMENT("nominalconc", &&ConcFormatFunc&(nominalconc_sigrnd, &&ConcFigures&)),
          XMLELEMENT("subset", subset),
          XMLELEMENT("bias", FormatRounded(bias_sigrnd, &&DecPlPercBias&)),
          XMLELEMENT("accuracy", FormatRounded(accuracy_sigrnd,&&DecPlPercBias&)),
          XMLELEMENT("cv", FormatRounded(cv_sigrnd, &&DecPlPercCV&)),
          --XMLELEMENT("analytearea", analytearea),
          XMLELEMENT("flag", flagged),
          XMLELEMENT("deactivated", isdeactivated),
          XMLELEMENT("samples", xml)
          &&DilValue&)
          order by runnr--, runsamplesequencenumber
          )) as val,
      XMLELEMENT("result",
        XMLELEMENT("n", Count(useconc)),
        XMLELEMENT("mean", &&ConcFormatFunc&(&&ConcRepresentationFunc&(Avg(
                           case when '&&FinalAccuracy&' = 'T' then useconc else useconc_sigrnd
                           end), &&ConcFigures&), &&ConcFigures&)),

        XMLELEMENT("sd", &&ConcFormatFunc&(&&ConcRepresentationFunc&(Stddev(
                        case when '&&FinalAccuracy&' = 'T' then useconc
                        else useconc_sigrnd
                        end), &&ConcSDFigures&), &&ConcSDFigures&)),
        XMLELEMENT("cv", case when '&&FinalAccuracy&' = 'T' then FormatRounded(round(Stddev(useconc)/Avg(useconc)*100, &&DecPlPercCV&), &&DecPlPercCV&)
                         else FormatRounded(round(&&ConcRepresentationFunc&(Stddev(useconc_sigrnd), &&ConcFigures&)/&&ConcRepresentationFunc&(Avg(useconc_sigrnd), &&ConcFigures&)*100, &&DecPlPercCV&), &&DecPlPercCV&)
                         end), &&InterLotCvResult&
        XMLELEMENT("bias", FormatRounded(round(Avg(case when '&&FinalAccuracy&' = 'T' then usebias else usebias_sigrnd end), &&DecPlPercBias&), &&DecPlPercBias&)),
        XMLELEMENT("biasonmean", FormatRounded(round(case when nominalconc = 0 then null
                                                    when '&&FinalAccuracy&' = 'T' then (Avg(useconc)-nominalconc)/nominalconc*100
                                                    else (&&ConcRepresentationFunc&(Avg(useconc_sigrnd), &&ConcFigures&)-nominalconc_sigrnd)/nominalconc_sigrnd*100
                                               end, &&DecPlPercBias&), &&DecPlPercBias&)),
        XMLELEMENT("accuracy", FormatRounded(round(case when nominalconc = 0 then null
                                                    when '&&FinalAccuracy&' = 'T' then ((Avg(useconc)-nominalconc)/nominalconc*100)+100
                                                    else ((&&ConcRepresentationFunc&(Avg(useconc_sigrnd), &&ConcFigures&)-nominalconc_sigrnd)/nominalconc_sigrnd*100)+100
                                               end, &&DecPlPercBias&), &&DecPlPercBias&)),
        XMLELEMENT("totalerror", FormatRounded(round(case when nominalconc = 0 then null
                                                    when '&&FinalAccuracy&' = 'T' then Abs((Avg(useconc)-nominalconc)/nominalconc*100) +
                                                                                       FormatRounded(round(Stddev(useconc)/Avg(useconc)*100, &&DecPlPercCV&), &&DecPlPercCV&)
                                                    else Abs((&&ConcRepresentationFunc&(Avg(useconc_sigrnd), &&ConcFigures&)-nominalconc_sigrnd)/nominalconc_sigrnd*100) +
                                                         FormatRounded(round(&&ConcRepresentationFunc&(Stddev(useconc_sigrnd), &&ConcFigures&)/&&ConcRepresentationFunc&(Avg(useconc_sigrnd), &&ConcFigures&)*100, &&DecPlPercCV&), &&DecPlPercCV&)
                                               end, &&DecPlPercTE&), &&DecPlPercTE&)),
        XMLELEMENT("flagged", 
        case when abs(round(case when nominalconc = 0 then null
                                                    when '&&FinalAccuracy&' = 'T' then (Avg(useconc)-nominalconc)/nominalconc*100
                                                    else (&&ConcRepresentationFunc&(Avg(useconc_sigrnd), &&ConcFigures&)-nominalconc_sigrnd)/nominalconc_sigrnd*100
                                               end, &&DecPlPercBias&)) > min(flagpercent) then 'T' else 'F' end),
        XMLELEMENT("flagpercent", min(flagpercent)),
        case when min(knowntype) = 'STANDARD' then XMLELEMENT("anchor",min(anchor)) else null end
        ) as res
    FROM
    (SELECT
     name, subset, &&InterLotCvCalc&
     nominalconc, nominalconc_sigrnd, analyteid &&DilColumn&,
     lloqconv, uloqconv, knowntype, max(isdeactivated) isdeactivated,
     hours, longtermtime, longtermunits, temperature, concentrationunits,
     case when knowntype = 'STANDARD' and (nominalconc_sigrnd<&&ConcRepresentationFunc&(lloqconv, &&ConcFigures&) or nominalconc_sigrnd>&&ConcRepresentationFunc&(uloqconv, &&ConcFigures&)) then 'T' else 'F' end anchor,
     analyteorder, min(knownorder) knownorder, species, matrix,
     Avg(conc),
     &&ConcRepresentationFunc&(Avg(conc), &&ConcFigures&) conc_sigrnd,
     (Avg(conc)-nominalconc)*100/nominalconc bias,
     Stddev(conc)/Avg(conc)*100 cv,
     (&&ConcRepresentationFunc&(Avg(conc), &&ConcFigures&)-nominalconc_sigrnd)*100/nominalconc_sigrnd bias_sigrnd,
     round(&&ConcRepresentationFunc&(Avg(conc), &&ConcFigures&)*100/nominalconc_sigrnd,&&DecPlPercBias&) accuracy_sigrnd,
     round(&&ConcRepresentationFunc&(Stddev(conc), &&ConcFigures&)/&&ConcRepresentationFunc&(Avg(conc), &&ConcFigures&)*100, &&DecPlPercCV&) cv_sigrnd,
     flagpercent,
     acceptedrun, runid, runnr, 
     &&ConcRepresentationFunc&(Avg(case when ISDEACTIVATED = 'T' then null else conc end), &&ConcFigures&) useconc_sigrnd,
     (&&ConcRepresentationFunc&(Avg(case when ISDEACTIVATED = 'T' then null else conc end), &&ConcFigures&)-nominalconc_sigrnd)*100/nominalconc_sigrnd usebias_sigrnd,
     Avg(case when ISDEACTIVATED = 'T' then null else conc end) useconc,
     (Avg(case when ISDEACTIVATED = 'T' then null else conc end)-nominalconc)*100/nominalconc usebias,
     round(&&ConcRepresentationFunc&(Avg(case when ISDEACTIVATED = 'T' then null else conc end), &&ConcFigures&)*100/nominalconc_sigrnd,&&DecPlPercBias&) useaccuracy_sigrnd,
     case when abs(
       case
         when '&&FinalAccuracy&' = 'T' then
           (Avg(case when ISDEACTIVATED = 'T' then null else conc end)-nominalconc)*100/nominalconc
         else
           (&&ConcRepresentationFunc&(Avg(case when ISDEACTIVATED = 'T' then null else conc end), &&ConcFigures&)-nominalconc_sigrnd)*100/nominalconc_sigrnd
       end) > flagpercent then '&&FlaggedSymbol&' else null end flagged,
     case when min(ISDEACTIVATED) <> 'T' and abs(
       case
         when '&&FinalAccuracy&' = 'T' then
           (Avg(case when ISDEACTIVATED = 'T' then null else conc end)-nominalconc)*100/nominalconc
         else
           (&&ConcRepresentationFunc&(Avg(case when ISDEACTIVATED = 'T' then null else conc end), &&ConcFigures&)-nominalconc_sigrnd)*100/nominalconc_sigrnd
       end) <= flagpercent then 1 else null end unflagged,
     Xmlagg(
            XMLELEMENT("sample",
              XMLATTRIBUTES(sampleid AS "sampleid", sampleresultid AS "sampleresultid" ),
              XMLELEMENT("runsamplesequencenumber", runsamplesequencenumber),
              XMLELEMENT("replicatenumber", replicatenumber),
              XMLELEMENT("resultcommenttext", resultcommenttext),
              XMLELEMENT("conctext", conctext),
              XMLELEMENT("analytearea", analytearea),
              XMLELEMENT("deactivated", isdeactivated),
              case
                when isr_deactivation = 'T' then
                  XMLELEMENT("deactivationreason", deactivationreason)
                else null
              end
            ) order by runsamplesequencenumber
          ) xml
     FROM
     (SELECT K.ID knownID,
        s.ID sampleID, sr.ID sampleresultID,
        &&KnownNameTargetField& name,
        k.name subset,
        case when ra.RunAnalyteRegressionStatus = &&RejectedRunAnalyte& then 'F' else 'T' end acceptedrun,
        case
          when d.code is not null then 'T'
          else s.ISDEACTIVATED
        end ISDEACTIVATED,
        case
          when d.code is not null then 'T'
          else 'F'
        end isr_deactivation,
        d.reason deactivationreason,
        &&DilColumnQualif&
        s.runsamplesequencenumber, s.replicatenumber,
        ra.analyteid, sa.analyteorder, a.species, a.sampletypeid matrix,
        kwz.ordernumber knownorder,
        &&ConcRepresentationFunc&(sr.nominalconcentrationconv, &&ConcFigures&) nominalconc_sigrnd,
        sr.nominalconcentrationconv nominalconc, sr.resultcommenttext,
        ra.lloqconv, ra.uloqconv, K.knowntype,
        k.flagpercent, s.runid, r.runid runnr,
        sr.concentrationconv conc, &&StabilityInfo&
        case when '&&ExcludeLLOQULOQFromStats&'='F' then null -- disable the LLOQ/LLOQ-feature, treat as numbers
             when sr.concentrationconv/s.dilutionfactor < ra.lloqconv then '< LLOQ' 
             when sr.concentrationconv/s.dilutionfactor > ra.uloqconv then '> ULOQ' 
             else null 
        end conctext,
        ra.concentrationunits,
        srw.analytearea
      FROM &&TempTabPrefix&bio$run$ana$known K,
           &&TempTabPrefix&bio$run r,
           &&TempTabPrefix&bio$run$analytes ra,
           &&TempTabPrefix&bio$study$analytes sa,
           &&TempTabPrefix&bio$assay a,
           &&TempTabPrefix&bio$run$samples s,
           &&TempTabPrefix&bio$run$sample$results sr,
           &&TempTabPrefix&bio$run$sample$result$raw srw,
           &&TempTabPrefix&bio$covib$run$sample$deact d,
           isr$crit kwz,
           table(&&LALPackage&.GetRunStates) rs
      WHERE K.ID = s.runknownID
        AND R.ID = s.runid AND R.ID = sr.runid
        AND r.RuntypeDescription in (&&RunTypes&)
        AND s.ID = sr.runsampleID
        AND K.knowntype = '&&KnownType&'
        AND s.sampletype = 'known'
        AND s.runsamplekind = '&&KnownType&'
        AND K.knowntype = s.runsamplekind
        AND ra.id = sr.runanalyteid
        AND a.id = r.assayid
        AND sa.id = ra.analyteid
        AND ra.runid = s.runid
        AND K.studyid = '&&StudyID&'
        AND s.studyid = '&&StudyID&'
        AND sr.studyID = '&&StudyID&' AND r.studyid = '&&StudyID&'
        AND ra.studyID = '&&StudyID&'
        AND s.studycode || '-' || r.runid || '-' || s.runsamplesequencenumber = d.code (+)
        AND s.ID = srw.runSampleID (+)
        AND rs.column_value = r.runstatusnum
        AND kwz.key = &&KnownNameJoinField&
        AND kwz.entity = '&&KnownEntity&'
        AND kwz.masterkey = '&&ExpMasterkey&'
        AND repid = &&RepID&
        &&DilCond& &&AddCondition&
     )
     GROUP BY analyteid, analyteorder, species, matrix, name, nominalconc, nominalconc_sigrnd, temperature, hours, longtermtime, longtermunits, concentrationunits, acceptedrun, runid, runnr, knowntype, lloqconv, uloqconv, flagpercent &&DilColumn&, subset &&InterLotCvCalc&
    )
    GROUP BY analyteid, analyteorder, species, matrix, name, nominalconc, nominalconc_sigrnd, temperature, hours, longtermtime, longtermunits, concentrationunits, acceptedrun &&DilColumnGroupBy& &&RunIDColumn&)
    GROUP BY analyteid, analyteorder, species, matrix, temperature, hours, longtermtime, longtermunits, concentrationunits, acceptedrun &&RunIDColumn&
