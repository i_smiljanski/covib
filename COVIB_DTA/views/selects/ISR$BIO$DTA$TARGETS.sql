CREATE OR REPLACE VIEW ISR$BIO$DTA$TARGETS (ENTITY, CODE, NAME, VALUE, ORDERNUMBER, VISIBLE) AS SELECT 'BIO$DTA$TARGETS' Entity
, parametername Code
, utd$msglib.getMsg (description, stb$security.getCurrentLanguage) Name
, parametervalue Value
, orderno OrderNumber
, visible Visible
FROM STB$REPTYPEPARAMETER pv
WHERE parametername LIKE 'DTA_FOR_%'
AND editable = 'T'
AND reporttypeid = STB$OBJECT.getCurrentReporttypeID