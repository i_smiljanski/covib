CREATE OR REPLACE VIEW ISR$BIO$DTA$COLUMN (ENTITY, CODE, COLUMNCODE, TARGETID, DTAID, NAME, FIELD, TYPE, WIDTH, ORDERNUMBER) AS SELECT 'BIO$DTA$COLUMN'
       , c.MasterKey||'@'||df.Field||'@'||df.OrderBy Code
       , df.Field||'@'||df.OrderBy ColumnCode
       , c.MasterKey TargetID
       , df.DTAID DTAID
       , df.Name Name
       , nvl(df.Field,' ') Field
       , df.Type Type
       , df.Width Width
       , df.OrderBY OrderBy
  FROM ISR$COVIB$DTA$FIELDS df, ISR$CRIT c
 WHERE c.Entity = 'BIO$DTA$SELECTION'
   AND c.RepID = STB$OBJECT.getCurrentRepID
   AND df.DTAID = c.Key
 ORDER BY c.MasterKey, df.DTAID, df.OrderBy