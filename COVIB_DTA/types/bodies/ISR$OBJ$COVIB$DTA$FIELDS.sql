CREATE OR REPLACE TYPE BODY isr$obj$covib$dta$fields as

--**********************************************************************************************************************************
static function putParameters(oParameter in stb$menuentry$record, olParameter in stb$property$list, clParameter in clob default null) 
  return stb$oerror$record is
  sCurrentName        constant varchar2(100) := $$PLSQL_UNIT||'.putParameters('||oParameter.sToken||')';
  oErrorObj           STB$OERROR$RECORD := STB$OERROR$RECORD();
  sMsg              varchar2(4000);
  oParamListRec   ISR$PARAMLIST$REC := ISR$PARAMLIST$REC();
  olnodelist  STB$TREENODELIST:= stb$treenodelist ();
  
 begin 
  isr$trace.stat('begin','begin',sCurrentName);
  isr$trace.debug('parameter','olParameter',sCurrentName,olParameter);
  isr$trace.debug('parameter', 'oParameter', sCurrentName, oParameter);
  isr$trace.debug('parameter', 'olNodeList', sCurrentName, olNodeList);
  isr$trace.debug('parameter','length(clParameter): '||length(clParameter),sCurrentName,clParameter);

  --oParamListRec.AddParam('TABLE', 'ISR$COVIB$DTA$FIELDS');
  --oErrorObj := isr$obj$grid().buildGridMask(oParameter.sToken, oParamListRec, olNodeList);
  
  oParamListRec.AddParam('TABLE', 'ISR$COVIB$DTA$FIELDS');
  oParamListRec.AddParam('AUDITTYPE', 'SYSTEMAUDIT');
  oParamListRec.AddParam('AUDITREFERENCE', TO_CHAR (-1));
  oErrorObj := isr$obj$grid().modifyGridTable(oParameter.sToken, clParameter, oParamListRec);
  isr$trace.stat('end','end',sCurrentName);
  
  return oErrorObj;
exception
  when others then
    sMsg :=  utd$msglib.getmsg ('exUnexpectedError', isr$obj$base().nCurrentLanguage, csP1 => SQLERRM(SQLCODE));
    oErrorObj.handleError ( Stb$typedef.cnSeverityCritical, sMsg, sCurrentName,
                            DBMS_UTILITY.format_error_stack || CHR(10) || DBMS_UTILITY.format_error_backtrace );
    return oErrorObj; 
 end putParameters;
 
static function callFunction (oParameter in STB$MENUENTRY$RECORD, olNodeList out STB$TREENODELIST)
  return STB$OERROR$RECORD is
  sCurrentName         constant varchar2(100) := $$PLSQL_UNIT||'.callFunction('||oParameter.sToken||')';
  oErrorObj                     STB$OERROR$RECORD := STB$OERROR$RECORD();
  sMsg              varchar2(4000);
  oParamListRec   ISR$PARAMLIST$REC := ISR$PARAMLIST$REC();
  nGetActiveServers             NUMBER;
begin
  isr$trace.stat('begin', 'oParameter.sToken: '||oParameter.sToken ,sCurrentName);
  isr$trace.debug('parameter', 'oParameter', sCurrentName, oParameter);
  isr$trace.debug('parameter', 'olNodeList', sCurrentName, olNodeList);
    
  STB$OBJECT.setCurrentToken(oparameter.stoken);
  
  oParamListRec.AddParam('TABLE', 'ISR$COVIB$DTA$FIELDS');
  oErrorObj := isr$obj$grid().buildGridMask(oParameter.sToken, oParamListRec, olNodeList);

  
    /*  olnodelist := stb$treenodelist ();
      olnodelist.EXTEND;
      olnodelist (1) := STB$TREENODE$RECORD(STB$UTIL.getMaskTitle(Upper (oParameter.sToken)),
                                            'ISR$REPORTTYPE$PACKAGE', 'TRANSPORT', 'MASK');
      oErrorObj := STB$UTIL.getPropertyList('CAN_MODIFY_DTA_SELECTION', olNodeList(1).tloPropertylist);
      isr$trace.debug('after getPropertyList','olNodeList(1)',sCurrentName,olNodelist(1));
*/
  isr$trace.stat('end', 'end', sCurrentName);
  return oErrorObj;
exception
  when others then
    sMsg :=  utd$msglib.getmsg ('exUnexpectedError', isr$obj$base().nCurrentLanguage, csP1 => SQLERRM(SQLCODE));
    oErrorObj.handleError ( Stb$typedef.cnSeverityCritical, sMsg, sCurrentName,
                            DBMS_UTILITY.format_error_stack || CHR(10) || DBMS_UTILITY.format_error_backtrace );
    return oErrorObj; 
end callFunction;


-- ***************************************************************************************************
static function getLov( sEntity        in varchar2,
                 sSuchwert      in varchar2, 
                 oSelectionList out ISR$TLRSELECTION$LIST ) return STB$OERROR$RECORD 
is
  sCurrentName constant varchar2(100) := $$PLSQL_UNIT||'.getLov(' || sEntity || ')';
  sMsg         varchar2(4000);
  nCounter     number            := 0;
  oErrorObj    STB$OERROR$RECORD := STB$OERROR$RECORD();

begin
  isr$trace.stat('begin '||sEntity, 'begin', sCurrentName);
  oSelectionList := ISR$TLRSELECTION$LIST ();

   CASE sEntity
        WHEN 'DTAID'THEN
            FOR rGetDta IN (select dtaid ,description
                                 from isr$covib$dta 
                                where editable ='T' ) LOOP
              oSelectionList.EXTEND;
              oSelectionList (oSelectionList.COUNT()) := ISR$OSELECTION$RECORD(rGetDta.dtaid, rGetDta.dtaid, rGetDta.dtaid, null, oSelectionList.COUNT());
            END LOOP;       

              isr$obj$base.setLovListSelection(ssuchwert, oSelectionList);
        WHEN 'FIELD' THEN 
            oSelectionList.extend;
            oSelectionList (oSelectionList.COUNT()) := ISR$OSELECTION$RECORD(null, null, null,  null, oSelectionList.COUNT(), 'STRING');
            FOR rGetField IN (select field,type, UTD$MSGLIB.getMsg (field, stb$security.GetCurrentLanguage) translation 
                             from ISR$COVIB$VALID$DTAFIELDS
                            ) LOOP
              oSelectionList.EXTEND;
              oSelectionList (oSelectionList.COUNT()) := ISR$OSELECTION$RECORD(rGetField.field, rGetField.field, rGetField.translation,  null, oSelectionList.COUNT(), rGetField.type);
            END LOOP;
              isr$obj$base.setLovListSelection(ssuchwert, oSelectionList);
        ELSE
            oErrorObj := isr$obj$base.getLov( sEntity,
                 sSuchwert, 
                 oSelectionList );

    END CASE; 
  
  isr$trace.stat ('end', 'oSelectionList.count() = ' || oSelectionList.count(), sCurrentName);
  return oErrorObj;
exception
  when others then
    sMsg :=  utd$msglib.getmsg ('exUnexpectedError', stb$security.getcurrentlanguage, csP1 => sqlerrm(sqlcode));
    oErrorObj.handleError ( Stb$typedef.cnSeverityCritical, sMsg, sCurrentName, 
                            substr(dbms_utility.format_error_stack || CHR(10) || dbms_utility.format_error_backtrace, 1,4000)  );  
    return oErrorObj;    
end getLov;

end;