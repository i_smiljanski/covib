CREATE OR REPLACE TYPE BODY isr$obj$covib$dta as

--**********************************************************************************************************************************
static function putParameters(oParameter in stb$menuentry$record, olParameter in stb$property$list, clParameter in clob default null) 
  return stb$oerror$record is
  sCurrentName        constant varchar2(100) := $$PLSQL_UNIT||'.putParameters('||oParameter.sToken||')';
  oErrorObj           STB$OERROR$RECORD := STB$OERROR$RECORD();
  sMsg              varchar2(4000);
  oParamListRec   ISR$PARAMLIST$REC := ISR$PARAMLIST$REC();
  sDuplicatedRowid   varchar2(100);
  sDta_neu   varchar2(100);
  sDta_old   varchar2(100);
  cursor cDtaIdDuplicate is
    select DTAID,DUPLICATED_ROWID from ISR$COVIB$DTA 
    where DUPLICATED_ROWID is not null
    order by 1;
  
 begin 
  isr$trace.stat('begin','begin',sCurrentName);
  oParamListRec.AddParam('TABLE', 'ISR$COVIB$DTA');
  oParamListRec.AddParam('AUDITTYPE', 'SYSTEMAUDIT');
  oParamListRec.AddParam('AUDITREFERENCE', TO_CHAR (-1));
  oErrorObj := isr$obj$grid().modifyGridTable(oParameter.sToken, clParameter, oParamListRec);
    for rDtaIdDuplicate in cDtaIdDuplicate loop
      sDta_neu := rDtaIdDuplicate.DTAID;
      sDuplicatedRowid := rDtaIdDuplicate.DUPLICATED_ROWID;
      select DTAID into sDta_old from ISR$COVIB$DTA where ROWID=sDuplicatedRowid;
      isr$trace.debug ('Dta_neu', sDta_neu, sCurrentName);
      isr$trace.debug ('DuplicatedRowid', sDuplicatedRowid, sCurrentName);
      isr$trace.debug ('Dta_old', sDta_old, sCurrentName);
      INSERT INTO isr$covib$dta$fields
      (DTAID,NAME,FIELD,TYPE,LENGTH,MASK,EDITABLE,PLUGIN)
        SELECT sDta_neu,NAME,FIELD,TYPE,LENGTH,MASK,EDITABLE,PLUGIN
        FROM isr$covib$dta$fields where DTAID in sDta_old;

      UPDATE ISR$COVIB$DTA
        SET DUPLICATED_ROWID = ''
        where DUPLICATED_ROWID=sDuplicatedRowid;

    end loop;
    commit;

  isr$trace.stat('end','end',sCurrentName);
  return oErrorObj;
exception
  when others then
    sMsg :=  utd$msglib.getmsg ('exUnexpectedError', isr$obj$base().nCurrentLanguage, csP1 => SQLERRM(SQLCODE));
    oErrorObj.handleError ( Stb$typedef.cnSeverityCritical, sMsg, sCurrentName,
                            DBMS_UTILITY.format_error_stack || CHR(10) || DBMS_UTILITY.format_error_backtrace );
    return oErrorObj; 
 end putParameters;
 
static function callFunction (oParameter in STB$MENUENTRY$RECORD, olNodeList out STB$TREENODELIST)
  return STB$OERROR$RECORD is
  sCurrentName         constant varchar2(100) := $$PLSQL_UNIT||'.callFunction('||oParameter.sToken||')';
  oErrorObj                     STB$OERROR$RECORD := STB$OERROR$RECORD();
  sMsg              varchar2(4000);
  oParamListRec   ISR$PARAMLIST$REC := ISR$PARAMLIST$REC();
begin
  isr$trace.stat('begin', 'oParameter.sToken: '||oParameter.sToken ,sCurrentName);
  isr$trace.debug('parameter', 'oParameter', sCurrentName, oParameter);
    
  STB$OBJECT.setCurrentToken(oparameter.stoken);
  
  oParamListRec.AddParam('TABLE', 'ISR$COVIB$DTA');
  oErrorObj := isr$obj$grid().buildGridMask(oParameter.sToken, oParamListRec, olNodeList);
  isr$trace.stat('end', 'end', sCurrentName);
  return oErrorObj;
exception
  when others then
    sMsg :=  utd$msglib.getmsg ('exUnexpectedError', isr$obj$base().nCurrentLanguage, csP1 => SQLERRM(SQLCODE));
    oErrorObj.handleError ( Stb$typedef.cnSeverityCritical, sMsg, sCurrentName,
                            DBMS_UTILITY.format_error_stack || CHR(10) || DBMS_UTILITY.format_error_backtrace );
    return oErrorObj; 
end callFunction;
static function getLov( sEntity        in varchar2,
                 sSuchwert      in varchar2, 
                 oSelectionList out ISR$TLRSELECTION$LIST ) return STB$OERROR$RECORD 
is
  sCurrentName constant varchar2(100) := $$PLSQL_UNIT||'.getLov(' || sEntity || ')';
  sMsg         varchar2(4000);
  nCounter     number            := 0;
  oErrorObj    STB$OERROR$RECORD := STB$OERROR$RECORD();

begin
  isr$trace.stat('begin '||sEntity, 'begin', sCurrentName);
  oSelectionList := ISR$TLRSELECTION$LIST ();

   CASE sEntity
        WHEN 'PRESENTATION'THEN
            FOR rGetFormat IN (select value 
                                 from isr$parameter$values
                                where editable ='T' and entity='EXPORTFORMAT' ) LOOP
              oSelectionList.EXTEND;
              oSelectionList (oSelectionList.COUNT()) := ISR$OSELECTION$RECORD(rGetFormat.value, rGetFormat.value, rGetFormat.value, null, oSelectionList.COUNT());
            END LOOP;       

              isr$obj$base.setLovListSelection(ssuchwert, oSelectionList);
        ELSE
            oErrorObj := isr$obj$base.getLov( sEntity,
                 sSuchwert, 
                 oSelectionList );

    END CASE; 
  
  isr$trace.stat ('end', 'oSelectionList.count() = ' || oSelectionList.count(), sCurrentName);
  return oErrorObj;
exception
  when others then
    sMsg :=  utd$msglib.getmsg ('exUnexpectedError', stb$security.getcurrentlanguage, csP1 => sqlerrm(sqlcode));
    oErrorObj.handleError ( Stb$typedef.cnSeverityCritical, sMsg, sCurrentName, 
                            substr(dbms_utility.format_error_stack || CHR(10) || dbms_utility.format_error_backtrace, 1,4000)  );  
    return oErrorObj;    
end getLov;


-- ***************************************************************************************************

end;