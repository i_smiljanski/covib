package com.utd.isr.snapshot.db;//CREATE OR REPLACE AND RESOLVE JAVA SOURCE NAMED "com/utd/isr/snapshot/db/JAVA$ISR$SNAPSHOT" as

import com.uptodata.isr.db.trace.DbLogging;
import com.uptodata.isr.utils.ConvertUtils;
import com.uptodata.isr.utils.xml.dom.XmlHandler;
import oracle.jdbc.OracleCallableStatement;
import oracle.jdbc.OracleConnection;
import oracle.jdbc.OracleResultSet;
import oracle.jdbc.OracleStatement;
import org.apache.commons.lang.StringUtils;
//import org.apache.logging.log4j.LogManager;
//import org.apache.logging.log4j.Logger;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;

import java.io.*;
import java.sql.*;
import java.util.ArrayList;
import java.util.Hashtable;
import java.util.Map;
import java.util.Properties;

//import com.utd.isr.snapshot.utils.XmlHandler;

public class JAVA$ISR$SNAPSHOT {
 //   private static Logger log = LogManager.getRootLogger();
    private OracleConnection connection;

    // fields:
    private OracleCallableStatement addMessageStatement;
    private JAVA$ISR$SNAPSHOT() throws Exception {

        this("jdbc:default:connection", "BEGIN ISR$SNAPSHOT.addMessage(?, ?, ?); END;");

    }

    // constructors:

    public JAVA$ISR$SNAPSHOT(String user, String password, String host, String port, String SID) throws Exception {

        this("jdbc:oracle:thin:" + user + "/" + password + "@" + host + ":" + port + ":" + SID);

    }

    public JAVA$ISR$SNAPSHOT(String jdbcConnectionString) throws SQLException {

        this(jdbcConnectionString, "BEGIN ISR$SNAPSHOT.addMessage(?, ?, ?); END;");

    }

    public JAVA$ISR$SNAPSHOT(String jdbcConnectionString, String logMethodStatement) throws SQLException {

        OracleCallableStatement callableStatement;

//		try {

        this.setConnection((OracleConnection) DriverManager.getConnection(jdbcConnectionString));
        this.setAddMessageStatement((OracleCallableStatement) this.getConnection().prepareCall(logMethodStatement));

        callableStatement = (OracleCallableStatement) this.getConnection().prepareCall("BEGIN ISR$SNAPSHOT.prepareMessageStore; END;");
        callableStatement.execute();
        callableStatement.close();

//		catch (Exception ex) {
//			throw new Exception("Creating snapshot interface for DB failed!", ex); }

    }

    public static Blob getSnapshotAsBlob(Clob includeList, Clob excludeList) throws Exception {

        /*
         * first, create snapshot interface
         */
        JAVA$ISR$SNAPSHOT snapshotInterface = new JAVA$ISR$SNAPSHOT();

        /*
         * create bytes for the given clob lists
         */
        snapshotInterface.addMessage("D", "java.getSnapshotAsBlob", "preparing lists..");
        byte[] include = JAVA$ISR$SNAPSHOT.CLOBToBytes(includeList);
        byte[] exclude = JAVA$ISR$SNAPSHOT.CLOBToBytes(excludeList);
        snapshotInterface.addMessage("I", "java.getSnapshotAsBlob", "lists prepared!");

        /*
         * then call create snapshot
         */
        snapshotInterface.addMessage("D", "java.getSnapshotAsBlob", "creating snapshot..");
        DBSnapshot snapshot = snapshotInterface.createSnapshot(include, exclude);
        snapshotInterface.addMessage("I", "java.getSnapshotAsBlob", "snapshot created!");

        /*
         * now create a blob
         */
        snapshotInterface.addMessage("D", "java.getSnapshotAsBlob", "creating BLOB..");
        Blob blob = snapshotInterface.getConnection().createBlob();
//        if (blob == null) throw new Exception("creating a BLOB failed! Sorry, no error message available!");
//        snapshotInterface.addMessage("I", "java.getSnapshotAsBlob", "BLOB created!");

        /*
         * write snapshot to BLOB
         */
        snapshotInterface.addMessage("D", "java.getSnapshotAsBlob", "writing snapshot to BLOB..");
        snapshot.toBLOB(blob);
        snapshotInterface.addMessage("I", "java.getSnapshotAsBlob", "snapshot written to BLOB!");

        snapshotInterface.addMessage("D", "java.getSnapshotBLOB", "closing connection and returning the BLOB..");
        snapshotInterface.getConnection().close();

        return blob;

    }

    // methods:

    private static byte[] CLOBToBytes(Clob clob) throws Exception {

        Reader reader;
        ByteArrayOutputStream baos;
        OutputStreamWriter writer;

        try {

            baos = new ByteArrayOutputStream();
            writer = new OutputStreamWriter(baos, "UTF-8");

            if (clob == null) return null;

            reader = clob.getCharacterStream();

            int read;
            char[] buffer = new char[4096];
            while ((read = reader.read(buffer)) != -1) writer.write(buffer, 0, read);

            reader.close();
            writer.close();

            return baos.toByteArray();

        } catch (Exception ex) {

            String msg = "creating bytes for clob failed! " + ex.getClass() + " with message: " + ex.getMessage();
            throw new Exception(msg);

        }

    }

    public ArrayList<String> getDBResultSelect(String selectStatement) {
        ArrayList<String> DBResultSelect = new ArrayList();
        ResultSet rs = null;
        if (this.getConnection() != null) {
            //log.info(" connection != null  " + selectStatement);
            Statement statement;
            try {
                statement = this.getConnection().createStatement();

                rs = statement.executeQuery(selectStatement);
            } catch (SQLException e) {
                e.printStackTrace();
            }
            try {
                while (rs.next()) {
                    DBResultSelect.add(rs.getString(1));
                }
            } catch (SQLException e) {
                e.printStackTrace();
            }
        }
        return DBResultSelect;
    }

    public DBSnapshot createSnapshot(byte[] include, byte[] exclude) throws Exception {
        DbLogging log = new DbLogging();

        DBSnapshot snapshot;

        SnapshotObjectsReader snapshotObjectsReader;

        OracleCallableStatement callableStatement;
        OracleStatement statement;
        OracleResultSet resultSet;

        try {
            log.info("prepareMessageStore..", "");
            callableStatement = (OracleCallableStatement) this.getConnection().prepareCall("BEGIN ISR$SNAPSHOT.prepareMessageStore; END;");
            callableStatement.execute();
            callableStatement.close();
            addMessage("D", "java.createSnapshot", "message table prepared!..");
            /*
             * set include and exclude lists
             */
            addMessage("D", "java.createSnapshot", "setting include and exclude lists..");
            this.setIncludeList(include);
            this.setExcludeList(exclude);
            addMessage("I", "java.createSnapshot", "include and exclude lists set!");

            /*
             * then, call 'collectAll' function.
             */
            addMessage("D", "java.createSnapshot", "collecting objects..");
            callableStatement = (OracleCallableStatement) this.getConnection().prepareCall("BEGIN ISR$SNAPSHOT.collectAll; END;");
            callableStatement.execute();
            callableStatement.close();
            addMessage("I", "java.createSnapshot", "objects collected!");

            /*
             * after the call of 'collectAll' function, objects are in the 'ISR$SNAPSHOT$OBJECTS' table
             */
            addMessage("D", "java.createSnapshot", "query objects..");
            statement = (OracleStatement) this.getConnection().createStatement();
            resultSet = (OracleResultSet) statement.executeQuery("SELECT	ISO.MODULE AS MODULE, " + "		ISO.TYPE AS TYPE, " + "		ISO.NAME AS NAME, " + "		ISO.SNAPSHOT_OBJECT AS SNAPSHOT_OBJECT " + "FROM	ISR$SNAPSHOT$OBJECTS ISO, ISR$SNAPSHOT$COLLECT ISC " + "WHERE	ISO.MODULE = ISC.MODULE AND ISO.TYPE = ISC.TYPE AND ISO.NAME = ISC.NAME " + "UNION ALL " + "SELECT	ISO.MODULE, " + "		'META', " + "		ISO.NAME, " + "		ISO.SNAPSHOT_OBJECT " + "FROM	ISR$SNAPSHOT$OBJECTS ISO " + "WHERE	ISO.TYPE = 'OBJECT_LIST' AND (ISO.NAME = 'INCLUDE' OR ISO.NAME = 'EXCLUDE')");
            addMessage("I", "java.createSnapshot", "objects queried!");

            /*
             * create the snapshot reader object
             */
            addMessage("D", "java.createSnapshot", "creating snapshot objects reader..");
            snapshotObjectsReader = new SnapshotObjectsReader(resultSet, "MODULE", "TYPE", "NAME", "SNAPSHOT_OBJECT");
            snapshotObjectsReader.next();
            addMessage("I", "java.createSnapshot", "snapshot objects reader created!");

            snapshot = new DBSnapshot(snapshotObjectsReader);


        } catch (Exception ex) {

            String msg = "creating snapshot failed! " + ex.getClass() + " with message: " + ex.getMessage();
            throw new Exception(msg, ex);

        }

        return snapshot;

    }

    private void setIncludeList(byte[] includeBytes) {
        addMessage("D", "setIncludeList", "");
        if (includeBytes != null) {
            String temp = new String(includeBytes);
            addMessage("D", "setIncludeList  111", "");
            addMessage("D", "includeBytes", temp);
        }
        this.setList("INCLUDE", includeBytes);

    }

    private void setExcludeList(byte[] excludeBytes) {

        this.setList("EXCLUDE", excludeBytes);

    }

    private void setList(String listType, byte[] listBytes) {
        DbLogging log = new DbLogging();
        String functionToCall;
        String functionParam = "NULL";
        addMessage("D", "setList  00", "");
        String statement;
        OracleCallableStatement callableStatement;


        try {

            if (!listType.equalsIgnoreCase("INCLUDE") && !listType.equalsIgnoreCase("EXCLUDE"))
                throw new Exception("unknown list type [" + listType + "]! only INCLUDE or EXCLUDE are alowed!");
            else functionToCall = "set" + listType.trim() + "List";
            addMessage("D", "functionToCall: ", functionToCall);
            Clob listClob = null;

            if (listBytes != null) {
                listClob = getConnection().createClob();
                addMessage("D", "listBytes!= null    ", "");
                functionParam = "?";

				/*since internal ojvm is using ojdbc from oracle version 11.2.0.3 to establish a 'oracle:default:connection',
                  calling the Connection.createClob() method will lead to an AbstractMethodException
				  since this method is not implemented in the old version of ojdbc */
                //listClob = this.getConnection().createClob();
                /*instead, methods from oracles CLOB class can be used to cerate a CLOB object*/


                //listClob = CLOB.createTemporary(this.getConnection(), true, CLOB.DURATION_SESSION);
                //listClob.open(CLOB.MODE_READWRITE);

                Writer writer = listClob.setCharacterStream(0L);
                writer.write(new String(listBytes, "UTF-8"));
                //writer.write(temp);

                writer.flush();
                writer.close();

            }

            statement = "BEGIN ISR$SNAPSHOT." + functionToCall + "(" + functionParam + "); END;";
            addMessage("D", "statement: ", statement);
            callableStatement = (OracleCallableStatement) this.getConnection().prepareCall(statement);

            if (listClob != null) callableStatement.setClob(1, listClob);

            callableStatement.execute();
            callableStatement.close();

        } catch (Exception ex) {

            String msg = ex.getClass() + " in com.utd.isr.snapshot.JAVA$ISR$SNAPSHOT.setList(listType, listBytes): " + ex.getMessage();

            addMessage("E", "java.setList", msg);

        }

    }

    private void addMessage(String messageType, String messagePlace, String messageText) {
        DbLogging log = new DbLogging();

        try {

            this.getAddMessageSatement().setString(1, messageType);
            this.getAddMessageSatement().setString(2, messagePlace);
            this.getAddMessageSatement().setString(3, messageText);

            this.getAddMessageSatement().execute();
            log.debug(messageText, "");

        } catch (Exception ex) {

            String msg = ex.getClass() + " in com.utd.isr.snapshot.JAVA$ISR$SNAPSHOT.addMessage(): " + ex.getMessage();

            // TODO: handle exception
            System.err.println(messagePlace + ": " + msg);
            //ex.printStackTrace();

        }

    }

    public OracleConnection getConnection() {

        return this.connection;

    }

    // getter&setter:

    private void setConnection(OracleConnection connection) {

        this.connection = connection;

    }

    private OracleCallableStatement getAddMessageSatement() {

        return this.addMessageStatement;

    }

    private void setAddMessageStatement(OracleCallableStatement addMessageStatement) {

        this.addMessageStatement = addMessageStatement;

    }

    public int getDBObjectsExist(String TableDbName) {
        int ncount = 0;
        ResultSet rs = null;
        if (this.connection != null) {
            String checkExistTable = "SELECT count(*) FROM all_tables where table_name='" + TableDbName + "'";

            Statement statement = null;
            try {
                statement = this.connection.createStatement();
                rs = statement.executeQuery(checkExistTable);

            } catch (SQLException e) {
                e.printStackTrace();
            }
            try {
                while (rs.next()) {
                    ncount = rs.getInt(1);
                }
            } catch (SQLException e) {
                e.printStackTrace();
            }
        }
        return ncount;
    }

    public void tableObjects_Plugins(String[] args) {


        File obPlugins = new File("lib/xmls", "OBJECTS_PLUGINS.xml");
        byte[] obPluginslXml = null;
        try {
            obPluginslXml = ConvertUtils.fileToBytes(obPlugins.toString());
        } catch (IOException e) {
            e.printStackTrace();
        }
        ;
        if (obPluginslXml != null) {
            boolean saxon = true;
            XmlHandler obPluginslXmllHandler = new XmlHandler(obPluginslXml, saxon);
            String parameterStatement = "";
            StringBuilder parameterStatementsScript = new StringBuilder();
            Statement statement = null;

            NodeList objectPluginEntries = (NodeList) obPluginslXmllHandler.findXpath("//OBJECT_PLUGIN", "NODESET");
            int max = objectPluginEntries.getLength();
            // iterate over ordered sequence of scripts
            // finding a record(i)  with the desired orderno=file_order
            for (int i = 0; i < max; i++) {
                Node objectPluginNode = objectPluginEntries.item(i);

                String name = obPluginslXmllHandler.attributeRead("NAME", objectPluginNode);
                String type = obPluginslXmllHandler.attributeRead("TYPE", objectPluginNode);
                String plugin = obPluginslXmllHandler.attributeRead("PLUGIN", objectPluginNode);
                //System.out.println("name = " + name);

                parameterStatement = "INSERT INTO " + "ISR$OBJECTS$PLUGINS" + " (OBJECT_MODULE, OBJECT_TYPE,OBJECT_NAME, PLUGIN_NAME)" + " VALUES (" + "'ISR','" + type + "', " + "'" + name + "', " + "'" + plugin + "'" + ")";
                parameterStatementsScript.append(parameterStatement).append(";\n");
            }
            System.out.println("parameterStatement = " + parameterStatementsScript);
            try {

                statement = (Statement) connection.createStatement();
                statement.execute("BEGIN " + parameterStatementsScript + " END;");
                statement.close();
                System.out.println("Install parameters written..");
            } catch (Exception ex) {
                System.out.println("Executing install parameter statements failed! " + ex.getMessage());
            }

        }
    }

    public void getDBResultSelectToFile(String selectStatement) {
        File obPlugins = new File("lib/xmls", "OBJECTS_PLUGINS_test.xml");
        Properties DBResultSelect = new Properties();
        ResultSet rs = null;
        if (getConnection() != null) {
            //log.info(" connection != null  " + selectStatement);
            Statement statement;
            try {
                statement = connection.createStatement();

                rs = statement.executeQuery(selectStatement);
            } catch (SQLException e) {
                e.printStackTrace();
            }
            try {
                while (rs.next()) {
                    String string1 = rs.getString(1);
                    String string2 = rs.getString(2);
                    if (StringUtils.isNotBlank(string2)) DBResultSelect.setProperty(string1, string2);
                }
            } catch (SQLException e) {
                e.printStackTrace();
            }
        }
    }

    public void convertDbToFile() {
        DbLogging log = new DbLogging();
        XmlHandler logXmlHandler;
        String selectStatement = "select OBJECT_MODULE AS MODULE, OBJECT_TYPE AS TYPE,  OBJECT_NAME  AS NAME ,PLUGIN_NAME AS PLUGIN from isr$objects$plugins";
        // ArrayList<String> DBResultSelect = new ArrayList<>();


        if (StringUtils.isNotEmpty(selectStatement)) {
            ResultSet rs = null;
            if (connection != null) {
                log.info(" connection != null  ", selectStatement);
                Statement statement;
                try {
                    statement = connection.createStatement();
                    rs = statement.executeQuery(selectStatement);
                    boolean saxon = true;
                    logXmlHandler = new XmlHandler(saxon);
                    logXmlHandler.nodeRoot("OBJECTS_PLUGINS");

                    Node nodeLoad = logXmlHandler.getNode("OBJECTS_PLUGINS");
                    while (rs.next()) {
                        String str = rs.getString(1);
                        //DBResultSelect.add(rs.getString(1));
                        Map<String, String> attributes = new Hashtable();
                        ;
                        for (Attribut atr : Attribut.values()) {
                            String st = rs.getString(atr.ordinal() + 1);
                            if (StringUtils.isNotEmpty(st)) {
                                attributes.put(atr.name(), st);
                            }
                        }
                                /*attributes.put("MODULE",rs.getString(1));
                                attributes.put("TYPE",rs.getString(2));
                                attributes.put("NAME",rs.getString(3));
                                attributes.put("PLUGIN",rs.getString(4));*/
                        logXmlHandler.createElement("OBJECT_PLUGIN", null, attributes, nodeLoad);
                    }

                    logXmlHandler.save("lib/xmls/OBJECTS_PLUGINS.xml");
                } catch (SQLException e) {
                    log.info("No table 'ISR$OBJECT$PLUGINS', reading data from file 'OBJECTS_PLUGINS.xml'.", "");
                }
            }
        }

    }

    public enum Attribut {
        MODULE, TYPE, NAME, PLUGIN
    }

}
