package com.utd.isr.snapshot.db;//CREATE OR REPLACE AND RESOLVE JAVA SOURCE NAMED "com/utd/isr/snapshot/db/DBSnapshot" as

//import org.apache.logging.log4j.LogManager;
//import org.apache.logging.log4j.Logger;
import com.uptodata.isr.db.trace.DbLogging;

import java.io.OutputStream;
import java.io.OutputStreamWriter;
import java.io.Writer;
import java.nio.charset.StandardCharsets;
import java.sql.Blob;
import java.util.zip.ZipEntry;
import java.util.zip.ZipOutputStream;

public class DBSnapshot {
/*
    static Logger log = LogManager.getRootLogger();
    static Logger logger = LogManager.getLogger(DBSnapshot.class.getName());
    static Logger logerror = LogManager.getLogger("warnError");
*/

    // fields:

    private SnapshotObjectsReader objectsReader;

    // constructors:

    public DBSnapshot(SnapshotObjectsReader objectsReader) {

        this.setSnapshotObjectsReader(objectsReader);

    }

    // methods:

    public void toBLOB(Blob blobOut) throws Exception {

        this.toStream(blobOut.setBinaryStream(0L));

    }

    public void toZip(ZipOutputStream zipOut) throws Exception {

        this.toZip(zipOut, true);

    }

    public void toZip(ZipOutputStream zipOut, boolean closeZipAfterFinish) throws Exception {
        DbLogging log = new DbLogging();
        log.debug("begin","");
        Writer writer = null;
        ZipEntry zipEntry = null;

        try {

            writer = new OutputStreamWriter(zipOut, StandardCharsets.UTF_8);
            log.debug("writer","");
            zipEntry = new ZipEntry(this.getSnapshotObjectsReader().getCurrentEntry());
            log.debug("zipEntry","");
            zipOut.putNextEntry(zipEntry);

            char[] buffer = new char[SnapshotObjectsReader.BUFFER_SIZE];
            int read = -1;
            while ((read = this.getSnapshotObjectsReader().read(buffer)) != -1) {

                if (!this.getSnapshotObjectsReader().getCurrentEntry().equalsIgnoreCase(zipEntry.getName())) {

                    zipEntry = new ZipEntry(this.getSnapshotObjectsReader().getCurrentEntry());
                    writer.flush();
                    zipOut.putNextEntry(zipEntry);

                }

                writer.write(buffer, 0, read);

            }

            writer.flush();
            writer.close();

            this.getSnapshotObjectsReader().close();
            if (closeZipAfterFinish) zipOut.close();
            log.debug("end","");
        } catch (Exception ex) {

            throw new Exception("Writing snapshot to zip failed!", ex);
        }

    }

    public void toStream(OutputStream out) throws Exception {

        this.toZip(new ZipOutputStream(out));

    }

    // getter&setter:

    public SnapshotObjectsReader getSnapshotObjectsReader() {

        return this.objectsReader;

    }

    private void setSnapshotObjectsReader(SnapshotObjectsReader objectsReader) {

        this.objectsReader = objectsReader;

    }

}
