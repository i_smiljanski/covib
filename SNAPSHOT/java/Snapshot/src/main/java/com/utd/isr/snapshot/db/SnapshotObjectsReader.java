package com.utd.isr.snapshot.db;//CREATE OR REPLACE AND RESOLVE JAVA SOURCE NAMED "com/utd/isr/snapshot/db/SnapshotObjectsReader" as

//import org.apache.logging.log4j.LogManager
// import org.apache.logging.log4j.Logger;

import com.uptodata.isr.db.trace.DbLogging;

import java.io.IOException;
import java.io.Reader;
import java.sql.ResultSet;
import java.sql.SQLException;

public class SnapshotObjectsReader extends Reader {

    // fields:

    private ResultSet resultSet;
    private String moduleColumnName;
    private String typeColumnName;
    private String nameColumnName;
    private String snapshotColumnName;

    private Reader currentStream;
    private String currentEntry;

    final static int BUFFER_SIZE = 4096;
    //static Logger log = LogManager.getRootLogger();

    // constructors:

    public SnapshotObjectsReader(ResultSet resultSet, String moduleColumnName, String typeColumnName, String nameColumnName, String snapshotColumnName) {
        DbLogging log = new DbLogging();
        String temp = moduleColumnName + "; " + typeColumnName + "; " + nameColumnName + "; " + snapshotColumnName;
        log.debug("begin",temp);
        this.setResultSet(resultSet);
        this.setModuleColumnName(moduleColumnName);
        this.setTypeColumnName(typeColumnName);
        this.setNameColumnName(nameColumnName);
        this.setSnapshotColumnName(snapshotColumnName);
    }

    // methods:

    /**
     * get the next result row using the ResultSet.next() method.
     * If another row is returned by the result set, then set the char stream of the snapshot object is set as current stream.
     * Use module, type and name to construct the entry name. set this entry name as current entry.
     *
     * @return
     * @throws Exception
     */
    boolean next() throws Exception {

		/*
		 * get next row from the result set
		 */
        boolean nextStreamFlag = this.getResultSet().next();
		/*
		 * if a row is returned, set current stream and entry
		 */
        if (nextStreamFlag) {

			/*
			 * if set, close previous current stream
			 */
            if (this.getCurrentStream() != null) this.getCurrentStream().close();
			/*
			 * retrieve the char stream of the snapshot object and set as current
			 */
            this.setCurrentStream(this.getResultSet().getClob(this.getSnapshotColumnName()).getCharacterStream());

			/*
			 * module, type and name are used to create an entry
			 */
            String module = this.getResultSet().getString(this.getModuleColumnName());
            String type = this.getResultSet().getString(this.getTypeColumnName());
            String name = this.getResultSet().getString(this.getNameColumnName());

			/*
			 *
			 * an entry is defined by the module, type and name of the objects
			 */
            String entry = module + "/" + type + "/" + name + ".xml";
//            log.debug(entry);
            this.setCurrentEntry(entry);
//			String objRef = "[" + module + "].[" + type + "].[" + name + "]";
//			if ((module + type + name).indexOf('/') >= 0)/*TODO: was ist zu tun?*/;

        }

        return nextStreamFlag;

    }

    @Override
    public int read(char[] cbuf, int off, int len) throws IOException {

        int read = this.getCurrentStream().read(cbuf, off, len);

        try {

            if (read == -1)
                if (this.next()) {
                    read = this.getCurrentStream().read(cbuf, off, len);
                }
        } catch (Exception ex) {

            throw new IOException(ex.getMessage());
        }

        return read;

    }

    @Override
    public void close() throws IOException {

        try {

            if (this.resultSet != null)
                this.resultSet.getStatement().close();
        } catch (SQLException ex) {

            throw new IOException(ex.getMessage());
        }

    }

    // getter&setter:

    private ResultSet getResultSet() {

        return this.resultSet;

    }

    private void setResultSet(ResultSet resultSet) {

        this.resultSet = resultSet;

    }

    private String getModuleColumnName() {

        return this.moduleColumnName;

    }

    private void setModuleColumnName(String moduleColumnName) {

        this.moduleColumnName = moduleColumnName;

    }

    private String getTypeColumnName() {

        return this.typeColumnName;

    }

    private void setTypeColumnName(String typeColumnName) {

        this.typeColumnName = typeColumnName;

    }

    private String getNameColumnName() {

        return this.nameColumnName;

    }

    private void setNameColumnName(String nameColumnName) {

        this.nameColumnName = nameColumnName;

    }

    private String getSnapshotColumnName() {

        return this.snapshotColumnName;

    }

    private void setSnapshotColumnName(String snapshotColumnName) {

        this.snapshotColumnName = snapshotColumnName;

    }

    private Reader getCurrentStream() {

        return this.currentStream;

    }

    private void setCurrentStream(Reader currentStream) {

        this.currentStream = currentStream;

    }

    String getCurrentEntry() {

        return this.currentEntry;

    }

    private void setCurrentEntry(String currentEntry) {

        this.currentEntry = currentEntry;

    }

}