CREATE OR REPLACE FUNCTION ISR$SNAPSHOT$SRC$LINEAGG(lines IN ISR$SNAPSHOT$SOURCECODE$LINE)
RETURN CLOB AS

	output CLOB := EMPTY_CLOB();
	temp_clob CLOB := EMPTY_CLOB();

BEGIN

	FOR i IN 1..lines.COUNT LOOP
	  if i = lines.COUNT then
      temp_clob := replace(lines(i), chr(10), '');
    else
      temp_clob := lines(i);
    end if;
    output := output || temp_clob;    
		if temp_clob != chr(10) then
      DBMS_LOB.FREETEMPORARY(temp_clob);
    end if;
	END LOOP;
	
	RETURN output;

END ISR$SNAPSHOT$SRC$LINEAGG;