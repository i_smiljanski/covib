WITH
	OBJECT_REF (MODULE, TYPE, NAME) AS (
	SELECT		:m_name,
			:t_name,
			:o_name
	FROM DUAL)
SELECT	o_ref.MODULE,
		o_ref.TYPE,
		o_ref.NAME,
		/*ISR$SNAPSHOT.getXMLTypeForStatement(TAB.TABLE_SELECT, TAB.ORDER_COLS) SNAPSHOT_OBJECT*/
		ISR$SNAPSHOT.getXMLTypeForStatement(TAB.TABLE_SELECT, NULL) SNAPSHOT_OBJECT
FROM	OBJECT_REF o_ref,
		XMLTABLE('	let $tab := /TABLE
					let $tab_name := $tab/TABLE_NAME
					let $all_cols := $tab/COLUMNS/COLUMN[COLUMN_NAME != "MODIFIEDON" and COLUMN_NAME != "MODIFIEDBY"]
					let $all :=	string-join(
									$all_cols/
										string(
											concat(	''XMLELEMENT("COLUMN", XMLATTRIBUTES('''''', COLUMN_NAME, '''''' AS NAME, '''''', EXCLUDED, '''''' AS EXTERNAL), '',
													if ((DATA_TYPE = "BLOB" or DATA_TYPE = "XMLTYPE") and EXCLUDED="T") then 
                            ''null''
                          else if ((DATA_TYPE = "BLOB" or DATA_TYPE = "XMLTYPE") ) then 
                            COLUMN_NAME
													else concat(''XMLCDATA(DBMS_XMLGEN.CONVERT('', COLUMN_NAME, ''))''),
													'')''
											)
										),
									'', ''
								)
					let $pk_cols := $tab/CONSTRAINTS/CONSTRAINT[CONSTRAINT_TYPE="P" and STATUS="ENABLED"]/COLUMNS
					let $pk :=	concat(''XMLELEMENT("COLUMNS", '',
									if (count($pk_cols/COLUMN) > 0)
										then string-join($pk_cols/COLUMN/concat(''XMLELEMENT("COLUMN", XMLATTRIBUTES('''''', @NAME, '''''' AS NAME))''), '', '')
										else string-join($all_cols[DATA_TYPE != "BLOB" and DATA_TYPE != "CLOB" and DATA_TYPE != "XMLTYPE"]/concat(''XMLELEMENT("COLUMN", XMLATTRIBUTES('''''', COLUMN_NAME, '''''' AS NAME))''), '', ''),
									'')''
								)
					let $row_cols := string-join(
									$all_cols/
										string(
											concat(''XMLELEMENT("COLUMN", XMLATTRIBUTES('''''', COLUMN_NAME, '''''' AS NAME, '''''', DATA_TYPE, '''''' AS TYPE)'', '')'')
										),
									'', ''
								)
					let $order_cols :=	if (count($pk_cols/COLUMN) > 0)
											then string-join($pk_cols/COLUMN/@NAME, '','')
											else string-join($all_cols[DATA_TYPE != "BLOB" and DATA_TYPE != "CLOB" and DATA_TYPE != "XMLTYPE"]/COLUMN_NAME, '','')
					let $where := if(count(/TABLE/COLUMNS/COLUMN[COLUMN_NAME="PLUGIN"]) = 0) 
                      then string(concat(" where exists (select 1 from isr$objects$plugins where object_name=''", $tab_name, "'' 
                      and  object_type=''TABLE'' and plugin_name not in(select name from ISR$SNAPSHOT$OBJECTS   where type=''PLUGIN_EXCLUDE'')
                      and plugin_name  in(select name from ISR$SNAPSHOT$OBJECTS   where type=''PLUGIN_INCLUDE''))"))
                      else string(" where plugin not in(select name from ISR$SNAPSHOT$OBJECTS   where type=''PLUGIN_EXCLUDE'') 
                      and plugin  in(select name from ISR$SNAPSHOT$OBJECTS   where type=''PLUGIN_INCLUDE'') ")
					return element TABLE {
						element TABLE_NAME {$tab_name},
						<TABLE_SELECT>SELECT XMLELEMENT("ROWSET", 
						     XMLATTRIBUTES (''{$tab_name}'' AS TABLE_NAME), 
						     XMLELEMENT("ROW_IDENTIFIER", {$pk}), 
						     XMLELEMENT("ROW_COLUMNS", {$row_cols}), 
						     XMLAGG(XMLELEMENT("ROW", {$all}) ORDER BY {$order_cols})) 
						FROM {$tab_name}   {$where} 
						 </TABLE_SELECT>,
						element ORDER_COLS {$order_cols}
					}'
					PASSING	XMLTYPE(ISR$SNAPSHOT.getObject(o_ref.MODULE, 'TABLE', o_ref.NAME))
					COLUMNS	TABLE_NAME		VARCHAR2(128)	PATH	'/TABLE/TABLE_NAME',
							TABLE_SELECT	CLOB			PATH	'/TABLE/TABLE_SELECT',
							ORDER_COLS		VARCHAR2(4000)	PATH	'/TABLE/ORDER_COLS') TAB
WHERE	TAB.TABLE_NAME = o_ref.NAME