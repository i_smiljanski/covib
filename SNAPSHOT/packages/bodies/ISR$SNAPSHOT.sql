CREATE OR REPLACE PACKAGE BODY ISR$SNAPSHOT IS

	collectObjEx EXCEPTION;
	getObjEx EXCEPTION;
	getObjListEx EXCEPTION;
	typeDefNotFound	EXCEPTION;
	execObjSel		EXCEPTION;
	objNotPut		EXCEPTION;
	objNotFound		EXCEPTION;

	INFO CONSTANT VARCHAR2(1 CHAR) := 'I';
	DEBUG CONSTANT VARCHAR2(1 CHAR) := 'D';
	ERROR CONSTANT VARCHAR2(1 CHAR) := 'E';
	WARNING CONSTANT VARCHAR2(1 CHAR) := 'W';

	MESSAGE_STORE_SEQ CONSTANT VARCHAR2(30 CHAR) := 'ISR$SNAPSHOT$LOGS$SEQ';
	MESSAGE_STORE CONSTANT VARCHAR2(30 CHAR) := 'ISR$SNAPSHOT$LOGS';


   TYPE clob_name_rec is RECORD (
      c1 VARCHAR2(3000) ,
      c2 VARCHAR2(300)
    );

   rec1 clob_name_rec;

	FUNCTION getSnapshotAsBlob(include IN CLOB, exclude IN CLOB)
		RETURN BLOB AS
		LANGUAGE JAVA
		NAME 'com.utd.isr.snapshot.db.JAVA$ISR$SNAPSHOT.getSnapshotAsBlob(java.sql.Clob, java.sql.Clob) return java.sql.Blob';

	PROCEDURE putSnapshotAsBlob(snapshot_object IN BLOB, include_list IN CLOB) AS
		LANGUAGE JAVA
		NAME 'com.utd.isr.snapshot.db.JAVA$ISR$SNAPSHOT.putSnapshotAsBlob(java.sql.Blob, java.sql.Clob)';

	PROCEDURE collectAll AS

			object_reference	VARCHAR2(512 CHAR)	:= NULL;

		BEGIN

			ISR$SNAPSHOT.prepareMessageStore;
			addMessage(DEBUG, 'collectAll', 'message store initialized');

			addMessage(INFO, 'collectAll', 'begin collecting all objects');

			FOR rec IN (SELECT MODULE, TYPE, NAME FROM ISR$SNAPSHOT$COLLECT) LOOP



				object_reference := '[' || rec.MODULE || '].[' || rec.TYPE || '].[' || rec.NAME || ']';

				BEGIN
					addMessage(DEBUG, 'collectAll', 'collecting object ' || object_reference || '..');

					ISR$SNAPSHOT.collectObject(rec.MODULE, rec.TYPE, rec.NAME);
					addMessage(DEBUG, 'collectAll', 'object ' || object_reference || ' collected!');
				EXCEPTION
					WHEN OTHERS THEN
						addMessage(ERROR, 'collectAll', 'colelcting object ' || object_reference || ' failed! [' || SQLERRM || ']');
				END;

			END LOOP;

			addMessage(DEBUG, 'collectAll', 'begin collecting messages');
			ISR$SNAPSHOT.collectObject('ISR', 'TABLE_DATA', MESSAGE_STORE);
			addMessage(DEBUG, 'collectAll', 'end collecting messages');

			addMessage(INFO, 'collectAll', 'end collecting all objects');

		EXCEPTION
			WHEN OTHERS THEN
				addMessage(ERROR, 'collectAll', 'colelcting all objects failed! [' || SQLERRM || ']');
		END;

	PROCEDURE collectObject(m_name IN VARCHAR2, t_name IN VARCHAR2, o_name IN VARCHAR2) AS

			snapshot_row		ISR$SNAPSHOT$OBJECTS%ROWTYPE	:= NULL;

			collect_flag		VARCHAR2(1 CHAR)	:= NULL;
			object_select		CLOB;

			object_reference	VARCHAR2(512 CHAR)	:= NULL;

		BEGIN

			object_reference := '[' || m_name || '].[' || t_name || '].[' || o_name || ']';

			addMessage(INFO, 'collectObject', 'begin collecting object ' || object_reference || '..');

			BEGIN

				addMessage(DEBUG, 'collectObject', 'retrieving objects type definition..');

				SELECT	isot.COLLECT_FLAG, isot.OBJECT_SELECT
				INTO	collect_flag, object_select
				FROM	ISR$SNAPSHOT$OBJECTTYPES isot
				WHERE	isot.TYPE_NAME = t_name;

				addMessage(DEBUG, 'collectObject', 'objects type definition retrieved!');

			EXCEPTION
				WHEN NO_DATA_FOUND THEN RAISE typeDefNotFound;
			END;

			IF collect_flag = 'T' THEN

				BEGIN

					addMessage(DEBUG, 'collectObject', 'executing types OBJECT_SELECT..');

					EXECUTE IMMEDIATE object_select
						INTO snapshot_row
						USING IN m_name, IN t_name, IN o_name;

					addMessage(DEBUG, 'collectObject', 'types OBJECT_SELECT executed!');

				EXCEPTION
					WHEN OTHERS THEN RAISE execObjSel;
				END;

				BEGIN

					addMessage(DEBUG, 'collectObject', 'writing object to ISR$SNAPSHOT$OBJECTS..');
          IF snapshot_row.TYPE!='TABLE_DATA' OR (snapshot_row.TYPE='TABLE_DATA' AND INSTR( snapshot_row.SNAPSHOT_OBJECT,'CDATA') > 0) THEN
          ISR$SNAPSHOT.putObject(snapshot_row.MODULE, snapshot_row.TYPE, snapshot_row.NAME, snapshot_row.SNAPSHOT_OBJECT);


          END IF;





					addMessage(DEBUG, 'collectObject', 'object written to ISR$SNAPSHOT$OBJECTS!');

				EXCEPTION
					WHEN OTHERS THEN RAISE objNotPut;
				END;

				addMessage(INFO, 'collectObject', 'end collecting object ' || object_reference || '..');

			ELSE addMessage(WARNING, 'collectObject', 'object type is not defined to be collected!');
			END IF;

		EXCEPTION
			WHEN objNotPut THEN
				addMessage(ERROR, 'collectObject', 'writing object ' || object_reference || ' to ISR$SNAPSHOT$OBJECTS failed! [' || SQLERRM || ']');
				RAISE collectObjEx;
			WHEN execObjSel THEN
				addMessage(ERROR, 'collectObject', 'executing types OBJECT_SELECT for object ' || object_reference || ' failed! [' || SQLERRM || ']');
				RAISE collectObjEx;
			WHEN typeDefNotFound THEN
				addMessage(ERROR, 'collectObject', 'retrieving objects ' || object_reference || ' type definition failed! [' || SQLERRM || ']');
				RAISE collectObjEx;
			WHEN OTHERS THEN
				addMessage(ERROR, 'collectObject', 'collecting object ' || object_reference || ' failed! [' || SQLERRM || ']');
				RAISE;
		END collectObject;

	PROCEDURE setIncludeList(includeList IN CLOB) AS

		static_include	CLOB;
		default_include	CLOB;
		plugin_include	CLOB;
		default_table_data CLOB;
		plugin_list     CLOB := NULL;
		isr_objects_liste CLOB;
		meta_list CLOB;
		plugin_include_isr CLOB;
    object_list     CLOB;

		countStaticIncludes NUMBER;
		countPluginIncludes NUMBER:=0;
    countTypeIncludes NUMBER:=0;
		countNoPluginIncludes NUMBER:=0;
    countPluginISR NUMBER:=0;

    sql_stmt      VARCHAR2(2000); -- Dynamic SQL statement to execute
    result        XMLType;
    xmlquery_string VARCHAR2(3000);
    done VARCHAR2(300);



		BEGIN

			addMessage(DEBUG, 'setIncludeList', 'begin setting include list..');
			IF includeList IS NOT NULL THEN

			countNoPluginIncludes:=getCountEntry_XmlString(includeList,'$a/ITEM[@NAME!="ALL" and @TYPE!="PLUGIN" and @TYPE!="PLUGINWITHCHILD"]');
      countPluginIncludes:=getCountEntry_XmlString(includeList,'$a/ITEM[@TYPE="PLUGIN" or @TYPE="PLUGINWITHCHILD"  ]');
      countTypeIncludes:=getCountEntry_XmlString(includeList,'$a/ITEM[@NAME="ALL"  ]');

      --Liste der angegebenen Typen in  'ISR$SNAPSHOT$OBJECTS '

          IF countTypeIncludes>0 THEN
          INSERT
              INTO ISR$SNAPSHOT$OBJECTS (MODULE, TYPE, NAME)
              SELECT  'ISR','TYPE_INCLUDE',TYPE from
              XMLTABLE('for $item in /ITEMS/ITEM return $item'
              PASSING XMLTYPE(includeList)
              COLUMNS MODULE varchar2(300) PATH '@MODULE',
              TYPE varchar2(300) PATH '@TYPE',
              NAME varchar2(300) PATH '@NAME') TAB
              where  tab.name='ALL'   ;
              COMMIT;
          END IF;  		--countTypeIncludes


          addMessage(DEBUG, 'setIncludeList', 'countPluginIncludes   :'||countPluginIncludes);
          addMessage(DEBUG, 'setIncludeList', 'countNoPluginIncludes   :'||countNoPluginIncludes);
          addMessage(DEBUG, 'setIncludeList', 'countTypeIncludes   :'||countTypeIncludes);
          addMessage(DEBUG, 'setIncludeList', 'includeList   :'||includeList);

          --Snapshot muss 2 Dateien  aus META enthalten
          SELECT XMLELEMENT("ITEMS",
              XMLAGG(XMLELEMENT("ITEM", XMLATTRIBUTES(MODULE AS "MODULE", TYPE AS "TYPE", NAME AS "NAME")))).getClobVal()
              into meta_list
              from(
              select 'ISR' AS MODULE, 'META' AS TYPE,  'SNAPSHOT_INFO'  AS NAME
              from dual
              union
              select 'ISR' AS MODULE, 'META' AS TYPE,  'ITEMS_ORDER'  AS NAME
              from dual
              );

          IF countPluginIncludes>0 THEN
          --  list of defined plugins and child plugins and meta

          xmlquery_string :='''for $a in /ITEMS/ITEM , $b in ora:view("ISR$PLUGIN")
                where $b/ROW/PARENTPLUGIN =$a/@NAME    and $a/@TYPE ="PLUGINWITHCHILD"
                 return <ITEM MODULE="ISR" TYPE="PLUGIN" NAME="{$b/ROW/PLUGIN}" />   ''';
          plugin_list:=getList(includeList,xmlquery_string);
          xmlquery_string :='''for $a in /ITEMS/ITEM
                where $a/@TYPE ="PLUGIN"    or $a/@TYPE ="PLUGINWITHCHILD"
                 return <ITEM MODULE="ISR" TYPE="PLUGIN" NAME="{$a/@NAME}" />   ''';

          plugin_list:='<ITEMS>'||plugin_list||getList(includeList,xmlquery_string)||'<ITEM MODULE="ISR" TYPE="PLUGIN" NAME="META"/>'||'</ITEMS>';
          plugin_list:=unique_items(plugin_list);

          addMessage(DEBUG, 'setIncludeList', 'plugin_list   :'||plugin_list);

         --check, if there is a plugin isr
          countPluginISR:=getCountEntry_XmlString(plugin_list,'$a/ITEM[@NAME="ISR"  ]');

          addMessage(DEBUG, 'setIncludeList', 'countPluginISR   :'||countPluginISR);

          default_table_data := ISR$SNAPSHOT.getTableData;
          addMessage(DEBUG, 'setIncludeList','default_table_data   :'|| default_table_data);

          IF countPluginISR>0 THEN

          SELECT XMLELEMENT("ITEMS",
              XMLAGG(XMLELEMENT("ITEM", XMLATTRIBUTES(MODULE AS "MODULE", TYPE AS "TYPE", NAME AS "NAME")))).getClobVal()
              INTO isr_objects_liste
              FROM(
              SELECT MODULE,TYPE,NAME from ISR$SNAPSHOT$OBJECTTYPES ISOT,
                XMLTABLE ('for $item in /ITEMS/ITEM return $item'
                PASSING XMLTYPE(ISR$SNAPSHOT.getObjectList(ISOT.TYPE_NAME))
                COLUMNS MODULE VARCHAR2 (512 CHAR) PATH './@MODULE',
                TYPE VARCHAR2 (512 CHAR) PATH './@TYPE',
                NAME VARCHAR2 (512 CHAR) PATH './@NAME') ITEMS
                WHERE COLLECT_FLAG = 'T' and TYPE!='TABLE_DATA'
                and
                NAME not  in(select OBJECT_NAME from ISR$OBJECTS$PLUGINS ));

                addMessage(DEBUG, 'setIncludeList','isr_objects_liste   :'|| isr_objects_liste);

          END IF;  --countPluginISR




          INSERT
              INTO ISR$SNAPSHOT$OBJECTS (MODULE, TYPE, NAME)
              select distinct 'ISR','PLUGIN_INCLUDE',NAME from XMLTABLE('for $item in /ITEMS/ITEM return $item'
              PASSING XMLTYPE(plugin_list)COLUMNS MODULE varchar2(300) PATH '@MODULE',
              TYPE varchar2(300) PATH '@TYPE',
              NAME varchar2(300) PATH '@NAME') TAB
              where tab.type='PLUGIN';
          END IF;  --countPluginIncludes
      END IF;
      addMessage(DEBUG, 'setIncludeList','countPluginIncludes   :'|| countPluginIncludes|| '   countTypeIncludes  :' || countTypeIncludes);

			IF countPluginIncludes=0 THEN
        for rGetPlugins in (SELECT DISTINCT PLUGIN
                     FROM ISR$PLUGIN
                     UNION
                     SELECT DISTINCT PLUGIN_NAME
                     FROM ISR$OBJECTS$PLUGINS
                     WHERE PLUGIN_NAME NOT IN (SELECT DISTINCT PLUGIN FROM ISR$PLUGIN)
                   ) loop
          INSERT INTO ISR$SNAPSHOT$OBJECTS (MODULE, TYPE, NAME) values('ISR', 'PLUGIN_INCLUDE', rGetPlugins.plugin);
        end loop;
      END IF;
      --Liste aller enthaltenen  TYPES
      IF countTypeIncludes< 1 THEN
        for cGetTypeName in (SELECT TYPE_NAME  FROM ISR$SNAPSHOT$OBJECTTYPES  WHERE COLLECT_FLAG='T') loop
          INSERT
              INTO ISR$SNAPSHOT$OBJECTS (MODULE, TYPE, NAME)
              values( 'ISR','TYPE_INCLUDE',cGetTypeName.TYPE_NAME);
          addMessage(DEBUG, 'setIncludeList','sTypeName   :'|| cGetTypeName.TYPE_NAME || ' ' || sql%rowcount);
        end loop;
      END IF;

      COMMIT;



			IF (countNoPluginIncludes=0 AND countPluginIncludes=0)  THEN

				addMessage(DEBUG, 'setIncludeList', 'no include list is given - retrieving predefined include list..');

				static_include := ISR$SNAPSHOT.getObject('ISR', 'OBJECT_LIST', 'STATIC_INCLUDE');
				SELECT COUNT(ITEM)
				INTO countStaticIncludes
				FROM XMLTABLE(
					'for $item in /ITEMS/ITEM return $item'
					PASSING XMLTYPE(static_include)
					COLUMNS ITEM XMLTYPE	PATH '.'
				) LS;
        addMessage(DEBUG, 'setIncludeList', 'countStaticIncludes   :'||countStaticIncludes);
				IF static_include IS NULL OR countStaticIncludes = 0 THEN

					addMessage(DEBUG, 'setIncludeList', 'no predefined include list found - setting default include list..');

					default_include := ISR$SNAPSHOT.getObject('ISR', 'OBJECT_LIST', 'DEFAULT_INCLUDE');
					ISR$SNAPSHOT.putObject('ISR', 'OBJECT_LIST', 'INCLUDE',default_include);
					--addMessage(DEBUG, 'setIncludeList','default_include   :'|| default_include);

					addMessage(DEBUG, 'setIncludeList', 'no predefined include list found - default include list set!');

				ELSE
					ISR$SNAPSHOT.putObject('ISR', 'OBJECT_LIST', 'INCLUDE', static_include);
					addMessage(DEBUG, 'setIncludeList', 'no include list is given - predefined include list set!');
				END IF;

			ELSE



        IF countNoPluginIncludes=0 THEN


				SELECT XMLELEMENT("ITEMS",
              XMLAGG(XMLELEMENT("ITEM", XMLATTRIBUTES(MODULE AS "MODULE", TYPE AS "TYPE", NAME AS "NAME")))).getClobVal()
              INTO plugin_include
              FROM(Select object_module as MODULE,object_type as TYPE, object_name as NAME  from isr$objects$plugins plugins,
              XMLTABLE('for $plugin in /ITEMS/ITEM return $plugin'
              PASSING XMLTYPE(plugin_list)COLUMNS MODULE varchar2(300) PATH '@MODULE',
              TYPE varchar2(300) PATH '@TYPE',
              NAME varchar2(300) PATH '@NAME') TAB
              where plugins.plugin_name=tab.NAME   and plugins.object_type IN (SELECT  NAME FROM ISR$SNAPSHOT$OBJECTS snapshot WHERE snapshot.TYPE='TYPE_INCLUDE'
              )
               union
              select MODULE AS "MODULE", TYPE AS "TYPE", NAME AS "NAME"
              FROM
              XMLTABLE ('for $item in /ITEMS/ITEM return $item'
              PASSING XMLTYPE(meta_list)
              COLUMNS MODULE varchar2(300) PATH '@MODULE',
              TYPE varchar2(300) PATH '@TYPE',
              NAME varchar2(300) PATH '@NAME')
              union
              select MODULE AS "MODULE", TYPE AS "TYPE", NAME AS "NAME"
              FROM
              XMLTABLE ('for $item in /ITEMS/ITEM return $item'
              PASSING XMLTYPE(default_table_data)
              COLUMNS MODULE varchar2(300) PATH '@MODULE',
              TYPE varchar2(300) PATH '@TYPE',
              NAME varchar2(300) PATH '@NAME')
              WHERE TYPE IN (SELECT  NAME FROM ISR$SNAPSHOT$OBJECTS snapshot WHERE snapshot.TYPE='TYPE_INCLUDE')
               );

              addMessage(DEBUG, 'setIncludeList','plugin_include   '|| plugin_include);

        IF countPluginISR>0 THEN
        SELECT XMLELEMENT("ITEMS",
              XMLAGG(XMLELEMENT("ITEM", XMLATTRIBUTES(MODULE AS "MODULE", TYPE AS "TYPE", NAME AS "NAME")))).getClobVal()
              INTO plugin_include_isr
              FROM(
              select MODULE AS "MODULE", TYPE AS "TYPE", NAME AS "NAME"
              FROM
              XMLTABLE ('for $item in /ITEMS/ITEM return $item'
              PASSING XMLTYPE(plugin_include)
              COLUMNS MODULE varchar2(300) PATH '@MODULE',
              TYPE varchar2(300) PATH '@TYPE',
              NAME varchar2(300) PATH '@NAME')
              union
              select MODULE AS "MODULE", TYPE AS "TYPE", NAME AS "NAME"
              FROM
              XMLTABLE ('for $item in /ITEMS/ITEM  return $item'
              PASSING XMLTYPE(isr_objects_liste)
              COLUMNS MODULE varchar2(300) PATH '@MODULE',
              TYPE varchar2(300) PATH '@TYPE',
              NAME varchar2(300) PATH '@NAME')
              WHERE TYPE IN (SELECT  NAME FROM ISR$SNAPSHOT$OBJECTS snapshot WHERE snapshot.TYPE='TYPE_INCLUDE'));

        ISR$SNAPSHOT.putObject('ISR', 'OBJECT_LIST', 'INCLUDE', plugin_include_isr);

				addMessage(DEBUG, 'setIncludeList','plugin_include_isr   '|| plugin_include_isr);

				ELSE

				ISR$SNAPSHOT.putObject('ISR', 'OBJECT_LIST', 'INCLUDE', plugin_include);

				END IF;



				ELSE

				IF countPluginISR>0 THEN
				       xmlquery_string :='''for $a in /ITEMS/ITEM , $b in ora:view("ISR$SNAPSHOT$OBJECTS")/ROW[TYPE="TYPE_INCLUDE"]
                where  $a/@NAME !="ALL"  and fn:exists($a[@TYPE =$b/NAME])
                 return <ITEM NAME="{$a/@NAME}" MODULE="{$a/@MODULE}" TYPE="{$a/@TYPE}" ACTION="{$a/@ACTION}"  />   ''';
        ELSE
				--Die resultierende Liste enthält Objekte, die zum entsprechenden Plugin gehören und den entsprechenden Type haben.
				xmlquery_string :='''for $a in /ITEMS/ITEM , $b in ora:view("ISR$SNAPSHOT$OBJECTS")/ROW[TYPE="TYPE_INCLUDE"],
                $c in ora:view("ISR$SNAPSHOT$OBJECTS")/ROW[TYPE="PLUGIN_INCLUDE"],
                $d in ora:view("ISR$OBJECTS$PLUGINS")/ROW
                where  $a/@NAME !="ALL"  and fn:exists($a[@TYPE =$b/NAME]) and fn:exists($a[@NAME =$d/OBJECT_NAME])  and $c/NAME =$d/PLUGIN_NAME
                 return <ITEM NAME="{$a/@NAME}" MODULE="{$a/@MODULE}" TYPE="{$a/@TYPE}" ACTION="{$a/@ACTION}"  />   ''';
        END IF;

        plugin_include:='<ITEMS>'||getList(includeList,xmlquery_string)||'<ITEM MODULE="ISR" TYPE="META" NAME="SNAPSHOT_INFO" ACTION="INCLUDE"/>
        <ITEM MODULE="ISR" TYPE="META" NAME="ITEMS_ORDER" ACTION="INCLUDE"/>'||'</ITEMS>';


              addMessage(DEBUG, 'setIncludeList', 'includeList_meta   :'||plugin_include);
              ISR$SNAPSHOT.putObject('ISR', 'OBJECT_LIST', 'INCLUDE', plugin_include);
        END IF;    --countNoPluginIncludes=0

				addMessage(DEBUG, 'setIncludeList', 'include list set!');
			END IF;
    EXCEPTION
					WHEN OTHERS THEN
						addMessage(ERROR, 'setIncludeList',  SQLERRM );
		END setIncludeList;


	PROCEDURE setExcludeList(excludeList IN CLOB)AS

		static_exclude	CLOB;
		default_exclude	CLOB;
		plugin_exclude	CLOB;
		plugin_list     CLOB;
		object_list     CLOB;

		countStaticExcludes NUMBER;
		countPluginExcludes NUMBER:=0;
		countNoPluginExcludes NUMBER:=0;



		BEGIN

			addMessage(DEBUG, 'setExcludeList', 'begin setting exclude list..');



			IF excludeList IS NOT NULL THEN

       countNoPluginExcludes:=getCountEntry_XmlString(excludeList,'$a/ITEM[@TYPE!="PLUGIN" and @TYPE!="PLUGINWITHCHILD"]');
         /* SELECT  count(TYPE)
          INTO countNoPluginExcludes
          FROM 	XMLTABLE('for $item in /ITEMS/ITEM return $item'
              PASSING XMLTYPE(excludeList)COLUMNS TYPE varchar2(300) PATH '@TYPE') TAB
              WHERE TYPE!='PLUGIN' and TYPE!='PLUGINWITHCHILD';*/

       countPluginExcludes:=getCountEntry_XmlString(excludeList,'$a/ITEM[@TYPE="PLUGIN" or @TYPE="PLUGINWITHCHILD"]');

          /*SELECT  count(TYPE)
          INTO countPluginExcludes
          FROM 	XMLTABLE('for $item in /ITEMS/ITEM return $item'
              PASSING XMLTYPE(excludeList)COLUMNS TYPE varchar2(300) PATH '@TYPE') TAB
              WHERE TYPE='PLUGIN' or TYPE='PLUGINWITHCHILD';*/

          addMessage(DEBUG, 'setExcludeList', 'countPluginExcludes   :'||countPluginExcludes);
          addMessage(DEBUG, 'setExcludeList', 'countNoPluginExcludes   :'||countNoPluginExcludes);

          IF countPluginExcludes>0 THEN

          --  list of defined plugins and child plugins
          SELECT XMLELEMENT("ITEMS",
              XMLAGG(XMLELEMENT("ITEM", XMLATTRIBUTES(MODULE AS "MODULE", TYPE AS "TYPE", NAME AS "NAME")))).getClobVal()
              INTO plugin_list
              FROM(select 'ISR' AS MODULE, 'PLUGIN' AS TYPE,  PLUGIN AS NAME  from ISR$PLUGIN  isrplugin,
              XMLTABLE('for $item in /ITEMS/ITEM return $item'
              PASSING XMLTYPE(excludeList)COLUMNS MODULE varchar2(300) PATH '@MODULE',
              TYPE varchar2(300) PATH '@TYPE',
              NAME varchar2(300) PATH '@NAME') TAB
              where isrplugin.parentplugin=tab.NAME and tab.type='PLUGINWITHCHILD'
              union
              select 'ISR' AS MODULE, 'PLUGIN' AS TYPE,  NAME AS NAME from
              XMLTABLE('for $item in /ITEMS/ITEM return $item'
              PASSING XMLTYPE(excludeList)
              COLUMNS MODULE varchar2(300) PATH '@MODULE',
              TYPE varchar2(300) PATH '@TYPE',
              NAME varchar2(300) PATH '@NAME') TAB
              where  tab.type='PLUGIN' OR tab.type='PLUGINWITHCHILD');

          addMessage(DEBUG, 'setExcludeList', 'plugin_list   :'||plugin_list);
          INSERT
              INTO ISR$SNAPSHOT$OBJECTS (MODULE, TYPE, NAME)
              select distinct 'ISR','PLUGIN_EXCLUDE',NAME from XMLTABLE('for $item in /ITEMS/ITEM return $item'
              PASSING XMLTYPE(plugin_list)COLUMNS MODULE varchar2(300) PATH '@MODULE',
              TYPE varchar2(300) PATH '@TYPE',
              NAME varchar2(300) PATH '@NAME') TAB
              where tab.type='PLUGIN';
              COMMIT;

          END IF;  --countPluginExcludes>0
      END IF;   --excludeList IS NOT NULL

       IF (countNoPluginExcludes=0) THEN
			--IF excludeList IS NULL THEN
				addMessage(DEBUG, 'setExcludeList', 'no exclude list is given - retrieving predefined exclude list..');

				static_exclude := ISR$SNAPSHOT.getObject('ISR', 'OBJECT_LIST', 'STATIC_EXCLUDE');
				addMessage(DEBUG, 'setExcludeList', static_exclude);
				SELECT COUNT(ITEM)
				INTO countStaticExcludes
				FROM XMLTABLE(
					'for $item in /ITEMS/ITEM return $item'
					PASSING XMLTYPE(static_exclude)
					COLUMNS ITEM XMLTYPE	PATH '.'
				) LS;

				IF static_exclude IS NULL OR countStaticExcludes = 0 THEN

					addMessage(DEBUG, 'setExcludeList', 'no predefined exclude list found - setting default exclude list..');

					default_exclude := ISR$SNAPSHOT.getObject('ISR', 'OBJECT_LIST', 'DEFAULT_EXCLUDE');
					--ISR$SNAPSHOT.putObject('ISR', 'OBJECT_LIST', 'EXCLUDE', default_exclude);
					object_list:=default_exclude;
					addMessage(DEBUG, 'setExcludeList', 'no predefined exclude list found - default exclude list set!');
					addMessage(DEBUG, 'setExcludeList','default_exclude   :'|| default_exclude);
					addMessage(DEBUG, 'setExcludeList','object_list   :'|| object_list);

				ELSE
					--ISR$SNAPSHOT.putObject('ISR', 'OBJECT_LIST', 'EXCLUDE', static_exclude);
					object_list:=static_exclude;
					addMessage(DEBUG, 'setExcludeList', 'no exclude list is given - predefined static exclude list set!');
					addMessage(DEBUG, 'setExcludeList','static_exclude   :'|| static_exclude);
					addMessage(DEBUG, 'setExcludeList','object_list   :'|| object_list);
				END IF;  --static_exclude IS NULL

			ELSE


				--ISR$SNAPSHOT.putObject('ISR', 'OBJECT_LIST', 'EXCLUDE', excludeList);
				object_list:=excludeList;


				addMessage(DEBUG, 'setExcludeList', 'exclude list set!');
        addMessage(DEBUG, 'setExcludeList','excludeList   :'|| excludeList);
				addMessage(DEBUG, 'setExcludeList','object_list   :'|| object_list);
			END IF;	 --countNoPluginExcludes =0

			IF (countPluginExcludes =0)  THEN

			SELECT XMLELEMENT("ITEMS",
              XMLAGG(XMLELEMENT("ITEM", XMLATTRIBUTES(MODULE AS "MODULE", TYPE AS "TYPE", NAME AS "NAME")))).getClobVal()
              INTO plugin_exclude
              FROM
              XMLTABLE ('for $item in /ITEMS/ITEM return $item'
              PASSING XMLTYPE(object_list)
              COLUMNS MODULE varchar2(300) PATH '@MODULE',
              TYPE varchar2(300) PATH '@TYPE',
              NAME varchar2(300) PATH '@NAME');
				addMessage(DEBUG, 'setExcludeList', 'object_list set!');

			ELSE

				SELECT XMLELEMENT("ITEMS",
              XMLAGG(XMLELEMENT("ITEM", XMLATTRIBUTES(MODULE AS "MODULE", TYPE AS "TYPE", NAME AS "NAME")))).getClobVal()
              INTO plugin_exclude
              FROM(Select object_module as MODULE,object_type as TYPE, object_name as NAME  from isr$objects$plugins,XMLTABLE('for $item in /ITEMS/ITEM return $item'
              PASSING XMLTYPE(plugin_list)COLUMNS MODULE varchar2(300) PATH '@MODULE',
              TYPE varchar2(300) PATH '@TYPE',
              NAME varchar2(300) PATH '@NAME') TAB
              where plugin_name=tab.NAME and tab.type='PLUGIN'
              union
              select MODULE AS "MODULE", TYPE AS "TYPE", NAME AS "NAME"
              FROM
              XMLTABLE ('for $item in /ITEMS/ITEM return $item'
              PASSING XMLTYPE(object_list)
              COLUMNS MODULE varchar2(300) PATH '@MODULE',
              TYPE varchar2(300) PATH '@TYPE',
              NAME varchar2(300) PATH '@NAME')
              );
        addMessage(DEBUG, 'setExcludeList', 'object_list  plugin_list  set!');


      END IF;
      ISR$SNAPSHOT.putObject('ISR', 'OBJECT_LIST', 'EXCLUDE', plugin_exclude);

      addMessage(DEBUG, 'setPluginExcludeList', plugin_exclude);





		END setExcludeList;


	FUNCTION getObject(m_name IN VARCHAR2, t_name IN VARCHAR2, o_name IN VARCHAR2)
		RETURN CLOB AS

			snapshot_object		CLOB							:= NULL;
			snapshot_row		ISR$SNAPSHOT$OBJECTS%ROWTYPE	:= NULL;

			collect_flag	VARCHAR2(1 CHAR)	:= NULL;
			object_select	CLOB;

			object_reference	VARCHAR2(512 CHAR)	:= NULL;

		BEGIN

			object_reference := '[' || m_name || '].[' || t_name || '].[' || o_name || ']';

			addMessage(INFO, 'getObject', 'begin retireving object ' || object_reference || '..');

			BEGIN

				addMessage(DEBUG, 'getObject', 'retrieving objects type definition..');

				SELECT	isot.COLLECT_FLAG, isot.OBJECT_SELECT
				INTO	collect_flag, object_select
				FROM	ISR$SNAPSHOT$OBJECTTYPES isot
				WHERE	isot.TYPE_NAME = t_name;

				addMessage(DEBUG, 'getObject', 'objects type definition retrieved!');

			EXCEPTION
				WHEN NO_DATA_FOUND THEN RAISE typeDefNotFound;
			END;

			IF collect_flag = 'T' THEN

				addMessage(DEBUG, 'getObject', 'object needs to be collected first..');

				ISR$SNAPSHOT.collectObject(m_name, t_name, o_name);

				BEGIN

					addMessage(DEBUG, 'getObject', 'selecting object from ISR$SNAPSHOT$OBJECTS..');

					SELECT	SNAPSHOT_OBJECT
					INTO	snapshot_object
					FROM	ISR$SNAPSHOT$OBJECTS
					WHERE	ISR$SNAPSHOT$OBJECTS.MODULE = m_name
						AND	ISR$SNAPSHOT$OBJECTS.TYPE = t_name
						AND	ISR$SNAPSHOT$OBJECTS.NAME = o_name;

					addMessage(DEBUG, 'getObject', 'object selected from ISR$SNAPSHOT$OBJECTS!');

				EXCEPTION
					WHEN NO_DATA_FOUND THEN RAISE objNotFound;
				END;

			ELSE

				BEGIN

					addMessage(DEBUG, 'getObject', 'executing types OBJECT_SELECT..'||t_name||'   '||o_name);

					EXECUTE IMMEDIATE object_select
						INTO snapshot_row
						USING IN m_name, IN t_name, IN o_name;

					addMessage(DEBUG, 'getObject', 'types OBJECT_SELECT executed!');

				EXCEPTION
					WHEN OTHERS THEN RAISE execObjSel;
				END;

				snapshot_object := snapshot_row.SNAPSHOT_OBJECT;

			END IF;
			if snapshot_object is null then
        snapshot_object := '<EMPTY/>';
      end if;
      --addMessage(DEBUG, 'getObject', 'snapshot_object:   '||snapshot_object);
			RETURN snapshot_object;


		EXCEPTION
			WHEN objNotFound THEN
				addMessage(ERROR, 'getObject', 'selecting object from ISR$SNAPSHOT$OBJECTS failed! [' || SQLERRM || ']');
				RAISE getObjEx;
			WHEN execObjSel THEN
				addMessage(ERROR, 'getObject', 'executing types OBJECT_SELECT failed! [' || SQLERRM || ']');
				RAISE getObjEx;
			WHEN typeDefNotFound THEN
				addMessage(ERROR, 'getObject', 'retrieving objects type definition failed! [' || SQLERRM || ']');
				RAISE getObjEx;
			WHEN OTHERS THEN
				addMessage(ERROR, 'getObject', 'retrieving object failed! [' || SQLERRM || ']');
				RAISE;
		END getObject;

	FUNCTION getObjectList(t_name IN VARCHAR2)
		RETURN CLOB AS

			list_select		CLOB;
			snapshot_row	ISR$SNAPSHOT$OBJECTS%ROWTYPE	:= NULL;

		BEGIN

			addMessage(INFO, 'getObjectList', 'begin creating object list for type [' || t_name || ']..');

			addMessage(DEBUG, 'getObjectList', 'retrieving objects type definition..');

			SELECT	isot.LIST_SELECT
			INTO	list_select
			FROM	ISR$SNAPSHOT$OBJECTTYPES isot
			WHERE	isot.TYPE_NAME = t_name;

			addMessage(DEBUG, 'getObjectList', 'objects type definition retrieved!');

			addMessage(DEBUG, 'getObjectList', 'executing types LIST_SELECT..');

			EXECUTE IMMEDIATE list_select
				INTO snapshot_row
				USING t_name;

			addMessage(DEBUG, 'getObjectList', 'types LIST_SELECT executed!');
			addMessage(INFO, 'getObjectList', 'end creating object list for type [' || t_name || ']!');

			RETURN snapshot_row.SNAPSHOT_OBJECT;

		EXCEPTION
			WHEN NO_DATA_FOUND THEN
				addMessage(ERROR, 'getObjectList', 'retrieving objects type definition failed! [' || SQLERRM || ']');
				RAISE getObjListEx;
			WHEN OTHERS THEN
				addMessage(ERROR, 'getObject', 'executing types OBJECT_SELECT failed! [' || SQLERRM || ']');
				RAISE getObjListEx;
		END getObjectList;

FUNCTION getTableData
		RETURN CLOB AS

			list_select		CLOB;
			snapshot_row	ISR$SNAPSHOT$OBJECTS%ROWTYPE	:= NULL;
			t_name VARCHAR2(30) :='TABLE_DATA';


		BEGIN


			addMessage(DEBUG, 'getObjectList', 'retrieving objects type definition..');

			SELECT	isot.LIST_SELECT
			INTO	list_select
			FROM	ISR$SNAPSHOT$OBJECTTYPES isot
			WHERE	isot.TYPE_NAME = t_name;

			addMessage(DEBUG, 'getObjectListTableData', 'objects type definition retrieved!');

			addMessage(DEBUG, 'getObjectListTableData', 'executing types LIST_SELECT..');

			EXECUTE IMMEDIATE list_select
				INTO snapshot_row
				USING t_name;

			addMessage(DEBUG, 'getObjectListTableData', 'types LIST_SELECT executed!');

			RETURN snapshot_row.SNAPSHOT_OBJECT;

		EXCEPTION
			WHEN NO_DATA_FOUND THEN
				addMessage(ERROR, 'getObjectListTableData', 'retrieving objects type definition failed! [' || SQLERRM || ']');
				RAISE getObjListEx;
			WHEN OTHERS THEN
				addMessage(ERROR, 'getObjectListTableData', 'executing types OBJECT_SELECT failed! [' || SQLERRM || ']');
				RAISE getObjListEx;
		END getTableData;

		FUNCTION getList(list  IN CLOB,xmlquery_string IN varchar2)
		RETURN CLOB AS

			result        XMLType;
			sql_stmt      VARCHAR2(2000);


		BEGIN

      addMessage(DEBUG, 'getList', 'getList!');

      sql_stmt :=
        'SELECT XMLQuery('||xmlquery_string||
                  'PASSING XMLTYPE('''||list||''')  '||
                  'RETURNING CONTENT) FROM DUAL';

      addMessage(DEBUG, 'getList', 'sql_stmt:   '||sql_stmt);

      EXECUTE IMMEDIATE sql_stmt INTO result ;

IF result is  null THEN
return null;
else
addMessage(DEBUG, 'getList', 'result:   '|| result.getclobval());

end if;
      --addMessage(DEBUG, 'getList', 'result:   '||result);
			RETURN result.getCLobVal();

		EXCEPTION
			WHEN OTHERS THEN
				addMessage(ERROR, 'getList', 'failed! [' || SQLERRM || ']');
				RAISE getObjListEx;
		END getList;

		FUNCTION unique_items(list  IN CLOB)
		RETURN CLOB AS

			res boolean;
			result        XMLType;

		BEGIN

      addMessage(DEBUG, 'unique_items', 'unique_items!');

      res := DBMS_XDB.createResource('/public/imps.xml',   list);
      SELECT XMLQuery('let $items :=
                   doc("/public/imps.xml")//ITEM/@NAME
                   let $unique-items := distinct-values($items)
                  return
                  <ITEMS>
                  {
                     for $item in $unique-items
                     return  <ITEM MODULE="ISR" TYPE="PLUGIN" NAME="{$item}" />
                  }
                  </ITEMS>'
              RETURNING CONTENT)
              into result FROM DUAL;
              dbms_xdb.DeleteResource('/public/imps.xml');
              commit;

              RETURN result.getCLobVal();

		EXCEPTION
			WHEN OTHERS THEN
				addMessage(ERROR, 'unique_items', 'failed! [' || SQLERRM || ']');
				RAISE getObjListEx;
		END unique_items;
		FUNCTION getCountEntry_XmlString(list  IN CLOB,where_list IN varchar2)
		RETURN NUMBER AS

    sql_stmt      VARCHAR2(2000); -- Dynamic SQL statement to execute
    result        XMLType;
    ncount        NUMBER;
    str           VARCHAR2(300);
    BEGIN
      --where_string:='$a/ITEM[@NAME!="ALL" and @TYPE!="PLUGIN" and @TYPE!="PLUGINWITHCHILD"]';
      sql_stmt :=
      'SELECT XMLQuery(
              ''for $a in /ITEMS  '  ||
              'return <ncount>{count('||where_list||')}</ncount>  '' ' ||
              'PASSING XMLTYPE('''||list||''')  '||
              'RETURNING CONTENT) FROM DUAL';
              addMessage(DEBUG, 'getCountEntry_XmlString', 'list   :'||list);
              addMessage(DEBUG, 'getCountEntry_XmlString', 'where_list   :'||where_list);
      addMessage(DEBUG, 'getCountEntry_XmlString', 'sql_stmt   :'||sql_stmt);
      EXECUTE IMMEDIATE sql_stmt INTO result ;
      str:=result.getCLobVal();
      ncount :=to_number( SUBSTR(str,INSTR(str, '>', 1)+1,INSTR(str, '<', 2)-INSTR(str, '>', 1)-1)) ;

			RETURN ncount;

		EXCEPTION
			WHEN OTHERS THEN
				addMessage(ERROR, 'countEntry_XmlString', 'failed! [' || SQLERRM || ']');
				RAISE getObjListEx;
		END getCountEntry_XmlString;

	PROCEDURE putObject(m_name IN VARCHAR2, t_name IN VARCHAR2, o_name IN VARCHAR2, snapshot_object IN CLOB) AS

			PRAGMA AUTONOMOUS_TRANSACTION;

			object_reference	VARCHAR2(512) := NULL;


		BEGIN


			object_reference := '[' || m_name || '].[' || t_name || '].[' || o_name || ']';

			addMessage(INFO, 'putObject', 'begin writing object ' || object_reference || ' to ISR$SNAPSHOT$OBJECTS..');


			addMessage(DEBUG, 'putObject', 'deleting existing object ' || object_reference || '..');

			DELETE
			FROM	ISR$SNAPSHOT$OBJECTS
			WHERE	MODULE = m_name
				AND	TYPE = t_name
				AND	NAME = o_name;

			addMessage(DEBUG, 'putObject', 'existing object ' || object_reference || ' deleted!');

			IF snapshot_object IS NOT NULL THEN

				addMessage(DEBUG, 'putObject', 'inserting object ' || object_reference || '..');

				INSERT
					INTO ISR$SNAPSHOT$OBJECTS (MODULE, TYPE, NAME, SNAPSHOT_OBJECT)
				VALUES
					/* TODO: this line creates invalid contents -> investigate*/
					/*(m_name, t_name, o_name, XMLTYPE(XMLSERIALIZE(CONTENT snapshot_object INDENT SIZE = 3)));*/
					(m_name, t_name, o_name, snapshot_object);

					/* TODO: this line creates invalid contents -> investigate*/
					/*(m_name, t_name, o_name, XMLTYPE(XMLSERIALIZE(CONTENT snapshot_object INDENT SIZE = 3)));*/

				addMessage(DEBUG, 'putObject', 'object ' || object_reference || ' inserted!');

			ELSE
				addMessage(WARNING, 'putObject', 'given object is NULL - object deleted from ISR$SNAPSHOT$OBJECTS!');
			END IF;

			COMMIT;

			addMessage(INFO, 'putObject', 'end writing object ' || object_reference || ' to ISR$SNAPSHOT$OBJECTS!');

		EXCEPTION
			WHEN OTHERS THEN
				addMessage(ERROR, 'putObject', 'writing object ' || object_reference || ' to ISR$SNAPSHOT$OBJECTS failed! [' || SQLERRM || ']');
				ROLLBACK;
				RAISE;
		END putObject;

	FUNCTION getDATA_DEFAULT(tab_name IN VARCHAR2, col_name VARCHAR2)
		RETURN VARCHAR2 AS

		lng		LONG;

		BEGIN

			SELECT	DATA_DEFAULT
			INTO	lng
			FROM	USER_TAB_COLUMNS
			WHERE	TABLE_NAME = tab_name
				AND	COLUMN_NAME = col_name;

			RETURN TO_CHAR(lng);

		END getDATA_DEFAULT;

	FUNCTION getSEARCH_CONDITION(const_name IN VARCHAR2)
		RETURN VARCHAR2 AS

			output VARCHAR2(4000);

		BEGIN

			SELECT	SEARCH_CONDITION
			INTO	output
			FROM	USER_CONSTRAINTS
			WHERE	CONSTRAINT_NAME = CONST_NAME;

			RETURN output;

		END getSEARCH_CONDITION;

	FUNCTION getVIEW_TEXT(v_name IN VARCHAR2)
		RETURN CLOB AS

		lng		LONG;

		BEGIN

			SELECT	UV.TEXT
			INTO	lng
			FROM	USER_VIEWS UV
			WHERE	UV.VIEW_NAME = v_name;

			RETURN TO_CLOB(lng);

		END getVIEW_TEXT;

	FUNCTION getITEMS_ORDER
		RETURN CLOB AS

		items_order	CLOB;

		BEGIN

			WITH
				OBJECT_REF (MODULE, TYPE, NAME) AS (
					SELECT 'ISR', 'META', 'ITEMS_ORDER' FROM DUAL),
				TABLES_WITH_PK (TABLE_NAME, PK_NAME) AS (
					SELECT TABLE_NAME, CONSTRAINT_NAME
					FROM USER_CONSTRAINTS
					WHERE CONSTRAINT_TYPE = 'P'),
				TABLES_WITH_FK (TABLE_NAME, FK_NAME, PK_NAME) AS (
					SELECT UC.TABLE_NAME, UC.CONSTRAINT_NAME, UC.R_CONSTRAINT_NAME
					FROM USER_CONSTRAINTS UC
					WHERE UC.CONSTRAINT_TYPE = 'R'),
				TABLE_DEPS (NAME, REFERENCED_NAME) AS (
					SELECT TWFK.TABLE_NAME, TWPK.TABLE_NAME
					FROM TABLES_WITH_FK TWFK, TABLES_WITH_PK TWPK
					WHERE TWFK.PK_NAME = TWPK.PK_NAME),
				USER_DEPS (TYPE, NAME, REFERENCED_TYPE, REFERENCED_NAME) AS (
					SELECT	ud.TYPE,
							ud.NAME,
							ud.REFERENCED_TYPE,
							ud.REFERENCED_NAME
					FROM	USER_DEPENDENCIES UD
					WHERE	UD.REFERENCED_OWNER = USER AND UD.TYPE NOT LIKE 'JAVA%'),
				OBJ_DEPS (TYPE, NAME, REFERENCED_TYPE, REFERENCED_NAME) AS (
					SELECT TYPE, NAME, REFERENCED_TYPE, REFERENCED_NAME FROM USER_DEPS
					UNION
					SELECT 'TABLE', NAME, 'TABLE', REFERENCED_NAME FROM TABLE_DEPS
					UNION
					SELECT 'TABLE_DATA', NAME, 'TABLE_DATA', REFERENCED_NAME FROM TABLE_DEPS),
				OBJ_WITHOUT_DEPS (TYPE, NAME) AS (
					(SELECT UO.OBJECT_TYPE, UO.OBJECT_NAME FROM USER_OBJECTS UO WHERE UO.OBJECT_TYPE NOT LIKE 'JAVA%' AND UO.OBJECT_TYPE NOT IN ('LOB', 'JOB', 'INDEX')
					UNION
					SELECT 'TABLE_DATA', TABLE_NAME FROM USER_TABLES)
					MINUS
					SELECT TYPE, NAME
					FROM OBJ_DEPS),
				ALL_DEPS (TYPE, NAME, REFERENCED_TYPE, REFERENCED_NAME) AS (
					SELECT TYPE, NAME, REFERENCED_TYPE, REFERENCED_NAME FROM OBJ_DEPS
					UNION
					SELECT TYPE, NAME, NULL, NULL FROM OBJ_WITHOUT_DEPS),
				ALL_ORDER (ORDER_NUM, TYPE, NAME) AS (
					SELECT MAX(LEVEL) AS LVL, TYPE, NAME
					FROM ALL_DEPS
						START WITH	(REFERENCED_TYPE IS NULL AND REFERENCED_NAME IS NULL)
									OR
									(	(TYPE = REFERENCED_TYPE AND NAME = REFERENCED_NAME)
										AND
										(TYPE, NAME) NOT IN (SELECT TYPE, NAME FROM TABLE_DEPS WHERE TYPE != REFERENCED_TYPE AND NAME != REFERENCED_NAME))
						CONNECT BY NOCYCLE REFERENCED_TYPE = PRIOR TYPE AND	REFERENCED_NAME = PRIOR NAME
					GROUP BY TYPE, NAME)
			SELECT	XMLELEMENT(
						  "ITEMS"
						, XMLAGG(
							XMLELEMENT(
								  "ITEM"
								, XMLATTRIBUTES( 'ISR' AS "MODULE", O.TYPE AS "TYPE", O.NAME AS "NAME", O.ORDER_NUM AS "ORDER")
							)
						ORDER BY O.ORDER_NUM, O.TYPE)
					).getClobVal()
			INTO	items_order
			FROM	ALL_ORDER O
			JOIN ISR$SNAPSHOT$COLLECT C ON C.TYPE = O.TYPE AND C.NAME = O.NAME;
			/*WITH
				OBJECT_REF (MODULE, TYPE, NAME) AS (
					SELECT 'ISR', 'META', 'ITEMS_ORDER' FROM DUAL),
				TABLES_WITH_PK (TABLE_NAME, PK_NAME) AS (
					SELECT TABLE_NAME, CONSTRAINT_NAME
					FROM USER_CONSTRAINTS
					WHERE CONSTRAINT_TYPE = 'P'),
				TABLES_WITH_FK (TABLE_NAME, FK_NAME, PK_NAME) AS (
					SELECT UC.TABLE_NAME, UC.CONSTRAINT_NAME, UC.R_CONSTRAINT_NAME
					FROM USER_CONSTRAINTS UC
					WHERE UC.CONSTRAINT_TYPE = 'R'),
				TABLES_WITHOUT_FK (TABLE_NAME) AS (
					SELECT TABLE_NAME FROM USER_TABLES
					MINUS
					SELECT TABLE_NAME
					FROM USER_CONSTRAINTS
					WHERE CONSTRAINT_TYPE = 'R'
					GROUP BY TABLE_NAME),
				TABLE_DEPS (TYPE, NAME, REFERENCED_TYPE, REFERENCED_NAME) AS (
					SELECT 'TABLE', TWFK.TABLE_NAME, 'TABLE', TWPK.TABLE_NAME
					FROM TABLES_WITH_FK TWFK, TABLES_WITH_PK TWPK
					WHERE TWFK.PK_NAME = TWPK.PK_NAME
					UNION
					SELECT 'TABLE', TABLE_NAME, NULL, NULL FROM TABLES_WITHOUT_FK),
				TAB_DATA_ORDER (ORDER_NUM, TABLE_NAME) AS (
					SELECT MAX(LEVEL), NAME
					FROM TABLE_DEPS
						START WITH REFERENCED_NAME IS NULL
						CONNECT BY NOCYCLE REFERENCED_NAME = PRIOR NAME
					GROUP BY NAME),
				USER_DEPS (TYPE, NAME, REFERENCED_TYPE, REFERENCED_NAME) AS (
					SELECT	ud.TYPE,
							ud.NAME,
							ud.REFERENCED_TYPE,
							ud.REFERENCED_NAME
					FROM	USER_DEPENDENCIES UD
					WHERE	UD.REFERENCED_OWNER = USER AND UD.TYPE NOT LIKE 'JAVA%'),
				OBJ_WITHOUT_DEPS (TYPE, NAME) AS (
					SELECT UO.OBJECT_TYPE, UO.OBJECT_NAME
					FROM USER_OBJECTS UO, USER_DEPS UD
					WHERE UO.OBJECT_TYPE = UD.TYPE AND UO.OBJECT_NAME = UD.NAME OR UO.OBJECT_TYPE = UD.REFERENCED_TYPE AND UO.OBJECT_NAME = UD.REFERENCED_NAME
					MINUS
					SELECT TYPE, NAME
					FROM USER_DEPS),
				OBJ_DEPS (TYPE, NAME, REFERENCED_TYPE, REFERENCED_NAME) AS (
					SELECT TYPE, NAME, REFERENCED_TYPE, REFERENCED_NAME FROM USER_DEPS
					UNION
					SELECT TYPE, NAME, NULL, NULL FROM OBJ_WITHOUT_DEPS WHERE TYPE != 'TABLE'
					UNION
					SELECT TYPE, NAME, REFERENCED_TYPE, REFERENCED_NAME FROM TABLE_DEPS),
				OBJS_ORDER (ORDER_NUM, TYPE, NAME) AS (
					SELECT MAX(LEVEL) AS LVL, TYPE, NAME
					FROM OBJ_DEPS
						START WITH	REFERENCED_TYPE IS NULL
								AND	REFERENCED_NAME IS NULL
						CONNECT BY NOCYCLE REFERENCED_TYPE = PRIOR TYPE
								AND	REFERENCED_NAME = PRIOR NAME
					GROUP BY TYPE, NAME),
				ORDER_OF_ALL_OBJECTS (ORDER_NUM, TYPE, NAME) AS (
					SELECT ORDER_NUM, TYPE, NAME FROM OBJS_ORDER
					UNION
					SELECT ORDER_NUM, 'TABLE_DATA', TABLE_NAME FROM TAB_DATA_ORDER)
			SELECT	XMLELEMENT(
						  "ITEMS"
						, XMLAGG(
							XMLELEMENT(
								  "ITEM"
								, XMLATTRIBUTES( 'ISR' AS "MODULE", TYPE AS "TYPE", NAME AS "NAME", ORDER_NUM AS "ORDER")
							)
						)
					)
			INTO	items_order
			FROM ORDER_OF_ALL_OBJECTS
			ORDER BY ORDER_NUM, TYPE, NAME;*/

			RETURN items_order;

		EXCEPTION
			WHEN OTHERS THEN
				addMessage(ERROR, 'getITEMS_ORDER', 'collecting order information failed! [' || SQLERRM || ']');
		END getITEMS_ORDER;

	FUNCTION getNonEmptyTables
		RETURN CLOB AS

			countRows		NUMBER;
			nonEmptyTables	XMLTYPE;

		BEGIN
      addMessage(INFO, 'getNonEmptyTables', 'begin crating table data list..');

			FOR rec IN (SELECT UT.TABLE_NAME FROM USER_TABLES UT) LOOP

				EXECUTE IMMEDIATE 'SELECT COUNT(*) FROM ' || rec.TABLE_NAME into countRows;

				IF countRows > 0 THEN
					SELECT XMLCONCAT(nonEmptyTables, XMLELEMENT("ITEM", XMLATTRIBUTES('ISR' AS "MODULE", 'TABLE_DATA' AS "TYPE", rec.TABLE_NAME AS "NAME")))
					INTO nonEmptyTables FROM DUAL;
				END IF;

			END LOOP;
			addMessage(INFO, 'getNonEmptyTables', 'end crating table data list!');
			SELECT XMLELEMENT("ITEMS", nonEmptyTables) INTO nonEmptyTables FROM DUAL;
			RETURN nonEmptyTables.getClobVal();

		EXCEPTION
			WHEN OTHERS THEN
				RAISE;

		END getNonEmptyTables;

	FUNCTION getXMLTypeForStatement(selectStatement IN CLOB, orderCols IN VARCHAR2)
		RETURN CLOB AS

			output			XMLTYPE;
			finalStatement	CLOB := selectStatement;

		BEGIN

			addMessage(INFO, 'getXMLTypeForStatement', 'begin executing dynamic sql..');
			addMessage(INFO, 'selectStatement', selectStatement);

			IF orderCols IS NOT NULL THEN

				addMessage(DEBUG, 'getXMLTypeForStatement', 'appending order columns..');

				finalStatement := finalStatement || ' ORDER BY ' || orderCols;

				addMessage(DEBUG, 'getXMLTypeForStatement', 'order columns appended!');

			END IF;

			addMessage(DEBUG, 'getXMLTypeForStatement', 'executing final statement..');

			EXECUTE IMMEDIATE finalStatement INTO output;

			addMessage(INFO, 'getXMLTypeForStatement', 'end executing dynamic sql!');

			RETURN output.getClobVal();

		EXCEPTION
			WHEN OTHERS THEN
				addMessage(ERROR, 'getXMLTypeForStatement', 'executing dynamic sql failed! [' || SQLERRM || ']');
				RAISE;

		END getXMLTypeForStatement;

	PROCEDURE prepareMessageStore AS
		PRAGMA AUTONOMOUS_TRANSACTION;

		snapshotLogTable NUMBER;
		snapshotLogSequence NUMBER;

	BEGIN

		SELECT COUNT(OBJECT_NAME) INTO snapshotLogTable FROM USER_OBJECTS WHERE OBJECT_TYPE = 'TABLE' AND OBJECT_NAME = 'ISR$SNAPSHOT$LOGS' AND STATUS = 'VALID';
		SELECT COUNT(OBJECT_NAME) INTO snapshotLogSequence FROM USER_OBJECTS WHERE OBJECT_TYPE = 'SEQUENCE' AND OBJECT_NAME = 'ISR$SNAPSHOT$LOGS$SEQ' AND STATUS = 'VALID';
		IF snapshotLogTable != 1 THEN
			EXECUTE IMMEDIATE 'CREATE TABLE ' || MESSAGE_STORE || ' (ID NUMBER, MSG_TYPE VARCHAR2(4000 CHAR), MSG_PLACE VARCHAR2(4000 CHAR), MSG_TEXT VARCHAR2(4000 CHAR),TIME_I DATE)';
		--ELSE EXECUTE IMMEDIATE 'TRUNCATE TABLE ' || MESSAGE_STORE;
		END IF;
		IF snapshotLogSequence != 1 THEN
			EXECUTE IMMEDIATE 'CREATE SEQUENCE ' || MESSAGE_STORE_SEQ;
		END IF;

	END prepareMessageStore;

	PROCEDURE addMessage(msg_type IN VARCHAR2, msg_place IN VARCHAR2, msg_text IN VARCHAR2) AS
		PRAGMA AUTONOMOUS_TRANSACTION;

		snapshotLogTable NUMBER;
		snapshotLogSequence NUMBER;

		isrLog NUMBER;
		isrLogLevel NUMBER;

	BEGIN

		EXECUTE IMMEDIATE 'INSERT INTO ISR$SNAPSHOT$LOGS VALUES(ISR$SNAPSHOT$LOGS$SEQ.NEXTVAL, :1, :2, :3,:4)'
			USING msg_type, msg_place, substr(msg_text,0, 4000),sysdate;
		COMMIT;

		/*SELECT	COUNT(OBJECT_NAME)
		INTO	isrLog
		FROM	USER_OBJECTS
		WHERE	OBJECT_TYPE = 'PACKAGE BODY'
			AND	OBJECT_NAME = 'STB$TRACE'
			AND STATUS = 'VALID';

		IF isrLog = 1 THEN
			CASE msg_type
				WHEN 'D' THEN isrLogLevel := 250;
				WHEN 'I' THEN isrLogLevel := 100;
				WHEN 'W' THEN isrLogLevel := 50;
				WHEN 'E' THEN isrLogLevel := -1;
				ELSE isrLogLevel := -100;
			END CASE;

			BEGIN
				EXECUTE IMMEDIATE q'{BEGIN STB$TRACE.setLog(:1, 'ISR$SNAPSHOT.' || :2, :3, :4); COMMIT; END;}'
					USING msg_type, msg_place, msg_text, isrLogLevel;
			EXCEPTION
				WHEN OTHERS THEN DBMS_OUTPUT.PUT_LINE('[E] in [ISR$SNAPSHOT.addMessage]:	[' || SQLERRM || ']');
			END;
		ELSE
			DBMS_OUTPUT.PUT_LINE('[' || msg_type || '] in [' || msg_place || ']:	[' || msg_text || ']');
		END IF;*/

	END addMessage;

END ISR$SNAPSHOT;