CREATE OR REPLACE PACKAGE ISR$SNAPSHOT AS
	
	FUNCTION getSnapshotAsBlob(include IN CLOB, exclude IN CLOB)
		RETURN BLOB;
	
	PROCEDURE putSnapshotAsBlob(snapshot_object IN BLOB, include_list IN CLOB);
	
	PROCEDURE collectAll;

	PROCEDURE collectObject(m_name IN VARCHAR2, t_name IN VARCHAR2, o_name IN VARCHAR2);

	PROCEDURE setIncludeList(includeList IN CLOB);

	PROCEDURE setExcludeList(excludeList IN CLOB);

	FUNCTION getObject(m_name IN VARCHAR2, t_name IN VARCHAR2, o_name IN VARCHAR2)
		RETURN CLOB;

	FUNCTION getObjectList(t_name IN VARCHAR2)
	RETURN CLOB;
		
	FUNCTION getTableData
	RETURN CLOB ;
		
	FUNCTION getList(list  IN CLOB,xmlquery_string IN varchar2)
		RETURN CLOB ;
		
		FUNCTION unique_items(list  IN CLOB)
		RETURN CLOB ;
		
		FUNCTION getCountEntry_XmlString(list  IN CLOB,where_list IN varchar2)
		RETURN NUMBER;
		
	PROCEDURE putObject(m_name IN VARCHAR2, t_name IN VARCHAR2, o_name IN VARCHAR2, snapshot_object IN CLOB);
	
	FUNCTION getDATA_DEFAULT(tab_name IN VARCHAR2, col_name VARCHAR2)
		RETURN VARCHAR2;

	FUNCTION getSEARCH_CONDITION(const_name IN VARCHAR2)
		RETURN VARCHAR2;
	
	FUNCTION getVIEW_TEXT(v_name IN VARCHAR2)
		RETURN CLOB;
	
	FUNCTION getITEMS_ORDER
		RETURN CLOB;
	
	FUNCTION getNonEmptyTables
		RETURN CLOB;
		
	FUNCTION getXMLTypeForStatement(selectStatement IN CLOB, orderCols IN VARCHAR2 DEFAULT NULL)
		RETURN CLOB;
	
	PROCEDURE prepareMessageStore;
	
	PROCEDURE addMessage(msg_type IN VARCHAR2, msg_place IN VARCHAR2, msg_text IN VARCHAR2);

END ISR$SNAPSHOT;