CREATE OR REPLACE PACKAGE ISR$SNAPSHOT$FRONTEND AS
	
	FUNCTION createSnapshot(cnJobid IN NUMBER, cxXml IN OUT XMLTYPE)
		RETURN STB$OERROR$RECORD;
	
	FUNCTION callFunction(oParameter IN STB$MENUENTRY$RECORD, olNodeList OUT STB$TREENODELIST)
		RETURN STB$OERROR$RECORD;
	
	FUNCTION putParameters(oParameter IN STB$MENUENTRY$RECORD, olParameter IN STB$PROPERTY$LIST, clParameter in CLOB DEFAULT NULL)
		RETURN STB$OERROR$RECORD;
	
	FUNCTION createConfigXml(jobID IN NUMBER, filePath IN VARCHAR2, overwrite IN VARCHAR2)
		RETURN XMLTYPE;
	
	FUNCTION parseDirectoryFromPath(filePath IN VARCHAR2)
		RETURN VARCHAR2;
	
	FUNCTION parseFileNameFromPath(filePath IN VARCHAR2)
		RETURN VARCHAR2;
	
	FUNCTION parseFileExtensionFromPath(filePath IN VARCHAR2)
		RETURN VARCHAR2;
		function setMenuAccess( sParameter  in  varchar2,
                       sMenuAllowed out varchar2 )  return STB$OERROR$RECORD;

END ISR$SNAPSHOT$FRONTEND;