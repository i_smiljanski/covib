CREATE OR REPLACE PACKAGE BODY ISR$SNAPSHOT$FRONTEND IS

function getSnapshot(includeList IN xmltype, excludeList IN xmltype) return blob
is
		sCurrentName    constant varchar2(100) := $$PLSQL_UNIT||'.getSnapshot';
		snapshot BLOB;
		createSnapshotEx EXCEPTION;
		clInclList   clob;
		clExcllList   clob;
		BEGIN
		
			-- create snapshot
			isr$trace.debug('creating snapshot..',' ',sCurrentName);
			if includeList is not null then
        clInclList := includeList.getClobVal();
      end if;
      if excludeList is not null then
        clExcllList := excludeList.getClobVal();
      end if;

			snapshot :=	ISR$SNAPSHOT.getSnapshotAsBlob(clInclList,clExcllList);
			
			isr$trace.debug('snapshot created!','snapshot size = ' || DBMS_LOB.GETLENGTH(snapshot),sCurrentName);
		  return snapshot;
  EXCEPTION
    WHEN OTHERS THEN
      RAISE createSnapshotEx;
      return null;
  END getSnapshot;
  procedure snapshotToJobTrail(cnJobId IN NUMBER, snapshot IN blob) 
is
		sCurrentName    constant varchar2(100) := $$PLSQL_UNIT||'.snapshotToJobTrail';
		jobTrailId NUMBER;
	  moveSnapshotEx EXCEPTION;

			
		BEGIN
      isr$trace.debug('inserting snapshot into STB$JOBTRAIL..','',sCurrentName);
			-- get id for STB$JOBTRAIL
			jobTrailId := STB$JOBTRAIL$SEQ.NEXTVAL;
			isr$trace.debug('jobTrailId, JOBID',jobTrailId||'   '||cnJobId,sCurrentName);
			-- create STB$JOBTRAIL entry containing the snapshot
			INSERT INTO STB$JOBTRAIL(ENTRYID, JOBID, DOCUMENTTYPE, FILENAME, TIMESTAMP, DOWNLOAD, MIMETYPE, BLOBFLAG, TEXTFILE, BINARYFILE)
			VALUES(jobTrailId, cnJobId, 'TARGET', USER || '_' || cnJobId ||'.isrs', SYSDATE, 'T', 'application/octet-stream', 'B', EMPTY_CLOB(), snapshot);
			COMMIT;
      isr$trace.debug('snapshot inserted into STB$JOBTRAIL!','',sCurrentName);
	EXCEPTION
    WHEN OTHERS THEN
      RAISE moveSnapshotEx;
		END snapshotToJobTrail;



	FUNCTION createSnapshot(cnJobid IN NUMBER, cxXml IN OUT XMLTYPE)
		RETURN STB$OERROR$RECORD AS
		
			-- required for iSR integration
			oErrorObj STB$OERROR$RECORD := STB$OERROR$RECORD(NULL, NULL, STB$TYPEDEF.CNNOERROR);
			sCurrentName    constant varchar2(100) := $$PLSQL_UNIT||'.createSnapshot';
			sMsg varchar2(4000);
			ip STB$JOBSESSION.IP%TYPE;
			port STB$JOBSESSION.PORT%TYPE;
			
			-- exceptions catched in here
			createSnapshotEx EXCEPTION;
			loadListsFileEx EXCEPTION;
			initUploadEx EXCEPTION;
			uploadSnapshotEx EXCEPTION;
			uploadConfigEx EXCEPTION;
			moveSnapshotEx EXCEPTION;
			
			snapshot BLOB;
			jobTrailId NUMBER;
			
			includeList XMLTYPE := NULL;
			excludeList XMLTYPE := NULL;
			
			-- to check return status values of the client loader
			--loaderStatusCode NUMBER;
		
	BEGIN
	
	  isr$trace.stat('begin','begin',sCurrentName);
	  isr$trace.debug('cnJobId',cnJobId,sCurrentName);
		/*
		 * retrieve ip and port of the client loader to exchenge files.
		 */
		 isr$trace.debug('ip','retrieving ip and port of the client loader..',sCurrentName);
		SELECT	JS.ip,
				TO_CHAR(JS.port)
		INTO	ip, port
		FROM	STB$JOBSESSION JS
		WHERE	JS.jobid = cnJobId;
		isr$trace.debug('ip','ip and port of the client loader retrieved!'||'ip=' || ip || ' and port=' ,sCurrentName);

		
		/*
		 * get INCLUDE and EXCLUDE from client.
		 * client provides the lists in a single file.
		 * first, load the whole file into STB$JOBTRAIL.
		 * then, retrieved INCLUDE and EXCLUDE lists from the file.
		 */
		<<get_lists>>
		DECLARE
		
			listsPathOnClient VARCHAR2(1024 CHAR) := STB$JOB.getJobParameter('LISTS_FILE', cnJobId);
			listsFileContent CLOB;
			
		
		BEGIN
      isr$trace.debug('listsPathOnClient',listsPathOnClient,sCurrentName);
			
			/*
			 * first, load the whole file into STB$JOBTRAIL if set.
			 */
			IF listsPathOnClient IS NOT NULL THEN
        isr$trace.debug('loading file containing lists from client..','',sCurrentName);
				
				-- get id for STB$JOBTRAIL
				jobTrailId := STB$JOBTRAIL$SEQ.NEXTVAL;
				-- crate STB$JOBTRAIL entry for the file
				INSERT INTO STB$JOBTRAIL(ENTRYID, JOBID, DOCUMENTTYPE, FILENAME, TIMESTAMP, DOWNLOAD, MIMETYPE, BLOBFLAG, TEXTFILE, BINARYFILE)
				VALUES(jobTrailId, cnJobId, 'TARGET', listsPathOnClient, SYSDATE, 'F', 'text/xml', 'C', EMPTY_CLOB(), EMPTY_BLOB());
				COMMIT;
				
				-- load file
				--loaderStatusCode := STB$JAVA.loadClobFileFromLoader(ip, port, TO_CHAR(cnJobId), listsPathOnClient, jobTrailId, 'STB$JOBTRAIL');
				--IF loaderStatusCode != 0 THEN
				--	RAISE loadListsFileEx;
				--END IF;
				
				-- get the content of the lists file from STB$JOBTRAIL
				SELECT TEXTFILE INTO listsFileContent FROM STB$JOBTRAIL WHERE ENTRYID=jobTrailId;
				
				--STB$TRACE.setLog('file containing lists loaded!', 'ISR$SNAPSHOT$FRONTEND.createSnapshot', 'jobTrailId=' || jobTrailId || ', file content in LOGCLOB', 100, listsFileContent);
				
				/*
				 * now retrieve INCLUDE and EXCLUDE lists from file.
				 */
				
				--STB$TRACE.setLog('retrieving INCLUDE and EXCLUDE lists from file..', 'ISR$SNAPSHOT$FRONTEND.createSnapshot', '', 75);
				SELECT
					  LS.INCLUDE
					, LS.EXCLUDE
				INTO includeList, excludeList
				FROM
					XMLTABLE(
						'let $lists := /ITEMS_LIST
						 return $lists'
						PASSING XMLTYPE(listsFileContent)
						COLUMNS
							INCLUDE	XMLTYPE	PATH 'ITEMS[@TYPE="INCLUDE"]',
							EXCLUDE	XMLTYPE	PATH 'ITEMS[@TYPE="EXCLUDE"]'
					) LS;
				--STB$TRACE.setLog('INCLUDE list retrieved!', 'ISR$SNAPSHOT$FRONTEND.createSnapshot', 'INCLUDE list in LOGCLOB', 100, includeList.getClobVal());
				--STB$TRACE.setLog('EXCLUDE list retrieved!', 'ISR$SNAPSHOT$FRONTEND.createSnapshot', 'EXCLUDE list in LOGCLOB', 100, excludeList.getClobVal());
			
			ELSE
			NULL;
        isr$trace.debug('no lists file set! using default lists..',' ',sCurrentName);
			END IF;
			
		--EXCEPTION
			--WHEN loadListsFileEx THEN STB$TRACE.setLog('loading lists file from client failed!', 'ISR$SNAPSHOT$FRONTEND.createSnapshot', 'loaderStatusCode=' || loaderStatusCode, -1);
			--WHEN OTHERS THEN STB$TRACE.setLog('retrieving lists from file failed! using default lists..', 'ISR$SNAPSHOT$FRONTEND.createSnapshot', 'SQLERRM=' || SQLERRM, -1);
		END;
		
		snapshot := getSnapshot(includeList,excludeList)  ;
		snapshotToJobTrail(cnJobId , snapshot ) ;
		
		isr$trace.debug('end','snapshot created ',sCurrentName);
		RETURN oErrorObj;
		
	EXCEPTION
		WHEN createSnapshotEx THEN
			oErrorObj.sErrorCode := UTD$MSGLIB.GetMsg ('createSnapshotEx', STB$SECURITY.GetCurrentLanguage);
			sMsg :=  UTD$MSGLIB.GetMsg ('createSnapshotExMsg', STB$SECURITY.GetCurrentLanguage);
      oErrorObj.handleError ( Stb$typedef.cnSeverityMessage, sMsg, sCurrentName,  dbms_utility.format_error_stack ||stb$typedef.crlf|| dbms_utility.format_error_backtrace  );    
			STB$JOB.markDBJobAsBroken(cnJobId, ip, port, oErrorObj.sErrorCode, oErrorObj.sErrorMessage, oErrorObj.sSeverityCode);
			RETURN oErrorObj;
		WHEN initUploadEx THEN
			oErrorObj.sErrorCode := UTD$MSGLIB.GetMsg ('initUploadEx', STB$SECURITY.GetCurrentLanguage);
			sMsg := UTD$MSGLIB.GetMsg ('initUploadExMsg', STB$SECURITY.GetCurrentLanguage);
			oErrorObj.handleError ( Stb$typedef.cnSeverityMessage, sMsg, sCurrentName,  
			     dbms_utility.format_error_stack ||stb$typedef.crlf|| dbms_utility.format_error_backtrace  );    
			STB$JOB.markDBJobAsBroken(cnJobId, ip, port, oErrorObj.sErrorCode, oErrorObj.sErrorMessage, oErrorObj.sSeverityCode);
			RETURN oErrorObj;
		WHEN uploadSnapshotEx THEN
			oErrorObj.sErrorCode := UTD$MSGLIB.GetMsg ('uploadSnapshotEx', STB$SECURITY.GetCurrentLanguage);
			sMsg := UTD$MSGLIB.GetMsg ('uploadSnapshotExMsg', STB$SECURITY.GetCurrentLanguage);
			
			oErrorObj.handleError ( Stb$typedef.cnSeverityMessage, sMsg, sCurrentName,  
			     dbms_utility.format_error_stack ||stb$typedef.crlf|| dbms_utility.format_error_backtrace  );  
			STB$JOB.markDBJobAsBroken(cnJobId, ip, port, oErrorObj.sErrorCode, oErrorObj.sErrorMessage, oErrorObj.sSeverityCode);
			RETURN oErrorObj;
		WHEN uploadConfigEx THEN
			oErrorObj.sErrorCode := UTD$MSGLIB.GetMsg ('uploadConfigEx', STB$SECURITY.GetCurrentLanguage);
			sMsg := UTD$MSGLIB.GetMsg ('uploadConfigExMsg', STB$SECURITY.GetCurrentLanguage);
			oErrorObj.handleError ( Stb$typedef.cnSeverityMessage, sMsg, sCurrentName,  
			     dbms_utility.format_error_stack ||stb$typedef.crlf|| dbms_utility.format_error_backtrace  );  
			STB$JOB.markDBJobAsBroken(cnJobId, ip, port, oErrorObj.sErrorCode, oErrorObj.sErrorMessage, oErrorObj.sSeverityCode);
			RETURN oErrorObj;
		WHEN moveSnapshotEx THEN
			oErrorObj.sErrorCode := UTD$MSGLIB.GetMsg ('moveSnapshotEx', STB$SECURITY.GetCurrentLanguage);
			sMsg := UTD$MSGLIB.GetMsg ('moveSnapshotExMsg', STB$SECURITY.GetCurrentLanguage);
			oErrorObj.handleError ( Stb$typedef.cnSeverityMessage, sMsg, sCurrentName,  
			     dbms_utility.format_error_stack ||stb$typedef.crlf|| dbms_utility.format_error_backtrace  );  
			STB$JOB.markDBJobAsBroken(cnJobId, ip, port, oErrorObj.sErrorCode, oErrorObj.sErrorMessage, oErrorObj.sSeverityCode);
			RETURN oErrorObj;
		WHEN OTHERS THEN
			ROLLBACK;
			oErrorObj.sErrorCode := UTD$MSGLIB.GetMsg ('undefinedSnapshotEx', STB$SECURITY.GetCurrentLanguage);
			sMsg := SQLCODE || ': ' || SQLERRM;
      oErrorObj.handleError ( Stb$typedef.cnSeverityCritical, sMsg, sCurrentName, dbms_utility.format_error_stack ||stb$typedef.crlf|| dbms_utility.format_error_backtrace );
			STB$JOB.markDBJobAsBroken(cnJobId, ip, port, oErrorObj.sErrorCode, oErrorObj.sErrorMessage, oErrorObj.sSeverityCode);
			RETURN oErrorObj;
	END createSnapshot;
	
	FUNCTION createConfigXml(jobID IN NUMBER, filePath IN VARCHAR2, overwrite IN VARCHAR2)
		RETURN XMLTYPE IS
		
		configXml XMLTYPE;

	BEGIN
	
		SELECT
			XMLELEMENT(
				  "config"
				, XMLATTRIBUTES(jobID AS "job-id")
				, XMLELEMENT(
					  "apps"
					, XMLATTRIBUTES(1 AS "num")
					, XMLELEMENT("fullpathname", ISR$SNAPSHOT$FRONTEND.parseDirectoryFromPath(filePath))
					, XMLELEMENT("target", ISR$SNAPSHOT$FRONTEND.parseFileNameFromPath(filePath))
					, XMLELEMENT("extension", ISR$SNAPSHOT$FRONTEND.parseFileExtensionFromPath(filePath))
					, XMLELEMENT("overwrite", overwrite)
					, XMLELEMENT(
						  "app"
						, XMLATTRIBUTES(1 AS "order", 'ME' AS "name")
						, XMLELEMENT("wait_for_process", 'false')
						, XMLELEMENT("command", 'SAVE')
						, XMLELEMENT(
							  "sources"
							, XMLELEMENT("source", XMLATTRIBUTES(ISR$SNAPSHOT$FRONTEND.parseFileNameFromPath(filePath) AS "name"))
						)
					)
					, XMLELEMENT("docid")
					--, XMLELEMENT("applicationpathes", '&&PATHES')
					, XMLELEMENT("applicationpathes")
				)
			)
		INTO configXml
		FROM
			DUAL;
		
		RETURN configXml;
	
	EXCEPTION
		WHEN OTHERS THEN NULL;
	END createConfigXml;
	
	FUNCTION parseDirectoryFromPath(filePath IN VARCHAR2)
		RETURN VARCHAR2 IS

	BEGIN
	
		RETURN SUBSTR(filePath, 0, INSTR(filePath, '\', -1));
	
	EXCEPTION
		WHEN OTHERS THEN NULL;
	END parseDirectoryFromPath;
	
	FUNCTION parseFileNameFromPath(filePath IN VARCHAR2)
		RETURN VARCHAR2 IS

	BEGIN
	
		RETURN SUBSTR(filePath, INSTR(filePath, '\', -1) + 1);
	
	EXCEPTION
		WHEN OTHERS THEN NULL;
	END parseFileNameFromPath;
	
	FUNCTION parseFileExtensionFromPath(filePath IN VARCHAR2)
		RETURN VARCHAR2 IS

	BEGIN
	
		RETURN SUBSTR(filePath, INSTR(filePath, '.', -1) + 1);
	
	EXCEPTION
		WHEN OTHERS THEN NULL;
	END parseFileExtensionFromPath;
	
	FUNCTION callFunction (oParameter IN STB$MENUENTRY$RECORD, olNodeList OUT STB$TREENODELIST)
		RETURN STB$OERROR$RECORD IS
		
		sCurrentName         constant varchar2(100) := $$PLSQL_UNIT||'.callFunction('||oParameter.sToken||')';
    oErrorObj                     STB$OERROR$RECORD := STB$OERROR$RECORD();
    oCurNode                      STB$TREENODE$RECORD := STB$TREENODE$RECORD();
    olParameter                   STB$PROPERTY$LIST;
    sMsg                          varchar2(2000);
    sToken                        varchar2(500);


	BEGIN
	isr$trace.stat('begin', 'oParameter.sToken: '||oParameter.sToken ,sCurrentName);
    isr$trace.debug('parameter', 'oParameter', sCurrentName, oParameter);
    olNodeList := STB$TREENODELIST();

    STB$OBJECT.setCurrentToken(oparameter.stoken);

    -- get the current node
    oCurNode := Stb$object.getCurrentNode;
    isr$trace.debug('value', 'oCurNode', sCurrentName, oCurnode);
    
    sToken := UPPER (oParameter.sToken);
    
        olNodeList.EXTEND;
        olNodeList (1) := STB$TREENODE$RECORD(STB$UTIL.getMaskTitle(UPPER (oParameter.sToken)), 'ISR$SNAPSHOT$FRONTEND', 'TRANSPORT', 'MASK', '');
        isr$trace.debug('value','olNodeList',sCurrentName,olNodeList);
        oErrorObj := STB$UTIL.getPropertyList(sToken, olNodeList (1).tloPropertylist);
        
      if olNodeList is not null then -- ISRC-612 for CAN_SELECT return null in nodelist
      isr$trace.debug('value', 'olNodeList OUT olNodeList.count: '||olNodeList.COUNT, sCurrentName, olNodeList);
    end if;
    isr$trace.stat('end', 'end', sCurrentName);
    return oErrorObj;


	
	EXCEPTION
    when others then
      sMsg :=  utd$msglib.getmsg ('ERROR_PLACE', stb$security.getcurrentlanguage, SQLERRM(SQLCODE));
      oErrorObj.handleError ( Stb$typedef.cnSeverityWarning, sMsg, sCurrentName,  DBMS_UTILITY.format_error_stack ||stb$typedef.crlf|| DBMS_UTILITY.format_error_backtrace  );
      rollback;
      return oErrorObj;
	END callFunction;
	
	FUNCTION putParameters(oParameter IN STB$MENUENTRY$RECORD, olParameter IN STB$PROPERTY$LIST, clParameter in CLOB DEFAULT NULL)
		RETURN STB$OERROR$RECORD AS
		
		oErrorObj STB$OERROR$RECORD := STB$OERROR$RECORD(NULL, NULL, STB$TYPEDEF.CNNOERROR);
		sCurrentName         constant varchar2(100) := $$PLSQL_UNIT||'.putParameters('||oParameter.sToken||')';
		nOutputId ISR$REPORT$OUTPUT$TYPE.OUTPUTID%TYPE;
		nActionId ISR$ACTION.ACTIONID%TYPE;
		sFunction ISR$REPORT$OUTPUT$TYPE.ACTIONFUNCTION%TYPE;
		sMsg varchar2(4000);
		
		CURSOR cGetJobIds(cstoken IN ISR$ACTION.TOKEN%TYPE, csdocdefinition IN ISR$ACTION$OUTPUT.DOC_DEFINITION%TYPE) IS
			SELECT	AO.outputid,
					AO.actionid,
					O.actionfunction myfunction
			FROM	ISR$ACTION A,
					ISR$REPORT$OUTPUT$TYPE O,
					ISR$ACTION$OUTPUT AO
			WHERE	(UPPER(O.token) = UPPER(cstoken) OR UPPER(A.token) = UPPER(cstoken)) 
				AND	O.reporttypeid IS NULL 
				AND	O.outputid = AO.outputid 
				AND	A.actionid = AO.actionid
				AND	AO.doc_definition = NVL(csdocdefinition, 'NOT_DETERMINED');
		
	BEGIN
	
	isr$trace.stat('begin','begin',sCurrentName);
  isr$trace.debug('parameter','olParameter',sCurrentName,olParameter);
	
		OPEN cGetJobIds ('CAN_CREATE_SNAPSHOT', null);
			FETCH cGetJobIds
			INTO  nOutputId, nActionId, sFunction;
		CLOSE cGetJobIds;
		
		STB$JOB.nJobid := STB$JOB$SEQ.NEXTVAL;
		
		--oErrorObj := STB$UTIL.createFilterParameter(olParameter);
		
		STB$JOB.setJobParameter ('TOKEN', UPPER (oParameter.sToken), STB$JOB.nJobid);
		STB$JOB.setJobParameter ('OUTPUTID', nOutputId, STB$JOB.nJobid);
		STB$JOB.setJobParameter ('ACTIONID', nActionId, STB$JOB.nJobid);
		--STB$JOB.setJobParameter ('WHERE', NULL, STB$JOB.nJobid);
		STB$JOB.setJobParameter ('REFERENCE', STB$OBJECT.getCurrentNodeId, STB$JOB.nJobid);
		STB$JOB.setJobParameter ('FUNCTION', sFunction, STB$JOB.nJobid);
		STB$JOB.setJobParameter ('ACTIONTYPE', 'SAVE', STB$JOB.nJobid);
		
		STB$JOB.setJobParameter ('TO', STB$UTIL.getProperty(olParameter, 'SNAPSHOT_FILE'), STB$JOB.nJobid);
		STB$JOB.setJobParameter ('LISTS_FILE', STB$UTIL.getProperty(olParameter, 'LISTS_FILE'), STB$JOB.nJobid);
		STB$JOB.setJobParameter ('OVERWRITE', STB$UTIL.getProperty(olParameter, 'OVERWRITE_SNAPSHOT_FILE'), STB$JOB.nJobid);
		--STB$JOB.setJobParameter ('OVERWRITE_SNAPSHOT_FILE', STB$UTIL.getProperty(olParameter, 'OVERWRITE_SNAPSHOT_FILE'), STB$JOB.nJobid);

		
		oErrorObj := STB$JOB.startJob;
		
		RETURN oErrorObj;
	
	EXCEPTION
		WHEN OTHERS THEN
			ROLLBACK;
			oErrorObj.sErrorCode := UTD$MSGLIB.GetMsg ('ERROR', Stb$security.GetCurrentLanguage) || SQLCODE || ' ' || SQLERRM;
			sMsg := UTD$MSGLIB.GetMsg ('ERROR_PLACE', Stb$security.GetCurrentLanguage) || ' ISR$SNAPSHOT.putParameters';
      oErrorObj.handleError ( Stb$typedef.cnSeverityWarning, sMsg, sCurrentName,  DBMS_UTILITY.format_error_stack ||stb$typedef.crlf|| DBMS_UTILITY.format_error_backtrace  );
			RETURN oErrorObj;
	END putParameters;
	
	
	function setMenuAccess( sParameter  in  varchar2,
                       sMenuAllowed out varchar2 )  return STB$OERROR$RECORD
is
  sCurrentName constant varchar2(100) := $$PLSQL_UNIT||'.setMenuAccess(' || sParameter || ')';
  oErrorObj    STB$OERROR$RECORD := STB$OERROR$RECORD();
  sMsg        varchar2(4000);

begin
  isr$trace.info_all('begin', 'sParameter: ' || sParameter || ' '  || sMenuAllowed,  sCurrentName);
  sMenuAllowed := Stb$security.checkGroupRight (sParameter);
  isr$trace.info_all ('end',  'sMenuAllowed:' || sMenuAllowed, sCurrentName);
  return oErrorObj;
exception
  when others then
    sMsg :=  utd$msglib.getmsg ('exUnexpectedError', stb$security.getcurrentlanguage, csP1 => sqlerrm(sqlcode));
    oErrorObj.handleError ( Stb$typedef.cnSeverityCritical, sMsg, sCurrentName,
                            substr(dbms_utility.format_error_stack || CHR(10) || dbms_utility.format_error_backtrace, 1,4000)  );
    return oErrorObj;
end setMenuAccess;

END ISR$SNAPSHOT$FRONTEND;