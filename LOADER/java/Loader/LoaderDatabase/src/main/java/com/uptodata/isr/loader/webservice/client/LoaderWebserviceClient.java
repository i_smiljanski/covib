package com.uptodata.isr.loader.webservice.client;

import com.uptodata.isr.db.connection.ConnectionUtil;
import com.uptodata.isr.db.trace.DbLogging;
import com.uptodata.isr.utils.ConvertUtils;
import com.uptodata.isr.utils.exceptions.MessageStackException;
import com.uptodata.isr.utils.ws.SOAPUtil;
import org.apache.commons.codec.binary.Base64;
import org.apache.commons.lang.StringUtils;

import java.io.IOException;
import java.sql.Blob;
import java.sql.Clob;
import java.util.Arrays;

/**
 * @author dortm19
 */
public class LoaderWebserviceClient {

    public static String ping(String host, String port, String context, String namespace, int timeout) {
        String returnMsg;
        try {
            DbLogging log = new DbLogging();
            String url = "http://" + host + ":" + port + context;
            log.stat("begin ", url);
            String ping = SOAPUtil.callMethod(url, namespace, "ping", timeout);
            log.stat("end", "ping=" + ping);
            if ("true".equals(ping)) {
                returnMsg = null;
            } else {
                returnMsg = "server " + host + ":" + port + " not responding";
            }
        } catch (MessageStackException e) {
            returnMsg = e.toXml().xmlToString();
        }
        if (StringUtils.isEmpty(returnMsg)) {
            return null;
        } else {
            return returnMsg.length() > 4000 ? returnMsg.substring(0, 3999) : returnMsg;
        }
    }


    public static String sendJobList(String host, String port, String context, String namespace, java.sql.Blob file,
                                     int timeout) {
        try {
            String url = "http://" + host + ":" + port + context;
            String[] args = new String[1];
            byte[] fileBytes = ConvertUtils.getBytes(file);
            args[0] = Base64.encodeBase64String(fileBytes);
            return SOAPUtil.callMethod(url, namespace, args, "sendJobList", timeout);
        } catch (Exception ex) {
            return ex.getMessage() + ": " + host + " " + port;
        }
    }

    public static String startJob(String host, String port, String context, String namespace, int jobid,
                                  String foldername, String reactivate, int jobCount, int timeout) {
        try {
            DbLogging log = new DbLogging();
            String url = "http://" + host + ":" + port + context;
            log.stat("begin", "url = " + url);
            String[] args = new String[4];
            args[0] = jobid + "";
            args[1] = foldername;
            args[2] = String.valueOf(reactivate.equals("T"));
            args[3] = String.valueOf(jobCount);

            log.debug("params", Arrays.asList(args));
            String ret = SOAPUtil.callMethod(url, namespace, args, "startJob", timeout);
            log.stat("end", "return = " + ret);
            return ret;
        } catch (Exception ex) {
            return ex.getMessage() + ": " + host + " " + port;
        }
    }

    public static String terminateJob(String host, String port, String context, String namespace, int jobid,
                                      int jobCount, int timeout) {
        try {
            String url = "http://" + host + ":" + port + context;
            String[] args = new String[2];
            args[0] = String.valueOf(jobid);
            args[1] = String.valueOf(jobCount);
            return SOAPUtil.callMethod(url, namespace, args, "terminateJob", timeout);
        } catch (Exception ex) {
            return ex.getMessage() + ": " + host + " " + port;
        }
    }


    public static String showTerminateSuccess(String host, String port, String context, String namespace, int jobid, int timeout) {
        return showTerminateJob(host, port, context, namespace, jobid, timeout, "showTerminateSuccess");
    }


    public static String showTerminateWarning(String host, String port, String context, String namespace, int jobid, int timeout) {
        return showTerminateJob(host, port, context, namespace, jobid, timeout, "showTerminateWarning");
    }


    private static String showTerminateJob(String host, String port, String context, String namespace, int jobid, int timeout, String function) {
        try {
            String url = "http://" + host + ":" + port + context;
            String[] args = new String[1];
            args[0] = String.valueOf(jobid);
            return SOAPUtil.callMethod(url, namespace, args, function, timeout);
        } catch (Exception ex) {
            return ex.getMessage() + ": " + host + " " + port;
        }
    }

    public static String showJobError(String host, String port, String context, String namespace, int jobid,
                                      String message, int timeout) {
        try {
            String url = "http://" + host + ":" + port + context;
            String[] args = new String[2];
            args[0] = jobid + "";
            args[1] = message;
            return SOAPUtil.callMethod(url, namespace, args, "showJobError", timeout);
        } catch (Exception ex) {
            return ex.getMessage() + ": " + host + " " + port;
        }
    }

    public static Clob sendErrorObject(String host, String port, String context, String namespace,
                                       Clob message, int jobId, int timeout) throws Exception {
        DbLogging log = new DbLogging();
        String url = "http://" + host + ":" + port + context;
        log.stat("begin", "url = " + url);
        try {
            byte[] inputXml = ConvertUtils.getBytes(message);
            String[] args = new String[2];
            args[0] = String.valueOf(jobId);
            args[1] = Base64.encodeBase64String(inputXml);
            String result = SOAPUtil.callMethod(url, namespace, args, "showErrorMessage", timeout);
            log.stat("end", result);
            return null;
        } catch (MessageStackException e) {
            log.error("exception", e);
            String erStr = e.toXml().xmlToString();
            log.debug("after xmlToString", erStr);
            byte[] exBytes = erStr.getBytes();
            return ConvertUtils.getClob(ConnectionUtil.getDefaultConnection(), exBytes);
        }
    }


    public static String showJobProgress(String host, String port, String context, String namespace,
                                         int jobid, String text, int progress, int timeout) throws MessageStackException {
        String url = "http://" + host + ":" + port + context;
        DbLogging log = new DbLogging();
        log.stat("begin", "url: " + url);
        String[] args = new String[3];
        args[0] = jobid + "";
        args[1] = text;
        args[2] = progress + "";
        return SOAPUtil.callMethod(url, namespace, args, "showJobProgress", timeout);
    }

    public static Clob startApp(String host, String port, String context, String namespace, int jobid, int timeout) throws MessageStackException {
        String response;
        try {
            String url = "http://" + host + ":" + port + context;
            String[] args = new String[2];
            args[0] = jobid + "";
            args[1] = timeout / 60 + "";
            try {
                response = SOAPUtil.callMethod(url, namespace, args, "startApp", timeout);
            } catch (Exception e) {
                MessageStackException exList;
                if (e instanceof MessageStackException) {
                    exList = (MessageStackException) e;
                } else {
                    exList = new MessageStackException(e, MessageStackException.SEVERITY_CRITICAL,
                            e.getMessage() + " , url: " + url,
                            MessageStackException.getCurrentMethod());
                }
                response = exList.toXml().xmlToString();
                if (StringUtils.isNotEmpty(response)) {
                    return ConvertUtils.getClob(ConnectionUtil.getDefaultConnection(), response.getBytes());
                }
            }
            return null;
        } catch (Exception ex) {
            throw new MessageStackException(ex, MessageStackException.SEVERITY_CRITICAL, ex.getMessage(),
                    MessageStackException.getCurrentMethod());
        }
    }


    public static Clob loadFileToLoader(String host, String port, String context, String namespace,
                                        int jobid, String filename, String configName, String docType, String logLevel, String logReference, int timeout)
            throws MessageStackException {
        DbLogging log = new DbLogging();
        String url = "http://" + host + ":" + port + context;
        log.stat("begin", "url: " + url + "; filename = " + filename);
        String response;
        try {
            String[] args = getArgsForLoadFile(jobid, filename, configName, docType, logLevel, logReference);
            try {
                response = SOAPUtil.callMethod(url, namespace, args, "loadFileToLoader", timeout);
                log.stat("end", "response: " + response);
            } catch (Exception e) {
                MessageStackException exList = new MessageStackException(e, MessageStackException.SEVERITY_CRITICAL,
                        e.getMessage() + " , url: " + url,
                        MessageStackException.getCurrentMethod());
                response = exList.toXml().xmlToString();
                if (StringUtils.isNotEmpty(response)) {
                    return ConvertUtils.getClob(ConnectionUtil.getDefaultConnection(), response.getBytes());
                }
            }
            return null;
        } catch (Exception ex) {
            throw new MessageStackException(ex, MessageStackException.SEVERITY_CRITICAL, ex.getMessage() + ", url: " + url,
                    MessageStackException.getCurrentMethod());
        }
    }


    private static String[] getArgsForLoadFile(int jobid, String filename, String configName, String docType, String logLevel, String logReference) {
        String[] args = new String[6];
        args[0] = jobid + "";
        args[1] = filename;
        args[2] = configName;
        args[3] = docType;
        args[4] = logLevel;
        args[5] = logReference;
        return args;
    }


    public static String loadFileFromLoader(String host, String port, String context, String namespace, int jobid,
                                            String filename, String configName, String docType, String logLevel,
                                            String logReference, int timeout) throws MessageStackException {
        try {
            DbLogging log = new DbLogging();
            String url = "http://" + host + ":" + port + context;
            log.stat("begin", "url: " + url + " filename: " + filename);
            String[] args = getArgsForLoadFile(jobid, filename, configName, docType, logLevel, logReference);

            String response = SOAPUtil.callMethod(url, namespace, args, "loadFileFromLoader", timeout);
            log.stat("end", "end " + response);
            return response;
        } catch (Exception ex) {
            throw new MessageStackException(ex, MessageStackException.SEVERITY_CRITICAL, ex.getMessage() + ": " + host + " " + port,
                    MessageStackException.getCurrentMethod());
        }
    }


    public static String loadLoaderDir(String host, String port, String context, String namespace, int jobid,
                                       String configName, String logLevel, String logReference, int timeout) throws MessageStackException {
        try {
            DbLogging log = new DbLogging();
            String url = "http://" + host + ":" + port + context;
            log.stat("begin", "url: " + url);
            String[] args = new String[4];
            args[0] = jobid + "";
            args[1] = configName;
            args[2] = logLevel;
            args[3] = logReference;

            String response = SOAPUtil.callMethod(url, namespace, args, "uploadLoaderDir", timeout);
            log.stat("end", "end " + response);
            return response;
        } catch (Exception ex) {
            throw new MessageStackException(ex, MessageStackException.SEVERITY_CRITICAL, ex.getMessage() + ": " + host + " " + port,
                    MessageStackException.getCurrentMethod());
        }
    }

    public static String getFilesNamesInDirectory(String host, String port, String context, String namespace, int jobid,
                                                  int timeout) {
        try {
            String url = "http://" + host + ":" + port + context;
            String[] args = new String[1];
            args[0] = jobid + "";
            return SOAPUtil.callMethod(url, namespace, args, "getFilesNamesInDirectory", timeout);
        } catch (Exception ex) {
            return ex.getMessage() + ": " + host + " " + port;
        }
    }

    public static String updateTree(String host, String port, String context, String namespace, int timeout,
                                    String nodetype, String nodeid, String side) {
        try {
            DbLogging log = new DbLogging();
            String url = "http://" + host + ":" + port + context;
            log.stat("begin", "url: " + url + " nodetype: " + nodetype);
            String[] args = new String[4];
            args[0] = timeout + "";
            args[1] = nodetype;
            args[2] = nodeid;
            args[3] = side;
            log.debug("args", Arrays.asList(args));
            String response = SOAPUtil.callMethod(url, namespace, args, "updateTree", timeout);
            log.stat("end", "end " + response);
            return response;
        } catch (Exception ex) {
            return ex.getMessage() + ": " + host + " " + port;
        }
    }

    public static String updateRibbon(String host, String port, String context, String namespace, int timeout) {
        try {
            String url = "http://" + host + ":" + port + context;
            String[] args = new String[1];
            args[0] = timeout + "";
            return SOAPUtil.callMethod(url, namespace, args, "updateRibbon", timeout);
        } catch (Exception ex) {
            return ex.getMessage() + ": " + host + " " + port;
        }
    }

    public static String sendDbToken(String host, String port, String context, String namespace, java.sql.Blob file1,
                                     java.sql.Blob file2, int timeout) {
        try {
            DbLogging log = new DbLogging();
            String url = "http://" + host + ":" + port + context;
            log.stat("begin", url);
            String[] args = new String[2];
            byte[] fileBytes1 = ConvertUtils.getBytes(file1);
            args[0] = Base64.encodeBase64String(fileBytes1);
            byte[] fileBytes2 = ConvertUtils.getBytes(file2);
            args[1] = Base64.encodeBase64String(fileBytes2);
            SOAPUtil.callMethod(url, namespace, args, "saveDbToken", timeout);
            log.stat("end", "T");
            return "T";
        } catch (Exception ex) {
            return ex.getMessage() + ": " + host + " " + port;
        }
    }

    public static String removeJobFolder(String host, String port, String context, String namespace, int jobId, int timeout) {
        try {
            DbLogging log = new DbLogging();
            String url = "http://" + host + ":" + port + context;
            log.stat("begin", url);
            String[] args = new String[1];
            args[0] = String.valueOf(jobId);
            SOAPUtil.callMethod(url, namespace, args, "removeJobFolder", timeout);
            log.stat("end", "T");
            return "T";
        } catch (Exception ex) {
            return ex.getMessage() + ": " + host + " " + port;
        }
    }


    public static String compareDocumentOID(String host, String port, String context, String namespace,
                                            String documentOID, Blob file, int timeout) {
        try {
            DbLogging log = new DbLogging();
            String url = "http://" + host + ":" + port + context;
            log.stat("begin", url);
            String[] args = new String[2];
            args[0] = documentOID;
            byte[] fileBytes = ConvertUtils.getBytes(file);
            args[1] = Base64.encodeBase64String(fileBytes);
            String ret = SOAPUtil.callMethod(url, namespace, args, "compareDocumentOID", timeout);
            log.stat("end", ret);
            return ret;
        } catch (Exception ex) {
            return ex.getMessage() + ": " + host + " " + port;
        }
    }


    public static String protectAllowOnlyRevisions(String host, String port, String context, String namespace,
                                                   int jobId, String fileName, String password, Blob file, int timeout) {
        try {
            DbLogging log = new DbLogging();
            String url = "http://" + host + ":" + port + context;
            log.stat("begin", url);
            String[] args = new String[4];
            args[0] = String.valueOf(jobId);
            args[1] = fileName;
            args[2] = password;
            byte[] fileBytes = ConvertUtils.getBytes(file);
            args[3] = Base64.encodeBase64String(fileBytes);
            String ret = SOAPUtil.callMethod(url, namespace, args, "protectAllowOnlyRevisions", timeout);
            log.stat("end", ret);
            return ret;
        } catch (Exception ex) {
            return ex.getMessage() + ": " + host + " " + port;
        }
    }


    public static void main(String[] args) throws IOException, MessageStackException {
        String ret = showJobProgress("10.0.100.219", "5031", "/ws/rendering/isr/uptodata/com",
                "http://ws.rendering.isr.uptodata.com/", 3783, "Ich teste", 64, 1);
        System.out.println("ret = " + ret);
    }
}
