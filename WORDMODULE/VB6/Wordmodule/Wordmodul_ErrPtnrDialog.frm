VERSION 5.00
Begin VB.Form ErrPtnr 
   BorderStyle     =   3  'Fester Dialog
   Caption         =   "ProgInfo - Error Message"
   ClientHeight    =   6045
   ClientLeft      =   1395
   ClientTop       =   2220
   ClientWidth     =   7425
   ControlBox      =   0   'False
   LinkTopic       =   "Form1"
   MaxButton       =   0   'False
   MinButton       =   0   'False
   ScaleHeight     =   6045
   ScaleWidth      =   7425
   ShowInTaskbar   =   0   'False
   Begin VB.Frame framComment 
      BackColor       =   &H00000000&
      BorderStyle     =   0  'Kein
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   -1  'True
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H000000FF&
      Height          =   975
      Left            =   0
      TabIndex        =   0
      Top             =   0
      Width           =   7215
      Begin VB.Label lblComment 
         BackStyle       =   0  'Transparent
         Caption         =   "lblComment"
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H00FFFFFF&
         Height          =   735
         Left            =   120
         TabIndex        =   1
         Top             =   120
         Width           =   6975
         WordWrap        =   -1  'True
      End
   End
   Begin VB.Frame framFehler 
      Caption         =   "Error description:"
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00000000&
      Height          =   1575
      Left            =   0
      TabIndex        =   2
      Top             =   960
      Width           =   7215
      Begin VB.Timer timTimeout 
         Interval        =   10000
         Left            =   6720
         Top             =   1080
      End
      Begin VB.Label lblErrTitel 
         Caption         =   "Error-Nr:"
         Height          =   255
         Index           =   0
         Left            =   960
         TabIndex        =   4
         Top             =   240
         Width           =   975
      End
      Begin VB.Label lblErrTitel 
         Caption         =   "Module:"
         Height          =   255
         Index           =   1
         Left            =   960
         TabIndex        =   6
         Top             =   720
         Width           =   975
      End
      Begin VB.Label lblErrTitel 
         Caption         =   "Procedure:"
         Height          =   255
         Index           =   2
         Left            =   960
         TabIndex        =   8
         Top             =   960
         Width           =   975
      End
      Begin VB.Label lblErrTitel 
         BackStyle       =   0  'Transparent
         Caption         =   "Line-Nr.:"
         Height          =   255
         Index           =   3
         Left            =   960
         TabIndex        =   10
         Top             =   1200
         Width           =   975
      End
      Begin VB.Label lblError 
         BackColor       =   &H00FFFFFF&
         BorderStyle     =   1  'Fest Einfach
         Caption         =   "lblError(0)"
         Height          =   495
         Index           =   0
         Left            =   2040
         TabIndex        =   5
         Top             =   240
         Width           =   5085
      End
      Begin VB.Label lblError 
         AutoSize        =   -1  'True
         BackColor       =   &H00FFFFFF&
         BorderStyle     =   1  'Fest Einfach
         Caption         =   "lblError(1)"
         Height          =   255
         Index           =   1
         Left            =   2040
         TabIndex        =   7
         Top             =   720
         Width           =   840
      End
      Begin VB.Label lblError 
         AutoSize        =   -1  'True
         BackColor       =   &H00FFFFFF&
         BorderStyle     =   1  'Fest Einfach
         Caption         =   "lblError(2)"
         Height          =   255
         Index           =   2
         Left            =   2040
         TabIndex        =   9
         Top             =   960
         Width           =   840
      End
      Begin VB.Label lblError 
         AutoSize        =   -1  'True
         BackColor       =   &H00FFFFFF&
         BorderStyle     =   1  'Fest Einfach
         Caption         =   "lblError(3)"
         Height          =   255
         Index           =   3
         Left            =   2040
         TabIndex        =   11
         Top             =   1200
         Width           =   840
      End
      Begin VB.Label Label1 
         Alignment       =   2  'Zentriert
         AutoSize        =   -1  'True
         Caption         =   " ! "
         BeginProperty Font 
            Name            =   "Arial"
            Size            =   36
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H000000FF&
         Height          =   840
         Left            =   120
         TabIndex        =   3
         Top             =   360
         Width           =   645
      End
   End
   Begin VB.Frame framProtocol 
      Caption         =   "Error protocol ..."
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00000000&
      Height          =   735
      Left            =   0
      TabIndex        =   12
      Top             =   2520
      Width           =   7215
      Begin VB.CommandButton cmdProtocol 
         Caption         =   "&WWW"
         Height          =   375
         Index           =   3
         Left            =   5640
         TabIndex        =   16
         Top             =   240
         Width           =   1335
      End
      Begin VB.CommandButton cmdProtocol 
         Caption         =   "&Save"
         Height          =   375
         Index           =   1
         Left            =   2040
         TabIndex        =   14
         Top             =   240
         Width           =   1335
      End
      Begin VB.CommandButton cmdProtocol 
         Caption         =   "&EMail"
         Height          =   375
         Index           =   2
         Left            =   3840
         TabIndex        =   15
         Top             =   240
         Width           =   1335
      End
      Begin VB.CommandButton cmdProtocol 
         Caption         =   "&Print"
         Height          =   375
         Index           =   0
         Left            =   240
         TabIndex        =   13
         Top             =   240
         Width           =   1335
      End
   End
   Begin VB.Frame framCallStack 
      Caption         =   "CallStack:"
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00000000&
      Height          =   1695
      Left            =   0
      TabIndex        =   17
      Top             =   3240
      Width           =   7215
      Begin VB.ListBox lstCallStack 
         Height          =   1395
         IntegralHeight  =   0   'False
         Left            =   120
         TabIndex        =   18
         Top             =   240
         Width           =   6975
      End
   End
   Begin VB.Frame framContinue 
      Caption         =   "Futher program operation..."
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00000000&
      Height          =   855
      Left            =   0
      TabIndex        =   19
      Top             =   4920
      Width           =   7215
      Begin VB.CommandButton cmdContinue 
         Caption         =   "&Repeat Command"
         Height          =   495
         Index           =   0
         Left            =   240
         TabIndex        =   20
         Top             =   240
         Width           =   1335
      End
      Begin VB.CommandButton cmdContinue 
         Caption         =   "&Continue with next command"
         Height          =   495
         Index           =   1
         Left            =   2040
         TabIndex        =   21
         Top             =   240
         Width           =   1335
      End
      Begin VB.CommandButton cmdContinue 
         Caption         =   "&Leave Procedure"
         Height          =   495
         Index           =   2
         Left            =   3840
         TabIndex        =   22
         Top             =   240
         Width           =   1335
      End
      Begin VB.CommandButton cmdContinue 
         Caption         =   "&Quit Program"
         Height          =   495
         Index           =   3
         Left            =   5640
         TabIndex        =   23
         Top             =   240
         Width           =   1335
      End
   End
End
Attribute VB_Name = "ErrPtnr"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Option Explicit

' The following code block includes the dialog settings
' which are modified ONLY by the ErrorPartner wizard.
'
'-- START Settings --- Do not modify THIS line!
Const ErrPtnr_LogFile% = 1
Const ErrPtnr_EMail$ = "maintenance@uptodata.de"
Const ErrPtnr_WWW$ = "www.uptodata.de"
Const ErrPtnr_Comment$ = "A problem occurred. Please contact technical support of up to data professional services GmbH."
Const ErrPtnr_cmd_End% = 1
Const ErrPtnr_cmd_Exit% = 0
Const ErrPtnr_cmd_Next% = 0
Const ErrPtnr_cmd_Resume% = 0
Const ErrPtnr_fram_CallStack% = 0
Const ErrPtnr_cmd_WWW% = 0
Const ErrPtnr_cmd_FileSave% = 1
Const ErrPtnr_cmd_EMail% = 0
Const ErrPtnr_cmd_Print% = 1
Const ErrPtnr_fram_Protocol% = 1
Const ErrPtnr_fram_ErrInfo% = 1
Const ErrPtnr_fram_Comment% = 1
'-- END Settings --- Do not modify THIS line!
'

' We need this declaration/function to open a URL from
' within the error dialog.
Private Declare Function ShellExecute Lib "shell32" Alias "ShellExecuteA" (ByVal hwnd As Long, ByVal lpOperation As String, ByVal lpFile As String, ByVal lpParameters As String, ByVal lpDirectory As String, ByVal nShowCmd As Long) As Long

' We need this declaration/function to identify the current
' Windows version.
Private Type OSVERSIONINFO
    dwOSVersionInfoSize As Long
    dwMajorVersion As Long
    dwMinorVersion As Long
    dwBuildNumber As Long
    dwPlatformId As Long
    szCSDVersion As String * 128
End Type
Private Declare Function GetVersionEx& Lib "kernel32" Alias "GetVersionExA" (lpVersionInformation As OSVERSIONINFO)
Private Const VER_PLATFORM_WIN32_NT& = 2
Private Const VER_PLATFORM_WIN32_WINDOWS& = 1
Private Const VER_PLATFORM_WIN32s& = 0

' count of rows for the CallStack-Listbox/Collection
Private Const CallStackLength% = 40
' The CallStack is during Runtime in a collection
Private CallStackCollection As New Collection
' DON'T fill the ListBox direct during Runtime,
' becuse the formcode is then loaded and must
' manually unload to end the main-programm !!!
' Thias was the bug in this form bevor V6.0.130)

Dim Continuation%, ProgInfo$

Private Sub cmdContinue_Click(Index As Integer)
'.------------------------------------------------------------------------------
'.  Function :  The user just decided how to continue after the error
'.              situation. The error dialog will be closed.
'.------------------------------------------------------------------------------
Continuation% = Index
Hide
''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''[ custom code ]'''
'If Continuation% = 4 Then ExitProc -1
''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''[ custom code ]'''
End Sub

Private Sub BuildDialog(ContBits%)
'.------------------------------------------------------------------------------
'.  Function :  Display the error dialog corresponding to the settings.
'.------------------------------------------------------------------------------

Dim y&  ' y-position of the next dialog frame
y& = 0
' comments
With framComment
    .Visible = ErrPtnr_fram_Comment
    If ErrPtnr_fram_Comment Then
        .Move 0, y&
        y& = y& + .Height
        lblComment = ErrPtnr_Comment
    End If
End With
'--------------
' error description
With framFehler
    .Visible = ErrPtnr_fram_ErrInfo
    If ErrPtnr_fram_ErrInfo Then
        .Move 0, y&
        y& = y& + .Height
    End If
End With
'--------------
' protocol buttons
With framProtocol
    .Visible = ErrPtnr_fram_Protocol
    If ErrPtnr_fram_Protocol Then
        .Move 0, y&
        y& = y& + .Height
        cmdProtocol(0).Visible = ErrPtnr_cmd_Print
        cmdProtocol(1).Visible = ErrPtnr_cmd_FileSave
        cmdProtocol(2).Visible = ErrPtnr_cmd_EMail
        cmdProtocol(3).Visible = ErrPtnr_cmd_WWW
    End If
End With
'--------------
' call stack
With framCallStack
    .Visible = ErrPtnr_fram_CallStack
    If ErrPtnr_fram_CallStack Then
        .Move 0, y&
        y& = y& + .Height
    End If
End With
'--------------
' continuation buttons
With framContinue
    .Move 0, y&
    y& = y& + .Height
    cmdContinue(0).Visible = (ErrPtnr_cmd_Resume And (ContBits% And 1) = 1)
    cmdContinue(1).Visible = (ErrPtnr_cmd_Next And (ContBits% And 2) = 2)
    cmdContinue(2).Visible = (ErrPtnr_cmd_Exit And (ContBits% And 4) = 4)
    cmdContinue(3).Visible = (ErrPtnr_cmd_End And (ContBits% And 8) = 8)
End With

'----------------------------------------------------------
' adjust the form's height and width
'--------------
Width = (Width - ScaleWidth) + framFehler.Width
Height = (Height - ScaleHeight) + y&
Move (Screen.Width - Width) / 2, (Screen.Height - Height) / 2

End Sub

Public Function OnError%(AktModul$, AktProc$, Optional ContBits% = 255)
'.------------------------------------------------------------------------------
'.  Function :  The automatic error handler routines inserted in the project by
'.              ErrorPartner call this function.
'.              1. identify the error conditions
'.              2. display the error dialog (modal)
'.              3. Return the contionuation code to the calling function
'.------------------------------------------------------------------------------

''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''[ custom code ]'''
'capture error msg
Dim sErrDesc As String
Dim sErrNum As Long
Dim sErrLine As Long

sErrDesc = Err.Description
sErrNum = Err.Number
sErrLine = Erl()

If AktModul = "modWord" Then
    modError.WriteErrorToLog sErrDesc, sErrNum, sErrLine
    goLogging.CloseLog
    If bDocsOpen = True Then
        modError.SaveDoc oWordProps.sTargetDocName, oWord
    End If
    If Not oWord Is Nothing Then
        oWord.Quit wdDoNotSaveChanges
        Set oWord = Nothing
    End If
End If
''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''[ custom code ]'''

' fill the dialog's label captions
lblError(0) = sErrNum & " (" & sErrDesc & ")"
lblError(1) = AktModul$
lblError(2) = AktProc$
lblError(3) = sErrLine

' create programm information
If Len(ProgInfo$) = 0 Then ProgInfo$ = App.EXEName + "  V" & App.Major & "." & App.Minor & "." & App.Revision
Caption = ProgInfo$ + " - Error message"
Screen.MousePointer = vbDefault

' fill the CallStack-listbox
CallStack2Listbox

' build the error dialog
BuildDialog ContBits%

' write the log file
If ErrPtnr_LogFile% Then WriteLogfile

' display the dialog
Beep
Show vbModal

''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''[ custom code ]'''
'Alles weitere kann man sich schenken da nur die Option "Programm verlassen" m�glich ist
If sErrNum = -2147417851 Then ' Spezialbehandlung f�r diesen Fehler - neustarten l�st oft das Problem.
    ExitProcess -3
Else
    ExitProcess -1
End If
''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''[ custom code ]'''

' return the continuation code to the calling procedure
OnError% = Continuation%

End Function

Private Sub BuildProtocol(a$)
'.------------------------------------------------------------------------------
'.  Function :  concatenate and append the error protocol
'.------------------------------------------------------------------------------

Dim t$, r$, F%, e&, b$
Dim v As OSVERSIONINFO

t$ = String$(70, "-") + vbCrLf
r$ = Space$(10)

' header
a$ = t$
a$ = a$ + "E R R O R P R O T O C O L from ErrorPartner" + vbCrLf
' program info
a$ = a$ + t$
a$ = a$ + Caption + vbCrLf
' error description
a$ = a$ + t$
a$ = a$ + UCase$(framFehler.Caption) + vbCrLf
For F% = 0 To 3
    a$ = a$ + r$ + lblErrTitel(F%) + vbTab + lblError(F%) + vbCrLf
Next
a$ = a$ + r$ + "Date/Time:" + vbTab + Date$ + " / " + Time$ + vbCrLf
' call stack
a$ = a$ + t$
a$ = a$ + UCase$(framCallStack.Caption) + vbCrLf
For F% = 0 To lstCallStack.ListCount - 1
    a$ = a$ + r$ + lstCallStack.List(F%) + vbCrLf
Next
' system information
a$ = a$ + t$
a$ = a$ + "SYSTEMINFO:" + vbCrLf
a$ = a$ + r$ + "Platform:" + vbTab
v.dwOSVersionInfoSize = 148
e& = GetVersionEx&(v)
Select Case v.dwPlatformId
Case VER_PLATFORM_WIN32_NT: a$ = a$ + "Windows NT" + vbCrLf
Case VER_PLATFORM_WIN32_WINDOWS: a$ = a$ + "Windows 95" + vbCrLf
Case VER_PLATFORM_WIN32s: a$ = a$ + "Win32s" + vbCrLf
End Select
a$ = a$ + r$ + "Version:" + vbTab & v.dwMajorVersion & "." & v.dwMinorVersion & vbCrLf
a$ = a$ + r$ + "Build:" + vbTab & (v.dwBuildNumber And &HFFFF&) & "   " & LPSTRToVBString$(v.szCSDVersion) & vbCrLf
' Ende
a$ = a$ + t$

End Sub

Private Sub cmdProtocol_Click(Index As Integer)
'.------------------------------------------------------------------------------
'.  Function :  How does the user want to preceed with
'.              the error protocol?
'.------------------------------------------------------------------------------

Dim Prot$, n$, H%, p%, w$, zl%, z$

' concatenate the error protocoll
BuildProtocol Prot$

Select Case Index
'--------------------------
Case 0  ' print the protocol
'--------------------------
    Prot$ = Prot$ + vbCrLf
    Do
        ' fetch next line
        p% = InStr(Prot$, vbCrLf)
        If p% = 0 Then Exit Do
        n$ = Left$(Prot$, p% - 1) + " "
        Prot$ = Mid$(Prot$, p% + 2)
        ' print the line word by word with a word wrap in column 70
        zl% = 0
        z$ = ""
        Do
            p% = InStr(n$, " ")
            If p% = 0 Then Exit Do
            w$ = Left$(n$, p%)
            n$ = Mid$(n$, p% + 1)
            zl% = zl% + p%
            If zl% > 73 Then
                If Len(z$) Then Printer.Print z$: z$ = Space$(20)
                zl% = p%
            End If
            z$ = z$ + w$
        Loop
        If Len(z$) Then Printer.Print z$
    Loop
    Printer.EndDoc
'--------------------------
Case 1  ' save the protocol
'--------------------------
    n$ = App.Path
    If Right$(n$, 1) <> "\" Then n$ = n$ + "\"
    n$ = n$ + App.EXEName + ".ERR"
    If Len(Dir$(n$)) Then
        If MsgBox("Overwrite exiting file '" + n$ + "' ?", vbYesNo) = vbNo Then Exit Sub
    End If
    H% = FreeFile
    Open n$ For Output As #H%
    Print #H%, Prot$
    Close H%
    MsgBox "The Errorprotocoll was written to '" + n$ + "'!"
'--------------------------
Case 2  ' send the protocoll by email
'--------------------------
    z$ = "Your Standard-Email-program will now be started. The error description as well as the protocol"
    z$ = z$ + vbCrLf + "were copied to the clipboard for your convenience."
    z$ = z$ + vbCrLf + ""
    z$ = z$ + vbCrLf + "Please insert the contents of the clipboard into your email,"
    z$ = z$ + vbCrLf + "by using the keys [Ctrl] and [V] at the same time. Alternativly, you may insert"
    z$ = z$ + vbCrLf + "the contens into your Email by using the command 'Insert' from the menu 'Edit'."
    
    MsgBox z$
    Clipboard.Clear
    Clipboard.SetText Prot$, 1
    Call ShellExecute(hwnd, "Open", "mailto:" + Trim$(ErrPtnr_EMail$), "", "", 1)
'--------------------------
Case 3  ' open a url
'--------------------------
    Clipboard.Clear
    Clipboard.SetText Prot$, 1
    Call ShellExecute(hwnd, "Open", ErrPtnr_WWW$, "", "", 1)
'--------------------------
End Select

End Sub

Public Sub SetProgInfo(Text$)
'.------------------------------------------------------------------------------
'  Define the contents of the program information text to be diplayed in the
'  error dialog. For example: "Program: YourApp v.1.99 / Serial no: 12345"
'  Syntax:
'       ErrPtnr.SetProgInfo "Program: YourApp v.1.99 / Serial no: 12345"
'.------------------------------------------------------------------------------
ProgInfo$ = Text$
End Sub

Private Sub WriteLogfile()
'.------------------------------------------------------------------------------
'   append the error information to an existing log file or create a new one
'.------------------------------------------------------------------------------

Dim Prot$, n$, H%

' concatenate the error protocoll text
BuildProtocol Prot$

' append it to the log file
n$ = App.Path
If Right$(n$, 1) <> "\" Then n$ = n$ + "\"
n$ = n$ + App.EXEName + ".EPT"
H% = FreeFile
Open n$ For Append As #H%
Print #H%, Prot$
Close H%

End Sub

' extracts a VB string from a buffer containing a null terminated string
Public Function LPSTRToVBString$(ByVal s$)
Dim nullpos&
nullpos& = InStr(s$, Chr$(0))
If nullpos > 0 Then
    LPSTRToVBString = Left$(s$, nullpos - 1)
Else
    LPSTRToVBString = ""
End If
End Function

Public Sub CallStack(a$)
'.------------------------------------------------------------------------------
'.  Function :  push a procedure to the call stack
'.------------------------------------------------------------------------------
a$ = Time$ + " " + a$
With CallStackCollection
    If .Count Then .Add a$, , 1 Else .Add a$
    If .Count > CallStackLength% Then .Remove .Count
End With
'--------------------------
' If you comment out the following line the call stack will be displayed
' in the VB IDE's debug window.
'Debug.Print a$
'--------------------------
End Sub

Public Sub CallStackParam(ParamName$, v As Variant)
'.------------------------------------------------------------------------------
'.  Function :  push a procedure's parameter to the call stack
'.------------------------------------------------------------------------------
Dim a$
Do
    If IsArray(v) Then a$ = "[ARRAY]": Exit Do
    If IsNull(v) Then a$ = "[NULL]": Exit Do
    If IsEmpty(v) Then a$ = "[EMPTY]": Exit Do
    If IsObject(v) Then
        If v Is Nothing Then
            a$ = "[NOTHING]"
        Else
            a$ = "[OBJECT:" + TypeName(v) + "]"
        End If
        Exit Do
    End If
    a$ = CStr(v): Exit Do
Loop
a$ = vbTab + ParamName$ + ":" + vbTab + a$
With CallStackCollection
    .Add a$, , , 1
    If .Count > CallStackLength% Then .Remove .Count
End With
'--------------------------
' If you comment out the following line the call stack will be displayed
' in the VB IDE's debug window.
'Debug.Print a$
'--------------------------
End Sub

Private Sub CallStack2Listbox()
'.------------------------------------------------------------------------------
'.  Sub :  fill the listbox from the CallStack-Array
'.------------------------------------------------------------------------------
Dim a As Variant
lstCallStack.Clear
For Each a In CallStackCollection
    lstCallStack.AddItem CStr(a)
Next
End Sub

Private Sub timTimeout_Timer()

    cmdContinue_Click (4)

End Sub
