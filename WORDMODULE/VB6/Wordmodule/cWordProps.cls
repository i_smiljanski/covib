VERSION 1.0 CLASS
BEGIN
  MultiUse = -1  'True
  Persistable = 0  'NotPersistable
  DataBindingBehavior = 0  'vbNone
  DataSourceBehavior  = 0  'vbNone
  MTSTransactionMode  = 0  'NotAnMTSObject
END
Attribute VB_Name = "cWordProps"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = True
Attribute VB_PredeclaredId = False
Attribute VB_Exposed = False
Attribute VB_Ext_KEY = "SavedWithClassBuilder6" ,"Yes"
Attribute VB_Ext_KEY = "Top_Level" ,"Yes"
'''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
' Klasse            : cWordProps
' Beschreibung      : Nimmt diverse Eigenschaften der Word-Dokumente auf.
' Autor             : ms
' Datum             : 20.04.2004
' �nderungshistorie :
' Autor  Datum       Grund            Kommentar
' -----  ----------  ---------------  ---------------------------------------
'
'''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
Option Explicit

'Private oWord As Word.Application
Private oWord As Object

Private aStructBM()
Private aDocInfoText()
Private aDocInfoValue()

Private mvariStructBM As Integer 'j
Private mvariDocInfo As Integer 'z

Private mvarsWordVersion As String

Private mvarbPaginationSetting As Boolean
Private mvarbConversionSetting As Boolean

Private mvarsSourceDocName As String
Private mvarsTargetDocName As String

Public Property Get sWordVersion() As String
    sWordVersion = mvarsWordVersion
End Property

Public Property Let sTargetDocName(ByVal vData As String)
    mvarsTargetDocName = vData
End Property

Public Property Get sTargetDocName() As String
    sTargetDocName = mvarsTargetDocName
End Property

Public Property Let sSourceDocName(ByVal vData As String)
    mvarsSourceDocName = vData
End Property

Public Property Get sSourceDocName() As String
    sSourceDocName = mvarsSourceDocName
End Property

Public Property Let bConversionSetting(ByVal vData As Boolean)
    mvarbConversionSetting = vData
End Property

Public Property Get bConversionSetting() As Boolean
    bConversionSetting = mvarbConversionSetting
End Property

Public Property Let bPaginationSetting(ByVal vData As Boolean)
    mvarbPaginationSetting = vData
End Property

Public Property Get bPaginationSetting() As Boolean
    bPaginationSetting = mvarbPaginationSetting
End Property

Public Property Get iDocInfo() As Integer
    iDocInfo = mvariDocInfo
End Property

Public Property Get iStructBM() As Integer
    iStructBM = mvariStructBM
End Property

Function GetStructBM(iNum As Integer) As Variant
    GetStructBM = aStructBM(iNum)
End Function

Function GetDocInfoText(iNum As Integer) As Variant
    GetDocInfoText = aDocInfoText(iNum)
End Function

Function GetDocInfoValue(iNum As Integer) As Variant
    GetDocInfoValue = aDocInfoValue(iNum)
End Function

'''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
' Funktion            : SetStructBM
' Beschreibung        : Weisst der StructBM einen neuen Eintrag zu und erh�ht den Z�hler
' Parameter           : vValue - Zu speichernder Wert
' wird aufgerufen von : modWord.GetUserDefinedProperties
' R�ckgabewert        : -
' �nderungshistorie   :
' Autor  Datum       Grund            Kommentar
' -----  ----------  ---------------  ---------------------------------------
'
'''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
Function SetStructBM(vValue As Variant)
    
    If mvariStructBM Mod 10 = 0 Then
        ReDim Preserve aStructBM(mvariStructBM + 10)
    End If
    
    mvariStructBM = mvariStructBM + 1
    
    aStructBM(mvariStructBM) = vValue

End Function

'''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
' Funktion            : SetDocInfo
' Beschreibung        : Weisst der DocInfo einen neuen Eintrag (bestehnd aus Text und Wert) zu
'                       und erh�ht den Z�hler
' Parameter           : vText  - zu speichernder Text
'                       vValue - Zu speichernder Wert
' wird aufgerufen von : modWord.GetUserDefinedProperties
' R�ckgabewert        : -
' �nderungshistorie   :
' Autor  Datum       Grund            Kommentar
' -----  ----------  ---------------  ---------------------------------------
'
'''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
Function SetDocInfo(vText As Variant, vValue As Variant)
    
    If mvariDocInfo Mod 10 = 0 Then
        ReDim Preserve aDocInfoText(mvariDocInfo + 10)
        ReDim Preserve aDocInfoValue(mvariDocInfo + 10)
    End If
        
    mvariDocInfo = mvariDocInfo + 1
        
    aDocInfoText(mvariDocInfo) = vText
    aDocInfoValue(mvariDocInfo) = vValue
    
End Function

'''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
' Sub-Prozedur        : InitWordProps
' Beschreibung        : Initalisiert die Instanz des Objekts mit Defaultwerten
'                       quasi der Konstruktor, wenns den in VB geben w�rde. =)
' Parameter           : oWrd - das Word-Objekt
' wird aufgerufen von : modWord.ConvertToWord
' �nderungshistorie   : -
' Autor  Datum       Grund            Kommentar
' -----  ----------  ---------------  ---------------------------------------
'
'''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
Sub InitWordProps(oWrd As Word.Application)
      
    Set oWord = oWrd

    ' Defaultwerte festlegen
    mvariStructBM = 0
    mvariDocInfo = 0

    ' Version feststellen
    mvarsWordVersion = oWord.Application.Version
    
    ' Word Application Option Eigenschaften merken
    mvarbPaginationSetting = oWord.Options.Pagination
    mvarbConversionSetting = oWord.Options.ConfirmConversions

End Sub

'''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
' Sub-Prozedur        : ReleaseWordProps
' Beschreibung        : Entfernt lediglich den Verweis auf das Word-Objekt.
'                       Quasi der Destruktor
' Parameter           : -
' wird aufgerufen von : modWord.ConvertToWord
' �nderungshistorie   : -
' Autor  Datum       Grund            Kommentar
' -----  ----------  ---------------  ---------------------------------------
'
'''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
Sub ReleaseWordProps()
    
    Set oWord = Nothing

End Sub
