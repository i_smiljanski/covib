Attribute VB_Name = "modGlobal"
'''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
' Modul             : modGlobal
' Beschreibung      : Alles globale (Objekte, Variablen, Konstanten) findet sich hier.
' Autor             : ms
' Datum             : 19.03.2004 ff.
' Änderungshistorie :
' Autor  Datum       Grund            Kommentar
' -----  ----------  ---------------  ---------------------------------------
'
'''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''

Option Explicit

Global Const cCompilationDate = "20160915"
Global Const bHARDDEBUG = False

Global Const sBM_UEBER = "*CAP*"            'sTM
'Global Const sBM_TEXTMARKE = "*Textmarke*" 'sTM2
Global Const sBM_END = "*END*"              'sTM3
Global Const sBM_BEGIN = "*BGN*"            'sTM4
Global Const sBM_STRUCT = "S_*"
Global Const sBM_GRAPHIC = "G_*"
Global Const sBM_TABLE = "T_*"
Global Const sBM_REF = "REF_*"

Global Const iMAXPROPERTIES As Integer = 40
Global Const sTABLENAME = "TPAS"

Global bDEBUG As Boolean

Global goMD5 As cMD5
Global goOptions As cOptions
Global goLogging As cLogging

Global Const sERRORTEXT = "An Error has occured !" & vbCr & "Please send the following files to 'up to data':" & vbCr
Global Const sERRORTITLE = "Wordmodul Error Message"
Global Const sPROGRESSTEXT = "Current progress: "

'Remove Close Icon
Global Const MF_BYPOSITION = &H400
Declare Function GetSystemMenu Lib "User32.dll" (ByVal hwnd As Long, ByVal bRevert As Long) As Long
Declare Function RemoveMenu Lib "User32.dll" (ByVal hMenu As Long, ByVal nPosition As Long, ByVal wFlags As Long) As Long

Declare Sub ExitProcess Lib "kernel32" (ByVal Errorlevel As Long)

Declare Function GetTempPath Lib "kernel32" Alias "GetTempPathA" (ByVal nBufferLength As Long, ByVal lpBuffer As String) As Long
Declare Function GetTempFileName Lib "kernel32" Alias "GetTempFileNameA" (ByVal lpszPath As String, ByVal lpPrefixString As String, ByVal wUnique As Long, ByVal lpTempFileName As String) As Long
Global Const iLENGTH = 255


'Templates: ;-)

'''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
' Sub-Prozedur        :
' Beschreibung        :
' Parameter           :
' wird aufgerufen von :
' Änderungshistorie   :
' Autor  Datum       Grund            Kommentar
' -----  ----------  ---------------  ---------------------------------------
'
'''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''

'''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
' Funktion            :
' Beschreibung        :
' Parameter           :
' wird aufgerufen von :
' Rückgabewert        :
' Änderungshistorie   :
' Autor  Datum       Grund            Kommentar
' -----  ----------  ---------------  ---------------------------------------
'
'''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''

'''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
' Modul             :
' Beschreibung      :
' Autor             :
' Datum             :
' Änderungshistorie :
' Autor  Datum       Grund            Kommentar
' -----  ----------  ---------------  ---------------------------------------
'
'''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''

'''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
' Klasse            :
' Beschreibung      :
' Autor             :
' Datum             :
' Änderungshistorie :
' Autor  Datum       Grund            Kommentar
' -----  ----------  ---------------  ---------------------------------------
'
'''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''

'''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
' Form              :
' Beschreibung      :
' Autor             :
' Datum             :
' Änderungshistorie :
' Autor  Datum       Grund            Kommentar
' -----  ----------  ---------------  ---------------------------------------
'
'''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''

'''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
' Beschreibung      :
' Änderungshistorie :
' Autor  Datum       Grund            Kommentar
' -----  ----------  ---------------  ---------------------------------------
'
'''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''


