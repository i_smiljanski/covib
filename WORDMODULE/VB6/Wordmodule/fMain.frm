VERSION 5.00
Begin VB.Form fMain 
   BorderStyle     =   1  'Fest Einfach
   Caption         =   "Caption defined at runtime"
   ClientHeight    =   960
   ClientLeft      =   -15
   ClientTop       =   270
   ClientWidth     =   6495
   LinkTopic       =   "Form1"
   MaxButton       =   0   'False
   MinButton       =   0   'False
   ScaleHeight     =   960
   ScaleWidth      =   6495
   StartUpPosition =   3  'Windows-Standard
   Begin VB.PictureBox pic_isrLogo 
      BorderStyle     =   0  'Kein
      Height          =   1095
      Left            =   0
      Picture         =   "fMain.frx":0000
      ScaleHeight     =   1095
      ScaleWidth      =   1095
      TabIndex        =   1
      Top             =   0
      Width           =   1095
   End
   Begin VB.TextBox txtMsg 
      BackColor       =   &H8000000F&
      BorderStyle     =   0  'Kein
      Enabled         =   0   'False
      Height          =   285
      Left            =   1200
      Locked          =   -1  'True
      TabIndex        =   0
      Text            =   "Start processing..."
      Top             =   160
      Width           =   5175
   End
   Begin VB.Label lblProgress 
      Caption         =   "Current progress: -"
      Height          =   255
      Left            =   1200
      TabIndex        =   2
      Top             =   560
      Width           =   5175
   End
End
Attribute VB_Name = "fMain"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
'''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
' Form              : fMain
' Beschreibung      : Zeigt einen Text sowie einen Fortschrittsbalken an.
' Autor             : ms
' Datum             : 22.03.2004
' Änderungshistorie :
' Autor  Datum       Grund            Kommentar
' -----  ----------  ---------------  ---------------------------------------
'
'''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
Option Explicit


'''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
' Sub-Prozedur        : Form_Load
' Beschreibung        : Füllt die Caption des Forms (d.h. den Windowtitel) mit
'                       Informationen zur Applikation (Produktname, Version, Datum)
' Parameter           : -
' wird aufgerufen von : modMain.Main
' Änderungshistorie   :
' Autor  Datum       Grund            Kommentar
' -----  ----------  ---------------  ---------------------------------------
'
'''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
Private Sub Form_Load()

    With App
        Me.Caption = .ProductName & " " & .Major & "." & .Minor & "." & .Revision & " (" & cCompilationDate
        If bHARDDEBUG = True Then Me.Caption = Me.Caption & " debug"
        Me.Caption = Me.Caption & ")"
    End With

End Sub


'''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
' Sub-Prozedur        : UpdateMessage
' Beschreibung        : Schreibt den mitgegebenen Text in txtMsg
' Parameter           : sMessage - Textnachricht
' wird aufgerufen von : verschieden
' Änderungshistorie   :
' Autor  Datum       Grund            Kommentar
' -----  ----------  ---------------  ---------------------------------------
'
'''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
Public Sub UpdateMessage(sMessage As String)

    txtMsg = sMessage
    Me.Refresh

End Sub

'''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
' Sub-Prozedur        : UpdateProgressbar
' Beschreibung        : Ändert den Wert des Fortschrittsbalken anhand dem mitgegebenen Wert
' Parameter           : iPercent - Integer-Wert zwischen 1 und 100
' wird aufgerufen von : verschieden
' Änderungshistorie   :
' Autor  Datum       Grund            Kommentar
' -----  ----------  ---------------  ---------------------------------------
'
'''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
Public Sub UpdateProgressbar(iPercent As Integer)

    If iPercent = 0 Then
        lblProgress.Caption = sPROGRESSTEXT & "0%"
    ElseIf iPercent < 0 Then
        lblProgress.Caption = sPROGRESSTEXT & "0%"
    ElseIf iPercent > 100 Then
        lblProgress.Caption = sPROGRESSTEXT & "100%"
    Else
        lblProgress.Caption = sPROGRESSTEXT & iPercent & "%"
    End If
    Me.Refresh

End Sub

'''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
' Sub-Prozedur        : RemoveCloseIcon
' Beschreibung        : Entfernt das Schliessen-Symbol vom Fenster
' Parameter           : -
' wird aufgerufen von : frmMain.Main
' Änderungshistorie   :
' Autor  Datum       Grund            Kommentar
' -----  ----------  ---------------  ---------------------------------------
'
'''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
Public Sub RemoveCloseIcon()
    
    'RemoveMenu und GetSystemMenu sind Systemaufrufe (user32.dll, definiert in modGlobal)
    RemoveMenu GetSystemMenu(hwnd, 0), 6, MF_BYPOSITION
    Me.Refresh

End Sub

