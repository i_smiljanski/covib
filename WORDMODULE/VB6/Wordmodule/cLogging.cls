VERSION 1.0 CLASS
BEGIN
  MultiUse = -1  'True
  Persistable = 0  'NotPersistable
  DataBindingBehavior = 0  'vbNone
  DataSourceBehavior  = 0  'vbNone
  MTSTransactionMode  = 0  'NotAnMTSObject
END
Attribute VB_Name = "cLogging"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = True
Attribute VB_PredeclaredId = False
Attribute VB_Exposed = False
Attribute VB_Ext_KEY = "SavedWithClassBuilder6" ,"Yes"
Attribute VB_Ext_KEY = "Top_Level" ,"Yes"
'''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
' Klasse            : cLogging
' Beschreibung      : Stellt den Logging Mechanismus zur Verf�gung
' Autor             : ms
' Datum             : 19.03.2004
' �nderungshistorie :
' Autor  Datum       Grund            Kommentar
' -----  ----------  ---------------  ---------------------------------------
'
'''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
Option Explicit

Private iFileNumber As Integer
Private bFileOpen   As Boolean

Private mvarLogFileName As String

Public Property Let LogFileName(ByVal vData As String)
    If bFileOpen = False Then
        mvarLogFileName = vData
    End If
End Property

Public Property Get LogFileName() As String
    LogFileName = mvarLogFileName
End Property

'''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
' Funktion            : OpenLog
' Beschreibung        : �ffnet das Logging-File und schreibt einen Header.
'                       An den �bergebenen Filenamen wird ".log" angeh�ngt.
' Parameter           : -
' wird aufgerufen von : Main
' R�ckgabewert        : true - wenn File korrekt ge�ffnet wurde.
'                       false - wenn beim �ffnen ein Fehler aufgetreten ist.
' �nderungshistorie   :
' Autor  Datum       Grund            Kommentar
' -----  ----------  ---------------  ---------------------------------------
'
'''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
Public Function OpenLog(Optional sLogFileName As String) As Boolean

    On Error GoTo OpenLog_Error

    If Not IsMissing(sLogFileName) And bFileOpen = False Then
        mvarLogFileName = Trim(sLogFileName)
    End If

    'Pr�fen, ob ein Filename bereits zugewiesen wurde und nicht bereits ein Log offen ist
    If mvarLogFileName = "" Or bFileOpen = True Then
        OpenLog = False
        Exit Function
    End If

    'Dateinamen f�r das Logging ermitteln
    If Right(mvarLogFileName, 1) <> Chr(34) Then
        mvarLogFileName = mvarLogFileName & ".log"
    Else
        'Dateiname ist in " eingeschlossen
        mvarLogFileName = Mid(mvarLogFileName, 2, Len(mvarLogFileName) - 2) & ".log"
    End If

    'n�chste freie Dateinummer ermitteln
    iFileNumber = FreeFile
    
    'LogDatei �ffnen
    Open mvarLogFileName For Output As #iFileNumber
    
    bFileOpen = True
    
    'Logbeginn
    WriteLog ("--[ " & Date & " ]-[ " & Time & " ]--------------------------------------- --- -- -")

    OpenLog = True
    
    Exit Function

OpenLog_Error:
    OpenLog = False
    
End Function

'''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
' Funktion            : CloseLog
' Beschreibung        : Schreibt einen Footer und schliesst das Logging-File
' Parameter           : -
' wird aufgerufen von : Main
' R�ckgabewert        : true - wenn File korrekt geschlossen wurde.
'                       false - wenn beim Schliessen ein Fehler aufgetreten ist.
' �nderungshistorie   :
' Autor  Datum       Grund            Kommentar
' -----  ----------  ---------------  ---------------------------------------
'
'''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
Public Function CloseLog() As Boolean

    On Error GoTo CloseLog_Error

    'Pr�fen, ob �berhaupt ein Log offen ist
    If bFileOpen = False Then
        CloseLog = False
        Exit Function
    End If

    WriteLog ("==[ " & Date & " ]=[ " & Time & " ]======================================= === == =")

    Close #iFileNumber
    bFileOpen = False

    CloseLog = True
    
    Exit Function

CloseLog_Error:
    CloseLog = False

End Function

'''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
' Funktion            : WriteLog
' Beschreibung        : Schreibt einen String in das Logfile
' Parameter           : sMsg (String) - der zu schreibende String
' wird aufgerufen von : unterschiedlichen Funktionen
' R�ckgabewert        : true - wenn der String korrekt geschrieben wurde.
'                       false - wenn beim Schreiben ein Fehler aufgetreten ist.
' �nderungshistorie   :
' Autor  Datum       Grund            Kommentar
' -----  ----------  ---------------  ---------------------------------------
'
'''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
Public Function WriteLog(sMsg As String) As Boolean

    On Error GoTo WriteLog_Error

    'Pr�fen, ob �berhaupt ein Log offen ist
    If bFileOpen = False Then
        WriteLog = False
        Exit Function
    End If

    Print #iFileNumber, Time & " | " & sMsg

    'Harddebug wird ben�tigt, um Speicherverletzungen etc. auf die Spur zu kommen,
    'da ansonsten die Log-Datei nicht vollst�ndig geschrieben wird.
    If bHARDDEBUG = True Then
        Close #iFileNumber
        Open mvarLogFileName For Append As #iFileNumber
    End If

    WriteLog = True
    
    Exit Function

WriteLog_Error:
    WriteLog = False

End Function

'''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
' Funktion            : DeleteLog
' Beschreibung        : L�scht das genannte Logfile. Ist es noch offen, wird es geschlossen.
' Parameter           : -
' wird aufgerufen von : unterschiedlichen Funktionen
' R�ckgabewert        : true - wenn File korrekt gel�scht wurde.
'                       false - wenn beim L�schen ein Fehler aufgetreten ist.
' �nderungshistorie   :
' Autor  Datum       Grund            Kommentar
' -----  ----------  ---------------  ---------------------------------------
'
'''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
Public Function DeleteLog() As Boolean

    On Error GoTo DeleteLog_Error

    'Pr�fen, ob ein Log offen ist
    If bFileOpen = True Then
        CloseLog
    End If

    Kill mvarLogFileName

    DeleteLog = True

    Exit Function
    
DeleteLog_Error:
    DeleteLog = False

End Function
