Attribute VB_Name = "modMain"
'''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
' Modul             : modMain
' Beschreibung      : Hier startet das Programm.
' Autor             : ms, basierend auf sourcen von as.
' Datum             : 19.03.2004 ff.
' �nderungshistorie :
' Autor  Datum       Grund            Kommentar
' -----  ----------  ---------------  ---------------------------------------
'
'''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
Option Explicit

' Ben�tigt f�r das abrufen der Windows-Versionsinformationen:
' Werte f�r dwPlatformId von OSVERSIONINFO
Private Const VER_PLATFORM_WIN32s        As Long = 0&
Private Const VER_PLATFORM_WIN32_WINDOWS As Long = 1&
Private Const VER_PLATFORM_WIN32_NT      As Long = 2&
Private Type OSVERSIONINFO
  dwOSVersionInfoSize As Long   ' Gr��e der Struktur
  dwMajorVersion      As Long   ' Major-Versionsnummer
  dwMinorVersion      As Long   ' Minor-Versionsnummer
  dwBuildNumber       As Long   ' Build-Versionsnummer
  dwPlatformId        As Long   ' Plattform-Kennzeichner
  szCSDVersion        As String * 128 ' Service Pack (Klartext)
End Type
Private Declare Function GetVersionEx Lib "kernel32" Alias "GetVersionExA" (ByRef VersionInformation As OSVERSIONINFO) As Long

'''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
' Sub-Prozedur        : Main
' Beschreibung        : Hier startet das Programm.
' Parameter           : -
' wird aufgerufen von : Windows ;-)
' �nderungshistorie   :
' Autor  Datum       Grund            Kommentar
' -----  ----------  ---------------  ---------------------------------------
'
'''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
Sub Main()

    Dim sCommandline As String

    ' Feststellen, ob wir in der IDE oder Standalone laufen (-> bDEBUG wird gesetzt)
    IsIDE

    'Globale Objekte zuweisen
    Set goMD5 = New cMD5
    Set goOptions = New cOptions
    Set goLogging = New cLogging
  
    sCommandline = Command()
   
    'Befehlszeilenoptionen auswerten
    If goOptions.ParseArgs(sCommandline) = False Then
        ExitProc -1
        End
    End If

    'Hauptarbeitsschritte
    'goLogging.OpenLog goOptions.sFile_DocTarget  'Optionen -a und -z
    goLogging.OpenLog goOptions.sConfigPath & "\" & ExtractFilename(goOptions.sFile_DocTarget)  'Optionen -a und -z

    'Informationen zu den Befehlszeilenoptionen ins Log schreiben
    goLogging.WriteLog "Commandline Options: " & Command()
    goOptions.WriteOptionsToLog
    goLogging.WriteLog "  App.path        = " & App.Path


    If goOptions.sOption = "view" Or _
       goOptions.sOption = "lock" Or _
       goOptions.sOption = "lockview" Or _
       goOptions.sOption = "finalize" Or _
       goOptions.sOption = "refereshproperties" Or _
       goOptions.sOption = "checkin" Then
        'Optionen view/lock bereits hier abhandeln
        
        Call ViewDoc
    
    ElseIf goOptions.sOption = "compare" Then
        
        Call CompareDocument
    
    Else
        
        'Form mit Progressbar anzeigen
        fMain.Show
        fMain.RemoveCloseIcon
        
        'Nach Word konvertieren
        Call ConvertToWord
    
    End If
    
    'Logging beenden
    goLogging.CloseLog
    
    'Aufr�umen
    Set goLogging = Nothing
    Set goOptions = Nothing
    Set goMD5 = Nothing
    
    'Programmende
    ExitProc 0
    
End Sub

'''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
' Sub-Prozedur        : ViewDoc
' Beschreibung        : Bearbeitet die Optionen lock/view
'                       zum anzeigen oder gesch�tzten Anzeigen oder gesch�tztem Speichern
'                       der Word-Dokumente.
' Parameter           : -
' wird aufgerufen von : Main
' �nderungshistorie   :
' Autor  Datum       Grund            Kommentar
' -----  ----------  ---------------  ---------------------------------------
'
'''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
Sub ViewDoc()

    Dim oWord           As Word.Application
    Dim sDocumentFile   As String
    Dim sPassword       As String
    Dim iSectionCount   As Integer
    Dim iSectionCounter As Integer
    Dim sDocName        As String
    Dim iFileFormat     As Integer
    
    goLogging.WriteLog "Instanciating Word Object..."
    Set oWord = CreateObject("Word.Application")
        
    'Sollte schon visible = false stehen, aber zur sicherheit...
    goLogging.WriteLog "Hide Word..."
    oWord.Visible = False
    
    goLogging.WriteLog "Handling Option: " & goOptions.sOption
    If goOptions.sOption = "view" Then 'Bearbeiten->Dokument auschecken
        
        goLogging.WriteLog "Open File " & goOptions.sFile_DocTarget
        oWord.Application.Documents.Open FileName:=goOptions.sFile_DocTarget
        
        goLogging.WriteLog "Fetch the name of the document..."
        sDocName = oWord.ActiveDocument.Name

        'Ausdr�cklich in wdPrintView wechseln (f�r Word 2013)
        oWord.ActiveDocument.ActiveWindow.View = wdPrintView
        
        'Dokument komplett entsch�tzen falls notwendig
        goLogging.WriteLog "Word Protection Type: " & oWord.ActiveDocument.ProtectionType
        If oWord.ActiveDocument.ProtectionType <> wdNoProtection Then
            
            goLogging.WriteLog "Case true"
            
            goLogging.WriteLog "Unprotect the Document"
            If oWord.Documents(sDocName).ProtectionType <> wdNoProtection Then
                oWord.ActiveDocument.Unprotect Password:=goOptions.sPassword_source
            End If
        
            ' Dokumenteigenschaften in das Zieldokument einf�gen
            goLogging.WriteLog "Writing DocProperties..."
            Call WriteDocProperties(oWord.ActiveDocument.Name, oWord, False, True)
        
            ' Dokument aktualisieren
            goLogging.WriteLog "UpdateDoc..."
            Call UpdateDoc(oWord.ActiveDocument.Name, oWord)
                
            '...und sch�tzen
            goLogging.WriteLog "Protecting the Document..."
            If goOptions.sUseProtection = "true" Or goOptions.sUseProtection = "not_available" Then
                oWord.ActiveDocument.Protect Password:=goOptions.sPassword_target, NoReset:=False, Type:=wdAllowOnlyFormFields
            End If
        
        Else
            
            goLogging.WriteLog "Case false"
        
            ' Dokumenteigenschaften in das Zieldokument einf�gen
            goLogging.WriteLog "Writing DocProperties..."
            Call WriteDocProperties(oWord.ActiveDocument.Name, oWord, False, True)
        
            ' Dokument aktualisieren
            goLogging.WriteLog "UpdateDoc..."
            Call UpdateDoc(oWord.ActiveDocument.Name, oWord)
        
            '...und sch�tzen
            If goOptions.sUseProtection = "true" Then
                goLogging.WriteLog "Protecting the Document..."
                oWord.ActiveDocument.Protect Password:=goOptions.sPassword_target, NoReset:=False, Type:=wdAllowOnlyFormFields
            End If
        
        End If
        
        goLogging.WriteLog "Saving the Document..."
        'sicherstellen, dass das Dokument wirklich gespeichert wird
        oWord.Documents(goOptions.sFile_DocTarget).Saved = False
        oWord.Documents(goOptions.sFile_DocTarget).Save
        
        goLogging.WriteLog "Quit Word..."
        oWord.Quit wdDoNotSaveChanges
    
    ElseIf Left(goOptions.sOption, 4) = "lock" Then
        
        'Dokument �ffnen und Namen merken
        goLogging.WriteLog "Opening the document..."
        If goOptions.sOption = "lockview" Then
            oWord.Application.Documents.Open FileName:=goOptions.sFile_DocTarget
        Else
            oWord.Application.Documents.Open FileName:=goOptions.sTemplate
        End If
        
        goLogging.WriteLog "Fetch the name of the document..."
        sDocName = oWord.ActiveDocument.Name
            
        'Ausdr�cklich in wdPrintView wechseln (f�r Word 2013)
        oWord.Documents(sDocName).ActiveWindow.View = wdPrintView
            
        If goOptions.bUnprotected = False Then
            'Dokument komplett entsch�tzen...
            goLogging.WriteLog "Unprotecting the document..."
            If oWord.Documents(sDocName).ProtectionType <> wdNoProtection Then
                oWord.Documents(sDocName).Unprotect Password:=goOptions.sPassword_source
            End If
        End If
   
        If goOptions.bUnprotected = False Or goOptions.sOption = "lockview" Then
            'Dann alle Sections f�r das Protect vorbereiten...
            goLogging.WriteLog "Preparing all sections for protection..."
            iSectionCount = oWord.Documents(sDocName).Sections.Count
            For iSectionCounter = 1 To iSectionCount
                oWord.Documents(sDocName).Sections(iSectionCounter).ProtectedForForms = True
            Next iSectionCounter
        End If

        ' Dokument aktualisieren
        goLogging.WriteLog "UpdateDoc..."
        Call UpdateDoc(oWord.ActiveDocument.Name, oWord)

        If goOptions.bUnprotected = False Or goOptions.sOption = "lockview" Then
            '...und sch�tzen
            goLogging.WriteLog "Protecting the Document..."
            oWord.Documents(sDocName).Protect Password:=goOptions.sPassword_target, NoReset:=False, Type:=wdAllowOnlyFormFields
        End If
        
        'Feststellen, ob doc oder docx gespeichert werden soll
        If LCase(Right(Trim(goOptions.sFile_DocTarget), 5)) = ".docx" Then
            iFileFormat = wdFormatXMLDocument
        Else
            iFileFormat = wdFormatDocument
        End If
        
        'Schliesslich das Dokument in der neuen Form speichern
        goLogging.WriteLog "Saving the Document..."
        oWord.Documents(sDocName).SaveAs FileName:=goOptions.sFile_DocTarget, FileFormat:=iFileFormat, _
                                        LockComments:=False, Password:="", AddToRecentFiles:=True, _
                                        WritePassword:="", ReadOnlyRecommended:=False, EmbedTrueTypeFonts:=False, _
                                        SaveNativePictureFormat:=False, SaveFormsData:=False, SaveAsAOCELetter:=False
           
        goLogging.WriteLog "Quit Word..."
        oWord.Quit wdDoNotSaveChanges
    
    
    ElseIf goOptions.sOption = "finalize" Or goOptions.sOption = "refereshproperties" Then
            
        goLogging.WriteLog "Opening the document..."
        oWord.Application.Documents.Open FileName:=goOptions.sTemplate
        
        goLogging.WriteLog "Fetch the name of the document..."
        sDocName = oWord.ActiveDocument.Name
        
        'Ausdr�cklich in wdPrintView wechseln (f�r Word 2013)
        oWord.Documents(sDocName).ActiveWindow.View = wdPrintView

        ' Dokument entsch�tzen
        If goOptions.bUnprotected = False Then
            goLogging.WriteLog "Unprotecting the document..."
            If oWord.Documents(sDocName).ProtectionType <> wdNoProtection Then
                oWord.Documents(sDocName).Unprotect Password:=goOptions.sPassword_source
            End If
        End If
        
        ' Dokumenteigenschaften in das Zieldokument einf�gen
        goLogging.WriteLog "Writing DocProperties..."
        Call WriteDocProperties(sDocName, oWord, False, False)
        
        ' Dokument aktualisieren
        goLogging.WriteLog "UpdateDoc..."
        Call UpdateDoc(oWord.ActiveDocument.Name, oWord)
        
        'Dokument sch�tzen
        If goOptions.bUnprotected = False Then
            goLogging.WriteLog "Protecting the Document..."
            oWord.Documents(sDocName).Protect Password:=goOptions.sPassword_target, NoReset:=False, Type:=wdAllowOnlyFormFields
        End If
        
        'Feststellen, ob doc oder docx gespeichert werden soll
        If LCase(Right(Trim(goOptions.sFile_DocTarget), 5)) = ".docx" Then
            iFileFormat = wdFormatXMLDocument
        Else
            iFileFormat = wdFormatDocument
        End If
      
        goLogging.WriteLog "Saving the Document..."
        oWord.Documents(sDocName).SaveAs FileName:=goOptions.sFile_DocTarget, FileFormat:=iFileFormat, _
                                        LockComments:=False, Password:="", AddToRecentFiles:=True, _
                                        WritePassword:="", ReadOnlyRecommended:=False, EmbedTrueTypeFonts:=False, _
                                        SaveNativePictureFormat:=False, SaveFormsData:=False, SaveAsAOCELetter:=False
            
        goLogging.WriteLog "Quit Word..."
        oWord.Quit wdDoNotSaveChanges
    
    ElseIf goOptions.sOption = "checkin" Then
    
        goLogging.WriteLog "Opening the document..."
        oWord.Application.Documents.Open FileName:=goOptions.sFile_DocTarget
        
        'Ausdr�cklich in wdPrintView wechseln (f�r Word 2013)
        oWord.ActiveDocument.ActiveWindow.View = wdPrintView

        ' Dokument aktualisieren
        goLogging.WriteLog "UpdateDoc..."
        Call UpdateDoc(oWord.ActiveDocument.Name, oWord)
        
        If goOptions.sUseProtection = "true" And oWord.ActiveDocument.ProtectionType = wdNoProtection Then
            '...und sch�tzen
            goLogging.WriteLog "Protecting the Document..."
            oWord.ActiveDocument.Protect Password:=goOptions.sPassword_target, NoReset:=False, Type:=wdAllowOnlyFormFields
        End If
        
        goLogging.WriteLog "Saving the Document..."
        oWord.Documents(goOptions.sFile_DocTarget).Saved = False
        oWord.Documents(goOptions.sFile_DocTarget).Save
        
        goLogging.WriteLog "Quit Word..."
        oWord.Quit wdDoNotSaveChanges
    
    End If

    'Word wird ab hier nicht mehr ben�tigt, also freigeben.
    goLogging.WriteLog "Destroying the word instance..."
    Set oWord = Nothing

End Sub

'''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
' Sub-Prozedur        : CompareDocument
' Beschreibung        : Compares the document in source and target
' Parameter           : -
' wird aufgerufen von : ~
' �nderungshistorie   :
' Autor  Datum       Grund            Kommentar
' -----  ----------  ---------------  ---------------------------------------
'
'''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
Sub CompareDocument()
    
    Dim iFileFormat As Integer
    Dim docSource   As Document
    Dim docTarget   As Document
    Dim docResult   As Document
    
    goLogging.WriteLog "Comparing Documents..."
   
    goLogging.WriteLog "Instanciating Word Object..."
    Set oWord = CreateObject("Word.Application")
        
    'Sollte schon visible = false stehen, aber zur sicherheit...
    goLogging.WriteLog "Hide Word..."
    oWord.Visible = False
   
    'Open document Number 1
    goLogging.WriteLog "Open File " & goOptions.sTemplate
    Set docSource = oWord.Application.Documents.Open(goOptions.sTemplate)
    docSource.ActiveWindow.View = wdPrintView

    'Unprotect the document if necessary
    goLogging.WriteLog "Word Protection Type: " & docSource.ProtectionType
    If docSource.ProtectionType <> wdNoProtection Then
        goLogging.WriteLog "Unprotect the Document"
        If docSource.ProtectionType <> wdNoProtection Then
            docSource.Unprotect Password:=goOptions.sPassword_source
        End If
    End If

    'Open document Number 2
    goLogging.WriteLog "Open File " & goOptions.sFile_DocTarget
    Set docTarget = oWord.Application.Documents.Open(goOptions.sFile_DocTarget)
    docTarget.ActiveWindow.View = wdPrintView

    'Unprotect the document if necessary
    goLogging.WriteLog "Word Protection Type: " & docTarget.ProtectionType
    If docTarget.ProtectionType <> wdNoProtection Then
        goLogging.WriteLog "Unprotect the Document"
        If docTarget.ProtectionType <> wdNoProtection Then
            docTarget.Unprotect Password:=goOptions.sPassword_target
        End If
    End If
    
    'Compare the documents (optional values are in comments with default value)
    goLogging.WriteLog "Comparing " & docSource.Name & " to " & docTarget.Name & " ..."
    Set docResult = oWord.CompareDocuments(OriginalDocument:=docSource, _
                           RevisedDocument:=docTarget, _
                           RevisedAuthor:="iSR", _
                           IgnoreAllComparisonWarnings:=True)
                           'Destination:=wdCompareDestinationNew, _
                           'Granularity:=wdGranularityWordLevel, _
                           'CompareFormatting:=True, _
                           'CompareCaseChanges:=True, _
                           'CompareWhitespace:=True, _
                           'CompareTables:=True, _
                           'CompareHeaders:=True, _
                           'CompareFootnotes:=True, _
                           'CompareTextboxes:=True, _
                           'CompareFields:=True, _
                           'CompareComments:=True, _
                           'CompareMoves:=True, _


    'close the origin documents
    goLogging.WriteLog "Closing original documents..."
    docSource.Close
    docTarget.Close
    
    'Protect the Resulting document using the target password
    If goOptions.sUseProtection = "true" Then
        goLogging.WriteLog "Protecting the result document..."
        docResult.Protect Password:=goOptions.sPassword_target, NoReset:=False, Type:=wdAllowOnlyFormFields
    End If
    
    'Feststellen, ob doc oder docx gespeichert werden soll
    If LCase(Right(Trim(goOptions.sFile_DocTarget), 5)) = ".docx" Then
        iFileFormat = wdFormatXMLDocument
    Else
        iFileFormat = wdFormatDocument
    End If
    
    goLogging.WriteLog "Saving the result document as " & goOptions.sFile_DocTarget
    docResult.SaveAs FileName:=goOptions.sFile_DocTarget, FileFormat:=iFileFormat, _
                               LockComments:=False, Password:="", AddToRecentFiles:=True, _
                               WritePassword:="", ReadOnlyRecommended:=False, EmbedTrueTypeFonts:=False, _
                               SaveNativePictureFormat:=False, SaveFormsData:=False, SaveAsAOCELetter:=False
        
    goLogging.WriteLog "Quit Word..."
    oWord.Quit wdDoNotSaveChanges

End Sub

'''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
' Sub-Prozedur        : WriteDocProperties
' Beschreibung        : Writes the Document Properties to the Taget Document
' Parameter           : sTargetDocName - The target document name
'                       oWord - The word object
' wird aufgerufen von : ViewDoc
' �nderungshistorie   :
' Autor  Datum       Grund            Kommentar
' -----  ----------  ---------------  ---------------------------------------
'
'''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
Sub WriteDocProperties(sTargetDocName As String, oWord As Word.Application, deleteDocProps As Boolean, bOIDonly As Boolean)

    Dim I               As Integer
    Dim j               As Integer
    Dim sDocInfoText    As String
    Dim sDocInfoValue   As String
    Dim oProp           As Object
    Dim iDocProps       As Integer
    Dim aDocProps()     As String
    Dim oRange          As Word.Range
    
    'Nur wenn das Dokument nicht final ist, alle properties l�schen, sonst nur �berschreiben/hinzuf�gen
    If deleteDocProps = True And bOIDonly = False Then
        'Alle DocProperties entfernen (ausser DOC_EMPTY, sonst gibts kuddelmuddel)
        For Each oProp In oWord.Documents(sTargetDocName).CustomDocumentProperties
            If oProp.Name <> "DOC_EMPTY" Then
                oProp.Delete
            End If
        Next oProp
        'Alle DocVariables entfernen (DOC_EMPTY spielt hier keine Rolle)
        For Each oProp In oWord.Documents(sTargetDocName).Variables
            oProp.Delete
        Next oProp
    End If
    
    'Eine Liste aller verf�gbaren DocProperties erstellen
    iDocProps = oWord.Documents(sTargetDocName).CustomDocumentProperties.Count
    ReDim aDocProps(iDocProps)
    I = 0
    For Each oProp In oWord.Documents(sTargetDocName).CustomDocumentProperties
        I = I + 1
        aDocProps(I) = oProp.Name
    Next oProp
    iDocProps = I

    'Einf�gen
    For j = 1 To goOptions.iDocProperties

        sDocInfoText = goOptions.GetDocPropertyName(j)
        sDocInfoValue = goOptions.GetDocPropertyValue(j)
        
        If bOIDonly = True Then
            If sDocInfoText = "DOC_OID" Then
                DoWriteDocProperty sTargetDocName, oWord, sDocInfoText, sDocInfoValue, aDocProps, iDocProps
            End If
        Else
            DoWriteDocProperty sTargetDocName, oWord, sDocInfoText, sDocInfoValue, aDocProps, iDocProps
        End If
    
    Next j
    
    'Aufr�umen
    goLogging.WriteLog "Destructing Objects..."
    Set oProp = Nothing
    
End Sub
    
'''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
' Sub-Prozedur        : UpdateDoc
' Beschreibung        : Aktualisiert alle stories des dokumentes
' Parameter           : sTargetDocName - The target document name
'                       oWord - The word object
' wird aufgerufen von : ~
' �nderungshistorie   :
' Autor  Datum       Grund            Kommentar
' -----  ----------  ---------------  ---------------------------------------
'
'''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
Sub UpdateDoc(sTargetDocName As String, oWord As Word.Application)
    
    Dim oStory          As Object
    Dim oToc            As Object
    Dim oField          As Object
    
    'Dokument updaten
    'oWord.ScreenUpdating = False
    With oWord.Documents(sTargetDocName)

        goLogging.WriteLog "Update stories..."
        For Each oStory In .StoryRanges
            For Each oField In oStory.Fields
                'No update if this is a graphic field.
                'May destroy the scaling and is not neccessary anyway
                If oField.Type <> 67 Then oField.Update
            Next oField
        Next oStory

        goLogging.WriteLog "Update TOCs..."
        For Each oToc In .TablesOfContents
            oToc.Update 'update TOC's
        Next oToc
    End With
    'oWord.ScreenUpdating = True
    
    'Aufr�umen
    goLogging.WriteLog "Destructing Objects..."
    Set oStory = Nothing
    Set oToc = Nothing
    
End Sub

'''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
' Sub-Prozedur        : DoWriteDocProperty
' Beschreibung        : Schreibt die eigentliche Property
' Parameter           : -
' wird aufgerufen von : WriteDocProperties, und nur von dort
' �nderungshistorie   :
' Autor  Datum       Grund            Kommentar
' -----  ----------  ---------------  ---------------------------------------
'
'''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
Private Function DoWriteDocProperty(sTargetDocName As String, oWord As Word.Application, sDocInfoText As String, sDocInfoValue As String, aDocProps() As String, iDocProps As Integer)
    
    Dim PropExists As Boolean
    Dim I          As Integer

    goLogging.WriteLog "Updating Docproperty " & sDocInfoText

    'Dokumenteneigenschaften nur dann �berschreiben, wenn ein Wert �bergeben wird
    If sDocInfoValue = "" Then sDocInfoValue = " "
        
    'Pr�fen ob es die DocProperty bereits gibt
    goLogging.WriteLog "Check if Docproperty exists..."
    PropExists = False
    For I = 1 To iDocProps
        If aDocProps(I) = sDocInfoText Then
            PropExists = True
            Exit For
        End If
    Next I

    If PropExists = True Then
        goLogging.WriteLog "Overwrite existing Docproperty..."
        oWord.Documents(sTargetDocName).CustomDocumentProperties(sDocInfoText).Value = sDocInfoValue
        goLogging.WriteLog "Overwrite existing Docvariable..."
        oWord.Documents(sTargetDocName).Variables(sDocInfoText).Value = sDocInfoValue
    Else
        goLogging.WriteLog "Write new Docproperty..."
        On Error Resume Next
        oWord.Documents(sTargetDocName).CustomDocumentProperties.Add Name:=sDocInfoText, Value:=sDocInfoValue, LinkToContent:=False, Type:=4 ' msoPropertyTypeString
        On Error GoTo 0
        goLogging.WriteLog "Write new Docvariable..."
        On Error Resume Next
        oWord.Documents(sTargetDocName).Variables.Add Name:=sDocInfoText, Value:=sDocInfoValue
        On Error GoTo 0
    End If

End Function

'''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
' Funktion            : GetTemp
' Beschreibung        : Liefert den Pfad zum tempor�ren Directory
' Parameter           : -
' wird aufgerufen von : modWord
' R�ckgabewert        : Pfad zum tempor�ren Directory
' �nderungshistorie   :
' Autor  Datum       Grund            Kommentar
' -----  ----------  ---------------  ---------------------------------------
'
'''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
Public Function GetTemp() As String
    
    Dim sBuffer     As String
    Dim lReturn     As Long
    Dim sTempPath   As String

    sBuffer = Space(iLENGTH)
    lReturn = GetTempPath(iLENGTH, sBuffer)
    If lReturn > 0 Then
        sTempPath = Left(sBuffer, lReturn)
    Else
        sTempPath = CurDir$
        If Right$(sTempPath, 1) <> "\" Then
            sTempPath = sTempPath & "\"
        End If
    End If

    GetTemp = sTempPath

    Exit Function
    
End Function

'''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
' Funktion            : ExtractPath
' Beschreibung        : Liefert den Pfad-Anteil eines Pfad/Filenamen Paares
'                       Falls es keinen Pfad-Anteil gibt, wird stattdessen der
'                       App.path geliefert
' Parameter           : Pfad\Filename.ext
' wird aufgerufen von : ~
' R�ckgabewert        : Pfad
' �nderungshistorie   :
' Autor  Datum       Grund            Kommentar
' -----  ----------  ---------------  ---------------------------------------
'
'''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
Public Function ExtractPath(ByVal sPath As String) As String

    Dim sTemp As String

    ' StrReverse is only working in VB6
    sTemp = StrReverse(sPath)
    sTemp = Right(sTemp, Len(sTemp) - InStr(sTemp, "\"))
    sTemp = StrReverse(sTemp)
    
    If sPath = sTemp Then
        sTemp = App.Path
    End If
    
    ExtractPath = sTemp
    
End Function

'''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
' Funktion            : ExtractFile
' Beschreibung        : Liefert den Filename-Anteil eines Pfad/Filenamen Paares
'                       Falls es keinen Filename-Anteil gibt, wird stattdessen
'                       ein leerer string geliefert.
' Parameter           : Pfad\Filename.ext
' wird aufgerufen von : ~
' R�ckgabewert        : Pfad
' �nderungshistorie   :
' Autor  Datum       Grund            Kommentar
' -----  ----------  ---------------  ---------------------------------------
'
'''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
Public Function ExtractFilename(ByVal sPath As String) As String

    Dim sTemp As String

    ' StrReverse is only working in VB6
    sTemp = StrReverse(sPath)
    sTemp = Left(sTemp, InStr(sTemp, "\") - 1)
    sTemp = StrReverse(sTemp)
    
    If sPath = sTemp Then
        sTemp = ""
    End If
    
    ExtractFilename = sTemp
    
End Function

'''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
' Funktion            : IsIDE
' Beschreibung        : Pr�ft mit einem einfachen Trick, ob das Programm in der
'                       Entwicklungsumgebung ausgef�hrt wurde oder nicht.
' Parameter           : -
' wird aufgerufen von : ~
' R�ckgabewert        : true  - wenn aufgerufen aus IDE
'                       false - wenn standalone ausgef�hrt
' �nderungshistorie   :
' Autor  Datum       Grund            Kommentar
' -----  ----------  ---------------  ---------------------------------------
'
'''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
Public Function IsIDE() As Boolean
    
    On Error GoTo ErrHandler
    'because debug statements are ignored when
    'the app is compiled, the next statment will
    'never be executed in the EXE.
    Debug.Print 1 / 0
    bDEBUG = False
Exit Function
ErrHandler:
    'If we get an error then we are
    'running in IDE / Debug mode
    bDEBUG = True

End Function

'''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
' Funktion            : GetWindowsVersion
' Beschreibung        : Gibt die Version von Windows so detailliert wie m�glich zur�ck
' Parameter           : -
' wird aufgerufen von : ~
' R�ckgabewert        : String mit der Versionsinformation
' �nderungshistorie   :
' Autor  Datum       Grund            Kommentar
' -----  ----------  ---------------  ---------------------------------------
'
'''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
Public Function GetWindowsVersion() As String

    Dim OSVI As OSVERSIONINFO
    Dim lSuccess As Long
    
    OSVI.dwOSVersionInfoSize = Len(OSVI) ' Gr��e der OSVERSIONINFO-Struktur angeben
    lSuccess = GetVersionEx(OSVI)        ' Abruf der Windows-Versionsinformationen
  
    ' Erfolgreiche Abarbeitung �berpr�fen
    If lSuccess = 0 Then
        Exit Function
    End If
    
    ' Auswertung der Versionsnummern:
    Select Case OSVI.dwPlatformId ' Plattform-Kennzeichner
        Case VER_PLATFORM_WIN32s ' Win32s-Erweiterung
            GetWindowsVersion = "Win32s f�r Windows 3.x"
        
        Case VER_PLATFORM_WIN32_WINDOWS ' Windows 9x/Me
            Select Case OSVI.dwMinorVersion
                Case 0
                    GetWindowsVersion = "Windows 95"
                Case 10
                    GetWindowsVersion = "Windows 98"
                Case 90
                    GetWindowsVersion = "Windows Me"
            End Select
        
        Case VER_PLATFORM_WIN32_NT ' Windows NT/2000/XP
            Select Case OSVI.dwMajorVersion
                Case 3
                    GetWindowsVersion = "Windows NT 3." & CStr(OSVI.dwMinorVersion)
                Case 4
                    GetWindowsVersion = "Windows NT 4.0"
                Case 5
                    Select Case OSVI.dwMinorVersion
                        Case 0
                            GetWindowsVersion = "Windows 2000"
                        Case 1
                            GetWindowsVersion = "Windows XP"
                        Case 1
                            GetWindowsVersion = "Windows Server 2003"
                    End Select
                Case 6
                    GetWindowsVersion = "Windows Vista"
                Case Else ' zuk�nftige Versionen von Windows NT
                    GetWindowsVersion = "Windows NT / Folgeversion"
            End Select
    End Select

    GetWindowsVersion = GetWindowsVersion & " - " & OSVI.dwMajorVersion & "."
    GetWindowsVersion = GetWindowsVersion & OSVI.dwMinorVersion & "."
    GetWindowsVersion = GetWindowsVersion & OSVI.dwBuildNumber & " "
    GetWindowsVersion = GetWindowsVersion & OSVI.szCSDVersion

End Function

'''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
' Sub-Prozedur        : MsgBoxCheck
' Beschreibung        : Offnet im Debug eine Messagebox, life wird ein Fehler erzeugt (f�r Logfile & Co)
' Parameter           : sPrompt - Die auszugebende Nachricht
' wird aufgerufen von : ~
' �nderungshistorie   :
' Autor  Datum       Grund            Kommentar
' -----  ----------  ---------------  ---------------------------------------
'
'''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
Sub MsgBoxCheck(sPrompt As String)
    
    If bDEBUG = True Then
        MsgBox (sPrompt)
    Else
        ErrPtnr.OnError "modMain - MsgBoxHandler", sPrompt
    End If

End Sub

'''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
' Funktion            : FileExist
' Beschreibung        : Pr�ft, ob ein File �berhaupt existiert
' Parameter           : -
' wird aufgerufen von : ~
' R�ckgabewert        : true, wenn existiert, ansonsten false
' �nderungshistorie   :
' Autor  Datum       Grund            Kommentar
' -----  ----------  ---------------  ---------------------------------------
'
'''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
Public Function FileExist(dateiname As String) As Boolean

    On Error GoTo fehler:
    FileExist = Dir$(dateiname) <> ""
    Exit Function
fehler:
    FileExist = False
    Resume Next

End Function
