Attribute VB_Name = "modWord"
'''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
' Modul             : modWord
' Beschreibung      : Enth�lt die gesamte Funktionalit�t zum Bearbeiten der Word-Dokumente
' Autor             : as, erweitert und neu struktiert von ms
' Datum             : 20.04.2004
' �nderungshistorie :
' Autor  Datum       Grund            Kommentar
' -----  ----------  ---------------  ---------------------------------------
'
'''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
Option Explicit

Global oWordProps  As cWordProps
Global oWord       As Word.Application
Global iWordVer    As Integer
Global bDocsOpen   As Boolean 'Flag f�r das Errorhandling

'''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
' Sub-Prozedur        : ConvertToWord
' Beschreibung        : Convertiert ein HTML-File zu einem Word-Dokument, bzw. aktualisiert ein
'                       entsprechendes Dokument.
' Parameter           : -
' wird aufgerufen von : modMain.Main
' �nderungshistorie   :
' Autor  Datum       Grund            Kommentar
' -----  ----------  ---------------  ---------------------------------------
'
'''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
Sub ConvertToWord()

    Dim iFileFormat As Integer

    bDocsOpen = False

    goLogging.WriteLog "Start: ConvertToWord"

    ' Word Objekt anlegen
    CreateWordObject
    goLogging.WriteLog "Word Object created"

    ' oWordProperties initialisieren
    Set oWordProps = New cWordProps
    oWordProps.InitWordProps oWord
    goLogging.WriteLog "  Windows Version: " & Trim(GetWindowsVersion())
    goLogging.WriteLog "  Word Version   : " & Trim(oWord.Build)

    iWordVer = Val(Left(oWord.Build, InStr(1, oWord.Build, ".") - 1))

    ' ########################################################################################

    ' Einige Werte f�r Word setzen
    Call SetWordOptions

    ' Supress Word Alerts
    oWord.Application.DisplayAlerts = wdAlertsNone

    ' ########################################################################################

    ' Quell und Zieldokument �ffnen
    Call UpdateFeedback(0, "Open target document")
    Call OpenTargetDoc
    Call CreateListOfTargetBMs
    Call OpenSourceDoc

    ' ########################################################################################

'    'Benutzerdefinierte Eigeneschaften des Quelldokuments auslesen und Namen und Anzahl der
'    'strukturierten Textmarken zur�ckgeben
'    Call UpdateFeedback(0, "Setze Dokumenteneigenschaften")
'    Call CreateDocProperties(oWordProps.sSourceDocName)
'    Call GetUserDefinedProperties

    ' ########################################################################################

    ' Dokumenteigenschaften in das Zieldokument einf�gen
    Call InsertDocProperties(oWordProps.sTargetDocName)

    ' ########################################################################################

    ' Status des Zieldokuments �berpr�fen und anhand dieses bearbeiten
    ' (leeres Dokument -> emptyDoc, gef�lltes Dokument -> fullDoc)
    With oWord.Documents(oWordProps.sTargetDocName).CustomDocumentProperties("DOC_EMPTY")
        If .Value = True Then   'true  = Dokument ist leer
            Call DocEmpty
        Else                    'false = Dokument ist gef�llt
            Call DocFull
        End If
    End With

    ' ########################################################################################

    'Leere Textmarken entfernen
    If goOptions.bRemoveEmptyBM = True Then
        RemoveEmptyBookmarks
    End If
    goLogging.WriteLog "CP   : RemoveEmptyBookmarks"


    ' ########################################################################################

    ' Aktualisiere alle Felder V.2
    Call UpdateAllFields
    goLogging.WriteLog "CP   : UpdateAllFields"

    ' ########################################################################################

    With oWord.Documents(oWordProps.sTargetDocName)

        ' Dokument sch�tzen
        If goOptions.sPassword_target <> "" And goOptions.bUnprotected = False Then
            .Protect Password:=goOptions.sPassword_target, NoReset:=False, Type:=wdAllowOnlyFormFields
        End If
        goLogging.WriteLog "CP   : Doc Protected"

        'Feststellen, ob doc oder docx gespeichert werden soll
        If LCase(Right(Trim(goOptions.sFile_DocTarget), 5)) = ".docx" Then
            iFileFormat = wdFormatXMLDocument
        Else
            iFileFormat = wdFormatDocument
        End If

        ' Datei speichern
        'oWord.ChangeFileOpenDirectory GetTemp()
        .SaveAs FileName:=goOptions.sFile_DocTarget, FileFormat:=iFileFormat, _
                LockComments:=False, Password:="", AddToRecentFiles:=True, _
                WritePassword:="", ReadOnlyRecommended:=False, EmbedTrueTypeFonts:=False, _
                SaveNativePictureFormat:=False, SaveFormsData:=False, SaveAsAOCELetter:=False
        goLogging.WriteLog "CP   : Doc Saved"

        .Close
        goLogging.WriteLog "CP   : Doc Closed"

    End With

    ' ########################################################################################

    ' Word Application Option Eigenschaften wie vorgefunden zur�cksetzen
    With oWord.Options
        .Pagination = oWordProps.bPaginationSetting
        .ConfirmConversions = oWordProps.bConversionSetting
    End With
    goLogging.WriteLog "CP   : Word Application Options Restored"

    ' ########################################################################################

    ' Aufr�umen
    oWord.Quit wdDoNotSaveChanges
    bDocsOpen = False
    oWordProps.ReleaseWordProps
    Set oWord = Nothing
    Set oWordProps = Nothing

    goLogging.WriteLog "End  : ConvertToWord"

End Sub

'''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
' Sub-Prozedur        : UpdateFeedback
' Beschreibung        : Fenster mit Nachricht und Prozentwert (f�r die ProgressBar) updaten
' Parameter           : -
' wird aufgerufen von : ConvertToWord
' �nderungshistorie   :
' Autor  Datum       Grund            Kommentar
' -----  ----------  ---------------  ---------------------------------------
'
'''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
Sub UpdateFeedback(iPercent As Integer, sMessage As String)

    fMain.UpdateMessage sMessage
    fMain.UpdateProgressbar iPercent

End Sub

'''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
' Sub-Prozedur        : SetWordOptions
' Beschreibung        : Setzen einiger Eigenschaften des Word-Objekts
' Parameter           : -
' wird aufgerufen von : ConvertToWord
' �nderungshistorie   :
' Autor  Datum       Grund            Kommentar
' -----  ----------  ---------------  ---------------------------------------
'
'''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
Sub SetWordOptions()

    ' Word Application Options setzen
    goLogging.WriteLog "Start: SetWordOptions"
    With oWord.Options
        .Pagination = True              ' F�hre Zeilenumbruch im Hintergrund durch
        .CheckSpellingAsYouType = False ' Schalte Rechtschreibepr�fung aus
        .CheckGrammarAsYouType = False  ' Schalte Grammatikpr�fung aus
        .ConfirmConversions = False
    End With

    ' nb: hier auf TRUE setzen um "visuell" zu debuggen
'    oWord.Visible = False
    goLogging.WriteLog "End  : SetWordOptions"

    Exit Sub

End Sub

'''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
' Sub-Prozedur        : OpenSourceDoc
' Beschreibung        : �ffnen ein Quelldokument
' Parameter           : -
' wird aufgerufen von : ConvertToWord
' �nderungshistorie   :
' Autor  Datum       Grund            Kommentar
' -----  ----------  ---------------  ---------------------------------------
'
'''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
Sub OpenSourceDoc()

    Dim I As Integer
    Dim sLoadedFiles As String

    ' Quelldokument �ffnen und Ansicht festlegen
    goLogging.WriteLog "Start: OpenSourceDoc"

    oWord.Documents.Add
    oWordProps.sSourceDocName = oWord.ActiveDocument.Name

    For I = 1 To goOptions.iFile_Sources
        If goOptions.GetFile_Source(I) <> "" Then

            'Make sure that the same file is not loaded several times.
            If InStr(sLoadedFiles, goOptions.GetFile_Source(I)) = 0 Then

                If FileExist(goOptions.GetFile_Source(I)) Then
                    goLogging.WriteLog "  Load: " & goOptions.GetFile_Source(I) & " - (" & goOptions.GetFile_Bookmark(I) & ")"

                    With oWord.Selection
                        .InsertFile FileName:=goOptions.GetFile_Source(I), ConfirmConversions:=False
                        .InsertParagraphAfter
                        .InsertBreak Type:=wdSectionBreakNextPage
                        .Collapse Direction:=wdCollapseEnd
                    End With

                    sLoadedFiles = sLoadedFiles & goOptions.GetFile_Source(I) & "|"
                Else
                    goLogging.WriteLog "  Load failed: " & goOptions.GetFile_Source(I) & " - (" & goOptions.GetFile_Bookmark(I) & ")"
                End If

            End If

        Else
            goLogging.WriteLog "  Load failed: No File stated for " & goOptions.GetFile_Bookmark(I)
        End If
    Next I

    goLogging.WriteLog "End  : OpenSourceDoc"

End Sub

'''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
' Sub-Prozedur        : OpenTargetDoc
' Beschreibung        : �ffnet das Zieldokument
' Parameter           : -
' wird aufgerufen von : ConvertToWord
' �nderungshistorie   :
' Autor  Datum       Grund            Kommentar
' -----  ----------  ---------------  ---------------------------------------
'
'''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
Sub OpenTargetDoc()

    ' Zieldokument �ffnen und Ansicht festlegen
    goLogging.WriteLog "Start: OpenTargetDoc"

    oWord.Documents.Open goOptions.sTemplate
    oWordProps.sTargetDocName = oWord.ActiveDocument.Name

    With oWord.Documents(oWordProps.sTargetDocName).ActiveWindow

        If .View.SplitSpecial <> wdPaneNone Then
            .Panes(2).Close
        End If

        If .ActivePane.View.Type = wdNormalView _
            Or .ActivePane.View.Type = wdOutlineView _
            Or .ActivePane.View.Type = wdMasterView Then
            .ActivePane.View.Type = wdPrintView
        End If

        .View.Type = wdPrintView

    End With

    goLogging.WriteLog "  Target Doc open:" & oWordProps.sTargetDocName

    bDocsOpen = True

    goLogging.WriteLog "End  : OpenTargetDoc"

End Sub

Sub CreateListOfTargetBMs()

    Dim oBMrange            As Word.Range
    Dim iBMcount            As Integer
    Dim sBMname             As String
    Dim I                   As Integer

    goLogging.WriteLog "Start: CreateListOfTargetBMs"

    Set oBMrange = oWord.Documents(oWordProps.sTargetDocName).Range
    iBMcount = oBMrange.Bookmarks.Count

    For I = 1 To iBMcount
        sBMname = oBMrange.Bookmarks(I).Name

        If UCase(sBMname) Like sBM_STRUCT Then
            oWordProps.SetStructBM sBMname
            goLogging.WriteLog "  S_TM found: " & sBMname
        End If
    Next I

    goLogging.WriteLog "End  : CreateListOfTargetBMs"

End Sub

'''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
' Sub-Prozedur        : InsertDocProperties
' Beschreibung        : Dokumenteigenschaften in das Zieldokument einf�gen
' Parameter           : -
' wird aufgerufen von : ConvertToWord
' �nderungshistorie   :
' Autor  Datum       Grund            Kommentar
' -----  ----------  ---------------  ---------------------------------------
'
'''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
Sub InsertDocProperties(sTargetDocName As String)

    Dim m               As Integer
    Dim sDocInfoName    As String
    Dim sDocInfoValue   As String
    Dim oProp           As Object
    Dim iDocProps       As Integer
    Dim aDocProps()     As String
    Dim I               As Integer
    Dim PropExists      As Boolean

    goLogging.WriteLog "Start: InsertDocProperties"

    'Alle DocProperties entfernen (ausser DOC_EMPTY, sonst gibts kuddelmuddel)
    For Each oProp In oWord.Documents(sTargetDocName).CustomDocumentProperties
        If oProp.Name <> "DOC_EMPTY" Then
            oProp.Delete
        End If
    Next
    'Alle DocVariables entfernen (DOC_EMPTY spielt hier keine Rolle)
    For Each oProp In oWord.Documents(sTargetDocName).Variables
        oProp.Delete
    Next

    'Eine Liste aller verf�gbaren DocProperties erstellen
    iDocProps = oWord.Documents(sTargetDocName).CustomDocumentProperties.Count
    ReDim aDocProps(iDocProps)
    I = 0
    For Each oProp In oWord.Documents(sTargetDocName).CustomDocumentProperties
        I = I + 1
        aDocProps(I) = oProp.Name
    Next

    For m = 1 To goOptions.iDocProperties

        sDocInfoName = goOptions.GetDocPropertyName(m)
        sDocInfoValue = goOptions.GetDocPropertyValue(m)
        If sDocInfoValue = "" Then sDocInfoValue = " "

        'Dokumenteneigenschaften nur dann �berschreiben, wenn ein Wert �bergeben wird
        If sDocInfoValue <> "" Then

            'Pr�fen ob es die DocProperty bereits gibt
            PropExists = False
            For I = 1 To iDocProps
                If aDocProps(I) = sDocInfoName Then
                    PropExists = True
                    Exit For
                End If
            Next I

            If PropExists = True Then
                oWord.Documents(sTargetDocName).CustomDocumentProperties(sDocInfoName).Value = sDocInfoValue
                oWord.Documents(sTargetDocName).Variables(sDocInfoName).Value = sDocInfoValue
            Else
                oWord.Documents(sTargetDocName).CustomDocumentProperties.Add Name:=sDocInfoName, Value:=sDocInfoValue, LinkToContent:=False, Type:=4 ' msoPropertyTypeString
                oWord.Documents(sTargetDocName).Variables.Add Name:=sDocInfoName, Value:=sDocInfoValue
                iDocProps = iDocProps + 1
                ReDim Preserve aDocProps(iDocProps)
                aDocProps(iDocProps) = sDocInfoName
            End If

            goLogging.WriteLog "  Document Property " & sDocInfoName & " = " & sDocInfoValue
            goLogging.WriteLog "  Document Variable " & sDocInfoName & " = " & sDocInfoValue
        Else
            goLogging.WriteLog "  Document Property " & sDocInfoName & " is empty => ignored"
        End If

    Next m

    goLogging.WriteLog "End  : InsertDocProperties"

End Sub

'''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
' Sub-Prozedur        : UpdateAllFields
' Beschreibung        : Aktualisiert alle Felder
' Parameter           : -
' wird aufgerufen von : ConvertToWord
' �nderungshistorie   :
' Autor  Datum       Grund            Kommentar
' -----  ----------  ---------------  ---------------------------------------
'
'''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
Sub UpdateAllFields()

    Dim oStory As Object
    Dim oToc As Object

    Dim objShape As Word.Shape
    Dim objField As Field
    Dim objInlineShape As InlineShape

    'Application.ScreenUpdating = False

    With oWord.Documents(oWordProps.sTargetDocName)

        'Update fields in all stories
        For Each oStory In .StoryRanges
            oStory.Fields.Update
        Next oStory
        goLogging.WriteLog "CP   : Stories updated"

        'Update all TOC
        For Each oToc In .TablesOfContents
            oToc.Update
        Next oToc
        goLogging.WriteLog "CP   : All Table of Contents updated"

        'Update all TOF
        For Each oToc In .TablesOfFigures
            oToc.Update
        Next oToc
        goLogging.WriteLog "CP   : All Table of Figures updated"

        'Break all graphic Links - 2003
        For Each objField In .Fields
          If Not objField.LinkFormat Is Nothing And objField.Type = wdFieldIncludePicture Then
            goLogging.WriteLog "Breaking Link: " & objField.LinkFormat.SourceFullName
            objField.LinkFormat.BreakLink
            .UndoClear
          End If
        Next
        goLogging.WriteLog "CP   : Breaking Graphic Links - 2003 style finished"

        'Break all graphic Links - 2007
        For Each objInlineShape In .InlineShapes
            If Not objInlineShape.LinkFormat Is Nothing Then
                objInlineShape.LinkFormat.SavePictureWithDocument = True
            End If
        Next
        goLogging.WriteLog "CP   : Breaking Graphic Links - 2007 style finished"

        'Shortly switch to Print Preview to update all fields
        goLogging.WriteLog "CP   : Activate print preview"
        .ActiveWindow.View = wdPrintPreview
        If .ActiveWindow.View = wdPrintPreview Then
            .ClosePrintPreview
            goLogging.WriteLog "CP   : Print preview closed"
        Else
            goLogging.WriteLog "CP   : Print preview failed to activate, current Viewtype: " & .ActiveWindow.View
        End If

    End With

    'Application.ScreenUpdating = True

End Sub

'''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
' Sub-Prozedur        : DocEmpty
' Beschreibung        : Bearbeitet ein leeres Dokument
' Parameter           : -
' wird aufgerufen von : ConvertToWord
' �nderungshistorie   :
' Autor  Datum       Grund            Kommentar
' -----  ----------  ---------------  ---------------------------------------
'
'''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
Sub DocEmpty()

    Dim I                   As Integer
    Dim j                   As Integer
    Dim x                   As Integer
    Dim bDocEmpty           As Boolean
    Dim oBMrange            As Word.Range
    Dim iBMcount            As Integer
    Dim sBMname             As String
    Dim bStructBM           As Boolean
    Dim sStructBM           As String
    Dim oStructBMrange      As Word.Range
    Dim iSourceBMcount      As Integer
    Dim sSourceBMname       As String       'sQuelleTMName
    Dim iSelectedRange      As Integer      'nSelektAbschnitt
    Dim oSourceBMrange      As Word.Range   'QuelleTMBereich
    Dim oTargetBMrange      As Word.Range   'ZielTMBereich
    Dim iCountStructBM      As Integer      'f�r Progressbar
    Dim sLastTableBMname    As String

    Dim bUnprotected        As Boolean
    Dim bNoCaption          As Boolean
    Dim a                   As Integer

    goLogging.WriteLog "Start: DocEmpty"

    iCountStructBM = 0
    bDocEmpty = True

    ' Falls das Ziel-Dokument gesch�tzt ist (was bei einem leeren doc nicht passieren sollte) -> schutz aufheben
    If oWord.Documents(oWordProps.sTargetDocName).ProtectionType <> wdNoProtection Then
        oWord.Documents(oWordProps.sTargetDocName).Unprotect Password:=goOptions.sPassword_source
    End If

    ' F�ge alle Textmarken zusammen
    Call BeginEndBMtransformation

    Set oBMrange = oWord.Documents(oWordProps.sSourceDocName).Range
    iBMcount = oBMrange.Bookmarks.Count
    goLogging.WriteLog "Number of all BM in Source document = " & iBMcount


    ' Gehe alle BM im Quelldokument der REIHE NACH durch
    For I = 1 To iBMcount
        sBMname = oBMrange.Bookmarks(I).Name
        goLogging.WriteLog "--[ " & sBMname & " ]" & Right("-------------------------------------------------------------- --- -- -", 72 - Len(sBMname))

        For a = 1 To goOptions.iFile_Sources
            If goOptions.GetFile_Bookmark(a) = sBMname Then
                If goOptions.GetFile_Protected(a) <> "false" Then
                    bUnprotected = False
                Else
                    bUnprotected = True
                End If

                If goOptions.GetFile_SetCaption(a) <> "false" Then
                    bNoCaption = False
                Else
                    bNoCaption = True
                End If

                Exit For
            End If
        Next a
        goLogging.WriteLog "    " & sBMname & ": unprotected: " & bUnprotected

        ' �berpr�fe, ob sBMName eine Strukturierte Textmarke ist.
        bStructBM = False
        For j = 1 To oWordProps.iStructBM
            If sBMname = oWordProps.GetStructBM(j) Then
                bStructBM = True
                Exit For
            End If
        Next j

        ' wenn sBMname eine Strukturierte Textmarke ist:
        If bStructBM = True Then

            If oWord.Documents(oWordProps.sTargetDocName).Bookmarks.Exists(sBMname) = True Then

                goLogging.WriteLog "    " & sBMname & ": " & I & ". structured Bookmark."

                sStructBM = oWordProps.GetStructBM(j)
                Set oStructBMrange = oWord.Documents(oWordProps.sSourceDocName).Bookmarks(sBMname).Range
                iSourceBMcount = oStructBMrange.Bookmarks.Count

                'Name of the last tablebookmark (needed to suppress the final Page Break)
                x = iSourceBMcount
               
                Do While x > 0
                    If oStructBMrange.Bookmarks(x).Name Like sBM_TABLE Or oStructBMrange.Bookmarks(x).Name Like sBM_GRAPHIC Then Exit Do
                    x = x - 1
                Loop
                
                If x <> 0 Then sLastTableBMname = oStructBMrange.Bookmarks(x).Name

                ' wenn die Strukt TM eine S_TM ist , dann...
                If UCase(sBMname) Like sBM_STRUCT Then

                    iCountStructBM = iCountStructBM + 1

                    For x = 2 To iSourceBMcount  ' beginnt mit x=2, um die strukturierte TM selbst auszuschliessen

                        Call UpdateFeedback(((100 / oWordProps.iStructBM) * (iCountStructBM - 1)) + ((100 / oWordProps.iStructBM / iSourceBMcount) * x - 1), "Insertion of data tables into new document " & I & "/" & iBMcount)


                        ' da ich in dieser Schleife bin, werden Tabellen und �S in das Zieldokument eingef�gt und deshalb
                        ' die Variable bDocEmpty auf false gesetzt

                        bDocEmpty = False
                        sSourceBMname = oStructBMrange.Bookmarks(x).Name

                        goLogging.WriteLog oStructBMrange.Bookmarks(x).Name & ":  " & x & ". SubBM inside of " & sStructBM

                        If x = 2 Then
                            oWord.Documents(oWordProps.sTargetDocName).ActiveWindow.Selection.GoTo What:=wdGoToBookmark, Name:=sBMname
'                            oWord.Documents(oWordProps.sTargetDocName).ActiveWindow.Selection.StartOf Unit:=wdSection, Extend:=wdMove
                        End If

                        If oStructBMrange.Bookmarks(x).Name Like sBM_UEBER And bNoCaption = False Then
                            If x = 2 Then
                                Call Insert1stCaption(sSourceBMname, sBMname)
                            Else
                                Call InsertCaption(sSourceBMname, sBMname)
                            End If
                        ElseIf oStructBMrange.Bookmarks(x).Name Like sBM_TABLE Or oStructBMrange.Bookmarks(x).Name Like sBM_GRAPHIC Then

                            Call InsertTable(sSourceBMname, bUnprotected)

                            If bNoCaption = True And sSourceBMname <> sLastTableBMname Then
                                oWord.Documents(oWordProps.sTargetDocName).ActiveWindow.Selection.InsertBreak Type:=wdPageBreak
                            End If

                        End If

                        oWord.Documents(oWordProps.sTargetDocName).UndoClear
                        I = I + 1

                    Next x

                ' wenn die Strukt TM keine S_TM ist , dann...
                Else

                    For x = 2 To iSourceBMcount

                        sSourceBMname = oStructBMrange.Bookmarks(x).Name

                        With oWord.Documents(oWordProps.sTargetDocName)
                            If x = 2 Then
                                    .ActiveWindow.Selection.GoTo What:=wdGoToBookmark, Name:=sStructBM
                                    .ActiveWindow.Selection.Collapse Direction:=wdCollapseStart
                                    ' ^^^ sehr hilfreich, um eine Markierung auf den Anfang zu verkleinern
                                    Call InsertTable(sSourceBMname, bUnprotected)
                                    .Sections(iSelectedRange).ProtectedForForms = False
                            Else
                                Call InsertTable(sSourceBMname, bUnprotected)
                                .Sections(iSelectedRange).ProtectedForForms = False
                            End If
                            .UndoClear
                            I = I + 1
                        End With

                    Next x

                End If

            Else
                ' Die TM ist in der Vorlage NICHT vorhanden
                goLogging.WriteLog sBMname & ":  " & I & ". structured bookmark, which is NOT part of the target document."
            End If

        ' wenn sBMName KEINE Strukturierte TM ist, dann
        Else
            If oWord.Documents(oWordProps.sTargetDocName).Bookmarks.Exists(sBMname) = True _
            And Not sBMname Like sBM_REF _
            And Not sBMname Like sBM_TABLE _
            And Not sBMname Like sBM_GRAPHIC _
            Then

                ' TM ist in der Vorlage vorhanden und ist KEINE "Referenzierte Textmarke"
                goLogging.WriteLog "    " & sBMname & ":  " & I & ". NON structured bookmark, which is part of the target document."


                With oWord.Documents(oWordProps.sTargetDocName)

                    Set oTargetBMrange = .Bookmarks(sBMname).Range
                    oTargetBMrange.Select
                    .ActiveWindow.Selection.Delete
                    oTargetBMrange.Select

                    Call InsertTable(sBMname, bUnprotected)

                End With

            Else
                ' Die TM ist in der Vorlage NICHT vorhanden
                goLogging.WriteLog sBMname & ":  " & I & ". NON structured bookmark, which is NOT part of the target document or is part of another bookmark."
            End If

        End If
        oWord.Documents(oWordProps.sTargetDocName).UndoClear
    Next I

    goLogging.WriteLog "--[ End of Bookmarks Loop ]----------------------------------------- --- -- -"

    If iSelectedRange <> Empty Then
        oWord.Documents(oWordProps.sTargetDocName).Sections(iSelectedRange).ProtectedForForms = False 'vorletzte Abschnitt des Result Bereiches nicht sch�tzen
        ' Was passiert aber, wenn nach der Result TM noch eine weitere strukt.TM kommt  wenn-> Schaue Dir an, wie Du es bei den andere strukt TM gel�st hast
        ' ->  wrd.activeDocument.Sections(iSelectedRange - 1).ProtectedForForms = True  DANACH DANN  wrd.activeDocument.Sections(iSelectedRange).ProtectedForForms = False
    End If

    ' �berpr�fe, ob Dokument leer blieb
    If Not bDocEmpty Then
        oWord.Documents(oWordProps.sTargetDocName).CustomDocumentProperties("DOC_EMPTY").Value = False
    End If

    goLogging.WriteLog "   DOC_EMPTY = " & bDocEmpty

    goLogging.WriteLog "End  : DocEmpty"

End Sub

'''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
' Sub-Prozedur        : DocFull
' Beschreibung        : Bearbeitet ein Dokument, welches bereits Inhalte enth�lt.
'                       Diese werden dann beibehalten.
' Parameter           : -
' wird aufgerufen von : ConvertToWord
' �nderungshistorie   :
' Autor  Datum       Grund            Kommentar
' -----  ----------  ---------------  ---------------------------------------
'
'''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
Sub DocFull()

    Dim I                   As Integer
    Dim j                   As Integer
    Dim x                   As Integer
    Dim oBMrange            As Word.Range
    Dim iBMcount            As Integer
    Dim sBMname             As String
    Dim sStructBM           As String
    Dim bStructBM           As Boolean
    Dim oStructBMrange      As Word.Range
    Dim iSourceBMcount      As Integer

    Dim sPreviousBMname     As String
    Dim sSourceBMname       As String       'sQuelleTMName
    Dim iSelectedRange      As Integer      'nSelektAbschnitt
    Dim oSourceBMrange      As Word.Range   'QuelleTMBereich
    Dim oTargetBMrange      As Word.Range   'ZielTMBereich
    Dim sLastTableBMname    As String

    Dim bUnprotected        As Boolean
    Dim bNoCaption          As Boolean
    Dim a                   As Integer

    Dim oBM                 As Word.Bookmark

    goLogging.WriteLog "Start: DocFull"

    ' Dokument entsperren
    If goOptions.sPassword_source <> "" And goOptions.bUnprotected = False Then
        If oWord.Documents(oWordProps.sTargetDocName).ProtectionType <> wdNoProtection Then
            oWord.Documents(oWordProps.sTargetDocName).Unprotect Password:=goOptions.sPassword_source
        End If
    End If

    ' F�ge alle Textmarken im Quelldokument zusammen
    Call BeginEndBMtransformation

    Set oBMrange = oWord.Documents(oWordProps.sSourceDocName).Range
    iBMcount = oBMrange.Bookmarks.Count
    goLogging.WriteLog "Number of bookmarks in the source document = " & iBMcount


    ' Gehe alle BM im Quelldokument der REIHE NACH durch
    For I = 1 To iBMcount
        sBMname = oBMrange.Bookmarks(I).Name
        goLogging.WriteLog "--[ " & sBMname & " ]" & Right("-------------------------------------------------------------- --- -- -", 72 - Len(sBMname))

        For a = 1 To goOptions.iFile_Sources
            If goOptions.GetFile_Bookmark(a) = sBMname Then
                If goOptions.GetFile_Protected(a) <> "false" Then
                    bUnprotected = False
                Else
                    bUnprotected = True
                End If
                If goOptions.GetFile_SetCaption(a) <> "false" Then
                    bNoCaption = False
                Else
                    bNoCaption = True
                End If
                Exit For
            End If
        Next a
        goLogging.WriteLog "    " & sBMname & ": unprotected: " & bUnprotected

        ' �berpr�fe, ob sBMName eine Strukturierte Textmarke ist.
        bStructBM = False
        For j = 1 To oWordProps.iStructBM
            If sBMname = oWordProps.GetStructBM(j) Then
                bStructBM = True
                Exit For
            End If
        Next j

        ' wenn sBMname eine Strukturierte Textmarke ist:
        If bStructBM = True Then

            ' Pr�fen, ob sich die Reihenfolge der Sortierung ver�ndert hat.
            CheckForChanges (sBMname)

            goLogging.WriteLog "    " & sBMname & ": " & I & ". structured bookmark."

            sStructBM = oWordProps.GetStructBM(j)
            Set oStructBMrange = oWord.Documents(oWordProps.sSourceDocName).Bookmarks(sBMname).Range
            iSourceBMcount = oStructBMrange.Bookmarks.Count

            'Name of the last tablebookmark (needed to suppress the final Page Break)
            x = iSourceBMcount
            
            Do While x > 0
                If oStructBMrange.Bookmarks(x).Name Like sBM_TABLE Or oStructBMrange.Bookmarks(x).Name Like sBM_GRAPHIC Then Exit Do
                x = x - 1
            Loop
            
            If x <> 0 Then sLastTableBMname = oStructBMrange.Bookmarks(x).Name

            If UCase(sBMname) Like sBM_STRUCT Then

                goLogging.WriteLog "    " & sBMname & ": " & I & ". structured bookmark."

                ' beginnt mit x=2, um die strukturierte TM selbst auszuschliessen
                For x = 2 To iSourceBMcount
                    Call UpdateFeedback(x / iSourceBMcount * 100, "Insertion of data into existing document (" & CStr(x) & " of " & CStr(iSourceBMcount) & ")")

                    sSourceBMname = oStructBMrange.Bookmarks(x).Name
                    goLogging.WriteLog "    " & sSourceBMname & ":  " & x & ". SubBM inside of " & sStructBM

                    ' wenn die TM im Zieldokument existiert
                    If oWord.Documents(oWordProps.sTargetDocName).Bookmarks.Exists(sSourceBMname) = True Then


                        If oStructBMrange.Bookmarks(x).Name Like sBM_UEBER And bNoCaption = False Then
                            ' wenn die TM eine �berschrift ist, dann...
                            Call ReplaceCaption(sSourceBMname)
                        ElseIf oStructBMrange.Bookmarks(x).Name Like sBM_TABLE Or oStructBMrange.Bookmarks(x).Name Like sBM_GRAPHIC Then
                            ' wenn die TM eine Tabelle ist, dann...
                            Call ReplaceTable(sSourceBMname, bUnprotected)

                        End If

                    ' wenn die TM im Zieldokument NICHT existiert
                    Else

                        ' wenn x>2 dann kann die TM nicht die 1.�S sein
                        If x > 2 Then
                            If sSourceBMname Like sBM_UEBER And bNoCaption = False Then

                                ' wenn die neue TM eine �berschrift ist ,dann...
                                goLogging.WriteLog "    Insert Type 3"

                                With oWord.Documents(oWordProps.sTargetDocName).ActiveWindow.Selection
                                    sPreviousBMname = PreviousLegalBookmark(oStructBMrange, x - 1, bNoCaption)
                                    .GoTo What:=wdGoToBookmark, Name:=sPreviousBMname
                                    iSelectedRange = .Information(wdActiveEndSectionNumber)
                                    .GoTo What:=wdGoToSection, Name:=iSelectedRange + 1
                                End With

                                Call InsertCaption(sSourceBMname, sBMname)

                            ElseIf sSourceBMname Like sBM_TABLE Or sSourceBMname Like sBM_GRAPHIC Then
                                ' wenn die neue TM eine Tabelle ist , dann....
                                sPreviousBMname = PreviousLegalBookmark(oStructBMrange, x - 1, bNoCaption)

                                ' Typ 1

                                    If sPreviousBMname Like sBM_UEBER And bNoCaption = False Then
                                        ' wenn die vorhergehende TM eine �S ist, dann
                                        goLogging.WriteLog "    Insert Type 1"
                                        With oWord.Documents(oWordProps.sTargetDocName)
                                            .ActiveWindow.ActivePane.View.SeekView = wdSeekCurrentPageHeader
                                            .Bookmarks(sPreviousBMname).Range.Sections.Item(1).Range.Select
                                            ' MS 20080302 Move to the end, or else the table will end up in the previous chapter (not good!)
                                            '.ActiveWindow.Selection.StartOf Unit:=wdSection, Extend:=wdMove
                                            .ActiveWindow.Selection.EndOf Unit:=wdSection, Extend:=wdMove
                                        End With

                                    ' Typ 2
                                    Else
                                        'wenn die vorhergehende TM eine Tabelle ist , dann...
                                        goLogging.WriteLog "    Insert Type 2"
                                        With oWord.Documents(oWordProps.sTargetDocName).ActiveWindow.Selection
                                            If bNoCaption = False Then
                                                .GoTo What:=wdGoToBookmark, Name:=sPreviousBMname
                                                iSelectedRange = .Information(wdActiveEndSectionNumber)
                                                .GoTo What:=wdGoToSection, Name:=iSelectedRange + 1
                                                .EndOf Unit:=wdSection, Extend:=wdMove 'GEHE ANS ENDE DES ABSCHNITTES (weil davor eventuell Text steht!!!)
                                            Else
                                                'move to the end of the current S_ bookmark
                                                .GoTo What:=wdGoToBookmark, Name:=sBMname
                                                .MoveRight Unit:=wdCharacter, Count:=1
                                                .MoveLeft Unit:=wdCharacter, Count:=1
                                            End If

                                        End With

                                    End If

                                Call InsertTable(sSourceBMname, bUnprotected)

                                If bNoCaption = True And sSourceBMname <> sLastTableBMname Then
                                    oWord.Documents(oWordProps.sTargetDocName).ActiveWindow.Selection.InsertBreak Type:=wdPageBreak
                                End If

                            End If

                        ' wenn x = 2. dann ist TM die 1. �S
                        Else
                            goLogging.WriteLog "    Insert Type 4"
                            If bNoCaption = False Then InsertCaptionAs1stCaption sSourceBMname, sBMname
                        End If

                    End If
                    I = I + 1
                Next x

            ' wenn es nicht die RESULT TM ist, dann
            Else
                ' beginnt mit x=2, um die strukturierte TM selbst auszuschliessen
                For x = 2 To iSourceBMcount
                    sSourceBMname = oStructBMrange.Bookmarks(x).Name
                    If oWord.Documents(oWordProps.sTargetDocName).Bookmarks.Exists(sSourceBMname) = True Then  ' gepr�ft und OK

                            Call ReplaceTable(sSourceBMname, bUnprotected)

                    Else
                        With oWord.Documents(oWordProps.sTargetDocName).ActiveWindow.Selection
                            If x > 2 Then ' gepr�ft und OK
                                sPreviousBMname = PreviousLegalBookmark(oStructBMrange, x - 1, bNoCaption)
                                .GoTo What:=wdGoToBookmark, Name:=sPreviousBMname
                                iSelectedRange = .Information(wdActiveEndSectionNumber)
                                .GoTo What:=wdGoToSection, Name:=iSelectedRange + 1
                                .EndOf Unit:=wdSection, Extend:=wdMove 'GEHE ANS ENDE DES ABSCHNITTES (weil davor eventuell Text steht!!!)

                                Call InsertTable(sSourceBMname, bUnprotected)

                            Else ' wenn x = 2 ( gepr�ft und OK )
                                .GoTo What:=wdGoToBookmark, Name:=sStructBM
                                .Collapse Direction:=wdCollapseStart

                                Call InsertTable(sSourceBMname, bUnprotected)

                            End If
                        End With
                    End If
                Next x
            End If

            'Call AlignSourceWithTarget(sBMname)

        ' wenn es keine strukturierte TM ist
        Else

            If oWord.Documents(oWordProps.sTargetDocName).Bookmarks.Exists(sBMname) = True _
            And Not sBMname Like sBM_REF _
            And Not sBMname Like sBM_TABLE _
            And Not sBMname Like sBM_GRAPHIC _
            Then

                ' TM ist in der Vorlage vorhanden und ist KEINE "Referenzierte Textmarke"
                goLogging.WriteLog "    " & sBMname & ":  " & I & ". NON structured bookmark, which is part of the target document."



                With oWord.Documents(oWordProps.sTargetDocName)

                    goLogging.WriteLog "    Length of target Bookmark " & Len(.Bookmarks(sBMname).Range.Text)

                    If Len(oWord.Documents(oWordProps.sTargetDocName).Bookmarks(sBMname).Range.Text) < 5 Then

                        Set oTargetBMrange = .Bookmarks(sBMname).Range
                        oTargetBMrange.Select
                        .ActiveWindow.Selection.Delete
                        oTargetBMrange.Select

                        Call InsertTable(sBMname, bUnprotected)

                    Else
                        Call ReplaceTable(sBMname, bUnprotected)
                    End If

                End With

            End If
        End If
    Next I

    goLogging.WriteLog "--[ End of Bookmarks Loop ]----------------------------------------- --- -- -"

    'Align Source With Target
    'durch alle Strukturierten Bookmarks im TARGET gehen und schaun, ob sich was getan hat
    With oWord.Documents(oWordProps.sTargetDocName)
        For Each oBM In .Bookmarks
            If oBM.Name Like sBM_STRUCT Then
                
                bNoCaption = True
                
                For a = 1 To goOptions.iFile_Sources
                    If goOptions.GetFile_Bookmark(a) = sBMname Then
                        If goOptions.GetFile_SetCaption(a) <> "false" Then bNoCaption = False
                        Exit For
                    End If
                Next a
                
                If bNoCaption = True Then
                    Call AlignSourceWithTarget(oBM.Name)
                    'Call AlignSourceWithTargetLegacy(oBM.Name)
                Else
                    Call AlignSourceWithTargetLegacy(oBM.Name)
                End If
            End If
        Next oBM
    End With

    'Statische Bookmarks leeren
    Call ClearStaticBookmarks

    goLogging.WriteLog "End  : DocFull"

End Sub


'''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
' Sub-Prozedur        : BeginEndBMtransformation
' Beschreibung        : Diese Prozedur f�gt folgende Textmarken zusammen:
'                        - �berschriften_Begin_Textmarken und �berschriften_End_Textmarken => �S_Textmarken            z.B. UEBER1
'                        - Tabellen_Begin_Textmarken und Tabellen_End_Textmarken           => Tab_Textmarke            z.B. A1
'                        - Formatierungs_Begin_Textmarke und Formatierungs_End_Textmarke   => Formatierungs_Textmarke  z.B. _TPASR02
'                        - Referenzierte_Begin_Textmarke und Referenzierte_End_Textmarke   => Referenzierte_Textmarke  z.B. ID12345
' Parameter           :
' wird aufgerufen von : EmptyDoc, FullDoc
' m�gliche Fehler     : Eine oder beide der Textmarken (Begin oder End) sind nicht vorhanden
' �nderungshistorie   :
' Autor  Datum       Grund            Kommentar
' -----  ----------  ---------------  ---------------------------------------
'
'''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
Sub BeginEndBMtransformation()

    Dim iBMcount            As Integer ' Anzahl der Bookmarks
    Dim I                   As Integer

    Dim aBookmarks()        As Variant
    Dim sTempBookmarkName   As String

    Dim oRange              As Word.Range
    Dim oShape              As Word.InlineShape
    Dim oShapeRange         As Word.Range

    Dim iPos                As Integer
    Dim sBookmark1          As String
    Dim sBookmark2          As String
    Dim sBookmark3          As String
    Dim sFormat             As String
    Dim sFrame              As String
    Dim iLine               As Integer
    Dim sBackground         As String

    goLogging.WriteLog "Start: BeginEndBMtransformationExt"

    oWord.Documents(oWordProps.sSourceDocName).Bookmarks.ShowHidden = True

    iBMcount = oWord.Documents(oWordProps.sSourceDocName).Range.Bookmarks.Count
    ReDim aBookmarks(iBMcount)

    ' Schreibe die Textmarken nach der Position im Dokument in das Array
    For I = 1 To iBMcount
        aBookmarks(I) = oWord.Documents(oWordProps.sSourceDocName).Range.Bookmarks(I).Name
    Next I

    I = 1
    Do While I < iBMcount
        Call UpdateFeedback(I / iBMcount * 100, "Transforming bookmarks")

        sTempBookmarkName = aBookmarks(I)

        ' Wenn die TM eine unsichtbare TM ist, dann
        If Left(sTempBookmarkName, 1) = "_" Then

            ' Wenn die TM ein END beinhaltet, dann
            If sTempBookmarkName Like sBM_END Then

                'Styles extrahieren
                sFormat = Right(sTempBookmarkName, 3)
                sFrame = Mid(sFormat, 1, 1)
                sBackground = Mid(sFormat, 2, 1)
                iLine = Mid(sFormat, 3, 1)

                'Bookmarks zusammenbauen
                iPos = InStr(1, sTempBookmarkName, "END")
                sBookmark1 = sTempBookmarkName
                sBookmark2 = Left(sTempBookmarkName, iPos - 1) & "BGN"
                sBookmark3 = Left(sTempBookmarkName, iPos - 1) & sFrame & sBackground & iLine

                goLogging.WriteLog "    " & sBM_END & ":"
                goLogging.WriteLog "    sBookmark1 = " & sBookmark1
                goLogging.WriteLog "    sBookmark2 = " & sBookmark2
                goLogging.WriteLog "    sBookmark3 = " & sBookmark3
                goLogging.WriteLog "    Rahmen = " & sFrame
                goLogging.WriteLog "    iLine = " & iLine

                With oWord.Documents(oWordProps.sSourceDocName)
                    Set oRange = .Range(Start:=.Bookmarks(sBookmark2).Start, End:=.Bookmarks(sBookmark1).End)
                    oRange.Select
                    .Bookmarks.Add Range:=.ActiveWindow.Selection.Range, Name:=sBookmark3
                    .Bookmarks(sBookmark1).Delete
                    .Bookmarks(sBookmark2).Delete
                End With

            Else
                goLogging.WriteLog "    sTempBookmarkName = " & sTempBookmarkName
            End If

        ' Wenn die TM KEINE unsichtbare TM ist, dann
        Else

            ' wenn die TM ein BEGIN beinhaltet, dann
            If sTempBookmarkName Like sBM_BEGIN Then

                iPos = InStr(1, sTempBookmarkName, "BGN")
                sBookmark1 = sTempBookmarkName
                sBookmark2 = Left(sTempBookmarkName, iPos - 1) & "END"
                sBookmark3 = Left(sTempBookmarkName, iPos - 2)

                goLogging.WriteLog "    " & sBM_BEGIN & ":"
                goLogging.WriteLog "    sBookmark1 = " & sBookmark1
                goLogging.WriteLog "    sBookmark2 = " & sBookmark2
                goLogging.WriteLog "    sBookmark3 = " & sBookmark3

                With oWord.Documents(oWordProps.sSourceDocName)
                    Set oRange = .Range(Start:=.Bookmarks(sBookmark1).Start, End:=.Bookmarks(sBookmark2).End)
                    oRange.Select
                    .Bookmarks.Add Range:=.ActiveWindow.Selection.Range, Name:=sBookmark3
                    .Bookmarks(sBookmark1).Delete
                    .Bookmarks(sBookmark2).Delete

                    If UCase(sTempBookmarkName) Like sBM_GRAPHIC Then
                        goLogging.WriteLog "Graphic found: " & sTempBookmarkName
                    End If

                End With

            Else
                goLogging.WriteLog "    sTempBookmarkName = " & sTempBookmarkName
            End If

        End If

        oWord.Documents(oWordProps.sSourceDocName).UndoClear
        I = I + 1
    Loop

    oWord.Documents(oWordProps.sSourceDocName).Bookmarks.ShowHidden = False

    goLogging.WriteLog "End  : BeginEndBMtransformationExt"

End Sub


'''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
' Sub-Prozedur        : FormatRange
' Beschreibung        : Formatiert den �bergebenen Abschnitt
' Parameter           : oFormatRange - Abschnitt, welcher formatiert werden soll
' wird aufgerufen von : ~
' �nderungshistorie   :
' Autor  Datum       Grund            Kommentar
' -----  ----------  ---------------  ---------------------------------------
'
'''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
Sub FormatRange(oFormatRange As Word.Range)

    Dim oFormatRangeTemp    As Word.Range
    Dim I                   As Integer
    Dim sBMname             As String
    Dim sFrame              As String
    Dim iLine               As Integer
    Dim sBackground         As String

    goLogging.WriteLog "Start: FormatRange"

    oFormatRange.Bookmarks.ShowHidden = True

    For I = 1 To oFormatRange.Bookmarks.Count

        sBMname = oFormatRange.Bookmarks(I).Name

        ' Ersetze in der Tabelle "TPAS" <br> durch einen Zeilenumbruch
        If sBMname = sTABLENAME Then

            Set oFormatRangeTemp = oFormatRange.Bookmarks(sBMname).Range
            With oFormatRangeTemp.Find
                .ClearFormatting
                .Text = "<br>"

                With .Replacement
                    .ClearFormatting
                    .Text = Chr(11)
                End With

                .Execute Replace:=wdReplaceAll, Format:=True, MatchCase:=True, MatchWholeWord:=True
            End With

        End If

        ' wenn das Style-Sheet fehlerhaft ist, k�nnne "BEGIN" - Textmarken �brig bleiben !!!
        If (InStr(1, sBMname, "BEGIN", vbTextCompare) > 0) Then
            goLogging.WriteLog "    INCONSITENCY: this bookmark is left over: " + sBMname
        End If

        If (Left(sBMname, 1) = "_") And (InStr(1, sBMname, "BEGIN", vbTextCompare) = 0) And (InStr(1, sBMname, "REF", vbTextCompare) = 0) Then

            ' Ermittle die Formatierungs Anweisungen RAHMEN und LINIE
            sFrame = Mid(Right(sBMname, 3), 1, 1)
            sBackground = Mid(Right(sBMname, 3), 2, 1)
            iLine = Mid(Right(sBMname, 3), 3, 1)

            ' Selektiere den zu formatierenden Bereich
            oFormatRange.Bookmarks(sBMname).Select

            ' Rufe die Formatierung auf
            '************************************************************
            Call Formating(sFrame, sBackground, iLine)
            '************************************************************
        End If

    Next I

    ' setze die unsichtbaren Textmarken wieder unsichtbar
    oFormatRange.Bookmarks.ShowHidden = False

    goLogging.WriteLog "End  : FormatRange"

End Sub

'''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
' Sub-Prozedur        : Formating
' Beschreibung        : Formatiert das Aussehen einer Tabelle anhand der �bergabewerte
' Parameter           : sLocFrame - Kennzeichnung f�r den Rahmentyp
'                       nLocBackground - Kennzeichnung f�r die Hintergrundart
'                       nLocLine - Kennzeichnung f�r die Linienart
' wird aufgerufen von : FormatRange
' �nderungshistorie   :
' Autor  Datum       Grund            Kommentar
' -----  ----------  ---------------  ---------------------------------------
'
'''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
Public Sub Formating(sLocFrame As String, nLocBackground As String, nLocLine As Integer)

    ' 2005-07-25, (nb) - klappt nur, wenn auch wirklich eine Tabelle markiert ist
    ' -> diese Funktion setzt voraus, das immerdie GANZE Tabelle markiert ist .... trifft nicht immer (eigentlich nie) zu
    '
    On Error Resume Next
    oWord.Selection.Rows.AllowBreakAcrossPages = False
    On Error GoTo 0

    With oWord.Selection.Cells

        Select Case sLocFrame

            Case "A"
                ' BackgroundColor festlegen
                With .Shading
                    .Texture = wdTextureNone
                    .ForegroundPatternColor = wdColorAutomatic
                    .BackgroundPatternColor = BackgroundColor(nLocBackground)
                End With

                ' Rahmen und Linie setzten
                .Borders(wdBorderLeft).LineStyle = LineType(nLocLine)
                .Borders(wdBorderRight).LineStyle = LineType(nLocLine)
                .Borders(wdBorderTop).LineStyle = LineType(nLocLine)
                .Borders(wdBorderBottom).LineStyle = LineType(nLocLine)

            Case "B"
                ' Rahmenlinie = links
                ' Hintergrundfarbe festlegen
                With .Shading
                    .Texture = wdTextureNone
                    .ForegroundPatternColor = wdColorAutomatic
                    .BackgroundPatternColor = BackgroundColor(nLocBackground)
                End With

                ' Rahmen und Linie setzten
                With .Borders(wdBorderLeft)
                    .LineStyle = LineType(nLocLine)
                    .LineWidth = wdLineWidth050pt
                    .Color = wdColorAutomatic
                End With

                .Borders.Shadow = False

            Case "C"
                ' Rahmenlinie = unten
                ' Hintergrundfarbe festlegen
                With .Shading
                    .Texture = wdTextureNone
                    .ForegroundPatternColor = wdColorAutomatic
                    .BackgroundPatternColor = BackgroundColor(nLocBackground)
                End With

                ' Rahmen und Linie setzten
                .Borders(wdBorderLeft).LineStyle = wdLineStyleNone
                .Borders(wdBorderRight).LineStyle = wdLineStyleNone
                .Borders(wdBorderTop).LineStyle = wdLineStyleNone
                .Borders(wdBorderBottom).LineStyle = LineType(nLocLine)

                .Borders.Shadow = False

            Case "D"
                ' Rahmenlinie = rechts
                ' Hintergrundfarbe festlegen
                With .Shading
                    .Texture = wdTextureNone
                    .ForegroundPatternColor = wdColorAutomatic
                    .BackgroundPatternColor = BackgroundColor(nLocBackground)
                End With

                ' Rahmen und Linie setzten
                .Borders(wdBorderLeft).LineStyle = wdLineStyleNone
                .Borders(wdBorderRight).LineStyle = LineType(nLocLine)
                .Borders(wdBorderTop).LineStyle = wdLineStyleNone
                .Borders(wdBorderBottom).LineStyle = wdLineStyleNone
                .Borders(wdBorderHorizontal).LineStyle = wdLineStyleNone
                .Borders(wdBorderVertical).LineStyle = wdLineStyleNone
                .Borders(wdBorderDiagonalDown).LineStyle = wdLineStyleNone
                .Borders(wdBorderDiagonalUp).LineStyle = wdLineStyleNone

            Case "E"
                ' Rahmenlinie = oben
                ' Hintergrundfarbe festlegen
                With .Shading
                    .Texture = wdTextureNone
                    .ForegroundPatternColor = wdColorAutomatic
                    .BackgroundPatternColor = BackgroundColor(nLocBackground)
                End With

                ' Rahmen und Linie setzten
                .Borders(wdBorderLeft).LineStyle = wdLineStyleNone
                .Borders(wdBorderRight).LineStyle = wdLineStyleNone
                .Borders(wdBorderTop).LineStyle = LineType(nLocLine)

                .Borders.Shadow = False

            Case "F"
                ' Rahmenlinie = Mitte-Horizontal, Mitte-Vertikal ( 2 in der Mitte gekreuzte Linie)
                ' Hintergrundfarbe festlegen
                With .Shading
                    .Texture = wdTextureNone
                    .ForegroundPatternColor = wdColorAutomatic
                    .BackgroundPatternColor = BackgroundColor(nLocBackground)
                End With

                ' Rahmen und Linie setzten
                .Borders(wdBorderHorizontal).LineStyle = LineType(nLocLine)
                .Borders(wdBorderVertical).LineStyle = LineType(nLocLine)

            Case "G" ' wie Typ A, Selection wird als Tabellen�berschrift markieren
                ' BackgroundColor festlegen
                With .Shading
                    .Texture = wdTextureNone
                    .ForegroundPatternColor = wdColorAutomatic
                    .BackgroundPatternColor = BackgroundColor(nLocBackground)
                End With

                ' Rahmen und Linie setzten
                .Borders(wdBorderLeft).LineStyle = LineType(nLocLine)
                .Borders(wdBorderRight).LineStyle = LineType(nLocLine)
                .Borders(wdBorderTop).LineStyle = LineType(nLocLine)
                .Borders(wdBorderBottom).LineStyle = LineType(nLocLine)

                ' Selection als Tabellen�berschrift markieren
                oWord.Selection.Rows.HeadingFormat = True

            Case "H"
                ' BackgroundColor festlegen
                With .Shading
                    .Texture = wdTextureNone
                    .ForegroundPatternColor = wdColorAutomatic
                    .BackgroundPatternColor = BackgroundColor(nLocBackground)
                End With

                ' Rahmen und Linie setzten
                .Borders(wdBorderVertical).LineStyle = LineType(nLocLine)
                .Borders(wdBorderHorizontal).LineStyle = LineType(nLocLine)
                .Borders(wdBorderVertical).LineWidth = wdLineWidth025pt
                .Borders(wdBorderHorizontal).LineWidth = wdLineWidth025pt

            Case "I"
                With oWord.Selection
                    .Fields.Add Range:=oWord.Selection.Range, Type:=wdFieldPage
                    Select Case nLocBackground
                        Case "A" 'von
                            .TypeText Text:=" von "
                        Case "B" 'of
                            .TypeText Text:=" of "
                        Case Else
                            .TypeText Text:="/"
                    End Select
                    .Fields.Add Range:=oWord.Selection.Range, Type:=wdFieldNumPages
                End With

            Case Else

        End Select

    End With

End Sub

'''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
' Funktion            : LineType
' Beschreibung        : Bestimmt den Linienstil anhand des �bergebenen Wertes
' Parameter           : nLocNum - Kennzeichnung f�r die Linienart
' wird aufgerufen von : Formating
' R�ckgabewert        : wdLineStyleDouble oder wdLineStyleSingle
' �nderungshistorie   :
' Autor  Datum       Grund            Kommentar
' -----  ----------  ---------------  ---------------------------------------
'
'''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
Public Function LineType(nLocNum As Integer) As String

    Dim sStyle As String

    Select Case nLocNum

            Case 1
                    ' LinienTyp = doppelt
                    sStyle = wdLineStyleDouble
            Case 2
                    ' LinienTyp = einfach
                    sStyle = wdLineStyleSingle
    End Select

    LineType = sStyle

End Function

Public Function BackgroundColor(nLocBG As String) As String
'''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
' Funktion            : BackgroundColor
' Beschreibung        : Bestimmt die Farbe des Hintergrundes anhand des �bergebenen Wertes
' Parameter           : nLocBG - Kennzeichnung f�r den Hintergrund
' wird aufgerufen von : Formating
' R�ckgabewert        : wdColorAutomatic oder wdColorGray05/10/125
' �nderungshistorie   :
' Autor  Datum       Grund            Kommentar
' -----  ----------  ---------------  ---------------------------------------
'
'''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''

    Dim sBackground As String

    Select Case nLocBG
        Case "M"
                    ' KEINE Schattierung
                    sBackground = wdColorAutomatic
        Case "N"
                    ' Schattierung Grau 5%
                    sBackground = wdColorGray05
        Case "O"
                    ' Schattierung Grau 10%
                    sBackground = wdColorGray10
        Case "P"
                    ' Schattierung Grau 12,5%
                    sBackground = wdColorGray125
    End Select

    BackgroundColor = sBackground

End Function

'''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
' Sub-Prozedur        : Insert1stCaption
' Beschreibung        : F�gt die erste �berschrift in das Zieldokument ein
' Parameter           : sSourceBMname - Name der Quelltextmarke
' wird aufgerufen von : EmptyDoc
' �nderungshistorie   :
' Autor  Datum       Grund            Kommentar
' -----  ----------  ---------------  ---------------------------------------
'
'''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
Sub Insert1stCaption(sSourceBMname As String, sBMname As String)   'fuege_erste_Ueberschrift_ein

    Dim nSectionNumber  As Integer
    Dim nPageNumer      As Integer
    Dim oHeader         As HeaderFooter
    Dim oRange          As Range
    Dim oBookmark       As Bookmark
    Dim oSourceBMrange  As Range
    Dim oTargetBMrange  As Range

    goLogging.WriteLog "Start: Insert1stCaption"

    With oWord.Documents(oWordProps.sTargetDocName)

        'Position der Einf�gemarke vermerken
        goLogging.WriteLog "  Cursorposition         = " & .ActiveWindow.Selection.Range.Start & "." & .ActiveWindow.Selection.Range.End

        nSectionNumber = .Bookmarks(sBMname).Range.Information(wdActiveEndSectionNumber)
        nPageNumer = .Bookmarks(sBMname).Range.Information(wdActiveEndPageNumber)

        'Abschnittsnummer und Seite merken
        goLogging.WriteLog "  nSectionNumber         = " & nSectionNumber
        goLogging.WriteLog "  nPageNumer             = " & nPageNumer

        Set oHeader = .Sections(nSectionNumber).Headers(wdHeaderFooterPrimary)

        'Merken, ob die Kopfzeile mit der vorherigen verbunden ist
        goLogging.WriteLog "  oHeader.LinkToPrevious = " & oHeader.LinkToPrevious

        Set oRange = oHeader.Range
        oRange.SetRange Start:=oHeader.Range.End - 3, End:=oHeader.Range.End
        oHeader.Range.Bookmarks.Add sSourceBMname, oRange
        Set oSourceBMrange = oWord.Documents(oWordProps.sSourceDocName).Bookmarks(sSourceBMname).Range

        '************************************************
        Call FormatRange(oSourceBMrange)
        '************************************************

        Set oTargetBMrange = .Bookmarks(sSourceBMname).Range
        oTargetBMrange.FormattedText = oSourceBMrange.FormattedText

        'Setzte den Cursor auf die 1. Seite nach Resulttext
        Set oBookmark = .Bookmarks(sBMname)
        Set oRange = oBookmark.Range
        oRange.SetRange Start:=oBookmark.Start, End:=oBookmark.Start
        oRange.Select

        goLogging.WriteLog "  Cursorposition = " & .ActiveWindow.Selection.Range.Start & "." & .ActiveWindow.Selection.Range.End
        goLogging.WriteLog "  Cursor is positioned on page " & oRange.Information(wdActiveEndPageNumber)
        goLogging.WriteLog "  Cursor is positioned in section " & oRange.Information(wdActiveEndSectionNumber)

        goLogging.WriteLog "End  : Insert1stCaption"

    End With


End Sub

'''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
' Sub-Prozedur        : InsertCaption
' Beschreibung        : F�gt eine �berschrift in das Zieldokument ein
' Parameter           : sSourceBMname - Name der Quelltextmarke
' wird aufgerufen von : EmptyDoc
' �nderungshistorie   :
' Autor  Datum       Grund            Kommentar
' -----  ----------  ---------------  ---------------------------------------
'
'''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
Sub InsertCaption(sSourceBMname As String, sBMname As String)

    Dim oHeader             As HeaderFooter
    Dim iSelectedRange      As Integer
    Dim oTargetBMrangeTable As Object
    Dim oTargetTable        As Word.Range
    Dim oSourceBMrange      As Word.Range
    Dim oTargetBMrange      As Word.Range
    Dim iHeaderTableCount   As Integer

    goLogging.WriteLog "Start: InsertCaption"

    With oWord.Documents(oWordProps.sTargetDocName)

        'Position der Einf�gemarke vermerken
        goLogging.WriteLog "  Cursorposition                = " & .ActiveWindow.Selection.Range.Start & "." & .ActiveWindow.Selection.Range.End

        .ActiveWindow.Selection.InsertBreak Type:=wdSectionBreakNextPage  ' hier kommen die sich aufschaukelnden Zeilen her
        iSelectedRange = .ActiveWindow.Selection.Information(wdActiveEndSectionNumber)
        .Sections(iSelectedRange - 1).ProtectedForForms = False
        goLogging.WriteLog "  iSelectedRange                = " & iSelectedRange

        Set oHeader = .Sections(iSelectedRange).Headers(wdHeaderFooterPrimary)
        goLogging.WriteLog "  oHeader.LinkToPrevious before = " & oHeader.LinkToPrevious
        oHeader.LinkToPrevious = False
        goLogging.WriteLog "  oHeader.LinkToPrevious after  = " & oHeader.LinkToPrevious

        iHeaderTableCount = oHeader.Range.Tables.Count
        Set oTargetBMrangeTable = oHeader.Range.Tables(iHeaderTableCount)

        Set oTargetTable = oTargetBMrangeTable.Range
        oTargetTable.Columns.Delete

        '**********************************************************************************************
        'Wenn ein oder mehrere Tabellen vorhanden sein sollten, muss zus�tzlich ein Return-Zeichen mitausgew�hlt werden.
        'Workaround f�r �ltere Word Versionen (bis 2003)? In neueren f�hrt das zu ganz komischen Effekten.
        '**********************************************************************************************

        If iWordVer <= 11 Then
            If iHeaderTableCount = 1 Then
                oTargetTable.SetRange Start:=oTargetTable.Start - 1, End:=oTargetTable.End
            End If

            If iHeaderTableCount > 1 Then
                oTargetTable.SetRange Start:=oTargetTable.Start - 2, End:=oTargetTable.End
            End If
        End If

        Set oSourceBMrange = oWord.Documents(oWordProps.sSourceDocName).Bookmarks(sSourceBMname).Range
        '************************************************
        Call FormatRange(oSourceBMrange)
        '************************************************
        oTargetTable.FormattedText = oSourceBMrange.FormattedText

        'Wo sitzt der Cursor
        goLogging.WriteLog "  Cursorposition         = " & .ActiveWindow.Selection.Range.Start & "." & .ActiveWindow.Selection.Range.End

    End With

    goLogging.WriteLog "End  : InsertCaption"

End Sub

'''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
' Sub-Prozedur        : InsertCaptionAs1stCaption
' Beschreibung        : F�gt eine �berschrift als erste �berschrift ein
' Parameter           : sSourceBMname - Name der Quelltextmarke
' wird aufgerufen von : DocFull
' �nderungshistorie   :
' Autor  Datum       Grund            Kommentar
' -----  ----------  ---------------  ---------------------------------------
'
'''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
Sub InsertCaptionAs1stCaption(sSourceBMname As String, sBMname As String)

    Dim oTargetBMrangeTable As Object
    Dim iSelectedRange      As Integer
    Dim sTempBMname         As String
    Dim oHeader             As HeaderFooter
    Dim oTargetTable        As Word.Range
    Dim oSourceBMrange      As Word.Range
    Dim oTargetBMrange      As Word.Range

    goLogging.WriteLog "Start: InsertCaptionAs1stCaption"

    With oWord.Documents(oWordProps.sTargetDocName)

        .ActiveWindow.Selection.GoTo What:=wdGoToBookmark, Name:=sBMname
        .ActiveWindow.Selection.StartOf Unit:=wdSection, Extend:=wdMove

        iSelectedRange = .ActiveWindow.Selection.Information(wdActiveEndSectionNumber)
        .ActiveWindow.ActivePane.View.SeekView = wdSeekCurrentPageHeader
        Set oTargetBMrange = .Sections(iSelectedRange).Headers(wdHeaderFooterPrimary).Range

        '##########################################
        ' achtgeben auf nachfolgende Zeile, falls man manuelle die Kopfzeilenverkn�pfung getrennt hat, da automatisch der Bookmark in der unteren Kopfzeile VERSCHWINDET!

        'Ja ich weiss, das ist ein wirklich w�ster Hack. Nicht zuhause nachmachen.
        On Error GoTo NoBookmarkError
            sTempBMname = oTargetBMrange.Bookmarks(1).Name
        GoTo NoBookmarkCont
NoBookmarkError:
       .ActiveWindow.ActivePane.View.SeekView = wdSeekMainDocument
        Call Insert1stCaption(sSourceBMname, sBMname)
        sTempBMname = sSourceBMname
        GoTo EndSub
NoBookmarkCont:
        On Error GoTo 0

        '##########################################
        .ActiveWindow.ActivePane.View.SeekView = wdSeekMainDocument
        '.ActiveWindow.Selection.InsertBreak Type:=wdSectionBreakNextPage
        iSelectedRange = .ActiveWindow.Selection.Information(wdActiveEndSectionNumber)

        Set oHeader = .Sections(iSelectedRange).Headers(wdHeaderFooterPrimary)
        goLogging.WriteLog "oHeader.LinkToPrevious before = " & oHeader.LinkToPrevious
        oHeader.LinkToPrevious = False
        goLogging.WriteLog "oHeader.LinkToPrevious after  = " & oHeader.LinkToPrevious

        'Die letzte Tabelle im Header selektieren
        Set oTargetBMrangeTable = oHeader.Range.Tables(oHeader.Range.Tables.Count)

        Set oTargetTable = oTargetBMrangeTable.Range
        oTargetTable.Select
        .ActiveWindow.Selection.Bookmarks.Add sTempBMname, Range:=oWord.Selection.Range
        .ActiveWindow.Selection.GoTo What:=wdGoToBookmark, Name:=sBMname
        .ActiveWindow.Selection.StartOf Unit:=wdSection, Extend:=wdMove
        iSelectedRange = .ActiveWindow.Selection.Information(wdActiveEndSectionNumber)

        Set oHeader = .Sections(iSelectedRange).Headers(wdHeaderFooterPrimary)
        goLogging.WriteLog "oHeader.LinkToPrevious before = " & oHeader.LinkToPrevious
        oHeader.LinkToPrevious = False
        goLogging.WriteLog "oHeader.LinkToPrevious after  = " & oHeader.LinkToPrevious

        'Die letzte Tabelle im Header selektieren
        Set oTargetBMrangeTable = oHeader.Range.Tables(oHeader.Range.Tables.Count)

        Set oTargetTable = oTargetBMrangeTable.Range
        oTargetTable.Select
        .ActiveWindow.Selection.Rows.Delete
        Set oSourceBMrange = oWord.Documents(oWordProps.sSourceDocName).Bookmarks(sSourceBMname).Range

        '************************************************
        Call FormatRange(oSourceBMrange)
        '************************************************

        oTargetTable.FormattedText = oSourceBMrange.FormattedText


        ' Code generated by Macro Begin
        ' Force Change to Print View Mode
        If .ActiveWindow.View.SplitSpecial <> wdPaneNone Then
            .ActiveWindow.Panes(2).Close
        End If
        If .ActiveWindow.ActivePane.View.Type = wdNormalView Or .ActiveWindow.ActivePane.View.Type = wdOutlineView Then
            .ActiveWindow.ActivePane.View.Type = wdPrintView
        End If
        .ActiveWindow.ActivePane.View.SeekView = wdSeekCurrentPageHeader
        If .ActiveWindow.View.SplitSpecial = wdPaneNone Then
            .ActiveWindow.ActivePane.View.Type = wdPrintView
        Else
            .ActiveWindow.View.Type = wdPrintView
        End If
        ' Code generated by Macro End

        .ActiveWindow.ActivePane.View.SeekView = wdSeekMainDocument

    End With

EndSub:

    goLogging.WriteLog "End  : InsertCaptionAs1stCaption"

End Sub

'''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
' Sub-Prozedur        : InsertTable
' Beschreibung        : F�gt eine Tabelle in das Zieldokument ein
' Parameter           : sSourceBMname - Name der Quelltextmarke
' wird aufgerufen von : DocEmpty, DocFull
' �nderungshistorie   :
' Autor  Datum       Grund            Kommentar
' -----  ----------  ---------------  ---------------------------------------
'
'''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
Sub InsertTable(sSourceBMname As String, bUnprotected As Boolean)

    Dim iSelectedRange  As Integer
    Dim oSourceBMrange  As Word.Range
    Dim oTargetBMrange  As Word.Range
    Dim I               As Integer

    goLogging.WriteLog "Start: InsertTable"

    With oWord.Documents(oWordProps.sTargetDocName)

        If goOptions.bUnprotected = False Then
            .ActiveWindow.Selection.InsertBreak Type:=wdSectionBreakContinuous  ' Abschnittwechsel fortlaufend
            iSelectedRange = .ActiveWindow.Selection.Information(wdActiveEndSectionNumber)
            .Sections(iSelectedRange - 1).ProtectedForForms = False
        End If
        
        .ActiveWindow.Selection.TypeText Text:="  "
        .ActiveWindow.Selection.MoveLeft Unit:=wdCharacter, Count:=2, Extend:=wdExtend
        .ActiveWindow.Selection.Bookmarks.Add sSourceBMname, Range:=.ActiveWindow.Selection.Range
        Set oSourceBMrange = oWord.Documents(oWordProps.sSourceDocName).Bookmarks(sSourceBMname).Range

        '************************************************
        Call FormatRange(oSourceBMrange)
        '************************************************

        Set oTargetBMrange = .Bookmarks(sSourceBMname).Range

        oTargetBMrange.FormattedText = oSourceBMrange.FormattedText
        
        ' !!! Dieser Schritt dauert bei grossen Dokumenten lange.
'        .ActiveWindow.Selection.GoTo What:=wdGoToBookmark, Name:=sSourceBMname
'        .ActiveWindow.Selection.Collapse Direction:=wdCollapseEnd 'gehe an das Ende der wrd.selection, dh der Cursor springt an den Zeilenanfang ;

        ' !!! erstzt durch das hier:
        .ActiveWindow.Selection.Start = oTargetBMrange.End

'        'eine Zeile nach dem TextmarkenKlammerEnde
'        If .ActiveWindow.Selection.Information(wdWithInTable) Then
'            'Ich vermute, dass dieser Check nicht mehr notwendig ist
'            goLogging.WriteLog "       Check wdWithInTable"
'            .ActiveWindow.Selection.MoveDown wdParagraph, 1
'        End If

        If goOptions.bUnprotected = False Then
            .ActiveWindow.Selection.InsertBreak Type:=wdSectionBreakContinuous
            iSelectedRange = .ActiveWindow.Selection.Information(wdActiveEndSectionNumber)

            'Check, if this table has to be unprotected
            If bUnprotected = True Then
                .Sections(iSelectedRange - 1).ProtectedForForms = False
            Else
                .Sections(iSelectedRange - 1).ProtectedForForms = True
            End If
        End If

    End With

    goLogging.WriteLog "End  : InsertTable"

End Sub

'''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
' Sub-Prozedur        : ReplaceTable
' Beschreibung        : Ersetzt eine bestehende Tabelle mit einer neuen
' Parameter           : sSourceBMname - Name der Quelltextmarke
' wird aufgerufen von : DocFull
' �nderungshistorie   :
' Autor  Datum       Grund            Kommentar
' -----  ----------  ---------------  ---------------------------------------
'
'''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
Sub ReplaceTable(sSourceBMname As String, bUnprotected As Boolean)

    Dim iSelectedRange  As Integer
    Dim oSourceBMrange  As Word.Range
    Dim oTargetBMrange  As Word.Range

    goLogging.WriteLog "Start: ReplaceTable"

    Set oSourceBMrange = oWord.Documents(oWordProps.sSourceDocName).Bookmarks(sSourceBMname).Range
    '************************************************
    Call FormatRange(oSourceBMrange)
    '************************************************

    With oWord.Documents(oWordProps.sTargetDocName)
        Set oTargetBMrange = .Bookmarks(sSourceBMname).Range
        oTargetBMrange.Select
        .ActiveWindow.Selection.Delete
        oTargetBMrange.Select
        On Error Resume Next
        .ActiveWindow.Selection.Rows.Delete
        oTargetBMrange.FormattedText = oSourceBMrange.FormattedText
        On Error GoTo 0
        .ActiveWindow.Selection.GoTo What:=wdGoToBookmark, Name:=sSourceBMname
'        .ActiveWindow.Selection.Rows.AllowBreakAcrossPages = False
'        .ActiveWindow.Selection.ParagraphFormat.KeepWithNext = True '  ABS�TZE NICHT TRENNEN <- WICHTIG!

        'Check, if this table has to be unprotected (only relevant for non structured bookmarks
        If Not sSourceBMname Like sBM_STRUCT And Not sSourceBMname Like sBM_TABLE And Not sSourceBMname Like sBM_GRAPHIC Then
            iSelectedRange = .ActiveWindow.Selection.Information(wdActiveEndSectionNumber)

            If bUnprotected = True Then
                .Sections(iSelectedRange).ProtectedForForms = False
                goLogging.WriteLog "    " & sSourceBMname & " (" & iSelectedRange & "): unprotected."
            Else
                .Sections(iSelectedRange).ProtectedForForms = True
                goLogging.WriteLog "    " & sSourceBMname & " (" & iSelectedRange & "): unprotected."
            End If
        End If

    End With

    goLogging.WriteLog "End  : ReplaceTable"

End Sub

'''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
' Sub-Prozedur        : ReplaceCaption
' Beschreibung        : Ersetzt eine bestehende �berschrift mit einer neuen
' Parameter           : sSourceBMname - Name der Quelltextmarke
' wird aufgerufen von : DocFull
' �nderungshistorie   :
' Autor  Datum       Grund            Kommentar
' -----  ----------  ---------------  ---------------------------------------
'
'''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
Sub ReplaceCaption(sSourceBMname As String)

    Dim oSourceBMrange  As Word.Range
    Dim oTargetBMrange  As Word.Range

    goLogging.WriteLog "Start: ReplaceCaption"

    With oWord.Documents(oWordProps.sTargetDocName)
        .ActiveWindow.ActivePane.View.SeekView = wdSeekCurrentPageHeader
        Set oSourceBMrange = oWord.Documents(oWordProps.sSourceDocName).Bookmarks(sSourceBMname).Range

        '************************************************
        Call FormatRange(oSourceBMrange)
        '************************************************

        Set oTargetBMrange = .Bookmarks(sSourceBMname).Range

'        oTargetBMrange.WholeStory
'        oTargetBMrange.Delete

        oTargetBMrange.Select

        On Error GoTo NoRowError
        .ActiveWindow.Selection.Rows.Delete
        GoTo NoRowCont
NoRowError:
        .ActiveWindow.Selection.Delete
NoRowCont:
        On Error GoTo 0


        oTargetBMrange.FormattedText = oSourceBMrange.FormattedText
        .ActiveWindow.ActivePane.View.SeekView = wdSeekMainDocument
    End With

    goLogging.WriteLog "End  : ReplaceCaption"

End Sub

Sub AlignSourceWithTarget(sBMname As String)

    Dim sTargetBMname   As String
    Dim iTargetBMcount  As Integer
    Dim oStructBMrange  As Word.Range
    Dim sBM             As Variant
    Dim oDeleteableParagraphRange As Word.Range
    
    goLogging.WriteLog "Start: AlignSourceWithTarget (" & sBMname & ")"
    Call UpdateFeedback(99, "Check for deletable data")
    
    ' sBMname is always a structured bookmarkname
    
    Set oStructBMrange = oWord.Documents(oWordProps.sTargetDocName).Bookmarks(sBMname).Range
    
    For Each sBM In oStructBMrange.Bookmarks
    sTargetBMname = sBM
        If sBM Like sBM_TABLE Or sBM Like sBM_GRAPHIC Then
            If oWord.Documents(oWordProps.sSourceDocName).Bookmarks.Exists(sBM) = False Then
                'If this does not exist in the source doc, remove in the target doc
                With oWord.Documents(oWordProps.sTargetDocName)
                    .Bookmarks(sBM).Select
                    
                    Set oDeleteableParagraphRange = .Range(Start:=.ActiveWindow.Selection.Sections.First.Range.Start - 1, End:=.ActiveWindow.Selection.Sections.Last.Range.End)

                    'Add empty chars. Else a (protected!) section break will be left over
                    oDeleteableParagraphRange.InsertAfter " "
                    oDeleteableParagraphRange.InsertBefore " "
                    oDeleteableParagraphRange.Select
                    .ActiveWindow.Selection.Delete
                                
                    '.ActiveWindow.Selection.Delete
                    goLogging.WriteLog "  " & sTargetBMname & " was deleted"
                End With
            End If
        End If
    Next
    
    goLogging.WriteLog "End  : AlignSourceWithTarget"

End Sub

'''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
' Sub-Prozedur        : AlignSourceWithTargetLegacy
' Beschreibung        : Diese Prozedur f�hrt einen Abgleich zwischen Quell- und
'                       Zieldokument durch und l�scht die Textmarken im
'                       Zieldokument, die es im Quelldokument nicht findet
' Parameter           : sBMname
' wird aufgerufen von : fullDoc
' �nderungshistorie   :
' Autor  Datum       Grund            Kommentar
' -----  ----------  ---------------  ---------------------------------------
'
'''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
Sub AlignSourceWithTargetLegacy(sBMname As String)

    Dim I As Integer
    Dim iDeletedBDcount As Integer
    Dim oStructBMrange  As Word.Range
    Dim aTargetBM()     As Variant
    Dim iTagetBMcount   As Integer
    Dim iResult         As Integer
    Dim iArrayIndex     As Integer
    Dim iSelectedRange  As Long
    Dim sTargetBMname   As String
    Dim sBM             As String

    Dim oPreviousParagraphRange   As Word.Range
    Dim oDeleteableParagraphRange As Word.Range
    Dim iPreviousParagraphEnd     As Long
    Dim iPreviousParagraphsCount  As Long

    goLogging.WriteLog "Start: AlignSourceWithTargetLegacy (" & sBMname & ")"

    iDeletedBDcount = 0

    Set oStructBMrange = oWord.Documents(oWordProps.sTargetDocName).Bookmarks(sBMname).Range
    iTagetBMcount = oStructBMrange.Bookmarks.Count

    If sBMname Like sBM_STRUCT And iTagetBMcount > 1 Then

        ' Trenne die Tabellen-Textmarken von den zwischenzeitlich
        ' eventuell eingef�gten Textmarken beim �berarbeiten
        ReDim aTargetBM(1 To iTagetBMcount - 1)
        iArrayIndex = 0

        For I = 1 To iTagetBMcount
            sBM = oStructBMrange.Bookmarks(I).Name
            If sBM Like sBM_UEBER Or sBM Like sBM_TABLE Or sBM Like sBM_GRAPHIC Then
                iArrayIndex = iArrayIndex + 1
                aTargetBM(iArrayIndex) = sBM
            End If
        Next I

        ' Beginne die gefilterten TM aus dem Array auszulesen und zu �berpr�fen,
        ' ob diese gel�scht werden sollen
        For I = 1 To iArrayIndex
            sTargetBMname = aTargetBM(I)
            Call UpdateFeedback(I / (iArrayIndex) * 100, "Check for deletable data")

            If oWord.Documents(oWordProps.sSourceDocName).Bookmarks.Exists(sTargetBMname) = False Then

                ' wenn im Source Dokument der BM nicht enthalten ist, dann l�sche im Target-Dokument den BM
                ' dabei kann es sich nur um eine Tabelle handeln, da das RangeObjekt die BM in den Kopfzeilen nicht mit ausliest
                With oWord.Documents(oWordProps.sTargetDocName)

                    .ActiveWindow.Selection.GoTo What:=wdGoToBookmark, Name:=sTargetBMname
                    iSelectedRange = .ActiveWindow.Selection.Information(wdActiveEndSectionNumber)

                    If iArrayIndex = iDeletedBDcount + 1 Then  ' Fall 4a: getestet und OK
                        Set oPreviousParagraphRange = .Sections(iSelectedRange - 1).Range
                        iPreviousParagraphEnd = oPreviousParagraphRange.End
                        Set oDeleteableParagraphRange = .Range(Start:=iPreviousParagraphEnd - 1, End:=.Sections(iSelectedRange).Range.End)
                        oDeleteableParagraphRange.Select
                        .ActiveWindow.Selection.Delete
                        .ActiveWindow.Selection.PageSetup.SectionStart = wdSectionNewPage
                        goLogging.WriteLog "  Type 4a: " & sTargetBMname & " was deleted"
                        DeleteCaption sBMname
                    Else

                        .Sections(iSelectedRange + 2).Range.Select

                        ' wenn der Abschnittwechsel "FORTLAUFEND" ist , dann l�sche nicht
                        ' den Abschnittwechsel vor der zu l�schenden Tabelle, da hinter der
                        ' zu l�schende Tabelle noch weitere zu der �berschrift geh�renden Tabellen folgen
                        If .ActiveWindow.Selection.PageSetup.SectionStart = 0 Then

                            'SETZE DEN VOR DER ZU L�SCHENDEN TAB LIEGENDE ABSCHNITT UND ERMITTLE DAS ENDE DIESES ABSCHNITTES
                            Set oPreviousParagraphRange = .Sections(iSelectedRange - 1).Range
                            oPreviousParagraphRange.Select

                            ' Fall 1a oder Fall 1b: beides getestet und OK
                            If .ActiveWindow.Selection.PageSetup.SectionStart = 0 Then
                                iPreviousParagraphEnd = oPreviousParagraphRange.End
                                Set oDeleteableParagraphRange = .Range(Start:=iPreviousParagraphEnd - 1, End:=.Sections(iSelectedRange).Range.End)

                                'Add an empty char. Else a (protected!) section break will be left over
                                oDeleteableParagraphRange.InsertAfter " "
                                oDeleteableParagraphRange.Select
                                .ActiveWindow.Selection.Delete
                                iDeletedBDcount = iDeletedBDcount + 1
                                If (I = iArrayIndex) Then
                                    goLogging.WriteLog "  Type 1b: " & sTargetBMname & " was deleted"
                                Else
                                    goLogging.WriteLog "  Type 1a: " & sTargetBMname & " was deleted"  ' hier per i = indexarray-1 den log eintrag unterscheiden !
                                End If

                                ' Fall 2a oder Fall 2b: beides getestet und OK
                            Else
                                ' Fall 2b
                                If (I = iArrayIndex) Then      ' Tabelle am Dokumentenende
                                    '
                                    ' L�sche zuerst die �berschrift und setzte dann einen Link zu der vorherigen
                                    '
                                    .ActiveWindow.Selection.GoTo What:=wdGoToBookmark, Name:=sTargetBMname
                                    .ActiveWindow.Selection.StartOf Unit:=wdSection, Extend:=wdMove
                                    iSelectedRange = .ActiveWindow.Selection.Information(wdActiveEndSectionNumber)
                                    .ActiveWindow.ActivePane.View.SeekView = wdSeekCurrentPageHeader
                                    .Sections(iSelectedRange).Headers(wdHeaderFooterPrimary).Range.Tables(2).Range.Select ' ACHTUNG, Tables = Tables(2)!

                                    .ActiveWindow.Selection.Rows.Delete

                                    .Activate
                                    .ActiveWindow.Selection.HeaderFooter.LinkToPrevious = True
                                    .ActiveWindow.ActivePane.View.SeekView = wdSeekMainDocument

                                    ' L�sche die Tabelle
                                    iPreviousParagraphsCount = .Sections.Count
                                    iPreviousParagraphEnd = oPreviousParagraphRange.End
                                    Set oDeleteableParagraphRange = .Range(Start:=iPreviousParagraphEnd - 1, End:=.Sections(iSelectedRange).Range.End)
                                    oDeleteableParagraphRange.Select
                                    .ActiveWindow.Selection.Delete
                                    Call CheckDeletion(iPreviousParagraphsCount)
                                    .ActiveWindow.Selection.PageSetup.SectionStart = wdSectionNewPage
                                    iDeletedBDcount = iDeletedBDcount + 1

                                    ' Setze den "AW neue Seite" auf "fortlaufenden AW"
                                    iSelectedRange = .ActiveWindow.Selection.Information(wdActiveEndSectionNumber)
                                    Set oDeleteableParagraphRange = .Range(Start:=.Sections(iSelectedRange - 1).Range.End - 1, End:=.Sections(iSelectedRange - 1).Range.End)
                                    oDeleteableParagraphRange.Select
                                    .ActiveWindow.Selection.Delete
                                    .ActiveWindow.Selection.PageSetup.SectionStart = wdSectionContinuous
                                    goLogging.WriteLog "  Type 2b: " & sTargetBMname & " was deleted"
                                ' Fall 2a
                                Else
                                    iPreviousParagraphsCount = .Sections.Count
                                    iPreviousParagraphEnd = oPreviousParagraphRange.End
                                    Set oDeleteableParagraphRange = .Range(Start:=iPreviousParagraphEnd - 1, End:=.Sections(iSelectedRange).Range.End)
                                    oDeleteableParagraphRange.Select
                                    .ActiveWindow.Selection.Delete
                                    Call CheckDeletion(iPreviousParagraphsCount)
                                    .ActiveWindow.Selection.PageSetup.SectionStart = wdSectionNewPage
                                    iDeletedBDcount = iDeletedBDcount + 1
                                    goLogging.WriteLog "  Type 2a: " & sTargetBMname & " was deleted"
                                End If
                            End If
                        Else ' d.h der �bern�chste Abschnittwechsel ist ein "AW neue Seite"
                            Set oPreviousParagraphRange = .Sections(iSelectedRange - 1).Range
                            oPreviousParagraphRange.Select

                            ' Fall 3: getestet und OK
                            If .ActiveWindow.Selection.PageSetup.SectionStart = 0 Then
                                iPreviousParagraphEnd = oPreviousParagraphRange.End
                                Set oDeleteableParagraphRange = .Range(Start:=iPreviousParagraphEnd - 1, End:=.Sections(iSelectedRange).Range.End)
                                oDeleteableParagraphRange.Select
                                .ActiveWindow.Selection.Delete
                                iDeletedBDcount = iDeletedBDcount + 1
                                goLogging.WriteLog "  Type 3: " & sTargetBMname & " was deleted"
                            ' Fall 4
                            Else
                                '  Fall 4b: getestet und OK
                                iPreviousParagraphEnd = oPreviousParagraphRange.End
                                Set oDeleteableParagraphRange = .Range(Start:=iPreviousParagraphEnd - 1, End:=.Sections(iSelectedRange).Range.End)
                                oDeleteableParagraphRange.Select
                                .ActiveWindow.Selection.Delete

                                ' �berpr�fe, ob beim L�schen der vorherige Abschnittwechsel "neue Seite" von Word automatisch in einen "fortlaufenden AW"
                                ' umgewandelt wurde. Wenn JA, dann mache aus dem fortl.AW wieder einen AW "neue Seite"
                                If .ActiveWindow.Selection.PageSetup.SectionStart = 0 Then
                                    .ActiveWindow.Selection.PageSetup.SectionStart = wdSectionNewPage
                                End If

                                ' L�sche Abschnittwechsel "Neue Seite" nach dem vom User eingef�gten Text
                                iSelectedRange = .ActiveWindow.Selection.Information(wdActiveEndSectionNumber)
                                Set oDeleteableParagraphRange = .Range(Start:=.Sections(iSelectedRange).Range.End - 1, End:=.Sections(iSelectedRange).Range.End)
                                oDeleteableParagraphRange.Select
                                .ActiveWindow.Selection.Delete
                                iDeletedBDcount = iDeletedBDcount + 1
                                goLogging.WriteLog "  Type 4b: " & sTargetBMname & " was deleted"
                            End If
                        End If
                    End If
                End With 'oWord.Documents(oWordProps.sTargetDocName)
            End If 'oWord.Documents(oWordProps.sSourceDocName).Bookmarks.Exists(sTargetBMname) = False
        Next I

    Else

        I = 2
        Do While I <= iTagetBMcount  'For i = 2 To iTagetBMcount  beginnt mit i=2, um die strukturierte TM selbst auszuschliessen

            sTargetBMname = oStructBMrange.Bookmarks(I).Name

            If oWord.Documents(oWordProps.sSourceDocName).Bookmarks.Exists(sTargetBMname) = False Then
                With oWord.Documents(oWordProps.sTargetDocName)
                    .ActiveWindow.Selection.GoTo What:=wdGoToBookmark, Name:=sTargetBMname
                    iSelectedRange = .ActiveWindow.Selection.Information(wdActiveEndSectionNumber)
                    Set oPreviousParagraphRange = .Sections(iSelectedRange - 1).Range
                    oPreviousParagraphRange.Select
                    iPreviousParagraphEnd = oPreviousParagraphRange.End
                    Set oDeleteableParagraphRange = .Range(Start:=iPreviousParagraphEnd - 1, End:=.Sections(iSelectedRange).Range.End)
                    oDeleteableParagraphRange.Select
                    .ActiveWindow.Selection.Delete
                End With
                I = I - 1
                iTagetBMcount = iTagetBMcount - 1
            End If

            I = I + 1
        Loop
    End If

    goLogging.WriteLog "End  : AlignSourceWithTargetLegacy"

End Sub

'''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
' Sub-Prozedur        : DeleteCaption
' Beschreibung        : L�scht eine? �berschrift aus dem Zieldokument
' Parameter           : -
' wird aufgerufen von : AlignSourceWithTargetLegacy
' �nderungshistorie   :
' Autor  Datum       Grund            Kommentar
' -----  ----------  ---------------  ---------------------------------------
'
'''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
Sub DeleteCaption(sBMname As String)

    Dim iSelectedRange As Integer
    Dim iNumTables As Integer
    Dim iNumBookmarks As Integer

    goLogging.WriteLog "Start: DeleteCaption"

    With oWord.Documents(oWordProps.sTargetDocName)

        .ActiveWindow.Selection.GoTo What:=wdGoToBookmark, Name:=sBMname
        .ActiveWindow.Selection.StartOf Unit:=wdSection, Extend:=wdMove

        iSelectedRange = .ActiveWindow.Selection.Information(wdActiveEndSectionNumber)
        .ActiveWindow.ActivePane.View.SeekView = wdSeekCurrentPageHeader

        'Eventuell verbliebene Bookmarks entfernen
        iNumBookmarks = .Sections(iSelectedRange).Headers(wdHeaderFooterPrimary).Range.Bookmarks.Count
        If iNumBookmarks > 0 Then
            .Sections(iSelectedRange).Headers(wdHeaderFooterPrimary).Range.Bookmarks(1).Select
            .ActiveWindow.Selection.Delete
        End If

        .ActiveWindow.ActivePane.View.SeekView = wdSeekMainDocument

    End With

    goLogging.WriteLog "End  : DeleteCaption"

End Sub

'''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
' Sub-Prozedur        : CheckDeletion
' Beschreibung        : Diese Prozedur �berpr�ft, ob Word den selektierten Abschnitt korrekt gel�scht hat
' Parameter           : iPreviousParagraphCount - Anzahl der Abschnitte
' wird aufgerufen von : AlignSourceWithTargetLegacy
' �nderungshistorie   :
' Autor  Datum       Grund            Kommentar
' -----  ----------  ---------------  ---------------------------------------
'
'''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
Sub CheckDeletion(iPreviousParagraphCount As Long)

    With oWord.Documents(oWordProps.sTargetDocName)

        If iPreviousParagraphCount - 2 <> .Sections.Count Then
            .ActiveWindow.Selection.Delete
            goLogging.WriteLog "   Word did not delete correctly !"
        End If

    End With

End Sub

'''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
' Sub-Prozedur        : CreateWordObject
' Beschreibung        : Creates the oWord object. Used to return an appropriate
'                       error code in case of failure
' Parameter           : -
' wird aufgerufen von : ConvertToWord
' �nderungshistorie   :
' Autor  Datum       Grund            Kommentar
' -----  ----------  ---------------  ---------------------------------------
'
'''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
Sub CreateWordObject()

    On Error GoTo CreateWordObject_Error
    Set oWord = CreateObject("word.Application")
    Exit Sub

CreateWordObject_Error:
    ExitProc (-3)

End Sub

'''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
' Funktion            : CheckForChanges
' Beschreibung        : �berpr�ft, ob sich an der Reihenfolge der Bookmarks etwas ge�ndert hat
' Parameter           : Name der zu �berpr�fenden Strukturierten Bookmark
' R�ckgabewert        : true - Reihenfolge hat sich ge�ndert.
'                       false - Reihenfolge ist gleich geblieben
' �nderungshistorie   : -
' Autor  Datum       Grund            Kommentar
' -----  ----------  ---------------  ---------------------------------------
'
'''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
Function CheckForChanges(sStructBM) As Boolean

    Dim iBMcountSource          As Integer
    Dim iBMcountTarget          As Integer

    Dim oStructBMrangeSource    As Word.Range
    Dim oStructBMrangeTarget    As Word.Range

    Dim sBMNameSource           As String
    Dim sBMNameTarget           As String

    Dim I                       As Integer
    Dim j                       As Integer

    Dim Tcount                  As Integer
    Dim Tcollect()
    Dim iSelectedRange          As Integer
    Dim oHeaderTarget           As Word.Range
    Dim sHeaderBMname           As String

    goLogging.WriteLog "Start: CheckForChanges"


    Set oStructBMrangeSource = oWord.Documents(oWordProps.sSourceDocName).Bookmarks(sStructBM).Range
    Set oStructBMrangeTarget = oWord.Documents(oWordProps.sTargetDocName).Bookmarks(sStructBM).Range

    iBMcountSource = oStructBMrangeSource.Bookmarks.Count
    iBMcountTarget = oStructBMrangeTarget.Bookmarks.Count

    CheckForChanges = False
    j = 0
    Tcount = 0

    ' Alle T Textmarken z�hlen
    For I = 1 To iBMcountTarget
        sBMNameTarget = oStructBMrangeTarget.Bookmarks(I).Name
        If sBMNameTarget Like sBM_TABLE Then
            Tcount = Tcount + 1
        End If
    Next I

    ReDim Tcollect(Tcount)
    Tcount = 0

    ' Bestimmen, ob sich die Reihenfolge der "T_*" Textmarken ver�ndert hat.
    For I = 1 To iBMcountTarget

        sBMNameTarget = oStructBMrangeTarget.Bookmarks(I).Name

        If sBMNameTarget Like sBM_TABLE Then
            Tcount = Tcount + 1
            Tcollect(Tcount) = sBMNameTarget
            Do While j < iBMcountSource
                j = j + 1
                sBMNameSource = oStructBMrangeSource.Bookmarks(j).Name
                If sBMNameSource Like sBM_TABLE Then
                    Exit Do
                End If
            Loop

            If sBMNameSource <> sBMNameTarget Then CheckForChanges = True

        End If

    Next I

    ' Reihenfolge der "T_*" Textmarken hat sich ver�ndert
    If CheckForChanges = True Then

        goLogging.WriteLog "    Order of bookmarks has changed!"

        'Eventuelle References entfernen.
        For I = iBMcountTarget To 1 Step -1
            If oStructBMrangeTarget.Bookmarks(I).Name Like sBM_REF Then
                goLogging.WriteLog "    " & oStructBMrangeTarget.Bookmarks(I).Name & " removed."
                oStructBMrangeTarget.Bookmarks(I).Delete
            End If
        Next I

        For I = 1 To Tcount
            sBMNameTarget = Tcollect(I)

            Set oStructBMrangeTarget = oWord.Documents(oWordProps.sTargetDocName).Bookmarks(sBMNameTarget).Range

            ' Alle Bookmarks durchgehen, und die Bookmarks mit Bookmarks erstetzen, welche den Namen "_DEL" tragen.
            ' Auf diese Weise wird das Dokument nicht leer (was die nachfolgenden Prozeduren nicht vertragen w�rden),
            ' und die mit "*_DEL" markierten Abschnitte werden beim Abgleich entfernt.
            With oWord.Documents(oWordProps.sTargetDocName)

                .ActiveWindow.Selection.GoTo What:=wdGoToBookmark, Name:=sBMNameTarget
                .ActiveWindow.Selection.StartOf Unit:=wdSection, Extend:=wdMove

                oStructBMrangeTarget.Bookmarks.Add sBMNameTarget & "_DEL"
                oWord.Documents(oWordProps.sTargetDocName).Bookmarks(sBMNameTarget).Delete

                goLogging.WriteLog "    Flagged Bookmark for deletion: " & sBMNameTarget & "_DEL"

            End With

        Next I
    End If

    goLogging.WriteLog "End  : CheckForChanges"

End Function

'''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
' Sub-Prozedur        : RemoveEmptyBookmarks
' Beschreibung        : Removes Empty Bookmarks from the document, if the bookmark is not existing
'                       in the set of source files.
' Parameter           : -
' wird aufgerufen von : ConvertToWord
' �nderungshistorie   :
' Autor  Datum       Grund            Kommentar
' -----  ----------  ---------------  ---------------------------------------
'
'''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
Sub RemoveEmptyBookmarks()

    Dim oBookmark  As Bookmark
    Dim bBMfound1  As Boolean
    Dim bBMfound2  As Boolean
    Dim I          As Integer
    Dim iSelectedRange As Integer
    Dim sLog      As String

    goLogging.WriteLog "Start: RemoveEmptyBookmarks"

    For Each oBookmark In oWord.Documents(oWordProps.sTargetDocName).Bookmarks

        'Bekannte Bookmarktypen gleich von der weiteren Pr�fung ausschliessen
        If Not (oBookmark.Name Like sBM_UEBER Or oBookmark.Name Like sBM_TABLE Or oBookmark.Name Like sBM_REF) Then

            bBMfound1 = False

            'Schauen, ob die betreffende Bookmark relevant ist
            For I = 1 To goOptions.iIsrBookmarksNumber
                If oBookmark.Name = goOptions.GetIsrBookmarks(I) Then
                    bBMfound1 = True
                    Exit For
                End If
            Next I

            If bBMfound1 = True Then

                bBMfound2 = False

                'Schauen, ob die aktuelle Bookmark in der Liste der Bl�cke noch vorkommt.
                For I = 1 To goOptions.iFile_Sources
                    If oBookmark.Name = goOptions.GetFile_Bookmark(I) Then
                        bBMfound2 = True
                        Exit For
                    End If
                Next I

                If bBMfound2 = False Then
                    'Bookmark kommt nicht mehr vor, also entfernen

                    sLog = oBookmark.Name & " - " & Len(oBookmark.Range.Text) & " characters. "

                    'Vor dem Entfernen des Bookmarks den Bereich entsch�tzen
                    oBookmark.Select
                    iSelectedRange = oWord.Documents(oWordProps.sTargetDocName).ActiveWindow.Selection.Information(wdActiveEndSectionNumber)
                    oWord.Documents(oWordProps.sTargetDocName).Sections(iSelectedRange).ProtectedForForms = False

                    'Wenn es sich nicht um eine strukturierte Textmarke handelt, auch den Inhalt l�schen
                    If Not oBookmark.Name Like sBM_STRUCT Then
                        oWord.Documents(oWordProps.sTargetDocName).Sections(iSelectedRange).Range.Delete
                    Else
                        oBookmark.Delete
                    End If

                    sLog = "Removed: " & sLog

                    goLogging.WriteLog "  " & sLog

                End If
            End If
        End If

    Next oBookmark

    goLogging.WriteLog "End  : RemoveEmptyBookmarks"

End Sub

'''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
' Funktion            : PreviousLegalBookmark
' Beschreibung        : Ermittelt in einem Range die vorangegangene "legale" Bookmark,
'                       d.h. die �berschrift, strukturierte Tabelle oder Graphic ist.
' Parameter           : oBMrange - der zu untersuchende Range
'                       iNum     - Nummer der aktuellen Bookmark
' R�ckgabewert        : String - Name der Bookmark
' �nderungshistorie   : -
' Autor  Datum       Grund            Kommentar
' -----  ----------  ---------------  ---------------------------------------
'
'''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
Function PreviousLegalBookmark(oBMrange As Word.Range, iNum As Integer, bNoCaption As Boolean) As String

    Dim I       As Integer
    Dim sBMname As String

    I = iNum

    Do
        sBMname = oBMrange.Bookmarks(I).Name

        If (sBMname Like sBM_UEBER And bNoCaption = False) Or sBMname Like sBM_TABLE Or sBMname Like sBM_GRAPHIC Then
            PreviousLegalBookmark = sBMname
        End If
        I = I - 1

    Loop Until PreviousLegalBookmark <> "" Or I < 1

End Function

'''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
' Funktion            : ClearStaticBookmarks
' Beschreibung        : Pr�ft alle Statischen (nicht strukturierten Bookmarks), ob diese in der Liste
'                       der zu verarbeitenden Bookmarks stehen. Wenn nicht: Bookmark leeren.
' Parameter           : -
' R�ckgabewert        : -
' �nderungshistorie   : -
' Autor  Datum       Grund            Kommentar
' -----  ----------  ---------------  ---------------------------------------
'
'''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
Sub ClearStaticBookmarks()

    Dim I               As Integer
    Dim j               As Integer
    Dim bBMfound        As Boolean
    Dim sLog            As String
    Dim oBookmark       As Bookmark
    Dim iSelectedRange  As Integer
    Dim sBM             As String

    If goOptions.bDeactClearStaticBM = False Then

        goLogging.WriteLog "Start: ClearStaticBookmarks"

        'Alle m�glichen Bookmarks betrachten
        For I = 1 To goOptions.iIsrBookmarksNumber

            sBM = goOptions.GetIsrBookmarks(I)

            'Strukturierte sind nicht relevant, die werden von AlignSourceWithTarget abgedeckt
            If Not (sBM Like sBM_STRUCT) Then

                bBMfound = False

                'Mit der Blocksliste abgleichen
                For j = 1 To goOptions.iFile_Sources
                    If sBM = goOptions.GetFile_Bookmark(j) Then
                        bBMfound = True
                        Exit For
                    End If
                Next j

                'Nicht gefunden -> L�schkandidat
                If bBMfound = False Then
                    With oWord.Documents(oWordProps.sTargetDocName)
                        If .Bookmarks.Exists(sBM) = True Then

                            Set oBookmark = .Bookmarks(sBM)

                            sLog = oBookmark.Name & " - " & Len(oBookmark.Range.Text) & " characters. "

                            'Vor dem Entfernen des Inhaltes den Bereich entsch�tzen
                            oBookmark.Select
                            iSelectedRange = .ActiveWindow.Selection.Information(wdActiveEndSectionNumber)
                            .Sections(iSelectedRange).ProtectedForForms = False

                            'Bookmark + Inhalt l�schen
                            .ActiveWindow.Selection.TypeText Text:="  "
                            .ActiveWindow.Selection.MoveLeft Unit:=wdCharacter, Count:=2, Extend:=wdExtend
                            'Bookmark an dieser Stelle frisch anlegen
                            .ActiveWindow.Selection.Bookmarks.Add sBM, Range:=.ActiveWindow.Selection.Range

                            sLog = "Cleared: " & sLog
                            goLogging.WriteLog "  " & sLog

                        End If
                    End With
                End If
            End If
        Next I
        goLogging.WriteLog "End  : ClearStaticBookmarks"
    Else
        goLogging.WriteLog "Skip : ClearStaticBookmarks"
    End If

End Sub

