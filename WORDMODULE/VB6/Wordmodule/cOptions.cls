VERSION 1.0 CLASS
BEGIN
  MultiUse = -1  'True
  Persistable = 0  'NotPersistable
  DataBindingBehavior = 0  'vbNone
  DataSourceBehavior  = 0  'vbNone
  MTSTransactionMode  = 0  'NotAnMTSObject
END
Attribute VB_Name = "cOptions"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = True
Attribute VB_PredeclaredId = False
Attribute VB_Exposed = False
Attribute VB_Ext_KEY = "SavedWithClassBuilder6" ,"Yes"
Attribute VB_Ext_KEY = "Top_Level" ,"Yes"
'''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
' Klasse            : cOptions
' Beschreibung      : Stellt die Optionen zur Verf�gung
' Autor             : ms
' Datum             : 19.03.2004
' �nderungshistorie :
' Autor  Datum       Grund            Kommentar
' -----  ----------  ---------------  ---------------------------------------
'
'''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
Option Explicit

Private mvarsOption             As String
Private mvarsTemplate           As String
Private mvarsFile_DocTarget     As String
Private mvarsPassword_source    As String
Private mvarsPassword_target    As String
Private mvaraFile_Source()      As String
Private mvaraFile_Bookmark()    As String
Private mvaraFile_protected()   As String
Private mvaraFile_setCaption()  As String
Private mvariFile_Sources       As Integer
Private mvarbUnprotected        As Boolean
Private mvarbRemoveEmptyBM      As Boolean
Private mvarbDeactClearStaticBM As Boolean
Private mvarsUseProtection      As String
Private mvarsConfigPath         As String

Private mvariDocProperties      As Integer
Private aDocPropertiesName()    As String
Private aDocPropertiesValue()   As String
Private iMSXMLver               As Integer
Private iOrder                  As Integer

Private mvaraIsrBookmarks()     As String
Private mvariIsrBookmarks       As Integer

'Private oConfigDOM              As DOMDocument
Private oConfigDOM              As Object

Public Property Get sConfigPath() As String
    sConfigPath = mvarsConfigPath
End Property

Public Property Get iDocProperties() As Integer
    iDocProperties = mvariDocProperties
End Property

Public Property Get sOption() As String
    sOption = mvarsOption
End Property

Public Property Get sTemplate() As String
    sTemplate = mvarsTemplate
End Property

Public Property Get sFile_DocTarget() As String
    sFile_DocTarget = mvarsFile_DocTarget
End Property

Public Property Get sPassword_target() As String
    sPassword_target = mvarsPassword_target
End Property

Public Property Get sPassword_source() As String
    sPassword_source = mvarsPassword_source
End Property

Function GetDocPropertyName(iNum As Integer) As Variant
    GetDocPropertyName = aDocPropertiesName(iNum)
End Function

Function GetDocPropertyValue(iNum As Integer) As Variant
    GetDocPropertyValue = aDocPropertiesValue(iNum)
End Function

Function GetFile_Source(iNum As Integer) As Variant
    GetFile_Source = mvaraFile_Source(iNum)
End Function

Function GetFile_Protected(iNum As Integer) As Variant
    GetFile_Protected = mvaraFile_protected(iNum)
End Function

Function GetFile_SetCaption(iNum As Integer) As Variant
    GetFile_SetCaption = mvaraFile_setCaption(iNum)
End Function

Function GetFile_Bookmark(iNum As Integer) As Variant
    GetFile_Bookmark = mvaraFile_Bookmark(iNum)
End Function

Public Property Get iFile_Sources() As Integer
    iFile_Sources = mvariFile_Sources
End Property

Public Property Get bUnprotected() As Boolean
    bUnprotected = mvarbUnprotected
End Property

Public Property Get bRemoveEmptyBM() As Boolean
    bRemoveEmptyBM = mvarbRemoveEmptyBM
End Property

Public Property Get bDeactClearStaticBM() As Boolean
    bDeactClearStaticBM = mvarbDeactClearStaticBM
End Property

Public Property Get sUseProtection() As String
    sUseProtection = mvarsUseProtection
End Property

Function GetIsrBookmarks(iNum As Integer) As Variant
    GetIsrBookmarks = mvaraIsrBookmarks(iNum)
End Function

Public Property Get iIsrBookmarksNumber() As Integer
    iIsrBookmarksNumber = mvariIsrBookmarks
End Property

'''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
' Funktion            : ParseArgs
' Beschreibung        : Ruft den Parser auf und verteilt die Befehlszeilenargumente
'                       in die entsprechenden Properties.
'                       Startet die Interpretation der XML-Config.
' Parameter           : sArgs (String) - die Befehlszeilenargumente
' wird aufgerufen von : modMain.Main
' R�ckgabewert        : true - Befehlszeile konnte korrekt interpretiert werden
'                       false - Beim Interpretieren der Befehlszeile ist eine Unstimmigkeit aufgetreten
' �nderungshistorie   :
' Autor  Datum       Grund            Kommentar
' -----  ----------  ---------------  ---------------------------------------
'
'''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
Public Function ParseArgs(sArgs As String) As Boolean
    
    Dim aArgs()     As String
    Dim iNumArgs    As Integer
   
    If sArgs = "" Then
        'Offenbar keine Parameter angegeben, default annehmen
        sArgs = "config.xml"
    End If
   
    'Options in ein Feld transferieren
    aArgs = SplitArgs(sArgs)
       
    'ConfigDOM in einer eigenen Subroutine initialisieren -> zum Abfangen bei Nichtexistenz
    SetConfigDOM
        
    oConfigDOM.setProperty "SelectionLanguage", "XPath"
     
    iNumArgs = UBound(aArgs) - LBound(aArgs)
            
    mvarsConfigPath = ExtractPath(aArgs(0))
    
    If iNumArgs > 0 Then
        If Val(aArgs(1)) Then
            iOrder = Val(aArgs(1))
        End If
    Else
        iOrder = 1
    End If
            
    'sArgs enth�lt den Pfad+Configfile
    If oConfigDOM.Load(aArgs(0)) = False Then
        MsgBoxCheck "ParseArgs: Failed to load xml config file from '" + mvarsConfigPath + "'."
        ParseArgs = False
        Exit Function
    End If
     
    If ReadArgs <> "" Then
        MsgBoxCheck "ParseArgs: Failed to load xml config data from file - " & ReadArgs
        ParseArgs = False
        Exit Function
    End If
     
    ParseArgs = True
   
End Function

'''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
' Funktion            : SplitArgs
' Beschreibung        : Verteilt die Befehlszeilenargumente in ein Array.
'                       Filenamen in QuotMarks werden korrekt ber�cksichtigt.
' Parameter           : sArgs (String) - die Befehlszeilenargumente
' wird aufgerufen von : .ParseArgs
' R�ckgabewert        : String() - enth�lt die aufgesplittete Befehlszeile
' �nderungshistorie   :
' Autor  Datum       Grund            Kommentar
' -----  ----------  ---------------  ---------------------------------------
'
'''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
Private Function SplitArgs(sArgs As String) As String()

    Dim aTempArgs()     As String
    Dim aArgs()         As String
    Dim iTempNumArgs    As Integer
    Dim iNumArgs        As Integer
    Dim bIsFilename     As Boolean
    Dim I               As Integer
            
    bIsFilename = False

    'Argumentstring in ein Array einzelner Worte aufsplitten
    aTempArgs = Split(sArgs, " ", -1)
    
    'Anzahl der Elemente im Array bestimmen (funktioniert nur bei eindimensionalen Arrays!)
    iTempNumArgs = UBound(aTempArgs) - LBound(aTempArgs)

    If iTempNumArgs = -1 Then Exit Function

    ReDim aArgs(iTempNumArgs)

    'Um Filenamen, die in Quotmarks stehen, verarbeiten zu k�nnen, das gesplittete Feld in einem
    'neuen Feld entsprechend wieder zusammenbauen.
    For I = 0 To iTempNumArgs
        If Trim(aTempArgs(I)) = "" Then
            'Leerzeichen -> ignore
        ElseIf Right(aTempArgs(I), 1) = Chr(34) Then
            bIsFilename = False
            aArgs(iNumArgs) = aArgs(iNumArgs) & aTempArgs(I)
            iNumArgs = iNumArgs + 1
        ElseIf Left(aTempArgs(I), 1) = Chr(34) Or bIsFilename = True Then
            bIsFilename = True
            aArgs(iNumArgs) = aArgs(iNumArgs) & aTempArgs(I) & " "
        ElseIf bIsFilename = False Then
            aArgs(iNumArgs) = aTempArgs(I)
            iNumArgs = iNumArgs + 1
        End If
    Next I
    
    ' Gr��e des Datenfeldes neu festlegen
    ReDim Preserve aArgs(iNumArgs - 1)

    SplitArgs = aArgs

End Function

'''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
' Sub-Prozedur        : WriteOptionsToLog
' Beschreibung        : Schreibt das Ergebnis der Optionsauswertung ins Log.
'                       goLogging sollte zur Verf�gung stehen, und die Optionen
'                       sollten bereits interpretiert sein.
' Parameter           : -
' wird aufgerufen von : modMain.Main
' �nderungshistorie   :
' Autor  Datum       Grund            Kommentar
' -----  ----------  ---------------  ---------------------------------------
'
'''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
Sub WriteOptionsToLog()

    If mvarsOption = "" Or goLogging Is Nothing Then Exit Sub
    
    goLogging.WriteLog "  Option          = " & mvarsOption
    
    Select Case mvarsOption
        Case "create"
            goLogging.WriteLog "  Template        = " & mvarsTemplate
            goLogging.WriteLog "  Password_source = " & mvarsPassword_source
            goLogging.WriteLog "  Password_target = " & mvarsPassword_target
            goLogging.WriteLog "  File_DocTarget  = " & mvarsFile_DocTarget
        Case "view"
            goLogging.WriteLog "  File_DocTarget  = " & mvarsFile_DocTarget
        Case "lockview"
            goLogging.WriteLog "  File_DocTarget  = " & mvarsFile_DocTarget
            goLogging.WriteLog "  Password_source = " & mvarsPassword_source
        Case "lock"
            goLogging.WriteLog "  File_DocTarget  = " & mvarsFile_DocTarget
            goLogging.WriteLog "  Password_source = " & mvarsPassword_source
    End Select

End Sub

'''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
' Funktion            : ReadArgs
' Beschreibung        : Liest die Optionen aus dem XML-DOM aus
'                       und weisst diese den Member-Variablen zu.
' Parameter           : -
' wird aufgerufen von :
' R�ckgabewert        : true  - wenn der angegebene Task erfolgreich in die Optionen geladen wurde
'                       false - wenn das Laden fehlgeschlagen ist, d.h. es diesen Task nicht gibt
' �nderungshistorie   :
' Autor  Datum       Grund            Kommentar
' -----  ----------  ---------------  ---------------------------------------
'
'''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
Private Function ReadArgs() As String

'    Dim oRoot       As IXMLDOMElement
'    Dim oChilds     As IXMLDOMNodeList
'    Dim oChild      As IXMLDOMNode
'    Dim oNode       As IXMLDOMNode
'    Dim oProps      As IXMLDOMNodeList
'    Dim oFiles      As IXMLDOMNodeList
    Dim oRoot       As Object
    Dim oChilds     As Object
    Dim oChild      As Object
    Dim oNode       As Object
    Dim oProps      As Object
    Dim oFiles      As Object

    Dim sCommand    As String
            
    Dim sFullpath   As String
    Dim sFullpathTarget As String
        
    ReadArgs = ""
    
    Set oRoot = oConfigDOM.documentElement

    Set oChilds = oRoot.ChildNodes
    Set oChild = oChilds.Item(0) 'Item z�hlt ab 0

    'Wenn oChild nothing ist, ist was faul
    If oChild Is Nothing Then
        ReadArgs = "No XML childs found."
        Exit Function
    End If

    goLogging.WriteLog oChild.nodeName
    
    If iOrder = 0 Then
        Set oChild = oChild.SelectSingleNode("app[@name='wordmodul']")
    Else
        Set oChild = oChild.SelectSingleNode("app[@name='wordmodul' and @order=" & iOrder & "]")
    End If

    'Wenn oChild nothing ist, ist schon wieder was faul
    If oChild Is Nothing Then
        ReadArgs = "No wordmodul app entry found in xml."
        Exit Function
    End If

    If Not oChild.SelectSingleNode("sourcepassword") Is Nothing Then
        mvarsPassword_source = oChild.SelectSingleNode("sourcepassword").Text
    End If
        
    If Not oChild.SelectSingleNode("targetpassword") Is Nothing Then
        mvarsPassword_target = oChild.SelectSingleNode("targetpassword").Text
    End If

    If Not oChild.SelectSingleNode("../fullpathname") Is Nothing Then
        sFullpath = oChild.SelectSingleNode("../fullpathname").Text
    End If
    
    If Not oChild.SelectSingleNode("../target") Is Nothing Then
        If Right(sFullpath, 1) = "\" Then
            sFullpathTarget = sFullpath & oChild.SelectSingleNode("../target").Text
        Else
            If sFullpathTarget = "" Then
                sFullpathTarget = App.Path & "\" & oChild.SelectSingleNode("../target").Text
            Else
                sFullpathTarget = sFullpath & "\" & oChild.SelectSingleNode("../target").Text
            End If
        End If
    End If

    If Not oChild.SelectSingleNode("target") Is Nothing Then
        mvarsFile_DocTarget = CheckPath(oChild.SelectSingleNode("target").Text)
    Else
        If sFullpathTarget <> "" Then
            mvarsFile_DocTarget = sFullpathTarget
        Else
            ReadArgs = "No target found."
        End If
    End If

    If Not oChild.SelectSingleNode("mode") Is Nothing Then
        If Left(oChild.SelectSingleNode("mode").Text, 1) = "T" Then
            mvarbUnprotected = True
        Else
            mvarbUnprotected = False
        End If
    Else
        mvarbUnprotected = False
    End If

    If Not oChild.SelectSingleNode("removeNonReferencedBookmarks") Is Nothing Then
        If Left(oChild.SelectSingleNode("removeNonReferencedBookmarks").Text, 1) = "T" Then
            mvarbRemoveEmptyBM = True
        Else
            mvarbRemoveEmptyBM = False
        End If
    Else
        mvarbRemoveEmptyBM = False
    End If

    If Not oChild.SelectSingleNode("deactivateClearStaticBookmarks") Is Nothing Then
        If Left(oChild.SelectSingleNode("deactivateClearStaticBookmarks").Text, 1) = "T" Then
            mvarbDeactClearStaticBM = True
        Else
            mvarbDeactClearStaticBM = False
        End If
    Else
        mvarbDeactClearStaticBM = False
    End If

    If Not oChild.SelectSingleNode("use-protection") Is Nothing Then
        If Left(oChild.SelectSingleNode("use-protection").Text, 1) = "T" Then
            mvarsUseProtection = "true"
        Else
            mvarsUseProtection = "false"
        End If
    Else
        mvarsUseProtection = "not_available"
    End If

    If Not oChild.SelectSingleNode("command") Is Nothing Then
        
        Select Case oChild.SelectSingleNode("command").Text
        
            Case "CAN_COMPARE_DOCUMENTS" 'CAN_COMPARE_DOCUMENTS
                mvarsOption = "compare"
            
                If Not oChild.SelectSingleNode("template") Is Nothing Then
                    mvarsTemplate = CheckPath(oChild.SelectSingleNode("template").Text)
                Else
                    ReadArgs = "No template entry found in app."
                End If
                
            Case "CAN_CHECKIN" 'CAN_CHECKIN
                mvarsOption = "checkin"
                
            Case "CAN_REFRESH_DOC_PROPERTIES" 'CAN_REFRESH_DOC_PROPERTIES
                mvarsOption = "refereshproperties"
        
                If Not oChild.SelectSingleNode("template") Is Nothing Then
                    mvarsTemplate = CheckPath(oChild.SelectSingleNode("template").Text)
                Else
                    ReadArgs = "No template entry found in app."
                End If
        
            Case "CAN_CHECKOUT" 'CAN_CHECKOUT
                mvarsOption = "view"
                
                ' das ist ein hack! In der config.xml geht an dieser Stelle alles schief.
                mvarsFile_DocTarget = sFullpathTarget
        
            Case "CAN_DISPLAY_REPORT" 'CAN_DISPLAY_REPORT
                mvarsOption = "lockview"
                
                'If mvarsPassword_source = "" Then ReadArgs = "No source passowrd found."
               
            Case "CAN_RUN_REPORT" 'CAN_RUN_REPORT
                
                ' Entscheiden, um welche Option es sich handelt
                mvarsOption = "create"
                If Not oChild.SelectSingleNode("properties/custom-property[@name = 'REPSTATUS']") Is Nothing Then
                    If oChild.SelectSingleNode("properties/custom-property[@name = 'REPSTATUS']").Text = 3 Then
                        mvarsOption = "finalize"
                    End If
                End If
                                
                If Not oChild.SelectSingleNode("template") Is Nothing Then
                    mvarsTemplate = CheckPath(oChild.SelectSingleNode("template").Text)
                Else
                    ReadArgs = "No template entry found in app."
                End If
                              
                ' Laden aller Bl�cke.
                If Not oChild.SelectSingleNode("blocks") Is Nothing Then
                    Set oFiles = oChild.SelectSingleNode("blocks").ChildNodes
                
                    For Each oNode In oFiles
                        'Feldgr�sse den Gegebenheiten anpassen
                        If mvariFile_Sources Mod 10 = 0 Then
                            ReDim Preserve mvaraFile_Source(mvariFile_Sources + 10)
                            ReDim Preserve mvaraFile_Bookmark(mvariFile_Sources + 10)
                            ReDim Preserve mvaraFile_protected(mvariFile_Sources + 10)
                            ReDim Preserve mvaraFile_setCaption(mvariFile_Sources + 10)
                        End If
                        
                        mvariFile_Sources = mvariFile_Sources + 1
                        mvaraFile_Source(mvariFile_Sources) = CheckPath(oNode.Text)
                        mvaraFile_Bookmark(mvariFile_Sources) = oNode.Attributes.getNamedItem("name").Text
                        
                        If Not oNode.Attributes.getNamedItem("protected") Is Nothing Then
                            mvaraFile_protected(mvariFile_Sources) = oNode.Attributes.getNamedItem("protected").Text
                        End If
                        
                        If Not oNode.Attributes.getNamedItem("setcaption") Is Nothing Then
                            mvaraFile_setCaption(mvariFile_Sources) = oNode.Attributes.getNamedItem("setcaption").Text
                        Else
                            mvaraFile_setCaption(mvariFile_Sources) = "false"
                        End If
                        
                    Next oNode
                End If
                
                ' Laden aller Bookmarks.
                If Not oChild.SelectSingleNode("isr-bookmarks") Is Nothing Then
                    Set oFiles = oChild.SelectSingleNode("isr-bookmarks").ChildNodes
                
                    For Each oNode In oFiles
                        
                        'Feldgr�sse den Gegebenheiten anpassen
                        If mvariIsrBookmarks Mod 10 = 0 Then
                            ReDim Preserve mvaraIsrBookmarks(mvariIsrBookmarks + 10)
                        End If
                        
                        mvariIsrBookmarks = mvariIsrBookmarks + 1
                        mvaraIsrBookmarks(mvariIsrBookmarks) = oNode.Text
                    
                    Next oNode
                End If
                
                If mvariFile_Sources = 0 And mvarsOption = "create" Then
                    ReadArgs = "No source files (htmls) found."
                End If
                                
            Case Else
                MsgBoxCheck "ReadArgs: Illegal Command: " & oChild.SelectSingleNode("command").Text
                ReadArgs = "Illegal Command."
                Exit Function
                            
        End Select
    Else
        ReadArgs = "No command found."
    End If

    'Properties auslesen
    mvariDocProperties = 0
    
    Set oProps = GetCustomProperties
    
    If Not oProps Is Nothing Then
        For Each oNode In oProps
        
            'Feldgr�sse den Gegebenheiten anpassen
            If mvariDocProperties Mod 10 = 0 Then
                ReDim Preserve aDocPropertiesName(mvariDocProperties + 10)
                ReDim Preserve aDocPropertiesValue(mvariDocProperties + 10)
            End If

            mvariDocProperties = mvariDocProperties + 1
            
            aDocPropertiesName(mvariDocProperties) = oNode.Attributes.getNamedItem("name").Text
            
            'Replace vbCrLf Tokens
            aDocPropertiesValue(mvariDocProperties) = Replace(oNode.Text, "\r\n", vbCrLf)
            
        Next oNode
    Else
        If mvarsOption = "lock" Or mvarsOption = "create" Then
            ReadArgs = False
        End If
    End If

    'MD5 "Verschl�sselung"
    If mvarsPassword_source <> "" Then
        mvarsPassword_source = Left(goMD5.MD5(mvarsPassword_source), 8)
    End If
    If mvarsPassword_target <> "" Then
        mvarsPassword_target = Left(goMD5.MD5(mvarsPassword_target), 8)
    End If

End Function

'''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
' Funktion            : GetCustomProperties
' Beschreibung        : Liefert eine Nodelist zur�ck, welche alle Custom Properties
'                       des aktuellen Tasks enth�lt.
' Parameter           : -
' wird aufgerufen von :
' R�ckgabewert        : IXMLDOMNodeList - Nodelist mit den aktuellen Custom Properties
'                       Nothing - wenn es keine Nodeliste gibt, z.B. bei der Option "view"
' �nderungshistorie   :
' Autor  Datum       Grund            Kommentar
' -----  ----------  ---------------  ---------------------------------------
'
'''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
'Function GetCustomProperties() As IXMLDOMNodeList
Function GetCustomProperties() As Object

    Dim sXpath As String
    sXpath = "/config/apps/app[@name='wordmodul' and @order=" & iOrder & "]/properties"

    'Pr�fen, ob die Node vorhanden ist.
    If Not oConfigDOM.SelectSingleNode(sXpath) Is Nothing Then
        Set GetCustomProperties = oConfigDOM.SelectSingleNode(sXpath).ChildNodes
    Else
        Set GetCustomProperties = Nothing
    End If
    
End Function

'''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
' Funktion            : CheckPath
' Beschreibung        : Checks if there is an "\" in the path, else the Path
'                       to the Config will be added
' Parameter           : The filename with or without path
' wird aufgerufen von :
' R�ckgabewert        : File with path
' �nderungshistorie   :
' Autor  Datum       Grund            Kommentar
' -----  ----------  ---------------  ---------------------------------------
'
'''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
Function CheckPath(sFile) As String

    Dim sTemp As String

    If Not InStr(sFile, "\") > 0 Then
        CheckPath = mvarsConfigPath & "\" & sFile
    Else
        CheckPath = sFile
    End If

End Function

'''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
' Sub-Prozedur        : SetConfigDOM
' Beschreibung        : Looks up the version of MSXML and sets iMSXMLver accordingly
'                       (or exits if there is no MSXML found)
' Parameter           : -
' wird aufgerufen von :
' �nderungshistorie   :
' Autor  Datum       Grund            Kommentar
' -----  ----------  ---------------  ---------------------------------------
'
'''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
Sub SetConfigDOM()

    On Error Resume Next
    
    Set oConfigDOM = CreateObject("MSXML2.DOMDocument.7.0")
    If oConfigDOM Is Nothing Then
        Set oConfigDOM = CreateObject("MSXML2.DOMDocument.6.0")
        If oConfigDOM Is Nothing Then
            Set oConfigDOM = CreateObject("MSXML2.DOMDocument.5.0")
            If oConfigDOM Is Nothing Then
                Set oConfigDOM = CreateObject("MSXML2.DOMDocument.4.0")
                If oConfigDOM Is Nothing Then
                    Set oConfigDOM = CreateObject("MSXML2.DOMDocument.3.0")
                    If oConfigDOM Is Nothing Then
                        Set oConfigDOM = CreateObject("MSXML2.DOMDocument.2.6")
                        If oConfigDOM Is Nothing Then
                            Set oConfigDOM = CreateObject("MSXML.DOMDocument")
                            If oConfigDOM Is Nothing Then
                                ExitProc (-1)
                            Else
                                ExitProc (-2)
                            End If
                        Else
                            iMSXMLver = 26
                        End If
                    Else
                        iMSXMLver = 30
                    End If
                Else
                    iMSXMLver = 40
                End If
            Else
                iMSXMLver = 50
            End If
        Else
            iMSXMLver = 60
        End If
    Else
        iMSXMLver = 70
    End If

End Sub

