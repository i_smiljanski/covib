Attribute VB_Name = "modError"
'''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
' Modul             : modError
' Beschreibung      : Hilfsfunktionen für das Abhandeln von aufgetretenen Fehlern
' Autor             : ms
' Datum             : 20.04.2004
' Änderungshistorie :
' Autor  Datum       Grund            Kommentar
' -----  ----------  ---------------  ---------------------------------------
'
'''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
Option Explicit

'''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
' Sub-Prozedur        : SaveDoc
' Beschreibung        : Speichert das unvollständige Dokument im temporären Verzeichnis
' Parameter           : sDocName - Name des unvollständigen Dokumentes
'                       sWord    - das Word.Application-Objekt, in dessem Kontext sich sDocName bedinfet
' wird aufgerufen von : modWord
' Änderungshistorie   :
' Autor  Datum       Grund            Kommentar
' -----  ----------  ---------------  ---------------------------------------
'
'''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
Sub SaveDoc(sDocName As String, ByRef oWord As Word.Application)

    Dim sErrorDoc As String

    ' Speichere die Datei bei einem Fehler
    
    sErrorDoc = CreateTempFile("ERR")
    sErrorDoc = Left(sErrorDoc, Len(sErrorDoc) - 3) & "doc"
    
    'oWord.ChangeFileOpenDirectory GetTemp
    oWord.Documents(sDocName).SaveAs _
        FileName:=sErrorDoc, _
        FileFormat:=wdFormatDocument, _
        LockComments:=False, Password:="", _
        AddToRecentFiles:=True, _
        WritePassword:="", _
        ReadOnlyRecommended:=False, _
        EmbedTrueTypeFonts:=False, _
        SaveNativePictureFormat:=False, _
        SaveFormsData:=False, _
        SaveAsAOCELetter:=False

    oWord.Quit wdDoNotSaveChanges

End Sub

'''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
' Funktion            : CreateTempFile
' Beschreibung        : Gibt Namen und Pfad einer Datei im temporären Pfad zurück
' Parameter           : sPrefix - Ein Prefix, der an den Anfang des Dateinamens gesetzt wird (max. 3 Zeichen)
' wird aufgerufen von : SaveDoc
' Rückgabewert        : String Pfad und Name einer Datei im temporären Pfad zurück
' Änderungshistorie   :
' Autor  Datum       Grund            Kommentar
' -----  ----------  ---------------  ---------------------------------------
'
'''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
Private Function CreateTempFile(sPrefix As String) As String

    Dim sBuffer As String
    Dim sTempPath As String
    Dim lReturn As Long

    sTempPath = GetTemp()

    sBuffer = Space(iLENGTH)
    lReturn = GetTempFileName(sTempPath, Left$(sPrefix, 3), 0&, sBuffer)

    If lReturn <> 0 Then
        sBuffer = Left(sBuffer, InStr(sBuffer, Chr$(0)) - 1)
        Kill sBuffer
    End If

    CreateTempFile = sBuffer

End Function

'''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
' Sub-Prozedur        : WriteErrorToLog
' Beschreibung        : Schreibt die Beschreibung des Fehlers in die Log-Datei
' Parameter           : sDesc - Beschreibung des Fehlers (Err.Description)
'                       iNum  - Fehlernummer (Err.Number)
'                       iErl  - Zeile, in welcher der Fehler aufgetreten ist
'                               (vorausgesetzt der Sourccode is mit Zeilennummern versehen)
' wird aufgerufen von : überall
' Änderungshistorie   :
' Autor  Datum       Grund            Kommentar
' -----  ----------  ---------------  ---------------------------------------
'
'''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
Sub WriteErrorToLog(sDesc As String, iNum As Long, iErl As Long)

    goLogging.WriteLog "*** Fehler aufgetreten! "
    goLogging.WriteLog "    Fehlerbeschreibung: " & sDesc
    goLogging.WriteLog "    Fehlernummer      : " & iNum
    goLogging.WriteLog "    Fehlerzeile       : " & iErl
    goLogging.WriteLog "***"
    goLogging.CloseLog

End Sub


'''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
' Sub-Prozedur        : ExitProc
' Beschreibung        : Verlässt das Programm mittels ExitProcess.
'                       In erster linie zu Debugging-Zwecken eingeführt, da ein ExitProcess
'                       gleich noch VB mitreisst. =)
' Parameter           : iNum - der Rückgabewert, mit dem das Programm verlassen werden soll.
' wird aufgerufen von : überall
' Änderungshistorie   :
' Autor  Datum       Grund            Kommentar
' -----  ----------  ---------------  ---------------------------------------
'
'''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
Public Sub ExitProc(iNum As Integer)

    If Not bDEBUG Then
        ExitProcess iNum
    End If

    End

End Sub


