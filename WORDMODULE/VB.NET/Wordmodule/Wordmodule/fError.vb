﻿Imports System.IO

Public Class fError

    Private iTimeout = 10

    Private Sub fError_Load(sender As System.Object, e As System.EventArgs) Handles MyBase.Load

        Me.Text = Me.Text & " " & System.Diagnostics.FileVersionInfo.GetVersionInfo(System.Reflection.Assembly.GetExecutingAssembly.Location).FileVersion & " (" & cCompilationDate & ")"
        UpdateTimer()
        oTimeout.Start()

    End Sub

    Public Sub setMessage(sMessage As String)

        txtErrorMessage.Text = String.Empty
        txtErrorMessage.Text = txtErrorMessage.Text & "Wordmodule " & System.Diagnostics.FileVersionInfo.GetVersionInfo(System.Reflection.Assembly.GetExecutingAssembly.Location).FileVersion & " (" & cCompilationDate & ")" & vbCrLf
        If gVersionWindows <> String.Empty And gVersionWord <> String.Empty Then
            txtErrorMessage.Text = txtErrorMessage.Text & "Windows " & gVersionWindows & " - Microsoft Word " & gVersionWord & vbCrLf
        End If
        txtErrorMessage.Text = txtErrorMessage.Text & vbCrLf & sMessage
        Me.Refresh()

        ' Write an error file
        File.AppendAllText(goOptions.sConfigPath & "\" & sConfigAppName & ".error", txtErrorMessage.Text)

    End Sub

    Private Sub oTimeout_Tick(sender As Object, e As System.EventArgs) Handles oTimeout.Tick

        iTimeout = iTimeout - 1

        If iTimeout = -1 Then
            Me.Close()
        End If

        UpdateTimer()

    End Sub

    Private Sub lTimeOutCounter_Click(sender As System.Object, e As System.EventArgs) Handles lTimeOutCounter.Click

        oTimeout.Stop()
        lTimeOutCounter.Text = ""

    End Sub

    Private Sub UpdateTimer()

        lTimeOutCounter.Text = "Closing in " & iTimeout & " sec. Click here to stop timeout."
        Me.Refresh()

    End Sub

    Private Sub btnLogToClipboard_Click(sender As System.Object, e As System.EventArgs) Handles btnLogToClipboard.Click

        Clipboard.SetText(goLogging.GetWholeLog)
        btnLogToClipboard.Text = btnLogToClipboard.Text

    End Sub

End Class