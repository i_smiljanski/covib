﻿Imports System.Diagnostics
Imports System.Collections
Imports System.Text

Public Class WordmoduleListener
    Inherits TraceListener

    Private q As Queue

    Public Sub New()
        q = New Queue()
    End Sub

    Public Overloads Overrides Sub Write(ByVal Message As String)
        q.Enqueue(Message)
    End Sub

    Public Overloads Overrides Sub WriteLine(ByVal Message As String)
        q.Enqueue(Message + Environment.NewLine)
    End Sub

    Public ReadOnly Property Text() As String
        Get
            Return GetText()
        End Get
    End Property

    Private Function GetText() As String

        Dim S As StringBuilder = New StringBuilder()
        Dim E As IEnumerator = q.GetEnumerator

        While (E.MoveNext)
            S.Append(CType(E.Current, String))
        End While

        q.Clear()
        Return S.ToString()

    End Function

End Class
