﻿Imports Microsoft.Office.Interop

Public Class cWordProps

    '''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
    ' Klasse            : cWordProps
    ' Beschreibung      : Nimmt diverse Eigenschaften der Word-Dokumente auf.
    ' Autor             : ms
    ' Datum             : 20.04.2004
    ' Änderungshistorie :
    ' Autor  Datum       Grund            Kommentar
    ' -----  ----------  ---------------  ---------------------------------------
    '
    '''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''

    'Private oWord As Word.Application
    Private oWord As Object

    Private aStructBM()
    Private aDocInfoText()
    Private aDocInfoValue()

    Private mvariStructBM As Integer 'j
    Private mvariDocInfo As Integer 'z

    Private mvarsWordVersion As String

    Private mvarbPaginationSetting As Boolean
    Private mvarbConversionSetting As Boolean

    Private mvardocSource As Word.Document
    Private mvardocTarget As Word.Document

    Public ReadOnly Property sWordVersion() As String
        Get
            Return mvarsWordVersion
        End Get
    End Property

    Public Property docTarget() As Word.Document
        Get
            Return mvardocTarget
        End Get
        Set(ByVal obj As Word.Document)
            mvardocTarget = obj
        End Set
    End Property

    Public Property docSource() As Word.Document
        Get
            Return mvardocSource
        End Get
        Set(ByVal obj As Word.Document)
            mvardocSource = obj
        End Set
    End Property

    Public Property bConversionSetting() As Boolean
        Get
            Return mvarbConversionSetting
        End Get
        Set(ByVal vData As Boolean)
            mvarbConversionSetting = vData
        End Set
    End Property

    Public Property bPaginationSetting() As Boolean
        Get
            Return mvarbPaginationSetting
        End Get
        Set(ByVal vData As Boolean)
            mvarbPaginationSetting = vData
        End Set
    End Property

    Public ReadOnly Property iDocInfo() As Integer
        Get
            Return mvariDocInfo
        End Get
    End Property

    Public ReadOnly Property iStructBM() As Integer
        Get
            Return mvariStructBM
        End Get

    End Property

    Function GetStructBM(iNum As Integer) As Object
        GetStructBM = aStructBM(iNum)
    End Function

    Function GetDocInfoText(iNum As Integer) As Object
        GetDocInfoText = aDocInfoText(iNum)
    End Function

    Function GetDocInfoValue(iNum As Integer) As Object
        GetDocInfoValue = aDocInfoValue(iNum)
    End Function

    '''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
    ' Funktion            : SetStructBM
    ' Beschreibung        : Weisst der StructBM einen neuen Eintrag zu und erhöht den Zähler
    ' Parameter           : vValue - Zu speichernder Wert
    ' wird aufgerufen von : modWord.GetUserDefinedProperties
    ' Rückgabewert        : -
    ' Änderungshistorie   :
    ' Autor  Datum       Grund            Kommentar
    ' -----  ----------  ---------------  ---------------------------------------
    '
    '''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
    Sub SetStructBM(vValue As Object)

        If mvariStructBM Mod 10 = 0 Then
            ReDim Preserve aStructBM(mvariStructBM + 10)
        End If

        mvariStructBM = mvariStructBM + 1

        aStructBM(mvariStructBM) = vValue

    End Sub

    '''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
    ' Funktion            : SetDocInfo
    ' Beschreibung        : Weisst der DocInfo einen neuen Eintrag (bestehnd aus Text und Wert) zu
    '                       und erhöht den Zähler
    ' Parameter           : vText  - zu speichernder Text
    '                       vValue - Zu speichernder Wert
    ' wird aufgerufen von : modWord.GetUserDefinedProperties
    ' Rückgabewert        : -
    ' Änderungshistorie   :
    ' Autor  Datum       Grund            Kommentar
    ' -----  ----------  ---------------  ---------------------------------------
    '
    '''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
    Sub SetDocInfo(vText As Object, vValue As Object)

        If mvariDocInfo Mod 10 = 0 Then
            ReDim Preserve aDocInfoText(mvariDocInfo + 10)
            ReDim Preserve aDocInfoValue(mvariDocInfo + 10)
        End If

        mvariDocInfo = mvariDocInfo + 1

        aDocInfoText(mvariDocInfo) = vText
        aDocInfoValue(mvariDocInfo) = vValue

    End Sub

    '''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
    ' Sub-Prozedur        : InitWordProps
    ' Beschreibung        : Initalisiert die Instanz des Objekts mit Defaultwerten
    '                       quasi der Konstruktor, wenns den in VB geben würde. =)
    ' Parameter           : oWrd - das Word-Objekt
    ' wird aufgerufen von : modWord.ConvertToWord
    ' Änderungshistorie   : -
    ' Autor  Datum       Grund            Kommentar
    ' -----  ----------  ---------------  ---------------------------------------
    '
    '''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
    Sub InitWordProps(oWrd As Word.Application)

        oWord = oWrd

        ' Defaultwerte festlegen
        mvariStructBM = 0
        mvariDocInfo = 0

        ' Version feststellen
        mvarsWordVersion = oWord.Application.Version

        ' Word Application Option Eigenschaften merken
        mvarbPaginationSetting = oWord.Options.Pagination
        mvarbConversionSetting = oWord.Options.ConfirmConversions

    End Sub

    '''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
    ' Sub-Prozedur        : ReleaseWordProps
    ' Beschreibung        : Entfernt lediglich den Verweis auf das Word-Objekt.
    '                       Quasi der Destruktor
    ' Parameter           : -
    ' wird aufgerufen von : modWord.ConvertToWord
    ' Änderungshistorie   : -
    ' Autor  Datum       Grund            Kommentar
    ' -----  ----------  ---------------  ---------------------------------------
    '
    '''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
    Sub ReleaseWordProps()

        oWord = Nothing

    End Sub


End Class
