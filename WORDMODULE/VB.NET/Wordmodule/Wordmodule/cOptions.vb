﻿Imports System.Security.Cryptography
Imports System.Text
Imports System.Xml.XPath
Imports System.IO
Imports Microsoft.Office.Interop

'''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
' Klasse            : cOptions
' Beschreibung      : Stellt die Optionen zur Verfügung
' Autor             : ms
' Datum             : 19.03.2004
' Änderungshistorie :
' Autor  Datum       Grund            Kommentar
' -----  ----------  ---------------  ---------------------------------------
'
'''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''

Public Class cOptions

    Private mvarsOption As String
    Private mvarsTemplate As String
    Private mvarsFile_DocTarget As String
    Private mvarsTemplate_Extension As String
    Private mvarsFile_DocTarget_Extension As String
    Private mvarsPassword_source As String
    Private mvarsPassword_target As String
    Private mvaraFile_Source() As String
    Private mvaraFile_Bookmark() As String
    Private mvaraFile_protected() As String
    Private mvaraFile_setCaption() As String
    Private mvariFile_Sources As Integer
    Private mvarbUnprotected As Boolean
    Private mvarbRemoveEmptyBM As Boolean
    Private mvarbDeactClearStaticBM As Boolean
    Private mvarsUseProtection As String
    Private mvarsConfigPath As String
    Private mvarsSaveFileFormat As Integer
    Private mvarbError As Boolean

    Private mvariDocProperties As Integer
    Private aDocPropertiesName() As String
    Private aDocPropertiesValue() As String
    Private iMSXMLver As Integer
    Private iOrder As Integer

    Private mvaraIsrBookmarks() As String
    Private mvariIsrBookmarks As Integer

    Private sConfigFilename = "config.xml"

    Private oConfigDOM As Object

    Public ReadOnly Property sConfigPath() As String
        Get
            Return mvarsConfigPath
        End Get
    End Property

    Public ReadOnly Property iDocProperties() As Integer
        Get
            Return mvariDocProperties
        End Get
    End Property

    Public ReadOnly Property sOption() As String
        Get
            Return mvarsOption
        End Get
    End Property

    Public ReadOnly Property sTemplate() As String
        Get
            Return mvarsTemplate
        End Get
    End Property

    Public ReadOnly Property sTemplate_Extension() As String
        Get
            Return mvarsTemplate_Extension
        End Get
    End Property

    Public ReadOnly Property sFile_DocTarget() As String
        Get
            Return mvarsFile_DocTarget
        End Get
    End Property

    Public ReadOnly Property sFile_DocTarget_Extension() As String
        Get
            Return mvarsFile_DocTarget_Extension
        End Get
    End Property

    Public ReadOnly Property sPassword_target() As String
        Get
            Return mvarsPassword_target
        End Get
    End Property

    Public ReadOnly Property sPassword_source() As String
        Get
            Return mvarsPassword_source
        End Get
    End Property

    Public ReadOnly Property iFile_Sources() As Integer
        Get
            Return mvariFile_Sources
        End Get
    End Property

    Public Property bUnprotected() As Boolean
        Get
            Return mvarbUnprotected
        End Get
        Set(value As Boolean)

        End Set
    End Property

    Public ReadOnly Property bRemoveEmptyBM() As Boolean
        Get
            Return mvarbRemoveEmptyBM
        End Get
    End Property

    Public Property bDeactClearStaticBM() As Boolean
        Get
            Return mvarbDeactClearStaticBM
        End Get
        Set(value As Boolean)

        End Set
    End Property

    Public ReadOnly Property sUseProtection() As String
        Get
            Return mvarsUseProtection
        End Get
    End Property

    Public ReadOnly Property iIsrBookmarksNumber() As Integer
        Get
            Return mvariIsrBookmarks
        End Get
    End Property

    Public ReadOnly Property iSaveFileFormat() As Integer
        Get
            Return mvarsSaveFileFormat
        End Get
    End Property

    Public ReadOnly Property bError() As Boolean
        Get
            Return mvarbError
        End Get
    End Property

    Function GetDocPropertyName(iNum As Integer) As Object
        Return aDocPropertiesName(iNum)
    End Function

    Function GetDocPropertyValue(iNum As Integer) As Object
        Return aDocPropertiesValue(iNum)
    End Function

    Function GetFile_Source(iNum As Integer) As Object
        Return mvaraFile_Source(iNum)
    End Function

    Function GetFile_Protected(iNum As Integer) As Object
        Return mvaraFile_protected(iNum)
    End Function

    Function GetFile_SetCaption(iNum As Integer) As Object
        Return mvaraFile_setCaption(iNum)
    End Function

    Function GetFile_Bookmark(iNum As Integer) As Object
        Return mvaraFile_Bookmark(iNum)
    End Function

    Function GetIsrBookmarks(iNum As Integer) As Object
        Return mvaraIsrBookmarks(iNum)
    End Function

    '''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
    ' Funktion            : ParseArgs
    ' Beschreibung        : Ruft den Parser auf und verteilt die Befehlszeilenargumente
    '                       in die entsprechenden Properties.
    '                       Startet die Interpretation der XML-Config.
    ' Parameter           : sArgs (String) - die Befehlszeilenargumente
    ' wird aufgerufen von : modMain.Main
    ' Rückgabewert        : true - Befehlszeile konnte korrekt interpretiert werden
    '                       false - Beim Interpretieren der Befehlszeile ist eine Unstimmigkeit aufgetreten
    ' Änderungshistorie   :
    ' Autor  Datum       Grund            Kommentar
    ' -----  ----------  ---------------  ---------------------------------------
    '
    '''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
    Public Function ParseArgs() As Boolean

        If My.Application.CommandLineArgs.Count > 0 Then
            mvarsConfigPath = Path.GetDirectoryName(My.Application.CommandLineArgs(0))
            sConfigFilename = Path.GetFileName(My.Application.CommandLineArgs(0))
        Else
            goLogging.WriteLog("No config file parameter given. Trying default config.xml.")
        End If

        If String.IsNullOrWhiteSpace(mvarsConfigPath) Then mvarsConfigPath = Application.StartupPath

        If My.Application.CommandLineArgs.Count > 1 Then
            iOrder = Convert.ToInt32(My.Application.CommandLineArgs(1))
        Else
            goLogging.WriteLog("No order parameter given. Trying default 1.")
            iOrder = 1 'Default
        End If

        ' Distribute values from config to mvars
        If ReadArgs() = False Then
            MsgBoxCheck("ParseArgs: Failed to load xml config data from file.")
            Return False
        End If

        Return True

    End Function

    '''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
    ' Sub-Prozedur        : WriteOptionsToLog
    ' Beschreibung        : Schreibt das Ergebnis der Optionsauswertung ins Log.
    '                       goLogging sollte zur Verfügung stehen, und die Optionen
    '                       sollten bereits interpretiert sein.
    ' Parameter           : -
    ' wird aufgerufen von : modMain.Main
    ' Änderungshistorie   :
    ' Autor  Datum       Grund            Kommentar
    ' -----  ----------  ---------------  ---------------------------------------
    '
    '''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
    Sub WriteOptionsToLog()

        If mvarsOption = "" Or goLogging Is Nothing Then Exit Sub

        goLogging.WriteLog("  Option          = " & mvarsOption)

        Select Case mvarsOption
            Case "create"
                goLogging.WriteLog("  Template        = " & mvarsTemplate)
                goLogging.WriteLog("  Password_source = " & mvarsPassword_source)
                goLogging.WriteLog("  Password_target = " & mvarsPassword_target)
                goLogging.WriteLog("  File_DocTarget  = " & mvarsFile_DocTarget)
            Case "view"
                goLogging.WriteLog("  File_DocTarget  = " & mvarsFile_DocTarget)
            Case "lockview"
                goLogging.WriteLog("  File_DocTarget  = " & mvarsFile_DocTarget)
                goLogging.WriteLog("  Password_source = " & mvarsPassword_source)
            Case "lock"
                goLogging.WriteLog("  File_DocTarget  = " & mvarsFile_DocTarget)
                goLogging.WriteLog("  Password_source = " & mvarsPassword_source)
        End Select

    End Sub

    '''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
    ' Funktion            : ReadArgs
    ' Beschreibung        : Liest die Optionen aus dem XML-DOM aus
    '                       und weisst diese den Member-Variablen zu.
    ' Parameter           : -
    ' wird aufgerufen von :
    ' Rückgabewert        : true  - wenn der angegebene Task erfolgreich in die Optionen geladen wurde
    '                       false - wenn das Laden fehlgeschlagen ist, d.h. es diesen Task nicht gibt
    ' Änderungshistorie   :
    ' Autor  Datum       Grund            Kommentar
    ' -----  ----------  ---------------  ---------------------------------------
    '
    '''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
    Private Function ReadArgs() As Boolean

        Dim sFullpath As String = ""
        Dim sFullpathTarget As String = ""
        Dim sConfig As String

        Dim oXpathDoc As XPathDocument
        Dim oXmlNav As XPathNavigator
        Dim oXmlNav2 As XPathNavigator
        Dim oResult As Object

        Dim sXpathBase As String = "/config/apps/app[@name='wordmodul' and @order='" & iOrder & "' ]/"

        Try
            sConfig = mvarsConfigPath & "\" & sConfigFilename
            oXpathDoc = New XPathDocument(sConfig)
            oXmlNav = oXpathDoc.CreateNavigator()
        Catch ex As XPathException
            HandleException(ex, "ReadConfigFile, XMLException")
            Return False
        Catch ex As Exception
            HandleException(ex, "ReadConfigFile")
            Return False
        End Try

        'Check path
        oResult = oXmlNav.SelectSingleNode(sXpathBase & "*")
        If oResult Is Nothing Then
            Return False
        End If

        'Read Fullpath
        oResult = oXmlNav.SelectSingleNode(sXpathBase & "../fullpathname")
        If Not oResult Is Nothing Then
            If oResult.ToString.EndsWith("\") Then
                sFullpath = oResult.ToString
            Else
                sFullpath = oResult.ToString & "\"
            End If

            oTrace.TraceEvent(TraceEventType.Information, 11, "sFullpath = " & sFullpath)
        End If

        'Read sFullpathTarget
        oResult = oXmlNav.SelectSingleNode(sXpathBase & "../target")
        If Not oResult Is Nothing Then
            If oResult.ToString = String.Empty Then
                sFullpathTarget = Environment.CurrentDirectory & "\" & oResult.ToString
            Else
                sFullpathTarget = sFullpath & oResult.ToString
            End If
            oTrace.TraceEvent(TraceEventType.Information, 11, "sFullpathTarget = " & sFullpathTarget)
        End If

        'Read mvarsFile_DocTarget
        oResult = oXmlNav.SelectSingleNode(sXpathBase & "target")
        If Not oResult Is Nothing Then
            If Not oResult.ToString = String.Empty Then
                mvarsFile_DocTarget = CheckPath(oResult.ToString)
            Else
                If Not sFullpathTarget = String.Empty Then
                    mvarsFile_DocTarget = sFullpathTarget
                Else
                    ReadArgs = "No target found."
                End If
            End If
            oTrace.TraceEvent(TraceEventType.Information, 11, "mvarsFile_DocTarget = " & mvarsFile_DocTarget)
        Else
            If Not sFullpathTarget = String.Empty Then
                mvarsFile_DocTarget = sFullpathTarget
            Else
                ReadArgs = "No target found."
            End If
            oTrace.TraceEvent(TraceEventType.Information, 11, "mvarsFile_DocTarget = " & mvarsFile_DocTarget)
        End If

        'Read mvarsPassword_source
        oResult = oXmlNav.SelectSingleNode(sXpathBase & "sourcepassword")
        If Not oResult Is Nothing Then
            mvarsPassword_source = oResult.ToString
            oTrace.TraceEvent(TraceEventType.Information, 11, "mvarsPassword_source   = " & mvarsPassword_source)
        End If

        'Read mvarsPassword_source
        oResult = oXmlNav.SelectSingleNode(sXpathBase & "targetpassword")
        If Not oResult Is Nothing Then
            mvarsPassword_target = oResult.ToString
            oTrace.TraceEvent(TraceEventType.Information, 11, "mvarsPassword_target   = " & mvarsPassword_target)
        End If

        'Read mvarbUnprotected
        oResult = oXmlNav.SelectSingleNode(sXpathBase & "mode")
        If Not oResult Is Nothing Then
            If Left(oResult.ToString, 1) = "T" Then
                mvarbUnprotected = True
            Else
                mvarbUnprotected = False
            End If
        Else
            mvarbUnprotected = False
        End If
        oTrace.TraceEvent(TraceEventType.Information, 11, "mvarbUnprotected   = " & mvarbUnprotected)

        'Read mvarbRemoveEmptyBM
        oResult = oXmlNav.SelectSingleNode(sXpathBase & "removeNonReferencedBookmarks")
        If Not oResult Is Nothing Then
            If Left(oResult.ToString, 1) = "T" Then
                mvarbRemoveEmptyBM = True
            Else
                mvarbRemoveEmptyBM = False
            End If
        Else
            mvarbRemoveEmptyBM = False
        End If
        oTrace.TraceEvent(TraceEventType.Information, 11, "mvarbRemoveEmptyBM   = " & mvarbRemoveEmptyBM)

        'Read mvarbDeactClearStaticBM
        oResult = oXmlNav.SelectSingleNode(sXpathBase & "deactivateClearStaticBookmarks")
        If Not oResult Is Nothing Then
            If Left(oResult.ToString, 1) = "T" Then
                mvarbDeactClearStaticBM = True
            Else
                mvarbDeactClearStaticBM = False
            End If
        Else
            mvarbDeactClearStaticBM = False
        End If
        oTrace.TraceEvent(TraceEventType.Information, 11, "mvarbDeactClearStaticBM   = " & mvarbDeactClearStaticBM)

        'Read mvarsUseProtection
        oResult = oXmlNav.SelectSingleNode(sXpathBase & "use-protection")
        If Not oResult Is Nothing Then
            If Left(oResult.ToString, 1) = "T" Then
                mvarsUseProtection = "true"
            Else
                mvarsUseProtection = "false"
            End If
        Else
            mvarsUseProtection = "not_available"
        End If
        oTrace.TraceEvent(TraceEventType.Information, 11, "mvarsUseProtection   = " & mvarsUseProtection)

        'Read mvarbError
        oResult = oXmlNav.SelectSingleNode(sXpathBase & "error")
        If Not oResult Is Nothing Then
            If Left(oResult.ToString, 1) = "T" Then
                mvarbError = True
            Else
                mvarbError = False
            End If
        Else
            mvarbError = False
        End If
        oTrace.TraceEvent(TraceEventType.Information, 11, "mvarbError   = " & mvarbError)

        'Read mvarsOption
        oResult = oXmlNav.SelectSingleNode(sXpathBase & "command")
        If Not oResult Is Nothing Then

            Select Case oResult.ToString

                Case "CAN_COMPARE_DOCUMENTS" 'CAN_COMPARE_DOCUMENTS
                    mvarsOption = "compare"

                    oResult = oXmlNav.SelectSingleNode(sXpathBase & "template")
                    If Not oResult Is Nothing Then
                        mvarsTemplate = CheckPath(oResult.ToString)
                    Else
                        ReadArgs = "No template entry found in app."
                    End If


                Case "CAN_CHECKIN" 'CAN_CHECKIN
                    mvarsOption = "checkin"

                Case "CAN_REFRESH_DOC_PROPERTIES" 'CAN_REFRESH_DOC_PROPERTIES
                    mvarsOption = "refereshproperties"

                    oResult = oXmlNav.SelectSingleNode(sXpathBase & "template")
                    If Not oResult Is Nothing Then
                        mvarsTemplate = CheckPath(oResult.ToString)
                    Else
                        Return False
                    End If

                Case "CAN_CHECKOUT" 'CAN_CHECKOUT
                    mvarsOption = "view"

                    ' das ist ein hack! In der config.xml geht an dieser Stelle alles schief.
                    mvarsFile_DocTarget = sFullpathTarget

                Case "CAN_DISPLAY_REPORT" 'CAN_DISPLAY_REPORT
                    mvarsOption = "lockview"

                Case "CAN_RUN_REPORT" 'CAN_RUN_REPORT

                    ' Entscheiden, um welche Option es sich handelt
                    mvarsOption = "create"

                    oResult = oXmlNav.SelectSingleNode(sXpathBase & "properties/custom-property[@name = 'REPSTATUS']")
                    If Not oResult Is Nothing Then
                        If Not oResult.ToString = String.Empty Then
                            If oResult.ToString = 3 Then
                                mvarsOption = "finalize"
                            End If
                        End If
                    End If

                    oResult = oXmlNav.SelectSingleNode(sXpathBase & "template")
                    If Not oResult Is Nothing Then
                        mvarsTemplate = CheckPath(oResult.ToString)
                    Else
                        Return False
                    End If

                    ' Laden aller Blöcke.
                    oResult = oXmlNav.Select(sXpathBase & "blocks/block")
                    If Not oResult Is Nothing Then

                        'Feldgrösse anpassen
                        ReDim mvaraFile_Source(oResult.count)
                        ReDim mvaraFile_Bookmark(oResult.count)
                        ReDim mvaraFile_protected(oResult.count)
                        ReDim mvaraFile_setCaption(oResult.count)

                        For Each oXmlNav2 In oResult
                            mvariFile_Sources = mvariFile_Sources + 1
                            mvaraFile_Source(mvariFile_Sources) = CheckPath(oXmlNav2.ToString)
                            mvaraFile_Bookmark(mvariFile_Sources) = oXmlNav2.GetAttribute("name", "")

                            If Not oXmlNav2.GetAttribute("protected", "") = String.Empty Then
                                mvaraFile_protected(mvariFile_Sources) = oXmlNav2.GetAttribute("protected", "")
                            End If

                            If Not oXmlNav2.GetAttribute("setcaption", "") = String.Empty Then
                                mvaraFile_setCaption(mvariFile_Sources) = oXmlNav2.GetAttribute("setcaption", "")
                            Else
                                mvaraFile_setCaption(mvariFile_Sources) = "false"
                            End If

                        Next oXmlNav2

                    End If

                    ' Laden aller Bookmarks.
                    oResult = oXmlNav.Select(sXpathBase & "isr-bookmarks/bookmarks")

                    If Not oResult Is Nothing Then

                        'Feldgrösse anpassen

                        ReDim mvaraIsrBookmarks(oResult.count)

                        For Each oXmlNav2 In oResult
                            mvariIsrBookmarks = mvariIsrBookmarks + 1
                            mvaraIsrBookmarks(mvariIsrBookmarks) = oXmlNav2.ToString
                        Next oXmlNav2

                    End If

                    If mvariFile_Sources = 0 And mvarsOption = "create" Then
                        Return False
                    End If

                Case Else
                    MsgBoxCheck("ReadArgs: Illegal Command: " & oResult.ToString)
                    Return False

            End Select
        Else
            Return False
        End If

        'Properties auslesen
        mvariDocProperties = 0

        oResult = oXmlNav.Select(sXpathBase & "properties/custom-property")
        If Not oResult Is Nothing Then

            ReDim aDocPropertiesName(oResult.count)
            ReDim aDocPropertiesValue(oResult.count)

            For Each oXmlNav2 In oResult

                mvariDocProperties = mvariDocProperties + 1

                'Include replacing of vbCrLf Tokens
                aDocPropertiesName(mvariDocProperties) = oXmlNav2.GetAttribute("name", "")

                'Replace vbCrLf Tokens
                aDocPropertiesValue(mvariDocProperties) = Replace(oXmlNav2.ToString, "\r\n", vbCrLf)

            Next oXmlNav2
        Else
            If mvarsOption = "lock" Or mvarsOption = "create" Then
                Return False
            End If
        End If

        'MD5 "Verschlüsselung"
        Using md5Hash As MD5 = MD5.Create()

            If mvarsPassword_source <> "" Then
                mvarsPassword_source = Left(GetMd5Hash(md5Hash, mvarsPassword_source), 8)
            End If
            If mvarsPassword_target <> "" Then
                mvarsPassword_target = Left(GetMd5Hash(md5Hash, mvarsPassword_target), 8)
            End If
        End Using

        'Determine Extensions 
        mvarsTemplate_Extension = Path.GetExtension(mvarsTemplate)
        mvarsFile_DocTarget_Extension = Path.GetExtension(mvarsFile_DocTarget)

        'Determine to save doc or docx
        If mvarsFile_DocTarget_Extension.ToLower = ".docx" Then
            mvarsSaveFileFormat = Word.WdSaveFormat.wdFormatXMLDocument
        Else
            mvarsSaveFileFormat = Word.WdSaveFormat.wdFormatDocument
        End If

        Return True

    End Function

    '''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
    ' Funktion            : GetCustomProperties
    ' Beschreibung        : Liefert eine Nodelist zurück, welche alle Custom Properties
    '                       des aktuellen Tasks enthält.
    ' Parameter           : -
    ' wird aufgerufen von :
    ' Rückgabewert        : IXMLDOMNodeList - Nodelist mit den aktuellen Custom Properties
    '                       Nothing - wenn es keine Nodeliste gibt, z.B. bei der Option "view"
    ' Änderungshistorie   :
    ' Autor  Datum       Grund            Kommentar
    ' -----  ----------  ---------------  ---------------------------------------
    '
    '''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
    'Function GetCustomProperties() As IXMLDOMNodeList
    Function GetCustomProperties() As Object

        Dim sXpath As String = "/config/apps/app[@name='wordmodul' and @order=" & iOrder & "]/properties"

        'Prüfen, ob die Node vorhanden ist.
        If Not oConfigDOM.SelectSingleNode(sXpath) Is Nothing Then
            GetCustomProperties = oConfigDOM.SelectSingleNode(sXpath).ChildNodes
        Else
            GetCustomProperties = Nothing
        End If

    End Function

    '''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
    ' Funktion            : CheckPath
    ' Beschreibung        : Checks if there is an "\" in the path, else the Path
    '                       to the Config will be added
    ' Parameter           : The filename with or without path
    ' wird aufgerufen von :
    ' Rückgabewert        : File with path
    ' Änderungshistorie   :
    ' Autor  Datum       Grund            Kommentar
    ' -----  ----------  ---------------  ---------------------------------------
    '
    '''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
    Function CheckPath(sFile) As String

        If Not InStr(sFile.ToString, "\") > 0 Then
            CheckPath = mvarsConfigPath & "\" & sFile.ToString
        Else
            CheckPath = sFile.ToString
        End If

    End Function

    Shared Function GetMd5Hash(ByVal md5Hash As MD5, ByVal input As String) As String

        ' Convert the input string to a byte array and compute the hash.
        Dim data As Byte() = md5Hash.ComputeHash(Encoding.UTF8.GetBytes(input))

        ' Create a new Stringbuilder to collect the bytes
        ' and create a string.
        Dim sBuilder As New StringBuilder()

        ' Loop through each byte of the hashed data 
        ' and format each one as a hexadecimal string.
        Dim i As Integer
        For i = 0 To data.Length - 1
            sBuilder.Append(data(i).ToString("x2"))
        Next i

        ' Return the hexadecimal string.
        Return sBuilder.ToString()

    End Function 'GetMd5Hash

End Class
