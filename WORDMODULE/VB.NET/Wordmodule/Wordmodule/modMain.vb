﻿' <summary>The Main function, and some global utility routines for error handling.</summary>

Imports System
Imports System.Security.Cryptography
Imports System.Text
Imports Microsoft.Office.Interop
Imports System.IO

Module modMain

    Private Declare Auto Function GetConsoleWindow Lib "kernel32.dll" () As IntPtr
    Private Declare Auto Function ShowWindow Lib "user32.dll" (ByVal hWnd As IntPtr, ByVal nCmdShow As Integer) As Boolean

    Private Const SW_HIDE As Integer = 0
    Private Const SW_SHOW As Integer = 5

    Private fError As fError

    Public oTrace As TraceSource = New TraceSource("WordModule")

    ''' <summary>
    ''' Name of the application as known in iSR
    ''' </summary>
    Public sConfigAppName As String = "wordmodul"

    ''' <summary>
    ''' Here we go.
    ''' </summary>
    Function Main() As Integer

        Dim hWndConsole As Integer
        Dim iReturn As Integer = 0

        'Hide the console
        hWndConsole = GetConsoleWindow()
        ShowWindow(hWndConsole, SW_HIDE)

        ' Determine if the program rund in the IDE (set bDEBUG to true) or standlone.
        bDEBUG = Debugger.IsAttached

        'Instanciate global objects
        goOptions = New cOptions
        goLogging = New cLogging

        'Parse the command line
        If goOptions.ParseArgs = False Then
            Return -1
        End If

        goLogging.OpenLog(goOptions.sConfigPath & "\" & Path.GetFileName(goOptions.sFile_DocTarget))  'Optionen -a und -z

        'Write the informations from the command line to the log
        goLogging.WriteLog("Commandline Options: " & Command())
        goOptions.WriteOptionsToLog()
        goLogging.WriteLog("  App.path        = " & Application.StartupPath)

        If bDEBUG = False Then

            Try

                'Create an error if requested
                If goOptions.bError = True Then Throw New System.Exception("Error Exception raised ON PURPOSE!")

                If goOptions.sOption = "view" Or _
                   goOptions.sOption = "lock" Or _
                   goOptions.sOption = "lockview" Or _
                   goOptions.sOption = "finalize" Or _
                   goOptions.sOption = "refereshproperties" Or _
                   goOptions.sOption = "checkin" Then

                    '(flavour of) Display the document 
                    Call ViewDoc()

                ElseIf goOptions.sOption = "compare" Then

                    Call CompareDocument()

                Else

                    'Show form with progress bar
                    gfMain = New fMain
                    gfMain.Show()

                    'Convert to Wort
                    Call ConvertToWord()

                End If
            Catch ex As Exception

                iReturn = -3
                goLogging.WriteLog("Error while creating document:" & ex.ToString)

                If Not oWord Is Nothing Then
                    CloseWord()
                End If

                fError = New fError
                fError.setMessage(ex.ToString)
                fError.ShowDialog()

            End Try

        Else
            'Let the IDE do the error handling
            If goOptions.sOption = "view" Or _
               goOptions.sOption = "lock" Or _
               goOptions.sOption = "lockview" Or _
               goOptions.sOption = "finalize" Or _
               goOptions.sOption = "refereshproperties" Or _
               goOptions.sOption = "checkin" Then

                '(flavour of) Display the document 
                Call ViewDoc()

            ElseIf goOptions.sOption = "compare" Then

                Call CompareDocument()

            Else

                'Show form with progress bar
                gfMain = New fMain
                gfMain.Show()

                'Convert to Wort
                Call ConvertToWord()

            End If
        End If

        'End Logging
        goLogging.CloseLog()

        'Clean up
        goLogging = Nothing
        goOptions = Nothing

        'End of program
        Return iReturn

    End Function

    ''' <summary>
    ''' Handles the Options lock/view
    ''' for display, locked display or locked save of the word document
    ''' </summary>
    Sub ViewDoc()

        Dim oWord As Word.Application
        Dim iSectionCount As Integer
        Dim iSectionCounter As Integer
        Dim sDocName As String

        goLogging.WriteLog("Instanciating Word Object...")
        oWord = CreateObject("Word.Application")

        'Sollte schon visible = false stehen, aber zur sicherheit...
        goLogging.WriteLog("Hide Word...")
        oWord.Visible = False

        goLogging.WriteLog("Handling Option: " & goOptions.sOption)
        If goOptions.sOption = "view" Then 'Bearbeiten->Dokument auschecken

            goLogging.WriteLog("Open File " & goOptions.sFile_DocTarget)
            oWord.Application.Documents.Open(FileName:=goOptions.sFile_DocTarget)

            goLogging.WriteLog("Fetch the name of the document...")
            sDocName = oWord.ActiveDocument.Name

            'Ausdrücklich in wdPrintView wechseln (für Word 2013)
            CheckPrintView(oWord.ActiveDocument)

            'Dokument komplett entschützen falls notwendig
            goLogging.WriteLog("Word Protection Type: " & oWord.ActiveDocument.ProtectionType)
            If oWord.ActiveDocument.ProtectionType <> Word.WdProtectionType.wdNoProtection Then

                goLogging.WriteLog("Case true")

                goLogging.WriteLog("Unprotect the Document")
                If oWord.Documents(sDocName).ProtectionType <> Word.WdProtectionType.wdNoProtection Then
                    oWord.ActiveDocument.Unprotect(Password:=goOptions.sPassword_source)
                End If

                ' Dokumenteigenschaften in das Zieldokument einfügen
                goLogging.WriteLog("Writing DocProperties...")
                Call WriteDocProperties(oWord.ActiveDocument.Name, oWord, False, True)

                ' Dokument aktualisieren
                goLogging.WriteLog("UpdateDoc...")
                Call UpdateDoc(oWord.ActiveDocument.Name, oWord)

                '...und schützen
                goLogging.WriteLog("Protecting the Document...")
                If goOptions.sUseProtection = "true" Or goOptions.sUseProtection = "not_available" Then
                    oWord.ActiveDocument.Protect(Password:=goOptions.sPassword_target, NoReset:=False, Type:=Word.WdProtectionType.wdAllowOnlyFormFields)
                End If

            Else

                goLogging.WriteLog("Case false")

                ' Dokumenteigenschaften in das Zieldokument einfügen
                goLogging.WriteLog("Writing DocProperties...")
                Call WriteDocProperties(oWord.ActiveDocument.Name, oWord, False, True)

                ' Dokument aktualisieren
                goLogging.WriteLog("UpdateDoc...")
                Call UpdateDoc(oWord.ActiveDocument.Name, oWord)

                '...und schützen
                If goOptions.sUseProtection = "true" Then
                    goLogging.WriteLog("Protecting the Document...")
                    oWord.ActiveDocument.Protect(Password:=goOptions.sPassword_target, NoReset:=False, Type:=Word.WdProtectionType.wdAllowOnlyFormFields)
                End If

            End If

            goLogging.WriteLog("Saving the Document...")
            'sicherstellen, dass das Dokument wirklich gespeichert wird
            oWord.Documents(goOptions.sFile_DocTarget).Saved = False
            oWord.Documents(goOptions.sFile_DocTarget).Save()

            goLogging.WriteLog("Quit Word...")
            oWord.Quit(Word.WdSaveOptions.wdDoNotSaveChanges)

        ElseIf Left(goOptions.sOption, 4) = "lock" Then

            'Dokument öffnen und Namen merken
            goLogging.WriteLog("Opening the document...")
            If goOptions.sOption = "lockview" Then
                oWord.Application.Documents.Open(FileName:=goOptions.sFile_DocTarget)
            Else
                oWord.Application.Documents.Open(FileName:=goOptions.sTemplate)
            End If

            goLogging.WriteLog("Fetch the name of the document...")
            sDocName = oWord.ActiveDocument.Name

            'Ausdrücklich in wdPrintView wechseln (für Word 2013)
            CheckPrintView(oWord.ActiveDocument)

            If goOptions.bUnprotected = False Then
                'Dokument komplett entschützen...
                goLogging.WriteLog("Unprotecting the document...")
                If oWord.Documents(sDocName).ProtectionType <> Word.WdProtectionType.wdNoProtection Then
                    oWord.Documents(sDocName).Unprotect(Password:=goOptions.sPassword_source)
                End If
            End If

            If goOptions.bUnprotected = False Or goOptions.sOption = "lockview" Then
                'Dann alle Sections für das Protect vorbereiten...
                goLogging.WriteLog("Preparing all sections for protection...")
                iSectionCount = oWord.Documents(sDocName).Sections.Count
                For iSectionCounter = 1 To iSectionCount
                    oWord.Documents(sDocName).Sections(iSectionCounter).ProtectedForForms = True
                Next iSectionCounter
            End If

            ' Dokument aktualisieren
            goLogging.WriteLog("UpdateDoc...")
            Call UpdateDoc(oWord.ActiveDocument.Name, oWord)

            If goOptions.bUnprotected = False Or goOptions.sOption = "lockview" Then
                '...und schützen
                goLogging.WriteLog("Protecting the Document...")
                oWord.Documents(sDocName).Protect(Password:=goOptions.sPassword_target, NoReset:=False, Type:=Word.WdProtectionType.wdAllowOnlyFormFields)
            End If

            'Schliesslich das Dokument in der neuen Form speichern
            goLogging.WriteLog("Saving the Document...")
            oWord.Documents(sDocName).SaveAs(FileName:=goOptions.sFile_DocTarget, FileFormat:=goOptions.iSaveFileFormat, _
                                            LockComments:=False, Password:="", AddToRecentFiles:=True, _
                                            WritePassword:="", ReadOnlyRecommended:=False, EmbedTrueTypeFonts:=False, _
                                            SaveNativePictureFormat:=False, SaveFormsData:=False, SaveAsAOCELetter:=False)

            goLogging.WriteLog("Quit Word...")
            oWord.Quit(Word.WdSaveOptions.wdDoNotSaveChanges)


        ElseIf goOptions.sOption = "finalize" Or goOptions.sOption = "refereshproperties" Then

            goLogging.WriteLog("Opening the document...")
            oWord.Application.Documents.Open(FileName:=goOptions.sTemplate)

            goLogging.WriteLog("Fetch the name of the document...")
            sDocName = oWord.ActiveDocument.Name

            'Ausdrücklich in wdPrintView wechseln (für Word 2013)
            CheckPrintView(oWord.ActiveDocument)

            ' Dokument entschützen
            If goOptions.bUnprotected = False Then
                goLogging.WriteLog("Unprotecting the document...")
                If oWord.Documents(sDocName).ProtectionType <> Word.WdProtectionType.wdNoProtection Then
                    oWord.Documents(sDocName).Unprotect(Password:=goOptions.sPassword_source)
                End If
            End If

            ' Dokumenteigenschaften in das Zieldokument einfügen
            goLogging.WriteLog("Writing DocProperties...")
            Call WriteDocProperties(sDocName, oWord, False, False)

            ' Dokument aktualisieren
            goLogging.WriteLog("UpdateDoc...")
            Call UpdateDoc(oWord.ActiveDocument.Name, oWord)

            'Dokument schützen
            If goOptions.bUnprotected = False Then
                goLogging.WriteLog("Protecting the Document...")
                oWord.Documents(sDocName).Protect(Password:=goOptions.sPassword_target, NoReset:=False, Type:=Word.WdProtectionType.wdAllowOnlyFormFields)
            End If

            goLogging.WriteLog("Saving the Document...")
            oWord.Documents(sDocName).SaveAs(FileName:=goOptions.sFile_DocTarget, FileFormat:=goOptions.iSaveFileFormat, _
                                            LockComments:=False, Password:="", AddToRecentFiles:=True, _
                                            WritePassword:="", ReadOnlyRecommended:=False, EmbedTrueTypeFonts:=False, _
                                            SaveNativePictureFormat:=False, SaveFormsData:=False, SaveAsAOCELetter:=False)

            goLogging.WriteLog("Quit Word...")
            oWord.Quit(Word.WdSaveOptions.wdDoNotSaveChanges)

        ElseIf goOptions.sOption = "checkin" Then

            goLogging.WriteLog("Opening the document...")
            oWord.Application.Documents.Open(FileName:=goOptions.sFile_DocTarget)

            'Ausdrücklich in wdPrintView wechseln (für Word 2013)
            CheckPrintView(oWord.ActiveDocument)

            ' Dokument aktualisieren
            goLogging.WriteLog("UpdateDoc...")
            Call UpdateDoc(oWord.ActiveDocument.Name, oWord)

            If goOptions.sUseProtection = "true" And oWord.ActiveDocument.ProtectionType = Word.WdProtectionType.wdNoProtection Then
                '...und schützen
                goLogging.WriteLog("Protecting the Document...")
                oWord.ActiveDocument.Protect(Password:=goOptions.sPassword_target, NoReset:=False, Type:=Word.WdProtectionType.wdAllowOnlyFormFields)
            End If

            goLogging.WriteLog("Saving the Document...")
            oWord.Documents(goOptions.sFile_DocTarget).Saved = False
            oWord.Documents(goOptions.sFile_DocTarget).Save()

            goLogging.WriteLog("Quit Word...")
            oWord.Quit(Word.WdSaveOptions.wdDoNotSaveChanges)

        End If

        'Word wird ab hier nicht mehr benötigt, also freigeben.
        goLogging.WriteLog("Destroying the word instance...")
        oWord = Nothing

    End Sub

    ''' <summary>
    ''' Compares the document in source and target
    ''' </summary>
    Sub CompareDocument()

        Dim docSource As Word.Document
        Dim docTarget As Word.Document
        Dim docResult As Word.Document
        Dim oRange As Word.Range

        goLogging.WriteLog("Comparing Documents...")

        goLogging.WriteLog("Instanciating Word Object...")
        oWord = CreateObject("Word.Application")

        'Sollte schon visible = false stehen, aber zur sicherheit...
        goLogging.WriteLog("Hide Word...")
        oWord.Visible = False

        'Open document Number 1
        goLogging.WriteLog("Open File " & goOptions.sTemplate)
        docSource = oWord.Application.Documents.Open(goOptions.sTemplate)
        CheckPrintView(docSource)

        'Unprotect the document if necessary
        goLogging.WriteLog("Word Protection Type: " & docSource.ProtectionType)
        If docSource.ProtectionType <> Word.WdProtectionType.wdNoProtection Then
            goLogging.WriteLog("Unprotect the Document")
            If docSource.ProtectionType <> Word.WdProtectionType.wdNoProtection Then
                docSource.Unprotect(Password:=goOptions.sPassword_source)
            End If
        End If

        'Open document Number 2
        goLogging.WriteLog("Open File " & goOptions.sFile_DocTarget)
        docTarget = oWord.Application.Documents.Open(goOptions.sFile_DocTarget)
        CheckPrintView(docTarget)

        'Unprotect the document if necessary
        goLogging.WriteLog("Word Protection Type: " & docTarget.ProtectionType)
        If docTarget.ProtectionType <> Word.WdProtectionType.wdNoProtection Then
            goLogging.WriteLog("Unprotect the Document")
            If docTarget.ProtectionType <> Word.WdProtectionType.wdNoProtection Then
                docTarget.Unprotect(Password:=goOptions.sPassword_target)
            End If
        End If

        'Compare the documents (optional values are in comments with default value)
        goLogging.WriteLog("Comparing " & docSource.Name & " to " & docTarget.Name & " ...")
        docResult = oWord.CompareDocuments(OriginalDocument:=docSource, _
                               RevisedDocument:=docTarget, _
                               RevisedAuthor:="iSR", _
                               IgnoreAllComparisonWarnings:=True)
        'Destination:=wdCompareDestinationNew, _
        'Granularity:=wdGranularityWordLevel, _
        'CompareFormatting:=True, _
        'CompareCaseChanges:=True, _
        'CompareWhitespace:=True, _
        'CompareTables:=True, _
        'CompareHeaders:=True, _
        'CompareFootnotes:=True, _
        'CompareTextboxes:=True, _
        'CompareFields:=True, _
        'CompareComments:=True, _
        'CompareMoves:=True, _


        'close the origin documents
        goLogging.WriteLog("Closing original documents...")
        docSource.Close()
        docTarget.Close()

        'Generate Changes Report
        ExtractTrackedChangesToNewDoc(goOptions.sConfigPath & "\Changelog" & goOptions.sFile_DocTarget_Extension)

        'Copy the changelog at the end of the result
        oRange = docResult.Range
        With oRange
            .Collapse(0)
            .InsertBreak(Type:=Word.WdBreakType.wdSectionBreakNextPage)
            .End = docResult.Range.End
            .Collapse(0)
            .PageSetup.Orientation = Word.WdOrientation.wdOrientLandscape
            .InsertFile(goOptions.sConfigPath & "\Changelog" & goOptions.sFile_DocTarget_Extension)
        End With

        'Protect the Resulting document using the target password
        If goOptions.sUseProtection = "true" Then
            goLogging.WriteLog("Protecting the result document...")
            docResult.Protect(Password:=goOptions.sPassword_target, NoReset:=False, Type:=Word.WdProtectionType.wdAllowOnlyFormFields)
        End If

        goLogging.WriteLog("Saving the result document as " & goOptions.sFile_DocTarget)
        docResult.SaveAs(FileName:=goOptions.sFile_DocTarget, FileFormat:=goOptions.iSaveFileFormat, _
                                   LockComments:=False, Password:="", AddToRecentFiles:=True, _
                                   WritePassword:="", ReadOnlyRecommended:=False, EmbedTrueTypeFonts:=False, _
                                   SaveNativePictureFormat:=False, SaveFormsData:=False, SaveAsAOCELetter:=False)

        goLogging.WriteLog("Quit Word...")
        oWord.Quit(Word.WdSaveOptions.wdDoNotSaveChanges)

    End Sub

    ''' <summary>
    ''' Writes the Document Properties to the Taget Document
    ''' Additionally clones them as Doc Variables. Necessary if the Doc Property can get very long.
    ''' </summary>
    ''' <param name="sTargetDocName">The filename of the target document</param>  
    ''' <param name="oWord">Word instance</param>  
    ''' <param name="deleteDocProps">true: Delete all docproperties (except DOC_EMPTY) before proceeding.</param>  
    ''' <param name="bOIDonly">true: Only update the OID</param>  
    Sub WriteDocProperties(sTargetDocName As String, oWord As Word.Application, deleteDocProps As Boolean, bOIDonly As Boolean)

        Dim I As Integer
        Dim j As Integer
        Dim sDocInfoText As String
        Dim sDocInfoValue As String
        Dim oProp As Object
        Dim iDocProps As Integer
        Dim aDocProps() As String

        'Nur wenn das Dokument nicht final ist, alle properties löschen, sonst nur überschreiben/hinzufügen
        If deleteDocProps = True And bOIDonly = False Then
            'Alle DocProperties entfernen (ausser DOC_EMPTY, sonst gibts kuddelmuddel)
            For Each oProp In oWord.Documents(sTargetDocName).CustomDocumentProperties
                If oProp.Name <> "DOC_EMPTY" Then
                    oProp.Delete()
                End If
            Next oProp
            'Alle DocVariables entfernen (DOC_EMPTY spielt hier keine Rolle)
            For Each oProp In oWord.Documents(sTargetDocName).Variables
                oProp.Delete()
            Next oProp
        End If

        'Eine Liste aller verfügbaren DocProperties erstellen
        iDocProps = oWord.Documents(sTargetDocName).CustomDocumentProperties.Count
        ReDim aDocProps(iDocProps)
        I = 0
        For Each oProp In oWord.Documents(sTargetDocName).CustomDocumentProperties
            I = I + 1
            aDocProps(I) = oProp.Name
        Next oProp
        iDocProps = I

        'Einfügen
        For j = 1 To goOptions.iDocProperties

            sDocInfoText = goOptions.GetDocPropertyName(j)
            sDocInfoValue = goOptions.GetDocPropertyValue(j)

            If bOIDonly = True Then
                If sDocInfoText = "DOC_OID" Then
                    DoWriteDocProperty(sTargetDocName, oWord, sDocInfoText, sDocInfoValue, aDocProps, iDocProps)
                End If
            Else
                DoWriteDocProperty(sTargetDocName, oWord, sDocInfoText, sDocInfoValue, aDocProps, iDocProps)
            End If

        Next j

        'Clean up
        goLogging.WriteLog("Destructing Objects...")
        oProp = Nothing

    End Sub

    ''' <summary>
    ''' Updates all stories and the ToC of the target document
    ''' </summary>
    ''' <param name="sTargetDocName">The filename of the target document</param>  
    ''' <param name="oWord">Word instance</param>  
    Sub UpdateDoc(sTargetDocName As String, oWord As Word.Application)

        Dim oStory As Object
        Dim oToc As Object
        Dim oField As Object

        'Update document
        With oWord.Documents(sTargetDocName)

            goLogging.WriteLog("Update stories...")
            For Each oStory In .StoryRanges
                For Each oField In oStory.Fields
                    'No update if this is a graphic field.
                    'May destroy the scaling and is not neccessary anyway
                    If oField.Type <> 67 Then oField.Update()
                Next oField
            Next oStory

            goLogging.WriteLog("Update TOCs...")
            For Each oToc In .TablesOfContents
                oToc.Update() 'update TOC's
            Next oToc
        End With

        'Clean Up
        goLogging.WriteLog("Destructing Objects...")
        oStory = Nothing
        oToc = Nothing

    End Sub

    '''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
    ''' <summary>
    ''' Writes a docproperty to the target document.
    ''' </summary>
    ''' <param name="sTargetDocName">The filename of the target document</param>  
    ''' <param name="oWord">Word instance</param>  
    ''' <param name="sDocInfoText">The name of the doc property</param>  
    ''' <param name="sDocInfoValue">The value oif the doc property</param>  
    ''' <param name="aDocProps">An array containing the names of all doc properties</param>  
    ''' <param name="iDocProps">The number of entries in aDocProps</param>  
    Private Sub DoWriteDocProperty(sTargetDocName As String, oWord As Word.Application, sDocInfoText As String, sDocInfoValue As String, aDocProps() As String, iDocProps As Integer)

        Dim PropExists As Boolean
        Dim I As Integer

        goLogging.WriteLog("Updating Docproperty " & sDocInfoText)

        'Dokumenteneigenschaften nur dann überschreiben, wenn ein Wert übergeben wird
        If sDocInfoValue = "" Then sDocInfoValue = " "

        'Check if the doc property is already existing
        goLogging.WriteLog("Check if Docproperty exists...")
        PropExists = False
        For I = 1 To iDocProps
            If aDocProps(I) = sDocInfoText Then
                PropExists = True
                Exit For
            End If
        Next I

        If PropExists = True Then
            goLogging.WriteLog("Overwrite existing Docproperty...")
            oWord.Documents(sTargetDocName).CustomDocumentProperties(sDocInfoText).Value = sDocInfoValue
            goLogging.WriteLog("Overwrite existing Docvariable...")
            oWord.Documents(sTargetDocName).Variables(sDocInfoText).Value = sDocInfoValue
        Else

            goLogging.WriteLog("Set new Docproperty...")
            Try
                oWord.Documents(sTargetDocName).CustomDocumentProperties.Add(Name:=sDocInfoText, Value:=sDocInfoValue, LinkToContent:=False, Type:=4) ' msoPropertyTypeString
            Catch ex As Exception
                goLogging.WriteLog("Docproperty " & sDocInfoText & " could not be set!")
            End Try

            goLogging.WriteLog("Set new Docvariable...")
            Try
                oWord.Documents(sTargetDocName).Variables.Add(Name:=sDocInfoText, Value:=sDocInfoValue)
            Catch ex As Exception
                goLogging.WriteLog("Docvariable " & sDocInfoText & " could not be set!")
            End Try
        End If

    End Sub

    ''' <summary>
    ''' Writes the sPrompt to the logfile.
    ''' If in debug mode, additionally a message box will be displayed.
    ''' </summary>
    ''' <param name="sPrompt">The message</param>  
    Sub MsgBoxCheck(sPrompt As String)

        goLogging.WriteLog("modMain - MsgBoxHandler " & sPrompt)

        If bDEBUG = True Then
            MsgBox(sPrompt)
        End If

    End Sub

    ''' <summary>
    ''' Handles Exceptions by writing a message to the trace log.
    ''' Called only at places where exceptions are expected, i.e. in the catch block.
    ''' </summary>
    ''' <param name="ex">The exception.</param>  
    ''' <param name="sSource">Additional information, usually the name of the method.</param>  
    Public Sub HandleException(ByVal ex As Exception, ByVal sSource As String)

        oTrace.TraceEvent(TraceEventType.Warning, 10, "Exception Occured in " & sSource & ": " & ex.ToString)

    End Sub


    Public Sub ExtractTrackedChangesToNewDoc(sFilename As String)

        '=========================
        'Macro created 2007 by Lene Fredborg, DocTools - www.thedoctools.com
        'THIS MACRO IS COPYRIGHT. YOU ARE WELCOME TO USE THE MACRO BUT YOU MUST KEEP THE LINE ABOVE.
        'YOU ARE NOT ALLOWED TO PUBLISH THE MACRO AS YOUR OWN, IN WHOLE OR IN PART.
        '=========================
        'The macro creates a new document
        'and extracts insertions and deletions
        'marked as tracked changes from the active document
        'NOTE: Other types of changes are skipped
        '(e.g. formatting changes or inserted/deleted footnotes and endnotes)
        'Only insertions and deletions in the main body of the document will be extracted
        'The document will also include metadata
        'Inserted text will be applied black font color
        'Deleted text will be applied red font color

        'Minor adjustments are made to the styles used
        'You may need to change the style settings and table layout to fit your needs
        '=========================

        Dim oDoc As Word.Document
        Dim oNewDoc As Word.Document
        Dim oTable As Word.Table
        Dim oRow As Word.Row
        Dim oCol As Word.Column
        Dim oRange As Word.Range
        Dim oRevision As Word.Revision
        Dim strText As String
        Dim n As Long
        Dim i As Long
        Dim Title As String

        goLogging.WriteLog("Begin generating tracked changes report...")

        Title = "Extract Tracked Changes to New Document"
        n = 0 'use to count extracted changes

        oDoc = oWord.ActiveDocument

        'Create a new document for the tracked changes, base on Normal.dot
        oNewDoc = oWord.Documents.Add
        'Set to landscape
        oNewDoc.PageSetup.Orientation = Word.WdOrientation.wdOrientLandscape
        With oNewDoc
            'Make sure any content is deleted
            '.Content = ""
            'Set appropriate margins
            With .PageSetup
                .LeftMargin = oWord.Application.CentimetersToPoints(2)
                .RightMargin = oWord.Application.CentimetersToPoints(2)
                .TopMargin = oWord.Application.CentimetersToPoints(2.5)
            End With
            'Insert a 6-column table for the tracked changes and metadata
            oTable = .Tables.Add(Range:=oWord.Selection.Range, NumRows:=1, NumColumns:=4)
        End With

        'Insert info in header
        oNewDoc.Sections(1).Headers(Word.WdHeaderFooterIndex.wdHeaderFooterPrimary).Range.Text = _
            "Tracked changes extracted from: " & oDoc.FullName & vbCrLf & _
            "Created by: " & oWord.Application.UserName & vbCrLf & _
            "Creation date: " & Format("0;d", Date.Today)

        'Adjust the Normal style and Header style
        With oNewDoc.Styles(Word.WdBuiltinStyle.wdStyleNormal)
            With .Font
                .Name = "Arial"
                .Size = 9
                .Bold = False
            End With
            With .ParagraphFormat
                .LeftIndent = 0
                .SpaceAfter = 6
            End With
        End With

        With oNewDoc.Styles(Word.WdBuiltinStyle.wdStyleHeader)
            .Font.Size = 8
            .ParagraphFormat.SpaceAfter = 0
        End With

        'Format the table appropriately
        With oTable
            .Range.Style = Word.WdBuiltinStyle.wdStyleNormal
            .AllowAutoFit = False
            .PreferredWidthType = Word.WdPreferredWidthType.wdPreferredWidthPercent
            .PreferredWidth = 100
            For Each oCol In .Columns
                oCol.PreferredWidthType = Word.WdPreferredWidthType.wdPreferredWidthPercent
            Next oCol
            .Columns(1).PreferredWidth = 5  'Page
            .Columns(2).PreferredWidth = 5  'Line
            .Columns(3).PreferredWidth = 10 'Type of change
            .Columns(4).PreferredWidth = 80 'Inserted/deleted text
            '.Columns(4).PreferredWidth = 55 'Inserted/deleted text
            '.Columns(5).PreferredWidth = 15 'Author
            '.Columns(6).PreferredWidth = 10 'Revision date
        End With

        'Insert table headings
        With oTable.Rows(1)
            .Cells(1).Range.Text = "Page"
            .Cells(2).Range.Text = "Line"
            .Cells(3).Range.Text = "Type"
            .Cells(4).Range.Text = "What has been inserted or deleted"
            '.Cells(5).Range.Text = "Author"
            '.Cells(6).Range.Text = "Date"
        End With

        'Get info from each tracked change (insertion/deletion) from oDoc and insert in table
        For Each oRevision In oDoc.Revisions
            Select Case oRevision.Type
                'Only include insertions and deletions
                Case Word.WdRevisionType.wdRevisionInsert, Word.WdRevisionType.wdRevisionDelete
                    'In case of footnote/endnote references (appear as Chr(2)),
                    'insert "[footnote reference]"/"[endnote reference]"
                    With oRevision
                        'Get the changed text
                        strText = .Range.Text

                        oRange = .Range
                        Do While InStr(1, oRange.Text, Chr(2)) > 0
                            'Find each Chr(2) in strText and replace by appropriate text
                            i = InStr(1, strText, Chr(2))

                            If oRange.Footnotes.Count = 1 Then
                                strText = Replace(Expression:=strText, Find:=Chr(2), Replacement:="[footnote reference]", Start:=1, Count:=1)
                                'To keep track of replace, adjust oRange to start after i
                                oRange.Start = oRange.Start + i

                            ElseIf oRange.Endnotes.Count = 1 Then
                                strText = Replace(Expression:=strText, Find:=Chr(2), Replacement:="[endnote reference]", Start:=1, Count:=1)
                                'To keep track of replace, adjust oRange to start after i
                                oRange.Start = oRange.Start + i
                            End If
                        Loop
                    End With
                    'Add 1 to counter
                    n = n + 1
                    'Add row to table
                    oRow = oTable.Rows.Add

                    'Insert data in cells in oRow
                    With oRow
                        'Page number
                        .Cells(1).Range.Text = _
                            oRevision.Range.Information(Word.WdInformation.wdActiveEndPageNumber)

                        'Line number - start of revision
                        .Cells(2).Range.Text = _
                            oRevision.Range.Information(Word.WdInformation.wdFirstCharacterLineNumber)

                        'Type of revision
                        If oRevision.Type = Word.WdRevisionType.wdRevisionInsert Then
                            .Cells(3).Range.Text = "Inserted"
                            'Apply automatic color (black on white)
                            oRow.Range.Font.Color = Word.WdColor.wdColorAutomatic
                        Else
                            .Cells(3).Range.Text = "Deleted"
                            'Apply red color
                            oRow.Range.Font.Color = Word.WdColor.wdColorRed
                        End If

                        'The inserted/deleted text
                        .Cells(4).Range.Text = strText

                        'The author
                        '.Cells(5).Range.Text = oRevision.Author

                        'The revision date
                        '.Cells(6).Range.Text = Format(oRevision.Date, "mm-dd-yyyy")
                    End With
            End Select
        Next oRevision

        'If no insertions/deletions were found, show message and close oNewDoc
        If n = 0 Then
            'MsgBox("No insertions or deletions were found.", vbOKOnly, Title)
            oNewDoc.Close(SaveChanges:=Word.WdSaveOptions.wdDoNotSaveChanges)
            GoTo ExitHere
        End If

        'Apply bold formatting and heading format to row 1
        With oTable.Rows(1)
            .Range.Font.Bold = True
            .HeadingFormat = True
        End With

        goLogging.WriteLog("Saving the result document as " & sFilename)
        oNewDoc.SaveAs(FileName:=sFilename, FileFormat:=goOptions.iSaveFileFormat, _
                                   LockComments:=False, Password:="", AddToRecentFiles:=True, _
                                   WritePassword:="", ReadOnlyRecommended:=False, EmbedTrueTypeFonts:=False, _
                                   SaveNativePictureFormat:=False, SaveFormsData:=False, SaveAsAOCELetter:=False)

        ' MsgBox(n & " tracked changed have been extracted. " & "Finished creating document.", vbOKOnly, Title)

ExitHere:
        oDoc = Nothing
        oTable = Nothing
        oRow = Nothing
        oRange = Nothing
        oNewDoc = Nothing

        goLogging.WriteLog("End generating tracked changes report.")

    End Sub

End Module
