﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class fError
    Inherits System.Windows.Forms.Form

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.components = New System.ComponentModel.Container()
        Me.oTimeout = New System.Windows.Forms.Timer(Me.components)
        Me.lTimeOutCounter = New System.Windows.Forms.Label()
        Me.btnClose = New System.Windows.Forms.Button()
        Me.lTitle = New System.Windows.Forms.Label()
        Me.txtErrorMessage = New System.Windows.Forms.TextBox()
        Me.btnLogToClipboard = New System.Windows.Forms.Button()
        Me.SuspendLayout()
        '
        'oTimeout
        '
        Me.oTimeout.Interval = 1000
        '
        'lTimeOutCounter
        '
        Me.lTimeOutCounter.Anchor = CType((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Left), System.Windows.Forms.AnchorStyles)
        Me.lTimeOutCounter.AutoSize = True
        Me.lTimeOutCounter.Location = New System.Drawing.Point(12, 184)
        Me.lTimeOutCounter.Name = "lTimeOutCounter"
        Me.lTimeOutCounter.Size = New System.Drawing.Size(19, 13)
        Me.lTimeOutCounter.TabIndex = 0
        Me.lTimeOutCounter.Text = "00"
        '
        'btnClose
        '
        Me.btnClose.Anchor = CType((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.btnClose.DialogResult = System.Windows.Forms.DialogResult.OK
        Me.btnClose.Location = New System.Drawing.Point(687, 180)
        Me.btnClose.Name = "btnClose"
        Me.btnClose.Size = New System.Drawing.Size(105, 21)
        Me.btnClose.TabIndex = 1
        Me.btnClose.Text = "Close"
        Me.btnClose.UseVisualStyleBackColor = True
        '
        'lTitle
        '
        Me.lTitle.AutoSize = True
        Me.lTitle.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lTitle.Location = New System.Drawing.Point(12, 9)
        Me.lTitle.Name = "lTitle"
        Me.lTitle.Size = New System.Drawing.Size(106, 13)
        Me.lTitle.TabIndex = 2
        Me.lTitle.Text = "An error occured:"
        '
        'txtErrorMessage
        '
        Me.txtErrorMessage.Anchor = CType((((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Bottom) _
            Or System.Windows.Forms.AnchorStyles.Left) _
            Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.txtErrorMessage.Location = New System.Drawing.Point(12, 30)
        Me.txtErrorMessage.Multiline = True
        Me.txtErrorMessage.Name = "txtErrorMessage"
        Me.txtErrorMessage.Size = New System.Drawing.Size(780, 144)
        Me.txtErrorMessage.TabIndex = 3
        '
        'btnLogToClipboard
        '
        Me.btnLogToClipboard.Anchor = CType((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.btnLogToClipboard.Location = New System.Drawing.Point(547, 180)
        Me.btnLogToClipboard.Name = "btnLogToClipboard"
        Me.btnLogToClipboard.Size = New System.Drawing.Size(134, 21)
        Me.btnLogToClipboard.TabIndex = 4
        Me.btnLogToClipboard.Text = "Copy Log to Clipboard"
        Me.btnLogToClipboard.UseVisualStyleBackColor = True
        '
        'fError
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(805, 213)
        Me.Controls.Add(Me.btnLogToClipboard)
        Me.Controls.Add(Me.txtErrorMessage)
        Me.Controls.Add(Me.lTitle)
        Me.Controls.Add(Me.btnClose)
        Me.Controls.Add(Me.lTimeOutCounter)
        Me.Name = "fError"
        Me.Text = "Wordmodule Error"
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub
    Friend WithEvents oTimeout As System.Windows.Forms.Timer
    Friend WithEvents lTimeOutCounter As System.Windows.Forms.Label
    Friend WithEvents btnClose As System.Windows.Forms.Button
    Friend WithEvents lTitle As System.Windows.Forms.Label
    Friend WithEvents txtErrorMessage As System.Windows.Forms.TextBox
    Friend WithEvents btnLogToClipboard As System.Windows.Forms.Button
End Class
