﻿Imports Microsoft.Office.Interop.Word
Imports System.IO

Module modWord
    '''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
    ' Modul             : modWord
    ' Beschreibung      : Enthält die gesamte Funktionalität zum Bearbeiten der Word-Dokumente
    ' Autor             : as, erweitert und neu struktiert von ms
    ' Datum             : 20.04.2004
    ' Änderungshistorie :
    ' Autor  Datum       Grund            Kommentar
    ' -----  ----------  ---------------  ---------------------------------------
    '
    '''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''

    Public oWordProps As cWordProps
    Public oWord As Application
    Public iWordVer As Integer
    Public bDocsOpen As Boolean 'Flag für das Errorhandling

    '''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
    ' Sub-Prozedur        : ConvertToWord
    ' Beschreibung        : Convertiert ein HTML-File zu einem Word-Dokument, bzw. aktualisiert ein
    '                       entsprechendes Dokument.
    ' Parameter           : -
    ' wird aufgerufen von : modMain.Main
    ' Änderungshistorie   :
    ' Autor  Datum       Grund            Kommentar
    ' -----  ----------  ---------------  ---------------------------------------
    '
    '''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
    Sub ConvertToWord()

        Dim bDocEmtpy As Boolean

        bDocsOpen = False

        goLogging.WriteLog("Start: ConvertToWord")

        ' Word Objekt anlegen

        oWord = CreateObject("word.Application")

        goLogging.WriteLog("Word Object created")

        ' oWordProperties initialisieren
        oWordProps = New cWordProps
        oWordProps.InitWordProps(oWord)

        gVersionWindows = Trim(Environment.OSVersion.ToString())
        gVersionWord = Trim(oWord.Build)

        goLogging.WriteLog("  Wordmodule Version: " & System.Diagnostics.FileVersionInfo.GetVersionInfo(System.Reflection.Assembly.GetExecutingAssembly.Location).FileVersion & " (" & cCompilationDate & ")")
        goLogging.WriteLog("  Windows Version   : " & gVersionWindows)
        goLogging.WriteLog("  Word Version      : " & gVersionWord)

        iWordVer = Val(Left(oWord.Build, InStr(1, oWord.Build, ".") - 1))

        ' ########################################################################################

        ' Einige Werte für Word setzen
        Call SetWordOptions()

        ' Supress Word Alerts
        oWord.Application.DisplayAlerts = WdAlertLevel.wdAlertsNone

        ' ########################################################################################

        ' Quell und Zieldokument öffnen
        Call UpdateFeedback(0, "Open documents")
        Call OpenTargetDoc()
        Call CreateListOfTargetBMs()
        Call OpenSourceDoc()

        ' ########################################################################################

        '    'Benutzerdefinierte Eigeneschaften des Quelldokuments auslesen und Namen und Anzahl der
        '    'strukturierten Textmarken zurückgeben
        '    Call UpdateFeedback(0, "Setze Dokumenteneigenschaften")
        '    Call CreateDocProperties(sSourceDoc)
        '    Call GetUserDefinedProperties

        ' ########################################################################################

        ' Dokumenteigenschaften in das Zieldokument einfügen
        Call UpdateFeedback(0, "Set document properties")
        Call InsertDocProperties(oWordProps.docTarget)

        ' ########################################################################################

        ' Status des Zieldokuments überprüfen und anhand dieses bearbeiten
        ' (leeres Dokument -> emptyDoc, gefülltes Dokument -> fullDoc)

        Try
            bDocEmtpy = oWordProps.docTarget.CustomDocumentProperties("DOC_EMPTY").value
        Catch ex As System.ArgumentException
            goLogging.WriteLog("ERROR: DOC_EMPTY not found in Template!")
            CloseWord()
            goLogging.WriteLog("Exit : ConvertToWord")
            Exit Sub
        End Try

        If bDocEmtpy = True Then   'true  = Dokument ist leer
            Call DocEmpty()
        Else                    'false = Dokument ist gefüllt
            Call DocFull()
        End If

        ' ########################################################################################

        'Leere Textmarken entfernen
        If goOptions.bRemoveEmptyBM = True Then
            Call UpdateFeedback(96, "Removing empty bookmarks")
            RemoveEmptyBookmarks()
        End If
        goLogging.WriteLog("CP   : RemoveEmptyBookmarks")


        ' ########################################################################################

        ' Aktualisiere alle Felder V.2
        Call UpdateFeedback(97, "Updating all fields")
        Call UpdateAllFields()
        goLogging.WriteLog("CP   : UpdateAllFields")

        ' ########################################################################################

        With oWordProps.docTarget

            ' Dokument schützen
            If goOptions.sPassword_target <> "" And goOptions.bUnprotected = False Then
                Call UpdateFeedback(98, "Protecting target document")
                .Protect(Password:=goOptions.sPassword_target, NoReset:=False, Type:=WdProtectionType.wdAllowOnlyFormFields)
            End If
            goLogging.WriteLog("CP   : Doc Protected")

            ' Datei speichern
            Call UpdateFeedback(99, "Saving document")
            'oWord.ChangeFileOpenDirectory GetTemp()
            .SaveAs(FileName:=goOptions.sFile_DocTarget, FileFormat:=goOptions.iSaveFileFormat, _
                    LockComments:=False, Password:="", AddToRecentFiles:=True, _
                    WritePassword:="", ReadOnlyRecommended:=False, EmbedTrueTypeFonts:=False, _
                    SaveNativePictureFormat:=False, SaveFormsData:=False, SaveAsAOCELetter:=False)
            goLogging.WriteLog("CP   : Doc Saved")

            Call UpdateFeedback(100, "Closing document/Word")
            .Close()
            goLogging.WriteLog("CP   : Doc Closed")

        End With

        ' ########################################################################################

        ' Word Application Option Eigenschaften wie vorgefunden zurücksetzen
        With oWord.Options
            .Pagination = oWordProps.bPaginationSetting
            .ConfirmConversions = oWordProps.bConversionSetting
        End With
        goLogging.WriteLog("CP   : Word Application Options Restored")

        ' ########################################################################################

        CloseWord()

        goLogging.WriteLog("End  : ConvertToWord")

    End Sub

    Sub CloseWord()

        ' Aufräumen
        oWord.Quit(WdSaveOptions.wdDoNotSaveChanges)
        bDocsOpen = False
        oWordProps.ReleaseWordProps()
        oWord = Nothing
        oWordProps = Nothing

    End Sub

    '''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
    ' Sub-Prozedur        : UpdateFeedback
    ' Beschreibung        : Fenster mit Nachricht und Prozentwert (für die ProgressBar) updaten
    ' Parameter           : -
    ' wird aufgerufen von : ConvertToWord
    ' Änderungshistorie   :
    ' Autor  Datum       Grund            Kommentar
    ' -----  ----------  ---------------  ---------------------------------------
    '
    '''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
    Sub UpdateFeedback(iPercent As Integer, sMessage As String)

        gfMain.UpdateMessage(sMessage)
        gfMain.UpdateProgressbar(iPercent)

    End Sub

    '''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
    ' Sub-Prozedur        : SetWordOptions
    ' Beschreibung        : Setzen einiger Eigenschaften des Word-Objekts
    ' Parameter           : -
    ' wird aufgerufen von : ConvertToWord
    ' Änderungshistorie   :
    ' Autor  Datum       Grund            Kommentar
    ' -----  ----------  ---------------  ---------------------------------------
    '
    '''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
    Sub SetWordOptions()

        ' Word Application Options setzen
        goLogging.WriteLog("Start: SetWordOptions")
        With oWord.Options
            .Pagination = True              ' Führe Zeilenumbruch im Hintergrund durch
            .CheckSpellingAsYouType = False ' Schalte Rechtschreibeprüfung aus
            .CheckGrammarAsYouType = False  ' Schalte Grammatikprüfung aus
            .ConfirmConversions = False
        End With

        ' nb: hier auf TRUE setzen um "visuell" zu debuggen
        '    oWord.Visible = False
        goLogging.WriteLog("End  : SetWordOptions")

        Exit Sub

    End Sub

    '''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
    ' Sub-Prozedur        : OpenSourceDoc
    ' Beschreibung        : Öffnen ein Quelldokument
    ' Parameter           : -
    ' wird aufgerufen von : ConvertToWord
    ' Änderungshistorie   :
    ' Autor  Datum       Grund            Kommentar
    ' -----  ----------  ---------------  ---------------------------------------
    '
    '''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
    Sub OpenSourceDoc()

        Dim I As Integer
        Dim sLoadedFiles As String = ""

        ' Quelldokument öffnen und Ansicht festlegen
        goLogging.WriteLog("Start: OpenSourceDoc")

        oWordProps.docSource = oWord.Documents.Add()

        For I = 1 To goOptions.iFile_Sources
            If goOptions.GetFile_Source(I) <> "" Then

                'Make sure that the same file is not loaded several times.
                If InStr(sLoadedFiles, goOptions.GetFile_Source(I)) = 0 Then

                    If File.Exists(goOptions.GetFile_Source(I)) Then
                        goLogging.WriteLog("  Load: " & goOptions.GetFile_Source(I) & " - (" & goOptions.GetFile_Bookmark(I) & ")")

                        With oWordProps.docSource.ActiveWindow.Selection
                            .InsertFile(FileName:=goOptions.GetFile_Source(I), ConfirmConversions:=False)
                            .InsertParagraphAfter()
                            .InsertBreak(Type:=WdBreakType.wdSectionBreakNextPage)
                            .Collapse(Direction:=WdCollapseDirection.wdCollapseEnd)
                        End With

                        sLoadedFiles = sLoadedFiles & goOptions.GetFile_Source(I) & "|"
                    Else
                        goLogging.WriteLog("  Load failed: " & goOptions.GetFile_Source(I) & " - (" & goOptions.GetFile_Bookmark(I) & ")")
                    End If

                End If

            Else
                goLogging.WriteLog("  Load failed: No File stated for " & goOptions.GetFile_Bookmark(I))
            End If
        Next I

        goLogging.WriteLog("End  : OpenSourceDoc")

    End Sub

    '''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
    ' Sub-Prozedur        : OpenTargetDoc
    ' Beschreibung        : Öffnet das Zieldokument
    ' Parameter           : -
    ' wird aufgerufen von : ConvertToWord
    ' Änderungshistorie   :
    ' Autor  Datum       Grund            Kommentar
    ' -----  ----------  ---------------  ---------------------------------------
    '
    '''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
    Sub OpenTargetDoc()

        ' Zieldokument öffnen und Ansicht festlegen
        goLogging.WriteLog("Start: OpenTargetDoc")

        oWordProps.docTarget = oWord.Documents.Open(goOptions.sTemplate)

        CheckPrintView(oWordProps.docTarget)

        goLogging.WriteLog("  Target Doc open:" & oWordProps.docTarget.Name)

        bDocsOpen = True

        goLogging.WriteLog("End  : OpenTargetDoc")

    End Sub

    Sub CreateListOfTargetBMs()

        Dim oBMrange As Range
        Dim iBMcount As Integer
        Dim sBMname As String
        Dim I As Integer

        goLogging.WriteLog("Start: CreateListOfTargetBMs")

        oBMrange = oWordProps.docTarget.Range
        iBMcount = oBMrange.Bookmarks.Count

        For I = 1 To iBMcount
            sBMname = oBMrange.Bookmarks(I).Name

            If UCase(sBMname) Like sBM_STRUCT Then
                oWordProps.SetStructBM(sBMname)
                goLogging.WriteLog("  S_TM found: " & sBMname)
            End If
        Next I

        goLogging.WriteLog("End  : CreateListOfTargetBMs")

    End Sub

    '''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
    ' Sub-Prozedur        : InsertDocProperties
    ' Beschreibung        : Dokumenteigenschaften in das Zieldokument einfügen
    ' Parameter           : -
    ' wird aufgerufen von : ConvertToWord
    ' Änderungshistorie   :
    ' Autor  Datum       Grund            Kommentar
    ' -----  ----------  ---------------  ---------------------------------------
    '
    '''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
    Sub InsertDocProperties(sTargetDoc As Document)

        Dim m As Integer
        Dim sDocInfoName As String
        Dim sDocInfoValue As String
        Dim oProp As Object
        Dim iDocProps As Integer
        Dim aDocProps() As String
        Dim I As Integer
        Dim PropExists As Boolean

        goLogging.WriteLog("Start: InsertDocProperties")

        'Alle DocProperties entfernen (ausser DOC_EMPTY, sonst gibts kuddelmuddel)
        For Each oProp In sTargetDoc.CustomDocumentProperties
            If oProp.Name <> "DOC_EMPTY" Then
                oProp.Delete()
            End If
        Next
        'Alle DocVariables entfernen (DOC_EMPTY spielt hier keine Rolle)
        For Each oProp In sTargetDoc.Variables
            oProp.Delete()
        Next

        'Eine Liste aller verfügbaren DocProperties erstellen
        iDocProps = sTargetDoc.CustomDocumentProperties.Count
        ReDim aDocProps(iDocProps)
        I = 0
        For Each oProp In sTargetDoc.CustomDocumentProperties
            I = I + 1
            aDocProps(I) = oProp.Name
        Next

        For m = 1 To goOptions.iDocProperties

            sDocInfoName = goOptions.GetDocPropertyName(m)
            sDocInfoValue = goOptions.GetDocPropertyValue(m)
            If sDocInfoValue = "" Then sDocInfoValue = " "

            'Dokumenteneigenschaften nur dann überschreiben, wenn ein Wert übergeben wird
            If sDocInfoValue <> "" Then

                'Prüfen ob es die DocProperty bereits gibt
                PropExists = False
                For I = 1 To iDocProps
                    If aDocProps(I) = sDocInfoName Then
                        PropExists = True
                        Exit For
                    End If
                Next I

                If PropExists = True Then
                    sTargetDoc.CustomDocumentProperties(sDocInfoName).Value = sDocInfoValue
                    sTargetDoc.Variables(sDocInfoName).Value = sDocInfoValue
                Else
                    sTargetDoc.CustomDocumentProperties.Add(Name:=sDocInfoName, Value:=sDocInfoValue, LinkToContent:=False, Type:=4) ' msoPropertyTypeString
                    sTargetDoc.Variables.Add(Name:=sDocInfoName, Value:=sDocInfoValue)
                    iDocProps = iDocProps + 1
                    ReDim Preserve aDocProps(iDocProps)
                    aDocProps(iDocProps) = sDocInfoName
                End If

                goLogging.WriteLog("  Document Property " & sDocInfoName & " = " & sDocInfoValue)
                goLogging.WriteLog("  Document Variable " & sDocInfoName & " = " & sDocInfoValue)
            Else
                goLogging.WriteLog("  Document Property " & sDocInfoName & " is empty => ignored")
            End If

        Next m

        goLogging.WriteLog("End  : InsertDocProperties")

    End Sub

    '''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
    ' Sub-Prozedur        : UpdateAllFields
    ' Beschreibung        : Aktualisiert alle Felder
    ' Parameter           : -
    ' wird aufgerufen von : ConvertToWord
    ' Änderungshistorie   :
    ' Autor  Datum       Grund            Kommentar
    ' -----  ----------  ---------------  ---------------------------------------
    '
    '''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
    Sub UpdateAllFields()

        Dim oStory As Object
        Dim oToc As Object

        Dim objField As Field
        Dim objInlineShape As InlineShape

        With oWordProps.docTarget

            'Update fields in all stories
            For Each oStory In .StoryRanges
                oStory.Fields.Update()
            Next oStory
            goLogging.WriteLog("CP   : Stories updated")

            'Update all TOC
            For Each oToc In .TablesOfContents
                oToc.Update()
            Next oToc
            goLogging.WriteLog("CP   : All Table of Contents updated")

            'Update all TOF
            For Each oToc In .TablesOfFigures
                oToc.Update()
            Next oToc
            goLogging.WriteLog("CP   : All Table of Figures updated")

            'Break all graphic Links - 2003
            For Each objField In .Fields
                If Not objField.LinkFormat Is Nothing And objField.Type = WdFieldType.wdFieldIncludePicture Then
                    goLogging.WriteLog("Breaking Link: " & objField.LinkFormat.SourceFullName)
                    objField.LinkFormat.BreakLink()
                    .UndoClear()
                End If
            Next
            goLogging.WriteLog("CP   : Breaking Graphic Links - 2003 style finished")

            'Break all graphic Links - 2007
            For Each objInlineShape In .InlineShapes
                If Not objInlineShape.LinkFormat Is Nothing Then
                    objInlineShape.LinkFormat.SavePictureWithDocument = True
                End If
            Next
            goLogging.WriteLog("CP   : Breaking Graphic Links - 2007 style finished")

            'Shortly switch to Print Preview to update all fields
            goLogging.WriteLog("CP   : Activate print preview")
            .ActiveWindow.View.Type = WdViewType.wdPrintPreview
            If .ActiveWindow.View.Type = WdViewType.wdPrintPreview Then
                .ClosePrintPreview()
                goLogging.WriteLog("CP   : Print preview closed")
            Else
                goLogging.WriteLog("CP   : Print preview failed to activate, current Viewtype: " & .ActiveWindow.View.ToString)
            End If

        End With

    End Sub

    '''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
    ' Sub-Prozedur        : DocEmpty
    ' Beschreibung        : Bearbeitet ein leeres Dokument
    ' Parameter           : -
    ' wird aufgerufen von : ConvertToWord
    ' Änderungshistorie   :
    ' Autor  Datum       Grund            Kommentar
    ' -----  ----------  ---------------  ---------------------------------------
    '
    '''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
    Sub DocEmpty()

        Dim I As Integer
        Dim j As Integer
        Dim x As Integer
        Dim bDocEmpty As Boolean
        Dim oBMrange As Range
        Dim iBMcount As Integer
        Dim sBMname As String
        Dim bStructBM As Boolean
        Dim sStructBM As String
        Dim oStructBMrange As Range
        Dim iSourceBMcount As Integer
        Dim sSourceBMname As String       'sQuelleTMName
        Dim iSelectedRange As Integer      'nSelektAbschnitt
        Dim oTargetBMrange As Range   'ZielTMBereich
        Dim iCountStructBM As Integer      'für Progressbar
        Dim sLastTableBMname As String = ""

        Dim bUnprotected As Boolean
        Dim bNoCaption As Boolean
        Dim a As Integer

        goLogging.WriteLog("Start: DocEmpty")

        iCountStructBM = 0
        bDocEmpty = True

        ' Falls das Ziel-Dokument geschützt ist (was bei einem leeren doc nicht passieren sollte) -> schutz aufheben
        If oWordProps.docTarget.ProtectionType <> WdProtectionType.wdNoProtection Then
            oWordProps.docTarget.Unprotect(Password:=goOptions.sPassword_source)
        End If

        ' Füge alle Textmarken zusammen
        Call BeginEndBMtransformation()

        oBMrange = oWordProps.docSource.Range
        iBMcount = oBMrange.Bookmarks.Count
        goLogging.WriteLog("Number of all BM in Source document = " & iBMcount)


        ' Gehe alle BM im Quelldokument der REIHE NACH durch
        For I = 1 To iBMcount
            sBMname = oBMrange.Bookmarks(I).Name
            goLogging.WriteLog("--[ " & sBMname & " ]" & Right("-------------------------------------------------------------- --- -- -", 72 - Len(sBMname)))

            For a = 1 To goOptions.iFile_Sources
                If goOptions.GetFile_Bookmark(a) = sBMname Then

                    If goOptions.GetFile_Protected(a) <> "false" Then
                        bUnprotected = False
                    Else
                        bUnprotected = True
                    End If

                    If goOptions.GetFile_SetCaption(a) <> "false" Then
                        bNoCaption = False
                    Else
                        bNoCaption = True
                    End If

                    Exit For
                End If
            Next a
            goLogging.WriteLog("    " & sBMname & ": unprotected: " & bUnprotected)

            ' Überprüfe, ob sBMName eine Strukturierte Textmarke ist.
            bStructBM = False
            For j = 1 To oWordProps.iStructBM
                If sBMname = oWordProps.GetStructBM(j) Then
                    bStructBM = True
                    Exit For
                End If
            Next j

            ' wenn sBMname eine Strukturierte Textmarke ist:
            If bStructBM = True Then

                If oWordProps.docTarget.Bookmarks.Exists(sBMname) = True Then

                    goLogging.WriteLog("    " & sBMname & ": " & I & ". structured Bookmark.")

                    sStructBM = oWordProps.GetStructBM(j)
                    oStructBMrange = oWordProps.docSource.Bookmarks(sBMname).Range
                    iSourceBMcount = oStructBMrange.Bookmarks.Count

                    'Name of the last tablebookmark (needed to suppress the final Page Break) 
                    x = iSourceBMcount

                    Do While x > 0
                        If oStructBMrange.Bookmarks(x).Name Like sBM_TABLE Or oStructBMrange.Bookmarks(x).Name Like sBM_GRAPHIC Then Exit Do
                        x = x - 1
                    Loop
                    If x <> 0 Then sLastTableBMname = oStructBMrange.Bookmarks(x).Name

                    ' wenn die Strukt TM eine S_TM ist , dann...
                    If UCase(sBMname) Like sBM_STRUCT Then

                        iCountStructBM = iCountStructBM + 1

                        For x = 2 To iSourceBMcount  ' beginnt mit x=2, um die strukturierte TM selbst auszuschliessen

                            Call UpdateFeedback((I / iBMcount * 93) + 2, "Insertion of data tables into new document " & I & "/" & iBMcount)

                            ' da ich in dieser Schleife bin, werden Tabellen und ÜS in das Zieldokument eingefügt und deshalb
                            ' die Variable bDocEmpty auf false gesetzt

                            bDocEmpty = False
                            sSourceBMname = oStructBMrange.Bookmarks(x).Name

                            goLogging.WriteLog(oStructBMrange.Bookmarks(x).Name & ":  " & x & ". SubBM inside of " & sStructBM)

                            If x = 2 Then
                                oWordProps.docTarget.ActiveWindow.Selection.GoTo(What:=WdGoToItem.wdGoToBookmark, Name:=sBMname)
                                '                            oWord.Documents(oWordProps.sTargetDocName).ActiveWindow.Selection.StartOf Unit:=wdSection, Extend:=wdMove 
                            End If

                            If oStructBMrange.Bookmarks(x).Name Like sBM_UEBER And bNoCaption = False Then
                                If x = 2 Then
                                    Call Insert1stCaption(sSourceBMname, sBMname)
                                Else
                                    Call InsertCaption(sSourceBMname, sBMname)
                                End If
                            ElseIf oStructBMrange.Bookmarks(x).Name Like sBM_TABLE Or oStructBMrange.Bookmarks(x).Name Like sBM_GRAPHIC Then
                                Call InsertTable(sSourceBMname, bUnprotected)
                                If bNoCaption = True And sSourceBMname <> sLastTableBMname Then
                                    oWordProps.docTarget.ActiveWindow.Selection.InsertBreak(WdBreakType.wdPageBreak)
                                End If
                            End If

                            oWordProps.docTarget.UndoClear()
                            I = I + 1

                        Next x

                        ' wenn die Strukt TM keine S_TM ist , dann...
                    Else

                        For x = 2 To iSourceBMcount

                            sSourceBMname = oStructBMrange.Bookmarks(x).Name

                            With oWordProps.docTarget
                                If x = 2 Then
                                    .ActiveWindow.Selection.GoTo(What:=WdGoToItem.wdGoToBookmark, Name:=sStructBM)
                                    .ActiveWindow.Selection.Collapse(Direction:=WdCollapseDirection.wdCollapseStart)
                                    ' ^^^ sehr hilfreich, um eine Markierung auf den Anfang zu verkleinern
                                    Call InsertTable(sSourceBMname, bUnprotected)
                                    .Sections(iSelectedRange).ProtectedForForms = False
                                Else
                                    Call InsertTable(sSourceBMname, bUnprotected)
                                    .Sections(iSelectedRange).ProtectedForForms = False
                                End If
                                .UndoClear()
                                I = I + 1
                            End With

                        Next x

                    End If

                Else
                    ' Die TM ist in der Vorlage NICHT vorhanden
                    goLogging.WriteLog(sBMname & ":  " & I & ". structured bookmark, which is NOT part of the target document.")
                End If

                ' wenn sBMName KEINE Strukturierte TM ist, dann
            Else
                If oWordProps.docTarget.Bookmarks.Exists(sBMname) = True _
                And Not sBMname Like sBM_REF _
                And Not sBMname Like sBM_TABLE _
                And Not sBMname Like sBM_GRAPHIC _
                Then

                    ' TM ist in der Vorlage vorhanden und ist KEINE "Referenzierte Textmarke"
                    goLogging.WriteLog("    " & sBMname & ":  " & I & ". NON structured bookmark, which is part of the target document.")


                    With oWordProps.docTarget

                        oTargetBMrange = .Bookmarks(sBMname).Range
                        oTargetBMrange.Select()
                        .ActiveWindow.Selection.Delete()
                        oTargetBMrange.Select()

                        Call InsertTable(sBMname, bUnprotected)

                    End With

                Else
                    ' Die TM ist in der Vorlage NICHT vorhanden
                    goLogging.WriteLog(sBMname & ":  " & I & ". NON structured bookmark, which is NOT part of the target document or is part of another bookmark.")
                End If

            End If
            oWordProps.docTarget.UndoClear()
        Next I

        goLogging.WriteLog("--[ End of Bookmarks Loop ]----------------------------------------- --- -- -")

        If iSelectedRange <> Nothing Then
            oWordProps.docTarget.Sections(iSelectedRange).ProtectedForForms = False 'vorletzte Abschnitt des Result Bereiches nicht schützen
            ' Was passiert aber, wenn nach der Result TM noch eine weitere strukt.TM kommt  wenn-> Schaue Dir an, wie Du es bei den andere strukt TM gelöst hast
            ' ->  wrd.activeDocument.Sections(iSelectedRange - 1).ProtectedForForms = True  DANACH DANN  wrd.activeDocument.Sections(iSelectedRange).ProtectedForForms = False
        End If

        ' Überprüfe, ob Dokument leer blieb
        If Not bDocEmpty Then
            oWordProps.docTarget.CustomDocumentProperties("DOC_EMPTY").Value = False
        End If

        goLogging.WriteLog("   DOC_EMPTY = " & bDocEmpty)

        goLogging.WriteLog("End  : DocEmpty")

    End Sub

    '''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
    ' Sub-Prozedur        : DocFull
    ' Beschreibung        : Bearbeitet ein Dokument, welches bereits Inhalte enthält.
    '                       Diese werden dann beibehalten.
    ' Parameter           : -
    ' wird aufgerufen von : ConvertToWord
    ' Änderungshistorie   :
    ' Autor  Datum       Grund            Kommentar
    ' -----  ----------  ---------------  ---------------------------------------
    '
    '''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
    Sub DocFull()

        Dim I As Integer
        Dim j As Integer
        Dim x As Integer
        Dim oBMrange As Range
        Dim iBMcount As Integer
        Dim sBMname As String = ""
        Dim sStructBM As String
        Dim bStructBM As Boolean
        Dim oStructBMrange As Range
        Dim iSourceBMcount As Integer

        Dim sPreviousBMname As String
        Dim sSourceBMname As String         'sQuelleTMName
        Dim iSelectedRange As Integer       'nSelektAbschnitt
        Dim oTargetBMrange As Range         'ZielTMBereich
        Dim sLastTableBMname As String = ""

        Dim bUnprotected As Boolean
        Dim bNoCaption As Boolean
        Dim a As Integer

        Dim oBM As Bookmark

        goLogging.WriteLog("Start: DocFull")

        ' Dokument entsperren
        If goOptions.sPassword_source <> "" And goOptions.bUnprotected = False Then
            If oWordProps.docTarget.ProtectionType <> WdProtectionType.wdNoProtection Then
                oWordProps.docTarget.Unprotect(Password:=goOptions.sPassword_source)
            End If
        End If

        ' Füge alle Textmarken im Quelldokument zusammen
        Call BeginEndBMtransformation()

        oBMrange = oWordProps.docSource.Range
        iBMcount = oBMrange.Bookmarks.Count
        goLogging.WriteLog("Number of bookmarks in the source document = " & iBMcount)


        ' Gehe alle BM im Quelldokument der REIHE NACH durch
        For I = 1 To iBMcount
            sBMname = oBMrange.Bookmarks(I).Name
            goLogging.WriteLog("--[ " & sBMname & " ]" & Right("-------------------------------------------------------------- --- -- -", 72 - Len(sBMname)))

            For a = 1 To goOptions.iFile_Sources
                If goOptions.GetFile_Bookmark(a) = sBMname Then
                    If goOptions.GetFile_Protected(a) <> "false" Then
                        bUnprotected = False
                    Else
                        bUnprotected = True
                    End If
                    If goOptions.GetFile_SetCaption(a) <> "false" Then
                        bNoCaption = False
                    Else
                        bNoCaption = True
                    End If
                    Exit For
                End If
            Next a
            goLogging.WriteLog("    " & sBMname & ": unprotected: " & bUnprotected)

            ' Überprüfe, ob sBMName eine Strukturierte Textmarke ist.
            bStructBM = False
            For j = 1 To oWordProps.iStructBM
                If sBMname = oWordProps.GetStructBM(j) Then
                    bStructBM = True
                    Exit For
                End If
            Next j

            ' wenn sBMname eine Strukturierte Textmarke ist:
            If bStructBM = True Then

                ' Prüfen, ob sich die Reihenfolge der Sortierung verändert hat.
                CheckForChanges(sBMname)

                goLogging.WriteLog("    " & sBMname & ": " & I & ". structured bookmark.")

                sStructBM = oWordProps.GetStructBM(j)
                oStructBMrange = oWordProps.docSource.Bookmarks(sBMname).Range
                iSourceBMcount = oStructBMrange.Bookmarks.Count

                'Name of the last tablebookmark (needed to suppress the final Page Break) 
                x = iSourceBMcount
                Do While x > 0
                    If oStructBMrange.Bookmarks(x).Name Like sBM_TABLE Or oStructBMrange.Bookmarks(x).Name Like sBM_GRAPHIC Then Exit Do
                    x = x - 1
                Loop

                If x <> 0 Then sLastTableBMname = oStructBMrange.Bookmarks(x).Name

                If UCase(sBMname) Like sBM_STRUCT Then

                    goLogging.WriteLog("    " & sBMname & ": " & I & ". structured bookmark.")

                    ' beginnt mit x=2, um die strukturierte TM selbst auszuschliessen
                    For x = 2 To iSourceBMcount

                        Call UpdateFeedback((I / iBMcount * 92) + 2, "Insertion of data into existing document (" & CStr(x) & " of " & CStr(iSourceBMcount) & ")")


                        sSourceBMname = oStructBMrange.Bookmarks(x).Name
                        goLogging.WriteLog("    " & sSourceBMname & ":  " & x & ". SubBM inside of " & sStructBM)

                        ' wenn die TM im Zieldokument existiert
                        If oWordProps.docTarget.Bookmarks.Exists(sSourceBMname) = True Then


                            If oStructBMrange.Bookmarks(x).Name Like sBM_UEBER And bNoCaption = False Then
                                ' wenn die TM eine Überschrift ist, dann...
                                Call ReplaceCaption(sSourceBMname)
                            ElseIf oStructBMrange.Bookmarks(x).Name Like sBM_TABLE Or oStructBMrange.Bookmarks(x).Name Like sBM_GRAPHIC Then
                                ' wenn die TM eine Tabelle ist, dann...
                                Call ReplaceTable(sSourceBMname, bUnprotected)

                            End If

                            ' wenn die TM im Zieldokument NICHT existiert
                        Else

                            ' wenn x>2 dann kann die TM nicht die 1.ÜS sein
                            If x > 2 Then
                                If sSourceBMname Like sBM_UEBER And bNoCaption = False Then

                                    ' wenn die neue TM eine Überschrift ist ,dann...
                                    goLogging.WriteLog("    Insert Type 3")

                                    With oWordProps.docTarget.ActiveWindow.Selection
                                        sPreviousBMname = PreviousLegalBookmark(oStructBMrange, x - 1, bNoCaption)
                                        .GoTo(What:=WdGoToItem.wdGoToBookmark, Name:=sPreviousBMname)
                                        iSelectedRange = .Information(WdInformation.wdActiveEndSectionNumber)
                                        .GoTo(What:=WdGoToItem.wdGoToSection, Name:=iSelectedRange + 1)
                                    End With

                                    Call InsertCaption(sSourceBMname, sBMname)

                                ElseIf sSourceBMname Like sBM_TABLE Or sSourceBMname Like sBM_GRAPHIC Then
                                    ' wenn die neue TM eine Tabelle ist , dann....
                                    sPreviousBMname = PreviousLegalBookmark(oStructBMrange, x - 1, bNoCaption)

                                    ' Typ 1
                                    If sPreviousBMname Like sBM_UEBER And bNoCaption = False Then
                                        ' wenn die vorhergehende TM eine ÜS ist, dann
                                        goLogging.WriteLog("    Insert Type 1")
                                        With oWordProps.docTarget
                                            .ActiveWindow.ActivePane.View.SeekView = WdSeekView.wdSeekCurrentPageHeader
                                            .Bookmarks(sPreviousBMname).Range.Sections.Item(1).Range.Select()
                                            ' MS 20080302 Move to the end, or else the table will end up in the previous chapter (not good!)
                                            '.ActiveWindow.Selection.StartOf Unit:=wdSection, Extend:=wdMove
                                            .ActiveWindow.Selection.EndOf(Unit:=WdUnits.wdSection, Extend:=WdMovementType.wdMove)
                                        End With

                                        ' Typ 2
                                    Else
                                        'wenn die vorhergehende TM eine Tabelle ist , dann...
                                        goLogging.WriteLog("    Insert Type 2")
                                        With oWordProps.docTarget.ActiveWindow.Selection
                                            If bNoCaption = False Then
                                                .GoTo(What:=WdGoToItem.wdGoToBookmark, Name:=sPreviousBMname)
                                                iSelectedRange = .Information(WdInformation.wdActiveEndSectionNumber)
                                                .GoTo(What:=WdGoToItem.wdGoToSection, Name:=iSelectedRange + 1)
                                                .EndOf(Unit:=WdUnits.wdSection, Extend:=WdMovementType.wdMove) 'GEHE ANS ENDE DES ABSCHNITTES (weil davor eventuell Text steht!!!)
                                            Else
                                                'move to the end of the current S_ bookmark
                                                .GoTo(What:=WdGoToItem.wdGoToBookmark, Name:=sBMname)
                                                .MoveRight(Unit:=WdUnits.wdCharacter, Count:=1)
                                                .MoveLeft(Unit:=WdUnits.wdCharacter, Count:=1)
                                            End If
                                        End With

                                    End If

                                    Call InsertTable(sSourceBMname, bUnprotected)

                                    If bNoCaption = True And sSourceBMname <> sLastTableBMname Then
                                        oWordProps.docTarget.ActiveWindow.Selection.InsertBreak(Type:=WdBreakType.wdPageBreak)
                                    End If

                                End If
                                ' wenn x = 2. dann ist TM die 1. ÜS 
                            Else
                                goLogging.WriteLog("    Insert Type 4")
                                If bNoCaption = False Then InsertCaptionAs1stCaption(sSourceBMname, sBMname)
                            End If

                        End If
                        I = I + 1
                    Next x

                    ' wenn es nicht die RESULT TM ist, dann
                Else
                    ' beginnt mit x=2, um die strukturierte TM selbst auszuschliessen
                    For x = 2 To iSourceBMcount
                        sSourceBMname = oStructBMrange.Bookmarks(x).Name
                        If oWordProps.docTarget.Bookmarks.Exists(sSourceBMname) = True Then  ' geprüft und OK

                            Call ReplaceTable(sSourceBMname, bUnprotected)

                        Else
                            With oWordProps.docTarget.ActiveWindow.Selection
                                If x > 2 Then ' geprüft und OK
                                    sPreviousBMname = PreviousLegalBookmark(oStructBMrange, x - 1, bNoCaption)
                                    .GoTo(What:=WdGoToItem.wdGoToBookmark, Name:=sPreviousBMname)
                                    iSelectedRange = .Information(WdInformation.wdActiveEndSectionNumber)
                                    .GoTo(What:=WdGoToItem.wdGoToSection, Name:=iSelectedRange + 1)
                                    .EndOf(Unit:=WdUnits.wdSection, Extend:=WdMovementType.wdMove) 'GEHE ANS ENDE DES ABSCHNITTES (weil davor eventuell Text steht!!!)

                                    Call InsertTable(sSourceBMname, bUnprotected)

                                Else ' wenn x = 2 ( geprüft und OK )
                                    .GoTo(What:=WdGoToItem.wdGoToBookmark, Name:=sStructBM)
                                    .Collapse(Direction:=WdCollapseDirection.wdCollapseStart)

                                    Call InsertTable(sSourceBMname, bUnprotected)

                                End If
                            End With
                        End If
                    Next x
                End If

                'Call AlignSourceWithTarget(sBMname)

                ' wenn es keine strukturierte TM ist
            Else

                If oWordProps.docTarget.Bookmarks.Exists(sBMname) = True _
                And Not sBMname Like sBM_REF _
                And Not sBMname Like sBM_TABLE _
                And Not sBMname Like sBM_GRAPHIC _
                Then

                    ' TM ist in der Vorlage vorhanden und ist KEINE "Referenzierte Textmarke"
                    goLogging.WriteLog("    " & sBMname & ":  " & I & ". NON structured bookmark, which is part of the target document.")



                    With oWordProps.docTarget

                        goLogging.WriteLog("    Length of target Bookmark " & Len(.Bookmarks(sBMname).Range.Text))

                        If Len(oWordProps.docTarget.Bookmarks(sBMname).Range.Text) < 5 Then

                            oTargetBMrange = .Bookmarks(sBMname).Range
                            oTargetBMrange.Select()
                            .ActiveWindow.Selection.Delete()
                            oTargetBMrange.Select()

                            Call InsertTable(sBMname, bUnprotected)

                        Else
                            Call ReplaceTable(sBMname, bUnprotected)
                        End If

                    End With

                End If
            End If
        Next I

        goLogging.WriteLog("--[ End of Bookmarks Loop ]----------------------------------------- --- -- -")

        'Align Source With Target
        'durch alle Strukturierten Bookmarks im TARGET gehen und schaun, ob sich was getan hat
        With oWordProps.docTarget
            For Each oBM In .Bookmarks
                If oBM.Name Like sBM_STRUCT Then

                    bNoCaption = True

                    For a = 1 To goOptions.iFile_Sources
                        If goOptions.GetFile_Bookmark(a) = sBMname Then
                            If goOptions.GetFile_SetCaption(a) <> "false" Then bNoCaption = False
                            Exit For
                        End If
                    Next a

                    If bNoCaption = True Then
                        Call AlignSourceWithTarget(oBM.Name)
                        'Call AlignSourceWithTargetLegacy(oBM.Name)
                    Else
                        Call AlignSourceWithTargetLegacy(oBM.Name)
                    End If
                End If
            Next oBM
        End With

        'Statische Bookmarks leeren
        Call ClearStaticBookmarks()

        goLogging.WriteLog("End  : DocFull")

    End Sub


    '''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
    ' Sub-Prozedur        : BeginEndBMtransformation
    ' Beschreibung        : Diese Prozedur fügt folgende Textmarken zusammen:
    '                        - Überschriften_Begin_Textmarken und Überschriften_End_Textmarken => ÜS_Textmarken            z.B. UEBER1
    '                        - Tabellen_Begin_Textmarken und Tabellen_End_Textmarken           => Tab_Textmarke            z.B. A1
    '                        - Formatierungs_Begin_Textmarke und Formatierungs_End_Textmarke   => Formatierungs_Textmarke  z.B. _TPASR02
    '                        - Referenzierte_Begin_Textmarke und Referenzierte_End_Textmarke   => Referenzierte_Textmarke  z.B. ID12345
    ' Parameter           :
    ' wird aufgerufen von : EmptyDoc, FullDoc
    ' mögliche Fehler     : Eine oder beide der Textmarken (Begin oder End) sind nicht vorhanden
    ' Änderungshistorie   :
    ' Autor  Datum       Grund            Kommentar
    ' -----  ----------  ---------------  ---------------------------------------
    '
    '''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
    Sub BeginEndBMtransformation()

        Dim iBMcount As Integer ' Anzahl der Bookmarks
        Dim I As Integer

        Dim aBookmarks() As Object
        Dim sTempBookmarkName As String

        Dim oRange As Range

        Dim iPos As Integer
        Dim sBookmark1 As String
        Dim sBookmark2 As String
        Dim sBookmark3 As String
        Dim sFormat As String
        Dim sFrame As String
        Dim iLine As Integer
        Dim sBackground As String

        goLogging.WriteLog("Start: BeginEndBMtransformationExt")

        oWordProps.docSource.Bookmarks.ShowHidden = True

        iBMcount = oWordProps.docSource.Range.Bookmarks.Count
        ReDim aBookmarks(iBMcount)

        ' Schreibe die Textmarken nach der Position im Dokument in das Array
        For I = 1 To iBMcount
            aBookmarks(I) = oWordProps.docSource.Range.Bookmarks(I).Name
        Next I

        I = 1
        Do While I < iBMcount

            Call UpdateFeedback(1, "Transforming bookmarks " & I & "/" & iBMcount)

            sTempBookmarkName = aBookmarks(I)

            ' Wenn die TM eine unsichtbare TM ist, dann
            If Left(sTempBookmarkName, 1) = "_" Then

                ' Wenn die TM ein END beinhaltet, dann
                If sTempBookmarkName Like sBM_END Then

                    'Styles extrahieren
                    sFormat = Right(sTempBookmarkName, 3)
                    sFrame = Mid(sFormat, 1, 1)
                    sBackground = Mid(sFormat, 2, 1)
                    iLine = Mid(sFormat, 3, 1)

                    'Bookmarks zusammenbauen
                    iPos = InStr(1, sTempBookmarkName, "END")
                    sBookmark1 = sTempBookmarkName
                    sBookmark2 = Left(sTempBookmarkName, iPos - 1) & "BGN"
                    sBookmark3 = Left(sTempBookmarkName, iPos - 1) & sFrame & sBackground & iLine

                    goLogging.WriteLog("    " & sBM_END & ":")
                    goLogging.WriteLog("    sBookmark1 = " & sBookmark1)
                    goLogging.WriteLog("    sBookmark2 = " & sBookmark2)
                    goLogging.WriteLog("    sBookmark3 = " & sBookmark3)
                    goLogging.WriteLog("    Rahmen = " & sFrame)
                    goLogging.WriteLog("    iLine = " & iLine)

                    With oWordProps.docSource
                        oRange = .Range(Start:=.Bookmarks(sBookmark2).Start, End:=.Bookmarks(sBookmark1).End)
                        oRange.Select()
                        .Bookmarks.Add(Range:=.ActiveWindow.Selection.Range, Name:=sBookmark3)
                        .Bookmarks(sBookmark1).Delete()
                        .Bookmarks(sBookmark2).Delete()
                    End With

                Else
                    goLogging.WriteLog("    sTempBookmarkName = " & sTempBookmarkName)
                End If

                ' Wenn die TM KEINE unsichtbare TM ist, dann
            Else

                ' wenn die TM ein BEGIN beinhaltet, dann
                If sTempBookmarkName Like sBM_BEGIN Then

                    iPos = InStr(1, sTempBookmarkName, "BGN")
                    sBookmark1 = sTempBookmarkName
                    sBookmark2 = Left(sTempBookmarkName, iPos - 1) & "END"
                    sBookmark3 = Left(sTempBookmarkName, iPos - 2)

                    goLogging.WriteLog("    " & sBM_BEGIN & ":")
                    goLogging.WriteLog("    sBookmark1 = " & sBookmark1)
                    goLogging.WriteLog("    sBookmark2 = " & sBookmark2)
                    goLogging.WriteLog("    sBookmark3 = " & sBookmark3)

                    With oWordProps.docSource
                        oRange = .Range(Start:=.Bookmarks(sBookmark1).Start, End:=.Bookmarks(sBookmark2).End)
                        oRange.Select()
                        .Bookmarks.Add(Range:=.ActiveWindow.Selection.Range, Name:=sBookmark3)
                        .Bookmarks(sBookmark1).Delete()
                        .Bookmarks(sBookmark2).Delete()

                        If UCase(sTempBookmarkName) Like sBM_GRAPHIC Then
                            goLogging.WriteLog("Graphic found: " & sTempBookmarkName)
                        End If

                    End With

                Else
                    goLogging.WriteLog("    sTempBookmarkName = " & sTempBookmarkName)
                End If

            End If

            oWordProps.docSource.UndoClear()
            I = I + 1
        Loop

        oWordProps.docSource.Bookmarks.ShowHidden = False

        goLogging.WriteLog("End  : BeginEndBMtransformationExt")

    End Sub


    '''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
    ' Sub-Prozedur        : FormatRange
    ' Beschreibung        : Formatiert den übergebenen Abschnitt
    ' Parameter           : oFormatRange - Abschnitt, welcher formatiert werden soll
    ' wird aufgerufen von : ~
    ' Änderungshistorie   :
    ' Autor  Datum       Grund            Kommentar
    ' -----  ----------  ---------------  ---------------------------------------
    '
    '''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
    Sub FormatRange(oFormatRange As Range)

        Dim oFormatRangeTemp As Range
        Dim I As Integer
        Dim sBMname As String
        Dim sFrame As String
        Dim iLine As Integer
        Dim sBackground As String

        goLogging.WriteLog("Start: FormatRange")

        oFormatRange.Bookmarks.ShowHidden = True

        For I = 1 To oFormatRange.Bookmarks.Count

            sBMname = oFormatRange.Bookmarks(I).Name

            ' Ersetze in der Tabelle "TPAS" <br> durch einen Zeilenumbruch
            If sBMname = sTABLENAME Then

                oFormatRangeTemp = oFormatRange.Bookmarks(sBMname).Range
                With oFormatRangeTemp.Find
                    .ClearFormatting()
                    .Text = "<br>"

                    With .Replacement
                        .ClearFormatting()
                        .Text = Chr(11)
                    End With

                    .Execute(Replace:=WdReplace.wdReplaceAll, Format:=True, MatchCase:=True, MatchWholeWord:=True)
                End With

            End If

            ' wenn das Style-Sheet fehlerhaft ist, könnne "BEGIN" - Textmarken übrig bleiben !!!
            If (InStr(1, sBMname, "BEGIN", vbTextCompare) > 0) Then
                goLogging.WriteLog("    INCONSITENCY: this bookmark is left over: " + sBMname)
            End If

            If (Left(sBMname, 1) = "_") And (InStr(1, sBMname, "BEGIN", vbTextCompare) = 0) And (InStr(1, sBMname, "REF", vbTextCompare) = 0) And sBMname <> "_GoBack" Then

                ' Ermittle die Formatierungs Anweisungen RAHMEN und LINIE
                sFrame = Mid(Right(sBMname, 3), 1, 1)
                sBackground = Mid(Right(sBMname, 3), 2, 1)
                iLine = Mid(Right(sBMname, 3), 3, 1)

                ' Selektiere den zu formatierenden Bereich
                oFormatRange.Bookmarks(sBMname).Select()

                ' Rufe die Formatierung auf
                '************************************************************
                Call Formating(sFrame, sBackground, iLine)
                '************************************************************
            End If

        Next I

        ' setze die unsichtbaren Textmarken wieder unsichtbar
        oFormatRange.Bookmarks.ShowHidden = False

        goLogging.WriteLog("End  : FormatRange")

    End Sub

    '''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
    ' Sub-Prozedur        : Formating
    ' Beschreibung        : Formatiert das Aussehen einer Tabelle anhand der Übergabewerte
    ' Parameter           : sLocFrame - Kennzeichnung für den Rahmentyp
    '                       nLocBackground - Kennzeichnung für die Hintergrundart
    '                       nLocLine - Kennzeichnung für die Linienart
    ' wird aufgerufen von : FormatRange
    ' Änderungshistorie   :
    ' Autor  Datum       Grund            Kommentar
    ' -----  ----------  ---------------  ---------------------------------------
    '
    '''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
    Public Sub Formating(sLocFrame As String, nLocBackground As String, nLocLine As Integer)

        ' 2005-07-25, (nb) - klappt nur, wenn auch wirklich eine Tabelle markiert ist
        ' -> diese Funktion setzt voraus, das immerdie GANZE Tabelle markiert ist .... trifft nicht immer (eigentlich nie) zu
        '
        On Error Resume Next
        oWord.Selection.Rows.AllowBreakAcrossPages = False
        On Error GoTo 0

        With oWord.Selection.Cells

            Select Case sLocFrame

                Case "A"
                    ' BackgroundColor festlegen
                    With .Shading
                        .Texture = WdTextureIndex.wdTextureNone
                        .ForegroundPatternColor = WdColor.wdColorAutomatic
                        .BackgroundPatternColor = BackgroundColor(nLocBackground)
                    End With

                    ' Rahmen und Linie setzten
                    .Borders(WdBorderType.wdBorderLeft).LineStyle = LineType(nLocLine)
                    .Borders(WdBorderType.wdBorderRight).LineStyle = LineType(nLocLine)
                    .Borders(WdBorderType.wdBorderTop).LineStyle = LineType(nLocLine)
                    .Borders(WdBorderType.wdBorderBottom).LineStyle = LineType(nLocLine)

                Case "B"
                    ' Rahmenlinie = links
                    ' Hintergrundfarbe festlegen
                    With .Shading
                        .Texture = WdTextureIndex.wdTextureNone
                        .ForegroundPatternColor = WdColor.wdColorAutomatic
                        .BackgroundPatternColor = BackgroundColor(nLocBackground)
                    End With

                    ' Rahmen und Linie setzten
                    With .Borders(WdBorderType.wdBorderLeft)
                        .LineStyle = LineType(nLocLine)
                        .LineWidth = WdLineWidth.wdLineWidth050pt
                        .Color = WdColor.wdColorAutomatic
                    End With

                    .Borders.Shadow = False

                Case "C"
                    ' Rahmenlinie = unten
                    ' Hintergrundfarbe festlegen
                    With .Shading
                        .Texture = WdTextureIndex.wdTextureNone
                        .ForegroundPatternColor = WdColor.wdColorAutomatic
                        .BackgroundPatternColor = BackgroundColor(nLocBackground)
                    End With

                    ' Rahmen und Linie setzten
                    .Borders(WdBorderType.wdBorderLeft).LineStyle = WdLineStyle.wdLineStyleNone
                    .Borders(WdBorderType.wdBorderRight).LineStyle = WdLineStyle.wdLineStyleNone
                    .Borders(WdBorderType.wdBorderTop).LineStyle = WdLineStyle.wdLineStyleNone
                    .Borders(WdBorderType.wdBorderBottom).LineStyle = LineType(nLocLine)

                    .Borders.Shadow = False

                Case "D"
                    ' Rahmenlinie = rechts
                    ' Hintergrundfarbe festlegen
                    With .Shading
                        .Texture = WdTextureIndex.wdTextureNone
                        .ForegroundPatternColor = WdColor.wdColorAutomatic
                        .BackgroundPatternColor = BackgroundColor(nLocBackground)
                    End With

                    ' Rahmen und Linie setzten
                    .Borders(WdBorderType.wdBorderLeft).LineStyle = WdLineStyle.wdLineStyleNone
                    .Borders(WdBorderType.wdBorderRight).LineStyle = LineType(nLocLine)
                    .Borders(WdBorderType.wdBorderTop).LineStyle = WdLineStyle.wdLineStyleNone
                    .Borders(WdBorderType.wdBorderBottom).LineStyle = WdLineStyle.wdLineStyleNone
                    .Borders(WdBorderType.wdBorderHorizontal).LineStyle = WdLineStyle.wdLineStyleNone
                    .Borders(WdBorderType.wdBorderVertical).LineStyle = WdLineStyle.wdLineStyleNone
                    .Borders(WdBorderType.wdBorderDiagonalDown).LineStyle = WdLineStyle.wdLineStyleNone
                    .Borders(WdBorderType.wdBorderDiagonalUp).LineStyle = WdLineStyle.wdLineStyleNone

                Case "E"
                    ' Rahmenlinie = oben
                    ' Hintergrundfarbe festlegen
                    With .Shading
                        .Texture = WdTextureIndex.wdTextureNone
                        .ForegroundPatternColor = WdColor.wdColorAutomatic
                        .BackgroundPatternColor = BackgroundColor(nLocBackground)
                    End With

                    ' Rahmen und Linie setzten
                    .Borders(WdBorderType.wdBorderLeft).LineStyle = WdLineStyle.wdLineStyleNone
                    .Borders(WdBorderType.wdBorderRight).LineStyle = WdLineStyle.wdLineStyleNone
                    .Borders(WdBorderType.wdBorderTop).LineStyle = LineType(nLocLine)

                    .Borders.Shadow = False

                Case "F"
                    ' Rahmenlinie = Mitte-Horizontal, Mitte-Vertikal ( 2 in der Mitte gekreuzte Linie)
                    ' Hintergrundfarbe festlegen
                    With .Shading
                        .Texture = WdTextureIndex.wdTextureNone
                        .ForegroundPatternColor = WdColor.wdColorAutomatic
                        .BackgroundPatternColor = BackgroundColor(nLocBackground)
                    End With

                    ' Rahmen und Linie setzten
                    .Borders(WdBorderType.wdBorderHorizontal).LineStyle = LineType(nLocLine)
                    .Borders(WdBorderType.wdBorderVertical).LineStyle = LineType(nLocLine)

                Case "G" ' wie Typ A, Selection wird als Tabellenüberschrift markieren
                    ' BackgroundColor festlegen
                    With .Shading
                        .Texture = WdTextureIndex.wdTextureNone
                        .ForegroundPatternColor = WdColor.wdColorAutomatic
                        .BackgroundPatternColor = BackgroundColor(nLocBackground)
                    End With

                    ' Rahmen und Linie setzten
                    .Borders(WdBorderType.wdBorderLeft).LineStyle = LineType(nLocLine)
                    .Borders(WdBorderType.wdBorderRight).LineStyle = LineType(nLocLine)
                    .Borders(WdBorderType.wdBorderTop).LineStyle = LineType(nLocLine)
                    .Borders(WdBorderType.wdBorderBottom).LineStyle = LineType(nLocLine)

                    ' Selection als Tabellenüberschrift markieren
                    oWord.Selection.Rows.HeadingFormat = True

                Case "H"
                    ' BackgroundColor festlegen
                    With .Shading
                        .Texture = WdTextureIndex.wdTextureNone
                        .ForegroundPatternColor = WdColor.wdColorAutomatic
                        .BackgroundPatternColor = BackgroundColor(nLocBackground)
                    End With

                    ' Rahmen und Linie setzten
                    .Borders(WdBorderType.wdBorderVertical).LineStyle = LineType(nLocLine)
                    .Borders(WdBorderType.wdBorderHorizontal).LineStyle = LineType(nLocLine)
                    .Borders(WdBorderType.wdBorderVertical).LineWidth = WdLineWidth.wdLineWidth025pt
                    .Borders(WdBorderType.wdBorderHorizontal).LineWidth = WdLineWidth.wdLineWidth025pt

                Case "I"
                    With oWord.Selection
                        .Fields.Add(Range:=oWord.Selection.Range, Type:=WdFieldType.wdFieldPage)
                        Select Case nLocBackground
                            Case "A" 'von
                                .TypeText(Text:=" von ")
                            Case "B" 'of
                                .TypeText(Text:=" of ")
                            Case Else
                                .TypeText(Text:="/")
                        End Select
                        .Fields.Add(Range:=oWord.Selection.Range, Type:=WdFieldType.wdFieldNumPages)
                    End With

                Case Else

            End Select

        End With

    End Sub

    '''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
    ' Funktion            : LineType
    ' Beschreibung        : Bestimmt den Linienstil anhand des übergebenen Wertes
    ' Parameter           : nLocNum - Kennzeichnung für die Linienart
    ' wird aufgerufen von : Formating
    ' Rückgabewert        : wdLineStyleDouble oder wdLineStyleSingle
    ' Änderungshistorie   :
    ' Autor  Datum       Grund            Kommentar
    ' -----  ----------  ---------------  ---------------------------------------
    '
    '''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
    Public Function LineType(nLocNum As Integer) As String

        Dim sStyle As String = ""

        Select Case nLocNum

            Case 1
                ' LinienTyp = doppelt
                sStyle = WdLineStyle.wdLineStyleDouble
            Case 2
                ' LinienTyp = einfach
                sStyle = WdLineStyle.wdLineStyleSingle
        End Select

        LineType = sStyle

    End Function

    Public Function BackgroundColor(nLocBG As String) As String
        '''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
        ' Funktion            : BackgroundColor
        ' Beschreibung        : Bestimmt die Farbe des Hintergrundes anhand des übergebenen Wertes
        ' Parameter           : nLocBG - Kennzeichnung für den Hintergrund
        ' wird aufgerufen von : Formating
        ' Rückgabewert        : wdColorAutomatic oder wdColorGray05/10/125
        ' Änderungshistorie   :
        ' Autor  Datum       Grund            Kommentar
        ' -----  ----------  ---------------  ---------------------------------------
        '
        '''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''

        Dim sBackground As String = ""

        Select Case nLocBG
            Case "M"
                ' KEINE Schattierung
                sBackground = WdColor.wdColorAutomatic
            Case "N"
                ' Schattierung Grau 5%
                sBackground = WdColor.wdColorGray05
            Case "O"
                ' Schattierung Grau 10%
                sBackground = WdColor.wdColorGray10
            Case "P"
                ' Schattierung Grau 12,5%
                sBackground = WdColor.wdColorGray125
        End Select

        BackgroundColor = sBackground

    End Function

    '''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
    ' Sub-Prozedur        : Insert1stCaption
    ' Beschreibung        : Fügt die erste Überschrift in das Zieldokument ein
    ' Parameter           : sSourceBMname - Name der Quelltextmarke
    ' wird aufgerufen von : EmptyDoc
    ' Änderungshistorie   :
    ' Autor  Datum       Grund            Kommentar
    ' -----  ----------  ---------------  ---------------------------------------
    '
    '''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
    Sub Insert1stCaption(sSourceBMname As String, sBMname As String)   'fuege_erste_Ueberschrift_ein

        Dim nSectionNumber As Integer
        Dim nPageNumer As Integer
        Dim oHeader As HeaderFooter
        Dim oRange As Range
        Dim oBookmark As Bookmark
        Dim oSourceBMrange As Range
        Dim oTargetBMrange As Range

        goLogging.WriteLog("Start: Insert1stCaption")

        With oWordProps.docTarget

            'Position der Einfügemarke vermerken
            goLogging.WriteLog("  Cursorposition         = " & .ActiveWindow.Selection.Range.Start & "." & .ActiveWindow.Selection.Range.End)

            nSectionNumber = .Bookmarks(sBMname).Range.Information(WdInformation.wdActiveEndSectionNumber)
            nPageNumer = .Bookmarks(sBMname).Range.Information(WdInformation.wdActiveEndPageNumber)

            'Abschnittsnummer und Seite merken
            goLogging.WriteLog("  nSectionNumber         = " & nSectionNumber)
            goLogging.WriteLog("  nPageNumer             = " & nPageNumer)

            oHeader = .Sections(nSectionNumber).Headers(WdHeaderFooterIndex.wdHeaderFooterPrimary)

            'Merken, ob die Kopfzeile mit der vorherigen verbunden ist
            goLogging.WriteLog("  oHeader.LinkToPrevious = " & oHeader.LinkToPrevious)

            oRange = oHeader.Range
            oRange.SetRange(Start:=oHeader.Range.End - 3, End:=oHeader.Range.End)
            oHeader.Range.Bookmarks.Add(sSourceBMname, oRange)
            oSourceBMrange = oWordProps.docSource.Bookmarks(sSourceBMname).Range

            '************************************************
            Call FormatRange(oSourceBMrange)
            '************************************************

            oTargetBMrange = .Bookmarks(sSourceBMname).Range
            oTargetBMrange.FormattedText = oSourceBMrange.FormattedText

            'Setzte den Cursor auf die 1. Seite nach Resulttext
            oBookmark = .Bookmarks(sBMname)
            oRange = oBookmark.Range
            oRange.SetRange(Start:=oBookmark.Start, End:=oBookmark.Start)
            oRange.Select()

            goLogging.WriteLog("  Cursorposition = " & .ActiveWindow.Selection.Range.Start & "." & .ActiveWindow.Selection.Range.End)
            goLogging.WriteLog("  Cursor is positioned on page " & oRange.Information(WdInformation.wdActiveEndPageNumber))
            goLogging.WriteLog("  Cursor is positioned in section " & oRange.Information(WdInformation.wdActiveEndSectionNumber))

            goLogging.WriteLog("End  : Insert1stCaption")

        End With


    End Sub

    '''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
    ' Sub-Prozedur        : InsertCaption
    ' Beschreibung        : Fügt eine Überschrift in das Zieldokument ein
    ' Parameter           : sSourceBMname - Name der Quelltextmarke
    ' wird aufgerufen von : EmptyDoc
    ' Änderungshistorie   :
    ' Autor  Datum       Grund            Kommentar
    ' -----  ----------  ---------------  ---------------------------------------
    '
    '''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
    Sub InsertCaption(sSourceBMname As String, sBMname As String)

        Dim oHeader As HeaderFooter
        Dim iSelectedRange As Integer
        Dim oTargetBMrangeTable As Object
        Dim oTargetTable As Range
        Dim oSourceBMrange As Range
        Dim iHeaderTableCount As Integer

        goLogging.WriteLog("Start: InsertCaption")

        With oWordProps.docTarget

            'Position der Einfügemarke vermerken
            goLogging.WriteLog("  Cursorposition                = " & .ActiveWindow.Selection.Range.Start & "." & .ActiveWindow.Selection.Range.End)

            .ActiveWindow.Selection.InsertBreak(WdBreakType.wdSectionBreakNextPage)  ' hier kommen die sich aufschaukelnden Zeilen her
            iSelectedRange = .ActiveWindow.Selection.Information(WdInformation.wdActiveEndSectionNumber)
            .Sections(iSelectedRange - 1).ProtectedForForms = False
            goLogging.WriteLog("  iSelectedRange                = " & iSelectedRange)

            oHeader = .Sections(iSelectedRange).Headers(WdHeaderFooterIndex.wdHeaderFooterPrimary)
            goLogging.WriteLog("  oHeader.LinkToPrevious before = " & oHeader.LinkToPrevious)
            oHeader.LinkToPrevious = False
            goLogging.WriteLog("  oHeader.LinkToPrevious after  = " & oHeader.LinkToPrevious)

            iHeaderTableCount = oHeader.Range.Tables.Count
            oTargetBMrangeTable = oHeader.Range.Tables(iHeaderTableCount)

            oTargetTable = oTargetBMrangeTable.Range
            oTargetTable.Columns.Delete()

            '**********************************************************************************************
            'Wenn ein oder mehrere Tabellen vorhanden sein sollten, muss zusätzlich ein Return-Zeichen mitausgewählt werden.
            'Workaround für ältere Word Versionen (bis 2003)? In neueren führt das zu ganz komischen Effekten.
            '**********************************************************************************************

            If iWordVer <= 11 Then
                If iHeaderTableCount = 1 Then
                    oTargetTable.SetRange(Start:=oTargetTable.Start - 1, End:=oTargetTable.End)
                End If

                If iHeaderTableCount > 1 Then
                    oTargetTable.SetRange(Start:=oTargetTable.Start - 2, End:=oTargetTable.End)
                End If
            End If

            oSourceBMrange = oWordProps.docSource.Bookmarks(sSourceBMname).Range
            '************************************************
            Call FormatRange(oSourceBMrange)
            '************************************************
            oTargetTable.FormattedText = oSourceBMrange.FormattedText

            'Wo sitzt der Cursor
            goLogging.WriteLog("  Cursorposition         = " & .ActiveWindow.Selection.Range.Start & "." & .ActiveWindow.Selection.Range.End)

        End With

        goLogging.WriteLog("End  : InsertCaption")

    End Sub

    '''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
    ' Sub-Prozedur        : InsertCaptionAs1stCaption
    ' Beschreibung        : Fügt eine Überschrift als erste Überschrift ein
    ' Parameter           : sSourceBMname - Name der Quelltextmarke
    ' wird aufgerufen von : DocFull
    ' Änderungshistorie   :
    ' Autor  Datum       Grund            Kommentar
    ' -----  ----------  ---------------  ---------------------------------------
    '
    '''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
    Sub InsertCaptionAs1stCaption(sSourceBMname As String, sBMname As String)

        Dim oTargetBMrangeTable As Object
        Dim iSelectedRange As Integer
        Dim sTempBMname As String
        Dim oHeader As HeaderFooter
        Dim oTargetTable As Range
        Dim oSourceBMrange As Range
        Dim oTargetBMrange As Range

        goLogging.WriteLog("Start: InsertCaptionAs1stCaption")

        With oWordProps.docTarget

            .ActiveWindow.Selection.GoTo(What:=WdGoToItem.wdGoToBookmark, Name:=sBMname)
            .ActiveWindow.Selection.StartOf(Unit:=WdUnits.wdSection, Extend:=WdMovementType.wdMove)

            iSelectedRange = .ActiveWindow.Selection.Information(WdInformation.wdActiveEndSectionNumber)
            .ActiveWindow.ActivePane.View.SeekView = WdSeekView.wdSeekCurrentPageHeader
            oTargetBMrange = .Sections(iSelectedRange).Headers(WdHeaderFooterIndex.wdHeaderFooterPrimary).Range

            '##########################################
            ' achtgeben auf nachfolgende Zeile, falls man manuelle die Kopfzeilenverknüpfung getrennt hat, da automatisch der Bookmark in der unteren Kopfzeile VERSCHWINDET!

            'Ja ich weiss, das ist ein wirklich wüster Hack. Nicht zuhause nachmachen.
            On Error GoTo NoBookmarkError
            sTempBMname = oTargetBMrange.Bookmarks(1).Name
            GoTo NoBookmarkCont
NoBookmarkError:
            .ActiveWindow.ActivePane.View.SeekView = WdSeekView.wdSeekMainDocument
            Call Insert1stCaption(sSourceBMname, sBMname)
            sTempBMname = sSourceBMname
            GoTo EndSub
NoBookmarkCont:
            On Error GoTo 0

            '##########################################
            .ActiveWindow.ActivePane.View.SeekView = WdSeekView.wdSeekMainDocument
            '.ActiveWindow.Selection.InsertBreak Type:=wdSectionBreakNextPage
            iSelectedRange = .ActiveWindow.Selection.Information(WdInformation.wdActiveEndSectionNumber)

            oHeader = .Sections(iSelectedRange).Headers(WdHeaderFooterIndex.wdHeaderFooterPrimary)
            goLogging.WriteLog("oHeader.LinkToPrevious before = " & oHeader.LinkToPrevious)
            oHeader.LinkToPrevious = False
            goLogging.WriteLog("oHeader.LinkToPrevious after  = " & oHeader.LinkToPrevious)

            'Die letzte Tabelle im Header selektieren
            oTargetBMrangeTable = oHeader.Range.Tables(oHeader.Range.Tables.Count)

            oTargetTable = oTargetBMrangeTable.Range
            oTargetTable.Select()
            .ActiveWindow.Selection.Bookmarks.Add(sTempBMname, Range:=oWord.Selection.Range)
            .ActiveWindow.Selection.GoTo(What:=WdGoToItem.wdGoToBookmark, Name:=sBMname)
            .ActiveWindow.Selection.StartOf(Unit:=WdUnits.wdSection, Extend:=WdMovementType.wdMove)
            iSelectedRange = .ActiveWindow.Selection.Information(WdInformation.wdActiveEndSectionNumber)

            oHeader = .Sections(iSelectedRange).Headers(WdHeaderFooterIndex.wdHeaderFooterPrimary)
            goLogging.WriteLog("oHeader.LinkToPrevious before = " & oHeader.LinkToPrevious)
            oHeader.LinkToPrevious = False
            goLogging.WriteLog("oHeader.LinkToPrevious after  = " & oHeader.LinkToPrevious)

            'Die letzte Tabelle im Header selektieren
            oTargetBMrangeTable = oHeader.Range.Tables(oHeader.Range.Tables.Count)

            oTargetTable = oTargetBMrangeTable.Range
            oTargetTable.Select()
            .ActiveWindow.Selection.Rows.Delete()
            oSourceBMrange = oWordProps.docSource.Bookmarks(sSourceBMname).Range

            '************************************************
            Call FormatRange(oSourceBMrange)
            '************************************************

            oTargetTable.FormattedText = oSourceBMrange.FormattedText


            ' Code generated by Macro Begin
            ' Force Change to Print View Mode
            If .ActiveWindow.View.SplitSpecial <> WdSpecialPane.wdPaneNone Then
                .ActiveWindow.Panes(2).Close()
            End If
            If .ActiveWindow.ActivePane.View.Type = WdViewType.wdNormalView Or .ActiveWindow.ActivePane.View.Type = WdViewType.wdOutlineView Then
                .ActiveWindow.ActivePane.View.Type = WdViewType.wdPrintView
            End If
            .ActiveWindow.ActivePane.View.SeekView = WdSeekView.wdSeekCurrentPageHeader
            If .ActiveWindow.View.SplitSpecial = WdSpecialPane.wdPaneNone Then
                .ActiveWindow.ActivePane.View.Type = WdViewType.wdPrintView
            Else
                .ActiveWindow.View.Type = WdViewType.wdPrintView
            End If
            ' Code generated by Macro End

            .ActiveWindow.ActivePane.View.SeekView = WdSeekView.wdSeekMainDocument

        End With

EndSub:

        goLogging.WriteLog("End  : InsertCaptionAs1stCaption")

    End Sub

    '''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
    ' Sub-Prozedur        : InsertTable
    ' Beschreibung        : Fügt eine Tabelle in das Zieldokument ein
    ' Parameter           : sSourceBMname - Name der Quelltextmarke
    ' wird aufgerufen von : DocEmpty, DocFull
    ' Änderungshistorie   :
    ' Autor  Datum       Grund            Kommentar
    ' -----  ----------  ---------------  ---------------------------------------
    '
    '''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
    Sub InsertTable(sSourceBMname As String, bUnprotected As Boolean)

        Dim iSelectedRange As Integer
        Dim oSourceBMrange As Range
        Dim oTargetBMrange As Range

        goLogging.WriteLog("Start: InsertTable")

        With oWordProps.docTarget

            If goOptions.bUnprotected = False Then
                .ActiveWindow.Selection.InsertBreak(Type:=WdBreakType.wdSectionBreakContinuous) ' Abschnittwechsel fortlaufend
                iSelectedRange = .ActiveWindow.Selection.Information(WdInformation.wdActiveEndSectionNumber)
                .Sections(iSelectedRange - 1).ProtectedForForms = False
            End If

            .ActiveWindow.Selection.TypeText(Text:="  ")
            .ActiveWindow.Selection.MoveLeft(Unit:=WdUnits.wdCharacter, Count:=2, Extend:=WdMovementType.wdExtend)
            .ActiveWindow.Selection.Bookmarks.Add(sSourceBMname, Range:=.ActiveWindow.Selection.Range)
            oSourceBMrange = oWordProps.docSource.Bookmarks(sSourceBMname).Range

            '************************************************
            Call FormatRange(oSourceBMrange)
            '************************************************

            oTargetBMrange = .Bookmarks(sSourceBMname).Range

            oTargetBMrange.FormattedText = oSourceBMrange.FormattedText

            ' !!! Dieser Schritt dauert bei grossen Dokumenten lange. 
            '        .ActiveWindow.Selection.GoTo What:=wdGoToBookmark, Name:=sSourceBMname 
            '        .ActiveWindow.Selection.Collapse Direction:=wdCollapseEnd 'gehe an das Ende der wrd.selection, dh der Cursor springt an den Zeilenanfang ; 

            ' !!! erstzt durch das hier: 
            .ActiveWindow.Selection.Start = oTargetBMrange.End

            '        'eine Zeile nach dem TextmarkenKlammerEnde 
            '        If .ActiveWindow.Selection.Information(wdWithInTable) Then 
            '            'Ich vermute, dass dieser Check nicht mehr notwendig ist 
            '            goLogging.WriteLog "       Check wdWithInTable" 
            '            .ActiveWindow.Selection.MoveDown wdParagraph, 1 
            '        End If 

            If goOptions.bUnprotected = False Then
                .ActiveWindow.Selection.InsertBreak(Type:=WdBreakType.wdSectionBreakContinuous)
                iSelectedRange = .ActiveWindow.Selection.Information(WdInformation.wdActiveEndSectionNumber)

                'Check, if this table has to be unprotected
                If bUnprotected = True Then
                    .Sections(iSelectedRange - 1).ProtectedForForms = False
                Else
                    .Sections(iSelectedRange - 1).ProtectedForForms = True
                End If
            End If

        End With

        goLogging.WriteLog("End  : InsertTable")

    End Sub

    '''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
    ' Sub-Prozedur        : ReplaceTable
    ' Beschreibung        : Ersetzt eine bestehende Tabelle mit einer neuen
    ' Parameter           : sSourceBMname - Name der Quelltextmarke
    ' wird aufgerufen von : DocFull
    ' Änderungshistorie   :
    ' Autor  Datum       Grund            Kommentar
    ' -----  ----------  ---------------  ---------------------------------------
    '
    '''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
    Sub ReplaceTable(sSourceBMname As String, bUnprotected As Boolean)

        Dim iSelectedRange As Integer
        Dim oSourceBMrange As Range
        Dim oTargetBMrange As Range

        goLogging.WriteLog("Start: ReplaceTable")

        oSourceBMrange = oWordProps.docSource.Bookmarks(sSourceBMname).Range
        '************************************************
        Call FormatRange(oSourceBMrange)
        '************************************************

        With oWordProps.docTarget
            oTargetBMrange = .Bookmarks(sSourceBMname).Range
            oTargetBMrange.Select()
            .ActiveWindow.Selection.Delete()
            oTargetBMrange.Select()
            On Error Resume Next
            .ActiveWindow.Selection.Rows.Delete()
            oTargetBMrange.FormattedText = oSourceBMrange.FormattedText
            On Error GoTo 0
            .ActiveWindow.Selection.GoTo(What:=WdGoToItem.wdGoToBookmark, Name:=sSourceBMname)
            '        .ActiveWindow.Selection.Rows.AllowBreakAcrossPages = False
            '        .ActiveWindow.Selection.ParagraphFormat.KeepWithNext = True '  ABSÄTZE NICHT TRENNEN <- WICHTIG!

            'Check, if this table has to be unprotected (only relevant for non structured bookmarks
            If Not sSourceBMname Like sBM_STRUCT And Not sSourceBMname Like sBM_TABLE And Not sSourceBMname Like sBM_GRAPHIC Then
                iSelectedRange = .ActiveWindow.Selection.Information(WdInformation.wdActiveEndSectionNumber)

                If bUnprotected = True Then
                    .Sections(iSelectedRange).ProtectedForForms = False
                    goLogging.WriteLog("    " & sSourceBMname & " (" & iSelectedRange & "): unprotected.")
                Else
                    .Sections(iSelectedRange).ProtectedForForms = True
                    goLogging.WriteLog("    " & sSourceBMname & " (" & iSelectedRange & "): unprotected.")
                End If
            End If

        End With

        goLogging.WriteLog("End  : ReplaceTable")

    End Sub

    '''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
    ' Sub-Prozedur        : ReplaceCaption
    ' Beschreibung        : Ersetzt eine bestehende Überschrift mit einer neuen
    ' Parameter           : sSourceBMname - Name der Quelltextmarke
    ' wird aufgerufen von : DocFull
    ' Änderungshistorie   :
    ' Autor  Datum       Grund            Kommentar
    ' -----  ----------  ---------------  ---------------------------------------
    '
    '''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
    Sub ReplaceCaption(sSourceBMname As String)

        Dim oSourceBMrange As Range
        Dim oTargetBMrange As Range

        goLogging.WriteLog("Start: ReplaceCaption")

        With oWordProps.docTarget
            .ActiveWindow.ActivePane.View.SeekView = WdSeekView.wdSeekCurrentPageHeader
            oSourceBMrange = oWordProps.docSource.Bookmarks(sSourceBMname).Range

            '************************************************
            Call FormatRange(oSourceBMrange)
            '************************************************

            oTargetBMrange = .Bookmarks(sSourceBMname).Range

            '        oTargetBMrange.WholeStory
            '        oTargetBMrange.Delete

            oTargetBMrange.Select()

            On Error GoTo NoRowError
            .ActiveWindow.Selection.Rows.Delete()
            GoTo NoRowCont
NoRowError:
            .ActiveWindow.Selection.Delete()
NoRowCont:
            On Error GoTo 0


            oTargetBMrange.FormattedText = oSourceBMrange.FormattedText
            .ActiveWindow.ActivePane.View.SeekView = WdSeekView.wdSeekMainDocument
        End With

        goLogging.WriteLog("End  : ReplaceCaption")

    End Sub

    Sub AlignSourceWithTarget(sBMname As String)

        Dim oStructBMrange As Range
        Dim oBM As Bookmark
        Dim sBM As String

        Dim oDeleteableParagraphRange As Range

        goLogging.WriteLog("Start: AlignSourceWithTarget (" & sBMname & ")")
        Call UpdateFeedback(95, "Check for deletable data")

        ' sBMname is always a structured bookmarkname

        oStructBMrange = oWordProps.docTarget.Bookmarks(sBMname).Range

        For Each oBM In oStructBMrange.Bookmarks
            sBM = oBM.Name
            If sBM Like sBM_TABLE Or sBM Like sBM_GRAPHIC Then
                If oWordProps.docSource.Bookmarks.Exists(sBM) = False Then
                    'If this does not exist in the source doc, remove in the target doc
                    With oWordProps.docTarget
                        .Bookmarks(sBM).Select()

                        If goOptions.bUnprotected = False Then

                            oDeleteableParagraphRange = .Range(Start:=.ActiveWindow.Selection.Sections.First.Range.Start - 1, End:=.ActiveWindow.Selection.Sections.Last.Range.End)

                            'Add empty chars. Else a (protected!) section break will be left over
                            oDeleteableParagraphRange.InsertAfter(" ")
                            oDeleteableParagraphRange.InsertBefore(" ")
                            oDeleteableParagraphRange.Select()

                        End If

                        .ActiveWindow.Selection.Delete()

                        '.ActiveWindow.Selection.Delete
                        goLogging.WriteLog("  " & sBM & " was deleted")
                    End With
                End If
            End If
        Next

        goLogging.WriteLog("End  : AlignSourceWithTarget")

    End Sub
    '''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
    ' Sub-Prozedur        : AlignSourceWithTarget
    ' Beschreibung        : Diese Prozedur führt einen Abgleich zwischen Quell- und
    '                       Zieldokument durch und löscht die Textmarken im
    '                       Zieldokument, die es im Quelldokument nicht findet
    ' Parameter           : sBMname
    ' wird aufgerufen von : fullDoc
    ' Änderungshistorie   :
    ' Autor  Datum       Grund            Kommentar
    ' -----  ----------  ---------------  ---------------------------------------
    '
    '''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
    Sub AlignSourceWithTargetLegacy(sBMname As String)

        Dim I As Integer
        Dim iDeletedBDcount As Integer
        Dim oStructBMrange As Range
        Dim aTargetBM() As Object
        Dim iTagetBMcount As Integer
        Dim iArrayIndex As Integer
        Dim iSelectedRange As Long
        Dim sTargetBMname As String
        Dim sBM As String

        Dim oPreviousParagraphRange As Range
        Dim oDeleteableParagraphRange As Range
        Dim iPreviousParagraphEnd As Long
        Dim iPreviousParagraphsCount As Long

        goLogging.WriteLog("Start: AlignSourceWithTargetLegacy (" & sBMname & ")")

        iDeletedBDcount = 0

        oStructBMrange = oWordProps.docTarget.Bookmarks(sBMname).Range
        iTagetBMcount = oStructBMrange.Bookmarks.Count

        If sBMname Like sBM_STRUCT And iTagetBMcount > 1 Then

            ' Trenne die Tabellen-Textmarken von den zwischenzeitlich
            ' eventuell eingefügten Textmarken beim Überarbeiten
            ReDim aTargetBM(0 To iTagetBMcount)
            iArrayIndex = 0

            For I = 1 To iTagetBMcount
                sBM = oStructBMrange.Bookmarks(I).Name
                If sBM Like sBM_UEBER Or sBM Like sBM_TABLE Or sBM Like sBM_GRAPHIC Then
                    iArrayIndex = iArrayIndex + 1
                    aTargetBM(iArrayIndex) = sBM
                End If
            Next I

            ' Beginne die gefilterten TM aus dem Array auszulesen und zu überprüfen,
            ' ob diese gelöscht werden sollen
            For I = 1 To iArrayIndex
                sTargetBMname = aTargetBM(I)

                Call UpdateFeedback(95, "Check for deletable data " & I & "/" & iArrayIndex)

                If oWordProps.docSource.Bookmarks.Exists(sTargetBMname) = False Then

                    ' wenn im Source Dokument der BM nicht enthalten ist, dann lösche im Target-Dokument den BM
                    ' dabei kann es sich nur um eine Tabelle handeln, da das RangeObjekt die BM in den Kopfzeilen nicht mit ausliest
                    With oWordProps.docTarget

                        .ActiveWindow.Selection.GoTo(What:=WdGoToItem.wdGoToBookmark, Name:=sTargetBMname)
                        iSelectedRange = .ActiveWindow.Selection.Information(WdInformation.wdActiveEndSectionNumber)

                        If iArrayIndex = iDeletedBDcount + 1 Then  ' Fall 4a: getestet und OK
                            oPreviousParagraphRange = .Sections(iSelectedRange - 1).Range
                            iPreviousParagraphEnd = oPreviousParagraphRange.End
                            oDeleteableParagraphRange = .Range(Start:=iPreviousParagraphEnd - 1, End:=.Sections(iSelectedRange).Range.End)
                            oDeleteableParagraphRange.Select()
                            .ActiveWindow.Selection.Delete()
                            .ActiveWindow.Selection.PageSetup.SectionStart = WdSectionStart.wdSectionNewPage
                            goLogging.WriteLog("  Type 4a: " & sTargetBMname & " was deleted")
                            DeleteCaption(sBMname)
                        Else

                            .Sections(iSelectedRange + 2).Range.Select()

                            ' wenn der Abschnittwechsel "FORTLAUFEND" ist , dann lösche nicht
                            ' den Abschnittwechsel vor der zu löschenden Tabelle, da hinter der
                            ' zu löschende Tabelle noch weitere zu der Überschrift gehörenden Tabellen folgen
                            If .ActiveWindow.Selection.PageSetup.SectionStart = 0 Then

                                'SETZE DEN VOR DER ZU LÖSCHENDEN TAB LIEGENDE ABSCHNITT UND ERMITTLE DAS ENDE DIESES ABSCHNITTES
                                oPreviousParagraphRange = .Sections(iSelectedRange - 1).Range
                                oPreviousParagraphRange.Select()

                                ' Fall 1a oder Fall 1b: beides getestet und OK
                                If .ActiveWindow.Selection.PageSetup.SectionStart = 0 Then
                                    iPreviousParagraphEnd = oPreviousParagraphRange.End
                                    oDeleteableParagraphRange = .Range(Start:=iPreviousParagraphEnd - 1, End:=.Sections(iSelectedRange).Range.End)

                                    'Add an empty char. Else a (protected!) section break will be left over 
                                    oDeleteableParagraphRange.InsertAfter(" ")
                                    oDeleteableParagraphRange.Select()
                                    .ActiveWindow.Selection.Delete()
                                    iDeletedBDcount = iDeletedBDcount + 1
                                    If (I = iArrayIndex) Then
                                        goLogging.WriteLog("  Type 1b: " & sTargetBMname & " was deleted")
                                    Else
                                        goLogging.WriteLog("  Type 1a: " & sTargetBMname & " was deleted")  ' hier per i = indexarray-1 den log eintrag unterscheiden !
                                    End If

                                    ' Fall 2a oder Fall 2b: beides getestet und OK
                                Else
                                    ' Fall 2b
                                    If (I = iArrayIndex) Then      ' Tabelle am Dokumentenende
                                        '
                                        ' Lösche zuerst die Überschrift und setzte dann einen Link zu der vorherigen
                                        '
                                        .ActiveWindow.Selection.GoTo(What:=WdGoToItem.wdGoToBookmark, Name:=sTargetBMname)
                                        .ActiveWindow.Selection.StartOf(Unit:=WdUnits.wdSection, Extend:=WdMovementType.wdMove)
                                        iSelectedRange = .ActiveWindow.Selection.Information(WdInformation.wdActiveEndSectionNumber)
                                        .ActiveWindow.ActivePane.View.SeekView = WdSeekView.wdSeekCurrentPageHeader
                                        .Sections(iSelectedRange).Headers(WdHeaderFooterIndex.wdHeaderFooterPrimary).Range.Tables(2).Range.Select() ' ACHTUNG, Tables = Tables(2)!

                                        .ActiveWindow.Selection.Rows.Delete()

                                        .Activate()
                                        .ActiveWindow.Selection.HeaderFooter.LinkToPrevious = True
                                        .ActiveWindow.ActivePane.View.SeekView = WdSeekView.wdSeekMainDocument

                                        ' Lösche die Tabelle
                                        iPreviousParagraphsCount = .Sections.Count
                                        iPreviousParagraphEnd = oPreviousParagraphRange.End
                                        oDeleteableParagraphRange = .Range(Start:=iPreviousParagraphEnd - 1, End:=.Sections(iSelectedRange).Range.End)
                                        oDeleteableParagraphRange.Select()
                                        .ActiveWindow.Selection.Delete()
                                        Call CheckDeletion(iPreviousParagraphsCount)
                                        .ActiveWindow.Selection.PageSetup.SectionStart = WdSectionStart.wdSectionNewPage
                                        iDeletedBDcount = iDeletedBDcount + 1

                                        ' Setze den "AW neue Seite" auf "fortlaufenden AW"
                                        iSelectedRange = .ActiveWindow.Selection.Information(WdInformation.wdActiveEndSectionNumber)
                                        oDeleteableParagraphRange = .Range(Start:=.Sections(iSelectedRange - 1).Range.End - 1, End:=.Sections(iSelectedRange - 1).Range.End)
                                        oDeleteableParagraphRange.Select()
                                        .ActiveWindow.Selection.Delete()
                                        .ActiveWindow.Selection.PageSetup.SectionStart = WdSectionStart.wdSectionContinuous
                                        goLogging.WriteLog("  Type 2b: " & sTargetBMname & " was deleted")
                                        ' Fall 2a
                                    Else
                                        iPreviousParagraphsCount = .Sections.Count
                                        iPreviousParagraphEnd = oPreviousParagraphRange.End
                                        oDeleteableParagraphRange = .Range(Start:=iPreviousParagraphEnd - 1, End:=.Sections(iSelectedRange).Range.End)
                                        oDeleteableParagraphRange.Select()
                                        .ActiveWindow.Selection.Delete()
                                        Call CheckDeletion(iPreviousParagraphsCount)
                                        .ActiveWindow.Selection.PageSetup.SectionStart = WdSectionStart.wdSectionNewPage
                                        iDeletedBDcount = iDeletedBDcount + 1
                                        goLogging.WriteLog("  Type 2a: " & sTargetBMname & " was deleted")
                                    End If
                                End If
                            Else ' d.h der übernächste Abschnittwechsel ist ein "AW neue Seite"
                                oPreviousParagraphRange = .Sections(iSelectedRange - 1).Range
                                oPreviousParagraphRange.Select()

                                ' Fall 3: getestet und OK
                                If .ActiveWindow.Selection.PageSetup.SectionStart = 0 Then
                                    iPreviousParagraphEnd = oPreviousParagraphRange.End
                                    oDeleteableParagraphRange = .Range(Start:=iPreviousParagraphEnd - 1, End:=.Sections(iSelectedRange).Range.End)
                                    oDeleteableParagraphRange.Select()
                                    .ActiveWindow.Selection.Delete()
                                    iDeletedBDcount = iDeletedBDcount + 1
                                    goLogging.WriteLog("  Type 3: " & sTargetBMname & " was deleted")
                                    ' Fall 4
                                Else
                                    '  Fall 4b: getestet und OK
                                    iPreviousParagraphEnd = oPreviousParagraphRange.End
                                    oDeleteableParagraphRange = .Range(Start:=iPreviousParagraphEnd - 1, End:=.Sections(iSelectedRange).Range.End)
                                    oDeleteableParagraphRange.Select()
                                    .ActiveWindow.Selection.Delete()

                                    ' Überprüfe, ob beim Löschen der vorherige Abschnittwechsel "neue Seite" von Word automatisch in einen "fortlaufenden AW"
                                    ' umgewandelt wurde. Wenn JA, dann mache aus dem fortl.AW wieder einen AW "neue Seite"
                                    If .ActiveWindow.Selection.PageSetup.SectionStart = 0 Then
                                        .ActiveWindow.Selection.PageSetup.SectionStart = WdSectionStart.wdSectionNewPage
                                    End If

                                    ' Lösche Abschnittwechsel "Neue Seite" nach dem vom User eingefügten Text
                                    iSelectedRange = .ActiveWindow.Selection.Information(WdInformation.wdActiveEndSectionNumber)
                                    oDeleteableParagraphRange = .Range(Start:=.Sections(iSelectedRange).Range.End - 1, End:=.Sections(iSelectedRange).Range.End)
                                    oDeleteableParagraphRange.Select()
                                    .ActiveWindow.Selection.Delete()
                                    iDeletedBDcount = iDeletedBDcount + 1
                                    goLogging.WriteLog("  Type 4b: " & sTargetBMname & " was deleted")
                                End If
                            End If
                        End If
                    End With 'oWord.Documents(oWordProps.sTargetDocName)
                End If 'oWordProps.sSourceDoc.Bookmarks.Exists(sTargetBMname) = False
            Next I

        Else

            I = 2
            Do While I <= iTagetBMcount  'For i = 2 To iTagetBMcount  beginnt mit i=2, um die strukturierte TM selbst auszuschliessen

                sTargetBMname = oStructBMrange.Bookmarks(I).Name

                If oWordProps.docSource.Bookmarks.Exists(sTargetBMname) = False Then
                    With oWordProps.docTarget
                        .ActiveWindow.Selection.GoTo(What:=WdGoToItem.wdGoToBookmark, Name:=sTargetBMname)
                        iSelectedRange = .ActiveWindow.Selection.Information(WdInformation.wdActiveEndSectionNumber)
                        oPreviousParagraphRange = .Sections(iSelectedRange - 1).Range
                        oPreviousParagraphRange.Select()
                        iPreviousParagraphEnd = oPreviousParagraphRange.End
                        oDeleteableParagraphRange = .Range(Start:=iPreviousParagraphEnd - 1, End:=.Sections(iSelectedRange).Range.End)
                        oDeleteableParagraphRange.Select()
                        .ActiveWindow.Selection.Delete()
                    End With
                    I = I - 1
                    iTagetBMcount = iTagetBMcount - 1
                End If

                I = I + 1
            Loop
        End If

        goLogging.WriteLog("End  : AlignSourceWithTargetLegacy")

    End Sub

    '''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
    ' Sub-Prozedur        : DeleteCaption
    ' Beschreibung        : Löscht eine? Überschrift aus dem Zieldokument
    ' Parameter           : -
    ' wird aufgerufen von : AlignSourceWithTargetLegacy
    ' Änderungshistorie   :
    ' Autor  Datum       Grund            Kommentar
    ' -----  ----------  ---------------  ---------------------------------------
    '
    '''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
    Sub DeleteCaption(sBMname As String)

        Dim iSelectedRange As Integer
        Dim iNumBookmarks As Integer

        goLogging.WriteLog("Start: DeleteCaption")

        With oWordProps.docTarget

            .ActiveWindow.Selection.GoTo(What:=WdGoToItem.wdGoToBookmark, Name:=sBMname)
            .ActiveWindow.Selection.StartOf(Unit:=WdUnits.wdSection, Extend:=WdMovementType.wdMove)

            iSelectedRange = .ActiveWindow.Selection.Information(WdInformation.wdActiveEndSectionNumber)
            .ActiveWindow.ActivePane.View.SeekView = WdSeekView.wdSeekCurrentPageHeader

            'Eventuell verbliebene Bookmarks entfernen
            iNumBookmarks = .Sections(iSelectedRange).Headers(WdHeaderFooterIndex.wdHeaderFooterPrimary).Range.Bookmarks.Count
            If iNumBookmarks > 0 Then
                .Sections(iSelectedRange).Headers(WdHeaderFooterIndex.wdHeaderFooterPrimary).Range.Bookmarks(1).Select()
                .ActiveWindow.Selection.Delete()
            End If

            .ActiveWindow.ActivePane.View.SeekView = WdSeekView.wdSeekMainDocument

        End With

        goLogging.WriteLog("End  : DeleteCaption")

    End Sub

    '''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
    ' Sub-Prozedur        : CheckDeletion
    ' Beschreibung        : Diese Prozedur überprüft, ob Word den selektierten Abschnitt korrekt gelöscht hat
    ' Parameter           : iPreviousParagraphCount - Anzahl der Abschnitte
    ' wird aufgerufen von : AlignSourceWithTargetLegacy
    ' Änderungshistorie   :
    ' Autor  Datum       Grund            Kommentar
    ' -----  ----------  ---------------  ---------------------------------------
    '
    '''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
    Sub CheckDeletion(iPreviousParagraphCount As Long)

        With oWordProps.docTarget

            If iPreviousParagraphCount - 2 <> .Sections.Count Then
                .ActiveWindow.Selection.Delete()
                goLogging.WriteLog("   Word did not delete correctly !")
            End If

        End With

    End Sub


    '''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
    ' Funktion            : CheckForChanges
    ' Beschreibung        : Überprüft, ob sich an der Reihenfolge der Bookmarks etwas geändert hat
    ' Parameter           : Name der zu überprüfenden Strukturierten Bookmark
    ' Rückgabewert        : true - Reihenfolge hat sich geändert.
    '                       false - Reihenfolge ist gleich geblieben
    ' Änderungshistorie   : -
    ' Autor  Datum       Grund            Kommentar
    ' -----  ----------  ---------------  ---------------------------------------
    '
    '''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
    Function CheckForChanges(sStructBM) As Boolean

        Dim iBMcountSource As Integer
        Dim iBMcountTarget As Integer

        Dim oStructBMrangeSource As Range
        Dim oStructBMrangeTarget As Range

        Dim sBMNameSource As String = ""
        Dim sBMNameTarget As String = ""

        Dim I As Integer
        Dim j As Integer

        Dim Tcount As Integer
        Dim Tcollect()

        goLogging.WriteLog("Start: CheckForChanges")


        oStructBMrangeSource = oWordProps.docSource.Bookmarks(sStructBM).Range
        oStructBMrangeTarget = oWordProps.docTarget.Bookmarks(sStructBM).Range

        iBMcountSource = oStructBMrangeSource.Bookmarks.Count
        iBMcountTarget = oStructBMrangeTarget.Bookmarks.Count

        CheckForChanges = False
        j = 0
        Tcount = 0

        ' Alle T Textmarken zählen
        For I = 1 To iBMcountTarget
            sBMNameTarget = oStructBMrangeTarget.Bookmarks(I).Name
            If sBMNameTarget Like sBM_TABLE Then
                Tcount = Tcount + 1
            End If
        Next I

        ReDim Tcollect(Tcount)
        Tcount = 0

        ' Bestimmen, ob sich die Reihenfolge der "T_*" Textmarken verändert hat.
        For I = 1 To iBMcountTarget

            sBMNameTarget = oStructBMrangeTarget.Bookmarks(I).Name

            If sBMNameTarget Like sBM_TABLE Then
                Tcount = Tcount + 1
                Tcollect(Tcount) = sBMNameTarget
                Do While j < iBMcountSource
                    j = j + 1
                    sBMNameSource = oStructBMrangeSource.Bookmarks(j).Name
                    If sBMNameSource Like sBM_TABLE Then
                        Exit Do
                    End If
                Loop

                If sBMNameSource <> sBMNameTarget Then CheckForChanges = True

            End If

        Next I

        ' Reihenfolge der "T_*" Textmarken hat sich verändert
        If CheckForChanges = True Then

            goLogging.WriteLog("    Order of bookmarks has changed!")

            'Eventuelle References entfernen.
            For I = iBMcountTarget To 1 Step -1
                If oStructBMrangeTarget.Bookmarks(I).Name Like sBM_REF Then
                    goLogging.WriteLog("    " & oStructBMrangeTarget.Bookmarks(I).Name & " removed.")
                    oStructBMrangeTarget.Bookmarks(I).Delete()
                End If
            Next I

            For I = 1 To Tcount
                sBMNameTarget = Tcollect(I)

                oStructBMrangeTarget = oWordProps.docTarget.Bookmarks(sBMNameTarget).Range

                ' Alle Bookmarks durchgehen, und die Bookmarks mit Bookmarks erstetzen, welche den Namen "_DEL" tragen.
                ' Auf diese Weise wird das Dokument nicht leer (was die nachfolgenden Prozeduren nicht vertragen würden),
                ' und die mit "*_DEL" markierten Abschnitte werden beim Abgleich entfernt.
                With oWordProps.docTarget

                    .ActiveWindow.Selection.GoTo(What:=WdGoToItem.wdGoToBookmark, Name:=sBMNameTarget)
                    .ActiveWindow.Selection.StartOf(Unit:=WdUnits.wdSection, Extend:=WdMovementType.wdMove)

                    oStructBMrangeTarget.Bookmarks.Add(sBMNameTarget & "_DEL")
                    oWordProps.docTarget.Bookmarks(sBMNameTarget).Delete()

                    goLogging.WriteLog("    Flagged Bookmark for deletion: " & sBMNameTarget & "_DEL")

                End With

            Next I
        End If

        goLogging.WriteLog("End  : CheckForChanges")

    End Function

    '''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
    ' Sub-Prozedur        : RemoveEmptyBookmarks
    ' Beschreibung        : Removes Empty Bookmarks from the document, if the bookmark is not existing
    '                       in the set of source files.
    ' Parameter           : -
    ' wird aufgerufen von : ConvertToWord
    ' Änderungshistorie   :
    ' Autor  Datum       Grund            Kommentar
    ' -----  ----------  ---------------  ---------------------------------------
    '
    '''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
    Sub RemoveEmptyBookmarks()

        Dim oBookmark As Bookmark
        Dim bBMfound1 As Boolean
        Dim bBMfound2 As Boolean
        Dim I As Integer
        Dim iSelectedRange As Integer
        Dim sLog As String

        goLogging.WriteLog("Start: RemoveEmptyBookmarks")

        For Each oBookmark In oWordProps.docTarget.Bookmarks

            'Bekannte Bookmarktypen gleich von der weiteren Prüfung ausschliessen
            If Not (oBookmark.Name Like sBM_UEBER Or oBookmark.Name Like sBM_TABLE Or oBookmark.Name Like sBM_REF) Then

                bBMfound1 = False

                'Schauen, ob die betreffende Bookmark relevant ist
                For I = 1 To goOptions.iIsrBookmarksNumber
                    If oBookmark.Name = goOptions.GetIsrBookmarks(I) Then
                        bBMfound1 = True
                        Exit For
                    End If
                Next I

                If bBMfound1 = True Then

                    bBMfound2 = False

                    'Schauen, ob die aktuelle Bookmark in der Liste der Blöcke noch vorkommt.
                    For I = 1 To goOptions.iFile_Sources
                        If oBookmark.Name = goOptions.GetFile_Bookmark(I) Then
                            bBMfound2 = True
                            Exit For
                        End If
                    Next I

                    If bBMfound2 = False Then
                        'Bookmark kommt nicht mehr vor, also entfernen

                        sLog = oBookmark.Name & " - " & Len(oBookmark.Range.Text) & " characters. "

                        'Vor dem Entfernen des Bookmarks den Bereich entschützen
                        oBookmark.Select()
                        iSelectedRange = oWordProps.docTarget.ActiveWindow.Selection.Information(WdInformation.wdActiveEndSectionNumber)
                        oWordProps.docTarget.Sections(iSelectedRange).ProtectedForForms = False

                        'Wenn es sich nicht um eine strukturierte Textmarke handelt, auch den Inhalt löschen
                        If Not oBookmark.Name Like sBM_STRUCT Then
                            oWordProps.docTarget.Sections(iSelectedRange).Range.Delete()
                        Else
                            oBookmark.Delete()
                        End If

                        sLog = "Removed: " & sLog

                        goLogging.WriteLog("  " & sLog)

                    End If
                End If
            End If

        Next oBookmark

        goLogging.WriteLog("End  : RemoveEmptyBookmarks")

    End Sub

    '''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
    ' Funktion            : PreviousLegalBookmark
    ' Beschreibung        : Ermittelt in einem Range die vorangegangene "legale" Bookmark,
    '                       d.h. die Überschrift, strukturierte Tabelle oder Graphic ist.
    ' Parameter           : oBMrange - der zu untersuchende Range
    '                       iNum     - Nummer der aktuellen Bookmark
    ' Rückgabewert        : String - Name der Bookmark
    ' Änderungshistorie   : -
    ' Autor  Datum       Grund            Kommentar
    ' -----  ----------  ---------------  ---------------------------------------
    '
    '''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
    Function PreviousLegalBookmark(oBMrange As Range, iNum As Integer, bNoCaption As Boolean) As String

        Dim I As Integer
        Dim sBMname As String

        I = iNum
        PreviousLegalBookmark = ""

        Do
            sBMname = oBMrange.Bookmarks(I).Name

            If (sBMname Like sBM_UEBER And bNoCaption = False) Or sBMname Like sBM_TABLE Or sBMname Like sBM_GRAPHIC Then
                PreviousLegalBookmark = sBMname
            End If
            I = I - 1

        Loop Until PreviousLegalBookmark <> "" Or I < 1


    End Function

    '''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
    ' Funktion            : ClearStaticBookmarks
    ' Beschreibung        : Prüft alle Statischen (nicht strukturierten Bookmarks), ob diese in der Liste
    '                       der zu verarbeitenden Bookmarks stehen. Wenn nicht: Bookmark leeren.
    ' Parameter           : -
    ' Rückgabewert        : -
    ' Änderungshistorie   : -
    ' Autor  Datum       Grund            Kommentar
    ' -----  ----------  ---------------  ---------------------------------------
    '
    '''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
    Sub ClearStaticBookmarks()

        Dim I As Integer
        Dim j As Integer
        Dim bBMfound As Boolean
        Dim sLog As String
        Dim oBookmark As Bookmark
        Dim iSelectedRange As Integer
        Dim sBM As String

        If goOptions.bDeactClearStaticBM = False Then

            goLogging.WriteLog("Start: ClearStaticBookmarks")

            'Alle möglichen Bookmarks betrachten
            For I = 1 To goOptions.iIsrBookmarksNumber

                sBM = goOptions.GetIsrBookmarks(I)

                'Strukturierte sind nicht relevant, die werden von AlignSourceWithTarget abgedeckt
                If Not (sBM Like sBM_STRUCT) Then

                    bBMfound = False

                    'Mit der Blocksliste abgleichen
                    For j = 1 To goOptions.iFile_Sources
                        If sBM = goOptions.GetFile_Bookmark(j) Then
                            bBMfound = True
                            Exit For
                        End If
                    Next j

                    'Nicht gefunden -> Löschkandidat
                    If bBMfound = False Then
                        With oWordProps.docTarget
                            If .Bookmarks.Exists(sBM) = True Then

                                oBookmark = .Bookmarks(sBM)

                                sLog = oBookmark.Name & " - " & Len(oBookmark.Range.Text) & " characters. "

                                'Vor dem Entfernen des Inhaltes den Bereich entschützen
                                oBookmark.Select()
                                iSelectedRange = .ActiveWindow.Selection.Information(WdInformation.wdActiveEndSectionNumber)
                                .Sections(iSelectedRange).ProtectedForForms = False

                                'Bookmark + Inhalt löschen
                                .ActiveWindow.Selection.TypeText(Text:="  ")
                                .ActiveWindow.Selection.MoveLeft(Unit:=WdUnits.wdCharacter, Count:=2, Extend:=WdMovementType.wdExtend)
                                'Bookmark an dieser Stelle frisch anlegen
                                .ActiveWindow.Selection.Bookmarks.Add(sBM, Range:=.ActiveWindow.Selection.Range)

                                sLog = "Cleared: " & sLog
                                goLogging.WriteLog("  " & sLog)

                            End If
                        End With
                    End If
                End If
            Next I
            goLogging.WriteLog("End  : ClearStaticBookmarks")
        Else
            goLogging.WriteLog("Skip : ClearStaticBookmarks")
        End If

    End Sub

    ''' <summary>
    ''' Makes sure the document is in PrintView (needed since Word 2013)
    ''' </summary>
    Sub CheckPrintView(ByRef oDoc As Document)

        With oDoc.ActiveWindow

            If .View.SplitSpecial <> WdSpecialPane.wdPaneNone Then
                .Panes(2).Close()
            End If

            If .ActivePane.View.Type = WdViewType.wdNormalView _
                Or .ActivePane.View.Type = WdViewType.wdOutlineView _
                Or .ActivePane.View.Type = WdViewType.wdMasterView Then
                .ActivePane.View.Type = WdViewType.wdPrintView
            End If

            .View.Type = WdViewType.wdPrintView

        End With

    End Sub

End Module
