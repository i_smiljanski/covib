﻿Public Class fMain

    '''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
    ' Form              : fMain
    ' Beschreibung      : Zeigt einen Text sowie einen Fortschrittsbalken an.
    ' Autor             : ms
    ' Datum             : 22.03.2004
    ' Änderungshistorie :
    ' Autor  Datum       Grund            Kommentar
    ' -----  ----------  ---------------  ---------------------------------------
    '
    '''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''

    Private Declare Function HideCaret Lib "user32.dll" (ByVal hwnd As Int32) As Int32
    Private Declare Function ShowCaret Lib "user32.dll" (ByVal hwnd As Int32) As Int32

    '''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
    ' Sub-Prozedur        : Form_Load
    ' Beschreibung        : Füllt die Caption des Forms (d.h. den Windowtitel) mit
    '                       Informationen zur Applikation (Produktname, Version, Datum)
    ' Parameter           : -
    ' wird aufgerufen von : modMain.Main
    ' Änderungshistorie   :
    ' Autor  Datum       Grund            Kommentar
    ' -----  ----------  ---------------  ---------------------------------------
    '
    '''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
    Private Sub fMain_Load(sender As System.Object, e As System.EventArgs) Handles MyBase.Load

        Me.Text = Me.Text & System.Diagnostics.FileVersionInfo.GetVersionInfo(System.Reflection.Assembly.GetExecutingAssembly.Location).FileVersion & " (" & cCompilationDate & ")"
        txtMsg.Text = ""
        lblProgress.Text = ""

    End Sub


    '''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
    ' Sub-Prozedur        : UpdateMessage
    ' Beschreibung        : Schreibt den mitgegebenen Text in txtMsg
    ' Parameter           : sMessage - Textnachricht
    ' wird aufgerufen von : verschieden
    ' Änderungshistorie   :
    ' Autor  Datum       Grund            Kommentar
    ' -----  ----------  ---------------  ---------------------------------------
    '
    '''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
    Public Sub UpdateMessage(sMessage As String)

        txtMsg.Text = sMessage
        HideCaret(txtMsg.Handle)
        Me.Refresh()

    End Sub

    '''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
    ' Sub-Prozedur        : UpdateProgressbar
    ' Beschreibung        : Ändert den Wert des Fortschrittsbalken anhand dem mitgegebenen Wert
    ' Parameter           : iPercent - Integer-Wert zwischen 1 und 100
    ' wird aufgerufen von : verschieden
    ' Änderungshistorie   :
    ' Autor  Datum       Grund            Kommentar
    ' -----  ----------  ---------------  ---------------------------------------
    '
    '''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
    Public Sub UpdateProgressbar(iPercent As Integer)

        If iPercent = 0 Then
            lblProgress.Text = sPROGRESSTEXT & "0%"
        ElseIf iPercent < 0 Then
            lblProgress.Text = sPROGRESSTEXT & "0%"
        ElseIf iPercent > 100 Then
            lblProgress.Text = sPROGRESSTEXT & "100%"
        Else
            lblProgress.Text = sPROGRESSTEXT & iPercent & "%"
        End If
        Me.Refresh()

    End Sub

End Class