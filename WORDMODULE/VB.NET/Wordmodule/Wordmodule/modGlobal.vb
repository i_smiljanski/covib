﻿' <summary>All global Objects, Variables, Constants reside here</summary>

Module modGlobal

    Public Const cCompilationDate = "20170228"

    Public Const sBM_UEBER = "*CAP*"            'sTM
    Public Const sBM_END = "*END*"              'sTM3
    Public Const sBM_BEGIN = "*BGN*"            'sTM4
    Public Const sBM_STRUCT = "S_*"
    Public Const sBM_GRAPHIC = "G_*"
    Public Const sBM_TABLE = "T_*"
    Public Const sBM_REF = "REF_*"

    Public Const sTABLENAME = "TPAS"

    Public bDEBUG As Boolean

    Public goOptions As cOptions
    Public goLogging As cLogging
    Public gfMain As fMain

    Public gVersionWindows As String
    Public gVersionWord As String

    Public Const sPROGRESSTEXT = "Current progress: "

End Module
