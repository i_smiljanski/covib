﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class fMain
    Inherits System.Windows.Forms.Form

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(fMain))
        Me.txtMsg = New System.Windows.Forms.TextBox()
        Me.lblProgress = New System.Windows.Forms.Label()
        Me.pbISRlogo = New System.Windows.Forms.PictureBox()
        CType(Me.pbISRlogo, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SuspendLayout()
        '
        'txtMsg
        '
        Me.txtMsg.BackColor = System.Drawing.SystemColors.Control
        Me.txtMsg.BorderStyle = System.Windows.Forms.BorderStyle.None
        Me.txtMsg.Location = New System.Drawing.Point(80, 17)
        Me.txtMsg.Name = "txtMsg"
        Me.txtMsg.Size = New System.Drawing.Size(295, 13)
        Me.txtMsg.TabIndex = 0
        Me.txtMsg.Text = "txtMsg"
        '
        'lblProgress
        '
        Me.lblProgress.AutoSize = True
        Me.lblProgress.Location = New System.Drawing.Point(78, 44)
        Me.lblProgress.Name = "lblProgress"
        Me.lblProgress.Size = New System.Drawing.Size(58, 13)
        Me.lblProgress.TabIndex = 1
        Me.lblProgress.Text = "lblProgress"
        '
        'pbISRlogo
        '
        Me.pbISRlogo.Image = Global.Wordmodule.My.Resources.Resources.iSR35_Flat_256
        Me.pbISRlogo.InitialImage = Nothing
        Me.pbISRlogo.Location = New System.Drawing.Point(5, 5)
        Me.pbISRlogo.Name = "pbISRlogo"
        Me.pbISRlogo.Size = New System.Drawing.Size(64, 64)
        Me.pbISRlogo.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage
        Me.pbISRlogo.TabIndex = 2
        Me.pbISRlogo.TabStop = False
        '
        'fMain
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(387, 76)
        Me.Controls.Add(Me.pbISRlogo)
        Me.Controls.Add(Me.lblProgress)
        Me.Controls.Add(Me.txtMsg)
        Me.Icon = CType(resources.GetObject("$this.Icon"), System.Drawing.Icon)
        Me.Name = "fMain"
        Me.Text = "Wordmodule "
        CType(Me.pbISRlogo, System.ComponentModel.ISupportInitialize).EndInit()
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub
    Friend WithEvents txtMsg As System.Windows.Forms.TextBox
    Friend WithEvents lblProgress As System.Windows.Forms.Label
    Friend WithEvents pbISRlogo As System.Windows.Forms.PictureBox
End Class
