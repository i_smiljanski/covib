CREATE OR REPLACE TYPE isr$obj$main$config under isr$obj$grid(
--**********************************************************************************************************************************
 static function putParameters(oParameter in stb$menuentry$record, olParameter in stb$property$list, clParameter in clob default null)
  return stb$oerror$record
--**********************************************************************************************************************************
, static function callFunction (oParameter in STB$MENUENTRY$RECORD, olNodeList out STB$TREENODELIST)
  return STB$OERROR$RECORD
--**********************************************************************************************************************************
, static function buildXmlFile(xMainXml out xmltype) return STB$OERROR$RECORD 
--**********************************************************************************************************************************
, static function saveXmlFile(xMainXml in xmltype) return STB$OERROR$RECORD 
--***********************************************************************************************************************
, static function getLov (sEntity  in varchar2, ssuchwert  in varchar2, oSelectionList out isr$tlrselection$list) return STB$OERROR$RECORD 
)
  not final