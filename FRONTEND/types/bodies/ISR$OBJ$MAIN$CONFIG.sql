CREATE OR REPLACE TYPE BODY isr$obj$main$config as


--**********************************************************************************************************************************
static function putParameters(oParameter in stb$menuentry$record, olParameter in stb$property$list, clParameter in clob default null) 
  return stb$oerror$record is
  sCurrentName        constant varchar2(100) := $$PLSQL_UNIT||'.putParameters('||oParameter.sToken||')';
  oErrorObj           STB$OERROR$RECORD := STB$OERROR$RECORD();
  sMsg              varchar2(4000);
  oParamListRec   ISR$PARAMLIST$REC := ISR$PARAMLIST$REC();
  xMainXml   xmltype;
 begin 
  isr$trace.stat('begin','begin',sCurrentName);
  oParamListRec.AddParam('TABLE', 'ISR$FRONTEND$MAIN$CONFIG');
  oParamListRec.AddParam('AUDITTYPE', 'SYSTEMAUDIT');
  oParamListRec.AddParam('AUDITREFERENCE', TO_CHAR (-1));
  
  oErrorObj := isr$obj$grid().modifyGridTable(oParameter.sToken, clParameter, oParamListRec);
  if oErrorObj.isExceptionCritical then
    raise stb$typedef.exCritical;
  end if;
  
  oErrorObj := buildXmlFile(xMainXml);
  if oErrorObj.isExceptionCritical then
    raise stb$typedef.exCritical;
  end if;
  
  oErrorObj := saveXmlFile(xMainXml);
  if oErrorObj.isExceptionCritical then
    raise stb$typedef.exCritical;
  end if;
  
  isr$trace.stat('end','end',sCurrentName);
  return oErrorObj;
exception
  when others then
    sMsg :=  utd$msglib.getmsg ('exUnexpectedError', isr$obj$base().nCurrentLanguage, csP1 => SQLERRM(SQLCODE));
    oErrorObj.handleError ( Stb$typedef.cnSeverityCritical, sMsg, sCurrentName,
                            DBMS_UTILITY.format_error_stack || CHR(10) || DBMS_UTILITY.format_error_backtrace );
    return oErrorObj; 
 end putParameters;
 
 --**********************************************************************************************************************************
static function callFunction (oParameter in STB$MENUENTRY$RECORD, olNodeList out STB$TREENODELIST)
  return STB$OERROR$RECORD is
  sCurrentName         constant varchar2(100) := $$PLSQL_UNIT||'.callFunction('||oParameter.sToken||')';
  oErrorObj                     STB$OERROR$RECORD := STB$OERROR$RECORD();
  sMsg              varchar2(4000);
  oParamListRec   ISR$PARAMLIST$REC := ISR$PARAMLIST$REC();
begin
  isr$trace.stat('begin', 'oParameter.sToken: '||oParameter.sToken ,sCurrentName);
  isr$trace.debug('parameter', 'oParameter', sCurrentName, oParameter);
    
  STB$OBJECT.setCurrentToken(oparameter.stoken);
  
  oParamListRec.AddParam('TABLE', 'ISR$FRONTEND$MAIN$CONFIG');
  oErrorObj := isr$obj$grid().buildGridMask(oParameter.sToken, oParamListRec, olNodeList);
  isr$trace.stat('end', 'end', sCurrentName);
  return oErrorObj;
exception
  when others then
    sMsg :=  utd$msglib.getmsg ('exUnexpectedError', isr$obj$base().nCurrentLanguage, csP1 => SQLERRM(SQLCODE));
    oErrorObj.handleError ( Stb$typedef.cnSeverityCritical, sMsg, sCurrentName,
                            DBMS_UTILITY.format_error_stack || CHR(10) || DBMS_UTILITY.format_error_backtrace );
    return oErrorObj; 
end callFunction;

--**********************************************************************************************************************************
static function buildXmlFile(xMainXml out xmltype) return STB$OERROR$RECORD is
  sCurrentName         constant varchar2(100) := $$PLSQL_UNIT||'.buildXmlFile';
  oErrorObj                     STB$OERROR$RECORD := STB$OERROR$RECORD();
  sMsg              varchar2(4000);
begin
  isr$trace.stat('begin', 'begin', sCurrentName);
  select xmlelement("ITEMS", xmlelement("TIMESTAMP", to_char(sysdate,isr$util.getInternalDate) )
          ,xmlagg(
             xmlelement("ITEM", xmlattributes(isdefault AS "default"),xmlelement("ALIAS", alias)
            , xmlelement("FULLNAME", dbhost || ':' || dbport || 
                          case when sid is not null then ':' || sid when service_name is not null then  '/' || service_name end )
             , xmlelement("DBUSER", dbuser) 
             , xmlelement("DBPASSWORD", dbpassword) 
             ))) into xMainXml from isr$frontend$main$config;
  isr$trace.stat('end', 'end', sCurrentName);
  return oErrorObj;           
exception
  when others then
    sMsg :=  utd$msglib.getmsg ('exUnexpectedError', isr$obj$base().nCurrentLanguage, csP1 => SQLERRM(SQLCODE));
    oErrorObj.handleError ( Stb$typedef.cnSeverityCritical, sMsg, sCurrentName,
                            DBMS_UTILITY.format_error_stack || CHR(10) || DBMS_UTILITY.format_error_backtrace );
    return oErrorObj; 
end buildXmlFile;

--**********************************************************************************************************************************
static function saveXmlFile(xMainXml in xmltype) return STB$OERROR$RECORD is
  sCurrentName         constant varchar2(100) := $$PLSQL_UNIT||'.saveXmlFile';
  oErrorObj                     STB$OERROR$RECORD := STB$OERROR$RECORD();
  sMsg       varchar2(4000);
  sMainXml   clob ;
begin
  isr$trace.stat('begin', 'begin', sCurrentName);
  select XMLSerialize(content xMainXml as clob version '1.0') into sMainXml from dual;
  isr$trace.debug('xMainXml', '-> logclob', sCurrentName, sMainXml);
  merge into isr$frontend$file ff using
  (select 'main.xml' as filename, 'text/xml' as mimetype, sMainXml as textfile from dual) j
  on (ff.filename = j.filename)
  when matched then update set ff.textfile = j.textfile            
                               , ff.mimetype = j.mimetype 
  when not matched then insert ( ff.fileid
                               , ff.filename
                               , ff.mimetype
                               , ff.textfile)
  values( SEQ$ISR$FRONTEND$FILE.NEXTVAL
         , j.filename
         , j.mimetype
         , j.textfile); 
  commit;       
  isr$trace.stat('end', 'end', sCurrentName);
  return oErrorObj;           
exception
  when others then
    sMsg :=  utd$msglib.getmsg ('exUnexpectedError', isr$obj$base().nCurrentLanguage, csP1 => SQLERRM(SQLCODE));
    oErrorObj.handleError ( Stb$typedef.cnSeverityCritical, sMsg, sCurrentName,
                            DBMS_UTILITY.format_error_stack || CHR(10) || DBMS_UTILITY.format_error_backtrace );
    return oErrorObj; 
end saveXmlFile;


--***********************************************************************************************************************
static function getLov (sEntity  in varchar2, ssuchwert  in varchar2, oSelectionList out isr$tlrselection$list)
   return stb$oerror$record
is
  sCurrentName                  constant varchar2(100) := $$PLSQL_UNIT||'.getLov('||sEntity||')'; 
  oErrorObj                     stb$oerror$record := STB$OERROR$RECORD();
  
 
begin
  isr$trace.stat('begin','sEntity = ' || sEntity || '; ssuchwert =' || ssuchwert, sCurrentName); 
  oSelectionList := isr$tlrselection$list (); 
  case
    when UPPER (sEntity) = 'ISDEFAULT' then
        oSelectionList.EXTEND;
        oSelectionList (oSelectionList.COUNT()) := ISR$OSELECTION$RECORD(null, null, null, null, oSelectionList.COUNT());
        oSelectionList.EXTEND;
        oSelectionList (oSelectionList.COUNT()) := ISR$OSELECTION$RECORD('true', 'true', 'true', null, oSelectionList.COUNT());
        oSelectionList.EXTEND;
        oSelectionList (oSelectionList.COUNT()) := ISR$OSELECTION$RECORD('false', 'false', 'false', null, oSelectionList.COUNT());
   
  end case;  
  
  isr$obj$base.setLovListSelection(ssuchwert, oSelectionList);
  
  isr$trace.stat('end','end', sCurrentName);
  return oErrorObj;
exception
    when others then
    oErrorObj.handleError ( Stb$typedef.cnSeverityCritical, SQLERRM, sCurrentName,
                           DBMS_UTILITY.format_error_stack || CHR(10) || DBMS_UTILITY.format_error_backtrace);
    return oErrorObj;
end   getLov;

end;