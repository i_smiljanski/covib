package resources;

import com.utd.gui.resources.I18N;
import com.utd.stb.back.database.Dispatcher;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import javax.imageio.ImageIO;
import javax.swing.*;
import java.awt.image.BufferedImage;
import java.net.URL;
import java.util.Map;
import java.util.MissingResourceException;
import java.util.ResourceBundle;


/**
 * Resources for the Gui part of Stabber, need some Work and Documentation.
 *
 * @author PeterBuettner.de
 */
public class Res {
    public static final Logger log = LogManager.getRootLogger();
    public static final String RESOURCE_PATH = "icons/resources/";

    public static Dispatcher dispatcher;

    private static Class clazz = Res.class;

    private static ResourceBundle bundle;

    /**
     * The resourcebundle
     *
     * @return
     */
    public static ResourceBundle getBundle() {

        if (bundle == null) {
            bundle = ResourceBundle.getBundle(RESOURCE_PATH + "StabberStringsBundle");
        }
        return bundle;
    }

    /**
     * rereads the Resourcebundle, so data <b>read in the future </b> will be
     * new
     */
    public static void resetLanguage() {

        bundle = null;
    }

    /**
     * get the image icon with the specified name
     *
     * @param name
     * @return ImageIcon
     */
    public static ImageIcon getII(String name) {
        ClassLoader cl = clazz.getClassLoader();
        URL iconUrl = cl.getResource(Res.RESOURCE_PATH + name);
        if (iconUrl != null) {
            try {
                BufferedImage image = ImageIO.read(iconUrl);
                return new ImageIcon(image);
            } catch (Exception e) {
                log.error(e);
            }
        }
        else{
            log.warn("resources '"+ name + "' could not be find");
        }
        return null;
    }

    /**
     * Loads the properties of an action into a map, reads name mnemonic
     * accelerator tooltip, output are the corresponding names in Action. This
     * puts <b>Strings </b> into the map, so you have to convert them to
     * Integer, KeyStroke,... by yourself
     *
     * @param resName the starting part, e.g.: "reportwizzard.action.x"
     * @return
     */
    public static Map getActionMap(String resName) {

        return I18N.getActionMap(getBundle(), resName);
    }

    /**
     * returns the String from the backend
     *
     * @param key
     * @return
     */
    public static String getBackendString(String key, String... params) {

        try {
            String text = dispatcher.getTranslation(key, params);
            if (text.equals(key)) throw new Exception();
            return text;
        } catch (Exception e) {
            try {
                return getBundle().getString(key);
            } catch (MissingResourceException mre) {
//                mre.printStackTrace();
                log.error("MissingResourceException for key '" + key + "'");
            }
            return key;
        }
    }

    /**
     * returns the String from the default bundle or null if not found
     *
     * @param key
     * @return
     */
    public static String getString(String key, String... params) {

        return getBackendString(key, params);
    }
}