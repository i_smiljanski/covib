package resources;

import com.utd.stb.back.database.Dispatcher;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import javax.imageio.ImageIO;
import javax.swing.*;
import java.awt.*;
import java.awt.image.BufferedImage;
import java.io.IOException;
import java.io.InputStream;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.HashMap;
import java.util.Map;
import java.util.Properties;


/**
 * Load icons by id, gui may ask for a special size, if size is not present
 * behavior is undefined by now. Only ask for icons you know are there.
 *
 * @author PeterBuettner.de
 */
public class IconSource {
    public static final Logger log = LogManager.getRootLogger();

    public static Dispatcher dispatcher;

    private Properties iconMap = new Properties();

    /**
     * cache and never throw away, later use SoftReferences. Use prefixes
     * :small/ medium/ header/ for now
     */
    private Map<String, Icon> iconCache = new HashMap<>();

    /*
     * maybe we use the look & feel icons: UIManager.getIcon(...); with
     * "OptionPane.errorIcon" "OptionPane.informationIcon"
     * "OptionPane.warningIcon" "OptionPane.questionIcon"
     */

    public IconSource() {

        try {
            ClassLoader loader = Thread.currentThread().getContextClassLoader();
            InputStream is = loader.getResourceAsStream(Res.RESOURCE_PATH + "iconmap.txt");
            iconMap.load(is);
        } catch (IOException e) { // should not happen, if so we show no icons
            e.printStackTrace();
        }
    }

    private Icon getIcon(String size, String id) {
//        log.debug(id);
        if (id == null || id.length() < 2) return null;// we do only those
        // stock ones for now
        String sizeId = size + "/" + id;
        Icon icon = iconCache.get(sizeId);
        if (icon == null) {

            if (id.indexOf(':') >= 0) { // an url is meant, we ignore here the SIZE !

                URL url = null;
                try {
                    url = new URL(id);
                } catch (MalformedURLException e) {
                    e.printStackTrace();
                }
                if (url != null) {

                    BufferedImage bi = null;
                    try {
                        bi = ImageIO.read(url.openStream());
                    } catch (IOException e1) {
                        e1.printStackTrace();
                    }
                    if (bi != null) icon = new ImageIcon(bi);// ImageIcon(url);  would block
                }

            } else { // an id

                byte[] bytes = getIconAsBytes(size, id);
                if (bytes != null) {
                    icon = new ImageIcon(bytes);
                }
                try {
                    if (icon == null) {
                        if (id.charAt(0) == ',') {
                            id = id.substring(1); // remove ','
                        }
                        String imgName = Res.RESOURCE_PATH + iconMap.getProperty(size + "/" + id);
                        URL url = getClass().getClassLoader().getResource(imgName);
//                        log.debug(url);
                        if (url != null) icon = new ImageIcon(url);
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
            iconCache.put(sizeId, icon);// even put on null
        }
//        log.debug(icon);
        return icon;
    }

    /**
     * tries to load the icon with the id, with a size suitable for display in a
     * tree/list/listview of around 16x16 pixel
     *
     * @param id
     * @return
     */
    public Icon getSmallIconByID(String id) {

        return getIcon("small", id);
    }

    /**
     * tries to load the icon with the id, with a size suitable for display in a
     * tree/list/listview with a big font of around 32x32 pixel
     *
     * @param id
     * @return
     */
    public Icon getMediumIconByID(String id) {

        return getIcon("medium", id);
    }

    /**
     * tries to load the icon with the id, with a size suitable for display in a
     * headerbar of a dialog/wizzard of around 50x50 pixel
     *
     * @param id
     * @return
     */
    public Icon getHeaderIconByID(String id) {

        return getIcon("header", id);
    }

    /**
     * tries to load the <b>image </b> with the id, with a size for display as
     * an icon in a frame
     *
     * @param id
     * @return
     */
    public Image getSmallImageByID(String id) {

        ImageIcon ii = (ImageIcon) getIcon("small", id);
        return ii == null ? null : ii.getImage();
    }

    public Image getMediumImageByID(String id) {

        ImageIcon ii = (ImageIcon) getIcon("medium", id);
        return ii == null ? null : ii.getImage();
    }

    /**
     * tries to load the icon from the database
     *
     * @param size, id
     * @return
     */
    private byte[] getIconAsBytes(String size, String id) {
        if (dispatcher != null) {
            try {
                byte[] iconAsBytes = dispatcher.getIcon(size, id);
                return iconAsBytes;
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
        return null;
    }

}