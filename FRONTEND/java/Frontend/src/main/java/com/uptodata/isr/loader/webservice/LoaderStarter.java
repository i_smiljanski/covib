package com.uptodata.isr.loader.webservice;

import com.uptodata.isr.gui.lookAndFeel.TweakUI;
import com.uptodata.isr.server.utils.logging.log4j2.LogHelper;
import com.utd.stb.Stabber;
import org.apache.commons.lang.StringUtils;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.apache.logging.log4j.core.LoggerContext;

import javax.swing.*;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.net.URISyntaxException;
import java.net.URL;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Properties;

/**
 * Created by schroedera85 on 12.11.2015.
 */
public class LoaderStarter {
    public static final Logger log = LogManager.getRootLogger();

    public static void main(String args[]) {
        String loaderPropertiesFileName = "loader.properties";
        File propertiesFile = new File(loaderPropertiesFileName);
        if (propertiesFile.exists()) { // properties file was found
            log.debug("Try to start with parameters from  properties file " + propertiesFile.toString());
            Properties properties = readProperties(propertiesFile);
            if (properties == null) {
                writeErrorInLogAndExit("Can not load properties for loader. Please correct the file " + propertiesFile.getAbsolutePath() + " or remove/rename it and start with arguments");
            }
            start(properties);
        } else {
            log.debug("Could not load file " + propertiesFile.getAbsolutePath() + " because it is not found");
            log.debug("Try to start with parameters from arguments");
            start(args);
        }
    }


    // nicht entfernen! die methode witrd im projekt RenderingServer verwendet
    //todo das ist derselbe code wie in main methode. Damit muss man irgendwas tun
    public static Properties readProperties(String propertiesFileName) {
        File propertiesFile = new File(propertiesFileName);
        if (propertiesFile.exists()) { // properties file was found
            log.debug("Try to read  parameters from  properties file " + propertiesFile.toString());
            Properties properties = readProperties(propertiesFile);
            return properties;
        }
        return null;
    }

    public static Properties readProperties(File propertiesFile) {
        try {
            Properties loaderProperties = new Properties();
            FileInputStream inputStream = new FileInputStream(propertiesFile);
            loaderProperties.load(inputStream);
            return loaderProperties;
        } catch (FileNotFoundException e) {
            log.error("File " + propertiesFile.getAbsolutePath() + " is not found");
        } catch (IOException e) {
            log.error("Can not read from file " + propertiesFile.getAbsolutePath());
        }
        return null;
    }

    private static void start(Properties loaderProperties) {
        String[] expectedProperties = new String[]{"port", "context", "directory", "loglevel", "isSystemTrayApp", "mainLoader", "dbAlias"};
        ArrayList<String> notExistKeys = getNotExistKeys(loaderProperties, expectedProperties);
        if (!notExistKeys.isEmpty()) {
            writeErrorInLogAndExit("Can not start with properties because the follow parameters are not set : " + notExistKeys);
        }
        String port = loaderProperties.getProperty("port");
        String context = loaderProperties.getProperty("context");
        String directory = loaderProperties.getProperty("directory");
        String loglevel = loaderProperties.getProperty("loglevel");
        String isSystemTrayApp = loaderProperties.getProperty("isSystemTrayApp");
        String mainLoader = loaderProperties.getProperty("mainLoader");
        String dbAlias = loaderProperties.getProperty("dbAlias");

        Stabber.initLogger(loglevel);
        start(port, context, directory, loglevel, isSystemTrayApp.equals("T"), mainLoader, dbAlias);
    }

    public static void start(Properties loaderProperties, LoaderWebserviceImpl loaderWebserver) {
        String[] expectedProperties = new String[]{"port", "context", "directory"};
        ArrayList<String> notExistKeys = getNotExistKeys(loaderProperties, expectedProperties);
        if (!notExistKeys.isEmpty()) {
            writeErrorInLogAndExit("Can not start with properties because the follow parameters are not set : " + notExistKeys);
        }
        String port = loaderProperties.getProperty("port");
        String context = loaderProperties.getProperty("context");
        String directory = loaderProperties.getProperty("directory");
        start(port, context, directory, loaderWebserver);
    }

    private static ArrayList<String> getNotExistKeys(Properties properties, String[] expectedKeys) {
        ArrayList<String> notExistKeys = new ArrayList<>();
        for (String key : expectedKeys) {
            String property = properties.getProperty(key);
            if (StringUtils.isEmpty(property)) {
                notExistKeys.add(key);
            }
        }
        return notExistKeys;
    }

    private static void start(String[] args) {
        int expectedSize = 6;
        if (args.length == 0) {
            log.error("No parameters for start up");
            System.exit(-1);
        }

        if (args.length < expectedSize) {
            log.error("Missing parameters for start up " + Arrays.toString(args));
            System.exit(-1);
        }

        final String port = args[0];
        final String context = args[1];
        final String directory = args[2];
        final String loglevel = args[3];
        final String isSystemTrayApp = args[4];
        final String mainLoader = args[5];
        final String dbAlias = args[6];
        start(port, context, directory, loglevel, isSystemTrayApp.equals("T"), mainLoader, dbAlias);
    }

    private static void writeErrorInLogAndExit(String error) {
        log.error(error);
        System.exit(-1);
    }

    public static void start(String port, String context, String directory, String loglevel, boolean isSystemTrayApp, String dbConnLoader, String dbAlias) {
        SwingUtilities.invokeLater(new Runnable() {
            public void run() {
                initLookAndFeel();
                try {
                    LoaderWebserviceImpl loaderWebserver = new LoaderWebserviceImpl(isSystemTrayApp, dbConnLoader, dbAlias);
//                    loaderWebserver.setLogLevel(loglevel);
                    loaderWebserver.init(port, context, directory, null);
                    initLoaderGui(loaderWebserver);
                } catch (Exception ex) {
                    ex.printStackTrace();
                }
            }
        });
    }

    public static void start(String port, String context, String directory, LoaderWebserviceImpl loaderWebserver) {
        SwingUtilities.invokeLater(new Runnable() {
            public void run() {
                initLookAndFeel();
                try {
                    loaderWebserver.init(port, context, directory, null);
                    initLoaderGui(loaderWebserver);
                } catch (Exception ex) {
                    ex.printStackTrace();
                }
            }
        });
    }

    public static void initLookAndFeel() {
        String licenseKey = "w6.tuJhnfnDInGp:fvqIJaaeKT4yKF42";
        com.jidesoft.utils.Lm.verifyLicense("up to data professional services", "iStudyReporter", licenseKey);
        TweakUI.setLookAndFeel();
    }

    public static void initLoaderGui(LoaderWebserviceImpl loaderWebserver) {
        log.debug("initLoaderGui");
        LoaderGui loaderGui = new LoaderGui();
        loaderGui.setSystemTrayApp(true);
        loaderWebserver.setLoaderGui(loaderGui);
        int port = loaderWebserver.getPort();
        String directory = loaderWebserver.getBaseDirectoryPart();
        loaderGui.createStatusBar(String.valueOf(port), directory);
        loaderGui.buildGUI();
        loaderGui.createTrayMessenger();
    }
}
