package com.uptodata.isr.loader.webservice.word;

import com.aspose.words.Document;
import com.aspose.words.DocumentProperty;
import org.apache.commons.lang.BooleanUtils;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.io.FileInputStream;
import java.io.InputStream;

/**
 * Created by smiljanskii60 on 21.01.2021.
 */
public class CompareDocumentOID {
    public static Logger log = LogManager.getRootLogger();

    public static boolean compareDocumentOID(String documentOID, InputStream stream)
            throws Exception {
        log.debug("begin");
        AsposeWord.loadLicence();
        Document doc = new Document(stream);
        stream.close();
        log.debug(documentOID);
        for (DocumentProperty prop : doc.getCustomDocumentProperties()) {
            if ("DOC_OID".equalsIgnoreCase(prop.getName()) && documentOID.equals(prop.getValue())) {
                log.debug("true");
                return true;
            }
        }
        log.debug("false");
        return false;
    }


    public static void main(String[] args) {
        try {
            FileInputStream is = new FileInputStream(System.getProperty("user.dir") + "\\test\\G-A-LAB-20-10-11-FEB-2021-11410.docx");
            String isEqual = BooleanUtils.toStringTrueFalse(compareDocumentOID("1.3.6.1.4.1.24263.1.1.15326.1.2.1.11410.10683", is));
            System.out.println("is = " + isEqual);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}
