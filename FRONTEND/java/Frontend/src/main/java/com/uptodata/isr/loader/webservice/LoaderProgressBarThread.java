package com.uptodata.isr.loader.webservice;

import com.jidesoft.status.ProgressStatusBarItem;

/**
 * @author Inna Smiljanski created 04.01.11
 */
public class LoaderProgressBarThread extends Thread {

    final private int delay;
    final private ProgressStatusBarItem statusBarProgress;
    private int jobCnt;
    boolean stopped;

    public LoaderProgressBarThread(ProgressStatusBarItem statusBarProgress, int delay) {
        this.statusBarProgress = statusBarProgress;
        statusBarProgress.setProgress(0);
        this.delay = delay;
    }

    public void run() {

        while (!stopped) {
            statusBarProgress.setIndeterminate(jobCnt > 0);
            try {
                Thread.sleep(delay);
            } catch (InterruptedException ignored) {
                ignored.printStackTrace();
            }
        }
    }

    public void informToStop() {
        statusBarProgress.setIndeterminate(false);
        stopped = true;
    }

    public void setJobCnt(int jobCnt) {
        this.jobCnt = jobCnt;
    }

    public int getJobCnt() {
        return jobCnt;
    }
}
