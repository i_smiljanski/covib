package com.uptodata.isr.loader.webservice;

import com.utd.stb.gui.loader.resources.Res;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import resources.IconSource;

import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;

/**
 * Created by schroedera85 on 12.11.2015.
 */
public class TrayMessager {

    static final String LOADER_PROGRESS_TOOLTIP = "Loader.progress.tooltip";
    static final String LOADER_SYSTEM_TRAY_ERROR = "Loader.system.tray.error";
    static final String LOADER_SYSTEM_TRAY_ICON_ERROR = "Loader.system.tray.icon.error";
    static final String LOADER_SYSTEM_TRAY_ITEM_DISPLAY = "Loader.system.tray.item.display";
    static final String LOADER_SYSTEM_TRAY_ITEM_EXIT = "Loader.system.tray.item.exit";
    static final String LOADER_RUNNING_JOBS_ERROR = "Loader.running.jobs.error";
    static final String LOADER_TRAY_ICON = "loaderTrayApp";

    public static final Logger log = LogManager.getRootLogger();
    private TrayIcon trayIcon;
    private IconSource icons = new IconSource();
    private LoaderGui loaderGui;

    void showTrayMessage(final String header, final String msg, final TrayIcon.MessageType type) {
        if (trayIcon != null) {
            SwingUtilities.invokeLater(new Runnable() {
                public void run() {
                    trayIcon.displayMessage(header, msg, type);
                }
            });
        }
    }

    public void buildTrayIcon() {
        if (!SystemTray.isSupported()) {
            log.error(Res.getString(LOADER_SYSTEM_TRAY_ERROR));
            trayIcon = null;
            return;
        }

        Image image = icons.getSmallImageByID(LOADER_TRAY_ICON);
        if (image == null) {
            log.error("Can not load image " + LOADER_TRAY_ICON + " for tray icon");
        }
        trayIcon = new TrayIcon(image);

        final SystemTray tray = SystemTray.getSystemTray();
        final PopupMenu popup = new PopupMenu();

        MenuItem displayItem = new MenuItem(Res.getString(LOADER_SYSTEM_TRAY_ITEM_DISPLAY));
        MenuItem exitItem = new MenuItem(Res.getString(LOADER_SYSTEM_TRAY_ITEM_EXIT));

        popup.add(displayItem);
        popup.add(exitItem);

        trayIcon.setPopupMenu(popup);
        setTrayTooltip(0);

        try {
            tray.add(trayIcon);
        } catch (AWTException e) {
            log.error(Res.getString(LOADER_SYSTEM_TRAY_ICON_ERROR));
            trayIcon = null;
            return;
        }

        displayItem.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e) {
                loaderGui.showFrame();
            }
        });

        exitItem.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e) {
                boolean isServiceStopped = loaderGui.tryToStopService();
                if (isServiceStopped) {
                    tray.remove(trayIcon);
                    System.exit(0);
                } else {
                    showTrayMessage("", String.format(Res.getString(LOADER_RUNNING_JOBS_ERROR), loaderGui.getJobsCount()), TrayIcon.MessageType.INFO);
                }
            }
        });

        trayIcon.addMouseListener(new MouseAdapter() {
            @Override
            public void mouseClicked(MouseEvent e) {
                if (e.getClickCount() == 2) {
                    loaderGui.showFrame();
                }
            }
        });

    }

    void setTrayTooltip(int jobCnt) {
        if (trayIcon != null) {
            SwingUtilities.invokeLater(new Runnable() {
                public void run() {
                    trayIcon.setToolTip(String.format(Res.getString(LOADER_PROGRESS_TOOLTIP), jobCnt));
                }
            });
        }
    }

    public void setLoaderGui(LoaderGui loaderGui) {
        this.loaderGui = loaderGui;
    }
}
