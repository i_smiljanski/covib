/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package com.uptodata.isr.loader.webservice;

import java.io.File;

/**
 * @author dortm19
 */
public class LoaderJob {

    private int jobid;
    private boolean running;
    private File jobDirectory;
    private int progress;

    public LoaderJob(int jobid, File jobDirectory) {
        this.jobid = jobid;
        this.running = true;
        this.jobDirectory = jobDirectory;
    }

    public int getJobid() {
        return jobid;
    }

    public void setJobid(int jobid) {
        this.jobid = jobid;
    }

    public boolean isRunning() {
        return running;
    }

    public void setRunning(boolean running) {
        this.running = running;
    }

    public File getJobDirectory() {
        return jobDirectory;
    }

    public void setJobDirectory(File jobDirectory) {
        this.jobDirectory = jobDirectory;
    }

    public int getProgress() {
        return progress;
    }

    public void setProgress(int progress) {
        this.progress = progress;
    }

}
