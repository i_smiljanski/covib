package com.uptodata.isr.loader.webservice;

import com.uptodata.isr.utils.xml.dom.XmlHandler;
import com.uptodata.isr.gui.lookAndFeel.TweakUI;
import com.utd.stb.gui.loader.resources.Res;
import org.apache.commons.io.FileUtils;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.w3c.dom.Document;
import org.w3c.dom.Node;

import javax.swing.*;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.ArrayList;

/**
 * Created by schroedera85 on 13.11.2015.
 */
public class AppExecuter {
    static final String LOADER_CHOOSER_OPEN_TITLE = "Loader.chooser.open.title";
    static final String LOADER_CHOOSER_OPEN_OK = "Loader.chooser.open.ok";
    static final String LOADER_WARNING_OK = "Loader.warning.ok";
    static final String LOADER_PROGRAM_SHORT = "Loader.program.short";
    static final String LOADER_PROGRAM_TEXT = "Loader.program.text";
    static final String LOADER_CONFIG_FILE = "config.xml";

    public static final Logger log = LogManager.getRootLogger();

    private static boolean cancelled;

    public static Process executeApplication(File mainDirectory, String application, String applicationPathes,
                                             ArrayList<String> parameterList, XmlHandler configXml) throws IOException {
        log.debug("executeApplication: application " + application);
        Process p;
        ProcessBuilder processBuilder;
        parameterList.add(0, application);

        String[] parameter = parameterList.toArray(new String[parameterList.size()]);
//        log.debug("executeApplication: parameter " + parameter);

        // try to start in the job directory
        parameter[0] = mainDirectory + System.getProperty("file.separator") + application;
        log.debug("try to start command in job directory " + parameter[0]);
        try {
            processBuilder = new ProcessBuilder(parameter);
            p = processBuilder.start();
            if (p != null) {
                cancelled = false;
                return p;
            }
        } catch (Exception ignored) {
            log.error(ignored);
            // ignore error and try it with pathes in config.xml
        }

        // try to start with the pathes in config.xml
        String[] applicationPathList;
        if (applicationPathes != null) {
            applicationPathList = applicationPathes.split(";");
            for (String curPath : applicationPathList) {
                parameter[0] = curPath + System.getProperty("file.separator") + application;
                log.debug("try to start command in application pathes " + parameter[0]);
                try {
                    processBuilder = new ProcessBuilder(parameter);
                    File errorProcessFile = FileUtils.getFile(mainDirectory, "error.log");
                    processBuilder.redirectError(errorProcessFile);
                    p = processBuilder.start();
                    if (p != null) {
                        cancelled = false;
                        return p;
                    }
                } catch (Exception ignored) {
                    log.error(ignored);
                    // ignore error and try it with the other pathes or ask user for path
                }
            }
        }

        // application not in path. Show a warn.
        int answer = showJobWarning(Res.getString(LOADER_PROGRAM_SHORT), Res.getString(LOADER_PROGRAM_TEXT) + " " + application);

        if (answer == 0) {

            // ask user to select a correct path
            File file = showFileDialog();

            String selectedPath;

            if (file != null) {
                if (file.isDirectory()) {
                    selectedPath = file.getAbsolutePath();
                } else {
                    selectedPath = file.getParent();
                }
                parameter[0] = selectedPath + System.getProperty("file.separator") + application;
                log.debug("try to start command with selected path " + parameter[0]);
                if ((p = Runtime.getRuntime().exec(parameter)) != null) {
                    // save new path list
                    try {
                        applicationPathes = ((applicationPathes != null && !applicationPathes.equals("")) ? applicationPathes + ";" : "") + selectedPath;
                        log.debug("save new application path list " + applicationPathes);
                        Document configDoc = configXml.getDoc();
                        Node appsNode = configDoc.getElementsByTagName("apps").item(0);
                        try {
                            Node applicationPathesNode = configDoc.getElementsByTagName("applicationpathes").item(0);
                            appsNode.removeChild(applicationPathesNode);
                        } catch (Exception ignored) {
                            log.error(ignored);
                            //todo ist hier nullpointer exception?
                            // node not in xml so we don't need to delete it
                        }
                        configXml.createElement("applicationpathes", applicationPathes, null, null, appsNode);
                        FileOutputStream oStream = new FileOutputStream(mainDirectory.getAbsolutePath() + System.getProperty("file.separator") + LOADER_CONFIG_FILE);
                        configXml.save(oStream);
                        oStream.close();
                    } catch (Exception ignored) {
                        log.error(ignored);
                        //todo hier muss man prüfen, welche exception kommt
                        // if we can't save the new path list the user can enter it again when he needs it
                    }
                    cancelled = false;
                    return p;
                }
            } else {
                cancelled = true;
                return null;
            }
        } else {
            cancelled = true;
            return null;
        }
        // enough. May loop until user finds or whatever
        cancelled = false;
        return null;
    }


    private static File showFileDialog() {
        JFileChooser fc;
        try {
            fc = new JFileChooser(System.getProperty("user.home"));
        } catch (Exception e) {
            // for Vista and Windows 7
            try {
                UIManager.setLookAndFeel(UIManager.getCrossPlatformLookAndFeelClassName());
            } catch (Exception ignored) {
                log.error("Can not set look and feel. " + ignored);
            }
            fc = new JFileChooser(System.getProperty("user.home"));
            TweakUI.setLookAndFeel();
        }

        fc.setMultiSelectionEnabled(true);
        fc.setFileSelectionMode(JFileChooser.FILES_AND_DIRECTORIES);
        fc.setMultiSelectionEnabled(true);
        fc.setDialogTitle(Res.getString(LOADER_CHOOSER_OPEN_TITLE));
        fc.setApproveButtonText(Res.getString(LOADER_CHOOSER_OPEN_OK));
        int returnVal = fc.showSaveDialog(null);
        if (returnVal == JFileChooser.APPROVE_OPTION) {
            return fc.getSelectedFile();
        }
        return null;
    }

    private static int showJobWarning(String title, String message) {
        Object[] options = {Res.getString(LOADER_WARNING_OK)};
        return JOptionPane.showOptionDialog(null, message, title, JOptionPane.DEFAULT_OPTION,
                JOptionPane.WARNING_MESSAGE, null, options, options[0]);
    }

    public static boolean isCancelled() {
        return cancelled;
    }
}
