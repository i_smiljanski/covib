package com.uptodata.isr.loader.webservice;

import com.uptodata.isr.utils.ConvertUtils;
import com.uptodata.isr.utils.exceptions.MessageStackException;
import com.uptodata.isr.utils.xml.dom.XmlHandler;
import com.utd.stb.gui.loader.resources.Res;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;
import org.apache.commons.io.FileUtils;

import java.io.*;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.security.DigestInputStream;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import static com.uptodata.isr.loader.webservice.LoaderWebserviceImpl.onError;

/**
 * Created by schroedera85 on 13.11.2015.
 */

//todo man kann FileSystemWorker aus dem projekt observer benutzen
public class FileSystemWorker {
    public static final Logger log = LogManager.getRootLogger();
    static final String LOADER_FILE_COPY_ERROR = "Loader.file.copy.error";
    static final String LOADER_FILE_EXISTS_ERROR = "Loader.file.exists.error";
    static final String LOADER_FILE_DELETE_ERROR = "Loader.file.delete.error";

    public static String deleteFiles(String path, NodeList sources) throws MessageStackException {

        log.debug("deleteFiles: path " + path + " sources " + sources.getLength());

        for (int i = 0; i < sources.getLength(); i++) {
            Node source = sources.item(i);
            Element sourceElement = (Element) source;
            String filename = sourceElement.getAttribute("name");
            File file = new File(path, filename);
            log.debug("deleteFiles: file " + file);
            if (!file.delete()) {
                String userErrorMessage = String.format(Res.getString(LOADER_FILE_DELETE_ERROR), path + filename);
                StackTraceElement[] stackTraceElements = Thread.currentThread().getStackTrace();
                String stackString = Arrays.toString(stackTraceElements);
                MessageStackException messageStackException = new MessageStackException(MessageStackException.SEVERITY_CRITICAL, userErrorMessage, userErrorMessage, stackString);
                log.error(userErrorMessage);
                throw messageStackException;
            }
        }
        return "T";
    }

    public static String copyFiles(File mainDirectory, String path, String document, boolean overwrite, NodeList sources) throws MessageStackException {

        log.debug("copyFiles: mainDirectory=" + mainDirectory + " overwrite=" + overwrite + " path=" + path + " document=" + document + " sources=" + sources.getLength());
        String returnValue = "F";
        for (int i = 0; i < sources.getLength(); i++) {
            Node source = sources.item(i);
            Element sourceElement = (Element) source;
            String sourceName = sourceElement.getAttribute("name");
            log.debug(sourceName);
            boolean isMaster = sourceElement.getAttribute("leading").equalsIgnoreCase("T");
            if (document != null && document.length() != 0 && (isMaster || i == 0)) {
                File file = new File(path, document);
                if ((file.exists() && overwrite) || !file.exists()) {
                    returnValue = copy(mainDirectory, path, sourceName, document);
                } else {
                    returnValue = String.format(Res.getString(LOADER_FILE_EXISTS_ERROR), path + " " + document);
                }
            } else {
                File file = new File(path, sourceName);
                if ((file.exists() && overwrite) || !file.exists()) {
                    returnValue = copy(mainDirectory, path, sourceName, sourceName);
                } else {
                    returnValue = String.format(Res.getString(LOADER_FILE_EXISTS_ERROR), path + " " + sourceName);
                }
            }
        }
        return returnValue;
    }

    static List<String> copyFilesToPath(File fromDirectory, String toDirectory, boolean overwrite, NodeList sources) throws IOException, NoSuchAlgorithmException {

        log.debug("copyFiles: from " + fromDirectory + " overwrite=" + overwrite + " to " + toDirectory + " sources=" + sources.getLength());

        List<String> retList = new ArrayList<>();
        String hashValue = null;
        File hashFile = null;

//        File hash = copyFileToPath(fromDirectory, toDirectory, overwrite, "test", "hashes.txt", null);
        String format = "%-15s %-70s %-412s";
        StringBuilder hashStr = new StringBuilder();
        hashStr.append(System.getProperty("line.separator"));
        hashStr.append(String.format(format, "Algorithm", "Hash", "Path"));
        hashStr.append(System.getProperty("line.separator"));
        hashStr.append(String.format(format, "---------", "----", "----"));
        hashStr.append(System.getProperty("line.separator"));
        try {
            for (int i = 0; i < sources.getLength(); i++) {
                Node source = sources.item(i);
                Element sourceElement = (Element) source;
                String sourceName = sourceElement.getAttribute("name");
                String sourcePath = sourceElement.getAttribute("path");
                boolean isHashtable = sourceName.startsWith("Hashtable") && sourceName.endsWith(".txt");
//            log.debug(String.format("%-6s %-55s %-6s %-20s", "path:", sourcePath, "name:", sourceName));
                File file = copyFileToPath(fromDirectory, toDirectory, overwrite, sourcePath, sourceName, hashStr, format);
                if (isHashtable) {
                    hashFile = file;
                }

            }
            log.debug(hashFile);
            if (hashFile == null){
                hashFile = new File(toDirectory, "Hashtable.txt");
            }
//            log.debug(hashStr);
            if (hashFile != null) {
                Writer hashWriter = new FileWriter(hashFile);
                ConvertUtils.writeStringToWriter(hashStr.toString(), hashWriter);
                hashWriter.close();
                MessageDigest md = MessageDigest.getInstance("SHA-256");
                hashValue = checksum(hashFile, md);
            }
            retList.add("T");
            retList.add(hashValue);
        } catch (IOException | NoSuchAlgorithmException e) {
            log.error(e);
            throw e;
        }
        return retList;
    }

    private static File copyFileToPath(File fromDirectory, String toDirectory, boolean overwrite, String path,
                                       String fileName, StringBuilder hashStr, String format) throws IOException, NoSuchAlgorithmException {
        File file;
        try {
            Path dir = Paths.get(toDirectory, path);
            Files.createDirectories(dir);
            file = new File(dir.toFile(), fileName);
//        log.debug(path + "  " + fileName);
            String pathForHash = "";
            String[] pathParts = path.split(File.pathSeparator);
            int pathPartsLength = pathParts.length;
            pathForHash = path.substring(path.indexOf(pathParts[pathPartsLength - 1]));
            if (file.exists() && overwrite || !file.exists()) {
                String hex = copyFileAndCreateChecksum(fromDirectory, dir.toFile().getAbsolutePath(), fileName, fileName);
                if (!fileName.startsWith("Hashtable")) {
                    // log.debug(pathForHash);
                    hashStr.append(String.format(format, "SHA256", hex, pathForHash + fileName)).append(System.getProperty("line.separator"));
                }
            }
        } catch (IOException | NoSuchAlgorithmException e) {
            log.error(e);
            throw e;
        }
        return file;
    }

    private static String copyFileAndCreateChecksum(File sourceDir, String targetDir, String sourceName, String targetname) throws IOException, NoSuchAlgorithmException {
        String hex;
        File source = new File(sourceDir, sourceName);
        File target = new File(targetDir, targetname);
        try {
            FileUtils.copyFile(source, target);
            MessageDigest md = MessageDigest.getInstance("SHA-256");
            hex = checksum(target, md);
        } catch (IOException | NoSuchAlgorithmException e) {
            log.error(e);
            throw e;
        }
//        log.debug(hex);
        return hex;
    }

    private static String checksum(File filepath, MessageDigest md) throws IOException {

        // file hashing with DigestInputStream
        try (DigestInputStream dis = new DigestInputStream(new FileInputStream(filepath), md)) {
            while (dis.read() != -1) ;
            md = dis.getMessageDigest();
        }

        // bytes to hex
        StringBuilder result = new StringBuilder();
        for (byte b : md.digest()) {
            result.append(String.format("%02x", b));
        }
        return result.toString();

    }

    public static String copy(File sourceDir, String targetDir, String sourceName, String targetname) throws MessageStackException {

        log.debug("copy: " + sourceDir + " " + sourceName + " to " + targetDir + " " + targetname);
        DataInputStream in = null;
        FileOutputStream fileOutStream = null;
        try {
            File source = new File(sourceDir, sourceName);
            File target = new File(targetDir, targetname);
            in = new DataInputStream(new FileInputStream(source));
            fileOutStream = new FileOutputStream(target);
            int len = (int) source.length();
            byte[] buf = new byte[len];
            in.readFully(buf);
            fileOutStream.write(buf);

        } catch (IOException e) {
            throw onError(e, String.format(Res.getString(LOADER_FILE_COPY_ERROR), sourceDir + " " + sourceName + " -> " + targetDir + " " + targetname),
                    MessageStackException.getCurrentMethod());
        } finally {
            try {
                if (in != null)
                    in.close();
                if (fileOutStream != null)
                    fileOutStream.close();
            } catch (IOException e) {
                e.printStackTrace();
            }

        }
        return "T";
    }


    public static void createAndSaveHashFile(File jobDirectory, String hashValue) throws IOException {
        XmlHandler xmlHandler = new XmlHandler();
        Element hashNode = xmlHandler.nodeRoot("HASH");
        xmlHandler.createElement("VALUE", hashValue,null, hashNode);
        File hashFile = new File(jobDirectory, "hashFile.xml");
        xmlHandler.save(new FileOutputStream(hashFile));
    }
}
