package com.uptodata.isr.loader.webservice.word;

import com.aspose.words.License;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.io.InputStream;

/**
 * Created by smiljanskii60 on 25.01.2021.
 */
public class AsposeWord {
    public static Logger log = LogManager.getRootLogger();
    /**
     * sets Aspose license
     */
    static void loadLicence() {
//        log.debug( "begin");
        ClassLoader loader = Thread.currentThread().getContextClassLoader();
        InputStream licenseFile = loader.getResourceAsStream("Aspose.Words.Java.lic");
        try {
            if (licenseFile.available() > 0) {
                License license = new License();
                license.setLicense(licenseFile);
//                log.debug( "end");
            }
        } catch (Exception e) {
            log.error("licenseFile error", e);
        }
    }
}
