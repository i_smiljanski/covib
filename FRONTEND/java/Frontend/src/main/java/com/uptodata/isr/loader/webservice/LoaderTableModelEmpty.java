package com.uptodata.isr.loader.webservice;

import com.utd.stb.gui.loader.resources.Res;

import javax.swing.table.AbstractTableModel;

import static com.uptodata.isr.loader.webservice.LoaderWebserviceImpl.LOADER_EMPTY_TABLE;

/**
 * little helper: empty data
 *
 * @author PeterBuettner.de
 */
class LoaderTableModelEmpty extends AbstractTableModel {

    /*
     * colCount: '1' will create a header, '0' no, but both may show an 'No
     * Data' in a table that supports this (draws this by itself
     */
    public int getColumnCount() {

        return 1;
    }

    public String getColumnName(int column) {

        return " ";
    }

    public int getRowCount() {

        return 1;
    }

    public Object getValueAt(int rowIndex, int columnIndex) {
        /*JLabel emptyLabel = new JLabel();
        emptyLabel.setText (Res.getString(LOADER_EMPTY_TABLE));
        emptyLabel.setIcon (UIManager.getIcon("OptionPane.informationIcon"));
        emptyLabel.setBorder (new EmptyBorder(5, 5, 5, 5));
        return emptyLabel; */
        return Res.getString(LOADER_EMPTY_TABLE);
    }

}
