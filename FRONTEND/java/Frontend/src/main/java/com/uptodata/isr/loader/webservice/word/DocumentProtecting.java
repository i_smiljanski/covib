package com.uptodata.isr.loader.webservice.word;

import com.aspose.words.Document;
import com.aspose.words.ProtectionType;
import com.aspose.words.SaveFormat;
import com.uptodata.isr.utils.ConvertUtils;
import org.apache.commons.io.FilenameUtils;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.InputStream;

/**
 * Created by smiljanskii60 on 21.10.2015.
 */
public class DocumentProtecting {
    public static Logger log = LogManager.getRootLogger();

    public static ByteArrayOutputStream unprotect(String docExt, String password, InputStream docStream) {
        AsposeWord.loadLicence();
        ByteArrayOutputStream output = null;
        try {
            Document doc = new Document(docStream);
            docStream.close();
            doc.unprotect(ConvertUtils.createHash(password, 8, "MD5"));
            output = new ByteArrayOutputStream();
            doc.save(output, getSaveFormat(docExt));
        } catch (Exception e) {
            log.error("Exception", e);
        }
        log.debug("end");
        return output;
    }


    public static ByteArrayOutputStream protectAllowOnlyRevisions(String fileName, String password, InputStream docStream)
            throws Exception {
        AsposeWord.loadLicence();
        ByteArrayOutputStream output;
        log.debug("begin");
        Document doc = new Document(docStream);
        docStream.close();
        String hashPwd = ConvertUtils.createHash(password, 8, "MD5");
        doc.unprotect(hashPwd);
        doc.protect(ProtectionType.ALLOW_ONLY_REVISIONS, hashPwd);
        output = new ByteArrayOutputStream();
        doc.save(output, getSaveFormat(fileName));
        log.debug("end");
        return output;
    }


    private static int getSaveFormat(String fileName) {
        String ext = FilenameUtils.getExtension(fileName);
        if ("DOCX".equalsIgnoreCase(ext)) {
            return SaveFormat.DOCX;
        }
        return SaveFormat.DOC;
    }


    /**
     * Only for testing: calling of application
     *
     * @param args 0:directory with files when calling
     * @throws Exception exception
     */
    public static void main(String[] args) {
        String targetpassword = "287eb3a05c0397e9cdf51915e02c9b80";

        try {
            File file = new File(System.getProperty("user.dir") + "\\test\\a.docx");
            FileInputStream stream = new FileInputStream(file);

            ByteArrayOutputStream outputStream = protectAllowOnlyRevisions(file.getName(), targetpassword, stream);
            outputStream.close();
            ConvertUtils.bytesToFile(outputStream.toByteArray(), new File(System.getProperty("user.dir") + "\\test\\result.docx"));
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}

