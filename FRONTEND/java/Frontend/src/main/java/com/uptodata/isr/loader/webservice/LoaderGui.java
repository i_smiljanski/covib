package com.uptodata.isr.loader.webservice;

import com.jidesoft.alert.Alert;
import com.jidesoft.alert.AlertGroup;
import com.jidesoft.animation.CustomAnimation;
import com.jidesoft.grid.CellRendererManager;
import com.jidesoft.grid.HierarchicalTable;
import com.jidesoft.grid.TableModelWrapperUtils;
import com.jidesoft.status.LabelStatusBarItem;
import com.jidesoft.status.ProgressStatusBarItem;
import com.jidesoft.status.StatusBar;
import com.jidesoft.swing.JideBoxLayout;
import com.jidesoft.utils.PortingUtils;
import com.uptodata.isr.gui.lookAndFeel.TweakUI;
import com.uptodata.isr.utils.exceptions.MessageStackException;
import com.uptodata.isr.utils.xml.dom.XmlHandler;
import com.utd.gui.controls.BorderSplitPane;
import com.utd.gui.controls.table.ListViewTable;
import com.utd.gui.controls.table.TableTools;
import com.utd.stb.back.data.client.*;
import com.utd.stb.back.database.MenuToDetail;
import com.utd.stb.back.ui.impl.userinputs.fields.TextInputImpl;
import com.utd.stb.back.ui.impl.userinputs.fields.TextUserInput;
import com.utd.stb.gui.loader.resources.Res;
import com.utd.stb.gui.swingApp.*;
import com.utd.stb.gui.swingApp.exceptions.ExceptionDialog;
import com.utd.stb.inter.application.DetailView;
import com.utd.stb.inter.application.Nodes;
import com.utd.stb.inter.userinput.UserInput;
import com.utd.stb.inter.userinput.UserInputs;
import org.apache.commons.codec.binary.Base64;
import org.apache.commons.lang.StringUtils;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;
import resources.IconSource;

import javax.swing.*;
import javax.swing.border.MatteBorder;
import javax.swing.table.AbstractTableModel;
import javax.swing.table.TableModel;
import java.awt.*;
import java.awt.event.MouseEvent;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;
import java.io.ByteArrayInputStream;
import java.sql.Types;
import java.util.ArrayList;

/**
 * Created by schroedera85 on 12.11.2015.
 */
public class LoaderGui {
    public static final Logger log = LogManager.getRootLogger();
    private IconSource icons = new IconSource();

    static final String LOADER_TEXT_START = "Loader.text.start";
    static final String LOADER_TEXT_TERMINATE = "Loader.text.terminate";
    static final String LOADER_TEXT_WARN = "Loader.text.warn";
    static final String LOADER_TEXT_ERROR = "Loader.text.error";
    static final String LOADER_FRAME_TITLE = "Loader.frame.title";
    static final String LOADER_PROGRESS_TOOLTIP = "Loader.progress.tooltip";
    static final String LOADER_STATUSBAR_PORT_ERROR_LABEL = "Loader.statusbar.port.error.label";
    static final String LOADER_STATUSBAR_PORT_LABEL = "Loader.statusbar.port.label";
    static final String LOADER_STATUSBAR_DIR_ERROR_LABEL = "Loader.statusbar.dir.error.label";
    static final String LOADER_STATUSBAR_DIR_LABEL = "Loader.statusbar.dir.label";
    static final String LOADER_APP_TREE_ERROR = "Loader.app.tree.error";
    static final String LOADER_APP_RIBBON_ERROR = "Loader.app.ribbon.error";
    static final String LOADER_RUNNING_JOBS_ERROR = "Loader.running.jobs.error";
    static final String LOADER_FRAME_ICON = "loaderFrameApp";
    static final String LOADER_WARNING_ICON = "loaderWarning";

    private String jobid_table_field = "ISRJOBID";
    private String progress_table_field = "PROGRESS";
    private String error_table_field = "ISRERRORCODE";

    private boolean isSystemTrayApp = false;

    private App app;
    private JFrame jobFrame;
    private StatusBar statusBar;
    private LoaderProgressBarThread progressBarThread;
    private JTable jobListTable;
    private AbstractTableModel jobListTableModel = new LoaderTableModelEmpty();
    private ProgressStatusBarItem statusBarProgress;
    private JProgressBar progressBar;
    private LoaderWebserviceImpl loaderWebservice;
    private TrayMessager trayMessager;
    private String schemaOwner;

    // Achtung! die methode darf nur dann aufgerufen, wann frontend geschlossen wird /nicht existiert.
    // Der grund dafür: statusBar wird sowohl in frontend als auch in jobFrame benutzt.
    // Wenn die methode aufgerufen wird und dann statusBar in frontend hinzugefügt(siehe methode "show()" in App.java), erscheint statusBar nicht mehr in jobFrame
    public void buildGUI() {

        log.debug("buildGUI");

        app = null;

        jobFrame = new JFrame(Res.getString(LOADER_FRAME_TITLE));
        log.debug("jobFrame init:" + jobFrame);
        TweakUI.setFrameDecoration(jobFrame);
        jobFrame.setIconImage(icons.getSmallImageByID(LOADER_FRAME_ICON));
        jobFrame.setDefaultCloseOperation(JFrame.DO_NOTHING_ON_CLOSE);

        jobFrame.addWindowListener(new WindowAdapter() {
            public void windowClosing(WindowEvent e) {
                onClickExit();
            }
        });

        jobListTable = createJobListTable();
        JPanel panel = new JPanel(new BorderLayout());

        panel.add(new BorderSplitPane(new JScrollPane(jobListTable), null, true));
        panel.add(statusBar, BorderLayout.SOUTH);
        jobFrame.getContentPane().add(panel);

        Dimension screenSize = Toolkit.getDefaultToolkit().getScreenSize();
        Dimension frameSize = new Dimension(1200, 400);
        int top = (screenSize.height - frameSize.height) / 2 - 100;
        int left = (screenSize.width - frameSize.width) / 2;
        jobFrame.setSize(frameSize);
        jobFrame.setLocation(left, top);
    }

    public void onClickExit() {
        if (isSystemTrayApp) {
            jobFrame.setVisible(false);
        } else {
            // if all jobs are terminated it is allowed to close the loader
            boolean isServiceStopped = tryToStopService();
            if (isServiceStopped) {
                System.exit(0);
            } else {
                showCanNotExitAlert();
            }
        }
    }

    public boolean tryToStopService() {
        boolean canExit = loaderWebservice.canTerminate();
        if (canExit) {
            stopService();
            return true;
        }
        return false;
    }

    public void showCanNotExitAlert() {
        int jobsCount = loaderWebservice.getJobCnt();
        String errorTemplate = Res.getString(LOADER_RUNNING_JOBS_ERROR);
        String message = String.format(errorTemplate, jobsCount);
        showAlert(message);
    }

    public void stopService() {
        progressBarThread.informToStop();
        loaderWebservice.terminateService();
    }

    public void showFrame() {
        jobFrame.setVisible(true);
        showJobListTable(true);
    }

    public int getJobsCount() {
        return loaderWebservice.getJobCnt();
    }

    public void setApp(App myApp) {
        app = myApp;
    }

    public StatusBar getStatusBar() {
        return statusBar;
    }

    public void refreshJobCount(int jobCnt) {
        log.debug(jobCnt);
        progressBarThread.setJobCnt(jobCnt);
        if (isSystemTrayApp && trayMessager != null) {
            trayMessager.setTrayTooltip(jobCnt);
        }
    }

    public int sendJobList(byte[] file) {
        final TreeNodeList treeNodeList = new TreeNodeList();
        XmlHandler jobListXml = new XmlHandler(new ByteArrayInputStream(file));
        Node treenodesNode = jobListXml.getDoc().getDocumentElement();

        schemaOwner = treenodesNode.getAttributes().getNamedItem("SCHEMA").getTextContent();
        if (schemaOwner != null) {
            log.trace("schemaOwner " + schemaOwner);
//            schemaOwner += ".";
        } else {
            schemaOwner = "";
        }
        String dbName = schemaOwner + ".STB$TREENODE$RECORD";
//        log.debug(dbName);

        NodeList treenodesNodeList = treenodesNode.getChildNodes();
        for (int i = 0; i < treenodesNodeList.getLength(); i++) {
            Node treenodeNode = treenodesNodeList.item(i);

            if (treenodeNode.getNodeType() == Node.ELEMENT_NODE) {

//                log.debug("nodes " + (i + 1) + " dbName " + dbName);
                StructMap nodeMap = new StructMap(dbName);

                NodeList treenodeNodeList = treenodeNode.getChildNodes();
                for (int j = 0; j < treenodeNodeList.getLength(); j++) {
                    Node node = treenodeNodeList.item(j);
                    if (node.getNodeType() == Node.ELEMENT_NODE) {
                        Element nodeElement = (Element) node;
                        String fieldName = nodeElement.getTagName();
                        if (nodeElement.getChildNodes().getLength() == 1
                                && !fieldName.equals("TLOPROPERTYLIST")
                                && !fieldName.equals("TLOVALUELIST")
                                && !fieldName.equals("CLDETAILS")) {
                            String fieldValue = nodeElement.getTextContent();
                            log.trace("node " + (j + 1) + " " + fieldName + " " + fieldValue);
                            nodeMap.put(fieldName, new DatabaseFieldMap(fieldName, "java.lang.String", Types.VARCHAR, 4000, "VARCHAR", fieldValue));
                        } else {
                            String dbRecordName = schemaOwner + ".";
                            String dbListName = schemaOwner + ".";
                            switch (fieldName) {
                                case "TLOPROPERTYLIST": {
                                    dbRecordName += "STB$PROPERTY$RECORD";
                                    dbListName += "STB$PROPERTY$LIST";
                                    ArrayMap data = getArrayList(node, dbRecordName, dbListName);
                                    nodeMap.put(fieldName, new DatabaseFieldMap(fieldName, "oracle.sql.ARRAY", Types.ARRAY, 0, dbListName, data));
                                    break;
                                }
                                case "TLOVALUELIST": {
                                    dbRecordName += "ISR$VALUE$RECORD";
                                    dbListName += "ISR$VALUE$LIST";
                                    ArrayMap data = getArrayList(node, dbRecordName, dbListName);
                                    nodeMap.put(fieldName, new DatabaseFieldMap(fieldName, "oracle.sql.ARRAY", Types.ARRAY, 0, dbListName, data));
                                    break;
                                }
                                default:
                                    // ignore value list (needed for tree) and cldetails (needed for grid)
                                    break;
                            }
                        }
                    }
                }

                TreeNodeRecord treeNodeRecord = new TreeNodeRecord(nodeMap);
                treeNodeList.add(treeNodeRecord);

            }
        }
//        log.debug(treeNodeList);
        SwingUtilities.invokeLater(new Runnable() {
            public void run() {
                createJobListTableModel(new MenuToDetail(treeNodeList, null));
                showJobListTable(false);
            }
        });
        int jobCnt = treeNodeList.getSize();
        log.debug("jobCnt " + jobCnt);
        return jobCnt;
    }


    private ArrayMap getArrayList(Node startNode, String dbRecordName, String dbListName) {
        ArrayList data = new ArrayList();
        NodeList nodelist = startNode.getChildNodes();

        if (nodelist != null) {
            for (int i = 0; i < nodelist.getLength(); i++) {
                Node record = nodelist.item(i);

                if (record.getNodeType() == Node.ELEMENT_NODE) {

                    log.trace("nodes " + (i + 1) + " dbRecordName " + dbRecordName);
                    StructMap nodeMap = new StructMap(dbRecordName);

                    NodeList recordNodes = record.getChildNodes();
                    for (int j = 0; j < recordNodes.getLength(); j++) {
                        Node node = recordNodes.item(j);
                        if (node.getNodeType() == Node.ELEMENT_NODE) {
                            Element nodeElement = (Element) node;
                            String fieldName = nodeElement.getTagName();
                            if (nodeElement.getChildNodes().getLength() == 1) {
                                String fieldValue = nodeElement.getTextContent();
                                log.trace("node " + (j + 1) + " " + fieldName + " " + fieldValue);
                                nodeMap.put(fieldName, new DatabaseFieldMap(fieldName, "java.lang.String", Types.VARCHAR, 4000, "VARCHAR", fieldValue));
                            }
                        }
                    }
                    data.add(nodeMap);
                }
            }
        }
        return new ArrayMap(data, dbListName);
    }

    public void showAlert(String msg) {
        showAlert(msg, null);
    }

    public void showAlert(String msg, String icon) {
        log.debug(msg);
        SwingUtilities.invokeLater(() -> {
            AlertGroup _alertGroup = new AlertGroup();

            Alert alert = new Alert();
            alert.getContentPane().setLayout(new BorderLayout());
            final JLabel label = new JLabel(msg, SwingConstants.CENTER);

            if (!msg.startsWith("<html>")) {
                FontMetrics metrics = label.getFontMetrics(label.getFont());
                int height = metrics.getHeight() * 2;
                int width = metrics.stringWidth(msg) + 16;

                label.setPreferredSize(new Dimension(width, height));
            }
            //ISRC-1225
            if (StringUtils.isNotEmpty(icon)) {
                label.setIcon(icons.getSmallIconByID(icon));
                label.setVerticalTextPosition(SwingConstants.TOP);
                label.setHorizontalTextPosition(SwingConstants.RIGHT);
            }
            alert.getContentPane().add(label);
            alert.setMovable(true);
            int timeout = loaderWebservice != null ? loaderWebservice.dispatcher.getDialogTimeout() : 3;
            alert.setTimeout(timeout * 1000); //milliseconds
            alert.setTransient(false);
            alert.setResizable(false);
            alert.setPopupBorder(BorderFactory.createLineBorder(new Color(10, 30, 106)));

            CustomAnimation showAnimation = new CustomAnimation(CustomAnimation.TYPE_ENTRANCE,
                    CustomAnimation.EFFECT_FLY, CustomAnimation.SMOOTHNESS_MEDIUM, CustomAnimation.SPEED_MEDIUM);
            showAnimation.setDirection(CustomAnimation.BOTTOM);
            showAnimation.setVisibleBounds(PortingUtils.getLocalScreenBounds());
            alert.setShowAnimation(showAnimation);

            CustomAnimation hideAnimation = new CustomAnimation(CustomAnimation.TYPE_ENTRANCE,
                    CustomAnimation.EFFECT_FLY, CustomAnimation.SMOOTHNESS_MEDIUM, CustomAnimation.SPEED_MEDIUM);
            hideAnimation.setDirection(CustomAnimation.BOTTOM);
            hideAnimation.setVisibleBounds(PortingUtils.getLocalScreenBounds());
            alert.setHideAnimation(hideAnimation);

            _alertGroup.add(alert);
            alert.showPopup(SwingConstants.SOUTH_EAST);
        });
    }

    private JTable createJobListTable() {

        jobListTable = new ListViewTable(new LoaderTableModelEmpty());

        ((HierarchicalTable) jobListTable).setHierarchicalColumn(-1);
        ((HierarchicalTable) jobListTable).setSingleExpansion(true);

        CellRendererManager.registerRenderer(com.utd.stb.inter.application.Node.class, new NodeTableRenderer(new IconSource()));
        CellRendererManager.registerRenderer(UserInput.class, new UserInputRenderer());

        jobListTable.setRowHeight(jobListTable.getFontMetrics(jobListTable.getFont()).getHeight() + 2);
        jobListTable.setSelectionMode(ListSelectionModel.SINGLE_SELECTION);

        showJobListTable(true);
        return jobListTable;
    }

    public void createTrayMessenger() {
        trayMessager = new TrayMessager();
        trayMessager.setLoaderGui(this);
        trayMessager.buildTrayIcon();
        int jobsCount = loaderWebservice.getJobCnt();
        trayMessager.setTrayTooltip(jobsCount);
    }


    public StatusBar createStatusBar(String port, String loaderDirectory) {
        log.debug("createStatusBar");
        statusBar = new StatusBar();
        final LabelStatusBarItem statusBarLabel = new LabelStatusBarItem();
//        String loaderDirectory = System.getProperty("java.loader.directory");
        String statusBarText = ((port == null) ? Res.getString(LOADER_STATUSBAR_PORT_ERROR_LABEL) : String.format(Res.getString(LOADER_STATUSBAR_PORT_LABEL), port))
                + ((loaderDirectory == null) ? Res.getString(LOADER_STATUSBAR_DIR_ERROR_LABEL) : String.format(Res.getString(LOADER_STATUSBAR_DIR_LABEL), loaderDirectory));
        statusBarLabel.setText(statusBarText);

        if (port == null || loaderDirectory == null) {
            statusBarLabel.setIcon(icons.getSmallIconByID(LOADER_WARNING_ICON));
        }

        statusBarProgress = new ProgressStatusBarItem();

        statusBarProgress.addMouseListener(new BarProgressMouseListener(statusBarProgress, String.format(Res.getString(LOADER_PROGRESS_TOOLTIP), 0)) {
            @Override
            public void mouseClicked(MouseEvent e) {
                boolean loaderTable = jobListTableModel instanceof LoaderTableModelNodes || jobListTableModel instanceof LoaderTableModelEmpty;
                log.debug("loaderTable ? " + loaderTable + " app != null? " + (app != null));
                if (loaderTable && app != null) {
                    log.info("Load job list from database");
                    DetailView detailView = app.getDetailJobView();
                    createJobListTableModel(detailView);
                } else {
                    log.info("Show job list from local variable");
                }
                showJobListTable(true);
                if (app != null) {
                    app.updateRibbonMenu();
                }
                super.mouseClicked(e);
            }

            @Override
            public void mouseEntered(MouseEvent e) {
                int jobsCount = progressBarThread.getJobCnt();
                log.debug("jobsCount = " + jobsCount);
                log.debug("startedJobs: " + loaderWebservice.startedJobs);
                setText(String.format(Res.getString(LOADER_PROGRESS_TOOLTIP), jobsCount));
                super.mouseEntered(e);
            }
        });

        progressBarThread = new LoaderProgressBarThread(statusBarProgress, 2000);
        progressBarThread.start();

        statusBar.add(statusBarLabel, JideBoxLayout.FLEXIBLE);
        statusBar.add(statusBarProgress, JideBoxLayout.FIX);
        statusBar.setBorder(new MatteBorder(1, 1 /*left*/, 0, 3 /*right*/, UIManager.getColor("ScrollPane.background")));

        return statusBar;
    }

    private void showJobListTable(boolean force) {
        log.debug("showJobListTable force? " + force);
        if (app != null) {
            jobListTable = app.getTable();
        }
        TableModel model = TableModelWrapperUtils.getActualTableModel(jobListTable.getModel());
        if (force || model instanceof LoaderTableModelNodes || model instanceof LoaderTableModelEmpty) {
            log.debug(schemaOwner);
            //todo
            /*if (StringUtils.isNotEmpty(schemaOwner)) {
                Dispatcher dispatcher = loaderWebservice.dispatcher;
                DetailView detailView = (DetailView) dispatcher.getMenuForJobs(schemaOwner);
                createJobListTableModel(detailView);
            }*/
            jobListTable.setModel(jobListTableModel);
            TableTools.autoSizeColumns(jobListTable, false);
        }
    }


    private void createJobListTableModel(DetailView detailView) {
        log.debug("begin");
        Nodes nodes = detailView.getNodes();
        if (nodes == null || nodes.getSize() == 0) {
            jobListTableModel = new LoaderTableModelEmpty();
        } else {
            UserInputs[] uis = new UserInputs[nodes.getSize()];
            for (int i = 0; i < uis.length; i++) {
                uis[i] = detailView.getNodeProperties(nodes.getNode(i));
            }
//            log.debug("Display node list " + nodes.toString());
            jobListTableModel = new LoaderTableModelNodes(nodes, uis);
        }
        log.debug("end");
    }


    public String updateRibbon(int timeout) {
        if (app != null) {
            log.debug("updateRibbon");
            app.updateRibbonMenu();
        } else {
            log.error(Res.getString(LOADER_APP_RIBBON_ERROR));
            return LOADER_APP_RIBBON_ERROR;
        }
        return "T";
    }

    public String updateTree(int timeout, String nodetype, String nodeid, String side) {
        if (app != null) {
            app.updateTree(nodetype, nodeid, side);
            /*if (nodetype.equals("updateTreeTask")) {
                app.updateTreeTask(timeout);
            } else {
                Thread thread = new Thread() {
                    @Override
                    public void run() {
                        TableModel model = TableModelWrapperUtils.getActualTableModel(app.getTable().getModel());
                        boolean loaderTable = model instanceof LoaderTableModelNodes || model instanceof LoaderTableModelEmpty;
                        if (side.equals("LEFT")
                                || side.equals("BOTH")
                                || side.equals("RIGHT") && !(loaderTable)) {
                            log.debug("updateTree: nodetype " + nodetype + " nodeid " + nodeid + " side " + side);
                            app.updateTree(nodetype, nodeid, (side.equals("BOTH") && loaderTable) ? "LEFT" : side);
                            log.debug("update from backend");
                        } else {
                            log.debug("updateTree: should not update right side when job list is opened");
                        }

                    }
                };
                thread.start();
                log.debug("end");
            }*/
        } else {
            log.error(String.format(Res.getString(LOADER_APP_TREE_ERROR), nodetype + " " + nodeid + " " + side));
            return LOADER_APP_TREE_ERROR;
        }
        return "T";
    }


    private void updateModel(NodesTableModel model, int jobid, String text, int progress, String iconId) {
        UserInputs[] uis = model.getUserInputs();
        int row = -1;
        for (UserInputs ui : uis) {
            row++;
            TextInputImpl textInputProgress = null;
            TextInputImpl textInputJobId = null;
            TextInputImpl textInputError = null;
            for (int i = 0; i < ui.getSize(); i++) {
                Object field = ui.get(i);
                String fieldName = null;
                if (field instanceof TextUserInput) {
                    fieldName = ((TextUserInput) field).getProperty().getParameterName();
                    log.trace(fieldName);
                }
                Object data = ui.get(i).getData();
                log.trace(data);
                if (data instanceof JProgressBar) {
                    progressBar = (JProgressBar) data;
                }
                if (data instanceof TextInputImpl) {
                    if (fieldName.equals(jobid_table_field)) {
                        textInputJobId = (TextInputImpl) data;
                    }
                    if (fieldName.equals(progress_table_field)) {
                        textInputProgress = (TextInputImpl) data;
                    }
                    if (fieldName.equals(error_table_field)) {
                        textInputError = (TextInputImpl) data;
                    }
                }
            }
            if (textInputJobId != null && String.valueOf(jobid).equals(textInputJobId.getText())) {
                if (progress != -1) {
                    if (textInputProgress != null) {
                        textInputProgress.setText(progress + " %");
                    }
                    if (progressBar != null) {
                        SwingUtilities.invokeLater(new Runnable() {

                            public void run() {
                                progressBar.setValue(progress);
                            }
                        });
                        if (text != null) {
                            progressBar.setString(text);
                        }
                    }
                } else {
                    if (textInputError != null) {
                        textInputError.setText(text);
                    }
                }

                com.utd.stb.inter.application.Node currentNode = (com.utd.stb.inter.application.Node) model.getValueAt(row, 0);
                currentNode.setIconId(iconId);
                model.setValueAt(ui, row, 0);
                break;
            }
        }
        SwingUtilities.invokeLater(new Runnable() {
            public void run() {
//                log.debug("fireTableDataChanged");
                model.fireTableDataChanged();
            }
        });
    }

    public String showJobProgress(int jobid, String text, int progress, String iconId) {
        log.debug(iconId);
//        int jobsCount = loaderWebservice.getJobCnt();
//        progressBarThread.setJobCnt(jobsCount);

        try {
            LoaderTableModelNodes model1;
            NodesTableModel model2;
            if (jobListTableModel instanceof LoaderTableModelEmpty) {
                model1 = null;
            } else {
                model1 = (LoaderTableModelNodes) jobListTableModel;
            }
//            log.debug("model1 = " + model1);
            if (model1 != null) {
                log.debug("Update user job model");
                updateModel(model1, jobid, text, progress, iconId);
                jobListTableModel = model1;
            }

            // check if there is another table with jobs displayed (jobs, scheduled jobs)
            if (app != null) {
                JTable displayedTable = app.getTable();
                TableModel displayedTableModel = TableModelWrapperUtils.getActualTableModel(displayedTable.getModel());
                if (displayedTableModel instanceof NodesTableModel) {
                    log.debug("Update nodes table model");
                    model2 = (NodesTableModel) displayedTableModel;
                    updateModel(model2, jobid, text, progress, iconId);
                }
            }

        } catch (Exception e) {
            log.error(e.getMessage());
            return e.getMessage();
        }
        return "T";
    }

    public void showJobError(final int jobid, final String message) {
        if (isSystemTrayApp) {
            showJobErrorInTray(jobid);
        }

        if (!loaderWebservice.isStandalone() || jobFrame != null && jobFrame.isVisible()) {
            SwingUtilities.invokeLater(new Runnable() {

                public void run() {
                    JOptionPane oPane = new JOptionPane(message, JOptionPane.ERROR_MESSAGE);
                    JDialog dialog = oPane.createDialog(jobFrame, String.format(Res.getString(LOADER_TEXT_ERROR), jobid));
                    dialog.setVisible(true);
                    dialog.toFront();
                    dialog.requestFocusInWindow();
                }
            });
        }
    }

    protected void showJobErrorInTray(int jobid) {
        if (trayMessager != null) {
            String messageTemplate = Res.getString(LOADER_TEXT_ERROR);
            String errorMessage = String.format(messageTemplate, jobid);
            trayMessager.showTrayMessage("", errorMessage, TrayIcon.MessageType.ERROR);
        }
    }

    protected void showStartNewJobInTray(int jobid) {
        if (trayMessager != null) {
            String messageTemplate = Res.getString(LOADER_TEXT_START);
            String message = String.format(messageTemplate, jobid);
            trayMessager.showTrayMessage("", message, TrayIcon.MessageType.INFO);
        }
    }

    protected void showTerminateSuccessInTray(int jobid) {
        if (trayMessager != null) {
            log.debug("showTerminateSuccessInTray: " + LOADER_TEXT_TERMINATE);
            String messageTemplate = Res.getString(LOADER_TEXT_TERMINATE);
            String message = String.format(messageTemplate, jobid);
            trayMessager.showTrayMessage("", message, TrayIcon.MessageType.INFO);
        }
    }

    public void showTerminateSuccess(int jobid) {
        if (isSystemTrayApp) {
            showTerminateSuccessInTray(jobid);
        }
        log.debug("showAlert: " + Res.getString(LOADER_TEXT_TERMINATE));
        showAlert(String.format(Res.getString(LOADER_TEXT_TERMINATE), jobid));
    }

    public void showTerminateWarn(int jobid) {
        if (isSystemTrayApp) {
            showTerminateSuccessInTray(jobid);
        }
        log.debug("showAlert: " + String.format(Res.getString(LOADER_TEXT_WARN), jobid));
        showAlert(String.format(Res.getString(LOADER_TEXT_WARN), jobid), LOADER_WARNING_ICON);
    }


    public MessageStackException showMessageObject(String messageObject) {

        byte[] xmlBytes = Base64.decodeBase64(messageObject);
        XmlHandler xml = new XmlHandler(xmlBytes);
        String message = xml.nodeRead("//SERRORMESSAGE");
        MessageStackException messageStackException = new MessageStackException(xmlBytes, message);
        int severity = messageStackException.getSeverity();

        if (severity == ErrorRecord.cnMsgWarningTimeout) {
            showAlert(messageStackException.getMessage());
        } else if (severity != ErrorRecord.cnNoError) {
            int dialogShowTime = getDialogShowTimeBySeverity(severity);
            SwingUtilities.invokeLater(() -> ExceptionDialog.showExceptionDialog(null, messageStackException, null, false, dialogShowTime));
        }
        return messageStackException;
    }


    // delay in seconds
    private int getDialogShowTimeBySeverity(int severity) {
        switch (severity) {
            case ErrorRecord.cnSeverityCritical:
                return 180;
            case ErrorRecord.cnSeverityWarning:
                return 60;
            case ErrorRecord.cnSeverityMessage:
                return 30;
            case ErrorRecord.cnMsgWarningTimeout:
                return 30;
        }
        return -1;
    }


    public void showStartNewJob(int jobid) {
        if (isSystemTrayApp) {
            showStartNewJobInTray(jobid);
        }
        if (loaderWebservice.isStandalone()) { // only if standalone, otherwise the error message is displayed as start info
            showAlert(String.format(Res.getString(LOADER_TEXT_START), jobid));
        }
    }

    public void setSystemTrayApp(boolean isSystemTrayApp) {
        this.isSystemTrayApp = isSystemTrayApp;
    }

    public void setLoaderWebservice(LoaderWebserviceImpl loaderWebservice) {
        this.loaderWebservice = loaderWebservice;
    }

    public void onAppExit() {
        if (!isSystemTrayApp) {
            showFrame();
        } else {
            createTrayMessenger();
        }
    }


}
