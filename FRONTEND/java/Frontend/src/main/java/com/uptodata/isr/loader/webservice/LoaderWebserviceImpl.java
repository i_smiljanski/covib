/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.uptodata.isr.loader.webservice;

import com.uptodata.isr.loader.webservice.cells.CompareExcelDocumentOID;
import com.uptodata.isr.loader.webservice.word.CompareDocumentOID;
import com.uptodata.isr.loader.webservice.word.DocumentProtecting;
import com.uptodata.isr.observer.transfer.DbFileSupplier;
import com.uptodata.isr.observers.base.dbAccess.DbData;
import com.uptodata.isr.server.utils.fileSystem.ClassLoaderUtil;
import com.uptodata.isr.server.utils.fileSystem.Constants;
import com.uptodata.isr.server.utils.network.UrlUtil;
import com.uptodata.isr.server.utils.process.ProcessHelper;
import com.uptodata.isr.server.utils.security.ProtectionUtil;
import com.uptodata.isr.utils.ConvertUtils;
import com.uptodata.isr.utils.exceptions.MessageStackException;
import com.uptodata.isr.utils.security.SymmetricProtection;
import com.uptodata.isr.utils.xml.dom.XmlHandler;
import com.utd.stb.back.database.DBInterface;
import com.utd.stb.back.database.Dispatcher;
import com.utd.stb.gui.loader.resources.Res;
import com.utd.stb.gui.swingApp.exceptions.ExceptionDialog;
import com.utd.stb.gui.swingApp.login.DatabaseInfo;
import com.utd.stb.inter.application.BackendException;
import org.apache.commons.io.FileUtils;
import org.apache.commons.lang.StringUtils;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;
import org.zeroturnaround.zip.ZipUtil;

import javax.crypto.BadPaddingException;
import javax.crypto.IllegalBlockSizeException;
import javax.crypto.NoSuchPaddingException;
import javax.jws.WebMethod;
import javax.jws.WebService;
import javax.jws.soap.SOAPBinding;
import javax.swing.*;
import javax.xml.ws.Endpoint;
import java.io.*;
import java.lang.reflect.InvocationTargetException;
import java.net.BindException;
import java.net.MalformedURLException;
import java.security.InvalidAlgorithmParameterException;
import java.security.InvalidKeyException;
import java.security.NoSuchAlgorithmException;
import java.security.spec.InvalidKeySpecException;
import java.security.spec.InvalidParameterSpecException;
import java.sql.Connection;
import java.sql.Driver;
import java.sql.DriverManager;
import java.sql.SQLException;
import java.util.*;
import java.util.concurrent.TimeUnit;
import java.util.regex.Pattern;

/**
 * @author dortm19
 */
//@com.sun.xml.ws.developer.servlet.HttpSessionScope
@WebService(name = "LoaderWebserviceImpl", targetNamespace = "http://ws.rendering.isr.uptodata.com/")
@SOAPBinding(style = SOAPBinding.Style.RPC)
public class LoaderWebserviceImpl {
    private LoaderGui loaderGui;
    public static Logger log = LogManager.getRootLogger();
    private Connection connection;

    private static Endpoint endpoint;

    static final String LOADER_EMPTY_TABLE = "Loader.empty.table";
    private static final String LOADER_APP_EXECUTE_ERROR = "Loader.app.execute.error";
    private static final String LOADER_FILE_NULL_ERROR = "Loader.file.null.error";
    private static final String LOADER_FILE_DISPLAY_ERROR = "Loader.file.display.error";
    private static final String LOADER_FILE_TO_DB_ERROR = "Loader.file.to.db.error";
    private static final String LOADER_CONFIG_FILE = "config.xml";
    private static final String USER_CANCELLED = "USER_CANCELLED";
    private static final String PROCESS_ERROR = "PROCESS_ERROR";
    private static final String PROCESS_TIMEOUT = "PROCESS_TIMEOUT";
    private static final String PROCESS_SPAWN_ERROR = "PROCESS_SPAWN_ERROR";
    private static final String PROCESS_READY = "PROCESS_READY";
    private static final String dbDriver = "oracle.jdbc.driver.OracleDriver";

    static final String LOADER_JOB_ERROR = "gear_error";
    static final String LOADER_JOB_START = "gear_run";
    static final String LOADER_JOB_END = "gear_stop";
    static final String LOADER_JOB_END_WARN = "gear_warn";
    HashMap<Integer, String> icons = new HashMap<>();

    private static Pattern pattern1 = Pattern.compile("\\[ISR.TEMP.PATH]");
    private String port;
    private int jobCnt;
    private boolean standalone = false;
    private String dbConnLoader;
    private String dbName;
    Hashtable<Integer, LoaderJob> startedJobs = new Hashtable<>();
    private boolean repeated = false;
    private String baseDirectoryPart;
    Dispatcher dispatcher;
    File loaderDirectory;

    public String wsUrl;

    //    it must be protected because of RenderingServer!
    protected DbData dbData;

    // Webservice needs empty constructor
//    it must be protected because of RenderingServer!
    public LoaderWebserviceImpl() {

    }

    public LoaderWebserviceImpl(boolean standalone) {
        this.standalone = standalone;
    }

    //    it must be protected because of RenderingServer!
    protected LoaderWebserviceImpl(boolean standalone, String dbConnLoader, String dbName) throws Exception {
        this(standalone);
        this.dbConnLoader = dbConnLoader;
        this.dbName = dbName;
    }

    /*public void setLogLevel(String loglevel) {
        log.setLevel(Level.toLevel(loglevel));
    }*/

    public void init(String portRange, String context, String directory, String host) throws Exception {
        log.debug("Try to start Loader webservice on host {} with port range {} in context {}", host, portRange, context);

        try {
            createEnvironment(directory);
        } catch (IOException e) {
            log.error("Error by creating of environment with parameter  " + directory + ". Exception: " + e);
            initException(e);
        }
        try {
            endpoint = startServer(portRange, context, host);
        } catch (Exception e) {
            log.error("Error by start of server with parameters: portRange  " + portRange + ", context " + context + ". Exception: " + e);
            initException(e);
        }
        log.info("startServer: " + "On server " + port + " webservice " + endpoint.getImplementor().getClass().getName() + " started");
        log.debug("standalone = " + standalone + " dbConnLoader = " + dbConnLoader);
        String userName = System.getProperty("user.name");
        try {
            if (standalone && (dbConnLoader != null && !dbConnLoader.isEmpty())) {
                dbSession();

                DBInterface db = new DBInterface(connection);
                db.checkIsValidOwner();
                try {
                    dispatcher = new Dispatcher(db);
                    dispatcher.setLoaderConnection(port, userName, true);
                } catch (BackendException e) {
                    log.error("Db dispatcher could not be created. Error: " + e);
                    ExceptionDialog.showExceptionDialog(null, e, null);
                    if (e.getSeverity() == BackendException.SEVERITY_ACTION_EXIT) {
                        System.exit(1);
                    }
                }
            }
        } catch (Exception e) {
            log.error("Error by creating of db connection with parameters name  '" + userName + "', port " + port + ". Exception: " + e);
            initException(e);
        }
    }

    private void initException(Exception e) throws Exception {
        if (e instanceof BindException) {
            log.error(e.getMessage());
        }
        if (standalone) {
            if (connection != null) {
                connection.close();
            }
            System.exit(-1);
        } else {
            throw e;
        }
    }

    private void dbSession() throws IOException, SQLException, ClassNotFoundException, IllegalAccessException, InstantiationException, NoSuchAlgorithmException, InvalidKeyException, InvalidAlgorithmParameterException, NoSuchPaddingException, BadPaddingException, InvalidParameterSpecException, InvalidKeySpecException, IllegalBlockSizeException {
        DatabaseInfo[] databases = DatabaseInfo.readDataBases(new XmlHandler(new FileInputStream(dbConnLoader)));
        DatabaseInfo databaseInfo = getDataBaseByName(dbName, databases);
        if (databaseInfo == null) {
            throw new RuntimeException("In file " + dbConnLoader + " was not found database with alias " + dbName);
        }
        log.debug("Try to connect to " + databaseInfo);
        DriverManager.registerDriver((Driver) (Class.forName(dbDriver).newInstance()));
        String decrPwd = SymmetricProtection.decrypt256(databaseInfo.getdbPassword());
        if (StringUtils.isEmpty(decrPwd)) {
            decrPwd = SymmetricProtection.decrypt(databaseInfo.getdbPassword());
        }
        connection = DriverManager.getConnection("jdbc:oracle:thin:@" + databaseInfo.getFullName(),
                databaseInfo.getdbUser(), decrPwd);
        connection.setAutoCommit(false);
    }


    private DatabaseInfo getDataBaseByName(String dbName, DatabaseInfo[] databases) {
        for (DatabaseInfo databaseInfo : databases) {
            if (databaseInfo.getAlias().equalsIgnoreCase(dbName)) {
                return databaseInfo;
            }
        }
        return null;
    }

    @WebMethod
    public boolean ping() {
        return true;
    }

    @WebMethod
    public String sendJobList(byte[] file) {
        log.debug("sendJobList: standalone " + standalone);

        if (loaderGui != null) {
            loaderGui.sendJobList(file);
        }
        return "T";
    }

    @WebMethod
    public synchronized String startJob(final int jobid, String foldername, boolean reactivate, int jobCnt) throws MessageStackException {
//        jobCnt++;
        this.jobCnt = jobCnt;
        log.info("startJob: jobid " + jobid + ", running jobs count:" + this.jobCnt + " foldername: " + foldername);

        File jobDirectory = new File(System.getProperty("java.loader.directory"), foldername);

        if (jobDirectory.exists() && jobDirectory.isFile()) {
            String userErrorMessage = "Can not create job directory, the proposed name exists as file: " + jobDirectory.getAbsolutePath();
            String implStack = userErrorMessage + ", " + MessageStackException.getCurrentMethod();

            MessageStackException messageStackException = new MessageStackException(
                    MessageStackException.SEVERITY_CRITICAL, userErrorMessage, implStack, implStack);
            throw messageStackException;
        } else if (jobDirectory.exists() && jobDirectory.isDirectory()) {
            log.debug("Job directory exists already: " + jobDirectory.getAbsolutePath());
        } else {
            if (!jobDirectory.mkdirs()) {
                String userErrorMessage = "Could not create job directory: " + jobDirectory.getAbsolutePath();
                String implStack = userErrorMessage + ", " + MessageStackException.getCurrentMethod();

                MessageStackException messageStackException = new MessageStackException(
                        MessageStackException.SEVERITY_CRITICAL, userErrorMessage, implStack, implStack);
                log.error(implStack);
                throw messageStackException;
            }
            log.debug("Created job directory: " + jobDirectory.getAbsolutePath());
        }

        startedJobs.put(jobid, new LoaderJob(jobid, jobDirectory));
        if (loaderGui != null) {
            loaderGui.refreshJobCount(jobCnt);
            if (!reactivate) {
                loaderGui.showStartNewJob(jobid);
                showJobProgress(jobid, 0);
            }
        }
        return "T";
    }

    /* returns if the job is obsolete */
    private boolean terminateJobProcess(int jobid) {
        LoaderJob job = startedJobs.get(jobid);
        log.debug(jobid + ";  " + job.isRunning() + ";  " + jobCnt);
        if (job != null && job.isRunning()) {
            job.setRunning(false);
            if (jobCnt > 0) {
                jobCnt--;
            }
            if (!standalone && loaderGui != null) {
                loaderGui.refreshJobCount(jobCnt);
            }
            return false;
        }
        return true;
    }


    @WebMethod
    public synchronized String terminateJob(int jobid, int jobCnt) {
        log.info("terminateJob: jobid " + jobid + " running jobs " + jobCnt);
        this.jobCnt = jobCnt;
        boolean obsoleteJob = terminateJobProcess(jobid);
        log.debug(obsoleteJob + " loaderGui null? " + loaderGui == null);
        return "T";
    }

    private void actionAfterTerminateJob(String className, String methodName, int jobid, String message) {
        actionAfterTerminateJob(className, methodName, jobid, message, null);
    }

    private void actionAfterTerminateJob(String className, String methodName, int jobid, String message, String filePath) {
        log.info("begin");
        Class clazz;
        try {
            clazz = ClassLoaderUtil.loadClassAtRuntime(className,
                    ClassLoaderUtil.getClassLoaderSingle(Constants.LIB_PATH));
        } catch (ClassNotFoundException | MalformedURLException e) {
            log.info("end: Class '" + className + "' not found");
            return;
        }

        SwingUtilities.invokeLater(() -> {
            HashMap<String, String> map = new HashMap<>();
            map.put("jobid", String.valueOf(jobid));
            map.put("message", message);
            String path = filePath;
            if (StringUtils.isEmpty(path) && dispatcher != null) {
                try {
                    path = dispatcher.getJobParameter("USER_PATH", jobid);
                } catch (BackendException e) {
                    log.info(e);
                }
            }

            map.put("path", path);
            try {
                Object ret = ClassLoaderUtil.invoke(clazz, methodName, new Class[]{HashMap.class}, new Object[]{map});
                log.debug(ret);
            } catch (RuntimeException | IllegalAccessException | InvocationTargetException | InstantiationException | NoSuchMethodException e) {
                log.info("end with error: " + e);
            }
        });

        log.info("end");

    }


    @WebMethod
    public synchronized String showTerminateSuccess(int jobid) {
        log.info("showTerminateSuccess: jobid " + jobid);
        if (loaderGui != null) {
            icons.put(jobid, LOADER_JOB_END);
            log.debug(icons);
            showJobProgress(jobid, 100);
            loaderGui.showTerminateSuccess(jobid);
        }
        actionAfterTerminateJob("com.uptodata.isr.loader.addaction.Main", "doAction", jobid, "successful");
        return "T";
    }


    @WebMethod
    public String showJobError(final int jobid, final String message) {
        log.error("showJobError: jobid " + jobid + " text " + message);
        icons.put(jobid, LOADER_JOB_ERROR);
        showJobProgress(jobid, message, -1);
        if (loaderGui != null) {
            loaderGui.showJobError(jobid, message);
        }
        return "T";
    }


    @WebMethod
    public String showErrorMessage(final int jobid, final String messageObject) {
        log.debug("showMessage: jobid " + jobid);
        MessageStackException messageStackException = loaderGui.showMessageObject(messageObject);
        String message = messageStackException.getMessage();
        icons.put(jobid, LOADER_JOB_ERROR);
        showJobProgress(jobid, message, -1);
        actionAfterTerminateJob("com.uptodata.isr.loader.addaction.Main", "doAction", jobid, messageObject);
        return "T";
    }


    @WebMethod
    public String showTerminateWarning(final int jobid) {
        log.debug("showMessage: jobid " + jobid);
        if (loaderGui != null) {
            icons.put(jobid, LOADER_JOB_END_WARN);
            showJobProgress(jobid, 100);
            loaderGui.showTerminateWarn(jobid);
        }
        actionAfterTerminateJob("com.uptodata.isr.loader.addaction.Main", "doAction", jobid, "warning");
        return "T";
    }


    private void showJobProgress(int jobid, int progress) {
        showJobProgress(jobid, null, progress);
    }


    @WebMethod
    public String showJobProgress(int jobid, String text, int progress) {
        if (loaderGui != null) {
            log.debug("showJobProgress: jobid " + jobid + "; text: " + text);
            if (StringUtils.isEmpty(icons.get(jobid))) {
                log.debug("progress = " + progress);
                switch (progress) {
                    case -1:
                        icons.put(jobid, LOADER_JOB_ERROR);
                        break;
                    case 100:
                        icons.put(jobid, LOADER_JOB_END);
                        break;
                    default:
                        icons.put(jobid, LOADER_JOB_START);
                        break;
                }
            }
            loaderGui.showJobProgress(jobid, text, progress, icons.get(jobid));
        }
        return "T";
    }


    @WebMethod
    public void saveDbToken(byte[] tokenFile, byte[] keyFile) throws MessageStackException {
        log.debug("begin");
        ByteArrayOutputStream bos = new ByteArrayOutputStream();
        Properties connProps = new Properties();
        try {
            //save key file in tmp-directory
            File util = ConvertUtils.bytesToFile(keyFile, System.getProperty("java.loader.directory") + Constants.fileSeparator + Constants.JAR_FILE1);
            //!TEST [ISRC-815]!
            //File util =  ConvertUtils.bytesToFile(keyFile,  "D:\\Temp\\TEST" + Constants.fileSeparator +  Constants.JAR_FILE1);
            log.debug(util);
            ProtectionUtil.decrypt(new ByteArrayInputStream(tokenFile), bos, Constants.KEY_CLASS, Constants.JAR_FILE1,
                    System.getProperty("java.loader.directory"));
            connProps.load(new InputStreamReader(new ByteArrayInputStream(bos.toByteArray())));
            dbData = new DbData(connProps);
            log.debug(dbData);
            boolean isDeleted = util.delete();
            log.debug("key file deleted? " + isDeleted);
        } catch (Exception e) {
            throw onError(e, e.getMessage(), MessageStackException.getCurrentMethod());
        }
        log.debug("end");
    }


    private String getXPathValue(XmlHandler configXml, String xPath) {
        String nodeValue = "";
        try {
            nodeValue = configXml.selectNodes(xPath).item(0).getTextContent();
        } catch (Exception ignored) {
            String error = Arrays.toString(ignored.getStackTrace());
            log.debug(error);
        }
        return nodeValue;
    }

    private String displayDocument(String path, String document) throws MessageStackException {
        log.debug("displayDocument: path =" + path + "; document =" + document);
        try {
            String out = ProcessHelper.displayDocument(path, document);
            log.debug(out);
            return "T";
        } catch (Exception e) {
            throw onError(e, String.format(Res.getString(LOADER_FILE_DISPLAY_ERROR), "'" + path + document + "'"),
                    MessageStackException.getCurrentMethod());
        }
    }


    private String waitForProcess(Process p, long timeout, File moduleErrorFile) throws Exception {

        log.debug("waitForProcess timeout " + timeout);
        int exitVal = -1;
        if (!p.waitFor(timeout, TimeUnit.MINUTES)) {
            p.destroy();
            log.error(Res.getString(PROCESS_TIMEOUT));
            return PROCESS_TIMEOUT;
        } else {
            exitVal = p.exitValue();
        }
        String error = getProcessError(moduleErrorFile);
        if (StringUtils.isNotEmpty(error)) {
            throw new Exception(error);
        }
        if (exitVal == 0) {
            return "T";
        }
        return "F";

    }


    private String getProcessError(File moduleErrorFile) {
        String error = null;

        if (moduleErrorFile != null && moduleErrorFile.canRead()) {
            try {
                error = error + "\n" + FileUtils.readFileToString(moduleErrorFile);
//                            log.error(error);
            } catch (IOException e) {
                log.warn(e);
            }
        }
        return error;
    }


    @WebMethod
    public String startApp(int jobid, int timeout) throws MessageStackException {
        log.info("begin");
        LoaderJob job = startedJobs.get(jobid);
        File jobDirectory = job.getJobDirectory();

        String configFileName = "";

        try {
            configFileName = jobDirectory.getAbsolutePath() + System.getProperty("file.separator") + LOADER_CONFIG_FILE;
            XmlHandler configXml = new XmlHandler(new FileInputStream(configFileName));
            NodeList actions = configXml.selectNodes("//app");
            log.debug("startApp: jobid " + jobid + " have to do " + actions.getLength() + " actions");
            for (int i = 0; i < actions.getLength(); i++) {
                Node action = actions.item(i);
                Element actionElement = (Element) action;
                boolean synchronous = "true".equalsIgnoreCase(configXml.getTextContentByTagName(actionElement, "wait_for_process"));
                String application = actionElement.getAttribute("name");
                String command = configXml.getTextContentByTagName(actionElement, "command");
                String order = actionElement.getAttribute("order");

                log.debug("startApp: jobid " + jobid + " execute action " + application + " " + command + ", synchronous " + synchronous);

                Process process;
                ProcessBuilder processBuilder;
                File moduleErrorFile = FileUtils.getFile(jobDirectory, application + ".error");
                switch (application) {
                    case "ME":
                        String fullpathname = configXml.nodeRead("//fullpathname");
                        log.debug(fullpathname + "; jobDirectory = " + jobDirectory);
                        fullpathname = pattern1.matcher(fullpathname).replaceAll((jobDirectory + System.getProperty("file.separator")).replaceAll("\\\\", "\\\\\\\\"));
                        log.debug(fullpathname);
                        String target = getXPathValue(configXml, "config/apps/target");//getNodeValue(configDocRoot, "target");
                        log.debug(target);

                        String returnValue = "";
                        if (command.equalsIgnoreCase("SAVE")) {
                            NodeList sources = actionElement.getElementsByTagName("source");
                            boolean overwrite = "T".equalsIgnoreCase(configXml.nodeRead("//overwrite"));
                            log.debug(sources.getLength() + " " + overwrite);
                            returnValue = FileSystemWorker.copyFiles(jobDirectory, fullpathname, target, overwrite, sources);
                            log.debug(returnValue);
                        } else if (command.equalsIgnoreCase("SAVE_TO_PATH")) {
                            NodeList sources = actionElement.getElementsByTagName("source");
                            boolean overwrite = "T".equalsIgnoreCase(configXml.nodeRead("//overwrite"));
                            log.debug(sources.getLength() + " " + overwrite);
                            List<String> retList = FileSystemWorker.copyFilesToPath(jobDirectory, fullpathname, overwrite, sources);
                            returnValue = retList.get(0);
                            log.debug(retList);
                            FileSystemWorker.createAndSaveHashFile(jobDirectory, retList.get(1));
                        } else if (command.equalsIgnoreCase("DELETE")) {
                            NodeList sources = actionElement.getElementsByTagName("source");
                            returnValue = FileSystemWorker.deleteFiles(fullpathname, sources);
                        } else if (command.equalsIgnoreCase("DISPLAY")) {
                            returnValue = displayDocument(fullpathname, target);
                        } else if (command.equalsIgnoreCase("OPEN_FOLDER")) {
                            returnValue = displayDocument(fullpathname, "");
                        } else if (command.equalsIgnoreCase("CHECK_EXCEL_OID")) {
                            String docOid = configXml.getTextContentByTagName(actionElement, "DOC_OID");
                            target = configXml.getTextContentByTagName(actionElement, "target");
                            if (StringUtils.isEmpty(fullpathname)) {
                                fullpathname = jobDirectory.getAbsolutePath() + System.getProperty("file.separator");
                            }
                            returnValue = CompareExcelDocumentOID.compareDocumentOID(docOid, fullpathname + target);
                        }
                        if (!returnValue.equals("T")) {
                            throw onError(returnValue, MessageStackException.getCurrentMethod());
                        }
                        break;
                    case "APPL":

                        String processResult = "";
//                    AppExecuter appExecuter = new AppExecuter();
                        try {
                            ArrayList<String> parameterList = getParameterList(actionElement.getElementsByTagName("parameter"), jobDirectory);
                            process = AppExecuter.executeApplication(jobDirectory,
                                    configXml.getTextContentByTagName(actionElement, "executable"),
                                    getXPathValue(configXml, "config/apps/applicationpathes"),
                                    parameterList,
                                    configXml);
                        } catch (Exception e) {
                            throw onError(e, Res.getString(PROCESS_SPAWN_ERROR) + " " + processResult, MessageStackException.getCurrentMethod());
                        }

                        if (process == null && AppExecuter.isCancelled()) {
                            throw onError(USER_CANCELLED, MessageStackException.getCurrentMethod());
                        }
                        if (process == null) {
                            String userErrorMessage = Res.getString(PROCESS_SPAWN_ERROR) + " " + processResult;
                            throw onError(userErrorMessage, MessageStackException.getCurrentMethod());
                        }

                        if (synchronous) {
                            processResult = waitForProcess(process, timeout, moduleErrorFile);
                            log.debug("startApp: jobid " + jobid + " processResult " + processResult);
                            if (!processResult.equalsIgnoreCase("T")) {
                                throw onError(processResult, MessageStackException.getCurrentMethod());
                            }
                        }
                        break;
                    default:
                        try {
                            String orgFullpathname = getXPathValue(configXml, "config/apps/fullpathname");
                            fullpathname = orgFullpathname;
                            fullpathname = pattern1.matcher(fullpathname).replaceAll((jobDirectory + System.getProperty("file.separator")).replaceAll("\\\\", "\\\\\\\\"));
                            log.debug("fullpathname=" + fullpathname);
                            if (fullpathname != null && !fullpathname.equals(orgFullpathname)) {
                                Node appsNode = configXml.getNode("//apps");
                                try {
                                    Node fullpathnameNode = configXml.getNode("//fullpathname");
                                    log.debug(fullpathnameNode);
                                    appsNode.removeChild(fullpathnameNode);
                                    configXml.createElement("fullpathname", fullpathname, null, null, appsNode);

                                    FileOutputStream oStream = new FileOutputStream(jobDirectory.getAbsolutePath() + System.getProperty("file.separator") + LOADER_CONFIG_FILE);
                                    configXml.save(oStream);
//                                    log.debug(configXml.xmlToString());
                                    oStream.close();
                                } catch (Exception ignored) {// node not in xml so we don't need to delete it

                                }
                            }
                        } catch (Exception ignored) {// if we can't save the new path list the user can enter it again when he needs it

                        }

                        String commandLine = jobDirectory + System.getProperty("file.separator")
                                + configXml.getTextContentByTagName(actionElement, "executable");

                        try {
                            log.debug("startApp: jobid " + jobid + " commandLine " + commandLine + " " + LOADER_CONFIG_FILE + " " + order);
                            processBuilder = new ProcessBuilder(commandLine, LOADER_CONFIG_FILE, order);
                            process = processBuilder.start();
                        } catch (Exception e) {
                            throw onError(e, Res.getString(PROCESS_SPAWN_ERROR) + " " + e.getMessage(), MessageStackException.getCurrentMethod());
                        }

                        if (synchronous) {

                            processResult = waitForProcess(process, timeout, moduleErrorFile);
                            log.debug("startApp: jobid " + jobid + " processResult " + processResult);

                            if (!repeated && processResult.equals(PROCESS_READY + -3)) {
                                repeated = true;
                                process = Runtime.getRuntime().exec(commandLine);
                                processResult = waitForProcess(process, timeout, moduleErrorFile);
                                if (!processResult.equalsIgnoreCase("T")) {
                                    throw onError(processResult, MessageStackException.getCurrentMethod());
                                }
                            } else {
                                if (!processResult.equalsIgnoreCase("T")) {
                                    throw onError(processResult, MessageStackException.getCurrentMethod());
                                }
                            }
                        }
                }
            }
        } catch (Exception ex) {
            log.debug(ex.getMessage());
            throw onError(ex, String.format(Res.getString(LOADER_APP_EXECUTE_ERROR),
                    configFileName) + " " + ex.getMessage(), MessageStackException.getCurrentMethod());
        }
        return "T";
    }

    @WebMethod(exclude = true)
    public String loadFileToClient(File targetDir, String filename, String configName, String logLevel, String logReference) throws MessageStackException {

        log.debug("loadFileToClient: filename " + filename);

        DbFileSupplier fileWorker = new DbFileSupplier(targetDir, dbData);
        String transferKeyId = filename;
        return downloadFile(targetDir, filename, configName, logLevel, logReference, fileWorker, transferKeyId);
    }


    @WebMethod(exclude = true)
    private String downloadFile(File targetDir, String filename, String configName, String logLevel, String logReference, DbFileSupplier fileWorker, String transferKeyId) throws MessageStackException {
        try {
            File loadedFile = fileWorker.downloadFile(filename, targetDir.getAbsolutePath(), configName, transferKeyId, logLevel, logReference);
            if (loadedFile != null) {
                log.info("File " + filename + " downloaded successfully");
            }
            return "T";
        } catch (Exception e) {
            throw onError(e, String.format(Res.getString(LOADER_FILE_TO_DB_ERROR), filename) + " " + e.getMessage(), MessageStackException.getCurrentMethod());
        }
    }


    @WebMethod
    public String loadFileToLoader(int jobid, String filename, String configName, String docType,
                                   String logLevel, String logReference) throws MessageStackException {

        log.debug("loadFileToLoader: jobid " + jobid + " filename " + filename);

        LoaderJob job = startedJobs.get(jobid);
        File jobDirectory = job.getJobDirectory();
        log.debug(jobDirectory.getAbsolutePath());
        DbFileSupplier fileWorker = new DbFileSupplier(jobDirectory, dbData);
        String transferKeyId = jobid + ";" + filename + (StringUtils.isNotEmpty(docType) ? ";" + docType : "");
        return downloadFile(jobDirectory, filename, configName, logLevel, logReference, fileWorker, transferKeyId);
    }


    @WebMethod
    public String uploadLoaderDir(int jobId, String configName, String logLevel, String logReference) throws MessageStackException {
        log.debug("uploadLoaderDir: jobid = " + jobId + " configName = " + configName);

        LoaderJob job = startedJobs.get(jobId);
        File jobDirectory = job.getJobDirectory();
        File[] allFiles = jobDirectory.listFiles();
        File zipLoader = FileUtils.getFile(jobDirectory, "loader.zip");
        ZipUtil.packEntries(allFiles, zipLoader);
        String ret = loadFileFromLoader(jobId, zipLoader.getName(), configName, "LOADER_DIR", logLevel, logReference);
        log.debug("end " + ret);
        return ret;
    }


    @WebMethod
    public String loadFileFromLoader(int jobId, String filename, String configName, String docType, String
            logLevel, String logReference)
            throws MessageStackException {
        log.debug("uploadFileToDb: jobid " + jobId + " filename " + filename);

        LoaderJob job = startedJobs.get(jobId);
        File jobDirectory = job.getJobDirectory();

        DbFileSupplier fileWorker;
        try {
            Object[] fileAndDoc = getFileForLoader(filename, jobDirectory);
            File fileToUpload = (File) fileAndDoc[0];
            String document = (String) fileAndDoc[1];
            log.debug(fileToUpload);
            String transferKeyId = jobId + ";" + document + (StringUtils.isNotEmpty(docType) ? ";" + docType : "");

            if (!fileToUpload.isFile() || !fileToUpload.exists()) {
                String userErrorMessage = String.format(Res.getString(LOADER_FILE_NULL_ERROR), fileToUpload.getAbsolutePath());
                showJobError(jobId, userErrorMessage);
                throw onError(userErrorMessage, MessageStackException.getCurrentMethod());
            } else {
                fileWorker = new DbFileSupplier(jobDirectory, dbData);
                boolean isUploaded = fileWorker.uploadFile(fileToUpload, transferKeyId, configName, logLevel, logReference);
                if (isUploaded) {
                    log.info("File " + filename + " uploaded successfully");
                }
            }
            return "T";
        } catch (Exception e) {
            throw onError(e, String.format(Res.getString(LOADER_FILE_TO_DB_ERROR), filename) + " " + e.getMessage(), MessageStackException.getCurrentMethod());
        }

    }


    private Object[] getFileForLoader(String filename, File jobDirectory) throws FileNotFoundException {
        String directory;
        String document;
        if (filename.isEmpty()) {
            XmlHandler configXml = new XmlHandler(new FileInputStream(jobDirectory.getAbsolutePath() + System.getProperty("file.separator") + LOADER_CONFIG_FILE));

            directory = getXPathValue(configXml, "config/apps/fullpathname");
            directory = pattern1.matcher(directory).replaceAll((jobDirectory + System.getProperty("file.separator")).replaceAll("\\\\", "\\\\\\\\"));
            document = getXPathValue(configXml, "config/apps/target");//getNodeValue(configDocRoot, "target");
        } else if (new File(filename).getParent() == null) {
            directory = jobDirectory.getAbsolutePath();
            document = filename;
        } else {
            directory = null;
            document = filename;
        }
        File file = directory == null ? new File(document) : new File(directory, document);
        return new Object[]{file, document};
    }


    @WebMethod
    public String getFilesNamesInDirectory(int jobid) {
        log.debug("getFilesNamesInDirectory: jobid " + jobid);

        LoaderJob job = startedJobs.get(jobid);
        File jobDirectory = job.getJobDirectory();

        ArrayList<String> filenames = new ArrayList<>();
        for (File f : jobDirectory.listFiles()) {
            filenames.add(f.getName());
        }
        log.debug("getFilesNamesInDirectory: jobid " + jobid + ", filenames in folder " + jobDirectory.getAbsolutePath() + ": " + filenames.toString());
        return filenames.toString();
    }

    @WebMethod
    public String updateTree(int timeout, String nodetype, String nodeid, String side) {
        log.info("begin " + nodetype + " " + nodeid);
        if (!standalone && loaderGui != null) {
            loaderGui.updateTree(timeout, nodetype, nodeid, side);
        }
        log.info("end");
        return "T";
    }

    @WebMethod
    public String updateRibbon(int timeout) {
        if (!standalone && loaderGui != null) {
            loaderGui.updateRibbon(timeout);
        }
        return "T";
    }

    // Server
    private Endpoint startServer(int port, int maxPort, String context, Object implementer, String host) throws
            MessageStackException {
        try {
            this.port = String.valueOf(port);
            String localHost = StringUtils.isNotEmpty(host) ? host : UrlUtil.getLocalHostAddress();
            wsUrl = UrlUtil.buildUrl("http", localHost, String.valueOf(port), context);
            Endpoint endpoint = UrlUtil.createAndPublishEndpoint(wsUrl, implementer);
            log.info("loader webservice started on " + wsUrl);
            log.debug("endpoint isPublished: " + endpoint.isPublished());
            log.debug("ping: " + ((LoaderWebserviceImpl) endpoint.getImplementor()).ping());
            return endpoint;
        } catch (MessageStackException e) {
            if (port + 1 <= maxPort) {
                return startServer(port + 1, maxPort, context, implementer, host);
            } else {
                this.port = null;
                throw e;
            }
        }
    }

    protected Endpoint startServer(String portRange, String context, String host) throws MessageStackException {
        portRange = portRange.trim();
        int minPort;
        int maxPort;
        if (portRange.indexOf("-") > 1) {
            minPort = Integer.valueOf(portRange.substring(0, portRange.indexOf("-")).trim());
            maxPort = Integer.valueOf(portRange.substring(portRange.indexOf("-") + 1).trim());
        } else {
            minPort = Integer.valueOf(portRange);
            maxPort = minPort;
        }
        log.debug("startServer: Portrange " + minPort + " " + maxPort);
        endpoint = startServer(minPort, maxPort, context, this, host);
        return endpoint;
    }

    private void stopHttpServer() {
        if (endpoint != null) {
            log.debug("stopHttpServer: " + "On server " + port + " webservice " + endpoint.getImplementor().getClass().getName() + " stopped");
            endpoint.stop();
        }

        if (port != null) {
            log.info("stopHttpServer: " + "HTTP-Server stopped on port " + port);
            this.port = null;
        }
    }

    public int getPort() {
        return Integer.parseInt(port);
    }

    @WebMethod(exclude = true)
    public void createEnvironment(String baseDirectoryPart) throws IOException {
        this.baseDirectoryPart = baseDirectoryPart;
        String loaderDirectoryFromSystemProperties = System.getProperty("java.loader.directory");
        log.debug("loader Directory From System Properties: " + loaderDirectoryFromSystemProperties);

        if (loaderDirectoryFromSystemProperties == null) {
            loaderDirectory = new File(System.getProperty("java.io.tmpdir"), baseDirectoryPart);
            System.setProperty("java.loader.directory", loaderDirectory.getAbsolutePath());
        } else {
            loaderDirectory = new File(loaderDirectoryFromSystemProperties);
        }
        checkLoaderDirectory(loaderDirectory);
    }

    @WebMethod(exclude = true)
    public void createEnvironment(String loaderDirectoryPath, String baseDirectoryPart) throws IOException {
        this.baseDirectoryPart = baseDirectoryPart;
        File loaderDirectory = new File(loaderDirectoryPath, baseDirectoryPart);
        System.setProperty("java.loader.directory", loaderDirectory.getAbsolutePath());
        checkLoaderDirectory(loaderDirectory);
    }

    protected void checkLoaderDirectory(File loaderDirectory) throws IOException {
        if (loaderDirectory.exists()) {
            if (loaderDirectory.isFile()) {
                throw new IOException("Can not create loader directory, the proposed name exists as file: " + loaderDirectory.getAbsolutePath());
            }
            log.debug("Load directory is " + loaderDirectory.getAbsolutePath());
        } else {
            boolean wasCreated = loaderDirectory.mkdirs();
            if (!wasCreated) {
                throw new IOException("Could not create loader directory: " + loaderDirectory.getAbsolutePath());
            }
            log.info("Created loader directory: " + loaderDirectory.getAbsolutePath());
        }
    }

    protected void clearEnvironment() {
        log.debug("clearEnvironment number of run jobs " + startedJobs.size());
        Enumeration e = startedJobs.keys();
        while (e.hasMoreElements()) {
            int jobid = (Integer) e.nextElement();
            removeJobFolder(jobid);
            startedJobs.remove(jobid);
        }
    }


    @WebMethod
    public void removeJobFolder(int jobId) {
        log.debug("begin: " + jobId);
        LoaderJob loaderJob = startedJobs.get(jobId);
        File jobDirectory = loaderJob.getJobDirectory();
        String jobDirectoryName = jobDirectory.getAbsolutePath();
        try {
            com.uptodata.isr.server.utils.fileSystem.FileSystemWorker.removeFolder(jobDirectory);
            log.info("Deleted job directory " + jobDirectoryName);
        } catch (IOException e1) {
            log.error("directory " + jobDirectoryName + " coul not be deleted: " + e1);
        }
    }


    public int getJobCnt() {
        return jobCnt;
    }

    public void terminateService() {
        log.debug("terminateService");
        if (standalone && (dbConnLoader != null && !dbConnLoader.isEmpty())) {
            try {
                dispatcher.setLoaderConnection(port, System.getProperty("user.name"), false);
            } catch (BackendException e) {
                ExceptionDialog.showExceptionDialog(null, e, null);
            }
        }
        stopHttpServer();
        clearEnvironment();
    }


    public void setLoaderGui(LoaderGui loaderGui) {
        this.loaderGui = loaderGui;
        loaderGui.setLoaderWebservice(this);
    }

    public LoaderGui getLoaderGui() {
        return loaderGui;
    }

    public void setDispatcher(Dispatcher dispatcher) {
        this.dispatcher = dispatcher;
    }

    boolean canTerminate() {
        return jobCnt <= 0;
    }

    boolean isStandalone() {
        return standalone;
    }

    public void setStandalone(boolean standalone) {
        this.standalone = standalone;
    }

    public void setDbConnLoader(String dbConnLoader) {
        this.dbConnLoader = dbConnLoader;
    }

    String getBaseDirectoryPart() {
        return baseDirectoryPart;
    }

    @WebMethod
    public String compareDocumentOID(String documentOID, byte[] bytes) throws MessageStackException {
        try {
            boolean isEqual = CompareDocumentOID.compareDocumentOID(documentOID, new ByteArrayInputStream(bytes));
            String compare = isEqual ? "T" : "F";
            log.debug(compare);
            return compare;
        } catch (Exception e) {
            throw onError(e, e.getMessage(), MessageStackException.getCurrentMethod());
        }
    }


    @WebMethod
    public String protectAllowOnlyRevisions(int jobId, String fileName, String password, byte[] bytes) throws MessageStackException {
        try {
            ByteArrayOutputStream outputStream = DocumentProtecting.protectAllowOnlyRevisions(
                    fileName, password, new ByteArrayInputStream(bytes));
            LoaderJob job = startedJobs.get(jobId);
            File jobDirectory = job.getJobDirectory();
            File protectedFile = FileUtils.getFile(jobDirectory, fileName);
            log.debug(protectedFile);
            ConvertUtils.bytesToFile(outputStream.toByteArray(), protectedFile);
            return "T";
        } catch (Exception e) {
            log.error(e);
            throw onError(e, e.getMessage(), MessageStackException.getCurrentMethod());
        }
    }


    private static ArrayList<String> getParameterList(NodeList parameters, File mainDirectory) {
        ArrayList<String> parameterList = new ArrayList<>();
        for (int i = 0; i < parameters.getLength(); i++) {
            Node parameter = parameters.item(i);
            String parameterText = parameter.getTextContent();
            try {
                parameterText = pattern1.matcher(parameterText).replaceAll((mainDirectory + System.getProperty("file.separator")).replaceAll("\\\\", "\\\\\\\\"));
            } catch (NullPointerException ignored) {
                log.error(ignored);
                // ignore the error todo kann die exception passieren?
            }
            parameterList.add(parameterText);
        }
        return parameterList;
    }

    protected static MessageStackException onError(Exception e, String userErrorMessage, String implStack) {
        log.error(e);
        MessageStackException messageStackException;
        if (e instanceof MessageStackException) {
            messageStackException = (MessageStackException) e;
        } else {
            messageStackException = new MessageStackException(e, MessageStackException.SEVERITY_CRITICAL, userErrorMessage, implStack);
        }
        return messageStackException;
    }

    protected static MessageStackException onError(String userErrorMessage, String implStack) {
        MessageStackException messageStackException = new MessageStackException(MessageStackException.SEVERITY_CRITICAL, userErrorMessage, implStack, implStack);
        log.error(implStack);
        return messageStackException;
    }

    public static void main(String[] args) {
        File jobDirectory = new File("test");
        LoaderWebserviceImpl loaderWebservice = new LoaderWebserviceImpl();
        File processErrorFile = FileUtils.getFile(jobDirectory, "processError.error");

        log.debug(args[0]);

        switch (args[0]) {
            case "1":  //test of executeApplication
                try {
                    AppExecuter.executeApplication(new File("."),
                            "profiler.exe",
                            "C:\\Users\\smiljanskii60\\AppData\\Local\\Temp\\iStudyReporter\\loader\\smiljanskii60_6942\\2016-07-04_ iStudyReporter_ISRC_35_RC_5.ISRPROF",
                            new ArrayList<String>(),
                            null);
                } catch (IOException e) {
                    e.printStackTrace();
                }
                break;
            case "2":  //test of display
                try {
                    String path = "C:\\Users\\Smiljanskii60\\AppData\\Local\\Temp\\iStudyReporter\\loader\\smiljanskii60_12408\\";
                    String document = "JBE1000-JBE1000-001-JBE1000-(004).DOC";
                    String ret = ProcessHelper.displayDocument(path, document);
                    System.out.println("ret = " + ret);

                } catch (Exception e) {
                    e.printStackTrace();
                }
                break;
            case "3": //test of startApp
                jobDirectory = new File("test\\smiljanskii60_8879");
                String commandLine = jobDirectory + "\\archivemodule.exe";
                try {
                    ProcessBuilder processBuilder = new ProcessBuilder(commandLine, LOADER_CONFIG_FILE, "1");
                    log.debug(processBuilder.command());
                    File moduleErrorFile = FileUtils.getFile(jobDirectory, "archivemodule" + ".error");
                    processBuilder.redirectError(processErrorFile);
                    Process process = processBuilder.start();

                    String ret = loaderWebservice.waitForProcess(process, 1, moduleErrorFile);
                    log.debug(ret);
                } catch (Exception e) {
                    e.printStackTrace();
                }
            case "4":
                loaderWebservice.actionAfterTerminateJob("com.uptodata.isr.loader.addaction.Main",
                        "doAction", 12127, "test", "D:\\subversion\\astellas\\iStudyReporter\\ASTIB\\Trunk\\AUTOMATED_ARCHIVING\\java\\AutomatedArchiving");
        }
    }
}
