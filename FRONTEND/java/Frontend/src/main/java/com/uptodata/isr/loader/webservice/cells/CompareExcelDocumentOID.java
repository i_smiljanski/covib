package com.uptodata.isr.loader.webservice.cells;

import com.aspose.cells.DocumentProperty;
import com.aspose.cells.DocumentPropertyCollection;
import com.aspose.cells.Workbook;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.io.FileInputStream;
import java.io.InputStream;

/**
 * Created by smiljanskii60 on 28.11.2019.
 */
public class CompareExcelDocumentOID {
    public static Logger log = LogManager.getLogger(CompareExcelDocumentOID.class.getName());

    public static String compareDocumentOID(String documentOID, String docPath) {
        log.info("begin " + docPath);
        boolean checkOid = false;
        String error = "";
        try {
            FileInputStream is = new FileInputStream(docPath);
            checkOid = compareDocumentOID(documentOID, is);
            is.close();
            log.info("end: " + checkOid);
        } catch (Exception e) {
            e.printStackTrace();
            error = e.getMessage();
        }
        return checkOid ? "T" : "Document OID '" + documentOID + "' does not match with OID in file '" + docPath + " " + error;
    }

    private static boolean compareDocumentOID(String documentOID, InputStream stream) throws Exception {
        Workbook workbook = new Workbook(stream);
        DocumentPropertyCollection customProperties = workbook.getWorksheets().getCustomDocumentProperties();
        for (Object o : customProperties) {
            DocumentProperty prop = (DocumentProperty) o;
            if ("DOC_OID".equalsIgnoreCase(prop.getName()) && documentOID.equals(prop.getValue())) {
                log.debug(prop.getName() + " : " + prop.getValue());
                return true;
            }
        }
        return false;
    }


    public static void main(String[] args) {

        try {
            log.debug(CompareExcelDocumentOID.compareDocumentOID("1.3.6.1.4.1.24263.1.1.15326.1.2.1.11400.10666",
                    "C:\\Users\\Smiljanskii60\\Test.xlsx"));
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}
