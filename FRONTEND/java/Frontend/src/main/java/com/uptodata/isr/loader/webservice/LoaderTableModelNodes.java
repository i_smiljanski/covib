package com.uptodata.isr.loader.webservice;

import com.utd.stb.gui.swingApp.NodesTableModel;
import com.utd.stb.inter.application.Nodes;
import com.utd.stb.inter.userinput.UserInputs;

/**
 * little helper: empty data
 *
 * @author PeterBuettner.de
 */
class LoaderTableModelNodes extends NodesTableModel {

    public LoaderTableModelNodes(Nodes nodes, UserInputs[] userInputs) {
        super(nodes, userInputs);
    }

}
