package com.utd.stb.inter.wizzard.formtypes;

import com.utd.stb.inter.userinput.UserInput;
import com.utd.stb.inter.wizzard.FormData;


/**
 * The UserInput may be edited inplace, so only commit the (potentially) changed
 * data on saveData().
 * <p>
 * Scenario: <br>
 * User gets some Controls presented, may change data and press [next] to commit
 * all changes.
 *
 * @author PeterBuettner.de
 */
public interface FormDataUserInput extends FormData {

    /**
     * call {@link saveData()}to use the changed UserInput
     *
     * @return UserInput
     */
    UserInput getUserInput(int index);

    /**
     * Gets the number of UserInput
     *
     * @return
     */
    int getUserInputCount();

}