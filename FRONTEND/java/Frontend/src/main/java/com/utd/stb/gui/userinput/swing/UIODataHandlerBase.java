package com.utd.stb.gui.userinput.swing;

import javax.swing.event.EventListenerList;


/**
 * Observer for changes in UserInput and commiting of changed data.
 * <p>
 * May also handle multiple controls, e.g. used to ask a Collection of
 * UserInputs/Controls for some action, maybe all in one _panel/tabsheet. So we
 * can show a hint in the Tab of a tabsheet if at least one control is invalid,
 * or dis/enable an [Ok] button
 *
 * @author PeterBuettner.de
 */
public abstract class UIODataHandlerBase {

    protected EventListenerList listenerList = new EventListenerList();
    private DataChangeEvent changeEvent;

    /**
     * Ask the 'controls' if the data is valid, <b>must not </b> look at empty
     * state, since the gui may want to show the state of empty/invalid in
     * different colors/hints.
     *
     * @return
     */
    public abstract boolean isValid();

    /**
     * Ask the 'controls' if the data is empty, has also say 'empty' if empty is
     * allowed, since anyone may want to list all 'not filled' controls.
     * <p>
     * TODOlater what about: A readonly component that is illegal empty
     * shouldn't be invalid here, or should other gui layers handle this?
     *
     * @return
     */
    public abstract boolean isEmpty();

    /**
     * push data from control to UserInput
     */
    public abstract void commitData();

    /**
     * Adds a <code>ChangeListener</code>
     *
     * @param listener the listener to be added
     */
    public void addChangeListener(DataChangeListener listener) {

        listenerList.add(DataChangeListener.class, listener);
    }

    /**
     * Removes a <code>ChangeListener</code>
     *
     * @param listener the listener to be removed
     */
    public void removeChangeListener(DataChangeListener listener) {

        listenerList.remove(DataChangeListener.class, listener);
    }

    /**
     * Notifies all listeners that have registered interest for notification on
     * this event type. The event instance is lazily created, the source is
     * <code>this</code>.
     *
     * @see EventListenerList
     */

    protected void fireChanged() {
        if (changeEvent == null) changeEvent = new DataChangeEvent(this);
        fireChanged(changeEvent);
    }

    /**
     * Notifies all listeners that have registered interest for notification on
     * this event type. Usefull for redirecting events, since the event isn't
     * changed.
     *
     * @see EventListenerList
     */

    protected void fireChanged(DataChangeEvent event) {

        Object[] listeners = listenerList.getListenerList();
        for (int i = listeners.length - 2; i >= 0; i -= 2) {
            if (listeners[i] == DataChangeListener.class) {
                ((DataChangeListener) listeners[i + 1]).dataChanged(event);
            }
        }
    }

}