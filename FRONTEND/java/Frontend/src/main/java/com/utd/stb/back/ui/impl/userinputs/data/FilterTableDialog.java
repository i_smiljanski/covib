package com.utd.stb.back.ui.impl.userinputs.data;

import com.jidesoft.dialog.JideOptionPane;
import com.utd.stb.back.ui.impl.lib.ButtonItemsImpl;
import com.utd.stb.back.ui.impl.lib.StandardButton;
import com.utd.stb.back.ui.impl.lib.WindowActionImpl;
import com.utd.stb.back.ui.impl.msgboxes.WarningMsgBox;
import com.utd.stb.inter.action.ButtonItem;
import com.utd.stb.inter.action.ButtonItems;
import com.utd.stb.inter.action.WindowAction;
import com.utd.stb.inter.application.BackendException;
import com.utd.stb.inter.dialog.HeaderBar;
import com.utd.stb.inter.dialog.HelpSource;
import com.utd.stb.inter.dialog.UserDialog;
import com.utd.stb.inter.userinput.UserInputs;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import resources.Res;

import javax.swing.*;
import java.awt.*;
import java.util.ArrayList;

/**
 * Implementation of UserDialog for grid tables.
 *
 * @author Inna Smiljanski  created  23.02.11
 */
public class FilterTableDialog implements UserDialog {
    public static final Logger log = LogManager.getRootLogger();

    private String title;
    private HelpSource hs;
    private FilterTableData ui;
    private ButtonItemsImpl buttons;
    private boolean okButtonPressed = false;
    private Object actionOnClose = null;

    public FilterTableDialog(FilterTableData ui, String title, HelpSource hs) {
        this.title = title;
        this.hs = hs;
        this.ui = ui;
        createButtonItems();
    }

    public String getTitle() {
        return title;
    }

    public HeaderBar getHeaderBar() {
        return null;
    }

    public UserInputs getUserInputs() {
        return ui;
    }

    public HelpSource getHelpSource() {
        return hs;
    }

    public ButtonItems getButtonItems() {
        return buttons;
    }

    public ButtonItem getDefaultButtonItem() {
        return buttons.get(0);
    }

    @Override
    public Object doAction(Object action) throws BackendException {
        return doAction(action, null);
    }

    @Override
    public Object doAction(Object action, Component owner) throws BackendException {
        log.debug("action: " + action + " okButtonPressed: " + okButtonPressed);
        if (action instanceof ButtonItem) {
            ButtonItem b = (ButtonItem) action;
            if (b.getType().equals(ButtonItem.CLOSE)) {
                return doCloseAction(owner);
            } else if (b.getType().equals(ButtonItem.SAVE)) {
                return doSaveAction(WindowAction.SAVE_DATA);
            } else if (b.getType().equals(ButtonItem.SAVE_AND_CLOSE)) {
                return doSaveAction(WindowAction.SAVE_DATA_AND_CLOSE_WINDOW);
            }
            // WindowAction means a window button is pressed
        } else if (action instanceof WindowAction) {
            Object retAction = doWindowAction(action);
            return retAction;
        }
        return null;
    }

    private Object doWindowAction(Object action) throws BackendException {
        WindowAction winac = (WindowAction) action;
        Object returnAction = null;
        String actionType = (String) winac.getType();
        log.info("begin " + actionType);

        switch (actionType) {
            case WindowAction.DATA_SAVED:
                returnAction = getActionForSave(WindowAction.NOT_CLOSE_WINDOW);
                break;

            case WindowAction.DATA_SAVED_AND_WINDOW_CLOSED:
                returnAction = getActionForSave(WindowAction.CLOSE_WINDOW);
                if (returnAction == null) {
                    returnAction = new WindowActionImpl(WindowAction.CLOSE_WINDOW);
                }
                break;
            case WindowAction.WINDOW_CLOSED:
                // did we already
                if (!(actionOnClose instanceof SaveAuditDialog)) {
                    returnAction = actionOnClose;
                }
                break;

            case WindowAction.WANT_CLOSE:
                ui.cancelAction();
                returnAction = new WindowActionImpl(WindowAction.CLOSE_WINDOW);
                break;
            default:
                returnAction = null;
        }
        log.info("end " + returnAction);
        return returnAction;
    }

    private Object getActionForSave(String actionAfterAudit) throws BackendException {
        Object returnAction = null;
        if (okButtonPressed) {
            actionOnClose = ui.saveData();
            log.debug("after saveData " + actionOnClose);
            if (!ui.isValid) {
                okButtonPressed = false;
                returnAction = new WarningMsgBox(("exceptionDialog.window.title"), ui.getMessage());
            } else if (actionOnClose instanceof SaveAuditDialog) { // if the action during closing an audit is, return the action without closing the window
                okButtonPressed = false;
                ((SaveAuditDialog) actionOnClose).setWindowActionAfterSaving(actionAfterAudit);
                returnAction = actionOnClose;
            }
        }
        return returnAction;
    }


    private Object doSaveAction(String actionType) {
        if (!okButtonPressed) {
            okButtonPressed = true;
        }
        // tell GUI to write back data to UserInputs
        return new WindowActionImpl(actionType);
    }

    private Object doCloseAction(Component owner) throws BackendException {
        log.debug("CLOSE");
        // tell backend dialog is canceled, and GUI to close window
        okButtonPressed = false;
//                closeButtonPressed = true;
        boolean isMaskChanged = ui.isMaskChanged();
        if (isMaskChanged) {
            String[] options = {
                    Res.getString("com.utd.gui.action.save.name"),
                    Res.getString("com.utd.gui.action.notSave.name"),
                    Res.getString("com.utd.gui.action.cancel.name")
            };
            int result = JideOptionPane.showOptionDialog(owner, Res.getString("dialog.confirm.question"),
                    title, JOptionPane.DEFAULT_OPTION, JOptionPane.WARNING_MESSAGE, null, // optional icon, use null to use the default icon
                    options, options[0]);
            log.debug(result);
            if (result == JOptionPane.YES_OPTION) {
                return doSaveAction(WindowAction.SAVE_DATA_AND_CLOSE_WINDOW);
            } else if (result == JOptionPane.NO_OPTION) {
                ui.cancelAction();
                return new WindowActionImpl(WindowAction.CLOSE_WINDOW);
            }
        } else {
            ui.cancelAction();
            return new WindowActionImpl(WindowAction.CLOSE_WINDOW);
        }
        return null;
    }

    /**
     * OK and Cancel button for this dialog, OK is standard
     */
    private void createButtonItems() {

        ArrayList<StandardButton> list = new ArrayList<StandardButton>();
        StandardButton b = new StandardButton(ButtonItem.SAVE, true);
        list.add(b);
        b = new StandardButton(ButtonItem.SAVE_AND_CLOSE, true);
        list.add(b);
        b = new StandardButton(ButtonItem.CLOSE, false);
        list.add(b);
        buttons = new ButtonItemsImpl(list);

    }
}
