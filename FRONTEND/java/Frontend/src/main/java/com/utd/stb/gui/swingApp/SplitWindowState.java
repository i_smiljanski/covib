package com.utd.stb.gui.swingApp;

import com.uptodata.isr.gui.util.PropertyTools;
import com.uptodata.isr.gui.awt.tools.WindowState;

import java.awt.*;
import java.io.Serializable;
import java.util.Properties;


/**
 * Container for gui options of the main window (has to be a Bean?)
 *
 * @author PeterBuettner.de
 */
public class SplitWindowState implements Serializable {

    private WindowState windowState = new WindowState();
    private int dividerPos = 300;

    public SplitWindowState() {

    }

    public WindowState getWindowState() {

        return windowState;
    }

    public void setWindowState(WindowState windowState) {

        this.windowState = windowState;
    }

    public int getDividerPos() {

        return dividerPos;
    }

    public void setDividerPos(int dividerPos) {

        this.dividerPos = dividerPos;
    }

    public void store(Properties p, String prefix) {

        PropertyTools
                .setRectangle(p, prefix + ".windowState.bounds", windowState.getBounds());
        p.setProperty(prefix + ".windowState", "" + windowState.getState());

        p.setProperty(prefix + ".dividerPos", "" + dividerPos);
    }

    public void load(Properties p, String prefix) {

        Rectangle r = PropertyTools.getRectangle(p, prefix + ".windowState.bounds");
        if (r != null) windowState.setBounds(r);

        Integer i;
        i = PropertyTools.getInteger(p, prefix + ".windowState");
        if (i != null) windowState.setState(i.intValue());

        dividerPos = PropertyTools.getInt(p, prefix + ".dividerPos", dividerPos);
    }

}