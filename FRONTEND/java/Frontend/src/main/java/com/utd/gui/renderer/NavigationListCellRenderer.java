package com.utd.gui.renderer;

import com.uptodata.isr.gui.borders.DashedBorder;
import com.uptodata.isr.gui.util.Colors;

import javax.swing.*;
import java.awt.*;


/**
 * ListcellRenderer for/with: single selection, last selection/focus always
 * hilite (~controlLtHighlight), icon before this item and bold font,
 * Focusborder on Listfocus
 *
 * @author PeterBuettner.de
 */

public class NavigationListCellRenderer extends DefaultListCellRenderer {

    private Icon selIcon;

    /**
     * @param selIcon if null then no icon
     */
    public NavigationListCellRenderer(Icon selIcon) {

        this.selIcon = selIcon;

    }

    // pay attention, since selIcon maybe null!
    Icon noSelIcon = new Icon() { // empty Icon with same size

        public int getIconHeight() {

            return selIcon.getIconHeight();

        }

        public int getIconWidth() {

            return selIcon.getIconWidth();

        }

        public void paintIcon(Component c, Graphics g, int x, int y) {

        }

    };

    public Component getListCellRendererComponent(JList list, Object value, int index,
                                                  boolean isSelected, boolean cellHasFocus) {

        setComponentOrientation(list.getComponentOrientation());
        setEnabled(list.isEnabled());
        setIconTextGap(1);
        setText((value == null) ? "" : value.toString());

        // always show ('focus' == last selected) like everywhere in
        // DefaultListCellRenderer
        boolean showHilite = (index == list.getLeadSelectionIndex()) || isSelected;

        if (showHilite) {

            if (selIcon != null)

                setIcon(selIcon);

            setFont(list.getFont().deriveFont(Font.BOLD));
            setBackground(Colors.getItemHiliteInactiveBack());
            setForeground(Colors.getItemHiliteInactiveText());

        } else {

            if (selIcon != null)

                setIcon(noSelIcon);//noSelIcon depends from selIcon

            setFont(list.getFont());
            setBackground(list.getBackground());
            setForeground(list.getForeground());

        }

        if (cellHasFocus) { // means list and cell is focused

            setBorder(new DashedBorder(list.getForeground(), list.getBackground()));

        } else {

            setBorder(noFocusBorder);

        }

        return this;

    }

}