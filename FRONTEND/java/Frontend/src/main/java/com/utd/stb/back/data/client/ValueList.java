package com.utd.stb.back.data.client;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;


/**
 * Wraps a list of record data of type ValueRecord
 *
 * @author marie-christin dort Created 14.Feb 2006
 * @see ValueRecord
 */
public class ValueList {

    private ArrayList data = new ArrayList();
    private Iterator iterator;

    /**
     * creates an empty EntityList
     */
    public ValueList() {

    }

    /**
     * creates a ValueList with data from List in
     *
     * @param in a list
     */
    public ValueList(ArrayList in) {

        Iterator it = in.iterator();
        while (it.hasNext()) {
            StructMap map = (StructMap) it.next();
            ValueRecord value = new ValueRecord(map);
            data.add(value);
        }
    }

    /**
     * adds all fields of input list to this list
     *
     * @param list a List to add
     */
    public ValueList(List list) {

        data.addAll(list);
    }

    /**
     * adds all fields of input list to this list
     *
     * @param in an PropertyList to add
     */
    public void addAll(ValueList in) {

        in.initIterator();
        while (in.hasNext()) {
            data.add(in.getNext());
        }
    }

    /**
     * adds the record to this list
     *
     * @param sr Record to add
     */
    public void add(ValueRecord rec) {

        data.add(rec);
    }

    /**
     * @param index
     * @return the Record at index
     */
    public ValueRecord get(int index) {

        return (ValueRecord) data.get(index);
    }

    /**
     * sets record at given index to input record
     *
     * @param index
     * @param sr    record to set
     */
    public void set(int index, ValueRecord rec) {

        data.set(index, rec);
    }

    /**
     * sets list iterator to the beginning
     */
    public void initIterator() {

        iterator = data.iterator();
    }

    /**
     * @return true if there is a next record
     */
    public boolean hasNext() {

        return iterator.hasNext();
    }

    /**
     * @return the next ValueRecord
     */
    public ValueRecord getNext() {

        return (ValueRecord) iterator.next();
    }

    /**
     * @return the size of this list
     */
    public int getSize() {

        return data.size();
    }

    /**
     * @param s Record to look up
     * @return the index of this record
     */
    public int indexOf(ValueRecord rec) {

        return data.indexOf(rec);
    }

    /**
     * @return the first ValueRecord in list
     */
    public ValueRecord getFirst() {

        if (data.size() > 0) {
            return (ValueRecord) data.get(0);
        } else {
            return null;
        }
    }

}