package com.utd.stb.back.ui.impl.userinputs.fields;

import com.utd.stb.back.data.client.PropertyRecord;
import com.utd.stb.back.ui.impl.userinputs.data.AbstractInputData;
import com.utd.stb.inter.userinput.NumberInput;


/**
 * Implementation of DirectUserInput for number values
 *
 * @author smiljanskii60 Created 20191112
 */
public class IntegerUserInput extends DirectUserInput implements NumberInput {

    PropertyRecord property;

    /**
     * @param label    Label of field
     * @param value    The value of field
     * @param property Record containing complete information for field
     */
    public IntegerUserInput(String label, Integer value, PropertyRecord property) {

        super(label, Integer.class, value);
        this.property = property;
    }

    /**
     * Sets the new value in underlying data record
     */
    private void setValue() {
        property.setValue(getData());
    }

    /**
     * This is called when saving field to db
     *
     * @return the data record for the field
     * @see AbstractInputData#getListToSave()
     */
    public PropertyRecord getProperty() {
        setValue();
        return property;
    }

    //No Info about that available
    public Integer getPrecision() {
        return null;
    }

    //No Info about that available
    public Number getMin() {
        return null;
    }

    //No Info about that available
    public Number getMax() {
        return null;
    }

    public Number getNumber() {
        return (Integer) getData();
    }

    public void setNumber(Number n) {
        setData(n);
        setValue();
    }

    public void setData(Object data) {
        this.data = data;
    }
}