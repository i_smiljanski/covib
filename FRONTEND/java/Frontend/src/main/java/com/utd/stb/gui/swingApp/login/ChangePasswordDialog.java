/*
 * Created on 23.02.2006
 *
 * @author marie.dort
 */
package com.utd.stb.gui.swingApp.login;

import com.jgoodies.forms.builder.DefaultFormBuilder;
import com.jgoodies.forms.layout.CellConstraints;
import com.jgoodies.forms.layout.FormLayout;
import com.utd.gui.action.ActionUtil;
import com.utd.gui.action.ActionWrap;
import com.utd.gui.event.DocumentChangeAdapter;
import com.utd.gui.util.DialogUtils;
import resources.IconSource;
import resources.Res;

import javax.swing.*;
import javax.swing.event.DocumentEvent;
import javax.swing.text.Document;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;


public class ChangePasswordDialog extends JDialog {

    private JDialog dialog;

    // ... controls .............................
    private Action acStart;
    private Action acCancel;

    private JPasswordField tfOldPassword;
    private JPasswordField tfNewPassword;
    private JPasswordField tfNewPasswordReenter;

    private JButton bStart;
    private JButton bCancel;

    private JProgressBar progress;
    private JLabel lblInfo;

    // have some fields so we may change some components in the content
    /**
     * for the content panel
     */
    private DefaultFormBuilder builder;

    /**
     * in the content pane
     */
    private int infoRow;

    /**
     * content - where the fileds , progress, etc. lives
     */
    private JPanel contentPanel;

    /**
     * save the old password
     */
    private String oldPassword;

    public ChangePasswordDialog(JFrame frame, String userPass) {

        super(frame);

        this.oldPassword = userPass;

        // ... now build dialog
        IconSource iconSource = new IconSource();

        this.setModal(true);
        this.setTitle(getString("window.title"));
        this.setResizable(false);
        this.setDefaultCloseOperation(JFrame.DO_NOTHING_ON_CLOSE);
        this.addWindowListener(new WindowAdapter() {

            public void windowClosing(WindowEvent e) {

                closeLoginFrame(true);
            }
        });
        //dialog.setIconImage( iconSource.getSmallImageByID( "dialogLogin" ) );

        createActions();
        createControls();

        // ............................
        Icon headerIcon = iconSource.getHeaderIconByID("login");
        JPanel titlePanel = DialogUtils.createTitlePanel(getString("headerbar.passwordexpired.title"),
                getString("headerbar.passwordexpired.subText"),
                headerIcon, 150);

        JComponent buttonPanel = DialogUtils.createButtonPanel(bStart,
                bCancel,
                null,
                BorderLayout.NORTH);

        contentPanel = createContentPanel();

        Container c = this.getContentPane();
        c.add(titlePanel, BorderLayout.NORTH);
        c.add(contentPanel);
        c.add(buttonPanel, BorderLayout.SOUTH);

        // Ok/Cancel Actions per Keyboard. default: start , Key-Escape: cancel
        this.getRootPane().setDefaultButton(bStart);
        ActionUtil.putIntoActionMap(this.getRootPane(), acCancel);

        acStart.setEnabled(fieldsAreValid());

        this.pack();
        this.setLocationRelativeTo(null);// center on screen
        tfOldPassword.requestFocus();
        this.setVisible(true);

        // something's going wrong, the layout needs a second pass after show();
        // reason: the JTextPane in the title pane
        this.pack();
    }

    /**
     * read from the resource bundle, it is prefixed with a dialog key
     */
    private String getString(String key) {

        return Res.getString("loginDialog." + key);
    }

    /**
     * @param exit then System.exit();
     */
    private void closeLoginFrame(boolean exit) {

        if (exit)
            System.exit(0);
        else
            this.dispose();
    }

    /**
     * needs controls created, and cbDatabase filled
     *
     * @return
     */
    private JPanel createContentPanel() {

        FormLayout layout = new FormLayout("L:D:G, 2dlu, L:P,2dlu,P,0dlu", "");
        builder = DialogUtils.getFormBuilder(layout);

        appendRow(builder, "oldpassword", tfOldPassword);
        appendRow(builder, "newpassword", tfNewPassword);
        appendRow(builder, "newpasswordreenter", tfNewPasswordReenter);
        builder.appendUnrelatedComponentsGapRow();
        builder.nextLine();

        /*
         * progress and info containing line, the two compos overlap, but
         * doesn't matter since one is always invisible
         */
        builder.append(new JLabel(" "));// Hack to have the 'block' right
        // aligned
        infoRow = builder.getRow();
        builder.append(new JLabel(" "));
        /**
         * in the content pane
         */
        int infoCol = builder.getColumn();
        builder.append(progress);
        builder.nextLine();
        builder.add(lblInfo, new CellConstraints().xyw(1, infoRow, builder.getColumnCount()));
        // ..........................

        builder.setDefaultDialogBorder();
        return builder.getPanel();
    }

    private void appendRow(DefaultFormBuilder builder, String resKey, JComponent compo) {

        builder.append(new JLabel(" "));// Hack to have the 'block' right
        // aligned
        builder.append(createAndBindLabel(resKey, compo), compo);
        builder.nextLine();

    }

    private JLabel createAndBindLabel(String resKey, JComponent compo) {

        JLabel label = new JLabel();
        ActionUtil.initLabelFromMap(label, Res.getActionMap("loginDialog.label." + resKey));
        label.setLabelFor(compo);
        return label;

    }

    private JPasswordField getPasswordField() {

        JPasswordField field;

        field = new JPasswordField(15);
        field.setEchoChar('\u25cf');
        // full round bullet
        // looks also good with default, but with tahoma it is more elegant:
        field.setFont(Font.decode("Tahoma-" + field.getFont().getSize()));

        DocumentChangeAdapter dca = new DocumentChangeAdapter() {

            public void documentChange(DocumentEvent e) {

                acStart.setEnabled(fieldsAreValid());
            }
        };
        field.getDocument().addDocumentListener(dca);

        return field;
    }

    private void createControls() {

        tfOldPassword = getPasswordField();
        tfNewPassword = getPasswordField();
        tfNewPasswordReenter = getPasswordField();

        bStart = new JButton(acStart);
        bCancel = new JButton(acCancel);

        progress = new JProgressBar(JProgressBar.HORIZONTAL) {

            public Dimension getPreferredSize() {// small width, or layout may
                // jumping

                Dimension d = super.getPreferredSize();
                if (d != null) d.width = 20;
                return d;
            }
        };
        progress.setIndeterminate(true);
        progress.setVisible(false);
        lblInfo = new JLabel(" ", JLabel.RIGHT);
    }

    private boolean fieldsAreValid() {

        Document doc;

        doc = tfOldPassword.getDocument();
        if (doc == null || doc.getLength() == 0) return false;
        doc = tfNewPassword.getDocument();
        if (doc == null || doc.getLength() == 0) return false;
        doc = tfNewPasswordReenter.getDocument();
        if (doc == null || doc.getLength() == 0) return false;
        return new String(tfOldPassword.getPassword()).equals(oldPassword)
                && new String(tfNewPassword.getPassword()).equals(new String(tfNewPasswordReenter.getPassword()));
    }

    private void createActions() {

        acStart = new ActionWrap(Res.getActionMap("loginDialog.action.connect"), null) {

            public void actionPerformed(ActionEvent e) {

                closeLoginFrame(false);
            }
        };

        acCancel = new ActionWrap(Res.getActionMap("loginDialog.action.abort"), null) {

            public void actionPerformed(ActionEvent e) {

                closeLoginFrame(true);
            }
        };

    }

    public String getNewPassword() {
        return !fieldsAreValid() ? "" : new String(tfNewPassword.getPassword());
    }
}
