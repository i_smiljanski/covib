package com.utd.stb.gui.wizzard.choosers;

import com.utd.stb.gui.swingApp.exceptions.ExceptionHandler;
import com.utd.stb.inter.wizzard.FormData;
import com.utd.stb.inter.wizzard.formtypes.*;


/**
 * create Choosers to diplay the gui for a FormData
 *
 * @author PeterBuettner.de <br>
 *         maybe create a ChooserFactory-interface
 */
public class ChooserFactory {

    private static final ChooserFactory shared = new ChooserFactory();

    private ChooserFactory() {

    }

    public static ChooserFactory getInstance() {

        return shared;
    }

    public AbstractChooser getChooser(ExceptionHandler exceptionHandler, FormData formData) {

        if (formData instanceof FormDataOneOutOfList)
            return new SingleItemChooser(exceptionHandler,
                    (FormDataOneOutOfList) formData);

        if (formData instanceof FormDataGridOptions)
            return new GridChooser(exceptionHandler,
                    (FormDataGridOptions) formData);

        if (formData instanceof FormDataListOutOfList)
            return new TwoTableChooser(exceptionHandler,
                    (FormDataListOutOfList) formData);

        if (formData instanceof FormDataListOutOfCategoriesList)
            return new TwoTreeTableChooser(exceptionHandler,
                    (FormDataListOutOfCategoriesList) formData);

        if (formData instanceof FormDataUserInput)
            return new UserInputChooser(exceptionHandler,
                    (FormDataUserInput) formData);
        return null;
    }

}