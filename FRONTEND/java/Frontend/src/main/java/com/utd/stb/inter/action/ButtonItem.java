package com.utd.stb.inter.action;

/**
 * Actions the user may invoke e.g. in a dialog: <br>
 * Ok, Cancel, Yes, No, Save,... <br>
 * Defined to create them easy and i18n, the type (Ok, Cancel, Yes, No) is
 * identified by type, the order is specified (in some cases) by the gui, or by
 * the Dialog interface On invocation by the user the instance is send as is to
 * the base
 *
 * @author PeterBuettner.de
 */
public interface ButtonItem {

    public final static Object OK = "Ok";
    public final static Object CANCEL = "Cancel";
    public final static Object YES = "Yes";
    public final static Object NO = "No";
    public final static Object SAVE = "Save";
    public final static Object SAVE_AND_CLOSE = "Save and close";
    public final static Object CLOSE = "Close";
    public final static Object NEW = "New";
    public final static Object DELETE = "Delete";
    public final static Object OTHER = "Other";

    /**
     * Use:
     * <p>
     * <pre>
     * ButtonAction.OK
     * <br>
     * ButtonAction.CANCEL
     * <br>
     * ButtonAction.YES
     * <br>
     * ButtonAction.NO
     * <br>
     * ButtonAction.SAVE
     * <br>
     * ButtonAction.CLOSE
     * <br>
     * <br>
     * ButtonAction.OTHER
     * <br>
     * </pre>
     * <p>
     * Use OTHER to have a button with a special text, getTranslation() is then used
     * for display.
     *
     * @return
     */

    Object getType();

    /**
     * if getType is OTHER, getTranslation() will be the text displayed on the button,
     * <br>
     * if type is not OTHER this one is ignored
     *
     * @return
     */
    String getText();

    /**
     * if true then this button is only enabled if the data displayed in the
     * dialog is valid (All UserInput.isValid()... and not empty in the case
     * that this is forbidden). This property is ignored if there is no data to
     * be validated.
     *
     * @return
     */
    boolean isNeedsValidData();

}