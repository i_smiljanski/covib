package com.utd.stb.gui.wizzard.wizzardStep;

import com.utd.gui.event.EventTools;
import com.utd.gui.util.DialogUtils;
import com.utd.stb.inter.wizzard.WizardStep;
import com.utd.stb.inter.wizzard.WizardSteps;

import javax.swing.*;
import java.awt.*;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.util.ArrayList;


/**
 * Dialog to choose a WizardStep out of a list.
 *
 * @author PeterBuettner.de
 */
public class WizardStepDialog {

    static class GoToWizardStepCellRenderer extends DefaultListCellRenderer {

        public Component getListCellRendererComponent(JList list, Object value, int index,
                                                      boolean isSelected, boolean cellHasFocus) {

            super.getListCellRendererComponent(list, value, index, isSelected, cellHasFocus);
            WizardStepTools.configLabelFromWizardStep(this, value);
            return this;
        }
    }

    /**
     * Shows the Dialog to choose a WizardStep out of a list, returns null if
     * nothing choosen (Cancel) or the step the user seleected on Ok
     *
     * @param parent      a component, we search for the Window(Frame/Dialog) this
     *                    component belongs to
     * @param title       of the dialog
     * @param label       a small text on top of the list, uses textWithMnemonic (prefix
     *                    any char with &amp; for mnemonic)
     * @param wizardSteps the steps the user may choose from
     * @param current     the step selected on startup of the created dialog
     * @return the choosen one or null on Cancel
     */
    static public WizardStep showGoToDialog(Component parent, String title, String label,
                                            WizardSteps wizardSteps, final WizardStep current) {

        final JList listSteps = new JList(WizardStepTools.wizardStepsToArray(wizardSteps));
        listSteps.setCellRenderer(new GoToWizardStepCellRenderer());

        listSteps.addMouseListener(new MouseAdapter() {// press ok on dblclick

            public void mouseClicked(MouseEvent e) {

                if (!EventTools.isLeftMouseDblClick(e)) return;
                JRootPane rp = (JRootPane) SwingUtilities
                        .getAncestorOfClass(JRootPane.class, listSteps);
                if (rp == null) return;
                JButton b = rp.getDefaultButton();
                if (b == null) return;
                b.doClick(1);
            }
        });

        SwingUtilities.invokeLater(new Runnable() {

            public void run() {// we need to be visible!

                listSteps.requestFocusInWindow();
                listSteps.setSelectedValue(current, true);
            }
        });

        JPanel navPnl = new JPanel(new BorderLayout(3, 3));
        navPnl.add(DialogUtils.buildAndBindJLabel(label, listSteps), "North");
        navPnl.add(new JScrollPane(listSteps));
        Dimension dim = navPnl.getPreferredSize();
        dim.width = Math.max(dim.width, 300);
        navPnl.setPreferredSize(dim);

        if (!DialogUtils.showDialogOkCancel(parent, navPnl, title)) return null;

        WizardStep sel = (WizardStep) listSteps.getSelectedValue();
        if (current.equals(sel)) return null;// no change
        return sel;
    }

    /**
     * Shows the Dialog to choose a WizardStep out of a list, returns null if
     * nothing choosen (Cancel) or the step the user seleected on Ok
     *
     * @param parent      a component, we search for the Window(Frame/Dialog) this
     *                    component belongs to
     * @param title       of the dialog
     * @param label       a small text on top of the list, uses textWithMnemonic (prefix
     *                    any char with &amp; for mnemonic)
     * @param wizardSteps the steps the user may choose from
     * @param current     the step selected on startup of the created dialog
     * @return the choosen one or null on Cancel
     */
    static public String showFilterDialog(Component parent,
                                          String title,
                                          String label,
                                          final Object listData) {

        final JList listSteps = new JList(((ArrayList) listData).toArray());
        listSteps.setCellRenderer(new GoToWizardStepCellRenderer());

        listSteps.addMouseListener(new MouseAdapter() { // press ok on dblclick

            public void mouseClicked(MouseEvent e) {

                if (!EventTools.isLeftMouseDblClick(e)) return;
                JRootPane rp = (JRootPane) SwingUtilities
                        .getAncestorOfClass(JRootPane.class, listSteps);
                if (rp == null) return;
                JButton b = rp.getDefaultButton();
                if (b == null) return;
                b.doClick(1);
            }
        });

        SwingUtilities.invokeLater(new Runnable() {

            public void run() {// we need to be visible!

                listSteps.requestFocusInWindow();
                listSteps.setSelectionInterval(0, 0);
                //listSteps.setSelectedValue( current, true );
            }
        });

        JPanel navPnl = new JPanel(new BorderLayout(3, 3));
        navPnl.add(DialogUtils.buildAndBindJLabel(label, listSteps), "North");
        navPnl.add(new JScrollPane(listSteps));
        Dimension dim = navPnl.getPreferredSize();
        dim.width = Math.max(dim.width, 300);
        navPnl.setPreferredSize(dim);

        if (!DialogUtils.showDialogOkCancel(parent, navPnl, title)) return null;

        return listSteps.getSelectedValue() == null ? "" : listSteps.getSelectedValue().toString();
    }

}