package com.utd.stb.back.custom.impl;

import com.utd.stb.back.custom.basic.AbstractCustomMenu;
import com.utd.stb.back.custom.basic.CustomMenuItemImpl;

import java.sql.Connection;
import java.util.ArrayList;
import java.util.List;


/**
 * @author rainer bruns Created 20.12.2004 Example Implementation of a
 *         CustomMenu
 */
public class CustomMenu extends AbstractCustomMenu {

    Connection conn;

    public CustomMenu(Connection conn) {

        this.conn = conn;
    }

    public static boolean hasCustomMenu() {

        return false;
    }

    public List getCustomMenu(List menuList) {

        ArrayList list = new ArrayList();
        list.add(new CustomMenuItemImpl("Do something", true, new CustomActionDemo(conn)));
        list.add(new CustomMenuItemImpl("Stop Loader", true, new StopLoader()));
        CustomMenuItemsImpl customMenuList = new CustomMenuItemsImpl(list);
        menuList.add(new CustomMenuItemImpl(customMenuList, "Custom"));
        return menuList;
    }
}

