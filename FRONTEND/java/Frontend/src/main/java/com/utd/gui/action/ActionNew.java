package com.utd.gui.action;

import com.utd.gui.resources.I18N;

import javax.swing.*;


public abstract class ActionNew extends AbstractAction {

    public ActionNew() {

        ActionUtil.initActionFromMap(this, I18N.getActionMap(ActionNew.class.getName()),
                null);
        putValue(SHORT_DESCRIPTION, null);
    }

}