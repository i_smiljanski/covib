package com.utd.gui.controls;

import com.uptodata.isr.gui.util.Colors;

import javax.swing.*;
import java.awt.*;


public class GradientTitle extends JLabel {

    public GradientTitle(String text) {

        super(text);
        setOpaque(false);
    }

    protected void paintComponent(Graphics g) {

        Color fore = Colors.getUIControlLtHighlight();

        if (!getForeground().equals(fore)) setForeground(fore);

        paintGradient(g);
        super.paintComponent(g);
    }

    private void paintGradient(Graphics g) {

        Color back = Colors.getUIControlDkShadow();
        Color grad = Colors.getUIControlShadow();
        float x2 = getWidth() * .85f;

        Graphics2D g2 = (Graphics2D) g;
        Paint savePaint = g2.getPaint();
        g2.setPaint(new GradientPaint(0, 0, back, x2, 0, grad));
        g2.fillRect(0, 0, getWidth(), getHeight());
        g2.setPaint(savePaint);
    }

}