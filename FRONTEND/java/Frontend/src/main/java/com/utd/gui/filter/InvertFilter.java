package com.utd.gui.filter;

/**
 * A Filter based on another, inverting the match.
 *
 * @author PeterBuettner.de
 */
public class InvertFilter extends ChainingFilter {

    private boolean invert = true;

    public InvertFilter(Filter baseFilter) {

        super(baseFilter);

    }

    public boolean matches(Object candidate) {

        return super.matches(candidate) ^ invert;

    }

    public boolean isInvert() {

        return invert;

    }

    public void setInvert(boolean invert) {

        this.invert = invert;

        if (this.invert != invert) fireFilterChanged();

    }

}