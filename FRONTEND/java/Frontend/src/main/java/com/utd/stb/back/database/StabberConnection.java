package com.utd.stb.back.database;

import com.utd.stb.inter.application.BackendException;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;
import java.sql.SQLWarning;
import java.util.Properties;


/**
 * Object that wraps everything around the database connection itsself
 *
 * @author rainer bruns Created 13.01.2005
 */
public class StabberConnection {
    public static final Logger log = LogManager.getRootLogger();

    private String user;
    private String password;
    private String server;
    private String serverAlias;
    private String newPassword;
    private Connection connection;
    private SQLWarning sqlw;


    /**
     * @param user        the username
     * @param password    the password
     * @param server      the serverstring (host:port:sid)
     * @param serverAlias the alias of the server
     */
    public StabberConnection(String user,
                             String password,
                             String server,
                             String serverAlias,
                             String newPassword) {

        this.user = user;
        this.password = password;
        this.server = server;
        this.serverAlias = serverAlias;
        this.newPassword = newPassword;

    }

    /**
     * tries to connect to the database with given connection data from
     * constructor. Stores warrnings.
     *
     * @return the database connection object
     * @throws SQLException
     */
    public Connection connect() throws SQLException {
        DriverManager.registerDriver(new oracle.jdbc.driver.OracleDriver());

        Properties props = new Properties();
        props.put("user", user);
        props.put("password", password);
        props.put("SetBigStringTryClob", "true");
        log.debug("begin " + user );

        if (!newPassword.equals("")) {
            props.put("OCINewPassword", newPassword);
            String host = server.substring(0, server.indexOf(":"));
            String port = server.substring(server.indexOf(":") + 1, server.lastIndexOf(":"));
            String sid = server.substring(server.lastIndexOf(":") + 1);
            connection = DriverManager.getConnection("jdbc:oracle:oci:@" +
                            "(DESCRIPTION=" +
                            "(ADDRESS_LIST=" +
                            "(ADDRESS=" +
                            "(PROTOCOL=TCP)" +
                            "(HOST=" + host + ")" +
                            "(PORT=" + port + ")))" +
                            "(CONNECT_DATA=" +
                            "(SID=" + sid + ")))",
                    props);
            if (connection != null && !connection.isClosed()) connection.close();
            props.remove("OCINewPassword");
            props.remove("password");
            props.put("password", newPassword);
        }
        connection = DriverManager.getConnection("jdbc:oracle:thin:@" + server, props);
        connection.setAutoCommit(false);
        sqlw = connection.getWarnings();
        return connection;
    }

    /**
     * @return true if warning indicates password expiration
     */
    protected boolean passwordWillExpire() {

        return sqlw != null && sqlw.getErrorCode() == 28002;
    }

    /**
     * tries to close database connection
     *
     * @throws SQLException
     */
    public void disconnect() throws SQLException {

        connection.close();
    }

    /**
     * @return the username
     */
    public String getUser() {

        return user;
    }

    /**
     * @return the password. Clear, but may think about encoding (like as demo
     * in Dispatcher)
     */
    public String getPassword() {

        return password;
    }

    /**
     * @return complete server string
     */
    public String getServer() {

        return server;
    }

    /**
     * @return the server alias
     */
    public String getServerAlias() {

        return serverAlias;
    }

    /**
     * @param s the new user
     */
    protected void setUser(String s) {

        user = s;
    }

    /**
     * @param s the new password. After password change.
     */
    protected void setPassword(String s) {

        password = s;
    }

    /**
     * @param s the new server
     */
    protected void setServer(String s) {

        server = s;
    }

    /**
     * custom actions get their db connection from here. It is seperate from ISR
     * connection.
     *
     * @return a database connection object
     * @throws BackendException
     */
    protected Connection getCustomConnection() throws BackendException {

        try {
            Connection custom = DriverManager.getConnection("jdbc:oracle:thin:@" + server,
                    user, password);
            custom.setAutoCommit(false);
            return custom;
        } catch (SQLException se) {
            throw new BackendException(se.getMessage(), se,
                    BackendException.SEVERITY_INFORMATIONAL);
        }
    }

    /**
     * just in case. But should be backends job normally.
     */
    protected void rollback() {

        try {
            connection.rollback();
        } catch (SQLException s) {
            log.error(s);
        }

    }
}