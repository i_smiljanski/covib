package com.utd.gui.event;

import javax.swing.*;
import javax.swing.event.DocumentEvent;
import javax.swing.event.DocumentListener;


/**
 * A DocumentListener that informs something about a document change, but
 * deferred, by the help of SwingUtilities.invokeLater
 *
 * @author PeterBuettner.de
 */
public abstract class DocumentDeferredChangeAdapter implements DocumentListener {

    public void changedUpdate(DocumentEvent e) {

        docChangeLater(e);
    }

    public void insertUpdate(DocumentEvent e) {

        docChangeLater(e);
    }

    public void removeUpdate(DocumentEvent e) {

        docChangeLater(e);
    }

    private void docChangeLater(final DocumentEvent e) {

        SwingUtilities.invokeLater(new Runnable() {

            public void run() {

                deferredChange(e);
            }
        });

    }

    /**
     * Informs about a document change, but deferred, called through
     * SwingUtilities.invokeLater
     *
     * @param e The original unchanged DocumentEvent
     */
    public abstract void deferredChange(DocumentEvent e);

}