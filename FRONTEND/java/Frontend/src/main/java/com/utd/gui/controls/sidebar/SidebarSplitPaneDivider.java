package com.utd.gui.controls.sidebar;

import com.utd.gui.action.ActionUtil;
import com.utd.gui.action.NullAction;
import com.utd.gui.animate.Mover;
import com.utd.gui.animate.SwingAnimator;
import com.utd.gui.event.EventTools;
import com.utd.gui.resources.I18N;
import com.utd.gui.util.DialogUtils;

import javax.swing.*;
import javax.swing.border.EmptyBorder;
import javax.swing.plaf.basic.BasicSplitPaneDivider;
import javax.swing.plaf.basic.BasicSplitPaneUI;
import java.awt.*;
import java.awt.event.*;
import java.awt.geom.AffineTransform;
import java.awt.geom.GeneralPath;
import java.awt.geom.Line2D;
import java.awt.geom.Rectangle2D;
import java.util.Iterator;


/**
 * Divider like the SidebarDivider in Mozilla, just one button to open/close it,
 * dblClick: set to preferredsize
 * <p>
 * Disable focus on left component - current state: use an intermediate left
 * component, hide their only child on 'move-to-zero' and show it on
 * 'move-to-non-zero'. also do this procedure on size-to-preferred
 * <p>
 *
 * @author PeterBuettner.de
 */
public class SidebarSplitPaneDivider extends BasicSplitPaneDivider {

    /*
     * 
     * 
     * Strategy: - The Layout is set to (No, not:BoxLayout) a special own
     * miniLayout. - The default 'left' 'right' Buttons are set to size (0,0)
     * and are removed - instead [JButton] is added. - the buttons size is
     * defined indirect by the icon (and the Layout maximizes the narrow extent)
     * 
     * It is not tested with all L&F, but a lot. seems to work
     */

    /* like WindowsSplitPaneDivider: show focus for keyboard sizing */
    public void paint(Graphics g) {

        super.paint(g);
        if (!splitPane.hasFocus()) return;
        Color bgColor = UIManager.getColor("SplitPane.shadow");
        if (bgColor == null) return;

        Dimension d = getSize();
        g.setColor(bgColor);
        g.fillRect(0, 0, d.width, d.height);

    }

    // used to change language & co
    private Action acToggle;

    private AbstractButton toggleButton;

    /**
     * if location while dragging &lt; snapToMargin set loc to 0
     */
    private int snapToMargin = 15;

    public SidebarSplitPaneDivider(BasicSplitPaneUI ui) {

        super(ui);

        createAction();

        // Handle splitter move; show/hide 'Left' Component
        addComponentListener(new ComponentAdapter() {

            public void componentMoved(ComponentEvent e) {

                onComponentMoved();
            }

        });

        // DblClick on splitter (not button) set size to preferred
        addMouseListener(new MouseAdapter() {

            public void mouseClicked(MouseEvent e) {

                if (!EventTools.isLeftMouseDblClick(e)) return;

                //splitPane.resetToPreferredSizes(); // no, we only look on
                // left component
                Component leftComponent = splitPane.getLeftComponent();
                if (leftComponent == null) return;
                Dimension d = leftComponent.getPreferredSize();
                if (d == null) return;

                if (doAnimate())
                    smoothMoveTo(splitPane.getDividerLocation(), isVertical()
                            ? d.width
                            : d.height);
                else
                    splitPane.setDividerLocation(isVertical() ? d.width : d.height);
                e.consume();
            }
        });

        setLayout(new CenterLayout());

        // throw away focus on escape
        // addKeyListener(new KeyAdapter() doesn't see anything
    }// -----------------------------------------------------------

    private void createAction() {

        if (acToggle != null) return;// only once, but we need to call
        // createAction() twice to be shure... if
        // we set the ui later on a JSplitPane

        acToggle = new AbstractAction() {

            public void actionPerformed(ActionEvent e) {

                toggleOpenClose();
            }
        };

        acToggle
                .putValue(Action.SHORT_DESCRIPTION, "<html><body style='padding:2px;'>Toggle<br>"
                        + DialogUtils
                        .getColoredKeystrokeHtmlText(KeyStroke
                                .getKeyStroke("F9")));

        Action acTemp = new NullAction();
        ActionUtil.initActionFromMap(acTemp, I18N.getActionMap(getClass().getName()), null);
        configureFromAction(acTemp);
    }// -----------------------------------------------------------

    /**
     * reads ACCELERATOR_KEY and SHORT_DESCRIPTION from template, rest is
     * ignored.
     *
     * @param template
     */
    public void configureFromAction(Action template) {

        Object value = template.getValue(Action.SHORT_DESCRIPTION);
        acToggle.putValue(Action.SHORT_DESCRIPTION, value);

        value = template.getValue(Action.ACCELERATOR_KEY);
        acToggle.putValue(Action.ACCELERATOR_KEY, value);

    }

    private boolean doAnimate() {

        return splitPane.isContinuousLayout();
    }

    /**
     * we hide the component if minimized, so it can't get focus
     */

    protected void onComponentMoved() {

        Component c = splitPane.getLeftComponent();
        if (c == null) return;

        if (!(c instanceof Container)) return;
        Component child = ((Container) c).getComponent(0);
        if (child == null) return;
        child.setVisible(!isOnMinimum());
        //child.setEnabled() doesn't work

    }// ------------------------------------------------

    /**
     * If the <b>splitter is vertical <b>, which is equvalent to: <br>
     * splitPane.getOrientation() == JSplitPane.HORIZONTAL_SPLIT
     *
     * @return
     */
    private boolean isVertical() {

        return splitPane.getOrientation() == JSplitPane.HORIZONTAL_SPLIT;

    }

    /**
     * really specialized LayoutManager, only for this class
     */
    private final class CenterLayout implements LayoutManager {

        public void layoutContainer(Container parent) {

            if (parent.getComponentCount() == 0) return;
            if (toggleButton == null) return;
            Component c = parent.getComponent(0);
            if (c != toggleButton) return;

            Dimension ps = parent.getSize();
            Dimension cs = c.getPreferredSize();
            if (isVertical())
                c.setBounds(0, (ps.height - cs.height) / 2, ps.width, cs.height);
            else
                c.setBounds((ps.width - cs.width) / 2, 0, cs.width, ps.height);
        }

        public void addLayoutComponent(String name, Component comp) {

        }

        public void removeLayoutComponent(Component comp) {

        }

        public Dimension minimumLayoutSize(Container parent) {

            return new Dimension();
        }

        public Dimension preferredLayoutSize(Container parent) {

            return new Dimension();
        }
    }

    protected JButton createLeftOneTouchButton() {

        return new DummyButton();
    }

    protected JButton createRightOneTouchButton() {

        return new DummyButton();
    }

    /**
     * creates the special Toggle Button
     */
    protected AbstractButton createToggleButton() {

        createAction();
        AbstractButton b = new JButton(acToggle);

        b.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
        b.setFocusable(false);
        b.setBorderPainted(false);
        b.setBorder(new EmptyBorder(0, 0, 0, 0));
        b.setContentAreaFilled(false);// e.g. for metall L&F, on Win doesn't
        // matter

        b.setIcon(new LineIcon(100, false));
        b.setPressedIcon(new LineIcon(100, true));
        b.setHorizontalAlignment(SwingConstants.CENTER);
        return b;
    }

    protected synchronized void oneTouchExpandableChanged() {

        // first defined like in super.oneTouchExpandableChanged
        boolean first = splitPane.isOneTouchExpandable() && leftButton == null
                && rightButton == null;
        super.oneTouchExpandableChanged();

        if (first) {
            removeAll();
            if (toggleButton == null) toggleButton = createToggleButton();
            add(toggleButton);
        }
    }

    private boolean isOnMinimum() {

        // like in BasicSplitPaneDivider:
        // We use the location from the UI directly, as the location the
        // JSplitPane itself maintains is not necessarly correct.
        return splitPaneUI.getDividerLocation(splitPane) <= getEdge();
    }

    /**
     * Left/Top inset of the splitpane, depending on orientation VERTICAL or
     * HORIZONTAL
     *
     * @return
     */
    private int getEdge() {

        return (isVertical()) ? splitPane.getInsets().left : splitPane.getInsets().top;
    }

    //inspired by OneTouchActionHandler in BasicSplitPaneDivider
    protected void toggleOpenClose() {

        // We use the location from the UI directly, as the location the
        // JSplitPane itself maintains is not necessarly correct.
        int currentLoc = splitPaneUI.getDividerLocation(splitPane);
        int edge = getEdge();
        // reset to 'zero' or use last loc
        int newLoc = (currentLoc > edge) ? edge : splitPane.getLastDividerLocation();

        if (currentLoc != newLoc) {
            if (doAnimate())
                smoothMoveTo(currentLoc, newLoc);
            else {
                splitPane.setDividerLocation(newLoc);
                splitPane.setLastDividerLocation(currentLoc);

            }

        }

    }

    private void smoothMoveTo(final int start, final int end) {

        if (start == end) return;
        Iterator values = new Mover(start, end).iteratorInteger();
        // Iterator values = new Mover(start, end).iteratorLinearInteger();

        SwingAnimator ani = new SwingAnimator() {

            protected void processValue(Number value, boolean isLastValue) {

                if (isLastValue) {
                    splitPane.setDividerLocation(end);
                    //set after end. 'cause setDividerLocation had set it to an
                    // intermediate value:
                    splitPane.setLastDividerLocation(start);
                } else
                    splitPane.setDividerLocation(value.intValue());
            }
        };
        ani.run(this, 20, values);

    }

    private int snapMargin(int location) {

        int edge = getEdge();
        return location < snapToMargin + edge ? edge : location; // snap to zero

        //	return (location<=edge)?edge:(location>snapToMargin+edge? location:
        // snapToMargin+edge);// Mozilla-like, at least margin

        //	//Mozilla-like, at least margin, but with soft border:( enhance
        // parameters, depend on snapToMargin
        //	if (location<=edge)return edge;
        //	if (location>snapToMargin+edge)return location;
        //	// location between edge and snapToMargin+ edge; be smooth
        //	double off = snapToMargin/1f*
        // (1f-Math.exp(-Math.abs(snapToMargin+edge-location)/3f/snapToMargin));
        //	return (int)(snapToMargin+edge - off) ;
    }

    protected void dragDividerTo(int location) {

        super.dragDividerTo(snapMargin(location));
    }

    protected void finishDraggingTo(int location) {

        super.finishDraggingTo(snapMargin(location));
    }

    protected void prepareForDragging() {

        super.prepareForDragging();
    }

    public int getSnapToMargin() {

        return snapToMargin;
    }

    public void setSnapToMargin(int snapToMargin) {

        this.snapToMargin = snapToMargin;
    }

    private class LineIcon implements Icon {

        // two lines looking like two JSeparators
        private int width = 5;
        private int length;
        private boolean pressed;

        public LineIcon(int length, boolean pressed) {

            this.length = length;
            this.pressed = pressed;
        }

        public int getIconHeight() {

            return isVertical() ? length : width;
        }

        public int getIconWidth() {

            return isVertical() ? width : length;
        }

        public void paintIcon(Component c, Graphics g1, int x, int y) {

            Color dark = UIManager.getColor("controlShadow");
            Color light = UIManager.getColor("controlLtHighlight");

            if (pressed) dark = dark.darker();
            // if(pressed) dark = UIManager.getColor("controlDkShadow");

            Graphics2D g = (Graphics2D) g1;
            Object saveAntialias = g.getRenderingHint(RenderingHints.KEY_ANTIALIASING);
            g.setRenderingHint(RenderingHints.KEY_ANTIALIASING,
                    RenderingHints.VALUE_ANTIALIAS_ON);

            boolean down = pressed && false; // no offset if pressed (looks
            // better)
            int off = down ? 1 : 0;

            g.setColor(down ? dark : light);
            if (isVertical())
                g.fill(new Rectangle2D.Float(x + off, y, width, length));
            else
                g.fill(new Rectangle2D.Float(x, y + off, length, width));

            g.setColor(down ? light : dark);
            if (isVertical()) {
                g.fill(new Rectangle2D.Float(x + 1 + off, y, 1, length));
                g.fill(new Rectangle2D.Float(x + 3 + off, y, 1, length));
            } else {
                g.fill(new Rectangle2D.Float(x, y + 1 + off, length, 1));
                g.fill(new Rectangle2D.Float(x, y + 3 + off, length, 1));
            }

            // experimental triangle (center on (0,0)):------------------
            float d = width / 2f; // half width
            GeneralPath tri = new GeneralPath(new Line2D.Float(-d, -d * 2, d, 0));
            tri.lineTo(-d, d * 2);
            tri.closePath();

            boolean v = isVertical();
            //	flip/mirror/rotate tiangle by theta:
            double theta = (v ? 0 : Math.PI / 2) + (isOnMinimum() ? 0 : Math.PI);
            float xoff = x + (v ? (off + d) : (length / 2f));
            float yoff = y + (!v ? (off + d) : (length / 2f));

            AffineTransform at = AffineTransform.getTranslateInstance(xoff, yoff);
            at.rotate(theta);// concatenates matix on _RIGHT_ side
            tri.transform(at);
            g.fill(tri);

            g.setRenderingHint(RenderingHints.KEY_ANTIALIASING, saveAntialias);
        }
    }

    // ################################################################
    private static final class DummyButton extends JButton {

        Dimension ZERO = new Dimension();

        private DummyButton() {

            setFocusable(false);
        }

        public Dimension getMaximumSize() {

            return ZERO;
        }

        public Dimension getMinimumSize() {

            return ZERO;
        }

        public Dimension getPreferredSize() {

            return ZERO;
        }
    }

}

