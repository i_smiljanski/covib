package com.utd.stb.back.data.client;

import java.util.ArrayList;


/**
 * wraps the Array type data. Stores the db descriptor string, so when writing
 * Array data to db, from here we know how to.
 *
 * @author rainer bruns Created 20.12.2004
 */
public class ArrayMap extends ArrayList {

    String descriptor;
    ArrayList data;

    /**
     * @param data the Array data
     * @param desc the db descriptor string of Array
     */
    public ArrayMap(ArrayList data, String desc) {

        this.data = data;
        this.descriptor = desc;
    }

    /**
     * @return the db descriptor string
     */
    public String getDescriptor() {

        return descriptor;
    }

    /**
     * @return the Array data
     */
    public ArrayList getArray() {

        return data;
    }

}