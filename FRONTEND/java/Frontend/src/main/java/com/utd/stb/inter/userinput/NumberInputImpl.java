package com.utd.stb.inter.userinput;

import java.math.BigInteger;


/**
 * @author PeterBuettner.de
 */
public class NumberInputImpl implements NumberInput {

    private Number number;
    private Number min;
    private Number max;
    private Integer precision;
    private Class type;

    public NumberInputImpl(Number number, Number min, Number max, Integer precision,
                           Class type) {

        this.number = number;
        this.min = min;
        this.max = max;
        this.precision = precision;
        this.type = type;
    }

    public Number getNumber() {

        return number;
    }

    public void setNumber(Number n) {

        if (precision == null) {// nothing to do
            number = n;
            return;
        }

        // the integral types doesn't need to be rounded
        if (n instanceof BigInteger || n instanceof Long || n instanceof Integer
                || n instanceof Short || n instanceof Byte) {
            number = n;
            return;
        }

        if (precision.intValue() == 0) {
            double d = Math.round(n.doubleValue());
            if (n instanceof Double)
                number = new Double(d);
            else if (n instanceof Float)
                number = new Float(d);
            else
                number = n; // BigDecimal, BigInteger
            return;
        }

        number = n;
    }

    public Integer getPrecision() {

        return precision;
    }

    public Number getMin() {

        return min;
    }

    public Number getMax() {

        return max;
    }

    public Class getType() {

        return type;
    }
}