package com.utd.stb.gui.swingApp.exceptions;

import java.util.List;

/**
 * @author Inna Smiljanski
 *         Date: 04.04.2014
 */
public class MessageStack {

    List<String> userStack;
    List<String> implStack;
    List<String> devStack;

    public MessageStack(List<String> userStack, List<String> implStack, List<String> devStack) {
        this.userStack = userStack;
        this.implStack = implStack;
        this.devStack = devStack;
    }

    public MessageStack() {
    }

    /*public String toString() {
        return userStack;
    }*/

    public List<String> getUserStack() {
        return userStack;
    }

    public List<String> getImplStack() {
        return implStack;
    }

    public List<String> getDevStack() {
        return devStack;
    }

    public void setUserStack(List<String> userStack) {
        this.userStack = userStack;
    }

    public void setImplStack(List<String> implStack) {
        this.implStack = implStack;
    }

    public void setDevStack(List<String> devStack) {
        this.devStack = devStack;
    }

    public String getListAsString(List<String> textList) {
        StringBuffer txt = new StringBuffer();
        for (String text : textList) {
            txt.append(text).append(System.getProperty("line.separator"));
        }
        return txt.toString();
    }
}