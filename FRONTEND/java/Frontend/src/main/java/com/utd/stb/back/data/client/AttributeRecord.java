package com.utd.stb.back.data.client;

/**
 * Wraps database struct data. Has appropriate get/set methods for data from db
 * struct STB$SELECTION$RECORD
 *
 * @author rainer bruns Created 27.08.2004
 */
public class AttributeRecord {

    StructMap attributeData;

    /**
     * @param s the StructMap with the data for this Record
     */
    public AttributeRecord(StructMap s) {

        attributeData = s;
    }

    /**
     * @return the value of underlying field SVALUE
     * @see StructMap#getValueAsString(String)
     */
    public String getValue() {

        return attributeData.getValueAsString("SVALUE");
    }

    /**
     * @return the value of underlying field SPARAMETER
     * @see StructMap#getValueAsString(String)
     */
    public String getParameter() {

        return attributeData.getValueAsString("SPARAMETER");
    }

    /**
     * @return the value of underlying field SINFO
     * @see StructMap#getValueAsString(String)
     */
    public String getPrepresent() {

        return attributeData.getValueAsString("SPREPRESENT");
    }

    /**
     * @return the value of underlying field BBLOBVALUE
     * @see StructMap#getValueAsString(String)
     */

    public byte[] getBlobValueAsBytes() {

        return attributeData.getBlobValueAsBytes("BBLOBVALUE");
    }

    public Object getBlobValue() {

        return attributeData.getValue("BBLOBVALUE");
    }

}

