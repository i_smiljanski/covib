package com.utd.stb.back.ui.impl.userinputs.fields;

import com.utd.stb.back.data.client.PropertyRecord;
import com.utd.stb.inter.userinput.MetaInput;
import com.utd.stb.inter.userinput.MetaInputFactory;


/**
 * Implementation of UserInput for boolean fields
 *
 * @author rainer bruns Created 02.12.2004
 */
public class TabSheetUserInput extends DirectUserInput {

    PropertyRecord property;

    /**
     * @param label    field Label
     * @param value    field Value
     * @param property complete record of field information from database
     */
    public TabSheetUserInput(String label, PropertyRecord property) {

        super(label, MetaInput.class, MetaInputFactory.getTabSheet());
        this.property = property;
    }

    /**
     * Is used when saving field to db
     *
     * @return the record to be stored to db
     * @see AbstractInputData#getListToSave()
     */
    public PropertyRecord getProperty() {

        return property;
    }
}