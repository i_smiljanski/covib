/*
 * Created on 21.12.2005
 *
 */
package com.utd.stb;

import com.utd.stb.back.database.Dispatcher;
import com.utd.stb.inter.application.BackendException;
import org.apache.commons.lang.StringUtils;

import java.util.HashMap;
import java.util.Map;


/**
 * @author marie.dort
 */
public final class StabberInfo {

    public static Dispatcher dispatcher;

    final public static String frontendVersion = "1.0.1";
    final public static String loaderVersion = "1.0.0";

    private static String dbLocale;
    private static String dbDatePattern;

    static Map<String, String> applicationInfo = new HashMap<>();

    public static String getApplicationInfo(String sParameterName) {
        String sParametervalue = "";
        if (!applicationInfo.containsKey(sParameterName)) {
            if (dispatcher != null) {
                sParametervalue = dispatcher.getParameterValue(sParameterName);
                if (sParametervalue == null || sParametervalue.length() == 0)
                    sParametervalue = "";
            } else {
                sParametervalue = "";
            }
            applicationInfo.put(sParameterName, sParametervalue);
        }
        return applicationInfo.get(sParameterName);
    }

    public static String getLocale() {
        if (!StringUtils.isNotEmpty(dbLocale))
            dbLocale = getApplicationInfo("NLS_LANGUAGE");
        return dbLocale;
    }

    public static String getTranslation(String msg) {
        String sTransl = "";
        if (dispatcher != null) {
            try {
                sTransl = dispatcher.getTranslation(msg);
            } catch (BackendException e) {
                e.printStackTrace();
            }
            if (sTransl == null || sTransl.length() == 0)
                sTransl = "";
        } else {
            sTransl = "";
        }
        return sTransl;
    }

    public static String getDatePattern() {
        if (!StringUtils.isNotEmpty(dbDatePattern))
            dbDatePattern = getApplicationInfo("JAVA_DATE_FORMAT");
        return dbDatePattern;
    }
}