package com.utd.stb.gui.itemTools;

import com.utd.stb.inter.items.DisplayableItem;
import com.utd.stb.inter.items.DisplayableItems;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Iterator;
import java.util.List;


/**
 * <b>Modifiable </b> Version, changes in the backing list will be visible
 * outside, but it is very lite, no copied data
 *
 * @author PeterBuettner.de
 */
public class DisplayableItemsWrap implements DisplayableItems {

    protected List data;

    /**
     * Empty List
     */
    public DisplayableItemsWrap() {

        this(new ArrayList(0));
    }

    /**
     * no copy made!
     *
     * @param data
     */
    public DisplayableItemsWrap(List data) {

        this.data = data;
    }

    /**
     * no copy made!
     *
     * @param items
     */
    public DisplayableItemsWrap(DisplayableItem[] items) {

        data = Arrays.asList(items);
    }

    public int getSize() {

        return data.size();
    }

    public DisplayableItem get(int index) {

        return (DisplayableItem) data.get(index);
    }

    public int indexOf(DisplayableItem item) {

        return data.indexOf(item);
    }

    public Iterator iterator() {

        return data.iterator();
    }

    public DisplayableItem[] toArray() {

        return (DisplayableItem[]) data.toArray(new DisplayableItem[0]);

    }

    /* (non-Javadoc)
     * @see com.utd.stb.inter.items.DisplayableItems#getAttributeListColNames()
     */
    public String getAttributeListColNames() {

        return null;
    }

    /* (non-Javadoc)
     * @see com.utd.stb.inter.items.DisplayableItems#getAttributeListColCount()
     */
    public int getAttributeListColCount() {

        return 0;
    }

}