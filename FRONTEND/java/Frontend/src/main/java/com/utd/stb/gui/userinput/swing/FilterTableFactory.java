package com.utd.stb.gui.userinput.swing;

import com.jidesoft.comparator.ObjectComparatorManager;
import com.jidesoft.converter.ObjectConverterManager;
import com.jidesoft.dialog.JideOptionPane;
import com.jidesoft.filter.EqualFilter;
import com.jidesoft.filter.Filter;
import com.jidesoft.grid.*;
import com.jidesoft.swing.JideButton;
import com.jidesoft.swing.JideComboBox;
import com.jidesoft.swing.JideSwingUtilities;
import com.uptodata.isr.gui.util.Colors;
import com.utd.stb.back.data.client.SelectionList;
import com.utd.stb.back.ui.impl.msgboxes.WarningMsgBox;
import com.utd.stb.back.ui.impl.userinputs.data.ContextDefaultTableModel;
import com.utd.stb.back.ui.impl.userinputs.data.FilterTableData;
import com.utd.stb.gui.swingApp.UserDialogTool;
import com.utd.stb.inter.application.BackendException;
import com.utd.stb.inter.dialog.UserDialog;
import org.apache.commons.lang.StringUtils;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import resources.IconSource;
import resources.Res;

import javax.swing.*;
import javax.swing.event.ChangeEvent;
import javax.swing.plaf.basic.BasicComboBoxRenderer;
import javax.swing.table.DefaultTableCellRenderer;
import javax.swing.table.TableColumn;
import javax.swing.table.TableColumnModel;
import javax.swing.table.TableModel;
import java.awt.*;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.util.*;
import java.util.List;

/**
 * Creates a grid table
 *
 * @author Inna Smiljanski
 * Date: 24.02.11
 */
public class FilterTableFactory {
    public static final Logger log = LogManager.getRootLogger();
    CalculatedTableModel _tableModel;
    FilterableTableModel _filterableTableModel;

    //a _table which uses SortableTableModel so that multiple columns can be sorted
    SortableTable _table;
    Window owner;
    AutoFilterTableHeader _header;
    JideButton button = new JideButton("");
    List<Object> _buttons;
    JPanel _panel;
    ContextDefaultTableModel dataTableModel;
    // boolean _isValid;

    /**
     * Creates a _panel containing a grid table with data tableData
     *
     * @param tableData UserInputs
     * @param buttons   Actions
     * @param owner     UserInputDialog
     * @return a _panel
     */
    public JComponent createPanel(FilterTableData tableData, List<Object> buttons, Window owner) {
        log.info("begin");
        _buttons = buttons;
        this.owner = owner;
        _panel = new JPanel(new BorderLayout(12, 12));

        initDefaultValues();

        JFrame frame = new JFrame();
        JPanel tablePanel = new JPanel(new BorderLayout(6, 6));
        frame.add(tablePanel);

        GridState options = tableData.getOptions();
        options.setFilterTableFactory(this);

        options.getWindowState().putWindowStateListener(owner);

        //data from db
        dataTableModel = (ContextDefaultTableModel) tableData.getTableModel();

        if (dataTableModel == null) {
            return _panel;
        }

        _table = new SortableTable();
        _table.setSurrendersFocusOnKeystroke(true);
        _filterableTableModel = createTableModel(options);
        _table.setModel(_filterableTableModel);

        setColumnsWidth(options.tablePref);
        registerEditorsAndRenderer();
        editFirstColumn();

        addTableListeners(frame);
        _table.setColumnAutoResizable(true);

        log.debug("doTable end" + _table.getModel().getRowCount() + " " + _filterableTableModel.getColumnCount());

        _header = createHeader();

        TableUtils.setSortableTablePreference(_table, options.sortingOrders, true);
        TableUtils.setTablePreferenceByName(_table, options.tablePref);

//        log.debug("isAutoResort:" + _table.isAutoResort() + " tablePref=" + options.tablePref);
        _table.setTableHeader(_header);

        //column auto resizable
        _table.setColumnAutoResizable(true);

        TableHeaderPopupMenuInstaller installer = new TableHeaderPopupMenuInstaller(_table);
        installer.addTableHeaderPopupMenuCustomizer(new AutoResizePopupMenuCustomizer());
        installer.addTableHeaderPopupMenuCustomizer(new TableColumnChooserPopupMenuCustomizer());

        JScrollPane scrollPane = buildScrollPane(_table, dataTableModel);

        tablePanel.add(scrollPane);
        _panel.add(tablePanel);


        options.getWindowState().restore(this.owner);
        this.owner.setPreferredSize(options.getWindowState().getBounds().getSize());

//        log.debug(this.owner.getBounds());

        button.addActionListener(e -> {
            _table.requestFocus();
            _table.editCellAt(0, 2);
        });
        button.setPreferredSize(new Dimension(10, 0));
        ((ContextDefaultTableModel) tableData.getTableModel()).setButton(button);
        _panel.add(button, BorderLayout.AFTER_LAST_LINE);
        log.info("end");
        return _panel;
    }


    private void initDefaultValues() {
        CellEditorManager.initDefaultEditor();
        CellRendererManager.initDefaultRenderer();
        ObjectConverterManager.initDefaultConverter();
        ObjectComparatorManager.initDefaultComparator();
    }

    private AutoFilterTableHeader createHeader() {
        log.info("begin");
        //filters
        AutoFilterTableHeader header = new AutoFilterTableHeader(_table) {
            @Override
            protected IFilterableTableModel createFilterableTableModel(TableModel model) {
//                log.debug("model.getRowCount()=" + model.getRowCount() + "; " + _filterableTableModel);
                return _filterableTableModel;
            }
        };

        header.setAutoFilterEnabled(true);
        header.setShowFilterNameAsToolTip(true);
        header.setShowFilterIcon(true);
        log.info("end");
        return header;
    }


    /**
     * table model which wraps another table model so that you can apply filters on it
     *
     * @param dataTableModel DefaultTableModel with data
     * @return table model
     */
    public FilterableTableModel createTableModel(GridState options) {
        log.info("begin ");
        _tableModel = new CalculatedTableModel(dataTableModel);

        _tableModel.addAllColumns();

//        log.debug(_tableModel.getRowCount() + " " + _tableModel.getCalculatedColumnAt(0));

        FilterableTableModel filterableTableModel = new FilterableTableModel(_tableModel) {
            @Override
            public boolean isColumnFilterable(int i) {
                return super.isColumnFilterable(i);
            }

            @Override
            public boolean isColumnAutoFilterable(int i) {
                if ("FILE".equalsIgnoreCase(dataTableModel.getColumnType(i))) {
                    ((SortableTableModel) _table.getModel()).setColumnSortable(i, false);
                    return false;
                }
                return super.isColumnAutoFilterable(i);
            }

        };

        //todo? andere filter
        filterableTableModel.clearFilters();
        List<String> filterList = options.filterList;
        List<String> filterColumnList = options.filterColumnList;
        List<String> filterValueList = options.filterValueList;

        if (filterList != null && filterColumnList != null && filterValueList != null) {
            log.debug(filterList.size());
            for (int i = 0; i < filterList.size(); i++) {
                String filterName = filterList.get(i);
                Filter filter = JideFilters.getFilter(filterName, filterValueList.get(i));
//                log.debug(filter + "; " + filterColumnList.get(i));
                if (filter instanceof SingleValueFilter)
                    filterableTableModel.addFilter(Integer.parseInt(filterColumnList.get(i)), filter);
            }
        }

        filterableTableModel.setFiltersApplied(true);
        log.info("end");
        return filterableTableModel;
    }


    private void addTableListeners(JFrame frame) {
        log.info("begin");
        _table.addCellEditorListener(new CellEditorListener(dataTableModel, frame));


        //listener to create context menu with add, delete and duplicate row items
        _table.addMouseListener(new MouseAdapter() {
            private void event(MouseEvent e) {
                if (e.isPopupTrigger() && _table.isEnabled()) {
                    Point p = new Point(e.getX(), e.getY());
                    int col = _table.columnAtPoint(p);
                    int row = _table.rowAtPoint(p);

//                    log.debug(((CalculatedTableModel)((FilterableTableModel)((SortableTableModel)_table.getModel()).
//                            getActualModel()).getActualModel()).getActualModel());

                    // translate table index to model index
//                    int mcol = _table.getColumn(_table.getColumnName(col)).getModelIndex();

                    if (row >= 0 && row < _table.getRowCount()) {
                        cancelCellEditing(_table);

                        // create popup menu...
                        JPopupMenu contextMenu = createRowContextMenu(row, col, dataTableModel);


                        // ... and show it
                        if (contextMenu != null && contextMenu.getComponentCount() > 0) {
                            try {
                                contextMenu.show(_table, p.x, p.y);
                            } catch (Exception ignore) {
                            }
                        }
                    }
                }
            }

            @Override
            public void mousePressed(MouseEvent e) {
                event(e);
            }

            @Override
            public void mouseReleased(MouseEvent e) {
                event(e);
            }
        });
        log.info("end");
    }

    /**
     * sets each second line gray
     *
     * @param row       row number
     * @param component TableCellRendererComponent
     */
    private void setRowColor(int row, Component component) {
        if (row % 2 == 0) {
            component.setBackground(Colors.getUIControl());
        } else
            component.setBackground(Colors.getUIControlTableBackground());
        component.setForeground(UIManager.getColor("Table.foreground"));
    }


    /**
     * "save" button enabled or disabled
     *
     * @param enabled true or false
     */
    private void setButtonEnabled(boolean enabled) {
        _buttons.stream().filter(aButton -> aButton != null).forEach(aButton -> {
            ((Action) aButton).setEnabled(enabled);
        });
    }

    /**
     * context menu in rows
     *
     * @param row            current row number
     * @param dataTableModel data model
     * @return menu
     * @throws BackendException
     */
    private JPopupMenu createRowContextMenu(int row, final int col,
                                            ContextDefaultTableModel dataTableModel) {
        log.info("begin");
        final JPopupMenu contextMenu = new JPopupMenu();

        final ListSelectionModel selectionModel = _table.getSelectionModel();
        selectionModel.setSelectionInterval(row, row);

        final int rownum = dataTableModel.getRowCount();
        final int[] actualRow = {TableModelWrapperUtils.getActualRowAt(_table.getModel(), row)};

        log.debug("actualRow " + actualRow[0] + "; row=" + row + " filter:" + _header.getFilterableTableModel().hasFilter() + "; " + rownum + " " + _table.getRowCount());

        JMenuItem addMenu = new JMenuItem();
        addMenu.setText(Res.getString("ADD_ROW"));
        boolean isEnabled = false;

        if (dataTableModel.isInsertAllowed()) {
            addMenu.addActionListener(e -> {
                        Vector<String> newRowVector = new Vector<>();
                        for (int i = 0; i < _table.getColumnModel().getColumnCount(); i++) {
                            newRowVector.add(dataTableModel.getCellDefaultValue(i));
                        }
                        log.debug(rownum - actualRow[0]);

                        if (actualRow[0] < 0) actualRow[0] = 0;

                        dataTableModel.insertRow(actualRow[0], newRowVector);

                        log.debug(TableModelWrapperUtils.getRowAt(_table.getModel(), actualRow[0]));

                        _table.scrollRowToVisible(TableModelWrapperUtils.getRowAt(_table.getModel(), actualRow[0]));
                        selectionModel.clearSelection();
                        selectionModel.setSelectionInterval(TableModelWrapperUtils.getRowAt(_table.getModel(), actualRow[0]),
                                TableModelWrapperUtils.getRowAt(_table.getModel(), actualRow[0]));
                    }
            );
            isEnabled = true;
        }
        log.debug("hasFilter:" + _header.getFilterableTableModel().hasFilter());
        addMenu.setEnabled(isEnabled && !_header.getFilterableTableModel().hasFilter());
        contextMenu.add(addMenu);

        if (rownum != 0) {
            isEnabled = false;
            JMenuItem deleteMenu = new JMenuItem();
            deleteMenu.setText(Res.getString("DELETE_ROW"));
            if (dataTableModel.isDeleteAllowed(actualRow[0])) {
                deleteMenu.addActionListener(e -> {
                    dataTableModel.removeRow(actualRow[0]);
                    selectionModel.clearSelection();
                });
                isEnabled = true;
            }
            deleteMenu.setEnabled(isEnabled);
            contextMenu.add(deleteMenu);

            isEnabled = false;
            JMenuItem duplicateRowMenu = new JMenuItem();
            duplicateRowMenu.setText(Res.getString("DUPLICATE_ROW"));
            if (dataTableModel.isInsertAllowed()) {
                duplicateRowMenu.addActionListener(e -> {
                    button.doClick();
                    //   int actualRow = TableModelWrapperUtils.getActualRowAt(_table.getModel(), row);
                    Vector<Object> duplicateRowVector = new Vector<>();
                    for (int i = 0; i < _table.getColumnModel().getColumnCount(); i++) {
                        if (i == 1)
                            duplicateRowVector.add("");
                        else {
                            String colIdentifier = (String) _table.getColumnModel().getColumn(i).getIdentifier();
                            HashMap<String, Object> colMap = dataTableModel.getColumnMap(colIdentifier);
                            if (colMap != null && "DUPLICATED_ROWID".equalsIgnoreCase((String) colMap.get("entity"))) {
                                duplicateRowVector.add(dataTableModel.getValueAt(actualRow[0], 1));
                            } else {
                                duplicateRowVector.add(dataTableModel.getValueAt(actualRow[0], i));
                            }
                        }
                    }
                    dataTableModel.insertRow(actualRow[0], duplicateRowVector);
                    log.debug("duplicateRow: " + duplicateRowVector + " " + row);

                    _table.scrollRowToVisible(TableModelWrapperUtils.getRowAt(_table.getModel(), actualRow[0]));
                    selectionModel.clearSelection();
                    selectionModel.setSelectionInterval(TableModelWrapperUtils.getRowAt(_table.getModel(), actualRow[0]),
                            TableModelWrapperUtils.getRowAt(_table.getModel(), actualRow[0]));
                });
                isEnabled = true;
            }
            duplicateRowMenu.setEnabled(isEnabled);
            contextMenu.add(duplicateRowMenu);
        }

//        System.out.println("dataTableModel.getColumnClass() = " + dataTableModel.getColumnClass(col));
        if (dataTableModel.getColumnClass(col).equals(String.class)) {
            JMenuItem editorRowMenu = new JMenuItem();
            editorRowMenu.setText(Res.getString("POPUP_EDITOR"));

            editorRowMenu.addActionListener(e -> {
                String[] options = {
                        "Save",
                        "Cancel"
                };
                Object[] message = new Object[1];
                JTextArea textArea = new JTextArea();
                textArea.setPreferredSize(new Dimension(400, 300));
                message[0] = textArea;
                textArea.setText((String) dataTableModel.getValueAt(actualRow[0], col));
                int result = JideOptionPane.showOptionDialog(
                        contextMenu,                             // the parent that the dialog blocks
                        message,                                    // the dialog message array
                        "Grid Popup Editor",                 // the title of the dialog window
                        JOptionPane.OK_CANCEL_OPTION,                 // option type
                        JOptionPane.PLAIN_MESSAGE,            // message type
                        null,                                       // optional icon, use null to use the default icon
                        options,                                    // options string array, will be made into buttons
                        options[1]                                  // option that should be made into a default button
                );
                if (result == JOptionPane.YES_OPTION) {
                    dataTableModel.setValueAt(((JTextArea) message[0]).getText(), actualRow[0], col);
                }
            });
            isEnabled = true;

            editorRowMenu.setEnabled(isEnabled);
            //contextMenu.add(editorRowMenu);
        }
        log.info("end");
        return contextMenu;
    }


    private void cancelCellEditing(JTable table) {
        CellEditor ce = table.getCellEditor();
        if (ce != null) {
            ce.cancelCellEditing();
        }
    }

    private void registerEditorsAndRenderer() {
        log.info("begin");
        List<Class> columnClasses = new ArrayList<>();
        for (int col = 1; col < _table.getColumnModel().getColumnCount(); col++) {
            setLovCellEditor(dataTableModel, col);
            columnClasses.add(_table.getColumnClass(col));
        }
        for (Class cl : columnClasses) {
            CellRendererManager.registerRenderer(cl, new DefaultTableCellRenderer() {
                @Override
                public Component getTableCellRendererComponent(JTable table,
                                                               Object value,
                                                               boolean isSelected,
                                                               boolean hasFocus,
                                                               int row, int column) {
                    Component component = super.getTableCellRendererComponent(
                            table, value, isSelected, hasFocus, row, column);
                    setRowColor(row, component);
                    return component;
                }
            });
        }
        registerBoolean();
        registerNumber();
        registerBytes();
        registerPassword();
        log.info("end");
    }

    /**
     * editor for lovs
     *
     * @param dataTableModel data model
     * @param col            column number
     */
    private void setLovCellEditor(ContextDefaultTableModel dataTableModel, int col) {
//        log.info("begin");

        TableColumnModel columnModel = _table.getColumnModel();
        String columnIdentifier = columnModel.getColumn(col).getIdentifier().toString();
        HashMap<String, Object> columnMap = dataTableModel.getColumnMap(columnIdentifier);
        String lov = (String) columnMap.get("lov");
        if (!"F".equals(lov)) {

            Vector<Object> lovDataVector = new Vector<>();
            Vector<String> lovInfoVector = new Vector<>();  //tooltip
            fillLovVectors(columnMap, lovDataVector, lovInfoVector);
            JideComboBox comboBox = new JideComboBox(lovDataVector);
//            comboBox.setModel(new DefaultComboBoxModel(lovDataVector));
            log.debug(col + " " + dataTableModel.isLovEditable(col));
            comboBox.setEditable(dataTableModel.isLovEditable(col));

            comboBox.setRenderer(getLovCellRenderer(lovInfoVector));
            columnModel.getColumn(col).setCellEditor(new DefaultCellEditor(comboBox));

        }
//        log.info("end");
    }

    private void fillLovVectors(HashMap<String, Object> columnMap, Vector<Object> lovDataVector, Vector<String> lovInfoVector) {
        SelectionList selectionList;
        String entity = (String) columnMap.get("entity");
        try {
            selectionList = dataTableModel.getDispatcher().getLov(entity, "");
            columnMap.put("selectionList", selectionList);
            for (int i = 0; i < selectionList.getSize(); i++) {
                lovDataVector.add(selectionList.get(i).getData());
                lovInfoVector.add(selectionList.get(i).getInfo() + "_" + selectionList.get(i).getMasterKey());
            }
        } catch (BackendException e1) {
            log.error(e1);
        }
    }

    private ListCellRenderer getLovCellRenderer(Vector<String> lovInfoVector) {
        return new BasicComboBoxRenderer() {
            public Component getListCellRendererComponent(JList list, Object value,
                                                          int index, boolean isSelected, boolean cellHasFocus) {
                if (isSelected) {
                    setBackground(list.getSelectionBackground());
                    setForeground(list.getSelectionForeground());
                    if (-1 < index) {
                        list.setToolTipText(lovInfoVector.get(index));
                    }
                } else {
                    setBackground(list.getBackground());
                    setForeground(list.getForeground());
                }
                setFont(list.getFont());
                setText((value == null) ? "" : value.toString());
                return this;
            }
        };
    }

    private void registerBoolean() {
        CellRendererManager.registerRenderer(Boolean.class, new BooleanCheckBoxCellRenderer() {
                    @Override
                    public Component getTableCellRendererComponent(JTable table, Object value, boolean isSelected,
                                                                   boolean hasFocus, int row, int column) {
                        Component component = super.getTableCellRendererComponent(table, value, isSelected, hasFocus, row, column);
                        setRowColor(row, component);
                        return component;
                    }
                }
        );
        CellEditorManager.registerEditor(Boolean.class, BooleanCheckBoxCellEditor::new);
    }

    private void registerNumber() {

        CellRendererManager.registerRenderer(Number.class, new NumberCellRenderer() {
            @Override
            public Component getTableCellRendererComponent(JTable table, Object value,
                                                           boolean isSelected, boolean hasFocus, int row, int column) {
                Component component = super.getTableCellRendererComponent(table, value, isSelected, hasFocus, row, column);
                setRowColor(row, component);
                return component;
            }
        });
        CellEditorManager.registerEditor(Number.class, NumberCellEditor::new);
    }


    private void registerPassword() {

        CellRendererManager.registerRenderer(char[].class, new PasswordCellRenderer() {
            @Override
            public Component getTableCellRendererComponent(JTable table, Object value,
                                                           boolean isSelected, boolean hasFocus, int row, int column) {
                Component component = super.getTableCellRendererComponent(table, value, isSelected, hasFocus, row, column);
                setRowColor(row, component);
                return component;
            }
        });
        CellEditorManager.registerEditor(char[].class, PasswordCellEditor::new);
    }


    private void registerBytes() {
        FileCellRenderer fileCellRenderer = new FileCellRenderer(_panel, dataTableModel) {
            @Override
            public Component getTableCellRendererComponent(JTable table, Object value,
                                                           boolean isSelected, boolean hasFocus, int row, int column) {
                Component component = super.getTableCellRendererComponent(table, value, isSelected, hasFocus, row, column);
                setRowColor(row, component);
                return component;
            }
        };
        CellRendererManager.registerRenderer(byte[].class, fileCellRenderer);

        FileCellRenderer fileCellEditor = new FileCellRenderer(_panel, dataTableModel);
        CellEditorManager.registerEditor(byte[].class, () -> fileCellEditor);
    }


    private void editFirstColumn() {
        log.info("begin");
        TableColumn bColumn = _table.getColumn(dataTableModel.getColumnName(0));
        //  bColumn.setPreferredWidth(25);
        bColumn.setMaxWidth(25);
        bColumn.setResizable(false);

        bColumn.setCellRenderer(new SortTableHeaderRenderer());
        ((SortableTableModel) _table.getModel()).setColumnSortable(0, false);

        log.info("end");
    }


    private void setColumnsWidth(String tablePref) {
        log.info("begin");
        TableColumnModel columnModel = _table.getColumnModel();
        //hide invisible columns
        for (int i = 1; i < columnModel.getColumnCount(); i++) {
            //  log.debug(i + " " + columnModel.getColumn(i).getIdentifier() + " " + dataTableModel.getAllColumnsMap().get(i).get("displayed"));
            if (!"T".equals(dataTableModel.getAllColumnsMap().get(columnModel.getColumn(i).getIdentifier().toString()).get("displayed"))) {
                columnModel.getColumn(i).setMinWidth(0);
                columnModel.getColumn(i).setMaxWidth(0);
            } else if (tablePref != null && tablePref.contains(i + "\t0\t0\t0\t0")) {
                columnModel.getColumn(i).setMinWidth(200);
                columnModel.getColumn(i).setMaxWidth(200);
                columnModel.getColumn(i).setResizable(true);
            }
        }

        log.info("end");
    }


    private JScrollPane buildScrollPane(SortableTable table, ContextDefaultTableModel dataTableModel) {
        log.info("begin");
        JScrollPane scrollPane = new JScrollPane(table);

        scrollPane.addMouseListener(new MouseAdapter() {
            @Override
            public void mouseReleased(MouseEvent e) {
                event(e);
            }

            @Override
            public void mousePressed(MouseEvent e) {
                event(e);
            }

            private void event(MouseEvent e) {
//                    log.debug(e + " " + _table.getModel().getRowCount());
                if (table.getModel().getRowCount() == 0 && e.isPopupTrigger()) {
                    Point p = new Point(e.getX(), e.getY());

                    // create popup menu...
                    JPopupMenu contextMenu = createRowContextMenu(0, 0, dataTableModel);


                    // ... and show it
                    if (contextMenu != null && contextMenu.getComponentCount() > 0) {
                        try {
                            contextMenu.show(table, p.x, p.y);
                        } catch (Exception ignored) {
                            log.error(ignored);
                        }
                    }
                }
            }
        });
        log.info("end");
        return scrollPane;
    }

    public Window getOwner() {
        return owner;
    }

    public void setOwner(Window owner) {
        this.owner = owner;
    }


    class CellEditorListener implements JideCellEditorListener {
        boolean isValueValid = true;
        boolean isMessage = false;
        ContextDefaultTableModel dataTableModel;
        JFrame frame;
        Map<Object, String> valuePaar = new HashMap<>();

        CellEditorListener(ContextDefaultTableModel dataTableModel, JFrame frame) {
            this.dataTableModel = dataTableModel;
            this.frame = frame;
        }

        public boolean editingStarting(ChangeEvent changeEvent) {
            return true;
        }

        public void editingStarted(ChangeEvent changeEvent) {
//            log.info("begin");

            //setButtonEnabled(_isValid);
            int col = ((CellChangeEvent) changeEvent).getColumn();
            int row = ((CellChangeEvent) changeEvent).getRow();
            log.debug("editingStarted begin:" + row + " " + dataTableModel.getValueAt(row, 1) + " " + isValueValid);

            setButtonEnabled(isValueValid);
            TableColumnModel columnModel = _table.getColumnModel();
            String colIdentifier = columnModel.getColumn(col).getIdentifier().toString();

//            if (dataTableModel.getColumnType(colIdentifier).equalsIgnoreCase("FILE")) {
//                editFileColumn(colIdentifier, row, changeEvent.getSource());
//            }
            if (dataTableModel.isLovColumn(colIdentifier)) {
                editLovColumn(colIdentifier, row, changeEvent.getSource());
            }
            // }
            isMessage = false;
            log.info("end " + row);
        }


        public boolean editingStopping(ChangeEvent e) {
            int col = ((CellChangeEvent) e).getColumn();
            int row = ((CellChangeEvent) e).getRow();
            isValueValid = true;
            int actualRow = TableModelWrapperUtils.getRowAt(_table.getModel(), row);
            int actualCol = TableModelWrapperUtils.getActualColumnAt(_table.getModel(), col);

            log.debug("editingStopping " + col + " " + actualCol + " " + dataTableModel.getValueAt(row, 1) + " " +
                    " " + ((CellEditor) e.getSource()).getCellEditorValue() + " " +
                    (actualRow > -1 ? dataTableModel.getValueAt(actualRow, 1) : ""));

            Object value = ((CellEditor) e.getSource()).getCellEditorValue();

            TableColumnModel columnModel = _table.getColumnModel();
            String identifier = (String) columnModel.getColumn(actualCol).getIdentifier();
            if (dataTableModel.isLovColumn(identifier)) { //hack!! COVIB-107, is 09.Jan.2020
                String lovHack = valuePaar.get(value);
                if (StringUtils.isNotEmpty(lovHack)) {
                    log.debug(lovHack + "  " + (actualCol + 1));
                    log.debug(row + "; " + actualRow + "; " + TableModelWrapperUtils.getActualRowAt(_table.getModel(), row));
                    dataTableModel.setValueAt(lovHack, TableModelWrapperUtils.getActualRowAt(_table.getModel(), row), actualCol + 1);
                }
            }
            try {
                if (!dataTableModel.validateNotNull(value, identifier)) {
                    isValueValid = false;
                }
               /* if (!dataTableModel.validateConstraint(value,
                        TableModelWrapperUtils.getActualRowAt(_table.getModel(), row), identifier, -1)) {
                    isValueValid = false;
                }*/  // doesn't work for more keys
                if (!dataTableModel.validateNumber(value, identifier)) {
                    isValueValid = false;
                }
                //field length

                if (!dataTableModel.validateFormat(value,
                        TableModelWrapperUtils.getActualRowAt(_table.getModel(), row), identifier)) {
                    isValueValid = false;
                }


                //_isValid = isValueValid[0];

                log.debug("isValid: " + isValueValid);
                setButtonEnabled(isValueValid);

                if (!isValueValid
                        && !isMessage
                ) {
                    UserDialog dialog = new WarningMsgBox(dataTableModel.getTranslation("exceptionDialog.window.title"),
                            dataTableModel.message);
                    UserDialogTool.executeUserDialog(frame, dialog);
                    isMessage = true;
                }
            } catch (BackendException e2) {
                log.error(e2);

            }

            setButtonEnabled(isValueValid);
            return isValueValid;
        }

        public void editingStopped(ChangeEvent e) {
//            log.debug("editingStopped ");
        }

        public void editingCanceled(ChangeEvent e) {
            log.debug("editingCanceled ");
        }


        private void editLovColumn(String colIdentifier, int row, Object editor) {
            log.info("begin");
            HashMap<String, Object> columnMap = dataTableModel.getColumnMap(colIdentifier);
            int actualRow = TableModelWrapperUtils.getActualRowAt(_table.getModel(), row);
            String rowId = (String) dataTableModel.getValueAt(actualRow, 1);

            if (editor instanceof DefaultCellEditor) {
                DefaultCellEditor lovCellEditor = (DefaultCellEditor) editor;
                if (!(lovCellEditor.getComponent() instanceof JComboBox)) {
                    return;
                }
                JComboBox comboBox = ((JComboBox) lovCellEditor.getComponent());
                //  log.debug("editingStarted " + col + " " + row + " " + cellEditor.getCellEditorValue());

                final Vector<Object> nLovDataVector = new Vector<>();

                if ("N".equals(columnMap.get("lov")) && StringUtils.isNotEmpty(rowId)) {
                    nLovDataVector.add(dataTableModel.getValueAt(actualRow, dataTableModel.getColumnNumber(colIdentifier)));
                } else {
                    try {
                        SelectionList selectionList = dataTableModel.getDispatcher().
                                getLov((String) columnMap.get("entity"), "");
                        for (int i = 0; i < selectionList.getSize(); i++) {
                            nLovDataVector.add(selectionList.get(i).getData());
                            if (dataTableModel.isLovColumn(colIdentifier)) { //hack!
                                valuePaar.put(selectionList.get(i).getData(), selectionList.get(i).getMasterKey());
                            }
                        }
                    } catch (BackendException e1) {
                        log.error(e1);
                    }
                }
                comboBox.setModel(new DefaultComboBoxModel<>(nLovDataVector));
             /* String parent = (String) columnMap.get("parent");

                if (!"F".equals(columnMap.get("lov")) && StringUtils.isNotEmpty(parent)) {

                    final Vector<Object> lovDataVector = new Vector<>();
                    String lovEntity = (String) dataTableModel.getValueAt(actualRow, dataTableModel.findColumn(parent));
                    log.debug(lovEntity);
                    if (StringUtils.isNotEmpty(lovEntity)) {
                        try {
                            SelectionList selectionList = dataTableModel.getDispatcher().getLov("VALUE_" + lovEntity, "");

                            for (int i = 0; i < selectionList.getSize(); i++) {
                                lovDataVector.add(selectionList.get(i).getData());
                            }
                        } catch (BackendException e1) {
                            log.error(e1);
                        }
                        comboBox.setModel(new DefaultComboBoxModel<>(lovDataVector));
                    }
                }*/
            }
            log.info("end");
        }
    }


    /**
     * enum with jide filters
     */
    protected enum JideFilters {
        EQUAL("EqualFilter", EqualFilter.class),
        SINGLEVALUE("SingleValueFilter", SingleValueFilter.class)
//        ,NOTEQUAL("NotEqualFilter", NotEqualFilter.class),
//        BETWEEN("BetweenFilter", BetweenFilter.class),
//        AND("AndFilter", AndFilter.class),
//        EMPTY("EmptyFilter", EmptyFilter.class),
//        GRATEROREQUAL("GreaterOrEqualFilter", GreaterOrEqualFilter.class),
//        GREATERTHAN("GreaterThanFilter", GreaterThanFilter.class),
//        OR("OrFilter", OrFilter.class),
//        WILDCARD("WildcardFilter", WildcardFilter.class)
        ;

        private String name;
        private Class clazz;

        private JideFilters(String name, Class clazz) {
            this.name = name;
            this.clazz = clazz;
        }

        public static String getName(Class clazz) {
//            log.debug(clazz);
            for (JideFilters f : JideFilters.values()) {
                if (f.clazz.equals(clazz))
                    return f.name;
            }
            return clazz.toString();
        }

        public static Class getClazz(String name) {
            for (JideFilters f : JideFilters.values()) {
                if (f.name.equals(name))
                    return f.clazz;
            }
            return null;
        }

        public static Object getFilterValue(String name, Filter filter) {
            if (EQUAL.name.equals(name))
                return ((EqualFilter) filter).getValue();
//            else if (BETWEEN.name.equals(name))
//                return ((BetweenFilter) filter).getValue1() + ";filter2=" +
//                        ((BetweenFilter) filter).getValue2() + ";operator=" +
//                        ((BetweenFilter) filter).getOperator();
//            else if (WILDCARD.name.equals(name)) {
//                return ((WildcardFilter) filter).getPattern();
//            }
            else if (SINGLEVALUE.name.equals(name)) {
                return ((SingleValueFilter) filter).getValue();
            }
            return null;
        }

        public static Filter getFilter(String name, String value) {
            Filter filter = null;
            if (EQUAL.name.equals(name)) {
                filter = new EqualFilter();
                ((EqualFilter) filter).setValue(value);
//            }
//            else if (WILDCARD.name.equals(name)) {
//                filter = new WildcardFilter("*" + value + "*");
//            } else if (NOTEQUAL.name.equals(name)) {
//                filter = new NotEqualFilter();
//                ((NotEqualFilter) filter).setValue(value);
            } else if (SINGLEVALUE.name.equals(name)) {
                filter = new SingleValueFilter(value);
            }
            return filter;
        }
    }

    public static void main(String[] args) {
        IconSource iconSource = new IconSource();
        Icon loadIcon = iconSource.getSmallIconByID("import");
        Icon loadRolloverIcon = iconSource.getSmallIconByID("importRollover");
        JButton button = new JideButton(loadIcon);
        button.setRolloverIcon(loadRolloverIcon);

        button.addActionListener(log::debug);

        JPanel panel = new JPanel();

        panel.add(new Label("test"));
        panel.add(button);

        JFrame frame = new JFrame();
        frame.setResizable(true);
        frame.setPreferredSize(new Dimension(250, 150));
        frame.getContentPane().add(panel);
        frame.pack();

        JideSwingUtilities.globalCenterWindow(frame);
        frame.setVisible(true);
        frame.toFront();
    }

}
