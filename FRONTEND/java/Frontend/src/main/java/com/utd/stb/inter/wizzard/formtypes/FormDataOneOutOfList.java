package com.utd.stb.inter.wizzard.formtypes;

import com.utd.stb.inter.items.DisplayableItemComboList;
import com.utd.stb.inter.wizzard.FormData;


/**
 * User may choose one item out of a list of items, the List of available items
 * maybe loaded lazily, since it may take a long time, so only call getItems()
 * if you need it. On startup the selectedItem is filled, if the user chooses
 * this one there is no need to get the whole list.
 *
 * @author PeterBuettner.de
 */
public interface FormDataOneOutOfList extends FormData {

    /**
     * get the available items and the selected one, note that the list maybe
     * filled on demand, so don't call any methods on it since it may last long
     *
     * @return
     */
    DisplayableItemComboList getDisplayableItemComboList();

    /**
     * The label to be displayed above the list.
     *
     * @return
     */
    String getLabel();
}