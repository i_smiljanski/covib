package com.utd.stb.inter.items;

/**
 * @author Inna Smiljanski  created 04.03.14
 */
public class DisplayableItemInfo implements DisplayableItem {
    private DisplayableItem item;

    public DisplayableItemInfo(DisplayableItem item) {
        this.item = item;
    }

    public String toString() {
        return item.getInfo();
    }

    @Override
    public Object getData() {
        return item.getData();
    }

    @Override
    public String getInfo() {
        return item.getInfo();
    }

    @Override
    public Object getAttributeList() {
        return item.getAttributeList();
    }
}
