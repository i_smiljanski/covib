package com.utd.stb.gui.swingApp;

import com.utd.gui.controls.table.ListViewTools;
import com.utd.stb.gui.userinput.swing.UIOTableCellRenderer;

import javax.swing.*;
import java.awt.*;


public class UserInputRenderer extends UIOTableCellRenderer {

    public Component getTableCellRendererComponent(JTable table, Object value, boolean sel,
                                                   boolean foc, int row, int col) {
        //System.out.println("UserInputRenderer.getTableCellRendererComponent "+value +"; "+value.getClass());
        sel = table.getSelectionModel().isSelectedIndex(row);
        Component c = super.getTableCellRendererComponent(table, value, sel, foc, row, col);
        ListViewTools.changeDefaultTableCellRenderer(table, c, row, col);
        return c;
    }
}