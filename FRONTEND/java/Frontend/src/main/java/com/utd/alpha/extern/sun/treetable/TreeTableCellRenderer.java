package com.utd.alpha.extern.sun.treetable;

import com.utd.stb.back.data.client.AttributeList;
import com.utd.stb.inter.items.DisplayableItem;
import resources.IconSource;

import javax.swing.*;
import javax.swing.table.DefaultTableCellRenderer;
import javax.swing.table.TableCellRenderer;
import javax.swing.tree.TreePath;
import java.awt.*;


/**
 * A TableCellRenderer that displays <b>like </b> a JTree.
 * <p>
 * Needs: expanded/collapsed icon of <b>same </b> size
 */
public class TreeTableCellRenderer extends DefaultTableCellRenderer implements
        TableCellRenderer {

    /**
     * anywhere unknown
     */
    public static final int HIT_NONE = 0;
    /**
     * 'left' of handle
     */
    public static final int HIT_FRONT = 1;
    /**
     * handle
     */
    public static final int HIT_HANDLE = 2;
    /**
     * Tree-Icon
     */
    public static final int HIT_ICON = 3;
    /**
     * text (plus the text-icon gap)
     */
    public static final int HIT_TEXT = 4;

    /**
     * Test where a 'point' is (y is irrelevant)
     *
     * @param x relative to Renderer-left
     * @return one of
     * <ul>
     * <li>TreeTableCellRenderer.HIT_NONE
     * <p>
     * <li>TreeTableCellRenderer.HIT_FRONT
     * <li>TreeTableCellRenderer.HIT_HANDLE
     * <li>TreeTableCellRenderer.HIT_ICON
     * <li>TreeTableCellRenderer.HIT_TEXT
     * </ul>
     */
    public int getHitCode(int x) {

        if (x < offsetHandle) return HIT_FRONT;
        if (x < offsetIcon) return HIT_HANDLE;
        if (x < offsetText) return HIT_ICON;
        return HIT_TEXT;
    }

    // Icons
    /**
     * Icon used to show non-leaf nodes that aren't expanded.
     */
    private Icon closedIcon;

    /**
     * Icon used to show leaf nodes.
     */
    private Icon leafIcon;

    /**
     * Icon used to show non-leaf nodes that are expanded.
     */
    private Icon openIcon;

    private Icon icHandleExpanded;
    private Icon icHandleCollapsed;
    /**
     * each level
     */
    private int inset = 10;

    protected JTreeTable treeTable;

    private int level;
    private boolean expanded;
    private boolean leaf;

    private int offsetHandle;
    private int offsetIcon;
    private int offsetText;

    private Icon tIcon = new TIcon();

    private Icon iconLabel;
    private TreePath treePath;

    private IconSource is;

    public TreeTableCellRenderer(JTreeTable treeTable) {

        this.treeTable = treeTable;
        this.is = new IconSource();
        initUI();
    }

    public void updateUI() {

        super.updateUI();
        initUI();
    }

    private void initUI() {

        icHandleExpanded = UIManager.getIcon("Tree.expandedIcon");
        icHandleCollapsed = UIManager.getIcon("Tree.collapsedIcon");

        setLeafIcon(UIManager.getIcon("Tree.leafIcon"));
        setClosedIcon(UIManager.getIcon("Tree.closedIcon"));
        setOpenIcon(UIManager.getIcon("Tree.openIcon"));

        inset = icHandleExpanded.getIconWidth();
        inset += getIconTextGap();
    }

    public Component getTableCellRendererComponent(JTable table, Object value,
                                                   boolean isSelected, boolean hasFocus,
                                                   int row, int column) {
        super.getTableCellRendererComponent(table, value, isSelected, hasFocus, row, column);
        treePath = treeTable.getPathForRow(row);
        if (treePath == null) return null;

        level = getLevelDisplay(treePath);
        expanded = treeTable.isExpanded(treePath);
        leaf = treeTable.isLeaf(treePath);

        if (leaf) {
            setLabelIcon(getLeafIcon());
        } else if (expanded) {
            setLabelIcon(getOpenIcon());
        } else {
            setLabelIcon(getClosedIcon());
        }

        calculateOffsets();

        setIcon(tIcon);

        AttributeList attrList = null;
        if (value instanceof DisplayableItem) {
            DisplayableItem item = (DisplayableItem) value;
            attrList = (AttributeList) item.getAttributeList();
            // VALUE
            if (column == table.getColumnModel().getColumn(0).getModelIndex())
                this.setValue(item.getData());
            if (column == table.getColumnModel().getColumn(1).getModelIndex())
                this.setValue(item.getInfo());
        }
        if (attrList != null) {

            // COLOR
            Color color = attrList.getColor();
            if (!isSelected && !hasFocus) this.setBackground(color);

            // HIGHLIGHT
            Font font = attrList.getHighlighted();
            if (font != null) this.setFont(font);

            // ICON
            String icon = attrList.getIcon();
            if (leaf && level != 0 && column == 0 && icon != null)
                this.setIcon(is.getSmallIconByID(icon));
            if (leaf && level != 0 && column != 0) this.setIcon(null);


            // VALUE for Spalte 2 und mehr
            for (int i = 2; i < table.getColumnCount(); i++) {

                if (column == table.getColumnModel().getColumn(i).getModelIndex()) { // table.getColumnModel().getColumn(i).getModelIndex() ) {
                    this.setValue(attrList.getColValue(table.getModel().getColumnName(column)));
                }
            }
        }
        return this;
    }

    /**
     * we may recalculate this if the LabelIcon is set after Renderer is created
     */
    private void calculateOffsets() {

        offsetHandle = level * inset;
        offsetIcon = offsetHandle + icHandleExpanded.getIconWidth() + getIconTextGap();
        if (iconLabel == null)
            offsetText = offsetIcon;
        else
            offsetText = offsetIcon + iconLabel.getIconWidth();
    }

    public void setLabelIcon(Icon icon) {

        if (iconLabel != icon) {
            iconLabel = icon;
            calculateOffsets();
        } else
            iconLabel = icon;
    }

    public Icon getLabelIcon() {

        return iconLabel;
    }

    private int getLevelDisplay(TreePath path) {

        int off = 0;
        if (!treeTable.isRootVisible()) off--; // root is in path!
        if (!treeTable.getShowsRootHandles()) off--;
        return (path.getPathCount() - 1) + off;
    }

    /**
     * handles level, expandedState, leaf, labelIcon. set this icon into a
     * JLabel (~ defaulttablecellrenderer) note that this is painted 'centered'
     * in the Label
     *
     * @author PeterBuettner.de
     */
    private class TIcon implements Icon {

        public int getIconHeight() {

            if (iconLabel == null) return icHandleExpanded.getIconHeight();
            return Math.max(iconLabel.getIconHeight(), icHandleExpanded.getIconHeight());
        }

        public int getIconWidth() {

            return offsetText;
        }

        public void paintIcon(Component c, Graphics g, int x, int y) {

            if (iconLabel != null) paintIconVCentered(c, g, x + offsetIcon, iconLabel);

            if (leaf || level < 0) return; // paint no handle
            paintIconVCentered(c, g, x + offsetHandle, expanded
                    ? icHandleExpanded
                    : icHandleCollapsed);
        }

        /**
         * vert-center in component c.
         *
         * @param c
         * @param g
         * @param x
         * @param icon
         */
        private void paintIconVCentered(Component c, Graphics g, int x, Icon icon) {

            icon.paintIcon(c, g, x, (c.getHeight() - icon.getIconHeight()) / 2);
        }

    }

    public Icon getClosedIcon() {

        return closedIcon;
    }

    public void setClosedIcon(Icon closedIcon) {

        this.closedIcon = closedIcon;
    }

    public Icon getLeafIcon() {

        return leafIcon;
    }

    public void setLeafIcon(Icon leafIcon) {

        this.leafIcon = leafIcon;
    }

    public Icon getOpenIcon() {

        return openIcon;
    }

    public void setOpenIcon(Icon openIcon) {

        this.openIcon = openIcon;
    }

    protected boolean isExpanded() {

        return expanded;
    }

    protected boolean isLeaf() {

        return leaf;
    }

    /**
     * Treepath of the current row
     *
     * @return
     */
    public TreePath getTreePath() {

        return treePath;
    }
}