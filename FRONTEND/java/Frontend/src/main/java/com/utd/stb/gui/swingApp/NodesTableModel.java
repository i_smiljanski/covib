package com.utd.stb.gui.swingApp;

import com.jidesoft.converter.ConverterContext;
import com.jidesoft.grid.ContextSensitiveTableModel;
import com.jidesoft.grid.EditorContext;
import com.jidesoft.grid.GroupableTableModel;
import com.jidesoft.grid.HierarchicalTableModel;
import com.jidesoft.grouper.GrouperContext;
import com.utd.stb.back.ui.impl.userinputs.fields.UserInputConverter;
import com.utd.stb.inter.application.Node;
import com.utd.stb.inter.application.Nodes;
import com.utd.stb.inter.userinput.UserInput;
import com.utd.stb.inter.userinput.UserInputs;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import resources.Res;

import javax.swing.table.AbstractTableModel;
import java.util.ArrayList;


/**
 * the data has to be in the right condition (rectangular matrix).
 *
 * @author PeterBuettner.de
 */
public class NodesTableModel extends AbstractTableModel implements ContextSensitiveTableModel, GroupableTableModel, HierarchicalTableModel {
    public static final Logger log = LogManager.getRootLogger();

    private Nodes nodes;
    private UserInputs[] userInputs;
    private ArrayList<String> colNames = new ArrayList<String>();
    private ArrayList<Integer> colIndexes = new ArrayList<Integer>();
    private boolean isIconByDetailView;

    public NodesTableModel(Nodes nodes, UserInputs[] userInputs) {

        this.nodes = nodes;
        this.userInputs = userInputs;

        isIconByDetailView = nodes.getNode(0).getIconId() != null && nodes.getNode(0).getIconId().length() > 0;

        // prepare loading col names ...
        UserInputs ui = userInputs[0];
        //System.out.println("nodes.getNode(0).getIconId() = " + nodes.getNode(0).getIconId()+" "+isIconByDetailView);
        // ... load colnames into list ...
        if (userInputs.length == 0) {
            colNames.add(Res.getString("main.detailView.column.node.name"));

        } else if (nodes.getNode(0).getDisplayType() == Node.MENU_DETAIL_VIEW) {
            if (isIconByDetailView) {
                colNames.add("");
            }
            for (int i = 0; i < ui.getSize(); i++) {
                if (ui.get(i).isVisible()) {
                    colNames.add(ui.get(i).getLabel());
                    colIndexes.add(i);
                }
            }
        } else {
            colNames.add(Res.getString("main.detailView.column.node.name"));
            for (int i = 1; i < ui.getSize() + 1; i++) {
                colNames.add(ui.get(i - 1).getLabel());
            }
        }
    }

    public void addRow(int rowIndex) {
        nodes.addRow(rowIndex);
    }


    public void deleteRow(int rowIndex) {
        nodes.deleteRow(rowIndex);
    }

    public boolean isCellEditable(int rowIndex, int columnIndex) {
        return !userInputs[rowIndex].get(columnIndex).isReadOnly();
    }

    public Class getColumnClass(int col) {
        Class colClass = UserInput.class;
//        log.debug(col + "  "+ colNames.get(col) +" "+ userInputs[0].get(col).getType());
        if (nodes.getNode(0).getDisplayType() != Node.MENU_DETAIL_VIEW || isIconByDetailView) {
            colClass = col == 0 ? Node.class : UserInput.class;
        }
        return colClass;
    }

    public String getColumnName(int col) {

        return colNames.get(col);
    }

    public int getColumnCount() {

        return colNames.size();
    }

    public int getRowCount() {

        return nodes.getSize();
    }

    public UserInputs[] getUserInputs() {
        return userInputs;
    }

    public Object getValueAt(int row, int col) {
        if (col == -1) return nodes.getNode(row);
        if (col == 0
                && nodes.getNode(0).getDisplayType() != Node.MENU_DETAIL_VIEW
        )
            return nodes.getNode(row);
        else if (col == 0 && isIconByDetailView) {
            nodes.getNode(row).setShowText(false);
            return nodes.getNode(row);
        }
        if (nodes.getNode(0).getDisplayType() != Node.MENU_DETAIL_VIEW || isIconByDetailView) {
            return userInputs[row].get(col - 1);
        }
        return userInputs[row].get(colIndexes.get(col));
    }

    /**
     * get the row index of the node
     *
     * @param node
     * @return index, or -1 if not found
     */
    public int indexOf(Node node) {

        return nodes.indexOf(node);
    }

    // MCD, 09.Dez 2008, sort table, TODO
    /*public void sortByColumn(final int column) {
        try {
	    	TreeNodeRecord nodeRec = (TreeNodeRecord)nodes.getNode(0);
	        PropertyList props = nodeRec.getProperties();
    	} catch (Exception e) {
    	}
    }*/

    public ConverterContext getConverterContextAt(int i, int i1) {
        return UserInputConverter.CONTEXT;
    }

    public EditorContext getEditorContextAt(int i, int i1) {
        return null;
    }

    public Class<?> getCellClassAt(int row, int column) {
        return getColumnClass(column);
    }

    public GrouperContext getGrouperContext(int column) {
        if (getColumnClass(column).equals(UserInput.class))
            return new GrouperContext("UserInput");
        return null;
    }

    @Override
    public boolean hasChild(int row) {
        return true;
    }

    @Override
    public boolean isHierarchical(int row) {
        return true;
    }

    @Override
    public Object getChildValueAt(int row) {
        return null;
    }

    @Override
    public boolean isExpandable(int row) {
        return true;
    }
}