package com.utd.gui.action;

import com.utd.gui.resources.I18N;

import javax.swing.*;


public abstract class ActionClose extends AbstractAction {

    public ActionClose() {

        ActionUtil.initActionFromMap(this, I18N.getActionMap(ActionClose.class.getName()),
                null);
        putValue(SHORT_DESCRIPTION, null);
    }

}