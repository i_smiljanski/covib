package com.utd.stb.back.ui.impl.userinputs.data;

import com.utd.stb.back.data.client.PropertyList;
import com.utd.stb.back.data.client.PropertyRecord;
import com.utd.stb.back.database.Dispatcher;
import com.utd.stb.back.ui.impl.userinputs.fields.*;
import com.utd.stb.inter.application.BackendException;
import com.utd.stb.inter.userinput.DirectoryInputImpl;
import com.utd.stb.inter.userinput.DisplayableItemInput;
import com.utd.stb.inter.userinput.FileInputImpl;
import com.utd.stb.inter.userinput.UserInput;
import org.apache.commons.lang.StringUtils;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import javax.imageio.ImageIO;
import javax.swing.*;
import java.awt.*;
import java.awt.image.BufferedImage;
import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.util.ArrayList;


/**
 * Provides common methods for different implementations of InputData. InputData
 * are derived from DB struct PropertyRecord that contains a Display for a field
 * a Value for a field(equals display except fields has a list of valid data) a
 * prompt for the field the type of field(number, date...) format of field(i.e.
 * dd.MM.yyyy for a date field) has the field a list of valid data, is it
 * editable/displayed
 *
 * @author rainer bruns Created 14.12.2004
 * @see PropertyRecord
 */
public abstract class AbstractInputData extends ListItemProvider {
    public static final Logger log = LogManager.getRootLogger();

    private PropertyList properties;
    ArrayList<UserInput> data = new ArrayList<>();
    //might get a user and/or password field to verify
    int indexOfPasswordField = -1;
    int indexOfUserField = -1;
    //for multiple typing of new passwords and later compare input
    ArrayList<Object> indecesOfNewPasswordFields = new ArrayList<>();
    boolean hasPasswordToCheck = false;

    /**
     * @param props the list of field data - PropertyRecords
     * @param d
     */
    public AbstractInputData(PropertyList props, Dispatcher d) {

        super(d);
        this.properties = props;
    }

    /**
     * @param index requested index in list
     * @return UserInput at index
     */
    public UserInput get(int index) {

        return data.get(index);
    }


    /**
     * @return size of field list
     */
    public int getSize() {

        return data.size();
    }

    /**
     * @param u the input to look up
     * @return index of input
     */
    public int indexOf(UserInput u) {

        return data.indexOf(u);
    }

    /**
     * go through the field list, determine the type of field and initialize the
     * appropriate field implementation. Set properties like editable, displayed
     */
    void createData() throws BackendException {
        data = new ArrayList<>();
        properties.initIterator();

        // log.debug("properties size="+properties.getSize());
        for (int i = 0; properties.hasNext(); i++) {

            PropertyRecord property = properties.getNext();

            String propertyType = property.getType();
//            log.debug(property.getDisplayAsString() + " " + propertyType);
            /*try {
              log.debug(i+": "+property.getPrompt()+"; "+property.getValue()+"; "+propertyType);
            } catch (ParseException e) {
                    log.error(e);
                }*/

            if (property.hasLov() && !propertyType.equals("FILE")) {
                DisplayableItemInput liui = new ListItemInput(property, this);
//                try {
//                    log.debug(property.getValue());
//                } catch (ParseException e) {
//                    log.error(e);
//                }
                DisplayableItemUserInput diui = new DisplayableItemUserInput(property.getPrompt(), liui);
                diui.setEmptyAllowed(property.isEmptyAllowed());
                diui.setVisible(property.isDisplayed());
                diui.setReadOnly(property.isReadOnly());
                diui.setChangeable(property.isChangeable());
                diui.setFocus(property.getFocus());

                data.add(diui);
            } else if (propertyType.endsWith("PASSWORD")
                    || propertyType.equals("STRING")
                    || propertyType.equals("LONGSTRING")) {
                // normal String
                // MCD, 22.Jun 2006
                TextUserInput tui = new TextUserInput(property.getPrompt(),
                        new TextInputImpl(property.getDisplayAsString(),
                                property.getSize(),
                                propertyType.equals("LONGSTRING"), // multiline
                                propertyType.endsWith("PASSWORD")), // password
                        property);
                tui.setEmptyAllowed(property.isEmptyAllowed());
                tui.setVisible(property.isDisplayed());
                tui.setReadOnly(property.isReadOnly());
                tui.setFocus(property.getFocus());
                data.add(tui);
                if (property.getParameterName().equals("USER")) {
                    indexOfUserField = i;
                }
                if (propertyType.equals("PASSWORD")
                        || propertyType.equals("OPASSWORD")) {
                    /*
                     * this is to remember the index of password for later
                     * validation of input PASSWORD is used in esigs, when value is
                     * not sent to DB in passwordChangeDialog password is sent to DB
                     * so its OPASSWORD
                     */
                    indexOfPasswordField = i;
//                    log.debug(indexOfPasswordField);
                    hasPasswordToCheck = true;
                } else if (propertyType.equals("NPASSWORD")) {
                    // the rest are new Password Fields, remember for input validation
                    indecesOfNewPasswordFields.add(i);
                }
            } else if (propertyType.equals("LONG")) {
                Long value = null;
                try {
                    value = new Long(property.getDisplayAsString());
                } catch (Exception e) {
                    log.error(e);
                }
                LongUserInput lui = new LongUserInput(property.getPrompt(), value, property);
                lui.setEmptyAllowed(property.isEmptyAllowed());
                lui.setVisible(property.isDisplayed());
                lui.setReadOnly(property.isReadOnly());
                lui.setFocus(property.getFocus());
                data.add(lui);
            } else if (propertyType.equals("INTEGER")) {
                Integer value = null;
                try {
                    value = new Integer(property.getDisplayAsString());
                } catch (Exception e) {
                    log.error(e);
                }
                IntegerUserInput lui = new IntegerUserInput(property.getPrompt(), value, property);
                lui.setEmptyAllowed(property.isEmptyAllowed());
                lui.setVisible(property.isDisplayed());
                lui.setReadOnly(property.isReadOnly());
                lui.setFocus(property.getFocus());
                log.debug(value);
                data.add(lui);
            } else if (propertyType.equals("NUMBER")) {
                Double value = null;
                try {
                    value = new Double(property.getDisplayAsString());
                } catch (Exception e) {
                    log.error(e);
                }
                DoubleUserInput dui = new DoubleUserInput(property.getPrompt(), value, property);
                dui.setEmptyAllowed(property.isEmptyAllowed());
                dui.setVisible(property.isDisplayed());
                dui.setReadOnly(property.isReadOnly());
                dui.setFocus(property.getFocus());
                data.add(dui);
            } else if (propertyType.equals("BOOLEAN")) {
                BooleanUserInput bui = new BooleanUserInput(property.getPrompt(),
                        property.convertToBoolean(property.getDisplayAsString()),
                        property);
                //bui.setEmptyAllowed(property.isEmptyAllowed()); ISRC-415
                bui.setVisible(property.isDisplayed());
                bui.setReadOnly(property.isReadOnly());
                bui.setFocus(property.getFocus());
                data.add(bui);
            } else if (propertyType.equals("DATE") || propertyType.equals("DATETIME")) {
                DateInputImpl di = new DateInputImpl(property);
                DateUserInput dui = new DateUserInput(property.getPrompt(), di);
                dui.setEmptyAllowed(property.isEmptyAllowed());
                dui.setVisible(property.isDisplayed());
                dui.setReadOnly(property.isReadOnly());
                dui.setFocus(property.getFocus());
                data.add(dui);
            } else if (propertyType.equals("FILE")) {
                FileInputImpl fi = new FileInputImpl(property);
                FileUserInput fui = new FileUserInput(property.getPrompt(), fi, property);
                fui.setEmptyAllowed(property.isEmptyAllowed());
                fui.setVisible(property.isDisplayed());
                fui.setReadOnly(property.isReadOnly());
                fui.setFocus(property.getFocus());
                data.add(fui);
            } else if (propertyType.equals("DIRECTORY")) {
                DirectoryInputImpl fi = new DirectoryInputImpl(property);
                DirectoryUserInput fui = new DirectoryUserInput(property.getPrompt(), fi, property);
                fui.setEmptyAllowed(property.isEmptyAllowed());
                fui.setVisible(property.isDisplayed());
                fui.setReadOnly(property.isReadOnly());
                fui.setFocus(property.getFocus());
                data.add(fui);
            } else if (propertyType.equals("GROUP")) {
                GroupUserInput group = new GroupUserInput(property.getPrompt(), property);
                data.add(group);
            } else if (propertyType.equals("TABSHEET")) {
                TabSheetUserInput tab = new TabSheetUserInput(property.getPrompt(), property);
                tab.setVisible(property.isDisplayed());
                data.add(tab);
            } else if (propertyType.equals("PROGRESSBAR")) {
                JProgressBar progressBar;
                //log.debug("property = " + property.getValueAsString());
                if (property.getDisplayAsString().length() > 0) {
                    progressBar = new JProgressBar(0, 100);
                    progressBar.setPreferredSize(new Dimension(300, 30));
                    try {
                        if (StringUtils.isNotEmpty(property.getValueAsString()))
                            progressBar.setValue(Integer.parseInt(property.getValueAsString()));
                    } catch (Exception e) {
                        log.error(e);
                    }
                    progressBar.setStringPainted(true);
                    progressBar.setString(property.getDisplayAsString());
                    ProgressBarUserInput pb = new ProgressBarUserInput(property.getPrompt(), progressBar, property);
                    data.add(pb);
                } else
                    data.add(new ProgressBarUserInput(property.getPrompt(), null, property));
            } else if (propertyType.equals("BLOB")) {
                BufferedImage image = null;
                try {
                    byte[] bytes = property.getBlobValueAsBytes();
                    if (bytes != null) image = ImageIO.read(new ByteArrayInputStream(bytes));
                } catch (IOException e) {
                    log.error(e);
                }
                //  Image image = new ImageIcon(property.getBlobValueAsBytes()).getImage();
                ImageUserInput imi = new ImageUserInput(property.getPrompt(), image, property);

                data.add(imi);
            } else if (propertyType.equals("DISPLAY_TEXT")) {
                DisplayTextUserInput txt = new DisplayTextUserInput("", property.getDisplayAsString(), property);
                txt.setVisible(property.isDisplayed());
                data.add(txt);
            }
        }
        // log.debug("data size = "+data.size());
        /*for (Object aData : data) {
            log.debug(aData);
        }*/
    }


    /**
     * Go through the list of UserInputs, get the PropertyRecord from each entry
     * and add to the list to save. Intended to be called by InputData
     * implementations on the saveData call from their dialog to get the list to
     * save.
     *
     * @return the list of field data to save
     */
    public PropertyList getListToSave() {
        log.debug("begin ");
        PropertyList propsToSave = new PropertyList();
        for (Object aData : data) {
            UserInput ui = (UserInput) aData;
//           log.debug(ui.getLabel()+" "+ui.getClass());
            PropertyRecord rec = null;
            if (ui instanceof TextUserInput) {
                TextUserInput txt = (TextUserInput) ui;
                rec = txt.getProperty();
                try {
//                    log.debug(ui.getLabel()+": "+rec.getValue());
                    rec.setDisplayValue(rec.getValue());
                } catch (Exception e) {
                    log.error(e);
                }
            } else if (ui instanceof BooleanUserInput) {
                BooleanUserInput bui = (BooleanUserInput) ui;
                rec = bui.getProperty();
            } else if (ui instanceof DoubleUserInput) {
                DoubleUserInput lui = (DoubleUserInput) ui;
                rec = lui.getProperty();
            } else if (ui instanceof LongUserInput) {
                LongUserInput lui = (LongUserInput) ui;
                rec = lui.getProperty();
            } else if (ui instanceof IntegerUserInput) {
                IntegerUserInput lui = (IntegerUserInput) ui;
                rec = lui.getProperty();
            } else if (ui instanceof DisplayableItemUserInput) {
                DisplayableItemUserInput diui = (DisplayableItemUserInput) ui;
                DisplayableItemInput lii = (DisplayableItemInput) diui.getData();
                ListItemInput liui = (ListItemInput) lii;
                rec = liui.getProperty();
//                try {
//                    log.debug(ui.getLabel() + ": " + rec.getValue());
//                } catch (ParseException e) {
//                    log.error(e);
//                }
            } else if (ui instanceof DateUserInput) {
                DateUserInput dui = (DateUserInput) ui;
                DateInputImpl di = (DateInputImpl) dui.getData();
                rec = di.getProperty();
            } else if (ui instanceof TabSheetUserInput) {
                // TABSHEET
                TabSheetUserInput tab = (TabSheetUserInput) ui;
                rec = tab.getProperty();
            } else if (ui instanceof GroupUserInput) {
                // TABSHEET
                GroupUserInput tab = (GroupUserInput) ui;
                rec = tab.getProperty();
            } else if (ui instanceof FileUserInput) {
                // FILE
                FileUserInput fui = (FileUserInput) ui;
                FileInputImpl fi = (FileInputImpl) fui.getData();
                rec = fi.getProperty();
            } else if (ui instanceof DirectoryUserInput) {
                // FILE
                DirectoryUserInput fui = (DirectoryUserInput) ui;
                DirectoryInputImpl fi = (DirectoryInputImpl) fui.getData();
                rec = fi.getProperty();
            } else if (ui instanceof ImageUserInput) {
                ImageUserInput iui = (ImageUserInput) ui;
                rec = iui.getProperty();
            } else if (ui instanceof DisplayTextUserInput) {
                DisplayTextUserInput iui = (DisplayTextUserInput) ui;
                rec = iui.getProperty();
            }
            if (!(ui instanceof TabSheetUserInput) && !(ui instanceof DisplayTextUserInput)) {
                rec.setFocus(ui.hasFocus());
            }
            //do not send back PASSWORD
            if (!rec.getType().equals("PASSWORD")) {
                propsToSave.add(rec);
            }
        }
        log.debug("end");
        return propsToSave;
    }

    public PropertyList getListToSave(ArrayList<UserInput> data) {
        this.data = data;
        return getListToSave();
    }
}