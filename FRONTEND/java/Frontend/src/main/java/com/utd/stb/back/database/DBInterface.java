package com.utd.stb.back.database;

import com.uptodata.isr.utils.ConvertUtils;
import com.utd.stb.back.data.client.*;
import com.utd.stb.back.ui.impl.userinputs.data.UserInputData;
import com.utd.stb.gui.swingApp.login.UserLoginException;
import com.utd.stb.inter.application.BackendException;
import oracle.jdbc.OracleCallableStatement;
import oracle.jdbc.OracleResultSet;
import oracle.jdbc.driver.OracleConnection;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.io.*;
import java.sql.*;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Properties;

/**
 * This class provides methods to call the different methods in package stb$gal
 *
 * @created 10. Dezember 2003
 */
public class DBInterface extends AbstractDBInterface {

    public static final Logger log = LogManager.getRootLogger();
    private Connection conn;
    private String schemaOwner;
    private String fullUsername;

    private FormInfoRecord formInfoRecord;
    private ArrayList<EntityRecord> entityList;
    private SelectionList selectionList;
    private boolean bModifiedFlag;

    /**
     * Constructor mit Datenbankverbindung
     *
     * @param connection
     * @throws UserLoginException
     * @throws SQLException
     */
    public DBInterface(Connection connection) {
        super();
        this.conn = connection;
    }

    void setSchemaOwner(String schemaOwner) {
        this.schemaOwner = schemaOwner;
    }

    public void checkIsValidOwner() throws Exception {
        log.info("begin");
        try {
            log.debug(conn);
            OracleCallableStatement cs = (OracleCallableStatement) conn.prepareCall(" begin ? := stb$gal.checkISROwner; end; ");
            cs.registerOutParameter(1, java.sql.Types.VARCHAR);
            OracleResultSet rs = (OracleResultSet) cs.executeQuery();
            this.schemaOwner = cs.getString(1);

            closeCursor(cs, rs);
            log.info("end");
            if (schemaOwner.toUpperCase().equals(conn.getMetaData().getUserName().toUpperCase())) {
                throw new UserLoginException(UserLoginException.ISROWNERCONNECTION);
//                throw new Exception("ISROWNERCONNECTION");
            }
        } catch (Exception e) {
            log.error(e);
            e.printStackTrace();
//            throw new UserLoginException(UserLoginException.ISROWNERCONNECTION);
              if (e.getMessage().equals("ISROWNERCONNECTION")) {
                throw e;
            } else {
                throw new BackendException(e.getMessage(), e, BackendException.SEVERITY_ACTION_EXIT);
            }
        }
    }

    /**
     * need the owner of the objects to resolve them. Exit when error actually
     * only workaround, because there are several possibilities, but only few
     * exceptions better let us set the message in Dialog calls
     * stb$gal.checkISRUser(varchar2 out, varchar2 out)
     *
     * @return name of the stabber owner
     * @throws BackendErrorException
     * @throws BackendException
     */
    void checkIsValidUser(String user, String password) throws UserLoginException, SQLException {

        log.info("begin");
        // check user
        if (schemaOwner == null) {
            throw new UserLoginException(UserLoginException.SYNONYMS_MISSING);
        }
        OracleCallableStatement cs = (OracleCallableStatement) conn.prepareCall(" begin ? := stb$gal.checkISRUser(?, ?, ?); end; ");
        cs.registerOutParameter(1, Types.STRUCT, schemaOwner + ".STB$OERROR$RECORD");
        cs.setString(2, user);
        cs.setString(3, password);
        cs.registerOutParameter(4, java.sql.Types.VARCHAR);
        OracleResultSet rs = (OracleResultSet) cs.executeQuery();
        Struct struct = (Struct) cs.getObject(1);


        try {
            checkError(struct);
        } catch (BackendException e) {
            closeCursor(cs, rs);
//            if (e instanceof MessageStackException) {
//                throw new MessageStackException(((MessageStackException) e).getUserStack(),
//                        ((MessageStackException) e).getImplStack(), ((MessageStackException) e).getDevStack());
//            }
            throw new UserLoginException(e.getMessage());
        }
        this.fullUsername = cs.getString(4);
        closeCursor(cs, rs);

    }


    String getRecordTimestamp(String tableName, String tableKey, String keyValue) throws BackendException {
        log.info("begin");
        OracleCallableStatement cs = null;
        OracleResultSet rs = null;
        try {
            cs = (OracleCallableStatement) conn.prepareCall(" begin ? := stb$gal.getRecordTimestamp(?, ?, ?, ?); end; ");
            cs.registerOutParameter(1, Types.STRUCT, schemaOwner + ".STB$OERROR$RECORD");
            cs.setString(2, tableName);
            cs.setString(3, tableKey);
            cs.setString(4, keyValue);
            cs.registerOutParameter(5, Types.VARCHAR);
            rs = (OracleResultSet) cs.executeQuery();
            Struct struct = cs.getSTRUCT(1);
            checkError(struct);

            String timestamp = cs.getString(5);
            closeCursor(cs, rs);
            log.info("end " + timestamp);
            return timestamp;
        } catch (SQLException se) {
            closeCursor(cs, rs);
            log.info("end with error " + se);
            throw new BackendException(se.getMessage(), se, BackendException.SEVERITY_ACTION_EXIT);
        }
    }


    /**
     * need the owner of the objects to resolve them. Exit when error actually
     * only workaround, because there are several possibilities, but only few
     * exceptions better let us set the message in Dialog calls
     * stb$gal.checkISRUser(varchar2 out, varchar2 out)
     *
     * @return name of the stabber owner
     * @throws BackendException
     * @throws BackendException
     */
    private void updateSessionInfo() throws BackendException {
//        log.debug("begin");
        OracleCallableStatement cs = null;
        OracleResultSet rs = null;
        try {
            cs = (OracleCallableStatement) conn.prepareCall(" begin ? := stb$gal.updateSessionInfo; end; ");
            cs.registerOutParameter(1, Types.STRUCT, schemaOwner + ".STB$OERROR$RECORD");
            rs = (OracleResultSet) cs.executeQuery();
            Struct struct = cs.getSTRUCT(1);
            checkError(struct);
            closeCursor(cs, rs);
//            log.debug("end");
        } catch (SQLException se) {
            log.error(se);
            closeCursor(cs, rs);
            throw new BackendException(se.getMessage(), se, BackendException.SEVERITY_ACTION_EXIT);
        }
    }

    /**
     * Retrieves Information for actual form calls
     * stb$gal.GetCurrentFormInfo(ISR$FormInfo$Record out, STB$Entity$List out)
     *
     * @return ArrayList
     */
    protected FormInfoRecord getCurrentFormInfo(ArrayList<EntityRecord> entities, int nextNavigationMask)
            throws BackendException {

        log.trace("begin");
        updateSessionInfo();
        OracleCallableStatement cs = null;
        OracleResultSet rs = null;
        try {

            cs = (OracleCallableStatement) conn.prepareCall(" begin ? := stb$gal.GetCurrentFormInfo(?,?,?); end; ");
            cs.registerOutParameter(1, Types.STRUCT, schemaOwner + ".STB$OERROR$RECORD");
            cs.registerOutParameter(2, Types.STRUCT, schemaOwner + ".ISR$FORMINFO$RECORD");
            cs.registerOutParameter(3, oracle.jdbc.OracleTypes.ARRAY, schemaOwner + ".STB$ENTITY$LIST");
            cs.setInt(4, nextNavigationMask);
            rs = (OracleResultSet) cs.executeQuery();
            Struct sErr = cs.getSTRUCT(1);
            //will throw Exception when Error is severe
            checkError(sErr);
            Struct sFormInfo = cs.getSTRUCT(2);
            ComplexDatabaseRecord rec = new ComplexDatabaseRecord(sFormInfo, conn);
            FormInfoRecord formInfo = new FormInfoRecord(rec.getStruct());
            Array array = cs.getArray(3);
            Object[] elements = (Object[]) array.getArray();
            for (Object element : elements) {
                Struct s = (Struct) element;
                ComplexDatabaseRecord r = new ComplexDatabaseRecord(s, conn);
                EntityRecord e = new EntityRecord(r.getStruct());
                entities.add(e);
            }
            closeCursor(cs, rs);
            return formInfo;
        } catch (SQLException se) {
            se.printStackTrace();
            closeCursor(cs, rs);
            throw new BackendException(se.getMessage(), se, BackendException.SEVERITY_ACTION_EXIT);
        }
    }

    /**
     * Gets the List of Available Data for the entity. Parameter masterKey is
     * used in Grid ( the rowid ) and in Multiselect (category). calls
     * stb$gal.getsList(varchar2 in, varchar2 in, varchar2 in, varchar2 out,
     * STB$SELECTION$LIST out) or stb$gal.getGridSelection(varchar2 in, varchar2
     * in, varchar2 in, varchar2 out, STB$SELECTION$LIST out)
     *
     * @return SelectionList
     */
    protected SelectionList getList(String entity,
                                    String masterKey,
                                    boolean navigate,
                                    int nextNavigationMask,
                                    boolean grid)
            throws BackendException {
        log.debug("begin: masterKey= " + masterKey + "; nextNavigationMask=" + nextNavigationMask + "; entity=" + entity);
        updateSessionInfo();

        OracleCallableStatement cs = null;
        OracleResultSet rs = null;
        try {

            SelectionList data = new SelectionList();
            if (grid) {
                cs = (OracleCallableStatement) conn.prepareCall(" begin ? := stb$gal.GetGridSelection(?,?,?); end; ");
                cs.registerOutParameter(1, Types.STRUCT, schemaOwner + ".STB$OERROR$RECORD");
                cs.setString(2, entity);
                cs.setString(3, navigate ? "T" : "F");
                cs.registerOutParameter(4, java.sql.Types.ARRAY, schemaOwner + ".ISR$TLRSELECTION$LIST");
            } else {
                cs = (OracleCallableStatement) conn.prepareCall(" begin ? := stb$gal.GetsList(?,?,?,?,?,?,?); end; ");
                cs.registerOutParameter(1, Types.STRUCT, schemaOwner + ".STB$OERROR$RECORD");
                cs.setString(2, entity);
                cs.setString(3, masterKey);
                cs.setString(4, navigate ? "T" : "F");
                cs.setInt(5, nextNavigationMask);
                cs.registerOutParameter(6, java.sql.Types.ARRAY, schemaOwner + ".ISR$TLRSELECTION$LIST");
                cs.registerOutParameter(7, Types.STRUCT, schemaOwner + ".ISR$FORMINFO$RECORD");
                cs.registerOutParameter(8, oracle.jdbc.OracleTypes.ARRAY, schemaOwner + ".STB$ENTITY$LIST");
            }
            rs = (OracleResultSet) cs.executeQuery();

            Array array;
            Object[] elements;

            // Out 2
            if (grid) {
                array = cs.getArray(4);
            } else {
                array = cs.getArray(6);
            }
            elements = (Object[]) array.getArray();
            for (Object element1 : elements) {
                Struct s = (Struct) element1;
                ComplexDatabaseRecord rec = new ComplexDatabaseRecord(s, conn);
                SelectionRecord sl = new SelectionRecord(rec.getStruct());
//                log.debug(sl.getDisplay() + "; info = " + sl.getInfo());
                data.add(sl);
            }

            selectionList = data;
            // log.debug(selectionList.getData());

            if (!grid) {

                // Out 3
                Struct sFormInfo = cs.getSTRUCT(7);
                ComplexDatabaseRecord rec = new ComplexDatabaseRecord(sFormInfo, conn);
                formInfoRecord = new FormInfoRecord(rec.getStruct());

                // Out 4
                entityList = new ArrayList<>();
                array = cs.getArray(8);
                elements = (Object[]) array.getArray();
                for (Object element : elements) {
                    Struct s = (Struct) element;
                    ComplexDatabaseRecord r = new ComplexDatabaseRecord(s, conn);
                    EntityRecord e = new EntityRecord(r.getStruct());
                    entityList.add(e);
                }
                //  log.debug(entityList);

            }

            // Out 1
            Struct struct = cs.getSTRUCT(1);
            checkError(struct);

            closeCursor(cs, rs);
            log.debug("end data size = " + data.getSize());
            return data;
        } catch (SQLException se) {
            closeCursor(cs, rs);
            throw new BackendException(se.getMessage(), se, BackendException.SEVERITY_ACTION_EXIT);
        }
    }

    /**
     * Gets the list of available items for a grid cell. Entity is the column
     * header, rowid is the rowid in grid. calls stb$gal.getLov(varchar2 in,
     * varchar2 in, STB$SELECTION$LIST out)
     *
     * @param entity the entity for which list is requested ( column header)
     * @param rowid  the row header
     * @return the List of available Items
     * @throws BackendException
     */
    protected SelectionList getLov(String entity, String rowid, boolean adminMode) throws BackendException {

        log.info("begin " + entity);
        OracleCallableStatement cs = null;
        OracleResultSet rs = null;
        try {

            SelectionList data = new SelectionList();

            if (adminMode) {
                cs = (OracleCallableStatement) conn.prepareCall(" begin ? := isr$install$dialog.GetLov(?,?,?); end; ");
            } else {
                updateSessionInfo();
                cs = (OracleCallableStatement) conn.prepareCall(" begin ? := stb$gal.GetLov(?,?,?); end; ");
            }
            cs.registerOutParameter(1, Types.STRUCT, schemaOwner + ".STB$OERROR$RECORD");
            cs.setString(2, entity);
            cs.setString(3, rowid);
            cs.registerOutParameter(4, java.sql.Types.ARRAY, schemaOwner + ".ISR$TLRSELECTION$LIST");
            rs = (OracleResultSet) cs.executeQuery();

            Struct struct = cs.getSTRUCT(1);
            checkError(struct);

            Array array = cs.getArray(4);
            if (array == null) {
                return data;
            }
            Object[] elements = (Object[]) array.getArray();
            for (Object element : elements) {
                Struct s = (Struct) element;
                ComplexDatabaseRecord rec = new ComplexDatabaseRecord(s, conn);
                SelectionRecord sl = new SelectionRecord(rec.getStruct());
                data.add(sl);
            }
            closeCursor(cs, rs);
            log.debug(data.getSize());
            return data;
        } catch (SQLException se) {
            closeCursor(cs, rs);
            throw new BackendException(se.getMessage(), se, BackendException.SEVERITY_ACTION_EXIT);
        }
    }

    /**
     * called after a change in a grid cell. Determines all dependend entities
     * for parameter entity values for those entities in the actual row are set
     * to null calls stb$gal.getGridDependency(varchar2 in, STB$ENTITY$LIST out,
     * varchar2 in)
     *
     * @param entity the entity ( column header )
     * @param rowid  the row header
     * @return a list of Entities which are dependend on parameter entity
     * @throws BackendException
     */
    protected EntityList getGridDependency(String entity, String rowid) throws BackendException {

        log.info("begin");
        updateSessionInfo();

        OracleCallableStatement cs = null;
        OracleResultSet rs = null;
        try {
            EntityList data = new EntityList();
            cs = (OracleCallableStatement) conn.prepareCall(" begin ? := stb$gal.GetGridDependency(?,?,?); end; ");
            cs.registerOutParameter(1, Types.STRUCT, schemaOwner + ".STB$OERROR$RECORD");
            cs.setString(2, entity);
            cs.registerOutParameter(3, java.sql.Types.ARRAY, schemaOwner + ".STB$ENTITY$LIST");
            cs.setString(4, rowid);
            rs = (OracleResultSet) cs.executeQuery();

            Struct struct = cs.getSTRUCT(1);
            checkError(struct);
            Array array = cs.getArray(3);
            Object[] elements = (Object[]) array.getArray();
            for (Object element : elements) {
                Struct s = (Struct) element;
                ComplexDatabaseRecord rec = new ComplexDatabaseRecord(s, conn);
                EntityRecord sl = new EntityRecord(rec.getStruct());
                data.add(sl);
            }
            closeCursor(cs, rs);
            return data;
        } catch (SQLException se) {
            closeCursor(cs, rs);
            throw new BackendException(se.getMessage(), se, BackendException.SEVERITY_ACTION_EXIT);
        }
    }

    /**
     * on initialization of MultiSelect Form the list of categories is queried
     * calls stb$gal.initList(varchar2 in, STB$TLRSELECTION$LIST out)
     *
     * @param master entity for which to retrieve the list of categories
     * @return the list of categories
     * @throws BackendException
     */
    protected SelectionList initMasterList(String master) throws BackendException {

        log.info("begin");
        updateSessionInfo();

        OracleCallableStatement cs = null;
        OracleResultSet rs = null;
        try {
            SelectionList data = new SelectionList();
            cs = (OracleCallableStatement) conn.prepareCall(" begin ? := stb$gal.initList(?,?); end; ");
            cs.registerOutParameter(1, Types.STRUCT, schemaOwner + ".STB$OERROR$RECORD");
            cs.registerOutParameter(3, java.sql.Types.ARRAY, schemaOwner + ".ISR$TLRSELECTION$LIST");
            cs.setString(2, master);
            rs = (OracleResultSet) cs.executeQuery();
            Struct struct = cs.getSTRUCT(1);
            checkError(struct);

            Array array = cs.getArray(3);
            Object[] elements = (Object[]) array.getArray();
            for (Object element : elements) {
                Struct s = (Struct) element;
                ComplexDatabaseRecord rec = new ComplexDatabaseRecord(s, conn);
                SelectionRecord sl = new SelectionRecord(rec.getStruct());
                data.add(sl);
            }
            closeCursor(cs, rs);
            return data;
        } catch (SQLException se) {
            closeCursor(cs, rs);
            throw new BackendException(se.getMessage(), se, BackendException.SEVERITY_ACTION_EXIT);
        }
    }

    /**
     * used in report wizards formdata to save data calls
     * stb$gal.putsList(varchar2 in, varchar2 in, STB$TLRSELECTION$LIST in)
     *
     * @param entity the entity to save
     * @param data   the list of data to save
     * @throws BackendException
     */
    protected void putList(String entity, SelectionList data, boolean grid)
            throws BackendException {

        log.info("begin " + data);
        OracleCallableStatement cs = null;
        OracleResultSet rs = null;
        updateSessionInfo();

        Object[] structList = new Object[data.getSize()];
        try {
            data.initIterator();
            int i = 0;
            while (data.hasNext()) {
                SelectionRecord sl = data.getNext();
                log.debug(sl);
                if (sl != null) {
                    sl.setAttributeList(null);
                    sl.setSelected("T");
                    if (!grid) {
                        sl.setOrderNum(i + 1);
                    }
                    ComplexDatabaseRecord rec = new ComplexDatabaseRecord(conn, sl.getStruct());
                    Struct struct = rec.toDatum();
                    structList[i] = struct;
                }
                i++;
            }
        } catch (SQLException se) {
            throw new BackendException(se.getMessage(), se, BackendException.SEVERITY_ACTION_EXIT);
        }

        try {
            cs = (OracleCallableStatement) conn
                    .prepareCall(" begin ? := stb$gal.putsList(?,?); end; ");
            cs.registerOutParameter(1, Types.STRUCT, schemaOwner + ".STB$OERROR$RECORD");
            cs.setString(2, entity);

            Array inPropertyList = ((OracleConnection) conn).createARRAY(schemaOwner + ".ISR$TLRSELECTION$LIST", structList);
            cs.setArray(3, inPropertyList);
            rs = (OracleResultSet) cs.executeQuery();
            Struct struct = cs.getSTRUCT(1);
            checkError(struct);
            closeCursor(cs, rs);
        } catch (SQLException se) {
            closeCursor(cs, rs);
            throw new BackendException(se.getMessage(), se, BackendException.SEVERITY_ACTION_EXIT);
        }
    }

    /**
     * retrieves the list of output options for the current report calls
     * stb$gal.getOutputOption(STB$PROPERTY$LIST out)
     *
     * @param whatList an Identifier for the listtype, OUTPUT Option only
     *                 actually
     * @return a list of properties
     * @throws BackendException
     * @see com.utd.stb.back.data.client.PropertyRecord
     */
    protected PropertyList getPropertyList(String whatList) throws BackendException {

        log.info("begin");
        OracleCallableStatement cs = null;
        OracleResultSet rs = null;
        updateSessionInfo();

        try {
            PropertyList data = new PropertyList();
            cs = (OracleCallableStatement) conn.prepareCall(" begin ? := stb$gal.GetOutputOption(?); end; ");
            cs.registerOutParameter(1, Types.STRUCT, schemaOwner + ".STB$OERROR$RECORD");
            cs.registerOutParameter(2, oracle.jdbc.OracleTypes.ARRAY, schemaOwner + ".STB$PROPERTY$LIST");
            rs = (OracleResultSet) cs.executeQuery();
            Struct sErr = cs.getSTRUCT(1);
            checkError(sErr);
            Array array = cs.getArray(2);
            Object[] elements = (Object[]) array.getArray();
            for (Object element : elements) {
                Struct s = (Struct) element;
                ComplexDatabaseRecord option = new ComplexDatabaseRecord(s, conn);
                PropertyRecord rec = new PropertyRecord(option.getStruct());
                data.add(rec);
            }
            closeCursor(cs, rs);
            log.debug("end");
            return data;
        } catch (SQLException se) {
            closeCursor(cs, rs);
            throw new BackendException(se.getMessage(), se, BackendException.SEVERITY_ACTION_EXIT);
        }
    }

    protected void putTable(TreeNodeList nodes, boolean adminMode) throws BackendException {

        log.info("begin");
        OracleCallableStatement cs = null;
        OracleResultSet rs = null;
        try {

            Object[] structList = new Object[nodes.getSize()];
            nodes.initIterator();
            int i = 0;
            while (nodes.hasNext()) {
                structList[i] = new ComplexDatabaseRecord(conn, nodes.getNext().getStruct()).toDatum();
                i++;
            }

            String sInterface = "stb$gal";
            if (adminMode) {
                sInterface = "isr$install$dialog";
            } else {
                updateSessionInfo();
            }

            cs = (OracleCallableStatement) conn.prepareCall(" begin ? := " + sInterface + ".putTable(?); end; ");
            cs.registerOutParameter(1, Types.STRUCT, schemaOwner + ".STB$OERROR$RECORD");
            Array inPropertyList = ((OracleConnection) conn).createARRAY(schemaOwner + ".STB$TREENODELIST", structList);
            cs.setArray(2, inPropertyList);
            rs = (OracleResultSet) cs.executeQuery();
            Struct struct = cs.getSTRUCT(1);
            ErrorRecord err = checkError(struct);
            //err.checkForInfo();
            closeCursor(cs, rs);

        } catch (SQLException se) {
            closeCursor(cs, rs);
            throw new BackendException(se.getMessage(), se, BackendException.SEVERITY_ACTION_EXIT);
        }

    }

    /**
     * used in ReportWizard to get the fields for audit and save dialog. It is a
     * TreeNodelist, however we only need the propertylist of first node. Dont
     * know why we get this. calls stb$gal.getSaveDialog(STB$TREENODE$LIST out)
     * if whatList is SAVEDIALOG or stb$gal.GetAuditMask(STB$TREENODE$LIST out)
     * if whatList is AUDITDIALOG
     *
     * @param whatList SAVEDIALOG or AUDITDIALOG
     * @return a list of Treenodes, actually only one (??)
     * @throws BackendException
     */
    protected TreeNodeList getTreeNodeList(String whatList) throws BackendException {

        log.info("begin");
        OracleCallableStatement cs = null;
        OracleResultSet rs = null;
        updateSessionInfo();

        try {
            TreeNodeList data = new TreeNodeList();
            if (whatList.equals("SAVEDIALOG")) {
                cs = (OracleCallableStatement) conn.prepareCall(" begin ? := stb$gal.GetSaveDialog(?); end; ");
            } else if (whatList.equals("AUDITDIALOG")) {
                cs = (OracleCallableStatement) conn.prepareCall(" begin ? := stb$gal.GetAuditMask(?); end; ");
            }
            cs.registerOutParameter(1, Types.STRUCT, schemaOwner + ".STB$OERROR$RECORD");
            cs.registerOutParameter(2, oracle.jdbc.OracleTypes.ARRAY, schemaOwner + ".STB$TREENODELIST");
            rs = (OracleResultSet) cs.executeQuery();
            Struct sErr = cs.getSTRUCT(1);
            checkError(sErr);

            Array array = cs.getArray(2);
            Object[] elements = (Object[]) array.getArray();
            for (Object element : elements) {
                Struct s = (Struct) element;
                ComplexDatabaseRecord option = new ComplexDatabaseRecord(s, conn);
                TreeNodeRecord rec = new TreeNodeRecord(option.getStruct());
                data.add(rec);
            }
            closeCursor(cs, rs);
            return data;
        } catch (SQLException se) {
            closeCursor(cs, rs);
            throw new BackendException(se.getMessage(), se, BackendException.SEVERITY_ACTION_EXIT);
        }
    }

    /**
     * calls stb$gal.getTimeout(number out)
     *
     * @return the timeout in minutes
     * @throws BackendException
     */
    protected int getTimeout() throws BackendException {

        log.info("begin");
        OracleCallableStatement cs = null;
        OracleResultSet rs = null;
        try {
            int timeout;
            cs = (OracleCallableStatement) conn.prepareCall(" begin ? := stb$gal.GetTimeout(?); end; ");
            cs.registerOutParameter(1, Types.STRUCT, schemaOwner + ".STB$OERROR$RECORD");
            cs.registerOutParameter(2, java.sql.Types.INTEGER);
            cs.executeQuery();
            Struct struct = cs.getSTRUCT(1);
            checkError(struct);
            timeout = cs.getInt(2);
            closeCursor(cs, rs);
            return timeout;
        } catch (SQLException se) {
            closeCursor(cs, rs);
            throw new BackendException(se.getMessage(), se, BackendException.SEVERITY_APP_EXIT);
        }
    }

    /**
     * calls stb$gal.getCurrentLanguage
     *
     * @return the user language 1: German 2: Englisch
     * @throws BackendException
     */
    public int getLanguage() throws BackendException {

        log.info("begin");
        OracleCallableStatement cs = null;
        OracleResultSet rs = null;
        try {
            int language;
            cs = (OracleCallableStatement) conn.prepareCall(" begin ? := stb$gal.GetCurrentLanguage(); end; ");
            cs.registerOutParameter(1, java.sql.Types.INTEGER);
            cs.executeQuery();
            language = cs.getInt(1);
            closeCursor(cs, rs);
            return language;
        } catch (SQLException se) {
            closeCursor(cs, rs);
            throw new BackendException(se.getMessage(), se, BackendException.SEVERITY_APP_EXIT);
        }
    }

    /**
     * calls utd$msglib.getMsg(varchar2 in, number in)
     *
     * @param message  the string to translate
     * @param language the user language
     * @return the translated string
     * @throws BackendException
     */

    //max length of params is 5
    public String getTranslation(String message, int language, String... params) throws BackendException {

        log.trace("begin " + message + " language: " + language);
        OracleCallableStatement cs = null;
        OracleResultSet rs = null;
        try {

            String retval;
            StringBuilder stmtParams = new StringBuilder(",?,?");
            if (params != null && params.length > 0) {
                for (int i = 0; i < params.length; i++) {
                    stmtParams.append(",?");
                }
            }
            String stmtParamsStr = stmtParams.toString().replaceFirst(",", "");
            cs = (OracleCallableStatement) conn.prepareCall(" begin ? := utd$msglib.getMsg(" + stmtParamsStr + "); end; ");
            cs.registerOutParameter(1, java.sql.Types.VARCHAR);
            cs.setString(2, message);
            cs.setInt(3, language);
            if (params != null && params.length > 0) {
                for (int i = 0; i < params.length; i++) {
                    cs.setString(4 + i, params[i]);
                }
            }
            rs = (OracleResultSet) cs.executeQuery();
            retval = cs.getString(1);
            closeCursor(cs, rs);
            return retval;

        } catch (Exception se) {
            closeCursor(cs, rs);
            // geaendert MCD, 08.04.2005
            // statt return message; folgendes:
            se.printStackTrace();
            throw new BackendException(se.getMessage(), se, BackendException.SEVERITY_APP_EXIT);
        }

    }

    /**
     * returns wether a menuitem is enabled or not. If errors in backend returns
     * false. calls stb$gal.setMenuAccess(varchar2 in, varchar2 out)
     *
     * @param parameter the menu identifier
     * @return boolean
     * @throws BackendException on unhandled SQLErrors
     */
    protected boolean getItemAccess(String parameter) throws BackendException {
        log.debug("begin: parameter = " + parameter);
        OracleCallableStatement cs = null;
        OracleResultSet rs = null;
        // ISRC-1031 not necessary
        // updateSessionInfo();

        try {
            boolean retval;
            String accessFlag;
            cs = (OracleCallableStatement) conn.prepareCall(" begin ? := stb$gal.SetMenuAccess(?,?); end; ");
            cs.registerOutParameter(1, Types.STRUCT, schemaOwner + ".STB$OERROR$RECORD");
            cs.registerOutParameter(3, java.sql.Types.VARCHAR);
            cs.setString(2, parameter);
            rs = (OracleResultSet) cs.executeQuery();
            Struct struct = cs.getSTRUCT(1);
            try {
                checkError(struct);
            } catch (Exception e) {
                closeCursor(cs, rs);
                return false;
            }
            accessFlag = cs.getString(3);
            try {
                retval = accessFlag.equals("T");
            } catch (NullPointerException ne) {
                retval = true;
            }
            closeCursor(cs, rs);
            log.debug("end: parameter = " + parameter + " " + retval);
            return retval;
        } catch (SQLException se) {
            closeCursor(cs, rs);
            throw new BackendException(se.getMessage(), se, BackendException.SEVERITY_ACTION_EXIT);
        }
    }

    /**
     * sets the selected node in backend. On select and menu request calls
     * stb$gal.setCurrentNode(STB$TRENODE$RECORD)
     *
     * @param node the selected TreeNode
     * @throws BackendException
     */
    protected void setCurrentNode(TreeNodeRecord node, boolean left) throws BackendException {
        log.info("begin:node = " + node.getDisplay());
        updateSessionInfo();

        OracleCallableStatement cs = null;
        OracleResultSet rs = null;
        try {
//            log.debug("setCurrentNode: type = " + node != null ? node.getNodeType() + "; nodeid = " + node.getNodeId() : "node is null");
            ComplexDatabaseRecord rec = new ComplexDatabaseRecord(conn, node.getNodeData());
            cs = (OracleCallableStatement) conn.prepareCall(" begin ? := stb$gal.SetCurrentNode(?, ?); end; ");
            cs.registerOutParameter(1, Types.STRUCT, schemaOwner + ".STB$OERROR$RECORD");
            cs.setObject(2, rec.toDatum());
            cs.setString(3, left ? "T" : "F");
            rs = (OracleResultSet) cs.executeQuery();
            Struct struct = cs.getSTRUCT(1);
            checkError(struct);
            closeCursor(cs, rs);
            log.info("end:node = " + node.getDisplay());
        } catch (SQLException se) {
            se.printStackTrace();
            closeCursor(cs, rs);
            throw new BackendException(se.getMessage(), se, BackendException.SEVERITY_ACTION_EXIT);
        }

    }

    /**
     * The initial node of the tree, constructed here. Must be Type TOPNODE with
     * package stb$gal
     *
     * @return a TreeNodeRecord
     */
    protected TreeNodeRecord getInitialNode() {

        log.info("begin");
        StructMap initialNodeMap = new StructMap(schemaOwner + ".STB$TREENODE$RECORD");
        log.debug(initialNodeMap);
        return new com.utd.stb.back.data.client.TreeNodeRecord(initialNodeMap);
    }

    /**
     * Gets the childs of input parameter treeNode. Since there are different
     * functions in Backend for tree childs and detail childs parameter
     * forChilds is passed. If true tree childs are quried, else node details.
     * calls stb$gal.getNodeChilds(STB$TREENODE$RECORD in, STB$TREENODE$LIST
     * out) for childs of node or stb$gal.getNodeList(STB$TREENODE$RECORD in,
     * STB$TREENODE$LIST out) for details of node.
     *
     * @param treeNode
     * @param forChilds
     * @return the list of childs
     * @throws BackendException
     */
    protected TreeNodeList getNodeChilds(TreeNodeRecord treeNode, boolean forChilds)
            throws BackendException {
        long duration = System.currentTimeMillis();
        log.debug("begin:treeNode = " + treeNode.getDisplay() + "; " + treeNode.getNodeType() + "; forChilds: " + forChilds);
        updateSessionInfo();

        OracleCallableStatement cs = null;
        OracleResultSet rs = null;
        TreeNodeList nodeList = new TreeNodeList();

        try {
            if (forChilds) {
                cs = (OracleCallableStatement) conn.prepareCall(" begin ? := stb$gal.GetNodeChilds(?, ?); end; ");
            } else {
                cs = (OracleCallableStatement) conn.prepareCall(" begin ? := stb$gal.GetNodeList(?, ?); end; ");
            }
            cs.registerOutParameter(1, Types.STRUCT, schemaOwner + ".STB$OERROR$RECORD");

            ComplexDatabaseRecord rec = new ComplexDatabaseRecord(conn, treeNode.getNodeData());
            Struct struct = rec.toDatum();
            cs.setObject(2, struct);
            cs.registerOutParameter(3, oracle.jdbc.OracleTypes.ARRAY, schemaOwner + ".STB$TREENODELIST");
            cs.executeQuery();
            Struct errRec = cs.getSTRUCT(1);
            checkError(errRec);
            Array array = cs.getArray(3);
            Object[] elements = (Object[]) array.getArray();
            for (Object element : elements) {
                Struct s = (Struct) element;
                ComplexDatabaseRecord rl = new ComplexDatabaseRecord(s, conn);
                TreeNodeRecord node = new TreeNodeRecord(rl.getStruct());
//                log.debug("getNodeChilds:node = " + node.getDisplay() + " getDisplayType: " + node.getDisplayType());
                nodeList.add(node);
            }
            closeCursor(cs, rs);
            //   log.debug("getNodeChilds: size of list "+nodeList.getSize());
            duration = System.currentTimeMillis() - duration;
            log.debug("end:treeNode = " + treeNode.getDisplay() + " forChilds: " + forChilds + " nodeList size " +
                    (nodeList != null ? nodeList.getSize() : null) + " duration: " + duration);
            return nodeList;
        } catch (SQLException se) {
            log.error(se.getMessage(), se);
            closeCursor(cs, rs);
            throw new BackendException(se.getMessage(), se, BackendException.SEVERITY_APP_EXIT);
        } catch (NullPointerException ne) {
            log.error(ne.getMessage());
            closeCursor(cs, rs);
            return nodeList;
        }
    }

    /**
     * gets the menu items for given menuNumber. If 0 its the main menu calls
     * stb$gal.getContextMenu(number in, STB$MENUENTRY$LIST out)
     *
     * @param menuNumber
     * @return list of menu items
     * @throws BackendException
     */
    protected MenuEntryList getContextMenu(int menuNumber) throws BackendException {

        log.info("begin " + menuNumber);
        if (menuNumber == 0) {
            updateSessionInfo();
        }

        OracleCallableStatement cs = null;
        OracleResultSet rs = null;
        MenuEntryList nodeList = new MenuEntryList();
        try {
            cs = (OracleCallableStatement) conn.prepareCall(" begin ? := stb$gal.getContextMenu(?, ?); end; ");
            cs.registerOutParameter(1, Types.STRUCT, schemaOwner + ".STB$OERROR$RECORD");
            cs.registerOutParameter(3, oracle.jdbc.OracleTypes.ARRAY, schemaOwner + ".STB$MENUENTRY$LIST");
            cs.setInt(2, menuNumber);
            cs.executeQuery();
            Struct errRec = cs.getSTRUCT(1);
            checkError(errRec);
            Array array = cs.getArray(3);
            Object[] elements = (Object[]) array.getArray();
            for (Object element : elements) {
                Struct s = (Struct) element;
                ComplexDatabaseRecord rl = new ComplexDatabaseRecord(s, conn);
                MenuEntryRecord mer = new MenuEntryRecord(rl.getStruct());
                nodeList.add(mer);
            }
            closeCursor(cs, rs);
            log.info("end " + menuNumber);
            return nodeList;
        } catch (SQLException se) {
            closeCursor(cs, rs);
            throw new BackendException(se.getMessage(), se, BackendException.SEVERITY_APP_EXIT);
        } catch (NullPointerException ne) {
            closeCursor(cs, rs);
            return nodeList;
        }
    }

    /**
     * For the given menuEntry retrieves list of Nodes. If nodes size is 1, a
     * Dialog is opened with the nodes properties, else properties are displayed
     * in detail window of tree calls stb$gal.callFunction(STB$MenuEntry$Record
     * in, STB$TREENODE$LIST out)
     *
     * @param menuEntry
     * @return list of Treenodes
     * @throws NullPointerException
     * @throws BackendException
     */
    protected TreeNodeList callFunction(MenuEntryRecord menuEntry, boolean adminMode)
            throws NullPointerException, BackendException {
        log.debug("begin :menuEntry = " + menuEntry.print() + " adminMode:" + adminMode);
        OracleCallableStatement cs = null;
        OracleResultSet rs = null;
        try {
            TreeNodeList nodeList = new TreeNodeList();
            ComplexDatabaseRecord rec = new ComplexDatabaseRecord(conn, menuEntry.getStruct());

            if (adminMode) {
                try {
                    cs = (OracleCallableStatement) conn.prepareCall(" begin ? := isr$install$dialog.callFunction(?, ?); end; ");
                } catch (SQLException se) {
                    log.error(se);
                    throw se;
                }
            } else {
                updateSessionInfo();
                try {
                    cs = (OracleCallableStatement) conn.prepareCall(" begin ? := stb$gal.callFunction(?, ?); end; ");
                } catch (SQLException se) {
                    log.error(se);
                    throw se;
                }
            }
            try {
                cs.registerOutParameter(1, Types.STRUCT, schemaOwner + ".STB$OERROR$RECORD");

                Struct struct = rec.toDatum();
                cs.setObject(2, struct);
                cs.registerOutParameter(3, oracle.jdbc.OracleTypes.ARRAY, schemaOwner + ".STB$TREENODELIST");
            } catch (SQLException se) {
                log.error(se);
                throw se;
            }
            try {
                cs.executeQuery();
            } catch (SQLException se) {
                log.error(se);
                se.printStackTrace();
                throw se;
            }
            try {
                Struct errRec = cs.getSTRUCT(1);
                ErrorRecord err = checkError(errRec);

                Array array = cs.getArray(3);
                if (array == null) {
                    log.debug("return null");
                    return null;
                }
                Object[] elements = (Object[]) array.getArray();
                for (Object element : elements) {

                    Struct s = (Struct) element;
                    ComplexDatabaseRecord rl = new ComplexDatabaseRecord(s, conn);
                    TreeNodeRecord node = new TreeNodeRecord(rl.getStruct());
                    // log.debug("callFunction:node = " + node.getClass()+" "+node.getNodeData().getValues()[0]+" "+node.getDisplayType());
                    nodeList.add(node);
                }
            } catch (SQLException se) {
                log.error(se);
                throw se;
            }
            closeCursor(cs, rs);
            log.debug("end");
            return nodeList;
        } catch (SQLException se) {
            log.error(se);
            closeCursor(cs, rs);
            throw new BackendException(se.getMessage(), se, BackendException.SEVERITY_INFORMATIONAL);
        }
    }

    /**
     * ???
     *
     * @param menuEntryRecord
     * @param propertyList
     * @return list of Treenodes
     * @throws BackendException
     */
    protected PropertyList checkDependenciesOnMask(MenuEntryRecord menuEntryRecord,
                                                   PropertyList propertyList,
                                                   boolean adminMode)
            throws BackendException {

        log.info("begin");
        PropertyList data = new PropertyList();

        OracleCallableStatement cs = null;
        OracleResultSet rs = null;
        try {

            // build menu entry record
            ComplexDatabaseRecord rec = new ComplexDatabaseRecord(conn, menuEntryRecord.getStruct());

            // build property list
            Object[] structList = new Object[propertyList.getSize()];
            propertyList.initIterator();
            int i = 0;

            while (propertyList.hasNext()) {

                PropertyRecord sl = propertyList.getNext();
                //    log.debug(sl.getValueAsString());
                ComplexDatabaseRecord rec1 = new ComplexDatabaseRecord(conn, sl.getStruct());
                Struct struct = rec1.toDatum();
                structList[i] = struct;
                i++;
            }

            // execute statement
            if (adminMode) {
                cs = (OracleCallableStatement) conn.prepareCall(" begin ? := isr$install$dialog.checkDependenciesOnMask(?, ?, ?); end; ");
            } else {
                updateSessionInfo();
                cs = (OracleCallableStatement) conn.prepareCall(" begin ? := stb$gal.checkDependenciesOnMask(?, ?, ?); end; ");
            }
            cs.registerOutParameter(1, Types.STRUCT, schemaOwner + ".STB$OERROR$RECORD");
            cs.registerOutParameter(3, oracle.jdbc.OracleTypes.ARRAY, schemaOwner + ".STB$PROPERTY$LIST");
            cs.registerOutParameter(4, oracle.jdbc.OracleTypes.VARCHAR);
            Struct inMenuEntryRecord = rec.toDatum();
            cs.setObject(2, inMenuEntryRecord);

            Array inPropertyList = ((OracleConnection) conn).createARRAY(schemaOwner + ".STB$PROPERTY$LIST", structList);
            cs.setArray(3, inPropertyList);
            cs.executeQuery();

            // get out parameter
            // error object
            Struct outErrorRecord = cs.getSTRUCT(1);
            ErrorRecord err = checkError(outErrorRecord);
            //err.checkForInfo();

            // property list
            Array array = cs.getArray(3);
            Object[] elements = (Object[]) array.getArray();
            for (Object element : elements) {
                Struct s = (Struct) element;
                ComplexDatabaseRecord propRec = new ComplexDatabaseRecord(s, conn);
                PropertyRecord propertyRecord = new PropertyRecord(propRec.getStruct());
                data.add(propertyRecord);
            }

            bModifiedFlag = cs.getString(4).equals("T");
            closeCursor(cs, rs);
            return data;

        } catch (SQLException se) {
            se.printStackTrace();
            closeCursor(cs, rs);
            throw new BackendException(se.getMessage(), se, BackendException.SEVERITY_APP_EXIT);
        }
    }

    /**
     * * saves the list of data by either calling
     * stb$gal.putOutputOption(STB$PROPERTY$LIST in) for Report output options
     * stb$gal.putSaveDialog(STB$PROPERTY$LIST in) for saving the Save dialog
     * stb$gal.putAuditMask(STB$PROPERTY$LIST in) for Audit information
     *
     * @param theList list of data to save
     * @param action  SAVEDIALOG or AUDITDIALOG
     * @throws BackendErrorException
     */
    protected void putPropertyList(PropertyList theList, String clobParameter, Object action, boolean adminMode)
            throws BackendException {
        log.debug("begin: " + action);
        OracleCallableStatement cs = null;
        OracleResultSet rs = null;
        try {
            Object[] structList = null;
            if (theList != null) {
                structList = new Object[theList.getSize()];
                theList.initIterator();
                int i = 0;

                while (theList.hasNext()) {
                    PropertyRecord sl = theList.getNext();
                    ComplexDatabaseRecord rec = new ComplexDatabaseRecord(conn, sl.getStruct());
                    Struct struct = rec.toDatum();
                    structList[i] = struct;
                    i++;
                }
            }

            String sInterface = "stb$gal";
            if (adminMode) {
                sInterface = "isr$install$dialog";
            } else {
                updateSessionInfo();
            }
            log.debug("DBInterface.putPropertyList:  action= " + action + " " + (action != null ? action.getClass() : null));

            if (action == null) {
                cs = (OracleCallableStatement) conn.prepareCall(" begin ? := " + sInterface + ".putEsigFailure(?); end; ");
            } else if (action instanceof String && action.equals("OUTPUTOPTION")) {
                cs = (OracleCallableStatement) conn.prepareCall(" begin ? := " + sInterface + ".putOutputOption(?); end; ");
            } else if (action instanceof String && action.equals(UserInputData.SAVE_REPORT)) {
                cs = (OracleCallableStatement) conn.prepareCall(" begin ? := " + sInterface + ".putSaveDialog(?); end; ");
            } else if (action instanceof String && action.equals(UserInputData.SAVE_AUDIT)) {
                cs = (OracleCallableStatement) conn.prepareCall(" begin ? := " + sInterface + ".putAuditMask(?); end; ");
            } else if (action instanceof MenuEntryRecord && ((MenuEntryRecord) action).getToken().equals("CAN_CHANGE_OWN_PASSWORD")) {
                cs = (OracleCallableStatement) conn.prepareCall(" begin ? := " + schemaOwner + ".changeOwnPassword(?); end; ");
            } else if (action instanceof MenuEntryRecord) {
                cs = (OracleCallableStatement) conn.prepareCall(" begin ? := " + sInterface + ".putParameters(?, ?, ?); end; ");
//                cs = (OracleCallableStatement) conn.prepareCall(" begin ? := " + sInterface + ".putParameters(?, ?); end; ");
                Struct menuStruct;
                ComplexDatabaseRecord menuRec = new ComplexDatabaseRecord(conn, ((MenuEntryRecord) action).getStruct());

                menuStruct = menuRec.toDatum();
                cs.setObject(3, menuStruct);

                Clob clob = conn.createClob();
                if (clobParameter != null) {
                    clob.setString(1, clobParameter);
                }
                cs.setClob(4, clob);
            }

            cs.registerOutParameter(1, Types.STRUCT, schemaOwner + ".STB$OERROR$RECORD");

            Array inPropertyList = ((OracleConnection) conn).createARRAY(schemaOwner + ".STB$PROPERTY$LIST", structList);
            cs.setArray(2, inPropertyList);

            rs = (OracleResultSet) cs.executeQuery();
            Struct struct = cs.getSTRUCT(1);
            ErrorRecord err = checkError(struct);
            log.debug(err);
            //err.checkForInfo();
            closeCursor(cs, rs);

        } catch (SQLException se) {
            closeCursor(cs, rs);
            log.error(se);
            throw new BackendException(se.getMessage(), se, BackendException.SEVERITY_ACTION_EXIT);
        }

    }

    protected void changePassword(String newPassword, String oldPassword) throws BackendException {

        log.info("begin");
        updateSessionInfo();
        OracleCallableStatement cs = null;
        OracleResultSet rs = null;

        try {
            cs = (OracleCallableStatement) conn.prepareCall(" begin changePassword(?,?); end; ");
            cs.setString(1, newPassword);
            cs.setString(2, oldPassword);
            cs.execute();
            closeCursor(cs, rs);
        } catch (SQLException se) {
            closeCursor(cs, rs);
            throw new BackendException(se.getMessage(), se, BackendException.SEVERITY_INFORMATIONAL);

        }
    }

    /**
     * Called after an action is completed. Determines which TreeNodes to expand
     * and Children to reload (nodesToLoad) and which node to select ( newNode
     * ). Return value token is RIGHT if node selection should be in details
     * view. calls stb$gal.synchronize(STB$TREENODE$LIST out,
     * STB$TREENODE$RECORD out)
     *
     * @param nodesToLoad
     * @param newNode
     * @return
     * @throws NullPointerException
     * @throws BackendException
     */
    protected String synchronize(TreeNodeList nodesToLoad, TreeNodeRecord newNode, boolean adminMode)
            throws NullPointerException,
            BackendException {
        log.info("begin");
        OracleCallableStatement cs = null;
        OracleResultSet rs = null;
        try {
            if (adminMode) {
                cs = (OracleCallableStatement) conn.prepareCall(" begin ? := isr$install$dialog.synchronize(?, ?, ?); end; ");
            } else {
                updateSessionInfo();
                cs = (OracleCallableStatement) conn.prepareCall(" begin ? := stb$gal.synchronize(?, ?, ?); end; ");
            }
            cs.registerOutParameter(1, Types.STRUCT, schemaOwner + ".STB$OERROR$RECORD");
            cs.registerOutParameter(2, java.sql.Types.VARCHAR);
            cs.registerOutParameter(3, oracle.jdbc.OracleTypes.ARRAY, schemaOwner + ".STB$TREENODELIST");
            cs.registerOutParameter(4, Types.STRUCT, schemaOwner + ".STB$TREENODE$RECORD");
            cs.executeQuery();
            String token = cs.getString(2);
            Array array = cs.getArray(3);
            Object[] elements = (Object[]) array.getArray();
            for (Object element : elements) {
                Struct s = (Struct) element;
                ComplexDatabaseRecord rl = new ComplexDatabaseRecord(s, conn);
                nodesToLoad.add(new TreeNodeRecord(rl.getStruct()));
            }
            Struct neuNode = cs.getSTRUCT(4);
            ComplexDatabaseRecord newRec = new ComplexDatabaseRecord(neuNode, conn);
            newNode.setStruct(newRec.getStruct());
            closeCursor(cs, rs);
            log.info("end");
            return token;
        } catch (SQLException se) {
            closeCursor(cs, rs);
            throw new BackendException(se.getMessage(), se, BackendException.SEVERITY_APP_EXIT);
        }
    }

    String synchronizeInBackground(TreeNodeList nodesToLoad, TreeNodeRecord newNode, String
            nodetype, String nodeid, String sessionid)
            throws BackendException {
        String paramMsg = "newNode = " + newNode + " ; nodetype = " + nodetype+ " ; nodeid =  " + nodeid;
        log.info("begin: " + paramMsg);
        OracleCallableStatement cs = null;
        OracleResultSet rs = null;
        try {
            cs = (OracleCallableStatement) conn.prepareCall(" begin ? := " + "stb$gal.synchronizeInBackground(?, ?, ?, ?, ?, ?); end; ");
            cs.registerOutParameter(1, Types.STRUCT, schemaOwner + ".STB$OERROR$RECORD");
            cs.registerOutParameter(2, java.sql.Types.VARCHAR);
            cs.registerOutParameter(3, oracle.jdbc.OracleTypes.ARRAY, schemaOwner + ".STB$TREENODELIST");
            cs.registerOutParameter(4, Types.STRUCT, schemaOwner + ".STB$TREENODE$RECORD");
            cs.setString(5, nodetype);
            cs.setString(6, nodeid);
            cs.setString(7, sessionid);
            cs.executeQuery();
            String token = cs.getString(2);
            Array array = cs.getArray(3);
            if (array == null) {
                throw new RuntimeException("STB$TREENODELIST 'treeReloadList' is null! Parameters: " + paramMsg);
            }
            Object[] elements = (Object[]) array.getArray();
            for (Object element : elements) {
                Struct s = (Struct) element;
                ComplexDatabaseRecord rl = new ComplexDatabaseRecord(s, conn);
                nodesToLoad.add(new TreeNodeRecord(rl.getStruct()));
            }
            Struct neuNode = cs.getSTRUCT(4);
            if (neuNode == null) {
                throw new RuntimeException("STB$TREENODE$RECORD 'treeReloadNode' is null! Parameters: " + paramMsg);
            }
            ComplexDatabaseRecord newRec = new ComplexDatabaseRecord(neuNode, conn);
            newNode.setStruct(newRec.getStruct());
            closeCursor(cs, rs);
            log.info("end");
            return token;
        } catch (SQLException se) {
            closeCursor(cs, rs);
            throw new BackendException(se.getMessage(), se, BackendException.SEVERITY_APP_EXIT);
        }
    }

    /**
     * Cleans up database after report wizard was canceled calls
     * stb$gal.cancelWizard(STB$TREENODE$RECORD out)
     *
     * @return
     * @throws BackendException
     */
    protected TreeNodeRecord cleanUp() throws BackendException {

        log.info("begin");
        updateSessionInfo();
        OracleCallableStatement cs = null;
        OracleResultSet rs = null;

        try {
            cs = (OracleCallableStatement) conn.prepareCall(" begin stb$gal.cancelWizzard(?); end; ");
            cs.registerOutParameter(1, Types.STRUCT, schemaOwner + ".STB$TREENODE$RECORD");
            cs.executeQuery();
            Struct loadNode = cs.getSTRUCT(1);
            TreeNodeRecord baseLoadNode = null;
            if (loadNode != null) {
                ComplexDatabaseRecord loadRec = new ComplexDatabaseRecord(loadNode, conn);
                baseLoadNode = new TreeNodeRecord(loadRec.getStruct());
            }
            closeCursor(cs, rs);
            log.debug("end baseLoadNode = " + baseLoadNode);
            return baseLoadNode;
        } catch (SQLException se) {
            closeCursor(cs, rs);
            throw new BackendException(se.getMessage(), se, BackendException.SEVERITY_APP_EXIT);
        }
    }

    /**
     * saves the persistance data as Clob to db calls
     * stb$gal.setGuiPersistance(Clob in)
     *
     * @param persistance persistance data from GUI
     * @throws BackendException
     */
    protected void putPersistance(Properties persistance) throws BackendException {

        log.info("begin");
        updateSessionInfo();

        OracleCallableStatement cs = null;
        OracleResultSet rs = null;
        if (persistance.size() == 0) {
            return;
        }
        try {
            cs = (OracleCallableStatement) conn.prepareCall(" begin stb$gal.setGuiPersistance(?); end; ");
            Clob clob = conn.createClob();

            OutputStream ostream = clob.setAsciiStream(1);
            persistance.store(ostream, null);
            ostream.flush();
            ostream.close();
            cs.setClob(1, clob);
            cs.executeQuery();

            closeCursor(cs, rs);
            log.info("end");
        } catch (Exception e) {
            closeCursor(cs, rs);
            throw new BackendException(e.getMessage(), e, BackendException.SEVERITY_INFORMATIONAL);
        }
    }

    /**
     * calls stb$gal.getGuiPersistance(Clob out)
     *
     * @return a Properties Object with GUI Persistance data
     * @throws BackendException
     */
    protected Properties getPersistance() throws BackendException {

        log.info("begin");
        updateSessionInfo();

        OracleCallableStatement cs = null;
        OracleResultSet rs = null;
        try {
            cs = (OracleCallableStatement) conn.prepareCall(" begin stb$gal.getGuiPersistance(?); end; ");
            cs.registerOutParameter(1, java.sql.Types.CLOB);
            cs.executeQuery();
            Clob clob = cs.getClob(1);

            InputStream istream = clob.getAsciiStream();
            String tempDir = System.getProperty("java.io.tmpdir");
            log.debug("tempDir " + tempDir);
            OutputStream ostream = new FileOutputStream(new java.io.File(tempDir + "persistance.properties"));
            // OutputStream ostream = new FileOutputStream(new java.io.File("C:\\Temp\\persistance.properties"));

            Properties persistance = new Properties();
            persistance.load(istream);
            persistance.store(ostream, "SAVE");

            istream.close();
            ostream.close();

            closeCursor(cs, rs);

            return persistance;
        } catch (Exception e) {
            closeCursor(cs, rs);
            throw new BackendException(e.getMessage(), e, BackendException.SEVERITY_INFORMATIONAL);
        }
    }

    /**
     * @return actual time of database
     * @throws BackendException
     */
    protected String getDBTime() throws BackendException {

        log.info("begin");
        PreparedStatement ps = null;
        ResultSet rs = null;
        try {
            String query = "select to_char(sysdate,'DD.MM.YYYY HH24:Mi:SS') from dual";
            ps = conn.prepareStatement(query);
            rs = ps.executeQuery();
            rs.next();
            return rs.getString(1);
        } catch (SQLException e) {
            try {
                rs.close();
                ps.close();
            } catch (SQLException ignored) {
                ignored.printStackTrace();
            }
            throw new BackendException(e.getMessage(), e, BackendException.SEVERITY_INFORMATIONAL);
        }
    }

    /**
     * gets the MenuEntryRecord related to this token. Needed for default
     * actions. calls stb$gal.getMenu(varchar2 in, STB$MenuEntry$Record out)
     *
     * @param token
     * @return
     * @throws BackendException
     */
    protected MenuEntryRecord getMenu(String token, boolean adminMode) throws BackendException {

        log.info("begin: token=" + token);
        OracleCallableStatement cs = null;
        OracleResultSet rs = null;
        try {
            if (adminMode) {
                cs = (OracleCallableStatement) conn.prepareCall(" begin ? := isr$install$dialog.getMenu(?,?); end; ");
            } else {
                updateSessionInfo();
                cs = (OracleCallableStatement) conn.prepareCall(" begin ? := stb$gal.getMenu(?,?); end; ");
            }
            cs.registerOutParameter(1, Types.STRUCT, schemaOwner + ".STB$OERROR$RECORD");
            cs.setString(2, token);
            cs.registerOutParameter(3, oracle.jdbc.OracleTypes.ARRAY, schemaOwner + ".STB$MENUENTRY$LIST");
            cs.executeQuery();
            Struct errRec = cs.getSTRUCT(1);
            checkError(errRec);
            Array array = cs.getArray(3);
            //log.debug("array = " + array);
            Object[] elements = (Object[]) array.getArray();
            Struct s = (Struct) elements[0];
            ComplexDatabaseRecord rl = new ComplexDatabaseRecord(s, conn);
            MenuEntryRecord mer = new MenuEntryRecord(rl.getStruct());
            closeCursor(cs, rs);
            log.debug("end");
            return mer;
        } catch (SQLException se) {
            closeCursor(cs, rs);
            throw new BackendException(se.getMessage() + "; token = " + token, se, BackendException.SEVERITY_INFORMATIONAL);
        }
    }

    protected String getJobParameter(String parametername, int jobid) throws BackendException {
        log.info("begin: parameter=" + parametername);
        String parametervalue;
        OracleCallableStatement cs = null;
        OracleResultSet rs = null;
        try {
            cs = (OracleCallableStatement) conn.prepareCall(" begin ? := stb$gal.getJobParameter(?,?); end; ");
//            PreparedStatementParameterSetter setter = new PreparedStatementParameterSetter(cs);
            cs.registerOutParameter(1, java.sql.Types.VARCHAR);
            cs.setString(2, parametername);
            cs.setInt(3, jobid);
//            log.debug("setter.toString() = " + setter.prepareSql(" begin ? := stb$job.getJobParameter(?,?); end; "));
            cs.executeQuery();
            parametervalue = cs.getString(1);
            closeCursor(cs, rs);
            log.info("end " + parametervalue);
            return parametervalue;
        } catch (SQLException se) {
            closeCursor(cs, rs);
            throw new BackendException(se.getMessage(), se, BackendException.SEVERITY_INFORMATIONAL);
        }
    }


    protected void setLoaderConnection(String port, String user, boolean start) throws BackendException {
        log.info("begin: port = " + port + "; user = " + user);
        OracleCallableStatement cs = null;
        OracleResultSet rs = null;
        try {
            cs = (OracleCallableStatement) conn.prepareCall("begin ? :=  stb$gal.setLoaderConnection( ?, ?, ? ); end; ");
            cs.registerOutParameter(1, Types.STRUCT, schemaOwner + ".STB$OERROR$RECORD");
            cs.setString(2, port);
            cs.setString(3, user);
            cs.setString(4, start ? "T" : "F");
            rs = (OracleResultSet) cs.executeQuery();
            Struct errRec = cs.getSTRUCT(1);
            checkError(errRec);

            closeCursor(cs, rs);
            log.info("END");
        } catch (SQLException se) {
            closeCursor(cs, rs);
            throw new BackendException(se.getMessage(), se, BackendException.SEVERITY_INFORMATIONAL);
        }
    }


    /**
     * if severity is critical, warning or message BackendErrorException is
     * thrown
     *
     * @param s the data structure
     * @return an ErrorRecord anyway, might have more information
     * @throws SQLException
     * @throws BackendErrorException
     * @see ErrorRecord
     */
    private ErrorRecord checkError(Struct s) throws SQLException, BackendErrorException {
        ComplexDatabaseRecord rec = new ComplexDatabaseRecord(s, conn);
        ErrorRecord err = new ErrorRecord(rec.getStruct());
        err.errCheck();
        return err;
    }

    /**
     * called by db-queriing methods, ensures cursors are closed
     */
    private void closeCursor(OracleCallableStatement cs, OracleResultSet rs) {

        try {
            if (rs != null) {
                rs.close();
            }
            if (cs != null) {
                cs.close();
            }
        } catch (Exception ignored) {
            ignored.printStackTrace();
        }
    }

    /**
     * called when a dialog was canceled calls stb$gal.cancelMask
     *
     * @return
     */
    protected boolean cancel(boolean adminMode) {

        OracleCallableStatement cs = null;
        OracleResultSet rs = null;
        try {
            if (adminMode) {
                cs = (OracleCallableStatement) conn.prepareCall(" begin isr$install$dialog.cancelMask(); end; ");
            } else {
                updateSessionInfo();
                cs = (OracleCallableStatement) conn.prepareCall(" begin stb$gal.cancelMask(); end; ");
            }
            rs = (OracleResultSet) cs.executeQuery();
            closeCursor(cs, rs);
        } catch (Exception e) {
            closeCursor(cs, rs);
            e.printStackTrace();
            return false;
        }
        return true;
    }

    /**
     * called when the dispatcher needs to know if reload the explorer
     *
     * @return
     */
    protected boolean checkReload() {

        String retVal;
        OracleCallableStatement cs = null;
        OracleResultSet rs = null;
        try {
            cs = (OracleCallableStatement) conn.prepareCall(" begin ? := stb$gal.checkReload(?); end; ");
            cs.registerOutParameter(1, Types.STRUCT, schemaOwner + ".STB$OERROR$RECORD");
            cs.registerOutParameter(2, java.sql.Types.VARCHAR);
            cs.executeQuery();
            retVal = cs.getString(2);
            closeCursor(cs, rs);
        } catch (Exception e) {
            closeCursor(cs, rs);
            return false;
        }
        return retVal.equals("T");
    }

    /**
     * called on ISR exit calls stb$gal.destroyOldValues
     *
     * @return
     */
    protected boolean finalise(int reason) {

        OracleCallableStatement cs = null;
        OracleResultSet rs = null;
        try {
            cs = (OracleCallableStatement) conn.prepareCall(" begin stb$gal.destroyOldValues(?); end; ");
            cs.setInt(1, reason);
            rs = (OracleResultSet) cs.executeQuery();
            closeCursor(cs, rs);
        } catch (Exception e) {
            closeCursor(cs, rs);
            e.printStackTrace();
            return false;
        }
        return true;
    }

    /**
     * sets the port id from the client calls stb$gal.setClientPort
     *
     * @return
     */
    protected boolean setClientPort(int basisPort) throws BackendException {

        log.info("begin: basisPort=" + basisPort);
        OracleCallableStatement cs = null;
        OracleResultSet rs = null;
        try {
            cs = (OracleCallableStatement) conn.prepareCall(" begin ? := stb$gal.setClientPort( ? ); end; ");
            cs.registerOutParameter(1, Types.STRUCT, schemaOwner + ".STB$OERROR$RECORD");
            cs.setInt(2, basisPort);
            rs = (OracleResultSet) cs.executeQuery();
            Struct errRec = cs.getSTRUCT(1);
            checkError(errRec);

            closeCursor(cs, rs);
            log.info("end");
        } catch (SQLException e) {
            return setClientPortWithoutReturn(basisPort);  //compatibility
        }
        return true;
    }

    //because compatibility, delete later
    protected boolean setClientPortWithoutReturn(int basisPort) {

        log.info("begin: basisPort=" + basisPort);
        OracleCallableStatement cs = null;
        OracleResultSet rs = null;
        try {
            cs = (OracleCallableStatement) conn.prepareCall(" begin stb$gal.setClientPort( ? ); end; ");
            cs.setInt(1, basisPort);
            rs = (OracleResultSet) cs.executeQuery();

            closeCursor(cs, rs);
            log.info("end");
        } catch (Exception e) {
            closeCursor(cs, rs);
            e.printStackTrace();
            return false;
        }

        return true;
    }

    /**
     * gets a paramter from stb$systemparameter
     *
     * @return
     */
    protected String getParameterValue(String parameter) {

        log.info("begin: parameter=" + parameter);
        PreparedStatement ps = null;
        ResultSet rs = null;

        try {
            String query = "select stb$gal.getParameterValue(?) from dual";
            ps = conn.prepareStatement(query);
            ps.setString(1, parameter);
            rs = ps.executeQuery();
            rs.next();
            parameter = rs.getString(1);
            rs.close();
            ps.close();

        } catch (SQLException se) {
            try {
                log.info(se);
                ps.close();
                rs.close();
            } catch (Exception ignored) {
                log.error(ignored);
            }
        }
        log.debug("end " + parameter);
        return parameter;
    }

    /**
     * gets the java loglevel
     *
     * @return
     */
    protected String getJavaLogLevel() {

        log.info("begin");
        PreparedStatement ps = null;
        ResultSet rs = null;
        String javaLogLevel = "DEBUG";

        try {
            String query = "select stb$gal.getJavaLogLevel from dual";
            ps = conn.prepareStatement(query);
            rs = ps.executeQuery();
            rs.next();
            javaLogLevel = rs.getString(1);
            rs.close();
            ps.close();
            log.info("end; javaLogLevel = " + javaLogLevel);
        } catch (SQLException se) {
            se.printStackTrace();
            try {
                ps.close();
                rs.close();
            } catch (Exception ignored) {
                ignored.printStackTrace();
            }
        }

        return javaLogLevel;
    }

    /**
     * gets a blob from isr$icon
     *
     * @return
     */
    protected byte[] getIcon(String size, String id) {

        log.trace("begin: icon id=" + id);
        OracleCallableStatement cs = null;
        OracleResultSet rs = null;
        try {

            cs = (OracleCallableStatement) conn.prepareCall(" begin ? := stb$gal.getIcon(?, ?); end; ");
            cs.registerOutParameter(1, java.sql.Types.BLOB);
            cs.setString(2, size);
            cs.setString(3, id);
            cs.executeQuery();

            Blob blob = cs.getBlob(1);
            byte[] icon = new byte[new Long(blob.length()).intValue()];
            InputStream in = blob.getBinaryStream();
            in.read(icon);
            in.close();

            closeCursor(cs, rs);
            return icon;

        } catch (Exception e) {
            closeCursor(cs, rs);
        }

        return null;
    }

    /**
     * gets xml from database
     *
     * @return
     */
    public byte[] getXml(MenuEntryRecord menuEntry, byte[] action) throws
            NullPointerException, BackendException {

        long duration = System.currentTimeMillis();
        log.info("begin " + menuEntry);
        OracleCallableStatement cs = null;
        OracleResultSet rs = null;
        try {

            updateSessionInfo();

            cs = (OracleCallableStatement) conn.prepareCall(" begin ? := stb$gal.getXml(?, ?, ?); end; ");

            cs.registerOutParameter(1, Types.STRUCT, schemaOwner + ".STB$OERROR$RECORD");
            ComplexDatabaseRecord rec = new ComplexDatabaseRecord(conn, menuEntry.getStruct());
            Struct struct = rec.toDatum();
            cs.setObject(2, struct);
            cs.registerOutParameter(3, java.sql.Types.CLOB);
            if (action != null) {
                cs.setClob(4, ConvertUtils.getClob(conn, action));
            } else {
                cs.setCLOB(4, null);
            }

            cs.executeQuery();

            Struct errRec = cs.getSTRUCT(1);
            ErrorRecord err = checkError(errRec);

            Clob clob = cs.getClob(3);
            closeCursor(cs, rs);
            duration = System.currentTimeMillis() - duration;
            log.info("end duration " + duration);
            return ConvertUtils.getBytes(clob);

        } catch (SQLException se) {
            se.printStackTrace();
            closeCursor(cs, rs);
            throw new BackendException(se.getMessage(), se, BackendException.SEVERITY_APP_EXIT);
        } catch (Exception e) {
            e.printStackTrace();
            throw new BackendException(e.getMessage(), e, BackendException.SEVERITY_APP_EXIT);
        }
    }

    /**
     * puts xml to database
     *
     * @return
     */
    public void putXml(MenuEntryRecord menuEntry, byte[] action) throws
            NullPointerException, BackendException, IOException {

        log.info("begin");
        OracleCallableStatement cs = null;
        OracleResultSet rs = null;
        try {
            ErrorRecord err = null;
            updateSessionInfo();

            cs = (OracleCallableStatement) conn.prepareCall(" begin ? := stb$gal.putXml(?, ?); end; ");

            cs.registerOutParameter(1, Types.STRUCT, schemaOwner + ".STB$OERROR$RECORD");
            ComplexDatabaseRecord rec = new ComplexDatabaseRecord(conn, menuEntry.getStruct());
            Struct struct = rec.toDatum();
            cs.setObject(2, struct);
            cs.setClob(3, ConvertUtils.getClob(conn, action));

            cs.executeQuery();

            Struct errRec = cs.getSTRUCT(1);
            err = checkError(errRec);

            closeCursor(cs, rs);

        } catch (Exception se) {
            se.printStackTrace();
            closeCursor(cs, rs);
            throw new BackendException(se.getMessage(), se, BackendException.SEVERITY_APP_EXIT);
        }
    }

    /**
     * gets node details from database
     */
    public byte[] getNodeDetails() throws NullPointerException, BackendException {

        log.info("begin");
        OracleCallableStatement cs = null;
        OracleResultSet rs = null;
        try {

            updateSessionInfo();

            cs = (OracleCallableStatement) conn.prepareCall(" begin ? := stb$gal.getNodeDetails(?); end; ");

            cs.registerOutParameter(1, Types.STRUCT, schemaOwner + ".STB$OERROR$RECORD");
            cs.registerOutParameter(2, java.sql.Types.CLOB);

            try {
                cs.executeQuery();
            } catch (SQLException e) {
                if (e.getErrorCode() == 6550) { //try to execute a invalid pl/sql block
                    return null;
                } else {
                    throw e;
                }
            }

            Struct errRec = cs.getSTRUCT(1);
            ErrorRecord err = checkError(errRec);

            Clob clob = cs.getClob(2);
            closeCursor(cs, rs);
            log.debug(clob);
            byte[] bytes = ConvertUtils.getBytes(clob);
            log.info("end");
            return bytes;

        } catch (Exception se) {
            se.printStackTrace();
            closeCursor(cs, rs);
            throw new BackendException(se.getMessage(), se, BackendException.SEVERITY_APP_EXIT);
        }
    }


    ArrayList getCurrentEntityList() {
        return entityList;
    }

    FormInfoRecord getCurrentFormInfoRecord() {
        return formInfoRecord;
    }

    SelectionList getCurrentSelectionList() {
        log.debug(selectionList.getData());
        return selectionList;
    }

    boolean getModifiedFlag() {
        return bModifiedFlag;
    }

    String getFullUserName() {
        return fullUsername;
    }

    /* INSTALLER - DATABASE - INTERFACE
     *
     *
     *
     */
    protected boolean checkRole(String sRole) {

        log.info("begin");
        boolean success = false;

        PreparedStatement ps = null;
        ResultSet rs = null;
        String stmt = " select 'T', user from user_role_privs where granted_role IN ('" + sRole.toUpperCase() + "') ";
        log.debug(stmt);
        try {
            ps = conn.prepareStatement(stmt);
            rs = ps.executeQuery();
            rs.next();
            String retval = rs.getString(1);
            if (retval.equals("T")) {
                success = true;
                this.schemaOwner = rs.getString(2);
            }
        } catch (SQLException se) {
            success = false;
        } finally {
            try {
                ps.close();
                rs.close();
            } catch (Exception e) {
                e.printStackTrace();
            }
        }

        return success;
    }

    protected boolean preInstall(String sPackage, String jarFileName, String jarFileVersion) {

        log.info("begin");
        boolean success;

        OracleCallableStatement cs = null;
        OracleResultSet rs = null;
        try {
            cs = (OracleCallableStatement) conn.prepareCall(" begin ? := " + sPackage + ".prepareFiles(?,?); end; ");
            cs.registerOutParameter(1, java.sql.Types.VARCHAR);
            cs.setString(2, jarFileName);
            cs.setString(3, jarFileVersion);

            rs = (OracleResultSet) cs.executeQuery();

            String retval = cs.getString(1);

            if (retval.equals("T")) {
                success = true;
            } else {
                throw new SQLException(retval);
            }
        } catch (SQLException se) {
            closeCursor(cs, rs);
            success = false;
        } finally {
            closeCursor(cs, rs);
        }

        return success;
    }

    protected boolean preInstallFile(String object) throws BackendException {

        boolean success;
        Statement stmt = null;
        try {
            stmt = conn.createStatement();
            stmt.execute(object);
            stmt.close();
            success = true;
        } catch (SQLException se) {
            try {
                if (stmt != null) {
                    stmt.close();
                }
            } catch (SQLException ex) {
                log.error(ex);
            }
            throw new BackendException(se.getMessage(), se, BackendException.SEVERITY_APP_EXIT);
        }
        return success;
    }

    protected boolean loadFile(String sPackage, File loadFile, String type, String objectType,
                               String objectDescription, String objectVersion, String jarFileName, String jarFileVersion) throws
            BackendException {

        log.info("begin");
        OracleCallableStatement cs = null;
        OracleResultSet rs = null;
        boolean success = false;
        try {
            cs = (OracleCallableStatement) conn.prepareCall(" begin ? := " + sPackage + ".loadFile(?,?,?,?,?,?,?,?,?); end; ");
            cs.registerOutParameter(1, java.sql.Types.VARCHAR);
            cs.setString(2, loadFile.getName());
            cs.setString(3, type);
            cs.setString(4, objectType);
            cs.setString(5, objectDescription);
            cs.setString(6, objectVersion);
            cs.setString(7, jarFileName);
            cs.setString(8, jarFileVersion);
            // JAVA, CHECK, PREINSTALL, USERPARAMETER, PREINSTALLJAR, INSTALLJAR (DON'T LOAD)
            if (type.equals("JAVA") || type.equals("CHECK") || type.equals("PREINSTALL") || type.equals("USERPARAMETER") || type.equals("INSTALLJAR") || type.equals("PREINSTALLJAR")) {
                cs.setBinaryStream(9, null, 0);
                cs.setCharacterStream(10, null, 0);
            } else {
                if (type.equals("DML") || type.equals("DDL") || type.equals("SQL_EXEC")) {
                    cs.setBinaryStream(9, null, 0);
                } // LOAD, LOADTOSERVER or other
                else {
                    cs.setBinaryStream(9, loadFile.exists() ? new BufferedInputStream(new FileInputStream(loadFile)) : null, (int) loadFile.length());
                }
                // DML, DDL, SQL_EXEC, LOAD, LOADTOSERVER or other
                cs.setCharacterStream(10, loadFile.exists() ? new BufferedReader(new FileReader(loadFile)) : null, (int) loadFile.length());
            }

            rs = (OracleResultSet) cs.executeQuery();

            String retval = cs.getString(1);

            if (retval.equals("T")) {
                success = true;
            } else {
                throw new SQLException(retval);
            }
        } catch (SQLException | FileNotFoundException se) {
            closeCursor(cs, rs);
            throw new BackendException(se.getMessage(), se, BackendException.SEVERITY_APP_EXIT);
        } finally {
            closeCursor(cs, rs);
        }

        return success;
    }

    protected int getCurrentFileId(String sPackage, String fileName, String type, String objectType, String
            objectDescription, String objectVersion, String jarFileName, String jarFileVersion) throws BackendException {

        log.info("begin");
        // -8	   - File can be executed again
        // -6	   - File has to be executed again
        // -5	   - File in repository with user modifications
        // -4	   - File in repository in a higher version
        // -3	   - File already executed
        // -2      - Function finished unsuccessfully
        // -1      - File in repository
        //  0      - File not in installation table
        //  1 .. x - File is executable
        int fileId = -2;
        OracleCallableStatement cs = null;
        OracleResultSet rs = null;

        try {
            cs = (OracleCallableStatement) conn.prepareCall(" begin ? := " + sPackage + ".getCurrentFileId(?,?,?,?,?,?,?,?); end; ");
            cs.registerOutParameter(1, java.sql.Types.INTEGER);
            cs.registerOutParameter(9, java.sql.Types.VARCHAR);
            cs.setString(2, fileName);
            cs.setString(3, type);
            cs.setString(4, objectType);
            cs.setString(5, objectDescription);
            cs.setString(6, objectVersion);
            cs.setString(7, jarFileName);
            cs.setString(8, jarFileVersion);
            rs = (OracleResultSet) cs.executeQuery();
            fileId = cs.getInt(1);
            String successFlag = cs.getString(9);
            closeCursor(cs, rs);
            switch (successFlag) {
                case "0":
                case "-2":
                case "-3":
                    return new Integer(successFlag);
                case "-1":
                    throw new BackendException("skip " + fileId, new SQLException(), BackendException.SEVERITY_INFORMATIONAL);
                case "-4":
                    throw new BackendException("xml " + fileId, new SQLException(), BackendException.SEVERITY_INFORMATIONAL);
                case "-5":
                    throw new BackendException("dialog2 " + fileId, new SQLException(), BackendException.SEVERITY_INFORMATIONAL);
                case "-8":
                    throw new BackendException("dialog3 " + fileId, new SQLException(), BackendException.SEVERITY_INFORMATIONAL);
                default:
                    return fileId;
            }
        } catch (SQLException se) {
            se.printStackTrace();
        } finally {
            closeCursor(cs, rs);
        }
        return fileId;
    }

    protected boolean executeCheck(String sFunction) throws BackendException {
        log.info("begin");
        boolean success;

        OracleCallableStatement cs = null;
        OracleResultSet rs = null;
        try {
            cs = (OracleCallableStatement) conn.prepareCall(" begin ? := " + sFunction + "; end; ");
            cs.registerOutParameter(1, java.sql.Types.VARCHAR);
            rs = (OracleResultSet) cs.executeQuery();
            String retval = cs.getString(1);
            closeCursor(cs, rs);
            if (retval.equals("T")) {
                success = true;
            } else {
                throw new SQLException(retval);
            }
        } catch (SQLException se) {
            closeCursor(cs, rs);
            throw new BackendException(se.getMessage(), se, BackendException.SEVERITY_APP_EXIT);
        }
        return success;
    }


    protected boolean executeFile(String sPackage, int fileId) throws BackendException {

        log.info("begin");
        boolean success;

        OracleCallableStatement cs = null;
        OracleResultSet rs = null;
        try {
            cs = (OracleCallableStatement) conn.prepareCall(" begin ? := " + sPackage + ".runScript(?); end; ");
            cs.registerOutParameter(1, java.sql.Types.VARCHAR);
            cs.setInt(2, fileId);
            rs = (OracleResultSet) cs.executeQuery();
            String retval = cs.getString(1);
            closeCursor(cs, rs);
            if (retval.equals("T")) {
                success = true;
            } else {
                throw new SQLException(retval);
            }
        } catch (SQLException se) {
            closeCursor(cs, rs);
            // Installer has to care about the error message
            if (se.getMessage().equals("F")) {
                HashMap list = getError(fileId);
                throw new BackendException(list.get("severity_code").toString() + " " + Integer.toString(fileId), se, BackendException.SEVERITY_INFORMATIONAL);
            } else {
                setSuccessFlag(sPackage, fileId, "F");
                throw new BackendException(se.getMessage(), se, BackendException.SEVERITY_APP_EXIT);
            }
        }
        return success;
    }


    protected boolean installFile(String sPackage, int fileId, String table, String column, String row, String
            rowValue) throws BackendException {

        log.info("begin");
        boolean success;

        OracleCallableStatement cs = null;
        OracleResultSet rs = null;
        try {
            cs = (OracleCallableStatement) conn.prepareCall(" begin ? := " + sPackage + ".installFile(?,?,?,?,?); end; ");
            cs.registerOutParameter(1, java.sql.Types.VARCHAR);
            cs.setInt(2, fileId);
            cs.setString(3, table);
            cs.setString(4, column);
            cs.setString(5, row);
            cs.setString(6, rowValue);
            rs = (OracleResultSet) cs.executeQuery();
            String retval = cs.getString(1);
            closeCursor(cs, rs);
            if (retval.equals("T")) {
                success = true;
            } else {
                throw new SQLException(retval);
            }
        } catch (SQLException se) {
            closeCursor(cs, rs);
            // Installer has to care about the error message
            if (se.getMessage().equals("F")) {
                HashMap list = getError(fileId);
                throw new BackendException(list.get("severity_code").toString() + " " + Integer.toString(fileId), se, BackendException.SEVERITY_INFORMATIONAL);
            } else {
                setSuccessFlag(sPackage, fileId, "F");
                throw new BackendException(se.getMessage(), se, BackendException.SEVERITY_APP_EXIT);
            }
        }
        return success;
    }

    protected boolean postInstall(String sPackage, String jarFileName, String jarFileVersion) throws
            BackendException {

        log.info("begin");
        boolean success;

        OracleCallableStatement cs = null;
        OracleResultSet rs = null;
        try {
            cs = (OracleCallableStatement) conn.prepareCall(" begin ? := " + sPackage + ".clearFiles(?,?); end; ");
            cs.registerOutParameter(1, java.sql.Types.VARCHAR);
            cs.setString(2, jarFileName);
            cs.setString(3, jarFileVersion);

            rs = (OracleResultSet) cs.executeQuery();

            String retval = cs.getString(1);

            if (retval.equals("T")) {
                success = true;
            } else {
                throw new SQLException(retval);
            }
        } catch (SQLException se) {
            closeCursor(cs, rs);
            throw new BackendException(se.getMessage(), se, BackendException.SEVERITY_APP_EXIT);
        } finally {
            closeCursor(cs, rs);
        }

        return success;
    }

    protected void setSuccessFlag(String sPackage, int fileId, String success) {

        log.info("begin");
        OracleCallableStatement cs = null;
        OracleResultSet rs = null;
        try {
            cs = (OracleCallableStatement) conn.prepareCall(" begin " + sPackage + ".setSuccessFlag(?, ?); end; ");
            cs.setInt(1, fileId);
            cs.setString(2, success);
            rs = (OracleResultSet) cs.executeQuery();
        } catch (SQLException se) {
            se.printStackTrace();
        } finally {
            closeCursor(cs, rs);
        }
    }

    protected HashMap getError(int fileId) {

        log.info("begin");
        HashMap<String, Object> list = new HashMap<>();

        OracleCallableStatement cs = null;
        OracleResultSet rs = null;
        try {
            cs = (OracleCallableStatement) conn.prepareCall(" select err1.entryid, err1.referenceid, errorcode, errormessage, severity_code, statement from isr$installerrors err1, (select max(entryid) entryid from isr$installerrors  where referenceid = ? ) err2 where referenceid = ? and err1.entryid = err2.entryid ");
            cs.setInt(1, fileId);
            cs.setInt(2, fileId);
            rs = (OracleResultSet) cs.executeQuery();
            rs.next();
            list.put("entryid", rs.getString("entryid"));
            list.put("referenceid", rs.getString("referenceid"));
            list.put("errorcode", rs.getString("errorcode"));
            list.put("errormessage", rs.getString("errormessage"));
            list.put("severity_code", rs.getString("severity_code"));
            list.put("statement", rs.getString("statement"));
        } catch (SQLException se) {
            se.printStackTrace();
        } finally {
            closeCursor(cs, rs);
        }

        return list;
    }

    protected void setError(String sPackage, String errorCode, String errorMessage, String errorStatement, String
            exceptionHandling, int entryid, int referenceid) {

        log.info("begin");
        OracleCallableStatement cs = null;
        OracleResultSet rs = null;
        try {
            if (entryid == 0) {
                cs = (OracleCallableStatement) conn.prepareCall(" begin " + sPackage + ".setError(?,?,?,?,?); end; ");
            } else {
                cs = (OracleCallableStatement) conn.prepareCall(" begin " + sPackage + ".setError(?,?,?,?,?,?); end; ");
            }
            cs.setInt(1, referenceid);
            cs.setString(2, errorCode);
            cs.setString(3, errorMessage);
            cs.setString(4, errorStatement);
            cs.setString(5, exceptionHandling);
            if (entryid != 0) {
                cs.setInt(6, entryid);
            }
            rs = (OracleResultSet) cs.executeQuery();
        } catch (SQLException se) {
            se.printStackTrace();
        } finally {
            closeCursor(cs, rs);
        }
    }

    void updateTree() {
//        log.debug("begin");
        OracleCallableStatement cs = null;
        OracleResultSet rs = null;
        try {
            cs = (OracleCallableStatement) conn.prepareCall(" begin STB$GAL.updateTree(); end; ");
            rs = (OracleResultSet) cs.executeQuery();
        } catch (Exception e) {
            log.error(e);
            e.printStackTrace();
        } finally {
            closeCursor(cs, rs);
//            log.debug("end");
        }
    }


}
