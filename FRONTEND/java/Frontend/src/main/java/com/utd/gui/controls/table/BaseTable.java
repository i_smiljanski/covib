package com.utd.gui.controls.table;

import com.jidesoft.grid.HierarchicalTable;
import com.uptodata.isr.gui.util.Colors;
import com.utd.gui.action.ActionUtil;
import com.utd.gui.controls.ControlUtils;
import com.utd.stb.back.data.client.AttributeList;
import com.utd.stb.inter.items.DisplayableItem;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import javax.swing.*;
import javax.swing.table.TableColumn;
import javax.swing.table.TableModel;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.FocusEvent;
import java.awt.event.FocusListener;
import java.awt.event.KeyEvent;


/**
 * eats &lt;Ctrl-A&gt; (default, change with processCtrlA), and &lt;Enter&gt;
 * (-&gt; processEnter) so keyboard-shortcuts works again,
 * <p>
 * JTable extension a bit tweaked:
 * <ul>
 * <p>
 * <li>autosize columns on Header-DblClick
 * <li>hilite color change on focus lost
 * <li>autoresize off
 * <li>CornerComponent in top-right corner of scrollpane, see:
 * {@link com.utd.gui.controls.table.CornerComponent}
 * </ul>
 * <p>
 * <p>
 * ?? remove the hack with processEnter, just remove it from the inputtable: it
 * seems this doesn't work, also if removed from all Maps
 * (WHEN_IN_FOCUSED_WINDOW,... )
 *
 * @author PeterBuettner.de
 */
public class BaseTable extends HierarchicalTable {
    public static final Logger log = LogManager.getRootLogger();

    private boolean processCtrlA;
    private boolean processEnter;

    public BaseTable(TableModel dm) {

        super(dm);
        // autosize on dbl-header-click
        setAutoResizeMode(AUTO_RESIZE_OFF);
        if (tableHeader != null)
            tableHeader
                    .addMouseListener(new AutoSizeMouseAdapter(true));

        ActionUtil.putIntoActionMap(this, new ColumnSizer(-5, KeyStroke
                .getKeyStroke("ctrl shift LEFT")));
        ActionUtil.putIntoActionMap(this, new ColumnSizer(5, KeyStroke
                .getKeyStroke("ctrl shift RIGHT")));

        // background of selected cells is different with/without focus, so
        // repaint:
        addFocusListener(new FocusListener() {

            @Override
            public void focusGained(FocusEvent e) {
                repaint();
            }

            @Override
            public void focusLost(FocusEvent e) {
                repaint();
            }
        });

    }

    protected boolean isProcessCtrlA() {

        return processCtrlA;
    }

    protected void setProcessCtrlA(boolean processCtrlA) {

        this.processCtrlA = processCtrlA;
    }

    protected boolean isProcessEnter() {

        return processEnter;
    }

    protected void setProcessEnter(boolean processEnter) {

        this.processEnter = processEnter;
    }

    //	remove 'ctrl a' and 'Enter' to use them in the whole form
    public boolean processKeyBinding(KeyStroke ks, KeyEvent e, int condition,
                                     boolean pressed) {

        if (e.getKeyCode() == 'A' && e.isControlDown() && !processCtrlA) return false;
        if (e.getKeyCode() == KeyEvent.VK_ENTER && !processEnter) return false;
        return super.processKeyBinding(ks, e, condition, pressed);
    }

    public Color getSelectionForeground() {

        if (!isShowLiteHilite()) return UIManager.getColor("Table.selectionForeground");
        return Colors.getItemHiliteInactiveText();
    }

    public Color getSelectionBackground() {

        if (!isShowLiteHilite()) return UIManager.getColor("Table.selectionBackground");
        return Colors.getItemHiliteInactiveBack();
    }

    /**
     * May be overwritten, if returns true a lite (e.g. lite gray) background is
     * shown, if false a darker (e.g. dark blue) one is used, textcolor is a
     * contrast one, normally a table shows darkHilite if table
     *
     * @return
     */

    protected boolean isShowLiteHilite() {

        return !ControlUtils.isPermFocusAndAppActive(this) && !isEditing();
        // return !isFocusOwner() && !isEditing();
    }

    /*
     * overwritten to add a cornercomponent in the upper right
     */
    protected void configureEnclosingScrollPane() {

        super.configureEnclosingScrollPane();

        JScrollPane sp = ControlUtils.getEnclosingScrollPane(this);
        if (sp == null) return;
        sp.setCorner(JScrollPane.UPPER_RIGHT_CORNER, new CornerComponent());

    }

    private final class ColumnSizer extends AbstractAction {

        private int delta = 5;

        ColumnSizer(int delta, KeyStroke keyStroke) {

            this.delta = delta;
            putValue(ACCELERATOR_KEY, keyStroke);

        }

        public void actionPerformed(ActionEvent e) {

            sizeFocusColumn(delta);
        }

    }

    /**
     * adds delta to the preferred width of the column of the focussed cell
     *
     * @param delta
     */

    private void sizeFocusColumn(int delta) {

        int col = columnModel.getSelectionModel().getAnchorSelectionIndex();
        if (col == -1) return;
        TableColumn tc = getColumnModel().getColumn(col);
        int width = tc.getPreferredWidth() + delta;
        if (width < 0) width = 0;
        tc.setPreferredWidth(width);

    }

    // MCD, 17.Jan 2006
    public boolean isCellEditable(int row, int column) {

        Object value = getValueAt(row, column);
        AttributeList attrList = null;
        if (value instanceof DisplayableItem) {
            DisplayableItem item = (DisplayableItem) value;
            attrList = (AttributeList) item.getAttributeList();
            log.debug(attrList);
        }
        boolean isEditable = attrList != null && attrList.isEditable();
//        log.debug("isCellEditable: " + value + " " + isEditable);
        return isEditable;
    }

}