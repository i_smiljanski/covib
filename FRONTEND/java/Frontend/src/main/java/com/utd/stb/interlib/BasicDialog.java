package com.utd.stb.interlib;

import com.utd.stb.inter.action.ButtonItem;
import com.utd.stb.inter.action.ButtonItems;
import com.utd.stb.inter.dialog.HeaderBar;
import com.utd.stb.inter.dialog.HelpSource;
import com.utd.stb.inter.dialog.UserDialog;
import com.utd.stb.inter.userinput.UserInputs;


/**
 * A basic template for dialogs, defines buttons ready to use
 *
 * @author PeterBuettner.de
 */
abstract public class BasicDialog implements UserDialog {

    /**
     * Ok Button, don't needs valid data
     */
    static final protected ButtonItem bOk = new StandardButton(ButtonItem.OK,
            false);

    /**
     * Ok Button, needs valid data
     */
    static final protected ButtonItem bOkNeedValidData = new StandardButton(ButtonItem.OK,
            true);

    /**
     * Cancel Button
     */
    static final protected ButtonItem bCancel = new StandardButton(
            ButtonItem.CANCEL,
            false);

    /**
     * Yes Button
     */
    static final protected ButtonItem bYes = new StandardButton(ButtonItem.YES,
            false);

    /**
     * No Button
     */
    static final protected ButtonItem bNo = new StandardButton(ButtonItem.NO,
            false);

    /**
     * Save Button
     */
    static final protected ButtonItem bSave = new StandardButton(ButtonItem.SAVE,
            false);

    /**
     * Close Button
     */
    static final protected ButtonItem bClose = new StandardButton(ButtonItem.CLOSE,
            false);

    /**
     * New Button
     */
    static final protected ButtonItem bNew = new StandardButton(ButtonItem.NEW,
            false);

    /**
     * delete Button
     */
    static final protected ButtonItem bDelete = new StandardButton(ButtonItem.DELETE,
            false);

    protected String title;
    protected HeaderBar headerBar;
    protected UserInputs userInputs;
    protected HelpSource helpSource;
    protected ButtonItems buttonItems;

    /**
     * you may change all items after creation, you must set the buttonItems
     *
     * @param title      the window caption
     * @param userInputs maybe null until you want to show the dialog
     */
    public BasicDialog(String title, UserInputs userInputs) {

        this(title, null, null, userInputs);
    }

    /**
     * you may change all items after creation, you must set the buttonItems
     *
     * @param title      the window caption
     * @param helpSource maybe null
     * @param userInputs maybe null until you want to show the dialog
     */
    public BasicDialog(String title, HelpSource helpSource, UserInputs userInputs) {

        this(title, null, helpSource, userInputs);
    }

    /**
     * you may change all items after creation, you must set the buttonItems
     *
     * @param title      the window caption
     * @param headerBar  maybe null
     * @param helpSource maybe null
     * @param userInputs maybe null until you want to show the dialog
     */
    public BasicDialog(String title, HeaderBar headerBar, HelpSource helpSource,
                       UserInputs userInputs) {

        this.title = title;
        this.headerBar = headerBar;
        this.helpSource = helpSource;
        this.userInputs = userInputs;

    }

    public String getTitle() {

        return title;
    }

    public HeaderBar getHeaderBar() {

        return headerBar;
    }

    public UserInputs getUserInputs() {

        return userInputs;
    }

    public HelpSource getHelpSource() {

        return helpSource;
    }

    public ButtonItems getButtonItems() {

        return buttonItems;
    }

}