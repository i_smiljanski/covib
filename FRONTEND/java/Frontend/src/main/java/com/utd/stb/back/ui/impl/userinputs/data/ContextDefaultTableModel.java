package com.utd.stb.back.ui.impl.userinputs.data;

import com.jidesoft.converter.ConverterContext;
import com.jidesoft.grid.ContextSensitiveTableModel;
import com.jidesoft.grid.EditorContext;
import com.jidesoft.swing.JideButton;
import com.uptodata.isr.utils.security.SymmetricProtection;
import com.utd.stb.back.database.Dispatcher;
import com.utd.stb.inter.application.BackendException;
import org.apache.commons.lang.ObjectUtils;
import org.apache.commons.lang.StringUtils;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import javax.swing.table.DefaultTableModel;
import java.util.*;

/**
 * @author Inna Smiljanski  created 05.05.11
 */
public abstract class ContextDefaultTableModel extends DefaultTableModel implements ContextSensitiveTableModel {
    public static final Logger log = LogManager.getRootLogger();
    String _insertAllowed;
    String _deleteAllowed;
    HashMap<Integer, String> _columns;
    Dispatcher _dispatcher;
    HashMap<String, HashMap<String, Object>> _updatedRows = new HashMap<>();
    HashMap<String, HashMap<String, Object>> _removedRows = new HashMap<>();

    HashMap<String, HashMap<String, Object>> _allColumnsMap = new HashMap<>();
    List<String> _primaryKeys = new ArrayList<String>();


    private JideButton button = null;

    public String message;

    ContextDefaultTableModel(Vector data, Vector columnNames) {
        super(data, columnNames);
        init();
    }

    private static boolean isTrimNotNull(String s) {
        return s != null && s.trim().length() > 0;
    }

    public static Locale oracleLangToLocale(String oracleLang) {
        Locale locale = Locale.ENGLISH;

        if ("GERMAN".equalsIgnoreCase(oracleLang)) locale = Locale.GERMANY;
        if ("FRENCH".equalsIgnoreCase(oracleLang)) locale = Locale.FRENCH;
        if ("ITALIAN".equalsIgnoreCase(oracleLang)) locale = Locale.ITALIAN;
        if ("SPANISH".equalsIgnoreCase(oracleLang)) locale = new Locale("es");

        return locale;
    }

    public abstract void init();

    @Override
    public Object getValueAt(int row, int column) {
        Object value = super.getValueAt(row, column);
        if (column != 0) {
            String type = (String) getColumnMap(column).get("type");
            if (type.equalsIgnoreCase("BOOLEAN")) {
                if ("T".equals(value.toString()) || Boolean.TRUE.equals(value) || value.equals(true))
                    value = Boolean.TRUE;
                else
                    value = Boolean.FALSE;
            } else if (type.equalsIgnoreCase("DATE")) {
                log.debug(value);
            }
        }
        return value;
    }

    @Override
    public Class getColumnClass(int col) {
        if (col != 0)
            return getColumnClass(getColumnName(col));
        return
                String.class;
    }

    private String getColumnType(String columnIdentifier) {
        HashMap<String, Object> colMap = getColumnMap(columnIdentifier);
        if (colMap != null) {
            Object type = colMap.get("type");
            if (type instanceof String) {
                return (String) type;
            }
        }
        return null;
    }


    public String getColumnType(int column) {
        String columnIdentifier = getColumnName(column);
        String type = getColumnType(columnIdentifier);
        return type;
    }


    public Class getColumnClass(String columnIdentifier) {
        if (columnIdentifier != null) {
            String type = getColumnType(columnIdentifier);
//            log.debug("type=" + type);
            if (type.equalsIgnoreCase("STRING"))
                return String.class;
            else if (type.equalsIgnoreCase("DATE"))
                return Date.class;
            else if (type.equalsIgnoreCase("BOOLEAN"))
                return Boolean.class;
            else if (type.equalsIgnoreCase("NUMBER"))
                return Number.class;
            else if (type.equalsIgnoreCase("INTEGER"))
                return Integer.class;
            else if (type.equalsIgnoreCase("FILE"))
                return byte[].class;
            if (type.equalsIgnoreCase("PASSWORD"))
                return char[].class;
        }
        return String.class;
    }

    @Override
    public boolean isCellEditable(int row, int column) {
        return column != 0 && "T".equals(getColumnMap(column).get("editable"));
    }

    @Override
    public void setValueAt(Object aValue, int row, int column) {
        log.debug(row + " " + column + (aValue != null));
        Object valueOld = getValueAt(row, column);
        super.setValueAt(aValue, row, column);
        if (column != 0) {
//            log.debug("setValueAt begin: " + valueOld + "; " + aValue + " " + row + " ");
            if (aValue == null || !aValue.equals(valueOld)) {
                log.debug(getValueAt(row, 1));
                setValueAt("*", row, 0);
                // rowChangedMap.put((String) getValueAt(row, 1), true);
            }

            final HashMap<String, Object> updatedColumns = new HashMap<String, Object>();

            for (int i = 0; i < _columns.size(); i++) {
                Object colValue = getValueAt(row, i + 1);
                if (colValue != null && getColumnType(i + 1).equalsIgnoreCase("PASSWORD")) {
                    try {
                        String pwd = new String((char[]) colValue);
//                        log.debug(pwd);
                        colValue = SymmetricProtection.encrypt256(pwd);
                    } catch (Exception e) {
                        log.error(e);
                    }
                }
//                  log.debug(i+" "+_columns.get(i)+" "+colValue);
                if (i == 0 && colValue.toString().startsWith("tempRowId")) colValue = "";
                updatedColumns.put(_columns.get(i), colValue);
            }
            _updatedRows.put(String.valueOf(getValueAt(row, 1)), updatedColumns);
//            log.debug("setValueAt end :_updatedRows " + _updatedRows);
        }
    }


    @Override
    public void insertRow(int row, Vector rowData) {
        log.debug("insertRow: " + row);
        super.insertRow(row, rowData);
        setValueAt("+", row, 0);

        if (StringUtils.isEmpty(ObjectUtils.toString(getValueAt(row, 1)))) {
            Random rand = new Random();
            setValueAt("tempRowId" + (rand.nextInt(100000000) + 1), row, 1);
        }
    }

    @Override
    public void addRow(Vector rowData) {
        log.debug("addRow ");
        super.addRow(rowData);
        // setValueAt("+", row, 0);
    }

    @Override
    public void removeRow(int row) {
        log.debug("removeRow: " + row + " " + this.getDataVector().size() + "; ");

        final HashMap<String, Object> deletedColumns = new HashMap<String, Object>();

        if (getValueAt(row, 1) != null || getValueAt(row, 1).toString().length() < 1) {
            for (int i = 0; i < _columns.size(); i++) {
                deletedColumns.put(_columns.get(i), getValueAt(row, i + 1));
            }
        }
        String rowid = (String) getValueAt(row, 1);
        if (StringUtils.isNotEmpty(rowid) && !rowid.startsWith("tempRowId"))
            _removedRows.put(rowid, deletedColumns);
        else
            _updatedRows.remove(rowid);
        super.removeRow(row);
        log.debug("_removedRows:" + _removedRows);
    }


    boolean validate() throws BackendException {
        for (int row = 0; row < getRowCount(); row++) {
            if (StringUtils.isNotEmpty(ObjectUtils.toString(getValueAt(row, 0)))) {

                for (int col = 1; col < getColumnCount(); col++) {
                    String columnIdentifier = getColumnName(col);
                    log.debug(row + " " + columnIdentifier + ": " + getCellClassAt(row, col));
                    boolean notNull = validateNotNull(getValueAt(row, col), columnIdentifier);
                    if (!notNull) {
                        log.debug(notNull + " " + row + " " + col + " " + getValueAt(row, col));
                        return false;
                    }
                    boolean unique = validateConstraint(getValueAt(row, col), row, columnIdentifier, col);
                    if (!unique) {
                        log.debug(unique + " " + row + " " + col + " " + getValueAt(row, col));
                        return false;
                    }
                    boolean number = validateNumber(getValueAt(row, col), columnIdentifier);
                    if (!number) {
                        log.debug(number + " " + row);
                        return false;
                    }

                    boolean format = validateFormat(getValueAt(row, col), row, columnIdentifier);
                    if (!format) {
                        log.debug(format + " " + row);
                        return false;
                    }
                }
            }
        }
        return true;
    }

    public boolean validateNotNull(Object value, String columnIdentifier) throws BackendException {
        if (isTrimNotNull(columnIdentifier)) {
            if (isCellRequired(columnIdentifier) && (value == null || value.toString().length() < 1)) {
                log.debug("not null! " + columnIdentifier);
                message = getTranslation("exceptionDialog.window.required") + " " + columnIdentifier;
                return false;
            }
        }
        return true;

    }

    public boolean validateFormat(Object value, int row, String columnIdentifier) throws BackendException {
        if (isTrimNotNull(columnIdentifier)) {
            Object format = getColumnMap(columnIdentifier).get("format");
            log.debug(format + " value=" + value);
            if (StringUtils.isNotEmpty(ObjectUtils.toString(format)) && value != null) {
                if (value.toString().length() > Integer.parseInt(format.toString())) {
                    log.debug("format! " + getValueAt(row, 1));
                    message = getTranslation("exceptionDialog.window.length") + " in column " + Integer.valueOf(format.toString()) + " " + columnIdentifier;
                    return false;
                }
            }
        }
        return true;
    }

    public boolean validateConstraint(Object newValue, int row, String columnIdentifier, int col) throws BackendException {
        // log.debug("validateConstraint: " + getValueAt(row, 1) + " " + _allColumnsMap.get(getColumnName(col)).get("primaryKey") + "; " + getPrimaryKeys());
        if (col < 0)
            col = getColumnNumber(columnIdentifier);


        if ("T".equals(_allColumnsMap.get(columnIdentifier).get("primaryKey"))) {

            HashMap<String, Object> mapToCompare = new HashMap<>();

            for (int j = 1; j < getColumnCount(); j++) {
                Object value = getValueAt(row, j);
                if (j == col)
                    value = newValue;
                if (getPrimaryKeys().contains(getColumnName(j)))
                    mapToCompare.put(getColumnName(j), value);
            }


            String rowId = (String) getValueAt(row, 1);

            for (int i = 0; i < getRowCount(); i++) {

               if (getValueAt(i, 1).equals(rowId)) {
                    log.debug(i);
                } else {
                    boolean isMapNotEqual = false;

                    for (String sKey : mapToCompare.keySet()) {
//                        log.debug(i + "; " + sKey + ";  " + getValueAt(i, getColumnNumber(sKey)) + ";  " + mapToCompare.get(sKey));
                        if (!getValueAt(i, getColumnNumber(sKey)).equals(mapToCompare.get(sKey))) {
                            isMapNotEqual = true;
                            break;
                        }
                    }

                    if (!isMapNotEqual) {
                        log.debug("unique constraint " + getValueAt(row, 1) + ";" + columnIdentifier);
                        message = getTranslation("exceptionDialog.window.unique") + " in column " + columnIdentifier;
                        return false;
                    }
                }
            }
        }
        return true;
    }


    public boolean validateNumber(Object value, String columnIdentifier) throws BackendException {
        Class columnClass = getColumnClass(columnIdentifier);
        if (columnClass.equals(Number.class) && StringUtils.isNotEmpty(ObjectUtils.toString(value)))
            try {
                Double.valueOf(value.toString());
            } catch (NumberFormatException e) {
                message = getTranslation("exceptionDialog.window.number.column") + columnIdentifier;
                return false;
            }
        return true;
    }


    public ConverterContext getConverterContextAt(int row, int column) {
        return null;
    }

    public EditorContext getEditorContextAt(int row, int column) {
        return null;
    }

    public Class getCellClassAt(int row, int column) {
        return getColumnClass(column);
    }

    public boolean isInsertAllowed() {
        return "T".equals(_insertAllowed);
    }

    public boolean isDeleteAllowed(int row) {
        return "T".equals(_deleteAllowed) || getValueAt(row, 1) == null
                || getValueAt(row, 1).toString().length() < 1;
    }

    public HashMap<String, HashMap<String, Object>> getAllColumnsMap() {
        return _allColumnsMap;
    }

    private List<String> getPrimaryKeys() {
        return _primaryKeys;
    }

    public boolean isLovEditable(int column) {
        return "W".equals(getColumnMap(column).get("lov"));
    }

    private boolean isCellRequired(String columnIdentifier) {
        return "T".equals(_allColumnsMap.get(columnIdentifier).get("required"));
    }

    public boolean isLovColumn(String columnIdentifier) {
        String lovColumn = (String) getColumnMap(columnIdentifier).get("lov");
        return StringUtils.isNotEmpty(lovColumn) && !lovColumn.equalsIgnoreCase("F");
    }


    public String getCellDefaultValue(int column) {
        if (column != 0)
            return (String) getColumnMap(column).get("defaultvalue");
        return null;
    }

    public Dispatcher getDispatcher() {
        return _dispatcher;
    }

    public String getTranslation(String msg) throws BackendException {
        return _dispatcher.getTranslation(msg);
    }

    public int getColumnNumber(String identifier) {
        int col = -1;
        for (int i = 0; i < getColumnCount(); i++)
            if (getColumnName(i).equals(identifier))
                return i;
        return col;
    }

    private HashMap<String, Object> getColumnMap(int column) {
        return getColumnMap(getColumnName(column));
    }


    public HashMap<String, Object> getColumnMap(String columnIdentifier) {
        return _allColumnsMap.get(columnIdentifier);
    }

    public JideButton getButton() {
        return button;
    }

    public void setButton(JideButton button) {
        this.button = button;
    }

}

