package com.utd.stb.gui.userinput.swing;

import com.jidesoft.combobox.AbstractComboBox;
import com.jidesoft.combobox.DateComboBox;
import com.jidesoft.combobox.TableComboBox;
import com.utd.gui.controls.BooleanComboBox;
import com.utd.gui.event.DocumentDeferredChangeAdapter;
import com.utd.stb.back.ui.impl.userinputs.data.UserInputData;
import com.utd.stb.inter.userinput.FileInput;
import com.utd.stb.inter.userinput.UserInput;
import org.apache.commons.lang.ObjectUtils;
import org.apache.commons.lang.StringUtils;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import javax.swing.*;
import javax.swing.event.CellEditorListener;
import javax.swing.event.ChangeEvent;
import javax.swing.event.ChangeListener;
import javax.swing.event.DocumentEvent;
import javax.swing.text.BadLocationException;
import javax.swing.text.Document;
import javax.swing.text.JTextComponent;
import java.awt.event.ItemEvent;
import java.awt.event.ItemListener;
import java.awt.image.BufferedImage;
import java.io.File;
import java.text.ParseException;
import java.time.ZoneId;
import java.util.Date;


/**
 * Connector between a single UserInput and a single Control. To handle a
 * Collection of UserInputs/Controls use {@link UIODataHandlerBase}
 *
 * @author PeterBuettner.de
 */
public abstract class UIODataHandler extends UIODataHandlerBase {
    public static final Logger log = LogManager.getRootLogger();

    protected UserInput userInput;

    protected JComponent component;

    protected DelegatingListener delegatingListener = new DelegatingListener();

    private final class DelegatingListener extends DocumentDeferredChangeAdapter
            implements
            ChangeListener,
            ItemListener {

        public void stateChanged(ChangeEvent e) {

            fireChanged();
        }

        public void itemStateChanged(ItemEvent e) {

            fireChanged();
        }

        public void deferredChange(DocumentEvent e) {

            fireChanged();
        }
    }

    public UIODataHandler(JComponent component, UserInput userInput) {

        this.userInput = userInput;
        this.component = component;

    }

    /**
     * Get the {@link JComponent}associated with this Handler/UserInput
     *
     * @return
     */
    protected JComponent getComponent() {

        return component;
    }

    /**
     * Get the {@link UserInput}associated with this Handler/Component
     *
     * @return
     */
    protected UserInput getUserInput() {

        return userInput;
    }

    // -------------------------------------------------------------------------
    static class CheckBoxDataHandler extends UIODataHandler {

        public CheckBoxDataHandler(JCheckBox cb, UserInput userInput) {

            super(cb, userInput);
            cb.addChangeListener(delegatingListener);
        }

        public boolean isValid() {

            return true;
        }

        public boolean isEmpty() {

            return false;
        }

        public void commitData() {

            userInput.setData(((JCheckBox) component).isSelected());
        }
    }// -------------------------------------------------------------------------

    static class BooleanComboDataHandler extends UIODataHandler {

        public BooleanComboDataHandler(BooleanComboBox cb, UserInput userInput) {

            super(cb, userInput);
            cb.addItemListener(delegatingListener);
        }

        public boolean isValid() {

            return true;
        }

        public boolean isEmpty() {

            return ((BooleanComboBox) component).getSelectedBoolean() == null;
        }

        public void commitData() {

            userInput.setData(((BooleanComboBox) component).getSelectedBoolean());
        }
    }// -------------------------------------------------------------------------

    static class DisplayableItemInputDataHandler extends UIODataHandler {

        private CellEditor edi;

        public DisplayableItemInputDataHandler(JTextField textField, CellEditor edi,
                                               UserInput userInput) {

            super(textField, userInput);
            this.edi = edi;
            textField.getDocument().addDocumentListener(delegatingListener); // s.u.
            edi.addCellEditorListener(new CellEditorListener() {

                public void editingCanceled(ChangeEvent e) {

                }

                public void editingStopped(ChangeEvent e) {

                    fireChanged();
                }
            });
        }

        public boolean isValid() {

            return true;
        }

        public boolean isEmpty() {

            // check for text not empty since:
            //		PopupListen Standalone im Wizzard:
            //		was selektieren -> valid; dann l?schen -> bleibt valid; nun:
            //		click auf weiter/fertig geht zwar nicht, doch click auf die
            // navListe
            // (evtl. hiervor ein focuswechsel und ohne delegatingListener
            // auskommen)
            return edi.getCellEditorValue() == null
                    || ((JTextField) getComponent()).getText().length() == 0;
        }

        public void commitData() {
            userInput.setData(edi.getCellEditorValue());
        }

    }


    static abstract class TextDataHandler extends UIODataHandler {

        public TextDataHandler(JTextComponent tf, UserInput userInput) {

            super(tf, userInput);
            tf.getDocument().addDocumentListener(delegatingListener);
        }

        public boolean isEmpty() {

            Document doc = ((JTextComponent) component).getDocument();
            //		return doc == null || doc.getLength()==0;
            return !hasNonWhiteSpace(doc); // @@PEB-2005-05-01@@
        }

        /**
         * check if there is any char other than whitespace in the document,
         * whitespace: {@link java.lang.Character#isWhitespace(char) }, empty
         * document returns false.
         *
         * @param doc
         * @return false if doc is null, empty or contains only whitespace
         */
        protected boolean hasNonWhiteSpace(Document doc) {

            //		 @@PEB-2005-05-01@@
            if (doc == null) return false;
            int len = doc.getLength();

            // we check char by char, this is most expensive for texts starting
            // with lots of whitespace,
            // but in the most common case they start with non-whitespace
            try {
                for (int i = 0; i < len; i++) {
                    String aChar = doc.getText(i, 1);
                    if (!Character.isWhitespace(aChar.charAt(0))) return true;
                }
                // may see javax.swing.text.Segment later, may improve
                // performance since the Segment is not created for every char
            } catch (BadLocationException e) {
                throw new RuntimeException(
                        "Illegal data state: the text in the JTextComponents document is shorter than it was as we checked immediately before.",
                        e);
            }

            return false;
        }

    }

    static class FormattedTextDataHandler extends TextDataHandler {

        public FormattedTextDataHandler(JFormattedTextField tf, UserInput userInput) {

            super(tf, userInput);

        }

        public boolean isValid() {

            return isEmpty() || ((JFormattedTextField) component).isEditValid();

        }

        public void commitData() {

            try {

                ((JFormattedTextField) component).commitEdit();
                userInput.setData(((JFormattedTextField) component).getValue());

            } catch (ParseException pe) {

                userInput.setData(null);
            }

        }

    }

    static class ComboBoxDataHandler extends UIODataHandler {

        public ComboBoxDataHandler(AbstractComboBox tf, UserInput userInput) {

            super(tf, userInput);
            tf.addItemListener(delegatingListener);
        }

        public boolean isValid() {
            return true;
        }

        @Override
        public boolean isEmpty() {
            Object o = ((AbstractComboBox) component).getSelectedItem();
            return !StringUtils.isNotEmpty(ObjectUtils.toString(o));
        }

        public void commitData() {
            //  ((AbstractComboBox) component).commitEdit();
            if (component instanceof DateComboBox) {
                Date date = ((DateComboBox) component).getDate();
                if (date != null) {
                    java.time.Instant inst = date.toInstant();
                    userInput.setData(inst.atZone(ZoneId.systemDefault()));
                }
            }
            if (component instanceof TableComboBox) userInput.setData(((TableComboBox) component).getSelectedItem());

        }

    }

    static class PlainTextDataHandler extends TextDataHandler {

        public PlainTextDataHandler(JTextComponent tc, UserInput userInput) {

            super(tc, userInput);
        }

        public boolean isValid() {
            if (FileInput.class.isAssignableFrom(userInput.getType())) {
                String fileName = ((JTextComponent) component).getText();
                File file = new File(fileName);
                return (file.exists() && ((FileInput) (userInput.getData())).hasLov())
                        || (file.getParentFile() != null && file.getParentFile().isDirectory() && !((FileInput) (userInput.getData())).hasLov());
            }
            return true;
        }

        public void commitData() {

            userInput.setData(((JTextComponent) component).getText());
        }

    }

    protected static class ImageDataHandler extends UIODataHandler {
        BufferedImage img;

        public ImageDataHandler(JComponent component, UserInput userInput) {
            super(component, userInput);
            ImageIcon icon = (ImageIcon) ((JLabel) component).getIcon();
            if (icon != null)
                img = (BufferedImage) icon.getImage();
        }

        public void setImg(BufferedImage img) {
            this.img = img;
        }

        @Override
        public boolean isValid() {
            return true;  //To change body of implemented methods use File | Settings | File Templates.
        }

        @Override
        public boolean isEmpty() {
            return img != null;
        }

        @Override
        public void commitData() {
            log.debug(img);
            userInput.setData(img);
        }
    }

}