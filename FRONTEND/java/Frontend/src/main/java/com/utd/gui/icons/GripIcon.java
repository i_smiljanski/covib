package com.utd.gui.icons;

import com.uptodata.isr.gui.util.Colors;

import javax.swing.*;
import java.awt.*;


/**
 * Icon like the grip in Windows windows in the bottom-right corner to size a
 * window.
 *
 * @author PeterBuettner.de
 */
public class GripIcon implements Icon {

    private int size;

    public GripIcon(int size) {

        this.size = size;
    }

    public int getIconHeight() {

        return size;
    }

    public int getIconWidth() {

        return size;
    }

    private void lines(Graphics g, int i) {

        g.setColor(Colors.getUIControlShadow());
        int x = size - i - 1;
        int y = size - 1;
        g.drawLine(x, y, x + i, y - i);
        i++;
        x--;
        g.drawLine(x, y, x + i, y - i);
        i++;
        x--;
        g.setColor(Colors.getUIControlLtHighlight());
        g.drawLine(x, y, x + i, y - i);

    }

    public void paintIcon(Component c, Graphics g, int x, int y) {

        Color old = g.getColor();
        g.translate(x, y);
        for (int i = 1; i <= 12 && i < size; i += 4)
            lines(g, i);
        g.translate(-x, -y);
        g.setColor(old);

    }

}