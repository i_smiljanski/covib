package com.utd.gui.controls.table.model;

import javax.swing.table.AbstractTableModel;
import java.util.*;


/**
 * A TableModel based on a List, fires necessary events as needed
 *
 * @author PeterBuettner.de
 */
public abstract class AbstractListTableModel extends AbstractTableModel implements
        RowTableModel {

    /**
     * the container is final to vave it write protected, to get a custom list
     * overwrite {@link createList()}
     */
    protected final List data;

    /**
     * empty data
     */
    public AbstractListTableModel() {

        data = createList();

    }

    /**
     * fill with the data of the collection, items are not copied
     *
     * @param collection
     */
    public AbstractListTableModel(Collection collection) {

        this();
        data.addAll(collection);

    }

    /**
     * overwrite this to have a custom data container, this will shurely be
     * called exactly once, but in the constructor, so be aware that <b>your
     * constructor is 'not called' at this time, and your fields are not
     * initialized! </b>
     *
     * @return
     */
    protected List createList() {

        return new ArrayList();

    }

    public int getRowCount() {

        return data.size();

    }

    /**
     * get the row
     *
     * @param row
     * @return
     */
    public Object getRow(int row) {

        return data.get(row);

    }

    /**
     * maps row indices to Objects
     *
     * @param rows
     * @return
     */
    public List getRows(int[] rows) {

        List result = new ArrayList(rows.length);
        for (int i = 0; i < rows.length; i++)
            result.add(getRow(rows[i]));
        return result;

    }

    /**
     * get all Objects, creates a copy
     *
     * @return
     */
    public List getRows() {

        return new ArrayList(data);

    }

    /**
     * fires change event
     *
     * @param rowData
     */
    public void addRow(Object rowData) {

        addRows(Collections.singletonList(rowData));

    }

    /**
     * fires change event
     *
     * @param rowData
     */
    public void addRow(int index, Object rowData) {

        addRows(index, Collections.singletonList(rowData));

    }

    /**
     * fires change event
     *
     * @param rows
     */
    public void addRows(List rows) {

        addRows(data.size(), rows);

    }

    /**
     * if you overwrite: all addRowXXX go through this method fires change event
     *
     * @param rows
     */
    public void addRows(int index, List rows) {

        if (rows.isEmpty()) return;
        data.addAll(index, rows);
        fireTableRowsInserted(index, index + rows.size());

    }

    /**
     * fires change event
     *
     * @param row
     */
    public void removeRow(int row) {

        data.remove(row);
        fireTableRowsDeleted(row, row);

    }

    /**
     * fires change event
     *
     * @param row
     * @throws IllegalArgumentException if row not found
     */
    public void removeRow(Object row) throws IllegalArgumentException {

        removeRows(Collections.singletonList(row));

    }

    /**
     * Removes all rows with the indices in rows <br>. The indices have to be
     * sorted ascending, or you get an exception, you should trigger sorting by
     * using needsSort if you have unsorted data <br>
     * fires change event for each row!
     *
     * @param rows
     * @param needsSort the array is cloned, so your original array is untouched
     */
    public void removeRows(int[] rows, boolean needsSort) {

        if (needsSort) {

            rows = rows.clone();
            Arrays.sort(rows);

        }

        for (int i = rows.length - 1; i >= 0; i--) {
            removeRow(rows[i]);
        }

    }

    /**
     * Removes all rows in rows <br>. You get an IllegalArgumentException if an
     * item in the List isn't in the data. throws IllegalArgumentException if
     * one Object isn't found
     *
     * @param rows
     * @throws IllegalArgumentException if row not found
     */
    public void removeRows(List rows) throws IllegalArgumentException {

        int[] iRows = findRows(rows);
        Arrays.sort(iRows); // sort here to save cloning
        removeRows(iRows, false);

    }

    /**
     * Removes all rows <br>.
     */
    public void removeAllRows() {

        int size = data.size();
        if (size == 0) return;
        data.clear();
        fireTableRowsDeleted(0, size - 1);

    }

    /**
     * searches the dataRows for the indices of the objects in the List find
     * <br>
     * <b>Note: </b> that the indices are not sorted, they are in the order to
     * match the searched rows <br>
     * throws IllegalArgumentException if one Object isn't found
     *
     * @param find
     * @return never null
     */
    public int[] findRows(List find) throws IllegalArgumentException {

        return findRows(find, true);

    }

    /**
     * searches the dataRows for the indices of the objects in the List find,
     * for not found rows: <br>
     * if check throws IllegalArgumentException if one Object isn't found, <br>
     * if !check then the associated is set to -1 and no exception thrown <br>
     * <b>Note: </b> that the indices are not sorted, they are in the order to
     * match the searched rows
     *
     * @param find
     * @param check
     * @return never null
     */
    public int[] findRows(List find, boolean check) {

        int len = find.size();
        int[] res = new int[len];

        for (int i = 0; i < len; i++) {

            res[i] = data.indexOf(find.get(i));

            if (check && res[i] == -1)

                throw new IllegalArgumentException(
                        "Could not find the searched object in the TableData");

        }

        return res;

    }

    /**
     * searches the dataRows for the index of the object, returns -1 if not
     * found
     *
     * @param toFind
     * @return
     */
    public int findRow(Object toFind) {

        return data.indexOf(toFind);

    }

}