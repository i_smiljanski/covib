package com.utd.gui.popup;

import com.utd.gui.filter.Filter;
import com.utd.gui.filter.FilteredListListModel;

import javax.swing.*;
import javax.swing.event.ListDataEvent;
import javax.swing.event.ListDataListener;
import java.awt.*;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.util.ArrayList;
import java.util.List;


/**
 * A Celleditor that appears like a combobox: a textfield that works as a filter
 * for a popup JList. <br>
 * if only one item in list, select this per default, so an [ENTER]-key will use
 * it <br>
 * See: {@link com.utd.gui.popup.AbstractFilteringCellEditor}for important
 * details
 *
 * @author PeterBuettner.de
 */
public class JListFilteringCellEditor extends AbstractFilteringCellEditor {

    private final List data;

    /**
     * standalone defaluts to false
     *
     * @param textField
     * @param data
     */
    public JListFilteringCellEditor(JTextField textField, List data) {

        this(textField, data, false);

    }

    /**
     * @param textField
     * @param data
     * @param standalone placed in a JTable
     */
    public JListFilteringCellEditor(JTextField textField, List data, boolean standalone) {
        super(textField, standalone);
        this.data = data;

    }

    protected JComponent createList(Filter filter) {

        ListModel model = new FilteredListListModel(new ArrayList(data), filter);
        final JList list = new JList(model) {

            public Dimension getPreferredScrollableViewportSize() {

                Dimension dim = super.getPreferredScrollableViewportSize();
                dim.width = Math.max(150, Math.min(dim.width, 200));
                return dim;
            }
        };
        //
        list.setVisibleRowCount(8);

        list.addMouseListener(new MouseAdapter() {

            public void mouseClicked(MouseEvent e) {

                if (SwingUtilities.isLeftMouseButton(e)) useCurrentListSelection();
            }
        });

        list.addMouseMotionListener(new MouseHoverSelectsListener());

        // note: setModel is evil, overwrite setModel() in JList, since
        // we connect here to the model.
        model.addListDataListener(new ListDataListener() {

            // autoselect first entry, but only if filter isn't empty
            private void change() {

                SwingUtilities.invokeLater(new Runnable() {

                    public void run() {

                        if (isFilterEmpty()) return;
                        if (list.getModel().getSize() > 0) list.setSelectedIndex(0);
                    }
                });
            }

            public void contentsChanged(ListDataEvent e) {

            }

            public void intervalAdded(ListDataEvent e) {

                change();
            }

            public void intervalRemoved(ListDataEvent e) {

                change();
            }
        });

        return list;

    }

    protected Object getCurrentListSelection() {

        JList list = (JList) displayList;
        int selIdx = list.getSelectedIndex();
        if (selIdx == -1) return null;
        return list.getSelectedValue();
    }

    protected boolean isPopupList() {
        return true;
    }

}