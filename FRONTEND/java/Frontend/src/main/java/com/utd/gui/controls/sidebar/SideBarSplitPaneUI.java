package com.utd.gui.controls.sidebar;

import com.sun.java.swing.plaf.windows.WindowsSplitPaneUI;

import javax.swing.*;
import javax.swing.plaf.ComponentUI;
import javax.swing.plaf.basic.BasicSplitPaneDivider;
import java.awt.event.ActionEvent;


public class SideBarSplitPaneUI extends WindowsSplitPaneUI {

    public static final String TOGGLE_OPEN_CLOSE_ACTION = "toggleOpenCloseAction";

    public void installUI(JComponent c) {

        super.installUI(c);

        Action acToggle = new AbstractAction() {

            public void actionPerformed(ActionEvent e) {

                toggleDividerOpenClose();
            }

        };
        c.getActionMap().put(TOGGLE_OPEN_CLOSE_ACTION, acToggle);
        c.getInputMap(JComponent.WHEN_ANCESTOR_OF_FOCUSED_COMPONENT)
                .put(KeyStroke.getKeyStroke("F9"), TOGGLE_OPEN_CLOSE_ACTION);

    }

    public static ComponentUI createUI(JComponent x) {

        return new SideBarSplitPaneUI();
    }

    public BasicSplitPaneDivider createDefaultDivider() {

        return new SidebarSplitPaneDivider(this);
    }

    public void toggleDividerOpenClose() {

        ((SidebarSplitPaneDivider) getDivider()).toggleOpenClose();
    }
}