package com.utd.stb.inter.action;

/**
 * A Collection of ButtonItems
 *
 * @author PeterBuettner.de
 */
public interface ButtonItems {

    /**
     * Number of ButtonItems
     *
     * @return
     */
    int getSize();

    /**
     * get ButtonItem with index i
     *
     * @param i
     * @return
     */
    ButtonItem get(int i);

    /**
     * index in the Collection or -1 if not found
     *
     * @param menuItem
     * @return
     */
    int indexOf(ButtonItem buttonItem);

}