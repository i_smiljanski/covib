package com.utd.stb.back.data.client;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;


/**
 * Wraps a list of record data of type MenuEntryRecord
 *
 * @author rainer bruns Created 27.08.2004
 * @see MenuEntryRecord
 */
public class MenuEntryList {

    private ArrayList data = new ArrayList();
    private Iterator iterator;

    /**
     * Creates an empty MenuEntryList
     */
    public MenuEntryList() {

    }

    /**
     * Creates an MenuEntryList with this data
     *
     * @param in the list data
     */
    public MenuEntryList(ArrayList in) {

        data = in;
    }

    /**
     * Creates an MenuEntryList with this data
     *
     * @param in the list data
     */
    public MenuEntryList(List list) {

        data.addAll(list);
    }

    /**
     * adds all fields of input list to this list
     *
     * @param in an MenuEntryList to add
     */
    public void addAll(MenuEntryList in) {

        in.initIterator();
        while (in.hasNext()) {
            data.add(in.getNext());
        }
    }

    /**
     * adds this record to the list
     *
     * @param sr the MenuEntryRecord to add
     */
    public void add(MenuEntryRecord sr) {

        data.add(sr);
    }

    /**
     * @param index
     * @return the record at this index
     */
    public MenuEntryRecord get(int index) {

        return (MenuEntryRecord) data.get(index);
    }

    /**
     * Sets the field at index to this MenuEntryRecord
     *
     * @param index
     * @param sr
     */
    public void set(int index, MenuEntryRecord sr) {

        data.set(index, sr);
    }

    /**
     * sets list iterator to the beginning
     */
    public void initIterator() {

        iterator = data.iterator();
    }

    /**
     * @return true if there is a next record
     */
    public boolean hasNext() {

        return iterator.hasNext();
    }

    /**
     * @return the next MenuEntryRecord
     */
    public MenuEntryRecord getNext() {

        return (MenuEntryRecord) iterator.next();
    }

    /**
     * @return the size of this list
     */
    public int getSize() {

        return data.size();
    }

    /**
     * @param s Record to look up
     * @return the index of this record
     */
    public int indexOf(MenuEntryRecord s) {

        return data.indexOf(s);
    }

}