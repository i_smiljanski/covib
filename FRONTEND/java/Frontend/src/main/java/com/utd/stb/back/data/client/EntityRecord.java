package com.utd.stb.back.data.client;

/**
 * Wraps database struct data. Has appropriate get/set methods for data from db
 * struct STB$ENTITY$RECORD
 *
 * @author rainer bruns Created 27.08.2004
 */
public class EntityRecord {

    StructMap selectionData;

    /**
     * Creates an empty EntityRecord
     */
    public EntityRecord() {

    }

    /**
     * @param s the StructMap with the data for this Record
     */
    public EntityRecord(StructMap s) {

        selectionData = s;
    }

    /*
     * (non-Javadoc)
     * 
     * @see java.lang.Object#toString() returns the value of field sEnt
     */
    public String toString() {

        return selectionData.getValueAsString("SENT");
    }

    /**
     * @return the value of field sEnt, this is the entity
     */
    public String getEntity() {

        return selectionData.getValueAsString("SENT");
    }

    /**
     * in StructMap is "T", else false.
     *
     * @return true if this entity is selected. This is if underlieing field
     * SPRESELECTED
     * @see StructMap#getValueAsBoolean(String)
     */
    public boolean isSelected() {

        return selectionData.getValueAsBoolean("SPRESELECTED");
    }

    /**
     * @return the underlying data
     */
    protected StructMap getStruct() {

        return selectionData;
    }
}

