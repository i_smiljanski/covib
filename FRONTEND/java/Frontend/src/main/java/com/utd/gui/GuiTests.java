package com.utd.gui;

import com.jidesoft.combobox.TableComboBox;
import com.jidesoft.grid.CellRendererManager;
import com.jidesoft.grid.SortableTable;
import com.jidesoft.grid.TableComboBoxCellEditor;
import com.jidesoft.grid.TableModelWrapperUtils;
import com.utd.gui.controls.table.BaseTable;
import com.utd.gui.controls.table.ListViewTools;
import com.uptodata.isr.gui.util.Colors;
import com.utd.stb.back.data.client.AttributeList;
import com.utd.stb.back.data.client.SelectionRecord;
import com.utd.stb.inter.application.BackendException;
import com.utd.stb.inter.items.DisplayableItem;
import com.utd.stb.inter.items.DisplayableItemComboList;
import com.utd.stb.inter.wizzard.formtypes.FormDataGridOptions;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import resources.IconSource;

import javax.swing.*;
import javax.swing.event.TableModelListener;
import javax.swing.table.*;
import java.awt.*;

/**
 * @author Inna Smiljanski  created 07.10.13
 */
public class GuiTests {

    public static final Logger log = LogManager.getRootLogger();

    public static void main(String[] args) {
        com.jidesoft.utils.Lm.verifyLicense("up to data professional services", "iStudyReporter", "w6.tuJhnfnDInGp:fvqIJaaeKT4yKF42");

        Object[][] tableData = new Object[][]{
                new Object[]{"Maryland1", "Annapolis1"},
                new Object[]{"New Hampshire", "Concord"},
                new Object[]{"New Jersey", "Trenton"},
                new Object[]{"New Mexico", "Santa Fe"},
                new Object[]{"North Dakota", "Bismark"},
        };
        String[] columns = new String[]{"Item", "Value"};
        final TableModel model = new DefaultTableModel(tableData, columns);

        final Object[][] tableData1 = new Object[][]{
                new Object[]{"Maryland2", "Annapolis2"},
                new Object[]{"Item2", "Value2"},
                new Object[]{"Item3", "Value3"},
                new Object[]{"Item4", "Value4"},
                new Object[]{"Item5", "Value5"},
        };
        String[] columns1 = new String[]{"Item", "Value"};
        final TableModel model1 = new AbstractTableModel() {

            @Override
            public int getRowCount() {
                return 5;
            }

            @Override
            public int getColumnCount() {
                return 2;
            }


            @Override
            public TableModelListener[] getTableModelListeners() {
                log.debug("getTableModelListeners");
                return super.getTableModelListeners();
            }

            @Override
            public Object getValueAt(int row, int col) {
                Object[] item = tableData1[row];
                return item[col];
            }
        };

        final TableComboBoxCellEditor tableCellEditor = new TableComboBoxCellEditor(model1) {
          /*  @Override
            public void itemStateChanged(ItemEvent itemEvent) {
                log.debug("itemStateChanged "+itemEvent);
                super.itemStateChanged(itemEvent);
            }*/

            @Override
            public Component getTableCellEditorComponent(JTable table, Object value, boolean isSelected, int row, int column) {
                Component component = super.getTableCellEditorComponent(table, value, isSelected, row, column);

                if (component instanceof TableComboBox) {
                    TableComboBox tableComboBox = (TableComboBox) component;
                    log.debug(table.getModel().getValueAt(row, column));
                    tableComboBox.setEditable(false);
                    tableComboBox.setMaximumRowCount(12);
                    tableComboBox.setValueColumnIndex(0);
                    tableComboBox.setSelectedItem(table.getModel().getValueAt(row, column));

//                    new TableComboBoxSearchable(tableComboBox);

                    return tableComboBox;
                }
                return component;
            }
        };

        final JTable tab = new BaseTable(model) {
            @Override
            public TableCellEditor getCellEditor(int row, int column) {
                return tableCellEditor;
            }

            @Override
            public boolean isCellEditable(int row, int column) {
                return true;
            }
        };


        ((SortableTable) tab).setSortable(false);

        tab.setSurrendersFocusOnKeystroke(true);// needed for popupeditor
        tab.setSelectionMode(ListSelectionModel.SINGLE_SELECTION);
        tab.setColumnSelectionAllowed(true);
        tab.setRowSelectionAllowed(true);
        //		table.setShowGrid(true); // we are no _list_view, more like a table
        tab.setAutoResizeMode(JTable.AUTO_RESIZE_OFF);
        tab.getTableHeader().setReorderingAllowed(false);// right-columns are
        // reset on leftmost
        // change
        //		table.getColumnModel().setColumnMargin(0);// looks better? grid is
        // now aligned, but cells paint over gridlines

//        tab.setDefaultRenderer(String.class, new RowHeaderRenderer()); // labels
//        CellRendererManager.registerRenderer(String.class, new RowHeaderRenderer());

        tab.setDefaultRenderer(Object.class, new ItemCellRenderer());
        CellRendererManager.registerRenderer(Object.class, new ItemCellRenderer());

        /*TableComboBox tableComboBox = new TableComboBox(model) {
            @Override
            protected JTable createTable(TableModel model) {
                return new SortableTable(model);
            }

        };
       // tableComboBox.setEditable(false);
        tableComboBox.setSelectedItem("Trenton");
        tableComboBox.setMaximumRowCount(12);
        tableComboBox.setValueColumnIndex(1);

        tableComboBox.setPreferredSize(new Dimension(200, 20));

        new TableComboBoxSearchable(tableComboBox);
                                              */


        JPanel p = new JPanel(new BorderLayout());
        p.add(tab, BorderLayout.BEFORE_FIRST_LINE);
        //  p.add(new JScrollPane(sortableTable));

        JFrame frame = new JFrame("DetailedComboBox Demo");
        frame.setPreferredSize(new Dimension(200, 100));
        frame.getContentPane().add(p, BorderLayout.CENTER);
        frame.pack();
        frame.setLocationRelativeTo(null);
        frame.setDefaultCloseOperation(WindowConstants.EXIT_ON_CLOSE);
        frame.setVisible(true);

    }

    private static class RowHeaderRenderer extends DefaultTableCellRenderer {

        public Component getTableCellRendererComponent(JTable table, Object value,
                                                       boolean isSelected, boolean hasFocus,
                                                       int row, int column) {

            Component lbl = super.getTableCellRendererComponent(table, value, isSelected,
                    false, row, column);
            ListViewTools.changeDefaultTableCellRenderer(lbl, hasFocus);
            lbl.setBackground(Colors.getItemHiliteInactiveBack());
            lbl.setForeground(Colors.getItemHiliteInactiveText());
            setHorizontalAlignment(JLabel.RIGHT);
            setToolTipText(value == null ? "" : value.toString());
            return lbl;
        }
    }

    private static class ItemCellRenderer extends DefaultTableCellRenderer {

        private IconSource is;

        public ItemCellRenderer() {

            this.is = new IconSource();
        }

        public Component getTableCellRendererComponent(JTable table,
                                                       Object value,
                                                       boolean isSelected,
                                                       boolean hasFocus,
                                                       int row,
                                                       int column) {

            // test if invalid empty
            TableModel tableModel = TableModelWrapperUtils.getActualTableModel(table.getModel());
            boolean valid = false;
            if (tableModel instanceof FormDataGridOptionsTableModel)
                valid = ((FormDataGridOptionsTableModel) tableModel).isValid(row, table.convertColumnIndexToModel(column));

            // MCD, 17.Jan 2006, Attributliste auswerten
            if (value instanceof DisplayableItem) {

                if (((DisplayableItem) value).getAttributeList() != null) {

                    AttributeList attrList = (AttributeList) ((DisplayableItem) value).getAttributeList();

                    // COLOR
                    Color color = valid ? attrList.getColor() : Colors.getDataBackIllegalEmpty();
                    if (!isSelected && !hasFocus) this.setBackground(color);

                    // HIGHLIGHT
                    Font font = attrList.getHighlighted();
                    if (font != null) this.setFont(font);

                    // ICON
                    String icon = attrList.getIcon();
                    if (column == 0) this.setIcon(is.getSmallIconByID(icon));
                    if (column != 0) this.setIcon(null);
                }

                // VALUE
                value = ((DisplayableItem) value).getData();
            }

            JLabel lbl = (JLabel) super.getTableCellRendererComponent(table,
                    value,
                    isSelected,
                    false,
                    row,
                    column);
            ListViewTools.changeDefaultTableCellRenderer(lbl, hasFocus);
            return lbl;
        }
    }

    private static class FormDataGridOptionsTableModel extends AbstractTableModel {

        FormDataGridOptions data;

        public FormDataGridOptionsTableModel(FormDataGridOptions data) {

            this.data = data;
        }

        public int getColumnCount() {

            return data.getGridColumnCount();
        }

        public int getRowCount() {

            return data.getGridRowCount();
        }

        public String getColumnName(int col) {

            try {

                return data.getColumnTitle(col);

            } catch (BackendException be) {

                return be.getLocalizedMessage();

            }
        }

        public Class getColumnClass(int col) {

            return DisplayableItem.class;
        }

        public boolean isValid(int row, int col) {

            DisplayableItemComboList cl = data.getValue(row, col);
            return cl == null ? true : cl.isEmptySelectionAllowed()
                    || (cl.getSelectedItem() != null);
        }

        public Object getValueAt(int row, int col) {

            DisplayableItemComboList cl = data.getValue(row, col);
            return cl == null ? null : cl.getSelectedItem();
        }

        public void setValueAt(Object value, int row, int col) {

            if (value != null && !(value instanceof DisplayableItem ||
                    value instanceof String /*MCD 26.Aug 2008, Grid editable*/))
                throw new IllegalArgumentException("GridChooser: Error, unexpected class: " + value.getClass().getName());

            DisplayableItemComboList cbl = data.getValue(row, col);
            if (cbl == null)
                throw new IllegalArgumentException("GridChooser, Error, DisplayableItemComboList:" + row + "/" + col + " is null");

            DisplayableItem oldValue = cbl.getSelectedItem();
            /*
             * we only set value on real change, since the editor/table/whoever
             * maybe lazy and send a value-changed-message with the 'old' value
             */
            if (oldValue == null && value == null) return; // both null -> same
            if (value != null && value.equals(oldValue)) return;

            // MCD 26.Aug 2008, Grid editable
            boolean editable = false;
            if (oldValue != null) {
                AttributeList attrList = (AttributeList) oldValue.getAttributeList();
                if (attrList != null) editable = attrList.isEditable();
            }
            if (value instanceof String && editable) {
                SelectionRecord selection = (SelectionRecord) oldValue;
                selection.setValue(value.toString());
                selection.setDisplay(value.toString());
                cbl.setSelectedItem(selection);
            } else if (value instanceof DisplayableItem) {
                cbl.setSelectedItem((DisplayableItem) value);
            }

            // we known: row may have changed in the whole:
            fireTableRowsUpdated(row, row);
        }
    }

}
