package com.utd.stb.inter.userinput;

import com.utd.stb.inter.application.BackendException;
import com.utd.stb.inter.items.DisplayableItem;
import com.utd.stb.inter.items.DisplayableItems;


/**
 * DisplayableItem out of a list of availables, list is probably lazy loaded, so
 * call only if needed
 *
 * @author PeterBuettner.de
 */
public interface DisplayableItemInput {

    /**
     * maybe filled on startup with the default/lastUsed/predefined,... item
     *
     * @return
     */
    DisplayableItem getSelectedItem();

    /**
     * Use only items got from {@link getAvailableItems()}. (maybe changed if
     * DisplayableItem always implements a usable equals() so that also
     * 'synthesized' items are possible)
     *
     * @param item
     */
    void setSelectedItem(DisplayableItem item);

    /**
     * The list of available items, maybe lazy loaded, so call only if you need
     * them.
     *
     * @return
     */
    DisplayableItems getAvailableItems() throws BackendException;

}