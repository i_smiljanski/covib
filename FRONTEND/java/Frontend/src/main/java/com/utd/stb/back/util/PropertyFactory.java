/*
 * Created on 15.11.2004
 *
 */
package com.utd.stb.back.util;

import com.utd.stb.back.data.client.DatabaseFieldMap;
import com.utd.stb.back.data.client.PropertyList;
import com.utd.stb.back.data.client.PropertyRecord;
import com.utd.stb.back.data.client.StructMap;

import java.util.Enumeration;
import java.util.Hashtable;


/**
 * @author rainer
 */
public final class PropertyFactory {

    public static PropertyList createProperties(Hashtable input, String schemaOwner) {

        Enumeration e = input.keys();
        PropertyList props = new PropertyList();
        while (e.hasMoreElements()) {
            String prompt = e.nextElement().toString();
            String display = input.get(prompt).toString();
            StructMap pMap = new StructMap(schemaOwner + ".STB$PROPERTY$RECORD");
            DatabaseFieldMap dummy = new DatabaseFieldMap("SPROMPTDISPLAY",
                    "java.lang.String", 12, 255,
                    "VARCHAR", prompt);
            pMap.put("SPROMPTDISPLAY", dummy);
            dummy = new DatabaseFieldMap("SEDITABLE", "java.lang.String", 12, 255, "VARCHAR",
                    "F");
            pMap.put("SEDITABLE", dummy);
            dummy = new DatabaseFieldMap("SREQUIRED", "java.lang.String", 12, 255, "VARCHAR",
                    "T");
            pMap.put("SREQUIRED", dummy);
            dummy = new DatabaseFieldMap("SDISPLAYED", "java.lang.String", 12, 255,
                    "VARCHAR", "T");
            pMap.put("SDISPLAYED", dummy);
            dummy = new DatabaseFieldMap("HASLOV", "java.lang.String", 12, 255, "VARCHAR",
                    "F");
            pMap.put("HASLOV", dummy);
            dummy = new DatabaseFieldMap("TYPE", "java.lang.String", 12, 255, "VARCHAR",
                    "STRING");
            pMap.put("TYPE", dummy);
            dummy = new DatabaseFieldMap("SDISPLAY", "java.lang.String", 12, 255, "VARCHAR",
                    display);
            pMap.put("SDISPLAY", dummy);
            dummy = new DatabaseFieldMap("SVALUE", "java.lang.String", 12, 255, "VARCHAR",
                    display);
            pMap.put("SVALUE", dummy);
            PropertyRecord prop = new PropertyRecord(pMap);
            props.add(prop);
        }
        return props;
    }
}