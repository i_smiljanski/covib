package com.utd.stb.back.ui.impl.wizard;

import com.utd.stb.back.data.client.ErrorRecord;
import com.utd.stb.back.database.BackendErrorException;


/**
 * Extension of BackendErrorException. Used to get validation errors in wizard
 *
 * @author marie-christin dort Created 27.Jan 2006
 * @see BackendErrorException, BackendException
 */
public class StandardWizardErrorException extends BackendErrorException {

    private String errorCode;
    private String errorMessage;
    private int severity;

    public static final int SEVERITY_WIZARD_INFORMATIONAL = 0;
    public static final int SEVERITY_WIZARD_ACTION_EXIT = 1;

    /**
     * @param err the original ErrorRecord
     */
    public StandardWizardErrorException(ErrorRecord err) {

        super(err);
        this.errorCode = err.getErrorCode();
        this.errorMessage = err.getErrorMsg();
        this.severity = err.getSeverityForGui();
    }

    /**
     * @return the ErrorCode
     */
    public String getErrorCode() {

        return errorCode;
    }

    /**
     * @return the ErrorMessage
     */
    public String getErrorMessage() {

        return errorMessage;
    }
}