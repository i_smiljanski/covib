package com.utd.stb.back.data.client;

/**
 * Wraps database struct data. Has appropriate get/set methods for data from db
 * struct ISR$FORMINFO$RECORD
 *
 * @author rainer bruns Created 27.08.2004
 */
public class FormInfoRecord {

    StructMap dataMap;

    /**
     * @param s the StructMap with the data for this Record
     */
    public FormInfoRecord(StructMap in) {

        dataMap = in;
    }

    /**
     * Title is the value of field SFORMTITLE
     *
     * @return the title of this Form Object
     * @see StructMap#getValueAsString(String)
     */
    public String getTitle() {

        return dataMap.getValueAsString("STITLE");
    }

    /**
     * Entity is the value of field SENTITY
     *
     * @return the entity of this Form Object
     * @see StructMap#getValueAsString(String)
     */
    public String getEntity() {

        return dataMap.getValueAsString("SENTITY");
    }

    /**
     * Entity is the value of field SENTITY
     *
     * @return the entity of this Form Object
     * @see StructMap#getValueAsString(String)
     */
    public String getEntityForHelp() {

        String entity = dataMap.getValueAsString("SENTITY");
        return entity == null || entity.length() == 0 ? entity : entity.substring(3);
    }

    /**
     * Prompt1 is the value of field SPROMPTTEXT1
     *
     * @return the first prompt of this Form Object
     * @see StructMap#getValueAsString(String)
     */
    public String getPrompt1() {

        return dataMap.getValueAsString("SHEADLINE1");
    }

    /**
     * Prompt2 is the value of field SPROMPTTEXT2
     *
     * @return the second prompt of this Form Object
     * @see StructMap#getValueAsString(String)
     */
    public String getPrompt2() {

        return dataMap.getValueAsString("SHEADLINE2");
    }

    /**
     * Type is the value of field NFORMTYPE
     *
     * @return the formtype of this Form Object
     * @see StructMap#getValueAsInt(String)
     * @see DispatcherBase#SINGLESELECT
     * @see DispatcherBase#PICKLIST
     * @see DispatcherBase#GRID
     * @see DispatcherBase#MULTISELECT
     * @see DispatcherBase#OPTIONS
     */
    public int getType() {

        return dataMap.getValueAsInt("NMASKTYPE");
    }

    /**
     * Formpackage is the value of field SFORMPACKAGE
     *
     * @return the database package of this Form Object
     * @see StructMap#getValueAsString(String)
     */
    public String getFormPackage() {

        return dataMap.getValueAsString("SISRPACKAGE");
    }

    /**
     * Form number is the value of field NFORMNUMBER
     *
     * @return the index of this Form Object
     * @see StructMap#getValueAsInt(String)
     */
    public int getFormNumber() {

        return dataMap.getValueAsInt("NMASKNO");
    }

    /**
     * Total forms is the value of field NTOTALFORMSNUM
     *
     * @return the total number of forms
     * @see StructMap#getValueAsInt(String)
     */
    public int getTotalForms() {

        return dataMap.getValueAsInt("NTOTALFORMSNUM");
    }

    /**
     * the form description
     *
     * @return
     */
    public String getFormDescription() {

        return dataMap.getValueAsString("SDESCRIPTION");
    }

    /**
     * the form description
     *
     * @return
     */
    public String getFormToolTip() {

        return dataMap.getValueAsString("STOOLTIP");
    }

    @Override
    public String toString() {
        return "FormInfoRecord{" +
                "dataMap=" + dataMap +
                ", getFormNumber=" + getFormNumber() +
                ", getType=" + getType() +
                '}';
    }
}

