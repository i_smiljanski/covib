package com.utd.gui.filter;

import javax.swing.event.ChangeEvent;
import javax.swing.event.ChangeListener;


/**
 * A Filter based on another. Chain them.
 *
 * @author PeterBuettner.de
 */
public abstract class ChainingFilter extends AbstractFilter {

    private Filter baseFilter;

    public ChainingFilter(Filter baseFilter) {

        this.baseFilter = baseFilter;
        baseFilter.addChangeListener(new ChangeListener() {

            public void stateChanged(ChangeEvent e) {

                fireFilterChanged();
            }
        });
    }

    public boolean matches(Object candidate) {

        return baseFilter.matches(candidate);
    }

}