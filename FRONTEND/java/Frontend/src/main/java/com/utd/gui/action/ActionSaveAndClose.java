package com.utd.gui.action;

import com.utd.gui.resources.I18N;

import javax.swing.*;


public abstract class ActionSaveAndClose extends AbstractAction {

    public ActionSaveAndClose() {
        ActionUtil.initActionFromMap(this, I18N.getActionMap(ActionSaveAndClose.class.getName()),
                null);
        putValue(SHORT_DESCRIPTION, null);
    }

}