/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.utd.stb.gui.ribbon;

import com.uptodata.isr.utils.ConvertUtils;
import com.uptodata.isr.utils.xml.dom.XmlHandler;
import com.uptodata.isr.gui.lookAndFeel.TweakUI;
import com.utd.stb.gui.swingApp.App;
import org.apache.commons.codec.binary.Base64;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.pushingpixels.flamingo.api.common.CommandButtonDisplayState;
import org.pushingpixels.flamingo.api.common.JCommandButton;
import org.pushingpixels.flamingo.api.common.JCommandButton.CommandButtonKind;
import org.pushingpixels.flamingo.api.common.RichTooltip;
import org.pushingpixels.flamingo.api.common.icon.ImageWrapperResizableIcon;
import org.pushingpixels.flamingo.api.common.icon.ResizableIcon;
import org.pushingpixels.flamingo.api.common.popup.JCommandPopupMenu;
import org.pushingpixels.flamingo.api.common.popup.JPopupPanel;
import org.pushingpixels.flamingo.api.common.popup.PopupPanelCallback;
import org.pushingpixels.flamingo.api.ribbon.*;
import org.pushingpixels.flamingo.api.ribbon.resize.CoreRibbonResizePolicies;
import org.pushingpixels.flamingo.api.ribbon.resize.IconRibbonBandResizePolicy;
import org.pushingpixels.flamingo.api.ribbon.resize.RibbonBandResizePolicy;
import org.pushingpixels.flamingo.internal.ui.ribbon.AbstractBandControlPanel;
import org.pushingpixels.flamingo.internal.ui.ribbon.appmenu.JRibbonApplicationMenuButton;
import org.pushingpixels.flamingo.internal.utils.FlamingoUtilities;
import org.pushingpixels.substance.api.SubstanceLookAndFeel;
import org.pushingpixels.substance.internal.utils.filters.GrayscaleFilter;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;

import javax.swing.*;
import javax.xml.xpath.XPathExpressionException;
import java.awt.*;
import java.awt.color.ColorSpace;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.image.BufferedImage;
import java.awt.image.ColorConvertOp;
import java.awt.image.ConvolveOp;
import java.awt.image.Kernel;
import java.util.ArrayList;
import java.util.List;

import static javax.swing.JFrame.EXIT_ON_CLOSE;
import static org.pushingpixels.flamingo.api.ribbon.RibbonElementPriority.MEDIUM;
import static org.pushingpixels.flamingo.api.ribbon.RibbonElementPriority.TOP;

/**
 * @author dortm19
 */
public class RibbonMenu {

    private static final long serialVersionUID = 1L;

    public static final Logger log = LogManager.getRootLogger();

    App app;

    private static long creationTime;
    private static long refreshTime;

    static ResizableIcon getResizableIconFromResource(String encodedImage) {
        if (encodedImage.equals("")) {
            return null;
        }
        ResizableIcon resizableIcon = null;
        try {
            byte[] image = Base64.decodeBase64(encodedImage);
            Image icon = new ImageIcon(image).getImage();
            resizableIcon = ImageWrapperResizableIcon.getIcon(icon, new Dimension(48, 48));
        } catch (Exception e) {
            resizableIcon = ImageWrapperResizableIcon.getIcon(RibbonMenu.class.getClassLoader().getResource("com/utd/stb/gui/ribbon/resources/" + encodedImage), new Dimension(48, 48));
        }
        return resizableIcon;
    }

    static Image getImage(String encodedImage) {
        byte[] image = Base64.decodeBase64(encodedImage);
        Image icon = new ImageIcon(image).getImage();
        return icon;
    }

    /**
     * Converts a given Image into a BufferedImage
     *
     * @param img The Image to be converted
     * @return The converted BufferedImage
     */
    public static BufferedImage toBufferedImage(Image img) {
        if (img instanceof BufferedImage) {
            return (BufferedImage) img;
        }
        BufferedImage bimage = null;

        try {
            // Create a buffered image with transparency
            bimage = new BufferedImage(img.getWidth(null), img.getHeight(null), BufferedImage.TYPE_INT_ARGB);

            // Draw the image on to the buffered image
            Graphics2D bGr = bimage.createGraphics();
            bGr.drawImage(img, 0, 0, null);
            bGr.dispose();
        } catch (Exception e) {
            log.error(e);
        }

        return bimage;
    }

    public BufferedImage blur(BufferedImage biSrc) {
        BufferedImage biDest = new BufferedImage(biSrc.getWidth(), biSrc.getHeight(), biSrc.getType());
        float data[] = {0f, 0f, 0f,
            0.0f, 1.4f, 0.0f,
            0f, 0f, 0f};
        Kernel kernel = new Kernel(3, 3, data);
        ConvolveOp convolve = new ConvolveOp(kernel, ConvolveOp.EDGE_NO_OP, null);
        convolve.filter(biSrc, biDest);
        return biDest;
    }

    public BufferedImage toGrayImage(BufferedImage colorImage) {
        BufferedImage gray = new BufferedImage(colorImage.getWidth(), colorImage.getHeight(), BufferedImage.TYPE_INT_ARGB);
        // Automatic converstion....
        ColorConvertOp op = new ColorConvertOp(ColorSpace.getInstance(ColorSpace.CS_GRAY), null);
        op.filter(colorImage, gray);
        return gray;
    }

    String getNodeValue(Element element, String tag) {
        return element.getElementsByTagName(tag).item(0).getTextContent();
    }

    public JRibbon createRibbon(byte[] xml, App app) {
        this.app = app;
        return createRibbon(xml);
    }

    private void refreshTask(RibbonTask task, XmlHandler xPathEvaluator) throws XPathExpressionException {
        for (int j = 0; j < task.getBandCount(); j++) {
            AbstractRibbonBand band = task.getBand(j);
            AbstractBandControlPanel controlPanel = band.getControlPanel();
            boolean hasButtons = false;
            for (int k = 0; k < controlPanel.getComponents().length; k++) {
                ISRRibbonButton button = (ISRRibbonButton) controlPanel.getComponents()[k];
                int id = button.getId();

                boolean active = false;
                boolean visible = false;

                String buttonTooltipHeader = "";
                String buttonTooltipText = "";
                Element buttonElement = null;
                try {
                    buttonElement = (Element) xPathEvaluator.selectNodes("//RIBBONBUTTON[BUTTONID=" + id + "]").item(0);
                    active = getNodeValue(buttonElement, "ACTIVE").equals("T");
                    visible = getNodeValue(buttonElement, "VISIBLE").equals("T");
                    buttonTooltipHeader = getNodeValue(buttonElement, "TOOLTIP_HEADER");
                    buttonTooltipText = getNodeValue(buttonElement, "TOOLTIP");
                } catch (NullPointerException npe) {
                    log.error(npe);
                }

                if (button.isEnabled() != active) {
                    button.setEnabled(active);
                }
                if (button.isVisible() != visible) {
                    button.setVisible(visible);
                }

                RichTooltip tooltip = new RichTooltip(buttonTooltipHeader, buttonTooltipText);
                button.setActionRichTooltip(tooltip);

                if (button.isVisible() && button.isEnabled() && buttonElement != null) {
                    String token = getNodeValue(buttonElement, "TOKEN");
                    if (!token.equals(button.getToken())) {
                        button.setToken(token);
                        button.removeActionListener(button.getActionListener());
                        ActionListener buttonListener = new ISRButtonListener(token);
                        button.setActionListener(buttonListener);
                        button.addActionListener(buttonListener);
                    }
                }

                if (button.getPopupCallback() != null) {

                    JCommandPopupMenu popupMenu = (JCommandPopupMenu) button.getPopupCallback().getPopupPanel(button);
                    boolean hasMenuItems = false;
                    boolean hasActiveMenuItems = false;
                    for (Component item : popupMenu.getMenuComponents()) {
                        ISRRibbonMenuButton menuButton = (ISRRibbonMenuButton) item;
                        int menuButtonId = menuButton.getId();

                        Element menuButtonElement = (Element) xPathEvaluator.selectNodes("//RIBBONBUTTON[BUTTONID=" + menuButtonId + "]").item(0);
                        boolean menuButtonActive = getNodeValue(menuButtonElement, "ACTIVE").equals("T");
                        boolean menuButtonVisible = getNodeValue(menuButtonElement, "VISIBLE").equals("T");
                        String menuButtonTooltipHeader = getNodeValue(menuButtonElement, "TOOLTIP_HEADER");
                        String menuButtonTooltipText = getNodeValue(menuButtonElement, "TOOLTIP");

                        if (menuButtonActive != menuButton.isEnabled()) {
                            menuButton.setEnabled(menuButtonActive);
                        }
                        if (menuButtonVisible != menuButton.isVisible()) {
                            menuButton.setVisible(menuButtonVisible);
                        }
                        RichTooltip menuTooltip = new RichTooltip(menuButtonTooltipHeader, menuButtonTooltipText);
                        menuButton.setActionRichTooltip(menuTooltip);

                        if (!hasMenuItems) {
                            hasMenuItems = menuButtonVisible;
                        }
                        if (!hasActiveMenuItems) {
                            hasActiveMenuItems = menuButtonActive;
                        }

                        if (menuButton.isVisible() && menuButton.isEnabled()) {
                            String menuButtonToken = getNodeValue(menuButtonElement, "TOKEN");
                            if (!menuButtonToken.equals(menuButton.getToken())) {
                                menuButton.setToken(menuButtonToken);
                                menuButton.removeActionListener(menuButton.getActionListener());
                                ActionListener menuButtonListener = new ISRButtonListener(menuButtonToken);
                                menuButton.setActionListener(menuButtonListener);
                                menuButton.addActionListener(menuButtonListener);
                            }
                        }
                    }
                    button.setHasMenu(hasMenuItems);
                    button.setHasActiveMenu(hasActiveMenuItems);

                }

                if (button.hasActiveMenu() && !button.isEnabled()) {
                    button.setEnabled(true);
                }
                button.setCommandButtonKind(!button.hasMenu() ? CommandButtonKind.ACTION_ONLY : (button.getToken().equals("") ? CommandButtonKind.POPUP_ONLY : CommandButtonKind.ACTION_AND_POPUP_MAIN_ACTION));

                if (!hasButtons) {
                    hasButtons = button.isVisible();
                }
            }
        }
    }

    private void deactivateTask(RibbonTask task) {

        for (int j = 0; j < task.getBandCount(); j++) {
            AbstractRibbonBand band = task.getBand(j);
            AbstractBandControlPanel controlPanel = band.getControlPanel();
            try {
                for (int k = 0; k < controlPanel.getComponents().length; k++) {
                    ISRRibbonButton button = (ISRRibbonButton) controlPanel.getComponents()[k];
                    if (button.isEnabled()) {
                        button.setEnabled(false);
                    }
                    if (button.getPopupCallback() != null) {
                        JCommandPopupMenu popupMenu = (JCommandPopupMenu) button.getPopupCallback().getPopupPanel(button);
                        boolean hasActiveMenuItems = false;
                        for (Component item : popupMenu.getMenuComponents()) {
                            ISRRibbonMenuButton menuButton = (ISRRibbonMenuButton) item;
                            if (menuButton.isEnabled()) {
                                menuButton.setEnabled(false);
                            }
                        }
                        button.setHasActiveMenu(hasActiveMenuItems);
                    }
                }
            } catch (Exception e) {
                log.error("error in deactivateTask "  + task.getTitle() + " " + band.getTitle() + ": " + e.getMessage());
            }
        }
    }

    public void deactivateAll(JRibbon ribbon) {

        refreshTime = System.currentTimeMillis();
        //log.debug("Started ribbon refresh " + refreshTime);

        for (int i = 0; i < ribbon.getTaskCount(); i++) {
            RibbonTask task = ribbon.getTask(i);
            deactivateTask(task);
        }

        for (int i = 0; i < ribbon.getContextualTaskGroupCount(); i++) {
            RibbonContextualTaskGroup ribbonContextualTaskGroup = ribbon.getContextualTaskGroup(i);
            for (int j = 0; j < ribbonContextualTaskGroup.getTaskCount(); j++) {
                RibbonTask task = ribbonContextualTaskGroup.getTask(j);
                deactivateTask(task);
            }
        }

        refreshTime = System.currentTimeMillis() - refreshTime;
        //log.debug("Needed milliseconds for ribbon refresh: " + refreshTime);

    }

    public void refreshRibbon(byte[] xml, JRibbon ribbon) throws XPathExpressionException {

        refreshTime = System.currentTimeMillis();
//        log.debug("Started ribbon refresh " + refreshTime);

        XmlHandler xPathEvaluator = new XmlHandler(xml);

        /*try {
         xPathEvaluator.save(new FileOutputStream("C:\\Temp\\"+System.currentTimeMillis()+".xml"));
         } catch (FileNotFoundException ex) {
         log.error(ex);
         }*/
        for (int i = 0; i < ribbon.getTaskCount(); i++) {
            RibbonTask task = ribbon.getTask(i);
            refreshTask(task, xPathEvaluator);
        }

        for (int i = 0; i < ribbon.getContextualTaskGroupCount(); i++) {
            RibbonContextualTaskGroup ribbonContextualTaskGroup = ribbon.getContextualTaskGroup(i);
            boolean tabVisible = xPathEvaluator.selectNodes("(//RIBBONBUTTON[TABCONTEXT=\"" + ribbonContextualTaskGroup.getTitle() + "\"])[1]/TABVISIBLE").item(0).getTextContent().equals("T");
            ribbon.setVisible(ribbonContextualTaskGroup, tabVisible);
            if (tabVisible) {
                for (int j = 0; j < ribbonContextualTaskGroup.getTaskCount(); j++) {
                    RibbonTask task = ribbonContextualTaskGroup.getTask(j);
                    refreshTask(task, xPathEvaluator);
                }
                //ribbon.setSelectedTask(ribbonContextualTaskGroup.getTask(0));
            }
        }

        refreshTime = System.currentTimeMillis() - refreshTime;
//        log.debug("Needed milliseconds for ribbon refresh: " + refreshTime);

    }

    public JRibbon createRibbon(byte[] xml) {

        creationTime = System.currentTimeMillis();
        log.info("Started ribbon creation " + creationTime);

        final XmlHandler xPathEvaluator = new XmlHandler(xml);
//        log.debug(xPathEvaluator.xmlToString());

        JRibbon ribbon = new JRibbon();

        String tabXPath = "(//RIBBONBUTTON/RIBBONTAB[not(.=preceding::RIBBONBUTTON/RIBBONTAB)])";
        NodeList tabs = null;
        try {
            tabs = xPathEvaluator.selectNodes(tabXPath);

            for (int t = 0; t < tabs.getLength(); t++) {
                Node tab = tabs.item(t);
                String ribbonTab = tab.getTextContent();
                log.trace("Tab " + t + ": " + ribbonTab + " - " + System.currentTimeMillis());
                String tabContext = xPathEvaluator.selectNodes("//RIBBONBUTTON[RIBBONTAB=\"" + ribbonTab + "\"]/TABCONTEXT").item(0).getTextContent();

                List<JRibbonBand> currentButtonGroups = new ArrayList<JRibbonBand>();

                boolean hasButtonGroups = false;

                String groupXPath = "(//RIBBONBUTTON[RIBBONTAB=\"" + ribbonTab + "\"]/BUTTONGROUP[not(.=preceding::RIBBONBUTTON[RIBBONTAB=\"" + ribbonTab + "\"]/BUTTONGROUP)])";
                NodeList groups = xPathEvaluator.selectNodes(groupXPath);
                for (int g = 0; g < groups.getLength(); g++) {
                    Node group = groups.item(g);
                    String buttonGroup = group.getTextContent();

                    List<RibbonBandResizePolicy> currentButtonGroupPolicy = new ArrayList<RibbonBandResizePolicy>();
                    JRibbonBand currentButtonGroup = new JRibbonBand(buttonGroup, null);

                    boolean hasButtons = false;

                    String buttonXPath = "(//RIBBONBUTTON[RIBBONTAB=\"" + ribbonTab + "\" and BUTTONGROUP=\"" + buttonGroup + "\" and not(starts-with(BUTTONTYPE, 'MENU_ITEM_'))])";
                    NodeList buttons = xPathEvaluator.selectNodes(buttonXPath);
                    for (int i = 0; i < buttons.getLength(); i++) {
                        Node button = buttons.item(i);
                        Element buttonElement = (Element) button;
                        String buttonId = getNodeValue(buttonElement, "BUTTONID");

                        String buttonText = getNodeValue(buttonElement, "BUTTON");
                        String buttonImage = getNodeValue(buttonElement, "ICON");
                        boolean large = getNodeValue(buttonElement, "LARGE").equals("T");
                        boolean active = getNodeValue(buttonElement, "ACTIVE").equals("T");
                        boolean visible = getNodeValue(buttonElement, "VISIBLE").equals("T");

                        String buttonTooltipHeader = getNodeValue(buttonElement, "TOOLTIP_HEADER");
                        String buttonTooltipText = getNodeValue(buttonElement, "TOOLTIP");
                        RichTooltip currentTooltip = new RichTooltip(buttonTooltipHeader, buttonTooltipText);

                        String buttonType = getNodeValue(buttonElement, "BUTTONTYPE");

                        ISRRibbonButton currentButton = new ISRRibbonButton(buttonText, getResizableIconFromResource(buttonImage));
                        Image icon = getImage(buttonImage);
                        BufferedImage bufferedImage = toBufferedImage(icon);
                        if (bufferedImage != null) {
                            BufferedImage grayImage = new GrayscaleFilter().filter(bufferedImage, null);
                            BufferedImage blurImage = blur(grayImage);
//                    BufferedImage bluredImage = changeIntensity(grayImage, 1.2f);
                            ResizableIcon disabledIcon = ImageWrapperResizableIcon.getIcon(blurImage, new Dimension(48, 48));
                            currentButton.setDisabledIcon(disabledIcon);
                        }
                        currentButton.setId(new Integer(buttonId));
                        currentButton.setEnabled(active);
                        currentButton.setVisible(visible);

                        if (!hasButtons) {
                            hasButtons = visible && !buttonType.equals("MAINBUTTON") && !buttonType.equals("TASKPANELBUTTON") && !buttonType.equals("HELPBUTTON");
                        }
                        currentButton.setDisplayState(large ? CommandButtonDisplayState.BIG : CommandButtonDisplayState.MEDIUM);
                        currentButton.setActionRichTooltip(currentTooltip);

                        ActionListener currentButtonListener = null;
                        if (currentButton.isVisible() && currentButton.isEnabled()) {
                            String token = getNodeValue(buttonElement, "TOKEN");
                            currentButton.setToken(token);
                            app.setRibbonAccelerator(token);
                            currentButtonListener = new ISRButtonListener(token);
                            currentButton.setActionListener(currentButtonListener);
                            currentButton.addActionListener(currentButtonListener);
                        }

                        if (buttonType.contains("ARROW_DOWN")) {

                            boolean hasMenuItems = false;
                            boolean hasActiveMenuItems = false;

                            final JCommandPopupMenu menu = new JCommandPopupMenu();
                            final String menuButtonXPath = "(//RIBBONBUTTON[RIBBONTAB=\"" + ribbonTab + "\" and BUTTONGROUP=\"" + buttonGroup + "\" and BUTTONTYPE=\"MENU_ITEM_" + buttonId + "\"])";
                            final NodeList menuButtons = xPathEvaluator.selectNodes(menuButtonXPath);
                            for (int j = 0; j < menuButtons.getLength(); j++) {
                                Node menuButton = menuButtons.item(j);
                                Element menuButtonElement = (Element) menuButton;

                                String menuButtonButtonId = getNodeValue(menuButtonElement, "BUTTONID");

                                String menuButtonText = getNodeValue(menuButtonElement, "BUTTON");
                                String menuButtonImage = getNodeValue(menuButtonElement, "ICON");

                                String menuButtonTooltipHeader = getNodeValue(menuButtonElement, "TOOLTIP_HEADER");
                                String menuButtonTooltipText = getNodeValue(menuButtonElement, "TOOLTIP");
                                RichTooltip currentMenuTooltip = new RichTooltip(menuButtonTooltipHeader, menuButtonTooltipText);

                                boolean menuButtonActive = getNodeValue(menuButtonElement, "ACTIVE").equals("T");
                                boolean menuButtonVisible = getNodeValue(menuButtonElement, "VISIBLE").equals("T");

                                ResizableIcon resizableIcon = getResizableIconFromResource(menuButtonImage);
                                ISRRibbonMenuButton currentMenuButton = new ISRRibbonMenuButton(menuButtonText, resizableIcon);

                                currentMenuButton.setId(new Integer(menuButtonButtonId));
                                currentMenuButton.setEnabled(menuButtonActive);
                                currentMenuButton.setVisible(menuButtonVisible);
                                currentMenuButton.setActionRichTooltip(currentMenuTooltip);
                                if (!hasMenuItems) {
                                    hasMenuItems = menuButtonVisible;
                                }
                                if (!hasActiveMenuItems) {
                                    hasActiveMenuItems = menuButtonActive;
                                }

                                String menuButtonToken = getNodeValue(menuButtonElement, "TOKEN");
                                currentMenuButton.setToken(menuButtonToken);
                                ActionListener currentMenuButtonListener = new ISRButtonListener(menuButtonToken);
                                currentMenuButton.setActionListener(currentMenuButtonListener);
                                currentMenuButton.addActionListener(currentMenuButtonListener);

                                menu.addMenuButton(currentMenuButton);
                            }

                            currentButton.setHasMenu(hasMenuItems);
                            currentButton.setHasActiveMenu(hasActiveMenuItems);

                            currentButton.setPopupCallback(new PopupPanelCallback() {
                                public JPopupPanel getPopupPanel(JCommandButton commandButton) {
                                    return menu;
                                }
                            });
                        }
                        if (currentButton.hasActiveMenu() && !currentButton.isEnabled()) {
                            currentButton.setEnabled(true);
                        }
                        currentButton.setCommandButtonKind(!currentButton.hasMenu() ? CommandButtonKind.ACTION_ONLY : (currentButton.getToken().equals("") ? CommandButtonKind.POPUP_ONLY : CommandButtonKind.ACTION_AND_POPUP_MAIN_ACTION));
                        switch (buttonType) {
                            case "MAINBUTTON":

                                RibbonApplicationMenu ribbonApplicationMenu = new RibbonApplicationMenu();
                                ribbon.setApplicationMenu(ribbonApplicationMenu);
                                ribbon.setApplicationMenuRichTooltip(currentTooltip);

                                for (Component comp : ribbon.getComponents()) {
                                    if (comp instanceof JRibbonApplicationMenuButton) {
                                        ((JRibbonApplicationMenuButton) comp).setIcon(currentButton.getIcon());
                                        final ActionListener currentMainButtonListener = currentButtonListener;
                                        ((JRibbonApplicationMenuButton) comp).setPopupCallback(new PopupPanelCallback() {
                                            public JPopupPanel getPopupPanel(JCommandButton commandButton) {
                                                if (currentMainButtonListener != null) {
                                                    currentMainButtonListener.actionPerformed(null);
                                                }
                                                return null;
                                            }
                                        });

                                    }
                                }
                                break;
                            case "TASKPANELBUTTON":
                                ribbon.addTaskbarComponent(currentButton);
                                break;
                            case "HELPBUTTON":
                                ribbon.configureHelp(currentButton.getIcon(), currentButtonListener);
                                break;
                            default:
                                currentButtonGroup.addCommandButton(currentButton, large ? TOP : MEDIUM);
                                break;
                        }

                    }

                    if (hasButtons) {
                        currentButtonGroupPolicy.add(new CoreRibbonResizePolicies.Mirror(currentButtonGroup.getControlPanel()));
                    }

                    currentButtonGroupPolicy.add(new IconRibbonBandResizePolicy(currentButtonGroup.getControlPanel()));
                    currentButtonGroup.setResizePolicies(currentButtonGroupPolicy);

                    try {
                        FlamingoUtilities.checkResizePoliciesConsistency(currentButtonGroup);
                    } catch (Exception ignored) {
                        reorganizePolicies(currentButtonGroup, true);
                    }

                    currentButtonGroup.setVisible(hasButtons);
                    currentButtonGroups.add(currentButtonGroup);
                    if (!hasButtonGroups) {
                        hasButtonGroups = hasButtons;
                    }
                }

                if (hasButtonGroups) {
                    RibbonTask currentTab = new RibbonTask(ribbonTab, currentButtonGroups.toArray(new JRibbonBand[0]));
                    if (!tabContext.equals("")) {
                        RibbonContextualTaskGroup ribbonContextualTaskGroup = new RibbonContextualTaskGroup(tabContext, Color.RED, currentTab);
                        ribbon.addContextualTaskGroup(ribbonContextualTaskGroup);
                    } else {
                        ribbon.addTask(currentTab);
                    }
                }
            }

            creationTime = System.currentTimeMillis() - creationTime;
            log.info("Needed milliseconds for ribbon creation: " + creationTime);
        } catch (XPathExpressionException e) {
            log.error(e);
        }
        return ribbon;

    }

    protected void reorganizePolicies(JRibbonBand band, boolean tolerateExceptions) {
        Insets ins = band.getInsets();
        AbstractBandControlPanel controlPanel = band.getControlPanel();
        if (controlPanel == null) {
            return;
        }
        int height = controlPanel.getPreferredSize().height
                + band.getUI().getBandTitleHeight() + ins.top
                + ins.bottom;
        List<RibbonBandResizePolicy> resizePolicies = band.getResizePolicies();
        FlamingoUtilities.checkResizePoliciesConsistencyBase(band);
        int index = -1;
        while ((index = checkPolicies(band, height, resizePolicies)) > -1) {
            if (tolerateExceptions) {
                band.setResizePolicies(buildNewList(resizePolicies, index));
                break;
            } else {
                throw new IllegalStateException(getExceptionMessage(band, height, resizePolicies, index));
            }

        }
    }

    private List buildNewList(List<RibbonBandResizePolicy> resizePolicies, int index) {
        ArrayList<RibbonBandResizePolicy> newList = new ArrayList<RibbonBandResizePolicy>();
        for (int i = 0; i < resizePolicies.size(); i++) {
            if (i == index) {
                continue;
            }
            newList.add(resizePolicies.get(i));
        }
        return newList;
    }

    private int checkPolicies(AbstractRibbonBand ribbonBand, int height, List<RibbonBandResizePolicy> resizePolicies) {
        for (int i = 0; i < (resizePolicies.size() - 1); i++) {
            RibbonBandResizePolicy policy1 = resizePolicies.get(i);
            RibbonBandResizePolicy policy2 = resizePolicies.get(i + 1);
            int width1 = policy1.getPreferredWidth(height, 4);
            int width2 = policy2.getPreferredWidth(height, 4);
            if (width1 < width2) {
                return i + 1;
            }
        }
        return -1;
    }

    private String getExceptionMessage(AbstractRibbonBand ribbonBand, int height, List<RibbonBandResizePolicy> resizePolicies,
            int errorIndex) {
        RibbonBandResizePolicy policy1 = resizePolicies.get(errorIndex - 1);
        RibbonBandResizePolicy policy2 = resizePolicies.get(errorIndex);
        int width1 = policy1.getPreferredWidth(height, 4);
        // create the trace message
        StringBuilder builder = new StringBuilder();
        builder.append("Inconsistent preferred widths\n");
        builder.append("Ribbon band '" + ribbonBand.getTitle()
                + "' has the following resize policies\n");
        for (int j = 0; j < resizePolicies.size(); j++) {
            RibbonBandResizePolicy policy = resizePolicies.get(j);
            int width = policy.getPreferredWidth(height, 4);
            builder.append("\t" + policy.getClass().getName()
                    + " with preferred width " + width + "\n");
        }
        builder.append(policy1.getClass().getName()
                + " with pref width " + width1
                + " is followed by resize policy "
                + policy2.getClass().getName()
                + " with larger pref width\n");

        return builder.toString();
    }

    public static void updateUI(final JRootPane rootPane, final String laf, final String skin) {
        //SwingUtilities.invokeLater(new Runnable() {
        //public void run() {
        try {
            UIManager.setLookAndFeel(laf);
            if (!skin.equals("")) {
                SubstanceLookAndFeel.setSkin(skin);
            }
            SwingUtilities.updateComponentTreeUI(rootPane);

            //  Use custom decorations when supported by the LAF
            JFrame frame = (JFrame) SwingUtilities.windowForComponent(rootPane);
            frame.dispose();
            TweakUI.setFrameDecoration(frame);
            frame.setVisible(true);
        } catch (Exception e) {
            e.printStackTrace();
            log.debug("Could not update ui: " + e);
        }
        //}
        //});
    }

    public static void main(String args[]) {

        SwingUtilities.invokeLater(new Runnable() {

            @Override
            public void run() {

                try {

                    TweakUI.setLookAndFeel();

                    JFrame frame = new JFrame();
                    TweakUI.setFrameDecoration(frame);
                    frame.setSize(1920, 1080);
                    frame.setTitle("Ribbon");
                    frame.setDefaultCloseOperation(EXIT_ON_CLOSE);

                    JPanel main = new JPanel(new BorderLayout());

                    RibbonMenu ribbonMenu = new RibbonMenu();
                    JRibbon ribbon = ribbonMenu.createRibbon(ConvertUtils.fileToBytes("ribbon.xml"));
                    main.add(ribbon, BorderLayout.NORTH);
                    frame.getContentPane().add(main);

                    frame.setVisible(true);

                    //ribbonMenu.refreshRibbon(new FileInputStream("ribbon_refresh.xml"), ribbon);
                    //String layoutName = "OfficeBlue2007";//"OfficeBlack2007";                    
                    //ribbonMenu.updateUI(frame.getRootPane(), "org.pushingpixels.substance.api.skin.Substance" + layoutName + "LookAndFeel", "org.pushingpixels.substance.api.skin." + layoutName + "Skin");
                } catch (Exception ex) {
                    ex.printStackTrace();
                    log.error(ex);
                }
            }
        });

    }

    class ISRButtonListener implements ActionListener {

        String token;

        public ISRButtonListener(String token) {
            this.token = token;
        }

        @Override
        public void actionPerformed(ActionEvent e) {
            log.debug("Clicked " + token);
            if (app != null) {
                app.ribbonActionPerformed(e, token);
            }
        }
    }

}
