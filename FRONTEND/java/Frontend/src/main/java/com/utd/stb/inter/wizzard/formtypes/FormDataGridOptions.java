package com.utd.stb.inter.wizzard.formtypes;

import com.utd.stb.inter.application.BackendException;
import com.utd.stb.inter.items.DisplayableItemComboList;
import com.utd.stb.inter.wizzard.FormData;


/**
 * Choosable items are organised in a 2dim table. Columns are of same type. In a
 * single row the choosable items to the right (RI) of a 'previous' item (PI)
 * may depend on the selection of this PI. So the RIs needs to get reloaded on a
 * change.
 *
 * @author PeterBuettner.de
 */
public interface FormDataGridOptions extends FormData {

    /**
     * Number of rows, doesn't change
     *
     * @return
     */
    int getGridRowCount();

    /**
     * Number of columns, doesn't change
     *
     * @return
     */
    int getGridColumnCount();

    /**
     * All rows have the same structure, maybe some are shorter than others,
     * value is null if no backing data in the 'cell'
     *
     * @param row
     * @param column
     * @return
     */
    DisplayableItemComboList getValue(int row, int column);

    /**
     * @param column
     * @return
     * @throws BackendException
     */
    String getColumnTitle(int column) throws BackendException;

    /**
     * @param row
     * @return
     */
    String getRowTitle(int row);

    /**
     * The label to be displayed above the grid.
     *
     * @return
     */
    String getLabel();

}