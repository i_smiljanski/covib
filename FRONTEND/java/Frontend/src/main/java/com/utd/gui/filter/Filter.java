package com.utd.gui.filter;

import javax.swing.event.ChangeListener;


/**
 * Interface to implement generic usable filters
 *
 * @author PeterBuettner.de
 */
public interface Filter {

    /**
     * The Filter must be able to deal with null candidates
     *
     * @param candidate
     * @return
     */
    boolean matches(Object candidate);

    /**
     * Adds a listener to the list that is notified each time a change to the
     * filter occurs.
     *
     * @param l the ChangeListener
     */
    public void addChangeListener(ChangeListener l);

    /**
     * Removes a listener from the list that is notified each time a change to
     * the filter occurs.
     *
     * @param l the ChangeListener
     */
    public void removeChangeListener(ChangeListener l);

}