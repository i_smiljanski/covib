package com.utd.stb.back.data.client;

import com.utd.stb.inter.application.Node;
import com.utd.stb.inter.application.Nodes;

import java.util.ArrayList;
import java.util.Iterator;

/**
 * Wraps a list of record data of type TreeNodeRecord
 *
 * @author rainer bruns Created 27.08.2004
 * @see TreeNodeRecord
 */
public class TreeNodeList implements Nodes {

    ArrayList<Node> treeList = new ArrayList<>();
    Iterator it;

    /**
     * creates an empty TreeNodeList
     */
    public TreeNodeList() {

        treeList = new ArrayList<>();
    }

    /**
     * adds the TreeNodeRecord to this list
     *
     * @param rec
     */
    public void add(TreeNodeRecord rec) {

        treeList.add(rec);
    }

    /*
     * (non-Javadoc)
     * 
     * @see com.utd.stb.inter.application.Nodes#getNode(int)
     */
    public Node getNode(int index) {

        return treeList.get(index);
    }

    /**
     * @param index
     * @return the TreeNodeRecord at index
     */
    public TreeNodeRecord get(int index) {
        if (treeList.size() <= index) {
            return new TreeNodeRecord();
        }
        return (TreeNodeRecord) treeList.get(index);
    }

    /**
     * sets list iterator to the beginning
     */
    public void initIterator() {

        it = treeList.iterator();
    }

    /**
     * @return true if there is a next record
     */
    public boolean hasNext() {

        return it.hasNext();
    }

    /**
     * @return the next TreeNodeRecord
     */
    public TreeNodeRecord getNext() {

        return (TreeNodeRecord) it.next();
    }

    /*
     * (non-Javadoc)
     * 
     * @see com.utd.stb.inter.application.Nodes#getSize()
     */
    public int getSize() {

        return treeList.size();
    }

    /*
     * (non-Javadoc)
     * 
     * @see com.utd.stb.inter.application.Nodes#indexOf(com.utd.stb.inter.application.Node)
     */
    public int indexOf(Node node) {

        return treeList.indexOf(node);
    }

    public void addRow(int i) {
        treeList.add(i, get(i));
    }

    public void deleteRow(int i) {
        treeList.remove(i);
    }

    public String toString() {
        /*return "TreeNodeList{" +
                "data=" + treeList +
                '}';*/
        String treeNodeListString = "";
        for (int i = 0; i < treeList.size(); i++) {
            TreeNodeRecord rec = (TreeNodeRecord) treeList.get(i);
            treeNodeListString += "node " + i + " " + rec.toStringForLog() + "\n";
        }
        return treeNodeListString;
    }

}
