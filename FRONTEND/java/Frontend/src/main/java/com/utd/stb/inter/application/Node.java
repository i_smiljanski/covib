package com.utd.stb.inter.application;


/**
 * An object to be displayed int the etree or detail area of the gui
 *
 * @author PeterBuettner.de
 */
public interface Node {

    int MENU_DIALOG = 1;
    int MENU_DETAIL_VIEW = 2;
    int ESIG_DIALOG = 3;
    int OWN_PASSWORD_DIALOG = 4;
    int MENU_LIST_DIALOG = 5;
    int MENU_CALL_PROCESS = 6;

    /**
     * The Text displayed, e.g. in the tree or in the header of a list.
     *
     * @return
     */
    String getText();

    void setShowText(boolean show);

    boolean isShowText();

    /**
     * The display type of a node
     *
     * @return
     */
    Integer getDisplayType();

    /**
     * references a icon to be displayed, e.g. in the tree or in the header of a
     * list. If null a default is used.
     *
     * @return
     */
    String getIconId();

    String getTooltip();

    /**
     * very important to overwrite this to find reloaded nodes
     */
    public int hashCode();

    /**
     * very important to overwrite this to find reloaded nodes
     */
    public boolean equals(Object obj);

    public void setIconId(String icon);

}