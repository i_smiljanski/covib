package com.utd.stb.back.ui.impl.lib;

import com.utd.stb.inter.action.WindowAction;


/**
 * @author PeterBuettner.de
 */
public class WindowActionImpl implements WindowAction {

    private Object type;

    public WindowActionImpl(Object type) {

        this.type = type;
    }

    public Object getType() {

        return type;
    }

    public String toString() {

        return getClass().getName() + "; Type:" + type;
    }
}