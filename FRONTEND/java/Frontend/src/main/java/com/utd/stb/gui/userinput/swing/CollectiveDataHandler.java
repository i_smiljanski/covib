package com.utd.stb.gui.userinput.swing;

import com.utd.stb.inter.userinput.UserInput;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import javax.swing.*;
import java.awt.*;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Iterator;
import java.util.List;


/**
 * Handler to do Validation for a collection of components in a
 * (JComponent)container, collects all children, grandchildren,..., it can also
 * iterate over all handlers (components+userInputs)
 *
 * @author PeterBuettner.de
 */
public class CollectiveDataHandler {

    public static final Logger log = LogManager.getRootLogger();
    protected JComponent container;
    private List handler = Collections.EMPTY_LIST;

    protected CollectiveDataHandler(JComponent container) {

        this.container = container;
    }

    /**
     * get all handlers of all contained subcomponents (also grandchildren etc.)
     * whose UserInput is: "!isReadOnly() && isVisible", <br>
     * <p>
     * Note: call {@link collectComponents()}before, or it doesn't work
     *
     * @return
     */
    protected Iterator handlerIterator() {

        return handler.iterator();
    }

    /**
     * @return true if all handlers are valid
     */
    protected boolean isValid() {

        for (Object aHandler : handler) {
            if (aHandler instanceof UIODataHandler) {
                UIODataHandler uioDataHandler = (UIODataHandler) aHandler;
                if (!uioDataHandler.isValid()) return false;
            }
        }
        return true;// 'no' components are always valid
    }

    /**
     * @return true if at least one handler is empty, also accounts the
     * isEmptyAllowed flag and sees those fields as not empty!
     */
    protected boolean isEmpty() {

        for (Object aHandler : handler) {
            if (aHandler instanceof UIODataHandler) {
                UIODataHandler h = (UIODataHandler) aHandler;
                // log.debug(h.getUserInput().isEmptyAllowed()+" "+h.isEmpty()+" "+ h.userInput.getLabel()+" "+h.getClass());
                if (h.isEmpty() && !h.getUserInput().isEmptyAllowed()) return true;
            }
        }
        return false;// 'no' components are never empty
    }

    /**
     * push data from all controls to UserInput
     */
    protected void commitData() {

        for (Object aHandler : handler) {
            if (aHandler instanceof UIODataHandler) {
                UIODataHandler h = (UIODataHandler) aHandler;
                h.commitData();
            }
        }
    }

    /**
     * recursive walk through all childs, get all UIODataHandler, clears
     * internal list before.
     */
    protected void collectComponents() {

        handler = new ArrayList();
        collectComponents(container);
    }

    /**
     * recursive walk through all childs, get all UIODataHandler and put them
     * into the list.
     *
     * @param parent
     */
    private void collectComponents(Container parent) {

        Component[] cc = parent.getComponents();
        for (Component aCc : cc) {
            if (aCc instanceof JComponent) {// seek handler
                Object h = ((JComponent) aCc).getClientProperty(UIODataHandler.class);
                if (h instanceof UIODataHandler) {
                    UserInput ui = ((UIODataHandler) h).getUserInput();
                    if (!ui.isReadOnly() && ui.isVisible()) handler.add(h);
                }
            }
            if (aCc instanceof Container) // recursion
                collectComponents((Container) aCc);
//            if (aCc instanceof SortableTable) {
//                SortableTable h = (SortableTable) aCc;
//                FilterTableData.ContextDefaultTableModel defaultTableModel =
//                        (FilterTableData.ContextDefaultTableModel) TableModelWrapperUtils.getActualTableModel(h.getModel());
//                System.out.println("defaultTableModel = " + defaultTableModel);
//            }
        }
    }
}