package com.utd.stb.back.ui.impl.wizard.formdata;

import com.utd.stb.back.data.client.FormInfoRecord;
import com.utd.stb.back.data.client.SelectionList;
import com.utd.stb.back.data.client.SelectionRecord;
import com.utd.stb.back.database.Dispatcher;
import com.utd.stb.back.ui.inter.WizardFormData;
import com.utd.stb.inter.application.BackendException;
import com.utd.stb.inter.items.DisplayableItems;
import com.utd.stb.inter.wizzard.formtypes.FormDataListOutOfCategoriesList;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.util.ArrayList;
import java.util.Enumeration;
import java.util.Hashtable;


/**
 * Handles the data in MultiSelect window
 *
 * @author rainer bruns Created 13.12.2004
 */
public class MultiSelect implements FormDataListOutOfCategoriesList, WizardFormData {
    public static final Logger log = LogManager.getRootLogger();

    Dispatcher dispatcher;
    SelectionList categories = new SelectionList();
    SelectionList selection;
    String masterValue;
    String suchwert;
    String entity;
    ArrayList selectedData = new ArrayList();
    ArrayList deSelectedData = new ArrayList();
    ArrayList allChilds = new ArrayList();
    ArrayList selectedMaster = new ArrayList();
    Hashtable dataMap = new Hashtable();
    Hashtable dataToSave;
    private FormInfoRecord formInfo;
    private boolean navigate;

    /**
     * @param d        the dispatcher
     * @param data     the list of categories
     * @param entity   the entity
     * @param formInfo record with form information, used for the labels
     * @throws BackendException
     */
    public MultiSelect(Dispatcher d, SelectionList data, String entity,
                       FormInfoRecord formInfo, boolean navigate) throws BackendException {

        this.dispatcher = d;
        this.categories = data;
        this.entity = entity;
        this.formInfo = formInfo;
        this.navigate = navigate;
        getDetails();
    }

    /**
     * for each category, go through the list of details and check if selected
     *
     * @return the lst of selected data
     */
    private SelectionList getSelectedData() {

        SelectionList selectedData = new SelectionList();
        categories.initIterator();
        while (categories.hasNext()) {
            SelectionRecord master = categories.getNext();
            SelectionList details = (SelectionList) dataMap.get(master);
            details.initIterator();
            while (details.hasNext()) {
                SelectionRecord s = details.getNext();
                if (s.isSelected()) {
                    selectedData.add(s);
                }
            }
        }
        return selectedData;
    }

    /*
     * (non-Javadoc) Hashtable dataToSave contains the selected data for each
     * category. This is prepared in setSelectedItems. For each category calls
     * putlist with the list of details.
     * 
     * @see com.utd.stb.inter.wizzard.FormData#saveData()
     */
    public void saveData() throws BackendException {

        SelectionList detailList = new SelectionList();
        for (int i = 0; i < selectedMaster.size(); i++) {
            SelectionRecord master = (SelectionRecord) selectedMaster.get(i);
            SelectionList details = (SelectionList) dataToSave.get(master);
            for (int j = 0; j < details.getSize(); j++) {
                detailList.add((SelectionRecord) details.get(j));
            }
        }
        dispatcher.save(entity, detailList, false);
    }

    /*
     * (non-Javadoc) returns the list of categories
     * 
     * @see com.utd.stb.inter.wizzard.formtypes.FormDataListOutOfCategoriesList#getCategoryList()
     */
    public DisplayableItems getCategoryList() {

        return categories;
    }

    /*
     * (non-Javadoc) returns the selected details for category number (arrgh...)
     * 
     * @see com.utd.stb.inter.wizzard.formtypes.FormDataListOutOfCategoriesList#getSelectedItems(int)
     */
    public DisplayableItems getSelectedItems(int categoryNumber) {

        return (SelectionList) selectedData.get(categoryNumber);
    }

    /*
     * (non-Javadoc) returns all details for category number
     * 
     * @see com.utd.stb.inter.wizzard.formtypes.FormDataListOutOfCategoriesList#getCategoryItems(int)
     */
    public DisplayableItems getCategoryItems(int categoryNumber) {

        return (SelectionList) allChilds.get(categoryNumber);
    }

    /**
     * for each category get the details from backend. Store selected details
     * and deselected details and all details in seperate ArrayLists ( this is
     * because GUI queries details for category by number). Store all details in
     * a Hashtable with category as key (prefer this to be the only storage, but
     * GUI must support)
     *
     * @throws BackendException
     */
    private void getDetails() throws BackendException {

        SelectionList allItems = dispatcher.getAvailableItems(entity, null, false, false);

        categories.initIterator();
        while (categories.hasNext()) {
            SelectionRecord master = categories.getNext();
//            log.debug("master = " + master);
            SelectionList childs = new SelectionList();
            SelectionList selectedChilds = new SelectionList();
            SelectionList deSelectedChilds = new SelectionList();
            allItems.initIterator();
            while (allItems.hasNext()) {
                SelectionRecord selection = allItems.getNext();
                if (selection.getMasterKey().equals(master.getValue()) ||
                        selection.getMasterKey().equals(master.getMasterKey())) {
                    childs.add(selection);
                    // get selected and deselected childs
                    if (selection.isSelected())
                        selectedChilds.add(selection);
                    else
                        deSelectedChilds.add(selection);
                }
            }
            selectedData.add(selectedChilds);
            deSelectedData.add(deSelectedChilds);
            allChilds.add(childs);
            selectedMaster.add(master);
            dataMap.put(master, childs);
        }

    }

    /*
     * (non-Javadoc) parameter is a list of all selected details(in a for here
     * random order). go through the map of details and see if detail object can
     * be found in in selected details. If so, add to a temporary SelectionList,
     * which then is stored together with its category object as key in the
     * global Hashtable used by saveData method
     * 
     * @see com.utd.stb.inter.wizzard.formtypes.FormDataListOutOfCategoriesList#setSelectedItems(com.utd.stb.inter.items.DisplayableItems)
     */
    public void setSelectedItems(DisplayableItems item) {

        boolean found;
        dataToSave = new Hashtable();
        Enumeration e = dataMap.keys();
        /*
         * for each category object initialise temp SelectionList, get the
         * details list
         */
        while (e.hasMoreElements()) {
            SelectionList listForMasterToSave = new SelectionList();
            SelectionRecord master = (SelectionRecord) e.nextElement();
            SelectionList list = (SelectionList) dataMap.get(master);
            
            /*
             * for each detail in list, check if in list of selected details
             */
            for (int i = 0; i < item.getSize(); i++) {
                SelectionRecord rec = (SelectionRecord) item.get(i);

                list.initIterator();
                found = false;
                while (list.hasNext() && !found) {
                    SelectionRecord r = list.getNext();
                    
                    /*
                     * if detail object is found in selected data add to
                     * temporary list and finish inner loop
                     */
                    if (r.equals(rec)) {
                        listForMasterToSave.add(rec);
                        found = true;
                    }
                }
            }
            /*
             * put the list of selected details together with its master object
             * in table to be saved
             */
            dataToSave.put(master, listForMasterToSave);
        }
    }

    /*
     * (non-Javadoc) label on left side
     * 
     * @see com.utd.stb.inter.wizzard.formtypes.FormDataListOutOfCategoriesList#getLabelAllItems()
     */
    public String getLabelAllItems() {

        return formInfo.getPrompt1();
    }

    /*
     * (non-Javadoc) label on right side
     * 
     * @see com.utd.stb.inter.wizzard.formtypes.FormDataListOutOfCategoriesList#getLabelChoosenItems()
     */
    public String getLabelChoosenItems() {

        return formInfo.getPrompt2();
    }

    /* (non-Javadoc)
     * @see com.utd.stb.back.ui.inter.WizardFormData#getDataToSave()
     */
    public SelectionList getDataToSave() {

        return getSelectedData();
    }

    /* (non-Javadoc)
     * @see com.utd.stb.back.ui.inter.WizardFormData#getEntity()
     */
    public String getEntity() {
        return entity;
    }

}

