package com.utd.stb.back.ui.impl.userinputs.fields;

import com.utd.stb.back.data.client.PropertyRecord;
import com.utd.stb.inter.userinput.TextInput;


/**
 * Implementation of AbstractUserInput for text fields
 *
 * @author rainer bruns Created 02.12.2004
 */
public class TextUserInput extends AbstractUserInput {

    private TextInput textInput;
    private PropertyRecord property;

    /**
     * Creates the TextUserInput with label, TextInput and complete data record
     *
     * @param label    Label of field
     * @param data     The wrap around the String
     * @param property The complete record of field data
     */
    public TextUserInput(String label, TextInput data, PropertyRecord property) {

        super(label);
        this.textInput = data;
        this.property = property;
    }

    /*
     * (non-Javadoc)
     * 
     * @see com.utd.stb.inter.userinput.UserInput#getType()
     */
    public Class getType() {

        return TextInput.class;
    }

    /*
     * (non-Javadoc) Sets new text in TextInput and in value field of original
     * data record
     * 
     * @see com.utd.stb.inter.userinput.UserInput#setData(java.lang.Object)
     */
    public void setData(Object data) {

        textInput.setText((String) data);
        property.setValue(data);
    }


    /*
    * (non-Javadoc)
    *
    * @see com.utd.stb.inter.userinput.UserInput#getData()
    */
    public Object getData() {

        return textInput;
    }

    /**
     * Called when saving data to db
     *
     * @return the data record
     * @see AbstractInputData#getListToSave()
     */
    public PropertyRecord getProperty() {

        return property;
    }

}