package com.utd.gui.animate;

import javax.swing.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.Iterator;
import java.util.WeakHashMap;


/**
 * Helper to ease animation in swing gui. Usage example:
 * <p>
 * <pre>
 *
 * private void sizeColumn( final int end, final TableColumn col ) {
 *
 *     int start = col.getWidth();
 *     if ( start == end ) return;
 *
 *     SwingAnimator ani = new SwingAnimator() {
 *
 *         protected void processValue( Number value, boolean isLastValue ) {
 *
 *             if ( isLastValue )
 *                 col.setPreferredWidth( end );
 *             else
 *                 col.setPreferredWidth( value.intValue() );
 *         }
 *     };
 *     Iterator values = new Mover( start, end ).iteratorLinearInteger();
 *     ani.run( this, 20, values );
 * }
 * </pre>
 *
 * @author PeterBuettner.de
 */
public abstract class SwingAnimator {

    // don't put null into, since all nulls are seen as the same key.
    private final static WeakHashMap locks = new WeakHashMap();

    /**
     * Starts a timer, calls setValue() at the timer intervals; if an animation
     * with singleAnimation is running, this method returns without any action.
     *
     * @param singleAnimation use null to start animation without lock checking, put any
     *                        Object as a key to lock simultaneous animations
     * @param intervalMillies
     * @param values          delivers the data (Number) to send to processValue()
     * @return true if animating, false if an animation with singleAnimation is
     * running
     */
    public boolean run(final Object singleAnimation, int intervalMillies,
                       final Iterator values) {

        if (isLocked(singleAnimation)) return false;

        Timer timer = new Timer(intervalMillies, new ActionListener() {

            public void actionPerformed(ActionEvent evt) {

                Number value = (Number) values.next();
                if (values.hasNext())
                    processValue(value, false);
                else {// last step
                    ((Timer) evt.getSource()).stop();
                    processValue(value, true);
                    unlock(singleAnimation);
                }
            }
        });
        try {// just to be shure that if the timer won't start 'cause of
            // an exception we do this construct, but i don't know if it is
            // neccesary
            lock(singleAnimation);
            timer.start();
        } catch (Throwable e) {
            unlock(singleAnimation);
            throw new RuntimeException(e);
        }
        return true;
    }

    private void lock(Object lock) {

        if (lock == null) return;
        synchronized (locks) {
            locks.put(lock, Boolean.TRUE);
        }
    }

    private void unlock(Object lock) {

        if (lock == null) return;
        synchronized (locks) {
            locks.remove(lock);
        }
    }

    private boolean isLocked(Object lock) {

        if (lock == null) return false;
        synchronized (locks) {
            return locks.containsKey(lock);
        }
    }

    /**
     * Called in the SwingThread, so it's secure to manipulate the gui, don't
     * use to many time here, since the Gui will be not responsive.
     *
     * @param value       current value of the iterator
     * @param isLastValue if this is the last value
     */
    protected abstract void processValue(Number value, boolean isLastValue);

}