package com.utd.stb.interlib;

import com.utd.stb.inter.action.NodeAction;
import com.utd.stb.inter.application.Node;


public class NodeActionImpl implements NodeAction {

    private String type;
    private Node target;

    public NodeActionImpl(Node target, String type) {

        this.target = target;
        this.type = type;
    }

    public Node getTargetNode() {

        return target;
    }

    public String getType() {

        return type;
    }
}