package com.utd.stb.gui.wizzard.choosers;

import com.jidesoft.grid.CellRendererManager;
import com.utd.alpha.extern.sun.treetable.AbstractTreeTableModel;
import com.utd.alpha.extern.sun.treetable.JTreeTable;
import com.utd.alpha.extern.sun.treetable.TreeTableCellRenderer;
import com.utd.alpha.extern.sun.treetable.TreeTableModel;
import com.utd.gui.action.ActionUtil;
import com.utd.gui.controls.table.ListViewTools;
import com.utd.gui.controls.table.ListViewTreeTable;
import com.utd.gui.controls.table.TableTools;
import com.utd.gui.controls.table.TreeTableTools;
import com.utd.gui.controls.table.model.AbstractListTableModel;
import com.utd.gui.controls.table.model.MovableRowsTableModel;
import com.utd.gui.controls.table.model.StayInOrderListTableModel;
import com.utd.gui.event.RepaintScrollerOnSelectionChange;
import com.utd.gui.event.TableDblClickAdapter;
import com.utd.stb.gui.itemTools.*;
import com.utd.stb.gui.swingApp.exceptions.ExceptionHandler;
import com.utd.stb.inter.application.BackendException;
import com.utd.stb.inter.items.DisplayableItem;
import com.utd.stb.inter.items.DisplayableItems;
import com.utd.stb.inter.wizzard.formtypes.FormDataListOutOfCategoriesList;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import resources.IconSource;

import javax.swing.*;
import javax.swing.event.ListSelectionEvent;
import javax.swing.event.ListSelectionListener;
import javax.swing.event.TableModelEvent;
import javax.swing.event.TableModelListener;
import javax.swing.table.TableCellRenderer;
import javax.swing.tree.TreePath;
import java.awt.*;
import java.awt.event.MouseEvent;
import java.util.*;
import java.util.List;


/**
 * @author PeterBuettner.de
 */
class TwoTreeTableChooser extends TwoComponentChooser {
    public static final Logger log = LogManager.getRootLogger();

    private SourceModel sourceModel;
    private DestinationModel destModel;
    private Categories categories;
    private JTreeTable ttSrc;
    private JTreeTable ttDst;
    private FormDataListOutOfCategoriesList data;
    private JComponent content;

    JTextField leftFilter;
    JTextField rightFilter;

//    QuickTreeFilterField leftFilter;
//    QuickTreeFilterField rightFilter;

    public TwoTreeTableChooser(ExceptionHandler exceptionHandler,
                               FormDataListOutOfCategoriesList data) {

        super(exceptionHandler);
        this.data = data;
    }

    public void saveData() throws BackendException {
        data.setSelectedItems(new DisplayableItemsWrap(getChoosen()));
        data.saveData();
    }

    public boolean isValid() {

        return true;
    }

    public JComponent getContent() {

        if (content != null) return content;

        createActions();

        categories = new Categories(data.getCategoryList());
        sourceModel = new SourceModel(categories);
        destModel = new DestinationModel(categories);

        DisplayableItemTableCellRenderer renderer = new DisplayableItemTableCellRenderer();
        ListSelectionListener lslActionManager = new ListSelectionListener() {

            public void valueChanged(ListSelectionEvent e) {

                //			if(e.getValueIsAdjusting()) {
                //				disableAllActions();// we know nothing!
                //				return;
                //				}

                // without 'invokeLater' the repeating up/down buttons won't
                // work,
                // probably the ButtonModel statechange series is broken, button
                // disabled in between
                SwingUtilities.invokeLater(new Runnable() {

                    public void run() {

                        manageActionEnabeling();
                    }
                });
            }
        };

        // ... build treetables:
        ttSrc = createTreeTable(sourceModel, renderer, true, lslActionManager);
        ttDst = createTreeTable(destModel, renderer, false, lslActionManager);

//        leftFilter = new QuickTreeFilterField(sourceModel);
//        ttSrc.getTree().setModel(leftFilter.getDisplayTreeModel());
//        leftFilter.setTree(ttSrc.getTree());
//
//        rightFilter = new QuickTreeFilterField(destModel);
//        ttDst.getTree().setModel(rightFilter.getDisplayTreeModel());
//        rightFilter.setTree(ttDst.getTree());

        ttSrc.getInputMap().put(KeyStroke.getKeyStroke("SPACE"), "acToRightOne");
        ttSrc.getActionMap().put("acToRightOne", acToRightOne);
        ttDst.getInputMap().put(KeyStroke.getKeyStroke("SPACE"), "acToLeftOne");
        ttDst.getActionMap().put("acToLeftOne", acToLeftOne);

        cmpLeft = ttSrc;
        cmpRight = ttDst;
        content = getComponentPanel(data.getLabelAllItems(), data.getLabelChoosenItems());

        ActionUtil.putIntoActionMap(content, new Action[]{acToLeftAll, acToLeftOne,
                        acToRightOne, acToRightAll, acMoveUp, acMoveDown,},
                JComponent.WHEN_ANCESTOR_OF_FOCUSED_COMPONENT // fire
                // when
                // focus
                // in this content
                //	JComponent.WHEN_IN_FOCUSED_WINDOW // fire always, but Navlist
                // eats Ctrl-A
        );

        expandAllVisibleRows(ttDst);// before col sizing! show all choosen,
        // but
        // not whole source since we do lazy loading

        // MCD, 29.Jun 2010, expand all deselected rows
        expandAllVisibleRows(ttSrc);

        TableTools.autoSizeColumns(ttSrc, false);
        TableTools.autoSizeColumns(ttDst, false);
        TableTools.syncColumnSizes(ttSrc, ttDst);

        manageActionEnabeling();

        return content;
    }// ----------------------------------------------------------------------

    protected JComponent createLeftBottomRow() {

        rightFilter = new JTextField("");
        return createFilterBar(ttSrc, rightFilter);
        //return leftFilter;
    }

    protected JComponent createRightBottomRow() {

        leftFilter = new JTextField("");
        return createFilterBar(ttDst, leftFilter);
        //  return rightFilter;
    }

    /**
     * expand all currently int the treeTable visible rows, not recursively
     *
     * @param treeTable
     */
    private void expandAllVisibleRows(JTreeTable treeTable) {

        for (int i = treeTable.getRowCount() - 1; i >= 0; i--)
            // start at end!
            treeTable.expandRow(i);
    }

    /**
     * create and configure one treeTable (src or dest)
     *
     * @param model            the data source
     * @param objectRenderer   default renderer for Object.class columns
     * @param isSrc            if this treeTable is the source one, where 'all' available
     *                         items, needed to decide what to do on doubleClicks
     * @param lslActionManager add this ListSelectionListener
     * @return
     */
    private JTreeTable createTreeTable(CategoryModel model, TableCellRenderer objectRenderer,
                                       final boolean isSrc,
                                       ListSelectionListener lslActionManager) {

        JTreeTable tt = new ListViewTreeTable(model) {

            {
                setProcessCtrlA(true);
            }
        };
        tt.setBackground(new Color(UIManager.getColor("Table.background").getRGB()));
        tt.getTableHeader().setBackground(new Color(UIManager.getColor("TableHeader.background").getRGB()));
        tt.setSelectionMode(ListSelectionModel.MULTIPLE_INTERVAL_SELECTION);
        //	tt.setSelectionMode(ListSelectionModel.SINGLE_SELECTION);
        tt.setShowsRootHandles(true);
        tt.setRootVisible(false);

        tt.setTreeTableCellRenderer(new TCR(tt));
        tt.setDefaultRenderer(Object.class, objectRenderer);
        CellRendererManager.registerRenderer(Object.class, objectRenderer);

        TableTools.selectFirstRow(tt, false);

        // move only clicked, not all selected, since ctrl-dblclick maybe
        // confusing
        tt.addMouseListener(new TreeTableDblClickAdapter() {

            public void rowDblClick(JTreeTable treeTable, int row, int hitCode) {

                chooseSelectedRows(isSrc, row, true);
            }
        });

        // markers for selection:
        tt
                .putClientProperty(
                        "com.utd.gui.lookAndFeel.FlatWindowsScrollBarUI.PAINT_SELECTION_MARKER",
                        "V");
        tt.getSelectionModel()
                .addListSelectionListener(new RepaintScrollerOnSelectionChange(tt, false));

        tt.getSelectionModel().addListSelectionListener(lslActionManager);
        return tt;
    }

    /**
     * dis- or enable Actions, depending on the selectionstate of the lists
     */
    private void manageActionEnabeling() {

        acToRightOne.setEnabled(ttSrc.getSelectedRow() != -1);
        acToRightAll.setEnabled(canChooseAll(true));

        acToLeftOne.setEnabled(ttDst.getSelectedRow() != -1);
        acToLeftAll.setEnabled(canChooseAll(false));

        acMoveUp.setEnabled(canMoveSelectedRowsUpDown(true));
        acMoveDown.setEnabled(canMoveSelectedRowsUpDown(false));
    }

    /**
     * disable all Actions
     */
    private void disableAllActions() {

        acToRightOne.setEnabled(false);
        acToRightAll.setEnabled(false);
        acToLeftOne.setEnabled(false);
        acToLeftAll.setEnabled(false);
        acMoveUp.setEnabled(false);
        acMoveDown.setEnabled(false);
    }

    protected void onMoveUp() {

        moveSelectedRowsUpDown(true);
    }

    protected void onMoveDown() {

        moveSelectedRowsUpDown(false);
    }

    protected void onChooseSelected() {
        int selRows = ttSrc.getSelectionModel().getMaxSelectionIndex() - ttSrc.getSelectionModel().getMinSelectionIndex();
        chooseSelectedRows(true, ttSrc.getSelectedRow(), selRows < 1);
    }

    protected void onUnchooseSelected() {
        int selRows = ttDst.getSelectionModel().getMaxSelectionIndex() - ttDst.getSelectionModel().getMinSelectionIndex();
        chooseSelectedRows(false, ttDst.getSelectedRow(), selRows < 1);
    }

    protected void onChooseAll() {

        chooseAllRows(true);
    }

    protected void onUnchooseAll() {

        chooseAllRows(false);
    }

    // ******************************************************************************
    // ******************************************************************************
    // Start of data changing code
    // ******************************************************************************
    // ******************************************************************************

    private boolean canMoveSelectedRowsUpDown(boolean up) {

        List sel = getSelectedPathsNoCategories(ttDst);
        SelectionHelper sh = new SelectionHelper(sel);
        for (Iterator i = sh.categoriesIterator(); i.hasNext(); ) {
            Category cat = (Category) i.next();
            List childs = sh.getChildsAsItems(cat);
            if (childs == null) continue; // no sel in this cat
            if (childs.isEmpty()) continue;// whole cat was selected
            if (cat.canDestMoveUpDown(childs, up)) return true;// at least
            // this works
        }
        return false;
    }

    private void moveSelectedRowsUpDown(boolean up) {

        final List sel = getSelectedPathsNoCategories(ttDst);
        SelectionHelper sh = new SelectionHelper(sel);
        for (Iterator i = sh.categoriesIterator(); i.hasNext(); ) {
            Category cat = (Category) i.next();
            List childs = sh.getChildsAsItems(cat);
            if (childs == null) continue; // no sel in this cat
            if (childs.isEmpty()) continue;// whole cat was selected
            cat.destMoveUpDown(childs, up);
        }
        // categories are not selected anymore afterwards
        SwingUtilities.invokeLater(new Runnable() {

            public void run() {
                TableTools
                        .setSelectionByIndices(ttDst, TreeTableTools
                                .pathToRows(ttDst, sel, true), true);
            }
        });
    }

    /**
     * input a list of <code>TreePath</code>s, you can ask for some subLists
     * and other info about the selection
     */
    private static class SelectionHelper {

        /*
         * we use a List for each category. If the List is empty the category
         * was selected (user means 'all items'), else only some items of that
         * category were meant. items in the Lists are saved as TreePaths in the
         * order they are in the input List
         */
        /**
         * keys are Category, Value is List of TreePath
         */
        private Map childs = new HashMap();

        /**
         * all Category as path, sorted
         */
        private LinkedHashSet catPaths = new LinkedHashSet();

        /**
         * @param selPaths list of <code>TreePath</code> s
         */
        public SelectionHelper(List selPaths) {
            //... collect childs
            for (Object selPath : selPaths) {
                TreePath path = (TreePath) selPath;
                if (path.getLastPathComponent() instanceof Category)
                    createList(path);
                else
                    createList(path.getParentPath()).add(path);
            }
        }

        /**
         * creates the list if not already created
         */
        private List createList(TreePath categoryPath) {
            catPaths.add(categoryPath);
            Object category = categoryPath.getLastPathComponent();
            List list = (List) childs.get(category);
            if (list == null) {
                list = new ArrayList();
                childs.put(category, list);
            }
            return list;
        }

        /**
         * all categories in the input order
         */
        public Iterator categoriesIterator() {

            final Iterator i = categoriesAsTreePathsIterator();
            return new Iterator() {

                public void remove() {

                    throw new UnsupportedOperationException();
                }

                public boolean hasNext() {

                    return i.hasNext();
                }

                public Object next() {

                    return ((TreePath) i.next()).getLastPathComponent();
                }
            };
        }

        /**
         * all categories path in the input order
         */
        public Iterator categoriesAsTreePathsIterator() {

            return catPaths.iterator();
        }

        /**
         * Childs of a category as <code>TreePath</code> s in the selection in
         * original order
         *
         * @param category
         * @return null: category had no selection <br>
         * empty List if category was selected <br>
         * List with childs if at least one child was selected <br>
         * <b>Note: you can't distinct for now if a category and one of
         * its childs were selected </b>
         */
        public List getChildsAsTreePaths(Category category) {

            return (List) childs.get(category);
        }

        /**
         * Items (the <code>TreePath</code> s last component), see
         * {@link Category getChildTreePaths}
         *
         * @param category
         * @return see {@link Category getChildTreePaths}, but here you get
         * the items, not the <code>TreePath</code> s
         */
        public List getChildsAsItems(Category category) {

            List c = (List) childs.get(category);
            if (c == null) return null;
            List items = new ArrayList(c.size());
            for (Object aC : c) {
                items.add(((TreePath) aC).getLastPathComponent());
            }
            return items;
        }

        /**
         * get the last selected item in the category, <br>
         * return null if category wasn't in the selection list or no item in
         * the category was selected.
         *
         * @param category
         * @return item or null
         */
        public Object getLastSelectionItem(Category category) {

            List c = (List) childs.get(category);
            if (c == null || c.size() == 0) return null;
            return ((TreePath) c.get(c.size() - 1)).getLastPathComponent();
        }

    }// --- end of SelectionHelper

    // --------------------------------------------------------------

    private void chooseAllRows(boolean chooseItems) {

        // version 1) old, works, but we need the moved paths to select them
        //	for(int i=0;i<categories.size();i++)
        //		categories.get(i).moveAllItems(chooseItems);

        // version 2) medium, had a flicker
        //	(chooseItems? ttSrc: ttDst).selectAll();

        //	 version 3) last one, select all categories, then move them
        List paths = createChildPaths(new TreePath(rootNode), categories.asList());
        TreeTableTools.setSelectedPaths((chooseItems ? ttSrc : ttDst), paths);

        chooseSelectedRows(chooseItems, -1, false);
        //	expandAllVisibleRows(chooseItems? ttDst: ttSrc);
        //	manageActionEnabeling();
    }

    /**
     * @param chooseItems choose or unchoose
     * @param selectedRow if -1 ignore and use selection in the table, else ignore
     *                    selection (used for row-double-click)
     */
    private void chooseSelectedRows(boolean chooseItems, final int selectedRow, final boolean isSingleRow) {
        final JTreeTable tSrc = chooseItems ? ttSrc : ttDst;
        final JTreeTable tDst = !chooseItems ? ttSrc : ttDst;

        // selected categories have no childs:
        final List toMove = (!isSingleRow)
                ? getSelectedPathsMayOmitChilds(tSrc)
                : Collections.singletonList(tSrc.getPathForRow(selectedRow));
        log.debug(toMove);
        if (toMove.isEmpty()) return;

        /*
         * if we move only one row (row-double-click) and want the selection in
         * source to be unmodified we save it here
         */
        //final List completeSelection = (!isSingleRow) ? null : TreeTableTools.getSelectedPaths(tSrc);

        /*
         * helper to save the places in the dest where to insert items, (the
         * selection in dest is changed on insertion)
         */
        SelectionHelper insertInfo = new SelectionHelper(getSelectedPathsNoCategories(tDst));

        final SelectionHelper selInfo = new SelectionHelper(toMove);

        // collect all moved items here, to select them afterwards
        final List<TreePath> movedPaths = new ArrayList<TreePath>();

        for (Iterator i = selInfo.categoriesIterator(); i.hasNext(); ) {
            Category cat = (Category) i.next();
            TreePath catPath = new TreePath(new Object[]{rootNode, cat});

            List childs = selInfo.getChildsAsItems(cat);
            if (childs == null) continue; // no sel in this cat
            List moved;// the moved items
            log.debug(childs.isEmpty());
            if (childs.isEmpty()) {// whole cat was selected
                moved = cat.moveAllItems(chooseItems, insertInfo.getLastSelectionItem(cat));
            } else {
                log.debug(childs + "; " + chooseItems + "; " + insertInfo.getLastSelectionItem(cat));
                cat.moveItems(childs, chooseItems, insertInfo.getLastSelectionItem(cat)); // only
                // single
                // items
                // were
                // selected
                moved = childs;
            }
            movedPaths.addAll(createChildPaths(catPath, moved));
        }

        // do later: change selections
        SwingUtilities.invokeLater(new Runnable() {

            public void run() {

                // first expand
                for (Iterator i = selInfo.categoriesAsTreePathsIterator(); i.hasNext(); ) {
                    TreePath treePath = (TreePath) i.next();
                    tDst.expandPath(treePath);
                }

                // now select
                //		TableTools.setSelectionByIndices(tDst,TreeTableTools.pathToRows(tDst,toMove,
                // true),true);
                TableTools.setSelectionByIndices(tDst, TreeTableTools.pathToRows(tDst,
                                movedPaths,
                                true),
                        true);

                // scroll to view first
                TableTools.scrollRowToVisible(tDst, tDst.getRowForPath((TreePath) toMove
                        .get(0)));

                /*
                 * handle source selection:
                 * 
                 * if only single selection and it is the last in a category
                 * don't go into the next category, select the previous item.
                 * 
                 * May do later(NO!): if was single selection and it was a
                 * category then go to the next category ... NO,NO,NO! if the
                 * cat goes empty, cat is selected, and we set the sel to the
                 * next cat, all cats are moved to fast
                 */

                ListSelectionModel selModel = tSrc.getSelectionModel();

                int row = selectedRow;

                if (!isSingleRow) {
                    //			row = selModel.getLeadSelectionIndex();
                    // row = selModel.getAnchorSelectionIndex();

                    if (row == -1 || row > tSrc.getRowCount() - 1)
                        row = tSrc.getRowCount() - 1;
                }
//                else {
//                    row = selectedRow;
//                }

                if (toMove.size() == 1) {
                    TreePath singleSel = (TreePath) toMove.get(0);
                    //			if(isCategory(singleSel)) { row++; } else{ ... // select
                    // next cat
                    TreePath newSelPath = tSrc.getPathForRow(row);
                    if (!hasSameParent(singleSel, newSelPath) && !isCategory(singleSel))
                        row--;
                }

                if (row >= tSrc.getRowCount()) row = tSrc.getRowCount() - 1;

                selModel.setSelectionInterval(row, row);
                TableTools.scrollRowToVisible(tSrc, row);

                // log.debug(row);
            }
        });
    }

    /**
     * collects selected paths, but may omit some childs: <br>
     * child is ignored when its category is selected too. <br>
     * use to un-/choose
     *
     * @param treeTable
     * @return list of TreePath, never null
     */
    private static List getSelectedPathsMayOmitChilds(JTreeTable treeTable) {

        int[] rows = treeTable.getSelectedRows();
        List<TreePath> l = new ArrayList<TreePath>(rows.length);

        // we know that a parent arrives in a row-index before child, no other
        // parent in between
        TreePath lastCategory = null;
        for (int row : rows) {
            TreePath path = treeTable.getPathForRow(row);
            if (isCategory(path))
                lastCategory = path;
            else if (lastCategory != null && lastCategory.equals(path.getParentPath()))
                continue;
            l.add(path);
        }
        return l;
    }

    /**
     * collects selected paths, but categories are omitted. use for moving rows
     * in dest.
     *
     * @param treeTable
     * @return list of TreePath, never null
     */
    private static List getSelectedPathsNoCategories(JTreeTable treeTable) {

        int[] rows = treeTable.getSelectedRows();
        List<TreePath> l = new ArrayList<TreePath>(rows.length);
        for (int row : rows) {
            TreePath path = treeTable.getPathForRow(row);
            //log.debug(path.getLastPathComponent()+"; "+isCategory(path));
            // sorry: when last row is sel and one does un/choose all, an
            // illegal index may be seen
            if (path != null && !isCategory(path)) l.add(path);
        }
        return l;
    }

    private static boolean isCategory(TreePath path) {
        return path.getLastPathComponent() instanceof Category;
    }

    /**
     * false if one is null or one parent-path is null
     */
    private static boolean hasSameParent(TreePath p1, TreePath p2) {

        if (p1 != null && p2 != null) {
            if (!isCategory(p1)) p1 = p1.getParentPath();
            if (!isCategory(p2)) p2 = p2.getParentPath();
            if (p1 != null && p2 != null) return p1.equals(p2);
        }
        return false;
    }

    /**
     * for every child in childs it creates a new TreePath by appending the
     * child to basePath, builds a list and returns it
     *
     * @param basePath
     * @param childs
     * @return a list of TreePath
     */
    private static List<TreePath> createChildPaths(TreePath basePath, List childs) {

        List<TreePath> paths = new ArrayList<TreePath>(childs.size());
        for (Object child : childs) paths.add(basePath.pathByAddingChild(child));
        return paths;
    }

    /**
     * @param choose or unchoose
     * @return
     */
    private boolean canChooseAll(boolean choose) {

        // here at least the first category is loaded
        for (int i = 0; i < categories.size(); i++) {
            Category cat = categories.get(i);
            if (choose) {
                if (cat.getSourceItems().getRowCount() > 0) return true;
            } else {
                if (cat.getDestItems().getRowCount() > 0) return true;
            }
        }
        return false;
    }

    /**
     * get all choosen items, in the current order, not the categories but only
     * the items
     *
     * @return
     */
    private List getChoosen() {

        List list = new ArrayList();
        for (int i = 0; i < categories.size(); i++) {
            list.addAll(categories.get(i).getDestItems().getRows());
        }
        return list;
    }

    /* (non-Javadoc)
      * @see com.utd.stb.gui.wizzard.choosers.AbstractChooser#getFilterFieldValue()
      */
    public String getFilterFieldValue() {

        if (leftFilter.getText() == null && rightFilter.getText() == null) return null;
        if (rightFilter.getText() == null ||
                rightFilter.getText().length() == 0) return leftFilter.getText();
        if (leftFilter.getText() != null ||
                leftFilter.getText().length() == 0) return rightFilter.getText();
        return null;
    }

    /* (non-Javadoc)
      * @see com.utd.stb.gui.wizzard.choosers.AbstractChooser#setFilterFieldValue()
      */
    public void setFilterFieldValue(String regExpr) {
        leftFilter.setText(regExpr);
        rightFilter.setText(regExpr);
    }

    //******************************************************************************
    //  End of data changing code
    //******************************************************************************

    //######################################################################
    //######################################################################
    //######################################################################

    /**
     * copying wrapper for easy access
     */
    private class Categories {

        private final List list;
        DisplayableItems cats;

        public Categories(DisplayableItems cats) {

            this.cats = cats;

            list = new ArrayList(cats.getSize());
            for (int i = 0; i < cats.getSize(); i++)
                list.add(new Category(cats.get(i), i));
        }

        public int size() {

            return list.size();
        }

        public Category get(int i) {

            return (Category) list.get(i);
        }

        // MCD, 26.Jan 2006
        public DisplayableItems getAll() {
            return cats;
        }

        /**
         * all categories as a unmodifiable list
         */
        public List asList() {

            return Collections.unmodifiableList(list);
        }
    }//	 --- Categories --------------------------

    /**
     * wrap items to have an index/childs and access childs
     */
    private class Category implements DisplayableItem, Comparable {

        private DisplayableItem base;      // display info

        private int index;     // my position, index into
        // data

        private AbstractListTableModel src;       // lazy loading;
        // StayInOrderListTableModel

        private MovableRowsTableModel dest;      // lazy loading

        private TableModelListener listener = new TableModelListener() {

            public void tableChanged(TableModelEvent e) {

                if (e.getSource() == src) sourceModel.changed(Category.this);
                else if (e.getSource() == dest) destModel.changed(Category.this);
            }
        };

        Category(DisplayableItem base, int index) {

            this.base = base;
            this.index = index;
        }

        public Object getData() {

            return base.getData();
        }

        public String getInfo() {

            return base.getInfo();
        }

        public int getIndex() {

            return index;
        }

        public boolean equals(Object o) {

            return (o instanceof Category) && base.equals(((Category) o).base);
        }

        public int compareTo(Object o) {

            return index - ((Category) o).index;
        }

        public int hashCode() {

            return index;
        }

        private MovableRowsTableModel getDestItems() {

            if (dest == null) {
                List l = DisplayableItemsConv.asList(data.getSelectedItems(index));
                dest = new MovableRowsTableModel(l) {

                    public int getColumnCount() {

                        return 0;
                    }

                    public Object getValueAt(int row, int columnIndex) {

                        return getRow(row);
                    }
                };
                dest.addTableModelListener(listener);
            }
            return dest;
        }

        /**
         * returns true if (src not loaded) or (loaded and src has childs)
         */
        private boolean getLazySourceHasChildren() {

            return src == null || src.getRowCount() > 0;
        }

        private AbstractListTableModel getSourceItems() {

            if (src == null) {
                List l = DisplayableItemsConv.asList(data.getCategoryItems(index));
                src = new StayInOrderListTableModel(l) {

                    public int getColumnCount() {

                        return 0;
                    }

                    public Object getValueAt(int row, int columnIndex) {

                        return getRow(row);
                    }
                };
                // remove the already selected ones
                src.removeRows(DisplayableItemsConv.asList(data.getSelectedItems(index)));
                src.addTableModelListener(listener);
            }
            return src;
        }

        /**
         * un/choose all items in this Category, inserted after the given
         * object, but note that unchoosen items are sorted at their natural
         * indices, insertAfter is ignored then
         *
         * @param choose
         * @param insertAfter if!=null then insert the items after this, if null or not
         *                    found also insert at end
         * @return the moved nodes
         */
        public List moveAllItems(boolean choose, Object insertAfter) {

            AbstractListTableModel src = choose ? getSourceItems() : getDestItems();
            AbstractListTableModel dest = !choose ? getSourceItems() : getDestItems();
            List items = src.getRows();
            src.removeAllRows();
            if (choose)
                dest.addRows(getInsertIndex(dest, insertAfter), items);
            else
                dest.addRows(items);

            return items;
        }

        /**
         * un/choose the given items in this Category, IllegalArgumentException
         * if one of the items is not in this category <br>
         * They are inserted after the given object, but note that unchoosen
         * items are sorted at their natural indices, insertAfter is ignored
         * then
         *
         * @param choose
         * @param insertAfter if!=null then insert the items after this, if null or not
         *                    found also insert at end
         * @throws IllegalArgumentException if one of the items is not in this category
         */
        public void moveItems(List items, boolean choose, Object insertAfter) {

            AbstractListTableModel src = choose ? getSourceItems() : getDestItems();
            AbstractListTableModel dest = !choose ? getSourceItems() : getDestItems();
            src.removeRows(items);
            if (choose) {
                dest.addRows(getInsertIndex(dest, insertAfter), items);
            } else
                dest.addRows(items);
        }

        /**
         * map Object to index where to insert new items,
         *
         * @param dest
         * @param insertAfter if null or not found -&gt; at end
         * @return always a valid index
         */
        private int getInsertIndex(AbstractListTableModel dest, Object insertAfter) {

            int i = insertAfter == null ? -1 : dest.findRow(insertAfter);
            if (i == -1) return dest.getRowCount(); // null or not found -> at
            // end
            return i + 1;
        }

        /**
         * test if items can be moved one up
         *
         * @param items only those that are childs of this category or
         *              IllegalArgumentException
         * @param up    toward index zero
         * @throws IllegalArgumentException
         */
        public boolean canDestMoveUpDown(List items, boolean up) {

            return getDestItems().canMove(getDestItems().findRows(items), up);
        }

        /**
         * moves all items one up, if no one can be moved does nothing
         *
         * @param items only those that are childs of this category or
         *              IllegalArgumentException
         * @param up    toward index zero
         * @throws IllegalArgumentException
         */
        public void destMoveUpDown(List items, boolean up) throws IllegalArgumentException {

            int[] rows = getDestItems().findRows(items);
            if (up)
                getDestItems().moveUp(rows);
            else
                getDestItems().moveDown(rows);
        }

        /* (non-Javadoc)
         * @see com.utd.stb.inter.items.DisplayableItem#getAttributeList()
         */
        public Object getAttributeList() {

            return base.getAttributeList();
        }

        @Override
        public String toString() {
            return getInfo();
        }
    }

    private final static Object rootNode = "Root";

    private class SourceModel extends CategoryModel {

        public SourceModel(Categories categories) {

            super(categories);
        }

        public boolean isLeaf(Object node) {// we do lazy load
            if (node == rootNode) return false;
            if (!(node instanceof Category)) return true;
            // MCD 29.Nov 2005 auskommentiert und durch untere Zeile ersetzt
            // return !( (Category)node ).getLazySourceHasChildren();
            return getChildCount(node) == 0;
        }

        AbstractListTableModel getChilds(Object parent) {
            //         log.debug(parent);
            if (parent instanceof Category) return ((Category) parent).getSourceItems();
            return null;
        }
    }

    private class DestinationModel extends CategoryModel {

        public DestinationModel(Categories categories) {

            super(categories);
        }

        public boolean isLeaf(Object node) {// we don't lazy load here
            return getChildCount(node) == 0;
        }

        AbstractListTableModel getChilds(Object parent) {
            //log.debug(parent);
            if (parent instanceof Category) return ((Category) parent).getDestItems();
            return null;
        }
    }

    private abstract class CategoryModel extends AbstractTreeTableModel {

        // we know: only DisplayableItems herein
        protected Categories categories; // wrapping
        protected DisplayableItemsTableModelMaker modelMaker;

        public CategoryModel(Categories categories) {

            super(rootNode);
            this.categories = categories;
            this.modelMaker = new DisplayableItemsTableModelMaker(categories.getAll());
        }

        public boolean isLeaf(Object node) {
            //log.debug(node);
            return node != rootNode && !(node instanceof Category);
        }

        public Class getColumnClass(int column) {

            if (column == 0) return TreeTableModel.class;
            return String.class;
        }

        public int getColumnCount() {

            return modelMaker.getColumnCount();
        }

        public String getColumnName(int col) {

            return modelMaker.getColumnName(col);
        }

        public Object getValueAt(Object node, int col) {
            // System.out.println("CategoryModel getValueAt = " + node);
            return modelMaker.getValueAt(node, col);
        }

        public int getChildCount(Object parent) {

            if (parent == rootNode) return categories.size();
            AbstractListTableModel di = getChilds(parent);
            return di == null ? 0 : di.getRowCount();
        }

        public Object getChild(Object parent, int index) {

            if (parent == rootNode) return categories.get(index);
            AbstractListTableModel di = getChilds(parent);
            return di == null ? null : di.getRow(index);
        }

        abstract AbstractListTableModel getChilds(Object parent);

        protected void changed(Category category) {

            fireTreeStructureChanged(this, new Object[]{rootNode, category}, new int[0],
                    null);
        }
    }

    // ######################################################################
    // ######################################################################

    // ********************** Gui Helpers ************************************

    private static class TCR extends TreeTableCellRenderer {

        private Icon emptyFolder;

        private DisplayableItemToolTipMaker ttm = new DisplayableItemToolTipMaker();

        public TCR(JTreeTable treeTable) {

            super(treeTable);
            IconSource is = new IconSource();
            setLeafIcon(null);
            setClosedIcon(is.getSmallIconByID("folder"));
            setOpenIcon(is.getSmallIconByID("folderOpen"));
            emptyFolder = is.getSmallIconByID("folderGhost");
        }

        public Component getTableCellRendererComponent(JTable table, Object value,
                                                       boolean isSelected, boolean hasFocus,
                                                       int row, int column) {

            ttm.extractData(table.getModel(), value, row);
            super.getTableCellRendererComponent(table,
                    value,
                    isSelected,
                    hasFocus,
                    row,
                    column);

            if (getTreePath().getLastPathComponent() instanceof Category)
                setLabelIcon(isExpanded() ? getOpenIcon() :
                        (isLeaf() ? emptyFolder : getClosedIcon()));

            ListViewTools.changeDefaultTableCellRenderer(table, this, row, column);
            return this;
        }

        //	 use this since setToolTipText() calls getToolTipText -> lot of unused
        // junk
        public String getToolTipText() {

            return ttm.getToolTipText();
        }
    }

    /**
     * fires only if no special area is hit
     */
    private static abstract class TreeTableDblClickAdapter extends TableDblClickAdapter {

        /**
         * fired only if all is valid, row is never -1., only clicks on Text and
         * left
         *
         * @param treeTable
         * @param row
         * @param hitCode
         */
        abstract public void rowDblClick(JTreeTable treeTable, int row, int hitCode);

        public final void rowDblClick(JTable table, int row, MouseEvent e) {

            JTreeTable tt = (JTreeTable) table;
            int hitCode = tt.getHitCode(e.getX(), row);
            if (TreeTableCellRenderer.HIT_HANDLE == hitCode
                    || TreeTableCellRenderer.HIT_ICON == hitCode) return;
            rowDblClick(tt, row, hitCode);
        }
    }
}