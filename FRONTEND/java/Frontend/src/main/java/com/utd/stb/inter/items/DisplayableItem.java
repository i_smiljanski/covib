package com.utd.stb.inter.items;

/**
 * Items to be choosen, have <code>data</code> (mostly String, but also
 * Integer, Boolean,...) and <code>info</code> to be shown, to be used in
 * lists and tables, simple.
 *
 * @author PeterBuettner.de
 */
public interface DisplayableItem {

    /**
     * The 'Name' of the item, mostly String but also Integer, Boolean etc.
     * String/Integer/Boolean/... not only String
     *
     * @return
     */
    Object getData();

    /**
     * additional info, displayed in table or as tooltip
     *
     * @return
     */
    String getInfo();

    /**
     * attributes describing the look of the item
     *
     * @return
     */
    Object getAttributeList();

    default public String getMasterKey() {
        return null;
    }
}