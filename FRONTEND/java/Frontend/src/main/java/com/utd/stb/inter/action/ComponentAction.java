package com.utd.stb.inter.action;

/**
 * ???
 *
 * @author Marie-Christin Dort
 */
public interface ComponentAction {

    /**
     * change a component or anything else
     */
    public static final Object CHECK_MASK = "CheckMask";
    public static final Object CHANGE_MASK = "ChangeMask";

    /**
     * What to do:
     * <p>
     * <pre>
     *
     *  ComponentAction.CHECK_MASK
     *
     * </pre>
     *
     * @return
     */
    Object getType();

}