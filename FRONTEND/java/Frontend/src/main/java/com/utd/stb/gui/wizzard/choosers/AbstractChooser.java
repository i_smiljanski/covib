package com.utd.stb.gui.wizzard.choosers;

import com.utd.gui.action.ActionUtil;
import com.utd.gui.action.ActionWrap;
import com.utd.stb.gui.swingApp.exceptions.ExceptionHandler;
import com.utd.stb.inter.application.BackendException;
import resources.Res;

import javax.swing.*;
import javax.swing.event.ChangeEvent;
import javax.swing.event.ChangeListener;
import javax.swing.event.EventListenerList;
import java.awt.event.ActionEvent;


/**
 * Basic class for all choosers, with common functions
 *
 * @author PeterBuettner.de
 */

public abstract class AbstractChooser {

    protected EventListenerList listenerList = new EventListenerList();
    protected ExceptionHandler exceptionHandler;

    public AbstractChooser(ExceptionHandler exceptionHandler) {

        this.exceptionHandler = exceptionHandler;
    }

    /**
     * returns a JComponent to be included in a JForm/JDialog/JApplet displaying
     * and let the user edit one entity from an implementation of FormData
     *
     * @return
     */
    public abstract JComponent getContent();

    /**
     * Call this to let the chooser save its data.
     *
     * @throws BackendException
     */
    public abstract void saveData() throws BackendException;

    /**
     * Call this to ask the if the data is valid.
     *
     * @return
     */
    abstract public boolean isValid();

    /**
     * Call this to get the filter field value
     *
     * @return
     */
    abstract public String getFilterFieldValue();


    /**
     * Call this to set the filter field value
     *
     * @return
     */
    abstract public void setFilterFieldValue(String regExpr);


    private ChangeEvent changeEvent;

    /**
     * Adds a <code>ChangeListener</code>
     *
     * @param listener the listener to be added
     */
    public void addChangeListener(ChangeListener listener) {

        listenerList.add(ChangeListener.class, listener);
    }

    /**
     * Removes a <code>ChangeListener</code>
     *
     * @param listener the listener to be removed
     */
    public void removeChangeListener(ChangeListener listener) {

        listenerList.remove(ChangeListener.class, listener);
    }

    /**
     * Notifies all listeners that have registered interest for notification on
     * this event type. The event instance is lazily created.
     */
    protected void fireValidationChanged() {

        Object[] listeners = listenerList.getListenerList();
        for (int i = listeners.length - 2; i >= 0; i -= 2) {
            if (listeners[i] == ChangeListener.class) {
                if (changeEvent == null) changeEvent = new ChangeEvent(this);
                ((ChangeListener) listeners[i + 1]).stateChanged(changeEvent);
            }
        }
    }

    /**
     * Creates a JLabel, binds it to the component, the Labels Tooltip is
     * created from 'some' mechanism out of the resources, showing the
     * 'shortcut' to set the focus to the compo, since there are 1,2,...(more
     * later?) components and the label-texts are from the backend, index will
     * 'count' the shortcuts, e.g. 'alt 1' for first/left component (index==0)
     * 'alt 2' for second, etc. The created action is put into the inputmap of
     * the compo with JComponent.WHEN_IN_FOCUSED_WINDOW
     *
     * @param text  the text of the label
     * @param compo the component to be focussed
     * @param index starting from 0
     * @return
     */
    static JLabel buildComponentLabel(String text, final JComponent compo, int index) {

        String tt = Res.getString("AbstractChooser.action.focusComponent.tooltip");
        Action a = new ActionWrap(null, null, '\0', tt, "alt " + (index + 1)) {

            public void actionPerformed(ActionEvent e) {

                if (compo.isShowing()) compo.requestFocusInWindow();
            }
        };
        JLabel label = new JLabel(text);
        label.setLabelFor(compo);
        label.setToolTipText((String) a.getValue(Action.SHORT_DESCRIPTION));// the
        // accelerator-key
        // text
        // is
        // build
        // in!

        ActionUtil.putIntoActionMap(compo, a, JComponent.WHEN_IN_FOCUSED_WINDOW);
        return label;
    }


}