package com.utd.stb.gui.wizzard.choosers;

import com.utd.stb.gui.swingApp.exceptions.ExceptionHandler;
import com.utd.stb.gui.userinput.swing.*;
import com.utd.stb.inter.application.BackendException;
import com.utd.stb.inter.userinput.UserInput;
import com.utd.stb.inter.wizzard.formtypes.FormDataUserInput;

import javax.swing.*;
import javax.swing.border.EmptyBorder;
import java.util.Iterator;


/**
 * Swing gui to let a user edit FormDataOneOutOfList.
 *
 * @author PeterBuettner.de
 */
class UserInputChooser extends AbstractChooser {

    private FormDataUserInput data;
    private UserInput[] userInputs;
    private boolean valid;
    private PanelDataChangeListener dataHandler;

    /**
     * only package access for now
     *
     * @param formDataOneOutOfList
     */
    UserInputChooser(ExceptionHandler exceptionHandler, FormDataUserInput formDataUserInput) {

        super(exceptionHandler);
        this.data = formDataUserInput;

        int n = data.getUserInputCount();
        userInputs = new UserInput[n];
        for (int i = 0; i < n; i++)
            userInputs[i] = data.getUserInput(i);
    }

    public void saveData() throws BackendException {

        dataHandler.commitData();
        data.saveData();
    }

    public boolean isValid() {

        return valid;
    }

    public JComponent getContent() {

        JComponent componentPanel = new UIOPanelFactory().createPanel(userInputs);
        componentPanel.setBorder(new EmptyBorder(0, 0, 0, 0));

        dataHandler = new PanelDataChangeListener(componentPanel);
        dataHandler.dataChanged(null);

        //	JPanel p = new JPanel(new BorderLayout()); // ugly
        //	p.add(componentPanel,"West");
        //	p.add(new JLabel("C"));
        //	return p;
        return componentPanel;
    }// ----------------------------------------------------------------------

    private class PanelDataChangeListener extends CollectiveDataHandler implements
            DataChangeListener {

        public PanelDataChangeListener(JComponent content) {

            super(content);
            collectComponents();
            for (Iterator i = handlerIterator(); i.hasNext(); )
                ((UIODataHandler) i.next()).addChangeListener(this);

        }

        protected void commitData() {

            super.commitData();
        } // make visible

        public void dataChanged(DataChangeEvent event) {

            boolean newValid = isValid() && !isEmpty();

            if (valid != newValid) { // eat irrelevant changes
                valid = newValid; // set first
                fireValidationChanged();// now fire
            }

        }

    }

    /* (non-Javadoc)
     * @see com.utd.stb.gui.wizzard.choosers.AbstractChooser#getFilterFieldValue()
     */
    public String getFilterFieldValue() {
        return null;
    }

    /* (non-Javadoc)
     * @see com.utd.stb.gui.wizzard.choosers.AbstractChooser#setFilterFieldValue()
     */
    public void setFilterFieldValue(String regExpr) {
    }

}