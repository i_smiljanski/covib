package com.utd.stb.back.data.client;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;


/**
 * Wraps a list of record data of type MessageRec
 *
 * @author Inna Smiljanski Created 06.01.2015
 * @see com.utd.stb.back.data.client.MessageRec
 */
public class MessageList {

    private ArrayList data = new ArrayList();
    private Iterator iterator;

    /**
     * Creates an empty MessageRec
     */
    public MessageList() {
    }

    /**
     * Creates an MessageRec with this data
     *
     * @param in the list data
     */
    public MessageList(ArrayList in) {
        data = in;
    }

    /**
     * Creates an MessageRec with this data
     *
     * @param list the list data
     */
    public MessageList(List list) {
        data.addAll(list);
    }

    /**
     * adds all fields of input list to this list
     *
     * @param in an MessageRec to add
     */
    public void addAll(MessageList in) {
        in.initIterator();
        while (in.hasNext()) {
            data.add(in.getNext());
        }
    }

    /**
     * adds this record to the list
     *
     * @param sr the MessageRec to add
     */
    public void add(MessageRec sr) {
        data.add(sr);
    }

    /**
     * @param index
     * @return the record at this index
     */
    public MessageRec get(int index) {
        return (MessageRec) data.get(index);
    }

    /**
     * Sets the field at index to this MessageRec
     *
     * @param index
     * @param sr
     */
    public void set(int index, MessageRec sr) {
        data.set(index, sr);
    }

    /**
     * sets list iterator to the beginning
     */
    public void initIterator() {
        iterator = data.iterator();
    }

    /**
     * @return true if there is a next record
     */
    public boolean hasNext() {
        return iterator.hasNext();
    }

    /**
     * @return the next MessageRec
     */
    public MessageRec getNext() {
        return (MessageRec) iterator.next();
    }

    /**
     * @return the size of this list
     */
    public int getSize() {
        return data.size();
    }

    /**
     * @param s Record to look up
     * @return the index of this record
     */
    public int indexOf(MessageRec s) {
        return data.indexOf(s);
    }

}