package com.utd.gui.controls.table;

import com.utd.alpha.extern.sun.treetable.JTreeTable;

import javax.swing.tree.TreePath;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;


/**
 * Helper special for JTreeTable
 *
 * @author PeterBuettner.de
 */
public class TreeTableTools {

    /**
     * collects all selected paths
     *
     * @param treeTable
     * @return never null
     */
    public static List getSelectedPaths(JTreeTable treeTable) {

        int[] rows = treeTable.getSelectedRows();

        List l = new ArrayList(rows.length);
        for (int i = 0; i < rows.length; i++) {
            TreePath path = treeTable.getPathForRow(rows[i]);
            if (path != null) l.add(path);
        }
        return l;

    }// -------------------------------------------------------------------------

    /**
     * sets the selection from the path in list
     *
     * @param treeTable
     * @param treePaths
     */
    public static void setSelectedPaths(JTreeTable treeTable, List treePaths) {

        TableTools.setSelectionByIndices(treeTable, pathToRows(treeTable, treePaths, true),
                true);

    }// -------------------------------------------------------------------------

    /**
     * map TreePath to rows, but note: treePath.get(i) maybe not mapped to
     * array[i], since missing paths are not found.
     * <p>
     * <b>skipMissing </b> <br>
     * then the direct mapping maybe wrong and the resulting array is smaller
     * than the input list. <br>
     * <b>not skipMissing </b> <br>
     * the missing ones are not skipped, theire row-indices are set to -1, the
     * mapping is always as expected: treePath.get(i)~ array[i]
     *
     * @param treeTable
     * @param treePaths
     * @param skipMissing if true skip missing, if false set rowIndex:=-1
     * @return if some paths not found (and depending from skipMissing ) the
     * indices are not in the result, it maybe smaller!
     */

    public static int[] pathToRows(JTreeTable treeTable, List treePaths, boolean skipMissing) {

        int[] rows = new int[treePaths.size()];
        int k = 0;
        for (Iterator i = treePaths.iterator(); i.hasNext(); ) {
            int row = treeTable.getRowForPath((TreePath) i.next());

            if (row == -1 && skipMissing) continue; // row==-1 -> path not
            // found
            rows[k++] = row;
        }
        if (k < treePaths.size()) {// some were not found
            int[] temp = new int[k];
            System.arraycopy(rows, 0, temp, 0, k);
            rows = temp;
        }
        return rows;

    }// -------------------------------------------------------------------------

}