package com.utd.stb.inter.application;

import com.jidesoft.status.ProgressStatusBarItem;

import javax.swing.*;

/**
 * Created by IntelliJ IDEA.
 * User: smiljanskii60
 * Date: 08.11.2010
 * Time: 10:17:14
 */

public class MainWindowInfo {
    private ProgressStatusBarItem statusBarProgress;
    private JTable table;
    private JLabel label;

    public ProgressStatusBarItem getStatusBarProgress() {
        return statusBarProgress;
    }

    public void setStatusBarProgress(ProgressStatusBarItem statusBarProgress) {
        this.statusBarProgress = statusBarProgress;
    }

    public JTable getTable() {
        return table;
    }

    public void setTable(JTable table) {
        this.table = table;
    }

    public JLabel getLabel() {
        return label;
    }

    public void setLabel(JLabel label) {
        this.label = label;
    }
}
