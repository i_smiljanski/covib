package com.utd.stb.back.ui.impl.msgboxes;

import com.utd.stb.inter.action.ButtonItem;
import com.utd.stb.interlib.BasicMessageBox;

/**
 * @author Inna Smiljanski  created 09.12.2010
 */

public class AlertMsgBox extends BasicMessageBox {
    int timeout;

    public AlertMsgBox(String title, String text, int timeout) {
        super(null, createHeaderBar(title, ICON_EXCLAMATION), null, text, new ButtonItem[]{bClose}, null, true);
        this.timeout = timeout;
    }


    public static void main(String[] arg) {
        com.jidesoft.utils.Lm.verifyLicense("up to data professional services", "iStudyReporter",
                "w6.tuJhnfnDInGp:fvqIJaaeKT4yKF42");

        AlertMsgBox msgBox = new AlertMsgBox("title", "1.text text text text text text text text " +
                "\r2.text text texttext text text text  text  " +
                "\r3.text text text text text text text text text text 4." +
                "\rtext ", 2000);
//        msgBox.initAlert(title, text, timeout);

    }

    public int getTimeout() {
        return timeout;
    }

    @Override
    protected Object onBoxClose(ButtonItem clickedButton) {
        return null;
    }

}
