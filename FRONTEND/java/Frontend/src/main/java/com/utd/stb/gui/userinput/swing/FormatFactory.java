package com.utd.stb.gui.userinput.swing;

import com.utd.stb.inter.userinput.DateInput;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import resources.Res;

import java.text.DateFormat;
import java.text.NumberFormat;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Locale;
import java.util.TimeZone;


/**
 * Central source for Date and Number Formats, TimeZone, Calendar ...
 *
 * @author PeterBuettner.de
 */
public class FormatFactory {
    public static final Logger log = LogManager.getRootLogger();

    private static Locale locale = Locale.getDefault();
    private static TimeZone timeZone = TimeZone.getDefault();
    // private static TimeZone timeZone = TimeZone.getTimeZone("Etc/GMT-14"); //
    // extrem value for testing
    private static TimeZone timeZoneGMT = TimeZone.getTimeZone("GMT");

    public static Calendar getCalendar() {

        return Calendar.getInstance(timeZone, locale);
    }

    public static Calendar getCalendarGMT() {

        return Calendar.getInstance(timeZoneGMT, locale);
    }

    static DateFormat getDateInputFormatter(DateInput dateInput) {

        return getDateInputFormatter(dateInput.getType());
    }

     static DateFormat getDateInputFormatter(String type) {

        // TODOlater cache formatter? but look for changed locale/timezone (or
        // make them unchangable)
        DateFormat dateFormat = getFormat(type);
        dateFormat.setTimeZone(timeZone);
        dateFormat.setLenient(false);
        return dateFormat;
    }

    /**
     * read from resources, create
     *
     * @param type DateInput.TYPE_...
     * @return always a usable DateFormat, on any error a default one is used
     */
    private static DateFormat getFormat(String type) {
        String resKey = null;
//        log.debug(type);
        switch (type) {
            case DateInput.TYPE_DATE:
                resKey = "dateformat.date";
                break;
            case DateInput.TYPE_TIME:
                resKey = "dateformat.time";
                break;
            case DateInput.TYPE_DATETIME:
                resKey = "dateformat.datetime";
                break;
        }

        try {
            // locale is for dateformatsymbols (day names etc)
            return new SimpleDateFormat(Res.getString(resKey), locale);
        } catch (Exception e) { // wrong pattern?
            // NullPointerException if the given pattern is null (res not found)
            // IllegalArgumentException if the given pattern is invalid
            // so if nothing helps:
            return getDateFormatOnErr();
        }
    }

    /**
     * if we have problems (should never happen) to get a Dateformat we use this
     */
    private static DateFormat getDateFormatOnErr() {

        return SimpleDateFormat.getDateTimeInstance(DateFormat.MEDIUM, DateFormat.SHORT,
                locale);
    }

    public static NumberFormat getNumberFormat() {

        return NumberFormat.getInstance(locale);
    }

    public static NumberFormat getIntegerFormat() {

        return NumberFormat.getIntegerInstance(locale);
    }

    /**
     * Dependig of the numberClass returns an adequate NumberFormatter
     *
     * @param numberClass
     * @return
     */
    public static NumberFormat getNumberFormat(Class numberClass) {

        if (numberClass.equals(Long.class) ||
                numberClass.equals(Integer.class) ||
                numberClass.equals(Short.class) ||
                numberClass.equals(Byte.class))
            return getIntegerFormat();

        return getNumberFormat();
    }
}