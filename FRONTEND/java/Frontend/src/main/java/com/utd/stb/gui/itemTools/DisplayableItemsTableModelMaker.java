package com.utd.stb.gui.itemTools;

import com.utd.stb.inter.items.DisplayableItem;
import com.utd.stb.inter.items.DisplayableItemInfo;
import com.utd.stb.inter.items.DisplayableItems;
import resources.Res;


/**
 * static methods to delegate from your table model to make a TableModel out of
 * <code>DisplayableItem</code> s <br>
 * <p>
 * <b>Usage: </b>
 * <p>
 * <pre>
 *
 * public int getColumnCount() {
 *
 *     return DisplayableItemsTableModelMaker.getColumnCount();
 * }
 *
 * public Object getValueAt( int row, int col ) {
 *
 *     return DisplayableItemsTableModelMaker.getValueAt( items.get( row ), col );
 * }
 *
 * public String getColumnName( int col ) {
 *
 *     return DisplayableItemsTableModelMaker.getColumnName( col );
 * }
 * </pre>
 *
 * @author PeterBuettner.de
 */
public class DisplayableItemsTableModelMaker {

    DisplayableItems items;
    private static String[] colNames;

    public DisplayableItemsTableModelMaker(DisplayableItems items) {

        this.items = items;
        String cols = items == null || items.getAttributeListColCount() == 0 ?
                "Item,Info" :
                "Item,Info," + items.getAttributeListColNames();

        colNames = cols.split(",");
        colNames[0] = Res.getString(DisplayableItemsTableModelMaker.class.getName()
                + ".columnNameData");
        colNames[1] = Res.getString(DisplayableItemsTableModelMaker.class.getName()
                + ".columnNameInfo");
    }

    public int getColumnCount() {

        return colNames.length;
    }

    public String getColumnName(int column) {

        return colNames[column];
    }

    /**
     * returns the row Object for col==-1, null if Object isn't DisplayableItem
     *
     * @param row
     * @param col
     * @return
     */

    public Object getValueAt(Object row, int col) {
        if (row == null) return null;
        if (!(row instanceof DisplayableItem)) {
            System.out.println("row = " + row.getClass());
            return null;
        }
        if (col == -1 || col > 1) return row;
        DisplayableItem item = (DisplayableItem) row;
        if (col == 0) return item;
        if (col == 1) return new DisplayableItemInfo(item);
        return null;
    }

}