package com.utd.stb.back.ui.impl.userinputs.fields;

/**
 * Implements methods of AbstractUserInput that are common to several UserInputs
 *
 * @author rainer bruns Created 15.11.2004
 */
public class DirectUserInput extends AbstractUserInput {

    Object data;
    Class type;

    /**
     * @param label Label of field
     * @param type  Type of field(the Class implemting the data)
     * @param value Data implentation for field
     */
    public DirectUserInput(String label, Class type, Object value) {

        super(label);
        if (type == null) throw new IllegalArgumentException("Can not use empty clazz.");
        this.type = type;
        this.data = value;
    }

    /*
     * (non-Javadoc)
     *
     * @see com.utd.stb.inter.userinput.UserInput#getType()
     */
    public Class getType() {

        return type;
    }

    /*
     * (non-Javadoc)
     *
     * @see com.utd.stb.inter.userinput.UserInput#setData(java.lang.Object)
     */
    public void setData(Object data) {

        if (data != null &&
                !(type.isInstance(data)) &&
                // MCD, 19.Dec 2008 therewith simple Long values can be written in Double fields
                !(type.getName().equals("java.lang.Double") && data instanceof Long))
            throw new IllegalArgumentException("Data has to be of type:"
                    + type.getName()
                    + " is:"
                    + data.getClass().getName());
        this.data = data;
    }

    /*
     * (non-Javadoc)
     *
     * @see com.utd.stb.inter.userinput.UserInput#getData()
     */
    public Object getData() {

        return data;
    }


    /*
     * (non-Javadoc)
     *
     * @see com.utd.stb.back.ui.impl.userinputs.fields.AbstractUserInput#setReadOnly(boolean)
     */
    public void setReadOnly(boolean b) {

        super.setReadOnly(b);
    }
}