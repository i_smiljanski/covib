package com.utd.stb.back.data.client;

import com.utd.stb.back.database.DBInterface;


/**
 * Wraps database struct data. Has appropriate get/set methods for data from db
 * struct ISR$VALUE$RECORD
 *
 * @author marie-christin dort Created 14.Feb 2006
 */
public class ValueRecord {

    StructMap value;

    public ValueRecord(StructMap s) {

        value = s;
    }

    /**
     * @return the value of underlying field PARAMETERVALUE as String
     */
    public String getParameterValue() {

        return value.getValueAsString("SPARAMETERVALUE");
    }

    /**
     * @return the name of the parameter represented by this ValueRecord
     */
    public String getParameterName() {

        return value.getValueAsString("SPARAMETERNAME");
    }

    public String getNodeType() {

        return value.getValueAsString("SNODETYPE");
    }

    public String getNodeId() {

        return value.getValueAsString("SNODEID");
    }


    /**
     * sets the value of field PARAMETERVALUE to the specified value
     *
     * @param val the value to set
     */
    public void setValue(Object val) {

        value.setValue("SPARAMETERVALUE", val);
    }

    /**
     * used when writing to db
     *
     * @return the underlying data
     * @see DBInterface#putParameter(PropertyList, MenuEntryRecord)
     * @see DBInterface#putPropertyList(PropertyList, String)
     */
    public StructMap getStruct() {

        return value;
    }
}