package com.utd.stb.gui.wizzard.wizzardStep;

import com.utd.gui.renderer.NavigationListCellRenderer;

import javax.swing.*;
import java.awt.*;


/**
 * Renders <code>WizardStep</code> s <br>
 * ListcellRenderer for/with: single selection, last selection/focus always
 * hilite (controlLtHighlight), icon before this item and bold font, Focusborder
 * on Listfocus, some pixels more space to the left side
 *
 * @author PeterBuettner.de
 */
public class WizardStepCellRenderer extends NavigationListCellRenderer {

    /**
     * use the icon before the selected item, non selected will have empty space
     * as wide as this icon
     *
     * @param selIcon null will show no icon
     */
    public WizardStepCellRenderer(Icon selIcon) {

        super(selIcon);
    }

    /**
     * with no icon
     */
    public WizardStepCellRenderer() {

        super(null);
    }

    public Component getListCellRendererComponent(JList list, Object value, int index,
                                                  boolean isSelected, boolean cellHasFocus) {

        super.getListCellRendererComponent(list, value, index, isSelected, cellHasFocus);
        WizardStepTools.configLabelFromWizardStep(this, value);
        return this;
    }

    public Insets getInsets(Insets insets) {

        Insets i = super.getInsets(insets);
        i.left += 4;
        return i;
    }

}