package com.utd.gui.action;

import com.utd.gui.resources.I18N;

import javax.swing.*;


public abstract class ActionDelete extends AbstractAction {

    public ActionDelete() {

        ActionUtil.initActionFromMap(this, I18N.getActionMap(ActionDelete.class.getName()),
                null);
        putValue(SHORT_DESCRIPTION, null);
    }

}