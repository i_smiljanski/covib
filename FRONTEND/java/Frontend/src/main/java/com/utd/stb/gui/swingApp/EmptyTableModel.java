package com.utd.stb.gui.swingApp;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import javax.swing.table.AbstractTableModel;


/**
 * little helper: empty data
 *
 * @author PeterBuettner.de
 */
public class EmptyTableModel extends AbstractTableModel {
    public static final Logger log = LogManager.getRootLogger();

    /*
     * colCount: '1' will create a header, '0' no, but both may show an 'No
     * Data' in a table that supports this (draws this by itself
     */
    public int getColumnCount() {

        return 1;
    }

    public String getColumnName(int column) {

        return " ";
    }

    public int getRowCount() {

        return 0;
    }

    public Object getValueAt(int rowIndex, int columnIndex) {

        return null;
    }


}