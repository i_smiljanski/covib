package com.utd.gui.controls.table;

import com.jidesoft.dialog.JideOptionPane;
import com.jidesoft.grid.HierarchicalPanel;
import com.jidesoft.grid.HierarchicalTable;
import com.jidesoft.grid.HierarchicalTableComponentFactory;
import com.jidesoft.grid.TreeTable;
import com.jidesoft.pane.BookmarkPane;
import com.jidesoft.plaf.UIDefaultsLookup;
import com.jidesoft.plaf.basic.ThemePainter;
import com.jidesoft.swing.*;
import com.uptodata.isr.utils.ConvertUtils;
import com.uptodata.isr.utils.xml.dom.XmlHandler;
import com.utd.gui.controls.table.xmltable.TreeElementProvider;
import com.utd.gui.controls.table.xmltable.XmlNode;
import com.utd.gui.controls.table.xmltable.XmlTreeTableModel;
import com.utd.gui.event.TableDblClickAdapter;
import com.utd.stb.inter.application.Node;
import org.apache.commons.lang.StringUtils;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.w3c.dom.Element;
import org.xml.sax.InputSource;

import javax.swing.*;
import javax.swing.border.EmptyBorder;
import java.awt.*;
import java.awt.event.MouseEvent;
import java.io.*;
import java.nio.charset.Charset;
import java.util.ArrayList;

/**
 * Created by smiljanskii60 on 24.06.2015.
 */
public class DetailsTableComponentFactory implements HierarchicalTableComponentFactory {
    public static final Logger log = LogManager.getRootLogger();
    byte[] details;

    public byte[] getDetails() {
        return details;
    }

    public void setDetails(byte[] details) {
        this.details = details;
    }

    public byte[] getDetailsByError(Exception e) {
        byte[] errDetails = null;
        XmlHandler xmlHandler = new XmlHandler();
        org.w3c.dom.Node root = xmlHandler.nodeRoot("details");
        xmlHandler.createElement("error", e.getMessage(), null, root);

        String xmlStr = xmlHandler.xmlToString();
        if (StringUtils.isNotEmpty(xmlStr)) {
            errDetails = xmlStr.getBytes(Charset.forName(ConvertUtils.encoding));
        }
        return errDetails;
    }

    public Component createChildComponent(HierarchicalTable table, Object value, int row) {
        BookmarkPane pane = new BookmarkPane() {
            @Override
            public void insertTab(String title, Icon icon, Component component, String tip, int index) {
                if (!(component instanceof ResizablePanel)) {
                    ResizablePanel panel = new ResizablePanel(new BorderLayout()) {
                        @Override
                        protected Resizable createResizable() {
                            Resizable resizable = new Resizable(this);
                            resizable.setResizableCorners(Resizable.LOWER);
                            return resizable;
                        }
                    };
                    panel.add(component);
                    panel.setBorder(new EmptyBorder(0, 0, 4, 0) {
                        @Override
                        public void paintBorder(Component c, Graphics g, int x, int y, int width, int height) {
                            super.paintBorder(c, g, x, y, width, height);
                            ThemePainter painter = (ThemePainter) UIDefaultsLookup.get("Theme.painter");
                            if (painter != null) {
                                Insets insets = getBorderInsets(c);
                                Rectangle rect = new Rectangle(0, y + height - insets.bottom, width, insets.bottom);
                                painter.paintGripper((JComponent) c, g, rect, SwingConstants.HORIZONTAL, ThemePainter.STATE_DEFAULT);
                            }
                        }
                    });
                    super.insertTab(title, icon, panel, tip, index);
                } else {
                    super.insertTab(title, icon, component, tip, index);
                }
            }
        };
        Object node = table.getModel().getValueAt(row, -1);
        if (node instanceof Node) {

            if (details != null) {
                try {
                    pane.addTab("Details", new DetailsPanel(details));
                } catch (Exception e) {
                    log.error(e);
                    e.printStackTrace();
                }
            }
        }
        return new HierarchicalPanel(pane, BorderFactory.createEmptyBorder());
    }

    public void destroyChildComponent(HierarchicalTable table, Component component, int row) {
    }


    static class DetailsPanel extends JPanel {

        public DetailsPanel(byte[] details) throws Exception {
            initComponents(details);
        }

        void initComponents(byte[] details) throws Exception {
            setLayout(new BorderLayout(4, 4));
            setBorder(BorderFactory.createCompoundBorder(
                    new JideTitledBorder(new PartialLineBorder(UIDefaultsLookup.getColor("controlShadow"), 1,
                            PartialSide.NORTH), "Details", JideTitledBorder.RIGHT, JideTitledBorder.CENTER),
                    BorderFactory.createEmptyBorder(2, 2, 2, 2)));
            Component panel = createDetailPanel(details);
            add(panel, BorderLayout.CENTER);
            JideSwingUtilities.setOpaqueRecursively(this, false);
            setOpaque(true);
        }

        private Component createDetailPanel(byte[] details) {
            InputStream inputStream = new ByteArrayInputStream(details);
            Reader reader = null;
            try {
                reader = new InputStreamReader(inputStream, ConvertUtils.encoding);
            } catch (UnsupportedEncodingException e) {
                log.warn(e);
            }
            InputSource is = new InputSource(reader);
            XmlTreeTableModel xmlTable;
            XmlNode xmlNode;
            try {
                xmlNode = new XmlNode(TreeElementProvider.createDocument(is).getDocumentElement());
            } catch (Exception e) {
                log.info(e);
                XmlHandler xmlHandler = new XmlHandler();
                Element msg = xmlHandler.nodeRoot("message");
//                String text = is.toString();
//                msg.setTextContent(WordUtils.wrap(text, 20));
                msg.setTextContent(is.toString());
                xmlNode = new XmlNode(msg);
            }

            java.util.List<XmlNode> list = new ArrayList<XmlNode>();
            list.add(xmlNode);
            xmlTable = new XmlTreeTableModel(list);
            TreeTable treeTable = new TreeTable(xmlTable);
            treeTable.setColumnResizable(true);

            TableDblClickAdapter dblClickAdapter = new TableDblClickAdapter() {
                @Override
                public void rowDblClick(JTable table, int row, MouseEvent e) {
                    String title = (table.getModel().getValueAt(row, 0)).toString();
                    String msg = (String) table.getModel().getValueAt(row, -1);

                    JTextArea area = new JTextArea(msg);
                    area.setEditable(false);
                    area.setOpaque(false);
                    JideOptionPane.showMessageDialog(treeTable, area, title, JideOptionPane.INFORMATION_MESSAGE, null);
                }

            };

            treeTable.addMouseListener(dblClickAdapter);
            return treeTable;
        }
    }
}
