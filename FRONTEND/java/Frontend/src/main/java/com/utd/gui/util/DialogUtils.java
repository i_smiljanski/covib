package com.utd.gui.util;

import com.jgoodies.forms.builder.ButtonBarBuilder;
import com.jgoodies.forms.builder.ButtonStackBuilder;
import com.jgoodies.forms.builder.DefaultFormBuilder;
import com.jgoodies.forms.debug.FormDebugPanel;
import com.jgoodies.forms.factories.Borders;
import com.jgoodies.forms.factories.ButtonBarFactory;
import com.jgoodies.forms.factories.DefaultComponentFactory;
import com.jgoodies.forms.layout.ConstantSize;
import com.jgoodies.forms.layout.FormLayout;
import com.jgoodies.forms.util.LayoutStyle;
import com.uptodata.isr.gui.util.Colors;
import com.uptodata.isr.gui.util.PixelSizes;
import com.utd.gui.action.ActionCancel;
import com.utd.gui.action.ActionOk;
import com.utd.gui.action.ActionUtil;
import com.uptodata.isr.gui.borders.SeparatorBorder;
import com.utd.gui.controls.RepeatingButtonDecorator;
import com.utd.gui.controls.TitleBar;
import com.uptodata.isr.gui.lookAndFeel.TweakUI;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import javax.swing.*;
import javax.swing.border.Border;
import javax.swing.border.CompoundBorder;
import javax.swing.border.EmptyBorder;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.KeyEvent;

/**
 * TODOlater remove all BorderLayout#String pos definition and check that we
 * doesn't need them
 *
 * @author PeterBuettner.de
 */
public class DialogUtils implements SwingConstants/* sorry:-) */ {
    public static final Logger log = LogManager.getRootLogger();

    private static boolean debugLayout;

    /**
     * See {@link com.utd.gui.controls.TitleBar}
     *
     * @param sTitle
     * @param subText
     * @param icon
     * @param prefSize
     * @return
     */
    public static JPanel createTitlePanel(String sTitle, String subText, Icon icon,
                                          int prefSize) {

        return new TitleBar(sTitle, subText, icon, prefSize);
    }

    /**
     * creates a compound border, with a separator on one side (pos)
     *
     * @param border
     * @param separatorInside separator is inside the given border, else the
     *                        border is wrapped into the SeparatorBorder
     * @param pos             from Borderlayout: North, South, ... not Center:-)
     * @return
     */
    public static Border wrapIntoSeparatorBorder(Border border, boolean separatorInside,
                                                 String pos) {

        Border sepBorder = new SeparatorBorder(borderLayoutToSwingConstant(pos));
        if (separatorInside) {
            return new CompoundBorder(border, sepBorder);
        } else {
            return new CompoundBorder(sepBorder, border);
        }
    }

    /**
     * @param separatorInside separator is inside the given border, else the
     *                        border is wrapped into the SeparatorBorder
     * @param pos             where the separator should be
     *                        SwingConstants.TOP,LEFT,BOTTOM,RIGHT
     * @return
     */
    public static Border getSeparatorButtonBarBorder(boolean separatorInside, int pos) {

        LayoutStyle ls = LayoutStyle.getCurrent();
        ConstantSize gap = ls.getButtonBarPad();
        Border border = Borders.createEmptyBorder((pos == TOP) ? gap : ls
                        .getDialogMarginY(), (pos == LEFT) ? gap : ls.getDialogMarginX(),
                (pos == BOTTOM) ? gap : ls
                        .getDialogMarginY(), (pos == RIGHT)
                        ? gap
                        : ls.getDialogMarginX());

        Border sepBorder = new SeparatorBorder(pos);
        if (separatorInside) {
            return new CompoundBorder(border, sepBorder);
        } else {
            return new CompoundBorder(sepBorder, border);
        }

    }

    /**
     * Sets a new Border with a separator on side pos (Borderlayout .NORTH,
     * .SOUTH, .WEST, .EAST) e.g. as a Ok/Cancel Panel below in a dialog. Also a
     * predefined predefined border (empty, some pixels) is set 'between'
     * Component and Separator (
     *
     * @param component
     * @param pos
     */
    public static void setSeparatedDialogBorder(JComponent component, String pos) {

        component.setBorder(wrapIntoSeparatorBorder(Borders.DIALOG_BORDER, false, pos));
    }

    /**
     * here we
     * use SwingConstants.TOP,LEFT,BOTTOM,RIGHT
     *
     * @param component
     * @param pos
     */
    public static void setSeparatedDialogBorder(JComponent component, int pos) {

        component.setBorder(new CompoundBorder(new SeparatorBorder(pos),
                Borders.DIALOG_BORDER));
    }

    /**
     * Returns SwingConstants.VERTICAL (North, South) or
     * SwingConstants.HORIZONTAL (West, East) or throws
     * IllegalArgumentException.
     *
     * @param borderLayoutPos
     * @return
     * @throws IllegalArgumentException if not North, South, West, East
     */
    public static int borderLayoutToSwingConstantVH(String borderLayoutPos)
            throws IllegalArgumentException {

        if (borderLayoutPos.equals(BorderLayout.WEST)
                || borderLayoutPos.equals(BorderLayout.EAST)) {
            return HORIZONTAL;
        } else if (borderLayoutPos.equals(BorderLayout.NORTH)
                || borderLayoutPos.equals(BorderLayout.SOUTH)) {
            return VERTICAL;
        } else {
            throw new IllegalArgumentException(
                    "Illegal pos:'"
                            + borderLayoutPos
                            + "', use the values from BorderLayout, but not CENTER.");
        }
    }

    /**
     * Returns SwingConstants.(TOP , LEFT , BOTTOM, RIGHT) on input (North,
     * West, South, East) or throws IllegalArgumentException.
     *
     * @param pos
     * @return
     * @throws IllegalArgumentException
     */
    public static int borderLayoutToSwingConstant(String pos)
            throws IllegalArgumentException {

        if (pos.equals(BorderLayout.NORTH)) {
            return TOP;
        } else if (pos.equals(BorderLayout.WEST)) {
            return LEFT;
        } else if (pos.equals(BorderLayout.SOUTH)) {
            return BOTTOM;
        } else if (pos.equals(BorderLayout.EAST)) {
            return RIGHT;
        } else {
            throw new IllegalArgumentException(
                    "Illegal pos:'"
                            + pos
                            + "', use the values from BorderLayout, but not CENTER.");
        }
    }

    /**
     * Swaps SwingConstants.HORIZONTAL and SwingConstants.VERTICAL.
     *
     * @param vertHorz
     * @return
     * @throws IllegalArgumentException if input not SwingConstants.HORIZONTAL
     *                                  or SwingConstants.VERTICAL
     */
    private static int swingConstantsSwapVH(int vertHorz) throws IllegalArgumentException {

        if (vertHorz != HORIZONTAL && vertHorz != VERTICAL) {
            throw new IllegalArgumentException(
                    "Use only SwingConstants.HORIZONTAL or SwingConstants.VERTICAL, not:"
                            + vertHorz);
        }

        return (vertHorz == HORIZONTAL) ? VERTICAL : HORIZONTAL;
    }

    /**
     * creates a new JComponent (maybe JPanel but maybe not) with Ok, Cancel,
     * (optional Help), with a separator on side pos (Borderlayout .NORTH,
     * .SOUTH, .WEST, .EAST) for a Ok/Cancel/Help Panel below/besides in a
     * dialog. There is a predefined border around 'the buttons' (
     *
     * @param bOk
     * @param bCancel
     * @param bHelp   use null if no help
     * @param pos
     * @return
     */
    public static JComponent createButtonPanel(JButton bOk, JButton bCancel, JButton bHelp,
                                               String pos) {

        return createButtonPanel(new JButton[]{bOk, bCancel}, bHelp, pos);
    }

    /**
     * creates a new JComponent (maybe JPanel but maybe not) with Buttons
     * (optional Help), with a separator on side pos (Borderlayout .NORTH,
     * .SOUTH, .WEST, .EAST) for a [Buttons]/Help Panel below/besides in a
     * dialog. There is a predefined border around 'the buttons'
     *
     * @param buttons
     * @param bHelp   use null if no help
     * @param pos
     * @return
     */
    public static JComponent createButtonPanel(JButton[] buttons, JButton bHelp, String pos) {

        // no chance for now to have it on horizontal ones without copying
        // code from ButtonBarFactory, so no debug view
        // JPanel pnl = debugLayout? new FormDebugPanel() : new JPanel();
        if (HORIZONTAL == borderLayoutToSwingConstantVH(pos) /* verifies pos */) {

            // ButtonStackBuilder builder = new ButtonStackBuilder(pnl);
            ButtonStackBuilder builder = new ButtonStackBuilder();

            for (int i = 0; i < buttons.length; i++) {

                builder.addGridded(buttons[i]);
                if (i < buttons.length - 1) {
                    builder.addRelatedGap();
                }

            }

            if (bHelp != null) {

                builder.addGlue();
                builder.addUnrelatedGap();
                builder.addGridded(bHelp);

            }

            JPanel p = builder.getPanel();
            setSeparatedDialogBorder(p, pos);
            return p;

        }

        // SwingConstants.VERTICAL
        // JComponent c =
        //	(bHelp==null)?
        //  ButtonBarFactory.buildOKCancelBar(bOk, bCancel)
        //	:ButtonBarFactory.buildHelpOKCancelBar(bHelp,bOk, bCancel);
        ButtonBarBuilder builder = new ButtonBarBuilder();

        if (bHelp != null) {
            builder.addGridded(bHelp);
        }

        // always add some space on the left side
        builder.addGlue();
        builder.addUnrelatedGap();
        builder.addUnrelatedGap();

        // now the buttons
        for (int i = 0; i < buttons.length; i++) {

            builder.addGridded(buttons[i]);
            if (i < buttons.length - 1) {
                builder.addRelatedGap();
            }

        }

        JPanel c = builder.getPanel();
        setSeparatedDialogBorder(c, pos);
        return c;

    }

    public static DefaultFormBuilder getFormBuilder(FormLayout layout) {

        if (debugLayout) {
            return new DefaultFormBuilder(layout, new FormDebugPanel());
        } else {
            return new DefaultFormBuilder(layout);
        }

    }

    public static boolean isDebugLayout() {

        return debugLayout;

    }

    public static void setDebugLayout(boolean debugLayout) {

        DialogUtils.debugLayout = debugLayout;
    }

    /**
     * Creates a user presentable text from a KeyStroke, like 'Alt + Umschalt'
     * or 'Alt + Shift' depending on the locale, the delimiter is from
     * UIManager, [UIManager.getString("MenuItem.acceleratorDelimiter")]
     *
     * @param accelerator
     * @return
     */
    public static String keyStrokeToUserText(KeyStroke accelerator) {

        if (accelerator == null) {
            return null;
        }
        String text = "";

        int modifiers = accelerator.getModifiers();
        if (modifiers > 0) {
            text = KeyEvent.getKeyModifiersText(modifiers);
            String acceleratorDelimiter = UIManager
                    .getString("MenuItem.acceleratorDelimiter");
            if (acceleratorDelimiter == null) {
                acceleratorDelimiter = "+";
            }
            text += acceleratorDelimiter;
        }
        text += KeyEvent.getKeyText(accelerator.getKeyCode());

        return text;
    }

    /**
     * create a html fragment (currently "&lt;font
     * style='color:#anyValue;'&gt;[$keyStroke]&lt;/font&gt;') for usage in html
     * Tooltips for buttons/actions, if color==null returns the fragment without
     * color/font
     *
     * @param keyStroke
     * @return null if input is empty
     */
    public static String getColoredKeystrokeHtmlText(KeyStroke keyStroke, Color color) {

        if (keyStroke == null) {
            return null;
        }
        if (color == null) {
            return "[" + keyStrokeToUserText(keyStroke) + "]";
        }
        String sColor = "000000" + Integer.toHexString(color.getRGB());
        sColor = "color:#" + sColor.substring(sColor.length() - 6) + ";";
        return "<font style='" + sColor + "'>[" + keyStrokeToUserText(keyStroke)
                + "]</font>";

    }

    /**
     * the color is
     * a usefull one from UIManager (currently: controlDkShadow)
     *
     * @param keyStroke
     * @return
     */
    public static String getColoredKeystrokeHtmlText(KeyStroke keyStroke) {

        return getColoredKeystrokeHtmlText(keyStroke, Colors.getUIControlDkShadow());
    }

    /**
     * Creates a label (with mnemonic, &amp; is prefix use &amp;&amp; for a
     * &amp;) and binds it to the component. see
     * {@link com.jgoodies.forms.factories.ComponentFactory}
     *
     * @param textWithMnemonic
     * @param bindTo
     * @return
     */
    public static JLabel buildAndBindJLabel(String textWithMnemonic, Component bindTo) {

        return buildAndBindJLabel(textWithMnemonic, bindTo, true);
    }

    /**
     * Creates a label and binds it to the component, may or may not generate a
     * Mnemonic
     *
     * @param text
     * @param bindTo
     * @param generateMnemonic with mnemonic, &amp; is prefix use &amp;&amp; for
     *                         a &amp; see {@link com.jgoodies.forms.factories.ComponentFactory}
     * @return
     */
    public static JLabel buildAndBindJLabel(String text, Component bindTo,
                                            boolean generateMnemonic) {

        JLabel label;
        if (generateMnemonic) {
            label = DefaultComponentFactory.getInstance().createLabel(text);
        } else {
            label = new JLabel(text);
        }

        label.setLabelFor(bindTo);
        return label;
    }

    /**
     * currently with a smaller inner border (maybe we can set this in the L&F)
     * and non-focusable
     *
     * @param a
     * @return
     */
    public static AbstractButton createToolbarButton(Action a) {

        JButton b = new JButton(a) {

            public Insets getInsets() {

                return new Insets(2, 2, 2, 2);
            }
        };
        b.setFocusable(false);
        return b;
    }// ----------------------------------------------------------------------

    /**
     * repeating like a
     * scrollbar button see:
     * {@link com.utd.gui.controls.RepeatingButtonDecorator}
     *
     * @param a
     * @return
     */
    public static AbstractButton createToolbarButtonRepeating(Action a) {

        AbstractButton b = createToolbarButton(a);
        RepeatingButtonDecorator.addButton(b);
        return b;
    }// ----------------------------------------------------------------------

    /**
     * Creates a non-modal dialog without a title.
     *
     * @param parentComponent a component, we search for the
     *                        Window(Frame/Dialog) this component belongs to, if it is null we create a
     *                        new JFrame as a dummy parent
     * @return
     */
    public static JDialog createDialog(Component parentComponent) {

        JDialog dialog = null;
        // this works too (tested) if there is another modal out there
        if (parentComponent != null) {
            Window parentWindow = SwingUtilities.getWindowAncestor(parentComponent);

            if (parentWindow instanceof Frame) {
                dialog = new JDialog((Frame) parentWindow);
            } else if (parentWindow instanceof JDialog) {
                dialog = new JDialog((Dialog) parentWindow);
            }
        }
        if (dialog == null) {
            dialog = new JDialog((JFrame) null);
        }
//        log.debug("dialog = " + dialog);
        TweakUI.setDialogDecoration(dialog);
        return dialog;
    }

    /**
     * Shows an Ok/Cancel dialog with the specified content and title, no
     * headerbar, centered on the parent components frame/dialog
     *
     * @param parentComponent a component, we search for the
     *                        Window(Frame/Dialog) this component belongs to
     * @param content
     * @param title           of the dialog
     * @return true if user pressed Ok, false else
     */
    static public boolean showDialogOkCancel(Component parentComponent, Component content, String title) {

        final boolean[] returnValue = new boolean[1];// array to have a final
        // writable value
        returnValue[0] = false;// is implicit, but just to make it clear

        // ... create dialog ..............................
        final JDialog dlg = createDialog(parentComponent);
        dlg.setModal(true);
        dlg.setTitle(title);

        dlg.setDefaultCloseOperation(JDialog.HIDE_ON_CLOSE);

        // ... Actions .....................................
        Action acOk = new ActionOk() {

            public void actionPerformed(ActionEvent e) {

                returnValue[0] = true;
                dlg.setVisible(false);
            }
        };

        Action acCancel = new ActionCancel() {

            public void actionPerformed(ActionEvent e) {

                returnValue[0] = false;
                dlg.setVisible(false);
            }
        };
        // ... Buttons .....................................
        JButton bOk = new JButton(acOk);
        JPanel buttons = ButtonBarFactory.buildOKCancelBar(bOk, new JButton(acCancel));
        buttons.setBorder(new EmptyBorder(PixelSizes.getButtonBarGapY(buttons), 0, 0, 0));

        // ... Put together .................................
        JPanel pnlMain = new JPanel(new BorderLayout());
        pnlMain.setBorder(Borders.DIALOG_BORDER);

        pnlMain.add(content, BorderLayout.CENTER);
        pnlMain.add(buttons, BorderLayout.SOUTH);
        dlg.getContentPane().add(pnlMain);

        // Ok/Cancel Actinos per Keyboard. default: ok , Key-Escape: cancel
        dlg.getRootPane().setDefaultButton(bOk);
        // not acOk or a focussed cancel won't work with enter
        ActionUtil.putIntoActionMap(dlg.getRootPane(), new Action[]{acCancel});

        // ... show and go .........................
        dlg.pack();
        // center on parent
        dlg.setLocationRelativeTo(dlg.getOwner());
        dlg.setVisible(true);
        return returnValue[0];
    }

}
