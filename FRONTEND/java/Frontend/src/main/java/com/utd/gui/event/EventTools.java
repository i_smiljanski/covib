package com.utd.gui.event;

import java.awt.*;
import java.awt.event.InputEvent;
import java.awt.event.KeyEvent;
import java.awt.event.MouseEvent;
import java.lang.reflect.Field;


/**
 * Usefull tools, e.g. to answer questions like: 'is the event a left mouse
 * button double click?'
 *
 * @author PeterBuettner.de
 */
public class EventTools {

    /**
     * <b>�rks:-) </b> Try by a really dirty hack if the KeyEvent is a Windows
     * Popup Key - only works on KeyEvent.KEY_PRESSED. for jre 1.5 we may use
     * VK_CONTEXT_MENU
     *
     * @param e
     * @return true if it is one, false on any error:doesn't matter
     */
    public static boolean isPopupKey(KeyEvent e) {

        if (!init_keyEvent_bData) init_keyEvent_bData();
        if (keyEvent_bData == null) return false;

        byte[] b = null;
        try {
            b = (byte[]) keyEvent_bData.get(e);
        } catch (Exception e1) {
            return false;
        }
        if (b != null && b.length > 8) return b[8] == 93;

        return false;

    }

    private static Field keyEvent_bData;

    private static boolean init_keyEvent_bData;

    private static void init_keyEvent_bData() {

        init_keyEvent_bData = true; // try only once
        try {
            keyEvent_bData = AWTEvent.class.getDeclaredField("bdata");
        } catch (Exception e) {
            e.printStackTrace();
        }
        if (keyEvent_bData == null) return;

        try {
            keyEvent_bData.setAccessible(true);
        } catch (Exception e) {
            keyEvent_bData = null;
        }

    }

    /**
     * Sees only the first DblClick: later click events produced have a
     * ClickCount of 3,4,5,6,.... Use {@link isLeftMouseEvenClick}if you want
     * to react on the following 'Dbl-Click' events (4,6,8...)
     *
     * @param e
     * @return
     */
    public static boolean isLeftMouseDblClick(MouseEvent e) {

        return ((e.getButton() == MouseEvent.BUTTON1) && (e.getClickCount() == 2));

    }

    /**
     * Sees 'every' DblClick: later click events produced have a ClickCount of
     * 3,4,5,6,.... this method sees every 'Dbl-Click' event (4,6,8...) 'even'
     * count clicks.
     *
     * @param e
     * @return
     */
    public static boolean isLeftMouseEvenClick(MouseEvent e) {

        return ((e.getButton() == MouseEvent.BUTTON1) && (e.getClickCount() % 2 == 0));

    }

    /**
     * creates a no KeyEvent, based on the InputEvent(when, modifiers) with
     * id=KeyEvent.KEY_PRESSED, keyChar is sythesized by '(char)keyCode'
     *
     * @param e
     * @param keyCode
     * @return
     */
    public static KeyEvent toKeyEvent(InputEvent e, int keyCode) {

        return toKeyEvent(e, KeyEvent.KEY_PRESSED, keyCode);

    }

    /**
     * creates a new KeyEvent, based on the InputEvent(when, modifiers)
     *
     * @param e
     * @param id      KeyEvent.KEY_PRESSED, KEY_RELEASED, KEY_TYPED
     * @param keyCode
     * @return
     */
    public static KeyEvent toKeyEvent(InputEvent e, int id, int keyCode) {

        return new KeyEvent(e.getComponent(), id, e.getWhen(), e.getModifiers(), keyCode,
                (char) keyCode);

    }

    /**
     * better to read KeyEvent dump
     *
     * @param e
     * @return
     */
    public static String debugPrint(KeyEvent e) {

        StringBuffer str = new StringBuffer(100);

        switch (e.getID()) {

            case KeyEvent.KEY_PRESSED:
                str.append("P");
                break;

            case KeyEvent.KEY_RELEASED:
                str.append("R");
                break;

            case KeyEvent.KEY_TYPED:
                str.append("T");
                break;

            default:
                str.append("?");
                break;

        }

        //    KeyEvent.
        int keyCode = e.getKeyCode();
        str.append("\t# ").append(keyCode);
        str.append("\tT ")
                .append(keyCode == 0 ? "?" : KeyEvent.getKeyText(e.getKeyCode()));

        /*
         * Some keychars don't print well, e.g. escape, backspace, tab, return,
         * delete, cancel. Get keyText for the keyCode instead of the keyChar.
         */
        str.append("\t ch=");

        switch (e.getKeyChar()) {

            case '\b':
                str.append(KeyEvent.getKeyText(KeyEvent.VK_BACK_SPACE));
                break;

            case '\t':
                str.append(KeyEvent.getKeyText(KeyEvent.VK_TAB));
                break;

            case '\n':
                str.append(KeyEvent.getKeyText(KeyEvent.VK_ENTER));
                break;

            case '\u0018':
                str.append(KeyEvent.getKeyText(KeyEvent.VK_CANCEL));
                break;

            case '\u001b':
                str.append(KeyEvent.getKeyText(KeyEvent.VK_ESCAPE));
                break;

            case '\u007f':
                str.append(KeyEvent.getKeyText(KeyEvent.VK_DELETE));
                break;

            case KeyEvent.CHAR_UNDEFINED:
                str.append("?");
                break;

            default:
                str.append("'").append(e.getKeyChar()).append("'");
                break;
        }

        str.append("\tmod=[");
        str.append(KeyEvent.getKeyModifiersText(e.getModifiers()));
        str.append("  ").append(KeyEvent.getModifiersExText(e.getModifiersEx()));
        str.append("]");
        return str.toString();

    }

}