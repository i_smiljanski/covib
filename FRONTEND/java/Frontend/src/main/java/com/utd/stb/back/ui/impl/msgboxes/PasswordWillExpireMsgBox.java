package com.utd.stb.back.ui.impl.msgboxes;

import com.utd.stb.back.database.Dispatcher;
import com.utd.stb.inter.action.ButtonItem;
import com.utd.stb.inter.application.BackendException;
import com.utd.stb.interlib.BasicMessageBox;


/**
 * Implementation of BasicMessageBox that shows a warning if on logon password
 * expiration is detected. If desired allows change of password.
 *
 * @author rainer bruns Created 06.12.2004
 */
public class PasswordWillExpireMsgBox extends BasicMessageBox {

    Dispatcher d;

    /**
     * Creates a message box with yes/no buttons
     *
     * @param title Title of MessageBox
     * @param text  Text of MessageBox
     * @param d     Dispatcher
     */
    public PasswordWillExpireMsgBox(String title, String text, Dispatcher d) {

        super(null, createHeaderBar(title, ICON_EXCLAMATION), null, text, new ButtonItem[]{
                bYes, bNo}, bYes, true);
        this.d = d;

    }

    /*
     * (non-Javadoc)
     * 
     * @see com.utd.stb.interlib.BasicMessageBox#onBoxClose(com.utd.stb.inter.action.ButtonItem)
     *      If OK button was pressed show password change dialog. Else do
     *      nothing.
     */
    protected Object onBoxClose(ButtonItem clickedButton) {

        if (bYes.equals(clickedButton)) {
            try {
                return d.changePassword();
            } catch (BackendException be) {
                be.printStackTrace();
            }
        }
        return null;
    }
}