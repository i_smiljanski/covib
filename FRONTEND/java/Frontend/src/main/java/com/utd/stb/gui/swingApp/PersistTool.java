package com.utd.stb.gui.swingApp;

import com.uptodata.isr.gui.util.PropertyTools;
import com.utd.stb.gui.userinput.swing.FilterTableFactory;
import com.utd.stb.gui.userinput.swing.GridState;
import com.utd.stb.inter.application.GuiPersistance;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import javax.swing.*;
import java.util.ArrayList;
import java.util.Properties;


/**
 * Dirty Helper to save and load SplitWindowState, and some more
 *
 * @author PeterBuettner.de
 */
public class PersistTool {
    public static final Logger log = LogManager.getRootLogger();

    /**
     * gui properties from server
     */
    public static void store(GuiPersistance guiPersistance, SplitWindowState state, String key) {
        Properties p = new Properties();
        state.store(p, "");// no prefix, it is in the persistance
        guiPersistance.putGuiPersistenceData(key, null, PropertyTools.toByteArray(p, 500));
    }

    public static SplitWindowState read(GuiPersistance guiPersistance, String key) {

        SplitWindowState state = new SplitWindowState();

        byte[] b = guiPersistance.getGuiPersistenceData(key, null);
        if (b != null) { // data available
            Properties p = PropertyTools.fromByteArray(b);
            state.load(p, "");// no prefix, it is in the persistance
        }

        // older version (other option class) to newer version
        return state;
    }

    /**
     * gui properties from server
     */
    public static void store(GuiPersistance guiPersistance, String regExpr, String key) {

        Object o = load(guiPersistance, key);
        ArrayList regExprs = new ArrayList();
        Properties p = new Properties();

        if (o != null && o instanceof ArrayList) regExprs = (ArrayList) o;

        if (regExprs.contains(regExpr) || regExpr == null || regExpr.length() == 0) return;
        regExprs.add(0, regExpr);

        PropertyTools.setArrayList(p, key, regExprs);
        guiPersistance.putGuiPersistenceData(key, null, PropertyTools.toByteArray(p, 500));
    }

    /**
     * gui properties from server
     */
    public static Object load(GuiPersistance guiPersistance, String key) {

        ArrayList regExprs = null;
        byte[] b = guiPersistance.getGuiPersistenceData(key, null);
        if (b != null) {
            Properties p = PropertyTools.fromByteArray(b);
            return PropertyTools.getArrayList(p, key);
        }
        return null;
    }

   /* public static void store(GuiPersistance guiPersistance, GridWidowPersistent state, String key) {

        Properties properties = new Properties();;
        state.fillProperties(properties);
        guiPersistance.putGuiPersistenceData(key, null, PropertyTools.toByteArray(properties, 500));
    }*/

    public static void store(GuiPersistance guiPersistance, GridState state, String key) {
        log.info("begin");
        Properties p = new Properties();
        state.store(p);
        guiPersistance.putGuiPersistenceData(key, null, PropertyTools.toByteArray(p, 500));
        log.info("end");
    }

    public static GridState readGridState(GuiPersistance guiPersistance, String key) {

        GridState state = new GridState();

        byte[] b = guiPersistance.getGuiPersistenceData(key, null);
        if (b != null) { // data available
            Properties p = PropertyTools.fromByteArray(b);
            state.load(p);
        }

        // older version (other option class) to newer version
        return state;
    }



    /**
     * find global GuiPersistance instance
     *
     * @return may return null if not found, but this should never happen since
     * we need this in the app, and we shurely put it into the UIManager
     */
    public static GuiPersistance findGuiPersistance() {

        return (GuiPersistance) UIManager.get(com.utd.stb.inter.application.GuiPersistance.class);
    }

}