package com.utd.stb.back.ui.impl.userinputs.fields;

import com.utd.stb.back.data.client.PropertyRecord;

import javax.swing.*;

/**
 * @author Inna Smiljanski  created 01.12.2010
 */
public class ProgressBarUserInput extends AbstractUserInput {

    PropertyRecord property;
    Object progressBar;

    /**
     * @param label       Label of field
     * @param progressBar Type of field(the Class implemting the data)
     * @param property    Record containing complete information for field
     */
    public ProgressBarUserInput(String label, Object progressBar, PropertyRecord property) {
        super(label);
        this.property = property;
        this.progressBar = progressBar;
    }

    public void setData(Object data) {
        if (data != null && data instanceof JProgressBar && progressBar instanceof JProgressBar) {
            ((JProgressBar) progressBar).setString(((JProgressBar) data).getString());
            ((JProgressBar) progressBar).setValue(((JProgressBar) data).getValue());
        }
        property.setValue(data);
    }

    public Object getData() {
        return progressBar;
    }

    public Class getType() {
        return JProgressBar.class;
    }
}
