package com.utd.stb.inter.action;

/**
 * Defines a Menuitem the user may see. To do the Action behind, send this
 * object to the base
 *
 * @author PeterBuettner.de
 */
public interface MenuItem {

    /**
     * The label of the menu
     *
     * @return
     */
    String getText();

    /**
     * The icon of the menu
     *
     * @return
     */
    String getIconId();

    /**
     * Show enabled or disabled
     *
     * @return
     */
    boolean isEnabled();

    /**
     * If this is a Separator, the other properties of this item are ignored
     *
     * @return
     */
    boolean isSeparator();

    /**
     * subitems if null we are a normal menuitem
     *
     * @return
     */
    MenuItems getChildren();

}