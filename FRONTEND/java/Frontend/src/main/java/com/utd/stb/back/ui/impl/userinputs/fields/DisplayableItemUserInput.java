package com.utd.stb.back.ui.impl.userinputs.fields;

import com.utd.stb.inter.items.DisplayableItem;
import com.utd.stb.inter.userinput.DisplayableItemInput;


/**
 * Implementation of AbstractUserInput for fields with List of values.
 *
 * @author rainer bruns Created 02.12.2004
 * @see ListItemInput
 */
public class DisplayableItemUserInput extends AbstractUserInput {

    private DisplayableItemInput itemInput;

    /**
     * @param label Label of field
     * @param data  Field data Implentation
     */
    public DisplayableItemUserInput(String label, DisplayableItemInput data) {

        super(label);
        this.itemInput = data;
    }

    /*
     * (non-Javadoc)
     * 
     * @see com.utd.stb.inter.userinput.UserInput#getType()
     */
    public Class getType() {

        return DisplayableItemInput.class;
    }

    /*
     * (non-Javadoc)
     * 
     * @see com.utd.stb.inter.userinput.UserInput#setData(java.lang.Object) set
     *      new value in data implentation
     */
    public void setData(Object data) {
        if (data instanceof DisplayableItem) itemInput.setSelectedItem((DisplayableItem) data);
    }

    /*
     * (non-Javadoc)
     * 
     * @see com.utd.stb.inter.userinput.UserInput#getData()
     */
    public Object getData() {

        return itemInput;
    }

}

