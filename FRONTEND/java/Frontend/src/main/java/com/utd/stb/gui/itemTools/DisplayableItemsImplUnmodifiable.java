package com.utd.stb.gui.itemTools;

import com.utd.stb.inter.items.DisplayableItem;
import com.utd.stb.inter.items.DisplayableItems;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;


/**
 * Unmodifiable Version.
 *
 * @author PeterBuettner.de
 */
public class DisplayableItemsImplUnmodifiable implements DisplayableItems {

    protected List selectedData;
    protected DisplayableItems allItems;

    /**
     * Initialized with the items of the collection, with check of valid data
     * type
     *
     * @param collection not null
     */
    public DisplayableItemsImplUnmodifiable(Collection selectedItems, DisplayableItems allItems) {

        for (Object selectedItem : selectedItems)
            if (!(selectedItem instanceof DisplayableItem))
                throw new IllegalArgumentException("At least one attribute is not of type :"
                        + DisplayableItem.class.getName());
        this.selectedData = new ArrayList(selectedItems);

        this.allItems = allItems;
    }

    public int getSize() {

        return selectedData.size();
    }

    public DisplayableItem get(int index) {
        return (DisplayableItem) selectedData.get(index);
    }

    public int indexOf(DisplayableItem item) {

        return selectedData.indexOf(item);
    }

    /* (non-Javadoc)
     * @see com.utd.stb.inter.items.DisplayableItems#getAttributeListColNames()
     */
    public String getAttributeListColNames() {

        if (allItems != null) return allItems.getAttributeListColNames();
        return "";
    }

    /* (non-Javadoc)
     * @see com.utd.stb.inter.items.DisplayableItems#getAttributeListColCount()
     */
    public int getAttributeListColCount() {

        if (allItems != null) return allItems.getAttributeListColCount();
        return 0;
    }

}