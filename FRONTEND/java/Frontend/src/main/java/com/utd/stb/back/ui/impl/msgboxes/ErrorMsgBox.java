package com.utd.stb.back.ui.impl.msgboxes;

import com.utd.stb.inter.action.ButtonItem;
import com.utd.stb.interlib.BasicMessageBox;

/**
 * Implementation of BasicMessageBox that displays a warning
 *
 * @author rainer bruns Created 06.12.2004
 */
public class ErrorMsgBox extends BasicMessageBox {

    /**
     * Creates a message box with close button
     *
     * @param title Title of MessageBox
     * @param text  Text of MessageBox
     * @throws BackendException
     */
    public ErrorMsgBox(String title, String text) {

        super(null, createHeaderBar(title, ICON_STOP), null, text, new ButtonItem[]{bClose}, null, true);
        System.out.println("ErrorMsgBox:title = " + title + " text = " + text + " " + Thread.currentThread().getStackTrace()[2]);
    }

    /*
     * (non-Javadoc)
     * 
     * @see com.utd.stb.interlib.BasicMessageBox#onBoxClose(com.utd.stb.inter.action.ButtonItem)
     *      Do nothing here
     */
    protected Object onBoxClose(ButtonItem clickedButton) {

        return null;
    }
}