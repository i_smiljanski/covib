package com.utd.gui.action;

import com.utd.gui.resources.I18N;

import javax.swing.*;
import java.util.Map;


public abstract class ActionHelp extends AbstractAction {

    public ActionHelp() {
        Map map = I18N.getActionMap(ActionHelp.class.getName());
        ActionUtil.initActionFromMap(this, map, null);
    }

}