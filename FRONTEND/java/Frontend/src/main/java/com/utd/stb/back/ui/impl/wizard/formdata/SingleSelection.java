package com.utd.stb.back.ui.impl.wizard.formdata;

import com.utd.stb.back.data.client.FormInfoRecord;
import com.utd.stb.back.data.client.SelectionList;
import com.utd.stb.back.data.client.SelectionRecord;
import com.utd.stb.back.database.Dispatcher;
import com.utd.stb.back.ui.inter.WizardFormData;
import com.utd.stb.inter.application.BackendException;
import com.utd.stb.inter.items.DisplayableItem;
import com.utd.stb.inter.items.DisplayableItemComboList;
import com.utd.stb.inter.items.DisplayableItems;
import com.utd.stb.inter.wizzard.formtypes.FormDataOneOutOfList;


/**
 * Handles data for the SingleSelection window
 *
 * @author rainer bruns
 */
public class SingleSelection implements FormDataOneOutOfList, WizardFormData {

    private SelectionList data;
    private Dispatcher dispatcher;
    private SelectionRecord selected;
    private SelectionList dataToSave;
    private SingleSelectionList list;
    private FormInfoRecord formInfo;

    /**
     * get the selected item from lst and initialises SingleSelectionList
     *
     * @param dispatcher the dispatcher
     * @param data       list of valid data
     * @param formInfo   record with form information, used for the label
     * @throws BackendException
     * @see Dispatcher
     * @see FormInfoRecord
     */
    public SingleSelection(Dispatcher dispatcher, SelectionList data, FormInfoRecord formInfo)
            throws BackendException {

        this.data = data;
        this.dispatcher = dispatcher;
        this.formInfo = formInfo;
        getSelectedData();
        list = new SingleSelectionList();
    }

    /**
     * loops through list of available data, gets the selected one (if present)
     */
    private void getSelectedData() {

        data.initIterator();
        while (data.hasNext()) {
            SelectionRecord s = data.getNext();
            if (s.isSelected()) {
                selected = s;
                return;
            }
        }
    }

    /**
     * @return list of available items
     */
    private SelectionList getLovData() {

        return data;
    }

    /*
     * (non-Javadoc) saves the selected item
     * 
     * @see com.utd.stb.inter.wizzard.FormData#saveData()
     */
    public void saveData() throws BackendException {

        SelectionList dataToSave = new SelectionList();
        dataToSave.add(selected);
        dispatcher.save(null, dataToSave, false);
    }

    /*
     * (non-Javadoc) returns the SingleSelectionList
     * 
     * @see com.utd.stb.inter.wizzard.formtypes.FormDataOneOutOfList#getDisplayableItemComboList()
     */
    public DisplayableItemComboList getDisplayableItemComboList() {

        return list;
    }

    /*
     * (non-Javadoc) label for use in GUI
     * 
     * @see com.utd.stb.inter.wizzard.formtypes.FormDataOneOutOfList#getLabel()
     */
    public String getLabel() {

        return formInfo.getPrompt1();
    }

    /* (non-Javadoc)
     * @see com.utd.stb.back.ui.inter.WizardFormData#getDataToSave()
     */
    public SelectionList getDataToSave() {

        SelectionList dataToSave = new SelectionList();
        dataToSave.add(selected);
        return dataToSave;
    }

    /**
     * implementation that is used by SingleItemChooser. Implements the methods
     * for getting and setting the selected item, returning the list of
     * available datas.
     *
     * @author rainer bruns
     * @see SingleItemChooser
     */
    private class SingleSelectionList implements DisplayableItemComboList {

        public DisplayableItem getSelectedItem() {

            return selected;
        }

        public DisplayableItems getAvailableItems() {

            return getLovData();
        }

        public boolean isEmptySelectionAllowed() {

            return false;
        }

        public void setSelectedItem(DisplayableItem item) {

            selected = (SelectionRecord) item;
        }// no check for demo

        public boolean isReadOnly() {

            return false;
        }
    }

    /* (non-Javadoc)
     * @see com.utd.stb.back.ui.inter.WizardFormData#getEntity()
     */
    public String getEntity() {
        return formInfo.getEntity();
    }
}