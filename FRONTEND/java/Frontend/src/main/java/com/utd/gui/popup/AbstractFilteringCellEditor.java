package com.utd.gui.popup;

import com.utd.gui.action.ActionUtil;
import com.utd.gui.controls.RemoteNavigatonTool;
import com.utd.gui.controls.WindowGrip;
import com.utd.gui.event.DocumentDeferredChangeAdapter;
import com.utd.gui.filter.Filter;
import com.utd.gui.filter.FilterErrorMsgDisplay;
import com.utd.gui.filter.RegExpFilter;
import com.utd.gui.filter.ShowAllFilterAction;
import com.utd.gui.resources.I18N;
import com.uptodata.isr.gui.util.Colors;

import javax.swing.*;
import javax.swing.border.EmptyBorder;
import javax.swing.border.LineBorder;
import javax.swing.border.MatteBorder;
import javax.swing.event.CellEditorListener;
import javax.swing.event.ChangeEvent;
import javax.swing.event.DocumentEvent;
import javax.swing.text.Document;
import java.awt.*;
import java.awt.event.*;


/**
 * A Celleditor that appears like a combobox: a textfield that works as a filter
 * for a popup list, the list will automatically popup, so it is nearly all time
 * visible. <br>
 * <b>When using in a JTable: </b> <br>
 * Start typing changes cell without having focus. Solutions: <br>
 * {@link JTable#setSurrendersFocusOnKeystroke()}with true or <br>
 * {@link JTable#putClientProperty()}with
 * table.putClientProperty("JTable.autoStartsEdit", Boolean.FALSE) <br>
 * where the former is a good solution and the latter disables 'start editing by
 * typing'
 * <p>
 * <ul>
 * <li>popup always visible (rarely one must type any char)
 * <li>click on list item selects and copies into textfield
 * <li>Key [Enter] copies list-selected into textfield
 * </ul>
 * <p>
 * Note that some changes are made to the Action/Input maps of the textfield,
 * but there are no changes in the textfield's view (borders, colors...)
 * <p>
 * <p>
 * Much later Maybe, but now the standalone version works really ok, if we need
 * other editors or non Mandantory lists look at this again: <br>
 * <p>
 * remove the base celleditor, use this standalone ... (some methods like
 * celleditor) and create a CellEditor using this class as a delegate, give the
 * delegate to the constructor of the CellEditor so we can tweak this class to
 * work finest as standalone and for CellEditors. Will be lean and mean.
 * <p>
 * // TODOlater don't show popup if the filterfield is readonly / non editable
 *
 * @author PeterBuettner.de
 */

public abstract class AbstractFilteringCellEditor extends DefaultCellEditor {

    private JTextField textField;
    protected JComponent displayList;
    private ComboLikePopper popup;
    protected FilterErrorMsgDisplay filterMsg = new FilterErrorMsgDisplay(
            I18N.getString(AbstractFilteringCellEditor.class,
                    "filterMsg"));
    private RegExpFilter regExpFilter = new RegExpFilter(null);
    private PopupShowableScrollable pss = new MyPopupShowableScrollable();
    /**
     * little different behavior if in Table... or standalone in e.g. a panel
     */
    private final boolean standalone;

    boolean isNotNull;


    // ----------------------------------------------------------------------

    /**
     * @param textField
     * @param standalone true for use in JTable, false if you put the textFiled into a
     *                   Panel etc.
     */
    public AbstractFilteringCellEditor(final JTextField textField, final boolean standalone) {

        super(new JTextField());// use a dummy one, thrown away soon, we need
        // it to call super constructor?
        this.standalone = standalone;

        //......... Since the DefaultCellEditor is written 'silly' we correct
        // some things here ..........
        // we need to set the text from an object
        editorComponent = textField;
        // MCD, 26.Aug 2008, Grid editable
        if (!isPopupList()) {
            delegate = new EditorDelegate() {
                public void setValue(Object value) {
                    super.setValue(value);
                    String s = valueToString(value);
                    textField.setText(s == null || s.equals(com.utd.stb.back.data.client.StructMap.NoValueAvailable) ? "" : s);
                    textField.selectAll();
                    /*String s = valueToString( value );
                    textField.setTranslation( s == null ? "" : s );
	                // works on F2: invoking via Key, not on mouseclick
	                textField.selectAll();*/
                }

                public Object getCellEditorValue() {
                    return textField.getText();
                }
            };
        } else {
            delegate = new EditorDelegate() {
                public void setValue(Object value) {
//                    System.out.println("AbstractFilteringCellEditor: value = " + value+"; "+
//                    (value instanceof UserInputRecord ?((UserInputRecord)value).getData():""));
                    super.setValue(value);
                    String s = valueToString(value);
                    textField.setText(s == null ? "" : s);
                    textField.selectAll();// works on F2: invoking via Key, not on mouseclick
                }
            };
        }

        textField.addActionListener(delegate); // what was the sense of this?
        //..........................................................................
        this.textField = textField;
        setClickCountToStart(1);

        // ... add some listeners to the textfield, for open/close popup ......
        ShowHideManagingListener shml = new ShowHideManagingListener(pss);
        textField.addAncestorListener(shml);
        textField.addComponentListener(shml);

        // filter ENTER/ESCAPE, for the standalone thing , works on the popup,
        // nowhere else!
        KeyListener kl = new KeyListenerFilter(shml) {

            protected boolean reject(KeyEvent e) {

                int code = e.getKeyCode();
                if (code == KeyEvent.VK_ESCAPE) return true;
                if (code == KeyEvent.VK_ENTER) return true;
                if (code == KeyEvent.VK_F1) return true; // the help one, may
                // add others later

                // eat some 'dead' keys
                if (e.getKeyChar() == KeyEvent.CHAR_UNDEFINED
                        && (code == KeyEvent.VK_ALT || code == KeyEvent.VK_SHIFT
                        || code == KeyEvent.VK_CONTROL || code == KeyEvent.VK_CAPS_LOCK
                        || code == KeyEvent.VK_ALT_GRAPH || code == KeyEvent.VK_META))
                    return true;
                // eat Alt, Shift, ... alone, w/o a key

                return false;
            }
        };

        textField.addKeyListener(kl);
        textField.addMouseListener(shml);
        textField.addFocusListener(shml);
        
        /*
         * note: a bound prop of JTextComponent is 'document', so we may react
         * if the document of the JTextComponent is exchanged? (really rare,
         * don't do it now) if we do then: remove listener from doc
         */
        textField.getDocument().addDocumentListener(new DocumentDeferredChangeAdapter() {

            public void deferredChange(DocumentEvent e) {

                updateModel();
                pss.scrollEditorToVisible();
            }
        });

        // used to close popup on [Enter] especially on stand alone
        addCellEditorListener(new CellEditorListener() {

            public void editingCanceled(ChangeEvent e) {

                pss.popShow(false);
            }

            public void editingStopped(ChangeEvent e) {

                pss.popShow(false);
            }
        });

        // use selected with enter key:
        Action useAction = new AbstractAction() {

            public void actionPerformed(ActionEvent e) {

                if (!useCurrentSelectionIfPopupVisible() && standalone) {
                    // not visible and standalone => forward 'KeyStroke'
                    ActionUtil.invokeActionOfKeyStrokeInAncestor(getComponent(), KeyStroke
                            .getKeyStroke("ENTER"));
                }
            }
        };

        // 'released ENTER' doesn't works in table, "ENTER" is ok on standalone
        textField.getInputMap().put(KeyStroke.getKeyStroke("ENTER"), useAction);
        textField.getActionMap().put(useAction, useAction);

        if (standalone) tweakStandalone();

    }// -----------------------------------------------------

    /**
     * enhancements for stand-alone-editor
     */
    private void tweakStandalone() {

        final KeyStroke keyEscape = KeyStroke.getKeyStroke("ESCAPE");
        Action escapeAction = new AbstractAction() {

            public void actionPerformed(ActionEvent e) {

                boolean popUpWasHidden = popup == null || !popup.visible();
                cancelCellEditing();
                pss.popShow(false);
                if (popUpWasHidden)
                    // forward keyStroke to parent component
                    ActionUtil
                            .invokeActionOfKeyStrokeInAncestor(getComponent(),
                                    keyEscape);
            }
        };
        textField.getInputMap().put(keyEscape, escapeAction);
        textField.getActionMap().put(escapeAction, escapeAction);
        // ................................
        textField.addFocusListener(new FocusListener() {

            public void focusGained(FocusEvent e) {

            }

            public void focusLost(FocusEvent e) {

                if (e.isTemporary()) return;
                if (useCurrentSelectionIfPopupVisible()) return;// was set
                // now reset on focus lost.
                // use 'empty', this works different than in JTable
                if (isFilterEmpty())
                    delegate.setValue(null);
                else
                    delegate.setValue(getCellEditorValue());
                stopCellEditing();

            }
        });
    }// ----------------------------------------------

    /**
     * return true if value was set
     */
    private boolean useCurrentSelectionIfPopupVisible() {

        //	for standalones, don't selected by accident
        if (popup == null || !popup.visible()) return false;
        useCurrentListSelection();
        return true;

    }

    /**
     * Tests if there is nothting in the textfield that may define a filter
     *
     * @return
     */
    protected boolean isFilterEmpty() {

        Document document = textField.getDocument();
        return document == null || document.getLength() == 0;

    }

    /**
     * Create the displayList (JList, JTable,...) and return it, use filter to
     * filter your data, the filter will fire changeEvents if the text in the
     * textFiled changes, this is <b>not </b> called in the constructor, only on
     * demand
     *
     * @param filter
     * @return
     */
    protected abstract JComponent createList(Filter filter);

    /**
     * MCD, 26.Aug 2008, Grid editable
     * Has to return true if a popup list exists
     * Has to return false if the textfield is editable
     *
     * @return
     */
    protected abstract boolean isPopupList();

    /**
     * <b>Caution: </b> <br>
     * don't call in constructor, since we call abstract methods and subclassed
     * methods will be called before subclassconstructor is fully done=> fields
     * maybe not initialized!
     */
    private synchronized void ensurePopupCreated() {

        if (popup != null) return; // already created

        // ... the Combobox like list ..........................
        displayList = createList(regExpFilter);

        // ... add some actions and KeyStrokes to the textfield ......
        RemoteNavigatonTool.putAllActions(textField, displayList);
        // ...
        popup = new ComboLikePopper(textField, createPopUpContainer());
    }

    /**
     * Binds all the content together.
     *
     * @return
     */
    protected JComponent createPopUpContainer() {

        boolean statusOnTop = false; // note: to change this we need more
        // tweaking (layout,grip working different
        // if on top-right)

        final JLabel errLbl = filterMsg.getComponent();
        errLbl.setHorizontalAlignment(JLabel.LEADING);
        errLbl.setHorizontalTextPosition(JLabel.RIGHT);

        JScrollPane sp = new JScrollPane(displayList);
        sp.setBorder(BorderFactory.createEmptyBorder());
        sp.setFocusable(false);
        sp.getVerticalScrollBar().setFocusable(false);
        sp.getHorizontalScrollBar().setFocusable(false);

        final JPanel container = new JPanel(new BorderLayout());
        container.setBorder(new LineBorder(Colors.getUIWindowBorder()));
        container.setOpaque(true);
        container.setDoubleBuffered(true);
        container.setFocusable(false);

        // new with grip
        errLbl.setBorder(new EmptyBorder(1, 1, 1, 1));

        JPanel status = new JPanel(new BorderLayout());
        status.add(errLbl);
        WindowGrip grip = new WindowGrip();
        grip.setBorder(new EmptyBorder(0, 0, 1, 1));
        status.add(grip, BorderLayout.EAST);
        status.setBorder(new MatteBorder(statusOnTop ? 0 : 1, 0, statusOnTop ? 1 : 0, 0,
                Colors.getUIWindowBorder()));
        container.add(status, BorderLayout.SOUTH);

        // ..........................................
        Action showAll = new ShowAllFilterAction() {

            public void actionPerformed(ActionEvent e) {

                textField.setText(null);
            }
        };
        JButton button = new JButton(showAll);
        button.setMargin(new Insets(0, 1, 0, 1));
        status.add(button, BorderLayout.WEST);

        container.add(sp);

        /*
         * test (working, maybe add a invokeLater): status/label always on the
         * opposite side of textField container.addAncestorListener(new
         * AncestorListener(){ public void ancestorAdded(AncestorEvent e) {
         * Point ptE =null; Point ptT =null; try{ ptE =
         * errLbl.getLocationOnScreen(); ptT = textField.getLocationOnScreen(); }
         * catch (Exception ex){return;}// maybe not on screen, do nothing
         * boolean top = ptE.y <ptT.y; errLbl.setBorder(new CompoundBorder(new
         * MatteBorder(top?0:1,0,top?1:0,0,Colors.getUIWindowBorder()),new
         * EmptyBorder(1,1,1,1))); container.add(errLbl,
         * top?BorderLayout.NORTH:BorderLayout.SOUTH); } public void
         * ancestorMoved(AncestorEvent event) { } public void
         * ancestorRemoved(AncestorEvent event) { }});
         */

        /*
         * we need to nail the size, since the JTableHeader isn't in the
         * hierarchy early enough. So if the Popup is placed above the
         * textFiled, it would hide it (off by height of JTableHeader)
         */

        container.setPreferredSize(container.getPreferredSize());
        return container;
    }// -------------------------------------------------------------------

    /**
     * better call this only on startup if you use this as a standalone thingy,
     * but should work later too
     *
     * @param value
     */
    public void setValue(Object value) {

        delegate.setValue(value);

    }

    /**
     * return the selected 'thing', the textFields text will be set to the
     * valueToString(returned), which most probably filters the list to this
     * single item (or if there are more with this name, well...) This will stop
     * editing, and null return does nothing.
     *
     * @return
     */
    protected abstract Object getCurrentListSelection();

    /**
     * e.g. the selected Object is translated and the result shown in the
     * textfield, default returns null or toString() <br>
     * <b>overwrite this! </b>
     *
     * @param value
     * @return
     */
    protected String valueToString(Object value) {

        return value == null ? null : value.toString();

    }

    /**
     * if something is selected, the text is copied into the textfield and
     * editing is stopped, calls {@link getCurrentListSelection()}
     */
    protected void useCurrentListSelection() {

        // calls textField.setTranslation() see above in constructor
        delegate.setValue(getCurrentListSelection());
        stopCellEditing();

    }

    private void updateModel() {

        // MCD, 30.Jan 2006, LOV
        if (textField.getText() == null || textField.getText().equals("")) {
            isNotNull = false;
        }

        // Beim Start ist isNotNull immer false, so dass die ganze LOV angezeigt wird
        if (isNotNull) {
            regExpFilter.setPattern(filterMsg.createPattern(textField.getText()));
        } else {
            regExpFilter.setPattern(null);
        }

        if (textField.getText() != null) isNotNull = true;
    }

    /**
     * make an inner class so we don't present the show/hide etc. to the outside
     */
    private class MyPopupShowableScrollable implements PopupShowableScrollable {

        public void popShow(boolean show) {

            if (!show && popup == null) return;// only create if needed
            if (!getComponent().isShowing() && popup == null) return;    // only create if needed

            if (show) {
                if (popup == null && !isPopupList()) return; // MCD 26.Aug 2008, Grid editable
                if (getComponent().getParent() == null) return;    // not in a window
                if (!getComponent().isFocusOwner()) return;    // better for stand-alone-editor
                ensurePopupCreated();
                if (popup.visible()) return;
                popup.show(getComponent());

            } else {

                ensurePopupCreated();
                popup.hide();

            }

        }

        public void scrollEditorToVisible() {

            Component editor = getComponent();
            JTable t = (JTable) SwingUtilities.getAncestorOfClass(JTable.class, editor);
            if (t == null) return;
            Point p = SwingUtilities.convertPoint(editor.getParent(), editor.getLocation(), t);
            t.scrollRectToVisible(new Rectangle(p, editor.getSize()));

        }

    }

    /**
     * Eats KeyEvents on demand.
     */
    private abstract static class KeyListenerFilter implements KeyListener {

        KeyListener k;

        KeyListenerFilter(KeyListener delegate) {

            this.k = delegate;

        }

        public void keyPressed(KeyEvent e) {

            if (!reject(e)) k.keyPressed(e);

        }

        public void keyReleased(KeyEvent e) {

            if (!reject(e)) k.keyReleased(e);

        }

        public void keyTyped(KeyEvent e) {

            if (!reject(e)) k.keyTyped(e);

        }

        protected abstract boolean reject(KeyEvent e);

    }

    public static void main(String[] args) {
        String msg = I18N.getString(AbstractFilteringCellEditor.class, "filterMsg");
        System.out.println("msg = " + msg);
        FilterErrorMsgDisplay filterMsg = new FilterErrorMsgDisplay(msg);
        System.out.println("filterMsg = " + filterMsg);

    }
}