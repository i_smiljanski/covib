package com.utd.gui.controls.table;

import com.utd.gui.event.EventTools;

import javax.swing.table.JTableHeader;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;


/**
 * Adds "autosize columns" for JTables by dblclick on tableheader. <br>
 * Usage: <br>
 * table.getTableHeader().addMouseListener(new AutoSizeMouseAdapter()); <br>
 * <br>
 * Button1_DblClick autosizes, Ctrl+Button1_DblClick autosizes all columns
 *
 * @author PeterBuettner.de
 */
public class AutoSizeMouseAdapter extends MouseAdapter {

    private boolean animate;

    /**
     * @param smoothAnimate
     */
    public AutoSizeMouseAdapter(boolean animate) {

        this.animate = animate;
    }

    /**
     * Default: with no animation.
     */
    public AutoSizeMouseAdapter() {

    }

    public void mouseClicked(MouseEvent e) {

        if (!EventTools.isLeftMouseDblClick(e)) return;

        if (!(e.getSource() instanceof JTableHeader)) return;
        JTableHeader header = (JTableHeader) e.getSource();

        // now we are shure: first button dblClick on a JTableHeader

        if (e.isControlDown())// not animate on 'resize all'
            TableTools.autoSizeColumns(header.getTable(), false);
        else {
            int column = TableTools.resizeColumnFromHeaderClick(header, e);
            if (column == -1) return;
            TableTools.autoSizeColumn(header.getTable(), column, animate);
        }

    }
}