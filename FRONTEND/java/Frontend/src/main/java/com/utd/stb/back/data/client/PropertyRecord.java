package com.utd.stb.back.data.client;

import com.utd.stb.StabberInfo;
import org.apache.commons.lang.StringUtils;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.text.ParseException;
import java.time.ZonedDateTime;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;


/**
 * Wraps database struct data. Has appropriate get/set methods for data from db
 * struct STB$PROPERTY$RECORD
 *
 * @author rainer bruns Created 27.08.2004
 */
public class PropertyRecord {
    public static final Logger log = LogManager.getRootLogger();

    private StructMap property;
    private String dateFormat;

    public PropertyRecord(StructMap s) {

        property = s;
    }

    /**
     * @return the value of underlying field SAVALUE as String
     */
    public String getValueAsString() {

        return property.getValueAsString("SVALUE");
    }

    public byte[] getBlobValueAsBytes() {

        return property.getBlobValueAsBytes("SBlobVALUE");
    }

    /**
     * Depending on on the result of getType, what is the value of field STYPE
     * creates the appropriate java object(Long, Boolean, Date..) and returns
     * it.
     *
     * @return an Object from underlying field SVALUE
     * @throws ParseException
     * @throws NumberFormatException
     */
    public Object getValue() throws NumberFormatException {

        switch (getType()) {
            case "NUMBER":
                return (long) property.getValueAsInt("SVALUE");
            case "BOOLEAN":
                return convertToBoolean(getValueAsString());
            case "DATE":
            case "DATETIME":
                return property.getValueAsDate("SVALUE");
            case "BLOB":
                return property.getBlobValueAsBytes("SBlobVALUE");
            default:
                return getValueAsString();
        }
    }

    /**
     * Depending on on the result of getType, what is the value of field STYPE
     * creates the appropriate java object(Long, Boolean, Date..) and returns
     * it.
     *
     * @return an Object from underlying field sDisplay
     * @throws ParseException
     * @throws NumberFormatException
     */
    public Object getDisplay() throws  NumberFormatException {

        if ("NUMBER".equals(getType())) {
            return (long) property.getValueAsInt("SDISPLAY");
        } else if ("BOOLEAN".equals(getType())) {
            return convertToBoolean(getDisplayAsString());
        } else if ("DATE".equals(getType())) {
            return property.getValueAsDate("SDISPLAY");
        } else {
            return getDisplayAsString();
        }
    }

    /**
     * @return the value of underlying field SPROMPTDISPLAY
     * @see StructMap#getValueAsString(String)
     */
    public String getPrompt() {

        return property.getValueAsString("SPROMPTDISPLAY");
        //return property.getValueFromType("SPROMPTDISPLAY").toString();
    }


    /**
     * @return the value of underlying field SDISPLAY
     * @see StructMap#getValueAsString(String)
     */
    public String getDisplayAsString() {

        return property.getValueAsString("SDISPLAY");
    }

    /**
     * Standard method StructMap.getValueAsBoolean is not used here, because the
     * default here(if field is null) is true.
     *
     * @return the value of underlying field SDISPLAYED interpreted as boolean
     */
    public boolean isDisplayed() {

        try {
//            System.out.println(getPrompt()+ " SDISPLAYED = " + property.getValue( "SDISPLAYED" )+"; "+property.getValueAsString( "SDISPLAY" ));
            return convertToBoolean(property.getValue("SDISPLAYED").toString());
        } catch (NullPointerException ne) {
            return true;
        }

    }

    /**
     * in StructMap is "T", else false.
     *
     * @return true if this property has a list. This is if underlieing field
     * SHASLOV
     * @see StructMap#getValueAsBoolean(String)
     */
    public boolean hasLov() {

        return property.getValueAsBoolean("HASLOV");
    }

    /**
     * Standard method StructMap.getValueAsBoolean is not used here, because the
     * default here(if field is null) is true. Return is false if value is "T",
     * true if value is "F"(Converted, GUI uses it own standards).
     *
     * @return the value of underlying field SREQUIRED interpreted as boolean
     */
    public boolean isEmptyAllowed() {

        try {
            return !convertToBoolean(property.getValue("SREQUIRED").toString());
        } catch (NullPointerException ne) {
            return false;
        }
    }


    /**
     * in StructMap is "F", else false.
     *
     * @return true if this property cannot be edited. This is if underlieing
     * field SEDITABLE
     * @see StructMap#getValueAsBoolean(String)
     */
    public boolean isReadOnly() {
        List<String> trueList = new ArrayList<>();
        trueList.add("T");
        trueList.add("C");     //changeable
        return !property.getValueAsBoolean("SEDITABLE", trueList);
    }

    /**
     * for LOVs: value can be various, not necessarily from LOV
     *
     * @return
     */
    public boolean isChangeable() {

        return property.getValueAsBoolean("SEDITABLE", "C");
    }

    /**
     * This is the value of underlying field STYPE(STRING, DATE, NUMBER...)
     *
     * @return the type of the value
     */
    public String getType() {
//        System.out.println("property type = " + property.getValueAsString( "TYPE" )+
//        " getParameterName: "+ getParameterName()+ " "+Thread.currentThread().getStackTrace()[2]);
        return property.getValueAsString("TYPE");
    }

    /**
     * @return the name of the parameter represented by this PropertyRecord
     */
    public String getParameterName() {

        return property.getValueAsString("SPARAMETER");
    }

    /**
     * This is the size from database definition, i.e. 2 for varchar(2)
     *
     * @return the size of field SDISPLAY
     */
    public int getSize() {

        return property.getFieldLength("SDISPLAY");
    }

    /**
     * Is a String like dd.MM.YYYY indicating a date format.
     *
     * @return the format of the parameter represented by this PropertyRecord
     */
    public String getFormat() {

        return property.getValueAsString("FORMAT");
    }

    /**
     * Check if the property record has the focus
     *
     * @return true if this property has the focus.
     * This is if underlieing field SFOCUS == "T"
     */
    public boolean getFocus() {

        return property.getValueAsBoolean("SFOCUS");
    }

    /**
     * Set the focus of this property record
     */
    public void setFocus(boolean focus) {

        property.setValue("SFOCUS", focus ? "T" : "F");
    }

    /**
     * @param s
     * @return the boolean interpretation of s
     */
    public boolean convertToBoolean(String s) {

        try {
            if (s.equals("T")) {
                return true;
            }
        } catch (NullPointerException ne) {
            ne.printStackTrace();
        }
        return false;
    }

    /**
     * sets the value of field SVALUE to the specified value
     *
     * @param val the value to set
     */
    public void setValue(Object val) {
        property.setValue("SVALUE", val);
    }

    /**
     * neu MCD 03.05.05 sets the value of field SDISPLAY to the specified value
     *
     * @param val the value to set
     */
    public void setDisplayValue(Object val) {

        property.setValue("SDISPLAY", val);
    }


    /**
     * sets the value of field SPARAMETERname to the specified value
     *
     * @param val the value to set
     */
    public void setParameterName(Object val) {

        property.setValue("SPARAMETER", val);
    }

    /**
     * sets the value of field SVALUE to "T" if bool is true, else to "F"
     *
     * @param bool
     */
    public void setBooleanValue(Boolean bool) {

        if (bool) {
            setValue("T");
        } else {
            setValue("F");
        }
    }

    /**
     * sets the value of field SVALUE to the string representation of date
     *
     * @param date
     * @see StructMap#setDateValue(String, Date)
     */
    public void setDateValue(ZonedDateTime date) {
        if (StringUtils.isEmpty(dateFormat)) {
            dateFormat = StabberInfo.getDatePattern();
        }
        DateTimeFormatter dateFormatter = DateTimeFormatter.ofPattern(dateFormat);
        String dateString = (date != null) ? date.format(dateFormatter) : null;
//        log.debug(dateString);
        property.setValue("SVALUE", dateString);
    }

    public void setBlobValue(byte[] bytes) {

        property.setValue("SBLOBVALUE", bytes);
    }

    /**
     * used when writing to db
     *
     * @return the underlying data
     */
    public StructMap getStruct() {

        return property;
    }

    @Override
    public String toString() {
        return "PropertyRecord{" +
                "getDisplayAsString=" + getDisplayAsString() +
                ", getType=" + getType() +
                ", getParameterName=" + getParameterName() +
                ", getValueAsString=" + getValueAsString() +
                '}';
    }
}