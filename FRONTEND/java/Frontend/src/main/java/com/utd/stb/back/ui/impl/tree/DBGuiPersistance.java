package com.utd.stb.back.ui.impl.tree;

import com.utd.stb.inter.application.GuiPersistance;

import java.io.ByteArrayOutputStream;
import java.util.*;


/**
 * Holds different data of GUI information(elements size..). Is loaded on
 * startup from DB if available for user. Data are cached during application run
 * and stored to DB on shutdown
 *
 * @author rainer bruns Created 13.12.2004
 */
public class DBGuiPersistance implements GuiPersistance {

    private static final char MARKER = '$';

    private Map map = new HashMap();

    /*
     * (non-Javadoc) sores information for element identified by key in map
     * 
     * @see com.utd.stb.inter.application.GuiPersistance#putGuiPersistenceData(java.lang.Object,
     *      java.lang.String, byte[])
     */
    public void putGuiPersistenceData(Object key, String context, byte[] data) {

        String k = key == null ? "" : key.toString();

        k += context == null ? "$$" : "$$" + context;
        map.put(k, data);
    }

    /*
     * (non-Javadoc) gets data for element identified by key from map
     * 
     * @see com.utd.stb.inter.application.GuiPersistance#getGuiPersistenceData(java.lang.Object,
     *      java.lang.String)
     */
    public byte[] getGuiPersistenceData(Object key, String context) {

        String k = key == null ? "" : key.toString();
        k += context == null ? "$$" : "$$" + context;
        return (byte[]) map.get(k);
    }

    /**
     * @return the list of data for several elements as properties. Stored to DB
     * as CLOB
     * @see com.utd.stb.back.database.DBInterface
     */
    public Properties save() {

        Properties prop = new Properties();
        for (Iterator i = map.entrySet().iterator(); i.hasNext(); ) {
            Map.Entry e = (Map.Entry) i.next();
            prop.setProperty((String) e.getKey(), convert((byte[]) e.getValue()));
        }
        return prop;
    }

    /**
     * initiales the cache map. Data are queried from DB
     *
     * @param prop list of GUI data as properties
     * @see com.utd.stb.back.database.DBInterface
     */
    public void load(Properties prop) {

        Enumeration e = prop.propertyNames();
        while (e.hasMoreElements()) {
            String name = (String) e.nextElement();
            map.put(name, convert(prop.getProperty(name)));
        }
    }

    /**
     * char/byte "32..127 but not [MARKER]" stay as they are, others are
     * converted to [MARKER]XX; where XX is the hex representation of that byte,
     * e.g.'$3f;'
     *
     * @param data
     * @return
     */
    private String convert(byte[] data) {

        if (data == null) return null;

        StringBuffer sb = new StringBuffer(data.length * 110 / 100);

        for (int i = 0; i < data.length; i++) {
            char c = (char) data[i];
            if (' ' <= c && c <= 127 && c != MARKER) {
                sb.append(c);
                continue;
            }
            sb.append(MARKER);
            String s = Integer.toHexString(data[i] & 0xff); // no 0x...
            if (s.length() < 2) sb.append('0');
            sb.append(s);
            sb.append(';');
        }
        return sb.toString();
    }

    /**
     * inverts {@link #convert(byte[])}
     *
     * @param in
     * @return
     */
    private byte[] convert(String in) {

        if (in == null) return null;

        ByteArrayOutputStream baos = new ByteArrayOutputStream(in.length() * 110 / 100);
        char[] cc = in.toCharArray();

        for (int i = 0; i < cc.length; i++) {
            char c = cc[i];
            if (' ' <= c && c <= 127 && c != MARKER) {
                baos.write(c & 0xff);
                continue;
            }
            if (c != MARKER)
                throw new IllegalArgumentException("marker char != '" + MARKER
                        + "'");
            char c1 = cc[++i];
            char c2 = cc[++i];
            if (cc[++i] != ';')
                throw new IllegalArgumentException("end marker char != ';'");

            baos.write((byte) Integer.parseInt("" + c1 + c2, 16));
        }

        return baos.toByteArray();
    }

}