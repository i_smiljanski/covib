package com.utd.stb.inter.userinput;

/**
 * Container for <code>UserInput</code>
 *
 * @author PeterBuettner.de
 */

public interface UserInputs {

    /**
     * Number of items
     *
     * @return
     */
    int getSize();

    /**
     * Get Item at index
     *
     * @param index
     * @return
     */
    UserInput get(int index);

    /**
     * Get the index of the item, -1 if not in list
     *
     * @param item
     * @return
     */
    int indexOf(UserInput item);

}