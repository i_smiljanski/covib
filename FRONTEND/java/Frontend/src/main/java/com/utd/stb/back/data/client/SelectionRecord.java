package com.utd.stb.back.data.client;

import com.utd.stb.back.database.DBInterface;
import com.utd.stb.inter.items.DisplayableItem;

import java.util.ArrayList;


/**
 * Wraps database struct data. Has appropriate get/set methods for data from db
 * struct STB$SELECTION$RECORD
 *
 * @author rainer bruns Created 27.08.2004
 */
public class SelectionRecord implements DisplayableItem {

    StructMap selectionData;

    /**
     * This constructor is used in Grid, to create dummy data for missing fields
     * in db data need those fields for table.
     */
    public SelectionRecord() { // Dummy, nur for den Grid wenn Daten aus Backend
        // fehlen
        DatabaseFieldMap dummy;

        dummy = new DatabaseFieldMap("SDISPLAY", "java.lang.String", 12, 255, "VARCHAR", null);
        selectionData = new StructMap("STBOWNER.STB$OSELECTION$RECORD");
        selectionData.put("SDISPLAY", dummy);

        dummy = new DatabaseFieldMap("SVALUE", "java.lang.String", 12, 255, "VARCHAR", null);
        selectionData.put("SINFO", dummy);

        dummy = new DatabaseFieldMap("SVALUE", "java.lang.String", 12, 255, "VARCHAR", null);
        //selectionData = new StructMap( "STBOWNER.STB$OSELECTION$RECORD" );
        selectionData.put("SVALUE", dummy);
    }

    /**
     * @param s the StructMap with the data for this Record
     */
    public SelectionRecord(StructMap s) {

        selectionData = s;

        ArrayList array = ((ArrayMap) selectionData.getValue("TLOATTRIBUTELIST")).getArray();
        AttributeList attributes = new AttributeList();
        for (Object anArray : array) {
            AttributeRecord attribute = new AttributeRecord((StructMap) anArray);
            attributes.add(attribute);
        }
        setAttributeList(attributes);

    }

    public String toString() {

        return selectionData.toString();
    }

    /**
     * @return the value of underlying field SVALUE
     * @see StructMap#getValueAsString(String)
     */
    public String getValue() {

        return selectionData.getValueAsString("SVALUE");
    }

    /**
     * @return the value of underlying field SDISPLAY
     * @see StructMap#getValueAsString(String)
     */
    public String getDisplay() {

        return selectionData.getValueAsString("SDISPLAY");
    }

    /**
     * @return the value of underlying field SINFO
     * @see StructMap#getValueAsString(String)
     */
    public String getInfo() {

        return selectionData.getValueAsString("SINFO");
    }

    /**
     * @return the converted value of underlying field SSELECTED, true if value
     * is "T"
     * @see StructMap#getValueAsBoolean(String)
     */
    public boolean isSelected() {

        return selectionData.getValueAsBoolean("SSELECTED");
    }

    /**
     * @return the value of underlying field NORDERNUM
     * @see StructMap#getValueAsInt(String)
     */
    public int getOrderNum() {

        return selectionData.getValueAsInt("NORDERNUM");
    }

    /**
     * @return the value of underlying field SMASTERKEY
     */
    public String getMasterKey() {

        return selectionData.getValueAsString("SMASTERKEY");
    }

    /**
     * @return the value of underlying field TLOATTRIBUTELIST
     */
    /* (non-Javadoc)
     * @see com.utd.stb.inter.items.DisplayableItem#getAttributeList()
     */
    public Object getAttributeList() {
        if (selectionData.getValue("TLOATTRIBUTELIST") instanceof AttributeList)
            return selectionData.getValue("TLOATTRIBUTELIST");
        else
            return null;
    }

    /**
     * sets the value of underlying field TLOATTRIBUTELIST
     *
     * @param attrList the attributeList to be set
     */
    public void setAttributeList(AttributeList attrList) {

        selectionData.setValue("TLOATTRIBUTELIST", attrList);
    }

    /**
     * sets the vale of underlying field SSELECTED
     *
     * @param s "T" or "F"
     */
    public void setSelected(String s) {

        selectionData.setValue("SSELECTED", s);
    }

    /**
     * sets the value of field NORDERNUM to i
     *
     * @param i order number of this record
     */
    public void setOrderNum(int i) {

        selectionData.setValue("NORDERNUM", new Integer(i).toString());
    }

    /**
     * sets the value of field SVALUE
     *
     * @param s the new value
     */
    public void setValue(String s) {

        selectionData.setValue("SVALUE", s);
    }

    /**
     * sets the value of field SDISPLAY
     *
     * @param s the new value
     */
    public void setDisplay(String s) {

        selectionData.setValue("SDISPLAY", s);
    }

    /**
     * @return the value of underlying field SMASTERKEY
     */
    public void setMasterKey(String s) {

        selectionData.setValue("SMASTERKEY", s);
    }


    public void setInfo(String s) {

        selectionData.setValue("SINFO", s);
    }

    /*
     * (non-Javadoc)
     * 
     * @see com.utd.stb.inter.items.DisplayableItem#getData() GUI means data for
     *      display
     */
    public Object getData() {

        return selectionData.getValue("SDISPLAY");
    }

    /**
     * used when writing to db
     *
     * @return the underlying data
     * @see DBInterface#putList(String, String, SelectionList)
     */
    public StructMap getStruct() {

        return selectionData;
    }

    /**
     * prints an XML Representation of this record at element el in document doc
     *
     * @param doc
     * @param el
     */
    /*public void printXML( XMLDocument doc, XMLElement el ) {

        selectionData.printXML( doc, el );
    }*/

}

