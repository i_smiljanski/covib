package com.utd.stb.inter.action;

/**
 * Things the Gui should do with a window, or useractions like 'want close'
 * (pressing the Windowmanager [x] button in the titlebar). These Actions are
 * executed only in dialogs, the main window doesn't react.
 *
 * @author PeterBuettner.de
 */
public interface WindowAction {

    /**
     * user likes to close window (e.g. pressed [x] in the titlebar, or maybe
     * pressed the 'Escape' Key
     */
    public static final String WANT_CLOSE = "WantClose";

    /**
     * Gui has closed the window
     */
    public static final String WINDOW_CLOSED = "WindowClosed";

    /**
     * Gui has saved the data in the Dialog
     */
    public static final String DATA_SAVED = "DataSaved";
    /**
     * Gui has saved the data in the Dialog and closed the window
     */
    public static final String DATA_SAVED_AND_WINDOW_CLOSED = "DataSavedAndWindowClosed";

    /**
     * Gui has closed the window 'cause of the user inactive timeout, return
     * value is ignored, and the window is closed without any action, of course
     * the window is already hidden this message is send.
     */
    public static final Object TIMEOUT_WINDOW_CLOSED = "TimeoutWindowClosed";

    /**
     * Gui has closed the dialog 'cause of an Exception (send from the Backend),
     * return value is ignored, and the window is closed without any action, the
     * window is already hidden when this message is send. All other dialogs are
     * also closed this way, but not the main window.
     * <p>
     * <b>Don't send this from the backend to the gui, </b> it is just to inform
     * the backend and tell the parent windows what to do!
     */
    public static final Object EXCEPTION_ABORT_ACTION = "ExceptionAbortAction";

    /**
     * Gui has closed the dialog 'cause of an Exception (send from the Backend),
     * return value is ignored, and the window is closed without any action, the
     * window is already hidden when this message is send. All other dialogs are
     * also closed this way.
     * <p>
     * After the whole action is cleaned up the application exits.
     * <p>
     * <b>Don't send this from the backend to the gui, </b> it is just to inform
     * the backend and tell the parent windows what to do!
     */
    public static final String EXCEPTION_ABORT_APPLICATION = "ExceptionAbortApplication";

    /**
     * Gui should close the window
     */
    public static final String CLOSE_WINDOW = "CloseWindow";

    public static final String NOT_CLOSE_WINDOW = "NotCloseWindow";

    /**
     * Gui should save the data in the Dialog
     */
    public static final String SAVE_DATA = "SaveData";

    /**
     * Gui should save the data in the Dialog and close the window
     */
    public static final String SAVE_DATA_AND_CLOSE_WINDOW = "SaveDataAndCloseWindow";

    /**
     * What to do:
     * <p>
     * <pre>
     *
     *  WindowAction.WANT_CLOSE
     *  WindowAction.CLOSE_WINDOW
     *  WindowAction.WINDOW_CLOSED
     *  WindowAction.SAVE_DATA
     *  WindowAction.DATA_SAVED
     *
     *  WindowAction.TIMEOUT_WINDOW_CLOSED
     *
     * </pre>
     *
     * @return
     */
    Object getType();

}