package com.utd.gui.event;

import javax.swing.*;
import java.awt.*;
import java.awt.event.AdjustmentEvent;
import java.awt.event.AdjustmentListener;


/**
 * Auto en-/disables ScrollBars, etc (Adjustable) if the visible amount is less
 * than max-min. In some Look and feels this may be default, not on windows
 * L&amp;F <br>
 * <b>Usage: <br>
 * </b> sp.getVerticalScrollBar().addAdjustmentListener(new
 * AdjustableEnabler());
 *
 * @author PeterBuettner.de
 */
public class AdjustableEnabler implements AdjustmentListener {

    public void adjustmentValueChanged(AdjustmentEvent e) {

        Adjustable a = e.getAdjustable();

        if (!(a instanceof JComponent)) return;

        ((JComponent) a)
                .setEnabled(a.getVisibleAmount() < (a.getMaximum() - a.getMinimum()));

    }

}