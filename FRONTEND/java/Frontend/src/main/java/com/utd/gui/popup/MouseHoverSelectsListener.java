package com.utd.gui.popup;

import javax.swing.*;
import java.awt.*;
import java.awt.event.MouseEvent;
import java.awt.event.MouseMotionAdapter;


/**
 * A listener that auto selects the item in a JList/JTable if the mouse hovers
 * over, used for selfmade combolike PopUps, maybe enhanced to handle JTree.
 * <br>
 * <b>Usage: </b> <br>
 * list.addMouseMotionListener(new MouseHoverSelectsListener());
 *
 * @author PeterBuettner.de
 */
public class MouseHoverSelectsListener extends MouseMotionAdapter {

    public void mouseMoved(MouseEvent e) {

        Component target = e.getComponent();
        if (target instanceof JList)
            doSelection(e, (JList) target);
        else if (target instanceof JTable) doSelection(e, (JTable) target);

    }

    protected void doSelection(MouseEvent e, JList list) {

        int i = list.locationToIndex(e.getPoint());
        if (i < 0) return;
        list.setSelectedIndex(i);
    }

    private void doSelection(MouseEvent e, JTable table) {

        int i = table.rowAtPoint(e.getPoint());
        if (i < 0) return;
        table.getSelectionModel().setSelectionInterval(i, i);
    }

}