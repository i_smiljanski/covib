package com.utd.stb.gui.userinput.swing;

import com.jgoodies.forms.builder.DefaultFormBuilder;
import com.jgoodies.forms.factories.Borders;
import com.jgoodies.forms.layout.FormLayout;
import com.uptodata.isr.gui.util.Colors;
import com.uptodata.isr.utils.exceptions.MessageStackException;
import com.utd.gui.util.DialogUtils;
import com.utd.stb.gui.swingApp.exceptions.ExceptionDialog;
import com.utd.stb.inter.userinput.MetaInput;
import com.utd.stb.inter.userinput.UserInput;
import com.utd.stb.inter.userinput.UserInputs;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import javax.swing.*;
import javax.swing.border.EmptyBorder;
import javax.swing.text.JTextComponent;
import java.awt.*;
import java.awt.event.FocusAdapter;
import java.awt.event.FocusEvent;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;


/**
 * transform a collection of UserInputs into a Panel containing the generated
 * JComponents
 *
 * @author PeterBuettner.de
 */
public class UIOPanelFactory {
    public static final Logger log = LogManager.getRootLogger();

    /**
     * add one tab and connects a listener to change tab color on for
     * data-validation
     */
    private static void addTab(String label, JTabbedPane tabs, JComponent content) {

        tabs.addTab(label, content);
        new TabDataChangeListener(tabs, content);
    }

    /**
     * creates a _panel with length# of ui-controls from start-index in userInput
     * it is always wrapped in a scrollpane
     *
     * @param userInput
     * @param start     first index
     * @param length    number of entries to be used
     * @return
     */
    private static JComponent createPanel(UserInput[] userInput, int start, int length) {
        try {
            FormLayout layout = new FormLayout("r:m,2dlu,m:g", "");

            final DefaultFormBuilder builder = DialogUtils.getFormBuilder(layout);
            log.debug("   column  " + layout.getColumnCount() + "   " + (start + length));
            final JScrollPane scrollPane = new JScrollPane();
//        FocusListener fl = new ComponentsFocusListener();
            for (int i = start; i < start + length; i++) {
                final UserInput ui = userInput[i];
                if (!ui.isVisible()) continue;
                // JScrollPane is not focussable, so:
                JComponent row = buildRow(builder, ui);
                JComponent c = getScrollPanedComponent(row);
            }
            Dimension dimension = builder.getPanel().getPreferredSize();
            builder.getPanel().setPreferredSize(new Dimension((int) dimension.getWidth(), (int) dimension.getHeight()));

            int maxHeight = 2 * Toolkit.getDefaultToolkit().getScreenSize().height / 3;
            if (builder.getPanel().getPreferredSize().getHeight() > maxHeight) {
                scrollPane.setPreferredSize(new Dimension(scrollPane.getPreferredSize().width, maxHeight));
                scrollPane.setHorizontalScrollBarPolicy(ScrollPaneConstants.HORIZONTAL_SCROLLBAR_NEVER);
            }

            scrollPane.setViewportView(builder.getPanel());
            scrollPane.setViewportBorder(Borders.DIALOG_BORDER); // TODOlater is
            // DIALOG_BORDER
            // the right size
            // for an
            // 'internal' pane?
            scrollPane.setBorder(new EmptyBorder(0, 0, 0, 0));// is a line
            // border
            // per default
            return scrollPane;
        } catch (Exception e) {
            log.error(e);
            ExceptionDialog.showExceptionDialog(null, e, null, false, 0);
            return null;
        }
    }// -----------------------------------------------------------------------------

    /**
     * if c is a scrollpane get the ViewComponent (Child of ViewPort) of the
     * Scrollpane, else return c, c maybe null on input/output
     *
     * @param c
     * @return
     */
    private static JComponent getScrollPanedComponent(JComponent c) {

        if (c instanceof JScrollPane) {
            return (JComponent) ((JScrollPane) c).getViewport().getView();
        }
        return c;

    }

    /**
     * build one row in the _panel, can handle only 'single column'
     * (label+component=1column) for now
     *
     * @param builder
     * @param ui
     * @return the added component, or null if it is a nonfocussable (like a
     * separator) maybe a surrounding JScrollpane is returned!
     */
    private static JComponent buildRow(DefaultFormBuilder builder, UserInput ui) throws MessageStackException {

        Class type = ui.getType();

        // we create Titled Groups for all MetaInput, even unknown,
        // so we are (backwards?) compatible if we create new types in the
        // future
        if (MetaInput.class.isAssignableFrom(type)) {

            MetaInput mi = (MetaInput) ui.getData();
            if (mi != null) {
                String miType = mi.getType();
                if (MetaInput.FLOATING_TEXT.equals(miType)) {

                    JTextArea label = new JTextArea(ui.getLabel());
                    label.setSize(400, 10);// we must set an initial width to get
                    // any meaningfull values from
                    // getPreferredSize()!!!11!elf
                    label.setFont(UIManager.getFont("EditorPane.font"));
                    label.setLineWrap(true);
                    label.setWrapStyleWord(true);
                    label.setEditable(false);
                    label.setOpaque(false);

                    builder.append(label, 3);
                    builder.nextLine();

                    return null;

                }

                if (MetaInput.HTML_TEXT.equals(miType)) {

                    JEditorPane label = new JEditorPane("text/html", ui.getLabel()) {

                        //don't know, but here we need this, so that the label
                        // won't grow _really_ wide
                        public Dimension getPreferredSize() {

                            return getMinimumSize();
                        }
                    };

                    label.setEditable(false);
                    label.setOpaque(false);

                    builder.append(label, 3);
                    builder.nextLine();
                    return null;

                }
            }

            //builder.append(comp,3);
            builder.appendSeparator(ui.getLabel());
            builder.nextLine();
            return null;

        }// .................................................

        JComponent comp = UIOComponentFactory.createComponent(ui);

        // checkbox, we need the text there (not in label) to show focus,
        // but if readOnly the checkbox-text is shadowed (difficult to read)
        // so build a label then!
        if (comp instanceof JCheckBox && !ui.isReadOnly()) {

            // empty label, and put into 2nd column
            builder.append("", comp);
            builder.nextLine();
            return comp;

        }// .................................................

        // readonly checkbox, would be bad to read (shaded disabled text:-(
        if (comp instanceof JCheckBox) ((JCheckBox) comp).setText(null);

        // create lables here since Forms uses '&' as a pointer to the Mnemonic
        JLabel lbl = new JLabel(ui.getLabel());
        lbl.setLabelFor(comp);

        // wrap some into ScrollPane
        if (comp instanceof JTextComponent && !(comp instanceof JTextField)) {

            comp = new JScrollPane(comp);
            // and tweak this
            lbl.setVerticalAlignment(JLabel.TOP);// top align to component
            lbl.setBorder(Borders.createEmptyBorder("1dlu,0,0,0"));//'align
            // label and
            // text'
            // we need special rows:
            builder.appendRelatedComponentsGapRow();
            builder.appendRow("F:P:G");// let it fill and grow
            builder.nextLine();

        }

        builder.append(lbl, comp);
        builder.nextLine();

        return comp;

    }

    /**
     * Creates a _panel containing all the components for userInput, can create
     * TabSheets and Titled Groups
     *
     * @param userInputs
     * @return
     */
    public JComponent createPanel(UserInputs userInputs) {

        UserInput[] ui = new UserInput[userInputs.getSize()];
        for (int i = 0; i < ui.length; i++) {
            ui[i] = userInputs.get(i);
        }
        return createPanel(ui);

    }// ------------------------------------------------------------

    /**
     * Creates a _panel containing all the components for userInput, can create
     * TabSheets and Titled Groups
     *
     * @param userInput
     * @return
     */
    public JComponent createPanel(UserInput[] userInput) {

        // any Tabs here?
        int[] tabStarts = findTabStartIndices(userInput);
        log.debug(tabStarts.length + " " + userInput.length);
        // no? so put all in one Panel
        if (!(tabStarts.length > 0)) return createPanel(userInput, 0, userInput.length);

        // ... build tabs ...................
        JTabbedPane tabs = new JTabbedPane();

        // doesn't starts with a tab, insert a default one:
        int firstIdx = tabStarts[0];
//        if (firstIdx != 0) addTab("Unnamed", tabs, createPanel(userInput, 0, firstIdx));

        // other tabs:
        int lastIdx = firstIdx;
        for (int i = 0; i < tabStarts.length; i++) {

            int next = (i + 1 < tabStarts.length) ? tabStarts[i + 1] : userInput.length;

            //only those with more than 0 components
            if (next - lastIdx > 1) {
                addTab(userInput[lastIdx].getLabel(), tabs, createPanel(userInput, lastIdx + 1, next - lastIdx - 1));
            }

            lastIdx = next;
        }

        return tabs;

    }// -----------------------------------------------------------

    /**
     * finds all indices where a new tab should start,
     *
     * @param userInput
     * @return always an array, but maybe with length=0
     */
    private int[] findTabStartIndices(UserInput[] userInput) {

        List<Integer> list = new ArrayList<>();

        log.debug("userInput.length = " + userInput.length);
        for (int i = 0; i < userInput.length; i++) {
            UserInput ui = userInput[i];
            if (MetaInput.class.isAssignableFrom(ui.getType())) {
//                log.debug(i + " " + ui.getLabel() + " " + ui.isVisible() );
                if (ui.getData() != null && ui.isVisible() && ((MetaInput) ui.getData()).getType().equals(MetaInput.TABSHEET)) {
                    list.add(i);
                }
            }
        }

        int[] tabStarts = new int[list.size()];

        for (int j = 0; j < tabStarts.length; j++) {
            tabStarts[j] = list.get(j);
        }
        return tabStarts;

    }// --------------------------------------------------------------

    static private final class TabDataChangeListener extends CollectiveDataHandler implements DataChangeListener {

        private JTabbedPane pane;

        public TabDataChangeListener(JTabbedPane pane, JComponent content) {

            super(content);
            this.pane = pane;
            collectComponents();
        }


        public void dataChanged(DataChangeEvent event) {

            int i = pane.indexOfComponent(container);
            Color color = null;// null sets default on TabbedPane
            if (!isValid()) color = Colors.getDataBackInvalid();
            else if (isEmpty()) color = Colors.getDataBackIllegalEmpty();
            pane.setBackgroundAt(i, color);
        }

        /**
         * call after content is populated with UserInput aware JComponents,
         * most times only called once
         */
        protected void collectComponents() {

            // remove old (most times iterator is empty, since we probably
            // collect only once)
            for (Iterator i = handlerIterator(); i.hasNext(); )
                ((UIODataHandler) i.next()).removeChangeListener(this);

            // add new
            super.collectComponents();
            for (Iterator i = handlerIterator(); i.hasNext(); )
                ((UIODataHandler) i.next()).addChangeListener(this);
            dataChanged(null);

        }

    }

    /**
     * Use an instance of this to have Components that are childs of a
     * JComponent to be scrolled into view the moment they become focussed.
     * <p>
     * MCD, 21.Jun 2006, class extended for further use
     * if a component looses the focus, an action to check the components on the dialog is sent
     */
    static private class ComponentsFocusListener extends FocusAdapter {

        private static void scroll(Component parent, Component child) {

            if (parent instanceof JComponent) ((JComponent) parent).scrollRectToVisible(child.getBounds());

        }

        private Component getActionDialog(Component c) {
            if (c.getParent() == null || c instanceof ActionDialog) return c;
            return getActionDialog(c.getParent());
        }

        public void focusLost(FocusEvent e) {

            Component c = e.getComponent();
            if (c == null) return;

            Component dialog = getActionDialog(c);
            if (dialog != null && dialog instanceof ActionDialog) {

                Component co = e.getOppositeComponent();
                if (co != null && co instanceof JComponent) {

                    Object h = ((JComponent) co).getClientProperty(UIODataHandler.class);

                    if (h != null && h instanceof UIODataHandler) {
                        UserInput ui = ((UIODataHandler) h).getUserInput();
//                        log.debug("ui = " + ui.getLabel());
                        ui.setFocus(true);
                    }

                    if (h != null && h instanceof UIODataHandler) {
                        UserInput ui = ((UIODataHandler) h).getUserInput();

                        ui.setFocus(false);
                    }
                    log.debug("focusLost c= " + c);
                    ((ActionDialog) dialog).performComponentAction();
                }
            }
        }

        public void focusGained(FocusEvent e) {

            if (e.isTemporary()) return;

            Component c = e.getComponent();
            if (c == null) return;

            Component p = c.getParent();
            if (p == null) return;

            if (p instanceof JViewport) {// we are the direct child of a
                // JScrollPane

                p = p.getParent();// JScrollPane
                if (p != null && p.getParent() != null) scroll(p.getParent(), p);
                return;

            }

            scroll(p, c);

        }
    }

}