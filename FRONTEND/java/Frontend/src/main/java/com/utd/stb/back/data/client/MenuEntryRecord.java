package com.utd.stb.back.data.client;

import com.utd.stb.back.database.DBInterface;

/**
 * Wraps database struct data. Has appropriate get/set methods for data from db
 * struct STB$MenuEntry$RECORD
 *
 * @author rainer bruns Created 27.08.2004
 */
public class MenuEntryRecord {

    StructMap selectionData;
    boolean enabled;
    boolean enabledQueried = false;

    /**
     * @param s the StructMap with the data for this Record
     */
    public MenuEntryRecord(StructMap s) {
        selectionData = s;
    }

    public String toString() {

        return selectionData.toString();
    }

    /**
     * Returns the value from underlying field STYPE
     *
     * @return the type of this MenuEntry(menu, item or seperator)
     * @see StructMap#getValueAsString(String)
     */
    private String getType() {

        return selectionData.getValueAsString("STYPE");
    }

    /**
     * Returns the value from underlying field SDISPLAY
     *
     * @return the display of this MenuEntry(menu, item or seperator)
     * @see StructMap#getValueAsString(String)
     */
    public String getDisplay() {

        return selectionData.getValueAsString("SDISPLAY");
    }

    /**
     * Returns the value from underlying field NID
     *
     * @return the id of this MenuEntry(menu, item or seperator)
     * @see StructMap#getValueAsInt(String)
     */
    public int getId() {

        return selectionData.getValueAsInt("NID");
    }

    /**
     * Returns the value from underlying field STOKEN(like CAN_RUN_REPORT)
     *
     * @return the token of this MenuEntry(menu, item or seperator)
     * @see StructMap#getValueAsString(String)
     */
    public String getToken() {

        return selectionData.getValueAsString("STOKEN");
    }

    /**
     * @return true if menu entry is a separator
     */
    public boolean isSeparator() {

        if (getType().equals("SEPARATOR")) {
            return true;
        }
        return false;
    }

    /**
     * @return true if this menu entry is a menu
     */
    public boolean isMenu() {

        if (getType().equals("DISPLAY")) {
            return true;
        }
        return false;
    }

    /**
     * Sets the enabled field to value of b
     *
     * @param b
     */
    public void setEnabled(boolean b) {

        enabled = b;
        enabledQueried = true;
    }

    /**
     * @return true if this menu entry is enabled
     */
    public boolean isEnabled() {

        return enabled;
    }

    /**
     * used when a MenuEntry is sent to database
     *
     * @return the underlying StructMap
     * @see DBInterface#callFunction(MenuEntryRecord, boolean)
     */
    public StructMap getStruct() {

        return selectionData;
    }

    /**
     * @return true if value of field SCONFIRM is "T"
     * @see StructMap#getValueAsBoolean(String)
     */
    public String isConfirmationNeeded() {

        return selectionData.getValueAsString("SCONFIRM"); //getValueAsBoolean("SCONFIRM");

    }

    public String getConfirm() {

        return selectionData.getValueAsString("SCONFIRM");
    }

    /**
     * Returns the value from underlying field SCONFIRMTEXT
     *
     * @return the confirm text of this MenuEntry
     */
    public String getConfirmText() {

        return selectionData.getValueAsString("SCONFIRMTEXT");
    }

    public String getIconId() {
        return selectionData.getValueAsString("SICON");
    }

    public String getHandledBy() {

        return selectionData.getValueAsString("SHANDLEDBY");
    }

    /**
     * sets the value of underlying fied SCONFIRM to "T" if parameter is true,
     * else "F"
     *
     * @param isNeeded
     */
    public void setConfirmationNeeded(boolean isNeeded) {

        if (isNeeded)
            selectionData.setValue("SCONFIRM", "T");
        else
            selectionData.setValue("SCONFIRM", "F");
    }

    /**
     * prints an XML representation of the data under element el in document doc
     */
   /* public void printXML( XMLDocument doc, XMLElement el ) {

        selectionData.printXML( doc, el );
    }*/
    public String print() {
        return "MenuEntryRecord{" +
                "token=" + getToken() +
                "; Id=" + getId() +
                "; type=" + getType() +
                "; confirm=" + getConfirm() +
                "; confirmText=" + getConfirmText() +
                "; display=" + getDisplay() +
                "; handleBy=" + getHandledBy() +
                "; iconId=" + getIconId() +
                '}';
    }
}

