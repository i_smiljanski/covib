package com.utd.stb.gui.userinput.swing;

import com.uptodata.isr.gui.util.Colors;
import com.utd.gui.controls.CenteredJSeparator;
import com.utd.stb.back.ui.impl.userinputs.fields.DateInputImpl;
import com.utd.stb.inter.userinput.DateInput;
import com.utd.stb.inter.userinput.MetaInput;
import com.utd.stb.inter.userinput.TextInput;
import com.utd.stb.inter.userinput.UserInput;

import javax.swing.*;
import javax.swing.border.EmptyBorder;
import javax.swing.border.LineBorder;
import javax.swing.table.TableCellRenderer;
import java.awt.*;
import java.text.Format;
import java.time.ZonedDateTime;
import java.time.format.DateTimeFormatter;
import java.time.format.FormatStyle;
import java.util.Locale;


/**
 * A TableCellRenderer that can display all UserInput types, on design it was
 * thought to mimic a 'Form' using spacers between cells and 'control' color as
 * background, but now it is only used to render the items in a standrad
 * Listviewlike table
 * <p>
 * TODOlater floating and html text, but this is not needed for now
 *
 * @author PeterBuettner.de
 */
public class UIOTableCellRenderer implements TableCellRenderer {

    // we use delegation:

    TableCellRenderer def = new BaseTableCellRenderer();


    public Component getTableCellRendererComponent(JTable table, Object value, boolean sel,
                                                   boolean foc, int row, int col) {

        if (!(value instanceof UserInput))
            return def.getTableCellRendererComponent(table,
                    value,
                    sel,
                    foc,
                    row,
                    col);

        UserInput userInput = (UserInput) value;
        Class type = userInput.getType();
        value = userInput.getData();
        Component c;

        if (!userInput.isVisible())
            // hiddene
            return def.getTableCellRendererComponent(table, "", sel,
                    foc, row, col);

        if (Boolean.class.isAssignableFrom(type)) {
            TableCellRenderer booleanRenderer = new BooleanRenderer();
            c = booleanRenderer.getTableCellRendererComponent(table, value, sel, foc, row,
                    col);
        }

        else if (Number.class.isAssignableFrom(type)) {
            TableCellRenderer numberRenderer = new DoubleRenderer();
            c = numberRenderer.getTableCellRendererComponent(table, value, sel, foc, row, col);
        }

        else if (DateInput.class.isAssignableFrom(type)) {
            TableCellRenderer dateInputRenderer = new DateInputRenderer();
            c = dateInputRenderer.getTableCellRendererComponent(table, value, sel, foc, row, col);
        }

        else if (TextInput.class.isAssignableFrom(type)) {
            TextInput ti = (TextInput) value;
            String text = ti.getText();

            //		 we even don't use the size for display in a table, since it might
            // be a hint
            if (ti.isSecret() && text != null && text.length() != 0) {
                //			text = "\u25cf\u25cf\u25cf";
                text = "\u2022\u2022\u2022\u2022\u2022"; // these bullets are
                // less big
            }

            c = def.getTableCellRendererComponent(table, text, sel, foc, row, col);
        } else if (MetaInput.class.isAssignableFrom(type)) {
            TableCellRenderer groupRenderer = new GroupRenderer();
            c = groupRenderer.getTableCellRendererComponent(table, value, sel, foc, row, col);
            return c;
        } else if (JProgressBar.class.isAssignableFrom(type)) {
            ProgressBarRenderer progressBarRenderer = new ProgressBarRenderer();
            c = progressBarRenderer.getTableCellRendererComponent(table, value, sel, foc, row, col);
            return c;
        } else
            c = def.getTableCellRendererComponent(table, value, sel, foc, row, col);

        return c;
    }

    private static class BaseTableCellRenderer extends BasicTableCellRenderer {

        BaseTableCellRenderer() {

            //noFocusBorder = new EmptyBorder(1, 1, 1, 1);
            noFocusBorder = new LineBorder(Colors.getUIControlShadow());
        }
    }

    static class GroupRenderer extends BaseTableCellRenderer {

        JPanel sepPanel = new JPanel(new java.awt.BorderLayout());

        GroupRenderer() {

            setLayout(new BorderLayout());
            setOpaque(true);
            setBackground(Colors.getUIControl());
            sepPanel.add(new CenteredJSeparator());
            noFocusBorder = new EmptyBorder(1, 1, 1, 1);
        }

        public Component getTableCellRendererComponent(JTable table, Object value,
                                                       boolean isSelected, boolean hasFocus,
                                                       int row, int col) {

            isSelected = table.isRowSelected(row);
            super.getTableCellRendererComponent(table, value, isSelected, false, row, col);
            sepPanel.setBackground(getBackground());
            return sepPanel;
        }
    }

    /*
     * The Cheap ones. Nearly copied from JTable to have them here for easy
     * changes
     *
     *
     */
    /*
     * not extend JCheckbox but delegate, so we use the default borders and
     * colors
     */

    static class BooleanRenderer extends BaseTableCellRenderer {

        static class MyJCheckBox extends JCheckBox {// make paintComponent
            // visible

            public void paintComponent(Graphics g) {

                super.paintComponent(g);
            }
        }

        private MyJCheckBox delegate = new MyJCheckBox();
        private Object value;                        // to show null values

        public BooleanRenderer() {

            noFocusBorder = new EmptyBorder(1, 1, 1, 1);
            delegate.setHorizontalAlignment(CENTER);
            delegate.setBorderPaintedFlat(true);
            delegate.setBorderPainted(false);
            delegate.setOpaque(false);
        }

        public Dimension getPreferredSize() {

            Dimension d = delegate.getPreferredSize();
            Dimension td = super.getPreferredSize();// border only
            td.width += d.width;
            return td;
        }

        public Component getTableCellRendererComponent(JTable table, Object value,
                                                       boolean isSelected, boolean hasFocus,
                                                       int row, int column) {

            // colors and borders:
            super.getTableCellRendererComponent(table, null, isSelected, hasFocus, row,
                    column);
            this.value = value;
            delegate.setSelected(Boolean.TRUE.equals(value));
            return this;
        }

        protected void paintComponent(Graphics g) {

            super.paintComponent(g);// background
            if (value == null) return;// no painting if value is empty
            delegate.setSize(getSize());
            delegate.paintComponent(g);
        }

        public void updateUI() {

            super.updateUI();
            if (delegate != null) delegate.updateUI();
        }
    }

    /**
     * Basic Renderer using a Format, right aligned per default. Changed from
     * the one in JTable
     */
    protected static abstract class FormatRenderer extends BaseTableCellRenderer {

        private Format formatter;

        public FormatRenderer() {

            setHorizontalAlignment(JLabel.RIGHT);
        }

        /**
         * return a formatter, only called once on first use
         */
        protected abstract Format createFormatter();

        public void setValue(Object value) {

            if (formatter == null) formatter = createFormatter();
            setText((value == null) ? "" : formatter.format(value));
        }
    }

    protected static class DoubleRenderer extends FormatRenderer {

        protected Format createFormatter() {

            return FormatFactory.getNumberFormat();
        }
    }

    protected static class DateInputRenderer extends BaseTableCellRenderer {

        DateInputRenderer() {

            setHorizontalAlignment(JLabel.RIGHT);
        }

        public void setValue(Object value) {
            String text = null;
//            Format formatter = FormatFactory.getDateInputFormatter((DateInput) value);
            DateTimeFormatter formatter = DateTimeFormatter.ofPattern(((DateInput) value).getFormat()).withLocale(Locale.getDefault());
            ZonedDateTime date = ((DateInput) value).getDate();
//            log.debug(date);
            if (date != null) {
                try {
                    text = formatter.format(date);
                } catch (Exception e) {
                    log.error(e);
                }
                setText(text);
            }
        }

    }

    private static class ProgressBarRenderer extends BaseTableCellRenderer {
        public Component getTableCellRendererComponent(JTable table, Object value,
                                                       boolean isSelected, boolean hasFocus,
                                                       int row, int column) {
            if (value instanceof JProgressBar)
                return (JProgressBar) value;
            else if (value instanceof JLabel)
                return (JLabel) value;
            else
                return super.getTableCellRendererComponent(table, value, isSelected, hasFocus, row, column);
        }
    }

}