package com.utd.stb.back.ui.impl.wizard;

import com.utd.stb.back.ui.impl.lib.HelpSourceImpl;
import com.utd.stb.back.ui.inter.ExtendedWizardStep;
import com.utd.stb.back.ui.inter.WizardFormData;
import com.utd.stb.inter.dialog.HelpSource;
import com.utd.stb.inter.wizzard.FormData;
import com.utd.stb.inter.wizzard.FormDisplayInfo;


/**
 * Holds the data for a Wizard Step. Data are loaded when a step is navigated to
 * every time, because changes on other steps might change the data (not really
 * in backward navigation)
 *
 * @author rainer Created 14.09.2004
 */
public class StandardWizardStep implements ExtendedWizardStep {

    WizardFormData formdata;
    StandardFormDisplayInfo displayInfo;
    int currentFormNumber;
    boolean validData = false;
    String stepName;
    HelpSourceImpl help;

    /**
     * @param currentNumber the number of step
     * @param stepName      the name, used in the Navigator Panel
     */
    public StandardWizardStep(int currentNumber, String stepName, String helpName, String tooltip) {

        //the displays used by GUI
        this.displayInfo = new StandardFormDisplayInfo(new Integer(currentNumber).toString(),
                stepName,
                tooltip);
        this.currentFormNumber = currentNumber;
        this.stepName = stepName;
        this.help = new HelpSourceImpl(helpName);
    }

    /*
     * (non-Javadoc)
     * 
     * @see com.utd.stb.inter.wizzard.WizardStep#getFormData()
     */
    public FormData getFormData() {

        return formdata;
    }

    /*
     * (non-Javadoc) on initialisation we don't have the data, so set them
     * before step is displayed
     * 
     * @see com.utd.stb.back.ui.inter.ExtendedWizardStep#setFormData(com.utd.stb.back.ui.inter.WizardFormData)
     */
    public void setFormData(WizardFormData f) {

        this.formdata = f;
        validData = true;
    }

    /*
     * (non-Javadoc)
     * 
     * @see com.utd.stb.inter.wizzard.WizardStep#getFormDisplayInfo()
     */
    public FormDisplayInfo getFormDisplayInfo() {

        return displayInfo;
    }

    /*
     * (non-Javadoc) Only get part of the display info from backend if form is
     * current, so need to set the data then. Only name and number we have on
     * init
     * 
     * @see com.utd.stb.back.ui.inter.ExtendedWizardStep#setDisplayInfo(java.lang.String,
     *      java.lang.String, java.lang.String)
     */
    public void setDisplayInfo(String pkg, String title) {

        displayInfo.setDisplayInfo(pkg, title);
    }

    /*
     * (non-Javadoc)
     * 
     * @see com.utd.stb.back.ui.inter.ExtendedWizardStep#getCurrentIndex()
     */
    public int getCurrentIndex() {

        // (currentFormNumber - 1) for synchronisation of front- and backend,
        // because backend counts from 1, frontend from 0
        return currentFormNumber - 1;
    }

    /*
     * (non-Javadoc) not used actually, but when navigating backwards it should
     * not be necessary to load data from DB.
     * 
     * @see com.utd.stb.back.ui.inter.ExtendedWizardStep#isFormDataValid(int)
     */
    public boolean isFormDataValid(int direction) {

        if (direction == 1) {
            return false;
        }
        return validData;
    }

    /*
     * (non-Javadoc)
     * 
     * @see com.utd.stb.inter.wizzard.WizardStep#getHelpSource()
     */
    public HelpSource getHelpSource() {

        return help;
    }

    public String getStepName() {

        return stepName;
    }

}