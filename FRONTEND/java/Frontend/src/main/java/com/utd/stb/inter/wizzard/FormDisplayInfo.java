package com.utd.stb.inter.wizzard;

/**
 * Readonly, mostly textual info about a form, doesn't pick up any complex data,
 * so it is fast, for navigation list e.g.. <br>
 * If texts needs more space than is available it is cut, but maybe shown in a
 * tooltip.
 *
 * @author PeterBuettner.de
 */
public interface FormDisplayInfo {

    /**
     * the reportgenerator has a bigger titlebar on top, this is the bold text
     * in that, may be much longer than NavigationListText around 1 line is all
     * right ~ 50 chars.
     *
     * @return
     */
    String getTitleBarTitleText();

    /**
     * the reportgenerator has a bigger titlebar on top, this is the normal
     * informational text in that, around 2 lines long so 80-120 chars is all
     * right,
     *
     * @return
     */
    String getTitleBarInfo();

    /**
     * short text for display in a navigationlist
     *
     * @return
     */
    String getNavigationListText();

    /**
     * For additional info on navigation lists e.g.: tooltips.
     *
     * @return
     */
    String getNavigationListInfo();

}