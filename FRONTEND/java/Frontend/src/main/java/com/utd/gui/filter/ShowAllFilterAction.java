package com.utd.gui.filter;

import com.utd.gui.resources.I18N;

import javax.swing.*;


/**
 * Little helper for continuous look: use this to display an action to reset a
 * Filter to show all elements
 *
 * @author PeterBuettner.de
 */
public abstract class ShowAllFilterAction extends AbstractAction {

    public ShowAllFilterAction() {

        String s = I18N.getString(com.utd.gui.filter.ShowAllFilterAction.class.getName()
                + ".tooltip");
        putValue(Action.SHORT_DESCRIPTION, s);
        putValue(Action.SMALL_ICON, new ShowAllIcon());

    }

}