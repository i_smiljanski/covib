package com.utd.stb.gui.swingApp;

import com.jidesoft.popup.JidePopup;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import javax.swing.*;
import java.awt.*;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;

/**
 * MouseListener for job bar progress below head window
 *
 * @author Inna Smiljanski created 05.01.11
 */
public class BarProgressMouseListener implements MouseListener {

    public static final Logger log = LogManager.getRootLogger();
    JidePopup popup;
    Component owner;
    String text;
    JLabel label;

    public BarProgressMouseListener(Component owner) {
        this(owner, "");
    }

    public BarProgressMouseListener(Component owner, String text) {
        this.owner = owner;
        this.text = text;
        init();
    }

    /**
     * initialized popup as JidePopup with text "No jobs"
     */
    private void init() {
        popup = new JidePopup();
        popup.setMovable(true);
        popup.getContentPane().setLayout(new BorderLayout());
        label = new JLabel(text, SwingConstants.CENTER);
        int height = label.getFontMetrics(label.getFont()).getHeight() * 2;
        label.setPreferredSize(new Dimension(100, height));
        popup.getContentPane().add(label);
    }

    public void mouseClicked(MouseEvent e) {
        showPopup();
    }

    public void mouseEntered(MouseEvent e) {
        showPopup();
    }

    public void mouseExited(MouseEvent e) {
        popup.hidePopup();
    }

    public void mousePressed(MouseEvent e) {
    }

    public void mouseReleased(MouseEvent e) {
    }

    /**
     * opens popup with new text on mouse click
     */
    private void showPopup() {
        if (text != null) {
            Component component = popup.getContentPane().getComponent(0);
            if (component instanceof JLabel) {
                ((JLabel) component).setText(text);
            }
            popup.setOwner(owner);
            if (popup.isPopupVisible()) {
                popup.hidePopup();
            } else {
                popup.showPopup();
            }
        }
    }

    public void setText(String text) {
        this.text = text;
    }
}
