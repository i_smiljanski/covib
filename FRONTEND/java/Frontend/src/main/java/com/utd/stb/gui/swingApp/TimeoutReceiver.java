package com.utd.stb.gui.swingApp;

/**
 * This Frame/Dialog gets informed about a timeout
 *
 * @author PeterBuettner.de
 */
public interface TimeoutReceiver {

    /**
     * called when a timeout occured, window should hide/close without any
     * action, well if it is based on the backed, it should inform backed
     * 'doAction()' that it has closed
     */
    void timeoutOccured();

}