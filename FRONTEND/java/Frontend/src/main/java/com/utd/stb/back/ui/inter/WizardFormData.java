package com.utd.stb.back.ui.inter;

import com.utd.stb.back.data.client.SelectionList;
import com.utd.stb.inter.wizzard.FormData;


/**
 * Extension of interface FormData used in base
 *
 * @author rainer Created 14.12.2004
 */
public interface WizardFormData extends FormData {

    public SelectionList getDataToSave();

    // MCD, 26.Jun 2006
    public String getEntity();

}