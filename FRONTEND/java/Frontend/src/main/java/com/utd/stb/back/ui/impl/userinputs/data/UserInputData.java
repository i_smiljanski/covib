package com.utd.stb.back.ui.impl.userinputs.data;

import com.utd.stb.back.data.client.MenuEntryRecord;
import com.utd.stb.back.data.client.PropertyList;
import com.utd.stb.back.data.client.PropertyRecord;
import com.utd.stb.back.database.Dispatcher;
import com.utd.stb.back.ui.impl.msgboxes.AskCloseMsgBox;
import com.utd.stb.back.ui.impl.userinputs.fields.TextUserInput;
import com.utd.stb.inter.application.BackendException;
import com.utd.stb.inter.userinput.UserInput;
import com.utd.stb.inter.userinput.UserInputs;
import org.apache.commons.codec.binary.Base64;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import javax.crypto.Cipher;
import javax.crypto.IllegalBlockSizeException;
import java.io.UnsupportedEncodingException;
import java.nio.charset.StandardCharsets;
import java.util.ArrayList;
import java.util.Iterator;


/**
 * Implementation of UserInputs, the interface expected by Dialogs.
 *
 * @author rainer Created 14.12.2004
 */
public class UserInputData extends AbstractInputData implements UserInputs {
    public static final Logger log = LogManager.getRootLogger();

    //in wizard dialogs we need special methods for saving(->DB), identify them
    public final static String SAVE_AUDIT = "SAVEAUDIT";
    public final static String SAVE_REPORT = "SAVEREPORT";
    private Dispatcher dispatcher;
    PropertyList properties;    
    private Object initComp;
    private Cipher ecipher;


    private boolean noEsigSuccess = false;

    public UserInputData(Dispatcher d) {

        super(null, d);
        this.dispatcher = d;
    }

    /**
     * @param d          the dispatcher
     * @param properties the list of field data
     * @param o          either the menu item from where dialog was called or the
     *                   strings SAVE_AUDIT or SAVE_REPORT
     */
    public UserInputData(Dispatcher d, PropertyList properties, Object o)
            throws BackendException {

        super(properties, d);
        this.dispatcher = d;
        this.properties = properties;
        this.initComp = o;
        createData();

    }

   /* *//**
     * @param d
     *            the dispatcher
     * @param xmlStream
     *            the xml data
     * @param o
     *            either the menu item from where dialog was called or the
     *            strings SAVE_AUDIT or SAVE_REPORT
     * @param ecipher
     *            a key to encrypt secret fields
     * @throws BackendException
     *//*
    public UserInputData( Dispatcher d, InputStream xmlStream, Object o, Cipher ecipher )
    	throws BackendException {

        super( null, d );
        this.dispatcher = d;
        this.xmlStream = xmlStream;
        this.initComp = o;
        this.ecipher = ecipher;

    }*/

    /**
     * @param d       the dispatcher
     * @param o       either the menu item from where dialog was called or the
     *                strings SAVE_AUDIT or SAVE_REPORT
     * @param ecipher a key to encrypt secret fields
     * @throws BackendException
     */
    public UserInputData(Dispatcher d, PropertyList properties, Object o, Cipher ecipher)
            throws BackendException {

        super(properties, d);
        this.dispatcher = d;
        this.properties = properties;
//        log.debug("props = " + properties+" "+Thread.currentThread().getStackTrace()[2]);
        this.initComp = o;
        this.ecipher = ecipher;
        //make UserInputs from PropertyList
        createData();

    }


    /**
     * make PropertyList from UserInputs and put to DB
     *
     * @return an action to be done after saving(another dialog, tree
     * reloads...)
     * @throws BackendException
     */
    public Object saveData() throws BackendException {

        noEsigSuccess = false;
        return putPropertyList(getListToSave());
    }


    Object deleteData() {
        try {
            AskCloseMsgBox askExit = new AskCloseMsgBox(dispatcher.getTranslation("main.askSaveChanges.header.title"),
                    dispatcher.getTranslation("main.askSaveChanges.text"), this);
            return askExit;
        } catch (BackendException e) {
            log.error(e);
        }
        return null;
    }


    public PropertyList getData(ArrayList data) throws BackendException {

        return getListToSave(data);
    }

    /**
     * ???
     *
     * @param titleOfMask the title of the current mask
     * @return an action to be done after saving(another dialog, tree
     * reloads...)
     * @throws BackendException
     */
     Object checkMask(String titleOfMask, int maskType, Object noBtnAction) throws BackendException {

        return checkDependenciesOnMask(getListToSave(), titleOfMask, maskType, noBtnAction);
    }

     Object checkMask(String titleOfMask, int maskType) throws BackendException {

        return checkMask(titleOfMask, maskType, null);
    }

    /**
     * make PropertyList from UserInputs and put to DB
     *
     * @return an action to be done after saving(another dialog, tree
     * reloads...)
     * @throws BackendException
     */
     Object saveDataWithNewToken() throws BackendException {

        // only one token (ESIG_NO_SUCCESS) is not in the context menu,
        // set the context menu record to null
        noEsigSuccess = true;
        return putPropertyList(getListToSave());
    }

    /**
     * used in esig and change password dialogs to verify password input
     *
     * @return the value of PASSWORD field
     */
    protected String getPassword() {
        log.debug(indexOfPasswordField);
        return getValueOfTextField(indexOfPasswordField);
    }

    /**
     * used in esig dialog to verify the user
     *
     * @return the value of PASSWORD field
     */
    protected String getUser() {

        return getValueOfTextField(indexOfUserField);
    }

    /**
     * @return true if there are fields named PASSWORD or OPASSWORD
     */
     boolean hasPasswordToCheck() {

        return hasPasswordToCheck;
    }

    /**
     * Sends encrypted password to dispatcher for comparism
     *
     * @return true if entered password is users password
     * @see Dispatcher#checkPassword(String)
     */
     boolean isPasswordOk() {
        return dispatcher.checkPassword(encrypt(getPassword()));
    }

    /**
     * tests if connected user is the same as the entered user
     *
     * @return true if entered user is the connected user
     */
     boolean isUserOk() {

        return dispatcher.checkUser(getUser());
    }

    /**
     * tests if the prompt for an username is shown on the esig dialog
     *
     * @return true if the username is shown
     */
     boolean isUsernameShown() {

        return dispatcher.getPromptUsername();
    }

    /**
     * @return the number a user types a wrong password
     */
     int getNumberOfPasswordFailures() {

        return dispatcher.getNumberOfPasswordFailures();
    }

    /**
     * @return the number a user types a wrong password
     */
     int getNumberOfUserFailures() {

        return dispatcher.getNumberOfUserFailures();
    }

    /**
     * @return set the number a user types a wrong password or wrong username
     */
     void setNumberOfFailuresToNull() {

        dispatcher.setNumberOfFailuresToNull();
    }

    /**
     * @return the possible number a user can type a wrong password or a wrong
     * username
     */
     int getNumberOfPossibleFailures() {

        return dispatcher.getNumberOfPossibleFailures();
    }

    /**
     * @return if cancel an esig dialog send a failure message or not
     */
     boolean sendFailureOnCancel() {

        return dispatcher.sendFailureOnCancel();
    }

    /**
     * Compares all input in list that are indexed as NPASSWORDFIELD
     *
     * @return true if all NPASSPWRDFIELDS are equal
     */
    /* boolean passwordEqual() {
         log.debug("begin");

        if (this.indecesOfNewPasswordFields.size() < 2) return true;

        try {

            Iterator it = indecesOfNewPasswordFields.iterator();
            String[] passwords = new String[indecesOfNewPasswordFields.size()];
            for (int i = 0; it.hasNext(); i++) {

                int index = (Integer) it.next();
                passwords[i] = getValueOfTextField(index);
                if (i > 0 && !passwords[i].equals(passwords[i - 1])) {
                    return false;
                }
            }

        } catch (Exception e) {

            return false;

        }

        return true;

    }*/

    /**
     * @param index index of field
     * @return the value
     */
    private String getValueOfTextField(int index) {
        String value = "";
        UserInput ui = (UserInput) data.get(index);
        log.debug(index + " " + ui.getLabel());
        if (ui instanceof TextUserInput) {
            TextUserInput txt = (TextUserInput) ui;
            PropertyRecord rec = txt.getProperty();
            value = rec.getValueAsString();
        }

        return value;

    }

    /**
     * ???
     *
     * @param propsToSave the list of fields as PropertyList
     * @param titleOfMask the title of the current mask
     * @return an Object to be executed as net action
     * @throws BackendException
     */

    private Object checkDependenciesOnMask(PropertyList propsToSave,
                                           String titleOfMask, int maskType) throws BackendException {
        return checkDependenciesOnMask(propsToSave, titleOfMask, maskType, null);
    }


    private Object checkDependenciesOnMask(PropertyList propsToSave,
                                           String titleOfMask, int maskType, Object noBtnAction) throws BackendException {
        log.debug(initComp);
        if (initComp != null && initComp instanceof MenuEntryRecord)
            return dispatcher.checkDependenciesOnMask((MenuEntryRecord) initComp,
                    propsToSave,
                    titleOfMask,
                    maskType,
                    noBtnAction);
        return null;
    }

    /**
     * calls putPropertyList in dispatcher
     *
     * @param propsToSave the list of fields as PropertyList
     * @return an Object to be executed as next action
     * @throws BackendException
     */
    private Object putPropertyList(PropertyList propsToSave) throws BackendException {

        if (noEsigSuccess) {

            return dispatcher.putPropertyList(propsToSave, null);

        } else if (initComp instanceof MenuEntryRecord) {

            return dispatcher.putPropertyList(propsToSave, initComp);

        } else if (initComp instanceof String) {

            if (Dispatcher.PASSWORDACTION.equals(initComp)) {
                return changePassword(propsToSave);
            }

            return dispatcher.putPropertyList(propsToSave, initComp.toString());
        }

        return null;
    }

    private Object changePassword(PropertyList list) throws BackendException {

        String oldPassword;
        String newPassword;
        try {
            if (indexOfPasswordField != -1)
                oldPassword = list.get(indexOfPasswordField).getValueAsString();
            else
                oldPassword = null;
            newPassword = list.get(((Integer) indecesOfNewPasswordFields.get(0)).intValue()).getValueAsString();
        } catch (Exception e) {
            throw new BackendException("Application configuration error", e,
                    BackendException.SEVERITY_ACTION_EXIT);
        }
        return dispatcher.changePassword(newPassword, oldPassword);
    }

    /**
     * @param msg string to translate
     * @return the translated string
     * @throws BackendException
     */
    protected String getTranslation(String msg) throws BackendException {

        return dispatcher.getTranslation(msg);
    }

    /**
     * cancel whatever on cancel button
     */
    public void cancelAction() {

        dispatcher.cancelAction();
    }

    /**
     * @param str string to encrypt
     * @return encrypted string
     */
    private String encrypt(String str) {

        try {
            // Encode the string into bytes using utf-8
            byte[] utf8 = str.getBytes(StandardCharsets.UTF_8);

            // Encrypt
            byte[] enc = ecipher.doFinal(utf8);

            // Encode bytes to base64 to get a string
            return Base64.encodeBase64String(enc);
        } catch (javax.crypto.BadPaddingException | IllegalBlockSizeException e) {
            log.error(e);
        }
        return null;
    }

}