package com.utd.stb.back.data.client;

import com.uptodata.isr.utils.ConvertUtils;
import com.utd.stb.StabberInfo;
import com.utd.stb.back.ui.impl.userinputs.data.ContextDefaultTableModel;
import org.apache.commons.lang.StringUtils;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.sql.Blob;
import java.sql.Clob;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.ZoneId;
import java.time.ZonedDateTime;
import java.time.format.DateTimeFormatter;
import java.time.format.FormatStyle;
import java.util.*;

/**
 * This class is used to hold data from database structures. This includes
 * column names/types/pecision and the values. Storage is based on a Hashtable
 * with column label as key and a DatabaseFieldMap as value. Several access
 * methods to those values are provided. Additionally the name of the descriptor
 * is stored, so based on a StructMap the database struct object can easily be
 * constructed. The database object <->StructMap transfer operations are handled
 * by ComplexDatabaseRecord.
 *
 * @author rainer bruns Created 15.12.2004
 * @see DatabaseFieldMap
 */
public class StructMap {

    public static final Logger log = LogManager.getRootLogger();


    /**
     * For data storage
     */
    private Hashtable<String, DatabaseFieldMap> structFields;
    /**
     * name of struct descriptor
     */
    private String descriptor;

    private static Locale dbLocale;

    /**
     * @param desc the name of the struct descripor
     */
    public StructMap(String desc) {
        dbLocale = ContextDefaultTableModel.oracleLangToLocale(StabberInfo.getLocale());
        structFields = new Hashtable<>();
        this.descriptor = desc;
        if (dbLocale == null || StringUtils.isEmpty(dbLocale.toString())) {
            dbLocale = Locale.getDefault();
        }
    }

    /**
     * @return the name of the descriptor
     */
    public String getDescriptor() {

        return descriptor;
    }

    /**
     * put a DatabaseFieldMap with key fieldName
     *
     * @param fieldName column name in DB
     * @param map       column attribute
     */
    public void put(String fieldName, DatabaseFieldMap map) {
        structFields.put(fieldName, map);
    }

    /**
     * @param fieldName name of the field
     * @return its attributes
     */
    private DatabaseFieldMap get(String fieldName) {

        return structFields.get(fieldName);
    }



    /**
     * @param fieldName the field name
     * @return the length of field, i.e. 2 for varchar(2)
     */
    int getFieldLength(String fieldName) {

        try {
            return get(fieldName).getFieldSize();
        } catch (Exception e) {
            return -1;
        }
    }

    /**
     * @param fieldName the field name
     * @return the value of field
     */
    public Object getValue(String fieldName) {
        try {
            return get(fieldName).getValue();
        } catch (NullPointerException ne) {
            return null;
        }
    }

    /**
     * @param fieldName field name
     * @return the value of field as String
     */
    public String getValueAsString(String fieldName) {
        DatabaseFieldMap field = get(fieldName);
        if (field == null) {
            log.info(fieldName + ": DatabaseFieldMap is null");
            return "";
        }
        Object value = field.getValue();
        if (value == null) {
//            log.info(fieldName + ": value is null");
            return "";
        }
//        log.debug(value);
        return value.toString();

    }

    /**
     * @param fieldName field name
     * @return the value of field as int
     */
    int getValueAsInt(String fieldName) {
        try {
            return new Integer(get(fieldName).getValue().toString());
        } catch (NullPointerException | NumberFormatException ne) {
            return 0;
        }
    }

    /**
     * value T is mapped to true, anything else false
     *
     * @param fieldName field name
     * @return the value of field as boolean
     */
    boolean getValueAsBoolean(String fieldName) {
        return getValueAsBoolean(fieldName, "T");
    }

    /**
     * value special is mapped to true, anything else false
     *
     * @param fieldName field name
     * @param special   the "special" value different from "T" and "F", e.g. "C"
     *                  for changeable
     * @return the value of field as boolean
     */
    boolean getValueAsBoolean(String fieldName, String special) {
        List<String> list = new ArrayList<String>();
        list.add(special);
        return getValueAsBoolean(fieldName, list);
    }

    // several values can be mean "true"
    boolean getValueAsBoolean(String fieldName, List<String> trueValues) {
        DatabaseFieldMap fieldMap = get(fieldName);
        if (fieldMap != null) {
            String value = (String) fieldMap.getValue();
            return trueValues.contains(value);
        }

        return false;
    }

    /**
     * @param fieldName field name
     * @return the value of field as String
     */
    byte[] getBlobValueAsBytes(String fieldName) {
        byte[] bytes = null;
        try {
            Object val = get(fieldName).getValue();
            if (val instanceof Blob) {
                Blob blob = (Blob) val;
                bytes = ConvertUtils.getBytes(blob);
            } else if (val instanceof byte[]) {
                bytes = (byte[]) val;
            }
        } catch (Exception ne) {
            log.error(ne);
        }
        return bytes;
    }

    byte[] getClobValue(String fieldName) {
        byte[] bytes = null;
        try {
            Object val = get(fieldName).getValue();
            if (val instanceof Clob) {
                Clob clob = (Clob) val;
                bytes = ConvertUtils.getBytes(clob);
            }
        } catch (Exception e) {
            log.error(e);
        }
        return bytes;
    }

    /**
     * If there are non emtpy FORMAT or TYPE fields in underlying struct, those
     * are used to parse the String. Else a default is used. If parsing is not
     * succesfull null is returned.
     *
     * @param fieldName field name
     * @return the value of field as date
     */
    ZonedDateTime getValueAsDate(String fieldName) {

        DateTimeFormatter formatter = DateTimeFormatter.ofLocalizedDate(FormatStyle.SHORT);
        String format = getValueAsString("FORMAT");
//        log.debug(format);
        if (StringUtils.isNotEmpty(format)) {
            try {
                formatter = DateTimeFormatter.ofPattern(format).withLocale(dbLocale);
            } catch (Exception e) {
                log.error("Format: " + format + " error: " + e);
            }
        }
        String dateAsString = getValueAsString(fieldName);
        if (StringUtils.isEmpty(dateAsString)) {
            return null;
        }
        ZonedDateTime d = null;
        try {
            LocalDateTime date = LocalDateTime.parse(dateAsString, formatter);
            d = date.atZone(ZoneId.systemDefault());
        } catch (Exception e) {
            log.error("Format: " + format + ", value: " + dateAsString + " error: " + e);
        }

        if (d == null) {
            try {
                LocalDate date = LocalDate.parse(dateAsString, formatter);
                d = date.atStartOfDay(ZoneId.systemDefault());
            } catch (Exception e) {
                log.error("Format: " + format + ", value: " + dateAsString + " error: " + e);
            }
        }
//        log.debug(dateAsString + ";  " + formatter + ";  " + d);
        return d;
    }


    /**
     * @return an Array of all values of struct
     */
    public Object[] getValues() {

        Collection c = structFields.values();
        return c.toArray();
    }

    public static String NoValueAvailable = "No Display Field available";

    public String toString() {
        DatabaseFieldMap displayMap = get("SDISPLAY");
        DatabaseFieldMap infoMap = get("SINFO");
        String retString = "";
        if (displayMap != null && StringUtils.isNotEmpty(String.valueOf(displayMap))) {
            Object value = displayMap.getValue();
            if (value != null) {
                retString = displayMap.getValue().toString();
            }
        }
        /*if (StringUtils.isNotEmpty(infoMap) && StringUtils.isNotEmpty(infoMap.getValue())) {            //for filter
         retString += " " + infoMap.getValue().toString();
         }*/
        return StringUtils.isNotEmpty(retString) ? retString : NoValueAvailable;
    }

    /**
     * @param fieldName field name
     * @param val       value to set field to
     */
    public void setValue(String fieldName, Object val) {

        get(fieldName).setValue(val);
    }

    public static void main(String[] args) {
        DateTimeFormatter formatter = DateTimeFormatter.ofPattern("dd.MM.yyyy HH:mm:ss").withLocale(Locale.getDefault());
        String dateAsString = "11.08.2018 16:45:11";
        String dateAsString2 = "11.08.2018 17:45:09";

        ZonedDateTime d = null;
        try {
            LocalDateTime date = LocalDateTime.parse(dateAsString, formatter);
            LocalDateTime date2 = LocalDateTime.parse(dateAsString2, formatter);
            log.debug(date + " " +date.compareTo(date2));
            d = date.atZone(ZoneId.systemDefault());
        } catch (Exception e) {
            log.debug(e);
        }

        if (d == null) {
            try {
                LocalDate date = LocalDate.parse(dateAsString, formatter);
                d = date.atStartOfDay(ZoneId.systemDefault());
            } catch (Exception e) {
                log.debug(e);
            }
        }
        log.debug(dateAsString + ";  " + formatter + ";  " + d);

    }
}
