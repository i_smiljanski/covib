package com.utd.stb.back.ui.impl.msgboxes;

import com.utd.stb.inter.action.ButtonItem;
import com.utd.stb.interlib.BasicMessageBox;


/**
 * Implementation of BasicMessageBox that displays a warning
 *
 * @author rainer bruns Created 06.12.2004
 */
public class WarningMsgBox extends BasicMessageBox {

    int timeout;

    /**
     * Creates a message box with close button
     *
     * @param title Title of MessageBox
     * @param text  Text of MessageBox
     */
    public WarningMsgBox(String title, String text) {
        this(title, text, 0);
    }

    /**
     * Creates a message box with close button, closes the message box after
     * timeout MCD 12.07.2005
     *
     * @param title Title of MessageBox
     * @param text  Text of MessageBox
     */
    public WarningMsgBox(String title, String text, int timeout) {

        super(null, createHeaderBar(title, ICON_EXCLAMATION), null, text, new ButtonItem[]{bClose}, null, true);

        this.timeout = timeout;
    }

    /*
     * (non-Javadoc)
     * 
     * @see com.utd.stb.interlib.BasicMessageBox#onBoxClose(com.utd.stb.inter.action.ButtonItem)
     *      Do nothing here
     */
    protected Object onBoxClose(ButtonItem clickedButton) {

        return null;
    }

    public int getTimeout() {

        return timeout;
    }


}