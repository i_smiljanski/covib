package com.utd.stb.gui.help;

import com.uptodata.isr.server.utils.fileSystem.FileSystemWorker;
import com.uptodata.isr.server.utils.process.ProcessHelper;
import com.utd.gui.controls.sidebar.SideBarSplitPaneUI;
import com.uptodata.isr.gui.util.Colors;
import com.utd.stb.StabberInfo;
import com.utd.stb.back.ui.impl.userinputs.data.ContextDefaultTableModel;
import com.utd.stb.gui.swingApp.PersistTool;
import com.utd.stb.gui.swingApp.SplitWindowState;
import com.utd.stb.gui.swingApp.exceptions.ExceptionDialog;
import com.utd.stb.gui.swingApp.exceptions.ExceptionTools;
import com.utd.stb.inter.application.BackendException;
import com.utd.stb.inter.application.GuiPersistance;
import com.utd.stb.inter.dialog.HelpSource;
import org.apache.commons.io.FileUtils;
import org.apache.commons.lang.StringUtils;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import javax.help.*;
import javax.help.plaf.basic.BasicFavoritesCellRenderer;
import javax.swing.*;
import javax.swing.border.*;
import javax.swing.tree.TreeCellRenderer;
import java.awt.*;
import java.awt.event.HierarchyBoundsListener;
import java.awt.event.HierarchyEvent;
import java.beans.PropertyChangeEvent;
import java.beans.PropertyChangeListener;
import java.io.File;
import java.io.IOException;
import java.net.MalformedURLException;
import java.net.URI;
import java.net.URL;
import java.net.URLClassLoader;


/**
 * Wrapper, central entry point for JavaHelp, some hacks accumulated, <br>
 * please enshure that e.g. the JHelp Object is never destroyed
 *
 * @author PeterBuettner.de
 */
public class Help {
    public static final Logger log = LogManager.getRootLogger();

    //private static Border SPACER_BORDER = new EmptyBorder(3,3,3,3);
    private static final int SPC = 8;

    private static Border LABEL_SPACER_BORDER = new MatteBorder(
            SPC,
            SPC,
            1,
            1,
            Colors
                    .getUIControl());

    private static Border TEXTFIELD_SPACER_BORDER = new MatteBorder(
            SPC,
            1,
            1,
            SPC,
            Colors
                    .getUIControl());

    private static Border TREE_SPACER_BORDER = new MatteBorder(
            SPC,
            SPC,
            SPC,
            SPC,
            Colors
                    .getUIControl());

    private static Border SCROLLPANE_BORDER = new CompoundBorder(
            TREE_SPACER_BORDER,
            new LineBorder(
                    Colors
                            .getUIControlShadow(),
                    1));

    private static Border CONTENT_BORDER = new CompoundBorder(
            new EmptyBorder(
                    3,
                    0,
                    3,
                    3),
            new LineBorder(
                    Colors
                            .getUIControlShadow(),
                    1));

    private HelpSet hs;
    public static String jar = "JavaHelp.jar";
    //private static final String    	helpHS  = "JavaHelp/iStudyReporterHelp.hs";
    private DefaultHelpBroker hb;
    private boolean isInit;

    /**
     * save/restore window properties, favorites
     */
    private GuiPersistance guiPersistance;
    private SplitWindowState options = new SplitWindowState();
    private static final String PERSIST = "help.frame";
    private JHelpContentViewer helpContentViewer;
    private JHelp jHelp;
    private PropertyChangeListener dividerChangeListener;
    private static Help instance = new Help();

    private Help() {
    }

    private static long tick = 0;

    private static void tick() {

        long t = System.currentTimeMillis();
        if (tick == 0) tick = t;
        tick = t;
    }

    /**
     * tries to load the helpset, maybe called more than once, but has only
     * effect on first call
     *
     * @throws IOException
     */
    private void init() throws Exception {

        if (isInit) return;
        isInit = true; // try only once

        guiPersistance = PersistTool.findGuiPersistance();
        options = PersistTool.read(guiPersistance, PERSIST);

        URL[] urls;
        File help = new File("app", StabberInfo.getApplicationInfo("ISR_NAME") + "Help.jar");
        URI helpUri = help.toURI();

        try {
            urls = new URL[]{helpUri.toURL()};
        } catch (MalformedURLException e) {
            log.error(e);
            throw new Exception("HelpSetException " + e);
        }
        ClassLoader cl = new URLClassLoader(urls);
        if (cl == null) {
            cl = Help.class.getClassLoader();
        }
        tick();

        String helpSetName = StabberInfo.getApplicationInfo("ISR_NAME") + "Help.hs";
        URL hsURL = HelpSet.findHelpSet(cl, helpSetName);
        if (hsURL == null) {
            String er = "Help set '" + helpSetName + "' could not be found";
            log.error(er);
            throw new IOException(er);
        }
        tick();
        try {
            hs = new HelpSet(null, hsURL);
        } catch (HelpSetException e) {
            throw new Exception("HelpSetException " + e);
        }
        hs.setTitle(StabberInfo.getApplicationInfo("ISR_NAME"));
        tick();

        hb = new DefaultHelpBroker(hs);
        tick();

        hb.initPresentation();
        tick();
        Window window = getWindow();
        tick();
        enhanceWindow(window);
        tick();

        if (helpContentViewer != null) changeSPBorders(helpContentViewer, CONTENT_BORDER);
        JSplitPane splitMain = getSplitPane();
        if (splitMain != null) enhanceSplitMain(splitMain);

        // since a window adapter doesn't see a 'application exit' save on every change
        // we look at jHelp, since the window changes if switched from frame to dialog
        //		componentAdapter = new ComponentAdapter(){
        //			public void componentMoved(ComponentEvent e) {savePosAndSize();}
        //			public void componentResized(ComponentEvent e) {savePosAndSize();}
        //			};
        //		jHelp.addComponentListener(componentAdapter);

        jHelp.addHierarchyBoundsListener(new HierarchyBoundsListener() {

            public void ancestorMoved(HierarchyEvent e) {

                savePosAndSize();
            }

            public void ancestorResized(HierarchyEvent e) {

                savePosAndSize();
            }
        });
        //options.getWindowState().putWindowStateListener(window);

        dividerChangeListener = new PropertyChangeListener() {

            public void propertyChange(PropertyChangeEvent evt) {

                saveDivider();
            }
        };
        tick();
    }

    /**
     * make it a 'sidebar'
     */
    private void enhanceSplitMain(JSplitPane splitMain) {

        splitMain.setContinuousLayout(false);// continuous is to slow
        splitMain.setUI(new SideBarSplitPaneUI());
        splitMain.setDividerSize(8);// after setUI()

        // let user size to zero:
        ((JComponent) splitMain.getLeftComponent()).setMinimumSize(new Dimension());
        ((JComponent) splitMain.getRightComponent()).setMinimumSize(new Dimension());

        // if 'glossary' is visible, set divider to 0, open divider again-> TOC
        // is shown
        // this is to disable that behaviour (sidebar hides navigation)
        // so we need to fix this, its is nearly the difference between
        // JSplitPane & SideBar
        JPanel intermediateLeftCompo = new JPanel(new BorderLayout()) {

            public Dimension getPreferredSize() {

                Component c = getComponent(0);
                if (c == null)
                    return super.getPreferredSize();
                else
                    return c.getPreferredSize();
            }

            protected void addImpl(Component comp, Object constraints, int index) {

                removeAll();
                super.addImpl(comp, constraints, index);
            }
        };

        Component c = splitMain.getLeftComponent();
        splitMain.setLeftComponent(intermediateLeftCompo);
        intermediateLeftCompo.add(c);
    }

    /**
     * the top window (Frame/Dialog) of the presentation
     */
    private Window getWindow() {

        return hb.getWindowPresentation().getHelpWindow();
    }

    private void savePosAndSize() {

        Window window = getWindow();
        if (window == null) return;
        options.getWindowState().save(window);
        PersistTool.store(guiPersistance, options, PERSIST);
    }

    private void saveDivider() {

        JSplitPane splitMain = getSplitPane();
        if (splitMain == null) return;
        options.setDividerPos(splitMain.getDividerLocation());
        PersistTool.store(guiPersistance, options, PERSIST);
    }

    /**
     * remove that ugly BEVEL Borders that are set in JavaHelp, since the Look
     * &amp; Feel implementation there is <b>very </b> ugly, see later for a
     * better solution to look pretty.
     * <p>
     * Also sets rollover for toolbar buttons, enhances trees, ...
     *
     * @param p
     */
    private void enhanceWindow(Container p) {

        Component[] cc = p.getComponents();
        for (int i = 0; i < cc.length; i++) {
            Component c = cc[i];
            if (c instanceof javax.swing.CellRendererPane) return;

            if (c instanceof JScrollPane) ((JComponent) c).setBorder(SCROLLPANE_BORDER);

            if (c instanceof JToolBar) ((JToolBar) c).setRollover(true);

            if (c instanceof JTree) {
                JTree tree = (JTree) c;
                tree.setShowsRootHandles(false);
                setTI(tree);
                tree.setBorder(new EmptyBorder(1, 2, 1, 2));// 'insets'
            }
            if (c instanceof JTextField) wrapBorder(c, TEXTFIELD_SPACER_BORDER);
            if (c instanceof JLabel) wrapBorder(c, LABEL_SPACER_BORDER);

            if (c instanceof JHelpContentViewer) helpContentViewer = (JHelpContentViewer) c;
            if (c instanceof JHelp) jHelp = (JHelp) c;

            if (c instanceof Container) enhanceWindow((Container) c);
        }
    }

    /**
     * recursively sets the border of all JScrollPanes to border
     */
    private void changeSPBorders(Container p, Border border) {

        Component[] cc = p.getComponents();
        for (int i = 0; i < cc.length; i++) {
            Component c = cc[i];
            if (c instanceof JScrollPane) ((JComponent) c).setBorder(border);
            if (c instanceof Container) changeSPBorders((Container) c, border);
        }
    }

    /**
     * debug helper: level of c in hierarchy
     */
    private static String level(Component c) {

        StringBuffer b = new StringBuffer(100);
        while ((c = c.getParent()) != null)
            b.append("   ");
        return b.toString();
    }

    /**
     * sets the border of c to a compound border, the inner one is the 'old'
     * border
     */
    private static void wrapBorder(Component c, Border outer) {

        if (!(c instanceof JComponent)) return;
        JComponent jc = (JComponent) c;
        jc.setBorder(new CompoundBorder(outer, jc.getBorder()));
    }

    /**
     * really hacking, inspect later, may use a delegate pattern: new renderer,
     * uses old one
     */
    private void setTI(JTree tree) {

        TreeCellRenderer tcr = tree.getCellRenderer();
        if (!(tcr instanceof BasicFavoritesCellRenderer)) return;

        tree.setCellRenderer(new BasicFavoritesCellRenderer() {

            public Icon getDefaultLeafIcon() {

                return null; //UIManager.getIcon("OptionPane.informationIcon");
            }
        });
    }

    /**
     * The central splitter
     *
     * @return may be null
     */
    private JSplitPane getSplitPane() {

        if (helpContentViewer == null) return null;
        return (JSplitPane) SwingUtilities.getAncestorOfClass(JSplitPane.class,
                helpContentViewer);
    }

    private void display() {

        if (hb.isDisplayed()) {
            getWindow().toFront();
            return;
        }

        Rectangle bounds = options.getWindowState().getBounds();

        //	options.getWindowState().restore(window); //flickers or doesn't work
        // (if before setDisplayed())

        if (getWindow() instanceof JFrame) {
            hb.setLocation(bounds.getLocation());// for frames: looks better
            hb.setSize(bounds.getSize());
            hb.setDisplayed(true);
        } else {
            hb.setDisplayed(true);
            hb.setLocation(bounds.getLocation());// for dialogs this has to be
            // after setDisplayed()
            hb.setSize(bounds.getSize());
        }

        // it seems that the divider is set later, so we have to set/save it
        // later
        SwingUtilities.invokeLater(new Runnable() {

            public void run() {

                handleDividerSetting();
            }
        });
    }

    private void handleDisplay(Component component) {

        // they forget to hide the frame on switch to dialog (JavaHelp 2.0)
        Window newCaller = null;
        if (component instanceof Window)
            newCaller = (Window) component;
        else
            newCaller = SwingUtilities.getWindowAncestor(component);

        Window oldCaller = hb.getWindowPresentation().getActivationWindow();
        boolean isSame = (newCaller instanceof Dialog) == (oldCaller instanceof Dialog);

        //	boolean reshowOnClose=false;
        if (!isSame && hb.isDisplayed()) {
            Window oldDisplay = getWindow();
            oldDisplay.setVisible(false);
            // reshow after new is closed
            //		reshowOnClose = oldDisplay instanceof JFrame;
        }

        // needs to be before display(); for dialogs
        hb.setActivationWindow(newCaller);
        display();

        /*
         * // this is much to simple! e.g. frame->Help; modal dialog ->
         * help->close->help *bang* help is a frame and unreachable
         * if(reshowOnClose){ final Window newDisplay = getWindow();
         * newDisplay.addComponentListener(new ComponentAdapter(){ public void
         * componentHidden(ComponentEvent e) {
         * newDisplay.removeComponentListener(this);// use once
         * hb.setActivationWindow(null); display(); } }); }
         */

    }

    /**
     * Show Help for the thing defined in helpSource <br>
     *
     * @param helpSource if null or pointer to id.. is enull/empty show home page
     * @param component  maybe null, 'owner' is important if used in modal dialogs,
     *                   throw in any Component in a hierarchy
     */
    public static void showHelp(Component component, HelpSource helpSource) {
        log.debug("showHelp");
        try {
            instance.show(component, helpSource);
        } catch (Exception e) { // BadIDException,
            log.error(e);
            ExceptionTools.callExceptionHandler(component,
                    new BackendException("Could not show the Help. " + e, e, BackendException.SEVERITY_INFORMATIONAL),
                    true); // TODOi18n
        }
    }

    /**
     * Show Help: the start page.
     * <p>
     * <br>
     *
     * @param component maybe null, 'owner' important if used in modal dialogs
     */
    public static void showHelp(Component component) {

        showHelp(component, null);
    }


    public static void showHelp() {
        log.info("begin");
        try {
            String locale = ContextDefaultTableModel.oracleLangToLocale(StabberInfo.getLocale()).getLanguage();
            String out = ProcessHelper.displayDocument("help\\", "Help_" + locale + ".pdf");
            log.debug(out);
            log.info("end");
        } catch (Exception e) {
            ExceptionDialog.showExceptionDialog(null, e, "Help could not be shown:\n" + e);
            log.error(e);
        }
    }


    private void show(Component component, HelpSource helpSource) throws Exception {
        init();
        // MCD, 20.Jul 2006, Einstieg bei den Help IDs
        String id = (helpSource == null) ? null : "WordDocuments/" + helpSource.getHelpId();
        log.debug("helpId " + id);

        try {
            hb.setCurrentID(id);
        } catch (Exception e) {
            hb.setCurrentID(hs.getHomeID());
        }

        handleDisplay(component);
    }

    /**
     * set pos initial and setup a propertyChange... to see changes later post
     * it, don't call it the event thread (use invokeLater()
     */
    private void handleDividerSetting() {

        JSplitPane splitMain = getSplitPane();
        if (splitMain == null) return;
        splitMain.setDividerLocation(options.getDividerPos());

        // after open and set, or we read the default one
        splitMain.removePropertyChangeListener(JSplitPane.DIVIDER_LOCATION_PROPERTY,
                dividerChangeListener);
        splitMain.addPropertyChangeListener(JSplitPane.DIVIDER_LOCATION_PROPERTY,
                dividerChangeListener);
    }

}