package com.utd.stb.gui.itemTools;

import com.utd.gui.filter.Filter;
import com.utd.gui.popup.AbstractFilteringCellEditor;

import javax.swing.*;


/**
 * A Celleditor that appears like a combobox: a textfield that works as a filter
 * for a popup JTable. <br>
 * if only one attribute in list, select this per default, so an [ENTER]-key will use
 * it <br>
 * See: {@link com.utd.gui.popup.AbstractFilteringCellEditor}for important
 * details, especially to configure the table
 *
 * @author PeterBuettner.de
 */
public class JTableCellEditor extends AbstractFilteringCellEditor {

    /**
     * standalone defaluts to false
     *
     * @param textField
     * @param data
     */
    public JTableCellEditor(JTextField textField) {

        this(textField, false);
    }

    /**
     * @param textField
     * @param standalone placed in a JTable
     */

    public JTableCellEditor(JTextField textField, boolean standalone) {

        super(textField, standalone);
    }

    protected Object getCurrentListSelection() {

        return null;
    }

    protected JComponent createList(Filter filter) {

        return null;
    }

    protected boolean isPopupList() {
        return false;
    }

}