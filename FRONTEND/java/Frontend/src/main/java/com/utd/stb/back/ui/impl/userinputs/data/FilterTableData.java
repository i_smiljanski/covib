package com.utd.stb.back.ui.impl.userinputs.data;

import com.uptodata.isr.utils.xml.dom.XmlHandler;
import com.utd.stb.back.data.client.SelectionList;
import com.utd.stb.back.data.client.SelectionRecord;
import com.utd.stb.back.database.Dispatcher;
import com.utd.stb.back.ui.impl.tree.DBGuiPersistance;
import com.utd.stb.gui.swingApp.PersistTool;
import com.utd.stb.gui.userinput.swing.GridState;
import com.utd.stb.inter.application.BackendException;
import com.utd.stb.inter.application.GuiPersistance;
import com.utd.stb.inter.userinput.UserInput;
import com.utd.stb.inter.userinput.UserInputs;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;

import javax.swing.*;
import javax.swing.table.TableModel;
import javax.xml.xpath.XPathExpressionException;
import java.awt.*;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.OutputStream;
import java.util.*;
import java.util.List;

/**
 * Implementation of UserInputs, the data to be displayed are from xml file.
 * Creator: smiljanski Date: 24.02.11
 */
public class FilterTableData implements UserInputs {
    public static final Logger log = LogManager.getRootLogger();

    private static final String PERSIST = "grid.";
    private GridState options;
    private GuiPersistance guiPersistance;

    private Dispatcher dispatcher;
    private TableModel tableModel;
    private Object initComp;

    private HashMap<Integer, String> columns = new HashMap<>();
    private String tableName;

    boolean isValid;

    /**
     * Constructor builds the table model for grid table
     *
     * @param stream xml file from db
     * @param d      the dispatcher
     * @param o      the menu item pressed
     */
    public FilterTableData(byte[] xml, Dispatcher d, Object o) {
        super();
        this.dispatcher = d;
        this.initComp = o;
        try {
            createData(xml);
        } catch (XPathExpressionException e) {
            log.error(e);
        }
    }

    public int getSize() {
        return 0;
    }

    public UserInput get(int index) {
        return null;
    }

    public int indexOf(UserInput item) {
        return 0;
    }


    public TableModel getTableModel() {
        return tableModel;
    }

    /**
     * fills the table model with data from db
     *
     * @param resource xml file
     */
    public void createData(byte[] resource) throws XPathExpressionException {
        log.info("begin");
        if (resource == null) {
            return;
        }

        //Table data and columns
        Vector<Vector> data = new Vector<Vector>();
        Vector<String> columnNames = new Vector<String>();
        columnNames.add("#");
        //column properties
        final HashMap<String, HashMap<String, Object>> allColumnsMap = new HashMap<>();
        final List<String> primaryKeys = new ArrayList<String>();

        XmlHandler xmlFile = new XmlHandler(resource);
//        log.debug(xmlFile.xmlToString());
        NodeList headerNdList = xmlFile.selectNodes("//HEADSET/*");
        NodeList dataNdList = xmlFile.selectNodes("//ROWSET/ROW");

        final String insertAllowed = xmlFile.nodeRead("//INSERT_ALLOWED");
        final String deleteAllowed = xmlFile.nodeRead("//DELETE_ALLOWED");

        tableName = xmlFile.nodeRead("//TABLE");


        //column names, type, lovs
        for (int i = 0; i < headerNdList.getLength(); i++) {
            org.w3c.dom.Node headerNode = headerNdList.item(i);
            //column names
            columns.put(i, headerNode.getNodeName());
            columnNames.add(headerNode.getTextContent());
//            log.debug(i + " headerNode=" + headerNode.getNodeName() + " " + headerNode.getTextContent());

            HashMap<String, Object> columnMap = new HashMap<>();

            columnMap.put("header", headerNode.getTextContent());
            columnMap.put("entity", headerNode.getNodeName());
            String lov = xmlFile.attributeRead("haslov", headerNode);
            columnMap.put("type", xmlFile.attributeRead("type", headerNode));
            columnMap.put("lov", lov);
            columnMap.put("required", xmlFile.attributeRead("required", headerNode));
            columnMap.put("editable", xmlFile.attributeRead("editable", headerNode));
//            columnMap.put("parent", xmlFile.attributeRead("parent", headerNode));
            columnMap.put("defaultvalue", xmlFile.attributeRead("defaultvalue", headerNode));
            columnMap.put("displayed", xmlFile.attributeRead("displayed", headerNode));
            columnMap.put("format", xmlFile.attributeRead("format", headerNode));
            String primaryKey = xmlFile.attributeRead("primaryKey", headerNode);
            columnMap.put("primaryKey", primaryKey);

            allColumnsMap.put(headerNode.getTextContent(), columnMap);

            if ("T".equals(primaryKey))
                primaryKeys.add(headerNode.getTextContent());
        }


        //table data
        log.debug(dataNdList.getLength());
        for (int i = 0; i < dataNdList.getLength(); i++) {
            org.w3c.dom.Node rowNode = dataNdList.item(i);

            Vector<Object> lineData = new Vector<>();
            lineData.add(null);
            List<Node> dataNodeList = xmlFile.getChildNodesWithoutText(rowNode);
            for (Node oNode : dataNodeList) {
                lineData.add(oNode.getTextContent());
            }
            if (lineData != null && lineData.size() > 1) data.add(lineData);
        }

        tableModel = new ContextDefaultTableModel(data, columnNames) {


            @Override
            public void init() {
                this._insertAllowed = insertAllowed;
                this._deleteAllowed = deleteAllowed;
                this._allColumnsMap = allColumnsMap;
                this._primaryKeys = primaryKeys;
                this._columns = columns;
                this._dispatcher = dispatcher;
            }

        };

        log.debug("createData:tableModel: " + tableModel.getColumnCount());
        log.info("end");
    }

    /**
     * creates and fills an output xml file, saves it and persistance in db
     *
     * @return a UserDialog object
     * @throws BackendException
     */
    public Object saveData() throws BackendException {
        log.debug("saveData begin:tableModel = " + tableModel.getRowCount() + tableModel);
        ContextDefaultTableModel model = (ContextDefaultTableModel) tableModel;
        JButton button = model.getButton();
        button.getTopLevelAncestor().setCursor(Cursor.getPredefinedCursor(Cursor.WAIT_CURSOR));
        button.doClick();
        isValid = model.validate();
        log.debug("validate:" + isValid);
        if (isValid) {
            XmlHandler xmlFile = new XmlHandler();
            Element root = xmlFile.nodeRoot("DOCUMENT");
            Element update = xmlFile.createElement("UPDATE", "", "count", Integer.toString(model._updatedRows.size()), root);
            Element delete = xmlFile.createElement("DELETE", "", "count", Integer.toString(model._removedRows.size()), root);

            HashMap<String, HashMap<String, Object>> columnMap = model.getAllColumnsMap();
            for (String rowNum : model._updatedRows.keySet()) {
                createXmlRow(model._updatedRows, rowNum, columnMap, xmlFile, update);
            }
            for (String rowNum : model._removedRows.keySet()) {
                createXmlRow(model._removedRows, rowNum, columnMap, xmlFile, delete);
            }
//            OutputStream stream = new ByteArrayOutputStream();

            try {
//                xmlFile.save(stream);
                log.debug(xmlFile.xmlToString());

                Properties properties = dispatcher.loadPersistance();
                ((DBGuiPersistance) guiPersistance).load(properties);
            } catch (Exception e) {
                log.error(e);
            }
            PersistTool.store(guiPersistance, options, PERSIST + tableName);

            dispatcher.savePersistance(((DBGuiPersistance) guiPersistance).save());
            Object o = dispatcher.putPropertyList(null, xmlFile.xmlToString(), initComp);
            /*try {
                stream.flush();
                stream.close();
            } catch (IOException e) {
                log.error(e);
            }*/
            log.debug(tableModel.getRowCount());
            for (int row = 0; row < tableModel.getRowCount(); row++) {
                if (tableModel.getValueAt(row, 0) == "*") {
                    tableModel.setValueAt("", row, 0);  // delete star indicators [ISRC-618]
                }
            }
            model._updatedRows = new HashMap<>();
            model._removedRows = new HashMap<>();
            button.getTopLevelAncestor().setCursor(Cursor.getPredefinedCursor(Cursor.DEFAULT_CURSOR));
            log.debug("saveData end ");
            return o;
        } else
            return null;
//        return new WarningMsgBox(getTranslation("exceptionDialog.window.title"), message);
    }


    public boolean isMaskChanged() {
        ContextDefaultTableModel model = (ContextDefaultTableModel) tableModel;
        if (model._updatedRows.size() > 0 || model._removedRows.size() > 0) {
            log.debug("updatedRows: " + model._updatedRows);
            log.debug("remobedRows: " + model._removedRows);
            return true;
        }
        return false;
    }

    /**
     * creates elements for lov
     *
     * @param rowMap    set of updated or removed rows
     * @param rowNum    rowId
     * @param columnMap column infos
     * @param xmlFile   output file
     * @param parent    parent element in xml (delete or update)
     */
    private void createXmlRow(HashMap<String, HashMap<String, Object>> modifiedRows, String rowNum,
                              HashMap<String, HashMap<String, Object>> columnMap,
                              XmlHandler xmlFile, Element parent) {
        Element row = xmlFile.createElement("ROW", "", null, parent);
        HashMap<String, Object> modifiedRow = modifiedRows.get(rowNum);
        for (String key : modifiedRow.keySet()) {
            Object modifiedValue = modifiedRow.get(key);
//            log.debug(modifiedValue.getClass());
            String value;
            if (modifiedValue instanceof Boolean)
                value = ((Boolean) modifiedValue) ? "T" : "F";
            else {
                value = (String) modifiedValue;
//                log.debug(value + " " + key);
                for (int i = 1; i < tableModel.getColumnCount(); i++) {
                    HashMap<String, Object> column = columnMap.get(tableModel.getColumnName(i));
                    if (!column.get("lov").equals("F")) {
                        SelectionList selectionList = (SelectionList) column.get("selectionList");
                        for (int j = 0; j < selectionList.getSize(); j++) {
                            if (((SelectionRecord) selectionList.get(j)).getDisplay().equals(value)) {
                                value = ((SelectionRecord) selectionList.get(j)).getValue();
                                break;
                            }
                        }
                    }
                }
            }
            xmlFile.createElement(key, value, null, row);
        }
    }

    /**
     * @return window and grid table properties
     */
    public GridState getOptions() {
        guiPersistance = dispatcher.getBase().getGuiPersistance();
        options = PersistTool.readGridState(guiPersistance, PERSIST + tableName);
        return options;
    }

    /**
     * @return window and grid table properties
     */
    /*public void setGridWidowPersistent() {
        guiPersistance = dispatcher.getBase().getGuiPersistance();
        gridWidowPersistent = PersistTool.readGridState(guiPersistance, PERSIST + tableName);
    }*/


    /**
     * cancel whatever on cancel button
     */
    public void cancelAction() {
        dispatcher.cancelAction();
    }

    /**
     * @param msg string to translate from table utd$msg
     * @return translated string according to users language
     * @throws BackendException
     */
    public String getTranslation(String msg) throws BackendException {
        return ((ContextDefaultTableModel) tableModel).getTranslation(msg);
    }

    /**
     * validate message
     *
     * @return Message
     */
    public String getMessage() {
        return ((ContextDefaultTableModel) tableModel).message;
    }

}
