package com.utd.stb.back.ui.impl.tree;

import com.uptodata.isr.loader.webservice.LoaderWebserviceImpl;
import com.utd.stb.StabberInfo;
import com.utd.stb.back.custom.basic.CustomMenuItemImpl;
import com.utd.stb.back.custom.impl.CustomMenu;
import com.utd.stb.back.data.client.*;
import com.utd.stb.back.database.Dispatcher;
import com.utd.stb.back.ui.impl.lib.ActionList;
import com.utd.stb.back.ui.impl.msgboxes.ConfirmActionMsgBox;
import com.utd.stb.back.ui.impl.userinputs.data.UserInputData;
import com.utd.stb.inter.action.MenuItem;
import com.utd.stb.inter.action.MenuItems;
import com.utd.stb.inter.action.NodeAction;
import com.utd.stb.inter.application.*;
import com.utd.stb.inter.userinput.UserInput;
import com.utd.stb.inter.userinput.UserInputs;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.util.*;

/**
 * Wraps underlying functionality in Dispatcher for setting up the tree. Used by
 * App
 *
 * @author Rainer Bruns
 * <p>
 * @see com.utd.stb.gui.swingApp.App
 * @see com.utd.stb.back.database.Dispatcher
 */
public class TreeBase implements Base {

    public static final Logger log = LogManager.getRootLogger();
    /*
     * for GUI persistance
     */
    private DBGuiPersistance persistance = new DBGuiPersistance();
    private Dispatcher dispatcher;
    /*
     * used to store a list of actions in a queue
     */
    private ActionList actions = null;

    private Hashtable<String, Object> userData;
    private TreeNodeRecord selectedNode;

    private JobsTableInfo jobsTableInfo;

    /**
     * loads the persitance and user data
     *
     * @param d The Dispatcher
     */
    public TreeBase(Dispatcher d) throws BackendException {

        this.dispatcher = d;
        //Load GUI Persistance Data
        try {
            persistance.load(dispatcher.loadPersistance());
        } catch (Exception e) {
            e.printStackTrace();
        }

        userData = dispatcher.getUserInfo();
    }

    /**
     * returns the Detail Information of TreeNode node.
     *
     * @param node
     * @return Nodes
     */
    private Nodes getNodeChildsDetail(Node node) throws BackendException {

        selectedNode = (TreeNodeRecord) node;
        // MCD, 02. Nov 2015: not needed anymore, is set with click in App in a Swingworker
        //dispatcher.setNode(selectedNode);
        TreeNodeList nodeList = dispatcher.getTreenodeDetails(selectedNode);
        return nodeList;
    }

    /**
     * returns the Children of TreeNode node.
     *
     * @param node
     * @return Nodes
     */
    public Nodes getNodeChildsTree(Node node) throws BackendException {

        TreeNodeList nodeList = null;

        //so, guess we initialize the tree
        if (node == null) {
            node = dispatcher.getInitialNode();
        }

        nodeList = dispatcher.getTreenodeChilds((TreeNodeRecord) node);
        return nodeList;
    }

    /**
     * Returns the list of properties as UserInputs. This is presented as Dialog
     * by GUI
     *
     * @param node
     * @return UserInputs
     * @see com.utd.stb.back.ui.impl.userinputs.data.UserInputData
     */
    private UserInputs getNodeProperties(Node node) throws BackendException {

        TreeNodeRecord nodeRec = (TreeNodeRecord) node;
        PropertyList props = nodeRec.getProperties();
        UserInputData propData = new UserInputData(dispatcher, props, nodeRec);
        return propData;

    }

    /**
     * @param a The list of actions to do
     * @see com.utd.stb.back.ui.impl.lib.ActionList
     */
    public void setActionList(ActionList a) {
        actions = a;
    }


    /*
     * (non-Javadoc)
     * 
     * @see com.utd.stb.inter.application.Base#hasTask() returns true if
     *      actionlist is initialzed ( by setActionList ) and more actions are
     *      available
     */
    public boolean hasTask() {

        if (actions == null) {
            return false;
        } else {
            return actions.hasMoreActions();
        }
    }

    /*
     * (non-Javadoc)
     * 
     * @see com.utd.stb.inter.application.Base#getNextTask() returns the next
     *      action in list
     */
    public Object getNextTask() {

        if (actions != null) {
            return actions.nextAction();
        } else {
            return null;
        }
    }

    /*
     * (non-Javadoc)
     * 
     * @see com.utd.stb.inter.application.Base#getNodeMenu(com.utd.stb.inter.application.Node)
     */
    public MenuItem getNodeMenu(Node node) throws BackendException {

        /*
         * Construct a basic Menu under which to store the items
         */
        log.info("Start TreeBase.getNodeMenu");
        MenuEntryRecord main = makeBase(node.getText());
        MenuEntryList itemList = null;
        selectedNode = (TreeNodeRecord) node;
        List list = new ArrayList();
        // MCD, 02. Nov 2015: not needed anymore, is set with click in App in a Swingworker
        //dispatcher.setNode(selectedNode);
        //get first level items
        itemList = dispatcher.getMenuList(0);
        itemList.initIterator();
        //loop through the items
        while (itemList.hasNext()) {

            MenuEntryRecord menuItem = itemList.getNext();
            MenuItemImpl item = null;
            /*
             * if item is menu, get all submenus
             */
            if (menuItem.isMenu()) {

                MenuItems subMenu = getSubItems(menuItem);
                boolean hasEnabledSubItems = false;
                for (int i = 0; i < subMenu.getSize(); i++) {
                    if (subMenu.get(i).isEnabled()) {
                        hasEnabledSubItems = true;
                        break;
                    }
                }

                menuItem.setEnabled(hasEnabledSubItems);
                item = new MenuItemImpl(subMenu, menuItem);

            } else if (menuItem.isSeparator()) {

                item = new MenuItemImpl(menuItem, true);

            } /*
             * else check status(enabled/disabled and add to menu of node
             */ else {

                boolean enabled = (menuItem.getIconId().equals("T"));
                menuItem.setEnabled(enabled);
                //menuItem.setEnabled(dispatcher.isMenuItemEnabled(menuItem.getToken()));
                item = new MenuItemImpl(menuItem, false);

            }

            list.add(item);

        }

        log.info("End TreeBase.getNodeMenu");
        return new MenuItemImpl(new MenuItemsImpl(list), main);
    }

    /**
     * @param text: this is only a dummy under which to Store the real menu
     * @return
     */
    private MenuEntryRecord makeBase(String text) {

        StructMap map = new StructMap("STB$MenuEntry$Record");
        DatabaseFieldMap dummy = new DatabaseFieldMap("SDISPLAY", "java.lang.String", 12, 255, ".STB$MenuEntry$RECORD", text);
        map.put("SDISPLAY", dummy);
        dummy = new DatabaseFieldMap("STYPE", "java.lang.String", 12, 255, ".STB$MenuEntry$RECORD", "DISPLAY");
        map.put("STYPE", dummy);
        MenuEntryRecord ret = new MenuEntryRecord(map);
        ret.setEnabled(false);
        return ret;
    }

    /**
     * for each menuitem of type menu, attach there submenuitems. Might be menus
     * too so recursive calls might occur
     *
     * @param main the main menu for the submenuitems
     * @return a list of menuitems
     * @throws BackendException
     */
    private MenuItems getSubItems(MenuEntryRecord main) throws BackendException {

        ArrayList list = new ArrayList();
        int menuNumber = main.getId();
        // get submenus for this menu id
        MenuEntryList itemList = dispatcher.getMenuList(menuNumber);
        itemList.initIterator();
        //loop through the list
        while (itemList.hasNext()) {

            MenuEntryRecord menuItem = itemList.getNext();
            MenuItemImpl item = null;
            //if it is a menu, recurive call
            if (menuItem.isMenu()) {

                menuItem.setEnabled(true);
                item = new MenuItemImpl(getSubItems(menuItem), menuItem);

            } else if (menuItem.isSeparator()) {

                item = new MenuItemImpl(menuItem, true);

            } else {
                boolean enabled = (menuItem.getIconId().equals("T"));
                menuItem.setEnabled(enabled);
                //menuItem.setEnabled(dispatcher.isMenuItemEnabled(menuItem.getToken()));
                item = new MenuItemImpl(menuItem, false);

            }

            list.add(item);

        }

        return new MenuItemsImpl(list);
    }

    /**
     * @return true if there is a custom menu
     */
    public boolean hasCustomMenu() {
        return CustomMenu.hasCustomMenu();
    }

    /*
     * (non-Javadoc) Gets menus for the main menu bar. This is the My Account
     * menu and custom menus if implemented
     * 
     * @see com.utd.stb.inter.application.Base#getMainMenuBar()
     */
    public MenuItems getMainMenuBar() throws BackendException {

        if (CustomMenu.hasCustomMenu()) {

            List list = new ArrayList();
            CustomMenu customMenu = new CustomMenu(dispatcher.getCustomConnection());
            list = customMenu.getCustomMenu(list);
            return new MenuItemsImpl(list);

        }

        return null;
    }

    public TreeNodeRecord createTransportNode(String nodetype, String nodeid, String sessionid) {

        StructMap map = new StructMap("STB$TREENODE$RECORD");
        DatabaseFieldMap dummy = new DatabaseFieldMap("SNODETYPE", "java.lang.String", 12, 255, ".STB$TREENODE$RECORD", nodetype);
        map.put("SNODETYPE", dummy);
        dummy = new DatabaseFieldMap("SNODEID", "java.lang.String", 12, 255, ".STB$TREENODE$RECORD", nodeid);
        map.put("SNODEID", dummy);
        dummy = new DatabaseFieldMap("STOOLTIP", "java.lang.String", 12, 255, ".STB$TREENODE$RECORD", sessionid);
        map.put("STOOLTIP", dummy);
        TreeNodeRecord node = new TreeNodeRecord(map);
        log.debug(node);
        return node;
    }

    /*
     * (non-Javadoc) if action is a MenuItem, return dispatcher.callAction. If
     * action has to be confirmed first ask the user. If it is a
     * CustomMenuItemImpl execute the custom action. If it is a NodeAction
     * execute the default action
     * 
     * @see com.utd.stb.inter.application.Base#invokeAction(java.lang.Object)
     */
    public Object invokeAction(Object action) throws BackendException {
        log.debug(action);

        if (action instanceof MenuItem) {
            if (action instanceof MenuItemImpl) {
                MenuEntryRecord menu = ((MenuItemImpl) action).getData();
                if (menu.isConfirmationNeeded().equals("T")) {
                    //ask the user if we really should do this
                    menu.setConfirmationNeeded(false);
                    return new ConfirmActionMsgBox(dispatcher.getTranslation("msgConfirmTitle"),
                            menu.getConfirmText(),
                            menu.getDisplay(),
                            action,
                            this);
                }
                return dispatcher.callAction(menu, selectedNode);
            } else {
                if (action instanceof CustomMenuItemImpl) {
                    CustomMenuItemImpl menu = (CustomMenuItemImpl) action;
                    return menu.executeAction(selectedNode);
                }
            }
        }

        if (action instanceof NodeAction) {
            NodeAction nAct = (NodeAction) action;
            if (nAct.getType().equals(NodeAction.DBLCLICK_IN_DETAIL)) {
                TreeNodeRecord node = (TreeNodeRecord) nAct.getTargetNode();
                setNode(node, false);
                String token = node.getDefaultAction();
                log.debug(token);
                return token == null || token.equals("") ? null : dispatcher.getActionFromToken(token);
            }
            if (nAct.getType().equals(NodeAction.UPDATE_TREE)) {
                TreeNodeRecord node = (TreeNodeRecord) nAct.getTargetNode();
                dispatcher.reload(true, node.getNodeType(), node.getNodeId(), node.getTooltip() /*sessionid hidden*/);
            }
        }

        if (action instanceof BackendException) {
            throw (BackendException) action;
        }

        return null;
    }

    /**
     * inner class that implements the MenuItem interface
     *
     * @author rainer bruns Created 13.12.2004
     */
    class MenuItemImpl implements MenuItem {

        boolean seperator = false;
        private MenuItems subItems;
        private String display;
        private String iconId;
        private boolean enabled = true;
        /**
         * the real data object, containing all the info
         */
        private MenuEntryRecord theRealMenu;

        /**
         * separator
         */
        public MenuItemImpl() {

        }

        /**
         * popup
         */
        public MenuItemImpl(MenuItems subItems, MenuEntryRecord menu) {

            this.subItems = subItems;
            this.theRealMenu = menu;
            this.display = menu.getDisplay();
            this.iconId = menu.getIconId();
        }

        /**
         * item
         */
        public MenuItemImpl(MenuEntryRecord menu, boolean seperator) {

            this(null, menu);
            this.seperator = seperator;
            this.display = menu.getDisplay();
            this.enabled = menu.isEnabled();
            this.iconId = menu.getIconId();
        }

        public MenuItemImpl(MenuItems subItems, String display, String iconId) {

            this.subItems = subItems;
            this.display = display;
            this.iconId = iconId;
        }

        public String toString() {

            return getClass().getName() + "\tText:" + display + " icon:" + iconId;
        }

        public String getText() {

            return display;
        }

        public String getIconId() {
            return iconId;
        }

        public boolean isEnabled() {

            return enabled;
        }

        public boolean isSeparator() {

            return seperator;
        }

        public MenuItems getChildren() {

            return subItems;
        }

        public MenuEntryRecord getData() {

            return theRealMenu;
        }
    }

    /**
     * implemtation of the MenuItems interface. Holds all menu items for a node
     * or menu item object
     *
     * @author rainer bruns Created 13.12.2004
     */
    static class MenuItemsImpl implements MenuItems {

        List list;

        public MenuItemsImpl(List list) {

            this.list = list;
        }

        public int getSize() {

            return list.size();
        }

        public MenuItem get(int i) {

            return (MenuItem) list.get(i);
        }

        public int indexOf(MenuItem menuItem) {

            return list.indexOf(menuItem);
        }

    }

    /*
     * (non-Javadoc) the object with detail data for a node.
     * 
     * @see com.utd.stb.inter.application.Base#getDetailViewAction(com.utd.stb.inter.application.Node)
     */
    public DetailView getDetailViewAction(Node node) throws BackendException {

        // have to be preloaded:
        final Nodes nodes = getNodeChildsDetail(node);
        Map<Node, UserInputs> uis = null;
        if (nodes != null) {
            uis = new HashMap<>(nodes.getSize());
            for (int i = 0; i < nodes.getSize(); i++) {

                Node n = nodes.getNode(i);
                // System.out.println("n.getIconId() = " + n.getIconId()+" for "+n.getTranslation());
                uis.put(n, getNodeProperties(n));
            }
        }

        final Map finalUis = uis;
        return new DetailView() {

            public String getTitle() {

                return null;
            }

            public Nodes getNodes() {

                return nodes;
            }

            public UserInputs getNodeProperties(Node node) {

                return (UserInputs) finalUis.get(node);
            }
        };

    }

    public DetailView getDetailViewActionInBackground(Node node) throws BackendException {
        return getDetailViewAction(node);
    }

    /*
     * (non-Javadoc) ApplicationInfo contais user name, database server name,
     * user language, a title and the timeout
     * 
     * @see com.utd.stb.inter.application.Base#getApplicationInfo()
     */
    public ApplicationInfo getApplicationInfo() {

        return new ApplicationInfo() {

            public String getUserName() {

                return userData.get("Name").toString();
            }

            public Locale getUserLanguage() {

                return (Locale) userData.get("Lang");
            }

            public String getServerName() {

                return userData.get("Server").toString();
            }

            public String isNewMainXml() {

                return userData.get("IsNewMainXml").toString();
            }

            public String getApplicationTitle() {

                return StabberInfo.getApplicationInfo("ISR_NAME")
                        + " - ["
                        + userData.get("Fullname").toString()
                        + "/"
                        + StabberInfo.getApplicationInfo("STBOWNER")
                        + "@"
                        + userData.get("Alias").toString()
                        + "]";

            }

            public int getTimeoutInMinutes() {

                return (Integer) userData.get("Timeout");
            }

            @Override
            public int getDialogTimeoutInSec() {
                return (int) userData.get("DialogTimeout");
            }

            public String getServerAliasName() {

                return userData.get("Alias").toString();
            }

        };

    }

    /*
     * (non-Javadoc) return the GUI Persistance object
     * 
     * @see com.utd.stb.inter.application.Base#getGuiPersistance()
     */
    public GuiPersistance getGuiPersistance() {

        return persistance;
    }

    /*
     * (non-Javadoc) on application end, store GUI persistance data, and clean
     * up
     * 
     * @see com.utd.stb.inter.application.Base#shutDown(int)
     */
    public void shutDown(int reason) {

        try {
            dispatcher.savePersistance(persistance.save());
        } catch (Exception e) {
            e.printStackTrace();
        }

        try {
            dispatcher.shutdown(reason);
        } catch (Exception se) {
            se.printStackTrace();
        }

    }

    /* (non-Javadoc)
     * @see com.utd.stb.inter.application.Base#showDocVersionSeparately()
     */
    public boolean showDocVersionSeparately() {

        return dispatcher.showDocVersionSeparately();
    }

    /* (non-Javadoc)
     * @see com.utd.stb.inter.application.Base#checkReload()
     */
    public boolean checkReload() {
        return dispatcher.checkReload();
    }

    public boolean splitTree() {
        return dispatcher.getBooleanParameterValue("SPLIT_TREE");
    }

    public void commitTable(Object table) throws BackendException {
        dispatcher.commitTable(table);
    }

    public Object getMenuForJobs() {
        return dispatcher.getMenuForJobs();
    }

    public String getJobParameter(String parameterName, int jobId) throws BackendException {
        return dispatcher.getJobParameter(parameterName, jobId);
    }

    public JobsTableInfo getJobsTableInfo() {
        if (jobsTableInfo == null) {
            jobsTableInfo = new JobsTableInfo();
        }
//    System.out.println("jobsTableInfo = " + jobsTableInfo+ " "+Thread.currentThread().getStackTrace()[3]);
//        jobsTableInfo.setTreeBase(this);
        return jobsTableInfo;
    }

    public String getParameterValue(String parameter) {
        return dispatcher.getParameterValue(parameter);
    }

    public void setNode(Node node, boolean left) {
        try {
            dispatcher.setNode((TreeNodeRecord) node, left);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    /*not used anymore*/
    public void setNodeInBackground(Node node, boolean left) {
        log.info(node);
        long duration = System.currentTimeMillis();
        setNode(node, left);
        duration = System.currentTimeMillis() - duration;
        log.info(node + ", duration " + duration);
    }

    public byte[] getXml(Node selectedNode, MenuItem menuEntry, byte[] action)
            throws BackendException {
        Object bytes = dispatcher.getXml(((MenuItemImpl) menuEntry).getData(), null);
        return (byte[]) bytes;
    }

    public byte[] getXmlInBackground(Node selectedNode, MenuItem menuEntry, byte[] action)
            throws BackendException {
        return getXml(selectedNode, menuEntry, action);
    }

    public MenuItem createRibbonMenuEntry(String token) {

        MenuEntryRecord menuEntryRecord;
        try {
            menuEntryRecord = dispatcher.getMenuFromToken(token);
            return new MenuItemImpl(menuEntryRecord, false);
        } catch (BackendException ex) {
            return null;
        }
    }

    public MenuItem createRibbonMenuEntryInBackground(String token) {
        return createRibbonMenuEntry(token);
    }

    public LoaderWebserviceImpl getLoader() {
        return dispatcher.getLoader();
    }

    @Override
    public byte[] getNodeDetails() throws BackendException {
        Object bytes = dispatcher.getNodeDetails();
        if (bytes instanceof byte[]) {
            return (byte[]) bytes;
        }
        return null;
    }

    @Override
    public byte[] getNodeDetailsInBackground() throws BackendException {
        return getNodeDetails();
    }

    @Override
    public void updateTreeInBackground() {
        dispatcher.updateTree();
    }
}
