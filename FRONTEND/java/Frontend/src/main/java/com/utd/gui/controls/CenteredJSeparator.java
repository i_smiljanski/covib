package com.utd.gui.controls;

import javax.swing.*;
import java.awt.*;


/**
 * centered JSeparator, vert or horiz
 *
 * @author PeterBuettner.de
 */
public class CenteredJSeparator extends JSeparator {

    public void paintComponent(Graphics g) {

        int tx = 0;
        int ty = 0;
        Dimension s = getPreferredSize();
        if (s == null) s = new Dimension();

        if (getOrientation() == HORIZONTAL)
            ty = (getHeight() - s.height) / 2 + 1;
        else
            tx = (getWidth() - s.width) / 2 + 1;

        g.translate(tx, ty);
        super.paintComponent(g);
        g.translate(-tx, -ty);
    }
}