package com.utd.stb.gui.wizzard.choosers;

import com.jgoodies.forms.builder.DefaultFormBuilder;
import com.jgoodies.forms.layout.FormLayout;
import com.jidesoft.combobox.TableComboBox;
import com.jidesoft.combobox.TableComboBoxSearchable;
import com.jidesoft.grid.CellRendererManager;
import com.jidesoft.grid.SortableTable;
import com.jidesoft.grid.TableComboBoxCellEditor;
import com.jidesoft.grid.TableModelWrapperUtils;
import com.utd.gui.controls.BorderSplitPane;
import com.utd.gui.controls.table.BaseTable;
import com.utd.gui.controls.table.ListViewTools;
import com.utd.gui.controls.table.TableTools;
import com.uptodata.isr.gui.util.Colors;
import com.utd.gui.util.DialogUtils;
import com.utd.stb.back.data.client.AttributeList;
import com.utd.stb.back.data.client.SelectionRecord;
import com.utd.stb.gui.itemTools.DisplayableItemsTableModel;
import com.utd.stb.gui.itemTools.JTableCellEditor;
import com.utd.stb.gui.swingApp.exceptions.ExceptionHandler;
import com.utd.stb.inter.application.BackendException;
import com.utd.stb.inter.items.DisplayableItem;
import com.utd.stb.inter.items.DisplayableItemComboList;
import com.utd.stb.inter.items.DisplayableItems;
import com.utd.stb.inter.wizzard.formtypes.FormDataGridOptions;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import resources.IconSource;

import javax.swing.*;
import javax.swing.event.TableModelEvent;
import javax.swing.event.TableModelListener;
import javax.swing.table.AbstractTableModel;
import javax.swing.table.DefaultTableCellRenderer;
import javax.swing.table.TableCellEditor;
import javax.swing.table.TableColumn;
import java.awt.*;
import java.util.HashMap;
import java.util.Map;


/**
 * The Gui for FormDataGridOptions, a table with
 * filtering-popup-DisplayableItems that rereads all lists in a row if one
 * selection in the row change, so lists that depend on a selection in that row
 * are possible.
 * <p>
 *
 * @author PeterBuettner.de
 */

class GridChooser extends AbstractChooser {
    public static final Logger log = LogManager.getRootLogger();

    private FormDataGridOptions data;
    private JTable table;
    private JComponent rowHeader;

    Map<String, Boolean> changeable = new HashMap<String, Boolean>();
    Map<String, Boolean> editable = new HashMap<String, Boolean>();
    //Map<String, String> values = new HashMap<>();

    public GridChooser(ExceptionHandler exceptionHandler,
                       FormDataGridOptions formDataGridOptions) {

        super(exceptionHandler);
        this.data = formDataGridOptions;
    }// ---------------------------------------------------------------------

    public JComponent getContent() {

        FormDataGridOptionsTableModel model = new FormDataGridOptionsTableModel(data);
        table = createTable(model);
        model.addTableModelListener(new TableModelListener() {// after
            // createTable?

            public void tableChanged(TableModelEvent e) {

                fireValidationChanged();
            }
        });

        rowHeader = createTableRowHeader();
        TableTools.autoSizeColumns(table, false);

        return getComponentPanel();
    }// ---------------------------------------------------------------------

    public void saveData() throws BackendException {
        //by exit from field the value is not going to lose
        table.requestFocusInWindow();
        table.editCellAt(0, 2);

        data.saveData();
    }// ---------------------------------------------------------------------

    public boolean isValid() {

        // run though all combos and ask them:
        int rows = data.getGridRowCount();
        int cols = data.getGridColumnCount();
        for (int r = 0; r < rows; r++)
            for (int c = 0; c < cols; c++) {
                DisplayableItemComboList cl = data.getValue(r, c);
                if (!cl.isEmptySelectionAllowed() && cl.getSelectedItem() == null)
                    return false;
            }
        return true;
    }

    /**
     * Builds Panel &amp; Layout Preconditions: components (table/header) build
     *
     * @return
     */
    private JComponent getComponentPanel() {

        // setup FormLayout Builder ..........................
        FormLayout layout = new FormLayout("F:min:G", "P, 2dlu, F:min:G");
        DefaultFormBuilder builder = DialogUtils.getFormBuilder(layout);

        // Label Row ......................................
        builder.append(buildComponentLabel(data.getLabel(), table, 0));
        builder.nextLine(2);

        // component Row .................................
        JScrollPane scrollPane = new JScrollPane(table);
        scrollPane.setRowHeaderView(rowHeader);
        scrollPane.setCorner(JScrollPane.UPPER_LEFT_CORNER, ((JTable) rowHeader).getTableHeader());
        //scrollPane.setCursor(Cursor.getPredefinedCursor(Cursor.WAIT_CURSOR));

        BorderSplitPane sp = new BorderSplitPane(scrollPane, null, false);
        builder.append(sp);

        TableColumn tc = ((BaseTable) rowHeader).getColumnModel().getColumn(0);
        /*
         * not too wide, this construct can't show a scrollbar there and the
         * sizing grip may get out of users reach 300 is a reasonable size,
         * should fit on any screen
         */
        // MCD, 01.Feb 2006, eventuell wieder einkommentieren
        //tc.setMaxWidth( 500 );

        return builder.getPanel();
    }// ------------------------------------------------------------------

    private TableCellEditor getCellEditorModelColumn(int row, int column) {
        TableCellEditor tableCellEditor = null;
        DisplayableItemComboList cbl = data.getValue(row, column);

        if (cbl != null) {
            DisplayableItems items = null;
            try {
                // MCD 28.Nov 2005, Wait Cursor
                table.setCursor(Cursor.getPredefinedCursor(Cursor.WAIT_CURSOR));
                items = cbl.getAvailableItems();
                log.debug(row + "x" + column + ": " + items.getSize());
                table.setCursor(Cursor.getDefaultCursor());
            } catch (BackendException e) {
                exceptionHandler.exceptionHandler(e, true);
            }

           /* for (int i = 0; i < items.getSize(); i++) {
                DisplayableItem item = items.get(i);
                log.debug(item.getData() + "; " + ((AttributeList) item.getAttributeList()).getSize());
            }*/
            if (items.getSize() != 0) {
                // MCD 26.Aug 2008, Grid editable
                boolean editable = false;

                DisplayableItem item = items.get(0);
                if (item != null) {
                    AttributeList attrList = (AttributeList) item.getAttributeList();
                    editable = attrList.isEditable();
                }
                if (editable) {
                    JTextField fake = new JTextField();
                    fake.setText("test");
                    tableCellEditor = new JTableCellEditor(fake);
                }
                // else return new JTableFilteringCellEditor(new JTextField(), new DisplayableItemsTableModel( items ));
                else {
                    final String key = row + "&" + column;

                    final DisplayableItemsTableModel itemsTableModel = new DisplayableItemsTableModel(items) /*{
                        @Override
                        public Object getValueAt(int row, int col) {
//                            return items.get(row);
                            if (items.get(row) instanceof DisplayableItem) {
                                DisplayableItem rowItem = items.get(row);
                                if (rowItem == null) return null;
                                if (col == -1 || col > 1) return rowItem;
                                if (col == 0) return rowItem.getData();
                                if (col == 1) return rowItem.getInfo();

                            }
                            return null;
                        }
                    }*/;
                    tableCellEditor = new TableComboBoxCellEditor(itemsTableModel) {

                        @Override
                        public Component getTableCellEditorComponent(JTable table, Object value, boolean isSelected, int row, int col) {

                            Component component = super.getTableCellEditorComponent(table, value, isSelected, row, col);
                            if (component instanceof TableComboBox) {
                                TableComboBox tableComboBox = (TableComboBox) component;

                                tableComboBox.setMaximumRowCount(16);
                                tableComboBox.setValueColumnIndex(0);

                                Object data = table.getModel().getValueAt(row, col);
                                Object attrList = null;
                                log.debug(row + " " + col + " " + data);

                                if (data instanceof DisplayableItem) {
                                    attrList = ((DisplayableItem) data).getAttributeList();
                                    // data = ((DisplayableItem) data).getData();
                                }

                                if (!changeable.containsKey(key)) {
                                    changeable.put(key, attrList != null && ((AttributeList) attrList).isChangeable());
                                }
                                tableComboBox.setEditable(changeable.get(key));
                                log.debug(key + " changeable=" + tableComboBox.isEditable());
                                if (data instanceof SelectionRecord) {
                                    log.debug("display: " + ((SelectionRecord) data).getDisplay() + "; info: "
                                            + ((SelectionRecord) data).getInfo()
                                            + "; value: " + ((SelectionRecord) data).getValue());

                                }
                                // setSelectedItem takes always display because StructMap.toString shows sdisplay
                                tableComboBox.setSelectedItem(data);
                                new TableComboBoxSearchable(tableComboBox);

                                return tableComboBox;
                            }

                            return component;
                        }
                    };
                }

            }
        }

        // nothing available
        UIManager.getLookAndFeel().provideErrorFeedback(null);

        return tableCellEditor;
    }


    private JTable createTable(FormDataGridOptionsTableModel model) {

        JTable tab = new /* ListViewSized */BaseTable(model) {
            @Override
            public TableCellEditor getCellEditor(int row, int column) {
//               return super.getCellEditor(row, column);
                return GridChooser.this.getCellEditorModelColumn(row, convertColumnIndexToModel(column));
            }

            @Override
            public boolean isCellEditable(int row, int column) {
                String key = row + "&" + column;
                if (!editable.containsKey(key)) {
                    editable.put(key, super.isCellEditable(row, column));
                }
                log.debug(key + " editable=" + editable.get(key));

                return editable.get(key);
            }
        };

        ((SortableTable) tab).setSortable(false);


        tab.setSurrendersFocusOnKeystroke(true);// needed for popupeditor
        tab.setSelectionMode(ListSelectionModel.SINGLE_SELECTION);
        tab.setColumnSelectionAllowed(true);
        tab.setRowSelectionAllowed(true);
        //		table.setShowGrid(true); // we are no _list_view, more like a table
        tab.setAutoResizeMode(JTable.AUTO_RESIZE_OFF);
        tab.getTableHeader().setReorderingAllowed(false);// right-columns are
        // reset on leftmost
        // change
        //		table.getColumnModel().setColumnMargin(0);// looks better? grid is
        // now aligned, but cells paint over gridlines

        tab.setDefaultRenderer(String.class, new RowHeaderRenderer()); // labels
        tab.setDefaultRenderer(DisplayableItem.class, new ItemCellRenderer());
        CellRendererManager.registerRenderer(String.class, new RowHeaderRenderer());
        CellRendererManager.registerRenderer(DisplayableItem.class, new ItemCellRenderer());

        return tab;
    }// ------------------------------------------------------------------

    private JTable createTableRowHeader() {

        JTable header = new BaseTable(new RowHeaderTableModel(data)) {

            public Dimension getPreferredScrollableViewportSize() {

                return getPreferredSize();
            }
        };

        ((SortableTable) header).setSortable(false);
        header.setFocusable(false);
        header.setDefaultRenderer(String.class, new RowHeaderRenderer());
        CellRendererManager.registerRenderer(String.class, new RowHeaderRenderer());

        // sizing after renderer set!
        TableTools.autoSizeColumn(header, 0, false);

        return header;
    }// --------------------------------------------------------

    /**
     * little helper to show nonscrollable, nonfocussable rowheader
     */
    private final static class RowHeaderTableModel extends AbstractTableModel {

        FormDataGridOptions data;

        public RowHeaderTableModel(FormDataGridOptions data) {

            this.data = data;
        }

        public String getColumnName(int column) {

            try {
                return " " + data.getColumnTitle(-1);
            } catch (BackendException e) {
                return " ";
            }

        }

        public int getColumnCount() {

            return 1;
        }

        public int getRowCount() {

            return data.getGridRowCount();
        }

        public Object getValueAt(int row, int col) {
            Object o = (col != 0) ? null : data.getRowTitle(row);
//            log.debug(o);
            return o;
        }

        public Class getColumnClass(int col) {

            return String.class;
        }
    }

    //	 ###############################################################################

    private static class FormDataGridOptionsTableModel extends AbstractTableModel {

        FormDataGridOptions data;

        public FormDataGridOptionsTableModel(FormDataGridOptions data) {

            this.data = data;
        }

        public int getColumnCount() {

            return data.getGridColumnCount();
        }

        public int getRowCount() {

            return data.getGridRowCount();
        }

        public String getColumnName(int col) {

            try {

                return data.getColumnTitle(col);

            } catch (BackendException be) {

                return be.getLocalizedMessage();

            }
        }

        public Class getColumnClass(int col) {

            return DisplayableItem.class;
        }

        public boolean isValid(int row, int col) {

            DisplayableItemComboList cl = data.getValue(row, col);
            return cl == null || cl.isEmptySelectionAllowed()
                    || (cl.getSelectedItem() != null);
        }

        @Override
        public Object getValueAt(int row, int col) {
            DisplayableItemComboList cl = data.getValue(row, col);
            return cl == null ? null : cl.getSelectedItem();
        }

        @Override
        public void setValueAt(Object value, int row, int col) {
            if (value != null && !(value instanceof DisplayableItem || value instanceof String))    /*MCD 26.Aug 2008, Grid editable*/
                throw new IllegalArgumentException("GridChooser: Error, unexpected class: " + value.getClass().getName());

            DisplayableItemComboList cbl = data.getValue(row, col);
            if (cbl == null)
                throw new IllegalArgumentException("GridChooser, Error, DisplayableItemComboList:" + row + "/" + col + " is null");

            DisplayableItem oldValue = cbl.getSelectedItem();
            /*
             * we only set value on real change, since the editor/table/whoever
             * maybe lazy and send a value-changed-message with the 'old' value
             */
            if (oldValue == null && value == null) return; // both null -> same
            if (value != null && value.equals(oldValue)) return;

            // MCD 26.Aug 2008, Grid editable
            /*boolean editable = false;
            if (oldValue != null) {
                AttributeList attrList = (AttributeList) oldValue.getAttributeList();
                if (attrList != null) editable = attrList.isEditable();
            }*/
//               log.debug("setValueAt "+ value );
            if (value instanceof String) { //&& editable) {
                SelectionRecord selection = (SelectionRecord) oldValue;
                selection.setValue(value.toString());
                selection.setDisplay(value.toString());
                cbl.setSelectedItem(selection);
            } else if (value instanceof DisplayableItem) {
                cbl.setSelectedItem((DisplayableItem) value);
            }

            // we known: row may have changed in the whole:
            fireTableRowsUpdated(row, row);
        }
    }

    // ###############################################################################

    private static class RowHeaderRenderer extends DefaultTableCellRenderer {

        public Component getTableCellRendererComponent(JTable table, Object value,
                                                       boolean isSelected, boolean hasFocus,
                                                       int row, int column) {

            Component lbl = super.getTableCellRendererComponent(table, value, isSelected,
                    false, row, column);
            ListViewTools.changeDefaultTableCellRenderer(lbl, hasFocus);
            lbl.setBackground(Colors.getItemHiliteInactiveBack());
            lbl.setForeground(Colors.getItemHiliteInactiveText());
            setHorizontalAlignment(JLabel.RIGHT);
            setToolTipText(value == null ? "" : value.toString());
            return lbl;
        }
    }

    // ###############################################################################
    private static class ItemCellRenderer extends DefaultTableCellRenderer {

        private IconSource is;

        public ItemCellRenderer() {

            this.is = new IconSource();
        }

        public Component getTableCellRendererComponent(JTable table,
                                                       Object value,
                                                       boolean isSelected,
                                                       boolean hasFocus,
                                                       int row,
                                                       int column) {

            // test if invalid empty
            boolean valid = ((FormDataGridOptionsTableModel) TableModelWrapperUtils.getActualTableModel(table.getModel()))
                    .isValid(row, table.convertColumnIndexToModel(column));

            AttributeList attrList = null;
            if (value instanceof DisplayableItem) {
                attrList = (AttributeList) ((DisplayableItem) value).getAttributeList();
                value = ((DisplayableItem) value).getData();
            } else if (value != null) {
                log.debug(value.getClass());
            }
            if (attrList != null) {
                // COLOR
                Color color = valid ? attrList.getColor() : Colors.getDataBackIllegalEmpty();
                if (!isSelected && !hasFocus) this.setBackground(color);

                // HIGHLIGHT
                Font font = attrList.getHighlighted();
                if (font != null) this.setFont(font);

                // ICON
                String icon = attrList.getIcon();
                if (column == 0) this.setIcon(is.getSmallIconByID(icon));
                if (column != 0) this.setIcon(null);
            }

//            log.debug(value );
            JLabel lbl = (JLabel) super.getTableCellRendererComponent(table,
                    value,
                    isSelected,
                    false,
                    row,
                    column);
            ListViewTools.changeDefaultTableCellRenderer(lbl, hasFocus);
//            if(value == null) log.debug(row+" "+column);
            return lbl;
        }
    }

    /* (non-Javadoc)
     * @see com.utd.stb.gui.wizzard.choosers.AbstractChooser#getFilterFieldValue()
     */
    public String getFilterFieldValue() {
        return null;
    }

    /* (non-Javadoc)
     * @see com.utd.stb.gui.wizzard.choosers.AbstractChooser#setFilterFieldValue()
     */
    public void setFilterFieldValue(String regExpr) {
    }


}