package com.utd.stb.back.data.client;

import com.utd.stb.back.database.BackendErrorException;
import com.utd.stb.back.database.BackendWorkflowException;
import com.utd.stb.back.ui.impl.wizard.StandardWizardErrorException;
import com.utd.stb.gui.swingApp.exceptions.MessageStack;
import com.utd.stb.inter.application.BackendException;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.util.ArrayList;
import java.util.List;


/**
 * in StructMap is "T", else false.
 *
 * @author rainer bruns Created 13.01.2004
 */
public class ErrorRecord {
    public static final Logger log = LogManager.getRootLogger();

    /**
     * Comment for <code>cnSeverityFatal</code> Severity for Fatal
     */
    public static int cnSeverityFatal = -1;


    /**
     * Comment for <code>cnNoError</code> Severity for No error
     */
    public static final int cnNoError = 0;
    /**
     * Comment for <code>cnSeverityCritical</code> Severity for critical
     * errors
     */
    public static final int cnSeverityCritical = 1;
    /**
     * Comment for <code>cnSeverityWarning</code> Severity for warnings
     */
    public static final int cnSeverityWarning = 2;
    /**
     * Comment for <code>cnSeverityMessage</code> Severity for informational
     * messages
     */
    public static final int cnSeverityMessage = 3;

    /**
     * Comment for <code>cnAudit</code> Severity if auditing is required
     */
    public static final int cnAudit = 5;
    /**
     * Comment for <code>cnFirstForm</code> Indicates that ReportWizard has to
     * be started
     */
    public static final int cnFirstForm = 6;

    /**
     * Comment for <code>cnMsgWarning</code>/<code>cnMsgCritical</code>/<code>cnMsgWarningTimeout</code>
     * This is used if message warns the user (with or without timeout) or rather stops frontend
     */
    public static final int cnMsgWarningTimeout = 4;
//    public static int cnMsgCritical = 7;

    /**
     * Comment for <code>cnWizardValidateWarn</code>/<code>cnWizardValidateStop</code>
     * This is used if an user selection warns the user or rather stops the wizard
     */
    public static final int cnWizardValidateWarn = 8;
    public static final int cnWizardValidateStop = 9;


    public final static String MANIPULATION = "manipulation";
    StructMap dataMap;

    /**
     * @param in the db data representation
     * @see StructMap
     */
    public ErrorRecord(StructMap in) {
        dataMap = in;
        Object value = dataMap.getValue("TLOMESSAGELIST");
        if (value != null) {
            ArrayList array = ((ArrayMap) value).getArray();
            MessageList messages = new MessageList();
            for (Object anArray : array) {
                MessageRec message = new MessageRec((StructMap) anArray);
                messages.add(message);
            }
            setMessageList(messages);
        }
    }

    /**
     * @return the value of field SERRORMESSAGE
     * @see StructMap#getValueAsString(String)
     */
    public String getErrorMsg() {

        return dataMap.getValueAsString("SERRORMESSAGE");
    }

    /**
     * @return the value of field SERRORCODE
     * @see StructMap#getValueAsString(String)
     */
    public String getErrorCode() {
        return dataMap.getValueAsString("SERRORCODE");
    }

    /**
     * @return the value of field SSEVERITYCODE
     * @see StructMap#getValueAsInt(String)
     */
    public int getSeverity() {

        return dataMap.getValueAsInt("SSEVERITYCODE");
    }


    /**
     * @return the value of underlying field TLOMESSAGELIST
     */
    public MessageList getMessageList() {
        Object o = dataMap.getValue("TLOMESSAGELIST");
        if (o instanceof MessageList)
            return (MessageList) o;
        else
            return null;
    }

    /**
     * sets the value of underlying field TLOMESSAGELIST
     *
     * @param messageList the attributeList to be set
     */
    public void setMessageList(MessageList messageList) {
        dataMap.setValue("TLOMESSAGELIST", messageList);
    }

    /**
     * checks the Severity code. If severity is Critical, Warning or Message
     * throws the Exception
     *
     * @throws BackendErrorException
     */
    public void errCheck() throws BackendErrorException {
        int severity = getSeverity();
//        log.debug(severity);
        if (severity == cnSeverityCritical
                || severity == cnSeverityFatal
                || severity == cnSeverityWarning
                || severity == cnSeverityMessage
                || severity == cnMsgWarningTimeout
//                || severity == cnMsgCritical
                ) {
            throw new BackendErrorException(this);
        } else if (severity == cnWizardValidateStop
                || severity == cnWizardValidateWarn) {
            throw new StandardWizardErrorException(this);
        } else if (severity == cnFirstForm
                || severity == cnAudit) {
            throw new BackendWorkflowException(this);
        }
    }

    /**
     * @return the Severity codes for the GUI, that for some reasons like to
     * have another coding.
     * @see BackendException
     */
    public int getSeverityForGui() {
        int severity = getSeverity();
        return getSeverityForGui(severity);
    }

    public static int getSeverityForGui(int severity) {
        if (severity == cnSeverityFatal) return BackendException.SEVERITY_APP_EXIT;
        if (severity == cnSeverityWarning) return BackendException.SEVERITY_WARNING;
//        if (severity == cnMsgCritical) return BackendException.SEVERITY_ACTION_EXIT;
        if (severity == cnSeverityCritical) return BackendException.SEVERITY_ACTION_EXIT;
        if (severity == cnSeverityMessage) return BackendException.SEVERITY_INFORMATIONAL;
        if (severity == cnMsgWarningTimeout) return BackendException.SEVERITY_INFORMATIONAL;
        if (severity == cnWizardValidateStop) return StandardWizardErrorException.SEVERITY_WIZARD_ACTION_EXIT;
        if (severity == cnWizardValidateWarn) return StandardWizardErrorException.SEVERITY_WIZARD_INFORMATIONAL;
        return BackendException.SEVERITY_ACTION_EXIT;
    }


//    public boolean isError(){
//        int severity = getSeverity();
//        return severity != cnFirstForm && severity != cnAudit;
//    }

    public MessageStack getMessageStack() {
        MessageList msgList = getMessageList();
        if (msgList != null) {
            List<String> userStack = new ArrayList<String>();
            List<String> implStack = new ArrayList<String>();
            List<String> devStack = new ArrayList<String>();
            for (int i = 0; i < msgList.getSize(); i++) {
                MessageRec msgRec = msgList.get(i);
//                log.debug(msgRec);
                userStack.add(msgRec.getUserStack());
                implStack.add(msgRec.getImplStack());
                devStack.add(msgRec.getDevStack());
            }
            if (userStack.size() > 0 || implStack.size() > 0 || devStack.size() > 0) {
                return new MessageStack(userStack, implStack, devStack);
            }
        }
        return null;
    }

    @Override
    public String toString() {
        MessageList messageList = getMessageList();
        return "ErrorRecord{" +
                "dataMap=" + dataMap +
                ", message list size=" + (messageList != null ? messageList.getSize() : null) +
                '}';
    }
}