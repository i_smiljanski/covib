package com.utd.stb.inter.dialog;

/**
 * Info for displaying a big HeaderBar, for example in a Dialog
 * <p>
 * <pre>
 *
 *  -----------------------------------------------------------
 *  |                                              ---------   |
 *  |  TitleLine (bold)                            |       |   |
 *  |       Text, (plain text), foo Lorem          | icon  |   |
 *  |       ipsum dolor sit amet, consectetuer     |       |   |
 *  |       adipiscing nibh euismod tincidunt      ---------   |
 *  |                                                          |
 *  ------------------------------------------------------------
 *
 * </pre>
 *
 * @author PeterBuettner.de
 */
public interface HeaderBar {

    /**
     * The "bold" title line
     *
     * @return
     */
    String getTitleLine();

    /**
     * "non bold" informational text
     *
     * @return
     */
    String getText();

    /**
     * if null a default Icon (context dependent) will be used, if "" no icon.
     *
     * @return
     */
    String getIconID();
}