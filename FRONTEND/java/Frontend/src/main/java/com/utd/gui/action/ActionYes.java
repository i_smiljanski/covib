package com.utd.gui.action;

import com.utd.gui.resources.I18N;

import javax.swing.*;


public abstract class ActionYes extends AbstractAction {

    public ActionYes() {

        ActionUtil.initActionFromMap(this, I18N.getActionMap(ActionYes.class.getName()),
                null);
        putValue(SHORT_DESCRIPTION, null);
    }

}