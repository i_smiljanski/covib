package com.utd.gui.event;

import javax.swing.*;
import javax.swing.text.Document;
import javax.swing.text.JTextComponent;
import java.awt.*;
import java.awt.event.FocusAdapter;
import java.awt.event.FocusEvent;


/**
 * selects all in a JTextComponent the moment it gains focus, but only if
 * nothing is currently selected. One instance maybe shared between all
 * components. Usefull for readonly textcomponents to work like in windows: see
 * Explorer -> FilePorperties and 'Tab' into the textfields
 *
 * @author PeterBuettner.de
 */
public class SelectAllFocusListener extends FocusAdapter {

    public void focusGained(FocusEvent e) {

        if (e.isTemporary()) return;
        Component c = e.getComponent();
        if (!(c instanceof JTextComponent)) return;
        final JTextComponent tc = (JTextComponent) c;

        Document doc = tc.getDocument();
        if (doc == null) return;// empty
        final int len = doc.getLength();
        if (len == 0) return; // empty

        int start = tc.getSelectionStart();
        int end = tc.getSelectionEnd();
        if (start != 0 || end != 0) return; // anything is selected

        /*
         * we have to invoke it later since JFormattedTextFields won't show the
         * selection, i think they set it by themself, but since we probably
         * only work on readOnlys this new behaviour won't disturb anything
         */

        SwingUtilities.invokeLater(new Runnable() {

            public void run() {

                tc.selectAll();
            }
        });

    }

}