package com.utd.stb.inter.userinput;

/**
 * TODO exceptions or silently eat it?
 *
 * @author PeterBuettner.de
 */
public class TextInputImpl implements TextInput {

    private boolean multiLineAllowed;
    private boolean secret;
    private boolean fileChooserField;
    private Integer maxLen;
    private String data;

    /**
     * @param initialText
     * @param maxLen      -1 for unlimited
     * @param multiLine   accept newlines or not, if secret==true it probably will be
     *                    ignored since guis rarely have a multiline passwort area
     * @param secret      if true a passwortfield will be created
     * @param true        if true a filechooserfield will be created
     */
    public TextInputImpl(String initialText, int maxLen, boolean multiLine, boolean secret, boolean fileChooserField) {

        this.secret = secret;
        this.multiLineAllowed = multiLine;
        this.fileChooserField = fileChooserField;
        if (maxLen != -1) this.maxLen = new Integer(maxLen);
        setText(initialText);
    }

    public TextInputImpl(String initialText, int maxLen, boolean multiLine, boolean secret) {

        new TextInputImpl(initialText, maxLen, multiLine, secret, false);
    }

    /**
     * @param initialText
     * @param maxLen      -1 for unlimited
     * @param multiLine   accept newlines or not
     */
    public TextInputImpl(String initialText, int maxLen, boolean multiLine) {

        this(initialText, maxLen, multiLine, false);
    }

    /**
     * Get a Single line Text UI
     *
     * @param initialText
     * @param maxLen      -1 for unlimited
     */
    public TextInputImpl(String initialText, int maxLen) {

        this(initialText, maxLen, false, false);
    }

    public String getText() {

        return data;
    }

    public void setText(String text) {

        if (maxLen != null && text.length() > maxLen.intValue())

            throw new IllegalArgumentException("Text to long! Is:" + maxLen);

        data = text;
    }

    public Integer getMaxLength() {

        return maxLen;
    }

    public boolean isMultiLineAllowed() {

        return multiLineAllowed;
    }

    public boolean isSecret() {

        return secret;
    }

    public boolean isFileChooserField() {

        return fileChooserField;
    }
}