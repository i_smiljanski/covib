package com.utd.stb.back.ui.impl.userinputs.fields;

import com.utd.stb.back.data.client.PropertyRecord;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import javax.imageio.ImageIO;
import java.awt.image.BufferedImage;
import java.io.ByteArrayOutputStream;
import java.io.IOException;

/**
 * @author Inna Smiljanski  created 01.12.2010
 */
public class ImageUserInput extends AbstractUserInput {
    public static final Logger log = LogManager.getRootLogger();

    PropertyRecord property;
    BufferedImage image;

    /**
     * @param label    Label of field
     * @param image
     * @param property Record containing complete information for field
     */
    public ImageUserInput(String label, BufferedImage image, PropertyRecord property) {
        super(label);
        this.property = property;
        this.image = image;
    }

    public void setData(Object data) {
        image = (BufferedImage) data;
        setValue();
    }

    public Object getData() {
        return image;
    }

    public Class getType() {
        return BufferedImage.class;
    }

    /**
     * set the value in db representation record
     */
    private void setValue() {
//         log.debug("setValue: " + image);
        if (image != null) property.setBlobValue(getImageByteData());
    }

    public PropertyRecord getProperty() {
        setValue();
        return property;
    }

    private byte[] getImageByteData() {
        byte[] imageData = null;
        try {
            ByteArrayOutputStream byteArrayOutputStream = new ByteArrayOutputStream();
            ImageIO.write(image, "jpeg", byteArrayOutputStream);          //todo:jpeg
            imageData = byteArrayOutputStream.toByteArray();

        } catch (IOException e) {
            log.error(e);
        }
        return imageData;
    }
}
