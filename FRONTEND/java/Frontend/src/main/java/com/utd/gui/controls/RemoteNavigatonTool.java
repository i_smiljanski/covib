package com.utd.gui.controls;

import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.KeyEvent;


/**
 * If a Component (e.g. TextField) want's to control the selection of a
 * JList/JTable (others not implemented for now, but it's easy) this will help
 * you - e.g. used for filters. The navigationkeys (up,down,pageUp,pageDown)
 * pressed in the navigation-textfield will change the selection in the target
 * (JList, JTable)
 *
 * @author PeterBuettner.de
 */
public class RemoteNavigatonTool {

    /**
     * set up action and input map of controller to move selection in target
     * (JList or JTable) Keys are: VK_UP, VK_DOWN, VK_PAGE_UP, VK_PAGE_DOWN
     *
     * @param controller
     * @param target
     */

    public static void putAllActions(JComponent controller, JComponent target) {

        InputMap im = controller.getInputMap();
        ActionMap am = controller.getActionMap();
        putAction(target, im, am, KeyEvent.VK_UP);
        putAction(target, im, am, KeyEvent.VK_DOWN);
        putAction(target, im, am, KeyEvent.VK_PAGE_UP);
        putAction(target, im, am, KeyEvent.VK_PAGE_DOWN);
    }

    private static void putAction(JComponent target, InputMap im, ActionMap am, int keyCode) {

        NavigationalAction na = null;
        if (target instanceof JTable)
            na = new NavigationalAction((JTable) target, keyCode);
        else if (target instanceof JList)
            na = new NavigationalAction((JList) target, keyCode);
        else
            throw new IllegalArgumentException("Only JList and JTable allowed, is: "
                    + target.getClass());

        im.put(KeyStroke.getKeyStroke(na.keyCode, 0), na);
        am.put(na, na);

    }

    static class NavigationalAction extends AbstractAction {

        int keyCode;
        // hack: only one is not null, the target
        JTable table;
        JList list;

        NavigationalAction(JTable table, int keyCode) {

            this.table = table;
            this.keyCode = keyCode;
        }

        NavigationalAction(JList list, int keyCode) {

            this.list = list;
            this.keyCode = keyCode;
        }

        public void actionPerformed(ActionEvent ev) {

            /*
             * NEW! much easier, 'retarget' the keyevent, may be more flexible
             * so we may use other components (Tree,etc). We may provide a
             * mapping from VK_DOWN to VK_PAGEDOWN etc. Or, even better(?) use
             * the action-string-commands defined in the look and feel. All the
             * same.
             * 
             * 
             * PROBLEM: if nothing is selected, the first 'DOWN' will select row
             * 2, or if not first usage there may happen to be selected anything
             * on the first/second navigation.
             *  
             */
            //		Component target = table!=null?(Component)table :(
            // list!=null?(Component)list:null);
            //		if(target!=null){
            //			KeyEvent ke = new
            // KeyEvent(target,KeyEvent.KEY_PRESSED,ev.getWhen(),ev.getModifiers(),keyCode,
            // KeyEvent.CHAR_UNDEFINED);
            //			SwingUtilities.processKeyBindings(ke);
            //			return;
            //			}
            /* OLD working fine */

            // ... JTable .............................
            if (table != null) {
                int i = getNextIndexTable(table);
                if (i >= 0 && i < table.getRowCount()) {
                    table.setRowSelectionInterval(i, i);
                    Rectangle r = table.getCellRect(i, 0, true);
                    table.scrollRectToVisible(r);
                }
                return;
            }

            // ... JList .............................
            if (list != null) {
                int i = getNextIndexList(list);
                if (i >= 0 && i < list.getModel().getSize()) {
                    list.setSelectedIndex(i);
                    list.ensureIndexIsVisible(i);
                }
                return;
            }
        }// ------------------------------------------------------------

        // page increment may be different on every row, since rowheigths may be
        // different
        private int getNextIndexTable(JTable table) {

            int sel = table.getSelectedRow(); // maybe -1 !
            int max = table.getRowCount() - 1; // maybe -1 !

            // if you argue blockscroll will be 1 of: it's the same as scrolling
            // in table
            // look in BasicTableUI#PagingAction they do it the same way
            int pageRowDelta = 10;
            if (keyCode == KeyEvent.VK_PAGE_UP || keyCode == KeyEvent.VK_PAGE_DOWN) {
                int up = keyCode == KeyEvent.VK_PAGE_UP ? -1 : 1;

                Component parent = table.getParent();
                int pageSize = parent == null ? 400 : parent.getSize().height; // silly
                // default
                Rectangle selRect = table.getCellRect(sel, 0, true);
                int target = selRect.y + pageSize * up;
                int newRow = table.rowAtPoint(new Point(1, target));
                if (newRow != -1)
                    pageRowDelta = newRow - sel; // page +- 1
                else
                    pageRowDelta = max * up; // max or below zero -> the range
                // will be fixed below:
            }

            return calcNewSelectionIndex(sel, max, pageRowDelta);
        }// ------------------------------------------------------------

        //	TODO page increment may be different on every row, since rowheigths
        // may be different
        private int getNextIndexList(JList list) {

            int sel = list.getSelectedIndex(); // maybe -1 !
            int max = list.getModel().getSize() - 1; // maybe -1 !

            int pageRowDelta = 0;
            if (keyCode == KeyEvent.VK_PAGE_UP || keyCode == KeyEvent.VK_PAGE_DOWN)
                pageRowDelta = list
                        .getVisibleRowCount()
                        * (keyCode == KeyEvent.VK_PAGE_UP
                        ? -1
                        : 1);

            return calcNewSelectionIndex(sel, max, pageRowDelta);
        }// ------------------------------------------------------------

        /**
         * pageRowDelta(+ or -) is only needed on PageUp/Down
         */
        private int calcNewSelectionIndex(int current, int max, int pageRowDelta) {

            switch (keyCode) {
                case KeyEvent.VK_PAGE_UP:
                    return Math.max(current + pageRowDelta, 0);
                case KeyEvent.VK_PAGE_DOWN:
                    return Math.min(current + pageRowDelta, max);
                case KeyEvent.VK_HOME:
                    return 0;
                case KeyEvent.VK_END:
                    return max;
                case KeyEvent.VK_UP:
                    return Math.max(current - 1, 0);
                case KeyEvent.VK_DOWN:
                    return Math.min(current + 1, max);
                default:
                    return current;
            }
        }// ------------------------------------------------------------
    }

}