package com.utd.stb.gui.swingApp;

import com.uptodata.isr.gui.lookAndFeel.WinTreeCellRenderer;
import com.utd.stb.inter.application.Node;
import resources.IconSource;

import javax.swing.*;
import java.awt.*;


/**
 * displays com.utd.stb.action.Node in a tree
 *
 * @author PeterBuettner.de
 */
public class NodeTreeCellRenderer extends WinTreeCellRenderer {

    private IconSource iconSource;

    public NodeTreeCellRenderer(IconSource iconSource) {

        this.iconSource = iconSource;
    }

    public Component getTreeCellRendererComponent(JTree tree, Object value, boolean sel,
                                                  boolean expanded, boolean leaf, int row,
                                                  boolean hasFocus) {

        if (!(value instanceof Node)) {
            super.getTreeCellRendererComponent(tree, value, sel, expanded, leaf, row,
                    hasFocus);
        } else {
            Node n = (Node) value;

            super.getTreeCellRendererComponent(tree, n.getText(), sel, expanded, leaf, row,
                    hasFocus);
            setIcon(iconSource.getSmallIconByID(n.getIconId()));
            // System.out.println(n.getTranslation()+" n.getTooltip() = " + n.getTooltip());
//            if(StringUtils.isNotEmpty(n.getTooltip()) )
            setToolTipText(n.getTooltip());
        }

        return this;
    }

}