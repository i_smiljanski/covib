package com.utd.stb.back.ui.impl.userinputs.fields;

import com.utd.stb.back.data.client.PropertyRecord;


/**
 * Implementation of UserInput for boolean fields
 *
 * @author rainer bruns Created 02.12.2004
 */
public class BooleanUserInput extends DirectUserInput {

    PropertyRecord property;

    /**
     * @param label    field Label
     * @param value    field Value
     * @param property complete record of field information from database
     */
    public BooleanUserInput(String label, Boolean value, PropertyRecord property) {

        super(label, Boolean.class, value);
        //what is empty for a checkbox??
        setEmptyAllowed(false);
        this.property = property;
        super.setReadOnly(property.isReadOnly());
    }

    /**
     * set the value in db representation record
     */
    private void setValue() {

        property.setBooleanValue((Boolean) getData());
    }

    /**
     * Is used when saving field to db
     *
     * @return the record to be stored to db
     * @see AbstractInputData#getListToSave()
     */
    public PropertyRecord getProperty() {

        setValue();
        return property;
    }
}