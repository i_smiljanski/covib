package com.utd.gui.filter;

import javax.swing.event.ChangeEvent;
import javax.swing.event.ChangeListener;
import javax.swing.event.TableModelEvent;
import javax.swing.event.TableModelListener;
import javax.swing.table.AbstractTableModel;
import javax.swing.table.TableModel;
import java.util.Arrays;


/**
 * Show only filterd data.
 * <p>
 * <b>ATTENTION: </b> changes in the base Model are only sparsely supported for
 * now, event ranges are not mapped; now it may be allright, without mapping a
 * bit slow(?) but JTable paints most times all new, problem maybe selection
 * </p>
 * <p>
 * <p>
 * <p>
 * Note to enhancers: since we filter the map is sorted, binarySearch etc. is
 * possible
 * </p>
 *
 * @author PeterBuettner.de
 */
public class FilterTableModel extends AbstractTableModel {

    private int filterColumn;
    private Filter filter;
    private TableModel model;

    /**
     * if null then fall through
     */
    private int[] rowMapToBase;

    private ChangeListener filterChanged = new ChangeListener() {

        public void stateChanged(ChangeEvent e) {

            updateData(true);
        }
    };

    /**
     * @param model          the base model
     * @param filterStrategy defines how it is filtered
     * @param filterColumn   column to filter
     */
    public FilterTableModel(TableModel model, Filter filterStrategy, int filterColumn) {

        this.model = model;
        this.filterColumn = filterColumn;
        this.filter = filterStrategy;
        this.filter.addChangeListener(filterChanged);
        model.addTableModelListener(new ATableModelListener());
        updateData(true);

    }

    /**
     * The column the filter is attached to
     *
     * @return
     */
    public int getFilterColumn() {

        return filterColumn;

    }

    /**
     * The column the filter is attached to
     *
     * @param filterColumn
     */
    public void setFilterColumn(int filterColumn) {

        this.filterColumn = filterColumn;
        updateData(true);
    }

    public Filter getFilter() {

        return filter;
    }

    /**
     * Set to null for null-filtering
     *
     * @param newFilter
     */
    public void setFilter(Filter newFilter) {

        if (filter != null) filter.removeChangeListener(filterChanged);
        filter = newFilter;
        filter.addChangeListener(filterChanged);
        updateData(true);

    }

    /**
     * the underlying model
     */
    public TableModel getModel() {

        return model;

    }

    // #################################################################

    /**
     * put in a index of the visible model, get the index in the base model
     *
     * @param visibleRow
     * @return
     */
    public int mapToBaseModel(int visibleRow) {

        return rowMapToBase == null ? visibleRow : rowMapToBase[visibleRow];
    }

    /**
     * put in a index of the base model, get the index in the visible model,
     * return -1 if not found
     *
     * @param baseRow
     * @return -1 if not found ~ filtered out
     */
    public int mapFromBaseModel(int baseRow) {

        if (rowMapToBase == null) return baseRow;
        int i = Arrays.binarySearch(rowMapToBase, baseRow);
        if (i < 0) return -1;// not found
        return i;
    }

    /**
     * <b>UNTESTED! </b> <br>
     * <br>
     * <br>
     * put in a index of the base model, get the index in the visible model, if
     * not found get the nearest index depenging from below below/above or -1 if
     * data is empty
     *
     * @param baseRow
     * @param below
     * @return
     */
    public int mapFromBaseModel(int baseRow, boolean below) {

        if (getRowCount() == 0) return -1;
        if (rowMapToBase == null) return baseRow;
        int i = Arrays.binarySearch(rowMapToBase, baseRow);
        if (i >= 0) return i;// found
        i = -(i + 1); // insertion point
        if (below)
            return i == 0 ? 0 : i - 1;
        else
            return i;

    }

    // #################################################################

    private void updateData(boolean fireChange) {

        rowMapToBase = FilterTools.createMap(model, filter, filterColumn);
        if (fireChange) fireTableDataChanged();

    }

    // #################################################################

    // easy delegates first
    public int getColumnCount() {

        return model.getColumnCount();
    }

    public Class getColumnClass(int col) {

        return model.getColumnClass(col);
    }

    public String getColumnName(int col) {

        return model.getColumnName(col);
    }

    // now the mapping delegates
    public int getRowCount() {

        if (rowMapToBase == null) return model.getRowCount();
        return rowMapToBase.length;

    }

    public Object getValueAt(int row, int col) {

        if (rowMapToBase != null) row = rowMapToBase[row];
        return model.getValueAt(row, col);

    }

    public void setValueAt(Object value, int row, int col) {

        if (rowMapToBase != null) row = rowMapToBase[row];
        model.setValueAt(value, row, col);

    }

    public boolean isCellEditable(int row, int col) {

        if (rowMapToBase != null) row = rowMapToBase[row];
        return model.isCellEditable(row, col);

    }

    private class ATableModelListener implements TableModelListener {

        // By default forward all events to all the listeners, change some
        public void tableChanged(TableModelEvent e) {

            updateData(false);// fires no event

            if (e.getFirstRow() == TableModelEvent.HEADER_ROW) {// structure

                fireTableChanged(e);
                return;

            }

            if (e.getFirstRow() > 0 || e.getLastRow() < Integer.MAX_VALUE) {// no
                // mapping

                fireTableDataChanged();
                // mapping untested:
                // fireTableChanged(new TableModelEvent(
                // (TableModel)e.getSource(),
                // mapFromBaseModel(e.getFirstRow(),true),
                // mapFromBaseModel(e.getLastRow(),false),
                // e.getColumn(),e.getType())
                // );
                return;

            }

            fireTableChanged(e);

        }

    }

}

