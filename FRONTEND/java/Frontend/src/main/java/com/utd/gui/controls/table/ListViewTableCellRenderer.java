package com.utd.gui.controls.table;

import com.utd.stb.back.data.client.AttributeList;
import com.utd.stb.inter.items.DisplayableItem;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import resources.IconSource;

import javax.swing.*;
import javax.swing.table.DefaultTableCellRenderer;
import java.awt.*;


/**
 * A renderer based on DefaultTableCellRenderer to look more like a listview:
 * changes Borders , full row select, focus shown always in full row.
 *
 * @author PeterBuettner.de
 */
public class ListViewTableCellRenderer
        extends DefaultTableCellRenderer {
    public static final Logger log = LogManager.getRootLogger();

    private IconSource is;

    public ListViewTableCellRenderer() {

        this.is = new IconSource();
    }

    public Component getTableCellRendererComponent(JTable table, Object value,
                                                   boolean isSelected, boolean hasFocus,
                                                   int row, int column) {

        super.getTableCellRendererComponent(table, value, isSelected, hasFocus, row, column);
        AttributeList attrList = null;
//        log.debug(value.getClass());
        if (value instanceof DisplayableItem) {
            DisplayableItem item = (DisplayableItem) value;
            attrList = (AttributeList) item.getAttributeList();
            // VALUE
            if (column == table.getColumnModel().getColumn(0).getModelIndex())
                this.setValue(item.getData());
            if (column == table.getColumnModel().getColumn(1).getModelIndex())
                this.setValue(item.getInfo());
        }
        if (attrList != null) {

            // COLOR
            Color color = attrList.getColor();
            if (!isSelected && !hasFocus) this.setBackground(color);

            // HIGHLIGHT
            Font font = attrList.getHighlighted();
            if (font != null) this.setFont(font);

            // ICON
            String icon = attrList.getIcon();
            if (column == 0 && icon != null) this.setIcon(is.getSmallIconByID(icon));
            if (column != 0) this.setIcon(null);


            // VALUE for Spalte 2 und mehr
            for (int i = 2; i < table.getColumnCount(); i++) {

                if (column == table.getColumnModel().getColumn(i).getModelIndex()) { // table.getColumnModel().getColumn(i).getModelIndex() ) {
                    this.setValue(attrList.getColValue(table.getModel().getColumnName(column)));
                }
            }
        }
        ListViewTools.changeDefaultTableCellRenderer(table, this, row, column);
        return this;
    }
}