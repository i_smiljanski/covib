package com.utd.stb.inter.application;

/**
 * Exceptions from the Backend are all descendants of this class. Severity may
 * have values (use the defined constants!):
 * <ul>
 * <li><b>0 </b> informational - just present the user, do nothing else (if
 * possible)
 * <li><b>1 </b> cancel - current action (wizzard, dialog,...) until main
 * window is visible
 * <li><b>not 0,1 </b> severe - close application
 * </ul>
 *
 * @author PeterBuettner.de
 */
public class BackendException extends Exception {

    public static final int SEVERITY_INFORMATIONAL = 0;
    public static final int SEVERITY_ACTION_EXIT = 1;
    public static final int SEVERITY_APP_EXIT = 2;
    public static final int SEVERITY_WARNING = 3;

    private final int severity;

    public BackendException(int severity) {

        super();
        this.severity = severity;
    }

    public BackendException(String message, int severity) {

        super(message);
        this.severity = severity;
    }

    public BackendException(String message, Throwable cause, int severity) {

        super(message, cause);
        this.severity = severity;
    }

    public BackendException(Throwable cause, int severity) {

        super(cause);
        this.severity = severity;
    }

    /**
     * just present the user, do nothing else (if possible)
     */
    public final boolean isInformational() {

        return severity == SEVERITY_INFORMATIONAL;
    }

    /**
     * cancel all until main application window is visible
     */
    public final boolean isActionExit() {

        return severity == SEVERITY_ACTION_EXIT;
    }

    /**
     * close application
     */
    public final boolean isAppExit() {
        return severity == SEVERITY_APP_EXIT;
    }

    public final boolean isWarning() {
        return severity == SEVERITY_WARNING;
    }

    public int getSeverity() {

        return severity;
    }
}