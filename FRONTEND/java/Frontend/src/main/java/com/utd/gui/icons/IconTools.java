package com.utd.gui.icons;

import javax.swing.*;
import java.awt.*;
import java.awt.image.BufferedImage;


/**
 * Tools for Icons, e.g. buttons may deliver a 'Disabled Icon', if not set
 * explicitly, but only for ImageIcon, so we construct a helper to make a
 * ImageIcon out of an icon.
 *
 * @author PeterBuettner.de
 */
public class IconTools {

    private static final Color COLOR_TRANSPARENT = new Color(0, 0, 0, 0);
    private static final JLabel DUMMY_LABEL = new JLabel();

    /**
     * Creates a ImageIcon out of an icon, unpainted background is transparent.
     * Usage e.g. for buttons that deliver a generated 'Disabled Icon' if not
     * set explicitly, but only for ImageIcon, so use this helper to make a
     * ImageIcon out of an icon. If icon is already an ImageIcon does nothing.
     *
     * @param icon
     * @return
     */
    static public ImageIcon toImageIcon(Icon icon) {

        if (icon instanceof ImageIcon) return (ImageIcon) icon;
        return new ImageIcon(toBufferedImage(icon));

    }

    /**
     * Creates a BufferedImage out of an icon, unpainted background is
     * transparent.
     *
     * @param icon
     * @return
     */
    static public BufferedImage toBufferedImage(Icon icon) {

        int w = icon.getIconWidth();
        int h = icon.getIconHeight();

        BufferedImage image = new BufferedImage(w, h, BufferedImage.TYPE_4BYTE_ABGR);
        Graphics2D g = (Graphics2D) image.getGraphics();
        try {

            g.setColor(COLOR_TRANSPARENT);
            g.fillRect(0, 0, w, h);
            icon.paintIcon(DUMMY_LABEL, g, 0, 0);

        } finally {

            g.dispose();

        }

        return image;

    }

}