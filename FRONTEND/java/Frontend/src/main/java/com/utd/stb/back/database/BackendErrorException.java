package com.utd.stb.back.database;

import com.utd.stb.back.data.client.ErrorRecord;
import com.utd.stb.inter.application.BackendException;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;


/**
 * Extension of BackendException. Used in middle tier to get information about
 * errors.
 *
 * @author rainer bruns Created 13.01.2005
 * @see BackendException
 */
public class BackendErrorException extends BackendException {

    public static final Logger log = LogManager.getRootLogger();
    private String errorCode;
    private int severity;
    private int internalSeverity;
    private ErrorRecord errorRecord;

    /**
     * @param err the original ErrorRecord
     */
    public BackendErrorException(ErrorRecord err) {

        super(err.getErrorMsg(), err.getSeverityForGui());
        this.errorRecord = err;
        this.errorCode = err.getErrorCode();
        this.severity = err.getSeverityForGui();
        this.internalSeverity = err.getSeverity();
        log.debug(this);
    }

    /**
     * @return the ErrorCode containing the severity
     */
    public String getErrorCode() {

        return errorCode;
    }

    /**
     * @return the Severity
     */
    public int getInternalSeverity() {

        return internalSeverity;
    }

    @Override
    public int getSeverity() {
        return severity;
    }

    public ErrorRecord getErrorRecord() {
        return errorRecord;
    }

    @Override
    public String toString() {
        return "BackendErrorException{" +
                "errorCode='" + errorCode + '\'' +
                ", severity=" + severity +
                ", internalSeverity=" + internalSeverity +
                ", errorRecord=" + errorRecord +
                '}';
    }
}