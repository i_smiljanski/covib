package com.utd.gui.controls.table;

import javax.swing.*;
import javax.swing.border.Border;
import java.awt.*;


/**
 * A Table in a ScrollPane with Scrollbar on the right will have a 'empty'
 * top-right corner, use this to fill it
 *
 * @author PeterBuettner.de
 */
public class CornerComponent extends JComponent {

    public void paint(Graphics g) {

        super.paint(g);
        Border border = UIManager.getBorder("TableHeader.cellBorder");
        if (border != null) {

            Insets i = border.getBorderInsets(this);
            border.paintBorder(this, g, -i.left - 5, 0, getWidth() + i.right + i.left + 10,
                    getHeight());

        }
    }
}