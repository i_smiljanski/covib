package com.utd.stb.inter.action;

import com.utd.stb.inter.application.Node;


/**
 * Action describes all instructions the base may send to the gui which change
 * the tree or detail view, also the gui sends double-Click actions on nodes to
 * the base.
 * <p>
 * TODOlater change the constants from String to Object (we may use enum later)
 * TODOlater maybe add 'NodeAction getNext()' to have a clear queue here
 *
 * @author PeterBuettner.de
 */
public interface NodeAction {

    /**
     * Forget all children and descendants of this node, the detailview is
     * unchanged, so if the displayed childs change, also reload the details
     * <p>
     * <br>
     * if the current selected node is
     * <ul>
     * <li>an ancestor of the node -&gt; selection is unchanged
     * <li>the node itself -&gt; selection is unchanged
     * <li>a descendant of the node -&gt; the selection is removed
     * </ul>
     * <p>
     * <p>
     * TODO later save expanded state? --> use other type
     */
    public static final String RELOAD = "ReloadNode";

    /**
     * expand this node
     */
    public static final String EXPAND = "ExpandNode";

    /**
     * set the selection of the tree to this node, the <b>details are not
     * updated </b> but cleared, since the Base may like to show alternate
     * details and therefor we should avoid unnecessary loading of the default
     * details, for the standard case see {@link SELECT_AND_RELOAD_DETAIL}
     */
    public static final String SELECT = "SelectNode";

    /**
     * set the selection of the tree to this node and afterwards forces detail
     * view to reload (default childs from base).
     */
    public static final String SELECT_AND_RELOAD_DETAIL = "SelectAndReloadDetail";

    /**
     * sets the selection in the detail view to the node, the base has to assure
     * that the node is in the current detail view
     */
    public static final String SELECT_IN_DETAIL = "SelectInDetail";

    /**
     * Sent from the Gui to the base on double click in the detail view (may
     * also be triggert on 'ENTER' key)
     */
    public static final String DBLCLICK_IN_DETAIL = "DblClickInDetail";
    
    public static final String UPDATE_TREE = "UpdateTree";

    /**
     * the node this action cares about
     *
     * @return
     */
    Node getTargetNode();

    /**
     * what to do, see the constants of this class:
     * <p>
     * <pre>
     *
     *  NodeAction.RELOAD
     *  NodeAction.EXPAND
     *  NodeAction.SELECT
     *
     * </pre>
     *
     * @return
     */
    String getType();

}