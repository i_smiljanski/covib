package com.utd.stb.inter.application;

import java.util.Locale;


/**
 * Some static info that the gui may show, or use. Use a structure to make clear
 * that this info is available. Must not throw any exceptions.
 *
 * @author PeterBuettner.de
 */
public interface ApplicationInfo {

    /**
     * Shown e.g. in the statusbar
     *
     * @return
     */

    String getUserName();

    /**
     * Locale to use for the language in the gui.
     *
     * @return
     */
    Locale getUserLanguage();

    /**
     * Shown e.g. in the statusbar
     *
     * @return
     */
    String getServerName();

    /**
     * Shown e.g. in the statusbar
     *
     * @return
     */
    String getServerAliasName();

    String isNewMainXml();

    /**
     * Used 'as is' in Titlebars
     *
     * @return
     */
    String getApplicationTitle();

    /**
     * After this time of user inactivity thee application exits - without any
     * warning, the base gets informed
     *
     * @return
     */
    int getTimeoutInMinutes();

    int getDialogTimeoutInSec();

}