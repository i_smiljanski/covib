package com.utd.stb.back.database;

import com.utd.stb.back.data.client.ErrorRecord;

/**
 * Exception extension used if Backend Error is thrown for workflow information
 * only
 *
 * @author rainer bruns Created 13.01.2005
 */
public class BackendWorkflowException extends BackendErrorException {

    private String errorCode;
    private String errorMessage;
    private int severity;

    /**
     * Standard Constructor
     */
    public BackendWorkflowException(ErrorRecord err) {

        super(err);
        this.errorCode = err.getErrorCode();
        this.errorMessage = err.getErrorMsg();
        this.severity = err.getSeverityForGui();
    }

    /**
     * @return the ErrorCode
     */
    public String getErrorCode() {

        return errorCode;
    }

    /**
     * @return the ErrorMessage
     */
    public String getErrorMessage() {

        return errorMessage;
    }
}