package com.utd.gui.controls.table;

import com.utd.gui.animate.Mover;
import com.utd.gui.animate.SwingAnimator;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import javax.swing.*;
import javax.swing.table.JTableHeader;
import javax.swing.table.TableCellRenderer;
import javax.swing.table.TableColumn;
import javax.swing.table.TableColumnModel;
import java.awt.*;
import java.awt.event.MouseEvent;
import java.util.Iterator;


/**
 * Tools to enhance JTables: autosize columns, smooth Column resizing ...
 *
 * @author PeterBuettner.de
 */
public class TableTools {
    public static final Logger log = LogManager.getRootLogger();

    /**
     * Autosizes all columns.
     *
     * @param table
     */
    public static void autoSizeColumns(JTable table, boolean animate) {

        for (int i = 0; i < table.getColumnCount(); i++)
            autoSizeColumn(table, i, animate);
    }

    /**
     * Autosizes one column to the maximum value of all
     * cellsRenders-preferred-Width
     *
     * @param table
     * @param columnIndex
     * @param animate
     */
    public static void autoSizeColumn(JTable table, int columnIndex, boolean animate) {

//        log.debug("begin "+columnIndex);
        if (table.getColumnCount() <= columnIndex) return;
        int newSize = 0;
        //table.getTableHeader(); WindowsTableHeaderUI
        if (table.getTableHeader() != null) {
            Component head = getHeaderRenderer(table.getTableHeader(), columnIndex);
            if (head != null) {
                Dimension d = head.getPreferredSize();
                if (d != null) newSize = d.width;
            }
        }

        int nRow = table.getRowCount();
        for (int row = 0; row < nRow; row++) {
            TableCellRenderer renderer = table.getCellRenderer(row, columnIndex);
            if (renderer == null) continue;
            Component component = table.prepareRenderer(renderer, row, columnIndex);
            if (component == null) continue;
            Dimension d = component.getPreferredSize();
            if (d == null) continue;
            if (d.width > newSize) newSize = d.width;
        }
        if (table.getShowVerticalLines()) newSize++;// they eat up one pixel

        TableColumn column = table.getColumnModel().getColumn(columnIndex);

        if (animate)
            smoothSizeColumn(column, newSize, column);
        else
            column.setPreferredWidth(newSize);
//        log.debug("end");
    }// ---------------------------------------------------------------------------

    /**
     * The Component to render the header; Copied from BasicTableHeaderUI, maybe
     * it doesn't works in some L&F
     *
     * @param header
     * @param col
     * @return
     */
    private static Component getHeaderRenderer(JTableHeader header, int col) {

        TableColumn tc = header.getColumnModel().getColumn(col);
        TableCellRenderer renderer = tc.getHeaderRenderer();
        if (renderer == null) renderer = header.getDefaultRenderer();
        return renderer.getTableCellRendererComponent(header.getTable(), tc.getHeaderValue(),
                false, false, -1, col);
    }// ---------------------------------------------------------------------------

    /**
     * Sizes the TableColumn to newWidth with an animation.
     *
     * @param lock     see
     *                 {@link SwingAnimator#run(final Object singleAnimation, int intervalMillies, final Iterator values)}
     * @param newWidth
     * @param col
     */
    public static void smoothSizeColumn(Object lock, final int newWidth, final TableColumn col) {

        int start = col.getWidth();
        if (start == newWidth) return;

        SwingAnimator ani = new SwingAnimator() {

            protected void processValue(Number value, boolean isLastValue) {

                if (isLastValue)
                    col.setPreferredWidth(newWidth);
                else
                    col.setPreferredWidth(value.intValue());
            }
        };
        // Iterator values = new Mover(start, end).iteratorInteger();
        Iterator values = new Mover(start, newWidth).iteratorLinearInteger();
        ani.run(lock, 20, values);

    }

    /**
     * get the column to resize if clicked <b>between </b> two columns, so
     * returning the left one (or the right one, depending on
     * component-orientation), returns -1 if not clicked near the
     * column-separators
     *
     * @param header
     * @param evt    needed to find out the clicked position
     * @return
     */
    public static int resizeColumnFromHeaderClick(JTableHeader header, MouseEvent evt) {

        // inspired from
        // javax.swing.plaf.basic.BasicTableHeaderUI.MouseInputHandler
        Point p = evt.getPoint();
        int column = header.getColumnModel().getColumnIndexAtX(p.x);

        Rectangle r = header.getHeaderRect(column);
        r.grow(-3, 0);
        if (r.contains(p)) return -1;

        if (header.getComponentOrientation().isLeftToRight()) {
            if (p.x < r.getCenterX()) column--;
        } else if (p.x > r.getCenterX()) column--;

        return column;

    }// ---------------------------------------------------------------------------

    /**
     * syncs the column sizes of two tables: columns should be the 'same', no
     * check is made (e.g. same order) but is is assured that only existing
     * columns ('index' &lt; getColumnCount()) are handled.
     * <p>
     * <p>
     * Syncing means: <br>
     * the columns are compared, set to the max of the 2, width =
     * max(tab1.col(i), tab2.col(i))
     *
     * @param tab1
     * @param tab2
     */
    public static void syncColumnSizes(JTable tab1, JTable tab2) {

        TableColumnModel tm1 = tab1.getColumnModel();
        TableColumnModel tm2 = tab2.getColumnModel();

        int n = Math.min(tm1.getColumnCount(), tm2.getColumnCount());
        for (int i = 0; i < n; i++) {

            int w1 = tm1.getColumn(i).getPreferredWidth();
            int w2 = tm2.getColumn(i).getPreferredWidth();
            int w = Math.max(w1, w2);
            if (w1 < w) tm1.getColumn(i).setPreferredWidth(w);
            if (w2 < w) tm2.getColumn(i).setPreferredWidth(w);

        }

    }

    /**
     * copies the column sizes from src to dest: columns should be the 'same',
     * no check is made, but is is assured that only existing columns (index <
     * getColumnCount()) are handled
     *
     * @param src
     * @param dest
     */
    public static void copyColumnSizes(TableColumnModel src, TableColumnModel dest) {

        int nSrc = src.getColumnCount();
        int nDest = dest.getColumnCount();
        for (int i = 0; i < nSrc && i < nDest; i++)
            dest.getColumn(i).setPreferredWidth(src.getColumn(i).getPreferredWidth());

    }

    /**
     * if the table is not empty selects the first row, if 'later' it is called
     * by {@link SwingUtilities.invokeLater}so put at the tail of the
     * EventQueue -&gt; postEvent
     *
     * @param table
     * @param later
     */
    public static void selectFirstRow(final JTable table, boolean later) {

        if (later)
            SwingUtilities.invokeLater(new Runnable() {

                public void run() {

                    if (table.getRowCount() > 0) table.setRowSelectionInterval(0, 0);
                }
            });
        else if (table.getRowCount() > 0) table.setRowSelectionInterval(0, 0);
    }// ---------------------------------------------------------------------------

    /**
     * sets/adds the selection of the table to the rows in sel, if an index in
     * sel is -1 this is ignored (may be seen from a mapping in filtered
     * tablemodels or listtablemodel)
     *
     * @param table
     * @param sel   if null doesn't selects anything
     * @param clear clear old selection before
     */
    public static void setSelectionByIndices(JTable table, int[] sel, boolean clear) {

        if (clear) table.clearSelection();
        if (sel == null) return;

        for (int aSel : sel)
            if (aSel != -1) {
                table.addRowSelectionInterval(aSel, aSel);
            }
    }

    public static void setSelectionByList(JTable table, java.util.List sel, boolean clear) {
        if (clear) table.clearSelection();
        for (int aRow = 0; aRow < table.getRowCount(); aRow++) {
            for (Object srcRow : sel) {
                Object value = table.getModel().getValueAt(aRow, 0);
                if (value != null && value.equals(srcRow)) {
                    //log.debug(aRow + " : " + table.getModel().getValueAt(aRow, 0));
                    table.addRowSelectionInterval(aRow, aRow);
                }
            }
        }
    }

    /**
     * scrolls the first selected item (if any) to be visible (this works only
     * if the table is in a JScrollPane), if no selection does nothing
     *
     * @param table
     */
    public static void scrollFirstSelectedToVisible(JTable table) {

        scrollRowToVisible(table, table.getSelectedRow());

    }

    /**
     * scrolls the first row to be visible (this works only if the table is in a
     * JScrollPane)
     *
     * @param table
     * @param row   if -1 then does nothing
     */
    public static void scrollRowToVisible(JTable table, int row) {

        if (row == -1) return;
        Rectangle r = table.getCellRect(row, 0, true);
        if (r == null) return;
        r.x = 0;
        r.width = table.getWidth();
        table.scrollRectToVisible(r);

    }
}