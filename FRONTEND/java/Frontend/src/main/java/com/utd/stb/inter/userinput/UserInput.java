package com.utd.stb.inter.userinput;

/**
 * Basic attributs of a gui control that may be able to edit some data
 *
 * @author PeterBuettner.de
 */
public interface UserInput {

    void setData(Object data);

    Object getData();

    /**
     * Type of data: String, Integer, Date, DisplayableItem, ... <br>
     *
     * @return
     */
    Class getType();

    /**
     * Label for gui, use a '&amp;' to define the Mnemonic (underlined char),
     * '&amp;&amp;' to have a real '&amp;' on the screen. <br>
     * optional <br>
     *
     * @return
     */
    String getLabel();

    /**
     * if 'null' values are allowed
     *
     * @return
     */
    boolean isEmptyAllowed();

    void setEmptyAllowed(boolean emptyAllowed);


    /**
     * display ony or editable
     *
     * @return
     */
    boolean isReadOnly();

    /**
     * for LOVs: value can be various, not necessarily from LOV
     *
     * @return
     */
    boolean isChangeable();

    /**
     * Displayed or not in the Gui
     *
     * @return
     */
    boolean isVisible();

    void setVisible(boolean visible);

    /**
     * Has focus or not in the Gui
     *
     * @return
     */
    boolean hasFocus();

    /**
     * Set true if the component has focus or not in the Gui
     *
     * @return
     */
    void setFocus(boolean focus);


    /**
     * Gets one of this object's properties using the associated key, for
     * extensions.
     */
    public Object getExtra(String key);

}