/*
 * Created on 21.12.2004
 *
 */
package com.utd.stb.back.custom.impl;

import com.utd.stb.back.custom.basic.CustomAction;
import com.utd.stb.back.data.client.TreeNodeRecord;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;


/**
 * @author rainer Created 21.12.2004
 */
public class CustomActionDemo implements CustomAction {

    Connection conn;

    public CustomActionDemo(Connection conn) {

        this.conn = conn;
    }

    public CustomAction execute(TreeNodeRecord node) {

        try {
            PreparedStatement ps = conn.prepareStatement("select user from dual");
            ResultSet rs = ps.executeQuery();
            rs.next();
            rs.close();
            ps.close();
        } catch (SQLException se) {
            se.printStackTrace();
        }
        return null;
    }
}