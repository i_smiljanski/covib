package com.utd.stb.back.data.client;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;


/**
 * Wraps a list of record data of type PropertyRecord
 *
 * @author rainer bruns Created 27.08.2004
 * @see PropertyRecord
 */
public class PropertyList {

    private ArrayList data = new ArrayList();
    private Iterator iterator;

    /**
     * creates an empty EntityList
     */
    public PropertyList() {
    }

    /**
     * creates a PropertyList with data from List in
     *
     * @param in a list
     */
    public PropertyList(ArrayList in) {

        Iterator it = in.iterator();
        while (it.hasNext()) {
            StructMap map = (StructMap) it.next();
            PropertyRecord prop = new PropertyRecord(map);
            data.add(prop);
        }
    }

    /**
     * adds all fields of input list to this list
     *
     * @param list a List to add
     */
    public PropertyList(List list) {

        data.addAll(list);
    }

    /**
     * adds all fields of input list to this list
     *
     * @param in an PropertyList to add
     */
    public void addAll(PropertyList in) {

        in.initIterator();
        while (in.hasNext()) {
            data.add(in.getNext());
        }
    }

    /**
     * adds the record to this list
     *
     * @param sr Record to add
     */
    public void add(PropertyRecord sr) {

        data.add(sr);
    }

    /**
     * @param index
     * @return the Record at index
     */
    public PropertyRecord get(int index) {

        return (PropertyRecord) data.get(index);
    }

    /**
     * sets record at given index to input record
     *
     * @param index
     * @param sr    record to set
     */
    public void set(int index, PropertyRecord sr) {

        data.set(index, sr);
    }

    /**
     * sets list iterator to the beginning
     */
    public void initIterator() {

        iterator = data.iterator();
    }

    /**
     * @return true if there is a next record
     */
    public boolean hasNext() {

        return iterator.hasNext();
    }

    /**
     * @return the next EntityRecord
     */
    public PropertyRecord getNext() {

        return (PropertyRecord) iterator.next();
    }

    /**
     * @return the size of this list
     */
    public int getSize() {

        return data.size();
    }

    /**
     * @param s Record to look up
     * @return the index of this record
     */
    public int indexOf(PropertyRecord s) {

        return data.indexOf(s);
    }

    /**
     * @return the first PropertyRecord in list
     */
    public PropertyRecord getFirst() {

        if (data.size() > 0) {
            return (PropertyRecord) data.get(0);
        } else {
            return null;
        }
    }

    @Override
    public String toString() {
        /*return "PropertyList{" +
                "data=" + data +
                '}';*/
        String propertyListString = "";
        for (int i = 0; i < data.size(); i++) {
            PropertyRecord rec = (PropertyRecord) data.get(i);
            propertyListString += "property " + i + " " + rec.toString() + "\n";
        }
        return propertyListString;
    }
}