package com.utd.stb.gui.swingApp.login;

import com.uptodata.isr.utils.xml.dom.XmlHandler;
import org.apache.commons.lang.BooleanUtils;
import org.apache.commons.lang.StringUtils;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.w3c.dom.NamedNodeMap;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;

import javax.xml.xpath.XPathExpressionException;
import java.io.FileInputStream;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;

/**
 * container for alias and full name, also a helper method to read this from a
 * property file, later we may read from other sources (LDAP, url, ...)
 */
public class DatabaseInfo {

    public static final Logger log = LogManager.getRootLogger();

    private String alias;
    private String fullName;
    private String dbUser;
    private String dbPassword;
    private boolean isDefault;

    public DatabaseInfo(String alias, String fullName, String dbUser, String dbPassword) {
        this.alias = alias;
        this.fullName = fullName;
        this.dbUser = dbUser;
        this.dbPassword = dbPassword;
    }

    public String getAlias() {
        return alias;
    }

    public String getFullName() {
        return fullName;
    }

    public String getdbUser() {
        return dbUser;
    }

    public String getdbPassword() {
        return dbPassword;
    }

    public boolean isDefault() {
        return isDefault;
    }

    public void setDefault(boolean aDefault) {
        isDefault = aDefault;
    }


    public String toString() {
        return "Alias:" + alias + "; FullName:" + fullName + "; DBUser:" + dbUser + "; default: " + isDefault;
    }

    /**
     * reads all Databases from properties.
     * <p>
     * Format example:
     * <p>
     * <
     * pre>
     * <p>
     * <p>
     * # first char is item separator # every item is 'trimmed' # order in
     * choosebox is same as here
     * <p>
     * stabber.databases.items=|alias1|alias2|alias3
     * <p>
     * <p>
     * # use a token from stabber.databases.items, append it # to
     * &quot;stabber.databases.item.&quot; to create a key:
     * <p>
     * stabber.databases.item.alias1=server.de:123:456@test/test
     * stabber.databases.item.alias2=myServer.de:123:456@test/test
     * stabber.databases.item.alias3=yourServer.de:123:456@test/test
     * <p>
     * <p>
     * </pre>
     *
     * @param props
     * @param prefix e.g. "stabber.databases" the prefix for the names in the
     * properties
     * @return never null, but maybe an empty array
     */
    static boolean firstCall = true;

    public static DatabaseInfo[] readDataBases(XmlHandler xmlDbFile) {
        log.debug("begin readDataBases");
        DatabaseInfo[] dbInfos;
        List<DatabaseInfo> list = new ArrayList<DatabaseInfo>();
        try {

            NodeList items = xmlDbFile.selectNodes("//ITEM");
            if (items == null || items.getLength() < 1) {
                return new DatabaseInfo[0];
            }
            for (int i = 0; i < items.getLength(); i++) {
                Node item = items.item(i);
                Node file = XmlHandler.getChildNode(item, "FILE");
                if (file != null) {
                    if (firstCall) {
                        firstCall = false;
                        try {
                            String fileLoc = file.getTextContent();
                            log.debug("Redirected to location " + fileLoc);
                            dbInfos = readDataBases(new XmlHandler(new FileInputStream(fileLoc)));
                            return dbInfos;
                        } catch (Exception e) {
                            e.printStackTrace();
                        }
                    }
                } else {
                    DatabaseInfo dataBaseInfo = nodeToDataBaseInfo(item, xmlDbFile);
                    list.add(dataBaseInfo);
                }
            }

            list.sort((o1, o2) -> {
                if (o1.isDefault && o2.isDefault) {
                    return 0;
                }

                if (!o1.isDefault && !o2.isDefault) {
                    return 0;
                }

                if (o1.isDefault && !o2.isDefault) {
                    return -1;
                }

                return 1;
            });

        } catch (XPathExpressionException e) {
            log.error(e);
        }
        list.add(new DatabaseInfo("add", null, null, null));
        dbInfos = list.toArray(new DatabaseInfo[0]);
        log.debug(dbInfos[0]);
        return dbInfos;
    }

    private static DatabaseInfo nodeToDataBaseInfo(Node item, XmlHandler xmlDbFile) {
        String aliasText = xmlDbFile.getTextContentByTagName(item, UserLogin.MAIN_NODE_ALIAS);
        String databaseName = xmlDbFile.getTextContentByTagName(item, UserLogin.MAIN_NODE_FULLNAME);
        String userName = xmlDbFile.getTextContentByTagName(item, UserLogin.MAIN_NODE_DBUSER);
        String password = xmlDbFile.getTextContentByTagName(item, UserLogin.MAIN_NODE_DBPASSWORD);

        DatabaseInfo databaseInfo = new DatabaseInfo(aliasText, databaseName, userName, password);
        NamedNodeMap attributes = item.getAttributes();
        Node defaultAttribut = attributes.getNamedItem("default");
        if (defaultAttribut != null) {
            String value = defaultAttribut.getNodeValue();
            if (BooleanUtils.toBooleanObject(value)) {
                databaseInfo.setDefault(true);
            }
        }
        return databaseInfo;
    }

}
