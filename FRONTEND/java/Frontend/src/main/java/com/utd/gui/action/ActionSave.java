package com.utd.gui.action;

import com.utd.gui.resources.I18N;

import javax.swing.*;


public abstract class ActionSave extends AbstractAction {

    public ActionSave() {
        ActionUtil.initActionFromMap(this, I18N.getActionMap(ActionSave.class.getName()),
                null);
        putValue(SHORT_DESCRIPTION, null);
    }

}