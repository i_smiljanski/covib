package com.utd.stb.gui.userinput.swing;

import java.util.EventListener;


/**
 * Used by UIODataHandler for informing someone about changes in the associated
 * JComponents
 *
 * @author PeterBuettner.de
 */
public interface DataChangeListener extends EventListener {

    void dataChanged(DataChangeEvent event);
}