package com.utd.stb.gui.wizzard.choosers;

import com.jgoodies.forms.builder.DefaultFormBuilder;
import com.jgoodies.forms.layout.FormLayout;
import com.utd.gui.action.ActionWrap;
import com.utd.gui.controls.BorderSplitPane;
import com.utd.gui.controls.table.TableTools;
import com.utd.gui.event.DocumentDeferredChangeAdapter;
import com.utd.gui.filter.FilterErrorMsgDisplay;
import com.utd.gui.filter.FilterPanelBuilder;
import com.utd.gui.filter.FilterTools;
import com.utd.gui.icons.IconFactory;
import com.utd.gui.util.DialogUtils;
import com.utd.stb.gui.swingApp.exceptions.ExceptionHandler;
import resources.Res;

import javax.swing.*;
import javax.swing.border.EmptyBorder;
import javax.swing.event.DocumentEvent;
import javax.swing.event.ListSelectionEvent;
import javax.swing.event.ListSelectionListener;
import java.awt.event.ActionEvent;


/**
 * Basic class for TwoTable and TwoTree Chooser, has a left/right component,
 * probably with a filter row below each (createXXXBottomRow) a left/right move
 * bar between and a up/down bar on the right side of the right conponent
 *
 * @author PeterBuettner.de
 */
abstract class TwoComponentChooser extends AbstractChooser {

    /**
     * some extra space before the first button of the two toolbars
     */
    private static final boolean BARS_ON_TOP = !false;

    protected Action acMoveDown;
    protected Action acMoveUp;
    protected Action acToLeftOne;
    protected Action acToLeftAll;
    protected Action acToRightAll;
    protected Action acToRightOne;

    protected JComponent cmpLeft;
    protected JComponent cmpRight;

    //static JTextComponent filterField;

    /**
     * @param exceptionHandler
     */
    public TwoComponentChooser(ExceptionHandler exceptionHandler) {

        super(exceptionHandler);
    }

    protected abstract void onMoveUp();

    protected abstract void onMoveDown();

    protected abstract void onChooseSelected();

    protected abstract void onUnchooseSelected();

    protected abstract void onChooseAll();

    protected abstract void onUnchooseAll();

    protected JComponent createUpDownBar(boolean vertical) {

        JToolBar bar = new JToolBar(vertical
                ? SwingConstants.VERTICAL
                : SwingConstants.HORIZONTAL);
        bar.setRollover(true);
        bar.setFloatable(false);

        if (vertical) {
            if (!BARS_ON_TOP)
                bar.add(Box.createGlue());
            else
                bar.add(Box.createVerticalStrut(30));
        } else
            bar.add(Box.createGlue());

        bar.add(DialogUtils.createToolbarButtonRepeating(acMoveUp));
        bar.add(DialogUtils.createToolbarButtonRepeating(acMoveDown));
        bar.add(Box.createGlue());
        if (vertical) bar.setBorder(new EmptyBorder(6, 0, 0, 0));// a bit
        // space
        // on top
        return bar;
    }

    /**
     * Bar with buttons '>' '>>' ' <' ' < <'
     *
     * @return
     */
    protected JComponent createChooseToolBar() {

        JToolBar bar = new JToolBar(SwingConstants.VERTICAL);
        bar.setFloatable(false);
        bar.setRollover(true);

        if (!BARS_ON_TOP)
            bar.add(Box.createVerticalGlue());
        else
            bar.add(Box.createVerticalStrut(30));

        bar.add(DialogUtils.createToolbarButton(acToRightOne));
        bar.add(DialogUtils.createToolbarButton(acToLeftOne));
        bar.add(Box.createVerticalStrut(10));
        bar.add(DialogUtils.createToolbarButton(acToRightAll));
        bar.add(DialogUtils.createToolbarButton(acToLeftAll));
        bar.add(Box.createVerticalGlue());
        return bar;
    }

    /**
     * Builds the Panel, collects toolbars etc. Preconditions: actions,
     * left/right-components (e.g. listViewTables) build
     *
     * @param labelCmpLeft
     * @param labelCmpRight
     * @return
     */
    protected JComponent getComponentPanel(String labelCmpLeft, String labelCmpRight) {

        // setup FormLayout Builder ..........................
        String list = "min:G";
        String space = "1dlu";
        FormLayout layout = new FormLayout(list + ", " + space + ", P, " + space + ", "
                + list + ", " + space + ", C:P",
                //                                      List | Move | List | UpDown
                "P, 2dlu, F:min:G"); // rows: label
        // div
        // controls
        layout.setColumnGroups(new int[][]{{1, 5}});// lists have same
        // width
        DefaultFormBuilder builder = DialogUtils.getFormBuilder(layout);

        // Label Row ......................................
        builder.append(buildComponentLabel(labelCmpLeft, cmpLeft, 0));
        builder.nextColumn(2);
        builder.append(buildComponentLabel(labelCmpRight, cmpRight, 1));
        builder.nextColumn(2);
        builder.nextLine(2);

        // component Row ......................................
        builder.append(createCompound(cmpLeft, createLeftBottomRow()));
        builder.append(createChooseToolBar());// un/choose Toolbar
        builder.append(createCompound(cmpRight, createRightBottomRow()));
        builder.append(createUpDownBar(true));
        // .........................................................

        return builder.getPanel();
    }

    /**
     * Creates a compound component out of two, the top one is growing
     * (~table,tree,list,...) the bottom one gets its preferred height, they are
     * separated by a small gap (FormLayout RelatedComponentsGapRow), the
     * growingTop is always wrapped in a JScrollPane
     *
     * @param growingTop
     * @param bottom     if null only the growingTop wrapped in a JScrollPane is
     *                   returned
     * @return
     */
    private JComponent createCompound(JComponent growingTop, JComponent bottom) {

        BorderSplitPane sp = new BorderSplitPane(new JScrollPane(growingTop), null, false);

        if (bottom == null) return sp;

        FormLayout layout = new FormLayout("min:G", "F:min:G");
        DefaultFormBuilder builder = DialogUtils.getFormBuilder(layout);
        builder.appendRelatedComponentsGapRow(); // spacer
        builder.appendRow("P"); // filter

        builder.append(sp);
        builder.nextLine(2);
        builder.append(bottom);

        return builder.getPanel();
    }

    /**
     * overwrite! <br>
     * e.g. a filter row below the table, return null to add nothing
     *
     * @return
     */
    protected JComponent createLeftBottomRow() {

        return null;
    }

    /**
     * overwrite! <br>
     * e.g. a filter row below the table, return null to add nothing
     *
     * @return
     */
    protected JComponent createRightBottomRow() {

        return null;
    }

    /**
     * Creates the default actions: toLeft, toLeftAll, ...
     */
    protected void createActions() {

        Object[] icons = IconFactory.createMoverIcons(); // order is
        // right,left,rightAll,leftAll,up,down

        acToRightOne = new ResAction("acToRightOne", icons[0]) {

            public void actionPerformed(ActionEvent e) {

                onChooseSelected();
            }
        };

        acToRightAll = new ResAction("acToRightAll", icons[2]) {

            public void actionPerformed(ActionEvent e) {

                onChooseAll();
            }
        };

        acToLeftOne = new ResAction("acToLeftOne", icons[1]) {

            public void actionPerformed(ActionEvent e) {

                onUnchooseSelected();
            }
        };

        acToLeftAll = new ResAction("acToLeftAll", icons[3]) {

            public void actionPerformed(ActionEvent e) {

                onUnchooseAll();
            }
        };

        acMoveUp = new ResAction("acMoveUp", icons[4]) {

            public void actionPerformed(ActionEvent e) {

                onMoveUp();
            }
        };

        acMoveDown = new ResAction("acMoveDown", icons[5]) {

            public void actionPerformed(ActionEvent e) {

                onMoveDown();
            }
        };

    }

    private static abstract class ResAction extends ActionWrap {

        public ResAction(String name, Object icon) {

            super(Res.getActionMap("reportwizzard.TwoComponentChooser.action." + name),
                    icon);
        }
    }

    protected static JComponent createFilterBar(final JTable table,
                                                final JTextField filterField
                                                 /*, String initText*/) {

        final FilterErrorMsgDisplay fd = new FilterErrorMsgDisplay();

        //filterField = filter;
        // filterField = new JTextField( initText );

        final String lblText = Res
                .getString("reportwizzard.TwoComponentChooser.filterNumberFound.label.name");
        final String lblTooltip = Res
                .getString("reportwizzard.TwoComponentChooser.filterNumberFound.label.tooltip");

        // remove found# on manual change
        final ListSelectionListener lsl = new ListSelectionListener() {

            public void valueChanged(ListSelectionEvent e) {

                if (!e.getValueIsAdjusting()) fd.setDefaultMessage(null, null);
            }
        };
        table.getSelectionModel().addListSelectionListener(lsl);
        filterField.getDocument().addDocumentListener(new DocumentDeferredChangeAdapter() {

            public void deferredChange(DocumentEvent e) {

                table.getSelectionModel().removeListSelectionListener(lsl);
                try {
                    FilterTools.updateSelectionByRegex(table, fd, filterField.getText());
                } finally {
                    table.getSelectionModel().addListSelectionListener(lsl);
                }
                TableTools.scrollFirstSelectedToVisible(table);

                fd.setDefaultMessage(lblText + table.getSelectedRowCount(), lblTooltip);
            }
        });
        return FilterPanelBuilder.getInstance().buildSelectionPane(fd, filterField);
    }

    /* (non-Javadoc)
     * @see com.utd.stb.gui.wizzard.choosers.AbstractChooser#getFilterFieldValue()
     */
    public String getFilterFieldValue() {
        return null;
    }

    /* (non-Javadoc)
     * @see com.utd.stb.gui.wizzard.choosers.AbstractChooser#setFilterFieldValue()
     */
    public void setFilterFieldValue(String regExpr) {
    }
} 