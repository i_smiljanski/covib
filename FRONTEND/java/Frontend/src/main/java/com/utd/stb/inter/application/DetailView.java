package com.utd.stb.inter.application;

import com.utd.stb.inter.userinput.UserInputs;


/**
 * The gui has to show other than the standart content in the detailpane
 *
 * @author PeterBuettner.de
 */
public interface DetailView {

    /**
     * a String to tell the user that the view is different, shown e.g. in a
     * label near the table
     *
     * @return
     */
    String getTitle();

    /**
     * Those nodes should be displayed
     *
     * @return
     */
    Nodes getNodes();

    /**
     * Since the nodes may have different properties than in the standard-case,
     * get them here
     *
     * @param node
     * @return
     */
    UserInputs getNodeProperties(Node node);

}