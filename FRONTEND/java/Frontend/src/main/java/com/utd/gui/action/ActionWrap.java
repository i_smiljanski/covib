package com.utd.gui.action;

import javax.swing.*;
import java.util.Map;


/**
 * Descendent of Abstract Action to ease the creation of nice buttons/menus/...
 *
 * @author PeterBuettner.de
 */
public abstract class ActionWrap extends AbstractAction {

    /**
     * if a parameter is null then it is unused/empty
     *
     * @param text
     * @param icon     use '/myPath/in/jar/orfilesystem/name.png', or you may put an
     *                 Icon in
     * @param mnemonic if == ' ' (Space) then unused
     * @param tooltip
     * @param accel    format see {@link javax.swing.KeyStroke.getKeyStroke(String)}
     */
    public ActionWrap(String text, Object icon, char mnemonic, String tooltip, String accel) {

        super(text);
        ActionUtil.initAction(this, icon, mnemonic, tooltip, accel);
    }

    /**
     * see {@link com.utd.gui.action.ActionUtil.initActionFromMap()}
     *
     * @param resName name in the St
     * @param icon
     */
    public ActionWrap(Map map, Object icon) {

        ActionUtil.initActionFromMap(this, map, icon);
    }

}