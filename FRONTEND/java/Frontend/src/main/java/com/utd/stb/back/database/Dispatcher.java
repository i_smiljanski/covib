package com.utd.stb.back.database;

import com.uptodata.isr.loader.webservice.LoaderGui;
import com.uptodata.isr.loader.webservice.LoaderWebserviceImpl;
import com.uptodata.isr.server.utils.process.ProcessHelper;
import com.uptodata.isr.utils.Constants;
import com.uptodata.isr.utils.ws.WsUtil;
import com.utd.gui.resources.I18N;
import com.utd.stb.Stabber;
import com.utd.stb.StabberInfo;
import com.utd.stb.back.data.client.*;
import com.utd.stb.back.ui.impl.lib.ActionList;
import com.utd.stb.back.ui.impl.lib.BasicNodeAction;
import com.utd.stb.back.ui.impl.lib.HelpSourceImpl;
import com.utd.stb.back.ui.impl.lib.WindowActionImpl;
import com.utd.stb.back.ui.impl.msgboxes.AlertMsgBox;
import com.utd.stb.back.ui.impl.msgboxes.AskCloseMsgBox;
import com.utd.stb.back.ui.impl.msgboxes.PasswordWillExpireMsgBox;
import com.utd.stb.back.ui.impl.msgboxes.WarningMsgBox;
import com.utd.stb.back.ui.impl.tree.TreeBase;
import com.utd.stb.back.ui.impl.userinputs.data.*;
import com.utd.stb.back.ui.impl.wizard.StandardReportWizard;
import com.utd.stb.back.ui.impl.wizard.StandardWizardErrorException;
import com.utd.stb.back.ui.impl.wizard.formdata.Grid;
import com.utd.stb.back.ui.impl.wizard.formdata.MultiSelect;
import com.utd.stb.back.ui.inter.ExtendedWizardStep;
import com.utd.stb.gui.loader.agent.LoaderAgent;
import com.utd.stb.gui.swingApp.App;
import com.utd.stb.gui.swingApp.exceptions.ExceptionDialog;
import com.utd.stb.gui.swingApp.login.UserLoginException;
import com.utd.stb.inter.action.NodeAction;
import com.utd.stb.inter.action.WindowAction;
import com.utd.stb.inter.application.*;
import com.utd.stb.inter.dialog.UserDialog;
import com.utd.stb.inter.userinput.UserInputs;
import com.utd.stb.inter.wizzard.WizardStep;
import org.apache.commons.codec.binary.Base64;
import org.apache.commons.lang.StringUtils;
import org.apache.commons.lang.math.NumberUtils;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import resources.IconSource;
import resources.Res;

import javax.crypto.*;
import javax.crypto.spec.PBEKeySpec;
import javax.crypto.spec.PBEParameterSpec;
import javax.swing.*;
import java.io.IOException;
import java.io.ObjectOutputStream;
import java.net.ServerSocket;
import java.net.Socket;
import java.nio.charset.StandardCharsets;
import java.security.InvalidAlgorithmParameterException;
import java.security.InvalidKeyException;
import java.security.NoSuchAlgorithmException;
import java.security.spec.AlgorithmParameterSpec;
import java.security.spec.InvalidKeySpecException;
import java.security.spec.KeySpec;
import java.sql.Connection;
import java.sql.SQLException;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.*;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.FutureTask;
import java.util.concurrent.RunnableFuture;

/**
 * Is the main interface to the underlying Database class, that executes the
 * queries Coordinates the sequences in the Report Wizard and supplies methods
 * used by TreeBase and the Dialogs.
 *
 * @author Rainer Bruns
 * @see com.utd.stb.back.database.DBInterface
 */
public class Dispatcher extends DispatcherBase {

    public static final Logger log = LogManager.getRootLogger();

    private TreeBase treeBase;
    private LoaderControl loaderControl;
    public final static String PASSWORDACTION = "CAN_CHANGE_OWN_PASSWORD";
    private final static String USER_JOBS = "CAN_DISPLAY_USER_JOBS";
    private boolean inWizard = false;
    private Cipher ecipher;
    private Cipher dcipher;
    private int counterOfFailures = 0;
    private MenuEntryRecord allJobsMenuRecord;

    //  8-byte Salt
    private byte[] salt = {(byte) 0xA9, (byte) 0x9B,
            (byte) 0xC8, (byte) 0x32,
            (byte) 0x56, (byte) 0x35,
            (byte) 0xE3, (byte) 0x03};

    // Ports
    private int controlPort;
    private int basisPort;

    // Loader
    @SuppressWarnings("unused")
    boolean loaderAvailable;

    // if the iSR runs in admin mode call others packages
    private boolean adminMode = false;

    private String user;
    private String password;

    private String server;
    private String dbUser;
    private String dbPassword;

    private String mainXmlTimestamp;

    private LoaderWebserviceImpl loader;


    public Dispatcher(String user,
                      String password,
                      String server,
                      String serverAlias) throws Exception {

        dbConnect(user, password, server, serverAlias, "");
    }

    /**
     * Constructor for the Dispatcher object
     *
     * @param user     The user name
     * @param password password users Password
     * @param server   the server user wants to connect to
     * @see com.utd.stb.back.database.DBInterface
     * @see com.utd.stb.Stabber
     */
    public Dispatcher(String user,
                      String password,
                      String dbUser,
                      String dbPassword,
                      String server,
                      String serverAlias,
                      String newPassword,
                      String mainXmlTimestamp
    )
            throws Exception {

        super();

        this.user = user;
        this.password = password;
        this.mainXmlTimestamp = mainXmlTimestamp;
        this.server = server;

        try {
            log.debug("try Oracle " + user);
            dbConnect(user, password, server, serverAlias, newPassword);
        } catch (Exception e) {
            log.debug("try LDAP " + dbUser);
//            boolean installationFailed = false;
            if (dbUser != null && dbUser.length() > 0
                    && dbPassword != null && dbPassword.length() > 0
                    && !dbUser.equals(user)) {
                dbConnect(dbUser, dbPassword, server, serverAlias, newPassword);
            } else {
                throw e;
            }
        }
        log.info("Database connection established");


        // check the configuration (user, versions, ...)
        db.checkIsValidUser(user, password);
        log.debug("Checked user");

        setDispatcherSettings();

        // initialize the TreeBase ( and with subsequent calls all the GUI stuff)
        treeBase = new TreeBase(this);
        log.debug("Treebase initialized");

        Exception startedEx = startWSLoader();
        log.debug(startedEx);
        if (startedEx != null) {
            throw startedEx;
        }
        log.info("Loader started");

        if (stabberConn.passwordWillExpire()) { //later will_expire
            try {
                showPasswordWillExpireDialog();
            } catch (BackendException ignored) {
                log.error(ignored);
            }
        }

        setLogDate();
        makeKeys("Rainers ganz geheimer Schluessel");

        log.debug("Dispatcher initialized");
    }


    public Dispatcher(DBInterface db) throws BackendException {
        this.db = db;
        setDispatcherSettings();
    }

    private void setDispatcherSettings() throws BackendException {
        setLanguage();
        // set dispatcher in the static classes for translations, icons, parameters ...
        StabberInfo.dispatcher = this;
        I18N.dispatcher = this;
        Res.dispatcher = this;
        IconSource.dispatcher = this;
        log.debug("Settings set");
    }

    private boolean checkMailXmlForUpdate(String sDateTimeMainXml) throws BackendException {
        String dateAsStringFromDb = db.getRecordTimestamp("ISR$FRONTEND$FILE", "FILENAME", "main.xml");
        log.debug(dateAsStringFromDb);
        log.debug(sDateTimeMainXml);
        boolean checkUpdateMainXml = true;
        if (StringUtils.isEmpty(dateAsStringFromDb)) {
            checkUpdateMainXml = false;
        } else if (StringUtils.isNotEmpty(sDateTimeMainXml)) {
            DateTimeFormatter formatter = DateTimeFormatter.ofPattern(Constants.DATE_PATTERN).withLocale(Locale.getDefault());
            LocalDateTime dateMainXml = LocalDateTime.parse(sDateTimeMainXml, formatter);
            LocalDateTime dateMainXmlDb = LocalDateTime.parse(dateAsStringFromDb, formatter);
            checkUpdateMainXml = checkUpdateFile(dateMainXml, dateMainXmlDb);
            log.debug(checkUpdateMainXml);
        }
        return checkUpdateMainXml;
    }


    private boolean checkUpdateFile(LocalDateTime dateTimeClient, LocalDateTime dateTimeDb) {
        int i = dateTimeClient.compareTo(dateTimeDb);
        log.debug(i);
        return i < 0;
    }

    private void dbConnect(String user, String password, String server,
                           String serverAlias, String newPassword) throws Exception {
//        log.debug(String.format("%s; %s; %s", user, password, server));
        try {
            connect(user, password, server, serverAlias, newPassword, this);
            createInterface();
            this.dbUser = user;
            this.dbPassword = password;
        } catch (Exception e) {
//            e.printStackTrace();
            if (e instanceof SQLException) {
                SQLException se = (SQLException) e;
                log.debug(((SQLException) e).getErrorCode());
                if (se.getErrorCode() == 1017) {
                    throw new UserLoginException(UserLoginException.USER_OR_PASSWORD);
                } else if (se.getErrorCode() == 17002) {
                    throw new UserLoginException(UserLoginException.DATABASE_NOT_FOUND);
                } else if (se.getErrorCode() == 28001) {
                    throw new UserLoginException(UserLoginException.PASSWORD_EXPIRED);
                } else if (se.getErrorCode() == 28000) {
                    throw new UserLoginException(UserLoginException.ACCOUNT_LOCKED);
                } else if (se.getErrorCode() == 6550) {
                    throw new UserLoginException(UserLoginException.IDENTIFIER_MUST_BE_DECLARED);
                } else if (se.getErrorCode() == 1403) {
                    throw new UserLoginException(UserLoginException.NO_DATA_FOUND);
                }
            }
            throw e;
        }
    }

    public LoaderWebserviceImpl getLoader() {
        return loader;
    }

    /**
     * starts the Loader
     *
     * @throws UserLoginException, BackendException
     */
    private Exception startWSLoader() {
        log.debug("startWSLoader");
        String portRange = getParameterValue("PORTRANGE");
        String context = getParameterValue("LOADER_CONTEXT");
        String loaderDir = getParameterValue("LOADER_BASEDIR");
        String host = getParameterValue("IP_ADDRESS");

        RunnableFuture<Exception> rf = new FutureTask<>(() -> {
            try {
                loader = new LoaderWebserviceImpl(false);
                String javaLogLevel = getDbLogLevel();
                if (StringUtils.isEmpty(javaLogLevel)) {
                    javaLogLevel = Stabber.getLogLevelFromProperties();
                }
                System.out.println("javaLogLevel = " + javaLogLevel);
                Stabber.initLogger(javaLogLevel);
//                log.setLevel(Level.toLevel(javaLogLevel));
//                loader.setLogLevel(javaLogLevel);
                loader.init(portRange, context, loaderDir, host);

                WsUtil wsUtil = new WsUtil();
                log.debug("is server '" + loader.wsUrl + "?wsdl'" + " available ? " + wsUtil.isServerAvailable(loader.wsUrl + "?wsdl"));

                LoaderGui loaderGui = new LoaderGui();
//                loaderGui.setSystemTrayApp(true);
                loader.setLoaderGui(loaderGui);
                loader.setDispatcher(this);

                int port = loader.getPort();
                loaderGui.createStatusBar(String.valueOf(port), loaderDir);
                setClientPort(port);
                return null;
            } catch (Exception ex) {
                log.error(ex);
                if (ex instanceof BackendErrorException) {
                    if (((BackendException) ex).getSeverity() == BackendException.SEVERITY_ACTION_EXIT) {
                        return ex;
                    }
                }
                ExceptionDialog.showExceptionDialog(null, ex, null, false, 0);
                return null;
            }
        });
        SwingUtilities.invokeLater(rf);
        try {
            return rf.get();
        } catch (InterruptedException | ExecutionException e) {
            e.printStackTrace();
            return e;
        }
    }

    /**
     * starts the Loader
     *
     * @throws UserLoginException, BackendException
     */
    private void startLoader() throws UserLoginException, BackendException {

        log.info("begin");
        boolean useSingleLoader = useSingleLoader();
        int minPort = getMinPort();
        int maxPort = getMaxPort();

        // if useSingleLoader take the min port, else check for the next available port
        basisPort = useSingleLoader ? minPort : checkPorts(minPort);
        // if useSingleLoader take the basis port + 1, else check for the next available port
        controlPort = useSingleLoader ? basisPort + 1 : checkPorts(basisPort + 1);

        // start the loader control with the ports
        if (basisPort <= maxPort || controlPort <= maxPort) {
            String[] args = {Integer.toString(basisPort),
                    Integer.toString(controlPort),
                    getParameterValue("loader.baseDir"),
                    getParameterValue("loader.socketTimeout"),
                    getParameterValue("loader.logging"),
                    getParameterValue("loader.debug")};
            loaderControl = new LoaderControl(true, basisPort, controlPort, args);
            loaderControl.start();
            /*if ( !loaderAvailable ) {
             throw new UserLoginException( UserLoginException.NO_STARTED_LOADER );
             }*/
            setClientPort(basisPort);
            log.info("end, basisPort: " + basisPort);
        } else {
            throw new UserLoginException(UserLoginException.NO_AVAILABLE_PORTS);
        }
    }

    /**
     * check for a free clientPort
     *
     * @throws IOException
     */
    private int checkPorts(int port) {

        try {
            new Socket("127.0.0.1", port).close();
            return checkPorts(port + 1);
        } catch (Exception e) {
            try {
                new ServerSocket(port).close();
            } catch (Exception e1) {
                return checkPorts(port + 1);
            }

            return port;
        }
    }

    /**
     * @return get the minimal port
     */
    private int getMinPort()  {

        String range = getParameterValue("PORTRANGE");
        return Integer.parseInt(range.substring(0, range.indexOf("-")).trim());
    }

    /**
     * @return get the maximal port
     */
    private int getMaxPort() throws BackendException {

        String range = getParameterValue("PORTRANGE");
        return Integer.parseInt(range.substring(range.indexOf("-") + 1).trim());
    }

    /**
     * @return if true use a single loader
     */
    private boolean useSingleLoader() throws BackendException {

        return getBooleanParameterValue("USESINGLELOADER");
    }

    /**
     * @return if true show doc versions separatly
     */
    public boolean showDocVersionSeparately() {

        return getBooleanParameterValue("SHOW_DOCVERSIONS_SEPARATELY");
    }

    /**
     * @return if true send a failure message by cancel on an esig dialog
     */
    public boolean sendFailureOnCancel() {

        return getBooleanParameterValue("SEND_ESIG_FAILURE_ON_CANCEL");
    }

    /**
     * @return if true prompt for an username on the esig dialog
     */
    public boolean getPromptUsername() {

        return getBooleanParameterValue("PROMPT_ESIG_USERNAME");
    }

    /**
     * @return the timeout of a dialog
     */
    public int getDialogTimeout() {

        int dialog_timeout = getIntParameterValue("DIALOG_TIMEOUT");
        return dialog_timeout == 0 ? 5 : dialog_timeout;
    }

    public int getTimeoutDialogTimeout() {

        int dialog_timeout = getIntParameterValue("TIMEOUT_DIALOG_TIMEOUT_SEC");
        return dialog_timeout == 0 ? 30 : dialog_timeout;
    }

    /**
     * @return the number a user types a wrong password
     */
    public int getNumberOfPossibleFailures() {

        return getIntParameterValue("NUMBER_ESIG_PASSWORD_FAILURE");
    }

    /**
     * User data are name, server, timeout, language, server alias and log level
     *
     * @return a map with user data
     * @throws BackendException
     */
    private Hashtable<String, Object> getUserData() throws BackendException {

        Hashtable<String, Object> userData = new Hashtable<String, Object>();
        userData.put("Name", stabberConn.getUser());
        userData.put("Fullname", db.getFullUserName());
        userData.put("Server", stabberConn.getServer());
        userData.put("Timeout", db.getTimeout());
        userData.put("DialogTimeout", getTimeoutDialogTimeout());
        userData.put("Lang", language == 1 ? Locale.GERMAN : Locale.ENGLISH);
        userData.put("Alias", stabberConn.getServerAlias());
        userData.put("IsNewMainXml", checkMailXmlForUpdate(mainXmlTimestamp));
        return userData;
    }

    private void setLogDate() {

        String dbDate;
        DateFormat df = new SimpleDateFormat(StabberInfo.getDatePattern(), Locale.GERMANY);
        try {
            dbDate = db.getDBTime();
            df.parse(dbDate);
        } catch (Exception ignored) {
            log.error(ignored);
        }
    }

    /**
     * called from Main Class on the Login Procedure, when this was succesfull
     *
     * @return the Base for the GUI
     */
    public Base getBase() {

        return treeBase;
    }

    /**
     * the Grid is initialized with data for first column. For each row in that
     * column data for the other columns is retrieved from this method.
     *
     * @param ent the entity (row) for which to get the list of data
     * @return a Selection List
     * @throws BackendException
     */
    public SelectionList getGridSelection(String ent)
            throws BackendException {

        SelectionList data = new SelectionList();
        data = db.getList(ent, null, true, 0, true);
        return data;
    }

    /**
     * saves the data from the different forms. This is the field value in
     * single selection the list of selected data in Picklist, the cell value in
     * Grid and the list of selectded data of one category in MultiSelect.
     * entity is the entity of the form, if null retrieved from actual form
     * info. suchwert is the required additional info in Grid: entity is the
     * column header, suchwert the rowid (first column value in row)
     * MultiSelect: suchwert is the category (or master).
     *
     * @param entity    the entity the data to save belong to
     * @param selection data to save
     * @throws BackendException
     */
    public void save(String entity,
                     SelectionList selection,
                     boolean grid)
            throws BackendException {

        if (entity == null) {
            entity = formInfo.getEntity();
        }
        try {
            db.putList(entity, selection, grid);
        } catch (StandardWizardErrorException swe) {
            showWizardError(swe);
        }
    }

    /**
     * gets the list of values for a cell in Grid or the details for a category
     * in MultiSelect
     *
     * @param entity       the entity for which to get the list of data: in Grid rowid
     * @param masterKey    in Grid: in Grid columnid
     * @param navigateMode
     * @param grid         if called from grid true, use another backend method in
     *                     DBInterface
     * @return the list of available data
     * @throws BackendException
     * @see Grid
     * @see MultiSelect
     */
    public SelectionList getAvailableItems(String entity,
                                           String masterKey,
                                           boolean navigateMode,
                                           boolean grid)
            throws BackendException {

        return db.getList(entity, masterKey, false, 0, grid);
    }

    /**
     * GUI asks ReportWizard for WizardStep (on Wizard initialization and
     * navigation) The request is forwarded to here. Its determined wether ->
     * current step number is lower than requested step number and if difference
     * is larger 1. Then call displayNextForm, either in Navigate mode or not ->
     * current step number is larger than requested step number and if
     * difference is larger 1. Then call displayPreviousForm, either in Navigate
     * mode or not -> no difference: call displayForm to fill step with FormData
     *
     * @param destination the WizardStep to display
     * @return the WizardStep to display, with correct FormData
     * @throws BackendException
     * @see StandardReportWizard
     */
    public WizardStep navigate(ExtendedWizardStep destination) throws BackendException {
        WizardStep step;
        if (destination.getCurrentIndex() > stdWizard.getIndexOfCurrentStep()) {

            step = displayForm(stdWizard.getIndexOfCurrentStep() + 1 != destination.getCurrentIndex(),
                    destination.getCurrentIndex());

        } else if (destination.getCurrentIndex() < stdWizard.getIndexOfCurrentStep()) {

            step = displayForm(stdWizard.getIndexOfCurrentStep() - 1 != destination.getCurrentIndex(),
                    destination.getCurrentIndex());
        } else {

            step = displayForm(false, stdWizard.getIndexOfCurrentStep());
        }
        return step;
    }

    /**
     * changes the password
     *
     * @return Object
     * @throws BackendException
     */
    public Object changePassword() throws BackendException {

        return callAction(getMenuFromToken(PASSWORDACTION), null);
    }

    public Object getMenuForJobs() {
        return getMenuForJobs(null);
    }


    public Object getMenuForJobs(String schemaOwner) {

        try {
            if (allJobsMenuRecord == null) {
                if (StringUtils.isNotEmpty(schemaOwner)) {
                    db.setSchemaOwner(schemaOwner);
                }
                allJobsMenuRecord = getMenuFromToken(USER_JOBS);
            }
            return callAction(allJobsMenuRecord, null);
        } catch (BackendException e) {
            e.printStackTrace();
            return null;
        }
    }

    public String getJobParameter(String parameterName, int jobId) throws BackendException {
        return db.getJobParameter(parameterName, jobId);
    }

    /**
     * ???
     *
     * @param menuEntryRecord the menu item pressed
     * @param propertyList    the property list of the mask to check
     * @param titleOfMask     the title of the mask to check
     * @return a displayable data object
     * @throws BackendException
     */
    public Object checkDependenciesOnMask(MenuEntryRecord menuEntryRecord,
                                          PropertyList propertyList,
                                          String titleOfMask,
                                          int maskType) throws BackendException {

        return checkDependenciesOnMask(menuEntryRecord, propertyList, titleOfMask, maskType, null);
    }

    public Object checkDependenciesOnMask(MenuEntryRecord menuEntryRecord,
                                          PropertyList propertyList,
                                          String titleOfMask,
                                          int maskType,
                                          Object noBtnAction)
            throws BackendException {

        log.debug("begin");
        UserDialog dialog = null;
        Object returnAction;

        PropertyList props = db.checkDependenciesOnMask(menuEntryRecord,
                propertyList,
                adminMode);

        boolean isModified = db.getModifiedFlag();
        log.debug("getModifiedFlag: " + isModified);

        if (maskType == Node.MENU_LIST_DIALOG) {
            UserInputData dialogData = new UserInputData(this, props, menuEntryRecord, ecipher);
            log.debug(props.getFirst().getValueAsString());
            if (isModified) {
                if (noBtnAction == null) {
                    noBtnAction = new MenuListDialog(dialogData,
                            titleOfMask,
                            new HelpSourceImpl(menuEntryRecord.getToken()));
                }
                UserDialog askExit = new AskCloseMsgBox(getTranslation("main.askSaveChanges.header.title"),
                        getTranslation("main.askSaveChanges.text"), dialogData, noBtnAction);
                log.debug(askExit);
                returnAction = askExit;
                log.debug("end " + returnAction);
                return returnAction;
            } else if (noBtnAction != null) {  //close window
                returnAction = noBtnAction;
                log.debug("end " + returnAction);
                return returnAction;
            } else {
                dialog = new MenuListDialog(dialogData,
                        titleOfMask,
                        new HelpSourceImpl(menuEntryRecord.getToken()));
            }
        } // nur neuen Dialog anzeigen, wenn sich die property list geaendert hat
        else if (db.getModifiedFlag()) {

            UserInputData dialogData = new UserInputData(this, props, menuEntryRecord, ecipher);
            dialog = new MenuDialog(dialogData,
                    titleOfMask,
                    new HelpSourceImpl(menuEntryRecord.getToken()));
        }
        returnAction = dialog;
        log.debug("end " + returnAction);
        return returnAction;
    }

    /**
     * Gets action to execute from a token. First gets menu for token, and then
     * action for menu
     *
     * @param token the menu token(i.e. CAN_CHANGE_PASSWORD) for which to get
     *              action
     * @return the action(dialog..) to be done
     * @throws BackendException
     */
    public Object getActionFromToken(String token) throws BackendException {

        if (isMenuItemEnabled(token)) {
            log.debug(token);
            MenuEntryRecord menu = db.getMenu(token, adminMode);
            return callAction(menu, null);
        }

        return null;
    }

    /**
     * Gets a menu entry record from a token(i.e. CAN_CHANG_PASSWORD)
     *
     * @param token the token for which to get menu
     * @return the MenuEntryRecord
     * @throws BackendException
     */
    public MenuEntryRecord getMenuFromToken(String token) throws BackendException {

        return db.getMenu(token, adminMode);
    }

    /**
     * if a grid cell has been changed, dependend columns have to be set to null
     * the columns are identified by this method
     *
     * @param entity the entity (column) in Grid that has been changed
     * @param rowid  the row of change
     * @return a list of dependend entities
     * @throws BackendException
     * @see Grid
     */
    public EntityList getDependendData(String entity, String rowid) throws BackendException {

        return db.getGridDependency(entity, rowid);
    }

    /**
     * @param props a property list
     * @return true if any field in list is editable
     */
    private boolean hasEditableFields(PropertyList props) {

        for (int i = 0; i < props.getSize(); i++) {

            PropertyRecord rec = props.get(i);
            if (!rec.isReadOnly()) {

                return true;
            }
        }

        return false;
    }

    /**
     * called from putPropertyList. Returns a dialog if auditing is required
     *
     * @return a DialogInWizard to be displayed
     * @throws BackendException
     */
    public SaveAuditDialog showAuditDialog() throws BackendException {
        log.info("begin");

        TreeNodeList treeNodes = getAuditDialog();
        TreeNodeRecord treeNode = null;
        SaveAuditDialog dialog = null;

        try {

            //the one and only
            treeNode = treeNodes.get(0);

        } catch (NullPointerException ignored) {
            log.error(ignored);
        }

        PropertyList properties = treeNode.getProperties();

        // initialise InputData and dialog and return to show
        UserInputData dialogData = new UserInputData(this,
                properties,
                UserInputData.SAVE_AUDIT);

        // if in Wizard, don't call cancelMask!
        dialog = new SaveAuditDialog(dialogData,
                getTranslation("AUDIT_TEXT"),
                !inWizard,
                "AuditHelp");
        log.info("end " + dialog);
        return dialog;

    }

    /**
     * called from the Report Wizard on NavigateFinish gets the fields available
     * in save dialog for current report type( as a list of properties in the
     * only TreeNodeRecord in TreeNodeList returned from backend if save dialog
     * has editable fields return a DialogInWizard object to be displayed else
     * call the saveData method in this dialog, which return a dialog for
     * auditing by calling the saveCustomProperties method here. Might be null.
     *
     * @return a UserInputDialog
     * @throws BackendException
     * @see SaveAuditDialog
     * @see UserInputData
     */
    public Object showSaveDialog() throws BackendException {
        log.info("begin");
        TreeNodeList treeNodes = getSaveDialog();
        TreeNodeRecord treeNode = null;
        Object dialog;

        try {
            //the only one
            treeNode = treeNodes.get(0);
        } catch (NullPointerException ignored) {
        }

        PropertyList properties = treeNode.getProperties();

        //initialise the data, tell to save the report, and show
        UserInputData dialogData = new UserInputData(this,
                properties,
                UserInputData.SAVE_REPORT);
        //check if there is an editable field
        if (hasEditableFields(properties)) {
            //if in Wizard, don't call cancelMask!
            dialog = new SaveAuditDialog(dialogData,
                    getTranslation("msgSaveReport"),
                    !inWizard,
                    "SaveReport");
        } else {
            //save might return another (audit) dialog
            dialog = dialogData.saveData();
        }
        log.info("end " + dialog);
        return dialog;
    }

    /**
     * called from the Dispatcher after start the start of the application if a
     * warning has to be shown
     *
     * @return null
     * @throws BackendException
     */
    public Object showWarningDialog(Exception e) {

        String text = e.getMessage();
        String message = e.getLocalizedMessage();

        try {
            text = getTranslation(e.getMessage());
            message = getTranslation(e.getLocalizedMessage());
        } catch (BackendException ignored) {
            log.error(ignored);
        }

        UserDialog u = new WarningMsgBox(text, message);
        ActionList actionList = new ActionList();
        actionList.add(u);
        treeBase.setActionList(actionList);
        return null;
    }

    /**
     * called from the Dispatcher after start the start of the application if
     * the password will expire
     *
     * @return null
     * @throws BackendException
     */
    public Object showPasswordWillExpireDialog() throws BackendException {

        UserDialog u = new PasswordWillExpireMsgBox(getTranslation("msgWarning"),
                getTranslation("msgPasswordWillExpireText"),
                this);
        ActionList actionList = new ActionList();
        actionList.add(u);
        treeBase.setActionList(actionList);
        return null;
    }

    /**
     * @return reload or not reload
     */
    public boolean checkReload() {
        return db.checkReload();
    }

    /**
     * called from any action (dialog, wizard) on exit. Checks what Nodes in
     * tree have to be reloaded/expanded/selected. gets a list of nodes to be
     * reloaded and expanded, and a single node to be selected token returned
     * from backend determines wether selected node is in tree (token == null)
     * or in deatil view (token == RIGHT). A list of NodeActions is build and
     * set as actionList in TreeBase, from where they are queried by GUI.
     *
     * @return NodeAction, however actually null only
     * @throws BackendException
     * @see TreeBase
     * @see ActionList
     */
    NodeAction reload() throws BackendException {
        return reload(false, "", "", "");
    }

    public NodeAction reload(boolean inBackground, String nodetype, String nodeid, String sessionid) throws BackendException {
        log.debug("begin reload");
        String token;
        TreeNodeList nodesToLoad = new TreeNodeList();
        TreeNodeRecord newNode = new TreeNodeRecord();
        TreeNodeRecord lastLoaded = null;

        try {
            //get the data from backend
            if (inBackground) {
                token = db.synchronizeInBackground(nodesToLoad, newNode, nodetype, nodeid, sessionid);
            } else {
                token = db.synchronize(nodesToLoad, newNode, adminMode);
            }
        } catch (NullPointerException ne) {
            log.error("NullPointerException during reload tree " + ne);
            return null;
        } catch (RuntimeException re){
            log.error(re);
            return null;
        }

        ActionList loadActions = new ActionList();
//        log.debug(nodesToLoad);
        // for each nodeToLoad create an reload and expand action
        for (int i = 0; i < nodesToLoad.getSize(); i++) {
            TreeNodeRecord myNode = nodesToLoad.get(i);
            log.debug("load node " + myNode.getDisplay());
            if (!myNode.getDisplay().equals("DONT_RELOAD")) {
                loadActions.add(new BasicNodeAction(NodeAction.RELOAD, myNode));
            }
            loadActions.add(new BasicNodeAction(NodeAction.EXPAND, myNode));
            lastLoaded = myNode;
        }

        log.debug("load side " + token + " and node " + newNode);
        if (token == null) {
            // reload details from the new node and select the newNode in detail
            loadActions.add(new BasicNodeAction(NodeAction.SELECT_AND_RELOAD_DETAIL, newNode));
        } else if (token.equalsIgnoreCase("MENU_DETAIL_VIEW")) {
            loadActions = new ActionList();
            // reload details from the last loaded node (left window)
            loadActions.add(new BasicNodeAction(NodeAction.SELECT_AND_RELOAD_DETAIL, nodesToLoad.get(nodesToLoad.getSize() - 1)));
            // the menu detail nodes are all nodes except for the last node
            TreeNodeList menuDetailNodes = new TreeNodeList();
            for (int i = 0; i < nodesToLoad.getSize() - 1; i++) {
                menuDetailNodes.add(nodesToLoad.get(i));
            }
            loadActions.add(new MenuToDetail(menuDetailNodes, this));
            // select the newNode in detail (right window)
            loadActions.add(new BasicNodeAction(NodeAction.SELECT_IN_DETAIL, newNode));
        } else if (token.equalsIgnoreCase("RIGHT")) {
            // reload details from the last loaded node ...
            loadActions.add(new BasicNodeAction(NodeAction.SELECT_AND_RELOAD_DETAIL, lastLoaded));
            // ... and select the newNode in detail
            loadActions.add(new BasicNodeAction(NodeAction.SELECT_IN_DETAIL, newNode));
        }
        //set the list of actions in TreeBase
        treeBase.setActionList(loadActions);
        log.debug("end reload");

        return null;
    }

    /**
     * all menu actions get here. Form backend a TreeNodeList record is
     * returned. actually some tokens have to be interpreted here -> if type of
     * record is LOCKEDREPORTS return a DetailViewAction to be displayed in the
     * detail part of main window. Data is a list of TreeNodeRecords containing
     * the locked reports -> if token of menuEntry is CAN_RELEASE_FOR_INSPECTION
     * return an EsigDialog -> if token of menuEntry is CAN_CHANGE_OWN_PASSWORD
     * return a ChangePasswordDialog -> else return a MenuDialog all three are
     * UserDialogs, but with diffrence in data handling (password checks)
     * another special: if a menuitem corresponding to report editing is pressed
     * a BackendWorkflowException is thrown from DBInterface (its originated
     * from the ErrorRecord returned from backend by interpreting the error
     * code) if so call method firstForm to initialise the Wizard (for
     * convinience think of changing this from Exception to TreeNode Type later)
     *
     * @param menuEntry the menu item pressed
     * @return a displayable data object
     * @throws BackendException
     */
    public Object callAction(MenuEntryRecord menuEntry,
                             TreeNodeRecord selectedNode)
            throws BackendException {
        log.debug("begin " + menuEntry);
        TreeNodeList actionList;
        UserDialog dialog = null;
        UserInputData dialogData;

        try {
            actionList = db.callFunction(menuEntry, adminMode);
            log.debug("actionList null? " + (actionList == null));
            if (actionList == null || actionList.getSize() == 0) {
                reload();  //this function can cause error, but without it "synchronizeTree" doesn't work [ISRC-729] todo: watch it
                log.debug("end " + menuEntry);
                return null;
            }
            TreeNodeRecord rec = actionList.get(0);

            /*if("J".equals(menuEntry.getConfirm())){
             log.debug(menuEntry.getDisplay());
             treeBase.getJobsTableInfo().setJobsTable(true);
             }
             else{
             treeBase.getJobsTableInfo().setJobsTable(false);
             }*/
            if (rec.getDisplayType() == Node.MENU_DETAIL_VIEW) {
                return new MenuToDetail(actionList, this);
            }

            if (rec.getDisplayType() == Node.MENU_CALL_PROCESS) {
                callProcess(rec);
                return null;
            }

            byte[] xmlInput = rec.getXmlFile();
            HelpSourceImpl helpSource = new HelpSourceImpl(menuEntry.getToken());
//            log.debug(menuEntry.print());
            if (xmlInput != null) {
                FilterTableData tableData = new FilterTableData(xmlInput, this, menuEntry);
//                log.debug("tableData: "+tableData.getTableModel());
                dialog = new FilterTableDialog(tableData,
                        rec.getDisplay(),
                        helpSource);
            } else {
                PropertyList props = rec.getProperties();
                if (rec.getDisplayType() == Node.OWN_PASSWORD_DIALOG) {
                    dialogData = new UserInputData(this, props, menuEntry.getToken(), ecipher);
                    dialog = new MenuDialog(dialogData,
                            rec.getDisplay(),
                            helpSource);
                } else {
                    dialogData = new UserInputData(this, props, menuEntry, ecipher);
                    log.debug(rec.getDisplayType());
                    if (rec.getDisplayType() == Node.ESIG_DIALOG) {
                        dialog = new MenuDialog(dialogData,
                                rec.getDisplay(),
                                helpSource,
                                true);
                    } else if (rec.getDisplayType() == Node.MENU_LIST_DIALOG) {
                        dialog = new MenuListDialog(dialogData,
                                rec.getDisplay(),
                                helpSource);
                    } else {
                        dialog = new MenuDialog(dialogData,
                                rec.getDisplay(),
                                helpSource);
                    }
                }
            }
            log.debug("end " + menuEntry);
        } catch (BackendWorkflowException bee) {
            if (bee.getInternalSeverity() == ErrorRecord.cnFirstForm) {
                inWizard = true;
                return showWizardDialog(1, selectedNode);
            }

            if (bee.getInternalSeverity() == ErrorRecord.cnAudit) {
                reload();
                return showAuditDialog();
            }

        } catch (BackendException be) {
            if (be instanceof BackendErrorException && ((BackendErrorException) be).getInternalSeverity() != ErrorRecord.cnMsgWarningTimeout) {
                reload();
            }
            return callWarningMsgBox(be);
        } catch (Exception ne) {
            log.error(ne);
            return null;
        }
        return dialog;
    }


    private void callProcess(TreeNodeRecord rec) {
        ValueList valueList = rec.getValueList();
        String process = "";
        List<String> args = new ArrayList<>();
        for (int k = 0; k < valueList.getSize(); k++) {
            String paramName = valueList.get(k).getParameterName();
            String paramValue = valueList.get(k).getParameterValue();
            if ("process".equals(paramName)) {
                process = paramValue;
            } else {
                args.add(paramName + "=" + paramValue);
            }
        }

        /*try {
            log.debug(SymmetricProtection.encrypt(dbPassword));
        } catch (Exception e) {
            e.printStackTrace();
        }*/
        args.add("DbUser=" + dbUser);
        args.add("DbPwd=" + dbPassword);

        int connLenth = server.split(":").length;
        args.add("DbHost=" + server.split(":")[0]);
        if (connLenth == 3) {
            args.add("DbPort=" + server.split(":")[1]);
            args.add("DbSid=" + server.split(":")[2]);
        } else if (connLenth == 2) {
            String serverPart = server.split(":")[1];
            args.add("DbPort=" + serverPart.split("/")[0]);
            args.add("DbSid=" + serverPart.split("/")[1]);
        }

        log.debug(process + " " + args);

        String ret = null;
        try {
            ret = ProcessHelper.startCmdProcess("lib\\", process, args.toArray(new String[0]));
        } catch (Exception e) {
            log.error(e);
        }
        log.debug(ret);
    }


    public void commitTable(Object table) throws BackendException {

        TreeNodeList nodes = new TreeNodeList();

        ArrayList tableData = (ArrayList) table;
        for (int i = 0; i < tableData.size(); i++) {
            if (i % 2 == 0) {
                PropertyList properties = new UserInputData(this).getData((ArrayList) tableData.get(i + 1));
                TreeNodeRecord node = (TreeNodeRecord) ((Node) tableData.get(i));
                //node.setProperties(properties);
                nodes.add(node);
            }
        }

        db.putTable(nodes, adminMode);
    }

    /**
     * called from any MenuDialog on save. A BackendWorkflowExcepion might be
     * thrown if auditing is required. This is initialise by the ErrorRecord by
     * checking the error code. If so, auditing is required and UserDialog
     * Object is returned. Else checkReload. (later better change backend to
     * return dialog data directly, then kill BackendWorkflowException)
     *
     * @param props the list of data to be saved
     * @return a UserDialog object if audit is required
     * @throws BackendException
     */
    public Object putPropertyList(PropertyList props, Object object)
            throws BackendException {
        return putPropertyList(props, null, object);
    }

    public Object putPropertyList(PropertyList props,
                                  String stream,
                                  Object object)
            throws BackendException {

        try {
            db.putPropertyList(props, stream, object, adminMode);

            // UND WENN ES KEIN FENSTER ZUM SCHLIESSEN GIBT???
            // SOLLTE EGAL SEIN ...
            if (object != null) {
                try {
                    reload();
                } catch (Exception e) {
                    log.error("error during reload tree " + e);
                }
            }

            return new WindowActionImpl(WindowAction.CLOSE_WINDOW);

        } catch (BackendWorkflowException bee) {
            log.debug(bee);
            if (bee.getInternalSeverity() == ErrorRecord.cnFirstForm) {

                // display wizard
                inWizard = true;
                return showWizardDialog(1, null);
            }

            if (bee.getInternalSeverity() == ErrorRecord.cnAudit) {

                // display audit
                reload();
                return showAuditDialog();
            }

        } catch (BackendException be) {
            log.debug(be);
            return callWarningMsgBox(be);
            // throw hard error (is displayed in App)

        }

        reload();
        return null;
    }

    private Object callWarningMsgBox(BackendException be) throws BackendException {
        log.debug("begin " + be.getSeverity());
        if (be.isInformational()) {
            if (be instanceof BackendErrorException && ((BackendErrorException) be).getInternalSeverity()
                    == ErrorRecord.cnMsgWarningTimeout) {
                return new AlertMsgBox(((BackendErrorException) be).getErrorCode(), be.getMessage(), getDialogTimeout());

            } else {
                return ExceptionDialog.createWarningMsgBox("Backend exception", (BackendErrorException) be);
            }

        }
        log.debug("end");
        // throw hard error (is displayed in App)
        throw be;
    }

    /**
     * called in the change password dialog
     *
     * @param newPassword the new password
     * @param oldPassword the old password
     * @return a UserDialog object
     * @throws BackendException
     */
    public Object changePassword(String newPassword, String oldPassword)
            throws BackendException {

        db.changePassword(newPassword, oldPassword);
        if (oldPassword != null) {
            stabberConn.setPassword(newPassword);
        }
        return new WindowActionImpl(WindowAction.CLOSE_WINDOW);
    }

    /**
     * @return
     */
    public String getUser() {

        return user;
    }

    /**
     * @return
     */
    private String getUserPassword() {

        return password;
    }

    /**
     * @return
     */
    public String getServerAlias() {

        return stabberConn.getServerAlias();
    }

    /**
     * @param password
     * @return true if encryted String password equals users password
     */
    public boolean checkPassword(String password) {

        if (getUserPassword().toUpperCase().equals(decrypt(password).toUpperCase())) {
            return true;
        } else {
            counterOfFailures++;
            return false;
        }
    }

    /**
     * @param user
     * @return true if String equals connected user
     */
    public boolean checkUser(String user) {

        if (getUser().toUpperCase().equals(user.toUpperCase())) {
            return true;
        } else {
            counterOfFailures++;
            return false;
        }
    }

    /**
     * @return the number a user types a wrong password
     */
    public int getNumberOfPasswordFailures() {

        return counterOfFailures;
    }

    /**
     * @return the number a user types a wrong username
     */
    public int getNumberOfUserFailures() {

        return counterOfFailures;
    }

    public void setNumberOfFailuresToNull() {

        counterOfFailures = 0;
    }

    /**
     * on abort wizard. Calls the cleanup in backend, which returns a treenode
     * to select
     *
     * @return NodeAction, what to do in tree ba GUI
     * @throws BackendException
     */
    public NodeAction abortWizard() throws BackendException {

        inWizard = false;
        return new BasicNodeAction(NodeAction.SELECT, db.cleanUp());
    }

    /**
     * on finish wizard. Calls the checkReload in backend, which returns a
     * treenode
     *
     * @return NodeAction, what to do in tree ba GUI
     * @throws BackendException
     */
    public NodeAction finishWizard() throws BackendException {

        inWizard = false;
        return reload();
    }

    /**
     * tell the loader that we finish, cleanup in backend, close connection
     *
     * @throws SQLException
     */
    public void shutdown(int reason) throws SQLException {
        log.debug("Dispatcher.shutdown:reason = " + reason);
        loaderControl = new LoaderControl(false, basisPort, controlPort, null);
        loaderControl.start();
        db.finalise(reason);
    }

    /**
     * called from TreeBase
     *
     * @param node the node for which to get the childs
     * @return the list of childs for node
     * @throws BackendException
     * @see TreeBase
     */
    public TreeNodeList getTreenodeChilds(TreeNodeRecord node) throws BackendException {

        return getNodeChilds(node);
    }

    /**
     * called from Treebase
     *
     * @param node the node for which to get the details
     * @return the list of deatil data
     * @throws BackendException
     * @see TreeBase
     */
    public TreeNodeList getTreenodeDetails(TreeNodeRecord node) throws BackendException {

        return getNodeList(node);
    }

    /**
     * sets the current node in backend when node is selected or menu is
     * requested
     *
     * @param node the current selected node
     * @throws BackendException
     * @see TreeBase
     */
    public void setNode(TreeNodeRecord node, boolean left) throws BackendException {

        db.setCurrentNode(node, left);
    }

    /**
     * gets a list of menu items for given menu id
     *
     * @param id id of menu, 0 for main
     * @return a list of MenuEntries
     * @throws BackendException
     * @see TreeBase
     */
    public MenuEntryList getMenuList(int id) throws BackendException {

        return db.getContextMenu(id);
    }

    /**
     * checks wether menu item is enabled
     *
     * @param id id of menu item
     * @return true if enabled, else false
     * @throws BackendException
     * @see TreeBase
     */
    public boolean isMenuItemEnabled(String id) throws BackendException {

        return db.getItemAccess(id);
    }

    /**
     * gets user info, actually username, connection info, Locale and timeout
     *
     * @return the user data
     * @throws BackendException
     * @see App
     */
    public Hashtable<String, Object> getUserInfo() throws BackendException {

        return getUserData();
    }

    /**
     * for GUI element persistance
     *
     * @return a list of Object key/Object property
     * @throws BackendException
     */
    public Properties loadPersistance() throws BackendException {

        return db.getPersistance();
    }

    /**
     * The initial tree node is send to backend to retrieve the main nodes
     *
     * @return an initial TreeNodeRecord
     * @throws BackendException
     */
    public TreeNodeRecord getInitialNode() throws BackendException {

        return (db.getNodeChilds(db.getInitialNode(), true)).get(0);
    }

    /**
     * MCD, 25.Jan 2006
     *
     * @return returns the form info for the form i
     * @throws BackendException
     */
    public FormInfoRecord getCurrentFormInfo(int i) throws BackendException {

        ArrayList entities = new ArrayList();
        FormInfoRecord formInfo = db.getCurrentFormInfo(entities, i);
        return formInfo;
    }

    /**
     * @param entity   entity to get LOV for
     * @param suchwert additional information
     * @return a list of valid entries
     * @throws BackendException
     */
    public SelectionList getLov(String entity, String suchwert) throws BackendException {

        return db.getLov(entity, suchwert, adminMode);
    }

    /**
     * For custom menus a separate db connection is created
     *
     * @return a new database connection
     * @throws BackendException
     */
    public Connection getCustomConnection() throws BackendException {

        return stabberConn.getCustomConnection();
    }

    /**
     * saves GUI elements properties
     *
     * @param persistance a list of Object key/Object property
     * @throws BackendException
     */
    public void savePersistance(Properties persistance) throws BackendException {

        db.putPersistance(persistance);
    }

    /**
     * Creates the keys for encryption and decrytion
     *
     * @param passPhrase an string used in key creation
     */
    private void makeKeys(String passPhrase) {

        try {

            // Create the key
            // Iteration count
            int iterationCount = 19;
            KeySpec keySpec = new PBEKeySpec(passPhrase.toCharArray(), salt, iterationCount);
            SecretKey key = SecretKeyFactory.getInstance("PBEWithMD5AndDES").generateSecret(keySpec);
            ecipher = Cipher.getInstance(key.getAlgorithm());
            dcipher = Cipher.getInstance(key.getAlgorithm());

            // Prepare the parameter to the ciphers
            AlgorithmParameterSpec paramSpec = new PBEParameterSpec(salt, iterationCount);

            // Create the ciphers
            ecipher.init(Cipher.ENCRYPT_MODE, key, paramSpec);
            dcipher.init(Cipher.DECRYPT_MODE, key, paramSpec);

        } catch (InvalidAlgorithmParameterException | InvalidKeySpecException | NoSuchPaddingException | NoSuchAlgorithmException | InvalidKeyException ignored) {
            log.error(ignored);
        }
    }

    /**
     * @param str String to decrypt
     * @return the decrypted String
     */
    private String decrypt(String str) {

        try {

            // Decode base64 to get bytes
            byte[] dec = Base64.decodeBase64(str);

            // Decrypt
            byte[] utf8 = dcipher.doFinal(dec);

            // Decode using utf-8
            return new String(utf8, StandardCharsets.UTF_8);

        } catch (javax.crypto.BadPaddingException | IllegalBlockSizeException e) {
            log.error(e);
        }

        return null;
    }

    /**
     * called on any action canceled
     */
    public void cancelAction() {

        db.cancel(adminMode);

    }

    /**
     * get a system parameter
     */
    public String getParameterValue(String parameter) {

        try {
            return db.getParameterValue(parameter);
        } catch (Exception e) {
            return "";
        }
    }

    /**
     * get the logleve
     */
    public String getDbLogLevel() {

        try {
            return db.getJavaLogLevel();
        } catch (Exception e) {
            return "";
        }
    }

    /**
     * get a system parameter as boolean
     */
    public boolean getBooleanParameterValue(String parameter) {

        try {
            return getParameterValue(parameter).equals("T");
        } catch (Exception e) {
            return false;
        }
    }

    /**
     * get a system parameter as integer
     */
    public int getIntParameterValue(String parameter) {
        String value = getParameterValue(parameter);
        return NumberUtils.toInt(value, 0);
    }

    /**
     * get a icon
     */
    public byte[] getIcon(String size, String id) {
        return db.getIcon(size, id);
    }

    /**
     * @param msg string to translate
     * @return translated string according to users language
     * @throws BackendException
     */
    public String getTranslation(String msg, String... params) throws BackendException {

        return db.getTranslation(msg, language, params);
    }

    // Installer
    public boolean checkRole(String sRole) {
        return db.checkRole(sRole);
    }

    public boolean preInstallFile(String object) throws BackendException {
        return db.preInstallFile(object);
    }

    /**
     * implements a DetailView, this is what is shown in detail part of main
     * window
     *
     * @author rainer bruns
     */
    private class MenuToDetail implements DetailView {

        private TreeNodeList treeNodes;
        private Dispatcher dispatcher;

        public MenuToDetail(TreeNodeList treeNodes, Dispatcher d) {

            this.treeNodes = treeNodes;
            this.dispatcher = d;

        }

        public String getTitle() {

            // MCD, 26. Nov 2014, use null to create the same view as in the right explorer window
            //TreeNodeRecord node = treeNodes.get(0);
            //return node.getDisplay();
            return null;

        }

        public Nodes getNodes() {

            return treeNodes;
        }

        public UserInputs getNodeProperties(Node node) {

            try {
                TreeNodeRecord nodeRec = (TreeNodeRecord) node;
                PropertyList props = nodeRec.getProperties();
                return new UserInputData(dispatcher, props, nodeRec);
            } catch (BackendException be) {
                return null;
            }
        }
    }

    /**
     * a thread that starts a Loader process if not already started, or
     * registers at Loader. On exit of application deregister
     *
     * @author rainer bruns
     * @see com.utd.stb.gui.loader.agent.LoaderAgent
     */
    private class LoaderControl extends Thread {

        Socket s;
        boolean mode;
        int basisPort;
        int controlPort;
        String[] args;

        /**
         * @param mode true: start or register at loader, else deregister
         */
        public LoaderControl(boolean mode, int basisPort, int controlPort, String[] args) {
            this.mode = mode;
            this.basisPort = basisPort;
            this.controlPort = controlPort;
            this.args = args;
        }

        public void run() {
            if (mode) {
                startLoader();
            } else {
                informLoaderFromStop();
            }
        }

        /**
         * starts the loader process
         *
         * @throws UserLoginException
         * @throws Exception
         */
        private void initLoader() {

            log.info("Bootpath " + System.getProperty("sun.boot.library.path"));
            log.info("args = " + args[0] + " " + args[1] + " " + args[2] + " " + args[3] + " " + args[4] + " " + args[5]);
            new LoaderAgent(args, null);
        }

        /**
         * check whether Loader is available on port and register, else start a
         * process
         *
         * @throws UserLoginException
         * @throws Exception
         */
        private void startLoader() {
            try {

                s = new Socket("127.0.0.1", controlPort);

                Hashtable<String, Object> data = new Hashtable<String, Object>();
                data.put("HEAD", "NO_JOBID");
                data.put("INFO", "1");

                ObjectOutputStream ostream = new ObjectOutputStream(s.getOutputStream());
                ostream.writeObject(data);
                ostream.flush();
                ostream.close();

                s.close();
            } catch (IOException ie) {
                ie.printStackTrace();
                initLoader();
            }

        }

        /**
         * say bye to loader, no nice answer expected
         */
        public void informLoaderFromStop() {
            LoaderAgent.stopLoader(controlPort);
        }
    }

    /**
     * <code>OFF</code>, <code>FATAL</code>, <code>ERROR</code>,
     * <code>WARN</code>, <code>INFO</code, <code>DEBUG</code> and
     * <code>ALL</code>
     *
     * @param level
     * @return Level
     */
  /*  private Level getLogLevel(int level) {
        if (level == Integer.MIN_VALUE) {
            return Level.OFF;
        } else if (level < -10000) {
            return Level.FATAL;
        } else if (level < -800) {
            return Level.ERROR;
        } else if (level < 0) {
            return Level.WARN;
        } else if (level < 101) {
            return Level.INFO;
        } else if (level < 260) {
            return Level.DEBUG;
        } else {
            return Level.ALL;
        }
    }*/

    /**
     * gets xml from database
     *
     * @return
     */
    public Object getXml(MenuEntryRecord menuEntry, Object action) throws BackendException {

        byte[] actionList = null;

        try {

            actionList = db.getXml(menuEntry, (byte[]) action);

        } catch (BackendWorkflowException | NullPointerException bee) {
            log.error(bee);
            bee.printStackTrace();
        } catch (BackendException be) {
            return callWarningMsgBox(be);
            // throw hard error (is displayed in App)		
        }
        return actionList;
    }

    /**
     * puts xml to database
     *
     * @return
     */
    public Object putXml(MenuEntryRecord menuEntry, Object action) throws BackendException {

        try {

            db.putXml(menuEntry, (byte[]) action);

        } catch (BackendWorkflowException | NullPointerException | IOException bee) {
            log.error(bee);
            bee.printStackTrace();
        } catch (BackendException be) {
            return callWarningMsgBox(be);
            // throw hard error (is displayed in App)
        }

        reload();
        return null;
    }

    public Object getNodeDetails() throws BackendException {

        byte[] actionList = null;

        try {
            actionList = db.getNodeDetails();
        } catch (BackendWorkflowException | NullPointerException bee) {
            log.error(bee);
            bee.printStackTrace();
        } catch (BackendException be) {
            return callWarningMsgBox(be);
        }
        return actionList;
    }

    public void updateTree() {

        try {
            db.updateTree();
        } catch (Exception e) {
            log.error(e);
        }
    }

    public void setLoaderConnection(String port, String user, boolean start) throws BackendException {
        db.setLoaderConnection(port, user, start);
    }

}
