package com.utd.gui.controls.table.model;

import java.util.Collection;
import java.util.Collections;


/**
 * A <code>List</code> based Tablemodel that allow to move a bunch of rows
 * up/down very easy, rows on the top/bottom side stay at their position,
 * multiple TableEvents fired
 *
 * @author PeterBuettner.de
 */
public abstract class MovableRowsTableModel extends AbstractListTableModel {

    public MovableRowsTableModel(Collection collection) {

        super(collection);
    }

    public MovableRowsTableModel() {

    }

    private static boolean canMove(int[] rowsIndices, int count, boolean moveTowardZero) {

        if (rowsIndices == null || rowsIndices.length == 0 || rowsIndices.length == count)

            return false;// all selected or nothing

        if (moveTowardZero) {

            for (int i = 0; i < rowsIndices.length; i++)

                if (rowsIndices[i] > i)

                    return true;// found non selected gap

        } else {

            int k = count - 1; // top row that can't be moved down

            for (int i = rowsIndices.length - 1; i >= 0; i--) {// go down

                if (rowsIndices[i] < k)

                    return true;// found non selected gap

                k--;

            }

        }

        return false;

    }

    /**
     * moves all rows one up (direction to zero index), but not the first
     * contiguous intervall starting at 'zero', so noncontiguous rows are moved
     * up until they are all stacked together at the start.
     *
     * @param rows needs to be sorted!
     */
    public void moveUp(int[] rows) {

        for (int i = 0; i < rows.length; i++) { // go up

            if (rows[i] > i) {// w/o: mix a block of selected rows
                Collections.swap(data, rows[i] - 1, rows[i]);
                fireTableRowsUpdated(rows[i] - 1, rows[i]);
            }

        }

    }

    // for filtered table
    public void moveUp(int[] rows, int[] upRows) {
        for (int i = 0; i < rows.length; i++) { // go up
            if (rows[i] > i) {
                Collections.swap(data, upRows[i], rows[i]);
                fireTableRowsUpdated(upRows[i], rows[i]);
            }
        }
    }

    /**
     * moves all rows one down (direction to max index), but not the last
     * contiguous intervall that 'hits the end', so noncontiguous rows are moved
     * down until they are all stacked together at the end.
     *
     * @param rows needs to be sorted!
     */
    public void moveDown(int[] rows) {

        int k = data.size() - 1;// top row that shouldn't be moved

        for (int i = rows.length - 1; i >= 0; i--) { // go down

            if (rows[i] < k) {// w/o: mix a block of selected rows

                Collections.swap(data, rows[i], rows[i] + 1);
                fireTableRowsUpdated(rows[i], rows[i] + 1);

            }

            k--;

        }

    }

    // for filtered table
    public void moveDown(int[] rows, int[] downRows) {

        int k = data.size();
        for (int i = rows.length - 1; i >= 0; i--) {
            if (downRows.length > i && downRows[i] < k) {
                Collections.swap(data, rows[i], downRows[i]);
                fireTableRowsUpdated(rows[i], downRows[i]);
            }
            k--;
        }
    }

    /**
     * Tests if at least one row maybe moved up/down, which means that there is
     * at least one gap in the row-indices.
     * <p>
     * Examples:
     * <p>
     * <pre>
     *
     *  --------------------------------------------------------------------
     *  | rowsIndices (0 based)   |  count  |   moveTowardZero  |  result  |
     *  --------------------------------------------------------------------
     *  | 1,2,3,4                 |  20     |       true        |   true   |
     *  | 1,2,3,4                 |   5     |       false       |   false  |
     *  | 0,1,2,   [Gap]   15,16  |  17     |       true        |   true   |
     *  | 0,1,2,   [Gap]   15,16  |  17     |       false       |   true   |
     *  | 0,1,2,3,4               |  20     |       true        |   false  |
     *  --------------------------------------------------------------------
     *  &lt;i&gt;count: total rows in model&lt;/i&gt;
     *
     * </pre>
     * <p>
     * <table border="1" cellspacing="0" cellpadding="3">
     * <tr>
     * <th>rowsIndices (0 based)</th>
     * <th>count</th>
     * <th>moveTowardZero</th>
     * <th>result</th>
     * </tr>
     * <tr>
     * <td>1,2,3,4</td>
     * <td>20</td>
     * <td>true</td>
     * <td>true</td>
     * </tr>
     * <tr>
     * <td>1,2,3,4</td>
     * <td>5</td>
     * <td>false</td>
     * <td>false</td>
     * </tr>
     * <tr>
     * <td>0,1,2,&nbsp;&nbsp; <em>[Gap]</em> &nbsp; 15,16</td>
     * <td>17</td>
     * <td>true</td>
     * <td>true</td>
     * </tr>
     * <tr>
     * <td>0,1,2,&nbsp;&nbsp; <em>[Gap]</em> &nbsp; 15,16</td>
     * <td>17</td>
     * <td>false</td>
     * <td>true</td>
     * </tr>
     * <tr>
     * <td>0,1,2,3,4</td>
     * <td>20</td>
     * <td>true</td>
     * <td>false</td>
     * </tr>
     * </table>
     *
     * @param rowsIndices
     * @param moveTowardZero
     * @return
     */

    public boolean canMove(int[] rowsIndices, boolean moveTowardZero) {

        return canMove(rowsIndices, getRowCount(), moveTowardZero);

    }

}