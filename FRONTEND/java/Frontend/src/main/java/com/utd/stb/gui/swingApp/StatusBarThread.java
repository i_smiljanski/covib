package com.utd.stb.gui.swingApp;

import com.jidesoft.status.ProgressStatusBarItem;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.awt.*;

/**
 * @author Inna Smiljanski  created 04.01.11
 */
public class StatusBarThread extends Thread {
    public static final Logger log = LogManager.getRootLogger();
    private int DELAY = 2000;
    private ProgressStatusBarItem statusBarProgress;
    private boolean notEmpty;

    public StatusBarThread(ProgressStatusBarItem statusBarProgress) {
        this.statusBarProgress = statusBarProgress;
    }


    public void run() {
        Runnable runner = new Runnable() {
            public void run() {
                int value = statusBarProgress.getProgressBar().getValue();
                // log.debug("statusBarProgress run " + notEmpty+" value="+value);
                if (notEmpty) {
                    statusBarProgress.setProgress(0);
                    statusBarProgress.setIndeterminate(true);
                } else {
                    statusBarProgress.setProgress(0);
                }
            }
        };

        try {
            //log.debug("statusBarProgress invokeLater ");
            EventQueue.invokeLater(runner);
            sleep(DELAY);
        } catch (InterruptedException ignored) {
            log.error(ignored);
        }
//        catch (InvocationTargetException ignoredException) {
//        }
    }

    @Override
    public void interrupt() {
        // System.out.println("statusBarProgress interrupt ");
        statusBarProgress.getProgressBar().setValue(0);
        statusBarProgress.setIndeterminate(false);
        super.interrupt();
    }

    public ProgressStatusBarItem getStatusBarProgress() {
        return statusBarProgress;
    }

    public void setNotEmpty(boolean notEmpty) {
        this.notEmpty = notEmpty;
    }

    public boolean isNotEmpty() {
        return notEmpty;
    }
}
