package com.utd.stb.gui.swingApp;

import com.utd.stb.back.ui.impl.wizard.StandardWizardErrorException;
import com.utd.stb.gui.swingApp.exceptions.ExceptionDialog;
import com.utd.stb.gui.userinput.swing.ActionDialog;
import com.utd.stb.gui.wizzard.ReportWizardGui;
import com.utd.stb.inter.application.BackendException;
import com.utd.stb.inter.application.GuiPersistance;
import com.utd.stb.inter.dialog.UserDialog;
import com.utd.stb.inter.wizzard.ReportWizard;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import javax.swing.*;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;


public class ReportWizardDlgFactory {

    private static final String PERSIST = "ReportWizzard";
    public static final Logger log = LogManager.getRootLogger();

    private ReportWizardDlgFactory() {
    }

    /**
     * shows a reportwizar in a dialog (modal) returns what to do next returns
     * after the dialog is closed
     *
     * @param parent
     * @param reportWizzard
     * @param guiPersistance
     * @return what to do next
     */
    public static Object executeReportWizardDialog(JFrame parent, ReportWizard reportWizzard,
                                                   GuiPersistance guiPersistance) {

        ReportWizardDialog dlg = null;
        try {
            dlg = new ReportWizardDialog(parent, reportWizzard, guiPersistance);
        } catch (BackendException e) {
            //this error is processed earlier
            log.warn(e);
        }
        if (dlg != null) {
            log.debug(dlg.reportWizard.getCurrentWizardStep());
            if (dlg.reportWizard.getCurrentWizardStep() != null) {
                dlg.setVisible(true);// wait here
                Object o = dlg.getReturnValue();
                log.debug(o);
                return o;
            }
        }
        return null;
    }

    private static class ReportWizardDialog extends ActionDialog {

        private Gui gui;

        private final GuiPersistance guiPersistance;

        private SplitWindowState dlgSizes = new SplitWindowState();
        private ReportWizard reportWizard;

        public ReportWizardDialog(JFrame parent, ReportWizard reportWizard,
                                  final GuiPersistance guiPersistance) throws BackendException {

            super(parent);
            setTitle(reportWizard.getTitle());
            setResizable(true);
            setModal(true);
            setDefaultCloseOperation(JDialog.DO_NOTHING_ON_CLOSE);

            // setup data
            this.reportWizard = reportWizard;
            this.guiPersistance = guiPersistance;
            gui = new Gui(reportWizard, parent);
            JPanel panel = gui.getContentPanel(this);
            getContentPane().add(panel);
            TimeoutManager.addSizeMoveListener(this);

            // ... guiPersistance ............................
            dlgSizes = PersistTool.read(guiPersistance, PERSIST);
            dlgSizes.getWindowState().putWindowStateListener(this);
            addWindowListener(new OpenCloseWndListener());

            // ... restore state
            gui.setDividerLocation(dlgSizes.getDividerPos());
            dlgSizes.getWindowState().restore(this);
        }// --------------------------------------------------------

        protected Object doAction(Object action) throws BackendException {

            return reportWizard.doAction(action);
        }

        /**
         * implement some helper. use private inner class to hide from outside
         */
        private class Gui extends ReportWizardGui {

            JFrame parent;

            Gui(ReportWizard reportWizard, JFrame parent) {

                super(reportWizard);
                this.parent = parent;
            }

            protected void savePersistance() {

                dlgSizes.getWindowState().save(ReportWizardDialog.this);
                dlgSizes.setDividerPos(gui.getDividerLocation());
                PersistTool.store(guiPersistance, dlgSizes, PERSIST);
            }

            protected void userWantAbortWizard() {

                // MCD 29.Nov 2005, save the persistance before exit
                savePersistance();
                executeActionLoop(ReportWizard.NAVIGATE_ABORT);

            }

            protected void userWantFinishWizard() {

                // MCD 29.Nov 2005, save the persistance before exit
                savePersistance();
                executeActionLoop(ReportWizard.NAVIGATE_FINISH);

            }


            protected void abortWizardOnError() {
                savePersistance();
                executeActionLoop(ReportWizard.NAVIGATE_EXIT);
            }


            protected boolean exceptionHandler(Throwable e, boolean clientCanContinue) {

                return ReportWizardDialog.this.exceptionHandler(e, clientCanContinue);
            }

            /* (non-Javadoc)
             * @see com.utd.stb.gui.wizzard.ReportWizardGui#storeUsersRegExpr(java.lang.String)
             */
            protected void storeUsersRegExpr(String regExpr, String entity) {

                if (entity != null && entity.length() != 0
                        && regExpr != null && regExpr.length() != 0) {
                    PersistTool.store(guiPersistance, regExpr, entity);
                }
            }

            /* (non-Javadoc)
             * @see com.utd.stb.gui.wizzard.ReportWizardGui#loadUsersRegExpr(java.lang.String)
             */
            protected Object loadUsersRegExpr(String entity) {
                return PersistTool.load(guiPersistance, entity);
            }

            protected void showWizardError(StandardWizardErrorException swe) {
                UserDialog dialog = ExceptionDialog.createWarningMsgBox(null, swe);
                UserDialogTool.executeUserDialog(parent, dialog);

            }
        }

        /**
         * size/pos + abort question
         */
        private final class OpenCloseWndListener extends WindowAdapter {

            public void windowClosing(WindowEvent e) {

                gui.userWantAbortWizard();
            }

            // usefull if parent frame is minimized on reportaction:
            public void windowOpened(WindowEvent e) {

                e.getWindow().toFront();
            }
        }

    }

}