package com.utd.gui.controls.table;

import com.jidesoft.grid.CellRendererManager;

import javax.swing.*;
import javax.swing.table.TableModel;
import java.awt.*;


/**
 * JTable extension behaving more like a windows ListView than JTable,
 * <p>
 * is no focuscycleroot, so doesn't handle Tab/Shift-Tab,
 * <p>
 * sets a special renderer, so pay attention on reuse.
 *
 * @author PeterBuettner.de
 */

public class ListViewTable extends BaseTable {

    public ListViewTable(TableModel dm) {

        super(dm);
        ListViewTools.makeListViewSized(this);

        setDefaultRenderer(Object.class, new ListViewTableCellRenderer());
        CellRendererManager.registerRenderer(Object.class, new ListViewTableCellRenderer());
        // remove 'Tab' + 'Shift Tab' to stop internal navigation,
        // So Table works as List, with processKeyBinding() Enter is gone too
        setFocusTraversalKeys(KeyboardFocusManager.FORWARD_TRAVERSAL_KEYS, null);
        setFocusTraversalKeys(KeyboardFocusManager.BACKWARD_TRAVERSAL_KEYS, null);
        // maybe (isManagingFocus() return false may work: no JTable sets them

        setUpKeyBindings();

    }// --------------------------------------------------------------------------------


    private void setUpKeyBindings() {

        setUpKeyBinding("HOME", "selectFirstRow");
        setUpKeyBinding("END", "selectLastRow");
        setUpKeyBinding("shift HOME", "selectFirstRowExtendSelection");
        setUpKeyBinding("shift END", "selectLastRowExtendSelection");
    }

    private void setUpKeyBinding(String keyStroke, String actionName) {

        getInputMap().put(KeyStroke.getKeyStroke(keyStroke), actionName);
    }

}