package com.utd.stb.inter.application;

import com.utd.stb.gui.swingApp.App;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import javax.swing.table.AbstractTableModel;

/**
 * @author Inna Smiljanski  created 16.11.2010
 */
public class JobsTableInfo {
    public static final Logger log = LogManager.getRootLogger();
    private AbstractTableModel model;
    private boolean runLoader;
    private boolean isDisplayed;
    private App app;


    public AbstractTableModel getModel() {
//        System.out.println("getModel:model "+model);
        return model;
    }

    public void setModel(AbstractTableModel model) {
//        System.out.println("setModel:model "+model);
        this.model = model;
    }

    public boolean isRunLoader() {
        return runLoader;
    }

    public void setRunLoader(boolean runLoader) {
        this.runLoader = runLoader;
    }


    public boolean isDisplayed() {
        return isDisplayed;
    }

    public void show() {
        this.isDisplayed = true;
    }


    public void hide() {
        this.isDisplayed = false;
    }

    public App getApp() {
        return app;
    }

    public void setApp(App app) {
        this.app = app;
    }
}
