package com.utd.stb.inter.items;

/**
 * Container for <code>DisplayableItem </code>
 *
 * @author PeterBuettner.de
 */

public interface DisplayableItems {

    /**
     * Number of items
     *
     * @return
     */
    int getSize();

    /**
     * Get Item at index
     *
     * @param index
     * @return
     */
    DisplayableItem get(int index);

    /**
     * Get the index of the item, -1 if not in list
     *
     * @param item
     * @return
     */
    int indexOf(DisplayableItem item);

    /**
     * Get all colNames (separated by ,) from all selection record's attributes list
     *
     * @return
     */
    String getAttributeListColNames();

    /**
     * Get the number of cols (separated by ,) from all selection record's attributes list
     *
     * @return
     */
    int getAttributeListColCount();

}