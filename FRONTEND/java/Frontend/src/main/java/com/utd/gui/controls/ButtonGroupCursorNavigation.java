package com.utd.gui.controls;

import javax.swing.*;
import java.awt.event.ActionEvent;
import java.util.ArrayList;
import java.util.List;


/**
 * Navigate with cursor left/right between JButtons. In Windows it is common to
 * navigate in a group of buttons [in MessageBoxes] (maybe other contriols too)
 * with VK_LEFT and VK_RIGHT,VK_UP,VK_DOWN here we help support this
 *
 * @author PeterBuettner.de
 */
public class ButtonGroupCursorNavigation {

    private List list = new ArrayList();

    static private KeyStroke[] ksForeward = new KeyStroke[]{
            KeyStroke.getKeyStroke("RIGHT"), KeyStroke.getKeyStroke("DOWN")};

    static private KeyStroke[] ksBackward = new KeyStroke[]{KeyStroke.getKeyStroke("LEFT"),
            KeyStroke.getKeyStroke("UP")};

    private Action acForeward = new MoveAction("Next Button", true);

    private Action acBackward = new MoveAction("Previous Button", false);

    /**
     * the component is added to the group, we put our actions and keybindings
     * into its action/input Map, note that this may change it's behaviour
     *
     * @param component
     */
    public void add(JComponent component) {

        list.add(component);

        // ... addBindings ...........................
        // we don't use ActionUtil 'cause we want 2 bindings per action, also it
        // is
        // easier to remove
        putIntoMaps(component, ksForeward, acForeward);
        putIntoMaps(component, ksBackward, acBackward);
    }

    private void putIntoMaps(JComponent compo, KeyStroke[] ks, Action action) {

        InputMap imap = compo.getInputMap(JComponent.WHEN_FOCUSED);
        for (int i = 0; i < ks.length; i++)
            imap.put(ks[i], action);
        compo.getActionMap().put(action, action);
    }

    /**
     * the component are added to the group, we put our actions and keybindings
     * into their action/input Maps, note that this may change it's behaviour
     *
     * @param components
     */
    public void add(JComponent[] components) {

        for (int i = 0; i < components.length; i++)
            add(components[i]);
    }

    //	/**
    //	 * the component is removed from the group, we remove our actions and
    //	 * keybindings from its action/input Maps, note that the original bindigs
    // of
    //	 * the KeyStrokes are not restored
    //	 *
    //	 * @param component
    //	 */
    //public void remove(JComponent component){
    //
    //}

    /**
     * find the component that should be focussed next, null if not found
     *
     * @param compo   a component, you may use ActionEvent#getSource(), if that is
     *                no component it is probably not found
     * @param forward
     * @return
     */
    private JComponent findNextComponent(Object compo, boolean forward) {

        int start = list.indexOf(compo);
        if (start == -1) return null;

        int size = list.size();
        int dir = forward ? +1 : -1;
        for (int i = 1; i < size; i++) {
            JComponent n = (JComponent) list.get((start + i * dir + size) % size); // +size
            // 'cause
            // otherwise
            // the
            // index
            // may
            // be
            // <0
            if (n.isFocusable() && n.isEnabled() && n.isShowing()) return n;
        }
        return null;
    }

    private final class MoveAction extends AbstractAction {

        private boolean forward;

        MoveAction(String name, boolean forward) {

            super(name);
            this.forward = forward;
        }

        public void actionPerformed(ActionEvent e) {

            JComponent c = findNextComponent(e.getSource(), forward);
            if (c != null) c.requestFocusInWindow();

        }
    }

}

