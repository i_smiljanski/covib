package com.utd.stb.interlib;

import com.utd.stb.inter.action.ComponentAction;


/**
 * @author PeterBuettner.de
 */
public class ComponentActionImpl implements ComponentAction {

    private Object type;

    public ComponentActionImpl(Object type) {

        this.type = type;
    }

    public Object getType() {

        return type;
    }

    public String toString() {

        return getClass().getName() + "; Type:" + type;
    }
}