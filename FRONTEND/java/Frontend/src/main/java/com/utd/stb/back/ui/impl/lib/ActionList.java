package com.utd.stb.back.ui.impl.lib;

import java.util.ArrayList;
import java.util.Iterator;


/**
 * Intended to store a lost of Objects that are capable to be used in the action
 * queue of TreeBase
 *
 * @author rainer bruns Created 14.12.2004
 * @see TreeBase#getNextTask()
 * @see Dispatcher#checkReload
 */
public class ActionList {

    ArrayList actions;
    Iterator iterator = null;

    /**
     * Initialises the action storage list empty
     */
    public ActionList() {

        actions = new ArrayList();
    }

    /**
     * @param o an array of actions(probably)
     */
    public ActionList(Object[] o) {

        int length = o.length;
        actions = new ArrayList(length);
        for (int i = 0; i < length; i++) {
            actions.add(o[i]);
        }
        iterator = actions.iterator();
    }

    /**
     * @param o object to add to list
     */
    public void add(Object o) {

        actions.add(o);
    }

    /**
     * Sets iterator to first record
     */
    private void initIterator() {

        iterator = actions.iterator();
    }

    /**
     * @return true if there are more actions
     */
    public boolean hasMoreActions() {

        if (iterator == null) {
            iterator = actions.iterator();
        }
        return iterator.hasNext();
    }

    /**
     * @return size of the list
     */
    public int getSize() {

        return actions.size();
    }

    /**
     * @return the next action in list, or null if no more action
     */
    public Object nextAction() {

        if (hasMoreActions()) {
            return iterator.next();
        } else
            return null;
    }
}