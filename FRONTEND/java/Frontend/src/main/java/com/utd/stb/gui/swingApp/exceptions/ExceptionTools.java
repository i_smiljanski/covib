package com.utd.stb.gui.swingApp.exceptions;

import com.utd.stb.inter.application.BackendException;

import javax.swing.*;
import java.awt.*;


/**
 * @author PeterBuettner.de
 */
public class ExceptionTools {

    /**
     * Find the first Component in the hierarchie that implements
     * ExceptionHandler, also client may returned if it implements
     * ExceptionHandler.
     *
     * @param client the component where we start to search for a handler
     * @return
     */
    public static ExceptionHandler findExceptionHandler(Component client) {

        if (client instanceof ExceptionHandler) return (ExceptionHandler) client;
        return (ExceptionHandler) SwingUtilities.getAncestorOfClass(ExceptionHandler.class,
                client);
    }

    /**
     * Tries to find an ExceptionHandler, if found calls it. see
     * {@link ExceptionHandler.exceptionHandler(Throwable, boolean )}
     * <p>
     * <p>
     * If none is found (or client==null) we show an exception dialog and may
     * exit the application (depends on clientCanContinue and
     * BackendException#isInformational() ) with <b>System.exit() </b> without
     * further cleanup.
     *
     * @param client            see {@link #findExceptionHandler()}, if it is null (maybe a
     *                          child component that has an exception in its constructor: it
     *                          is not part of the component hierarchie) we may close the
     *                          whole application since we can not inform any parent: see
     *                          description of this method
     * @param throwable
     * @param clientCanContinue
     * @return see
     * {@link ExceptionHandler.exceptionHandler(Throwable, boolean )}
     */
    public static boolean callExceptionHandler(Component client, Throwable throwable,
                                               boolean clientCanContinue) {
        ExceptionHandler handler = findExceptionHandler(client);

        if (handler != null) {
            return handler.exceptionHandler(throwable, clientCanContinue);
        }

        ExceptionDialog.showExceptionDialog(client, throwable, null);

        if (clientCanContinue && (throwable instanceof BackendException)) {
            // maybe not too bad
            if (!((BackendException) throwable).isAppExit()) return true;
        }
//        System.exit(1);
        return false;

    }

}