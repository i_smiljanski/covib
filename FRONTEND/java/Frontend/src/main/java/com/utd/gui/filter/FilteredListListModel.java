package com.utd.gui.filter;

import javax.swing.*;
import javax.swing.event.ChangeEvent;
import javax.swing.event.ChangeListener;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;


/**
 * Has a List as basedata (unchangable), delivers a javax.swing.ListModel from
 * this data which is filtered, so the ListModel-data changes on filterchange.
 *
 * @author PeterBuettner.de
 */
public class FilteredListListModel extends AbstractListModel {

    private List baseData;
    private Filter filter;
    // remark: sometimes we set filteredData = baseData, so never do
    // filteredData.clear() or check before
    private List filteredData = Collections.EMPTY_LIST;

    private ChangeListener filterChanged = new ChangeListener() {

        public void stateChanged(ChangeEvent e) {

            updateData();
        }
    };

    /**
     * we don't see changes in the data, since we copy: but see that if you
     * change the data the list points to we see those changes
     *
     * @param baseData
     */
    public FilteredListListModel(List baseData, Filter filter) {

        this.baseData = new ArrayList(baseData);
        this.filter = filter;
        this.filter.addChangeListener(filterChanged);
        updateData();

    }

    public int getSize() {

        return filteredData.size();

    }

    public Object getElementAt(int index) {

        return filteredData.get(index);

    }

    private void updateData() {

        // remark: sometimes we set filteredData = baseData, so never do
        // filteredData.clear() or check before

        int oldSize = filteredData.size();

        filteredData = new ArrayList();
        if (oldSize > 0) fireIntervalRemoved(this, 0, oldSize - 1);

        if (filter != null) {

            for (int i = 0, max = baseData.size(); i < max; i++) {

                Object o = baseData.get(i);

                if (filter.matches(o)) filteredData.add(o);

            }

        } else {// empty filter, show all

            filteredData = baseData;

        }

        fireIntervalAdded(this, 0, filteredData.size());

    }

}