package com.utd.stb.gui.wizzard.wizzardStep;

import com.utd.stb.inter.wizzard.FormDisplayInfo;
import com.utd.stb.inter.wizzard.WizardStep;
import com.utd.stb.inter.wizzard.WizardSteps;

import javax.swing.*;
import java.util.AbstractList;
import java.util.ArrayList;
import java.util.List;


/**
 * Some tools to ease life with WizardStep/s
 *
 * @author PeterBuettner.de
 */
public class WizardStepTools {

    /**
     * Usefull for CellRenderers, if value is of type WizardStep text, tooltip
     * (later maybe icon) of the jLabel ist set from value's
     * FormDisplayInfo#getNavigationList... if not tooltip becomes null and text =
     * value.toString()
     *
     * @param jLabel
     * @param value
     * @return true if it was WizardStep, els false
     */
    public static boolean configLabelFromWizardStep(JLabel jLabel, Object value) {

        if (!(value instanceof WizardStep)) {
            jLabel.setText((value == null) ? "" : value.toString());
            jLabel.setToolTipText(null);
            return false;
        }

        FormDisplayInfo info = ((WizardStep) value).getFormDisplayInfo();

        jLabel.setText(info.getNavigationListText());
        jLabel.setToolTipText("<html><body style='padding:2px;'>" + "<b>"
                + info.getNavigationListText() + "</b><br>"
                + info.getNavigationListInfo());
        return true;
    }

    /**
     * copies into a List
     *
     * @param wizardSteps
     * @return
     */
    public static List<WizardStep> wizardStepsToList(WizardSteps wizardSteps) {
        ArrayList<WizardStep> wizardStepList = new ArrayList<>(wizardStepsAsList(wizardSteps));
        return wizardStepList;
    }

    /**
     * copies into a Array
     *
     * @param wizardSteps
     * @return
     */
    public static WizardStep[] wizardStepsToArray(WizardSteps wizardSteps) {
        ArrayList<WizardStep> l = new ArrayList<>(wizardStepsAsList(wizardSteps));
        return l.toArray(new WizardStep[0]);

    }

    /**
     * doesn't copy, just wraps the data
     *
     * @param wizardSteps
     * @return
     */
    public static AbstractList<WizardStep> wizardStepsAsList(final WizardSteps wizardSteps) {

        return new AbstractList<WizardStep>() {

            public WizardStep get(int index) {

                return wizardSteps.get(index);
            }

            public int size() {

                return wizardSteps.getSize();
            }
        };
    }

}