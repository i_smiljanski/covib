package com.utd.gui.event;

import javax.swing.*;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;


/**
 * easy adapter that sees only double clicks (4 clicks are 2 double clicks), it
 * filters 'row not found', so rowDblClick() is always usable
 *
 * @author PeterBuettner.de
 */
public abstract class TableDblClickAdapter extends MouseAdapter {

    public void mouseClicked(MouseEvent e) {

        if (!EventTools.isLeftMouseEvenClick(e)) return;

        if (!(e.getSource() instanceof JTable)) return;

        JTable table = (JTable) e.getSource();
        int row = table.rowAtPoint(e.getPoint());
        if (row == -1) return;
        rowDblClick(table, row, e);

    }

    /**
     * fired only if all is valid, row is never -1.
     *
     * @param table
     * @param row
     * @param e     let one see extra data
     */
    abstract public void rowDblClick(JTable table, int row, MouseEvent e);

}