package com.utd.gui.action;

import com.utd.gui.util.DialogUtils;
import resources.Res;

import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.util.Map;


/**
 * Put Actions into maps, invoke actions by code, configure Label from a Map
 * (e.g. read from a resoure file) like an action
 * <p>
 *
 * @author PeterBuettner.de
 */
public class ActionUtil {

    /**
     * See {@link putIntoActionMap(JComponent, Action, int)}it is called with
     * condition== JComponent.WHEN_ANCESTOR_OF_FOCUSED_COMPONENT
     *
     * @param compo
     * @param action
     */
    public static void putIntoActionMap(JComponent compo, Action action) {

        putIntoActionMap(compo, action, JComponent.WHEN_ANCESTOR_OF_FOCUSED_COMPONENT);
    }

    /**
     * puts the action into the action map of the component and their shortcut
     * into the input map (WHEN_ANCESTOR_OF_FOCUSED_COMPONENT), but if the
     * associated KeyStroke is null then nothing is done.
     *
     * @param compo     the target component
     * @param action
     * @param condition choose inputMap, see JComponent.WHEN_...
     */
    public static void putIntoActionMap(JComponent compo, Action action, int condition) {

        KeyStroke ks = (KeyStroke) action.getValue(Action.ACCELERATOR_KEY);
        if (ks == null) return;

        InputMap imap = compo.getInputMap(condition);
        imap.put(ks, action);
        ActionMap amap = compo.getActionMap();
        amap.put(action, action);
    }

    /**
     * See {@link putIntoActionMap(JComponent, Action [], int)}it is called
     * with condition== JComponent.WHEN_ANCESTOR_OF_FOCUSED_COMPONENT
     *
     * @param compo
     * @param actions
     */
    public static void putIntoActionMap(JComponent compo, Action[] actions) {

        putIntoActionMap(compo, actions, JComponent.WHEN_ANCESTOR_OF_FOCUSED_COMPONENT);
    }

    /**
     * Puts all actions 'into' the compo, see
     * {@link putIntoActionMap(JComponent, Action )}for Details.
     *
     * @param compo
     * @param actions
     * @param condition choose inputMap, see JComponent.WHEN_...
     */
    public static void putIntoActionMap(JComponent compo, Action[] actions, int condition) {

        for (int i = 0; i < actions.length; i++)
            if (actions[i] != null) putIntoActionMap(compo, actions[i], condition);
    }

    /**
     * invokes an action from the components actionmap. The action is only
     * invoked if it is enabled. returns true if action is found, else false,
     * independent of the enabled state. Sender is the component.
     *
     * @param component
     * @param actionKey most times probably a string
     * @return
     */
    public static boolean invokeAction(JComponent component, Object actionKey) {

        ActionMap actionMap = component.getActionMap();
        if (actionMap == null) return false;

        Action action = actionMap.get(actionKey);
        if (action == null) return false;

        invokeAction(component, action);

        return true;
    }

    /**
     * invokes the action if it is enabled, sender is the component
     *
     * @param component
     * @param action
     */
    private static void invokeAction(JComponent component, Action action) {

        if (!action.isEnabled()) return;
        Object cmd = action.getValue(Action.ACTION_COMMAND_KEY);
        if (cmd != null) cmd = cmd.toString();
        action.actionPerformed(new ActionEvent(component, ActionEvent.ACTION_PERFORMED,
                (String) cmd));
    }

    /**
     * search in all ancestors for the keyStroke in the inputmap(condition:
     * WHEN_ANCESTOR_OF_FOCUSED_COMPONENT) the corresponding action in actionmap
     * and invoke it (if enabled), return after first found action (also if
     * disabled)
     *
     * @param component this ones inputMap is not searched, starting at this' parent
     * @param keyStroke
     * @return true if a action was found, (doesn't matter if en- oder disabled)
     */
    public static boolean invokeActionOfKeyStrokeInAncestor(Component component,
                                                            KeyStroke keyStroke) {

        Component p = component;
        while (p != null) {
            p = p.getParent();
            if (p instanceof Window) return false;// don't go into
            // Frame/Dialog/Window since
            // this may return
            // owners(other dialogs...)
            if (!(p instanceof JComponent)) continue;

            JComponent jc = (JComponent) p;
            Action action = getActionForKeystroke(
                    jc,
                    JComponent.WHEN_ANCESTOR_OF_FOCUSED_COMPONENT,
                    keyStroke);
            if (action == null) continue;

            invokeAction(jc, action);
            return true;
        }
        return false;
    }

    /**
     * Tries to find the action for a keyStroke in a component. The inputmap for
     * inputmapCondition is used, if anything isn't found null is returned
     *
     * @param component
     * @param inputmapCondition
     * @param keyStroke
     * @return null if anything isn't found (inputmap, keyStroke, actionmap,
     * action)
     */
    private static Action getActionForKeystroke(JComponent component, int inputmapCondition,
                                                KeyStroke keyStroke) {

        InputMap im = component.getInputMap(inputmapCondition);
        if (im == null) return null;
        Object binding = im.get(keyStroke);
        if (binding == null) return null;

        ActionMap am = component.getActionMap();
        if (am == null) return null;
        return am.get(binding);
    }

    /**
     * configures a action from some data, data that is null will be ignored
     *
     * @param a        the action
     * @param icon     String ('filename' for now) or Icon
     * @param mnemonic use ' ' (space) for empty
     * @param tooltip
     * @param accel    text representation like "F1", "ENTER", "ctrl G", see
     *                 {@link KeyStroke.getKeyStroke(String)}
     */
    public static void initAction(Action a, Object icon, char mnemonic, String tooltip,
                                  String accel) {

        if (mnemonic != ' ') a.putValue(Action.MNEMONIC_KEY, (int) mnemonic);

        if (icon instanceof String)
            a.putValue(Action.SMALL_ICON, Res.getII((String) icon));
        else if (icon instanceof Icon)
            a.putValue(Action.SMALL_ICON, icon);

        KeyStroke ks = KeyStroke.getKeyStroke(accel);// null result in null
        if (ks != null) a.putValue(Action.ACCELERATOR_KEY, ks);

        // tooltip: if null then only ksText, if this is null too then nothing
        String ksText = DialogUtils.getColoredKeystrokeHtmlText(ks);
        if (tooltip != null && tooltip.length() > 0) {
            if (ksText != null) tooltip += "<br>" + ksText;
        } else
            tooltip = ksText; // if null stay empty
        if (tooltip != null)
            a.putValue(Action.SHORT_DESCRIPTION,
                    ("<html><body style='padding:2px;'>" + tooltip));
    }

    /**
     * set properties of a action from values in a map Keys are the ones in
     * Action, for now only name, mnemonic, accelerator and tooltip are
     * supported
     *
     * @param a    the Action
     * @param map  of Strings (is checked, so don't worry)
     * @param icon String or Icon
     */
    public static void initActionFromMap(Action a, Map map, Object icon) {
        a.putValue(Action.NAME, map.get(Action.NAME));
        Object ms = map.get(Action.MNEMONIC_KEY);
        ms = (ms instanceof String) ? ms + " " : " ";// ==> len always >=1!
        char mc = ((String) ms).charAt(0);

        Object tt = map.get(Action.SHORT_DESCRIPTION);
        if (!(tt instanceof String)) tt = null;

        Object ac = map.get(Action.ACCELERATOR_KEY);

        initAction(a, icon, mc, (String) tt, (String) ac);
    }

    /**
     * configures a Label from some data like action, uses: name mnemonic and
     * tooltip
     *
     * @param lbl
     * @param map
     */
    public static void initLabelFromMap(JLabel lbl, Map map) {

        lbl.setText((String) map.get(Action.NAME));

        Object ms = map.get(Action.MNEMONIC_KEY);
        ms = (ms instanceof String) ? ms + "\0" : "\0";// ==> len always >=1!
        char mc = ((String) ms).charAt(0);
        lbl.setDisplayedMnemonic(mc);

        Object tt = map.get(Action.SHORT_DESCRIPTION);
        if (!(tt instanceof String)) tt = null;
        lbl.setToolTipText((String) tt);
    }

}