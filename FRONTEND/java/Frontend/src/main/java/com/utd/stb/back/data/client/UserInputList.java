package com.utd.stb.back.data.client;

import com.utd.stb.inter.items.DisplayableItem;
import com.utd.stb.inter.items.DisplayableItems;

import java.util.ArrayList;
import java.util.Iterator;


/**
 * Wraps a list of record data of type UserInputObject
 *
 * @author rainer bruns Created 10.10.2004
 * @see UserInputRecord
 */
public class UserInputList implements DisplayableItems {

    private ArrayList data = new ArrayList();
    private Iterator iterator;

    /**
     * Creates an empty UserInputList
     */
    public UserInputList() {

    }

    /**
     * adds all records of the list to this list
     *
     * @param in the list to add
     */
    public void addAll(UserInputList in) {

        in.initIterator();
        while (in.hasNext()) {
            data.add(in.getNext());
        }
    }

    /**
     * adds this record to the list
     *
     * @param sr the record to add
     */
    public void add(UserInputRecord sr) {

        data.add(sr);
    }

    /*
     * (non-Javadoc)
     * 
     * @see com.utd.stb.inter.items.DisplayableItems#get(int)
     */
    public DisplayableItem get(int index) {

        return (UserInputRecord) data.get(index);
    }

    /**
     * gets the record at index
     *
     * @param index
     * @param sr
     */
    public void set(int index, UserInputRecord sr) {

        data.set(index, sr);
    }

    /**
     * sets list iterator to the beginning
     */
    public void initIterator() {

        iterator = data.iterator();
    }

    /**
     * @return true if there is a next record
     */
    public boolean hasNext() {

        return iterator.hasNext();
    }

    /**
     * @return the next EntityRecord
     */
    public UserInputRecord getNext() {

        return (UserInputRecord) iterator.next();
    }

    /*
     * (non-Javadoc)
     * 
     * @see com.utd.stb.inter.items.DisplayableItems#getSize()
     */
    public int getSize() {

        return data.size();
    }

    /*
     * (non-Javadoc)
     * 
     * @see com.utd.stb.inter.items.DisplayableItems#indexOf(com.utd.stb.inter.items.DisplayableItem)
     */
    public int indexOf(DisplayableItem s) {

        return data.indexOf(s);
    }

    /* (non-Javadoc)
     * @see com.utd.stb.inter.items.DisplayableItems#getAttributeListColNames()
     */
    public String getAttributeListColNames() {

        return null;
    }

    /* (non-Javadoc)
     * @see com.utd.stb.inter.items.DisplayableItems#getAttributeListColCount()
     */
    public int getAttributeListColCount() {

        return 0;
    }

}