package com.utd.stb.interlib;

import com.utd.stb.inter.action.ButtonItem;

import java.util.Arrays;
import java.util.HashSet;
import java.util.Set;


/**
 * simple implementation to have OK, CANCEL, YES, NO, SAVE, CLOSE Buttons, <b>
 * but not OTHER </b> Buttons
 *
 * @author PeterBuettner.de
 */
public class StandardButton implements ButtonItem {

    private Object type;
    private boolean needsValidData;

    private static Set validTypes = new HashSet(Arrays.asList(new Object[]{OK, CANCEL, YES,
            NO, SAVE, SAVE_AND_CLOSE, CLOSE, NEW, DELETE}));

    /**
     * With check if type is valid!
     *
     * @param type           see types in {@link ButtonItem}: OK, CANCEL, YES, NO, SAVE,
     *                       CLOSE <b>but not OTHER </b>
     * @param needsValidData in a dialog the button will be disabled if the UserInputs are
     *                       not all valid
     * @throws IllegalArgumentException if type is not in OK, CANCEL, YES, NO, SAVE, CLOSE
     */
    public StandardButton(Object type, boolean needsValidData)
            throws IllegalArgumentException {

        if (!(validTypes.contains(type)))
            throw new IllegalArgumentException(
                    "Type: '"
                            + type
                            + "' is not in valid range: "
                            + validTypes);

        this.type = type;
        this.needsValidData = needsValidData;
    }

    public Object getType() {

        return type;
    }

    public String getText() {

        return null;
    }

    public boolean isNeedsValidData() {

        return needsValidData;
    }

    public String toString() {

        return getClass().getName() + "; Type:" + type + "; needsValidData:" + needsValidData;
    }

    /**
     * is the buttonItem one of the 'Standard' ones: OK, CANCEL, YES, NO, SAVE,
     * CLOSE; <b>but not OTHER
     *
     * @param buttonItem
     * @return
     */
    public static boolean isStandardButton(ButtonItem buttonItem) {

        return validTypes.contains(buttonItem.getType());
    }
}