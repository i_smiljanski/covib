package com.utd.stb.gui.userinput.swing;

import javax.swing.*;
import java.awt.*;
import java.awt.event.KeyEvent;
import java.text.DateFormat;
import java.util.Date;


class DateField extends BasicFormattedField {

    private Icon icon;

    public DateField(DateFormat format, Date initialDate, Icon icon) {

        super(format, initialDate);
        this.icon = icon;
    }

    public DateField(DateFormat format, Date initialDate) {

        super(format, initialDate);
    }

    protected boolean processKeyBinding(KeyStroke ks, KeyEvent e, int condition,
                                        boolean pressed) {

        /*
         * scenario: Field with content: '22:30aaa', cursor after last 'a' or
         * near other 'a'. User likes to scroll with up/down arrows: Exception
         * thrown in International Formatter. Solution: catch and beep
         */
        try {
            return super.processKeyBinding(ks, e, condition, pressed);
        } catch (IllegalArgumentException iae) {
            UIManager.getLookAndFeel().provideErrorFeedback(this);
            return true;
        }
    }

    protected void paintComponent(Graphics g) {

        super.paintComponent(g);
        if (icon == null || !isEditable()) return;

        Insets insets = super.getInsets();
        int yi = (getHeight() - icon.getIconHeight()) / 2;
        icon.paintIcon(this, g, insets.left, yi);
    }

    public Insets getInsets() {

        Insets insets = super.getInsets();
        if (icon == null || !isEditable()) return insets;
        insets.left += icon.getIconWidth() + 2;
        return insets;
    }

}