package com.utd.stb.inter.userinput;

import java.time.ZonedDateTime;


/**
 * Contains (wraps) Date, but restricts to date/datetime/time/...
 * <p>
 * <p>
 * <h2>Formats</h2>
 * <p>
 * Cut from java.text.SimpleDateFormat, we use only some <b>ids </b>, since we
 * don't format but define valid entries.
 * </p>
 * <p>
 * <p>
 * The following id letters are defined (all other characters from 'A' to 'Z'
 * and from 'a' to 'z' are reserved):
 * </p>
 * <table border="1" cellspacing="0" cellpadding="3" >
 * <p>
 * <tbody>
 * <tr bgcolor="#cccccc">
 * <th>Letter</th>
 * <th>Component</th>
 * <th>Examples</th>
 * </tr>
 * <tr>
 * <td>y</td>
 * <td>Year</td>
 * <td>1996</td>
 * </tr>
 * <tr>
 * <td>M</td>
 * <td>Month in year</td>
 * <td>7</td>
 * </tr>
 * <tr>
 * <td>d</td>
 * <td>Day in month</td>
 * <td>10</td>
 * </tr>
 * <tr>
 * <td>H</td>
 * <td>Hour in day</td>
 * <td>0</td>
 * </tr>
 * <tr>
 * <td>m</td>
 * <td>Minute in hour</td>
 * <td>30</td>
 * </tr>
 * <tr>
 * <td>s</td>
 * <td>Second in minute</td>
 * <td>55</td>
 * </tr>
 * <tr>
 * <td>S</td>
 * <td>Millisecond</td>
 * <td>978</td>
 * </tr>
 * </tbody> </table>
 * <p>
 * <p>
 * Use only one letter, sequence is meaningless : 'yMd' for Date only. <br>
 * Maybe later we use shemes like 's-M' to have all from 'MdHms''.
 * </p>
 * <p>
 * For now use only the predefined ones DATE, TIME, DATETIME, where we put an
 * prefix 'P:' before the ids.
 * </p>
 *
 * @author PeterBuettner.de
 */
public interface DateInput {

    /** Later: a complex type, maybe "M,d,h,m", */
    //	public static final String TYPE_COMPLEX="";
    /**
     * Y + M + d, e.g.: 21.07.2005
     */
    public static final String TYPE_DATE = "P:DATE";

    /**
     * h + m, e.g.: 15:39
     */
    public static final String TYPE_TIME = "P:TIME";

    /**
     * Y + M + d + h + m, e.g.: 21.07.2005 15:39
     */
    public static final String TYPE_DATETIME = "P:DATETIME";

    /**
     * TODO be strict or throw an error the implementor should strip any
     * (erroneous) info that doesn't fit into the type from {@link getType()},
     * a call to {@link getDate()}returns the stripped data.
     *
     * @param date
     */

    void setDate(ZonedDateTime date);

    /**
     * TODO be strict or throw an error the implementor should strip any
     * (erroneous) info that doesn't fit into the type from {@link getType()}.
     *
     * @return
     */
    ZonedDateTime getDate();

    /**
     * returns one of:
     * <p>
     * <pre>
     *
     *  DateProperty.TYPE_DATE
     *  DateProperty.TYPE_TIME
     *  DateProperty.TYPE_DATETIME
     *  or a 'free' one, see interface documentation
     *
     *
     * </pre>
     *
     * @return
     */
    String getType();

    String getFormat();

}