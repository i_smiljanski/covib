package com.utd.stb.gui.userinput.swing;

import java.util.EventObject;


/**
 * Used by UIODataHandler for informing someone about changes in the associated
 * JComponents, see {@link DataChangeListener}, source is a
 * {@link UIODataHandlerBase}
 *
 * @author PeterBuettner.de
 */
public class DataChangeEvent extends EventObject {

    /**
     * the source is a {@link UIODataHandlerBase}
     *
     * @param source
     */
    public DataChangeEvent(UIODataHandlerBase source) {

        super(source);
    }

    protected UIODataHandlerBase getUIODataHandler() {

        return (UIODataHandlerBase) source;
    }
}