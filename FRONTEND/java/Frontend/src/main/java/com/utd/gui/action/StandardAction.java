package com.utd.gui.action;

import com.utd.gui.resources.I18N;

import javax.swing.*;
import java.util.Arrays;
import java.util.HashSet;
import java.util.Set;


/**
 * Not used for now, but introduce later: better mapping to other code
 * (ButtonItem in com.utd.stb.inter...)
 * <p>
 * <p>
 * Have predefined Actions for OK, CANCEL, YES, NO, SAVE, CLOSE with no
 * SHORT_DESCRIPTION. The resource name that is used is:
 * 'com.utd.gui.action.Action$$$' where $$$ is one of: Ok, Cancel, Yes, No,
 * Save, Close
 *
 * @author PeterBuettner.de
 */
public abstract class StandardAction extends AbstractAction {

    public final static Object OK = "Ok";
    public final static Object CANCEL = "Cancel";
    public final static Object YES = "Yes";
    public final static Object NO = "No";
    public final static Object SAVE = "Save";
    public final static Object CLOSE = "Close";

    private static final String packageName;

    static {
        String s = StandardAction.class.getName();
        int i = s.lastIndexOf('.');
        if (i == -1)
            packageName = s;
        else
            packageName = s.substring(0, i);
    }

    private static Set validTypes = new HashSet(Arrays.asList(new Object[]{OK,
            CANCEL, YES, NO, SAVE, CLOSE,}));

    /**
     * a 'empty button', only usefull if setType() is called afterwards, used to
     * create an Action an set the type later, e.g. if type is delivered from a
     * map
     */
    public StandardAction() {

    }

    /**
     * With check if type is valid!
     *
     * @param type types: OK, CANCEL, YES, NO, SAVE, CLOSE
     * @throws IllegalArgumentException if type is not in OK, CANCEL, YES, NO, SAVE, CLOSE
     */
    public StandardAction(Object type) throws IllegalArgumentException {

        setType(type);
    }

    /**
     * changes the display of this action, use with care to not confuse the
     * users. With check if type is valid!
     *
     * @param type types: OK, CANCEL, YES, NO, SAVE, CLOSE
     * @throws IllegalArgumentException if type is not in OK, CANCEL, YES, NO, SAVE, CLOSE
     */
    public void setType(Object type) {

        if (!(validTypes.contains(type)))
            throw new IllegalArgumentException(
                    "Type: '"
                            + type
                            + "' is not in valid range: "
                            + validTypes);

        String resName = packageName + ".Action" + type.toString();
        ActionUtil.initActionFromMap(this, I18N.getActionMap(resName), null);
        putValue(SHORT_DESCRIPTION, null);
    }

}