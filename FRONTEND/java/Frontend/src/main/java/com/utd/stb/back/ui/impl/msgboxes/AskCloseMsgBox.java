package com.utd.stb.back.ui.impl.msgboxes;

import com.utd.stb.inter.action.ButtonItem;
import com.utd.stb.interlib.BasicMessageBox;


/**
 * Implementation of BasicMessageBox for confirmation of any closing action. If
 * confirmed an action(probably closing a window) can be executed.
 *
 * @author rainer bruns Created 06.12.2004
 */
public class AskCloseMsgBox extends BasicMessageBox {

    Object defaultAction;
    Object defaultCloseAction;

    /**
     * Creates a MessageBox with yes/no buttons.
     *
     * @param title         Title of Messagebox.
     * @param text          Text of Messagebox
     * @param defaultAction The action to be done on confirmation
     */
    public AskCloseMsgBox(String title, String text, Object defaultAction) {
        super(null, createHeaderBar(title, ICON_QUESTION), null, text, new ButtonItem[]{
                bYes, bNo}, bNo, true);
        this.defaultAction = defaultAction;
    }

    /**
     * Creates a MessageBox with yes/no buttons.
     *
     * @param title              Title of Messagebox.
     * @param text               Text of Messagebox
     * @param defaultAction      The action to be done on confirmation
     * @param defaultCloseAction The action to be done on close
     */
    public AskCloseMsgBox(String title, String text, Object defaultAction, Object defaultCloseAction) {
        this(title, text, defaultAction);
        this.defaultCloseAction = defaultCloseAction;
    }

    /*
     * (non-Javadoc)
     * 
     * @see com.utd.stb.interlib.BasicMessageBox#onBoxClose(com.utd.stb.inter.action.ButtonItem)
     *      If confirmed do the action.
     */
    protected Object onBoxClose(ButtonItem clickedButton) {

        System.out.println("defaultAction = " + defaultAction);
        if (bYes.equals(clickedButton)) {
            //return new WindowActionImpl(WindowAction.CLOSE_WINDOW);
            return defaultAction;

        } else {

            return defaultCloseAction;

        }

    }

}