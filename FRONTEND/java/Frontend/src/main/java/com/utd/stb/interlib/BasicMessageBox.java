package com.utd.stb.interlib;

import com.utd.stb.inter.action.ButtonItem;
import com.utd.stb.inter.action.WindowAction;
import com.utd.stb.inter.dialog.HeaderBar;
import com.utd.stb.inter.dialog.HelpSource;
import com.utd.stb.inter.userinput.MetaInput;
import com.utd.stb.inter.userinput.MetaInputFactory;
import com.utd.stb.inter.userinput.UserInput;

import java.util.Collections;


/**
 * A MessageBox showing some buttons, and a noneditable text, it closes after
 * the first buttonclick of the user. <br>
 * You may overwrite onBoxClose() in your own subclass, best solution. <br>
 * <p>
 * <p>
 * headerBar and helpSource may be null
 *
 * @author PeterBuettner.de
 */
public abstract class BasicMessageBox extends BasicDialog {

    static final public String ICON_EXCLAMATION = "msgBox_Exclamation";
    static final public String ICON_STOP = "msgBox_Stop";
    static final public String ICON_QUESTION = "msgBox_Question";
    static final public String ICON_INFORMATION = "msgBox_Information";

    private ButtonItem defaultButton;

    protected boolean escapeCloses;

    protected ButtonItem clickedButton;

    /**
     * A MessageBox showing some buttons, and a noneditable text, it closes
     * after the first buttonclick of the user, and onBoxClose() is called.
     * <p>
     * headerBar and helpSource may be null
     *
     * @param title         the window caption, if null uses the parents caption
     * @param headerBar     maybe null
     * @param helpSource    maybe null
     * @param text          floating uneditable text
     * @param buttons       the buttons the user gets presented, if null/empty we generate
     *                      a 'close' button
     * @param defaultButton the default button in the former list
     * @param escapeCloses  if user can 'press escape' or 'close with the caption[x]
     *                      button', then null will be the clicked button
     */
    public BasicMessageBox(String title, HeaderBar headerBar, HelpSource helpSource,
                           String text, ButtonItem[] buttons, ButtonItem defaultButton,
                           boolean escapeCloses) {

        super(title, headerBar, helpSource, createUserInput(text));

        buttonItems = new ButtonItemsImpl(buttons);
        this.defaultButton = defaultButton;
        this.escapeCloses = escapeCloses;
    }

    /**
     * only here to be called in the constructor
     */
    private static UserInputsListImpl createUserInput(String text) {

        return new UserInputsListImpl(Collections
                .singletonList(new FloatingTextUserInput(text))
                //			Collections.singletonList(new UI.DirectUserInput(text,
                // MetaInput.class, MetaInputFactory.getFloatingText()))
        );
    }

    public ButtonItem getDefaultButtonItem() {

        return defaultButton;
    }

    public Object doAction(Object action) {

        if (action instanceof ButtonItem) {// close on first click
            if (buttonItems.indexOf((ButtonItem) action) != -1) {
                clickedButton = (ButtonItem) action;
                return new WindowActionImpl(WindowAction.CLOSE_WINDOW);
            }
        }

        if (action instanceof WindowAction) {
            Object type = ((WindowAction) action).getType();

            //	let the user close:
            if (WindowAction.WANT_CLOSE.equals(type) && escapeCloses)
                return new WindowActionImpl(WindowAction.CLOSE_WINDOW);

            /*
             * now return the clicked button, or null if user pressed
             * escape/closed with the caption[x] button
             */
            if (WindowAction.WINDOW_CLOSED.equals(type))
                return onBoxClose(clickedButton);
        }

        return null; // nothing to do
    }

    /**
     * Implement this in a inner subclass, return the action the target Window
     * should do after the messagebox is closed.
     *
     * @param clickedButton maybe null if the user closed the window with [escape] or [x]
     *                      and escapeCloses is true
     * @return
     */
    abstract protected Object onBoxClose(ButtonItem clickedButton);

    public static HeaderBar createHeaderBar(final String title, final String iconId) {

        return new HeaderBar() {

            public String getTitleLine() {

                return title;
            }

            public String getText() {

                return null;
            }

            public String getIconID() {

                return iconId;
            }
        };
    }

    /**
     * TODOlater it is independant from UI now ... rechange later... to use
     * common classes, move it to UI...
     */
    private static class FloatingTextUserInput implements UserInput {

        private final String text;
        private static final Class type = MetaInput.class;
        private static final Object data = MetaInputFactory.getFloatingText();

        public FloatingTextUserInput(String text) {

            this.text = text;
        }

        public String getLabel() {

            return text;
        }

        public Object getData() {

            return data;
        }

        public Class getType() {

            return type;
        }

        public void setData(Object data) {

        }

        public boolean isEmptyAllowed() {

            return true;
        }


        @Override
        public void setEmptyAllowed(boolean emptyAllowed) {

        }


        public boolean isReadOnly() {

            return true;
        }

        public boolean isChangeable() {
            return false;
        }

        public boolean isVisible() {

            return true;
        }

        public Object getExtra(String key) {

            return null;
        }

        public boolean hasFocus() {
            return false;
        }

        public void setFocus(boolean focus) {

        }

        public void setVisible(boolean emptyAllowed) {

        }
    }

}