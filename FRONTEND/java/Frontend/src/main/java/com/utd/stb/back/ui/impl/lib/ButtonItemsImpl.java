package com.utd.stb.back.ui.impl.lib;

import com.utd.stb.inter.action.ButtonItem;
import com.utd.stb.inter.action.ButtonItems;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;


/**
 * A Collection of ButtonItems
 *
 * @author PeterBuettner.de
 */
public class ButtonItemsImpl implements ButtonItems {

    private List data;

    public ButtonItemsImpl(List data) {

        this.data = new ArrayList(data);
    }

    public ButtonItemsImpl(ButtonItem[] data) {

        this.data = Arrays.asList(data);
    }

    public ButtonItemsImpl(ButtonItem data) {

        this.data = Collections.singletonList(data);
    }

    public int getSize() {

        return data.size();
    }

    public ButtonItem get(int i) {

        return (ButtonItem) data.get(i);
    }

    public int indexOf(ButtonItem buttonItem) {

        return data.indexOf(buttonItem);
    }

}