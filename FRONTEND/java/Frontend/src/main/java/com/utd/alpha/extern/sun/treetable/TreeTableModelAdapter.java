package com.utd.alpha.extern.sun.treetable;

import javax.swing.*;
import javax.swing.event.TreeExpansionEvent;
import javax.swing.event.TreeExpansionListener;
import javax.swing.event.TreeModelEvent;
import javax.swing.event.TreeModelListener;
import javax.swing.table.AbstractTableModel;
import javax.swing.tree.AbstractLayoutCache;
import javax.swing.tree.FixedHeightLayoutCache;
import javax.swing.tree.TreePath;
import java.awt.*;
import java.beans.PropertyChangeEvent;
import java.beans.PropertyChangeListener;


/**
 * This is a wrapper class takes a TreeTableModel and implements the table model
 * interface. The implementation is trivial, with all of the event dispatching
 * support provided by the superclass: the AbstractTableModel.
 * <p>
 * Now the implementation is more smart, we don't loose selection state while
 * expand/collapse. We use a cache (FixedHeightLayoutCache, not using any
 * geometric parts from it) to still have the treeState 'before' any change
 * events
 * <p>
 * TODO be smart for TreeModelEvents too :-)
 *
 * @author Philip Milne
 * @author Scott Violet
 * @author Peter Buettner
 * @version 1.2 10/27/98
 */
public class TreeTableModelAdapter extends AbstractTableModel {

    // ##### Wrappers, implementing TableModel
    // interface.#########################
    public int getColumnCount() {

        return model.getColumnCount();
    }

    public String getColumnName(int column) {

        return model.getColumnName(column);
    }

    public Class getColumnClass(int column) {

        return model.getColumnClass(column);
    }

    public int getRowCount() {

        return tree.getRowCount();
    }

    protected Object nodeForRow(int row) {

        TreePath treePath = tree.getPathForRow(row);
        return treePath == null ? null : treePath.getLastPathComponent();
    }

    public Object getValueAt(int row, int column) {

        return model.getValueAt(nodeForRow(row), column);
    }

    public boolean isCellEditable(int row, int column) {

        return model.isCellEditable(nodeForRow(row), column);
    }

    public void setValueAt(Object value, int row, int column) {

        model.setValueAt(value, nodeForRow(row), column);
    }

    // ##########################################################################

    private final TreeTableModel model;
    private final JTree tree;

    /**
     * we need this because removed nodes are not in the model anymore, so we
     * cache them, sizes are dummy ones
     */
    private AbstractLayoutCache mapper;
    private InterTreeModelListener inter = new InterTreeModelListener();

    /**
     * intermediate wrapper above the mapper/cache
     */
    private class InterTreeModelListener implements TreeModelListener {

        public void treeNodesChanged(TreeModelEvent e) {

            mapper.treeNodesChanged(e);
        }

        public void treeNodesInserted(TreeModelEvent e) {

            mapper.treeNodesInserted(e);
        }

        public void treeNodesRemoved(TreeModelEvent e) {

            mapper.treeNodesRemoved(e);
        }

        public void treeStructureChanged(TreeModelEvent e) {

            mapper.treeStructureChanged(e);
        }

        public void setExpandedState(TreePath path, boolean isExpanded) {

            // we expand -> insert, so first update cache
            if (isExpanded) mapper.setExpandedState(path, isExpanded);

            int rowStart = mapper.getRowForPath(path) + 1;// invariant
            int nRows = mapper.getVisibleChildCount(path);
            int maxRow = mapper.getRowCount() - 1;

            if (!isExpanded) // collapse -> delete: ask cache for state before
                // change
                mapper.setExpandedState(path, isExpanded);

            // sanityCheck: no childs, last row [maybe we get silly events...
            // never seen, but..]
            if (nRows == 0 || rowStart > maxRow) return;

            if (isExpanded)
                fireTableRowsInserted(rowStart, rowStart + nRows - 1);
            else
                fireTableRowsDeleted(rowStart, rowStart + nRows - 1);
        }
    }

    private AbstractLayoutCache createCache(JTree tree) {

        final AbstractLayoutCache cache = new FixedHeightLayoutCache();
        cache.setRowHeight(10);
        cache.setNodeDimensions(new AbstractLayoutCache.NodeDimensions() {

            public Rectangle getNodeDimensions(Object value, int row, int depth,
                                               boolean expanded, Rectangle bounds) {

                // never called since we never ask for sizes
                if (bounds == null) bounds = new Rectangle();
                bounds.setBounds(0, row * 10, 100, 10);
                return bounds;
            }
        });

        cache.setRootVisible(tree.isRootVisible());

        cache.setModel(model);
        tree.addPropertyChangeListener(JTree.ROOT_VISIBLE_PROPERTY,
                new PropertyChangeListener() {

                    public void propertyChange(PropertyChangeEvent evt) {

                        cache.setRootVisible(((Boolean) evt
                                .getNewValue()).booleanValue());
                    }
                });
        return cache;
    }

    public TreeTableModelAdapter(TreeTableModel treeTableModel, JTree tree) {

        this.tree = tree;
        this.model = treeTableModel;
        mapper = createCache(tree);

        model.addTreeModelListener(inter);

        tree.addTreeExpansionListener(new TreeExpansionListener() {

            public void treeExpanded(TreeExpansionEvent e) {

                inter.setExpandedState(e.getPath(), true);
            }

            public void treeCollapsed(TreeExpansionEvent e) {

                inter.setExpandedState(e.getPath(), false);
            }
        });

        // Installs a TreeModelListener that can update the table when
        // the tree changes. We use delayedFireTableDataChanged as we can
        // not be guaranteed the tree will have finished processing
        // the event before us.
        treeTableModel.addTreeModelListener(new TreeModelListener() {

            public void treeNodesChanged(TreeModelEvent e) {

                // use fireTableRowsUpdated here, it's easy
                delayedFireTableDataChanged();
            }

            public void treeNodesInserted(TreeModelEvent e) {

                delayedFireTableDataChanged();
            }

            public void treeNodesRemoved(TreeModelEvent e) {

                delayedFireTableDataChanged();
            }

            public void treeStructureChanged(TreeModelEvent e) {

                delayedFireTableDataChanged();
            }
        });
    }

    /**
     * Invokes fireTableDataChanged after all the pending events have been
     * processed. SwingUtilities.invokeLater is used to handle this.
     */
    protected void delayedFireTableDataChanged() {

        SwingUtilities.invokeLater(new Runnable() {

            public void run() {

                fireTableDataChanged();
            }
        });
    }

}

