package com.utd.stb.back.ui.impl.userinputs.fields;

import com.utd.stb.inter.userinput.DateInput;

import java.time.ZonedDateTime;


/**
 * Implementation of AbstractUserInput for date fields
 *
 * @author rainer bruns Created 02.12.2004
 */
public class DateUserInput extends AbstractUserInput {

    private DateInput dateInput;

    /**
     * @param label Label of field
     * @param data  the data for field
     */
    public DateUserInput(String label, DateInput data) {

        super(label);
        this.dateInput = data;
    }

    /*
     * (non-Javadoc)
     * 
     * @see com.utd.stb.inter.userinput.UserInput#getType()
     */
    public Class getType() {

        return DateInput.class;
    }

    /*
     * (non-Javadoc)
     * 
     * @see com.utd.stb.inter.userinput.UserInput#setData(java.lang.Object)
     */
    public void setData(Object data) {

        dateInput.setDate((ZonedDateTime) data);
    }

    /*
     * (non-Javadoc)
     * 
     * @see com.utd.stb.inter.userinput.UserInput#getData()
     */
    public Object getData() {

        return dateInput;
    }

}