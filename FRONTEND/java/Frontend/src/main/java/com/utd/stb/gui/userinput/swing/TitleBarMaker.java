package com.utd.stb.gui.userinput.swing;

import com.utd.gui.controls.TitleBar;
import com.utd.stb.inter.dialog.HeaderBar;
import resources.IconSource;

import javax.swing.*;


/**
 * Little Tool to build a TitleBar
 *
 * @author PeterBuettner.de
 */
public class TitleBarMaker {

    /**
     * returns null if header is null
     *
     * @param header
     * @param defaultIcon if the HeaderBar contains null as icon we use this as default
     * @return
     */
    public static TitleBar createTitleBar(HeaderBar header, String defaultIcon) {

        if (header == null) return null;

        String iconId = header.getIconID();
        Icon icon = null;

        if (iconId == null)
            icon = new IconSource().getHeaderIconByID(defaultIcon); // null ->
            // default
        else if (iconId.length() == 0)
            icon = null; // "" -> empty
        else
            icon = new IconSource().getHeaderIconByID(iconId); // use

        return new TitleBar(header.getTitleLine(), header.getText(), icon, 0);
    }

}