package com.utd.gui.popup;

import javax.swing.*;
import javax.swing.event.AncestorEvent;
import javax.swing.event.AncestorListener;
import java.awt.event.*;


/**
 * Helper Listener for selfmade combolike popups, shows/hides a popup on several
 * events,
 * here is a snippet:
 * <p>
 * <pre>
 * ShowHideManagingListener showHideManagingListener = new ShowHideManagingListener( this );
 *
 * textField.addAncestorListener( showHideManagingListener );
 * textField.addComponentListener( showHideManagingListener );
 * textField.addKeyListener( showHideManagingListener );// not neccesary anymore?
 * textField.addMouseListener( showHideManagingListener );
 * textField.addFocusListener( showHideManagingListener );
 * </pre>
 * <p>
 * it will inform 'this' to show/hide the popup, and to scroll the editor into
 * view (you may ignore the latter if it's not in a Table...)
 *
 * @author PeterBuettner.de
 */
class ShowHideManagingListener implements AncestorListener, ComponentListener, KeyListener,
        MouseListener, FocusListener {

    private PopupShowableScrollable associated;

    public ShowHideManagingListener(PopupShowableScrollable associated) {

        this.associated = associated;

    }

    private void show() {
        associated.popShow(true);

    }

    private void hide() {

        associated.popShow(false);

    }

    public void ancestorAdded(AncestorEvent e) {

        show();

    }

    public void ancestorMoved(AncestorEvent e) {

        hide();

    }

    public void ancestorRemoved(AncestorEvent e) {

        hide();

    }

    public void componentMoved(ComponentEvent e) {

        show();

    }

    public void componentShown(ComponentEvent e) {

        show();

    }

    public void componentHidden(ComponentEvent e) {

        hide();

    }

    public void componentResized(ComponentEvent e) {

        hide();

    }

    public void keyPressed(KeyEvent e) {

        associated.scrollEditorToVisible();
        show();

    }

    public void keyReleased(KeyEvent e) {

        show();

    }

    public void keyTyped(KeyEvent e) {

    }

    public void mouseClicked(MouseEvent e) {

    }

    public void mouseEntered(MouseEvent e) {

    }

    public void mouseExited(MouseEvent e) {

    }

    public void mousePressed(MouseEvent e) {

        showLater();

    }

    public void mouseReleased(MouseEvent e) {

        showLater();

    }

    private void showLater() { // don't know anymore why we do this 'later',
        // maybe when we used JPopUp?

        SwingUtilities.invokeLater(new Runnable() {

            public void run() {

                associated.scrollEditorToVisible();
                show();

            }
        });

    }

    // --------------------------------------------------------------------

    // selfmade popup (vs. JPopupMenu)
    public void focusGained(FocusEvent e) {

    }

    public void focusLost(FocusEvent e) {

        /*
         * if we return on e.isTemporary() the mousewheel works, but popup may
         * be hidden behind frame: No: the hiding is resolved since we now
         * always create Heavyweight Popups with a _owner_
         */
        if (e.isTemporary()) return;
        hide();

    }

}