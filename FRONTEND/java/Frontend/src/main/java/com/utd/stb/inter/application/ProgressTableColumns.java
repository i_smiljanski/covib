package com.utd.stb.inter.application;

import com.jidesoft.swing.JideButton;

import javax.swing.*;
import javax.swing.table.*;
import java.awt.*;

/**
 * @author Inna Smiljanski  created 16.11.2010
 */
public enum ProgressTableColumns {
    JOBID("Jobid", 0, 50, null),
    TIME("Start time", 1, 100, null),
    JOB("Job name", 2, 200, null),
    PROGRESS("Progress", 3, 240, new ProgressBarCellRenderer()),
//    BUTTON(" ", 3, 50, new ButtonCellRenderer())
    ;

    private String label;
    private int column;
    private int width;
    private TableCellRenderer cellRenderer;

    ProgressTableColumns(String s, int i, int i1, TableCellRenderer cr) {
        label = s;
        column = i;
        width = i1;
        cellRenderer = cr;
    }


    private static class ProgressBarCellRenderer extends DefaultTableCellRenderer {
        public Component getTableCellRendererComponent(JTable table, Object value,
                                                       boolean isSelected, boolean hasFocus,
                                                       int row, int column) {
            if (value instanceof JProgressBar)
                return (JProgressBar) value;
            else if (value instanceof JLabel)
                return (JLabel) value;
            else
                return super.getTableCellRendererComponent(table, value, isSelected, hasFocus, row, column);
        }
    }

    private static class ButtonCellRenderer extends DefaultTableCellRenderer {
        public Component getTableCellRendererComponent(JTable table, Object value,
                                                       boolean isSelected, boolean hasFocus,
                                                       int row, int column) {
            if (value instanceof JideButton)
                return (JideButton) value;
            else
                return super.getTableCellRendererComponent(table, value, isSelected, hasFocus, row, column);
        }
    }

    public static void setColumnModel(TableColumnModel columnModel) {
        TableColumn col;
        for (ProgressTableColumns p : ProgressTableColumns.values()) {
            col = columnModel.getColumn(p.column);
            col.setPreferredWidth(p.width);
            col.setCellRenderer(p.cellRenderer);
        }
//        TableColumn colJobid = columnModel.getColumn(JOBID.column);
//        colJobid.setPreferredWidth(JOBID.width);
//
//        TableColumn colProgress = columnModel.getColumn(PROGRESS.column);
//        colProgress.setPreferredWidth(PROGRESS.width);
//        colProgress.setCellRenderer(PROGRESS.cellRenderer);
//
//        TableColumn colButton = columnModel.getColumn(BUTTON.column);
//        colButton.setPreferredWidth(BUTTON.width);
//        colButton.setCellRenderer(BUTTON.cellRenderer);

    }

    public static void addColumn(DefaultTableModel model) {
        for (ProgressTableColumns p : ProgressTableColumns.values()) {
            model.addColumn(p.label);
        }
    }

    public String getLabel() {
        return label;
    }

    public int getColumn() {
        return column;
    }

    public int getWidth() {
        return width;
    }

    public TableCellRenderer getCellRenderer() {
        return cellRenderer;
    }
}
