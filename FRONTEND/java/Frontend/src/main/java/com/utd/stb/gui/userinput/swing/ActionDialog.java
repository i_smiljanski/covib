package com.utd.stb.gui.userinput.swing;

import com.uptodata.isr.gui.lookAndFeel.TweakUI;
import com.utd.stb.back.ui.impl.userinputs.data.FilterTableDialog;
import com.utd.stb.gui.swingApp.TimeoutReceiver;
import com.utd.stb.gui.swingApp.UserDialogTool;
import com.utd.stb.gui.swingApp.exceptions.ExceptionDialog;
import com.utd.stb.gui.swingApp.exceptions.ExceptionHandler;
import com.utd.stb.inter.action.ComponentAction;
import com.utd.stb.inter.action.WindowAction;
import com.utd.stb.inter.application.BackendException;
import com.utd.stb.inter.dialog.UserDialog;
import com.utd.stb.interlib.ComponentActionImpl;
import com.utd.stb.interlib.WindowActionImpl;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import javax.swing.*;
import java.awt.*;


/**
 * Used as a basic for ReportWizardDialog and UserInputDialog, to handle some
 * common actions
 *
 * @author PeterBuettner.de
 */
public abstract class ActionDialog extends JDialog implements ReturnValueProvider,
        TimeoutReceiver, ExceptionHandler {
    public static final Logger log = LogManager.getRootLogger();

    /* interface ReturnValueProvider */
    protected Object returnValue;

    public Object getReturnValue() {

        return returnValue;
    }

    /* interface TimeoutReceiver */
    public void timeoutOccured() {

        returnValue = null; // just to be shure...
        // do nothing... the window is hidden/disposed from outside
        try {
            doAction(new WindowActionImpl(WindowAction.TIMEOUT_WINDOW_CLOSED));
        } catch (BackendException e) {
            e.printStackTrace();
        } // we just inform ... eat exceptions

    }

    /* interface ExceptionHandler */
    public boolean exceptionHandler(Throwable throwable, boolean clientCanContinue) {

        ExceptionDialog.showExceptionDialog(this, throwable, null);

        if (clientCanContinue && throwable instanceof BackendException) {

            // maybe not too bad
            if (((BackendException) throwable).isInformational()) return true;
        }

        exitDlgExceptional(createExceptionMsg(throwable));
        return false;

    }

    public ActionDialog(Dialog owner) throws HeadlessException {

        super(owner);
        TweakUI.setDialogDecoration(this);
    }

    public ActionDialog(Frame owner) throws HeadlessException {

        super(owner);
        TweakUI.setDialogDecoration(this);
        if (owner != null) {
            this.setIconImage(owner.getIconImage());
        }
    }

    /**
     * does the actions until one returns null
     *
     * @param action
     * @return
     */
    protected void executeActionLoop(Object action) {
        while (action != null) {
            action = executeAction(action);
        }
    }

    /**
     * executes one action
     *
     * @param action
     * @return the result of the action
     */
    protected Object executeAction(Object action) {
        log.debug(action);
        if (action == null) return null;

        if (action instanceof WindowAction) {
            return executeWindowAction((WindowAction) action);
        }

        if (action instanceof UserDialog) {
//            if (action instanceof FilterTableDialog){
//               log.debug(this);
//            }
            if (action instanceof com.utd.stb.back.ui.impl.userinputs.data.MenuDialog) {
                JFrame app = (JFrame) this.getParent();
                this.setModal(false);
                Rectangle bounds = this.getBounds();
                log.debug("title:" + this.getTitle() + " bounds:" + bounds.getLocation() + " ");
                // MCD, 20. May 2008 without 'later' we get (gray) artefacts on the screen
                SwingUtilities.invokeLater(new Runnable() {

                    public void run() {
                        log.debug("dispose ");
                        dispose();
                    }
                });
//        		this.dispose();

                return UserDialogTool.executeUserDialog(app, ((UserDialog) action), bounds);
            }
            return UserDialogTool.executeUserDialog(this, ((UserDialog) action));
        }

        // we don't understand: send it back
        return doActionChecked(action);
    }

    /**
     * you may overwrite this to handle messages different, here is only
     * handled:
     * <ul>
     * <li>CLOSE_WINDOW
     * <li>EXCEPTION_ABORT_ACTION
     * <li>EXCEPTION_ABORT_APPLICATION
     * </ul>
     * all other types are send back to the base
     *
     * @param action
     * @return
     */
    protected Object executeWindowAction(WindowAction action) {
        if (WindowAction.CLOSE_WINDOW.equals(action.getType())) {
            super.getParent().setCursor(Cursor.getPredefinedCursor(Cursor.WAIT_CURSOR));

            // since doActionChecked() may set returnValue on error we
            // handle this special
            returnValue = null;
            Object tmp = doActionChecked(new WindowActionImpl(WindowAction.WINDOW_CLOSED));
            if (returnValue == null) returnValue = tmp;// set only if not set
            // before!
            log.debug(returnValue);
            setVisible(false);

            super.getParent().setCursor(Cursor.getDefaultCursor());
            return null;// stop executing here in the dialog

        }

        // send exceptions back to the last dialog; and the backend
        if (WindowAction.EXCEPTION_ABORT_ACTION.equals(action.getType())
                || WindowAction.EXCEPTION_ABORT_APPLICATION.equals(action.getType())) {
            return exitDlgExceptional(action);
        }

        // we don't understand: send it back
        return doActionChecked(action);

    }

    /**
     * Call from the focus lost event of a component
     */
    public void performComponentAction() {
        executeActionLoop(new ComponentActionImpl(ComponentAction.CHECK_MASK));
    }

    /**
     * a wrapper for doAction() that catches <br>
     * exceptions and handles them: on exception does: (inform backend, set
     * return value, hide dialog, return null)
     *
     * @param action what we should try to do
     * @return
     */
    protected Object doActionChecked(Object action) {
        try {
            Object returnAction = doAction(action);
            log.debug(returnAction);
            return returnAction;
        } catch (BackendException e) {
            ExceptionDialog.showExceptionDialog(this, e, null);
            if (e.isInformational()) return null; // just an information, do
            // nothing
            return exitDlgExceptional(createExceptionMsg(e));
        }

    }

    /**
     * informs the backend, sets the return value, hides the dialog, returns
     * null
     *
     * @param action use WindowAction.EXCEPTION_ABORT_APPLICATION for example
     */
    protected Object exitDlgExceptional(WindowAction action) {

        try {
            doAction(action);
        } catch (BackendException e1) {
            e1.printStackTrace();
        } // eat exceptions - we close afterwards

        // tell parent window about exception
        returnValue = action;
        // MCD, 23.Jan 2006, funktioniert nicht: hide(); WARUM???
//        setVisible(false);  is, 7.apr.2016 auskommentiert. Falls exception in  Wizard passierte, wurden alle Fenster incl. Wizard geschlossen
        return null;

    }

    /**
     * Creates a WinowAction that informs parent Dialogs/Windows about the
     * exception (thy may close then), depending from the throwable: <br>
     * <p>
     * If throwable is a BackendException the proper Windowaction is created,
     * for every other exception WindowAction.EXCEPTION_ABORT_APPLICATION is
     * used
     *
     * @param throwable
     * @return
     */
    protected WindowAction createExceptionMsg(Throwable throwable) {
        log.debug(throwable);
        if (throwable instanceof BackendException) {

            BackendException e = (BackendException) throwable;

            if (e.isAppExit())
                return new WindowActionImpl(WindowAction.EXCEPTION_ABORT_APPLICATION);

            return new WindowActionImpl(WindowAction.EXCEPTION_ABORT_ACTION);

        }

        return new WindowActionImpl(WindowAction.EXCEPTION_ABORT_APPLICATION);

    }

    /**
     * please return doAction(action) of the defining interface (ReportWizzard,
     * UserDialog), don't handle the exceptions, this is done down here
     *
     * @param action
     * @return
     * @throws BackendException
     */
    protected abstract Object doAction(Object action) throws BackendException;

}