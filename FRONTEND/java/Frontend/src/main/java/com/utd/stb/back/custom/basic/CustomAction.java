package com.utd.stb.back.custom.basic;

import com.utd.stb.back.data.client.TreeNodeRecord;


/**
 * @author rainer bruns Created 21.12.2004 Interface for Custom actions in ISR.
 *         These are actions implemented in custom menu
 */
public interface CustomAction {

    /**
     * @param node TreeNode selected when action is started
     * @return another CustmAction or null
     */
    public CustomAction execute(TreeNodeRecord node);
}