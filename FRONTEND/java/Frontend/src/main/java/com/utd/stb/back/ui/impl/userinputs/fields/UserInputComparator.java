package com.utd.stb.back.ui.impl.userinputs.fields;

import com.utd.stb.inter.userinput.DateInput;
import com.utd.stb.inter.userinput.TextInput;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.time.ZonedDateTime;
import java.util.Comparator;

/**
 * @author Inna Smiljanski created 07.04.11
 */
public class UserInputComparator implements Comparator {
    public static final Logger log = LogManager.getRootLogger();

    //todo
    public int compare(Object ui1, Object ui2) {
        //  log.debug(ui1+"; "+ui2);
        // log.debug(((TextUserInput) ui1).getData());
        if (ui1 instanceof TextUserInput && ((TextUserInput) ui1).getData() instanceof TextInput) {
            String text1 = ((TextInput) ((TextUserInput) ui1).getData()).getText();
            String text2 = ((TextInput) ((TextUserInput) ui2).getData()).getText();
//             log.debug("compare: "+text1+"; "+text2+"; "+text1.compareTo(text2));
            return text1.compareTo(text2);
        } else if (ui1 instanceof DateUserInput
                && ((DateUserInput) ui1).getData() instanceof DateInput       //todo
                ) {
            ZonedDateTime text1 = ((DateInput) ((DateUserInput) ui1).getData()).getDate();
            ZonedDateTime text2 = ((DateInput) ((DateUserInput) ui2).getData()).getDate();

            return text1.compareTo(text2);
        } else
            return 0;
    }
}
