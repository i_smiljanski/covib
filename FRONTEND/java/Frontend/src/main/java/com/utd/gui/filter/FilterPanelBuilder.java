package com.utd.gui.filter;

import com.jgoodies.forms.builder.DefaultFormBuilder;
import com.jgoodies.forms.layout.CellConstraints;
import com.jgoodies.forms.layout.FormLayout;
import com.utd.gui.resources.I18N;
import com.utd.gui.util.DialogUtils;

import javax.swing.*;
import javax.swing.text.JTextComponent;
import java.awt.*;
import java.awt.event.ActionEvent;


/**
 * Factory to have consistent filterpanels all around
 *
 * @author PeterBuettner.de
 */
public class FilterPanelBuilder {

    private FilterPanelBuilder() {

    }

    public static FilterPanelBuilder getInstance() {

        return new FilterPanelBuilder();

    }

    /**
     * Doesn't bind the textfield to the filter, since some constellations work
     * special.
     *
     * @param errorMsgLabel
     * @param filterField
     * @return
     */
    public JComponent build(FilterErrorMsgDisplay errorMsgLabel, JTextComponent filterField) {

        FormLayout layout = new FormLayout("p,1dlu, p, 2dlu, min:G", "p");
        DefaultFormBuilder builder = DialogUtils.getFormBuilder(layout);

        CellConstraints cc = new CellConstraints();
        builder.add(filterField, cc.xy(5, 1));
        builder.add(createShowAllButton(filterField), cc.xy(1, 1));
        builder.add(errorMsgLabel.getComponent(), cc.xy(3, 1));

        return builder.getPanel();

    }

    /**
     * Creates a small Button (non-focussable, non defaultable, i18n) that sets
     * the text of the filterField null
     *
     * @param filterField
     * @return
     */
    public JButton createShowAllButton(final JTextComponent filterField) {

        Action acShowAll = new ShowAllFilterAction() {

            public void actionPerformed(ActionEvent e) {

                filterField.setText(null);
            }
        };

        return makeUpButton(new JButton(acShowAll));

    }

    public JComponent buildSelectionPane(FilterErrorMsgDisplay errorMsgLabel,
                                         JTextComponent filterField) {

        FormLayout layout = new FormLayout("p,1dlu, p, 2dlu, min:G", "p");
        DefaultFormBuilder builder = DialogUtils.getFormBuilder(layout);

        CellConstraints cc = new CellConstraints();
        builder.add(filterField, cc.xy(5, 1));
        builder.add(createUpdateButton(filterField), cc.xy(1, 1));
        builder.add(errorMsgLabel.getComponent(), cc.xy(3, 1));

        return builder.getPanel();

    }

    /**
     * Creates a small Button (non-focussable, non defaultable, i18n) that sets
     * the text of the filterField 'to itself': <br>
     * <code>setTranslation(getTranslation())</code><br>
     * so the field gets an documentchange event, ... well it's a bit a hack but
     * works fine
     *
     * @param filterField
     * @return
     */
    public JButton createUpdateButton(final JTextComponent filterField) {

        Action acUpdate = new ShowAllFilterAction() {

            public void actionPerformed(ActionEvent e) {

                filterField.setText(filterField.getText());
            }
        };
        acUpdate.putValue(Action.SHORT_DESCRIPTION, I18N
                .getString("com.utd.gui.filter.UpdateSelectionFilterAction.tooltip"));
        return makeUpButton(new JButton(acUpdate));

    }

    /**
     * little helper to makeup some properties of the little button: margin,
     * nonDefaultCapable, nonFocussable
     *
     * @param button
     * @return the button
     */
    private static JButton makeUpButton(JButton button) {

        button.setMargin(new Insets(0, 1, 0, 1));
        button.setDefaultCapable(false);
        button.setFocusable(false);
        return button;

    }

}

