package com.utd.gui.action;

import javax.swing.*;
import java.awt.event.ActionEvent;


/**
 * Does Nothing, has no properties.
 *
 * @author PeterBuettner.de
 */
public class NullAction extends AbstractAction {

    public void actionPerformed(ActionEvent e) {

    }

}