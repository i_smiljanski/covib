package com.utd.stb.back.ui.impl.msgboxes;

import com.utd.stb.back.database.BackendErrorException;
import com.utd.stb.back.ui.impl.tree.TreeBase;
import com.utd.stb.gui.swingApp.exceptions.ExceptionDialog;
import com.utd.stb.inter.action.ButtonItem;
import com.utd.stb.inter.application.BackendException;
import com.utd.stb.interlib.BasicMessageBox;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;


/**
 * Implementation of BasicMessageBox for confirmation of Action execution
 *
 * @author rainer bruns Created 06.12.2004
 */
public class ConfirmActionMsgBox extends BasicMessageBox {

    public static final Logger log = LogManager.getRootLogger();
    Object item;
    TreeBase base;

    /**
     * Creates a MessageBox with yes/no buttons.
     *
     * @param title   Title of MessageBox
     * @param text    Text of MessageBox
     * @param barText Text in HeaderBar
     * @param item    An object that is used to identify what do do on confirmation.
     * @param base    The base of tree
     */
    public ConfirmActionMsgBox(String title, String text, String barText, Object item,
                               TreeBase base) {
        super(title, createHeaderBar(barText, ICON_QUESTION), null, text, new ButtonItem[]{
                bYes, bNo}, bNo, true);

        log.debug("title = " + title + " text = " + text + " " + Thread.currentThread().getStackTrace()[2]);
        this.item = item;
        this.base = base;
    }

    /*
     * (non-Javadoc)
     * 
     * @see com.utd.stb.interlib.BasicMessageBox#onBoxClose(com.utd.stb.inter.action.ButtonItem)
     *      On confirmation call invokeAction with item(this is of type
     *      MenuEntryRecord) in base. So the desired action of menu will be
     *      executed. This is actually the only invocation type, but can be
     *      extended for sure.
     */
    protected Object onBoxClose(ButtonItem clickedButton) {
        log.debug(clickedButton);
        try {
            if (bYes.equals(clickedButton)) {
                //return new WindowActionImpl(WindowAction.CLOSE_WINDOW);
                Object returnAction = base.invokeAction(item);
                log.debug(returnAction);
                return returnAction;
            }
        } catch (BackendException be) {
            BackendErrorException b = (BackendErrorException) be;
            /*
             * if(b.getSeverity() == ErrorRecord.cnSeverityCritical) {
             * ActionList action = new ActionList(); action.add(be); return
             * null; }
             */
            // should later be handled by GUI
            ExceptionDialog.showExceptionDialog(null, b, null);
//            return new WarningMsgBox(b.getErrorCode(), b.getMessage() );
        }
        return null;
    }
}