package com.utd.stb.inter.wizzard;

import com.utd.stb.inter.application.BackendException;


/**
 * Base interface for all FormData extensions, it defines what the user can
 * edit, the type (on out of list, many out of list...) is defined by a subtype
 * of this interface.
 *
 * @author PeterBuettner.de
 */
public interface FormData {

    /**
     * Send the configuration to the Backend,
     */
    void saveData() throws BackendException;

}