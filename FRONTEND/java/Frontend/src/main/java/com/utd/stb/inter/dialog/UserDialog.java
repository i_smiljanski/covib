package com.utd.stb.inter.dialog;

import com.utd.stb.inter.action.ButtonItem;
import com.utd.stb.inter.action.ButtonItems;
import com.utd.stb.inter.application.BackendException;
import com.utd.stb.inter.userinput.UserInputs;

import java.awt.*;


/**
 * Definition of an allround dialog using UserInput
 *
 * @author PeterBuettner.de
 */
public interface UserDialog {

    /**
     * For the Windows-Captionbar (WindowManagers one), null will use the
     * caption of the parent window
     *
     * @return
     */
    String getTitle();

    /**
     * null means a 'standard' Dialog without a HeaderBar
     *
     * @return
     */
    HeaderBar getHeaderBar();

    /**
     * The data to be displayed or shown
     *
     * @return UserInputs
     */
    UserInputs getUserInputs();

    /**
     * Source to show Help for this dialog, if null no help is known.
     *
     * @return
     */
    HelpSource getHelpSource();

    /**
     * defines the actions (==buttons) the user gets presented, never use null,
     * define at least one ButtonItem
     *
     * @return
     */
    ButtonItems getButtonItems();

    /**
     * The Button that should be the default button in the dialog (activated on
     * 'Enter'-Key), if null and there is a [Ok] this is the default one, if
     * there is only one actionButton this will become the default one, 'Help'
     * will never be the default button
     *
     * @return
     */

    ButtonItem getDefaultButtonItem();

    /**
     * some user action occured (pressed button) or window was closed..., or
     * data saved
     *
     * @param action
     * @return what to do next
     */
    Object doAction(Object action) throws BackendException;

    default Object doAction(Object action, Component owner) throws BackendException{
        return doAction(action);
    }
}