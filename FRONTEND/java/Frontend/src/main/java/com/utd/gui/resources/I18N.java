package com.utd.gui.resources;

import com.utd.stb.back.database.Dispatcher;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import javax.swing.*;
import java.util.HashMap;
import java.util.Map;
import java.util.MissingResourceException;
import java.util.ResourceBundle;


/**
 * Project independent Strings for the controls, and some tools to read them.
 *
 * @author PeterBuettner.de
 */
public class I18N {

    public static final Logger log = LogManager.getRootLogger();
    public static Dispatcher dispatcher;

    private static ResourceBundle bundle;

    /**
     * The resourcebundle, read on first access, uses the default locale
     *
     * @return
     */

    public static ResourceBundle getBundle() {

        if (bundle == null) {
            bundle = ResourceBundle.getBundle("I18N", new UTF8Control());
        }
        return bundle;
    }

    /**
     * rereads the Resourcebundle, so data <b>read in the future </b> will be
     * new
     */
    public static void resetLanguage() {

        bundle = null;
    }

    /**
     * returns the String from the backend
     *
     * @param key
     * @return
     */
    public static String getBackendString(String key, ResourceBundle bundle) {
        String text = "";
        try {
            text = dispatcher.getTranslation(key);
            if (text.equals(key)) throw new Exception();
            return text;
        } catch (Exception e) {
            try {
                String value = bundle.getString(key);
//                log.debug(key + " : " + value);
                return value;
            } catch (MissingResourceException mre) {
//                log.debug(text);
                log.error("MissingResourceException for key '" + key + "'");
            }
            return null;
        }
    }

    /**
     * returns the String from the default bundle or null if not found
     *
     * @param key
     * @return
     */
    public static String getString(String key, ResourceBundle bundle) {

        return getBackendString(key, bundle);
    }

    /**
     * returns the String from the default bundle or null if not found
     *
     * @param key
     * @return
     */
    public static String getString(String key) {

        return getBackendString(key, getBundle());
    }

    /**
     * returns the String from the default bundle or null if not found, the
     * searched String is combined from clazz.getName()+"."+key
     *
     * @param key
     * @return
     */
    public static String getString(Class clazz, String key) {

        return getString(clazz.getName() + "." + key);
    }

    /**
     * Loads the properties of an action into a map, reads name mnemonic
     * accelerator tooltip, output are the corresponding names in Action. This
     * puts <b>Strings </b> into the map, so you have to convert them to
     * Integer, KeyStroke,... by yourself, no error if a string isn't found
     *
     * @param bundle
     * @param resName the starting part, e.g.: "reportwizzard.action.x"
     * @return
     */
    public static Map getActionMap(ResourceBundle bundle, String resName) {

        Map map = new HashMap(5);
        putValue(bundle, resName + ".name", map, Action.NAME);
        putValue(bundle, resName + ".mnemonic", map, Action.MNEMONIC_KEY);
        putValue(bundle, resName + ".accelerator", map, Action.ACCELERATOR_KEY);
        putValue(bundle, resName + ".tooltip", map, Action.SHORT_DESCRIPTION);
        return map;

    }

    /**
     * uses the default Bundle
     *
     * @param resName
     * @return
     */
    public static Map getActionMap(String resName) {

        return getActionMap(getBundle(), resName);
    }

    private static void putValue(ResourceBundle r, String name, Map map, Object key) {

        try {

            // BEOBACHTEN
            String value = getString(name, r);
            if (value == null) value = r.getString(name);
            if (value != null) map.put(key, value);

        } catch (MissingResourceException e) {
            e.printStackTrace();
            // on missing resource do nothing, later we may log
        }
    }
}