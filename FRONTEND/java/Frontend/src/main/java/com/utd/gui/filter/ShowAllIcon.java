package com.utd.gui.filter;

import com.uptodata.isr.gui.util.Colors;

import javax.swing.*;
import java.awt.*;


public class ShowAllIcon implements Icon {

    public int getIconHeight() {

        return 9;
    }

    public int getIconWidth() {

        return 7;
    }

    public void paintIcon(Component c, Graphics g, int x, int y) {

        Color old = g.getColor();
        g.translate(x, y);
        g.setColor(Colors.getUIWindow());
        g.fillRect(0, 0, 7, 9);
        g.setColor(Colors.getUIControlDkShadow());
        int x0 = 1, yy = 1, x1 = x0 + 4;
        g.drawLine(x0, yy, x1, yy);
        yy += 2;
        g.drawLine(x0, yy, x1, yy);
        yy += 2;
        g.drawLine(x0, yy, x1, yy);
        yy += 2;
        g.drawLine(x0, yy, x1, yy);

        g.translate(-x, -y);
        g.setColor(old);

    }

}