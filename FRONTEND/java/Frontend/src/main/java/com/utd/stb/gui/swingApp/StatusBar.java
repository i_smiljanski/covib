package com.utd.stb.gui.swingApp;

import resources.Res;

import javax.swing.*;
import java.text.DateFormat;
import java.text.SimpleDateFormat;


class StatusBar extends com.jidesoft.status.StatusBar {

//    private JPanel theBar;

    private JLabel inactiveTime = new JLabel();

    //	 create in advance so we can set text before Panel is created
    private JLabel multiPurpose = new JLabel();
    private JLabel userAndServer = new JLabel();
    private String userName = "";
    private String serverName = "";

    // --------------------------------------------------------------------

    StatusBar() {

        inactiveTime.setToolTipText("Nur zum Testen: letzte Benutzeraktivitaet vor [mm:ss]");

        userAndServer.setToolTipText(getRes("userAndServer.tooltip"));

        final DateFormat df = new SimpleDateFormat("mm:ss");
//        new Timer( 500, new ActionListener() {
//
//            public void actionPerformed( ActionEvent e ) {
//
//                long lastEventTime = TimeoutManager.getLastEventTime();
//                inactiveTime.setTranslation( df.format( new Date( System.currentTimeMillis()
//                                                           - lastEventTime ) ) );
//            }
//        } ).start();

        // create();

    }

    // --------------------------------------------------------------------

    protected void setServerName(String serverName) {

        this.serverName = serverName;
        updateUserText();
    }

    protected void setUserName(String userName) {

        this.userName = userName;
        updateUserText();
    }

    private void updateUserText() {

        StringBuffer text = new StringBuffer();
        if (userName != null) text.append(userName);
        text.append('@');
        if (serverName != null) text.append(serverName);

        this.userAndServer.setText(text.toString());
    }

//    public JComponent getStatusBar() {
//
//        return theBar;
//    }

//    private void create() {
//
//        if ( theBar != null ) return;
//
//        /*
//         * we removed the userinfo inthe statusbar. But this was IMHO much
//         * better: since i think we will put it in again, just commented out
//         * those parts marked them with '@@@withContent'. Note that the
//         * timeoutinfo will probably not come back, but maybe it does as a
//         * backwards counter
//         */
//
//        FormLayout layout = new FormLayout( "F:pref:G, " + // growing
//                                                           // multipurpose
//                                            //	    		"2dlu,p,2dlu, "+ // separator //
//                                            // @@@withContent
//
//                                            //	    		"L:max(10dlu;pref), " + // timer
//                                            // // @@@withContent
//                                            //	    		"2dlu,p,2dlu, "+ // separator //
//                                            // @@@withContent
//                                            //
//                                            //	    		"L:max(10dlu;pref), " + //
//                                            // user@Server // @@@withContent
//
//                                            "2dlu,R:pref, " + // Grip (note the
//                                                              // space before)
//                                            "", "F:min(16px;pref)" );
//
//        DefaultFormBuilder builder = DialogUtils.getFormBuilder( layout );
//
//        builder.append( multiPurpose );
//
//        //		builder.append(makeSep()); // @@@withContent
//        //		builder.append(inactiveTime); // @@@withContent
//        //
//        //		builder.append(makeSep()); // @@@withContent
//        //		builder.append(userAndServer); // @@@withContent
//
//        builder.append( new WindowGrip() );
//
//        theBar = builder.getPanel();
//        Border border = new EmptyBorder( 0, 1, 1, 0 ); // @@@withContent
//        //		Border border = new EmptyBorder(3,1,1,0); // @@@withContent
//
//        // to much lines?!
//        //		Border border = new CompoundBorder(new EmptyBorder(3,0,0,0),new
//        // Flat3DBorder(true));
//        theBar.setBorder( border );
//    }

    /**
     * A new Separator
     */
    private JComponent makeSep() {

        return new JSeparator(JSeparator.VERTICAL);
    }

    private String getRes(String key) {

        return Res.getString("main.statusbar." + key);
    }

}