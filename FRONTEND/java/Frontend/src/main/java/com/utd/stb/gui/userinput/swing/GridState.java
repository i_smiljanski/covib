package com.utd.stb.gui.userinput.swing;

import com.jidesoft.filter.Filter;
import com.jidesoft.grid.*;
import com.uptodata.isr.gui.util.PropertyTools;
import com.uptodata.isr.gui.awt.tools.WindowState;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.awt.*;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import java.util.Properties;

/**
 * window and grid table properties: the window state, the columns sorting, size and order
 */
public class GridState implements Serializable {
    public static final Logger log = LogManager.getRootLogger();

    private FilterTableFactory filterTableFactory;
    private WindowState windowState = new WindowState();
    public List<String> filterList = new ArrayList<String>();
    public List<String> filterColumnList = new ArrayList<String>();
    public List<String> filterValueList = new ArrayList<String>();
    String sortingOrders;
    String tablePref;


    public void setFilterTableFactory(FilterTableFactory filterTableFactory) {
        this.filterTableFactory = filterTableFactory;
    }

    public WindowState getWindowState() {
        return windowState;
    }


    public void store(Properties p) {
        log.info("begin");
        List<String> filterList = new ArrayList<String>();
        List<Integer> filterColumn = new ArrayList<Integer>();
        List<Object> filterValue = new ArrayList<Object>();

        FilterableTableModel tableModel = filterTableFactory._filterableTableModel;
        SortableTable table = filterTableFactory._table;
        try {
            for (int i = 0; i < tableModel.getFilterItems().size(); i++) {
                IFilterableTableModel.FilterItem iFilterableModel = tableModel.getFilterItems().get(i);
                Filter filter = iFilterableModel.getFilter();
                if (filter instanceof SingleValueFilter) {
                    String filterName = FilterTableFactory.JideFilters.getName(iFilterableModel.getFilter().getClass());
                    filterList.add(filterName);
                    filterColumn.add(iFilterableModel.getColumn());
                    filterValue.add(FilterTableFactory.JideFilters.getFilterValue(filterName, filter));
                }
            }
//            log.debug(filterableTableModel.getFilterItems().size());

            String sortingOrders = TableUtils.getSortableTablePreference(table, true);
            p.setProperty("sortingOrders", sortingOrders);

            String tablePref = TableUtils.getTablePreferenceByName(table);
            p.setProperty("tablePref", tablePref);


            getWindowState().save(filterTableFactory.owner);
            log.debug(filterTableFactory.owner.getBounds());

            PropertyTools.setRectangle(p, "windowState.bounds", windowState.getBounds());
            p.setProperty("windowState", "" + windowState.getState());

            if (filterList.size() > 0 && filterColumn.size() > 0 && filterValue.size() > 0) {
                PropertyTools.setArrayList(p, "filter.filter", filterList);
            }
            PropertyTools.setArrayList(p, "filter.column", filterColumn);
            PropertyTools.setArrayList(p, "filter.value", filterValue);
            log.info("end");
        } catch (Exception e) {
            log.error(e);
        }
    }


    public void load(Properties p) {
        filterList = PropertyTools.getArrayList(p, "filter.filter");
        filterColumnList = PropertyTools.getArrayList(p, "filter.column");
        filterValueList = PropertyTools.getArrayList(p, "filter.value");

        sortingOrders = p.getProperty("sortingOrders");
        tablePref = p.getProperty("tablePref");

        Rectangle r = PropertyTools.getRectangle(p, "windowState.bounds");
        log.debug(r);
        if (r != null) windowState.setBounds(r);

        Integer i = PropertyTools.getInteger(p, "windowState");
        if (i != null) windowState.setState(i);

    }
}
