package com.utd.stb.back.database;

import com.utd.stb.back.data.client.PropertyList;
import com.utd.stb.back.data.client.TreeNodeList;
import com.utd.stb.back.data.client.TreeNodeRecord;
import com.utd.stb.back.ui.impl.userinputs.data.UserInputData;
import com.utd.stb.inter.application.BackendException;
import com.utd.stb.inter.application.DetailView;
import com.utd.stb.inter.application.Node;
import com.utd.stb.inter.application.Nodes;
import com.utd.stb.inter.userinput.UserInputs;

/**
 * implements a DetailView, this is what is shown in detail part of main window
 *
 * @author rainer bruns
 */
public class MenuToDetail implements DetailView {

    private TreeNodeList treeNodes;
    private Dispatcher dispatcher;

    public MenuToDetail(TreeNodeList treeNodes, Dispatcher d) {

        this.treeNodes = treeNodes;
        this.dispatcher = d;

    }

    public String getTitle() {

        // MCD, 26. Nov 2014, use null to create the same view as in the right explorer window
        //TreeNodeRecord node = treeNodes.get(0);
        //return node.getDisplay();
        return null;

    }

    public Nodes getNodes() {

        return treeNodes;
    }

    public UserInputs getNodeProperties(Node node) {

        try {
            TreeNodeRecord nodeRec = (TreeNodeRecord) node;
            PropertyList props = nodeRec.getProperties();
            return new UserInputData(dispatcher, props, nodeRec);
        } catch (BackendException be) {
            return null;
        }
    }

}
