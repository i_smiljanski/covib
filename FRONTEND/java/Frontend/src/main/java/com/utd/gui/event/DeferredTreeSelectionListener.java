package com.utd.gui.event;

import javax.swing.*;
import javax.swing.event.TreeSelectionEvent;
import javax.swing.event.TreeSelectionListener;
import javax.swing.tree.TreePath;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;


/**
 * Inform me later if the user stops selecting around in the tree, <b>only
 * tested for single selection trees </b>, timeout is 800ms for now. We fire
 * only if the selection is changed, if the user walks around for a long time,
 * then only selecting the node that was selected at startup of this long walk
 * nothing is fired. <br>
 * For multiselect trees it maybe ok not to compare the selection for changes,
 * but only to wait for the user to stop selecting.
 *
 * @author PeterBuettner.de
 */
public abstract class DeferredTreeSelectionListener implements TreeSelectionListener {

    private Object lastSelectedNode;
    private JTree tree;
    private static final int TIMEOUT = 800;

    /**
     * single shot timer, restarted on every selection change
     */
    private Timer timer;

    /**
     * this is called if the user stopped walking around with the trees
     * selection
     */
    protected abstract void nodeSelectionChanged();

    /**
     * works with a single tree only
     *
     * @param tree
     */
    public DeferredTreeSelectionListener(JTree tree) {

        this.tree = tree;
        timer = new Timer(TIMEOUT, new ActionListener() {

            public void actionPerformed(ActionEvent e) {

                timerTick();
            }
        });
        timer.setRepeats(false);

    }

    private void timerTick() {

        TreePath path = tree.getSelectionPath();
        Object o = null;
        if (path != null) o = path.getLastPathComponent();

        /* MCD, 23. Nov 2015, ISRC-376
                && !lastSelectedNode.equals( o )*/
        if (lastSelectedNode != null || o != null) {

            lastSelectedNode = o;
            nodeSelectionChanged();

        }

    }

    public void valueChanged(TreeSelectionEvent e) {

        timer.restart();

    }

    /**
     * Causes this to fire a changeevent without delay, only if the current
     * selection is different from the 'last one'
     */
    public void immediateChange() {

        timerTick();

    }

}

