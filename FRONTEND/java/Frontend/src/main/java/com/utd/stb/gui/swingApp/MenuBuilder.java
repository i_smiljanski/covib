package com.utd.stb.gui.swingApp;

import com.utd.stb.inter.action.MenuItem;
import com.utd.stb.inter.action.MenuItems;
import resources.IconSource;
import resources.Res;

import javax.swing.*;
import javax.swing.border.EmptyBorder;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.HashMap;
import java.util.Map;


/**
 * Builds and handles actions out of a {@link com.utd.stb.inter.action.MenuItem}
 * and its submenus
 * <p>
 *
 * @author PeterBuettner.de
 */
abstract class MenuBuilder {

    private MenuItem root;
    private JPopupMenu jRootMenu;

    private ActionListener listener;
    private Map actionMap;
    private IconSource iconSource;

    public MenuBuilder(MenuItem root, IconSource iconSource) {

        this.root = root;
        this.iconSource = iconSource;
        actionMap = new HashMap();

        listener = new ActionListener() {

            public void actionPerformed(ActionEvent e) {

                ActionEvent ae = new ActionEvent(this,
                        e.getID(),
                        e.getActionCommand(),
                        e.getWhen(),
                        e.getModifiers());
                MenuBuilder.this.actionPerformed(ae, (MenuItem) actionMap.get(e.getSource()));
            }
        };
    }

    /**
     * builds and adds item and subitems recursively
     */
    private void buildItem(MenuItem item, JComponent jParent) {

        MenuItems subs = item.getChildren();

        if (item.isSeparator()) // separator
            jParent.add(new JPopupMenu.Separator());

        else if (subs == null) // standard item
            jParent.add(buildItem(item));

        else { //popup item
            JMenu jPop = (JMenu) jParent.add(buildPop(item));
            for (int i = 0; i < subs.getSize(); i++)
                buildItem(subs.get(i), jPop);
        }

    }

    /**
     * popup item non recursive
     */
    private JMenu buildPop(MenuItem item) {

        JMenu pop = new JMenu(item.getText());
        pop.setEnabled(item.isEnabled());
        pop.setIcon(iconSource.getSmallIconByID(item.getIconId()));
        return pop;
    }// -------------------------------

    /**
     * standard item
     */
    private JMenuItem buildItem(MenuItem item) {

        JMenuItem jItem = new JMenuItem(item.getText(), iconSource.getSmallIconByID(item.getIconId()));
        boolean enabled = item.isEnabled();
        jItem.setEnabled(enabled);
        if (enabled) actionMap.put(jItem, item);

        jItem.addActionListener(listener);
        return jItem;
    }// -------------------------------

    /**
     * returns the menu (a popup one), always the same for this instance, lazy
     * created on first access
     *
     * @return
     */
    JPopupMenu getPopUpMenu() {

        if (jRootMenu == null) jRootMenu = createPopUpMenu(root);
        return jRootMenu;
    }

    /**
     * creates the menu (a popup one), always a new one
     *
     * @return
     */
    private JPopupMenu createPopUpMenu(MenuItem root) {

        String label = root.getText();
        JPopupMenu menu = new JPopupMenu(label);
        addMenuTitle(menu, label);
        addItems(menu, root);
        return menu;
    }

    /**
     * creates a menu to be used in a JMenuBar, always a new one
     *
     * @return
     */
    JMenu createMenu() {

        JMenu menu = new JMenu(root.getText());
        menu.setEnabled(root.isEnabled());
        menu.setIcon(iconSource.getSmallIconByID(root.getIconId()));
        addItems(menu, root);
        return menu;
    }

    /**
     * adds all items defined by root to the menucomponent (JMenu, JPopupMenu),
     * recursively. If root has no children a disabled item with text from the
     * resources is used.
     *
     * @param menu
     * @param root
     */
    private void addItems(JComponent menu, MenuItem root) {

        MenuItems subs = root.getChildren();
        if (subs == null || subs.getSize() == 0) {
            String text = Res.getString("general.menu.emptyMenu.label.text");
            menu.add(new JMenuItem(text)).setEnabled(false);
        } else
            for (int i = 0; i < subs.getSize(); i++)
                buildItem(subs.get(i), menu);
    }

    /**
     * may add a title to a popupMenu, non Win L&F may do this per default, so
     * remove this, if you don't like it overwrite it to do nothing
     *
     * @param menu  add the title here
     * @param label maybe null or empty
     */
    protected void addMenuTitle(JPopupMenu menu, String label) {

        if (label != null) {
            //		GradientTitle title = new GradientTitle(label);
            //		title.setBorder(new EmptyBorder(2,2,2,2));

            JLabel title = new JLabel(label) {

                public Dimension getPreferredSize() {// set a 'max' size

                    Dimension d = super.getPreferredSize();
                    if (d != null) d.width = 40;
                    return d;
                }
            };
            title.setToolTipText(label);
            title.setBorder(new EmptyBorder(1, 3, 1, 3));
            //		title.setBackground(Colors.getUIControlShadow());
            //		title.setBackground(Colors.getUIWindow());
            title.setFont(title.getFont().deriveFont(Font.ITALIC));

            //		title.setBackground(Colors.getUIInactiveCaption());
            //		title.setForeground(Colors.getUIInactiveCaptionText());
            //		title.setBackground(Colors.getUIActiveCaption());
            title.setOpaque(true);

            JPanel p = new JPanel(new BorderLayout()); // stretch
            p.add(title);
            menu.add(p);
            menu.add(new JPopupMenu.Separator());
        }
    }// ----------------------------------------------------------

    /**
     * gets called if the user chooses an item
     *
     * @param e    source is this instance
     * @param item the MenuItem defining ths JMenuItem
     */
    public abstract void actionPerformed(ActionEvent e, MenuItem item);
}