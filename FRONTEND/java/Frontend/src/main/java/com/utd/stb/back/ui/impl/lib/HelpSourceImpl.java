package com.utd.stb.back.ui.impl.lib;

import com.utd.stb.inter.dialog.HelpSource;


/**
 * Holds the help id for an object in ISR
 *
 * @author rainer bruns Created 20.12.2004
 */
public class HelpSourceImpl implements HelpSource {

    String helpId;
    String identifier;

    public HelpSourceImpl(String id) {

        this.helpId = id.replaceAll("_", "").replaceAll("\036", "").toLowerCase() + ".htm";
        this.identifier = id;

    }

    public String getHelpId() {
        return helpId;
    }

    public String getIdentifier() {
        return identifier;
    }

}