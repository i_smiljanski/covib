package com.utd.stb.back.data.client;

import java.awt.*;
import java.util.ArrayList;
import java.util.Iterator;

/**
 * Wraps a list of record data of type SelectionRecord
 *
 * @author rainer bruns Created 27.08.2004
 * @see SelectionRecord
 */
public class AttributeList {

    private ArrayList data = new ArrayList();
    private Iterator iterator;

    /**
     * Creates an empty AttributeList
     */
    public AttributeList() {
    }

    /**
     * adds all records of in to this list
     *
     * @param in the SelectionList to add to this list
     */
    public void addAll(AttributeList in) {

        in.initIterator();
        while (in.hasNext()) {
            data.add(in.getNext());
        }
    }

    /**
     * adds the input record to this list
     *
     * @param attrRec record to add
     */
    public void add(AttributeRecord attrRec) {

        data.add(attrRec);
    }

    /**
     * sets list iterator to the beginning
     */
    public void initIterator() {

        iterator = data.iterator();
    }

    /**
     * @return true if there is a next record
     */
    public boolean hasNext() {

        return iterator.hasNext();
    }

    /**
     * @return the next AttributeRecord
     */
    public AttributeRecord getNext() {

        return (AttributeRecord) iterator.next();
    }

    /**
     * @return the size of the attribute list
     */
    public int getSize() {

        return data.size();
    }

    /**
     * @return the color saved in the attributeList
     */
    public Color getColor() {

        initIterator();
        while (hasNext()) {
            AttributeRecord rec = getNext();
            if (rec.getParameter().equals("COLOR") &&
                    !rec.getPrepresent().equals("V")) {

                return new Color(Integer.parseInt(rec.getValue(), 16));
            }
        }
        return null;
    }

    /**
     * @return the font saved in the attributeList
     */
    public Font getHighlighted() {

        initIterator();
        while (hasNext()) {

            AttributeRecord rec = getNext();
            if (rec.getParameter().equals("HIGHLIGHT") &&
                    rec.getValue().equals("T")) {

                if (rec.getPrepresent().equals("B")) {
                    return new Font("MS Sans Serif", Font.BOLD, 11);
                } else if (rec.getPrepresent().equals("I")) {
                    return new Font("MS Sans Serif", Font.ITALIC, 11);
                } else {
                    return new Font("MS Sans Serif", Font.PLAIN, 11);
                }
            }
        }
        return null;
    }

    /**
     * @return the icon saved in the attributeList
     */
    public String getIcon() {

        //IconSource is = new IconSource();
        initIterator();
        while (hasNext()) {

            AttributeRecord rec = getNext();
            if (rec.getParameter().equals("ICON") &&
                    !rec.getPrepresent().equals("V")) {

                return rec.getValue();//is.getSmallIconByID( rec.getValue() );
            }
        }
        return null;
    }

    /**
     * @return the value of editable saved in the attributeList
     */
    public boolean isEditable() {
//        System.out.println("isEditable: " + Thread.currentThread().getStackTrace()[2]);
        initIterator();
        while (hasNext()) {
            AttributeRecord rec = getNext();
            if (rec.getParameter().equals("EDITABLE") && !rec.getPrepresent().equals("V")) {
//                System.out.println("isEditable:rec.getValue() = " + rec.getValue());
                return rec.getValue().equals("T");
            }
        }
        return false;
    }

    public boolean isChangeable() {

        initIterator();
        while (hasNext()) {

            AttributeRecord rec = getNext();
            System.out.println("isChangeable: parameter = " + rec.getParameter() + "; prepresent: " + rec.getPrepresent() + "; value = " + rec.getValue());
            if (rec.getParameter().equals("CHANGEABLE") && !rec.getPrepresent().equals("V")) {
                System.out.println("isChangeable:rec.getValue() = " + rec.getValue());
                return rec.getValue().equals("T");
            }
        }
        return false;
    }

    /**
     * @return the value of editable saved in the attributeList
     */
    public String getColValue(String colName) {

        initIterator();
        while (hasNext()) {

            AttributeRecord rec = getNext();
            if (rec.getParameter().equals(colName) &&
                    rec.getPrepresent().equals("V")) {
                return rec.getValue();
            }
        }
        return "";
    }
}