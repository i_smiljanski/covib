package com.utd.stb.gui.wizzard.choosers;

import com.jgoodies.forms.builder.DefaultFormBuilder;
import com.jgoodies.forms.layout.CellConstraints;
import com.jgoodies.forms.layout.FormLayout;
import com.jidesoft.grid.CellRendererManager;
import com.jidesoft.grid.QuickTableFilterField;
import com.jidesoft.grid.SortableTable;
import com.jidesoft.grid.TableUtils;
import com.utd.gui.controls.BorderSplitPane;
import com.utd.gui.controls.ControlUtils;
import com.utd.gui.controls.table.ListViewTable;
import com.utd.gui.controls.table.TableTools;
import com.utd.gui.event.DocumentDeferredChangeAdapter;
import com.utd.gui.filter.FilterErrorMsgDisplay;
import com.utd.gui.filter.RegExpFilter;
import com.uptodata.isr.gui.util.Colors;
import com.utd.gui.util.DialogUtils;
import com.utd.stb.gui.itemTools.DisplayableItemTableCellRenderer;
import com.utd.stb.gui.itemTools.DisplayableItemsImplUnmodifiable;
import com.utd.stb.gui.itemTools.DisplayableItemsTableModel;
import com.utd.stb.gui.swingApp.exceptions.ExceptionHandler;
import com.utd.stb.inter.application.BackendException;
import com.utd.stb.inter.items.DisplayableItem;
import com.utd.stb.inter.items.DisplayableItems;
import com.utd.stb.inter.wizzard.formtypes.FormDataOneOutOfList;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import javax.swing.*;
import javax.swing.event.DocumentEvent;
import javax.swing.event.ListSelectionEvent;
import javax.swing.event.ListSelectionListener;
import java.awt.*;
import java.util.Collections;


/**
 * Swing gui to let a user edit FormDataOneOutOfList.
 *
 * @author PeterBuettner.de
 */
class SingleItemChooser extends AbstractChooser {
    public static final Logger log = LogManager.getRootLogger();

    private FormDataOneOutOfList data;

    private JTable tblChoose;
    // private JTextField                   filterField   = new JTextField();
    private QuickTableFilterField quickFilter;

    private FilterErrorMsgDisplay errorMsgLabel = new FilterErrorMsgDisplay();

    /**
     * indicates if the items are already loaded
     */
    private boolean itemModelFilled;
    //  private FilterTableModel             filterModel;
    private MyDisplayableItemsTableModel itemModel;

    private final RegExpFilter regExpFilter = new RegExpFilter(null);

    /**
     * only package access for now
     *
     * @param formDataOneOutOfList
     */
    SingleItemChooser(ExceptionHandler exceptionHandler,
                      FormDataOneOutOfList formDataOneOutOfList) {

        super(exceptionHandler);
        this.data = formDataOneOutOfList;
    }

    public boolean isValid() {
        // all is valid
        return data.getDisplayableItemComboList().isEmptySelectionAllowed() || getSelectedItem() != null;
    }

    public void saveData() throws BackendException {

        data.getDisplayableItemComboList().setSelectedItem(getSelectedItem());
        data.saveData();
    }

    /**
     * the current selected item in the table, maybe null
     *
     * @return
     */
    private DisplayableItem getSelectedItem() {

        int sel = tblChoose.getSelectedRow();
        if (sel == -1) return null;
        DisplayableItem item = (DisplayableItem) tblChoose.getModel().getValueAt(sel, -1);
        return item;
    }

    public JComponent getContent() {

        final DisplayableItem selected = data.getDisplayableItemComboList().getSelectedItem();
        // ... create table
        DisplayableItems modelItems = null;
        /*try {
            modelItems = data.getDisplayableItemComboList().getAvailableItems();
        } catch ( BackendException e ) {
            exceptionHandler.exceptionHandler( e, false );
        }*/
        if (selected != null) {// show only selected, fill list on demand
            //filterField.setText(selected.getData().toString());
            //filterField.selectAll();
            try {
                modelItems = new DisplayableItemsImplUnmodifiable(Collections.singletonList(selected),
                        data.getDisplayableItemComboList().getAvailableItems());
            } catch (BackendException e) {
                modelItems = new DisplayableItemsImplUnmodifiable(Collections.singletonList(selected),
                        null);
            }
            // no selection, get whole list  
        } else {
            try {
                modelItems = data.getDisplayableItemComboList().getAvailableItems();
            } catch (BackendException e) {
                exceptionHandler.exceptionHandler(e, false);
            }
        }
        itemModel = new MyDisplayableItemsTableModel(modelItems);

        //filterModel = new FilterTableModel( itemModel, regExpFilter, 0 );

        //tblChoose = new ListViewTable( filterModel );
        tblChoose = new ListViewTable(itemModel);
        ((SortableTable) tblChoose).setSortable(false);
        tblChoose.setDefaultRenderer(Object.class, new DisplayableItemTableCellRenderer());
        CellRendererManager.registerRenderer(Object.class, new DisplayableItemTableCellRenderer());
        tblChoose.setSelectionMode(ListSelectionModel.SINGLE_SELECTION);

        final Color validTableColor = tblChoose.getBackground();

        tblChoose.getSelectionModel().addListSelectionListener(new ListSelectionListener() {

            public void valueChanged(ListSelectionEvent e) {

                if (e.getValueIsAdjusting()) return;
                fireValidationChanged();

                JScrollPane sp = ControlUtils.getEnclosingScrollPane(tblChoose);
                if (sp == null) return;
                if (getSelectedItem() == null) TableTools.selectFirstRow(tblChoose, true);
                Color c = isValid() ? validTableColor : Colors.getDataBackIllegalEmpty();
                sp.getViewport().setBackground(c);
                tblChoose.setBackground(c);
            }
        });

        // connect add the listener after setTranslation, so DocChange isn't fired
       /* filterField.getDocument().addDocumentListener( new DocumentDeferredChangeAdapter() {

            public void deferredChange( DocumentEvent e ) {

                filterStringChange();
            }
        } );
        // make up,down,... in textfield => change selection in list
        RemoteNavigatonTool.putAllActions( filterField, tblChoose );*/

        // select first in table if
        if (selected != null) TableTools.selectFirstRow(tblChoose, true);
        //TableTools.autoSizeColumns( tblChoose, false );

        quickFilter = new QuickTableFilterField(tblChoose.getModel());
        tblChoose.setModel(quickFilter.getDisplayTableModel());
        quickFilter.setTable(tblChoose);

        if (selected != null) quickFilter.setText(selected.getData().toString());

        quickFilter.getTextField().getDocument().addDocumentListener(new DocumentDeferredChangeAdapter() {

            public void deferredChange(DocumentEvent e) {

                filterStringChange();
            }
        });


        TableUtils.autoResizeAllColumns(tblChoose);

        return getComponentPanel();
    }

    /**
     * Builds Panel &amp; Layout, collects toolbars etc. Preconditions: actions,
     * components (tables) build
     *
     * @return
     */
    private JComponent getComponentPanel() {

        //                                           lbl space tbl
        FormLayout layout = new FormLayout("min:G", "P, 2dlu, F:min:G");
        DefaultFormBuilder builder = DialogUtils.getFormBuilder(layout);

        builder.appendRelatedComponentsGapRow(); // spacer
        builder.appendRow("P"); // filter

        CellConstraints cc = new CellConstraints();
        //   builder.add( FilterPanelBuilder.getInstance().build( errorMsgLabel, filterField ), cc.xy( 1, 5 ) );
        builder.add(quickFilter, cc.xy(1, 5));
        builder.add(buildComponentLabel(data.getLabel(), tblChoose, 0), cc.xy(1, 1));
        BorderSplitPane sp = new BorderSplitPane(new JScrollPane(tblChoose), null, false);
        builder.add(sp, cc.xy(1, 3));

        return builder.getPanel();
    }

    private void filterStringChange() {

        if (!itemModelFilled) {
            DisplayableItems availableItems = null;
            try {
                availableItems = data.getDisplayableItemComboList().getAvailableItems();
            } catch (BackendException e) {
                exceptionHandler.exceptionHandler(e, false);
                return;
            }
            itemModel.setItems(availableItems);
            itemModelFilled = true;
            TableTools.autoSizeColumns(tblChoose, false);// set new since much
            // more data
        }

        // LOV
//        regExpFilter.setPattern( errorMsgLabel.createPattern( filterField.getText() ) );
//        if ( filterField.getDocument().getLength() != 0 )
        // select first only if empty
        TableTools.selectFirstRow(tblChoose, true);
    }

    /* (non-Javadoc)
     * @see com.utd.stb.gui.wizzard.choosers.AbstractChooser#getFilterFieldValue()
     */
    public String getFilterFieldValue() {
        return quickFilter.getText();
    }

    /* (non-Javadoc)
     * @see com.utd.stb.gui.wizzard.choosers.AbstractChooser#setFilterFieldValue()
     */
    public void setFilterFieldValue(String regExpr) {
        quickFilter.setText(regExpr);
    }

    private static final class MyDisplayableItemsTableModel extends DisplayableItemsTableModel {

        public MyDisplayableItemsTableModel(DisplayableItems items) {

            super(items);
        }

        public void setItems(DisplayableItems items) {
            this.items = items;
            fireTableDataChanged();
        }

    }

}