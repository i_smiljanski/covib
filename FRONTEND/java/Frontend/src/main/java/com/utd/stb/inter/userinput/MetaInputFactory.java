package com.utd.stb.inter.userinput;

/**
 * Conveniance class to create MetaInfos
 *
 * @author PeterBuettner.de
 */
public class MetaInputFactory {

    private MetaInputFactory() {

    }

    /**
     * see {@link MetaInput.TABSHEET}
     */
    static public MetaInput getTabSheet() {

        return new TabSheet();
    }

    /**
     * see {@link MetaInput.GROUP}
     */
    static public MetaInput getGroup() {

        return new Group();
    }

    /**
     * see {@link MetaInput.FLOATING_TEXT}
     */
    static public MetaInput getFloatingText() {

        return new FloatingText();
    }

    /**
     * see {@link MetaInput.HTML_TEXT}
     */
    static public MetaInput getHtmlText() {

        return new HtmlText();
    }

    private static final class TabSheet implements MetaInput {

        public String getType() {

            return MetaInput.TABSHEET;
        }
    }

    private static final class Group implements MetaInput {

        public String getType() {

            return MetaInput.GROUP;
        }
    }

    private static final class FloatingText implements MetaInput {

        public String getType() {

            return MetaInput.FLOATING_TEXT;
        }
    }

    private static final class HtmlText implements MetaInput {

        public String getType() {

            return MetaInput.HTML_TEXT;
        }
    }

}