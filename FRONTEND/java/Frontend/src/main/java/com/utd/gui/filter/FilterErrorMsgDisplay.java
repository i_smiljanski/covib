package com.utd.gui.filter;

import com.utd.gui.resources.I18N;
import com.uptodata.isr.gui.util.Colors;
import resources.Res;

import javax.swing.*;
import java.awt.*;
import java.util.Arrays;
import java.util.regex.Pattern;
import java.util.regex.PatternSyntaxException;


/**
 * Q&amp;D thingy to display an errormessage for Filters, enhance later (more
 * options...)
 *
 * @author PeterBuettner.de
 *         <p>
 *         TODOres remove direct image refs
 */
public class FilterErrorMsgDisplay {

    private JLabel label = new JLabel();

    /**
     * if not null will be shown on no error
     */
    private String defaultMessage;
    private String defaultTooltip;
    private boolean isError;

    public JLabel getComponent() {

        return label;

    }

    public FilterErrorMsgDisplay() {

        this(null);

    }

    /**
     * @param defaultMessage if not null will be shown on no error
     */
    public FilterErrorMsgDisplay(String defaultMessage) {

        this.defaultMessage = defaultMessage;
        label.setHorizontalAlignment(JLabel.TRAILING);
        label.setHorizontalTextPosition(JLabel.LEFT);
        clearErrMsg();

    }

    private Icon icOk = Res.getII("filter.png");
    private Icon icErr = Res.getII("info.png");

    private void setErrMsg(String msg, String extendedMsg) {

        if (msg == null || msg.length() == 0) {

            clearErrMsg();
            return;

        }

        label.setForeground(Color.RED);
        label.setText(msg);
        label.setIcon(icErr);

        if (extendedMsg != null && extendedMsg.length() > 0)

            label.setToolTipText("<html><body><pre>" + extendedMsg);

        else

            label.setToolTipText(null);

        isError = true;

    }

    private void clearErrMsg() {

        label.setForeground(Colors.getUIControlText());
        label.setText(defaultMessage);
        label.setToolTipText(defaultTooltip);
        // setTranslation("RegEx Filter");
        label.setIcon(icOk);
        label.setToolTipText(null);
        isError = false;

    }

    /**
     * Create the RegExp Pattern, sets errormessages if necessary, see see
     * {@link createPattern(String, int )}where we use Pattern.CASE_INSENSITIVE
     * as flags.
     *
     * @param regex
     * @return
     */

    public Pattern createPattern(String filter) {

        return createPattern(filter, Pattern.CASE_INSENSITIVE);

    }

    /**
     * Create the RegExp Pattern, sets errormessages if necessary. Returns null
     * on null-input or error. If regex starts with "r:" or "R:" it is seen as a
     * regex, otherwise it will be seen as 'normal Text' and a escaped string is
     * created. Parameters: see {@link Pattern.compile(String, int )}
     *
     * @param regex
     * @param flags
     * @return
     */
    public Pattern createPattern(String regex, int flags) {

        clearErrMsg();

        try {

            if (regex == null || regex.length() == 0) return null;

            if (!regex.startsWith("R:") && !regex.startsWith("r:")) {

                regex = escapeRE(regex);

            } else {

                if (regex.length() <= 2) return null; // empty regex
                regex = regex.substring(2);

            }

            return Pattern.compile(regex, flags | Pattern.UNICODE_CASE);

        } catch (PatternSyntaxException pse) {

            setErrMsg(I18N.getString(getClass(), "errorInPattern"), pse
                    .getLocalizedMessage());

        } catch (IllegalArgumentException iae) {

            setErrMsg(I18N.getString(getClass(), "errorInPattern"), iae
                    .getLocalizedMessage());

        }

        return null;

    }

    /**
     * if not null will be shown on no error
     */
    public String getDefaultMessage() {

        return defaultMessage;
    }

    /**
     * if not null will be shown on no error, is updated immediately if
     * currently no error is shown
     */
    public void setDefaultMessage(String defaultMessage) {

        this.defaultMessage = defaultMessage;
        if (!isError) // update Label if normal msg is shown
            label.setText(defaultMessage);

    }

    /**
     * if not null will be shown on no error
     */
    public String getDefaultTooltip() {

        return defaultTooltip;

    }

    /**
     * if not null will be shown on no error, is updated immediately if
     * currently no error is shown
     */
    public void setDefaultTooltip(String defaultTooltip) {

        this.defaultTooltip = defaultTooltip;
        if (!isError) // update Label if normal msg is shown
            label.setToolTipText(defaultTooltip);

    }

    /**
     * sets message and tooltip together
     */
    public void setDefaultMessage(String defaultMessage, String defaultTooltip) {

        setDefaultMessage(defaultMessage);
        setDefaultTooltip(defaultTooltip);

    }

    /**
     * Returns a pattern where all regex chars are escaped.
     */
    protected static String escapeRE(String str) {

        StringBuffer sb = new StringBuffer(str.length() * 110 / 100);
        char[] in = str.toCharArray();
        for (int c = 0; c < in.length; c++) {

            // if(sRegExChars.indexOf(in[c])>=0)
            if (Arrays.binarySearch(regExChars, in[c]) >= 0)

                sb.append('\\'); // escape!
            sb.append(in[c]);

        }

        return sb.toString();

    }

    private static final String sRegExChars = "().+\\[]*?^$|{}";         // commons
    // first
    private static final char[] regExChars = sRegExChars.toCharArray();

    static {
        Arrays.sort(regExChars);
    }

}

