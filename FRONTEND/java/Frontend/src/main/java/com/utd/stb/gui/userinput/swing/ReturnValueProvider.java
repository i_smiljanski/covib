package com.utd.stb.gui.userinput.swing;

/**
 * Used for modal dialogs to provide a return value, maybee usefull elsewhere
 *
 * @author PeterBuettner.de
 */
public interface ReturnValueProvider {

    /**
     * Used for modal dialogs to provide a return value, maybee usefull
     * elsewhere
     *
     * @return
     */
    Object getReturnValue();

}