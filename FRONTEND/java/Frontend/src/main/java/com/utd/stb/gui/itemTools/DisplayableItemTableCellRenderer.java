package com.utd.stb.gui.itemTools;

import com.utd.gui.controls.table.ListViewTableCellRenderer;
import com.utd.stb.inter.items.DisplayableItem;

import javax.swing.*;
import java.awt.*;


/**
 * A renderer for DisplayableItem, cares <b>only </b> about rich html tooltips,
 * uses data/info to create a html Tooltip for every cell, the datadisplay in
 * the cell is unchanged from the 'parent' renderer:
 * {@link ListViewTableCellRenderer}
 *
 * @author PeterBuettner.de
 */
public class DisplayableItemTableCellRenderer extends ListViewTableCellRenderer {

    DisplayableItemToolTipMaker ttm = new DisplayableItemToolTipMaker();

    public DisplayableItemTableCellRenderer() {
    }


    public Component getTableCellRendererComponent(JTable table, Object value,
                                                   boolean isSelected, boolean hasFocus,
                                                   int row, int column) {
        // MCD, 13.Apr 2006, MERKEN
        if (value instanceof DisplayableItem) {
            ttm.extractData(table.getModel(),
                    column == 0 ? ((DisplayableItem) value).getData() : ((DisplayableItem) value).getInfo(),
                    row);
        } else {
            ttm.extractData(table.getModel(), value, row);
        }
        return super.getTableCellRendererComponent(table,
                value,
                isSelected,
                hasFocus,
                row,
                column);
    }


    // use this since setToolTipText() calls getToolTipText -> lot of unused junk
    public String getToolTipText() {
        return ttm.getToolTipText();
    }

}