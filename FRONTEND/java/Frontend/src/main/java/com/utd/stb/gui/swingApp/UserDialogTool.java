package com.utd.stb.gui.swingApp;

import com.jidesoft.alert.Alert;
import com.utd.stb.back.ui.impl.msgboxes.AlertMsgBox;
import com.utd.stb.back.ui.impl.msgboxes.SuccessMsgBox;
import com.utd.stb.back.ui.impl.msgboxes.WarningMsgBox;
import com.utd.stb.gui.userinput.swing.DialogFactory;
import com.utd.stb.gui.userinput.swing.ReturnValueProvider;
import com.utd.stb.inter.dialog.UserDialog;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import javax.swing.*;
import java.awt.*;


/**
 * Just to reuse some code
 *
 * @author PeterBuettner.de
 */
public class UserDialogTool {
    public static final Logger log = LogManager.getRootLogger();

    public static Object executeUserDialog(JFrame parent, UserDialog userDialog) {
        return executeUserDialog(parent, userDialog, null);
    }

    public static Object executeUserDialog(JFrame parent, UserDialog userDialog, Rectangle bounds) {
        DialogFactory.modal = (bounds == null);
        //is, 13.Dec 2010
        if (userDialog instanceof AlertMsgBox)
            return executeInternal(DialogFactory.createAlert(parent, userDialog),
                    userDialog);
        else {
            return executeInternal(parent, DialogFactory.createDialog(parent, userDialog),
                    userDialog, bounds);
        }
    }

    public static Object executeUserDialog(JDialog parent, UserDialog userDialog) {
        DialogFactory.modal = true;
        if (userDialog instanceof AlertMsgBox)
            return executeInternal(DialogFactory.createAlert(parent, userDialog),
                    userDialog);
        else {
            return executeInternal(parent, DialogFactory.createDialog(parent, userDialog),
                    userDialog, null);
        }
    }


    private static Object executeInternal(Window parent, final JDialog dlg, UserDialog userDialog, Rectangle bounds) {
        int timeout1;

        // MCD 12.07.2005 get the timeout that the box is closes
        final int timeout = userDialog instanceof WarningMsgBox ?
                ((WarningMsgBox) (userDialog)).getTimeout() :
                userDialog instanceof SuccessMsgBox ?
                        ((SuccessMsgBox) (userDialog)).getTimeout() :
                        0;
        TimeoutManager.addSizeMoveListener(dlg);

        // MCD 12.07.2005 to close a dialog it must not be modal and dispose_on_close
        if (timeout != 0) {
            // dlg.setModal(false);
            dlg.setDefaultCloseOperation(JDialog.DO_NOTHING_ON_CLOSE);
        }

        dlg.pack();

        if (dlg.getTitle() == null)
            if (parent instanceof JDialog) {
                dlg.setTitle(((JDialog) parent).getTitle());

            } else if (parent instanceof JFrame)
                dlg.setTitle(((JFrame) parent).getTitle());


        dlg.setLocationRelativeTo(parent);
        if (bounds != null) dlg.setLocation(bounds.getLocation());
        if (timeout == 0) dlg.setVisible(true);

        // MCD 12.07.2005 only if the timeout is not 0 the box is closed
        if (timeout != 0) {
            // MCD 30.Nov 2005 it has to run in a thread not in a timer because a timer never ends
//            Thread closeDialog = new Thread() {
//                public void run() {
//                    try {
//                        Thread.sleep(timeout * 1000);
//
//                       // if (dlg.isVisible()) dlg.dispose();
//                    } catch (InterruptedException e) {log.debug(e);
//                    }
//                }
//            };
//            closeDialog.start();

            Thread t = new Thread(new Runnable() {
                public void run() {
                    dlg.setVisible(true);
                }
            });
            t.start();

            SwingUtilities.invokeLater(new Runnable() {
                public void run() {
                    try {
                        Thread.sleep(timeout * 1000);
                        dlg.setVisible(false);
                    } catch (InterruptedException e) {
                        log.error(e);
                    }
                }
            });


        }

        if (dlg instanceof ReturnValueProvider)
            return ((ReturnValueProvider) dlg).getReturnValue();

        return null;
    }


    private static Object executeInternal(Alert alert, UserDialog userDialog) {
        final int timeout = ((AlertMsgBox) userDialog).getTimeout();
        log.debug("timeout = " + timeout);
        alert.setTimeout(timeout);
        alert.showPopup(SwingConstants.SOUTH_EAST);
        log.debug("end");
        return null;
    }
}