package com.utd.stb.gui.itemTools;

import com.utd.gui.filter.FilterTableModel;
import com.uptodata.isr.gui.util.HtmlText;
import com.utd.stb.back.data.client.UserInputRecord;
import com.utd.stb.inter.items.DisplayableItem;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import javax.swing.*;
import javax.swing.table.TableModel;
import java.awt.*;


/**
 * A Tooltip for a DisplayableItem has a special look, here it is created
 * centraly, in JTable it may also use column 0+1, cares about rich html
 * tooltips, uses data/info to create a html Tooltip.
 * <p>
 * <p>
 * Usage: <code>
 * maker.extractData(model, value, row);
 * <p>
 * <p>
 * 'later' when needed (uses more CPU, so only on demand)
 * public String getToolTipText() {
 * return maker.getToolTipText();
 * }
 * <p>
 * </code>
 *
 * @author PeterBuettner.de
 */
public class DisplayableItemToolTipMaker {
    public static final Logger log = LogManager.getRootLogger();

    private String text;
    private String info;

    /**
     * extract the data to be shown in the tooltip
     * <ol>
     * <li>if value is DisplayableItem this is used
     * <li>if model is DisplayableItemsTableModel or a FilterTableModel with
     * DisplayableItemsTableModel as base use this (+ row)
     * <li>as a last chance use the first two columns as text/info (+ row)
     * </ol>
     *
     * @param model
     * @param value
     * @param row
     */
    public void extractData(TableModel model, Object value, int row) {

        if (value instanceof DisplayableItem) {
            extractData((DisplayableItem) value);
            return;
        }

        text = null;
        info = null;

        // tunnel through the filteredModel and get the DisplayableItem
        boolean hasDisplItems = (model instanceof DisplayableItemsTableModel)
                || (model instanceof FilterTableModel && ((FilterTableModel) model)
                .getModel() instanceof DisplayableItemsTableModel);
        // note: dont use the base model from the filtered, since the rows are
        // mapped
        // and we would get the wrong data.
        DisplayableItem item = null;
        if (hasDisplItems) {
            item = (DisplayableItem) model.getValueAt(row, -1);
            extractData(item);
        }
        //	try harder
        if (item == null && model.getColumnCount() > 1) {
            Object textObject = model.getValueAt(row, 0);
            if (textObject instanceof UserInputRecord)
                textObject = ((UserInputRecord) textObject).getData();
            text = HtmlText.toHtml(textObject);
            Object infoObject = model.getValueAt(row, 1);
//            log.debug(infoObject);
            if (infoObject instanceof UserInputRecord)
                infoObject = ((UserInputRecord) infoObject).getInfo();
            info = HtmlText.toHtml(infoObject);
        }
    }

    /**
     * if you want to diplay the tooltip in another place - no table or you know
     * there is only a DisplayableItem - use this.
     *
     * @param item if null both text/info is set to an empty String -&gt; ""
     */
    public void extractData(DisplayableItem item) {
        if (item != null) {
            text = HtmlText.toHtml(item.getData());
            info = HtmlText.toHtml(item.getInfo());
        } else
            text = info = "";
    }


    // use this since setToolTipText() calls getToolTipText -> lot of unused
    // junk
    private JToolTip tt = new JToolTip();
    private static final int maxToolTipWidth = 200;           // in pixels
    // TODOlater
    // remove
    // hardcoded
    // tooltip width,
    // use a parameter
    // or get a
    // usefull value
    // from the font
    // of JToolTip and
    // set char-width

    public String getToolTipText() {
        String toolTipText = "<html><body style='padding:2px;'>" + "<b>" + text + "</b><br>"
                + info;

        // test, if to wide set a defined size
        tt.setTipText(toolTipText);
        Dimension d = tt.getPreferredSize();
        if (d.width > maxToolTipWidth)
            toolTipText = "<html><body style='padding:2px;width:"
                    + maxToolTipWidth + "px;'>" + "<b>"
                    + text + "</b><br>" + info;

        return toolTipText;
    }
}