package com.utd.gui.event;

import javax.swing.*;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;


/**
 * A MouseListner firing only on Jables, on PopupTrigger, and if the
 * pressed-event occured on the same row as the popup-event, this row is
 * selected before doPopup
 *
 * @author PeterBuettner.de
 */

public abstract class TablePopupListener extends MouseAdapter {

    static private final JPopupMenu triggerTester = new JPopupMenu("xxx");
    private int lastDownRow;

    private void event(MouseEvent e) {

        if (!(e.getComponent() instanceof JTable)) return;
        JTable table = (JTable) e.getSource();

        int row = table.rowAtPoint(e.getPoint());
        int col = table.columnAtPoint(e.getPoint());

        if (row == -1 || col == -1) return;

        // check for down/up on same node
        if (e.getID() == MouseEvent.MOUSE_PRESSED) lastDownRow = row;

        if (lastDownRow != row) {
            lastDownRow = -1; // reset on miss
            return;
        }
        table.setRowSelectionInterval(row, row);

        if (!triggerTester.isPopupTrigger(e)) return;

        doPopUp(row, col, e);

    }

    public void mousePressed(MouseEvent e) {

        event(e);
    }

    public void mouseReleased(MouseEvent e) {

        event(e);
    }

    /**
     * fires on PopupTrigger, and if the pressed-event occured on the same row
     * as the popup-event, the row is selected before doPopup col maybe
     * different between press and popup
     *
     * @param row
     * @param col
     * @param e
     */
    public abstract void doPopUp(int row, int col, MouseEvent e);

}