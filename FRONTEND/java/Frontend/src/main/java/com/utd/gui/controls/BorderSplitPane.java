/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.utd.gui.controls;

import com.jidesoft.swing.JideSwingUtilities;

import javax.swing.*;
import javax.swing.border.MatteBorder;
import java.awt.*;

/**
 * @author dortm19
 */
public class BorderSplitPane extends JSplitPane {

    public BorderSplitPane(Component leftComponent, Component rightComponent, boolean useMatteBorder) {

        super(JSplitPane.HORIZONTAL_SPLIT, true, leftComponent, rightComponent);
        if (rightComponent == null) {
            this.setDividerSize(0);

        }
        if (useMatteBorder) {
            this.setBorder(new MatteBorder(0, 1 /*left*/, 0, 3 /*right*/, UIManager.getColor("BorderSplitPane.borderColor")));
        }
        this.setBackground(UIManager.getColor("BorderSplitPane.background"));
        JideSwingUtilities.setOpaqueRecursively(leftComponent, false);
        JideSwingUtilities.setOpaqueRecursively(rightComponent, false);
    }
}
