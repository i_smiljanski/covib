/*
 * Created on 10.12.2004
 *
 */
package com.utd.stb.gui.loader.resources;

import com.jidesoft.icons.IconsFactory;

import javax.swing.*;
import java.util.MissingResourceException;
import java.util.ResourceBundle;


/**
 * @author rainer bruns
 */
public class Res {

    private static Class clazz = Res.class;
    private static ResourceBundle bundle;

    /**
     * The resourcebundle
     *
     * @return
     */
    public static ResourceBundle getBundle() {

        if (bundle == null) {
            bundle = ResourceBundle.getBundle(resources.Res.RESOURCE_PATH + "LoaderStrings");
        }
        return bundle;
    }

    /**
     * get the value to the key
     *
     * @param key
     * @return value
     */
    public static String getString(String key) {

        try {
            return getBundle().getString(key);
        } catch (MissingResourceException e) {
            e.printStackTrace();
            // do nothing
        }
        return key;
    }

    /**
     * get the image icon with the specified name
     *
     * @param name
     * @return ImageIcon
     */
    public static ImageIcon getFrameIcon(String name) {
        System.out.println("clazz.getClassLoader() = " + clazz.getClassLoader());
        ClassLoader loader = clazz.getClassLoader();
        if (loader == null)
            loader = ClassLoader.getSystemClassLoader();
        return new ImageIcon(loader.getResource(resources.Res.RESOURCE_PATH + name));
    }

    public static ImageIcon getImageIcon(String name) {
        if (name != null)
            return IconsFactory.getImageIcon(Res.class, name);
        else
            return null;
    }

    public static void main(String[] args) throws Exception {
        // Class<GetResourcesExample> clazz = GetResourcesExample.class;
        ClassLoader cl = clazz.getClassLoader();
        System.out.println(cl.getResource(resources.Res.RESOURCE_PATH + "iSR30-48.png"));
    }
}