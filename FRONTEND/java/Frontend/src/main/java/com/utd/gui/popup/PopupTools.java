package com.utd.gui.popup;

import javax.swing.*;
import java.awt.*;


/**
 * @author PeterBuettner.de
 */
public class PopupTools {

    /**
     * Calculates a rectangle relative to a component (ComboBox, FilterField) so
     * that the rectangle is visible on screen, also changes y-coords. Put in
     * the size you think is right, a rectangle below compo, if it excesses the
     * lower screen bound, you get a new one back, with enhanced coords.
     *
     * @param baseCompo we align pop here
     * @param pop       in coords relative to baseCompo, unchanged
     * @return new coords/bounds to use
     */
    public static Rectangle computePopupBounds(Component baseCompo, Rectangle pop) {

        Point p = new Point(0, 0);// screenCoords of component c
        SwingUtilities.convertPointFromScreen(p, baseCompo);

        // Calculate the desktop dimensions relative to the 'combo box'.
        GraphicsConfiguration gc = baseCompo.getGraphicsConfiguration();
        Rectangle screen;
        if (gc != null) {

            Insets i = Toolkit.getDefaultToolkit().getScreenInsets(gc);
            screen = gc.getBounds();
            screen.width -= (i.left + i.right);
            screen.height -= (i.top + i.bottom);
            screen.x += (p.x + i.left);
            screen.y += (p.y + i.top);

        } else {

            screen = new Rectangle(p, Toolkit.getDefaultToolkit().getScreenSize());

        }

        Rectangle rect = new Rectangle(pop);
        if ((rect.y + rect.height > screen.y + screen.height) // excess
                // height
                && (rect.height < screen.height)) // and is not to tall
            rect.y = -rect.height - 1;

        if (rect.x + rect.width > screen.x + screen.width) { // excess width
            // && (pop.width < screen.width)) // and is not to wide
            rect.x = screen.x + screen.width - rect.width - 1;

        }

        return rect;

    }

}