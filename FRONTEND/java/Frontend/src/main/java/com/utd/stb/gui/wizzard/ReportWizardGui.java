package com.utd.stb.gui.wizzard;

import com.jgoodies.forms.builder.ButtonBarBuilder;
import com.jgoodies.forms.factories.Borders;
import com.utd.gui.action.ActionHelp;
import com.utd.gui.action.ActionUtil;
import com.utd.gui.action.ActionWrap;
import com.utd.gui.controls.BorderSplitPane;
import com.utd.gui.controls.ButtonGroupCursorNavigation;
import com.utd.gui.controls.TitleBar;
import com.utd.gui.controls.sidebar.SideBar;
import com.utd.gui.controls.sidebar.SideBarSplitPaneUI;
import com.utd.gui.icons.IconFactory;
import com.uptodata.isr.gui.util.Colors;
import com.uptodata.isr.gui.util.PixelSizes;
import com.utd.stb.back.ui.impl.wizard.StandardWizardErrorException;
import com.utd.stb.back.ui.inter.WizardFormData;
import com.utd.stb.gui.help.Help;
import com.utd.stb.gui.swingApp.exceptions.ExceptionHandler;
import com.utd.stb.gui.wizzard.choosers.AbstractChooser;
import com.utd.stb.gui.wizzard.choosers.ChooserFactory;
import com.utd.stb.gui.wizzard.wizzardStep.WizardStepCellRenderer;
import com.utd.stb.gui.wizzard.wizzardStep.WizardStepDialog;
import com.utd.stb.gui.wizzard.wizzardStep.WizardStepTools;
import com.utd.stb.inter.application.BackendException;
import com.utd.stb.inter.wizzard.FormDisplayInfo;
import com.utd.stb.inter.wizzard.ReportWizard;
import com.utd.stb.inter.wizzard.WizardStep;
import com.utd.stb.inter.wizzard.WizardSteps;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import resources.IconSource;
import resources.Res;

import javax.swing.*;
import javax.swing.border.CompoundBorder;
import javax.swing.border.EmptyBorder;
import javax.swing.border.LineBorder;
import javax.swing.border.MatteBorder;
import javax.swing.event.ChangeEvent;
import javax.swing.event.ChangeListener;
import javax.swing.event.ListSelectionEvent;
import javax.swing.event.ListSelectionListener;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.util.List;

/**
 * Notes:
 * <ul>
 * <li>In the navigation area the only defaultcapable (~activatable with
 * [Enter]) button is Next/Finish. The other buttons are all not defaultcapable
 * since the focus may easily walk to e.g. 'Abort' so the user may loose work.
 * </li>
 * <p>
 * </ul>
 *
 * @author PeterBuettner.de
 */
public abstract class ReportWizardGui {

    public static final Logger log = LogManager.getRootLogger();

    // ... Actions ..................
    private Action acNavBack;
    private Action acNavNext;
    private Action acFinish;
    private Action acAbort;
    private Action acHelp;
    private Action acGotoDialog;
    private Action acFilterDialog;

    private Action acOpenCloseSidebar;

    // ... Components ..................
    private TitleBar titleBar;
    private SideBar spSidebar;
    private JList listNavigation;

    /**
     * placeholder to put the content in
     */
    private JPanel chooserContainer;

    /**
     * (No: this is not enough!: just en/disable the action, not the button); it
     * is a field so we can set this button as default
     */
    private JButton bNext;

    /**
     * data source
     */
    protected ReportWizard reportWizard;

    /**
     * the panel where all other components are embedded, may use it in a
     * JDialog/Frame/Applet, is returned in getContentPanel()
     */
    private JPanel pnlMain;

    /**
     * the current visible Chooser
     */
    private AbstractChooser currentChooser;

    /**
     * recursionBlocker, List will be build new and selection set will cause a
     * selChangeEvent.
     */
    private boolean internalNavStepSet;

    /**
     * experimental: sidebar has whole height and buttonbar is indented
     */
    private static final boolean buttonsBottomRight = false;

    public ReportWizardGui(ReportWizard reportWizard) {

        this.reportWizard = reportWizard;
    }

    /**
     * called if user want to abort, we do nothing here, the surrunding dialog
     * needs to care about all (confirm, window close, call reportWizard.abort()
     * )
     */
    protected abstract void userWantAbortWizard();

    /**
     * called if user want to finish the wizard, we do nothing here, the
     * surrunding dialog needs to care about all (window close, call
     * reportWizard.navigateFinish() ) But it is shure that the current changes
     * in the data is saved before this is called.
     */
    protected abstract void userWantFinishWizard();

    protected abstract void abortWizardOnError();

    /**
     * called when an exception occured, since the surrounding Dialog/frame has
     * to handle it, we inform it here. the dlg is hidden afterwards! see
     * {@link com.utd.stb.gui.swingApp.exceptions.ExceptionHandler}
     *
     * @param e
     */
    protected abstract boolean exceptionHandler(Throwable e, boolean clientCanContinue);

    /**
     * get a panel with the full content. It is separated 'cause one may want to
     * create a JFrame/JDialog/JApplet as the gui for the reportwizzard.
     *
     * @param rootPaneContainer needed to set the defaut button
     * @return
     */
    public JPanel getContentPanel(RootPaneContainer rootPaneContainer) throws BackendException {

        pnlMain = new JPanel(new BorderLayout());

        createActions();
        createControls();
        // ..................................

        listNavigation.addListSelectionListener(new NavListSelectionListener());
        titleBar = new TitleBar("No title", "Nothing here",
                new IconSource().getHeaderIconByID("wizzard"), 0);

        chooserContainer = new JPanel(new BorderLayout());
        chooserContainer.setBorder(new EmptyBorder(PixelSizes
                .getButtonBarGapY(chooserContainer), 0, 0, 0));

        // central splitter/sidebar pane + nav panel
        JComponent navPanel = createNavListPanel();
        navPanel.setMinimumSize(new Dimension());// let SideBar size to zero

        if (buttonsBottomRight) {

            JComponent pnlRight = new JPanel(new BorderLayout());
            pnlRight.add(chooserContainer);
            spSidebar = new SideBar(JSplitPane.HORIZONTAL_SPLIT, true, navPanel, pnlRight);
            pnlRight.add(createButtonBar(), BorderLayout.SOUTH);

        } else {

            spSidebar = new SideBar(JSplitPane.HORIZONTAL_SPLIT, true, navPanel,
                    chooserContainer);
        }

        pnlMain.add(titleBar, BorderLayout.NORTH);
        pnlMain.add(spSidebar);
        if (!buttonsBottomRight) {
            pnlMain.add(createButtonBar(), BorderLayout.SOUTH);
        }

        // Default Button on Enter: ok
        JRootPane root = rootPaneContainer.getRootPane();
        root.setDefaultButton(bNext);

        // no escape button, maybe hit by accident to fast, also needed for some
        // control actions
        ActionUtil.putIntoActionMap(pnlMain,
                new Action[]{acAbort,
                        acHelp,
                        acNavBack,
                        acNavNext,
                        acOpenCloseSidebar,
                        acGotoDialog,
                        acFilterDialog});

        try {

            // don't call navigateStep() since there is some anti-recursion lock
            reportWizard.navigateToWizardStep(getCurrentStep());
            if (getCurrentStep() == null) {
                abortWizardOnError();
            }
            afterNavigationCleanup();

        } catch (BackendException e) {

            // what to do if the gui can't created? well, the dialog is cloesd!
            exceptionHandler(e, false);
            throw e;

        }

        return pnlMain;

    }// -----------------------------------------------------------------------

    private Component createButtonBar() {

        ButtonBarBuilder builder = new ButtonBarBuilder();
        JButton bAbort = new JButton(acAbort);
        JButton bHelp = new JButton(acHelp);
        JButton bBack = new JButton(acNavBack);

        builder.addGridded(bHelp);
        builder.addUnrelatedGap();
        builder.addGlue();
        builder.addGridded(bBack);
        builder.addGridded(bNext);
        builder.addUnrelatedGap();
        builder.addGridded(bAbort);

        JPanel buttonBar = builder.getPanel();
        buttonBar.setBorder(Borders.DIALOG_BORDER);

        ButtonGroupCursorNavigation bg = new ButtonGroupCursorNavigation();
        bg.add(bAbort);
        bg.add(bHelp);
        bg.add(bBack);
        bg.add(bNext);

        return buttonBar;
    }// --------------------------------------------------------------------------

    private JComponent createNavListPanel() {

        JLabel lblNav = new JLabel("");
        lblNav.setBorder(new CompoundBorder(new MatteBorder(0, 0, 1, 0, Colors
                .getUIControlShadow()), new EmptyBorder(2, 6, 3, 6)));

        ActionUtil
                .initLabelFromMap(lblNav, Res.getActionMap("reportwizzard.label.sidebar"));
        lblNav.setLabelFor(listNavigation);
        JPanel navPanel = new JPanel(new BorderLayout());
        navPanel.add(lblNav, BorderLayout.NORTH);
        BorderSplitPane sp = new BorderSplitPane(new JScrollPane(listNavigation), null, false);
        navPanel.add(sp);
        if (UIManager.getBoolean("com.utd.gui.flatStyle")) {
            sp.setBorder(null);
            navPanel.setBorder(new LineBorder(Colors.getUIControlShadow(), 1));
        }
        return navPanel;
    }

    /**
     * needs actions created before
     */
    private void createControls() {

        listNavigation = new JList();

        listNavigation
                .setCellRenderer(new WizardStepCellRenderer( /* IconFactory.getTriangleIcon(9,0) */));
        listNavigation.setSelectionMode(ListSelectionModel.SINGLE_SELECTION);
        listNavigation.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));

        bNext = new JButton(acNavNext);
        bNext.setHorizontalTextPosition(SwingConstants.LEFT);

    }// ----------------------------------------------------------------

    private void createActions() {

        final int nextPrevSize = 9;

        acNavNext = new ResAction("next", IconFactory.getTriangleIcon(nextPrevSize, 0)) {

            public void actionPerformed(ActionEvent e) {

                navigateDelta(+1);
            }
        };

        acFinish = new ResAction("finish", IconFactory.getCheckIcon(nextPrevSize)) {

            public void actionPerformed(ActionEvent e) {

                navigateFinish();
            }
        };

        acNavBack = new ResAction("back", IconFactory.getTriangleIcon(nextPrevSize, Math.PI)) {

            public void actionPerformed(ActionEvent e) {

                navigateDelta(-1);
            }
        };

        acHelp = new ActionHelp() {

            public void actionPerformed(ActionEvent e) {
//                Help.showHelp(pnlMain, getCurrentStep().getHelpSource());
                Help.showHelp();
            }
        };

        acAbort = new ResAction("abort", null) {

            public void actionPerformed(ActionEvent e) {

                userWantAbortWizard();
            }
        };

        acGotoDialog = new ResAction("gotoDialog", null) {

            public void actionPerformed(ActionEvent e) {

                goToDialog();
            }
        };

        acFilterDialog = new ResAction("filterDialog", null) {

            public void actionPerformed(ActionEvent e) {

                filterDialog();
            }
        };

        acOpenCloseSidebar = new ResAction("openCloseSidebar", null) {

            public void actionPerformed(ActionEvent e) {

                ActionUtil.invokeAction(spSidebar,
                        SideBarSplitPaneUI.TOGGLE_OPEN_CLOSE_ACTION);
            }
        };
    }// --------------------------------------------------------------

    private static abstract class ResAction extends ActionWrap {

        public ResAction(String name, Object icon) {

            super(Res.getActionMap("reportwizzard.action." + name), icon);
            // on error do emergency solution
            if (getValue(Action.NAME) == null) {
                putValue(Action.NAME, "$$" + name);
            }
        }
    }

    private final class NavListSelectionListener implements ListSelectionListener {

        public void valueChanged(ListSelectionEvent e) {

            if (internalNavStepSet || e.getValueIsAdjusting()) {
                return;
            }
            Object o = ((JList) e.getSource()).getSelectedValue();
            if (o instanceof WizardStep) {
                navigateStep((WizardStep) o);
            }
        }
    }

    // ##################################################################
    // ################## end of gui creating stuff #####################
    // ##################################################################
    private void setContent(JComponent content) {

        chooserContainer.removeAll();
        chooserContainer.add(content);
        chooserContainer.validate();
    }

    //################## navigation stuff #####################
    // to make life easier
    private WizardSteps getSteps() {

        return reportWizard.getWizardSteps();
    }

    private WizardStep getCurrentStep() {

        return reportWizard.getCurrentWizardStep();
    }

    private void navigateFinish() {

        if (currentChooser != null) {

            try {
                currentChooser.saveData();
            } catch (StandardWizardErrorException swe) {
                showWizardError(swe);
                if (swe.getSeverity() == StandardWizardErrorException.SEVERITY_WIZARD_ACTION_EXIT) {
                    return;
                }
            } catch (BackendException e) {
                exceptionHandler(e, false);
                return;
            }

        }

        userWantFinishWizard();
    }

    /**
     * goes delta steps forward/back (mostly used with +-1)
     *
     * @param delta
     * @throws IllegalArgumentException if delta is to big so that it would
     *                                  result a wrong index
     */
    private void navigateDelta(int delta) throws IllegalArgumentException {

        WizardSteps steps = getSteps();
        WizardStep current = getCurrentStep();

        int index = steps.indexOf(current);
        int destIndex = index + delta;
        // since this method is called only in this class with +-1 we probably
        // never will be out of range
        if (destIndex < 0 || destIndex >= steps.getSize()) {
            throw new IllegalArgumentException("Illegal delta: " + delta
                    + " will result in step index:" + destIndex);
        }
        navigateStep(steps.get(destIndex));

    }

    //navigate always through this only
    private void navigateStep(WizardStep step) {

        // the list fires sel change if the user Ctrl-clicks an item two times
        if (step.equals(getCurrentStep())) {
            return;
        }

        // set reg expr in database
        storeUsersRegExpr(currentChooser.getFilterFieldValue(),
                ((WizardFormData) getCurrentStep().getFormData()).getEntity());

        Component saveFocus = KeyboardFocusManager.
                getCurrentKeyboardFocusManager().
                getPermanentFocusOwner();

        disableNavigation();// well..., never seen since we are in the Swing
        // Thread, -> if an exception occures
        //  log.debug("set WAIT_CURSOR ");
        pnlMain.setCursor(Cursor.getPredefinedCursor(Cursor.WAIT_CURSOR));
        if (currentChooser != null) {
            try {
                currentChooser.saveData();
            } catch (BackendException e) {
                pnlMain.setCursor(Cursor.getDefaultCursor());
                exceptionHandler(e, false);
                return;
            }
        }

        try {
            reportWizard.navigateToWizardStep(step);
        } catch (BackendException e) {
            pnlMain.setCursor(Cursor.getDefaultCursor());
            exceptionHandler(e, false);
            return;
        }

        pnlMain.setCursor(Cursor.getDefaultCursor());
        afterNavigationCleanup();

        // if we are in a Dialog focussing behaves mysterious, so restore
        if (saveFocus.isShowing()) {
            saveFocus.requestFocusInWindow();
        } else {
            listNavigation.requestFocusInWindow();
        }

        // on invalid data in Chooser: Tab out from list won't work.
        // since user has to fill in something set focus to chooser
        // also the next(last button maybe focussed, and afterwards disabled
        // if(!currentIsValid()) // changeOnNavigationDisabling uncomment this
        // if the navigation is partly disabled
        // KeyboardFocusManager.getCurrentKeyboardFocusManager().focusNextComponent(chooserContainer);
    }

    /**
     * be aware of recursion, better call navigateStep
     */
    private void afterNavigationCleanup() {
        WizardSteps steps = getSteps();
        WizardStep current = getCurrentStep();
        if (current == null) {
            return;
        }
        FormDisplayInfo formDisplayInfo = current.getFormDisplayInfo();
        titleBar.setTitle(formDisplayInfo.getTitleBarTitleText());
        titleBar.setSubText(formDisplayInfo.getTitleBarInfo());

        updateNavList(steps, current);

        ExceptionHandler eh = new ExceptionHandler() { // wrap myself is easy,
            // not implement
            // ExceptionHandler

            public boolean exceptionHandler(Throwable throwable, boolean clientCanContinue) {

                return ReportWizardGui.this.exceptionHandler(throwable, clientCanContinue);
            }
        };

        currentChooser = ChooserFactory.getInstance().getChooser(eh, current.getFormData());
        if (currentChooser != null) {
            log.debug("setContent");
            setContent(currentChooser.getContent());
            currentChooser.addChangeListener(validationListener);
//            validationMayChanged();
        } else {
            JLabel label = new JLabel("No appropriate gui interface found", JLabel.CENTER);
            label.setForeground(Colors.getUIControlTextWarn());
            setContent(label);
        }
        updateActionEnabling();
    }

    // ----------------------------------------------------------------------

    /**
     * call on validation changed, or after navigation (this is done in
     * afterNavigationCleanup)
     */

    private void updateActionEnabling() {

        WizardSteps steps = getSteps();
        WizardStep current = getCurrentStep();

        boolean isValid = currentIsValid();
        isValid = true; //TODOx remove isValid=true to disable buttons on
        // invalid
        // search for changeOnNavigationDisabling in this file!

        int index = steps.indexOf(current);
        log.debug("set buttons enabled: " + (index > 0 && isValid));
        acNavBack.setEnabled(index > 0 && isValid);
        acNavNext.setEnabled(index < steps.getSize() - 1 && index != -1 && isValid);
        acFinish.setEnabled(index == steps.getSize() - 1 && index != -1 && currentIsValid());
        bNext.setAction(index < steps.getSize() - 1 ? acNavNext : acFinish);
        bNext.setEnabled(bNext.getAction().isEnabled());
        listNavigation.setEnabled(isValid);

    }// ----------------------------------------------------------------------

    /**
     * 'temporary' disable all navigation tools
     */
    private void disableNavigation() {

        boolean enable = false;
        log.debug("set buttons disabled");
        acNavBack.setEnabled(enable);
        acNavNext.setEnabled(enable);
        acFinish.setEnabled(enable);
        listNavigation.setEnabled(enable);

    }// ---------------------------------------------

    /**
     * gets informed if the state of the current chooser changed
     */
    private ChangeListener validationListener = new ChangeListener() {

        public void stateChanged(ChangeEvent e) {

            validationMayChanged();
        }

    };

    private void validationMayChanged() {

        updateActionEnabling();

    }// ---------------------------------------------

    private boolean currentIsValid() {

        return currentChooser != null && currentChooser.isValid();

    }// ---------------------------------------------

    /**
     * after navigation the steps may have changed
     */
    private void updateNavList(WizardSteps wizardSteps, WizardStep current) {

        // copy: if steps changes while displaying..., decoupling of gui and
        // data
        final List data = WizardStepTools.wizardStepsToList(wizardSteps);

        ListModel m = new AbstractListModel() {

            public int getSize() {

                return data.size();
            }

            public Object getElementAt(int i) {

                return data.get(i);
            }
        };
        listNavigation.setModel(m);

        try {

            internalNavStepSet = true;
            listNavigation.setSelectedValue(current, true);

        } finally {
            internalNavStepSet = false;
        }

    } // ---------------------------------------------

    private void goToDialog() {

        String title = Res.getString("reportwizzard.gotoDialog.title");
        String label = Res.getString("reportwizzard.gotoDialog.label");
        WizardStep newStep = WizardStepDialog.showGoToDialog(pnlMain, title, label,
                getSteps(), getCurrentStep());
        if (newStep != null) {
            navigateStep(newStep);
        }

    } // ---------------------------------------------

    private void filterDialog() {

        Object listData = loadUsersRegExpr(((WizardFormData) getCurrentStep().getFormData()).getEntity());
        if (listData == null) {
            return;
        }
        String regExpr = WizardStepDialog.showFilterDialog(pnlMain,
                Res.getString("reportwizzard.filterDialog.title"),
                Res.getString("reportwizzard.filterDialog.label"),
                listData);
        if (regExpr != null && regExpr.length() != 0) {
            currentChooser.setFilterFieldValue(regExpr);
        }

    } // ---------------------------------------------

    /**
     * to set and get the divider position
     */
    public void setDividerLocation(int pos) {

        spSidebar.setDividerLocation(pos);
    }

    public int getDividerLocation() {

        return spSidebar.getDividerLocation();
    }

    /**
     * called if user navigates to save the entered regular expression
     */
    protected abstract void storeUsersRegExpr(String regExpr, String entity);

    /**
     * called if user navigates to get the entered regular expression
     */
    protected abstract Object loadUsersRegExpr(String entity);

    /**
     * called if an error in the wizard occurs
     */
    protected abstract void showWizardError(StandardWizardErrorException swe);

}
