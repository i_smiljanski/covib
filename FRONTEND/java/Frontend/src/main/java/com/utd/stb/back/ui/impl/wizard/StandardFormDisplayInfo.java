package com.utd.stb.back.ui.impl.wizard;

import com.utd.stb.inter.wizzard.FormDisplayInfo;


/**
 * Provides the extra information displayed for a particular form in report
 * wizard
 *
 * @author rainer bruns Created 02.09.2004
 */
public class StandardFormDisplayInfo implements FormDisplayInfo {

    String entity;
    String number;
    String title;
    String pkg;
    String name;
    String tooltip;

    /**
     * On creation, that is on wizard start, we have only name and number
     *
     * @param number
     * @param name
     */
    public StandardFormDisplayInfo(String number, String name, String tooltip) {

        this.name = name;
        this.number = number;
        this.tooltip = tooltip;
    }

    /*
     * (non-Javadoc)
     * 
     * @see com.utd.stb.inter.wizzard.FormDisplayInfo#getTitleBarTitleText()
     */
    public String getTitleBarTitleText() {

        return title;
    }

    /*
     * (non-Javadoc)
     * 
     * @see com.utd.stb.inter.wizzard.FormDisplayInfo#getTitleBarInfo()
     */
    public String getTitleBarInfo() {

        return pkg;
    }

    /*
     * (non-Javadoc)
     * 
     * @see com.utd.stb.inter.wizzard.FormDisplayInfo#getNavigationListText()
     */
    public String getNavigationListText() {

        return name;
    }

    /*
     * (non-Javadoc)
     * 
     * @see com.utd.stb.inter.wizzard.FormDisplayInfo#getNavigationListInfo()
     */
    public String getNavigationListInfo() {

        return tooltip;
    }

    /**
     * receive this information if current form
     *
     * @param pkg
     * @param title
     * @param entity
     */
    public void setDisplayInfo(String pkg, String title) {

        this.pkg = pkg;
        this.title = title;
    }
}