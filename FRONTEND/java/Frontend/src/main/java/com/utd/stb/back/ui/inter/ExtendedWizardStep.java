package com.utd.stb.back.ui.inter;

import com.utd.stb.inter.wizzard.FormData;
import com.utd.stb.inter.wizzard.WizardStep;


/**
 * Extended interface of WizardStep used by Dispatcher
 *
 * @author rainer bruns Created 14.12.2004
 */
public interface ExtendedWizardStep extends WizardStep {

    /**
     * When navigated to or "through" a step the form data is quieried from DB
     * and set in step
     *
     * @param formData the data for the step
     * @see FormData
     * @see Dispatcher#displayForm()
     */
    public void setFormData(WizardFormData formData);

    /**
     * Set the data for this step when it is the current step
     *
     * @param direction direction of navigation 1/-1
     * @return true if data do not have to be reloaded, that is in backward
     * navigation not used actually
     */
    public boolean isFormDataValid(int direction);

    /**
     * We don't have those information on initialisation, so set them when
     * displayed
     *
     * @param pkg    the db-package name
     * @param title  title of step
     * @param entity entity of step
     */
    public void setDisplayInfo(String pkg, String title);

    /**
     * Needed to decide wether navigate forward or backward and if we are in
     * navigate mode ( then delta current/new > 1)
     *
     * @return the index of step
     */
    public int getCurrentIndex();

}