package com.utd.stb.gui.swingApp;

import java.awt.*;
import java.awt.event.AWTEventListener;
import java.awt.event.ComponentAdapter;
import java.awt.event.ComponentEvent;
import java.awt.event.MouseEvent;


/**
 * Looks for user interaction, inspects AWTEventQueue, some interactions can't
 * be seen: <br>
 * <ul>
 * <li>Top window, but parented (e.g. modal dialogs) resize/move
 * <li>
 * <li>
 * <li>
 *
 * @author PeterBuettner.de
 */
public class TimeoutManager {

    private TimeoutManager() {
    }// no instances

    private static AWTEventListener awtl;

    /*
     * "volatile" See: JLS 17.4
     * http://java.sun.com/docs/books/jls/second_edition/html/memory.doc.html#28733
     * 
     * if here two threads overlap while writing the two 32 Bit halfs of the
     * time it may matter every 42 days (2^32/1000/60/60/24) Thread upper lower
     * A x 0xFFFFFFFF B x+1 0x00000001
     * 
     * May result in x+1 0xFFFFFFFF
     * 
     * and so ~42 days are added and the application exits immediately
     */

    volatile private static long lastEventTime = System.currentTimeMillis();
    private static final Object LOCK = new Object();

    /**
     * call only once, following calls have no effect
     */
    static synchronized void install() {

        if (awtl != null) return;

        awtl = new AWTEventListener() {

            // isn't called very often, so don't optimizee
            public void eventDispatched(AWTEvent e) {

                if (e instanceof MouseEvent) {
                    /*
                     * only see _real_ input, if the user only hovers over a
                     * window: eat
                     */
                    int id = ((MouseEvent) e).getID();
                    if (id != MouseEvent.MOUSE_PRESSED && id != MouseEvent.MOUSE_RELEASED
                            && id != MouseEvent.MOUSE_WHEEL) return;
                }
                lastEventTime = System.currentTimeMillis();
            }
        };

        /*
         * ACTION_EVENT_MASK ADJUSTMENT_EVENT_MASK COMPONENT_EVENT_MASK
         * CONTAINER_EVENT_MASK FOCUS_EVENT_MASK HIERARCHY_BOUNDS_EVENT_MASK
         * HIERARCHY_EVENT_MASK INPUT_METHOD_EVENT_MASK INVOCATION_EVENT_MASK
         * ITEM_EVENT_MASK KEY_EVENT_MASK MOUSE_EVENT_MASK
         * MOUSE_MOTION_EVENT_MASK MOUSE_WHEEL_EVENT_MASK PAINT_EVENT_MASK
         * TEXT_EVENT_MASK WINDOW_EVENT_MASK WINDOW_FOCUS_EVENT_MASK
         * WINDOW_STATE_EVENT_MASK
         */

        //HierarchyBoundsListener
        long eventMask =

                //	AWTEvent.HIERARCHY_BOUNDS_EVENT_MASK | // doesn't sees dialog
                // move/resize
                AWTEvent.KEY_EVENT_MASK | AWTEvent.MOUSE_WHEEL_EVENT_MASK | AWTEvent.MOUSE_EVENT_MASK
                        | AWTEvent.WINDOW_STATE_EVENT_MASK | // iconified,
                        // maximized,
                        // normalized
                        AWTEvent.WINDOW_EVENT_MASK | // activated,
                        // opened,
                        // closed,...
                        0;
        Toolkit.getDefaultToolkit().addAWTEventListener(awtl, eventMask);
    }// ----------------------------------------------------

    /**
     * Use this if any interaction occured that can't be cought by the
     * AWTEventQueue, maybe 'cause of optimization or just 'can't/don't know how
     * to see it'
     * <p>
     * e.g. Topwindow Resizing
     */
    private static void interactionOccured() {

        lastEventTime = System.currentTimeMillis();
    }

    /**
     * Same meaning as System.currentTimeMillis(), if no event occured since
     * install it is the time install was called
     *
     * @return
     */
    public static long getLastEventTime() {

        return lastEventTime;
    }

    /**
     * Use ths on any top level window so we get interaction Events for
     * sizing/moving here in the TimeoutManager.
     *
     * @param window
     */
    static void addSizeMoveListener(Window window) {

        window.addComponentListener(new ComponentAdapter() {

            public void componentMoved(ComponentEvent e) {

                interactionOccured();
            }

            public void componentResized(ComponentEvent e) {

                interactionOccured();
            }
        });

    }

}