package com.utd.stb.back.ui.impl.wizard.formdata;

import com.utd.stb.back.data.client.FormInfoRecord;
import com.utd.stb.back.data.client.SelectionList;
import com.utd.stb.back.data.client.SelectionRecord;
import com.utd.stb.back.database.Dispatcher;
import com.utd.stb.back.ui.inter.WizardFormData;
import com.utd.stb.inter.application.BackendException;
import com.utd.stb.inter.items.DisplayableItems;
import com.utd.stb.inter.wizzard.formtypes.FormDataListOutOfList;


/**
 * Handles the data for the Picklist Window
 *
 * @author rainer bruns
 */
public class Picklist implements FormDataListOutOfList, WizardFormData {

    private Dispatcher dispatcher;
    private SelectionList data;
    private SelectionList selection;
    private FormInfoRecord formInfo;

    /**
     * @param d        the dispatcher
     * @param data     all data for window
     * @param formInfo record with form information, used for the Labels
     * @throws BackendException
     */
    public Picklist(Dispatcher d, SelectionList data, FormInfoRecord formInfo)
            throws BackendException {

        this.dispatcher = d;
        this.data = data;
        this.formInfo = formInfo;
        selection = (SelectionList) getSelectedItems();
    }

    /*
     * (non-Javadoc) saves the selected data, called from TwoTableChooser
     * 
     * @see com.utd.stb.inter.wizzard.FormData#saveData()
     * @see TwoTableChooser
     */
    public void saveData() throws BackendException {

        dispatcher.save(null, selection, false);
    }

    /*
     * (non-Javadoc) returns the selected items ( right side of window
     * 
     * @see com.utd.stb.inter.wizzard.formtypes.FormDataListOutOfList#getSelectedItems()
     */
    public DisplayableItems getSelectedItems() {

        SelectionList selectedData = new SelectionList();
        data.initIterator();
        while (data.hasNext()) {
            SelectionRecord s = data.getNext();
            if (s.isSelected()) {
                selectedData.add(s);
            }
        }
        return selectedData;
    }

    /*
     * (non-Javadoc) returns all items
     * 
     * @see com.utd.stb.inter.wizzard.formtypes.FormDataListOutOfList#getAvailableItems()
     */
    public DisplayableItems getAvailableItems() {

        return data;
    }

    /*
     * (non-Javadoc) sets the selected items before saving
     * 
     * @see com.utd.stb.inter.wizzard.formtypes.FormDataListOutOfList#setSelectedItems(com.utd.stb.inter.items.DisplayableItems)
     */
    public void setSelectedItems(DisplayableItems items) {

        selection.clear();
        for (int i = 0; i < items.getSize(); i++) {
            SelectionRecord rec = (SelectionRecord) items.get(i);
            selection.add(rec);
        }
    }

    /*
     * (non-Javadoc) returns label for left side
     * 
     * @see com.utd.stb.inter.wizzard.formtypes.FormDataListOutOfList#getLabelAllItems()
     */
    public String getLabelAllItems() {

        return formInfo.getPrompt1();
    }

    /*
     * (non-Javadoc) returns label for right side
     * 
     * @see com.utd.stb.inter.wizzard.formtypes.FormDataListOutOfList#getLabelChoosenItems()
     */
    public String getLabelChoosenItems() {

        return formInfo.getPrompt2();
    }

    /* (non-Javadoc)
     * @see com.utd.stb.back.ui.inter.WizardFormData#getDataToSave()
     */
    public SelectionList getDataToSave() {

        return selection;
    }

    /* (non-Javadoc)
     * @see com.utd.stb.back.ui.inter.WizardFormData#getEntity()
     */
    public String getEntity() {
        return formInfo.getEntity();
    }
}

