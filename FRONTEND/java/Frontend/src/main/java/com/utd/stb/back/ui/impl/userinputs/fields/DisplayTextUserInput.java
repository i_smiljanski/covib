package com.utd.stb.back.ui.impl.userinputs.fields;

import com.utd.stb.back.data.client.PropertyRecord;


/**
 * Implementation of UserInput for display text fields
 *
 * @author smiljanskii60 Created 27.01.2015
 */
public class DisplayTextUserInput extends AbstractUserInput {

    PropertyRecord property;
    String text;

    /**
     * @param label    field Label
     *                 field Value
     * @param property complete record of field information from database
     */
    public DisplayTextUserInput(String label, String text, PropertyRecord property) {
        super(label);
        this.property = property;
        this.text = text;
    }

    /**
     * Is used when saving field to db
     *
     * @return the record to be stored to db
     */
    public PropertyRecord getProperty() {
        return property;
    }

    @Override
    public void setData(Object data) {

    }

    @Override
    public Object getData() {
        return text;
    }

    @Override
    public Class getType() {
        return String.class;
    }
}