package com.utd.stb.inter.wizzard;

/**
 * Container for <code>WizardStep</code>s, no navigation, just to hold the
 * data.
 *
 * @author PeterBuettner.de
 */
public interface WizardSteps {

    /**
     * Number of items
     *
     * @return
     */
    int getSize();

    /**
     * Get Item at index
     *
     * @param index
     * @return
     */
    WizardStep get(int index);

    /**
     * returns the index <br>
     * or -1 if not found
     *
     * @param wizardStep
     * @return
     */
    int indexOf(WizardStep wizardStep);

}