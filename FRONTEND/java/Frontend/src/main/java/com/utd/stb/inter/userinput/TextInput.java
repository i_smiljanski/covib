package com.utd.stb.inter.userinput;

/**
 * Free or formatted text, no complex data
 *
 * @author PeterBuettner.de
 */
public interface TextInput {

    /**
     * gets the data
     *
     * @return
     */
    String getText();

    /**
     * TODO silently strip or throw exception if longer than MaxLength will be
     * stripped
     *
     * @param text
     */
    void setText(String text);

    /**
     * This is the maximum count of chars the string may hold, use null to
     * indicate no restriction.
     *
     * @return
     */
    Integer getMaxLength();

    /** may we create something like this? which format? regex? */
    //getFormat();

    /**
     * if newlines are allowed or not. <br>
     * if true gui should render a 'textarea' rather than a textfield, has to
     * work even if multiline areas are not supported.
     *
     * @return
     */
    boolean isMultiLineAllowed();

    /**
     * if set the gui renders a 'passwort' like field: the typed chars are
     * encrypted in the display; multiLineAllowed will probably be ignored
     *
     * @return
     */
    boolean isSecret();
}