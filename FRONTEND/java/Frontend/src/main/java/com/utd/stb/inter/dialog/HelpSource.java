package com.utd.stb.inter.dialog;

/**
 * Interface containing information what help can be shown
 *
 * @author PeterBuettner.de
 */
public interface HelpSource {

    /**
     * the id for the Help toppic
     *
     * @return
     */
    String getHelpId();

    public String getIdentifier();

}