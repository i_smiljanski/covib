package com.utd.stb.gui.userinput.swing;

import com.jgoodies.forms.factories.Borders;
import com.jgoodies.forms.layout.Sizes;
import com.jidesoft.alert.Alert;
import com.jidesoft.animation.CustomAnimation;
import com.jidesoft.utils.PortingUtils;
import com.utd.gui.action.*;
import com.utd.gui.controls.ButtonGroupCursorNavigation;
import com.utd.gui.controls.TitleBar;
import com.utd.gui.util.DialogUtils;
import com.utd.stb.back.ui.impl.userinputs.data.FilterTableData;
import com.utd.stb.gui.help.Help;
import com.utd.stb.inter.action.ButtonItem;
import com.utd.stb.inter.action.ButtonItems;
import com.utd.stb.inter.action.WindowAction;
import com.utd.stb.inter.application.BackendException;
import com.utd.stb.inter.dialog.UserDialog;
import com.utd.stb.inter.userinput.UserInputs;
import com.utd.stb.interlib.BasicMessageBox;
import com.utd.stb.interlib.WindowActionImpl;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import javax.swing.*;
import java.awt.*;
import java.awt.event.*;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;


/**
 * Creates a modal JDialog out of a UserDialog, the concrete JDialog-class is
 * hidden from outside, an extension of {@link ActionDialog}, note the
 * interfaces this baseclass implements! <br>
 * There are two factory methods so you may have Frame or Dialog as parent.
 *
 * @author PeterBuettner.de
 */
public class DialogFactory {
    public static final Logger log = LogManager.getRootLogger();
    public static boolean modal = true;

    /**
     * Creates a modal JDialog out of a UserDialog, the returned dialog is not
     * visible and not sized, so you may change some of it's properties (size,
     * title, modality,...)
     *
     * @param owner
     * @param dialog
     * @return
     */
    public static JDialog createDialog(Frame owner, UserDialog dialog) {

        return new UserInputDialog(owner, dialog);
    }


    /**
     * Creates a modal JDialog out of a UserDialog, the returned dialog is not
     * visible and not sized, so you may change some of it's properties (size,
     * title, modality,...)
     *
     * @param owner
     * @param dialog
     * @return
     */
    public static JDialog createDialog(Dialog owner, UserDialog dialog) {
        return new UserInputDialog(owner, dialog);
    }


    //is, 13.Dec 2010
    public static Alert createAlert(Frame owner, UserDialog dialog) {
        return new AlertInput(owner, new UserInputDialog(owner, dialog));
    }

    public static Alert createAlert(Dialog owner, UserDialog dialog) {
        return new AlertInput(owner, new UserInputDialog(owner, dialog));
    }


    private static class UserInputDialog extends ActionDialog {

        // ... Content ..........................
        JComponent content;
        private UserDialog dlgDef;
        private PanelDataChangeListener dataHandler;
        /**
         * null if no help for the dialog is available
         */
        private Action acHelp;
        /**
         * a special one to work like 'close the window with the [x] Button in
         * the caption
         */
        private Action acEscape;
        /**
         * here will be those Actions that needs the data to be valid
         */
        private List needValid;
        /**
         * all Actions never null, maybe empty
         */
        private List allActions;
        private JButton[] buttons;

        UserInputDialog(Frame owner, UserDialog dialog) {

            super(owner);
            init(dialog);
        }

        UserInputDialog(Dialog owner, UserDialog dialog) {

            super(owner);
            init(dialog);
        }

        private void init(UserDialog dialog) {
            this.dlgDef = dialog;

            // ... setup the JDialog ................
            setTitle(dlgDef.getTitle());
            log.debug(modal);
            setModal(modal);
            setDefaultCloseOperation(DO_NOTHING_ON_CLOSE);

            addWindowListener(new WindowAdapter() {

                public void windowClosing(WindowEvent e) {

                    wantClose();
                }// is not called on hide()

                public void windowOpened(WindowEvent e) {

                    onWindowOpened();
                }
            });

            // ... The 3 components: titlebar, content,
            // buttonbar....................
            TitleBar titleBar = TitleBarMaker.createTitleBar(dlgDef.getHeaderBar(),
                    "dialogDefault");// maybe
            // null
            // null


            JComponent buttonBar = createButtonBar();// before data
            final UserInputs userInputs = dlgDef.getUserInputs();

            if (userInputs instanceof FilterTableData) {
                content = new FilterTableFactory().createPanel((FilterTableData) userInputs, needValid, this);
                //for(int i=0;i<content.getComponentCount();i++)
                // log.debug(content.getComponent(i));
            } else
                content = new UIOPanelFactory().createPanel(userInputs);
            //if so uses a not separated border
            boolean isTabbed = content instanceof JTabbedPane;
            content.setBorder(isTabbed ? Borders.DIALOG_BORDER : Borders.EMPTY_BORDER);

            if (isTabbed) buttonBar.setBorder(Borders.DIALOG_BORDER);// TODOlater

            // handler!
            // ......................
            dataHandler = new PanelDataChangeListener(content) {
                public void dataChanged(DataChangeEvent event) {
                    if (!(userInputs instanceof FilterTableData)) {
                        boolean valid = (dataHandler.isValid() && !dataHandler.isEmpty());
                        for (Object aNeedValid : needValid) {
                            ((Action) aNeedValid).setEnabled(valid);
                        }
                    }
                }
            };
            dataHandler.dataChanged(null);// init, note: we know to handle
            // null

            // ... Glue together ..........................
            JPanel pnlMain = new JPanel(new BorderLayout());
            if (titleBar != null) pnlMain.add(titleBar, BorderLayout.NORTH);
            pnlMain.add(content, BorderLayout.CENTER);
            pnlMain.add(buttonBar, BorderLayout.SOUTH);

            // ... activate the Shortcuts ...............
            acEscape = new AbstractAction() {

                public void actionPerformed(ActionEvent e) {

                    wantClose();
                }
            };
            acEscape.putValue(Action.ACCELERATOR_KEY, KeyStroke.getKeyStroke("ESCAPE"));

            ActionUtil
                    .putIntoActionMap(pnlMain, (Action[]) allActions.toArray(new Action[0]));
            if (acHelp != null) ActionUtil.putIntoActionMap(pnlMain, acHelp);
            ActionUtil.putIntoActionMap(pnlMain, acEscape);

            getContentPane().add(pnlMain);


        }// ---------------------------------------------------------------

        public Dimension getPreferredSize() {

            // we use at least: 4 buttons wide, this looks much better
            // testing: at least 2,7 buttons high
            // and ... well... the dialog seems to be some px to short in
            // vertical dir,
            Dimension d = super.getPreferredSize();
            if (d != null) {
                int buttonWidth = Sizes.dluX(50).getPixelSize(this);
                if (d.width < buttonWidth * 4) d.width = buttonWidth * 4;
                d.height += 10;
                if (d.height < buttonWidth * 2.7) d.height = (int) (buttonWidth * 2.7);
            }
            return d;
        }// ------------------------------------------------

        /**
         * creates allActions, builds buttons and buttonactions
         *
         * @return
         */
        private JComponent createButtonBar() {

            JButton bHelp = null;
            if (dlgDef.getHelpSource() != null) {
                acHelp = new ActionHelp() {

                    public void actionPerformed(ActionEvent e) {
//                        Help.showHelp(UserInputDialog.this, dlgDef.getHelpSource());
                        Help.showHelp();
                    }
                };
                bHelp = new JButton(acHelp);
            }
            // ----------------------------------------------------------

            ButtonItem defaultButtonItem = dlgDef.getDefaultButtonItem();
            int defaultIndex = -1;

            ButtonItems bi = dlgDef.getButtonItems();
            int count = bi.getSize();

            allActions = new ArrayList(count);
            needValid = new ArrayList(count);

            JButton[] b = new JButton[count];

            for (int i = 0; i < count; i++) {
                ButtonItem item = bi.get(i);
                Action action = createAction(item);
                allActions.add(action);
                b[i] = new JButton(action);


                if (dlgDef instanceof BasicMessageBox) {
                    KeyStroke keyStroke = KeyStroke.getKeyStroke(b[i].getMnemonic(), 0);
                    action.putValue(Action.ACCELERATOR_KEY, keyStroke);
                }
                //log.debug("  getMnemonic  " + b[i].getMnemonic());

                //		b[i].setDefaultCapable(false);
                if (defaultButtonItem == null) {
                    if (ButtonItem.OK.equals(item.getType())) defaultIndex = i;
                } else if (item.equals(defaultButtonItem)) defaultIndex = i;

                if (item.isNeedsValidData()) needValid.add(action);

//                    log.debug("needValid2: " + action);
                // we remove escape and enter to have default button etc.
                // working
                Object key = action.getValue(Action.ACCELERATOR_KEY);
//                log.debug("   key   " + Action.ACCELERATOR_KEY + key);
                if (key instanceof KeyStroke) {
                    KeyStroke ks = (KeyStroke) key;
                    log.debug(" KeyStroke  " + ks);
                    int kc = ks.getKeyCode();
                    if (kc == KeyEvent.VK_ENTER || kc == KeyEvent.VK_ESCAPE)
                        action.putValue(Action.ACCELERATOR_KEY, null);
                }
            }

            // only one button: this becomes the default
            if (count == 1) defaultIndex = 0;

            if (defaultIndex != -1) {
                //		b[defaultIndex].setDefaultCapable(true);
                getRootPane().setDefaultButton(b[defaultIndex]);
            }

            ButtonGroupCursorNavigation bg = new ButtonGroupCursorNavigation();
            if (bHelp != null) bg.add(bHelp);
            bg.add(b);

            JComponent panel = DialogUtils.createButtonPanel(b, bHelp, BorderLayout.NORTH);

            setButtons(b);
            // remove
            // this
            // hack,
            // change
            // DialogUtils
            // to
            // be
            // clean
            return panel;
        }// ---------------------------------------------------------------

        /**
         * create the appropriate Action from a ButtonItem
         *
         * @param item
         * @return
         */
        private Action createAction(ButtonItem item) {

            Object type = item.getType();
            String text = item.getText();
            Action action;

            /*
             * since we have distinct classes for Ok,Cancel,... we need to do it
             * this was, better use a parametrised version?
             */
            if (ButtonItem.OK.equals(type))
                action = new ActionOk() {

                    public void actionPerformed(ActionEvent e) {
                        setCursor(Cursor.getPredefinedCursor(Cursor.WAIT_CURSOR));
                        setEnabled(false);
                        performButtonAction(this);
                    }
                };

            else if (ButtonItem.CANCEL.equals(type))
                action = new ActionCancel() {

                    public void actionPerformed(ActionEvent e) {

                        performButtonAction(this);
                    }
                };
                // ..........................................
            else if (ButtonItem.YES.equals(type))
                action = new ActionYes() {

                    public void actionPerformed(ActionEvent e) {

                        performButtonAction(this);
                    }
                };

            else if (ButtonItem.NO.equals(type))
                action = new ActionNo() {

                    public void actionPerformed(ActionEvent e) {

                        performButtonAction(this);
                    }
                };
                // ..........................................

            else if (ButtonItem.SAVE.equals(type))
                action = new ActionSave() {

                    public void actionPerformed(ActionEvent e) {

                        performButtonAction(this);
                    }
                };

            else if (ButtonItem.SAVE_AND_CLOSE.equals(type))
                action = new ActionSaveAndClose() {

                    public void actionPerformed(ActionEvent e) {

                        performButtonAction(this);
                    }
                };

            else if (ButtonItem.CLOSE.equals(type))
                action = new ActionClose() {

                    public void actionPerformed(ActionEvent e) {

                        performButtonAction(this);
                    }
                };

            else if (ButtonItem.OTHER.equals(type))
                action = new AbstractAction(text) {

                    public void actionPerformed(ActionEvent e) {

                        performButtonAction(this);
                    }
                };
            else if (ButtonItem.NEW.equals(type))
                action = new ActionNew() {

                    public void actionPerformed(ActionEvent e) {

                        performButtonAction(this);
                    }
                };
            else if (ButtonItem.DELETE.equals(type))
                action = new ActionDelete() {

                    public void actionPerformed(ActionEvent e) {

                        performButtonAction(this);
                    }
                };
            else
                throw new IllegalArgumentException("The ButtonItems type is to new to be handled. Type:"
                        + item.getType() + "; item:" + item);

            // save the ButtonItem in the Action with a key that is this class's name
            if (action != null) action.putValue(getClass().getName(), item);

            return action;

        }// ---------------------------------------------------------------

        /**
         * Call from the actionPerformed event of a Action that defines a Button
         *
         * @param action
         */
        public void performButtonAction(Action action) {

            // we saved the ButtonItem in the Action with a key that is this
            // class's name
            Object val = action.getValue(getClass().getName());
            if (!(val instanceof ButtonItem)) return;// sanity check
            executeActionLoop(val);
        }// ----------------------------------------------------------


        /**
         * setup focus, one of:
         * <ul>
         * <li>first component that is visible+editable
         * <li>first tabsheet
         * <li>button
         * </ul>
         */
        private void onWindowOpened() {

            JComponent focusComp = dataHandler.getFocussable();

            JComponent firstComp = focusComp == null ? dataHandler.getFirstFocussable() : focusComp;

            if (firstComp == null) {
                // if we have only readonly or metainfo useritems focus button
                getRootPane().getDefaultButton().requestFocusInWindow();
                return;
            }

            // search for a tabsheet:
            JTabbedPane tab = (JTabbedPane) SwingUtilities.getAncestorOfClass(JTabbedPane.class, firstComp);
            if (tab == null) {
                firstComp.requestFocusInWindow();
                return;
            }

            // if firstCompo is not on tab(0) -> activate tab(0)

            // find tabSheet of firstCompo
            int tabIdxOfFirst = -1;
            Component p = firstComp;
            while ((p = p.getParent()) != null) {
                if (p == tab) break;// never reached, but be shure
                tabIdxOfFirst = tab.indexOfComponent(p);
                if (tabIdxOfFirst != -1) break;// found
            }

            if (tabIdxOfFirst > 0) { // is not on tab#0 -> focus tab#0
                tab.setSelectedIndex(0);
                tab.requestFocusInWindow();
                return;
            }

            firstComp.requestFocusInWindow();
        }

        /**
         * call if the user clicks the [x]-Button of the window manager, or
         * presses escape
         */
        private void wantClose() {

            executeActionLoop(doActionChecked(new WindowActionImpl(WindowAction.WANT_CLOSE)));
        }

        // ###############################
        // now the action executing parts
        // ###############################

        /* we overwrite this to commit the data */
        protected Object executeWindowAction(WindowAction action) {
            log.info("begin " + action);
            if (WindowAction.SAVE_DATA.equals(action.getType())) {
                dataHandler.commitData();
                Object checkedAction = doActionChecked(new WindowActionImpl(WindowAction.DATA_SAVED));
                log.info("end");
                return checkedAction;
            } else if (WindowAction.SAVE_DATA_AND_CLOSE_WINDOW.equals(action.getType())) {
                dataHandler.commitData();
                Object checkedAction = doActionChecked(new WindowActionImpl(WindowAction.DATA_SAVED_AND_WINDOW_CLOSED));
                log.info("end");
                return checkedAction;
            }
            log.info("end");
            return super.executeWindowAction(action);
        }

        /* we have to forward the request to the dialog definition */
        protected Object doAction(Object action) throws BackendException {
            log.debug("action: " + action + " dlgDef=" + dlgDef);
            Object o = dlgDef.doAction(action, content);
            log.debug(o);
            return o;
        }

        public JButton[] getButtons() {
            return buttons;
        }

        public void setButtons(JButton[] buttons) {
            this.buttons = buttons;
        }

        /**
         * handles data change (check validation) and calculates first
         * focussable component
         *
         * @author PeterBuettner.de
         */
        private static abstract class PanelDataChangeListener
                extends CollectiveDataHandler
                implements DataChangeListener {

            public PanelDataChangeListener(JComponent content) {

                super(content);
                collectComponents();
                for (Iterator i = handlerIterator(); i.hasNext(); )
                    ((UIODataHandler) i.next()).addChangeListener(this);
            }

            protected void commitData() {

                super.commitData();
            }// make method visible here

            /**
             * gets the first visible + editable JComponent
             *
             * @return null if no visible &amp; editable one is found (we maybe
             * a messagebox)
             */
            protected JComponent getFirstFocussable() {

                // the iterator only delivers: visible &amp; editable !
                for (Iterator i = handlerIterator(); i.hasNext(); )
                    return ((UIODataHandler) i.next()).getComponent();

                return null;
            }

            protected JComponent getFocussable() {

                // the iterator only delivers: visible &amp; editable !
                for (Iterator i = handlerIterator(); i.hasNext(); ) {
                    UIODataHandler handler = (UIODataHandler) i.next();
                    if (handler.getUserInput().hasFocus())
                        return handler.getComponent();
                }
                return null;
            }

        }// ------------------------------------------------
    }

    //is, 13 Dec 2010
    private static class AlertInput extends Alert {
        private UserInputDialog dialog;
        private Window parent;

        private AlertInput(Frame parent, UserInputDialog dialog) {
            this.dialog = dialog;
            this.parent = parent;
            init();
        }

        private AlertInput(Dialog parent, UserInputDialog dialog) {
            this.dialog = dialog;
            this.parent = parent;
            init();
        }

        private void init() {
            setContentPane(dialog.getContentPane());
            setOwner(parent);

            JButton[] buttons = dialog.getButtons();
            for (JButton button : buttons) {
                log.debug(button.getText());
                if (ButtonItem.CLOSE.equals(button.getText())) {
                    button.addActionListener(new ActionListener() {
                        public void actionPerformed(ActionEvent e) {
                            hidePopupImmediately();
                        }
                    });
                }
            }

            setResizable(true);
            setMovable(true);
            setTransient(false);
            setPopupBorder(BorderFactory.createLineBorder(new Color(10, 30, 106)));

            CustomAnimation showAnimation = new CustomAnimation(CustomAnimation.TYPE_ENTRANCE, CustomAnimation.EFFECT_FLY,
                    CustomAnimation.SMOOTHNESS_MEDIUM, CustomAnimation.SPEED_MEDIUM);
            showAnimation.setDirection(CustomAnimation.BOTTOM);
            showAnimation.setVisibleBounds(PortingUtils.getLocalScreenBounds());
            setShowAnimation(showAnimation);

            CustomAnimation hideAnimation = new CustomAnimation(CustomAnimation.TYPE_ENTRANCE, CustomAnimation.EFFECT_FLY,
                    CustomAnimation.SMOOTHNESS_MEDIUM, CustomAnimation.SPEED_MEDIUM);
            hideAnimation.setDirection(CustomAnimation.BOTTOM);
            hideAnimation.setVisibleBounds(PortingUtils.getLocalScreenBounds());
            setHideAnimation(hideAnimation);
        }
    }
}

