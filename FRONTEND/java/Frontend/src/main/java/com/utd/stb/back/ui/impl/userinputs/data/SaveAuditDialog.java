package com.utd.stb.back.ui.impl.userinputs.data;

import com.utd.stb.back.ui.impl.lib.ButtonItemsImpl;
import com.utd.stb.back.ui.impl.lib.HelpSourceImpl;
import com.utd.stb.back.ui.impl.lib.StandardButton;
import com.utd.stb.back.ui.impl.lib.WindowActionImpl;
import com.utd.stb.back.ui.impl.msgboxes.WarningMsgBox;
import com.utd.stb.back.ui.impl.wizard.StandardReportWizard;
import com.utd.stb.inter.action.ButtonItem;
import com.utd.stb.inter.action.ButtonItems;
import com.utd.stb.inter.action.WindowAction;
import com.utd.stb.inter.application.BackendException;
import com.utd.stb.inter.dialog.HeaderBar;
import com.utd.stb.inter.dialog.HelpSource;
import com.utd.stb.inter.dialog.UserDialog;
import com.utd.stb.inter.userinput.UserInputs;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.util.ArrayList;


/**
 * Handles the forwarded user actions in dialogs of Wizard(save and audit
 * dialog).
 *
 * @author rainer bruns Created 14.12.2004
 * @UserInpuData
 * @see StandardReportWizard
 */
public class SaveAuditDialog implements UserDialog {
    public static final Logger log = LogManager.getRootLogger();

    private UserInputData ui;
    private String title;
    private ButtonItemsImpl buttons;
    private boolean okButtonPressed;
    private Object actionOnClosed = null;
    private boolean cancel;
    private HelpSource helpSource;
    private String windowActionAfterSaving = WindowAction.CLOSE_WINDOW;

    /**
     * @param ui    the list of data, this is the list of fields and their
     *              properties
     * @param title a Dialog title
     */
    public SaveAuditDialog(UserInputData ui, String title, boolean cancel, String hs) {
        this.ui = ui;
        this.title = title;
        this.cancel = cancel;
        this.helpSource = new HelpSourceImpl(hs);
        createButtonItems();
    }

    public void setWindowActionAfterSaving(String windowActionAfterSaving) {
        this.windowActionAfterSaving = windowActionAfterSaving;
    }

    /**
     * Need OK and Cancel Button
     *
     * @see ButtonItem
     */
    private void createButtonItems() {

        ArrayList list = new ArrayList();
        StandardButton b = new StandardButton(ButtonItem.OK, true);
        list.add(b);
        b = new StandardButton(ButtonItem.CANCEL, false);
        list.add(b);
        buttons = new ButtonItemsImpl(list);

    }

    public String getTitle() {

        return title;

    }

    public HeaderBar getHeaderBar() {

        return null;

    }

    public UserInputs getUserInputs() {

        return ui;

    }

    /*
     * (non-Javadoc) handle the forwarded actions, what can be a button pressed
     * or a window action, including DATA_SAVED window action
     * 
     * @see com.utd.stb.inter.dialog.UserDialog#doAction(java.lang.Object)
     */
    public Object doAction(Object action) throws BackendException {
        log.info("begin " + action);
        if (action instanceof ButtonItem) {

            ButtonItem b = (ButtonItem) action;

            // close dialog and return to last wizard step
            if (b.getType().equals(ButtonItem.CANCEL)) {

                if (cancel) {
                    ui.cancelAction(); //ok for wizard??
                }
                okButtonPressed = false;
                actionOnClosed = null;
                windowActionAfterSaving = WindowAction.NOT_CLOSE_WINDOW;

                return new WindowActionImpl(WindowAction.CLOSE_WINDOW);
            }

            //tell GUI to save data, means to write them tu back to
            // UserInputData
            else if (b.getType().equals(ButtonItem.OK)) {

                okButtonPressed = true;
                return new WindowActionImpl(WindowAction.SAVE_DATA);
            }
        } else if (action instanceof WindowAction) {

            WindowAction winac = (WindowAction) action;

            /*
             * GUI wrote back the data. If it was OK button save them to DB and
             * hold the next action Anyway, close the dialog
             */
            if (winac.getType().equals(WindowAction.DATA_SAVED)) {

                if (okButtonPressed) {
                    actionOnClosed = ui.saveData();
                }
                log.debug("actionOnClosed " + actionOnClosed);
                if (actionOnClosed instanceof WarningMsgBox || actionOnClosed instanceof SaveAuditDialog) {
                    return actionOnClosed;
                }
                return new WindowActionImpl(WindowAction.CLOSE_WINDOW);
            }

            //window is closed, maybe another dialog? Like audit after save
            else if (winac.getType().equals(WindowAction.WINDOW_CLOSED)) {
                log.debug("actionOnClosed: " + actionOnClosed + "; windowActionAfterSaving:" + windowActionAfterSaving);
                if (WindowAction.WINDOW_CLOSED.equals(windowActionAfterSaving)) {
                    return actionOnClosed;
                } else {
                    return new WindowActionImpl(windowActionAfterSaving);
                }
            }
        }

        return null;
    }

    public HelpSource getHelpSource() {

        return helpSource;

    }

    public ButtonItems getButtonItems() {

        return buttons;

    }

    public ButtonItem getDefaultButtonItem() {

        return buttons.get(0);

    }

}