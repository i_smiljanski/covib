package com.utd.stb.back.ui.impl.userinputs.fields;

import com.utd.stb.back.data.client.SelectionList;
import com.utd.stb.back.data.client.SelectionRecord;
import com.utd.stb.back.data.client.UserInputList;
import com.utd.stb.back.data.client.UserInputRecord;
import com.utd.stb.back.database.Dispatcher;
import com.utd.stb.inter.application.BackendException;


/**
 * Wraps the functionality of providing a list of values for
 * DisplayableItemUserInput fields
 *
 * @author rainer bruns Created 02.12.2004
 * @see DisplayableItemUserInput
 */
public class ListItemProvider {

    private Dispatcher dispatcher;

    /**
     * Creates the ListItemProvider with the Dispatcher to use for retrieving
     * the list.
     *
     * @param dispatcher
     */
    public ListItemProvider(Dispatcher dispatcher) {

        this.dispatcher = dispatcher;
    }

    /**
     * Gets the list of values from db. Creates UserInputObject from
     * SelectionRecord returned.
     *
     * @param parameterName the field for which to retrieve the list
     * @param notUsed       for extensions
     * @return the list of values for the field
     * @throws BackendException
     * @see Dispatcher#getLov(String, String)
     */
    public UserInputList getAvailableItems(String parameterName)
            throws BackendException {

        SelectionList list = null;
        UserInputList retList = new UserInputList();
        list = dispatcher.getLov(parameterName, "");
        list.initIterator();
        while (list.hasNext()) {
            SelectionRecord s = list.getNext();
            // make the UserInputObject from SelectionRecord. This is for GUI
            // Interface. GUI does not know about db stuff
            retList.add(new UserInputRecord(s.getValue(),
                    s.getDisplay(),
                    s.getInfo(),
                    s.getAttributeList()));
        }
        return retList;
    }
}