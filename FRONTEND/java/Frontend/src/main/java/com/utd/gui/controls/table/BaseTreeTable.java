package com.utd.gui.controls.table;

import com.utd.alpha.extern.sun.treetable.JTreeTable;
import com.utd.alpha.extern.sun.treetable.TreeTableModel;
import com.utd.gui.controls.ControlUtils;
import com.uptodata.isr.gui.util.Colors;

import javax.swing.*;
import java.awt.*;
import java.awt.event.FocusEvent;
import java.awt.event.FocusListener;


/**
 * changes some of the look: corner component if it is in a scrollpane (header
 * looks better) and 'lite hilite' selection if it has no focus
 * <p>
 * Fast hack, see BaseTable, later put it into a delegate class
 *
 * @author PeterBuettner.de
 */
public class BaseTreeTable extends JTreeTable {

    public BaseTreeTable(TreeTableModel dm) {

        super(dm);

        // autosize on dbl-header-click
        setAutoResizeMode(AUTO_RESIZE_OFF);
        getTableHeader().addMouseListener(new AutoSizeMouseAdapter(true));

        // background of selected cells is different with/without focus, so
        // repaint:
        addFocusListener(new FocusListener() {

            public void focusGained(FocusEvent e) {

                repaint();
            }

            public void focusLost(FocusEvent e) {

                repaint();
            }

        });

    }

    public Color getSelectionForeground() {

        if (!isShowLiteHilite()) return UIManager.getColor("Table.selectionForeground");
        return Colors.getItemHiliteInactiveText();

    }

    public Color getSelectionBackground() {

        if (!isShowLiteHilite()) return UIManager.getColor("Table.selectionBackground");
        return Colors.getItemHiliteInactiveBack();

    }

    /**
     * May be overwritten, if returns true a lite (e.g. lite gray) background is
     * shown, if false a darker (e.g. dark blue) one is used, textcolor is a
     * contrast one, normally a table shows darkHilite if table
     * {@link isFocusOwner()}or {@link isEditing()}
     *
     * @return
     */
    protected boolean isShowLiteHilite() {

        return !isFocusOwner() && !isEditing();

    }

    /*
     * overwritten to add a cornercomponent in the upper right
     */
    protected void configureEnclosingScrollPane() {

        super.configureEnclosingScrollPane();

        JScrollPane sp = ControlUtils.getEnclosingScrollPane(this);
        if (sp == null) return;
        sp.setCorner(JScrollPane.UPPER_RIGHT_CORNER, new CornerComponent());

    }
}