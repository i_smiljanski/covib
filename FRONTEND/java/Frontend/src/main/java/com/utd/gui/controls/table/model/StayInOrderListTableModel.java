package com.utd.gui.controls.table.model;

import java.util.*;


/**
 * <p>
 * Keeps the order of the data as the were inserted first on creation, so if you
 * remove and later re-add elements, they get their original place. Later added
 * (never seen) Objects get the 'last' position.
 * </p>
 * <p>
 * No duplicates please ( <b>IllegalStateException </b>), the order of the
 * objects is mapped by their hashValue() + equals(), so enshure that they don't
 * change those properties, see also {@link Map}
 * </p>
 * <p>
 * <p>
 * If you extend this class, be shure not to add any data directly to the
 * backing List
 * </p>
 * <p>
 * <br>
 * <b>No Synchronization is done! </b> <br>
 *
 * @author PeterBuettner.de
 */
public abstract class StayInOrderListTableModel extends AbstractListTableModel {

    /*
     * re-inserted are in original order, key method is is addRow(rowData,
     * noEvent)
     */

    private Map order = new HashMap();
    private Comparator orderComparator = new Comparator() {

        public int compare(Object o1, Object o2) {

            return getOrder(o1) - getOrder(o2);
        }
    };

    public StayInOrderListTableModel(Collection collection) {

        super(collection);
        initOrder(collection);

    }

    private void initOrder(Collection c) {

        int size = c.size();
        order = new HashMap((int) (size * 1.1f));
        Iterator it = c.iterator();
        int i = 0;
        while (it.hasNext())
            order.put(it.next(), new Integer(i++));

    }

    private int getOrder(Object rowData) {

        Object o = order.get(rowData);
        if (o == null) {
            o = new Integer(order.size());
            order.put(rowData, o);

        }

        return ((Integer) o).intValue();

    }

    public void addRow(Object rowData) {

        addRow(rowData, false);

    }

    private void addRow(Object rowData, boolean noEvent) {

        int pos = Collections.binarySearch(data, rowData, orderComparator);

        if (pos >= 0)
            // already here

            throw new IllegalStateException(
                    "Row is already in the list, no dupes please.");

        pos = -(pos + 1);
        data.add(pos, rowData);
        if (!noEvent) fireTableRowsInserted(pos, pos);

    }

    /**
     * clearly index is ignored, but so we shurely catch all of the baseclass
     * implementation
     */
    public void addRows(int index, List rows) {

        addRows(rows);

    }

    public void addRows(List rows) {

        if (rows.isEmpty()) return;

        for (Iterator i = rows.iterator(); i.hasNext(); )

            addRow(i.next(), true);

        fireTableDataChanged();

    }

}