package com.utd.gui.controls;

import com.jgoodies.forms.builder.DefaultFormBuilder;
import com.jgoodies.forms.factories.Borders;
import com.jgoodies.forms.layout.CellConstraints;
import com.jgoodies.forms.layout.FormLayout;
import com.uptodata.isr.gui.borders.SeparatorBorder;
import com.uptodata.isr.gui.util.HtmlText;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import javax.swing.*;
import javax.swing.border.CompoundBorder;
import java.awt.*;
import java.awt.font.LineMetrics;


/**
 * creates a JPanel like windows wizzards:
 * <p>
 * <pre>
 *
 *  -----------------------------------------------------------
 *  |                                              ---------   |
 *  |  Title Line (bold)                           |       |   |
 *  |       subtext, (plain text), foo Lorem       | icon  |   |
 *  |       ipsum dolor sit amet, consectetuer     |       |   |
 *  |       adipiscing nibh euismod tincidunt      ---------   |
 *  |                                                          |
 *  ------------------------------------------------------------
 *
 * </pre>
 * <p>
 * with white background (more exact: it is the window color, like the user
 * choosed). We set a special border (since this construct is almost on top now,
 * with a separator border on bottom.
 * <p>
 * TODO show elipse ('...') if subText is to long -> paint this 2 lines 'by
 * Hand'/ use 2 labels to control view
 */
public class TitleBar extends JPanel {
    public static final Logger log = LogManager.getRootLogger();

    private String title;
    private String subText;
    private Icon icon;
    private int prefSize;

    private JLabel lblTtitle;
    private JLabel lblImage;
    private JTextPane cmpSubtext;

    /**
     * @param title    the one line of bold text on top
     * @param subText  non bold text, maybe more than one line
     * @param icon     the image on the right side
     * @param prefSize 0 or below: enhance for resizable; 1 or above: minimum size
     *                 dialogUnits(~pixels) , may grow
     */

    public TitleBar(String title, String subText, Icon icon, int prefSize) {

        this.title = title;
        this.subText = subText;
        this.icon = icon;
        this.prefSize = prefSize;

        // ... init components ......................
        Font defFont = new JLabel().getFont(); // use label font as base
        //Font defFont = UIManager.getFont("Label.font")// ? maybe better?

        lblTtitle = new JLabel(title);
        lblTtitle.setFont(defFont.deriveFont(Font.BOLD)); // standard but
        // bold
        lblImage = new JLabel(icon);
        cmpSubtext = new JTextPane();
        cmpSubtext.setText(subText);
        cmpSubtext.setEditable(false);
        cmpSubtext.setFocusable(false);
        cmpSubtext.setBorder(null);// less space on top
        cmpSubtext.setOpaque(false);

        // ... build Layout .......................................
        FormLayout layout;
        if (prefSize < 1)
            layout = new FormLayout("L:10dlu, F:min:G, 5dlu, P", "T:P, T:P:G");
        else
            layout = new FormLayout("L:10dlu, F:max(" + prefSize + "dlu;min):G, 5dlu, P",
                    "T:P, T:P:G");

        DefaultFormBuilder builder = new DefaultFormBuilder(layout, this);
        CellConstraints cc = new CellConstraints();
        builder.add(lblTtitle, cc.xywh(1, 1, 2, 1));
        builder.add(cmpSubtext, cc.xywh(2, 2, 1, 1));
        builder.add(lblImage, cc.xywh(4, 1, 1, 2));
        // ......................................................

        setLayout(layout);
        setBackground(SystemColor.window);
        setBorder(new CompoundBorder(new SeparatorBorder(SwingConstants.BOTTOM),
                Borders.DIALOG_BORDER));
    }

    private final class MaxHeightLabel extends JTextPane {

        public Dimension getPreferredSize() {

            Font font = getFont();
            //inaccurate, doesn't work with lm.getHeight():
            if (getGraphics() != null) {
                LineMetrics lm = font.getLineMetrics("", ((Graphics2D) getGraphics()).getFontRenderContext());


                // swing does it this way, we too. May change later.
//            FontMetrics fm = getToolkit().getFontMetrics(font);

                Dimension d = super.getPreferredSize();
                //			int newHight = 2*Math.round(lm.getHeight()+0.5f);
                float newHight = 2 * lm.getHeight();
                if (d.height > newHight) {
                    setToolTipText("<html><body style='padding:2px;width:200px;'>"
                            + HtmlText.toHtml(getText()));
                } else
                    setToolTipText(null);
                d.height = (int) newHight;
                return d;
            }
            return super.getPreferredSize();
        }
    }

    public void setIcon(Icon icon) {

        this.icon = icon;
        lblImage.setIcon(icon);
    }

    public void setSubText(String subText) {

        this.subText = subText;
        cmpSubtext.setText(subText);
    }

    public void setTitle(String title) {

        this.title = title;
        lblTtitle.setText(title);
    }
}