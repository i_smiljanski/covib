package com.utd.gui.action;

import com.utd.gui.resources.I18N;

import javax.swing.*;
import java.util.Map;


public abstract class ActionCancel extends AbstractAction {

    public ActionCancel() {
        Map map = I18N.getActionMap(ActionCancel.class.getName());
        ActionUtil.initActionFromMap(this, map, null);
        putValue(SHORT_DESCRIPTION, null);
    }
}