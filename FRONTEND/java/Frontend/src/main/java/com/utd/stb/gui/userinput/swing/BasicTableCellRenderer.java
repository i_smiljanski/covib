package com.utd.stb.gui.userinput.swing;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import javax.swing.*;
import javax.swing.border.Border;
import javax.swing.border.EmptyBorder;
import javax.swing.table.TableCellRenderer;
import java.awt.*;
import java.io.Serializable;


/**
 * Mostly copied from DefaultTableCellRenderer, see there for the comments about
 * overwriting of methods for performance stripped here to get it small. <br>
 * Note that there are some changes in here.
 *
 * @author PeterBuettner.de
 */
public class BasicTableCellRenderer extends JLabel implements TableCellRenderer, Serializable {

    public static final Logger log = LogManager.getRootLogger();
    protected Border noFocusBorder = new EmptyBorder(1, 1, 1, 1);
    private Color unselForeground;
    private Color unselBackground;

    public BasicTableCellRenderer() {

        setOpaque(true);
        setBorder(noFocusBorder);
    }

    public Component getTableCellRendererComponent(JTable table, Object value,
                                                   boolean isSelected, boolean hasFocus,
                                                   int row, int column) {
        if (isSelected) {
            super.setForeground(table.getSelectionForeground());
            super.setBackground(table.getSelectionBackground());
        } else {
            super.setForeground((unselForeground != null) ? unselForeground : table.getForeground());
            super.setBackground((unselBackground != null) ? unselBackground : table.getBackground());
        }
        setFont(table.getFont());

        if (hasFocus) {
            setBorder(UIManager.getBorder("Table.focusCellHighlightBorder"));
            if (table.isCellEditable(row, column)) {
                super.setForeground(UIManager.getColor("Table.focusCellForeground"));
                super.setBackground(UIManager.getColor("Table.focusCellBackground"));
            }
        } else {
            setBorder(noFocusBorder);
        }

        setValue(value);

        return this;
    }

    protected void setValue(Object value) {

        setText((value == null) ? "" : value.toString());
    }

    // ---------- Helper methods -----------------------------------

    public void setForeground(Color c) {

        super.setForeground(c);
        unselForeground = c;
    }

    public void setBackground(Color c) {

        super.setBackground(c);
        unselBackground = c;
    }

    public void updateUI() {

        super.updateUI();
        setForeground(null);
        setBackground(null);
    }

    /*
     * following methods are overridden as a performance ...
     */

    public boolean isOpaque() {

        Color back = getBackground();
        Component p = getParent();
        if (p != null) p = p.getParent();

        // p should now be the JTable.
        boolean colorMatch = (back != null) && (p != null)
                && back.equals(p.getBackground()) && p.isOpaque();
        return !colorMatch && super.isOpaque();
    }

    public void validate() {

    }

    public void revalidate() {

    }

    public void repaint(long tm, int x, int y, int width, int height) {

    }

    public void repaint(Rectangle r) {

    }

    public void firePropertyChange(String propertyName, boolean oldValue, boolean newValue) {

    }

    protected void firePropertyChange(String propertyName, Object oldValue, Object newValue) {

        if (!propertyName.equals("text")) return; // Strings get interned...
        super.firePropertyChange(propertyName, oldValue, newValue);
    }

}

