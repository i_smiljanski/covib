package com.utd.stb.inter.userinput;

import com.utd.stb.back.data.client.PropertyRecord;
import com.utd.stb.inter.application.BackendException;

/**
 * TODO exceptions or silently eat it?
 *
 * @author PeterBuettner.de
 */
public class FileInputImpl implements FileInput {

    private PropertyRecord data;
    private String file;
    private boolean hasLov;

    /**
     * @param initialText
     * @throws BackendException
     */
    public FileInputImpl(PropertyRecord data) throws BackendException {

        this.data = data;
        this.hasLov = data.hasLov();
        setText(data.getValueAsString());
    }

    public PropertyRecord getProperty() {

        return data;
    }

    public String getText() {

        return file;
    }

    public boolean hasLov() {

        return hasLov;
    }

    public void setText(String text) {

        this.file = text;
    }
}