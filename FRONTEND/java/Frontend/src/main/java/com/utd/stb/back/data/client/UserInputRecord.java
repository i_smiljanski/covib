package com.utd.stb.back.data.client;

import com.utd.stb.inter.items.DisplayableItem;


/**
 * Implementation of DisplayableItem for dialogs
 *
 * @author rainer Created 10.10.2004
 */
public class UserInputRecord implements DisplayableItem {

    String value;
    String display;
    String info;
    AttributeList attributeList;

    /**
     * @param value         the value of a SelectionRecord
     * @param display       the display of a SelectionRecord
     * @param attributeList the attributeList of the SelectionRecord
     * @see SelectionRecord
     */
    public UserInputRecord(String value, String display, String info, Object attributeList) {

        this.value = value;
        this.display = display;
        this.info = info;
        this.attributeList = (AttributeList) attributeList;
    }

    /**
     * @param value   the value of a SelectionRecord
     * @param display the display of a SelectionRecord
     * @see SelectionRecord
     */
    public UserInputRecord(String value, String display, String info) {

        this.value = value;
        this.display = display;
        this.info = info;
    }

    /*
     * (non-Javadoc)
     * 
     * @see com.utd.stb.inter.items.DisplayableItem#getData() for GUI data is
     *      the display
     */
    public Object getData() {

        return display;
    }

    // 
    public String getValue() {

        return value;
    }

    /*
     * (non-Javadoc)
     * 
     * @see com.utd.stb.inter.items.DisplayableItem#getInfo() for GUI info is
     *      the value
     */
    public String getInfo() {

        return info;
    }

    /* (non-Javadoc)
     * @see com.utd.stb.inter.items.DisplayableItem#getAttributeList()
     */
    public Object getAttributeList() {

        return attributeList;
    }

    public String getDisplay() {
        return display;
    }

    public String toString() {

        return display;
    }

}