package com.utd.stb.back.ui.impl.userinputs.fields;

import com.jidesoft.converter.ConverterContext;
import com.jidesoft.grouper.DefaultObjectGrouper;
import com.utd.stb.inter.userinput.TextInput;
import com.utd.stb.inter.userinput.UserInput;

/**
 * @author Inna Smiljanski  created 10.05.11
 */
public class UserInputGrouper extends DefaultObjectGrouper {
    @Override
    public Object getValue(Object o) {
        String s = "";
        if (o instanceof TextUserInput && ((TextUserInput) o).getData() instanceof TextInput) {
            s = ((TextInput) ((TextUserInput) o).getData()).getText();
        }
        return s;
    }

    @Override
    public Class<?> getType() {
        return UserInput.class;
    }

    @Override
    public ConverterContext getConverterContext() {
        return UserInputConverter.CONTEXT;
    }
}
