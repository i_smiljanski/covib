package com.utd.stb.inter.action;

/**
 * A Collection of MenuItems
 *
 * @author PeterBuettner.de
 */
public interface MenuItems {

    /**
     * Number of MenuItem
     *
     * @return
     */
    int getSize();

    /**
     * get MenuItem with index i
     *
     * @param i
     * @return
     */
    MenuItem get(int i);

    /**
     * index in the Collection or -1 if not found
     *
     * @param menuItem
     * @return
     */
    int indexOf(MenuItem menuItem);

}