package com.utd.gui.controls.sidebar;

import javax.swing.*;
import javax.swing.plaf.SplitPaneUI;
import java.awt.*;


/**
 * Self registers the used UI, you may tweak this if you wan't to use another
 * UI.
 *
 * @author PeterBuettner.de
 */
public class SideBar extends JSplitPane {

    private static final String UI_NAME = "SideBarSplitPaneUI";

    static {
        UIManager.put(UI_NAME, SideBarSplitPaneUI.class.getName());
    }

    /*
     * we need to make the 'left content' invisible to remove focus if it is
     * minimized, but there needs to be a visible container or we can't mov the
     * splitter above 0. So use an intermediate container
     */
    private JPanel iLeftCompo = new JPanel(new BorderLayout()) {

        public Dimension getPreferredSize() {

            Component c = getComponent(0);
            if (c == null)
                return super.getPreferredSize();
            else
                return c.getPreferredSize();

        }

    };

    public String getUIClassID() {

        return UI_NAME;
    }

    public SideBar(int orientation, boolean continuousLayout, JComponent left,
                   JComponent right) {

        super(orientation, continuousLayout, null, right);
        setLeftComponent(iLeftCompo);
        iLeftCompo.add(left);

        setOneTouchExpandable(true);
        setBorder(null);
        setDividerSize(8);

    }

    public void setLeftComponent(Component comp) {

        if (comp == iLeftCompo) {

            super.setLeftComponent(iLeftCompo);
            return;
        }

        iLeftCompo.removeAll();
        iLeftCompo.add(comp);

    }

    /**
     * Sets some properties of the button on the Divider: reads ACCELERATOR_KEY
     * and SHORT_DESCRIPTION from template, rest is ignored.
     *
     * @param template
     */
    public void configureFromAction(Action template) {

        SplitPaneUI ui = getUI();

        if (!(ui instanceof SideBarSplitPaneUI)) return;

        Component div = ((SideBarSplitPaneUI) ui).getDivider();

        if (!(div instanceof SidebarSplitPaneDivider)) return;

        ((SidebarSplitPaneDivider) div).configureFromAction(template);
    }

}