package com.utd.stb.inter.userinput;

/**
 * Metainfo for UserInputObject-collections, use to separate groups of inputs
 * into titled groups or put them into different tabsheets, all following UIO in
 * the collection will be 'under' the 'last' group. <br>
 * If you use Tabsheets don't forget to have an initial one, or the
 * ComponentFactory creates a default 'unnamed' one for you. Groups may be used
 * without an initial one, since they probably result in a separator/ruler/...,
 * if it has no label it will be a line/box/etc. without title
 *
 * @author PeterBuettner.de
 */
public interface MetaInput {

    /**
     * a Group is probably a separator with the labal as title, but also a box
     * around the controls.
     */
    public static final String GROUP = "Group";

    /**
     * A Tabsheet is a Tabsheet, but it may also rendered as 'separate Pages',
     * imagin a html Interface where the Tabs are all in a column one below the
     * other, visible all together (by scrolling)
     */
    public static final String TABSHEET = "TabSheet";

    /**
     * Floating text, will fill the space of label and the component column,
     * don't use html here, since the gui probably have to convert it to html.
     * The label is the text to be shown.
     */
    public static final String FLOATING_TEXT = "FloatingText";

    /**
     * Html text, will fill the space of label and the component column, use
     * html here, but no &lt;html&gt;, &lt;body&gt; tags. Note that only
     * minimalistic html is supported, and that all &lt;style&gt; tags maybe
     * removed if we render this on a Browserbased html gui. use
     * B,I,U,FONT,TABLE,...
     */
    public static final String HTML_TEXT = "HtmlText";

    /**
     * Use
     * <p>
     * <pre>
     *
     *  MetaInput.TABSHEET
     *  MetaInput.GROUP
     *  MetaInput.FLOATING_TEXT
     *
     * </pre>
     *
     * @return
     */
    String getType();

}