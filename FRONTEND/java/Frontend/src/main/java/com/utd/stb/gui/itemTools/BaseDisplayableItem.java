package com.utd.stb.gui.itemTools;

import com.utd.stb.inter.items.DisplayableItem;

import javax.swing.*;


/**
 * Usefull if you don't have your own Class and need only simple data
 * Scheint nicht benutzt zu werden
 *
 * @author PeterBuettner.de
 */
public class BaseDisplayableItem implements DisplayableItem {

    Object name;
    String info;
    Icon icon;

    public BaseDisplayableItem(Object name, String info) {

        this.name = name;
        this.info = info;
    }

    public Object getData() {

        return name;
    }

    public String getInfo() {

        return info;
    }

    public String toString() {

        return name + " :: " + info;
    }

    /* (non-Javadoc)
     * @see com.utd.stb.inter.items.DisplayableItem#getAttributeList()
     */
    public Object getAttributeList() {
        return null;
    }
}