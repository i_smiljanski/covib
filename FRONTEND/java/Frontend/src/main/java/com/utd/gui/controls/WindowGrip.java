package com.utd.gui.controls;

import com.utd.gui.icons.GripIcon;
import com.uptodata.isr.gui.util.Screen;
import com.uptodata.isr.gui.awt.wind.hwnd.IWindowGripCanvas;

import javax.swing.*;
import javax.swing.event.MouseInputAdapter;
import java.awt.*;
import java.awt.event.MouseEvent;


/**
 * Component with an Icon like the grip in Windows windows in the bottom-right
 * corner to size a window. Let the user size the topmost Window it contains.
 * _TODO flickers :-) Not anymore _TODO user may size with grip outside of
 * screen, may not reach the grip after
 *
 * @author PeterBuettner.de
 */
public class WindowGrip extends JLabel {

    private void tryLoadCanvas() {

        // if not linked or 'secutity is an issue' then work without
        try {
            Class c = Class.forName("com.uptodata.isr.gui.hwnd.WindowGripCanvas");
            if (c != null) windowGripCanvas = (IWindowGripCanvas) c.newInstance();
        } catch (Throwable e) {
            // MCD, 18.Jan 2006
            e.printStackTrace();
        }

        if (windowGripCanvas instanceof Component) {
            Component canvas = (Component) windowGripCanvas;
            Dimension d = getPreferredSize();
            canvas.setBounds(d.width - 1, d.height - 1, 1, 1);
            add(canvas);
        }
    }

    private IWindowGripCanvas windowGripCanvas;

    /**
     * rel to Screen
     */
    private Point lastDown;
    private Point offsetDown;

    /**
     * uses default size of 16
     */
    public WindowGrip() {

        this(16);
    }

    public WindowGrip(int size) {

        super(new GripIcon(size));
        tryLoadCanvas();

        setCursor(Cursor.getPredefinedCursor(Cursor.SE_RESIZE_CURSOR));

        MouseInputAdapter mia = new MouseInputAdapter() {

            public void mousePressed(MouseEvent e) {

                //				SwingUtilities.getAncestorOfClass(JRootPane.class,WindowGrip.this).setVisible(false);
                sizeStart(e);
            }

            public void mouseReleased(MouseEvent e) {

                sizeEnd();
                //				SwingUtilities.getAncestorOfClass(JRootPane.class,WindowGrip.this).setVisible(true);
                getWindow().validate();
            }

            public void mouseDragged(MouseEvent e) {

                sizeSet(e);
                if (windowGripCanvas == null) getWindow().validate();
            }
        };

        addMouseListener(mia);
        addMouseMotionListener(mia);

    }

    private void sizeStart(MouseEvent e) {

        lastDown = toScreen(e);
        Container wnd = getWindow();
        Point wndLoc = wnd.getLocationOnScreen();
        Dimension size = wnd.getSize();
        offsetDown = new Point(size.width - (lastDown.x - wndLoc.x),
                size.height - (lastDown.y - wndLoc.y));

    }

    private void sizeEnd() {

        lastDown = null;
        offsetDown = null;

    }

    private void sizeSet(MouseEvent e) {

        if (lastDown == null) return;

        Point curPos = toScreen(e);
        Container wnd = getWindow();
        Point wndLoc = wnd.getLocationOnScreen();
        int w = curPos.x - wndLoc.x + offsetDown.x;
        int h = curPos.y - wndLoc.y + offsetDown.y;

        Rectangle b = Screen.getScreenBounds(wnd);
        if (h < 50) h = 50;
        if (w < 50) w = 50;
        if (wndLoc.y + h > b.y + b.height) h = b.y + b.height - wndLoc.y;
        if (wndLoc.x + w > b.x + b.width) w = b.x + b.width - wndLoc.x;

        if (windowGripCanvas != null)
            noFlickerSize(wndLoc, w, h);
        else
            wnd.setSize(w, h);// flickers
    }

    private void noFlickerSize(Point wndLoc, int w, int h) {

        windowGripCanvas.setWindowPos(windowGripCanvas.getWindowHandle(), 0, wndLoc.x,
                wndLoc.y, w, h, IWindowGripCanvas.SWP_NOACTIVATE
                        | IWindowGripCanvas.SWP_NOZORDER);
    }

    private Point toScreen(MouseEvent e) {

        return toScreen((e.getPoint()));

    }

    private Point toScreen(Point pt) {

        pt = new Point(pt);//clone
        SwingUtilities.convertPointToScreen(pt, WindowGrip.this);
        return pt;

    }

    private Container getWindow() {

        Container root = SwingUtilities.getAncestorOfClass(JRootPane.class, this);
        return root.getParent();

    }

    public static void main(String[] args) {

        Toolkit.getDefaultToolkit().setDynamicLayout(true);
        JFrame f = new JFrame("Test");
        f.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        JLabel red = new JLabel("");
        red.setBackground(Color.RED);
        red.setOpaque(true);
        JPanel p = new JPanel(new BorderLayout());
        JLabel blue = new JLabel("");
        blue.setBackground(Color.BLUE);
        blue.setOpaque(true);
        p.add(blue);
        p.add(new WindowGrip(16), "South");
        f.getContentPane().add(red);
        f.getContentPane().add(p, "East");
        f.setBounds(100, 100, 200, 150);
        f.setVisible(true);
    }

}