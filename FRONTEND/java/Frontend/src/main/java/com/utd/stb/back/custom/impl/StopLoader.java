/*
 * Created on 22.12.2004
 *
 */
package com.utd.stb.back.custom.impl;

import com.utd.stb.back.custom.basic.CustomAction;
import com.utd.stb.back.data.client.TreeNodeRecord;

import java.io.IOException;
import java.io.OutputStream;
import java.net.Socket;


/**
 * @author rainer Created 22.12.2004
 */
public class StopLoader implements CustomAction {

    public CustomAction execute(TreeNodeRecord node) {

        try {
            Socket s = new Socket("127.0.0.1", 3334);
            OutputStream out = s.getOutputStream();
            out.write(2);
            out.flush();
            out.close();
            s.close();
        } catch (IOException ie) {
            ie.printStackTrace();
        }
        return null;
    }
}