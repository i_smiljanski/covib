package com.utd.stb.gui.swingApp;

import java.lang.reflect.InvocationHandler;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.lang.reflect.Proxy;


/**
 * wrap an interface to be informed before and after each method call, note that
 * also equals and hashCode gets intercepted, so better not sort a large
 * collection of wrapped interfaces.
 * <p>
 * may use this to intercept all calls to the base: show cursor, en/disable
 * components, use a separate thread to have the Application responsive Source:
 * {@link http://java.sun.com/developer/technicalArticles/JavaLP/Interposing/}
 * <br>
 * tested: works fine
 */
abstract class PrePostProxy implements InvocationHandler {

    /**
     * create a new Proxy
     *
     * @param inter the interface whose calls shoud be wrapped
     * @param proxy the prebuild PrePostProxy
     * @return the Proxy cast it with your interface
     */
    public static Object wrap(Class inter, PrePostProxy proxy) {

        return Proxy.newProxyInstance(inter.getClassLoader(), new Class[]{inter}, proxy);
    }

    private Object target;

    /**
     * @param target calls are delegated to this instance of 'your interface'
     */
    public PrePostProxy(Object target) {

        this.target = target;
    }

    public Object invoke(Object proxy, Method m, Object[] args) throws Throwable {

        Object result;
        try {
            pre(m);
            result = m.invoke(target, args);
        } catch (InvocationTargetException e) {
            throw e.getTargetException();
        } catch (Exception e) {
            throw new RuntimeException("Unexpected invocation exception: " + e.getMessage());
        } finally {
            post(m);
        }
        return result;
    }

    /**
     * called before invocation
     *
     * @param method what will be called, just compare it never invoke
     */
    abstract protected void pre(Method method);

    /**
     * called after invocation, even if an exception occured,
     *
     * @param method what was called, just compare it never invoke
     */
    abstract protected void post(Method method);

}