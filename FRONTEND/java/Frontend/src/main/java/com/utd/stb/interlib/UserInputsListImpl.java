package com.utd.stb.interlib;

import com.utd.stb.inter.userinput.UserInput;
import com.utd.stb.inter.userinput.UserInputs;

import java.util.List;


/**
 * Wraps UserInputs around a list
 *
 * @author PeterBuettner.de
 */
public class UserInputsListImpl implements UserInputs {

    private final List ui;

    public UserInputsListImpl(List ui) {

        this.ui = ui;
    }

    public int getSize() {

        return ui.size();
    }

    public UserInput get(int index) {

        return (UserInput) ui.get(index);
    }

    public int indexOf(UserInput item) {

        return ui.indexOf(item);
    }


}