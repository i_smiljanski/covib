package com.utd.gui.action;

import com.utd.gui.resources.I18N;

import javax.swing.*;


public abstract class ActionOk extends AbstractAction {

    public ActionOk() {

        ActionUtil.initActionFromMap(this, I18N.getActionMap(ActionOk.class.getName()),
                null);
        putValue(SHORT_DESCRIPTION, null);
    }

}