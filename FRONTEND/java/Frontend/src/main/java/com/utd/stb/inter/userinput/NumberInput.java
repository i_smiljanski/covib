package com.utd.stb.inter.userinput;

/**
 * To have good results use Long and especially Double, never Float.
 * <p>
 * <p>
 * Note that (besides of Long/Int/Short/Byte) we never round numbers since
 * float/double can't be rounded exactly (1.234 may resolve to
 * 1.2339999999999999)
 * <p>
 * <p>
 * think about using BigDecimal later.
 * <p>
 *
 * @author PeterBuettner.de
 */
public interface NumberInput {

    /**
     * get the current value, see Note in class {@link NumberInput}, maybe of
     * different type (larger) than the number set: (Byte/Short/Integer -> Long)
     * (Float -> Double)
     *
     * @return
     */
    Number getNumber();

    /**
     * set the current value, see Note in class {@link NumberInput}
     *
     * @param n
     */
    void setNumber(Number n);

    /**
     * the Number class type that should be used, neccessary if data is null and
     * we need to limit the min/max bsed on type
     * (Byte,Short,Integer,Long,Float,Double)
     *
     * @return
     */
    Class getType();

    /**
     * The number of decimal-digits after the 'comma', mostly used to setup the
     * gui editors, you can't expect to have the value from getNumber() to be
     * rounded: see Note in class {@link NumberInput}
     * <p>
     * <pre>
     *
     *  null  no check
     *  0     an integer
     *  2     something like ####,##
     *  -2    something like ##00
     *
     * </pre>
     *
     * @return
     */
    Integer getPrecision();

    /**
     * Minimum allowed value, null if no limit
     *
     * @return
     */
    Number getMin();

    /**
     * Maximum allowed value, null if no limit
     *
     * @return
     */
    Number getMax();

}