package com.utd.stb.back.ui.impl.userinputs.fields;

import com.jidesoft.filter.*;
import com.utd.stb.inter.application.Node;
import com.utd.stb.inter.userinput.TextInput;
import com.utd.stb.inter.userinput.UserInput;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.util.Locale;

/**
 * @author Inna Smiljanski
 *         Date: 21.04.11
 */
public class UserInputFilterFactoryManager extends FilterFactoryManager {
    public static final Logger log = LogManager.getRootLogger();

    public static boolean isInstanceOfTextInput(Object o) {
        return o instanceof TextUserInput &&
                ((TextUserInput) o).getData() instanceof TextInput;
    }

    @Override
    public void registerDefaultFilterFactories() {
        // log.debug("registerDefaultFilterFactories");

        //AllFilter
        registerFilterFactory(UserInput.class, new FilterFactory() {
            public com.jidesoft.filter.Filter createFilter(Object... objects) {
                return new AllFilter<UserInput>();
            }

            public Class[] getExpectedDataTypes() {
                return new Class[]{UserInput.class};
            }

            public String getName() {
                return "all";
            }

            public String getConditionString(Locale locale) {
                return com.jidesoft.filter.AbstractFilter.getConditionString(locale, "string", getName());
            }
        });

        //EqualFilter
        registerFilterFactory(UserInput.class, new FilterFactory() {
            public com.jidesoft.filter.Filter createFilter(Object... objects) {
                if (isInstanceOfTextInput(objects[0])) {
                    String s = ((TextInput) ((TextUserInput) objects[0]).getData()).getText();
                    WildcardFilter beginWith = new WildcardFilter(s);
//                    log.debug(beginWith.getPattern() + "; " + s);
                    beginWith.setBeginWith(true);
                    beginWith.setEndWith(true);
                    return beginWith;
                }
                return new EqualFilter<UserInput>((UserInput) objects[0]);
            }

            public Class[] getExpectedDataTypes() {
                return new Class[]{UserInput.class};
            }

            public String getName() {
                return "equal";
            }

            public String getConditionString(Locale locale) {
                return com.jidesoft.filter.AbstractFilter.getConditionString(locale, "string", getName());
            }
        });

        //NotEqualFilter
        registerFilterFactory(UserInput.class, new FilterFactory() {
            public com.jidesoft.filter.Filter createFilter(Object... objects) {
                return new NotEqualFilter<UserInput>((UserInput) objects[0]);
            }

            public Class[] getExpectedDataTypes() {
                return new Class[]{UserInput.class};
            }

            public String getName() {
                return "not.equal";
            }

            public String getConditionString(Locale locale) {
                return com.jidesoft.filter.AbstractFilter.getConditionString(locale, "string", getName());
            }
        });

        //EmptyFilter
        registerFilterFactory(UserInput.class, new FilterFactory() {
            public com.jidesoft.filter.Filter createFilter(Object... objects) {
                return new EmptyFilter<UserInput>();
            }

            public Class[] getExpectedDataTypes() {
                return new Class[]{UserInput.class};
            }

            public String getName() {
                return "empty";
            }

            public String getConditionString(Locale locale) {
                return com.jidesoft.filter.AbstractFilter.getConditionString(locale, "string", getName());
            }
        });

        //NotFilter
        registerFilterFactory(UserInput.class, new FilterFactory() {
            public com.jidesoft.filter.Filter createFilter(Object... objects) {
                return new NotFilter(new EmptyFilter<UserInput>());
            }

            public Class[] getExpectedDataTypes() {
                return new Class[]{UserInput.class};
            }

            public String getName() {
                return "not.empty";
            }

            public String getConditionString(Locale locale) {
                return com.jidesoft.filter.AbstractFilter.getConditionString(locale, "string", getName());
            }
        });

        //WildcardFilter begin
        registerFilterFactory(UserInput.class, new FilterFactory() {
            public com.jidesoft.filter.Filter createFilter(Object... objects) {
                if (isInstanceOfTextInput(objects[0])) {
                    String s = ((TextInput) ((TextUserInput) objects[0]).getData()).getText();
                    WildcardFilter beginWith = new WildcardFilter("*" + s + "*");
//                    log.debug(beginWith.getPattern() + "; " + s);
                    beginWith.setBeginWith(true);
                    beginWith.setEndWith(false);
                    return beginWith;
                }
                return new AllFilter<UserInput>();
            }

            public Class[] getExpectedDataTypes() {
                return new Class[]{UserInput.class};
            }

            public String getName() {
                return "beginWith";
            }

            public String getConditionString(Locale locale) {
                return com.jidesoft.filter.AbstractFilter.getConditionString(locale, "string", getName());
            }
        });

        //Node
        //AllFilter
        registerFilterFactory(Node.class, new FilterFactory() {
            public com.jidesoft.filter.Filter createFilter(Object... objects) {
                return new AllFilter<Node>();
            }

            public Class[] getExpectedDataTypes() {
                return new Class[]{Node.class};
            }

            public String getName() {
                return "all";
            }

            public String getConditionString(Locale locale) {
                return com.jidesoft.filter.AbstractFilter.getConditionString(locale, "string", getName());
            }
        });

        registerFilterFactory(Node.class, new FilterFactory() {
            public com.jidesoft.filter.Filter createFilter(Object... objects) {
                log.debug((Node) objects[0]);
                return new EqualFilter<Node>((Node) objects[0]);
            }

            public Class[] getExpectedDataTypes() {
                return new Class[]{Node.class};
            }

            public String getName() {
                return "equal";
            }

            public String getConditionString(Locale locale) {
                return com.jidesoft.filter.AbstractFilter.getConditionString(locale, "string", getName());
            }
        });


        super.registerDefaultFilterFactories();




       /*
        for (int i = 0; i < getFilterFactories(String.class).size(); i++) {

            final FilterFactory stringFactory = getFilterFactories(String.class).get(i);
            log.debug(i + " " + stringFactory.getConditionString(new Locale("EN")) + "; " + stringFactory.getName());


            registerFilterFactory(UserInput.class, new FilterFactory() {

                public com.jidesoft.filter.Filter createFilter(Object... objects) {
                    if (objects[0] instanceof TextUserInput) {
                        String sObject = ((TextInput) ((TextUserInput) objects[0]).getData()).getTranslation();
                        if ("all".equals(stringFactory.getName()))
                            return new AllFilter<UserInput>();
                        if ("equal".equals(stringFactory.getName()))
                            return new EqualFilter<UserInput>((UserInput) objects[0]);
                        if ("not.equal".equals(stringFactory.getName()))
                            return new NotEqualFilter<UserInput>((UserInput) objects[0]);
//                        if("in".equals(stringFactory.getName()))
//                            return new InFilter<Object>((Object[]) objects[0]);
//                        if("not.in".equals(stringFactory.getName()))
//                            return new NotFilter(new InFilter<Object>((Object[]) objects[0]));
                        if ("empty".equals(stringFactory.getName()))
                            return new EmptyFilter<UserInput>();
                        if ("not.empty".equals(stringFactory.getName()))
                            return new NotFilter(new EmptyFilter<UserInput>());

                        if ("beginWith".equals(stringFactory.getName())) {
                            WildcardFilter beginWith = new WildcardFilter<String>(sObject);
                            log.debug(beginWith.getPattern() + "; " + sObject);
                            beginWith.setBeginWith(true);
                            beginWith.setEndWith(false);
                            return beginWith;
                        }


//                        if("endWith".equals(stringFactory.getName()))
//                            return new EqualFilter<UserInput>((UserInput) objects[0]);
//                        if("contain".equals(stringFactory.getName()))
//                            return new EqualFilter<UserInput>((UserInput) objects[0]);
//                        if("not.contain".equals(stringFactory.getName()))
//                            return new EqualFilter<UserInput>((UserInput) objects[0]);
                    }
                    return new AllFilter<UserInput>();
                }

                public String getConditionString(Locale locale) {
                    // return AbstractFilter.getConditionString(locale, "string", "beginWith");
                    return stringFactory.getConditionString(locale);
                }

                public String getName() {
                    return stringFactory.getName();
                }

                public Class[] getExpectedDataTypes() {
                    return new Class[]{UserInput.class};
                }

            });
        }*/
    }


}
