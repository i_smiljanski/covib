package com.utd.alpha.extern.sun.treetable;

import javax.swing.*;
import javax.swing.event.MouseInputAdapter;
import javax.swing.tree.TreePath;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.MouseEvent;


/**
 * This example shows how to create a simple JTreeTable component, by using a
 * JTree as a renderer (and editor) for the cells in a particular column in the
 * JTable. No editing for now
 * <p>
 * Heavyly hacked by Peter Buettner, removed 'Tree paints cells', table does the
 * tree-painting by itself. Needs: expanded/collapsed icon of <b>same </b> size
 *
 * @author PeterBuettner.de
 */
public class JTreeTable extends JTable {

    //WindowsLookAndFeel
    //BasicLookAndFeel

    /**
     * It is used as a data holder only
     */
    private JTree tree;

    /**
     * paints control handles, icons and text, knows what is hit by a point
     */
    private TreeTableCellRenderer tcr;

    // TODOlater may remove this get/setTreeTableCellRenderer, maybe let the
    // user set/access one? maybe set a delegate is better? or place the
    // delegate only in the 'non-tree-handle' part
    public TreeTableCellRenderer getTreeTableCellRenderer() {

        return tcr;
    }

    public void setTreeTableCellRenderer(TreeTableCellRenderer tcr) {

        this.tcr = tcr;
        setDefaultRenderer(TreeTableModel.class, tcr);
    }

    /**
     * expand/collapse/toggle
     */
    private MouseHandler mouseHandler = new MouseHandler();

    public JTreeTable(TreeTableModel treeTableModel) {

        setSelectionModel(new MyLSL());

        tree = new JTree(treeTableModel);
        tree.setRootVisible(true);
        tree.setShowsRootHandles(true);

        // Installs a tableModel representing the visible rows in the tree.
        super.setModel(new TreeTableModelAdapter(treeTableModel, tree));

        tcr = new TreeTableCellRenderer(this);
        setDefaultRenderer(TreeTableModel.class, tcr);
        //		setDefaultEditor(TreeTableModel.class, new TreeTableCellEditor());

        setShowGrid(false);
        setIntercellSpacing(new Dimension(0, 0));
        //		setRowHeight(40);

        // And update the height of the trees row to match that of the table.
        //		if (tree.getRowHeight() < 1) setRowHeight(20); // Metal looks better
        // like this.

        addExtraKeys();

        addMouseListener(mouseHandler);
        addMouseMotionListener(mouseHandler);
        updateUI();// so we get informed before the ui and may consume
        // mouseEvents
    }

    /**
     * Overridden to message super and forward the method to the tree. Since the
     * tree is not actually in the component hierarchy it will never receive
     * this unless we forward it in this manner.
     */
    public void updateUI() {

        super.updateUI();
        if (tree != null) tree.updateUI();
        if (tcr != null) tcr.updateUI();
    }

    private int getTreeColumn() {

        for (int i = 0, len = getColumnCount(); i < len; i++)
            if (getColumnClass(i).equals(TreeTableModel.class)) return i;
        return -1;
    }

    /**
     * prepares the renderer and calculates the hitcode, see
     * {@link TreeTableCellRenderer.getHitCode(int)}
     *
     * @param x   relative to the table,
     * @param row
     * @return
     */
    public int getHitCode(int x, int row) {

        int iTCol = getTreeColumn();
        if (iTCol == -1) return TreeTableCellRenderer.HIT_NONE;

        for (int i = 0; i < iTCol; i++) {
            x -= columnModel.getColumn(i).getWidth();
            if (x < 0) return TreeTableCellRenderer.HIT_NONE; // column on
            // left, fail as
            // early as
            // possible
        }
        // now x is relative to the TreeColumn
        if (columnModel.getColumn(iTCol).getWidth() < x)
            return TreeTableCellRenderer.HIT_NONE;// column
        // not
        // hit

        tcr.getTableCellRendererComponent(JTreeTable.this, "dummy", false, false, row, iTCol);
        return tcr.getHitCode(x);
    }

    /**
     * Hanlde clicks and dbl-clicks in the TreeHandles and the Icon
     */
    private class MouseHandler extends MouseInputAdapter {

        private boolean consume;

        public void mousePressed(MouseEvent e) {

            consume = handle(e);
            if (consume) e.consume();
        }

        public void mouseReleased(MouseEvent e) {

            /*
             * don't rely on handle(), since a release event on a row without a
             * handleIcon '[+]' would change selection
             */
            if (consume) e.consume();
            consume = false;
        }

        public void mouseDragged(MouseEvent e) {

            if (consume) e.consume();
        }

        private boolean handle(MouseEvent e) {

            if (!SwingUtilities.isLeftMouseButton(e)) return false;

            int row = rowAtPoint(e.getPoint());
            if (row == -1) return false; // below, beside

            switch (getHitCode(e.getX(), row)) {
                case TreeTableCellRenderer.HIT_HANDLE:
                    if (e.getID() == MouseEvent.MOUSE_PRESSED) toggleTree(row);
                    return true;

                case TreeTableCellRenderer.HIT_ICON: // PRESSED: we react on 2nd
                    // down
                    if (e.getID() == MouseEvent.MOUSE_PRESSED && e.getClickCount() % 2 == 0)
                        toggleTree(row);
                    return true;
            }
            return false;
        }

        private void toggleTree(int row) {

            if (tree.isExpanded(row))
                tree.collapseRow(row);
            else
                tree.expandRow(row);
        }
    }

    /**
     * with this special model new (expanded) rows are not selected, also fixes
     * non-adjusting anchor
     */
    private static class MyLSL extends DefaultListSelectionModel {

        public void insertIndexInterval(int index, int length, boolean before) {

            int anchor = getAnchorSelectionIndex();// save anchor
            super.insertIndexInterval(index, length, before);

            int min = (before) ? index : index + 1;
            int max = (min + length) - 1;

            // super only selects if index is selected:
            if (isSelectedIndex(index)) removeSelectionInterval(min, max);

            if (anchor >= min)
                setAnchorSelectionIndex(anchor + length);
            else
                setAnchorSelectionIndex(anchor);// they have it really broken?
        }

        public void removeIndexInterval(int index0, int index1) {

            // purpose: fix anchor
            int anchor = getAnchorSelectionIndex();// save anchor
            super.removeIndexInterval(index0, index1);

            int min = Math.min(index0, index1);
            int max = Math.max(index0, index1);
            int length = (max - min) + 1;

            if (anchor < min) return;

            if (anchor <= max) {// between
                /*
                 * set the anchor to the one before min. after max would be
                 * better but we can not access the 'last available index' -
                 * that probably doesn't exist for a SelectionModel
                 */
                setAnchorSelectionIndex(Math.max(0, min - 1));
            } else
                setAnchorSelectionIndex(anchor - length);// after
        }
    }

    /**
     * expand and collapse with cursor and +/- keys
     */
    private void addExtraKeys() {

        ActionMap am = getActionMap();
        am.put("PEBExpand", new ExpandCurrentAction(tree, this, true));
        am.put("PEBCollapse", new ExpandCurrentAction(tree, this, false));
        InputMap im = getInputMap();
        im.put(KeyStroke.getKeyStroke("PLUS"), "PEBExpand");
        im.put(KeyStroke.getKeyStroke("ADD"), "PEBExpand");// NumPad
        im.put(KeyStroke.getKeyStroke("RIGHT"), "PEBExpand");

        im.put(KeyStroke.getKeyStroke("MINUS"), "PEBCollapse");
        im.put(KeyStroke.getKeyStroke("SUBTRACT"), "PEBCollapse");// NumPad
        im.put(KeyStroke.getKeyStroke("LEFT"), "PEBCollapse");
    }

    /**
     * expand/collapse, may select next/prev row if current can't be
     * expanded/collapsed
     */
    private static class ExpandCurrentAction extends AbstractAction {

        private boolean expand;
        private JTree tree;
        private JTable table;

        ExpandCurrentAction(JTree tree, JTable table, boolean expand) {

            this.expand = expand;
            this.tree = tree;
            this.table = table;
        }

        public void actionPerformed(ActionEvent e) {
            int row = table.getSelectionModel().getLeadSelectionIndex();
            //		int row = table.getSelectionModel().getAnchorSelectionIndex();

            if (row < 0) return;
            boolean isExpanded = tree.isExpanded(row);
            if (expand) {
                if (isExpanded)
                    selectNextRow(row);
                else {
                    if (isLeaf(row))
                        selectNextRow(row);
                    else
                        tree.expandRow(row);
                }
            } else {
                if (isExpanded)
                    tree.collapseRow(row);
                else
                    selectParentRow(row);
            }
        }

        private void selectNextRow(int row) {

            if (row < tree.getRowCount() - 1)
                table.changeSelection(row + 1, -1, false,
                        false);
        }

        /**
         * sets a <b>new </b> selection, scrolls to be visible
         */
        private void selectParentRow(int row) {

            TreePath path = tree.getPathForRow(row);
            if (path == null) return;
            path = path.getParentPath();
            if (path == null) return;
            row = tree.getRowForPath(path);
            if (row != -1) table.changeSelection(row, -1, false, false);
        }

        private boolean isLeaf(int row) {

            TreePath path = tree.getPathForRow(row);
            if (path == null) return false;
            return 0 == tree.getModel().getChildCount(path.getLastPathComponent());
        }
    }

    public JTree getTree() {

        return tree;
    } // TODO remove for Production

    public TreePath getPathForRow(int row) {

        return tree.getPathForRow(row);
    }

    public int getRowForPath(TreePath path) {

        return tree.getRowForPath(path);
    }

    public void expandPath(TreePath path) {
        tree.expandPath(path);
    }

    public boolean isExpanded(TreePath path) {

        return tree.isExpanded(path);
    }

    public boolean isLeaf(TreePath path) {

        return tree.getModel().isLeaf(path.getLastPathComponent());
    }

    public void expandRow(int row) {

        expandPath(getPathForRow(row));
    }

    public void setShowsRootHandles(boolean newValue) {

        tree.setShowsRootHandles(newValue);
    }

    public void setRootVisible(boolean rootVisible) {

        tree.setRootVisible(rootVisible);
    }

    public boolean isRootVisible() {

        return tree.isRootVisible();
    }

    public boolean getShowsRootHandles() {

        return tree.getShowsRootHandles();
    }
}

