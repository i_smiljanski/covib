package com.utd.gui.controls;

/**
 * Class to limit the size of a document
 *
 * @author PeterBuettner.de <br>
 *
 * Source:
 * {@link http://java.sun.com/docs/books/tutorial/uiswing/components/example-1dot4/DocumentSizeFilter.java}
 * but changed a bit, removed debugging
 */

import javax.swing.*;
import javax.swing.text.*;


public class DocumentSizeFilter extends DocumentFilter {

    private int maxCharacters;

    public DocumentSizeFilter(int maxChars) {

        maxCharacters = maxChars;
    }

    public void insertString(FilterBypass fb, int offs, String str, AttributeSet a)
            throws BadLocationException {

        if ((fb.getDocument().getLength() + str.length()) <= maxCharacters)
            super.insertString(fb, offs, str, a);
        else
            UIManager.getLookAndFeel().provideErrorFeedback(null);
    }

    public void replace(FilterBypass fb, int offs, int length, String str, AttributeSet a)
            throws BadLocationException {

        if ((fb.getDocument().getLength() + str.length() - length) <= maxCharacters)
            super.replace(fb, offs, length, str, a);
        else
            UIManager.getLookAndFeel().provideErrorFeedback(null);

    }

    /**
     * adds a filter to restrict the size of the document of the textcomponent,
     * works only if the textcomponent has a AbstractDocument
     *
     * @param textComponent
     * @param max
     */
    public static void setFilter(JTextComponent textComponent, int max) {

        Document document = textComponent.getDocument();
        if (document instanceof AbstractDocument)
            ((AbstractDocument) document)
                    .setDocumentFilter(new DocumentSizeFilter(
                            max));
    }

}