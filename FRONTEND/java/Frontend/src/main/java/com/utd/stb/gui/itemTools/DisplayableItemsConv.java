package com.utd.stb.gui.itemTools;

import com.utd.stb.back.data.client.SelectionList;
import com.utd.stb.back.data.client.SelectionRecord;
import com.utd.stb.inter.items.DisplayableItems;

import java.util.ArrayList;
import java.util.List;


/**
 * Helper to convert DisplayableItems to misc containers - removed all but one
 * conversion since the others never used (and they were really simple, easy to
 * introduce later if needed)
 *
 * @author PeterBuettner.de
 */

public class DisplayableItemsConv {

    /**
     * A list of <code>DisplayableItem</code>s, copies the List but only the
     * references to the items.
     *
     * @return
     */
    public static List asList(DisplayableItems data) {

        int size = data.getSize();
        List list = new ArrayList(size);
        for (int i = 0; i < size; i++)
            list.add(data.get(i));
        return list;
    }

    /**
     * A list of <code>DisplayableItem</code>s, copies the List but only the
     * references to the items. MCD, 26.Jan 2006
     *
     * @return
     */
    public static DisplayableItems asDisplayableItems(List list) {

        int size = list.size();
        SelectionList items = new SelectionList();
        for (Object aList : list) {
            if (aList instanceof SelectionRecord) {
                SelectionRecord rec = (SelectionRecord) aList;
                items.add(rec);
            }
        }
        if (items instanceof DisplayableItems) {
            return items;
        }
        return null;
    }
}