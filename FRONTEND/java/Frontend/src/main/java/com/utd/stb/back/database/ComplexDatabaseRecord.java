package com.utd.stb.back.database;

import com.uptodata.isr.utils.ConvertUtils;
import com.utd.stb.back.data.client.ArrayMap;
import com.utd.stb.back.data.client.DatabaseFieldMap;
import com.utd.stb.back.data.client.ErrorRecord;
import com.utd.stb.back.data.client.StructMap;
import oracle.jdbc.OracleCallableStatement;
import oracle.jdbc.OracleResultSet;
import oracle.jdbc.OracleStruct;
import oracle.jdbc.OracleTypeMetaData;
import oracle.jdbc.driver.OracleConnection;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.io.IOException;
import java.sql.*;
import java.util.ArrayList;
import java.util.Iterator;


/**
 * Class that maps oracle.sql.struct and com.utd.stb.data.client.StructMap.
 * Given structs are mapped into the StructMap and other way roundusing the
 * Metainformation. structs can contain any kind of field(though act. blob and
 * clob not implemented, not used from Backend). This can be user defined lists
 * or structs too.
 *
 * @author rainer bruns Created 14.12.2004
 */
public class ComplexDatabaseRecord {
    public static final Logger log = LogManager.getRootLogger();

    StructMap theMap;
    Connection connection;
    String objectName;

    /**
     * @param s    the struct to map
     * @param conn the DB connection object
     * @throws SQLException
     */
    public ComplexDatabaseRecord(Struct s, Connection conn)
            throws SQLException {

        connection = conn;
        theMap = setStructMap(s);
    }


    /**
     * @param conn the database connection object
     * @param in   the StructMap to transfer to struct
     * @throws SQLException
     */
    ComplexDatabaseRecord(Connection conn, StructMap in) {

        theMap = in;
        connection = conn;
        objectName = in.getDescriptor();

    }


    /**
     * For each fieldentry in struct creates a DatabaseFieldMap with information
     * on the field and the value. For ARRAY resolves the ARRAY, for STRUCT
     * recursive call. Stores DatabaseFieldMap in StructMap with
     * ColumnName(=FieldName) as key
     *
     * @param struct the struct to map
     * @return a StructMap Object for struct
     * @throws SQLException
     */
    private StructMap setStructMap(Struct str) throws SQLException {

        Object[] data;

        OracleTypeMetaData.Struct meta = ((OracleTypeMetaData.Struct) ((OracleStruct) str).getOracleMetaData());
        ResultSetMetaData rsm = meta.getMetaData();
        StructMap map = new StructMap(meta.getName());

        data = str.getAttributes();
        for (int i = 1; i <= rsm.getColumnCount(); i++) {
//            log.debug("data = " + data[i - 1] + " " + rsm.getColumnClassName(i)
//                            + " column=" + rsm.getColumnName(i) + " type=" + rsm.getColumnType(i)
//                            + " display size=" + rsm.getColumnDisplaySize(i) + " type name=" + rsm.getColumnTypeName(i)
//            );

            if (rsm.getColumnClassName(i).equals("oracle.jdbc.OracleArray")) { // respect Specials
                Object value = null;
                try {
                    value = resolveArray(data[i - 1]);
                } catch (Exception e) { // check grants for types!!!
                    log.error(e);
                    log.info("data = " + data[i - 1] + " " + rsm.getColumnClassName(i)
                            + " column=" + rsm.getColumnName(i) + " type=" + rsm.getColumnType(i)
                            + " display size=" + rsm.getColumnDisplaySize(i) + " type name=" + rsm.getColumnTypeName(i)
                    );

                }
                DatabaseFieldMap fieldMap = new DatabaseFieldMap(rsm.getColumnName(i),
                        "oracle.sql.ARRAY",
                        rsm.getColumnType(i),
                        rsm.getColumnDisplaySize(i),
                        rsm.getColumnTypeName(i),
                        value);
                map.put(rsm.getColumnName(i), fieldMap);

            } else if (rsm.getColumnClassName(i).equals("oracle.sql.STRUCT")) {

                DatabaseFieldMap fieldMap = new DatabaseFieldMap(rsm.getColumnName(i),
                        "oracle.sql.STRUCT",
                        rsm.getColumnType(i),
                        rsm.getColumnDisplaySize(i),
                        rsm.getColumnTypeName(i),
                        setStructMap((Struct) data[i - 1]));
                map.put(rsm.getColumnName(i), fieldMap);

            } else { //if here error occurs, check the synonym in isrtester-user! (bitter experience - is)
                DatabaseFieldMap fieldMap = new DatabaseFieldMap(rsm.getColumnName(i),
                        rsm.getColumnClassName(i),
                        rsm.getColumnType(i),
                        rsm.getColumnDisplaySize(i),
                        rsm.getColumnTypeName(i),
                        data[i - 1]);
                map.put(rsm.getColumnName(i), fieldMap);
            }
        }
        return map;
    }

    /**
     * Resolve the content of an java.sql.Array
     *
     * @param o array to resolve
     * @return an ArrayList
     * @throws SQLException
     */
    private Object resolveArray(Object o) throws SQLException {

        Array array = (Array) o;
        ArrayList<Object> propertyList = new ArrayList<>();
        Object[] elements = (Object[]) array.getArray();
        for (Object element : elements) {
            if (element instanceof Struct) {
                Struct properties = (Struct) element;
                StructMap rl = setStructMap(properties);
                propertyList.add(rl);
            } else if (element instanceof Array) {
                propertyList.add(resolveArray(element));
            } else {
                propertyList.add(element);
            }
        }
        ArrayMap arrayMap = new ArrayMap(propertyList, array.getBaseTypeName());
        return arrayMap;
    }

    /**
     * from a StructMap Objects creates the oracle.sql.Struct
     *
     * @return a database struct
     * @throws SQLException
     */
    public Struct toDatum() throws SQLException {
        Struct str = connection.createStruct(objectName, null);
        OracleTypeMetaData.Struct meta = ((OracleTypeMetaData.Struct) ((OracleStruct) str).getOracleMetaData());
        ResultSetMetaData rsm = meta.getMetaData();

        Object[] data = new Object[rsm.getColumnCount()];
        for (int i = 1; i <= rsm.getColumnCount(); i++) {
//            log.debug(rsm.getColumnClassName(i) + " : " + rsm.getColumnName(i));
            switch (rsm.getColumnClassName(i)) {
                case "oracle.jdbc.OracleArray":  // respect
                    // Specials

                    data[i - 1] = listToARRAY(theMap.getValue(rsm.getColumnName(i)),
                            rsm.getColumnTypeName(i));

                    break;
                case "oracle.sql.STRUCT":

                    StructMap s = (StructMap) theMap.getValue(rsm.getColumnName(i));
                    ComplexDatabaseRecord rec = new ComplexDatabaseRecord(connection, s);
                    data[i - 1] = rec.toDatum();

                    break;
                case "oracle.jdbc.OracleBlob":
                    //log.debug(i + " : " + rsm.getColumnName(i));
                    Object value = theMap.getValue(rsm.getColumnName(i));
                    //log.debug(value+" "+(value instanceof byte[]));
                    if (value instanceof Blob) {
                        data[i - 1] = value;
                    } else if (value instanceof byte[])
                        try {
                            data[i - 1] = ConvertUtils.getBlob(connection, (byte[]) value);
                        } catch (IOException e) {
                            log.error(e);
                        }
                    break;
                default:
                    data[i - 1] = theMap.getValue(rsm.getColumnName(i));
                    break;
            }

        }

        return connection.createStruct(objectName, data);

    }

    /**
     * Make a java.sql.Array from an Array of Objects
     *
     * @param list      an ArrayMap, containg name of DB Array and an ArrayList of
     *                  Array data
     * @param arrayName the name of the array, to get the descriptor
     * @return the oracle.sql.ARRAY
     * @throws SQLException
     */
    private Array listToARRAY(Object list, String arrayName) throws SQLException {

        Object[] o;
        ArrayList propList;
        try {

            ArrayMap map = (ArrayMap) list;
            propList = map.getArray();
            o = new Object[propList.size()];

        } catch (NullPointerException ne) {
            return null;
        } catch (Exception e) {
            log.error(arrayName + ": " + e.getMessage());
            return null;
        }

        Array saveList;
        Iterator it = propList.iterator();

        for (int i = 0; it.hasNext(); i++) {

            Object el = it.next();
            if (el instanceof StructMap) {

                StructMap p = (StructMap) el;
                ComplexDatabaseRecord rec = new ComplexDatabaseRecord(connection, p);
                Struct struct = rec.toDatum();
                o[i] = struct;

            } else if (el instanceof ArrayMap) {

                ArrayMap subMap = (ArrayMap) el;
                Array array = listToARRAY(subMap, subMap.getDescriptor());
                o[i] = array;

            } else {
                o[i] = el;
            }

        }
        saveList = ((OracleConnection) connection).createARRAY(arrayName, o);
        return saveList;

    }

    /**
     * @return the created StructMap, wrapped into appropriate record by
     * DBInterface
     */
    public StructMap getStruct() {
        return theMap;
    }

    public static void main(String[] args) throws Exception {
        DriverManager.registerDriver(new oracle.jdbc.driver.OracleDriver());
        Connection conn = DriverManager.getConnection("jdbc:oracle:thin:@isrs-d-d12:1521:ora12201", "isradmin_isrs", "isradmin_isrs");
        OracleCallableStatement cs = null;
        OracleResultSet rs = null;
        try {
            cs = (OracleCallableStatement) conn.prepareCall(" begin ? := stb$gal.updateSessionInfo; end; ");
            cs.registerOutParameter(1, Types.STRUCT, "ISRADMIN_ISRS" + ".STB$OERROR$RECORD");
            rs = (OracleResultSet) cs.executeQuery();
            Struct struct = cs.getSTRUCT(1);
            ComplexDatabaseRecord rec = new ComplexDatabaseRecord(struct, conn);
            ErrorRecord err = new ErrorRecord(rec.getStruct());
            err.errCheck();
            rs.close();
            cs.close();
        } catch (SQLException se) {
            se.printStackTrace();
        }
    }

}