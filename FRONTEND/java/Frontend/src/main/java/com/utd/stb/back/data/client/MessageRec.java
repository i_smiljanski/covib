package com.utd.stb.back.data.client;

import org.apache.commons.lang.StringUtils;

/**
 * Wraps database struct data. Has appropriate get/set methods for data from db
 * struct ISR$MESSAGESTACK$RECORD
 *
 * @author Inna Smiljanski Created 07.03.2014
 */
public class MessageRec {

    StructMap messageStackData;
    String userStack;
    String implStack;
    String devStack;

    /**
     * Creates an empty EntityRecord
     */
    public MessageRec() {

    }

    public MessageRec(String userStack, String implStack, String devStack) {
        this.userStack = userStack;
        this.implStack = implStack;
        this.devStack = devStack;
    }

    /**
     * @param s the StructMap with the data for this Record
     */
    public MessageRec(StructMap s) {

        messageStackData = s;
    }

    /*
     * (non-Javadoc)
     *
     * @see java.lang.Object#toString() returns the value of field sUserStack
     */
    public String toString() {
        return messageStackData != null ? messageStackData.getValueAsString("SUSERMSG") : userStack;
    }


    public String getUserStack() {
        return messageStackData != null ? messageStackData.getValueAsString("SUSERMSG") : userStack;
    }

    public String getImplStack() {
        return messageStackData != null ? messageStackData.getValueAsString("SIMPLMSG") : implStack;
    }

    public String getDevStack() {
        return messageStackData != null ? messageStackData.getValueAsString("SDEVMSG") : devStack;
    }

    public boolean isNotEmpty() {
        return StringUtils.isNotEmpty(getUserStack()) || StringUtils.isNotEmpty(getImplStack()) || StringUtils.isNotEmpty(getDevStack());
    }


    /**
     * @return the underlying data
     */
    protected StructMap getStruct() {

        return messageStackData;
    }
}

