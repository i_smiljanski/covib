package com.utd.stb.back.ui.impl.userinputs.data;

import com.utd.stb.back.data.client.PropertyList;
import com.utd.stb.back.data.client.SelectionList;
import com.utd.stb.back.database.Dispatcher;
import com.utd.stb.back.ui.inter.WizardFormData;
import com.utd.stb.inter.application.BackendException;
import com.utd.stb.inter.userinput.UserInput;
import com.utd.stb.inter.wizzard.formtypes.FormDataUserInput;


/**
 * Unlike the other forms in ReportWizard the Output options(act. the only one)
 * uses PropertyRecords instead of SelectionRecords as base data. This class
 * represents the mixture of Dialogs - based on PropertyRecords - and FormData
 * Implementation
 *
 * @author rainer Created 14.12.2004
 */
public class ReportOptions extends AbstractInputData implements FormDataUserInput,
        WizardFormData {

    UserInput[] userInputs;
    Dispatcher dispatcher;
    PropertyList propsToSave = null;

    /**
     * creates the list of user inputs from PropertyList
     *
     * @param d          the Dispatcher
     * @param properties the list of fields(options) in the dialog
     */
    public ReportOptions(Dispatcher d, PropertyList properties) throws BackendException {

        super(properties, d);
        this.dispatcher = d;
        userInputs = createUserInputs();
    }

    /*
     * (non-Javadoc)
     * 
     * @see com.utd.stb.inter.wizzard.formtypes.FormDataUserInput#getUserInput(int)
     */
    public UserInput getUserInput(int index) {

        return userInputs[index];
    }

    /*
     * (non-Javadoc)
     * 
     * @see com.utd.stb.inter.wizzard.formtypes.FormDataUserInput#getUserInputCount()
     */
    public int getUserInputCount() {

        return userInputs.length;
    }

    /*
     * (non-Javadoc)
     * 
     * @see com.utd.stb.inter.wizzard.FormData#saveData()
     */
    public void saveData() throws BackendException {

        putPropertyList(getListToSave());
    }

    /**
     * Save the list of output options from dialog
     *
     * @param propsToSave
     * @throws BackendException
     */
    private void putPropertyList(PropertyList propsToSave) throws BackendException {

        dispatcher.putPropertyList(propsToSave, "OUTPUTOPTION");
    }

    /**
     * @return the list of fields as UserInput[]
     */
    private UserInput[] createUserInputs() throws BackendException {

        createData();
        return (UserInput[]) data.toArray(new UserInput[0]);
    }

    /* (non-Javadoc)
     * @see com.utd.stb.back.ui.inter.WizardFormData#getDataToSave()
     */
    public SelectionList getDataToSave() {

        return null;
    }

    /* (non-Javadoc)
     * @see com.utd.stb.back.ui.inter.WizardFormData#getEntity()
     */
    public String getEntity() {
        return null;
    }
}