package com.utd.gui.controls.table;

import com.jidesoft.grid.CellRendererManager;
import com.utd.alpha.extern.sun.treetable.TreeTableModel;

import javax.swing.*;
import java.awt.*;
import java.awt.event.KeyEvent;


/**
 * JTreeTable extension behaving more like a windows ListView than JTable, eats
 * &lt;Ctrl-A&gt; (default, change with processCtrlA), and &lt;Enter&gt; (-&gt;
 * processEnter) so keyboard-shortcuts works again,
 * <p>
 * is no focuscycleroot, so doesn't handle Tab/Shift-Tab,
 * <p>
 * sets a special renderer, so pay attention on reuse.
 * ?? remove the hack with processEnter, just remove it from the inputtable: it
 * seems this doesn't work, also if removed from all Maps
 * (WHEN_IN_FOCUSED_WINDOW,... )
 *
 * @author PeterBuettner.de
 */

public class ListViewTreeTable extends BaseTreeTable {

    private boolean processCtrlA;
    private boolean processEnter;

    public ListViewTreeTable(TreeTableModel dm) {
        super(dm);
        init(dm);
    }

    // --------------------------------------------------------------------------------


    private void init(TreeTableModel dm) {

        //... these 3 are from ListViewSizedTable
        setShowGrid(false);// if grid it is invisible if selected
        setRowMargin(0);// or we get lines between rows in background color if
        // selected
        getColumnModel().setColumnMargin(0);// ugly without too

        setDefaultRenderer(Object.class, new ListViewTableCellRenderer());
        CellRendererManager.registerRenderer(Object.class, new ListViewTableCellRenderer());

        // remove 'Tab' + 'Shift Tab' to stop internal navigation,
        // So Table works as List, with processKeyBinding() Enter is gone too
        setFocusTraversalKeys(KeyboardFocusManager.FORWARD_TRAVERSAL_KEYS, null);
        setFocusTraversalKeys(KeyboardFocusManager.BACKWARD_TRAVERSAL_KEYS, null);
        // maybe (isManagingFocus() return false may work: no JTable sets them

        setUpKeyBindings();
    }

    private void setUpKeyBindings() {

        setUpKeyBinding("HOME", "selectFirstRow");
        setUpKeyBinding("END", "selectLastRow");
        setUpKeyBinding("shift HOME", "selectFirstRowExtendSelection");
        setUpKeyBinding("shift END", "selectLastRowExtendSelection");
    }

    private void setUpKeyBinding(String keyStroke, String actionName) {

        getInputMap().put(KeyStroke.getKeyStroke(keyStroke), actionName);
    }

    protected boolean isProcessCtrlA() {

        return processCtrlA;
    }

    protected void setProcessCtrlA(boolean processCtrlA) {

        this.processCtrlA = processCtrlA;
    }

    protected boolean isProcessEnter() {

        return processEnter;
    }

    protected void setProcessEnter(boolean processEnter) {

        this.processEnter = processEnter;
    }

    //	remove 'ctrl a' and 'Enter' to use them in the whole form
    protected boolean processKeyBinding(KeyStroke ks, KeyEvent e, int condition,
                                        boolean pressed) {

        if (e.getKeyCode() == 'A' && e.isControlDown() && !processCtrlA) return false;
        if (e.getKeyCode() == KeyEvent.VK_ENTER && !processEnter) return false;
        return super.processKeyBinding(ks, e, condition, pressed);
    }

}