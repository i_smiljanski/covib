package com.utd.stb.back.ui.impl.wizard;

import com.utd.stb.back.data.client.FormInfoRecord;
import com.utd.stb.back.database.Dispatcher;
import com.utd.stb.inter.application.BackendException;
import com.utd.stb.inter.wizzard.WizardStep;
import com.utd.stb.inter.wizzard.WizardSteps;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.util.ArrayList;


/**
 * Holds the list of Wizardsteps. Initialised wher Report Wizard is started
 *
 * @author rainer Created 14.12.2004
 */
public class StandardWizardSteps implements WizardSteps {
    public static final Logger log = LogManager.getRootLogger();

    Dispatcher dispatcher;
    int numberOfSteps;
    ArrayList steps;

    /**
     * Queries the number of forms for reporttype and initialises one wizardstep
     * for each form
     *
     * @param d the dispatcher
     * @throws BackendException
     */
    public StandardWizardSteps(Dispatcher d) throws BackendException {

        this.dispatcher = d;
        steps = new ArrayList();

        // MCD, 30.Jan 2007
        // fetch formInfo only once for each mask
        FormInfoRecord formInfo = dispatcher.getCurrentFormInfo(1);
        log.debug(formInfo);
        StandardWizardStep step = new StandardWizardStep(1,
                dispatcher.getTranslation(formInfo.getEntity()),
                "",
                dispatcher.getTranslation(formInfo.getFormToolTip()));
        steps.add(step);

        this.numberOfSteps = formInfo.getTotalForms();

        // MCD, 20.Jul 2006
        // set help id with the entity when the help for the entities is ready ?
        for (int i = 2; i <= numberOfSteps; i++) {
            formInfo = dispatcher.getCurrentFormInfo(i);
            step = new StandardWizardStep(i,
                    dispatcher.getTranslation(formInfo.getEntity()),
                    "",
                    dispatcher.getTranslation(formInfo.getFormToolTip()));
            steps.add(step);
        }
    }

    /*
     * (non-Javadoc)
     * 
     * @see com.utd.stb.inter.wizzard.WizardSteps#getSize()
     */
    public int getSize() {

        return numberOfSteps;

    }

    /*
     * (non-Javadoc)
     * 
     * @see com.utd.stb.inter.wizzard.WizardSteps#get(int)
     */
    public WizardStep get(int index) throws IndexOutOfBoundsException {

        return (WizardStep) steps.get(index);

    }

    /*
     * (non-Javadoc)
     * 
     * @see com.utd.stb.inter.wizzard.WizardSteps#indexOf(com.utd.stb.inter.wizzard.WizardStep)
     */
    public int indexOf(WizardStep wizardStep) {

        return steps.indexOf(wizardStep);

    }

}