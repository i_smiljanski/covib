package com.utd.stb.back.ui.impl.msgboxes;

import com.utd.stb.inter.action.ButtonItem;
import com.utd.stb.interlib.BasicMessageBox;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;


/**
 * Implementation of BasicMessageBox that shows a (happy) message. An action
 * might be executed on close
 *
 * @author rainer bruns Created 06.12.2004
 */
public class SuccessMsgBox extends BasicMessageBox {
    public static final Logger log = LogManager.getRootLogger();

    Object action;
    int timeout;

    /**
     * Creates a message box with OK button
     *
     * @param title  Title of MessageBox
     * @param text   Text of MessageBox
     * @param action Action to be executed on close.
     */
    public SuccessMsgBox(String title, String text, Object action) {

        this(title, text, action, 0);

    }

    /**
     * Creates a message box with OK button, closes the message box after
     * timeout MCD 12.07.2005
     *
     * @param title  Title of MessageBox
     * @param text   Text of MessageBox
     * @param action Action to be executed on close.
     */
    public SuccessMsgBox(String title, String text, Object action, int timeout) {

        super(null, createHeaderBar(title, ICON_INFORMATION), null, text,
                new ButtonItem[]{bOk}, null, true);
        log.debug("SuccessMsgBox:title = " + title + " text = " + text + " " + action + " " + timeout + " ");
        this.action = action;
        this.timeout = timeout;

    }

    /*
     * (non-Javadoc)
     * 
     * @see com.utd.stb.interlib.BasicMessageBox#onBoxClose(com.utd.stb.inter.action.ButtonItem)
     *      Execute the action.
     */
    protected Object onBoxClose(ButtonItem clickedButton) {

        return action;
    }

    public int getTimeout() {

        return timeout;
    }
}