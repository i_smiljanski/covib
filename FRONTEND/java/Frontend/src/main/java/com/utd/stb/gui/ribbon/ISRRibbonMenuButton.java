/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package com.utd.stb.gui.ribbon;

import org.pushingpixels.flamingo.api.common.JCommandMenuButton;
import org.pushingpixels.flamingo.api.common.icon.ResizableIcon;

import java.awt.event.ActionListener;

/**
 * @author dortm19
 */
public class ISRRibbonMenuButton extends JCommandMenuButton {

    int id;
    String token = "";
    String handler = "";
    ActionListener actionListener = null;

    public ISRRibbonMenuButton(String title, ResizableIcon icon) {
        super(title, icon);
    }

    public int getId() {
        return id;
    }

    public String getToken() {
        return token;
    }

    public String getHandler() {
        return handler;
    }

    public void setId(int id) {
        this.id = id;
    }

    public void setToken(String token) {
        this.token = token;
    }

    public void setHandler(String handler) {
        this.handler = handler;
    }

    public ActionListener getActionListener() {
        return actionListener;
    }

    public void setActionListener(ActionListener actionListener) {
        this.actionListener = actionListener;
    }
}
