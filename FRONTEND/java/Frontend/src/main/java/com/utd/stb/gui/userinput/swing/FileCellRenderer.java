package com.utd.stb.gui.userinput.swing;

import com.jidesoft.dialog.ButtonPanel;
import com.jidesoft.dialog.StandardDialog;
import com.jidesoft.grid.ContextSensitiveCellEditor;
import com.jidesoft.grid.TableModelWrapperUtils;
import com.jidesoft.plaf.UIDefaultsLookup;
import com.jidesoft.swing.JideButton;
import com.uptodata.isr.gui.lookAndFeel.TweakUI;
import com.uptodata.isr.utils.ConvertUtils;
import com.utd.stb.back.ui.impl.userinputs.data.ContextDefaultTableModel;
import org.apache.commons.codec.binary.Base64;
import org.apache.commons.lang.StringUtils;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import resources.IconSource;
import resources.Res;

import javax.swing.*;
import javax.swing.table.TableCellEditor;
import javax.swing.table.TableCellRenderer;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.io.File;
import java.io.IOException;

/**
 * Created by smiljanskii60 on 07.12.2016.
 */
class FileCellRenderer extends ContextSensitiveCellEditor implements TableCellRenderer, TableCellEditor {

    public static final Logger log = LogManager.getRootLogger();

    final static String RES_GRID_TABLE = "gridTable.editFile.";
    String title = Res.getString(RES_GRID_TABLE + "title");
    Component owner;
    Object _value;
    ContextDefaultTableModel dataTableModel;
    StandardDialog dialog;

    public FileCellRenderer(Component owner, ContextDefaultTableModel dataTableModel) {
        this.owner = owner;
        this.dataTableModel = dataTableModel;
    }

    @Override
    public Component getTableCellEditorComponent(JTable table, Object value, boolean isSelected, int row, int column) {
        log.info("begin " + value.getClass());
        int actualRow = TableModelWrapperUtils.getActualRowAt(table.getModel(), row);
        log.debug(row + " " + actualRow);
        createChoiceEditDialog(value, actualRow, column);
        log.info("end");
        return null;
    }

    @Override
    public Object getCellEditorValue() {
        return null;
    }

    @Override
    public Component getTableCellRendererComponent(JTable table, Object value, boolean isSelected, boolean hasFocus, int row, int column) {
        JPanel panel = new JPanel(new GridLayout(1, 2));
        panel.setOpaque(true);
        JLabel label = new JLabel(title);
        panel.add(label);
        return panel;
    }


    private void createChoiceEditDialog(Object value, int row, int column) {
        _value = value;
        dialog = new StandardDialog((Frame) null, title) {

            @Override
            public JComponent createBannerPanel() {
                return null;
            }

            @Override
            public JComponent createContentPanel() {
                JPanel panel = new JPanel(new GridLayout(2, 1, 2, 2));
                panel.setPreferredSize(new Dimension(200, 60));
                Icon icon;
                String iconText;
                String buttonText;
                String dialogTitle;
                AbstractAction buttonListener;
                IconSource iconSource = new IconSource();
                if (_value != null && StringUtils.isNotEmpty(_value.toString())) {
                    icon = iconSource.getMediumIconByID("download");
                    iconText = Res.getString(RES_GRID_TABLE + "chooseFile.save.icon");
                    buttonText = Res.getString(RES_GRID_TABLE + "chooseFile.save.button");
                    dialogTitle = Res.getString(RES_GRID_TABLE + "chooseFile.save.title");
                    buttonListener = new FileChooserActionListener(iconText, icon, buttonText, dialogTitle,
                            false, row, column);
                    panel.add(createButton(buttonListener));
                }

                icon = iconSource.getMediumIconByID("upload");
                iconText = Res.getString(RES_GRID_TABLE + "chooseFile.load.icon");
                buttonText = Res.getString(RES_GRID_TABLE + "chooseFile.load.button");
                dialogTitle = Res.getString(RES_GRID_TABLE + "chooseFile.load.title");
                buttonListener = new FileChooserActionListener(iconText, icon, buttonText, dialogTitle, true, row, column);
                panel.add(createButton(buttonListener));

                return panel;
            }

            @Override
            public ButtonPanel createButtonPanel() {
                ButtonPanel buttonPanel = new ButtonPanel();
                JButton cancelButton = new JButton();
                cancelButton.setName(CLOSE);
                buttonPanel.addButton(cancelButton, ButtonPanel.CANCEL_BUTTON);

                cancelButton.setAction(new AbstractAction(UIDefaultsLookup.getString("OptionPane.cancelButtonText")) {
                    public void actionPerformed(ActionEvent e) {
                        setDialogResult(RESULT_CANCELLED);
                        setVisible(false);
                        dispose();
                    }
                });

                setDefaultCancelAction(cancelButton.getAction());
                buttonPanel.setBorder(BorderFactory.createEmptyBorder(10, 10, 10, 10));
                buttonPanel.setSizeConstraint(ButtonPanel.NO_LESS_THAN);
                return buttonPanel;
            }
        };

        dialog.pack();
        dialog.setLocationRelativeTo(owner);
        dialog.setVisible(true);
    }

    private JButton createButton(AbstractAction listener) {
        JideButton button = new JideButton(listener);
//        button.setRolloverIcon(rolloverIcon);
        button.setButtonStyle(JideButton.TOOLBAR_STYLE);
        button.setHorizontalAlignment(SwingConstants.LEADING);
        button.setFocusable(false);
        return button;
    }


    class FileChooserActionListener extends AbstractAction {
        String buttonText;
        String dialogTitle;
        boolean isLoad;
        int row;
        int column;


        public FileChooserActionListener(String iconText, Icon icon, String buttonText, String dialogTitle,
                                         boolean isLoad, int row, int column) {
            super(iconText, icon);
            this.buttonText = buttonText;
            this.dialogTitle = dialogTitle;
            this.isLoad = isLoad;
            this.row = row;
            this.column = column;
        }


        @Override
        public void actionPerformed(ActionEvent e) {

            try {
                UIManager.setLookAndFeel(UIManager.getSystemLookAndFeelClassName());
            } catch (ClassNotFoundException | InstantiationException | IllegalAccessException | UnsupportedLookAndFeelException ex) {
                log.error(ex);
            }

            JFileChooser chooser = new JFileChooser();
            TweakUI.setLookAndFeel();
//            chooser.setFileSelectionMode(JFileChooser.FILES_AND_DIRECTORIES);
            chooser.setCurrentDirectory(null);

            int dialogType = isLoad ? JFileChooser.OPEN_DIALOG : JFileChooser.SAVE_DIALOG;
            chooser.setDialogType(dialogType);
            chooser.setApproveButtonText(buttonText);
            int result = chooser.showDialog(null, dialogTitle);

            if (result == JFileChooser.APPROVE_OPTION) {
                File selectedFile = chooser.getSelectedFile();
                String selectedFilePath = selectedFile.getAbsolutePath();
                if (isLoad) {
                    log.debug(selectedFilePath);
                    try {
                        uploadFile(selectedFilePath);
                        dataTableModel.setValueAt(_value, row, column);
                    } catch (IOException ioException) {
                        log.error(ioException);
                    }
                } else {
                    int result1 = Integer.MIN_VALUE;
                    if (selectedFile.exists()) {
                        result1 = JOptionPane.showConfirmDialog(chooser, "The file exists, overwrite?",
                                "Existing file", JOptionPane.YES_NO_CANCEL_OPTION);
                    }
                    if (result1 == Integer.MIN_VALUE || result1 == JOptionPane.YES_OPTION) {
                        try {
                            downloadFile(selectedFilePath);
                        } catch (IOException ioException) {
                            log.error(ioException);
                        }
                    }
                }
            }
        }
    }

    void uploadFile(String selectedFilePath) throws IOException {
        byte[] bytes = ConvertUtils.fileToBytes(selectedFilePath);
        _value = Base64.encodeBase64String(bytes);
        dialog.setVisible(false);
    }

    void downloadFile(String selectedFilePath) throws IOException {
        byte[] base64Bytes = Base64.decodeBase64(((String) _value));
        File storedFile = ConvertUtils.bytesToFile(base64Bytes, selectedFilePath);
        log.debug(storedFile);
        dialog.setVisible(false);
    }

}
