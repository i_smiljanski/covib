package com.utd.stb.back.ui.impl.userinputs.fields;

import com.utd.stb.back.data.client.PropertyRecord;
import com.utd.stb.gui.userinput.swing.FormatFactory;
import com.utd.stb.inter.application.BackendException;
import com.utd.stb.inter.userinput.DateInput;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.text.ParseException;
import java.time.ZonedDateTime;
import java.util.Calendar;


/**
 * Implementation of UserInput for date fields
 *
 * @author rainer bruns Created 02.12.2004 Implementation of UserInput for date
 *         fields
 */
public class DateInputImpl implements DateInput {
    public static final Logger log = LogManager.getRootLogger();

    private ZonedDateTime date;
    private String type;
    private PropertyRecord data;
    private String format;

    /**
     * Creates DateField. Tries to cast value of field to date.
     *
     * @param data the data record
     * @throws BackendException
     * @see PropertyRecord#getValue()
     */
    public DateInputImpl(PropertyRecord data) throws BackendException {

        this.data = data;
        //Date Type: DATE, DATETIME or TIME
        this.type = getDateType();
        this.format = getFormat();
        //try to parse string to date
        try {
            this.date = (ZonedDateTime) data.getValue();
        } catch (Exception pe) {
            throw new BackendException(pe.getMessage(), pe,
                    BackendException.SEVERITY_ACTION_EXIT);
        }
    }

    /**
     * information of type is in field type of PropertyRecord. Type can be DATE,
     * DATETIME or TIME
     *
     * @return a string indicating type of date
     */
    private String getDateType() {
//        log.debug("getDateType: " + data.getType());
        switch (data.getType()) {
            case "DATE":
                return TYPE_DATE;
            case "TIME":
                return TYPE_TIME;
            case "DATETIME":
                return TYPE_DATETIME;
            default:
                throw new IllegalArgumentException("Type:'" + type + "' not yet implemented.");
        }
    }

    @Override
    public String getFormat() {
        return data.getFormat();
    }

    /*
     * (non-Javadoc)
     * 
     * @see com.utd.stb.inter.userinput.DateInput#setDate(java.util.Date) sets
     *      the value field of underlying data record to new date. Underlying
     *      value field is always String
     */
    public void setDate(ZonedDateTime date) {
//        this.date = strip( date );
        this.date = date;
        data.setDateValue(date);
    }

    /*
     * (non-Javadoc)
     * 
     * @see com.utd.stb.inter.userinput.DateInput#getDate()
     */
    public ZonedDateTime getDate() {
//        return strip( date );
        return date;
    }

    /*
     * (non-Javadoc)
     * 
     * @see com.utd.stb.inter.userinput.DateInput#getType()
     */
    public String getType() {

        return type;
    }


    /**
     * Is used when saving field to db
     *
     * @return the record to be stored to db
     */
    public PropertyRecord getProperty() {
        data.setDateValue(getDate());
        return data;
    }

    static private Calendar cal = FormatFactory.getCalendar();

    //static private Calendar calDateOnly = FormatFactory.getCalendarGMT();
    /**
     * @param all
     *            complete date data
     * @return striped date data, depending on type
     */
    /*private ZonedDateTime strip( ZonedDateTime all ) {

        if ( all == null ) return null;

        if ( type.equals( TYPE_DATETIME ) ) return all;

        if ( type.equals( TYPE_DATE ) ) {// strip time
            *//*Calendar dateCal = cal;//DateOnly;
            dateCal.setTime( all );
            dateCal.clear( Calendar.MILLISECOND );
            dateCal.clear( Calendar.SECOND );
            dateCal.clear( Calendar.MINUTE );
            dateCal.clear( Calendar.HOUR_OF_DAY );*//*
            return all.getTime();
        }
        if ( type.equals( TYPE_TIME ) ) {// strip date
            cal.setTime( all );
            cal.clear( Calendar.YEAR );
            cal.clear( Calendar.MONTH );
            cal.clear( Calendar.DAY_OF_MONTH );
            return cal.getTime();
        }

        throw new IllegalStateException( "Type:'" + type + "' not yet implemented." );
    }*/

}