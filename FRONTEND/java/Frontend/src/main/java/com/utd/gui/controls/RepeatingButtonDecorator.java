package com.utd.gui.controls;

import javax.swing.*;
import javax.swing.event.ChangeEvent;
import javax.swing.event.ChangeListener;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.lang.ref.WeakReference;


/**
 * Decorator to change a standart button to a repeating button (like
 * ScrollbarButtons)
 *
 * @author PeterBuettner.de
 *         <p>
 *         TODOlater removeButton(), look for hard references, use weakhashmap, only one
 *         listener
 */
public class RepeatingButtonDecorator {

    private WeakReference buttonRef;
    private Timer timer;
    private int delayMillies;
    private int initialDelayMillies;

    /**
     * Change button to a repeating on - binds to the buttonModel, so if model
     * changes it stops working
     * <p>
     * Default times are 'usefull values', currently: frequency of 7Hz and an
     * initial delay of 500ms, but they may change
     *
     * @param button
     */

    public static void addButton(AbstractButton button) {

        addButton(button, 140, 500);
    }

    /**
     * Change button to a repeating on - binds to the buttonModel
     *
     * @param button
     * @param delayMillies
     * @param initialDelayMillies
     */
    public static void addButton(AbstractButton button, int delayMillies,
                                 int initialDelayMillies) {

        new RepeatingButtonDecorator(button, delayMillies, initialDelayMillies);
    }

    private RepeatingButtonDecorator(AbstractButton button, int delayMillies,
                                     int initialDelayMillies) {

        buttonRef = new WeakReference(button);
        this.delayMillies = delayMillies;
        this.initialDelayMillies = initialDelayMillies;

        button.getModel().addChangeListener(new ButtonChangeListener());
    }

    /**
     * @param delayMillies
     * @param initialDelayMillies
     */
    private Timer createTimer() {

        Timer t = new Timer(delayMillies, new TimerListener());
        t.setInitialDelay(initialDelayMillies);
        return t;
    }

    private AbstractButton getButton() {

        return (AbstractButton) buttonRef.get();
    }

    private ButtonModel getButtonModel() {

        AbstractButton b = getButton();
        return b == null ? null : b.getModel();
    }

    private final class ButtonChangeListener implements ChangeListener {

        // start timer when armed+pressed+rollover and not timer.running
        // stop when !pressed and timer.running
        public void stateChanged(ChangeEvent e) {

            ButtonModel bm = (ButtonModel) e.getSource();

            //	+ (bm.isRollover() ? "R" : " ") + " " + e);

            if (timer != null && !bm.isPressed()) {
                timer.stop();
                timer = null;
            } else if (timer == null && bm.isPressed() && bm.isArmed()) {
                timer = createTimer();
                timer.start();
            }

        }

    }

    private final class TimerListener implements ActionListener {

        public void actionPerformed(ActionEvent e) {

            ButtonModel bm = getButtonModel();
            if (bm == null || !bm.isPressed() || !bm.isArmed() || !bm.isEnabled()) return;

            Action a = getButton().getAction();

            ActionEvent ev = new ActionEvent(getButton(), ActionEvent.ACTION_PERFORMED,
                    (String) a.getValue(Action.ACTION_COMMAND_KEY),
                    e.getWhen(), e.getModifiers());

            a.actionPerformed(ev);
        }
    }
}