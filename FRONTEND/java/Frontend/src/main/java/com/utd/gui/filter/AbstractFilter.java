package com.utd.gui.filter;

import javax.swing.event.ChangeEvent;
import javax.swing.event.ChangeListener;
import javax.swing.event.EventListenerList;


/**
 * @author PeterBuettner.de
 */
public abstract class AbstractFilter implements Filter {

    protected EventListenerList evtListeners = new EventListenerList();

    public abstract boolean matches(Object candidate);

    public void addChangeListener(ChangeListener l) {

        evtListeners.add(ChangeListener.class, l);

    }

    public void removeChangeListener(ChangeListener l) {

        evtListeners.remove(ChangeListener.class, l);

    }

    /**
     * Forwards the given notification event to all <code>ChangeListener</code>
     * that registered themselves as listeners for this Filter.
     *
     * @param e the event to be forwarded
     */
    protected void fireFilterChanged(ChangeEvent e) {

        Object[] li = evtListeners.getListenerList();

        for (int i = li.length - 2; i >= 0; i -= 2) {

            if (li[i] == ChangeListener.class) {

                ((ChangeListener) li[i + 1]).stateChanged(e);

            }

        }

    }

    /**
     * Notifies all <code>ChangeListener</code> s that registered themselves
     * as listeners for this Filter.
     */
    protected void fireFilterChanged() {

        fireFilterChanged(new ChangeEvent(this));

    }

}