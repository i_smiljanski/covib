package com.utd.stb.back.ui.impl.userinputs.fields;

import com.utd.stb.back.data.client.PropertyRecord;
import com.utd.stb.inter.userinput.FileInput;

/**
 * Implementation of UserInput for file fields
 *
 * @author rainer bruns Created 02.12.2004
 */
public class DirectoryUserInput extends AbstractUserInput {

    private FileInput fileInput;
    private PropertyRecord property;

    /**
     * @param label    field Label
     * @param value    field Value
     * @param property complete record of field information from database
     */
    public DirectoryUserInput(String label, FileInput data, PropertyRecord property) {

        super(label);
        this.fileInput = data;
        this.property = property;
    }

    /*
     * (non-Javadoc)
     * 
     * @see com.utd.stb.inter.userinput.UserInput#getData()
     */
    public Object getData() {
        return fileInput;
    }

    /*
     * (non-Javadoc)
     * 
     * @see com.utd.stb.inter.userinput.UserInput#getType()
     */
    public Class getType() {
        return FileInput.class;
    }

    /*
     * (non-Javadoc) Sets new text in TextInput and in value field of original
     * data record
     * 
     * @see com.utd.stb.inter.userinput.UserInput#setData(java.lang.Object)
     */
    public void setData(Object data) {
        //fileInput.setTranslation( data.toString() );
        property.setValue(data.toString());
    }

    public boolean hasLov() {
        return fileInput.hasLov();
    }

    /**
     * Called when saving data to db
     *
     * @return the data record
     * @see AbstractInputData#getListToSave()
     */
    public PropertyRecord getProperty() {

        return property;
    }
}