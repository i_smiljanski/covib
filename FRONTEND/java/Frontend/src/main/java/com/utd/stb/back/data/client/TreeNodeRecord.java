package com.utd.stb.back.data.client;

import com.uptodata.isr.utils.ConvertUtils;
import com.utd.stb.back.database.DBInterface;
import com.utd.stb.back.ui.impl.tree.TreeBase;
import com.utd.stb.inter.application.Node;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.sql.Clob;

/**
 * Wraps database struct data. Has appropriate get/set methods for data from db
 * struct STB$TREENODE$RECORD
 *
 * @author rainer bruns Created 27.08.2004
 */
public class TreeNodeRecord implements Node {

    public static final Logger log = LogManager.getRootLogger();

    StructMap treeNodeData;
    boolean showText = true;

    PropertyList props;

    /**
     * creates an empty TreeNodeRecord
     */
    public TreeNodeRecord() {
    }

    /**
     * creates a TreeNodeRecord with the input data
     *
     * @param data
     */
    public TreeNodeRecord(StructMap data) {
        treeNodeData = data;
    }

    /**
     * sets the data for this TreeNodeRecord
     *
     * @param in
     */
    public void setStruct(StructMap in) {

        treeNodeData = in;
    }

    /*
     * (non-Javadoc)
     *
     * @see java.lang.Object#toString() this is the value of field SDISPLAY
     */
    public String toString() {
        if (treeNodeData == null) {
            return null;
        }
        return treeNodeData.getValueAsString("SDISPLAY");
    }

    /**
     * @return the value of field SDISPLAY
     */
    public String getDisplay() {
        if (treeNodeData == null) {
            return null;
        }
        return treeNodeData.getValueAsString("SDISPLAY");
    }

    /**
     * @return the value of field NID
     */
    public String getNodeId() {
        if (treeNodeData == null) {
            return null;
        }
        return treeNodeData.getValueAsString("SNODEID");
    }

    /**
     * @return the value of field SNODETYPE
     */
    public String getNodeType() {
        if (treeNodeData == null) {
            return null;
        }
        return treeNodeData.getValueAsString("SNODETYPE");
    }

    /**
     * @return the value of field NDISPLAYTYPE
     */
    public Integer getDisplayType() {
        if (treeNodeData == null) {
            return null;
        }
        return treeNodeData.getValueAsInt("NDISPLAYTYPE");
    }

    /**
     * This is used after a doubleclick on a node to determine what action has
     * to be done
     *
     * @return the value of field SDEFAULTACTION
     * @see TreeBase#invokeAction(Object)
     */
    public String getDefaultAction() {
        if (treeNodeData == null) {
            return null;
        }
        return treeNodeData.getValueAsString("SDEFAULTACTION");
    }

    /**
     * used when writing to db
     *
     * @return the underlying data
     * @see DBInterface#getNodeChilds(TreeNodeRecord, boolean)
     * @see DBInterface#setCurrentNode(TreeNodeRecord)
     */
    public StructMap getNodeData() {

        return treeNodeData;
    }

    /**
     * db object STB$TREENODE$RECORD contains a field of type array and name
     * TLOPROPERTYLIST This is a list of PropertyRecords, so return a
     * PropertyList
     *
     * @return the list of node properties
     * @see PropertyList
     */
    public PropertyList getProperties() {

        if (props != null) {
            return props;
        }
        if (treeNodeData == null) {
            return null;
        }
        ArrayMap arrayMap = (ArrayMap) treeNodeData.getValue("TLOPROPERTYLIST");
        return new PropertyList(arrayMap.getArray());
    }

    /**
     * @return the value of field CLPROPERTYLIST
     */
    public byte[] getXmlFile() {
        //log.debug("getXmlFile begin");
        if (treeNodeData == null) {
            return null;
        }
        byte[] xml = null;
        Clob xmlCl = ((Clob) treeNodeData.getValue("CLPROPERTYLIST"));
        try {
            xml = ConvertUtils.getBytes(xmlCl);
        } catch (Exception e) {
            e.printStackTrace();
            log.error(e);
        }
        // log.debug("getXmlFile end: "+xmlStream);
        return xml;
    }

    public void setProperties(PropertyList properties) {

        try {
            treeNodeData.setValue("TLOPROPERTYLIST", properties);
        } catch (Exception e) {
            this.props = properties;
        }
    }

    /**
     * db object STB$TREENODE$RECORD contains a field of type array and name
     * TLOPROPERTYLIST This is a list of PropertyRecords, so return a
     * PropertyList
     *
     * @return the list of node properties
     * @see PropertyList
     */
    public ValueList getValueList() {

        if (treeNodeData == null) {
            return null;
        }
        ArrayMap arrayMap = (ArrayMap) treeNodeData.getValue("TLOVALUELIST");
        return new ValueList(arrayMap.getArray());

    }

    /*
     * (non-Javadoc)
     *
     * @see java.lang.Object#equals(java.lang.Object) an isr tree node is
     *      identified by id and type, so equals is overwritten.
     */
    public boolean equals(Object o) {

        TreeNodeRecord theOther = null;
        try {

            theOther = (TreeNodeRecord) o;
        } catch (ClassCastException ce) {

            return false;
        }

        try {

            if (getNodeId().equals(theOther.getNodeId())
                    && getNodeType().equals(theOther.getNodeType())) {
                return true;
            }
        } catch (NullPointerException ne) {
            ne.printStackTrace();
        }

        return false;

    }

    /*
     * (non-Javadoc)
     *
     * @see java.lang.Object#hashCode() make hashCode from id and type by simply
     *      returning the hashCode of the concatinated String
     */
    public int hashCode() {
        String myIdentity = getNodeId() + getNodeType();
        int i = myIdentity.hashCode();
        return i;

    }

    /*
     * (non-Javadoc)
     *
     * @see com.utd.stb.inter.application.Node#getTranslation()
     */
    public String getText() {

        return toString();
    }

    public void setShowText(boolean show) {
        showText = show;
    }

    public boolean isShowText() {
        return showText;
    }

    /*
     * (non-Javadoc)
     *
     * @see com.utd.stb.inter.application.Node#getIconId()
     */
    public String getIconId() {
        if (treeNodeData == null) {
            return null;
        }
        return treeNodeData.getValueAsString("SICON");
    }

    public void setIconId(String icon) {
        treeNodeData.setValue("SICON", icon);
    }

    public String getTooltip() {
        if (treeNodeData == null) {
            return null;
        }
        return treeNodeData.getValueAsString("STOOLTIP");
    }

    public StructMap getStruct() {

        return treeNodeData;
    }

    public String toStringForLog() {
        return "TreeNodeRecord{"
                + "getDisplay=" + getDisplay()
                + ", getNodeType=" + getNodeType()
                + ", getNodeData=" + getNodeData()
                + ", getDisplayType=" + getDisplayType()
                + ", getIconId=" + getIconId()
                //+ ", getNodeId=" + getNodeId()
                + ", properties=" + getProperties().toString()
                + '}';
    }
}
