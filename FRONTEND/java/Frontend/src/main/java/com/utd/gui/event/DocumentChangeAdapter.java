package com.utd.gui.event;

import javax.swing.event.DocumentEvent;
import javax.swing.event.DocumentListener;


/**
 * A DocumentListener that informs something about a document change
 *
 * @author PeterBuettner.de
 */
public abstract class DocumentChangeAdapter implements DocumentListener {

    public void changedUpdate(DocumentEvent e) {

        documentChange(e);
    }

    public void insertUpdate(DocumentEvent e) {

        documentChange(e);
    }

    public void removeUpdate(DocumentEvent e) {

        documentChange(e);
    }

    /**
     * Informs about a document change
     *
     * @param e The original unchanged DocumentEvent
     */
    public abstract void documentChange(DocumentEvent e);

}