package com.utd.stb.back.data.client;


/**
 * Holds information on fields in an oracle.sql.STRUCT object. Information is:
 * field name, field class name(like oracle.sql.ARRAY), field class (the integer
 * provided), field size (2 for varchar(2), 0 if not available), fieldTypeName
 * and the value as Object
 *
 * @author rainer bruns Created 15.12.2004
 */
public class DatabaseFieldMap {

    int fieldSize;
    Object fieldValue;

    /**
     * @param fieldName      field name
     * @param fieldClassName field class name
     * @param fieldClass     field class identifier
     * @param fieldSize      precision
     * @param fieldTypeName  field type
     * @param value          field value
     */
    public DatabaseFieldMap(String fieldName, String fieldClassName, int fieldClass,
                            int fieldSize, String fieldTypeName, Object value) {
        this.fieldSize = fieldSize;
        this.fieldValue = value;
    }


    /**
     * @return the field size
     */
    int getFieldSize() {

        return fieldSize;
    }


    /**
     * @return the value of field
     */
    public Object getValue() {
        return fieldValue;
    }



    /**
     * sets value of field to value
     *
     * @param value new value
     */
    public void setValue(Object value) {

        this.fieldValue = value;
    }



}