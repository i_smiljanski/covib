package com.utd.gui.controls.table.model;

import javax.swing.table.TableModel;


/**
 * A TableModel where the data for a row is 'contained' in a Object per row
 *
 * @author PeterBuettner.de
 */
public interface RowTableModel extends TableModel {

    /**
     * should return the 'Row Object' if columnIndex==-1, in other respects see
     * {@link TableModel}
     */
    public Object getValueAt(int rowIndex, int columnIndex);

    /**
     * get the Object that contains the data for the row
     *
     * @param row
     * @return
     */
    public Object getRow(int row);

}