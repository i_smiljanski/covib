package com.utd.stb.back.ui.impl.userinputs.fields;

import com.utd.stb.back.data.client.PropertyRecord;
import com.utd.stb.back.data.client.UserInputList;
import com.utd.stb.back.data.client.UserInputRecord;
import com.utd.stb.inter.application.BackendException;
import com.utd.stb.inter.items.DisplayableItem;
import com.utd.stb.inter.items.DisplayableItems;
import com.utd.stb.inter.userinput.DisplayableItemInput;


/**
 * Field data implementation for fields having a list of values
 *
 * @author rainer bruns Created 02.12.2004
 */
public class ListItemInput implements DisplayableItemInput {

    private PropertyRecord data;
    private ListItemProvider ui;

    /**
     * Creates the ListItemInput object with data record from db
     *
     * @param rec The complete record of field data
     * @param ui  Class managing all the list stuff
     */
    public ListItemInput(PropertyRecord rec, ListItemProvider ui) {

        data = rec;
        this.ui = ui;
    }

    /*
     * (non-Javadoc) returns the currently selected data. UserInputObject is a
     * wrapper around StructMap for this particular kind of data.
     * 
     * @see com.utd.stb.inter.userinput.DisplayableItemInput#getSelectedItem()
     * @see com.utd.stb.data.client.UserInputObject
     */
    public DisplayableItem getSelectedItem() {
        return new UserInputRecord(data.getValueAsString(),
                data.getDisplayAsString(),
                "");
    }

    /*
     * (non-Javadoc) sets the new value in underlying data record. GUI names
     * value for some reason info.
     * 
     * @see com.utd.stb.inter.userinput.DisplayableItemInput#setSelectedItem(com.utd.stb.inter.items.DisplayableItem)
     */
    public void setSelectedItem(DisplayableItem item) {

        try {
            // neu MCD 14.Mar 2006
            if (item instanceof UserInputRecord) {
                UserInputRecord input = (UserInputRecord) item;
                data.setValue(input.getValue());
                data.setDisplayValue(input.getData());
            }

        } catch (NullPointerException ne) {
            data.setValue(null);
        }
    }

    /*
     * (non-Javadoc)
     * 
     * @see com.utd.stb.inter.userinput.DisplayableItemInput#getAvailableItems()
     *      Get the list of values from ListItemProvider
     */
    public DisplayableItems getAvailableItems() throws BackendException {

        UserInputList retList = null;
        retList = ui.getAvailableItems(data.getParameterName());
        return retList;
    }

    /**
     * used when saving data to db
     *
     * @return the underlying data record
     * @see AbstractInputData#getListToSave()
     */
    public PropertyRecord getProperty() {

        return data;
    }
}