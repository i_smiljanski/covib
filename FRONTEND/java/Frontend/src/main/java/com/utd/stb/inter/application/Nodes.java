package com.utd.stb.inter.application;

/**
 * Collection of nodes
 *
 * @author PeterBuettner.de
 */
public interface Nodes {

    /**
     * nodecount
     *
     * @return
     */
    int getSize();

    /**
     * get a Node
     *
     * @param i index
     * @return Node
     */
    Node getNode(int i);

    /**
     * index in the Collection or -1 if not found
     *
     * @param node
     * @return
     */
    int indexOf(Node node);

    void deleteRow(int i);

    void addRow(int i);

    String toString();
}