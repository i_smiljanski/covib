package com.utd.stb.inter.wizzard;

import com.utd.stb.inter.application.BackendException;


/**
 * Generic dynamic ReportWizard, steps may change while navigating through them.
 *
 * @author PeterBuettner.de
 */
public interface ReportWizard {

    /**
     * The gui calls this if user want to abort the wizard - the aequivalent to
     * WindowAction(~WANT_CLOSE)
     */
    public final static Object NAVIGATE_ABORT = "NavigateAbort";

    /**
     * The gui calls this if user want to go straight to the end, or after the
     * last step is done.
     */
    public final static Object NAVIGATE_FINISH = "NavigateFinish";

    public final static Object NAVIGATE_EXIT = "NavigateExit";

    String getTitle();

    /**
     * here we don't fire the full variety of actions (less than in UserDialog),
     * NAVIGATE_ABORT, NAVIGATE_FINISH, WindowAction(~WINDOW_CLOSED)
     *
     * @param action
     * @return what to do next
     */
    Object doAction(Object action) throws BackendException;

    /**
     * may change after a call to {@link navigateToWizardStep()}, so reread it.
     *
     * @return
     */
    WizardSteps getWizardSteps();

    /**
     * Ask if you want to know the current one.
     *
     * @return
     */
    WizardStep getCurrentWizardStep();

    /**
     * Go to the passed step, since the wizard may fork depending on the data,
     * reread the WizardSteps afterwards, its content may have changed, note
     * that the destination step may not be reached, so note the stepNr before
     * and afterwards, compare them and inform the user.
     * <p>
     * We don't use doAction() for thie since we would need to add a paramater:
     * lazy developer:-) maybe if we need dialogs after navigation this will
     * return an action
     *
     * @param wizardStep
     */
    void navigateToWizardStep(WizardStep wizardStep) throws BackendException;

}