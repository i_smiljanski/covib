package com.utd.gui.animate;

import java.util.Iterator;
import java.util.NoSuchElementException;


/**
 * Generic method to animate some moving thing, s= 1/2 a * t^2,
 *
 * @author PeterBuettner.de
 *         <p>
 *         with s=1/2a*t^2; next(), hasNext() -> iterator mit Float() oder StepCount
 *         vorgeben (Zeit angeben in der es erledigt sein soll) oder eien Zeitrahmen:
 *         bitte etwa 1s, weniger als 1s oder mindestens 1s evtl auch floatIterator()
 *         und integerIterator() mindestStepSize (hier z.B. 1 oder 2 (pixel))
 */

public class Mover {

    // strategy is in mover, data presented by iterator

    // use float (maybe double?) since int has ugly rounding questions
    private float start;
    private float end;

    private float step;

    private float a = 1;        //acceleration=1; // a ~ accel.
    private float b = -2;       //slowdown = -3; // b ~ break

    private int numSteps;
    private int numStepsBreak;
    private boolean up;
    private boolean down;

    /*
     * 
     * assume (end > start), other direction same idea
     * 

     * 
     * Solve[ 1 == a/2 t^2 + a*t(1-t) + b/2 (1-t)^2, t ] 2 2 a - 2 b - Sqrt[-4
     * (2 - b) (a - b) + (-2 a + 2 b) ] {{t ->
     * ----------------------------------------------------}, 2 (a - b)
     * 
     * 2 2 a - 2 b + Sqrt[-4 (2 - b) (a - b) + (-2 a + 2 b) ] {t ->
     * ----------------------------------------------------}} 2 (a - b)
     * 
     * 
     * Solve[ 1 == a/2 t^2 + b/2 (1-t)^2 , t ] 2 2 2 b - Sqrt[4 b - 4 (-2 + b)
     * (a + b)] 2 b + Sqrt[4 b - 4 (-2 + b) (a + b)] {{t ->
     * -------------------------------------}, {t ->
     * -------------------------------------}} 2 (a + b) 2 (a + b)
     * 
     * 
     * 
     * Solve[ 1== Integrate[ a * t,{t,0,t1} ] + Integrate[ a*t1 +b * t,{t,t1,1} ],
     * t1 ] 2 2 2 a - Sqrt[4 a - 4 (2 - b) (a + b)] 2 a + Sqrt[4 a - 4 (2 - b)
     * (a + b)] {{t1 -> ------------------------------------}, {t1 ->
     * ------------------------------------}} 2 (a + b) 2 (a + b)
     *  
     */
    public Mover(float start, float end, int numSteps) {

        this.start = start;
        this.end = end;
        this.numSteps = numSteps;
        up = start < end;
        down = !up;

        //		numStepsBreak = (int)Math.round(numSteps*(-4*a + 4*b +
        // Math.sqrt((4*a-4*b)*(4*a-4*b) -8*b*(-4+a+2*b)))/(4*b));
        numStepsBreak = (int) Math
                .round(numSteps
                        * (2 * b + Math.sqrt((4 * b * b) - 4 * (-2 + b) * (a + b))) / 2
                        / (a + b));
        //		numStepsBreak = (int)Math.round(numSteps*(2*a - Math.sqrt( 4*a*a
        // -4*(2-b)*(a+b) ))/2/(a+b));

    }

    public Mover(float start, float end) {

        this.start = start;
        this.end = end;
        step = (end - start) / 10; // if step == 0 then return end in first
        // iteration
        up = start < end;
        down = !up;
    }

    /**
     * Always produces at least one step, maybe with the end-value, e.g. if
     * stepsize is to small.
     *
     * @return
     */
    public Iterator iteratorFloat() {

        return iterator(false);
    }

    public Iterator iteratorInteger() {

        return iterator(true);
    }

    public Iterator iteratorLinearInteger() {

        return new LinearMoverIterator(true);
    }

    public Iterator iteratorLinearFloat() {

        return new LinearMoverIterator(false);
    }

    protected Iterator iterator(boolean roundToInteger) {

        if (numSteps == 0) return new StandardMoverIterator(roundToInteger);
        return new STMoverIterator(roundToInteger);
    }

    private class STMoverIterator extends AbstractMoverIterator {

        private int n = 0;
        private float t1 = 1f * numStepsBreak / numSteps; // time of (start of
        // slowdown)
        private float s_t1 = 0.5f * a * t1 * t1;            // s(start of
        // slowdown)

        STMoverIterator(boolean roundToInteger) {

            super(roundToInteger);
        }

        public Object next() {

            if (!hasNext()) throw new NoSuchElementException();
            n++;
            float t = 1f * n / numSteps;
            float s;

            if (t <= t1)
                s = 0.5f * a * t * t;
            else
                s = a * t1 * (t - t1) + 0.5f * b * (t - t1) * (t - t1) + s_t1;

            float newCurrent = (end - start) * s;
            if (Math.abs(current - newCurrent) < 2)// enshure we reach end
                current += (up ? 2 : -2);
            else
                current = newCurrent;

            done = ((down && current < end) || (up && current > end)) || n > 30;
            if (done) current = end;// enshure not out of bounds
            return getValue(current);
        }
    }// -----------------------------------------------------------------------

    private class StandardMoverIterator extends AbstractMoverIterator {

        StandardMoverIterator(boolean roundToInteger) {

            super(roundToInteger);
        }

        public Object next() {

            if (!hasNext()) throw new NoSuchElementException();

            if (step == 0) {// hack
                done = true;
                return getValue(current);
            }
            current += step;
            step *= 0.9;
            if (Math.abs(step) < 2) step = 2f * (step < 0 ? -1 : 1);

            // the +1 offset is 'cause of rounding error defense
            done = ((down && current < end + 1) || (up && current > end - 1));
            if (done) current = end;// enshure not out of bounds

            return getValue(current);
        }
    }// -----------------------------------------------------------------------

    private class LinearMoverIterator extends AbstractMoverIterator {

        LinearMoverIterator(boolean roundToInteger) {

            super(roundToInteger);
        }

        public Object next() {

            if (!hasNext()) throw new NoSuchElementException();

            if (step == 0) {// hack
                done = true;
                return getValue(current);
            }
            current += step;
            if (Math.abs(step) < 2) step = (step < 0 ? -1 : 1);

            // the +1 offset is 'cause of rounding error defense
            done = ((down && current < end + 1) || (up && current > end - 1));
            if (done) current = end;// enshure not out of bounds

            return getValue(current);
        }
    }// -----------------------------------------------------------------------

    private abstract class AbstractMoverIterator implements Iterator {

        protected boolean roundToInteger;
        protected boolean done = false;
        protected float current;

        private AbstractMoverIterator(boolean roundToInteger) {

            this.roundToInteger = roundToInteger;
            current = start;
        }

        // generates a Number: Integer (rounded) or Float
        protected Number getValue(float value) {

            if (roundToInteger)
                return new Integer(Math.round(value));
            else
                return new Float(value);
        }

        public boolean hasNext() {

            return !done;
        }

        public void remove() {

            throw new UnsupportedOperationException("Just generating data, no collection.");
        }
    }// end of Iterator

}

//if (n<= numStepsBreak) current = 0.5f* a *n*n + start;
//else {
//	if(startBreak==0)
//		startBreak = 1/2* a *numStepsBreak*numStepsBreak + a*numStepsBreak + start;
//	int k= numStepsBreak - n;
//	float add = 0.5f* b *k*k + a*numStepsBreak*k;
//	if (Math.abs(add)<2) add = up? 2f : -2f;// enshure we reach end
//	current = add + startBreak;
//	}
