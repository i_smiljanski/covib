package com.utd.stb.inter.application;

import com.uptodata.isr.loader.webservice.LoaderWebserviceImpl;
import com.utd.stb.back.ui.impl.lib.ActionList;
import com.utd.stb.inter.action.MenuItem;
import com.utd.stb.inter.action.MenuItems;


/**
 * This is the main entry point for tha main window of the abstract application.
 *
 * @author PeterBuettner.de
 */
public interface Base {

    /**
     * standard application exit, see {@link #shutDown(int)}
     */
     static final int SHUTDOWN_NORMAL = 0;

    /**
     * the user was inactive for the defined time, see {@link #shutDown(int)}
     */
     static final int SHUTDOWN_TIMEOUT = 1;

    /**
     * application exit 'cause of an exception, see {@link #shutDown(int)}
     */
     static final int SHUTDOWN_EXCEPTION = 2;

    /**
     * the childs for the tree. Always requests them new from the Server, so the
     * gui has to cache them.
     *
     * @param node null will return the root nodes
     * @return
     */
    Nodes getNodeChildsTree(Node node) throws BackendException;

    /**
     * The childs for the detail view. Always requests them new from the Server,
     * so the gui has to cache them. Has to be completely filled, so no
     * exceptions may occure after the structure is returned.
     *
     * @param node
     * @return
     */
    DetailView getDetailViewAction(Node node) throws BackendException;

    DetailView getDetailViewActionInBackground(Node node) throws BackendException;

    /**
     * Menu structure for the node, the topmost MenuItem is not shown, it is
     * used for systems where popup menus have a title, give it a null as text
     * to not have any title. If the top-item has no children then nothing or
     * maybe a 'nothing available' menu is presented.
     *
     * @param node
     * @return
     */
    MenuItem getNodeMenu(Node node) throws BackendException;

    /**
     * Define menuitems in the main windows menubar. Every MenuItem may have
     * submenus. This is 'static'in terms of that it is called only once in the
     * applications lifetime, not after every nodechange: use the nodes context
     * menu for specific tasks. Maybe later we create a 'reload MenuBar' action.
     * <p>
     * The items got from here are placed between the other items in the
     * menubar: <br>
     * File Edit <b>&lt;MenuItem#1&gt; &lt;MenuItem#2&gt; ... &lt;MenuItem#n&gt;
     * </b> Help <br>
     *
     * @return null or an empty collection for none
     */
    MenuItems getMainMenuBar() throws BackendException;

    // -----------------------------------------------------------

    /**
     * The gui asks thee Base what to do, herein are filled MenuItems, and some
     * returnvalues use untyped for now, see later what is needed
     *
     * @param action
     * @return
     */
    Object invokeAction(Object action) throws BackendException;

    /**
     * if the base has a new Task ({@link getNextTask()}) for the gui to
     * fulfill
     *
     * @return
     */
    boolean hasTask();

    /**
     * The base may request the gui to reload/expand/select a treenode, show a
     * dialog,... will return null if there is no task, if you want to see
     * before if there is something to do use hasTask()
     *
     * @return
     */
    Object getNextTask();

    /**
     * The gui has to delete the actionlist if no reloading of the tree is desired
     */
    void setActionList(ActionList a);


    // -----------------------------------------------------------

    /**
     * Some static info about the application, language, titles, version... <br>
     * Must not throw any Exception (so preload all info on instance creation),
     * also the returned ApplicationInfo has to be without any Exception.
     *
     * @return
     */
    ApplicationInfo getApplicationInfo();

    /**
     * Provider for some persistance
     *
     * @return
     */
    GuiPersistance getGuiPersistance();

    /**
     * Has to be called to clearly shutdown the base. Values are:
     * <p>
     * <pre>
     *
     *  Base.SHUTDOWN_NORMAL
     *  Base.SHUTDOWN_TIMEOUT
     *  Base.SHUTDOWN_EXCEPTION
     *
     * </pre>
     * <p>
     * After calling this, no method in the base can be called
     *
     * @param reason
     * @throws BackendException is ignored from the gui if (reason != SHUTDOWN_NORMAL)
     */
    void shutDown(int reason) throws BackendException;


    /**
     * Has to be called to check if doc versions are displayed in a separate window
     */
    boolean showDocVersionSeparately();

    boolean splitTree();

    void commitTable(Object table) throws BackendException;

     JobsTableInfo getJobsTableInfo();
//     void setJobsTableInfo(JobsTableInfo progressTableInfo);

     Object getMenuForJobs();

     String getJobParameter(String parameterName, int jobId) throws BackendException;

     String getParameterValue(String parameter);

     void setNode(Node node, boolean left);

     void setNodeInBackground(Node node, boolean left);

     byte[] getXml(Node selectedNode, MenuItem menuEntry, byte[] action) throws BackendException;

     byte[] getXmlInBackground(Node selectedNode, MenuItem menuEntry, byte[] action) throws BackendException;

     MenuItem createRibbonMenuEntry(String token);

     MenuItem createRibbonMenuEntryInBackground(String token);

     LoaderWebserviceImpl getLoader();

     byte[] getNodeDetails() throws BackendException;

     byte[] getNodeDetailsInBackground() throws BackendException;

    void updateTreeInBackground();

    Node createTransportNode(String nodetype, String nodeid, String sessionid);


}