package com.utd.stb.gui.loader.agent;

import com.jidesoft.alert.Alert;
import com.jidesoft.alert.AlertGroup;
import com.jidesoft.animation.CustomAnimation;
import com.jidesoft.grid.TableModelWrapperUtils;
import com.jidesoft.status.ProgressStatusBarItem;
import com.jidesoft.swing.PaintPanel;
import com.jidesoft.utils.PortingUtils;
import com.uptodata.isr.utils.xml.dom.XmlHandler;
import com.utd.gui.controls.table.TableTools;
import com.utd.stb.StabberInfo;
import com.utd.stb.back.data.client.TreeNodeRecord;
import com.utd.stb.back.data.client.ValueList;
import com.utd.stb.back.ui.impl.msgboxes.WarningMsgBox;
import com.utd.stb.back.ui.impl.userinputs.fields.TextInputImpl;
import com.utd.stb.gui.loader.resources.Res;
import com.utd.stb.gui.swingApp.App;
import com.utd.stb.gui.swingApp.BarProgressMouseListener;
import com.utd.stb.gui.swingApp.NodesTableModel;
import com.utd.stb.gui.swingApp.StatusBarThread;
import com.utd.stb.inter.application.DetailView;
import com.utd.stb.inter.application.JobsTableInfo;
import com.utd.stb.inter.application.Nodes;
import com.utd.stb.inter.userinput.UserInputs;
import org.apache.commons.lang.StringUtils;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;

import javax.net.ServerSocketFactory;
import javax.net.ssl.SSLServerSocket;
import javax.net.ssl.SSLServerSocketFactory;
import javax.swing.*;
import javax.xml.xpath.XPathExpressionException;
import java.awt.*;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;
import java.io.*;
import java.net.ServerSocket;
import java.net.Socket;
import java.util.*;
import java.util.List;
import java.util.regex.Pattern;


/**
 * This class is responsible for client side tasks of job execution in ISR's
 * document handling. It loads file from database to client, starts applications
 * on the client and loads files back to database. The LoaderAgent creates a
 * server socket on a port and listens on requests. If a request received the
 * together with the Job Id(this is the database job id) in a Hashtable and
 * Thread Execution is started. If request is something else the Thread with the
 * requested Job Id is looked up in Hashtable, its environment is set to the
 * Streams of the socket and the thread is interrupted. Threads go in wait state
 * after one execution request is finished. Only on END and ERROR Thread will
 * not go to wait. Request are submitted from Java stored procedures in the
 * database. There is no persistance of database stored class over the total
 * execution of one Job (this would block rest of Job execution). However,
 * thread for job execution must be persistant througout the job. On startup
 * another thread is created and started, that listens for requests on another
 * port. It registers if ISR clients on the local machine start up or shut down.
 * If there is no more ISR client registered and no thread is in map anymore,
 * agent will shut down.
 *
 * @author rainer bruns Created 20.06.2004
 */
public class LoaderAgent {

    public static final Logger log = LogManager.getRootLogger();
//    public static final Logger log = LogManager.getRootLogger();

    Pattern pattern1 = Pattern.compile("\\[ISR.TEMP.PATH]");
    Pattern pattern2 = Pattern.compile("\\[ISR.DOWNLOAD.PATH]");
    Pattern pattern3 = Pattern.compile("\\[ISR.DOWNLOAD.FILE]");

    String progressTransl = StabberInfo.getTranslation("PROGRESS");
    String isrJobTransl = StabberInfo.getTranslation("ISRJOBID");

    ServerSocket serv;
    Hashtable<String, JobThread> map = new Hashtable<String, JobThread>();
    Controler controler;
    boolean markedForShutdown = false;
    boolean shutdown = false;
    //    private static FileHandler fh;
    Hashtable<String, String> pathStore = new Hashtable<String, String>();
    String baseDir;
    //where we put all the files
    List<String> directories = new ArrayList<String>();
    boolean ssl;

    String basisPort;
    String controlPort;
    String base;
    String socketTimeout;
    String logging;
    boolean debug = false;

    boolean canceled = false;
    boolean repeated = false;

    JobsTableInfo jobsTableInfo;
    StatusBarThread statusBarThread;

    String configPath;
//    Dispatcher dispatcher;
    // App app;

    //int jobCounter = 0;
    // MCD, 09.Aug 2006, next version with doc server
    // int						   identifier		 = 1;

    /**
     * Reads configuration from property file(i.e. port on which to listen),
     * starts logging, create server socket. Start listening loop.
     *
     * @param args:          basisPort,controlPort, "loader.baseDir",
     *                       "loader.socketTimeout", "loader.logging,"loader.debug"
     * @param jobsTableInfo: model, app, isjobsTable, isRunLoader
     */
    public LoaderAgent(String[] args, JobsTableInfo jobsTableInfo) {
        this.basisPort = args[0];
        this.controlPort = args[1];
        this.base = args[2] != null ? args[2] : "iStudyReporter\\loader";
        this.socketTimeout = args[3];
        this.logging = args[4];
        if (args[5] != null) {
            this.debug = args[5].equalsIgnoreCase("T");
        }
        this.jobsTableInfo = jobsTableInfo;
        init();
    }

    public LoaderAgent(String basisPort, String controlPort, String base, String socketTimeout, String debug, JobsTableInfo jobsTableInfo) {
        this.basisPort = basisPort;
        this.controlPort = controlPort;
        this.base = base;
        this.socketTimeout = socketTimeout;
        this.debug = "T".equalsIgnoreCase(debug);
        this.jobsTableInfo = jobsTableInfo;
        init();
    }

    private void init() {
        com.jidesoft.utils.Lm.verifyLicense("up to data professional services", "iStudyReporter", "w6.tuJhnfnDInGp:fvqIJaaeKT4yKF42");

        //set Look and feel
        try {
            UIManager.setLookAndFeel(UIManager.getSystemLookAndFeelClassName());
        } catch (Exception ignored) {
            log.error(ignored);
        }

        try {

            // if not existing create our base directory. If this fails, exit,
            // since we can't work
            if (!makeBaseDir()) {

                log.debug("Error creating basedir");
                System.exit(1);
            }

            //create server Socket with or without SSL
            serv = getSocket(basisPort);
            ssl = false;
            log.info("Basis started on port " + serv.getLocalPort());

            //start thread that listens for request of ISR clients
            controler = new Controler(Integer.parseInt(controlPort));
            controler.start();
            log.info("Controller started on port " + controlPort);

        } catch (Exception e) {

            if (e instanceof java.net.BindException) {

                log.debug("You should restart the loader, port in use");
                shutdownImmediate();

            } else {

                logException("Error initializing socket", e);
                System.exit(1);

            }

        }

        //now listen on socket
        waitForCalls();

    }

    private static ServerSocket getSocket(String port) throws Exception {

        log.info("Init normal socket");
        return new ServerSocket(Integer.parseInt(port));

    }

    private static ServerSocket getSSLSocket(String port, Properties props) throws Exception {

        log.info("SSL socket");
        java.lang.System.setProperty("javax.net.ssl.trustStore", props.getProperty("javax.net.ssl.trustStore"));
        java.lang.System.setProperty("javax.net.ssl.trustStorePassword", props.getProperty("javax.net.ssl"
                + ".trustStorePassword"));
        java.lang.System.setProperty("javax.net.ssl.keyStore", props.getProperty("javax.net.ssl.keyStore"));
        java.lang.System.setProperty("javax.net.ssl.keyStorePassword", props.getProperty("javax.net.ssl"
                + ".keyStorePassword"));
        ServerSocketFactory ssocketFactory = SSLServerSocketFactory.getDefault();
        SSLServerSocket ssocket = (SSLServerSocket) ssocketFactory.createServerSocket(Integer.parseInt(port));
        ssocket.setNeedClientAuth(true);
        return ssocket;

    }

    /**
     * Listen on socket until we are ready for shutdown. This is if no more
     * client is registered and all jobs are done. Additionally a shutdown might
     * be forced, but this is not officially implemented in ISR clients.
     */
    private void waitForCalls() {

        //loop until shutdown is true
        while (!shutdown) {

            Socket client;
            ObjectInputStream ois;
            ObjectOutputStream oos;

            try {
                client = serv.accept();
                log.info("Connection accept.");

                client.setSoTimeout(new Integer(socketTimeout) * 1000);

                oos = new ObjectOutputStream(client.getOutputStream());
                ois = new ObjectInputStream(client.getInputStream());
                client.setSoTimeout(0);

                Hashtable<String, Object> data = (Hashtable<String, Object>) ois.readObject();
                String head = data.get("HEAD").toString();
                String info = data.get("INFO").toString();
                log.info("ObjectInputStream read: " + head + " " + info);

                if (head.equals("NO_JOBID")) {
                    log.info("Only checking for a free port, " + "this is not free, "
                            + "good bye (Port " + client.getLocalPort() + ")");
                } else {
                    log.debug("Hi, here is a database job for you!");
                    ConnectionThread conn = new ConnectionThread(client, data, ois, oos);
                    log.debug("Initialize thread. ");
                    conn.start();
                    log.debug("Try to start thread.");
                }

            } catch (IOException ioe) {
                log.error(ioe);
                //  log.debug("IOException: " + ioe.getLocalizedMessage());
            } catch (ClassNotFoundException e) {
                log.debug("ClassNotFoundException: " + e.getLocalizedMessage());
                logException("ClassNotFoundException", e);
            }

        }

    }

    /**
     * Called from Controler thread, when the last ISR client has gone. If there
     * is no more thread in map shutdown, else set a flag, that we can shutdown,
     * when last thread has done.
     */
    private synchronized void mayBeExit() {
        log.info("mayBeExit");
        if (map.isEmpty()) {
            shutdown();
        }
        markedForShutdown = true;
    }

    /**
     * reset marked for shutdown status
     */
    private synchronized void cancelShutdown() {

        log.info("Shutdown canceled");
        markedForShutdown = false;
    }

    /**
     * shutdown without any check
     */
    private synchronized void shutdownImmediate() {

        shutdown();
    }

    /**
     * Each job has a timeout. When it's reached we stop this thread, assuming
     * something has gone wrong in database
     *
     * @param data     Hashtable containing execution data for thread
     * @param threadId id of thread
     */
    private synchronized void stopThread(Hashtable<String, Object> data, String threadId) {

        JobThread s = map.get(threadId);
        map.remove(threadId);
        log.info("Tread removed");
        s.setEnv(null, null, data);
        s.interrupt();
    }

    /**
     * Do what has to be done before exiting... Store all properties, close
     * loghandle and socket, remove temporary file and directories
     */
    private void shutdown() {

        log.info("Shutting down");

        try {
            //FileOutputStream propStream = new FileOutputStream( new File( propsLocation ) );
            //props.store( propStream, "Loader" );
//            fh.close();
            log.debug("isClosed: " + serv.isClosed());
            serv.close();
        } catch (IOException ie) {
            log.warn("Problem shutting down " + ie.getLocalizedMessage());
        }
        log.debug("server closed");
        cleanUp();
//        shutdown = true;
        System.exit(999);
    }

    /**
     * If the base directory does not exists try to create it. Base directory is
     * java.io.tmpdir\iStudyReporter\loader-> on windows(german) usually
     * C:\Dokumente und Einstellungen\$user\Lokale
     * Einstellungen\temp\iStudyreporter\loader
     *
     * @return true if directory exists or succesfully created
     */
    private boolean makeBaseDir() {

        log.info("Basis " + System.getProperty("java.io.tmpdir") + " " + base);
        File dir = new File(System.getProperty("java.io.tmpdir"), base);
        baseDir = dir.getAbsolutePath();
        //   directory = baseDir + System.getProperty("file.separator");
        //if there is a file with this name we can't do this
        if (dir.exists() && dir.isFile()) {
            return false;
        } //directory exists, ok
        else if (dir.exists() && dir.isDirectory()) {
            return true;
        }
        //try to create
        return dir.mkdirs();

    }

    /**
     * Delete all files in basedir(on shutdown). If an directory ends with .err
     * an error occured. Additional information might be in there. Keep it!
     */
    private void cleanUp() {

        //File dir = new File(System.getProperty("java.io.tmpdir"), base);
//        File[] files = dir.listFiles();
//        log.debug(directory+" "+files.length);
//        for (File file : files) {
//            if (!file.getName().endsWith(".err") && !file.isFile() ||
//                    file.getName().endsWith(".lck") ||     // MCD, 07.Feb 2006
//                    file.getName().endsWith(".log"))    // MCD, 07.Feb 2006
//                deleteFile(file);
//        }
        log.debug(directories);
        for (String dirName : directories) {
            log.debug("directories:" + dirName);
            File dir = new File(dirName);
            deleteFile(dir);
        }
    }

    /**
     * If file is a file, try to delete it. If it is directory, get all
     * files(that means dirs too) and call us for each of them -> recursive
     * delete
     *
     * @param file file or directory to delete
     */
    private void deleteFile(File file) {

        if (file.isDirectory()) {

            File[] files = file.listFiles();
            for (File file1 : files) {
                deleteFile(file1);
            }

        }
        boolean isDelete = file.delete();
        log.debug(file + " " + isDelete);
    }

    /**
     * In JobThreads due to repetitive waits stack trace is long and boring. So
     * cut it down.
     *
     * @param msg a message to log additionally;
     * @param e   exception with info to log
     */
    private void logException(String msg, Exception e) {

        try {
            StackTraceElement[] se = e.getStackTrace();
            log.debug(msg + " " + e.toString() + " in " + se[0].getFileName() + " at " + se[0].getLineNumber());
            log.debug("Error in Method " + se[0].getMethodName() + " Error is " + e.getMessage());
            int i;
            for (i = 0; i < 10 && i < se.length; i++) {
                log.debug("->" + se[i].toString());
            }
            int diff = se.length - i;
            log.debug("->.." + diff + " more ");
        } catch (Exception ignored) {
            log.error(ignored);
        }

    }

    private class ConnectionThread extends Thread {

        Socket socket;
        Hashtable<String, Object> data;
        ObjectInputStream in;
        ObjectOutputStream out;

        public ConnectionThread(Socket s, Hashtable<String, Object> data, ObjectInputStream in,
                                ObjectOutputStream out) {

            this.socket = s;
            this.data = data;
            this.in = in;
            this.out = out;

        }

        public void run() {

            checkThreadExists();
        }

        /**
         * Check the kind of request. If START -> make a new JobThread, store
         * reference in map and start thread ENDE/ERROR -> remove threads
         * refernce from map. If there is no more work to do(map empty, no
         * client) let the thread clean up, and then shutdown. ELSE -> lookup
         * thread in map, set it's environment(these are the streams from
         * socket) and interrupt. If thread is not in map, it's a serious
         * problem, tell database.
         *
         * @return true
         */
        private boolean checkThreadExists() {

            try {
                if (data.get("HEAD").toString().equals("UpdateTree")) {
                    final App app = jobsTableInfo.getApp();
                    if (app != null) {
                        if (data.get("NODETYPE") != null && data.get("NODEID") != null && data.get("SIDE") != null) {
                            app.updateTree(data.get("NODETYPE").toString(), data.get("NODEID").toString(), data.get("SIDE").toString());
                        }
                    }
                } else if (data.get("HEAD").toString().equals("RefreshRibbon")) {
                    final App app = jobsTableInfo.getApp();
                    if (app != null) {
                        app.updateRibbonMenu();
                    }
                } else {
                    //Job Id is in field HEAD
                    String jobId = data.get("HEAD").toString();
                    //and request type in field INFO
                    String info = data.get("INFO").toString();
                    log.debug("info = " + info + " jobId = " + jobId);
                    JobThread jobThread = map.get(jobId);

                    //if there is a thread ref in map
                    if (jobThread != null) {
                        //on ENDE and ERROR remove reference
                        if (info.equals("ENDE")) {
                            map.remove(jobId);
                        }
                        if (info.equals("ERROR")) {
                            map.remove(jobId);
                        }
//                        log.info("Map has elements: " + map.size());
                        // if there is no more work, let the thread clean up, and
                        // shutdown

                        if (map.isEmpty() && jobsTableInfo != null) {
                            jobsTableInfo.setRunLoader(false);
                            statusBarThread.interrupt();
                        }

                        if (map.isEmpty() && markedForShutdown) {

                            log.info("Map is empty, marked for shutdown");
                            jobThread.setEnv(in, out, data);
                            jobThread.closeThread(info);
                            shutdown();
                        } else {
                            // else let the thread do its work
                            // give thread the streams and actual request data, then
                            // interrupt the wait
                            log.info("Interrupt the waiting ");
                            jobThread.setEnv(in, out, data);
                            jobThread.interrupt();
                        }

                    } else if (info.equals("Start") && jobId.equals("isDocServerAvailable")) {
                        try {
                            Hashtable<String, String> writeBack = new Hashtable<String, String>();
                            log.info("Doc server is available");
                            writeBack.put("INFO", "Doc server available");
                            writeBack.put("CODE", Integer.toString(0));
                            out.writeObject(writeBack);
                            out.flush();
                            in.close();
                            out.close();
                        } catch (Exception ie) {
                            log.debug("IOError while writing back for " + jobId);
                        }

                    } else if (info.equals("Start")) {
                        // create a new thread, store ref in map and start it
                        log.info("Starte Thread");

                        //create thread with streams and actual data
                        jobThread = new JobThread(in, out, data);
                        jobThread.setName(jobId);
                        map.put(jobId, jobThread);
                        jobThread.start();

                    } else {

                        // problem. Tell database
                        Hashtable<String, String> ret = new Hashtable<String, String>();
                        ret.put("INFO", "Invalid Sequence for " + jobId + " and request" + info);
                        ret.put("CODE", "8");
                        out.writeObject(ret);
                        out.flush();
                        out.close();
                        in.close();
                        log.debug("Invalid Sequence for " + jobId + " and request" + info);
                    }
                }

            } catch (Exception ie) {
                // cannot do much on this
                try {
                    socket.close();
                } catch (Exception e) {
                    log.error(e.getMessage());
                } finally {
                    logException("IOError in Thread Ini Socket=" + socket.toString(), ie);
                    showwarn("Network error", "Message is " + ie.getLocalizedMessage());
                }

            } /*
             * catch ( ClassNotFoundException ce ) { // seems to be someone we
             * don't like try { socket.close(); } catch ( Exception e ) {}
             * logException("Invalid data in request", ce);
             * showwarn("Invalid request data", "Message is " +
             * ce.getLocalizedMessage()); }
             */
            /*catch (Exception e) {

             try {
             socket.close();
             } catch (Exception e1) {
             log.error(e1.getMessage());
             }finally {
             logException("Unknown error in request", e);
             showwarn("Error", "Message is " + e.getLocalizedMessage());
             }
             }*/

            return true;

        }

        /**
         * Shows a warn dialog.
         *
         * @param title title of warn
         * @param msg   message of warn
         * @return user choice
         */
        private int showwarn(String title, String msg) {

            Object[] options = {"Cancel"};
            return JOptionPane.showOptionDialog(null, msg, title, JOptionPane.DEFAULT_OPTION,
                    JOptionPane.WARNING_MESSAGE, null, options, options[0]);
        }

//        private class UpdateTreeTimeout extends TimerTask {
//            App app;
//
//            private UpdateTreeTimeout(App app) {
//                this.app = app;
//            }
//
//            public void run() {
//                app.updateTree();
//            }
//        }
    }

    /**
     * Started by each JobThread. When timeout occurs, force the thread to
     * finish.
     *
     * @author rainer bruns Created 07.01.2005
     */
    private class Timeout extends TimerTask {

        String parentId;

        /**
         * @param parentId id of thread(the job id)
         */
        public Timeout(String parentId) {

            this.parentId = parentId;
        }

        /**
         * Main processing method for the Timeout object Create a Hashtable with
         * execution info for thread. This is ERROR -> means thred should close
         * MSG and CODE: at least try to inform the db, but probably not
         * succesful.
         *
         * @see JobThread#closeThread(String)
         * @see LoaderAgent#stopThread(Hashtable, String)
         */
        public void run() {

            try {

                Hashtable<String, Object> data = new Hashtable<String, Object>();
                data.put("INFO", "ERROR");
                data.put("MSG", "TIMEOUT for thread");
                data.put("CODE", "111");
                stopThread(data, parentId);

            } catch (Exception ignored) {
                log.error(ignored);
            }

        }

    }

    /**
     * * JobThread handles all the request for one database Job(loading files
     * from/to database, starting applications, showing and updating a progress
     * bar...). Thread will only finish when called with information ENDE or
     * ERROR, or when method closeThread is called directly. Execution of
     * request is done as follows: first method setEnv is called by LoaderAgent,
     * with the appropriate streams from socket and actual execution data(in a
     * Hashtable). Then thread is interrupted, reads INFO field of Hashtable,
     * does its work, and inform the database that work is done(successfull or
     * with error). Then wait until next interrupt, except INFO was ENDE or
     * ERROR.
     *
     * @author rainer bruns Created 20.06.2004 f
     */
    private class JobThread extends Thread {

        //our job id
        String jobId;
        // the Hashtable with execution info
        Hashtable<String, Object> data;
        //GUI stuff
        JFrame frame = new JFrame();
        int bytesLoaded = 0;
        //        //where we put all the files
        String directory = baseDir + System.getProperty("file.separator");
        //Sreams from socket
        ObjectInputStream istream;
        ObjectOutputStream ostream;
        String logInfo;
        java.util.Timer timer;
        JTable baseTable;
        Object action;
        NodesTableModel model;
        String toolTipText;
        private boolean mouseEntered;
        private App app;
// 1

        // hour

        /**
         * Creates a JobThread with the streams and execution data
         *
         * @param in  inputstream from socket
         * @param out outputstream from socket
         * @param d   execution data
         */
        public JobThread(ObjectInputStream in, ObjectOutputStream out, Hashtable<String, Object> d) {

            data = d;
            jobId = data.get("HEAD").toString();
            logInfo = " for " + jobId;
            log.info("New Thread" + logInfo);
            istream = in;
            ostream = out;
            //jobCounter++;
        }


        /*
         * (non-Javadoc)
         * 
         * @see java.lang.Runnable#run() Just tell database we have started and
         *      then wait
         */
        public void run() {

            try {
                /*create directory*/
                String folderName = "";
                try {
                    folderName = data.get("FOLDERNAME").toString();
                } catch (Exception e) {
                    log.error(e.getMessage());
                }
                if (folderName.equals("")) {
                    folderName = jobId + "_" + System.currentTimeMillis();
                }
                directory = directory + folderName + System.getProperty("file.separator");
                log.debug("directory: " + directory);
                directories.add(directory);

                writeFinalMessage(initLoader(), "Initialsieren" + logInfo);
                //setTimeout(timeout);
                goWaiting();

            } catch (Exception e) {

                logException("Error in starting Thread for " + jobId, e);
                e.printStackTrace();

            }

        }

        /**
         * Called from LoaderAgent on each request before interrupting. Sets
         * appropriate streams and execution data.
         *
         * @param in  inputstream from socket
         * @param out outputstream from socket
         * @param d   execution data
         */
        public void setEnv(ObjectInputStream in, ObjectOutputStream out, Hashtable<String, Object> d) {

            data = d;
            ostream = out;
            istream = in;

        }

        /**
         * After each request wait until interrupted
         */
        private synchronized void goWaiting() {
            log.debug("goWaiting " + data.get("INFO").toString());
            try {
                wait();
            } catch (InterruptedException re) {

                // and now start to work
                String info = data.get("INFO").toString();
                // log.info("info = " + info);
                executeJob(info);

            }

        }

        /**
         * Depending on info call the method to execute, inform db that work is
         * done(or not) and go waiting again(except on ENDE/ERROR). Improve:
         * make it static Strings in some class, that both Loader and here must
         * know.
         *
         * @param info describes what do to
         */
        private void executeJob(String info) {
            log.debug("executeJob: " + info + " " + data);
            switch (info) {
                case "Progress":

                    //set progress bar to new value
                    log.info("Progress request for " + jobId + " " + data.get("PROGRESS") + " " + data.get("STATUS"));
                    //setProgress();
                    refreshTableProgressOnly();
                    writeFinalMessage(0, "SUCCESS init " + logInfo);

                    break;
                case "InitUpload":

                    //prepare an upload -> make directory
                    log.info("Staring upload for " + jobId);
                    writeFinalMessage(initUpload(), "initUpload" + logInfo);

                    break;
                case "LoadBlob":

                    //load a blob from db
                    log.info("Loading Blob for " + jobId);
                    writeFinalMessage(writeBlobFile(), "Write Blob" + logInfo);

                    break;
                case "LoadClob":

                    // load a Clob from db
                    log.info("Loading Blob for " + jobId);
                    writeFinalMessage(writeClobFile(), "WriteClob" + logInfo);

                    break;
                case "LoadFinished":

                    // do whatever is in config.xml with loaded files
                    try {

                        log.info("Configured Job for " + jobId);
                        writeFinalMessage(doTheJobs(), " Completed Job" + logInfo);
                        refreshTable();

                    } catch (Exception e) {

                        log.debug("Error in Configured Job for " + jobId);
//                    log.log(Level.debug, e.getLocalizedMessage(), e);
                        writeFinalMessage(1, "Job Execution Error" + logInfo);
                        refreshTable();

                    }

                    break;
                case "InitDownLoad":

                    break;
                case "DownloadBlob":

                    log.info("Starting download Blob for " + jobId);
                    writeFinalMessage(loadBlobFileToDB(false), "BlobToDb " + logInfo);

                    break;
                case "DownloadClob":

                    log.info("Starting download Clob for " + jobId);
                    writeFinalMessage(loadClobFileToDB(false), "ClobToDB " + logInfo);

                    break;
                case "DownloadPreparedBlob":

                    log.info("Starting special download blob for " + jobId);
                    writeFinalMessage(loadBlobFileToDB(true), "PreparedBlobToDB " + logInfo);

                    break;
                case "DownloadPreparedClob":

                    log.info("Starting special download clob for " + jobId);
                    writeFinalMessage(loadClobFileToDB(true), "PreparedClobToDB " + logInfo);

                    break;
                case "GetFileNames":

                    log.info("Starting put file names for " + jobId);
                    writeFinalMessage(putFileNamesToDB(), "putFileNamesToDB " + logInfo);

                    break;
                case "ERROR":

                    // on error and end clean up and do not wait anymore
                    writeErrorMessage();
                    closeThread(info);
                    return;

            }

            if (info.equals("ENDE")) {

                closeThread(info);
                return;

            }

            goWaiting();

        }

        /**
         * Backend waits for this message. After each execution step, tell what
         * has happened. Then close all streams.
         *
         * @param code    0 -> success, else some error
         * @param message some string for errors
         */
        private void writeFinalMessage(int code, String message) {

            try {
                Hashtable<String, Object> writeBack = new Hashtable<String, Object>();
                log.info("Writing back: code= " + code + " message=" + message);
                writeBack.put("INFO", message);
                writeBack.put("CODE", Integer.toString(code));
                ostream.writeObject(writeBack);
                ostream.flush();
                istream.close();
                ostream.close();
            } catch (Exception ie) {
                log.debug("IOError while writing back for " + jobId);
            }

            try {
                //code not 0 -> there was an error, rename dir to keep it
                if (code != 0) {
                    File neu = new File(directory + ".err");
                    File old = new File(directory);
                    old.renameTo(neu);
                    log.debug("code " + code + " " + neu.getPath() + " " + neu.getName() + " " + old.getName());
                }
            } catch (Exception ignored) {
                log.error(ignored);
            }
//            refreshTable();
        }

        /**
         * Called from run() methods, initialize the GUI stuff
         *
         * @return 0 -> success, 1 on exception
         */
        private int initLoader() {
            log.debug("begin jobid: " + jobId + " jobsTableInfo=" + jobsTableInfo);
            if (jobsTableInfo != null) {
                app = jobsTableInfo.getApp();
                baseTable = app.getTable();
                statusBarThread = app.getStatusBarThread();
                final ProgressStatusBarItem statusBarProgress = statusBarThread.getStatusBarProgress();
                jobsTableInfo.setRunLoader(true);

                if (!statusBarThread.isAlive()) {
                    statusBarThread.setNotEmpty(true);
                    statusBarThread.run();
                }

                refreshTable();

                MouseListener[] listeners = statusBarProgress.getMouseListeners();
                for (MouseListener listener : listeners) {
                    log.debug("listener = " + (listener instanceof BarProgressMouseListener));
                    if (listener instanceof BarProgressMouseListener) {
                        statusBarProgress.removeMouseListener(listener);
                        BarProgressMouseListener progressMouseListener = new BarProgressMouseListener(statusBarProgress) {
                            @Override
                            public void mouseClicked(MouseEvent e) {
                                // log.debug("mouseClicked");
                                jobsTableInfo.show();
                                refreshTable();
                                setText(toolTipText);
                                super.mouseClicked(e);
                            }

                            @Override
                            public void mouseEntered(MouseEvent e) {
                                mouseEntered = true;
                                refreshTable();
                                //log.debug("mouseEntered " + toolTipText);
                                setText(toolTipText);
                                super.mouseEntered(e);
                            }
                        };
                        statusBarProgress.addMouseListener(progressMouseListener);
                    }
                }

                if (jobsTableInfo != null) {
                    frame = jobsTableInfo.getApp().getAppFrame();
                }
            }
            log.info("Thread initialized for " + jobId);
            return 0;
        }

        private void initAlert(String text) {
            log.info("initAlert head = " + text);
            AlertGroup _alertGroup = new AlertGroup();

            Alert alert = new Alert();
            alert.getContentPane().setLayout(new BorderLayout());
            // alert.getContentPane().add(createAlert(head, aJProgressBar));
            final JLabel message = new JLabel(text);

            PaintPanel panel = new PaintPanel(new BorderLayout(6, 6));
            panel.setBorder(BorderFactory.createEmptyBorder(6, 7, 7, 7));
            panel.add(message, BorderLayout.NORTH);
            alert.getContentPane().add(message);
            alert.setResizable(true);
            alert.setMovable(true);
            alert.setTimeout(2000);
            alert.setTransient(false);
            alert.setResizable(false);
            alert.setPopupBorder(BorderFactory.createLineBorder(new Color(10, 30, 106)));

            CustomAnimation showAnimation = new CustomAnimation(CustomAnimation.TYPE_ENTRANCE,
                    CustomAnimation.EFFECT_FLY, CustomAnimation.SMOOTHNESS_MEDIUM, CustomAnimation.SPEED_MEDIUM);
            showAnimation.setDirection(CustomAnimation.BOTTOM);
            showAnimation.setVisibleBounds(PortingUtils.getLocalScreenBounds());
            alert.setShowAnimation(showAnimation);

            CustomAnimation hideAnimation = new CustomAnimation(CustomAnimation.TYPE_ENTRANCE,
                    CustomAnimation.EFFECT_FLY, CustomAnimation.SMOOTHNESS_MEDIUM, CustomAnimation.SPEED_MEDIUM);
            hideAnimation.setDirection(CustomAnimation.BOTTOM);
            hideAnimation.setVisibleBounds(PortingUtils.getLocalScreenBounds());
            alert.setHideAnimation(hideAnimation);

            _alertGroup.add(alert);
            alert.showPopup(SwingConstants.SOUTH_EAST);
//            alert.showPopup(SwingConstants.CENTER);

        }

        /**
         * refresh jobs table model
         *
         * @return true if table model refresched
         */
        private boolean refreshTable() {
            log.debug("refreshTable begin: jobid=" + jobId
                    + " shutdown=" + shutdown);
            model = null;

            //set model with data from DB
            if (jobsTableInfo != null) {
                if ((jobsTableInfo.isDisplayed() || mouseEntered) && !markedForShutdown) {
                    EventQueue.invokeLater(new Runnable() {
                        public void run() {

                            int selectedRow = baseTable.getSelectedRow();

                            log.debug("dispatcher.getMenuForJobs() begin ");
                            action = app.getBase().getMenuForJobs();
                            log.debug("dispatcher.getMenuForJobs() end " + action);
                            if (action instanceof DetailView) {
                                //log.debug(baseTable.getModel());

                                DetailView detailView = (DetailView) action;
                                Nodes nodes = detailView.getNodes();
                                UserInputs[] uis = new UserInputs[nodes.getSize()];

                                StringBuilder buffer = new StringBuilder("<html>");
                                boolean isJob = false;

                                for (int i = 0; i < uis.length; i++) {
                                    ValueList valueList = ((TreeNodeRecord) nodes.getNode(i)).getValueList();
//                            for (int k = 0; k < valueList.getSize(); k++) {
//                                log.debug(valueList.get(k).getParameterName() + " : " + valueList.get(k).getParameterValue());
//                            }
                                    uis[i] = detailView.getNodeProperties(nodes.getNode(i));

                                    if (mouseEntered) {
                                        for (int j = 0; j < uis[i].getSize(); j++) {

                                            if (uis[i].get(j).getData() instanceof JProgressBar) {

                                                String progress = ((TextInputImpl) uis[i].get(j - 1).getData()).getText();
                                                buffer.append("job ").append(((TextInputImpl) uis[i].get(0).getData())
                                                        .getText()).
                                                        append(" ");
                                                if (StringUtils.isNotEmpty(progress)) {
                                                    buffer.append(progress).append("<br>");
                                                }
                                                isJob = true;
                                                break;
                                            }
                                        }
                                    }
                                }
                                buffer.append("</html>");

                                if (isJob) {
                                    toolTipText = buffer.toString();
                                } else {
                                    toolTipText = "No jobs";
                                }
                                log.debug("toolTipText = " + toolTipText);
                                if (jobsTableInfo.isDisplayed()) {
                                    model = new NodesTableModel(nodes, uis);

                                    if (jobsTableInfo.isDisplayed()) {
                                        baseTable.setModel(model);
                                        jobsTableInfo.setModel(model);
                                    }

                                    try {
                                        if (selectedRow == -1) {
                                            baseTable.clearSelection();
                                        } else {
                                            baseTable.setRowSelectionInterval(selectedRow, selectedRow);
                                        }
                                    } catch (Exception ignored) {
                                        log.error(ignored);
                                    }
//                            TableTools.selectFirstRow(baseTable, false);
                                    TableTools.autoSizeColumns(baseTable, false);
                                }
                            } else if (action instanceof WarningMsgBox) {
                                jobsTableInfo.getApp().setNodeDetailsClear();
                            }
                            // update model
                            log.debug("model: " + model);
                            if (model != null) {
                                model.fireTableDataChanged();
                            }
                        }
                    });
                }
            }

//            log.debug("refreshTable end:model " + model + " " + toolTipText + " " + statusBarThread.isAlive());
            return model != null;
        }

        private boolean refreshTableProgressOnly() {

            try {
                model = (NodesTableModel) TableModelWrapperUtils.getActualTableModel(baseTable.getModel());
                UserInputs[] uis = model.getUserInputs();
                StringBuilder buffer = new StringBuilder("<html>");
                boolean isJob = false;
                for (UserInputs ui : uis) {
                    JProgressBar progressBar = null;
                    TextInputImpl progressText = null;
                    TextInputImpl textInputJobId = null;
                    for (int i = 0; i < ui.getSize(); i++) {
//                       if( ui.get(i).getData() instanceof TextInputImpl )
//                    log.debug(i+": "+((TextInputImpl)ui.get(i).getData()).getText());
                        Object data = ui.get(i).getData();
                        if (data instanceof JProgressBar) {
                            progressBar = (JProgressBar) ui.get(i).getData();
                        }
                        if (data instanceof TextInputImpl && ui.get(i).getLabel().equals(progressTransl)) {
                            progressText = (TextInputImpl) data;
                        }
                        if (data instanceof TextInputImpl && ui.get(i).getLabel().equals(isrJobTransl)) {
                            textInputJobId = (TextInputImpl) data;
                        }
                    }
                    if (textInputJobId != null && jobId.equals(textInputJobId.getText())) {
//                        log.debug(((TextInputImpl)(ui.get(0).getData())).getText());
                        String progress = (String) data.get("PROGRESS");
                        if (progressText != null) {
                            progressText.setText(progress + " %");
                        }
                        if (progressBar != null) {
                            progressBar.setValue(Integer.parseInt(progress));
                            progressBar.setString((String) data.get("STATUS"));
                        }
                        if (mouseEntered) {
                            buffer.append("job ").append(textInputJobId.getText()).append(" ");
                            if (StringUtils.isNotEmpty(progress)) {
                                buffer.append(progress).append(" %").append("<br>");
                            }
                            isJob = true;
                        }
                        break;
                    }
                }
                buffer.append("</html>");
                if (isJob) {
                    toolTipText = toolTipText + buffer.toString();
                }

                model.fireTableDataChanged();
            } catch (Exception e) {
                log.error(e.getMessage());
            }

            return model != null;
        }

        /**
         * When thread exits with error, write to log.
         */
        private void writeErrorMessage() {

            String message = data.get("MSG").toString();
            String code = data.get("CODE").toString();
            log.debug("Backenderror:" + message + "; code: " + code + " for " + jobId);
        }

        /**
         * Tries to create the directory where files loaded are stored. This is
         * basedir\jobId. If this fails return 1, job execution has failed.
         *
         * @return 0 -> success, 1 on error
         */
        private int initUpload() {
            refreshTable();
            log.info("Creating directory " + directory + " for " + jobId);
            File dir = new File(directory);
            if (!dir.isDirectory() && !dir.mkdirs()) {
                log.debug("Error creating directory " + directory + " for " + jobId);
                return 1;
            }
            return 0;
        }

        /**
         * Reads filename and content from request data, writes content to job
         * directory.
         *
         * @return 0 on success, 2 on IOException, 5 if request data are invalid
         */
        private int writeBlobFile() {

            String filename = null;
            try {
                Hashtable fileData;
                fileData = (Hashtable) istream.readObject();
                log.debug("fileData.size()=" + fileData.size());
                filename = fileData.get("FILENAME").toString();
                log.info("Loading " + filename + logInfo);
                refreshTable();
                log.debug("after refreshing");
                File f = new File(directory, filename);
                log.debug("file:" + f);
                FileOutputStream fileOutStream = new FileOutputStream(f);
                fileOutStream.write((byte[]) fileData.get("CONTENT"));
                log.debug("CONTENT=" + fileData.get("CONTENT"));
                bytesLoaded = bytesLoaded + Integer.parseInt(fileData.get("BYTES").toString());
                log.debug("bytesLoaded=" + bytesLoaded);
                fileOutStream.close();
            } catch (IOException ie) {
                log.debug("Error writing blobfile: " + ie.getLocalizedMessage() + " " + filename + logInfo);
                return 2;
            } catch (ClassNotFoundException ce) {
                log.debug("Invalid Data on Loading Blob" + logInfo);
                return 5;
            }
            return 0;
        }

        /**
         * Reads filename and content from request data, writes content to job
         * directory.
         *
         * @return 0 on success, 2 on IOException, 5 if request data are invalid
         */
        private int writeClobFile() {

            String filename = null;
            try {
                Hashtable fileData;
                fileData = (Hashtable) istream.readObject();
                filename = fileData.get("FILENAME").toString();
                log.info("Loading " + filename + logInfo);
                File f = new File(directory, filename);
                FileWriter fileOutStream = new FileWriter(f);
                ArrayList content = (ArrayList) fileData.get("CONTENT");
                for (Object aContent : content) {
                    String line = aContent.toString();
                    fileOutStream.write(line);
                    bytesLoaded = bytesLoaded + line.length();
                }
                fileOutStream.close();

            } catch (IOException ie) {
                log.debug("Error writing clobfile: " + ie.getLocalizedMessage() + " " + filename + logInfo);
                return 2;
            } catch (ClassNotFoundException ce) {
                log.debug("Invalid Data on Loading Clob" + logInfo);
                return 5;
            }
            return 0;
        }

        /**
         * After each upload is finished(that does mean groups of file, not
         * single files) some operation has to be done. What has to be done is
         * read from file config.xml, loaded from database. Operation actually
         * is displaying this file with default application, displaying file
         * with given(in config.xml) application, saving file to new location or
         * starting wordmodule.exe and wait for it to finish. Grown
         * evolutianary(?). Give better structure.
         *
         * @return 0 on success, 1 on error
         * @throws Exception
         */
        private int doTheJobs() throws Exception {

            int retval = 0;

            // do we have to wait for an exeternal process?
            boolean wait = Boolean.valueOf(data.get("MODUS").toString());

            //helps reading XML, something like XPath
            configPath = directory + System.getProperty("file.separator") + "config.xml";
            FileInputStream configFile = new FileInputStream(configPath);
            XmlHandler xmlHandler = new XmlHandler(configFile);
            try {
                Node fullPathNode = xmlHandler.getNode("//apps/fullpathname");
                String fullPath = fullPathNode.getFirstChild().getNodeValue();
                if (fullPath != null && !new File(fullPath).exists()) {
                    fullPath = pattern1.matcher(fullPath).replaceAll(directory.replaceAll("\\\\", "\\\\\\\\"));
                }
                fullPathNode.getFirstChild().setNodeValue(fullPath);
                FileOutputStream fos = new FileOutputStream(configPath);
                xmlHandler.save(fos);
            } catch (Exception ignored) {
                log.error(ignored);
            }


            /*
             * There can be several tasks to execute. They are ordered by
             * attribute order. Bring them in the right order according to this
             * attribute.
             */
            NodeList apps = xmlHandler.selectNodes("//app");
            TreeMap<String, Node> appNodes = orderNodesByAttribute(apps, "order",
                    xmlHandler);
            Set set = appNodes.keySet();
            Iterator it = set.iterator();

            int processCnt = set.size();
            int curProcess = 0;
            log.info("Have to do " + processCnt + " jobs.");

            canceled = false;

            //loop through tasks
            while (it.hasNext()) {

                String order = it.next().toString();
                Node node = appNodes.get(order);
                curProcess++;
                boolean nextProcessWait = false;

                /*
                 * Each App Node has attribute name -> if Me we have to do job,
                 * else some application. Node command under Appl contains
                 * processing command
                 */
                try {
                    Node waitNode = xmlHandler.getNode(node, "wait_for_process", null, null);
                    wait = Boolean.valueOf(waitNode.getFirstChild().getNodeValue());
                    if (curProcess < processCnt) {
                        String nextOrder = Integer.toString(Integer.parseInt(order) + 1);
                        Node nextNode = appNodes.get(nextOrder);
                        Node nextWaitNode = xmlHandler.getNode(nextNode, "wait_for_process", null, null);
                        nextProcessWait = Boolean.parseBoolean(nextWaitNode.getFirstChild().getNodeValue());
                    }
                } catch (Exception e) {
                    log.info("Exception " + e.getLocalizedMessage());
                }
                Node commandNode = xmlHandler.getNode(node, "command", null, null);
                String command = commandNode.getFirstChild().getNodeValue();
                String name = xmlHandler.attributeRead("name", node);
                log.info("Processing job " + curProcess + "/" + processCnt + " " + name + " " + command + logInfo + ""
                        + " waiting " + wait);

                if (name.equalsIgnoreCase("Me")) {

                    // interpret command there and execute
                    retval = execute(node, xmlHandler, command);
                    if (retval != 0) {
                        log.info("Failed " + retval);
                        return retval;
                    }

                } else if (name.equalsIgnoreCase("APPL")) {

                    if (canceled) {
                        return -2;
                    }

                    // start some document with given application available on system
                    Process p = executeApplication(node, xmlHandler, command);

                    if (p == null && canceled) {
                        return -2;
                    }
                    if (p == null) {
                        return 1;
                    }

                    if (wait) {

                        // process ran into the timeout or failed
                        // ge�ndert mcd 23.06.2005
                        retval = waitForProcess(p, curProcess, processCnt, nextProcessWait);
                        if (retval != 0) {

                            log.info("Failed/Timeout " + retval);
                            return retval;
                        }
                    }

                } else {

                    if (canceled) {
                        return -2;
                    }

                    // start some document with given application loaded from db(-> wordmodule)
                    String executable = xmlHandler.getNode(node, "executable", null, null).getFirstChild().getNodeValue();
//                    progressBar.setString(Res.getString("LoaderAgent.progress.text.exec") + " " + executable);
                    //progressBar1.setString(Res.getString("LoaderAgent.progress.text.exec") + " " + executable);
                    refreshTable();
                    // Start the process, but never with process.waitFor()
                    Process p = getProcess("\"" + directory + System.getProperty("file.separator") + executable + "\" config"
                            + ".xml " + order);

                    if (p == null && canceled) {
                        return -2;
                    }
                    if (p == null) {
                        return 1;
                    }

                    if (wait) {

                        // process ran into the timeout or failed
                        // ge�ndert mcd 23.06.2005
                        retval = waitForProcess(p, curProcess, processCnt, nextProcessWait);
                        if (retval != 0 && retval != -3 && !repeated) {

                            log.info("Failed/Timeout " + retval);
                            return retval;
                        }

                        if (!repeated && retval == -3) {
                            log.info("Repeat job");
                            repeated = true;
                            retval = doTheJobs();
                            if (retval != -3) {
                                repeated = false;
                            }
                            return retval;
                        }
                    }
                }
            }

            log.info("retval is " + retval);
            return retval;
        }

        /**
         * Reads the parameter string and calls appropriate method.
         *
         * @param app        the Node from XML for this execution
         * @param XmlHandler our XmlHandler
         * @param parameter  a string indicating what to do
         * @return 0 on succes, 1 on error
         */
        private int execute(Node app, XmlHandler XmlHandler, String parameter) {

            log.info("Executing " + parameter + logInfo);

            if (parameter.equalsIgnoreCase("SAVE")) {

                // save file(s) to new location
                return copyFiles(XmlHandler, app);

            } else if (parameter.equalsIgnoreCase("DELETE")) {

                // delete files if flag is set 
                return deleteFiles(XmlHandler, app);

            } else if (parameter.equalsIgnoreCase("DISPLAY")) {

                // display file(s) with default application
                displayDocument(XmlHandler);
            }
            return 0;
        }

        /**
         * Reads application to start and parameter list from XML and tries to
         * start the application with this parameters.
         *
         * @param app        the node from XML for this execution
         * @param xmlHandler our XmlHandler
         * @param program    not used
         * @return 0 on success, 1 else
         */
        private Process executeApplication(Node app, XmlHandler xmlHandler, String program) throws XPathExpressionException {

            String thePath;
            String[] allPathes;
            Process p;

            //application to start, i.e. excel.exe
            String application = xmlHandler.getNode(app, "executable", null, null).getFirstChild().getNodeValue();

            /*
             * There can be several parameters. They are ordered by attribute
             * order. Bring them in the right order according to this attribute.
             */
            TreeMap<String, Node> paraNodes = orderNodesByAttribute(xmlHandler.selectNodes("//app/parameter"), "order",
                    xmlHandler);
            String[] parameter = new String[paraNodes.size() + 1];

            /*
             * create the string for Runtime.exec. executable parameter1, parameter2.... fileToDisplay
             */
            parameter[0] = application;
            Set set = paraNodes.keySet();
            Iterator it = set.iterator();

            // loop through parameter list
            for (int i = 1; it.hasNext(); i++) {
                String order = it.next().toString();
                Node node = paraNodes.get(order);
                parameter[i] = pattern1.matcher(node.getFirstChild().getNodeValue()).replaceAll(directory.replaceAll("\\\\",
                        "\\\\\\\\"));
                try {
                    String downloadPath = xmlHandler.nodeRead("//fullpathname[1]") + System.getProperty("file.separator");
                    String downloadFile = xmlHandler.nodeRead("//target[1]");
                    parameter[i] = pattern1.matcher(parameter[i]).replaceAll(directory.replaceAll("\\\\", "\\\\\\\\"));
                    parameter[i] = pattern2.matcher(parameter[i]).replaceAll(downloadPath.replaceAll("\\\\",
                            "\\\\\\\\"));
                    parameter[i] = pattern3.matcher(parameter[i]).replaceAll(downloadFile.replaceAll("\\\\",
                            "\\\\\\\\"));
                } catch (NullPointerException ignored) {
                    log.error(ignored);
                }
                log.info(parameter[i]);
            }

            // 0. try to start application with the temp path
            try {
                parameter[0] = directory + application;
                if ((p = tryToStartProgram(parameter)) != null) {
                    canceled = false;
                    return p;
                }
            } catch (NullPointerException ignored) {
                log.error(ignored);
            }

            // 1. first try to start application with a path we have in memory
            try {
                thePath = pathStore.get(program);
                parameter[0] = thePath + System.getProperty("file.separator") + application;
                if ((p = tryToStartProgram(parameter)) != null) {
                    canceled = false;
                    return p;
                }
            } catch (NullPointerException ignored) {
                log.error(ignored);
            }

            // 2. this try failed. No read our own path. Try to start
            // application with all those entries
            //allPathes = props.getProperty( "loader.path" ).split( ";" );
            String pathList;
            try {
                pathList = xmlHandler.nodeRead("//applicationpathes[1]");
            } catch (Exception e) {
                pathList = null;
            }
            if (pathList != null) {
                allPathes = pathList.split(";");
                for (String allPathe : allPathes) {
                    thePath = allPathe;
                    parameter[0] = thePath + System.getProperty("file.separator") + application;
                    //yeaah.. found application.
                    if ((p = tryToStartProgram(parameter)) != null) {
                        canceled = false;
                        return p;
                    }
                }
            }

            // application not in path. Show a warn.
            int answer = showwarn(Res.getString("Loader.program.short"), Res.getString("Loader.program.text") + " "
                    + application);

            if (answer == 0) {

                // ask user to select a correct path
                File file = showFileDialog();

                if (file != null) {
                    if (file.isDirectory()) {
                        thePath = file.getAbsolutePath();
                    } else {
                        thePath = file.getParent();
                    }
                    parameter[0] = thePath + System.getProperty("file.separator") + application;
                    // 3. and now try to start with this path
                    if ((p = tryToStartProgram(parameter)) != null) {
                        //success. Store to our own pathes
                        //props.setProperty( "loader.path", props.getProperty( "loader.path" ) + ";" + thePath );
//		            	XmlHandler.setValue(XmlHandler.getFirstNode("config.apps","applicationpathes"),
// (pathList != null) ? pathList + ";" + thePath : thePath);
                        xmlHandler.createElement("applicationpathes", (pathList != null) ? pathList + ";" + thePath : thePath, null, xmlHandler.getNode("//apps"));
                        try {
                            FileOutputStream fos = new FileOutputStream(configPath);
                            xmlHandler.save(fos);
                        } catch (Exception ignored) {
                            log.error(ignored);
                        }
                        pathStore.put(program, thePath);
                        canceled = false;
                        return p;
                    }
                } else {
                    canceled = true;
                    return null;
                }
            } else {
                canceled = true;
                return null;
            }
            // enough. May loop until user finds or whatever
            canceled = false;
            return null;
        }

        /**
         * Display document with default application for the extension. Used on
         * view common docs. Here path is threads workin directory.
         *
         * @param xmlHandler our XmlHandler
         */
        private void displayDocument(XmlHandler xmlHandler) {

            String path = xmlHandler.nodeRead("//fullpathname");
            String document = xmlHandler.nodeRead("//target[1]");
//            String extension = XmlHandler.getValue(XmlHandler.getFirstNode("config.apps", "extension"));

            /*
             * Try to get default program for extension. If not, show a list of
             * all programs where the user can select one.
             */

            /*Program p = Program.findProgram( "." + extension );
             if ( p == null ) {
             GetProgram program = new GetProgram( new Display(), frame );
             if ( ( p = program.getSelectedItem() ) == null ) return;
             }
             log.info( "Loading Program for " + extension + " and document " + document );
             p.execute( path + document );
             */
//            String[] parameter = {"cmd.exe", "/C", path + document};
            if (Desktop.isDesktopSupported()) {
                try {
                    Desktop.getDesktop().open(new File(path + document));
                } catch (IOException e) {
                    log.error(e);
                }
            }
        }

        /**
         * Try to call Runtime.exec with parameter. Return true if success, else
         * false
         *
         * @param parameter the parameter list for Runtime.exec
         * @return true if successfull, else false
         */
        private Process tryToStartProgram(String[] parameter) {

            Process p;
            try {
                String process = "";
                for (String aParameter : parameter) {
                    process += aParameter + " ";
                }
                log.info("Try to start programm " + process);
                p = Runtime.getRuntime().exec(parameter);
                log.info("Started program " + parameter[0] + logInfo);
                return p;
            } catch (IOException ie) {
                log.warn(ie.getLocalizedMessage());
                return null;
            } catch (Exception e) {
                log.warn(e.getLocalizedMessage());
                return null;
            }
        }

        /**
         * check if a list of files really exist
         *
         * @param files files array
         * @return true if all files exist else false
         */
        public boolean checkFilesExist(File[] files) {

            for (File file : files) {
                if (!file.exists()) {
                    return false;
                }
            }

            return true;

        }

        /**
         * Let the user select file or directory. Used here if an application
         * can not be found to get the path to application.
         *
         * @return selected file
         */
        private File showFileDialog() {

            JFileChooser fc;

            try {
                fc = new JFileChooser(System.getProperty("user.home"));
            } catch (Exception e) {
                // for Vista and Windows 7
                try {
                    UIManager.setLookAndFeel(UIManager.getCrossPlatformLookAndFeelClassName());
                } catch (Exception ignored) {
                    log.error(ignored);
                }
                fc = new JFileChooser(System.getProperty("user.home"));
                try {
                    UIManager.setLookAndFeel(UIManager.getSystemLookAndFeelClassName());
                } catch (Exception ignored) {
                    log.error(ignored);
                }
            }

            fc.setMultiSelectionEnabled(true);
            fc.setFileSelectionMode(JFileChooser.FILES_AND_DIRECTORIES);
            fc.setMultiSelectionEnabled(true);
            fc.setDialogTitle(Res.getString("LoaderAgent.chooser.open.title"));
            fc.setApproveButtonText(Res.getString("LoaderAgent.chooser.open.ok"));
//            frame.toFront();
            int returnVal = fc.showSaveDialog(frame);
            if (returnVal == JFileChooser.APPROVE_OPTION) {
                return fc.getSelectedFile();
            }
            return null;
        }

        /**
         * This is called on document checkout. Copies all files loaded before
         * to a directory of user choice, or user.home if user does not select
         * any file or directory. Masterfile is the main document, other files
         * are??? Updates config.xml with new location.
         *
         * @param xmlHandler our XmlHandler
         * @param node       the XML node for this operation
         * @return name of "masterfile"
         */
        private int copyFiles(XmlHandler xmlHandler, Node node) {

            String targetDir = xmlHandler.nodeRead("//fullpathname");
            String targetName = xmlHandler.nodeRead("//target[1]");
            String overwrite = xmlHandler.nodeRead("//overwrite[1]");
            List<Node> sources = xmlHandler.selectNodes(node, "source");

            for (int i = 0; i < sources.size(); i++) {
                String sourceName = xmlHandler.attributeRead("name", sources.get(i));
                try {
                    if (targetName != null && targetName.length() != 0 && (isMaster(sources.get(i),
                            xmlHandler) || i == 0)) {
                        File file = new File(targetDir, targetName);
                        if ((file.exists() && overwrite.equals("T")) || !file.exists()) {
                            copy(targetDir, sourceName, targetName);
                        } else {
                            return -5;
                        }
                    } else {
                        File file = new File(targetDir, sourceName);
                        if ((file.exists() && overwrite.equals("T")) || !file.exists()) {
                            copy(targetDir, sourceName, sourceName);
                        } else {
                            return -5;
                        }
                    }
                } catch (IOException ioe) {
                    log.warn(ioe.getLocalizedMessage());
                    return 1;
                }
            }
            return 0;
        }

        /**
         * Prepares a list of file for later deletion(that means, stores names
         * in List)
         *
         * @param xmlHandler our XmlHandler
         * @param node       XML node for this operation
         * @return 0
         */
        private int deleteFiles(XmlHandler xmlHandler, Node node) {

            String dir = xmlHandler.nodeRead("//fullpathnam[1]");
            List<Node> sources = xmlHandler.selectNodes(node, "source");
            ArrayList<File> files = new ArrayList<File>(sources.size());
            for (Node source : sources) {
                files.add(new File(dir, xmlHandler.attributeRead("name", source)));
            }
            deleteFileList(files);
            return 0;
        }

        /**
         * A file is supposed to be the master, if value of attribute leading is
         * "T"
         *
         * @param node       node with source file information
         * @param xmlHandler our XmlHandler
         * @return true, if source is masterfile
         */
        private boolean isMaster(Node node, XmlHandler xmlHandler) {

            try {
                String flag = xmlHandler.attributeRead("leading", node);
                if (flag.equalsIgnoreCase("T")) {
                    return true;
                }
            } catch (NullPointerException ignored) {
                log.error(ignored);
            }

            return false;
        }

        /**
         * Copies file quelle to directory dir with new name ziel
         *
         * @param targetDir
         * @param sourceName
         * @param targetname
         * @throws IOException
         */
        private void copy(String targetDir, String sourceName, String targetname) throws IOException {

            File source = new File(directory, sourceName);
            File target = new File(targetDir, targetname);
            DataInputStream in = new DataInputStream(new FileInputStream(source));
            FileOutputStream fileOutStream = new FileOutputStream(target);
            int len = (int) source.length();
            byte[] buf = new byte[len];
            in.readFully(buf);
            fileOutStream.write(buf);
            in.close();
            fileOutStream.close();
            log.info("Copy: " + source + " nach " + target + " kopiert" + logInfo);
        }

        /**
         * @param toExecute String to path ro runtime.exec
         * @return the created process, null on error
         */
        private Process getProcess(String toExecute) {
            log.info(toExecute);
            Runtime runtime = Runtime.getRuntime();
            Process p;
            try {
                p = runtime.exec(toExecute);
                log.info("Started executable " + toExecute + logInfo);
            } catch (IOException ioe) {

                log.debug("Error creating process: " + toExecute + " Error " + ioe.getLocalizedMessage() + logInfo);
                return null;
            }

            return p;
        }

        /**
         * This method waits for the end of process execution. It reads on the
         * socket connected to database stored procedure. If request comes and
         * field INFO is PING looks for the exeit value of process. If not
         * available listen on socket again. If 0 return true, if <>0 return
         * false. If field info in request is TIMEOUT kill process and return
         * false.
         *
         * @param p               process to wait for
         * @param curProcess
         * @param processCnt
         * @param nextProcessWait
         * @return int 0 - on success 1 - on timeout -1 - on processFailure
         */
        // ge�ndert mcd 23.06.2005 von return boolean auf return int
        private int waitForProcess(Process p, int curProcess, int processCnt, boolean nextProcessWait) {

            if (debug) {
                log.info("waitForProcess " + p.toString() + " " + curProcess + "/" + processCnt + " nextProcessWait "
                        + nextProcessWait);
            }

            boolean waiting = true;
            //boolean retval = false;
            int retval = -1;

            Hashtable<String, Object> data;
            try {

                while (waiting) {

                    // read request from socket
                    data = (Hashtable<String, Object>) istream.readObject();
                    String info = data.get("INFO").toString();
                    log.info("info " + info);
                    data = new Hashtable<String, Object>();
                    if (info.equals("PING")) {

                        //check process exit val
                        try {

                            int exitVal = p.exitValue();
                            log.info("Process ready " + p.toString() + " with Exit " + exitVal + logInfo);

                            // wait for the next processes, when no error 
                            // or repeat process, when exitVal -3 and not yet repeated
                            if ((curProcess != processCnt && exitVal == 0 && nextProcessWait) || (exitVal == -3
                                    && !repeated)) {
                                // write information to db
                                data.put("INFO", "WORKING");
                                ostream.writeObject(data);
                                ostream.flush();
                                return exitVal;
                            }

                            // tell database exit code
                            data.put("INFO", Integer.toString(exitVal));
                            waiting = false;

                            // everythings ok
                            retval = exitVal;

                        } catch (Exception e) { //process not ready

                            log.info("Still waiting for " + p.toString() + logInfo);
                            // tell database about
                            data.put("INFO", "WORKING");

                        }

                    } else if (info.equals("TIMEOUT")) {

                        // timeout -> kill
                        data.put("INFO", "TIMEOUT");
                        waiting = false;
                        //retval = false;
                        retval = 1;
                        log.warn("Timeout for process " + p.toString() + logInfo);
                        p.destroy();

                    }

                    // write information to db
                    ostream.writeObject(data);
                    ostream.flush();

                }

            } catch (Exception e) {

                // whatever it was
                log.debug("Error waiting for process " + p.toString() + " Error: " + e.getLocalizedMessage()
                        + logInfo);

            }

            return retval;
        }

        /**
         * Calls loadBlobFileToDB with field FILENAME from request as filename.
         * If filename has no parent(is not a path) threads working directory is
         * prepended.
         *
         * @param special
         * @return 0 on success, else 1
         */
        private int loadBlobFileToDB(boolean special) {

            try {
                if (special) {
                    XmlHandler xmlHandler = new XmlHandler(directory + System.getProperty("file.separator") + "config"
                            + ".xml");
                    String directory = xmlHandler.nodeRead("//fullpathname[1]");
                    String document = xmlHandler.nodeRead("//target[1]");
                    File file = new File(directory, document);
                    if (!file.isFile() || !file.exists()) {
                        showMessage(Res.getString("LoaderAgent.title.error"),
                                Res.getString("LoaderAgent.text.fileNotExists"), JOptionPane.ERROR_MESSAGE);
                        return 1;
                    }
                    loadBlobFileToDB(file);
                } else if (new File(data.get("FILENAME").toString()).getParent() == null) {
                    loadBlobFileToDB(new File(directory, data.get("FILENAME").toString()));
                } else {
                    loadBlobFileToDB(new File(data.get("FILENAME").toString()));
                }
                refreshTable();
            } catch (Exception e) {
                showMessage(Res.getString("LoaderAgent.title.error"), Res.getString("LoaderAgent.text.fileLoad"
                        + ".error"), JOptionPane.ERROR_MESSAGE);
                return 1;
            }
            return 0;
        }

        /**
         * @param fileToLoad full qualified name of file to load
         * @return true if success
         * @throws Exception
         */
        private boolean loadBlobFileToDB(File fileToLoad) throws Exception {

            Hashtable<String, Object> data;
            log.info("Lade Bin Datei " + fileToLoad.getAbsolutePath() + logInfo);
            //get InputStream and read
            DataInputStream in = new DataInputStream(new FileInputStream(fileToLoad));
            int len = (int) fileToLoad.length();
            byte[] buf = new byte[len];
            in.readFully(buf);
            in.close();
            //make Hashtable to send back to database
            data = new Hashtable<>();
            data.put("INFO", "LOADFILE");
            data.put("HEAD", jobId);
            //the file content and name
            data.put("CONTENT", buf);
            data.put("FILENAME", fileToLoad);
            ostream.writeObject(data);
            ostream.flush();
            return true;
        }

        /**
         * Calls loadBlobFileToDB with field FILENAME from request as filename.
         * If filename has no parent(is not a path) threads working directory is
         * prepended.
         *
         * @param special
         * @return 0 on success, else 1
         */
        private int loadClobFileToDB(boolean special) {

            try {

                if (special) {
                    XmlHandler xmlHandler = new XmlHandler(directory + System.getProperty("file.separator") + "config"
                            + ".xml");
                    String directory = xmlHandler.nodeRead("//apps/fullpathname");
                    String document = xmlHandler.nodeRead("//target[1]");
                    File file = new File(directory, document);
                    if (!file.isFile() || !file.exists()) {
                        showMessage(Res.getString("LoaderAgent.title.error"),
                                Res.getString("LoaderAgent.text.fileNotExists"), JOptionPane.ERROR_MESSAGE);
                        return 1;
                    }
                    loadClobFileToDB(file);
                } else if (new File(data.get("FILENAME").toString()).getParent() == null) {
                    loadClobFileToDB(new File(directory, data.get("FILENAME").toString()));
                } else {
                    loadClobFileToDB(new File(data.get("FILENAME").toString()));
                }
            } catch (Exception e) {
                showMessage(Res.getString("LoaderAgent.title.error"), Res.getString("LoaderAgent.text.fileLoad"
                        + ".error"), JOptionPane.ERROR_MESSAGE);
                return 1;
            }
            return 0;
        }

        /**
         * Calls loadBlobFileToDB with field FILENAME from request as filename.
         * If filename has no parent(is not a path) threads working directory is
         * prepended.
         *
         * @return 0 on success, else 1
         */
        private int putFileNamesToDB() {

            try {
                log.info("Send name of files " + directory + " " + baseDir);
                String files[] = new File(directory).list();
                String entries[] = new String[files.length];
//                String file = new File(baseDir +
//                        System.getProperty("file.separator") + "loader" + basisPort + ".log").getAbsolutePath();
//                String entries[] = new String[files.length + (file.length() > 0 ? 1 : 0)];
//                if (entries.length > files.length) entries[files.length] = file;
                for (int i = 0; i < files.length; i++) {
                    entries[i] = files[i];
                }

                // make Hashtable to send back to database
                Hashtable<String, Object> data = new Hashtable<String, Object>();
                data.put("INFO", "PutFileNames");
                data.put("HEAD", jobId);
                // files content and name
                data.put("CONTENT", Arrays.asList(entries));
                ostream.writeObject(data);
                ostream.flush();

                log.info("Name of files sent " + Arrays.asList(entries));

            } catch (Exception e) {
                return 1;
            }
            return 0;
        }

        /**
         * @param fileToLoad full qualified name of file to load
         * @return true if success
         * @throws Exception
         */
        private boolean loadClobFileToDB(File fileToLoad) throws Exception {

            Hashtable<String, Object> data = new Hashtable<String, Object>();
            log.info("Lade Text Datei " + fileToLoad.getAbsolutePath() + logInfo);
            //get Reader and read line by line
            BufferedReader in = new BufferedReader(new FileReader(fileToLoad));
            ArrayList<String> lines = new ArrayList<String>();
            String line;
            while ((line = in.readLine()) != null) {
                lines.add(line);
            }
            in.close();
            //make Hashtable to send back to database
            data.put("INFO", "LOADFILE");
            data.put("HEAD", jobId);
            //files content and name
            data.put("CONTENT", lines);
            data.put("FILENAME", fileToLoad);
            ostream.writeObject(data);
            ostream.flush();
            log.info("Datei geladen " + fileToLoad + logInfo);
            return true;
        }

        /**
         * Deletes all files in the list after users confirmation
         *
         * @param files array
         */
        private void deleteFileList(ArrayList files) {

            try {
                for (Object file : files) {
                    ((File) file).delete();
                }
            } catch (Exception e) {
                log.info("Error deleting files" + logInfo);
            }
        }

        /**
         * Shows a warn dialog.
         *
         * @param title title of warn
         * @param msg   message of warn
         * @return user choice
         */
        private int showwarn(String title, String msg) {
            System.out.println("showwarn:title = " + title);
            /*
             frame.pack();
             frame.setVisible(true);
             frame.toFront();
             frame.requestFocus();  */
            Object[] options = {"Ok", "Cancel"};
            return JOptionPane.showOptionDialog(frame, msg, title, JOptionPane.DEFAULT_OPTION,
                    JOptionPane.WARNING_MESSAGE, null, options, options[0]);
        }

        /**
         * Shows a dialog of requested type
         *
         * @param title title of message
         * @param msg   message of dialog
         * @param typ   dialog type
         */
        private void showMessage(String title, String msg, int typ) {
            if (title.equals(Res.getString("LoaderAgent.title.success"))) {
                log.debug("showMessage:" + msg);
                JButton button = new JButton("Close");
                button.setPreferredSize(new Dimension(80, 30));
                initAlert("<html>Job " + jobId + " successfully finished<br><br></html>");

//                if (!jobsTableInfo.isDisplayed()) {
////                    final AppHelper appHelper = jobsTableInfo.getAppHelper();
//                    EventQueue.invokeLater(new Runnable() {
//                        public void run() {
//                            //app.updateDetailFromTreeSelection();
//                            app.updateTree();
//                        }
//                    });
//                }
            } else {
                log.debug("showMessage:title = " + title);
                JOptionPane oPane = new JOptionPane(formatErrorMsg(msg), typ);

                JDialog dialog = oPane.createDialog(frame, title);
                dialog.setVisible(true);
                dialog.toFront();
                dialog.requestFocusInWindow();
//                if (frame != null) frame.dispose();
            }
            refreshTable();
        }

        public String formatErrorMsg(String s) {
            StringBuilder sb = new StringBuilder();
            String[] lines = s.split("\n");
            for (String line : lines) {
                String[] words = line.split(" ");
                int i = 0;
                int rowLength = 0;
                while (i < words.length) {
                    if (rowLength > 50) {
                        sb.append("\n");
                        rowLength = 0;
                    }
                    rowLength = rowLength + words[i].length() + 1;
                    sb.append(words[i]).append(" ");
                    i++;
                }
                sb.append("\n");
            }
            return sb.toString();
        }

        /**
         * Restarts timer with this timeout
         *
         * @param timeout timeout in millis
         */
        public void resetTimeout(int timeout) {

            try {
                timer.cancel();
                timer = new java.util.Timer();
                timer.schedule(new Timeout(jobId), timeout);
            } catch (Exception ignored) {
                log.error(ignored);
            }
        }

        /**
         * Confirms threads close to database, shows Success or Errormessage
         * depending on info and disposes frame. Is called from database ENDE or
         * ERROR request or on timeout locally.
         *
         * @param info ENDE or ERROR
         */
        private synchronized void closeThread(String info) {
            log.debug("map:" + map);
            writeFinalMessage(0, "Close Thread " + jobId);
            refreshTable();
            if (info.equals("ENDE")) {
                showMessage(Res.getString("LoaderAgent.title.success"), Res.getString("LoaderAgent.text.success")
                        + jobId, JOptionPane.INFORMATION_MESSAGE);
            } else {

                String message = data.get("MSG").toString();
                String code = data.get("CODE").toString();
                // MCD, 09.Mar 2006, show correct message
                showMessage(Res.getString("LoaderAgent.text.error") + jobId, "Backenderror: " + message,
                        JOptionPane.ERROR_MESSAGE);

            }
            //frame.dispose();
//            jobCounter--;
            log.info("Closed Thread" + logInfo);

        }

        /**
         * @param nodes      Nodearray to order
         * @param attribute  attributes name to use for order
         * @param xmlHandler our XmlHandler
         * @return a treemap with attributes value as key - so its ordered
         */
        private TreeMap<String, Node> orderNodesByAttribute(NodeList nodes, String attribute, XmlHandler xmlHandler) {

            TreeMap<String, Node> nodeMap = new TreeMap<String, Node>();
            for (int i = 0; i < nodes.getLength(); i++) {
                Node node = nodes.item(i);
                String orderNum = xmlHandler.attributeRead(attribute, node);
                nodeMap.put(orderNum, node);
            }
            return nodeMap;
        }

    }

    /**
     * Thread that listens on requests from ISR clients. Request can mean
     * registration, deregistration and kill request.
     *
     * @author rainer bruns Created 15.12.2004
     */
    private class Controler extends Thread {

        ServerSocket control;
        int clients = 1;
        int controlPort;

        /**
         * Creates the thread
         *
         * @param controlPort
         */
        public Controler(int controlPort) {

            this.controlPort = controlPort;

        }

        /*
         * (non-Javadoc)
         * 
         * @see java.lang.Runnable#run() Start listening on socket
         */
        public void run() {

            try {

                control = new ServerSocket(controlPort);

            } catch (IOException ie) {

                ie.printStackTrace();
            }

            listen();

        }

        /**
         * listens on server socket for requests. If request is 2, stop
         * LoaderAgent immediate. if request is 1 register client. If clients
         * was 0 before, cancel shutdown( agent was marked for shutdown). If
         * request is 0 deregister client. If clients is 0 then tell LoaderAgent
         * about.
         */
        public void listen() {

            while (true) {
                try {
                    Socket client = control.accept();
//                    InputStream in = client.getInputStream();
//                    int request = in.read();
                    ObjectInputStream ois = new ObjectInputStream(client.getInputStream());
                    Hashtable data = (Hashtable) ois.readObject();

                    String head = data.get("HEAD").toString();
                    String info = data.get("INFO").toString();

                    int request = Integer.parseInt(info);

                    log.info("Request from " + client.getRemoteSocketAddress() + " " + request);

                    if (head.equals("NO_JOBID")) {
                        if (request == 100) {
                            log.info("check if port is available for the controller (Port " + client.getLocalPort() + ")");
                        }
                        if (request == 101) {
                            log.info("check if the loader is started (Port " + client.getLocalPort() + ")");
                        }
                        if (request == 2) {
                            log.info("Shutdown forced by " + client.getRemoteSocketAddress());
                            shutdownImmediate();
                        }
                        if (request == 1) {
                            if (clients == 0) {
                                cancelShutdown();
                            }
                            clients++;
                            log.info("Client registered from " + client.getRemoteSocketAddress());
                        } else if (clients <= 1 && request == 0) {
                            log.info("Last Client removed from " + client.getRemoteSocketAddress());
                            if (clients == 1) {
                                clients--;
                            }
                            mayBeExit();
                        } else if (request == 0) {
                            log.info("Client removed from " + client.getRemoteSocketAddress() + " " + clients);
                            clients--;
                        }
                    }
                } catch (IOException ioe) {
                    log.error("Error on Client request " + ioe.getLocalizedMessage());
                } catch (ClassNotFoundException e) {
                    log.error(e);
                }
            }
        }
    }

    public static void stopLoader(int controlPort) {
        try {
            Socket s = new Socket("127.0.0.1", controlPort);

            Hashtable<String, Object> data = new Hashtable<String, Object>();
            data.put("HEAD", "NO_JOBID");
            data.put("INFO", "0");

            ObjectOutputStream ostream = new ObjectOutputStream(s.getOutputStream());
            ostream.writeObject(data);
            ostream.flush();
            ostream.close();

            s.close();

        } catch (IOException ie) {
            log.error(ie.getMessage());
        }
    }

    public static void main(String args[]) {
        //4333 4334 iStudyReporter\\loader 30 log4j_loader.properties T
        //STOP 4334
        switch (args[0]) {
            case "STOP":
                stopLoader(Integer.parseInt(args[1]));
                break;
            default:
                new LoaderAgent(args, null);
                break;
        }
    }

}
