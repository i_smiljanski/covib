package com.utd.stb.back.data.client;

import java.util.ArrayList;
import java.util.Iterator;


/**
 * Wraps a list of record data of type EntityRecord
 *
 * @author rainer bruns Created 27.08.2004
 * @see EntityRecord
 */
public class EntityList {

    private ArrayList<EntityRecord> data = new ArrayList<>();
    private Iterator iterator;

    /**
     * creates an empty EntityList
     */
    public EntityList() {

    }

    /**
     * adds all fields of input list to this list
     *
     * @param in an EntityList to add
     */
    public void addAll(EntityList in) {

        in.initIterator();
        while (in.hasNext()) {
            data.add(in.getNext());
        }
    }

    /**
     * adds the reord to this list
     *
     * @param sr Record to add
     */
    public void add(EntityRecord sr) {

        data.add(sr);
    }

    /**
     * @param index
     * @return the Record at index
     */
    public EntityRecord get(int index) {

        return  data.get(index);
    }

    /**
     * sets record at given index to input record
     *
     * @param index
     * @param sr    record to set
     */
    public void set(int index, EntityRecord sr) {

        data.set(index, sr);
    }

    /**
     * sets list iterator to the beginning
     */
    public void initIterator() {

        iterator = data.iterator();
    }

    /**
     * @return true if there is a next record
     */
    public boolean hasNext() {

        return iterator.hasNext();
    }

    /**
     * @return the next EntityRecord
     */
    public EntityRecord getNext() {

        return (EntityRecord) iterator.next();
    }

    /**
     * @return the size of this list
     */
    public int getSize() {

        return data.size();
    }

    /**
     * @param s Record to look up
     * @return the index of this record
     */
    public int indexOf(EntityRecord s) {

        return data.indexOf(s);
    }

}