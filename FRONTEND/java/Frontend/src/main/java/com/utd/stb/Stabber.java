package com.utd.stb;

import com.uptodata.isr.gui.lookAndFeel.TweakUI;
import com.uptodata.isr.server.utils.fileSystem.FileSystemWorker;
import com.uptodata.isr.server.utils.logging.log4j2.LogHelper;
import com.uptodata.isr.utils.ConvertUtils;
import com.utd.stb.back.database.Dispatcher;
import com.utd.stb.gui.swingApp.exceptions.ExceptionTools;
import com.utd.stb.gui.swingApp.login.UserLogin;
import com.utd.stb.inter.application.Base;
import org.apache.commons.lang.StringUtils;
import org.apache.logging.log4j.Level;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.apache.logging.log4j.core.LoggerContext;

import javax.swing.*;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.PrintStream;
import java.net.URISyntaxException;
import java.net.URL;
import java.time.ZonedDateTime;
import java.time.format.DateTimeFormatter;
import java.util.Map;
import java.util.Properties;


/**
 * iStudyReporter 2.1 Starter class <br>
 * <p>
 * Create a new ThreadGroup and start from there, so we get informed about
 * uncaughtException(), handle it and =&gt;exit application
 * <p>
 * <p>
 * <b>Note: </b> <br>
 * This has to be the first thing that accesses the AWT/Swing parts, since this
 * first access (may? at least window/component creation) creates the
 * AWT-Event-queue Thread and we need this to live in our ThreadGroup to handle
 * uncaught exceptions.
 *
 * @author PeterBuettner.de
 */
public class Stabber {
    public static final Logger log = LogManager.getRootLogger();

    private static String servletUri;
    public static boolean isRibbons = true;

    //    private static String javaLogLevel;
    private static String frontendPropertiesFileName = "frontend.properties";

    public Stabber() {
        init("ERROR");
    }

    public Stabber(String argLogLevel) {
        init(argLogLevel);
    }

    private void init(String argLogLevel) {
//        initErrorStream();
        //Read the configuration file with the period to wait between each check
//        PropertyConfigurator.configureAndWatch("log4j.properties", 60 * 1000);

        if (argLogLevel == null) {
            argLogLevel = getLogLevelFromProperties();
        }
        initLogger(argLogLevel);
        log.info("applLogLevel = " + argLogLevel);

        ThreadGroup threadGroup = new ExceptionGroup();
        Thread guiThread = new Thread(() -> {

            // the event Thread, this thread is now created in our Thread Group
            SwingUtilities.invokeLater(Stabber::startGUI);
        });
        if (log.getLevel().intLevel() >= Level.DEBUG.intLevel()) {
            guiThread.setUncaughtExceptionHandler(threadGroup);
        }
        guiThread.start();
    }

    public static void initLogger(String logLevel) {
        LoggerContext context = (LoggerContext) LogManager.getContext(false);
        URL url = Stabber.class.getClassLoader().getResource("log4j2_frontend.xml");
        try {
            context.setConfigLocation(url.toURI());
        } catch (URISyntaxException e) {
            e.printStackTrace();
        }
        LogHelper.changeLoggerLevel(logLevel, LogManager.ROOT_LOGGER_NAME);
    }


    public static void main(String[] args) {

        com.jidesoft.utils.Lm.verifyLicense("up to data professional services", "iStudyReporter", "w6.tuJhnfnDInGp:fvqIJaaeKT4yKF42");

        Map<String, String> argsMap;
        String argLogLevel = null;
        try {
            argsMap = ConvertUtils.convertArrayToMap(args);
//            System.out.println("argsMap = " + argsMap);
            argLogLevel = argsMap.get("loglevel");
            String ribbons = argsMap.get("isRibbons");
            servletUri = argsMap.get("servlet_uri");
            if (StringUtils.isNotEmpty(ribbons)) {
                isRibbons = Boolean.parseBoolean(ribbons);
            }
//            System.out.println("isRibbons = " + isRibbons);
//            System.out.println("System.getProperties() = " + System.getProperties());
        } catch (Exception e) {
            e.printStackTrace();
        }

//        if (StringUtils.isNotEmpty(argLogLevel)) {
//            javaLogLevel = argLogLevel;
//        }

        new Stabber(argLogLevel);

    }

    /**
     * Create the gui, call this in the event Thread only!
     */
    private static void startGUI() {
        TweakUI.setLookAndFeel();

        makeBaseDir();
        UserLogin userLogin = new UserLogin(servletUri, false) {

            protected Base createBase(String userName,
                                      String userPassword,
                                      String dbUser,
                                      String dbPassword,
                                      String server,
                                      String serverAlias,
                                      String newPassword,
                                      String sDateTimeMainXml) throws Exception {
                Dispatcher dispatcher = new Dispatcher(userName, userPassword, dbUser, dbPassword, server, serverAlias, newPassword, sDateTimeMainXml);
                return dispatcher.getBase();
            }
        };
        log.debug(userLogin);
    }


    /**
     * If the base directory does not exists try to create it. Base directory is
     * java.io.tmpdir\iStudyReporter\loader-> on windows(german) usually
     * C:\Dokumente und Einstellungen\$user\Lokale
     * Einstellungen\temp\iStudyreporter
     *
     * @return true if directory exists or succesfully created
     */
    private static boolean makeBaseDir() {

        String base = "iStudyReporter";
        File dir = new File(System.getProperty("java.io.tmpdir"), base);
        //if there is a file with this name we can't do this
        if (dir.exists() && dir.isFile()) {

            return false;
        }
        //directory exists, ok
        else if (dir.exists() && dir.isDirectory())

            return true;

        //try to create
        return dir.mkdirs();

    }

    private static void initErrorStream() {
        String base = "iStudyReporter";
        String pattern = "dd-MM-yy_HH_mm";
        DateTimeFormatter dateFormatter = DateTimeFormatter.ofPattern(pattern);
        ZonedDateTime nowData = ZonedDateTime.now();
        String fileName = nowData.format(dateFormatter);
        File dir = new File(System.getProperty("java.io.tmpdir"), base);
        File errorFile = new File(dir, "isrErrors_" + fileName + ".log");
        try {
            System.setErr(new PrintStream(errorFile));
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        }
    }

    public static String getLogLevelFromProperties() {
        Properties frontendProperties = null;
        String logLevel = "ERROR";
        try {
            frontendProperties = FileSystemWorker.loadProperties(frontendPropertiesFileName);
        } catch (Exception e) {
            System.out.println("Java log level not known, it set to 'ERROR' because of " + e);
        }
        if (frontendProperties != null) {
            logLevel = frontendProperties.getProperty("javaLogLevel");
        }
        return logLevel;
    }


    /**
     * Purpose: Handle uncaught exceptions, exit Application
     */
    private static class ExceptionGroup extends ThreadGroup {

        private ExceptionGroup() {
            super("Stabber-Application");
            log.debug("ExceptionGroup");
        }

        public void uncaughtException(Thread t, Throwable e) {
            log.error("begin " + t + "; " + e);
            e.printStackTrace();
//            if (e instanceof ThreadDeath) return; // see
            // super.uncaughtException()
            ExceptionTools.callExceptionHandler(null, e, false);// never
        }
    }
}

