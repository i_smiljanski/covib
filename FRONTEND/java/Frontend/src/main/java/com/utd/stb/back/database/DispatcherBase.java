package com.utd.stb.back.database;

import com.utd.stb.back.data.client.*;
import com.utd.stb.back.ui.impl.userinputs.data.ReportOptions;
import com.utd.stb.back.ui.impl.wizard.StandardReportWizard;
import com.utd.stb.back.ui.impl.wizard.StandardWizardErrorException;
import com.utd.stb.back.ui.impl.wizard.StandardWizardSteps;
import com.utd.stb.back.ui.impl.wizard.formdata.Grid;
import com.utd.stb.back.ui.impl.wizard.formdata.MultiSelect;
import com.utd.stb.back.ui.impl.wizard.formdata.Picklist;
import com.utd.stb.back.ui.impl.wizard.formdata.SingleSelection;
import com.utd.stb.back.ui.inter.ExtendedWizardStep;
import com.utd.stb.back.ui.inter.WizardFormData;
import com.utd.stb.gui.swingApp.UserDialogTool;
import com.utd.stb.gui.swingApp.exceptions.ExceptionDialog;
import com.utd.stb.gui.swingApp.login.UserLoginException;
import com.utd.stb.inter.application.BackendException;
import com.utd.stb.inter.dialog.UserDialog;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import javax.swing.*;
import java.sql.SQLException;
import java.util.ArrayList;

/**
 * Holds basic server side methods used in data flow from DBInterface to GUI
 *
 * @author rainer bruns
 * @see DBInterface
 */
class DispatcherBase {

    public static final Logger log = LogManager.getRootLogger();

    protected DBInterface db;
    protected StabberConnection stabberConn;
    protected int language;
    protected StandardReportWizard stdWizard = null;
    private Dispatcher parent;

    /**
     * Comment for <code>SINGLESELECT</code> Formtype single selection
     */
    static int SINGLESELECT = 1;

    /**
     * Comment for <code>PICKLIST</code> Formtype Picklist
     */
    static int PICKLIST = 2;

    /**
     * Comment for <code>GRID</code> Formtype Grid
     */
    static int GRID = 4;

    /**
     * Comment for <code>MULTISELECT</code> Formtype MultiSelect
     */
    static int MULTISELECT = 3;

    /**
     * Comment for <code>OPTIONS</code> Formtype for Options
     */
    static int OPTIONS = 5;

    /**
     * Object for Forminformation
     */
    protected FormInfoRecord formInfo;

    /**
     * Constructor of DispatcherBase
     */
    protected DispatcherBase() {

    }

    /**
     * Creates a StabberConnection Object with given connection info
     *
     * @param user
     * @param password
     * @param server
     * @param serverAlias
     * @param d
     * @see StabberConnection
     */
    protected void connect(String user,
                           String password,
                           String server,
                           String serverAlias,
                           String newPassword,
                           Dispatcher d) {

        this.parent = d;
        stabberConn = new StabberConnection(user, password, server, serverAlias, newPassword);
    }

    /**
     * Creates the DBInterface
     *
     * @throws SQLException
     * @throws UserLoginException
     * @see DBInterface
     */
    protected void createInterface() throws Exception {

        db = new DBInterface(stabberConn.connect());
        db.checkIsValidOwner();
    }

    /**
     * Set user language
     *
     * @throws BackendException
     */
    protected void setLanguage() throws BackendException {

        language = db.getLanguage();
    }

    /**
     * Sets the port number
     *
     * @param basisPort: port to set
     * @throws BackendException
     */
    protected void setClientPort(int basisPort) throws BackendException {

        db.setClientPort(basisPort);

    }

    /**
     * @param treeNode node for which to get childs
     * @return a list of childs for the node
     * @throws BackendException
     */
    protected TreeNodeList getNodeChilds(TreeNodeRecord treeNode) throws BackendException {

        return db.getNodeChilds(treeNode, true);
    }

    /**
     * @param treeNode node for which to get details
     * @return a list of details for the node
     * @throws BackendException
     */
    protected TreeNodeList getNodeList(TreeNodeRecord treeNode) throws BackendException {

        return db.getNodeChilds(treeNode, false);
    }

    /**
     * @return the list of output options for current report type
     * @throws BackendException
     */
    protected PropertyList getOutputOption() throws BackendException {

        return db.getPropertyList("OPTION");
    }

    /**
     * @return list of fields in save dialog in report wizard
     * @throws BackendException
     */
    protected TreeNodeList getSaveDialog() throws BackendException {

        return db.getTreeNodeList("SAVEDIALOG");
    }

    /**
     * @return list of fields in audit dialog
     * @throws BackendException
     */
    protected TreeNodeList getAuditDialog() throws BackendException {

        return db.getTreeNodeList("AUDITDIALOG");
    }

    /**
     * called from callAction when Backend throws Error with message exFirstForm
     * Initializes The StandardWizardSteps, StandardReportWizard and the frame
     * for Wizard
     *
     * @param i only present if later jump from menu to wizard might be enabled
     * @throws BackendException
     * @see StandardWizardSteps
     * @see StandardReportWizard
     */
    protected Object showWizardDialog(int i, TreeNodeRecord selectedNode) throws BackendException {

        String nodeDisplay = "Report";
        if (selectedNode != null) {
            nodeDisplay = selectedNode.getDisplay();
        }
        /*
         * initialize all steps for current report type
         */
        StandardWizardSteps steps = new StandardWizardSteps(parent);

        /*
         * Initialize the Wizard Object
         */
        stdWizard = new StandardReportWizard(parent, steps, i, nodeDisplay);

        //new ReportWizardFrameGui(stdWizard, stabber.getFrame());
        //wizardGUI = new ReportWizardFrameGui(stdWizard);
        return stdWizard;
    }

    /**
     * on wizard error.
     */
    public void showWizardError(StandardWizardErrorException e) {
        log.info("begin " + e.getSeverity());
        UserDialog dialog = ExceptionDialog.createWarningMsgBox("Warning", e);
        UserDialogTool.executeUserDialog((JFrame) null, dialog);
        log.info("end");
    }

    /**
     * checks the Formtype, gets the initialation data for the form, and
     * initialse the corresponding FormData Object
     *
     * @return ExtendedWizardStep, the step corresponding to the Form, filled
     * with data
     * @throws SQLException
     * @throws BackendException
     * @see SingleSelection
     * @see Picklist
     * @see MultiSelect
     * @see Grid
     * @see ReportOptions
     */
    public ExtendedWizardStep displayForm(boolean navigate, int nextNavigationMask)
            throws BackendException {
        log.info("begin " + nextNavigationMask);
        try {
            // (nextNavigationMask + 1) for synchronisation of front- and backend,
            // because backend counts from 1, frontend from 0
            db.getList(null, null, true, (nextNavigationMask + 1), false);
        } catch (StandardWizardErrorException swe) {
            showWizardError(swe);
            log.debug("formInfo = " + formInfo);
            return null;
        }
        log.debug(navigate);
        return createForm(navigate);
    }


    protected ExtendedWizardStep createForm(boolean navigate) throws BackendException {

        formInfo = db.getCurrentFormInfoRecord();
        log.debug("formInfo = " + formInfo);
        ArrayList entities = db.getCurrentEntityList();
        SelectionList data = db.getCurrentSelectionList();

        WizardFormData formData = null;

        // Initialize the correct FormData Object for Formtype
        if (formInfo.getType() == SINGLESELECT) {

            formData = new SingleSelection(parent, data, formInfo);

        } else if (formInfo.getType() == PICKLIST) {

            formData = new Picklist(parent, data, formInfo);

        } else if (formInfo.getType() == GRID) {

            formData = new Grid(parent, data, entities, formInfo, navigate);

        } else if (formInfo.getType() == MULTISELECT) {

            if (((EntityRecord) entities.get(0)).isSelected()) {
                formData = new MultiSelect(parent, data, ((EntityRecord) entities.get(1)).getEntity(), formInfo, navigate);
            } else {
                formData = new MultiSelect(parent, data, ((EntityRecord) entities.get(0)).getEntity(), formInfo, navigate);
            }

        } else if (formInfo.getType() == OPTIONS) {

            // parent is special, a list of fields with properties
            formData = new ReportOptions(parent, getOutputOption());

        }

        // get the appropriate Wizard Step for current form Object
        ExtendedWizardStep wizardStep = (ExtendedWizardStep) stdWizard.getWizardSteps().get(formInfo.getFormNumber() == 0 ? 0 : formInfo.getFormNumber() - 1);
        log.debug(wizardStep.getCurrentIndex());
        // set data in step
        wizardStep.setFormData(formData);
        // set display info in step
        wizardStep.setDisplayInfo(formInfo.getFormDescription(), formInfo.getTitle());
        log.debug("end");
        // return wizardStep
        return wizardStep;
    }
}
