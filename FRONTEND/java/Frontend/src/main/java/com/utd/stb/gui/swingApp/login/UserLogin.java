package com.utd.stb.gui.swingApp.login;

import com.jgoodies.forms.builder.DefaultFormBuilder;
import com.jgoodies.forms.layout.CellConstraints;
import com.jgoodies.forms.layout.FormLayout;
import com.uptodata.isr.gui.extern.sun.swingworker.SwingWorker;
import com.uptodata.isr.gui.lookAndFeel.TweakUI;
import com.uptodata.isr.gui.util.Colors;
import com.uptodata.isr.gui.util.HtmlText;
import com.uptodata.isr.utils.ConvertUtils;
import com.uptodata.isr.utils.security.PasswordUtil;
import com.uptodata.isr.utils.security.SymmetricProtection;
import com.uptodata.isr.utils.xml.dom.XmlHandler;
import com.utd.gui.action.ActionUtil;
import com.utd.gui.action.ActionWrap;
import com.utd.gui.event.DocumentChangeAdapter;
import com.utd.gui.util.DialogUtils;
import com.utd.stb.gui.swingApp.AboutBox;
import com.utd.stb.gui.swingApp.App;
import com.utd.stb.gui.swingApp.exceptions.ExceptionDialog;
import com.utd.stb.gui.swingApp.exceptions.MessageStack;
import com.utd.stb.gui.userinput.swing.DialogFactory;
import com.utd.stb.inter.action.ButtonItem;
import com.utd.stb.inter.application.BackendException;
import com.utd.stb.inter.application.Base;
import com.utd.stb.inter.dialog.UserDialog;
import com.utd.stb.interlib.BasicMessageBox;
import com.utd.stb.interlib.StandardButton;
import org.apache.commons.lang.StringUtils;
import org.apache.logging.log4j.Level;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.w3c.dom.Node;
import resources.IconSource;
import resources.Res;

import javax.jnlp.BasicService;
import javax.jnlp.ServiceManager;
import javax.swing.*;
import javax.swing.event.DocumentEvent;
import javax.swing.text.Document;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ItemEvent;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;
import java.io.*;
import java.net.URL;
import java.util.Properties;

/**
 * User can type user/password, may choose from a combobox a server, while
 * connecting progress is shown, connecting: means create a dispatcher, when
 * this is done create the rest of the gui and when the main window is ready we
 * hide us and show the main window. <br>
 * if any error occures while connecting or creating the gui we either sow a
 * little note (user/password/server wrong/timeout), on any other error the user
 * gets the exception presented and we exit here.
 * <p>
 * <br>
 * The list of databases are read from some properties, a file "main.properties"
 * in the working dir for the moment <br>
 * <p>
 * usage: see main()
 * <p>
 * <p>
 * TODOlater remove thos hacks with frame/error/dialog: create the frame, try to
 * read the DBdef, if Ok fill frame with working controls else fill the frame
 * with error message (header/text/buttonbar: all standards)
 *
 * @author PeterBuettner.de
 */
public abstract class UserLogin {

    //    public static final Logger log = LogManager.getRootLogger();
    public static final Logger log = LogManager.getRootLogger();
    private static boolean directStart;

    /**
     * Return the complete constructed Base, throw an exception if anything goes
     * wrong.
     *
     * @param userName
     * @param server   plain name, what was defined as the value of an alias,
     *                 maybe null: a default should be used
     * @return
     */
    protected abstract Base createBase(String userName, String userPassword, String dbUser, String dbPassword,
                                       String server, String serverAlias,
                                       String newPassword, String sDateTimeMainXml) throws Exception;

    private static final String DATABASE_PROPFILE = "main.xml";
    private static final String ADD_TEXT = "add";

    static final String MAIN_NODE_ALIAS = "ALIAS";
    static final String MAIN_NODE_FULLNAME = "FULLNAME";
    static final String MAIN_NODE_DBUSER = "DBUSER";
    static final String MAIN_NODE_DBPASSWORD = "DBPASSWORD";

    private XmlHandler xmlDbHandler = null;
    private boolean isWebstart;

    private JFrame frame;
    private App app;

    // ... controls .............................
    private Action acStart;
    private Action acCancel;

    private JTextField tfUser;
    private JPasswordField tfPassword;
    private JComboBox cbDatabase;

    // for adding entries
    private JTextField tfConnectString;
    private JTextField tfConnectAlias;
    private JCheckBox cbLDAP;
    private JTextField tfDBUser;
    private JPasswordField tfDBPassword;

    private JButton bStart;
    private JButton bCancel;

    private JProgressBar progress;
    private JLabel lblInfo;

    // have some fields so we may change some components in the content

    private Component hadFocusOnStart;

    private boolean showDBFields;
    private boolean showLDAPFields;
    private boolean addNewItem;


    /**
     * @param exit then System.exit();
     */
    private void closeLoginFrame(boolean exit) {

        if (exit) {
            System.exit(0);
        } else {
            frame.dispose();
        }
    }

    protected UserLogin(String servletUri, boolean useDirectStart) {
        directStart = useDirectStart;
//        GuiLogger.info("Start Creating UserLogin");
        log.info("Start Creating UserLogin");
//        SVNInfo.saveSVNInfo();
        // message here
        File mainXmlFile = null;

        try {
            BasicService basicService = null;
            try {
                basicService = (BasicService) ServiceManager.lookup("javax.jnlp.BasicService");
            } catch (Exception e1) {
                log.info(e1);
            }
            if (basicService != null && basicService.isWebBrowserSupported()) {
                URL codebase = basicService.getCodeBase();
                URL mainXmlUrl = new URL(codebase.toString() + servletUri.replaceFirst("/", "") + "?file=main.xml");
                log.debug("mainXmlUrl:" + mainXmlUrl.toString());
                InputStream in = mainXmlUrl.openStream();
                xmlDbHandler = new XmlHandler(in);
                log.debug(xmlDbHandler.xmlToString());
                isWebstart = true;
            } else {
                mainXmlFile = new File(DATABASE_PROPFILE).getCanonicalFile();
                xmlDbHandler = new XmlHandler(new FileInputStream(mainXmlFile));
                log.debug("main.xml: " + mainXmlFile);
            }

        } catch (IOException e) {
            log.error(e);
        }

        DatabaseInfo[] databases = DatabaseInfo.readDataBases(xmlDbHandler);
        String errorString = checkDataBases(databases);
        if (!errorString.isEmpty()) {
            showErrorNoDeafultDatabaseDefinition(errorString);
            log.error(errorString);
            System.exit(-1);
        }
        // ... now build frame
        IconSource iconSource = new IconSource();
        frame = new JFrame();
        TweakUI.setFrameDecoration(frame);
        frame.setTitle(getString("window.title"));
        frame.setResizable(false);
        frame.setDefaultCloseOperation(JFrame.DO_NOTHING_ON_CLOSE);
        frame.addWindowListener(new WindowAdapter() {
            public void windowClosing(WindowEvent e) {
                closeLoginFrame(true);
            }
        });
        frame.setIconImage(iconSource.getSmallImageByID("frameLogin"));

        createActions();
        createControls();

        if (databases != null && databases.length > 0) {
            cbDatabase.setModel(new DefaultComboBoxModel(databases));
        }

        // ............................
        Icon headerIcon = iconSource.getHeaderIconByID("login");
        log.debug("headerIcon=" + headerIcon);
        Properties props = new AboutBox().loadReleaseInfoProperties();

        String subText = String.format(getString("headerbar.subText"), props.getProperty("svn.version"), props.getProperty("builddate"));

        if (log.getLevel().intLevel() >= Level.DEBUG.intLevel()) {
            subText += String.format(getString("headerbar.subTextBuild"), props.getProperty("svn.version"), props.getProperty("builddate"));
        }
        /*if (log.getLevel().toInt() <= Level.DEBUG_INT) {
            subText += String.format(getString("headerbar.subTextBuild"), props.getProperty("svn.version"), props.getProperty("builddate"));
        }*/
        log.debug(subText);
        JPanel titlePanel = DialogUtils.createTitlePanel(getString("headerbar.title"), subText, headerIcon, 180);
        JComponent buttonPanel = DialogUtils.createButtonPanel(bStart, bCancel, null, BorderLayout.NORTH);

        /*
      content - where the fileds , progress, etc. lives
     */
        JPanel contentPanel = createContentPanel();

        // ............................
        Container c = frame.getContentPane();
        c.add(titlePanel, BorderLayout.NORTH);
        c.add(contentPanel);
        c.add(buttonPanel, BorderLayout.SOUTH);

        // Ok/Cancel Actions per Keyboard. default: start , Key-Escape: cancel
        frame.getRootPane().setDefaultButton(bStart);
        ActionUtil.putIntoActionMap(frame.getRootPane(), acCancel);
        // ............................

        acStart.setEnabled(fieldsAreValid());
        if (directStart) {
            acStart.actionPerformed(null);
        } else {
            frame.pack();
            frame.setLocationRelativeTo(null);// center on screen
            tfUser.requestFocus();

            frame.setVisible(true);

            // something's going wrong, the layout needs a second pass after show();
            // reason: the JTextPane in the title pane
            frame.pack();
        }

        //  show on top of the frame, so the application is visible in the taskbar
        if (databases == null || databases.length == 0) {
            log.warn("Error readin databases from:" + mainXmlFile);
            //showErrorNoDatabaseDefinition( frame, errorTextException, propertyFile );
        }

    }

    private String checkDataBases(DatabaseInfo[] databases) {
        DatabaseInfo firstDatabaseInfo = databases[0];
        if (firstDatabaseInfo.getAlias().equalsIgnoreCase(ADD_TEXT)) {
            return ""; // alles ok
        }
        if (!firstDatabaseInfo.isDefault()) {
            return getString("noDefaultDb.error.text");
        }

        if (databases.length > 1) {
            DatabaseInfo secondDatabaseInfo = databases[1];
            if (secondDatabaseInfo.isDefault()) {
                return getString("notUniqueDefaultDb.error.text");
            }
        }
        return "";
    }

    /**
     * shows a modal messagebox on top of parent
     *
     * @param parent
     * @param extraText if not null then put in an extra line below the resource
     *                  driven error text, if null we append the filename (since the filename is
     *                  mostly placed in the error extraText)
     * @param file      shown if extraText==null and file !=null
     */
    private void showErrorNoDatabaseDefinition(JFrame parent, String extraText, File file) {

        String text = getString("configread.error.text");
        if (text == null) {
            text = "";
        }
        if (extraText != null) {
            text += "\n\n" + extraText;
        } else if (file != null) {
            text += "\n\n" + file;
        }
        showErrorNoDeafultDatabaseDefinition(text);

    }

    private void showErrorNoDeafultDatabaseDefinition(String extraText) {
        ButtonItem bClose = new StandardButton(ButtonItem.CLOSE, false);

        UserDialog ud = new BasicMessageBox(getString("window.title"), BasicMessageBox.createHeaderBar(getString("configread.error.title"),
                BasicMessageBox.ICON_STOP), null, extraText, new ButtonItem[]{bClose}, bClose,
                true
        ) {
            protected Object onBoxClose(ButtonItem clickedButton) {
                return null;
            }
        };

        DialogFactory.modal = true;
        JDialog dlg = DialogFactory.createDialog((Frame) null, ud);
        dlg.pack();
        dlg.setLocationRelativeTo(frame);
        dlg.setVisible(true);
    }


    private void createActions() {
        acStart = new ActionWrap(Res.getActionMap("loginDialog.action.connect"), null) {
            public void actionPerformed(ActionEvent e) {
                onPressedStart();
            }
        };

        acCancel = new ActionWrap(Res.getActionMap("loginDialog.action.abort"), null) {

            public void actionPerformed(ActionEvent e) {

                closeLoginFrame(true);
            }
        };

    }

    private void createControls() {

        tfUser = new JTextField(15);

        tfPassword = new JPasswordField(15);
        tfPassword.setEchoChar('\u25cf');// full round bullet
        // looks also good with default, but with tahoma it is more elegant:
        tfPassword.setFont(Font.decode("Tahoma-" + tfPassword.getFont().getSize()));

        // for adding entries
        tfConnectString = new JTextField(20);
        tfConnectAlias = new JTextField(20);
        cbLDAP = new JCheckBox();
        tfDBUser = new JTextField(15);
        tfDBPassword = new JPasswordField(15);
        tfDBPassword.setEchoChar('\u25cf');// full round bullet
        // looks also good with default, but with tahoma it is more elegant:
        tfDBPassword.setFont(Font.decode("Tahoma-" + tfPassword.getFont().getSize()));

        DocumentChangeAdapter dca = new DocumentChangeAdapter() {

            public void documentChange(DocumentEvent e) {

                acStart.setEnabled(fieldsAreValid());
            }
        };
        tfUser.getDocument().addDocumentListener(dca);
        tfPassword.getDocument().addDocumentListener(dca);
        tfConnectString.getDocument().addDocumentListener(dca);
        tfConnectAlias.getDocument().addDocumentListener(dca);

        cbDatabase = new JComboBox() {
        };
        cbDatabase.setEditable(false);
        cbDatabase.setRenderer(new DatabaseRenderer());
        cbDatabase.addActionListener(e -> changePanel());

        cbLDAP.addItemListener(e -> {
            System.out.println("addLDAP");
            addLDAP(e.getStateChange() == ItemEvent.SELECTED);
        });

        bStart = new JButton(acStart);
        bCancel = new JButton(acCancel);

        progress = new JProgressBar(JProgressBar.HORIZONTAL) {

            public Dimension getPreferredSize() {// small width, or layout may
                // jumping

                Dimension d = super.getPreferredSize();
                if (d != null) {
                    d.width = 20;
                }
                return d;
            }
        };
        progress.setIndeterminate(true);
        progress.setVisible(false);
        lblInfo = new JLabel(" ");

    }

    private boolean fieldsAreValid() {

        Document doc = tfPassword.getDocument();
        if (doc == null || doc.getLength() == 0) {
            return false;
        }
        doc = tfUser.getDocument();
        if (doc == null || doc.getLength() == 0) {
            return false;
        }
        if (cbDatabase.getSelectedItem() == null) {
            doc = tfConnectString.getDocument();
            if (doc == null || doc.getLength() == 0) {
                return false;
            }
            doc = tfConnectAlias.getDocument();
            if (doc == null || doc.getLength() == 0) {
                return false;
            }
        }
        return true;

    }

    private void changePanel() {

        DatabaseInfo dbi = (DatabaseInfo) cbDatabase.getSelectedItem();
        String alias = dbi.getAlias().toLowerCase();
        if (alias.equals(ADD_TEXT)) {
            addNewItem = true;
            showDBFields = true;
            cbDatabase.setSelectedItem(null);

            frame.getContentPane().remove(frame.getContentPane().getComponent(1));
            frame.getContentPane().add(createContentPanel(), 1);
            acStart.setEnabled(fieldsAreValid());
            frame.pack();
            tfConnectString.requestFocus();
            frame.setVisible(true);

            showDBFields = false;
        }
    }

    private void addLDAP(boolean ldap) {

        showLDAPFields = ldap;
        if (ldap) {
            showDBFields = true;
        } else {
            tfDBUser.setText("");
            tfDBPassword.setText("");
        }

        frame.getContentPane().remove(frame.getContentPane().getComponent(1));
        frame.getContentPane().add(createContentPanel(), 1);
        acStart.setEnabled(fieldsAreValid());
        frame.pack();
        tfConnectString.requestFocus();
        frame.setVisible(true);
    }

    /**
     * needs controls created, and cbDatabase filled
     *
     * @return
     */
    private JPanel createContentPanel() {

        FormLayout layout = new FormLayout("L:D:G, 2dlu, L:P,2dlu,P,0dlu", "");
        /**
         * for the content panel
         */
        DefaultFormBuilder builder = DialogUtils.getFormBuilder(layout);

        appendRow(builder, "user", tfUser);
        appendRow(builder, "password", tfPassword);

        builder.appendUnrelatedComponentsGapRow();
        builder.nextLine();

        if (cbDatabase.getItemCount() > 0 && !showDBFields) {// only if not empty or desired, show combobox

            appendRow(builder, "database", cbDatabase);
            builder.appendUnrelatedComponentsGapRow();
            builder.nextLine();

        } else {

            //TitledBorder title;
            //title = BorderFactory.createTitledBorder("Parameters");
            appendRow(builder, "connectstring", tfConnectString);
            appendRow(builder, "connectalias", tfConnectAlias);
            appendRow(builder, "ldap", cbLDAP);

            if (showLDAPFields) {
                appendRow(builder, "dbuser", tfDBUser);
                appendRow(builder, "dbpassword", tfDBPassword);

            }

            builder.appendUnrelatedComponentsGapRow();
            builder.nextLine();
        }

        /*
         * progress and info containing line, the two compos overlap, but
         * doesn't matter since one is always invisible
         */
        builder.append(new JLabel(" "));// Hack to have the 'block' right
        // aligned
        /**
         * in the content pane
         */
        int infoRow = builder.getRow();
        builder.append(new JLabel(" "));
        int infoCol = builder.getColumn();
        builder.append(progress);
        builder.nextLine();
        builder.add(lblInfo, new CellConstraints().xyw(1, infoRow, builder.getColumnCount()));

        // ..........................
        builder.setDefaultDialogBorder();
        return builder.getPanel();
    }

    private void appendRow(DefaultFormBuilder builder, String resKey, JComponent component) {

        builder.append(new JLabel(" "));// Hack to have the 'block' right aligned
        builder.append(createAndBindLabel(resKey, component), component);
        builder.nextLine();

    }

    private JLabel createAndBindLabel(String resKey, JComponent component) {

        JLabel label = new JLabel();
        ActionUtil.initLabelFromMap(label, Res.getActionMap("loginDialog.label." + resKey));
        label.setLabelFor(component);
        return label;

    }// ----------------------------------------------------------------

    private static class DatabaseRenderer extends DefaultListCellRenderer {

        public Component getListCellRendererComponent(JList list, Object value, int index, boolean isSelected,
                                                      boolean cellHasFocus) {

            if (value instanceof DatabaseInfo) {
                DatabaseInfo dbi = (DatabaseInfo) value;
                setToolTipText(dbi.getFullName());
                return super.getListCellRendererComponent(list, dbi.getAlias(), index, isSelected, cellHasFocus);
            }
            return super.getListCellRendererComponent(list, value, index, isSelected, cellHasFocus);
        }
    }// ----------------------------------------------------------------

    /**
     * read from the resource bundle, it is prefixed with a dialog key
     */
    private String getString(String key) {

        return Res.getString("loginDialog." + key);
    }

    //----------------------------------------------------------------
    private void onPressedStart() {

        final SwingWorker worker = createWorker();
        hadFocusOnStart = KeyboardFocusManager.getCurrentKeyboardFocusManager().getPermanentFocusOwner();
        SwingUtilities.invokeLater(() -> bStart.requestFocus());

        progress.setIndeterminate(false);// start always from same 'point'
        progress.setIndeterminate(true);
        setStateConnecting(false);
        worker.start();
    }

    /**
     * set not hard, e.g. start-button state depends on fieldsAreValid()
     *
     * @param done
     */
    private void setStateConnecting(boolean done) {

        acStart.setEnabled(done && fieldsAreValid());
        acCancel.setEnabled(done);

        tfUser.setEnabled(done);
        tfPassword.setEnabled(done);
        cbDatabase.setEnabled(done);

        progress.setVisible(!done);
        lblInfo.setVisible(done);

        frame.invalidate();

    }// ------------------------------------------------

    private SwingWorker createWorker() {

        final String user = tfUser.getText();
        final String pass = new String(tfPassword.getPassword());

        DatabaseInfo dbi = (DatabaseInfo) cbDatabase.getSelectedItem();
        final String server = dbi == null ? tfConnectString.getText() : dbi.getFullName();
        final String serverAlias = dbi == null ? tfConnectAlias.getText() : dbi.getAlias();
        final String dbUser = dbi == null ? tfDBUser.getText() : dbi.getdbUser();
        String decrPwd = "";
        if (dbi != null) {
            String encPwd = dbi.getdbPassword();
            try {
                decrPwd = SymmetricProtection.decrypt256(encPwd);
            } catch (Exception e) {
                log.info(e);
            }
            if (StringUtils.isEmpty(decrPwd)) {
                try {
                    decrPwd = SymmetricProtection.decrypt(encPwd);
                } catch (Exception e1) {
//                    e1.printStackTrace();
                    log.error(e1);
                }
            }
            if (StringUtils.isEmpty(decrPwd)) {
                try {
                    decrPwd = SymmetricProtection.decryptBase64(encPwd);
                } catch (Exception e1) {
//                    e1.printStackTrace();
                    log.error(e1);
                }
            }
        }


        final String dbPassword = dbi == null ? new String(tfDBPassword.getPassword()) : decrPwd;
        String sDateTimeMainXml = "";
        log.debug("serverAlias = " + serverAlias + "; server = " + server + "; dbUser = " + dbUser + "; dbPassword = "
                + "" + dbPassword + "; ");
        log.debug("directStart: " + directStart + " isWebstart: " + isWebstart);
        try {
            File mainXml = new File(DATABASE_PROPFILE).getCanonicalFile();
            log.debug(mainXml.exists());

            Node timestamp = xmlDbHandler.getNode("//TIMESTAMP");
            if (timestamp != null) {
                sDateTimeMainXml = timestamp.getTextContent();
            }

            if (!isWebstart && !directStart && (!mainXml.exists() || addNewItem)) {
                // create property file if not available

                try {
                    Node items = xmlDbHandler.getNode("//ITEMS");
                } catch (NullPointerException npe) {
                    xmlDbHandler = new XmlHandler();
                    xmlDbHandler.nodeRoot("ITEMS");
                }


                Node item = xmlDbHandler.getNode("//ITEM/" + MAIN_NODE_ALIAS + "[text()=\"" + serverAlias + "\"]/..");

                if (item != null) {
                    xmlDbHandler.removeChildNodes(item);
                } else {
                    item = xmlDbHandler.createElementBefore("ITEM", null, xmlDbHandler.getNode("//ITEMS"),
                            xmlDbHandler.getNode("//ITEMS/ITEM[1]"));
                }

                xmlDbHandler.createElement(MAIN_NODE_ALIAS, serverAlias, null, item);
                if (dbUser != null && dbPassword != null) {
                    xmlDbHandler.createElement(MAIN_NODE_FULLNAME, server, null, item);
                    if (!(dbUser.isEmpty() && dbPassword.isEmpty())) {
                        xmlDbHandler.createElement(MAIN_NODE_DBUSER, dbUser, null, item);
                        xmlDbHandler.createElement(MAIN_NODE_DBPASSWORD, SymmetricProtection.encrypt256(dbPassword), null, item);
                    }
                }

                // store properties
                xmlDbHandler.save(new FileOutputStream(mainXml));
            }
        } catch (
                Exception e) {
            log.error(e);
        }

        String finalSDateTimeMainXml = sDateTimeMainXml;
        return new

                SwingWorker() {

                    public Object construct() {

                        try {

                            log.info("Connecting as '" + user + "' at serverAlias:'" + serverAlias + "' server:'" + server
                                    + "'.");
                            Base base = createBase(user, pass, dbUser, dbPassword, server, serverAlias, "", finalSDateTimeMainXml);

                            log.info("Base created, now starting App.");

                            return new App(base);

                        } catch (Throwable e) {
                            if (e instanceof UserLoginException && UserLoginException.PASSWORD_EXPIRED == ((UserLoginException) e).getType()) {

                                frame.setVisible(false);

                                // Create password-change-dialog
                                ChangePasswordDialog dialog = new ChangePasswordDialog(frame, pass);

                                // Get the new password
                                String newPassword = dialog.getNewPassword();

                                // try ...
                                try {

                                    // ... to create base ...
                                    Base base = createBase(user, pass, dbUser, dbPassword, server, serverAlias, newPassword, finalSDateTimeMainXml);
                                    log.info("Password changed, base created, now starting App.");

                                    // ... and return application
                                    return new App(base);

                                } catch (Throwable ex) {

                                    frame.setVisible(true);
                                    return ex;

                                }

                            }

                            return e;
                        }

                    }

                    public void finished() {

                        Object result = get();
                        String msg;
                        if (result instanceof Throwable) {
                            boolean isDialogShowed = false;
                            msg = ((Throwable) result).getLocalizedMessage();
                            if (result instanceof BackendException || result instanceof MessageStack) {
                                ExceptionDialog.showExceptionDialog(frame, (Throwable) result, null);
                                isDialogShowed = true;
                            } else if (result instanceof UserLoginException && UserLoginException.PASSWORD_EXPIRED != ((UserLoginException) result).getType()) {
                                msg = getString("errormsg." + ((UserLoginException) result).getType());
                                if (msg.equals("loginDialog.errormsg." + ((UserLoginException) result).getType())) { // no translation
                                    msg = ((UserLoginException) result).getType().toString();
                                }
                            }
                            ((Throwable) result).printStackTrace();

                            setErrorMsg(msg);
                            if (!isDialogShowed) {
                                JOptionPane oPane = new JOptionPane(msg, JOptionPane.ERROR_MESSAGE);
                                JDialog dialog = oPane.createDialog(frame, getString("errorOptionPane.title"));
                                dialog.setVisible(true);
                                dialog.toFront();
                                dialog.requestFocusInWindow();
                            }

                        } else if (result instanceof App) {

                            // we have to show the main frame in the swing thread
                            app = (App) result;
                            log.info("UserLogin: Showing App.");
                            app.show();
                            // MCD, 24.Jan 2007,
                            // check if the application is started and the explorer has to be reloaded
                    /*SwingUtilities.invokeLater( new Runnable() {

                     public void run() {

                     app.checkReload();
                     }
                     } );*/
                            log.info("UserLogin: App is shown.");
                            if (!directStart) closeLoginFrame(false);
                        }

                        // well ... result = null? should never happen, we get
                        // exceptions
                        setStateConnecting(true);
                        if (hadFocusOnStart != null) {
                            hadFocusOnStart.requestFocus();
                        }
                    }

                }

                ;

    }// ------------------------------------------------

    private void setErrorMsg(String msg) {
        lblInfo.setText(msg);
        if (msg != null && msg.length() > 60) {
            lblInfo.setToolTipText("<html><body style='padding:2px;width:200px;'>" + HtmlText.toHtml(msg));
        } else {
            lblInfo.setToolTipText(msg);
        }

        lblInfo.setForeground(Colors.getUIControlTextWarn());
    }

    public static void main(String[] args) {
        InputStream is = PasswordUtil.class.getClassLoader().getResourceAsStream("key.txt");
        try {
            log.debug(ConvertUtils.inputStreamToString(is) + "\njhgh");
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}
