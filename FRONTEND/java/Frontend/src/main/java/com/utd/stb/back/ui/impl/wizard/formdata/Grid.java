package com.utd.stb.back.ui.impl.wizard.formdata;

import com.utd.stb.back.data.client.*;
import com.utd.stb.back.database.Dispatcher;
import com.utd.stb.back.ui.inter.WizardFormData;
import com.utd.stb.inter.application.BackendException;
import com.utd.stb.inter.items.DisplayableItem;
import com.utd.stb.inter.items.DisplayableItemComboList;
import com.utd.stb.inter.items.DisplayableItems;
import com.utd.stb.inter.wizzard.formtypes.FormDataGridOptions;

import java.util.ArrayList;
import java.util.Iterator;


/**
 * Handles data for the Grid window
 *
 * @author rainer bruns Created 20.01.2004
 */
public class Grid implements FormDataGridOptions, WizardFormData {

    Dispatcher dispatcher;
    /**
     * list of grid lines
     */
    ArrayList lines = new ArrayList();
    /*
     * list of grid entities, the columns
     */
    ArrayList entities;
    /**
     * list of data for first column
     */
    SelectionList data;
    SelectionList selection;
    SelectionRecord selected;
    int columns = 0;
    boolean navigate;
    DisplayableItemComboList comboList;
    private FormInfoRecord formInfo;
    private BackendException savedException = null;

    /**
     * initialises the data matrix in Grid
     *
     * @param d        the dispatcher
     * @param data     the list of data for first column
     * @param entities the list of entities(columns)
     * @param formInfo record of Form information, used for labels
     * @throws BackendException
     */
    public Grid(Dispatcher d,
                SelectionList data,
                ArrayList entities,
                FormInfoRecord formInfo,
                boolean navigate)
            throws BackendException {

        this.dispatcher = d;
        this.data = data;
        this.entities = entities;
        this.formInfo = formInfo;
        this.navigate = navigate;
        //now fill the matrix
        fillGrid(data);
    }

    /**
     * We get the data from backend column wise. First, with the first column
     * prepare a list for each entry, to store data of subsequent columns. This
     * is, to have data in a linewise order, needed for a table
     *
     * @param firstColList the data for the first column
     * @throws BackendException
     */
    private void fillGrid(SelectionList firstColList) throws BackendException {

        columns = 1;

        if (!navigate) firstColList = dispatcher.getGridSelection(entities.get(0).toString());

        //go through the entries in first column
        firstColList.initIterator();
        //for(int i = 0; i < firstColList.getSize(); i++)
        for (int i = 0; firstColList.hasNext(); i++) {
            // for each row make a list
            ArrayList temp = new ArrayList();
            /*
             * get next entry and create GridSelectionRecord store it as first
             * entry in the list
             */
            SelectionRecord sel = firstColList.getNext();
            // MCD, 16.Jan 2006
            temp.add(new GridSelectionRecord(sel,
                    sel.getValue(),
                    getEntity(columns - 1),
                    i));
            lines.add(temp);
        }
        //loop through all expected columns
        Iterator entIterator = entities.iterator();
        entIterator.next();
        while (entIterator.hasNext()) {
            columns++;
            EntityRecord e = (EntityRecord) entIterator.next();
            //get the list of data for column
            SelectionList list = dispatcher.getGridSelection(e.getEntity());
            // preselect first row/first editable column,
            // the really first one is the row header
            if (columns == 1) selected = (SelectionRecord) list.get(0);
            //and reorder the column
            colToRow(lines, list, columns - 1);
        }
    }

    /**
     * Backend sends us the data in columns. For table presentation order data
     * in rows
     *
     * @param lines   list with size according to the lines in grid
     * @param colList list of data for a column
     * @param aktCol  number of act. column ( starting with 0)
     */
    private void colToRow(ArrayList lines, SelectionList colList, int aktCol) {

        colList.initIterator();
        Iterator it2 = lines.iterator();

        /*
         * go through each line in grid
         */
        for (int i = 0; it2.hasNext(); i++) {
            //get the list for the columns
            ArrayList temp = (ArrayList) it2.next();
            // used to have the rowid in each cell data
            GridSelectionRecord rowidRecord = (GridSelectionRecord) temp.get(0);
            /*
             * there might not be data for all rows in every column. If not
             * create an entry with null value. However, it must not be, that
             * i.e. there is no entry in list for row 2, but for row 3 again
             * Then row 3 will end up in row 2, we can not guess this here.
             */
            if (colList.hasNext()) {
                SelectionRecord selection = colList.getNext();
                // MCD, 16.Jan 2006
                temp.add(new GridSelectionRecord(selection,
                        rowidRecord.getRowId(),
                        getEntity(aktCol),
                        i));
            } else { //list maybe empty or shorter then no of lines, but we need.
                // Init should be done by backend
                temp.add(new GridSelectionRecord(new SelectionRecord(),
                        rowidRecord.getRowId(),
                        getEntity(aktCol),
                        i));
            }
        }
    }

    /**
     * @param col no of col
     * @return the value of the entity
     */
    private String getEntity(int col) {

        EntityRecord e = (EntityRecord) entities.get(col);
        return e.getEntity();
    }

    /*
     * (non-Javadoc) return the Data for cell at col/row. GUI starts counting at
     * column 2 of data, because col 1 is header, so add 1 to when retrieving
     * from data, where we start with real first column
     * 
     * @see com.utd.stb.inter.wizzard.formtypes.FormDataGridOptions#getValue(int,
     *      int)
     */
    public DisplayableItemComboList getValue(int row, int column) {

        ArrayList selList = (ArrayList) lines.get(row);
        return (GridSelectionRecord) selList.get(column + 1);
    }

    /*
     * (non-Javadoc) nothing to do here, save is on setSelectedItem of each
     * single GridSelectionRecord
     * 
     * @see com.utd.stb.inter.wizzard.FormData#saveData()
     */
    public void saveData() throws BackendException {

        if (savedException == null) return;
        throw savedException;
    }

    /*
     * (non-Javadoc) the number of rows, is lines size
     * 
     * @see com.utd.stb.inter.wizzard.formtypes.FormDataGridOptions#getGridRowCount()
     */
    public int getGridRowCount() {

        return lines.size();
    }

    /*
     * (non-Javadoc) our data internal col count - 1, because of header column
     * 
     * @see com.utd.stb.inter.wizzard.formtypes.FormDataGridOptions#getGridColumnCount()
     */
    public int getGridColumnCount() {

        return columns - 1;
    }

    /*
     * (non-Javadoc) title is entity, col + 1 because of header column
     * 
     * @see com.utd.stb.inter.wizzard.formtypes.FormDataGridOptions#getColumnTitle(int)
     */
    public String getColumnTitle(int col) throws BackendException {

        return dispatcher.getTranslation("GRID$COL$" + getEntity(col + 1));
    }

    /**
     * data in a cell in a row might be dependend on another cell in same row
     * after a cell has been changed, ask backend for all entities(columns) that
     * are dependend, and clear data there, they are invalid now
     *
     * @param entity the entity that has been changed by user
     * @param rowid  and the rowid of row of the cell
     * @param row    the number of the row
     * @throws BackendException
     */
    public void clearDependends(String entity, String rowid, int row)
            throws BackendException {

        ArrayList line = (ArrayList) lines.get(row);

        //get the list of dependend entities and go through the list..
        EntityList cols = dispatcher.getDependendData(entity, rowid);
        cols.initIterator();
        for (int i = 0; cols.hasNext(); i++) {
            EntityRecord rec = cols.getNext();
            //if this entry is selected it is dependend
            if (rec.isSelected()) {
                //get the data record and clear data
                GridSelectionRecord cell = (GridSelectionRecord) line.get(i);
                cell.setValue(null);
                cell.setDisplay(null);
            }
        }
    }

    /*
     * (non-Javadoc) row Titles are the display of first column in the row
     * 
     * @see com.utd.stb.inter.wizzard.formtypes.FormDataGridOptions#getRowTitle(int)
     */
    public String getRowTitle(int row) {

        ArrayList theRow = (ArrayList) lines.get(row);
        GridSelectionRecord g = (GridSelectionRecord) theRow.get(0);
        return g.getDisplay();
    }

    /*
     * (non-Javadoc) label from FormInfo
     * 
     * @see com.utd.stb.inter.wizzard.formtypes.FormDataGridOptions#getLabel()
     */
    public String getLabel() {

        return formInfo.getPrompt1();
    }

    /* (non-Javadoc)
     * @see com.utd.stb.back.ui.inter.WizardFormData#getDataToSave()
     */
    public SelectionList getDataToSave() {

        return selection;
    }

    /**
     * this is what the GUI works on. Provides methods for getting the data for
     * a cell, setting them, get the list of available datas
     *
     * @author rainer bruns
     */
    private class GridSelectionRecord implements DisplayableItemComboList {

        SelectionRecord selection;
        String entity;
        String masterKey;
        int myRow;
        boolean readOnly;

        /**
         * @param selection record containing the data
         * @param row       the rowid of this cell
         * @param entity    the column identifier of this cell
         * @param row       number of row(?needed)
         */
        public GridSelectionRecord(SelectionRecord selection,
                                   String masterKey,
                                   String entity,
                                   int row) {

            this.selection = selection;
            this.masterKey = masterKey;
            this.entity = entity;
            this.myRow = row;
        }

        public DisplayableItem getSelectedItem() {

            return selection;
        }

        /*
         * (non-Javadoc) ask backend for the list of items, and return them
         * 
         * @see com.utd.stb.inter.items.DisplayableItemComboList#getAvailableItems()
         */
        public DisplayableItems getAvailableItems() throws BackendException {

            DisplayableItems dis = null;
            dis = dispatcher.getAvailableItems(entity, masterKey, false, false);
            return dis;
        }

        /*
         * (non-Javadoc) actually, no one gives us this information
         * 
         * @see com.utd.stb.inter.items.DisplayableItemComboList#isEmptySelectionAllowed()
         */
        public boolean isEmptySelectionAllowed() {

            return false;
        }

        /*
         * (non-Javadoc) called from GUI on setValueAt. Store data in backend
         * and check the dependencies
         * 
         * @see com.utd.stb.inter.items.DisplayableItemComboList#setSelectedItem(com.utd.stb.inter.items.DisplayableItem)
         */
        public void setSelectedItem(DisplayableItem item) {

            SelectionRecord oldSelection = selection;
            selection = (SelectionRecord) item;
            SelectionList selList = new SelectionList();
            selection.setOrderNum(oldSelection.getOrderNum());
            selList.add(selection);
            try {
                dispatcher.save(entity, selList, true);
                clearDependends(entity, masterKey, myRow);
            } catch (BackendException be) {
                savedException = be;
            }
        }

        private String getRowId() {

            // MCD, 21.Dez 2005
            return selection.getMasterKey();
        }

        private String getDisplay() {

            return selection.getDisplay();
        }

        private void setValue(String s) {

            selection.setValue(s);
        }

        private void setDisplay(String s) {
            try {
                selection.setDisplay(s);
            } catch (NullPointerException e) {
                e.printStackTrace();
            }

        }
    }

    /* (non-Javadoc)
     * @see com.utd.stb.back.ui.inter.WizardFormData#getEntity()
     */
    public String getEntity() {
        return formInfo.getEntity();
    }
}