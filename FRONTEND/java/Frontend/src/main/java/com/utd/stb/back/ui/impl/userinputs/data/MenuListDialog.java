package com.utd.stb.back.ui.impl.userinputs.data;

import com.utd.stb.StabberInfo;
import com.utd.stb.back.ui.impl.lib.ButtonItemsImpl;
import com.utd.stb.back.ui.impl.lib.StandardButton;
import com.utd.stb.inter.action.ButtonItem;
import com.utd.stb.inter.action.ButtonItems;
import com.utd.stb.inter.action.ComponentAction;
import com.utd.stb.inter.action.WindowAction;
import com.utd.stb.inter.application.BackendException;
import com.utd.stb.inter.application.Node;
import com.utd.stb.inter.dialog.HelpSource;
import com.utd.stb.interlib.WindowActionImpl;

import java.util.ArrayList;

/**
 * Implementation of interface UserDialog for  Menu dialogs with first element list of values (master-data).
 * By selected the value the details fields are filled.
 * There are Save, Close, New, Delete and Help buttons.
 *
 * @author Inna Smiljanski  created 12.07.13
 */
public class MenuListDialog extends MenuDialog {

    private ButtonItemsImpl buttons;
    private boolean okButtonPressed = false;
    private boolean closeButtonPressed = false;
    private boolean deleteButtonPressed = false;
    private Object actionOnClose = null;


    public MenuListDialog(UserInputData ui, String title, HelpSource hs) {
        super(ui, title, hs);
    }

    protected void createButtonItems() {

        ArrayList<StandardButton> list = new ArrayList<StandardButton>();
        StandardButton b;
        b = new StandardButton(ButtonItem.NEW, true);
        list.add(b);
        b = new StandardButton(ButtonItem.DELETE, true);
        list.add(b);
        b = new StandardButton(ButtonItem.SAVE, true);
        list.add(b);
        b = new StandardButton(ButtonItem.CLOSE, false);
        list.add(b);
        buttons = new ButtonItemsImpl(list);

    }

    public Object doAction(Object action) throws BackendException {
        log.debug("action = " + action);
        if (action instanceof UserInputData) {
            ui.saveData();
            if (closeButtonPressed) {
                ui.cancelAction();
                return new WindowActionImpl(WindowAction.CLOSE_WINDOW);
            } else if (deleteButtonPressed) {
                ui.properties.getFirst().setValue(StabberInfo.getTranslation("NEW_ENTRY"));
                ui.properties.getFirst().setDisplayValue(StabberInfo.getTranslation("NEW_ENTRY"));
                return ui.checkMask(getTitle(), Node.MENU_LIST_DIALOG);
            } else {
                return new MenuListDialog((UserInputData) action,
                        this.getTitle(),
                        this.getHelpSource());
            }
        }
        // ComponentAction means a component has changed
        if (action instanceof ComponentAction) {
            return ui.checkMask(getTitle(), Node.MENU_LIST_DIALOG);
            // ButtonItem means a button is pressed
        } else if (action instanceof ButtonItem) {
            ButtonItem b = (ButtonItem) action;

            if (b.getType().equals(ButtonItem.CLOSE) && !closeButtonPressed) {
                okButtonPressed = false;
                closeButtonPressed = true;
                Object checkMask = ui.checkMask(getTitle(), Node.MENU_LIST_DIALOG, new WindowActionImpl(WindowAction.CLOSE_WINDOW));
                return checkMask;
            } else if (b.getType().equals(ButtonItem.SAVE) && !okButtonPressed) {
                okButtonPressed = true;
                return new WindowActionImpl(WindowAction.SAVE_DATA);

            } else if (b.getType().equals(ButtonItem.NEW)) {
                //The first property must be a list of values (master-data)
                ui.properties.getFirst().setValue(StabberInfo.getTranslation("NEW_ENTRY"));
                ui.properties.getFirst().setDisplayValue(StabberInfo.getTranslation("NEW_ENTRY"));
                return ui.checkMask(getTitle(), Node.MENU_LIST_DIALOG);
            } else if (b.getType().equals(ButtonItem.DELETE)) {
                deleteButtonPressed = true;
                ui.properties.getFirst().setValue("DELETE");
                return ui.deleteData();
            }
            // WindowAction means a window button is pressed
        } else if (action instanceof WindowAction) {

            WindowAction winac = (WindowAction) action;
            if (winac.getType().equals(WindowAction.DATA_SAVED)) {
                Object returnSave = ui.saveData();
                log.debug(returnSave);
                if (returnSave instanceof WindowAction) {

                    return ui.checkMask(getTitle(), Node.MENU_LIST_DIALOG);
                } else return returnSave;

            } else if (winac.getType().equals(WindowAction.WINDOW_CLOSED)) {
                ui.cancelAction();
                return actionOnClose;
            } else if (winac.getType().equals(WindowAction.WANT_CLOSE)) {
                return ui.checkMask(getTitle(), Node.MENU_LIST_DIALOG, new WindowActionImpl(WindowAction.CLOSE_WINDOW));
            }
        }
        return null;
    }

    public ButtonItems getButtonItems() {

        return buttons;
    }

    public ButtonItem getDefaultButtonItem() {

        return buttons.get(0);
    }

}
