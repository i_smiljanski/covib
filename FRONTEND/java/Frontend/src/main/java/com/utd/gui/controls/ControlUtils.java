package com.utd.gui.controls;

import javax.swing.*;
import java.awt.*;


/**
 * Some tiny things help enhancing controls, don't know where to put else.
 *
 * @author PeterBuettner.de
 */
public class ControlUtils {

    /**
     * returns the enclosing JScrollpane of a JTable/JList/component/... <br>
     * works like {@link JTable.configureEnclosingScrollPane()}
     *
     * @param component
     * @return
     */
    public static JScrollPane getEnclosingScrollPane(Component component) {

        // look into JTable#configureEnclosingScrollPane(), remove the trash, it
        // will look like this
        JScrollPane sp = (JScrollPane) SwingUtilities.getAncestorOfClass(JScrollPane.class,
                component);
        /*
         * Make certain we are the viewPort's view and not, for example, the
         * rowHeaderView of the scrollPane - an implementor of fixed columns
         * might do this.
         */
        if (sp == null) return null;
        JViewport viewport = sp.getViewport();
        if (viewport == null) return null;
        if (viewport.getView() != component) return null;

        return sp;
    }

    /**
     * check if the component is the permanent focus owner and its top window is
     * the focused window
     *
     * @param component
     * @return
     */
    public static boolean isPermFocusAndAppActive(Component component) {

        KeyboardFocusManager fm = KeyboardFocusManager.getCurrentKeyboardFocusManager();
        if (component != fm.getPermanentFocusOwner()) return false;

        Window fw = fm.getFocusedWindow();
        return fw == component || SwingUtilities.getWindowAncestor(component) == fw;
    }

}