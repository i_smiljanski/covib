package com.utd.gui.filter;

import java.util.regex.Matcher;
import java.util.regex.Pattern;


/**
 * A Filter implementation that uses regular expressions. Set the pattern, use
 * {@link java.util.regex.Pattern#compile()}.<br>
 * If this is in wrong state (pattern is null) everything matches. If pattern ==
 * null everything matches.
 *
 * @author PeterBuettner.de
 */
public class RegExpFilter extends AbstractFilter {

    /**
     * Enumeration for matchType
     */
    public static final class Type {

        private String name;

        public String toString() {

            return name;

        }

        private Type(String name) {

            this.name = name;

        }

        /**
         * start of candidate matches the pattern, tail doesn't matter, but
         * respects '$' on end, so to have a type 'START' and match the whole
         * thing you may use 'yourPattern[a-f]+$' e.g.
         */
        public static final Type START = new Type("lookingAt");

        /**
         * the whole candidate matches the pattern
         */
        public static final Type ALL = new Type("matches");

        /**
         * any (small) part of the candidate matches the pattern
         */
        public static final Type PART = new Type("find");

    }

    private Pattern pattern;
    private Type matchType = Type.START;

    /**
     * type defaults to Type.START, which respects '$' on the end
     *
     * @param pattern
     */
    public RegExpFilter(Pattern pattern) {

        this(pattern, Type.START);
        //this.pattern = pattern;
        //this.matchType = Type.START;
    }

    public RegExpFilter(Pattern pattern, Type matchType) {

        this.pattern = pattern;
        this.matchType = matchType;
    }

    /**
     * tests the candidate against the pattern, candidate.toString() is used.
     */
    public boolean matches(Object candidate) {

        if (pattern == null) return true;
        Matcher m = pattern.matcher(String.valueOf(candidate));

        // MCD, 30.Jan 2006, klaeren??, LOV
        if (candidate instanceof com.utd.stb.back.data.client.UserInputRecord)

            m = pattern.matcher(String.valueOf(((com.utd.stb.back.data.client.UserInputRecord) candidate).getInfo()));

        if (Type.ALL == matchType) return m.matches();    // passt genau, z.B. EISENBAHNER in EISENBAHNER
        if (Type.START == matchType) return m.lookingAt();    // der Anfang passt, z.B. EIS in EISENBAHNER
        if (Type.PART == matchType) return m.find();        // ein Teil passt, z.B. BAHN in EISENBAHNER

        // should never be reached, since we enumerate
        throw new IllegalStateException("matchType has wrong value: " + matchType);
    }

    /**
     * Get the pattern.
     *
     * @return
     */
    public Pattern getPattern() {

        return pattern;
    }

    /**
     * Sets the pattern, <br>
     * note: Type.START automatically respects '$' at the end of the regExp! if
     * pattern == null: match all
     *
     * @param pattern
     */
    public void setPattern(Pattern pattern) {

        this.pattern = pattern;
        fireFilterChanged();
    }

    /**
     * Combined to only fire Changed one time (filtering x0.000 entries twice
     * maybe slow)
     *
     * @param pattern
     * @param matchType
     */
    public void setPatternAndType(Pattern pattern, Type matchType) {

        this.pattern = pattern;
        this.matchType = matchType;
        fireFilterChanged();
    }

    /**
     * See {@link getMatchType(Type)}.
     *
     * @return
     */
    public Type getMatchType() {

        return matchType;
    }

    /**
     * If the whole data-string have to fit, only parts, or only the start of
     * it. <br>
     * <p>
     * <pre>
     *
     *  Examples:
     *  Data      Pattern    MatchType   will match
     *  &quot;abcde&quot;    &quot;bcd&quot;       ALL        no
     *  &quot;abcde&quot;    &quot;bcd&quot;       PART       yes
     *  &quot;abcde&quot;    &quot;ab&quot;        START      yes
     *
     * </pre>
     *
     * @param matchesAll
     */
    public void setMatchType(Type matchType) {

        this.matchType = matchType;
        if (this.matchType != matchType) fireFilterChanged();
    }

}