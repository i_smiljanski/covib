package com.utd.gui.popup;

public interface PopupShowableScrollable {

    /**
     * show/hide the popup, show may not always have any effect, since it may
     * already be visible, or (if the popup's associated control[e.g.
     * 'textfield']) has no parent (not part of the window hirarchy)
     *
     * @param show
     */
    void popShow(boolean show);

    /**
     * if the popup's associated control[e.g. 'textfield']) is a celleditor, it
     * should be attempted to scroll the editor into the users view, called on
     * keypresses and some mouseevents
     */
    void scrollEditorToVisible();
}