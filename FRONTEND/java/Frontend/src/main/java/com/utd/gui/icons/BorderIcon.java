/*
 * Intellectual property of PeterBuettner.de
 * Use at your own risc.
 * 
 */
package com.utd.gui.icons;

import javax.swing.*;
import java.awt.*;


/**
 * Centers a smaller/bigger icon, if the 'inner' icon is bigger then it may
 * paint outside of the Rectangle (x,y,width,height)
 *
 * @author PeterBuettner.de
 */
public class BorderIcon implements Icon {

    private Icon icon;
    private int width;
    private int height;

    public BorderIcon(Icon icon, int width, int height) {

        this.icon = icon;
        this.width = width;
        this.height = height;

    }

    public BorderIcon(Icon icon, int size) {

        this(icon, size, size);

    }

    public int getIconHeight() {

        return height;

    }

    public int getIconWidth() {

        return width;

    }

    public void paintIcon(Component c, Graphics g, int x, int y) {

        icon.paintIcon(c, g, x + (width - icon.getIconWidth()) / 2, y
                + (height - icon
                .getIconHeight())
                / 2);

    }

}