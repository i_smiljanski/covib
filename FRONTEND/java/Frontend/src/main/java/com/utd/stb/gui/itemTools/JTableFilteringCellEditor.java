package com.utd.stb.gui.itemTools;

import com.jidesoft.grid.CellRendererManager;
import com.jidesoft.grid.JideTable;
import com.jidesoft.grid.TableUtils;
import com.utd.gui.controls.ControlUtils;
import com.utd.gui.controls.table.BaseTable;
import com.utd.gui.controls.table.ListViewTools;
import com.utd.gui.controls.table.TableTools;
import com.utd.gui.event.AdjustableEnabler;
import com.utd.gui.filter.Filter;
import com.utd.gui.filter.FilterTableModel;
import com.utd.gui.popup.AbstractFilteringCellEditor;
import com.utd.gui.popup.MouseHoverSelectsListener;
import com.utd.stb.gui.userinput.swing.ActionDialog;
import com.utd.stb.inter.items.DisplayableItem;

import javax.swing.*;
import javax.swing.border.EmptyBorder;
import javax.swing.event.TableModelEvent;
import javax.swing.event.TableModelListener;
import javax.swing.table.TableModel;
import java.awt.*;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;


/**
 * A Celleditor that appears like a combobox: a textfield that works as a filter
 * for a popup JTable. <br>
 * if only one attribute in list, select this per default, so an [ENTER]-key will use
 * it <br>
 * See: {@link com.utd.gui.popup.AbstractFilteringCellEditor}for important
 * details, especially to configure the table
 *
 * @author PeterBuettner.de
 */
public class JTableFilteringCellEditor extends AbstractFilteringCellEditor {

    /*
     * 'data' is needed as a field since we create the list on demand, and have
     * to prepare some things in the superclass before
     */
    private final TableModel data;

    /**
     * standalone defalut to false
     *
     * @param textField
     * @param data
     */
    public JTableFilteringCellEditor(JTextField textField, TableModel data) {
        this(textField, data, false);
    }

    /**
     * @param textField
     * @param data
     * @param standalone placed in a JTable
     */

    public JTableFilteringCellEditor(JTextField textField, TableModel data, boolean standalone) {

        super(textField, standalone);
//        System.out.println("JListFilteringCellEditor3 = " + textField);
        this.data = data;
    }

    protected JComponent createList(Filter filter) {

        FilterTableModel filterTableModel = new FilterTableModel(data, filter, 0);
        final JideTable table = new BaseTable(filterTableModel) {

            public Dimension getPreferredScrollableViewportSize() {
                //IS, 25.Jun 2013:  auto resize columns, s.below
                int width = 0;
                for (int i = 0; i < getColumnCount(); i++) {
                    width += getColumnModel().getColumn(i).getWidth();
                }
                // end
                Dimension dim = super.getPreferredScrollableViewportSize();
                // MCD, 20.Jun 2007, set the size of popup, VERBESSERN WIDTH
                // dim.width = Math.max( 200, Math.min( dim.width, 250 ) );
                dim.width = Math.min(dim.width, width + 40);
                dim.height = ((getRowCount() > 16 ? 16 : getRowCount()) + 2) * getRowHeight();
                //System.out.println("dim = " + dim);
                return dim;
            }

            protected void configureEnclosingScrollPane() {

                super.configureEnclosingScrollPane();


                JScrollPane sp = ControlUtils.getEnclosingScrollPane(this);
                if (sp == null) return;

                /*
                 * now tune the scrollbars this looks better on JTable since the
                 * number items change, the scrollbar toggles from in/visible
                 * and the columns jump around:
                 */
                sp.setVerticalScrollBarPolicy(JScrollPane.VERTICAL_SCROLLBAR_ALWAYS);
                sp.getVerticalScrollBar().addAdjustmentListener(new AdjustableEnabler());
            }

            public void tableChanged(TableModelEvent e) {

                super.tableChanged(e);
                // autoselect the single row todo (naechste zeile auskommentiert)
                //  if ( getRowCount() == 1 ) setRowSelectionInterval( 0, 0 );
            }

        };

        //IS, 25.Jun 2013:  auto resize columns
        table.setColumnResizable(true);
        table.setColumnAutoResizable(true);
        TableUtils.autoResizeAllColumns(table);

        ListViewTools.makeListViewSized(table);
        table.setBorder(new EmptyBorder(0, 0, 0, 0));
//        table.setAutoResizeMode(JTable.AUTO_RESIZE_ALL_COLUMNS);
        table.getTableHeader().setReorderingAllowed(false);
        table.setSelectionMode(ListSelectionModel.SINGLE_SELECTION);
        table.setDefaultRenderer(Object.class, new DisplayableItemTableCellRenderer());
        CellRendererManager.registerRenderer(Object.class, new DisplayableItemTableCellRenderer());

        table.addMouseListener(new ComponentMouseAdapter(this.getComponent()));

        table.addMouseMotionListener(new MouseHoverSelectsListener());

        filterTableModel.addTableModelListener(new TableModelListener() {

            public void tableChanged(TableModelEvent e) {
                // select first line, but only if text isn't empty
                if (!isFilterEmpty()) TableTools.selectFirstRow(table, true);
            }
        });

        return table;
    }

    private class ComponentMouseAdapter extends MouseAdapter {
        Component comp;

        public ComponentMouseAdapter(Component component) {
            comp = component;
        }

        private Component getActionDialog(Component c) {
            //System.out.println("c = " + c);
            if (c.getParent() == null || c instanceof ActionDialog) return c;
            return getActionDialog(c.getParent());
        }

        public void mouseClicked(MouseEvent e) {

            if (SwingUtilities.isLeftMouseButton(e)) {
                useCurrentListSelection();
                //IS, 27.Jun 2013:  the mask will be updated by list selection
                Component dialog = getActionDialog(comp);
                if (dialog != null && dialog instanceof ActionDialog) {
                    ((ActionDialog) dialog).performComponentAction();
                }

            }
        }
    }

    protected Object getCurrentListSelection() {
 
        /*
         * with DisplayableItem s in model and model reacting on getValueAt(row,
         * -1), use the DisplayableItem. if no DisplayableItem found, use first
         * cell (of model))
         */

        JTable table = (JTable) displayList;
        int selRow = table.getSelectedRow();
        if (selRow == -1) return null;

        TableModel model = table.getModel();
        Object o = model.getValueAt(selRow, -1);
        if (o instanceof DisplayableItem) return o;
        return model.getValueAt(selRow, 0); // first cell as default
    }

    protected String valueToString(Object value) {
        System.out.println("valueToString: value = " + value);
        if (value instanceof DisplayableItem)
            // super will convert to string/null
            value = ((DisplayableItem) value).getData();

        return super.valueToString(value);
    }

    protected boolean isPopupList() {
        return true;
    }

}