package com.utd.gui.controls.tree;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import javax.swing.tree.TreePath;
import java.util.*;


/**
 * A TreeModel that wraps any userData (~nodes), it is build from a NodeSource
 * and only the nodes are presented to the outside. The structure is only
 * accessible though the TreeModel interface. The nodes have to implement
 * <b>equals() and hashCode() </b> in the right way, since internally
 * <code>Map</code> s are used. If a node is reloaded the new child nodes may
 * equal the deceased ones, we handle this correct. <br>
 * All nodes are loaded on demand, so isLeaf() returns false as long as that
 * nodes children were never loaded.
 * <p>
 * <p>
 *
 * @author PeterBuettner.de
 */
public class WrappingTreeModel extends AbstractTreeModel {
    public static final Logger log = LogManager.getRootLogger();

    public interface NodeSource {

        /**
         * WrappingTreeModel loads nodes only through this interface
         *
         * @param parent use null to get the roots childs
         * @return null means no childs, also my be an empty list
         */
        List loadNodes(Object parent);
    }

    private NodeSource source;
    private static Object root = new Object();

    /**
     * is here to have at least one hard reference, so we save all other data
     * from beeing gc 'cause of the weakness of our Map
     */
    private final TN rootTn = new TN(null, root);

    /**
     * simple tree: nearly immutable (parents and data stay same), but children
     * maybe mutable: empty, unloaded, reloaded, they are lazy-loaded on demand
     * <br>
     * caveat: in some cases children are not loaded and maybe accessed from
     * outside (this is done 'cause we don't want them to be loaded only to see
     * if a node is a leaf), review this later, maybe we could secure this
     * TODOlater review this, maybe more secure
     * <p>
     * <p>
     * purpose: we don't want to make a whole DefaultMutableTreenode accessible
     * from outside.
     */
    private static class TN {

        /**
         * save the parent too -> create treepath is easy
         */
        private final TN parent;

        /**
         * this is what is presented to the outside from the TreeModel
         */
        private final Object data;

        /**
         * null: not loaded, <br>
         * empty list: loaded but no childs
         */
        private List childs;

        private TN(Object data) {

            this(null, data);
        }

        private TN(TN parent, Object data) {

            this.parent = parent;
            this.data = data;

        }

        private List getChilds(NodeSource source, Map map) {

            if (childs == null) {
                if (data == root) childs = makeChilds(source.loadNodes(null), map);
                else childs = makeChilds(source.loadNodes(data), map);
            }
            return childs;
        }

        private List makeChilds(List list, Map map) {

            if (list == null) return new ArrayList(0);// mark as loaded but
            // empty
            // wrap userData with TN:
            List l = new ArrayList(list.size()); // we almost never add an
            // entry, so fit to size
            for (Iterator i = list.iterator(); i.hasNext(); ) {
                TN tn = new TN(this, i.next());
                l.add(tn);
                map.put(tn.data, tn);
            }

            return l;
        }

        private boolean isLoaded() {

            return childs != null;
        }

        private TreePath getDataTreePath() {

            List list = new ArrayList();

            for (TN n = this; n != null; n = n.parent) {
                list.add(n.data);
            }
            Collections.reverse(list);

            return new TreePath(list.toArray());
        }

        /**
         * does nothing if not loaded, also removes all data object from
         * removeChildDataHere, works recursive
         *
         * @param removeChildDataHere we call removeAll() with a collection of all data Objects
         */
        private void removeChilds(Collection removeChildDataHere) {

            if (childs == null) return;
            removeChildDataHere.removeAll(dataList());
            for (int i = 0; i < childs.size(); i++)
                ((TN) childs.get(i)).removeChilds(removeChildDataHere);
            childs = null;
        }

        public String toString() {

            return super.toString() + "\tParent-data:" + (parent == null ? null : parent.data) + "\tData:" + data;
        }

        // ......................................................
        // crash if not loaded:
        private List dataList() {

            return new AbstractList() {// this is a thin one: only one(!) field:
                // modCount

                public Object get(int i) {

                    return getChildData(i);
                }

                public int size() {

                    return childs.size();
                }
            };
        }

        Object getChildData(int i) {

            return ((TN) childs.get(i)).data;
        }

        int getChildCount() {

            return childs.size();
        }

        int indexOfChild(Object data) {

            return dataList().indexOf(data);
        }// rarely called?

    }

    /**
     * map from <b>userObjects </b> (what is visible outside) <b>-&gt; TN </b>
     * (our shadow tree structure). <br>
     * we may use a weak one since we have a shadow structure with the TNs and
     * the root is also there
     * <p>
     * Noooo if we remove a node, the shadow stays here for a short time, if we
     * reread afterwards, we see an old zombie strucure, may work: remove
     * structure from map or remove them on loadNodes() in TN#getChilds()
     */
    private Map tnMap = new HashMap();

    /**
     * Get the wrapping TN from the map, may forceLoad of childs
     *
     * @param parent
     * @param forceLoad forces to load childs
     * @return
     */
    private TN getTn(Object parent, boolean forceLoad) {
        TN tn = (TN) tnMap.get(parent);
        if (tn == null) { // TODO :
            // this is a clear sign for an error in the
            // consistency, must be removed, but throws an
            // exception?
            log.warn("WrappingTreeModel: illegal state; " + "this parent is not found in the tree, " +
                    "" + "but it should: " + parent);
            tn = new TN(parent);
            tnMap.put(parent, tn);
        }
        if (forceLoad) tn.getChilds(source, tnMap);
        return tn;
    }

    /**
     * removes all childs from the tree, the state of parent is as never loaded
     * then
     */
    private void removeChilds(Object parent) {

        Object tn = tnMap.get(parent);
        if (tn != null) ((TN) tn).removeChilds(tnMap.keySet());

    }

    public WrappingTreeModel(NodeSource source) {

        this.source = source;
        tnMap.put(root, rootTn);
    }

    public Object getRoot() {

        return root;
    }

    public int getChildCount(Object parent) {

        return getTn(parent, true).getChildCount();
    }

    public boolean isLeaf(Object node) {

        TN tn = getTn(node, false);
        if (tn.isLoaded()) return tn.getChildCount() == 0;
        return false;
    }

    public Object getChild(Object parent, int i) {

        return getTn(parent, true).getChildData(i);
    }

    public int getIndexOfChild(Object parent, Object child) {

        return getTn(parent, true).indexOfChild(child);
    }

    /**
     * Does nothing here!
     *
     * @param path
     * @param newValue
     */
    public void valueForPathChanged(TreePath path, Object newValue) {

        // do nothing since we don't allow the nodes to be edited
    }

    /**
     * The full path from root to the node
     *
     * @param node
     * @return
     */
    public TreePath getTreePath(Object node) {

        return getTn(node, false).getDataTreePath();
    }

    /**
     * removes all childs <b>and descendants </b>, fires nodechange
     *
     * @param parent
     */

    public void setNodeUnloaded(Object parent) {

        removeChilds(parent);
        TreePath treePath = getTreePath(parent);
//        log.debug(treePath);
        fireTreeStructureChanged(treePath.getPath());
    }
}