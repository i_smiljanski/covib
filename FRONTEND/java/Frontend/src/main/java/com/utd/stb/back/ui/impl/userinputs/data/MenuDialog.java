package com.utd.stb.back.ui.impl.userinputs.data;

import com.utd.stb.back.ui.impl.lib.ButtonItemsImpl;
import com.utd.stb.back.ui.impl.lib.StandardButton;
import com.utd.stb.back.ui.impl.lib.WindowActionImpl;
import com.utd.stb.back.ui.impl.msgboxes.WarningMsgBox;
import com.utd.stb.inter.action.ButtonItem;
import com.utd.stb.inter.action.ButtonItems;
import com.utd.stb.inter.action.ComponentAction;
import com.utd.stb.inter.action.WindowAction;
import com.utd.stb.inter.application.BackendException;
import com.utd.stb.inter.application.Node;
import com.utd.stb.inter.dialog.HeaderBar;
import com.utd.stb.inter.dialog.HelpSource;
import com.utd.stb.inter.dialog.UserDialog;
import com.utd.stb.inter.userinput.UserInputs;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.util.ArrayList;

/**
 * Implementation of interface UserDialog for common Menu dialogs. This means:
 * OK, Cancel and Help button, and same behaviuor on saving, canceling
 *
 * @author rainer bruns Created 14.12.2004
 */
public class MenuDialog implements UserDialog {
    public static final Logger log = LogManager.getRootLogger();

    protected UserInputData ui;

    private String title;

    private ButtonItemsImpl buttons;

    private boolean okButtonPressed = false;

    private HelpSource hs;

    private Object actionOnClose = null;

    private boolean esig; // esig-Dialog

    private String type;


    /**
     * @param ui    dialog field data
     * @param title title
     * @param hs    source of help
     */
    public MenuDialog(UserInputData ui, String title, HelpSource hs) {
        this(ui, title, hs, false);
    }


    /**
     * @param ui    dialog field data
     * @param title title
     * @param hs    source of help
     * @param esig  boolean, is it esig dialog or not
     */
    public MenuDialog(UserInputData ui, String title, HelpSource hs, boolean esig) {

        this.ui = ui;
        this.title = title;
        this.hs = hs;
        this.esig = esig;
        createButtonItems();

    }


    /**
     * OK and Cancel button for this dialog, OK is standard
     */
    protected void createButtonItems() {

        ArrayList list = new ArrayList();
        StandardButton b;
        b = new StandardButton(ButtonItem.OK, true);
        list.add(b);
        b = new StandardButton(ButtonItem.CANCEL, false);
        list.add(b);
        buttons = new ButtonItemsImpl(list);

    }

    public String getTitle() {

        return title;
    }

    public HeaderBar getHeaderBar() {

        return null;
    }

    public UserInputs getUserInputs() {

        return ui;
    }

    /*
     * (non-Javadoc) defines the behaviour on saving, canceling for menu dialogs
     *
     * @see com.utd.stb.inter.dialog.UserDialog#doAction(java.lang.Object)
     */
    public Object doAction(Object action) throws BackendException {
//        log.debug(action);
        // ComponentAction means a component has changed
        if (action instanceof ComponentAction) {
            return ui.checkMask(getTitle(), Node.MENU_DIALOG);
            // ButtonItem means a button is pressed
        } else if (action instanceof ButtonItem) {

            ButtonItem b = (ButtonItem) action;

            if (b.getType().equals(ButtonItem.CANCEL)) {

                // tell backend dialog is canceled, and GUI to close window
                okButtonPressed = false;
                if (esig && ui.sendFailureOnCancel()) {
                    ui.setNumberOfFailuresToNull();
                    actionOnClose = ui.saveDataWithNewToken();
                }
                ui.cancelAction();
                return new WindowActionImpl(WindowAction.CLOSE_WINDOW);

            } else if (b.getType().equals(ButtonItem.OK)) {
//                    && !okButtonPressed) {
                // MCD 23.Nov 2005 !okButtonPressed to avoid the possibility of
                // double-clicks
// is 28.jun 2016 auskommentiert, weil im fall vom fehler beim wiederholten OK-button wurde nichts passiert (Maske 'Search Document'), isrc-364

                // tell GUI to write back data tu UserInputs
                okButtonPressed = true;
                return new WindowActionImpl(WindowAction.SAVE_DATA);

            }
            // WindowAction means a window button is pressed
        } else if (action instanceof WindowAction) {

            WindowAction winac = (WindowAction) action;
            if (winac.getType().equals(WindowAction.DATA_SAVED)) {

                // now write data to DB and tell GUI to close window
                if (okButtonPressed) {

                    // if it is a dialog containing password fields to the checks
                    // first check users password
                    if (ui.hasPasswordToCheck()) {

                        // esig-Dialog
                        if (esig) {

                            if (ui.isUsernameShown()
                                    && !ui.isUserOk()
                                    && ui.getNumberOfUserFailures() < ui.getNumberOfPossibleFailures()) {

                                // show warning box while password failures less than possible password failures
                                okButtonPressed = false;
                                ui.saveDataWithNewToken();
                                return new WarningMsgBox(ui.getTranslation("msgWrongUserTitle"),
                                        ui.getTranslation("msgWrongUserText"));

                            } else if (!ui.isPasswordOk()
                                    && ui.getNumberOfPasswordFailures() < ui.getNumberOfPossibleFailures()) {

                                // show warning box while password failures less than possible password failures
                                okButtonPressed = false;
                                ui.saveDataWithNewToken();
                                return new WarningMsgBox(ui.getTranslation("msgWrongPasswordTitle"),
                                        ui.getTranslation("msgWrongPasswordText"));

                            } else if ((ui.isUsernameShown() && !ui.isUserOk()) || !ui.isPasswordOk()) {

                                // set the number of failures = 0,
                                // send data to database and close window
                                ui.setNumberOfFailuresToNull();
                                actionOnClose = ui.saveDataWithNewToken();
                                ui.cancelAction();
                                return new WindowActionImpl(WindowAction.CLOSE_WINDOW);

                            }

                        } else {

                            if (!ui.isPasswordOk()) {

                                okButtonPressed = false;
                                return new WarningMsgBox(ui.getTranslation("msgWrongPasswordTitle"),
                                        ui.getTranslation("msgWrongPasswordText"));

                            } /*else if (!ui.passwordEqual()) {

                                okButtonPressed = false;
                                // on password change: new password must be typed two times identical
                                return new WarningMsgBox(ui.getTranslation("msgWrongInputTitle"),
                                        ui.getTranslation("msgPasswordNotEqualText"));

                            }*/

                        }

                    } /*else {

                        if (!ui.passwordEqual()) {

                            okButtonPressed = false;
                            // on password change: new password must be typed two times identical
                            return new WarningMsgBox(ui.getTranslation("msgWrongInputTitle"),
                                    ui.getTranslation("msgPasswordNotEqualText"));
                        }
                    }*/

                    actionOnClose = ui.saveData();
                    log.debug(actionOnClose);
                    // if the action during closing an audit is, return the action without closing the window
                    if (actionOnClose instanceof SaveAuditDialog) {
                        okButtonPressed = false;
                        return actionOnClose;
                    }

                }

                // close the window
                return new WindowActionImpl(WindowAction.CLOSE_WINDOW);

            } else if (winac.getType().equals(WindowAction.WINDOW_CLOSED)) {

                // did we already
                if (actionOnClose instanceof SaveAuditDialog) return null;
                // return an action for the GUI. Probably null
                // since tree reloads are stored in queue of TreeBase
                return actionOnClose;

            } else if (winac.getType().equals(WindowAction.WANT_CLOSE)) {

                // escape
                if (esig && ui.sendFailureOnCancel()) {
                    ui.setNumberOfFailuresToNull();
                    actionOnClose = ui.saveDataWithNewToken();
                }
                ui.cancelAction();
                return new WindowActionImpl(WindowAction.CLOSE_WINDOW);
            }
        }
        return null;
    }

    public HelpSource getHelpSource() {

        return hs;
    }

    public ButtonItems getButtonItems() {

        return buttons;
    }

    public ButtonItem getDefaultButtonItem() {

        return buttons.get(0);
    }

}