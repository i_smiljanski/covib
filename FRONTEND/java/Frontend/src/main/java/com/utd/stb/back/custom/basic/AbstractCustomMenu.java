package com.utd.stb.back.custom.basic;

import com.utd.stb.inter.action.MenuItem;
import com.utd.stb.inter.action.MenuItems;

import java.util.List;


/**
 * @author rainer bruns Created 20.12.2004 Abstract class describing a custom
 *         menu
 */
public abstract class AbstractCustomMenu {

    /**
     * @return true if there is a custom menu, else false
     */
    public static boolean hasCustomMenu() {

        return false;
    }

    /**
     * @param list a list where the created menu list has to be added to
     * @return the extended list
     */
    public abstract List getCustomMenu(List list);

    /**
     * @author rainer bruns Created 20.12.2004 Implementation of MenuItems for
     *         the custom menu
     */
    public static class CustomMenuItemsImpl implements MenuItems {

        List list;

        public CustomMenuItemsImpl(List list) {

            this.list = list;
        }

        public int getSize() {

            return list.size();
        }

        public MenuItem get(int i) {

            return (MenuItem) list.get(i);
        }

        public int indexOf(MenuItem menuItem) {

            return list.indexOf(menuItem);
        }

    }
}