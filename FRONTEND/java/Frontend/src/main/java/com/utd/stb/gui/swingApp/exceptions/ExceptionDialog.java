package com.utd.stb.gui.swingApp.exceptions;

import com.jgoodies.forms.builder.DefaultFormBuilder;
import com.jgoodies.forms.factories.Borders;
import com.jgoodies.forms.factories.ButtonBarFactory;
import com.jgoodies.forms.layout.FormLayout;
import com.uptodata.isr.gui.lookAndFeel.TweakUI;
import com.uptodata.isr.utils.exceptions.MessageException;
import com.uptodata.isr.utils.exceptions.MessageStackException;
import com.utd.gui.action.ActionCancel;
import com.utd.gui.action.ActionOk;
import com.utd.gui.action.ActionUtil;
import com.utd.gui.controls.TitleBar;
import com.utd.gui.util.DialogUtils;
import com.utd.stb.back.data.client.ErrorRecord;
import com.utd.stb.back.database.BackendErrorException;
import com.utd.stb.back.ui.impl.msgboxes.ErrorMsgBox;
import com.utd.stb.back.ui.impl.msgboxes.WarningMsgBox;
import com.utd.stb.inter.application.BackendException;
import com.utd.stb.interlib.BasicMessageBox;
import org.apache.commons.lang.StringUtils;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import resources.IconSource;
import resources.Res;

import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.io.PrintWriter;
import java.io.StringWriter;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;


/**
 * A standard dialog for an exception (Throwable) with only a ok(close) button
 * as action
 *
 * @author PeterBuettner.de
 */
public class ExceptionDialog {
    public static final Logger log = LogManager.getRootLogger();

    /**
     * Shows a standard dialog for an exception (Throwable) with only a
     * ok(close) button as action
     *
     * @param parentComponent a component, we search for the Window(Frame/Dialog) this
     *                        component belongs to
     * @param throwable
     * @param extraText       some extra text in the textarea, maybe null or empty
     * @return
     */
    static public void showExceptionDialog(Component parentComponent, Throwable throwable, String extraText) {
        showExceptionDialog(parentComponent, throwable, extraText, true, -1);
    }

    /**
     * @param parentComponent
     * @param throwable
     * @param extraText
     * @param isModal
     * @param showTime        in seconds
     */
    static public void showExceptionDialog(Component parentComponent, Throwable throwable, String extraText, boolean isModal, int showTime) {

        log.warn("Showing ExceptionDialog: " + throwable + "; showTime=" + showTime);
        JDialog dialog = createDialog(parentComponent, isModal);
        // ... Content: collect Info, build text
        int severity = BackendException.SEVERITY_INFORMATIONAL;

        // @@PEB-2005-05-01@@
        if (throwable instanceof BackendException) {
            BackendException beEx = (BackendException) throwable;
            severity = beEx.getSeverity();
        } else if (throwable instanceof MessageStackException) {
            MessageStackException messageStackException = (MessageStackException) throwable;
            int severityFromDb = messageStackException.getSeverity();
            severity = ErrorRecord.getSeverityForGui(severityFromDb);
        }

        String infoText;
        if (severity == BackendException.SEVERITY_INFORMATIONAL) {
            infoText = (extraText == null || extraText.isEmpty()) ? throwable.getLocalizedMessage() : extraText + "\n\n";
        } else {
            infoText = getInfoText(severity);
        }
        log.debug(infoText);
        JPanel content = createContent(throwable, severity, infoText, extraText, getFont());
        initDialogBody(dialog, severity, content);

        dialog.setLocationRelativeTo(dialog.getOwner());// center on parent
        dialog.setVisible(true);
        if (showTime > 0) {
            Timer timer = new Timer(showTime * 1000, arg0 -> dialog.setVisible(false));
            timer.setRepeats(false); // Only execute once
            timer.start();
        }
        log.warn("Showing ExceptionDialog end " + throwable);
    }

    private static JDialog createDialog(Component parentComponent, boolean isModal) {
        // ... create dialog ..............................
        final JDialog dlg = DialogUtils.createDialog(parentComponent); //todo
        dlg.setModal(isModal);
        // ....
        String title = getTopParentTitle(dlg);
        if (title == null) title = getResStr("window.title");
        dlg.setTitle(title);
        dlg.setDefaultCloseOperation(JDialog.DISPOSE_ON_CLOSE);

        // some Tools:
        IconSource iconSource = new IconSource();

        // .. now a small hack: if the frame is the default frame then set a new
        // icon
        // TODOlater move this thingy to the dialog creation in dilaog tools
        boolean setIcon = false;
        if (dlg.getOwner() instanceof Frame) {
            Frame f = (Frame) dlg.getOwner();
            if (f.getIconImage() == null) {
                Image icon = iconSource.getSmallImageByID("frameMainApp");
//                f.setIconImage(icon);
                dlg.setIconImage(icon);
                setIcon = true;
            }
        }

        if (setIcon && dlg.getOwner() instanceof Frame) {
            dlg.getOwner().setIconImage(null);
        }
        return dlg;
    }


    public static BasicMessageBox createWarningMsgBox(String title, BackendErrorException be) {
        if (StringUtils.isEmpty(title)) {
            title = be.getErrorCode();
        }
        StringBuilder text = new StringBuilder(be.getMessage());

        if (StringUtils.isBlank(be.getMessage())) {
            MessageStack messageStack = getMessageStackForBackendEx(be);
            ArrayList<String> userStackList = new ArrayList<>(messageStack.getUserStack());
            for (String userStack : userStackList) {
                text.append(System.getProperty("line.separator")).append(userStack);
            }
            ArrayList<String> devStackList = new ArrayList<>(messageStack.getDevStack());
            log.error(devStackList);
        }
        if (be.isWarning()) {
            return new WarningMsgBox(title, text.toString(), 0);
        } else {
            return new ErrorMsgBox(title, text.toString());
        }
    }


    private static JDialog createDialog(Component parentComponent) {
        return createDialog(parentComponent, true);
    }

    private static void initDialogBody(JDialog dialog, int severity, JPanel content) {
        // ... TitleBar .....................................
        log.debug("severity=" + severity);
        String subText = getResStr("header.subtext");
        Icon headerIcon = getIconBySeverity(severity);
        String headerTitle = getTitleBySeverity(severity);

        Component titlebar = new TitleBar(headerTitle, subText, headerIcon, 0);

        // ... Actions .....................................
        Action cancelAction = new ActionCancel() {// to have 'Escape' working
            public void actionPerformed(ActionEvent e) {
                dialog.setVisible(false);
            }
        };

        Action okAction = new ActionOk() {
            public void actionPerformed(ActionEvent e) {
                dialog.setVisible(false);
            }
        };

        // ... Buttons .....................................
        final JButton okButton = new JButton(okAction);
        JPanel buttonsPanel = ButtonBarFactory.buildOKBar(okButton);

        buttonsPanel.setBorder(Borders.DIALOG_BORDER);

        // ... Content: collect Info, build text

        // ... Put together .................................
        FormLayout layout = new FormLayout("F:max(550dlu;min):G", "P,F:min(200dlu;pref):G,P");
        DefaultFormBuilder builder = DialogUtils.getFormBuilder(layout);
        builder.append(titlebar);

        // if-clause hinzugefuegt von MCD, 07.04.2005
        // @@PEB-2005-05-01@@: wieder weg, das tab is bei informationals nun
        // draussen, ausserdem wuerde bei actionExit was versteckt
        // if (isApplicationExit)
        builder.append(content);
        builder.append(buttonsPanel);
        dialog.getContentPane().add(builder.getPanel());
        // ......................................................

        // Ok/Cancel Actinos per Keyboard. default: ok , Key-Escape: cancel
        dialog.getRootPane().setDefaultButton(okButton);
        ActionUtil.putIntoActionMap(dialog.getRootPane(), new Action[]{cancelAction});

        // ... show and go .........................
        dialog.pack();
        SwingUtilities.invokeLater(okButton::requestFocusInWindow);// after pack()!?
    }

    private static Icon getIconBySeverity(int severity) {
        log.debug("severity=" + severity);
        IconSource iconSource = new IconSource();
        Icon headerIcon = null;
        switch (severity) {
            case BackendException.SEVERITY_INFORMATIONAL:
                headerIcon = iconSource.getHeaderIconByID(BasicMessageBox.ICON_EXCLAMATION);
                break;
            case BackendException.SEVERITY_ACTION_EXIT:
                headerIcon = iconSource.getHeaderIconByID(BasicMessageBox.ICON_STOP);
                break;
            case BackendException.SEVERITY_WARNING:
                headerIcon = iconSource.getHeaderIconByID(BasicMessageBox.ICON_EXCLAMATION);
                break;
            case BackendException.SEVERITY_APP_EXIT:
                headerIcon = iconSource.getHeaderIconByID(BasicMessageBox.ICON_STOP);
                break;
            default:
                log.warn("There is no header icon defined for severity " + severity);
        }
        return headerIcon;
    }

    private static String getTitleBySeverity(int severity) {
        String sTitle = null;
        switch (severity) {
            case BackendException.SEVERITY_INFORMATIONAL:
                sTitle = getResStr("header.title.info");
                break;
            case BackendException.SEVERITY_ACTION_EXIT:
            case BackendException.SEVERITY_APP_EXIT:
                sTitle = getResStr("header.title.error");
                break;
            case BackendException.SEVERITY_WARNING:
                sTitle = getResStr("header.title.warning");
                break;
            default:
                log.warn("there is no title defined for severity " + severity);
        }
        return sTitle;
    }

    private static String getInfoText(int severity) {
        String infoText = null;
        switch (severity) {
            case BackendException.SEVERITY_ACTION_EXIT:
                infoText = getResStr("message.actionExit");
                break;
            case BackendException.SEVERITY_WARNING:
                infoText = getResStr("message.warning");
                break;
            case BackendException.SEVERITY_APP_EXIT:
                infoText = getResStr("message.appExit");
                break;
        }
        return infoText;
    }

    public static Font getFont() {
        return new JLabel().getFont();
    }

    static public void showExceptionDialog(Component parentComponent,
                                           String message,
                                           String extraText,
                                           int severity,
                                           java.util.List<String> userStackList,
                                           java.util.List<String> implStackList,
                                           java.util.List<String> devStackList) {
        JDialog dlg = createDialog(parentComponent);

        String infoText;
        if (severity == BackendException.SEVERITY_INFORMATIONAL) {
            infoText = (extraText == null || extraText.isEmpty()) ? message : extraText + "\n\n";
        } else {
            infoText = getInfoText(severity);
        }
        JPanel content = new JPanel(new BorderLayout());
        JTabbedPane tabbedPane = createPaneForStacks(infoText, userStackList, implStackList, devStackList);
        content.add(tabbedPane);
        content.setBorder(Borders.TABBED_DIALOG_BORDER);
        initDialogBody(dlg, severity, content);

        dlg.setVisible(true);
    }

    private static JPanel createContent(Throwable throwable, int severity, String infoText, String extraText, Font font) {
        JPanel content = new JPanel(new BorderLayout());
        if (throwable instanceof BackendErrorException) {
            BackendErrorException backendErrorException = (BackendErrorException) throwable;
            JTabbedPane tabbedPane = createPaneForBackendException(backendErrorException, infoText);
            content.add(tabbedPane);
        } else if (throwable instanceof MessageStackException && severity != BackendException.SEVERITY_INFORMATIONAL) {
            JTabbedPane tabbedPane = createPaneForBackendException((MessageStackException) throwable, infoText);
            content.add(tabbedPane);
        } else {
            // maybe we use MessageFormat later?
            String text = (extraText == null || extraText.isEmpty()) ? "" : extraText + "\n\n";
            if (throwable != null) {
                StringWriter buffer = new StringWriter();
                throwable.printStackTrace(new PrintWriter(buffer));
                String message = throwable.getLocalizedMessage();
                if (message == null) message = " - ";
                text += getResStr("error.description") + ":\n" + message + "\n\n" + getResStr("error.class") + ":\n" +
                        throwable.getClass().getName() + "\n\n" + getResStr("error.stackTrace") + ":\n" + buffer;
            }

            if (severity == BackendException.SEVERITY_INFORMATIONAL) { // @@PEB-2005-05-01@@
                JTextArea taInfo = createTextArea(infoText, font);
                JScrollPane sp = new JScrollPane(taInfo);
                sp.getViewport().setOpaque(false);
                sp.setOpaque(false);
                sp.setBorder(Borders.EMPTY_BORDER);
                content.add(sp);
            } else {
                JTabbedPane tabs = createPaneForText(text, infoText, font);
                content.add(tabs);
            }
            content.setBorder(Borders.TABBED_DIALOG_BORDER);
        }
        return content;
    }


    private static JTabbedPane createPaneForBackendException(BackendErrorException backendEx, String infoText) {
        MessageStack messageStack = getMessageStackForBackendEx(backendEx);

        ArrayList<String> userStackList = new ArrayList<>(messageStack.getUserStack());
        JTabbedPane tabbedPane = createPaneForStacks(infoText, userStackList, messageStack.getImplStack(), messageStack.getDevStack());
        return tabbedPane;
    }

    private static MessageStack getMessageStackForBackendEx(BackendErrorException backendEx) {
        MessageStack messageStack = backendEx.getErrorRecord().getMessageStack();

        if (messageStack == null) {
            StackTraceElement[] stack = backendEx.getStackTrace();
            StringBuilder devMsg = new StringBuilder();
            for (StackTraceElement aStack : stack) {
                devMsg.append(aStack).append("\n");
            }
            messageStack = new MessageStack(Collections.singletonList(backendEx.getMessage()),
                    Collections.singletonList(backendEx.getErrorCode()), Collections.singletonList(devMsg.toString()));
        }
        return messageStack;
    }

    private static MessageStack messageStackExceptionToMessageStack(MessageStackException messageStackException) {
        java.util.List<MessageException> exceptions = messageStackException.getExceptionList();
        List<String> userStack = new ArrayList<>();
        List<String> implStack = new ArrayList<>();
        List<String> devStack = new ArrayList<>();
        for (MessageException messageException : exceptions) {
            String userMessage = messageException.getUserStack();
            String implMessage = messageException.getImplStack();
            String devMessage = messageException.getDevStack();
            userStack.add(userMessage);
            implStack.add(implMessage);
            devStack.add(devMessage);
        }
        MessageStack messageStack = new MessageStack(userStack, implStack, devStack);
        return messageStack;
    }

    private static JTabbedPane createPaneForBackendException(MessageStackException backendEx, String infoText) {
        MessageStack messageStack = messageStackExceptionToMessageStack(backendEx);
        ArrayList<String> userStackList = new ArrayList<>(messageStack.getUserStack());
        JTabbedPane tabbedPane = createPaneForStacks(infoText, userStackList, messageStack.getImplStack(), messageStack.getDevStack());
        return tabbedPane;
    }

    private static JTabbedPane createPaneForStacks(String infoText, java.util.List<String> userStackList, java.util.List<String> implStackList, java.util.List<String> devStackList) {
        JTabbedPane tabbedPane = new JTabbedPane(JTabbedPane.TOP);
        tabbedPane.addTab("User stack", createTabPane(userStackList, infoText));
        tabbedPane.addTab("Implementer stack", createTabPane(implStackList, null));
        tabbedPane.addTab("Developer stack", createTabPane(devStackList, null));
        return tabbedPane;
    }

    private static JTabbedPane createPaneForText(String text, String infoText, Font font) {
        JTextArea ta = new JTextArea("", 10, 50);

        ta.setEditable(false);
        ta.setFont(font);
        ta.setText(text);
        ta.setCaretPosition(0);
        ta.setMargin(new Insets(4, 4, 4, 4));

        JTextArea taInfo = createTextArea(infoText, font);

        // ... content tabbed pane

        JTabbedPane tabs = new JTabbedPane(JTabbedPane.TOP);
        JScrollPane sp = new JScrollPane(taInfo);
        sp.setOpaque(false);
        sp.getViewport().setOpaque(false);
        sp.setBorder(Borders.EMPTY_BORDER);
        tabs.addTab(getResStr("tab.info.text"), sp); // @@PEB-2005-05-01@@  added JScrollPane ->  buttons stay visible  after sizing the  dialog
        sp = new JScrollPane(ta);
        JPanel inter = new JPanel(new BorderLayout());
        inter.add(sp);
        inter.setBorder(Borders.DIALOG_BORDER);
        tabs.addTab(getResStr("tab.detail.text"), inter);
        return tabs;
    }

    private static JTextArea createTextArea(String infoText, Font font) {
        JTextArea taInfo = new JTextArea();
        taInfo.setEditable(false);
        taInfo.setFont(font);
        taInfo.setText(infoText);

        taInfo.setCaretPosition(0);
        taInfo.setLineWrap(true);
        taInfo.setWrapStyleWord(true);
        taInfo.setOpaque(false);
        taInfo.setBorder(Borders.DIALOG_BORDER);
        return taInfo;
    }

    private static JScrollPane createTabPane(java.util.List<String> textList, String infoText) {
        JTextArea jTextArea = new JTextArea();
        jTextArea.setEditable(false);
        if (StringUtils.isNotEmpty(infoText)) {
            jTextArea.append(infoText);
            jTextArea.append(System.getProperty("line.separator"));
            jTextArea.append(System.getProperty("line.separator"));
            jTextArea.append(System.getProperty("line.separator"));
        }
        int i = 1;
//        log.debug(textList.size());
        for (int j = textList.size(); j > 0; j--) {
            String text = textList.get(j - 1);
//        }
//        for (String text : textList) {
            boolean isTextNotEmpty = text != null && StringUtils.isNotEmpty(text.trim());
            if (isTextNotEmpty) {
                jTextArea.append(i + ". ");
                i++;
            }
            jTextArea.append(text);
            if (isTextNotEmpty) {
                jTextArea.append(System.getProperty("line.separator"));
                jTextArea.append(System.getProperty("line.separator"));
            }
        }
        jTextArea.setLineWrap(true);
        JScrollPane scrollPane = new JScrollPane(jTextArea);
        scrollPane.setPreferredSize(new Dimension(600, 300));

        return scrollPane;
    }

    /**
     * title of topmost parent if that is a Frame/Dialog or null if that one is
     * untitled, the untitled case occures when the Dialog is created without an
     * explicit owner- the runtime then uses a shared invisible untitled frame.
     *
     * @param c
     * @return
     */
    static private String getTopParentTitle(Component c) {

        if (c == null) return null;
        c = c.getParent();
        if (c == null) return null;

        Component parent = c.getParent();
        while (parent != null) {
            c = parent;
            parent = parent.getParent();
        }// now parent==null and c !=null

        String title = null;
        if (parent instanceof Dialog) title = ((Dialog) parent).getTitle();
        else if (parent instanceof Frame) title = ((Frame) parent).getTitle();
        if (title == null || title.length() == 0) return null;

        return title;
    }

    /**
     * String from Resource, key will be prefixed, returns key if resource is
     * not found
     *
     * @param key
     * @return
     */
    private static String getResStr(String key) {

        String s = Res.getString("exceptionDialog." + key);
        return s != null ? s : key;
    }

    public static void main(String[] args) {

        TweakUI.setLookAndFeel();

        try {
            java.util.List<String> userStack = new ArrayList<String>();
            java.util.List<String> implStack = new ArrayList<String>();
            java.util.List<String> devStack = new ArrayList<String>();
            userStack.add("UserStack1");
            userStack.add("UserStack2");
            userStack.add("UserStack3");

            implStack.add("ImplStackImplStackImplStackImplStackImplStackImplStackImplStackImplStackImplStackImplStackImplStackImplStack1");
            implStack.add("ImplStack2");

            devStack.add("Devstack1");
            devStack.add("Devstack2");
            devStack.add("Devstack3");
            //throw new MessageStackException(userStack, implStack, devStack);
            throw new Exception("TEST");
        } catch (Exception e) {
            showExceptionDialog(null, e, null);
        }

        /*try {
            if ( true ) throw new BackendException( BackendException.SEVERITY_ACTION_EXIT );
        } catch ( Exception e ) {
            showExceptionDialog( null, e, null );
        }*/

        //System.exit(0);

    }

}