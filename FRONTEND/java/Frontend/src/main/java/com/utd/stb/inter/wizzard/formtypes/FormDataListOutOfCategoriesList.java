package com.utd.stb.inter.wizzard.formtypes;

import com.utd.stb.inter.items.DisplayableItems;
import com.utd.stb.inter.wizzard.FormData;


/**
 * Much like {@link FormDataListOutOfList}: User may choose many items out of a
 * list of items may sort the out list, but here there are more than one list of
 * available, they are categorized.
 *
 * @author PeterBuettner.de
 */
public interface FormDataListOutOfCategoriesList extends FormData {

    /**
     * A category may have a name and info
     *
     * @return
     */
    DisplayableItems getCategoryList();

    /**
     * Gets the (sub)items of a category
     *
     * @param categoryNumber
     * @return
     */
    DisplayableItems getCategoryItems(int categoryNumber);

    /**
     * The list of selected items, loaded on startup, note that a available
     * items category may not benn loaded, ao if the user deselects one, we have
     * to load the category
     *
     * @param categoryNumber
     * @return
     */
    DisplayableItems getSelectedItems(int categoryNumber);

    /**
     * Set this list to set the users choice, all categories together
     *
     * @param selected
     */
    void setSelectedItems(DisplayableItems selected);

    /**
     * Label for the Source/All-Items component
     *
     * @return
     */
    String getLabelAllItems();

    /**
     * Label for the Destination/Choosen-Items component
     *
     * @return
     */
    String getLabelChoosenItems();

}