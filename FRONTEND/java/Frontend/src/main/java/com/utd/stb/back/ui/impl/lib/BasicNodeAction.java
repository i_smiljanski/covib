package com.utd.stb.back.ui.impl.lib;

import com.utd.stb.back.data.client.TreeNodeRecord;
import com.utd.stb.inter.action.NodeAction;
import com.utd.stb.inter.application.Node;


/**
 * An action object on tree nodes. Action type can be reload, expand, select,
 * select and reload detail and select in detail
 *
 * @author rainer bruns Created 14.12.2004
 */
public class BasicNodeAction implements NodeAction {

    TreeNodeRecord node;
    String token;
    String type;

    /**
     * @param type the action type
     * @param node the node on which to do the action
     */
    public BasicNodeAction(String type, TreeNodeRecord node) {

        this.node = node;
        this.type = type;
    }

    /*
     * (non-Javadoc)
     * 
     * @see com.utd.stb.inter.action.NodeAction#getType()
     */
    public String getType() {

        return type;
    }

    /*
     * (non-Javadoc)
     * 
     * @see com.utd.stb.inter.action.NodeAction#getTargetNode()
     */
    public Node getTargetNode() {

        return node;
    }
}