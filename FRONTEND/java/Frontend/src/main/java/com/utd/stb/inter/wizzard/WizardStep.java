package com.utd.stb.inter.wizzard;

import com.utd.stb.inter.dialog.HelpSource;


/**
 * Container for Data and DisplayInfo of a Step, now also its HelpSource
 *
 * @author PeterBuettner.de
 */
public interface WizardStep {

    /**
     * To create the gui that allows the user to manipulate this entity.
     *
     * @return
     */
    FormData getFormData();

    /**
     * To display info about that step.
     *
     * @return
     */
    FormDisplayInfo getFormDisplayInfo();

    /**
     * The help to display for this step.
     *
     * @return
     */
    HelpSource getHelpSource();

}