package com.utd.stb.gui.userinput.swing;

import javax.swing.*;
import java.text.Format;
import java.text.ParseException;


/**
 * Basic JFormattedTextField that tweaks some properties (remove [Escape] and
 * [Enter] bindings from InputMap and allows null edits ), use always this as a
 * base for other (DateField, NumberField,...)
 *
 * @author PeterBuettner.de
 */
class BasicFormattedField extends JFormattedTextField {

    BasicFormattedField(Format format, Object initialValue) {

        super(format);
        setValue(initialValue);
        setHorizontalAlignment(JTextField.RIGHT);

        // remove the revert and commit actions
        InputMap map = getInputMap();
        map.put(KeyStroke.getKeyStroke("ESCAPE"), "utd-no-action");
        map.put(KeyStroke.getKeyStroke("ENTER"), "utd-no-action");
        // TODOlater check this, we may need to activate a notifyAction for
        // <Enter>

    }

    public void commitEdit() throws ParseException {

        if (getText() == null || getText().length() == 0) // allow nulls
            setValue(null);
        super.commitEdit();
    }
}