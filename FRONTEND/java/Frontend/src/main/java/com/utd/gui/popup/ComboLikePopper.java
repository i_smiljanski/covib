package com.utd.gui.popup;

import javax.swing.*;
import java.awt.*;
import java.awt.event.AWTEventListener;
import java.awt.event.MouseEvent;


/**
 * Manages hide and show, if mouse clicked outside of the content or the
 * filterField, work like combobox: pop on top of filterField if space below is
 * rare.
 *
 * @author PeterBuettner.de
 */
public class ComboLikePopper {

    private Popup popup;
    private JComponent filterField;
    private JComponent content;

    //	 TODOlater remove AWTEventListener ?( is for: unpop on click outside) may
    // not be possible in applets 'cause of security
    // maybe we go back to JPopupMenu or we open an focussed modal undecorated
    // dialog like e.g. eclipse does it
    // or get it's grabContainer code, it's all we need
    AWTEventListener aWTEventListener;

    public ComboLikePopper(JComponent filterField, JComponent content) {

        this.filterField = filterField;
        this.content = content;

    }

    /**
     * work like combobox: pop on top of filterField if space below is rare
     */
    public void show(Component c) {

        content.validate(); // ?? neccessary? to recalc the content size?

        Rectangle r = new Rectangle(content.getPreferredSize());
        r.y = filterField.getHeight() + 2;
        // border is inside, at least for field in Table, maybe different in
        // standalone field
        // Border border = filterField.getBorder();
        // r.y=filterField.getHeight()+
        // border.getBorderInsets(textField).bottom;
        r = PopupTools.computePopupBounds(filterField, r);

        Point p = new Point(r.x, r.y);
        SwingUtilities.convertPointToScreen(p, c); // coords relative to c
        // popup =
        // PopupFactory.getSharedInstance().getPopup(null,content,p.x,p.y);//
        // won't be deactiv on click all the time
        // popup = PopupFactory.getSharedInstance().getPopup(c,content,p.x,p.y);
        // // may produce lightwidth ones

        popup = new MyPopup(c, content, p.x, p.y);// new testing code, since
        // the above didn't worked
        // with JDialog

        popup.show();
        addAWTListener();

    }

    void hide() {

        Toolkit.getDefaultToolkit().removeAWTEventListener(aWTEventListener);
        aWTEventListener = null;
        if (popup != null) popup.hide();
        popup = null;

    }

    boolean visible() {

        return (popup != null);

    }

    private void addAWTListener() { // see outside mouseclicks

        aWTEventListener = new AWTEventListener() {

            public void eventDispatched(AWTEvent event) {

                MouseEvent me = ((MouseEvent) event);

                if (me.getID() != MouseEvent.MOUSE_PRESSED) return;

                Component c = me.getComponent();
                Window root = SwingUtilities.getWindowAncestor(content); // made by popup
                if (SwingUtilities.isDescendingFrom(c, root)) return;
                if (SwingUtilities.isDescendingFrom(c, filterField)) return;

                // without 'later' we get (gray) artefacts on the JTable->JScrollPane->Viewport
                SwingUtilities.invokeLater(new Runnable() {

                    public void run() {

                        hide();
                    }
                });
            }
        };

        Toolkit.getDefaultToolkit().addAWTEventListener(aWTEventListener,
                AWTEvent.MOUSE_EVENT_MASK);
    }

    //	 ------------------------------------------------------------

    private static final class MyPopup extends Popup {

        public MyPopup(Component c, JComponent content, int x, int y) {

            super(c, content, x, y);
        }

    }

}