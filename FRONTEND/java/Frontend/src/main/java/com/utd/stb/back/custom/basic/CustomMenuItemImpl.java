package com.utd.stb.back.custom.basic;

import com.utd.stb.back.data.client.TreeNodeRecord;
import com.utd.stb.inter.action.MenuItem;
import com.utd.stb.inter.action.MenuItems;


/**
 * @author rainer bruns Created 13.12.2004 class that implements the MenuItem
 *         interface. Seperate from standard ISR menu, so we can distinguish
 */
public class CustomMenuItemImpl implements MenuItem {

    boolean seperator = false;
    private MenuItems subItems;
    private String display;
    private String iconId;
    private boolean isMenu;
    private boolean isEnabled;
    private CustomAction action;
    private TreeNodeRecord node;

    /**
     * separator
     */
    public CustomMenuItemImpl() {

        this.seperator = true;
    }

    /**
     * popup
     */
    public CustomMenuItemImpl(MenuItems subItems, String display) {

        this.subItems = subItems;
        this.isEnabled = true;
        this.isMenu = true;
        this.display = display;
    }

    /**
     * item
     */
    public CustomMenuItemImpl(String display, boolean enabled, CustomAction action) {

        this.isEnabled = enabled;
        this.isMenu = false;
        this.display = display;
        this.action = action;
    }

    public String toString() {

        return getClass().getName() + "\tText:" + display;
    }

    public String getText() {

        return display;
    }

    public String getIconId() {
        return iconId;
    }

    public boolean isEnabled() {

        return isEnabled;
    }

    public boolean isSeparator() {

        return seperator;
    }

    public MenuItems getChildren() {

        return subItems;
    }

    public MenuItem getData() {

        return this;
    }

    public Object executeAction(TreeNodeRecord node) {

        return action.execute(node);
    }
}