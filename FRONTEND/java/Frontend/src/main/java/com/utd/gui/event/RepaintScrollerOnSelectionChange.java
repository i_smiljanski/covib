package com.utd.gui.event;

import com.utd.gui.controls.ControlUtils;

import javax.swing.*;
import javax.swing.event.ListSelectionEvent;
import javax.swing.event.ListSelectionListener;
import java.awt.*;


/**
 * A Listener to repaint the scrollbars of the enclosing JScrollPane of a
 * Component that is the View of this Scrollpane. Maybe used to update selection
 * markers in JTable/Tree/List
 *
 * @author PeterBuettner.de
 */
public class RepaintScrollerOnSelectionChange implements ListSelectionListener {

    private Component targetView;
    private boolean alsoAdjusting;

    /**
     * @param targetView    the component lying in a JScrollpane
     * @param horizontal    repaint horizontal Scrollbar
     * @param vertical      repaint vertical Scrollbar
     * @param alsoAdjusting also update if selection is adjusting
     */
    public RepaintScrollerOnSelectionChange(Component targetView, boolean alsoAdjusting) {

        this.targetView = targetView;
        this.alsoAdjusting = alsoAdjusting;

    }

    public void valueChanged(ListSelectionEvent e) {

        if (!alsoAdjusting && e.getValueIsAdjusting()) return;

        JScrollPane sp = ControlUtils.getEnclosingScrollPane(targetView);
        if (sp == null) return;
        sp.getHorizontalScrollBar().repaint();
        sp.getVerticalScrollBar().repaint();

    }

}