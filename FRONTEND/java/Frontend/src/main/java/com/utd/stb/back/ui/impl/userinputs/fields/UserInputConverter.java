package com.utd.stb.back.ui.impl.userinputs.fields;

import com.jidesoft.converter.ConverterContext;
import com.jidesoft.converter.ObjectConverter;
import com.utd.stb.inter.userinput.DateInput;
import com.utd.stb.inter.userinput.TextInput;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

/**
 * @author Inna Smiljanski created 13.04.11
 */
public class UserInputConverter implements ObjectConverter {
    public static final Logger log = LogManager.getRootLogger();
    public static ConverterContext CONTEXT = new ConverterContext("UserInput");

//    public UserInputConverter(Locale locale) {
//        super(locale);
//    }

    public UserInputConverter() {
    }

    public String toString(Object o, ConverterContext converterContext) {
        String s = "";
        if (o instanceof TextUserInput) {
            if (((TextUserInput) o).getData() instanceof TextInput) {
                s = ((TextInput) ((TextUserInput) o).getData()).getText();
//                    log.debug("toString " + o+" text="+s);
            }
        }
        if (o instanceof DateUserInput) {
            if (((DateUserInput) o).getData() instanceof DateInput) {
                s = ((DateInput) ((DateUserInput) o).getData()).getDate().toString();
                log.debug("date: " + s);
            }
        }
        return s;
    }

    public boolean supportToString(Object o, ConverterContext converterContext) {
        return true;
    }

    public Object fromString(String s, ConverterContext converterContext) {
        log.debug("fromString " + s);
        return null;
    }

    public boolean supportFromString(String s, ConverterContext converterContext) {
        return false;
    }
}
