package com.utd.gui.popup;

import javax.swing.*;
import javax.swing.event.TableModelEvent;
import javax.swing.event.TableModelListener;
import javax.swing.table.TableModel;
import java.beans.PropertyChangeEvent;
import java.beans.PropertyChangeListener;


/**
 * UNUSED, BUT MAYBE usefull <br>
 * this is easier:
 * <p>
 * <pre>
 *
 * public void tableChanged( TableModelEvent e ) {
 *
 *     super.tableChanged( e );
 *     if ( getRowCount() == 1 ) setRowSelectionInterval( 0, 0 );
 * }
 * </pre>
 * <p>
 * <br>
 * if the model contains only one row, this is selected in the table, used for
 * filtering purposes
 *
 * @author PeterBuettner.de
 */
public class AutoSelectSoleTableRow implements TableModelListener {

    private final JTable table;

    /**
     * binds the table to the listener, doesn't work if tablemodel changes <br>
     * use: model.addTableModelListener( new AutoSelectSoleTableRow(table)); see
     * the untested static adder in here, that will also handle model changes
     *
     * @param table
     */
    public AutoSelectSoleTableRow(JTable table) {

        this.table = table;
    }

    public void tableChanged(TableModelEvent e) {

        if (e.getType() == TableModelEvent.UPDATE) return; // nothing changed
        if (!table.getModel().equals(e.getSource())) return; // not ours

        SwingUtilities.invokeLater(new Runnable() {

            public void run() {

                if (table.getRowCount() == 1) table.setRowSelectionInterval(0, 0);
            }
        });
    }

    /**
     * observes the tables model, if it changes, ths listener will be
     * transfered. untested, should work
     *
     * @param table
     */
    private static void addAutoSelectSoleTableRow(JTable table) {

        AutoSelectSoleTableRow listener = new AutoSelectSoleTableRow(table);
        table.getModel().addTableModelListener(listener);
        table.putClientProperty(AutoSelectSoleTableRow.class, listener);

        table.addPropertyChangeListener("model", new PropertyChangeListener() {

            public void propertyChange(PropertyChangeEvent e) {

                if (!(e.getSource() instanceof JTable)) return;
                JTable table = (JTable) e.getSource();

                Object listener = table.getClientProperty(AutoSelectSoleTableRow.class);
                if (e.getOldValue() instanceof TableModel
                        && listener instanceof TableModelListener)
                    ((TableModel) e.getOldValue())
                            .removeTableModelListener((TableModelListener) listener);

                AutoSelectSoleTableRow newListener = new AutoSelectSoleTableRow(table);
                ((TableModel) e.getNewValue()).addTableModelListener(newListener);

                table.putClientProperty(AutoSelectSoleTableRow.class, newListener);
            }
        });

    }

}