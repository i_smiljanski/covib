package com.utd.gui.action;

import com.utd.gui.resources.I18N;

import javax.swing.*;


public abstract class ActionNo extends AbstractAction {

    public ActionNo() {

        ActionUtil.initActionFromMap(this, I18N.getActionMap(ActionNo.class.getName()),
                null);
        putValue(SHORT_DESCRIPTION, null);
    }

}