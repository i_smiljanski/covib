package com.utd.gui.controls.table;

import com.uptodata.isr.gui.borders.AlignedDashedBorder;
import com.uptodata.isr.gui.borders.DashedBorder;

import javax.swing.*;
import javax.swing.border.Border;
import javax.swing.border.CompoundBorder;
import javax.swing.border.EmptyBorder;
import java.awt.*;


/**
 * Tools to let JTable looking more like a windows ListView than JTable,
 * <p>
 * <p>
 * To get a renderer only with changed margins/focusBorder call <br>
 * <code>
 * changeDefaultTableCellRenderer(component,isSelected, hasFocus);
 * </code>
 * <br>
 * on your rendering Component (currently isSelected isn't used)
 * </p>
 * <p>
 * For "smaller header": <br>
 * set a special: UIManager.put("TableHeader.cellBorder",
 * getTableHeaderBorder(flat)); where not BasicBorder.ButtonBorder is used
 * (wrong insets there)
 * <p>
 *
 * @author PeterBuettner.de
 */
public class ListViewTools {

    private static int cellBorderHeight = 4;
    private static int cellBorderWidth = 3;
    private static Border standardCellBorder = new EmptyBorder(cellBorderHeight,
            cellBorderWidth,
            cellBorderHeight,
            cellBorderWidth);
    private static Border focusCellBorder;

    public static void makeListViewSized(JTable table) {

        table.setShowGrid(false); // if grid it is invisible if selected
        table.setRowMargin(0); // or we get lines between rows in background
        // color if selected
        table.getColumnModel().setColumnMargin(0); // ugly without too
    }

    /**
     * changes Borders of compo - only if it is a JComponent - to look more like
     * a listview, for a single cell
     *
     * @param compo
     */
    public static void changeDefaultTableCellRenderer(Component compo, boolean hasFocus) {

        if (!(compo instanceof JComponent)) return;
        JComponent c = (JComponent) compo;
        c.setBorder(hasFocus ? getFocusCellBorder() : standardCellBorder);

    }

    /**
     * With full row focus border: needs to know where the cell is: isLeftmost
     * and/or isRightmost <br>
     * changes Borders of compo - only if it is a JComponent - to look more like
     * a listview
     *
     * @param compo
     */
    public static void changeDefaultTableCellRenderer(Component compo, boolean hasFocus,
                                                      boolean isLeftmost, boolean isRightmost) {

        if (!(compo instanceof JComponent)) return;
        JComponent c = (JComponent) compo;
        c.setBorder(hasFocus
                ? getAlignedFocusCellBorder(isLeftmost, isRightmost)
                : standardCellBorder);

    }

    /**
     * With full row focus border, looks by it self where the cell is:
     * isLeftmost and/or isRightmost <br>
     * changes Borders of compo - only if it is a JComponent - to look more like
     * a listview
     *
     * @param compo
     */
    public static void changeDefaultTableCellRenderer(JTable table, Component compo, int row,
                                                      int column) {

        boolean rowIsAnchor = (table.getSelectionModel().getAnchorSelectionIndex() == row);// same
        // as
        // in
        // JTable
        boolean hasFocus = rowIsAnchor && table.isFocusOwner();
        boolean isLeftmost = column == 0;
        boolean isRightmost = column == table.getColumnCount() - 1;
        changeDefaultTableCellRenderer(compo, hasFocus, isLeftmost, isRightmost);

    }

    private static Border getFocusCellBorder() {

        if (focusCellBorder != null) return focusCellBorder;
        // 	UIManager.getBorder("Table.focusCellHighlightBorder"),
        focusCellBorder = new CompoundBorder(
                // 	new
                // DashedBorder(table.getSelectionForeground(),table.getSelectionBackground()),
                new DashedBorder(
                        UIManager
                                .getColor("Table.selectionForeground"),
                        UIManager
                                .getColor("Table.selectionBackground")),
                new EmptyBorder(cellBorderHeight - 1,
                        cellBorderWidth - 1,
                        cellBorderHeight - 1,
                        cellBorderWidth - 1)

        );

        return focusCellBorder;

    }

    private static Border getAlignedFocusCellBorder(boolean left, boolean right) {

        // no cache for now since we have left, center, right and also combined
        // left+right == single column

        return new CompoundBorder(AlignedDashedBorder.getShared(left, right),
                new EmptyBorder(cellBorderHeight - 1, cellBorderWidth - 1,
                        cellBorderHeight - 1, cellBorderWidth - 1));

    }

}