package com.utd.stb.gui.swingApp;

import com.utd.gui.controls.table.ListViewTools;
import com.utd.stb.inter.application.Node;
import resources.IconSource;

import javax.swing.*;
import javax.swing.table.DefaultTableCellRenderer;
import java.awt.*;


/**
 * Renders the Node column in a JTable, with icon
 *
 * @author PeterBuettner.de
 */
public class NodeTableRenderer extends DefaultTableCellRenderer {

    private IconSource iconSource;

    public NodeTableRenderer(IconSource iconSource) {
        this.iconSource = iconSource;
    }

    public Component getTableCellRendererComponent(JTable table, Object value,
                                                   boolean isSelected, boolean hasFocus,
                                                   int row, int column) {

        super.getTableCellRendererComponent(table, value, isSelected, hasFocus, row, column);
        ListViewTools.changeDefaultTableCellRenderer(table, this, row, column);
        if (!(value instanceof Node)) return this;
        Node node = (Node) value;
//        System.out.println("getTableCellRendererComponent : " + node.getTranslation()+" "+node.isShowText());
        if (node.isShowText())
            setText(node.getText());
        else
            setText("");
        setIcon(iconSource.getSmallIconByID(node.getIconId()));
        return this;
    }
}