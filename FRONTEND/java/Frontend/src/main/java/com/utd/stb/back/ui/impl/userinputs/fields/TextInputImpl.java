package com.utd.stb.back.ui.impl.userinputs.fields;

import com.utd.stb.inter.userinput.TextInput;


/**
 * Implementation of TextInput. Holds the String, can be multiline.
 *
 * @author rainer bruns Created 02.12.2004
 */
public class TextInputImpl implements TextInput {

    private boolean multiLineAllowed;
    private boolean secret;
    private Integer maxLen;
    private String data;

    /**
     * @param initialText
     * @param maxLen      -1 for unlimited
     * @param multiLine   accept newlines or not, if secret==true it probably will be
     *                    ignored since guis rarely have a multiline passwort area
     * @param secret      if true a passwortfield will be created
     */
    public TextInputImpl(String initialText, int maxLen, boolean multiLine, boolean secret) {

        this.secret = secret;
        this.multiLineAllowed = multiLine;
        if (maxLen != -1) this.maxLen = new Integer(maxLen);
        setText(initialText);
    }

    /**
     * @param initialText
     * @param maxLen      -1 for unlimited
     * @param multiLine   accept newlines or not
     */
    public TextInputImpl(String initialText, int maxLen, boolean multiLine) {

        this(initialText, maxLen, multiLine, false);
    }

    /**
     * Get a Single line Text UI
     *
     * @param initialText
     * @param maxLen      -1 for unlimited
     */
    public TextInputImpl(String initialText, int maxLen) {

        this(initialText, maxLen, false, false);
    }

    /*
     * (non-Javadoc)
     * 
     * @see com.utd.stb.inter.userinput.TextInput#getTranslation()
     */
    public String getText() {

        return data;
    }

    public void setText(String text) {

        if (maxLen != null && text != null && text.length() > maxLen.intValue())
            throw new IllegalArgumentException("Text to long! Is:" + text.length() + " Max:" + maxLen);
        data = text;
    }

    /*
     * (non-Javadoc)
     * 
     * @see com.utd.stb.inter.userinput.TextInput#getMaxLength()
     */
    public Integer getMaxLength() {

        return maxLen;
    }

    /*
     * (non-Javadoc)
     * 
     * @see com.utd.stb.inter.userinput.TextInput#isMultiLineAllowed()
     */
    public boolean isMultiLineAllowed() {

        return multiLineAllowed;
    }

    /*
     * (non-Javadoc)
     * 
     * @see com.utd.stb.inter.userinput.TextInput#isSecret()
     */
    public boolean isSecret() {

        return secret;
    }
}