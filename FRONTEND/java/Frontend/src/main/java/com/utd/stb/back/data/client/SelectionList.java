package com.utd.stb.back.data.client;

import com.utd.stb.inter.items.DisplayableItem;
import com.utd.stb.inter.items.DisplayableItems;

import java.util.ArrayList;
import java.util.Iterator;


/**
 * Wraps a list of record data of type SelectionRecord
 *
 * @author rainer bruns Created 27.08.2004
 * @see SelectionRecord
 */
public class SelectionList implements DisplayableItems {

    private ArrayList<SelectionRecord> data = new ArrayList<>();
    private Iterator iterator;

    /**
     * Creates an empty Selectionist
     */
    public SelectionList() {
    }

    public ArrayList<SelectionRecord> getData() {
        return data;
    }

    /*
    * public void sort() {
    *
    * int numSorted = 0; // number of values in order int index; // general
    * index SelectionRecord temp;
    *
    * while (numSorted < data.size()) {
    *
    * for ( index = 1; index < data.size() - numSorted; index++ ) {
    *
    * if (((SelectionRecord)data.get(index)).getOrderNum() <
    * ((SelectionRecord)data.get(index - 1)).getOrderNum()) {
    *  // swap temp = (SelectionRecord)data.get(index); data.set(index,
    * data.get(index-1)); data.set(index - 1, temp);
    *  }
    *  }
    *  // at least one more value in place numSorted++;
    *  }
    *  }
    */

    /**
     * adds all records of in to this list
     *
     * @param in the SelectionList to add to this list
     */
    public void addAll(SelectionList in) {

        in.initIterator();
        while (in.hasNext()) {
            data.add(in.getNext());
        }
    }

    /**
     * adds the input record to this list
     *
     * @param sr record to add
     */
    public void add(SelectionRecord sr) {

        data.add(sr);
    }

    /*
     * (non-Javadoc)
     * 
     * @see com.utd.stb.inter.items.DisplayableItems#get(int) returns the record
     *      at index as DisplayableItem
     */
    public DisplayableItem get(int index) {

        return (SelectionRecord) data.get(index);
    }


    /**
     * sets record at given index to input record
     *
     * @param index
     * @param sr    record to set
     */
    public void set(int index, SelectionRecord sr) {

        data.set(index, sr);
    }

    /**
     * sets list iterator to the beginning
     */
    public void initIterator() {

        iterator = data.iterator();
    }

    /**
     * @return true if there is a next record
     */
    public boolean hasNext() {

        return iterator.hasNext();
    }

    /**
     * @return the next SelectionRecord
     */
    public SelectionRecord getNext() {

        return (SelectionRecord) iterator.next();
    }

    /*
     * (non-Javadoc)
     * 
     * @see com.utd.stb.inter.items.DisplayableItems#getSize()
     */
    public int getSize() {

        return data.size();
    }

    /*
     * (non-Javadoc)
     * 
     * @see com.utd.stb.inter.items.DisplayableItems#indexOf(com.utd.stb.inter.items.DisplayableItem)
     */
    public int indexOf(DisplayableItem s) {

        return indexOf(s);
    }

    /**
     * clears all data from list
     */
    public void clear() {

        data = new ArrayList<>();
    }

    ArrayList<String> getColArrayList() {


        ArrayList<String> arrayList = new ArrayList<>();

        initIterator();
        while (hasNext()) {
            SelectionRecord rec = getNext();
            if (rec.getAttributeList() != null) {
                AttributeList attrList = (AttributeList) rec.getAttributeList();
                attrList.initIterator();
                while (attrList.hasNext()) {
                    AttributeRecord attrRec = attrList.getNext();
                    if (attrRec.getPrepresent().equals("V") &&
                            !arrayList.contains(attrRec.getParameter())) {
                        arrayList.add(attrRec.getParameter());
                    }
                }
            }
        }

        return arrayList;
    }

    /**
     * returns the colNames of the AttributeList
     */
    public String getAttributeListColNames() {

        StringBuilder colNames = new StringBuilder();
        for (int i = 0; i < getColArrayList().size(); i++) {
            colNames.append(i != 0 ? "," : "").append(getColArrayList().get(i).toString());
        }
        return colNames.toString();
    }

    public int getAttributeListColCount() {
        return getColArrayList().size();
    }

}