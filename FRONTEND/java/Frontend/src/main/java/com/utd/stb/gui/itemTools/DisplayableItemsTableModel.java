package com.utd.stb.gui.itemTools;


import com.utd.stb.inter.items.DisplayableItems;

import javax.swing.table.AbstractTableModel;


/**
 * Wraps a TableModel around DisplayItems, returns the attribute for col==-1
 *
 * @author PeterBuettner.de
 */
public class DisplayableItemsTableModel extends AbstractTableModel {

    protected DisplayableItems items;
    protected DisplayableItemsTableModelMaker modelMaker;

    public DisplayableItemsTableModel(DisplayableItems items) {

        this.items = items;
        this.modelMaker = new DisplayableItemsTableModelMaker(items);
    }

    public int getColumnCount() {

        return modelMaker.getColumnCount();
    }

    public String getColumnName(int col) {

        return modelMaker.getColumnName(col);
    }

    /**
     * returns the attribute Object for col==-1
     */
    public Object getValueAt(int row, int col) {
//        return items.get(row);
        return modelMaker.getValueAt(items.get(row), col);
    }

    public int getRowCount() {
        return items.getSize();
    }
}