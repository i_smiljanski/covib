package com.utd.stb.back.ui.impl.wizard;

import com.utd.stb.StabberInfo;
import com.utd.stb.back.database.Dispatcher;
import com.utd.stb.back.ui.impl.msgboxes.AskCloseMsgBox;
import com.utd.stb.back.ui.impl.msgboxes.SuccessMsgBox;
import com.utd.stb.inter.action.NodeAction;
import com.utd.stb.inter.action.WindowAction;
import com.utd.stb.inter.application.BackendException;
import com.utd.stb.inter.wizzard.ReportWizard;
import com.utd.stb.inter.wizzard.WizardStep;
import com.utd.stb.inter.wizzard.WizardSteps;
import com.utd.stb.interlib.WindowActionImpl;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;


/**
 * The base of the Report Wizard on data side. Stores the differnt steps,
 * forwards navigation requests from GUI to underlying structure ( this is the
 * Dispatcher directly organizing the logic between DB and here.
 *
 * @author rainer bruns Created 14.09.2004
 */
public class StandardReportWizard implements ReportWizard {

    public static final Logger log = LogManager.getRootLogger();
    Dispatcher dispatcher;
    WizardStep currentStep = null;
    StandardWizardSteps steps;
    int currentIndex = 0;
    int direction;
    boolean navigateMode;
    NodeAction wizardAbortedAction = null;
    boolean wizardAborted = false;
    String nodeDisplay;

    /**
     * Current step on startup is step 1 by default(so in WizardStep with index
     * 0)
     *
     * @param d     the dispatcher
     * @param steps the steps for given type
     * @param i     actually not really necessary. Intended for jumping from menu
     *              into wizard
     * @see StandardWizardSteps
     * @see Dispatcher
     */
    public StandardReportWizard(Dispatcher d,
                                StandardWizardSteps steps,
                                int i,
                                String nodeDisplay) {

        this.dispatcher = d;
        this.steps = steps;
        currentStep = steps.get(i - 1);
        this.nodeDisplay = nodeDisplay;

    }

    /**
     * @return number of steps for the report type
     */
    public int getNumberOfSteps() {

        return steps.getSize();

    }

    /*
     * (non-Javadoc)
     * 
     * @see com.utd.stb.inter.wizzard.ReportWizard#getWizardSteps()
     */
    public WizardSteps getWizardSteps() {

        return steps;

    }

    /*
     * (non-Javadoc) looks up the current step in list of steps. Requests
     * dispatcher to reallly navigate to this step, that means to inform backend
     * about navigation and query the step data
     * 
     * @see com.utd.stb.inter.wizzard.ReportWizard#navigateToWizardStep(com.utd.stb.inter.wizzard.WizardStep)
     */
    public void navigateToWizardStep(WizardStep wizardStep) throws BackendException {

        for (int i = 0; i < steps.getSize(); i++) {
            if (steps.get(i).equals(wizardStep)) {
                StandardWizardStep step = (StandardWizardStep) steps.get(i);
                log.debug("currentIndex=" + currentIndex);
                WizardStep navigateStep = dispatcher.navigate(step);
                if (navigateStep != null){
                    currentStep = navigateStep;
                }
//                currentStep = dispatcher.navigate(step);
                return;
            }
        }
        throw new IllegalArgumentException("Unknown WizardStep");
    }

    /**
     * on finishing the wizard the save dialog is displayed if there are
     * editable fields
     *
     * @throws BackendException
     */
    public void navigateFinish() throws BackendException {

        dispatcher.showSaveDialog();

    }

    /*
     * (non-Javadoc)
     * 
     * @see com.utd.stb.inter.wizzard.ReportWizard#getTitle()
     */
    public String getTitle() {

        return StabberInfo.getApplicationInfo("ISR_NAME")
                + " "
                + nodeDisplay;

    }

    /*
     * (non-Javadoc)
     * 
     * @see com.utd.stb.inter.wizzard.ReportWizard#getCurrentWizardStep()
     */
    public WizardStep getCurrentWizardStep() {

        return currentStep;

    }

    /**
     * @return the index of current step
     */
    public int getIndexOfCurrentStep() {

        return steps.indexOf(currentStep);

    }

    /*
     * (non-Javadoc) Handles the different ops, that occur by user actions, that
     * is closing window/aborting wizard, finishing the wizard.
     * 
     * @see com.utd.stb.inter.wizzard.ReportWizard#doAction(java.lang.Object)
     */
    public Object doAction(Object action) throws BackendException {
        log.info("begin " + action);
        if (action.equals(ReportWizard.NAVIGATE_ABORT)) {

            // user asked to stop
            wizardAborted = true;
            //let user confirm, default after this is closing window
            return new AskCloseMsgBox(dispatcher.getTranslation("msgConfirmTitle"),
                    dispatcher.getTranslation("msgCancelWizardText"),
                    new WindowActionImpl(WindowAction.CLOSE_WINDOW));

        } else if (action.equals(ReportWizard.NAVIGATE_EXIT)) {

            wizardAborted = true;
            return dispatcher.abortWizard();

        } else if (action.equals(ReportWizard.NAVIGATE_FINISH)) {

            // after OutputOption, user wants to save
            wizardAborted = false;
            //show saveDialog if there are editable fields
            return dispatcher.showSaveDialog();

        } else if (action instanceof WindowActionImpl && wizardAborted) {

            // MCD, 02.Feb 2006
            return dispatcher.abortWizard();

        } else if (action instanceof WindowAction) {

            // this is if one of the above happened
            WindowAction winac = (WindowAction) action;
            // actually the only one we know here
            if (winac.getType().equals(WindowAction.WINDOW_CLOSED)) {

                // save happened, all dialogs shown. 
                // Show a nice message and reload tree
                if (!wizardAborted) {

                    return new SuccessMsgBox(dispatcher.getTranslation("msgSuccessTitle"),
                            dispatcher.getTranslation("msgFinishWizardText"),
                            dispatcher.finishWizard(),
                            dispatcher.getDialogTimeout());

                } else {

                    // user really wanted to stop, so clean up
                    return dispatcher.abortWizard();

                }

            }

        }

        return null;

    }

}