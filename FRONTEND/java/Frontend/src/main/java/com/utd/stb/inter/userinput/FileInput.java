package com.utd.stb.inter.userinput;

/**
 * Free or formatted text, no complex data
 *
 * @author PeterBuettner.de
 */
public interface FileInput {

    String getText();

    boolean hasLov();
}