package com.utd.stb.gui.swingApp;

import com.jidesoft.dialog.JideOptionPane;
import com.uptodata.isr.utils.ConvertUtils;
import com.uptodata.isr.gui.lookAndFeel.TweakUI;
import com.utd.stb.StabberInfo;
import org.apache.commons.lang.StringUtils;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import javax.swing.*;
import javax.swing.event.HyperlinkEvent;
import javax.swing.event.HyperlinkListener;
import javax.swing.text.Document;
import javax.swing.text.html.HTMLDocument;
import java.awt.*;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;
import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.URISyntaxException;
import java.net.URL;
import java.util.Properties;

/**
 * The About box, read as html from "/com/utd/stb/gui/aboutContent/index.html"
 * in the classpath, be shure to use only iso-8859-1 chars!
 *
 * @author PeterBuettner.de
 */
public class AboutBox {

    public static final Logger log = LogManager.getRootLogger();
    private static final String PAGE_URL = "/aboutContent/about.html";
    private static final String RELEASE_INFO_URL = "releaseInfo.properties";

    private static final StringBuilder sb = new StringBuilder();

    static void setInfo(String parameter, String value) {
        if (StringUtils.isNotEmpty(value)) {
            if (sb.indexOf("[" + parameter + "]") > 0) {
                sb.replace(sb.indexOf("[" + parameter + "]"), sb.indexOf("[" + parameter + "]") + ("[" + parameter + "]").length(), value);
            }
        }
    }

    void showModal(JFrame parent) {

        final JDialog about = new JDialog(parent, parent.getTitle(), true);
        TweakUI.setDialogDecoration(about);

        about.addWindowListener(new WindowAdapter() {

            public void windowClosing(WindowEvent e) {

                about.setVisible(false);
            }
        });
        // MCD, 02.Mai 2006, hides the icon
        //about.setResizable( false );

        // ... content ...................
        JEditorPane edi;
        try {
            // read in advance since JEditorPane reads an url asynchron but we
            // need the size direct (is easier than wait for edi to be ready)
            URL url = AboutBox.class.getResource(PAGE_URL);
            if (url == null) {
                throw new IOException("Could not load url:" + PAGE_URL);
            }
            BufferedReader br = new BufferedReader(new InputStreamReader(url.openStream(), ConvertUtils.encoding));

            String project = "";
            String projectVersion = "";
            String svnRevision = "";
            String svnBuildDate = "";
            String javaVersion = System.getProperty("java.version");

            try {
                Properties props = loadReleaseInfoProperties();
                project = props.getProperty("project.name");
                projectVersion = props.getProperty("project.version");
                svnRevision = props.getProperty("svn.version");
                svnBuildDate = props.getProperty("builddate");
                javaVersion = props.getProperty("java.version");
            } catch (Exception e) {
                e.printStackTrace();
            }

            String isrName = StabberInfo.getApplicationInfo("ISR_NAME");
            String isrSystem = StabberInfo.getApplicationInfo("ISR_SYSTEM");
            String isrVersion = StabberInfo.getApplicationInfo("ISR_VERSION");
            String isrServicepack = StabberInfo.getApplicationInfo("ISR_SERVICEPACK");
            String isrMaxJarFileVersion = StabberInfo.getApplicationInfo("ISR_MAXJARFILEVERSION");
            String isrModules = StabberInfo.getApplicationInfo("ISR_MODULES");
            String isrJarfiles = StabberInfo.getApplicationInfo("ISR_JARFILES");
            String line;
            while ((line = br.readLine()) != null) {
                setInfo("isr.name", isrName);
                setInfo("isr.system", isrSystem);
                setInfo("isr.version", isrVersion);
                setInfo("isr.servicepack", isrServicepack);
                setInfo("isr.version", isrVersion);
                setInfo("isr.maxjarfileversion", isrMaxJarFileVersion);
                setInfo("isr.modules", isrModules);
                setInfo("isr.jarfiles", isrJarfiles);
                setInfo("project.name", project);
                setInfo("project.version", projectVersion);
                setInfo("svn.revision", svnRevision);
                setInfo("svn.builddate", svnBuildDate);
                setInfo("java.version", javaVersion);
                sb.append(line).append('\n');
            }

            edi = new JEditorPane();
            edi.setContentType("text/html");
            ((HTMLDocument) edi.getDocument()).setBase(url);
//            System.out.println("sb.toString() = " + sb.toString());
            edi.setText(sb.toString());
            edi.setMargin(new Insets(0, 0, 0, 0));
            HyperlinkListener hyperlinkListener = new ActivatedHyperlinkListener(edi);
            edi.addHyperlinkListener(hyperlinkListener);

            edi.setPreferredSize(new Dimension(700, 800));

        } catch (Exception e1) { // error goes into page

            edi = new JEditorPane("text/plain",
                    "Sorry, the about page could not be loaded.\n\n\nReason:\n"
                            + e1.getLocalizedMessage() + "\n\n\n\n\n\n\n\n");
            edi.setPreferredSize(new Dimension(400, 250));

        }
        edi.setEditable(false);

        // read dialog title from html
        String htmlTitle = (String) edi.getDocument().getProperty(Document.TitleProperty);
        if (htmlTitle != null && htmlTitle.length() > 0) {
            about.setTitle(htmlTitle);
        }
//        JDialog.setDefaultLookAndFeelDecorated(true);

        edi.setCaretPosition(0);
        JScrollPane scrollPane = new JScrollPane(edi, JScrollPane.VERTICAL_SCROLLBAR_AS_NEEDED,
                JScrollPane.HORIZONTAL_SCROLLBAR_NEVER);

        JideOptionPane.showMessageDialog(parent, scrollPane, htmlTitle, JideOptionPane.PLAIN_MESSAGE);

    }

    public Properties loadReleaseInfoProperties() {
        return ConvertUtils.loadPropertiesAsResource(RELEASE_INFO_URL);
    }



    /*public static void main(String[] args) {

        // MCD, 02.Mai 2006
        TweakUI.setLookAndFeel();
        JFrame frame = new JFrame();
        TweakUI.setFrameDecoration(frame);
        showModal(frame);
        System.exit(0);
    }*/

    static class ActivatedHyperlinkListener implements HyperlinkListener {

        JEditorPane editorPane;

        public ActivatedHyperlinkListener(JEditorPane editorPane) {
            this.editorPane = editorPane;
        }

        public void hyperlinkUpdate(HyperlinkEvent hyperlinkEvent) {
            HyperlinkEvent.EventType type = hyperlinkEvent.getEventType();
            final URL url = hyperlinkEvent.getURL();
            if (type == HyperlinkEvent.EventType.ENTERED) {
                System.out.println("URL: " + url);
            } else if (type == HyperlinkEvent.EventType.ACTIVATED) {
                System.out.println("Activated");

                Document doc = editorPane.getDocument();
                try {
                    Desktop.getDesktop().browse(url.toURI());
                } catch (IOException ioException) {
                    System.err.println("Error following link, Invalid link");
                    editorPane.setDocument(doc);
                } catch (URISyntaxException e) {
                    System.err.println("Error following link, Invalid link " + e);
                }
            }
        }
    }

}



/*
 * 
 * 
 * Action acClose = new ActionClose(){public void actionPerformed(ActionEvent e)
 * {about.setVisible(false);}};
 * acClose.putValue(Action.ACCELERATOR_KEY,KeyStroke.getKeyStroke("ESCAPE"));
 * ActionUtil.putIntoActionMap(rootPane,acClose);
 * 
 * ButtonBarBuilder builder = new ButtonBarBuilder(); final JButton button = new
 * JButton(acClose); rootPane.setDefaultButton(button); builder.addGlue();
 * builder.addGridded(button); JPanel bBar = builder.getPanel();
 * bBar.setBackground(ed.getBackground());
 *  // ... right panel ...................... JPanel pnl = new JPanel(new
 * BorderLayout()); pnl.add(bBar,BorderLayout.SOUTH); pnl.add(ed);
 * pnl.setBorder(Borders.DIALOG_BORDER);
 *  // ... glue together .........................
 * about.getContentPane().add(pnl); about.getContentPane().add(new
 * JLabel(Res.getII("utd.png")), BorderLayout.WEST);
 * pnl.setBackground(ed.getBackground());
 * 
 * SwingUtilities.invokeLater(new Runnable() {public void run()
 * {button.requestFocusInWindow();}});
 * 
 * about.pack(); about.setLocationRelativeTo(parent); about.setVisible(true);
 * 
 *  
 */
