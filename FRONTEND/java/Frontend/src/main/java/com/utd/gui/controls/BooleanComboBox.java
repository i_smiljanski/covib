package com.utd.gui.controls;

import javax.swing.*;
import java.awt.*;


/**
 * the selected item is 'Boolean.TRUE,Boolean.FALSE' if any other thing see it
 * as 'nothing selected'
 *
 * @author PeterBuettner.de
 */
public class BooleanComboBox extends JComboBox {

    private final static Object NULL = new Object();

    public BooleanComboBox(Boolean initial) {

        super(new Object[]{NULL, Boolean.TRUE, Boolean.FALSE});
        setSelectedItem(initial == null ? NULL : initial);
        setRenderer(new BoolListCellRenderer());
    }

    /**
     * The selected Boolean value or null if nothing selected
     *
     * @return
     */
    public Boolean getSelectedBoolean() {

        Object sel = getSelectedItem();
        if (sel instanceof Boolean) return (Boolean) sel;
        return null;
    }

    static private class BoolListCellRenderer extends DefaultListCellRenderer {

        private JCheckBox checker = new JCheckBox("");

        private BoolListCellRenderer() {

            checker.setBorderPaintedFlat(true);
            checker.setBorderPainted(true);
        }

        public Component getListCellRendererComponent(JList list, Object value, int index,
                                                      boolean isSelected, boolean cellHasFocus) {

            super.getListCellRendererComponent(list, " ", index, isSelected, cellHasFocus);// " "
            // not
            // null,
            // or
            // looks
            // bad
            if (value == null || NULL.equals(value)) return this;

            checker.setBackground(getBackground());
            checker.setForeground(getForeground());
            checker.setBorder(getBorder());
            checker.setSelected(Boolean.TRUE.equals(value));
            return checker;
        }

    }

    public static void main(String[] args) {

        JFrame f = new JFrame("Hi");
        f.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        f.getContentPane().add(new BooleanComboBox(Boolean.TRUE));
        f.pack();
        f.setVisible(true);
    }
}