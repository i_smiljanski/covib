package com.utd.stb.inter.wizzard.formtypes;

import com.utd.stb.inter.items.DisplayableItems;
import com.utd.stb.inter.wizzard.FormData;


/**
 * User may choose many items out of a list of items may sort the out list.
 *
 * @author PeterBuettner.de
 */
public interface FormDataListOutOfList extends FormData {

    /**
     * The list of available items, loaded on startup.
     *
     * @return
     */
    DisplayableItems getAvailableItems();

    /**
     * The list of selected items, loaded on startup.
     *
     * @return
     */
    DisplayableItems getSelectedItems();

    /**
     * Set this list to set the users choice
     *
     * @param selected
     */
    void setSelectedItems(DisplayableItems selected);

    /**
     * Label for the Source/All-Items component
     *
     * @return
     */
    String getLabelAllItems();

    /**
     * Label for the Destination/Choosen-Items component
     *
     * @return
     */
    String getLabelChoosenItems();
}