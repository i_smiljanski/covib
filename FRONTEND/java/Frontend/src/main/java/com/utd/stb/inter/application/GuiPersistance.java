package com.utd.stb.inter.application;

/**
 * @author PeterBuettner.de
 */
public interface GuiPersistance {

    /**
     * The gui may save some of its properties (window sizes, locations; column
     * order/size;...) this is always a small amount of data (&lt;&lt; 1000
     * bytes, but be aware: it maybe 1Meg if someone likes it, so use a blob in
     * the database) we use a byte array.
     * <p>
     * <p>
     * For performance it maybe good to have a cache in the Base implementation
     * that is flushed to the database if the gui idles (second thread), and
     * doesn't always request data from the backend but uses the cached one.
     * <br>
     * The data is not important, so if changed data in the cache is lost 'cause
     * of an exception it doesn't matters.
     * <p>
     * <p>
     * <p>
     * <b>Datatransport with byte[], InputStream or ByteBuffer </b>
     * <ul>
     * <li>byte[] may result in some unnecessary datacopy
     * <li>InputStream was a alternative, but one can't see its size in advance
     * <li>ByteBuffer maybe the best alternate: size is known in advance, data
     * is delivered on request
     * </ul>
     * <p>
     * But since we probably always deal with small amounts of data, byte[] will
     * be allright.
     *
     * @param key     This is a String or any Object the backend delivered to the
     *                gui. <b>For now only String is used </b>. If it is an Object
     *                (e.g. DisplayableItems) the Base has to decide to group those
     *                object-properties (all Aluminium-Packages share their
     *                column-width), so they share their gui-data. If you use this
     *                key (String) as a key in the database prefix it with anything
     *                like 'uk:' to distinguish it from the Base-generated Object
     *                keys. Length of this string is 'not too big' at most some 100
     *                chars
     * @param context the gui may like to distinguish different contexts, e.g.
     *                screen sizes: a window closed on a 800x600 mobile is a bit
     *                small on a 1900x1200 desktop. Length of this string is 'not
     *                too big' at most some 100 chars
     * @param data    The data that should be saved, maybe null to 'delete' old data
     *                (state as never saved)
     */
    public void putGuiPersistenceData(Object key, String context, byte[] data);

    /**
     * Get data saved by {@link #putGuiPersistenceData(Object, String, byte[])}
     *
     * @param key     see: {@link #putGuiPersistenceData(Object, String, byte[])}
     * @param context see: {@link #putGuiPersistenceData(Object, String, byte[])}
     * @return null if no data saved before
     */
    public byte[] getGuiPersistenceData(Object key, String context);
}