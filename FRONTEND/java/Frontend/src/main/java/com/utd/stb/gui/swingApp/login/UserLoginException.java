package com.utd.stb.gui.swingApp.login;

/**
 * only used for the first step userlogin: if the user could not be logged in,
 * not to be used if the server isn't foud or anything more severe, only 'cause
 * of user faults.
 *
 * @author PeterBuettner.de
 */
public class UserLoginException extends Exception {

    // these Strings are used as are (with a prefix) in the resourcebundle for i18n
    // SQLEXCEPTION
    public static final Object USER_OR_PASSWORD = "USER_OR_PASSWORD";
    public static final Object PASSWORD_EXPIRED = "PASSWORD_EXPIRED";
    public static final Object ACCOUNT_LOCKED = "ACCOUNT_LOCKED";
    public static final Object DATABASE_NOT_FOUND = "DATABASE_NOT_FOUND";
    public static final Object IDENTIFIER_MUST_BE_DECLARED = "IDENTIFIER_MUST_BE_DECLARED";
    public static final Object NO_DATA_FOUND = "NO_DATA_FOUND";
    public static final Object ISROWNERCONNECTION = "ISROWNERCONNECTION";

    // FRONT- AND BACKEND-CHECK
    public static final Object NO_AVAILABLE_PORTS = "NO_AVAILABLE_PORTS";
    public static final Object NO_STARTED_LOADER = "NO_STARTED_LOADER";

    // BACKEND-CHECK
    public static final Object ISROWNER_UNKNOWN = "ISROWNER_UNKNOWN";
    public static final Object SYNONYMS_MISSING = "SYNONYMS_MISSING";

    public static final Object ANY_ERROR = "ANY_ERROR";

    private Object type;


    /**
     * Which type of error:
     * <p>
     * <pre>
     *
     *  UserLoginException.USER_OR_PASSWORD
     *  UserLoginException.USER_UNKNOWN
     *  UserLoginException.WRONG_PASSWORD
     *  UserLoginException.PASSWORD_EXPIRED
     *
     * </pre>
     *
     * @param type
     */
    public UserLoginException(Object type) {

        this.type = type;
    }


    /**
     * Which type of error: <br>
     * USER_OR_PASSWORD, USER_UNKNOWN, WRONG_PASSWORD, PASSWORD_EXPIRED
     *
     * @return
     */
    protected Object getType() {

        return type;
    }

    public String getMessage() {

        return "Type:" + getType().toString();
    }

    public String toString() {

        return getMessage();
    }

}