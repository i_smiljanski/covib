package com.utd.gui.event;

import javax.swing.*;
import javax.swing.tree.TreePath;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;


/**
 * A MouseListner firing only on JTrees, on PopupTrigger, and if the
 * pressed-event occured on the same node as the popup-event, this node is
 * selected before doPopup
 *
 * @author PeterBuettner.de
 */

public abstract class TreePopupListener extends MouseAdapter {

    static private final JPopupMenu triggerTester = new JPopupMenu("xxx");
    private Object lastDownNode;

    private void event(MouseEvent e) {

        if (!(e.getComponent() instanceof JTree)) return;
        JTree tree = (JTree) e.getSource();

        TreePath path = tree.getPathForLocation(e.getX(), e.getY());
        if (path == null) return;

        Object node = path.getLastPathComponent();
        if (node == null) return;

        // check for down/up on same node
        if (e.getID() == MouseEvent.MOUSE_PRESSED) lastDownNode = node;

        if (lastDownNode != node) {

            lastDownNode = null; // reset on miss
            return;

        }

        tree.setSelectionPath(path);

        if (!triggerTester.isPopupTrigger(e))

            return;

        doPopUp(node, e);

    }

    public void mousePressed(MouseEvent e) {

        event(e);
    }

    public void mouseReleased(MouseEvent e) {

        event(e);
    }

    /**
     * fires on PopupTrigger, and if the pressed-event occured on the same node
     * as the popup-event, the node is selected before doPopup
     *
     * @param node clicked node
     * @param e    the original MouseEvent
     */
    public abstract void doPopUp(Object node, MouseEvent e);

}