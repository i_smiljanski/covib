package com.utd.stb.back.database;

import com.utd.stb.back.data.client.*;
import com.utd.stb.back.ui.impl.wizard.StandardWizardErrorException;
import com.utd.stb.gui.swingApp.login.UserLoginException;
import com.utd.stb.inter.application.BackendException;

import java.io.File;
import java.io.OutputStream;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Properties;


/**
 * @author rainer Created 04.03.2005
 */
abstract class AbstractDBInterface {

    /**
     * Constructor mit Datenbankverbindung
     */
    protected AbstractDBInterface() {

    }

    /**
     * Retrieves Information for actual form calls
     * stb$gal.GetCurrentFormInfo(STB$FormInfo$Record out, STB$Entity$List out)
     *
     * @param nextNavigationMask
     * @return ArrayList
     */

    protected abstract FormInfoRecord getCurrentFormInfo(ArrayList<EntityRecord> entities, int nextNavigationMask)
            throws BackendException;

    /**
     * * Gets the List of Available Data for the entity. Parameter masterKey is
     * used in Grid ( the rowid ) and in Multiselect (category). calls
     * stb$gal.getsList(varchar2 in, varchar2 in, varchar2 in, varchar2 out,
     * STB$SELECTION$LIST out) or stb$gal.getGridSelection(varchar2 in, varchar2
     * in, varchar2 in, varchar2 out, STB$SELECTION$LIST out)
     *
     * @param entity
     * @param masterKey
     * @param navigate
     * @param nextNavigationMask
     * @param grid
     * @return
     * @throws BackendException
     * @throws StandardWizardErrorException
     */
    protected abstract SelectionList getList(String entity,
                                             String masterKey,
                                             boolean navigate,
                                             int nextNavigationMask,
                                             boolean grid)
            throws BackendException, StandardWizardErrorException;

    /**
     * Gets the list of available items for a grid cell. Entity is the column
     * header, rowid is the rowid in grid. calls stb$gal.getLov(varchar2 in,
     * varchar2 in, STB$SELECTION$LIST out)
     *
     * @param entity the entity for which list is requested ( column header)
     * @param rowid  the row header
     * @return the List of available Items
     * @throws BackendException
     * @see com.utd.stb.back.ui.impl.wizard.formdata.Grid
     */
    protected abstract SelectionList getLov(String entity, String rowid, boolean adminMode)
            throws BackendException;

    /**
     * called after a change in a grid cell. Determines all dependend entities
     * for parameter entity values for those entities in the actual row are set
     * to null calls stb$gal.getGridDependency(varchar2 in, STB$ENTITY$LIST out,
     * varchar2 in)
     *
     * @param entity the entity ( column header )
     * @param rowid  the row header
     * @return a list of Entities which are dependend on parameter entity
     * @throws BackendException
     * @see com.utd.stb.back.ui.impl.wizard.formdata.Grid
     */
    protected abstract EntityList getGridDependency(String entity, String rowid)
            throws BackendException;

    /**
     * on initialization of MultiSelect Form the list of categories is queried
     * calls stb$gal.initList(varchar2 in, STB$TLRSELECTION$LIST out)
     *
     * @param master - the master entity for which to retrieve the list of categories
     * @return the list of categories
     * @throws BackendException
     * @see com.utd.stb.back.ui.impl.wizard.formdata.MultiSelect
     * @see com.utd.stb.back.database.Dispatcher
     */
    protected abstract SelectionList initMasterList(String master) throws BackendException;

    /**
     * used in report wizards formdata to save data calls
     * stb$gal.putsList(varchar2 in, varchar2 in, STB$TLRSELECTION$LIST in)
     *
     * @param entity the entity to save
     * @param data   the list of data to save
     * @throws BackendException
     */
    protected abstract void putList(String entity,
                                    SelectionList data,
                                    boolean grid)
            throws BackendException;

    /**
     * retrieves the list of output options for the current report calls
     * stb$gal.getOutputOption(STB$PROPERTY$LIST out)
     *
     * @param whatList an Identifier for the listtype, OUTPUT Option only actually
     * @return a list of properties
     * @throws BackendException
     * @see com.utd.stb.back.data.client.PropertyRecord
     */
    protected abstract PropertyList getPropertyList(String whatList) throws BackendException;

    /**
     * used in ReportWizard to get the fieds for audit and save dialog. It is a
     * TreeNodelist, however we only need the propertylist of first node. Dont
     * know why we get this. calls stb$gal.getSaveDialog(STB$TREENODE$LIST out)
     * if whatList is SAVEDIALOG or stb$gal.GetAuditMask(STB$TREENODE$LIST out)
     * if whatList is AUDITDIALOG
     *
     * @param whatList SAVEDIALOG or AUDITDIALOG
     * @return a list of Treenodes, actually only one (??)
     * @throws BackendException
     */
    protected abstract TreeNodeList getTreeNodeList(String whatList) throws BackendException;

    protected abstract void putTable(TreeNodeList nodes, boolean adminMode) throws BackendException;

    /**
     * calls stb$gal.getTimeout(number out)
     *
     * @return the timeout in minutes
     * @throws BackendException
     */
    protected abstract int getTimeout() throws BackendException;

    /**
     * calls stb$gal.getCurrentLanguage
     *
     * @return the user language 1: German 2: Englisch
     * @throws BackendException
     */
    protected abstract int getLanguage() throws BackendException;

    /**
     * calls utd$msglib.getMsg(varchar2 in, number in)
     *
     * @param message  the string to translate
     * @param language the user language
     * @return the translated string
     */
    protected abstract String getTranslation(String message, int language, String... params) throws BackendException;


    /**
     * returns wether a menuitem is enabled or not. If errors in backend returns
     * false. calls stb$gal.setMenuAccess(varchar2 in, varchar2 out)
     *
     * @param parameter the menu identifier
     * @return boolean
     * @throws BackendException on unhandled SQLErrors
     */
    protected abstract boolean getItemAccess(String parameter) throws BackendException;

    /**
     * sets the selected node in backend. On select and menu request calls
     * stb$gal.setCurrentNode(STB$TRENODE$RECORD)
     *
     * @param node the selected TreeNode
     * @throws BackendException
     */
    protected abstract void setCurrentNode(TreeNodeRecord node, boolean left) throws BackendException;

    /**
     * Gets the childs of input parameter treeNode. Since there are different
     * functions in Backend for tree childs and detail childs parameter
     * forChilds is passed. If true tree childs are quried, else node details.
     * calls stb$gal.getNodeChilds(STB$TREENODE$RECORD in, STB$TREENODE$LIST
     * out) for childs of node or stb$gal.getNodeList(STB$TREENODE$RECORD in,
     * STB$TREENODE$LIST out) for details of node.
     *
     * @param treeNode
     * @param forChilds
     * @return the list of childs
     * @throws BackendException
     */
    protected abstract TreeNodeList getNodeChilds(TreeNodeRecord treeNode, boolean forChilds)
            throws BackendException;

    /**
     * gets the menu items for given menuNumber. If 0 its the main menu calls
     * stb$gal.getContextMenu(number in, STB$MENUENTRY$LIST out)
     *
     * @param menuNumber
     * @return list of menu items
     * @throws BackendException
     */
    protected abstract MenuEntryList getContextMenu(int menuNumber)
            throws BackendException;

    /**
     * For the given menuEntry retrieves list of Nodes. If nodes size is 1, a
     * Dialog is opened with the nodes properties, else properties are displayed
     * in detail window of tree calls stb$gal.callFunction(STB$MenuEntry$Record
     * in, STB$TREENODE$LIST out)
     *
     * @param menuEntry
     * @return list of Treenodes
     * @throws NullPointerException
     * @throws BackendException
     */
    protected abstract Object callFunction(MenuEntryRecord menuEntry, boolean adminMode)
            throws NullPointerException, BackendException;

    /**
     * ???
     *
     * @param menuEntryRecord
     * @param propertyList
     * @return list of Treenodes
     * @throws BackendException
     */
    protected abstract PropertyList checkDependenciesOnMask(MenuEntryRecord menuEntryRecord,
                                                            PropertyList propertyList,
                                                            boolean adminMode)
            throws BackendException;

    /**
     * * saves the list of data by either calling
     * stb$gal.putOutputOption(STB$PROPERTY$LIST in) for Report output options
     * stb$gal.putSaveDialog(STB$PROPERTY$LIST in) for saving the Save dialog
     * stb$gal.putAuditMask(STB$PROPERTY$LIST in) for Audit information
     *
     * @param theList list of data to save
     * @param action  SAVEDIALOG or AUDITDIALOG
     * @throws BackendErrorException
     */
    protected abstract void putPropertyList(PropertyList theList, String clobParameter, Object action, boolean adminMode)
            throws BackendException, NullPointerException;

    /**
     * Called after an action is completed. Determines which TreeNodes to expand
     * and Children to reload (nodesToLoad) and which node to select ( newNode ).
     * Return value token is RIGHT if node selection should be in details view.
     * calls stb$gal.synchronize(STB$TREENODE$LIST out, STB$TREENODE$RECORD out)
     *
     * @param nodesToLoad
     * @param newNode
     * @return
     * @throws NullPointerException
     * @throws BackendException
     */
    protected abstract String synchronize(TreeNodeList nodesToLoad, TreeNodeRecord newNode, boolean adminMode)
            throws NullPointerException, BackendException;

    /**
     * Cleans up database after report wizard was canceled calls
     * stb$gal.cancelWizard(STB$TREENODE$RECORD out)
     *
     * @return
     * @throws BackendException
     */
    protected abstract TreeNodeRecord cleanUp() throws BackendException;

    /**
     * saves the persistance data as Clob to db calls
     * stb$gal.setGuiPersistance(Clob in)
     *
     * @param persistance persistance data from GUI
     * @throws BackendException
     */
    protected abstract void putPersistance(Properties persistance) throws BackendException;

    /**
     * calls stb$gal.getGuiPersistance(Clob out)
     *
     * @return a Properties Object with GUI Persistance data
     * @throws BackendException
     */
    protected abstract Properties getPersistance() throws BackendException;

    /**
     * @return actual time of database
     * @throws BackendException
     */
    protected abstract String getDBTime() throws BackendException;

    /**
     * gets the MenuEntryRecord related to this token. Needed for default
     * actions. calls stb$gal.getMenu(varchar2 in, STB$MenuEntry$Record out)
     *
     * @param token
     * @return
     * @throws BackendException
     */
    protected abstract MenuEntryRecord getMenu(String token, boolean adminMode) throws BackendException;

    /**
     * called when a dialog was canceled calls stb$gal.cancelMask
     *
     * @return
     */
    protected abstract boolean cancel(boolean adminMode);


    /**
     * called on ISR exit calls stb$gal.destroyOldValues
     *
     * @return
     */
    protected abstract boolean finalise(int reason);

    /**
     * The initial node of the tree, constructed here. Must be Type TOPNODE with
     * package stb$gal
     *
     * @return a TreeNodeRecord
     */
    protected abstract TreeNodeRecord getInitialNode();


    protected abstract String getParameterValue(String paramter);

    protected abstract byte[] getIcon(String size, String id);

    protected abstract void checkIsValidOwner() throws SQLException, UserLoginException, Exception;

    // Installer
    protected abstract boolean checkRole(String sRole);

    protected abstract boolean preInstall(String sPackage, String jarFileName, String jarFileVersion) throws BackendException;

    protected abstract boolean preInstallFile(String object) throws BackendException;

    protected abstract boolean executeCheck(String sFunction) throws BackendException;

    protected abstract boolean loadFile(String sPackage, File loadFile, String type, String objectType, String objectDescription, String objectVersion, String jarFileName, String jarFileVersion) throws BackendException;

    //    protected abstract boolean loadJava(String sPackage, int fileId, File loadFile, String connectionString) throws BackendException;
    protected abstract boolean installFile(String sPackage, int fileId, String table, String column, String row, String rowValue) throws BackendException;

    protected abstract boolean executeFile(String sPackage, int fileId) throws BackendException;

    protected abstract boolean postInstall(String sPackage, String jarFileName, String jarFileVersion) throws BackendException;

    protected abstract int getCurrentFileId(String sPackage, String fileName, String type, String objectType, String objectDescription, String objectVersion, String jarFileName, String jarFileVersion) throws BackendException;

    protected abstract HashMap getError(int fileId);

    protected abstract void setError(String sPackage, String errorCode, String errorMessage, String errorStatement, String exceptionHandling, int entryid, int referenceid);

    protected abstract void setSuccessFlag(String sPackage, int fileId, String success);

}