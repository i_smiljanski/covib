package com.utd.stb.back.ui.impl.userinputs.fields;

import com.utd.stb.inter.userinput.UserInput;

import java.util.List;


/**
 * Abstract definitions of methods that have to be implemented by various types
 * of userinputs
 *
 * @author rainer bruns Created 17.12.2004
 */
abstract class AbstractUserInput implements UserInput {

    private String label;
    private boolean emptyAllowed = true;
    private boolean readOnly = false;
    private boolean visible = true;
    private boolean focus = false;
    private boolean changeable = false;

    /**
     * @param label Label of field
     */
    public AbstractUserInput(String label) {

        this.label = label;
    }

    /*
     * (non-Javadoc)
     * 
     * @see com.utd.stb.inter.userinput.UserInput#getLabel()
     */
    public String getLabel() {

        return label;
    }

    /*
     * (non-Javadoc)
     * 
     * @see com.utd.stb.inter.userinput.UserInput#isEmptyAllowed()
     */
    public boolean isEmptyAllowed() {

        return emptyAllowed;
    }


    /*
     * (non-Javadoc)
     * 
     * @see com.utd.stb.inter.userinput.UserInput#isReadOnly()
     */
    public boolean isReadOnly() {

        return readOnly;
    }

    public boolean isChangeable() {
        return changeable;
    }

    /*
         * (non-Javadoc)
         *
         * @see com.utd.stb.inter.userinput.UserInput#isVisible()
         */
    public boolean isVisible() {

        return visible;
    }

    /*
     * (non-Javadoc)
     * 
     * @see com.utd.stb.inter.userinput.UserInput#hasFocus()
     */
    public boolean hasFocus() {
        return focus;
    }

    /*
     * (non-Javadoc)
     * 
     * @see com.utd.stb.inter.userinput.UserInput#getExtra(java.lang.String)
     */
    public Object getExtra(String key) {

        return null;
    }

    /**
     * @return true if only data from List are allowed
     */
    public boolean isDataListOnlyValid() {

        return false;
    }

    /**
     * @return List of values for field if field has List(this is if
     * PropertyRecord.hasLov is true)
     * @see com.utd.stb.back.data.client.PropertyRecord#hasLov()
     */
    public List getDataList() {

        return null;
    }

    /**
     * @param emptyAllowed true if field can be empty
     * @see com.utd.stb.back.data.client.PropertyRecord#isEmptyAllowed()
     */
    public void setEmptyAllowed(boolean emptyAllowed) {
        this.emptyAllowed = emptyAllowed;
    }


    /**
     * @param label Label of field
     */
    public void setLabel(String label) {
        this.label = label;
    }

    /**
     * @param readOnly true if field cannot be edited
     * @see com.utd.stb.back.data.client.PropertyRecord#isEmptyAllowed()
     */
    public void setReadOnly(boolean readOnly) {
        this.readOnly = readOnly;
    }

    public void setChangeable(boolean changeable) {
        this.changeable = changeable;
    }

    /**
     * @param visible true if field is displayed
     * @see com.utd.stb.back.data.client.PropertyRecord#isDisplayed()
     */
    public void setVisible(boolean visible) {
        this.visible = visible;
    }
    // TODO Idee: originalwert merken, methode 'undoEdit', so kann die Basis
    // leicht zurueck zum alten.

    /**
     * @param focus true if the field has the focus
     * @see com.utd.stb.back.data.client.PropertyRecord#getFocus()
     */
    public void setFocus(boolean focus) {
        this.focus = focus;
    }
}