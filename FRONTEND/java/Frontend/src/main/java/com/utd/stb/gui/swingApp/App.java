package com.utd.stb.gui.swingApp;

import com.jidesoft.comparator.ObjectComparatorManager;
import com.jidesoft.converter.ObjectConverterManager;
import com.jidesoft.dialog.JideOptionPane;
import com.jidesoft.filter.FilterFactoryManager;
import com.jidesoft.grid.*;
import com.jidesoft.grouper.ObjectGrouperManager;
import com.jidesoft.status.StatusBar;
import com.uptodata.isr.gui.lookAndFeel.TweakUI;
import com.uptodata.isr.loader.webservice.LoaderGui;
import com.uptodata.isr.loader.webservice.LoaderWebserviceImpl;
import com.uptodata.isr.server.utils.fileSystem.FileSystemWorker;
import com.uptodata.isr.utils.exceptions.MessageStackException;
import com.utd.gui.action.ActionHelp;
import com.utd.gui.action.ActionUtil;
import com.utd.gui.action.ActionWrap;
import com.utd.gui.controls.BorderSplitPane;
import com.utd.gui.controls.table.DetailsTableComponentFactory;
import com.utd.gui.controls.table.ListViewTable;
import com.utd.gui.controls.table.TableTools;
import com.utd.gui.controls.tree.WrappingTreeModel;
import com.utd.gui.event.*;
import com.utd.gui.resources.I18N;
import com.utd.stb.Stabber;
import com.utd.stb.back.data.client.TreeNodeRecord;
import com.utd.stb.back.data.client.ValueList;
import com.utd.stb.back.data.client.ValueRecord;
import com.utd.stb.back.ui.impl.userinputs.fields.UserInputComparator;
import com.utd.stb.back.ui.impl.userinputs.fields.UserInputConverter;
import com.utd.stb.back.ui.impl.userinputs.fields.UserInputFilterFactoryManager;
import com.utd.stb.back.ui.impl.userinputs.fields.UserInputGrouper;
import com.utd.stb.gui.help.Help;
import com.utd.stb.gui.ribbon.RibbonMenu;
import com.utd.stb.gui.swingApp.exceptions.ExceptionDialog;
import com.utd.stb.inter.action.MenuItem;
import com.utd.stb.inter.action.*;
import com.utd.stb.inter.application.Node;
import com.utd.stb.inter.application.*;
import com.utd.stb.inter.dialog.UserDialog;
import com.utd.stb.inter.userinput.UserInput;
import com.utd.stb.inter.userinput.UserInputs;
import com.utd.stb.inter.wizzard.ReportWizard;
import com.utd.stb.interlib.BasicMessageBox;
import com.utd.stb.interlib.NodeActionImpl;
import org.apache.commons.lang.StringUtils;
import org.apache.commons.lang.math.NumberUtils;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.pushingpixels.flamingo.api.ribbon.JRibbon;
import resources.IconSource;
import resources.Res;

import javax.swing.Timer;
import javax.swing.*;
import javax.swing.border.EmptyBorder;
import javax.swing.table.TableModel;
import javax.swing.tree.DefaultMutableTreeNode;
import javax.swing.tree.TreePath;
import javax.swing.tree.TreeSelectionModel;
import javax.xml.xpath.XPathExpressionException;
import java.awt.*;
import java.awt.event.*;
import java.io.File;
import java.lang.reflect.Field;
import java.lang.reflect.Method;
import java.util.List;
import java.util.*;

/**
 * The main stabber window.
 * <p>
 * Constructor maybe called in non-swing thread, show() only in swing thread
 *
 * @author PeterBuettner.de
 */

/*
 * There are 3 Sections in here: creation, selection changes and data display,
 * action handling
 *
 *
 *
 */
@SuppressWarnings("UnusedAssignment")
public class App {

    public static final Logger log = LogManager.getRootLogger();
    private static final String PERSIST = "main.frame";
    private SplitWindowState options = new SplitWindowState();

    /*
     * Note:
     *
     * don't create any (J)Components in the constructor or initializer, since
     * this instance is probably created out of the Swing thread
     *
     */
    private JFrame appFrame;
    private JSplitPane splitMain;

    /**
     * our data source
     */
    private final Base base;
    private final ApplicationInfo appInfo;
    private final GuiPersistance guiPersistance;

    private RibbonMenu ribbonMenu;
    private JRibbon ribbon;
    private boolean ribbonIsDisplayed;

    private Action acExit;
    private Action acEditSelection;
    private Action acHelp;
    private Action acAbout;

    /**
     * set the current node in the tree as unloaded, refresh the details, if
     * none is selected reload all ->acUnloadTreeAll
     */
    private Action acUnloadTreeBelow;

    /**
     * set the root node in the tree as unloaded, refresh the details
     */
    private Action acUnloadTreeAll;
    private Action acChangeConnection;

    private JTree tree;
    private WrappingTreeModel treeModel;

    /**
     * (alternate) tableviews may have some extra text
     */
    private JLabel tableLabel;
    private JTable table;

    private IconSource iconSource;
    private StatusBar statusBar;
    private StatusBarThread statusBarThread;

    Enumeration<TreePath> expanded;

    // --- Busy indicator ---------------------------------------
    /**
     * if 0 then default; if >0 then wait, refine the use later
     */
    private int cursorCounter;

    //sortable table values
    private String sortingOrders;
    private String tablePref;
    private List<IFilterableTableModel.FilterItem> filters;
    private boolean isReload;

//    boolean isFrameActive;
//    private AppHelper appHelper;
    /**
     * this can be initialized soonest in createTree *
     */
    private DeferredTreeSelectionListener deferredTreeSelectionListener;

    LoaderWebserviceImpl loader;

    Object globalAction;

    static boolean workingOnTreeUpdate = false;
    static boolean tableDoubleClick = false;
    static int selectedNodeHash;

    private void startLongAction() {
        cursorCounter++;
        if (appFrame == null) {
            return;
        }
        setCursor();
    }

    private void stopLongAction() {

        cursorCounter--;
        if (appFrame == null) {
            return;
        }
        setCursor();
    }

    private void setCursor() {

        if (cursorCounter <= 0) {
            appFrame.setCursor(Cursor.getDefaultCursor());
        } else {
            appFrame.setCursor(Cursor.getPredefinedCursor(Cursor.WAIT_CURSOR));
        }
    }

    /**
     * maybe called in non swing thread, don't shows anything
     *
     * @param base
     */
    public App(Base base) {
        log.debug("begin");

        // wrap to see all invocations
        // this.base = new BaseWrapper(base);
        PrePostProxy proxy = new PrePostProxy(base) {

            long duration;

            protected void pre(Method method) {

                if (!method.getName().endsWith("InBackground")) {
                    duration = System.currentTimeMillis();
                    startLongAction();
                }
            }

            protected void post(Method method) {

                if (!method.getName().endsWith("InBackground")) {
                    duration = System.currentTimeMillis() - duration;
//                    log.info(method.getName() + " duration " + duration);
                    stopLongAction();
                }
            }
        };
        this.base = (Base) PrePostProxy.wrap(Base.class, proxy);

        appInfo = this.base.getApplicationInfo();

        setupLanguage(appInfo.getUserLanguage());

        guiPersistance = this.base.getGuiPersistance();
        options = PersistTool.read(guiPersistance, PERSIST);

        iconSource = new IconSource();

        // .....
        treeModel = new WrappingTreeModel(new BaseNodeSource(base));
        // now load first level:
        treeModel.getChildCount(treeModel.getRoot());
        log.info("Application created, first tree level loaded");

    }


    private void checkFileForUpdate(String fileName) {
        String isNewMainXml = appInfo.isNewMainXml();
        log.debug(isNewMainXml);
        if (Boolean.parseBoolean(isNewMainXml)) {
          /*  String[] options = {
                    Res.getString("dialog.update.available.now"),
                    Res.getString("dialog.update.available.later")
            };
            int result = JideOptionPane.showOptionDialog(appFrame, Res.getString("dialog.update.available.question", fileName),
                    Res.getString("dialog.update.available.title"), JOptionPane.DEFAULT_OPTION, JOptionPane.QUESTION_MESSAGE, null,
                    options, options[0]);
            log.debug(result);
            if (result == JOptionPane.YES_OPTION) {
                String ret = downloadFrontendFile(fileName);
                if ("T".equals(ret)) {
                    JideOptionPane.showMessageDialog(appFrame, Res.getString("dialog.update.result.success"), Res.getString("dialog.update.result.title"), JideOptionPane.PLAIN_MESSAGE);
                }

            }*/
            UserDialog askExit = new YesNoMessageBox(Res.getString("dialog.update.available.title"),
                    Res.getString("dialog.update.available.question", fileName));
            if (UserDialogTool.executeUserDialog(appFrame, askExit).equals(Boolean.TRUE)) {
                String ret = downloadFrontendFile(fileName);

                if ("T".equals(ret)) {
                    JideOptionPane.showMessageDialog(appFrame, Res.getString("dialog.update.result.success"),
                            Res.getString("dialog.update.result.title"), JideOptionPane.PLAIN_MESSAGE);
                }
            }
        }
    }


    private String downloadFrontendFile(String fileName) {
        log.debug(loader);
        String ret = null;
        try {
            ret = loader.loadFileToClient(new File(System.getProperty("user.dir")), fileName, "FRONTEND_CLOB_FILE", log.getLevel().toString(), "frontend");
        } catch (MessageStackException e) {
            ExceptionDialog.showExceptionDialog(appFrame, e, null, false, 0);
        }
        log.debug(ret);
        return ret;

    }

    /**
     * show it for the first time, never call this twice, it will not have any
     * effect call only in the Swing Thread.
     */
    public void show() {
        log.debug("begin");
        if (appFrame != null) {
            return;// already called this routine
        }
        // global support for GuiPersistance
        UIManager.put(com.utd.stb.inter.application.GuiPersistance.class, guiPersistance);

        appFrame = new JFrame(appInfo.getApplicationTitle());
        TweakUI.setFrameDecoration(appFrame);
        appFrame.setIconImage(iconSource.getSmallImageByID("frameMainApp"));
        appFrame.setDefaultCloseOperation(JFrame.DO_NOTHING_ON_CLOSE);//@@PEB-2005-05-01@@

        // : ask
        // before
        // close
        appFrame.addWindowListener(new WindowAdapter() {

            public void windowClosing(WindowEvent e) {
                applicationExitProperly(true, false, appFrame.isActive());
            }

            @Override
            public void windowDeactivated(WindowEvent e) {
//                log.debug("windowDeactivated");
                super.windowDeactivated(e);
            }

            @Override
            public void windowActivated(WindowEvent e) {
//                log.debug("windowActivated ");
                super.windowActivated(e);
            }
        });
        options.getWindowState().putWindowStateListener(appFrame);

        // ... main components ...................................
        createTree();
        createTable();

        FocusListener fl = new FocusListener() {

            public void focusGained(FocusEvent e) {

                if (!e.isTemporary()) {
                    updateEditMenu();
                }
            }

            public void focusLost(FocusEvent e) {

                if (!e.isTemporary()) {
                    updateEditMenu();
                }
            }
        };
        tree.addFocusListener(fl);
        table.addFocusListener(fl);

        // ... some controllers, tree/table needs created
        // .....................................
        createActions();
        if (!Stabber.isRibbons) {
            appFrame.setJMenuBar(createMenuBar());
        }

        // ... detailPanel .....................................
        final JPanel detailPanel = new JPanel(new BorderLayout());
        tableLabel = new JLabel();
        tableLabel.setVisible(false);
        detailPanel.add(tableLabel, BorderLayout.NORTH);
        tableLabel.setBorder(new EmptyBorder(4, 4, 4, 4));
        JScrollPane spTable = new JScrollPane(table);
        detailPanel.add(spTable);

        UIManager.put("ClassLoader", App.class.getClassLoader());

        // ... glue together .............................................
        JPanel main = new JPanel(new BorderLayout());

        // ... Ribbon... ...................................
        if (Stabber.isRibbons) {
            JRibbon jRibbon = createRibbonMenu();
            ribbonIsDisplayed = (jRibbon != null);
            if (ribbonIsDisplayed) {
                main.add(jRibbon, BorderLayout.NORTH);
            }
        }

        // tree and detail window
        splitMain = new BorderSplitPane(new JScrollPane(tree), detailPanel, true);
        main.add(splitMain);

        // ... Statusbar ...................................
        try {
            loader = base.getLoader();
            LoaderGui loaderGui = loader.getLoaderGui();
            loaderGui.setApp(this);
            statusBar = loaderGui.getStatusBar();
        } catch (NullPointerException npe) {
            log.error(npe);
        }

        if (statusBar != null) {
            main.add(statusBar, BorderLayout.SOUTH);
        }

        appFrame.getContentPane().add(main);
        // ........................................

        splitMain.setDividerLocation(options.getDividerPos());
        options.getWindowState().restore(appFrame);

        appFrame.setVisible(true);
        tree.setModel(treeModel);

        // after all is made visible
        updateEditMenu();
        TimeoutManager.install();
        TimeoutManager.addSizeMoveListener(appFrame);
        startTimeOutWatcher();

        // do startup actions
        processAllTasks();

        checkFileForUpdate("main.xml");

        log.debug("end");
    }

    public DetailView getDetailJobView() {
        log.debug("begin");
        DetailView view = (DetailView) base.getMenuForJobs();
        log.debug("end");
        return view;
    }

    /**
     * a question box, returns Boolean.True exactly on yes
     *
     * @@PEB-2005-05-01@@ inserted
     */
    private class YesNoMessageBox extends BasicMessageBox {

        YesNoMessageBox(String title, String text) {

            super(null, createHeaderBar(title, ICON_QUESTION), null, text, new ButtonItem[]{bYes, bNo}, bYes, true);
        }

        protected Object onBoxClose(ButtonItem clickedButton) {

            return bYes.equals(clickedButton);
        }
    }

    /**
     * @param key
     * @return
     * @@PEB-2005-05-01@@ inserted String from Resource, key will be prefixed,
     * returns key if resource is not found
     */
    private static String getResStr(String key) {
        log.debug("begin");
        String s = Res.getString("main." + key);
        String ret = s != null ? s : key;
        log.debug("end " + ret);
        return ret;
    }

    /**
     * call to exit the application 'normal'
     */
    private void applicationExitProperly(boolean askForExit, boolean isConnectionChanged, boolean actived) {
        log.debug("begin");

        if (askForExit && actived) {
            UserDialog askExit = new YesNoMessageBox(getResStr("askExitAppMessageBox.header.title"),
                    getResStr("askExitAppMessageBox.text"));
            if (!UserDialogTool.executeUserDialog(appFrame, askExit).equals(Boolean.TRUE)) {
                return;
            }
        }
        applicationExit(askForExit, isConnectionChanged, Base.SHUTDOWN_NORMAL);
    }

    private void applicationExit(boolean askForExit, boolean isConnectionChanged, int reason) {
        log.debug("App: applicationExit");
        options.getWindowState().save(appFrame);
        options.setDividerPos(splitMain.getDividerLocation());
        PersistTool.store(guiPersistance, options, PERSIST);

        appFrame.dispose();
        if (loader != null && loader.getJobCnt() > 0) {
            log.debug("number of jobs: " + loader.getJobCnt());
            LoaderGui loaderGui = loader.getLoaderGui();
            loaderGui.buildGUI();
            loaderGui.onAppExit();
        } else if (askForExit || isConnectionChanged) {  // is, ISRC-21
            if (loader != null) {
                LoaderGui loaderGui = loader.getLoaderGui();
                loaderGui.stopService();
            }
            deleteOldLogFiles();
            shutdown(reason);
            if (!isConnectionChanged) {
                log.info("App: applicationExit, System.exit(0)");
                System.exit(0);
            }
        }
        log.debug("end");
    }


    public void deleteOldLogFiles() {
        log.info("begin");
        String maxLogAge = base.getParameterValue("MAX_LOG_AGE_SERVER");
        int days = NumberUtils.toInt(maxLogAge);

        File logDir = new File(System.getProperty("java.io.tmpdir"), "iStudyReporter");
        String deleted = FileSystemWorker.deleteOldFiles(logDir.getAbsolutePath(), days > 0 ? days : 3, "frontend*.log.gz");
        if (StringUtils.isNotEmpty(deleted)) {
            log.error(deleted);
        }
        log.info("end");
    }


    private void shutdown(int reason) {
        log.debug("begin");
        try {
            base.shutDown(reason);

        } catch (BackendException e) {

            log.warn("App: exception in base.shutDown(Base.SHUTDOWN_EXCEPTION).");
            log.debug("App: exception in base.shutDown(Base.SHUTDOWN_EXCEPTION).");
            log.error(App.class, e);
            ExceptionDialog.showExceptionDialog(null, e, null);
        }
        log.debug("end");
    }

    /**
     * call to exit the application with an exception
     */
    private void applicationExitExceptional() {
        log.debug("begin");

        shutdown(Base.SHUTDOWN_EXCEPTION);
        if (appFrame != null) {
            appFrame.dispose();
        }
        System.exit(0);
        log.debug("end");
    }


    /**
     * shows an exception dialog, if the severity is high enough it will
     * shutdown the system so in this case it will not return
     */
    private void showExceptionMayExit(BackendException be) {
        log.debug(be.getSeverity() + " " + be.getClass());

        ExceptionDialog.showExceptionDialog(appFrame, be, null);
        if (be.isAppExit()) {
            applicationExitExceptional();
        }
    }

    /**
     * Tries as hard as it can to set the language <br>
     * using default (= from OS) if an error happens: <br>- unknown Locale <br>-
     * can not set it in Toolkit: is necessary for Key Texts (e.g. Accelerators
     * in Menuitems) <br>
     * <br>
     *
     * @param userLanguage
     * @return true if could set language
     */
    private boolean setupLanguage(Locale userLanguage) {
        log.debug("begin");
        Res.resetLanguage();
        I18N.resetLanguage();
        try {

//            Locale.setDefault(userLanguage);
            /*
             * we need to reload language in Toolkit since it is loaded on class
             * creation and there is no public way to do this
             */
            Field f = Toolkit.class.getDeclaredField("resources");
            f.setAccessible(true);
            f.set(null, ResourceBundle.getBundle("sun.awt.resources.awt"));
            return true;

        } catch (Exception e) {

            ExceptionDialog.showExceptionDialog(null, e, "The desired language:" + userLanguage + " could not be set"
                    + ".");
        }

        log.debug("end");
        return false;
    }

    //########################################################################
    //#### Create Controls etc. ##############################################
    //########################################################################
    private void createTable() {
        log.debug("begin");
//       FilterableTableModel sortableTableModel = new FilterableTableModel(new EmptyTableModel()){
//           public Object[] getPossibleValues(int i, Comparator comparator) {
//               Object[] o = super.getPossibleValues(i, new UserInputComparator());
//                log.debug(o[0]);
//               return o;
//           }
//       };

        /*boolean exception = true;
         while (exception) {
         try {
         LookAndFeelFactory.installDefaultLookAndFeelAndExtension();
         exception = false;
         } catch (Exception e) {
         System.out.println(e.getMessage());
         exception = true;
         }
         }*/
        table = new ListViewTable(new EmptyTableModel()) {

            {
                setProcessEnter(true);
            }// or the keybinding won't work

            // if no rows show an empty marker:-)
            private JLabel emptyLabel = new JLabel();

            {
                emptyLabel.setText(Res.getString("general.table.empty.label.text"));
                emptyLabel.setIcon(UIManager.getIcon("OptionPane.informationIcon"));
                emptyLabel.setBorder(new EmptyBorder(5, 5, 5, 5));
            }

            public Dimension getPreferredSize() {

                if (getRowCount() > 0) {
                    return super.getPreferredSize();
                }
                emptyLabel.setFont(getFont());// use tables font
                return emptyLabel.getPreferredSize();
            }

            public void paint(Graphics g) {
                super.paint(g);
                if (getRowCount() > 0) {
                    return;
                }
                emptyLabel.setSize(getSize());// font is set in PreferredSize
                emptyLabel.paint(g);
            }

        };

        //((HierarchicalTable)table).setTableStyleProvider(new RowStripeTableStyleProvider());
        ((HierarchicalTable) table).setHierarchicalColumn(-1);
        ((HierarchicalTable) table).setSingleExpansion(true);

        DetailsTableComponentFactory detailsFactory = new DetailsTableComponentFactory();

        table.getSelectionModel().addListSelectionListener(e -> {
//            log.debug("getValueIsAdjusting:" + e.getValueIsAdjusting());
            if (!e.getValueIsAdjusting()) {

                updateEditMenu();

                Node rightSelectedNode = getSelectedNodeTable();
                log.debug("tableDoubleClick " + tableDoubleClick);
                if (!tableDoubleClick) {
                    if (rightSelectedNode != null) {
                        log.debug("SwingWorker table.getSelectionModel().addListSelectionListener: setNode");
                        base.setNode(rightSelectedNode, false);

                        byte[] detailsXml = null;
                        try {
                            detailsXml = base.getNodeDetailsInBackground();
                        } catch (Exception e1) {
                            detailsXml = detailsFactory.getDetailsByError(e1);
                        }
                        detailsFactory.setDetails(detailsXml);

                        if (ribbonIsDisplayed && selectedNodeHash != rightSelectedNode.hashCode()) {
                            log.debug("***** addListSelectionListener: call update ribbon, rightSelectedNode -> " + rightSelectedNode.hashCode());
                            SwingUtilities.invokeLater(new Runnable() {
                                public void run() {
                                    log.debug("***** addMouseListener: call update ribbon");
                                    updateRibbonMenu();
                                }
                            });
                        }
                        selectedNodeHash = rightSelectedNode.hashCode();

                        SwingUtilities.invokeLater(new Runnable() {
                            public void run() {
                                ((HierarchicalTable) table).expandRow(table.getSelectedRow());
                            }
                        });
                        log.debug("SwingWorker done() end");
                    }
                }
            }
        });

        ((HierarchicalTable) table).setComponentFactory(detailsFactory);

        CellRendererManager.registerRenderer(Node.class, new NodeTableRenderer(iconSource));
        CellRendererManager.registerRenderer(UserInput.class, new UserInputRenderer());

        ObjectConverterManager.registerConverter(UserInput.class, new UserInputConverter());
        ObjectComparatorManager.registerComparator(UserInput.class, new UserInputComparator());

        ObjectGrouperManager.initDefaultGrouper();
        ObjectGrouperManager.registerGrouper(UserInput.class, new UserInputGrouper());

        UserInputFilterFactoryManager factoryManager = new UserInputFilterFactoryManager();
        factoryManager.registerDefaultFilterFactories();
        FilterFactoryManager.setDefaultInstance(factoryManager);

        table.setRowHeight(table.getFontMetrics(table.getFont()).getHeight() + 2);
        table.setSelectionMode(ListSelectionModel.SINGLE_SELECTION);
        table.addKeyListener(new KeyAdapter() {

            public void keyPressed(KeyEvent e) {

                if (e.getID() == KeyEvent.KEY_PRESSED && EventTools.isPopupKey(e)) {
                    showPopupOnSelectionTable();
                }
            }
        });

        table.addMouseListener(new TablePopupListener() {

            public void doPopUp(int row, int col, MouseEvent e) {
                e.getComponent().requestFocusInWindow();
                showPopup(getSelectedNodeTable(), e.getComponent(), e.getPoint());
            }
        });

        // ... dblclick/Enter action for nodes
        table.addMouseListener(new TableDblClickAdapter() {

            public void rowDblClick(JTable table, int row, MouseEvent e) {
                log.debug("tableDoubleClick before " + tableDoubleClick);
                tableDoubleClick = true;  // double click is starting
                Object node = table.getModel().getValueAt(row, -1);
                if (!(node instanceof Node)) {
                    return;
                }
                executeBaseActionLater(new NodeActionImpl((Node) node, NodeAction.DBLCLICK_IN_DETAIL));
            }
        });

        Action acDblClick = new AbstractAction() {

            {
                putValue(Action.ACCELERATOR_KEY, KeyStroke.getKeyStroke("ENTER"));
            }

            public void actionPerformed(ActionEvent e) {

                Node node = getSelectedNodeTable();
                if (node == null) {
                    return;
                }
                executeBaseActionLater(new NodeActionImpl(node, NodeAction.DBLCLICK_IN_DETAIL));
            }
        };
        ActionUtil.putIntoActionMap(table, acDblClick, JComponent.WHEN_FOCUSED);
        // LookAndFeelFactory.installDefaultLookAndFeelAndExtension();

        AutoFilterTableHeader header = new AutoFilterTableHeader(table) {

            @Override
            protected IFilterableTableModel createFilterableTableModel(TableModel model) {

                return new FilterableTableModel(model) {
                    private static final long serialVersionUID = 7072186511643823323L;

                    @Override
                    protected void tableDataChanged() {
                        super.tableDataChanged();
                        sortingOrders = TableUtils.getSortableTablePreference((SortableTable) table, true);
                        tablePref = TableUtils.getTablePreferenceByName(table);
                        // log.debug("tableDataChanged: " + sortingOrders);
                    }

                    @Override
                    protected void filter(boolean b) {
                        super.filter(b);
                        filters = getFilterItems();
                        //  for(FilterItem item : filters) log.debug(item.column+" "+item.getFilter());
                    }

                    @Override
                    public boolean isColumnAutoFilterable(int column) {
                        // return column != 0;
                        if (column == 0 && table.getModel().getColumnClass(column).equals(Node.class) && table
                                .getModel().getValueAt(0, column) != null) {
                            if (((Node) table.getModel().getValueAt(0, column)).getDisplayType() == Node.MENU_DETAIL_VIEW) {
                                return false;
                            }
                        }
                        return true;
                    }
                };
            }

            @Override
            protected void customizeAutoFilterBox(AutoFilterBox autoFilterBox) {
                super.customizeAutoFilterBox(autoFilterBox);
                autoFilterBox.setAllowCustomFilter(false);
            }
        };

//        header.setAllowMultipleValues(true);
//        header.setAcceptTextInput(true);
        header.setAutoFilterEnabled(true);
        header.setShowFilterNameAsToolTip(true);
        header.setShowFilterIcon(true);

        table.setTableHeader(header);
        log.debug("end");
    }

    /**
     * creates and configures tree, needs: model, iconSource created
     */
    private void createTree() {
        log.debug("begin");
        tree = new JTree(new DefaultMutableTreeNode("Waiting..."));// dummy

        ToolTipManager.sharedInstance().registerComponent(tree);
        // node,
        // never
        // seen
        tree.setShowsRootHandles(true);
        tree.setRootVisible(false);
        tree.setCellRenderer(new NodeTreeCellRenderer(iconSource));

        tree.getSelectionModel().setSelectionMode(TreeSelectionModel.SINGLE_TREE_SELECTION);
        // remove ctrl-Space so nodes may not be deselected:
        tree.getInputMap().put(KeyStroke.getKeyStroke("ctrl SPACE"), "XXXXXXXXXXXXX");

        tree.addKeyListener(new KeyAdapter() {

            public void keyPressed(KeyEvent e) {
                log.debug("Key pressed " + e.getID());
                // 2017-06-28, MCD, added: because the selection listener is not active anymore
                if (e.getID() == KeyEvent.KEY_PRESSED && !EventTools.isPopupKey(e)) {
                    SwingUtilities.invokeLater(new Runnable() {
                        public void run() {
                            deferredTreeSelectionListener.immediateChange();
                        }
                    });
                }
                if (e.getID() == KeyEvent.KEY_PRESSED && EventTools.isPopupKey(e)) {
                    showPopupOnSelectionTree();
                }
            }
        });

        tree.addMouseListener(new TreePopupListener() {

            public void doPopUp(Object node, MouseEvent e) {
                //log.debug("TreePopupListener ");
                e.getComponent().requestFocusInWindow();
                showPopup(node, e.getComponent(), e.getPoint());
            }
        });

        deferredTreeSelectionListener = new DeferredTreeSelectionListener(tree) {

            protected void nodeSelectionChanged() {
                //    log.debug("nodeSelectionChanged ");
                sortingOrders = null;
                tablePref = null;
                filters = null;
//                base.getJobsTableInfo().setJobsTable(false);
                log.debug("DeferredTreeSelectionListener");
                treeNodeSelectionChanged();
            }
        };

        // 2017-06-28, MCD, removed: was executed twice with the mouse listener and selection listener
        //20191125, IS, added again, ISRC-1205
        tree.addTreeSelectionListener(deferredTreeSelectionListener);
        tree.addMouseListener(new MouseAdapter() {

            public void mousePressed(MouseEvent e) {
                log.debug("Tree mouse pressed");
                for (int i = 0; i < table.getMouseListeners().length; i++) {
                    if (table.getMouseListeners()[i] instanceof ProgressTablePopupListener) {
                        log.debug("removeMouseListener:i = " + i);
                        table.removeMouseListener(table.getMouseListeners()[i]);
                    }
                }

                Node leftSelectedNode = getSelectedNodeTree();
                if (leftSelectedNode != null) {

                    log.debug("tree.addMouseListener: setNode");
                    base.setNode(leftSelectedNode, true);

                    SwingUtilities.invokeLater(new Runnable() {
                        public void run() {
                            deferredTreeSelectionListener.immediateChange();
                        }
                    });
                    table.clearSelection();

                    if (ribbonIsDisplayed) {
                        SwingUtilities.invokeLater(new Runnable() {
                            public void run() {
                                log.debug("***** addMouseListener: call update ribbon");
                                updateRibbonMenu();
                            }
                        });
                    }

                    selectedNodeHash = 0;

                }
            }
        });

        log.debug("end");
    }

    //expand a single node
    private static void expandSingleNode(
            JTree tree,
            WrappingTreeModel data,
            TreePath path) {
        log.debug("expandSingleNode");
        if (path != null) {
            Object node = path.getLastPathComponent();
            int count = data.getChildCount(node);

            if (count == 0) {
                if (data.isLeaf(node)) {
                    tree.expandPath(path.getParentPath());
                } else {
                    tree.expandPath(path);
                }
            } else {
                for (int i = 0; i < count; i++) {
                    expandSingleNode(
                            tree,
                            data,
                            path.pathByAddingChild(data.getChild(node, i)));
                }
            }
        }
        log.debug("end");
    }

    private JMenuBar createMenuBar() {
        log.debug("begin");
        // ... File ...
        JMenu menuFile = new JMenu(new ResMenuAction("file", null));
        menuFile.add(acChangeConnection);
        menuFile.add(new JSeparator());
        menuFile.add(acUnloadTreeAll);
        menuFile.add(acUnloadTreeBelow);
        menuFile.add(new JSeparator());
        menuFile.add(acExit);

        // ... Edit ...
        JMenu menuEdit = new JMenu(new ResMenuAction("edit", null)) {
            //		public JPopupMenu getPopupMenu() { // save: fill on open, needs
            // work
            //			menuEditOpened(this);
            //			return super.getPopupMenu();
            //		}
            // addMenuListener(new MenuListener(){ fires also on select but not
            // show
        };
        menuEdit.add(acEditSelection);

        // ... Help ...
        JMenu menuHelp = new JMenu(new ResMenuAction("help", null));
        menuHelp.add(acHelp);
        menuHelp.add(acAbout);

        // ... build bar
        JMenuBar mb = new JMenuBar();
        mb.add(menuFile);
        mb.add(menuEdit);

        JMenu[] extra = createDynMenuItems();
        for (int i = 0; i < extra.length; i++) {
            mb.add(extra[i]);
        }

        mb.add(menuHelp);
        log.debug("end");
        return mb;

    }

    /**
     * reads the additional dynamic menuitems for the menuBar from the backend
     *
     * @return never null, maybe empty
     */
    private JMenu[] createDynMenuItems() {
        log.debug("begin");
        MenuItems menus = null;
        try {
            menus = base.getMainMenuBar();
        } catch (BackendException e) {
            showExceptionMayExit(e);
        }
        if (menus == null) {
            return new JMenu[0];
        }

        JMenu[] jMenus = new JMenu[menus.getSize()];
        for (int i = 0; i < jMenus.length; i++) {
            jMenus[i] = new ExecutingMenuBuilder(menus.get(i)).createMenu();
        }

        log.debug("end");
        return jMenus;

    }

    private void createActions() {
        log.debug("begin createActions");
        acHelp = new ActionHelp() {

            public void actionPerformed(ActionEvent e) {
                Node selNode = getSelectedNode();
                String nodeType = "";
                String identifier = "";
                if (selNode instanceof TreeNodeRecord) {
                    TreeNodeRecord nodeRecord = (TreeNodeRecord) getSelectedNode();
                    log.debug(nodeRecord.getNodeType());
                    ValueList valueList = ((TreeNodeRecord) getSelectedNode()).getValueList();
                    for (int k = 0; k < valueList.getSize(); k++) {
                        ValueRecord valueRecord = valueList.get(k);
                        if ("REPORTTYPE".equals(valueRecord.getNodeType())
                                || nodeRecord.getNodeType().equals(valueRecord.getNodeType())) {
                            if ("REPORTTYPE".equals(valueRecord.getNodeType())
                                    || nodeRecord.getNodeType().equals(valueRecord.getNodeType())) {
                                log.debug(valueRecord.getParameterName() + " " + valueRecord.getNodeId() + " " + valueRecord.getNodeType());
                                nodeType = valueRecord.getNodeType();
                                identifier = valueRecord.getNodeId();
                                break;
                            }
                        }
                    }
                } else {
                    log.debug(selNode);
                }
//                JOptionPane.showMessageDialog(null, "Help dummy.");
                Help.showHelp();
            }
        };

        acExit = new ResAction("exit", null) {

            public void actionPerformed(ActionEvent e) {
                log.debug("applicationExit  actionPerformed  acExit ");
                applicationExitProperly(true, false, appFrame.isFocused());
            }
        };

        acAbout = new ResAction("about", null) {

            public void actionPerformed(ActionEvent e) {
                new AboutBox().showModal(appFrame);
            }
        };

        acEditSelection = new ResAction("editSelection", null) {

            public void actionPerformed(ActionEvent e) {
                showPopupOnSelection();
            }
        };

        acUnloadTreeBelow = new ResAction("unloadTreeBelow", null) {

            public void actionPerformed(ActionEvent e) {
                onUnloadTreeBelow();
                selectedNodeHash = 0;
                if (ribbonIsDisplayed) {
                    updateRibbonMenu();
                }
            }
        };

        acUnloadTreeAll = new ResAction("unloadTreeAll", null) {

            public void actionPerformed(ActionEvent e) {
                onUnloadTreeAll();
                selectedNodeHash = 0;
                if (ribbonIsDisplayed) {
                    updateRibbonMenu();
                }
            }
        };

        acChangeConnection = new ResAction("changeConnection", null) {

            public void actionPerformed(ActionEvent e) {

                UserDialog askExit = new YesNoMessageBox(
                        getResStr("askChangeAppMessageBox.header.title"),
                        getResStr("askChangeAppMessageBox.text"));
                if (!UserDialogTool.executeUserDialog(appFrame, askExit).equals(Boolean.TRUE)) {
                    return;
                }
                log.debug("applicationExit  actionPerformed  acChangeConnection");
                applicationExitProperly(false, true, appFrame.isFocused());
                new Stabber();
            }
        };
        log.debug("end");
    }

    /**
     * for a JMenuItem in a JMenu
     */
    private static abstract class ResAction extends ActionWrap {

        public ResAction(String name, Object icon) {

            super(Res.getActionMap("main.action." + name), icon);
            // on error do emergency solution
            if (getValue(Action.NAME) == null) {
                putValue(Action.NAME, "$$" + name);
            }
        }
    }//-------------------------------------------------------------

    /**
     * for a JMenu in the JMenuBar, <b>not JMenuItem </b>
     */
    private final static class ResMenuAction extends ActionWrap {

        public ResMenuAction(String name, Object icon) {

            super(Res.getActionMap("main.menu." + name), icon);
            // on error do emergency solution
            if (getValue(Action.NAME) == null) {
                putValue(Action.NAME, "$$" + name);
            }
        }

        public void actionPerformed(ActionEvent e) {/*
         * dummy, is never called
         * by swing
         */

        }
    }//-------------------------------------------------------------

    //########################################################################
    //#### PopUps, selection etc. ############################################
    //########################################################################

    /**
     * called if a Treenode REALLY ( not like getValueIsAdjusting() ) changed,
     * user may change selection but we are only noticed if it is a real change
     * after ~800ms
     */
    private void treeNodeSelectionChanged() {
        log.debug("begin");
        updateEditMenu();
        updateDetailFromTreeSelection();
        log.debug("end");
    }

    /**
     * Shows a popup menu for node on component target at point (rel. to target)
     *
     * @param node   needs to be a com.utd.stb.action.Node, if null does nothing.
     * @param target the component where the point is relative to, also the
     *               targets window-parent will be the popups parent
     * @param point  ~ in target coords
     */
    private void showPopup(Object node, Component target, Point point) {
        log.debug("begin");

        if (node == null) {
            return;
        }

        MenuItem nodeMenu = null;
        try {
            nodeMenu = base.getNodeMenu((Node) node);
        } catch (BackendException e) {
            showExceptionMayExit(e);
            return;
        }
        new ExecutingMenuBuilder(nodeMenu).getPopUpMenu().show(target, point.x, point.y);
        log.debug("end");
    }

    /**
     * looks for current selection in tree
     *
     * @return
     */
    private Node getSelectedNodeTree() {
        log.debug("begin");
        TreePath path = tree.getSelectionPath();
        if (path == null) {
            log.debug("end");
            return null;
        }

        Object node = path.getLastPathComponent();
        if (!(node instanceof Node)) {
            log.debug("end");
            return null;
        }

        log.debug("end " + node);
        return (Node) node;
    }

    /**
     * looks for current selection in table
     *
     * @return
     */
    private Node getSelectedNodeTable() {
        log.debug("begin");
        int row = table.getSelectedRow();
        if (row == -1) {
            log.debug("end: row=-1");
            return null;
        }

        Object node = table.getModel().getValueAt(row, -1);
        log.debug(node);

        if (!(node instanceof Node)) {
            log.debug("end " + node);
            return null;
        }

        log.debug("end " + node);
        return (Node) node;
//        } catch (Exception e) {
//            return null;
//        }
    }

    /**
     * find the node in the table, selects it.
     *
     * @param node
     * @return true if node was found, otherwise false
     */
    private boolean setSelectedNodeTable(Node node) {
        log.debug("begin");
        //TableModel m = table.getModel();
        TableModel m = TableModelWrapperUtils.getActualTableModel(table.getModel());

        if (m.getRowCount() == 0) {
            return false;
        }
        if (!(m instanceof NodesTableModel)) {
            return false;
        }

        int row = ((NodesTableModel) m).indexOf(node);
        // log.debug("setSelectedNodeTable:"+row+" "+TableModelWrapperUtils.getRowAt(table.getModel(), row));
        if (row == -1) {
            table.clearSelection();
        } else {
            table.setRowSelectionInterval(TableModelWrapperUtils.getRowAt(table.getModel(), row), TableModelWrapperUtils.getRowAt(table.getModel(), row));
        }

        log.debug("end");
        return row != -1;
    }

    /**
     * tree, table or null
     */
    private Component getLastFocused() {
//        log.debug("begin");
        Component c = KeyboardFocusManager.getCurrentKeyboardFocusManager().getPermanentFocusOwner();
//        log.debug("end");
        return (c == tree || c == table) ? c : null;
    }

    /**
     * looks for current selection tree or list, what is more meaningfull
     * (focus)
     *
     * @return null if we can't decide which node eis meant
     */
    private Node getSelectedNode() {
        log.debug("begin");
        Node node = null;
        Component comp = getLastFocused();
        if (comp == table) {
            node = getSelectedNodeTable();
        } else if (comp == tree) {
            node = getSelectedNodeTree();
        }
        log.debug("end " + node);
        return node;
    }

    /**
     * call on main menu action, opens a popupmeneu at the cells location tree
     * or table
     */
    private void showPopupOnSelection() {
        log.debug("begin");
        if (getLastFocused() == table) {
            showPopupOnSelectionTable();
        } else if (getLastFocused() == tree) {
            showPopupOnSelectionTree();
        }
        log.debug("end");
    }

    /**
     * call on main menu action, opens a popupmeneu at the cells location
     */
    private void showPopupOnSelectionTree() {
        log.debug("begin");
        Node node = getSelectedNodeTree();
        if (node == null) {
            return;
        }
        TreePath path = tree.getSelectionPath();
        if (path == null) {
            return;
        }
        Rectangle r = tree.getPathBounds(path);
        if (r == null) {
            return;
        }

        Point pt = r.getLocation();
        pt.translate(10, r.height + 10);
        showPopup(node, tree, pt);
        log.debug("end");
    }

    /**
     * call on main menu action, opens a popupmenu at the cells location
     */
    private void showPopupOnSelectionTable() {
        log.debug("begin");
        Node node = getSelectedNodeTable();
        if (node == null) {
            return;
        }

        int row = table.getSelectedRow();
        if (row == -1) {
            return;
        }

        Rectangle r = table.getCellRect(row, 0, true);
        if (r == null) {
            return;
        }

        Point pt = r.getLocation();
        pt.translate(10, r.height + 10);
        showPopup(node, table, pt);
        log.debug("end");
    }

    /**
     * set it dis/enabled, later it maybe filled with the content for the node
     */
    private void updateEditMenu() {
        log.debug("begin");
        Node node = getSelectedNode();
        acEditSelection.setEnabled(node != null);
        log.debug("end " + node);
    }

    /**
     * sets new table content depending from the selected treenode
     */
    public void updateDetailFromTreeSelection() {
        log.debug("begin");
        // table.setVisible(false);// nice idea, but only works if we let swing
        // repaint! -> multi threading
//        startLongAction();// lots of calls to the base: wrap so hourglas cursor
        // does not flicker
        try {
            final Node node = getSelectedNodeTree();
            if (node == null) {
                setNodeDetailsClear();
                return;
            }
            log.debug("updateDetailFromTreeSelection node: " + node);
            setDetailsFromView(base.getDetailViewAction(node));
        } catch (BackendException e) {
            showExceptionMayExit(e);
        }
//finally {
//            stopLongAction();
//        }
        log.debug("end");
    }

    /**
     * Sets the details. <br>
     * If nodes==null or empty then 'no data' is assumed. Else nodes and
     * userInputs needs to be of same size
     *
     * @param detailView
     */
    private void setDetailsFromView(DetailView detailView) {
        log.debug("begin");
        // log.debug("setDetailsFromView: "+Thread.currentThread().getStackTrace()[2]+" "+
        //         Thread.currentThread().getStackTrace()[3]);
        Nodes nodes = detailView.getNodes();

        if (nodes == null || nodes.getSize() == 0) {
            setNodeDetailsClear();
            return;
        }

        UserInputs[] uis = new UserInputs[nodes.getSize()];

        for (int i = 0; i < uis.length; i++) {
            uis[i] = detailView.getNodeProperties(nodes.getNode(i));
//            for(int j=0;j<uis[i].getSize();j++)
//             log.debug(i + " "+j+" "+nodes.getNode(i) + "; " + uis[i].get(j).getLabel());
        }

        NodesTableModel model = new NodesTableModel(nodes, uis);

//        FilterableTreeTableModel filterableTableModel = new FilterableTreeTableModel(model);
//        SortableTreeTableModel treeTableModel = new SortableTreeTableModel(filterableTableModel);
        table.setModel(model);

//        for (int i = 0; i < treeTableModel.getRowCount(); i++)
//            log.debug("row " + i + " " + treeTableModel.getColumnClass(0) + " " + treeTableModel.getValueAt(i, 0) + " " + table.getCellRenderer(i, 0));
        // MCD, 09.Dez 2008, sort table, TODO
        /*table.getTableHeader().addMouseListener(new MouseAdapter() {
         public void mousePressed(MouseEvent e) {
         model.sortByColumn(table.columnAtPoint(e.getPoint()));
         }
         });*/
        //TableTools.selectFirstRow(table, false);
        TableTools.autoSizeColumns(table, false);
        log.debug("after autoSizeColumns");

        String label = detailView.getTitle();
        tableLabel.setText(label);
        tableLabel.setVisible(StringUtils.isNotEmpty(label));
        log.debug("end");
    }

    /**
     * set to 'no data'
     */
    public void setNodeDetailsClear() {
        log.debug("begin");
        table.setModel(new EmptyTableModel());
        tableLabel.setVisible(false);
        log.debug("end");
    }

    /**
     * This is done 'later' in the SwingThread, We should try to load node
     * childToFind in the tree, so:
     * <ul>
     * <li>load 'parentInTree's childs (if not loaded) in the tree
     * <li>find this node there
     * <li>if found expand parent
     * <li>select node as child
     * </ul>
     *
     * @param parentInTree
     * @param childToFind
     */
    private void tryToShowNodeInTreeLater(final Node parentInTree, final Node childToFind) {

        SwingUtilities.invokeLater(new Runnable() {

            public void run() {
                // loads childs
                int i = treeModel.getIndexOfChild(parentInTree, childToFind);
                // not found, MCD, 17.Jul 2006, show node list
                if (i == -1) {
                    if (base.showDocVersionSeparately()) {
                        updateDetailFromTreeSelection();
                    } else {
                        return;
                    }
                }
                TreePath path = treeModel.getTreePath(childToFind);
                if (path == null) {
                    return;
                }
                tree.setSelectionPath(path);
            }
        });
    }

    private void onUnloadTreeBelow() {

        onUnloadTreeBelow(getSelectedNodeTree());
    }

    /**
     * reload all
     */
    private void onUnloadTreeAll() {

        onUnloadTreeBelow(null);
    }

    /**
     * @param node if null then reload all
     */
    private void onUnloadTreeBelow(Object node) {
        log.debug("begin");
        if (node == null) {
            node = treeModel.getRoot();// root or nothing selected
        }
        treeModel.setNodeUnloaded(node);
        TreePath path = treeModel.getTreePath(node);
        if (path == null) {
            return;
        }
        tree.setSelectionPath(path);
        treeNodeSelectionChanged();

        // if the table is empty, set the focus into the tree, better to use
        if (table.getRowCount() == 0) {
            tree.requestFocusInWindow();
        }
        log.debug("end");
    }

    //########################################################################
    //#### Action Handling (high level, no Swing events) #####################
    //########################################################################

    /**
     * executes the action on activation
     */
    private final class ExecutingMenuBuilder extends MenuBuilder {

        private ExecutingMenuBuilder(MenuItem root) {

            super(root, iconSource);
        }

        public void actionPerformed(ActionEvent e, MenuItem item) {

            executeBaseActionLater(item);
        }

        protected void addMenuTitle(JPopupMenu menu, String label) {

        }
    }

    /**
     * sends the action to the base, processes all returns and tasks in the
     * queue, called 'later' in the Swing thread, call from menus to let them
     * <p>
     * close before
     *
     * @param action
     */
    private void executeBaseActionLater(final Object action) {
        log.debug("begin " + action);
        SwingUtilities.invokeLater(new Runnable() {

            public void run() {

                Object next = null;

                /*
                 * Special behaviour for NodeAction.DBLCLICK_IN_DETAIL, if the
                 * backend returns null we should try to 'open' the selected
                 * node in the tree. First we process all tasks, and later load
                 * the node.
                 *
                 */
                try {
                    next = base.invokeAction(action);
                } catch (BackendException e) {
                    showExceptionMayExit(e);
                } catch (Exception e) {
                    log.error(e);
                    //todo: frontend exception
                }
                if (next == null && action instanceof NodeAction) {
                    if (((NodeAction) action).getType().equals(NodeAction.DBLCLICK_IN_DETAIL)) {
                        Node node = ((NodeAction) action).getTargetNode();
                        base.setNode(node, false);
                        Node parent = getSelectedNodeTree();
                        if (parent != null && node != null) {
                            tryToShowNodeInTreeLater(parent, node);
                        }
                    }
                }

                if (next != null) {
                    executeActionLoop(next);
                }
                processAllTasks();
                tableDoubleClick = false; // double click is ready
                log.debug("tableDoubleClick=" + tableDoubleClick);
            }
        });
        log.debug("end");
    }

    /**
     * looks in the base if there are tasks to do and execute them all
     *
     * @return true if something was done, else false
     */
    private boolean processAllTasks() {
        log.debug("begin");
        boolean anyAction = false;
        Object task = base.getNextTask();
        while (task != null) {
            anyAction = true;
            executeActionLoop(task);
            task = base.getNextTask();
        }
        log.debug("end");
        return anyAction;
    }

    /**
     * does the actions until one return null
     *
     * @param action
     * @return
     */
    private void executeActionLoop(Object action) {
        log.debug("begin");
        while (action != null) {
            action = executeAction(action);
        }
        log.debug("end");
    }

    /**
     * executes one action
     *
     * @param action
     * @return the result of the action
     */
    private Object executeAction(Object action) {

        log.debug("action=" + action);
        if (action == null) {
            return null;
        }

        globalAction = action;
        if (action instanceof NodeAction) {
            return executeNodeAction((NodeAction) action);
        }
        if (action instanceof DetailView) {
            return executeDetailViewAction((DetailView) action);
        }
        if (action instanceof UserDialog) {
            return executeUserDialog((UserDialog) action);
        }
        if (action instanceof ReportWizard) {
            return executeReportWizard((ReportWizard) action);
        }
        if (action instanceof WindowAction) {
            return executeWindowAction((WindowAction) action);
        } else {

            try {
                return base.invokeAction(action);
            } catch (BackendException e) {
                log.debug("error");
                showExceptionMayExit(e);
                // TODO was tun wir wenn die basis noch was in auftrag geben wollte?
            }
            log.debug("end");
            return null;
        }
    }

    private Object executeDetailViewAction(DetailView action) {
        log.debug("begin");
        setDetailsFromView(action);

        if (isReload) {

            TableUtils.setSortableTablePreference((SortableTable) table, sortingOrders, true);
            if (tablePref != null) {
                TableUtils.setTablePreferenceByName(table, tablePref);
            }

            if (filters != null && filters.size() > 0) {
                for (IFilterableTableModel.FilterItem item : filters) {
                    ((AutoFilterTableHeader) table.getTableHeader()).getFilterableTableModel().addFilter(item);
                    ((AutoFilterTableHeader) table.getTableHeader()).getFilterableTableModel().setFiltersApplied(true);
                }
            }
        }
        log.debug("end");
        return null;
    }

    private Object executeNodeAction(NodeAction nodeAction) {
        log.debug("begin " + nodeAction);
        Node node = nodeAction.getTargetNode();
        log.debug(node);
        if (node == null) {
            return null;
        }
        String type = nodeAction.getType();

        TreePath path = treeModel.getTreePath(node);
        log.debug("executeNodeAction:" + type + "; path = " + path);

        if ((path == null || path.getPath() == null) && !NodeAction.SELECT_IN_DETAIL.equals(type)) // exit
        {
            showExceptionMayExit(new BackendException("Nodes are not synchronized anymore.",
                    BackendException.SEVERITY_APP_EXIT));
        }

        // EXPAND
        if (NodeAction.EXPAND.equals(type)) {
            tree.expandPath(path);
            if (expanded != null) {
                while (expanded.hasMoreElements()) {
                    TreePath tp = expanded.nextElement();
                    tree.expandPath(tp);
                }
            }
            // SELECT
        } else if (NodeAction.SELECT.equals(type)) {
            setNodeDetailsClear();
            try {
                tree.removeTreeSelectionListener(deferredTreeSelectionListener);

                if (path != null && path.getPathCount() > 0 && path.getPathComponent(0) != null) {
                    tree.setSelectionPath(path);
                }
            } finally {
                tree.addTreeSelectionListener(deferredTreeSelectionListener);
            }
            // RELOAD
        } else if (NodeAction.RELOAD.equals(type)) {
            expanded = tree.getExpandedDescendants(new TreePath(tree.getModel().getRoot()));
            // selection is removed by JTree if it is not visible anymore
            treeModel.setNodeUnloaded(node);
            // SELECT_AND_RELOAD_DETAIL
        } else if (NodeAction.SELECT_AND_RELOAD_DETAIL.equals(type)) {
            base.setNode(node, true);
            try {
                tree.setSelectionPath(path);
                // ISRC-1190
                tree.scrollPathToVisible(path);
            } catch (Exception ignored) {
                log.error(ignored);
            }
            deferredTreeSelectionListener.immediateChange();
            isReload = true;
            // MCD, 29. Nov 2018 is executed in the listener updateDetailFromTreeSelection();
            // SELECT_IN_DETAIL
        } else if (NodeAction.SELECT_IN_DETAIL.equals(type)) {
            isReload = false;
            if (treeModel.getIndexOfChild(getSelectedNodeTree(), node) != -1) {
                treeModel.setNodeUnloaded(node);
                tree.expandPath(path);
                base.setNode(node, false);
                tree.setSelectionPath(treeModel.getTreePath(node));
                deferredTreeSelectionListener.immediateChange();
                // MCD, 29. Nov 2018 is executed in the listener updateDetailFromTreeSelection();
            }
            if (expanded != null) {
                while (expanded.hasMoreElements()) {
                    TreePath tp = expanded.nextElement();
                    tree.expandPath(tp);
                }
            }
            setSelectedNodeTable(node);
        }

        log.debug("end");
        return null;
    }

    private Object executeUserDialog(UserDialog userDialog) {
        log.debug("begin");
        return UserDialogTool.executeUserDialog(appFrame, userDialog);
    }

    private Object executeReportWizard(ReportWizard reportWizard) {
        log.debug("begin");
        appFrame.setCursor(Cursor.getPredefinedCursor(Cursor.WAIT_CURSOR));
        Object o = ReportWizardDlgFactory.executeReportWizardDialog(appFrame, reportWizard, guiPersistance);
        appFrame.setCursor(Cursor.getPredefinedCursor(Cursor.DEFAULT_CURSOR));
        log.debug("end");
        return o;
    }

    private Object executeWindowAction(WindowAction action) {
        log.debug("begin");
        if (WindowAction.EXCEPTION_ABORT_APPLICATION.equals(action.getType())) {
            applicationExitExceptional();
        }
        log.debug("end");
        return null;
    }


    /**
     * starts the timer that look if the user was inactive for the defined time.
     * if so all windows are hidden, the backend dialogs/wizzards get informed
     * and the applikation exits
     */
    private void startTimeOutWatcher() {
        log.debug("begin " + appInfo.getTimeoutInMinutes());
        new Timer(10000, e -> {

            // we are creating?
            if (appFrame == null) {
                return;
            }
            // we are already hidden?
            if (!appFrame.isVisible()) {
                return;
            }

            long milliesInactive = System.currentTimeMillis() - TimeoutManager.getLastEventTime();
            long minutes = milliesInactive / (1000 * 60);
//            log.debug(minutes + "; number of running jobs: " + loader.getJobCnt());
            if (minutes < appInfo.getTimeoutInMinutes() || loader.getJobCnt() > 0) {  //ISRC-1007
                return;
            }

//            log.info("timeout occured:" + minutes + " minutes inactive till now, in seconds:" + (milliesInactive / 1000));

            Object[] options = {
                    ButtonItem.OK,
                    ButtonItem.CANCEL
            };
            Icon icon = new IconSource().getHeaderIconByID(BasicMessageBox.ICON_INFORMATION);
            JideOptionPane pane = new JideOptionPane(getResStr("timeoutExitAppMessageBox.text"), JOptionPane.INFORMATION_MESSAGE,
                    JOptionPane.OK_CANCEL_OPTION, icon, options, options[0]);

            JDialog timeoutDialog = pane.createDialog(appFrame, getResStr("timeoutExitAppMessageBox" + ".header.title"));
            pane.selectInitialValue();
            int dialogTimeout = appInfo.getDialogTimeoutInSec();
            log.debug(dialogTimeout);

            final Timer t = new Timer(dialogTimeout * 1000, e12 -> {
                timeoutDialog.setVisible(false);
                informAllWindowsAboutTimeOut(appFrame);
                applicationExit(true, false, Base.SHUTDOWN_TIMEOUT);
            });

            timeoutDialog.addComponentListener(new ComponentAdapter() {
                @Override
                public void componentShown(ComponentEvent e) {
                    super.componentShown(e);
                    t.start();
                }
            });

            timeoutDialog.setVisible(true);
            timeoutDialog.dispose();
            Object result = pane.getValue();
            log.debug(result);

            if (result == ButtonItem.OK) {
                informAllWindowsAboutTimeOut(appFrame);
                applicationExit(true, false, Base.SHUTDOWN_TIMEOUT);
            } else {
                t.stop();
                timeoutDialog.setVisible(false);
            }

        }).start();
        log.debug("end");
    }

    /**
     * recursive walks the tree of 'getOwnedWindows' windows, and invokes for
     * every instance of TimeoutReceiver timeoutOccured() - leafs first.
     *
     * @param parent
     */
    private void informAllWindowsAboutTimeOut(Window parent) {
        log.debug("begin");
        Window[] owneds = parent.getOwnedWindows();
        for (Window owned : owneds) {
            informAllWindowsAboutTimeOut(owned);// childs first
        }
        if (parent instanceof TimeoutReceiver) // now ourself
        {
            ((TimeoutReceiver) parent).timeoutOccured();
        }
        log.debug("end");
    }

    // ##############################################################################
    // ##############################################################################
    // ##############################################################################

    /**
     * Helper to load nodes from {@link Base}into a {@link WrappingTreeModel},
     * define this here to manage the exceptions more easy
     *
     * @author PeterBuettner.de
     */
    class BaseNodeSource implements WrappingTreeModel.NodeSource {

        private final Base base;

        public BaseNodeSource(Base base) {

            this.base = base;
        }

        public List loadNodes(Object parent) {

            long duration = System.currentTimeMillis();
            log.debug("parent " + parent);
            Nodes nodes = null;
            boolean showHourGlass = !workingOnTreeUpdate;
            try {
                if (showHourGlass) {
                    startLongAction();
                }
                nodes = base.getNodeChildsTree((Node) parent);
            } catch (BackendException e) {
                showExceptionMayExit(e);
            } finally {
                if (showHourGlass) {
                    stopLongAction();
                }
                duration = System.currentTimeMillis() - duration;
                log.debug("parent " + parent + ", duration " + duration);
            }
            if (nodes == null) {
                return Collections.EMPTY_LIST;
            }
            return new NodeList(nodes);
        }
    }

    /**
     * wraps Nodes into a list
     */
    private static final class NodeList extends AbstractList {

        final private Nodes nodes;

        public NodeList(final Nodes nodes) {

            this.nodes = nodes;
        }

        final public Object get(int index) {

            return nodes.getNode(index);
        }

        final public int size() {

            return nodes.getSize();
        }
    }

    private Node searchNodeInTree(String nodetype, String nodeid) {
        log.debug("begin");
        Enumeration<TreePath> expanded = tree.getExpandedDescendants(new TreePath(tree.getModel().getRoot()));
        Vector<TreePath> paths = sort(expanded);
        log.debug("searchNodeInTree:path size " + paths.size());
        for (TreePath treePath : paths) {
            Object node = treePath.getLastPathComponent();
            if (node instanceof TreeNodeRecord) {
                log.trace("searchNodeInTree: node " + node + " [" + ((TreeNodeRecord) node).getNodeType() + ", " + ((TreeNodeRecord) node).getNodeId() + "]");
                if (nodetype.equals(((TreeNodeRecord) node).getNodeType())
                        && nodeid.equals(((TreeNodeRecord) node).getNodeId())) {
                    return (TreeNodeRecord) node;
                }
            }
        }
        log.debug("end");
        return null;
    }

    public void updateTree(final String nodetype, final String nodeid, final String side) {
        log.debug("begin");
        Node trNode = base.createTransportNode(nodetype, nodeid, side);
        executeBaseActionLater(new NodeActionImpl(trNode, NodeAction.UPDATE_TREE));
        log.debug("end");
    }

    /**
     * sorts an enumeration of the expanded nodes by depth of the node
     *
     * @param expanded enumeration of the expanded nodes
     * @return sorted vector
     */
    public Vector<TreePath> sort(Enumeration expanded) {
        log.debug("begin");
        Vector<TreePath> paths = new Vector<TreePath>();
        Vector levels = new Vector();

        int maxLevel = -1;
        int actualLevel = 0;
        if (expanded != null) {
            while (expanded.hasMoreElements()) {
                TreePath tp = (TreePath) expanded.nextElement();
                paths.add(tp);
                StringTokenizer st = new StringTokenizer(tp.toString(), ",");
                levels.add(paths.indexOf(tp), st.countTokens());
                if (st.countTokens() > maxLevel) {
                    maxLevel = st.countTokens();
                }
            }
        }
        Object[] pathsV = paths.toArray();
        Object[] levelsV = levels.toArray();
        Vector<TreePath> sorted = new Vector<TreePath>();

        while (actualLevel <= maxLevel) {
            for (int i = 0; i < levelsV.length; ++i) {
                if ((Integer) levelsV[i] == actualLevel) {
                    sorted.add((TreePath) pathsV[i]);
                }
            }
            actualLevel++;
        }
        log.debug("end");
        return sorted;
    }

    public JTable getTable() {
        return table;
    }

    public StatusBarThread getStatusBarThread() {
        return statusBarThread;
    }

    public Base getBase() {
        return base;
    }

    public JFrame getAppFrame() {
        return appFrame;
    }

    public JRibbon createRibbonMenu() {
        log.debug("begin");

        App app = this;
        long duration = System.currentTimeMillis();

        log.info("Ribbon creation");
        ribbonMenu = new RibbonMenu();

        try {
            //log.debug("Create ribbon menu item CREATE");
            MenuItem menuItem = base.createRibbonMenuEntry("CREATE");
            //log.debug("Collect xml for menu item CREATE");
            byte[] ribbonXml = base.getXml(null, menuItem, null);
            long duration1 = System.currentTimeMillis() - duration;
            //log.info("Ribbon data collected in milliseconds: " + duration1);
            ribbon = ribbonMenu.createRibbon(ribbonXml, app);

        } catch (Exception ex) {
            ex.printStackTrace(System.out);
            ex.printStackTrace();
            log.error(ex);
        }

        duration = System.currentTimeMillis() - duration;
        log.info("Ribbon creation (data collection and frontend) finished in milliseconds: " + duration);

        return ribbon;
    }

    public void updateRibbonMenu() {

        SwingWorker<byte[], Void> worker = new SwingWorker<byte[], Void>() {

            long duration;

            @Override
            protected byte[] doInBackground() {

                duration = System.currentTimeMillis();
                log.info("Ribbon refresh");
                ribbonMenu.deactivateAll(ribbon);
                Node node = getSelectedNode();

                String workingSide = node == null || getLastFocused() == tree ? "LEFT" : "RIGHT";
                //log.debug("Create ribbon menu item REFRESH_" + workingSide + " for node " + ((node == null) ? "no node" : node.getText()));
                MenuItem menuItem = base.createRibbonMenuEntryInBackground("REFRESH_" + workingSide);
                try {
                    byte[] ribbonXml = base.getXmlInBackground(node, menuItem, null);
                    long duration1 = System.currentTimeMillis() - duration;
                    //log.info("Ribbon data collected in milliseconds: " + duration1);
                    return ribbonXml;
                } catch (BackendException ex) {
                    log.error(ex);
                    return null;
                }
            }

            @Override
            protected void done() {

                byte[] ribbonXml = null;
                try {
                    ribbonXml = get();
                } catch (Exception ex) {
                    log.error(ex);
                }

                if (ribbonXml != null) {
                    try {
                        ribbonMenu.refreshRibbon(ribbonXml, ribbon);
                    } catch (XPathExpressionException e) {
                        e.printStackTrace();
                        log.error(e);
                    }
                } else {
                    log.error("No xml for refreshing available");
                }

                duration = System.currentTimeMillis() - duration;
                log.info("Ribbon refresh finished in milliseconds: " + duration);
            }

        };
        if (ribbonIsDisplayed) {
            worker.execute();
        }
    }

    public void setRibbonAccelerator(String token) {
//        log.debug("begin");
        if (token.startsWith("FRONTEND.")) {
            if (token.endsWith("HELP")) {
                ActionUtil.putIntoActionMap(appFrame.getRootPane(), acHelp);
            }
            if (token.endsWith("ABOUT")) {
                ActionUtil.putIntoActionMap(appFrame.getRootPane(), acAbout);
            }
            if (token.endsWith("UNLOADTREEBELOW")) {
                ActionUtil.putIntoActionMap(tree, acUnloadTreeBelow, JComponent.WHEN_FOCUSED);
            }
            if (token.endsWith("UNLOADTREEALL")) {
                ActionUtil.putIntoActionMap(appFrame.getRootPane(), acUnloadTreeAll);
            }
            if (token.endsWith("CHANGECONNECTION")) {
                ActionUtil.putIntoActionMap(appFrame.getRootPane(), acChangeConnection);
            }
            if (token.endsWith("CLOSECONNECTION")) {
                ActionUtil.putIntoActionMap(appFrame.getRootPane(), acExit);
            }
        }
//        log.debug("end");
    }

    public void ribbonActionPerformed(ActionEvent e, String token) {

        log.debug("Fire action " + token);
        if (token.startsWith("FRONTEND.")) {
            Action selectedAction = null;
            if (token.endsWith("HELP")) {
                selectedAction = acHelp;
            }
            if (token.endsWith("ABOUT")) {
                selectedAction = acAbout;
            }
            if (token.endsWith("UNLOADTREEBELOW")) {
                selectedAction = acUnloadTreeBelow;
            }
            if (token.endsWith("UNLOADTREEALL")) {
                selectedAction = acUnloadTreeAll;
            }
            if (token.endsWith("CHANGECONNECTION")) {
                selectedAction = acChangeConnection;
            }
            if (token.endsWith("CLOSECONNECTION")) {
                selectedAction = acExit;
            }
            if (selectedAction != null) {
                selectedAction.actionPerformed(e);
            }
        } else {
            MenuItem item = base.createRibbonMenuEntry(token);
            executeBaseActionLater(item);
        }
        log.debug("end");
    }

}
