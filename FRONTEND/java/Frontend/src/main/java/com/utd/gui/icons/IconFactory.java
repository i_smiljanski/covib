package com.utd.gui.icons;

import com.uptodata.isr.gui.util.Colors;

import javax.swing.*;
import java.awt.*;
import java.awt.geom.AffineTransform;
import java.awt.geom.GeneralPath;
import java.awt.geom.Line2D;
import java.util.ArrayList;
import java.util.List;


/**
 * Source for default Icons to move icons: left, right,up,down allLeft,
 * allRight. <br>
 * and some others
 *
 * @author PeterBuettner.de
 */
public class IconFactory {

    /**
     * (Hack), order is right,left,rightAll,leftAll,up,down iconwidth is 20,
     * trianglebase 7, if String is returned, sizes may differ creates the icons
     * for left/right/up/down; Icon or String
     *
     * @return
     */
    public static Object[] createMoverIcons() {

        if (false)
            // images

            return new Object[]{"RightOne.png", "LeftOne.png", "RightAll.png",
                    "LeftAll.png", "stock_up-16.png", "stock_down-16.png",};

        List icons = new ArrayList();
        float triangleWidth = 7;
        int size = 20;
        Paint color = Colors.getUIControlText();

        GeneralPath triRight = createTranglePath(triangleWidth);

        GeneralPath triRightTwo = new GeneralPath(triRight);
        triRightTwo.append(triRight.createTransformedShape(AffineTransform
                .getTranslateInstance(triangleWidth * 0.8, 0)), false);

        AffineTransform mirrorX = AffineTransform.getScaleInstance(-1, 1);
        AffineTransform rotateLeft = AffineTransform.getRotateInstance(-Math.PI / 2);
        AffineTransform rotateRight = AffineTransform.getRotateInstance(Math.PI / 2);

        // right,left,rightAll,leftAll,up,down
        icons.add(makeImageIcon(triRight, color, size));
        icons.add(makeImageIcon(triRight.createTransformedShape(mirrorX), color, size));

        icons.add(makeImageIcon(triRightTwo, color, size));
        icons
                .add(makeImageIcon(triRightTwo.createTransformedShape(mirrorX), color, size));

        icons
                .add(makeImageIcon(triRight.createTransformedShape(rotateLeft), color, size));
        icons
                .add(makeImageIcon(triRight.createTransformedShape(rotateRight), color, size));

        return icons.toArray();
    }// ----------------------------------------------------------------------

    /**
     * creates a GeneralPath of a triangle, origin in (0,0), pointing to the
     * right, spanning down, height is 2*width
     * <p>
     * <pre>
     *
     *  | \
     *  |  \
     *  |   \
     *  |   /
     *  |  /
     *  | /
     *
     * </pre>
     *
     * @param width
     * @return
     */
    private static GeneralPath createTranglePath(float width) {

        GeneralPath triRight = new GeneralPath(GeneralPath.WIND_NON_ZERO);
        triRight.moveTo(0, 0);
        triRight.lineTo(width, width);
        triRight.lineTo(0, 2 * width);
        triRight.closePath();
        return triRight;

    }

    /**
     * creates a imageicon out of the shape, since buttons only automatic create
     * disabled icons from ImageIcon, maybe later we extend ImageIcon
     */
    private static Icon makeImageIcon(Shape shape, Paint paint, int size) {

        return IconTools.toImageIcon(new BorderIcon(new ShapeIcon(shape, paint), size));

    }

    /**
     * Icon like a checkbox/menuCheckBox one uses ControlText as Color
     *
     * @param size
     * @return
     */
    public static Icon getCheckIcon(int size) {

        return getCheckIcon(Colors.getUIControlText(), size);

    }

    /**
     * Icon like a checkbox/menuCheckBox one
     *
     * @param paint
     * @param size
     * @return
     */
    public static Icon getCheckIcon(Paint paint, int size) {

        GeneralPath gp = new GeneralPath(new Line2D.Double(0, 24, 30, 54));
        gp.lineTo(84, 0);
        gp.lineTo(84, 0);
        gp.lineTo(84, 35);
        gp.lineTo(24, 89);
        gp.lineTo(0, 59);
        gp.closePath();
        float scale = size / 90f;
        Shape shape = gp.createTransformedShape(AffineTransform.getScaleInstance(scale,
                scale));
        return IconTools.toImageIcon(new BorderIcon(new ShapeIcon(shape, paint), size));

    }

    /**
     * creates an icon of a triangle (size is base of triangle, height is size/2
     * rotated by rotateDegrees in 2*PI units, color is ControlText
     *
     * @param size
     * @param rotateDegrees
     * @return
     */
    public static Icon getTriangleIcon(int size, double rotateDegrees) {

        Paint color = Colors.getUIControlText();
        GeneralPath triRight = createTranglePath(size / 2f);
        triRight.transform(AffineTransform.getRotateInstance(rotateDegrees));
        return makeImageIcon(triRight, color, size);

    }

}