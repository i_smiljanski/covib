package com.utd.stb.gui.wizzard.choosers;

import com.jidesoft.grid.CellRendererManager;
import com.jidesoft.grid.QuickTableFilterField;
import com.jidesoft.grid.TableModelWrapperUtils;
import com.utd.gui.action.ActionUtil;
import com.utd.gui.controls.table.ListViewTable;
import com.utd.gui.controls.table.TableTools;
import com.utd.gui.controls.table.model.AbstractListTableModel;
import com.utd.gui.controls.table.model.MovableRowsTableModel;
import com.utd.gui.controls.table.model.StayInOrderListTableModel;
import com.utd.gui.event.RepaintScrollerOnSelectionChange;
import com.utd.gui.event.TableDblClickAdapter;
import com.utd.stb.gui.itemTools.DisplayableItemTableCellRenderer;
import com.utd.stb.gui.itemTools.DisplayableItemsConv;
import com.utd.stb.gui.itemTools.DisplayableItemsTableModelMaker;
import com.utd.stb.gui.itemTools.DisplayableItemsWrap;
import com.utd.stb.gui.swingApp.exceptions.ExceptionHandler;
import com.utd.stb.inter.application.BackendException;
import com.utd.stb.inter.wizzard.formtypes.FormDataListOutOfList;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import javax.swing.*;
import javax.swing.event.ListSelectionEvent;
import javax.swing.event.ListSelectionListener;
import javax.swing.table.TableCellRenderer;
import javax.swing.table.TableModel;
import java.awt.event.MouseEvent;
import java.util.Collection;
import java.util.List;


/**
 * @author PeterBuettner.de
 */
class TwoTableChooser extends TwoComponentChooser {
    public static final Logger log = LogManager.getRootLogger();

    private JTable tblSrc;
    private JTable tblDst;
    private FormDataListOutOfList data;
    private JComponent content;

    private SourceTableModel sourceModel;
    private DestTableModel destModel;

    //    JTextField leftFilter;
//    JTextField rightFilter;
    private QuickTableFilterField leftFilter;
    private QuickTableFilterField rightFilter;

    public TwoTableChooser(ExceptionHandler exceptionHandler, FormDataListOutOfList data) {

        super(exceptionHandler);
        this.data = data;
    }// ----------------------------------------------------------------------

    public void saveData() throws BackendException {
        data.setSelectedItems(new DisplayableItemsWrap(((DestTableModel) TableModelWrapperUtils.getActualTableModel(tblDst.getModel()))
                .getRows()));
        data.saveData();

    }

    public boolean isValid() {

        return true;
    }

    public JComponent getContent() {

        if (content != null) return content;

        createActions();
        List allItems = DisplayableItemsConv.asList(data.getAvailableItems());
        List selItems = DisplayableItemsConv.asList(data.getSelectedItems());

        // MCD, 26.Jan 2006, Reihenfolge vertauscht
        destModel = new DestTableModel(); // empty on start
        sourceModel = new SourceTableModel(allItems);

        destModel.addRows(selItems);
        sourceModel.removeRows(selItems);

        DisplayableItemTableCellRenderer objectRenderer = new DisplayableItemTableCellRenderer();

        ListSelectionListener lslActionManager = new ListSelectionListener() {

            public void valueChanged(ListSelectionEvent e) {

                //			if(e.getValueIsAdjusting())return; // no, we are fast enough
                // without 'invokeLater' the repeating up/down buttons won't
                // work,
                // probably the ButtonModel statechange series is broken, button
                // disabled in between
                SwingUtilities.invokeLater(new Runnable() {

                    public void run() {

                        manageActionEnabeling();
                    }
                });
            }
        };
        tblSrc = createTable(sourceModel, objectRenderer, lslActionManager);
        tblDst = createTable(destModel, objectRenderer, lslActionManager);


        leftFilter = new QuickTableFilterField(tblSrc.getModel());
        tblSrc.setModel(leftFilter.getDisplayTableModel());
        leftFilter.setTable(tblSrc);

        rightFilter = new QuickTableFilterField(tblDst.getModel());
        tblDst.setModel(rightFilter.getDisplayTableModel());
        rightFilter.setTable(tblDst);

        cmpLeft = tblSrc;
        cmpRight = tblDst;

        content = getComponentPanel(data.getLabelAllItems(), data.getLabelChoosenItems());

        // move only clicked, not all selected, since ctrl-dblclick maybe
        // confusing
        tblSrc.addMouseListener(new TableDblClickAdapter() {

            public void rowDblClick(JTable table, int row, MouseEvent e) {

                moveRows(tblSrc, tblDst, TableModelWrapperUtils.getActualRowsAt(leftFilter.getDisplayTableModel(), new int[]{row}, false));
            }
        });
        tblDst.addMouseListener(new TableDblClickAdapter() {

            public void rowDblClick(JTable table, int row, MouseEvent e) {

                moveRows(tblDst, tblSrc, TableModelWrapperUtils.getActualRowsAt(rightFilter.getDisplayTableModel(), new int[]{row}, false));
            }
        });

        // ... actions .......................
        tblSrc.getInputMap().put(KeyStroke.getKeyStroke("SPACE"), "acToRightOne");
        tblSrc.getActionMap().put("acToRightOne", acToRightOne);
        tblDst.getInputMap().put(KeyStroke.getKeyStroke("SPACE"), "acToLeftOne");
        tblDst.getActionMap().put("acToLeftOne", acToLeftOne);

        ActionUtil.putIntoActionMap(content, new Action[]{acToLeftAll, acToLeftOne,
                        acToRightOne, acToRightAll, acMoveUp, acMoveDown,},
                JComponent.WHEN_ANCESTOR_OF_FOCUSED_COMPONENT // fire
                // when
                // focus
                // in
                // this
                // content
                //	JComponent.WHEN_IN_FOCUSED_WINDOW // fire always, but Navlist
                // eats Ctrl-A
        );

        manageActionEnabeling();

        // ... startup column sizes: ......................
        TableTools.autoSizeColumns(tblSrc, false);
        TableTools.autoSizeColumns(tblDst, false);
        TableTools.syncColumnSizes(tblSrc, tblDst);

        return content;
    }// ----------------------------------------------------------------------

    private ListViewTable createTable(TableModel model, TableCellRenderer objectRenderer,
                                      ListSelectionListener lslActionManager) {

        ListViewTable t = new ListViewTable(model) {

            {
                setProcessCtrlA(true);
            }
        };


        t.setSortable(false);
        t.setDefaultRenderer(Object.class, objectRenderer);
        CellRendererManager.registerRenderer(Object.class, objectRenderer);
        TableTools.selectFirstRow(t, false);
        t.getSelectionModel().addListSelectionListener(lslActionManager);
        t.putClientProperty("com.utd.gui.lookAndFeel.FlatWindowsScrollBarUI.PAINT_SELECTION_MARKER", "V");
        t.getSelectionModel().addListSelectionListener(new RepaintScrollerOnSelectionChange(t, false));

        return t;
    }

    protected JComponent createLeftBottomRow() {

//        rightFilter = new JTextField("");
//        return createFilterBar( tblSrc, rightFilter );
        return leftFilter;
    }

    protected JComponent createRightBottomRow() {

//    	leftFilter = new JTextField("");
//        return createFilterBar( tblDst, leftFilter );
        return rightFilter;
    }

    protected void onMoveUp() {

        moveSelectedRowsUpDown(tblDst, true, rightFilter);
    }

    protected void onMoveDown() {

        moveSelectedRowsUpDown(tblDst, false, rightFilter);
    }

    protected void onChooseSelected() {

        moveSelectedRows(tblSrc, tblDst, leftFilter);
    }

    protected void onUnchooseSelected() {

        moveSelectedRows(tblDst, tblSrc, rightFilter);
    }

    protected void onChooseAll() {

        tblSrc.selectAll();
        moveSelectedRows(tblSrc, tblDst, leftFilter);
    }

    protected void onUnchooseAll() {

        tblDst.selectAll();
        moveSelectedRows(tblDst, tblSrc, rightFilter);
    }

    private void moveSelectedRowsUpDown(JTable table, boolean up, QuickTableFilterField filter) {

        int[] sel = table.getSelectedRows();
        if (sel.length == 0) return;

        DestTableModel src = (DestTableModel) filter.getTableModel();
        List data = src.getRows(TableModelWrapperUtils.getActualRowsAt(filter.getDisplayTableModel(), sel, false));

        if (up) {
            int[] upSel = new int[sel.length];
            for (int i = 0; i < sel.length; i++) {
                upSel[i] = sel[i] - 1;
            }
            src.moveUp(TableModelWrapperUtils.getActualRowsAt(filter.getDisplayTableModel(), sel, false),
                    TableModelWrapperUtils.getActualRowsAt(filter.getDisplayTableModel(), upSel, false));
        } else {
            int[] downSelSel = new int[sel.length];
            for (int i = 0; i < sel.length; i++) {
                downSelSel[i] = sel[i] + 1;
            }
            src.moveDown(TableModelWrapperUtils.getActualRowsAt(filter.getDisplayTableModel(), sel, false),
                    TableModelWrapperUtils.getActualRowsAt(filter.getDisplayTableModel(), downSelSel, false));
        }
        TableTools.setSelectionByList(table, data, true);
    }

    //  -----------------------------------------------------------------

    /**
     * Move the selected rows from src to dest, rows in dest are selected,
     * selection in src is set to last 'focused' row (if that was moved, to the
     * last one)
     *
     * @param tSrc
     * @param tDst
     */

    private void moveSelectedRows(JTable tSrc, JTable tDst, QuickTableFilterField filter) {
        int[] selectedRows = tSrc.getSelectedRows();
//        System.out.println("Thread.currentThread() = " + Thread.currentThread().getStackTrace()[2]);
        moveRows(tSrc, tDst, TableModelWrapperUtils.getActualRowsAt(filter.getDisplayTableModel(), selectedRows, false));
        //... change source sel+focus .................................
        int i = tSrc.getSelectionModel().getLeadSelectionIndex() - selectedRows.length + 1;
        if (i >= tSrc.getRowCount()) i = tSrc.getRowCount() - 1;
        if (i >= 0) tSrc.getSelectionModel().setSelectionInterval(i, i);
    }

    /**
     * Move the rows from src to dest, rows in dest are selected, selection in
     * src is unchanged useDestSel if true moves the rows after the selected one
     *
     * @param tSrc
     * @param tDst
     * @param rows in src indices
     */
    private void moveRows(JTable tSrc, JTable tDst, int[] rows) {
        if (rows.length == 0) return;
        AbstractListTableModel src = (AbstractListTableModel) TableModelWrapperUtils.getActualTableModel(tSrc.getModel());
        AbstractListTableModel dst = (AbstractListTableModel) TableModelWrapperUtils.getActualTableModel(tDst.getModel());
        List data = src.getRows(rows);
        List srcRows = src.getRows(src.findRows(data, false));

        if (tDst == tblDst) // add special
            dst.addRows(getDestTableInsertionPoint(), data);
        else
            dst.addRows(data);
        src.removeRows(rows, true);

        TableTools.setSelectionByList(tDst, srcRows, true);

        //  TableTools.setSelectionByIndices( tDst, dst.findRows( data, false ), true );
    }// -----------------------------------------------------------------

    /**
     * return the index where new entries should be inserted, currently we add:
     * <ul>
     * <li>after the last selected element
     * <li>at the end if nothing is selected
     * </ul>
     *
     * @return
     */
    protected int getDestTableInsertionPoint() {

        int[] sel = tblDst.getSelectedRows();
        int n = tblDst.getRowCount();
        if (sel.length == 0 || n == 0) return n;
        return sel[sel.length - 1] + 1;
    }

    /**
     * dis- or enable Actions, depending on the selectionstate of the lists
     */
    private void manageActionEnabeling() {

        acToRightOne.setEnabled(tblSrc.getSelectedRowCount() > 0);
        acToRightAll.setEnabled(tblSrc.getRowCount() > 0);

        acToLeftOne.setEnabled(tblDst.getSelectedRowCount() > 0);
        acToLeftAll.setEnabled(tblDst.getRowCount() > 0);

        acMoveUp.setEnabled(destModel.canMove(tblDst.getSelectedRows(), true));
        acMoveDown.setEnabled(destModel.canMove(tblDst.getSelectedRows(), false));
    }

    /* (non-Javadoc)
     * @see com.utd.stb.gui.wizzard.choosers.AbstractChooser#getFilterFieldValue()
     */
    public String getFilterFieldValue() {
        if (leftFilter.getText() == null && rightFilter.getText() == null) return null;
        if (rightFilter.getText() == null ||
                rightFilter.getText().length() == 0) return leftFilter.getText();
        if (leftFilter.getText() != null ||
                leftFilter.getText().length() == 0) return rightFilter.getText();
        return null;
    }

    /* (non-Javadoc)
     * @see com.utd.stb.gui.wizzard.choosers.AbstractChooser#setFilterFieldValue()
     */
    public void setFilterFieldValue(String regExpr) {
        leftFilter.setText(regExpr);
        rightFilter.setText(regExpr);
        /*if ( regExpr.indexOf(" | ") != -1 ) {
            leftFilter.setTranslation(regExpr.substring(0, regExpr.indexOf(" | ")));
			rightFilter.setTranslation(regExpr.substring(regExpr.indexOf(" | ") + 3));
		}*/
    }

    /**
     * All data changes methods fires the adequate TableEvents, all data are of
     * type DisplayableItem, rows are movable. On creation fill in List of
     * DisplayableItem, no check is done!
     */
    private static class DestTableModel extends MovableRowsTableModel {

        protected DisplayableItemsTableModelMaker modelMaker;

        DestTableModel() {

            List list = this.getRows();
            this.modelMaker = new DisplayableItemsTableModelMaker(DisplayableItemsConv.asDisplayableItems(this.getRows()));

        }

        public int getColumnCount() {

            return modelMaker.getColumnCount();
        }

        public Object getValueAt(int row, int col) {
            // System.out.println("DestTableModel getValueAt = " + row);

            return modelMaker.getValueAt(getRow(row), col);
        }

        public String getColumnName(int col) {

            return modelMaker.getColumnName(col);
        }
    }

    /**
     * All data changes methods fires the adequate TableEvents, all data are of
     * type DisplayableItem, rows keep their initial order on re-adding after
     * remove, <b>NO DUPE ROWS </b>, see
     * {@link com.utd.gui.controls.table.model.StayInOrderListTableModel}
     */
    private static class SourceTableModel extends StayInOrderListTableModel {

        protected DisplayableItemsTableModelMaker modelMaker;

        /**
         * fill in List of DisplayableItem, no check is done
         */
        SourceTableModel(Collection collection) {

            super(collection);
            this.modelMaker = new DisplayableItemsTableModelMaker(DisplayableItemsConv.asDisplayableItems(this.getRows()));
        }

        public int getColumnCount() {

            return modelMaker.getColumnCount();
        }

        public Object getValueAt(int row, int col) {
            // System.out.println("SourceTableModel getValueAt = " + row);

            return modelMaker.getValueAt(getRow(row), col);
        }

        public String getColumnName(int col) {

            return modelMaker.getColumnName(col);
        }
    }
}