package com.utd.stb.gui.userinput.swing;

import com.jgoodies.forms.factories.ComponentFactory;
import com.jgoodies.forms.factories.DefaultComponentFactory;
import com.jidesoft.combobox.DateComboBox;
import com.jidesoft.combobox.DefaultDateModel;
import com.jidesoft.combobox.TableComboBox;
import com.jidesoft.combobox.TableComboBoxSearchable;
import com.jidesoft.grid.CellRendererManager;
import com.jidesoft.grid.SortableTable;
import com.jidesoft.swing.MultilineLabel;
import com.uptodata.isr.gui.components.fileChooser.ImageFileChooserPanel;
import com.uptodata.isr.gui.lookAndFeel.TweakUI;
import com.uptodata.isr.gui.util.Colors;
import com.uptodata.isr.server.utils.fileSystem.WindowsReqistry;
import com.uptodata.isr.utils.exceptions.MessageStackException;
import com.utd.gui.controls.BooleanComboBox;
import com.utd.gui.controls.DocumentSizeFilter;
import com.utd.gui.event.SelectAllFocusListener;
import com.utd.gui.util.DialogUtils;
import com.utd.stb.gui.itemTools.DisplayableItemTableCellRenderer;
import com.utd.stb.gui.itemTools.DisplayableItemsImplUnmodifiable;
import com.utd.stb.gui.itemTools.DisplayableItemsTableModel;
import com.utd.stb.gui.swingApp.exceptions.ExceptionTools;
import com.utd.stb.inter.application.BackendException;
import com.utd.stb.inter.items.DisplayableItem;
import com.utd.stb.inter.items.DisplayableItems;
import com.utd.stb.inter.userinput.*;
import org.apache.commons.lang.ObjectUtils;
import org.apache.commons.lang.StringUtils;
import org.apache.commons.lang.SystemUtils;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.imgscalr.Scalr;
import resources.Res;

import javax.imageio.ImageIO;
import javax.swing.*;
import javax.swing.border.EmptyBorder;
import javax.swing.filechooser.FileFilter;
import javax.swing.table.TableModel;
import javax.swing.text.DefaultEditorKit;
import javax.swing.text.JTextComponent;
import java.awt.*;
import java.awt.event.*;
import java.awt.image.BufferedImage;
import java.beans.PropertyChangeEvent;
import java.beans.PropertyChangeListener;
import java.io.ByteArrayInputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.text.DateFormat;
import java.text.NumberFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.time.ZonedDateTime;
import java.util.*;
import java.util.regex.Pattern;


/**
 * Create JComponents from UserInput, one big Monsterclass :-) may split into
 * seperate JComponent derivates
 *
 * @author PeterBuettner.de
 */
public class UIOComponentFactory {
    public static final Logger log = LogManager.getRootLogger();

    /**
     * we use it for readonly JTextComponents
     */
    private static SelectAllFocusListener selectAllFocusListener = new SelectAllFocusListener();

    /**
     * used for textcomponents to insert a Tab: '\t'
     */
    private static final KeyStroke ksInsertTab = KeyStroke.getKeyStroke("ctrl TAB");

    /**
     * Create a JComponent from a UserInput, ready for use
     *
     * @param userInput
     * @return
     */
    static JComponent createComponent(UserInput userInput) throws MessageStackException {
        try {
            Class type = userInput.getType();
            Object data = userInput.getData();
            boolean isReadOnly = userInput.isReadOnly();
            boolean isChangeable = userInput.isChangeable();

            JComponent c;
            UIODataHandler h = null;

            if (FileInput.class.isAssignableFrom(type)) {
                // ... File ..................................................
                FileInput fi = (FileInput) data;
                log.debug(fi.getText());
                if (data instanceof DirectoryInputImpl) {
                    c = new DirChooserTextField(Res.getII("folder.gif"), Res.getString("dialog.filechooser.title"),
                            fi.getText(), fi.hasLov(), isReadOnly);
                    h = new UIODataHandler.PlainTextDataHandler((DirChooserTextField) c, userInput);
                } else {
                    c = new FileChooserTextField(Res.getII("folder.gif"), Res.getString("dialog.filechooser.title"),
                            fi.getText(), fi.hasLov(), isReadOnly);
                    h = new UIODataHandler.PlainTextDataHandler((FileChooserTextField) c, userInput);
                }
            } else if (Boolean.class.isAssignableFrom(type)) {
                // ... Boolean ..................................................
                if (!userInput.isEmptyAllowed()) {
                    c = new JCheckBox(userInput.getLabel(), null, Boolean.TRUE.equals(data));
                    h = new UIODataHandler.CheckBoxDataHandler((JCheckBox) c, userInput);
                    ((JCheckBox) c).addItemListener(new ComponentsItemListener());
                } else {
                    c = new BooleanComboBox((Boolean) data);
                    h = new UIODataHandler.BooleanComboDataHandler((BooleanComboBox) c, userInput);
                }
            } else if (DisplayableItemInput.class.isAssignableFrom(type)) {
                //log.debug("userInput type = " + type + " data = " + data + " isReadOnly = " + isReadOnly);
                // ... Popup with
                // DisplayableItems.......................................
                final DisplayableItemInput dii = (DisplayableItemInput) userInput.getData();
                if (isReadOnly) { // don't create any popup
                    c = new JTextField(20);
                    DisplayableItem item = dii.getSelectedItem();
                    Object value = item == null ? null : item.getData();
                    // TODO later data maybe no string but date etc.
                    ((JTextComponent) c).setText(value == null ? null : value.toString());
                } else {

                    TableModel model = new LazyDisplayableItemsTableModel() {
                        protected DisplayableItems loadItemsLazy() {
                            try {
                                return dii.getAvailableItems();
                            } catch (BackendException e) {
                                ExceptionTools.callExceptionHandler(new TextField(), e, true);
                                // never called?
                                return new DisplayableItemsImplUnmodifiable(Collections.EMPTY_LIST, null);
                            }
                        }
                    };
                    c = new TableComboBoxField(model, dii, isChangeable);
                    h = new UIODataHandler.ComboBoxDataHandler((TableComboBox) c, userInput);
                }
            } else if (Number.class.isAssignableFrom(type)) {
                // ... Number ..................................................
                NumberFormat numberFormat = FormatFactory.getNumberFormat(type);
                numberFormat.setGroupingUsed(false);
                numberFormat.setMaximumFractionDigits(16);
                c = new NumberField(numberFormat, (Number) data) {
                    public boolean isEditValid() { // @@PEB-2005-05-01@@

                        /*
                         * well, first commit, if an exception is thrown assume not
                         * valid let's see what happens on empty content
                         *
                         */
                        try {
                            commitEdit();
                        } catch (ParseException e) {
                            return false;
                        }

                        if (!super.isEditValid()) return false;
                        Number number = (Number) getValue();
                        if (number == null) return false;// difference between
                        // valid/empty is
                        // checked at another place

                        return number.doubleValue() >= 0;
                    }
                };
                h = new UIODataHandler.FormattedTextDataHandler((JFormattedTextField) c, userInput);
            } else if (DateInput.class.isAssignableFrom(type)) {
                // ... DateInput ..................................................
                DateInput di = (DateInput) data;
                DefaultDateModel model = new DefaultDateModel();
                c = new DateComboBox(model);

                ZonedDateTime d = di.getDate();

                ((DateComboBox) c).setTimeDisplayed(true);

                DateFormat dateFormat = null;
                if (SystemUtils.IS_OS_WINDOWS) {
                    log.debug("windows");
                    try {
                        String pattern = WindowsReqistry.readRegistry(WindowsReqistry.HKEY_CURRENT_USER + "\\Control Panel\\International", "sShortDate") +
                                " " + WindowsReqistry.readRegistry(WindowsReqistry.HKEY_CURRENT_USER + "\\Control Panel\\International", "sTimeFormat");
                        if (pattern != null) {
                            log.debug(pattern);
                            dateFormat = new SimpleDateFormat(pattern);
                        }
                    } catch (Exception e) {
                        log.error(e);
                    }
                }
                if (dateFormat == null) {
                    dateFormat = DateFormat.getDateTimeInstance(DateFormat.SHORT, DateFormat.LONG);
                }

                ((DateComboBox) c).setFormat(dateFormat);
                if (d != null) {
                    Calendar currentDate = GregorianCalendar.from(d);
                    ((DateComboBox) c).setCalendar(currentDate);
                }

                log.debug("currentDate = " + ((DateComboBox) c).getDate());
                h = new UIODataHandler.ComboBoxDataHandler((DateComboBox) c, userInput);
            } else if (TextInput.class.isAssignableFrom(type)) {
                // ... TextInput ..................................................
                TextInput ti = ((TextInput) data);
                Integer maxLength = ti.getMaxLength();
                log.debug("maxLength=" + maxLength);

                JTextComponent tc;
                if (ti.isSecret()) {

                    tc = new JPasswordField(20);
                    ((JPasswordField) tc).setEchoChar('\u25cf');// full round
                    // bullet looks also good with default, but with tahoma it is more elegant:
                    tc.setFont(Font.decode("Tahoma-" + tc.getFont().getSize()));

                } else if (ti.isMultiLineAllowed()) {

                    final int cols = Math.min(50, (maxLength == null ? 0 : maxLength));
                    final int rows = Math.min(4, (maxLength == null ? 4 : maxLength / cols));
                    JTextArea ta = new JTextArea("", rows, cols);
                    ta.setFont(UIManager.getFont("EditorPane.font"));
                    ta.setLineWrap(true);
                    ta.setWrapStyleWord(true);
                    // modified by MCD, 09.08.2005,
                    // if the textarea is read_only, color back- and foreground
                    if (isReadOnly) {

                        ta.setBackground(Colors.getItemHiliteInactiveBack());
                        ta.setForeground(Colors.getItemHiliteInactiveText());
                    }

                    fixupFocusTraversal(ta);

                    tc = ta;

                } else {
                    tc = new JTextField(20);
                }

                if (maxLength != null) {
                    DocumentSizeFilter.setFilter(tc, maxLength);
                }
                tc.setText(ti.getText());
                h = new UIODataHandler.PlainTextDataHandler(tc, userInput);
                c = tc;

            } else if (MetaInput.class.isAssignableFrom(type)) {
                // ... GroupLabel ..................................................
                ComponentFactory cf = DefaultComponentFactory.getInstance();
                c = cf.createSeparator(userInput.getLabel(), SwingConstants.LEFT);
            } else if (BufferedImage.class.isAssignableFrom(type)) {
                BufferedImage bi = (BufferedImage) data;
                final JLabel finalC;
                if (data != null) {
                    BufferedImage scaledImage = Scalr.resize(bi, Scalr.Method.QUALITY, Scalr.Mode.FIT_TO_WIDTH,
                            400, Scalr.OP_ANTIALIAS);
                    log.debug(scaledImage);
                    finalC = new JLabel(new ImageIcon(scaledImage));
                } else {
                    finalC = new JLabel(Res.getII("header/msgBox_Question.gif"));   //todo: empty image
                }
                c = finalC;
                h = new UIODataHandler.ImageDataHandler(c, userInput);
                final UIODataHandler finalH = h;
                c.addMouseListener(new MouseAdapter() {
                    @Override
                    public void mouseClicked(MouseEvent e) {
                        BufferedImage image;
                        if (e.getClickCount() == 2 && !e.isConsumed()) {
                            File file = new ImageFileChooserPanel("Attach", "Just Images").chooseFile();  //todo: text
                            if (file != null) {
                                try {
                                    FileInputStream fileInputStream = new FileInputStream(file);
                                    byte[] bytes = new byte[(int) file.length()];
                                    fileInputStream.read(bytes);
                                    fileInputStream.close();
                                    image = ImageIO.read(new ByteArrayInputStream(bytes));
                                    BufferedImage scaledImage = Scalr.resize(image, Scalr.Method.QUALITY, Scalr.Mode.FIT_TO_WIDTH,
                                            400, Scalr.OP_ANTIALIAS);
                                    finalC.setIcon(new ImageIcon(scaledImage));
//                                log.debug(((JLabel) finalC).getIcon().getIconHeight());
                                    log.debug(scaledImage);
                                    ((UIODataHandler.ImageDataHandler) finalH).setImg(scaledImage);
                                } catch (IOException e1) {
                                    log.error(e1);
                                }
                            }
                        }
                    }
                });
            } else if (String.class.isAssignableFrom(type)) {
                MultilineLabel lbl = new MultilineLabel((String) data);
                lbl.setOpaque(true);
                lbl.setLineWrap(true);
                // lbl.setBorder(new EmptyBorder(0, 0,0,0));
                c = lbl;
            } else {
                // ... unknown........................................
                JLabel lbl = new JLabel("Type '" + type.getName() + "' not implemented");
                lbl.setBackground(Color.RED);
                lbl.setOpaque(true);
                c = lbl;
            }

            // ... post makeup
            // ..........................................................

            if (c instanceof JTextComponent) {
                ((JTextComponent) c).setEditable(!isReadOnly);
                if (isReadOnly) c.addFocusListener(selectAllFocusListener);
            } else {
                if (isReadOnly) c.setEnabled(false);
            }

            if (c instanceof JFormattedTextField) {
                ((JFormattedTextField) c).setFocusLostBehavior(JFormattedTextField.COMMIT);
                ((JTextField) c).setHorizontalAlignment(JTextField.LEFT);
            }

            if (h != null) {
                c.putClientProperty(UIODataHandler.class, h);
                addBackColorHandler(h);
            }

            return c;
        } catch (Exception e) {
            log.error(e);
            throw new MessageStackException(e, MessageStackException.SEVERITY_CRITICAL, "component '" + userInput.getLabel() + "' could not be created", MessageStackException.getCurrentMethod());
        }
    }


    /**
     * setup Tab/shift-Tab as focus traversal, ctrl-Tab to insert a tab-char,
     * used for TextComponents (mostly multiline) but should woerk with every
     * JComponent
     *
     * @param component
     */
    private static void fixupFocusTraversal(JComponent component) {

        component.setFocusTraversalKeys(KeyboardFocusManager.FORWARD_TRAVERSAL_KEYS,
                Collections.singleton(KeyStroke.getKeyStroke("TAB")));
        component.setFocusTraversalKeys(KeyboardFocusManager.BACKWARD_TRAVERSAL_KEYS,
                Collections.singleton(KeyStroke.getKeyStroke("shift TAB")));
        component.getInputMap().put(ksInsertTab, DefaultEditorKit.insertTabAction);

    }

    /**
     * add a handler to change backColor depending on valid & empty, also sets
     * color from the current state
     *
     * @param handler
     */
    static void addBackColorHandler(UIODataHandler handler) {

        UserInput ui = handler.getUserInput();
        if (ui.isReadOnly()) return;

        final JComponent c = handler.getComponent();
        final Color color = c.getBackground();
        final boolean isEmptyAllowed = ui.isEmptyAllowed();

        DataChangeListener listener = e -> {
            UIODataHandlerBase h = e.getUIODataHandler();
            h.commitData();
//                 log.debug(h.isValid()+";"+h.isEmpty()+";"+isEmptyAllowed);
            if (!h.isValid()) {
                c.setBackground(Colors.getDataBackInvalid());
            } else if (!isEmptyAllowed && h.isEmpty()) {
                c.setBackground(Colors.getDataBackIllegalEmpty());
            } else {
                c.setBackground(color);
            }
        };
        handler.addChangeListener(listener);
        listener.dataChanged(new DataChangeEvent(handler));// set initial

    }


    /**
     * a text field that shows that it may popup a box, where the icon is shown
     * maybe changed in the future.
     */
    private static class PopupTextField extends JTextField {

        private Icon icon;

        /**
         * @param icon if null it behaves like a normal JTextField
         */
        public PopupTextField(Icon icon) {
            this.icon = icon;
        }

        protected void paintComponent(Graphics g) {

            super.paintComponent(g);
            if (icon == null) return;

            Insets insets = super.getInsets();
            int yi = (getHeight() - icon.getIconHeight()) / 2;
            icon.paintIcon(this, g, insets.left, yi);

        }

        public Insets getInsets() {

            Insets insets = super.getInsets();
            if (icon == null) return insets;
            insets.left += icon.getIconWidth() + 2;
            return insets;

        }

    }

    /**
     * a text field that shows that it may popup a box, where the icon is shown
     * maybe changed in the future.
     */
    private static class FileChooserTextField extends PopupTextField {

        JFileChooser filechooser;
        String title;
        boolean hasLov;

        /**
         * @param icon if null it behaves like a normal JTextField
         */
        FileChooserTextField(Icon icon, String title, String text, boolean hasLov, boolean readOnly) {

            super(icon);

            try {
                this.filechooser = new JFileChooser();
            } catch (Exception e) {
                // for Vista and Windows 7
                try {
                    UIManager.setLookAndFeel(UIManager.getCrossPlatformLookAndFeelClassName());
                } catch (Exception e1) {
                    log.error(e1);
                }
                this.filechooser = new JFileChooser();
                TweakUI.setLookAndFeel();
            }

            this.title = title;
            this.hasLov = hasLov;
            if (!readOnly) {
                addMouseListener(new MouseListener() {

                    public void mouseClicked(MouseEvent e) {
                        if (e.getClickCount() == 2) createDialog(e);
                    }

                    public void mouseReleased(MouseEvent arg0) {
                    }

                    public void mouseEntered(MouseEvent arg0) {
                    }

                    public void mouseExited(MouseEvent arg0) {
                    }

                    public void mousePressed(MouseEvent arg0) {
                    }
                });
            }

            try {
                if (!(text == null || text.length() == 0)) {
                    Pattern pattern1 = Pattern.compile("\\[user.home]");
                    filechooser.setSelectedFile(new File(pattern1.matcher(text).replaceAll((System.getProperty("user" +
                            ".home").replaceAll("\\\\", "\\\\\\\\")))));
                }
            } catch (Exception e) {
                log.error(e);
            }
            filechooser.setFileSelectionMode(JFileChooser.FILES_ONLY);
            filechooser.setMultiSelectionEnabled(false);
            filechooser.setControlButtonsAreShown(false);

            String filechoosers;
            if (!(filechoosers = Res.getString("dialog.filechooser.types")).equals("dialog.filechooser.types")) {
                String[] programs = filechoosers.split(";");
                for (final String program : programs) {
                    filechooser.addChoosableFileFilter(new FileFilter() {

                        String[] extensions = Res.getString("dialog.filechooser.type.ext." + program).split(";");
                        String name = Res.getString("dialog.filechooser.type.name." + program);

                        public boolean accept(File f) {
                            if (f.isDirectory()) {
                                return true;
                            }
                            for (String extension : extensions)
                                if (f.getName().toLowerCase().endsWith(extension.toLowerCase())) return true;
                            return false;
                        }

                        //The description of this filter
                        public String getDescription() {
                            return name;
                        }
                    });
                }
            }
            setData();
        }

        private void setData() {
            File file = new File(filechooser.getCurrentDirectory(), getFileNameField().getText());
//            log.debug(file + ";   " + filechooser.getCurrentDirectory().getName());
            if (!(file.getName().equals(filechooser.getCurrentDirectory().getName())) && (!hasLov || (hasLov && file
                    .exists()))) {
                setText(file.getAbsolutePath());
                setCaretPosition(1);
            } else {
                setText(new String());
            }
        }

        protected JTextField getFileNameField() {
            Vector v = new Vector();
            Stack s = new Stack();
            s.push(filechooser);
            while (!s.isEmpty()) {
                Component c = (Component) s.pop();

                if (c instanceof Container) {
                    Container d = (Container) c;
                    for (int i = 0; i < d.getComponentCount(); i++) {
                        if (d.getComponent(i) instanceof JTextField) v.add(d.getComponent(i));
                        else s.push(d.getComponent(i));
                    }
                }
            }
            return (JTextField) v.get(0);
        }

        private Component getActionDialog(Component c) {
            if (c.getParent() == null || c instanceof ActionDialog) return c;
            return getActionDialog(c.getParent());
        }

        private void createDialog(InputEvent e) {
            if (DialogUtils.showDialogOkCancel(getActionDialog(e.getComponent()), filechooser, title)) {
                setData();
            }
        }
    }


    private static class DirChooserTextField extends PopupTextField {

        JFileChooser filechooser;
        String title;
        boolean hasLov;


        public DirChooserTextField(Icon icon, String title, String text, boolean hasLov, boolean readOnly) {

            super(icon);

            try {
                this.filechooser = new JFileChooser(text);
            } catch (Exception e) {
                // for Vista and Windows 7
                try {
                    UIManager.setLookAndFeel(UIManager.getCrossPlatformLookAndFeelClassName());
                } catch (Exception e1) {
                    log.error(e1);
                }
                this.filechooser = new JFileChooser(text);
                TweakUI.setLookAndFeel();
            }

            this.title = title;
            this.hasLov = hasLov;
            if (!readOnly) {
                addMouseListener(new MouseListener() {

                    public void mouseClicked(MouseEvent e) {
                        if (e.getClickCount() == 2) {
//                            createDialog(e);
                            int returnValue = filechooser.showSaveDialog(null);
                            if (returnValue == JFileChooser.APPROVE_OPTION) {
                                File selectedFile = filechooser.getSelectedFile();
                                setText(selectedFile.getAbsolutePath());
                            }
                        }
                    }

                    public void mouseReleased(MouseEvent arg0) {
                    }

                    public void mouseEntered(MouseEvent arg0) {
                    }

                    public void mouseExited(MouseEvent arg0) {
                    }

                    public void mousePressed(MouseEvent arg0) {
                    }
                });
            }
            filechooser.setFileSelectionMode(JFileChooser.DIRECTORIES_ONLY);
            filechooser.setMultiSelectionEnabled(false);
            setText(text);
        }
    }

    /**
     * loads items on first access, only on demand!
     */
    static private abstract class LazyDisplayableItemsTableModel extends DisplayableItemsTableModel {

        LazyDisplayableItemsTableModel() {
            super(null);
        }

        /**
         * called only once! and on first access on the items
         *
         * @return never return null, use an empty list or you will get
         * exceptions, and it will be called really often
         */
        protected abstract DisplayableItems loadItemsLazy();

        public Object getValueAt(int row, int col) {
            if (items == null) items = loadItemsLazy();
            return items.get(row);
        }


        public int getRowCount() {

            if (items == null) items = loadItemsLazy();
            return super.getRowCount();
        }


    }

    static private class ComponentsItemListener implements ItemListener {


        private Component getActionDialog(Component c) {
            if (c.getParent() == null || c instanceof ActionDialog) return c;
            return getActionDialog(c.getParent());
        }


        //        @Override
        public void itemStateChanged(ItemEvent e) {
            Component dialog = getActionDialog((JCheckBox) e.getSource());
            // log.debug(" dialog:"+dialog);
            if (dialog != null && dialog instanceof ActionDialog) {
                ((ActionDialog) dialog).performComponentAction();
            }
        }
    }

    /**
     * a filed that value can be chosen from a drop down table
     */
    static private class TableComboBoxField extends TableComboBox {
        DisplayableItemInput dii;
        boolean isChangeable;

        public TableComboBoxField(TableModel tableModel, final DisplayableItemInput dii, boolean isChangeable) {
            super(tableModel);
            this.dii = dii;
            this.isChangeable = isChangeable;
            getSelectedItem();   //important, because at first time this method is called from constructor before allocation of parameters

            setEditable(isChangeable);

            setMaximumRowCount(16);
            setValueColumnIndex(0);

            //setStretchToFit(false);
            TableComboBoxSearchable searchable = new TableComboBoxSearchable(this); //for the search function

// hack
//            addKeyListener(new KeyAdapter() {
//                @Override
//                public void keyPressed(KeyEvent e) {
//                    onPressed(e);
//                }
//
//                @Override
//                public void keyReleased(KeyEvent e) {
//                    onPressed(e);
//                }
//
//                private void onPressed(KeyEvent e) {
//                    if (e.getKeyCode() == KeyEvent.VK_DOWN || e.getKeyCode() == KeyEvent.VK_UP) {
//                        showPopup();
//                    }
//                }
//            });


//           the mask will be updated by list selection
            addPropertyChangeListener(new PropertyChangeListener() {
                @Override
                public void propertyChange(PropertyChangeEvent evt) {
                    if ("selectedItem".equals(evt.getPropertyName())) {
                        dii.setSelectedItem((DisplayableItem) getSelectedItem());
                        Component dialog = getActionDialog(getParent());
                        if (dialog instanceof ActionDialog) {
                            ((ActionDialog) dialog).performComponentAction();
                        }
                    }
                }
            });

        }


        @Override
        protected JTable createTable(final TableModel model) {
            final SortableTable table = new SortableTable(model);

            table.setBorder(new EmptyBorder(0, 0, 0, 0));

            table.setDefaultRenderer(Object.class, new DisplayableItemTableCellRenderer());
            CellRendererManager.registerRenderer(Object.class, new DisplayableItemTableCellRenderer());

            return table;
        }


        @Override
        public Object getSelectedItem() {
            Object selectedItem = super.getSelectedItem();
            return StringUtils.isNotEmpty(ObjectUtils.toString(selectedItem)) || dii == null ? selectedItem : dii.getSelectedItem();
        }


        private static Component getActionDialog(Component c) {
            if (c.getParent() == null || c instanceof ActionDialog) return c;
            return getActionDialog(c.getParent());
        }
    }

}