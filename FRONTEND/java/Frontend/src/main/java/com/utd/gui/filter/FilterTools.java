package com.utd.gui.filter;

import com.utd.gui.controls.table.TableTools;
import com.utd.stb.back.data.client.UserInputRecord;

import javax.swing.*;
import javax.swing.table.TableModel;
import java.util.regex.Pattern;


/**
 * @author PeterBuettner.de
 */

public class FilterTools {

    /**
     * Creates a map for the rows that matches the filter, used by
     * {@link FilterTableModel}
     *
     * @param model
     * @param filter
     * @param filterColumn
     * @return null if filter==null, else always a map, but maybe empty if
     * nothing matches
     * @see com.utd.gui.filter.FilterTableModel
     */

    public static int[] createMap(TableModel model, Filter filter, int filterColumn) {

        if (filter != null) {

            int len = model.getRowCount();
            int[] tempMap = new int[len];
            int k = 0;
            for (int i = 0; i < len; i++) {
                // MCD, 02.Mai 2006, filtering the first column
                Object search;
                if (model.getValueAt(i, filterColumn) instanceof UserInputRecord)
                    search = ((UserInputRecord) model.getValueAt(i, filterColumn)).getData();
                else
                    search = model.getValueAt(i, filterColumn);
                if (filter.matches(search)) tempMap[k++] = i;
            }
            int[] result = new int[k];
            System.arraycopy(tempMap, 0, result, 0, k);
            return result;

        }

        return null;

    }

    /**
     * sets the selection in the table to those items matching a regex filter
     * defined by text, nothing selected on text is empty or not a valid regex
     *
     * @param table
     * @param fe
     * @param text
     */
    static public void updateSelectionByRegex(JTable table, FilterErrorMsgDisplay fe,
                                              String text) {

        Pattern pattern = fe.createPattern(text);
        if (text == null || text.length() == 0 || pattern == null) { // no sel
            // on
            // error
            table.clearSelection();
            return;
        }
        TableTools.setSelectionByIndices(table,
                createMap(table.getModel(),
                        new RegExpFilter(pattern),
                        0),
                true);

    }

}