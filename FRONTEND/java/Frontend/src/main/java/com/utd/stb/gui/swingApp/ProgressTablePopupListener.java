package com.utd.stb.gui.swingApp;

import com.utd.gui.event.TablePopupListener;

import javax.swing.*;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;

/**
 * @author Inna Smiljanski  created 18.11.2010
 */
public class ProgressTablePopupListener extends TablePopupListener {
    private JPopupMenu popmen;


    public ProgressTablePopupListener(JPopupMenu popmen) {
        this.popmen = popmen;
    }

    @Override
    public void doPopUp(int row, int col, MouseEvent me) {
        me.getComponent().requestFocusInWindow();
        popmen.show(me.getComponent(), me.getX(), me.getY());
    }

//    @Override
//    public void mouseReleased(MouseEvent me) {
//        if (me.isPopupTrigger())
//            popmen.show(me.getComponent(), me.getX(), me.getY());
//    }

    public static MouseListener getMouseListener(JTable table) {
        for (int i = 0; i < table.getMouseListeners().length; i++)
            if (table.getMouseListeners()[i] instanceof ProgressTablePopupListener) {
                return table.getMouseListeners()[i];
            }
        return null;
    }
}

    
