package com.utd.stb.inter.items;

import com.utd.stb.inter.application.BackendException;


/**
 * DisplayableItems plus a selected item, maybe used for: ComboBox,
 * Select-one-out-of-List (all the same idea)
 *
 * @author PeterBuettner.de
 */
public interface DisplayableItemComboList {

    /**
     * maybe filled on startup with the default/lastUsed/predefined,... item
     *
     * @return
     */
    DisplayableItem getSelectedItem();

    /**
     * Use only items got from {@link getAvailableItems()}. (maybe changed if
     * DisplayableItem always implements a usable equals() so that also
     * 'synthesized' items are possible)
     *
     * @param item
     */
    void setSelectedItem(DisplayableItem item);

    /**
     * The list of available items, maybe lazy loaded, so call only if you need
     * them.
     *
     * @return
     * @throws BackendException
     */
    DisplayableItems getAvailableItems() throws BackendException;

    /**
     * User has to select an item or not.
     *
     * @return
     */
    boolean isEmptySelectionAllowed();

}