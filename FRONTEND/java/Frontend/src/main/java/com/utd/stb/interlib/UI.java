package com.utd.stb.interlib;

import com.utd.stb.gui.itemTools.DisplayableItemsImplUnmodifiable;
import com.utd.stb.inter.items.DisplayableItem;
import com.utd.stb.inter.items.DisplayableItems;
import com.utd.stb.inter.userinput.*;

import java.io.File;
import java.time.ZonedDateTime;
import java.util.List;


/**
 * Implementations of several UserInput types
 *
 * @author PeterBuettner.de
 */
public class UI {

    public static class TextUserInput extends AbstractUserInput {

        private TextInput textInput;

        public TextUserInput(String label, TextInput data) {

            super(label);
            this.textInput = data;
        }

        public Class getType() {

            return TextInput.class;
        }


        public void setData(Object data) {

            textInput.setText((String) data);
        }

        public Object getData() {

            return textInput;
        }
    }

    public static class DateUserInput extends AbstractUserInput {

        private DateInput dateInput;

        public DateUserInput(String label, DateInput data) {

            super(label);
            this.dateInput = data;
        }

        public Class getType() {

            return DateInput.class;
        }

        public void setData(Object data) {

            dateInput.setDate((ZonedDateTime) data);
        }

        public Object getData() {

            return dateInput;
        }
    }

    /**
     * Use for Double,Long,Boolean,MetaInput, ... ~ 'no intermediate structure'.
     * <br>
     * Not for those with subcontainers like DateInput/TextInput
     */
    public static class DirectUserInput extends AbstractUserInput {

        private Object data;
        private Class type;

        public DirectUserInput(String label, Class type, Object value) {

            super(label);
            if (type == null)
                throw new IllegalArgumentException("Can not use empty clazz.");
            this.type = type;
            this.data = value;
        }

        public Class getType() {

            return type;
        }

        public void setData(Object data) {

            if (data != null && !(type.isInstance(data)))
                throw new IllegalArgumentException("Data has to be of type:" +
                        type.getName() +
                        " is:" +
                        data.getClass().getName());
            this.data = data;
        }

        public Object getData() {

            return data;
        }
    }

    public static class BooleanUserInput extends DirectUserInput {
        public BooleanUserInput(String label, Boolean value) {
            super(label, Boolean.class, value);
            setEmptyAllowed(false);
        }
    }

    public static class FileUserInput extends DirectUserInput {
        public FileUserInput(String label, File value) {
            super(label, File.class, value);
        }
    }

    public static class LongUserInput extends DirectUserInput {
        public LongUserInput(String label, Long value) {
            super(label, Long.class, value);
        }
    }

    public static class DoubleUserInput extends DirectUserInput {
        public DoubleUserInput(String label, Double value) {
            super(label, Double.class, value);
        }
    }

    public static abstract class AbstractUserInput implements UserInput {

        private String label;
        private boolean emptyAllowed = true;
        private boolean readOnly = false;
        private boolean visible = true;
        private boolean focus = true;
        private boolean changeable = false;

        public AbstractUserInput(String label) {

            this.label = label;
        }

        public String getLabel() {

            return label;
        }

        public boolean isEmptyAllowed() {

            return emptyAllowed;
        }


        public boolean isReadOnly() {

            return readOnly;
        }

        public boolean isChangeable() {
            return changeable;
        }

        public boolean isVisible() {

            return visible;
        }

        public boolean hasFocus() {

            return focus;
        }

        public Object getExtra(String key) {

            return null;
        }

        public void setEmptyAllowed(boolean emptyAllowed) {

            this.emptyAllowed = emptyAllowed;
        }

        protected void setLabel(String label) {

            this.label = label;
        }

        public void setReadOnly(boolean readOnly) {

            this.readOnly = readOnly;
        }

        public void setVisible(boolean visible) {

            this.visible = visible;
        }

        public void setFocus(boolean focus) {
            this.focus = focus;
        }
    }

    public static class DisplayableItemUserInput extends AbstractUserInput {

        private DisplayableItemInput itemInput;

        public DisplayableItemUserInput(String label, DisplayableItemInput data) {

            super(label);
            this.itemInput = data;
        }

        public Class getType() {

            return DisplayableItemInput.class;
        }

        public void setData(Object data) {

            itemInput.setSelectedItem((DisplayableItem) data);
        }

        public Object getData() {

            return itemInput;
        }

    }

    public static final class MyDisplayableItemInput implements DisplayableItemInput {

        protected DisplayableItem selected;
        protected DisplayableItems allItems;

        public MyDisplayableItemInput(List data) {

            // TODO remove this link to a class that shouldn't be referenced
            allItems = new DisplayableItemsImplUnmodifiable(data, null);
        }

        public DisplayableItem getSelectedItem() {

            return selected;
        }

        public void setSelectedItem(DisplayableItem item) {

            selected = item;
        }

        public DisplayableItems getAvailableItems() {

            return allItems;
        }
    }

    public static class TabSheet extends DirectUserInput {

        public TabSheet(String label) {

            super(label, MetaInput.class, MetaInputFactory.getTabSheet());
        }
    }

    public static class Group extends DirectUserInput {

        public Group(String label) {

            super(label, MetaInput.class, MetaInputFactory.getGroup());
        }
    }

    public static class FloatingText extends DirectUserInput {

        public FloatingText(String label) {

            super(label, MetaInput.class, MetaInputFactory.getFloatingText());
        }
    }

    public static class HtmlText extends DirectUserInput {

        public HtmlText(String label) {

            super(label, MetaInput.class, MetaInputFactory.getHtmlText());
        }
    }

}