package com.utd.stb.interlib;

import com.utd.stb.inter.application.GuiPersistance;

import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;


/**
 * This may used as an intermediate layer between the gui and the backend, it
 * caches the data so repeated reads are faster. Just create a GuiPersistance
 * that reads &amp; writes to the backend, and call
 * GuiPersistenceDataCache#flush to write the data into the backend.
 *
 * @author PeterBuettner.de
 */
public abstract class GuiPersistenceDataCache implements GuiPersistance {

    private Map cache = new HashMap();
    private GuiPersistance delegate;

    public GuiPersistenceDataCache(GuiPersistance backend) {

        this.delegate = backend;
    }// ----------------------------------------------------------

    public void putGuiPersistenceData(Object key, String context, byte[] data) {

        CacheKey mapKey = new CacheKey(getGroupKey(key), context);

        CacheEntry cacheEntry = (CacheEntry) cache.get(mapKey);
        if (cacheEntry != null) {
            cacheEntry.data = data;
            cacheEntry.dirty = true;
        } else
            cache.put(mapKey, new CacheEntry(data, true));
    }// ----------------------------------------------------------

    public byte[] getGuiPersistenceData(Object key, String context) {

        CacheKey mapKey = new CacheKey(getGroupKey(key), context);

        CacheEntry entry = (CacheEntry) cache.get(mapKey);
        if (entry != null) return entry.data;

        byte[] data = delegate.getGuiPersistenceData(key, context);

        // save in cache: don't ask backend again
        cache.put(mapKey, new CacheEntry(data, false));

        return data;
    }// ----------------------------------------------------------

    /**
     * return a key that groups 'similar' objects together, see
     * {@link GuiPersistance#putGuiPersistenceData(Object, String, byte[])}for
     * objects delivered to the gui by the backend, this is needed because of
     * the cache here. if key is generated from the gui it is a String and
     * should be returned unchanged
     *
     * @param key
     * @return
     */
    abstract protected Object getGroupKey(Object key);

    /**
     * stores all data into the delegate, marks all as not dirty
     */
    protected void flush() {

        for (Iterator i = cache.entrySet().iterator(); i.hasNext(); ) {
            Map.Entry e = (Map.Entry) i.next();

            CacheEntry cacheEntry = (CacheEntry) e.getValue();
            if (!cacheEntry.dirty) continue;

            CacheKey cacheKey = (CacheKey) e.getKey();
            delegate.putGuiPersistenceData(cacheKey.key, cacheKey.context, cacheEntry.data);
            cacheEntry.dirty = false;
        }
    }

    //	 #############################################################

    private static class CacheEntry {

        private byte[] data;
        private boolean dirty;

        public CacheEntry(byte[] data, boolean dirty) {

            this.dirty = dirty;
            this.data = data;
        }
    }

    // #############################################################

    private static class CacheKey {

        private Object key;
        private String context;

        public CacheKey(Object key, String context) {

            this.key = key;
            this.context = context;
        }

        public boolean equals(Object obj) {

            if (!(obj instanceof CacheKey)) return false;
            CacheKey other = (CacheKey) obj;
            return isEqual(key, other.key) && isEqual(context, other.context);
        }

        public int hashCode() {

            if (key == null) return context == null ? 0 : context.hashCode();
            // now: key != null
            if (context == null) return key == null ? 0 : key.hashCode();
            // now: both !=null
            return context.hashCode() ^ key.hashCode();
        }

        /**
         * little helper, nulls allowed
         */
        private static boolean isEqual(Object o1, Object o2) {

            if (o1 == null) return o2 == null;
            if (o2 == null) return o1 == null;
            return o1.equals(o2);
        }

    }

}