CREATE OR REPLACE TRIGGER "ISR_ISR$FRONTEND$FILE_B_I" BEFORE INSERT
ON "ISR$FRONTEND$FILE" REFERENCING OLD AS old NEW AS new
FOR EACH ROW
BEGIN
  :New.MODIFIEDON := SYSDATE;
  :New.MODIFIEDBY := STB$SECURITY.getOracleUserName(STB$SECURITY.getCurrentUser); 
  if :New.binaryfile is not null then
    :New.FILESIZE := dbms_lob.getlength(:New.binaryfile);
  else
    :New.FILESIZE := dbms_lob.getlength(:New.textfile);
  end if;
end;